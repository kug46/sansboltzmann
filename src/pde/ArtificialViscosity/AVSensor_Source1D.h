// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef AVSENSOR_SOURCE1D_H
#define AVSENSOR_SOURCE1D_H

// 1-D Advection-Diffusion PDE: source term term the sensor parameter

#include "tools/SANSnumerics.h"     // Real
#include "Surreal/PromoteSurreal.h"

#include "Topology/Dimension.h"

#include "tools/smoothmath.h"

#include "Surreal/SurrealS.h"

#include <iostream>
#include <cmath> // fabs

namespace SANS
{

//----------------------------------------------------------------------------//
// Source: Uniform
//
// source = a*q where 'a' is a constant
//----------------------------------------------------------------------------//

template <template <class> class PDETraitsSize>
class AVSensor_Source1D_Uniform
{
public:
  template<class T>
  using ArrayQ = typename PDETraitsSize<PhysD1>::template ArrayQ<T>;

  template<class T>
  using MatrixQ = typename PDETraitsSize<PhysD1>::template MatrixQ<T>;

  static const int iSens = PDETraitsSize<PhysD1>::iSens;

  explicit AVSensor_Source1D_Uniform( const Real a) : a_(a) {}

  bool hasSourceTerm() const { return true; }
  bool hasSourceTrace() const { return false; }
  bool hasSourceLiftedQuantity() const { return false; }
  bool needsSolutionGradientforSource() const { return false; }

  template<class PDE, class Tq, class Tg, class Tp>
  void source( const PDE& pde,
               const Tp& param, const Real& x, const Real& time,
               const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
               ArrayQ< typename promote_Surreal<Tq,Tg>::type >& source ) const
  {
    source(iSens) += a_*q(iSens);
  }

  template <class PDE, class Tp, class T, class Ts>
  void sourceTrace(const PDE& pde,
                   const Tp& paramL, const Real& xL,
                   const Tp& paramR, const Real& xR, const Real& time,
                   const ArrayQ<T>& qL, const ArrayQ<T>& qxL,
                   const ArrayQ<T>& qR, const ArrayQ<T>& qxR,
                   ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
  {
    sourceL(iSens) += a_*qL(iSens);
    sourceR(iSens) += a_*qR(iSens);
  }

  template <class PDE, class Tp, class T>
  void jacobianSource(
      const PDE& pde,
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx,
      MatrixQ<T>& dsdu ) const
  {
    dsdu(iSens,iSens) += a_;
  }

  template <class PDE, class Tp, class T>
  void jacobianSourceAbsoluteValue(
      const PDE& pde,
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx,
      MatrixQ<T>& dsdu ) const
  {
    dsdu(iSens,iSens) += fabs(a_);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

protected:
  const Real a_;
};


template <template <class> class PDETraitsSize, class Sensor>
class AVSensor_Source1D_Jump
{
public:
  template<class T>
  using ArrayQ = typename PDETraitsSize<PhysD1>::template ArrayQ<T>;

  template<class T>
  using MatrixQ = typename PDETraitsSize<PhysD1>::template MatrixQ<T>;

  static const int iSens = PDETraitsSize<PhysD1>::iSens;

  static const int D = 1;                     // physical dimensions


  typedef DLA::VectorS<D, Real> VectorX;

  explicit AVSensor_Source1D_Jump(int PDEorder, const Sensor& sensor, const Real C1 = 3.0, const Real C3 = 1.0) :
    PDEorder_(PDEorder),
    sensor_(sensor),
    C1_(C1),
    C3_(C3)
  {
    // Nothing
  }

  bool hasSourceTerm() const { return true; }
  bool hasSourceTrace() const { return true; }
  bool hasSourceLiftedQuantity() const { return true; }
  bool needsSolutionGradientforSource() const { return false; }

  template<class PDE, class Tp, class Tq, class Tg, class Ts>
  void source( const PDE& pde, const Tp& param, const Real& x, const Real& time,
               const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
               ArrayQ<Ts>& source ) const
  {
    SANS_DEVELOPER_EXCEPTION("AVSensor_Source1D_Jump::source - This source term requires a lifted quantity! Should not get here.");
  }

  template<class PDE, class Tp, class Tlq, class Tq, class Tg, class Ts>
  void source( const PDE& pde, const Tp& logH,
               const Real& x, const Real& time,
               const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
               ArrayQ<Ts>& source ) const
  {
    Tlq abs_lifted_quantity = fabs(lifted_quantity); //lifted_quantity may go negative if lifted quantity field is P1 or higher

    Tlq Sk = log10(abs_lifted_quantity + 1e-16);

#if 0

    Real psi0 = 1;
    if ( PDEorder_ > 0 )
      psi0 = 3.0 + 2.0*log10(PDEorder_);

    Tlq xi = Sk + psi0;

#if 0
    // Add +0.5 to center around smooth activiation
    xi += 0.5;
    Tlq sensor = smoothActivation_tanh(xi);
#else
    xi += 0.5; // Add +0.5 to center around smooth activiation
    xi /= 2;   // Scale so it activates over 2 orders of magnitude
#endif

#else
    const Real Sk_min = -4;
    const Real Sk_max = -1;
    Tlq xi = (Sk - Sk_min)/(Sk_max - Sk_min);
#endif

    Real alpha = 10;
    Tlq sensor = smoothActivation_exp(xi, alpha);


//    source(iSens) += (q(iSens) - C3_* lambda*hbar/max(PDEorder_,1) * sensor);
    source(iSens) += (q(iSens) - C3_*sensor);
  }

  template<class Tp, class Tq, class Ts>
  void sourceLiftedQuantity(
      const Tp& paramL, const Real& xL,
      const Tp& paramR, const Real& xR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      Ts& s ) const
  {
    Tq gL = 0, gR = 0;
    sensor_.jumpQuantity( qL, gL );
    sensor_.jumpQuantity( qR, gR );

    Tq dg = gL - gR;
    dg = smoothabs0(dg, 1.0e-5);

    Tq gbar = 0.5*(fabs(gL) + fabs(gR));

    // Check if dg is small relative to gbar
    static const Real delta = 1e-3;
    if ( delta*gbar + 1.0 == 1.0 )
      return; //gbar is nearly zero, just return

    Tq jump = dg / gbar;
    s = smoothabs0(jump, 0.03);
  }

  template <class PDE, class Tp, class Tq, class Tg, class Ts>
  void sourceTrace(
      const PDE& pde, const Tp& paramL, const Real& xL,
      const Tp& paramR, const Real& xR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
      ArrayQ<Ts>& sourceL,
      ArrayQ<Ts>& sourceR ) const
  {
    // Nothing
  }

  template <class PDE, class Tq, class Tg, class Ts>
  void jacobianSource(
      const PDE& pde, const DLA::MatrixSymS<D,Real>& param,
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Ts>& dsdu ) const
  {
#if 0
    DLA::MatrixSymS<D,Tp> H = exp(param); //generalized h-tensor
    DLA::MatrixSymS<D,Tp> Hsq = H*H;

    dsdu(iSens,iSens) += C3_;
#endif
  }

  template <class PDE, class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue(
      const PDE& pde, const DLA::MatrixSymS<D,Real>& param,
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Ts>& dsdu ) const
  {
#if 0
    DLA::MatrixSymS<D,Tp> H = exp(param); //generalized h-tensor
    DLA::MatrixSymS<D,Tp> Hsq = H*H;

    dsdu(iSens,iSens) += fabs(C3_);
#endif
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

protected:
  int PDEorder_;
  const Sensor& sensor_;
  const Real C1_;
  const Real C3_;
};


template <class PDE>
class AVSensor_Source1D_TwoPhase
{
public:
  static const int D = PhysD1::D;

  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  template<class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;

  static const int iSens = PDE::N - 1;
  static_assert(iSens == PDE::N - 1, "Artificial viscosity variable index mismatch!");

  typedef typename PDE::QInterpreter QInterpreter; // solution variable interpreter type

  explicit AVSensor_Source1D_TwoPhase(const PDE& pdeAV, const Real Sk_min = -4, const Real Sk_max = -1,
                                      const Real C = 1.0, const Real smooth_eps = 1e-3)
  : pdeAV_(pdeAV), Sk_min_(Sk_min), Sk_max_(Sk_max), C_(C), smooth_eps_(smooth_eps), qInterpreter_(pdeAV_.variableInterpreter())
  {
    // Nothing
  }

  bool hasSourceTerm() const { return true; }
  bool hasSourceTrace() const { return false; }
  bool hasSourceLiftedQuantity() const { return true; }
  bool needsSolutionGradientforSource() const { return true; }

  template<class Tp, class Tq, class Tg, class Ts>
  void source( const PDE& pde, const Tp& param, const Real& x, const Real& time,
               const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
               ArrayQ<Ts>& source ) const
  {
    SANS_DEVELOPER_EXCEPTION("AVSensor_Source1D_TwoPhase::source - This source term requires a lifted quantity! Should not get here.");
  }

  template<class Tp, class Tlq, class Tq, class Tg, class Ts>
  void source( const PDE& pde, const Tp& param, const Real& x, const Real& time,
               const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
               ArrayQ<Ts>& source ) const
  {
    Tlq abs_lifted_quantity = fabs(lifted_quantity); //lifted_quantity may go negative if lifted quantity field is P1 or higher

    Tlq Sk = log10(abs_lifted_quantity + 1e-16);
//    Real psi0 = 3.0 + 3.0*log10((Real) pdeAV_.PDEorder());
//    Tlq xi = 0.5*(Sk + psi0);

    Tlq xi = (Sk - Sk_min_)/(Sk_max_ - Sk_min_);

//    Tlq sensor = smoothActivation_tanh(xi);
    const Real alpha = 10;
    Tlq sensor = smoothActivation_exp(xi, alpha);

    Ts nu_max;
    pdeAV_.artViscMax(param, x, time, q, qx, nu_max);

    source(iSens) += q(iSens) - C_*sensor*nu_max;
  }

  template<class Tp, class Tq, class Ts>
  void sourceLiftedQuantity(
      const Tp& paramL, const Real& xL,
      const Tp& paramR, const Real& xR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      Ts& s ) const
  {
    Tq SwL = 0, SwR = 0;
    qInterpreter_.eval_Sw( qL, SwL );
    qInterpreter_.eval_Sw( qR, SwR );

    Tq jump = SwL - SwR;
    s = smoothabs0(jump, smooth_eps_);

    //Penalty for non-physical values
//    s += 0.5*( max(-SwL, 0.0) + max(SwL - 1.0, 0.0) );
//    s += 0.5*( max(-SwR, 0.0) + max(SwR - 1.0, 0.0) );
  }

  template <class Tp, class Tq, class Tg, class Ts>
  void sourceTrace(
      const PDE& pde, const Tp& paramL, const Real& xL,
      const Tp& paramR, const Real& xR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
      ArrayQ<Ts>& sourceL,
      ArrayQ<Ts>& sourceR ) const {}

  template <class Tp, class Tq, class Tg, class Ts>
  void jacobianSource(
      const PDE& pde, const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Ts>& dsdu ) const
  {
    dsdu(iSens,iSens) += 1.0;
  }

  template <class Tp, class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue(
      const PDE& pde, const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Ts>& dsdu ) const
  {
    dsdu(iSens,iSens) += 1.0;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

protected:
  const PDE& pdeAV_;
  const Real Sk_min_, Sk_max_;
  const Real C_;
  const Real smooth_eps_;
  const QInterpreter& qInterpreter_;
};

} //namespace SANS

#endif  // AVSENSOR_SOURCE1D_H
