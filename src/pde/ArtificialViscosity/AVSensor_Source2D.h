// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef AVSENSOR_SOURCE2D_H
#define AVSENSOR_SOURCE2D_H

// 1-D Advection-Diffusion PDE: source term term the sensor parameter

#include "tools/SANSnumerics.h"     // Real
#include "Surreal/PromoteSurreal.h"

#include "Topology/Dimension.h"

#include "Field/Tuple/ParamTuple.h"
#include "tools/Tuple.h"
#include "tools/smoothmath.h"

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Exp.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/Eigen.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include <iostream>
#include <limits>
#include <cmath> // fabs

namespace SANS
{

//----------------------------------------------------------------------------//
// Source: Uniform
//
// source = a*q where 'a' is a constant
//----------------------------------------------------------------------------//

template <template <class> class PDETraitsSize>
class AVSensor_Source2D_Uniform
{
public:
  template<class T>
  using ArrayQ = typename PDETraitsSize<PhysD2>::template ArrayQ<T>;

  template<class T>
  using MatrixQ = typename PDETraitsSize<PhysD2>::template MatrixQ<T>;

  static const int iSens = PDETraitsSize<PhysD2>::iSens;

  explicit AVSensor_Source2D_Uniform( const Real a) : a_(a) {}

  bool hasSourceTerm() const { return true; }
  bool hasSourceTrace() const { return false; }
  bool hasSourceLiftedQuantity() const { return false; }
  bool needsSolutionGradientforSource() const { return false; }

  template<class PDE, class Tq, class Tg, class Tp, class Ts>
  void source( const PDE& pde, const Tp& param, const Real& x, const Real& y, const Real& time,
               const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
               ArrayQ<Ts>& source ) const
  {
    source(iSens) += a_*q(iSens);
  }

  template <class PDE, class Tp, class T, class Ts>
  void sourceTrace(const PDE& pde, const Tp& paramL, const Real& xL, const Real& yL,
                   const Tp& paramR, const Real& xR, const Real& yR, const Real& time,
                   const ArrayQ<T>& qL, const ArrayQ<T>& qxL, const ArrayQ<T>& qyL,
                   const ArrayQ<T>& qR, const ArrayQ<T>& qxR, const ArrayQ<T>& qyR,
                   ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
  {
    sourceL(iSens) += a_*qL(iSens);
    sourceR(iSens) += a_*qR(iSens);
  }

  template<class PDE, class Tp, class Tlq, class Tq, class Tg, class Ts>
  void source( const PDE& pde, const Tp& logH,
               const Real& x, const Real& y, const Real& time,
               const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
               ArrayQ<Ts>& source ) const
  {
    // Nothing
  }

  template<class Tp, class Tq, class Ts>
  void sourceLiftedQuantity(
      const Tp& paramL, const Real& xL, const Real& yL,
      const Tp& paramR, const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      Ts& s ) const
  {
    // Nothing
  }

  template <class PDE, class Tp, class T>
  void jacobianSource(
      const PDE& pde, const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
      MatrixQ<T>& dsdu ) const
  {
    dsdu(iSens,iSens) += a_;
  }

  template <class PDE, class Tp, class T>
  void jacobianSourceAbsoluteValue(
      const PDE& pde, const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
      MatrixQ<T>& dsdu ) const
  {
    dsdu(iSens,iSens) += fabs(a_);
  }

  template <class PDE, class Tp, class Tq, class Tg, class Ts>
    void jacobianGradientSource(
        const PDE& pde, const Tp& param,
        const Real& x, const Real& y, const Real& time,
        const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
        MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy ) const
  {
    // Nothing
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

protected:
  const Real a_;
};


template <template <class> class PDETraitsSize, class Sensor>
class AVSensor_Source2D_Jump
{
public:
  template<class T>
  using ArrayQ = typename PDETraitsSize<PhysD2>::template ArrayQ<T>;

  template<class T>
  using MatrixQ = typename PDETraitsSize<PhysD2>::template MatrixQ<T>;

  static const int iSens = PDETraitsSize<PhysD2>::iSens;

  static const int D = 2;                     // physical dimensions


  typedef DLA::VectorS<D, Real> VectorX;

  explicit AVSensor_Source2D_Jump(int PDEorder, const Sensor& sensor, const Real C1 = 3.0, const Real C3 = 1.0) :
    PDEorder_(PDEorder),
    sensor_(sensor),
    C1_(C1),
    C3_(C3)
  {
    // Nothing
  }

  bool hasSourceTerm() const { return true; }
  bool hasSourceTrace() const { return true; }
  bool hasSourceLiftedQuantity() const { return true; }
  bool needsSolutionGradientforSource() const { return false; }

  template<class PDE, class Tp, class Tq, class Tg, class Ts>
  void source( const PDE& pde, const Tp& param, const Real& x, const Real& y, const Real& time,
               const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
               ArrayQ<Ts>& source ) const
  {
    SANS_DEVELOPER_EXCEPTION("AVSensor_Source2D_Jump::source - This source term requires a lifted quantity! Should not get here.");
  }

  template<class PDE, class Tp, class Tlq, class Tq, class Tg, class Ts>
  void source( const PDE& pde, const Tp& logH,
               const Real& x, const Real& y, const Real& time,
               const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
               ArrayQ<Ts>& source ) const
  {
    Tlq abs_lifted_quantity = fabs(lifted_quantity); //lifted_quantity may go negative if lifted quantity field is P1 or higher

    Tlq Sk = log10(abs_lifted_quantity + 1e-16);
    Real psi0 = 1;
    if ( PDEorder_ > 0 )
      psi0 = 3.0 + 2.0*log10(PDEorder_);
      //psi0 = 2.25 + 3.0*log10(PDEorder_);

    Tlq xi = Sk + psi0;

#if 0
    // Add +0.5 to center around smooth activiation
    xi += 0.5;
    Tlq sensor = smoothActivation_tanh(xi);
#else
    xi += 0.5; // Add +0.5 to center around smooth activiation
    xi /= 2;   // Scale so it activates over 2 orders of magnitude
    Real alpha = 10;
    Tlq sensor = smoothActivation_exp(xi, alpha);
#endif
    //source(iSens) += (q(iSens) - C3_* lambda*hbar/max(PDEorder_,1) * sensor);
    source(iSens) += (q(iSens) - C3_*sensor);
  }

  template<class Tp, class Tq, class Ts>
  void sourceLiftedQuantity(
      const Tp& paramL, const Real& xL, const Real& yL,
      const Tp& paramR, const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      Ts& s ) const
  {
    Tq gL = 0, gR = 0;
    sensor_.jumpQuantity( qL, gL );
    sensor_.jumpQuantity( qR, gR );

    Tq dg = gL - gR;
    dg = smoothabs0(dg, 1.0e-5);

    Tq gbar = 0.5*(fabs(gL) + fabs(gR));

    // Check if dg is small relative to gbar
    static const Real delta = 1e-3;
    if ( delta*gbar + 1.0 == 1.0 )
      return; //gbar is nearly zero, just return

    Tq jump = dg / gbar;
    s = smoothabs0(jump, 0.03);
  }

  template <class PDE, class Tp, class Tq, class Tg, class Ts>
  void sourceTrace(
      const PDE& pde, const Tp& paramL, const Real& xL, const Real& yL,
      const Tp& paramR, const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      ArrayQ<Ts>& sourceL,
      ArrayQ<Ts>& sourceR ) const
  {
    // Nothing
  }

  template <class PDE, class Tp, class Tq, class Tg, class Ts>
  void jacobianSource(
      const PDE& pde, const Tp& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const
  {
#if 0
    DLA::MatrixSymS<D,Tp> H = exp(param); //generalized h-tensor
    DLA::MatrixSymS<D,Tp> Hsq = H*H;

    dsdu(iSens,iSens) += C3_;
#endif
  }

  template <class PDE, class Tp, class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue(
      const PDE& pde, const Tp& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const
  {
#if 0
    DLA::MatrixSymS<D,Tp> H = exp(param); //generalized h-tensor
    DLA::MatrixSymS<D,Tp> Hsq = H*H;

    dsdu(iSens,iSens) += fabs(C3_);
#endif
  }

  template <class PDE, class Tp, class Tq, class Tg, class Ts>
    void jacobianGradientSource(
        const PDE& pde, const Tp& param,
        const Real& x, const Real& y, const Real& time,
        const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
        MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy ) const
  {
    // Nothing
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "AVSensor_Source2D_Jump:" << std::endl;
    out << indent << "  PDEorder_ = " << PDEorder_ << std::endl;
    out << indent << "  C1_ = " << C1_ << std::endl;
    out << indent << "  C3_ = " << C3_ << std::endl;
  }

protected:
  int PDEorder_;
  const Sensor& sensor_;
  const Real C1_;
  const Real C3_;
};


template <class PDE>
class AVSensor_Source2D_TwoPhase
{
public:
  static const int D = PhysD2::D;

  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  template<class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;

  static const int iSens = 2;
  static_assert(iSens == PDE::N - 1, "Artificial viscosity variable index mismatch!");

  typedef typename PDE::QInterpreter QInterpreter; // solution variable interpreter type

  explicit AVSensor_Source2D_TwoPhase(const PDE& pdeAV, const Real Sk_min = -4, const Real Sk_max = -1,
                                      const Real C = 1.0, const Real smooth_eps = 1e-3)
  : pdeAV_(pdeAV), Sk_min_(Sk_min), Sk_max_(Sk_max), C_(C), smooth_eps_(smooth_eps), qInterpreter_(pdeAV_.variableInterpreter())
  {
    // Nothing
  }

  bool hasSourceTerm() const { return true; }
  bool hasSourceTrace() const { return false; }
  bool hasSourceLiftedQuantity() const { return true; }
  bool needsSolutionGradientforSource() const { return true; }

  template<class Tp, class Tq, class Tg, class Ts>
  void source( const PDE& pde, const Tp& param, const Real& x, const Real& y, const Real& time,
               const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
               ArrayQ<Ts>& source ) const
  {
    SANS_DEVELOPER_EXCEPTION("AVSensor_Source2D_TwoPhase::source - This source term requires a lifted quantity! Should not get here.");
  }

  template<class Tp, class Tlq, class Tq, class Tg, class Ts>
  void source( const PDE& pde, const Tp& param, const Real& x, const Real& y, const Real& time,
               const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
               ArrayQ<Ts>& source ) const
  {
    Tlq abs_lifted_quantity = fabs(lifted_quantity); //lifted_quantity may go negative if lifted quantity field is P1 or higher

    Tlq Sk = log10(abs_lifted_quantity + 1e-16);
//    Real psi0 = 3.0 + 3.0*log10((Real) pdeAV_.PDEorder());
//    Tlq xi = 0.5*(Sk + psi0);

    Tlq xi = (Sk - Sk_min_)/(Sk_max_ - Sk_min_);

//    Tlq sensor = smoothActivation_tanh(xi);
    const Real alpha = 10;
    Tlq sensor = smoothActivation_exp(xi, alpha);

    Ts nu_max;
    pdeAV_.artViscMax(param, x, y, time, q, qx, qy, nu_max);

    source(iSens) += q(iSens) - C_*sensor*nu_max;
  }

  template<class Tp, class Tq, class Ts>
  void sourceLiftedQuantity(
      const Tp& paramL, const Real& xL, const Real& yL,
      const Tp& paramR, const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      Ts& s ) const
  {
    Tq SwL = 0, SwR = 0;
    qInterpreter_.eval_Sw( qL, SwL );
    qInterpreter_.eval_Sw( qR, SwR );

    Tq jumpSw = SwL - SwR;
    s = smoothabs0(jumpSw, smooth_eps_); //penalty on saturation jump

#if 0
    Tq pnL = 0, pnR = 0;
    qInterpreter_.eval_pn( qL, pnL );
    qInterpreter_.eval_pn( qR, pnR );

    Tq avgpn = 0.5*(fabs(pnL) + fabs(pnR));
    Tq jumppn = (pnL - pnR) / avgpn;
    s += smoothabs0(jumppn, smooth_eps_); //penalty on pressure jump
#endif
  }

  template <class Tp, class Tq, class Tg, class Ts>
  void sourceTrace(
      const PDE& pde, const Tp& paramL, const Real& xL, const Real& yL,
      const Tp& paramR, const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      ArrayQ<Ts>& sourceL,
      ArrayQ<Ts>& sourceR ) const {}

  template <class Tp, class Tq, class Tg, class Ts>
  void jacobianSource(
      const PDE& pde, const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const
  {
    dsdu(iSens,iSens) += 1.0;
  }

  template <class Tp, class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue(
      const PDE& pde, const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const
  {
    dsdu(iSens,iSens) += 1.0;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

protected:
  const PDE& pdeAV_;
  const Real Sk_min_, Sk_max_;
  const Real C_;
  const Real smooth_eps_;
  const QInterpreter& qInterpreter_;
};



//----------------------------------------------------------------------------//
// Shock sensor based on pressure gradient ala Ryan Glasby

template <template <class> class PDETraitsSize, class Sensor>
class AVSensor_Source2D_PressureGrad
{
public:
  template<class T>
  using ArrayQ = typename PDETraitsSize<PhysD2>::template ArrayQ<T>;

  template<class T>
  using MatrixQ = typename PDETraitsSize<PhysD2>::template MatrixQ<T>;

  static const int D = PhysD2::D;
  static const int iSens = PDETraitsSize<PhysD2>::iSens;

  typedef DLA::VectorS<D, Real> VectorX;

  explicit AVSensor_Source2D_PressureGrad(int PDEorder, const Sensor& sensor, const Real kappa = 5.0, const Real C1 = 3.0) :
    PDEorder_(Real(PDEorder)),
    sensor_(sensor),
    kappa_(kappa), C1_(C1), C3_(5.0)
  {
    // Nothing
  }

  bool hasSourceTerm() const { return true; }
  bool hasSourceTrace() const { return false; }
  bool hasSourceLiftedQuantity() const { return false; }
  bool needsSolutionGradientforSource() const { return true; }

  template<class PDE, class Tq, class Tg, class Ts>
  void source( const PDE& pde, const DLA::MatrixSymS<D,Real>& logH, const Real& x, const Real& y, const Real& time,
               const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
               ArrayQ<Ts>& src ) const
  {
    typedef typename promote_Surreal<Tq,Tg>::type Tp;

    if ( PDEorder_ < 0.5 ) return;

    Tq rho = 0, u = 0, v = 0, t = 0, p = 0, c = 0;
    Tp px = 0, py = 0;

    pde.variableInterpreter().eval( q, rho, u, v, t );
    p  = pde.gasModel().pressure(rho, t);
    c  = pde.gasModel().speedofSound(t);

    Tp rhox = 0, ux = 0, vx = 0, tx = 0;
    Tp rhoy = 0, uy = 0, vy = 0, ty = 0;

    pde.variableInterpreter().evalGradient( q, qx, rhox, ux, vx, tx );
    pde.variableInterpreter().evalGradient( q, qy, rhoy, uy, vy, ty );

    pde.gasModel().pressureGradient( rho, t, rhox, tx, px );
    pde.gasModel().pressureGradient( rho, t, rhoy, ty, py );

    DLA::MatrixSymS<D,Real> H = exp(logH); //generalized h-tensor

    typedef DLA::VectorS<D, Real> VectorX;
    VectorX hPrincipal;
    DLA::EigenValues(H, hPrincipal);

    //Get minimum length scale
    Real hmin = std::numeric_limits<Real>::max();
    for (int d = 0; d < D; d++)
      hmin = min(hmin, hPrincipal[d]);

    DLA::VectorS<D, Tp> gradp = {px, py};

    // sensor from Ryan Glasby, but with a generalized H scale
    //Tp Ugradp = Transpose(V)*H*gradp;
    //Ugradp /= max(PDEorder_, Real(1));

    // sensor from Ryan Glasby
    Ts Ugradp = hmin*(u*px + v*py);
    Ugradp /= max(PDEorder_, Real(1));

    // Only positive values of the sensor indicate a shock
    Ugradp = max(Tp(0), Ugradp);

    Ts sensor = Ugradp/(Ugradp + kappa_*c*p);

    sensor = sensor * tanh(10.0 * sensor);

    src(iSens) += C3_*(q(iSens) - sensor);
  }

  template<class PDE, class Tq, class Tg, class Ts>
  void source( const PDE& pde, const ParamTuple<DLA::MatrixSymS<D,Real>, Real, TupleClass<>>& param,
               const Real& x, const Real& y, const Real& time,
               const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
               ArrayQ<Ts>& src ) const
  {
    source(pde, param.left(), x, y, time, q, qx, qy, src);
  }

  template<class Tp, class Tlq, class Tq, class Tg, class Ts>
  void source( const Tp& param, const Real& x, const Real& y, const Real& time,
               const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
               ArrayQ<Ts>& source ) const
  {
    SANS_DEVELOPER_EXCEPTION("AVSensor_Source2D_PressureGrad::source with lifted quantity - Not implemented yet.");
  }

  template<class Tp, class Tq, class Ts>
  void sourceLiftedQuantity(
      const Tp& paramL, const Real& xL, const Real& yL,
      const Tp& paramR, const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      Ts& s ) const
  {
    SANS_DEVELOPER_EXCEPTION("AVSensor_Source2D_PressureGrad::sourceLiftedQuantity - Not implemented yet.");
  }

  template <class PDE, class Tq, class Tg, class Ts>
  void jacobianSource(
      const PDE& pde, const DLA::MatrixSymS<D,Real>& logH,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const
  {
    if ( PDEorder_ < 0.5 ) return;
#if 1
    typedef typename promote_Surreal<Tq,Tg>::type Tp;

    Tq rho = 0, u = 0, v = 0, t = 0, p = 0, c = 0;
    Tp px = 0, py = 0;

    pde.variableInterpreter().eval( q, rho, u, v, t );
    p  = pde.gasModel().pressure(rho, t);
    c  = pde.gasModel().speedofSound(t);

    Tq p_rho, p_t, c_t;
    pde.gasModel().pressureJacobian(rho, t, p_rho, p_t);
    pde.gasModel().speedofSoundJacobian(t, c_t);

    Tp rhox = 0, ux = 0, vx = 0, tx = 0;
    Tp rhoy = 0, uy = 0, vy = 0, ty = 0;

    pde.variableInterpreter().evalGradient( q, qx, rhox, ux, vx, tx );
    pde.variableInterpreter().evalGradient( q, qy, rhoy, uy, vy, ty );

    pde.gasModel().pressureGradient( rho, t, rhox, tx, px );
    pde.gasModel().pressureGradient( rho, t, rhoy, ty, py );

    Real Cv = pde.gasModel().Cv();
    Tq E = pde.gasModel().energy(rho, t) + 0.5*(u*u + v*v);
    Tq t_rho, t_rhou, t_rhov, t_rhoE;

    // Temperature from conservative variables
    //t = ( rhoE/rho - 0.5*( rhou*rhou + rhov*rhov )/(rho*rho) ) / Cv
    t_rho  = ( -E/rho + ( u*u + v*v )/rho ) / Cv;
    t_rhou = (        - (   u       )/rho ) / Cv;
    t_rhov = (        - (         v )/rho ) / Cv;
    t_rhoE = (  1/rho               ) / Cv;

    Tq c_rho  = c_t*t_rho;
    Tq c_rhou = c_t*t_rhou;
    Tq c_rhov = c_t*t_rhov;
    Tq c_rhoE = c_t*t_rhoE;

       p_rho += p_t*t_rho;
    Tq p_rhou = p_t*t_rhou;
    Tq p_rhov = p_t*t_rhov;
    Tq p_rhoE = p_t*t_rhoE;

    // velocity Jacobian wrt conservative variables
    Tq u_rho  = -u/rho;
    Tq u_rhou =  1/rho;

    Tq v_rho  = -v/rho;
    Tq v_rhov =  1/rho;

    DLA::MatrixSymS<D,Real> H = exp(logH); //generalized h-tensor

    typedef DLA::VectorS<D, Real> VectorX;
    VectorX hPrincipal;
    DLA::EigenValues(H, hPrincipal);

    //Get minimum length scale
    Real hmin = std::numeric_limits<Real>::max();
    for (int d = 0; d < D; d++)
      hmin = min(hmin, hPrincipal[d]);
    // pre-divide by order...
    hmin /= max(PDEorder_, Real(1));

    // sensor from Ryan Glasby, but with a generalized H scale
    //Tp Ugradp = Transpose(V)*H*gradp;
    //Ugradp /= max(PDEorder_, Real(1));

    // sensor from Ryan Glasby
    Ts Ugradp = hmin*(u*px + v*py);
    Ts Ugradp_rho  = (u_rho*px + v_rho*py)*hmin;
    Ts Ugradp_rhou = u_rhou*px*hmin;
    Ts Ugradp_rhov = v_rhov*py*hmin;
    Ts Ugradp_rhoE = 0.0;

    // Only positive values of the sensor indicate a shock
    if (Ugradp < 0)
    {
      Ugradp_rho = 0.0;
      Ugradp_rhou = 0.0;
      Ugradp_rhov = 0.0;
      Ugradp_rhoE = 0.0;
    }
    Ugradp = max(Tp(0), Ugradp);

    Ts s = Ugradp/(Ugradp + kappa_*c*p);
    Ts s_rho  = kappa_*c*p/pow(Ugradp + kappa_*c*p,2) * Ugradp_rho  - Ugradp/pow(Ugradp + kappa_*c*p,2) * kappa_ * (p*c_rho  + c*p_rho );
    Ts s_rhou = kappa_*c*p/pow(Ugradp + kappa_*c*p,2) * Ugradp_rhou - Ugradp/pow(Ugradp + kappa_*c*p,2) * kappa_ * (p*c_rhou + c*p_rhou);
    Ts s_rhov = kappa_*c*p/pow(Ugradp + kappa_*c*p,2) * Ugradp_rhov - Ugradp/pow(Ugradp + kappa_*c*p,2) * kappa_ * (p*c_rhov + c*p_rhov);
    Ts s_rhoE = kappa_*c*p/pow(Ugradp + kappa_*c*p,2) * Ugradp_rhoE - Ugradp/pow(Ugradp + kappa_*c*p,2) * kappa_ * (p*c_rhoE + c*p_rhoE);

    Ts stanh_s = 10 * s / (cosh(10*s)*cosh(10*s)) + tanh(10*s);

    dsdu(iSens,PDE::iCont) -= C3_*stanh_s * s_rho;
    dsdu(iSens,PDE::ixMom) -= C3_*stanh_s * s_rhou;
    dsdu(iSens,PDE::iyMom) -= C3_*stanh_s * s_rhov;
    dsdu(iSens,PDE::iEngy) -= C3_*stanh_s * s_rhoE;
#endif
    dsdu(iSens,iSens) += C3_*1.;
  }

  template<class PDE, class Tp, class Tlq, class Tq, class Tg, class Ts>
  void source( const PDE& pde, const Tp& logH,
               const Real& x, const Real& y, const Real& time,
               const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
               ArrayQ<Ts>& source ) const
  {
    SANS_DEVELOPER_EXCEPTION("AVSensor_Source2D_PressureGrad::source with lifted quantity - Not implemented yet.");
  }

  template <class PDE, class Tq, class Tg, class Ts>
  void jacobianSource(
      const PDE& pde, const ParamTuple<DLA::MatrixSymS<D,Real>, Real, TupleClass<>>& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const
  {
    jacobianSource(pde, param.left(), x, y, time, q, qx, qy, dsdu);
  }

  template <class PDE, class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue(
      const PDE& pde, const ParamTuple<DLA::MatrixSymS<D,Real>, Real, TupleClass<>>& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const
  {
    //since 1,1 entry is positive, jacobianSource is pos def.
    jacobianSource(pde, param.left(), x, y, time, q, qx, qy, dsdu);
  }

  template <class PDE, class Tq, class Tg, class Ts>
  void jacobianGradientSource(
      const PDE& pde, const DLA::MatrixSymS<D,Real>& logH,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy ) const
  {
    if ( PDEorder_ < 0.5 ) return;
#if 0
    typedef typename promote_Surreal<Tq,Tg>::type Tp;

    Tq rho = 0, u = 0, v = 0, t = 0, p = 0, c = 0;
    Tp px = 0, py = 0;

    pde.variableInterpreter().eval( q, rho, u, v, t );
    p  = pde.gasModel().pressure(rho, t);
    c  = pde.gasModel().speedofSound(t);

    Tq e = pde.gasModel().energy(rho, t);
    Tq E = e + 0.5*(u*u + v*v);

    Tp rhox = 0, ux = 0, vx = 0, tx = 0;
    Tp rhoy = 0, uy = 0, vy = 0, ty = 0;

    pde.variableInterpreter().eval( q, rho, u, v, t );
    pde.variableInterpreter().evalGradient( q, qx, rhox, ux, vx, tx );
    pde.variableInterpreter().evalGradient( q, qy, rhoy, uy, vy, ty );

    Tp rhoux = rhox*u + ux*rho;
    Tp rhovx = rhox*v + vx*rho;
    Tp rhouy = rhoy*u + uy*rho;
    Tp rhovy = rhoy*v + vy*rho;

    Tq ex, ey;
    pde.gasModel().energyGradient(rho, t, rhox, tx, ex);
    pde.gasModel().energyGradient(rho, t, rhoy, ty, ey);

    Tp Ex = ex + ux*u + vx*v;
    Tp Ey = ey + uy*u + vy*v;

    Tp rhoEx = rhox*E + rho*Ex;
    Tp rhoEy = rhoy*E + rho*Ey;

    // TODO: Implement Jacobian version of this (for QTypePrimitiveRhoPressure it is zero)
    pde.gasModel().pressureGradient( rho, t, rhox, tx, px );
    pde.gasModel().pressureGradient( rho, t, rhoy, ty, py );

    Tq p_rho, p_t, c_t;
    pde.gasModel().pressureJacobian(rho, t, p_rho, p_t);
    pde.gasModel().speedofSoundJacobian(t, c_t);

    Real Cv = pde.gasModel().Cv();
    Tq t_rho, t_rhou, t_rhov, t_rhoE;

    // Temperature from conservative variables
    //t = ( rhoE/rho - 0.5*( rhou*rhou + rhov*rhov )/(rho*rho) ) / Cv
    t_rho  = ( -E/rho + ( u*u + v*v )/rho ) / Cv;
    t_rhou = (        - (   u       )/rho ) / Cv;
    t_rhov = (        - (         v )/rho ) / Cv;
    t_rhoE = (  1/rho               ) / Cv;

//    Tq c_rho  = c_t*t_rho;
//    Tq c_rhou = c_t*t_rhou;
//    Tq c_rhov = c_t*t_rhov;
//    Tq c_rhoE = c_t*t_rhoE;

       p_rho += p_t*t_rho;
    Tq p_rhou = p_t*t_rhou;
    Tq p_rhov = p_t*t_rhov;
    Tq p_rhoE = p_t*t_rhoE;

    // velocity Jacobian wrt conservative variables
//    Tq u_rho  = -u/rho;
//    Tq u_rhou =  1/rho;
//
//    Tq v_rho  = -v/rho;
//    Tq v_rhov =  1/rho;

    DLA::MatrixSymS<D,Real> H = exp(logH); //generalized h-tensor

    DLA::VectorS<D, Tq> V = {u, v};
    DLA::VectorS<D, Tp> gradp = {px, py};

    DLA::VectorS<D, Tp> gradp_rhox  = {rhox  * p_rho,  0.0};
    DLA::VectorS<D, Tp> gradp_rhoux = {rhoux * p_rhou, 0.0};
    DLA::VectorS<D, Tp> gradp_rhovx = {rhovx * p_rhov, 0.0};
    DLA::VectorS<D, Tp> gradp_rhoEx = {rhoEx * p_rhoE, 0.0};

    DLA::VectorS<D, Tp> gradp_rhoy  = {0.0, rhoy  * p_rho};
    DLA::VectorS<D, Tp> gradp_rhouy = {0.0, rhouy * p_rhou};
    DLA::VectorS<D, Tp> gradp_rhovy = {0.0, rhovy * p_rhov};
    DLA::VectorS<D, Tp> gradp_rhoEy = {0.0, rhoEy * p_rhoE};

    // sensor from Ryan Glasby, but with a generalized H scale
    Tp Ugradp      = Transpose(V)*H*gradp;

    Tp Ugradp_rhox  = Transpose(V)*H*gradp_rhox;
    Tp Ugradp_rhoux = Transpose(V)*H*gradp_rhoux;
    Tp Ugradp_rhovx = Transpose(V)*H*gradp_rhovx;
    Tp Ugradp_rhoEx = Transpose(V)*H*gradp_rhoEx;

    Tp Ugradp_rhoy  = Transpose(V)*H*gradp_rhoy;
    Tp Ugradp_rhouy = Transpose(V)*H*gradp_rhouy;
    Tp Ugradp_rhovy = Transpose(V)*H*gradp_rhovy;
    Tp Ugradp_rhoEy = Transpose(V)*H*gradp_rhoEy;

    // Only positive values of the sensor indicate a shock
//    Ugradp = max(Tp(0), Ugradp);
    if (Ugradp < 0)
    {
      Ugradp_rhox = 0.0;
      Ugradp_rhoux = 0.0;
      Ugradp_rhovx = 0.0;
      Ugradp_rhoEx = 0.0;

      Ugradp_rhoy = 0.0;
      Ugradp_rhouy = 0.0;
      Ugradp_rhovy = 0.0;
      Ugradp_rhoEy = 0.0;
    }
    Ugradp = max(Tp(0), Ugradp);

//    Ts sensor = Ugradp/(Ugradp + kappa_*c*p);
    Ts s_rhox  = kappa_*c*p/pow(Ugradp + kappa_*c*p,2) * Ugradp_rhox;
    Ts s_rhoux = kappa_*c*p/pow(Ugradp + kappa_*c*p,2) * Ugradp_rhoux;
    Ts s_rhovx = kappa_*c*p/pow(Ugradp + kappa_*c*p,2) * Ugradp_rhovx;
    Ts s_rhoEx = kappa_*c*p/pow(Ugradp + kappa_*c*p,2) * Ugradp_rhoEx;

    Ts s_rhoy  = kappa_*c*p/pow(Ugradp + kappa_*c*p,2) * Ugradp_rhoy;
    Ts s_rhouy = kappa_*c*p/pow(Ugradp + kappa_*c*p,2) * Ugradp_rhouy;
    Ts s_rhovy = kappa_*c*p/pow(Ugradp + kappa_*c*p,2) * Ugradp_rhovy;
    Ts s_rhoEy = kappa_*c*p/pow(Ugradp + kappa_*c*p,2) * Ugradp_rhoEy;

    dsdux(iSens,PDE::iCont) -= s_rhox;
    dsdux(iSens,PDE::ixMom) -= s_rhoux;
    dsdux(iSens,PDE::iyMom) -= s_rhovx;
    dsdux(iSens,PDE::iEngy) -= s_rhoEx;

    dsduy(iSens,PDE::iCont) -= s_rhoy;
    dsduy(iSens,PDE::ixMom) -= s_rhouy;
    dsduy(iSens,PDE::iyMom) -= s_rhovy;
    dsduy(iSens,PDE::iEngy) -= s_rhoEy;
#endif
  }


  template <class PDE, class Tq, class Tg, class Ts>
  void jacobianGradientSource(
      const PDE& pde, const ParamTuple<DLA::MatrixSymS<D,Real>, Real, TupleClass<>>& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy ) const
  {
    jacobianGradientSource(pde, param.left(), x, y, time, q, qx, qy, dsdux, dsduy);
  }

  template <class PDE, class Tp, class Tq, class Tg, class Ts>
  void sourceTrace(
      const PDE& pde, const Tp& paramL, const Real& xL, const Real& yL,
      const Tp& paramR, const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      ArrayQ<Ts>& sourceL,
      ArrayQ<Ts>& sourceR ) const
  {
    // Nothing
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "AVSensor_Source2D_PressureGrad:" << std::endl;
    out << indent << "  PDEorder_ = " << PDEorder_ << std::endl;
    out << indent << "  C1_ = " << C1_ << std::endl;
    out << indent << "  kappa_ = " << kappa_ << std::endl;
  }

protected:
  Real PDEorder_;
  const Sensor& sensor_;
  const Real kappa_;
  const Real C1_;
  const Real C3_;
};



//----------------------------------------------------------------------------//
// Use relative solution `energy' in resolved and unresolved scales

template <template <class> class PDETraitsSize, class Sensor>
class AVSensor_Source2D_VMSD
{
public:
  template<class T>
  using ArrayQ = typename PDETraitsSize<PhysD2>::template ArrayQ<T>;

  template<class T>
  using MatrixQ = typename PDETraitsSize<PhysD2>::template MatrixQ<T>;

  static const int D = PhysD2::D;
  static const int iSens = PDETraitsSize<PhysD2>::iSens;

  typedef DLA::VectorS<D, Real> VectorX;

  explicit AVSensor_Source2D_VMSD(int PDEorder, const Sensor& sensor) :
    PDEorder_(Real(PDEorder)),
    sensor_(sensor)
  {
    // Nothing
  }

  bool hasSourceTerm() const { return true; }
  bool hasSourceTrace() const { return false; }
  bool hasSourceLiftedQuantity() const { return false; }
  bool needsSolutionGradientforSource() const { return true; }

  template<class PDE, class Tq, class Tg, class Tqp, class Tgp, class Ts>
  void source( const PDE& pde, const DLA::MatrixSymS<D,Real>& logH, const Real& x, const Real& y, const Real& time,
               const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
               const ArrayQ<Tgp>& qx, const ArrayQ<Tgp>& qy, const ArrayQ<Tg>& qpx, const ArrayQ<Tg>& qpy,
               ArrayQ<Ts>& source ) const
  {
    typedef typename promote_Surreal<Tq, Tqp>::type Tp;

    ArrayQ<Tp> qq = q + qp;

    Tq rhobar = 0, ubar = 0, vbar = 0, tbar = 0, pbar = 0;
    Tp rhoqq = 0, uqq = 0, vqq = 0, tqq = 0, pqq = 0;

    pde.variableInterpreter().eval( q, rhobar, ubar, vbar, tbar );
    pbar  = pde.gasModel().pressure(rhobar, tbar);
    pde.variableInterpreter().eval( qq, rhoqq, uqq, vqq, tqq );
    pqq  = pde.gasModel().pressure(rhoqq, tqq);

    Tp pratio = (pqq - pbar) / pqq;
    Tp sensor = 15.0*pratio*tanh(10.0*pratio);

    source(iSens) += (qq(iSens) - sensor);
  }

  template<class PDE, class Tq, class Tg, class Tqp, class Tgp, class Ts>
  void sourceCoarse( const PDE& pde, const DLA::MatrixSymS<D,Real>& logH, const Real& x, const Real& y, const Real& time,
                     const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
                     const ArrayQ<Tgp>& qx, const ArrayQ<Tgp>& qy, const ArrayQ<Tg>& qpx, const ArrayQ<Tg>& qpy,
                     ArrayQ<Ts>& src ) const
  {
    source(pde, logH, x, y, time, q, qp, qx, qy, qpx, qpy, src);
  }

  template<class PDE, class Tq, class Tg, class Tqp, class Tgp, class Ts>
  void sourceFine( const PDE& pde, const DLA::MatrixSymS<D,Real>& logH, const Real& x, const Real& y, const Real& time,
                   const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
                   const ArrayQ<Tgp>& qx, const ArrayQ<Tgp>& qy, const ArrayQ<Tg>& qpx, const ArrayQ<Tg>& qpy,
                   ArrayQ<Ts>& src ) const
  {
    source(pde, logH, x, y, time, q, qp, qx, qy, qpx, qpy, src);
  }

  template<class PDE, class Tp, class Tlq, class Tq, class Tg, class Tqp, class Tgp, class Ts>
  void source( const PDE& pde, const Tp& param, const Real& x, const Real& y, const Real& time,
               const Tlq& lifted_quantity,
               const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
               const ArrayQ<Tgp>& qx, const ArrayQ<Tgp>& qy, const ArrayQ<Tg>& qpx, const ArrayQ<Tg>& qpy,
               ArrayQ<Ts>& source ) const
  {
    SANS_DEVELOPER_EXCEPTION("AVSensor_Source2D_VMSD::source with lifted quantity - Not implemented yet.");
  }

  template<class PDE, class Tq, class Tg, class Ts>
  void source( const PDE& pde, const ParamTuple<DLA::MatrixSymS<D,Real>, Real, TupleClass<>>& param,
               const Real& x, const Real& y, const Real& time,
               const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
               ArrayQ<Ts>& src ) const
  {
    source(pde, param.left(), x, y, time, q, qx, qy, src);
  }

  template<class Tp, class Tlq, class Tq, class Tg, class Ts>
  void source( const Tp& param, const Real& x, const Real& y, const Real& time,
               const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
               ArrayQ<Ts>& source ) const
  {
    SANS_DEVELOPER_EXCEPTION("AVSensor_Source2D_VMSD::source with lifted quantity - Not implemented yet.");
  }

  template<class Tp, class Tq, class Ts>
  void sourceLiftedQuantity(
      const Tp& paramL, const Real& xL, const Real& yL,
      const Tp& paramR, const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      Ts& s ) const
  {
    SANS_DEVELOPER_EXCEPTION("AVSensor_Source2D_VMSD::sourceLiftedQuantity - Not implemented yet.");
  }

  template <class PDE, class Tq, class Tg, class Ts>
  void jacobianSource(
      const PDE& pde, const DLA::MatrixSymS<D,Real>& logH,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const
  {
//    SANS_DEVELOPER_EXCEPTION("AVSensor_Source2D_VMSD::jacobianSource - Not implemented yet.");
  }

  template<class PDE, class Tp, class Tlq, class Tq, class Tg, class Ts>
  void source( const PDE& pde, const Tp& logH,
               const Real& x, const Real& y, const Real& time,
               const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
               ArrayQ<Ts>& source ) const
  {
    SANS_DEVELOPER_EXCEPTION("AVSensor_Source2D_VMSD::source with lifted quantity - Not implemented yet.");
  }

  template <class PDE, class Tq, class Tg, class Ts>
  void jacobianSource(
      const PDE& pde, const ParamTuple<DLA::MatrixSymS<D,Real>, Real, TupleClass<>>& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const
  {
    jacobianSource(pde, param.left(), x, y, time, q, qx, qy, dsdu);
  }

  template <class PDE, class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue(
      const PDE& pde, const ParamTuple<DLA::MatrixSymS<D,Real>, Real, TupleClass<>>& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const
  {
    //since 1,1 entry is positive, jacobianSource is pos def.
    jacobianSource(pde, param.left(), x, y, time, q, qx, qy, dsdu);
  }

  template <class PDE, class Tq, class Tg, class Ts>
  void jacobianGradientSource(
      const PDE& pde, const DLA::MatrixSymS<D,Real>& logH,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy ) const
  {
    SANS_DEVELOPER_EXCEPTION("AVSensor_Source2D_VMSD::jacobianGradientSource - Not implemented yet.");
  }


  template <class PDE, class Tq, class Tg, class Ts>
  void jacobianGradientSource(
      const PDE& pde, const ParamTuple<DLA::MatrixSymS<D,Real>, Real, TupleClass<>>& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy ) const
  {
    jacobianGradientSource(pde, param.left(), x, y, time, q, qx, qy, dsdux, dsduy);
  }

  template <class PDE, class Tp, class Tq, class Tg, class Ts>
  void sourceTrace(
      const PDE& pde, const Tp& paramL, const Real& xL, const Real& yL,
      const Tp& paramR, const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      ArrayQ<Ts>& sourceL,
      ArrayQ<Ts>& sourceR ) const
  {
    // Nothing
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "AVSensor_Source2D_VMSD:" << std::endl;
    out << indent << "  PDEorder_ = " << PDEorder_ << std::endl;
  }

protected:
  Real PDEorder_;
  const Sensor& sensor_;
};


//----------------------------------------------------------------------------//
// Use the entropy-inequality violation

template <template <class> class PDETraitsSize, class Sensor>
class AVSensor_Source2D_VMSD_EntropyInequality
{
public:
  template<class T>
  using ArrayQ = typename PDETraitsSize<PhysD2>::template ArrayQ<T>;

  template<class T>
  using MatrixQ = typename PDETraitsSize<PhysD2>::template MatrixQ<T>;

  static const int D = PhysD2::D;
  static const int iSens = PDETraitsSize<PhysD2>::iSens;

  typedef DLA::VectorS<D, Real> VectorX;

  explicit AVSensor_Source2D_VMSD_EntropyInequality(int PDEorder, const Sensor& sensor) :
    PDEorder_(Real(PDEorder)),
    sensor_(sensor)
  {
    // Nothing
  }

  bool hasSourceTerm() const { return true; }
  bool hasSourceTrace() const { return false; }
  bool hasSourceLiftedQuantity() const { return false; }
  bool needsSolutionGradientforSource() const { return true; }

  template<class PDE, class Tq, class Tg, class Tqp, class Tgp, class Ts>
  void source( const PDE& pde, const DLA::MatrixSymS<D,Real>& param,
               const Real& x, const Real& y, const Real& time,
               const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
               const ArrayQ<Tgp>& qx, const ArrayQ<Tgp>& qy,
               const ArrayQ<Tg>& qpx, const ArrayQ<Tg>& qpy,
               ArrayQ<Ts>& src ) const
  {
    typedef typename promote_Surreal<Tq, Tqp>::type Tqq;
    typedef typename promote_Surreal<Tg, Tgp>::type Tgg;
    typedef typename promote_Surreal<Tqq, Tgg>::type Tp;

    ArrayQ<Tp> qq = q + qp;
    ArrayQ<Tp> qqx = qx + qpx;
    ArrayQ<Tp> qqy = qy + qpy;

    ArrayQ<Tp> V = 0;
    pde.adjointState(x, y, time, qq, V);
    V(iSens) = 0.0;

    ArrayQ<Tp> F = 0;
    pde.strongFluxAdvective(param, x, y, time, qq, qqx, qqy, F );
    F(iSens) = 0.0;

    Tp vF = dot(V, F);

    MatrixQ<Tp> dudv = 0;
    pde.adjointVariableJacobian(x, y, time, qq, dudv);

    DLA::MatrixSymS<D,Real> H = exp(param); //generalized h-tensor

    typedef DLA::VectorS<D, Real> VectorX;
    VectorX hPrincipal;
    DLA::EigenValues(H, hPrincipal);

    //Get minimum length scale
    Real hmin = std::numeric_limits<Real>::max();
    for (int d = 0; d < D; d++)
      hmin = min(hmin, hPrincipal[d]);

    Tp ctilde = 0;
    pde.speedCharacteristic( param, x, y, time, qq, ctilde );

    Tp denom = 2*ctilde * dot(V, dudv*V);
    // This is a C^{2} continuous approximation to max{0, vF}
    Tp numer = hmin * vF * tanh(15*vF) * tanh(15*vF);
    numer = max(numer, Tp(0));
    Tp sensor = 20.0 * numer / denom;

    src(iSens) += (qq(iSens) - sensor);
  }

  template<class PDE, class Tq, class Tg, class Tqp, class Tgp, class Ts>
  void sourceCoarse( const PDE& pde, const DLA::MatrixSymS<D,Real>& param,
                     const Real& x, const Real& y, const Real& time,
                     const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
                     const ArrayQ<Tgp>& qx, const ArrayQ<Tgp>& qy,
                     const ArrayQ<Tg>& qpx, const ArrayQ<Tg>& qpy,
                     ArrayQ<Ts>& src ) const
  {
    source(pde, param, x, y, time, q, qp, qx, qy, qpx, qpy, src);
  }

  template<class PDE, class Tq, class Tg, class Tqp, class Tgp, class Ts>
  void sourceFine( const PDE& pde, const DLA::MatrixSymS<D,Real>& param,
               const Real& x, const Real& y, const Real& time,
               const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
               const ArrayQ<Tgp>& qx, const ArrayQ<Tgp>& qy,
               const ArrayQ<Tg>& qpx, const ArrayQ<Tg>& qpy,
               ArrayQ<Ts>& src ) const
  {
    source(pde, param, x, y, time, q, qp, qx, qy, qpx, qpy, src);
  }

  template<class PDE, class Tp, class Tlq, class Tq, class Tg, class Tqp, class Tgp, class Ts>
  void source( const PDE& pde, const Tp& param, const Real& x, const Real& y, const Real& time,
               const Tlq& lifted_quantity,
               const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
               const ArrayQ<Tgp>& qx, const ArrayQ<Tgp>& qy, const ArrayQ<Tg>& qpx, const ArrayQ<Tg>& qpy,
               ArrayQ<Ts>& source ) const
  {
    SANS_DEVELOPER_EXCEPTION("AVSensor_Source2D_VMSD_EntropyInequality::source with lifted quantity - Not implemented yet.");
  }

  template<class PDE, class Tq, class Tg, class Ts>
  void source( const PDE& pde, const ParamTuple<DLA::MatrixSymS<D,Real>, Real, TupleClass<>>& param,
               const Real& x, const Real& y, const Real& time,
               const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
               ArrayQ<Ts>& src ) const
  {
    source(pde, param.left(), x, y, time, q, qx, qy, src);
  }

  template<class Tp, class Tlq, class Tq, class Tg, class Ts>
  void source( const Tp& param, const Real& x, const Real& y, const Real& time,
               const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
               ArrayQ<Ts>& source ) const
  {
    SANS_DEVELOPER_EXCEPTION("AVSensor_Source2D_VMSD_EntropyInequality::source with lifted quantity - Not implemented yet.");
  }

  template<class Tp, class Tq, class Ts>
  void sourceLiftedQuantity(
      const Tp& paramL, const Real& xL, const Real& yL,
      const Tp& paramR, const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      Ts& s ) const
  {
    SANS_DEVELOPER_EXCEPTION("AVSensor_Source2D_VMSD_EntropyInequality::sourceLiftedQuantity - Not implemented yet.");
  }

  template <class PDE, class Tq, class Tg, class Ts>
  void jacobianSource(
      const PDE& pde, const DLA::MatrixSymS<D,Real>& logH,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const
  {
//    SANS_DEVELOPER_EXCEPTION("AVSensor_Source2D_VMSD::jacobianSource - Not implemented yet.");
  }

  template<class PDE, class Tp, class Tlq, class Tq, class Tg, class Ts>
  void source( const PDE& pde, const Tp& logH,
               const Real& x, const Real& y, const Real& time,
               const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
               ArrayQ<Ts>& source ) const
  {
    SANS_DEVELOPER_EXCEPTION("AVSensor_Source2D_VMSD_EntropyInequality::source with lifted quantity - Not implemented yet.");
  }

  template <class PDE, class Tq, class Tg, class Ts>
  void jacobianSource(
      const PDE& pde, const ParamTuple<DLA::MatrixSymS<D,Real>, Real, TupleClass<>>& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const
  {
    jacobianSource(pde, param.left(), x, y, time, q, qx, qy, dsdu);
  }

  template <class PDE, class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue(
      const PDE& pde, const ParamTuple<DLA::MatrixSymS<D,Real>, Real, TupleClass<>>& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const
  {
    //since 1,1 entry is positive, jacobianSource is pos def.
    jacobianSource(pde, param.left(), x, y, time, q, qx, qy, dsdu);
  }

  template <class PDE, class Tq, class Tg, class Ts>
  void jacobianGradientSource(
      const PDE& pde, const DLA::MatrixSymS<D,Real>& logH,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy ) const
  {
    SANS_DEVELOPER_EXCEPTION("AVSensor_Source2D_VMSD_EntropyInequality::jacobianGradientSource - Not implemented yet.");
  }


  template <class PDE, class Tq, class Tg, class Ts>
  void jacobianGradientSource(
      const PDE& pde, const ParamTuple<DLA::MatrixSymS<D,Real>, Real, TupleClass<>>& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy ) const
  {
    jacobianGradientSource(pde, param.left(), x, y, time, q, qx, qy, dsdux, dsduy);
  }

  template <class PDE, class Tp, class Tq, class Tg, class Ts>
  void sourceTrace(
      const PDE& pde, const Tp& paramL, const Real& xL, const Real& yL,
      const Tp& paramR, const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      ArrayQ<Ts>& sourceL,
      ArrayQ<Ts>& sourceR ) const
  {
    // Nothing
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "AVSensor_Source2D_VMSD_EntropyInequality:" << std::endl;
    out << indent << "  PDEorder_ = " << PDEorder_ << std::endl;
  }

protected:
  Real PDEorder_;
  const Sensor& sensor_;
};

} //namespace SANS

#endif  // AVSENSOR_SOURCE2D_H
