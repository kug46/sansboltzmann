// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef TRAITSARTIFICIALVISCOSITY_H
#define TRAITSARTIFICIALVISCOSITY_H

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"

namespace SANS
{

template <template <class> class BaseTraitsSize, class PhysDim_>
class TraitsSizeArtificialViscosity
{
public:
  typedef PhysDim_ PhysDim;
  static const int D = PhysDim::D;                      // physical dimensions
  static const int N = BaseTraitsSize<PhysDim_>::N + 1; // solution variables (+1 for sensor variable)

  static const int iSens = N-1;

  template <class T>
  using ArrayQ = DLA::VectorS<N,T>;           // solution/residual arrays

  template <class T>
  using MatrixQ = DLA::MatrixS<N,N,T>;        // matrices
};

template <class PDEBase_, class AdvectiveFlux_, class ViscousFlux_, class Source_>
class TraitsModelArtificialViscosity
{
public:

  typedef PDEBase_ PDEBase;              // base PDE type
  typedef AdvectiveFlux_ AdvectiveFlux;  // sensor PDE advective flux type
  typedef ViscousFlux_ ViscousFlux;      // sensor PDE viscous flux type
  typedef Source_ Source;                // sensor PDE source term type
};

}

#endif // TRAITSARTIFICIALVISCOSITY_H
