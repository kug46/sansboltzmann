// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(AVSENSOR1D_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "AVSensor_AdvectiveFlux1D.h"

#include <string>
#include <iostream>

namespace SANS
{

//----------------------------------------------------------------------------//
// advective flux: uniform flow
template <template <class> class PDETraitsSize>
void
AVSensor_AdvectiveFlux1D_Uniform<PDETraitsSize>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "AVSensor_AdvectiveFlux1D_Uniform:"
            << "  u_ = " << u_ << std::endl;
}

} //namespace SANS
