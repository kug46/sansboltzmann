// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(AVSENSOR1D_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "AVSensor_ViscousFlux1D.h"

#include <string>
#include <iostream>


namespace SANS
{

//----------------------------------------------------------------------------//
// diffusion matrix: uniform
template <template <class> class PDETraitsSize>
void
AVSensor_DiffusionMatrix1D_Uniform<PDETraitsSize>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "AVSensor_DiffusionMatrix1D_Uniform:"
            << "  kxx_ = " << kxx_ << std::endl;
}

//----------------------------------------------------------------------------//
// diffusion matrix: Grid Dependent
template <template <class> class PDETraitsSize>
void
AVSensor_DiffusionMatrix1D_GridDependent<PDETraitsSize>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "AVSensor_DiffusionMatrix1D_GridDependent:"
            << "  C_ = " << C_ << std::endl;
}

//----------------------------------------------------------------------------//
// diffusion matrix: with generalized h-scale
template <template <class> class PDETraitsSize>
void
AVSensor_DiffusionMatrix1D_GenHScale<PDETraitsSize>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "AVSensor_DiffusionMatrix1D_GenHScale:"
      << "  C1_ = " << C1_ << std::endl
      << "  C2_ = " << C2_ << std::endl;
}

} //namespace SANS
