// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SENSOR_ADVECTIVEFLUX2D_H
#define SENSOR_ADVECTIVEFLUX2D_H

// 1-D Advection-Diffusion PDE: advective flux

#include "tools/SANSnumerics.h"     // Real
#include "tools/smoothmath.h"

#include "Topology/Dimension.h"

#include "Field/Tuple/ParamTuple.h"
#include "tools/Tuple.h"

#include <cmath> // fabs
#include <iostream>

namespace SANS
{

//----------------------------------------------------------------------------//
// advection velocity: uniform
//
// member functions:
//   .flux         advective flux: F(Q)
//   .jacobian     advective flux jacboian: d(F)/d(U)
//   .strongFlux   advective flux : div.F
//----------------------------------------------------------------------------//

template <template <class> class PDETraitsSize>
class AVSensor_AdvectiveFlux2D_Uniform
{
public:
  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysD2>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysD2>::template MatrixQ<T>;  // matrices

  bool hasFluxAdvective() const { return true; }

  AVSensor_AdvectiveFlux2D_Uniform( const Real u, const Real v ) : u_(u), v_(v) {}
  ~AVSensor_AdvectiveFlux2D_Uniform() {}

  static const int iSens = PDETraitsSize<PhysD2>::iSens;

  // advective flux: F(Q)
  template <class T, class Tp, class Tf>
  void flux( const Tp& param, const Real& x, const Real& y, const Real time,
             const ArrayQ<T>& q, ArrayQ<Tf>& fx, ArrayQ<Tf>& fy ) const
  {
    fx(iSens) += u_*q(iSens);
    fy(iSens) += v_*q(iSens);
  }

  // advective flux jacobian: d(F)/d(U)
  template <class Tp, class Tq, class Ta>
  void jacobian( const Tp& param, const Real& x, const Real& y, const Real time,
                 const ArrayQ<Tq>& q, MatrixQ<Ta>& ax, MatrixQ<Ta>& ay ) const
  {
    ax(iSens,iSens) += u_;
    ay(iSens,iSens) += v_;
  }

  template <class T, class Tp, class Tf>
  void fluxUpwind( const Tp& param, const Real& x, const Real& y, const Real time,
                   const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx, const Real& ny,
                   ArrayQ<Tf>& f, const Real& scaleFactor = 1 ) const
  {
    f(iSens) += scaleFactor * (0.5*(nx*u_ + ny*v_)*(qR(iSens) + qL(iSens)) - 0.5*fabs(nx*u_ + ny*v_)*(qR(iSens) - qL(iSens)));
  }

  template <class T, class Tp, class Tf>
  void fluxUpwindSpaceTime( const Tp& param, const Real& x, const Real& y, const Real time,
                            const ArrayQ<T>& qL, const ArrayQ<T>& qR,
                            const Real& nx, const Real& ny, const Real& nt, const bool& isSensorSteady,
                            ArrayQ<Tf>& f ) const
  {
    Real w = 1.0; //unit speed in temporal direction
    if (isSensorSteady) w = 0.0;

    f(iSens) += 0.5*(nx*u_ + ny*v_ + nt*w )*(qR(iSens) + qL(iSens)) - 0.5*fabs(nx*u_ + ny*v_ + nt*w)*(qR(iSens) - qL(iSens));
  }

  // strong advective flux: div.F
  template <class Tp, class Tq, class Tg, class Tf>
  void strongFlux( const Tp& param, const Real& x, const Real& y, const Real time,
                   const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
                   ArrayQ<Tf>& strongPDE ) const
  {
    strongPDE(iSens) += u_*qx(iSens) + v_*qy(iSens);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  const Real u_, v_;      // uniform advection velocity
};

} //namespace SANS

#endif  // SENSOR_ADVECTIVEFLUX2D_H
