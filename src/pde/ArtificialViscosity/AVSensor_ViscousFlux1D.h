// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef AVSENSOR_DIFFUSIONMATRIX1D_H
#define AVSENSOR_DIFFUSIONMATRIX1D_H

// 1-D Advection-Diffusion PDE: diffusion matrix

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"

#include "Field/Tuple/ParamTuple.h"
#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Exp.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"

#include <iostream>

namespace SANS
{

//----------------------------------------------------------------------------//
template<class DiffusionMatrix>
class AVSensor_ViscousFlux1D_LinearInGradient : public DiffusionMatrix
{
public:
  using DiffusionMatrix::diffusionViscous;
  using DiffusionMatrix::diffusionViscousSpaceTime;

  template <class T>
  using ArrayQ = typename DiffusionMatrix::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = typename DiffusionMatrix::template MatrixQ<T>;  // matrices

  template< class... Args > // cppcheck-suppress noExplicitConstructor
  AVSensor_ViscousFlux1D_LinearInGradient(Args&&... args) : DiffusionMatrix(std::forward<Args>(args)...) {}

  bool fluxViscousLinearInGradient() const { return true; }

  // viscous flux: Fv(Q, QX)
  template <class PDE, class Tp, class Tq, class Tg, class Tf>
  void fluxViscous(
      const PDE& pde, const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Tf>& f ) const
  {
    MatrixQ<Tq> kxx = 0.0;

    diffusionViscous(pde, param, x, time, q, qx, kxx );

    f -= kxx*qx;
  }

  // space-time viscous flux: Fv(Q, QX)
  template <class PDE, class Tp, class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const PDE& pde, const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& ft ) const
  {
    MatrixQ<Tq> kxx = 0.0, kxt = 0.0;
    MatrixQ<Tq> ktx = 0.0, ktt = 0.0;

    diffusionViscousSpaceTime(pde, param, x, time, q, qx, qt, kxx, kxt, ktx, ktt );

    fx -= kxx*qx + kxt*qt;
    ft -= ktx*qx + ktt*qt;
  }

  // viscous flux: normal flux with left and right states
  template <class PDE, class Tp, class Tq, class Tg, class Tf>
  void fluxViscous(
      const PDE& pde, const Tp& paramL, const Tp& paramR,
      const Real& x, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
      const Real& nx, ArrayQ<Tf>& fn ) const
  {
    ArrayQ<Tf> fL = 0, fR = 0;

    fluxViscous(pde, paramL, x, time, qL, qxL, fL);
    fluxViscous(pde, paramR, x, time, qR, qxR, fR);

    fn += 0.5*(fL + fR)*nx;
  }

  // space-time viscous flux: normal flux with left and right states
  template <class PDE, class Tp, class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const PDE& pde, const Tp& paramL, const Tp& paramR,
      const Real& x, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qtL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qtR,
      const Real& nx, const Real& nt, ArrayQ<Tf>& fn ) const
  {
    ArrayQ<Tf> fxL = 0, fxR = 0;
    ArrayQ<Tf> ftL = 0, ftR = 0;

    fluxViscousSpaceTime(pde, paramL, x, time, qL, qxL, qtL, fxL, ftL);
    fluxViscousSpaceTime(pde, paramR, x, time, qR, qxR, qtR, fxR, ftR);

    fn += 0.5*(fxL + fxR)*nx + 0.5*(ftL + ftR)*nt;
  }
};

//----------------------------------------------------------------------------//
// diffusion matrix: uniform
//
// member functions:
//   .()      functor returning diffusion coefficient matrix
//----------------------------------------------------------------------------//

template <template <class> class PDETraitsSize>
class AVSensor_DiffusionMatrix1D_Uniform
{
public:
  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysD1>::template MatrixQ<T>;  // matrices

  static const int D = 1;                     // physical dimensions

  bool hasFluxViscous() const { return true; }

  static const int iSens = PDETraitsSize<PhysD1>::iSens;

  explicit AVSensor_DiffusionMatrix1D_Uniform( const Real kxx ) :
      kxx_(kxx) {}

  template <class PDE, class Tp, class Tq, class Tg, class Tk>
  void diffusionViscous( const PDE& pde, const Tp& param,
                         const Real x, const Real time,
                         const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
                         MatrixQ<Tk>& kxx ) const
  {
    kxx(iSens,iSens) += kxx_;
  }

  template <class PDE, class Tp, class Tq, class Tg, class Tk>
  void diffusionViscousSpaceTime( const PDE& pde, const Tp& param,
                                  const Real x, const Real time,
                                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
                                  MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxt, MatrixQ<Tk>& ktx, MatrixQ<Tk>& ktt ) const
  {
    diffusionViscous(pde, param, x, time, q, qx, kxx);
  }

  // strong form of viscous flux: d(Fv)/d(X)
  template <class PDE, class Tp, class Tq, class Tg, class Tf>
  inline void
  strongFluxViscous( const PDE& pde, const Tp& param,
                     const Real& x, const Real& time,
                     const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
                     const ArrayQ<Tg>& qxx,
                     ArrayQ<Tf>& strongPDE ) const
  {
    strongPDE(iSens) -= kxx_*qxx(iSens);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  const Real kxx_;          // constant diffusion matrix
};

template <template <class> class PDETraitsSize>
using AVSensor_ViscousFlux1D_Uniform = AVSensor_ViscousFlux1D_LinearInGradient<AVSensor_DiffusionMatrix1D_Uniform<PDETraitsSize>>;

//----------------------------------------------------------------------------//
// diffusion matrix: Grid Dependent
//
// member functions:
//   .()      functor returning diffusion coefficient matrix
//----------------------------------------------------------------------------//

template <template <class> class PDETraitsSize>
class AVSensor_DiffusionMatrix1D_GridDependent
{
public:
  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysD1>::template MatrixQ<T>;  // matrices

  static const int D = 1;                     // physical dimensions

  bool hasFluxViscous() const { return true; }

  static const int iSens = PDETraitsSize<PhysD1>::iSens;

  explicit AVSensor_DiffusionMatrix1D_GridDependent( const Real& C ) :
      C_(C) {}

  template <class PDE, class Tp, class Tq, class Tg, class Tk>
  void diffusionViscous( const PDE& pde, const Tp& param,
                         const Real x, const Real time,
                         const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
                         MatrixQ<Tk>& kxx ) const
  {
    const Tp& h = param;
    kxx(iSens,iSens) += C_*h*h;
  }

  template <class PDE, class Tp, class Tq, class Tg, class Tk>
  void diffusionViscousSpaceTime( const PDE& pde, const Tp& param,
                                  const Real x, const Real time,
                                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
                                  MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxt, MatrixQ<Tk>& ktx, MatrixQ<Tk>& ktt ) const
  {
    diffusionViscous(pde, param, x, time, q, qx, kxx);
  }

  // strong form of viscous flux: d(Fv)/d(X)
  template <class PDE, class Tp, class Tq, class Tg, class Tf>
  inline void
  strongFluxViscous( const PDE& pde, const Tp& param,
                     const Real& x, const Real& time,
                     const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
                     const ArrayQ<Tg>& qxx,
                     ArrayQ<Tf>& strongPDE ) const
  {
    const Tp& h = param;
    strongPDE(iSens) -= C_*h*h*qxx(iSens);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  const Real C_; // constant scaling
};

template <template <class> class PDETraitsSize>
using AVSensor_ViscousFlux1D_GridDependent = AVSensor_ViscousFlux1D_LinearInGradient<AVSensor_DiffusionMatrix1D_GridDependent<PDETraitsSize>>;

//----------------------------------------------------------------------------//
// Diffusion matrix with generalized h-scale
// Formulation from Masa Yano's PhD thesis - page 191
//----------------------------------------------------------------------------//
template <template <class> class PDETraitsSize>
class AVSensor_DiffusionMatrix1D_GenHScale
{
public:
  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysD1>::template MatrixQ<T>;  // matrices

  static const int D = 1;                     // physical dimensions

  typedef DLA::VectorS<D, Real> VectorX;
  typedef DLA::VectorS<D+1, Real> VectorXT;

  static const int iSens = PDETraitsSize<PhysD1>::iSens;

  bool hasFluxViscous() const { return true; }

  explicit AVSensor_DiffusionMatrix1D_GenHScale( const Real order, const bool hasSpaceTimeDiffusion = false,
                                                 const Real C1 = 3, const Real C2 = 5) :
    order_(order),
    hasSpaceTimeDiffusion_(hasSpaceTimeDiffusion),
    C1_(C1),
    C2_(C2)
  {
    // Nothing
  }

  template <class PDE, class Tp, class Tq, class Tg, class Tk>
  void diffusionViscous( const PDE& pde, const Tp& param,
                         const Real x, const Real time,
                         const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
                         MatrixQ<Tk>& kxx) const
  {
    SANS_ASSERT(!hasSpaceTimeDiffusion_); //space-time diffusion requires a space-time NDPDE

    //Tp may be of type DLA::MatrixSymS<D,Real> or DLA::MatrixSymS<D+1,Real>
    Tp H = exp(param); //generalized h-tensor
    Tp Hsq = H*H;

    kxx(iSens,iSens) += C2_*Hsq(0,0);
  }

  template <class PDE, class Tq, class Tg, class Tk>
  void diffusionViscousSpaceTime( const PDE& pde, const DLA::MatrixSymS<D+1,Real>& param,
                                  const Real x, const Real time,
                                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
                                  MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxt,
                                  MatrixQ<Tk>& ktx, MatrixQ<Tk>& ktt) const
  {
    DLA::MatrixSymS<D+1,Real> H = exp(param); //generalized h-tensor
    DLA::MatrixSymS<D+1,Real> Hsq = H*H;

    kxx(iSens,iSens) += C2_*Hsq(0,0);

    if (hasSpaceTimeDiffusion_)
    {
      kxt(iSens,iSens) += C2_*Hsq(0,1);

      ktx(iSens,iSens) += C2_*Hsq(1,0);
      ktt(iSens,iSens) += C2_*Hsq(1,1);
    }

  }

  // strong form of viscous flux: d(Fv)/d(X)
  template <class PDE, class Tq, class Tg, class Tf>
  inline void
  strongFluxViscous( const PDE& pde, const DLA::MatrixSymS<D,Real>& param,
                     const Real& x, const Real& time,
                     const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
                     const ArrayQ<Tg>& qxx,
                     ArrayQ<Tf>& strongPDE ) const
  {
    SANS_ASSERT(!hasSpaceTimeDiffusion_); //space-time diffusion requires a space-time NDPDE

    DLA::MatrixSymS<D,Real> H = exp(param); //generalized h-tensor
    DLA::MatrixSymS<D,Real> Hsq = H*H;

    strongPDE(iSens) -= C2_*( Hsq(0,0)*qxx(iSens) );
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  const Real order_; // solution order
  const bool hasSpaceTimeDiffusion_;
  const Real C1_; // time scaling
  const Real C2_; // diffusion scaling
};

template <template <class> class PDETraitsSize>
using AVSensor_ViscousFlux1D_GenHScale = AVSensor_ViscousFlux1D_LinearInGradient<AVSensor_DiffusionMatrix1D_GenHScale<PDETraitsSize>>;

} //namespace SANS

#endif  // AVSENSOR_DIFFUSIONMATRIX1D_H
