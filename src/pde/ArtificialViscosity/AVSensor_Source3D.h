// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef AVSENSOR_SOURCE3D_H
#define AVSENSOR_SOURCE3D_H

// 3-D source term term the sensor parameter

#include "tools/SANSnumerics.h"     // Real
#include "Surreal/PromoteSurreal.h"

#include "Topology/Dimension.h"

#include "Field/Tuple/ParamTuple.h"
#include "tools/Tuple.h"
#include "tools/smoothmath.h"

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Exp.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/Eigen.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include <iostream>
#include <limits>
#include <cmath> // fabs

#include "pyrite_fstream.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Source: Uniform
//
// source = a*q where 'a' is a constant
//----------------------------------------------------------------------------//

template <template <class> class PDETraitsSize>
class AVSensor_Source3D_Uniform
{
public:
  template<class T>
  using ArrayQ = typename PDETraitsSize<PhysD3>::template ArrayQ<T>;

  template<class T>
  using MatrixQ = typename PDETraitsSize<PhysD3>::template MatrixQ<T>;

  static const int iSens = PDETraitsSize<PhysD3>::iSens;

  explicit AVSensor_Source3D_Uniform( const Real a) : a_(a) {}

  bool hasSourceTerm() const { return true; }
  bool hasSourceTrace() const { return false; }
  bool hasSourceLiftedQuantity() const { return false; }
  bool needsSolutionGradientforSource() const { return false; }

  template<class PDE, class Tq, class Tg, class Tp, class Ts>
  void source( const PDE& pde, const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
               const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
               ArrayQ<Ts>& source ) const
  {
    source(iSens) += a_*q(iSens);
  }

  template <class PDE, class Tp, class Tq, class Tg, class Ts>
  void sourceTrace(
      const PDE& pde, const Tp& paramL, const Real& xL, const Real& yL, const Real& zL,
      const Tp& paramR, const Real& xR, const Real& yR, const Real& zR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qzL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qzR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
  {
    sourceL(iSens) += a_*qL(iSens);
    sourceR(iSens) += a_*qR(iSens);
  }

  template<class PDE, class Tp, class Tlq, class Tq, class Tg, class Ts>
  void source( const PDE& pde, const Tp& logH, const Real& x, const Real& y, const Real& z, const Real& time,
               const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
               ArrayQ<Ts>& source ) const
  {
    // Nothing here
  }

  template<class Tp, class Tq, class Ts>
  void sourceLiftedQuantity(
      const Tp& paramL, const Real& xL, const Real& yL, const Real& zL,
      const Tp& paramR, const Real& xR, const Real& yR, const Real& zR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      Ts& s ) const
  {
    // Nothing here
  }

  template <class PDE, class Tp, class Tq, class Tg, class Ts>
  void jacobianSource(
      const PDE& pde, const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdu ) const
  {
    dsdu(iSens,iSens) += a_;
  }

  template <class PDE, class Tp, class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue(
      const PDE& pde, const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdu ) const
  {
    dsdu(iSens,iSens) += fabs(a_);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

protected:
  const Real a_;
};


template <template <class> class PDETraitsSize, class Sensor>
class AVSensor_Source3D_Jump
{
public:
  template<class T>
  using ArrayQ = typename PDETraitsSize<PhysD3>::template ArrayQ<T>;

  template<class T>
  using MatrixQ = typename PDETraitsSize<PhysD3>::template MatrixQ<T>;

  static const int D = PhysD3::D;
  static const int iSens = PDETraitsSize<PhysD3>::iSens;

  typedef DLA::VectorS<D, Real> VectorX;

  explicit AVSensor_Source3D_Jump(int PDEorder,
                                  const Sensor& sensor,
                                  const Real C1 = 3.0,
                                  const Real C3 = 1.0) :
    PDEorder_(PDEorder),
    sensor_(sensor),
    C1_(C1),
    C3_(C3)
  {
    // Nothing
  }

  bool hasSourceTerm() const { return true; }
  bool hasSourceTrace() const { return true; }
  bool hasSourceLiftedQuantity() const { return true; }
  bool needsSolutionGradientforSource() const { return false; }

  template<class PDE, class Tp, class Tq, class Tg, class Ts>
  void source( const PDE& pde, const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
               const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
               ArrayQ<Ts>& source ) const
  {
    SANS_DEVELOPER_EXCEPTION("AVSensor_Source3D_Jump::source - This source term requires a lifted quantity! Should not get here.");
  }

  template<class PDE, class Tp, class Tlq, class Tq, class Tg, class Ts>
  void source( const PDE& pde, const ParamTuple<DLA::MatrixSymS<D,Real>, Tp, TupleClass<>>& param,
               const Real& x, const Real& y, const Real& z, const Real& time,
               const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
               ArrayQ<Ts>& source ) const
  {
    this->source(pde, param.left(), x, y, z, time, lifted_quantity, q, qx, qy, qz, source);
  }

  template<class PDE, class Tlq, class Tq, class Tg, class Ts>
  void source( const PDE& pde, const DLA::MatrixSymS<D,Real>& logH, const Real& x, const Real& y, const Real& z, const Real& time,
               const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
               ArrayQ<Ts>& source ) const
  {
    Tlq abs_lifted_quantity = fabs(lifted_quantity); //lifted_quantity may go negative if lifted quantity field is P1 or higher

    Tlq Sk = log10(abs_lifted_quantity + 1e-16);
    Real psi0 = 1;
    if ( PDEorder_ > 0 )
      psi0 = 3.0 + 2.0*log10(PDEorder_);

    Tlq xi = Sk + psi0;

    xi += 0.5; // Add +0.5 to center around smooth activiation
    xi /= 2;   // Scale so it activates over 2 orders of magnitude
    Real alpha = 10;
    Tlq sensor = smoothActivation_exp(xi, alpha);
    source(iSens) += (q(iSens) - C3_*sensor);
  }

  template<class Tp, class Tq, class Ts>
  void sourceLiftedQuantity(
      const Tp& paramL, const Real& xL, const Real& yL, const Real& zL,
      const Tp& paramR, const Real& xR, const Real& yR, const Real& zR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      Ts& s ) const
  {
    Tq gL = 0, gR = 0;
    sensor_.jumpQuantity( qL, gL );
    sensor_.jumpQuantity( qR, gR );

    Tq dg = gL - gR;
    dg = smoothabs0(dg, 1.0e-5);

    Tq gbar = 0.5*(fabs(gL) + fabs(gR));

    // Check if dg is small relative to gbar
    static const Real delta = 1e-3;
    if ( delta*gbar + 1.0 == 1.0 )
      return; //gbar is nearly zero, just return

    Tq jump = dg / gbar;
    s = smoothabs0(jump, 0.03);
  }

  template <class PDE, class Tp, class Tq, class Tg, class Ts>
  void sourceTrace(
      const PDE& pde, const Tp& paramL, const Real& xL, const Real& yL, const Real& zL,
      const Tp& paramR, const Real& xR, const Real& yR, const Real& zR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qzL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qzR,
      ArrayQ<Ts>& sourceL,
      ArrayQ<Ts>& sourceR ) const
  {
    // Nothing
  }

  template <class PDE, class Tp, class Tq, class Tg, class Ts>
  void jacobianSource(
      const PDE& pde, const ParamTuple<DLA::MatrixSymS<D,Real>, Tp, TupleClass<>>& param,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdu ) const
  {
    this->jacobianSource(pde, param.left(), x, y, z, time, q, qx, qy, qz, dsdu);
  }

  template <class PDE, class Tp, class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue(
      const PDE& pde, const ParamTuple<DLA::MatrixSymS<D,Real>, Tp, TupleClass<>>& param,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdu ) const
  {}


  template <class PDE, class Tq, class Tg, class Ts>
  void jacobianSource(
      const PDE& pde, const DLA::MatrixSymS<D,Real>& logH, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdu ) const
  {
#if 0
    dsdu(iSens,iSens) += C3_;
#endif
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

protected:
  Real PDEorder_;
  const Sensor& sensor_;
  const Real C1_;
  const Real C3_;
};



//----------------------------------------------------------------------------//
// Shock sensor based on pressure gradient ala Ryan Glasby

template <template <class> class PDETraitsSize, class Sensor>
class AVSensor_Source3D_PressureGrad
{
public:
  template<class T>
  using ArrayQ = typename PDETraitsSize<PhysD3>::template ArrayQ<T>;

  template<class T>
  using MatrixQ = typename PDETraitsSize<PhysD3>::template MatrixQ<T>;

  static const int D = PhysD3::D;
  static const int iSens = PDETraitsSize<PhysD3>::iSens;

  typedef DLA::VectorS<D, Real> VectorX;

  explicit AVSensor_Source3D_PressureGrad(int PDEorder, const Sensor& sensor, const Real kappa = 0.3, const Real C1 = 3.0) :
    PDEorder_(Real(PDEorder)),
    sensor_(sensor),
    kappa_(kappa), C1_(C1)
  {
    // Nothing
  }

  bool hasSourceTerm() const { return true; }
  bool hasSourceTrace() const { return false; }
  bool hasSourceLiftedQuantity() const { return false; }
  bool needsSolutionGradientforSource() const { return true; }

  template<class PDE, class Tp, class Tq, class Tg, class Ts>
  void source( const PDE& pde, const ParamTuple<DLA::MatrixSymS<D,Real>, Tp, TupleClass<>>& param,
               const Real& x, const Real& y, const Real& z, const Real& time,
               const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
               ArrayQ<Ts>& source ) const
  {
    this->source(pde, param.left(), x, y, z, time, q, qx, qy, qz, source);
  }

  template<class PDE, class Tq, class Tg, class Ts>
  void source( const PDE& pde, const DLA::MatrixSymS<D,Real>& logH, const Real& x, const Real& y, const Real& z, const Real& time,
               const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
               ArrayQ<Ts>& source ) const
  {
    typedef typename promote_Surreal<Tq,Tg>::type Tp;

    if ( PDEorder_ < 0.5 ) return;

    Tq rho = 0, u = 0, v = 0, w = 0, t = 0, p = 0, c = 0;
    Tp px = 0, py = 0, pz = 0;

    sensor_.pde_.variableInterpreter().eval( q, rho, u, v, w, t );
    p  = sensor_.pde_.gasModel().pressure(rho, t);
    c  = sensor_.pde_.gasModel().speedofSound(t);

    Tp rhox = 0, ux = 0, vx = 0, wx = 0, tx = 0;
    Tp rhoy = 0, uy = 0, vy = 0, wy = 0, ty = 0;
    Tp rhoz = 0, uz = 0, vz = 0, wz = 0, tz = 0;

    pde.variableInterpreter().evalGradient( q, qx, rhox, ux, vx, wx, tx );
    pde.variableInterpreter().evalGradient( q, qy, rhoy, uy, vy, wy, ty );
    pde.variableInterpreter().evalGradient( q, qz, rhoz, uz, vz, wz, tz );

    pde.gasModel().pressureGradient( rho, t, rhox, tx, px );
    pde.gasModel().pressureGradient( rho, t, rhoy, ty, py );
    pde.gasModel().pressureGradient( rho, t, rhoz, tz, pz );

    DLA::MatrixSymS<D,Real> H = exp(logH); //generalized h-tensor

    typedef DLA::VectorS<D, Real> VectorX;
    VectorX hPrincipal;
    DLA::EigenValues(H, hPrincipal);

    //Get minimum length scale
    Real hmin = std::numeric_limits<Real>::max();
    for (int d = 0; d < D; d++)
      hmin = min(hmin, hPrincipal[d]);

    DLA::VectorS<D, Tq> V = {u, v, w};
    DLA::VectorS<D, Tp> gradp = {px, py, pz};

    // sensor from Ryan Glasby, but with a generalized H scale
    //Tp Ugradp = Transpose(V)*H*gradp;

    // sensor from Ryan Glasby
    Ts Ugradp = hmin*(u*px + v*py + w*pz);

    // Only positive values of the sensor indicate a shock
    Ugradp /= max(PDEorder_, Real(1));
    Ugradp = max(Tp(0), Ugradp);

    Ts sensor = Ugradp/(Ugradp + kappa_*c*p);

    sensor = sensor * tanh(10 * sensor);

    source(iSens) += (q(iSens) - sensor);
  }

  template<class Tp, class Tlq, class Tq, class Tg, class Ts>
  void source( const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
               const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
               ArrayQ<Ts>& source ) const
  {
    SANS_DEVELOPER_EXCEPTION("AVSensor_Source3D_PressureGrad::source with lifted quantity - Not implemented yet.");
  }

  template<class Tp, class Tq, class Ts>
  void sourceLiftedQuantity(
      const Tp& paramL, const Real& xL, const Real& yL, const Real& zL,
      const Tp& paramR, const Real& xR, const Real& yR, const Real& zR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      Ts& s ) const
  {
    SANS_DEVELOPER_EXCEPTION("AVSensor_Source3D_PressureGrad::sourceLiftedQuantity - Not implemented yet.");
  }

  template <class PDE, class Tp, class Tq, class Tg, class Ts>
  void jacobianSource(
      const PDE& pde, const ParamTuple<DLA::MatrixSymS<D,Real>, Tp, TupleClass<>>& param,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdu ) const
  {
    this->jacobianSource(pde, param.left(), x, y, z, time, q, qx, qy, qz, dsdu);
  }

  template <class PDE, class Tp, class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue(
      const PDE& pde, const ParamTuple<DLA::MatrixSymS<D,Real>, Tp, TupleClass<>>& param,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdu ) const
  {
    this->jacobianSource(pde, param.left(), x, y, z, time, q, qx, qy, qz, dsdu);
  }

  template <class PDE, class Tq, class Tg, class Ts>
  void jacobianSource(
      const PDE& pde, const DLA::MatrixSymS<D,Real>& logH,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdu ) const
  {
    if ( PDEorder_ < 0.5 ) return;
#if 1
    typedef typename promote_Surreal<Tq,Tg>::type Tp;

    Tq rho = 0, u = 0, v = 0, w = 0, t = 0, p = 0, c = 0;
    Tp px = 0, py = 0, pz = 0;

    pde.variableInterpreter().eval( q, rho, u, v, w, t );
    p  = pde.gasModel().pressure(rho, t);
    c  = pde.gasModel().speedofSound(t);

    Tq p_rho, p_t, c_t;
    pde.gasModel().pressureJacobian(rho, t, p_rho, p_t);
    pde.gasModel().speedofSoundJacobian(t, c_t);

    Tp rhox = 0, ux = 0, vx = 0, wx = 0, tx = 0;
    Tp rhoy = 0, uy = 0, vy = 0, wy = 0, ty = 0;
    Tp rhoz = 0, uz = 0, vz = 0, wz = 0, tz = 0;

    pde.variableInterpreter().evalGradient( q, qx, rhox, ux, vx, wx, tx );
    pde.variableInterpreter().evalGradient( q, qy, rhoy, uy, vy, wy, ty );
    pde.variableInterpreter().evalGradient( q, qz, rhoz, uz, vz, wz, tz );

    pde.gasModel().pressureGradient( rho, t, rhox, tx, px );
    pde.gasModel().pressureGradient( rho, t, rhoy, ty, py );
    pde.gasModel().pressureGradient( rho, t, rhoz, tz, pz );

    Real Cv = pde.gasModel().Cv();
    Tq E = pde.gasModel().energy(rho, t) + 0.5*(u*u + v*v + w*w);
    Tq t_rho, t_rhou, t_rhov, t_rhow, t_rhoE;

    // Temperature from conservative variables
    //t = ( rhoE/rho - 0.5*( rhou*rhou + rhov*rhov )/(rho*rho) ) / Cv
    t_rho  = ( -E/rho + ( u*u + v*v + w*w )/rho ) / Cv;
    t_rhou = (        - (   u             )/rho ) / Cv;
    t_rhov = (        - (         v       )/rho ) / Cv;
    t_rhow = (        - (             w   )/rho ) / Cv;
    t_rhoE = (  1/rho                     ) / Cv;

    Tq c_rho  = c_t*t_rho;
    Tq c_rhou = c_t*t_rhou;
    Tq c_rhov = c_t*t_rhov;
    Tq c_rhow = c_t*t_rhow;
    Tq c_rhoE = c_t*t_rhoE;

       p_rho += p_t*t_rho;
    Tq p_rhou = p_t*t_rhou;
    Tq p_rhov = p_t*t_rhov;
    Tq p_rhow = p_t*t_rhow;
    Tq p_rhoE = p_t*t_rhoE;

    // velocity Jacobian wrt conservative variables
    Tq u_rho  = -u/rho;
    Tq u_rhou =  1/rho;

    Tq v_rho  = -v/rho;
    Tq v_rhov =  1/rho;

    Tq w_rho  = -w/rho;
    Tq w_rhow =  1/rho;

    DLA::MatrixSymS<D,Real> H = exp(logH); //generalized h-tensor

    typedef DLA::VectorS<D, Real> VectorX;
    VectorX hPrincipal;
    DLA::EigenValues(H, hPrincipal);

    //Get minimum length scale
    Real hmin = std::numeric_limits<Real>::max();
    for (int d = 0; d < D; d++)
      hmin = min(hmin, hPrincipal[d]);
    // pre-divide by order...
    hmin /= max(PDEorder_, Real(1));

    // sensor from Ryan Glasby, but with a generalized H scale
    //Tp Ugradp = Transpose(V)*H*gradp;
    //Ugradp /= max(PDEorder_, Real(1));

    // sensor from Ryan Glasby
    Ts Ugradp = hmin*(u*px + v*py + w*pz);
    Ts Ugradp_rho  = (u_rho*px + v_rho*py + w_rho*pz)*hmin;
    Ts Ugradp_rhou = u_rhou*px*hmin;
    Ts Ugradp_rhov = v_rhov*py*hmin;
    Ts Ugradp_rhow = w_rhow*pz*hmin;
    Ts Ugradp_rhoE = 0.0;

    // Only positive values of the sensor indicate a shock
    if (Ugradp < 0)
    {
      Ugradp_rho = 0.0;
      Ugradp_rhou = 0.0;
      Ugradp_rhov = 0.0;
      Ugradp_rhow = 0.0;
      Ugradp_rhoE = 0.0;
    }
    Ugradp = max(Tp(0), Ugradp);

    Ts s = Ugradp/(Ugradp + kappa_*c*p);
    Ts s_rho  = kappa_*c*p/pow(Ugradp + kappa_*c*p,2) * Ugradp_rho  - Ugradp/pow(Ugradp + kappa_*c*p,2) * kappa_ * (p*c_rho  + c*p_rho );
    Ts s_rhou = kappa_*c*p/pow(Ugradp + kappa_*c*p,2) * Ugradp_rhou - Ugradp/pow(Ugradp + kappa_*c*p,2) * kappa_ * (p*c_rhou + c*p_rhou);
    Ts s_rhov = kappa_*c*p/pow(Ugradp + kappa_*c*p,2) * Ugradp_rhov - Ugradp/pow(Ugradp + kappa_*c*p,2) * kappa_ * (p*c_rhov + c*p_rhov);
    Ts s_rhow = kappa_*c*p/pow(Ugradp + kappa_*c*p,2) * Ugradp_rhow - Ugradp/pow(Ugradp + kappa_*c*p,2) * kappa_ * (p*c_rhow + c*p_rhow);
    Ts s_rhoE = kappa_*c*p/pow(Ugradp + kappa_*c*p,2) * Ugradp_rhoE - Ugradp/pow(Ugradp + kappa_*c*p,2) * kappa_ * (p*c_rhoE + c*p_rhoE);

    Ts stanh_s = 10 * s / (cosh(10*s)*cosh(10*s)) + tanh(10*s);

    dsdu(iSens,PDE::iCont) -= stanh_s * s_rho;
    dsdu(iSens,PDE::ixMom) -= stanh_s * s_rhou;
    dsdu(iSens,PDE::iyMom) -= stanh_s * s_rhov;
    dsdu(iSens,PDE::izMom) -= stanh_s * s_rhow;
    dsdu(iSens,PDE::iEngy) -= stanh_s * s_rhoE;
#endif
    dsdu(iSens,iSens) += 1.;
  }

  template <class Tp, class Tq, class Tg, class Ts>
  void sourceTrace(
      const Tp& paramL, const Real& xL, const Real& yL, const Real& zL,
      const Tp& paramR, const Real& xR, const Real& yR, const Real& zR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qzL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qzR,
      ArrayQ<Ts>& sourceL,
      ArrayQ<Ts>& sourceR ) const
  {
    // Nothing
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const {}

protected:
  Real PDEorder_;
  const Sensor& sensor_;
  const Real kappa_;
  const Real C1_;
};

} //namespace SANS

#endif  // AVSENSOR_SOURCE3D_H
