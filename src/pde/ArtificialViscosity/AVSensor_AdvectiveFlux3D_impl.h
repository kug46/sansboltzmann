// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(AVSENSOR3D_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include <string>
#include <iostream>

#include "AVSensor_AdvectiveFlux3D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// advection flux: uniform flow
template <template <class> class PDETraitsSize>
void
AVSensor_AdvectiveFlux3D_Uniform<PDETraitsSize>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "AVSensor_AdvectiveFlux3D_Uniform:"
            << "  u_ = " << u_ << std::endl
            << "  v_ = " << v_ << std::endl
            << "  w_ = " << w_ << std::endl;
}

} //namespace SANS
