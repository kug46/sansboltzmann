
INCLUDE( ${CMAKE_SOURCE_DIR}/CMakeInclude/ForceOutOfSource.cmake ) #This must be the first thing included

SET( POROUSMEDIA_SRC

    Q1DPrimitive_Sw.cpp
    Q1DPrimitive_SurrogateSw.cpp
    Q1DPrimitive_pnSw.cpp
    Q2DPrimitive_pnSw.cpp
    Q1DPrimitive_Surrogate_pnSw.cpp
    Q2DPrimitive_Surrogate_pnSw.cpp
    TwoPhaseVariable.cpp
    
    RelPermModel_PowerLaw.cpp
    ViscosityModel_Constant.cpp
    DensityModel.cpp
    PorosityModel.cpp
    CapillaryModel.cpp
    PermeabilityModel2D.cpp
    
    BCBuckleyLeverett1D.cpp
    BCBuckleyLeverett_ArtificialViscosity1D.cpp
    BCTwoPhase1D.cpp
    BCTwoPhase2D.cpp
    BCTwoPhaseArtificialViscosity1D.cpp
    BCTwoPhaseArtificialViscosity2D.cpp
    BCTwoPhaseLinearized1D.cpp
    
    SourceTwoPhase1D.cpp
    SourceTwoPhase2D.cpp
    SolutionFunction_TwoPhase1D.cpp
    SolutionFunction_TwoPhase2D.cpp
    
    CartTable.cpp
  )

ADD_LIBRARY( PorousMediaLib STATIC ${POROUSMEDIA_SRC} )

#Create the vera targets for this library
ADD_VERA_CHECKS_RECURSE( PorousMediaLib *.h *.cpp )
ADD_HEADER_COMPILE_CHECK_RECURSE( PorousMediaLib *.h )
ADD_CPPCHECK( PorousMediaLib ${POROUSMEDIA_SRC} )