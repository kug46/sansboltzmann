// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "CartTable.h"
#include "tools/SANSException.h"

#include <fstream>

namespace SANS
{

//===========================================================================//
CartTable<2>::CartTable( const std::string& filename ) :
    imax_(0), jmax_(0), dx_(0), dy_(0)
{
  std::ifstream file(filename);

  if (!file.is_open())
    SANS_RUNTIME_EXCEPTION( "CartTable - Error opening file: %s", filename.c_str() );

  file >> imax_;
  file >> jmax_;
  file >> dx_;
  file >> dy_;

  if (imax_ <= 0)
    SANS_RUNTIME_EXCEPTION( "CartTable - imax = %d must be positive!", imax_ );

  if (jmax_ <= 0)
    SANS_RUNTIME_EXCEPTION( "CartTable - jmax = %d must be positive!", jmax_ );

  if (dx_ <= 0)
    SANS_RUNTIME_EXCEPTION( "CartTable - dx = %f must be positive!", dx_ );

  if (dy_ <= 0)
    SANS_RUNTIME_EXCEPTION( "CartTable - dy = %f must be positive!", dy_ );

  table_.resize(imax_*jmax_);

  for (int i = 0; i < imax_; i++)
    for (int j = 0; j < jmax_; j++)
    {
      file >> table_[i*jmax_ + j];
      if (file.bad() || file.eof())
        SANS_RUNTIME_EXCEPTION( "CartTable - Failed to read table index %d %d!", i, j );
    }
}

//===========================================================================//
Real
CartTable<2>::operator()(const Real x, const Real y) const
{
  SANS_ASSERT_MSG( x >= 0 && x <= imax_*dx_, "CartTable x = %f is out of range %f!", x, imax_*dx_);
  SANS_ASSERT_MSG( y >= 0 && y <= jmax_*dy_, "CartTable y = %f is out of range %f!", y, jmax_*dy_);

  const int i = std::min((int)(x/dx_), imax_-1);
  const int j = std::min((int)(y/dy_), jmax_-1);
  return table_[i*jmax_ + j];
}

////===========================================================================//
//void
//CartTable<2>::scaleValues(const Real factor)
//{
//  for (auto& val : table_)
//    val *= factor;
//}

}
