// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDETWOPHASE_ARTIFICIALVISCOSITY1D_H
#define PDETWOPHASE_ARTIFICIALVISCOSITY1D_H

// 1D two-phase flow PDE class with artificial viscosity

#include <iostream>
#include <string>

#include "tools/SANSnumerics.h"     // Real
#include "tools/smoothmath.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Exp.h"

#include "Topology/Dimension.h"

#include "PDETwoPhase1D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: 1-D Two Phase with artificial viscosity
//
// equations: d/dt( rho_w phi Sw ) - div( rho_w K lambda_w grad(p_w) ) = rho_w q_w
//            d/dt( rho_n phi Sn ) - div( rho_n K lambda_n grad(p_n) ) = rho_n q_n
//
// where:    lambda_w = krw/mu_w
//           lambda_n = krn/mu_n
//
// template parameters:
//   T                        solution DOF data type (e.g. double)
//   QType                    solution variable set (e.g. primitive)
//   DensityModel             density models (for each phase)
//   PorosityModel            porosity model
//   RelPermModel             relative permeability models (for each phase)
//   ViscModel                viscosity models (for each phase)
//   CapillaryModel           capillary pressure model
//
// member functions:
//   .hasFluxAdvectiveTime        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//   .hasFluxViscous             T/F: PDE has viscous flux term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: dU/dQ
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .fluxViscous                viscous fluxes: Fv(Q, QX)
//   .diffusionViscous           viscous diffusion coefficient: d(Fv)/d(UX)
//   .isValidState               T/F: determine if state is physically valid (e.g. Sw > 0)
//   .setDOFFrom                 set from primitive variable array
//----------------------------------------------------------------------------//

template <template <class> class PDETraitsSize, class PDETraitsModel>
class PDETwoPhase_ArtificialViscosity1D : public PDETwoPhase1D<PDETraitsSize, PDETraitsModel>
{
public:
  typedef PhysD1 PhysDim;
  static const int D = PDETraitsSize<PhysDim>::D; // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N; // total solution variables

  typedef PDETwoPhase1D<PDETraitsSize, PDETraitsModel> BaseType;

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;   // solution/residual arrays

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>; // matrices

  using BaseType::qInterpreter_;
  using BaseType::rhow_;
  using BaseType::rhon_;
  using BaseType::krw_;
  using BaseType::krn_;
  using BaseType::muw_;
  using BaseType::mun_;
  using BaseType::Kxx_;
  using BaseType::pc_;

  // Constructor forwards arguments to PDE class using varargs
  template< class... PDEArgs >
  PDETwoPhase_ArtificialViscosity1D(const int order, const bool hasSpaceTimeDiffusion, PDEArgs&&... args) :
    BaseType(std::forward<PDEArgs>(args)...),
    order_(order), hasSpaceTimeDiffusion_(hasSpaceTimeDiffusion)
  {
    //Nothing
  }

  ~PDETwoPhase_ArtificialViscosity1D() {}

  PDETwoPhase_ArtificialViscosity1D& operator=( const PDETwoPhase_ArtificialViscosity1D& ) = delete;

  // flux components
  bool hasFluxAdvectiveTime() const { return true; }
  bool hasFluxAdvective() const { return BaseType::hasFluxAdvective(); }
  bool hasFluxViscous() const { return true; }
  bool hasSource() const { return BaseType::hasSource(); }
  bool hasSourceTrace() const { return BaseType::hasSourceTrace(); }
  bool hasForcingFunction() const { return BaseType::hasForcingFunction(); }

  // viscous flux is of the form K(q).gradu
  bool fluxViscousLinearInGradient() const { return true; }
  bool needsSolutionGradientforSource() const { return BaseType::needsSolutionGradientforSource(); }

  int PDEorder() const { return order_; }

  // unsteady temporal flux: Ft(Q)
  template <class Tp, class T, class Tf>
  void fluxAdvectiveTime(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tf>& ft ) const
  {
    BaseType::fluxAdvectiveTime(x, time, q, ft);
  }

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <class Tp, class T, class Tf>
  void jacobianFluxAdvectiveTime(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& J ) const
  {
    BaseType::jacobianFluxAdvectiveTime(x, time, q, J);
  }

  // master state: U(Q)
  template <class Tp, class T, class Tu>
  void masterState(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tu>& uCons ) const
  {
    BaseType::masterState(x, time, q, uCons);
  }

  // jacobian of master state wrt q: dU(Q)/dQ
  template<class Tp, class T>
  void jacobianMasterState(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
  {
    BaseType::jacobianMasterState( x, time, q, dudq);
  }

  // advective flux: F(Q)
  template <class Tp, class T, class Tf>
  void fluxAdvective(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tf>& f ) const
  {
    BaseType::fluxAdvective(x, time, q, f);
  }

  template <class Tp, class T, class Tf>
  void fluxAdvectiveUpwind(
      const Tp& param, const Real& x, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, ArrayQ<Tf>& f ) const
  {
    BaseType::fluxAdvectiveUpwind(x, time, qL, qR, nx, f);
  }

  template <class Tp, class T, class Tf>
  void fluxAdvectiveUpwindSpaceTime(
      const Tp& param, const Real& x, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const
  {
    BaseType::fluxAdvectiveUpwindSpaceTime(x, time, qL, qR, nx, nt, f);
  }

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class Tp, class T>
  void jacobianFluxAdvective(
      const Tp& param, const Real& x, const Real& time, const ArrayQ<T>& q,
      MatrixQ<T>& ax ) const
  {
    BaseType::jacobianFluxAdvective( x, time, q, ax );
  }

  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscous(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Tf>& f ) const;

  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& ft ) const;

  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscous(
      const Tp& paramL, const Tp& paramR, const Real& x, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
      const Real& nx, ArrayQ<Tf>& fn ) const;

  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Tp& paramL, const Tp& paramR, const Real& x, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qtL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qtR,
      const Real& nx, const Real& nt, ArrayQ<Tf>& fn ) const;

  // viscous diffusion matrix: d(Fv)/d(Ux)
  template <class Tp, class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Tk>& kxx) const;

  // solution-dependent source: S(x, Q, Qx)
  template <class Tp, class Tq, class Tg, class Ts>
  void source(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Ts>& source ) const
  {
    BaseType::source( x, time, q, qx, source );
  }

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX)
  template <class Tp, class Tlq, class Tq, class Tg, class Ts>
  void source(
      const Tp& param, const Real& x, const Real& time,
      const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Ts>& source ) const
  {
    BaseType::source( x, time, lifted_quantity, q, qx, source );
  }

  // dual-consistent source
  template <class Tp, class Tq, class Tg, class Ts>
  void sourceTrace(
      const Tp& paramL, const Real& xL,
      const Tp& paramR, const Real& xR,
      const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
  {
    BaseType::sourceTrace(xL, xR, time, qL, qxL, qR, qxR, sourceL, sourceR);
  }

  // trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
  template <class Tp, class Tq, class Ts>
  void sourceLiftedQuantity(
      const Tp& paramL, const Real& xL,
      const Tp& paramR, const Real& xR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, Ts& s ) const
  {
    BaseType::sourceLiftedQuantity(xL, xR, time, qL, qR, s);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tp, class T>
  void jacobianSource(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx,
      MatrixQ<T>& dsdu ) const
  {
    BaseType::jacobianSource(x, time, q, qx, dsdu);
  }
  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tp, class T>
  void jacobianSourceAbsoluteValue(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx,
      MatrixQ<T>& dsdu ) const
  {
    BaseType::jacobianSourceAbsoluteValue(x, time, q, qx, dsdu);
  }

  // right-hand-side forcing function
  template <class Tp, class T>
  void forcingFunction(
      const Tp& param,
      const Real& x, const Real& time, ArrayQ<T>& forcing ) const
  {
    BaseType::forcingFunction(x, time, forcing );
  }

  // characteristic speed (needed for timestep)
  template <class Tp, class T>
  void speedCharacteristic(
      const Tp& param, const Real& x, const Real& time,
      const Real& dx, const ArrayQ<T>& q, T& speed ) const
  {
    BaseType::speedCharacteristic(x, time, dx, q, speed);
  }

  // characteristic speed
  template <class Tp, class T, class Ts>
  void speedCharacteristic(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, Ts& speed ) const
  {
    BaseType::speedCharacteristic(x, time, q, speed);
  }

  // update fraction needed for physically valid state
  template <class Tp>
  void updateFraction(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
      const Real maxChangeFraction, Real& updateFraction ) const
  {
    BaseType::updateFraction(x, time, q, dq, maxChangeFraction, updateFraction);
  }

  template<class Tq, class Tg, class Tv>
  void characteristicVelocitySinglePhase( const Real& x, const Real& time,
                                          const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
                                          ArrayQ<Tv>& vx ) const;

  template<class Tq, class Tg, class Tv>
  void characteristicVelocity( const Real& x, const Real& time,
                               const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
                               Tv& vx ) const;

  template <class Tp, class Tq, class Tg, class Tf>
  void artViscMax(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      Tf& nu_max ) const;

  std::vector<std::string> derivedQuantityNames() const;

  template<class Tp, class Tq, class Tg, class Tf>
  void derivedQuantity(
      const int& index, const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, Tf& J ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

  int getOrder() const { return order_; };

protected:

  // Artificial viscosity flux - artificial viscosity variable in augmented state vector
  template <class Tp, class Tq, class Tg, class Tf>
  void artificialViscosityFlux(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Tf>& f ) const;

  // Space-time artificial viscosity flux - artificial viscosity variable in augmented state vector
  template <class Tp, class Tq, class Tg, class Tf>
  void artificialViscosityFluxSpaceTime(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& ft ) const;

  template<class Th, class T>
  T velocity_h( const Th& H, const T& vx ) const
  {
    Th Hsq = H*H; //H is a MatrixSymS<D,Real> or MatrixSymS<D+1,Real>

    T vT_Hsq_v = vx*Hsq(0,0)*vx;
    return sqrt(vT_Hsq_v);
  }

  const int order_;
  const bool hasSpaceTimeDiffusion_;
};

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDETwoPhase_ArtificialViscosity1D<PDETraitsSize, PDETraitsModel>::artificialViscosityFlux(
    const Tp& param, const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    ArrayQ<Tf>& fx ) const
{
  SANS_ASSERT(N == 3); //Check if the state vector has been augmented with the sensor

  typedef typename promote_Surreal<Tq,Tg>::type T;

  Tq nu = q(N-1); //artificial viscosity
  const Real alpha = 40.0;
  nu = smoothmax(nu, Tq(0.0), alpha); //ensure nu is non-negative (since it can go negative in under-resolved regions)

  Tq Sw = 0, Sn = 0, pw = 0, pn = 0;
  qInterpreter_.eval( q, pw, pn, Sw, Sn );

  //Evaluate densities
  Tq rhow = rhow_.density(x, time, pw);
  Tq rhon = rhon_.density(x, time, pn);

  T Swx = 0;
  T Snx = 0;
  qInterpreter_.eval_SwGradient( q, qx, Swx );
  qInterpreter_.eval_SnGradient( q, qx, Snx );

  fx(0) -=  rhow*nu*Swx;
  fx(1) -= -rhon*nu*Swx;
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDETwoPhase_ArtificialViscosity1D<PDETraitsSize, PDETraitsModel>::artificialViscosityFluxSpaceTime(
    const Tp& param, const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
    ArrayQ<Tf>& fx, ArrayQ<Tf>& ft ) const
{
  SANS_ASSERT(N == 3); //Check if the state vector has been augmented with the sensor

  typedef typename promote_Surreal<Tq,Tg>::type T;

  Tq nu = q(N-1); //artificial viscosity
  const Real alpha = 40.0;
  nu = smoothmax(nu, Tq(0.0), alpha); //ensure nu is non-negative (since it can go negative in under-resolved regions)

  Tq Sw = 0, Sn = 0, pw = 0, pn = 0;
  qInterpreter_.eval( q, pw, pn, Sw, Sn );

  //Evaluate densities
  Tq rhow = rhow_.density(x, time, pw);
  Tq rhon = rhon_.density(x, time, pn);

  T Swx = 0, Swt = 0;
  qInterpreter_.eval_SwGradient( q, qx, Swx );
  qInterpreter_.eval_SwGradient( q, qt, Swt );

  fx(0) -=  rhow*nu*Swx;
  fx(1) -= -rhon*nu*Swx;

  ft(0) -=  rhow*nu*Swt;
  ft(1) -= -rhon*nu*Swt;
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDETwoPhase_ArtificialViscosity1D<PDETraitsSize, PDETraitsModel>::artViscMax(
    const Tp& param, const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    Tf& nu_max ) const
{
  Tp H = exp(param); //param is either MatrixSymS<D,Th> or MatrixSymS<D+1,Th> of log(H)

#if 1
  ArrayQ<Tf> vx = 0;
  characteristicVelocitySinglePhase(x, time, q, qx, vx);

  Tf vw_h = velocity_h(H, vx[0]);
  Tf vn_h = velocity_h(H, vx[1]);

  const Real alpha = 40;
  Tf v_h = smoothmax(vw_h, vn_h, alpha);
#else
  Tf vx = 0;
  characteristicVelocity(x, time, q, qx, vx);
  Tf v_h = velocity_h(H, vx) + 1e-8;
#endif

  const Real Pe = 2.0; //required grid Peclet number

  nu_max = 0; //maximum artificial viscosity (when sensor is 1)

  if (order_ > 0) //don't need artificial viscosity for P0
  {
    nu_max = (v_h / order_) / Pe;
    if (nu_max <= 0)
    {
      std::cout << "nu_max: " << nu_max << std::endl;
      std::cout << "q: " << q << ", vx: " << vx << std::endl;
      std::cout << "H: " << H << std::endl;
    }
    SANS_ASSERT_MSG( nu_max > 0, "PDETwoPhase_ArtificialViscosity1D::artViscMax - Max artificial viscosity is not positive!");
  }
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDETwoPhase_ArtificialViscosity1D<PDETraitsSize, PDETraitsModel>::fluxViscous(
    const Tp& param, const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    ArrayQ<Tf>& fx ) const
{
  SANS_ASSERT(!hasSpaceTimeDiffusion_); //space-time diffusion requires a space-time NDPDE

  artificialViscosityFlux(param, x, time, q, qx, fx);
  BaseType::fluxViscous( x, time, q, qx, fx );
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDETwoPhase_ArtificialViscosity1D<PDETraitsSize, PDETraitsModel>::fluxViscousSpaceTime(
    const Tp& param, const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
    ArrayQ<Tf>& fx, ArrayQ<Tf>& ft ) const
{
  if (hasSpaceTimeDiffusion_)
    artificialViscosityFluxSpaceTime(param, x, time, q, qx, qt, fx, ft);
  else
    artificialViscosityFlux(param, x, time, q, qx, fx);

  BaseType::fluxViscousSpaceTime( x, time, q, qx, qt, fx, ft );
}

// viscous flux: normal flux with left and right states
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDETwoPhase_ArtificialViscosity1D<PDETraitsSize, PDETraitsModel>::fluxViscous(
    const Tp& paramL, const Tp& paramR, const Real& x, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
    const Real& nx, ArrayQ<Tf>& fn ) const
{
  SANS_ASSERT(!hasSpaceTimeDiffusion_); //space-time diffusion requires a space-time NDPDE

  ArrayQ<Tf> fxL = 0, fxR = 0;

  artificialViscosityFlux(paramL, x, time, qL, qxL, fxL);
  artificialViscosityFlux(paramR, x, time, qR, qxR, fxR);

  //Compute the average artificial viscosity flux from both sides
  fn += 0.5*(fxL + fxR)*nx;

  //Add on any interface viscous fluxes from the base class
  BaseType::fluxViscous( x, time, qL, qxL, qR, qxR, nx, fn );
}

// viscous flux: normal flux with left and right states
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDETwoPhase_ArtificialViscosity1D<PDETraitsSize, PDETraitsModel>::fluxViscousSpaceTime(
    const Tp& paramL, const Tp& paramR, const Real& x, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qtL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qtR,
    const Real& nx, const Real& nt, ArrayQ<Tf>& fn ) const
{
  ArrayQ<Tf> fxL = 0, fxR = 0;
  ArrayQ<Tf> ftL = 0, ftR = 0;

  if (hasSpaceTimeDiffusion_)
  {
    artificialViscosityFluxSpaceTime(paramL, x, time, qL, qxL, qtL, fxL, ftL);
    artificialViscosityFluxSpaceTime(paramR, x, time, qR, qxR, qtR, fxR, ftR);
  }
  else
  {
    artificialViscosityFlux(paramL, x, time, qL, qxL, fxL);
    artificialViscosityFlux(paramR, x, time, qR, qxR, fxR);
  }

  //Compute the average artificial viscosity flux from both sides
  fn += 0.5*(fxL + fxR)*nx + 0.5*(ftL + ftR)*nt;

  //Add on any interface viscous fluxes from the base class
  BaseType::fluxViscousSpaceTime( x, time, qL, qxL, qtL, qR, qxR, qtR, nx, nt, fn );
}

// viscous diffusion matrix: d(Fv)/d(Ux)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tk>
inline void
PDETwoPhase_ArtificialViscosity1D<PDETraitsSize, PDETraitsModel>::diffusionViscous(
    const Tp& param, const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    MatrixQ<Tk>& kxx ) const
{
  SANS_DEVELOPER_EXCEPTION("PDETwoPhase_ArtificialViscosity1D::diffusionViscous - Not implemented yet.");
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template<class Tq, class Tg, class Tv>
inline void
PDETwoPhase_ArtificialViscosity1D<PDETraitsSize, PDETraitsModel>::
characteristicVelocity( const Real& x, const Real& time,
                        const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, Tv& vx ) const
{
  typedef typename promote_Surreal<Tq,Tg>::type T;

  Tq Sw = 0, Sn = 0, pw = 0, pn = 0;
  T pwx = 0, pnx = 0;

  qInterpreter_.eval( q, pw, pn, Sw, Sn );

  qInterpreter_.eval_pwGradient( q, qx, pwx );
  qInterpreter_.eval_pnGradient( q, qx, pnx );

  T Kgradpwx = Kxx_*pwx;
  T Kgradpnx = Kxx_*pnx;

  //Evaluate relative permeabilities
  Tq krw = krw_.relativeperm(Sw);
  Tq krn = krn_.relativeperm(Sn);

  //Evaluate phase viscosities
  Real muw = muw_.viscosity(x, time);
  Real mun = mun_.viscosity(x, time);

  //Evaluate phase mobilities
  Tq lambda_w = krw/muw;
  Tq lambda_n = krn/mun;

  //Evaluate relative permeability jacobians
  Tq krw_Sw, krn_Sn;
  krw_.relativepermJacobian(Sw, krw_Sw);
  krn_.relativepermJacobian(Sn, krn_Sn);

  Real dSn_dSw = -1;
  Tq krn_Sw = krn_Sn * dSn_dSw;

  //Evaluate phase mobility jacobians
  Tq lambda_w_Sw = krw_Sw/muw;
  Tq lambda_n_Sw = krn_Sw/mun;

  //Velocity components - without phi factor in denominator (since that gets canceled out)
  vx = -(lambda_w_Sw*lambda_n*Kgradpwx - lambda_w*lambda_n_Sw*Kgradpnx) / (lambda_w + lambda_n);
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template<class Tq, class Tg, class Tv>
inline void
PDETwoPhase_ArtificialViscosity1D<PDETraitsSize, PDETraitsModel>::
characteristicVelocitySinglePhase( const Real& x, const Real& time,
                                   const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, ArrayQ<Tv>& vx ) const
{
  typedef typename promote_Surreal<Tq,Tg>::type T;

  T pwx = 0, pnx = 0;
  qInterpreter_.eval_pwGradient( q, qx, pwx );
  qInterpreter_.eval_pnGradient( q, qx, pnx );

  //Evaluate phase viscosities
  Real muw = muw_.viscosity(x, time);
  Real mun = mun_.viscosity(x, time);

  //Evaluate phase velocities (without mobility weighting)
  //Ignoring phi factor in denominator since it gets canceled out with the phi factor in the viscosity
  vx[0] = -Kxx_*pwx/muw;
  vx[1] = -Kxx_*pnx/mun;
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
inline std::vector<std::string>
PDETwoPhase_ArtificialViscosity1D<PDETraitsSize, PDETraitsModel>::
derivedQuantityNames() const
{
  std::vector<std::string> names = {"charvelocity",
                                    "charvelocity_wet",
                                    "charvelocity_nonwet",
                                    "nu_max",
                                    "Peclet",
                                    "hxx"
                                   };

  //Append any names from the base class
  std::vector<std::string> basenames = BaseType::derivedQuantityNames();
  names.insert(names.end(), basenames.begin(), basenames.end());

  return names;
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template<class Tp, class Tq, class Tg, class Tf>
inline void
PDETwoPhase_ArtificialViscosity1D<PDETraitsSize, PDETraitsModel>::
derivedQuantity(const int& index, const Tp& param, const Real& x, const Real& time,
                const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, Tf& J ) const
{
  int nQtyCurrent = derivedQuantityNames().size() - BaseType::derivedQuantityNames().size();
  if (index >= nQtyCurrent)
  {
    BaseType::derivedQuantity(index - nQtyCurrent, x, time, q, qx, J);
    return;
  }

  switch (index)
  {
  case 0:
  {
    characteristicVelocity( x, time, q, qx, J );
    break;
  }

  case 1:
  {
    ArrayQ<Tf> vx = 0;
    characteristicVelocitySinglePhase(x, time, q, qx, vx);
    J = vx[0];
    break;
  }

  case 2:
  {
    ArrayQ<Tf> vx = 0;
    characteristicVelocitySinglePhase(x, time, q, qx, vx);
    J = vx[1];
    break;
  }

  case 3:
  {
    artViscMax(param, x, time, q, qx, J);
    break;
  }

  case 4:
  {
    Tp H = exp(param); //param is either MatrixSymS<D,Th> or MatrixSymS<D+1,Th> of log(H)
    Tf vx = 0;
    characteristicVelocity(x, time, q, qx, vx);
    Tf v_h = velocity_h(H, vx);

    SANS_ASSERT(N == 3); //Check if the state vector has been augmented with the sensor
    Tq nu = q(N-1); //artificial viscosity
    nu = smoothabs0(nu, 1e-3); //ensure nu is positive (since it can go negative in under-resolved regions)

    J = v_h / (nu * order_);
    break;
  }

  case 5:
  {
    Tp H = exp(param); //param is either MatrixSymS<D,Th> or MatrixSymS<D+1,Th> of log(H)
    J = H(0,0);
    break;
  }

  default:
    SANS_DEVELOPER_EXCEPTION("PDETwoPhase_ArtificialViscosity1D::derivedQuantity - Invalid index!");
  }
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
inline void
PDETwoPhase_ArtificialViscosity1D<PDETraitsSize, PDETraitsModel>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  std::string indent2(indentSize+2, ' ');

  out << indent << "PDETwoPhase_ArtificialViscosity1D:" << std::endl;
  out << indent2 << "order_ = " << order_ << std::endl;
  out << indent2 << "hasSpaceTimeDiffusion_ = " << hasSpaceTimeDiffusion_ << std::endl;
  out << indent2 << "pde_ = " << std::endl;
  BaseType::dump(indentSize+4, out);
}

} //namespace SANS

#endif  // PDETWOPHASE_ARTIFICIALVISCOSITY1D_H
