// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef DENSITYMODEL_H
#define DENSITYMODEL_H

// Density models

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <cmath> // exp
#include <iostream>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// A class for representing input parameters for DensityModel_Constant
struct DensityModel_Constant_Params : noncopyable
{
  const ParameterNumeric<Real> rho{"rho", NO_DEFAULT, 0, NO_LIMIT, "Density"};

  static void checkInputs(PyDict d);

  static DensityModel_Constant_Params params;
};

//----------------------------------------------------------------------------//
// A class for representing input parameters for DensityModel_Comp
struct DensityModel_Comp_Params : noncopyable
{
  const ParameterNumeric<Real> rho_ref{"rho_ref", NO_DEFAULT, 0, NO_LIMIT, "Reference density"};
  const ParameterNumeric<Real> C{"C", NO_DEFAULT, 0, NO_LIMIT, "Phase compressibility"};

  static void checkInputs(PyDict d);

  static DensityModel_Comp_Params params;
};

//----------------------------------------------------------------------------//
// DensityModel:
//
// template parameters:
//   T                        solution DOF data type (e.g. double)
//
// member functions:
//   .rho_ref                 get the reference density
//   .C                       get the phase compressibility
//
//   .density                 density rho(p) - function of phase pressure
//   .densityJacobian         jacobian: d(rho)/d(p)
//   .densityGradient         gradient: d(rho)/dx and d(rho)/dy
//
//   .dump                    debug dump of private data
//----------------------------------------------------------------------------//


class DensityModel_Constant
{
public:
  DensityModel_Constant_Params& params;

  explicit DensityModel_Constant( const PyDict& d ) :
      params(DensityModel_Constant_Params::params),
      rho_(d.get(params.rho)){}

  DensityModel_Constant( const DensityModel_Constant& model ) :
      params( DensityModel_Constant_Params::params ),
      rho_(model.rho_){}

  explicit DensityModel_Constant( Real rho ) :
      params(DensityModel_Constant_Params::params),
      rho_(rho)
  {
    SANS_ASSERT( rho > 0 );
  }

  DensityModel_Constant& operator=(const DensityModel_Constant& model)
  {
    rho_ = model.rho_;
    return *this;
  }

  ~DensityModel_Constant() {}

  // Density
  template<class T>
  T density( const Real& x, const Real& time, const T& p ) const;
  template<class T>
  T density( const Real& x, const Real& y, const Real& time, const T& p ) const;

  template<class T>
  void densityJacobian( const Real& x, const Real& time, const T& p, T& rho_p ) const;
  template<class T>
  void densityJacobian( const Real& x, const Real& y, const Real& time, const T& p, T& rho_p ) const;

  template<class T>
  void densityGradient( const Real& x, const Real& time, const T& p, const T& px, T& rhox ) const;
  template<class T>
  void densityGradient( const Real& x, const Real& y, const Real& time, const T& p, const T& px, const T& py,
                        T& rhox, T& rhoy ) const;

  void dump( std::string indent, std::ostream& out = std::cout ) const;
  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  Real rho_; // Density (constant)
};


template <class T>
inline T
DensityModel_Constant::density( const Real& x, const Real& time, const T& p) const
{
  return rho_;
}

template <class T>
inline T
DensityModel_Constant::density( const Real& x, const Real& y, const Real& time, const T& p) const
{
  return rho_;
}

template <class T>
inline void
DensityModel_Constant::densityJacobian( const Real& x, const Real& time, const T& p, T& rho_p ) const
{
  rho_p = 0.0;
}

template <class T>
inline void
DensityModel_Constant::densityJacobian( const Real& x, const Real& y, const Real& time, const T& p, T& rho_p ) const
{
  rho_p = 0.0;
}

template <class T>
inline void
DensityModel_Constant::densityGradient( const Real& x, const Real& time, const T& p, const T& px, T& rhox ) const
{
  rhox = 0.0;
}

template <class T>
inline void
DensityModel_Constant::densityGradient( const Real& x, const Real& y, const Real& time, const T& p, const T& px, const T& py,
                                        T& rhox, T& rhoy) const
{
  rhox = 0.0;
  rhoy = 0.0;
}

/*-----------------------------------------------------------------------------------------------------------------------*/

class DensityModel_Comp
{
public:
  DensityModel_Comp_Params& params;

  explicit DensityModel_Comp( const PyDict& d, const Real& pref ) :
      params(DensityModel_Comp_Params::params),
      rho_ref_(d.get(params.rho_ref)),
      C_(d.get(params.C)),
      pref_(pref){}

  DensityModel_Comp( const DensityModel_Comp& model ) :
      params( DensityModel_Comp_Params::params ),
      rho_ref_(model.rho_ref_),
      C_(model.C_),
      pref_(model.pref_){}

  DensityModel_Comp( Real rho_ref, Real C, Real pref ) :
      params(DensityModel_Comp_Params::params),
      rho_ref_(rho_ref),
      C_(C),
      pref_(pref)
  {
    SANS_ASSERT( rho_ref > 0 );
    SANS_ASSERT( C >= 0 );
  }

  DensityModel_Comp& operator=(const DensityModel_Comp& model) = delete;

  ~DensityModel_Comp() {}

  Real rho_ref() const { return rho_ref_; }
  Real C() const { return C_; }

  // Density
  template<class T>
  T density( const Real& x, const Real& time, const T& p ) const;
  template<class T>
  T density( const Real& x, const Real& y, const Real& time, const T& p ) const;

  template<class T>
  void densityJacobian( const Real& x, const Real& time, const T& p, T& rho_p ) const;
  template<class T>
  void densityJacobian( const Real& x, const Real& y, const Real& time, const T& p, T& rho_p ) const;

  template<class T>
  void densityGradient( const Real& x, const Real& time, const T& p, const T& px, T& rhox ) const;
  template<class T>
  void densityGradient( const Real& x, const Real& y, const Real& time, const T& p, const T& px, const T& py,
                        T& rhox, T& rhoy ) const;

  void dump( std::string indent, std::ostream& out = std::cout ) const;
  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  Real rho_ref_; // Reference density
  Real C_; // Phase compressibility
  const Real pref_; //Reference pressure
};


template <class T>
inline T
DensityModel_Comp::density( const Real& x, const Real& time, const T& p) const
{
  T rho = rho_ref_*exp(C_*(p - pref_));
  return rho;
}

template <class T>
inline T
DensityModel_Comp::density( const Real& x, const Real& y, const Real& time, const T& p) const
{
  T rho = rho_ref_*exp(C_*(p - pref_));
  return rho;
}

template <class T>
inline void
DensityModel_Comp::densityJacobian( const Real& x, const Real& time, const T& p, T& rho_p ) const
{
  rho_p = C_*rho_ref_*exp(C_*(p - pref_));
}

template <class T>
inline void
DensityModel_Comp::densityJacobian( const Real& x, const Real& y, const Real& time, const T& p, T& rho_p ) const
{
  rho_p = C_*rho_ref_*exp(C_*(p - pref_));
}

#if 1
template <class T>
inline void
DensityModel_Comp::densityGradient( const Real& x, const Real& time, const T& p, const T& px, T& rhox ) const
{
  //Compute Jacobian
  T rho_p;
  densityJacobian( x, time, p, rho_p );

  //Apply chain rule for gradient: d(rho)/dx = d(rho)/dp * dp/dx
  rhox = rho_p*px;
}

template <class T>
inline void
DensityModel_Comp::densityGradient( const Real& x, const Real& y, const Real& time, const T& p, const T& px, const T& py,
                                    T& rhox, T& rhoy) const
{
  //Compute Jacobian
  T rho_p;
  densityJacobian( x, y, time, p, rho_p );

  //Apply chain rule for gradient: d(rho)/dx = d(rho)/dp * dp/dx
  rhox = rho_p*px;
  rhoy = rho_p*py;
}
#endif

} //namespace SANS

#endif  // DENSITYMODEL_H
