// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef TWOPHASEVARIABLE_H
#define TWOPHASEVARIABLE_H

#include "Python/PyDict.h" // python must be included first
#include "Python/Parameter.h"

#include <initializer_list>

#include "tools/SANSnumerics.h" // Real
#include "tools/SANSException.h"

namespace SANS
{

template<class Derived>
struct TwoPhaseVariableType
{
  //A convenient method for casting to the derived type
  inline const Derived& cast() const { return static_cast<const Derived&>(*this); }
};


//===========================================================================//
struct PressureNonWet_SaturationWet_Params : noncopyable
{
  const ParameterNumeric<Real> pn{"pn", NO_DEFAULT, 0, NO_LIMIT, "Non-wetting phase pressure"};
  const ParameterNumeric<Real> Sw{"Sw", NO_DEFAULT, 0, 1.0, "Wetting phase saturation"};

  static void checkInputs(PyDict d);
  static PressureNonWet_SaturationWet_Params params;
};

template<class T>
struct PressureNonWet_SaturationWet : public TwoPhaseVariableType<PressureNonWet_SaturationWet<T>>
{
  PressureNonWet_SaturationWet() {}
  PressureNonWet_SaturationWet( const T& pn_, const T& Sw_ )
    : pn(pn_), Sw(Sw_) {}

  explicit PressureNonWet_SaturationWet( PyDict& d )
    : pn( d.get(PressureNonWet_SaturationWet_Params::params.pn) ),
      Sw( d.get(PressureNonWet_SaturationWet_Params::params.Sw) ) {}

  PressureNonWet_SaturationWet& operator=( const std::initializer_list<Real>& s )
  {
    SANS_ASSERT(s.size() == 2);
    auto q = s.begin();
    pn = *q; q++;
    Sw = *q;
    return *this;
  }

  T pn;
  T Sw;
};


//===========================================================================//
struct TwoPhaseVariableTypeParams : noncopyable
{
  struct TwoPhaseVariableOptions
  {
    typedef DictKeyPair ExtractType;
    const ParameterString Variables{"Variables", NO_DEFAULT, "Name of the variable set" };
    const ParameterString& key = Variables;

    const DictOption PressureNonWet_SaturationWet{"PressureNonWet-SaturationWet", PressureNonWet_SaturationWet_Params::checkInputs};

    const std::vector<DictOption> options{PressureNonWet_SaturationWet};
  };
  const ParameterOption<TwoPhaseVariableOptions> StateVector{"StateVector", NO_DEFAULT, "The state vector of variables"};

  static void checkInputs(PyDict d);
  static TwoPhaseVariableTypeParams params;
};

}

#endif //TWOPHASEVARIABLE_H
