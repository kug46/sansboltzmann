// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef CAPILLARYMODEL_H
#define CAPILLARYMODEL_H

// Capillary models

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <iostream>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// A class for representing input parameters for CapillaryModel_None
struct CapillaryModel_None_Params : noncopyable
{
  static void checkInputs(PyDict d);

  static CapillaryModel_None_Params params;
};

//----------------------------------------------------------------------------//
// A class for representing input parameters for CapillaryModel_Linear
struct CapillaryModel_Linear_Params : noncopyable
{
  const ParameterNumeric<Real> pc_max{"pc_max", NO_DEFAULT, 0, NO_LIMIT, "Maximum capillary pressure"};

  static void checkInputs(PyDict d);

  static CapillaryModel_Linear_Params params;
};

//----------------------------------------------------------------------------//
// CapillaryModel:
//
// template parameters:
//   T                        solution DOF data type (e.g. double)
//
// member functions:
//
//   .capPressure                capillary pressure pc(S) - function of phase saturation
//   .capPressureJacobian        jacobian: d(pc)/d(S)
//   .capPressureGradient        gradient: d(pc)/dx and d(pc)/dy
//
//   .dump                    debug dump of private data
//----------------------------------------------------------------------------//


class CapillaryModel_None
{
public:
  CapillaryModel_None_Params& params;

  explicit CapillaryModel_None( const PyDict& d ) :
      params(CapillaryModel_None_Params::params){}

  CapillaryModel_None( const CapillaryModel_None& model ) :
      params(CapillaryModel_None_Params::params){}

  CapillaryModel_None& operator=(const CapillaryModel_None&) { return *this; }

  CapillaryModel_None() : params(CapillaryModel_None_Params::params){}

  ~CapillaryModel_None() {}

  bool hasFluxViscous() const { return false; }

  // Capillary pressure
  template<class T>
  T capPressure( const Real& x, const Real& time, const T& S ) const;
  template<class T>
  T capPressure( const Real& x, const Real& y, const Real& time, const T& S ) const;

  template<class T>
  void capPressureJacobian( const Real& x, const Real& time, const T& S, T& pc_S ) const;
  template<class T>
  void capPressureJacobian( const Real& x, const Real& y, const Real& time, const T& S, T& pc_S ) const;

  template<class T>
  void capPressureHessian( const Real& x, const Real& time, const T& S, T& pc_S2 ) const;
  template<class T>
  void capPressureHessian( const Real& x, const Real& y, const Real& time, const T& S, T& pc_S2 ) const;

  template<class T>
  void capPressureGradient( const Real& x, const Real& time, const T& S, const T& Sx, T& pcx ) const;
  template<class T>
  void capPressureGradient( const Real& x, const Real& y, const Real& time, const T& S, const T& Sx, const T& Sy,
                            T& pcx, T& pcy ) const;

  void dump( std::string indent, std::ostream& out = std::cout ) const;
  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
};


template <class T>
inline T
CapillaryModel_None::capPressure( const Real& x, const Real& time, const T& S) const
{
  return 0.0;
}

template <class T>
inline T
CapillaryModel_None::capPressure( const Real& x, const Real& y, const Real& time, const T& S) const
{
  return 0.0;
}

template <class T>
inline void
CapillaryModel_None::capPressureJacobian( const Real& x, const Real& time, const T& S, T& pc_S ) const
{
  pc_S = 0.0;
}

template <class T>
inline void
CapillaryModel_None::capPressureJacobian( const Real& x, const Real& y, const Real& time, const T& S, T& pc_S ) const
{
  pc_S = 0.0;
}

template <class T>
inline void
CapillaryModel_None::capPressureHessian( const Real& x, const Real& time, const T& S, T& pc_S2 ) const
{
  pc_S2 = 0.0;
}

template <class T>
inline void
CapillaryModel_None::capPressureHessian( const Real& x, const Real& y, const Real& time, const T& S, T& pc_S2 ) const
{
  pc_S2 = 0.0;
}

template <class T>
inline void
CapillaryModel_None::capPressureGradient( const Real& x, const Real& time, const T& S, const T& Sx, T& pcx ) const
{
  pcx = 0.0;
}

template <class T>
inline void
CapillaryModel_None::capPressureGradient( const Real& x, const Real& y, const Real& time, const T& S, const T& Sx, const T& Sy,
                                       T& pcx, T& pcy) const
{
  pcx = 0.0;
  pcy = 0.0;
}

/*-----------------------------------------------------------------------------------------------------------------------*/

class CapillaryModel_Linear
{

// Linear model : pc(S) = pc_max*S

public:
  CapillaryModel_Linear_Params& params;

  explicit CapillaryModel_Linear( const PyDict& d ) :
      params(CapillaryModel_Linear_Params::params),
      pc_max_(d.get(params.pc_max)){}

  explicit CapillaryModel_Linear( Real pc_max ) :
      params(CapillaryModel_Linear_Params::params),
      pc_max_(pc_max)
  {
    SANS_ASSERT( pc_max >= 0 );
  }

  CapillaryModel_Linear( const CapillaryModel_Linear& model ) :
      params( CapillaryModel_Linear_Params::params ),
      pc_max_(model.pc_max_){}

  CapillaryModel_Linear& operator=(const CapillaryModel_Linear& model)
  {
    pc_max_ = model.pc_max_;
    return *this;
  }

  ~CapillaryModel_Linear() {}

  bool hasFluxViscous() const { return (pc_max_ > 0); }
  Real pc_max() const { return pc_max_; }

  // Capillary pressure
  template<class T>
  T capPressure( const Real& x, const Real& time, const T& S ) const;
  template<class T>
  T capPressure( const Real& x, const Real& y, const Real& time, const T& S ) const;

  template<class T>
  void capPressureJacobian( const Real& x, const Real& time, const T& S, T& pc_S ) const;
  template<class T>
  void capPressureJacobian( const Real& x, const Real& y, const Real& time, const T& S, T& pc_S ) const;

  template<class T>
  void capPressureHessian( const Real& x, const Real& time, const T& S, T& pc_S2 ) const;
  template<class T>
  void capPressureHessian( const Real& x, const Real& y, const Real& time, const T& S, T& pc_S2 ) const;

  template<class Tq, class Tg, class T>
  void capPressureGradient( const Real& x, const Real& time,
                            const Tq& S, const Tg& Sx,
                            T& pcx ) const;
  template<class Tq, class Tg, class T>
  void capPressureGradient( const Real& x, const Real& y, const Real& time,
                            const Tq& S, const Tg& Sx, const Tg& Sy,
                            T& pcx, T& pcy ) const;

  void dump( std::string indent, std::ostream& out = std::cout ) const;
  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  Real pc_max_;
};


template <class T>
inline T
CapillaryModel_Linear::capPressure( const Real& x, const Real& time, const T& S) const
{
  return pc_max_*S;
}

template <class T>
inline T
CapillaryModel_Linear::capPressure( const Real& x, const Real& y, const Real& time, const T& S) const
{
  return pc_max_*S;
}

template <class T>
inline void
CapillaryModel_Linear::capPressureJacobian( const Real& x, const Real& time, const T& S, T& pc_S ) const
{
  pc_S = pc_max_;
}

template <class T>
inline void
CapillaryModel_Linear::capPressureJacobian( const Real& x, const Real& y, const Real& time, const T& S, T& pc_S ) const
{
  pc_S = pc_max_;
}

template <class T>
inline void
CapillaryModel_Linear::capPressureHessian( const Real& x, const Real& time, const T& S, T& pc_S2 ) const
{
  pc_S2 = 0.0;
}

template <class T>
inline void
CapillaryModel_Linear::capPressureHessian( const Real& x, const Real& y, const Real& time, const T& S, T& pc_S2 ) const
{
  pc_S2 = 0.0;
}

template <class Tq, class Tg, class T>
inline void
CapillaryModel_Linear::capPressureGradient( const Real& x, const Real& time,
                                            const Tq& S, const Tg& Sx,
                                            T& pcx ) const
{
  //Compute Jacobian
  Tq pc_S;
  capPressureJacobian( x, time, S, pc_S );

  //Apply chain rule for gradient: d(pc)/dx = d(pc)/dS * dS/dx
  pcx = pc_S*Sx;
}

template <class Tq, class Tg, class T>
inline void
CapillaryModel_Linear::capPressureGradient( const Real& x, const Real& y, const Real& time,
                                            const Tq& S, const Tg& Sx, const Tg& Sy,
                                            T& pcx, T& pcy) const
{
  //Compute Jacobian
  Tq pc_S;
  capPressureJacobian( x, y, time, S, pc_S );

  //Apply chain rule for gradient: d(pc)/dx = d(pc)/dS * dS/dx
  pcx = pc_S*Sx;
  pcy = pc_S*Sy;
}

} //namespace SANS

#endif  // CAPILLARYMODEL_H
