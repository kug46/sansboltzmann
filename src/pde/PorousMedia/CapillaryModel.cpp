// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "CapillaryModel.h"

//Instantiate the CheckInput function
//PARAMETER_VECTOR_IMPL( CapillaryModelParams );

namespace SANS
{

void CapillaryModel_None_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  d.checkUnknownInputs(allParams);
}
CapillaryModel_None_Params CapillaryModel_None_Params::params;

void
CapillaryModel_None::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "CapillaryModel_None<T>" << std::endl;
}

/*--------------------------------------------------------------------------------*/

void CapillaryModel_Linear_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.pc_max));
  d.checkUnknownInputs(allParams);
}
CapillaryModel_Linear_Params CapillaryModel_Linear_Params::params;

void
CapillaryModel_Linear::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "CapillaryModel_Linear<T>:" << std::endl;
  out << indent << "  pc_max_ = " << pc_max_ << std::endl;
}


}
