// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "ViscosityModel_Constant.h"

//Instantiate the CheckInput function
//PARAMETER_VECTOR_IMPL( ViscosityModel_Constant_Params );

namespace SANS
{
void ViscosityModel_Constant_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.mu));
  d.checkUnknownInputs(allParams);
}
ViscosityModel_Constant_Params ViscosityModel_Constant_Params::params;

void
ViscosityModel_Constant::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "ViscosityModel_Constant<T>:" << std::endl;
  out << indent << "  mu_ = " << mu_ << std::endl;
}

}
