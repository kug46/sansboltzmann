// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCTwoPhaseLinearized1D.h"
#include "Q1DPrimitive_pnSw.h"

#include "RelPermModel_PowerLaw.h"
#include "CapillaryModel.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{
//===========================================================================//
//
// Parameter classes
//

BCTwoPhaseLinearized1DParams<BCTypeFullState>::BCTwoPhaseLinearized1DParams()
  : StateVector(TwoPhaseVariableTypeParams::params.StateVector)
{}

// cppcheck-suppress passedByValue
void BCTwoPhaseLinearized1DParams<BCTypeFullState>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.StateVector));
  d.checkUnknownInputs(allParams);
}
BCTwoPhaseLinearized1DParams<BCTypeFullState> BCTwoPhaseLinearized1DParams<BCTypeFullState>::params;

//===========================================================================//

typedef PDETwoPhaseLinearized1D<QTypePrimitive_pnSw, RelPermModel_PowerLaw, RelPermModel_PowerLaw,
                                CapillaryModel_Linear> PDE_RelPower_CapLinear;

// Instantiate the BC parameters
BCPARAMETER_INSTANTIATE( BCTwoPhaseLinearized1DVector<PDE_RelPower_CapLinear> )

//===========================================================================//

template <class PDE>
void
BCTwoPhaseLinearized1D<BCTypeFullState, PDE>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCTwoPhaseLinearized1D<BCTypeFullState, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << std::endl;
  out << indent << "  qB_ = " << qB_ << std::endl;
}


// Explicit instantiations
template class BCNone<PhysD1,TraitsSizeTwoPhase<PhysD1>::N>;
template class BCTwoPhaseLinearized1D< BCTypeFullState, PDE_RelPower_CapLinear >;

} //namespace SANS
