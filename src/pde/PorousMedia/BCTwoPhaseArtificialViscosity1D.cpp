// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "Q1DPrimitive_pnSw.h"
#include "TraitsTwoPhaseArtificialViscosity.h"
#include "BCTwoPhaseArtificialViscosity1D.h"

#include "DensityModel.h"
#include "PorosityModel.h"
#include "RelPermModel_PowerLaw.h"
#include "ViscosityModel_Constant.h"
#include "CapillaryModel.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_Source1D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor1D.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{

#if 0
// cppcheck-suppress passedByValue
void BCmitAVSensor1DParams<BCTypeTimeOutflow, BCBaseDummy>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
//  allParams.push_back(d.checkInputs(params.StateVector));
  d.checkUnknownInputs(allParams);
}
BCmitAVSensor1DParams<BCTypeTimeOutflow, BCBaseDummy> BCmitAVSensor1DParams<BCTypeTimeOutflow, BCBaseDummy>::params;
#endif

//===========================================================================//
// Instantiate BC parameters

typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel_Comp, DensityModel_Comp, PorosityModel_Comp,
                            RelPermModel_PowerLaw, RelPermModel_PowerLaw, ViscosityModel_Constant, ViscosityModel_Constant,
                            Real, CapillaryModel_Linear> TraitsModel_RelPower_ViscConst_CapLinear;

typedef PDETwoPhase_ArtificialViscosity1D<TraitsSizeTwoPhaseArtificialViscosity,
                                          TraitsModel_RelPower_ViscConst_CapLinear> PDEBase;

typedef AVSensor_AdvectiveFlux1D_Uniform<TraitsSizeTwoPhaseArtificialViscosity> SensorAdvectiveFlux;
typedef AVSensor_ViscousFlux1D_GenHScale<TraitsSizeTwoPhaseArtificialViscosity> SensorViscousFlux;
typedef AVSensor_Source1D_TwoPhase<PDEBase> SensorSource;

typedef TraitsModelArtificialViscosity<PDEBase, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
typedef BCTwoPhaseArtificialViscosity1DVector<TraitsSizeTwoPhaseArtificialViscosity,TraitsModelAV> BCVector_pnSw;
BCPARAMETER_INSTANTIATE( BCVector_pnSw )

} //namespace SANS
