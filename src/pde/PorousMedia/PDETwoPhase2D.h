// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDETWOPHASE2D_H
#define PDETWOPHASE2D_H

// 2D two-phase flow PDE class

#include <iostream>
#include <string>

#include "tools/SANSnumerics.h"     // Real
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "tools/smoothmath.h"

#include "Topology/Dimension.h"

#include "SourceTwoPhase2D.h"
#include "TwoPhaseVariable.h"

//#define CONSERVATIVE_FLUX_HACK

namespace SANS
{

// forward declare: solution variables interpreter
template <class QType, class CapillaryModel, template <class> class PDETraitsSize>
class Q2D;

//----------------------------------------------------------------------------//
// PDE class: 2-D Two Phase
//
// equations: d/dt( rho_w phi Sw ) - div( rho_w K lambda_w grad(p_w) ) = rho_w q_w
//            d/dt( rho_n phi Sn ) - div( rho_n K lambda_n grad(p_n) ) = rho_n q_n
//
// where:    lambda_w = krw/mu_w
//           lambda_n = krn/mu_n
//
// template parameters:
//   T                        solution DOF data type (e.g. double)
//   QType                    solution variable set (e.g. primitive)
//   DensityModel             density models (for each phase)
//   PorosityModel            porosity model
//   RelPermModel             relative permeability models (for each phase)
//   ViscModel                viscosity models (for each phase)
//   CapillaryModel           capillary pressure model
//
// member functions:
//   .hasFluxAdvectiveTime        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//   .hasFluxViscous             T/F: PDE has viscous flux term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: dU/dQ
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .fluxViscous                viscous fluxes: Fv(Q, QX)
//   .diffusionViscous           viscous diffusion coefficient: d(Fv)/d(UX)
//   .isValidState               T/F: determine if state is physically valid (e.g. Sw > 0)
//   .setDOFFrom                 set from primitive variable array
//----------------------------------------------------------------------------//

template <template <class> class PDETraitsSize, class PDETraitsModel>
class PDETwoPhase2D
{
public:
  typedef PhysD2 PhysDim;
  static const int D = PDETraitsSize<PhysDim>::D; // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N; // total solution variables

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;   // solution/residual arrays

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>; // matrices

  //Extract model traits
  typedef typename PDETraitsModel::QType QType;
  typedef typename PDETraitsModel::DensityModelw DensityModelw;
  typedef typename PDETraitsModel::DensityModeln DensityModeln;
  typedef typename PDETraitsModel::PorosityModel PorosityModel;
  typedef typename PDETraitsModel::RelPermModelw RelPermModelw;
  typedef typename PDETraitsModel::RelPermModeln RelPermModeln;
  typedef typename PDETraitsModel::ViscModelw ViscModelw;
  typedef typename PDETraitsModel::ViscModeln ViscModeln;
  typedef typename PDETraitsModel::RockPermModel RockPermModel;
  typedef typename PDETraitsModel::CapillaryModel CapillaryModel;

  typedef Q2D<QType, CapillaryModel, PDETraitsSize> QInterpreter;  // solution variable interpreter type

  //Source term typedefs
  typedef SourceTwoPhase2D_FixedPressureInflow<PDETraitsSize, PDETraitsModel> SourceTwoPhase2D_FixedPressureInflowType;
  typedef SourceTwoPhase2D_FixedPressureOutflow<PDETraitsSize, PDETraitsModel> SourceTwoPhase2D_FixedPressureOutflowType;
  typedef SourceTwoPhase2D_FixedInflowRate<PDETraitsSize, PDETraitsModel> SourceTwoPhase2D_FixedInflowRateType;
  typedef SourceTwoPhase2D_FixedOutflowRate<PDETraitsSize, PDETraitsModel> SourceTwoPhase2D_FixedOutflowRateType;

  //No source terms
  PDETwoPhase2D( const DensityModelw& rhow, const DensityModeln& rhon, const PorosityModel& phi,
                 const RelPermModelw& krw, const RelPermModeln& krn, const ViscModelw& muw, const ViscModeln& mun,
                 const RockPermModel& K, const CapillaryModel& pc, const Real scalar_visc = 0.0, bool gravity_on = false) :
                   rhow_(rhow), rhon_(rhon), phi_(phi),
                   krw_(krw), krn_(krn), muw_(muw), mun_(mun),
                   K_(K), pc_(pc), qInterpreter_(pc_), scalar_visc_(scalar_visc), gravity_on_(gravity_on) {}

  //With source terms
  PDETwoPhase2D( const DensityModelw& rhow, const DensityModeln& rhon, const PorosityModel& phi,
                 const RelPermModelw& krw, const RelPermModeln& krn, const ViscModelw& muw, const ViscModeln& mun,
                 const RockPermModel& K, const CapillaryModel& pc, const PyDict& PySourceList, const Real scalar_visc = 0.0 ,
                 bool gravity_on = false);

  PDETwoPhase2D( const PDETwoPhase2D& pde ) :
      rhow_(pde.rhow_), rhon_(pde.rhon_), phi_(pde.phi_),
      krw_(pde.krw_), krn_(pde.krn_), muw_(pde.muw_), mun_(pde.mun_),
      K_(pde.K_), pc_(pde.pc_),
      sources_FixedPressureInflow_(pde.sources_FixedPressureInflow_),
      sources_FixedPressureOutflow_(pde.sources_FixedPressureOutflow_),
      sources_FixedInflowRate_(pde.sources_FixedInflowRate_),
      sources_FixedOutflowRate_(pde.sources_FixedOutflowRate_),
      qInterpreter_(pde.qInterpreter_), scalar_visc_(pde.scalar_visc_),
      gravity_on_(pde.gravity_on_) {}

  ~PDETwoPhase2D() {}

  PDETwoPhase2D& operator=( const PDETwoPhase2D& ) = delete;

  // flux components
  bool hasFluxAdvectiveTime() const { return true; }
  bool hasFluxAdvective() const { return gravity_on_; }
  bool hasFluxViscous() const { return true; }
  bool hasSource() const
  {
    return (sources_FixedPressureInflow_ .size() > 0) ||
           (sources_FixedPressureOutflow_.size() > 0) ||
           (sources_FixedInflowRate_.size() > 0) ||
           (sources_FixedOutflowRate_.size() > 0);
  }
  bool hasSourceTrace() const { return false; }
  bool hasSourceLiftedQuantity() const { return false; }
  bool hasForcingFunction() const { return false; }

  // viscous flux is of the form K(q).gradu
  bool fluxViscousLinearInGradient() const { return true; }
  bool needsSolutionGradientforSource() const { return false; }

  const QInterpreter& variableInterpreter() const { return qInterpreter_; }

  // unsteady temporal flux: Ft(Q)
  template <class T, class Tf>
  void fluxAdvectiveTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tf>& ft ) const
  {
    ArrayQ<Tf> ftLocal = 0;
    masterState(x, y, time, q, ftLocal);
    ft += ftLocal;
  }

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <class T, class Tf>
  void jacobianFluxAdvectiveTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& J ) const
  {
    J += DLA::Identity();
  }

  // master state: U(Q)
  template <class T, class Tu>
  void masterState(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tu>& uCons ) const;

  // jacobian of master state wrt q: dU(Q)/dQ
  template<class T>
  void jacobianMasterState(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const;

  // advective flux: F(Q)
  template <class Tq, class Tf>
  void fluxAdvective(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& fy ) const;

  template <class Tq, class Tf>
  void fluxAdvectiveUpwind(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& fn, const Real& scaleFactor = 1 ) const;

  template <class Tq, class Tf>
  void fluxAdvectiveUpwindSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, const Real& ny, const Real& nt, ArrayQ<Tf>& f ) const;

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class T>
  void jacobianFluxAdvective(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q,
            MatrixQ<T>& dfdu, MatrixQ<T>& dgdu ) const
  {
    SANS_DEVELOPER_EXCEPTION("Not implemented.");
  }

  // absolute value of advective flux jacobian: |n . d(F)/d(U)|
  template <class T>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q, const Real& nx, const Real& ny,
      MatrixQ<T>& mtx ) const
  {
    SANS_DEVELOPER_EXCEPTION("Not implemented.");
  }

  // absolute value of advective flux jacobian: |n . d(F)/d(U)|
  template <class T>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q, const Real& nx, const Real& ny, const Real& nt,
      MatrixQ<T>& mtx ) const
  {
    SANS_DEVELOPER_EXCEPTION("Not implemented.");
  }

  // viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const;

  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& fn ) const;

  template <class Tq, class Tg, class Tf>
  void fluxViscous_FEM(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& fn ) const;

  template <class Tq, class Tg, class Tf>
  void fluxViscous_FV_TPFA(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& fn ) const;

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const;

  // space-time viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& ft ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial viscous flux
    fluxViscous(x, y, time, q, qx, qy, fx, fy);
  }

  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qtL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qtR,
      const Real& nx, const Real& ny, const Real& nt,
      ArrayQ<Tf>& f ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial viscous flux
    fluxViscous(x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, f);
  }

  // space-time viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxt,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyt,
      MatrixQ<Tk>& ktx, MatrixQ<Tk>& kty, MatrixQ<Tk>& ktt ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial diffusion matrix
    diffusionViscous(x, y, time, q, qx, qy, kxx, kxy, kyx, kyy);
  }

  // space-time jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& at ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial jacobianFluxViscous
    jacobianFluxViscous(x, y, time, q, qx, qy, ax, ay);
  }

  // space-time strong form viscous fluxes: -d(Fv)/dx - d(Fv)/d(y)
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxt, const ArrayQ<Th>& qyt, const ArrayQ<Th>& qtt,
      ArrayQ<Tf>& strongPDE ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial strongFluxViscous
    strongFluxViscous(x, y, time, q, qx, qy, qxx, qxy, qyy, strongPDE);
  }

  // solution-dependent source: S(X, Q, QX)
  template <class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Ts>& source ) const;

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX)
  template <class Tlq, class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& y, const Real& time,
      const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Ts>& s ) const
  {
    source(x, y, time, q, qx, qy, s); //forward to regular source
  }

  // dual-consistent source
  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& xL, const Real& yL,
      const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const {}

  // trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
  template <class Tq, class Ts>
  void sourceLiftedQuantity(
      const Real& xL, const Real& yL, const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, Ts& s ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class T>
  void jacobianSource(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
      MatrixQ<T>& dsdu ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class T>
  void jacobianSourceAbsoluteValue(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
      MatrixQ<T>& dsdu ) const {}

  // right-hand-side forcing function
  template <class T>
  void forcingFunction( const Real& x, const Real& y, const Real& time, ArrayQ<T>& forcing ) const {}

  // characteristic speed (needed for timestep)
  template<class T>
  void speedCharacteristic( const Real& x, const Real& y, const Real& time,
                            const Real& dx, const Real& dy, const ArrayQ<T>& q, T& speed ) const;

  // characteristic speed
  template<class T, class Ts>
  void speedCharacteristic( const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q, Ts& speed ) const;

  // update fraction needed for physically valid state
  void updateFraction( const Real& x, const Real& y, const Real& time, const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
                       const Real maxChangeFraction, Real& updateFraction ) const;

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const;

  // set from primitive variable array
  template<class T>
  void setDOFFrom( ArrayQ<T>& q, const Real data[], const std::string name[], int nn ) const;

  // set from variable
  template<class T, class Variables>
  void setDOFFrom( ArrayQ<T>& q, const TwoPhaseVariableType<Variables>& data ) const;

  // set from variable
  template<class Variables>
  ArrayQ<Real> setDOFFrom( const TwoPhaseVariableType<Variables>& data ) const
  {
    ArrayQ<Real> q = 0;
    qInterpreter_.setFromPrimitive( q, data.cast() );
    return q;
  }

  // set from variable
  ArrayQ<Real> setDOFFrom( const PyDict& d ) const
  {
    ArrayQ<Real> q = 0;
    DictKeyPair data = d.get(TwoPhaseVariableTypeParams::params.StateVector);

    if ( data == TwoPhaseVariableTypeParams::params.StateVector.PressureNonWet_SaturationWet )
      qInterpreter_.setFromPrimitive( q, PressureNonWet_SaturationWet<Real>(data) );
    else
      SANS_DEVELOPER_EXCEPTION(" Unknown state vector option.");

    return q;
  }

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const;

  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const;

  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const;

  // how many residual equations are we dealing with
  int nMonitor() const;

  std::vector<std::string> derivedQuantityNames() const;

  template<class Tq, class Tg, class Tf>
  void derivedQuantity(
      const int& index, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, Tf& J ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const DensityModelw& rhow_; //Density model for wetting phase
  const DensityModeln& rhon_; //Density model for non-wetting phase

  const PorosityModel& phi_; //Porosity model

  const RelPermModelw& krw_; //Relative permeability model for wetting phase
  const RelPermModeln& krn_; //Relative permeability model for non-wetting phase

  const ViscModelw& muw_; //Viscosity model for wetting phase
  const ViscModeln& mun_; //Viscosity model for non-wetting phase

  const RockPermModel& K_; // Rock permeability model
  const CapillaryModel& pc_; //Capillary pressure model

  // lists of source terms
  std::vector<SourceTwoPhase2D_FixedPressureInflowType> sources_FixedPressureInflow_; //Fixed pressure inflow source terms
  std::vector<SourceTwoPhase2D_FixedPressureOutflowType> sources_FixedPressureOutflow_; //Fixed pressure outflow source terms
  std::vector<SourceTwoPhase2D_FixedInflowRateType> sources_FixedInflowRate_; //Fixed inflow rate source terms
  std::vector<SourceTwoPhase2D_FixedOutflowRateType> sources_FixedOutflowRate_; //Fixed outflow rate source terms

  const QInterpreter qInterpreter_; // solution variable interpreter class

  const Real scalar_visc_; //additional viscosity (used to stabilize single-phase cases, when Sw = 0 or 1)

  const bool gravity_on_;
  static constexpr Real gravity_const = 6.9468E-3; //Units: [psi ft^2 / lb]
};

template <template <class> class PDETraitsSize, class PDETraitsModel>
PDETwoPhase2D<PDETraitsSize, PDETraitsModel>::
PDETwoPhase2D( const DensityModelw& rhow, const DensityModeln& rhon, const PorosityModel& phi,
               const RelPermModelw& krw, const RelPermModeln& krn, const ViscModelw& muw, const ViscModeln& mun,
               const RockPermModel& K, const CapillaryModel& pc, const PyDict& PySourceList, const Real scalar_visc,
               bool gravity_on) :
               rhow_(rhow), rhon_(rhon), phi_(phi),
               krw_(krw), krn_(krn), muw_(muw), mun_(mun),
               K_(K), pc_(pc), qInterpreter_(pc_), scalar_visc_(scalar_visc), gravity_on_(gravity_on)
{
  // Extract the keys from the source list dictionary
  std::vector<std::string> keys = PySourceList.stringKeys();

  for (std::size_t i = 0; i < keys.size(); i++)
  {
    // Create a source dictionary parameter for the given key
    const ParameterDict sourceDictParam{keys[i], NO_DEFAULT, SourceTwoPhase2DParam::checkInputs, "Source dictionary"};

    PyDict sourceDict = PySourceList.get(sourceDictParam); //Extract the source dictionary

    //Extract the sourceType dictionary from previous the source dictionary
    DictKeyPair sourceType = sourceDict.get(SourceTwoPhase2DParam::params.Source);

    if (sourceType == SourceTwoPhase2DParam::params.Source.FixedPressureInflow)
    {
      sources_FixedPressureInflow_.emplace_back(sourceDict, rhow, rhon, krw, krn, muw, mun, K, pc);
    }
    else if (sourceType == SourceTwoPhase2DParam::params.Source.FixedPressureOutflow)
    {
      sources_FixedPressureOutflow_.emplace_back(sourceDict, rhow, rhon, krw, krn, muw, mun, K, pc);
    }
    else if (sourceType == SourceTwoPhase2DParam::params.Source.FixedInflowRate)
    {
      sources_FixedInflowRate_.emplace_back(sourceDict, rhow, rhon, krw, krn, muw, mun, K, pc);
    }
    else if (sourceType == SourceTwoPhase2DParam::params.Source.FixedOutflowRate)
    {
      sources_FixedOutflowRate_.emplace_back(sourceDict, rhow, rhon, krw, krn, muw, mun, K, pc);
    }
    else
      SANS_DEVELOPER_EXCEPTION("PDETwoPhase2D - Unknown source type. Code should not get here if checkInputs was called.");
  }
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tu>
inline void
PDETwoPhase2D<PDETraitsSize, PDETraitsModel>::
masterState(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, ArrayQ<Tu>& uCons ) const
{
  T Sw = 0, Sn = 0, pw = 0, pn = 0;
  qInterpreter_.eval( q, pw, pn, Sw, Sn );

  //Real dummy = 0.0;

  //Evaluate densities
  T rhow = rhow_.density(x, y, time, pw);
  T rhon = rhon_.density(x, y, time, pn);

  //Evaluate porosity
  T phi = phi_.porosity(x, y, time, pn);

  uCons(0) = phi*rhow*Sw;
  uCons(1) = phi*rhon*Sn;
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDETwoPhase2D<PDETraitsSize, PDETraitsModel>::
jacobianMasterState(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
{
#ifdef CONSERVATIVE_FLUX_HACK
  dudq = DLA::Identity(); //Returning identity because diffusionViscous returns d(Fv)/dq instead of d(Fv)/d(UX)
#else
  T Sw = 0, Sn = 0, pw = 0, pn = 0;
  qInterpreter_.eval( q, pw, pn, Sw, Sn );

  //Evaluate jacobians (wrt state vector)
  ArrayQ<T> pw_q = 0, pn_q = 0, Sw_q = 0, Sn_q = 0;
  qInterpreter_.jacobian_pw( q, pw_q );
  qInterpreter_.jacobian_pn( q, pn_q );
  qInterpreter_.jacobian_Sw( q, Sw_q );
  qInterpreter_.jacobian_Sn( q, Sn_q );

  //Real dummy = 0.0;

  //Evaluate densities
  T rhow = rhow_.density(x, y, time, pw);
  T rhon = rhon_.density(x, y, time, pn);
  T rhow_pw, rhon_pn;
  rhow_.densityJacobian(x, y, time, pw, rhow_pw);
  rhon_.densityJacobian(x, y, time, pn, rhon_pn);

  //Evaluate porosity
  T phi = phi_.porosity(x, y, time, pn);
  T phi_pn;
  phi_.porosityJacobian(x, y, time, pn, phi_pn);

  //Chain rules
  ArrayQ<T> rhow_q = rhow_pw*pw_q;
  ArrayQ<T> rhon_q = rhon_pn*pn_q;
  ArrayQ<T> phi_q  = phi_pn *pn_q;

  //Compute Jacobian
  dudq(0,0) = (phi_q[0]*rhow + phi*rhow_q[0])*Sw + phi*rhow*Sw_q[0];
  dudq(0,1) = (phi_q[1]*rhow + phi*rhow_q[1])*Sw + phi*rhow*Sw_q[1];
  dudq(1,0) = (phi_q[0]*rhon + phi*rhon_q[0])*Sn + phi*rhon*Sn_q[0];
  dudq(1,1) = (phi_q[1]*rhon + phi*rhon_q[1])*Sn + phi*rhon*Sn_q[1];
#endif
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDETwoPhase2D<PDETraitsSize, PDETraitsModel>::
fluxAdvective(const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q,
              ArrayQ<Tf>& fx, ArrayQ<Tf>& fy ) const
{
  Tq Sw = 0, Sn = 0, pw = 0, pn = 0;
  qInterpreter_.eval( q, pw, pn, Sw, Sn );

  //Evaluate densities
  Tq rhow = rhow_.density(x, y, time, pw);
  Tq rhon = rhon_.density(x, y, time, pn);

  //Evaluate relative permeabilities
  Tq krw = krw_.relativeperm(Sw);
  Tq krn = krn_.relativeperm(Sn);

  //Evaluate phase viscosities
  Real muw = muw_.viscosity(x, y, time);
  Real mun = mun_.viscosity(x, y, time);

  //Evaluate phase mobilities
  Tq lambda_w = krw/muw;
  Tq lambda_n = krn/mun;

  // Add gravity to y-direction
  Real grav_mask = gravity_const * gravity_on_;
  Tq pwy = rhow*grav_mask;
  Tq pny = rhon*grav_mask;

  //Evaluate rock permeability matrix
  Real Kxx, Kxy, Kyx, Kyy;
  K_.permeability(x, y, time, Kxx, Kxy, Kyx, Kyy);

  fx(0) -= rhow*lambda_w*(Kxy*pwy);
  fx(1) -= rhon*lambda_n*(Kxy*pny);

  fy(0) -= rhow*lambda_w*(Kyy*pwy);
  fy(1) -= rhon*lambda_n*(Kyy*pny);
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDETwoPhase2D<PDETraitsSize, PDETraitsModel>::
fluxAdvectiveUpwind(const Real& x, const Real& y, const Real& time,
                    const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
                    const Real& nx, const Real& ny, ArrayQ<Tf>& fn, const Real& scaleFactor ) const
{

  // Note: Not implementing this function, since the upwinding of the gravity term
  //       is handled by the "upwinding" term in the fluxViscous function.
  //       Correct function of this routine is no-op

#if 0
  Tq SwL = 0, SnL = 0, pwL = 0, pnL = 0;
  Tq SwR = 0, SnR = 0, pwR = 0, pnR = 0;

  qInterpreter_.eval( qL, pwL, pnL, SwL, SnL );
  qInterpreter_.eval( qR, pwR, pnR, SwR, SnR );

  //Evaluate densities
  Tq rhowL = rhow_.density(x, y, time, pwL);
  Tq rhonL = rhon_.density(x, y, time, pnL);
  Tq rhowR = rhow_.density(x, y, time, pwR);
  Tq rhonR = rhon_.density(x, y, time, pnR);

  //Average densities
  Tq rhow = 0.5*(rhowL + rhowR);
  Tq rhon = 0.5*(rhonL + rhonR);

  //Evaluate relative permeabilities
  Tq krwL = krw_.relativeperm(SwL);
  Tq krnL = krn_.relativeperm(SnL);
  Tq krwR = krw_.relativeperm(SwR);
  Tq krnR = krn_.relativeperm(SnR);

  //Evaluate phase viscosities
  Real muw = muw_.viscosity(x, y, time);
  Real mun = mun_.viscosity(x, y, time);

  //Evaluate phase mobilities
  Tq lambda_wL = krwL/muw;
  Tq lambda_nL = krnL/mun;
  Tq lambda_wR = krwR/muw;
  Tq lambda_nR = krnR/mun;

  //Evaluate rock permeability matrix
  Real Kxx, Kxy, Kyx, Kyy;
  K_.permeability(x, y, time, Kxx, Kxy, Kyx, Kyy);

  // Add gravity to y-direction
  Real grav_mask = gravity_const * gravity_on_;
  Tq pwy = rhow*grav_mask;
  Tq pny = rhon*grav_mask;

  //Upwind mobilities
  Tq lambda_w_up = (ny >= 0) ? lambda_wR : lambda_wL;
  Tq lambda_n_up = (ny >= 0) ? lambda_nL : lambda_nR;

  ArrayQ<Tf> fx = 0, fy = 0;
  fx(0) = -rhow*lambda_w_up*(Kxy*pwy);
  fx(1) = -rhon*lambda_n_up*(Kxy*pny);

  fy(0) = -rhow*lambda_w_up*(Kyy*pwy);
  fy(1) = -rhon*lambda_n_up*(Kyy*pny);

  fn += fx*nx + fy*ny;
#endif
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDETwoPhase2D<PDETraitsSize, PDETraitsModel>::
fluxAdvectiveUpwindSpaceTime(const Real& x, const Real& y, const Real& time,
                             const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
                             const Real& nx, const Real& ny, const Real& nt, ArrayQ<Tf>& f) const
{
  //Full temporal upwinding

  //Compute upwinded spatial advective flux
  ArrayQ<Tq> fn_spatial = 0;
  fluxAdvectiveUpwind( x, y, time, qL, qR, nx, ny, fn_spatial );

  //Upwind temporal flux (i.e. conservative flux)
  ArrayQ<Tq> ft = 0;
  if (nt >= 0)
    masterState( x, y, time, qL, ft );
  else
    masterState( x, y, time, qR, ft );

  f += ft*nt + fn_spatial;
}

// viscous flux
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDETwoPhase2D<PDETraitsSize, PDETraitsModel>::
fluxViscous( const Real& x, const Real& y, const Real& time,
             const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
             ArrayQ<Tf>& fx, ArrayQ<Tf>& fy ) const
{
  /*Note: This volume viscous flux term does not include the gravity term
   *      since it is accounted for by the volume fluxAdvective term. */

  typedef typename promote_Surreal<Tq,Tg>::type T;

  Tq Sw = 0, Sn = 0, pw = 0, pn = 0;
  T pwx = 0, pnx = 0;
  T pwy = 0, pny = 0;
  qInterpreter_.eval( q, pw, pn, Sw, Sn );
  qInterpreter_.eval_pwGradient( q, qx, qy, pwx, pwy );
  qInterpreter_.eval_pnGradient( q, qx, qy, pnx, pny );

  //Evaluate densities
  Tq rhow = rhow_.density(x, y, time, pw);
  Tq rhon = rhon_.density(x, y, time, pn);

  //Evaluate relative permeabilities
  Tq krw = krw_.relativeperm(Sw);
  Tq krn = krn_.relativeperm(Sn);

  //Evaluate phase viscosities
  Real muw = muw_.viscosity(x, y, time);
  Real mun = mun_.viscosity(x, y, time);

  //Evaluate phase mobilities
  Tq lambda_w = krw/muw;
  Tq lambda_n = krn/mun;

  //Evaluate rock permeability matrix
  Real Kxx, Kxy, Kyx, Kyy;
  K_.permeability(x, y, time, Kxx, Kxy, Kyx, Kyy);

  fx(0) -= rhow*lambda_w*(Kxx*pwx + Kxy*pwy);
  fx(1) -= rhon*lambda_n*(Kxx*pnx + Kxy*pny);

  fy(0) -= rhow*lambda_w*(Kyx*pwx + Kyy*pwy);
  fy(1) -= rhon*lambda_n*(Kyx*pnx + Kyy*pny);

  //Additional viscosity term for saturation equation (used to stabilize single-phase cases)
  T Swx = 0, Swy = 0;
  qInterpreter_.eval_SwGradient( q, qx, qy, Swx, Swy );

  fx(0) -=  rhow*scalar_visc_*Swx;
  fx(1) -= -rhon*scalar_visc_*Swx;

  fy(0) -=  rhow*scalar_visc_*Swy;
  fy(1) -= -rhon*scalar_visc_*Swy;
}

// viscous flux: normal flux with left and right states
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDETwoPhase2D<PDETraitsSize, PDETraitsModel>::
fluxViscous(const Real& x, const Real& y, const Real& time,
            const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
            const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
            const Real& nx, const Real& ny, ArrayQ<Tf>& fn ) const
{
#ifdef USE_FV_TPFA
  fluxViscous_FV_TPFA(x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, fn);
#else
  fluxViscous_FEM(x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, fn);
#endif
}

// viscous flux for finite element methods: normal flux with left and right states
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDETwoPhase2D<PDETraitsSize, PDETraitsModel>::
fluxViscous_FEM(const Real& x, const Real& y, const Real& time,
                const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
                const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
                const Real& nx, const Real& ny, ArrayQ<Tf>& fn ) const
{
  typedef typename promote_Surreal<Tq,Tg>::type T;

  Tq SwL = 0, SnL = 0, pwL = 0, pnL = 0;
  Tq SwR = 0, SnR = 0, pwR = 0, pnR = 0;
  T pwxL = 0, pnxL = 0, pwyL = 0, pnyL = 0;
  T pwxR = 0, pnxR = 0, pwyR = 0, pnyR = 0;

  qInterpreter_.eval( qL, pwL, pnL, SwL, SnL );
  qInterpreter_.eval( qR, pwR, pnR, SwR, SnR );
  qInterpreter_.eval_pwGradient( qL, qxL, qyL, pwxL, pwyL );
  qInterpreter_.eval_pnGradient( qL, qxL, qyL, pnxL, pnyL );
  qInterpreter_.eval_pwGradient( qR, qxR, qyR, pwxR, pwyR );
  qInterpreter_.eval_pnGradient( qR, qxR, qyR, pnxR, pnyR );

  //Evaluate densities
  Tq rhowL = rhow_.density(x, y, time, pwL);
  Tq rhonL = rhon_.density(x, y, time, pnL);
  Tq rhowR = rhow_.density(x, y, time, pwR);
  Tq rhonR = rhon_.density(x, y, time, pnR);

  //Evaluate relative permeabilities
  Tq krwL = krw_.relativeperm(SwL);
  Tq krnL = krn_.relativeperm(SnL);
  Tq krwR = krw_.relativeperm(SwR);
  Tq krnR = krn_.relativeperm(SnR);

  //Evaluate phase viscosities
  Real muw = muw_.viscosity(x, y, time);
  Real mun = mun_.viscosity(x, y, time);

  //Evaluate phase mobilities
  Tq lambda_wL = krwL/muw;
  Tq lambda_nL = krnL/mun;
  Tq lambda_wR = krwR/muw;
  Tq lambda_nR = krnR/mun;

  //Evaluate rock permeability matrix
  Real Kxx, Kxy, Kyx, Kyy;
  K_.permeability(x, y, time, Kxx, Kxy, Kyx, Kyy);

  ArrayQ<Tf> fxL = 0, fxR = 0;
  ArrayQ<Tf> fyL = 0, fyR = 0;

  // Add gravity to y-direction
  Real grav_mask = gravity_const * gravity_on_;
  pwyL += rhowL*grav_mask;
  pwyR += rhowR*grav_mask;
  pnyL += rhonL*grav_mask;
  pnyR += rhonR*grav_mask;

  fxL(0) -= rhowL*lambda_wL*(Kxx*pwxL + Kxy*pwyL);
  fxL(1) -= rhonL*lambda_nL*(Kxx*pnxL + Kxy*pnyL);
  fxR(0) -= rhowR*lambda_wR*(Kxx*pwxR + Kxy*pwyR);
  fxR(1) -= rhonR*lambda_nR*(Kxx*pnxR + Kxy*pnyR);

  fyL(0) -= rhowL*lambda_wL*(Kyx*pwxL + Kyy*pwyL);
  fyL(1) -= rhonL*lambda_nL*(Kyx*pnxL + Kyy*pnyL);
  fyR(0) -= rhowR*lambda_wR*(Kyx*pwxR + Kyy*pwyR);
  fyR(1) -= rhonR*lambda_nR*(Kyx*pnxR + Kyy*pnyR);

  //Additional viscosity term for saturation equation (used to stabilize single-phase cases)
  T SwxL = 0, SwyL = 0;
  T SwxR = 0, SwyR = 0;
  qInterpreter_.eval_SwGradient( qL, qxL, qyL, SwxL, SwyL );
  qInterpreter_.eval_SwGradient( qR, qxR, qyR, SwxR, SwyR );

  fxL(0) -=  rhowL*scalar_visc_*SwxL;
  fxL(1) -= -rhonL*scalar_visc_*SwxL;
  fxR(0) -=  rhowR*scalar_visc_*SwxR;
  fxR(1) -= -rhonR*scalar_visc_*SwxR;

  fyL(0) -=  rhowL*scalar_visc_*SwyL;
  fyL(1) -= -rhonL*scalar_visc_*SwyL;
  fyR(0) -=  rhowR*scalar_visc_*SwyR;
  fyR(1) -= -rhonR*scalar_visc_*SwyR;

  //Average normal flux
  fn += 0.5*(fxL + fxR)*nx + 0.5*(fyL + fyR)*ny;

  //Add upwinding

  //Evaluate relative permeability jacobians
  Tq krwL_SwL, krnL_SnL;
  Tq krwR_SwR, krnR_SnR;
  krw_.relativepermJacobian(SwL, krwL_SwL);
  krn_.relativepermJacobian(SnL, krnL_SnL);
  krw_.relativepermJacobian(SwR, krwR_SwR);
  krn_.relativepermJacobian(SnR, krnR_SnR);

  Real dSn_dSw = -1;
  Tq krnL_SwL = krnL_SnL * dSn_dSw;
  Tq krnR_SwR = krnR_SnR * dSn_dSw;

  //Evaluate phase mobility jacobians
  Tq lambda_wL_SwL = krwL_SwL/muw;
  Tq lambda_nL_SwL = krnL_SwL/mun;
  Tq lambda_wR_SwR = krwR_SwR/muw;
  Tq lambda_nR_SwR = krnR_SwR/mun;

  //Upwinding term
  T KgradpwL = (Kxx*pwxL + Kxy*pwyL)*nx + (Kyx*pwxL + Kyy*pwyL)*ny;
  T KgradpwR = (Kxx*pwxR + Kxy*pwyR)*nx + (Kyx*pwxR + Kyy*pwyR)*ny;

  T KgradpnL = (Kxx*pnxL + Kxy*pnyL)*nx + (Kyx*pnxL + Kyy*pnyL)*ny;
  T KgradpnR = (Kxx*pnxR + Kxy*pnyR)*nx + (Kyx*pnxR + Kyy*pnyR)*ny;

#if 1
  T cL = (lambda_wL_SwL*lambda_nL*KgradpwL - lambda_wL*lambda_nL_SwL*KgradpnL) / (lambda_wL + lambda_nL);
  T cR = (lambda_wR_SwR*lambda_nR*KgradpwR - lambda_wR*lambda_nR_SwR*KgradpnR) / (lambda_wR + lambda_nR);
#else //from linearized analysis which assumes gradSw = 0
  T cL = (lambda_wL_SwL*lambda_nL - lambda_wL*lambda_nL_SwL) / (lambda_wL + lambda_nL) * KgradpnL;
  T cR = (lambda_wR_SwR*lambda_nR - lambda_wR*lambda_nR_SwR) / (lambda_wR + lambda_nR) * KgradpnR;
#endif

  T rw_cL = rhowL * cL;
  T rw_cR = rhowR * cR;
  T rn_cL = rhonL * cL;
  T rn_cR = rhonR * cR;

  const Real eps = 1e-3;
  const Real alpha = 40;
  T cw_max = smoothmax( smoothabs0(rw_cL, eps), smoothabs0(rw_cR, eps), alpha );
  T cn_max = smoothmax( smoothabs0(rn_cL, eps), smoothabs0(rn_cR, eps), alpha );
//  T cw_max = max( smoothabs0(rw_cL, eps), smoothabs0(rw_cR, eps) );
//  T cn_max = max( smoothabs0(rn_cL, eps), smoothabs0(rn_cR, eps) );

  fn(0) += 0.5 * cw_max * (SwL - SwR);
  fn(1) -= 0.5 * cn_max * (SwL - SwR);
}

// viscous flux for finite volume method (TPFA): normal flux with left and right states
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDETwoPhase2D<PDETraitsSize, PDETraitsModel>::
fluxViscous_FV_TPFA(const Real& x, const Real& y, const Real& time,
                    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
                    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
                    const Real& nx, const Real& ny, ArrayQ<Tf>& fn ) const
{
  typedef typename promote_Surreal<Tq,Tg>::type T;

  Tq SwL = 0, SnL = 0, pwL = 0, pnL = 0;
  Tq SwR = 0, SnR = 0, pwR = 0, pnR = 0;
  T pwxL = 0, pnxL = 0, pwyL = 0, pnyL = 0;
  T pwxR = 0, pnxR = 0, pwyR = 0, pnyR = 0;

  qInterpreter_.eval( qL, pwL, pnL, SwL, SnL );
  qInterpreter_.eval( qR, pwR, pnR, SwR, SnR );
  qInterpreter_.eval_pwGradient( qL, qxL, qyL, pwxL, pwyL );
  qInterpreter_.eval_pnGradient( qL, qxL, qyL, pnxL, pnyL );
  qInterpreter_.eval_pwGradient( qR, qxR, qyR, pwxR, pwyR );
  qInterpreter_.eval_pnGradient( qR, qxR, qyR, pnxR, pnyR );

  //Evaluate densities
  Tq rhowL = rhow_.density(x, y, time, pwL);
  Tq rhonL = rhon_.density(x, y, time, pnL);
  Tq rhowR = rhow_.density(x, y, time, pwR);
  Tq rhonR = rhon_.density(x, y, time, pnR);

  //Evaluate relative permeabilities
  Tq krwL = krw_.relativeperm(SwL);
  Tq krnL = krn_.relativeperm(SnL);
  Tq krwR = krw_.relativeperm(SwR);
  Tq krnR = krn_.relativeperm(SnR);

  //Evaluate phase viscosities
  Real muw = muw_.viscosity(x, y, time);
  Real mun = mun_.viscosity(x, y, time);

  //Evaluate phase mobilities
  Tq lambda_wL = krwL/muw;
  Tq lambda_nL = krnL/mun;
  Tq lambda_wR = krwR/muw;
  Tq lambda_nR = krnR/mun;

  //Evaluate rock permeability matrix
  Real Kxx, Kxy, Kyx, Kyy;
  K_.permeability(x, y, time, Kxx, Kxy, Kyx, Kyy);

  ArrayQ<Tf> fxL = 0, fxR = 0;
  ArrayQ<Tf> fyL = 0, fyR = 0;

  // Add gravity to y-direction
  Real grav_mask = gravity_const * gravity_on_;
  pwyL += rhowL*grav_mask;
  pwyR += rhowR*grav_mask;
  pnyL += rhonL*grav_mask;
  pnyR += rhonR*grav_mask;

  //Mobility upwinding
  if (0.5*((pwxL + pwxR)*nx + (pwyL + pwyR)*ny) >= 0.0)
  {
    fxL(0) -= rhowL*lambda_wR*(Kxx*pwxL + Kxy*pwyL);
    fxR(0) -= rhowR*lambda_wR*(Kxx*pwxR + Kxy*pwyR);

    fyL(0) -= rhowL*lambda_wR*(Kyx*pwxL + Kyy*pwyL);
    fyR(0) -= rhowR*lambda_wR*(Kyx*pwxR + Kyy*pwyR);
  }
  else
  {
    fxL(0) -= rhowL*lambda_wL*(Kxx*pwxL + Kxy*pwyL);
    fxR(0) -= rhowR*lambda_wL*(Kxx*pwxR + Kxy*pwyR);

    fyL(0) -= rhowL*lambda_wL*(Kyx*pwxL + Kyy*pwyL);
    fyR(0) -= rhowR*lambda_wL*(Kyx*pwxR + Kyy*pwyR);
  }

  if (0.5*((pnxL + pnxR)*nx + (pnyL + pnyR)*ny) >= 0.0)
  {
    fxL(1) -= rhonL*lambda_nR*(Kxx*pnxL + Kxy*pnyL);
    fxR(1) -= rhonR*lambda_nR*(Kxx*pnxR + Kxy*pnyR);

    fyL(1) -= rhonL*lambda_nR*(Kyx*pnxL + Kyy*pnyL);
    fyR(1) -= rhonR*lambda_nR*(Kyx*pnxR + Kyy*pnyR);
  }
  else
  {
    fxL(1) -= rhonL*lambda_nL*(Kxx*pnxL + Kxy*pnyL);
    fxR(1) -= rhonR*lambda_nL*(Kxx*pnxR + Kxy*pnyR);

    fyL(1) -= rhonL*lambda_nL*(Kyx*pnxL + Kyy*pnyL);
    fyR(1) -= rhonR*lambda_nL*(Kyx*pnxR + Kyy*pnyR);
  }

  //Additional viscosity term for saturation equation (used to stabilize single-phase cases)
  T SwxL = 0, SwyL = 0;
  T SwxR = 0, SwyR = 0;
  qInterpreter_.eval_SwGradient( qL, qxL, qyL, SwxL, SwyL );
  qInterpreter_.eval_SwGradient( qR, qxR, qyR, SwxR, SwyR );

  fxL(0) -=  rhowL*scalar_visc_*SwxL;
  fxL(1) -= -rhonL*scalar_visc_*SwxL;
  fxR(0) -=  rhowR*scalar_visc_*SwxR;
  fxR(1) -= -rhonR*scalar_visc_*SwxR;

  fyL(0) -=  rhowL*scalar_visc_*SwyL;
  fyL(1) -= -rhonL*scalar_visc_*SwyL;
  fyR(0) -=  rhowR*scalar_visc_*SwyR;
  fyR(1) -= -rhonR*scalar_visc_*SwyR;

  //Average normal flux
  fn += 0.5*(fxL + fxR)*nx + 0.5*(fyL + fyR)*ny;
}

// viscous diffusion matrix: d(Fv)/d(UX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tk>
inline void
PDETwoPhase2D<PDETraitsSize, PDETraitsModel>::
diffusionViscous( const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
                  MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
                  MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const
{
  Tq Sw = 0, Sn = 0, pw = 0, pn = 0;
  qInterpreter_.eval( q, pw, pn, Sw, Sn );

  //Evaluate densities
  Tq rhow = rhow_.density(x, time, pw);
  Tq rhon = rhon_.density(x, time, pn);

  //Evaluate relative permeabilities
  Tq krw = krw_.relativeperm(Sw);
  Tq krn = krn_.relativeperm(Sn);

  //Evaluate phase viscosities
  Real muw = muw_.viscosity(x, time);
  Real mun = mun_.viscosity(x, time);

  //Evaluate phase mobilities
  Tq lambda_w = krw/muw;
  Tq lambda_n = krn/mun;

  //Evaluate rock permeability matrix
  Real Kxx, Kxy, Kyx, Kyy;
  K_.permeability(x, y, time, Kxx, Kxy, Kyx, Kyy);

  //Evaluate pressure jacobians (wrt state vector)
  ArrayQ<Tq> pw_q, pn_q;
  qInterpreter_.jacobian_pw( q, pw_q );
  qInterpreter_.jacobian_pn( q, pn_q );

//  f(0) -= rhow*lambda_w*(Kxx*pwx + Kxy*pwy);
//  f(1) -= rhon*lambda_n*(Kxx*pnx + Kxy*pny);
//
//  g(0) -= rhow*lambda_w*(Kyx*pwx + Kyy*pwy);
//  g(1) -= rhon*lambda_n*(Kyx*pnx + Kyy*pny);

  // d(Fv)/d(grad q)
  MatrixQ<Tq> Fvx_qx;
  Fvx_qx(0,0) = rhow*lambda_w*(Kxx*pw_q(0));
  Fvx_qx(0,1) = rhow*lambda_w*(Kxx*pw_q(1));
  Fvx_qx(1,0) = rhon*lambda_n*(Kxx*pn_q(0));
  Fvx_qx(1,1) = rhon*lambda_n*(Kxx*pn_q(1));

  MatrixQ<Tq> Fvx_qy;
  Fvx_qy(0,0) = rhow*lambda_w*(Kxy*pw_q(0));
  Fvx_qy(0,1) = rhow*lambda_w*(Kxy*pw_q(1));
  Fvx_qy(1,0) = rhon*lambda_n*(Kxy*pn_q(0));
  Fvx_qy(1,1) = rhon*lambda_n*(Kxy*pn_q(1));

  MatrixQ<Tq> Fvy_qx;
  Fvy_qx(0,0) = rhow*lambda_w*(Kyx*pw_q(0));
  Fvy_qx(0,1) = rhow*lambda_w*(Kyx*pw_q(1));
  Fvy_qx(1,0) = rhon*lambda_n*(Kyx*pn_q(0));
  Fvy_qx(1,1) = rhon*lambda_n*(Kyx*pn_q(1));

  MatrixQ<Tq> Fvy_qy;
  Fvy_qy(0,0) = rhow*lambda_w*(Kyy*pw_q(0));
  Fvy_qy(0,1) = rhow*lambda_w*(Kyy*pw_q(1));
  Fvy_qy(1,0) = rhon*lambda_n*(Kyy*pn_q(0));
  Fvy_qy(1,1) = rhon*lambda_n*(Kyy*pn_q(1));

#ifdef CONSERVATIVE_FLUX_HACK
  kxx = Fvx_qx;
  kxy = Fvx_qy;
  kyx = Fvy_qx;
  kyy = Fvy_qy;
#else
  MatrixQ<Tq> dudq;
  jacobianMasterState(x, y, time, q, dudq);

  Tq det = dudq(0,0)*dudq(1,1) - dudq(0,1)*dudq(1,0);

  if (fabs(det) < 1e-15)
  {
    std::cout << "det(dudq) = " << det << ", q = " << q << std::endl;
    SANS_DEVELOPER_EXCEPTION("Conservative flux Jacobian is singular!"); // det = %e, q = [%e, %e]", det, q[0], q[1]);
  }

  MatrixQ<Tq> inv_dudq;
  inv_dudq(0,0) =  dudq(1,1)/det;
  inv_dudq(0,1) = -dudq(0,1)/det;
  inv_dudq(1,0) = -dudq(1,0)/det;
  inv_dudq(1,1) =  dudq(0,0)/det;

  //d(Fv)/d(grad u) = d(Fv)/d(grad q) * inverse(du/dq)
  kxx += Fvx_qx*inv_dudq;
  kxy += Fvx_qy*inv_dudq;
  kyx += Fvy_qx*inv_dudq;
  kyy += Fvy_qy*inv_dudq;
#endif
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Ts>
inline void
PDETwoPhase2D<PDETraitsSize, PDETraitsModel>::
source(const Real& x, const Real& y, const Real& time,
       const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
       ArrayQ<Ts>& source ) const
{
  //Loop over all fixed pressure inflow source terms
  for (int i = 0; i < (int) sources_FixedPressureInflow_.size(); i++)
    sources_FixedPressureInflow_[i].source(x, y, time, q, qx, qy, source);

  //Loop over all fixed pressure outflow source terms
  for (int i = 0; i < (int) sources_FixedPressureOutflow_.size(); i++)
    sources_FixedPressureOutflow_[i].source(x, y, time, q, qx, qy, source);

  //Loop over all fixed inflow rate source terms
  for (int i = 0; i < (int) sources_FixedInflowRate_.size(); i++)
    sources_FixedInflowRate_[i].source(x, y, time, q, qx, qy, source);

  //Loop over all fixed outflow rate source terms
  for (int i = 0; i < (int) sources_FixedOutflowRate_.size(); i++)
    sources_FixedOutflowRate_[i].source(x, y, time, q, qx, qy, source);
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDETwoPhase2D<PDETraitsSize, PDETraitsModel>::
speedCharacteristic( const Real& x, const Real& y, const Real& time,
                     const Real& dx, const Real& dy, const ArrayQ<T>& q, T& speed ) const
{
  T Sw = 0, Sn = 0, pw = 0, pn = 0;
  qInterpreter_.eval( q, pw, pn, Sw, Sn );

  //Evaluate relative permeabilities
  T krw = krw_.relativeperm(Sw);
  T krn = krn_.relativeperm(Sn);

  //Evaluate phase viscosities
  Real muw = muw_.viscosity(x, y, time);
  Real mun = mun_.viscosity(x, y, time);

  //Evaluate phase mobilities
  T lambda_w = krw/muw;
  T lambda_n = krn/mun;

  //Evaluate relative permeability jacobians
  T krw_Sw, krn_Sn;
  krw_.relativepermJacobian(Sw, krw_Sw);
  krn_.relativepermJacobian(Sn, krn_Sn);

  Real dSn_dSw = -1;
  T krn_Sw = krn_Sn * dSn_dSw;

  //Evaluate phase mobility jacobians
  T lambda_w_Sw = krw_Sw/muw;
  T lambda_n_Sw = krn_Sw/mun;

  //Evaluate rock permeability matrix
  Real Kxx, Kxy, Kyx, Kyy;
  K_.permeability(x, y, time, Kxx, Kxy, Kyx, Kyy);

  // Hard code pnx and pny
  T pnx = 0.0;
  T rhow = rhow_.density(x, y, time, pw);
  T pny = -gravity_const*rhow;

  T Kgradp_x = Kxx*pnx + Kxy*pny;
  T Kgradp_y = Kyx*pnx + Kyy*pny;
  T Kgradp_norm = sqrt(Kgradp_x*Kgradp_x + Kgradp_y*Kgradp_y);
  T c = (lambda_w_Sw*lambda_n - lambda_w*lambda_n_Sw) / (lambda_w + lambda_n) * Kgradp_norm;

  speed = fabs(c) + 1.0e-5;
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Ts>
inline void
PDETwoPhase2D<PDETraitsSize, PDETraitsModel>::
speedCharacteristic( const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q, Ts& speed ) const
{
  // SANS_DEVELOPER_EXCEPTION("PDETwoPhase2D::speedCharacteristic - Not implemented yet.");
  speedCharacteristic(x, y, time, 1.0, 1.0, q, speed);
}


// update fraction needed for physically valid state
template <template <class> class PDETraitsSize, class PDETraitsModel>
void
PDETwoPhase2D<PDETraitsSize, PDETraitsModel>::
updateFraction( const Real& x, const Real& y, const Real& time, const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
                const Real maxChangeFraction, Real& updateFraction ) const
{
  SANS_DEVELOPER_EXCEPTION("Not implemented");
}


// is state physically valid
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline bool
PDETwoPhase2D<PDETraitsSize, PDETraitsModel>::
isValidState( const ArrayQ<Real>& q ) const
{
#ifndef USE_FV_TPFA
  return (q(0) > 0);
#else
  return qInterpreter_.isValidState(q);
#endif
}

// set from primitive variable array
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDETwoPhase2D<PDETraitsSize, PDETraitsModel>::
setDOFFrom( ArrayQ<T>& q, const Real data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn >= N);
  qInterpreter_.setFromPrimitive( q, data, name, nn );
}

// set from variables
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Variables>
inline void
PDETwoPhase2D<PDETraitsSize, PDETraitsModel>::
setDOFFrom( ArrayQ<T>& q, const TwoPhaseVariableType<Variables>& data ) const
{
  qInterpreter_.setFromPrimitive( q, data.cast() );
}

// interpret residuals of the solution variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDETwoPhase2D<PDETraitsSize, PDETraitsModel>::
interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
{
  int nOut = rsdPDEOut.m();

  SANS_ASSERT(nOut == N);
  for (int i = 0; i < nOut; i++)
    rsdPDEOut[i] = rsdPDEIn[i];
}

// interpret residuals of the gradients in the solution variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDETwoPhase2D<PDETraitsSize, PDETraitsModel>::
interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const
{
  //TEMPORARY SOLUTION FOR AUX VARIABLE
  int nOut = rsdAuxOut.m();

  SANS_ASSERT(nOut == N);
  for (int i = 0; i < nOut; i++)
    rsdAuxOut[i] = rsdAuxIn[i];
}

// interpret residuals at the boundary (should forward to BCs)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDETwoPhase2D<PDETraitsSize, PDETraitsModel>::
interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const
{
  int nOut = rsdBCOut.m();

  SANS_ASSERT(nOut == N);
  for (int i = 0; i < nOut; i++)
    rsdBCOut[i] = rsdBCIn[i];
}

// how many residual equations are we dealing with
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline int
PDETwoPhase2D<PDETraitsSize, PDETraitsModel>::nMonitor() const
{
  return N;
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
inline std::vector<std::string>
PDETwoPhase2D<PDETraitsSize, PDETraitsModel>::
derivedQuantityNames() const
{
  return {"pc", "phi", "Kxx", "Kyy"};

}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template<class Tq, class Tg, class Tf>
inline void
PDETwoPhase2D<PDETraitsSize, PDETraitsModel>::
derivedQuantity(const int& index, const Real& x, const Real& y, const Real& time,
                const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, Tf& J ) const
{
  switch (index)
  {
  case 0:
  {
    Tq Sw = 0, Sn = 0, pw = 0, pn = 0;
    qInterpreter_.eval( q, pw, pn, Sw, Sn );
    J = pn - pw;
    break;
  }
  case 1:
  {
    Tq Sw = 0, Sn = 0, pw = 0, pn = 0;
    qInterpreter_.eval( q, pw, pn, Sw, Sn );
    J = phi_.porosity(x, y, time, pn);
    break;
  }
  case 2:
  {
    Real Kxx, Kxy, Kyx, Kyy;
    K_.permeability(x, y, time, Kxx, Kxy, Kyx, Kyy);
    J = Kxx;
    break;
  }
  case 3:
  {
    Real Kxx, Kxy, Kyx, Kyy;
    K_.permeability(x, y, time, Kxx, Kxy, Kyx, Kyy);
    J = Kyy;
    break;
  }

  default:
    SANS_DEVELOPER_EXCEPTION("PDETwoPhase2D::derivedQuantity - Invalid index!");
  }
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
inline void
PDETwoPhase2D<PDETraitsSize, PDETraitsModel>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  std::string indent2(indentSize+2, ' ');

  out << indent << "PDETwoPhase2D<QType, DensityModelw, DensityModeln, PorosityModel, RelPermModelw, RelPermModeln, "
                   "ViscModelw, ViscModeln, RockPermModel, CapillaryModel>:" << std::endl;
  out << indent2 << "qInterpret_ = " << std::endl;
  qInterpreter_.dump(indentSize+4, out);
  out << indent2 << "rhow_ = " << std::endl;
  rhow_.dump(indentSize+4, out);
  out << indent2 << "rhon_ = " << std::endl;
  rhon_.dump(indentSize+4, out);
  out << indent2 << "phi_ = " << std::endl;
  phi_.dump(indentSize+4, out);
  out << indent2 << "krw_ = " << std::endl;
  krw_.dump(indentSize+4, out);
  out << indent2 << "krn_ = " << std::endl;
  krn_.dump(indentSize+4, out);
  out << indent2 << "muw_ = " << std::endl;
  muw_.dump(indentSize+4, out);
  out << indent2 << "mun_ = " << std::endl;
  mun_.dump(indentSize+4, out);
  out << indent2 << "K_ = " << std::endl;
  K_.dump(indentSize+4, out);
  out << indent2 << "pc_ = " << std::endl;
  pc_.dump(indentSize+4, out);
}


} //namespace SANS

#endif  // PDETWOPHASE2D_H
