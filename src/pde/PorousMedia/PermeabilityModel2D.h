// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PERMEABILITYMODEL2D_H
#define PERMEABILITYMODEL2D_H

// 2D rock permeability models

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <iostream>
#include <array>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"

#include "CartTable.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// PermeabilityModel2D:
//
// template parameters:
//   T                        solution DOF data type (e.g. double)
//
// member functions:
//   .permeability            get the rock permeability at a given location/time
//
//   .dump                    debug dump of private data
//----------------------------------------------------------------------------//


//----------------------------------------------------------------------------//
// PermeabilityModel2D_Constant:  constant permeability

//----------------------------------------------------------------------------//
// A class for representing input parameters for PermeabilityModel2D_Constant_Params
struct PermeabilityModel2D_Constant_Params : noncopyable
{
  const ParameterNumeric<Real> Kxx{"Kxx", NO_DEFAULT, 0, NO_LIMIT, "Kxx"};
  const ParameterNumeric<Real> Kxy{"Kxy", NO_DEFAULT, 0, NO_LIMIT, "Kxy"};
  const ParameterNumeric<Real> Kyx{"Kyx", NO_DEFAULT, 0, NO_LIMIT, "Kyx"};
  const ParameterNumeric<Real> Kyy{"Kyy", NO_DEFAULT, 0, NO_LIMIT, "Kyy"};

  static void checkInputs(PyDict d);

  static PermeabilityModel2D_Constant_Params params;
};

class PermeabilityModel2D_Constant
{
public:
  PermeabilityModel2D_Constant_Params& params;

  explicit PermeabilityModel2D_Constant( const PyDict& d ) :
      params(PermeabilityModel2D_Constant_Params::params),
      Kxx_(d.get(params.Kxx)), Kxy_(d.get(params.Kxy)),
      Kyx_(d.get(params.Kyx)), Kyy_(d.get(params.Kyy)){}

  PermeabilityModel2D_Constant( const PermeabilityModel2D_Constant& model ) :
      params( PermeabilityModel2D_Constant_Params::params ),
      Kxx_(model.Kxx_), Kxy_(model.Kxy_), Kyx_(model.Kyx_), Kyy_(model.Kyy_){}

  PermeabilityModel2D_Constant( Real Kxx, Real Kxy, Real Kyx, Real Kyy ) :
      params(PermeabilityModel2D_Constant_Params::params),
      Kxx_(Kxx), Kxy_(Kxy), Kyx_(Kyx), Kyy_(Kyy)
  {
    SANS_ASSERT( Kxx_ >= 0 );
    SANS_ASSERT( Kxy_ >= 0 );
    SANS_ASSERT( Kyx_ >= 0 );
    SANS_ASSERT( Kyy_ >= 0 );
  }

  ~PermeabilityModel2D_Constant() {}

  void permeability( const Real& x, const Real& y, const Real& time,
                     Real& Kxx, Real& Kxy, Real& Kyx, Real& Kyy) const
  {
    Kxx = Kxx_;
    Kxy = Kxy_;
    Kyx = Kyx_;
    Kyy = Kyy_;
  }

  void dump( std::string indent, std::ostream& out = std::cout ) const;
  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  Real Kxx_, Kxy_, Kyx_, Kyy_;
};

#if 0
//----------------------------------------------------------------------------//
// A class for representing input parameters for PermeabilityModel2D_RectBlock_Params
struct PermeabilityModel2D_RectBlock_Params : noncopyable
{
  const ParameterNumeric<Real> Kxx_ref{"Kxx_ref", NO_DEFAULT, 0, NO_LIMIT, "Background Kxx value"};
  const ParameterNumeric<Real> Kxy_ref{"Kxy_ref", NO_DEFAULT, 0, NO_LIMIT, "Background Kxy value"};
  const ParameterNumeric<Real> Kyx_ref{"Kyx_ref", NO_DEFAULT, 0, NO_LIMIT, "Background Kyx value"};
  const ParameterNumeric<Real> Kyy_ref{"Kyy_ref", NO_DEFAULT, 0, NO_LIMIT, "Background Kyy value"};

  const ParameterNumericList<Real> blocklist_Kxx{"blocklist_Kxx", EMPTY_LIST, 0, NO_LIMIT, "Kxx values for each square block"};
  const ParameterNumericList<Real> blocklist_Kxy{"blocklist_Kxy", EMPTY_LIST, 0, NO_LIMIT, "Kxy values for each square block"};
  const ParameterNumericList<Real> blocklist_Kyx{"blocklist_Kyx", EMPTY_LIST, 0, NO_LIMIT, "Kyx values for each square block"};
  const ParameterNumericList<Real> blocklist_Kyy{"blocklist_Kyy", EMPTY_LIST, 0, NO_LIMIT, "Kyy values for each square block"};

  const ParameterNumericList<Real> blocklist_xmin{"blocklist_xmin", EMPTY_LIST, NO_RANGE, "Min x-coordinates of each square block"};
  const ParameterNumericList<Real> blocklist_xmax{"blocklist_xmax", EMPTY_LIST, NO_RANGE, "Max x-coordinates of each square block"};
  const ParameterNumericList<Real> blocklist_ymin{"blocklist_ymin", EMPTY_LIST, NO_RANGE, "Min y-coordinates of each square block"};
  const ParameterNumericList<Real> blocklist_ymax{"blocklist_ymax", EMPTY_LIST, NO_RANGE, "Max y-coordinates of each square block"};

  static void checkInputs(PyDict d);

  static PermeabilityModel2D_RectBlock_Params params;
};

class PermeabilityModel2D_RectBlock
{
public:
  PermeabilityModel2D_RectBlock_Params& params;

  explicit PermeabilityModel2D_RectBlock( const PyDict& d ) :
      params(PermeabilityModel2D_RectBlock_Params::params),
      Kxx_ref_(d.get(params.Kxx_ref)), Kxy_ref_(d.get(params.Kxy_ref)),
      Kyx_ref_(d.get(params.Kyx_ref)), Kyy_ref_(d.get(params.Kyy_ref)),
      block_Kxx_(d.get(params.blocklist_Kxx)), block_Kxy_(d.get(params.blocklist_Kxy)),
      block_Kyx_(d.get(params.blocklist_Kyx)), block_Kyy_(d.get(params.blocklist_Kyy)),
      block_xmin_(d.get(params.blocklist_xmin)), block_xmax_(d.get(params.blocklist_xmax)),
      block_ymin_(d.get(params.blocklist_ymin)), block_ymax_(d.get(params.blocklist_ymax))
  {
    SANS_ASSERT( block_Kxx_.size() == block_Kxy_.size());
    SANS_ASSERT( block_Kxx_.size() == block_Kyx_.size());
    SANS_ASSERT( block_Kxx_.size() == block_Kyy_.size());
    SANS_ASSERT( block_Kxx_.size() == block_xmin_.size());
    SANS_ASSERT( block_Kxx_.size() == block_xmax_.size());
    SANS_ASSERT( block_Kxx_.size() == block_ymin_.size());
    SANS_ASSERT( block_Kxx_.size() == block_ymax_.size());
  }

  PermeabilityModel2D_RectBlock( const PermeabilityModel2D_RectBlock& model ) :
      params( PermeabilityModel2D_RectBlock_Params::params ),
      Kxx_ref_(model.Kxx_ref_), Kxy_ref_(model.Kxy_ref_), Kyx_ref_(model.Kyx_ref_), Kyy_ref_(model.Kyy_ref_),
      block_Kxx_(model.block_Kxx_), block_Kxy_(model.block_Kxy_),
      block_Kyx_(model.block_Kyx_), block_Kyy_(model.block_Kyy_),
      block_xmin_(model.block_xmin_), block_xmax_(model.block_xmax_),
      block_ymin_(model.block_ymin_), block_ymax_(model.block_ymax_) {}

  ~PermeabilityModel2D_RectBlock() {}

  void permeability( const Real& x, const Real& y, const Real& time,
                     Real& Kxx, Real& Kxy, Real& Kyx, Real& Kyy) const
  {
    Kxx = Kxx_ref_;
    Kxy = Kxy_ref_;
    Kyx = Kyx_ref_;
    Kyy = Kyy_ref_;

    for (int i = 0; i < (int) block_Kxx_.size(); i++)
    {
      if ( x >= block_xmin_[i] && x <= block_xmax_[i] )
        if ( y >= block_ymin_[i] && y <= block_ymax_[i] )
        {
          Kxx = block_Kxx_[i];
          Kxy = block_Kxy_[i];
          Kyx = block_Kyx_[i];
          Kyy = block_Kyy_[i];
        }
    }
  }

  void dump( std::string indent, std::ostream& out = std::cout ) const;
  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  Real Kxx_ref_, Kxy_ref_, Kyx_ref_, Kyy_ref_; //Background permeability value

  //Descriptions of rectangular blocks with different constant permeabilities to background value
  std::vector<Real> block_Kxx_;
  std::vector<Real> block_Kxy_;
  std::vector<Real> block_Kyx_;
  std::vector<Real> block_Kyy_;
  std::vector<Real> block_xmin_;
  std::vector<Real> block_xmax_;
  std::vector<Real> block_ymin_;
  std::vector<Real> block_ymax_;
};
#endif

class PermeabilityModel2D_QuadBlock
{
public:

  typedef std::array<Real, 2> Coord;
  typedef std::array<Coord, 4> QuadBlock;

  // basis function category
  enum PermeabilityType2D
  {
    PermeabilityType2D_QuadBlock,
    PermeabilityType2D_Cosine
  };

  explicit PermeabilityModel2D_QuadBlock( Real K_ref ) :
      K_ref_({{K_ref, 0.0}, {0.0, K_ref}}), type_(PermeabilityType2D_QuadBlock) {}

  explicit PermeabilityModel2D_QuadBlock( DLA::MatrixS<2,2,Real> K_ref ) :
      K_ref_(K_ref), type_(PermeabilityType2D_QuadBlock)
  {
    Real detK = K_ref_(0,0)*K_ref_(1,1) - K_ref_(0,1)*K_ref_(1,0);
    SANS_ASSERT_MSG( detK > 0, "PermeabilityModel2D_QuadBlock - Background permeability should be positive definite.");
  }

  PermeabilityModel2D_QuadBlock( DLA::MatrixS<2,2,Real> K_ref, std::vector<QuadBlock> block_coords,
                                 std::vector<DLA::MatrixS<2,2,Real>> block_K ) :
    K_ref_(K_ref), block_coords_(block_coords), block_K_(block_K), type_(PermeabilityType2D_QuadBlock)
  {
    SANS_ASSERT( block_coords_.size() == block_K_.size());

    //Check that the block vertices are in counter-clockwise order
    for (std::size_t i = 0; i < block_coords_.size(); i++)
    {
      Real detK = block_K_[i](0,0)*block_K_[i](1,1) - block_K_[i](0,1)*block_K_[i](1,0);
      SANS_ASSERT_MSG( detK > 0, "PermeabilityModel2D_QuadBlock - Permeability in block %d should be positive definite.", i);

      bool ccw_flag = true;
      ccw_flag &= isPointOnLeft(block_coords_[i][2], block_coords_[i][0], block_coords_[i][1]);
      ccw_flag &= isPointOnLeft(block_coords_[i][3], block_coords_[i][1], block_coords_[i][2]);
      ccw_flag &= isPointOnLeft(block_coords_[i][0], block_coords_[i][2], block_coords_[i][3]);
      ccw_flag &= isPointOnLeft(block_coords_[i][1], block_coords_[i][3], block_coords_[i][0]);

      SANS_ASSERT_MSG( ccw_flag, "PermeabilityModel2D_QuadBlock - The vertices in block %d should be in counter-clockwise order.", i);
    }
  }

  PermeabilityModel2D_QuadBlock( const Real K_ref, const Real Kratio, const Real Lx, const Real Ly,
                                 const Real amp, const Real freq, const Real width, const Real yoffset ) :
    K_ref_({{K_ref, 0.0}, {0.0, K_ref}}), type_(PermeabilityType2D_Cosine), Kratio_(Kratio),
    Lx_(Lx), Ly_(Ly), amp_(amp), freq_(freq), width_(width), yoffset_(yoffset)
  {
    SANS_ASSERT_MSG( K_ref > 0, "PermeabilityModel2D_QuadBlock - Background permeability should be positive definite.");
    SANS_ASSERT( Kratio_ > 1 );
  }

  ~PermeabilityModel2D_QuadBlock() {}

  void permeability( const Real& x, const Real& y, const Real& time,
                     Real& Kxx, Real& Kxy, Real& Kyx, Real& Kyy) const
  {
    Kxx = 0; Kxy = 0;
    Kyx = 0; Kyy = 0;

    if (type_ == PermeabilityType2D_QuadBlock)
    {
      Kxx = K_ref_(0,0);
      Kxy = K_ref_(0,1);
      Kyx = K_ref_(1,0);
      Kyy = K_ref_(1,1);

      Coord p = {{x, y}};

      for (std::size_t i = 0; i < block_coords_.size(); i++)
      {
        if ( isPointInsideQuad(p, block_coords_[i]) )
        {
          Kxx = block_K_[i](0,0);
          Kxy = block_K_[i](0,1);
          Kyx = block_K_[i](1,0);
          Kyy = block_K_[i](1,1);
        }
      }
    }
    else if (type_ == PermeabilityType2D_Cosine)
    {
      Real z = ( y - Ly_*(yoffset_ + amp_*cos(2.0*PI*freq_*x/Lx_)) ) / (width_*Ly_);
      Real sine_factor = exp(-z*z);
      Real K_factor = max(Kratio_*sine_factor, 1.0);

      Kxx = K_ref_(0,0) * K_factor;
      Kxy = K_ref_(0,1) * K_factor;
      Kyx = K_ref_(1,0) * K_factor;
      Kyy = K_ref_(1,1) * K_factor;
    }
  }

private:

  bool isPointOnLeft(const Coord& p, const Coord& v0, const Coord& v1) const
  {
    return ( (v1[0] - v0[0])*(p[1] - v0[1]) - (v1[1] - v0[1])*(p[0] - v0[0]) > 0.0 );
  }

  bool isPointInsideQuad(const Coord& p, const QuadBlock& block) const
  {
    //Check if point is to the left of all edges in quad
    if (isPointOnLeft(p, block[0], block[1]) == false) return false;
    if (isPointOnLeft(p, block[1], block[2]) == false) return false;
    if (isPointOnLeft(p, block[2], block[3]) == false) return false;
    if (isPointOnLeft(p, block[3], block[0]) == false) return false;

    return true;
  }

  DLA::MatrixS<2,2,Real> K_ref_; //Background permeability value

  //Descriptions of quadrilateral blocks with different constant permeabilities to background value
  std::vector<QuadBlock> block_coords_;
  std::vector<DLA::MatrixS<2,2,Real>> block_K_;

  //Hack for cosine-wave
  PermeabilityType2D type_;
  Real Kratio_ = 0;
  Real Lx_ = 0, Ly_ = 0; //domain sizes
  Real amp_ = 0, freq_ = 0, width_ = 0, yoffset_ = 0; //wave properties

};


//----------------------------------------------------------------------------//
// PermeabilityModel2D_CartTable:  table lookup

//----------------------------------------------------------------------------//
// A class for representing input parameters for PermeabilityModel2D_CartTable_Params
struct PermeabilityModel2D_CartTable_Params : noncopyable
{
  const ParameterFileName PermeabilityFile{"PermeabilityFile", std::ios_base::in, "", "File name with Cartesian Permeability table"};
  const ParameterNumeric<Real> scale_factor{"scale_factor", 1.0, 0, NO_LIMIT, "Constant scaling factor"};

  static void checkInputs(PyDict d);

  static PermeabilityModel2D_CartTable_Params params;
};


class PermeabilityModel2D_CartTable
{
public:
  PermeabilityModel2D_CartTable_Params& params;

  explicit PermeabilityModel2D_CartTable( const PyDict& d ) :
      params(PermeabilityModel2D_CartTable_Params::params),
      K_(d.get(params.PermeabilityFile))
  {
    Real scale_factor = d.get(params.scale_factor);
    if (scale_factor != 1.0)
      K_.scaleValues(scale_factor);
  }

  PermeabilityModel2D_CartTable( const PermeabilityModel2D_CartTable& model ) :
      params( PermeabilityModel2D_CartTable_Params::params ),
      K_(model.K_) {}

  PermeabilityModel2D_CartTable( const std::string& filename,
                                 const Real scale_factor = 1.0 ) :
      params(PermeabilityModel2D_CartTable_Params::params),
      K_(filename)
  {
    if (scale_factor != 1.0)
      K_.scaleValues(scale_factor);
  }

  ~PermeabilityModel2D_CartTable() = default;

  void permeability( const Real& x, const Real& y, const Real& time,
                     Real& Kxx, Real& Kxy, Real& Kyx, Real& Kyy) const
  {
    Real K = K_(x,y);
    Kxx = K;  Kyx = 0;
    Kxy = 0;  Kyy = K;
  }

  void dump( std::string indent, std::ostream& out = std::cout ) const;
  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  CartTable<2> K_;
};

} //namespace SANS

#endif  // PERMEABILITYMODEL2D_H
