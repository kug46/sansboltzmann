// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef Q1DPRIMITIVE_SURROGATESW_H
#define Q1DPRIMITIVE_SURROGATESW_H

// solution interpreter class
// Buckley-Leverett eqn: primitive variables (surrogate Sw)

#include "tools/SANSnumerics.h" // Real
#include "tools/SANSException.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Topology/Dimension.h"

#include <string>

namespace SANS
{

//----------------------------------------------------------------------------//
// solution variable interpreter: 1-D Buckley-Leverett
// primitive variables (surrogate Sw)
//
// template parameters:
//   T                    solution DOF data type (e.g. double)
//   QType                solution variable set (e.g. primitive)
//
// member functions:
//   .eval_Sw            extract wetting phase saturation (Sw)
//   .eval_Sn            extract non-wetting phase saturation (Sn)
//   .eval_SwGradient    extract wetting phase saturation gradient
//   .eval_SnGradient    extract non-wetting phase saturation gradient
//   .isValidState       T/F: determine if state is physically valid (e.g. Sw >= 0)
//   .setfromPrimitive   set from primitive variable array
//
//   .dump                debug dump of private data
//----------------------------------------------------------------------------//


// primitive variables (surrogate Sw)
class QTypePrimitive_SurrogateSw;


// primary template
template <class QType, template <class> class PDETraitsSize>
class Q1D;


template <template <class> class PDETraitsSize>
class Q1D<QTypePrimitive_SurrogateSw, PDETraitsSize>
{
public:
  static const int N = 1;                       // solution components

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  explicit Q1D(){}
  Q1D( const Q1D& q0 ){}
  ~Q1D() {}

  Q1D& operator=( const Q1D& );

  // evaluate primitive variables
  template<class T>
  void eval_Sw( const ArrayQ<T>& q, T& Sw ) const; //wetting phase saturation - Sw
  template<class T>
  void eval_Sn( const ArrayQ<T>& q, T& Sn ) const; //non-wetting phase saturation - Sn

  template<class T>
  void jacobian_Sw( const ArrayQ<T>& q, ArrayQ<T>& Sw_q ) const; //wetting phase saturation Jacobian - dSw/dq

  template<class Tq, class Tg, class T>
  void eval_SwGradient( const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, T& Swx ) const;
  template<class Tq, class Tg, class T>
  void eval_SnGradient( const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, T& Snx ) const;

  template<class T>
  void eval_SwHessian( const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qxx, T& Swxx ) const;
  template<class T>
  void eval_SnHessian( const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qxx, T& Snxx ) const;

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const;

  // set from primitive variable array
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const Real data[], const std::string name[], int nn ) const;

  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const Real data, const std::string& name ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:

  template<class T>
  void limit(const ArrayQ<T>& q0, ArrayQ<T>& q) const;

  template<class T>
  void limitJacobian( const T& Sw0, T& Sw_Sw0 ) const;

};


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitive_SurrogateSw, PDETraitsSize>::limit( const T& Sw0, T& Sw ) const
{
  Real c = 0.001;
  if (Sw0 < c)
  {
    T f = Sw0/c;
    Sw = c/( 3.0 - 3.0*f + f*f );
  }
  else if (Sw0 > 1.0 - c)
  {
    T f = (1.0 - Sw0)/c;
    Sw = 1.0 - c/( 3.0 - 3.0*f + f*f );
  }
  else
  {
    Sw = Sw0;
  }
}

template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitive_SurrogateSw, PDETraitsSize>::limitJacobian( const T& Sw0, T& Sw_Sw0 ) const
{
  Real c = 0.001;
  if (Sw0 < c)
  {
    T f = Sw0/c;
    Sw_Sw0 = (3.0 - 2.0*f) / pow( 3.0 - 3.0*f + f*f, 2.0 );
  }
  else if (Sw0 > 1.0 - c)
  {
    T f = (1.0 - Sw0)/c;
    Sw_Sw0 = (3.0 - 2.0*f) / pow( 3.0 - 3.0*f + f*f, 2.0 );
  }
  else
  {
    Sw_Sw0 = 1.0;
  }
}

template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitive_SurrogateSw, PDETraitsSize>::eval_Sw( const ArrayQ<T>& q, T& Sw ) const
{
  T Sw0 = q;
  limit(Sw0, Sw);
}

template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitive_SurrogateSw, PDETraitsSize>::eval_Sn( const ArrayQ<T>& q, T& Sn ) const
{
  T Sw0 = q;
  T Sw;
  limit(Sw0, Sw);
  Sn = 1.0 - Sw;
}

template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitive_SurrogateSw, PDETraitsSize>::jacobian_Sw( const ArrayQ<T>& q, ArrayQ<T>& Sw_q ) const
{
  T Sw0 = q;
  limitJacobian(Sw0, Sw_q);
}

template <template <class> class PDETraitsSize>
template <class Tq, class Tg, class T>
inline void
Q1D<QTypePrimitive_SurrogateSw, PDETraitsSize>::eval_SwGradient( const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, T& Swx ) const
{
  Swx = qx;
}

template <template <class> class PDETraitsSize>
template <class Tq, class Tg, class T>
inline void
Q1D<QTypePrimitive_SurrogateSw, PDETraitsSize>::eval_SnGradient( const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, T& Snx ) const
{
  Snx = -qx;
}

template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitive_SurrogateSw, PDETraitsSize>::eval_SwHessian( const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qxx, T& Swxx ) const
{
  Swxx = qxx;
}

template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitive_SurrogateSw, PDETraitsSize>::eval_SnHessian( const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qxx, T& Snxx ) const
{
  Snxx = -qxx;
}

// is state physically valid: check for 0 <= Sw <= 1
template <template <class> class PDETraitsSize>
inline bool
Q1D<QTypePrimitive_SurrogateSw, PDETraitsSize>::isValidState( const ArrayQ<Real>& q ) const
{
  Real Sw;
  eval_Sw(q, Sw);

#if 0
  printf( "%s: Sw = %e\n", __func__, Sw );
#endif

  return ((Sw >= 0) && (Sw <= 1));
}

template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitive_SurrogateSw, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const Real data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn == N);
  SANS_ASSERT(name[0] == "Sw");

  q = data[0]; //Sw
}

template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitive_SurrogateSw, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const Real data, const std::string& name ) const
{
  SANS_ASSERT(name == "Sw");
  q = data; //Sw
}

}

#endif  // Q1DPRIMITIVE_SURROGATESW_H
