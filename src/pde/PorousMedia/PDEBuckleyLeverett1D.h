// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDEBUCKLEYLEVERETT1D_H
#define PDEBUCKLEYLEVERETT1D_H

// 1-D Buckley-Leverett PDE class

#include <cmath>              // sqrt
#include <iostream>
#include <string>
#include <memory> //shared_ptr

#include "tools/SANSnumerics.h"     // Real
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "tools/smoothmath.h"

#include "Topology/Dimension.h"

#include "pde/ForcingFunctionBase.h"

namespace SANS
{

// forward declare: solution variables interpreter
template <class QType, template <class> class PDETraitsSize>
class Q1D;

//----------------------------------------------------------------------------//
// PDE class: 1-D Buckley-Leverett
//
// equation: d/dt( phi Sw ) + d/dx( uT fw(Sw) ) - d/dx( -K (lambda_w*lambda_n)/(lambda_w + lambda_n)*pc_Sw/phi * dSw/dx ) = 0
// where:    fw(Sw) = lambda_w / (lambda_w + lambda_n)
//           lambda_w = krw/mu_w
//           lambda_n = krn/mu_n
//
// template parameters:
//   T                        solution DOF data type (e.g. double)
//   QType                    solution variable set (e.g. primitive)
//   RelPermModel             relative permeability model (assumes same model for both wetting and non-wetting phases)
//   CapillaryModel           capillary pressure model
//
// member functions:
//   .hasFluxAdvectiveTime        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//   .hasFluxViscous             T/F: PDE has viscous flux term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: dU/dQ
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .fluxViscous                viscous fluxes: Fv(Q, QX)
//   .diffusionViscous           viscous diffusion coefficient: d(Fv)/d(UX)
//   .isValidState               T/F: determine if state is physically valid (e.g. Sw > 0)
//   .setDOFFrom                 set from primitive variable array
//----------------------------------------------------------------------------//

template <template <class> class PDETraitsSize, class PDETraitsModel>
class PDEBuckleyLeverett1D
{
public:
  typedef PhysD1 PhysDim;
  static const int D = PDETraitsSize<PhysDim>::D; // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N; // total solution variables

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>; // solution/residual arrays

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>; // matrices

  //Extract model traits
  typedef typename PDETraitsModel::QType QType;
  typedef typename PDETraitsModel::RelPermModel RelPermModel;
  typedef typename PDETraitsModel::CapillaryModel CapillaryModel;

  typedef Q1D<QType, PDETraitsSize> QInterpreter;            // solution variable interpreter type

  typedef ForcingFunctionBase1D<PDEBuckleyLeverett1D> ForcingFunctionType;

  PDEBuckleyLeverett1D( const Real& phi, const Real& uT, const RelPermModel& kr, const Real& mu_w, const Real& mu_n,
                        const Real& K, const CapillaryModel& pc, const std::shared_ptr<ForcingFunctionType>& force = NULL ) :
      phi_(phi),
      uT_(uT),
      kr_(kr),
      mu_w_(mu_w),
      mu_n_(mu_n),
      K_(K),
      pc_(pc),
      force_(force),
      qInterpreter_(){}

  PDEBuckleyLeverett1D( const PDEBuckleyLeverett1D& pde ) :
      phi_(pde.phi_),
      uT_(pde.uT_),
      kr_(pde.kr_),
      mu_w_(pde.mu_w_),
      mu_n_(pde.mu_n_),
      K_(pde.K_),
      pc_(pde.pc_),
      force_(pde.force_),
      qInterpreter_(pde.qInterpreter_) {}

  ~PDEBuckleyLeverett1D() {}

  PDEBuckleyLeverett1D& operator=( const PDEBuckleyLeverett1D& );

  // flux components
  bool hasFluxAdvectiveTime() const { return true; }
  bool hasFluxAdvective() const { return true; }
  bool hasFluxViscous() const { return pc_.hasFluxViscous(); }
  bool hasSource() const { return false; }
  bool hasSourceTrace() const { return false; }
  bool hasSourceLiftedQuantity() const { return false; }
  bool hasForcingFunction() const { return force_ != NULL && (*force_).hasForcingFunction(); }

  // viscous flux is of the form K(q).gradu
  bool fluxViscousLinearInGradient() const { return true; }
  bool needsSolutionGradientforSource() const { return false; }

  const QInterpreter& variableInterpreter() const { return qInterpreter_; }

  Real porosity() const { return phi_; }
  Real totalVelocity() const { return uT_; }
  Real viscosityWet() const { return mu_w_; }
  Real viscosityNonwet() const { return mu_n_; }
  Real permeability() const { return K_; }

  template <class T>
  void fractionalFlow( const T& Sw, const T& Sn, T& fw ) const;

  template <class T>
  void fractionalFlowJacobian( const T& Sw, const T& Sn, T& fw_Sw ) const;

  // unsteady temporal flux: Ft(Q)
  template <class T, class Tf>
  void fluxAdvectiveTime(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tf>& ft ) const
  {
    ArrayQ<T> ftLocal = 0;
    masterState(x, time, q, ftLocal);
    ft += ftLocal;
  }

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <class T, class Tf>
  void jacobianFluxAdvectiveTime(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& J ) const
  {
    MatrixQ<T> JLocal = 0;
    jacobianMasterState(x, time, q, JLocal);
    J += JLocal;
  }

  // master state: U(Q)
  template <class T, class Tu>
  void masterState(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tu>& uCons ) const;

  // jacobian of master state wrt q: dU(Q)/dQ
  template<class T>
  void jacobianMasterState(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const;

  // strong form of unsteady conservative flux: d(U)/d(t)
  template <class T>
  void strongFluxAdvectiveTime(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& strongPDE ) const;

  // advective flux: F(Q)
  template <class T, class Tf>
  void fluxAdvective(
      const Real& x, const Real& time, const ArrayQ<T>& q, ArrayQ<Tf>& f ) const;

  template <class T, class Tf>
  void fluxAdvectiveUpwind(
      const Real& x, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx,  ArrayQ<Tf>& f ) const;

  template <class T, class Tf>
  void fluxAdvectiveUpwind_Godunov(
      const Real& x, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx, ArrayQ<Tf>& fnx ) const;

  template <class T, class Tf>
  void fluxAdvectiveUpwindSpaceTime(
      const Real& x, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const;

#if 0 //Not used, since full-temporal upwinding works.
  template <class T>
  void fluxUpwindSpaceTime_Godunov(
      const Real& x, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& nt, ArrayQ<T>& fn ) const;

  template <class T>
  void fluxUpwindSpaceTime_LocalLaxFriedrichs(
      const Real& x, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& nt, ArrayQ<T>& f ) const;
#endif

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class T>
  void jacobianFluxAdvective(
      const Real& x, const Real& time, const ArrayQ<T>& q, MatrixQ<T>& ax ) const;

  // absolute value of advective flux jacobian: |n . d(F)/d(U)|
  template <class T>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Real& x, const Real& time, const ArrayQ<T>& q, const Real& nx, MatrixQ<T>& mtx ) const;

  // absolute value of advective flux jacobian: |n . d(F)/d(U)|
  template <class T>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Real& x, const Real& time, const ArrayQ<T>& q, const Real& nx, const Real& nt, MatrixQ<T>& mtx ) const;

  // strong form advective fluxes
  template <class T>
  void strongFluxAdvective( const Real& x, const Real& time, const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayQ<T>& strongPDE ) const;

  // viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& time, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, ArrayQ<Tf>& f ) const;

  // space-time viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const;

  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
      const Real& nx, ArrayQ<Tf>& f ) const;

  // perturbation to viscous flux from dux, duy
  template <class Tq, class Tg, class Tgu, class Tf>
  void perturbedGradFluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      const ArrayQ<Tgu>& dqx,
      ArrayQ<Tf>& df ) const
  {
    fluxViscous(x, time, q, qx, df);
  }

  // space-time viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qtL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qtR,
      const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const;

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscous( const Real& x, const Real& time,
                         const ArrayQ<Tq>& q,  const ArrayQ<Tg>& qx,
                         MatrixQ<Tk>& kxx ) const;

  // space-time viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscousSpaceTime(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxt,
      MatrixQ<Tk>& ktx, MatrixQ<Tk>& ktt ) const;

  // jacobian of viscous diffusion matrix
  template <class T>
  void jacobianDiffusionViscous( const Real& x, const Real& time, const ArrayQ<T>& q, MatrixQ<T>& kxx_u ) const;

  // strong form viscous fluxes
  template <class T>
  void strongFluxViscous( const Real& x, const Real& time,
                          const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qxx, ArrayQ<T>& strongPDE ) const;

  // space-time strong form viscous fluxes
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscousSpaceTime(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qtx, const ArrayQ<Th>& qtt,
      ArrayQ<Tf>& strongPDE ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial strongFluxViscous
    strongFluxViscous(x, time, q, qx, qxx, strongPDE);
  }
  // solution-dependent source: S(X, Q, QX)
  template <class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Ts>& source ) const;

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX)
  template <class Tlq, class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& time,
      const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Ts>& source ) const {}

  // dual-consistent source
  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& xL,
      const Real& xR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const {}

  // trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
  template <class Tq, class Ts>
  void sourceLiftedQuantity(
      const Real& xL, const Real& xR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, Ts& s ) const {}

  // jacobian of source wrt conservation variable gradients: d(S)/d(Ux), d(S)/d(Uy)
  template <class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Ts>& dsdux ) const {}

  // jacobian of source wrt conservation variable gradients: d(S)/d(Ux), d(S)/d(Uy)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSource(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Ts>& dsdux ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSourceAbsoluteValue(
      const Real& x, const Real& time, const Real& nx,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Ts>& dsdux ) const {}

  // right-hand-side forcing function
  template <class T>
  void forcingFunction( const Real& x, const Real& time, ArrayQ<T>& forcing ) const;

  // characteristic speed (needed for timestep)
  template<class T>
  void speedCharacteristic( const Real& x, const Real& time, const Real& dx, const ArrayQ<T>& q, T& speed ) const;

  // characteristic speed
  template<class T>
  void speedCharacteristic( const Real& x, const Real& time, const ArrayQ<T>& q, T& speed ) const;

  // update fraction needed for physically valid state
  void updateFraction( const Real& x, const Real& time, const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
                       const Real maxChangeFraction, Real& updateFraction ) const;

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const;

  // set from primitive variable array
  template<class T>
  void setDOFFrom( ArrayQ<T>& q, const Real data[], const std::string name[], int nn ) const;

  template<class T>
  void setDOFFrom( ArrayQ<T>& q, const Real data, const std::string& name ) const;

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const;

  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const;

  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const;

  // how many residual equations are we dealing with
  int nMonitor() const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  Real phi_; //Porosity (assumed to be constant)
  Real uT_; //Total velocity
  const RelPermModel& kr_; //Relative permeability model (assumed to be the same for both phases)

  Real mu_w_; //Wetting phase viscosity (assumed to be constant)
  Real mu_n_; //Non-wetting phase viscosity (assumed to be constant)

  Real K_; // Permeability (assumed to be constant - needed for diffusion term)
  const CapillaryModel& pc_; //Capillary pressure model (for diffusion term)

  const std::shared_ptr<ForcingFunctionType> force_;

  const QInterpreter qInterpreter_;   // solution variable interpreter class
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel>::fractionalFlow(
    const T& Sw, const T& Sn, T& fw ) const
{
  //Evaluate relative permeabilities
  T krw = kr_.relativeperm(Sw);
  T krn = kr_.relativeperm(Sn);

  //Evaluate phase mobilities
  T lambda_w = krw/mu_w_;
  T lambda_n = krn/mu_n_;

  fw = lambda_w/(lambda_w + lambda_n);
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel>::fractionalFlowJacobian(
    const T& Sw, const T& Sn, T& fw_Sw ) const
{
  //Evaluate relative permeabilities
  T krw = kr_.relativeperm(Sw);
  T krn = kr_.relativeperm(Sn);

  //Evaluate phase mobilities
  T lambda_w = krw/mu_w_;
  T lambda_n = krn/mu_n_;

  //Evaluate relative permeability jacobians
  T krw_Sw, krn_Sn;
  kr_.relativepermJacobian(Sw, krw_Sw);
  kr_.relativepermJacobian(Sn, krn_Sn);

  Real dSn_dSw = -1;
  T krn_Sw = krn_Sn * dSn_dSw;

  //Evaluate phase mobility jacobians
  T lambda_w_Sw = krw_Sw/mu_w_;
  T lambda_n_Sw = krn_Sw/mu_n_;

  //Chain rule
  fw_Sw = (lambda_w_Sw*lambda_n - lambda_w*lambda_n_Sw)/pow(lambda_w + lambda_n, 2);
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tu>
inline void
PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel>::masterState(
    const Real& x, const Real& time,
    const ArrayQ<T>& q, ArrayQ<Tu>& uCons ) const
{
  T Sw = 0;
  qInterpreter_.eval_Sw( q, Sw );

  DLA::index(uCons, 0) += Sw;
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel>::jacobianMasterState(
    const Real& x, const Real& time,
    const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
{
  ArrayQ<T> Sw_q;
  qInterpreter_.jacobian_Sw( q, Sw_q );

  DLA::index(dudq, 0, 0) = DLA::index(Sw_q, 0);
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel>::strongFluxAdvectiveTime(
    const Real& x, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& strongPDE ) const
{
  T Swt;
  qInterpreter_.eval_SwGradient( q, qt, Swt );

  DLA::index(strongPDE, 0) += Swt; // d(Sw)/dt
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tf>
inline void
PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel>::fluxAdvective(
    const Real& x, const Real& time, const ArrayQ<T>& q, ArrayQ<Tf>& f ) const
{
  T Sw = 0, Sn = 0;
  qInterpreter_.eval_Sw( q, Sw );
  qInterpreter_.eval_Sn( q, Sn );

  //Evaluate fractional flow function
  T fw;
  fractionalFlow(Sw, Sn, fw);

#if 0
  if (Sw < 0.0)
  {
    Real SwL = 1.0/sqrt(3.0);
    Real SnL = 1.0 - SwL;
    Real fw_SwL;
    fractionalFlowJacobian(SwL, SnL, fw_SwL);

    fw = fw_SwL*Sw;
  }
#endif

  DLA::index(f, 0) += uT_*fw/phi_;
}

// advective flux jacobian: d(F)/d(U) = d( uT*fw(Sw)/phi )/ d(Sw) = (uT/phi) * d(fw)/d(Sw)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvective(
    const Real& x, const Real& time, const ArrayQ<T>& q, MatrixQ<T>& ax ) const
{
  T Sw = 0, Sn = 0;
  qInterpreter_.eval_Sw( q, Sw );
  qInterpreter_.eval_Sn( q, Sn );

  //Evaluate fractional flow function jacobian (wrt Sw)
  T fw_Sw;
  fractionalFlowJacobian(Sw, Sn, fw_Sw);

#if 0
  if (Sw < 0.0)
  {
    Real SwL = 1.0/sqrt(3.0);
    Real SnL = 1.0 - SwL;
    Real fw_SwL;
    fractionalFlowJacobian(SwL, SnL, fw_SwL);

    fw_Sw = fw_SwL;
  }
#endif

  DLA::index(ax, 0, 0) += (uT_*fw_Sw)/phi_;
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tf>
inline void
PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel>::fluxAdvectiveUpwind(
    const Real& x, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx, ArrayQ<Tf>& f ) const
{
#if 0
  //Local Lax-Friedrichs
  ArrayQ<T> fL = 0, fR = 0;

  fluxAdvective(x, time, qL, fL );
  fluxAdvective(x, time, qR, fR );

  ArrayQ<T> axL = 0, axR = 0;

  jacobianFluxAdvective(x, time, qL, axL);
  jacobianFluxAdvective(x, time, qR, axR);

  //TODO: Rather arbitrary smoothing constants here
//  Real eps = 1e-10;
//  Real alpha = 40;

  T anL = nx*axL;
  T anR = nx*axR;

//  T a = smoothmax( smoothabs0(anL, eps), smoothabs0(anR, eps), alpha);
  T a = max( fabs(anL), fabs(anR) );

  f += 0.5*( nx*fR + nx*fL ) - 0.5*a*(qR - qL);

#else
  //Godunov's flux
  fluxAdvectiveUpwind_Godunov( x, time, qL, qR, nx, f);
#endif
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tf>
inline void
PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel>::fluxAdvectiveUpwind_Godunov(
    const Real& x, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx, ArrayQ<Tf>& fnx ) const
{
  /*Implementation of Godunov's flux for a scalar conservation law
  *
  * F(uL, uR) = min_{uL <= u <= uR} fn(u),  if uL <= uR
  *             max_{uR <= u <= uL} fn(u),  if uL >  uR
  */

  //Evaluate left and right advective fluxes
  ArrayQ<Tf> fL = 0, fR = 0;
  fluxAdvective(x, time, qL, fL );
  fluxAdvective(x, time, qR, fR );

  //Evaluate left and right normal fluxes
  Tf fnL = DLA::index(fL, 0)*nx;
  Tf fnR = DLA::index(fR, 0)*nx;
  Tf fnx0 = 0;

  T SwL, SwR;
  qInterpreter_.eval_Sw( qL, SwL );
  qInterpreter_.eval_Sw( qR, SwR );

  //Assuming the min/max of the flux function occurs at Sw = 0 and 1
  //True for power law relative permeability models
  Real Sw_min = 0.0, Sw_max = 1.0;

  if (nx < 0)
  {
    Sw_min = 1.0;
    Sw_max = 0.0;
  }

  if (fabs(SwL - SwR) <= 1e-14)
  {
//    fnx += 0.5*(fnL + fnR);
    if (uT_*nx >= 0)
    {
      fnx0 += fnL;
    }
    else
    {
      fnx0 += fnR;
    }
  }
  else if (SwL <= SwR)
  {
    if (SwL <= Sw_min && Sw_min <= SwR)
    {
      ArrayQ<T> qmin;
      qInterpreter_.setFromPrimitive( qmin, Sw_min, "Sw");

      ArrayQ<Tf> fmin = 0;
      fluxAdvective(x, time, qmin, fmin );
      Tf fnmin = DLA::index(fmin, 0)*nx;

      //Take the minimum normal flux out of the three evaluations
      fnx0 += min(fnL, min(fnmin, fnR));
    }
    else
    {
      fnx0 += min(fnL, fnR);
    }
  }
  else
  {
    if (SwR <= Sw_max && Sw_max <= SwL)
    {
      ArrayQ<T> qmax;
      qInterpreter_.setFromPrimitive( qmax, Sw_max, "Sw");

      ArrayQ<Tf> fmax = 0;
      fluxAdvective(x, time, qmax, fmax );
      Tf fnmax = DLA::index(fmax, 0)*nx;

      //Take the maximum normal flux out of the three evaluations
      fnx0 += max(fnR, max(fnmax, fnL));
    }
    else
    {
      fnx0 += max(fnR, fnL);
    }
  }

  DLA::index(fnx, 0) += fnx0;
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tf>
inline void
PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel>::fluxAdvectiveUpwindSpaceTime(
    const Real& x, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const
{
#if 0
//  f += 0.5*(nt*1 + nx*uT_)*(qR + qL) - 0.5*fabs(nt*1 + nx*uT_)*(qR - qL);

  fluxUpwindSpaceTime_LocalLaxFriedrichs( x, time, qL, qR, nx, nt, f );
//  fluxUpwindSpaceTime_Godunov( x, time, qL, qR, nx, nt, f );

#else
  //Full temporal upwinding

  //Compute upwinded spatial advective flux
  ArrayQ<Tf> fnx = 0;
  fluxAdvectiveUpwind( x, time, qL, qR, nx, fnx );

  //Upwind temporal flux
  ArrayQ<Tf> ft = 0;
  if (nt >= 0)
    masterState( x, time, qL, ft );
  else
    masterState( x, time, qR, ft );

  f += ft*nt + fnx;
#endif
}

#if 0 //These space-time upwinding methods aren't used. The full temporal upwinding scheme above seems to work fine.
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel>::fluxUpwindSpaceTime_Godunov(
    const Real& x, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx, const Real& nt, ArrayQ<T>& fn ) const
{
  int Nstep = 4;

  T SwL, SwR;
  qInterpreter_.eval_Sw( qL, SwL );
  qInterpreter_.eval_Sw( qR, SwR );

  if (SwL <= SwR)
  {
    ArrayQ<T> fnmin = std::numeric_limits<Real>::max();

    for (int step = 0; step <= Nstep; step++)
    {
      Real frac = (Real)step/(Real)Nstep;
      ArrayQ<T> q_tmp = qL + (qR - qL)*frac;

      ArrayQ<T> fadv = 0, fcons = 0;
      fluxAdvective( x, time, q_tmp, fadv );
      masterState( q_tmp, fcons );

      //Evaluate normal flux
      ArrayQ<T> fn_tmp = fadv*nx + fcons*nt;

      fnmin = min(fnmin, fn_tmp);
    }

    fn += fnmin;
  }
  else
  {
    ArrayQ<T> fnmax = -std::numeric_limits<Real>::max();

    for (int step = 0; step <= Nstep; step++)
    {
      Real frac = (Real)step/(Real)Nstep;
      ArrayQ<T> q_tmp = qL + (qR - qL)*frac;

      ArrayQ<T> fadv = 0, fcons = 0;
      fluxAdvective( x, time, q_tmp, fadv );
      masterState( q_tmp, fcons );

      //Evaluate normal flux
      ArrayQ<T> fn_tmp = fadv*nx + fcons*nt;

      fnmax = max(fnmax, fn_tmp);
    }

    fn += fnmax;
  }

}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel>::fluxUpwindSpaceTime_Godunov(
    const Real& x, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx, const Real& nt, ArrayQ<T>& fn ) const
{
  ArrayQ<T> fadvL = 0, fadvR = 0;
  ArrayQ<T> fconsL = 0, fconsR = 0;

  fluxAdvective( x, time, qL, fadvL );
  fluxAdvective( x, time, qR, fadvR );

  masterState( qL, fconsL );
  masterState( qR, fconsR );

  //Evaluate left and right normal fluxes
  ArrayQ<T> fnL = fadvL*nx + fconsL*nt;
  ArrayQ<T> fnR = fadvR*nx + fconsR*nt;

  T SwL, SwR;
  qInterpreter_.eval_Sw( qL, SwL );
  qInterpreter_.eval_Sw( qR, SwR );

  if (fabs(nx) < 1e-10)
  { //Horizontal interface
    if (nt > 0)
      fn += fnL;
    else
      fn += fnR;
  }
  else
  {
    if (SwL <= SwR)
    {
      fn += min(fnL, fnR);
    }
    else
    {
      fn += max(fnL, fnR);
    }
  }

}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel>::fluxUpwindSpaceTime_LocalLaxFriedrichs(
    const Real& x, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx, const Real& nt, ArrayQ<T>& f ) const
{
  ArrayQ<T> fL = 0, fR = 0;

  fluxAdvective(x, time, qL, fL );
  fluxAdvective(x, time, qR, fR );

  ArrayQ<T> axL = 0, axR = 0;

  jacobianFluxAdvective(x, time, qL, axL);
  jacobianFluxAdvective(x, time, qR, axR);

  //TODO: Rather arbitrary smoothing constants here
//  Real eps = 1e-10;
//  Real alpha = 40;

  T anL = nt + nx*axL;
  T anR = nt + nx*axR;

//  T a = smoothmax( smoothabs0(anL, eps), smoothabs0(anR, eps), alpha);
  T a = max( fabs(anL), fabs(anR) );

  // Local Lax-Friedrichs Flux
  f += 0.5*( (nt*qR + nx*fR) + (nt*qL + nx*fL) ) - 0.5*a*(qR - qL);

}
#endif

// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvectiveAbsoluteValue(
  const Real& x, const Real& time, const ArrayQ<T>& q, const Real& nx, MatrixQ<T>& mtx ) const
{
  MatrixQ<T> ax = 0;

  jacobianFluxAdvective(x, time, q, ax);

  T an = nx*ax;

  DLA::index(mtx, 0, 0) += fabs( an );
}


// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvectiveAbsoluteValueSpaceTime(
  const Real& x, const Real& time, const ArrayQ<T>& q, const Real& nx, const Real& nt, MatrixQ<T>& mtx ) const
{
  SANS_ASSERT( false );
}

// strong form of advective flux: d(F)/d(X)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel>::strongFluxAdvective(
    const Real& x, const Real& time, const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayQ<T>& strongPDE ) const
{
  MatrixQ<T> ax = 0;
  jacobianFluxAdvective(x, time, q, ax);

  T Swx;
  qInterpreter_.eval_SwGradient( q, qx, Swx );
//  std::cout<<ax<<","<<qx<<","<<Swx<<std::endl;
  DLA::index(strongPDE, 0, 0) += DLA::index(ax, 0, 0)*Swx;
}

// viscous flux
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel>::fluxViscous(
    const Real& x, const Real& time, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, ArrayQ<Tf>& f ) const
{
  typedef typename promote_Surreal<Tq, Tg>::type T;

  //Get viscous diffusion matrix
  MatrixQ<Tq> kxx = 0;
  diffusionViscous( x, time, q, qx, kxx);

  T Swx;
  qInterpreter_.eval_SwGradient( q, qx, Swx );

  DLA::index(f, 0) -= DLA::index(kxx, 0, 0)*Swx;
}

// space-time viscous flux
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel>::fluxViscousSpaceTime(
    const Real& x, const Real& time, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
{
  //Not adding temporal diffusion, so just forward the call to spatial viscous flux
  fluxViscous(x, time, q, qx, f);
}

// viscous flux: normal flux with left and right states
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel>::fluxViscous(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
    const Real& nx, ArrayQ<Tf>& f ) const
{
  ArrayQ<Tf> fL = 0, fR = 0;

  fluxViscous(x, time, qL, qxL, fL);
  fluxViscous(x, time, qR, qxR, fR);

  f += 0.5*(fL + fR)*nx;
}

// space-time viscous flux: normal flux with left and right states
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel>::fluxViscousSpaceTime(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qtL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qtR,
    const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const
{
  ArrayQ<Tf> fL = 0, fR = 0;
  ArrayQ<Tf> gL = 0, gR = 0;

  fluxViscousSpaceTime(x, time, qL, qxL, qtL, fL, gL);
  fluxViscousSpaceTime(x, time, qR, qxR, qtR, fR, gR);

  f += 0.5*(fL + fR)*nx + 0.5*(gL + gR)*nt;
}

// viscous diffusion matrix: d(Fv)/d(UX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tk>
inline void
PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel>::diffusionViscous(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    MatrixQ<Tk>& kxx ) const
{
  Tq Sw, Sn;
  qInterpreter_.eval_Sw( q, Sw );
  qInterpreter_.eval_Sn( q, Sn );

  //Evaluate relative permeabilities
  Tq krw = kr_.relativeperm(Sw);
  Tq krn = kr_.relativeperm(Sn);

  //Evaluate phase mobilities
  Tq lambda_w = krw/mu_w_;
  Tq lambda_n = krn/mu_n_;

  //Evaluate capillary pressure jacobian
  Tq pc_Sn;
  pc_.capPressureJacobian( x, time, Sn, pc_Sn );

  Real dSn_dSw = -1;
  Tq pc_Sw = pc_Sn * dSn_dSw;

  // d(Fv)/d(Ux)
  DLA::index(kxx, 0, 0) += -K_*(lambda_w*lambda_n)/(lambda_w + lambda_n)*pc_Sw/phi_;
}

// viscous diffusion matrix: d(Fv)/d(UX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tk>
inline void
PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel>::diffusionViscousSpaceTime(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
    MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxt,
    MatrixQ<Tk>& ktx, MatrixQ<Tk>& ktt ) const
{
  //Not adding temporal diffusion, so just forward the call to spatial diffusion matrix
  diffusionViscous(x, time, q, qx, kxx);
}

// jacobian of viscous diffusion matrix: d (d(Fv)/d(ux))/d(u)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel>::jacobianDiffusionViscous(
    const Real& x, const Real& time, const ArrayQ<T>& q, MatrixQ<T>& kxx_u ) const
{
  T Sw, Sn;
  qInterpreter_.eval_Sw( q, Sw );
  qInterpreter_.eval_Sn( q, Sn );

  //Evaluate relative permeabilities
  T krw = kr_.relativeperm(Sw);
  T krn = kr_.relativeperm(Sn);

  //Evaluate phase mobilities
  T lambda_w = krw/mu_w_;
  T lambda_n = krn/mu_n_;

  //Evaluate relative permeability jacobians
  T krw_Sw, krn_Sn;
  kr_.relativepermJacobian(Sw, krw_Sw);
  kr_.relativepermJacobian(Sn, krn_Sn);

  Real dSn_dSw = -1;
  T krn_Sw = krn_Sn * dSn_dSw;

  //Evaluate phase mobility jacobians
  T lambda_w_Sw = krw_Sw/mu_w_;
  T lambda_n_Sw = krn_Sw/mu_n_;

  //Evaluate capillary pressure jacobian and hessian
  T pc_Sn, pc_Sn2;
  pc_.capPressureJacobian( x, time, Sn, pc_Sn );
  pc_.capPressureHessian( x, time, Sn, pc_Sn2 );

  T pc_Sw = pc_Sn * dSn_dSw;
  T pc_Sw2 = pc_Sn2;

  MatrixQ<T> tmp = (lambda_w*lambda_n)/(lambda_w + lambda_n);
  MatrixQ<T> tmp_Sw = ((lambda_w_Sw*lambda_n + lambda_w*lambda_n_Sw) - tmp*(lambda_w_Sw + lambda_n_Sw))/(lambda_w + lambda_n);

  DLA::index(kxx_u, 0, 0) += -(K_/phi_)*(tmp_Sw*pc_Sw + tmp*pc_Sw2);
}

// strong form of viscous flux: d(Fv)/d(X)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel>::strongFluxViscous(
    const Real& x, const Real& time, const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qxx, ArrayQ<T>& strongPDE ) const
{
  //Get viscous diffusion matrix
  MatrixQ<T> kxx = 0;
  diffusionViscous( x, time, q, qx, kxx);

  //Get jacobian of viscous diffusion matrix
  MatrixQ<T> kxx_u = 0;
  jacobianDiffusionViscous( x, time, q, kxx_u);

  T Swx, Swxx;
  qInterpreter_.eval_SwGradient( q, qx, Swx );
  qInterpreter_.eval_SwHessian( q, qx, qxx, Swxx );

  DLA::index(strongPDE, 0, 0) -= DLA::index(kxx_u, 0, 0)*(Swx*Swx) + DLA::index(kxx, 0, 0)*Swxx;
}

// source terms
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Ts>
inline void
PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel>::source(
    const Real& x, const Real& time, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    ArrayQ<Ts>& source ) const
{
#if 0
  Tq Sw;
  qInterpreter_.eval_Sw( q, Sw );
  Real scale = 1.0;

  if (Sw < 0.0)
  {
    source += scale*pow(Sw, 2.0); //Add a source if Sw < 0
  }
  else if (Sw > 1.0)
  {
    source -= scale*pow(Sw - 1.0, 2.0); //Add a sink if Sw > 1
  }
#endif
}

// right-hand-side forcing function
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel>::forcingFunction(
    const Real& x, const Real& time, ArrayQ<T>& forcing ) const
{
  if (force_ == nullptr)
    SANS_DEVELOPER_EXCEPTION("PDEBuckleyLeverett1D::forcingFunction - No forcing function!");
  else
  {
    ArrayQ<Real> RHS = 0;
    (*force_)(*this, x, time, RHS);
  //  std::cout<<RHS<<std::endl;
    forcing += RHS;
  }
}

#if 0
// characteristic speed (needed for timestep)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template<class T>
inline void
PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel>::speedCharacteristic(
    const Real& x, const Real& time, const Real& dx, const ArrayQ<T>& q, T& speed ) const
{
  T rho, u, t, c;

  qInterpreter_.eval( q, rho, u,t );
  c = gas_.speedofSound(t);

  speed = fabs(dx*u)/sqrt(dx*dx) + c;
}
#endif

// characteristic speed
template <template <class> class PDETraitsSize, class PDETraitsModel>
template<class T>
inline void
PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel>::speedCharacteristic(
    const Real& x, const Real& time, const ArrayQ<T>& q, T& speed ) const
{
  MatrixQ<T> ax = 0;

  jacobianFluxAdvective(x, time, q, ax);

  speed = fabs(DLA::index(ax, 0, 0));
}

// update fraction needed for physically valid state
template <template <class> class PDETraitsSize, class PDETraitsModel>
void
PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel>::updateFraction(
    const Real& x, const Real& time, const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
    const Real maxChangeFraction, Real& updateFraction ) const
{
  SANS_DEVELOPER_EXCEPTION("Not implemented");
}

// is state physically valid
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline bool
PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel>::isValidState( const ArrayQ<Real>& q ) const
{
  return true; //qInterpreter_.isValidState(q);
}

// set from primitive variable array
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel>::setDOFFrom(
    ArrayQ<T>& q, const Real data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn >= N);
  qInterpreter_.setFromPrimitive( q, data, name, nn );
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel>::setDOFFrom(
    ArrayQ<T>& q, const Real data, const std::string& name ) const
{
  qInterpreter_.setFromPrimitive( q, data, name );
}

// interpret residuals of the solution variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
void
PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel>::interpResidVariable(
    const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
{
  int nOut = rsdPDEOut.m();

  SANS_ASSERT(nOut == N);
  for (int i = 0; i < nOut; i++)
    rsdPDEOut[i] = DLA::index(rsdPDEIn, i);
}

// interpret residuals of the gradients in the solution variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
void
PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel>::interpResidGradient(
    const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const
{
  int nOut = rsdAuxOut.m();

  SANS_ASSERT(nOut == N);
  for (int i = 0; i < nOut; i++)
    rsdAuxOut[i] = DLA::index(rsdAuxIn, i);
}

// interpret residuals at the boundary (should forward to BCs)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
void
PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel>::interpResidBC(
    const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const
{
  int nOut = rsdBCOut.m();

  SANS_ASSERT(nOut == N);
  for (int i = 0; i < nOut; i++)
    rsdBCOut[i] = DLA::index(rsdBCIn, i);
}

// how many residual equations are we dealing with
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline int
PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel>::nMonitor() const
{
  return N;
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
void
PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  std::string indent2(indentSize+2, ' ');

  out << indent << "PDEBuckleyLeverett1D<QType, RelPermModel>:" << std::endl;
  out << indent2 << "qInterpret_ = " << std::endl;
  qInterpreter_.dump(indentSize+4, out);
  out << indent2 << "kr_ = " << std::endl;
  kr_.dump(indentSize+4, out);
  out << indent2 << "phi_ = " << phi_ << std::endl;
  out << indent2 << "uT_ = " << uT_ << std::endl;
  out << indent2 << "mu_w_ = " << mu_w_ << std::endl;
  out << indent2 << "mu_n_ = " << mu_n_ << std::endl;
  out << indent2 << "K_ = " << K_ << std::endl;
  out << indent2 << "pc_ = " << std::endl;
  pc_.dump(indentSize+4, out);
  out << indent2 << "force_ = " << std::endl;
  if (force_ !=NULL)
    (*force_).dump(indentSize+4, out);
}


} //namespace SANS

#endif  // PDEBUCKLEYLEVERETT1D_H
