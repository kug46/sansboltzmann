// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef Q2DPRIMITIVE_SURROGATE_PNSW_H
#define Q2DPRIMITIVE_SURROGATE_PNSW_H

// solution interpreter class
// Two-phase eqn: primitive variables (non-wetting pressure pn and surrogate wetting saturation Sw)

#include "Topology/Dimension.h"
#include "TwoPhaseVariable.h"

#include <string>

namespace SANS
{

//----------------------------------------------------------------------------//
// solution variable interpreter: Two-phase flow
// primitive variables (non-wetting pressure pn and wetting saturation Sw)
//
// template parameters:
//   T                    solution DOF data type (e.g. double)
//   QType                solution variable set (e.g. primitive)
//
// member functions:
//   .eval_Sw            extract wetting phase saturation (Sw)
//   .eval_Sn            extract non-wetting phase saturation (Sn)
//   .eval_SwGradient    extract wetting phase saturation gradient
//   .eval_SnGradient    extract non-wetting phase saturation gradient
//   .isValidState       T/F: determine if state is physically valid (e.g. pn > 0 and Sw >= 0 and Sw <= 1)
//   .setfromPrimitive   set from primitive variable array
//
//   .dump                debug dump of private data
//----------------------------------------------------------------------------//


// primitive variables (pn, Sw)
class QTypePrimitive_Surrogate_pnSw;


// primary template
template <class QType, class CapillaryModel, template <class> class PDETraitsSize>
class Q2D;


template <class CapillaryModel, template <class> class PDETraitsSize>
class Q2D<QTypePrimitive_Surrogate_pnSw, CapillaryModel, PDETraitsSize>
{
public:
  typedef PhysD2 PhysDim;

  static const int D = PhysDim::D;                   // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;    // total solution variables

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>; // solution arrays

  explicit Q2D( const CapillaryModel& pc, const Real eps = 0.01 ) : pc_(pc), eps_(eps) {}
  Q2D( const Q2D& q0 ) : pc_(q0.pc_), eps_(q0.eps_) {}
  ~Q2D() {}

  Q2D& operator=( const Q2D& );

  // evaluate primitive variables
  template<class T>
  void eval( const ArrayQ<T>& q, T& pw, T& pn, T& Sw, T& Sn ) const;
  template<class T>
  void eval_pw( const ArrayQ<T>& q, T& pw ) const; //wetting phase pressure - pw
  template<class T>
  void eval_pn( const ArrayQ<T>& q, T& pn ) const; //non-wetting phase pressure - pn
  template<class T>
  void eval_Sw( const ArrayQ<T>& q, T& Sw ) const; //wetting phase saturation - Sw
  template<class T>
  void eval_Sn( const ArrayQ<T>& q, T& Sn ) const; //non-wetting phase saturation - Sn

  template<class Tq, class Tg, class T>
  void eval_pwGradient( const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, T& pwx, T& pwy ) const;
  template<class Tq, class Tg, class T>
  void eval_pnGradient( const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, T& pnx, T& pny ) const;
  template<class Tq, class Tg, class T>
  void eval_SwGradient( const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, T& Swx, T& Swy ) const;
  template<class Tq, class Tg, class T>
  void eval_SnGradient( const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, T& Snx, T& Sny ) const;

  template<class T>
  void jacobian_pw( const ArrayQ<T>& q, ArrayQ<T>& pw_q ) const;
  template<class T>
  void jacobian_pn( const ArrayQ<T>& q, ArrayQ<T>& pn_q ) const;
  template<class T>
  void jacobian_Sw( const ArrayQ<T>& q, ArrayQ<T>& Sw_q ) const;
  template<class T>
  void jacobian_Sn( const ArrayQ<T>& q, ArrayQ<T>& Sn_q ) const;

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const;

  // set from primitive variable array
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const Real data[], const std::string name[], int nn ) const;

  // set from variables
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const PressureNonWet_SaturationWet<T>& data ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:

  template<class T>
  void transform(const T& Sw0, T& Sw) const;

  template<class T>
  void inverseTransform(const T& Sw, T& Sw0) const;

  template<class T>
  void transformJacobian( const T& Sw0, T& Sw_Sw0 ) const;

  const CapillaryModel& pc_; //Capillary pressure model
  const Real eps_;
};

template <class CapillaryModel, template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitive_Surrogate_pnSw, CapillaryModel, PDETraitsSize>::transform( const T& Sw0, T& Sw ) const
{
  if (Sw0 < eps_)
  {
    T f = Sw0/eps_;
    Sw = eps_/( 3.0 - 3.0*f + f*f );
  }
  else if (Sw0 > 1.0 - eps_)
  {
    T f = (1.0 - Sw0)/eps_;
    Sw = 1.0 - eps_/( 3.0 - 3.0*f + f*f );
  }
  else
  {
    Sw = Sw0;
  }
}

template <class CapillaryModel, template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitive_Surrogate_pnSw, CapillaryModel, PDETraitsSize>::inverseTransform( const T& Sw, T& Sw0 ) const
{
  Real epssq_ = eps_*eps_;

  if (Sw < eps_)
  {
    if (Sw <= 0.0)
    {
      Sw0 = -10.0;
    }
    else
    {
      T a = Sw/epssq_;
      T b = -3.0*Sw/eps_;
      T c = 3.0*Sw - eps_;
      Sw0 = (-b - sqrt(b*b - 4.0*a*c)) / (2.0*a);
    }
  }
  else if (Sw > 1.0 - eps_)
  {
    if (Sw >= 1.0)
    {
      Sw0 = 11.0;
    }
    else
    {
      T Sn = 1.0 - Sw;
      T a = Sn/epssq_;
      T b = (3.0*eps_ - 2.0)*Sn/epssq_;
      T c = (3.0 - 3.0/eps_ + 1.0/epssq_)*Sn - eps_;
      Sw0 = (-b + sqrt(b*b - 4.0*a*c)) / (2.0*a);
    }
  }
  else
  {
    Sw0 = Sw;
  }
}

template <class CapillaryModel, template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitive_Surrogate_pnSw, CapillaryModel, PDETraitsSize>::transformJacobian( const T& Sw0, T& Sw_Sw0 ) const
{
  if (Sw0 < eps_)
  {
    T f = Sw0/eps_;
    Sw_Sw0 = (3.0 - 2.0*f) / pow( 3.0 - 3.0*f + f*f, 2.0 );
  }
  else if (Sw0 > 1.0 - eps_)
  {
    T f = (1.0 - Sw0)/eps_;
    Sw_Sw0 = (3.0 - 2.0*f) / pow( 3.0 - 3.0*f + f*f, 2.0 );
  }
  else
  {
    Sw_Sw0 = 1.0;
  }
}

template <class CapillaryModel, template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitive_Surrogate_pnSw, CapillaryModel, PDETraitsSize>::eval( const ArrayQ<T>& q, T& pw, T& pn, T& Sw, T& Sn ) const
{
  pn = q(0);
  T Sw0 = q(1);

  transform(Sw0, Sw);

  Sn = 1.0 - Sw;

  Real dummy = 0.0;
  T pc  = pc_.capPressure(dummy, dummy, Sn);

  pw = pn - pc;

}

template <class CapillaryModel, template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitive_Surrogate_pnSw, CapillaryModel, PDETraitsSize>::eval_pw( const ArrayQ<T>& q, T& pw ) const
{
  T Sw;
  transform(q(1), Sw);

  T Sn = 1.0 - Sw;

  Real dummy = 0.0;
  T pc = pc_.capPressure(dummy, dummy, Sn);

  pw = q(0) - pc;
}

template <class CapillaryModel, template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitive_Surrogate_pnSw, CapillaryModel, PDETraitsSize>::eval_pn( const ArrayQ<T>& q, T& pn ) const
{
  pn = q(0);
}

template <class CapillaryModel, template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitive_Surrogate_pnSw, CapillaryModel, PDETraitsSize>::eval_Sw( const ArrayQ<T>& q, T& Sw ) const
{
  transform(q(1), Sw);
}

template <class CapillaryModel, template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitive_Surrogate_pnSw, CapillaryModel, PDETraitsSize>::eval_Sn( const ArrayQ<T>& q, T& Sn ) const
{
  T Sw;
  transform(q(1), Sw);
  Sn = 1.0 - Sw;
}

template <class CapillaryModel, template <class> class PDETraitsSize>
template <class Tq, class Tg, class T>
inline void
Q2D<QTypePrimitive_Surrogate_pnSw, CapillaryModel, PDETraitsSize>::eval_pwGradient( const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
                                                                   T& pwx, T& pwy ) const
{
  Tq Sw, Sw_Sw0;
  transform(q(1), Sw);
  transformJacobian(q(1), Sw_Sw0);

  Tq Sn = 1.0 - Sw;
  T Snx = -Sw_Sw0*qx(1);
  T Sny = -Sw_Sw0*qy(1);

  Real dummy = 0.0;
  T pcx, pcy;
  pc_.capPressureGradient(dummy, dummy, dummy, Sn, Snx, Sny, pcx, pcy);

  pwx = qx(0) - pcx;
  pwy = qy(0) - pcy;
}

template <class CapillaryModel, template <class> class PDETraitsSize>
template <class Tq, class Tg, class T>
inline void
Q2D<QTypePrimitive_Surrogate_pnSw, CapillaryModel, PDETraitsSize>::eval_pnGradient( const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
                                                                   T& pnx, T& pny ) const
{
  pnx = qx(0);
  pny = qy(0);
}

template <class CapillaryModel, template <class> class PDETraitsSize>
template <class Tq, class Tg, class T>
inline void
Q2D<QTypePrimitive_Surrogate_pnSw, CapillaryModel, PDETraitsSize>::eval_SwGradient( const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
                                                         T& Swx, T& Swy ) const
{
  Tq Sw_Sw0;
  transformJacobian(q(1), Sw_Sw0);

  Swx = Sw_Sw0*qx(1);
  Swy = Sw_Sw0*qy(1);
}

template <class CapillaryModel, template <class> class PDETraitsSize>
template <class Tq, class Tg, class T>
inline void
Q2D<QTypePrimitive_Surrogate_pnSw, CapillaryModel, PDETraitsSize>::eval_SnGradient( const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
                                                         T& Snx, T& Sny ) const
{
  Tq Sw_Sw0;
  transformJacobian(q(1), Sw_Sw0);

  Snx = -Sw_Sw0*qx(1);
  Sny = -Sw_Sw0*qy(1);
}

template <class CapillaryModel, template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitive_Surrogate_pnSw, CapillaryModel, PDETraitsSize>::jacobian_pw( const ArrayQ<T>& q, ArrayQ<T>& pw_q ) const
{
  T Sw, Sw_Sw0;
  transform(q(1), Sw);
  transformJacobian(q(1), Sw_Sw0);

  T Sn = 1.0 - Sw;

  Real dummy = 0.0;
  T pc_Sn;
  pc_.capPressureJacobian( dummy, dummy, Sn, pc_Sn );
  T pc_Sw = -pc_Sn;

  // pw = pn - pc(Sw)
  pw_q(0) = 1.0; //dpw/dpn
  pw_q(1) = -pc_Sw*Sw_Sw0; //dpw/dSw
}

template <class CapillaryModel, template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitive_Surrogate_pnSw, CapillaryModel, PDETraitsSize>::jacobian_pn( const ArrayQ<T>& q, ArrayQ<T>& pn_q ) const
{
  pn_q(0) = 1.0; //dpn/dpn
  pn_q(1) = 0.0; //dpn/dSw
}

template <class CapillaryModel, template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitive_Surrogate_pnSw, CapillaryModel, PDETraitsSize>::jacobian_Sw( const ArrayQ<T>& q, ArrayQ<T>& Sw_q ) const
{
  T Sw_Sw0;
  transformJacobian(q(1), Sw_Sw0);

  Sw_q(0) = 0.0; //dSw/dpn
  Sw_q(1) = Sw_Sw0; //dSw/dSw
}

template <class CapillaryModel, template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitive_Surrogate_pnSw, CapillaryModel, PDETraitsSize>::jacobian_Sn( const ArrayQ<T>& q, ArrayQ<T>& Sn_q ) const
{
  T Sw_Sw0;
  transformJacobian(q(1), Sw_Sw0);

  Sn_q(0) = 0.0; //dSn/dpn
  Sn_q(1) = -Sw_Sw0; //dSn/dSw
}

// is state physically valid: check for 0 <= Sw <= 1
template <class CapillaryModel, template <class> class PDETraitsSize>
inline bool
Q2D<QTypePrimitive_Surrogate_pnSw, CapillaryModel, PDETraitsSize>::isValidState( const ArrayQ<Real>& q ) const
{
  Real pn = q(0);

  Real Sw;
  transformJacobian(q(1), Sw);

  return ((pn > 0) && (Sw >= 0) && (Sw <= 1));
}

template <class CapillaryModel, template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitive_Surrogate_pnSw, CapillaryModel, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const Real data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn == N);
  SANS_ASSERT(name[0] == "pn");
  SANS_ASSERT(name[1] == "Sw");

  Real Sw0;
  inverseTransform(data[1], Sw0);

  q(0) = data[0]; //pn0
  q(1) = Sw0; //Sw0
}

template <class CapillaryModel, template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitive_Surrogate_pnSw, CapillaryModel, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const PressureNonWet_SaturationWet<T>& data ) const
{
  T Sw0;
  inverseTransform(data.Sw, Sw0);

  q(0) = data.pn;
  q(1) = Sw0;
}

}

#endif // Q2DPRIMITIVE_SURROGATE_PNSW_H
