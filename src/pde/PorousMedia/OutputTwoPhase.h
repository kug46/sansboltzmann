// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef OUTPUTTWOPHASE_H
#define OUTPUTTWOPHASE_H

#include "Topology/Dimension.h"

#include "SourceTwoPhase1D.h"
#include "SourceTwoPhase2D.h"

#include "pde/OutputCategory.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Output functions to compute single output from two-phase primitive variables

template <class PDE_>
class OutputTwoPhase_SaturationPower : public OutputType< OutputTwoPhase_SaturationPower<PDE_> >
{
public:
  typedef PDE_ PDE;
  typedef typename PDE::PhysDim PhysDim;

  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  // Array of outputs
  template<class T>
  using ArrayJ = T;

  // Matrix required to represent the transposed Jacobian of this output functional
  template<class T>
  using MatrixJ = ArrayQ<T>;

  OutputTwoPhase_SaturationPower( const PDE& pde, const int phase,
                                  const int exponent, const double a = 1.0 ) :
    pde_(pde), phase_(phase), exponent_(exponent), a_(a)
  {
    SANS_ASSERT(phase_ == 0 || phase_ == 1);
  }

  bool needsSolutionGradient() const { return false; }

  template<class T>
  void operator()(const Real& x, const Real& time,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayJ<T>& output ) const
  {
    T S = 0;
    if (phase_ == 0)
      pde_.variableInterpreter().eval_Sw( q, S );
    else
      pde_.variableInterpreter().eval_Sn( q, S );

    output = a_*pow(S, exponent_);
  }

  template<class Tp, class T>
  void operator()(const Tp& param, const Real& x, const Real& time,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayJ<T>& output ) const
  {
    this->operator ()(x, time, q, qx, output);
  }

  template<class T>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayJ<T>& output ) const
  {
    this->operator ()(x, time, q, qx, output);
  }

  template<class Tp, class T>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& time,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayJ<T>& output ) const
  {
    this->operator ()(x, time, q, qx, output);
  }

  template<class T>
  void operator()(const Real& x, const Real& y, const Real& z, const Real& time,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz, ArrayJ<T>& output ) const
  {
    this->operator ()(x, time, q, qx, output);
  }

private:
  const PDE& pde_;
  const int phase_;
  const int exponent_;
  const double a_;
};


//----------------------------------------------------------------------------//
template <class PDE_>
class OutputTwoPhase_PressurePower : public OutputType< OutputTwoPhase_PressurePower<PDE_> >
{
public:
  typedef PDE_ PDE;
  typedef typename PDE::PhysDim PhysDim;

  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  // Array of outputs
  template<class T>
  using ArrayJ = T;

  // Matrix required to represent the transposed Jacobian of this output functional
  template<class T>
  using MatrixJ = ArrayQ<T>;

  OutputTwoPhase_PressurePower( const PDE& pde, const int phase,
                                const int exponent, const double a = 1.0 ) :
    pde_(pde), phase_(phase), exponent_(exponent), a_(a)
  {
    SANS_ASSERT(phase_ == 0 || phase_ == 1);
  }

  bool needsSolutionGradient() const { return false; }

  template<class T>
  void operator()(const Real& x, const Real& time,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayJ<T>& output ) const
  {
    T p = 0;
    if (phase_ == 0)
      pde_.variableInterpreter().eval_pw( q, p );
    else
      pde_.variableInterpreter().eval_pn( q, p );

    output = a_*pow(p, exponent_);
  }

  template<class Tp, class T>
  void operator()(const Tp& param, const Real& x, const Real& time,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayJ<T>& output ) const
  {
    this->operator ()(x, time, q, qx, output);
  }

  template<class T>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayJ<T>& output ) const
  {
    this->operator ()(x, time, q, qx, output);
  }

  template<class Tp, class T>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& time,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayJ<T>& output ) const
  {
    this->operator ()(x, time, q, qx, output);
  }

  template<class T>
  void operator()(const Real& x, const Real& y, const Real& z, const Real& time,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz, ArrayJ<T>& output ) const
  {
    this->operator ()(x, time, q, qx, output);
  }

private:
  const PDE& pde_;
  const int phase_;
  const int exponent_;
  const double a_;
};


//----------------------------------------------------------------------------//
template <class QInterpreter, class DensityModelw, class DensityModeln,
          class RelPermModelw, class RelPermModeln, class ViscModelw, class ViscModeln, class CapillaryModel>
class OutputTwoPhase1D_FixedWellPressure : public SourceTwoPhase1DBase,
  public OutputType< OutputTwoPhase1D_FixedWellPressure<QInterpreter, DensityModelw, DensityModeln,
                                                        RelPermModelw, RelPermModeln,
                                                        ViscModelw, ViscModeln, CapillaryModel> >
{
public:

  typedef PhysD1 PhysDim;
  static_assert( std::is_same< PhysDim, typename QInterpreter::PhysDim >::value, "Inconsistent physical dimensions.");

  template <class T>
  using ArrayQ = typename QInterpreter::template ArrayQ<T>;   // solution/residual arrays

  // Array of outputs
  template<class T>
  using ArrayJ = T;

  // Matrix required to represent the transposed Jacobian of this output functional
  template<class T>
  using MatrixJ = ArrayQ<T>;

  typedef SourceTwoPhase1DBase BaseType;
  typedef SourceTwoPhase1DParam BaseParamsType;
  typedef SourceTwoPhase1DType_FixedWellPressure_Param ParamsType;

  explicit OutputTwoPhase1D_FixedWellPressure(
      const PyDict& d, const DensityModelw& rhow, const DensityModeln& rhon,
      const RelPermModelw& krw, const RelPermModeln& krn, const ViscModelw& muw, const ViscModeln& mun,
      const Real K, const CapillaryModel& pc)
  : BaseType(d),
    pB_(d.get(BaseParamsType::params.Source).get(ParamsType::params.pB)),
    rhow_(rhow), rhon_(rhon), krw_(krw), krn_(krn), muw_(muw), mun_(mun),
    K_(K), pc_(pc), qInterpreter_(pc_){}

  bool needsSolutionGradient() const { return false; }

  template<class Tq, class Tg, class Tf>
  void operator()(const Real& x, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, ArrayJ<Tf>& output ) const
  {
    Real weight = getWeight(x, time);

    if (weight < 100*std::numeric_limits<Real>::epsilon())
    {
      output = 0.0; //The weighting factor is too small
      return;
    }

    Real Ls = getLength(); //Well size
    Real Lchar = 0.25*Ls; //Characteristic length scale

    Tq Sw = 0, Sn = 0, pw = 0, pn = 0;
    qInterpreter_.eval( q, pw, pn, Sw, Sn );

    //Evaluate relative permeabilities
//    T krw = krw_.relativeperm(Sw);
    Tq krn = krn_.relativeperm(Sn);

    //Evaluate phase viscosities
//    T muw = muw_.viscosity(x, time);
    Tq mun = mun_.viscosity(x, time);

    //Evaluate phase mobilities
//    T lambda_w = krw/muw;
    Tq lambda_n = krn/mun;

    Tq dpndx = (pn - pB_)/Lchar;

    output = weight*K_*lambda_n*dpndx/Ls; //Phase2 extracted volume (Oil)
  }

  ~OutputTwoPhase1D_FixedWellPressure() {}

protected:
  Real pB_;  //Fixed Bottom hole pressure

  const DensityModelw& rhow_; //Density model for wetting phase
  const DensityModeln& rhon_; //Density model for non-wetting phase

  const RelPermModelw& krw_; //Relative permeability model for wetting phase
  const RelPermModeln& krn_; //Relative permeability model for non-wetting phase

  const ViscModelw& muw_; //Viscosity model for wetting phase
  const ViscModeln& mun_; //Viscosity model for non-wetting phase

  Real K_; // Permeability (assumed to be constant for now)
  const CapillaryModel& pc_; //Capillary pressure model

  const QInterpreter qInterpreter_; // solution variable interpreter class
};


//----------------------------------------------------------------------------//
template <class SourceClass>
class OutputTwoPhase2D_Flowrate :
    public OutputType< OutputTwoPhase2D_Flowrate<SourceClass> >
{
public:

  typedef PhysD2 PhysDim;
  typedef typename SourceClass::QInterpreter QInterpreter;
  static_assert( std::is_same< PhysDim, typename QInterpreter::PhysDim >::value,
                 "Inconsistent physical dimensions.");

  template <class T>
  using ArrayQ = typename QInterpreter::template ArrayQ<T>;   // solution/residual arrays

  // Array of outputs
  template<class T>
  using ArrayJ = T;

  // Matrix required to represent the transposed Jacobian of this output functional
  template<class T>
  using MatrixJ = ArrayQ<T>;

  OutputTwoPhase2D_Flowrate(const std::vector<SourceClass>& sourceList, const int phase)
  : sourceList_(sourceList), phase_(phase) {}

  // scalar source initializer deprecated; causes memory leaks & the fix is not straightforward
  //OutputTwoPhase2D_Flowrate(const SourceClass& source, const int phase)
  //: sourceList_(*(new std::vector<SourceClass>(1, source))), phase_(phase) {}

  bool needsSolutionGradient() const { return false; }

  template<class T>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayJ<T>& output ) const
  {
    ArrayQ<T> source;
    output = 0;

    for (size_t n = 0; n < sourceList_.size(); n++)
    {
      source = 0;
      sourceList_[n].source(x, y, time, q, qx, qy, source);

      if (phase_ == 0)
        output += source[0];              // Extracted mass/volume of wetting phase (water)
      else if (phase_ == 1)
        output += source[1];              // Extracted mass/volume of non-wetting phase (oil)
      else if (phase_ == 2)
        output += source[0] + source[1];  // Extracted total mass/volume
    }
  }

  template<class Tp, class T>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& time,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayJ<T>& output ) const
  {
    this->operator()(x, y, time, q, qx, qy, output);
  }

  ~OutputTwoPhase2D_Flowrate() {}

protected:
  const std::vector<SourceClass>& sourceList_;
  const int phase_;
};


//----------------------------------------------------------------------------//
template <class SourceClass>
class OutputTwoPhase2D_BottomHolePressure :
    public OutputType< OutputTwoPhase2D_BottomHolePressure<SourceClass> >
{
public:

  typedef PhysD2 PhysDim;
  typedef typename SourceClass::QInterpreter QInterpreter;
  static_assert( std::is_same< PhysDim, typename QInterpreter::PhysDim >::value,
                 "Inconsistent physical dimensions.");

  template <class T>
  using ArrayQ = typename QInterpreter::template ArrayQ<T>;   // solution/residual arrays

  // Array of outputs
  template<class T>
  using ArrayJ = DLA::VectorS<2,T>;

  // Matrix required to represent the transposed Jacobian of this output functional
  template<class T>
  using MatrixJ = DLA::MatrixS<2,ArrayQ<T>::M,T>;

  OutputTwoPhase2D_BottomHolePressure(const SourceClass& source, const Real& rw)
  : source_(source), rw_(rw){}

  bool needsSolutionGradient() const { return false; }

  template<class T>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayJ<T>& output ) const
  {
    output = 0;
    source_.bottomHolePressureIntegrand(x, y, time, q, rw_, output);
  }

  template<class Tp, class T>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& time,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayJ<T>& output ) const
  {
    this->operator ()(x, y, time, q, qx, qy, output);
  }

  ~OutputTwoPhase2D_BottomHolePressure() {}

protected:
  const SourceClass& source_;
  const Real rw_; //well-bore radius
};

}

#endif //OUTPUTTWOPHASE_H
