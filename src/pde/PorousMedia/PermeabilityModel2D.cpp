// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "PermeabilityModel2D.h"

namespace SANS
{

void PermeabilityModel2D_Constant_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Kxx));
  allParams.push_back(d.checkInputs(params.Kxy));
  allParams.push_back(d.checkInputs(params.Kyx));
  allParams.push_back(d.checkInputs(params.Kyy));
  d.checkUnknownInputs(allParams);
}
PermeabilityModel2D_Constant_Params PermeabilityModel2D_Constant_Params::params;

void
PermeabilityModel2D_Constant::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PermeabilityModel2D_Constant:" << std::endl;
  out << indent << "  Kxx_ = " << Kxx_ << std::endl;
  out << indent << "  Kxy_ = " << Kxy_ << std::endl;
  out << indent << "  Kyx_ = " << Kyx_ << std::endl;
  out << indent << "  Kyy_ = " << Kyy_ << std::endl;
}

#if 0
void PermeabilityModel2D_RectBlock_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Kxx_ref));
  allParams.push_back(d.checkInputs(params.Kxy_ref));
  allParams.push_back(d.checkInputs(params.Kyx_ref));
  allParams.push_back(d.checkInputs(params.Kyy_ref));
  allParams.push_back(d.checkInputs(params.blocklist_Kxx));
  allParams.push_back(d.checkInputs(params.blocklist_Kxy));
  allParams.push_back(d.checkInputs(params.blocklist_Kyx));
  allParams.push_back(d.checkInputs(params.blocklist_Kyy));
  allParams.push_back(d.checkInputs(params.blocklist_xmin));
  allParams.push_back(d.checkInputs(params.blocklist_xmax));
  allParams.push_back(d.checkInputs(params.blocklist_ymin));
  allParams.push_back(d.checkInputs(params.blocklist_ymax));
  d.checkUnknownInputs(allParams);
}
PermeabilityModel2D_RectBlock_Params PermeabilityModel2D_RectBlock_Params::params;

void
PermeabilityModel2D_RectBlock::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PermeabilityModel2D_RectBlock:" << std::endl;
  out << indent << "  Kxx_ref_ = " << Kxx_ref_ << std::endl;
  out << indent << "  Kxy_ref_ = " << Kxy_ref_ << std::endl;
  out << indent << "  Kyx_ref_ = " << Kyx_ref_ << std::endl;
  out << indent << "  Kyy_ref_ = " << Kyy_ref_ << std::endl;
  out << indent << "  Block count = " << (int) block_Kxx_.size() << std::endl;
}
#endif

void PermeabilityModel2D_CartTable_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.PermeabilityFile));
  allParams.push_back(d.checkInputs(params.scale_factor));
  d.checkUnknownInputs(allParams);
}
PermeabilityModel2D_CartTable_Params PermeabilityModel2D_CartTable_Params::params;

void
PermeabilityModel2D_CartTable::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PermeabilityModel2D_CartTable:" << std::endl;
}

}
