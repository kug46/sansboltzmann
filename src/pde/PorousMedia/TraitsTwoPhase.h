// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef TRAITSTWOPHASE_H
#define TRAITSTWOPHASE_H

#include "Topology/Dimension.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h" // need to remove this later, switch to templates
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"

namespace SANS
{

template<class PhysDim_>
class TraitsSizeTwoPhase
{
public:
  typedef PhysDim_ PhysDim;
  static const int D = PhysDim::D; // physical dimensions
  static const int N = 2;          // total solution variables

  template <class T>
  using ArrayQ = DLA::VectorS<N,T>;  // solution/residual arrays

  template <class T>
  using VectorArrayQ = DLA::VectorS<D, ArrayQ<T> >;

  template <class T>
  using MatrixQ = DLA::MatrixS<N,N,T>; // matrices
};


template <class QType_, class DensityModelw_, class DensityModeln_, class PorosityModel_,
          class RelPermModelw_, class RelPermModeln_, class ViscModelw_, class ViscModeln_,
          class RockPermModel_, class CapillaryModel_>
class TraitsModelTwoPhase
{
public:
  typedef QType_ QType;
  typedef DensityModelw_ DensityModelw;
  typedef DensityModeln_ DensityModeln;
  typedef PorosityModel_ PorosityModel;
  typedef RelPermModelw_ RelPermModelw;
  typedef RelPermModeln_ RelPermModeln;
  typedef ViscModelw_ ViscModelw;
  typedef ViscModeln_ ViscModeln;
  typedef RockPermModel_ RockPermModel;
  typedef CapillaryModel_ CapillaryModel;
};

}

#endif //TRAITSTWOPHASE_H
