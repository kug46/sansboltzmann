// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCBuckleyLeverett_ArtificialViscosity1D.h"

#include "Q1DPrimitive_Sw.h"
#include "RelPermModel_PowerLaw.h"
#include "CapillaryModel.h"
#include "TraitsBuckleyLeverettArtificialViscosity.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{

//===========================================================================//
// Instantiate the BC parameters

typedef TraitsModelBuckleyLeverett<QTypePrimitive_Sw, RelPermModel_PowerLaw, CapillaryModel_Linear> TraitsModel_RelPower_CapLinear;

typedef BCBuckleyLeverett_ArtificialViscosity1DVector<
            TraitsSizeBuckleyLeverett, TraitsModel_RelPower_CapLinear > BCVector_Sw;

typedef BCBuckleyLeverett_ArtificialViscosity1DVector<
            TraitsSizeBuckleyLeverettArtificialViscosity,
            TraitsModel_RelPower_CapLinear > BCVector_AV_Sw;
BCPARAMETER_INSTANTIATE( BCVector_Sw )
BCPARAMETER_INSTANTIATE( BCVector_AV_Sw )

//===========================================================================//


#if 1
template <class BCBase>
void
BCmitAVSensor1D<BCTypeFlux_mitState, BCBase>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  std::string indent2(indentSize+2, ' ');
  out << indent << "BCBuckleyLeverett_ArtificialViscosity1D<BCTypeFlux_mitState>:" << std::endl;
//  out << indent2 << "baseBC_ = " << std::endl;
//  BCBase::dump(indentSize+4, out);
}

template <class BCBase>
void
BCmitAVSensor1D<BCTypeFlux_mitStateSpaceTime, BCBase>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  std::string indent2(indentSize+2, ' ');
  out << indent << "BCBuckleyLeverett_ArtificialViscosity1D<BCTypeFlux_mitStateSpaceTime>:" << std::endl;
//  out << indent2 << "baseBC_ = " << std::endl;
//  BCBase::dump(indentSize+4, out);
}
#endif

// Explicit instantiations
typedef PDEBuckleyLeverett_ArtificialViscosity1D<TraitsSizeBuckleyLeverett, TraitsModel_RelPower_CapLinear> PDEAV_RelPower_CapLinear;
template class BCNone<PhysD1,TraitsSizeBuckleyLeverett<PhysD1>::N>;
template class BCmitAVSensor1D<BCTypeFlux_mitState, BCBuckleyLeverett1D<BCTypeDirichlet_mitState, PDEAV_RelPower_CapLinear> >;
template class BCmitAVSensor1D<BCTypeFlux_mitState, BCBuckleyLeverett1D<BCTypeFunction_mitState, PDEAV_RelPower_CapLinear> >;
template class BCmitAVSensor1D<BCTypeFlux_mitStateSpaceTime, BCBuckleyLeverett1D<BCTypeTimeIC, PDEAV_RelPower_CapLinear> >;

} //namespace SANS
