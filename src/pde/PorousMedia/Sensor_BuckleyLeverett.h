// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SENSOR_BUCKLEYLEVERETT_H
#define SENSOR_BUCKLEYLEVERETT_H

#include "tools/SANSnumerics.h"     // Real

#include <iostream>
#include <string>

namespace SANS
{

template<class PDE>
class Sensor_BuckleyLeverett
{
public:
  template<class T>
  using ArrayQ = T;  // solution/flux arrays

  typedef typename PDE::QInterpreter QInterpreter; // solution variable interpreter type

  explicit Sensor_BuckleyLeverett(const PDE& pde) :
    qInterpreter_(pde.variableInterpreter())
  {
    // Nothing
  }

  ~Sensor_BuckleyLeverett() {}

  // variable used in calculating jump switch for artificial viscosity
  template<class T, class Tg>
  void jumpQuantity( const ArrayQ<T>& q, Tg& g ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const QInterpreter& qInterpreter_;
};

template<class PDE>
template<class T, class Tg>
void inline
Sensor_BuckleyLeverett<PDE>::
jumpQuantity( const ArrayQ<T>& q, Tg& g ) const
{
  T Sw;
  qInterpreter_.eval_Sw( q, Sw );

  g = Sw;
}

template<class PDE>
void inline
Sensor_BuckleyLeverett<PDE>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "Sensor_BuckleyLeverett: qInterpreter_ =" << std::endl;
  qInterpreter_.dump(indentSize+2, out);
}

} //namespace SANS

#endif  // SENSOR_BUCKLEYLEVERETT_H
