// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOLUTIONFUNCTION_TWOPHASE2D_H
#define SOLUTIONFUNCTION_TWOPHASE2D_H

// Solution functions for two phase flow in 2D

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "tools/SANSnumerics.h"     // Real

#include "TraitsTwoPhase.h"
#include "TwoPhaseVariable.h"

#include "pde/AnalyticFunction/Function2D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// solution: constant pressure field, discontinuous saturation field divided into 3 regions

struct SolutionFunction_TwoPhase2D_SingleWell_Peaceman_Params : noncopyable
{
  const ParameterNumeric<Real> pB{"pB", NO_DEFAULT, 0, NO_LIMIT, "Bottom-hole pressure"};
  const ParameterNumeric<Real> Rwellbore{"Rwellbore", NO_DEFAULT, 0, NO_LIMIT, "Well-bore radius"};
  const ParameterNumeric<Real> Sw{"Sw", NO_DEFAULT, 0, 1, "Specified saturation"};
  const ParameterNumeric<Real> mu{"mu", NO_DEFAULT, 0, NO_LIMIT, "Fluid viscosity"};
  const ParameterNumeric<Real> Q{"Q", NO_DEFAULT, NO_RANGE, "Flow rate"};
  const ParameterNumeric<Real> xcentroid{"xcentroid", NO_DEFAULT, NO_RANGE, "x-coordinate of the well center"};
  const ParameterNumeric<Real> ycentroid{"ycentroid", NO_DEFAULT, NO_RANGE, "y-coordinate of the well center"};
  const ParameterNumeric<Real> Kxx{"Kxx", NO_DEFAULT, 0, NO_LIMIT, "Permeability Kxx"};
  const ParameterNumeric<Real> Kyy{"Kyy", NO_DEFAULT, 0, NO_LIMIT, "Permeability Kyy"};

  static void checkInputs(PyDict d);

  static SolutionFunction_TwoPhase2D_SingleWell_Peaceman_Params params;
};


template <class QInterpreter, template <class> class PDETraitsSize>
class SolutionFunction_TwoPhase2D_SingleWell_Peaceman :
    public Function2DVirtualInterface<SolutionFunction_TwoPhase2D_SingleWell_Peaceman<QInterpreter, PDETraitsSize>, PDETraitsSize<PhysD2>>
{
public:
  typedef PhysD2 PhysDim;

  typedef SolutionFunction_TwoPhase2D_SingleWell_Peaceman_Params ParamsType;

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;  // solution/residual arrays

  SolutionFunction_TwoPhase2D_SingleWell_Peaceman( const PyDict& d, const QInterpreter& qInterpreter ) :
    pB_(d.get(ParamsType::params.pB)),
    rw_(d.get(ParamsType::params.Rwellbore)),
    Sw_(d.get(ParamsType::params.Sw)),
    mu_(d.get(ParamsType::params.mu)),
    Q_(d.get(ParamsType::params.Q)),
    xc_(d.get(ParamsType::params.xcentroid)),
    yc_(d.get(ParamsType::params.ycentroid)),
    Kxx_(d.get(ParamsType::params.Kxx)),
    Kyy_(d.get(ParamsType::params.Kyy)),
    qInterpreter_(qInterpreter) {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    ArrayQ<T> q = 0;

    Real Kxx_Kyy = Kxx_/Kyy_;
    Real Kyy_Kxx = Kyy_/Kxx_;

    T du = pow(Kyy_Kxx, 0.25) * (x - xc_);
    T dv = pow(Kxx_Kyy, 0.25) * (y - yc_);

    T r_uv = sqrt(du*du + dv*dv);
    Real rw_bar = 0.5*rw_*( pow(Kyy_Kxx, 0.25) + pow(Kxx_Kyy, 0.25) );

    T p = pB_ - (mu_*Q_)/(2.0*PI*sqrt(Kxx_*Kyy_))*log(r_uv/rw_bar);

    PressureNonWet_SaturationWet<T> qdata(p, Sw_);
    qInterpreter_.setFromPrimitive(q, qdata);

    return q;
  }

private:
  const Real pB_;
  const Real rw_;
  const Real Sw_;
  const Real mu_, Q_;
  const Real xc_, yc_;
  const Real Kxx_, Kyy_;
  const QInterpreter qInterpreter_;
};

struct SolutionFunction_TwoPhase2D_Reservoir_Params : noncopyable
{
  const ParameterNumeric<Real> Ly{"Ly", NO_DEFAULT, 0, NO_LIMIT, "Total height"};
  const ParameterNumeric<Real> y_owc{"y_owc", NO_DEFAULT, 0, NO_LIMIT, "Oil/water contact height"};
  const ParameterNumeric<Real> rho_o{"rho_o", NO_DEFAULT, 0, NO_LIMIT, "Oil density"};
  const ParameterNumeric<Real> rho_w{"rho_w", NO_DEFAULT, 0, NO_LIMIT, "Water density"};
  const ParameterNumeric<Real> p_offset{"p_offset", NO_DEFAULT, 0, NO_LIMIT, "Offset pressure"};
  const ParameterNumeric<Real> Sw_bottom{"Sw_bottom", NO_DEFAULT, 0, 1, "Wetting-phase saturation in bottom layer"};
  const ParameterNumeric<Real> Sw_top{"Sw_top", NO_DEFAULT, 0, 1, "Wetting-phase saturation in top layer"};

  static void checkInputs(PyDict d);

  static SolutionFunction_TwoPhase2D_Reservoir_Params params;
};


template <class QInterpreter, template <class> class PDETraitsSize>
class SolutionFunction_TwoPhase2D_Reservoir :
    public Function2DVirtualInterface<SolutionFunction_TwoPhase2D_Reservoir<QInterpreter, PDETraitsSize>, PDETraitsSize<PhysD2>>
{
public:
  typedef PhysD2 PhysDim;

  typedef SolutionFunction_TwoPhase2D_Reservoir_Params ParamsType;

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;  // solution/residual arrays

  SolutionFunction_TwoPhase2D_Reservoir( const PyDict& d, const QInterpreter& qInterpreter ) :
    Ly_(d.get(ParamsType::params.Ly)),
    y_owc_(d.get(ParamsType::params.y_owc)),
    rho_o_(d.get(ParamsType::params.rho_o)),
    rho_w_(d.get(ParamsType::params.rho_w)),
    p_offset_(d.get(ParamsType::params.p_offset)),
    Sw_bottom_(d.get(ParamsType::params.Sw_bottom)),
    Sw_top_(d.get(ParamsType::params.Sw_top)),
    qInterpreter_(qInterpreter) {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    ArrayQ<T> q = 0;

    T p, Sw;
    Real g = 6.9468e-3;
    if (y > y_owc_)
    {
      p = p_offset_ + rho_o_ * g * (Ly_ - y);
      Sw = Sw_top_;
    }
    else
    {
      p = p_offset_ + rho_o_ * g * (Ly_ - y_owc_) + rho_w_ * g * (y_owc_ - y);
      Sw = Sw_bottom_;
    }

    PressureNonWet_SaturationWet<T> qdata(p, Sw);
    qInterpreter_.setFromPrimitive(q, qdata);
    return q;
  }

private:
  const Real Ly_;
  const Real y_owc_;
  const Real rho_o_;
  const Real rho_w_;
  const Real p_offset_;
  const Real Sw_bottom_;
  const Real Sw_top_;
  const QInterpreter qInterpreter_;
};

} // namespace SANS

#endif  // SOLUTIONFUNCTION_TWOPHASE2D_H
