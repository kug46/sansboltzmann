// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef TRAITSBUCKLEYLEVERETTARTIFICIALVISCOSITY_H
#define TRAITSBUCKLEYLEVERETTARTIFICIALVISCOSITY_H

#include "TraitsBuckleyLeverett.h"
#include "pde/ArtificialViscosity/TraitsArtificialViscosity.h"

namespace SANS
{

template <class PhysDim_>
using TraitsSizeBuckleyLeverettArtificialViscosity = TraitsSizeArtificialViscosity<TraitsSizeBuckleyLeverett,PhysDim_>;

}

#endif // TRAITSBUCKLEYLEVERETTARTIFICIALVISCOSITY_H
