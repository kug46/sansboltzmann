// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDETWOPHASE1D_H
#define PDETWOPHASE1D_H

// 1-D Two-phase flow PDE class

#include <iostream>
#include <string>

#include "tools/SANSnumerics.h"     // Real
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "tools/smoothmath.h"

#include "Topology/Dimension.h"

#include "SourceTwoPhase1D.h"
#include "TwoPhaseVariable.h"

//#define CONSERVATIVE_FLUX_HACK

namespace SANS
{

// forward declare: solution variables interpreter
template <class QType, class CapillaryModel, template <class> class PDETraitsSize>
class Q1D;

//----------------------------------------------------------------------------//
// PDE class: 1-D Two Phase
//
// equations: d/dt( rho_w phi Sw ) - d/dx( rho_w K lambda_w dpw/dx ) = rho_w q_w
//            d/dt( rho_n phi Sn ) - d/dx( rho_n K lambda_n dpn/dx ) = rho_n q_n
//
// where:    lambda_w = krw/mu_w
//           lambda_n = krn/mu_n
//
// template parameters:
//   T                        solution DOF data type (e.g. double)
//   QType                    solution variable set (e.g. primitive)
//   DensityModel             density models (for each phase)
//   PorosityModel            porosity model
//   RelPermModel             relative permeability models (for each phase)
//   ViscModel                viscosity models (for each phase)
//   CapillaryModel           capillary pressure model
//
// member functions:
//   .hasFluxAdvectiveTime        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//   .hasFluxViscous             T/F: PDE has viscous flux term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: dU/dQ
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .fluxViscous                viscous fluxes: Fv(Q, QX)
//   .diffusionViscous           viscous diffusion coefficient: d(Fv)/d(UX)
//   .isValidState               T/F: determine if state is physically valid (e.g. Sw > 0)
//   .setDOFFrom                 set from primitive variable array
//----------------------------------------------------------------------------//

template <template <class> class PDETraitsSize, class PDETraitsModel>
class PDETwoPhase1D
{
public:
  typedef PhysD1 PhysDim;
  static const int D = PDETraitsSize<PhysDim>::D; // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N; // total solution variables

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;   // solution/residual arrays

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>; // matrices

  //Extract model traits
  typedef typename PDETraitsModel::QType QType;
  typedef typename PDETraitsModel::DensityModelw DensityModelw;
  typedef typename PDETraitsModel::DensityModeln DensityModeln;
  typedef typename PDETraitsModel::PorosityModel PorosityModel;
  typedef typename PDETraitsModel::RelPermModelw RelPermModelw;
  typedef typename PDETraitsModel::RelPermModeln RelPermModeln;
  typedef typename PDETraitsModel::ViscModelw ViscModelw;
  typedef typename PDETraitsModel::ViscModeln ViscModeln;
  typedef typename PDETraitsModel::RockPermModel RockPermModel;
  typedef typename PDETraitsModel::CapillaryModel CapillaryModel;

  static_assert( std::is_same<RockPermModel,Real>::value, "Permeability model should be a constant Real.");

  typedef Q1D<QType, CapillaryModel, PDETraitsSize> QInterpreter;  // solution variable interpreter type

  //Source term typedefs
  typedef SourceTwoPhase1D_FixedWellPressure<QInterpreter, DensityModelw, DensityModeln,
      RelPermModelw, RelPermModeln, ViscModelw, ViscModeln, CapillaryModel> SourceTwoPhase1D_FixedWellPressureType;

  //No source terms
  PDETwoPhase1D( const DensityModelw& rhow, const DensityModeln& rhon, const PorosityModel& phi,
                 const RelPermModelw& krw, const RelPermModeln& krn, const ViscModelw& muw, const ViscModeln& mun,
                 const Real Kxx, const CapillaryModel& pc, const Real scalar_visc = 0.0 ) :
                   rhow_(rhow), rhon_(rhon), phi_(phi),
                   krw_(krw), krn_(krn), muw_(muw), mun_(mun),
                   Kxx_(Kxx), pc_(pc), qInterpreter_(pc_), scalar_visc_(scalar_visc) {}

  //With source terms
  PDETwoPhase1D( const DensityModelw& rhow, const DensityModeln& rhon, const PorosityModel& phi,
                 const RelPermModelw& krw, const RelPermModeln& krn, const ViscModelw& muw, const ViscModeln& mun,
                 const Real Kxx, const CapillaryModel& pc, const PyDict& PySourceList, const Real scalar_visc = 0.0 );

  PDETwoPhase1D( const PDETwoPhase1D& pde ) :
      rhow_(pde.rhow_), rhon_(pde.rhon_), phi_(pde.phi_),
      krw_(pde.krw_), krn_(pde.krn_), muw_(pde.muw_), mun_(pde.mun_),
      Kxx_(pde.Kxx_), pc_(pde.pc_),
      sources_FixedWellPressure_(pde.sources_FixedWellPressure_),
      qInterpreter_(pde.qInterpreter_), scalar_visc_(pde.scalar_visc_) {}

  ~PDETwoPhase1D() {}

  PDETwoPhase1D& operator=( const PDETwoPhase1D& );

  // flux components
  bool hasFluxAdvectiveTime() const { return true; }
  bool hasFluxAdvective() const { return false; }
  bool hasFluxViscous() const { return true; }
  bool hasSource() const { return (sources_FixedWellPressure_.size() > 0); }
  bool hasSourceTrace() const { return false; }
  bool hasSourceLiftedQuantity() const { return false; }
  bool hasForcingFunction() const { return false; }

  // viscous flux is of the form K(q).gradu
  bool fluxViscousLinearInGradient() const { return true; }
  bool needsSolutionGradientforSource() const { return false; }

  Real permeability() const { return Kxx_; }

  const QInterpreter& variableInterpreter() const { return qInterpreter_; }

  // unsteady temporal flux: Ft(Q)
  template <class T, class Tf>
  void fluxAdvectiveTime(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tf>& ft ) const
  {
    ArrayQ<Tf> ftLocal = 0;
    masterState(x, time, q, ftLocal);
    ft += ftLocal;
  }

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <class T, class Tf>
  void jacobianFluxAdvectiveTime(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& J ) const
  {
    J += DLA::Identity();
  }

  // master state: U(Q)
  template <class T, class Tu>
  void masterState(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tu>& uCons ) const;

  // jacobian of master state wrt q: dU(Q)/dQ
  template<class T>
  void jacobianMasterState(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const;

  // advective flux: F(Q)
  template <class T, class Tf>
  void fluxAdvective(
      const Real& x, const Real& time, const ArrayQ<T>& q, ArrayQ<Tf>& f ) const {}

  template <class Tq, class Tf>
  void fluxAdvectiveUpwind(
      const Real& x, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx,  ArrayQ<Tf>& f ) const {}

  template <class Tq, class Tf>
  void fluxAdvectiveUpwindSpaceTime(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const;

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class T>
  void jacobianFluxAdvective(
      const Real& x, const Real& time, const ArrayQ<T>& q, MatrixQ<T>& ax ) const {}

  // absolute value of advective flux jacobian: |n . d(F)/d(U)|
  template <class T>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Real& x, const Real& time, const ArrayQ<T>& q, const Real& nx, MatrixQ<T>& mtx ) const {}

  // absolute value of advective flux jacobian: |n . d(F)/d(U)|
  template <class T>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Real& x, const Real& time, const ArrayQ<T>& q, const Real& nx, const Real& nt, MatrixQ<T>& mtx ) const {}

  // viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Tf>& f ) const;

  // space-time viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const;

  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
      const Real& nx, ArrayQ<Tf>& fn ) const;

  // space-time viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qtL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qtR,
      const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const;

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Tk>& kxx ) const;

  // space-time viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscousSpaceTime(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxt,
      MatrixQ<Tk>& ktx, MatrixQ<Tk>& ktt ) const;

  // solution-dependent source: S(X, Q, QX)
  template <class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Ts>& source ) const;

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX)
  template <class Tlq, class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& time,
      const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Ts>& s ) const
  {
    source(x, time, q, qx, s); //forward to regular source
  }

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Ts>& dsdu ) const;

  // dual-consistent source
  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& xL, const Real& xR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const {}

  // trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
  template <class Tq, class Ts>
  void sourceLiftedQuantity(
      const Real& xL, const Real& xR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, Ts& s ) const {}

  // right-hand-side forcing function
  template <class T>
  void forcingFunction( const Real& x, const Real& time, ArrayQ<T>& forcing ) const {}

  // characteristic speed (needed for timestep)
  template<class T>
  void speedCharacteristic( const Real& x, const Real& time, const Real& dx, const ArrayQ<T>& q, T& speed ) const;

  // characteristic speed (needed for timestep)
  template<class T, class Ts>
  void speedCharacteristic( const Real& x, const Real& time, const ArrayQ<T>& q, Ts& speed ) const;

  // update fraction needed for physically valid state
  void updateFraction( const Real& x, const Real& time, const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
                       const Real maxChangeFraction, Real& updateFraction ) const;

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const;

  // set from primitive variable array
  template<class T>
  void setDOFFrom( ArrayQ<T>& q, const Real data[], const std::string name[], int nn ) const;

  // set from variable
  template<class T, class Variables>
  void setDOFFrom( ArrayQ<T>& q, const TwoPhaseVariableType<Variables>& data ) const;

  // set from variable
  template<class Variables>
  ArrayQ<Real> setDOFFrom( const TwoPhaseVariableType<Variables>& data ) const
  {
    ArrayQ<Real> q = 0;
    qInterpreter_.setFromPrimitive( q, data.cast() );
    return q;
  }

  // set from variable
  ArrayQ<Real> setDOFFrom( const PyDict& d ) const
  {
    ArrayQ<Real> q = 0;
    DictKeyPair data = d.get(TwoPhaseVariableTypeParams::params.StateVector);

    if ( data == TwoPhaseVariableTypeParams::params.StateVector.PressureNonWet_SaturationWet )
      qInterpreter_.setFromPrimitive( q, PressureNonWet_SaturationWet<Real>(data) );
    else
      SANS_DEVELOPER_EXCEPTION(" Unknown state vector option.");

    return q;
  }

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const;

  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const;

  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const;

  // how many residual equations are we dealing with
  int nMonitor() const;

  std::vector<std::string> derivedQuantityNames() const;

  template<class Tq, class Tg, class Tf>
  void derivedQuantity(
      const int& index, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, Tf& J ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const DensityModelw& rhow_; //Density model for wetting phase
  const DensityModeln& rhon_; //Density model for non-wetting phase

  const PorosityModel& phi_; //Porosity model

  const RelPermModelw& krw_; //Relative permeability model for wetting phase
  const RelPermModeln& krn_; //Relative permeability model for non-wetting phase

  const ViscModelw& muw_; //Viscosity model for wetting phase
  const ViscModeln& mun_; //Viscosity model for non-wetting phase

  Real Kxx_; // Permeability (assumed to be constant for now)
  const CapillaryModel& pc_; //Capillary pressure model

  //Lists of source terms
  std::vector<SourceTwoPhase1D_FixedWellPressureType> sources_FixedWellPressure_; //Fixed well pressure source terms

  const QInterpreter qInterpreter_; // solution variable interpreter class

  const Real scalar_visc_; //additional viscosity (used to stabilize single-phase cases, when Sw = 0 or 1)
};

template <template <class> class PDETraitsSize, class PDETraitsModel>
PDETwoPhase1D<PDETraitsSize, PDETraitsModel>::
PDETwoPhase1D( const DensityModelw& rhow, const DensityModeln& rhon, const PorosityModel& phi,
               const RelPermModelw& krw, const RelPermModeln& krn, const ViscModelw& muw, const ViscModeln& mun,
               const Real Kxx, const CapillaryModel& pc, const PyDict& PySourceList, const Real scalar_visc ) :
               rhow_(rhow), rhon_(rhon), phi_(phi),
               krw_(krw), krn_(krn), muw_(muw), mun_(mun),
               Kxx_(Kxx), pc_(pc), qInterpreter_(pc_), scalar_visc_(scalar_visc)
{
  // Extract the keys from the source list dictionary
  std::vector<std::string> keys = PySourceList.stringKeys();

  for (std::size_t i = 0; i < keys.size(); i++)
  {
    // Create a source dictionary parameter for the given key
    const ParameterDict sourceDictParam{keys[i], NO_DEFAULT, SourceTwoPhase1DParam::checkInputs, "Source dictionary"};

    PyDict sourceDict = PySourceList.get(sourceDictParam); //Extract the source dictionary

    //Extract the sourceType dictionary from previous the source dictionary
    DictKeyPair sourceType = sourceDict.get(SourceTwoPhase1DParam::params.Source);

    if (sourceType == SourceTwoPhase1DParam::params.Source.FixedPressure)
    {
      sources_FixedWellPressure_.emplace_back(sourceDict, rhow, rhon, krw, krn, muw, mun, Kxx, pc);
    }
    else
      SANS_DEVELOPER_EXCEPTION("PDETwoPhase1D - Unknown source type. Code should not get here if checkInputs was called.");
  }
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tu>
inline void
PDETwoPhase1D<PDETraitsSize, PDETraitsModel>::
masterState(
    const Real& x, const Real& time,
    const ArrayQ<T>& q, ArrayQ<Tu>& uCons ) const
{
  T Sw = 0, Sn = 0, pw = 0, pn = 0;
  qInterpreter_.eval( q, pw, pn, Sw, Sn );

  Real dummy = 0.0;

  //Evaluate densities
  T rhow = rhow_.density(dummy, dummy, pw);
  T rhon = rhon_.density(dummy, dummy, pn);

  //Evaluate porosity
  T phi = phi_.porosity(dummy, dummy, pn);

  uCons(0) = phi*rhow*Sw;
  uCons(1) = phi*rhon*Sn;
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDETwoPhase1D<PDETraitsSize, PDETraitsModel>::
jacobianMasterState(
    const Real& x, const Real& time,
    const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
{
#ifdef CONSERVATIVE_FLUX_HACK
  dudq = DLA::Identity(); //Returning identity because diffusionViscous returns d(Fv)/dq instead of d(Fv)/d(UX)
#else
  T Sw = 0, Sn = 0, pw = 0, pn = 0;
  qInterpreter_.eval( q, pw, pn, Sw, Sn );

  //Evaluate jacobians (wrt state vector)
  ArrayQ<T> pw_q = 0, pn_q = 0, Sw_q = 0, Sn_q = 0;
  qInterpreter_.jacobian_pw( q, pw_q );
  qInterpreter_.jacobian_pn( q, pn_q );
  qInterpreter_.jacobian_Sw( q, Sw_q );
  qInterpreter_.jacobian_Sn( q, Sn_q );

  Real dummy = 0.0;

  //Evaluate densities
  T rhow = rhow_.density(dummy, dummy, pw);
  T rhon = rhon_.density(dummy, dummy, pn);
  T rhow_pw, rhon_pn;
  rhow_.densityJacobian(dummy, dummy, pw, rhow_pw);
  rhon_.densityJacobian(dummy, dummy, pn, rhon_pn);

  //Evaluate porosity
  T phi = phi_.porosity(dummy, dummy, pn);
  T phi_pn;
  phi_.porosityJacobian(dummy, dummy, pn, phi_pn);

  //Chain rules
  ArrayQ<T> rhow_q = rhow_pw*pw_q;
  ArrayQ<T> rhon_q = rhon_pn*pn_q;
  ArrayQ<T> phi_q  = phi_pn *pn_q;

  //Compute Jacobian
  dudq(0,0) = (phi_q[0]*rhow + phi*rhow_q[0])*Sw + phi*rhow*Sw_q[0];
  dudq(0,1) = (phi_q[1]*rhow + phi*rhow_q[1])*Sw + phi*rhow*Sw_q[1];
  dudq(1,0) = (phi_q[0]*rhon + phi*rhon_q[0])*Sn + phi*rhon*Sn_q[0];
  dudq(1,1) = (phi_q[1]*rhon + phi*rhon_q[1])*Sn + phi*rhon*Sn_q[1];
#endif
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDETwoPhase1D<PDETraitsSize, PDETraitsModel>::
fluxAdvectiveUpwindSpaceTime(const Real& x, const Real& time,
                             const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
                             const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const
{
  //Full temporal upwinding

  //Compute upwinded spatial advective flux
  ArrayQ<Tq> fnx = 0;
  fluxAdvectiveUpwind( x, time, qL, qR, nx, fnx );

  //Upwind temporal flux (i.e. conservative flux)
  ArrayQ<Tq> ft = 0;
  if (nt >= 0)
    masterState( x, time, qL, ft );
  else
    masterState( x, time, qR, ft );

  f += ft*nt + fnx;
}

// viscous flux
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDETwoPhase1D<PDETraitsSize, PDETraitsModel>::
fluxViscous( const Real& x, const Real& time,
             const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
             ArrayQ<Tf>& f ) const
{
  typedef typename promote_Surreal<Tq,Tg>::type T;

  Tq Sw = 0, Sn = 0, pw = 0, pn = 0;
  T pwx = 0, pnx = 0;
  qInterpreter_.eval( q, pw, pn, Sw, Sn );
  qInterpreter_.eval_pwGradient( q, qx, pwx );
  qInterpreter_.eval_pnGradient( q, qx, pnx );

  //Evaluate densities
  Tq rhow = rhow_.density(x, time, pw);
  Tq rhon = rhon_.density(x, time, pn);

  //Evaluate relative permeabilities
  Tq krw = krw_.relativeperm(Sw);
  Tq krn = krn_.relativeperm(Sn);

  //Evaluate phase viscosities
  Real muw = muw_.viscosity(x, time);
  Real mun = mun_.viscosity(x, time);

  //Evaluate phase mobilities
  Tq lambda_w = krw/muw;
  Tq lambda_n = krn/mun;

  f(0) -= rhow*Kxx_*lambda_w*pwx;
  f(1) -= rhon*Kxx_*lambda_n*pnx;

  //Additional viscosity term for saturation equation (used to stabilize single-phase cases)
  T Swx = 0, Snx = 0;
  qInterpreter_.eval_SwGradient( q, qx, Swx );
  qInterpreter_.eval_SnGradient( q, qx, Snx );

  f(0) -=  rhow*scalar_visc_*Swx;
  f(1) -= -rhon*scalar_visc_*Swx;
}

// space-time viscous flux
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDETwoPhase1D<PDETraitsSize, PDETraitsModel>::
fluxViscousSpaceTime( const Real& x, const Real& time,
                      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
                      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
{
  //Not adding temporal diffusion, so just forward the call to spatial viscous flux
  fluxViscous(x, time, q, qx, f);
}

// viscous flux: normal flux with left and right states
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDETwoPhase1D<PDETraitsSize, PDETraitsModel>::
fluxViscous(const Real& x, const Real& time,
            const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
            const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
            const Real& nx, ArrayQ<Tf>& fn ) const
{
  typedef typename promote_Surreal<Tq,Tg>::type T;

  Tq SwL = 0, SnL = 0, pwL = 0, pnL = 0;
  Tq SwR = 0, SnR = 0, pwR = 0, pnR = 0;
  T pwxL = 0, pnxL = 0;
  T pwxR = 0, pnxR = 0;

  qInterpreter_.eval( qL, pwL, pnL, SwL, SnL );
  qInterpreter_.eval( qR, pwR, pnR, SwR, SnR );
  qInterpreter_.eval_pwGradient( qL, qxL, pwxL );
  qInterpreter_.eval_pnGradient( qL, qxL, pnxL );
  qInterpreter_.eval_pwGradient( qR, qxR, pwxR );
  qInterpreter_.eval_pnGradient( qR, qxR, pnxR );

  //Evaluate densities
  Tq rhowL = rhow_.density(x, time, pwL);
  Tq rhonL = rhon_.density(x, time, pnL);
  Tq rhowR = rhow_.density(x, time, pwR);
  Tq rhonR = rhon_.density(x, time, pnR);

  //Evaluate relative permeabilities
  Tq krwL = krw_.relativeperm(SwL);
  Tq krnL = krn_.relativeperm(SnL);
  Tq krwR = krw_.relativeperm(SwR);
  Tq krnR = krn_.relativeperm(SnR);

  //Evaluate phase viscosities
  Real muw = muw_.viscosity(x, time);
  Real mun = mun_.viscosity(x, time);

  //Evaluate phase mobilities
  Tq lambda_wL = krwL/muw;
  Tq lambda_nL = krnL/mun;
  Tq lambda_wR = krwR/muw;
  Tq lambda_nR = krnR/mun;

  ArrayQ<Tf> fxL = 0, fxR = 0;

#if 1
  fxL(0) -= rhowL*Kxx_*lambda_wL*pwxL;
  fxL(1) -= rhonL*Kxx_*lambda_nL*pnxL;
  fxR(0) -= rhowR*Kxx_*lambda_wR*pwxR;
  fxR(1) -= rhonR*Kxx_*lambda_nR*pnxR;
#else
  //Mobility upwinding
  if (0.5*(pwxL + pwxR)*nx >= 0.0)
  {
    fxL(0) -= rhowL*Kxx_*lambda_wR*pwxL;
    fxR(0) -= rhowR*Kxx_*lambda_wR*pwxR;
  }
  else
  {
    fxL(0) -= rhowL*Kxx_*lambda_wL*pwxL;
    fxR(0) -= rhowR*Kxx_*lambda_wL*pwxR;
  }

  if (0.5*(pnxL + pnxR)*nx >= 0.0)
  {
    fxL(1) -= rhonL*Kxx_*lambda_nR*pnxL;
    fxR(1) -= rhonR*Kxx_*lambda_nR*pnxR;
  }
  else
  {
    fxL(1) -= rhonL*Kxx_*lambda_nL*pnxL;
    fxR(1) -= rhonR*Kxx_*lambda_nL*pnxR;
  }
#endif

  //Additional viscosity term for saturation equation (used to stabilize single-phase cases)
  T SwxL = 0, SwxR = 0;
  qInterpreter_.eval_SwGradient( qL, qxL, SwxL );
  qInterpreter_.eval_SwGradient( qR, qxR, SwxR );

  fxL(0) -=  rhowL*scalar_visc_*SwxL;
  fxL(1) -= -rhonL*scalar_visc_*SwxL;
  fxR(0) -=  rhowR*scalar_visc_*SwxR;
  fxR(1) -= -rhonR*scalar_visc_*SwxR;

  //Average normal flux
  fn += 0.5*(fxL + fxR)*nx;

#if 1 //Add upwinding

  //Evaluate relative permeability jacobians
  Tq krwL_SwL, krnL_SnL;
  Tq krwR_SwR, krnR_SnR;
  krw_.relativepermJacobian(SwL, krwL_SwL);
  krn_.relativepermJacobian(SnL, krnL_SnL);
  krw_.relativepermJacobian(SwR, krwR_SwR);
  krn_.relativepermJacobian(SnR, krnR_SnR);

  Real dSn_dSw = -1;
  Tq krnL_SwL = krnL_SnL * dSn_dSw;
  Tq krnR_SwR = krnR_SnR * dSn_dSw;

  //Evaluate phase mobility jacobians
  Tq lambda_wL_SwL = krwL_SwL/muw;
  Tq lambda_nL_SwL = krnL_SwL/mun;
  Tq lambda_wR_SwR = krwR_SwR/muw;
  Tq lambda_nR_SwR = krnR_SwR/mun;

  //Upwinding term
  T KgradpwL = (Kxx_*pwxL)*nx;
  T KgradpwR = (Kxx_*pwxR)*nx;

  T KgradpnL = (Kxx_*pnxL)*nx;
  T KgradpnR = (Kxx_*pnxR)*nx;

#if 1
  T cL = (lambda_wL_SwL*lambda_nL*KgradpwL - lambda_wL*lambda_nL_SwL*KgradpnL) / (lambda_wL + lambda_nL);
  T cR = (lambda_wR_SwR*lambda_nR*KgradpwR - lambda_wR*lambda_nR_SwR*KgradpnR) / (lambda_wR + lambda_nR);
#else //from linearized analysis which assumes gradSw = 0
  T cL = (lambda_wL_SwL*lambda_nL - lambda_wL*lambda_nL_SwL) / (lambda_wL + lambda_nL) * KgradpnL;
  T cR = (lambda_wR_SwR*lambda_nR - lambda_wR*lambda_nR_SwR) / (lambda_wR + lambda_nR) * KgradpnR;
#endif

  T rw_cL = rhowL * cL;
  T rw_cR = rhowR * cR;
  T rn_cL = rhonL * cL;
  T rn_cR = rhonR * cR;

  const Real eps = 1e-3;
  const Real alpha = 40;
  T cw_max = smoothmax( smoothabs0(rw_cL, eps), smoothabs0(rw_cR, eps), alpha );
  T cn_max = smoothmax( smoothabs0(rn_cL, eps), smoothabs0(rn_cR, eps), alpha );

  fn(0) += 0.5 * cw_max * (SwL - SwR);
  fn(1) -= 0.5 * cn_max * (SwL - SwR);
#endif
}

// space-time viscous flux: normal flux with left and right states
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDETwoPhase1D<PDETraitsSize, PDETraitsModel>::
fluxViscousSpaceTime( const Real& x, const Real& time,
                      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qtL,
                      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qtR,
                      const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const
{
  //Not adding temporal diffusion, so just forward the call to spatial viscous flux
  fluxViscous(x, time, qL, qxL, qR, qxR, nx, f);
}

// viscous diffusion matrix: d(Fv)/d(UX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tk>
inline void
PDETwoPhase1D<PDETraitsSize, PDETraitsModel>::
diffusionViscous( const Real& x, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
                  MatrixQ<Tk>& kxx ) const
{
  Tq Sw = 0, Sn = 0, pw = 0, pn = 0;
  qInterpreter_.eval( q, pw, pn, Sw, Sn );

  //Evaluate densities
  Tq rhow = rhow_.density(x, time, pw);
  Tq rhon = rhon_.density(x, time, pn);

  //Evaluate relative permeabilities
  Tq krw = krw_.relativeperm(Sw);
  Tq krn = krn_.relativeperm(Sn);

  //Evaluate phase viscosities
  Tq muw = muw_.viscosity(x, time);
  Tq mun = mun_.viscosity(x, time);

  //Evaluate phase mobilities
  Tq lambda_w = krw/muw;
  Tq lambda_n = krn/mun;

  //Evaluate pressure jacobians (wrt state vector)
  ArrayQ<Tq> pw_q, pn_q;
  qInterpreter_.jacobian_pw( q, pw_q );
  qInterpreter_.jacobian_pn( q, pn_q );

  // d(Fv)/d(grad q)
  MatrixQ<Tq> Fv_gradq;
  Fv_gradq(0,0) = rhow*Kxx_*lambda_w*pw_q(0);
  Fv_gradq(0,1) = rhow*Kxx_*lambda_w*pw_q(1);
  Fv_gradq(1,0) = rhon*Kxx_*lambda_n*pn_q(0);
  Fv_gradq(1,1) = rhon*Kxx_*lambda_n*pn_q(1);

#ifdef CONSERVATIVE_FLUX_HACK
  kxx = Fv_gradq;
#else
  MatrixQ<Tq> dudq;
  jacobianMasterState(x, time, q, dudq);

  Tq det = dudq(0,0)*dudq(1,1) - dudq(0,1)*dudq(1,0);

  if (fabs(det) < 1e-15)
    SANS_DEVELOPER_EXCEPTION("Conservative flux Jacobian is singular!"); // det = %e, q = [%e, %e]", det, q[0], q[1]);

  MatrixQ<Tq> inv_dudq;
  inv_dudq(0,0) =  dudq(1,1)/det;
  inv_dudq(0,1) = -dudq(0,1)/det;
  inv_dudq(1,0) = -dudq(1,0)/det;
  inv_dudq(1,1) =  dudq(0,0)/det;

  kxx += Fv_gradq*inv_dudq; //d(Fv)/d(grad u) = d(Fv)/d(grad q) * inverse(du/dq)
#endif
}

// viscous diffusion matrix: d(Fv)/d(UX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tk>
inline void
PDETwoPhase1D<PDETraitsSize, PDETraitsModel>::
diffusionViscousSpaceTime( const Real& x, const Real& time,
                           const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
                           MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxt,
                           MatrixQ<Tk>& ktx, MatrixQ<Tk>& ktt ) const
{
  //Not adding temporal diffusion, so just forward the call to spatial diffusion matrix
  diffusionViscous(x, time, q, qx, kxx);
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Ts>
inline void
PDETwoPhase1D<PDETraitsSize, PDETraitsModel>::
source(const Real& x, const Real& time, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
       ArrayQ<Ts>& source ) const
{
  //Loop over all fixed well pressure source terms
  for (int i = 0; i < (int) sources_FixedWellPressure_.size(); i++)
    sources_FixedWellPressure_[i].source(x, time, q, qx, source);
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Ts>
inline void
PDETwoPhase1D<PDETraitsSize, PDETraitsModel>::
jacobianSourceAbsoluteValue(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    MatrixQ<Ts>& dsdu ) const
{
  std::cout << "TWOPHASE JACOBIAN SOURCE ABSOLUTE VALUE NOT IMPLEMENTED, DO NOTHING FOR NOW\n";
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Ts>
inline void
PDETwoPhase1D<PDETraitsSize, PDETraitsModel>::
speedCharacteristic( const Real& x, const Real& time, const ArrayQ<T>& q, Ts& speed ) const
{
  SANS_DEVELOPER_EXCEPTION("PDETwoPhase1D::speedCharacteristic - Not implemented yet.");
}

// update fraction needed for physically valid state
template <template <class> class PDETraitsSize, class PDETraitsModel>
void
PDETwoPhase1D<PDETraitsSize, PDETraitsModel>::
updateFraction( const Real& x, const Real& time, const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
                const Real maxChangeFraction, Real& updateFraction ) const
{
  SANS_DEVELOPER_EXCEPTION("Not implemented");
}

// is state physically valid
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline bool
PDETwoPhase1D<PDETraitsSize, PDETraitsModel>::
isValidState( const ArrayQ<Real>& q ) const
{
  return (q(0) > 0); //qInterpret_.isValidState(q);
}

// set from primitive variable array
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDETwoPhase1D<PDETraitsSize, PDETraitsModel>::
setDOFFrom( ArrayQ<T>& q, const Real data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn >= N);
  qInterpreter_.setFromPrimitive( q, data, name, nn );
}

// set from variables
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Variables>
inline void
PDETwoPhase1D<PDETraitsSize, PDETraitsModel>::
setDOFFrom( ArrayQ<T>& q, const TwoPhaseVariableType<Variables>& data ) const
{
  qInterpreter_.setFromPrimitive( q, data.cast() );
}

// interpret residuals of the solution variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDETwoPhase1D<PDETraitsSize, PDETraitsModel>::
interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
{
  int nOut = rsdPDEOut.m();

  SANS_ASSERT(nOut == N);
  for (int i = 0; i < nOut; i++)
    rsdPDEOut[i] = rsdPDEIn[i];
}

// interpret residuals of the gradients in the solution variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
void
PDETwoPhase1D<PDETraitsSize, PDETraitsModel>::
interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const
{
  //TEMPORARY SOLUTION FOR AUX VARIABLE
  int nOut = rsdAuxOut.m();

  SANS_ASSERT(nOut == N);
  for (int i = 0; i < nOut; i++)
    rsdAuxOut[i] = rsdAuxIn[i];
}

// interpret residuals at the boundary (should forward to BCs)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDETwoPhase1D<PDETraitsSize, PDETraitsModel>::
interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const
{
  int nOut = rsdBCOut.m();

  SANS_ASSERT(nOut == N);
  for (int i = 0; i < nOut; i++)
    rsdBCOut[i] = rsdBCIn[i];
}

// how many residual equations are we dealing with
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline int
PDETwoPhase1D<PDETraitsSize, PDETraitsModel>::
nMonitor() const
{
  return N;
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
inline std::vector<std::string>
PDETwoPhase1D<PDETraitsSize, PDETraitsModel>::
derivedQuantityNames() const
{
  return {"pc"
         };
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template<class Tq, class Tg, class Tf>
inline void
PDETwoPhase1D<PDETraitsSize, PDETraitsModel>::
derivedQuantity(const int& index, const Real& x, const Real& time,
                const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, Tf& J ) const
{
  switch (index)
  {
  case 0:
  {
    Tq Sw = 0, Sn = 0, pw = 0, pn = 0;
    qInterpreter_.eval( q, pw, pn, Sw, Sn );
    J = pn - pw;
    break;
  }

  default:
    SANS_DEVELOPER_EXCEPTION("PDETwoPhase1D::derivedQuantity - Invalid index!");
  }
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
void
PDETwoPhase1D<PDETraitsSize, PDETraitsModel>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  std::string indent2(indentSize+2, ' ');

  out << indent << "PDETwoPhase1D<QType, DensityModelw, DensityModeln, PorosityModel, RelPermModelw, RelPermModeln, "
                   "ViscModelw, ViscModeln, CapillaryModel>:" << std::endl;
  out << indent2 << "qInterpret_ = " << std::endl;
  qInterpreter_.dump(indentSize+4, out);
  out << indent2 << "rhow_ = " << std::endl;
  rhow_.dump(indentSize+4, out);
  out << indent2 << "rhon_ = " << std::endl;
  rhon_.dump(indentSize+4, out);
  out << indent2 << "phi_ = " << std::endl;
  phi_.dump(indentSize+4, out);
  out << indent2 << "krw_ = " << std::endl;
  krw_.dump(indentSize+4, out);
  out << indent2 << "krn_ = " << std::endl;
  krn_.dump(indentSize+4, out);
  out << indent2 << "muw_ = " << std::endl;
  muw_.dump(indentSize+4, out);
  out << indent2 << "mun_ = " << std::endl;
  mun_.dump(indentSize+4, out);
  out << indent2 << "K_ = " << Kxx_ << std::endl;
  out << indent2 << "pc_ = " << std::endl;
  pc_.dump(indentSize+4, out);
}


} //namespace SANS

#endif  // PDETWOPHASE1D_H
