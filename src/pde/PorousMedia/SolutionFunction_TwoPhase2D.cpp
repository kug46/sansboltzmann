// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "SolutionFunction_TwoPhase2D.h"

namespace SANS
{

// cppcheck-suppress passedByValue
void SolutionFunction_TwoPhase2D_SingleWell_Peaceman_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.pB));
  allParams.push_back(d.checkInputs(params.Rwellbore));
  allParams.push_back(d.checkInputs(params.Sw));
  allParams.push_back(d.checkInputs(params.mu));
  allParams.push_back(d.checkInputs(params.Q));
  allParams.push_back(d.checkInputs(params.xcentroid));
  allParams.push_back(d.checkInputs(params.ycentroid));
  allParams.push_back(d.checkInputs(params.Kxx));
  allParams.push_back(d.checkInputs(params.Kyy));
  d.checkUnknownInputs(allParams);
}
SolutionFunction_TwoPhase2D_SingleWell_Peaceman_Params SolutionFunction_TwoPhase2D_SingleWell_Peaceman_Params::params;

// cppcheck-suppress passedByValue
void SolutionFunction_TwoPhase2D_Reservoir_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Ly));
  allParams.push_back(d.checkInputs(params.y_owc));
  allParams.push_back(d.checkInputs(params.rho_o));
  allParams.push_back(d.checkInputs(params.rho_w));
  d.checkUnknownInputs(allParams);
}
SolutionFunction_TwoPhase2D_Reservoir_Params SolutionFunction_TwoPhase2D_Reservoir_Params::params;

}
