// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef CARTTABLE_H
#define CARTTABLE_H

#include <string>
#include <vector>

#include "tools/SANSnumerics.h"

namespace SANS
{

template<int D>
class CartTable;

template<>
class CartTable<2>
{
public:
  explicit CartTable( const std::string& filename );

  ~CartTable() = default;

  Real operator()(const Real x, const Real y) const;

  inline void scaleValues(const Real factor)
  {
    for (auto& val : table_)
      val *= factor;
  }

protected:
  int imax_, jmax_;
  Real dx_, dy_;
  std::vector<Real> table_;
};

} // namespace SANS

#endif // CARTTABLE_H
