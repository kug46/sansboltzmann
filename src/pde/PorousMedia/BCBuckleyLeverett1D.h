// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCBUCKLEYLEVERETT1D_H
#define BCBUCKLEYLEVERETT1D_H

// 1-D Buckley-Leverett BC class

//PyDict must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "tools/SANSnumerics.h"     // Real
#include "Topology/Dimension.h"

#include "pde/BCCategory.h"
#include "pde/BCNone.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"

#include <iostream>

#include <boost/mpl/vector.hpp>

#include "PDEBuckleyLeverett1D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// BC class: 1-D BuckleyLeverett
//
// template parameters:
//   PhysDim              Physical dimensions
//   BCType               BC type (e.g. BCTypeLinearRobin)
//----------------------------------------------------------------------------//

template <class BCType, class PDE>
class BCBuckleyLeverett1D;

template <class BCType>
struct BCBuckleyLeverett1DParams;

//----------------------------------------------------------------------------//
// BC types
//----------------------------------------------------------------------------//

class BCTypeTimeIC;
class BCTypeLinearRobin;
class BCTypeDirichlet_mitState;
class BCTypeDirichlet_mitStateSpaceTime;
class BCTypeFunction_mitState;
class BCTypeTimeIC_Function;

// Define the vector of boundary conditions
template <class PDE>
using BCBuckleyLeverett1DVector = boost::mpl::vector7< BCNone<PhysD1,PDE::N>,
                                                       SpaceTimeBC< BCBuckleyLeverett1D<BCTypeTimeIC, PDE> >,
                                                       BCBuckleyLeverett1D<BCTypeLinearRobin, PDE>,
                                                       BCBuckleyLeverett1D<BCTypeDirichlet_mitState, PDE>,
                                                       SpaceTimeBC< BCBuckleyLeverett1D<BCTypeDirichlet_mitStateSpaceTime, PDE> >,
                                                       BCBuckleyLeverett1D<BCTypeFunction_mitState, PDE>,
                                                       SpaceTimeBC< BCBuckleyLeverett1D<BCTypeTimeIC_Function, PDE> >
                                                     >;

//----------------------------------------------------------------------------//
// Time IC BC:
//
// A class for representing Initial conditions

template<>
struct BCBuckleyLeverett1DParams<BCTypeTimeIC> : noncopyable
{
  const ParameterNumeric<Real> qB{"qB", NO_DEFAULT, NO_RANGE, "BC state"};

  static void checkInputs(PyDict d);

  static constexpr const char* BCName{"TimeIC"};
  struct Option
  {
    const DictOption TimeIC{BCBuckleyLeverett1DParams::BCName, BCBuckleyLeverett1DParams::checkInputs};
  };

  static BCBuckleyLeverett1DParams params;
};

template <class PDE_>
class BCBuckleyLeverett1D<BCTypeTimeIC, PDE_>
  : public BCType< BCBuckleyLeverett1D<BCTypeTimeIC, PDE_> >
{
public:
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCBuckleyLeverett1DParams<BCTypeTimeIC> ParamsType;
  typedef PDE_ PDE;

  typedef PhysD1 PhysDim;
  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = N;      // total BCs

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;  // matrices

  BCBuckleyLeverett1D(const PDE& pde, const PyDict& d ) :
    qB_(d.get(ParamsType::params.qB)), pde_(pde) {}

  BCBuckleyLeverett1D(const PDE& pde, const ArrayQ<Real>& qB ) :
    qB_(qB), pde_(pde) {}

  virtual ~BCBuckleyLeverett1D() {}

  BCBuckleyLeverett1D& operator=( const BCBuckleyLeverett1D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // BC state vector
  template <class T>
  void state( const Real& x, const Real& time,
              const Real& nx, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    qB = qB_;
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormalSpaceTime( const Real& x, const Real& time,
                            const Real& nx, const Real& nt,
                            const ArrayQ<T>& qI,
                            const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
                            const ArrayQ<T>& qB,
                            ArrayQ<Tf>& Fn) const
  {
    ArrayQ<T> uB = 0;
    pde_.fluxAdvectiveTime(x, time, qB, uB);
    Fn += nt*uB;
  }

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormalSpaceTime( const Tp& param,
                            const Real& x, const Real& time,
                            const Real& nx, const Real& nt,
                            const ArrayQ<T>& qI,
                            const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
                            const ArrayQ<T>& qB,
                            ArrayQ<Tf>& Fn) const
  {
    ArrayQ<T> uB = 0;
    pde_.fluxAdvectiveTime(param, x, time, qB, uB);
    Fn += nt*uB;
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& nt, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  ArrayQ<Real> qB_;
  const PDE& pde_;
};

//----------------------------------------------------------------------------//
// Robin BC:  A u + B (kn un + ks us) = bcdata
//
// coefficients A and B are arbitrary with the exception that they cannot
// simultaneously vanish
template<>
struct BCBuckleyLeverett1DParams<BCTypeLinearRobin> : noncopyable
{
  const ParameterNumeric<Real> A{"A", NO_DEFAULT, NO_RANGE, "Value coefficient"};
  const ParameterNumeric<Real> B{"B", NO_DEFAULT, NO_RANGE, "Viscous coefficient"};
  const ParameterNumeric<Real> bcdata{"bcdata", NO_DEFAULT, NO_RANGE, "BC data"};

  static constexpr const char* BCName{"LinearRobin"};
  struct Option
  {
    const DictOption LinearRobin{BCBuckleyLeverett1DParams::BCName, BCBuckleyLeverett1DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCBuckleyLeverett1DParams params;
};

template <class PDE_>
class BCBuckleyLeverett1D<BCTypeLinearRobin, PDE_>
  : public BCType< BCBuckleyLeverett1D<BCTypeLinearRobin, PDE_> >
{

public:
  typedef BCCategory::LinearScalar_sansLG Category;
  typedef BCBuckleyLeverett1DParams<BCTypeLinearRobin> ParamsType;
  typedef PDE_ PDE;

  typedef PhysD1 PhysDim;
  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = N;      // total BCs

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;  // matrices

  BCBuckleyLeverett1D(const PDE& pde, const PyDict& d ) :
    A_(d.get(ParamsType::params.A)),
    B_(d.get(ParamsType::params.B)),
    bcdata_(d.get(ParamsType::params.bcdata)) {}

  BCBuckleyLeverett1D( const Real& A, const Real& B, const Real& bcdata ) :
    A_(A), B_(B), bcdata_(bcdata) {}

  virtual ~BCBuckleyLeverett1D() {}

  BCBuckleyLeverett1D& operator=( const BCBuckleyLeverett1D& );

  // BC coefficients:  A u + B (kn un + ks us)
  template <class T>
  void coefficients( const Real& x, const Real& time, const Real& nx,
                     MatrixQ<T>& A, MatrixQ<T>& B ) const
  {
    A = A_;
    B = B_;
  }

  // BC data
  template <class T>
  void data( const Real& x, const Real& time, const Real& nx, ArrayQ<T>& bcdata ) const
  {
    bcdata = bcdata_;
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  MatrixQ<Real> A_;
  MatrixQ<Real> B_;
  ArrayQ<Real> bcdata_;
};


//----------------------------------------------------------------------------//
// Dirichlet BC based on state vector
//
// coefficients A and B are arbitrary with the exception that they cannot
// simultaneously vanish
template <>
struct BCBuckleyLeverett1DParams<BCTypeDirichlet_mitState> : noncopyable
{
  const ParameterNumeric<Real> qB{"qB", NO_DEFAULT, NO_RANGE, "BC state"};

  static constexpr const char* BCName{"Dirichlet_mitState"};
  struct Option
  {
    const DictOption Dirichlet_mitState{BCBuckleyLeverett1DParams::BCName, BCBuckleyLeverett1DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCBuckleyLeverett1DParams params;
};

template <class PDE_>
class BCBuckleyLeverett1D<BCTypeDirichlet_mitState, PDE_>
  : public BCType< BCBuckleyLeverett1D<BCTypeDirichlet_mitState, PDE_> >
{

public:
  typedef BCCategory::Flux_mitState Category;
  typedef BCBuckleyLeverett1DParams<BCTypeDirichlet_mitState> ParamsType;
  typedef PDE_ PDE;

  typedef PhysD1 PhysDim;
  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = N;      // total BCs

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;  // matrices

  BCBuckleyLeverett1D(const PDE& pde, const PyDict& d ) :
    qB_(d.get(ParamsType::params.qB)), pde_(pde) {}

  BCBuckleyLeverett1D(const PDE& pde, const ArrayQ<Real>& qB ) :
    qB_(qB), pde_(pde) {}

  virtual ~BCBuckleyLeverett1D() {}

  BCBuckleyLeverett1D& operator=( const BCBuckleyLeverett1D& );

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return pde_.hasFluxViscous(); }

  // BC state vector
  template <class T>
  void state( const Real& x, const Real& time,
              const Real& nx,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    qB = qB_;
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& time,
                   const Real& nx,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    // Add upwinded advective flux
    pde_.fluxAdvectiveUpwind(x, time, qI, qB, nx, Fn);

    ArrayQ<Tf> fv = 0;
    pde_.fluxViscous(x, time, qB, qIx, fv);

    // Add viscous flux
    Fn += fv*nx;
  }

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormal( const Tp& param,
                   const Real& x, const Real& time,
                   const Real& nx,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    // Add upwinded advective flux
    pde_.fluxAdvectiveUpwind(param, x, time, qI, qB, nx, Fn);

    ArrayQ<Tf> fv = 0;
    pde_.fluxViscous(param, x, time, qB, qIx, fv);

    // Add viscous flux
    Fn += fv*nx;
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  ArrayQ<Real> qB_;
  const PDE& pde_;
};

//----------------------------------------------------------------------------//
// Dirichlet BC based on state vector for space-time problems
//
// coefficients A and B are arbitrary with the exception that they cannot
// simultaneously vanish
template <>
struct BCBuckleyLeverett1DParams<BCTypeDirichlet_mitStateSpaceTime> : noncopyable
{
  const ParameterNumeric<Real> qB{"qB", NO_DEFAULT, NO_RANGE, "BC state"};
  const ParameterBool isOutflow{"isOutflow", false, "Is this an outflow boundary?"};

  static constexpr const char* BCName{"Dirichlet_mitStateSpaceTime"};
  struct Option
  {
    const DictOption Dirichlet_mitStateSpaceTime{BCBuckleyLeverett1DParams::BCName, BCBuckleyLeverett1DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCBuckleyLeverett1DParams params;
};

template <class PDE_>
class BCBuckleyLeverett1D<BCTypeDirichlet_mitStateSpaceTime, PDE_>
  : public BCType< BCBuckleyLeverett1D<BCTypeDirichlet_mitStateSpaceTime, PDE_> >
{

public:
  typedef BCCategory::Flux_mitState Category;
  typedef BCBuckleyLeverett1DParams<BCTypeDirichlet_mitStateSpaceTime> ParamsType;
  typedef PDE_ PDE;

  typedef PhysD1 PhysDim;
  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = N;      // total BCs

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;  // matrices

  BCBuckleyLeverett1D(const PDE& pde, const PyDict& d ) :
    pde_(pde), qB_(d.get(ParamsType::params.qB)), isOutflow_(d.get(ParamsType::params.isOutflow))  {}

  BCBuckleyLeverett1D(const PDE& pde, const ArrayQ<Real>& qB, const bool isOutflow = false ) :
    pde_(pde), qB_(qB), isOutflow_(isOutflow) {}

  virtual ~BCBuckleyLeverett1D() {}

  BCBuckleyLeverett1D& operator=( const BCBuckleyLeverett1D& );

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return pde_.hasFluxViscous(); }

  // BC state vector
  template <class T>
  void state( const Real& x, const Real& time,
              const Real& nx, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    if (isOutflow_)
      qB = qI;
    else
      qB = qB_;
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormalSpaceTime( const Real& x, const Real& time,
                            const Real& nx, const Real& nt,
                            const ArrayQ<T>& qI,
                            const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
                            const ArrayQ<T>& qB,
                            ArrayQ<Tf>& Fn) const
  {
    // Add upwinded advective flux
    pde_.fluxAdvectiveUpwindSpaceTime(x, time, qI, qB, nx, nt, Fn);

    // Add viscous flux
    ArrayQ<Tf> fx = 0, ft = 0;
    pde_.fluxViscousSpaceTime(x, time, qB, qIx, qIt, fx, ft);
    Fn += fx*nx + ft*nt;
  }

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormalSpaceTime( const Tp& param,
                            const Real& x, const Real& time,
                            const Real& nx, const Real& nt,
                            const ArrayQ<T>& qI,
                            const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
                            const ArrayQ<T>& qB,
                            ArrayQ<Tf>& Fn) const
  {
    // Add upwinded advective flux
    pde_.fluxAdvectiveUpwindSpaceTime(param, x, time, qI, qB, nx, nt, Fn);

    // Add viscous flux
    ArrayQ<Tf> fx = 0, ft = 0;
    pde_.fluxViscousSpaceTime(param, x, time, qB, qIx, qIt, fx, ft);
    Fn += fx*nx + ft*nt;
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& nt, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const PDE& pde_;
  ArrayQ<Real> qB_;
  const bool isOutflow_;
};

//----------------------------------------------------------------------------//
// Solution BC:  u = uexact(x,time)
//
template<>
struct BCBuckleyLeverett1DParams<BCTypeFunction_mitState> : ScalarFunction1DParams
{
  static constexpr const char* BCName{"Function_mitState"};
  struct Option
  {
    const DictOption Function_mitState{BCBuckleyLeverett1DParams::BCName, BCBuckleyLeverett1DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCBuckleyLeverett1DParams params;
};

template <class PDE_>
class BCBuckleyLeverett1D<BCTypeFunction_mitState, PDE_ > :
  public BCType< BCBuckleyLeverett1D< BCTypeFunction_mitState, PDE_ > >
{
public:
  typedef BCCategory::Flux_mitState Category;
  typedef BCBuckleyLeverett1DParams< BCTypeFunction_mitState > ParamsType;
  typedef PDE_ PDE;

  typedef PhysD1 PhysDim;
  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = N;      // total BCs

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;  // matrices

  typedef std::shared_ptr<Function1DBase<Real>> Function_ptr;

  BCBuckleyLeverett1D(const PDE& pde, const PyDict& d ) :
    uexact_( ParamsType::getFunction(d) ), pde_(pde) {}

  BCBuckleyLeverett1D(const PDE& pde, const Function_ptr& uexact ) :
    uexact_(uexact), pde_(pde) {}

  virtual ~BCBuckleyLeverett1D() {}

  BCBuckleyLeverett1D& operator=( const BCBuckleyLeverett1D& );

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return pde_.hasFluxViscous(); }

  // BC state vector
  template <class T>
  void state( const Real& x, const Real& time,
              const Real& nx,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    qB = (*uexact_)(x,time);
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& time,
                   const Real& nx,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    // Add upwinded advective flux
    pde_.fluxAdvectiveUpwind(x, time, qI, qB, nx, Fn);

    ArrayQ<Tf> fv = 0;
    pde_.fluxViscous(x, time, qB, qIx, fv);

    // Add viscous flux
    Fn += fv*nx;
  }

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormal( const Tp& param,
                   const Real& x, const Real& time,
                   const Real& nx,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    // Add upwinded advective flux
    pde_.fluxAdvectiveUpwind(param, x, time, qI, qB, nx, Fn);

    ArrayQ<Tf> fv = 0;
    pde_.fluxViscous(param, x, time, qB, qIx, fv);

    // Add viscous flux
    Fn += fv*nx;
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const Function_ptr uexact_;
  const PDE& pde_;
};


//----------------------------------------------------------------------------//
// Solution BC:  u = uexact(x,time)
//
template<>
struct BCBuckleyLeverett1DParams<BCTypeTimeIC_Function> : ScalarFunction1DParams
{
  static constexpr const char* BCName{"TimeIC_Function"};
  struct Option
  {
    const DictOption TimeIC_Function{BCBuckleyLeverett1DParams::BCName, BCBuckleyLeverett1DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCBuckleyLeverett1DParams params;
};

template <class PDE_>
class BCBuckleyLeverett1D<BCTypeTimeIC_Function, PDE_ > :
  public BCType< BCBuckleyLeverett1D< BCTypeTimeIC_Function, PDE_ > >
{
public:
  typedef BCCategory::Flux_mitState Category;
  typedef BCBuckleyLeverett1DParams< BCTypeTimeIC_Function > ParamsType;
  typedef PDE_ PDE;

  typedef PhysD1 PhysDim;
  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = N;      // total BCs

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;  // matrices

  typedef std::shared_ptr<Function1DBase<Real>> Function_ptr;

  BCBuckleyLeverett1D(const PDE& pde, const PyDict& d ) :
    uexact_( ParamsType::getFunction(d) ), pde_(pde) {}

  BCBuckleyLeverett1D(const PDE& pde, const Function_ptr& uexact ) :
    uexact_(uexact), pde_(pde) {}

  virtual ~BCBuckleyLeverett1D() {}

  BCBuckleyLeverett1D& operator=( const BCBuckleyLeverett1D& );

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // BC state vector
  template <class T>
  void state( const Real& x, const Real& time,
              const Real& nx, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    qB = (*uexact_)(x,time);
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormalSpaceTime( const Real& x, const Real& time,
                            const Real& nx, const Real& nt,
                            const ArrayQ<T>& qI,
                            const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
                            const ArrayQ<T>& qB,
                            ArrayQ<Tf>& Fn) const
  {
    ArrayQ<T> uB = 0;
    pde_.fluxAdvectiveTime(x, time, qB, uB);
    Fn += nt*uB;
  }

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormalSpaceTime( const Tp& param,
                            const Real& x, const Real& time,
                            const Real& nx, const Real& nt,
                            const ArrayQ<T>& qI,
                            const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
                            const ArrayQ<T>& qB,
                            ArrayQ<Tf>& Fn) const
  {
    ArrayQ<T> uB = 0;
    pde_.fluxAdvectiveTime(x, time, qB, uB);
    Fn += nt*uB;
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& nt, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const Function_ptr uexact_;
  const PDE& pde_;
};

} //namespace SANS

#endif  // BCBUCKLEYLEVERETT1D_H
