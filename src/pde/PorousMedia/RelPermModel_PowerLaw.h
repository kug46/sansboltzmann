// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RELPERMMODEL_POWERLAW_H
#define RELPERMMODEL_POWERLAW_H

// Relative permeability model - power law

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <cmath> // pow
#include <iostream>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// A class for representing input parameters for the RelativePermeabilityModel
struct RelPermModel_PowerLaw_Params : noncopyable
{
  const ParameterNumeric<Real> power{"power", 2, 0, NO_LIMIT, "Exponent of power law model"};

  static void checkInputs(PyDict d);

  static RelPermModel_PowerLaw_Params params;
};

//----------------------------------------------------------------------------//
// RelPermModel_PowerLaw: kr(S) = S^(power)
//
// template parameters:
//   T                        solution DOF data type (e.g. double)
//
// member functions:
//   .power                   get the exponent of the power law model
//
//   .relativeperm            relative permeability kr(S)
//   .relativepermJacobian    jacobian: d(kr)/dS
//   .relativepermGradient    gradient: d(kr)/dx and d(kr)/dy
//
//   .dump                    debug dump of private data
//----------------------------------------------------------------------------//



class RelPermModel_PowerLaw
{
public:
  RelPermModel_PowerLaw_Params& params;

  explicit RelPermModel_PowerLaw( const PyDict& d ) :
      params(RelPermModel_PowerLaw_Params::params),
      power_(d.get(params.power)) {}

  explicit RelPermModel_PowerLaw( Real power ) :
      params(RelPermModel_PowerLaw_Params::params),
      power_(power)
  {
    SANS_ASSERT( power >= 0 );
  }

  RelPermModel_PowerLaw( const RelPermModel_PowerLaw& model ) :
      params( RelPermModel_PowerLaw_Params::params ),
      power_(model.power_){}

  RelPermModel_PowerLaw& operator=( const RelPermModel_PowerLaw& model )
  {
    power_ = model.power_;
    return *this;
  }

  ~RelPermModel_PowerLaw() {}

  int power() const { return power_; }

  // Relative permeability
  template<class T>
  T relativeperm( const T& S ) const;

  template<class T>
  void relativepermJacobian( const T& S, T& kr_S ) const;

  template<class T>
  void relativepermGradient( const T& S, const T& Sx, T& krx ) const;
  template<class T>
  void relativepermGradient( const T& S, const T& Sx, const T& Sy, T& krx, T& kry ) const;

  void dump( std::string indent, std::ostream& out = std::cout ) const;
  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  Real power_; // Exponent in power law
};


template <class T>
inline T
RelPermModel_PowerLaw::relativeperm( const T& S ) const
{
  return pow(S, power_);
}

template <class T>
inline void
RelPermModel_PowerLaw::relativepermJacobian( const T& S, T& kr_S ) const
{
  kr_S = power_*pow(S, power_ - 1);
}

template <class T>
inline void
RelPermModel_PowerLaw::relativepermGradient( const T& S, const T& Sx, T& krx ) const
{
  //Compute Jacobian
  T kr_S;
  relativepermJacobian( S, kr_S );

  //Apply chain rule for gradient: d(kr)/dx = d(kr)/dS * dS/dx
  krx = kr_S*Sx;
}

template <class T>
inline void
RelPermModel_PowerLaw::relativepermGradient( const T& S, const T& Sx, const T& Sy, T& krx, T& kry) const
{
  //Compute Jacobian
  T kr_S;
  relativepermJacobian( S, kr_S );

  //Apply chain rule for gradient: d(kr)/dx = d(kr)/dS * dS/dx
  krx = kr_S*Sx;
  kry = kr_S*Sy;
}


} //namespace SANS

#endif  // RELPERMMODEL_POWERLAW_H
