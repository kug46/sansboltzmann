// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCTWOPHASE1D_H
#define BCTWOPHASE1D_H

// 1-D Two-phase BC class

//PyDict must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/BCCategory.h"
#include "pde/BCNone.h"
#include "Topology/Dimension.h"

#include <iostream>
#include <memory> // shared_ptr

#include <boost/mpl/vector.hpp>

#include "PDETwoPhase1D.h"
#include "SolutionFunction_TwoPhase1D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// BC types
//----------------------------------------------------------------------------//

class BCTypeTimeIC;
class BCTypeTimeIC_Function;
class BCTypeFullState;
class BCTypeFullStateSpaceTime;
class BCTypeFunction_mitState;

//----------------------------------------------------------------------------//
// BC class: 1-D TwoPhase
//
// template parameters:
//   PhysDim              Physical dimensions
//   BCType               BC type
//----------------------------------------------------------------------------//

template <class BCType, class PDE>
class BCTwoPhase1D;

template <class BCType>
struct BCTwoPhase1DParams;


// Define the vector of boundary conditions
template <class PDE>
using BCTwoPhase1DVector =  boost::mpl::vector6< BCNone<PhysD1,PDE::N>,
                                                 SpaceTimeBC< BCTwoPhase1D<BCTypeTimeIC, PDE> >,
                                                 SpaceTimeBC< BCTwoPhase1D<BCTypeTimeIC_Function, PDE> >,
                                                 BCTwoPhase1D<BCTypeFullState, PDE>,
                                                 SpaceTimeBC< BCTwoPhase1D<BCTypeFullStateSpaceTime, PDE> >,
                                                 BCTwoPhase1D<BCTypeFunction_mitState, PDE>
                                               >;

//----------------------------------------------------------------------------//
// Space-time initial condition
//----------------------------------------------------------------------------//

template<>
struct BCTwoPhase1DParams<BCTypeTimeIC> : noncopyable
{
  BCTwoPhase1DParams();

  const ParameterOption<TwoPhaseVariableTypeParams::TwoPhaseVariableOptions>& StateVector;

  static constexpr const char* BCName{"TimeIC"};
  struct Option
  {
    const DictOption TimeIC{BCTwoPhase1DParams::BCName, BCTwoPhase1DParams::checkInputs};
  };

  static void checkInputs(PyDict d);
  static BCTwoPhase1DParams params;
};


template <class PDE_>
class BCTwoPhase1D<BCTypeTimeIC, PDE_> :
    public BCType< BCTwoPhase1D<BCTypeTimeIC, PDE_> >
{
public:
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCTwoPhase1DParams<BCTypeTimeIC> ParamsType;
  typedef PDE_ PDE;

  typedef PhysD1 PhysDim;
  static const int D = PDE::D; // physical dimensions
  static const int N = PDE::N; // total solution variables

  static const int NBC = N;         // total BCs

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;   // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>; // matrices

  // cppcheck-suppress noExplicitConstructor
  template <class Variables>
  BCTwoPhase1D( const PDE& pde, const TwoPhaseVariableType<Variables>& data ) :
      pde_(pde),
      qB_(pde.setDOFFrom(data)){}

  BCTwoPhase1D(const PDE& pde, const PyDict& d ) :
    pde_(pde),
    qB_(pde.setDOFFrom(d)){}

  virtual ~BCTwoPhase1D() {}

  BCTwoPhase1D( const BCTwoPhase1D& ) = delete;
  BCTwoPhase1D& operator=( const BCTwoPhase1D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // BC data
  template <class T>
  void state( const Real& x, const Real& time,
              const Real& nx, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    qB = qB_;
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormalSpaceTime( const Real& x, const Real& time,
                            const Real& nx, const Real& nt,
                            const ArrayQ<T>& qI,
                            const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
                            const ArrayQ<T>& qB,
                            ArrayQ<Tf>& Fn) const
  {
    ArrayQ<T> uB = 0;
    pde_.fluxAdvectiveTime(x, time, qB, uB);
    Fn += nt*uB;

    //Add any contributions from space-time diffusive fluxes
    ArrayQ<Tf> fvx = 0, fvt = 0;
    pde_.fluxViscousSpaceTime(x, time, qB, qIx, qIt, fvx, fvt);
    Fn += fvx*nx + fvt*nt;
  }

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormalSpaceTime( const Tp& param,
                            const Real& x, const Real& time,
                            const Real& nx, const Real& nt,
                            const ArrayQ<T>& qI,
                            const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
                            const ArrayQ<T>& qB,
                            ArrayQ<Tf>& Fn) const
  {
    ArrayQ<T> uB = 0;
    pde_.fluxAdvectiveTime(param, x, time, qB, uB);
    Fn += nt*uB;

    //Add any contributions from space-time diffusive fluxes
    ArrayQ<Tf> fvx = 0, fvt = 0;
    pde_.fluxViscousSpaceTime(param, x, time, qB, qIx, qIt, fvx, fvt);
    Fn += fvx*nx + fvt*nt;
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& nt, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const ArrayQ<Real> qB_;
};

//----------------------------------------------------------------------------//
// Full state
//----------------------------------------------------------------------------//

template<>
struct BCTwoPhase1DParams<BCTypeFullState> : noncopyable
{
  BCTwoPhase1DParams();

  const ParameterOption<TwoPhaseVariableTypeParams::TwoPhaseVariableOptions>& StateVector;
  const ParameterBool isOutflow{"isOutflow", false, "Is this an outflow boundary?"};

  static constexpr const char* BCName{"FullState"};
  struct Option
  {
    const DictOption FullState{BCTwoPhase1DParams::BCName, BCTwoPhase1DParams::checkInputs};
  };

  static void checkInputs(PyDict d);
  static BCTwoPhase1DParams params;
};


template <class PDE_>
class BCTwoPhase1D<BCTypeFullState, PDE_> :
    public BCType< BCTwoPhase1D<BCTypeFullState, PDE_> >
{
public:
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCTwoPhase1DParams<BCTypeFullState> ParamsType;
  typedef PDE_ PDE;

  typedef PhysD1 PhysDim;
  static const int D = PDE::D; // physical dimensions
  static const int N = PDE::N; // total solution variables

  static const int NBC = N;         // total BCs

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;   // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>; // matrices

  typedef typename PDE::QInterpreter QInterpreter; // solution variable interpreter

  // cppcheck-suppress noExplicitConstructor
  template <class Variables>
  BCTwoPhase1D( const PDE& pde, const TwoPhaseVariableType<Variables>& data, const bool isOutflow = false ) :
      pde_(pde), qInterpreter_(pde_.variableInterpreter()),
      qB_(pde.setDOFFrom(data)), isOutflow_(isOutflow) {}

  BCTwoPhase1D(const PDE& pde, const PyDict& d ) :
    pde_(pde), qInterpreter_(pde_.variableInterpreter()),
    qB_(pde.setDOFFrom(d)), isOutflow_(d.get(ParamsType::params.isOutflow)) {}

  virtual ~BCTwoPhase1D() {}

  BCTwoPhase1D( const BCTwoPhase1D& ) = delete;
  BCTwoPhase1D& operator=( const BCTwoPhase1D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return pde_.hasFluxViscous(); }

  // BC data
  template <class T>
  void state( const Real& x, const Real& time, const Real& nx,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    qB = qB_;
    if (isOutflow_)
    {
      //Set pressure from BC data and saturation from interior
      Real pnB = 0;
      T SwI = 0;
      qInterpreter_.eval_pn(qB_, pnB );
      qInterpreter_.eval_Sw(qI, SwI );
      qInterpreter_.setFromPrimitive( qB, PressureNonWet_SaturationWet<T>(pnB, SwI) );
    }
    else
      qB = qB_;
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& time,
                   const Real& nx,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    // Add upwinded advective flux
    pde_.fluxAdvectiveUpwind(x, time, qI, qB, nx, Fn);

    // Add viscous flux
    ArrayQ<Tf> fx = 0;
    pde_.fluxViscous(x, time, qB, qIx, fx);
    Fn += fx*nx;
  }

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormal( const Tp& param,
                   const Real& x, const Real& time,
                   const Real& nx,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    // Add upwinded advective flux
    pde_.fluxAdvectiveUpwind(param, x, time, qI, qB, nx, Fn);

    // Add viscous flux
    ArrayQ<Tf> fx = 0;
    pde_.fluxViscous(param, x, time, qB, qIx, fx);
    Fn += fx*nx;
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const QInterpreter& qInterpreter_;
  const ArrayQ<Real> qB_;
  const bool isOutflow_;
};

//----------------------------------------------------------------------------//
// Full state for space-time
//----------------------------------------------------------------------------//

template<>
struct BCTwoPhase1DParams<BCTypeFullStateSpaceTime> : noncopyable
{
  BCTwoPhase1DParams();

  const ParameterOption<TwoPhaseVariableTypeParams::TwoPhaseVariableOptions>& StateVector;
  const ParameterBool isOutflow{"isOutflow", false, "Is this an outflow boundary?"};

  static constexpr const char* BCName{"FullStateSpaceTime"};
  struct Option
  {
    const DictOption FullStateSpaceTime{BCTwoPhase1DParams::BCName, BCTwoPhase1DParams::checkInputs};
  };

  static void checkInputs(PyDict d);
  static BCTwoPhase1DParams params;
};


template <class PDE_>
class BCTwoPhase1D<BCTypeFullStateSpaceTime, PDE_> :
    public BCType< BCTwoPhase1D<BCTypeFullStateSpaceTime, PDE_> >
{
public:
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCTwoPhase1DParams<BCTypeFullStateSpaceTime> ParamsType;
  typedef PDE_ PDE;

  typedef PhysD1 PhysDim;
  static const int D = PDE::D; // physical dimensions
  static const int N = PDE::N; // total solution variables

  static const int NBC = N;         // total BCs

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;   // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>; // matrices

  typedef typename PDE::QInterpreter QInterpreter; // solution variable interpreter

  // cppcheck-suppress noExplicitConstructor
  template <class Variables>
  BCTwoPhase1D( const PDE& pde, const TwoPhaseVariableType<Variables>& data, const bool isOutflow = false ) :
      pde_(pde), qInterpreter_(pde_.variableInterpreter()),
      qB_(pde.setDOFFrom(data)), isOutflow_(isOutflow) {}

  BCTwoPhase1D(const PDE& pde, const PyDict& d ) :
    pde_(pde), qInterpreter_(pde_.variableInterpreter()),
    qB_(pde.setDOFFrom(d)), isOutflow_(d.get(ParamsType::params.isOutflow)) {}

  virtual ~BCTwoPhase1D() {}

  BCTwoPhase1D( const BCTwoPhase1D& ) = delete;
  BCTwoPhase1D& operator=( const BCTwoPhase1D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return pde_.hasFluxViscous(); }

  // BC data
  template <class T>
  void state( const Real& x, const Real& time,
              const Real& nx, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    if (isOutflow_)
    {
      //Set pressure from BC data and saturation from interior
      Real pnB = 0;
      T SwI = 0;
      qInterpreter_.eval_pn(qB_, pnB );
      qInterpreter_.eval_Sw(qI, SwI );
      qInterpreter_.setFromPrimitive( qB, PressureNonWet_SaturationWet<T>(pnB, SwI) );
    }
    else
      qB = qB_;
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormalSpaceTime( const Real& x, const Real& time,
                            const Real& nx, const Real& nt,
                            const ArrayQ<T>& qI,
                            const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
                            const ArrayQ<T>& qB,
                            ArrayQ<Tf>& Fn) const
  {
    // Add upwinded advective flux
    pde_.fluxAdvectiveUpwindSpaceTime(x, time, qI, qB, nx, nt, Fn);

    // Add viscous flux
    ArrayQ<Tf> fx = 0, ft = 0;
    pde_.fluxViscousSpaceTime(x, time, qB, qIx, qIt, fx, ft);
    Fn += fx*nx + ft*nt;
  }

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormalSpaceTime( const Tp& param,
                            const Real& x, const Real& time,
                            const Real& nx, const Real& nt,
                            const ArrayQ<T>& qI,
                            const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
                            const ArrayQ<T>& qB,
                            ArrayQ<Tf>& Fn) const
  {
    // Add upwinded advective flux
    pde_.fluxAdvectiveUpwindSpaceTime(param, x, time, qI, qB, nx, nt, Fn);

    // Add viscous flux
    ArrayQ<Tf> fx = 0, ft = 0;
    pde_.fluxViscousSpaceTime(param, x, time, qB, qIx, qIt, fx, ft);
    Fn += fx*nx + ft*nt;
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& nt, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const QInterpreter& qInterpreter_;
  const ArrayQ<Real> qB_;
  const bool isOutflow_;
};


//----------------------------------------------------------------------------//
// Struct for creating a BC solution function
//
struct BCTwoPhase1DSolutionParams : noncopyable
{
  struct SolutionOptions
  {
    typedef DictKeyPair ExtractType;
    const ParameterString Name{"Name", NO_DEFAULT, "Solution function used on the BC" };
    const ParameterString& key = Name;

    const DictOption ThreeConstSaturation{"ThreeConstSaturation", SolutionFunction_TwoPhase1D_3ConstSaturation_Params::checkInputs};

    const std::vector<DictOption> options{ ThreeConstSaturation };
  };
  const ParameterOption<SolutionOptions> Function{"Function", NO_DEFAULT, "The BC is a solution function"};
};

// Struct for creating the solution pointer
template <class QInterpreter, template <class> class PDETraitsSize>
struct BCTwoPhase1DSolution
{
  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

  typedef std::shared_ptr<Function1DBase<ArrayQ<Real>>> Function_ptr;

  static Function_ptr getSolution(const PyDict& d, const QInterpreter& qInterpreter);
};


//----------------------------------------------------------------------------//
// Full state with Solution Function
//----------------------------------------------------------------------------//
template<>
struct BCTwoPhase1DParams< BCTypeFunction_mitState > : BCTwoPhase1DSolutionParams
{
  static constexpr const char* BCName{"Function_mitState"};
  struct Option
  {
    const DictOption Function_mitState{BCTwoPhase1DParams::BCName, BCTwoPhase1DParams::checkInputs};
  };

  static void checkInputs(PyDict d);
  static BCTwoPhase1DParams params;
};


template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
class BCTwoPhase1D<BCTypeFunction_mitState, PDE_<PDETraitsSize,PDETraitsModel>> :
    public BCType< BCTwoPhase1D<BCTypeFunction_mitState, PDE_<PDETraitsSize,PDETraitsModel>> >
{
public:
  typedef BCTypeFunction_mitState BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCTwoPhase1DParams<BCType> ParamsType;
  typedef PDE_<PDETraitsSize,PDETraitsModel> PDE;

  typedef typename PDE::QInterpreter QInterpreter; // solution variable interpreter type

  typedef PhysD1 PhysDim;
  static const int D = PDE::D; // physical dimensions
  static const int N = PDE::N; // total solution variables

  static const int NBC = N; // total BCs

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>; // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>; // matrices

  typedef std::shared_ptr<Function1DBase<ArrayQ<Real>>> Function_ptr;

  // cppcheck-suppress noExplicitConstructor
  BCTwoPhase1D( const PDE& pde, const Function_ptr& solution ) :
      pde_(pde), solution_(solution) {}
  ~BCTwoPhase1D() {}

  BCTwoPhase1D(const PDE& pde, const PyDict& d ) :
    pde_(pde),
    solution_( BCTwoPhase1DSolution<QInterpreter, PDETraitsSize>::getSolution(d, pde_.variableInterpreter()) ) {}

  BCTwoPhase1D( const BCTwoPhase1D& ) = delete;
  BCTwoPhase1D& operator=( const BCTwoPhase1D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return pde_.hasFluxViscous(); }

  // BC data
  template <class T>
  void state( const Real& x, const Real& time, const Real& nx,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    qB = (*solution_)(x,time);
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& time,
                   const Real& nx,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    // Add upwinded advective flux
    pde_.fluxAdvectiveUpwind(x, time, qI, qB, nx, Fn);

    ArrayQ<Tf> fv = 0;

    pde_.fluxViscous(x, time, qB, qIx, fv);

    // Add viscous flux
    Fn += fv*nx;
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const Function_ptr solution_;
};

//----------------------------------------------------------------------------//
// SpaceTime IC BC with function:
//----------------------------------------------------------------------------//
template<>
struct BCTwoPhase1DParams< BCTypeTimeIC_Function > : BCTwoPhase1DSolutionParams
{
  static constexpr const char* BCName{"TimeIC_Function"};
  struct Option
  {
    const DictOption TimeIC_Function{BCTwoPhase1DParams::BCName, BCTwoPhase1DParams::checkInputs};
  };

  static void checkInputs(PyDict d);
  static BCTwoPhase1DParams params;
};


template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
class BCTwoPhase1D<BCTypeTimeIC_Function, PDE_<PDETraitsSize,PDETraitsModel>> :
    public BCType< BCTwoPhase1D<BCTypeTimeIC_Function, PDE_<PDETraitsSize,PDETraitsModel>> >
{
public:
  typedef BCTypeTimeIC_Function BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCTwoPhase1DParams<BCType> ParamsType;
  typedef PDE_<PDETraitsSize,PDETraitsModel> PDE;

  typedef typename PDE::QInterpreter QInterpreter; // solution variable interpreter type

  typedef PhysD1 PhysDim;
  static const int D = PDE::D; // physical dimensions
  static const int N = PDE::N; // total solution variables

  static const int NBC = N; // total BCs

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>; // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>; // matrices

  typedef std::shared_ptr<Function1DBase<ArrayQ<Real>>> Function_ptr;

  // cppcheck-suppress noExplicitConstructor
  BCTwoPhase1D( const PDE& pde, const Function_ptr& solution ) :
      pde_(pde), solution_(solution) {}
  ~BCTwoPhase1D() {}

  BCTwoPhase1D(const PDE& pde, const PyDict& d ) :
    pde_(pde),
    solution_( BCTwoPhase1DSolution<QInterpreter, PDETraitsSize>::getSolution(d, pde_.variableInterpreter()) ) {}

  BCTwoPhase1D( const BCTwoPhase1D& ) = delete;
  BCTwoPhase1D& operator=( const BCTwoPhase1D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return pde_.hasFluxViscous(); }

  // BC data
  template <class T>
  void state( const Real& x, const Real& time, const Real& nx, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    qB = (*solution_)(x, time);
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormalSpaceTime( const Real& x, const Real& time,
                            const Real& nx, const Real& nt,
                            const ArrayQ<T>& qI,
                            const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
                            const ArrayQ<T>& qB,
                            ArrayQ<Tf>& Fn) const
  {
    ArrayQ<T> uB = 0;
    pde_.fluxAdvectiveTime(x, time, qB, uB);
    Fn += nt*uB;

    //Add any contributions from space-time diffusive fluxes
    ArrayQ<Tf> fvx = 0, fvt = 0;
    pde_.fluxViscousSpaceTime(x, time, qB, qIx, qIt, fvx, fvt);
    Fn += fvx*nx + fvt*nt;
  }

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormalSpaceTime( const Tp& param,
                            const Real& x, const Real& time,
                            const Real& nx, const Real& nt,
                            const ArrayQ<T>& qI,
                            const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
                            const ArrayQ<T>& qB,
                            ArrayQ<Tf>& Fn) const
  {
    ArrayQ<T> uB = 0;
    pde_.fluxAdvectiveTime(param, x, time, qB, uB);
    Fn += nt*uB;

    //Add any contributions from space-time diffusive fluxes
    ArrayQ<Tf> fvx = 0, fvt = 0;
    pde_.fluxViscousSpaceTime(param, x, time, qB, qIx, qIt, fvx, fvt);
    Fn += fvx*nx + fvt*nt;
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& nt, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const Function_ptr solution_;
};

} //namespace SANS

#endif  // BCTWOPHASE1D_H
