// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef POROSITYMODEL_H
#define POROSITYMODEL_H

// Porosity models

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <cmath> // exp
#include <iostream>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "CartTable.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// PorosityModel_Constant:  constant porosity
//
// template parameters:
//   T                        solution DOF data type (e.g. double)
//
// member functions:
//   .phi_ref                 get the reference porosity
//   .Cr                      get the rock compressibility
//
//   .porosity                porosity phi(pn) - function of non-wetting phase pressure
//   .porosityJacobian        jacobian: d(phi)/d(pn)
//   .porosityGradient        gradient: d(phi)/dx and d(phi)/dy
//
//   .dump                    debug dump of private data
//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//
// A class for representing input parameters for PorosityModel_Constant
struct PorosityModel_Constant_Params : noncopyable
{
  const ParameterNumeric<Real> phi{"phi", NO_DEFAULT, 0, NO_LIMIT, "Porosity"};

  static void checkInputs(PyDict d);

  static PorosityModel_Constant_Params params;
};


class PorosityModel_Constant
{
public:
  PorosityModel_Constant_Params& params;

  explicit PorosityModel_Constant( const PyDict& d ) :
      params(PorosityModel_Constant_Params::params),
      phi_(d.get(params.phi)){}

  PorosityModel_Constant( const PorosityModel_Constant& model ) :
      params( PorosityModel_Constant_Params::params ),
      phi_(model.phi_){}

  explicit PorosityModel_Constant( Real phi ) :
      params(PorosityModel_Constant_Params::params),
      phi_(phi)
  {
    SANS_ASSERT( phi > 0 );
  }

  PorosityModel_Constant& operator=(const PorosityModel_Constant& model) = delete;

  ~PorosityModel_Constant() {}

  // Porosity
  template<class T>
  Real porosity( const Real& x, const Real& time, const T& p ) const
  {
    return phi_;
  }
  template<class T>
  Real porosity( const Real& x, const Real& y, const Real& time, const T& p ) const
  {
    return phi_;
  }

  template<class T>
  void porosityJacobian( const Real& x, const Real& time, const T& p, T& phi_p ) const { phi_p = 0.0; }
  template<class T>
  void porosityJacobian( const Real& x, const Real& y, const Real& time, const T& p, T& phi_p ) const { phi_p = 0.0; }

  template<class T>
  void porosityGradient( const Real& x, const Real& time, const T& p, const T& px, T& phix ) const { phix = 0.0; }
  template<class T>
  void porosityGradient( const Real& x, const Real& y, const Real& time, const T& p, const T& px, const T& py,
                         T& phix, T& phiy ) const
  {
    phix = 0.0;
    phiy = 0.0;
  }

  void dump( std::string indent, std::ostream& out = std::cout ) const;
  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  Real phi_; // Porosity (constant)
};


//----------------------------------------------------------------------------//
// PorosityModel_Comp:  compressibility model
//
// template parameters:
//   T                        solution DOF data type (e.g. double)
//
// member functions:
//   .phi_ref                 get the reference porosity
//   .Cr                      get the rock compressibility
//
//   .porosity                porosity phi(pn) - function of non-wetting phase pressure
//   .porosityJacobian        jacobian: d(phi)/d(pn)
//   .porosityGradient        gradient: d(phi)/dx and d(phi)/dy
//
//   .dump                    debug dump of private data
//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//
// A class for representing input parameters for PorosityModel_Comp
struct PorosityModel_Comp_Params : noncopyable
{
  const ParameterNumeric<Real> phi_ref{"phi_ref", NO_DEFAULT, 0, NO_LIMIT, "Reference porosity"};
  const ParameterNumeric<Real> Cr{"Cr", NO_DEFAULT, 0, NO_LIMIT, "Rock compressibility"};

  static void checkInputs(PyDict d);

  static PorosityModel_Comp_Params params;
};


class PorosityModel_Comp
{
public:
  PorosityModel_Comp_Params& params;

  explicit PorosityModel_Comp( const PyDict& d, const Real& pref ) :
      params(PorosityModel_Comp_Params::params),
      phi_ref_(d.get(params.phi_ref)),
      Cr_(d.get(params.Cr)),
      pref_(pref){}

  PorosityModel_Comp( const PorosityModel_Comp& model ) :
      params( PorosityModel_Comp_Params::params ),
      phi_ref_(model.phi_ref_),
      Cr_(model.Cr_),
      pref_(model.pref_){}

  PorosityModel_Comp( Real phi_ref, Real Cr, Real pref ) :
      params(PorosityModel_Comp_Params::params),
      phi_ref_(phi_ref),
      Cr_(Cr),
      pref_(pref)
  {
    SANS_ASSERT( phi_ref > 0 );
    SANS_ASSERT( Cr >= 0 );
  }

  PorosityModel_Comp& operator=(const PorosityModel_Comp& model) = delete;

  ~PorosityModel_Comp() {}

  Real phi_ref() const { return phi_ref_; }
  Real Cr() const { return Cr_; }

  // Porosity
  template<class T>
  T porosity( const Real& x, const Real& time, const T& p ) const;
  template<class T>
  T porosity( const Real& x, const Real& y, const Real& time, const T& p ) const;

  template<class T>
  void porosityJacobian( const Real& x, const Real& time, const T& p, T& phi_p ) const;
  template<class T>
  void porosityJacobian( const Real& x, const Real& y, const Real& time, const T& p, T& phi_p ) const;

  template<class T>
  void porosityGradient( const Real& x, const Real& time, const T& pn, const T& px, T& phix ) const;
  template<class T>
  void porosityGradient( const Real& x, const Real& y, const Real& time, const T& pn, const T& px, const T& py,
                         T& phix, T& phiy ) const;

  void dump( std::string indent, std::ostream& out = std::cout ) const;
  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  Real phi_ref_; // Reference porosity
  Real Cr_; // Rock compressibility
  const Real pref_; //Reference pressure
};


template <class T>
inline T
PorosityModel_Comp::porosity( const Real& x, const Real& time, const T& p) const
{
  T phi = phi_ref_*exp(Cr_*(p - pref_));
  return phi;
}

template <class T>
inline T
PorosityModel_Comp::porosity( const Real& x, const Real& y, const Real& time, const T& p) const
{
  T phi = phi_ref_*exp(Cr_*(p - pref_));
  return phi;
}

template <class T>
inline void
PorosityModel_Comp::porosityJacobian( const Real& x, const Real& time, const T& p, T& phi_p ) const
{
  phi_p = Cr_*phi_ref_*exp(Cr_*(p - pref_));
}

template <class T>
inline void
PorosityModel_Comp::porosityJacobian( const Real& x, const Real& y, const Real& time, const T& p, T& phi_p ) const
{
  phi_p = Cr_*phi_ref_*exp(Cr_*(p - pref_));
}

#if 1
template <class T>
inline void
PorosityModel_Comp::porosityGradient( const Real& x, const Real& time, const T& p, const T& px, T& phix ) const
{
  //Compute Jacobian
  T phi_p;
  porosityJacobian( x, time, p, phi_p );

  //Apply chain rule for gradient: d(phi)/dx = d(phi)/dp * dp/dx
  phix = phi_p*px;
}

template <class T>
inline void
PorosityModel_Comp::porosityGradient( const Real& x, const Real& y, const Real& time, const T& p, const T& px, const T& py,
                                      T& phix, T& phiy) const
{
  //Compute Jacobian
  T phi_p;
  porosityJacobian( x, y, time, p, phi_p );

  //Apply chain rule for gradient: d(phi)/dx = d(phi)/dp * dp/dx
  phix = phi_p*px;
  phiy = phi_p*py;
}
#endif


//----------------------------------------------------------------------------//
// PorosityModel_CartTable:  table lookup
//
// template parameters:
//   T                        solution DOF data type (e.g. double)
//
// member functions:
//   .phi_ref                 get the reference porosity
//   .Cr                      get the rock compressibility
//
//   .porosity                porosity phi(pn) - function of non-wetting phase pressure
//   .porosityJacobian        jacobian: d(phi)/d(pn)
//   .porosityGradient        gradient: d(phi)/dx and d(phi)/dy
//
//   .dump                    debug dump of private data
//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//
// A class for representing input parameters for PorosityModel_CartTable
struct PorosityModel_CartTable_Params : noncopyable
{
  const ParameterFileName PorosityFile{"PorosityFile", std::ios_base::in, "", "File name with Cartesian porosity table"};
  const ParameterNumeric<Real> Cr{"Cr", NO_DEFAULT, 0, NO_LIMIT, "Rock compressibility"};

  static void checkInputs(PyDict d);

  static PorosityModel_CartTable_Params params;
};


class PorosityModel_CartTable
{
public:
  PorosityModel_CartTable_Params& params;

  explicit PorosityModel_CartTable( const PyDict& d, const Real& pref ) :
      params(PorosityModel_CartTable_Params::params),
      phi_table_(d.get(params.PorosityFile)),
      Cr_(d.get(params.Cr)), pref_(pref){}

  PorosityModel_CartTable( const PorosityModel_CartTable& model ) :
      params( PorosityModel_CartTable_Params::params ),
      phi_table_(model.phi_table_),
      Cr_(model.Cr_), pref_(model.pref_){}

  explicit PorosityModel_CartTable( const std::string& filename,
                                    const Real& Cr, const Real& pref ) :
      params(PorosityModel_CartTable_Params::params),
      phi_table_(filename), Cr_(Cr), pref_(pref)
  {
    SANS_ASSERT( Cr >= 0 );
  }

  PorosityModel_CartTable& operator=(const PorosityModel_Constant& model) = delete;

  ~PorosityModel_CartTable() = default;

  // Porosity
  template<class T>
  T porosity( const Real& x, const Real& time, const T& p ) const
  {
    SANS_DEVELOPER_EXCEPTION("1D table look-up not allowed");
    return 0;
  }
  template<class T>
  T porosity( const Real& x, const Real& y, const Real& time, const T& p ) const
  {
    return phi_table_(x,y)*exp(Cr_*(p - pref_));
  }

  template<class T>
  void porosityJacobian( const Real& x, const Real& time, const T& p, T& phi_p ) const
  {
    SANS_DEVELOPER_EXCEPTION("1D table look-up not allowed");
    phi_p = 0;
  }
  template<class T>
  void porosityJacobian( const Real& x, const Real& y, const Real& time, const T& p, T& phi_p ) const
  {
    phi_p = Cr_*phi_table_(x,y)*exp(Cr_*(p - pref_));
  }

  template<class T>
  void porosityGradient( const Real& x, const Real& time, const T& p, const T& px, T& phix ) const
  {
    SANS_DEVELOPER_EXCEPTION("1D table look-up not allowed");
    phix = 0;
  }
  template<class T>
  void porosityGradient( const Real& x, const Real& y, const Real& time, const T& p, const T& px, const T& py,
                         T& phix, T& phiy ) const
  {
    //Compute Jacobian
    T phi_p;
    porosityJacobian( x, y, time, p, phi_p );

    //Apply chain rule for gradient: d(phi)/dx = d(phi)/dp * dp/dx
    phix = phi_p*px;
    phiy = phi_p*py;
  }

  void dump( std::string indent, std::ostream& out = std::cout ) const;
  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  CartTable<2> phi_table_; // Reference porosity table lookup
  Real Cr_; // Rock compressibility
  const Real pref_; //Reference pressure
};

} //namespace SANS

#endif  // POROSITYMODEL_H
