// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCTwoPhase2D.h"
#include "Q2DPrimitive_pnSw.h"
#include "Q2DPrimitive_LogpnSw.h"
#include "Q2DPrimitive_Surrogate_pnSw.h"
#include "TraitsTwoPhaseArtificialViscosity.h"

#include "DensityModel.h"
#include "PorosityModel.h"
#include "RelPermModel_PowerLaw.h"
#include "ViscosityModel_Constant.h"
#include "PermeabilityModel2D.h"
#include "CapillaryModel.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{
//===========================================================================//
//
// Parameter classes
//

BCTwoPhase2DParams<BCTypeTimeIC>::BCTwoPhase2DParams()
  : StateVector(TwoPhaseVariableTypeParams::params.StateVector)
{}

// cppcheck-suppress passedByValue
void BCTwoPhase2DParams<BCTypeTimeIC>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.StateVector));
  d.checkUnknownInputs(allParams);
}
BCTwoPhase2DParams<BCTypeTimeIC> BCTwoPhase2DParams<BCTypeTimeIC>::params;

BCTwoPhase2DParams<BCTypeFullState>::BCTwoPhase2DParams()
  : StateVector(TwoPhaseVariableTypeParams::params.StateVector)
{}

// cppcheck-suppress passedByValue
void BCTwoPhase2DParams<BCTypeFullState>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.StateVector));
  d.checkUnknownInputs(allParams);
}
BCTwoPhase2DParams<BCTypeFullState> BCTwoPhase2DParams<BCTypeFullState>::params;

// cppcheck-suppress passedByValue
void BCTwoPhase2DParams<BCTypeNoFlux>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  d.checkUnknownInputs(allParams);
}
BCTwoPhase2DParams<BCTypeNoFlux> BCTwoPhase2DParams<BCTypeNoFlux>::params;

// cppcheck-suppress passedByValue
void BCTwoPhase2DParams<BCTypeNoFluxSpaceTime>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  d.checkUnknownInputs(allParams);
}
BCTwoPhase2DParams<BCTypeNoFluxSpaceTime> BCTwoPhase2DParams<BCTypeNoFluxSpaceTime>::params;

#if 1
template<class QInterpreter, template <class> class PDETraitsSize>
typename BCTwoPhase2DSolution<QInterpreter, PDETraitsSize>::Function_ptr
BCTwoPhase2DSolution<QInterpreter, PDETraitsSize>::getSolution(PyDict d, const QInterpreter& qInterpreter)
{
  BCTwoPhase2DSolutionParams params;

  DictKeyPair SolutionParam = d.get(params.Function);

  if ( SolutionParam == params.Function.SingleWell_Peaceman )
    return Function_ptr( new SolutionFunction_TwoPhase2D_SingleWell_Peaceman<QInterpreter, PDETraitsSize>( SolutionParam, qInterpreter ) );

  // Should never get here if checkInputs was called
  SANS_DEVELOPER_EXCEPTION("Unrecognized solution function specified");
  return NULL; // suppress compiler warnings
}
template struct BCTwoPhase2DSolution<Q2D<QTypePrimitive_pnSw, CapillaryModel_Linear, TraitsSizeTwoPhase>, TraitsSizeTwoPhase>;
template struct BCTwoPhase2DSolution<Q2D<QTypePrimitive_LogpnSw, CapillaryModel_Linear, TraitsSizeTwoPhase>, TraitsSizeTwoPhase>;
template struct BCTwoPhase2DSolution<Q2D<QTypePrimitive_pnSw, CapillaryModel_Linear, TraitsSizeTwoPhaseArtificialViscosity>,
                                     TraitsSizeTwoPhaseArtificialViscosity>;

void BCTwoPhase2DParams< BCTypeFunction_mitState >::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Function));
  d.checkUnknownInputs(allParams);
}
BCTwoPhase2DParams< BCTypeFunction_mitState > BCTwoPhase2DParams< BCTypeFunction_mitState >::params;
#endif

//===========================================================================//

typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel_Comp, DensityModel_Comp, PorosityModel_Comp,
                            RelPermModel_PowerLaw, RelPermModel_PowerLaw, ViscosityModel_Constant, ViscosityModel_Constant,
                            PermeabilityModel2D_Constant, CapillaryModel_Linear> TraitsModel_RelPower_ViscConst_PermConst_CapLinear;

typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel_Comp, DensityModel_Comp, PorosityModel_Comp,
                            RelPermModel_PowerLaw, RelPermModel_PowerLaw, ViscosityModel_Constant, ViscosityModel_Constant,
                            PermeabilityModel2D_QuadBlock, CapillaryModel_Linear> TraitsModel_RelPower_ViscConst_PermQuadBlock_CapLinear;

typedef TraitsModelTwoPhase<QTypePrimitive_LogpnSw, DensityModel_Comp, DensityModel_Comp, PorosityModel_Comp,
                            RelPermModel_PowerLaw, RelPermModel_PowerLaw, ViscosityModel_Constant, ViscosityModel_Constant,
                            PermeabilityModel2D_QuadBlock, CapillaryModel_Linear> TraitsModel_RelPower_ViscConst_PermQuadBlock_CapLinear_Logpn;

typedef TraitsModelTwoPhase<QTypePrimitive_Surrogate_pnSw, DensityModel_Comp, DensityModel_Comp, PorosityModel_Comp,
                            RelPermModel_PowerLaw, RelPermModel_PowerLaw, ViscosityModel_Constant, ViscosityModel_Constant,
                            PermeabilityModel2D_QuadBlock, CapillaryModel_Linear> TraitsModel_RelPower_ViscConst_PermQuadBlock_CapLinear_Surrogate;


typedef PDETwoPhase2D<TraitsSizeTwoPhase,
                      TraitsModel_RelPower_ViscConst_PermConst_CapLinear> PDE_Comp_RelPower_ViscConst_PermConst_CapLinear;

typedef PDETwoPhase2D<TraitsSizeTwoPhase,
                      TraitsModel_RelPower_ViscConst_PermQuadBlock_CapLinear> PDE_Comp_RelPower_ViscConst_PermQuadBlock_CapLinear;

typedef PDETwoPhase2D<TraitsSizeTwoPhase,
                      TraitsModel_RelPower_ViscConst_PermQuadBlock_CapLinear_Logpn> PDE_Comp_RelPower_ViscConst_PermQuadBlock_CapLinear_Logpn;

typedef PDETwoPhase2D<TraitsSizeTwoPhase,
                      TraitsModel_RelPower_ViscConst_PermQuadBlock_CapLinear_Surrogate>
        PDE_Comp_RelPower_ViscConst_PermQuadBlock_CapLinear_Surrogate;

// Instantiate the BC parameters
BCPARAMETER_INSTANTIATE( BCTwoPhase2DVector<PDE_Comp_RelPower_ViscConst_PermConst_CapLinear> )
BCPARAMETER_INSTANTIATE( BCTwoPhase2DVector<PDE_Comp_RelPower_ViscConst_PermQuadBlock_CapLinear> )
BCPARAMETER_INSTANTIATE( BCTwoPhase2DVector<PDE_Comp_RelPower_ViscConst_PermQuadBlock_CapLinear_Logpn> )
BCPARAMETER_INSTANTIATE( BCTwoPhase2DVector<PDE_Comp_RelPower_ViscConst_PermQuadBlock_CapLinear_Surrogate> )

//===========================================================================//


template <class PDE>
void
BCTwoPhase2D<BCTypeFullState, PDE>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCTwoPhase2D<BCTypeFullState, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << std::endl;
  out << indent << "  qB_ = " << qB_ << std::endl;
}

template <class PDE>
void
BCTwoPhase2D<BCTypeNoFlux, PDE>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCTwoPhase2D<BCTypeNoFlux>" << std::endl;
}

// Explicit instantiations
template class BCNone<PhysD2,TraitsSizeTwoPhase<PhysD2>::N>;
template class BCTwoPhase2D< BCTypeTimeIC   , PDE_Comp_RelPower_ViscConst_PermConst_CapLinear >;
template class BCTwoPhase2D< BCTypeFullState, PDE_Comp_RelPower_ViscConst_PermConst_CapLinear >;
template class BCTwoPhase2D< BCTypeNoFlux   , PDE_Comp_RelPower_ViscConst_PermConst_CapLinear >;

} //namespace SANS
