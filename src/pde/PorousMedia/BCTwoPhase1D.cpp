// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCTwoPhase1D.h"
#include "Q1DPrimitive_pnSw.h"
#include "TraitsTwoPhaseArtificialViscosity.h"

#include "DensityModel.h"
#include "PorosityModel.h"
#include "RelPermModel_PowerLaw.h"
#include "ViscosityModel_Constant.h"
#include "CapillaryModel.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{
//===========================================================================//
//
// Parameter classes
//
BCTwoPhase1DParams<BCTypeTimeIC>::BCTwoPhase1DParams()
  : StateVector(TwoPhaseVariableTypeParams::params.StateVector)
{}

// cppcheck-suppress passedByValue
void BCTwoPhase1DParams<BCTypeTimeIC>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.StateVector));
  d.checkUnknownInputs(allParams);
}
BCTwoPhase1DParams<BCTypeTimeIC> BCTwoPhase1DParams<BCTypeTimeIC>::params;

// cppcheck-suppress passedByValue
void BCTwoPhase1DParams<BCTypeTimeIC_Function>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Function));
  d.checkUnknownInputs(allParams);
}
BCTwoPhase1DParams<BCTypeTimeIC_Function> BCTwoPhase1DParams<BCTypeTimeIC_Function>::params;

// cppcheck-suppress passedByValue
void BCTwoPhase1DParams<BCTypeFullState>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.StateVector));
  allParams.push_back(d.checkInputs(params.isOutflow));
  d.checkUnknownInputs(allParams);
}
BCTwoPhase1DParams<BCTypeFullState> BCTwoPhase1DParams<BCTypeFullState>::params;

BCTwoPhase1DParams<BCTypeFullState>::BCTwoPhase1DParams()
  : StateVector(TwoPhaseVariableTypeParams::params.StateVector)
{}

// cppcheck-suppress passedByValue
void BCTwoPhase1DParams<BCTypeFullStateSpaceTime>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.StateVector));
  allParams.push_back(d.checkInputs(params.isOutflow));
  d.checkUnknownInputs(allParams);
}
BCTwoPhase1DParams<BCTypeFullStateSpaceTime> BCTwoPhase1DParams<BCTypeFullStateSpaceTime>::params;

BCTwoPhase1DParams<BCTypeFullStateSpaceTime>::BCTwoPhase1DParams()
  : StateVector(TwoPhaseVariableTypeParams::params.StateVector)
{}


template<class QInterpreter, template <class> class PDETraitsSize>
typename BCTwoPhase1DSolution<QInterpreter, PDETraitsSize>::Function_ptr
BCTwoPhase1DSolution<QInterpreter, PDETraitsSize>::getSolution(const PyDict& d, const QInterpreter& qInterpreter)
{
  BCTwoPhase1DSolutionParams params;

  DictKeyPair SolutionParam = d.get(params.Function);

  if ( SolutionParam == params.Function.ThreeConstSaturation )
    return Function_ptr( new SolutionFunction_TwoPhase1D_3ConstSaturation<QInterpreter, PDETraitsSize>( SolutionParam, qInterpreter ) );

  // Should never get here if checkInputs was called
  SANS_DEVELOPER_EXCEPTION("Unrecognized solution function specified");
  return NULL; // suppress compiler warnings
}
template struct BCTwoPhase1DSolution<Q1D<QTypePrimitive_pnSw, CapillaryModel_Linear, TraitsSizeTwoPhase>, TraitsSizeTwoPhase>;
template struct BCTwoPhase1DSolution<Q1D<QTypePrimitive_pnSw, CapillaryModel_Linear, TraitsSizeTwoPhaseArtificialViscosity>,
                                     TraitsSizeTwoPhaseArtificialViscosity>;

// cppcheck-suppress passedByValue
void BCTwoPhase1DParams< BCTypeFunction_mitState >::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Function));
  d.checkUnknownInputs(allParams);
}
BCTwoPhase1DParams< BCTypeFunction_mitState > BCTwoPhase1DParams< BCTypeFunction_mitState >::params;

//===========================================================================//

typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel_Comp, DensityModel_Comp, PorosityModel_Comp,
                            RelPermModel_PowerLaw, RelPermModel_PowerLaw, ViscosityModel_Constant, ViscosityModel_Constant,
                            Real, CapillaryModel_Linear> TraitsModel_RelPower_ViscConst_CapLinear;


typedef PDETwoPhase1D<TraitsSizeTwoPhase, TraitsModel_RelPower_ViscConst_CapLinear> PDE_Comp_RelPower_ViscConst_CapLinear;

// Instantiate the BC parameters
BCPARAMETER_INSTANTIATE( BCTwoPhase1DVector<PDE_Comp_RelPower_ViscConst_CapLinear> )

//===========================================================================//

template <class PDE>
void
BCTwoPhase1D<BCTypeFullState, PDE>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCTwoPhase1D<BCTypeFullState, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << std::endl;
  out << indent << "  qB_ = " << qB_ << std::endl;
}


// Explicit instantiations
template class BCNone<PhysD1,TraitsSizeTwoPhase<PhysD1>::N>;
template class BCTwoPhase1D< BCTypeTimeIC   , PDE_Comp_RelPower_ViscConst_CapLinear >;
template class BCTwoPhase1D< BCTypeFullState, PDE_Comp_RelPower_ViscConst_CapLinear >;

} //namespace SANS
