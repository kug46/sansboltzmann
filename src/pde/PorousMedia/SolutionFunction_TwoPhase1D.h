// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOLUTIONFUNCTION_TWOPHASE1D_H
#define SOLUTIONFUNCTION_TWOPHASE1D_H

// Solution functions for two phase flow in 1D

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/smoothmath.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "TraitsTwoPhase.h"
#include "TwoPhaseVariable.h"

#include "pde/AnalyticFunction/Function1D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// solution: constant pressure field, discontinuous saturation field divided into 3 regions

struct SolutionFunction_TwoPhase1D_3ConstSaturation_Params : noncopyable
{
  const ParameterNumeric<Real> pn{"pn", NO_DEFAULT, 0, NO_LIMIT, "Saturation constant in inner region"};
  const ParameterNumeric<Real> Sw_left{"Sw_left", NO_DEFAULT, 0, 1, "Saturation constant in leftmost region"};
  const ParameterNumeric<Real> Sw_mid{"Sw_mid", NO_DEFAULT, 0, 1, "Saturation constant in middle region"};
  const ParameterNumeric<Real> Sw_right{"Sw_right", NO_DEFAULT, 0, 1, "Saturation constant in rightmost region"};
  const ParameterNumeric<Real> xLeftMid{"xLeftMid", NO_DEFAULT, NO_RANGE, "x-coordinate of division between left and middle regions"};
  const ParameterNumeric<Real> xMidRight{"xMidRight", NO_DEFAULT, NO_RANGE, "x-coordinate of division between middle and right regions"};
  const ParameterNumeric<Real> Ltransit{"Ltransit", 0, 0, NO_LIMIT, "Transition length scale"};

  static void checkInputs(PyDict d);

  static SolutionFunction_TwoPhase1D_3ConstSaturation_Params params;
};


template <class QInterpreter, template <class> class PDETraitsSize>
class SolutionFunction_TwoPhase1D_3ConstSaturation :
    public Function1DVirtualInterface<SolutionFunction_TwoPhase1D_3ConstSaturation<QInterpreter, PDETraitsSize>, PDETraitsSize<PhysD1>>
{
public:
  typedef PhysD1 PhysDim;

  typedef SolutionFunction_TwoPhase1D_3ConstSaturation_Params ParamsType;

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;  // solution/residual arrays

  SolutionFunction_TwoPhase1D_3ConstSaturation( const PyDict& d, const QInterpreter& qInterpreter ) :
    pn_(d.get(ParamsType::params.pn)),
    Sw_left_(d.get(ParamsType::params.Sw_left)),
    Sw_mid_(d.get(ParamsType::params.Sw_mid)),
    Sw_right_(d.get(ParamsType::params.Sw_right)),
    x_LM_(d.get(ParamsType::params.xLeftMid)),
    x_MR_(d.get(ParamsType::params.xMidRight)),
    qInterpreter_(qInterpreter),
    Ltransit_(d.get(ParamsType::params.Ltransit))
  {
    SANS_ASSERT(x_MR_ > x_LM_);
  }

  SolutionFunction_TwoPhase1D_3ConstSaturation( const Real& pn, const Real& Sw_left, const Real& Sw_mid,
                                                const Real& Sw_right, const Real& xLeftMid, const Real& xMidRight,
                                                const QInterpreter& qInterpreter, const Real Ltransit = 0.0 ) :
    pn_(pn),
    Sw_left_(Sw_left),
    Sw_mid_(Sw_mid),
    Sw_right_(Sw_right),
    x_LM_(xLeftMid),
    x_MR_(xMidRight),
    qInterpreter_(qInterpreter),
    Ltransit_(Ltransit)
  {
    SANS_ASSERT(x_MR_ > x_LM_);
  }

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
    ArrayQ<T> q = 0;

    if (x < x_LM_ - 0.5*Ltransit_)
    {
      PressureNonWet_SaturationWet<T> qdata(pn_, Sw_left_);
      qInterpreter_.setFromPrimitive(q, qdata);
      return q;
    }
    else if (x >= x_LM_ - 0.5*Ltransit_ && x < x_LM_ + 0.5*Ltransit_)
    {
      T xf = (x - (x_LM_ - 0.5*Ltransit_))/Ltransit_;
      T Sw_interp = Sw_left_ + smoothActivation_cubic(xf)*(Sw_mid_ - Sw_left_);
      PressureNonWet_SaturationWet<T> qdata(pn_, Sw_interp);
      qInterpreter_.setFromPrimitive(q, qdata);
      return q;
    }
    else if (x >= x_LM_ + 0.5*Ltransit_ && x < x_MR_ - 0.5*Ltransit_)
    {
      PressureNonWet_SaturationWet<T> qdata(pn_, Sw_mid_);
      qInterpreter_.setFromPrimitive(q, qdata);
      return q;
    }
    else if (x >= x_MR_ - 0.5*Ltransit_ && x < x_MR_ + 0.5*Ltransit_)
    {
      T xf = (x - (x_MR_ - 0.5*Ltransit_))/Ltransit_;
      T Sw_interp = Sw_mid_ + smoothActivation_cubic(xf)*(Sw_right_ - Sw_mid_);
      PressureNonWet_SaturationWet<T> qdata(pn_, Sw_interp);
      qInterpreter_.setFromPrimitive(q, qdata);
      return q;
    }
    else if (x >= x_MR_ + 0.5*Ltransit_)
    {
      PressureNonWet_SaturationWet<T> qdata(pn_, Sw_right_);
      qInterpreter_.setFromPrimitive(q, qdata);
      return q;
    }

    SANS_DEVELOPER_EXCEPTION("SolutionFunction_TwoPhase1D_3ConstSaturation - Code should not get here.");
    return q;
  }

private:
  const Real pn_;
  const Real Sw_left_, Sw_mid_, Sw_right_;
  const Real x_LM_, x_MR_;
  const QInterpreter qInterpreter_;
  const Real Ltransit_;
};

} // namespace SANS

#endif  // SOLUTIONFUNCTION_TWOPHASE1D_H
