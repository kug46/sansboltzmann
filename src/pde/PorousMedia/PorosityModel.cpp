// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "PorosityModel.h"

//Instantiate the CheckInput function
//PARAMETER_VECTOR_IMPL( PorosityModelParams );

namespace SANS
{

void PorosityModel_Constant_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.phi));
  d.checkUnknownInputs(allParams);
}
PorosityModel_Constant_Params PorosityModel_Constant_Params::params;

void
PorosityModel_Constant::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PorosityModel_Constant<T>:" << std::endl;
  out << indent << "  phi_ = " << phi_ << std::endl;
}

/*--------------------------------------------------------------------------------*/

void PorosityModel_Comp_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.phi_ref));
  allParams.push_back(d.checkInputs(params.Cr));
  d.checkUnknownInputs(allParams);
}
PorosityModel_Comp_Params PorosityModel_Comp_Params::params;

void
PorosityModel_Comp::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PorosityModel_Comp<T>:" << std::endl;
  out << indent << "  phi_ref_ = " << phi_ref_ << std::endl;
  out << indent << "  Cr_ = " << Cr_ << std::endl;
  out << indent << "  pref_ = " << pref_ << std::endl;
}

/*--------------------------------------------------------------------------------*/

void PorosityModel_CartTable_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.PorosityFile));
  allParams.push_back(d.checkInputs(params.Cr));
  d.checkUnknownInputs(allParams);
}
PorosityModel_CartTable_Params PorosityModel_CartTable_Params::params;

void
PorosityModel_CartTable::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PorosityModel_CartTable<T>:" << std::endl;
  out << indent << "  Cr_ = " << Cr_ << std::endl;
}

}
