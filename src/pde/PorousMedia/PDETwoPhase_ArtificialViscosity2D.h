// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDETWOPHASE_ARTIFICIALVISCOSITY2D_H
#define PDETWOPHASE_ARTIFICIALVISCOSITY2D_H

// 2D two-phase flow PDE class with artificial viscosity

#include <iostream>
#include <string>

#include "tools/SANSnumerics.h"     // Real
#include "tools/smoothmath.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Exp.h"

#include "Topology/Dimension.h"

#include "PDETwoPhase2D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: 2-D Two Phase
//
// equations: d/dt( rho_w phi Sw ) - div( rho_w K lambda_w grad(p_w) ) = rho_w q_w
//            d/dt( rho_n phi Sn ) - div( rho_n K lambda_n grad(p_n) ) = rho_n q_n
//
// where:    lambda_w = krw/mu_w
//           lambda_n = krn/mu_n
//
// template parameters:
//   T                        solution DOF data type (e.g. double)
//   QType                    solution variable set (e.g. primitive)
//   DensityModel             density models (for each phase)
//   PorosityModel            porosity model
//   RelPermModel             relative permeability models (for each phase)
//   ViscModel                viscosity models (for each phase)
//   CapillaryModel           capillary pressure model
//
// member functions:
//   .hasFluxAdvectiveTime        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//   .hasFluxViscous             T/F: PDE has viscous flux term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: dU/dQ
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .fluxViscous                viscous fluxes: Fv(Q, QX)
//   .diffusionViscous           viscous diffusion coefficient: d(Fv)/d(UX)
//   .isValidState               T/F: determine if state is physically valid (e.g. Sw > 0)
//   .setDOFFrom                 set from primitive variable array
//----------------------------------------------------------------------------//

template <template <class> class PDETraitsSize, class PDETraitsModel>
class PDETwoPhase_ArtificialViscosity2D : public PDETwoPhase2D<PDETraitsSize, PDETraitsModel>
{
public:
  typedef PhysD2 PhysDim;
  static const int D = PDETraitsSize<PhysDim>::D; // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N; // total solution variables

  typedef PDETwoPhase2D<PDETraitsSize, PDETraitsModel> BaseType;

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;   // solution/residual arrays

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>; // matrices

  using BaseType::qInterpreter_;
  using BaseType::rhow_;
  using BaseType::rhon_;
  using BaseType::krw_;
  using BaseType::krn_;
  using BaseType::muw_;
  using BaseType::mun_;
  using BaseType::K_;
  using BaseType::pc_;
  using BaseType::gravity_on_;
  using BaseType::gravity_const;

  // Constructor forwards arguments to PDE class using varargs
  template< class... PDEArgs >
  PDETwoPhase_ArtificialViscosity2D(const int order, const bool hasSpaceTimeDiffusion, PDEArgs&&... args) :
    BaseType(std::forward<PDEArgs>(args)...),
    order_(order), hasSpaceTimeDiffusion_(hasSpaceTimeDiffusion)
  {
    //Nothing
  }

  ~PDETwoPhase_ArtificialViscosity2D() {}

  PDETwoPhase_ArtificialViscosity2D& operator=( const PDETwoPhase_ArtificialViscosity2D& ) = delete;

  // flux components
  bool hasFluxAdvectiveTime() const { return true; }
  bool hasFluxAdvective() const { return BaseType::hasFluxAdvective(); }
  bool hasFluxViscous() const { return true; }
  bool hasSource() const { return BaseType::hasSource(); }
  bool hasSourceTrace() const { return BaseType::hasSourceTrace(); }
  bool hasForcingFunction() const { return BaseType::hasForcingFunction(); }

  // viscous flux is of the form K(q).gradu
  bool fluxViscousLinearInGradient() const { return true; }
  bool needsSolutionGradientforSource() const { return BaseType::needsSolutionGradientforSource(); }

  int PDEorder() const { return order_; }

  // unsteady temporal flux: Ft(Q)
  template <class Tp, class T, class Tf>
  void fluxAdvectiveTime(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tf>& ft ) const
  {
    BaseType::fluxAdvectiveTime(x, y, time, q, ft);
  }

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <class Tp, class T, class Tf>
  void jacobianFluxAdvectiveTime(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& J ) const
  {
    BaseType::jacobianFluxAdvectiveTime(x, y, time, q, J);
  }

  // master state: U(Q)
  template <class Tp, class T, class Tu>
  void masterState(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tu>& uCons ) const
  {
    BaseType::masterState(x, y, time, q, uCons);
  }

  // jacobian of master state wrt q: dU(Q)/dQ
  template<class Tp, class T>
  void jacobianMasterState(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
  {
    BaseType::jacobianMasterState( x, y, time, q, dudq);
  }

  // advective flux: F(Q)
  template <class Tp, class T, class Tf>
  void fluxAdvective(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
  {
    BaseType::fluxAdvective(x, y, time, q, f, g);
  }

  template <class Tp, class T, class Tf>
  void fluxAdvectiveUpwind(
      const Tp& param, const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f, const Real& scaleFactor = 1 ) const
  {
    BaseType::fluxAdvectiveUpwind(x, y, time, qL, qR, nx, ny, f, scaleFactor);
  }

  template <class Tp, class T, class Tf>
  void fluxAdvectiveUpwindSpaceTime(
      const Tp& param, const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, const Real& nt, ArrayQ<Tf>& f ) const
  {
    BaseType::fluxAdvectiveUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  }

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class Tp, class T>
  void jacobianFluxAdvective(
      const Tp& param, const Real& x, const Real& y,  const Real& time, const ArrayQ<T>& q,
      MatrixQ<T>& ax, MatrixQ<T>& ay ) const
  {
    BaseType::jacobianFluxAdvective( x, y, time, q, ax, ay );
  }

  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscous(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const;

  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscous(
      const Tp& paramL, const Tp& paramR, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const;

  // viscous diffusion matrix: d(Fv)/d(Ux)
  template <class Tp, class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy) const;

  // space-time viscous flux: Fv(Q, QX)
  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& ft ) const;

  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Tp& paramL, const Tp& paramR, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qtL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qtR,
      const Real& nx, const Real& ny, const Real& nt,
      ArrayQ<Tf>& f ) const;

  // space-time viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tp, class Tq, class Tg, class Tk>
  void diffusionViscousSpaceTime(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxt,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyt,
      MatrixQ<Tk>& ktx, MatrixQ<Tk>& kty, MatrixQ<Tk>& ktt ) const;

  // solution-dependent source: S(x, Q, Qx)
  template <class Tp, class Tq, class Tg, class Ts>
  void source(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Ts>& source ) const
  {
    BaseType::source( x, y, time, q, qx, qy, source );
  }

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX)
  template <class Tp, class Tlq, class Tq, class Tg, class Ts>
  void source(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Ts>& source ) const
  {
    BaseType::source( x, y, time, lifted_quantity, q, qx, qy, source );
  }

  // dual-consistent source
  template <class Tp, class Tq, class Tg, class Ts>
  void sourceTrace(
      const Tp& paramL, const Real& xL, const Real& yL,
      const Tp& paramR, const Real& xR, const Real& yR,
      const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
  {
    BaseType::sourceTrace(xL, yL, xR, yR, time, qL, qxL, qyL, qR, qxR, qyR, sourceL, sourceR);
  }

  // trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
  template <class Tp, class Tq, class Ts>
  void sourceLiftedQuantity(
      const Tp& paramL, const Real& xL, const Real& yL,
      const Tp& paramR, const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, Ts& s ) const
  {
    BaseType::sourceLiftedQuantity(xL, yL, xR, yR, time, qL, qR, s);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tp, class T>
  void jacobianSource(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
      MatrixQ<T>& dsdu ) const
  {
    BaseType::jacobianSource(x, y, time, q, qx, qy, dsdu);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tp, class T>
  void jacobianSourceAbsoluteValue(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
      MatrixQ<T>& dsdu ) const
  {
    BaseType::jacobianSourceAbsoluteValue(x, y, time, q, qx, qy, dsdu);
  }


  // right-hand-side forcing function
  template <class Tp, class T>
  void forcingFunction(
      const Tp& param,
      const Real& x, const Real& y, const Real& time, ArrayQ<T>& forcing ) const
  {
    BaseType::forcingFunction(x, y, time, forcing );
  }

  // characteristic speed (needed for timestep)
  template <class Tp, class T>
  void speedCharacteristic(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const Real& dx, const Real& dy, const ArrayQ<T>& q, T& speed ) const
  {
    BaseType::speedCharacteristic(x, y, time, dx, dy, q, speed);
  }

  // characteristic speed
  template <class Tp, class T>
  void speedCharacteristic(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, T& speed ) const
  {
    BaseType::speedCharacteristic(x, y, time, q, speed);
  }

  // update fraction needed for physically valid state
  template <class Tp>
  void updateFraction(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
      const Real maxChangeFraction, Real& updateFraction ) const
  {
    BaseType::updateFraction(x, y, time, q, dq, maxChangeFraction, updateFraction);
  }

  template<class Tq, class Tg, class Tv>
  void characteristicVelocity( const Real& x, const Real& y, const Real& time,
                               const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
                               Tv& vx, Tv& vy ) const;

  template<class Tq, class Tg, class Tv>
  void characteristicVelocitySinglePhase( const Real& x, const Real& y, const Real& time,
                                          const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
                                          ArrayQ<Tv>& vx, ArrayQ<Tv>& vy ) const;

  template <class Tp, class Tq, class Tg, class Tf>
  void artViscMax(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      Tf& nu_max ) const;

  std::vector<std::string> derivedQuantityNames() const;

  template<class Tp, class Tq, class Tg, class Tf>
  void derivedQuantity(
      const int& index, const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, Tf& J ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

  int getOrder() const { return order_; };

protected:

#if 0
  // Artificial viscosity - generalized H-tensor, sensor variable in augmented state vector
  template <class Th, class Tq, class Tk>
  void artificialViscosity( const DLA::MatrixSymS<D,Th>& logH, const Real& x, const Real& y, const Real& time,
                            const ArrayQ<Tq>& q,
                            MatrixQ<Tk>& Kxx, MatrixQ<Tk>& Kxy, MatrixQ<Tk>& Kyx, MatrixQ<Tk>& Kyy ) const;
#endif

  // Artificial viscosity flux - generalized H-tensor, sensor variable in augmented state vector
  template <class Tp, class Tq, class Tg, class Tf>
  void artificialViscosityFlux(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const;

  // Space-time artificial viscosity flux - generalized H-tensor, sensor variable in augmented state vector
  template <class Tp, class Tq, class Tg, class Tf>
  void artificialViscosityFluxSpaceTime(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& ft ) const;

  template<class Th, class T>
  T velocity_h( const Th& H, const T& vx, const T& vy ) const
  {
    Th Hsq = H*H; //H is a MatrixSymS<D,Real> or MatrixSymS<D+1,Real>

    T vT_Hsq_v = vx*(Hsq(0,0)*vx + Hsq(0,1)*vy) + vy*(Hsq(1,0)*vx + Hsq(1,1)*vy);
    return sqrt(vT_Hsq_v);
  }

  const int order_;
  const bool hasSpaceTimeDiffusion_;
};


#if 0
// Artificial viscosity - generalized H-tensor, sensor variable in augmented state vector
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Th, class Tq, class Tk>
void inline
PDETwoPhase_ArtificialViscosity2D<PDETraitsSize, PDETraitsModel>::
artificialViscosity(
    const DLA::MatrixSymS<D,Th>& logH, const Real& x, const Real& y, const Real& time,const ArrayQ<Tq>& q,
    MatrixQ<Tk>& Kxx, MatrixQ<Tk>& Kxy, MatrixQ<Tk>& Kyx, MatrixQ<Tk>& Kyy ) const
{
  SANS_ASSERT(N == 3); //Check if the state vector has been augmented with the sensor

  Tq lambda;
  Real C = 1.0;

  BaseType::speedCharacteristic( x, y, time, q, lambda );

  Tq sensor = q(N-1);
  Tk factor = C/(Real(order_)+1) * lambda * smoothabs0(sensor, 1.0e-5);

  DLA::MatrixSymS<D,Th> H = exp(logH); //generalized h-tensor

  for (int i = 0; i < N-1; i++)
  {
    Kxx(i,i) += factor*lambda*H(0,0); // [length^2 / time]
    Kxy(i,i) += factor*lambda*H(0,1); // [length^2 / time]
    Kyx(i,i) += factor*lambda*H(1,0); // [length^2 / time]
    Kyy(i,i) += factor*lambda*H(1,1); // [length^2 / time]
  }
}
#endif

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDETwoPhase_ArtificialViscosity2D<PDETraitsSize, PDETraitsModel>::artificialViscosityFlux(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Tf>& fx, ArrayQ<Tf>& fy ) const
{
  SANS_ASSERT(N == 3); //Check if the state vector has been augmented with the sensor

  typedef typename promote_Surreal<Tq,Tg>::type T;

  Tq nu = q(N-1); //artificial viscosity
  const Real alpha = 40.0;
  nu = smoothmax(nu, Tq(0.0), alpha); //ensure nu is non-negative (since it can go negative in under-resolved regions)

  Tq Sw = 0, Sn = 0, pw = 0, pn = 0;
  qInterpreter_.eval( q, pw, pn, Sw, Sn );

  //Evaluate densities
  Tq rhow = rhow_.density(x, y, time, pw);
  Tq rhon = rhon_.density(x, y, time, pn);

  T Swx = 0, Swy = 0;
  T Snx = 0, Sny = 0;
  qInterpreter_.eval_SwGradient( q, qx, qy, Swx, Swy );
  qInterpreter_.eval_SnGradient( q, qx, qy, Snx, Sny );

  fx(0) -=  rhow*nu*Swx;
  fx(1) -= -rhon*nu*Swx;

  fy(0) -=  rhow*nu*Swy;
  fy(1) -= -rhon*nu*Swy;
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDETwoPhase_ArtificialViscosity2D<PDETraitsSize, PDETraitsModel>::artificialViscosityFluxSpaceTime(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
    ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& ft ) const
{
  SANS_ASSERT(N == 3); //Check if the state vector has been augmented with the sensor

  typedef typename promote_Surreal<Tq,Tg>::type T;

  Tq nu = q(N-1); //artificial viscosity
  const Real alpha = 40.0;
  nu = smoothmax(nu, Tq(0.0), alpha); //ensure nu is non-negative (since it can go negative in under-resolved regions)

  Tq Sw = 0, Sn = 0, pw = 0, pn = 0;
  qInterpreter_.eval( q, pw, pn, Sw, Sn );

  //Evaluate densities
  Tq rhow = rhow_.density(x, y, time, pw);
  Tq rhon = rhon_.density(x, y, time, pn);

  T Swx = 0, Swy = 0, Swt = 0;
  qInterpreter_.eval_SwGradient( q, qx, qy, qt, Swx, Swy, Swt );

  fx(0) -=  rhow*nu*Swx;
  fx(1) -= -rhon*nu*Swx;

  fy(0) -=  rhow*nu*Swy;
  fy(1) -= -rhon*nu*Swy;

  ft(0) -=  rhow*nu*Swt;
  ft(1) -= -rhon*nu*Swt;
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDETwoPhase_ArtificialViscosity2D<PDETraitsSize, PDETraitsModel>::artViscMax(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    Tf& nu_max ) const
{
  Tp H = exp(param); //param is either MatrixSymS<D,Th> or MatrixSymS<D+1,Th> of log(H)

  ArrayQ<Tf> vx = 0, vy = 0;
  characteristicVelocitySinglePhase(x, y, time, q, qx, qy, vx, vy);

  Tf vw_h = velocity_h(H, vx[0], vy[0]);
  Tf vn_h = velocity_h(H, vx[1], vy[1]);

  const Real alpha = 40;
  Tf v_h = smoothmax(vw_h, vn_h, alpha);

  const Real Pe = 2.0; //required grid Peclet number

  nu_max = 0; //maximum artificial viscosity (when sensor is 1)

  if (order_ > 0) //don't need artificial viscosity for P0
  {
    nu_max = (v_h / order_) / Pe;
    SANS_ASSERT_MSG( nu_max > 0, "PDETwoPhase_ArtificialViscosity2D::artViscMax - Max artificial viscosity is not positive!");
  }
}

// viscous flux:
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDETwoPhase_ArtificialViscosity2D<PDETraitsSize, PDETraitsModel>::fluxViscous(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Tf>& fx, ArrayQ<Tf>& fy ) const
{
  SANS_ASSERT(!hasSpaceTimeDiffusion_); //space-time diffusion requires a space-time NDPDE

  artificialViscosityFlux(param, x, y, time, q, qx, qy, fx, fy);
  BaseType::fluxViscous( x, y, time, q, qx, qy, fx, fy );
}

// viscous flux: normal flux with left and right states
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDETwoPhase_ArtificialViscosity2D<PDETraitsSize, PDETraitsModel>::fluxViscous(
    const Tp& paramL, const Tp& paramR, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
    const Real& nx, const Real& ny, ArrayQ<Tf>& fn ) const
{
  SANS_ASSERT(!hasSpaceTimeDiffusion_); //space-time diffusion requires a space-time NDPDE

  ArrayQ<Tf> fxL = 0, fxR = 0;
  ArrayQ<Tf> fyL = 0, fyR = 0;

  artificialViscosityFlux(paramL, x, y, time, qL, qxL, qyL, fxL, fyL);
  artificialViscosityFlux(paramR, x, y, time, qR, qxR, qyR, fxR, fyR);

  //Compute the average artificial viscosity flux from both sides
  fn += 0.5*(fxL + fxR)*nx + 0.5*(fyL + fyR)*ny;

  //Add on any interface viscous fluxes from the base class
  BaseType::fluxViscous( x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, fn );
}

// viscous diffusion matrix: d(Fv)/d(Ux)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tk>
inline void
PDETwoPhase_ArtificialViscosity2D<PDETraitsSize, PDETraitsModel>::diffusionViscous(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const
{
  SANS_DEVELOPER_EXCEPTION("PDETwoPhase_ArtificialViscosity2D::diffusionViscous - Not implemented yet.");
#if 0
  artificialViscosity( param, x, y, time, q,
                       kxx, kxy, kyx, kyy );

  BaseType::diffusionViscous( x, y, time,
                              q, qx, qy,
                              kxx, kxy,
                              kyx, kyy );
#endif
}

// space-time viscous flux
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDETwoPhase_ArtificialViscosity2D<PDETraitsSize, PDETraitsModel>::fluxViscousSpaceTime(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
    ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& ft ) const
{
  if (hasSpaceTimeDiffusion_)
    artificialViscosityFluxSpaceTime(param, x, y, time, q, qx, qy, qt, fx, fy, ft);
  else
    artificialViscosityFlux(param, x, y, time, q, qx, qy, fx, fy);

  BaseType::fluxViscousSpaceTime( x, y, time, q, qx, qy,qt, fx, fy, ft );
}

// space-time viscous flux: normal flux with left and right states
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDETwoPhase_ArtificialViscosity2D<PDETraitsSize, PDETraitsModel>::fluxViscousSpaceTime(
    const Tp& paramL, const Tp& paramR, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qtL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qtR,
    const Real& nx, const Real& ny, const Real& nt, ArrayQ<Tf>& fn ) const
{
  ArrayQ<Tf> fxL = 0, fxR = 0;
  ArrayQ<Tf> fyL = 0, fyR = 0;
  ArrayQ<Tf> ftL = 0, ftR = 0;

  if (hasSpaceTimeDiffusion_)
  {
    artificialViscosityFluxSpaceTime(paramL, x, y, time, qL, qxL, qyL, qtL, fxL, fyL, ftL);
    artificialViscosityFluxSpaceTime(paramR, x, y, time, qR, qxR, qyR, qtR, fxR, fyR, ftR);
  }
  else
  {
    artificialViscosityFlux(paramL, x, y, time, qL, qxL, qyL, fxL, fyL);
    artificialViscosityFlux(paramR, x, y, time, qR, qxR, qyR, fxR, fyR);
  }

  //Compute the average artificial viscosity flux from both sides
  fn += 0.5*(fxL + fxR)*nx + 0.5*(fyL + fyR)*ny + 0.5*(ftL + ftR)*nt;

  //Add on any interface viscous fluxes from the base class
  BaseType::fluxViscousSpaceTime( x, y, time, qL, qxL, qyL, qtL, qR, qxR, qyR, qtR, nx, ny, nt, fn );
}

// space-time viscous diffusion matrix: d(Fv)/d(Ux)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tk>
inline void
PDETwoPhase_ArtificialViscosity2D<PDETraitsSize, PDETraitsModel>::diffusionViscousSpaceTime(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
    MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxt,
    MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyt,
    MatrixQ<Tk>& ktx, MatrixQ<Tk>& kty, MatrixQ<Tk>& ktt ) const
{
  SANS_DEVELOPER_EXCEPTION("PDETwoPhase_ArtificialViscosity2D::diffusionViscousSpaceTime - Not implemented yet.");
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template<class Tq, class Tg, class Tv>
inline void
PDETwoPhase_ArtificialViscosity2D<PDETraitsSize, PDETraitsModel>::
characteristicVelocity( const Real& x, const Real& y, const Real& time,
                        const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, Tv& vx, Tv& vy ) const
{
  typedef typename promote_Surreal<Tq,Tg>::type T;

  Tq Sw = 0, Sn = 0, pw = 0, pn = 0;
  T pwx = 0, pwy = 0;
  T pnx = 0, pny = 0;

  qInterpreter_.eval( q, pw, pn, Sw, Sn );

  qInterpreter_.eval_pwGradient( q, qx, qy, pwx, pwy );
  qInterpreter_.eval_pnGradient( q, qx, qy, pnx, pny );

  // Add gravity to y-direction
  if (gravity_on_)
  {
    //Evaluate densities
    Tq rhow = rhow_.density(x, y, time, pw);
    Tq rhon = rhon_.density(x, y, time, pn);

    pwy += rhow*gravity_const;
    pny += rhon*gravity_const;
  }

  //Evaluate rock permeability matrix
  Real Kxx, Kxy, Kyx, Kyy;
  K_.permeability(x, y, time, Kxx, Kxy, Kyx, Kyy);

  T Kgradpwx = (Kxx*pwx + Kxy*pwy);
  T Kgradpwy = (Kyx*pwx + Kyy*pwy);

  T Kgradpnx = (Kxx*pnx + Kxy*pny);
  T Kgradpny = (Kyx*pnx + Kyy*pny);

  //Evaluate relative permeabilities
  Tq krw = krw_.relativeperm(Sw);
  Tq krn = krn_.relativeperm(Sn);

  //Evaluate phase viscosities
  Real muw = muw_.viscosity(x, y, time);
  Real mun = mun_.viscosity(x, y, time);

  //Evaluate phase mobilities
  Tq lambda_w = krw/muw;
  Tq lambda_n = krn/mun;

  //Evaluate relative permeability jacobians
  Tq krw_Sw, krn_Sn;
  krw_.relativepermJacobian(Sw, krw_Sw);
  krn_.relativepermJacobian(Sn, krn_Sn);

  Real dSn_dSw = -1;
  Tq krn_Sw = krn_Sn * dSn_dSw;

  //Evaluate phase mobility jacobians
  Tq lambda_w_Sw = krw_Sw/muw;
  Tq lambda_n_Sw = krn_Sw/mun;

  //Velocity components - without phi factor in denominator (since that gets canceled out)
  vx = -(lambda_w_Sw*lambda_n*Kgradpwx - lambda_w*lambda_n_Sw*Kgradpnx) / (lambda_w + lambda_n);
  vy = -(lambda_w_Sw*lambda_n*Kgradpwy - lambda_w*lambda_n_Sw*Kgradpny) / (lambda_w + lambda_n);
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template<class Tq, class Tg, class Tv>
inline void
PDETwoPhase_ArtificialViscosity2D<PDETraitsSize, PDETraitsModel>::
characteristicVelocitySinglePhase( const Real& x, const Real& y, const Real& time,
                                   const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
                                   ArrayQ<Tv>& vx, ArrayQ<Tv>& vy ) const
{
  typedef typename promote_Surreal<Tq,Tg>::type T;

  T pwx = 0, pwy = 0;
  T pnx = 0, pny = 0;
  qInterpreter_.eval_pwGradient( q, qx, qy, pwx, pwy );
  qInterpreter_.eval_pnGradient( q, qx, qy, pnx, pny );

#if 0
  // Add gravity to y-direction
  if (gravity_on_)
  {
    Tq Sw = 0, Sn = 0, pw = 0, pn = 0;
    qInterpreter_.eval( q, pw, pn, Sw, Sn );

    //Evaluate densities
    Tq rhow = rhow_.density(x, y, time, pw);
    Tq rhon = rhon_.density(x, y, time, pn);

    pwy += rhow*gravity_const;
    pny += rhon*gravity_const;
  }
#endif

  //Evaluate rock permeability matrix
  Real Kxx, Kxy, Kyx, Kyy;
  K_.permeability(x, y, time, Kxx, Kxy, Kyx, Kyy);

  //Evaluate phase viscosities
  Real muw = muw_.viscosity(x, y, time);
  Real mun = mun_.viscosity(x, y, time);

  //Evaluate phase velocities (without mobility weighting)
  //Ignoring phi factor in denominator since it gets canceled out with the phi factor in the viscosity
  vx[0] = -(Kxx*pwx + Kxy*pwy) / muw;
  vx[1] = -(Kxx*pnx + Kxy*pny) / mun;

  vy[0] = -(Kyx*pwx + Kyy*pwy) / muw;
  vy[1] = -(Kyx*pnx + Kyy*pny) / mun;
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
inline std::vector<std::string>
PDETwoPhase_ArtificialViscosity2D<PDETraitsSize, PDETraitsModel>::
derivedQuantityNames() const
{
  std::vector<std::string> names = {"charvelocityX_wet",
                                    "charvelocityY_wet",
                                    "charvelocityX_nonwet",
                                    "charvelocityY_nonwet",
                                    "nu_max",
                                    "hxx",
                                    "hyy"
                                   };

  //Append any names from the base class
  std::vector<std::string> basenames = BaseType::derivedQuantityNames();
  names.insert(names.end(), basenames.begin(), basenames.end());

  return names;
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template<class Tp, class Tq, class Tg, class Tf>
inline void
PDETwoPhase_ArtificialViscosity2D<PDETraitsSize, PDETraitsModel>::
derivedQuantity(const int& index, const Tp& param, const Real& x, const Real& y, const Real& time,
                const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, Tf& J ) const
{
  int nQtyCurrent = derivedQuantityNames().size() - BaseType::derivedQuantityNames().size();
  if (index >= nQtyCurrent)
  {
    BaseType::derivedQuantity(index - nQtyCurrent, x, y, time, q, qx, qy, J);
    return;
  }

  switch (index)
  {
  case 0:
  {
    ArrayQ<Tf> vx = 0, vy = 0;
    characteristicVelocitySinglePhase(x, y, time, q, qx, qy, vx, vy);
    J = vx[0];
    break;
  }

  case 1:
  {
    ArrayQ<Tf> vx = 0, vy = 0;
    characteristicVelocitySinglePhase(x, y, time, q, qx, qy, vx, vy);
    J = vy[0];
    break;
  }

  case 2:
  {
    ArrayQ<Tf> vx = 0, vy = 0;
    characteristicVelocitySinglePhase(x, y, time, q, qx, qy, vx, vy);
    J = vx[1];
    break;
  }

  case 3:
  {
    ArrayQ<Tf> vx = 0, vy = 0;
    characteristicVelocitySinglePhase(x, y, time, q, qx, qy, vx, vy);
    J = vy[1];
    break;
  }

  case 4:
  {
    artViscMax(param, x, y, time, q, qx, qy, J);
    break;
  }

  case 5:
  {
    Tp H = exp(param); //param is either MatrixSymS<D,Th> or MatrixSymS<D+1,Th> of log(H)
    J = H(0,0);
    break;
  }

  case 6:
  {
    Tp H = exp(param); //param is either MatrixSymS<D,Th> or MatrixSymS<D+1,Th> of log(H)
    J = H(1,1);
    break;
  }

  default:
    SANS_DEVELOPER_EXCEPTION("PDETwoPhase_ArtificialViscosity2D::derivedQuantity - Invalid index!");
  }
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
inline void
PDETwoPhase_ArtificialViscosity2D<PDETraitsSize, PDETraitsModel>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  std::string indent2(indentSize+2, ' ');

  out << indent << "PDETwoPhase_ArtificialViscosity2D:" << std::endl;
  out << indent2 << "order_ = " << order_ << std::endl;
  out << indent2 << "pde_ = " << std::endl;
  BaseType::dump(indentSize+4, out);
}

} //namespace SANS

#endif  // PDETWOPHASE_ARTIFICIALVISCOSITY2D_H
