// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "TwoPhaseVariable.h"

#define PYDICT_INSTANTIATE
#include "Python/PyDict_impl.h"

namespace SANS
{

template ParameterOption<TwoPhaseVariableTypeParams::TwoPhaseVariableOptions>::ExtractType
PyDict::get(const ParameterType<ParameterOption<TwoPhaseVariableTypeParams::TwoPhaseVariableOptions>>&) const;

void PressureNonWet_SaturationWet_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.pn));
  allParams.push_back(d.checkInputs(params.Sw));
  d.checkUnknownInputs(allParams);
}
PressureNonWet_SaturationWet_Params PressureNonWet_SaturationWet_Params::params;

void TwoPhaseVariableTypeParams::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.StateVector));
  d.checkUnknownInputs(allParams);
}
TwoPhaseVariableTypeParams TwoPhaseVariableTypeParams::params;

}
