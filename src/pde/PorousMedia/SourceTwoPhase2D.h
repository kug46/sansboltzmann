// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOURCETWOPHASE2D_H
#define SOURCETWOPHASE2D_H

// 2-D two-phase PDE: source terms

#include <cmath>              // sqrt
#include <iostream>
#include <limits>

//Python must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "tools/SANSnumerics.h"     // Real

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"

#include "TwoPhaseVariable.h"

namespace SANS
{

// forward declare: solution variables interpreter
template <class QType, class CapillaryModel, template <class> class PDETraitsSize>
class Q2D;

struct SourceTwoPhase2DType_FixedPressureInflow_Param : noncopyable
{
  const ParameterNumeric<Real> pB{"pB", NO_DEFAULT, 0, NO_LIMIT, "Fixed bottom hole pressure"};
  const ParameterNumeric<Real> Sw{"Sw", NO_DEFAULT, 0.0, 1.0, "Inflow wetting phase saturation"};
  const ParameterNumeric<Real> Rwellbore{"Rwellbore", NO_DEFAULT, 0, NO_LIMIT, "Well-bore radius"};
  const ParameterNumeric<int>  nParam{"nParam", NO_DEFAULT, 1, 7, "Number of parameters in distributed well model"};
  const ParameterNumeric<Real> s0{"s0", 0.0, 0.0, 1.0, "Starting s for activation function"};

  struct WellModelOptions
  {
    typedef std::string ExtractType;
    const std::string Gaussian = "Gaussian";
    const std::string Polynomial = "Polynomial";
    const std::string PolynomialFlat = "PolynomialFlat";

    const std::vector<std::string> options{Gaussian, Polynomial, PolynomialFlat};
  };
  const ParameterOption<WellModelOptions> WellModel{"WellModel", "Polynomial", "Well model type"};

  static void checkInputs(PyDict d);
  static SourceTwoPhase2DType_FixedPressureInflow_Param params;
};

struct SourceTwoPhase2DType_FixedPressureOutflow_Param : noncopyable
{
  const ParameterNumeric<Real> pB{"pB", NO_DEFAULT, 0, NO_LIMIT, "Fixed bottom hole pressure"};
  const ParameterNumeric<Real> Rwellbore{"Rwellbore", NO_DEFAULT, 0, NO_LIMIT, "Well-bore radius"};
  const ParameterNumeric<int>  nParam{"nParam", NO_DEFAULT, 1, 7, "Number of parameters in distributed well model"};
  const ParameterNumeric<Real> s0{"s0", 0.0, 0.0, 1.0, "Starting s for activation function"};

  struct WellModelOptions
  {
    typedef std::string ExtractType;
    const std::string Gaussian = "Gaussian";
    const std::string Polynomial = "Polynomial";
    const std::string PolynomialFlat = "PolynomialFlat";

    const std::vector<std::string> options{Gaussian, Polynomial, PolynomialFlat};
  };
  const ParameterOption<WellModelOptions> WellModel{"WellModel", "Polynomial", "Well model type"};

  static void checkInputs(PyDict d);
  static SourceTwoPhase2DType_FixedPressureOutflow_Param params;
};

struct SourceTwoPhase2DType_FixedInflowRate_Param : noncopyable
{
  const ParameterNumeric<Real> Q{"Q", NO_DEFAULT, 0.0, NO_LIMIT, "Fixed volumetric inflow rate (per unit length)"};
  const ParameterNumeric<Real> Sw{"Sw", NO_DEFAULT, 0.0, 1.0, "Inflow wetting phase saturation" };
  const ParameterNumeric<int>  nParam{"nParam", NO_DEFAULT, 1, 7, "Number of parameters in distributed well model"};

  static void checkInputs(PyDict d);
  static SourceTwoPhase2DType_FixedInflowRate_Param params;
};

struct SourceTwoPhase2DType_FixedOutflowRate_Param : noncopyable
{
  const ParameterNumeric<Real> Q{"Q", NO_DEFAULT, 0.0, NO_LIMIT, "Fixed volumetric rate (per unit length)"};
  const ParameterNumeric<int>  nParam{"nParam", NO_DEFAULT, 1, 7, "Number of parameters in distributed well model"};

  static void checkInputs(PyDict d);
  static SourceTwoPhase2DType_FixedOutflowRate_Param params;
};


struct SourceTwoPhase2DParam : noncopyable
{
  struct SourceOptions
  {
    typedef DictKeyPair ExtractType;
    const ParameterString SourceType = ParameterString("SourceType", NO_DEFAULT, "Type of source" );
    const ParameterString& key = SourceType;

    const DictOption FixedPressureInflow {"FixedPressureInflow", SourceTwoPhase2DType_FixedPressureInflow_Param::checkInputs};
    const DictOption FixedPressureOutflow {"FixedPressureOutflow", SourceTwoPhase2DType_FixedPressureOutflow_Param::checkInputs};
    const DictOption FixedInflowRate {"FixedInflowRate", SourceTwoPhase2DType_FixedInflowRate_Param::checkInputs};
    const DictOption FixedOutflowRate {"FixedOutflowRate", SourceTwoPhase2DType_FixedOutflowRate_Param::checkInputs};

    const std::vector<DictOption> options{FixedPressureInflow, FixedPressureOutflow, FixedInflowRate, FixedOutflowRate};
  };
  const ParameterOption<SourceOptions> Source{"Source", NO_DEFAULT, "Dictionary with source parameters"};

//  struct GeometryOptions
//  {
//    typedef DictKeyPair ExtractType;
//    const ParameterString Shape = ParameterString("Shape", NO_DEFAULT, "Shape of source" );
//    const ParameterString& key = Shape;
//
//    const DictOption Round {"Round", SourceTwoPhase1DType_FixedWellPressure_Param::checkInputs};
//    const DictOption Rectangular {"Rectangular", SourceTwoPhase1DType_FixedWellPressure_Param::checkInputs};
//
//    const std::vector<DictOption> options{Round, Rectangular};
//  };
//  const ParameterOption<GeometryOptions> Geometry{"Geometry", NO_DEFAULT, "Dictionary with geometry parameters"};

  const ParameterNumeric<Real> xcentroid{"xcentroid", NO_DEFAULT, NO_RANGE, "x-centroid of the active source region"};
  const ParameterNumeric<Real> ycentroid{"ycentroid", NO_DEFAULT, NO_RANGE, "y-centroid of the active source region"};
  const ParameterNumeric<Real> R{"R", NO_DEFAULT, 0.0, NO_LIMIT, "radius of the active source region"};
  const ParameterNumeric<Real> Tmin{"Tmin", NO_DEFAULT, NO_RANGE, "Starting time of the source term"};
  const ParameterNumeric<Real> Tmax{"Tmax", NO_DEFAULT, NO_RANGE, "Shut-off time of the source term"};
  const ParameterNumeric<Real> smoothLr{"smoothLr", 0.0, 0.0, NO_LIMIT, "Length scale for smoothing in radial direction"};
  const ParameterNumeric<Real> smoothT{"smoothT", 0.0, 0.0, NO_LIMIT, "Time scale for smoothing in temporal direction"};
  const ParameterNumeric<Real> visibleAngle{"visibleAngle", NO_DEFAULT, 0.0, 2*PI,
                                           "Angle subtended by the circular well inside the domain (in radians)"
                                           };

  static void checkInputs(PyDict d);
  static SourceTwoPhase2DParam params;
};

struct SourceTwoPhase2DListParam : noncopyable
{
  static void checkInputs(PyDict d);
  static SourceTwoPhase2DListParam params;
};

//Polynomial activation functions for distributed well models
std::vector<Real> wellActivationFunctionPolyCoeff(const int& order);
std::vector<Real> wellActivationFunctionPolyFlatCoeff(const int& order, const Real& s0);
void wellActivationFunctionPolynomial(const std::vector<Real>& coeff, const Real& s, const Real& s0, Real& f, Real& df, Real& d2f);


//----------------------------------------------------------------------------//
class SourceTwoPhase2DBase
{
public:

  typedef SourceTwoPhase2DParam ParamsType;

  explicit SourceTwoPhase2DBase(const PyDict& d)
  :
    xc_(d.get(ParamsType::params.xcentroid)), yc_(d.get(ParamsType::params.ycentroid)), R_(d.get(ParamsType::params.R)),
    Tmin_(d.get(ParamsType::params.Tmin)), Tmax_(d.get(ParamsType::params.Tmax)),
    smoothLr_(d.get(ParamsType::params.smoothLr)), smoothT_(d.get(ParamsType::params.smoothT)),
    visibleFrac_(d.get(ParamsType::params.visibleAngle)/(2.0*PI))
  {
     SANS_ASSERT(Tmax_ > Tmin_);
     SANS_ASSERT(smoothLr_ >= 0.0);
     SANS_ASSERT(smoothT_ >= 0.0);
  }

  Real getRadialDistance(const Real& x, const Real& y, const Real xscale = 1.0, const Real yscale = 1.0) const
  {
    Real dx = xscale*(x - xc_);
    Real dy = yscale*(y - yc_);
    return sqrt(dx*dx + dy*dy);
  }

  Real getWeight(const Real& x, const Real& y, const Real& t, const Real xscale = 1.0, const Real yscale = 1.0) const
  {
    Real r = getRadialDistance(x, y, xscale, yscale);

    Real wr = computeSmoothingWeight(r, 0.0, R_, 0.0, smoothLr_);
    Real wt = computeSmoothingWeight(t, Tmin_, Tmax_, smoothT_, smoothT_ );
    return wr*wt;
  }

#if 0
  void getNormal(const Real& x, const Real& y, Real& nx, Real& ny) const
  {
    Real dx = x - xc_;
    Real dy = y - yc_;
    Real r = sqrt(dx*dx + dy*dy);

    if (r > 100*std::numeric_limits<Real>::epsilon())
    {
      nx = dx/r;
      ny = dy/r;
    }
    else
    {
      nx = 1.0;
      ny = 0.0;
    }
  }
#endif

  ~SourceTwoPhase2DBase() {}

  SourceTwoPhase2DBase& operator=( const SourceTwoPhase2DBase& ) = delete;

protected:

  Real computeSmoothingWeight(const Real& x, const Real& xmin, const Real& xmax,
                              const Real& smoothL0, const Real& smoothL1) const;

  Real xc_;  //Active region of the source term
  Real yc_;
  Real R_;
  Real Tmin_;   //Active time-period of the source term
  Real Tmax_;
  Real smoothLr_; //radial smoothing length scale (use zero for no smoothing)
  Real smoothT_;  //temporal smoothing time scale (use zero for no smoothing)
  Real visibleFrac_; //fraction of the well that is visible inside the domain (i.e. actually integrated)
};

inline Real
SourceTwoPhase2DBase::computeSmoothingWeight(const Real& x, const Real& xmin, const Real& xmax,
                                             const Real& smoothL0, const Real& smoothL1) const
{
  if (x >= (xmin - 0.5*smoothL0) && x <= (xmax + 0.5*smoothL1))
  {
    if (smoothL0 < 100*std::numeric_limits<Real>::epsilon() && smoothL1 < 100*std::numeric_limits<Real>::epsilon()) //No smoothing
    {
      return 1.0;
    }
    else //Cubic smoothing function (y = 3x^2 - 2x^3)
    {
      //Smoothing factor for left side of source
      Real x0 = xmin - 0.5*smoothL0;
      Real wL = 0.0, wR = 0.0;

      if (x > x0 && x < (x0+smoothL0))
      {
        Real xf = (x-x0)/smoothL0;
        wL = 3.0*(xf*xf) - 2.0*(xf*xf*xf);
      }
      else if (x >= (x0+smoothL0))
      {
        wL = 1.0;
      }

      //Smoothing factor for right side of source
      x0 = xmax - 0.5*smoothL1;

      if (x <= x0)
      {
        wR = 1.0;
      }
      else if (x > x0 && x < (x0+smoothL1))
      {
        Real xf = (x-x0)/smoothL1;
        wR = 1.0 - ( 3.0*(xf*xf) - 2.0*(xf*xf*xf) );
      }

      return wL*wR;
    }
  }

  return 0.0;
}


//----------------------------------------------------------------------------//
template <template <class> class PDETraitsSize, class PDETraitsModel>
class SourceTwoPhase2D_FixedPressureInflow : public SourceTwoPhase2DBase
{
public:

  //Extract model traits
  typedef typename PDETraitsModel::QType QType;
  typedef typename PDETraitsModel::DensityModelw DensityModelw;
  typedef typename PDETraitsModel::DensityModeln DensityModeln;
  typedef typename PDETraitsModel::PorosityModel PorosityModel;
  typedef typename PDETraitsModel::RelPermModelw RelPermModelw;
  typedef typename PDETraitsModel::RelPermModeln RelPermModeln;
  typedef typename PDETraitsModel::ViscModelw ViscModelw;
  typedef typename PDETraitsModel::ViscModeln ViscModeln;
  typedef typename PDETraitsModel::RockPermModel RockPermModel;
  typedef typename PDETraitsModel::CapillaryModel CapillaryModel;
  typedef Q2D<QType, CapillaryModel, PDETraitsSize> QInterpreter;  // solution variable interpreter type

  template <class T>
  using ArrayQ = typename QInterpreter::template ArrayQ<T>;   // solution/residual arrays

  typedef SourceTwoPhase2DBase BaseType;
  typedef SourceTwoPhase2DParam BaseParamsType;
  typedef SourceTwoPhase2DType_FixedPressureInflow_Param ParamsType;

  explicit SourceTwoPhase2D_FixedPressureInflow(
      const PyDict& d, const DensityModelw& rhow, const DensityModeln& rhon,
      const RelPermModelw& krw, const RelPermModeln& krn, const ViscModelw& muw, const ViscModeln& mun,
      const RockPermModel& K, const CapillaryModel& pc)
  : BaseType(d),
    pB_(d.get(BaseParamsType::params.Source).get(ParamsType::params.pB)),
    Sw_in_(d.get(BaseParamsType::params.Source).get(ParamsType::params.Sw)),
    Rwb_(d.get(BaseParamsType::params.Source).get(ParamsType::params.Rwellbore)),
    nParam_(d.get(BaseParamsType::params.Source).get(ParamsType::params.nParam)),
    s0_(d.get(BaseParamsType::params.Source).get(ParamsType::params.s0)),
    wellModel_(d.get(BaseParamsType::params.Source).get(ParamsType::params.WellModel)),
    rhow_(rhow), rhon_(rhon), krw_(krw), krn_(krn), muw_(muw), mun_(mun),
    K_(K), pc_(pc), qInterpreter_(pc_)
  {
    if (wellModel_ == ParamsType::params.WellModel.Gaussian)
    {
      SANS_ASSERT(s0_ == 0.0);

      //Set the Gaussian model parameters
      avec_.resize(nParam_);

      if (nParam_ == 1)
      {
        avec_[0] = 1.0;
      }
      else if (nParam_ == 3)
      {
        avec_[0] =  1.6805486353473567929893462186086;
        avec_[1] = -0.61235120144821138025328586331859;
        avec_[2] =  0.23570005440147727558578930602427;
      }
      else
        SANS_DEVELOPER_EXCEPTION("SourceTwoPhase2D_FixedPressureInflow - Invalid parameter count for Gaussian model.");
    }
    else if (wellModel_ == ParamsType::params.WellModel.Polynomial)
    {
      SANS_ASSERT(s0_ == 0.0);
      avec_ = wellActivationFunctionPolyCoeff(nParam_);
    }
    else if (wellModel_ == ParamsType::params.WellModel.PolynomialFlat)
    {
      avec_ = wellActivationFunctionPolyFlatCoeff(nParam_, s0_);
    }
  }

  Real getWellPressure() { return pB_; }

  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& xL, const Real& yL,
      const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const {}

  template <class Tq, class Tg, class Ts>
  void source(const Real& x, const Real& y, const Real& time,
              const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
              ArrayQ<Ts>& source ) const
  {
    //Evaluate rock permeability matrix
    Real Kxx, Kxy, Kyx, Kyy;
    K_.permeability(x, y, time, Kxx, Kxy, Kyx, Kyy);

    SANS_ASSERT_MSG( fabs(Kxy) + fabs(Kyx) <= std::numeric_limits<Real>::epsilon(),
                     "SourceTwoPhase2D_FixedPressureInflow::source - K matrix needs to be diagonal." );

    Real detK = Kxx*Kyy - Kxy*Kyx;
    SANS_ASSERT_MSG( detK > 0, "SourceTwoPhase2D_FixedPressureInflow::source - K matrix needs to be positive definite.");

    //Coordinate transform parameters
    Real xscale = pow(Kyy/Kxx, 0.25);
    Real yscale = pow(Kxx/Kyy, 0.25);
    Real savg = 0.5*(xscale + yscale);
    xscale /= savg;
    yscale /= savg;
    Real volscale = xscale*yscale;

    Real r = getRadialDistance(x, y, xscale, yscale);

    Real weight = getWeight(x, y, time, xscale, yscale);
    if (weight < 100*std::numeric_limits<Real>::epsilon()) return; //The weighting factor is too small, so not worth computing source term

    Tq Sw = 0, Sn = 0, pw = 0, pn = 0;

    //Evaluate pn using reservoir state
    qInterpreter_.eval_pn( q, pn );

    //Create a new state using specified inflow saturation
    ArrayQ<Tq> q_in;
    qInterpreter_.setFromPrimitive( q_in, PressureNonWet_SaturationWet<Tq>(pn, Sw_in_) );

    //Evaluate using the modified state
    qInterpreter_.eval( q_in, pw, pn, Sw, Sn );

    //Evaluate densities
    Tq rhow = rhow_.density(x, y, time, pw);
    Tq rhon = rhon_.density(x, y, time, pn);

    //Evaluate relative permeabilities
    Tq krw = krw_.relativeperm(Sw);
    Tq krn = krn_.relativeperm(Sn);

    //Evaluate phase viscosities
    Tq muw = muw_.viscosity(x, y, time);
    Tq mun = mun_.viscosity(x, y, time);

    //Evaluate phase mobilities
    Tq lambda_w = krw/muw;
    Tq lambda_n = krn/mun;

    Tq dpndr2 = 0.0;

    if (wellModel_ == ParamsType::params.WellModel.Gaussian)
    {
      Real sum_ai = 0.0;
      Real sum_2i_ai = 0.0;
      Real sum_ai_r_R_power = 0.0;
      Real sum_4i2_ai_r_R_power = 0.0;
      Real sum_2i_ai_r_R_power = 0.0;

      for (int i = 1; i <= nParam_; i++)
      {
        sum_ai += avec_[i-1];
        sum_2i_ai += 2*i*avec_[i-1];
        sum_ai_r_R_power += avec_[i-1] * pow(r/R_, 2.0*i);
        sum_4i2_ai_r_R_power += 4*i*i * avec_[i-1] / pow(R_, 2.0*i) * pow(r, 2.0*i - 2.0);
        sum_2i_ai_r_R_power += 2*i * avec_[i-1] / pow(R_, 2.0*i) * pow(r, 2.0*i - 1.0);
      }

      Real expterm = exp(-sum_ai_r_R_power);
      dpndr2 = (pn - pB_) / ( exp(-sum_ai) * (1.0 + sum_2i_ai*log(R_/Rwb_) ) - expterm )
               * expterm * (sum_4i2_ai_r_R_power - (sum_2i_ai_r_R_power*sum_2i_ai_r_R_power));
    }
    else if (wellModel_ == ParamsType::params.WellModel.Polynomial)
    {
      Real s = r / R_;

      Real f, df, d2f;
      wellActivationFunctionPolynomial(avec_, s, 0.0, f, df, d2f);

      Real df_s;
      if (s == 0.0) //avoid division by zero
      {
        // f'(s) / s = a1/s + 2*a2 + 3*a3*s + ...
        SANS_ASSERT(avec_[1] == 0.0);
        df_s = 2.0*avec_[2];
      }
      else
        df_s = df/s;

      dpndr2 = (pn - pB_) / ((f + log(R_/Rwb_))*R_*R_) * (df_s + d2f);
    }
    else if (wellModel_ == ParamsType::params.WellModel.PolynomialFlat)
    {
      Real s = r / R_;

      Real f, df, d2f;
      Real f1, df1, d2f1;
      wellActivationFunctionPolynomial(avec_, s, s0_, f, df, d2f);
      wellActivationFunctionPolynomial(avec_, 1.0, s0_, f1, df1, d2f1); //evaluated at s = 1

      Real df_s;
      if (s == 0.0) //avoid division by zero
      {
        if (s0_ == 0.0)
        {
          // f'(s) / s = a1/s + 2*a2 + 3*a3*s + ...
          SANS_ASSERT(avec_[1] == 0.0);
          df_s = 2.0*avec_[2];
        }
        else
        {
          df_s = 0.0;
        }
      }
      else
        df_s = df/s;

      dpndr2 = (pn - pB_) / ((f - 1.0 + df1*log(R_/Rwb_))*R_*R_) * (df_s + d2f);
    }

    Tq tmp = weight * volscale * sqrt(Kxx*Kyy) * dpndr2;

    //Note that the source term is on LHS
    source[0] += rhow*lambda_w*tmp; //Mass flow rate of phase 1
    source[1] += rhon*lambda_n*tmp; //Mass flow rate of phase 2
  }

  ~SourceTwoPhase2D_FixedPressureInflow() {}

protected:
  const Real pB_; //Fixed Bottom hole pressure
  const Real Sw_in_; //Specified inflow wetting phase saturation
  const Real Rwb_; //Well-bore radius
  const int nParam_; //number of well model parameters
  const Real s0_;
  const std::string wellModel_;
  std::vector<Real> avec_; //well model parameters

  const DensityModelw& rhow_; //Density model for wetting phase
  const DensityModeln& rhon_; //Density model for non-wetting phase

  const RelPermModelw& krw_; //Relative permeability model for wetting phase
  const RelPermModeln& krn_; //Relative permeability model for non-wetting phase

  const ViscModelw& muw_; //Viscosity model for wetting phase
  const ViscModeln& mun_; //Viscosity model for non-wetting phase

  const RockPermModel& K_; // Rock permeability model
  const CapillaryModel& pc_; //Capillary pressure model

  const QInterpreter qInterpreter_; // solution variable interpreter class
};


//----------------------------------------------------------------------------//
template <template <class> class PDETraitsSize, class PDETraitsModel>
class SourceTwoPhase2D_FixedPressureOutflow : public SourceTwoPhase2DBase
{
public:

  //Extract model traits
  typedef typename PDETraitsModel::QType QType;
  typedef typename PDETraitsModel::DensityModelw DensityModelw;
  typedef typename PDETraitsModel::DensityModeln DensityModeln;
  typedef typename PDETraitsModel::PorosityModel PorosityModel;
  typedef typename PDETraitsModel::RelPermModelw RelPermModelw;
  typedef typename PDETraitsModel::RelPermModeln RelPermModeln;
  typedef typename PDETraitsModel::ViscModelw ViscModelw;
  typedef typename PDETraitsModel::ViscModeln ViscModeln;
  typedef typename PDETraitsModel::RockPermModel RockPermModel;
  typedef typename PDETraitsModel::CapillaryModel CapillaryModel;
  typedef Q2D<QType, CapillaryModel, PDETraitsSize> QInterpreter;  // solution variable interpreter type

  template <class T>
  using ArrayQ = typename QInterpreter::template ArrayQ<T>;    // solution/residual arrays

  typedef SourceTwoPhase2DBase BaseType;
  typedef SourceTwoPhase2DParam BaseParamsType;
  typedef SourceTwoPhase2DType_FixedPressureOutflow_Param ParamsType;

  explicit SourceTwoPhase2D_FixedPressureOutflow(
      const PyDict& d, const DensityModelw& rhow, const DensityModeln& rhon,
      const RelPermModelw& krw, const RelPermModeln& krn, const ViscModelw& muw, const ViscModeln& mun,
      const RockPermModel& K, const CapillaryModel& pc, const bool computeMassFlow = true)
  : BaseType(d),
    pB_(d.get(BaseParamsType::params.Source).get(ParamsType::params.pB)),
    Rwb_(d.get(BaseParamsType::params.Source).get(ParamsType::params.Rwellbore)),
    nParam_(d.get(BaseParamsType::params.Source).get(ParamsType::params.nParam)),
    s0_(d.get(BaseParamsType::params.Source).get(ParamsType::params.s0)),
    wellModel_(d.get(BaseParamsType::params.Source).get(ParamsType::params.WellModel)),
    rhow_(rhow), rhon_(rhon), krw_(krw), krn_(krn), muw_(muw), mun_(mun),
    K_(K), pc_(pc), qInterpreter_(pc_), computeMassFlow_(computeMassFlow)
  {
    if (wellModel_ == ParamsType::params.WellModel.Gaussian)
    {
      SANS_ASSERT(s0_ == 0.0);

      //Set the Gaussian model parameters
      avec_.resize(nParam_);

      if (nParam_ == 1)
      {
        avec_[0] = 1.0;
      }
      else if (nParam_ == 3)
      {
        avec_[0] =  1.6805486353473567929893462186086;
        avec_[1] = -0.61235120144821138025328586331859;
        avec_[2] =  0.23570005440147727558578930602427;
      }
      else
        SANS_DEVELOPER_EXCEPTION("SourceTwoPhase2D_FixedPressureInflow - Invalid parameter count for Gaussian model.");
    }
    else if (wellModel_ == ParamsType::params.WellModel.Polynomial)
    {
      SANS_ASSERT(s0_ == 0.0);
      avec_ = wellActivationFunctionPolyCoeff(nParam_);
    }
    else if (wellModel_ == ParamsType::params.WellModel.PolynomialFlat)
    {
      avec_ = wellActivationFunctionPolyFlatCoeff(nParam_, s0_);
    }
  }

  Real getWellPressure() { return pB_; }

  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& xL, const Real& yL,
      const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const {}

  template <class Tq, class Tg, class Ts>
  void source(const Real& x, const Real& y, const Real& time,
              const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
              ArrayQ<Ts>& source ) const
  {
    //Evaluate rock permeability matrix
    Real Kxx, Kxy, Kyx, Kyy;
    K_.permeability(x, y, time, Kxx, Kxy, Kyx, Kyy);

    SANS_ASSERT_MSG( fabs(Kxy) + fabs(Kyx) <= std::numeric_limits<Real>::epsilon(),
                     "SourceTwoPhase2D_FixedPressureOutflow::source - K matrix needs to be diagonal." );

    Real detK = Kxx*Kyy - Kxy*Kyx;
    SANS_ASSERT_MSG( detK > 0, "SourceTwoPhase2D_FixedPressureOutflow::source - K matrix needs to be positive definite.");

    //Coordinate transform parameters
    Real xscale = pow(Kyy/Kxx, 0.25);
    Real yscale = pow(Kxx/Kyy, 0.25);
    Real savg = 0.5*(xscale + yscale);
    xscale /= savg;
    yscale /= savg;
    Real volscale = xscale*yscale;

    Real r = getRadialDistance(x, y, xscale, yscale);

    Real weight = getWeight(x, y, time, xscale, yscale);
    if (weight < 100*std::numeric_limits<Real>::epsilon()) return; //The weighting factor is too small, so not worth computing source term

    Tq Sw = 0, Sn = 0, pw = 0, pn = 0;
    qInterpreter_.eval( q, pw, pn, Sw, Sn );

    //Evaluate relative permeabilities
    Tq krw = krw_.relativeperm(Sw);
    Tq krn = krn_.relativeperm(Sn);

    //Evaluate phase viscosities
    Tq muw = muw_.viscosity(x, y, time);
    Tq mun = mun_.viscosity(x, y, time);

    //Evaluate phase mobilities
    Tq lambda_w = krw/muw;
    Tq lambda_n = krn/mun;

    Tq dpndr2 = 0.0;

    if (wellModel_ == ParamsType::params.WellModel.Gaussian)
    {
      Real sum_ai = 0.0;
      Real sum_2i_ai = 0.0;
      Real sum_ai_r_R_power = 0.0;
      Real sum_4i2_ai_r_R_power = 0.0;
      Real sum_2i_ai_r_R_power = 0.0;

      for (int i = 1; i <= nParam_; i++)
      {
        sum_ai += avec_[i-1];
        sum_2i_ai += 2*i*avec_[i-1];
        sum_ai_r_R_power += avec_[i-1] * pow(r/R_, 2.0*i);
        sum_4i2_ai_r_R_power += 4*i*i * avec_[i-1] / pow(R_, 2.0*i) * pow(r, 2.0*i - 2.0);
        sum_2i_ai_r_R_power += 2*i * avec_[i-1] / pow(R_, 2.0*i) * pow(r, 2.0*i - 1.0);
      }

      Real expterm = exp(-sum_ai_r_R_power);
      dpndr2 = (pn - pB_) / ( exp(-sum_ai) * (1.0 + sum_2i_ai*log(R_/Rwb_) ) - expterm )
               * expterm * (sum_4i2_ai_r_R_power - (sum_2i_ai_r_R_power*sum_2i_ai_r_R_power));
    }
    else if (wellModel_ == ParamsType::params.WellModel.Polynomial)
    {
      Real s = r / R_;

      Real f, df, d2f;
      wellActivationFunctionPolynomial(avec_, s, 0.0, f, df, d2f);

      Real df_s;
      if (s == 0.0) //avoid division by zero
      {
        // f'(s) / s = a1/s + 2*a2 + 3*a3*s + ...
        SANS_ASSERT(avec_[1] == 0.0);
        df_s = 2.0*avec_[2];
      }
      else
        df_s = df/s;

      dpndr2 = (pn - pB_) / ((f + log(R_/Rwb_))*R_*R_) * (df_s + d2f);
    }
    else if (wellModel_ == ParamsType::params.WellModel.PolynomialFlat)
    {
      Real s = r / R_;

      Real f, df, d2f;
      Real f1, df1, d2f1;
      wellActivationFunctionPolynomial(avec_, s, s0_, f, df, d2f);
      wellActivationFunctionPolynomial(avec_, 1.0, s0_, f1, df1, d2f1); //evaluated at s = 1

      Real df_s;
      if (s == 0.0) //avoid division by zero
      {
        if (s0_ == 0.0)
        {
          // f'(s) / s = a1/s + 2*a2 + 3*a3*s + ...
          SANS_ASSERT(avec_[1] == 0.0);
          df_s = 2.0*avec_[2];
        }
        else
        {
          df_s = 0.0;
        }
      }
      else
        df_s = df/s;

      dpndr2 = (pn - pB_) / ((f - 1.0 + df1*log(R_/Rwb_))*R_*R_) * (df_s + d2f);
    }

    Tq tmp = weight * volscale * sqrt(Kxx*Kyy) * dpndr2;

    if (computeMassFlow_)
    {
      //Evaluate densities
      Tq rhow = rhow_.density(x, y, time, pw);
      Tq rhon = rhon_.density(x, y, time, pn);

      //Note that the source term is on LHS
      source[0] += rhow*lambda_w*tmp; //Mass flow rate of phase 1
      source[1] += rhon*lambda_n*tmp; //Mass flow rate of phase 2
    }
    else
    {
      //Note that the source term is on LHS
      source[0] += lambda_w*tmp; //Volume flow rate of phase 1
      source[1] += lambda_n*tmp; //Volume flow rate of phase 2
    }
  }

  ~SourceTwoPhase2D_FixedPressureOutflow() {}

protected:
  const Real pB_; //Fixed Bottom hole pressure
  const Real Rwb_; //Well-bore radius
  const int nParam_; //number of well model parameters
  const Real s0_;
  const std::string wellModel_;
  std::vector<Real> avec_; //well model parameters

  const DensityModelw& rhow_; //Density model for wetting phase
  const DensityModeln& rhon_; //Density model for non-wetting phase

  const RelPermModelw& krw_; //Relative permeability model for wetting phase
  const RelPermModeln& krn_; //Relative permeability model for non-wetting phase

  const ViscModelw& muw_; //Viscosity model for wetting phase
  const ViscModeln& mun_; //Viscosity model for non-wetting phase

  const RockPermModel& K_; // Rock permeability model
  const CapillaryModel& pc_; //Capillary pressure model

  const QInterpreter qInterpreter_; // solution variable interpreter class

  const bool computeMassFlow_; //mass flow rate or volumetric flow rate?
};


//----------------------------------------------------------------------------//
template <template <class> class PDETraitsSize, class PDETraitsModel>
class SourceTwoPhase2D_FixedInflowRate : public SourceTwoPhase2DBase
{
public:

  //Extract model traits
  typedef typename PDETraitsModel::QType QType;
  typedef typename PDETraitsModel::DensityModelw DensityModelw;
  typedef typename PDETraitsModel::DensityModeln DensityModeln;
  typedef typename PDETraitsModel::PorosityModel PorosityModel;
  typedef typename PDETraitsModel::RelPermModelw RelPermModelw;
  typedef typename PDETraitsModel::RelPermModeln RelPermModeln;
  typedef typename PDETraitsModel::ViscModelw ViscModelw;
  typedef typename PDETraitsModel::ViscModeln ViscModeln;
  typedef typename PDETraitsModel::RockPermModel RockPermModel;
  typedef typename PDETraitsModel::CapillaryModel CapillaryModel;
  typedef Q2D<QType, CapillaryModel, PDETraitsSize> QInterpreter;  // solution variable interpreter type

  template <class T>
  using ArrayQ = typename QInterpreter::template ArrayQ<T>;   // solution/residual arrays

  typedef SourceTwoPhase2DBase BaseType;
  typedef SourceTwoPhase2DParam BaseParamsType;
  typedef SourceTwoPhase2DType_FixedInflowRate_Param ParamsType;

  explicit SourceTwoPhase2D_FixedInflowRate(
      const PyDict& d, const DensityModelw& rhow, const DensityModeln& rhon,
      const RelPermModelw& krw, const RelPermModeln& krn, const ViscModelw& muw, const ViscModeln& mun,
      const RockPermModel& K, const CapillaryModel& pc)
  : BaseType(d),
    Q_(d.get(BaseParamsType::params.Source).get(ParamsType::params.Q)),
    Sw_in_(d.get(BaseParamsType::params.Source).get(ParamsType::params.Sw)),
    nParam_(d.get(BaseParamsType::params.Source).get(ParamsType::params.nParam)),
    rhow_(rhow), rhon_(rhon), krw_(krw), krn_(krn), muw_(muw), mun_(mun), K_(K), pc_(pc),
    qInterpreter_(pc_)
  {
    avec_ = wellActivationFunctionPolyCoeff(nParam_);
  }

  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& xL, const Real& yL,
      const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const {}

  template <class Tq, class Tg, class Ts>
  void source(const Real& x, const Real& y, const Real& time,
              const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
              ArrayQ<Ts>& source ) const
  {
    //Evaluate rock permeability matrix
    Real Kxx, Kxy, Kyx, Kyy;
    K_.permeability(x, y, time, Kxx, Kxy, Kyx, Kyy);

    SANS_ASSERT_MSG( fabs(Kxy) + fabs(Kyx) <= std::numeric_limits<Real>::epsilon(),
                     "SourceTwoPhase2D_FixedInflowRate::source - K matrix needs to be diagonal." );

    //Coordinate transform parameters
    Real xscale = pow(Kyy/Kxx, 0.25);
    Real yscale = pow(Kxx/Kyy, 0.25);
    Real savg = 0.5*(xscale + yscale);
    xscale /= savg;
    yscale /= savg;
    Real volscale = xscale*yscale;

    Real weight = getWeight(x, y, time, xscale, yscale);
    if (weight < 100*std::numeric_limits<Real>::epsilon()) return; //The weighting factor is too small, so not worth computing source term

    Real r = getRadialDistance(x, y, xscale, yscale);
    Real s = r / R_;

    Real f, df, d2f;
    wellActivationFunctionPolynomial(avec_, s, 0.0, f, df, d2f);

    Real df_s;
    if (s == 0.0) //avoid division by zero
    {
      // f'(s) / s = a1/s + 2*a2 + 3*a3*s + ...
      SANS_ASSERT(avec_[1] == 0.0);
      df_s = 2.0*avec_[2];
    }
    else
      df_s = df/s;

    Real qhat = df_s + d2f;

    Tq Sw = 0, Sn = 0, pw = 0, pn = 0;

    //Evaluate pn using reservoir state
    qInterpreter_.eval_pn( q, pn );

    //Create a new state using specified inflow saturation
    ArrayQ<Tq> q_in;
    qInterpreter_.setFromPrimitive( q_in, PressureNonWet_SaturationWet<Tq>(pn, Sw_in_) );

    //Evaluate using the modified state
    qInterpreter_.eval( q_in, pw, pn, Sw, Sn );

    //Evaluate densities
    Tq rhow = rhow_.density(x, y, time, pw);
    Tq rhon = rhon_.density(x, y, time, pn);

    //Evaluate relative permeabilities
    Tq krw = krw_.relativeperm(Sw);
    Tq krn = krn_.relativeperm(Sn);

    //Evaluate phase viscosities
    Tq muw = muw_.viscosity(x, y, time);
    Tq mun = mun_.viscosity(x, y, time);

    //Evaluate phase mobilities
    Tq lambda_w = krw/muw;
    Tq lambda_n = krn/mun;
    Tq lambda_t = lambda_w + lambda_n;

    Tq tmp = (weight/visibleFrac_) * Q_ / (2.0*PI*R_*R_) * qhat * volscale / lambda_t;

    //Note that source term is on LHS
    source[0] -= rhow*lambda_w*tmp; //Mass flow rate of phase 1
    source[1] -= rhon*lambda_n*tmp; //Mass flow rate of phase 2
  }

  template <class Tq>
  void bottomHolePressureIntegrand(const Real& x, const Real& y, const Real& time,
                                   const ArrayQ<Tq>& q, const Real& rw,
                                   DLA::VectorS<2,Tq>& integrand) const
  {
    //Evaluate rock permeability matrix
    Real Kxx, Kxy, Kyx, Kyy;
    K_.permeability(x, y, time, Kxx, Kxy, Kyx, Kyy);

    SANS_ASSERT_MSG( fabs(Kxy) + fabs(Kyx) <= std::numeric_limits<Real>::epsilon(),
                     "bottomHolePressureIntegrand - K matrix needs to be diagonal." );

    //Coordinate transform parameters
    Real xscale = pow(Kyy/Kxx, 0.25);
    Real yscale = pow(Kxx/Kyy, 0.25);
    Real savg = 0.5*(xscale + yscale);
    xscale /= savg;
    yscale /= savg;
    Real volscale = xscale*yscale;

    Real weight = getWeight(x, y, time, xscale, yscale);
    if (weight < 100*std::numeric_limits<Real>::epsilon())
      return; //The weighting factor is too small, so not worth computing source term

    Real r = getRadialDistance(x, y, xscale, yscale);
    Real s = r / R_;

    Real f, df, d2f;
    wellActivationFunctionPolynomial(avec_, s, 0.0, f, df, d2f);

    Real df_s;
    if (s == 0.0) //avoid division by zero
    {
      // f'(s) / s = a1/s + 2*a2 + 3*a3*s + ...
      SANS_ASSERT(avec_[1] == 0.0);
      df_s = 2.0*avec_[2];
    }
    else
      df_s = df/s;

    Real qhat = df_s + d2f;

    Tq Sw = 0, Sn = 0, pw = 0, pn = 0;

    //Evaluate pn using reservoir state
    qInterpreter_.eval_pn( q, pn );

    //Create a new state using specified inflow saturation
    ArrayQ<Tq> q_in;
    qInterpreter_.setFromPrimitive( q_in, PressureNonWet_SaturationWet<Tq>(pn, Sw_in_) );

    //Evaluate using the modified state
    qInterpreter_.eval( q_in, pw, pn, Sw, Sn );

    //Evaluate relative permeabilities
    Tq krw = krw_.relativeperm(Sw);
    Tq krn = krn_.relativeperm(Sn);

    //Evaluate phase viscosities
    Tq muw = muw_.viscosity(x, y, time);
    Tq mun = mun_.viscosity(x, y, time);

    //Evaluate phase mobilities
    Tq lambda_w = krw/muw;
    Tq lambda_n = krn/mun;
    Tq lambda_t = lambda_w + lambda_n;

    Tq tmp = lambda_t * sqrt(Kxx*Kyy) * volscale  / ((f + log(R_/rw))*R_*R_) * qhat;
    integrand[0] = tmp * pn;
    integrand[1] = tmp;
  }

  ~SourceTwoPhase2D_FixedInflowRate() {}

protected:
  Real Q_;  //Fixed inflow rate (per unit area)
  Real Sw_in_; //Specified inflow wetting phase saturation
  const int nParam_; //number of well model parameters
  std::vector<Real> avec_; //well model parameters

  const DensityModelw& rhow_; //Density model for wetting phase
  const DensityModeln& rhon_; //Density model for non-wetting phase

  const RelPermModelw& krw_; //Relative permeability model for wetting phase
  const RelPermModeln& krn_; //Relative permeability model for non-wetting phase

  const ViscModelw& muw_; //Viscosity model for wetting phase
  const ViscModeln& mun_; //Viscosity model for non-wetting phase

  const RockPermModel& K_; // Rock permeability model
  const CapillaryModel& pc_; //Capillary pressure model
  const QInterpreter qInterpreter_; // solution variable interpreter class
};


//----------------------------------------------------------------------------//
template <template <class> class PDETraitsSize, class PDETraitsModel>
class SourceTwoPhase2D_FixedOutflowRate : public SourceTwoPhase2DBase
{
public:

  //Extract model traits
  typedef typename PDETraitsModel::QType QType;
  typedef typename PDETraitsModel::DensityModelw DensityModelw;
  typedef typename PDETraitsModel::DensityModeln DensityModeln;
  typedef typename PDETraitsModel::PorosityModel PorosityModel;
  typedef typename PDETraitsModel::RelPermModelw RelPermModelw;
  typedef typename PDETraitsModel::RelPermModeln RelPermModeln;
  typedef typename PDETraitsModel::ViscModelw ViscModelw;
  typedef typename PDETraitsModel::ViscModeln ViscModeln;
  typedef typename PDETraitsModel::RockPermModel RockPermModel;
  typedef typename PDETraitsModel::CapillaryModel CapillaryModel;
  typedef Q2D<QType, CapillaryModel, PDETraitsSize> QInterpreter;  // solution variable interpreter type

  template <class T>
  using ArrayQ = typename QInterpreter::template ArrayQ<T>;    // solution/residual arrays

  typedef SourceTwoPhase2DBase BaseType;
  typedef SourceTwoPhase2DParam BaseParamsType;
  typedef SourceTwoPhase2DType_FixedOutflowRate_Param ParamsType;

  SourceTwoPhase2D_FixedOutflowRate(
      const PyDict& d, const DensityModelw& rhow, const DensityModeln& rhon,
      const RelPermModelw& krw, const RelPermModeln& krn, const ViscModelw& muw, const ViscModeln& mun,
      const RockPermModel& K, const CapillaryModel& pc, const bool computeMassFlow = true)
  : BaseType(d), dict_(d),
    Q_(d.get(BaseParamsType::params.Source).get(ParamsType::params.Q)),
    nParam_(d.get(BaseParamsType::params.Source).get(ParamsType::params.nParam)),
    rhow_(rhow), rhon_(rhon), krw_(krw), krn_(krn), muw_(muw), mun_(mun), K_(K), pc_(pc),
    qInterpreter_(pc_), computeMassFlow_(computeMassFlow)
  {
    avec_ = wellActivationFunctionPolyCoeff(nParam_);
  }

  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& xL, const Real& yL,
      const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const {}

  template <class Tq, class Tg, class Ts>
  void source(const Real& x, const Real& y, const Real& time,
              const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
              ArrayQ<Ts>& source ) const
  {
    //Evaluate rock permeability matrix
    Real Kxx, Kxy, Kyx, Kyy;
    K_.permeability(x, y, time, Kxx, Kxy, Kyx, Kyy);

    SANS_ASSERT_MSG( fabs(Kxy) + fabs(Kyx) <= std::numeric_limits<Real>::epsilon(),
                     "SourceTwoPhase2D_FixedOutflowRate::source - K matrix needs to be diagonal." );

    //Coordinate transform parameters
    Real xscale = pow(Kyy/Kxx, 0.25);
    Real yscale = pow(Kxx/Kyy, 0.25);
    Real savg = 0.5*(xscale + yscale);
    xscale /= savg;
    yscale /= savg;
    Real volscale = xscale*yscale;

    Real weight = getWeight(x, y, time, xscale, yscale);
    if (weight < 100*std::numeric_limits<Real>::epsilon()) return; //The weighting factor is too small, so not worth computing source term

    Real r = getRadialDistance(x, y, xscale, yscale);
    Real s = r / R_;

    Real f, df, d2f;
    wellActivationFunctionPolynomial(avec_, s, 0.0, f, df, d2f);

    Real df_s;
    if (s == 0.0) //avoid division by zero
    {
      // f'(s) / s = a1/s + 2*a2 + 3*a3*s + ...
      SANS_ASSERT(avec_[1] == 0.0);
      df_s = 2.0*avec_[2];
    }
    else
      df_s = df/s;

    Real qhat = df_s + d2f;

    Tq Sw = 0, Sn = 0, pw = 0, pn = 0;

    //Evaluate using reservoir state for producers
    qInterpreter_.eval( q, pw, pn, Sw, Sn );

    //Evaluate relative permeabilities
    Tq krw = krw_.relativeperm(Sw);
    Tq krn = krn_.relativeperm(Sn);

    //Evaluate phase viscosities
    Tq muw = muw_.viscosity(x, y, time);
    Tq mun = mun_.viscosity(x, y, time);

    //Evaluate phase mobilities
    Tq lambda_w = krw/muw;
    Tq lambda_n = krn/mun;
    Tq lambda_t = lambda_w + lambda_n;

    Tq tmp = (weight/visibleFrac_) * Q_ / (2.0*PI*R_*R_) * qhat * volscale / lambda_t;

    if (computeMassFlow_)
    {
      //Evaluate densities
      Tq rhow = rhow_.density(x, y, time, pw);
      Tq rhon = rhon_.density(x, y, time, pn);

      //Note that the source term is on LHS
      source[0] += rhow*lambda_w*tmp; //Mass flow rate of phase 1
      source[1] += rhon*lambda_n*tmp; //Mass flow rate of phase 2
    }
    else
    {
      //Note that the source term is on LHS
      source[0] += lambda_w*tmp; //Volume flow rate of phase 1
      source[1] += lambda_n*tmp; //Volume flow rate of phase 2
    }
  }

  template <class Tq>
  void bottomHolePressureIntegrand(const Real& x, const Real& y, const Real& time,
                                   const ArrayQ<Tq>& q, const Real& rw,
                                   DLA::VectorS<2,Tq>& integrand) const
  {
    //Evaluate rock permeability matrix
    Real Kxx, Kxy, Kyx, Kyy;
    K_.permeability(x, y, time, Kxx, Kxy, Kyx, Kyy);

    SANS_ASSERT_MSG( fabs(Kxy) + fabs(Kyx) <= std::numeric_limits<Real>::epsilon(),
                     "bottomHolePressureIntegrand- K matrix needs to be diagonal." );

    //Coordinate transform parameters
    Real xscale = pow(Kyy/Kxx, 0.25);
    Real yscale = pow(Kxx/Kyy, 0.25);
    Real savg = 0.5*(xscale + yscale);
    xscale /= savg;
    yscale /= savg;
    Real volscale = xscale*yscale;

    Real weight = getWeight(x, y, time, xscale, yscale);
    if (weight < 100*std::numeric_limits<Real>::epsilon())
      return; //The weighting factor is too small, so not worth computing source term

    Real r = getRadialDistance(x, y, xscale, yscale);
    Real s = r / R_;

    Real f, df, d2f;
    wellActivationFunctionPolynomial(avec_, s, 0.0, f, df, d2f);

    Real df_s;
    if (s == 0.0) //avoid division by zero
    {
      // f'(s) / s = a1/s + 2*a2 + 3*a3*s + ...
      SANS_ASSERT(avec_[1] == 0.0);
      df_s = 2.0*avec_[2];
    }
    else
      df_s = df/s;

    Real qhat = df_s + d2f;

    Tq Sw = 0, Sn = 0, pw = 0, pn = 0;
    qInterpreter_.eval( q, pw, pn, Sw, Sn );

    //Evaluate relative permeabilities
    Tq krw = krw_.relativeperm(Sw);
    Tq krn = krn_.relativeperm(Sn);

    //Evaluate phase viscosities
    Tq muw = muw_.viscosity(x, y, time);
    Tq mun = mun_.viscosity(x, y, time);

    //Evaluate phase mobilities
    Tq lambda_w = krw/muw;
    Tq lambda_n = krn/mun;
    Tq lambda_t = lambda_w + lambda_n;

    Tq tmp = lambda_t * sqrt(Kxx*Kyy) * volscale  / ((f + log(R_/rw))*R_*R_) * qhat;
    integrand[0] = tmp * pn;
    integrand[1] = tmp;
  }

  ~SourceTwoPhase2D_FixedOutflowRate() {}

protected:
  const PyDict dict_;

  Real Q_; //Fixed volumetric outflow rate per unit length [L^2/T]
  const int nParam_; //number of well model parameters
  std::vector<Real> avec_; //well model parameters

  const DensityModelw& rhow_; //Density model for wetting phase
  const DensityModeln& rhon_; //Density model for non-wetting phase

  const RelPermModelw& krw_; //Relative permeability model for wetting phase
  const RelPermModeln& krn_; //Relative permeability model for non-wetting phase

  const ViscModelw& muw_; //Viscosity model for wetting phase
  const ViscModeln& mun_; //Viscosity model for non-wetting phase

  const RockPermModel& K_; // Rock permeability model
  const CapillaryModel& pc_; //Capillary pressure model
  const QInterpreter qInterpreter_; // solution variable interpreter class

  const bool computeMassFlow_; //mass flow rate or volumetric flow rate?
};

} //namespace SANS

#endif  // SOURCETWOPHASE2D_H
