// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "Q2DPrimitive_pnSw.h"
#include "TraitsTwoPhaseArtificialViscosity.h"
#include "BCTwoPhaseArtificialViscosity2D.h"

#include "DensityModel.h"
#include "PorosityModel.h"
#include "RelPermModel_PowerLaw.h"
#include "ViscosityModel_Constant.h"
#include "PermeabilityModel2D.h"
#include "CapillaryModel.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_Source2D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor2D.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{

//===========================================================================//
// Instantiate BC parameters

typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel_Comp, DensityModel_Comp, PorosityModel_Comp,
                            RelPermModel_PowerLaw, RelPermModel_PowerLaw, ViscosityModel_Constant, ViscosityModel_Constant,
                            PermeabilityModel2D_QuadBlock, CapillaryModel_Linear> TraitsModel_RelPower_ViscConst_PermQuadBlock_CapLinear;

typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel_Comp, DensityModel_Comp, PorosityModel_CartTable,
                            RelPermModel_PowerLaw, RelPermModel_PowerLaw, ViscosityModel_Constant, ViscosityModel_Constant,
                            PermeabilityModel2D_CartTable, CapillaryModel_Linear> TraitsModel_RelPower_ViscConst_CartTable_CapLinear;


typedef PDETwoPhase_ArtificialViscosity2D<TraitsSizeTwoPhaseArtificialViscosity,
                                          TraitsModel_RelPower_ViscConst_PermQuadBlock_CapLinear> PDE_Comp_RelPower_ViscConst_PermQuadBlock_CapLinear;

typedef PDETwoPhase_ArtificialViscosity2D<TraitsSizeTwoPhaseArtificialViscosity,
                                          TraitsModel_RelPower_ViscConst_CartTable_CapLinear> PDE_Comp_RelPower_ViscConst_CartTable_CapLinear;


typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeTwoPhaseArtificialViscosity> SensorAdvectiveFlux;
typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeTwoPhaseArtificialViscosity> SensorViscousFlux;


typedef AVSensor_Source2D_TwoPhase<PDE_Comp_RelPower_ViscConst_PermQuadBlock_CapLinear> SensorSource_Comp_RelPower_ViscConst_PermQuadBlock_CapLinear;

typedef AVSensor_Source2D_TwoPhase<PDE_Comp_RelPower_ViscConst_CartTable_CapLinear> SensorSource_Comp_RelPower_ViscConst_CartTable_CapLinear;

typedef TraitsModelArtificialViscosity<PDE_Comp_RelPower_ViscConst_PermQuadBlock_CapLinear, SensorAdvectiveFlux, SensorViscousFlux,
                                       SensorSource_Comp_RelPower_ViscConst_PermQuadBlock_CapLinear>
        TraitsModelAV_Comp_RelPower_ViscConst_PermQuadBlock_CapLinear;

typedef TraitsModelArtificialViscosity<PDE_Comp_RelPower_ViscConst_CartTable_CapLinear, SensorAdvectiveFlux, SensorViscousFlux,
                                       SensorSource_Comp_RelPower_ViscConst_CartTable_CapLinear>
        TraitsModelAV_Comp_RelPower_ViscConst_CartTable_CapLinear;


// Instantiate the BC parameters
typedef BCTwoPhaseArtificialViscosity2DVector<TraitsSizeTwoPhaseArtificialViscosity,
                                              TraitsModelAV_Comp_RelPower_ViscConst_PermQuadBlock_CapLinear>
        BCVector_Comp_RelPower_ViscConst_PermQuadBlock_CapLinear;
BCPARAMETER_INSTANTIATE( BCVector_Comp_RelPower_ViscConst_PermQuadBlock_CapLinear )

typedef BCTwoPhaseArtificialViscosity2DVector<TraitsSizeTwoPhaseArtificialViscosity,
                                              TraitsModelAV_Comp_RelPower_ViscConst_CartTable_CapLinear>
        BCVector_Comp_RelPower_ViscConst_CartTable_CapLinear;
BCPARAMETER_INSTANTIATE( BCVector_Comp_RelPower_ViscConst_CartTable_CapLinear )

} //namespace SANS
