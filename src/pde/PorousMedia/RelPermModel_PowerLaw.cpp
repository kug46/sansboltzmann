// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "RelPermModel_PowerLaw.h"

//Instantiate the CheckInput function
//PARAMETER_VECTOR_IMPL( RelPermModel_PowerLaw_Params );

namespace SANS
{

void RelPermModel_PowerLaw_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.power));
  d.checkUnknownInputs(allParams);
}
RelPermModel_PowerLaw_Params RelPermModel_PowerLaw_Params::params;


void
RelPermModel_PowerLaw::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "RelPermModel_PowerLaw_Params<T>:" << std::endl;
  out << indent << "  power_ = " << power_ << std::endl;
}


}
