// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCTWOPHASEARTIFICIALVISCOSITY2D_H
#define BCTWOPHASEARTIFICIALVISCOSITY2D_H

// 2-D Two-phase BC class with artificial viscosity

//PyDict must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/BCCategory.h"
#include "pde/BCNone.h"
#include "Topology/Dimension.h"

#include <iostream>

#include <boost/mpl/vector.hpp>

#include "BCTwoPhase2D.h"
#include "PDETwoPhase_ArtificialViscosity2D.h"

namespace SANS
{

template <template <class> class PDETraitsSize, class PDETraitsModel>
class PDEmitAVSensor2D;

//----------------------------------------------------------------------------//
// BC types
//----------------------------------------------------------------------------//

class BCTypeTimeIC;
class BCTypeFullState;
class BCTypeNoFlux;
class BCTypeNoFluxSpaceTime;

class BCTypeFlux_mitState;
class BCTypeFlux_mitStateSpaceTime;

//----------------------------------------------------------------------------//
// BC class: 2-D TwoPhase with artificial viscosity
//
// template parameters:
//   PhysDim              Physical dimensions
//   BCType               BC type
//----------------------------------------------------------------------------//

template <class BCType, class BCBase>
class BCmitAVSensor2D;

template <class BCType, class BCBaseParams>
struct BCmitAVSensor2DParams;


// Define the vector of boundary conditions
template <template <class> class PDETraitsSize, class PDETraitsModel>
using BCTwoPhaseArtificialViscosity2DVector =  boost::mpl::vector5<
    BCNone<PhysD2,PDETraitsSize<PhysD2>::N>,
    SpaceTimeBC< BCmitAVSensor2D<BCTypeFlux_mitStateSpaceTime,
                 BCTwoPhase2D<BCTypeTimeIC, PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel> >> >,
    BCmitAVSensor2D<BCTypeFlux_mitState, BCTwoPhase2D<BCTypeFullState, PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>> >,
    BCmitAVSensor2D<BCTypeFlux_mitState   , BCTwoPhase2D<BCTypeNoFlux   , PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>> >,
    SpaceTimeBC< BCmitAVSensor2D<BCTypeFlux_mitStateSpaceTime,
                 BCTwoPhase2D<BCTypeNoFluxSpaceTime, PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel> >> >
    >;

//----------------------------------------------------------------------------//
// Conventional PX-style flux mitState
//----------------------------------------------------------------------------//
template<class BCBaseParams>
struct BCmitAVSensor2DParams<BCTypeFlux_mitState, BCBaseParams> : public BCBaseParams
{
  const ParameterNumeric<Real> Cdiff{"Cdiff", NO_DEFAULT, 0.0, NO_LIMIT, "Constant for diffusion length scale"};

  using BCBaseParams::BCName;
  typedef typename BCBaseParams::Option Option;

  // cppcheck-suppress passedByValue
  static void checkInputs(PyDict d)
  {
    //TODO: This is not quite right...
    std::vector<const ParameterBase*> allParams;
    allParams.push_back(d.checkInputs(params.Cdiff));
    BCBaseParams::checkInputs(d);
//    d.checkUnknownInputs(allParams);
  }

  static BCmitAVSensor2DParams params;
};
// Instantiate the singleton
template<class BCBaseParams>
BCmitAVSensor2DParams<BCTypeFlux_mitState, BCBaseParams> BCmitAVSensor2DParams<BCTypeFlux_mitState, BCBaseParams>::params;


template <class BCBase>
class BCmitAVSensor2D<BCTypeFlux_mitState, BCBase> : public BCBase
{
public:
  typedef PhysD2 PhysDim;
  typedef typename BCBase::Category Category;
  typedef BCmitAVSensor2DParams<BCTypeFlux_mitState, typename BCBase::ParamsType> ParamsType;
//  typedef typename BCBase::PDE PDE;

  using BCBase::D;   // physical dimensions
  using BCBase::N;   // total solution variables

  using BCBase::NBC; // total BCs

  static const int iSens = N - 1;

  template <class T>
  using ArrayQ = typename BCBase::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename BCBase::template MatrixQ<T>;   // matrices

  typedef DLA::VectorS<D, Real> VectorX;

  template< class... BCArgs > //cppcheck-suppress noExplicitConstructor
  BCmitAVSensor2D( const Real& Cdiff, BCArgs&&... args ) :
    BCBase(std::forward<BCArgs>(args)...), Cdiff_(Cdiff) {}

  BCmitAVSensor2D( const typename BCBase::PDE& pde, const PyDict& d ) :
    BCBase(pde, d), Cdiff_(d.get(ParamsType::params.Cdiff)) {}

  ~BCmitAVSensor2D() {}

  BCmitAVSensor2D( const BCmitAVSensor2D& ) = delete;
  BCmitAVSensor2D& operator=( const BCmitAVSensor2D& ) = delete;

  // BC state
  template <class Tp, class T>
  void state( const Tp& param,
              const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BCBase::state(x, y, time, nx, ny, qI, qB);
    qB(iSens) = qI(iSens);
  }

  // normal BC flux
  template <int PhysD, class T, class Tf>
  void fluxNormal( const DLA::MatrixSymS<PhysD,Real>& param,
                   const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    BCBase::fluxNormal(param, x, y, time, nx, ny, qI, qIx, qIy, qB, Fn);

    //Robin BC for artificial viscosity
    static_assert(PhysD == D || PhysD == D+1, "H-tensor dimension mismatch!");
    DLA::MatrixSymS<PhysD,Real> H = exp(param); //generalized h-tensor

    VectorX n = {nx, ny};
    Real hn = 0;
    for (int i = 0; i < D; i++)
      for (int j = 0; j < D; j++)
        hn += n[i]*H(i,j)*n[j];

    Real nu_inf = 0.0;
    Fn(iSens) = sqrt(Cdiff_)*hn*(qI(iSens) - nu_inf); //Robin condition for artificial viscosity
  }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const Real Cdiff_; //diffusion constant - should be the same constant used in AV diffusion matrix
};

//----------------------------------------------------------------------------//
// Conventional PX-style flux mitState for space-time
//----------------------------------------------------------------------------//
template<class BCBaseParams>
struct BCmitAVSensor2DParams<BCTypeFlux_mitStateSpaceTime, BCBaseParams> : public BCBaseParams
{
  const ParameterNumeric<Real> Cdiff{"Cdiff", NO_DEFAULT, 0.0, NO_LIMIT, "Constant for diffusion length scale"};

  using BCBaseParams::BCName;
  typedef typename BCBaseParams::Option Option;

  // cppcheck-suppress passedByValue
  static void checkInputs(PyDict d)
  {
    //TODO: This is not quite right...
    std::vector<const ParameterBase*> allParams;
    allParams.push_back(d.checkInputs(params.Cdiff));
    BCBaseParams::checkInputs(d);
//    d.checkUnknownInputs(allParams);
  }

  static BCmitAVSensor2DParams params;
};
// Instantiate the singleton
template<class BCBaseParams>
BCmitAVSensor2DParams<BCTypeFlux_mitStateSpaceTime, BCBaseParams>
BCmitAVSensor2DParams<BCTypeFlux_mitStateSpaceTime, BCBaseParams>::params;


template <class BCBase>
class BCmitAVSensor2D<BCTypeFlux_mitStateSpaceTime, BCBase> : public BCBase
{
public:
  typedef PhysD2 PhysDim;
  typedef typename BCBase::Category Category;
  typedef BCmitAVSensor2DParams<BCTypeFlux_mitStateSpaceTime, typename BCBase::ParamsType> ParamsType;
//  typedef typename BCBase::PDE PDE;

  using BCBase::D;   // physical dimensions
  using BCBase::N;   // total solution variables

  using BCBase::NBC; // total BCs

  static const int iSens = N - 1;

  template <class T>
  using ArrayQ = typename BCBase::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename BCBase::template MatrixQ<T>;   // matrices

  typedef DLA::VectorS<D+1, Real> VectorXT;

  template< class... BCArgs > //cppcheck-suppress noExplicitConstructor
  BCmitAVSensor2D( const Real& Cdiff, BCArgs&&... args ) :
    BCBase(std::forward<BCArgs>(args)...), Cdiff_(Cdiff) {}

  BCmitAVSensor2D( const typename BCBase::PDE& pde, const PyDict& d ) :
    BCBase(pde, d), Cdiff_(d.get(ParamsType::params.Cdiff)) {}

  ~BCmitAVSensor2D() {}

  BCmitAVSensor2D( const BCmitAVSensor2D& ) = delete;
  BCmitAVSensor2D& operator=( const BCmitAVSensor2D& ) = delete;

  // BC state
  template <class Tp, class T>
  void state( const Tp& param,
              const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BCBase::state(x, y, time, nx, ny, nt, qI, qB);
    qB(iSens) = qI(iSens);
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormalSpaceTime( const DLA::MatrixSymS<D+1,Real>& param,
                            const Real& x, const Real& y, const Real& time,
                            const Real& nx, const Real& ny, const Real& nt,
                            const ArrayQ<T>& qI,
                            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIt,
                            const ArrayQ<T>& qB,
                            ArrayQ<Tf>& Fn) const
  {
    BCBase::fluxNormalSpaceTime(param, x, y, time, nx, ny, nt, qI, qIx, qIy, qIt, qB, Fn);

    //Robin BC for artificial viscosity
    DLA::MatrixSymS<D+1,Real> H = exp(param); //generalized h-tensor

    VectorXT n = {nx, ny, nt};
    Real hn = 0;
    for (int i = 0; i < D+1; i++)
      for (int j = 0; j < D+1; j++)
        hn += n[i]*H(i,j)*n[j];

    const Real nu_inf = 0.0;
    Fn(iSens) = sqrt(Cdiff_)*hn*(qI(iSens) - nu_inf);
  }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const Real Cdiff_; //diffusion constant - should be the same constant used in AV diffusion matrix
};

} //namespace SANS

#endif  // BCTWOPHASEARTIFICIALVISCOSITY2D_H
