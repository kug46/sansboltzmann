// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SENSOR_TWOPHASE_H
#define SENSOR_TWOPHASE_H

#include "tools/SANSnumerics.h"     // Real
#include "tools/smoothmath.h"

#include <iostream>
#include <string>

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"

namespace SANS
{

template<class PDE>
class Sensor_TwoPhase
{
public:
  static const int D = PDE::D;

  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;  // solution/flux arrays

  typedef typename PDE::QInterpreter QInterpreter; // solution variable interpreter type

  explicit Sensor_TwoPhase(const PDE& pde) :
    pde_(pde), qInterpreter_(pde.variableInterpreter()) {}

  ~Sensor_TwoPhase() {}

  // variable used in calculating jump switch for artificial viscosity
  template<class T, class Ts>
  void jumpQuantity( const ArrayQ<T>& q, Ts& g ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const PDE& pde_;
  const QInterpreter& qInterpreter_;
};

template<class PDE>
template<class T, class Ts>
void inline
Sensor_TwoPhase<PDE>::
jumpQuantity( const ArrayQ<T>& q, Ts& g ) const
{
  T Sw;
  qInterpreter_.eval_Sw( q, Sw );

  g = Sw;
}

template<class PDE>
void inline
Sensor_TwoPhase<PDE>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "Sensor_TwoPhase: qInterpreter_ =" << std::endl;
  qInterpreter_.dump(indentSize+2, out);
}

} //namespace SANS

#endif  // SENSOR_TWOPHASE_H
