// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef VISCOSITYMODEL_CONSTANT_H
#define VISCOSITYMODEL_CONSTANT_H

// Phase viscosity models

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <iostream>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// A class for representing input parameters for the ViscosityModel
struct ViscosityModel_Constant_Params : noncopyable
{
  const ParameterNumeric<Real> mu{"mu", NO_DEFAULT, 0, NO_LIMIT, "Phase viscosity"};

  static void checkInputs(PyDict d);

  static ViscosityModel_Constant_Params params;
};

//----------------------------------------------------------------------------//
// ViscosityModel_Constant:
//
// template parameters:
//   T                        solution DOF data type (e.g. double)
//
// member functions:
//
//   .viscosity               viscosity of each phase: (e.g mu = [muw, mun] )
//   .viscosityJacobian       jacobian: d(kr)/d(Sw)
//   .viscosityGradient       gradient: d(kr)/dx and d(kr)/dy
//
//   .dump                    debug dump of private data
//----------------------------------------------------------------------------//



class ViscosityModel_Constant
{
public:
  ViscosityModel_Constant_Params& params;

  explicit ViscosityModel_Constant( const PyDict& d ) :
      params(ViscosityModel_Constant_Params::params),
      mu_(d.get(params.mu)){}

  ViscosityModel_Constant( const ViscosityModel_Constant& model ) :
      params( ViscosityModel_Constant_Params::params ),
      mu_(model.mu_){}

  explicit ViscosityModel_Constant( Real mu ) :
      params(ViscosityModel_Constant_Params::params),
      mu_(mu)
  {
    SANS_ASSERT( mu > 0 );
  }

  ViscosityModel_Constant& operator=(const ViscosityModel_Constant& model) = delete;

  ~ViscosityModel_Constant() {}

  // Viscosity of each phase
  Real viscosity( const Real& x, const Real& time ) const;
  Real viscosity( const Real& x, const Real& y, const Real& time ) const;

//  template<class T>
//  void viscosityJacobian( const Real& x, const Real& time, const T& Sw, T& mu_Sw ) const;
//
//  template<class T>
//  void viscosityGradient( const Real& x, const Real& time, const T& Sw, const T& Swx, T& krx ) const;
//  template<class T>
//  void viscosityGradient( const Real& x, const Real& time, const T& Sw, const T& Swx, const T& Swy,
//                          std::vector<T>& krx, std::vector<T>& kry ) const;

  void dump( std::string indent, std::ostream& out = std::cout ) const;
  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  Real mu_; // Phase viscosity (constant)
};


//template <class T>
inline Real
ViscosityModel_Constant::viscosity( const Real& x, const Real& time ) const
{
  return mu_;
}

inline Real
ViscosityModel_Constant::viscosity( const Real& x, const Real& y, const Real& time ) const
{
  return mu_;
}

#if 0
template <class T>
inline void
ViscosityModel::viscosityJacobian( const Real& x, const Real& time, const T& Sw, const int nphase, std::vector<T>& mu_Sw ) const
{
  SANS_ASSERT_MSG( nphase == Nphase_,
                  "The number of phases requested (=%d) is not consistent with that of the viscosity model (=%d).",
                  nphase, Nphase_);

  SANS_ASSERT((int) mu_Sw.size() == Nphase_ );

  if ( ModelType_ == params.ModelType.Constant )
  {
    for (int i=0; i<Nphase_; i++)
      mu_Sw[i] = 0.0;
  }
  else
    SANS_DEVELOPER_EXCEPTION( "ViscosityModel::viscosityJacobian - Unknown ModelType.");
}

template <class T>
inline void
ViscosityModel::viscosityGradient( const Real& x, const Real& time, const T& Sw, const T& Swx, const int nphase,
                                   std::vector<T>& mu_x ) const
{
  SANS_ASSERT_MSG( nphase == Nphase_,
                  "The number of phases requested (=%d) is not consistent with that of the viscosity model (=%d).",
                  nphase, Nphase_);

  SANS_ASSERT((int) mu_x.size() == Nphase_ );

  std::vector<T> mu_Sw(Nphase_);

  //Compute Jacobians
  viscosityJacobian( x, time, Sw, Nphase_, mu_Sw );

  //Apply chain rule for gradient: d(kr)/dx = d(kr)/d(Sw) * d(Sw)/dx
  for (int i=0; i < Nphase_; i++)
  {
    mu_x[i] = mu_Sw[i]*Swx;
  }
}

template <class T>
inline void
ViscosityModel::viscosityGradient( const Real& x, const Real& time, const T& Sw, const T& Swx, const T& Swy, const int nphase,
                                   std::vector<T>& mu_x, std::vector<T>& mu_y) const
{
  SANS_ASSERT_MSG( nphase == Nphase_,
                  "The number of phases requested (=%d) is not consistent with that of the viscosity model (=%d).",
                  nphase, Nphase_);

  SANS_ASSERT((int) mu_x.size() == Nphase_ );
  SANS_ASSERT((int) mu_y.size() == Nphase_ );

  std::vector<T> mu_Sw(Nphase_);

  //Compute Jacobians
  viscosityJacobian( x, time, Sw, Nphase_, mu_Sw );

  //Apply chain rule for gradient: d(mu)/dx = d(mu)/d(Sw) * d(Sw)/dx
  for (int i=0; i < Nphase_; i++)
  {
    mu_x[i] = mu_Sw[i]*Swx;
    mu_y[i] = mu_Sw[i]*Swy;
  }
}
#endif
} //namespace SANS

#endif  // VISCOSITYMODEL_CONSTANT_H
