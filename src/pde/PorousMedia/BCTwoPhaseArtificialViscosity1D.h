// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCTWOPHASEARTIFICIALVISCOSITY1D_H
#define BCTWOPHASEARTIFICIALVISCOSITY1D_H

// 1-D Two-phase BC class with artificial viscosity

//PyDict must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/BCCategory.h"
#include "pde/BCNone.h"
#include "Topology/Dimension.h"

#include <iostream>

#include <boost/mpl/vector.hpp>

#include "BCTwoPhase1D.h"
#include "PDETwoPhase_ArtificialViscosity1D.h"

namespace SANS
{

template <template <class> class PDETraitsSize, class PDETraitsModel>
class PDEmitAVSensor1D;

//----------------------------------------------------------------------------//
// BC types
//----------------------------------------------------------------------------//

class BCTypeTimeIC;
class BCTypeTimeIC_Function;
class BCTypeFullState;
class BCTypeFullStateSpaceTime;
//class BCTypeTimeOutflow;

class BCTypeFlux_mitState;
class BCTypeFlux_mitStateSpaceTime;

//----------------------------------------------------------------------------//
// BC class: 2-D TwoPhase with artificial viscosity
//
// template parameters:
//   PhysDim              Physical dimensions
//   BCType               BC type
//----------------------------------------------------------------------------//

template <class BCType, class BCBase>
class BCmitAVSensor1D;

template <class BCType, class BCBaseParams>
struct BCmitAVSensor1DParams;


// Define the vector of boundary conditions
template <template <class> class PDETraitsSize, class PDETraitsModel>
using BCTwoPhaseArtificialViscosity1DVector =  boost::mpl::vector5<
    BCNone<PhysD1,PDETraitsSize<PhysD1>::N>,
    SpaceTimeBC< BCmitAVSensor1D<BCTypeFlux_mitStateSpaceTime,
                 BCTwoPhase1D<BCTypeTimeIC, PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel> >> >,
    SpaceTimeBC< BCmitAVSensor1D<BCTypeFlux_mitStateSpaceTime,
                 BCTwoPhase1D<BCTypeTimeIC_Function, PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel> >> >,
    BCmitAVSensor1D<BCTypeFlux_mitState, BCTwoPhase1D<BCTypeFullState, PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>> >,
    SpaceTimeBC< BCmitAVSensor1D<BCTypeFlux_mitStateSpaceTime,
                 BCTwoPhase1D<BCTypeFullStateSpaceTime, PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>> > >
//    SpaceTimeBC< BCmitAVSensor1D<BCTypeTimeOutflow, PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel> > >
    >;

//----------------------------------------------------------------------------//
// Conventional PX-style flux mitState
//----------------------------------------------------------------------------//
template<class BCBaseParams>
struct BCmitAVSensor1DParams<BCTypeFlux_mitState, BCBaseParams> : public BCBaseParams
{
  const ParameterNumeric<Real> Cdiff{"Cdiff", NO_DEFAULT, 0.0, NO_LIMIT, "Constant for diffusion length scale"};

  using BCBaseParams::BCName;
  typedef typename BCBaseParams::Option Option;

  // cppcheck-suppress passedByValue
  static void checkInputs(PyDict d)
  {
    //TODO: This is not quite right...
    std::vector<const ParameterBase*> allParams;
    allParams.push_back(d.checkInputs(params.Cdiff));
    BCBaseParams::checkInputs(d);
//    d.checkUnknownInputs(allParams);
  }

  static BCmitAVSensor1DParams params;
};
// Instantiate the singleton
template<class BCBaseParams>
BCmitAVSensor1DParams<BCTypeFlux_mitState, BCBaseParams> BCmitAVSensor1DParams<BCTypeFlux_mitState, BCBaseParams>::params;


template <class BCBase>
class BCmitAVSensor1D<BCTypeFlux_mitState, BCBase> : public BCBase
{
public:
  typedef PhysD1 PhysDim;
  typedef typename BCBase::Category Category;
  typedef BCmitAVSensor1DParams<BCTypeFlux_mitState, typename BCBase::ParamsType> ParamsType;
//  typedef typename BCBase::PDE PDE;

  using BCBase::D;   // physical dimensions
  using BCBase::N;   // total solution variables

  using BCBase::NBC; // total BCs

  static const int iSens = N - 1;

  template <class T>
  using ArrayQ = typename BCBase::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename BCBase::template MatrixQ<T>;   // matrices

  typedef DLA::VectorS<D, Real> VectorX;

  template< class... BCArgs > //cppcheck-suppress noExplicitConstructor
  BCmitAVSensor1D( const Real& Cdiff, BCArgs&&... args ) :
    BCBase(std::forward<BCArgs>(args)...), Cdiff_(Cdiff) {}

  BCmitAVSensor1D( const typename BCBase::PDE& pde, const PyDict& d ) :
    BCBase(pde, d), Cdiff_(d.get(ParamsType::params.Cdiff)) {}

  ~BCmitAVSensor1D() {}

  BCmitAVSensor1D( const BCmitAVSensor1D& ) = delete;
  BCmitAVSensor1D& operator=( const BCmitAVSensor1D& ) = delete;

  // BC state
  template <class Tp, class T>
  void state( const Tp& param,
              const Real& x, const Real& time,
              const Real& nx,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BCBase::state(x, time, nx, qI, qB);
    qB(iSens) = qI(iSens);
  }

  // normal BC flux
  template <int PhysD, class T, class Tf>
  void fluxNormal( const DLA::MatrixSymS<PhysD,Real>& param,
                   const Real& x, const Real& time,
                   const Real& nx,
                   const ArrayQ<T>& qI, const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    BCBase::fluxNormal(param, x, time, nx, qI, qIx, qB, Fn);

    //Robin BC for artificial viscosity
    static_assert(PhysD == D || PhysD == D+1, "H-tensor dimension mismatch!");
    DLA::MatrixSymS<PhysD,Real> H = exp(param); //generalized h-tensor

    VectorX n = {nx};
    Real hn = 0;
    for (int i = 0; i < D; i++)
      for (int j = 0; j < D; j++)
        hn += n[i]*H(i,j)*n[j];

    const Real nu_inf = 0.0;
    Fn(iSens) = sqrt(Cdiff_)*hn*(qI(iSens) - nu_inf);
  }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const Real Cdiff_; //diffusion constant - should be the same constant used in AV diffusion matrix
};


//----------------------------------------------------------------------------//
// Conventional PX-style flux mitState for space-time
//----------------------------------------------------------------------------//
template<class BCBaseParams>
struct BCmitAVSensor1DParams<BCTypeFlux_mitStateSpaceTime, BCBaseParams> : public BCBaseParams
{
  const ParameterNumeric<Real> Cdiff{"Cdiff", NO_DEFAULT, 0.0, NO_LIMIT, "Constant for diffusion length scale"};

  using BCBaseParams::BCName;
  typedef typename BCBaseParams::Option Option;

  // cppcheck-suppress passedByValue
  static void checkInputs(PyDict d)
  {
    //TODO: This is not quite right...
    std::vector<const ParameterBase*> allParams;
    allParams.push_back(d.checkInputs(params.Cdiff));
    BCBaseParams::checkInputs(d);
//    d.checkUnknownInputs(allParams);
  }

  static BCmitAVSensor1DParams params;
};
// Instantiate the singleton
template<class BCBaseParams>
BCmitAVSensor1DParams<BCTypeFlux_mitStateSpaceTime, BCBaseParams>
BCmitAVSensor1DParams<BCTypeFlux_mitStateSpaceTime, BCBaseParams>::params;


template <class BCBase>
class BCmitAVSensor1D<BCTypeFlux_mitStateSpaceTime, BCBase> : public BCBase
{
public:
  typedef PhysD1 PhysDim;
  typedef typename BCBase::Category Category;
  typedef BCmitAVSensor1DParams<BCTypeFlux_mitStateSpaceTime, typename BCBase::ParamsType> ParamsType;
//  typedef typename BCBase::PDE PDE;

  using BCBase::D;   // physical dimensions
  using BCBase::N;   // total solution variables

  using BCBase::NBC; // total BCs

  static const int iSens = N - 1;

  template <class T>
  using ArrayQ = typename BCBase::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename BCBase::template MatrixQ<T>;   // matrices

  typedef DLA::VectorS<D+1, Real> VectorXT;

  template< class... BCArgs > //cppcheck-suppress noExplicitConstructor
  BCmitAVSensor1D( const Real& Cdiff, BCArgs&&... args ) :
    BCBase(std::forward<BCArgs>(args)...), Cdiff_(Cdiff) {}

  BCmitAVSensor1D( const typename BCBase::PDE& pde, const PyDict& d ) :
    BCBase(pde, d), Cdiff_(d.get(ParamsType::params.Cdiff)) {}

  ~BCmitAVSensor1D() {}

  BCmitAVSensor1D( const BCmitAVSensor1D& ) = delete;
  BCmitAVSensor1D& operator=( const BCmitAVSensor1D& ) = delete;

  // BC state
  template <class Tp, class T>
  void state( const Tp& param,
              const Real& x, const Real& time,
              const Real& nx, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BCBase::state(x, time, nx, nt, qI, qB);
    qB(iSens) = qI(iSens);
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormalSpaceTime( const DLA::MatrixSymS<D+1,Real>& param,
                            const Real& x, const Real& time,
                            const Real& nx, const Real& nt,
                            const ArrayQ<T>& qI,
                            const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
                            const ArrayQ<T>& qB,
                            ArrayQ<Tf>& Fn) const
  {
    BCBase::fluxNormalSpaceTime(param, x, time, nx, nt, qI, qIx, qIt, qB, Fn);

    //Robin BC for artificial viscosity
    DLA::MatrixSymS<D+1,Real> H = exp(param); //generalized h-tensor

    VectorXT n = {nx, nt};
    Real hn = 0;
    for (int i = 0; i < D+1; i++)
      for (int j = 0; j < D+1; j++)
        hn += n[i]*H(i,j)*n[j];

    const Real nu_inf = 0.0;
    Fn(iSens) = sqrt(Cdiff_)*hn*(qI(iSens) - nu_inf);
  }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const Real Cdiff_; //diffusion constant - should be the same constant used in AV diffusion matrix
};

#if 0
//----------------------------------------------------------------------------//
// Time outflow BC: a fancy version of the None BC which works for space-time diffusion

struct BCBaseDummy;

template<>
struct BCmitAVSensor1DParams<BCTypeTimeOutflow, BCBaseDummy> : noncopyable
{
  BCmitAVSensor1DParams() {}

//  const ParameterBool hasSpaceTimeDI{"isOutflow", false, "Is this an outflow boundary?"};

  static constexpr const char* BCName{"TimeOutflow"};
  struct Option
  {
    const DictOption TimeOutflow{BCmitAVSensor1DParams::BCName, BCmitAVSensor1DParams::checkInputs};
  };

  static void checkInputs(PyDict d);
  static BCmitAVSensor1DParams params;
};

template <class PDE_>
class BCmitAVSensor1D<BCTypeTimeOutflow, PDE_> : public BCType< BCmitAVSensor1D<BCTypeTimeOutflow, PDE_> >
{
public:
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCmitAVSensor1DParams<BCTypeTimeOutflow, BCBaseDummy> ParamsType;
  typedef PDE_ PDE;

  typedef PhysD1 PhysDim;
  static const int D = PDE::D; // physical dimensions
  static const int N = PDE::N; // total solution variables

  static const int NBC = N;         // total BCs

  static const int iSens = N - 1;

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  typedef DLA::VectorS<D+1, Real> VectorXT;

  BCmitAVSensor1D( const PDE& pde ) : pde_(pde) {}

  BCmitAVSensor1D( const PDE& pde, const PyDict& d ) : pde_(pde) {}

  ~BCmitAVSensor1D() {}

  BCmitAVSensor1D( const BCmitAVSensor1D& ) = delete;
  BCmitAVSensor1D& operator=( const BCmitAVSensor1D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return pde_.hasFluxViscous(); }

  // BC data
  template <class Tp, class T>
  void state( const Tp& param, const Real& x, const Real& time,
              const Real& nx, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    qB = qI;
  }

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormalSpaceTime( const Tp& param,
                            const Real& x, const Real& time,
                            const Real& nx, const Real& nt,
                            const ArrayQ<T>& qI,
                            const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
                            const ArrayQ<T>& qB,
                            ArrayQ<Tf>& Fn) const
  {
    // Add advective flux
    ArrayQ<Tf> fx = 0, ft = 0;
    pde_.fluxAdvective(param, x, time, qI, fx);
    pde_.masterState(qI, ft);

    // Add viscous flux
//    pde_.fluxViscousSpaceTime(param, x, time, qI, qIx, qIt, fx, ft);

    Fn += fx*nx + ft*nt;

    //Robin condition for artificial viscosity
//    DLA::MatrixSymS<D+1,Real> H = exp(param); //generalized h-tensor
//
//    VectorXT n = {nx, nt};
//    Real hn = 0;
//    for (int i = 0; i < D+1; i++)
//      for (int j = 0; j < D+1; j++)
//        hn += n[i]*H(i,j)*n[j];
//
//    const Real nu_inf = 0.0;
//    Fn(iSens) = hn*(qI(iSens) - nu_inf);
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& nt, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
};
#endif

} //namespace SANS

#endif  // BCTWOPHASEARTIFICIALVISCOSITY1D_H
