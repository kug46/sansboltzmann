// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "DensityModel.h"

//Instantiate the CheckInput function
//PARAMETER_VECTOR_IMPL( DensityModelParams );

namespace SANS
{

void DensityModel_Constant_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.rho));
  d.checkUnknownInputs(allParams);
}
DensityModel_Constant_Params DensityModel_Constant_Params::params;

void
DensityModel_Constant::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "DensityModel_Constant<T>:" << std::endl;
  out << indent << "  rho_ = " << rho_ << std::endl;
}

/*--------------------------------------------------------------------------------*/

void DensityModel_Comp_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.rho_ref));
  allParams.push_back(d.checkInputs(params.C));
  d.checkUnknownInputs(allParams);
}
DensityModel_Comp_Params DensityModel_Comp_Params::params;

void
DensityModel_Comp::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "DensityModel_Comp<T>:" << std::endl;
  out << indent << "  rho_ref_ = " << rho_ref_ << std::endl;
  out << indent << "  C_ = " << C_ << std::endl;
  out << indent << "  pref_ = " << pref_ << std::endl;
}


}
