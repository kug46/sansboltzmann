// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOLUTIONFUNCTION_BUCKLEYLEVERETT1D_H
#define SOLUTIONFUNCTION_BUCKLEYLEVERETT1D_H

// BuckleyLeverett exact solutions

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "Topology/Dimension.h"

#include "tools/SANSnumerics.h"     // Real

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "pde/AnalyticFunction/Function1D.h"

namespace SANS
{

//-----------------------------------------------------------------------------------------------------------//
// solution: Contact discontinuity (saturation shock) travelling at a constant speed starting from x = x_init
template <class QType, class PDEClass>
class SolutionFunction_BuckleyLeverett1D_Shock
    : public Function1DVirtualInterface<SolutionFunction_BuckleyLeverett1D_Shock<QType,PDEClass>, PDEClass>
{
public:
  typedef PhysD1 PhysDim;

  template <class T>
  using ArrayQ = typename PDEClass::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDEClass::template MatrixQ<T>;

  SolutionFunction_BuckleyLeverett1D_Shock( const PDEClass& pde, const Real& Sw0L, const Real& Sw0R, const Real& xinit = 0.0 ) :
    pde_(pde), Sw0L_(Sw0L), Sw0R_(Sw0R), xinit_(xinit), Sw_upstream_(calc_upstream_Sw()) {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
    ArrayQ<T> q = 0.0;

    ArrayQ<T> q_upstream = 0.0, qL = 0.0, qR = 0.0;
    pde_.setDOFFrom(q_upstream, Sw_upstream_, "Sw");
    pde_.setDOFFrom(qL, Sw0L_, "Sw");
    pde_.setDOFFrom(qR, Sw0R_, "Sw");

    T VL = char_speed(qL);
    T Vshock = shock_speed(q_upstream, qR);

    if (x <= xinit_ + VL*time)
    { //To the left of initial shock location, so Sw = Sw0L_
      pde_.setDOFFrom(q, Sw0L_, "Sw");
    }
    else if (x > (xinit_ + VL*time) && x < xinit_ + Vshock*time)
    {
      //Use interval bisection to find the Sw that satisfies z(Sw) = (x-xinit_)/t - char_speed(Sw) = 0

      Real tol = 1e-8;
      int nIterMax = 30;
      bool sol_found = false;

      Real SwL = Sw0L_, SwR = Sw_upstream_;

      ArrayQ<T> qL = 0.0, qR = 0.0;
      pde_.setDOFFrom(qL, SwL, "Sw");
      pde_.setDOFFrom(qR, SwR, "Sw");

      T zL = (x-xinit_)/time - char_speed(qL);

      for (int iter = 0; iter < nIterMax; iter++)
      {
        Real Swmid = 0.5*(SwL + SwR);

        ArrayQ<T> qmid = 0.0;
        pde_.setDOFFrom(qmid, Swmid, "Sw");

        T zmid = (x-xinit_)/time - char_speed(qmid);

//        std::cout<<"iter "<<iter<<": ["<<SwL<<","<<Swmid<<","<<SwR<<"] : "<< zL << ", " << zmid<<std::endl;

        if (fabs(SwR - SwL) <= 0.5*tol)
        {
          q = qmid;
          sol_found = true;
          break; //Found a solution
        }

        if (zL*zmid >= 0)
        { //Solution is in right half interval
          SwL = Swmid;
          zL = zmid;
        }
        else
        { //Solution is in left half interval
          SwR = Swmid;
        }
      }

      if (!sol_found)
        SANS_DEVELOPER_EXCEPTION("SolutionFunction_BuckleyLeverett1D_Shock<QType, PDE>::operator() - "
                                 "Saturation solution not found!");

    }
    else
    { //To the right of shock, so Sw = Sw0R_
      pde_.setDOFFrom(q, Sw0R_, "Sw");
    }

    return q;
  }

  Real Sw_upstream() const { return Sw_upstream_; }

  template<class T>
  T shock_speed(const ArrayQ<T>& qL, const ArrayQ<T>& qR) const;

  template<class T>
  T char_speed(const ArrayQ<T>& q) const;

private:
  PDEClass pde_; //Buckley-Leverett PDE

  Real Sw0L_; //Constant saturation at left boundary
  Real Sw0R_; //Constant saturation in the domain (to the right of shock)
  Real xinit_; //Initial location of shock

  Real Sw_upstream_;

  Real calc_upstream_Sw() const;

};


template <class QType, class PDEClass>
template <class T>
inline T
SolutionFunction_BuckleyLeverett1D_Shock<QType, PDEClass>::shock_speed(const ArrayQ<T>& qL, const ArrayQ<T>& qR) const
{
  ArrayQ<T> fadvL = 0, fadvR = 0, fconsL = 0, fconsR = 0;
  Real dummy = 0.0;

  pde_.fluxAdvective(dummy, dummy, qL, fadvL );
  pde_.fluxAdvective(dummy, dummy, qR, fadvR );

  pde_.masterState(dummy, dummy, qL, fconsL );
  pde_.masterState(dummy, dummy, qR, fconsR );

  return (DLA::index(fadvL,0) - DLA::index(fadvR, 0))/(DLA::index(fconsL, 0) - DLA::index(fconsR, 0));
}

template <class QType, class PDEClass>
template <class T>
inline T
SolutionFunction_BuckleyLeverett1D_Shock<QType, PDEClass>::char_speed(const ArrayQ<T>& q) const
{
  MatrixQ<T> mtx = 0;
  Real dummy = 0.0;

  pde_.jacobianFluxAdvective(dummy, dummy, q, mtx );
  return DLA::index(mtx, 0, 0);
}

template <class QType, class PDEClass>
inline Real
SolutionFunction_BuckleyLeverett1D_Shock<QType, PDEClass>::calc_upstream_Sw() const
{
  /* Use interval bisection to find the saturation value just upstream of the shock: Sw_up
   * z(Sw) = char_speed(Sw) - shock_speed(Sw, Sw0R_)
   * Find Sw_up such that z(Sw_up) = 0
   */

  Real tol = 1e-10;
  int nIterMax = 50;
  Real SwL = Sw0L_, SwR = Sw0R_;

  ArrayQ<Real> qL = 0.0, qR = 0.0;
  pde_.setDOFFrom(qL, SwL, "Sw");
  pde_.setDOFFrom(qR, SwR, "Sw");

  Real zL = char_speed(qL) - shock_speed(qL, qR);

  for (int iter = 0; iter < nIterMax; iter++)
  {
    Real Swmid = 0.5*(SwL + SwR);

    ArrayQ<Real> qmid = 0.0;
    pde_.setDOFFrom(qmid, Swmid, "Sw");

    Real zmid = char_speed(qmid) - shock_speed(qmid, qR);

//    std::cout<<"iter "<<iter<<": ["<<SwL<<","<<Swmid<<","<<SwR<<"] : "<< zL << ", " << zmid<<std::endl;

    if (fabs(SwR - SwL) <= 0.5*tol)
      return Swmid;

    if (zL*zmid>=0)
    { //Solution is in right half interval
      SwL = Swmid;
      zL = zmid;
    }
    else
    { //Solution is in left half interval
      SwR = Swmid;
    }
  }

  SANS_DEVELOPER_EXCEPTION("SolutionFunction_BuckleyLeverett1D_Shock<QType, PDE>::calc_upstream_Sw() - "
                           "Upstream saturation solution not found!");
  return -1.0;
}

} // namespace SANS

#endif  // SOLUTIONFUNCTION_BUCKLEYLEVERETT1D_H
