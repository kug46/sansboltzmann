// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDEBUCKLEYLEVERETT_ARTIFICIALVISCOSITY1D_H
#define PDEBUCKLEYLEVERETT_ARTIFICIALVISCOSITY1D_H

// 1-D Buckley-Leverett PDE class with artificial viscosity

#include "tools/SANSnumerics.h"     // Real
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Exp.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include <cmath>              // sqrt
#include <iostream>
#include <string>
#include "tools/smoothmath.h"

#include "Topology/Dimension.h"

#include "PDEBuckleyLeverett1D.h"

#include "Field/Tuple/ParamTuple.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: Wrapper class for 1-D Buckley-Leverett with artificial viscosity
//
// equation: d/dt( phi Sw ) + d/dx( uT fw(Sw) ) - d/dx( -K (lambda_w*lambda_n)/(lambda_w + lambda_n)*pc_Sw/phi * dSw/dx ) = 0
// where:    fw(Sw) = lambda_w / (lambda_w + lambda_n)
//           lambda_w = krw/mu_w
//           lambda_n = krn/mu_n
//
// template parameters:
//   T                           solution DOF data type (e.g. double)
//   QType                       solution variable set (e.g. primitive)
//   RelPermModel                relative permeability model (assumes same model for both wetting and non-wetting phases)
//   CapillaryModel              capillary pressure model
//
// member functions:
//   .hasFluxAdvectiveTime        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//   .hasFluxViscous             T/F: PDE has viscous flux term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: dU/dQ
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .fluxViscous                viscous fluxes: Fv(Q, QX)
//   .diffusionViscous           viscous diffusion coefficient: d(Fv)/d(UX)
//   .isValidState               T/F: determine if state is physically valid (e.g. Sw > 0)
//   .setDOFFrom                 set from primitive variable array
//----------------------------------------------------------------------------//

template <template <class> class PDETraitsSize, class PDETraitsModel>
class PDEBuckleyLeverett_ArtificialViscosity1D : public PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel>
{
public:
  typedef PDEBuckleyLeverett1D<PDETraitsSize, PDETraitsModel> BaseType;

  typedef PhysD1 PhysDim;
  static const int D = PDETraitsSize<PhysDim>::D; // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N; // total solution variables

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>; // solution/residual arrays

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>; // matrices

  template <class T>
  using MatrixParam = T;         // e.g. jacobian of PDEs w.r.t. parameters

  using BaseType::qInterpreter_;
  using BaseType::phi_;
  using BaseType::kr_;
  using BaseType::mu_w_;
  using BaseType::mu_n_;
  using BaseType::K_;
  using BaseType::pc_;

  // Constructor forwards arguments to PDE class using varargs
  template< class... PDEArgs >
  PDEBuckleyLeverett_ArtificialViscosity1D(const int order, const bool hasSpaceTimeDiffusion, PDEArgs&&... args) :
    BaseType(std::forward<PDEArgs>(args)...),
    order_(order), hasSpaceTimeDiffusion_(hasSpaceTimeDiffusion), C_artvisc_(1.0)
  {
    //Nothing
  }

//  PDEBuckleyLeverett_ArtificialViscosity1D( const Real& phi, const Real& uT, const RelPermModel& kr, const Real& mu_w, const Real& mu_n,
//                                            const Real& K, const CapillaryModel& pc, const int order, const Real C_artvisc = 1.0 ) :
//                                              BaseType(phi, uT, kr, mu_w, mu_n, K, pc), force_(nullptr), order_(order), C_artvisc_(C_artvisc) {}
//
//  PDEBuckleyLeverett_ArtificialViscosity1D( const Real& phi, const Real& uT, const RelPermModel& kr, const Real& mu_w, const Real& mu_n,
//                                            const Real& K, const CapillaryModel& pc, const std::shared_ptr<ForcingFunctionType>& force,
//                                            const int order, const Real C_artvisc = 1.0 ) :
//                                              BaseType(phi, uT, kr, mu_w, mu_n, K, pc), force_(force), order_(order), C_artvisc_(C_artvisc) {}

  ~PDEBuckleyLeverett_ArtificialViscosity1D() {}

  PDEBuckleyLeverett_ArtificialViscosity1D& operator=( const PDEBuckleyLeverett_ArtificialViscosity1D& ) = delete;

  // flux components
  bool hasFluxViscous() const { return true; }

  int PDEorder() const { return order_; }

  // unsteady temporal flux: Ft(Q)
  template <class Tp, class T, class Tf>
  void fluxAdvectiveTime(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tf>& ft ) const
  {
    BaseType::fluxAdvectiveTime(x, time, q, ft);
  }

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <class Tp, class T, class Tf>
  void jacobianFluxAdvectiveTime(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& J ) const
  {
    BaseType::jacobianFluxAdvectiveTime(x, time, q, J);
  }

  // master state: U(Q)
  template <class Tp, class Tq, class Tu>
  void masterState(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, ArrayQ<Tu>& uCons ) const
  {
    BaseType::masterState(x, time, q, uCons);
  }

  // jacobian of master state wrt q: dU(Q)/dQ
  template<class Tp, class T>
  void jacobianMasterState(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
  {
    BaseType::jacobianMasterState( x, time, q, dudq);
  }

  // strong form of unsteady conservative flux: dU(Q)/dt
  template <class Tp, class T>
  void strongFluxAdvectiveTime(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& strongPDE ) const
  {
    BaseType::strongFluxAdvectiveTime(x, time, q, qt, strongPDE);
  }

  // advective flux: F(Q)
  template <class Tp, class Tq, class Tf>
  void fluxAdvective(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, ArrayQ<Tf>& f ) const
  {
    BaseType::fluxAdvective(x, time, q, f);
  }

  template <class Tp, class Tq, class Tf>
  void fluxAdvectiveUpwind(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx,  ArrayQ<Tf>& f ) const
  {
    BaseType::fluxAdvectiveUpwind(x, time, qL, qR, nx, f);
  }

  template <class Tp, class Tq, class Tf>
  void fluxAdvectiveUpwindSpaceTime(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const
  {
    BaseType::fluxAdvectiveUpwindSpaceTime(x, time, qL, qR, nx, nt, f);
  }

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class Tp, class Tq, class Tf>
  void jacobianFluxAdvective(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, MatrixQ<Tf>& ax ) const
  {
    BaseType::jacobianFluxAdvective( x, time, q, ax );
  }

  // absolute value of advective flux jacobian: |n . d(F)/d(U)|
  template <class Tp, class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const Real& nx, MatrixQ<Tf>& mtx ) const
  {
    BaseType::jacobianFluxAdvectiveAbsoluteValue( x, time, q, nx, mtx );
  }

  // absolute value of advective flux jacobian: |n . d(F)/d(U)|
  template <class Tp, class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const Real& nx, const Real& nt, MatrixQ<Tf>& mtx ) const
  {
    BaseType::jacobianFluxAdvectiveAbsoluteValueSpaceTime( x, time, q, nx, mtx );
  }

  // strong form advective fluxes
  template <class Tp, class Tq, class Tg, class Tf>
  void strongFluxAdvective(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, ArrayQ<Tf>& strongPDE ) const
  {
    BaseType::strongFluxAdvective( x, time, q, qx, strongPDE );
  }

  // viscous flux: Fv(Q, QX)
  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscous(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, ArrayQ<Tf>& f ) const;

  // space-time viscous flux: Fv(Q, QX)
  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& ft ) const;

  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscous(
      const Tp& paramL, const Tp& paramR, const Real& x, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
      const Real& nx, ArrayQ<Tf>& f ) const;

  // space-time viscous flux: Fv(Q, QX)
  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Tp& paramL, const Tp& paramR, const Real& x, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qtL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qtR,
      const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const;

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class T, class Th, class Ts, class Tg, class Tf>
  void diffusionViscous(
      const ParamTuple<Th, Ts, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Tf>& kxx ) const;

  // space-time viscous diffusion matrix: d(Fv)/d(UX)
  template <class Th, class Ts, class Tq, class Tg, class Tk>
  void diffusionViscousSpaceTime(
      const ParamTuple<Th, Ts, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxt,
      MatrixQ<Tk>& ktx, MatrixQ<Tk>& ktt ) const;

  // jacobian of viscous diffusion matrix
  template <class T, class Th, class Tg, class Tf>
  void jacobianDiffusionViscous(
      const ParamTuple<Th, Tg, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& kxx_u ) const;

  // strong form viscous fluxes
  template <class T, class L, class R>
  void strongFluxViscous(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qxx, ArrayQ<T>& strongPDE ) const;

  // solution-dependent source: S(X, Q, QX)
  template <class Tp, class Tq, class Tg, class Ts>
  void source(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Ts>& source ) const
  {
    BaseType::source( x, time, q, qx, source );
  }

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX)
  template <class Tp, class Tlq, class Tq, class Tg, class Ts>
  void source(
      const Tp& param, const Real& x, const Real& time,
      const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Ts>& source ) const
  {
    BaseType::source( x, time, lifted_quantity, q, qx, source );
  }

  // dual-consistent source
  template <class Tp, class Tq, class Tg, class Ts>
  void sourceTrace(
      const Tp& paramL, const Real& xL,
      const Tp& paramR, const Real& xR,
      const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
  {
    BaseType::sourceTrace(xL, xR, time, qL, qxL, qR, qxR, sourceL, sourceR);
  }

  // trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
  template <class Tp, class Tq, class Ts>
  void sourceLiftedQuantity(
      const Tp& paramL, const Real& xL,
      const Tp& paramR, const Real& xR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, Ts& s ) const
  {
    BaseType::sourceLiftedQuantity(xL, xR, time, qL, qR, s);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tp, class Tq, class Tg, class Ts>
  void jacobianSource(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Ts>& dsdu ) const
  {
    BaseType::jacobianSource(x, time, q, qx, dsdu);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tp, class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Ts>& dsdu ) const
  {
    BaseType::jacobianSourceAbsoluteValue(x, time, q, qx, dsdu);
  }

  // right-hand-side forcing function
  template <class Tp, class Tf>
  void forcingFunction( const Tp& param, const Real& x, const Real& time,
                        ArrayQ<Tf>& forcing ) const
  {
    BaseType::forcingFunction(x, time, forcing );
  }

  // characteristic speed (needed for timestep)
  template <class T, class L, class R>
  void speedCharacteristic(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
      const Real& dx, const ArrayQ<T>& q, T& speed ) const
  {
    BaseType::speedCharacteristic(x, time, dx, q, speed);
  }

  // characteristic speed
  template <class Tp, class Tq, class Ts>
  void speedCharacteristic(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, Ts& speed ) const
  {
    BaseType::speedCharacteristic(x, time, q, speed);
  }

  // update fraction needed for physically valid state
  template <class Tp>
  void updateFraction(
      const Tp& param, const Real& x, const Real& time, const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
      const Real maxChangeFraction, Real& updateFraction ) const
  {
    BaseType::updateFraction(x, time, q, dq, maxChangeFraction, updateFraction);
  }

  template <class Tp, class Tq, class Tg, class Tf>
  void artViscMax(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      Tf& nu_max ) const;

//  // is state physically valid
//  using BaseType::isValidState;
//
//  // set from primitive variable array
//  using BaseType::setDOFFrom;
//
//  // interpret residuals of the solution variable
//  using BaseType::interpResidVariable;
//
//  // interpret residuals of the gradients in the solution variable
//  using BaseType::interpResidGradient;
//
//  // interpret residuals at the boundary (should forward to BCs)
//  using BaseType::interpResidBC;
//
//  // how many residual equations are we dealing with
//  using BaseType::nMonitor;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

  int getOrder() const { return order_; };

protected:

  // Artificial viscosity flux - artificial viscosity variable in augmented state vector
  template <class Tp, class Tq, class Tg, class Tf>
  void artificialViscosityFlux(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Tf>& f ) const;

  // Artificial viscosity flux - sensor parameter
  template <class Th, class Ts, class Tq, class Tg, class Tf>
  void artificialViscosityFlux(
      const ParamTuple<Th, Ts, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Tf>& fx ) const;

  // Space-time artificial viscosity flux - artificial viscosity variable in augmented state vector
  template <class Tp, class Tq, class Tg, class Tf>
  void artificialViscosityFluxSpaceTime(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& ft ) const;

  // Space-time artificial viscosity flux - sensor parameter
  template <class Th, class Ts, class Tq, class Tg, class Tf>
  void artificialViscosityFluxSpaceTime(
      const ParamTuple<Th, Ts, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& ft ) const;

  // artificial viscosity calculation with spatial h-tensor
  template <class T, class Th, class Tg, class Ts>
  void artificialViscosity( const Real& x, const Real& time, const ArrayQ<T>& q, const DLA::MatrixSymS<D,Th>& logH, const Tg& sensor,
                            MatrixQ<Ts>& Kxx ) const;

  // artificial viscosity calculation with space-time h-tensor
  template <class T, class Th, class Tg, class Ts>
  void artificialViscosity( const Real& x, const Real& time, const ArrayQ<T>& q, const DLA::MatrixSymS<D+1,Th>& logH, const Tg& sensor,
                            MatrixQ<Ts>& Kxx ) const;

  template <class T, class Th, class Tg, class Ts>
  void artificialViscositySpaceTime( const Real& x, const Real& time, const ArrayQ<T>& q,
                                     const DLA::MatrixSymS<D+1,Th>& logH, const Tg& sensor,
                                     MatrixQ<Ts>& Kxx, MatrixQ<Ts>& Kxt, MatrixQ<Ts>& Ktx, MatrixQ<Ts>& Ktt ) const;

  const int order_;
  const bool hasSpaceTimeDiffusion_;
  const Real C_artvisc_;
};

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDEBuckleyLeverett_ArtificialViscosity1D<PDETraitsSize, PDETraitsModel>::artificialViscosityFlux(
    const Tp& param, const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    ArrayQ<Tf>& fx ) const
{
  SANS_ASSERT(N == 2); //Check if the state vector has been augmented with the sensor

  typedef typename promote_Surreal<Tq,Tg>::type T;

  Tq nu = q(N-1); //artificial viscosity
  const Real alpha = 40.0;
  nu = smoothmax(nu, Tq(0.0), alpha); //ensure nu is non-negative (since it can go negative in under-resolved regions)

  T Swx = 0;
  qInterpreter_.eval_SwGradient( q, qx, Swx );

  fx(0) -= nu*Swx;
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Th, class Ts, class Tq, class Tg, class Tf>
inline void
PDEBuckleyLeverett_ArtificialViscosity1D<PDETraitsSize, PDETraitsModel>::artificialViscosityFlux(
    const ParamTuple<Th, Ts, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    ArrayQ<Tf>& fx ) const
{
  SANS_ASSERT(N == 1); //Check if the state vector contains only the saturation

  typedef typename promote_Surreal<Tq,Tg>::type T;

  MatrixQ<Tf> nu_xx = 0.0;
  MatrixQ<Tq> dudq = 0; // just for testing

  const Th& Hparam = param.left();
  const Ts& sensor = param.right();

  // Add artificial viscosity
  artificialViscosity( x, time, q, Hparam, sensor, nu_xx );

  BaseType::jacobianMasterState( x, time, q, dudq );
  ArrayQ<T> ux = dudq*qx;

  fx -= nu_xx*ux;
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDEBuckleyLeverett_ArtificialViscosity1D<PDETraitsSize, PDETraitsModel>::artificialViscosityFluxSpaceTime(
    const Tp& param, const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
    ArrayQ<Tf>& fx, ArrayQ<Tf>& ft ) const
{
  SANS_ASSERT(N == 2); //Check if the state vector has been augmented with the sensor

  typedef typename promote_Surreal<Tq,Tg>::type T;

  Tq nu = q(N-1); //artificial viscosity
  const Real alpha = 40.0;
  nu = smoothmax(nu, Tq(0.0), alpha); //ensure nu is non-negative (since it can go negative in under-resolved regions)

  T Swx = 0, Swt = 0;
  qInterpreter_.eval_SwGradient( q, qx, Swx );
  qInterpreter_.eval_SwGradient( q, qt, Swt );

  fx(0) -= nu*Swx;
  ft(0) -= nu*Swt;
}

template<template <class> class PDETraitsSize, class PDETraitsModel>
template <class Th, class Ts, class Tq, class Tg, class Tf>
inline void
PDEBuckleyLeverett_ArtificialViscosity1D<PDETraitsSize, PDETraitsModel>::artificialViscosityFluxSpaceTime(
    const ParamTuple<Th, Ts, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt, ArrayQ<Tf>& fx, ArrayQ<Tf>& ft ) const
{
  SANS_ASSERT(N == 1); //Check if the state vector contains only the saturation

  typedef typename promote_Surreal<Tq,Tg>::type T;

  MatrixQ<Tf> nu_xx = 0.0, nu_xt = 0.0;
  MatrixQ<Tf> nu_tx = 0.0, nu_tt = 0.0;
  MatrixQ<Tq> dudq = 0;

  const Th& Hparam = param.left();
  const Ts& sensor = param.right();

  // Add space-time artificial viscosity
  artificialViscositySpaceTime( x, time, q, Hparam, sensor,
                                nu_xx, nu_xt, nu_tx, nu_tt );

  BaseType::jacobianMasterState( x, time, q, dudq );
  ArrayQ<T> ux = dudq*qx;
  ArrayQ<T> ut = dudq*qt;

  fx -= nu_xx*ux + nu_xt*ut;
  ft -= nu_tx*ux + nu_tt*ut;
}

// Artificial viscosity calculation with spatial h-tensor
template<template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Th, class Tg, class Ts>
void inline
PDEBuckleyLeverett_ArtificialViscosity1D<PDETraitsSize, PDETraitsModel>::
artificialViscosity( const Real& x, const Real& time, const ArrayQ<T>& q, const DLA::MatrixSymS<D,Th>& logH, const Tg& sensor,
                     MatrixQ<Ts>& Kxx ) const
{
  T lambda;
  BaseType::speedCharacteristic( x, time, q, lambda );
//  lambda = fabs(BaseType::totalVelocity())/BaseType::porosity();

  Ts factor = C_artvisc_/(Real(order_)+1) * lambda * smoothabs0(sensor, 1.0e-5);

  DLA::MatrixSymS<D,Th> H = exp(logH); //generalized h-tensor

  Kxx += factor*H(0,0);
}

// Artificial viscosity calculation with space-time h-tensor
template<template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Th, class Tg, class Ts>
void inline
PDEBuckleyLeverett_ArtificialViscosity1D<PDETraitsSize, PDETraitsModel>::
artificialViscosity( const Real& x, const Real& time, const ArrayQ<T>& q, const DLA::MatrixSymS<D+1,Th>& logH, const Tg& sensor,
                     MatrixQ<Ts>& Kxx ) const
{
  T lambda;
  BaseType::speedCharacteristic( x, time, q, lambda );
//  lambda = fabs(BaseType::totalVelocity())/BaseType::porosity();

  Ts factor = C_artvisc_/(Real(order_)+1) * lambda * smoothabs0(sensor, 1.0e-5);

  DLA::MatrixSymS<D+1,Th> H = exp(logH); //generalized h-tensor

  Kxx += factor*H(0,0);
}

// Space-time artificial viscosity calculation with space-time h-tensor
template<template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Th, class Tg, class Ts>
void inline
PDEBuckleyLeverett_ArtificialViscosity1D<PDETraitsSize, PDETraitsModel>::
artificialViscositySpaceTime( const Real& x, const Real& time, const ArrayQ<T>& q,
                              const DLA::MatrixSymS<D+1,Th>& logH, const Tg& sensor,
                              MatrixQ<Ts>& Kxx, MatrixQ<Ts>& Kxt, MatrixQ<Ts>& Ktx, MatrixQ<Ts>& Ktt ) const
{
  T lambda;

  BaseType::speedCharacteristic( x, time, q, lambda );
//  lambda = fabs(BaseType::totalVelocity())/BaseType::porosity();

  Ts factor = C_artvisc_/(Real(order_)+1) * smoothabs0(sensor, 1.0e-5);

  DLA::MatrixSymS<D+1,Th> H = exp(logH); //generalized h-tensor

//  Th hbar = pow(DLA::Det(H), 1.0/(Real)(D+1));

  Kxx += factor*H(0,0)*lambda; // [length^2 / time]

  Kxt += factor*H(0,1)*sqrt(lambda); // [length]
  Ktx += factor*H(1,0)*sqrt(lambda); // [length]
  Ktt += factor*H(1,1); // [time]
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDEBuckleyLeverett_ArtificialViscosity1D<PDETraitsSize, PDETraitsModel>::artViscMax(
    const Tp& param, const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    Tf& nu_max ) const
{
  Tq vx = 0;
  BaseType::speedCharacteristic(x, time, q, vx);
//  vx = fabs(BaseType::totalVelocity())/BaseType::porosity();

  Tp H = exp(param); //param is either MatrixSymS<D,Th> or MatrixSymS<D+1,Th> of log(H)
  Tp Hsq = H*H; //H is a MatrixSymS<D,Real> or MatrixSymS<D+1,Real>

  Tf vT_Hsq_v = vx*Hsq(0,0)*vx;
  Tf v_h = sqrt(vT_Hsq_v);

  const Real Pe = 2.0; //required grid Peclet number

  nu_max = 0; //maximum artificial viscosity (when sensor is 1)

  if (order_ > 0) //don't need artificial viscosity for P0
    nu_max = (v_h / order_) / Pe;
}

// viscous flux
template<template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDEBuckleyLeverett_ArtificialViscosity1D<PDETraitsSize, PDETraitsModel>::fluxViscous(
    const Tp& param, const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, ArrayQ<Tf>& f ) const
{
  SANS_ASSERT(!hasSpaceTimeDiffusion_); //space-time diffusion requires a space-time NDPDE

  artificialViscosityFlux(param, x, time, q, qx, f);
  BaseType::fluxViscous( x, time, q, qx, f );
}

// space-time viscous flux
template<template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDEBuckleyLeverett_ArtificialViscosity1D<PDETraitsSize, PDETraitsModel>::fluxViscousSpaceTime(
    const Tp& param, const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt, ArrayQ<Tf>& fx, ArrayQ<Tf>& ft ) const
{
  if (hasSpaceTimeDiffusion_)
    artificialViscosityFluxSpaceTime(param, x, time, q, qx, qt, fx, ft);
  else
    artificialViscosityFlux(param, x, time, q, qx, fx);

  BaseType::fluxViscousSpaceTime( x, time, q, qx, qt, fx, ft );
}

// viscous flux: normal flux with left and right states
template<template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDEBuckleyLeverett_ArtificialViscosity1D<PDETraitsSize, PDETraitsModel>::fluxViscous(
    const Tp& paramL, const Tp& paramR, const Real& x, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
    const Real& nx, ArrayQ<Tf>& fn ) const
{
  SANS_ASSERT(!hasSpaceTimeDiffusion_); //space-time diffusion requires a space-time NDPDE

  ArrayQ<Tf> fxL = 0, fxR = 0;

  artificialViscosityFlux(paramL, x, time, qL, qxL, fxL);
  artificialViscosityFlux(paramR, x, time, qR, qxR, fxR);

  //Compute the average artificial viscosity flux from both sides
  fn += 0.5*(fxL + fxR)*nx;

  //Add on any interface viscous fluxes from the base class
  BaseType::fluxViscous( x, time, qL, qxL, qR, qxR, nx, fn );
}

// space-time viscous flux: normal flux with left and right states
template<template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDEBuckleyLeverett_ArtificialViscosity1D<PDETraitsSize, PDETraitsModel>::fluxViscousSpaceTime(
    const Tp& paramL, const Tp& paramR, const Real& x, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qtL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qtR,
    const Real& nx, const Real& nt, ArrayQ<Tf>& fn ) const
{
  ArrayQ<Tf> fxL = 0, fxR = 0;
  ArrayQ<Tf> ftL = 0, ftR = 0;

  if (hasSpaceTimeDiffusion_)
  {
    artificialViscosityFluxSpaceTime(paramL, x, time, qL, qxL, qtL, fxL, ftL);
    artificialViscosityFluxSpaceTime(paramR, x, time, qR, qxR, qtR, fxR, ftR);
  }
  else
  {
    artificialViscosityFlux(paramL, x, time, qL, qxL, fxL);
    artificialViscosityFlux(paramR, x, time, qR, qxR, fxR);
  }

  //Compute the average artificial viscosity flux from both sides
  fn += 0.5*(fxL + fxR)*nx + 0.5*(ftL + ftR)*nt;

  //Add on any interface viscous fluxes from the base class
  BaseType::fluxViscousSpaceTime( x, time, qL, qxL, qtL, qR, qxR, qtR, nx, nt, fn );
}

// viscous diffusion matrix: d(Fv)/d(UX)
template<template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Th, class Ts, class Tg, class Tf>
inline void
PDEBuckleyLeverett_ArtificialViscosity1D<PDETraitsSize, PDETraitsModel>::diffusionViscous(
    const ParamTuple<Th, Ts, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    MatrixQ<Tf>& kxx ) const
{
  SANS_ASSERT(!hasSpaceTimeDiffusion_); //space-time diffusion requires a space-time NDPDE

  const Th& Hparam = param.left();
  const Ts& sensor = param.right();

  // Add artificial viscosity
  artificialViscosity( x, time, q, Hparam, sensor, kxx );

  // Add the base class viscosity
  BaseType::diffusionViscous( x, time, q, qx, kxx );
}

// space-time viscous diffusion matrix: d(Fv)/d(UX)
template<template <class> class PDETraitsSize, class PDETraitsModel>
template <class Th, class Ts, class Tq, class Tg, class Tk>
inline void
PDEBuckleyLeverett_ArtificialViscosity1D<PDETraitsSize, PDETraitsModel>::diffusionViscousSpaceTime(
    const ParamTuple<Th, Ts, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
    MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxt,
    MatrixQ<Tk>& ktx, MatrixQ<Tk>& ktt ) const
{
  const Th& Hparam = param.left();
  const Ts& sensor = param.right();

  // Add space-time artificial viscosity
  if (hasSpaceTimeDiffusion_)
    artificialViscositySpaceTime( x, time, q, Hparam, sensor, kxx, kxt, ktx, ktt );
  else
    artificialViscosity( x, time, q, Hparam, sensor, kxx );

  // Add the base class viscosity
  BaseType::diffusionViscousSpaceTime( x, time, q, qx, qt, kxx, kxt, ktx, ktt );
}

// jacobian of viscous diffusion matrix: d (d(Fv)/d(ux))/d(u)
template<template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Th, class Tg, class Tf>
inline void
PDEBuckleyLeverett_ArtificialViscosity1D<PDETraitsSize, PDETraitsModel>::jacobianDiffusionViscous(
    const ParamTuple<Th, Tg, TupleClass<>>& param, const Real& x, const Real& time, const ArrayQ<T>& q, MatrixQ<Tf>& kxx_u ) const
{
  SANS_ASSERT(!hasSpaceTimeDiffusion_); //space-time diffusion requires a space-time NDPDE
  SANS_DEVELOPER_EXCEPTION("PDEBuckleyLeverett_ArtificialViscosity1D::jacobianDiffusionViscous - Not implemented.");
//  BaseType::jacobianDiffusionViscous( x, time, q, kxx_u );
}

// strong form of viscous flux: d(Fv)/d(X)
template<template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class L, class R>
inline void
PDEBuckleyLeverett_ArtificialViscosity1D<PDETraitsSize, PDETraitsModel>::strongFluxViscous(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qxx,
    ArrayQ<T>& strongPDE ) const
{
  SANS_DEVELOPER_EXCEPTION("PDEBuckleyLeverett_ArtificialViscosity1D::strongFluxViscous - Not implemented.");
//  BaseType::strongFluxViscous( x, time, q, qx, qxx, strongPDE );
}

template<template <class> class PDETraitsSize, class PDETraitsModel>
void
PDEBuckleyLeverett_ArtificialViscosity1D<PDETraitsSize, PDETraitsModel>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  std::string indent2(indentSize+2, ' ');
  out << indent << "PDEBuckleyLeverett_ArtificialViscosity1D:" << std::endl;
  out << indent2 << "order_ = " << order_ << std::endl;
  out << indent2 << "hasSpaceTimeDiffusion_ = " << hasSpaceTimeDiffusion_ << std::endl;
  out << indent2 << "C_artvisc_ = " << C_artvisc_ << std::endl;
  out << indent2 << "pde_ = " << std::endl;
  BaseType::dump(indentSize+4, out);
}

} //namespace SANS

#endif  // PDEBUCKLEYLEVERETT_ARTIFICIALVISCOSITY1D_H
