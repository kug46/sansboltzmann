// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDETWOPHASELINEARIZED1D_H
#define PDETWOPHASELINEARIZED1D_H

// 1-D Linearized two-phase flow PDE class

#include "tools/SANSnumerics.h"     // Real
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include <iostream>
#include <string>
#include "tools/smoothmath.h"

#include "Topology/Dimension.h"

#include "TraitsTwoPhase.h"
#include "SourceTwoPhase1D.h"
#include "TwoPhaseVariable.h"

//#define CONSERVATIVE_FLUX_HACK

namespace SANS
{

// forward declare: solution variables interpreter
template <class QType, class CapillaryModel, template <class> class PDETraitsSize>
class Q1D;

//----------------------------------------------------------------------------//
// PDE class: 1-D Linearized Two Phase
//
// equations: d/dt( rho_w phi Sw ) - d/dx( rho_w K lambda_w dpw/dx ) = rho_w q_w
//            d/dt( rho_n phi Sn ) - d/dx( rho_n K lambda_n dpn/dx ) = rho_n q_n
//
// where:    lambda_w = krw/mu_w
//           lambda_n = krn/mu_n
//
// template parameters:
//   T                        solution DOF data type (e.g. double)
//   QType                    solution variable set (e.g. primitive)
//   DensityModel             density models (for each phase)
//   PorosityModel            porosity model
//   RelPermModel             relative permeability models (for each phase)
//   ViscModel                viscosity models (for each phase)
//   CapillaryModel           capillary pressure model
//
// member functions:
//   .hasFluxAdvectiveTime        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//   .hasFluxViscous             T/F: PDE has viscous flux term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: dU/dQ
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .fluxViscous                viscous fluxes: Fv(Q, QX)
//   .diffusionViscous           viscous diffusion coefficient: d(Fv)/d(UX)
//   .isValidState               T/F: determine if state is physically valid (e.g. Sw > 0)
//   .setDOFFrom                 set from primitive variable array
//----------------------------------------------------------------------------//

template <class QType, class RelPermModelw, class RelPermModeln, class CapillaryModel>
class PDETwoPhaseLinearized1D
{
public:
  typedef PhysD1 PhysDim;
  static const int D = TraitsSizeTwoPhase<PhysDim>::D; // physical dimensions
  static const int N = TraitsSizeTwoPhase<PhysDim>::N; // total solution variables

  template <class T>
  using ArrayQ = TraitsSizeTwoPhase<PhysDim>::template ArrayQ<T>;   // solution/residual arrays

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using MatrixQ = TraitsSizeTwoPhase<PhysDim>::template MatrixQ<T>; // matrices

  typedef Q1D<QType, CapillaryModel, TraitsSizeTwoPhase> QInterpreter;  // solution variable interpreter type

  PDETwoPhaseLinearized1D( const Real& rhow, const Real& rhon, const Real& phi,
                           const RelPermModelw& krw, const RelPermModeln& krn,
                           const Real& muw, const Real& mun,
                           const Real& K, const CapillaryModel& pc,
                           const Real& pxbar, const Real& Swbar) :
                             rhow_(rhow), rhon_(rhon), phi_(phi),
                             krw_(krw), krn_(krn), muw_(muw), mun_(mun),
                             K_(K), pc_(pc), pxbar_(pxbar), Swbar_(Swbar),
                             Snbar_(1.0 - Swbar_), qInterpreter_(pc_) {}

  PDETwoPhaseLinearized1D( const PDETwoPhaseLinearized1D& pde ) :
      rhow_(pde.rhow_), rhon_(pde.rhon_), phi_(pde.phi_),
      krw_(pde.krw_), krn_(pde.krn_), muw_(pde.muw_), mun_(pde.mun_),
      K_(pde.K_), pc_(pde.pc_), pxbar_(pde.pxbar_), Swbar_(pde.Swbar_),
      Snbar_(pde.Snbar_), qInterpreter_(pde.qInterpreter_) {}

  ~PDETwoPhaseLinearized1D() {}

  PDETwoPhaseLinearized1D& operator=( const PDETwoPhaseLinearized1D& );

  // flux components
  bool hasFluxAdvectiveTime() const { return true; }
  bool hasFluxAdvective() const { return true; }
  bool hasFluxViscous() const { return true; }
  bool hasSource() const { return false; }
  bool hasSourceTrace() const { return false; }
  bool hasSourceLiftedQuantity() const { return false; }
  bool hasForcingFunction() const { return false; }

  // viscous flux is of the form K(q).gradu
  bool fluxViscousLinearInGradient() const { return true; }
  bool needsSolutionGradientforSource() const { return false; }

  Real permeability() const { return K_; }

  const QInterpreter& variableInterpreter() const { return qInterpreter_; }

  // unsteady temporal flux: Ft(Q)
  template <class T>
  void fluxAdvectiveTime(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, ArrayQ<T>& ft ) const
  {
    ArrayQ<T> ftLocal = 0;
    masterState(x, time, q, ftLocal);
    ft += ftLocal;
  }

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <class T>
  void jacobianFluxAdvectiveTime(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& J ) const
  {
    J += DLA::Identity();
  }

  // unsteady conservative flux: U(Q)
  template <class T, class Tu>
  void masterState(
      const Real& x, const Real& time,const ArrayQ<T>& q, ArrayQ<Tu>& uCons ) const;

  // unsteady conservative flux Jacobian: dU(Q)/dQ
  template<class T>
  void jacobianMasterState(
      const Real& x, const Real& time,const ArrayQ<T>& q, MatrixQ<T>& dudq ) const;

  // advective flux: F(Q)
  template <class Tq, class Tf>
  void fluxAdvective(
      const Real& x, const Real& time, const ArrayQ<Tq>& q, ArrayQ<Tf>& f ) const;

  template <class Tq, class Tf>
  void fluxAdvectiveUpwind(
      const Real& x, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx,  ArrayQ<Tf>& f ) const;

  template <class Tq, class Tf>
  void fluxAdvectiveUpwindSpaceTime(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const;

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class T>
  void jacobianFluxAdvective(
      const Real& x, const Real& time, const ArrayQ<T>& q, MatrixQ<T>& ax ) const;

  // absolute value of advective flux jacobian: |n . d(F)/d(U)|
  template <class T>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Real& x, const Real& time, const ArrayQ<T>& q, const Real& nx, MatrixQ<T>& mtx ) const {}

  // absolute value of advective flux jacobian: |n . d(F)/d(U)|
  template <class T>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Real& x, const Real& time, const ArrayQ<T>& q, const Real& nx, const Real& nt, MatrixQ<T>& mtx ) const {}

  // viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Tf>& f ) const;

  // space-time viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const;

  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
      const Real& nx, ArrayQ<Tf>& fn ) const;

  // space-time viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qtL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qtR,
      const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const;

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Tk>& kxx ) const;

  // space-time viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscousSpaceTime(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxt,
      MatrixQ<Tk>& ktx, MatrixQ<Tk>& ktt ) const;

  // solution-dependent source: S(X, Q, QX)
  template <class Tq, class Tg>
  void source(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ< typename promote_Surreal<Tq,Tg>::type >& source ) const;

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX)
  template <class Tlq, class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& time,
      const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Ts>& source ) const {}

  // dual-consistent source
  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& xL, const Real& xR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const {}

  // trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
  template <class Tq, class Ts>
  void sourceLiftedQuantity(
      const Real& xL, const Real& xR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, Ts& s ) const {}

  // right-hand-side forcing function
  template <class T>
  void forcingFunction( const Real& x, const Real& time, ArrayQ<T>& forcing ) const {}

  // characteristic speed (needed for timestep)
  template<class T>
  void speedCharacteristic( const Real& x, const Real& time, const Real& dx, const ArrayQ<T>& q, T& speed ) const;

  // update fraction needed for physically valid state
  void updateFraction( const Real& x, const Real& time, const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
                       const Real maxChangeFraction, Real& updateFraction ) const;

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const;

  // set from primitive variable array
  template<class T>
  void setDOFFrom( ArrayQ<T>& q, const Real data[], const std::string name[], int nn ) const;

  // set from variable
  template<class T, class Variables>
  void setDOFFrom( ArrayQ<T>& q, const TwoPhaseVariableType<Variables>& data ) const;

  // set from variable
  template<class Variables>
  ArrayQ<Real> setDOFFrom( const TwoPhaseVariableType<Variables>& data ) const
  {
    ArrayQ<Real> q;
    qInterpreter_.setFromPrimitive( q, data.cast() );
    return q;
  }

  // set from variable
  ArrayQ<Real> setDOFFrom( PyDict& d ) const
  {
    ArrayQ<Real> q;
    DictKeyPair data = d.get(TwoPhaseVariableTypeParams::params.StateVector);

    if ( data == TwoPhaseVariableTypeParams::params.StateVector.PressureNonWet_SaturationWet )
      qInterpreter_.setFromPrimitive( q, PressureNonWet_SaturationWet<Real>(data) );
    else
      SANS_DEVELOPER_EXCEPTION(" Unknown state vector option.");

    return q;
  }

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const;

  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const;

  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const;

  // how many residual equations are we dealing with
  int nMonitor() const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const Real rhow_; //Density model for wetting phase
  const Real rhon_; //Density model for non-wetting phase

  const Real phi_; //Porosity model

  const RelPermModelw& krw_; //Relative permeability model for wetting phase
  const RelPermModeln& krn_; //Relative permeability model for non-wetting phase

  const Real muw_; //Viscosity model for wetting phase
  const Real mun_; //Viscosity model for non-wetting phase

  Real K_; // Permeability (assumed to be constant for now)
  const CapillaryModel& pc_; //Capillary pressure model

  Real pxbar_; //Constant pressure gradient about which linearization is performed
  Real Swbar_; //Constant wetting saturation about which linearization is performed
  Real Snbar_; //Constant non-wetting saturation about which linearization is performed

  const QInterpreter qInterpreter_; // solution variable interpreter class
};

template <class QType, class RelPermModelw, class RelPermModeln, class CapillaryModel>
template <class T, class Tu>
inline void
PDETwoPhaseLinearized1D<QType, RelPermModelw, RelPermModeln, CapillaryModel>::
masterState(
    const Real& x, const Real& time,
    const ArrayQ<T>& q, ArrayQ<Tu>& uCons ) const
{
  T Sw = 0;
  qInterpreter_.eval_Sw( q, Sw );

  uCons(0) =  phi_*rhow_*Sw;
  uCons(1) = -phi_*rhon_*Sw;
}

template <class QType, class RelPermModelw, class RelPermModeln, class CapillaryModel>
template <class T>
inline void
PDETwoPhaseLinearized1D<QType, RelPermModelw, RelPermModeln, CapillaryModel>::
jacobianMasterState(
    const Real& x, const Real& time,
    const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
{
#ifdef CONSERVATIVE_FLUX_HACK
  dudq = DLA::Identity(); //Returning identity because diffusionViscous returns d(Fv)/dq instead of d(Fv)/d(UX)
#else
  //Evaluate jacobians (wrt state vector)
  ArrayQ<T> Sw_q = 0;
  qInterpreter_.jacobian_Sw( q, Sw_q );

  //Compute Jacobian
  dudq(0,0) =  phi_*rhow_*Sw_q[0];
  dudq(0,1) =  phi_*rhow_*Sw_q[1];
  dudq(1,0) = -phi_*rhon_*Sw_q[0];
  dudq(1,1) = -phi_*rhon_*Sw_q[1];
#endif
}

template <class QType, class RelPermModelw, class RelPermModeln, class CapillaryModel>
template <class Tq, class Tf>
inline void
PDETwoPhaseLinearized1D<QType, RelPermModelw, RelPermModeln, CapillaryModel>::
fluxAdvective(const Real& x, const Real& time,
              const ArrayQ<Tq>& q, ArrayQ<Tf>& f ) const
{
  Tq Sw = 0;
  qInterpreter_.eval_Sw( q, Sw );

  //Evaluate relative permeability jacobians at average state
  Real krw_Sw, krn_Sn;
  krw_.relativepermJacobian(Swbar_, krw_Sw);
  krn_.relativepermJacobian(Snbar_, krn_Sn);

  Real dSn_dSw = -1;
  Real krn_Sw = krn_Sn * dSn_dSw;

  //Evaluate phase mobility jacobians at average state
  Real lambda_w_Sw = krw_Sw/muw_;
  Real lambda_n_Sw = krn_Sw/mun_;

  f(0) += -rhow_*K_*lambda_w_Sw*Sw*pxbar_;
  f(1) += -rhon_*K_*lambda_n_Sw*Sw*pxbar_;
}

template <class QType, class RelPermModelw, class RelPermModeln, class CapillaryModel>
template <class Tq, class Tf>
inline void
PDETwoPhaseLinearized1D<QType, RelPermModelw, RelPermModeln, CapillaryModel>::
fluxAdvectiveUpwind(const Real& x, const Real& time,
                    const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
                    const Real& nx, ArrayQ<Tf>& fn ) const
{
  ArrayQ<Tq> fxL = 0, fxR = 0;

  fluxAdvective(x, time, qL, fxL);
  fluxAdvective(x, time, qR, fxR);

  //Average normal flux
  fn += 0.5*(fxL + fxR)*nx;
}

template <class QType, class RelPermModelw, class RelPermModeln, class CapillaryModel>
template <class Tq, class Tf>
inline void
PDETwoPhaseLinearized1D<QType, RelPermModelw, RelPermModeln, CapillaryModel>::
fluxAdvectiveUpwindSpaceTime(const Real& x, const Real& time,
                             const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
                             const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const
{
  //Full temporal upwinding

  //Compute upwinded spatial advective flux
  ArrayQ<Tq> fnx = 0;
  fluxAdvectiveUpwind( x, time, qL, qR, nx, fnx );

  //Upwind temporal flux (i.e. conservative flux)
  ArrayQ<Tq> ft = 0;
  if (nt >= 0)
    masterState( qL, ft );
  else
    masterState( qR, ft );

  f += ft*nt + fnx;
}

template <class QType, class RelPermModelw, class RelPermModeln, class CapillaryModel>
template <class T>
inline void
PDETwoPhaseLinearized1D<QType, RelPermModelw, RelPermModeln, CapillaryModel>::
jacobianFluxAdvective(const Real& x, const Real& time, const ArrayQ<T>& q, MatrixQ<T>& ax ) const
{
  T Sw = 0;
  qInterpreter_.eval_Sw( q, Sw );

  //Evaluate relative permeability jacobians at average state
  Real krw_Sw, krn_Sn;
  krw_.relativepermJacobian(Swbar_, krw_Sw);
  krn_.relativepermJacobian(Snbar_, krn_Sn);

  Real dSn_dSw = -1;
  Real krn_Sw = krn_Sn * dSn_dSw;

  //Evaluate phase mobility jacobians at average state
  Real lambda_w_Sw = krw_Sw/muw_;
  Real lambda_n_Sw = krn_Sw/mun_;

  ax(0,0) += 0.0;
  ax(0,1) += -rhow_*K_*lambda_w_Sw*pxbar_;
  ax(1,0) += 0.0;
  ax(1,1) += -rhon_*K_*lambda_n_Sw*pxbar_;
}

// viscous flux
template <class QType, class RelPermModelw, class RelPermModeln, class CapillaryModel>
template <class Tq, class Tg, class Tf>
inline void
PDETwoPhaseLinearized1D<QType, RelPermModelw, RelPermModeln, CapillaryModel>::
fluxViscous( const Real& x, const Real& time,
             const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
             ArrayQ<Tf>& f ) const
{
  typedef typename promote_Surreal<Tq,Tg>::type T;

  T pwx = 0, pnx = 0;
  qInterpreter_.eval_pwGradient( q, qx, pwx );
  qInterpreter_.eval_pnGradient( q, qx, pnx );

  //Evaluate relative permeabilities at average state
  Real krw = krw_.relativeperm(Swbar_);
  Real krn = krn_.relativeperm(Snbar_);

  //Evaluate phase mobilities
  Real lambda_w = krw/muw_;
  Real lambda_n = krn/mun_;

  f(0) -= rhow_*K_*lambda_w*pwx;
  f(1) -= rhon_*K_*lambda_n*pnx;
}

// space-time viscous flux
template <class QType, class RelPermModelw, class RelPermModeln, class CapillaryModel>
template <class Tq, class Tg, class Tf>
inline void
PDETwoPhaseLinearized1D<QType, RelPermModelw, RelPermModeln, CapillaryModel>::
fluxViscousSpaceTime( const Real& x, const Real& time,
                      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
                      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
{
  //Not adding temporal diffusion, so just forward the call to spatial viscous flux
  fluxViscous(x, time, q, qx, f);
}

// viscous flux: normal flux with left and right states
template <class QType, class RelPermModelw, class RelPermModeln, class CapillaryModel>
template <class Tq, class Tg, class Tf>
inline void
PDETwoPhaseLinearized1D<QType, RelPermModelw, RelPermModeln, CapillaryModel>::
fluxViscous(const Real& x, const Real& time,
            const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
            const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
            const Real& nx, ArrayQ<Tf>& fn ) const
{
  typedef typename promote_Surreal<Tq,Tg>::type T;

  T pwxL = 0, pnxL = 0;
  T pwxR = 0, pnxR = 0;

  qInterpreter_.eval_pwGradient( qL, qxL, pwxL );
  qInterpreter_.eval_pnGradient( qL, qxL, pnxL );
  qInterpreter_.eval_pwGradient( qR, qxR, pwxR );
  qInterpreter_.eval_pnGradient( qR, qxR, pnxR );

  //Evaluate relative permeabilities at average state
  Real krw = krw_.relativeperm(Swbar_);
  Real krn = krn_.relativeperm(Snbar_);

  //Evaluate phase mobilities
  Real lambda_w = krw/muw_;
  Real lambda_n = krn/mun_;

  ArrayQ<Tf> fxL = 0, fxR = 0;

  fxL(0) -= rhow_*K_*lambda_w*pwxL;
  fxL(1) -= rhon_*K_*lambda_n*pnxL;
  fxR(0) -= rhow_*K_*lambda_w*pwxR;
  fxR(1) -= rhon_*K_*lambda_n*pnxR;

  //Average normal flux
  fn += 0.5*(fxL + fxR)*nx;

#if 1 //Add upwinding

  Tq SwL = 0, SwR = 0;
  qInterpreter_.eval_Sw( qL, SwL );
  qInterpreter_.eval_Sw( qR, SwR );

  //Evaluate relative permeability jacobians at average state
  Real krw_Sw, krn_Sn;
  krw_.relativepermJacobian(Swbar_, krw_Sw);
  krn_.relativepermJacobian(Snbar_, krn_Sn);

  Real dSn_dSw = -1;
  Real krn_Sw = krn_Sn * dSn_dSw;

  //Evaluate phase mobility jacobians at average state
  Real lambda_w_Sw = krw_Sw/muw_;
  Real lambda_n_Sw = krn_Sw/mun_;

  //Upwinding term
  Real Kgradpn = (K_*pxbar_)*nx;

#if 1
  Real c = (lambda_w_Sw * lambda_n - lambda_w * lambda_n_Sw) / (lambda_w + lambda_n) * Kgradpn;
  T g = 0.5 * fabs(c) * (SwL - SwR);

  fn(0) += rhow_ * g;
  fn(1) -= rhon_ * g;
#else
  //Mobility upwinding
  fn(0) += rhow_ * 0.5 * fabs(Kgradpn) * lambda_w_Sw * (SwL - SwR);
  fn(1) += rhon_ * 0.5 * fabs(Kgradpn) * lambda_n_Sw * (SwL - SwR);
#endif

#endif
}

// space-time viscous flux: normal flux with left and right states
template <class QType, class RelPermModelw, class RelPermModeln, class CapillaryModel>
template <class Tq, class Tg, class Tf>
inline void
PDETwoPhaseLinearized1D<QType, RelPermModelw, RelPermModeln, CapillaryModel>::
fluxViscousSpaceTime( const Real& x, const Real& time,
                      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qtL,
                      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qtR,
                      const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const
{
  //Not adding temporal diffusion, so just forward the call to spatial viscous flux
  fluxViscous(x, time, qL, qxL, qR, qxR, nx, f);
}

// viscous diffusion matrix: d(Fv)/d(UX)
template <class QType, class RelPermModelw, class RelPermModeln, class CapillaryModel>
template <class Tq, class Tg, class Tk>
inline void
PDETwoPhaseLinearized1D<QType, RelPermModelw, RelPermModeln, CapillaryModel>::
diffusionViscous( const Real& x, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
                  MatrixQ<Tk>& kxx ) const
{
  SANS_DEVELOPER_EXCEPTION("Not implemented because the conservative flux Jacobian is singular!");
}

// viscous diffusion matrix: d(Fv)/d(UX)
template <class QType, class RelPermModelw, class RelPermModeln, class CapillaryModel>
template <class Tq, class Tg, class Tk>
inline void
PDETwoPhaseLinearized1D<QType, RelPermModelw, RelPermModeln, CapillaryModel>::
diffusionViscousSpaceTime( const Real& x, const Real& time,
                           const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
                           MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxt,
                           MatrixQ<Tk>& ktx, MatrixQ<Tk>& ktt ) const
{
  //Not adding temporal diffusion, so just forward the call to spatial diffusion matrix
  diffusionViscous(x, time, q, qx, kxx);
}

template <class QType, class RelPermModelw, class RelPermModeln, class CapillaryModel>
template <class Tq, class Tg>
inline void
PDETwoPhaseLinearized1D<QType, RelPermModelw, RelPermModeln, CapillaryModel>::
source(const Real& x, const Real& time, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
       ArrayQ< typename promote_Surreal<Tq,Tg>::type >& source ) const
{

}

// update fraction needed for physically valid state
template <class QType, class RelPermModelw, class RelPermModeln, class CapillaryModel>
void
PDETwoPhaseLinearized1D<QType, RelPermModelw, RelPermModeln, CapillaryModel>::
updateFraction( const Real& x, const Real& time, const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
                const Real maxChangeFraction, Real& updateFraction ) const
{
  SANS_DEVELOPER_EXCEPTION("Not implemented");
}

// is state physically valid
template <class QType, class RelPermModelw, class RelPermModeln, class CapillaryModel>
inline bool
PDETwoPhaseLinearized1D<QType, RelPermModelw, RelPermModeln, CapillaryModel>::
isValidState( const ArrayQ<Real>& q ) const
{
  return true; //qInterpret_.isValidState(q);
}

// set from primitive variable array
template <class QType, class RelPermModelw, class RelPermModeln, class CapillaryModel>
template <class T>
inline void
PDETwoPhaseLinearized1D<QType, RelPermModelw, RelPermModeln, CapillaryModel>::
setDOFFrom( ArrayQ<T>& q, const Real data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn >= N);
  qInterpreter_.setFromPrimitive( q, data, name, nn );
}

// set from variables
template <class QType, class RelPermModelw, class RelPermModeln, class CapillaryModel>
template <class T, class Variables>
inline void
PDETwoPhaseLinearized1D<QType, RelPermModelw, RelPermModeln, CapillaryModel>::
setDOFFrom( ArrayQ<T>& q, const TwoPhaseVariableType<Variables>& data ) const
{
  qInterpreter_.setFromPrimitive( q, data.cast() );
}

// interpret residuals of the solution variable
template <class QType, class RelPermModelw, class RelPermModeln, class CapillaryModel>
template <class T>
inline void
PDETwoPhaseLinearized1D<QType, RelPermModelw, RelPermModeln, CapillaryModel>::
interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
{
  int nOut = rsdPDEOut.m();

  SANS_ASSERT(nOut == 2);
  for (int i = 0; i < nOut; i++)
    rsdPDEOut[i] = rsdPDEIn[i];
}

// interpret residuals of the gradients in the solution variable
template <class QType, class RelPermModelw, class RelPermModeln, class CapillaryModel>
template <class T>
void
PDETwoPhaseLinearized1D<QType, RelPermModelw, RelPermModeln, CapillaryModel>::
interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const
{
  //TEMPORARY SOLUTION FOR AUX VARIABLE
  int nOut = rsdAuxOut.m();

  SANS_ASSERT(nOut == 2);
  for (int i = 0; i < nOut; i++)
    rsdAuxOut[i] = rsdAuxIn[i];
}

// interpret residuals at the boundary (should forward to BCs)
template <class QType, class RelPermModelw, class RelPermModeln, class CapillaryModel>
template <class T>
inline void
PDETwoPhaseLinearized1D<QType, RelPermModelw, RelPermModeln, CapillaryModel>::
interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const
{
  int nOut = rsdBCOut.m();

  SANS_ASSERT(nOut == 2);
  for (int i = 0; i < nOut; i++)
    rsdBCOut[i] = rsdBCIn[i];
}

// how many residual equations are we dealing with
template <class QType, class RelPermModelw, class RelPermModeln, class CapillaryModel>
inline int
PDETwoPhaseLinearized1D<QType, RelPermModelw, RelPermModeln, CapillaryModel>::
nMonitor() const
{
  return 2;
}

template <class QType, class RelPermModelw, class RelPermModeln, class CapillaryModel>
void
PDETwoPhaseLinearized1D<QType, RelPermModelw, RelPermModeln, CapillaryModel>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  std::string indent2(indentSize+2, ' ');

  out << indent << "PDETwoPhaseLinearized1D<QType, RelPermModelw, RelPermModeln, CapillaryModel>:" << std::endl;
  out << indent2 << "qInterpret_ = " << std::endl;
  qInterpreter_.dump(indentSize+4, out);
  out << indent2 << "rhow_ = " << rhow_ << std::endl;
  out << indent2 << "rhon_ = " << rhon_ << std::endl;
  out << indent2 << "phi_ = " << phi_ << std::endl;
  out << indent2 << "krw_ = " << std::endl;
  krw_.dump(indentSize+4, out);
  out << indent2 << "krn_ = " << std::endl;
  krn_.dump(indentSize+4, out);
  out << indent2 << "muw_ = " << muw_ << std::endl;
  out << indent2 << "mun_ = " << mun_ << std::endl;
  out << indent2 << "K_ = " << K_ << std::endl;
  out << indent2 << "pc_ = " << std::endl;
  pc_.dump(indentSize+4, out);
  out << indent2 << "pxbar_ = " << pxbar_ << std::endl;
  out << indent2 << "Swbar_ = " << Swbar_ << std::endl;
}


} //namespace SANS

#endif  // PDETWOPHASELINEARIZED1D_H
