// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOURCETWOPHASE1D_H
#define SOURCETWOPHASE1D_H

// 1-D two-phase PDE: source terms

#include <iostream>
#include <limits>

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "tools/SANSnumerics.h"     // Real

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"

namespace SANS
{

struct SourceTwoPhase1DType_FixedWellPressure_Param : noncopyable
{
  const ParameterNumeric<Real> pB{"pB", NO_DEFAULT, 0, NO_LIMIT, "Fixed bottom hole pressure"};

  static void checkInputs(PyDict d);
  static SourceTwoPhase1DType_FixedWellPressure_Param params;
};


struct SourceTwoPhase1DParam : noncopyable
{
  struct SourceOptions
  {
    typedef DictKeyPair ExtractType;
    const ParameterString SourceType = ParameterString("SourceType", NO_DEFAULT, "Type of source" );
    const ParameterString& key = SourceType;

    const DictOption FixedPressure {"FixedPressure", SourceTwoPhase1DType_FixedWellPressure_Param::checkInputs};

    const std::vector<DictOption> options{FixedPressure};
  };
  const ParameterOption<SourceOptions> Source{"Source", NO_DEFAULT, "Dictionary with source parameters"};

//  struct GeometryOptions
//  {
//    typedef DictKeyPair ExtractType;
//    const ParameterString Shape = ParameterString("Shape", NO_DEFAULT, "Shape of source" );
//    const ParameterString& key = Shape;
//
//    const DictOption Round {"Round", SourceTwoPhase1DType_FixedWellPressure_Param::checkInputs};
//    const DictOption Rectangular {"Rectangular", SourceTwoPhase1DType_FixedWellPressure_Param::checkInputs};
//
//    const std::vector<DictOption> options{Round, Rectangular};
//  };
//  const ParameterOption<GeometryOptions> Geometry{"Geometry", NO_DEFAULT, "Dictionary with geometry parameters"};

  const ParameterNumeric<Real> xmin{"xmin", NO_DEFAULT, NO_RANGE, "Minimum x-coordinate of the active source region"};
  const ParameterNumeric<Real> xmax{"xmax", NO_DEFAULT, NO_RANGE, "Maximum x-coordinate of the active source region"};
  const ParameterNumeric<Real> Tmin{"Tmin", NO_DEFAULT, NO_RANGE, "Starting time of the source term"};
  const ParameterNumeric<Real> Tmax{"Tmax", NO_DEFAULT, NO_RANGE, "Shut-off time of the source term"};
  const ParameterNumeric<Real> smoothLx{"smoothLx", 0.0, 0.0, NO_LIMIT, "Length scale for smoothing in x-direction"};
  const ParameterNumeric<Real> smoothT{"smoothT", 0.0, 0.0, NO_LIMIT, "Time scale for smoothing in temporal direction"};

  static void checkInputs(PyDict d);
  static SourceTwoPhase1DParam params;
};

struct SourceTwoPhase1DListParam : noncopyable
{
  static void checkInputs(PyDict d);
  static SourceTwoPhase1DListParam params;
};

//-------------------------------------------------------------


class SourceTwoPhase1DBase
{
public:

  typedef SourceTwoPhase1DParam ParamsType;

  explicit SourceTwoPhase1DBase(const PyDict& d)
  :
    xmin_(d.get(ParamsType::params.xmin)), xmax_(d.get(ParamsType::params.xmax)),
    Tmin_(d.get(ParamsType::params.Tmin)), Tmax_(d.get(ParamsType::params.Tmax)),
    smoothLx_(d.get(ParamsType::params.smoothLx)), smoothT_(d.get(ParamsType::params.smoothT))
  {
     SANS_ASSERT(xmax_ > xmin_);
     SANS_ASSERT(Tmax_ > Tmin_);
     SANS_ASSERT(smoothLx_ >= 0.0);
     SANS_ASSERT(smoothT_ >= 0.0);
  }

  Real getWeight(const Real& x, const Real& t) const
  {
    Real wx = computeSmoothingWeight(x, xmin_, xmax_, smoothLx_);
    Real wt = computeSmoothingWeight(t, Tmin_, Tmax_, smoothT_ );
    return wx*wt;
  }

  Real getLength() const { return xmax_ - xmin_ ;}

  ~SourceTwoPhase1DBase() {}

protected:

  Real computeSmoothingWeight(const Real& x, const Real& xmin, const Real& xmax, const Real& smoothL) const;

  Real xmin_;     //Active region of the source term
  Real xmax_;
  Real Tmin_;   //Active time-period of the source term
  Real Tmax_;
  Real smoothLx_; //smoothing length scale on either side of xmin_ and xmax_ (use zero for no smoothing)
  Real smoothT_;  //smoothing time scale on either side of Tstart_ and Tend_ (use zero for no smoothing)
};

inline Real
SourceTwoPhase1DBase::computeSmoothingWeight(const Real& x, const Real& xmin, const Real& xmax, const Real& smoothL) const
{
  if (x >= (xmin - 0.5*smoothL) && x <= (xmax + 0.5*smoothL))
  {
    if (smoothL < 100*std::numeric_limits<Real>::epsilon()) //No smoothing
    {
      return 1.0;
    }
    else //Cubic smoothing function (y = 3x^2 - 2x^3)
    {
      //Smoothing factor for left side of source
      Real x0 = xmin - 0.5*smoothL;
      Real wL = 0.0, wR = 0.0;

      if (x > x0 && x < (x0+smoothL))
      {
        Real xf = (x-x0)/smoothL;
        wL = 3.0*(xf*xf) - 2.0*(xf*xf*xf);
      }
      else if (x >= (x0+smoothL))
      {
        wL = 1.0;
      }

      //Smoothing factor for right side of source
      x0 = xmax - 0.5*smoothL;

      if (x <= x0)
      {
        wR = 1.0;
      }
      else if (x > x0 && x < (x0+smoothL))
      {
        Real xf = (x-x0)/smoothL;
        wR = 1.0 - ( 3.0*(xf*xf) - 2.0*(xf*xf*xf) );
      }

      return wL*wR;
    }
  }

  return 0.0;
}


template <class QInterpreter, class DensityModelw, class DensityModeln,
           class RelPermModelw, class RelPermModeln, class ViscModelw, class ViscModeln,
           class CapillaryModel>
 class SourceTwoPhase1D_FixedWellPressure : public SourceTwoPhase1DBase
{
public:

  template <class T>
  using ArrayQ = typename QInterpreter::template ArrayQ<T>;    // solution/residual arrays

  typedef SourceTwoPhase1DBase BaseType;
  typedef SourceTwoPhase1DParam BaseParamsType;
  typedef SourceTwoPhase1DType_FixedWellPressure_Param ParamsType;

  explicit SourceTwoPhase1D_FixedWellPressure(
      const PyDict& d, const DensityModelw& rhow, const DensityModeln& rhon,
      const RelPermModelw& krw, const RelPermModeln& krn, const ViscModelw& muw, const ViscModeln& mun,
      const Real K, const CapillaryModel& pc)
  : BaseType(d),
    pB_(d.get(BaseParamsType::params.Source).get(ParamsType::params.pB)),
    rhow_(rhow), rhon_(rhon), krw_(krw), krn_(krn), muw_(muw), mun_(mun),
    K_(K), pc_(pc), qInterpreter_(pc_){}

  Real getWellPressure() { return pB_; }

  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const {}

  template <class Tq, class Tg, class Ts>
  void source(const Real& x, const Real& time,
              const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
              ArrayQ<Ts>& source ) const
  {
    Real weight = getWeight(x, time);

    if (weight < 100*std::numeric_limits<Real>::epsilon()) return; //The weighting factor is too small, so not worth computing source term

    Real Ls = getLength(); //Well size
    Real Lchar = 0.25*Ls; //Characteristic length scale

    Tq Sw = 0, Sn = 0, pw = 0, pn = 0;
    qInterpreter_.eval( q, pw, pn, Sw, Sn );

    //Evaluate densities
    Tq rhow = rhow_.density(x, time, pw);
    Tq rhon = rhon_.density(x, time, pn);

    //Evaluate relative permeabilities
    Tq krw = krw_.relativeperm(Sw);
    Tq krn = krn_.relativeperm(Sn);

    //Evaluate phase viscosities
    Tq muw = muw_.viscosity(x, time);
    Tq mun = mun_.viscosity(x, time);

    //Evaluate phase mobilities
    Tq lambda_w = krw/muw;
    Tq lambda_n = krn/mun;

    Tq dpndx = (pn - pB_)/Lchar;

    Tq tmp_factor = weight*K_*dpndx/Ls;

    //Note that source term is on LHS
    source[0] += rhow*lambda_w*tmp_factor; //Phase1 extracted (Water)
    source[1] += rhon*lambda_n*tmp_factor; //Phase2 extracted (Oil)
  }

  ~SourceTwoPhase1D_FixedWellPressure() {}

protected:
  Real pB_;  //Fixed Bottom hole pressure

  const DensityModelw& rhow_; //Density model for wetting phase
  const DensityModeln& rhon_; //Density model for non-wetting phase

  const RelPermModelw& krw_; //Relative permeability model for wetting phase
  const RelPermModeln& krn_; //Relative permeability model for non-wetting phase

  const ViscModelw& muw_; //Viscosity model for wetting phase
  const ViscModeln& mun_; //Viscosity model for non-wetting phase

  Real K_; // Permeability (assumed to be constant for now)
  const CapillaryModel& pc_; //Capillary pressure model

  const QInterpreter qInterpreter_; // solution variable interpreter class
};

} //namespace SANS

#endif  // SOURCETWOPHASE1D_H
