// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef TRAITSBUCKLEYLEVERETT_H
#define TRAITSBUCKLEYLEVERETT_H

#include "Topology/Dimension.h"

namespace SANS
{

template<class PhysDim_>
class TraitsSizeBuckleyLeverett
{
public:
  typedef PhysDim_ PhysDim;
  static const int D = PhysDim::D;            // physical dimensions
  static const int N = 1;                     // total solution variables

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  template <class T>
  using MatrixQ = T;   // matrices
};

template <class QType_, class RelPermModel_, class CapillaryModel_>
class TraitsModelBuckleyLeverett
{
public:
  typedef QType_ QType;
  typedef RelPermModel_ RelPermModel;
  typedef CapillaryModel_ CapillaryModel;
};

}

#endif //TRAITSBUCKLEYLEVERETT_H
