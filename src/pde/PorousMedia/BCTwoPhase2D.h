// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCTWOPHASE2D_H
#define BCTWOPHASE2D_H

// 2-D Two-phase BC class

//PyDict must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/BCCategory.h"
#include "pde/BCNone.h"
#include "Topology/Dimension.h"

#include <iostream>
#include <memory> // shared_ptr

#include <boost/mpl/vector.hpp>

#include "PDETwoPhase2D.h"
#include "SolutionFunction_TwoPhase2D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// BC types
//----------------------------------------------------------------------------//

class BCTypeTimeIC;
class BCTypeFullState;
class BCTypeNoFlux;
class BCTypeNoFluxSpaceTime;
class BCTypeFunction_mitState;

//----------------------------------------------------------------------------//
// BC class: 2-D TwoPhase
//
// template parameters:
//   PhysDim              Physical dimensions
//   BCType               BC type
//----------------------------------------------------------------------------//

template <class BCType, class PDE>
class BCTwoPhase2D;

template <class BCType>
struct BCTwoPhase2DParams;


// Define the vector of boundary conditions
template <class PDE>
using BCTwoPhase2DVector =  boost::mpl::vector6< BCNone<PhysD2,PDE::N>,
                                                 SpaceTimeBC< BCTwoPhase2D<BCTypeTimeIC, PDE> >,
                                                 BCTwoPhase2D<BCTypeFullState, PDE>,
                                                 BCTwoPhase2D<BCTypeNoFlux, PDE>,
                                                 SpaceTimeBC< BCTwoPhase2D<BCTypeNoFluxSpaceTime, PDE> >,
                                                 BCTwoPhase2D<BCTypeFunction_mitState, PDE>
                                               >;

/*-------------------------------------------------------------------------------*/

template<>
struct BCTwoPhase2DParams<BCTypeTimeIC> : noncopyable
{
  BCTwoPhase2DParams();

  const ParameterOption<TwoPhaseVariableTypeParams::TwoPhaseVariableOptions>& StateVector;

  static constexpr const char* BCName{"TimeIC"};
  struct Option
  {
    const DictOption TimeIC{BCTwoPhase2DParams::BCName, BCTwoPhase2DParams::checkInputs};
  };

  static void checkInputs(PyDict d);
  static BCTwoPhase2DParams params;
};

template <class PDE_>
class BCTwoPhase2D<BCTypeTimeIC, PDE_> :
    public BCType< BCTwoPhase2D<BCTypeTimeIC, PDE_> >
{
public:
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCTwoPhase2DParams<BCTypeTimeIC> ParamsType;
  typedef PDE_ PDE;

  typedef PhysD2 PhysDim;
  static const int D = PDE::D; // physical dimensions
  static const int N = PDE::N; // total solution variables

  static const int NBC = N;         // total BCs

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;   // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>; // matrices

  // cppcheck-suppress noExplicitConstructor
  template <class Variables>
  BCTwoPhase2D( const PDE& pde, const TwoPhaseVariableType<Variables>& data ) :
      pde_(pde),
      qB_(pde.setDOFFrom(data)){}

  BCTwoPhase2D(const PDE& pde, const PyDict& d ) :
    pde_(pde),
    qB_(pde.setDOFFrom(d)){}

  virtual ~BCTwoPhase2D() {}

  BCTwoPhase2D( const BCTwoPhase2D& ) = delete;
  BCTwoPhase2D& operator=( const BCTwoPhase2D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // BC data
  template <class T>
  void state( const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    qB = qB_;
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormalSpaceTime( const Real& x, const Real& y, const Real& time,
                            const Real& nx, const Real& ny, const Real& nt,
                            const ArrayQ<T>& qI,
                            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIt,
                            const ArrayQ<T>& qB,
                            ArrayQ<Tf>& Fn) const
  {
    ArrayQ<T> uB = 0;
    pde_.fluxAdvectiveTime(x, y, time, qB, uB);
    Fn += nt*uB;

    //Add any contributions from space-time diffusive fluxes
    ArrayQ<Tf> fvx = 0, fvy = 0, fvt = 0;
    pde_.fluxViscousSpaceTime(x, y, time, qB, qIx, qIy, qIt, fvx, fvy, fvt);
    Fn += fvx*nx + fvy*ny + fvt*nt;
  }

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormalSpaceTime( const Tp& param,
                            const Real& x, const Real& y, const Real& time,
                            const Real& nx, const Real& ny, const Real& nt,
                            const ArrayQ<T>& qI,
                            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIt,
                            const ArrayQ<T>& qB,
                            ArrayQ<Tf>& Fn) const
  {
    ArrayQ<T> uB = 0;
    pde_.fluxAdvectiveTime(param, x, y, time, qB, uB);
    Fn += nt*uB;

    //Add any contributions from space-time diffusive fluxes
    ArrayQ<Tf> fvx = 0, fvy = 0, fvt = 0;
    pde_.fluxViscousSpaceTime(param, x, y, time, qB, qIx, qIy, qIt, fvx, fvy, fvt);
    Fn += fvx*nx + fvy*ny + fvt*nt;
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const Real& nt, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const ArrayQ<Real> qB_;
};

/*-------------------------------------------------------------------------------*/

template<>
struct BCTwoPhase2DParams<BCTypeFullState> : noncopyable
{
  BCTwoPhase2DParams();

  const ParameterOption<TwoPhaseVariableTypeParams::TwoPhaseVariableOptions>& StateVector;

  static constexpr const char* BCName{"FullState"};
  struct Option
  {
    const DictOption FullState{BCTwoPhase2DParams::BCName, BCTwoPhase2DParams::checkInputs};
  };

  static void checkInputs(PyDict d);
  static BCTwoPhase2DParams params;
};

template <class PDE_>
class BCTwoPhase2D<BCTypeFullState, PDE_> :
    public BCType< BCTwoPhase2D<BCTypeFullState, PDE_> >
{
public:
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCTwoPhase2DParams<BCTypeFullState> ParamsType;
  typedef PDE_ PDE;

  typedef PhysD2 PhysDim;
  static const int D = PDE::D; // physical dimensions
  static const int N = PDE::N; // total solution variables

  static const int NBC = N;         // total BCs

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;   // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>; // matrices

  // cppcheck-suppress noExplicitConstructor
  template <class Variables>
  BCTwoPhase2D( const PDE& pde, const TwoPhaseVariableType<Variables>& data ) :
      pde_(pde),
      qB_(pde.setDOFFrom(data)){}

  BCTwoPhase2D(const PDE& pde, const PyDict& d ) :
    pde_(pde),
    qB_(pde.setDOFFrom(d)){}

  virtual ~BCTwoPhase2D() {}

  BCTwoPhase2D( const BCTwoPhase2D& ) = delete;
  BCTwoPhase2D& operator=( const BCTwoPhase2D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return pde_.hasFluxViscous(); }

  // BC state
  template <class T>
  void state( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    qB = qB_;
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    // Add upwinded advective flux
    pde_.fluxAdvectiveUpwind(x, y, time, qI, qB, nx, ny, Fn);

    // Add viscous flux
    ArrayQ<Tf> fvx = 0, fvy = 0;
    pde_.fluxViscous(x, y, time, qB, qIx, qIy, fvx, fvy);
    Fn += fvx*nx + fvy*ny;
  }

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormal( const Tp& param,
                   const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    // Add upwinded advective flux
    pde_.fluxAdvectiveUpwind(param, x, y, time, qI, qB, nx, ny, Fn);

    // Add viscous flux
    ArrayQ<Tf> fvx = 0, fvy = 0;
    pde_.fluxViscous(param, x, y, time, qB, qIx, qIy, fvx, fvy);
    Fn += fvx*nx + fvy*ny;
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const ArrayQ<Real> qB_;
};

/*-------------------------------------------------------------------------------*/

template<>
struct BCTwoPhase2DParams<BCTypeNoFlux> : noncopyable
{
  static constexpr const char* BCName{"NoFlux"};
  struct Option
  {
    const DictOption NoFlux{BCTwoPhase2DParams::BCName, BCTwoPhase2DParams::checkInputs};
  };

  static void checkInputs(PyDict d);
  static BCTwoPhase2DParams params;
};


template <class PDE_>
class BCTwoPhase2D<BCTypeNoFlux, PDE_> :
    public BCType< BCTwoPhase2D<BCTypeNoFlux, PDE_> >
{
public:
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCTwoPhase2DParams<BCTypeNoFlux> ParamsType;
  typedef PDE_ PDE;

  typedef PhysD2 PhysDim;
  static const int D = PDE::D; // physical dimensions
  static const int N = PDE::N; // total solution variables

  static const int NBC = N;         // total BCs

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;   // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>; // matrices

  BCTwoPhase2D(const PDE& pde, const PyDict& d ) : pde_(pde) {}
  explicit BCTwoPhase2D(const PDE& pde ) : pde_(pde) {}

  virtual ~BCTwoPhase2D() {}

  BCTwoPhase2D( const BCTwoPhase2D& ) = delete;
  BCTwoPhase2D& operator=( const BCTwoPhase2D& ) = delete;

  bool useUpwind() const { return false; }

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // BC state
  template <class T>
  void state( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    qB = qI;
  }

  // normal BC flux
  template <class T>
  void fluxNormal( const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const
  {
    Fn = 0.0;
  }

  // normal BC flux
  template <class Tp, class T>
  void fluxNormal( const Tp& param, const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const
  {
    Fn = 0.0;
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
};

/*-------------------------------------------------------------------------------*/

template<>
struct BCTwoPhase2DParams<BCTypeNoFluxSpaceTime> : noncopyable
{
  static constexpr const char* BCName{"NoFluxSpaceTime"};
  struct Option
  {
    const DictOption NoFluxSpaceTime{BCTwoPhase2DParams::BCName, BCTwoPhase2DParams::checkInputs};
  };

  static void checkInputs(PyDict d);
  static BCTwoPhase2DParams params;
};

template <class PDE_>
class BCTwoPhase2D<BCTypeNoFluxSpaceTime, PDE_> :
    public BCType< BCTwoPhase2D<BCTypeNoFluxSpaceTime, PDE_> >
{
public:
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCTwoPhase2DParams<BCTypeNoFluxSpaceTime> ParamsType;
  typedef PDE_ PDE;

  typedef PhysD2 PhysDim;
  static const int D = PDE::D; // physical dimensions
  static const int N = PDE::N; // total solution variables

  static const int NBC = N;         // total BCs

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;   // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>; // matrices

  BCTwoPhase2D(const PDE& pde, const PyDict& d ) : pde_(pde) {}
  explicit BCTwoPhase2D(const PDE& pde ) : pde_(pde) {}

  virtual ~BCTwoPhase2D() {}

  BCTwoPhase2D( const BCTwoPhase2D& ) = delete;
  BCTwoPhase2D& operator=( const BCTwoPhase2D& ) = delete;

  bool useUpwind() const { return false; }

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // BC data
  template <class T>
  void state( const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    qB = qI;
  }

  // space-time normal BC flux
  template <class T, class Tf>
  void fluxNormalSpaceTime( const Real& x, const Real& y, const Real& time,
                            const Real& nx, const Real& ny, const Real& nt,
                            const ArrayQ<T>& qI,
                            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIt,
                            const ArrayQ<T>& qB,
                            ArrayQ<Tf>& Fn) const
  {
    Fn = 0.0;
  }

  // space-time normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormalSpaceTime( const Tp& param,
                            const Real& x, const Real& y, const Real& time,
                            const Real& nx, const Real& ny, const Real& nt,
                            const ArrayQ<T>& qI,
                            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIt,
                            const ArrayQ<T>& qB,
                            ArrayQ<Tf>& Fn) const
  {
    Fn = 0.0;
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const Real& nt, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
};

#if 1
//----------------------------------------------------------------------------//
// Struct for creating a BC solution function
//
struct BCTwoPhase2DSolutionParams : noncopyable
{
  struct SolutionOptions
  {
    typedef DictKeyPair ExtractType;
    const ParameterString Name{"Name", NO_DEFAULT, "Solution function used on the BC" };
    const ParameterString& key = Name;

    const DictOption SingleWell_Peaceman{"SingleWell_Peaceman", SolutionFunction_TwoPhase2D_SingleWell_Peaceman_Params::checkInputs};

    const std::vector<DictOption> options{ SingleWell_Peaceman };
  };
  const ParameterOption<SolutionOptions> Function{"Function", NO_DEFAULT, "The BC is a solution function"};
};

// Struct for creating the solution pointer
template <class QInterpreter, template <class> class PDETraitsSize>
struct BCTwoPhase2DSolution
{
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;    // solution/residual arrays

  typedef Function2DBase<ArrayQ<Real>> Function2DBaseType;
  typedef std::shared_ptr<Function2DBaseType> Function_ptr;

  static Function_ptr getSolution(PyDict d, const QInterpreter& qInterpreter);
};


//----------------------------------------------------------------------------//
// Full state with Solution Function
//----------------------------------------------------------------------------//
template<>
struct BCTwoPhase2DParams< BCTypeFunction_mitState > : BCTwoPhase2DSolutionParams
{
  static constexpr const char* BCName{"Function_mitState"};
  struct Option
  {
    const DictOption Function_mitState{BCTwoPhase2DParams::BCName, BCTwoPhase2DParams::checkInputs};
  };

  static void checkInputs(PyDict d);
  static BCTwoPhase2DParams params;
};


template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
class BCTwoPhase2D<BCTypeFunction_mitState, PDE_<PDETraitsSize,PDETraitsModel>> :
    public BCType< BCTwoPhase2D<BCTypeFunction_mitState, PDE_<PDETraitsSize,PDETraitsModel>> >
{
public:
  typedef BCTypeFunction_mitState BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCTwoPhase2DParams<BCType> ParamsType;
  typedef PDE_<PDETraitsSize,PDETraitsModel> PDE;

  typedef typename PDE::QInterpreter QInterpreter; // solution variable interpreter type

  typedef PhysD2 PhysDim;
  static const int D = PDE::D; // physical dimensions
  static const int N = PDE::N; // total solution variables

  static const int NBC = N; // total BCs

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>; // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>; // matrices

  typedef std::shared_ptr<Function2DBase<ArrayQ<Real>>> Function_ptr;

  // cppcheck-suppress noExplicitConstructor
  BCTwoPhase2D( const PDE& pde, const Function_ptr& solution ) :
      pde_(pde), solution_(solution) {}
  ~BCTwoPhase2D() {}

  BCTwoPhase2D(const PDE& pde, const PyDict& d ) :
    pde_(pde),
    solution_( BCTwoPhase2DSolution<QInterpreter, PDETraitsSize>::getSolution(d, pde_.variableInterpreter()) ) {}

  BCTwoPhase2D( const BCTwoPhase2D& ) = delete;
  BCTwoPhase2D& operator=( const BCTwoPhase2D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return pde_.hasFluxViscous(); }

  // BC data
  template <class T>
  void state( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    qB = (*solution_)(x,y,time);
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    // Add upwinded advective flux
    pde_.fluxAdvectiveUpwind(x, y, time, qI, qB, nx, ny, Fn);

    ArrayQ<Tf> fx = 0, fy = 0;
    pde_.fluxViscous(x, y, time, qB, qIx, qIy, fx, fy);

    // Add viscous flux
    Fn += fx*nx + fy*ny;
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const Function_ptr solution_;
};
#endif

} //namespace SANS

#endif  // BCTWOPHASE2D_H
