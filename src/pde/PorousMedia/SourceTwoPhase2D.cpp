// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "SourceTwoPhase2D.h"

#define PYDICT_INSTANTIATE
#include "Python/PyDict_impl.h"

namespace SANS
{

PYDICT_PARAMETER_OPTION_INSTANTIATE(SourceTwoPhase2DParam::SourceOptions)
PYDICT_PARAMETER_OPTION_INSTANTIATE(SourceTwoPhase2DType_FixedPressureInflow_Param::WellModelOptions)
PYDICT_PARAMETER_OPTION_INSTANTIATE(SourceTwoPhase2DType_FixedPressureOutflow_Param::WellModelOptions)

// cppcheck-suppress passedByValue
void SourceTwoPhase2DType_FixedPressureInflow_Param::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.pB));
  allParams.push_back(d.checkInputs(params.Sw));
  allParams.push_back(d.checkInputs(params.Rwellbore));
  allParams.push_back(d.checkInputs(params.nParam));
  allParams.push_back(d.checkInputs(params.s0));
  allParams.push_back(d.checkInputs(params.WellModel));
  d.checkUnknownInputs(allParams);
}
SourceTwoPhase2DType_FixedPressureInflow_Param SourceTwoPhase2DType_FixedPressureInflow_Param::params;

// cppcheck-suppress passedByValue
void SourceTwoPhase2DType_FixedPressureOutflow_Param::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.pB));
  allParams.push_back(d.checkInputs(params.Rwellbore));
  allParams.push_back(d.checkInputs(params.nParam));
  allParams.push_back(d.checkInputs(params.s0));
  allParams.push_back(d.checkInputs(params.WellModel));
  d.checkUnknownInputs(allParams);
}
SourceTwoPhase2DType_FixedPressureOutflow_Param SourceTwoPhase2DType_FixedPressureOutflow_Param::params;

// cppcheck-suppress passedByValue
void SourceTwoPhase2DType_FixedInflowRate_Param::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Q));
  allParams.push_back(d.checkInputs(params.Sw));
  allParams.push_back(d.checkInputs(params.nParam));
  d.checkUnknownInputs(allParams);
}
SourceTwoPhase2DType_FixedInflowRate_Param SourceTwoPhase2DType_FixedInflowRate_Param::params;

// cppcheck-suppress passedByValue
void SourceTwoPhase2DType_FixedOutflowRate_Param::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Q));
  allParams.push_back(d.checkInputs(params.nParam));
  d.checkUnknownInputs(allParams);
}
SourceTwoPhase2DType_FixedOutflowRate_Param SourceTwoPhase2DType_FixedOutflowRate_Param::params;

// cppcheck-suppress passedByValue
void SourceTwoPhase2DParam::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Source));
  allParams.push_back(d.checkInputs(params.xcentroid));
  allParams.push_back(d.checkInputs(params.ycentroid));
  allParams.push_back(d.checkInputs(params.R));
  allParams.push_back(d.checkInputs(params.Tmin));
  allParams.push_back(d.checkInputs(params.Tmax));
  allParams.push_back(d.checkInputs(params.smoothLr));
  allParams.push_back(d.checkInputs(params.smoothT));
  allParams.push_back(d.checkInputs(params.visibleAngle));
  d.checkUnknownInputs(allParams);
}
SourceTwoPhase2DParam SourceTwoPhase2DParam::params;

// cppcheck-suppress passedByValue
void SourceTwoPhase2DListParam::checkInputs(PyDict d)
{
  // Extract the keys from the dictionary
  std::vector<std::string> keys = d.stringKeys();

  for (std::size_t i = 0; i < keys.size(); i++)
  {
    // Create a source parameter for the given key
    const ParameterDict sourceDictParam{keys[i], NO_DEFAULT, SourceTwoPhase2DParam::checkInputs, "Source dictionary"};
    d.checkInputs(sourceDictParam);
  }
}
SourceTwoPhase2DListParam SourceTwoPhase2DListParam::params;


/* Calculates the coefficients of the polynomials f(s) that satisfy:
 * f(0) = 0, f(1) = 1
 * All derivatives upto Corder are zero at s = 0
 * All derivatives upto Corder match the derivatives of f'(1) ln(s) at s = 1
 */
std::vector<Real> wellActivationFunctionPolyFlatCoeff(const int& Corder, const Real& s0)
{
  std::vector<Real> a(2*Corder + 1, 0.0);

  Real s02 = s0*s0;
  Real s03 = s02*s0;
  Real s04 = s03*s0;
  Real s05 = s04*s0;
  Real s06 = s05*s0;

  //Zero derivatives at s = s0
  if (Corder == 1)
  {
    Real denom = pow(1.0 - s0, 2.0);
    a[0] = s02 / denom;
    a[1] = -2.0*s0 / denom;
    a[2] = 1.0 / denom;
  }
  else if (Corder == 2)
  {
    Real denom = pow(1.0 - s0, 4.0) * (s0 - 7.0);
    a[0] =      s03*(s02 - 11.0*s0 + 16.0) / denom;
    a[1] = 24.0*s02*(s0 - 2.0) / denom;
    a[2] = -6.0*s0 *(s02 + s0 - 8.0) / denom;
    a[3] =  8.0*    (s02 - 2.0*s0 - 2.0) / denom;
    a[4] = -3.0*    (s0 - 3.0) / denom;
  }
  else if (Corder == 3)
  {
    Real denom = pow(1.0 - s0, 6.0) * (s02 - 8.0*s0 + 37.0);
    a[0] =        s04*(s04 - 14.0*s03 + 100.0*s02 - 252.0*s0 + 225.0) / denom;
    a[1] = -180.0*s03*(s02 - 4.0*s0 + 5.0) / denom;
    a[2] =   90.0*s02*(s03 - 2.0*s02 - 4.0*s0 + 15.0) / denom;
    a[3] =  -20.0*s0 *(s04 + 4.0*s03 - 26.0*s02 + 36.0*s0 + 45.0) / denom;
    a[4] =       (45.0*s04 - 90.0*s03 - 180.0*s02 + 900.0*s0 + 225) / denom;
    a[5] =      -(36.0*s03 - 144.0*s02 + 180.0*s0 + 288.0) / denom;
    a[6] =       (10.0*s02 - 50.0*s0 + 100.0) / denom;
  }
  else if (Corder == 4)
  {
    Real denom = pow(1.0 - s0, 8.0) * (3.0*s03 - 29.0*s02 + 139.0*s0 - 533.0);
    a[0] =         s05*(3.0*s06 -   53.0*s05 + 455.0*s04 - 2625.0*s03 + 8240.0*s02 - 13328.0*s0 + 9408.0) / denom;
    a[1] =  3360.0*s04*(    s03 -    6.0*s02 +  14.0*s0  - 14.0) / denom;
    a[2] =  -840.0*s03*(3.0*s04 -   13.0*s03 +  10.0*s02 + 42.0*s0 - 112.0) / denom;
    a[3] =  1120.0*s02*(    s05 -        s04 -  15.0*s03 + 50.0*s02 - 56.0*s0 - 84.0) / denom;
    a[4] =  -210.0*s0 *(    s06 +    9.0*s05 -  55.0*s04 + 65.0*s03 + 120.0*s02 - 616.0*s0 - 224.0) / denom;
    a[5] =           (672.0*s06 -  672.0*s05 - 10080.0*s04 + 33600.0*s03 - 47040.0*s02 - 84672.0*s0 - 9408) / denom;
    a[6] =          (-840.0*s05 + 3640.0*s04 -  2800.0*s03 - 11760.0*s02 + 50960.0*s0 + 19600.0) / denom;
    a[7] =           (480.0*s04 - 2880.0*s03 +  6720.0*s02 -  6720.0*s0  - 14400.0) / denom;
    a[8] =          (-105.0*s03 +  735.0*s02 -  2205.0*s0  +  3675.0) / denom;
  }
  else if (Corder == 5)
  {
    SANS_ASSERT( s0 == 0.0 );
    a[6]  =   88200.0/1627.0;
    a[7]  = -259200.0/1627.0;
    a[8]  =  297675.0/1627.0;
    a[9]  = -156800.0/1627.0;
    a[10] =   31752.0/1627.0;
  }
  else if (Corder == 6)
  {
    SANS_ASSERT( s0 == 0.0 );
    a[7]  =   3136320.0/18107.0;
    a[8]  = -12006225.0/18107.0;
    a[9]  =  18972800.0/18107.0;
    a[10] = -15367968.0/18107.0;
    a[11] =   6350400.0/18107.0;
    a[12] =  -1067220.0/18107.0;
  }
  else
    SANS_DEVELOPER_EXCEPTION("wellActivationFunctionPolyFlatCoeff - Invalid continuity order.");

  return a;
}

/* Calculates the coefficients of the polynomials f(s) that satisfy:
 * f(1) = ln(1)
 * d^k/ds^k(f) = 0 at s = 0, for k = 1 and k >= 3 (i.e. f''(0) can be nonzero)
 * All derivatives up to Corder match the derivatives of ln(s) at s = 1
 */
std::vector<Real> wellActivationFunctionPolyCoeff(const int& Corder)
{
  if (Corder == 1)
  {
    return {-1.0/2.0, 0.0, 1.0/2.0};
  }
  else if (Corder == 2)
  {
    return {-5.0/6.0, 0.0, 3.0/2.0, -2.0/3.0};
  }
  else if (Corder == 3)
  {
    return {-19.0/20.0, 0.0, 5.0/3.0, 0.0, -5.0/4.0, 8.0/15.0};
  }
  else if (Corder == 4)
  {
    return {-106.0/105.0, 0.0, 7.0/4.0, 0.0, 0.0, -14.0/5.0, 35.0/12.0, -6.0/7.0};
  }
  else if (Corder == 5)
  {
    return {-527.0/504.0, 0.0, 9.0/5.0, 0.0, 0.0, 0.0, -7.0, 432.0/35.0, -63.0/8.0, 16.0/9.0};
  }
  else if (Corder == 6)
  {
    return {-29657.0/27720.0, 0.0, 11.0/6.0, 0.0, 0.0, 0.0, 0.0,
            -132.0/7.0, 385.0/8.0, -440.0/9.0, 231.0/10.0, -140.0/33.0};
  }
  else if (Corder == 7)
  {
    return {-55973.0/51480.0, 0.0, 13.0/7.0, 0.0, 0.0, 0.0, 0.0, 0.0,
            -429.0/8.0, 11440.0/63.0, -1287.0/5.0, 2080.0/11.0, -143.0/2.0, 144.0/13.0};
  }
  else
    SANS_DEVELOPER_EXCEPTION("wellActivationFunctionPolyCoeff - Invalid continuity order.");

  return {};
}

//Helper to evaluate polynomial activation functions for distributed wells
void wellActivationFunctionPolynomial(const std::vector<Real>& coeff, const Real& s, const Real& s0, Real& f, Real& df, Real& d2f)
{
  f = 0.0;
  df = 0.0;
  d2f = 0.0;

  if (s < s0)
  {
    return;
  }
  else if (s > 1)
  {
    f = 1.0;
    return;
  }

  //Evaluate the polynomials and derivatives
  Real spow = 1.0, spowm1 = 1.0, spowm2 = 1.0;
  for (int i = 0; i < (int) coeff.size(); i++)
  {
    f += coeff[i]*spow;
    spow *= s;

    if (i >= 1)
    {
      df += i*coeff[i]*spowm1;
      spowm1 *= s;

      if (i >= 2)
      {
        d2f += (i-1)*i*coeff[i]*spowm2;
        spowm2 *= s;
      }
    }
  }
}

} //namespace SANS
