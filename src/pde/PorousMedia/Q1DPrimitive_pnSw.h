// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef Q1DPRIMITIVE_PNSW_H
#define Q1DPRIMITIVE_PNSW_H

// solution interpreter class
// Two-phase eqn: primitive variables (non-wetting pressure pn and wetting saturation Sw)

#include "Topology/Dimension.h"
#include "TwoPhaseVariable.h"

#include <string>

namespace SANS
{

//----------------------------------------------------------------------------//
// solution variable interpreter: Two-phase flow
// primitive variables (non-wetting pressure pn and wetting saturation Sw)
//
// template parameters:
//   T                    solution DOF data type (e.g. double)
//   QType                solution variable set (e.g. primitive)
//
// member functions:
//   .eval_Sw            extract wetting phase saturation (Sw)
//   .eval_Sn            extract non-wetting phase saturation (Sn)
//   .eval_SwGradient    extract wetting phase saturation gradient
//   .eval_SnGradient    extract non-wetting phase saturation gradient
//   .isValidState       T/F: determine if state is physically valid (e.g. pn > 0 and Sw >= 0 and Sw <= 1)
//   .setfromPrimitive   set from primitive variable array
//
//   .dump                debug dump of private data
//----------------------------------------------------------------------------//


// primitive variables (pn, Sw)
class QTypePrimitive_pnSw;


// primary template
template <class QType, class CapillaryModel, template <class> class PDETraitsSize>
class Q1D;


template <class CapillaryModel, template <class> class PDETraitsSize>
class Q1D<QTypePrimitive_pnSw, CapillaryModel, PDETraitsSize>
{
public:
  typedef PhysD1 PhysDim;

  static const int D = PhysDim::D;                   // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;    // total solution variables

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>; // solution arrays

  explicit Q1D( const CapillaryModel& pc ) : pc_(pc) {}
  Q1D( const Q1D& q0 ) : pc_(q0.pc_) {}
  ~Q1D() {}

  Q1D& operator=( const Q1D& );

  // evaluate primitive variables
  template<class T>
  void eval( const ArrayQ<T>& q, T& pw, T& pn, T& Sw, T& Sn ) const;
  template<class T>
  void eval_pw( const ArrayQ<T>& q, T& pw ) const; //wetting phase pressure - pw
  template<class T>
  void eval_pn( const ArrayQ<T>& q, T& pn ) const; //non-wetting phase pressure - pn
  template<class T>
  void eval_Sw( const ArrayQ<T>& q, T& Sw ) const; //wetting phase saturation - Sw
  template<class T>
  void eval_Sn( const ArrayQ<T>& q, T& Sn ) const; //non-wetting phase saturation - Sn

  template<class Tq, class Tg, class T>
  void eval_pwGradient( const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, T& pwx ) const;
  template<class Tq, class Tg, class T>
  void eval_pnGradient( const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, T& pnx ) const;
  template<class Tq, class Tg, class T>
  void eval_SwGradient( const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, T& Swx ) const;
  template<class Tq, class Tg, class T>
  void eval_SnGradient( const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, T& Snx ) const;

  template<class T>
  void jacobian_pw( const ArrayQ<T>& q, ArrayQ<T>& pw_q ) const;
  template<class T>
  void jacobian_pn( const ArrayQ<T>& q, ArrayQ<T>& pn_q ) const;
  template<class T>
  void jacobian_Sw( const ArrayQ<T>& q, ArrayQ<T>& Sw_q ) const;
  template<class T>
  void jacobian_Sn( const ArrayQ<T>& q, ArrayQ<T>& Sn_q ) const;

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const;

  // set from primitive variable array
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const Real data[], const std::string name[], int nn ) const;

  // set from variables
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const PressureNonWet_SaturationWet<T>& data ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const CapillaryModel& pc_; //Capillary pressure model
};

template <class CapillaryModel, template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitive_pnSw, CapillaryModel, PDETraitsSize>::eval( const ArrayQ<T>& q, T& pw, T& pn, T& Sw, T& Sn ) const
{
  pn = q(0);
  Sw = q(1);
  Sn = 1.0 - Sw;

  Real dummy = 0.0;
  T pc  = pc_.capPressure(dummy, dummy, Sn);

  pw = pn - pc;

}

template <class CapillaryModel, template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitive_pnSw, CapillaryModel, PDETraitsSize>::eval_pw( const ArrayQ<T>& q, T& pw ) const
{
  T Sn = 1.0 - q(1);

  Real dummy = 0.0;
  T pc = pc_.capPressure(dummy, dummy, Sn);

  pw = q(0) - pc;
}

template <class CapillaryModel, template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitive_pnSw, CapillaryModel, PDETraitsSize>::eval_pn( const ArrayQ<T>& q, T& pn ) const
{
  pn = q(0);
}

template <class CapillaryModel, template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitive_pnSw, CapillaryModel, PDETraitsSize>::eval_Sw( const ArrayQ<T>& q, T& Sw ) const
{
  Sw = q(1);
}

template <class CapillaryModel, template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitive_pnSw, CapillaryModel, PDETraitsSize>::eval_Sn( const ArrayQ<T>& q, T& Sn ) const
{
  Sn = 1.0 - q(1);
}

template <class CapillaryModel, template <class> class PDETraitsSize>
template <class Tq, class Tg, class T>
inline void
Q1D<QTypePrimitive_pnSw, CapillaryModel, PDETraitsSize>::eval_pwGradient( const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, T& pwx ) const
{
  Tq Sn = 1.0 - q(1);
  Tg Snx = -qx(1);

  Real dummy = 0.0;
  T pcx;
  pc_.capPressureGradient(dummy, dummy, Sn, Snx, pcx);

  pwx = qx(0) - pcx;
}

template <class CapillaryModel, template <class> class PDETraitsSize>
template <class Tq, class Tg, class T>
inline void
Q1D<QTypePrimitive_pnSw, CapillaryModel, PDETraitsSize>::eval_pnGradient( const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, T& pnx ) const
{
  pnx = qx(0);
}

template <class CapillaryModel, template <class> class PDETraitsSize>
template <class Tq, class Tg, class T>
inline void
Q1D<QTypePrimitive_pnSw, CapillaryModel, PDETraitsSize>::eval_SwGradient( const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, T& Swx ) const
{
  Swx = qx(1);
}

template <class CapillaryModel, template <class> class PDETraitsSize>
template <class Tq, class Tg, class T>
inline void
Q1D<QTypePrimitive_pnSw, CapillaryModel, PDETraitsSize>::eval_SnGradient( const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, T& Snx ) const
{
  Snx = -qx(1);
}

template <class CapillaryModel, template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitive_pnSw, CapillaryModel, PDETraitsSize>::jacobian_pw( const ArrayQ<T>& q, ArrayQ<T>& pw_q ) const
{
  T Sn = 1.0 - q(1);

  Real dummy = 0.0;
  T pc_Sn;
  pc_.capPressureJacobian( dummy, dummy, Sn, pc_Sn );
  T pc_Sw = -pc_Sn;

  // pw = pn - pc(Sw)
  pw_q(0) = 1.0; //dpw/dpn
  pw_q(1) = -pc_Sw; //dpw/dSw
}

template <class CapillaryModel, template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitive_pnSw, CapillaryModel, PDETraitsSize>::jacobian_pn( const ArrayQ<T>& q, ArrayQ<T>& pn_q ) const
{
  pn_q(0) = 1.0; //dpn/dpn
  pn_q(1) = 0.0; //dpn/dSw
}

template <class CapillaryModel, template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitive_pnSw, CapillaryModel, PDETraitsSize>::jacobian_Sw( const ArrayQ<T>& q, ArrayQ<T>& Sw_q ) const
{
  Sw_q(0) = 0.0; //dSw/dpn
  Sw_q(1) = 1.0; //dSw/dSw
}

template <class CapillaryModel, template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitive_pnSw, CapillaryModel, PDETraitsSize>::jacobian_Sn( const ArrayQ<T>& q, ArrayQ<T>& Sn_q ) const
{
  Sn_q(0) = 0.0; //dSn/dpn
  Sn_q(1) = -1.0; //dSn/dSw
}

// is state physically valid: check for 0 <= Sw <= 1
template <class CapillaryModel, template <class> class PDETraitsSize>
inline bool
Q1D<QTypePrimitive_pnSw, CapillaryModel, PDETraitsSize>::isValidState( const ArrayQ<Real>& q ) const
{
  Real pn = q(0);
  Real Sw = q(1);

#if 0
  printf( "%s: Sw = %e\n", __func__, Sw );
#endif

  return ((pn > 0) && (Sw >= 0) && (Sw <= 1));
}

template <class CapillaryModel, template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitive_pnSw, CapillaryModel, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const Real data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn == N);
  SANS_ASSERT(name[0] == "pn");
  SANS_ASSERT(name[1] == "Sw");

  q(0) = data[0]; //pn
  q(1) = data[1]; //Sw
}

template <class CapillaryModel, template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitive_pnSw, CapillaryModel, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const PressureNonWet_SaturationWet<T>& data ) const
{
  q(0) = data.pn;
  q(1) = data.Sw;
}

}

#endif  // Q1DPRIMITIVE_PNSW_H
