// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "SourceTwoPhase1D.h"

#define PYDICT_INSTANTIATE
#include "Python/PyDict_impl.h"

namespace SANS
{

template ParameterOption<SourceTwoPhase1DParam::SourceOptions>::ExtractType
PyDict::get(const ParameterType<ParameterOption<SourceTwoPhase1DParam::SourceOptions>>&) const;

// cppcheck-suppress passedByValue
void SourceTwoPhase1DType_FixedWellPressure_Param::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.pB));
  d.checkUnknownInputs(allParams);
}
SourceTwoPhase1DType_FixedWellPressure_Param SourceTwoPhase1DType_FixedWellPressure_Param::params;

// cppcheck-suppress passedByValue
void SourceTwoPhase1DParam::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Source));
  allParams.push_back(d.checkInputs(params.xmin));
  allParams.push_back(d.checkInputs(params.xmax));
  allParams.push_back(d.checkInputs(params.Tmin));
  allParams.push_back(d.checkInputs(params.Tmax));
  allParams.push_back(d.checkInputs(params.smoothLx));
  allParams.push_back(d.checkInputs(params.smoothT));
  d.checkUnknownInputs(allParams);
}
SourceTwoPhase1DParam SourceTwoPhase1DParam::params;

// cppcheck-suppress passedByValue
void SourceTwoPhase1DListParam::checkInputs(PyDict d)
{
  // Extract the keys from the dictionary
  std::vector<std::string> keys = d.stringKeys();

  for (std::size_t i = 0; i < keys.size(); i++)
  {
    // Create a source parameter for the given key
    const ParameterDict sourceDictParam{keys[i], NO_DEFAULT, SourceTwoPhase1DParam::checkInputs, "Source dictionary"};
    d.checkInputs(sourceDictParam);
  }
}
SourceTwoPhase1DListParam SourceTwoPhase1DListParam::params;

} //namespace SANS
