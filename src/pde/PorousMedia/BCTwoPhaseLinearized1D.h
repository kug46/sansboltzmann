// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCTWOPHASELINEARIZED1D_H
#define BCTWOPHASELINEARIZED1D_H

// 1-D Linearized two-phase BC class

//PyDict must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/BCCategory.h"
#include "pde/BCNone.h"
#include "Topology/Dimension.h"

#include <iostream>
#include <memory> // shared_ptr

#include <boost/mpl/vector.hpp>

#include "PDETwoPhaseLinearized1D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// BC types
//----------------------------------------------------------------------------//

class BCTypeFullState;

//----------------------------------------------------------------------------//
// BC class: 1-D Linearized TwoPhase
//
// template parameters:
//   PhysDim              Physical dimensions
//   BCType               BC type
//----------------------------------------------------------------------------//

template <class BCType, class PDE>
class BCTwoPhaseLinearized1D;

template <class BCType>
struct BCTwoPhaseLinearized1DParams;


// Define the vector of boundary conditions
template <class PDE>
using BCTwoPhaseLinearized1DVector =  boost::mpl::vector2< BCNone<PhysD1,PDE::N>,
                                                           BCTwoPhaseLinearized1D<BCTypeFullState, PDE>
                                                         >;

template<>
struct BCTwoPhaseLinearized1DParams<BCTypeFullState> : noncopyable
{
  BCTwoPhaseLinearized1DParams();

  const ParameterOption<TwoPhaseVariableTypeParams::TwoPhaseVariableOptions>& StateVector;

  static constexpr const char* BCName{"FullState"};
  struct Option
  {
    const DictOption FullState{BCTwoPhaseLinearized1DParams::BCName, BCTwoPhaseLinearized1DParams::checkInputs};
  };

  static void checkInputs(PyDict d);
  static BCTwoPhaseLinearized1DParams params;
};


template <class PDE>
class BCTwoPhaseLinearized1D<BCTypeFullState, PDE> :
    public BCType< BCTwoPhaseLinearized1D<BCTypeFullState, PDE> >
{
public:
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCTwoPhaseLinearized1DParams<BCTypeFullState> ParamsType;

  typedef PhysD1 PhysDim;
  static const int D = PDE::D; // physical dimensions
  static const int N = PDE::N; // total solution variables

  static const int NBC = 2;         // total BCs

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;   // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>; // matrices

  // cppcheck-suppress noExplicitConstructor
  template <class Variables>
  BCTwoPhaseLinearized1D( const PDE& pde, const TwoPhaseVariableType<Variables>& data ) :
      pde_(pde),
      qB_(pde.setDOFFrom(data)){}

  BCTwoPhaseLinearized1D(const PDE& pde, PyDict& d ) :
    pde_(pde),
    qB_(pde.setDOFFrom(d)){}

  virtual ~BCTwoPhaseLinearized1D() {}

  BCTwoPhaseLinearized1D( const BCTwoPhaseLinearized1D& ) = delete;
  BCTwoPhaseLinearized1D& operator=( const BCTwoPhaseLinearized1D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return pde_.hasFluxViscous(); }

  // BC data
  template <class T>
  void state( const Real& x, const Real& time, const Real& nx,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& time,
                   const Real& nx,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const ArrayQ<Real> qB_;
};

template <class PDE>
template <class T>
inline void
BCTwoPhaseLinearized1D<BCTypeFullState, PDE>::state(const Real& x, const Real& time, const Real& nx,
                                                    const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  qB = qB_;
}

template <class PDE_>
template <class T, class Tf>
inline void
BCTwoPhaseLinearized1D<BCTypeFullState, PDE_>::
fluxNormal( const Real& x, const Real& time,
            const Real& nx,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Add upwinded advective flux
  pde_.fluxAdvectiveUpwind(x, time, qI, qB, nx, Fn);

  ArrayQ<Tf> fv = 0;

  pde_.fluxViscous(x, time, qB, qIx, fv);

  // Add viscous flux
  Fn += fv*nx;
}

} //namespace SANS

#endif  // BCTWOPHASELINEARIZED1D_H
