// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "SolutionFunction_TwoPhase1D.h"

namespace SANS
{

// cppcheck-suppress passedByValue
void SolutionFunction_TwoPhase1D_3ConstSaturation_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.pn));
  allParams.push_back(d.checkInputs(params.Sw_left));
  allParams.push_back(d.checkInputs(params.Sw_mid));
  allParams.push_back(d.checkInputs(params.Sw_right));
  allParams.push_back(d.checkInputs(params.xLeftMid));
  allParams.push_back(d.checkInputs(params.xMidRight));
  allParams.push_back(d.checkInputs(params.Ltransit));
  d.checkUnknownInputs(allParams);
}
SolutionFunction_TwoPhase1D_3ConstSaturation_Params SolutionFunction_TwoPhase1D_3ConstSaturation_Params::params;

}
