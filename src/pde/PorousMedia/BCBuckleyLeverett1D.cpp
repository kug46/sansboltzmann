// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCBuckleyLeverett1D.h"

#include "Q1DPrimitive_Sw.h"
#include "RelPermModel_PowerLaw.h"
#include "CapillaryModel.h"
#include "TraitsBuckleyLeverett.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{
//===========================================================================//
//
// Parameter classes
//

// cppcheck-suppress passedByValue
void BCBuckleyLeverett1DParams<BCTypeTimeIC>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.qB));
  d.checkUnknownInputs(allParams);
}
BCBuckleyLeverett1DParams<BCTypeTimeIC> BCBuckleyLeverett1DParams<BCTypeTimeIC>::params;

// cppcheck-suppress passedByValue
void BCBuckleyLeverett1DParams<BCTypeLinearRobin>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.A));
  allParams.push_back(d.checkInputs(params.B));
  allParams.push_back(d.checkInputs(params.bcdata));
  d.checkUnknownInputs(allParams);
}
BCBuckleyLeverett1DParams<BCTypeLinearRobin> BCBuckleyLeverett1DParams<BCTypeLinearRobin>::params;

// cppcheck-suppress passedByValue
void BCBuckleyLeverett1DParams<BCTypeDirichlet_mitState>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.qB));
  d.checkUnknownInputs(allParams);
}
BCBuckleyLeverett1DParams<BCTypeDirichlet_mitState> BCBuckleyLeverett1DParams<BCTypeDirichlet_mitState>::params;

// cppcheck-suppress passedByValue
void BCBuckleyLeverett1DParams<BCTypeDirichlet_mitStateSpaceTime>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.qB));
  allParams.push_back(d.checkInputs(params.isOutflow));
  d.checkUnknownInputs(allParams);
}
BCBuckleyLeverett1DParams<BCTypeDirichlet_mitStateSpaceTime> BCBuckleyLeverett1DParams<BCTypeDirichlet_mitStateSpaceTime>::params;

// cppcheck-suppress passedByValue
void BCBuckleyLeverett1DParams<BCTypeFunction_mitState>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Function));
  d.checkUnknownInputs(allParams);
}
BCBuckleyLeverett1DParams<BCTypeFunction_mitState> BCBuckleyLeverett1DParams<BCTypeFunction_mitState>::params;

// cppcheck-suppress passedByValue
void BCBuckleyLeverett1DParams<BCTypeTimeIC_Function>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Function));
  d.checkUnknownInputs(allParams);
}
BCBuckleyLeverett1DParams<BCTypeTimeIC_Function> BCBuckleyLeverett1DParams<BCTypeTimeIC_Function>::params;

//===========================================================================//

typedef TraitsModelBuckleyLeverett<QTypePrimitive_Sw, RelPermModel_PowerLaw, CapillaryModel_Linear> TraitsModel_RelPower_CapLinear;
typedef TraitsModelBuckleyLeverett<QTypePrimitive_Sw, RelPermModel_PowerLaw, CapillaryModel_None> TraitsModel_RelPower_CapNone;

typedef PDEBuckleyLeverett1D<TraitsSizeBuckleyLeverett, TraitsModel_RelPower_CapLinear> PDE_RelPower_CapLinear;
typedef PDEBuckleyLeverett1D<TraitsSizeBuckleyLeverett, TraitsModel_RelPower_CapNone> PDE_RelPower_CapNone;

// Instantiate the BC parameters
BCPARAMETER_INSTANTIATE( BCBuckleyLeverett1DVector<PDE_RelPower_CapLinear> )
BCPARAMETER_INSTANTIATE( BCBuckleyLeverett1DVector<PDE_RelPower_CapNone> )

//===========================================================================//

template <class PDE_>
void
BCBuckleyLeverett1D<BCTypeTimeIC, PDE_>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCBuckleyLeverett1D<BCTypeTimeIC>" << std::endl;
}

template <class PDE_>
void
BCBuckleyLeverett1D<BCTypeLinearRobin, PDE_>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCBuckleyLeverett1D<BCTypeLinearRobin>:" << std::endl;
  out << indent << "  A_ = " << A_ << std::endl;
  out << indent << "  B_ = " << B_ << std::endl;
  out << indent << "  bcdata_ = " << bcdata_ << std::endl;
}

template <class PDE_>
void
BCBuckleyLeverett1D<BCTypeDirichlet_mitState, PDE_>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCBuckleyLeverett1D<BCTypeDirichlet_mitState>:" << std::endl;
  out << indent << "  qB_ = " << qB_ << std::endl;
}

// Explicit instantiations
template class BCNone<PhysD1,TraitsSizeBuckleyLeverett<PhysD1>::N>;
template class BCBuckleyLeverett1D< BCTypeTimeIC            , PDE_RelPower_CapLinear >;
template class BCBuckleyLeverett1D< BCTypeLinearRobin       , PDE_RelPower_CapLinear >;
template class BCBuckleyLeverett1D< BCTypeDirichlet_mitState, PDE_RelPower_CapLinear >;

} //namespace SANS
