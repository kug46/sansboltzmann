// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "Q1DPrimitive_SurrogateSw.h"
#include "TraitsBuckleyLeverettArtificialViscosity.h"

namespace SANS
{

template <template <class> class PDETraitsSize>
void
Q1D<QTypePrimitive_SurrogateSw, PDETraitsSize>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "Q1D<QTypePrimitive_SurrogateSw>" << std::endl;
}

//=============================================================================
//Explicitly instantiate
template class Q1D<QTypePrimitive_SurrogateSw, TraitsSizeBuckleyLeverett>;
template class Q1D<QTypePrimitive_SurrogateSw, TraitsSizeBuckleyLeverettArtificialViscosity>;

}
