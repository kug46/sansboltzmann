// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "CapillaryModel.h"
#include "TraitsTwoPhase.h"
#include "Q1DPrimitive_Surrogate_pnSw.h"

namespace SANS
{

template <class CapillaryModel, template <class> class PDETraitsSize>
void
Q1D<QTypePrimitive_Surrogate_pnSw, CapillaryModel, PDETraitsSize>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  std::string indent2(indentSize+2, ' ');

  out << indent << "Q1D<QTypePrimitive_Surrogate_pnSw>" << std::endl;
  out << indent2 << "pc_ = " << std::endl;
  pc_.dump(indentSize+4, out);
}

//=============================================================================
//Explicitly instantiate
template class Q1D<QTypePrimitive_Surrogate_pnSw, CapillaryModel_None, TraitsSizeTwoPhase>;
template class Q1D<QTypePrimitive_Surrogate_pnSw, CapillaryModel_Linear, TraitsSizeTwoPhase>;

}
