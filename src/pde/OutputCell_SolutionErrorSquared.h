// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef OUTPUTCELL_SOLUTIONERRORSQUARED_H
#define OUTPUTCELL_SOLUTIONERRORSQUARED_H

// Python must be included first
#include "Python/PyDict.h"

#include "tools/SANSnumerics.h"     // Real

#include "OutputCategory.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Pow.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Diag.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Element cell integrand: solution error squared

template <class PDE_, class SolutionFunction>
class OutputCell_SolutionErrorSquared :
    public OutputType< OutputCell_SolutionErrorSquared<PDE_, SolutionFunction> >
{
public:
  typedef PDE_ PDE;
  typedef typename PDE::PhysDim PhysDim;
  static const int D = PhysDim::D;

  //Array of solution variables
  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  // Array of outputs
  template<class T>
  using ArrayJ = typename PDE::template ArrayQ<T>;

  // Matrix required to represent the Jacobian of this output functional
  template<class T>
  using MatrixJ = typename PDE::template ArrayQ<T>;

  explicit OutputCell_SolutionErrorSquared( const SolutionFunction& solFcn ) : solFcn_(solFcn) {}
  explicit OutputCell_SolutionErrorSquared( const PyDict& d ) : solFcn_(d) {}

  bool needsSolutionGradient() const { return false; }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, ArrayJ<To>& output ) const
  {
    ArrayQ<Real> qExact = solFcn_(x, time);
    ArrayQ<Tq> dq = q - qExact;
    output = pow(dq, 2.0);
  }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    ArrayQ<Real> qExact = solFcn_(x, y, time);
    ArrayQ<Tq> dq = q - qExact;
    output = pow(dq, 2.0);
  }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& z, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz, ArrayJ<To>& output ) const
  {
    ArrayQ<Real> qExact = solFcn_(x, y, z, time);
    ArrayQ<Tq> dq = q - qExact;
    output = pow(dq, 2.0);
  }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& z, const Real& t,
                  const ArrayQ<Tq>& q,
                  const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz, const ArrayQ<Tg>& qt,
                  ArrayJ<To>& output ) const
  {
    ArrayQ<Real> qExact = solFcn_(x, y, z, t);
    ArrayQ<Tq> dq = q - qExact;
    output = pow(dq, 2.0);
  }

  template<class Tq, class Tg, class To>
  void outputJacobian(const Real& x, const Real& time,
                      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, MatrixJ<To>& dJdu) const
  {
    ArrayQ<Real> qExact = solFcn_(x, time);
    ArrayQ<Tq> dq = q - qExact;

    dJdu += 2*dq;
  }

  template<class Tq, class Tg, class To>
  void outputJacobian(const Real& x, const Real& y, const Real& time,
                      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, MatrixJ<To>& dJdu) const
  {
    ArrayQ<Real> qExact = solFcn_(x, y, time);
    ArrayQ<Tq> dq = q - qExact;

    dJdu += 2*dq;
  }


  template<class Tq, class Tg, class To>
  void outputJacobian(const Real& x, const Real& y, const Real& z, const Real& time,
                      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
                      MatrixJ<To>& dJdu) const
  {
    ArrayQ<Real> qExact = solFcn_(x, y, z, time);
    ArrayQ<Tq> dq = q - qExact;

    dJdu += 2*dq;
  }

protected:
  const SolutionFunction& solFcn_;
};

}

#endif //OUTPUTCELL_SOLUTIONERRORSQUARED_H
