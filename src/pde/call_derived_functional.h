// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef CALL_DERIVED_FUNCTIONAL_H
#define CALL_DERIVED_FUNCTIONAL_H

#include <type_traits>
#include <typeinfo>

#include <boost/mpl/begin_end.hpp>
#include <boost/mpl/next.hpp>
#include <boost/mpl/deref.hpp>

#include "tools/SANSException.h"
#include "tools/demangle.h"

#include "OutputCategory.h"

namespace SANS
{

namespace detail
{

// Looping class. Check the current derived type stored in 'iter' to see if the 'base' is a base class of it.
// Otherwise continue to the next derived class in the sequence.
template < class iter, class end, class ParamT, class VectorX, class ArrayQ, class VectorArrayQ, class ArrayJ>
struct call_derived_functional
{
  typedef typename boost::mpl::deref<iter>::type currentType;
  typedef typename boost::mpl::next< iter >::type nextType;

  static bool call( const std::type_info& outputType, const OutputBase& outputFcn,
                    const ParamT& param, VectorX& N, ArrayQ& q, VectorArrayQ& gradq, ArrayJ& integrand )
  {
    // If the current type matches the derived type, then call f with the base class casted
    if (typeid(currentType) == outputType)
    {
      outputFcn.template cast<currentType>().operator()(param, N, q, gradq, integrand);
      return false;
    }

    // Check the next type to see if it's the derived type
    return call_derived_functional<nextType, end, ParamT, VectorX, ArrayQ, VectorArrayQ, ArrayJ >::
        call(outputType, outputFcn, param, N, q, gradq, integrand);
  }
};

// Recursive loop terminates when 'iter' is the same as 'end'
template < class end, class ParamT, class VectorX, class ArrayQ, class VectorArrayQ, class ArrayJ >
struct call_derived_functional< end, end, ParamT, VectorX, ArrayQ, VectorArrayQ, ArrayJ >
{
  static bool call( const std::type_info& outputType, const OutputBase& outputFcn,
                    const ParamT& param, VectorX& N, ArrayQ& q, VectorArrayQ& gradq, ArrayJ& integrand )
  {
    // Did not find the derived type of base in the sequence
    return true;
  }
};

} // namespace detail

// BasisWeighted interface
template < class NDOutputVector, class ParamT, class VectorX, class ArrayQ, class VectorArrayQ, class ArrayJ>
void call_derived_functional( const OutputBase& outputFcn,
                              const ParamT& param, VectorX& N, ArrayQ& q, VectorArrayQ& gradq, ArrayJ& integrand )
{
  // Get the start and end iterators from the sequence
  typedef typename boost::mpl::begin< NDOutputVector >::type begin;
  typedef typename boost::mpl::end< NDOutputVector >::type   end;

  // Call the implementation and throw an exception if the derived class is not in NDBCVector
  if ( detail::call_derived_functional< begin, end, ParamT, VectorX, ArrayQ, VectorArrayQ, ArrayJ >::
               call(outputFcn.derivedTypeID(), outputFcn, param, N, q, gradq, integrand) )
    SANS_DEVELOPER_EXCEPTION("\nCould not find derived type:\n\n%s\n\nin the sequence:\n\n%s",
                             demangle(outputFcn.derivedTypeID().name()).c_str(), demangle(typeid(NDOutputVector).name()).c_str() );
}

}

#endif //SANS_CALL_WITH_DERIVED_BC_H
