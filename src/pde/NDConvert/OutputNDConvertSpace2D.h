// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef OUTPUTNDCONVERTSPACE2D_H
#define OUTPUTNDCONVERTSPACE2D_H

// 2D NDConvert Output class for steady PDEs

// Python must be included first
#include "Python/PyDict.h"

#include <typeinfo> // typeid

#include "tools/SANSnumerics.h"     // Real
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "Topology/Dimension.h"
#include "Field/Tuple/ParamTuple.h"

#include "OutputNDConvert_fwd.h"
#include "GlobalTime.h"
#include "Temporal.h"

namespace SANS
{

template <class OutputFunctional>
class OutputNDConvertSpace<PhysD2, OutputFunctional> : public OutputFunctional
{
public:
  static_assert( std::is_same<PhysD2, typename OutputFunctional::PhysDim>::value, "Physical dimensions should match" );

  static const int D = PhysD2::D;

  typedef DLA::VectorS<D,Real> VectorX;

  template<class Z> using ArrayQ          = typename OutputFunctional::template ArrayQ<Z>;  // solution arrays
  template<class Z> using VectorArrayQ    = DLA::VectorS<D, ArrayQ<Z> >;                    // solution gradient arrays
  template<class Z> using ArrayJ          = typename OutputFunctional::template ArrayJ<Z>;  // output arrays
  template<class Z> using MatrixJ         = typename OutputFunctional::template MatrixJ<Z>; // output jacobians

  using OutputFunctional::operator();

  template< class... OutputArgs > // cppcheck-suppress noExplicitConstructor
  OutputNDConvertSpace(OutputArgs&&... args) : OutputFunctional(std::forward<OutputArgs>(args)...), time_(dummyTime_) {}

  template< class... OutputArgs > // cppcheck-suppress noExplicitConstructor
  OutputNDConvertSpace(GlobalTime& time, OutputArgs&&... args) : OutputFunctional(std::forward<OutputArgs>(args)...), time_(time.time) {}

  explicit OutputNDConvertSpace( const PyDict& d ) : OutputFunctional(d), time_(dummyTime_) {}

  OutputNDConvertSpace(const GlobalTime& time, const PyDict& d ) : OutputFunctional(d), time_(time.time) {}

  OutputNDConvertSpace( const OutputNDConvertSpace& ) = delete;
  OutputNDConvertSpace& operator=( const OutputNDConvertSpace& ) = delete;

  virtual ~OutputNDConvertSpace() {}

  TemporalMode temporal() const { return &time_ == &dummyTime_ ? eSteady : eUnsteady; }

  virtual const std::type_info& derivedTypeID() const override { return typeid(*this); }

  template<class Tq, class Tg, class To>
  void operator()( const VectorX& X,
                   const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, ArrayJ<To>& output ) const
  {
    OutputFunctional::operator()(X[0], X[1], time_, q, gradq[0], gradq[1], output);
  }

  template<class Tq, class Tg, class To>
  void operator()( const VectorX& X, const VectorX& N,
                   const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, ArrayJ<To>& output ) const
  {
    OutputFunctional::operator()(X[0], X[1], time_, N[0], N[1], q, gradq[0], gradq[1], output);
  }

  void operator()( const VectorX& X, ArrayQ<Real>& weights ) const
  {
    OutputFunctional::operator()(X[0], X[1], time_, weights);
  }

  template<class L, class Tq, class Tg, class Tout>
  void operator()( const ParamTuple<L, VectorX, TupleClass<>>& param,
                   const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, ArrayJ<Tout>& output ) const
  {
    const VectorX& X = param.right();
    OutputFunctional::operator()(param.left(), X[0], X[1], time_, q, gradq[0], gradq[1], output);
  }

  template<class L, class Tq, class Tg, class Tout>
  void operator()( const ParamTuple<L, VectorX, TupleClass<>>& param, const VectorX& N,
                   const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, ArrayJ<Tout>& output ) const
  {
    const VectorX& X = param.right();
    OutputFunctional::operator()(param.left(), X[0], X[1], time_, N[0], N[1], q, gradq[0], gradq[1], output);
  }

  template<class Tq, class Tg, class To>
  void outputJacobian( const VectorX& X,
                       const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, MatrixJ<To>& dJdu ) const
  {
    OutputFunctional::outputJacobian(X[0], X[1], time_, q, gradq[0], gradq[1], dJdu);
  }

  template<class L, class Tq, class Tg, class To>
  void outputJacobian( const ParamTuple<L, VectorX, TupleClass<>>& param,
                       const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, MatrixJ<To>& dJdu ) const
  {
    const VectorX& X = param.right();
    OutputFunctional::outputJacobian(param.left(), X[0], X[1], time_, q, gradq[0], gradq[1], dJdu);
  }



private:
  const Real dummyTime_ = 0;
  const Real& time_;
};

} //namespace SANS

#endif  // OUTPUTNDCONVERTSPACE2D_H
