// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCNDCONVERTSPACETIME3D_H
#define BCNDCONVERTSPACETIME3D_H

// 3-D NDConvert BC class for Spacetime

#include "Python/PyDict.h" //Python must be included first

#include <typeinfo> // typeid

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"

#include "Field/Tuple/ParamTuple.h"

#include "pde/BCCategory.h"

#include "BCNDConvert_fwd.h"
#include "Temporal.h"

namespace SANS
{
//===========================================================================//
// NDConvert for purely spatial BCs in a SpaceTime context
//===========================================================================//

template <class BC>
class BCNDConvertSpaceTime<PhysD3, BC> : public BC
{
public:
  static_assert( std::is_same<PhysD3, typename BC::PhysDim>::value, "Physical dimensions should match" );

  typedef typename BC::Category Category;

  static const int D = BC::D+1;                     // physical dimensions
  static const int N = BC::N;                       // total solution variables

  static const int NBC = BC::NBC;                   // total BCs

  template<class T> using ArrayQ        = typename BC::template ArrayQ<T>;
  template<class T> using MatrixQ       = typename BC::template MatrixQ<T>;    // matrices
  template<class Z> using VectorArrayQ  = DLA::VectorS<D, ArrayQ<Z> >;         // vector of solution arrays, i.e. solution gradients/flux vector

  typedef DLA::VectorS<D,Real> VectorXT;

  template< class... BCArgs > // cppcheck-suppress noExplicitConstructor
  BCNDConvertSpaceTime(BCArgs&&... args) : BC(std::forward<BCArgs>(args)...) {}

  template<class PDE>
  BCNDConvertSpaceTime(const PDE& pde, const PyDict& d) : BC(pde, d) {}

  virtual ~BCNDConvertSpaceTime() {}

  TemporalMode temporal() const { return eSpaceTime; }

  BCNDConvertSpaceTime( const BCNDConvertSpaceTime& ) = delete;
  BCNDConvertSpaceTime& operator=( const BCNDConvertSpaceTime& ) = delete;

  virtual const std::type_info& derivedTypeID() const override { return typeid(*this); }

  // BC coefficients:  A u + B (kn un + ks us)
  template <class T>
  void coefficients(
      const VectorXT& X, const VectorXT& Nrm,
      MatrixQ<T>& A, MatrixQ<T>& B ) const
  {
    BC::coefficients(X[0], X[1], X[2], X[3], Nrm[0], Nrm[1], Nrm[2], A, B);
  }

  template <class T>
  void data( const VectorXT& X, const VectorXT& Nrm, ArrayQ<T>& bcdata ) const
  {
    BC::data(X[0], X[1], X[2], X[3], Nrm[0], Nrm[1], Nrm[2], bcdata);
  }

  template <class T>
  void state( const VectorXT& X, const VectorXT& Nrm, const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BC::state(X[0], X[1], X[2], X[3], Nrm[0], Nrm[1], Nrm[2], qI, qB);
  }

  template <class T>
  void fluxNormal( const VectorXT& X, const VectorXT& Nrm, const ArrayQ<T>& qI, const VectorArrayQ<T>& gradqI,
                   const ArrayQ<T>& qB, ArrayQ<T>& Fn ) const
  {
    BC::fluxNormal(X[0], X[1], X[2], X[3], Nrm[0], Nrm[1], Nrm[2], qI, gradqI[0], gradqI[1], gradqI[2], qB, Fn);
  }

  template <class T>
  void strongBC( const VectorXT& X, const VectorXT& Nrm, const ArrayQ<T>& qI, const VectorArrayQ<T>& gradqI, ArrayQ<T>& rsdBC ) const
  {
    BC::strongBC(X[0], X[1], X[2], X[3], Nrm[0], Nrm[1], Nrm[2], qI, gradqI[0], gradqI[1], gradqI[2], rsdBC);
  }

  bool isValidState( const VectorXT& Nrm, const ArrayQ<Real>& q ) const
  {
    return BC::isValidState(Nrm[0], Nrm[1], Nrm[2], q);
  }
};

//===========================================================================//
// Specialization for SpaceTime BCs
//===========================================================================//

template <class BC>
class BCNDConvertSpaceTime<PhysD3, SpaceTimeBC<BC>> : public BC
{
public:
  static_assert( std::is_same<PhysD3, typename BC::PhysDim>::value, "Physical dimensions should match" );

  typedef typename BC::Category Category;

  static const int D = BC::D+1;                     // physical dimensions
  static const int N = BC::N;                       // total solution variables

  static const int NBC = BC::NBC;                   // total BCs

  typedef DLA::VectorS<D,Real> VectorXT;

  template<class T> using ArrayQ        = typename BC::template ArrayQ<T>;
  template<class T> using MatrixQ       = typename BC::template MatrixQ<T>;    // matrices
  template<class Z> using VectorArrayQ  = DLA::VectorS<D, ArrayQ<Z> >;         // vector of solution arrays, i.e. solution gradients/flux vector

  template< class... BCArgs >  // cppcheck-suppress noExplicitConstructor
  BCNDConvertSpaceTime(BCArgs&&... args) : BC(std::forward<BCArgs>(args)...) {}

  template<class PDE>
  BCNDConvertSpaceTime(const PDE& pde, const PyDict& d) : BC(pde, d) {}

  virtual ~BCNDConvertSpaceTime() {}

  BCNDConvertSpaceTime( const BCNDConvertSpaceTime& ) = delete;
  BCNDConvertSpaceTime& operator=( const BCNDConvertSpaceTime& ) = delete;

  TemporalMode temporal() const { return eSpaceTime; }

  virtual const std::type_info& derivedTypeID() const override { return typeid(*this); }

  template <class T>
  void state( const VectorXT& X, const VectorXT& Nrm, const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BC::state(X[0], X[1], X[2], X[3], Nrm[0], Nrm[1], Nrm[2], Nrm[3], qI, qB);
  }

  template <class L, class T, class B>
  void state( const ParamTuple<L, VectorXT, TupleClass<>>& param, const VectorXT& Nrm, const ArrayQ<T>& qI, ArrayQ<B>& qB ) const
  {
    const VectorXT& X = param.right();
    BC::state(param.left(), X[0], X[1], X[2], X[3], Nrm[0], Nrm[1], Nrm[2], Nrm[3], qI, qB);
  }

  template <class T>
  void fluxNormal( const VectorXT& X, const VectorXT& Nrm, const ArrayQ<T>& qI, const VectorArrayQ<T>& gradqI,
                   const ArrayQ<T>& qB, ArrayQ<T>& Fn ) const
  {
    BC::fluxNormalSpaceTime(X[0], X[1], X[2], X[3], Nrm[0], Nrm[1], Nrm[2], Nrm[3],
                            qI, gradqI[0], gradqI[1], gradqI[2], gradqI[3], qB, Fn);
  }

  template <class L, class T, class Tf>
  void fluxNormal( const ParamTuple<L, VectorXT, TupleClass<>>& param, const VectorXT& Nrm, const ArrayQ<T>& qI, const VectorArrayQ<T>& gradqI,
                   ArrayQ<T>& qB, ArrayQ<Tf>& Fn ) const
  {
    const VectorXT& X = param.right();
    BC::fluxNormalSpaceTime(param.left(), X[0], X[1], X[2], X[3], Nrm[0], Nrm[1], Nrm[2], Nrm[3],
                            qI, gradqI[0], gradqI[1], gradqI[2], gradqI[3], qB, Fn);
  }

  bool isValidState( const VectorXT& Nrm, const ArrayQ<Real>& q ) const
  {
    return BC::isValidState(Nrm[0], Nrm[1], Nrm[2], Nrm[3], q);
  }
};

} //namespace SANS

#endif  // BCNDCONVERTSPACETIME3D_H
