// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FUNCTIONNDCONVERT_FWD_H
#define FUNCTIONNDCONVERT_FWD_H

// Forward declare all the FunctionNDConvert classes

namespace SANS
{

template <class PhysDim, class ArrayQ>
class FunctionNDConvertSpace;

template <class PhysDim, class ArrayQ>
class FunctionNDConvertSpaceTime;

}

#endif //FUNCTIONNDCONVERT_FWD_H
