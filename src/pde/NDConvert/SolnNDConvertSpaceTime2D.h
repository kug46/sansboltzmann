// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOLNNDCONVERTSPACETIME2D_H
#define SOLNNDCONVERTSPACETIME2D_H

// 2D NDConvert Solution class for space-time

// Python must be included first
#include "Python/PyDict.h"

#include "tools/SANSnumerics.h"     // Real
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"
#include "Topology/Dimension.h"
#include "Surreal/SurrealS.h"

#include "SolnNDConvert_fwd.h"
#include "Temporal.h"

#include <iostream>
#include <string>

namespace SANS
{

template <class SOLN>
class SolnNDConvertSpaceTime<PhysD2, SOLN> : public SOLN
{
public:
  static_assert( std::is_same<PhysD2, typename SOLN::PhysDim>::value, "Physical dimensions should match" );

  template<class T>
  using ArrayQ = typename SOLN::template ArrayQ<T>;

  typedef PhysD3 PhysDim; // increase the physical dimension of the PDE to include time
  static const int D = PhysDim::D;

  typedef DLA::VectorS<D,Real> VectorXT;

  using SOLN::operator();
  using SOLN::gradient;
  using SOLN::secondGradient;

  template< class... SolnArgs > // cppcheck-suppress noExplicitConstructor
  SolnNDConvertSpaceTime(SolnArgs&&... args) : SOLN(std::forward<SolnArgs>(args)...) {}

  explicit SolnNDConvertSpaceTime( const PyDict& d ) : SOLN(d) {}

  SolnNDConvertSpaceTime& operator=( const SolnNDConvertSpaceTime& ) = delete;

  TemporalMode temporal() const { return eSpaceTime; }

  ArrayQ<Real> operator()( const VectorXT& X ) const
  {
    return SOLN::operator()(X[0], X[1], X[2]);
  }

  void gradient( const VectorXT& X, ArrayQ<Real>& q,
                 DLA::VectorS< D, ArrayQ<Real> >& gradq ) const
  {
    SOLN::gradient(X[0], X[1], X[2], q, gradq[0], gradq[1], gradq[2]);
  }

  void secondGradient( const VectorXT& X, ArrayQ<Real>& q,
                       DLA::VectorS< D, ArrayQ<Real> >& gradq,
                       DLA::MatrixSymS< D, ArrayQ<Real> >& hessianq ) const
  {
    SOLN::secondGradient(X[0], X[1], X[2], q, gradq[0], gradq[1], gradq[2],
                         hessianq(0,0),
                         hessianq(1,0), hessianq(1,1));
  }

};

} //namespace SANS

#endif  // SOLNNDCONVERTSPACETIME2D_H
