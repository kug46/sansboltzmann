// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOLNNDCONVERTSPACE3D_H
#define SOLNNDCONVERTSPACE3D_H

// 3D NDConvert Solution class for steady PDEs

// Python must be included first
#include "Python/PyDict.h"

#include "tools/SANSnumerics.h"     // Real
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"
#include "Topology/Dimension.h"
#include "Surreal/SurrealS.h"

#include "SolnNDConvert_fwd.h"
#include "GlobalTime.h"
#include "Temporal.h"

#include <iostream>
#include <string>

namespace SANS
{

template <class SOLN>
class SolnNDConvertSpace<PhysD3, SOLN> : public SOLN
{
public:
  static_assert( std::is_same<PhysD3, typename SOLN::PhysDim>::value, "Physical dimensions should match" );

  template<class T>
  using ArrayQ = typename SOLN::template ArrayQ<T>;

  static const int D = PhysD3::D;

  typedef DLA::VectorS<D,Real> VectorX;

  using SOLN::operator();

  template< class... SolnArgs > // cppcheck-suppress noExplicitConstructor
  SolnNDConvertSpace(SolnArgs&&... args) : SOLN(std::forward<SolnArgs>(args)...), time_(dummyTime_) {}

  template< class... SolnArgs >
  SolnNDConvertSpace(GlobalTime& time, SolnArgs&&... args) : SOLN(std::forward<SolnArgs>(args)...), time_(time.time) {}

  explicit SolnNDConvertSpace( const PyDict& d ) : SOLN(d), time_(dummyTime_) {}

  SolnNDConvertSpace( const GlobalTime& time, const PyDict& d ) : SOLN(d), time_(time.time) {}

  SolnNDConvertSpace& operator=( const SolnNDConvertSpace& ) = delete;

  TemporalMode temporal() const { return &time_ == &dummyTime_ ? eSteady : eUnsteady; }


  ArrayQ<Real> operator()( const VectorX& X ) const
  {
    return SOLN::operator()(X[0], X[1], X[2], time_);
  }

  void gradient( const VectorX& X, ArrayQ<Real>& q,
                 DLA::VectorS< D, ArrayQ<Real> >& gradQ ) const
  {
    ArrayQ<Real> qt;
    SOLN::gradient(X[0], X[1], X[2], time_, q, gradQ[0], gradQ[1], gradQ[2], qt);
  }


  void gradient( const Real &x, const Real &y, const Real &z, const Real &time, ArrayQ<Real>& q,
                 ArrayQ<Real>& qx, ArrayQ<Real>& qy, ArrayQ<Real>& qz, ArrayQ<Real>& qt ) const
  {
    SOLN::gradient(x, y, z, time_, q, qx, qy, qz, qt );
    if ( temporal() == eSteady )
      qt = 0; //No time derivative because of steady solution
  }

  void secondGradient( const VectorX& X, ArrayQ<Real>& q,
                       DLA::VectorS< D, ArrayQ<Real> >& gradQ,
                       DLA::MatrixSymS< D, ArrayQ<Real> >& hessianQ ) const
  {
    ArrayQ<Real> qt;
    SOLN::secondGradient(X[0], X[1], X[2], time_, q, gradQ[0], gradQ[1], gradQ[2], qt,
               hessianQ(0,0),
               hessianQ(1,0), hessianQ(1,1),
               hessianQ(2,0), hessianQ(2,1), hessianQ(2,2));
  }


  void secondGradient( const Real &x, const Real &y, const Real &z, const Real &time, ArrayQ<Real>& q,
                   ArrayQ<Real>& qx, ArrayQ<Real>& qy, ArrayQ<Real>& qz, ArrayQ<Real>& qt,
                   ArrayQ<Real>& qxx,
                   ArrayQ<Real>& qyx, ArrayQ<Real>& qyy,
                   ArrayQ<Real>& qzx, ArrayQ<Real>& qzy, ArrayQ<Real>& qzz) const
  {
    SOLN::secondGradient(x, y, z, time_, q, qx, qy, qz, qt, qxx, qyx, qyy, qzx, qzy, qzz );
    if ( temporal() == eSteady )
      qt = 0; //No time derivative because of marching solution
  }


protected:
  const Real dummyTime_ = 0;
  const Real& time_;
};

} //namespace SANS

#endif  // SOLNNDCONVERTSPACE3D_H
