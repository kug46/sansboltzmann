// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FUNCTIONNDCONVERTSPACE2D_H
#define FUNCTIONNDCONVERTSPACE2D_H

// 2D NDConvert function class

#include "tools/SANSnumerics.h"     // Real

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"
#include "LinearAlgebra/DenseLinAlg/tools/MatrixS_or_T.h"

#include "Topology/Dimension.h"

#include "FunctionNDConvert_fwd.h"
#include "GlobalTime.h"
#include "Temporal.h"

#include "pde/AnalyticFunction/Function2D.h"

namespace SANS
{

template<class ArrayQ_>
class FunctionNDConvertSpace<PhysD2, ArrayQ_>
{
public:
  typedef Function2DBase<ArrayQ_> FunctionType;

  typedef PhysD2 PhysDim;
  static const int D = PhysDim::D;

  typedef DLA::VectorS<D,Real> VectorX;
  typedef ArrayQ_ ArrayQ;
  typedef DLA::VectorS<D,ArrayQ> VectorArrayQ;
  typedef DLA::MatrixSymS<D,ArrayQ> TensorSymArrayQ;

  static const int N = DLA::VectorSize<ArrayQ>::M;
  typedef typename DLA::MatrixS_or_T<N, N, Real>::type MatrixQ;

  // cppcheck-suppress noExplicitConstructor
  FunctionNDConvertSpace( const FunctionType& fcn ) : fcn_(fcn), time_(dummyTime_) {}

  FunctionNDConvertSpace(const GlobalTime& time, const FunctionType& fcn) : fcn_(fcn), time_(time.time) {}

  FunctionNDConvertSpace( const FunctionNDConvertSpace& ) = delete;
  FunctionNDConvertSpace& operator=( const FunctionNDConvertSpace& ) = delete;

  TemporalMode temporal() const { return &time_ == &dummyTime_ ? eSteady : eUnsteady; }

  ArrayQ operator()( const VectorX& X ) const
  {
    return fcn_(X[0], X[1], time_);
  }

  void gradient( const VectorX& X, ArrayQ& q, VectorArrayQ& gradq ) const
  {
    ArrayQ qt;
    fcn_.gradient(X[0], X[1], time_, q, gradq[0], gradq[1], qt);
  }

  void secondGradient( const VectorX& X, ArrayQ& q, VectorArrayQ& gradq, TensorSymArrayQ& hessq ) const
  {
    ArrayQ qt;
    fcn_.gradient(X[0], X[1], time_,
                  q,
                  gradq[0], gradq[1], qt,
                  hessq(0,0),
                  hessq(1,0), hessq(1,1));
  }

private:
  const FunctionType& fcn_;
  const Real dummyTime_ = 0;
  const Real& time_;
};

} //namespace SANS

#endif  // FUNCTIONNDCONVERTSPACE2D_H
