// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCNDCONVERTSPACE2D_H
#define BCNDCONVERTSPACE2D_H

// 2D NDConvert BC class for steady PDEs

#include "Python/PyDict.h" //Python must be included first

#include <typeinfo> // typeid

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"

#include "Field/Tuple/ParamTuple.h"

#include "pde/BCCategory.h"

#include "BCNDConvert_fwd.h"
#include "PDENDConvert_fwd.h"
#include "GlobalTime.h"
#include "Temporal.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template <class BC>
class BCNDConvertSpace<PhysD2, BC> : public BC
{
public:
  static_assert( std::is_same<PhysD2, typename BC::PhysDim>::value, "Physical dimensions should match" );

  typedef typename BC::Category Category;
  typedef typename BC::ParamsType ParamsType;

  static const int D = BC::D;                     // physical dimensions
  static const int N = BC::N;                     // total solution variables

  static const int NBC = BC::NBC;                   // total BCs

  typedef DLA::VectorS<D,Real> VectorX;                                        // physical coordinate vector, i.e. cartesian or cylindrical

  template<class Z> using ArrayQ          = typename BC::template ArrayQ<Z>;   // solution/flux arrays
  template<class Z> using MatrixQ         = typename BC::template MatrixQ<Z>;  // diffusion matrix/flux jacobians
  template<class Z> using VectorArrayQ    = DLA::VectorS<D, ArrayQ<Z> >;       // vector of solution arrays, i.e. solution gradients/flux vector

  template< class... BCArgs > // cppcheck-suppress noExplicitConstructor
  BCNDConvertSpace(BCArgs&&... args) : BC(std::forward<BCArgs>(args)...), time_(dummyTime_) {}

  template< class... BCArgs >
  BCNDConvertSpace(GlobalTime& time, BCArgs&&... args) : BC(std::forward<BCArgs>(args)...), time_(time.time) {}

  template<class PDE>
  BCNDConvertSpace(const PDENDConvertSpace<PhysD2,PDE>& pde, PyDict& d) : BC(pde, d), time_(dummyTime_)
  {
    SANS_ASSERT(pde.temporal() == temporal());
  }

  template<class PDE>
  BCNDConvertSpace(const GlobalTime& time, const PDENDConvertSpace<PhysD2,PDE>& pde, PyDict& d) : BC(pde, d), time_(time.time)
  {
    SANS_ASSERT(pde.temporal() == temporal());
  }

  virtual ~BCNDConvertSpace() {}

  TemporalMode temporal() const { return &time_ == &dummyTime_ ? eSteady : eUnsteady; }

  BCNDConvertSpace( const BCNDConvertSpace& ) = delete;
  BCNDConvertSpace& operator=( const BCNDConvertSpace& ) = delete;

  virtual const std::type_info& derivedTypeID() const override { return typeid(*this); }

  // nonlinear BCs

  // BC strong-form residual
  template <class T>
  void strongBC( const VectorX& X, const VectorX& Nrm,
                 const ArrayQ<T>& q, const VectorArrayQ<T>& gradq, ArrayQ<T>& rsdBC ) const
  {
    BC::strongBC(X[0], X[1], time_, Nrm[0], Nrm[1], q, gradq[0], gradq[1], rsdBC);
  }

  template <class L, class T, class R>
  void strongBC( const ParamTuple<L, VectorX, TupleClass<>>& param, const VectorX& Nrm,
                 const ArrayQ<T>& q, const VectorArrayQ<T>& gradq, ArrayQ<R>& rsdBC ) const
  {
    const VectorX& X = param.right();
    BC::strongBC(param.left(), X[0], X[1], time_, Nrm[0], Nrm[1], q, gradq[0], gradq[1], rsdBC);
  }

  // strongBC for HSM2D: strongBC(..., X, X_sRef, Nrm, Q, QX, Q_sRef)
  template <class L, class T, class Tb>
  void strongBC( const ParamTuple<L, VectorX, TupleClass<>>& param,
                 const DLA::VectorS<TopoD2::D,VectorX>& X_sRef,
                 const VectorX& Nrm,
                 const ArrayQ<T>& q, const VectorArrayQ<T>& gradq, const VectorArrayQ<T>& q_sRef,
                 ArrayQ<Tb>& rsdBC ) const
  {
    const VectorX& X = param.right();
    const VectorX& Xs = X_sRef[0];
    const VectorX& Xt = X_sRef[1];

    BC::strongBC(param.left(),
                 X[0], X[1], time_,
                 Xs[0], Xt[0],  // xs, xt
                 Xs[1], Xt[1],  // ys, yt
                 Nrm[0], Nrm[1],
                 q,
                 gradq[0], gradq[1],    // qx, qy
                 q_sRef[0], q_sRef[1],  // qs, qt
                 rsdBC);
  }

  // BC strong-form residual (with Lagrange multiplier)
  template <class T>
  void strongBC( const VectorX& X, const VectorX& Nrm,
                 const ArrayQ<T>& q, const VectorArrayQ<T>& gradq, const ArrayQ<T>& lg, ArrayQ<T>& rsdBC ) const
  {
    BC::strongBC(X[0], X[1], time_, Nrm[0], Nrm[1], q, gradq[0], gradq[1], lg, rsdBC);
  }

  // conventional formulation BC weighting function
  template <class T>
  void weightBC( const VectorX& X, const VectorX& Nrm,
                 const ArrayQ<T>& q, const VectorArrayQ<T>& gradq, MatrixQ<T>& wghtBC ) const
  {
    BC::weightBC(X[0], X[1], time_, Nrm[0], Nrm[1], q, gradq[0], gradq[1], wghtBC);
  }

  // topo1D manifold specialization
  template <class T>
  void weightBC( const VectorX& X,
                 const DLA::VectorS<TopoD1::D, VectorX>& e0, const VectorX& Nrm,
                 const ArrayQ<T>& q, const VectorArrayQ<T>& gradq, MatrixQ<T>& wghtBC ) const
  {
    BC::weightBC(e0[0], X[0], X[1], time_, Nrm[0], Nrm[1], q, gradq[0], gradq[1], wghtBC);
  }

  template <class L, class T, class W>
  void weightBC( const ParamTuple<L, VectorX, TupleClass<>>& param, const VectorX& Nrm,
                 const ArrayQ<T>& q, const VectorArrayQ<T>& gradq, MatrixQ<W>& wghtBC ) const
  {
    const VectorX& X = param.right();
    BC::weightBC(param.left(), X[0], X[1], time_, Nrm[0], Nrm[1], q, gradq[0], gradq[1], wghtBC);
  }

  // weightBC for HSM2D: weightBC(..., X, X_sRef, Nrm, Q, QX, Q_sRef)
  template <class L, class T, class Tb>
  void weightBC( const ParamTuple<L, VectorX, TupleClass<>>& param,
                 const DLA::VectorS<TopoD2::D,VectorX>& X_sRef,
                 const VectorX& Nrm,
                 const ArrayQ<T>& q, const VectorArrayQ<T>& gradq, const VectorArrayQ<T>& q_sRef,
                 MatrixQ<Tb>& wghtBC ) const
  {
    const VectorX& X = param.right();
    const VectorX& Xs = X_sRef[0];
    const VectorX& Xt = X_sRef[1];

    BC::weightBC(param.left(),
                 X[0], X[1], time_,
                 Xs[0], Xt[0],  // xs, xt
                 Xs[1], Xt[1],  // ys, yt
                 Nrm[0], Nrm[1],
                 q,
                 gradq[0], gradq[1],    // qx, qy
                 q_sRef[0], q_sRef[1],  // qs, qt
                 wghtBC);
  }

  // Lagrange multiplier weighting function
  template <class T>
  void weightLagrange( const VectorX& X, const VectorX& Nrm,
                       const ArrayQ<T>& q, const VectorArrayQ<T>& gradq, MatrixQ<T>& wghtLG ) const
  {
    BC::weightLagrange(X[0], X[1], time_, Nrm[0], Nrm[1], q, gradq[0], gradq[1], wghtLG);
  }

  // Lagrange multiplier rhs
  template <class T>
  void rhsLagrange( const VectorX& X, const VectorX& Nrm,
                    const ArrayQ<T>& q, const VectorArrayQ<T>& gradq, ArrayQ<T>& rhsLG ) const
  {
    BC::rhsLagrange(X[0], X[1], time_, Nrm[0], Nrm[1], q, gradq[0], gradq[1], rhsLG);
  }


  // Lagrange multiplier term
  template <class T>
  void lagrangeTerm( const VectorX& X, const VectorX& Nrm,
                     const ArrayQ<T>& q, const VectorArrayQ<T>& gradq,
                     const ArrayQ<T>& lg, ArrayQ<T>& lgterm ) const
  {
    BC::lagrangeTerm(X[0], X[1], time_, Nrm[0], Nrm[1], q, gradq[0], gradq[1], lg, lgterm);
  }

  template <class T, class L>
  void lagrangeTerm( const ParamTuple<L, VectorX, TupleClass<>>& param, const VectorX& Nrm,
                     const ArrayQ<T>& q, const VectorArrayQ<T>& gradq,
                     const ArrayQ<T>& lg, ArrayQ<T>& lgterm ) const
  {
    const VectorX& X = param.right();
    BC::lagrangeTerm(param.left(), X[0], X[1], time_, Nrm[0], Nrm[1], q, gradq[0], gradq[1], lg, lgterm);
  }

  // linear BCs (deprecated?)

  // BC coefficients:  A u + B (kn un + ks us)
  template <class T>
  void coefficients(
      const VectorX& X, const VectorX& Nrm,
      MatrixQ<T>& A, MatrixQ<T>& B )
      const
  {
    BC::coefficients(X[0], X[1], time_, Nrm[0], Nrm[1], A, B);
  }

  // BC coefficients:  A u + B (kn un + ks us)
  template <class T, class L>
  void coefficients(
      const ParamTuple<L, VectorX, TupleClass<>>& param, const VectorX& Nrm,
      MatrixQ<T>& A, MatrixQ<T>& B )
      const
  {
    const VectorX& X = param.right();
    BC::coefficients(param.left(), X[0], X[1], time_, Nrm[0], Nrm[1], A, B);
  }

  template <class T>
  void data( const VectorX& X, const VectorX& Nrm, ArrayQ<T>& bcdata ) const
  {
    BC::data(X[0], X[1], time_, Nrm[0], Nrm[1], bcdata);
  }

  template <class T, class L>
  void data( const ParamTuple<L, VectorX, TupleClass<>>& param, const VectorX& Nrm, ArrayQ<T>& bcdata ) const
  {
    const VectorX& X = param.right();
    BC::data(param.left(), X[0], X[1], time_, Nrm[0], Nrm[1], bcdata);
  }

  template <class T>
  void state( const VectorX& X, const VectorX& Nrm, const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BC::state(X[0], X[1], time_, Nrm[0], Nrm[1], qI, qB);
  }

  template <class L, class T, class B>
  void state( const ParamTuple<L, VectorX, TupleClass<>>& param,
              const VectorX& Nrm, const ArrayQ<T>& qI, ArrayQ<B>& qB ) const
  {
    const VectorX& X = param.right();
    BC::state(param.left(), X[0], X[1], time_, Nrm[0], Nrm[1], qI, qB);
  }

  template <class T, class Tf>
  void fluxNormal( const VectorX& X, const VectorX& Nrm, const ArrayQ<T>& qI, const VectorArrayQ<T>& gradqI,
                   const ArrayQ<T>& qB, ArrayQ<Tf>& Fn ) const
  {
    BC::fluxNormal(X[0], X[1], time_, Nrm[0], Nrm[1], qI, gradqI[0], gradqI[1], qB, Fn);
  }

  template <class L, class T, class Tf>
  void fluxNormal( const ParamTuple<L, VectorX, TupleClass<>>& param,
                   const VectorX& Nrm, const ArrayQ<T>& qI, const VectorArrayQ<T>& gradqI,
                   const ArrayQ<T>& qB, ArrayQ<Tf>& Fn ) const
  {
    const VectorX& X = param.right();
    BC::fluxNormal(param.left(), X[0], X[1], time_, Nrm[0], Nrm[1], qI, gradqI[0], gradqI[1], qB, Fn);
  }

  bool isValidState( const VectorX& Nrm, const ArrayQ<Real>& q ) const
  {
    return BC::isValidState(Nrm[0], Nrm[1], q);
  }

#if 1 // [specific for hubtrace and IBL2D]
  template <class L, class T, class Tout>
  void fluxMatchingSourceHubtrace(
      const ParamTuple<L, VectorX, TupleClass<>>& param,
      const DLA::VectorS<TopoD1::D, VectorX>& e0L,
      const VectorX& nrm,
      const ArrayQ<T>& q, const ArrayQ<T>& hb,
      ArrayQ<Tout>& f, ArrayQ<Tout>& sourcehub) const
  {
    const VectorX& X = param.right();  // Cartesian coordinates of evaluation point

    BC::fluxMatchingSourceHubtrace(param.left(), e0L[0], X[0], X[1], time_, nrm[0], nrm[1],
                                   q, hb, f, sourcehub);
  }
#endif

protected:
  const Real dummyTime_ = 0;
  const Real& time_;
};

} //namespace SANS

#endif  // BCNDCONVERTSPACE2D_H
