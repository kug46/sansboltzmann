// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef OUTPUTNDCONVERTSPACETIME2D_H
#define OUTPUTNDCONVERTSPACETIME2D_H

// 2D NDConvert Output class for space-time PDEs

// Python must be included first
#include "Python/PyDict.h"

#include <typeinfo> // typeid

#include "tools/SANSnumerics.h"     // Real
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "Topology/Dimension.h"
#include "Field/Tuple/ParamTuple.h"

#include "OutputNDConvert_fwd.h"
#include "Temporal.h"

namespace SANS
{

template <class OutputFunctional>
class OutputNDConvertSpaceTime<PhysD2, OutputFunctional> : public OutputFunctional
{
public:
  static_assert( std::is_same<PhysD2, typename OutputFunctional::PhysDim>::value, "Physical dimensions should match" );

  typedef PhysD3 PhysDim; // increase the physical dimension of the PDE to include time
  static const int D = PhysDim::D;

  typedef DLA::VectorS<D,Real> VectorXT;

  template<class Z> using ArrayQ          = typename OutputFunctional::template ArrayQ<Z>;  // solution arrays
  template<class Z> using VectorArrayQ    = DLA::VectorS<D, ArrayQ<Z> >;                    // solution gradient arrays
  template<class Z> using ArrayJ          = typename OutputFunctional::template ArrayJ<Z>;  // output arrays
  template<class Z> using MatrixJ         = typename OutputFunctional::template MatrixJ<Z>; // output jacobians

  using OutputFunctional::operator();

  template< class... OutputArgs > // cppcheck-suppress noExplicitConstructor
  OutputNDConvertSpaceTime(OutputArgs&&... args) : OutputFunctional(std::forward<OutputArgs>(args)...) {}

  explicit OutputNDConvertSpaceTime( const PyDict& d ) : OutputFunctional(d) {}

  OutputNDConvertSpaceTime& operator=( const OutputNDConvertSpaceTime& ) = delete;

  TemporalMode temporal() const { return eSpaceTime; }

  virtual ~OutputNDConvertSpaceTime() {}

  virtual const std::type_info& derivedTypeID() const override { return typeid(*this); }

  template<class Tq, class Tg, class To>
  void operator()( const VectorXT& X,
                   const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, ArrayJ<To>& output ) const
  {
    OutputFunctional::operator()(X[0], X[1], X[2], q, gradq[0], gradq[1], output);
  }

  void operator()( const VectorXT& X, ArrayQ<Real>& weights ) const
  {
    OutputFunctional::operator()(X[0], X[1], X[2], weights);
  }

  template<class L, class Tq, class Tg, class To>
  void operator()( const ParamTuple<L, VectorXT, TupleClass<>>& param,
                   const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, ArrayJ<To>& output ) const
  {
    const VectorXT& X = param.right();
    OutputFunctional::operator()(param.left(), X[0], X[1], X[2], q, gradq[0], gradq[1], output);
  }

  template<class Tq, class Tg, class To>
  void outputJacobian( const VectorXT &X,
                       const ArrayQ<Tq> &q, const VectorArrayQ<Tg> &gradq, MatrixJ<To> &dJdu) const
  {
    OutputFunctional::outputJacobian(X[0], X[1], X[2], q, gradq[0], gradq[1], dJdu);
  }
};

} //namespace SANS

#endif  // OUTPUTNDCONVERTSPACETIME2D_H
