// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FUNCTIONNDCONVERTSPACETIME1D_H
#define FUNCTIONNDCONVERTSPACETIME1D_H

// 1D NDConvert function class for space-time

#include "tools/SANSnumerics.h"     // Real

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"
#include "LinearAlgebra/DenseLinAlg/tools/MatrixS_or_T.h"

#include "Topology/Dimension.h"

#include "FunctionNDConvert_fwd.h"
#include "Temporal.h"

#include "pde/AnalyticFunction/Function1D.h"

namespace SANS
{

template <class ArrayQ_>
class FunctionNDConvertSpaceTime<PhysD1, ArrayQ_>
{
public:
  typedef Function1DBase<ArrayQ_> FunctionType;

  typedef PhysD2 PhysDim; // increase the physical dimension to include time
  static const int D = PhysDim::D;

  typedef DLA::VectorS<D,Real> VectorXT;
  typedef ArrayQ_ ArrayQ;
  typedef DLA::VectorS<D,ArrayQ> VectorArrayQ;
  typedef DLA::MatrixSymS<D,ArrayQ> TensorSymArrayQ;

  // cppcheck-suppress noExplicitConstructor
  FunctionNDConvertSpaceTime( const FunctionType& fcn ) : fcn_(fcn) {}

  FunctionNDConvertSpaceTime& operator=( const FunctionNDConvertSpaceTime& ) = delete;

  TemporalMode temporal() const { return eSpaceTime; }

  ArrayQ operator()( const VectorXT& X ) const
  {
    return fcn_(X[0], X[1]);
  }

  void gradient( const VectorXT& X, ArrayQ& q, VectorArrayQ& gradq ) const
  {
    fcn_.gradient(X[0], X[1], q, gradq[0], gradq[1]);
  }

  void secondGradient( const VectorXT& X, ArrayQ& q, VectorArrayQ& gradq, TensorSymArrayQ& hessq ) const
  {
    fcn_.gradient(X[0], X[1],
                  q,
                  gradq[0], gradq[1],
                  hessq(0,0));
  }

private:
  const FunctionType& fcn_;
};

} //namespace SANS

#endif  // FUNCTIONNDCONVERTSPACETIME1D_H
