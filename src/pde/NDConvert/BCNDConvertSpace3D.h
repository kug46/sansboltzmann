// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCNDCONVERTSPACE3D_H
#define BCNDCONVERTSPACE3D_H

// 3-D NDConvert BC class for steady PDEs

#include "Python/PyDict.h" //Python must be included first

#include <typeinfo> // typeid

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"

#include "Field/Tuple/ParamTuple.h"

#include "pde/BCCategory.h"

#include "BCNDConvert_fwd.h"
#include "PDENDConvert_fwd.h"
#include "GlobalTime.h"
#include "Temporal.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template <class BC>
class BCNDConvertSpace<PhysD3, BC> : public BC
{
public:
  static_assert( std::is_same<PhysD3, typename BC::PhysDim>::value, "Physical dimensions should match" );

  typedef typename BC::Category Category;

  static const int D = BC::D;                     // physical dimensions
  static const int N = BC::N;                     // total solution variables

  static const int NBC = BC::NBC;                   // total BCs

  typedef DLA::VectorS<D,Real> VectorX;

  template<class T> using ArrayQ        = typename BC::template ArrayQ<T>;
  template<class T> using MatrixQ       = typename BC::template MatrixQ<T>;    // matrices
  template<class Z> using VectorArrayQ  = DLA::VectorS<D, ArrayQ<Z> >;         // vector of solution arrays, i.e. solution gradients/flux vector

  template< class... BCArgs > // cppcheck-suppress noExplicitConstructor
  BCNDConvertSpace(BCArgs&&... args) : BC(std::forward<BCArgs>(args)...), time_(dummyTime_) {}

  template< class... BCArgs >
  BCNDConvertSpace(GlobalTime& time, BCArgs&&... args) : BC(std::forward<BCArgs>(args)...), time_(time.time) {}

  template<class PDE>
  BCNDConvertSpace(const PDENDConvertSpace<PhysD3,PDE>& pde, PyDict& d) : BC(pde, d), time_(dummyTime_)
  {
    SANS_ASSERT(pde.temporal() == temporal());
  }

  template<class PDE>
  BCNDConvertSpace(const GlobalTime& time, const PDENDConvertSpace<PhysD3,PDE>& pde, PyDict& d) : BC(pde, d), time_(time.time)
  {
    SANS_ASSERT(pde.temporal() == temporal());
  }

  virtual ~BCNDConvertSpace() {}

  TemporalMode temporal() const { return &time_ == &dummyTime_ ? eSteady : eUnsteady; }

  BCNDConvertSpace( const BCNDConvertSpace& ) = delete;
  BCNDConvertSpace& operator=( const BCNDConvertSpace& ) = delete;

  virtual const std::type_info& derivedTypeID() const override { return typeid(*this); }

  // BC coefficients:  A u + B (kn un + ks us)
  template <class T>
  void coefficients(
      const VectorX& X, const VectorX& Nrm,
      MatrixQ<T>& A, MatrixQ<T>& B )
      const
  {
    BC::coefficients(X[0], X[1], X[2], time_, Nrm[0], Nrm[1], Nrm[2], A, B);
  }

  template <class T>
  void data( const VectorX& X, const VectorX& Nrm, ArrayQ<T>& bcdata ) const
  {
    BC::data(X[0], X[1], X[2], time_, Nrm[0], Nrm[1], Nrm[2], bcdata);
  }


  template <class T>
  void state( const VectorX& X, const VectorX& Nrm, const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BC::state(X[0], X[1], X[2], time_, Nrm[0], Nrm[1], Nrm[2], qI, qB);
  }

  template <class L, class T, class B>
  void state( const ParamTuple<L, VectorX, TupleClass<>>& param,
              const VectorX& Nrm, const ArrayQ<T>& qI, ArrayQ<B>& qB ) const
  {
    const VectorX& X = param.right();
    BC::state(param.left(), X[0], X[1], X[2], time_, Nrm[0], Nrm[1], Nrm[2], qI, qB);
  }

  template <class T>
  void fluxNormal( const VectorX& X, const VectorX& Nrm, const ArrayQ<T>& qI, const VectorArrayQ<T>& gradqI,
                   const ArrayQ<T>& qB, ArrayQ<T>& Fn ) const
  {
    BC::fluxNormal(X[0], X[1], X[2], time_, Nrm[0], Nrm[1], Nrm[2], qI, gradqI[0], gradqI[1], gradqI[2], qB, Fn);
  }

  template <class L, class T, class Tf>
  void fluxNormal( const ParamTuple<L, VectorX, TupleClass<>>& param,
                   const VectorX& Nrm, const ArrayQ<T>& qI, const VectorArrayQ<T>& gradqI,
                   const ArrayQ<T>& qB, ArrayQ<Tf>& Fn ) const
  {
    const VectorX& X = param.right();
    BC::fluxNormal(param.left(), X[0], X[1], X[2], time_, Nrm[0], Nrm[1], Nrm[2], qI, gradqI[0], gradqI[1], gradqI[2], qB, Fn);
  }

  bool isValidState( const VectorX& Nrm, const ArrayQ<Real>& q ) const
  {
    return BC::isValidState(Nrm[0], Nrm[1], Nrm[2], q);
  }
private:
  const Real dummyTime_ = 0;
  const Real& time_;
};

} //namespace SANS

#endif  // BCNDCONVERTSPACE3D_H
