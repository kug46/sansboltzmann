// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef TEMPORAL_H
#define TEMPORAL_H

namespace SANS
{

enum TemporalMode
{
  eSteady,
  eUnsteady,
  eSpaceTime
};

}

#endif //TEMPORAL_H
