// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOLNNDCONVERTSPACE1D_H
#define SOLNNDCONVERTSPACE1D_H

// 1D NDConvert Solution class for steady PDEs

// Python must be included first
#include "Python/PyDict.h"

#include "tools/SANSnumerics.h"     // Real
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"
#include "Topology/Dimension.h"
#include "Surreal/SurrealS.h"

#include "SolnNDConvert_fwd.h"
#include "GlobalTime.h"
#include "Temporal.h"

#include <iostream>
#include <string>

namespace SANS
{

template <class SOLN>
class SolnNDConvertSpace<PhysD1, SOLN> : public SOLN
{
public:
  static_assert( std::is_same<PhysD1, typename SOLN::PhysDim>::value, "Physical dimensions should match" );

  template<class T>
  using ArrayQ = typename SOLN::template ArrayQ<T>;

  static const int D = PhysD1::D;

  typedef DLA::VectorS<D,Real> VectorX;

  using SOLN::operator();

  template< class... SolnArgs > // cppcheck-suppress noExplicitConstructor
  SolnNDConvertSpace(SolnArgs&&... args) : SOLN(std::forward<SolnArgs>(args)...), time_(dummyTime_) {}

  template< class... SolnArgs >
  SolnNDConvertSpace(GlobalTime& time, SolnArgs&&... args) : SOLN(std::forward<SolnArgs>(args)...), time_(time.time) {}

  explicit SolnNDConvertSpace( const PyDict& d ) : SOLN(d), time_(dummyTime_) {}

  SolnNDConvertSpace( GlobalTime& time, const PyDict& d ) : SOLN(d), time_(time.time) {}

  SolnNDConvertSpace& operator=( const SolnNDConvertSpace& ) = delete;

  TemporalMode temporal() const { return &time_ == &dummyTime_ ? eSteady : eUnsteady; }


  ArrayQ<Real> operator()( const VectorX& X ) const
  {
    return SOLN::operator()(X[0], time_);
  }

  void gradient( const VectorX& X, ArrayQ<Real>& q,
                 DLA::VectorS< D, ArrayQ<Real> >& gradq ) const
  {
    ArrayQ<Real> qt;
    SOLN::gradient(X[0], time_, q, gradq[0], qt);
  }

  void gradient( const Real& x, const Real& time, ArrayQ<Real>& q,
                 ArrayQ<Real>& qx, ArrayQ<Real>& qt ) const
  {
    SOLN::gradient(x, time_, q, qx, qt);
    if ( temporal() == eSteady )
      qt = 0; //No time derivative because of steady solution
  }

  void secondGradient( const VectorX& X, ArrayQ<Real>& q,
                       DLA::VectorS< D, ArrayQ<Real> >& gradq,
                       DLA::MatrixSymS< D, ArrayQ<Real> >& hessianq ) const
  {
    ArrayQ<Real> qt;
    SOLN::secondGradient(X[0], time_, q, gradq[0], qt, hessianq(0,0));
  }

  void secondGradient( const Real& x, const Real& time, ArrayQ<Real>& q,
                       ArrayQ<Real>& qx, ArrayQ<Real>& qt,
                       ArrayQ<Real>& qxx ) const
  {
    SOLN::secondGradient(x, time_, q, qx, qt, qxx);
    if ( temporal() == eSteady )
      qt = 0; //No time derivative because of steady solution
  }

private:
  const Real dummyTime_ = 0;
  const Real& time_;
};


} //namespace SANS

#endif  // SOLNNDCONVERTSPACE1D_H
