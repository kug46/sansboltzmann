// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOLNNDCONVERT_FWD_H
#define SOLNNDCONVERT_FWD_H

// Forward declare all the PDENDConvert classes

namespace SANS
{

template <class PhysDim, class BC>
class SolnNDConvertSpace;

template <class PhysDim, class BC>
class SolnNDConvertSpaceTime;

}

#endif //SOLNNDCONVERT_FWD_H
