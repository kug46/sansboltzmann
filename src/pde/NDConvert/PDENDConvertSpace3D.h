// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDENDCONVERTSPACE3D_H
#define PDENDCONVERTSPACE3D_H

// 3-D PDE class wrapper for steady state calculations

#include "tools/SANSnumerics.h"     // Real
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "Topology/ElementTopology.h"
#include "Topology/Dimension.h"
#include "Field/Tuple/ParamTuple.h"

#include "PDENDConvert_fwd.h"
#include "GlobalTime.h"
#include "Temporal.h"

#include <type_traits> // is_same
#include <utility>     // forward

namespace SANS
{
//----------------------------------------------------------------------------//
// Methods for solving steady advection diffusion equations
//
// Strong form: div.(F(Q) - Fv(Q, QX)) + S(Q, QX) = RHS(X)
//
// Weak form: < phi n.(F(Q) - Fv(Q, QX)) > - grad(phi).(F(Q) - Fv(Q, QX)) + phi S(Q, QX) = phi RHS(X)
//
// template parameters:
//   PDE                     The PDE class that is wrapped
//
// member functions forward calls on to PDE class, with a dummy time added to spatial coordinates
//
//   .fluxAdvectiveTime                   temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime           jacobian of temporal flux: d(Ft)/dU
//   .masterState                         master state: U(Q)
//   .jacobianMasterState                 jacobian of master state wrt q: dU(Q)/dQ
//   .fluxAdvective                       advective/inviscid fluxes: F(Q)
//   .fluxAdvectiveUpwind                 upwinded advective/inviscid fluxes: n.F(QL, QR)
//   .jacobianFluxAdvective               jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
//   .jacobianFluxAdvectiveAbsoluteValue  absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
//   .strongFluxAdvective                 strong form advective fluxes: div.(F)
//   .fluxViscous                         viscous fluxes: Fv(Q, QX) and n.Fv(QL, QLX, QR, QRX)
//   .diffusionViscous                    viscous diffusion coefficient: d(Fv)/d(UX)
//   .jacobianFluxViscous                 jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
//   .strongFluxViscous                   strong form viscous fluxes: div.(Fv)
//   .source                              solution-dependent source: S(Q, QX)
//   .sourceTrace                dual-consistent source
//   .jacobianSource                      jacobian of source wrt conservation variables: d(S)/d(U)
//   .forcingFunction                     right-hand-side forcing function: f(X)
//   .speedCharacteristic                 characteristic speed (needed for timestep)
//   .updateFraction                      update fraction needed for physically valid state
//   .isValidState                        T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom                          set from primitive variable array
//----------------------------------------------------------------------------//

template<class PDE_>
class PDENDConvertSpace<PhysD3, PDE_> : public PDE_
{
public:
  typedef PDE_ PDE;

  static_assert( std::is_same<PhysD3, typename PDE::PhysDim>::value, "Physical dimensions should match" );

  typedef typename PDE::PhysDim PhysDim;
  static const int D = PhysDim::D;
  typedef TemporalMarch Temporal;

  typedef DLA::VectorS<D,Real> VectorX;                                        // physical coordinate vector, i.e. cartesian or cylindrical

  template<class Z> using ArrayQ          = typename PDE::template ArrayQ<Z>;  // solution/flux arrays
  template<class Z> using MatrixQ         = typename PDE::template MatrixQ<Z>; // diffusion matrix/flux jacobians
  template<class Z> using VectorArrayQ    = DLA::VectorS<D, ArrayQ<Z> >;       // vector of solution arrays, i.e. solution gradients/flux vector
  template<class Z> using VectorMatrixQ   = DLA::VectorS<D, MatrixQ<Z> >;      // vector of jacobians, i.e. flux jacobians d(F)/d(U)
  template<class Z> using TensorSymArrayQ = DLA::MatrixSymS<D, ArrayQ<Z> >;    // hessian of solution, i.e QXX
  template<class Z> using TensorMatrixQ   = DLA::MatrixS<D, D, MatrixQ<Z> >;   // diffusion, i.e. viscous flux jacobian d(Fv)/d(UX)
  template<class Z> using TensorArrayQ   =  DLA::MatrixS<D, D, ArrayQ<Z> >;   // diffusion, i.e.  gradient of viscous flux grad (Fv)
  template<class Z> using VectorTensorMatrixQ = DLA::VectorS<D,DLA::MatrixS<D, D, MatrixQ<Z> >>;

  // Constructor forwards arguments to PDE class using varargs
  template< class... PDEArgs > // cppcheck-suppress noExplicitConstructor
  PDENDConvertSpace(PDEArgs&&... args) : PDE(std::forward<PDEArgs>(args)...), time_(dummyTime_) {}

  template< class... PDEArgs >
  PDENDConvertSpace(GlobalTime& time, PDEArgs&&... args) : PDE(std::forward<PDEArgs>(args)...), time_(time.time) {}

  ~PDENDConvertSpace() {}

  PDENDConvertSpace( const PDENDConvertSpace& ) = delete;
  PDENDConvertSpace& operator=( const PDENDConvertSpace& ) = delete;

  TemporalMode temporal() const { return &time_ == &dummyTime_ ? eSteady : eUnsteady; }


  //--------
  // master state: U(Q)
  template <class Tq, class Tu>
  void masterState( const VectorX& X, const ArrayQ<Tq>& q, ArrayQ<Tu>& u ) const
  {
    PDE::masterState(X[0], X[1], X[2], time_, q, u);
  }

  //--------
  // master state: U(Q)
  template <class L, class Tq, class Tu>
  void masterState( const ParamTuple<L, VectorX, TupleClass<>>& param,
                         const ArrayQ<Tq>& q, ArrayQ<Tu>& u ) const
  {
    const VectorX& X = param.right();
    PDE::masterState(param.left(), X[0], X[1], X[2], time_, q, u);
  }


  //--------
  // conservative variable: U(Q)
  template <class Tq, class Tqp, class Tu>
  void perturbedMasterState( const VectorX& X, const ArrayQ<Tq>& q, const ArrayQ<Tqp>& dq, ArrayQ<Tu>& du ) const
  {
    PDE::perturbedMasterState(X[0], X[1], X[2], time_, q, dq, du);
  }

  //--------
  // conservative variable: U(Q)
  template <class L, class Tq, class Tqp, class Tu>
  void perturbedMasterState( const ParamTuple<L, VectorX, TupleClass<>>& param,
                             const ArrayQ<Tq>& q, const ArrayQ<Tqp>& dq, ArrayQ<Tu>& du ) const
  {
    const VectorX& X = param.right();
    PDE::perturbedMasterState(param.left(), X[0], X[1], X[2], time_, q, dq, du);
  }

  //--------
  // conservative variable: U(Q)
  template <class Tq, class Tg, class Tu>
  void masterStateGradient( const VectorX& X, const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, VectorArrayQ<Tu>& gradu ) const
  {
    PDE::masterStateGradient(X[0], X[1], X[2], time_, q, gradq[0], gradq[1], gradq[2], gradu[0], gradu[1],  gradu[2]);
  }

  //--------
  // conservative variable: U(Q)
  template <class L, class Tq, class Tg, class Tu>
  void masterStateGradient( const ParamTuple<L, VectorX, TupleClass<>>& param,
                            const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, VectorArrayQ<Tu>& gradu  ) const
  {
    const VectorX& X = param.right();
    PDE::masterStateGradient(param.left(), X[0], X[1], X[2], time_, q, gradq[0], gradq[1], gradq[2], gradu[0], gradu[1],  gradu[2]);
  }

  //--------
  // conservative variable: U(Q)
  template <class Tq, class Tg, class Th, class Tu>
  void masterStateHessian( const VectorX& X, const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, const TensorSymArrayQ<Th>& hessq,
                           TensorSymArrayQ<Tu>& hessu ) const
  {
    PDE::masterStateHessian(X[0], X[1], X[2], time_, q,
                            gradq[0], gradq[1], gradq[2],
                            hessq(0,0), hessq(0,1), hessq(0,2),
                            hessq(1,1),  hessq(1,2),
                            hessq(2,2),
                            hessu(0,0), hessu(0,1), hessu(0,2),
                            hessu(1,1), hessu(1,2),
                            hessu(2,2) );
  }

  //--------
  // conservative variable: U(Q)
  template <class L, class Tq, class Tg, class Th, class Tu>
  void masterStateHessian( const ParamTuple<L, VectorX, TupleClass<>>& param,
                           const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, const TensorSymArrayQ<Th>& hessq,
                           TensorSymArrayQ<Tu>& hessu ) const
  {
    const VectorX& X = param.right();
    PDE::masterStateHessian(param.left(), X[0], X[1], X[2], time_, q,
                            gradq[0], gradq[1], gradq[2],
                            hessq(0,0), hessq(0,1), hessq(0,2),
                            hessq(1,1),  hessq(1,2),
                            hessq(2,2),
                            hessu(0,0), hessu(0,1), hessu(0,2),
                            hessu(1,1), hessu(1,2),
                            hessu(2,2) );
  }

  //--------
  // jacobian of master state wrt q: dU/dQ
  template <class Tq, class Tf>
  void jacobianMasterState( const VectorX& X,
                            const ArrayQ<Tq>& q, MatrixQ<Tf>& a ) const
  {
    PDE::jacobianMasterState(X[0], X[1], X[2], time_, q, a);
  }

  //--------
  // jacobian of master state wrt q: dU/dQ
  template <class L, class Tq, class Tf>
  void jacobianMasterState( const ParamTuple<L, VectorX, TupleClass<>>& param,
                                 const ArrayQ<Tq>& q, MatrixQ<Tf>& a ) const
  {
    const VectorX& X = param.right();
    PDE::jacobianMasterState(param.left(), X[0], X[1], X[2], time_, q, a);
  }

  //--------
  // temporal flux: Ft(Q)
  template <class Tq, class Tf>
  void fluxAdvectiveTime( const VectorX& X, const ArrayQ<Tq>& q, ArrayQ<Tf>& ft ) const
  {
    PDE::fluxAdvectiveTime(X[0], X[1], X[2], time_, q, ft);
  }

  //--------
  // temporal flux: Ft(Q)
  template <class L, class Tq, class Tf>
  void fluxAdvectiveTime( const ParamTuple<L, VectorX, TupleClass<>>& param,
                         const ArrayQ<Tq>& q, ArrayQ<Tf>& ft ) const
  {
    const VectorX& X = param.right();
    PDE::fluxAdvectiveTime(param.left(), X[0], X[1], X[2], time_, q, ft);
  }

  //--------
  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <class Tq, class Tf>
  void jacobianFluxAdvectiveTime( const VectorX& X, const ArrayQ<Tq>& q, MatrixQ<Tf>& J ) const
  {
    PDE::jacobianFluxAdvectiveTime(X[0], X[1], X[2], time_, q, J);
  }

  //--------
  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <class L, class Tq, class Tf>
  void jacobianFluxAdvectiveTime( const ParamTuple<L, VectorX, TupleClass<>>& param,
                         const ArrayQ<Tq>& q, MatrixQ<Tf>& J ) const
  {
    const VectorX& X = param.right();
    PDE::jacobianFluxAdvectiveTime(param.left(), X[0], X[1], X[2], time_, q, J);
  }

  //--------
  // advective flux: F(X, Q)
  template <class T, class Tf>
  void fluxAdvective( const VectorX& X,
                      const ArrayQ<T>& q, VectorArrayQ<Tf>& F ) const
  {
    PDE::fluxAdvective(X[0], X[1], X[2], time_, q, F[0], F[1], F[2]);
  }

  // advective flux: F(..., X, Q)
  template <class L, class T, class Tf>
  void fluxAdvective( const ParamTuple<L, VectorX, TupleClass<>>& param,
                      const ArrayQ<T>& q, VectorArrayQ<Tf>& F ) const
  {
    const VectorX& X = param.right();
    PDE::fluxAdvective(param.left(), X[0], X[1], X[2], time_, q, F[0], F[1], F[2]);
  }


  //--------
  // advective flux: n . (F(X, QL) + F(X, QR) + Upwinding)/2
  template <class Tq, class Tf>
  void fluxAdvectiveUpwind( const VectorX& X,
                            const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, const VectorX& N, ArrayQ<Tf>& f ) const
  {
    PDE::fluxAdvectiveUpwind(X[0], X[1], X[2], time_, qL, qR, N[0], N[1], N[2], f);
  }

  // advective flux: n . (F(..., X, QL) + F(..., X, QR) + Upwinding)/2
  template <class L, class Tq, class Tf>
  void fluxAdvectiveUpwind( const ParamTuple<L, VectorX, TupleClass<>>& param,
                            const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, const VectorX& N, ArrayQ<Tf>& f ) const
  {
    const VectorX& X = param.right();
    PDE::fluxAdvectiveUpwind(param.left(), X[0], X[1], X[2], time_, qL, qR, N[0], N[1], N[2], f);
  }

  //--------
  // advective flux: n . (F(X, QL) + F(X, QR) + Upwinding)/2
  template <class Tq, class Tf>
  void fluxAdvectiveUpwind( const VectorX& X,
                            const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, const VectorX& N, ArrayQ<Tf>& f, const Real& scale ) const
  {
    PDE::fluxAdvectiveUpwind(X[0], X[1], X[2], time_, qL, qR, N[0], N[1], N[2], f, scale);
  }

  // advective flux: n . (F(..., X, QL) + F(..., X, QR) + Upwinding)/2
  template <class L, class Tq, class Tf>
  void fluxAdvectiveUpwind( const ParamTuple<L, VectorX, TupleClass<>>& param,
                            const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, const VectorX& N, ArrayQ<Tf>& f, const Real& scale ) const
  {
    const VectorX& X = param.right();
    PDE::fluxAdvectiveUpwind(param.left(), X[0], X[1], X[2], time_, qL, qR, N[0], N[1], N[2], f, scale);
  }

  //--------
  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix), where F(X, Q)
  template <class Tq, class Tf>
  void jacobianFluxAdvective( const VectorX& X,
                              const ArrayQ<Tq>& q, VectorMatrixQ<Tf>& a ) const
  {
    PDE::jacobianFluxAdvective(X[0], X[1], X[2], time_, q, a[0], a[1], a[2]);
  }

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix), where F(..., X, Q)
  template <class L, class Tq, class Tf>
  void jacobianFluxAdvective( const ParamTuple<L, VectorX, TupleClass<>>& param,
                              const ArrayQ<Tq>& q, VectorMatrixQ<Tf>& a ) const
  {
    const VectorX& X = param.right();
    PDE::jacobianFluxAdvective(param.left(), X[0], X[1], X[2], time_, q, a[0], a[1], a[2]);
  }


  //--------
  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|, where F(X, Q)
  template <class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValue( const VectorX& X,
                                           const ArrayQ<Tq>& q, const VectorX& N, MatrixQ<Tf>& a ) const
  {
    PDE::jacobianFluxAdvectiveAbsoluteValue(X[0], X[1], X[2], time_, q, N[0], N[1], N[2], a);
  }

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|, where F(..., X, Q)
  template <class L, class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValue( const ParamTuple<L, VectorX, TupleClass<>>& param,
                                           const ArrayQ<Tq>& q, const VectorX& N, MatrixQ<Tf>& a ) const
  {
    const VectorX& X = param.right();
    PDE::jacobianFluxAdvectiveAbsoluteValue(param.left(), X[0], X[1], X[2], time_, q, N[0], N[1], N[2], a);
  }


  //--------
  // strong form advective fluxes: div.(F), where F(X, Q)
  template <class Tq, class Tg, class Tf>
  void strongFluxAdvective( const VectorX& X,
                            const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, ArrayQ<Tf>& strongPDE ) const
  {
    PDE::strongFluxAdvective(X[0], X[1], X[2], time_, q, gradq[0], gradq[1], gradq[2], strongPDE);
  }

  // strong form advective fluxes: div.(F), where F(..., X, Q)
  template <class L, class Tq, class Tg, class Tf>
  void strongFluxAdvective( const ParamTuple<L, VectorX, TupleClass<>>& param,
                            const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, ArrayQ<Tf>& strongPDE ) const
  {
    const VectorX& X = param.right();
    PDE::strongFluxAdvective(param.left(), X[0], X[1], X[2], time_, q, gradq[0], gradq[1], gradq[2], strongPDE);
  }


  //--------
  // viscous flux: Fv(X, Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscous( const VectorX& X,
                    const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, VectorArrayQ<Tf>& F ) const
  {
    PDE::fluxViscous(X[0], X[1], X[2], time_, q, gradq[0], gradq[1], gradq[2], F[0], F[1], F[2]);
  }

  // viscous flux: Fv(..., X, Q, QX)
  template <class L, class Tq, class Tg, class Tf>
  void fluxViscous( const ParamTuple<L, VectorX, TupleClass<>>& param,
                    const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, VectorArrayQ<Tf>& F ) const
  {
    const VectorX& X = param.right();
    PDE::fluxViscous(param.left(), X[0], X[1], X[2], time_, q, gradq[0], gradq[1], gradq[2], F[0], F[1], F[2]);
  }


  //--------
  // viscous flux: n . ( Fv(X, QL, QLX) + Fv(X, QR, QRX) )/2
  template <class Tq, class Tg, class Tf>
  void fluxViscous( const VectorX& XL,
                    const VectorX& XR,
                    const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqL,
                    const ArrayQ<Tq>& qR, const VectorArrayQ<Tg>& gradqR,
                    const VectorX& N, ArrayQ<Tf>& f ) const
  {
    PDE::fluxViscous(XL[0], XL[1], XL[2], time_,
                     qL, gradqL[0], gradqL[1], gradqL[2],
                     qR, gradqR[0], gradqR[1], gradqR[2],
                     N[0], N[1], N[2], f);
  }

  // viscous flux: n . ( Fv(..., X, QL, QLX) + Fv(..., X, QR, QRX) )/2
  template <class L, class Tq, class Tg, class Tf>
  void fluxViscous( const ParamTuple<L, VectorX, TupleClass<>>& paramL,
                    const ParamTuple<L, VectorX, TupleClass<>>& paramR,
                    const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqL,
                    const ArrayQ<Tq>& qR, const VectorArrayQ<Tg>& gradqR,
                    const VectorX& N, ArrayQ<Tf>& f ) const
  {
    const VectorX& X = paramL.right();
    PDE::fluxViscous(paramL.left(), paramR.left(), X[0], X[1], X[2], time_,
                     qL, gradqL[0], gradqL[1], gradqL[2],
                     qR, gradqR[0], gradqR[1], gradqR[2],
                     N[0], N[1], N[2], f);
  }


  //--------
  // viscous flux: Fv(X, Q, QX)
  template <class Tq, class Tg, class Tgu, class Tf>
  void perturbedGradFluxViscous( const VectorX& X,
                    const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, const VectorArrayQ<Tgu>& graddq, VectorArrayQ<Tf>& dF ) const
  {
    PDE::perturbedGradFluxViscous(X[0], X[1], X[2], time_, q,
                                  gradq[0], gradq[1], gradq[2],
                                  graddq[0], graddq[1], graddq[2],
                                  dF[0], dF[1], dF[2]);
  }

  // viscous flux: Fv(..., X, Q, QX)
  template <class L, class Tq, class Tg, class Tgu, class Tf>
  void perturbedGradFluxViscous( const ParamTuple<L, VectorX, TupleClass<>>& param,
                                 const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, const VectorArrayQ<Tgu>& graddq, VectorArrayQ<Tf>& dF ) const
  {
    const VectorX& X = param.right();
    PDE::perturbedGradFluxViscous(param.left(), X[0], X[1], X[2], time_, q,
                                  gradq[0], gradq[1], gradq[2],
                                  graddq[0], graddq[1], graddq[2],
                                  dF[0], dF[1], dF[2]);
  }


  //--------
  // viscous flux: Fv(X, Q, QX)
  template <class Tk, class Tgu, class Tf>
  void perturbedGradFluxViscous( const VectorX& X,
                                 const TensorMatrixQ<Tk>& K, const VectorArrayQ<Tgu>& graddu, VectorArrayQ<Tf>& dF ) const
  {
    PDE::perturbedGradFluxViscous(X[0], X[1], X[2], time_,
                                  K(0,0), K(0,1), K(0,2),
                                  K(1,0), K(1,1), K(1,2),
                                  K(2,0), K(2,1), K(2,2),
                                  graddu[0], graddu[1], graddu[2],
                                  dF[0], dF[1], dF[2]);
  }

  // viscous flux: Fv(..., X, Q, QX)
  template <class L, class Tk, class Tgu, class Tf>
  void perturbedGradFluxViscous( const ParamTuple<L, VectorX, TupleClass<>>& param,
                                 const TensorMatrixQ<Tk>& K, const VectorArrayQ<Tgu>& graddu, VectorArrayQ<Tf>& dF ) const
  {
    const VectorX& X = param.right();
    PDE::perturbedGradFluxViscous(param.left(), X[0], X[1], X[2], time_,
                                  K(0,0), K(0,1), K(0,2),
                                  K(1,0), K(1,1), K(1,2),
                                  K(2,0), K(2,1), K(2,2),
                                  graddu[0], graddu[1], graddu[2],
                                  dF[0], dF[1], dF[2]);
  }

  //--------
  // viscous diffusion matrix: d(Fv)/d(UX), where Fv(X, Q, QX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscous( const VectorX& X,
                         const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq,
                         TensorMatrixQ<Tk>& K ) const
  {
    PDE::diffusionViscous(X[0], X[1], X[2], time_,
                          q, gradq[0], gradq[1], gradq[2],
                          K(0,0), K(0,1), K(0,2),
                          K(1,0), K(1,1), K(1,2),
                          K(2,0), K(2,1), K(2,2) );
  }

  // viscous diffusion matrix: d(Fv)/d(UX), where Fv(..., X, Q, QX)
  template <class L, class Tq, class Tg, class Tk>
  void diffusionViscous( const ParamTuple<L, VectorX, TupleClass<>>& param,
                         const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq,
                         TensorMatrixQ<Tk>& K ) const
  {
    const VectorX& X = param.right();
    PDE::diffusionViscous(param.left(), X[0], X[1], X[2], time_,
                          q, gradq[0], gradq[1], gradq[2],
                          K(0,0), K(0,1), K(0,2),
                          K(1,0), K(1,1), K(1,2),
                          K(2,0), K(2,1), K(2,2) );
  }

  //--------
  // gradient of viscous diffusion matrix: del dot d(Fv)/d(UX), where Fv(X, Q, QX)
  // GradK = [ kxx_x, kxy_x, kxz_x
  //           kyx_y, kyy_y, kyz_y
  //           kzx_z, kzy_z, kzz_z];

  template <class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient( const VectorX& X,
                         const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq,
                         const TensorSymArrayQ<Th>& hessq,
                         VectorTensorMatrixQ<Tk>& GradK ) const
  {
    PDE::diffusionViscousGradient(X[0], X[1], X[2], time_,
                                  q, gradq[0], gradq[1], gradq[2],
                                  hessq(0,0),
                                  hessq(1,0), hessq(1,1),
                                  hessq(2,0), hessq(2,1), hessq(2,2),
                                  GradK[0](0,0), GradK[0](0,1), GradK[0](0,2), GradK[0](1,0), GradK[0](2,0),
                                  GradK[1](0,1), GradK[1](1,0), GradK[1](1,1), GradK[1](1,2), GradK[1](2,1),
                                  GradK[2](0,2), GradK[2](1,2), GradK[2](2,0), GradK[1](2,1), GradK[2](2,2) );
  }

  // viscous diffusion matrix: d(Fv)/d(UX), where Fv(..., X, Q, QX)
  template <class L, class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient( const ParamTuple<L, VectorX, TupleClass<>>& param,
                         const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq,
                         const TensorSymArrayQ<Th>& hessq,
                         VectorTensorMatrixQ<Tk>& GradK ) const
  {
    const VectorX& X = param.right();
    PDE::diffusionViscousGradient(param.left(), X[0], X[1], X[2], time_,
                                  q, gradq[0], gradq[1], gradq[2],
                                  hessq(0,0),
                                  hessq(1,0), hessq(1,1),
                                  hessq(2,0), hessq(2,1), hessq(2,2),
                                  GradK[0](0,0), GradK[0](0,1), GradK[0](0,2), GradK[0](1,0), GradK[0](2,0),
                                  GradK[1](0,1), GradK[1](1,0), GradK[1](1,1), GradK[1](1,2), GradK[1](2,1),
                                  GradK[2](0,2), GradK[2](1,2), GradK[2](2,0), GradK[1](2,1), GradK[2](2,2) );
  }

  //--------
  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U), where Fv(X, Q, QX)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscous( const VectorX& X,
                            const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, VectorMatrixQ<Tf>& a ) const
  {
    PDE::jacobianFluxViscous(X[0], X[1], X[2], time_, q, gradq[0], gradq[1], gradq[2], a[0], a[1], a[2]);
  }

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U), where Fv(..., X, Q, QX)
  template <class L, class Tq, class Tg, class Tf>
  void jacobianFluxViscous( const ParamTuple<L, VectorX, TupleClass<>>& param,
                            const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, VectorMatrixQ<Tf>& a ) const
  {
    const VectorX& X = param.right();
    PDE::jacobianFluxViscous(param.left(), X[0], X[1], X[2], time_, q, gradq[0], gradq[1], gradq[2], a[0], a[1], a[2]);
  }


  //--------
  // strong form viscous fluxes: div.(Fv), where Fv(X, Q, QX)
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscous( const VectorX& X,
                          const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq,
                          const TensorSymArrayQ<Th>& hessq, ArrayQ<Tf>& strongPDE ) const
  {
    PDE::strongFluxViscous(X[0], X[1], X[2], time_, q, gradq[0], gradq[1], gradq[2],
                           hessq(0,0),
                           hessq(1,0), hessq(1,1),
                           hessq(2,0), hessq(2,1), hessq(2,2), strongPDE);
  }

  // strong form viscous fluxes: div.(Fv), where Fv(..., X, Q, QX)
  template <class L, class Tq, class Tg, class Th, class Tf>
  void strongFluxViscous( const ParamTuple<L, VectorX, TupleClass<>>& param,
                          const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq,
                          const TensorSymArrayQ<Th>& hessq, ArrayQ<Tf>& strongPDE ) const
  {
    const VectorX& X = param.right();
    PDE::strongFluxViscous(param.left(), X[0], X[1], X[2], time_, q, gradq[0], gradq[1], gradq[2],
                           hessq(0,0),
                           hessq(1,0), hessq(1,1),
                           hessq(2,0), hessq(2,1), hessq(2,2), strongPDE);
  }


  //--------
  // solution-dependent source: S(X, Q, QX)
  template <class Tq, class Tg, class Ts>
  void source( const VectorX& X,
               const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq,
               ArrayQ<Ts>& source ) const
  {
    PDE::source(X[0], X[1], X[2], time_, q, gradq[0], gradq[1], gradq[2], source);
  }

  // solution-dependent source: S(..., X, Q, QX)
  template <class L, class Tq, class Tg, class Ts>
  void source( const ParamTuple<L, VectorX, TupleClass<>>& param,
               const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq,
               ArrayQ<Ts>& source ) const
  {
    const VectorX& X = param.right();
    PDE::source(param.left(), X[0], X[1], X[2], time_, q, gradq[0], gradq[1], gradq[2], source);
  }

  template <class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void source( const VectorX& X,
               const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
               const VectorArrayQ<Tg>& gradq, const VectorArrayQ<Tgp>& gradqp,
               ArrayQ<Ts>& source ) const
  {
    PDE::source(X[0], X[1], X[2], time_, q, qp, gradq[0], gradq[1], gradq[2], gradqp[0], gradqp[1], gradqp[2], source);
  }

  // solution-dependent source: S(..., X, Q, QX)
  template <class L, class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void source( const ParamTuple<L, VectorX, TupleClass<>>& param,
               const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
               const VectorArrayQ<Tg>& gradq, const VectorArrayQ<Tgp>& gradqp,
               ArrayQ<Ts>& source ) const
  {
    const VectorX& X = param.right();
    PDE::source(param.left(), X[0], X[1], X[2], time_, q, qp, gradq[0], gradq[1], gradq[2], gradqp[0], gradqp[1], gradqp[2], source);
  }

  template <class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void sourceCoarse( const VectorX& X,
                     const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
                     const VectorArrayQ<Tg>& gradq, const VectorArrayQ<Tgp>& gradqp,
                     ArrayQ<Ts>& source ) const
  {
    PDE::source(X[0], X[1], X[2], time_, q, qp, gradq[0], gradq[1], gradq[2], gradqp[0], gradqp[1], gradqp[2], source);
  }

  // solution-dependent source: S(..., X, Q, QX)
  template <class L, class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void sourceCoarse( const ParamTuple<L, VectorX, TupleClass<>>& param,
                     const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
                     const VectorArrayQ<Tg>& gradq, const VectorArrayQ<Tgp>& gradqp,
                     ArrayQ<Ts>& source ) const
  {
    const VectorX& X = param.right();
    PDE::sourceCoarse(param.left(), X[0], X[1], X[2], time_, q, qp, gradq[0], gradq[1], gradq[2], gradqp[0], gradqp[1], gradqp[2], source);
  }

  template <class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void sourceFine( const VectorX& X,
                   const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
                   const VectorArrayQ<Tg>& gradq, const VectorArrayQ<Tgp>& gradqp,
                   ArrayQ<Ts>& source ) const
  {
    PDE::sourceFine(X[0], X[1], X[2], time_, q, qp, gradq[0], gradq[1], gradq[2], gradqp[0], gradqp[1], gradqp[2], source);
  }

  // solution-dependent source: S(..., X, Q, QX)
  template <class L, class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void sourceFine( const ParamTuple<L, VectorX, TupleClass<>>& param,
                   const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
                   const VectorArrayQ<Tg>& gradq, const VectorArrayQ<Tgp>& gradqp,
                   ArrayQ<Ts>& source ) const
  {
    const VectorX& X = param.right();
    PDE::sourceFine(param.left(), X[0], X[1], X[2], time_, q, qp, gradq[0], gradq[1], gradq[2], gradqp[0], gradqp[1], gradqp[2], source);
  }

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX)
  template <class Tlq, class Tq, class Tg, class Ts>
  void source( const VectorX& X, const Tlq& lifted_quantity,
               const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq,
               ArrayQ<Ts>& source ) const
  {
    PDE::source(X[0], X[1], X[2], time_,
                lifted_quantity, q, gradq[0], gradq[1], gradq[2], source);
  }

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX)
  template <class L, class Tlq, class Tq, class Tg, class Ts>
  void source( const ParamTuple<L, VectorX, TupleClass<>>& param, const Tlq& lifted_quantity,
               const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq,
               ArrayQ<Ts>& source ) const
  {
    const VectorX& X = param.right();
    PDE::source(param.left(), X[0], X[1], X[2], time_,
                lifted_quantity, q, gradq[0], gradq[1], gradq[2], source);
  }

  //--------
  // dual-consistent source: SL(X, QL, QR, QLX, QRX), SR(X, QL, QR, QLX, QRX)
  template <class Tq, class Tg, class Ts>
  void sourceTrace( const VectorX& XL, const VectorX& XR,
                    const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqL,
                    const ArrayQ<Tq>& qR, const VectorArrayQ<Tg>& gradqR,
                    ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
  {
    PDE::sourceTrace(XL[0], XL[1], XL[2],
                     XR[0], XR[1], XR[2], time_,
                     qL, gradqL[0], gradqL[1], gradqL[2],
                     qR, gradqR[0], gradqR[1], gradqR[2],
                     sourceL, sourceR);
  }

  // dual-consistent source: SL(..., X, QL, QR, QLX, QRX), SR(..., X, QL, QR, QLX, QRX)
  template <class L, class Tq, class Tg, class Ts>
  void sourceTrace( const ParamTuple<L, VectorX, TupleClass<>>& paramL,
                    const ParamTuple<L, VectorX, TupleClass<>>& paramR,
                    const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqL,
                    const ArrayQ<Tq>& qR, const VectorArrayQ<Tg>& gradqR,
                    ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
  {
    const VectorX& XL = paramL.right();
    const VectorX& XR = paramR.right();
    PDE::sourceTrace(paramL.left(), XL[0], XL[1], XL[2],
                     paramR.left(), XR[0], XR[1], XR[2], time_,
                     qL, gradqL[0], gradqL[1], gradqL[2],
                     qR, gradqR[0], gradqR[1], gradqR[2],
                     sourceL, sourceR);
  }

  //--------
  // lifted quantity for source: SL(X, QL, QR), SR(X, QL, QR)
  template <class Tq, class Ts>
  void sourceLiftedQuantity( const VectorX& XL, const VectorX& XR,
                             const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
                             Ts& s ) const
  {
    PDE::sourceLiftedQuantity(XL[0], XL[1], XL[2],
                              XR[0], XR[1], XR[2], time_, qL, qR, s);
  }

  // lifted quantity for source: SL(..., X, QL, QR), SR(..., X, QL, QR)
  template <class L, class Tq, class Ts>
  void sourceLiftedQuantity( const ParamTuple<L, VectorX, TupleClass<>>& paramL,
                             const ParamTuple<L, VectorX, TupleClass<>>& paramR,
                             const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
                             Ts& s ) const
  {
    const VectorX& XL = paramL.right();
    const VectorX& XR = paramR.right();
    PDE::sourceLiftedQuantity(paramL.left(), XL[0], XL[1], XL[2],
                              paramR.left(), XR[0], XR[1], XR[2], time_,
                              qL, qR, s);
  }

  //--------
  // linear change in source in response to linear perturbations du, dux, duy
  template <class Tq, class Tg, class Tu, class Tgu, class Ts>
  void perturbedSource( const VectorX& X,
                       const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq,
                       const ArrayQ<Tu>& du, const VectorArrayQ<Tgu>& dgradu, ArrayQ<Ts>& dS ) const
  {
    PDE::perturbedSource(X[0], X[1], X[2], time_, q, gradq[0], gradq[1], gradq[2],
                         du, dgradu[0], dgradu[1], dgradu[2], dS);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U), where S(..., X, Q, QX)
  template <class L, class Tq, class Tg,  class Tu, class Tgu, class Ts>
  void perturbedSource( const ParamTuple<L, VectorX, TupleClass<>>& param,
                                 const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq,
                                 const ArrayQ<Tu>& du, const VectorArrayQ<Tgu>& dgradu, ArrayQ<Ts>& dS ) const
  {
    const VectorX& X = param.right();
    PDE::perturbedSource(param.left(), X[0], X[1], X[2], time_, q, gradq[0], gradq[1], gradq[2],
                         du, dgradu[0], dgradu[1], dgradu[2], dS);
  }

  //--------
  // jacobian of source wrt conservation variables: d(S)/d(U), where S(X, Q, QX)
  template <class Tq, class Tg, class Ts>
  void jacobianSource( const VectorX& X,
                       const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, MatrixQ<Ts>& dsdu ) const
  {
    PDE::jacobianSource(X[0], X[1], X[2], time_, q, gradq[0], gradq[1], gradq[2], dsdu);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U), where S(..., X, Q, QX)
  template <class L, class Tq, class Tg, class Ts>
  void jacobianSource( const ParamTuple<L, VectorX, TupleClass<>>& param,
                       const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, MatrixQ<Ts>& dsdu ) const
  {
    const VectorX& X = param.right();
    PDE::jacobianSource(param.left(), X[0], X[1], X[2], time_, q, gradq[0], gradq[1], gradq[2], dsdu);
  }

  //--------
  // jacobian of source wrt conservation variables: d(S)/d(U), where S(X, Q, QX)
  template <class Tq, class Tg, class Ts>
  void jacobianSourceHACK( const VectorX& X,
                       const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, MatrixQ<Ts>& dsdu ) const
  {
    PDE::jacobianSourceHACK(X[0], X[1], X[2], time_, q, gradq[0], gradq[1], gradq[2], dsdu);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U), where S(..., X, Q, QX)
  template <class L, class Tq, class Tg, class Ts>
  void jacobianSourceHACK( const ParamTuple<L, VectorX, TupleClass<>>& param,
                       const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, MatrixQ<Ts>& dsdu ) const
  {
    const VectorX& X = param.right();
    PDE::jacobianSourceHACK(param.left(), X[0], X[1], X[2], time_, q, gradq[0], gradq[1], gradq[2], dsdu);
  }

  //--------
  // jacobian of source wrt conservation variables: d(S)/d(U), where S(X, Q, QX)
  template <class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue( const VectorX& X,
                       const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, MatrixQ<Ts>& dsdu ) const
  {
    PDE::jacobianSourceAbsoluteValue(X[0], X[1], X[2], time_, q, gradq[0], gradq[1], gradq[2], dsdu);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U), where S(..., X, Q, QX)
  template <class L, class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue( const ParamTuple<L, VectorX, TupleClass<>>& param,
                       const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, MatrixQ<Ts>& dsdu ) const
  {
    const VectorX& X = param.right();
    PDE::jacobianSourceAbsoluteValue(param.left(), X[0], X[1], X[2], time_, q, gradq[0], gradq[1], gradq[2], dsdu);
  }


  //--------
  // jacobian of source wrt conservation variable gradient: d(S)/d(Ux), d(s)/d(Uy), where S(X, Q, QX)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSource( const VectorX& X,
                       const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, VectorMatrixQ<Ts>& dsdgradu ) const
  {
    PDE::jacobianGradientSource(X[0], X[1], X[2], time_,
                                q, gradq[0], gradq[1], gradq[2],
                                dsdgradu[0], dsdgradu[1],  dsdgradu[2]);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U), where S(..., X, Q, QX)
  template <class L, class Tq, class Tg, class Ts>
  void jacobianGradientSource( const ParamTuple<L, VectorX, TupleClass<>>& param,
                       const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, VectorMatrixQ<Ts>& dsdgradu ) const
  {
    const VectorX& X = param.right();
    PDE::jacobianGradientSource(param.left(), X[0], X[1], X[2], time_,
                                q, gradq[0], gradq[1], gradq[2],
                                dsdgradu[0], dsdgradu[1],  dsdgradu[2]);
  }

  //--------
  // jacobian of source wrt conservation variable gradient: d(S)/d(Ux), d(s)/d(Uy), where S(X, Q, QX)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSourceHACK( const VectorX& X,
                       const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, VectorMatrixQ<Ts>& dsdgradu ) const
  {
    PDE::jacobianGradientSourceHACK(X[0], X[1], X[2], time_,
                                q, gradq[0], gradq[1], gradq[2],
                                dsdgradu[0], dsdgradu[1],  dsdgradu[2]);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U), where S(..., X, Q, QX)
  template <class L, class Tq, class Tg, class Ts>
  void jacobianGradientSourceHACK( const ParamTuple<L, VectorX, TupleClass<>>& param,
                       const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, VectorMatrixQ<Ts>& dsdgradu ) const
  {
    const VectorX& X = param.right();
    PDE::jacobianGradientSourceHACK(param.left(), X[0], X[1], X[2], time_,
                                q, gradq[0], gradq[1], gradq[2],
                                dsdgradu[0], dsdgradu[1],  dsdgradu[2]);
  }

  //--------
  // jacobian of source wrt conservation variable gradient: d(S)/d(Ux), d(s)/d(Uy), where S(X, Q, QX)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSourceAbsoluteValue( const VectorX& X, const VectorX& N,
                       const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, MatrixQ<Ts>& divSdotN ) const
  {
    PDE::jacobianGradientSourceAbsoluteValue(X[0], X[1], X[2], time_, N[0], N[1], N[2],
                                q, gradq[0], gradq[1], gradq[2], divSdotN);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U), where S(..., X, Q, QX)
  template <class L, class Tq, class Tg, class Ts>
  void jacobianGradientSourceAbsoluteValue( const ParamTuple<L, VectorX, TupleClass<>>& param, const VectorX& N,
                       const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, MatrixQ<Ts>& divSdotN ) const
  {
    const VectorX& X = param.right();
    PDE::jacobianGradientSourceAbsoluteValue(param.left(), X[0], X[1], X[2], time_, N[0], N[1], N[2],
                                q, gradq[0], gradq[1], gradq[2], divSdotN);
  }


  //--------
  // jacobian of source wrt conservation variable gradient: d(S)/d(Ux), d(s)/d(Uy), where S(X, Q, QX)
  template <class Tq, class Tg, class Th, class Ts>
  void jacobianGradientSourceGradient( const VectorX& X,
                       const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq,
                       const TensorSymArrayQ<Th>& hessq,
                       MatrixQ<Ts>& div_dsdgradu ) const
  {
    PDE::jacobianGradientSourceGradient(X[0], X[1], X[2], time_,
                                q, gradq[0], gradq[1], gradq[2],
                                hessq(0,0), hessq(1,0), hessq(1,1),
                                hessq(2,0), hessq(2,1), hessq(2,2),
                                div_dsdgradu);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U), where S(..., X, Q, QX)
  template <class L, class Tq, class Tg, class Th, class Ts>
  void jacobianGradientSourceGradient( const ParamTuple<L, VectorX, TupleClass<>>& param,
                       const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq,
                       const TensorSymArrayQ<Th>& hessq,
                       MatrixQ<Ts>& div_dsdgradu ) const
  {
    const VectorX& X = param.right();
    PDE::jacobianGradientSourceGradient(param.left(), X[0], X[1], X[2], time_,
                                q, gradq[0], gradq[1], gradq[2],
                                hessq(0,0), hessq(1,0), hessq(1,1),
                                hessq(2,0), hessq(2,1), hessq(2,2),
                                div_dsdgradu);
  }


  //--------
  // jacobian of source wrt conservation variable gradient: d(S)/d(Ux), d(s)/d(Uy), where S(X, Q, QX)
  template <class Tq, class Tg, class Th, class Ts>
  void jacobianGradientSourceGradientHACK( const VectorX& X,
                       const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq,
                       const TensorSymArrayQ<Th>& hessq,
                       MatrixQ<Ts>& div_dsdgradu ) const
  {
    PDE::jacobianGradientSourceGradientHACK(X[0], X[1], X[2], time_,
                                q, gradq[0], gradq[1], gradq[2],
                                hessq(0,0), hessq(1,0), hessq(1,1),
                                hessq(2,0), hessq(2,1), hessq(2,2),
                                div_dsdgradu);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U), where S(..., X, Q, QX)
  template <class L, class Tq, class Tg, class Th, class Ts>
  void jacobianGradientSourceGradientHACK( const ParamTuple<L, VectorX, TupleClass<>>& param,
                       const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq,
                       const TensorSymArrayQ<Th>& hessq,
                       MatrixQ<Ts>& div_dsdgradu ) const
  {
    const VectorX& X = param.right();
    PDE::jacobianGradientSourceGradientHACK(param.left(), X[0], X[1], X[2], time_,
                                q, gradq[0], gradq[1], gradq[2],
                                hessq(0,0), hessq(1,0), hessq(1,1),
                                hessq(2,0), hessq(2,1), hessq(2,2),
                                div_dsdgradu);
  }



  //--------
  // right-hand-side forcing function: RHS(X)
  template <class T>
  void forcingFunction( const VectorX& X,
                        ArrayQ<T>& source ) const
  {
    PDE::forcingFunction(X[0], X[1], X[2], time_, source);
  }

  // right-hand-side forcing function: RHS(..., X)
  template <class L, class T>
  void forcingFunction( const ParamTuple<L, VectorX, TupleClass<>>& param,
                        ArrayQ<T>& source ) const
  {
    const VectorX& X = param.right();
    PDE::forcingFunction(param.left(), X[0], X[1], X[2], time_, source);
  }

  using PDE::forcingFunction;

  //--------
  // characteristic speed (needed for timestep): c(X, DX, Q)
  template <class T>
  void speedCharacteristic( const VectorX& X,
                            const VectorX& DX, const ArrayQ<T>& q, Real& speed ) const
  {
    PDE::speedCharacteristic(X[0], X[1], X[2], time_, DX[0], DX[1], DX[2], q, speed);
  }

  // characteristic speed (needed for timestep): c(..., X, DX, Q)
  template <class L, class T>
  void speedCharacteristic( const ParamTuple<L, VectorX, TupleClass<>>& param,
                            const VectorX& DX, const ArrayQ<T>& q, Real& speed ) const
  {
    const VectorX& X = param.right();
    PDE::speedCharacteristic(param.left(), X[0], X[1], X[2], time_, DX[0], DX[1], DX[2], q, speed);
  }

  //--------
  // characteristic speed: c(X, Q)
  template <class T>
  void speedCharacteristic( const VectorX& X, const ArrayQ<T>& q, T& speed ) const
  {
    PDE::speedCharacteristic(X[0], X[1], X[2], time_, q, speed);
  }

  // characteristic speed: c(..., X, Q)
  template <class L, class T>
  void speedCharacteristic( const ParamTuple<L, VectorX, TupleClass<>>& param,
                            const ArrayQ<T>& q, T& speed ) const
  {
    const VectorX& X = param.right();
    PDE::speedCharacteristic(param.left(), X[0], X[1], X[2], time_, q, speed);
  }

  //--------
  // update fraction needed for physically valid state
  template <class T>
  void updateFraction( const VectorX& X,
                       const ArrayQ<T>& q, const ArrayQ<T>& dq,
                       const Real maxChangeFraction, Real& updateFraction ) const
  {
    PDE::updateFraction(X[0], X[1], X[2], time_, q, dq, maxChangeFraction, updateFraction);
  }

  // update fraction needed for physically valid state
  template <class L, class T>
  void updateFraction( const ParamTuple<L, VectorX, TupleClass<>>& param,
                       const ArrayQ<T>& q, const ArrayQ<T>& dq,
                       const Real maxChangeFraction, Real& updateFraction ) const
  {
    const VectorX& X = param.right();
    PDE::updateFraction(param.left(), X[0], X[1], X[2], time_, q, dq, maxChangeFraction, updateFraction);
  }


  //--------
  using PDE::isValidState;        // is state physically valid
  using PDE::setDOFFrom; // set from primitive variable array
  using PDE::dump;
protected:
  const Real dummyTime_ = 0;
  const Real& time_;
};



} //namespace SANS

#endif  // PDENDCONVERTSPACE3D_H
