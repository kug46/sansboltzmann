// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef OUTPUTNDCONVERT_FWD_H
#define OUTPUTNDCONVERT_FWD_H

// Forward declare all the OutputNDConvert classes

namespace SANS
{

template <class PhysDim, class OutputFunctional>
class OutputNDConvertSpace;

template <class PhysDim, class OutputFunctional>
class OutputNDConvertSpaceTime;

}

#endif //OUTPUTNDCONVERT_FWD_H
