// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCNDCONVERT_FWD_H
#define BCNDCONVERT_FWD_H

// Forward declare all the BCNDConvert classes

namespace SANS
{

template <class PhysDim, class BC>
class BCNDConvertSpace;

template <class PhysDim, class BC>
class BCNDConvertSpaceTime;

}

#endif //BCNDCONVERT_FWD_H
