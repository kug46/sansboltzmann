// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDENDCONVERT_FWD_H
#define PDENDCONVERT_FWD_H

// Forward declare all the PDENDConvert classes

namespace SANS
{

class TemporalMarch;
class TemporalSpaceTime;

template <class PhysDim, class PDE>
class PDENDConvertSpace;

template <class PhysDim, class PDE>
class PDENDConvertSpaceTime;

}

#endif //PDENDCONVERT_FWD_H
