// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef TIMEMARCH_H
#define TIMEMARCH_H

#include "tools/SANSnumerics.h"

namespace SANS
{

struct GlobalTime
{
  GlobalTime() : time(0) {}
  explicit GlobalTime(const Real t) : time(t) {}

  // We pass around the time marching with reference. This safe guards from accidental copies
  GlobalTime( const GlobalTime& ) = delete;
  GlobalTime& operator=( const GlobalTime& ) = delete;

  Real& operator= (const Real t) { time  = t; return time; }
  Real& operator+=(const Real t) { time += t; return time; }
  Real& operator-=(const Real t) { time -= t; return time; }
  Real& operator*=(const Real t) { time *= t; return time; }
  Real& operator/=(const Real t) { time /= t; return time; }

  bool operator==(const Real t) { return time == t; }
  bool operator<(const Real t) { return time < t; }
  bool operator>(const Real t) { return time > t; }

  operator const Real&() const { return time; }

  Real time;
};

inline bool operator==(const Real t, const GlobalTime& time) { return t == time.time; }
inline bool operator<(const Real t, const GlobalTime& time) { return t < time.time; }
inline bool operator>(const Real t, const GlobalTime& time) { return t > time.time; }

}

#endif //TIMEMARCH_H
