// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef OUTPUTNDCONVERTSPACE4D_H
#define OUTPUTNDCONVERTSPACE4D_H

// 3D NDConvert Output class for steady PDEs

// Python must be included first
#include "Python/PyDict.h"

#include <typeinfo> // typeid

#include "tools/SANSnumerics.h"     // Real
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "Topology/Dimension.h"
#include "Field/Tuple/ParamTuple.h"

#include "OutputNDConvert_fwd.h"
#include "GlobalTime.h"
#include "Temporal.h"

namespace SANS
{

template <class OutputFunctional>
class OutputNDConvertSpace<PhysD4, OutputFunctional> : public OutputFunctional
{
public:
  static_assert( std::is_same<PhysD4, typename OutputFunctional::PhysDim>::value, "Physical dimensions should match" );

  static const int D = PhysD4::D;

  typedef DLA::VectorS<D,Real> VectorX;

  template<class Z> using ArrayQ          = typename OutputFunctional::template ArrayQ<Z>;  // solution arrays
  template<class Z> using VectorArrayQ    = DLA::VectorS<D, ArrayQ<Z> >;                    // solution gradient arrays
  template<class Z> using ArrayJ          = typename OutputFunctional::template ArrayJ<Z>;  // output arrays

  using OutputFunctional::operator();

  template< class... OutputArgs > // cppcheck-suppress noExplicitConstructor
  OutputNDConvertSpace(OutputArgs&&... args) : OutputFunctional(std::forward<OutputArgs>(args)...) {}

  template< class... OutputArgs > // cppcheck-suppress noExplicitConstructor
  OutputNDConvertSpace(GlobalTime& time, OutputArgs&&... args) : OutputFunctional(std::forward<OutputArgs>(args)...) {}

  explicit OutputNDConvertSpace( const PyDict& d ) : OutputFunctional(d) {}

  OutputNDConvertSpace(const GlobalTime& time, const PyDict& d ) : OutputFunctional(d) {}

  OutputNDConvertSpace( const OutputNDConvertSpace& ) = delete;
  OutputNDConvertSpace& operator=( const OutputNDConvertSpace& ) = delete;

  virtual ~OutputNDConvertSpace() {}

  TemporalMode temporal() const { return eSteady; } // 4d is steady

  virtual const std::type_info& derivedTypeID() const override { return typeid(*this); }

  template<class Tq, class Tg, class To>
  void operator()( const VectorX& X,
                   const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, ArrayJ<To>& output ) const
  {
    OutputFunctional::operator()(X[0], X[1], X[2], X[3],
                                 q, gradq[0], gradq[1], gradq[2], gradq[3], output);
  }

  template<class Tq, class Tg, class To>
  void operator()( const VectorX& X, const VectorX& N,
                   const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, ArrayJ<To>& output ) const
  {
    SANS_DEVELOPER_EXCEPTION("implement!");
    //OutputFunctional::operator()(X[0], X[1], X[2], X[3], N[0], N[1], N[2], N[3], q, gradq[0], gradq[1], gradq[2], gradq[3], output);
  }

  void operator()( const VectorX& X, ArrayQ<Real>& weights ) const
  {
    OutputFunctional::operator()(X[0], X[1], X[2], X[3], weights);
  }

  template<class L, class Tq, class Tg, class To>
  void operator()( const ParamTuple<L, VectorX, TupleClass<>>& param,
                   const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, ArrayJ<To>& output ) const
  {
    const VectorX& X = param.right();
    OutputFunctional::operator()(param.left(), X[0], X[1], X[2], X[3],
                                 q, gradq[0], gradq[1], gradq[2], gradq[3], output);
  }

  template<class L, class Tq, class Tg, class To>
  void operator()( const ParamTuple<L, VectorX, TupleClass<>>& param, const VectorX& N,
                   const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, ArrayJ<To>& output ) const
  {
    SANS_DEVELOPER_EXCEPTION("implement");
    //const VectorX& X = param.right();
    //OutputFunctional::operator()(param.left(), X[0], X[1], X[2], time_,
    //                             N[0], N[1], N[2], q, gradq[0], gradq[1], gradq[2], output);
  }

};

} //namespace SANS

#endif  // OUTPUTNDCONVERTSPACE4D_H
