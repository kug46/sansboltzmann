// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCNDCONVERTSPACE1D_H
#define BCNDCONVERTSPACE1D_H

#include "Python/PyDict.h" //Python must be included first

#include <typeinfo> // typeid

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"

#include "Field/Tuple/ParamTuple.h"

#include "pde/BCCategory.h"

#include "BCNDConvert_fwd.h"
#include "PDENDConvert_fwd.h"
#include "GlobalTime.h"
#include "Temporal.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template <class BC>
class BCNDConvertSpace<PhysD1, BC> : public BC
{
public:
  static_assert( std::is_same<PhysD1, typename BC::PhysDim>::value, "Physical dimensions should match" );

  typedef typename BC::Category Category;
  typedef typename BC::ParamsType ParamsType;

  static const int D = BC::D;                     // physical dimensions
  static const int N = BC::N;                     // total solution variables

  static const int NBC = BC::NBC;                   // total BCs

  typedef DLA::VectorS<D,Real> VectorX;

  template<class T> using ArrayQ        = typename BC::template ArrayQ<T>;
  template<class T> using MatrixQ       = typename BC::template MatrixQ<T>;    // matrices
  template<class Z> using VectorArrayQ  = DLA::VectorS<D, ArrayQ<Z> >;         // vector of solution arrays, i.e. solution gradients/flux vector

  template< class... BCArgs > // cppcheck-suppress noExplicitConstructor
  BCNDConvertSpace(BCArgs&&... args) : BC(std::forward<BCArgs>(args)...), time_(dummyTime_) {}

  template< class... BCArgs >
  BCNDConvertSpace(GlobalTime& time, BCArgs&&... args) : BC(std::forward<BCArgs>(args)...), time_(time.time) {}

  template<class PDE>
  BCNDConvertSpace(const PDENDConvertSpace<PhysD1,PDE>& pde, PyDict& d) : BC(pde, d), time_(dummyTime_)
  {
    SANS_ASSERT(pde.temporal() == temporal());
  }

  template<class PDE>
  BCNDConvertSpace(const GlobalTime& time, const PDENDConvertSpace<PhysD1,PDE>& pde, PyDict& d) : BC(pde, d), time_(time.time)
  {
    SANS_ASSERT(pde.temporal() == temporal());
  }

  virtual ~BCNDConvertSpace() {}

  TemporalMode temporal() const { return &time_ == &dummyTime_ ? eSteady : eUnsteady; }

  BCNDConvertSpace( const BCNDConvertSpace& ) = delete;
  BCNDConvertSpace& operator=( const BCNDConvertSpace& ) = delete;

  virtual const std::type_info& derivedTypeID() const override { return typeid(*this); }

  // BC residual: BC equation
  template <class T>
  void strongBC( const VectorX& X, const VectorX& Nrm,
                 const ArrayQ<T>& q, const VectorArrayQ<T>& gradq, ArrayQ<T>& rsdBC ) const
  {
    BC::strongBC(X[0], time_, Nrm[0], q, gradq[0], rsdBC);
  }

  template <class L, class T, class R>
  void strongBC( const ParamTuple<L, VectorX, TupleClass<>>& param, const VectorX& Nrm,
                 const ArrayQ<T>& q, const VectorArrayQ<T>& gradq, ArrayQ<R>& rsdBC ) const
  {
    const VectorX& X = param.right();
    BC::strongBC(param.left(), X[0], time_, Nrm[0], q, gradq[0], rsdBC);
  }

  // BC strong-form residual (with Lagrange multiplier)
  template <class T>
  void strongBC( const VectorX& X, const VectorX& Nrm,
                 const ArrayQ<T>& q, const VectorArrayQ<T>& gradq, const ArrayQ<T>& lg, ArrayQ<T>& rsdBC ) const
  {
    BC::strongBC(X[0], time_, Nrm[0], q, gradq[0], lg, rsdBC);
  }

  template <class L, class T, class R>
  void strongBC( const ParamTuple<L, VectorX, TupleClass<>>& param, const VectorX& Nrm,
                 const ArrayQ<T>& q, const VectorArrayQ<T>& gradq, const ArrayQ<T>& lg, ArrayQ<R>& rsdBC ) const
  {
    const VectorX& X = param.right();
    BC::strongBC(param.left(), X[0], time_, Nrm[0], q, gradq[0], lg, rsdBC);
  }

  // conventional formulation BC weighting function
  template <class T>
  void weightBC( const VectorX& X, const VectorX& Nrm,
                 const ArrayQ<T>& q, const VectorArrayQ<T>& gradq, MatrixQ<T>& wghtBC ) const
  {
    BC::weightBC(X[0], time_, Nrm[0], q, gradq[0], wghtBC);
  }

  template <class L, class T, class W>
  void weightBC( const ParamTuple<L, VectorX, TupleClass<>>& param, const VectorX& Nrm,
                 const ArrayQ<T>& q, const VectorArrayQ<T>& gradq, MatrixQ<W>& wghtBC ) const
  {
    const VectorX& X = param.right();
    BC::weightBC(param.left(), X[0], time_, Nrm[0],q, gradq[0], wghtBC);
  }

  // Lagrange multiplier weighting function
  template <class T>
  void weightLagrange( const VectorX& X, const VectorX& Nrm,
                       const ArrayQ<T>& q, const VectorArrayQ<T>& gradq, MatrixQ<T>& wghtLG ) const
  {
    BC::weightLagrange(X[0], time_, Nrm[0], q, gradq[0], wghtLG);
  }

  template <class L, class T, class W>
  void weightLagrange( const ParamTuple<L, VectorX, TupleClass<>>& param, const VectorX& Nrm,
                 const ArrayQ<T>& q, const VectorArrayQ<T>& gradq, MatrixQ<W>& wghtLG ) const
  {
    const VectorX& X = param.right();
    BC::weightLagrange(param.left(), X[0], time_, Nrm[0], q, gradq[0], wghtLG);
  }

  // Lagrange multiplier rhs
  template <class T>
  void rhsLagrange( const VectorX& X, const VectorX& Nrm,
                    const ArrayQ<T>& q, const VectorArrayQ<T>& gradq, ArrayQ<T>& rhsLG ) const
  {
    BC::rhsLagrange(X[0], time_, Nrm[0], q, gradq[0], rhsLG);
  }

  // Lagrange multiplier rhs
  template <class L, class T, class R>
  void rhsLagrange( const ParamTuple<L, VectorX, TupleClass<>>& param, const VectorX& Nrm,
                    const ArrayQ<T>& q, const VectorArrayQ<T>& gradq, ArrayQ<R>& rhsLG ) const
  {
    const VectorX& X = param.right();
    BC::rhsLagrange(param.left(), X[0], time_, Nrm[0], q, gradq[0], rhsLG);
  }

  // BC jacobian:  Jacobian wrt Entropy variables
  template <class T>
  void jacobianEntropy( const VectorX& X, const VectorX& Nrm,
                        MatrixQ<T>& B ) const
  {
    BC::jacobianEntropy(X[0], time_, Nrm[0], B);

  }

  template <class L, class T>
  void jacobianEntropy( const ParamTuple<L, VectorX, TupleClass<>>& param, const VectorX& Nrm,
                        MatrixQ<T>& B ) const
  {
    const VectorX& X = param.right();
    BC::jacobianEntropy(param.left(), X[0], time_, Nrm[0], B);
  }

  // BC coefficients:  A u + B (kn un + ks us)
  template <class T>
  void coefficients(
      const VectorX& X, const VectorX& Nrm,
      MatrixQ<T>& A, MatrixQ<T>& B ) const
  {
    BC::coefficients(X[0], time_, Nrm[0], A, B);
  }

  template <class L, class T>
  void coefficients(
      const ParamTuple<L, VectorX, TupleClass<>>& param, const VectorX& Nrm,
      MatrixQ<T>& A, MatrixQ<T>& B ) const
  {
    const VectorX& X = param.right();
    BC::coefficients(param.left(), X[0], time_, Nrm[0], A, B);
  }

  template <class T>
  void data( const VectorX& X, const VectorX& Nrm, ArrayQ<T>& bcdata ) const
  {
    BC::data(X[0], time_, Nrm[0], bcdata);
  }

  template <class L, class T>
  void data( const ParamTuple<L, VectorX, TupleClass<>>& param, const VectorX& Nrm, ArrayQ<T>& bcdata ) const
  {
    const VectorX& X = param.right();
    BC::data(param.left(), X[0], time_, Nrm[0], bcdata);
  }

  template <class T>
  void state( const VectorX& X, const VectorX& Nrm, const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BC::state(X[0], time_, Nrm[0], qI, qB);
  }

  template <class L, class T, class B>
  void state( const ParamTuple<L, VectorX, TupleClass<>>& param, const VectorX& Nrm, const ArrayQ<T>& qI, ArrayQ<B>& qB ) const
  {
    const VectorX& X = param.right();
    BC::state(param.left(), X[0], time_, Nrm[0], qI, qB);
  }

  template <class T>
  void fluxNormal( const VectorX& X, const VectorX& Nrm, const ArrayQ<T>& qI, const VectorArrayQ<T>& gradqI,
                   const ArrayQ<T>& qB, ArrayQ<T>& Fn ) const
  {
    BC::fluxNormal(X[0], time_, Nrm[0], qI, gradqI[0], qB, Fn);
  }

  template <class L, class T, class Tf>
  void fluxNormal( const ParamTuple<L, VectorX, TupleClass<>>& param, const VectorX& Nrm, const ArrayQ<T>& qI, const VectorArrayQ<T>& gradqI,
                   ArrayQ<T>& qB, ArrayQ<Tf>& Fn ) const
  {
    const VectorX& X = param.right();
    BC::fluxNormal(param.left(), X[0], time_, Nrm[0], qI, gradqI[0], qB, Fn);
  }

  bool isValidState( const VectorX& Nrm, const ArrayQ<Real>& q ) const
  {
    return BC::isValidState(Nrm[0], q);
  }

protected:
  const Real dummyTime_ = 0;
  const Real& time_;
};

} //namespace SANS

#endif  // BCNDCONVERTSPACE1D_H
