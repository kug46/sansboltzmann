
INCLUDE( ${CMAKE_SOURCE_DIR}/CMakeInclude/ForceOutOfSource.cmake ) #This must be the first thing included

SET( NDCONVERT_SRC
  
  )

#ADD_LIBRARY( NDConvertLib STATIC ${NDCONVERT_SRC} )

# This should be deleted and replaced with the library if cpp files are added
ADD_CUSTOM_TARGET( NDConvertLib ) 

#Create the vera targest for this library
ADD_VERA_CHECKS_RECURSE( NDConvertLib *.h *.cpp )
ADD_HEADER_COMPILE_CHECK_RECURSE( NDConvertLib *.h )
#ADD_CPPCHECK( NDConvertLib ${NDCONVERT_SRC} )