// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDENDCONVERTSPACETIME1D_H
#define PDENDCONVERTSPACETIME1D_H

// 1D PDE class wrapper for space-time calculations

#include "tools/SANSnumerics.h"     // Real
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "Topology/ElementTopology.h"
#include "Topology/Dimension.h"
#include "Field/Tuple/ParamTuple.h"

#include "PDENDConvert_fwd.h"
#include "Temporal.h"

#include <type_traits> // is_same
#include <utility>     // forward

namespace SANS
{

//----------------------------------------------------------------------------//
// Methods for solving unsteady advection diffusion equations with a space time formulation
//
// Strong form: dU(Q)/dt + div.(F(Q) - Fv(Q, QX)) + S(Q, QX) = RHS(X)
//
// Weak form: < phi n.(U(Q) + F(Q) - Fv(Q, QX)) > - grad(phi).(U(Q) + F(Q) - Fv(Q, QX)) + phi S(Q, QX) = phi RHS(X)
//
// template parameters:
//   PDE                     The PDE class that is wrapped
//
// member functions forward calls on to PDE class, with a dummy time added to spatial coordinates
//
//   .fluxAdvectiveTime                   temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime           jacobian of temporal flux: d(Ft)/dU
//   .masterState                         master state: U(Q)
//   .jacobianMasterState                 jacobian of master state wrt q: dU(Q)/dQ
//   .fluxAdvective                       advective/inviscid fluxes: F(Q)
//   .fluxAdvectiveUpwind                 upwinded advective/inviscid fluxes: n.F(QL, QR)
//   .jacobianFluxAdvective               jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
//   .jacobianFluxAdvectiveAbsoluteValue  absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
//   .strongFluxAdvective                 strong form advective fluxes: div.(F)
//   .fluxViscous                         viscous fluxes: Fv(Q, QX) and n.Fv(QL, QLX, QR, QRX)
//   .diffusionViscous                    viscous diffusion coefficient: d(Fv)/d(UX)
//   .jacobianFluxViscous                 jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
//   .strongFluxViscous                   strong form viscous fluxes: div.(Fv)
//   .source                              solution-dependent source: S(Q, QX)
//   .sourceTrace                dual-consistent source
//   .jacobianSource                      jacobian of source wrt conservation variables: d(S)/d(U)
//   .forcingFunction                     right-hand-side forcing function: f(X)
//   .speedCharacteristic                 characteristic speed (needed for timestep)
//   .updateFraction                      update fraction needed for physically valid state
//   .isValidState                        T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom                          set from primitive variable array
//----------------------------------------------------------------------------//

template<class PDE_>
class PDENDConvertSpaceTime<PhysD1, PDE_> : public PDE_
{
public:
  typedef PDE_ PDE;

  static_assert( std::is_same<PhysD1, typename PDE::PhysDim>::value, "Physical dimensions should match" );

  typedef PhysD2 PhysDim; // increase the physical dimension of the PDE to include time
  static const int D = PhysDim::D;
  typedef TemporalSpaceTime Temporal;

  typedef DLA::VectorS<D,Real> VectorXT;                             // physical coordinate vector, i.e. cartesian or cylindrical, including time

  template<class Z> using ArrayQ          = typename PDE::template ArrayQ<Z>;    // solution/flux arrays
  template<class Z> using MatrixQ         = typename PDE::template MatrixQ<Z>;   // diffusion matrix/flux jacobians
  template<class Z> using VectorArrayQ    = DLA::VectorS<D, ArrayQ<Z> >;         // vector of solution arrays, i.e. solution gradients/flux vector
  template<class Z> using VectorMatrixQ   = DLA::VectorS<D, MatrixQ<Z> >;        // vector of jacobians, i.e. flux jacobians d(F)/d(U)
  template<class Z> using TensorSymArrayQ = DLA::MatrixSymS<D, ArrayQ<Z> >;      // hessian of solution, i.e QXX
  template<class Z> using TensorMatrixQ   = DLA::MatrixS<D, D, MatrixQ<Z> >;     // diffusion, i.e. viscous flux jacobian d(Fv)/d(UX)
  template<class Z> using TensorArrayQ   =  DLA::MatrixS<D, D, ArrayQ<Z> >;   // diffusion, i.e.  gradient of viscous flux grad (Fv)
  template<class Z> using VectorTensorMatrixQ= DLA::VectorS<D, DLA::MatrixS<D, D, MatrixQ<Z>>>;

  // Constructor forwards arguments to PDE class using varargs
  template< class... PDEArgs > // cppcheck-suppress noExplicitConstructor
  PDENDConvertSpaceTime(PDEArgs&&... args) : PDE(std::forward<PDEArgs>(args)...) {}

  ~PDENDConvertSpaceTime() {}

  PDENDConvertSpaceTime( const PDENDConvertSpaceTime& ) = delete;
  PDENDConvertSpaceTime& operator=( const PDENDConvertSpaceTime& ) = delete;

  TemporalMode temporal() const { return eSpaceTime; }

  bool hasFluxAdvective() const { return true; } //Space-time PDEs should always have an advective flux for the time-term

  //--------
  // master state: U(Q)
  template <class Tq, class Tu>
  void masterState( const VectorXT& X, const ArrayQ<Tq>& q, ArrayQ<Tu>& u ) const
  {
    PDE::masterState(X[0], X[1], q, u);
  }

  //--------
  // master state: U(Q)
  template <class L, class Tq, class Tu>
  void masterState( const ParamTuple<L, VectorXT, TupleClass<>>& param,
                         const ArrayQ<Tq>& q, ArrayQ<Tu>& u ) const
  {
    const VectorXT& X = param.right();
    PDE::masterState(param.left(), X[0], X[1], q, u);
  }


  //--------
  // conservative variable: U(Q)
  template <class Tq, class Tg, class Tu>
  void masterStateGradient( const VectorXT& X, const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, VectorArrayQ<Tu>& gradu ) const
  {
    PDE::masterStateGradient(X[0], X[1], q, gradq[0],gradu[0]);
  }

  //--------
  // conservative variable: U(Q)
  template <class L, class Tq, class Tg, class Tu>
  void masterStateGradient( const ParamTuple<L, VectorXT, TupleClass<>>& param,
                            const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, VectorArrayQ<Tu>& gradu  ) const
  {
    const VectorXT& X = param.right();
    PDE::masterStateGradient(param.left(), X[0], X[1], q, gradq[0],gradu[0]);
  }

  //--------
  // conservative variable: U(Q)
  template <class Tq, class Tg, class Th, class Tu>
  void masterStateHessian( const VectorXT& X, const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, const TensorSymArrayQ<Th>& hessq,
                           TensorSymArrayQ<Tu>& hessu ) const
  {
    PDE::masterStateHessian(X[0], X[1],q, gradq[0],
                             hessq(0,0),  hessu(0,0));
  }

  //--------
  // conservative variable: U(Q)
  template <class L, class Tq, class Tg, class Th, class Tu>
  void masterStateHessian( const ParamTuple<L, VectorXT, TupleClass<>>& param,
                           const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, const TensorSymArrayQ<Th>& hessq,
                           TensorSymArrayQ<Tu>& hessu ) const
  {
    const VectorXT& X = param.right();
    PDE::masterStateHessian(param.left(), X[0], X[1], q, gradq[0],
                             hessq(0,0),  hessu(0,0));
  }


  //--------
  // jacobian of master state wrt q: dU/dQ
  template <class Tq, class Tf>
  void jacobianMasterState( const VectorXT& X,
                                 const ArrayQ<Tq>& q, MatrixQ<Tf>& a ) const
  {
    PDE::jacobianMasterState(X[0], X[1], q, a);
  }

  //--------
  // jacobian of master state wrt q: dU/dQ
  template <class L, class Tq, class Tf>
  void jacobianMasterState( const ParamTuple<L, VectorXT, TupleClass<>>& param,
                                 const ArrayQ<Tq>& q, MatrixQ<Tf>& a ) const
  {
    const VectorXT& X = param.right();
    PDE::jacobianMasterState(param.left(), X[0], X[1], q, a);
  }



  //--------
  // conservative variable: U(Q)
  template <class Tq, class Tqp, class Tu>
  void perturbedMasterState( const VectorXT& X, const ArrayQ<Tq>& q, const ArrayQ<Tqp>& dq, ArrayQ<Tu>& du ) const
  {
    PDE::perturbedMasterState(X[0], X[1], q, dq, du);
  }

  //--------
  // conservative variable: U(Q)
  template <class L, class Tq, class Tqp, class Tu>
  void perturbedMasterState( const ParamTuple<L, VectorXT, TupleClass<>>& param,
                    const ArrayQ<Tq>& q, const ArrayQ<Tqp>& dq, ArrayQ<Tu>& du ) const
  {
    const VectorXT& X = param.right();
    PDE::perturbedMasterState(param.left(), X[0], X[1], q, dq, du);
  }

  //--------
  // temporal flux: Ft(Q)
  template <class Tq, class Tf>
  void fluxAdvectiveTime( const VectorXT& X, const ArrayQ<Tq>& q, ArrayQ<Tf>& ft ) const
  {
    PDE::fluxAdvectiveTime(X[0], X[1], q, ft);
  }

  //--------
  // temporal flux: Ft(Q)
  template <class L, class Tq, class Tf>
  void fluxAdvectiveTime( const ParamTuple<L, VectorXT, TupleClass<>>& param,
                         const ArrayQ<Tq>& q, ArrayQ<Tf>& ft ) const
  {
    const VectorXT& X = param.right();
    PDE::fluxAdvectiveTime(param.left(), X[0], X[1], q, ft);
  }

  //--------
  // jacobian of master state wrt q: dU/dQ
  template <class Tq, class Tf>
  void jacobianFluxAdvectiveTime( const VectorXT& X,
                                  const ArrayQ<Tq>& q, MatrixQ<Tf>& a ) const
  {
    PDE::jacobianFluxAdvectiveTime(X[0], X[1], q, a);
  }

  //--------
  // jacobian of master state wrt q: dU/dQ
  template <class L, class Tq, class Tf>
  void jacobianFluxAdvectiveTime( const ParamTuple<L, VectorXT, TupleClass<>>& param,
                                 const ArrayQ<Tq>& q, MatrixQ<Tf>& a ) const
  {
    const VectorXT& X = param.right();
    PDE::jacobianFluxAdvectiveTime(param.left(), X[0], X[1], q, a);
  }

  //--------
  // advective flux: F(X, Q)
  template <class T, class Tf>
  void fluxAdvective( const VectorXT& X,
                      const ArrayQ<T>& q, VectorArrayQ<Tf>& F ) const
  {
    PDE::fluxAdvective(X[0], X[1], q, F[0]);
    PDE::fluxAdvectiveTime( X[0], X[1], q, F[1] );
  }

  // advective flux: F(..., X, Q)
  template <class L, class T, class Tf>
  void fluxAdvective( const ParamTuple<L, VectorXT, TupleClass<>>& param,
                      const ArrayQ<T>& q, VectorArrayQ<Tf>& F ) const
  {
    const VectorXT& X = param.right();
    PDE::fluxAdvective(param.left(), X[0], X[1], q, F[0]);
    PDE::fluxAdvectiveTime( param.left(), X[0], X[1], q, F[1] );
  }


  //--------
  // advective flux: n . (F(X, QL) + F(X, QR) + Upwinding)/2
  template <class T, class Tf>
  void fluxAdvectiveUpwind( const VectorXT& X,
                            const ArrayQ<T>& qL, const ArrayQ<T>& qR, const VectorXT& N, ArrayQ<Tf>& f ) const
  {
    PDE::fluxAdvectiveUpwindSpaceTime(X[0], X[1], qL, qR, N[0], N[1], f);
  }

  // advective flux: n . (F(..., X, QL) + F(..., X, QR) + Upwinding)/2
  template <class L, class T, class Tf>
  void fluxAdvectiveUpwind( const ParamTuple<L, VectorXT, TupleClass<>>& param,
                            const ArrayQ<T>& qL, const ArrayQ<T>& qR, const VectorXT& N, ArrayQ<Tf>& f ) const
  {
    const VectorXT& X = param.right();
    PDE::fluxAdvectiveUpwindSpaceTime(param.left(), X[0], X[1], qL, qR, N[0], N[1], f);
  }

  //--------
  // advective flux: n . (F(X, QL) + F(X, QR) + Upwinding)/2
  template <class T, class Tf>
  void fluxAdvectiveUpwind( const VectorXT& X,
                            const ArrayQ<T>& qL, const ArrayQ<T>& qR, const VectorXT& N, ArrayQ<Tf>& f, const Real &scale ) const
  {
    PDE::fluxAdvectiveUpwindSpaceTime(X[0], X[1], qL, qR, N[0], N[1], f, scale);
  }

  // advective flux: n . (F(..., X, QL) + F(..., X, QR) + Upwinding)/2
  template <class L, class T, class Tf>
  void fluxAdvectiveUpwind( const ParamTuple<L, VectorXT, TupleClass<>>& param,
                            const ArrayQ<T>& qL, const ArrayQ<T>& qR, const VectorXT& N, ArrayQ<Tf>& f, const Real &scale ) const
  {
    const VectorXT& X = param.right();
    PDE::fluxAdvectiveUpwindSpaceTime(param.left(), X[0], X[1], qL, qR, N[0], N[1], f, scale);
  }


  //--------
  // jacobian of advective flux wrt conservation variables: d(U)/d(U) + d(F)/d(U) (advective velocity matrix), where F(X, Q)
  template <class Tq, class Ta>
  void jacobianFluxAdvective( const VectorXT& X,
                              const ArrayQ<Tq>& q, VectorMatrixQ<Ta>& a ) const
  {
    PDE::jacobianFluxAdvective(X[0], X[1], q, a[0]);
    a[1] += DLA::Identity(); //Temporal flux Jacobian with respect to conservative variables is the identity matrix by definition
  }

  // jacobian of advective flux wrt conservation variables: d(U)/d(U) + d(F)/d(U) (advective velocity matrix), where F(..., X, Q)
  template <class L, class Tq, class Ta>
  void jacobianFluxAdvective( const ParamTuple<L, VectorXT, TupleClass<>>& param,
                              const ArrayQ<Tq>& q, VectorMatrixQ<Ta>& a ) const
  {
    const VectorXT& X = param.right();
    PDE::jacobianFluxAdvective(param.left(), X[0], X[1], q, a[0]);
    a[1] += DLA::Identity(); //Temporal flux Jacobian with respect to conservative variables is the identity matrix by definition
  }


  //--------
  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|, where F(X, Q)
  template <class Tq, class Ta>
  void jacobianFluxAdvectiveAbsoluteValue( const VectorXT& X,
                                           const ArrayQ<Tq>& q, const VectorXT& N, MatrixQ<Ta>& a ) const
  {
    PDE::jacobianFluxAdvectiveAbsoluteValueSpaceTime(X[0], X[1], q, N[0], N[1], a);
  }

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|, where F(..., X, Q)
  template <class L, class Tq, class Ta>
  void jacobianFluxAdvectiveAbsoluteValue( const ParamTuple<L, VectorXT, TupleClass<>>& param,
                                           const ArrayQ<Tq>& q, const VectorXT& N, MatrixQ<Ta>& a ) const
  {
    const VectorXT& X = param.right();
    PDE::jacobianFluxAdvectiveAbsoluteValueSpaceTime(param.left(), X[0], X[1], q, N[0], N[1], a);
  }


  //--------
  // strong form advective fluxes: div.(F), where F(X, Q)
  template <class Tq, class Tg, class Ts>
  void strongFluxAdvective( const VectorXT& X,
                            const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, ArrayQ<Ts>& strongPDE ) const
  {
    PDE::strongFluxAdvective(X[0], X[1], q, gradq[0], strongPDE);
    PDE::strongFluxAdvectiveTime( X[0], X[1], q, gradq[1], strongPDE );
  }

  // strong form advective fluxes: div.(F), where F(..., X, Q)
  template <class L, class Tq, class Tg, class Ts>
  void strongFluxAdvective( const ParamTuple<L, VectorXT, TupleClass<>>& param,
                            const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, ArrayQ<Ts>& strongPDE ) const
  {
    const VectorXT& X = param.right();
    PDE::strongFluxAdvective(param.left(), X[0], X[1], q, gradq[0], strongPDE);
    PDE::strongFluxAdvectiveTime( param, X[0], X[1], q, gradq[1], strongPDE );
  }


  //--------
  // viscous flux: Fv(X, Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscous( const VectorXT& X,
                    const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, VectorArrayQ<Tf>& F ) const
  {
    PDE::fluxViscousSpaceTime(X[0], X[1], q, gradq[0], gradq[1], F[0], F[1]);
  }

  // viscous flux: Fv(..., X, Q, QX)
  template <class L, class Tq, class Tg, class Tf>
  void fluxViscous( const ParamTuple<L, VectorXT, TupleClass<>>& param,
                    const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, VectorArrayQ<Tf>& F ) const
  {
    const VectorXT& X = param.right();
    PDE::fluxViscousSpaceTime(param.left(), X[0], X[1], q, gradq[0], gradq[1], F[0], F[1]);
  }


  //--------
  // viscous flux: n . ( Fv(X, QL, QLX) + Fv(X, QR, QRX) )/2
  template <class Tq, class Tg, class Tf>
  void fluxViscous( const VectorXT& XL,
                    const VectorXT& XR,
                    const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqL,
                    const ArrayQ<Tq>& qR, const VectorArrayQ<Tg>& gradqR,
                    const VectorXT& N, ArrayQ<Tf>& f ) const
  {
    PDE::fluxViscousSpaceTime(XL[0], XL[1],
                              qL, gradqL[0], gradqL[1],
                              qR, gradqR[0], gradqR[1],
                              N[0], N[1], f);
  }

  // viscous flux: n . ( Fv(..., X, QL, QLX) + Fv(..., X, QR, QRX) )/2
  template <class L, class Tq, class Tg, class Tf>
  void fluxViscous( const ParamTuple<L, VectorXT, TupleClass<>>& paramL,
                    const ParamTuple<L, VectorXT, TupleClass<>>& paramR,
                    const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqL,
                    const ArrayQ<Tq>& qR, const VectorArrayQ<Tg>& gradqR,
                    const VectorXT& N, ArrayQ<Tf>& f ) const
  {
    const VectorXT& X = paramL.right();
    PDE::fluxViscousSpaceTime(paramL.left(), paramR.left(), X[0], X[1],
                              qL, gradqL[0], gradqL[1],
                              qR, gradqR[0], gradqR[1],
                              N[0], N[1], f);
  }


  //--------
  // viscous flux: Fv(X, Q, QX)
  template <class Tq, class Tg, class Tgu, class Tf>
  void perturbedGradFluxViscous( const VectorXT& X,
                const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, const VectorArrayQ<Tgu>& graddq, VectorArrayQ<Tf>& dF ) const
  {
    PDE::perturbedGradFluxViscous(X[0], X[1], q, gradq[0], graddq[0], dF[0]);
  }

  // viscous flux: Fv(..., X, Q, QX)
  template <class L, class Tq, class Tg, class Tgu, class Tf>
  void perturbedGradFluxViscous( const ParamTuple<L, VectorXT, TupleClass<>>& param,
                const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, const VectorArrayQ<Tgu>& graddq, VectorArrayQ<Tf>& dF ) const
  {
    const VectorXT& X = param.right();
    PDE::perturbedGradFluxViscous(param.left(), X[0], X[1], q, gradq[0], graddq[0], dF[0]);
  }

  //--------
  // viscous flux: Fv(X, Q, QX)
  template <class Tk, class Tgu, class Tf>
  void perturbedGradFluxViscous( const VectorXT& X,
                    const TensorMatrixQ<Tk>& K, const VectorArrayQ<Tgu>& graddu, VectorArrayQ<Tf>& dF ) const
  {
    PDE::perturbedGradFluxViscous(X[0], X[1], K(0,0), graddu[0], dF[0]);
  }

  // viscous flux: Fv(..., X, Q, QX)
  template <class L, class Tk, class Tgu, class Tf>
  void perturbedGradFluxViscous( const ParamTuple<L, VectorXT, TupleClass<>>& param,
                                 const TensorMatrixQ<Tk>& K, const VectorArrayQ<Tgu>& graddu, VectorArrayQ<Tf>& dF ) const
  {
    const VectorXT& X = param.right();
    PDE::perturbedGradFluxViscous(param.left(), X[0], X[1], K(0,0), graddu[0], dF[0]);
  }


  //--------
  // viscous diffusion matrix: d(Fv)/d(UX), where Fv(X, Q, QX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscous( const VectorXT& X,
                         const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq,
                         TensorMatrixQ<Tk>& K ) const
  {
    PDE::diffusionViscousSpaceTime(X[0], X[1],
                                   q, gradq[0], gradq[1],
                                   K(0,0), K(0,1),
                                   K(1,0), K(1,1) );
  }

  // viscous diffusion matrix: d(Fv)/d(UX), where Fv(..., X, Q, QX)
  template <class L, class Tq, class Tg, class Tk>
  void diffusionViscous( const ParamTuple<L, VectorXT, TupleClass<>>& param,
                         const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq,
                         TensorMatrixQ<Tk>& K ) const
  {
    const VectorXT& X = param.right();
    PDE::diffusionViscousSpaceTime(param.left(), X[0], X[1],
                                   q, gradq[0], gradq[1],
                                   K(0,0), K(0,1),
                                   K(1,0), K(1,1) );
  }

  //--------
  // gradient of viscous diffusion matrix: del dot d(Fv)/d(UX), where Fv(X, Q, QX)
  // GradK = [ kxx_x, kxy_x
  //           kyx_y, kyy_y];

  template <class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient( const VectorXT& X,
                                 const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq,
                                 const TensorSymArrayQ<Th>& hessq,
                                 VectorTensorMatrixQ<Tk>& GradK ) const
  {
    PDE::diffusionViscousGradient(X[0], X[1], q, gradq[0], hessq(0,0), GradK[0](0,0) );
  }

  // viscous diffusion matrix: d(Fv)/d(UX), where Fv(..., X, Q, QX)
  template <class L, class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient( const ParamTuple<L, VectorXT, TupleClass<>>& param,
                                 const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq,
                                 const TensorSymArrayQ<Th>& hessq,
                                 VectorTensorMatrixQ<Tk>& GradK ) const
  {
    const VectorXT& X = param.right();
    PDE::diffusionViscousGradient(param.left(), X[0], X[1],
                                  q, gradq[0], hessq(0,0), GradK[0](0,0) );
  }


  //--------
  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U), where Fv(X, Q, QX)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscous( const VectorXT& X,
                            const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, VectorMatrixQ<Tf>& a ) const
  {
    PDE::jacobianFluxViscousSpaceTime(X[0], X[1], q, gradq[0], gradq[1], a[0], a[1]);
  }

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U), where Fv(..., X, Q, QX)
  template <class L, class Tq, class Tg, class Tf>
  void jacobianFluxViscous( const ParamTuple<L, VectorXT, TupleClass<>>& param,
                            const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, VectorMatrixQ<Tf>& a ) const
  {
    const VectorXT& X = param.right();
    PDE::jacobianFluxViscousSpaceTime(param.left(), X[0], X[1], q, gradq[0], gradq[1], a[0], a[1]);
  }


  //--------
  // strong form viscous fluxes: div.(Fv), where Fv(X, Q, QX)
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscous( const VectorXT& X,
                          const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq,
                          const TensorSymArrayQ<Th>& hessq, ArrayQ<Tf>& strongPDE ) const
  {
    PDE::strongFluxViscousSpaceTime(X[0], X[1], q, gradq[0], gradq[1],
                                    hessq(0,0), hessq(1,0), hessq(1,1), strongPDE);
  }

  // strong form viscous fluxes: div.(Fv), where Fv(..., X, Q, QX)
  template <class L, class Tq, class Tg, class Th, class Tf>
  void strongFluxViscous( const ParamTuple<L, VectorXT, TupleClass<>>& param,
                          const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq,
                          const TensorSymArrayQ<Th>& hessq, ArrayQ<Tf>& strongPDE ) const
  {
    const VectorXT& X = param.right();
    PDE::strongFluxViscousSpaceTime(param.left(), X[0], X[1], q, gradq[0], gradq[1],
                                    hessq(0,0), hessq(1,0), hessq(1,1), strongPDE);
  }


  //--------
  // solution-dependent source: S(X, Q, QX)
  template <class Tq, class Tg, class Ts>
  void source( const VectorXT& X,
               const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq,
               ArrayQ<Ts>& source ) const
  {
    PDE::source(X[0], X[1], q, gradq[0], source);
  }

  // solution-dependent source: S(..., X, Q, QX)
  template <class L, class Tq, class Tg, class Ts>
  void source( const ParamTuple<L, VectorXT, TupleClass<>>& param,
               const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq,
               ArrayQ<Ts>& source ) const
  {
    const VectorXT& X = param.right();
    PDE::source(param.left(), X[0], X[1], q, gradq[0], source);
  }

  // solution-dependent source: S(X, Q, QX)
  template <class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void source( const VectorXT& X,
               const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
               const VectorArrayQ<Tg>& gradq, const VectorArrayQ<Tgp>& gradqp,
               ArrayQ<Ts>& source ) const
  {
    PDE::source(X[0], X[1], q, qp, gradq[0], gradqp[0], source);
  }

  // solution-dependent source: S(..., X, Q, QX)
  template <class L, class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void source( const ParamTuple<L, VectorXT, TupleClass<>>& param,
               const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
               const VectorArrayQ<Tg>& gradq, const VectorArrayQ<Tgp>& gradqp,
               ArrayQ<Ts>& source ) const
  {
    const VectorXT& X = param.right();
    PDE::source(param.left(), X[0], X[1], q, qp, gradq[0], gradqp[0], source);
  }


  // solution-dependent source: S(X, Q, QX)
  template <class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void sourceCoarse( const VectorXT& X,
                     const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
                     const VectorArrayQ<Tg>& gradq, const VectorArrayQ<Tgp>& gradqp,
                     ArrayQ<Ts>& source ) const
  {
    PDE::sourceCoarse(X[0], X[1], q, qp, gradq[0], gradqp[0], source);
  }

  // solution-dependent source: S(..., X, Q, QX)
  template <class L, class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void sourceCoarse( const ParamTuple<L, VectorXT, TupleClass<>>& param,
                     const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
                     const VectorArrayQ<Tg>& gradq, const VectorArrayQ<Tgp>& gradqp,
                     ArrayQ<Ts>& source ) const
  {
    const VectorXT& X = param.right();
    PDE::sourceCoarse(param.left(), X[0], X[1], q, qp, gradq[0], gradqp[0], source);
  }


  // solution-dependent source: S(X, Q, QX)
  template <class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void sourceFine( const VectorXT& X,
                   const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
                   const VectorArrayQ<Tg>& gradq, const VectorArrayQ<Tgp>& gradqp,
                   ArrayQ<Ts>& source ) const
  {
    PDE::sourceFine(X[0], X[1], q, qp, gradq[0], gradqp[0], source);
  }

  // solution-dependent source: S(..., X, Q, QX)
  template <class L, class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void sourceFine( const ParamTuple<L, VectorXT, TupleClass<>>& param,
                   const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
                   const VectorArrayQ<Tg>& gradq, const VectorArrayQ<Tgp>& gradqp,
                   ArrayQ<Ts>& source ) const
  {
    const VectorXT& X = param.right();
    PDE::sourceFine(param.left(), X[0], X[1], q, qp, gradq[0], gradqp[0], source);
  }

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX)
  template <class Tlq, class Tq, class Tg, class Ts>
  void source( const VectorXT& X,
               const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq,
               ArrayQ<Ts>& source ) const
  {
    PDE::source(X[0], X[1], lifted_quantity, q, gradq[0], source);
  }

  // solution-dependent source with lifted quantity: S(..., X, LQ, Q, QX)
  template <class L, class Tlq, class Tq, class Tg, class Ts>
  void source( const ParamTuple<L, VectorXT, TupleClass<>>& param,
               const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq,
               ArrayQ<Ts>& source ) const
  {
    const VectorXT& X = param.right();
    PDE::source(param.left(), X[0], X[1], lifted_quantity, q, gradq[0], source);
  }


  //--------
  // dual-consistent source: SL(X, QL, QR, QLX, QRX), SR(X, QL, QR, QLX, QRX)
  template <class Tq, class Tg, class Ts>
  void sourceTrace( const VectorXT& XL, const VectorXT& XR,
                    const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqL,
                    const ArrayQ<Tq>& qR, const VectorArrayQ<Tg>& gradqR,
                    ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
  {
    PDE::sourceTrace(XL[0],
                     XR[0], XL[1],
                     qL, gradqL[0],
                     qR, gradqR[0],
                     sourceL, sourceR);
  }

  // dual-consistent source: SL(..., X, QL, QR, QLX, QRX), SR(..., X, QL, QR, QLX, QRX)
  template <class L, class Tq, class Tg, class Ts>
  void sourceTrace( const ParamTuple<L, VectorXT, TupleClass<>>& paramL,
                    const ParamTuple<L, VectorXT, TupleClass<>>& paramR,
                    const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqL,
                    const ArrayQ<Tq>& qR, const VectorArrayQ<Tg>& gradqR,
                    ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
  {
    const VectorXT& XL = paramL.right();
    const VectorXT& XR = paramR.right();
    PDE::sourceTrace(paramL.left(), XL[0],
                     paramR.left(), XR[0], XL[1],
                     qL, gradqL[0],
                     qR, gradqR[0],
                     sourceL, sourceR);
  }

  //--------
  // lifted quantity for source: SL(X, QL, QR), SR(X, QL, QR)
  template <class Tq, class Ts>
  void sourceLiftedQuantity( const VectorXT& XL, const VectorXT& XR,
                             const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
                             Ts& s ) const
  {
    PDE::sourceLiftedQuantity(XL[0], XR[0], XL[1], qL, qR, s);
  }

  // lifted quantity for source: SL(..., X, QL, QR), SR(..., X, QL, QR)
  template <class L, class Tq, class Ts>
  void sourceLiftedQuantity( const ParamTuple<L, VectorXT, TupleClass<>>& paramL,
                             const ParamTuple<L, VectorXT, TupleClass<>>& paramR,
                             const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
                             Ts& s ) const
  {
    const VectorXT& XL = paramL.right();
    const VectorXT& XR = paramR.right();
    PDE::sourceLiftedQuantity(paramL.left(), XL[0],
                              paramR.left(), XR[0], XL[1],
                              qL, qR, s);
  }

  //--------
  // jacobian of source wrt conservation variables: d(S)/d(U), where S(X, Q, QX)
  template <class Tq, class Tg, class Tf>
  void jacobianSource( const VectorXT& X,
                       const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, MatrixQ<Tf>& dsdu ) const
  {
    PDE::jacobianSource(X[0], X[1], q, gradq[0], dsdu);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U), where S(..., X, Q, QX)
  template <class L, class Tq, class Tg, class Tf>
  void jacobianSource( const ParamTuple<L, VectorXT, TupleClass<>>& param,
                       const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, MatrixQ<Tf>& dsdu ) const
  {
    const VectorXT& X = param.right();
    PDE::jacobianSource(param.left(), X[0], X[1], q, gradq[0], dsdu);
  }

  //--------
  // jacobian of source wrt conservation variables: d(S)/d(U), where S(X, Q, QX)
  template <class Tq, class Tg, class Tf>
  void jacobianSourceHACK( const VectorXT& X,
                       const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, MatrixQ<Tf>& dsdu ) const
  {
    PDE::jacobianSourceHACK(X[0], X[1], q, gradq[0], dsdu);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U), where S(..., X, Q, QX)
  template <class L, class Tq, class Tg, class Tf>
  void jacobianSourceHACK( const ParamTuple<L, VectorXT, TupleClass<>>& param,
                       const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, MatrixQ<Tf>& dsdu ) const
  {
    const VectorXT& X = param.right();
    PDE::jacobianSourceHACK(param.left(), X[0], X[1], q, gradq[0], dsdu);
  }



  //--------
  // jacobian of source wrt conservation variables: d(S)/d(U), where S(X, Q, QX)
  template <class Tq, class Tg, class Tf>
  void jacobianSourceAbsoluteValue( const VectorXT& X,
                       const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, MatrixQ<Tf>& dsdu ) const
  {
    PDE::jacobianSourceAbsoluteValue(X[0], X[1], q, gradq[0], dsdu);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U), where S(..., X, Q, QX)
  template <class L, class Tq, class Tg, class Tf>
  void jacobianSourceAbsoluteValue( const ParamTuple<L, VectorXT, TupleClass<>>& param,
                       const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, MatrixQ<Tf>& dsdu ) const
  {
    const VectorXT& X = param.right();
    PDE::jacobianSourceAbsoluteValue(param.left(), X[0], X[1], q, gradq[0], dsdu);
  }



  //--------
  // jacobian of source wrt conservation variables: d(S)/d(U), where S(X, Q, QX)
  template <class Tq, class Tg, class Tf>
  void jacobianGradientSource( const VectorXT& X,
                       const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, VectorMatrixQ<Tf>& dsdgradu ) const
  {
    PDE::jacobianGradientSource(X[0], X[1], q, gradq[0], dsdgradu[0]);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U), where S(..., X, Q, QX)
  template <class L, class Tq, class Tg, class Tf>
  void jacobianGradientSource( const ParamTuple<L, VectorXT, TupleClass<>>& param,
                       const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, VectorMatrixQ<Tf>& dsdgradu ) const
  {
    const VectorXT& X = param.right();
    PDE::jacobianGradientSource( X[0], X[1], q, gradq[0], dsdgradu[0]);
  }


  //--------
  // jacobian of source wrt conservation variables: d(S)/d(U), where S(X, Q, QX)
  template <class Tq, class Tg, class Tf>
  void jacobianGradientSourceHACK( const VectorXT& X,
                       const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, VectorMatrixQ<Tf>& dsdgradu ) const
  {
    PDE::jacobianGradientSourceHACK(X[0], X[1], q, gradq[0], dsdgradu[0]);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U), where S(..., X, Q, QX)
  template <class L, class Tq, class Tg, class Tf>
  void jacobianGradientSourceHACK( const ParamTuple<L, VectorXT, TupleClass<>>& param,
                       const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, VectorMatrixQ<Tf>& dsdgradu ) const
  {
    const VectorXT& X = param.right();
    PDE::jacobianGradientSourceHACK( X[0], X[1], q, gradq[0], dsdgradu[0]);
  }

  //--------
  // jacobian of source wrt conservation variable gradient: d(S)/d(Ux), d(s)/d(Uy), where S(X, Q, QX)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSourceAbsoluteValue( const VectorXT& X, const VectorXT& N,
                       const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, MatrixQ<Ts>& divSdotN ) const
  {
    PDE::jacobianGradientSourceAbsoluteValue(X[0], X[1], N[0], q, gradq[0], divSdotN);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U), where S(..., X, Q, QX)
  template <class L, class Tq, class Tg, class Ts>
  void jacobianGradientSourceAbsoluteValue( const ParamTuple<L, VectorXT, TupleClass<>>& param, const VectorXT& N,
                       const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, MatrixQ<Ts>& divSdotN ) const
  {
    const VectorXT& X = param.right();
    PDE::jacobianGradientSourceAbsoluteValue(param.left(), X[0], X[1], N[0], q, gradq[0], divSdotN);
  }


  //--------
  // gradient jacobian of source wrt conservation variable gradient: grad.d(S)/d(Ux), d(s)/d(Uy), where S(X, Q, QX)
  template <class Tq, class Tg, class Th, class Tf>
  void jacobianGradientSourceGradient( const VectorXT& X,
                          const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq,
                          const TensorSymArrayQ<Th>& hessq, MatrixQ<Tf>& grad_dsdgradu ) const
  {
    PDE::jacobianGradientSourceGradient(X[0], X[1], q, gradq[0],
                           hessq(0,0), grad_dsdgradu);
  }

  template <class L, class Tq, class Tg, class Th, class Tf>
  void jacobianGradientSourceGradient( const ParamTuple<L, VectorXT, TupleClass<>>& param,
                          const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq,
                          const TensorSymArrayQ<Th>& hessq, MatrixQ<Tf>& grad_dsdgradu ) const
  {
    const VectorXT& X = param.right();
    PDE::jacobianGradientSourceGradient(param.left(), X[0], X[1], q, gradq[0],
                           hessq(0,0), grad_dsdgradu);
  }

  template <class Tq, class Tg, class Th, class Tf>
  void jacobianGradientSourceGradientHACK( const VectorXT& X,
                                           const ArrayQ<Tq> &q,
                                           const VectorArrayQ<Tg> &gradq,
                                           const TensorSymArrayQ<Th> &hessq,
                                           MatrixQ<Tf> &grad_dsdgradu ) const
  {
    SANS_DEVELOPER_EXCEPTION("not implemented... just a placeholder.");
  }

  template <class L, class Tq, class Tg, class Th, class Tf>
  void jacobianGradientSourceGradientHACK( const ParamTuple<L, VectorXT, TupleClass<>> &param,
                                           const VectorXT& X,
                                           const ArrayQ<Tq> &q,
                                           const VectorArrayQ<Tg> &gradq,
                                           const TensorSymArrayQ<Th> &hessq,
                                           MatrixQ<Tf> &grad_dsdgradu ) const
  {
    SANS_DEVELOPER_EXCEPTION("not implemented... just a placeholder.");
  }

  //--------
  // right-hand-side forcing function: RHS(X)
  template <class T>
  void forcingFunction( const VectorXT& X,
                        ArrayQ<T>& source ) const
  {
    PDE::forcingFunction(X[0], X[1], source);
  }

  // right-hand-side forcing function: RHS(..., X)
  template <class L, class T>
  void forcingFunction( const ParamTuple<L, VectorXT, TupleClass<>>& param,
                        ArrayQ<T>& source ) const
  {
    const VectorXT& X = param.right();
    PDE::forcingFunction(param.left(), X[0], X[1], source);
  }


  //--------
  // characteristic speed (needed for timestep): c(X, DX, Q)
  template <class T>
  void speedCharacteristic( const VectorXT& X,
                            const VectorXT& DX, const ArrayQ<T>& q, Real& speed ) const
  {
    PDE::speedCharacteristic(X[0], X[1], DX[0], q, speed);
  }

  // characteristic speed (needed for timestep): c(..., X, DX, Q)
  template <class L, class T>
  void speedCharacteristic( const ParamTuple<L, VectorXT, TupleClass<>>& param,
                            const VectorXT& DX, const ArrayQ<T>& q, Real& speed ) const
  {
    const VectorXT& X = param.right();
    PDE::speedCharacteristic(param.left(), X[0], X[1], DX[0], q, speed);
  }


  //--------
  // characteristic speed: c(X, Q)
  template <class T>
  void speedCharacteristic( const VectorXT& X,const ArrayQ<T>& q, T& speed ) const
  {
    PDE::speedCharacteristic(X[0], X[1], q, speed);
  }

  // characteristic speed (needed for timestep): c(..., X, Q)
  template <class L, class T>
  void speedCharacteristic( const ParamTuple<L, VectorXT, TupleClass<>>& param,
                            const ArrayQ<T>& q, T& speed ) const
  {
    const VectorXT& X = param.right();
    PDE::speedCharacteristic(param.left(), X[0], X[1], q, speed);
  }


  //--------
  // update fraction needed for physically valid state
  template <class T>
  void updateFraction( const VectorXT& X,
                       const ArrayQ<T>& q, const ArrayQ<T>& dq,
                       const Real maxChangeFraction, Real& updateFraction ) const
  {
    PDE::updateFraction(X[0], X[1], q, dq, maxChangeFraction, updateFraction);
  }

  // update fraction needed for physically valid state
  template <class L, class T>
  void updateFraction( const ParamTuple<L, VectorXT, TupleClass<>>& param,
                       const ArrayQ<T>& q, const ArrayQ<T>& dq,
                       const Real maxChangeFraction, Real& updateFraction ) const
  {
    const VectorXT& X = param.right();
    PDE::updateFraction(param.left(), X[0], X[1], q, dq, maxChangeFraction, updateFraction);
  }


  //--------
  using PDE::isValidState;        // is state physically valid
  using PDE::setDOFFrom; // set from primitive variable array
  using PDE::dump;
};

} //namespace SANS

#endif  // PDENDCONVERTSPACETIME1D_H
