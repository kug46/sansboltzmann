// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCSOURCEONLY2D_H
#define BCSOURCEONLY2D_H

// 2-D SourceOnly BC class

//PyDict must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/noncopyable.h"

#include "Topology/Dimension.h"

#include "pde/BCCategory.h"
#include "pde/BCNone.h"

#include <iostream>

#include <boost/mpl/vector.hpp>

namespace SANS
{

//----------------------------------------------------------------------------//
// BC types
//----------------------------------------------------------------------------//

// Define the vector of boundary conditions
template<class PDE>
using BCSourceOnly2DVector = boost::mpl::vector1< BCNone<PhysD2, PDE::N> >;

} //namespace SANS

#endif  // BCSOURCEONLY2D_H
