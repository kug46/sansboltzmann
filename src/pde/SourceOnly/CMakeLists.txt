
INCLUDE( ${CMAKE_SOURCE_DIR}/CMakeInclude/ForceOutOfSource.cmake ) #This must be the first thing included

SET( SOURCEONLY_SRC

	#...
   )

#ADD_LIBRARY( SourceOnlyLib STATIC ${SOURCEONLY} )

# This should be SourceOnlyLib and replaced with the library if cpp files are added
ADD_CUSTOM_TARGET( SourceOnlyLib ) 

#Create the vera targets for this library
ADD_VERA_CHECKS_RECURSE( SourceOnlyLib *.h *.cpp )
ADD_HEADER_COMPILE_CHECK_RECURSE( SourceOnlyLib *.h )
#ADD_CPPCHECK( SourceOnlyLib ${SOURCEONLY_SRC} )
