// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDESOURCEONLY2D_H
#define PDESOURCEONLY2D_H

// PDE class

//PyDict must be included first
#include "Python/PyDict.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "Topology/Dimension.h"

#include "LinearAlgebra/DenseLinAlg/tools/PromoteSurreal.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Exp.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "Field/Tuple/ParamTuple.h"
#include "tools/Tuple.h"
#include "tools/smoothmath.h"

#include <iostream>
#include <limits>
#include <string>


namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: PDE to generate initial function for Homotopy
//
// template parameters:
//   T                           solution DOF data type (e.g. double)
//   PDETraitsSize               define PDE size-related features
//     N, D                      PDE size, physical dimensions
//     ArrayQ                    solution/residual arrays
//     MatrixQ                   matrices (e.g. flux jacobian)
//   PDETraitsModel              define PDE model-related features
//     QType                     solution variable set (e.g. primitive)
//     QInterpret                solution variable interpreter
//     GasModel                  gas model (e.g. ideal gas law)
//
// member functions:
//   .hasFluxAdvectiveTime        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: U(Q)
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .isValidState               T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom                 set from primitive variable array
//----------------------------------------------------------------------------//

template <class PDEBase>
class PDESourceOnly2D : public PDEBase
{
public:
  typedef PhysD2 PhysDim;

  typedef PDEBase BaseType;

  using BaseType::D;               // physical dimensions
  using BaseType::N;               // total solution variables

  static const int Nparam = 1;               // total solution parameters

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using ArrayQ = typename BaseType::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using MatrixQ = typename BaseType::template MatrixQ<T>;   // matrices

  template <class TP>
  using ArrayParam = DLA::VectorS<Nparam,TP>;  // parameter array type

  template <class T>
  using MatrixParam = DLA::MatrixS<N,Nparam,T>;         // e.g. jacobian of PDEs w.r.t. parameters

  // Constructor forwards arguments to PDE class using varargs
  template< class... PDEArgs >
  PDESourceOnly2D(const ArrayQ<Real>& a, PDEArgs&&... args) :
    BaseType(std::forward<PDEArgs>(args)...),
    a_(a)
  {
    SANS_ASSERT_MSG(BaseType::isValidState(a_), "State passed to PDESourceOnly2D() is not valid");
  }

  ~PDESourceOnly2D() {}

  PDESourceOnly2D& operator=( const PDESourceOnly2D& ) = delete;

  // flux components
  bool hasFluxAdvectiveTime() const { return true; }
  bool hasFluxAdvective() const { return BaseType::hasFluxAdvective(); }
  bool hasFluxViscous() const { return BaseType::hasFluxViscous(); }
  bool hasSource() const { return true; }
  bool hasSourceTrace() const { return BaseType::hasSourceTrace(); }
  bool hasForcingFunction() const { return BaseType::hasForcingFunction(); }

  bool fluxViscousLinearInGradient() const { return BaseType::fluxViscousLinearInGradient(); }
  bool needsSolutionGradientforSource() const
  {
    return BaseType::needsSolutionGradientforSource();
  }

  // unsteady temporal flux: Ft(Q)
  template <int Dim, class T, class Tf>
  void fluxAdvectiveTime(
      const DLA::MatrixSymS<Dim,Real>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tf>& ft ) const
  {
    BaseType::fluxAdvectiveTime(param, x, y, time, q, ft);
  }

  // unsteady temporal flux: Ft(Q)
  template <int Dim, class T, class Tf>
  void fluxAdvectiveTime(
      const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tf>& ft ) const
  {
    fluxAdvectiveTime(param.left(), x, y, time, q, ft);
  }

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template<class T, class Tp, class Tf>
  void jacobianFluxAdvectiveTime(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& J ) const
  {
    BaseType::jacobianFluxAdvectiveTime(param, x, y, time, q, J);
  }

  // master state: U(Q)
  template <class T, class Tp, class Tu>
  void masterState(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tu>& uCons ) const;

  // master state Gradient: Ux(Q)
  template<class Tp, class Tq, class Tg, class Tf>
  void masterStateGradient(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& Ux, ArrayQ<Tf>& Uy  ) const
  {
    BaseType::masterStateGradient(param, x, y, time, q, qx, qy, Ux, Uy);
  }

  // master state Hessian: Uxx(Q), Uxy(Q), Uyy(Q)
  template<class Tp, class Tq, class Tg, class Th, class Tf>
  void masterStateHessian(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      ArrayQ<Tf>& Uxx, ArrayQ<Tf>& Uxy, ArrayQ<Tf>& Uyy ) const
  {
    BaseType::masterStateHessian(param, x, y, time, q, qx, qy, qxx, qxy, qyy, Uxx, Uxy, Uyy);
  }

  template<class Tp, class Tq, class Tqp, class Tf>
  void perturbedMasterState(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& dq, ArrayQ<Tf>& dU ) const
  {
    BaseType::perturbedMasterState(param, x, y, time, q, dq, dU);
  }

  // jacobian of master state wrt q: dU(Q)/dQ
  template<class T, class Tf>
  void jacobianMasterState(
      const DLA::MatrixSymS<D, Real>& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& dudq ) const;

  // jacobian of master state wrt q: dU(Q)/dQ
  template<class T, class Tp, class Tf>
  void jacobianMasterState(
      const ParamTuple<DLA::MatrixSymS<D, Real>, Tp, TupleClass<> >& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& dudq ) const;

  // jacobian of master state wrt q: dU(Q)/dQ
  template<class T, class Tf>
  void jacobianMasterState(
      const DLA::MatrixSymS<D+1, Real>& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& dudq ) const;

  // jacobian of master state wrt q: dU(Q)/dQ
  template<class T, class Tp, class Tf>
  void jacobianMasterState(
      const ParamTuple<DLA::MatrixSymS<D+1, Real>, Tp, TupleClass<> >& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& dudq ) const;

  // unsteady conservative flux: d(U)/d(t)
  template <int Dim, class T, class Tf>
  void strongFluxAdvectiveTime(
      const DLA::MatrixSymS<Dim, Real>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<Tf>& dudt ) const;

  // unsteady conservative flux: d(U)/d(t)
  template <int Dim, class T, class Tp, class Tf>
  void strongFluxAdvectiveTime(
      const ParamTuple<DLA::MatrixSymS<Dim, Real>, Tp, TupleClass<> >& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<Tf>& dudt ) const;

  // advective flux: F(Q)
  template <class T, class Tp, class Ts>
  void fluxAdvective(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Ts>& f, ArrayQ<Ts>& g ) const;

  template <class T, class Tp, class Ts>
  void fluxAdvectiveUpwind(
      const Tp& param, const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, ArrayQ<Ts>& f, const Real& scaleFactor = 1 ) const;

  template <class T, class Tp, class Tf>
  void fluxAdvectiveUpwindSpaceTime(
      const Tp& param, const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, const Real& nt, ArrayQ<Tf>& f ) const;

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class Tp, class Tq, class Ta>
  void jacobianFluxAdvective(
      const Tp& param, const Real& x, const Real& y,  const Real& time, const ArrayQ<Tq>& q,
      MatrixQ<Ta>& ax, MatrixQ<Ta>& ay ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tp, class T, class Ta>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const Real& nx, const Real& ny,
      MatrixQ<Ta>& a ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <int Dim, class Tp, class T, class Ta>
  void jacobianFluxAdvectiveAbsoluteValue(
      const ParamTuple<DLA::MatrixSymS<Dim, Real>, Tp, TupleClass<> >& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const Real& nx, const Real& ny,
      MatrixQ<Ta>& a ) const
  {
    // Nothing
  }

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tp, class Tq, class Ta>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Tp& param, const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q,
      const Real& nx, const Real& ny, const Real& nt,
      MatrixQ<Ta>& a ) const;

  // strong form advective fluxes
  template <class Tp, class Tq, class Tg, class Tf>
  void strongFluxAdvective(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& strongPDE ) const;

  // viscous flux: Fv(Q, Qx)
  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscous(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& fy ) const;

  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscous(
      const Tp& paramL, const Tp& paramR, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& fn ) const;

  // perturbation to viscous flux from dux, duy
  template <class Tp, class Tq, class Tg, class Tgu, class Tf>
  void perturbedGradFluxViscous(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Tgu>& dqx, const ArrayQ<Tgu>& dqy,
      ArrayQ<Tf>& df, ArrayQ<Tf>& dg ) const;


  // perturbation to viscous flux from dux, duy
  template <class Tp, class Tk, class Tgu, class Tf>
  void perturbedGradFluxViscous(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const MatrixQ<Tk>& Kxx, const MatrixQ<Tk>& Kxy, const MatrixQ<Tk>& Kyx, const MatrixQ<Tk>& Kyy,
      const ArrayQ<Tgu>& dux, const ArrayQ<Tgu>& duy,
      ArrayQ<Tf>& df, ArrayQ<Tf>& dg ) const;

  // viscous diffusion matrix: d(Fv)/d(Ux)
  template <class Tp, class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy) const;

  // viscous diffusion matrix: d(Fv)/d(Ux)
  template <class Tp, class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kyx_x,
      MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y) const;

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tp, class Tq, class Tg, class Tf>
  void jacobianFluxViscous(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu ) const;

  // strong form viscous fluxes
  template <class Tp, class Tq, class Tg,class Th, class Tf>
  void strongFluxViscous(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      ArrayQ<Tf>& strongPDE ) const;

  // space-time viscous flux: Fv(Q, QX)
  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& ft ) const;

  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Tp& paramL, const Tp& paramR, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qtL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qtR,
      const Real& nx, const Real& ny, const Real& nt,
      ArrayQ<Tf>& f ) const;

  // space-time viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tp, class Tq, class Tg, class Tk>
  void diffusionViscousSpaceTime(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxt,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyt,
      MatrixQ<Tk>& ktx, MatrixQ<Tk>& kty, MatrixQ<Tk>& ktt ) const;

  // space-time jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tp, class Tq, class Tg, class Tf>
  void jacobianFluxViscousSpaceTime(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& at ) const;

  // space-time strong form viscous fluxes: -d(Fv)/dx - d(Fv)/d(y)
  template <class Tp, class Tq, class Tg, class Th, class Tf>
  void strongFluxViscousSpaceTime(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxt, const ArrayQ<Th>& qyt, const ArrayQ<Th>& qtt,
      ArrayQ<Tf>& strongPDE ) const;

  // solution-dependent source: S(X, Q, QX)
  template <class Tp, class Tq, class Tg, class Ts>
  void source(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Ts>& source ) const;

  // solution-dependent source: S(X, Q, QX)
  template <class Tp, class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void source(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy,
      ArrayQ<Ts>& source ) const;

  // solution-dependent source: S(X, Q, QX)
  template <class Tp, class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void sourceCoarse(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy,
      ArrayQ<Ts>& source ) const;

  // solution-dependent source: S(X, Q, QX)
  template <class Tp, class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void sourceFine(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy,
      ArrayQ<Ts>& source ) const;

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX)
  template <class Tp, class Tlq, class Tq, class Tg, class Ts>
  void source(
      const Tp& param, const Real& x, const Real& y, const Real& time, const Tlq& lifted_quantity,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Ts>& source ) const;

  // dual-consistent source
  template <class Tq, class Tp, class Tg, class Ts>
  void sourceTrace(
      const Tp& paramL, const Real& xL, const Real& yL,
      const Tp& paramR, const Real& xR, const Real& yR,
      const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const;

  // trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
  template <class Tp, class Tq, class Ts>
  void sourceLiftedQuantity(
      const Tp& paramL, const Real& xL, const Real& yL,
      const Tp& paramR, const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, Ts& s ) const;

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tp, class Tq, class Tg, class Ts>
  void jacobianSource(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const;

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tp, class Tq, class Tg, class Ts>
  void jacobianSourceHACK(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const;

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tp, class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const
  {
    SANS_DEVELOPER_EXCEPTION("PDESourceOnly2D::jacobianSourceAbsoluteValue - NOT IMPLEMENTED");
  }

  template <class Tp, class Tq, class Tg, class Ts>
    void jacobianGradientSource(
        const Tp& param, const Real& x, const Real& y, const Real& time,
        const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
        MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy ) const;

  template <class Tp, class Tq, class Tg, class Ts>
    void jacobianGradientSourceHACK(
        const Tp& param, const Real& x, const Real& y, const Real& time,
        const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
        MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy ) const;

  template <class Tp, class Tq, class Tg, class Th, class Ts>
    void jacobianGradientSourceGradient(
        const Tp& param, const Real& x, const Real& y, const Real& time,
        const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
        const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
        MatrixQ<Ts>& div_dsgradu ) const
  {
    SANS_DEVELOPER_EXCEPTION("PDESourceOnly2D::jacobianGradientSourceGradient - NOT IMPLEMENTED");
  }

  template <class Tp, class Tq, class Tg, class Th, class Ts>
    void jacobianGradientSourceGradientHACK(
        const Tp& param, const Real& x, const Real& y, const Real& time,
        const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
        const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
        MatrixQ<Ts>& div_dsgradu ) const
  {
    SANS_DEVELOPER_EXCEPTION("PDESourceOnly2D::jacobianGradientSourceGradientHACK - NOT IMPLEMENTED");
  }

  // right-hand-side forcing function
  template <class T, class Tp>
  void forcingFunction( const Tp& param,
                        const Real& x, const Real& y, const Real& time, ArrayQ<T>& source ) const;

  // characteristic speed (needed for timestep)
  template <class T, class Tp>
  void speedCharacteristic(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const Real& dx, const Real& dy, const ArrayQ<T>& q, T& speed ) const;

  // characteristic speed
  template <class T, class Tp>
  void speedCharacteristic(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, T& speed ) const;

  // characteristic speed
  template <class T>
  void speedCharacteristic(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, T& speed ) const;

  // update fraction needed for physically valid state
  template <class Tp>
  void updateFraction(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
      const Real maxChangeFraction, Real& updateFraction ) const
  {
    // Nothing
  }

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const;

  // set from primitive variable array
  template <class T>
  void setDOFFrom(
      ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const;

  // set from variable
  ArrayQ<Real> setDOFFrom( const PyDict& d ) const;

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const ArrayQ<Real> a_;
};


template <class PDEBase>
template <class T, class Tp, class Tu>
inline void
PDESourceOnly2D<PDEBase>::masterState(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, ArrayQ<Tu>& uCons ) const
{
  BaseType::masterState(param, x, y, time, q, uCons);
}

template <class PDEBase>
template<class T, class Tf>
inline void
PDESourceOnly2D<PDEBase>::
jacobianMasterState(
      const DLA::MatrixSymS<D, Real>& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& dudq ) const
{
  BaseType::jacobianMasterState(param, x, y, time, q, dudq);
}

template <class PDEBase>
template<class T, class Tp, class Tf>
inline void
PDESourceOnly2D<PDEBase>::
jacobianMasterState(
      const ParamTuple<DLA::MatrixSymS<D, Real>, Tp, TupleClass<> >& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& dudq ) const
{
  BaseType::jacobianMasterState(param, x, y, time, q, dudq);
}

template <class PDEBase>
template<class T, class Tf>
inline void
PDESourceOnly2D<PDEBase>::
jacobianMasterState(
      const DLA::MatrixSymS<D+1, Real>& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& dudq ) const
{
  BaseType::jacobianMasterState(param, x, y, time, q, dudq);
}

template <class PDEBase>
template<class T, class Tp, class Tf>
inline void
PDESourceOnly2D<PDEBase>::
jacobianMasterState(
      const ParamTuple<DLA::MatrixSymS<D+1, Real>, Tp, TupleClass<> >& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& dudq ) const
{
  BaseType::jacobianMasterState(param, x, y, time, q, dudq);
}


template <class PDEBase>
template <int Dim, class T, class Tf>
inline void
PDESourceOnly2D<PDEBase>::strongFluxAdvectiveTime(
    const DLA::MatrixSymS<Dim, Real>& param,
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<Tf>& dudt ) const
{
  // Nothing
}


template <class PDEBase>
template <int Dim, class T, class Tp, class Tf>
inline void
PDESourceOnly2D<PDEBase>::strongFluxAdvectiveTime(
    const ParamTuple<DLA::MatrixSymS<Dim, Real>, Tp, TupleClass<> >& param,
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<Tf>& dudt ) const
{
  // Nothing
}

template <class PDEBase>
template <class T, class Tp, class Ts>
inline void
PDESourceOnly2D<PDEBase>::fluxAdvective(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, ArrayQ<Ts>& fx, ArrayQ<Ts>& fy ) const
{
  // Nothing
}

template <class PDEBase>
template <class T, class Tp, class Ts>
inline void
PDESourceOnly2D<PDEBase>::fluxAdvectiveUpwind(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx, const Real& ny,
    ArrayQ<Ts>& f, const Real& scaleFactor ) const
{
  // Nothing
}

template <class PDEBase>
template <class T, class Tp, class Tf>
inline void
PDESourceOnly2D<PDEBase>::fluxAdvectiveUpwindSpaceTime(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx, const Real& ny, const Real& nt,
    ArrayQ<Tf>& f ) const
{
  // Nothing
}

// advective flux jacobian: d(F)/d(U)
template <class PDEBase>
template <class Tp, class Tq, class Ta>
inline void
PDESourceOnly2D<PDEBase>::jacobianFluxAdvective(
    const Tp& param, const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q,
    MatrixQ<Ta>& ax, MatrixQ<Ta>& ay ) const
{
  // Nothing
}

// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template <class PDEBase>
template <class Tp, class T, class Ta>
inline void
PDESourceOnly2D<PDEBase>::jacobianFluxAdvectiveAbsoluteValue(
  const Tp& param, const Real& x, const Real& y, const Real& time,
  const ArrayQ<T>& q, const Real& nx, const Real& ny, MatrixQ<Ta>& mtx ) const
{
  // Nothing
}

// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template <class PDEBase>
template <class Tp, class Tq, class Ta>
inline void
PDESourceOnly2D<PDEBase>::jacobianFluxAdvectiveAbsoluteValueSpaceTime(
  const Tp& param, const Real& x, const Real& y, const Real& time,
  const ArrayQ<Tq>& q, const Real& nx, const Real& ny, const Real& nt, MatrixQ<Ta>& mtx ) const
{
  // Nothing
}

// strong form of advective flux: d(F)/d(x)
template <class PDEBase>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDESourceOnly2D<PDEBase>::strongFluxAdvective(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Tf>& strongPDE ) const
{
  // Nothing
}

// viscous flux
template <class PDEBase>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDESourceOnly2D<PDEBase>::fluxViscous(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Tf>& fx, ArrayQ<Tf>& fy ) const
{
  // Nothing
}

// space-time viscous flux
template <class PDEBase>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDESourceOnly2D<PDEBase>::fluxViscousSpaceTime(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
    ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& ft ) const
{
  // Nothing
}

// viscous flux: normal flux with left and right states
template <class PDEBase>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDESourceOnly2D<PDEBase>::fluxViscous(
    const Tp& paramL, const Tp& paramR, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
    const Real& nx, const Real& ny, ArrayQ<Tf>& fn ) const
{
  // Nothing
}

// perturbation to viscous flux from dux, duy
template <class PDEBase>
template <class Tp, class Tq, class Tg, class Tgu, class Tf>
void
PDESourceOnly2D<PDEBase>::perturbedGradFluxViscous(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Tgu>& dqx, const ArrayQ<Tgu>& dqy,
    ArrayQ<Tf>& df, ArrayQ<Tf>& dg ) const
{
  // Nothing
}
// perturbation to viscous flux from dux, duy
template <class PDEBase>
template <class Tp, class Tk, class Tgu, class Tf>
void
PDESourceOnly2D<PDEBase>::perturbedGradFluxViscous(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const MatrixQ<Tk>& Kxx, const MatrixQ<Tk>& Kxy, const MatrixQ<Tk>& Kyx, const MatrixQ<Tk>& Kyy,
    const ArrayQ<Tgu>& dux, const ArrayQ<Tgu>& duy,
    ArrayQ<Tf>& df, ArrayQ<Tf>& dg ) const
{
  // Nothing
}

// space-time viscous flux: normal flux with left and right states
template <class PDEBase>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDESourceOnly2D<PDEBase>::fluxViscousSpaceTime(
    const Tp& paramL, const Tp& paramR, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qtL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qtR,
    const Real& nx, const Real& ny, const Real& nt, ArrayQ<Tf>& fn ) const
{
  // Nothing
}

// viscous diffusion matrix: d(Fv)/d(Ux)
template <class PDEBase>
template <class Tp, class Tq, class Tg, class Tk>
inline void
PDESourceOnly2D<PDEBase>::diffusionViscous(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const
{
  // Nothing
}


// viscous diffusion matrix: d(Fv)/d(Ux)
template <class PDEBase>
template <class Tp, class Tq, class Tg, class Th, class Tk>
inline void
PDESourceOnly2D<PDEBase>::diffusionViscousGradient(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kyx_x,
    MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y) const
{
  // Nothing
}

// space-time viscous diffusion matrix: d(Fv)/d(Ux)
template <class PDEBase>
template <class Tp, class Tq, class Tg, class Tk>
inline void
PDESourceOnly2D<PDEBase>::diffusionViscousSpaceTime(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
    MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxt,
    MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyt,
    MatrixQ<Tk>& ktx, MatrixQ<Tk>& kty, MatrixQ<Tk>& ktt) const
{
  // Nothing
}

// strong form of viscous flux: d(Fv)/d(x)
template <class PDEBase>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDESourceOnly2D<PDEBase>::
jacobianFluxViscous(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu ) const
{
  // Nothing
}

// strong form of viscous flux: d(Fv)/d(x)
template <class PDEBase>
template <class Tp, class Tq, class Tg, class Th, class Tf>
inline void
PDESourceOnly2D<PDEBase>::strongFluxViscous(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Th>& qxx,
    const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    ArrayQ<Tf>& strongPDE ) const
{
  // Nothing
}

// space-time jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
template <class PDEBase>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDESourceOnly2D<PDEBase>::
jacobianFluxViscousSpaceTime(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
    MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& at ) const
{
  // Nothing
}

// space-time strong form of viscous flux: d(Fv)/d(x)
template <class PDEBase>
template <class Tp, class Tq, class Tg, class Th, class Tf>
inline void
PDESourceOnly2D<PDEBase>::strongFluxViscousSpaceTime(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
    const ArrayQ<Th>& qxx,
    const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    const ArrayQ<Th>& qxt, const ArrayQ<Th>& qyt, const ArrayQ<Th>& qtt,
    ArrayQ<Tf>& strongPDE ) const
{
  // Nothing
}

// solution-dependent source: S(X, Q, QX)
template <class PDEBase>
template <class Tp, class Tq, class Tg, class Ts>
inline void
PDESourceOnly2D<PDEBase>::source(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Ts>& source ) const
{
  DLA::MatrixSymS<D,Real> H = exp(param); //generalized h-tensor

  Real tau = 1.0;
  typedef DLA::VectorS<D, Real> VectorX;
  VectorX hPrincipal;
  DLA::EigenValues(H, hPrincipal);

  //Get minimum length scale
  Real hmin = std::numeric_limits<Real>::max();
  for (int d = 0; d < D; d++)
    hmin = min(hmin, hPrincipal[d]);

  Real lambda = 0;
  BaseType::speedCharacteristic( param, x, y, time, a_, lambda );

  // characteristic time of the PDE
  tau = hmin/lambda;
  
//  ArrayQ<Tq> u = 0, ua = 0;
//  BaseType::masterState(param, x, y, time, q, u);
//  BaseType::masterState(param, x, y, time, a_, ua);
//
//  source += tau * (u - ua);

  MatrixQ<Real> dudq = 0.0;
  BaseType::jacobianMasterState(param, x, y, time, a_, dudq);

  ArrayQ<Tq> dq = q - a_;
  source +=  1.0 / tau * dudq * dq;
}

// solution-dependent source: S(X, Q, QX)
template <class PDEBase>
template <class Tp, class Tq, class Tqp, class Tg, class Tgp, class Ts>
inline void
PDESourceOnly2D<PDEBase>::source(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
    const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy,
    ArrayQ<Ts>& source ) const
{
  typedef typename promote_Surreal<Tq,Tqp>::type Tqq;
  DLA::MatrixSymS<D,Real> H = exp(param); //generalized h-tensor

  Real tau = 1.0;
  typedef DLA::VectorS<D, Real> VectorX;
  VectorX hPrincipal;
  DLA::EigenValues(H, hPrincipal);

  //Get minimum length scale
  Real hmin = std::numeric_limits<Real>::max();
  for (int d = 0; d < D; d++)
    hmin = min(hmin, hPrincipal[d]);

  Real lambda = 0;
  BaseType::speedCharacteristic( param, x, y, time, a_, lambda );

  // characteristic time of the PDE
  tau = hmin/lambda;

//  ArrayQ<Tq> u = 0, ua = 0;
//  BaseType::masterState(param, x, y, time, q, u);
//  BaseType::masterState(param, x, y, time, a_, ua);
//
//  source += tau * (u - ua);

//  ArrayQ<Tq> a = a_;
  MatrixQ<Real> dudq = 0.0;
  BaseType::jacobianMasterState(param, x, y, time, a_, dudq);

  ArrayQ<Tqq> dq = q - a_;
  source +=  1.0 / tau * dudq * dq;
}

// solution-dependent source: S(X, Q, QX)
template <class PDEBase>
template <class Tp, class Tq, class Tqp, class Tg, class Tgp, class Ts>
inline void
PDESourceOnly2D<PDEBase>::sourceCoarse(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
    const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy,
    ArrayQ<Ts>& source ) const
{
  typedef typename promote_Surreal<Tq,Tqp>::type Tqq;
  DLA::MatrixSymS<D,Real> H = exp(param); //generalized h-tensor

  Real tau = 1.0;
  typedef DLA::VectorS<D, Real> VectorX;
  VectorX hPrincipal;
  DLA::EigenValues(H, hPrincipal);

  //Get minimum length scale
  Real hmin = std::numeric_limits<Real>::max();
  for (int d = 0; d < D; d++)
    hmin = min(hmin, hPrincipal[d]);

  Real lambda = 0;
  BaseType::speedCharacteristic( param, x, y, time, a_, lambda );

  // characteristic time of the PDE
  tau = hmin/lambda;

//  ArrayQ<Tq> u = 0, ua = 0;
//  BaseType::masterState(param, x, y, time, q, u);
//  BaseType::masterState(param, x, y, time, a_, ua);
//
//  source += tau * (u - ua);

//  ArrayQ<Tq> a = a_;
  MatrixQ<Real> dudq = 0.0;
  BaseType::jacobianMasterState(param, x, y, time, a_, dudq);

  ArrayQ<Tqq> dq = q - a_;
  source +=  1.0 / tau * dudq * dq;
}

// solution-dependent source: S(X, Q, QX)
template <class PDEBase>
template <class Tp, class Tq, class Tqp, class Tg, class Tgp, class Ts>
inline void
PDESourceOnly2D<PDEBase>::sourceFine(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
    const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy,
    ArrayQ<Ts>& source ) const
{
  typedef typename promote_Surreal<Tq,Tqp>::type Tqq;
  DLA::MatrixSymS<D,Real> H = exp(param); //generalized h-tensor

  Real tau = 1.0;
  typedef DLA::VectorS<D, Real> VectorX;
  VectorX hPrincipal;
  DLA::EigenValues(H, hPrincipal);

  //Get minimum length scale
  Real hmin = std::numeric_limits<Real>::max();
  for (int d = 0; d < D; d++)
    hmin = min(hmin, hPrincipal[d]);

  Real lambda = 0;
  BaseType::speedCharacteristic( param, x, y, time, a_, lambda );

  // characteristic time of the PDE
  tau = hmin/lambda;

//  ArrayQ<Tq> u = 0, ua = 0;
//  BaseType::masterState(param, x, y, time, q, u);
//  BaseType::masterState(param, x, y, time, a_, ua);
//
//  source += tau * (u - ua);

//  ArrayQ<Tq> a = a_;
  MatrixQ<Real> dudq = 0.0;
  BaseType::jacobianMasterState(param, x, y, time, a_, dudq);

  ArrayQ<Tqq> dq = qp;
  source +=  1.0 / tau * dudq * dq;
}

// solution-dependent source with lifted_quantity: S(X, Q, QX)
template <class PDEBase>
template <class Tp, class Tlq, class Tq, class Tg, class Ts>
inline void
PDESourceOnly2D<PDEBase>::source(
    const Tp& param, const Real& x, const Real& y, const Real& time, const Tlq& lifted_quantity,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Ts>& source ) const
{
  // Nothing
  SANS_DEVELOPER_EXCEPTION("PDESourceOnly2D::source - NOT IMPLEMENTED");
}

// dual-consistent source
template <class PDEBase>
template <class Tq, class Tp, class Tg, class Ts>
inline void
PDESourceOnly2D<PDEBase>::sourceTrace(
    const Tp& paramL, const Real& xL, const Real& yL,
    const Tp& paramR, const Real& xR, const Real& yR,
    const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
    ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
{
  // Nothing
  SANS_DEVELOPER_EXCEPTION("PDESourceOnly2D::sourceTrace - NOT IMPLEMENTED");
}

// trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
template <class PDEBase>
template <class Tp, class Tq, class Ts>
inline void
PDESourceOnly2D<PDEBase>::sourceLiftedQuantity(
    const Tp& paramL, const Real& xL, const Real& yL,
    const Tp& paramR, const Real& xR, const Real& yR,
    const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
    Ts& s ) const
{
  // Nothing
  SANS_DEVELOPER_EXCEPTION("PDESourceOnly2D::sourceLiftedQuantity - NOT IMPLEMENTED");
}

// jacobian of source wrt conservation variables: d(S)/d(U)
template <class PDEBase>
template <class Tp, class Tq, class Tg, class Ts>
inline void
PDESourceOnly2D<PDEBase>::jacobianSource(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Ts>& dsdu ) const
{
  DLA::MatrixSymS<D,Real> H = exp(param); //generalized h-tensor

  Real tau = 1.0;
  typedef DLA::VectorS<D, Real> VectorX;
  VectorX hPrincipal;
  DLA::EigenValues(H, hPrincipal);

  //Get minimum length scale
  Real hmin = std::numeric_limits<Real>::max();
  for (int d = 0; d < D; d++)
    hmin = min(hmin, hPrincipal[d]);

  Real lambda = 0;
  BaseType::speedCharacteristic( param, x, y, time, a_, lambda );

  // characteristic time of the PDE
  tau = hmin/lambda;

//  ArrayQ<Tq> a = a_;
  MatrixQ<Real> dudq = 0.0;
  BaseType::jacobianMasterState(param, x, y, time, a_, dudq);
  dsdu = 1.0 / tau * dudq;
}


// jacobian of source wrt conservation variables: d(S)/d(U)
template <class PDEBase>
template <class Tp, class Tq, class Tg, class Ts>
inline void
PDESourceOnly2D<PDEBase>::jacobianSourceHACK(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Ts>& dsdu ) const
{
  SANS_DEVELOPER_EXCEPTION("PDESourceOnly2D::jacobianSourceHACK - NOT IMPLEMENTED");
}

// jacobian of source wrt conservation variables: d(S)/d(U)
template <class PDEBase>
template <class Tp, class Tq, class Tg, class Ts>
inline void
PDESourceOnly2D<PDEBase>::jacobianGradientSource(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy ) const
{
  // Nothing
  SANS_DEVELOPER_EXCEPTION("PDESourceOnly2D::jacobianGradientSource - NOT IMPLEMENTED");
}

// jacobian of source wrt conservation variables: d(S)/d(U)
template <class PDEBase>
template <class Tp, class Tq, class Tg, class Ts>
inline void
PDESourceOnly2D<PDEBase>::jacobianGradientSourceHACK(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy ) const
{
  // Nothing
  SANS_DEVELOPER_EXCEPTION("PDESourceOnly2D::jacobianGradientSourceHACK - NOT IMPLEMENTED");
}


// right-hand-side forcing function
template <class PDEBase>
template <class T, class Tp>
inline void
PDESourceOnly2D<PDEBase>::forcingFunction(
    const Tp& param, const Real& x, const Real& y, const Real& time, ArrayQ<T>& forcing ) const
{
  // Nothing
}


// characteristic speed (needed for timestep)
template <class PDEBase>
template <class T, class Tp>
inline void
PDESourceOnly2D<PDEBase>::speedCharacteristic(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const Real& dx, const Real& dy, const ArrayQ<T>& q, T& speed ) const
{
  // Nothing
}


// characteristic speed
template <class PDEBase>
template <class T, class Tp>
inline void
PDESourceOnly2D<PDEBase>::speedCharacteristic(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, T& speed ) const
{
  // Nothing
}


// is state physically valid
template <class PDEBase>
inline bool
PDESourceOnly2D<PDEBase>::isValidState( const ArrayQ<Real>& q ) const
{
  return BaseType::isValidState(q);
}


// set from primitive variable array
template <class PDEBase>
template <class T>
void
PDESourceOnly2D<PDEBase>::setDOFFrom(
    ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const
{
  BaseType::setDOFFrom(q, data, name, nn);
}

// set from primitive variable
template <class PDEBase>
inline typename PDESourceOnly2D<PDEBase>::template ArrayQ<Real>
PDESourceOnly2D<PDEBase>::
setDOFFrom( const PyDict& d ) const
{
  ArrayQ<Real> q = BaseType::setDOFFrom(d);
  return q;
}

// interpret residuals of the solution variable
template <class PDEBase>
template <class T>
inline void
PDESourceOnly2D<PDEBase>::
interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
{
  rsdPDEIn = rsdPDEOut;
}

template <class PDEBase>
void
PDESourceOnly2D<PDEBase>::dump( int indentSize, std::ostream& out ) const
{
  // Nothing
}

} //namespace SANS

#endif  // PDESOURCEONLY2D_H
