// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDEBURGERS1D_H
#define PDEBURGERS1D_H

// 1-D Burgers Equation

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/AdvectionDiffusion_Traits.h"
#include "pde/AdvectionDiffusion/ViscousFlux1D.h"
#include "pde/AdvectionDiffusion/ForcingFunction1D.h"
#include "pde/AdvectionDiffusion/Source1D.h"

#include "PDEBurgers_fwd.h"

#include "pde/ForcingFunctionBase.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Topology/ElementTopology.h"
#include "Topology/Dimension.h"

#include "Surreal/PromoteSurreal.h"

#include <cmath>              // sqrt
#include <iostream>
#include <string>
#include <memory>             // shared_ptr

namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: 1-D Burgers Equation
//
// template parameters:
//   T                           solution DOF data type (e.g. double)
//
// member functions:
//   .hasFluxAdvectiveTime        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//   .hasFluxViscous             T/F: PDE has viscous flux term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: dU(Q)/dQ
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .fluxViscous                viscous fluxes: Fv(Q, Qx)
//   .diffusionViscous           viscous diffusion coefficient: d(Fv)/d(Ux)
//   .isValidState               T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom                 set from primitive variable array
//----------------------------------------------------------------------------//

template<class QType, class ViscousFlux, class Source>
class PDEBurgers<PhysD1, QType, ViscousFlux, Source>
{
public:
  typedef PhysD1 PhysDim;
  static const int D = AdvectionDiffusionTraits<PhysD1>::D;   // physical dimensions
  static const int N = AdvectionDiffusionTraits<PhysD1>::N;   // total solution variables

//  typedef Q1D<QType> QInterpret;            // solution variable interpreter type
  typedef QType QInterpret;            // solution variable interpreter type

  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD1>::template MatrixQ<T>; // matrices
  typedef PDEBurgers<PhysD1, QType, ViscousFlux, Source> PDEClass;
  typedef ForcingFunctionBase1D<PDEClass> ForcingFunctionType;

  PDEBurgers( const ViscousFlux& visc, const Source &src,
              const std::shared_ptr<ForcingFunctionType>& force = NULL ) :
    qInterpret_(),
    visc_(visc),
    source_(src),
    force_(force)
  {
    //Nothing
  }

  ~PDEBurgers() {}

  PDEBurgers& operator=( const PDEBurgers& ) = delete;

  // flux components
  bool hasFluxAdvectiveTime() const { return true; }
  bool hasFluxAdvective() const { return true; }
  bool hasFluxViscous() const { return visc_.hasFluxViscous(); }
  bool hasSource() const { return source_.hasSourceTerm(); }
  bool hasSourceTrace() const { return source_.hasSourceTrace(); }
  bool hasSourceLiftedQuantity() const { return false; }
  bool hasForcingFunction() const { return force_ != NULL && (*force_).hasForcingFunction(); }

  // viscous flux is of the form K(q).gradu
  bool fluxViscousLinearInGradient() const { return true; }
  bool needsSolutionGradientforSource() const { return source_.needsSolutionGradientforSource(); }

  const ViscousFlux& getViscousFlux() const { return visc_; }
  const QInterpret& variableInterpreter() const { return qInterpret_; }

  // unsteady temporal flux: Ft(Q)
  template <class T, class Tf>
  void fluxAdvectiveTime(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tf>& ft ) const
  {
    T ftLocal = 0;
    masterState(x, time, q, ftLocal);
    ft += ftLocal;
  }

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <class T>
  void jacobianFluxAdvectiveTime(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& J ) const
  {
    J += DLA::Identity();
  }

  // master state: U(Q)
  template <class T, class Tu>
  void masterState(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tu>& uCons ) const;

  // jacobian of master state wrt q: dU(Q)/dQ
  template<class T>
  void jacobianMasterState(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const;

  // unsteady conservative flux: d(U)/d(t)
  template <class T>
  void strongFluxAdvectiveTime(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& dudt ) const;

  // advective flux: F(Q)
  template <class T, class Ts>
  void fluxAdvective(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Ts>& f ) const;

  template <class T, class Ts>
  void fluxAdvectiveUpwind(
      const Real& x, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, ArrayQ<Ts>& f, const Real& scaleFactor = 1 ) const;

  template <class T, class Tf>
  void fluxAdvectiveUpwindSpaceTime(
      const Real& x, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const;

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class T>
  void jacobianFluxAdvective(
      const Real& x,  const Real& time, const ArrayQ<T>& q,
      MatrixQ<T>& ax ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class T>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, const Real& nx,
      MatrixQ<T>& a ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class T>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Real& x, const Real& time, const ArrayQ<T>& q, const Real& nx, const Real& nt,
      MatrixQ<T>& a ) const;

  // strong form advective fluxes
  template <class T>
  void strongFluxAdvective(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx,
      ArrayQ<T>& strongPDE ) const;

  // viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Tf>& f ) const;

  // space-time viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt, ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const;

  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
      const Real& nx, ArrayQ<Tf>& f ) const;

  // perturbation to viscous flux from dux, duy
  template <class Tq, class Tg, class Tgu, class Tf>
  void perturbedGradFluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      const ArrayQ<Tgu>& dqx,
      ArrayQ<Tf>& df ) const
  {
    fluxViscous(x, time, q, qx, df);
  }

  // space-time viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qtL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qtR,
      const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const;

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Tk>& kxx) const;

  // space-time viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscousSpaceTime(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxt,
      MatrixQ<Tk>& ktx, MatrixQ<Tk>& ktt ) const;

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class T>
  void jacobianFluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& ax ) const {}

  // strong form viscous fluxes
  template <class T>
  void strongFluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx,
      const ArrayQ<T>& qxx,
      ArrayQ<T>& strongPDE ) const;

  // solution-dependent source: S(X, Q, QX)
  template <class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Ts>& source ) const;

  // solution-dependent source: S(X, Q, QX)
  template <class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void source(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tgp>& qpx,
      ArrayQ<Ts>& source ) const
  {
    typedef typename promote_Surreal<Tq,Tqp>::type Tqq;
    typedef typename promote_Surreal<Tg,Tgp>::type Tgg;

    Tqq qq = q + qp;
    Tgg qqx = qx + qpx;

    source_.source(x, time, qq, qqx, source);
  }

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX)
  template <class Tlq, class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& time,
      const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Ts>& s ) const
  {
    source(x, time, q, qx, s); //forward to regular source
  }

  // dual-consistent source
  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& xL, const Real& xR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const;

  // trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
  template <class Tq, class Ts>
  void sourceLiftedQuantity(
      const Real& xL, const Real& xR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, Ts& s ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class T>
  void jacobianSource(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx,
      MatrixQ<T>& dsdu ) const;

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class T>
  void jacobianSourceAbsoluteValue(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx,
      MatrixQ<T>& dsdu ) const;

  // jacobian of source wrt conservation variable gradients: d(S)/d(Ux), d(S)/d(Uy)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSource(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Ts>& dsdux ) const;

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSourceAbsoluteValue(
      const Real& x, const Real& time, const Real& nx,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Ts>& dsdux ) const;

  // right-hand-side forcing function
  template <class T>
  void forcingFunction( const Real& x, const Real& time, ArrayQ<T>& source ) const;

  // characteristic speed (needed for timestep)
  template <class T>
  void speedCharacteristic(
      const Real& x, const Real& time,
      const Real& dx, const ArrayQ<T>& q, T& speed ) const;

  // characteristic speed
  template <class T>
  void speedCharacteristic(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, T& speed ) const;

  // update fraction needed for physically valid state
  void updateFraction(
      const Real& x, const Real& time, const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
      Real maxChangeFraction, Real& updateFraction ) const;

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const;

  // set from primitive variable array
  template <class T>
  void setDOFFrom(
      ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const;

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const;

  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const;

  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const;

  // how many residual equations are we dealing with
  int nMonitor() const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const QInterpret qInterpret_;   // solution variable interpreter class
  const ViscousFlux visc_;
  const Source source_;
  const std::shared_ptr<ForcingFunctionType> force_;
};

template<class QType, class ViscousFlux, class Source>
template <class T, class Tu>
inline void
PDEBurgers<PhysD1, QType, ViscousFlux, Source>::masterState(
    const Real& x, const Real& time,
    const ArrayQ<T>& q, ArrayQ<Tu>& uCons ) const
{
  qInterpret_.masterState(q, uCons);
}

template<class QType, class ViscousFlux, class Source>
template <class T>
inline void
PDEBurgers<PhysD1, QType, ViscousFlux, Source>::jacobianMasterState(
    const Real& x, const Real& time,
    const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
{
  qInterpret_.jacobianMasterState(q, dudq);
}


template<class QType, class ViscousFlux, class Source>
template <class T>
inline void
PDEBurgers<PhysD1, QType, ViscousFlux, Source>::strongFluxAdvectiveTime(
    const Real& x, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& dudt ) const
{
  ArrayQ<T> u;
  qInterpret_.masterState(q, u);

  MatrixQ<T> dudq = 0;
  jacobianMasterState(x, time, q, dudq);
  ArrayQ<T> ut = dudq * qt;

  dudt += u*ut;
}

template<class QType, class ViscousFlux, class Source>
template <class T, class Ts>
inline void
PDEBurgers<PhysD1, QType, ViscousFlux, Source>::fluxAdvective(
    const Real& x, const Real& time,
    const ArrayQ<T>& q, ArrayQ<Ts>& f ) const
{
  ArrayQ<T> u;
  qInterpret_.masterState(q, u);
  f += 0.5*u*u;
}

template<class QType, class ViscousFlux, class Source>
template <class T, class Ts>
inline void
PDEBurgers<PhysD1, QType, ViscousFlux, Source>::fluxAdvectiveUpwind(
    const Real& x, const Real& time,
    const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx,
    ArrayQ<Ts>& f, const Real& scaleFactor ) const
{
  ArrayQ<T> uL, uR;
  qInterpret_.masterState(qL, uL);
  qInterpret_.masterState(qR, uR);

  ArrayQ<T> fL = 0;
  ArrayQ<T> fR = 0;
  fL = 0.5*uL*uL;
  fR = 0.5*uR*uR;

  ArrayQ<T> axL = 0;
  ArrayQ<T> axR = 0;
  axL = uL;
  axR = uR;

  T anL = nx*axL;
  T anR = nx*axR;

  T a = max( fabs(anL), fabs(anR) );

  // Local Lax-Friedrichs Flux
  f += 0.5*( (nx*fR) + (nx*fL) ) - 0.5*a*(uR - uL)*scaleFactor;
}

template<class QType, class ViscousFlux, class Source>
template <class T, class Tf>
inline void
PDEBurgers<PhysD1, QType, ViscousFlux, Source>::fluxAdvectiveUpwindSpaceTime(
    const Real& x, const Real& time,
    const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx, const Real& nt,
    ArrayQ<Tf>& f ) const
{
  //Full temporal upwinding

  //Compute upwinded spatial advective flux
  ArrayQ<Tf> fnx = 0;
  fluxAdvectiveUpwind( x, time, qL, qR, nx, fnx );

  //Upwind temporal flux
  ArrayQ<Tf> ft = 0;
  if (nt >= 0)
    masterState( x, time, qL, ft );
  else
    masterState( x, time, qR, ft );

  f += ft*nt + fnx;
}


// advective flux jacobian: d(F)/d(U)
template<class QType, class ViscousFlux, class Source>
template <class T>
inline void
PDEBurgers<PhysD1, QType, ViscousFlux, Source>::jacobianFluxAdvective(
    const Real& x, const Real& time, const ArrayQ<T>& q,
    MatrixQ<T>& ax ) const
{
  ArrayQ<T> u;
  qInterpret_.masterState(q, u);
  ax  += u;
}


// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template<class QType, class ViscousFlux, class Source>
template <class T>
inline void
PDEBurgers<PhysD1, QType, ViscousFlux, Source>::jacobianFluxAdvectiveAbsoluteValue(
  const Real& x, const Real& time,
  const ArrayQ<T>& q, const Real& nx, MatrixQ<T>& mtx ) const
{
  MatrixQ<T> ax = 0;
  jacobianFluxAdvective(x, time, q, ax);

  T an = nx*ax;

  mtx += fabs( an );
}


// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template<class QType, class ViscousFlux, class Source>
template <class T>
inline void
PDEBurgers<PhysD1, QType, ViscousFlux, Source>::jacobianFluxAdvectiveAbsoluteValueSpaceTime(
  const Real& x, const Real& time,
  const ArrayQ<T>& q, const Real& nx, const Real& nt, MatrixQ<T>& mtx ) const
{
  MatrixQ<T> ax = 0;

  jacobianFluxAdvective(x, time, q, ax);

  //TODO: Rather arbitrary smoothing constants here
  //Real eps = 1e-8;

  T an = nx*ax + nt*1;

  //mtx += smoothabs0( an, eps );
  mtx += fabs( an );
}


// strong form of advective flux: d(F)/d(X)
template<class QType, class ViscousFlux, class Source>
template <class T>
inline void
PDEBurgers<PhysD1, QType, ViscousFlux, Source>::strongFluxAdvective(
    const Real& x, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qx,
    ArrayQ<T>& strongPDE ) const
{
  ArrayQ<T> u;
  qInterpret_.masterState(q, u);

  MatrixQ<T> dudq = 0;
  jacobianMasterState(x, time, q, dudq);
  ArrayQ<T> ux = dudq * qx;

  strongPDE += u*ux;
}


// viscous flux
template<class QType, class ViscousFlux, class Source>
template <class Tq, class Tg, class Tf>
inline void
PDEBurgers<PhysD1, QType, ViscousFlux, Source>::fluxViscous(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    ArrayQ<Tf>& f ) const
{
  typedef typename promote_Surreal<Tq,Tg>::type T;

  MatrixQ<Tq> kxx = 0.0;

  ArrayQ<Tq> u;
  qInterpret_.masterState(q, u);

  MatrixQ<Tq> dudq = 0;
  jacobianMasterState(x, time, q, dudq);
  ArrayQ<T> ux = dudq * qx;

  visc_.diffusionViscous( x, time, u, ux, kxx );

  f -= kxx*ux;
}

// space-time viscous flux
template<class QType, class ViscousFlux, class Source>
template <class Tq, class Tg, class Tf>
inline void
PDEBurgers<PhysD1, QType, ViscousFlux, Source>::fluxViscousSpaceTime(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
    ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
{
  //Not adding temporal diffusion, so just forward the call to spatial viscous flux
  fluxViscous(x, time, q, qx, f);
}

// viscous flux: normal flux with left and right states
template<class QType, class ViscousFlux, class Source>
template <class Tq, class Tg, class Tf>
inline void
PDEBurgers<PhysD1, QType, ViscousFlux, Source>::fluxViscous(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
    const Real& nx, ArrayQ<Tf>& f ) const
{
#if 1
  ArrayQ<Tf> fL = 0, fR = 0;

  fluxViscous(x, time, qL, qxL, fL);
  fluxViscous(x, time, qR, qxR, fR);

  f += 0.5*(fL + fR)*nx;

#else
  ArrayQ<Tq> uL, uR;
  qInterpret_.masterState(qL, uL);
  qInterpret_.masterState(qR, uR);

  MatrixQ<Tq> duLdqL = 0;
  MatrixQ<Tq> duRdqR = 0;
  jacobianMasterState(qL, duLdqL);
  jacobianMasterState(qR, duRdqR);
  ArrayQ<Tf> uxL = duLdqL * qxL;
  ArrayQ<Tf> uxR = duRdqR * qxR;

  ArrayQ<Tq> u = 0.5*(uL + uR);
  ArrayQ<Tg> ux = 0.5*(uxL + uxR);

  MatrixQ<Tq> kxx = 0.0;

  visc_.diffusionViscous( x, time, u, kxx );

  f -= nx*kxx*ux;
#endif
}

// space-time viscous flux: normal flux with left and right states
template <class QType, class ViscousFlux, class Source>
template <class Tq, class Tg, class Tf>
inline void
PDEBurgers<PhysD1, QType, ViscousFlux, Source>::fluxViscousSpaceTime(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qtL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qtR,
    const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const
{
  ArrayQ<Tf> fL = 0, fR = 0;
  ArrayQ<Tf> gL = 0, gR = 0;

  fluxViscousSpaceTime(x, time, qL, qxL, qtL, fL, gL);
  fluxViscousSpaceTime(x, time, qR, qxR, qtR, fR, gR);

  f += 0.5*(fL + fR)*nx + 0.5*(gL + gR)*nt;
}

// viscous diffusion matrix: d(Fv)/d(UX)
template<class QType, class ViscousFlux, class Source>
template <class Tq, class Tg, class Tk>
inline void
PDEBurgers<PhysD1, QType, ViscousFlux, Source>::diffusionViscous(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    MatrixQ<Tk>& kxx ) const
{
  ArrayQ<Tq> u;
  qInterpret_.masterState(q, u);

  visc_.diffusionViscous( x, time, u, qx, kxx );
}

// space-time viscous diffusion matrix: d(Fv)/d(UX)
template<class QType, class ViscousFlux, class Source>
template <class Tq, class Tg, class Tk>
inline void
PDEBurgers<PhysD1, QType, ViscousFlux, Source>::diffusionViscousSpaceTime(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
    MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxt,
    MatrixQ<Tk>& ktx, MatrixQ<Tk>& ktt ) const
{
  //Not adding temporal diffusion, so just forward the call to spatial diffusion matrix
  diffusionViscous(x, time, q, qx, kxx);
}

// strong form of viscous flux: d(Fv)/d(X)
template<class QType, class ViscousFlux, class Source>
template <class T>
inline void
PDEBurgers<PhysD1, QType, ViscousFlux, Source>::strongFluxViscous(
    const Real& x, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qx,
    const ArrayQ<T>& qxx,
    ArrayQ<T>& strongPDE ) const
{
  ArrayQ<T> u;
  qInterpret_.masterState(q, u);

  MatrixQ<T> dudq = 0;
  jacobianMasterState(x, time, q, dudq);
  ArrayQ<T> ux = dudq * qx;

  ArrayQ<T> uxx = 0;
  qInterpret_.evalSecondGradient(q, qx, qxx, uxx);

  visc_.strongFluxViscous( x, time, u, ux, uxx, strongPDE );
}

// solution-dependent source: S(X, Q, QX)
template<class QType, class ViscousFlux, class Source>
template <class Tq, class Tg, class Ts>
inline void
PDEBurgers<PhysD1, QType, ViscousFlux, Source>::source(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    ArrayQ<Ts>& source ) const
{
  typedef typename promote_Surreal<Tq,Tg>::type T;

  ArrayQ<Tq> u;
  qInterpret_.masterState(q, u);

  MatrixQ<Tq> dudq = 0;
  jacobianMasterState(x, time, q, dudq);
  ArrayQ<T> ux = dudq * qx;

  source_.source( x, time, u, ux, source );
}

// dual-consistent source
template<class QType, class ViscousFlux, class Source>
template <class Tq, class Tg, class Ts>
inline void
PDEBurgers<PhysD1, QType, ViscousFlux, Source>::sourceTrace(
    const Real& xL, const Real& xR, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
    ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
{
  typedef typename promote_Surreal<Tq, Tg>::type T;

  ArrayQ<Tq> uL, uR;
  qInterpret_.masterState(qL, uL);
  qInterpret_.masterState(qR, uR);

  MatrixQ<Tq> duLdqL = 0;
  MatrixQ<Tq> duRdqR = 0;
  jacobianMasterState(xL, time, qL, duLdqL);
  jacobianMasterState(xR, time, qR, duRdqR);
  ArrayQ<T> uxL = duLdqL * qxL;
  ArrayQ<T> uxR = duRdqR * qxR;

  source_.sourceTrace(xL, xR, time,
                      uL, uxL,
                      uR, uxR,
                      sourceL, sourceR);
}

// jacobian of source wrt conservation variables: d(S)/d(U)
template<class QType, class ViscousFlux, class Source>
template <class T>
inline void
PDEBurgers<PhysD1, QType, ViscousFlux, Source>::jacobianSource(
    const Real& x, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qx,
    MatrixQ<T>& dsdu ) const
{
  ArrayQ<T> u;
  qInterpret_.masterState(q, u);

  MatrixQ<T> dudq = 0;
  jacobianMasterState(x, time, q, dudq);
  ArrayQ<T> ux = dudq * qx;

  source_.jacobianSource(x, time, u, ux, dsdu);
}

// jacobian of source wrt conservation variables: d(S)/d(U)
template<class QType, class ViscousFlux, class Source>
template <class T>
inline void
PDEBurgers<PhysD1, QType, ViscousFlux, Source>::jacobianSourceAbsoluteValue(
    const Real& x, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qx,
    MatrixQ<T>& dsdu ) const
{
  ArrayQ<T> u;
  qInterpret_.masterState(q, u);

  MatrixQ<T> dudq = 0;
  jacobianMasterState(x, time, q, dudq);
  ArrayQ<T> ux = dudq * qx;

  source_.jacobianSourceAbsoluteValue(x, time, u, ux, dsdu);
}


// jacobian of source wrt conservation variables: d(S)/d(U)
template<class QType, class ViscousFlux, class Source>
template <class Tq, class Tg, class Ts>
inline void
PDEBurgers<PhysD1, QType, ViscousFlux, Source>::jacobianGradientSource(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    MatrixQ<Ts>& dsdux ) const
{
  ArrayQ<Tq> u;
  qInterpret_.masterState(q, u);

  MatrixQ<Tq> dudq = 0;
  jacobianMasterState(x, time, q, dudq);
  ArrayQ<Ts> ux = dudq * qx;

  source_.jacobianGradientSource(x, time, u, ux, dsdux);
}



// right-hand-side forcing function
template<class QType, class ViscousFlux, class Source>
template <class T>
inline void
PDEBurgers<PhysD1, QType, ViscousFlux, Source>::forcingFunction(
    const Real& x, const Real& time, ArrayQ<T>& forcing ) const
{
  SANS_ASSERT(force_ != NULL);
  ArrayQ<Real> RHS = 0;
  (*force_)(*this, x, time, RHS );
  forcing += RHS;
}


// characteristic speed (needed for timestep)
template<class QType, class ViscousFlux, class Source>
template <class T>
inline void
PDEBurgers<PhysD1, QType, ViscousFlux, Source>::speedCharacteristic(
    const Real& x, const Real& time,
    const Real& dx, const ArrayQ<T>& q, T& speed ) const
{
  MatrixQ<T> ax = 0;
  //Real eps = 1e-9;

  jacobianFluxAdvective(x, time, q, ax);

  T uh = dx*ax;

  speed = fabs(uh)/sqrt(dx*dx); // So this 'sqrt(dx*dx)' should be unnecessary
}


// characteristic speed
template<class QType, class ViscousFlux, class Source>
template <class T>
inline void
PDEBurgers<PhysD1, QType, ViscousFlux, Source>::speedCharacteristic(
    const Real& x, const Real& time,
    const ArrayQ<T>& q, T& speed ) const
{
  MatrixQ<T> ax = 0;
  //Real eps = 1e-9;

  jacobianFluxAdvective(x, time, q, ax);

  speed = fabs(ax);
}


// update fraction needed for physically valid state
template<class QType, class ViscousFlux, class Source>
void
PDEBurgers<PhysD1, QType, ViscousFlux, Source>::updateFraction(
    const Real& x, const Real& time, const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
    const Real maxChangeFraction, Real& updateFraction ) const
{
  SANS_DEVELOPER_EXCEPTION("Not tested");
#if 0
  // Limit the update
  ArrayQ<Real> nq = q - dq;

  updateFraction = 1;

  //q - updateFraction*dq >= (1-maxChangeFraction)*q
  if (nq < (1-maxChangeFraction)*q)
    updateFraction =  maxChangeFraction*q/dq;

  //q - updateFraction*dq <= (1+maxChangeFraction)*q
  if (nq > (1+maxChangeFraction)*q)
    updateFraction = -maxChangeFraction*q/dq;
#endif
}


// is state physically valid
template<class QType, class ViscousFlux, class Source>
inline bool
PDEBurgers<PhysD1, QType, ViscousFlux, Source>::isValidState( const ArrayQ<Real>& q ) const
{
  return qInterpret_.isValidState(q);
}


// set from primitive variable array
template<class QType, class ViscousFlux, class Source>
template <class T>
void
PDEBurgers<PhysD1, QType, ViscousFlux, Source>::setDOFFrom(
    ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn >= N);
  q = data[0];
}

// interpret residuals of the solution variable
template<class QType, class ViscousFlux, class Source>
template <class T>
void
PDEBurgers<PhysD1, QType, ViscousFlux, Source>::interpResidVariable(
    const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
{
  SANS_ASSERT(rsdPDEOut.m() == 1);
  rsdPDEOut[0] = rsdPDEIn;
}

// interpret residuals of the gradients in the solution variable
template<class QType, class ViscousFlux, class Source>
template <class T>
void
PDEBurgers<PhysD1, QType, ViscousFlux, Source>::interpResidGradient(
    const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const
{
  SANS_ASSERT(rsdAuxOut.m() == 1);
  rsdAuxOut[0] = rsdAuxIn;
}

// interpret residuals at the boundary (should forward to BCs)
template<class QType, class ViscousFlux, class Source>
template <class T>
void
PDEBurgers<PhysD1, QType, ViscousFlux, Source>::interpResidBC(
    const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const
{
  SANS_ASSERT(rsdBCOut.m() == 1);
  rsdBCOut[0] = rsdBCIn;
}

// how many residual equations are we dealing with
template<class QType, class ViscousFlux, class Source>
inline int
PDEBurgers<PhysD1, QType, ViscousFlux, Source>::nMonitor() const
{
  return 1;
}

template<class QType, class ViscousFlux, class Source>
void
PDEBurgers<PhysD1, QType, ViscousFlux, Source>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PDEBurgers1D<QType>: qInterpret_ = "  << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "PDEBurgers1D<QType>: visc_ =" << std::endl;
  visc_.dump(indentSize+2, out);
  out << indent << "PDEBurgers1D<QType>: force_ =" << std::endl;
  if ( force_ != NULL )
    (*force_).dump(indentSize+2, out);
  else
    out << " NULL" << std::endl;
  out << std::endl;
}

} //namespace SANS

#endif  // PDEBURGERS1D_H
