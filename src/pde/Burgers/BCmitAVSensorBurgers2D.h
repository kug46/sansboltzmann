// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCARTIFICIALVISCOSITYBURGERS2D_H
#define BCARTIFICIALVISCOSITYBURGERS2D_H

// 1-D Burgers class

//PyDict must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/BCCategory.h"
#include "pde/BCNone.h"

#include "Topology/Dimension.h"

#include "pde/AdvectionDiffusion/AdvectionDiffusion_Traits.h"

#include "pde/Burgers/PDEBurgers_ArtificialViscosity2D.h"
#include "pde/Burgers/PDEBurgers2D.h"

#include "pde/AnalyticFunction/ScalarFunction2D.h"

#include "pde/AdvectionDiffusion/ViscousFlux2D.h"

#include <iostream>

#include <boost/mpl/vector.hpp>

namespace SANS
{

//----------------------------------------------------------------------------//
// BC class: 1-D Burgers
//
// template parameters:
//   PhysDim              Physical dimensions
//   BCType               BC type (e.g. BCTypeLinearRobin)
//----------------------------------------------------------------------------//

template <class PhysDim, class BCType>
class BCmitAVSensorBurgers;

template <class PhysDim, class BCType>
struct BCmitAVSensorBurgersParams;


//----------------------------------------------------------------------------//
// BC types
//----------------------------------------------------------------------------//

class BCTypeLinearRobin_mitLG;
class BCTypeLinearRobin_sansLG;

template<class QType, class ViscousFlux>
class BCTypeDirichlet_mitState;

template<class PDE>
class BCTypeFlux;
class BCTypeFluxParams;
class BCTypeFunctionLinearRobin_mitLG;
class BCTypeFunctionLinearRobin_sansLG;


// Define the vector of boundary conditions
template<class QType, class ViscousFlux>
using BCmitAVSensorBurgers2DVector = boost::mpl::vector6< BCNone<PhysD2,AdvectionDiffusionTraits<PhysD2>::N>,
                                              BCmitAVSensorBurgers<PhysD2, BCTypeLinearRobin_mitLG>,
                                              BCmitAVSensorBurgers<PhysD2, BCTypeLinearRobin_sansLG>,
                                              BCmitAVSensorBurgers<PhysD2, BCTypeDirichlet_mitState<QType,ViscousFlux>>,
                                              BCmitAVSensorBurgers<PhysD2, BCTypeFunctionLinearRobin_mitLG>,
                                              BCmitAVSensorBurgers<PhysD2, BCTypeFunctionLinearRobin_sansLG>
                                            >;

//----------------------------------------------------------------------------//
// Robin BC:  A u + B (kn un + ks us) = bcdata
//
// coefficients A and B are arbitrary with the exception that they cannot
// simultaneously vanish
template<>
struct BCmitAVSensorBurgersParams<PhysD2, BCTypeLinearRobin_mitLG> : noncopyable
{
  const ParameterNumeric<Real> A{"A", NO_DEFAULT, NO_RANGE, "Value coefficient"};
  const ParameterNumeric<Real> B{"B", NO_DEFAULT, NO_RANGE, "Viscous coefficient"};
  const ParameterNumeric<Real> bcdata{"bcdata", NO_DEFAULT, NO_RANGE, "BC data"};

  static constexpr const char* BCName{"LinearRobin_mitLG"};
  struct Option
  {
    const DictOption LinearRobin_mitLG{BCmitAVSensorBurgersParams::BCName, BCmitAVSensorBurgersParams::checkInputs};
  };

  static void checkInputs(PyDict d);
  static BCmitAVSensorBurgersParams params;
};

template<>
struct BCmitAVSensorBurgersParams<PhysD2, BCTypeLinearRobin_sansLG> : noncopyable
{
  const ParameterNumeric<Real> A{"A", NO_DEFAULT, NO_RANGE, "Value coefficient"};
  const ParameterNumeric<Real> B{"B", NO_DEFAULT, NO_RANGE, "Viscous coefficient"};
  const ParameterNumeric<Real> bcdata{"bcdata", NO_DEFAULT, NO_RANGE, "BC data"};

  static constexpr const char* BCName{"LinearRobin_sansLG"};
  struct Option
  {
    const DictOption LinearRobin_sansLG{BCmitAVSensorBurgersParams::BCName, BCmitAVSensorBurgersParams::checkInputs};
  };

  static void checkInputs(PyDict d);
  static BCmitAVSensorBurgersParams params;
};

template <class PhysDim>
class BCmitAVSensorBurgers_LinearRobinBase;

template <>
class BCmitAVSensorBurgers_LinearRobinBase<PhysD2>
{
public:
  typedef PhysD2 PhysDim;
  static const int D = AdvectionDiffusionTraits<PhysD2>::D;   // physical dimensions
  static const int N = AdvectionDiffusionTraits<PhysD2>::N;   // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD2>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD2>::template MatrixQ<T>; // matrices

  BCmitAVSensorBurgers_LinearRobinBase( const Real& A, const Real& B, const Real& bcdata ) :
    A_(A), B_(B), bcdata_(bcdata) {}

  virtual ~BCmitAVSensorBurgers_LinearRobinBase() {}

  // BC coefficients:  A u + B (kn un + ks us)
  template <class T>
  void coefficients(
      const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      MatrixQ<T>& A, MatrixQ<T>& B ) const;

  template <class T, class L, class R>
  void coefficients(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      MatrixQ<T>& A, MatrixQ<T>& B ) const;

  // BC data
  template <class T>
  void data( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny, ArrayQ<T>& bcdata ) const;

  template <class T, class L, class R>
  void data( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
             const Real& nx, const Real& ny, ArrayQ<T>& bcdata ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  MatrixQ<Real> A_;
  MatrixQ<Real> B_;
  ArrayQ<Real> bcdata_;
};

template <class T>
inline void
BCmitAVSensorBurgers_LinearRobinBase<PhysD2>::coefficients(
    const Real&, const Real&, const Real&, const Real&, const Real&,
    MatrixQ<T>& A, MatrixQ<T>& B ) const
{
  A = A_;
  B = B_;
}

template <class T, class L, class R>
inline void
BCmitAVSensorBurgers_LinearRobinBase<PhysD2>::coefficients(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    MatrixQ<T>& A, MatrixQ<T>& B ) const
{
  coefficients(x, y, time, nx, ny, A, B);
}

template <class T>
inline void
BCmitAVSensorBurgers_LinearRobinBase<PhysD2>::data(
    const Real&, const Real&, const Real&, const Real& nx, const Real& ny, ArrayQ<T>& bcdata ) const
{
  bcdata = bcdata_;
}

template <class T, class L, class R>
inline void
BCmitAVSensorBurgers_LinearRobinBase<PhysD2>::data(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    ArrayQ<T>& bcdata ) const
{
  data(x, y, time, nx, ny, bcdata);
}

template <>
class BCmitAVSensorBurgers<PhysD2, BCTypeLinearRobin_mitLG> : public BCType< BCmitAVSensorBurgers<PhysD2, BCTypeLinearRobin_mitLG> >,
                                                              public BCmitAVSensorBurgers_LinearRobinBase<PhysD2>
{
public:
  typedef BCmitAVSensorBurgers_LinearRobinBase<PhysD2> BaseType;
  typedef BCCategory::LinearScalar_mitLG Category;
  typedef BCmitAVSensorBurgersParams<PhysD2, BCTypeLinearRobin_mitLG> ParamsType;

  template<class QType, class ViscousFlux, class Source>
  BCmitAVSensorBurgers(const PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source>& pde, const PyDict& d ) :
    BaseType( d.get(ParamsType::params.A), d.get(ParamsType::params.B), d.get(ParamsType::params.bcdata)) {}

  BCmitAVSensorBurgers( const Real& A, const Real& B, const Real& bcdata ) : BaseType( A, B, bcdata ) {}
};

template <>
class BCmitAVSensorBurgers<PhysD2, BCTypeLinearRobin_sansLG>
  : public BCType< BCmitAVSensorBurgers<PhysD2, BCTypeLinearRobin_sansLG> >,
    public BCmitAVSensorBurgers_LinearRobinBase<PhysD2>
{
public:
  typedef BCmitAVSensorBurgers_LinearRobinBase<PhysD2> BaseType;
  typedef BCCategory::LinearScalar_sansLG Category;
  typedef BCmitAVSensorBurgersParams<PhysD2, BCTypeLinearRobin_sansLG> ParamsType;

  template<class QType, class ViscousFlux, class Source>
  BCmitAVSensorBurgers(const PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source>& pde, const PyDict& d ) :
    BaseType( d.get(ParamsType::params.A), d.get(ParamsType::params.B), d.get(ParamsType::params.bcdata)) {}

  BCmitAVSensorBurgers( const Real& A, const Real& B, const Real& bcdata ) : BaseType( A, B, bcdata ) {}
};


//----------------------------------------------------------------------------//
// Dirichlet_mitState BC:  u = bcstate
//
class BCTypeDirichlet_mitStateParam;

template<>
struct BCmitAVSensorBurgersParams<PhysD2, BCTypeDirichlet_mitStateParam> : noncopyable
{
  const ParameterNumeric<Real> qB{"qB", NO_DEFAULT, NO_RANGE, "BC state"};

  static constexpr const char* BCName{"Dirichlet_mitState"};
  struct Option
  {
    const DictOption Dirichlet_mitState{BCmitAVSensorBurgersParams::BCName, BCmitAVSensorBurgersParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCmitAVSensorBurgersParams params;
};

template <class QType, class ViscousFlux>
class BCmitAVSensorBurgers<PhysD2, BCTypeDirichlet_mitState<QType, ViscousFlux>> :
  public BCType< BCmitAVSensorBurgers<PhysD2, BCTypeDirichlet_mitState<QType, ViscousFlux>> >
{
public:
  typedef BCCategory::Flux_mitState Category;
  typedef BCmitAVSensorBurgersParams<PhysD2, BCTypeDirichlet_mitStateParam> ParamsType;

  typedef PhysD2 PhysDim;
  static const int D = AdvectionDiffusionTraits<PhysD2>::D;   // physical dimensions
  static const int N = AdvectionDiffusionTraits<PhysD2>::N;   // total solution variables
  typedef QType QInterpret;

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD2>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD2>::template MatrixQ<T>; // matrices

  template<class Source>
  BCmitAVSensorBurgers(const PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source>& pde, const PyDict& d ) :
    qB_(d.get(ParamsType::params.qB)),
    qInterpret_(pde.variableInterpreter()),
    visc_( pde.getViscousFlux() ),
    v_(pde.getV()),
    fluxViscous_(pde.hasFluxViscous()) {}

  virtual ~BCmitAVSensorBurgers() {}

  BCmitAVSensorBurgers& operator=( const BCmitAVSensorBurgers& );

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return fluxViscous_; }

  // BC data
  template <class T>
  void state( const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  template <class T, class L, class R>
  void state( const ParamTuple<L, R, TupleClass<>>& param,
              const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // normal BC flux
  template <class T, class Tf, class L, class R>
  void fluxNormal( const ParamTuple<L, R, TupleClass<>>& param,
                   const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  ArrayQ<Real> qB_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const ViscousFlux& visc_;
  Real v_;
  bool fluxViscous_;
};

template <class QType, class ViscousFlux>
template <class T>
inline void
BCmitAVSensorBurgers<PhysD2, BCTypeDirichlet_mitState<QType, ViscousFlux>>::
state( const Real& x, const Real& y, const Real& time,
       const Real& nx, const Real& ny,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  qB = qB_;
}

template <class QType, class ViscousFlux>
template <class T, class L, class R>
inline void
BCmitAVSensorBurgers<PhysD2, BCTypeDirichlet_mitState<QType, ViscousFlux>>::
state( const ParamTuple<L, R, TupleClass<>>& param,
       const Real& x, const Real& y, const Real& time,
       const Real& nx, const Real& ny,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  qB = qB_;
}


template<class QType, class ViscousFlux>
template <class T, class Tf, class L, class R>
inline void
BCmitAVSensorBurgers<PhysD2, BCTypeDirichlet_mitState<QType,ViscousFlux>>::
fluxNormal( const ParamTuple<L, R, TupleClass<>>& param,
            const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  ArrayQ<T> uB;
  qInterpret_.masterState(qB, uB);

  ArrayQ<T> fx = 0.5*uB*uB;
  ArrayQ<T> fy = v_*uB;

  Fn += fx*nx + fy*ny;

  SANS_ASSERT(visc_.fluxViscousLinearInGradient());
  MatrixQ<T> kxx=0, kxy=0,
             kyx=0, kyy=0;
  // TODO: No parameter!?!?
  visc_.diffusionViscous( x, y, time, qB, qIx, qIy, kxx, kxy, kyx, kyy );


  ArrayQ<T> fv = 0, gv = 0;

  // Use gradient from interior
  fv -= kxx*qIx + kxy*qIy;
  gv -= kyx*qIx + kyy*qIy;

  // Add viscous flux
  Fn += fv*nx + gv*ny;
}
//----------------------------------------------------------------------------//
// Flux BC:  an u - (kn un + ks us) = bcdata
//
// an       normal advection velocity
// kn, ks   normal, tangential diffusion coefficients
template<>
struct BCmitAVSensorBurgersParams< PhysD2, BCTypeFluxParams > : noncopyable
{
  const ParameterNumeric<Real> bcdata{"bcdata", NO_DEFAULT, NO_RANGE, "BC data"};

  static constexpr const char* BCName{"Flux"};
  struct Option
  {
    const DictOption Flux{BCmitAVSensorBurgersParams::BCName, BCmitAVSensorBurgersParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCmitAVSensorBurgersParams params;
};

template<class QType, class ViscousFlux, class Source>
class BCmitAVSensorBurgers< PhysD2, BCTypeFlux< PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source> > > :
  public BCType<
    BCmitAVSensorBurgers< PhysD2, BCTypeFlux< PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source> > >
               >
{
public:
  typedef BCCategory::LinearScalar_mitLG Category;
  typedef BCmitAVSensorBurgersParams< PhysD2, BCTypeFluxParams > ParamsType;
  typedef PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source> PDE;

  typedef PhysD2 PhysDim;
  static const int D = AdvectionDiffusionTraits<PhysD2>::D;   // physical dimensions
  static const int N = AdvectionDiffusionTraits<PhysD2>::N;   // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD2>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD2>::template MatrixQ<T>; // matrices

  BCmitAVSensorBurgers(const PDE& pde, const PyDict& d ) :
    pde_(pde), bcdata_(d.get(ParamsType::params.bcdata)) {}

  //cppcheck-suppress noExplicitConstructor
  BCmitAVSensorBurgers( const PDE& pde, const ArrayQ<Real>& bcdata ) :
                        pde_(pde), bcdata_(bcdata) {}

  virtual ~BCmitAVSensorBurgers() {}

  // BC coefficients:  A u + B (kn un + ks us)
  template <class T>
  void coefficients(
      const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      MatrixQ<T>& A, MatrixQ<T>& B ) const;

  // BC data
  template <class T>
  void data( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny, ArrayQ<T>& bcdata ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const PDE& pde_;
  ArrayQ<Real> bcdata_;
};


template<class QType, class ViscousFlux, class Source>
template <class T>
inline void
BCmitAVSensorBurgers< PhysD2, BCTypeFlux< PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source> > >::coefficients(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    MatrixQ<T>& A, MatrixQ<T>& B ) const
{
  ArrayQ<T> q; // This function assumes jacobianFluxAdvective is not a function of q.
               // Valgrind will hopefully catch q incorrectly used by uninitialized data
  MatrixQ<T> Ax = 0;
  MatrixQ<T> Ay = 0;

  pde_.jacobianFluxAdvective( x, y, time, q, Ax, Ay );

  A = nx*Ax + ny*Ay;
  B = -1;   // will be -Identity for systems
}


template<class QType, class ViscousFlux, class Source>
template <class T>
inline void
BCmitAVSensorBurgers< PhysD2, BCTypeFlux< PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source> > >::
data( const Real&, const Real&, const Real&, const Real& nx, const Real& ny, ArrayQ<T>& bcdata ) const
{
  bcdata = bcdata_;
}

template<class QType, class ViscousFlux, class Source>
inline void
BCmitAVSensorBurgers< PhysD2, BCTypeFlux< PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source> > >::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCmitAVSensorBurgers<PhysD2, BCTypeFlux>:" << std::endl;
  out << indent << "  pde_:" << std::endl;
  pde_.dump( indentSize+2, out );
  out << indent << "  bcdata_ = " << bcdata_ << std::endl;
}


//----------------------------------------------------------------------------//
// Solution BC:  u = uexact(x,y)
//
// A = 1; B = 0
template<>
struct BCmitAVSensorBurgersParams< PhysD2, BCTypeFunctionLinearRobin_mitLG > : ScalarFunction2DParams
{
  static constexpr const char* BCName{"FunctionLinearRobin_mitLG"};
  struct Option
  {
    const DictOption FunctionLinearRobin_mitLG{BCmitAVSensorBurgersParams::BCName, BCmitAVSensorBurgersParams::checkInputs};
  };

  const ParameterNumeric<Real> A{"A", 1.0, NO_RANGE, "Value coefficient"};
  const ParameterNumeric<Real> B{"B", 0.0, NO_RANGE, "Viscous coefficient"};

  static void checkInputs(PyDict d);

  static BCmitAVSensorBurgersParams params;
};

template<>
struct BCmitAVSensorBurgersParams< PhysD2, BCTypeFunctionLinearRobin_sansLG > : ScalarFunction2DParams
{
  static constexpr const char* BCName{"FunctionLinearRobin_sansLG"};
  struct Option
  {
    const DictOption FunctionLinearRobin_sansLG{BCmitAVSensorBurgersParams::BCName, BCmitAVSensorBurgersParams::checkInputs};
  };

  const ParameterNumeric<Real> A{"A", 1.0, NO_RANGE, "Value coefficient"};
  const ParameterNumeric<Real> B{"B", 0.0, NO_RANGE, "Viscous coefficient"};

  static void checkInputs(PyDict d);

  static BCmitAVSensorBurgersParams params;
};

template <class PhysDim>
class BCmitAVSensorBurgers_SolutionBase;

template <>
class BCmitAVSensorBurgers_SolutionBase<PhysD2>
{
public:
  typedef PhysD2 PhysDim;
   static const int D = AdvectionDiffusionTraits<PhysD2>::D;   // physical dimensions
   static const int N = AdvectionDiffusionTraits<PhysD2>::N;   // total solution variables

   static const int NBC = 1;                   // total BCs

   template <class T>
   using ArrayQ = AdvectionDiffusionTraits<PhysD2>::template ArrayQ<T>;    // solution/residual arrays

   template <class T>
   using MatrixQ = AdvectionDiffusionTraits<PhysD2>::template MatrixQ<T>; // matrices

   typedef std::shared_ptr<Function2DBase<ArrayQ<Real>>> Function_ptr;

   //cppcheck-suppress noExplicitConstructor
   BCmitAVSensorBurgers_SolutionBase( const Function_ptr& uexact,
                                              const ViscousFlux2DBase& visc, Real A = 1, Real B = 0 ) :
       uexact_(uexact), visc_(visc), A_(A), B_(B) {}

   virtual ~BCmitAVSensorBurgers_SolutionBase() {}

   // BC coefficients:  A u + B (kn un + ks us)
   template <class T>
   void coefficients(
       const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
       MatrixQ<T>& A, MatrixQ<T>& B ) const;

   template <class T, class L, class R>
   void coefficients(
       const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
       MatrixQ<T>& A, MatrixQ<T>& B ) const;

   // BC data
   template <class T>
   void data( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny, ArrayQ<T>& bcdata ) const;

   template <class T, class L, class R>
   void data( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny, ArrayQ<T>& bcdata ) const;

   // is the boundary state valid
   bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

   void dump( int indentSize, std::ostream& out = std::cout ) const;

 private:
   const Function_ptr uexact_;
   const ViscousFlux2DBase& visc_;
   Real A_, B_;
};

template <class T>
inline void
BCmitAVSensorBurgers_SolutionBase< PhysD2 >::coefficients(
    const Real&, const Real&, const Real&, const Real&, const Real&,
    MatrixQ<T>& A, MatrixQ<T>& B ) const
{
  A = A_;
  B = B_;
}

template <class T, class L, class R>
inline void
BCmitAVSensorBurgers_SolutionBase<PhysD2>::coefficients(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    MatrixQ<T>& A, MatrixQ<T>& B ) const
{
  coefficients(x, y, time, nx, ny, A, B);
}

template <class T>
inline void
BCmitAVSensorBurgers_SolutionBase< PhysD2 >::data(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny, ArrayQ<T>& bcdata ) const
{
  Real u, ux, uy, ut;
  uexact_->gradient(x, y, time, u, ux, uy, ut);

  Real kxx = 0, kxy = 0, kyx = 0, kyy = 0;
  visc_.diffusionViscous(x, y, time, u, ux, uy, kxx, kxy, kyx, kyy);

  Real f = kxx*ux + kxy*uy;
  Real g = kyx*ux + kyy*uy;

  bcdata = A_*u + B_*(f*nx + g*ny);
}

template <class T, class L, class R>
inline void
BCmitAVSensorBurgers_SolutionBase< PhysD2 >::data(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
    const Real& nx, const Real& ny, ArrayQ<T>& bcdata ) const
{
  data(x, y, time, nx, ny, bcdata);
}

template <>
class BCmitAVSensorBurgers<PhysD2, BCTypeFunctionLinearRobin_mitLG>
  : public BCType< BCmitAVSensorBurgers<PhysD2, BCTypeFunctionLinearRobin_mitLG> >,
    public BCmitAVSensorBurgers_SolutionBase<PhysD2>
{
public:
  typedef BCmitAVSensorBurgers_SolutionBase<PhysD2> BaseType;
  typedef BCCategory::LinearScalar_mitLG Category;
  typedef BCmitAVSensorBurgersParams<PhysD2, BCTypeFunctionLinearRobin_mitLG> ParamsType;

  template<class QType, class ViscousFlux, class Source>
  BCmitAVSensorBurgers(const PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source>& pde,
                               PyDict& d) :
    BaseType( ParamsType::params.getFunction(d), pde.getViscousFlux(),
    d.get(ParamsType::params.A), d.get(ParamsType::params.B)) {}

  BCmitAVSensorBurgers( const Function_ptr& uexact, const ViscousFlux2DBase& visc, Real A = 1, Real B = 0 ) :
    BaseType( uexact, visc, A, B ) {}
};

template <>
class BCmitAVSensorBurgers<PhysD2, BCTypeFunctionLinearRobin_sansLG> :
  public BCType< BCmitAVSensorBurgers<PhysD2, BCTypeFunctionLinearRobin_sansLG> >,
  public BCmitAVSensorBurgers_SolutionBase<PhysD2>
{
public:
  typedef BCmitAVSensorBurgers_SolutionBase<PhysD2> BaseType;
  typedef BCCategory::LinearScalar_sansLG Category;
  typedef BCmitAVSensorBurgersParams<PhysD2, BCTypeFunctionLinearRobin_sansLG> ParamsType;

  template<class QType, class ViscousFlux, class Source>
  BCmitAVSensorBurgers(const PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source>& pde,
                               PyDict& d) :
    BaseType( ParamsType::params.getFunction(d), pde.getViscousFlux(),
    d.get(ParamsType::params.A), d.get(ParamsType::params.B)) {}

  BCmitAVSensorBurgers( const Function_ptr& uexact, const ViscousFlux2DBase& visc, Real A = 1, Real B = 0 ) :
    BaseType( uexact, visc, A, B ) {}
};

} //namespace SANS

#endif  // BCARTIFICIALVISCOSITYBURGERS2D_H
