// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BURGERSCONSERVATIVE1D_H
#define BURGERSCONSERVATIVE1D_H

// Burgers variable interpreter: conservative

#include "tools/SANSnumerics.h"     // Real
#include "tools/smoothmath.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "pde/AdvectionDiffusion/AdvectionDiffusion_Traits.h"

#include <iostream>

namespace SANS
{

//----------------------------------------------------------------------------//
// Burgers variable interpreter: conservative
//
// member functions:
//   .masterState           U(Q)
//   .jacobianMasterState   d(U)/d(Q)
//   .evalSecondGradient         d^2(U)/d(X)^2
//   .isValidState               is Q a valid state
//----------------------------------------------------------------------------//

class BurgersConservative1D
{
public:
  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD1>::template MatrixQ<T>;  // matrices

  explicit BurgersConservative1D() {}
  ~BurgersConservative1D() {}

  template<class T, class Tu>
  void masterState( const ArrayQ<T>& q, ArrayQ<Tu>& uCons ) const
  {
    uCons = q;
  }

  template<class T>
  void jacobianMasterState( const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
  {
    dudq = DLA::Identity();
  }

  template<class T>
  void evalSecondGradient( const ArrayQ<T>& q, const ArrayQ<T>& qx,
                           const ArrayQ<T>& qxx, ArrayQ<T>& uxx ) const
  {
    MatrixQ<T> dudq = 0;
    jacobianMasterState(q, dudq);

    uxx = dudq*qxx;
  }

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const;

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
};
} //namespace SANS

#endif  // BURGERSCONSERVATIVE1D_H
