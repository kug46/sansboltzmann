// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCmitAVSensorBurgers1D.h"

#include "pde/AdvectionDiffusion/ViscousFlux1D.h"
#include "pde/AdvectionDiffusion/Source1D.h"
#include "pde/AdvectionDiffusion/ForcingFunction1D.h"

#include "BurgersConservative1D.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{
//===========================================================================//
//
// Parameter classes
//
// cppcheck-suppress passedByValue
void  BCmitAVSensorBurgersParams<PhysD1, BCTypeLinearRobin_mitLG>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.A));
  allParams.push_back(d.checkInputs(params.B));
  allParams.push_back(d.checkInputs(params.bcdata));
  d.checkUnknownInputs(allParams);
}
BCmitAVSensorBurgersParams<PhysD1, BCTypeLinearRobin_mitLG>
  BCmitAVSensorBurgersParams<PhysD1, BCTypeLinearRobin_mitLG>::params;

// cppcheck-suppress passedByValue
void  BCmitAVSensorBurgersParams<PhysD1, BCTypeLinearRobin_sansLG>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.A));
  allParams.push_back(d.checkInputs(params.B));
  allParams.push_back(d.checkInputs(params.bcdata));
  d.checkUnknownInputs(allParams);
}
BCmitAVSensorBurgersParams<PhysD1, BCTypeLinearRobin_sansLG>
  BCmitAVSensorBurgersParams<PhysD1, BCTypeLinearRobin_sansLG>::params;


// cppcheck-suppress passedByValue
void  BCmitAVSensorBurgersParams<PhysD1, BCTypeDirichlet_mitStateParam>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.qB));
  d.checkUnknownInputs(allParams);
}
BCmitAVSensorBurgersParams<PhysD1, BCTypeDirichlet_mitStateParam>
  BCmitAVSensorBurgersParams<PhysD1, BCTypeDirichlet_mitStateParam>::params;

//===========================================================================//

 BCmitAVSensorBurgersSolution1DParams::Function_ptr // cppcheck-suppress passedByValue
 BCmitAVSensorBurgersSolution1DParams::getSolution(PyDict d)
{
   BCmitAVSensorBurgersSolution1DParams params;

  DictKeyPair SolutionParam = d.get(params.Function);

  if ( SolutionParam == params.Function.Const )
    return Function_ptr( new ScalarFunction1D_Const( SolutionParam ) );
  else if ( SolutionParam == params.Function.Linear )
    return Function_ptr( new ScalarFunction1D_Linear( SolutionParam ) );
  else if ( SolutionParam == params.Function.Monomial )
    return Function_ptr( new ScalarFunction1D_Monomial( SolutionParam ) );
  else if ( SolutionParam == params.Function.Quad )
    return Function_ptr( new ScalarFunction1D_Quad( SolutionParam ) );
  else if ( SolutionParam == params.Function.Hex )
    return Function_ptr( new ScalarFunction1D_Hex( SolutionParam ) );
  else if ( SolutionParam == params.Function.Hex2 )
    return Function_ptr( new ScalarFunction1D_Hex2( SolutionParam ) );
  else if ( SolutionParam == params.Function.Exp1 )
    return Function_ptr( new ScalarFunction1D_Exp1( SolutionParam ) );
  else if ( SolutionParam == params.Function.Exp2 )
    return Function_ptr( new ScalarFunction1D_Exp2( SolutionParam ) );
  else if ( SolutionParam == params.Function.Exp3 )
    return Function_ptr( new ScalarFunction1D_Exp3( SolutionParam ) );
  else if ( SolutionParam == params.Function.Sine )
    return Function_ptr( new ScalarFunction1D_Sine( SolutionParam ) );
  else if ( SolutionParam == params.Function.SineCosUnsteady )
    return Function_ptr( new ScalarFunction1D_SineCosUnsteady( SolutionParam ) );
  else if ( SolutionParam == params.Function.SineSineUnsteady )
    return Function_ptr( new ScalarFunction1D_SineSineUnsteady( SolutionParam ) );
  else if ( SolutionParam == params.Function.ConstSineUnsteady )
    return Function_ptr( new ScalarFunction1D_ConstSineUnsteady( SolutionParam ) );
  else if ( SolutionParam == params.Function.ConstCosUnsteady )
    return Function_ptr( new ScalarFunction1D_ConstCosUnsteady( SolutionParam ) );
  else if ( SolutionParam == params.Function.BL )
    return Function_ptr( new ScalarFunction1D_BL( SolutionParam ) );
  else if ( SolutionParam == params.Function.Tanh )
    return Function_ptr( new ScalarFunction1D_Tanh( SolutionParam ) );
  else if ( SolutionParam == params.Function.PiecewiseLinear )
    return Function_ptr( new ScalarFunction1D_PiecewiseLinear( SolutionParam ) );

  // Should never get here if checkInputs was called
  SANS_DEVELOPER_EXCEPTION("Unrecognized solution function specified");
}

// cppcheck-suppress passedByValue
void  BCmitAVSensorBurgersParams<PhysD1, BCTypeFunction_mitStateParam>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Function));
  d.checkUnknownInputs(allParams);
}
 BCmitAVSensorBurgersParams<PhysD1, BCTypeFunction_mitStateParam>
   BCmitAVSensorBurgersParams<PhysD1, BCTypeFunction_mitStateParam>::params;

// cppcheck-suppress passedByValue
void  BCmitAVSensorBurgersParams<PhysD1, BCTypeFunctionLinearRobin_mitLG>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Function));
  allParams.push_back(d.checkInputs(params.A));
  allParams.push_back(d.checkInputs(params.B));
  d.checkUnknownInputs(allParams);
}
BCmitAVSensorBurgersParams<PhysD1, BCTypeFunctionLinearRobin_mitLG>
  BCmitAVSensorBurgersParams<PhysD1, BCTypeFunctionLinearRobin_mitLG>::params;

// cppcheck-suppress passedByValue
void  BCmitAVSensorBurgersParams<PhysD1, BCTypeFunctionLinearRobin_sansLG>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Function));
  allParams.push_back(d.checkInputs(params.A));
  allParams.push_back(d.checkInputs(params.B));
  d.checkUnknownInputs(allParams);
}
BCmitAVSensorBurgersParams<PhysD1, BCTypeFunctionLinearRobin_sansLG>
  BCmitAVSensorBurgersParams<PhysD1, BCTypeFunctionLinearRobin_sansLG>::params;


//===========================================================================//
// Instantiate the BC parameters
typedef BCmitAVSensorBurgers1DVector< BurgersConservative1D, ViscousFlux1D_Uniform, Source1D_UniformGrad> BCVector_DUSUG;
typedef BCmitAVSensorBurgers1DVector< BurgersConservative1D, ViscousFlux1D_None, Source1D_UniformGrad> BCVector_DNSUG;

BCPARAMETER_INSTANTIATE( BCVector_DUSUG )
BCPARAMETER_INSTANTIATE( BCVector_DNSUG )

//===========================================================================//
void
 BCmitAVSensorBurgers_LinearRobinBase<PhysD1>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << " BCmitAVSensorBurgers<PhysD1, BCTypeLinearRobin>:" << std::endl;
  out << indent << "  A_ = " << A_ << std::endl;
  out << indent << "  B_ = " << B_ << std::endl;
  out << indent << "  bcdata_ = " << bcdata_ << std::endl;
}

void
 BCmitAVSensorBurgers_SolutionBase<PhysD1>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << " BCmitAVSensorBurgers<PhysD1, BCTypeFunction>" << std::endl;
}

} //namespace SANS
