// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDEBURGERS_FWD_H
#define PDEBURGERS_FWD_H

#include "Topology/Dimension.h"

namespace SANS
{

// Forward declaration
template<class PhysDim, class QType, class ViscousFlux, class Source>
class PDEBurgers;

}

#endif //PDEBURGERS_FWD_H
