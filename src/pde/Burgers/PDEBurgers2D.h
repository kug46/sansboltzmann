// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDEBURGERS2D_H
#define PDEBURGERS2D_H

// 2-D Burgers Equation

#include "tools/SANSnumerics.h"     // Real

#include "pde/AdvectionDiffusion/AdvectionDiffusion_Traits.h"
#include "pde/AdvectionDiffusion/ViscousFlux2D.h"
#include "pde/AdvectionDiffusion/ForcingFunction2D.h"
#include "pde/AdvectionDiffusion/Source2D.h"

#include "PDEBurgers_fwd.h"

#include "pde/ForcingFunctionBase.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Topology/ElementTopology.h"
#include "Topology/Dimension.h"

#include <cmath>              // sqrt
#include <iostream>
#include <string>
#include <memory>             // shared_ptr

namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: 2-D Burgers
//
// template parameters:
//   T                           solution DOF data type (e.g. double)
//
// member functions:
//   .hasFluxAdvectiveTime        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//   .hasFluxViscous             T/F: PDE has viscous flux term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: dU(Q)/dQ
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .fluxViscous                viscous fluxes: Fv(Q, Qx)
//   .diffusionViscous           viscous diffusion coefficient: d(Fv)/d(Ux)
//   .isValidState               T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom                 set from primitive variable array
//----------------------------------------------------------------------------//

template<class QType, class ViscousFlux, class Source>
class PDEBurgers<PhysD2, QType, ViscousFlux, Source>
{
public:
  typedef PhysD2 PhysDim;
  static const int D = AdvectionDiffusionTraits<PhysD2>::D;   // physical dimensions
  static const int N = AdvectionDiffusionTraits<PhysD2>::N;   // total solution variables

  typedef QType QInterpret;            // solution variable interpreter type

  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD2>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD2>::template MatrixQ<T>; // matrices

  typedef PDEBurgers<PhysD2, QType, ViscousFlux, Source> PDEClass;
  typedef ForcingFunctionBase2D<PDEClass> ForcingFunctionType;

  PDEBurgers( const Real v,
              const ViscousFlux& visc,
              const Source &src,
              const std::shared_ptr<ForcingFunctionType>& force = NULL  ) :
    v_(v),
    qInterpret_(),
    visc_(visc),
    source_(src),
    force_(force)
  {
    //Nothing
  }

  ~PDEBurgers() {}

  PDEBurgers( const PDEBurgers& ) = delete;
  PDEBurgers& operator=( const PDEBurgers& ) = delete;

  // flux components
  bool hasFluxAdvectiveTime() const { return true; }
  bool hasFluxAdvective() const { return true; }
  bool hasFluxViscous() const { return visc_.hasFluxViscous(); }
  bool hasSource() const { return source_.hasSourceTerm(); }
  bool hasSourceTrace() const { return source_.hasSourceTrace(); }
  bool hasSourceLiftedQuantity() const { return false; }
  bool hasForcingFunction() const { return force_ != NULL && (*force_).hasForcingFunction(); }

  // viscous flux is of the form K(q).gradu
  bool fluxViscousLinearInGradient() const { return true; }
  bool needsSolutionGradientforSource() const { return source_.needsSolutionGradientforSource(); }

  const ViscousFlux& getViscousFlux() const { return visc_; }
  const QInterpret& variableInterpreter() const { return qInterpret_; }
  Real getV() const { return v_; }

  // unsteady temporal flux: Ft(Q)
  template <class T>
  void fluxAdvectiveTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, ArrayQ<T>& ft ) const
  {
    T ftLocal = 0;
    masterState(x, y, time, q, ftLocal);
    ft += ftLocal;
  }

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <class T>
  void jacobianFluxAdvectiveTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& J ) const
  {
    J += DLA::Identity();
  }

  // master state: U(Q)
  template <class T>
  void masterState(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, ArrayQ<T>& uCons ) const;

  // jacobian of master state wrt q: dU(Q)/dQ
  template<class T>
  void jacobianMasterState(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const;

  // strong form of unsteady conservative flux: dU(Q)/dt
  template <class T>
  void strongFluxAdvectiveTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& strongPDE ) const;


  // advective flux: F(Q)
  template <class T, class Tf>
  void fluxAdvective(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q, ArrayQ<Tf>& fx, ArrayQ<Tf>& fy ) const;

  template <class T, class Tf>
  void fluxAdvectiveUpwind(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const;

  template <class T, class Tf>
  void fluxAdvectiveUpwindSpaceTime(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, const Real& nt, ArrayQ<Tf>& f ) const;

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class T>
  void jacobianFluxAdvective(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q,
      MatrixQ<T>& ax, MatrixQ<T>& ay ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class T>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q, const Real& nx, const Real& ny,
      MatrixQ<T>& a ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class T>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q, const Real& nx, const Real& ny, const Real& nt,
      MatrixQ<T>& a ) const;

  // strong form advective fluxes
  template <class T>
  void strongFluxAdvective(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
      ArrayQ<T>& strongPDE ) const;


  // viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const;

  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const;

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const;

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class T>
  void jacobianFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& ax, MatrixQ<T>& ay ) const {}

  // strong form viscous fluxes
  template <class T>
  void strongFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
      const ArrayQ<T>& qxx,
      const ArrayQ<T>& qxy, const ArrayQ<T>& qyy,
      ArrayQ<T>& strongPDE ) const;

  // space-time viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& ft ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial viscous flux
    fluxViscous(x, y, time, q, qx, qy, fx, fy);
  }

  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qtL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qtR,
      const Real& nx, const Real& ny, const Real& nt,
      ArrayQ<Tf>& f ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial viscous flux
    fluxViscous(x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, f);
  }

  // space-time viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxt,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyt,
      MatrixQ<Tk>& ktx, MatrixQ<Tk>& kty, MatrixQ<Tk>& ktt ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial diffusion matrix
    diffusionViscous(x, y, time, q, qx, qy, kxx, kxy, kyx, kyy);
  }

  // space-time jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& at ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial jacobianFluxViscous
    jacobianFluxViscous(x, y, time, q, qx, qy, ax, ay);
  }

  // space-time strong form viscous fluxes: -d(Fv)/dx - d(Fv)/d(y)
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxt, const ArrayQ<Th>& qyt, const ArrayQ<Th>& qtt,
      ArrayQ<Tf>& strongPDE ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial strongFluxViscous
    strongFluxViscous(x, y, time, q, qx, qy, qxx, qxy, qyy, strongPDE);
  }

  // solution-dependent source: S(X, Q, QX)
  template <class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Ts>& source ) const;

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX)
  template <class Tlq, class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const Tlq& lifted_quantity, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Ts>& s ) const
  {
    source(x, y, time, q, qx, qy, s); //forward to regular source
  }

  // dual-consistent source
  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& xL, const Real& yL, const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const;

  // trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
  template <class Tq, class Ts>
  void sourceLiftedQuantity(
      const Real& xL, const Real& yL, const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, Ts& s ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class T>
  void jacobianSource(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
      MatrixQ<T>& dsdu ) const;

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class T>
  void jacobianSourceAbsoluteValue(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
      MatrixQ<T>& dsdu ) const;

  // right-hand-side forcing function
  template <class T>
  void forcingFunction( const Real& x, const Real& y, const Real& time, ArrayQ<T>& forcing ) const;

  // characteristic speed (needed for timestep)
  template <class T>
  void speedCharacteristic(
      const Real& x, const Real& y, const Real& time, const Real& dx, const Real& dy, const ArrayQ<T>& q, T& speed ) const;

  // characteristic speed
  template <class T>
  void speedCharacteristic(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q, T& speed ) const;

  // update fraction needed for physically valid state
  void updateFraction(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
      const Real maxChangeFraction, Real& updateFraction ) const;

  // is state physically valid
  template <class T>
  bool isValidState( const ArrayQ<T>& q ) const { return true; }

  // set from primitive variable array
  template <class T>
  void setDOFFrom(
      ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const;

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const;

  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const;

  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const;

  // how many residual equations are we dealing with
  int nMonitor() const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const Real v_;
  const QInterpret qInterpret_;   // solution variable interpreter class
  const ViscousFlux visc_;
  const Source source_;
  const std::shared_ptr<ForcingFunctionType> force_;
};

template<class QType, class ViscousFlux, class Source>
template <class T>
inline void
PDEBurgers<PhysD2, QType, ViscousFlux, Source>::masterState(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, ArrayQ<T>& uCons ) const
{
  qInterpret_.masterState(q, uCons);
}

template<class QType, class ViscousFlux, class Source>
template <class T>
inline void
PDEBurgers<PhysD2, QType, ViscousFlux, Source>::jacobianMasterState(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
{
  qInterpret_.jacobianMasterState(q, dudq);
}

template<class QType, class ViscousFlux, class Source>
template <class T>
inline void
PDEBurgers<PhysD2, QType, ViscousFlux, Source>::strongFluxAdvectiveTime(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& dudt ) const
{
  ArrayQ<T> u;
  qInterpret_.masterState(q, u);

  MatrixQ<T> dudq = 0;
  jacobianMasterState(x, y, time, q, dudq);
  ArrayQ<T> ut = dudq * qt;

  dudt += u*ut;
}


template<class QType, class ViscousFlux, class Source>
template <class T, class Tf>
inline void
PDEBurgers<PhysD2, QType, ViscousFlux, Source>::fluxAdvective(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, ArrayQ<Tf>& fx, ArrayQ<Tf>& fy) const
{
  ArrayQ<T> u;
  qInterpret_.masterState(q, u);
  fx += 0.5*u*u;
  fy += v_*u;
}


template<class QType, class ViscousFlux, class Source>
template <class T, class Tf>
inline void
PDEBurgers<PhysD2, QType, ViscousFlux, Source>::fluxAdvectiveUpwind(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx, const Real& ny,
    ArrayQ<Tf>& f ) const
{
  ArrayQ<T> uL, uR;
  qInterpret_.masterState(qL, uL);
  qInterpret_.masterState(qR, uR);

  ArrayQ<T> fL = 0, gL = 0;
  ArrayQ<T> fR = 0, gR = 0;

  fL = 0.5*uL*uL;
  fR = 0.5*uR*uR;
  gL = v_*uL;
  gR = v_*uR;

  ArrayQ<T> axL = 0, ayL = 0;
  ArrayQ<T> axR = 0, ayR = 0;

  axL = uL;
  axR = uR;
  ayL = v_;
  ayR = v_;

  T anL = nx*axL + ny*ayL;
  T anR = nx*axR + ny*ayR;

  T a = max( fabs(anL), fabs(anR) );

  // Local Lax-Friedrichs Flux
  f += 0.5*( (nx*fR + ny*gR) + (nx*fL + ny*gL) ) - 0.5*a*(qR - qL);
}

template<class QType, class ViscousFlux, class Source>
template <class T, class Tf>
inline void
PDEBurgers<PhysD2, QType, ViscousFlux, Source>::fluxAdvectiveUpwindSpaceTime(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx, const Real& ny, const Real& nt,
    ArrayQ<Tf>& f ) const
{
  //Full temporal upwinding

  //Compute upwinded spatial advective flux
  ArrayQ<Tf> fn = 0;
  fluxAdvectiveUpwind( x, y, time, qL, qR, nx, y, fn);

  //Upwind temporal flux
  ArrayQ<Tf> ft = 0;
  if (nt >= 0)
    masterState( qL, ft );
  else
    masterState( qR, ft );

  f += ft*nt + fn;
}


// advective flux jacobian: d(F)/d(U)
template<class QType, class ViscousFlux, class Source>
template <class T>
inline void
PDEBurgers<PhysD2, QType, ViscousFlux, Source>::jacobianFluxAdvective(
    const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q,
    MatrixQ<T>& ax, MatrixQ<T>& ay ) const
{
  ArrayQ<T> u;
  qInterpret_.masterState(q, u);
  ax  += u;
  ay  += v_;
}


// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template<class QType, class ViscousFlux, class Source>
template <class T>
inline void
PDEBurgers<PhysD2, QType, ViscousFlux, Source>::jacobianFluxAdvectiveAbsoluteValue(
  const Real& x, const Real& y, const Real& time,
  const ArrayQ<T>& q, const Real& nx, const Real& ny, MatrixQ<T>& mtx ) const
{
  MatrixQ<T> ax = 0, ay = 0;

  jacobianFluxAdvective(x, y, time, q, ax, ay);

  //TODO: Rather arbitrary smoothing constants here
  //Real eps = 1e-8;

  T an = nx*ax + ny*ay;

  //mtx += smoothabs0( an, eps );
  mtx += fabs( an );
}

// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template<class QType, class ViscousFlux, class Source>
template <class T>
inline void
PDEBurgers<PhysD2, QType, ViscousFlux, Source>::jacobianFluxAdvectiveAbsoluteValueSpaceTime(
  const Real& x, const Real& y, const Real& time,
  const ArrayQ<T>& q, const Real& nx, const Real& ny, const Real& nt, MatrixQ<T>& mtx ) const
{
  MatrixQ<T> ax = 0, ay = 0;

  jacobianFluxAdvective(x, y, time, q, ax, ay);

  //TODO: Rather arbitrary smoothing constants here
  //Real eps = 1e-8;

  T an = nx*ax + ny*ay + nt*1;

  //mtx += smoothabs0( an, eps );
  mtx += fabs( an );
}


// strong form of advective flux: div.F
template<class QType, class ViscousFlux, class Source>
template <class T>
inline void
PDEBurgers<PhysD2, QType, ViscousFlux, Source>::strongFluxAdvective(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
    ArrayQ<T>& strongPDE ) const
{
  ArrayQ<T> u;
  qInterpret_.masterState(q, u);

  MatrixQ<T> dudq = 0;
  jacobianMasterState(x, y, time, q, dudq);
  ArrayQ<T> ux = dudq * qx;
  ArrayQ<T> uy = dudq * qy;

  strongPDE += u*ux;
  strongPDE += v_*uy;
}


// viscous flux
template<class QType, class ViscousFlux, class Source>
template <class Tq, class Tg, class Tf>
inline void
PDEBurgers<PhysD2, QType, ViscousFlux, Source>::fluxViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
{
  MatrixQ<Tq> kxx=0, kxy=0,
              kyx=0, kyy=0;

  visc_.diffusionViscous( x, y, time, q, qx, qy, kxx, kxy, kyx, kyy );

  f -= kxx*qx + kxy*qy;
  g -= kyx*qx + kyy*qy;
}


// viscous flux: normal flux with left and right states
template<class QType, class ViscousFlux, class Source>
template <class Tq, class Tg, class Tf>
inline void
PDEBurgers<PhysD2, QType, ViscousFlux, Source>::fluxViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
    const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const
{
#if 1
  ArrayQ<Tf> fL = 0, fR = 0;
  ArrayQ<Tf> gL = 0, gR = 0;

  fluxViscous(x, y, time, qL, qxL, qyL, fL, gL);
  fluxViscous(x, y, time, qR, qxR, qyR, fR, gR);

  f += 0.5*(fL + fR)*nx + 0.5*(gL + gR)*ny;

#else
  ArrayQ<Tq> q = 0.5*(qL + qR);
  ArrayQ<Tg> qx = 0.5*(qxL + qxR);
  ArrayQ<Tg> qy = 0.5*(qyL + qyR);

  MatrixQ<Tq> kxx=0, kxy=0,
              kyx=0, kyy=0;

  visc_.diffusionViscous( x, y, time, q, kxx, kxy, kyx, kyy );

  f -= ((nx*kxx + kyx*ny)*qx + (nx*kxy + ny*kyy)*qy);
#endif
}


// viscous diffusion matrix: d(Fv)/d(UX)
template<class QType, class ViscousFlux, class Source>
template <class Tq, class Tg, class Tk>
inline void
PDEBurgers<PhysD2, QType, ViscousFlux, Source>::diffusionViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
    MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const
{
  visc_.diffusionViscous( x, y, time, q, qx, qy, kxx, kxy, kyx, kyy );
}


// strong form of viscous flux: d(Fv)/d(X)
template<class QType, class ViscousFlux, class Source>
template <class T>
inline void
PDEBurgers<PhysD2, QType, ViscousFlux, Source>::strongFluxViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
    const ArrayQ<T>& qxx,
    const ArrayQ<T>& qyx, const ArrayQ<T>& qyy,
    ArrayQ<T>& strongPDE ) const
{
  visc_.strongFluxViscous( x, y, time, q, qx, qy, qxx, qyx, qyy, strongPDE );
}

// solution-dependent source: S(X, Q, QX)
template<class QType, class ViscousFlux, class Source>
template <class Tq, class Tg, class Ts>
inline void
PDEBurgers<PhysD2, QType, ViscousFlux, Source>::source(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Ts>& source ) const
{
  source_.source(x, y, time, q, qx, qy, source);
}

// dual-consistent source
template<class QType, class ViscousFlux, class Source>
template <class Tq, class Tg, class Ts>
inline void
PDEBurgers<PhysD2, QType, ViscousFlux, Source>::sourceTrace(
    const Real& xL, const Real& yL, const Real& xR, const Real& yR, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
    ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
{
  source_.sourceTrace(xL, yL, xR, yR, time,
                      qL, qxL, qyL,
                      qR, qxR, qyR,
                      sourceL, sourceR);
}

// jacobian of source wrt conservation variables: d(S)/d(U)
template<class QType, class ViscousFlux, class Source>
template <class T>
inline void
PDEBurgers<PhysD2, QType, ViscousFlux, Source>::jacobianSource(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
    MatrixQ<T>& dsdu ) const
{
  source_.jacobianSource(x, y, time, q, qx, qy, dsdu);
}

// jacobian of source wrt conservation variables: d(S)/d(U)
template<class QType, class ViscousFlux, class Source>
template <class T>
inline void
PDEBurgers<PhysD2, QType, ViscousFlux, Source>::jacobianSourceAbsoluteValue(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
    MatrixQ<T>& dsdu ) const
{
  source_.jacobianSourceAbsoluteValue(x, y, time, q, qx, qy, dsdu);
}


// right-hand-side forcing function
template<class QType, class ViscousFlux, class Source>
template <class T>
inline void
PDEBurgers<PhysD2, QType, ViscousFlux, Source>::forcingFunction(
    const Real& x, const Real& y, const Real& time, ArrayQ<T>& forcing ) const
{
  SANS_ASSERT(force_ != NULL);
  ArrayQ<Real> RHS = 0;
  (*force_)(*this, x, y, time, RHS );
  forcing += RHS;
}


// characteristic speed (needed for timestep)
template<class QType, class ViscousFlux, class Source>
template <class T>
inline void
PDEBurgers<PhysD2, QType, ViscousFlux, Source>::speedCharacteristic(
    const Real& x, const Real& y, const Real& time,
    const Real& dx, const Real& dy, const ArrayQ<T>& q, T& speed ) const
{
  MatrixQ<T> ax = 0, ay = 0;
  //Real eps = 1e-9;

  jacobianFluxAdvective(x, y, time, q, ax, ay);

  T uh = dx*ax + dy*ay;

  speed = fabs(uh)/sqrt(dx*dx + dy*dy);
}


// characteristic speed
template<class QType, class ViscousFlux, class Source>
template <class T>
inline void
PDEBurgers<PhysD2, QType, ViscousFlux, Source>::speedCharacteristic(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, T& speed ) const
{
  MatrixQ<T> ax = 0, ay = 0;
  //Real eps = 1e-9;

  jacobianFluxAdvective(x, y, time, q, ax, ay);

  speed = sqrt(ax*ax + ay*ay);
}


// update fraction needed for physically valid state
template<class QType, class ViscousFlux, class Source>
inline void
PDEBurgers<PhysD2, QType, ViscousFlux, Source>::updateFraction(
    const Real& x, const Real& y, const Real& time, const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
    const Real maxChangeFraction, Real& updateFraction ) const
{
  SANS_DEVELOPER_EXCEPTION("Not tested");
#if 0
  // Limit the update
  ArrayQ<Real> nq = q - dq;

  updateFraction = 1;

  //q - updateFraction*dq >= (1-maxChangeFraction)*q
  if (nq < (1-maxChangeFraction)*q)
    updateFraction =  maxChangeFraction*q/dq;

  //q - updateFraction*dq <= (1+maxChangeFraction)*q
  if (nq > (1+maxChangeFraction)*q)
    updateFraction = -maxChangeFraction*q/dq;
#endif
}


// set from primitive variable array
template<class QType, class ViscousFlux, class Source>
template <class T>
inline void
PDEBurgers<PhysD2, QType, ViscousFlux, Source>::setDOFFrom(
    ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn >= N);
  q = data[0];
}

// interpret residuals of the solution variable
template<class QType, class ViscousFlux, class Source>
template <class T>
void
PDEBurgers<PhysD2, QType, ViscousFlux, Source>::interpResidVariable(
    const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
{
  SANS_ASSERT(rsdPDEOut.m() == 1);
  rsdPDEOut[0] = rsdPDEIn;
}

// interpret residuals of the gradients in the solution variable
template<class QType, class ViscousFlux, class Source>
template <class T>
void
PDEBurgers<PhysD2, QType, ViscousFlux, Source>::interpResidGradient(
    const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const
{
  SANS_ASSERT(rsdAuxOut.m() == 1);
  rsdAuxOut[0] = rsdAuxIn;
}

// interpret residuals at the boundary (should forward to BCs)
template<class QType, class ViscousFlux, class Source>
template <class T>
void
PDEBurgers<PhysD2, QType, ViscousFlux, Source>::interpResidBC(
    const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const
{
  SANS_ASSERT(rsdBCOut.m() == 1);
  rsdBCOut[0] = rsdBCIn;
}

// how many residual equations are we dealing with
template<class QType, class ViscousFlux, class Source>
inline int
PDEBurgers<PhysD2, QType, ViscousFlux, Source>::nMonitor() const
{
  return 1;
}

template<class QType, class ViscousFlux, class Source>
void
PDEBurgers<PhysD2, QType, ViscousFlux, Source>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PDEBurgers2D<QType>: v_ = "  << v_ << std::endl;
  out << indent << "PDEBurgers2D<QType>: qInterpret_ = "  << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "PDEBurgers2D<QType>:: visc_ =" << std::endl;
  visc_.dump(indentSize+2, out);
  out << indent << "PDEBurgers2D<QType>:: force_ =" << std::endl;
  if ( force_ != NULL )
    (*force_).dump(indentSize+2, out);
  else
    out << " NULL" << std::endl;
}

} //namespace SANS

#endif  // PDEBURGERS2D_H
