// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCARTIFICIALVISCOSITYBURGERS1D_H
#define BCARTIFICIALVISCOSITYBURGERS1D_H

// 1-D Burgers class

//PyDict must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "tools/SANSnumerics.h"     // Real

#include "pde/BCCategory.h"
#include "pde/BCNone.h"

#include "Topology/Dimension.h"

#include "pde/AdvectionDiffusion/AdvectionDiffusion_Traits.h"

#include "pde/Burgers/PDEBurgers_ArtificialViscosity1D.h"
#include "pde/Burgers/PDEBurgers1D.h"

#include "pde/AnalyticFunction/ScalarFunction1D.h"

#include "pde/AdvectionDiffusion/ViscousFlux1D.h"

#include <iostream>

#include <boost/mpl/vector.hpp>

namespace SANS
{

//----------------------------------------------------------------------------//
// BC class: 1-D Burgers
//
// template parameters:
//   PhysDim              Physical dimensions
//   BCType               BC type (e.g. BCTypeLinearRobin)
//----------------------------------------------------------------------------//

template <class PhysDim, class BCType>
class BCmitAVSensorBurgers;

template <class PhysDim, class BCType>
struct BCmitAVSensorBurgersParams;

//----------------------------------------------------------------------------//
// BC types
//----------------------------------------------------------------------------//

class BCTypeLinearRobin_mitLG;
class BCTypeLinearRobin_sansLG;

template<class QType, class ViscousFlux, class Source>
class BCTypeDirichlet_mitState;

template<class QType, class ViscousFlux, class Source>
class BCTypeFunction_mitState;

class BCTypeFunctionLinearRobin_mitLG;
class BCTypeFunctionLinearRobin_sansLG;


// Define the vector of boundary conditions
template<class QType, class ViscousFlux, class Source>
using BCmitAVSensorBurgers1DVector = boost::mpl::vector7< BCNone<PhysD1,AdvectionDiffusionTraits<PhysD1>::N>,
                                              BCmitAVSensorBurgers<PhysD1, BCTypeLinearRobin_mitLG>,
                                              BCmitAVSensorBurgers<PhysD1, BCTypeLinearRobin_sansLG>,
                                              BCmitAVSensorBurgers<PhysD1, BCTypeDirichlet_mitState<QType,ViscousFlux,Source>>,
                                              BCmitAVSensorBurgers<PhysD1, BCTypeFunction_mitState<QType,ViscousFlux,Source>>,
                                              BCmitAVSensorBurgers<PhysD1, BCTypeFunctionLinearRobin_mitLG>,
                                              BCmitAVSensorBurgers<PhysD1, BCTypeFunctionLinearRobin_sansLG>
                                            >;

//----------------------------------------------------------------------------//
// Robin BC:  A u + B (kn un + ks us) = bcdata
//
// coefficients A and B are arbitrary with the exception that they cannot
// simultaneously vanish
template<>
struct BCmitAVSensorBurgersParams<PhysD1, BCTypeLinearRobin_mitLG> : noncopyable
{
  const ParameterNumeric<Real> A{"A", NO_DEFAULT, NO_RANGE, "Value coefficient"};
  const ParameterNumeric<Real> B{"B", NO_DEFAULT, NO_RANGE, "Viscous coefficient"};
  const ParameterNumeric<Real> bcdata{"bcdata", NO_DEFAULT, NO_RANGE, "BC data"};

  static constexpr const char* BCName{"LinearRobin_mitLG"};
  struct Option
  {
    const DictOption LinearRobin_mitLG{ BCmitAVSensorBurgersParams::BCName, BCmitAVSensorBurgersParams::checkInputs};
  };

  static void checkInputs(PyDict d);
  static BCmitAVSensorBurgersParams params;
};

template<>
struct BCmitAVSensorBurgersParams<PhysD1, BCTypeLinearRobin_sansLG> : noncopyable
{
  const ParameterNumeric<Real> A{"A", NO_DEFAULT, NO_RANGE, "Value coefficient"};
  const ParameterNumeric<Real> B{"B", NO_DEFAULT, NO_RANGE, "Viscous coefficient"};
  const ParameterNumeric<Real> bcdata{"bcdata", NO_DEFAULT, NO_RANGE, "BC data"};

  static constexpr const char* BCName{"LinearRobin_sansLG"};
  struct Option
  {
    const DictOption LinearRobin_sansLG{ BCmitAVSensorBurgersParams::BCName, BCmitAVSensorBurgersParams::checkInputs};
  };

  static void checkInputs(PyDict d);
  static BCmitAVSensorBurgersParams params;
};

template <class PhysDim>
class BCmitAVSensorBurgers_LinearRobinBase;

template <>
class BCmitAVSensorBurgers_LinearRobinBase<PhysD1>
{
public:
  typedef PhysD1 PhysDim;
  static const int D = AdvectionDiffusionTraits<PhysD1>::D;   // physical dimensions
  static const int N = AdvectionDiffusionTraits<PhysD1>::N;   // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD1>::template MatrixQ<T>; // matrices

  BCmitAVSensorBurgers_LinearRobinBase( const Real& A, const Real& B, const Real& bcdata ) :
    A_(A), B_(B), bcdata_(bcdata) {}

  virtual ~ BCmitAVSensorBurgers_LinearRobinBase() {}

  BCmitAVSensorBurgers_LinearRobinBase& operator=( const BCmitAVSensorBurgers_LinearRobinBase& );

  // BC coefficients:  A u + B (kn un + ks us)
  template <class T, class L, class R>
  void coefficients(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time, const Real& nx,
      MatrixQ<T>& A, MatrixQ<T>& B ) const;

  // BC data
  template <class T, class L, class R>
  void data( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time, const Real& nx, ArrayQ<T>& bcdata ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  MatrixQ<Real> A_;
  MatrixQ<Real> B_;
  ArrayQ<Real> bcdata_;
};


template <class T, class L, class R>
inline void
 BCmitAVSensorBurgers_LinearRobinBase<PhysD1>::coefficients(
     const ParamTuple<L, R, TupleClass<>>& param, const Real&, const Real& time, const Real&,
    MatrixQ<T>& A, MatrixQ<T>& B ) const
{
  A = A_;
  B = B_;
}


template <class T, class L, class R>
inline void
 BCmitAVSensorBurgers_LinearRobinBase<PhysD1>::data(
     const ParamTuple<L, R, TupleClass<>>& param, const Real&, const Real& time, const Real& nx, ArrayQ<T>& bcdata ) const
{
  bcdata = bcdata_;
}

template <>
class BCmitAVSensorBurgers<PhysD1, BCTypeLinearRobin_mitLG> : public BCType< BCmitAVSensorBurgers<PhysD1, BCTypeLinearRobin_mitLG> >,
                                                              public BCmitAVSensorBurgers_LinearRobinBase<PhysD1>
{
public:
  typedef BCmitAVSensorBurgers_LinearRobinBase<PhysD1> BaseType;
  typedef BCCategory::LinearScalar_mitLG Category;
  typedef BCmitAVSensorBurgersParams<PhysD1, BCTypeLinearRobin_mitLG> ParamsType;

  template<class QType, class ViscousFlux, class Source>
  BCmitAVSensorBurgers(const PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>& pde,
                               PyDict& d) :
    BaseType( d.get(ParamsType::params.A), d.get(ParamsType::params.B), d.get(ParamsType::params.bcdata)) {}

  BCmitAVSensorBurgers( const Real& A, const Real& B, const Real& bcdata ) : BaseType( A, B, bcdata ) {}
};

template <>
class BCmitAVSensorBurgers<PhysD1, BCTypeLinearRobin_sansLG> :
    public BCType< BCmitAVSensorBurgers<PhysD1, BCTypeLinearRobin_sansLG> >,
    public BCmitAVSensorBurgers_LinearRobinBase<PhysD1>
{
public:
  typedef BCmitAVSensorBurgers_LinearRobinBase<PhysD1> BaseType;
  typedef BCCategory::LinearScalar_sansLG Category;
  typedef BCmitAVSensorBurgersParams<PhysD1, BCTypeLinearRobin_sansLG> ParamsType;

  template<class QType, class ViscousFlux, class Source>
  BCmitAVSensorBurgers(const PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>& pde,
                               PyDict& d) :
    BaseType( d.get(ParamsType::params.A), d.get(ParamsType::params.B), d.get(ParamsType::params.bcdata)) {}

  BCmitAVSensorBurgers( const Real& A, const Real& B, const Real& bcdata ) : BaseType( A, B, bcdata ) {}
};

//----------------------------------------------------------------------------//
// Dirichlet_mitState BC:  u = bcstate
//
class BCTypeDirichlet_mitStateParam;

template<>
struct BCmitAVSensorBurgersParams<PhysD1, BCTypeDirichlet_mitStateParam> : noncopyable
{
  const ParameterNumeric<Real> qB{"qB", NO_DEFAULT, NO_RANGE, "BC state"};

  static constexpr const char* BCName{"Dirichlet_mitState"};
  struct Option
  {
    const DictOption Dirichlet_mitState{ BCmitAVSensorBurgersParams::BCName, BCmitAVSensorBurgersParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCmitAVSensorBurgersParams params;
};

template <class QType, class ViscousFlux, class Source>
class BCmitAVSensorBurgers<PhysD1, BCTypeDirichlet_mitState<QType, ViscousFlux, Source>> :
  public BCType< BCmitAVSensorBurgers<PhysD1, BCTypeDirichlet_mitState<QType, ViscousFlux, Source>> >
{
public:
  typedef BCCategory::Flux_mitState Category;
  typedef BCmitAVSensorBurgersParams<PhysD1, BCTypeDirichlet_mitStateParam> ParamsType;

  typedef PhysD1 PhysDim;
  static const int D = AdvectionDiffusionTraits<PhysD1>::D;   // physical dimensions
  static const int N = AdvectionDiffusionTraits<PhysD1>::N;   // total solution variables
  typedef QType QInterpret;

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD1>::template MatrixQ<T>; // matrices

  BCmitAVSensorBurgers(const PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>& pde,
                       PyDict& d) :
    qB_(d.get(ParamsType::params.qB)),
    qInterpret_(pde.variableInterpreter()),
    visc_( pde.getViscousFlux() ),
    fluxViscous_(pde.hasFluxViscous()) {}

  virtual ~ BCmitAVSensorBurgers() {}

  BCmitAVSensorBurgers& operator=( const BCmitAVSensorBurgers& );

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return fluxViscous_; }

  // BC state vector
  template <class T, class L, class R>
  void state( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
              const Real& nx,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    qB = qB_;
  }

  // normal BC flux
  template <class T, class Tf, class L, class R>
  void fluxNormal( const ParamTuple<L, R, TupleClass<>>& param,
                   const Real& x, const Real& time,
                   const Real& nx,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    ArrayQ<T> uB;
    qInterpret_.masterState(qB, uB);

    ArrayQ<T> fx = 0.5*uB*uB;

    Fn += fx*nx;

    SANS_ASSERT(visc_.fluxViscousLinearInGradient());
    MatrixQ<T> kxx=0;
    // TODO: No parameter?!?
    visc_.diffusionViscous( x, time, qB, qIx, kxx );

    ArrayQ<T> fv = 0;

    // Use gradient from interior
    fv -= kxx*qIx;

    // Add viscous flux
    Fn += fv*nx;
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "BCmitAVSensorBurgers<PhysD1, BCTypeDirichlet_mitState>:" << std::endl;
    out << indent << "  qB_ = " << qB_ << std::endl;
  }

private:
  ArrayQ<Real> qB_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const ViscousFlux& visc_;
  bool fluxViscous_;
};

//----------------------------------------------------------------------------//
// Struct for creating a BC solution function
//
struct BCmitAVSensorBurgersSolution1DParams : noncopyable
{
  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

  typedef std::shared_ptr<Function1DBase<ArrayQ<Real>>> Function_ptr;

  struct SolutionOptions
  {
    typedef DictKeyPair ExtractType;
    const ParameterString Name{"Name", NO_DEFAULT, "Solution function used on the BC" };
    const ParameterString& key = Name;

    const DictOption Const{"Const", ScalarFunction1D_Const::ParamsType::checkInputs};
    const DictOption Linear{"Linear", ScalarFunction1D_Linear::ParamsType::checkInputs};
    const DictOption Monomial{"Monomial", ScalarFunction1D_Monomial::ParamsType::checkInputs};
    const DictOption Quad{"Quad", ScalarFunction1D_Quad::ParamsType::checkInputs};
    const DictOption Hex{"Hex", ScalarFunction1D_Hex::ParamsType::checkInputs};
    const DictOption Hex2{"Hex2", ScalarFunction1D_Hex2::ParamsType::checkInputs};
    const DictOption Exp1{"Exp1", ScalarFunction1D_Exp1::ParamsType::checkInputs};
    const DictOption Exp2{"Exp2", ScalarFunction1D_Exp2::ParamsType::checkInputs};
    const DictOption Exp3{"Exp3", ScalarFunction1D_Exp3::ParamsType::checkInputs};
    const DictOption Sine{"Sine", ScalarFunction1D_Sine::ParamsType::checkInputs};
    const DictOption SineCosUnsteady{"SineCosUnsteady", ScalarFunction1D_SineCosUnsteady::ParamsType::checkInputs};
    const DictOption SineSineUnsteady{"SineSineUnsteady", ScalarFunction1D_SineSineUnsteady::ParamsType::checkInputs};
    const DictOption ConstSineUnsteady{"ConstSineUnsteady", ScalarFunction1D_ConstSineUnsteady::ParamsType::checkInputs};
    const DictOption ConstCosUnsteady{"ConstCosUnsteady", ScalarFunction1D_ConstCosUnsteady::ParamsType::checkInputs};
    const DictOption BL{"BL", ScalarFunction1D_BL::ParamsType::checkInputs};
    const DictOption Tanh{"Tanh", ScalarFunction1D_Tanh::ParamsType::checkInputs};
    const DictOption PiecewiseLinear{"PiecewiseLinear", ScalarFunction1D_PiecewiseLinear::ParamsType::checkInputs};

    const std::vector<DictOption> options{ Const, Linear, Monomial, Quad, Hex, Hex2, Exp1,
                                           Exp2, Exp3, Sine, SineCosUnsteady, SineSineUnsteady,
                                           ConstSineUnsteady, ConstCosUnsteady, BL, Tanh, PiecewiseLinear
                                         };
  };
  const ParameterOption<SolutionOptions> Function{"Function", NO_DEFAULT, "The BC is a solution function"};

  static Function_ptr getSolution(PyDict d);
};

//----------------------------------------------------------------------------//
// BCTypeFunction_mitState BC:  A u + B (kn un) = Fnbc, where u = uexact(x,time)
//
class BCTypeFunction_mitStateParam;

template<>
struct BCmitAVSensorBurgersParams<PhysD1, BCTypeFunction_mitStateParam> : BCmitAVSensorBurgersSolution1DParams
{
  static constexpr const char* BCName{"Function_mitState"};
  struct Option
  {
    const DictOption Function_mitState{ BCmitAVSensorBurgersParams::BCName, BCmitAVSensorBurgersParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCmitAVSensorBurgersParams params;
};

template<class QType, class ViscousFlux, class Source>
class BCmitAVSensorBurgers<PhysD1, BCTypeFunction_mitState<QType,ViscousFlux,Source>> :
  public BCType< BCmitAVSensorBurgers<PhysD1, BCTypeFunction_mitState<QType,ViscousFlux,Source>> >
{
public:
  typedef BCCategory::Flux_mitState Category;
  typedef BCmitAVSensorBurgersParams<PhysD1, BCTypeFunction_mitStateParam> ParamsType;

  typedef PhysD1 PhysDim;
  static const int D = AdvectionDiffusionTraits<PhysD1>::D;   // physical dimensions
  static const int N = AdvectionDiffusionTraits<PhysD1>::N;   // total solution variables
  typedef QType QInterpret;

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD1>::template MatrixQ<T>; // matrices

  typedef std::shared_ptr<Function1DBase<ArrayQ<Real>>> Function_ptr;

  BCmitAVSensorBurgers(const PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>& pde,
                       PyDict& d) :
    uexact_( ParamsType::getSolution(d) ),
    qInterpret_(pde.variableInterpreter()),
    visc_( pde.getViscousFlux() ) {}

  BCmitAVSensorBurgers( const Function_ptr& uexact, const QInterpret& qInterpret, const ViscousFlux& visc ) :
    uexact_(uexact),
    qInterpret_(qInterpret),
    visc_(visc)
  {
    //Nothing
  }

  virtual ~ BCmitAVSensorBurgers() {}

  BCmitAVSensorBurgers& operator=( const BCmitAVSensorBurgers& );

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return true; }

  // BC state vector
  template <class T, class L, class R>
  void state( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
              const Real& nx,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    qB = (*uexact_)( x, time);
  }

  // normal BC flux
  template <class L, class R, class T, class Tf>
  void fluxNormal( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
                   const Real& nx,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qB2,
                   ArrayQ<Tf>& Fn) const
  {
    Real qB, qBx, qBt;
    (*uexact_).gradient(x, time, qB, qBx, qBt);

    Real uB;
    qInterpret_.masterState(qB, uB);

    Real fx = 0.5*uB*uB;

    Fn += fx*nx;

    Real kxx = 0;
    visc_.diffusionViscous(x, time, uB, qBx, kxx);

    Real fv = kxx*qBx;

    Fn -= fv*nx;
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const Function_ptr uexact_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const ViscousFlux& visc_;
};

//----------------------------------------------------------------------------//
// Solution BC:  A u + B (kn un) = bcdata, where u = uexact(x,time)
//
template<>
struct BCmitAVSensorBurgersParams< PhysD1, BCTypeFunctionLinearRobin_mitLG > : BCmitAVSensorBurgersSolution1DParams
{
  const ParameterNumeric<Real> A{"A", 1.0, NO_RANGE, "Value coefficient"};
  const ParameterNumeric<Real> B{"B", 0.0, NO_RANGE, "Viscous coefficient"};

  static constexpr const char* BCName{"FunctionLinearRobin_mitLG"};
  struct Option
  {
    const DictOption FunctionLinearRobin_mitLG{ BCmitAVSensorBurgersParams::BCName, BCmitAVSensorBurgersParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCmitAVSensorBurgersParams params;
};

template<>
struct BCmitAVSensorBurgersParams< PhysD1, BCTypeFunctionLinearRobin_sansLG > : BCmitAVSensorBurgersSolution1DParams
{
  const ParameterNumeric<Real> A{"A", 1.0, NO_RANGE, "Value coefficient"};
  const ParameterNumeric<Real> B{"B", 0.0, NO_RANGE, "Viscous coefficient"};

  static constexpr const char* BCName{"FunctionLinearRobin_sansLG"};
  struct Option
  {
    const DictOption FunctionLinearRobin_sansLG{ BCmitAVSensorBurgersParams::BCName, BCmitAVSensorBurgersParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCmitAVSensorBurgersParams params;
};

template <class PhysDim>
class BCmitAVSensorBurgers_SolutionBase;

template <>
class BCmitAVSensorBurgers_SolutionBase<PhysD1>
{
public:
  typedef PhysD1 PhysDim;
   static const int D = AdvectionDiffusionTraits<PhysD1>::D;   // physical dimensions
   static const int N = AdvectionDiffusionTraits<PhysD1>::N;   // total solution variables

   static const int NBC = 1;                   // total BCs

   template <class T>
   using ArrayQ = AdvectionDiffusionTraits<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

   template <class T>
   using MatrixQ = AdvectionDiffusionTraits<PhysD1>::template MatrixQ<T>; // matrices

   typedef std::shared_ptr<Function1DBase<ArrayQ<Real>>> Function_ptr;

   //cppcheck-suppress noExplicitConstructor
   BCmitAVSensorBurgers_SolutionBase( const Function_ptr& uexact, const ViscousFlux1DBase& visc, Real A = 1, Real B = 0 ) :
       uexact_(uexact), visc_(visc), A_(A), B_(B) {}

   virtual ~ BCmitAVSensorBurgers_SolutionBase() {}

   // BC coefficients:  A u + B (kn un + ks us)
   template <class T, class L, class R>
   void coefficients(
       const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time, const Real& nx, MatrixQ<T>& A, MatrixQ<T>& B ) const;

   // BC data
   template <class T, class L, class R>
   void data( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time, const Real& nx, ArrayQ<T>& bcdata ) const;

   // is the boundary state valid
   bool isValidState( const Real& nx, const ArrayQ<Real>& q ) const { return true; }

   void dump( int indentSize, std::ostream& out = std::cout ) const;

 private:
   const Function_ptr uexact_;
   const ViscousFlux1DBase& visc_;
   Real A_, B_;
};

template <class T, class L, class R>
inline void
 BCmitAVSensorBurgers_SolutionBase< PhysD1 >::coefficients(
     const ParamTuple<L, R, TupleClass<>>& param, const Real&, const Real&, const Real&, MatrixQ<T>& A, MatrixQ<T>& B ) const
{
  A = A_;
  B = B_;
}

template <class T, class L, class R>
inline void
 BCmitAVSensorBurgers_SolutionBase<PhysD1 >::data(
     const ParamTuple<L, R, TupleClass<>>& param, const Real& x,const Real& time, const Real& nx, ArrayQ<T>& bcdata ) const
{
  Real u, ux, ut;
  uexact_->gradient(x, time, u, ux, ut);

  Real kxx = 0;
  visc_.diffusionViscous(x, time, u, ux, kxx);

  bcdata = A_*u + B_*kxx*ux*nx;
}

template <>
class BCmitAVSensorBurgers<PhysD1, BCTypeFunctionLinearRobin_mitLG> :
  public BCType< BCmitAVSensorBurgers<PhysD1, BCTypeFunctionLinearRobin_mitLG> >,
  public BCmitAVSensorBurgers_SolutionBase<PhysD1>
{
public:
  typedef BCmitAVSensorBurgers_SolutionBase<PhysD1> BaseType;
  typedef BCCategory::LinearScalar_mitLG Category;
  typedef BCmitAVSensorBurgersParams<PhysD1, BCTypeFunctionLinearRobin_mitLG> ParamsType;

  template<class QType, class ViscousFlux, class Source>
  BCmitAVSensorBurgers(const PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>& pde,
                               PyDict& d) :
    BaseType( ParamsType::params.getSolution(d), pde.getViscousFlux(),
    d.get(ParamsType::params.A), d.get(ParamsType::params.B)) {}

  BCmitAVSensorBurgers( const Function_ptr& uexact, const ViscousFlux1DBase& visc, Real A = 1, Real B = 0 ) :
    BaseType( uexact, visc, A, B ) {}
};

template <>
class BCmitAVSensorBurgers<PhysD1, BCTypeFunctionLinearRobin_sansLG> :
  public BCType< BCmitAVSensorBurgers<PhysD1, BCTypeFunctionLinearRobin_sansLG> >,
  public BCmitAVSensorBurgers_SolutionBase<PhysD1>
{
public:
  typedef BCmitAVSensorBurgers_SolutionBase<PhysD1> BaseType;
  typedef BCCategory::LinearScalar_sansLG Category;
  typedef BCmitAVSensorBurgersParams<PhysD1, BCTypeFunctionLinearRobin_sansLG> ParamsType;

  template<class QType, class ViscousFlux, class Source>
  BCmitAVSensorBurgers(const PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>& pde,
                               PyDict& d) :
    BaseType( ParamsType::params.getSolution(d), pde.getViscousFlux(),
    d.get(ParamsType::params.A), d.get(ParamsType::params.B)) {}

  BCmitAVSensorBurgers( const Function_ptr& uexact, const ViscousFlux1DBase& visc, Real A = 1, Real B = 0 ) :
    BaseType( uexact, visc, A, B ) {}
};

} //namespace SANS

#endif  // BCARTIFICIALVISCOSITYBURGERS1D_H
