// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDEBURGERS_ARTIFICIALVISCOSITY1D_H
#define PDEBURGERS_ARTIFICIALVISCOSITY1D_H

// 1D Burgers PDE class with artificial viscosity

#include "tools/SANSnumerics.h"     // Real
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Exp.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/AdvectionDiffusion/AdvectionDiffusion_Traits.h"

#include "Surreal/PromoteSurreal.h"

#include "Topology/ElementTopology.h"
#include "Topology/Dimension.h"

#include "Burgers_ArtificialViscosity_fwd.h"
#include "PDEBurgers1D.h"

#include "Field/Tuple/ParamTuple.h"
#include "tools/Tuple.h"
#include "tools/smoothmath.h"

#include <iostream>
#include <string>

namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: 1-D Burgers with artificial viscosity
//
// template parameters:
//   T                           solution DOF data type (e.g. double)
//
// member functions:
//   .hasFluxAdvectiveTime        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//   .hasFluxViscous             T/F: PDE has viscous flux term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: dU(Q)/dQ
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .fluxViscous                viscous fluxes: Fv(Q, Qx)
//   .diffusionViscous           viscous diffusion coefficient: d(Fv)/d(Ux)
//   .isValidState               T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom                 set from primitive variable array
//----------------------------------------------------------------------------//

template<class QType, class ViscousFlux, class Source>
class PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>
{
public:
  typedef PhysD1 PhysDim;
  static const int D = AdvectionDiffusionTraits<PhysD1>::D;   // physical dimensions
  static const int N = AdvectionDiffusionTraits<PhysD1>::N;   // total solution variables
  static const int Nparam = 1;

//  typedef Q1D<QType> QInterpret;            // solution variable interpreter type
  typedef QType QInterpret;            // solution variable interpreter type

  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD1>::template MatrixQ<T>; // matrices

  template<class T>
  using VectorArrayQ       = DLA::VectorS<D, ArrayQ<T> >;       // vector of solution arrays, i.e. solution gradients/flux vector

  template<class T>
  using VectorMatrixQ      = DLA::VectorS<D, MatrixQ<T> >;      // vector of jacobians, i.e. flux jacobians d(F)/d(U)

  template <class T>
  using MatrixParam = T;         // e.g. jacobian of PDEs w.r.t. parameters
//  using MatrixParam = DLA::MatrixS<N,Nparam,T>;         // e.g. jacobian of PDEs w.r.t. parameters


  explicit PDEBurgers_ArtificialViscosity(const int order, const PDEBurgers<PhysD1, QType, ViscousFlux, Source>& pde) :
    order_(order),
    pde_(pde)
  {
    // Nothing
  }

  ~PDEBurgers_ArtificialViscosity() {}

  PDEBurgers_ArtificialViscosity& operator=( const PDEBurgers_ArtificialViscosity& ) = delete;

  // flux components
  bool hasFluxAdvectiveTime() const { return true; }
  bool hasFluxAdvective() const { return pde_.hasFluxAdvective(); }
  bool hasFluxViscous() const { return true; }
  bool hasSource() const { return pde_.hasSource(); }
  bool hasSourceTrace() const { return pde_.hasSourceTrace(); }
  bool hasSourceLiftedQuantity() const { return false; }
  bool hasForcingFunction() const { return pde_.hasForcingFunction(); }

  // viscous flux is of the form K(q).gradu
  bool fluxViscousLinearInGradient() const { return true; }
  bool needsSolutionGradientforSource() const { return pde_.needsSolutionGradientforSource(); }

  const ViscousFlux& getViscousFlux() const { return pde_.getViscousFlux(); }
  const QInterpret& variableInterpreter() const { return pde_.variableInterpreter(); }

  // unsteady temporal flux: Ft(Q)
  template <class T, class Tf, class L, class R>
  void fluxAdvectiveTime(
      const ParamTuple<L, R, TupleClass<>>& param,
      const Real& x, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tf>& ft ) const
  {
    pde_.fluxAdvectiveTime(x, time, q, ft);
  }

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <class T, class L, class R>
  void jacobianFluxAdvectiveTime(
      const ParamTuple<L, R, TupleClass<>>& param,
      const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& J ) const
  {
    pde_.jacobianFluxAdvectiveTime(x, time, q, J);
  }

  // master state: U(Q)
  template <class T, class L, class R, class Tu>
  void masterState(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tu>& uCons ) const;

  // jacobian of master state wrt q: dU(Q)/dQ
  template<class L, class R, class T>
  void jacobianMasterState(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const;

  // unsteady conservative flux: d(U)/d(t)
  template <class L, class R, class T>
  void strongFluxAdvectiveTime(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& dudt ) const;

  // advective flux: F(Q)
  template <class T, class L, class R, class Ts>
  void fluxAdvective(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Ts>& f ) const;

  template <class T, class L, class R, class Ts>
  void fluxAdvectiveUpwind(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, ArrayQ<Ts>& f ) const;

  template <class T, class L, class R, class Tf>
  void fluxAdvectiveUpwindSpaceTime(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const;

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class T, class L, class R>
  void jacobianFluxAdvective(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x,  const Real& time, const ArrayQ<T>& q,
      MatrixQ<T>& ax ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class T, class L, class R>
  void jacobianFluxAdvectiveAbsoluteValue(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, const Real& nx,
      MatrixQ<T>& a ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class T, class L, class R>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time, const ArrayQ<T>& q, const Real& nx, const Real& nt,
      MatrixQ<T>& a ) const;

  // strong form advective fluxes
  template <class T, class L, class R>
  void strongFluxAdvective(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx,
      ArrayQ<T>& strongPDE ) const;

  // viscous flux: Fv(Q, Qx)
  template <class Tq, class Tqg, class Th, class Tg, class Tf>
  void fluxViscous(
      const ParamTuple<Th, Tg, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqg>& qx,
      ArrayQ<Tf>& f ) const;

  // space-time viscous flux: Fv(Q, QX)
  template <class Tq, class Tqg, class Th, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const ParamTuple<Th, Tg, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqg>& qx, const ArrayQ<Tqg>& qt, ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const;

  template <class Tq, class Tqg, class Th, class Tg, class Tf>
  void fluxViscous(
      const ParamTuple<Th, Tg, TupleClass<>>& paramL,
      const ParamTuple<Th, Tg, TupleClass<>>& paramR,
      const Real& x, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tqg>& qxL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tqg>& qxR,
      const Real& nx, ArrayQ<Tf>& f ) const;

  // space-time viscous flux: Fv(Q, QX)
  template <class Tq, class Tqg, class Th, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const ParamTuple<Th, Tg, TupleClass<>>& paramL,
      const ParamTuple<Th, Tg, TupleClass<>>& paramR,
      const Real& x, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tqg>& qxL, const ArrayQ<Tqg>& qtL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tqg>& qxR, const ArrayQ<Tqg>& qtR,
      const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const;

  // viscous diffusion matrix: d(Fv)/d(Ux)
  template <class Th, class Ts, class Tq, class Tg, class Tk>
  void diffusionViscous(
      const ParamTuple<Th, Ts, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Tk>& kxx) const;

  // space-time viscous diffusion matrix: d(Fv)/d(UX)
  template <class Th, class Ts, class Tq, class Tg, class Tk>
  void diffusionViscousSpaceTime(
      const ParamTuple<Th, Ts, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxt,
      MatrixQ<Tk>& ktx, MatrixQ<Tk>& ktt ) const;

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class T, class L, class R>
  void jacobianFluxViscous(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& ax ) const {}

  // strong form viscous fluxes
  template <class T, class L, class R>
  void strongFluxViscous(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx,
      const ArrayQ<T>& qxx,
      ArrayQ<T>& strongPDE ) const;

  // solution-dependent source: S(x, Q, Qx)
  template <class Tq, class Tg, class L, class R, class Ts>
  void source(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Ts>& source ) const;

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX)
  template <class Tp, class Tlq, class Tq, class Tg, class Ts>
  void source(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const Tlq& lifted_quantity, const ArrayQ<Tg>& qx,
      ArrayQ<Ts>& source ) const
  {
    pde_.source( x, time, lifted_quantity, q, qx, source );
  }

  // dual-consistent source
  template <class T, class L, class R, class Tg, class Ts>
  void sourceTrace(
      const ParamTuple<L, R, TupleClass<>>& paramL, const Real& xL,
      const ParamTuple<L, R, TupleClass<>>& paramR, const Real& xR,
      const Real& time,
      const ArrayQ<T>& qL, const ArrayQ<Tg>& qxL,
      const ArrayQ<T>& qR, const ArrayQ<Tg>& qxR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const;

  // trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
  template <class Tp, class Tq, class Ts>
  void sourceLiftedQuantity(
      const Tp& paramL, const Real& xL,
      const Tp& paramR, const Real& xR,
      const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      Ts& s ) const
  {
    pde_.source( xL, xR, time, qL, qR, s );
  }

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class T, class L, class R>
  void jacobianSource(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx,
      MatrixQ<T>& dsdu ) const;

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class T, class L, class R>
  void jacobianSourceAbsoluteValue(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx,
      MatrixQ<T>& dsdu ) const;

  // right-hand-side forcing function
  template <class T, class L, class R>
  void forcingFunction( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time, ArrayQ<T>& source ) const;

  // characteristic speed (needed for timestep)
  template <class T, class L, class R>
  void speedCharacteristic(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
      const Real& dx, const ArrayQ<T>& q, T& speed ) const;

  // characteristic speed
  template <class T, class L, class R>
  void speedCharacteristic(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, T& speed ) const;

  // update fraction needed for physically valid state
  template <class L, class R>
  void updateFraction(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time, const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
      const Real maxChangeFraction, Real& updateFraction ) const;

  // is state physically valid
  template <class T>
  bool isValidState( const ArrayQ<T>& q ) const;

  // set from primitive variable array
  template <class T>
  void setDOFFrom(
      ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const;

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const;

  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const;

  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const;

  // how many residual equations are we dealing with
  int nMonitor() const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

  // Artificial Viscosity Calculation
  template <class T, class Th, class Tg, class Ts>
  void artificialViscosity( const Real& x, const Real& time, const ArrayQ<T>& q, const Th& h, const Tg& sensor,
                            MatrixQ< Ts >& Kxx ) const;

  // artificial viscosity calculation with generalized h-tensor
  template <class T, class Th, class Tg, class Ts>
  void artificialViscosity( const Real& x, const Real& time, const ArrayQ<T>& q, const DLA::MatrixSymS<D,Th>& logH, const Tg& sensor,
                            MatrixQ<Ts>& Kxx ) const;

  template <class T, class Th, class Tg, class Ts>
  void artificialViscositySpaceTime( const Real& x, const Real& time, const ArrayQ<T>& q,
                                     const DLA::MatrixSymS<D+1,Th>& logH, const Tg& sensor,
                                     MatrixQ<Ts>& Kxx, MatrixQ<Ts>& Kxt, MatrixQ<Ts>& Ktx, MatrixQ<Ts>& Ktt ) const;

  int getOrder() const { return order_; };

private:
  const int order_;
  const PDEBurgers<PhysD1, QType, ViscousFlux, Source>& pde_;
};

// Artificial Viscosity Calculation
template<class QType, class ViscousFlux, class Source>
  template <class T, class Th, class Tg, class Ts>
void inline
PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>::
artificialViscosity( const Real& x, const Real& time, const ArrayQ<T>& q, const Th& h, const Tg& sensor,
                            MatrixQ< Ts >& Kxx ) const
{
  T lambda;
  Real C = 1.0;

  pde_.speedCharacteristic( x, time, q, lambda );

  Kxx = C*h/(Real(order_)+1) * lambda * smoothabs0(sensor, 1.0e-5);
}

// Artificial viscosity calculation with generalized h-tensor
template<class QType, class ViscousFlux, class Source>
template <class T, class Th, class Tg, class Ts>
void inline
PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>::artificialViscosity(
    const Real& x, const Real& time, const ArrayQ<T>& q, const DLA::MatrixSymS<D,Th>& logH, const Tg& sensor,
    MatrixQ<Ts>& Kxx ) const
{
  T lambda;
  Real C = 1.0;

  pde_.speedCharacteristic( x, time, q, lambda );

  Ts factor = C/(Real(order_)+1) * lambda * smoothabs0(sensor, 1.0e-5);

  DLA::MatrixSymS<D,Th> H = exp(logH); //generalized h-tensor

  Kxx += factor*H(0,0);
}

// Space-time artificial viscosity calculation with generalized h-tensor
template<class QType, class ViscousFlux, class Source>
template <class T, class Th, class Tg, class Ts>
void inline
PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>::artificialViscositySpaceTime(
    const Real& x, const Real& time, const ArrayQ<T>& q,
    const DLA::MatrixSymS<D+1,Th>& logH, const Tg& sensor,
    MatrixQ<Ts>& Kxx, MatrixQ<Ts>& Kxt, MatrixQ<Ts>& Ktx, MatrixQ<Ts>& Ktt ) const
{
  T lambda;
  Real C = 1.0;

  pde_.speedCharacteristic( x, time, q, lambda );

  Ts factor = C/(Real(order_)+1) * smoothabs0(sensor, 1.0e-5);

  DLA::MatrixSymS<D+1,Th> H = exp(logH); //generalized h-tensor

  Kxx += factor*lambda*H(0,0); // [length^2 / time]
  Kxt += factor*H(0,1); // [length]
  Ktx += factor*H(1,0); // [length]
  Ktt += factor*H(1,1); // [time]
}

template<class QType, class ViscousFlux, class Source>
template <class T, class L, class R, class Tu>
inline void
PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>::masterState(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<T>& q, ArrayQ<Tu>& uCons ) const
{
  pde_.masterState(x, time, q, uCons);
}

template<class QType, class ViscousFlux, class Source>
template <class L, class R, class T>
inline void
PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>::jacobianMasterState(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
{
   pde_.jacobianMasterState(x, time, q, dudq);
}


template<class QType, class ViscousFlux, class Source>
template <class L, class R, class T>
inline void
PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>::strongFluxAdvectiveTime(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& dudt ) const
{
  pde_.strongFluxAdvectiveTime(x, time, q, qt, dudt);
}

template<class QType, class ViscousFlux, class Source>
template <class T, class L, class R, class Ts>
inline void
PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>::fluxAdvective(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<T>& q, ArrayQ<Ts>& f ) const
{

  pde_.fluxAdvective(x, time, q, f);
}

template<class QType, class ViscousFlux, class Source>
template <class T, class L, class R, class Ts>
inline void
PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>::fluxAdvectiveUpwind(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx,
    ArrayQ<Ts>& f ) const
{

  pde_.fluxAdvectiveUpwind(x, time, qL, qR, nx, f);
}

template<class QType, class ViscousFlux, class Source>
template <class T, class L, class R, class Tf>
inline void
PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>::fluxAdvectiveUpwindSpaceTime(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx, const Real& nt,
    ArrayQ<Tf>& f ) const
{

  pde_.fluxAdvectiveUpwindSpaceTime(x, time, qL, qR, nx, nt, f);
}


// advective flux jacobian: d(F)/d(U)
template<class QType, class ViscousFlux, class Source>
template <class T, class L, class R>
inline void
PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>::jacobianFluxAdvective(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time, const ArrayQ<T>& q,
    MatrixQ<T>& ax ) const
{

  pde_.jacobianFluxAdvective( x, time, q, ax );
}


// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template<class QType, class ViscousFlux, class Source>
template <class T, class L, class R>
inline void
PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>::jacobianFluxAdvectiveAbsoluteValue(
  const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
  const ArrayQ<T>& q, const Real& nx, MatrixQ<T>& mtx ) const
{

  pde_.jacobianFluxAdvectiveAbsoluteValue( x, time, q, nx, mtx );
}


// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template<class QType, class ViscousFlux, class Source>
template <class T, class L, class R>
inline void
PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>::jacobianFluxAdvectiveAbsoluteValueSpaceTime(
  const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
  const ArrayQ<T>& q, const Real& nx, const Real& nt, MatrixQ<T>& mtx ) const
{

  pde_.jacobianFluxAdvectiveAbsoluteValue( x, time, q, nx, nt, mtx );
}


// strong form of advective flux: d(F)/d(x)
template<class QType, class ViscousFlux, class Source>
template <class T, class L, class R>
inline void
PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>::strongFluxAdvective(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qx,
    ArrayQ<T>& strongPDE ) const
{

  pde_.strongFluxAdvective( x, time, q, qx, strongPDE );
}


// viscous flux
template<class QType, class ViscousFlux, class Source>
template <class Tq, class Tqg, class Th, class Tg, class Tf>
inline void
PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>::fluxViscous(
    const ParamTuple<Th, Tg, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tqg>& qx,
    ArrayQ<Tf>& f ) const
{
  typedef typename promote_Surreal<Tq,Tqg>::type T;

  MatrixQ<Tf> nu_xx = 0.0;
  MatrixQ<Tq> dudq = 0;

  const Th& Hparam = param.left();
  const Tg& sensor = param.right();

  // Add artificial viscosity
  artificialViscosity( x, time, q, Hparam, sensor, nu_xx );

  pde_.jacobianMasterState( x, time, q, dudq );
  ArrayQ<T> ux = dudq*qx;

  f -= nu_xx*ux;

  pde_.fluxViscous( x, time, q, qx, f );
}

// space-time viscous flux
template<class QType, class ViscousFlux, class Source>
template <class Tq, class Tqg, class Th, class Tg, class Tf>
inline void
PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>::fluxViscousSpaceTime(
    const ParamTuple<Th, Tg, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tqg>& qx, const ArrayQ<Tqg>& qt,
    ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
{
  typedef typename promote_Surreal<Tq,Tqg>::type T;

  MatrixQ<Tf> nu_xx = 0.0, nu_xt = 0.0;
  MatrixQ<Tf> nu_tx = 0.0, nu_tt = 0.0;
  MatrixQ<Tq> dudq = 0;

  const Th& Hparam = param.left();
  const Tg& sensor = param.right();

  // Add space-time artificial viscosity
  artificialViscositySpaceTime( x, time, q, Hparam, sensor,
                                nu_xx, nu_xt, nu_tx, nu_tt );

  pde_.jacobianMasterState( x, time, q, dudq );
  ArrayQ<T> ux = dudq*qx;
  ArrayQ<T> ut = dudq*qt;

  f -= nu_xx*ux + nu_xt*ut;
  g -= nu_tx*ux + nu_tt*ut;

  pde_.fluxViscousSpaceTime( x, time, q, qx, qt, f, g );
}

// space-time viscous flux: normal flux with left and right states
template<class QType, class ViscousFlux, class Source>
template <class Tq, class Tqg, class Th, class Tg, class Tf>
inline void
PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>::fluxViscousSpaceTime(
    const ParamTuple<Th, Tg, TupleClass<>>& paramL,
    const ParamTuple<Th, Tg, TupleClass<>>& paramR,
    const Real& x, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tqg>& qxL, const ArrayQ<Tqg>& qtL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tqg>& qxR, const ArrayQ<Tqg>& qtR,
    const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const
{
  ArrayQ<Tf> fL = 0, fR = 0;
  ArrayQ<Tf> gL = 0, gR = 0;

  fluxViscousSpaceTime(paramL, x, time, qL, qxL, qtL, fL, gL);
  fluxViscousSpaceTime(paramR, x, time, qR, qxR, qtR, fR, gR);

  f += 0.5*(fL + fR)*nx + 0.5*(gL + gR)*nt;
}

// viscous flux: normal flux with left and right states
template<class QType, class ViscousFlux, class Source>
template <class Tq, class Tqg, class Th, class Tg, class Tf>
inline void
PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>::fluxViscous(
    const ParamTuple<Th, Tg, TupleClass<>>& paramL,
    const ParamTuple<Th, Tg, TupleClass<>>& paramR,
    const Real& x, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tqg>& qxL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tqg>& qxR,
    const Real& nx, ArrayQ<Tf>& f ) const
{
  ArrayQ<Tf> fL = 0, fR = 0;

  fluxViscous(paramL, x, time, qL, qxL, fL);
  fluxViscous(paramR, x, time, qR, qxR, fR);

  f += 0.5*(fL + fR)*nx;
}

// viscous diffusion matrix: d(Fv)/d(Ux)
template<class QType, class ViscousFlux, class Source>
template <class Th, class Ts, class Tq, class Tg, class Tk>
inline void
PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>::diffusionViscous(
    const ParamTuple<Th, Ts, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    MatrixQ<Tk>& kxx ) const
{
  const Th& Hparam = param.left();
  const Ts& sensor = param.right();

  // Add artificial viscosity
  artificialViscosity( x, time, q, Hparam, sensor, kxx );

  pde_.diffusionViscous( x, time, q, qx, kxx );
}

// space-time viscous diffusion matrix: d(Fv)/d(UX)
template <class QType, class ViscousFlux, class Source>
template <class Th, class Ts, class Tq, class Tg, class Tk>
inline void
PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>::diffusionViscousSpaceTime(
    const ParamTuple<Th, Ts, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
    MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxt,
    MatrixQ<Tk>& ktx, MatrixQ<Tk>& ktt ) const
{
  const Th& Hparam = param.left();
  const Ts& sensor = param.right();

  // Add space-time artificial viscosity
  artificialViscositySpaceTime( x, time, q, Hparam, sensor,
                                kxx, kxt, ktx, ktt );

  pde_.diffusionViscousSpaceTime( x, time, q, qx, qt, kxx, kxt, ktx, ktt );
}

// strong form of viscous flux: d(Fv)/d(x)
template<class QType, class ViscousFlux, class Source>
template <class T, class L, class R>
inline void
PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>::strongFluxViscous(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qx,
    const ArrayQ<T>& qxx,
    ArrayQ<T>& strongPDE ) const
{
  SANS_DEVELOPER_EXCEPTION("PDEBurgers_ArtificialViscosity<NDPDE>::strongFluxViscous is not implemented");

  pde_.strongFluxViscous( x, time, q, qx, qxx, strongPDE );
}

// solution-dependent source: S(x, Q, Qx)
template<class QType, class ViscousFlux, class Source>
template <class Tq, class Tg, class L, class R, class Ts>
inline void
PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>::source(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    ArrayQ<Ts>& source ) const
{

  pde_.source( x, time, q, qx, source );
}

// dual-consistent source
template<class QType, class ViscousFlux, class Source>
template <class T, class L, class R, class Tg, class Ts>
inline void
PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>::sourceTrace(
    const ParamTuple<L, R, TupleClass<>>& paramL, const Real& xL,
    const ParamTuple<L, R, TupleClass<>>& paramR, const Real& xR,
    const Real& time,
    const ArrayQ<T>& qL, const ArrayQ<Tg>& qxL,
    const ArrayQ<T>& qR, const ArrayQ<Tg>& qxR,
    ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
{
  pde_.sourceTrace(xL, xR, time, qL, qxL, qR, qxR, sourceL, sourceR);
}

// jacobian of source wrt conservation variables: d(S)/d(U)
template<class QType, class ViscousFlux, class Source>
template <class T, class L, class R>
inline void
PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>::jacobianSource(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qx,
    MatrixQ<T>& dsdu ) const
{

  pde_.jacobianSource(x, time, q, qx, dsdu);
}

// jacobian of source wrt conservation variables: d(S)/d(U)
template<class QType, class ViscousFlux, class Source>
template <class T, class L, class R>
inline void
PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>::jacobianSourceAbsoluteValue(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qx,
    MatrixQ<T>& dsdu ) const
{

  pde_.jacobianSourceAbsoluteValue(x, time, q, qx, dsdu);
}

// right-hand-side forcing function
template<class QType, class ViscousFlux, class Source>
template <class T, class L, class R>
inline void
PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>::forcingFunction(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time, ArrayQ<T>& forcing ) const
{
  pde_.forcingFunction(x, time, forcing );
}


// characteristic speed (needed for timestep)
template<class QType, class ViscousFlux, class Source>
template <class T, class L, class R>
inline void
PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>::speedCharacteristic(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
    const Real& dx, const ArrayQ<T>& q, T& speed ) const
{

  pde_.speedCharacteristic(x, time, dx, q, speed);
}


// characteristic speed
template<class QType, class ViscousFlux, class Source>
template <class T, class L, class R>
inline void
PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>::speedCharacteristic(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<T>& q, T& speed ) const
{

  pde_.speedCharacteristic(x, time, q, speed);
}


// update fraction needed for physically valid state
template<class QType, class ViscousFlux, class Source>
template <class L, class R>
inline void
PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>::updateFraction(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time, const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
    const Real maxChangeFraction, Real& updateFraction ) const
{
  pde_.updateFraction(x, time, q, dq, maxChangeFraction, updateFraction );
}


// is state physically valid
template<class QType, class ViscousFlux, class Source>
template <class T>
inline bool
PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>::isValidState( const ArrayQ<T>& q ) const
{
  return pde_.isValidState(q);
}


// set from primitive variable array
template<class QType, class ViscousFlux, class Source>
template <class T>
void
PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>::setDOFFrom(
    ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const
{
  pde_.setDOFFrom(q, data, name, nn);
}

// interpret residuals of the solution variable
template<class QType, class ViscousFlux, class Source>
template <class T>
void
PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>::interpResidVariable(
    const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
{
  pde_.interpResidVariable(rsdPDEIn, rsdPDEOut);
}

// interpret residuals of the gradients in the solution variable
template<class QType, class ViscousFlux, class Source>
template <class T>
void
PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>::interpResidGradient(
    const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const
{
  pde_.interpResidGradient(rsdAuxIn, rsdAuxOut);
}

// interpret residuals at the boundary (should forward to BCs)
template<class QType, class ViscousFlux, class Source>
template <class T>
void
PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>::interpResidBC(
    const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const
{
  pde_.interpResidBC(rsdBCIn, rsdBCOut);
}

// how many residual equations are we dealing with
template<class QType, class ViscousFlux, class Source>
inline int
PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>::nMonitor() const
{
  return pde_.nMonitor();
}

template<class QType, class ViscousFlux, class Source>
void
PDEBurgers_ArtificialViscosity<PhysD1, QType, ViscousFlux, Source>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PDEBurgers_ArtificialViscosity1D: pde_ =" << std::endl;
  pde_.dump(indentSize+2, out);
}

} //namespace SANS

#endif  // PDEBURGERS_ARTIFICIALVISCOSITY1D_H
