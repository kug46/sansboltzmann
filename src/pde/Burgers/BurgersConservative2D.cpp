// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BurgersConservative2D.h"

namespace SANS
{

// is state physically valid: check for positive density, temperature
bool
BurgersConservative2D::
isValidState( const ArrayQ<Real>& q ) const
{
  return true;
}

void
BurgersConservative2D::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BurgersConservative1D" << std::endl;
}

}
