// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCBURGERS1D_H
#define BCBURGERS1D_H

// 1-D Burgers class

//PyDict must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/BCCategory.h"
#include "pde/BCNone.h"
#include "Topology/Dimension.h"
#include "pde/AdvectionDiffusion/AdvectionDiffusion_Traits.h"
#include "pde/Burgers/PDEBurgers1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AdvectionDiffusion/ViscousFlux1D.h"

#include <iostream>

#include <boost/mpl/vector.hpp>

namespace SANS
{

//----------------------------------------------------------------------------//
// BC class: 1-D Burgers
//
// template parameters:
//   PhysDim              Physical dimensions
//   BCType               BC type (e.g. BCTypeLinearRobin)
//----------------------------------------------------------------------------//

template <class PhysDim, class BCType>
class BCBurgers;

template <class PhysDim, class BCType>
struct BCBurgersParams;

//----------------------------------------------------------------------------//
// BC types
//----------------------------------------------------------------------------//

class BCTypeLinearRobin_mitLG;
class BCTypeLinearRobin_sansLG;

template<class QType, class ViscousFlux, class Source>
class BCTypeDirichlet_mitState;

template<class QType, class ViscousFlux, class Source>
class BCTypeDirichlet_mitStateSpaceTime;

template<class QType, class ViscousFlux, class Source>
class BCTypeFunction_mitState;
class BCTypeFunctionLinearRobin_mitLG;
class BCTypeFunctionLinearRobin_sansLG;


// Define the vector of boundary conditions
template<class QType, class ViscousFlux, class Source>
using BCBurgers1DVector = boost::mpl::vector8< BCNone<PhysD1,AdvectionDiffusionTraits<PhysD1>::N>,
                                               BCBurgers<PhysD1, BCTypeLinearRobin_mitLG>,
                                               BCBurgers<PhysD1, BCTypeLinearRobin_sansLG>,
                                               BCBurgers<PhysD1, BCTypeDirichlet_mitState<QType,ViscousFlux,Source>>,
                                               SpaceTimeBC< BCBurgers<PhysD1, BCTypeDirichlet_mitStateSpaceTime<QType,ViscousFlux,Source>> >,
                                               BCBurgers<PhysD1, BCTypeFunction_mitState<QType,ViscousFlux,Source>>,
                                               BCBurgers<PhysD1, BCTypeFunctionLinearRobin_mitLG>,
                                               BCBurgers<PhysD1, BCTypeFunctionLinearRobin_sansLG>
                                             >;

//----------------------------------------------------------------------------//
// Robin BC:  A u + B (kn un + ks us) = bcdata
//
// coefficients A and B are arbitrary with the exception that they cannot
// simultaneously vanish
template<>
struct BCBurgersParams<PhysD1, BCTypeLinearRobin_mitLG> : noncopyable
{
  const ParameterNumeric<Real> A{"A", NO_DEFAULT, NO_RANGE, "Value coefficient"};
  const ParameterNumeric<Real> B{"B", NO_DEFAULT, NO_RANGE, "Viscous coefficient"};
  const ParameterNumeric<Real> bcdata{"bcdata", NO_DEFAULT, NO_RANGE, "BC data"};

  static constexpr const char* BCName{"LinearRobin_mitLG"};
  struct Option
  {
    const DictOption LinearRobin_mitLG{ BCBurgersParams::BCName, BCBurgersParams::checkInputs};
  };

  static void checkInputs(PyDict d);
  static BCBurgersParams params;
};

template<>
struct BCBurgersParams<PhysD1, BCTypeLinearRobin_sansLG> : noncopyable
{
  const ParameterNumeric<Real> A{"A", NO_DEFAULT, NO_RANGE, "Value coefficient"};
  const ParameterNumeric<Real> B{"B", NO_DEFAULT, NO_RANGE, "Viscous coefficient"};
  const ParameterNumeric<Real> bcdata{"bcdata", NO_DEFAULT, NO_RANGE, "BC data"};

  static constexpr const char* BCName{"LinearRobin_sansLG"};
  struct Option
  {
    const DictOption LinearRobin_sansLG{ BCBurgersParams::BCName, BCBurgersParams::checkInputs};
  };

  static void checkInputs(PyDict d);
  static BCBurgersParams params;
};

template <class PhysDim>
class BCBurgers_LinearRobinBase;

template <>
class BCBurgers_LinearRobinBase<PhysD1>
{
public:
  typedef PhysD1 PhysDim;
  static const int D = AdvectionDiffusionTraits<PhysD1>::D;   // physical dimensions
  static const int N = AdvectionDiffusionTraits<PhysD1>::N;   // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD1>::template MatrixQ<T>; // matrices

  BCBurgers_LinearRobinBase( const Real& A, const Real& B, const Real& bcdata ) :
    A_(A), B_(B), bcdata_(bcdata) {}

  virtual ~ BCBurgers_LinearRobinBase() {}

  BCBurgers_LinearRobinBase& operator=( const BCBurgers_LinearRobinBase& );

  // BC coefficients:  A u + B (kn un + ks us)
  template <class T>
  void coefficients(
      const Real& x, const Real& time, const Real& nx,
      MatrixQ<T>& A, MatrixQ<T>& B ) const;

  // BC data
  template <class T>
  void data( const Real& x, const Real& time, const Real& nx, ArrayQ<T>& bcdata ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  MatrixQ<Real> A_;
  MatrixQ<Real> B_;
  ArrayQ<Real> bcdata_;
};


template <class T>
inline void
BCBurgers_LinearRobinBase<PhysD1>::coefficients(
    const Real&, const Real& time, const Real&,
    MatrixQ<T>& A, MatrixQ<T>& B ) const
{
  A = A_;
  B = B_;
}


template <class T>
inline void
 BCBurgers_LinearRobinBase<PhysD1>::data( const Real&, const Real& time, const Real& nx, ArrayQ<T>& bcdata ) const
{
  bcdata = bcdata_;
}

template <>
class BCBurgers<PhysD1, BCTypeLinearRobin_mitLG> : public BCType< BCBurgers<PhysD1, BCTypeLinearRobin_mitLG> >,
                                                              public BCBurgers_LinearRobinBase<PhysD1>
{
public:
  typedef BCBurgers_LinearRobinBase<PhysD1> BaseType;
  typedef BCCategory::LinearScalar_mitLG Category;
  typedef BCBurgersParams<PhysD1, BCTypeLinearRobin_mitLG> ParamsType;

  template<class QType, class ViscousFlux, class Source>
  BCBurgers(const PDEBurgers<PhysD1, QType, ViscousFlux, Source>& pde, const PyDict& d ) :
    BaseType( d.get(ParamsType::params.A), d.get(ParamsType::params.B), d.get(ParamsType::params.bcdata)) {}

  BCBurgers( const Real& A, const Real& B, const Real& bcdata ) : BaseType( A, B, bcdata ) {}
};

template <>
class BCBurgers<PhysD1, BCTypeLinearRobin_sansLG> : public BCType< BCBurgers<PhysD1, BCTypeLinearRobin_sansLG> >,
                                                               public BCBurgers_LinearRobinBase<PhysD1>
{
public:
  typedef BCBurgers_LinearRobinBase<PhysD1> BaseType;
  typedef BCCategory::LinearScalar_sansLG Category;
  typedef BCBurgersParams<PhysD1, BCTypeLinearRobin_sansLG> ParamsType;

  template<class QType, class ViscousFlux, class Source>
  BCBurgers(const PDEBurgers<PhysD1, QType, ViscousFlux, Source>& pde, const PyDict& d ) :
    BaseType( d.get(ParamsType::params.A), d.get(ParamsType::params.B), d.get(ParamsType::params.bcdata)) {}

  BCBurgers( const Real& A, const Real& B, const Real& bcdata ) : BaseType( A, B, bcdata ) {}
};

//----------------------------------------------------------------------------//
// Dirichlet_mitState BC:  u = bcstate
//
class BCTypeDirichlet_mitStateParam;

template<>
struct BCBurgersParams<PhysD1, BCTypeDirichlet_mitStateParam> : noncopyable
{
  const ParameterNumeric<Real> qB{"qB", NO_DEFAULT, NO_RANGE, "BC state"};

  static constexpr const char* BCName{"Dirichlet_mitState"};
  struct Option
  {
    const DictOption Dirichlet_mitState{ BCBurgersParams::BCName, BCBurgersParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCBurgersParams params;
};

template <class QType, class ViscousFlux, class Source>
class BCBurgers<PhysD1, BCTypeDirichlet_mitState<QType, ViscousFlux, Source>> :
  public BCType< BCBurgers<PhysD1, BCTypeDirichlet_mitState<QType, ViscousFlux, Source>> >
{
public:
  typedef BCCategory::Flux_mitState Category;
  typedef BCBurgersParams<PhysD1, BCTypeDirichlet_mitStateParam> ParamsType;

  typedef PhysD1 PhysDim;
  static const int D = AdvectionDiffusionTraits<PhysD1>::D;   // physical dimensions
  static const int N = AdvectionDiffusionTraits<PhysD1>::N;   // total solution variables
  typedef QType QInterpret;

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD1>::template MatrixQ<T>; // matrices

  BCBurgers(const PDEBurgers<PhysD1, QType, ViscousFlux, Source>& pde, const PyDict& d ) :
    pde_(pde),
    qB_(d.get(ParamsType::params.qB)),
    qInterpret_(pde.variableInterpreter()),
    visc_( pde.getViscousFlux() ),
    fluxViscous_(pde.hasFluxViscous()) {}

  BCBurgers(const PDEBurgers<PhysD1, QType, ViscousFlux, Source>& pde, const ArrayQ<Real>& qB ) :
    pde_(pde),
    qB_(qB),
    qInterpret_(pde.variableInterpreter()),
    visc_( pde.getViscousFlux() ),
    fluxViscous_(pde.hasFluxViscous()) {}

  virtual ~ BCBurgers() {}

  BCBurgers& operator=( const BCBurgers& );

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return fluxViscous_; }

  // BC state vector
  template <class T>
  void state( const Real& x, const Real& time,
              const Real& nx,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    qB = qB_;
  }

  // normal BC flux
  template <class T>
  void fluxNormal( const Real& x, const Real& time,
                   const Real& nx,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const
  {
    // Add upwinded advective flux
    pde_.fluxAdvectiveUpwind(x, time, qI, qB, nx, Fn);

    ArrayQ<T> fv = 0;
    pde_.fluxViscous(x, time, qB, qIx, fv);

    // Add viscous flux
    Fn += fv*nx;
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "BCBurgers<PhysD1, BCTypeDirichlet_mitState>:" << std::endl;
    out << indent << "  qB_ = " << qB_ << std::endl;
  }

private:
  const PDEBurgers<PhysD1, QType, ViscousFlux, Source>& pde_;
  ArrayQ<Real> qB_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const ViscousFlux& visc_;
  bool fluxViscous_;
};

//----------------------------------------------------------------------------//
// Dirichlet_mitState BC for space-time:  u = bcstate
//
class BCTypeDirichlet_mitStateSpaceTimeParam;

template<>
struct BCBurgersParams<PhysD1, BCTypeDirichlet_mitStateSpaceTimeParam> : noncopyable
{
  const ParameterNumeric<Real> qB{"qB", NO_DEFAULT, NO_RANGE, "BC state"};

  static constexpr const char* BCName{"Dirichlet_mitStateSpaceTime"};
  struct Option
  {
    const DictOption Dirichlet_mitStateSpaceTime{ BCBurgersParams::BCName, BCBurgersParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCBurgersParams params;
};

template <class QType, class ViscousFlux, class Source>
class BCBurgers<PhysD1, BCTypeDirichlet_mitStateSpaceTime<QType, ViscousFlux, Source>> :
  public BCType< BCBurgers<PhysD1, BCTypeDirichlet_mitStateSpaceTime<QType, ViscousFlux, Source>> >
{
public:
  typedef BCCategory::Flux_mitState Category;
  typedef BCBurgersParams<PhysD1, BCTypeDirichlet_mitStateSpaceTimeParam> ParamsType;

  typedef PhysD1 PhysDim;
  static const int D = AdvectionDiffusionTraits<PhysD1>::D;   // physical dimensions
  static const int N = AdvectionDiffusionTraits<PhysD1>::N;   // total solution variables
  typedef QType QInterpret;

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD1>::template MatrixQ<T>; // matrices

  BCBurgers(const PDEBurgers<PhysD1, QType, ViscousFlux, Source>& pde, const PyDict& d ) :
    pde_(pde),
    qB_(d.get(ParamsType::params.qB)),
    qInterpret_(pde.variableInterpreter()),
    visc_( pde.getViscousFlux() ),
    fluxViscous_(pde.hasFluxViscous()) {}

  BCBurgers(const PDEBurgers<PhysD1, QType, ViscousFlux, Source>& pde, const ArrayQ<Real>& qB ) :
    pde_(pde),
    qB_(qB),
    qInterpret_(pde.variableInterpreter()),
    visc_( pde.getViscousFlux() ),
    fluxViscous_(pde.hasFluxViscous()) {}

  virtual ~ BCBurgers() {}

  BCBurgers& operator=( const BCBurgers& );

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return fluxViscous_; }

  // BC state vector
  template <class T>
  void state( const Real& x, const Real& time,
              const Real& nx, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    qB = qB_;
  }

  // normal BC flux
  template <class T>
  void fluxNormalSpaceTime( const Real& x, const Real& time,
                            const Real& nx, const Real& nt,
                            const ArrayQ<T>& qI,
                            const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
                            const ArrayQ<T>& qB,
                            ArrayQ<T>& Fn) const
  {
    // Add upwinded advective flux
    pde_.fluxAdvectiveUpwindSpaceTime(x, time, qI, qB, nx, nt, Fn);

    // Add viscous flux
    ArrayQ<T> fx = 0, ft = 0;
    pde_.fluxViscousSpaceTime(x, time, qB, qIx, qIt, fx, ft);
    Fn += fx*nx + ft*nt;
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& nt, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const PDEBurgers<PhysD1, QType, ViscousFlux, Source>& pde_;
  ArrayQ<Real> qB_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const ViscousFlux& visc_;
  bool fluxViscous_;
};

//----------------------------------------------------------------------------//
// BCTypeFunction_mitState BC:  A u + B (kn un) = Fnbc, where u = uexact(x,time)
//
class BCTypeFunction_mitStateParam;

template<>
struct BCBurgersParams<PhysD1, BCTypeFunction_mitStateParam> : ScalarFunction1DParams
{
  static constexpr const char* BCName{"Function_mitState"};
  struct Option
  {
    const DictOption Function_mitState{ BCBurgersParams::BCName, BCBurgersParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCBurgersParams params;
};

template<class QType, class ViscousFlux, class Source>
class BCBurgers<PhysD1, BCTypeFunction_mitState<QType,ViscousFlux, Source>> :
  public BCType< BCBurgers<PhysD1, BCTypeFunction_mitState<QType,ViscousFlux, Source>> >
{
public:
  typedef BCCategory::Flux_mitState Category;
  typedef BCBurgersParams<PhysD1, BCTypeFunction_mitStateParam> ParamsType;

  typedef PhysD1 PhysDim;
  static const int D = AdvectionDiffusionTraits<PhysD1>::D;   // physical dimensions
  static const int N = AdvectionDiffusionTraits<PhysD1>::N;   // total solution variables
  typedef QType QInterpret;

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD1>::template MatrixQ<T>; // matrices

  typedef std::shared_ptr<Function1DBase<ArrayQ<Real>>> Function_ptr;

  BCBurgers(const PDEBurgers<PhysD1, QType, ViscousFlux, Source>& pde, const PyDict& d ) :
    uexact_( ParamsType::getFunction(d) ),
    qInterpret_(pde.variableInterpreter()),
    visc_( pde.getViscousFlux() ) {}

  BCBurgers( const Function_ptr& uexact, const QInterpret& qInterpret, const ViscousFlux& visc ) :
    uexact_(uexact),
    qInterpret_(qInterpret),
    visc_(visc)
  {
    //Nothing
  }

  virtual ~ BCBurgers() {}

  BCBurgers& operator=( const BCBurgers& );

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return true; }

  // BC state vector
  template <class T>
  void state( const Real& x, const Real& time,
              const Real& nx,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    qB = (*uexact_)(x, time);
  }

  // normal BC flux
  template <class T>
  void fluxNormal( const Real& x, const Real& time,
                   const Real& nx,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qB2,
                   ArrayQ<T>& Fn) const
  {
    Real qB, qBx, qBt;
    (*uexact_).gradient(x, time, qB, qBx, qBt);

    Real uB;
    qInterpret_.masterState(qB, uB);

    Real fx = 0.5*uB*uB;

    Fn += fx*nx;

    Real kxx = 0;
    visc_.diffusionViscous(x, time, uB, qBx, kxx);

    Real fv = kxx*qBx;

    Fn -= fv*nx;
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const Function_ptr uexact_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const ViscousFlux& visc_;
};


//----------------------------------------------------------------------------//
// Solution BC:  A u + B (kn un) = bcdata, where u = uexact(x,time)
//
template<>
struct BCBurgersParams< PhysD1, BCTypeFunctionLinearRobin_mitLG > : ScalarFunction1DParams
{
  const ParameterNumeric<Real> A{"A", 1.0, NO_RANGE, "Value coefficient"};
  const ParameterNumeric<Real> B{"B", 0.0, NO_RANGE, "Viscous coefficient"};

  static constexpr const char* BCName{"FunctionLinearRobin_mitLG"};
  struct Option
  {
    const DictOption FunctionLinearRobin_mitLG{ BCBurgersParams::BCName, BCBurgersParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCBurgersParams params;
};

template<>
struct BCBurgersParams< PhysD1, BCTypeFunctionLinearRobin_sansLG > : ScalarFunction1DParams
{
  const ParameterNumeric<Real> A{"A", 1.0, NO_RANGE, "Value coefficient"};
  const ParameterNumeric<Real> B{"B", 0.0, NO_RANGE, "Viscous coefficient"};

  static constexpr const char* BCName{"FunctionLinearRobin_sansLG"};
  struct Option
  {
    const DictOption FunctionLinearRobin_sansLG{ BCBurgersParams::BCName, BCBurgersParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCBurgersParams params;
};

template <class PhysDim>
class BCBurgers_SolutionBase;

template <>
class BCBurgers_SolutionBase<PhysD1>
{
public:
  typedef PhysD1 PhysDim;
   static const int D = AdvectionDiffusionTraits<PhysD1>::D;   // physical dimensions
   static const int N = AdvectionDiffusionTraits<PhysD1>::N;   // total solution variables

   static const int NBC = 1;                   // total BCs

   template <class T>
   using ArrayQ = AdvectionDiffusionTraits<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

   template <class T>
   using MatrixQ = AdvectionDiffusionTraits<PhysD1>::template MatrixQ<T>; // matrices

   typedef std::shared_ptr<Function1DBase<ArrayQ<Real>>> Function_ptr;

   //cppcheck-suppress noExplicitConstructor
   BCBurgers_SolutionBase( const Function_ptr& uexact, const ViscousFlux1DBase& visc, Real A = 1, Real B = 0 ) :
       uexact_(uexact), visc_(visc), A_(A), B_(B) {}

   virtual ~ BCBurgers_SolutionBase() {}

   // BC coefficients:  A u + B (kn un + ks us)
   template <class T>
   void coefficients(
       const Real& x, const Real& time, const Real& nx, MatrixQ<T>& A, MatrixQ<T>& B ) const;

   // BC data
   template <class T>
   void data( const Real& x, const Real& time, const Real& nx, ArrayQ<T>& bcdata ) const;

   // is the boundary state valid
   bool isValidState( const Real& nx, const ArrayQ<Real>& q ) const { return true; }

   void dump( int indentSize, std::ostream& out = std::cout ) const;

 private:
   const Function_ptr uexact_;
   const ViscousFlux1DBase& visc_;
   Real A_, B_;
};

template <class T>
inline void
BCBurgers_SolutionBase< PhysD1 >::coefficients(
    const Real&, const Real&, const Real&, MatrixQ<T>& A, MatrixQ<T>& B ) const
{
  A = A_;
  B = B_;
}

template <class T>
inline void
BCBurgers_SolutionBase<PhysD1 >::data(
    const Real& x,const Real& time, const Real& nx, ArrayQ<T>& bcdata ) const
{
  Real u, ux, ut;
  uexact_->gradient(x, time, u, ux, ut);

  Real kxx = 0;
  visc_.diffusionViscous(x, time, u, ux, kxx);

  bcdata = A_*u + B_*kxx*ux*nx;
}

template <>
class BCBurgers<PhysD1, BCTypeFunctionLinearRobin_mitLG> : public BCType< BCBurgers<PhysD1, BCTypeFunctionLinearRobin_mitLG> >,
                                                public BCBurgers_SolutionBase<PhysD1>
{
public:
  typedef BCBurgers_SolutionBase<PhysD1> BaseType;
  typedef BCCategory::LinearScalar_mitLG Category;
  typedef BCBurgersParams<PhysD1, BCTypeFunctionLinearRobin_mitLG> ParamsType;

  template<class QType, class ViscousFlux, class Source>
  BCBurgers(const PDEBurgers<PhysD1, QType, ViscousFlux, Source>& pde, const PyDict& d ) :
    BaseType( ParamsType::params.getFunction(d), pde.getViscousFlux(),
    d.get(ParamsType::params.A), d.get(ParamsType::params.B)) {}

  BCBurgers( const Function_ptr& uexact, const ViscousFlux1DBase& visc, Real A = 1, Real B = 0 ) :
    BaseType( uexact, visc, A, B ) {}
};

template <>
class BCBurgers<PhysD1, BCTypeFunctionLinearRobin_sansLG> : public BCType< BCBurgers<PhysD1, BCTypeFunctionLinearRobin_sansLG> >,
                                                 public BCBurgers_SolutionBase<PhysD1>
{
public:
  typedef BCBurgers_SolutionBase<PhysD1> BaseType;
  typedef BCCategory::LinearScalar_sansLG Category;
  typedef BCBurgersParams<PhysD1, BCTypeFunctionLinearRobin_sansLG> ParamsType;

  template<class QType, class ViscousFlux, class Source>
  BCBurgers(const PDEBurgers<PhysD1, QType, ViscousFlux, Source>& pde, const PyDict& d ) :
    BaseType( ParamsType::params.getFunction(d), pde.getViscousFlux(),
    d.get(ParamsType::params.A), d.get(ParamsType::params.B)) {}

  BCBurgers( const Function_ptr& uexact, const ViscousFlux1DBase& visc, Real A = 1, Real B = 0 ) :
    BaseType( uexact, visc, A, B ) {}
};

} //namespace SANS

#endif  // BCBURGERS1D_H
