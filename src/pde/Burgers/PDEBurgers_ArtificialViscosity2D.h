// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDEBURGERS_ARTIFICIALVISCOSITY2D_H
#define PDEBURGERS_ARTIFICIALVISCOSITY2D_H

// 2-D Burgers PDE class with artificial viscosity

#include "tools/SANSnumerics.h"     // Real
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Exp.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "pde/AdvectionDiffusion/AdvectionDiffusion_Traits.h"

#include "Surreal/PromoteSurreal.h"

#include "Topology/ElementTopology.h"
#include "Topology/Dimension.h"

#include "Burgers_ArtificialViscosity_fwd.h"
#include "PDEBurgers2D.h"

#include "Field/Tuple/ParamTuple.h"
#include "tools/Tuple.h"
#include "tools/smoothmath.h"

#include <iostream>
#include <string>

namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: 2-D Burgers with artificial viscosity
//
// template parameters:
//   T                           solution DOF data type (e.g. double)
//
// member functions:
//   .hasFluxAdvectiveTime        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//   .hasFluxViscous             T/F: PDE has viscous flux term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: dU(Q)/dQ
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .fluxViscous                viscous fluxes: Fv(Q, Qx)
//   .diffusionViscous           viscous diffusion coefficient: d(Fv)/d(Ux)
//   .isValidState               T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom                 set from primitive variable array
//----------------------------------------------------------------------------//

template<class QType, class ViscousFlux, class Source>
class PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source>
{
public:
  typedef PhysD2 PhysDim;
  static const int D = AdvectionDiffusionTraits<PhysD2>::D;   // physical dimensions
  static const int N = AdvectionDiffusionTraits<PhysD2>::N;   // total solution variables

  typedef QType QInterpret;            // solution variable interpreter type

  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD2>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD2>::template MatrixQ<T>; // matrices

  template <class T>
  using MatrixParam = T;         // e.g. jacobian of PDEs w.r.t. parameters

  PDEBurgers_ArtificialViscosity(const int order,
                                 const PDEBurgers<PhysD2, QType, ViscousFlux, Source>& pde) :
                                   order_(order), pde_(pde) {}

  ~PDEBurgers_ArtificialViscosity() {}

  PDEBurgers_ArtificialViscosity( const PDEBurgers_ArtificialViscosity& ) = delete;
  PDEBurgers_ArtificialViscosity& operator=( const PDEBurgers_ArtificialViscosity& ) = delete;

  // flux components
  bool hasFluxAdvectiveTime() const { return true; }
  bool hasFluxAdvective() const { return pde_.hasFluxAdvective(); }
  bool hasFluxViscous() const { return true; }
  bool hasSource() const { return pde_.hasSource(); }
  bool hasSourceTrace() const { return pde_.hasSourceTrace(); }
  bool hasSourceLiftedQuantity() const { return false; }
  bool hasForcingFunction() const { return pde_.hasForcingFunction(); }

  // viscous flux is of the form K(q).gradu
  bool fluxViscousLinearInGradient() const { return true; }
  bool needsSolutionGradientforSource() const { return pde_.needsSolutionGradientforSource(); }

  const ViscousFlux& getViscousFlux() const { return pde_.getViscousFlux(); }
  const QInterpret& variableInterpreter() const { return pde_.variableInterpreter(); }
  Real getV() const { return pde_.getV(); }

  // unsteady temporal flux: Ft(Q)
  template <class T, class L, class R>
  void fluxAdvectiveTime(
      const ParamTuple<L, R, TupleClass<>>& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, ArrayQ<T>& ft ) const
  {
    pde_.fluxAdvectiveTime(x, y, time, q, ft);
  }

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <class T, class L, class R>
  void jacobianFluxAdvectiveTime(
      const ParamTuple<L, R, TupleClass<>>& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& J ) const
  {
    pde_.jacobianFluxAdvectiveTime(x, y, time, q, J);
  }

  // master state: U(Q)
  template <class T, class L, class R>
  void masterState(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, ArrayQ<T>& uCons ) const;

  // jacobian of master state wrt q: dU(Q)/dQ
  template<class T, class L, class R>
  void jacobianMasterState(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const;

  // strong form of unsteady conservative flux: dU(Q)/dt
  template <class T, class L, class R>
  void strongFluxAdvectiveTime(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& strongPDE ) const;


  // advective flux: F(Q)
  template <class T, class L, class R, class Tf>
  void fluxAdvective(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& fy ) const;

  template <class T, class L, class R, class Tf>
  void fluxAdvectiveUpwind(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const;

  template <class T, class L, class R, class Tf>
  void fluxAdvectiveUpwindSpaceTime(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, const Real& nt, ArrayQ<Tf>& f ) const;

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class T, class L, class R>
  void jacobianFluxAdvective(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q,
      MatrixQ<T>& ax, MatrixQ<T>& ay ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class T, class L, class R>
  void jacobianFluxAdvectiveAbsoluteValue(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const Real& nx, const Real& ny,
      MatrixQ<T>& a ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class T, class L, class R>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const Real& nx, const Real& ny, const Real& nt,
      MatrixQ<T>& a ) const;

  // strong form advective fluxes
  template <class T, class L, class R>
  void strongFluxAdvective(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
      ArrayQ<T>& strongPDE ) const;

  // viscous flux: Fv(Q, QX)
  template <class Tq, class Tqg, class Th, class Tg, class Tf>
  void fluxViscous(
      const ParamTuple<Th, Tg, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqg>& qx, const ArrayQ<Tqg>& qy,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const;

  template <class Tq, class Tqg, class Th, class Tg, class Tf>
  void fluxViscous(
      const ParamTuple<Th, Tg, TupleClass<>>& paramL,
      const ParamTuple<Th, Tg, TupleClass<>>& paramR,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tqg>& qxL, const ArrayQ<Tqg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tqg>& qxR, const ArrayQ<Tqg>& qyR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const;

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Th, class Ts, class Tq, class Tg, class Tk>
  void diffusionViscous(
      const ParamTuple<Th, Ts, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const;

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class T, class Th, class Tg, class Tf>
  void jacobianFluxViscous(
      const ParamTuple<Th, Tg, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& ax, MatrixQ<Tf>& ay ) const {}

  // strong form viscous fluxes
  template <class T, class L, class R>
  void strongFluxViscous(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
      const ArrayQ<T>& qxx,
      const ArrayQ<T>& qxy, const ArrayQ<T>& qyy,
      ArrayQ<T>& strongPDE ) const;

  // space-time viscous flux: Fv(Q, QX)
  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& ft ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial viscous flux
    fluxViscous(param, x, y, time, q, qx, qy, fx, fy);
  }

  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Tp& paramL, const Tp& paramR,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qtL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qtR,
      const Real& nx, const Real& ny, const Real& nt,
      ArrayQ<Tf>& f ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial viscous flux
    fluxViscous(paramL, paramR, x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, f);
  }

  // space-time viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tp, class Tq, class Tg, class Tk>
  void diffusionViscousSpaceTime(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxt,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyt,
      MatrixQ<Tk>& ktx, MatrixQ<Tk>& kty, MatrixQ<Tk>& ktt ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial diffusion matrix
    diffusionViscous(param, x, y, time, q, qx, qy, kxx, kxy, kyx, kyy);
  }

  // space-time jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tp, class Tq, class Tg, class Tf>
  void jacobianFluxViscousSpaceTime(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& at ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial jacobianFluxViscous
    jacobianFluxViscous(param, x, y, time, q, qx, qy, ax, ay);
  }

  // space-time strong form viscous fluxes: -d(Fv)/dx - d(Fv)/d(y)
  template <class Tp, class Tq, class Tg, class Th, class Tf>
  void strongFluxViscousSpaceTime(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxt, const ArrayQ<Th>& qyt, const ArrayQ<Th>& qtt,
      ArrayQ<Tf>& strongPDE ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial strongFluxViscous
    strongFluxViscous(param, x, y, time, q, qx, qy, qxx, qxy, qyy, strongPDE);
  }

  // solution-dependent source: S(X, Q, QX)
  template <class Tq, class Tg, class Ts, class L, class R>
  void source(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Ts>& source ) const;

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX)
  template <class Tp, class Tlq, class Tq, class Tg, class Ts>
  void source(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const Tlq& lifted_quantity, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Ts>& source ) const
  {
    pde_.source( x, y, time, lifted_quantity, q, qx, qy, source );
  }

  // dual-consistent source
  template <class T, class L, class R, class Tg, class Ts>
  void sourceTrace(
      const ParamTuple<L, R, TupleClass<>>& paramL, const Real& xL, const Real& yL,
      const ParamTuple<L, R, TupleClass<>>& paramR, const Real& xR, const Real& yR,
      const Real& time,
      const ArrayQ<T>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<T>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const;

  // trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
  template <class Tp, class Tq, class Ts>
  void sourceLiftedQuantity(
      const Tp& paramL, const Real& xL, const Real& yL,
      const Tp& paramR, const Real& xR, const Real& yR,
      const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      Ts& s ) const
  {
    pde_.source( xL, yL, xR, yR, time, qL, qR, s );
  }

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class T, class L, class R>
  void jacobianSource(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
      MatrixQ<T>& dsdu ) const;

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class T, class L, class R>
  void jacobianSourceAbsoluteValue(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
      MatrixQ<T>& dsdu ) const;

  // right-hand-side forcing function
  template <class T, class L, class R>
  void forcingFunction(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time, ArrayQ<T>& forcing ) const;

  // characteristic speed (needed for timestep)
  template <class T, class L, class R>
  void speedCharacteristic(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const Real& dx, const Real& dy, const ArrayQ<T>& q, T& speed ) const;

  // characteristic speed
  template <class T, class L, class R>
  void speedCharacteristic(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, T& speed ) const;

  // update fraction needed for physically valid state
  template <class L, class R>
  void updateFraction(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
      const Real maxChangeFraction, Real& updateFraction ) const;

  // is state physically valid
  template <class T>
  bool isValidState( const ArrayQ<T>& q ) const { return true; }

  // set from primitive variable array
  template <class T>
  void setDOFFrom(
      ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const;

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const;

  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const;

  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const;

  // how many residual equations are we dealing with
  int nMonitor() const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

  int getOrder() const { return order_; };

  // artificial viscosity calculation with generalized h-tensor
  template <class T, class Th, class Tg, class Ts>
  void artificialViscosity( const Real& x, const Real& y, const Real& time,
                            const ArrayQ<T>& q, const DLA::MatrixSymS<D,Th>& logH, const Tg& g,
                            MatrixQ<Ts>& Kxx, MatrixQ<Ts>& Kxy, MatrixQ<Ts>& Kyx, MatrixQ<Ts>& Kyy ) const;

private:
  const int order_;
  const PDEBurgers<PhysD2, QType, ViscousFlux, Source>& pde_;
};


// Artificial viscosity calculation with generalized h-tensor
template<class QType, class ViscousFlux, class Source>
template <class T, class Th, class Tg, class Ts>
inline void
PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source>::
artificialViscosity( const Real& x, const Real& y, const Real& time,
                     const ArrayQ<T>& q, const DLA::MatrixSymS<D,Th>& logH, const Tg& g,
                     MatrixQ<Ts>& Kxx, MatrixQ<Ts>& Kxy, MatrixQ<Ts>& Kyx, MatrixQ<Ts>& Kyy ) const
{
  T lambda;
  Real C = 1.0;

  pde_.speedCharacteristic( x, y, time, q, lambda );

  DLA::MatrixSymS<D,Th> H = exp(logH); //generalized h-tensor

  Ts factor = C/(Real(order_)+1) * lambda * smoothabs0(g, 1.0e-5);

  Kxx += factor*H(0,0);
  Kxy += factor*H(0,1);
  Kyx += factor*H(1,0);
  Kyy += factor*H(1,1);
}


template<class QType, class ViscousFlux, class Source>
template <class T, class L, class R>
inline void
PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source>::masterState(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, ArrayQ<T>& uCons ) const
{
  pde_.masterState(x, y, time, q, uCons);
}

template<class QType, class ViscousFlux, class Source>
template <class T, class L, class R>
inline void
PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source>::jacobianMasterState(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
{
  pde_.jacobianMasterState(x, y, time, q, dudq);
}

template<class QType, class ViscousFlux, class Source>
template <class T, class L, class R>
inline void
PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source>::strongFluxAdvectiveTime(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& uCons ) const
{
  pde_.strongFluxAdvectiveTime(x, y, time, q, qt, uCons);
}


template<class QType, class ViscousFlux, class Source>
template <class T, class L, class R, class Tf>
inline void
PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source>::fluxAdvective(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, ArrayQ<Tf>& fx, ArrayQ<Tf>& fy) const
{
  pde_.fluxAdvective( x, y, time, q, fx, fy );
}


template<class QType, class ViscousFlux, class Source>
template <class T, class L, class R, class Tf>
inline void
PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source>::fluxAdvectiveUpwind(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx, const Real& ny,
    ArrayQ<Tf>& f ) const
{
  pde_.fluxAdvectiveUpwind(x, y, time, qL, qR, nx, ny, f);
}

template<class QType, class ViscousFlux, class Source>
template <class T, class L, class R, class Tf>
inline void
PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source>::fluxAdvectiveUpwindSpaceTime(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx, const Real& ny, const Real& nt,
    ArrayQ<Tf>& f ) const
{
  pde_.fluxAdvectiveUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
}


// advective flux jacobian: d(F)/d(U)
template<class QType, class ViscousFlux, class Source>
template <class T, class L, class R>
inline void
PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source>::jacobianFluxAdvective(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q,
    MatrixQ<T>& ax, MatrixQ<T>& ay ) const
{
  pde_.jacobianFluxAdvective( x, y, time, q, ax, ay );
}


// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template<class QType, class ViscousFlux, class Source>
template <class T, class L, class R>
inline void
PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source>::jacobianFluxAdvectiveAbsoluteValue(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
  const ArrayQ<T>& q, const Real& nx, const Real& ny, MatrixQ<T>& mtx ) const
{
  pde_.jacobianFluxAdvectiveAbsoluteValue(x, y, time, q, nx, ny, mtx);
}


// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template<class QType, class ViscousFlux, class Source>
template <class T, class L, class R>
inline void
PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source>::jacobianFluxAdvectiveAbsoluteValueSpaceTime(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
  const ArrayQ<T>& q, const Real& nx, const Real& ny, const Real& nt, MatrixQ<T>& mtx ) const
{
  pde_.jacobianFluxAdvectiveAbsoluteValueSpaceTime(x, y, time, q, nx, ny, nt, mtx);
}


// strong form of advective flux: div.F
template<class QType, class ViscousFlux, class Source>
template <class T, class L, class R>
inline void
PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source>::strongFluxAdvective(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
    ArrayQ<T>& strongPDE ) const
{
  pde_.strongFluxAdvective( x, y, time, q, qx, qy, strongPDE );
}


// viscous flux
template<class QType, class ViscousFlux, class Source>
template <class Tq, class Tqg, class Th, class Tg, class Tf>
inline void
PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source>::fluxViscous(
    const ParamTuple<Th, Tg, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tqg>& qx, const ArrayQ<Tqg>& qy,
    ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
{
  const Th& logH = param.left();
  const Tg& sensor = param.right();

  // Add artificial viscosity
  MatrixQ<Tf> nu_xx = 0.0, nu_xy = 0.0;
  MatrixQ<Tf> nu_yx = 0.0, nu_yy = 0.0;
  artificialViscosity( x, y, time, q, logH, sensor, nu_xx, nu_xy, nu_yx, nu_yy );

  //Isotropic artificial viscosity
  f -= nu_xx*qx + nu_xy*qy;
  g -= nu_yx*qx + nu_yy*qy;

  pde_.fluxViscous( x, y, time, q, qx, qy, f, g );
}


// viscous flux: normal flux with left and right states
template<class QType, class ViscousFlux, class Source>
template <class Tq, class Tqg, class Th, class Tg, class Tf>
inline void
PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source>::fluxViscous(
    const ParamTuple<Th, Tg, TupleClass<>>& paramL,
    const ParamTuple<Th, Tg, TupleClass<>>& paramR,
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tqg>& qxL, const ArrayQ<Tqg>& qyL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tqg>& qxR, const ArrayQ<Tqg>& qyR,
    const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const
{
  ArrayQ<Tf> fL = 0, fR = 0;
  ArrayQ<Tf> gL = 0, gR = 0;

  fluxViscous(paramL, x, y, time, qL, qxL, qyL, fL, gL);
  fluxViscous(paramR, x, y, time, qR, qxR, qyR, fR, gR);

  f += 0.5*(fL + fR)*nx + 0.5*(gL + gR)*ny;
}


// viscous diffusion matrix: d(Fv)/d(UX)
template<class QType, class ViscousFlux, class Source>
template <class Th, class Ts, class Tq, class Tg, class Tk>
inline void
PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source>::diffusionViscous(
    const ParamTuple<Th, Ts, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
    MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const
{
  const Th& logH = param.left();
  const Ts& sensor = param.right();

  // Add artificial viscosity
  MatrixQ<Tk> nu_xx = 0.0, nu_xy = 0.0;
  MatrixQ<Tk> nu_yx = 0.0, nu_yy = 0.0;
  artificialViscosity( x, y, time, q, logH, sensor, nu_xx, nu_xy, nu_yx, nu_yy );

  pde_.diffusionViscous( x, y, time, q, qx, qy, kxx, kxy, kyx, kyy );

  kxx += nu_xx;
  kxy += nu_xy;
  kyx += nu_yx;
  kyy += nu_yy;
}


// strong form of viscous flux: d(Fv)/d(X)
template<class QType, class ViscousFlux, class Source>
template <class T, class L, class R>
inline void
PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source>::strongFluxViscous(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
    const ArrayQ<T>& qxx,
    const ArrayQ<T>& qyx, const ArrayQ<T>& qyy,
    ArrayQ<T>& strongPDE ) const
{
  SANS_DEVELOPER_EXCEPTION("PDEBurgers_ArtificialViscosity<PhysD2>::strongFluxViscous - Not implemented.");
}


// solution-dependent source: S(X, Q, QX)
template<class QType, class ViscousFlux, class Source>
template <class Tq, class Tg, class Ts, class L, class R>
inline void
PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source>::source(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Ts>& source ) const
{
  pde_.source(x, y, time, q, qx, qy, source);
}


// dual-consistent source
template<class QType, class ViscousFlux, class Source>
template <class T, class L, class R, class Tg, class Ts>
inline void
PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source>::sourceTrace(
    const ParamTuple<L, R, TupleClass<>>& paramL, const Real& xL, const Real& yL,
    const ParamTuple<L, R, TupleClass<>>& paramR, const Real& xR, const Real& yR,
    const Real& time,
    const ArrayQ<T>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
    const ArrayQ<T>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
    ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
{
  pde_.sourceTrace(xL, yL, xR, yR, time,
                   qL, qxL, qyL,
                   qR, qxR, qyR,
                   sourceL, sourceR);
}

// jacobian of source wrt conservation variables: d(S)/d(U)
template<class QType, class ViscousFlux, class Source>
template <class T, class L, class R>
inline void
PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source>::jacobianSource(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
    MatrixQ<T>& dsdu ) const
{
  pde_.jacobianSource(x, y, time, q, qx, qy, dsdu);
}

// jacobian of source wrt conservation variables: d(S)/d(U)
template<class QType, class ViscousFlux, class Source>
template <class T, class L, class R>
inline void
PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source>::jacobianSourceAbsoluteValue(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
    MatrixQ<T>& dsdu ) const
{
  pde_.jacobianSourceAbsoluteValue(x, y, time, q, qx, qy, dsdu);
}

// right-hand-side forcing function
template<class QType, class ViscousFlux, class Source>
template <class T, class L, class R>
inline void
PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source>::forcingFunction(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time, ArrayQ<T>& forcing ) const
{
  pde_.forcingFunction(x, y, time, forcing );
}


// characteristic speed (needed for timestep)
template<class QType, class ViscousFlux, class Source>
template <class T, class L, class R>
inline void
PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source>::speedCharacteristic(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
    const Real& dx, const Real& dy, const ArrayQ<T>& q, T& speed ) const
{
  pde_.speedCharacteristic(x, y, time, dx, dy, q, speed);
}


// characteristic speed
template<class QType, class ViscousFlux, class Source>
template <class T, class L, class R>
inline void
PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source>::speedCharacteristic(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, T& speed ) const
{
  pde_.speedCharacteristic(x, y, time, q, speed);
}


// update fraction needed for physically valid state
template<class QType, class ViscousFlux, class Source>
template <class L, class R>
inline void
PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source>::updateFraction(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
    const Real maxChangeFraction, Real& updateFraction ) const
{
  pde_.updateFraction(x, y, time, q, dq, maxChangeFraction, updateFraction );
}


// set from primitive variable array
template<class QType, class ViscousFlux, class Source>
template <class T>
inline void
PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source>::setDOFFrom(
    ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const
{
  pde_.setDOFFrom(q, data, name, nn);
}


// interpret residuals of the solution variable
template<class QType, class ViscousFlux, class Source>
template <class T>
void
PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source>::interpResidVariable(
    const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
{
  pde_.interpResidVariable(rsdPDEIn, rsdPDEOut);
}

// interpret residuals of the gradients in the solution variable
template<class QType, class ViscousFlux, class Source>
template <class T>
void
PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source>::interpResidGradient(
    const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const
{
  pde_.interpResidGradient(rsdAuxIn, rsdAuxOut);
}

// interpret residuals at the boundary (should forward to BCs)
template<class QType, class ViscousFlux, class Source>
template <class T>
void
PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source>::interpResidBC(
    const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const
{
  pde_.interpResidBC(rsdBCIn, rsdBCOut);
}

// how many residual equations are we dealing with
template<class QType, class ViscousFlux, class Source>
inline int
PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source>::nMonitor() const
{
  return pde_.nMonitor();
}

template<class QType, class ViscousFlux, class Source>
void
PDEBurgers_ArtificialViscosity<PhysD2, QType, ViscousFlux, Source>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PDEBurgers_ArtificialViscosity2D: pde_ =" << std::endl;
  pde_.dump(indentSize+2, out);
}

} //namespace SANS

#endif  // PDEBURGERS_ARTIFICIALVISCOSITY2D_H
