// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCBurgers1D.h"

#include "pde/AdvectionDiffusion/ViscousFlux1D.h"
#include "pde/AdvectionDiffusion/Source1D.h"
#include "pde/AdvectionDiffusion/ForcingFunction1D.h"

#include "BurgersConservative1D.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{
//===========================================================================//
//
// Parameter classes
//
// cppcheck-suppress passedByValue
void  BCBurgersParams<PhysD1, BCTypeLinearRobin_mitLG>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.A));
  allParams.push_back(d.checkInputs(params.B));
  allParams.push_back(d.checkInputs(params.bcdata));
  d.checkUnknownInputs(allParams);
}
BCBurgersParams<PhysD1, BCTypeLinearRobin_mitLG>  BCBurgersParams<PhysD1, BCTypeLinearRobin_mitLG>::params;

// cppcheck-suppress passedByValue
void  BCBurgersParams<PhysD1, BCTypeLinearRobin_sansLG>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.A));
  allParams.push_back(d.checkInputs(params.B));
  allParams.push_back(d.checkInputs(params.bcdata));
  d.checkUnknownInputs(allParams);
}
BCBurgersParams<PhysD1, BCTypeLinearRobin_sansLG>  BCBurgersParams<PhysD1, BCTypeLinearRobin_sansLG>::params;

// cppcheck-suppress passedByValue
void  BCBurgersParams<PhysD1, BCTypeDirichlet_mitStateParam>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.qB));
  d.checkUnknownInputs(allParams);
}
BCBurgersParams<PhysD1, BCTypeDirichlet_mitStateParam>  BCBurgersParams<PhysD1, BCTypeDirichlet_mitStateParam>::params;

// cppcheck-suppress passedByValue
void  BCBurgersParams<PhysD1, BCTypeDirichlet_mitStateSpaceTimeParam>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.qB));
  d.checkUnknownInputs(allParams);
}
BCBurgersParams<PhysD1, BCTypeDirichlet_mitStateSpaceTimeParam>  BCBurgersParams<PhysD1, BCTypeDirichlet_mitStateSpaceTimeParam>::params;

// cppcheck-suppress passedByValue
void  BCBurgersParams<PhysD1, BCTypeFunction_mitStateParam>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Function));
  d.checkUnknownInputs(allParams);
}
BCBurgersParams<PhysD1, BCTypeFunction_mitStateParam>  BCBurgersParams<PhysD1, BCTypeFunction_mitStateParam>::params;

// cppcheck-suppress passedByValue
void  BCBurgersParams<PhysD1, BCTypeFunctionLinearRobin_mitLG>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Function));
  allParams.push_back(d.checkInputs(params.A));
  allParams.push_back(d.checkInputs(params.B));
  d.checkUnknownInputs(allParams);
}
BCBurgersParams<PhysD1, BCTypeFunctionLinearRobin_mitLG>  BCBurgersParams<PhysD1, BCTypeFunctionLinearRobin_mitLG>::params;

// cppcheck-suppress passedByValue
void  BCBurgersParams<PhysD1, BCTypeFunctionLinearRobin_sansLG>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Function));
  allParams.push_back(d.checkInputs(params.A));
  allParams.push_back(d.checkInputs(params.B));
  d.checkUnknownInputs(allParams);
}
BCBurgersParams<PhysD1, BCTypeFunctionLinearRobin_sansLG>  BCBurgersParams<PhysD1, BCTypeFunctionLinearRobin_sansLG>::params;


//===========================================================================//
// Instantiate the BC parameters
typedef BCBurgers1DVector< BurgersConservative1D, ViscousFlux1D_Uniform, Source1D_Uniform> BCVector_DUSU;
typedef BCBurgers1DVector< BurgersConservative1D, ViscousFlux1D_Uniform, Source1D_None> BCVector_DUSN;
typedef BCBurgers1DVector< BurgersConservative1D, ViscousFlux1D_None, Source1D_None> BCVector_DNSN;

BCPARAMETER_INSTANTIATE( BCVector_DUSU )
BCPARAMETER_INSTANTIATE( BCVector_DUSN )
BCPARAMETER_INSTANTIATE( BCVector_DNSN )

//===========================================================================//
void
BCBurgers_LinearRobinBase<PhysD1>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << " BCBurgers<PhysD1, BCTypeLinearRobin>:" << std::endl;
  out << indent << "  A_ = " << A_ << std::endl;
  out << indent << "  B_ = " << B_ << std::endl;
  out << indent << "  bcdata_ = " << bcdata_ << std::endl;
}

void
BCBurgers_SolutionBase<PhysD1>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << " BCBurgers<PhysD1, BCTypeFunction>" << std::endl;
}

} //namespace SANS
