// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCmitAVSensorBurgers2D.h"

#include "pde/AdvectionDiffusion/ViscousFlux2D.h"
#include "pde/AdvectionDiffusion/Source2D.h"
#include "pde/AdvectionDiffusion/ForcingFunction2D.h"

#include "BurgersConservative2D.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{
//===========================================================================//
//
// Parameter classes
//
// cppcheck-suppress passedByValue
void BCmitAVSensorBurgersParams<PhysD2, BCTypeLinearRobin_mitLG>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.A));
  allParams.push_back(d.checkInputs(params.B));
  allParams.push_back(d.checkInputs(params.bcdata));
  d.checkUnknownInputs(allParams);
}
BCmitAVSensorBurgersParams<PhysD2, BCTypeLinearRobin_mitLG> BCmitAVSensorBurgersParams<PhysD2, BCTypeLinearRobin_mitLG>::params;

// cppcheck-suppress passedByValue
void BCmitAVSensorBurgersParams<PhysD2, BCTypeLinearRobin_sansLG>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.A));
  allParams.push_back(d.checkInputs(params.B));
  allParams.push_back(d.checkInputs(params.bcdata));
  d.checkUnknownInputs(allParams);
}
BCmitAVSensorBurgersParams<PhysD2, BCTypeLinearRobin_sansLG> BCmitAVSensorBurgersParams<PhysD2, BCTypeLinearRobin_sansLG>::params;

// cppcheck-suppress passedByValue
void BCmitAVSensorBurgersParams<PhysD2, BCTypeDirichlet_mitStateParam>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.qB));
  d.checkUnknownInputs(allParams);
}
BCmitAVSensorBurgersParams<PhysD2, BCTypeDirichlet_mitStateParam>
  BCmitAVSensorBurgersParams<PhysD2, BCTypeDirichlet_mitStateParam>::params;

// cppcheck-suppress passedByValue
void BCmitAVSensorBurgersParams<PhysD2, BCTypeFluxParams>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.bcdata));
  d.checkUnknownInputs(allParams);
}
BCmitAVSensorBurgersParams<PhysD2, BCTypeFluxParams> BCmitAVSensorBurgersParams<PhysD2, BCTypeFluxParams>::params;

// cppcheck-suppress passedByValue
void BCmitAVSensorBurgersParams<PhysD2, BCTypeFunctionLinearRobin_mitLG>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Function));
  allParams.push_back(d.checkInputs(params.A));
  allParams.push_back(d.checkInputs(params.B));
  d.checkUnknownInputs(allParams);
}
BCmitAVSensorBurgersParams<PhysD2, BCTypeFunctionLinearRobin_mitLG>
  BCmitAVSensorBurgersParams<PhysD2, BCTypeFunctionLinearRobin_mitLG>::params;

// cppcheck-suppress passedByValue
void BCmitAVSensorBurgersParams<PhysD2, BCTypeFunctionLinearRobin_sansLG>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Function));
  allParams.push_back(d.checkInputs(params.A));
  allParams.push_back(d.checkInputs(params.B));
  d.checkUnknownInputs(allParams);
}
BCmitAVSensorBurgersParams<PhysD2, BCTypeFunctionLinearRobin_sansLG>
  BCmitAVSensorBurgersParams<PhysD2, BCTypeFunctionLinearRobin_sansLG>::params;


//===========================================================================//
// Instantiate the BC parameters
typedef BCmitAVSensorBurgers2DVector< BurgersConservative2D, ViscousFlux2D_Uniform> BCVector_DU; BCPARAMETER_INSTANTIATE( BCVector_DU )

//===========================================================================//
void
BCmitAVSensorBurgers_LinearRobinBase<PhysD2>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << " BCmitAVSensorBurgers<PhysD2, BCTypeLinearRobin>:" << std::endl;
  out << indent << "  A_ = " << A_ << std::endl;
  out << indent << "  B_ = " << B_ << std::endl;
  out << indent << "  bcdata_ = " << bcdata_ << std::endl;
}

void
BCmitAVSensorBurgers_SolutionBase<PhysD2>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << " BCmitAVSensorBurgers<PhysD2, BCTypeFunction>" << std::endl;
}

} //namespace SANS
