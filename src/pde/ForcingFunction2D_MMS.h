// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FORCINGFUNCTION2D_MMS_H
#define FORCINGFUNCTION2D_MMS_H

#include "tools/SANSnumerics.h"     // Real

#include "pde/ForcingFunctionBase.h"
#include "AnalyticFunction/Function2D.h"

#include <iostream>

namespace SANS
{

//----------------------------------------------------------------------------//
// forcing function given a 2D analytical solution

template<class PDE>
class ForcingFunction2D_MMS : public ForcingFunctionBase2D<PDE>
{
public:
  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  static const int D = 2;                     // physical dimensions

  typedef Function2DBase<ArrayQ<Real>> FunctionBase;

  virtual bool hasForcingFunction() const override { return true; }

  // cppcheck-suppress noExplicitConstructor
  ForcingFunction2D_MMS( const FunctionBase& soln ) : soln_(soln) {}
  virtual ~ForcingFunction2D_MMS() {}

  void operator()( const PDE& pde,
                   const Real &x, const Real &y, const Real &time,
                   ArrayQ<Real>& forcing ) const override
  {
    ArrayQ<Real> q = 0;
    ArrayQ<Real> qx = 0, qy = 0, qt = 0;
    ArrayQ<Real> qxx = 0,
                 qyx = 0, qyy = 0;

    soln_.secondGradient( x, y, time,
                          q,
                          qx, qy, qt,
                          qxx,
                          qyx, qyy);

    pde.strongFluxAdvectiveTime(x, y, time, q, qt, forcing);
    pde.strongFluxAdvective(x, y, time, q, qx, qy, forcing);
    pde.strongFluxViscous(x, y, time, q, qx, qy, qxx, qyx, qyy, forcing);
    pde.source(x, y, time, q, qx, qy, forcing);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const override
  {
    std::string indent(indentSize, ' ');
    out << indent << "ForcingFunction2D_MMS:" << std::endl;
    soln_.dump(indentSize+2, out);
  }

private:
  const FunctionBase& soln_;  // exact solution
};

} //namespace SANS

#endif //FORCINGFUNCTION2D_MMS_H
