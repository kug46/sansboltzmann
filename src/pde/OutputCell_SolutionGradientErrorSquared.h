// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef OUTPUTCELL_SOLUTIONGRADIENTERRORSQUARED_H
#define OUTPUTCELL_SOLUTIONGRADIENTERRORSQUARED_H

// Python must be included first
#include "Python/PyDict.h"

#include "tools/SANSnumerics.h"     // Real

#include "OutputCategory.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Element cell integrand: solution error squared

template <class PDE_, class SolutionFunction>
class OutputCell_SolutionGradientErrorSquared : public OutputType< OutputCell_SolutionGradientErrorSquared<PDE_, SolutionFunction> >
{
public:
  typedef PDE_ PDE;
  typedef typename PDE::PhysDim PhysDim;
  static const int D = PhysDim::D;

  //Array of solution variables
  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  // Array of outputs
  template<class T>
  using ArrayJ = typename PDE::template ArrayQ<T>;

  // Matrix required to represent the Jacobian of this output functional
  template<class T>
  using MatrixJ = typename PDE::template MatrixQ<T>;

  explicit OutputCell_SolutionGradientErrorSquared( const SolutionFunction& solFcn ) : solFcn_(solFcn) {}
  explicit OutputCell_SolutionGradientErrorSquared( const PyDict& d ) : solFcn_(d) {}

  bool needsSolutionGradient() const { return true; }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, ArrayJ<To>& output ) const
  {
    ArrayQ<Real> qExact=0, qxExact=0, qtExact=0;
    solFcn_.gradient( x, time, qExact, qxExact, qtExact );

    ArrayQ<Tg> dqx = qx - qxExact;
    output = pow(dqx, 2.0);
  }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    ArrayQ<Real> qExact=0, qxExact=0, qyExact=0, qtExact=0;
    solFcn_.gradient( x, y, time, qExact, qxExact, qyExact, qtExact );

    ArrayQ<Tg> dqx = qx - qxExact;
    ArrayQ<Tg> dqy = qy - qyExact;
    output = pow(dqx, 2.0) + pow(dqy, 2.0);
  }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& z, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz, ArrayJ<To>& output ) const
  {
    ArrayQ<Real> qExact=0, qxExact=0, qyExact=0, qzExact=0, qtExact=0;
    solFcn_.gradient( x, y, z, time, qExact, qxExact, qyExact, qzExact, qtExact );

    ArrayQ<Tg> dqx = qx - qxExact;
    ArrayQ<Tg> dqy = qy - qyExact;
    ArrayQ<Tg> dqz = qz - qzExact;
    output = pow(dqx, 2.0) + pow(dqy, 2.0) + pow(dqz, 2.0);
  }

protected:
  const SolutionFunction& solFcn_;
};

}

#endif //OUTPUTCELL_SOLUTIONGRADIENTERRORSQUARED_H
