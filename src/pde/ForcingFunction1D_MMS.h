// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FORCINGFUNCTION1D_MMS_H
#define FORCINGFUNCTION1D_MMS_H

#include "tools/SANSnumerics.h"     // Real

#include "pde/ForcingFunctionBase.h"
#include "AnalyticFunction/Function1D.h"

#include <iostream>

namespace SANS
{

//----------------------------------------------------------------------------//
// forcing function given a 1D analytical solution

template<class PDE>
class ForcingFunction1D_MMS : public ForcingFunctionBase1D<PDE>
{
public:
  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  static const int D = 1;                     // physical dimensions

  typedef Function1DBase<ArrayQ<Real>> FunctionBase;

  virtual bool hasForcingFunction() const override { return true; }

  // cppcheck-suppress noExplicitConstructor
  ForcingFunction1D_MMS( const FunctionBase& soln ) : soln_(soln) {}
  virtual ~ForcingFunction1D_MMS() {}

  void operator()( const PDE& pde,
                   const Real &x, const Real &time,
                   ArrayQ<Real>& forcing ) const override
  {
    ArrayQ<Real> q = 0;
    ArrayQ<Real> qx = 0, qt = 0;
    ArrayQ<Real> qxx = 0;

    soln_.secondGradient( x, time,
                          q,
                          qx, qt,
                          qxx );

    pde.strongFluxAdvectiveTime(x, time, q, qt, forcing);
    pde.strongFluxAdvective(x, time, q, qx, forcing);
    pde.strongFluxViscous(x, time, q, qx, qxx, forcing);
    pde.source(x, time, q, qx, forcing);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const override
  {
    std::string indent(indentSize, ' ');
    out << indent << "ForcingFunction1D_MMS:" << std::endl;
    soln_.dump(indentSize+2, out);
  }

private:
  const FunctionBase& soln_;  // exact solution
};

} //namespace SANS

#endif //FORCINGFUNCTION1D_MMS_H
