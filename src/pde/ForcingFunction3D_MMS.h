// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FORCINGFUNCTION3D_MMS_H
#define FORCINGFUNCTION3D_MMS_H

#include "tools/SANSnumerics.h"     // Real

#include "pde/ForcingFunctionBase.h"
#include "AnalyticFunction/Function3D.h"

#include <iostream>

namespace SANS
{

//----------------------------------------------------------------------------//
// forcing function given a 3D analytical solution

template<class PDE>
class ForcingFunction3D_MMS : public ForcingFunctionBase3D<PDE>
{
public:
  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  static const int D = 3;                     // physical dimensions

  typedef Function3DBase<ArrayQ<Real>> FunctionBase;

  virtual bool hasForcingFunction() const override { return true; }

  // cppcheck-suppress noExplicitConstructor
  ForcingFunction3D_MMS( const FunctionBase& soln ) : soln_(soln) {}
  virtual ~ForcingFunction3D_MMS() {}

  void operator()( const PDE& pde,
                   const Real &x, const Real &y, const Real &z, const Real &time,
                   ArrayQ<Real>& forcing ) const override
  {
    ArrayQ<Real> q = 0;
    ArrayQ<Real> qx = 0, qy = 0, qz = 0, qt = 0;
    ArrayQ<Real> qxx = 0,
                 qyx = 0, qyy = 0,
                 qzx = 0, qzy = 0, qzz = 0;

    soln_.secondGradient( x, y, z, time,
                          q,
                          qx, qy, qz, qt,
                          qxx,
                          qyx, qyy,
                          qzx, qzy, qzz);

    pde.strongFluxAdvectiveTime(x, y, z, time, q, qt, forcing);
    pde.strongFluxAdvective(x, y, z, time, q, qx, qy, qz, forcing);
    pde.strongFluxViscous(x, y, z, time, q, qx, qy, qz, qxx,
                                                        qyx, qyy,
                                                        qzx, qzy, qzz, forcing);
    pde.source(x, y, z, time, q, qx, qy, qz, forcing);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const override
  {
    std::string indent(indentSize, ' ');
    out << indent << "ForcingFunction3D_MMS:" << std::endl;
    soln_.dump(indentSize+2, out);
  }

private:
  const FunctionBase& soln_;  // exact solution
};

} //namespace SANS

#endif //FORCINGFUNCTION3D_MMS_H
