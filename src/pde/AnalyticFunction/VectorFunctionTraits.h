// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef VECTORFUNCTIONTRAITS_H
#define VECTORFUNCTIONTRAITS_H

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"

namespace SANS
{

struct Vector2FunctionTraits
{
  template <class T>
  using ArrayQ = DLA::VectorS<2,T>; // solution/residual arrays

  template <class T>
  using MatrixQ = DLA::MatrixS<2,2,T>; // matrix
};

template<size_t m>
struct VectorFunctionTraits
{
  template <class T>
  using ArrayQ = DLA::VectorS<m,T>; // solution/residual arrays

  template <class T>
  using MatrixQ = DLA::MatrixS<m,m,T>; // matrix
};

} // namespace SANS

#endif  // VECTORFUNCTIONTRAITS_H
