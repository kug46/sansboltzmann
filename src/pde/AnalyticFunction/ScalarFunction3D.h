// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SCALARFUNCTION3D_H
#define SCALARFUNCTION3D_H

// 3-D Advection-Diffusion PDE: exact and MMS solutions

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <cmath>
#include <memory> //shared_ptr

#include "tools/SANSnumerics.h"     // Real

#include "LinearAlgebra/DenseLinAlg/InverseLU.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "Topology/Dimension.h"

#include "Function3D.h"
#include "ScalarFunctionTraits.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// solution: const

class ScalarFunction3D_Const: public Function3DVirtualInterface<ScalarFunction3D_Const, ScalarFunctionTraits>
{
public:
  typedef PhysD3 PhysDim;

  template<class T>
  using ArrayQ = T;    // solution/residual arrays

  explicit ScalarFunction3D_Const(const Real& a0)
      : a0_( a0 )
  {
  }

  template<class T>
  ArrayQ<T> operator()(const T& x, const T& y, const T& z, const T& time) const
  {
    return a0_;
  }

private:
  Real a0_;
};

//----------------------------------------------------------------------------//
// solution: linear

class ScalarFunction3D_Linear: public Function3DVirtualInterface<ScalarFunction3D_Linear, ScalarFunctionTraits>
{
public:
  typedef PhysD3 PhysDim;

  template<class T>
  using ArrayQ = T;    // solution/residual arrays

  ScalarFunction3D_Linear(const Real& a0, const Real& ax, const Real& ay, const Real& az)
    : a0_( a0 ), ax_( ax ), ay_( ay ), az_( az )
  {
  }

  template<class T>
  ArrayQ<T> operator()(const T& x, const T& y, const T& z, const T& time) const
  {
    return a0_ + ax_ * x + ay_ * y + az_ * z;
  }

private:
  Real a0_, ax_, ay_, az_;
};


//----------------------------------------------------------------------------//
// solution: Advection-Reaction Exponential
//
// u =  u0 * exp( alpha*(a*(x0 - x) + b*(y0 - y) + c*(z0 - z))/(a*a + b*b + c*c) )
//
// Exact solution to advection reaction: a ux + b uy + c uz + alpha*u = 0, u(x0,y0,z0) = u0

class ScalarFunction3D_ASExp : public Function3DVirtualInterface<ScalarFunction3D_ASExp, ScalarFunctionTraits>
{
public:
  typedef PhysD3 PhysDim;

  template<class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType: noncopyable
  {
    const ParameterNumeric<Real> u0{ "u0", 1, NO_RANGE, "u at X0"};
    const ParameterNumeric<Real> x0{ "x0", 0, NO_RANGE, "u0 at x0"};
    const ParameterNumeric<Real> y0{ "y0", 0, NO_RANGE, "u0 at y0"};
    const ParameterNumeric<Real> z0{ "z0", 0, NO_RANGE, "u0 at z0"};
    const ParameterNumeric<Real> a { "a", NO_DEFAULT, NO_RANGE, "a x-velocity"};
    const ParameterNumeric<Real> b { "b", NO_DEFAULT, NO_RANGE, "b y-velocity"};
    const ParameterNumeric<Real> c { "c", NO_DEFAULT, NO_RANGE, "c z-velocity"};
    const ParameterNumeric<Real> alpha{ "alpha", NO_DEFAULT, NO_RANGE, "Source strength"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction3D_ASExp( const Real u0, const Real x0, const Real y0, const Real z0,
                                   const Real a, const Real b, const Real c, const Real alpha)
  : u0_(u0), x0_(x0), y0_(y0), z0_(z0), a_(a), b_(b), c_(c), alpha_(alpha) {}

  explicit ScalarFunction3D_ASExp( const PyDict& d ) :
    u0_(d.get(ParamsType::params.u0)),
    x0_(d.get(ParamsType::params.x0)),
    y0_(d.get(ParamsType::params.y0)),
    z0_(d.get(ParamsType::params.z0)),
    a_(d.get(ParamsType::params.a)),
    b_(d.get(ParamsType::params.b)),
    c_(d.get(ParamsType::params.c)),
    alpha_(d.get(ParamsType::params.alpha))
  {}

  template<class T>
  ArrayQ<T> operator()(const T& x, const T& y, const T& z, const T& time) const
  {
    ArrayQ<T> x_sol = exp( alpha_*(x0_ - x)/a_);
    ArrayQ<T> y_sol = exp( alpha_*(y0_ - y)/b_);
    ArrayQ<T> z_sol = exp( alpha_*(z0_ - z)/c_);

    // This was Marshall's old version. Doesn't give sharp characteristics
//    return u0_ * exp( alpha_*(a_*(x0_ - x) + b_*(y0_ - y) + c_*(z0_ - z))/(a_*a_ + b_*b_ + c_*c_) );
    return u0_ * max( x_sol, max(y_sol,z_sol) );
  }


  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction3D_ASExp: " << std::endl;
    out << indent << "  u0_, x0_, y0_, z0_, a_, b_, c_, alpha_ = " << u0_ << ", " << x0_ << ", " << y0_ << ", " << z0_ << ", "
        << a_ << ", " << b_ << ", " << c_ << ", " << alpha_ << std::endl;
  }

private:
  Real u0_, x0_, y0_, z0_, a_, b_, c_, alpha_;
};


//----------------------------------------------------------------------------//
// solution: Exact solution to Laplace equation on a sphere
// Solving
// [1/R] [d(phi)/d(theta)] * 2 pi r h  = C
// where C is a constant, r = R sin(theta), and theta is in 0...pi from pole to pole
//
// The analytical solutions is
//
// phi(theta) = -A * log[ 1/tan(theta) + 1/sin(theta) ] + B
//

class ScalarFunction3D_LaplaceSphere: public Function3DVirtualInterface<ScalarFunction3D_LaplaceSphere, ScalarFunctionTraits>
{
public:
  typedef PhysD3 PhysDim;

  template<class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> thetamin{ "thetamin", NO_DEFAULT, NO_RANGE, "theta min"};
    const ParameterNumeric<Real> thetamax{ "thetamax", NO_DEFAULT, NO_RANGE, "theta max"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction3D_LaplaceSphere( const PyDict& d ) :
    ScalarFunction3D_LaplaceSphere( d.get(ParamsType::params.thetamin), d.get(ParamsType::params.thetamax) )
  {}

  ScalarFunction3D_LaplaceSphere( Real thetamin, Real thetamax ) : A_(0), B_(0)
  {
    DLA::MatrixS<2,2,Real> M;
    DLA::VectorS<2,Real> x = 0, b;

    thetamin *= PI/180; thetamax *= PI/180;

    b[0] = cos(thetamin)+1;
    b[1] = cos(thetamax)+1;

    M(0,0) = -log( 1/tan(thetamin) + 1/sin(thetamin) ); M(0,1) = 1;
    M(1,0) = -log( 1/tan(thetamax) + 1/sin(thetamax) ); M(1,1) = 1;

    x = DLA::InverseLU::Solve(M,b);

    A_ = x[0]; B_ = x[1];
  }

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& z, const T& time ) const
  {
    T r = sqrt(x*x + y*y + z*z);
    T theta = acos(z/r);
    return -A_ * log( 1/tan(theta) + 1/sin(theta) ) + B_;
  }

private:
  Real A_, B_;
};

//----------------------------------------------------------------------------//
// solution: monomial

class ScalarFunction3D_Monomial: public Function3DVirtualInterface<ScalarFunction3D_Monomial, ScalarFunctionTraits>
{
public:
  typedef PhysD3 PhysDim;

  template<class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> i{"i", 2, NO_RANGE, "monomial coefficient"};
    const ParameterNumeric<Real> j{"j", 2, NO_RANGE, "monomial coefficient"};
    const ParameterNumeric<Real> k{"k", 2, NO_RANGE, "monomial coefficient"};

    static void checkInputs(PyDict d);
    static ParamsType params;
  };

  explicit ScalarFunction3D_Monomial( const PyDict& d ) :
    i_(d.get(ParamsType::params.i)),
    j_(d.get(ParamsType::params.j)),
    k_(d.get(ParamsType::params.k)){}

  ScalarFunction3D_Monomial(int i, int j, int k)
      : i_( i ), j_( j ), k_( k )
  {
  }

  template<class T>
  ArrayQ<T> operator()(const T& x, const T& y, const T& z, const T& time) const
  {
    return pow( x, i_ ) * pow( y, j_ ) * pow( z, k_ );
  }

private:
  int i_, j_, k_;
};

//----------------------------------------------------------------------------//
// solution: sin(2*PI*x)*sin(2*PI*y)*sin(2*PI*z)

class ScalarFunction3D_SineSineSine: public Function3DVirtualInterface<ScalarFunction3D_SineSineSine, ScalarFunctionTraits>
{
public:
  typedef PhysD3 PhysDim;

  template<class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> a{"a", 2, NO_RANGE, "Frequency factor"};
    const ParameterNumeric<Real> b{"b", 2, NO_RANGE, "Frequency factor"};
    const ParameterNumeric<Real> c{"c", 2, NO_RANGE, "Frequency factor"};
    const ParameterNumeric<Real> C{"C", 0, NO_RANGE, "Constant offset"};
    const ParameterNumeric<Real> A{"A", 1, NO_RANGE, "Amplitude"};

    static void checkInputs(PyDict d);
    static ParamsType params;
  };

  explicit ScalarFunction3D_SineSineSine( const PyDict& d ) :
    a_(d.get(ParamsType::params.a)),
    b_(d.get(ParamsType::params.b)),
    c_(d.get(ParamsType::params.c)),
    C_(d.get(ParamsType::params.C)),
    A_(d.get(ParamsType::params.A)){}

  ScalarFunction3D_SineSineSine(const Real a=2, const Real b=2, const Real c=2, const Real C=0, const Real A=1)
  : a_(a), b_(b), c_(c), C_(C), A_(A) {}

  ~ScalarFunction3D_SineSineSine() {}

  template<class T>
  ArrayQ<T> operator()(const T& x, const T& y, const T& z, const T& time) const
  {
    return A_*sin( a_ * PI * x ) * sin( b_ * PI * y ) * sin( c_ * PI * z ) + C_;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction3D_SineSineSine: " << std::endl;
    out << indent << "  a_, b_, c_, C_, A_ = " << a_ << ", " << b_ << ", " << c_ << ", " << C_ << ", " << A_ << std::endl;
  }
protected:
  const Real a_,b_,c_,C_,A_;
};

//----------------------------------------------------------------------------//
// solution: sin(2*PI*x)*sin(2*PI*y)*sin(2*PI*z)*sin(2*PI*t)

class ScalarFunction3D_SineSineSineSineUnsteady: public Function3DVirtualInterface<ScalarFunction3D_SineSineSineSineUnsteady, ScalarFunctionTraits>
{
public:
  typedef PhysD3 PhysDim;

  template<class T>
  using ArrayQ = T;    // solution/residual arrays

  typedef ParamsNone ParamsType;

  ScalarFunction3D_SineSineSineSineUnsteady() {}
  explicit ScalarFunction3D_SineSineSineSineUnsteady( const PyDict& d ) {}

  template<class T>
  ArrayQ<T> operator()(const T& x, const T& y, const T& z, const T& time) const
  {
    return sin( 2 * PI * x ) * sin( 2 * PI * y ) * sin( 2 * PI * z ) * sin( 2 * PI * time );
  }
};

//----------------------------------------------------------------------------//
// A decaying-in-time, periodically sinusoidal MMS:
// solution: exp(-lambda*t)*sin(2*pi*x)*sin(2*pi*y)*sin(2*pi*z)
//----------------------------------------------------------------------------//
class ScalarFunction3D_SineSineSineDecay :
    public Function3DVirtualInterface<ScalarFunction3D_SineSineSineDecay, ScalarFunctionTraits>
{
public:
  typedef PhysD3 PhysDim;

  template <class T>
  using ArrayQ= T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> lambda{"lambda", 0.5, 0, NO_LIMIT, "decay rate constant."};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction3D_SineSineSineDecay(PyDict& d) :
          lambda_(d.get(ParamsType::params.lambda))
  {
    // do nothing except for setting in privates.
  }

  explicit ScalarFunction3D_SineSineSineDecay(const Real &lambda) :
          lambda_(lambda)
  {
    // do nothing except for setting in privates.
  }
  ~ScalarFunction3D_SineSineSineDecay() {}

  template<class T> ArrayQ<T> operator() (const T &x, const T &y, const T &z, const T &t) const
  {
    return exp(-lambda_*t)*sin(2*PI*x)*sin(2*PI*y)*sin(2*PI*z);
  }

  void dump(int indentSize= 0, std::ostream& out= std::cout) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction3D_SineSineSineDecay:\tlambda_= " << lambda_ << std::endl;
  }

private:
  Real lambda_;
};

//----------------------------------------------------------------------------//
// A decaying-in-time, periodically cosinusoidal solution that will satisfy the
// advection-diffusion equation!:
// solution: exp(-lambda*t)*cos(omega_x*x - omega_x*a_x*t)
//               *cos(omega_y*y - omega_y*a_y*t)*cos(omega_z*z - omega_z*a_z*t)
// satisfies the advection diffusion equation when:
//    lambda= nu*(omega_x^2 + omega_y^2 + omega_z^2)
//----------------------------------------------------------------------------//
class ScalarFunction3D_AdvectionDiffusionDecayingCosine :
    public Function3DVirtualInterface<
        ScalarFunction3D_AdvectionDiffusionDecayingCosine, ScalarFunctionTraits>
{
public:
  typedef PhysD3 PhysDim;

  template <class T>
  using ArrayQ= T; // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> nu{"nu", NO_DEFAULT, 0, NO_LIMIT, "diffusion coefficient."};
    const ParameterNumeric<Real> a_x{"a_x", NO_DEFAULT, NO_RANGE, "advection coefficient for x-direction."};
    const ParameterNumeric<Real> a_y{"a_y", NO_DEFAULT, NO_RANGE, "advection coefficient for y-direction."};
    const ParameterNumeric<Real> a_z{"a_z", NO_DEFAULT, NO_RANGE, "advection coefficient for z-direction."};
    const ParameterNumeric<Real> omega_x{"omega_x", 2*PI, 0, NO_LIMIT, "cosine frequency for x-direction."};
    const ParameterNumeric<Real> omega_y{"omega_y", 2*PI, 0, NO_LIMIT, "cosine frequency for y-direction."};
    const ParameterNumeric<Real> omega_z{"omega_z", 2*PI, 0, NO_LIMIT, "cosine frequency for z-direction."};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction3D_AdvectionDiffusionDecayingCosine(PyDict &d) :
          nu_(d.get(ParamsType::params.nu)),
          a_x_(d.get(ParamsType::params.a_x)),
          a_y_(d.get(ParamsType::params.a_y)),
          a_z_(d.get(ParamsType::params.a_z)),
          omega_x_(d.get(ParamsType::params.omega_x)),
          omega_y_(d.get(ParamsType::params.omega_y)),
          omega_z_(d.get(ParamsType::params.omega_z))
  {
    // under lambda= nu*(omega_x^2 + omega_y^2 + omega_z^2), exact AD solution
    lambda_= nu_*(omega_x_*omega_x_ + omega_y_*omega_y_ + omega_z_*omega_z_);
  }

  explicit ScalarFunction3D_AdvectionDiffusionDecayingCosine(const Real &nu,
                                                             const Real &a_x,
                                                             const Real &a_y,
                                                             const Real &a_z,
                                                             const Real &omega_x,
                                                             const Real &omega_y,
                                                             const Real &omega_z) :
          nu_(nu),
          a_x_(a_x),
          a_y_(a_y),
          a_z_(a_z),
          omega_x_(omega_x),
          omega_y_(omega_y),
          omega_z_(omega_z)
  {
    // under lambda= nu*(omega_x^2 + omega_y^2 + omega_z^2), exact AD solution
    lambda_= nu_*(omega_x_*omega_x_ + omega_y_*omega_y_ + omega_z_*omega_z_);
  }

  ~ScalarFunction3D_AdvectionDiffusionDecayingCosine() {}

  template <class T> ArrayQ<T> operator() (const T &x, const T &y, const T &z, const T &t) const
  {
    return exp(-lambda_*t)*cos(omega_x_*x - omega_x_*a_x_*t)
               *cos(omega_y_*y - omega_y_*a_y_*t)*cos(omega_z_*z - omega_z_*a_z_*t);
  }

  void dump(int indentSize= 0, std::ostream &out= std::cout) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction3D_AdvectionDiffusionDecayingCosine:" << std::endl;
    out << indent << "\tnu_= " << nu_ << std::endl;
    out << indent << "\ta_x_= " << a_x_ << std::endl;
    out << indent << "\ta_y_= " << a_y_ << std::endl;
    out << indent << "\ta_z_= " << a_z_ << std::endl;
    out << indent << "\tomega_x_= " << omega_x_ << std::endl;
    out << indent << "\tomega_y_= " << omega_y_ << std::endl;
    out << indent << "\tomega_z_= " << omega_z_ << std::endl;
  }

private:
  Real nu_;
  Real a_x_;
  Real a_y_;
  Real a_z_;
  Real omega_x_;
  Real omega_y_;
  Real omega_z_;
  Real lambda_;
};

//----------------------------------------------------------------------------//
// An expanding spherical wave with decaying (in time) intensity
//----------------------------------------------------------------------------//
class ScalarFunction3D_SphericalWaveDecay :
    public Function3DVirtualInterface<ScalarFunction3D_SphericalWaveDecay, ScalarFunctionTraits>
{
public:
  typedef PhysD3 PhysDim;

  template <class T>
  using ArrayQ= T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> alpha{"alpha", 1.0 , 0, NO_LIMIT, "temporal decay rate of intensity."};
    const ParameterNumeric<Real> k0{"k0", 1.0 , 0, NO_LIMIT, "initial intensity."};
    const ParameterNumeric<Real> k1{"k1", 100.0 , 0, NO_LIMIT, "spatial decay rate of intensity."};
    const ParameterNumeric<Real> velocity{"velocity", 0.5 , 0, NO_LIMIT, "constant wave propagation velocity."};
    const ParameterNumeric<Real> radius0{"radius0", 0.4 , 0.00001, 0.999999, "initial wave radius."};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction3D_SphericalWaveDecay(PyDict& d) :
    alpha_(d.get(ParamsType::params.alpha)),
    k0_(d.get(ParamsType::params.k0)),
    k1_(d.get(ParamsType::params.k1)),
    velocity_(d.get(ParamsType::params.velocity)),
    radius0_(d.get(ParamsType::params.radius0))
  {}

  explicit ScalarFunction3D_SphericalWaveDecay(const Real& alpha , const Real& k0 , const Real& k1 , const Real& velocity , const Real& radius0 ) :
    alpha_(alpha), k0_(k0), k1_(k1), velocity_(velocity), radius0_(radius0)
  {}

  ~ScalarFunction3D_SphericalWaveDecay() {}

  template<class T> ArrayQ<T> radius( const T& t ) const
  {
    return radius0_ +velocity_*t;
  }

  template<class T> ArrayQ<T> operator() (const T &x0, const T &y0, const T &z0, const T &t) const
  {
    const T x = x0 +offset_;
    const T y = y0 +offset_;
    const T z = z0 +offset_;
    const T R = radius(t);
    const T r = sqrt(x*x+y*y+z*z);
    return k0_*exp(-alpha_*t)*exp( -k1_*(R-r)*(R-r) );
    //if (u<1e-6) return 0;
    //return u;
    //return k0_*exp(-alpha_*t)*exp( -k1_*pow(radius(t)-sqrt(x*x+y*y+z*z),2.) );
  }

  void dump(int indentSize= 0, std::ostream& out= std::cout) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction3D_SphericalWaveDecay:\nalpha_= " << alpha_
    << "\nk0_ = " << k0_ << "\nk1_ = " << k1_
    << "\nV_ = " << velocity_ << "\nr0 = " << radius0_ << std::endl;

    // plus more parameters here i suppose...
  }

private:
  Real alpha_;
  Real k0_;
  Real k1_;
  Real velocity_;
  Real radius0_;
  const Real offset_ = 0.0;
};


//----------------------------------------------------------------------------//
// solution: single BL
//
// u(0,0,0) = 1  u(1,y,z) = 0  u(x,1,z) = 1  u(x,y,1) = 1
//
// NOTE: exact solution for 3D advection-diffusion: a ux + b uy + c uz - nu (uxx + uyy + uzz) = 0

class ScalarFunction3D_BoundaryLayer: public Function3DVirtualInterface<ScalarFunction3D_BoundaryLayer, ScalarFunctionTraits>
{
public:
  typedef PhysD3 PhysDim;

  template<class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType: noncopyable
  {
    const ParameterNumeric<Real> epsilon{"epsilon", NO_DEFAULT, 0, NO_LIMIT, "characteristic length of singular perturbation"};
    const ParameterNumeric<Real> beta{"beta", NO_DEFAULT, 0, NO_LIMIT, "regularization constant"};
    const ParameterNumeric<int> p{"p", NO_DEFAULT, 0, NO_LIMIT, "solution order"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction3D_BoundaryLayer( const PyDict& d ) :
                epsilon_(d.get(ParamsType::params.epsilon)),
                beta_(d.get(ParamsType::params.beta)),
                p_(d.get(ParamsType::params.p)) {}

  ScalarFunction3D_BoundaryLayer( const Real& epsilon, const Real& beta, const int& p) :
    epsilon_(epsilon),
    beta_(beta),
    p_(p)
  {
    SANS_ASSERT(epsilon_ > 0);
    SANS_ASSERT(beta_ > 0);
    SANS_ASSERT(p_ > 0);
  }

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& z, const T& time ) const
  {
    return exp(-x/epsilon_) + (beta_/factorial(p_+1))*pow(y,(p_+1));
  }


  static int
  factorial( int n )
  {
    int m;
    if (n <= 1)
      m = 1;
    else
      m = n * factorial(n - 1);

    return m;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction3D_BoundaryLayer: " << std::endl;
    out << indent << "  epsilon_, beta_, p_ = " << epsilon_ << ", " << beta_ << ", " << p_ << std::endl;
  }
private:
  Real epsilon_, beta_;
  int p_;
};

//----------------------------------------------------------------------------//
// solution: 4D boundary layer resulting from singular perturbation
//
// u(x,y,z,t) = exp(-x/e) + beta_y/((p+1)!)*y^(p+1) beta_z/((p+1)!)*z^(p+1) +beta_t/((p+1)!)*t^(p+1)

class ScalarFunction3D_BoundaryLayerSingularPerturbation :
  public Function3DVirtualInterface<ScalarFunction3D_BoundaryLayerSingularPerturbation, ScalarFunctionTraits>
{
public:
  typedef PhysD3 PhysDim;

  template <class T>
  using ArrayQ = T;

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> epsilon{"epsilon", NO_DEFAULT, 0, NO_LIMIT, "mean characteristic length of singular perturbation"};
    const ParameterNumeric<Real> betay{"betay", NO_DEFAULT, 0, NO_LIMIT, "regularization constant in y"};
    const ParameterNumeric<Real> betaz{"betaz", NO_DEFAULT, 0, NO_LIMIT, "regularization constant in z"};
    const ParameterNumeric<Real> betat{"betat", NO_DEFAULT, 0, NO_LIMIT, "regularization constant in t"};
    const ParameterNumeric<int> p{"p", NO_DEFAULT, 0, NO_LIMIT, "solution order"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction3D_BoundaryLayerSingularPerturbation( const PyDict& d ) :
    ScalarFunction3D_BoundaryLayerSingularPerturbation(
      d.get(ParamsType::params.epsilon) ,
      d.get(ParamsType::params.betay),
      d.get(ParamsType::params.betaz),
      d.get(ParamsType::params.betat),
      d.get(ParamsType::params.p)
    )
  {}

  ScalarFunction3D_BoundaryLayerSingularPerturbation( const Real& epsilon ,
                                  const Real& betay , const Real& betaz , const Real& betat ,
                                  const int& p ) :
    epsilon_(epsilon),
    betay_(betay), betaz_(betaz), betat_(betat), p_(p)
  {
    SANS_ASSERT(epsilon_ > 0);
    SANS_ASSERT(betay_ >= 0);
    SANS_ASSERT(betaz_ >= 0);
    SANS_ASSERT(betat_ >= 0);
    SANS_ASSERT(p_ > 0);
  }

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& z, const T& t ) const
  {
    return exp(-x/epsilon_) + (betay_/factorial(p_+1))*pow(y,(p_+1))
          + (betaz_/factorial(p_+1))*pow(z,(p_+1))
          + (betat_/factorial(p_+1))*pow(t,(p_+1));
  }

  static int
  factorial( int n )
  {
    int m;
    if (n <= 1)
      m = 1;
    else
      m = n * factorial(n - 1);

    return m;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction3D_BoundaryLayerSingularPerturbation: " << std::endl;
  }
private:
  Real epsilon_;
  Real betay_, betaz_, betat_;
  int p_;
};

//----------------------------------------------------------------------------//
// solution: triple BL
//
// u(0,0,0) = 1  u(1,y,z) = 0  u(x,1,z) = 0  u(x,y,1) = 0
//
// NOTE: exact solution for 3D advection-diffusion: a ux + b uy + c uz - nu (uxx + uyy + uzz) = 0

class ScalarFunction3D_TripleBL: public Function3DVirtualInterface<ScalarFunction3D_TripleBL, ScalarFunctionTraits>
{
public:
  typedef PhysD3 PhysDim;

  template<class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType: noncopyable
  {
    const ParameterNumeric<Real> a { "a", NO_DEFAULT, NO_RANGE, "a x-velocity"};
    const ParameterNumeric<Real> b { "b", NO_DEFAULT, NO_RANGE, "b y-velocity"};
    const ParameterNumeric<Real> c { "c", NO_DEFAULT, NO_RANGE, "c z-velocity"};
    const ParameterNumeric<Real> nu{ "nu", NO_DEFAULT, NO_RANGE, "viscosity"};
    const ParameterNumeric<Real> offset{ "offset", 0.0, NO_RANGE, "constant offset"};
    const ParameterNumeric<Real> scale{ "scale", 1.0, NO_RANGE, "scaling"};
    const ParameterBool temporal{ "temporal", false , "whether the temporal term should be added (QuadBL)"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction3D_TripleBL( const PyDict& d ) :
    a_(d.get(ParamsType::params.a)),
    b_(d.get(ParamsType::params.b)),
    c_(d.get(ParamsType::params.c)),
    nu_(d.get(ParamsType::params.nu)),
    offset_(d.get(ParamsType::params.offset)),
    scale_(d.get(ParamsType::params.scale)),
    temporal_(d.get(ParamsType::params.temporal))
  {}

  ScalarFunction3D_TripleBL( const Real& a, const Real& b, const Real& c, const Real& nu,
                             const Real& offset = 0.0, const Real& scale = 1.0 ) :
    a_(a), b_(b), c_(c), nu_(nu), offset_(offset), scale_(scale),
    temporal_(false)
  {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& z, const T& time ) const
  {
    T f, g, h;
    f = (1 - exp(-a_*(1 - x)/nu_)) / (1 - exp(-a_/nu_));
    g = (1 - exp(-b_*(1 - y)/nu_)) / (1 - exp(-b_/nu_));
    h = (1 - exp(-c_*(1 - z)/nu_)) / (1 - exp(-c_/nu_));
    T i = 1;
    if (temporal_)
    {
      // the "temporal" velocity needs to be 1 to get the exact solution with advection-diffusion
      i  = (1 - exp(-(1 - time)/nu_)) / (1 - exp(-1/nu_));
    }
    return scale_*f*g*h*i + offset_;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction3D_TripleBL: " << std::endl;
    out << indent << "  a_, b_, c_, nu_, offset_, scale_ = " << a_ << ", " << b_ << ", " << c_ << ", "
    << nu_ << ", " << offset_ << ", " << scale_ << std::endl;
  }

private:
  Real a_, b_, c_;        // advection velocity
  Real nu_;// diffusion coefficient
  Real offset_, scale_; // scaling factor and offset
  bool temporal_;
};

//----------------------------------------------------------------------------//
// solution: triple BL w/ temporal oscillation
//
// NOTE: exact solution for 3D advection-diffusion: ut + a ux + b uy + c uz - nu (uxx + uyy + uzz) = f
//     where f= sin(omega*t)!

class ScalarFunction3D_TripleBLsine: public Function3DVirtualInterface<ScalarFunction3D_TripleBLsine, ScalarFunctionTraits>
{
public:
  typedef PhysD3 PhysDim;

  template<class T>
  using ArrayQ= T; // solution/residual arrays

  struct ParamsType: noncopyable
  {
    const ParameterNumeric<Real> a {"a", NO_DEFAULT, NO_RANGE, "a x-velocity"};
    const ParameterNumeric<Real> b {"b", NO_DEFAULT, NO_RANGE, "b y-velocity"};
    const ParameterNumeric<Real> c {"c", NO_DEFAULT, NO_RANGE, "c z-velocity"};
    const ParameterNumeric<Real> nu {"nu", NO_DEFAULT, NO_RANGE, "viscosity"};
    const ParameterNumeric<Real> omega {"omega", 1.0, NO_RANGE, "forcing sine frequency"};
    const ParameterNumeric<Real> offset {"offset", 0.0, NO_RANGE, "constant offset"};
    const ParameterNumeric<Real> scale {"scale", 1.0, NO_RANGE, "scaling"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction3D_TripleBLsine(const PyDict &d) :
      a_(d.get(ParamsType::params.a)),
      b_(d.get(ParamsType::params.b)),
      c_(d.get(ParamsType::params.c)),
      nu_(d.get(ParamsType::params.nu)),
      omega_(d.get(ParamsType::params.omega)),
      offset_(d.get(ParamsType::params.offset)),
      scale_(d.get(ParamsType::params.scale))
  {}

  ScalarFunction3D_TripleBLsine(const Real &a, const Real &b, const Real &c,
                                       const Real &nu, const Real &omega,
                                       const Real &offset= 0.0, const Real &scale= 1.0) :
      a_(a), b_(b), c_(c), nu_(nu), omega_(omega), offset_(offset), scale_(scale)
  {}

  template<class T>
  ArrayQ<T> operator()( const T &x, const T &y, const T &z, const T &time) const
  {
    T f;
    T g;
    T h;
    T i;
    f= (1 - exp(-a_*(1 - x)/nu_))/(1 - exp(-a_/nu_));
    g= (1 - exp(-b_*(1 - y)/nu_))/(1 - exp(-b_/nu_));
    h= (1 - exp(-c_*(1 - z)/nu_))/(1 - exp(-c_/nu_));
    i= 1.0 - 1./omega_*cos(omega_*time);

    return scale_*f*g*h*i + offset_;
  }

  void dump(int indentSize= 0, std::ostream &out= std::cout) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction3D_TripleBLsine: " << std::endl;
    out << indent << "  a_, b_, c_, nu_, omega_, offset_, scale_= " << a_ << ", "
        << b_ << ", " << c_ << ", " << nu_ << ", " << omega_
        << ", " << offset_ << ", " << scale_ << std::endl;
  }

private:
  Real a_; // advection velocity, x
  Real b_; // advection velocity, y
  Real c_; // advection velocity, z
  Real nu_; // diffusion coefficient
  Real omega_; // forcing sinusoid frequency
  Real offset_; // offset
  Real scale_; // scaling factor
};

//----------------------------------------------------------------------------//
// solution: r^alpha type corner singularity
//
// u(x,y) = r^alpha sin( alpha*(theta + theta0) ),  r^2 = x^2 + y^2, theta = atan2(y,x)

class ScalarFunction3D_CornerSingularity : public Function3DVirtualInterface<ScalarFunction3D_CornerSingularity,ScalarFunctionTraits>
{
public:
  typedef PhysD3 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> alpha{"alpha", NO_DEFAULT, 0, NO_LIMIT, "strength of singularity"};
    const ParameterNumeric<Real> theta0{"theta0", 0, NO_RANGE, "offset angle"};
    const ParameterNumeric<Real> theta1{"theta1", 0, NO_RANGE, "offset angle"};
    const ParameterNumeric<Real> theta2{"theta2", 0, NO_RANGE, "offset angle"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction3D_CornerSingularity( const PyDict& d ) :
            alpha_(d.get(ParamsType::params.alpha)),
            theta0_(d.get(ParamsType::params.theta0)) {}

  ScalarFunction3D_CornerSingularity( const Real& alpha, const Real& theta0,
          const Real& theta1, const Real& theta2)
      : alpha_(alpha), theta0_(theta0), theta1_(theta1), theta2_(theta2)
  {
    SANS_ASSERT(alpha_ > 0);
  }

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& z, const T& time ) const
  {
    T r = sqrt( x*x + y*y +z*z);
    T theta0 = atan2(y,x);
    T theta1 = atan2(z,x);
    T theta2 = atan2(z,y);
    return pow(r,alpha_)*sin(alpha_*(theta0 + theta0_))*sin(alpha_*(theta1 + theta1_))*sin(alpha_*(theta2 + theta2_));
  }

  template<class T>
  ArrayQ<T> operator()( const DLA::VectorS<PhysDim::D,T>& X ) const
  {
    return operator()(X[0],X[1],X[2],0.0);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction3D_CornerSingularity: " << std::endl;
    out << indent << "  alpha_, theta0_, theta1_, theta2_ = " << alpha_ << ", " << theta0_ << ", " << theta1_ << ", " << theta2_ << std::endl;
  }
private:
  Real alpha_, theta0_, theta1_, theta2_;    // singularity parameters
};

//------------------------------------------------------------------------------------------------
// assume solution is of the type: Gaussian
//
class ScalarFunction3D_Gaussian : public Function3DVirtualInterface<ScalarFunction3D_Gaussian, ScalarFunctionTraits>
{
public:
  typedef PhysD3 PhysDim;

  template<class T>
  using ArrayQ = T; //solution array or residual

  struct ParamsType: noncopyable
  {
    const ParameterNumeric<Real> sigma { "sigma", 1, NO_RANGE, "sigma "};
    const ParameterNumeric<Real> x0{ "x0", 1, NO_RANGE, "x0 "};
    const ParameterNumeric<Real> y0{ "y0", 2, NO_RANGE, "y0 "};
    const ParameterNumeric<Real> z0{ "z0", 3, NO_RANGE, "z0 "};

    static void checkInputs(PyDict d);
    static ParamsType params;
  };

  explicit ScalarFunction3D_Gaussian( const PyDict& d) :
    sigma_(d.get(ParamsType::params.sigma)),
    x0_(d.get(ParamsType::params.x0)),
    y0_(d.get(ParamsType::params.y0)),
    z0_(d.get(ParamsType::params.z0))
  {}

  ScalarFunction3D_Gaussian(const Real sigma, const Real x0, const Real y0, const Real z0) :
    sigma_(sigma), x0_(x0), y0_(y0), z0_(z0)
  {}

  template <class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& z, const T&time ) const
  {
    return (1/pow(sqrt(2*PI)*sigma_,3))*exp((-1/(2*sigma_*sigma_))*(pow(x-x0_,2)+pow(y-y0_,2)+pow(z-z0_,2)));
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction3z0_Gaussian:" << std::endl;
    out << indent << "sigma_ = " << sigma_ << std::endl;
    out << indent << "x0_ = " << x0_ << std::endl;
    out << indent << "y0_ = " << y0_ << std::endl;
    out << indent << "z0_ = " << z0_ << std::endl;
  }

private:
  Real sigma_;
  Real x0_;
  Real y0_;
  Real z0_;
};


//----------------------------------------------------------------------------//
// solution: tahn3 from a paper by Adrien Loseille (not sure which...)
//
// This can only be used for L2 projection as the gradient and hessian
// have singularities (i.e. 1/0 stuff)
//
// u = tanh(pow(x + 1.3, 20.0) * pow(y - 0.3, 9.0) * z)
//

class ScalarFunction3D_Tanh3 : public Function3DBase<Real>
{
public:
  typedef PhysD3 PhysDim;

  template<class T>
  using ArrayQ = T;    // solution/residual arrays

  typedef ParamsNone ParamsType;

  explicit ScalarFunction3D_Tanh3( const PyDict& d ) {}

  ScalarFunction3D_Tanh3() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& z, const T& time ) const
  {
    return tanh(pow(x + 1.3, 20.0) * pow(y - 0.3, 9.0) * z);
  }

  virtual ArrayQ<Real> operator()( const Real& x, const Real& y, const Real& z, const Real& time ) const override
  {
    return this->template operator()<Real>(x, y, z, time);
  }

  virtual void gradient( const Real& x, const Real& y, const Real& z, const Real& time, ArrayQ<Real>& q,
                         ArrayQ<Real>& qx, ArrayQ<Real>& qy, ArrayQ<Real>& qz, ArrayQ<Real>& qt ) const override
  {
    q = this->operator()(x, y, z, time);
    SANS_DEVELOPER_EXCEPTION("Gradient can be singular for tanh3");
    qx = qy = qz = qt = 0;
    return;
#if 0
    auto sech = [](Real s) { return 2/(exp(s) + exp(-s)); };

    qx = 20.*pow(1.3 + x,19.)*pow(-0.3 + y,9.)*z*pow(sech(pow(1.3 + x,20.)*pow(-0.3 + y,9.)*z),2);
    qy =  9.*pow(1.3 + x,20.)*pow(-0.3 + y,8.)*z*pow(sech(pow(1.3 + x,20.)*pow(-0.3 + y,9.)*z),2);
    qz =     pow(1.3 + x,20.)*pow(-0.3 + y,9.)*  pow(sech(pow(1.3 + x,20.)*pow(-0.3 + y,9.)*z),2);

    qt = 0;
#endif
  }

  virtual void secondGradient( const Real& x, const Real& y, const Real& z, const Real& time, ArrayQ<Real>& q,
                               ArrayQ<Real>& qx, ArrayQ<Real>& qy, ArrayQ<Real>& qz, ArrayQ<Real>& qt,
                               ArrayQ<Real>& qxx,
                               ArrayQ<Real>& qyx, ArrayQ<Real>& qyy,
                               ArrayQ<Real>& qzx, ArrayQ<Real>& qzy, ArrayQ<Real>& qzz ) const override
  {
    this->gradient(x, y, z, time, q, qx, qy, qz, qt);
    SANS_DEVELOPER_EXCEPTION("Gradient can be singular for tanh3");
    qxx = qyx = qyy = qzx = qzy = qzz = 0;
    return;
#if 0
    auto sech = [](Real s) { return 2/(exp(s) + exp(-s)); };

    qxx = 380.*pow(1.3 + x,18.)*pow(-0.3 + y,9.)*z*
        pow(sech(pow(1.3 + x,20.)*pow(-0.3 + y,9.)*z),2) -
       800.*pow(1.3 + x,38.)*pow(-0.3 + y,18.)*pow(z,2)*
        pow(sech(pow(1.3 + x,20.)*pow(-0.3 + y,9.)*z),2)*
        tanh(pow(1.3 + x,20.)*pow(-0.3 + y,9.)*z);

    qyx = 180.*pow(1.3 + x,19.)*pow(-0.3 + y,8.)*z*
        pow(sech(pow(1.3 + x,20.)*pow(-0.3 + y,9.)*z),2) -
       360.*pow(1.3 + x,39.)*pow(-0.3 + y,17.)*pow(z,2)*
        pow(sech(pow(1.3 + x,20.)*pow(-0.3 + y,9.)*z),2)*
        tanh(pow(1.3 + x,20.)*pow(-0.3 + y,9.)*z);

    qyy = 72.*pow(1.3 + x,20.)*pow(-0.3 + y,7.)*z*
        pow(sech(pow(1.3 + x,20.)*pow(-0.3 + y,9.)*z),2) -
       162.*pow(1.3 + x,40.)*pow(-0.3 + y,16.)*pow(z,2)*
        pow(sech(pow(1.3 + x,20.)*pow(-0.3 + y,9.)*z),2)*
        tanh(pow(1.3 + x,20.)*pow(-0.3 + y,9.)*z);

    qzx = 20.*pow(1.3 + x,19.)*pow(-0.3 + y,9.)*
        pow(sech(pow(1.3 + x,20.)*pow(-0.3 + y,9.)*z),2) -
       40.*pow(1.3 + x,39.)*pow(-0.3 + y,18.)*z*
        pow(sech(pow(1.3 + x,20.)*pow(-0.3 + y,9.)*z),2)*
        tanh(pow(1.3 + x,20.)*pow(-0.3 + y,9.)*z);

    qzy = 9.*pow(1.3 + x,20.)*pow(-0.3 + y,8.)*
        pow(sech(pow(1.3 + x,20.)*pow(-0.3 + y,9.)*z),2) -
       18.*pow(1.3 + x,40.)*pow(-0.3 + y,17.)*z*
        pow(sech(pow(1.3 + x,20.)*pow(-0.3 + y,9.)*z),2)*
        tanh(pow(1.3 + x,20.)*pow(-0.3 + y,9.)*z);

    qzz = -2*pow(1.3 + x,40.)*pow(-0.3 + y,18.)*
        pow(sech(pow(1.3 + x,20.)*pow(-0.3 + y,9.)*z),2)*
        tanh(pow(1.3 + x,20.)*pow(-0.3 + y,9.)*z);
#endif
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const override
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction3D_Tanh3: " << std::endl;
  }

private:
};


//----------------------------------------------------------------------------//
// solution: sinatan3 from a paper by Adrien Loseille (not sure which...)
//
// eps = 0.1;
// xz = x * z;
// u = 0.1 * sin(50.0 * xz) + atan(eps / (sin(5.0 * y) - 2.0 * xz));
//
// This can only be used for L2 projection as the gradient and hessian have been removed
//

class ScalarFunction3D_SinATan3 : public Function3DBase<Real>
{
public:
  typedef PhysD3 PhysDim;

  template<class T>
  using ArrayQ = T;    // solution/residual arrays

  typedef ParamsNone ParamsType;

  explicit ScalarFunction3D_SinATan3( const PyDict& d ) {}

  ScalarFunction3D_SinATan3() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& z, const T& time ) const
  {
    Real eps = 0.1;
    T xz = x*z;
    return 0.1 * sin(50.0 * xz) + atan(eps / (sin(5.0 * y) - 2.0 * xz));
  }

  virtual ArrayQ<Real> operator()( const Real& x, const Real& y, const Real& z, const Real& time ) const override
  {
    return this->template operator()<Real>(x, y, z, time);
  }

  virtual void gradient( const Real& x, const Real& y, const Real& z, const Real& time, ArrayQ<Real>& q,
                         ArrayQ<Real>& qx, ArrayQ<Real>& qy, ArrayQ<Real>& qz, ArrayQ<Real>& qt ) const override
  {
    SANS_DEVELOPER_EXCEPTION("Gradient not implemented for sinatan3");
    q = this->operator()(x, y, z, time);
    qx = qy = qz = qt = 0;
    return;
  }

  virtual void secondGradient( const Real& x, const Real& y, const Real& z, const Real& time, ArrayQ<Real>& q,
                               ArrayQ<Real>& qx, ArrayQ<Real>& qy, ArrayQ<Real>& qz, ArrayQ<Real>& qt,
                               ArrayQ<Real>& qxx,
                               ArrayQ<Real>& qyx, ArrayQ<Real>& qyy,
                               ArrayQ<Real>& qzx, ArrayQ<Real>& qzy, ArrayQ<Real>& qzz ) const override
  {
    SANS_DEVELOPER_EXCEPTION("Gradient not implemented for sinatan3");
    q = this->operator()(x, y, z, time);
    qx = qy = qz = qt = 0;
    qxx = qyx = qyy = qzx = qzy = qzz = 0;
    return;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const override
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction3D_SinATan3: " << std::endl;
  }

private:
};


//----------------------------------------------------------------------------//
// solution: sinfun3 from a paper by Adrien Loseille (not sure which...)
//
// xyz = (x - 0.4) * (y - 0.4) * (z - 0.4); /* sphere2 */
//
// if (xyz <= (-1.0 * PI / 50.0))
//   u = 0.1 * sin(50. * xyz);
// else if (xyz <= (2.0 * PI / 50.0))
//   u = sin(50.0 * xyz);
// else
//   u = 0.1 * sin(50.0 * xyz);
//
// This can only be used for L2 projection as the gradient and hessian have been removed
//

class ScalarFunction3D_SinFun3 : public Function3DBase<Real>
{
public:
  typedef PhysD3 PhysDim;

  template<class T>
  using ArrayQ = T;    // solution/residual arrays

  typedef ParamsNone ParamsType;

  explicit ScalarFunction3D_SinFun3( const PyDict& d ) {}

  ScalarFunction3D_SinFun3() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& z, const T& time ) const
  {
    T xyz = (x - 0.4) * (y - 0.4) * (z - 0.4); /* sphere2 */

    if (xyz <= (-1.0 * PI / 50.0))
      return 0.1 * sin(50. * xyz);
    else if (xyz <= (2.0 * PI / 50.0))
      return sin(50.0 * xyz);
    else
      return 0.1 * sin(50.0 * xyz);
  }

  virtual ArrayQ<Real> operator()( const Real& x, const Real& y, const Real& z, const Real& time ) const override
  {
    return this->template operator()<Real>(x, y, z, time);
  }

  virtual void gradient( const Real& x, const Real& y, const Real& z, const Real& time, ArrayQ<Real>& q,
                         ArrayQ<Real>& qx, ArrayQ<Real>& qy, ArrayQ<Real>& qz, ArrayQ<Real>& qt ) const override
  {
    SANS_DEVELOPER_EXCEPTION("Gradient not implemented for sinfun3");
    q = this->operator()(x, y, z, time);
    qx = qy = qz = qt = 0;
    return;
  }

  virtual void secondGradient( const Real& x, const Real& y, const Real& z, const Real& time, ArrayQ<Real>& q,
                               ArrayQ<Real>& qx, ArrayQ<Real>& qy, ArrayQ<Real>& qz, ArrayQ<Real>& qt,
                               ArrayQ<Real>& qxx,
                               ArrayQ<Real>& qyx, ArrayQ<Real>& qyy,
                               ArrayQ<Real>& qzx, ArrayQ<Real>& qzy, ArrayQ<Real>& qzz ) const override
  {
    SANS_DEVELOPER_EXCEPTION("Gradient not implemented for sinfun3");
    q = this->operator()(x, y, z, time);
    qx = qy = qz = qt = 0;
    qxx = qyx = qyy = qzx = qzy = qzz = 0;
    return;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const override
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction3D_SinFun3: " << std::endl;
  }

private:
};

//----------------------------------------------------------------------------//
class ScalarFunction3D_OscillatingGaussian :
    public Function3DVirtualInterface<
        ScalarFunction3D_OscillatingGaussian, ScalarFunctionTraits>
{
public:
  typedef PhysD3 PhysDim;

  template <class T>
  using ArrayQ= T; // solution/residual arrays

  struct ParamsType : noncopyable
  {

    const ParameterNumeric<Real> sigma{"sigma", 1.0, 0, NO_LIMIT, "scale coefficient, x direction"};

    const ParameterNumeric<Real> x0{"x0", 0.0, NO_RANGE, "location parameter, x direction"};
    const ParameterNumeric<Real> y0{"y0", 0.0, NO_RANGE, "location parameter, y direction"};
    const ParameterNumeric<Real> z0{"z0", 0.0, NO_RANGE, "location parameter, z direction"};
    const ParameterNumeric<Real> tMin{"tMin", 0.0, NO_RANGE, "trough of oscillation"};
    const ParameterNumeric<Real> tMax{"tMax", 1.0, NO_RANGE, "peak of oscillation"};
    const ParameterNumeric<Real> f{"f", 1.0, 0.0, NO_LIMIT, "frequency of oscillation"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction3D_OscillatingGaussian( const PyDict& d) :
    sigma_(d.get(ParamsType::params.sigma)),
    x0_(d.get(ParamsType::params.x0)),
    y0_(d.get(ParamsType::params.y0)),
    z0_(d.get(ParamsType::params.z0)),
    tMin_(d.get(ParamsType::params.tMin)),
    tMax_(d.get(ParamsType::params.tMax)),
    f_(d.get(ParamsType::params.f))
  {}

  ScalarFunction3D_OscillatingGaussian(const Real sigma,
      const Real x0, const Real y0, const Real z0,
      const Real tMin, const Real tMax, const Real f) :
    sigma_(sigma), x0_(x0), y0_(y0), z0_(z0), tMin_(tMin), tMax_(tMax), f_(f)
  {}

  template <class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& z, const T&time ) const
  {
    return (0.5*(tMin_ + tMax_) + 0.5*(tMax_ - tMin_)*sin(2*PI*f_*time))
        *(1/pow(sqrt(2*PI)*sigma_, 3))*exp((-1/(2*sigma_*sigma_))
        *(pow(x - x0_,2) + pow(y - y0_, 2) + pow(z - z0_,2)));
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction3D_OscillatingGaussian:" << std::endl;
    out << indent << "sigma_ = " << sigma_ << std::endl;
    out << indent << "x0_ = " << x0_ << std::endl;
    out << indent << "y0_ = " << y0_ << std::endl;
    out << indent << "z0_ = " << z0_ << std::endl;
    out << indent << "tMin_ = " << tMin_ << std::endl;
    out << indent << "tMax_ = " << tMax_ << std::endl;
    out << indent << "f_ = " << f_ << std::endl;
  }

private:
  Real sigma_;
  Real x0_;
  Real y0_;
  Real z0_;
  Real tMin_;
  Real tMax_;
  Real f_;
};

//----------------------------------------------------------------------------//
// an exact solution that couples with the similarly name advective flux to give
// an unforced analytical solution to the advection-diffusion equation for a
// heat-transfer problem in a a square channel
class ScalarFunction3D_AdvectionDiffusionSquareChannel :
    public Function3DVirtualInterface<
        ScalarFunction3D_AdvectionDiffusionSquareChannel, ScalarFunctionTraits>
{
public:
  typedef PhysD3 PhysDim;

  template <class T>
  using ArrayQ= T; // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> a{"a", 1.0, NO_RANGE, "scaling coefficient"};
    const ParameterNumeric<Real> b{"b", 1.0, NO_RANGE, "shape coefficient for x gaussian"};
    const ParameterNumeric<Real> c{"c", 1.0, NO_RANGE, "shape coefficient for y gaussian"};
    const ParameterNumeric<Real> lambda_z{"lambda_z", 1.0, NO_RANGE, "growth/decay coefficient for z"};
    const ParameterNumeric<Real> lambda_t{"lambda_t", 1.0, NO_RANGE, "growth/decay coefficient for t"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction3D_AdvectionDiffusionSquareChannel(PyDict &d) :
          a_(d.get(ParamsType::params.a)),
          b_(d.get(ParamsType::params.b)),
          c_(d.get(ParamsType::params.c)),
          lambda_z_(d.get(ParamsType::params.lambda_z)),
          lambda_t_(d.get(ParamsType::params.lambda_t)) {}

  explicit ScalarFunction3D_AdvectionDiffusionSquareChannel(const Real &a,
                                                            const Real &b,
                                                            const Real &c,
                                                            const Real &lambda_z,
                                                            const Real &lambda_t) :
          a_(a), b_(b), c_(c), lambda_z_(lambda_z), lambda_t_(lambda_t) {}

  ~ScalarFunction3D_AdvectionDiffusionSquareChannel() {}

  template <class T> ArrayQ<T> operator() (const T &x, const T &y, const T &z, const T &time) const
  {
    return a_*exp(-1./b_*x*x - 1./c_*y*y + lambda_z_*z + lambda_t_*time);
  }

  void dump(int indentSize= 0, std::ostream &out= std::cout) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction3D_AdvectionDiffusionSquareChannel:" << std::endl;
    out << indent << "\ta_= " << a_ << std::endl;
    out << indent << "\tb_= " << b_ << std::endl;
    out << indent << "\tc_= " << c_ << std::endl;
    out << indent << "\tlambda_z_= " << lambda_z_ << std::endl;
    out << indent << "\tlambda_t_= " << lambda_t_ << std::endl;
  }

private:
  Real a_;
  Real b_;
  Real c_;
  Real lambda_z_;
  Real lambda_t_;
};

class ScalarFunction3D_AdvectionDiffusionFourierDecay :
    public Function3DVirtualInterface<ScalarFunction3D_AdvectionDiffusionFourierDecay, ScalarFunctionTraits>
{
public:
  typedef PhysD3 PhysDim;

  template <class T>
  using ArrayQ= T; // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> ax{"ax", 0.0, NO_RANGE, "x-direction advection coefficient"};
    const ParameterNumeric<Real> ay{"ay", 0.0, NO_RANGE, "y-direction advection coefficient"};
    const ParameterNumeric<Real> az{"az", 0.0, NO_RANGE, "z-direction advection coefficient"};
    const ParameterNumeric<Real> nu{"nu", 1.0, NO_RANGE, "diffusion coefficient"};
    const ParameterNumeric<Real> c0{"c0", 0.5, NO_RANGE, "steady state value at (xi, eta, zeta)= (0, 0, 0)"};
    const ParameterNumeric<Real> c1{"c1", 1.0, NO_RANGE, "steady state value at (xi, eta, zeta)= (1, 1, 1)"};
    const ParameterNumeric<Real> Lx{"Lx", 1.0, NO_RANGE, "domain length in x"};
    const ParameterNumeric<Real> Ly{"Ly", 1.0, NO_RANGE, "domain length in y"};
    const ParameterNumeric<Real> Lz{"Lz", 1.0, NO_RANGE, "domain length in z"};
    // const ParameterNumeric<std::vector<Real>> c_fourier{
    //   "c_fourier", NO_DEFAULT, NO_RANGE, "fourier coefficients for unsteady part"
    // };
    // const ParameterNumeric<std::vector<int>> n_fourier{
    //   "n_fourier", NO_DEFAULT, NO_RANGE, "wave numbers for x-direction unsteady part"
    // };
    // const ParameterNumeric<std::vector<int>> p_fourier{
    //   "p_fourier", NO_DEFAULT, NO_RANGE, "wave numbers for y-direction unsteady part"
    // };
    // const ParameterNumeric<std::vector<int>> q_fourier{
    //   "q_fourier", NO_DEFAULT, NO_RANGE, "wave numbers for z-direction unsteady part"
    // };

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction3D_AdvectionDiffusionFourierDecay(PyDict &d) :
          ax_(d.get(ParamsType::params.ax)),
          ay_(d.get(ParamsType::params.ay)),
          az_(d.get(ParamsType::params.az)),
          nu_(d.get(ParamsType::params.nu)),
          c0_(d.get(ParamsType::params.c0)),
          c1_(d.get(ParamsType::params.c1)),
          Lx_(d.get(ParamsType::params.Lx)),
          Ly_(d.get(ParamsType::params.Ly)),
          Lz_(d.get(ParamsType::params.Lz))
          // c_fourier_(d.get(ParamsType::params.c_fourier)),
          // n_fourier_(d.get(ParamsType::params.n_fourier)),
          // p_fourier_(d.get(ParamsType::params.p_fourier)),
          // q_fourier_(d.get(ParamsType::params.q_fourier))
  {
    c_fourier_= {1, 0.25, 0.0625};
    n_fourier_= {1, 2, 3};
    p_fourier_= {1, 2, 3};
    q_fourier_= {1, 2, 3};
    // if ((size_c != size_n) || (size_c != size_p) || (size_c != size_q))
    //   SANS_DEVELOPER_EXCEPTION("fourier coefficients and wavenumbers must match.");
  }

  explicit ScalarFunction3D_AdvectionDiffusionFourierDecay(const Real &ax,
                                                           const Real &ay,
                                                           const Real &az,
                                                           const Real &nu,
                                                           const Real &c0,
                                                           const Real &c1,
                                                           const Real &Lx,
                                                           const Real &Ly,
                                                           const Real &Lz,
                                                           const std::vector<Real> &c_fourier,
                                                           const std::vector<int> &n_fourier,
                                                           const std::vector<int> &p_fourier,
                                                           const std::vector<int> &q_fourier) :
          ax_(ax), ay_(ay), az_(az), nu_(nu), c0_(c0), c1_(c1),
          Lx_(Lx), Ly_(Ly), Lz_(Lz),
          c_fourier_(c_fourier), n_fourier_(n_fourier), p_fourier_(p_fourier),
          q_fourier_(q_fourier)
  {
    SANS_ASSERT(c_fourier_.size() == n_fourier_.size());
    SANS_ASSERT(c_fourier_.size() == p_fourier_.size());
    SANS_ASSERT(c_fourier_.size() == q_fourier_.size());
  }

  ~ScalarFunction3D_AdvectionDiffusionFourierDecay() {}

  template<class T> ArrayQ<T> operator() (const T &x, const T &y, const T &z, const T &time) const
  {
    // convert to heat equation equivalents...
    T xi= x - ax_*time;
    T eta= y - ay_*time;
    T zeta= z - az_*time;

    // compute steady state
    T out= steadyStateHeat<T>(xi, eta, zeta);

    // for each mode, add a fourier mode perturbation
    for (std::size_t i= 0; i < c_fourier_.size(); i++)
    {
      out += unsteadyPerturbationModeHeat<T>(xi, eta, zeta, time, c_fourier_[i],
          n_fourier_[i], p_fourier_[i], q_fourier_[i]);
    }

    return out;
  }

  void getFourier(std::vector<Real> &c_fourier,
                  std::vector<int> &n_fourier,
                  std::vector<int> &p_fourier,
                  std::vector<int> &q_fourier)
  {
    c_fourier= c_fourier_;
    n_fourier= n_fourier_;
    p_fourier= p_fourier_;
    q_fourier= q_fourier_;
  }

  void dump(int indentSize= 0, std::ostream &out= std::cout) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction3D_AdvectionDiffusionFourierDecay:" << std::endl;
    out << indent << "\tax_= " << ax_ << std::endl;
    out << indent << "\tay_= " << ay_ << std::endl;
    out << indent << "\taz_= " << az_ << std::endl;
    out << indent << "\tnu_= " << nu_ << std::endl;
    out << indent << "\tc0_= " << c0_ << std::endl;
    out << indent << "\tc1_= " << c1_ << std::endl;
    out << indent << "\tLx_= " << Lx_ << std::endl;
    out << indent << "\tLy_= " << Ly_ << std::endl;
    out << indent << "\tLz_= " << Lz_ << std::endl;
    // out << indent << "\tn_fourier_= " << n_fourier_ << std::endl;
    // out << indent << "\tp_fourier_= " << p_fourier_ << std::endl;
    // out << indent << "\tq_fourier_= " << q_fourier_ << std::endl;
  }

private:
  const Real ax_;
  const Real ay_;
  const Real az_;
  const Real nu_;
  const Real c0_;
  const Real c1_;
  const Real Lx_;
  const Real Ly_;
  const Real Lz_;
  std::vector<Real> c_fourier_;
  std::vector<int> n_fourier_;
  std::vector<int> p_fourier_;
  std::vector<int> q_fourier_;

  template <class T> ArrayQ<T> steadyStateHeat(const T &xi,
                                               const T &eta,
                                               const T &zeta) const
  {
    T c0_1d= pow(c0_, 1./3.);
    T c1_1d= pow(c1_, 1./3.);
    T f0= (c1_1d - c0_1d)/Lx_*xi + c0_1d;
    T f1= (c1_1d - c0_1d)/Ly_*eta + c0_1d;
    T f2= (c1_1d - c0_1d)/Lz_*zeta + c0_1d;
    return f0*f1*f2;
  }

  template <class T> ArrayQ<T> temporalDecay(const T &time,
                                             const int n_i,
                                             const int p_i,
                                             const int q_i) const
  {
    return exp(-nu_*PI*PI*(1.*n_i*n_i/(Lx_*Lx_) + 1.*p_i*p_i/(Ly_*Ly_)
        + 1.*q_i*q_i/(Lz_*Lz_))*time);
  }

  template <class T> ArrayQ<T> unsteadyPerturbationModeHeat(const T &xi,
                                                            const T &eta,
                                                            const T &zeta,
                                                            const T &time,
                                                            const T b_i,
                                                            const int n_i,
                                                            const int p_i,
                                                            const int q_i) const
  {
    return b_i*temporalDecay<T>(time, n_i, p_i, q_i)*sin(PI*n_i*xi/Lx_)
        *sin(PI*p_i*eta/Ly_)*sin(PI*q_i*zeta/Lz_);
  }
};

//----------------------------------------------------------------------------//
// solution: pde forcing function

template<class PDE>
class ScalarFunction3D_ForcingFunction
{
public:
  typedef PhysD3 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  explicit ScalarFunction3D_ForcingFunction( const PDE &pde ) : pde_(pde) {}

  ArrayQ<Real> operator()( const Real& x, const Real& y, const Real& z, const Real& time ) const
  {
    ArrayQ<Real> q = 0;
    pde_.forcingFunction(x, y, z, time, q);
    return q;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_ForcingFunction:" << std::endl;
  }
protected:
  const PDE& pde_;
};

//----------------------------------------------------------------------------//
// Struct for creating a scalar functions
//
struct ScalarFunction3DParams: noncopyable
{
  typedef std::shared_ptr<Function3DBase<Real>> Function_ptr;

  struct FunctionOptions
  {
    typedef DictKeyPair ExtractType;
    const ParameterString Name { "Name", NO_DEFAULT,"Function name"};
    const ParameterString& key = Name;

    const DictOption Exp{ "Exp", ScalarFunction3D_ASExp::ParamsType::checkInputs};
    const DictOption LaplaceSphere{ "LaplaceSphere", ScalarFunction3D_LaplaceSphere::ParamsType::checkInputs};
    const DictOption BoundaryLayer{ "BoundaryLayer", ScalarFunction3D_BoundaryLayer::ParamsType::checkInputs};
    const DictOption BoundaryLayerSingularPerturbation
    {
      "BoundaryLayerSingularPerturbation",
      ScalarFunction3D_BoundaryLayerSingularPerturbation::ParamsType::checkInputs
    };
    const DictOption TripleBL{ "TripleBL", ScalarFunction3D_TripleBL::ParamsType::checkInputs};
    const DictOption TripleBLsine{ "TripleBLsine", ScalarFunction3D_TripleBLsine::ParamsType::checkInputs};
    const DictOption CornerSingularity{ "CornerSingularity", ScalarFunction3D_CornerSingularity::ParamsType::checkInputs};
    const DictOption Gaussian{ "NonLinPoisson", ScalarFunction3D_Gaussian::ParamsType::checkInputs};
    const DictOption SineSineSine{ "SineSineSine", ScalarFunction3D_SineSineSine::ParamsType::checkInputs};
    const DictOption SineSineSineSineUnsteady{ "SineSineSineSineUnsteady", ScalarFunction3D_SineSineSineSineUnsteady::ParamsType::checkInputs};
    const DictOption SineSineSineDecay{ "SineSineSineDecay", ScalarFunction3D_SineSineSineDecay::ParamsType::checkInputs};
    const DictOption AdvectionDiffusionDecayingCosine
    {
      "AdvectionDiffusionDecayingCosine", ScalarFunction3D_AdvectionDiffusionDecayingCosine::ParamsType::checkInputs
    };
    const DictOption Monomial{ "Monomial", ScalarFunction3D_Monomial::ParamsType::checkInputs};
    const DictOption Tanh3{ "Tanh3", ScalarFunction3D_Tanh3::ParamsType::checkInputs};
    const DictOption SinATan3{ "SinATan3", ScalarFunction3D_SinATan3::ParamsType::checkInputs};
    const DictOption SinFun3{ "SinFun3", ScalarFunction3D_SinFun3::ParamsType::checkInputs};
    const DictOption SphericalWaveDecay{ "SphericalWave", ScalarFunction3D_SphericalWaveDecay::ParamsType::checkInputs};
    const DictOption OscillatingGaussian{ "OscillatingGaussian", ScalarFunction3D_OscillatingGaussian::ParamsType::checkInputs};
    const DictOption AdvectionDiffusionSquareChannel
    {
      "AdvectionDiffusionSquareChannel", ScalarFunction3D_AdvectionDiffusionSquareChannel::ParamsType::checkInputs
    };
    const DictOption AdvectionDiffusionFourierDecay
    {
      "AdvectionDiffusionFourierDecay", ScalarFunction3D_AdvectionDiffusionFourierDecay::ParamsType::checkInputs
    };

    const std::vector<DictOption> options{LaplaceSphere, Exp, BoundaryLayer, BoundaryLayerSingularPerturbation,TripleBL,
                                          TripleBLsine, Gaussian, SineSineSine, SineSineSineSineUnsteady, SineSineSineDecay,
                                          AdvectionDiffusionDecayingCosine,
                                          Monomial, Tanh3, SinATan3, SinFun3, SphericalWaveDecay, OscillatingGaussian,
                                          AdvectionDiffusionSquareChannel,
                                          AdvectionDiffusionFourierDecay
                                         };
  };
  const ParameterOption<FunctionOptions> Function{ "Function", NO_DEFAULT, "The 3D scalar function"};

  static Function_ptr getFunction(const PyDict& d);
};

}

#endif  // SCALARFUNCTION3D_H
