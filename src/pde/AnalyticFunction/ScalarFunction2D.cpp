// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "ScalarFunction2D.h"

#define PYDICT_INSTANTIATE
#include "Python/PyDict_impl.h"

namespace SANS
{

template ParameterOption<ScalarFunction2DParams::FunctionOptions>::ExtractType
PyDict::get(const ParameterType<ParameterOption<ScalarFunction2DParams::FunctionOptions>>&) const;

// cppcheck-suppress passedByValue
void ScalarFunction2D_Const::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.a0));
  d.checkUnknownInputs(allParams);
}
ScalarFunction2D_Const::ParamsType ScalarFunction2D_Const::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction2D_Linear::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.a0));
  allParams.push_back(d.checkInputs(params.ax));
  allParams.push_back(d.checkInputs(params.ay));
  d.checkUnknownInputs(allParams);
}
ScalarFunction2D_Linear::ParamsType ScalarFunction2D_Linear::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction2D_Quadratic::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.a0));
  allParams.push_back(d.checkInputs(params.ax));
  allParams.push_back(d.checkInputs(params.ay));
  allParams.push_back(d.checkInputs(params.axx));
  allParams.push_back(d.checkInputs(params.ayy));
  allParams.push_back(d.checkInputs(params.axy));
  d.checkUnknownInputs(allParams);
}
ScalarFunction2D_Quadratic::ParamsType ScalarFunction2D_Quadratic::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction2D_QuadraticExponential::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.a0));
  allParams.push_back(d.checkInputs(params.ax));
  allParams.push_back(d.checkInputs(params.ay));
  allParams.push_back(d.checkInputs(params.axx));
  allParams.push_back(d.checkInputs(params.ayy));
  allParams.push_back(d.checkInputs(params.axy));
  d.checkUnknownInputs(allParams);
}
ScalarFunction2D_QuadraticExponential::ParamsType ScalarFunction2D_QuadraticExponential::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction2D_PiecewiseConstBlock::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.a00));
  allParams.push_back(d.checkInputs(params.a01));
  allParams.push_back(d.checkInputs(params.a10));
  allParams.push_back(d.checkInputs(params.a11));
  allParams.push_back(d.checkInputs(params.x0));
  allParams.push_back(d.checkInputs(params.y0));
  d.checkUnknownInputs(allParams);
}
ScalarFunction2D_PiecewiseConstBlock::ParamsType ScalarFunction2D_PiecewiseConstBlock::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction2D_PiecewiseConstDiag::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.a0));
  allParams.push_back(d.checkInputs(params.a1));
  allParams.push_back(d.checkInputs(params.m));
  allParams.push_back(d.checkInputs(params.c));
  d.checkUnknownInputs(allParams);
}
ScalarFunction2D_PiecewiseConstDiag::ParamsType ScalarFunction2D_PiecewiseConstDiag::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction2D_Monomial::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.i));
  allParams.push_back(d.checkInputs(params.j));
  d.checkUnknownInputs(allParams);
}
ScalarFunction2D_Monomial::ParamsType ScalarFunction2D_Monomial::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction2D_ASExp::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back( d.checkInputs( params.u0) );
  allParams.push_back( d.checkInputs( params.x0) );
  allParams.push_back( d.checkInputs( params.y0) );
  allParams.push_back( d.checkInputs( params.a ) );
  allParams.push_back( d.checkInputs( params.b ) );
  allParams.push_back( d.checkInputs( params.alpha ) );
  d.checkUnknownInputs(allParams);
}
ScalarFunction2D_ASExp::ParamsType ScalarFunction2D_ASExp::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction2D_ASExpCombo::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back( d.checkInputs( params.u0) );
  allParams.push_back( d.checkInputs( params.x0) );
  allParams.push_back( d.checkInputs( params.y0) );
  allParams.push_back( d.checkInputs( params.a ) );
  allParams.push_back( d.checkInputs( params.b ) );
  allParams.push_back( d.checkInputs( params.s ) );
  allParams.push_back( d.checkInputs( params.alpha ));
  allParams.push_back( d.checkInputs( params.theta0 ));
  d.checkUnknownInputs(allParams);
}
ScalarFunction2D_ASExpCombo::ParamsType ScalarFunction2D_ASExpCombo::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction2D_VarExp::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.a));
  allParams.push_back(d.checkInputs(params.b));
  allParams.push_back(d.checkInputs(params.c));
  allParams.push_back(d.checkInputs(params.d));
  allParams.push_back(d.checkInputs(params.e));
  d.checkUnknownInputs(allParams);
}
ScalarFunction2D_VarExp::ParamsType ScalarFunction2D_VarExp::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction2D_VarExp2::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.a));
  allParams.push_back(d.checkInputs(params.b));
  allParams.push_back(d.checkInputs(params.c));
  allParams.push_back(d.checkInputs(params.d));
  d.checkUnknownInputs(allParams);
}
ScalarFunction2D_VarExp2::ParamsType ScalarFunction2D_VarExp2::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction2D_VarExp3::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.a));
  d.checkUnknownInputs(allParams);
}
ScalarFunction2D_VarExp3::ParamsType ScalarFunction2D_VarExp3::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction2D_SineSine::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.a));
  allParams.push_back(d.checkInputs(params.b));
  allParams.push_back(d.checkInputs(params.C));
  allParams.push_back(d.checkInputs(params.A));
  d.checkUnknownInputs(allParams);
}
ScalarFunction2D_SineSine::ParamsType ScalarFunction2D_SineSine::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction2D_SineCosineDecay::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.lambda));
  d.checkUnknownInputs(allParams);
}
ScalarFunction2D_SineCosineDecay::ParamsType ScalarFunction2D_SineCosineDecay::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction2D_CircularWaveDecay::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.alpha));
  allParams.push_back(d.checkInputs(params.k0));
  allParams.push_back(d.checkInputs(params.k1));
  allParams.push_back(d.checkInputs(params.velocity));
  allParams.push_back(d.checkInputs(params.radius0));

  d.checkUnknownInputs(allParams);
}
ScalarFunction2D_CircularWaveDecay::ParamsType ScalarFunction2D_CircularWaveDecay::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction2D_PolarGauss::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.k0));
  allParams.push_back(d.checkInputs(params.k1));
  allParams.push_back(d.checkInputs(params.r0));

  d.checkUnknownInputs(allParams);
}
ScalarFunction2D_PolarGauss::ParamsType ScalarFunction2D_PolarGauss::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction2D_DoubleBL::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.a));
  allParams.push_back(d.checkInputs(params.b));
  allParams.push_back(d.checkInputs(params.nu));
  allParams.push_back(d.checkInputs(params.scale));
  allParams.push_back(d.checkInputs(params.offset));
  d.checkUnknownInputs(allParams);
}
ScalarFunction2D_DoubleBL::ParamsType ScalarFunction2D_DoubleBL::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction2D_Vortex::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.x0));
  allParams.push_back(d.checkInputs(params.y0));
  d.checkUnknownInputs(allParams);
}
ScalarFunction2D_Vortex::ParamsType ScalarFunction2D_Vortex::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction2D_SineSineRotated::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.theta));
  d.checkUnknownInputs(allParams);
}
ScalarFunction2D_SineSineRotated::ParamsType ScalarFunction2D_SineSineRotated::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction2D_SineTheta::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.c));
  d.checkUnknownInputs(allParams);
}
ScalarFunction2D_SineTheta::ParamsType ScalarFunction2D_SineTheta::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction2D_CornerSingularity::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.alpha));
  allParams.push_back(d.checkInputs(params.theta0));
  d.checkUnknownInputs(allParams);
}
ScalarFunction2D_CornerSingularity::ParamsType ScalarFunction2D_CornerSingularity::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction2D_BoundaryLayer::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.epsilon));
  allParams.push_back(d.checkInputs(params.beta));
  allParams.push_back(d.checkInputs(params.p));
  d.checkUnknownInputs(allParams);
}
ScalarFunction2D_BoundaryLayer::ParamsType ScalarFunction2D_BoundaryLayer::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction2D_Gaussian::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.a));
  allParams.push_back(d.checkInputs(params.x0));
  allParams.push_back(d.checkInputs(params.y0));
  allParams.push_back(d.checkInputs(params.sx));
  allParams.push_back(d.checkInputs(params.sy));
  d.checkUnknownInputs(allParams);
}
ScalarFunction2D_Gaussian::ParamsType ScalarFunction2D_Gaussian::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction2D_GaussianCosine::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.a));
  allParams.push_back(d.checkInputs(params.y0));
  allParams.push_back(d.checkInputs(params.y1));
  allParams.push_back(d.checkInputs(params.sy));
  d.checkUnknownInputs(allParams);
}
ScalarFunction2D_GaussianCosine::ParamsType ScalarFunction2D_GaussianCosine::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction2D_ThetaGeometricSeries::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.c));
  allParams.push_back(d.checkInputs(params.p));
  d.checkUnknownInputs(allParams);
}
ScalarFunction2D_ThetaGeometricSeries::ParamsType ScalarFunction2D_ThetaGeometricSeries::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction2D_Tanh::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.nu));
  allParams.push_back(d.checkInputs(params.A));
  allParams.push_back(d.checkInputs(params.B));
  d.checkUnknownInputs(allParams);
}
ScalarFunction2D_Tanh::ParamsType ScalarFunction2D_Tanh::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction2D_Pulse::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.h));
  allParams.push_back(d.checkInputs(params.x0));
  allParams.push_back(d.checkInputs(params.w));
  d.checkUnknownInputs(allParams);
}
ScalarFunction2D_Pulse::ParamsType ScalarFunction2D_Pulse::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction2D_Ojeda::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.a));
  allParams.push_back(d.checkInputs(params.b));
  d.checkUnknownInputs(allParams);
}
ScalarFunction2D_Ojeda::ParamsType ScalarFunction2D_Ojeda::ParamsType::params;


//// cppcheck-suppress passedByValue
//void ScalarFunction2D_AdvectionReaction::ParamsType::checkInputs(PyDict d)
//{
//  std::vector<const ParameterBase*> allParams;
//  allParams.push_back(d.checkInputs(params.u));
//  allParams.push_back(d.checkInputs(params.v));
//  allParams.push_back(d.checkInputs(params.s));
//  d.checkUnknownInputs(allParams);
//}
//ScalarFunction2D_AdvectionReaction::ParamsType ScalarFunction2D_AdvectionReaction::ParamsType::params;

//===========================================================================//
ScalarFunction2DParams::Function_ptr
ScalarFunction2DParams::getFunction(const PyDict& d)
{
  ScalarFunction2DParams params;

  DictKeyPair FunctionParam = d.get(params.Function);

  if ( FunctionParam == params.Function.Const )
    return Function_ptr( new ScalarFunction2D_Const( FunctionParam ) );
  else if ( FunctionParam == params.Function.Linear )
    return Function_ptr( new ScalarFunction2D_Linear( FunctionParam ) );
  else if ( FunctionParam == params.Function.PiecewiseConstBlock )
    return Function_ptr( new ScalarFunction2D_PiecewiseConstBlock( FunctionParam ) );
  else if ( FunctionParam == params.Function.PiecewiseConstDiag )
    return Function_ptr( new ScalarFunction2D_PiecewiseConstDiag( FunctionParam ) );
  else if ( FunctionParam == params.Function.Monomial )
    return Function_ptr( new ScalarFunction2D_Monomial( FunctionParam ) );
  else if ( FunctionParam == params.Function.Quadratic )
    return Function_ptr( new ScalarFunction2D_Quadratic( FunctionParam ) );
  else if ( FunctionParam == params.Function.Pringle )
    return Function_ptr( new ScalarFunction2D_Pringle( FunctionParam ) );
  else if ( FunctionParam == params.Function.ASExp )
    return Function_ptr( new ScalarFunction2D_ASExp( FunctionParam ) );
  else if ( FunctionParam == params.Function.ASExpCombo )
    return Function_ptr( new ScalarFunction2D_ASExpCombo( FunctionParam ) );
  else if ( FunctionParam == params.Function.VarExp )
    return Function_ptr( new ScalarFunction2D_VarExp( FunctionParam ) );
  else if ( FunctionParam == params.Function.VarExp2 )
    return Function_ptr( new ScalarFunction2D_VarExp2( FunctionParam ) );
  else if ( FunctionParam == params.Function.VarExp3 )
    return Function_ptr( new ScalarFunction2D_VarExp3( FunctionParam ) );
  else if ( FunctionParam == params.Function.SineSine )
    return Function_ptr( new ScalarFunction2D_SineSine( FunctionParam ) );
  else if (FunctionParam == params.Function.SineCosineDecay)
    return Function_ptr(new ScalarFunction2D_SineCosineDecay(FunctionParam));
  else if (FunctionParam == params.Function.CircularWaveDecay)
    return Function_ptr(new ScalarFunction2D_CircularWaveDecay(FunctionParam));
  else if ( FunctionParam == params.Function.DoubleBL )
    return Function_ptr( new ScalarFunction2D_DoubleBL( FunctionParam ) );
  else if ( FunctionParam == params.Function.Vortex )
    return Function_ptr( new ScalarFunction2D_Vortex( FunctionParam ) );
  else if ( FunctionParam == params.Function.SineTheta )
    return Function_ptr( new ScalarFunction2D_SineTheta( FunctionParam ) );
  else if ( FunctionParam == params.Function.CornerSingularity )
    return Function_ptr( new ScalarFunction2D_CornerSingularity( FunctionParam ) );
  else if ( FunctionParam == params.Function.BoundaryLayer )
    return Function_ptr( new ScalarFunction2D_BoundaryLayer( FunctionParam ) );
  else if ( FunctionParam == params.Function.ThetaGeometricSeries )
    return Function_ptr( new ScalarFunction2D_ThetaGeometricSeries( FunctionParam ) );
  else if ( FunctionParam == params.Function.Tanh )
    return Function_ptr( new ScalarFunction2D_Tanh( FunctionParam ) );
  else if ( FunctionParam == params.Function.PiecewiseLinear )
    return Function_ptr( new ScalarFunction2D_PiecewiseLinear( FunctionParam ) );
  else if ( FunctionParam == params.Function.Pulse )
    return Function_ptr( new ScalarFunction2D_Pulse( FunctionParam ) );
  else if ( FunctionParam == params.Function.Gaussian )
    return Function_ptr( new ScalarFunction2D_Gaussian( FunctionParam ) );
  else if ( FunctionParam == params.Function.GaussianCosine )
    return Function_ptr( new ScalarFunction2D_GaussianCosine( FunctionParam ) );
  else if ( FunctionParam == params.Function.Ojeda )
    return Function_ptr( new ScalarFunction2D_Ojeda( FunctionParam ) );
  else if ( FunctionParam == params.Function.Tanh2 )
    return Function_ptr( new ScalarFunction2D_Tanh2( FunctionParam ) );
  //  else if ( FunctionParam == params.Function.AdvectionReaction )
  //    return Function_ptr( new ScalarFunction2D_AdvectionReaction( FunctionParam ) );

  // Should never get here if checkInputs was called
  SANS_DEVELOPER_EXCEPTION("Unrecognized function specified");
}

}
