// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SCALARFUNCTION2D_H
#define SCALARFUNCTION2D_H

// 2-D Advection-Diffusion PDE: exact and MMS solutions

// Python must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <cmath>
#include <memory> //shared_ptr

#include "tools/SANSnumerics.h"     // Real

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "Topology/Dimension.h"

#include "Function2D.h"
#include "ScalarFunctionTraits.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// solution: Linear Combination
// template for combining two solution functions to make another
template <class SOLN1, class SOLN2>
class ScalarFunction2D_Combination
    : public Function2DVirtualInterface<ScalarFunction2D_Combination<SOLN1,SOLN2>,ScalarFunctionTraits>
{
public:
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> a{"a", 1, NO_RANGE, "Coefficient for Function 1"};
    const ParameterNumeric<Real> b{"b", 1, NO_RANGE, "Coefficient for Function 2"};
    const ParameterDict SolutionArgs1{"SolutionArgs1", EMPTY_DICT, SOLN1::ParamsType::checkInputs, "Solution function arguments 1"};
    const ParameterDict SolutionArgs2{"SolutionArgs2", EMPTY_DICT, SOLN2::ParamsType::checkInputs, "Solution function arguments 2"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction2D_Combination( const PyDict& d ) :
              a_(d.get(ParamsType::params.a)),
              f1_(d.get(ParamsType::params.SolutionArgs1)),
              b_(d.get(ParamsType::params.b)),
              f2_(d.get(ParamsType::params.SolutionArgs1)) {}
  explicit ScalarFunction2D_Combination( const Real& a, const SOLN1& f1, const Real& b, const SOLN2& f2 ) :
              a_(a), f1_(f1), b_(b), f2_(f2) {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    return a_*f1_(x,y,time) + b_*f2_(x,y,time);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_Combination:  a_, b_ = " << a_ << ", " << b_ << std::endl;
  }

private:
  Real a_;
  SOLN1 f1_;
  Real b_;
  SOLN2 f2_;
};

//----------------------------------------------------------------------------//
// solution: const
class ScalarFunction2D_Const : public Function2DVirtualInterface<ScalarFunction2D_Const,ScalarFunctionTraits>
{
public:
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> a0{"a0", NO_DEFAULT, NO_RANGE, "Constant of the function"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction2D_Const( const PyDict& d ) : a0_(d.get(ParamsType::params.a0)) {}
  explicit ScalarFunction2D_Const( const Real& a0 ) : a0_(a0) {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    return a0_;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_Const: a0_ = " << a0_ << std::endl;
  }
private:
  Real a0_;
};


//----------------------------------------------------------------------------//
// solution: linear
class ScalarFunction2D_Linear : public Function2DVirtualInterface<ScalarFunction2D_Linear,ScalarFunctionTraits>
{
public:
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> a0{"a0", NO_DEFAULT, NO_RANGE, "Constant of the linear function"};
    const ParameterNumeric<Real> ax{"ax", NO_DEFAULT, NO_RANGE, "x-Linear of the linear function"};
    const ParameterNumeric<Real> ay{"ay", NO_DEFAULT, NO_RANGE, "y-Linear of the linear function"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction2D_Linear( const PyDict& d ) :
            a0_(d.get(ParamsType::params.a0)), ax_(d.get(ParamsType::params.ax)), ay_(d.get(ParamsType::params.ay)) {}
  ScalarFunction2D_Linear( const Real& a0, const Real& ax, const Real& ay ) : a0_(a0), ax_(ax), ay_(ay) {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    return a0_ + ax_*x + ay_*y;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_Linear: a0_, ax_, ay_ = " << a0_ << ", " << ax_ << ", " << ay_ << std::endl;
  }
private:
  const Real a0_, ax_, ay_;
};

//----------------------------------------------------------------------------//
// solution: quadratic
class ScalarFunction2D_Quadratic : public Function2DVirtualInterface<ScalarFunction2D_Quadratic,ScalarFunctionTraits>
{
public:
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> a0{"a0", 0, NO_RANGE, "Constant of the quadratic function"};
    const ParameterNumeric<Real> ax{"ax", 0, NO_RANGE, "x-mode of the quadratic function"};
    const ParameterNumeric<Real> ay{"ay", 0, NO_RANGE, "y-mode of the quadratic function"};
    const ParameterNumeric<Real> axx{"axx", 1, NO_RANGE, "x2-mode of the quadratic function"};
    const ParameterNumeric<Real> ayy{"ayy", 1, NO_RANGE, "y2-mode of the quadratic function"};
    const ParameterNumeric<Real> axy{"axy", 0, NO_RANGE, "xy-mode of the quadratic function"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction2D_Quadratic( const PyDict& d ) :
            a0_(d.get(ParamsType::params.a0)),
            ax_(d.get(ParamsType::params.ax)),
            ay_(d.get(ParamsType::params.ay)),
            axx_(d.get(ParamsType::params.axx)),
            ayy_(d.get(ParamsType::params.ayy)),
            axy_(d.get(ParamsType::params.axy)) {}
  ScalarFunction2D_Quadratic( const Real& a0, const Real& ax, const Real& ay,  const Real& axx, const Real& ayy, const Real& axy ) :
    a0_(a0), ax_(ax), ay_(ay), axx_(axx), ayy_(ayy), axy_(axy) {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    return a0_ + axx_*x*x + ayy_*y*y + axy_*x*y + ax_*x + ay_*y;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_Linear: a0_, ax_, ay_, axx_, ayy_, axy_ = "
        << a0_ << ", " << ax_ << ", " << ay_ << ", " << axx_ << ", " << ayy_ << ", " << axy_ << std::endl;
  }
private:
  const Real a0_, ax_, ay_, axx_, ayy_, axy_;
};


//----------------------------------------------------------------------------//
// solution: quadratic
class ScalarFunction2D_QuadraticExponential : public Function2DVirtualInterface<ScalarFunction2D_QuadraticExponential,ScalarFunctionTraits>
{
public:
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> a0{"a0", 0, NO_RANGE, "Constant of the quadratic function"};
    const ParameterNumeric<Real> ax{"ax", 0, NO_RANGE, "x-mode of the quadratic function"};
    const ParameterNumeric<Real> ay{"ay", 0, NO_RANGE, "y-mode of the quadratic function"};
    const ParameterNumeric<Real> axx{"axx", 1, NO_RANGE, "x2-mode of the quadratic function"};
    const ParameterNumeric<Real> ayy{"ayy", 1, NO_RANGE, "y2-mode of the quadratic function"};
    const ParameterNumeric<Real> axy{"axy", 0, NO_RANGE, "xy-mode of the quadratic function"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction2D_QuadraticExponential( const PyDict& d ) :
            a0_(d.get(ParamsType::params.a0)),
            ax_(d.get(ParamsType::params.ax)),
            ay_(d.get(ParamsType::params.ay)),
            axx_(d.get(ParamsType::params.axx)),
            ayy_(d.get(ParamsType::params.ayy)),
            axy_(d.get(ParamsType::params.axy)) {}
  ScalarFunction2D_QuadraticExponential( const Real& a0, const Real& ax, const Real& ay,  const Real& axx, const Real& ayy, const Real& axy ) :
    a0_(a0), ax_(ax), ay_(ay), axx_(axx), ayy_(ayy), axy_(axy) {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    return exp(a0_ + axx_*x*x + ayy_*y*y + axy_*x*y + ax_*x + ay_*y)-1;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_QuadraticExponential: a0_, ax_, ay_, axx_, ayy_, axy_ = "
        << a0_ << ", " << ax_ << ", " << ay_ << ", " << axx_ << ", " << ayy_ << ", " << axy_ << std::endl;
  }
private:
  const Real a0_, ax_, ay_, axx_, ayy_, axy_;
};

//----------------------------------------------------------------------------//
// solution: piecewise constant
class ScalarFunction2D_PiecewiseConstBlock : public Function2DVirtualInterface<ScalarFunction2D_PiecewiseConstBlock,ScalarFunctionTraits>
{
public:
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> a00{"a00", NO_DEFAULT, NO_RANGE, "Top-left constant"};
    const ParameterNumeric<Real> a01{"a01", NO_DEFAULT, NO_RANGE, "Top-right constant"};
    const ParameterNumeric<Real> a10{"a10", NO_DEFAULT, NO_RANGE, "Bottom-left constant"};
    const ParameterNumeric<Real> a11{"a11", NO_DEFAULT, NO_RANGE, "Bottom-right constant"};
    const ParameterNumeric<Real> x0{"x0", NO_DEFAULT, NO_RANGE, "x-coordinate of 'origin'"};
    const ParameterNumeric<Real> y0{"y0", NO_DEFAULT, NO_RANGE, "y-coordinate of 'origin'"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction2D_PiecewiseConstBlock( const PyDict& d ) :
              a00_(d.get(ParamsType::params.a00)),
              a01_(d.get(ParamsType::params.a01)),
              a10_(d.get(ParamsType::params.a10)),
              a11_(d.get(ParamsType::params.a11)),
              x0_(d.get(ParamsType::params.x0)),
              y0_(d.get(ParamsType::params.y0)) {}

  explicit ScalarFunction2D_PiecewiseConstBlock( const Real& a00, const Real& a01, const Real& a10, const Real& a11,
                                                 const Real& x0, const Real& y0 ) :
                                                        a00_(a00), a01_(a01), a10_(a10), a11_(a11),
                                                        x0_(x0), y0_(y0) {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    if (x < x0_)
    {
      if (y < y0_)
        return a10_;
      else
        return a00_;
    }
    else
    {
      if (y < y0_)
        return a11_;
      else
        return a01_;
    }
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_PiecewiseConstBlock: " << std::endl;
    out << indent << "  a00_, a01_ = " << a00_ << ", " << a01_ << std::endl;
    out << indent << "  a10_, a11_ = " << a10_ << ", " << a11_ << std::endl;
    out << indent << "  x0_, y0_   = " << x0_  << ", " << y0_  << std::endl;
  }
private:
  Real a00_, a01_, a10_, a11_;
  Real x0_, y0_;
};

//----------------------------------------------------------------------------//
// solution: piecewise constant - diagonal
class ScalarFunction2D_PiecewiseConstDiag : public Function2DVirtualInterface<ScalarFunction2D_PiecewiseConstDiag,ScalarFunctionTraits>
{
public:
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> a0{"a0", NO_DEFAULT, NO_RANGE, "Constant on left of diagonal"};
    const ParameterNumeric<Real> a1{"a1", NO_DEFAULT, NO_RANGE, "Constant on right of diagonal"};
    const ParameterNumeric<Real> m{"m", 1.0, NO_RANGE, "Gradient of diagonal"};
    const ParameterNumeric<Real> c{"c", 0.0, NO_RANGE, "y-intercept of diagonal"};

    static void checkInputs(PyDict d);
    static ParamsType params;
  };

  explicit ScalarFunction2D_PiecewiseConstDiag( const PyDict& d ) :
              a0_(d.get(ParamsType::params.a0)),
              a1_(d.get(ParamsType::params.a1)),
              m_(d.get(ParamsType::params.m)),
              c_(d.get(ParamsType::params.c)) {}

  explicit ScalarFunction2D_PiecewiseConstDiag( const Real& a0, const Real& a1, const Real m = 1, const Real c = 0) :
                                                        a0_(a0), a1_(a1), m_(m), c_(c) {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    if (y >= m_*x + c_)
      return a0_;
    else
      return a1_;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_PiecewiseConstDiag: " << std::endl;
    out << indent << "  a0_, a1_ = " << a0_ << ", " << a1_ << std::endl;
    out << indent << "  m_, c_   = " << m_  << ", " << c_  << std::endl;
  }
private:
  Real a0_, a1_;
  Real m_, c_;
};


//----------------------------------------------------------------------------//
// solution: monomial
class ScalarFunction2D_Monomial : public Function2DVirtualInterface<ScalarFunction2D_Monomial,ScalarFunctionTraits>
{
public:
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> i{"i", NO_DEFAULT, NO_RANGE, "x-monomial exponent"};
    const ParameterNumeric<Real> j{"j", NO_DEFAULT, NO_RANGE, "y-monomial exponent"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction2D_Monomial( const PyDict& d ) :
              i_(d.get(ParamsType::params.i)), j_(d.get(ParamsType::params.j)) {}
  ScalarFunction2D_Monomial( Real i, Real j ) : i_(i), j_(j) {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    return pow(x, i_)*pow(y, j_);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_Monomial: " << std::endl;
    out << indent << "  i_, j_ = " << i_ << ", " << j_ << std::endl;
  }
private:
  Real i_, j_;
};

//----------------------------------------------------------------------------//
// solution: 27*x*y*(1-x-y) - cubic bubble function
class ScalarFunction2D_Cubic : public Function2DVirtualInterface<ScalarFunction2D_Cubic,ScalarFunctionTraits>
{
public:
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  typedef ParamsNone ParamsType;

  explicit ScalarFunction2D_Cubic( const PyDict& d ) {}
  ScalarFunction2D_Cubic() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    return 27*x*y*(1-x-y);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_Cubic: " << std::endl;
  }
};


//----------------------------------------------------------------------------//
// solution: sin(pi x)*(1-cos(pi*y/2)) - pringle function
class ScalarFunction2D_Pringle : public Function2DVirtualInterface<ScalarFunction2D_Pringle,ScalarFunctionTraits>
{
public:
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  typedef ParamsNone ParamsType;

  explicit ScalarFunction2D_Pringle( const PyDict& d ) {}
  ScalarFunction2D_Pringle() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    return sin(PI*x)*(1-cos(PI*y/2));
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_Pringle: " << std::endl;
  }
};


//----------------------------------------------------------------------------//
// solution: Advection-Reaction Exponential
//
// u =  u0 * exp( alpha*(a*(x0 - x) + b*(y0 - y))/(a*a + b*b) )
//
// Exact solution to advection reaction: a ux + b uy + alpha*u = 0, u(x0,y0) = u0

class ScalarFunction2D_ASExp : public Function2DVirtualInterface<ScalarFunction2D_ASExp, ScalarFunctionTraits>
{
public:
  typedef PhysD2 PhysDim;

  template<class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType: noncopyable
  {
    const ParameterNumeric<Real> u0{ "u0", 1, NO_RANGE, "u on boundary"};
    const ParameterNumeric<Real> x0{ "x0", 0, NO_RANGE, "x location of boundary"};
    const ParameterNumeric<Real> y0{ "y0", 0, NO_RANGE, "y location of boundary"};
    const ParameterNumeric<Real> a { "a", NO_DEFAULT, NO_RANGE, "a x-velocity"};
    const ParameterNumeric<Real> b { "b", NO_DEFAULT, NO_RANGE, "b y-velocity"};
    const ParameterNumeric<Real> alpha{ "alpha", NO_DEFAULT, NO_RANGE, "Source strength"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction2D_ASExp( const Real u0, const Real x0, const Real y0,
                                   const Real a, const Real b, const Real alpha)
  : u0_(u0), x0_(x0), y0_(y0), a_(a), b_(b), alpha_(alpha) {}

  explicit ScalarFunction2D_ASExp( const PyDict& d ) :
            u0_(d.get(ParamsType::params.u0)),
            x0_(d.get(ParamsType::params.x0)),
            y0_(d.get(ParamsType::params.y0)),
            a_(d.get(ParamsType::params.a)),
            b_(d.get(ParamsType::params.b)),
            alpha_(d.get(ParamsType::params.alpha))
  {}

  template<class T>
  ArrayQ<T> operator()(const T& x, const T& y, const T& time) const
  {
    ArrayQ<T> x_sol = exp( alpha_*(x0_ - x)/a_);
    ArrayQ<T> y_sol = exp( alpha_*(y0_ - y)/b_);

    // This was Marshall's old version. Doesn't actually give sharp characteristics
    //    return u0_ * exp( alpha_*(a_*(x0_ - x) + b_*(y0_ - y))/(a_*a_ + b_*b_) );
    return u0_ * max( x_sol, y_sol);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_ASExp: " << std::endl;
    out << indent << "  u0_, x0_, y0_, a_, b_, alpha_ = "
        << u0_ << ", "
        << x0_ << ", "
        << y0_ << ", "
        << a_ << ", "
        << b_ << ", "
        << alpha_ << std::endl;
  }

private:
  Real u0_, x0_, y0_, a_, b_, alpha_;
};


//----------------------------------------------------------------------------//
// solution: Advection-Reaction Exponential
//
// u =  u0 * exp( alpha*(a*(x0 - x) + b*(y0 - y))/(a*a + b*b) )
//
// Exact solution to advection reaction: a ux + b uy + alpha*u = 0, u(x0,y0) = u0

class ScalarFunction2D_ASExpCombo : public Function2DVirtualInterface<ScalarFunction2D_ASExpCombo, ScalarFunctionTraits>
{
public:
  typedef PhysD2 PhysDim;

  template<class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType: noncopyable
  {
    const ParameterNumeric<Real> u0{ "u0", 1, NO_RANGE, "u on boundary"};
    const ParameterNumeric<Real> x0{ "x0", 0, NO_RANGE, "x location of boundary"};
    const ParameterNumeric<Real> y0{ "y0", 0, NO_RANGE, "y location of boundary"};
    const ParameterNumeric<Real> a { "a", NO_DEFAULT, NO_RANGE, "a x-velocity"};
    const ParameterNumeric<Real> b { "b", NO_DEFAULT, NO_RANGE, "b y-velocity"};
    const ParameterNumeric<Real> s{ "s", NO_DEFAULT, NO_RANGE, "Source strength"};

    const ParameterNumeric<Real> alpha{"alpha", NO_DEFAULT, 0, NO_LIMIT, "strength of singularity"};
    const ParameterNumeric<Real> theta0{"theta0", NO_DEFAULT, NO_RANGE, "offset angle"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction2D_ASExpCombo( const Real u0, const Real x0, const Real y0,
                                   const Real a, const Real b, const Real s, const Real alpha, const Real theta0 )
  : u0_(u0), x0_(x0), y0_(y0), a_(a), b_(b), s_(s), alpha_(alpha), theta0_(theta0) {}

  explicit ScalarFunction2D_ASExpCombo( const PyDict& d ) :
            u0_(d.get(ParamsType::params.u0)),
            x0_(d.get(ParamsType::params.x0)),
            y0_(d.get(ParamsType::params.y0)),
            a_(d.get(ParamsType::params.a)),
            b_(d.get(ParamsType::params.b)),
            s_(d.get(ParamsType::params.s)),
            alpha_(d.get(ParamsType::params.alpha)),
            theta0_(d.get(ParamsType::params.theta0))
  {}

  template<class T>
  ArrayQ<T> operator()(const T& x, const T& y, const T& time) const
  {
    T x_sol = exp( s_*(x0_ - x)/a_);
    T y_sol = exp( s_*(y0_ - y)/b_);
    T asexp = u0_ * max( x_sol, y_sol);

    T r = sqrt( x*x + y*y);
    T theta = atan2(y,x);
    T corner = pow(r,alpha_)*sin(alpha_*(theta + theta0_));

    return corner + asexp;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_ASExpCombo: " << std::endl;
    out << indent << "  u0_, x0_, y0_, a_, b_, s_, alpha_, theta0_  = "
        << u0_ << ", "
        << x0_ << ", "
        << y0_ << ", "
        << a_ << ", "
        << b_ << ", "
        << s_ << ", "
        << alpha_ << ", "
        << theta0_ << std::endl;
  }

private:
  Real u0_, x0_, y0_, a_, b_, s_, alpha_, theta0_;
};


//----------------------------------------------------------------------------//
// x*(1-x)*y*(1-y)*exp(a*x*x + b*y*y + c*x + d*y + e)
//
class ScalarFunction2D_VarExp : public Function2DVirtualInterface<ScalarFunction2D_VarExp,ScalarFunctionTraits>
{
public:
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> a{"a", NO_DEFAULT, NO_RANGE, "a exponent"};
    const ParameterNumeric<Real> b{"b", NO_DEFAULT, NO_RANGE, "b exponent"};
    const ParameterNumeric<Real> c{"c", NO_DEFAULT, NO_RANGE, "c exponent"};
    const ParameterNumeric<Real> d{"d", NO_DEFAULT, NO_RANGE, "d exponent"};
    const ParameterNumeric<Real> e{"e", NO_DEFAULT, NO_RANGE, "e exponent"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction2D_VarExp( const PyDict& d ) :
            a_(d.get(ParamsType::params.a)),
            b_(d.get(ParamsType::params.b)),
            c_(d.get(ParamsType::params.c)),
            d_(d.get(ParamsType::params.d)),
            e_(d.get(ParamsType::params.e)) {}

  ScalarFunction2D_VarExp(Real a, Real b, Real c, Real d, Real e) : a_(a), b_(b), c_(c), d_(d), e_(e) {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    return x*y*exp(a_*x*x + c_*x + b_*y*y + d_*y + e_)*(x - 1)*(y - 1);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_VarExp: " << std::endl;
    out << indent << "  a_, b_, c_, d_, e_ = " << a_ << ", " << b_ << ", " << c_ << ", " << d_ << ", " << e_ << std::endl;
  }
private:
  Real a_, b_, c_, d_, e_;
};

//----------------------------------------------------------------------------//
// (1-exp(a*x))*(1-exp(b*y))*(1-exp(c*(1-x)))*(1-exp(d*(1-y)));
//
class ScalarFunction2D_VarExp2 : public Function2DVirtualInterface<ScalarFunction2D_VarExp2,ScalarFunctionTraits>
{
public:
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> a{"a", NO_DEFAULT, NO_RANGE, "a exponent"};
    const ParameterNumeric<Real> b{"b", NO_DEFAULT, NO_RANGE, "b exponent"};
    const ParameterNumeric<Real> c{"c", NO_DEFAULT, NO_RANGE, "c exponent"};
    const ParameterNumeric<Real> d{"d", NO_DEFAULT, NO_RANGE, "d exponent"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction2D_VarExp2( const PyDict& d ) :
            a_(d.get(ParamsType::params.a)),
            b_(d.get(ParamsType::params.b)),
            c_(d.get(ParamsType::params.c)),
            d_(d.get(ParamsType::params.d)) {}

  ScalarFunction2D_VarExp2(Real a, Real b, Real c, Real d) : a_(a), b_(b), c_(c), d_(d) {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    return (1-exp(a_*x))*(1-exp(b_*y))*(1-exp(c_*(1-x)))*(1-exp(d_*(1-y)));
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_VarExp2: " << std::endl;
    out << indent << "  a_, b_, c_, d_ = " << a_ << ", " << b_ << ", " << c_ << ", " << d_ << std::endl;
  }
private:
  Real a_, b_, c_, d_;
};

//----------------------------------------------------------------------------//
// (exp(a*x*y) -1)*(1-x)*(1-y)
//
class ScalarFunction2D_VarExp3 : public Function2DVirtualInterface<ScalarFunction2D_VarExp3,ScalarFunctionTraits>
{
public:
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> a{"a", NO_DEFAULT, NO_RANGE, "exponent coefficient"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction2D_VarExp3( const PyDict& d ) :
            a_(d.get(ParamsType::params.a)) {}

  explicit ScalarFunction2D_VarExp3(const Real a) : a_(a) {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    return expm1(a_*x*y)*(1-x)*(1-y);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_VarExp3: " << std::endl;
    out << indent << "  a_ = " << a_ << std::endl;
  }
private:
  const Real a_;
};


//----------------------------------------------------------------------------//
// solution: A*sin(a*PI*x)*sin(b*PI*y) + c

class ScalarFunction2D_SineSine : public Function2DVirtualInterface<ScalarFunction2D_SineSine,ScalarFunctionTraits>
{
public:
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> a{"a", 2, NO_RANGE, "Frequency factor"};
    const ParameterNumeric<Real> b{"b", 2, NO_RANGE, "Frequency factor"};
    const ParameterNumeric<Real> C{"C", 0, NO_RANGE, "Constant offset"};
    const ParameterNumeric<Real> A{"A", 1, NO_RANGE, "Amplitude"};

    static void checkInputs(PyDict d);
    static ParamsType params;
  };

  explicit ScalarFunction2D_SineSine( const PyDict& d ) :
            a_(d.get(ParamsType::params.a)),
            b_(d.get(ParamsType::params.b)),
            C_(d.get(ParamsType::params.C)),
            A_(d.get(ParamsType::params.A)){}

  explicit ScalarFunction2D_SineSine(const Real a=2, const Real b=2, const Real C=0, const Real A=1) :
              a_(a), b_(b), C_(C), A_(A) {}

  ~ScalarFunction2D_SineSine() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    return A_*sin(a_*PI*x)*sin(b_*PI*y) + C_;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_SineSine: " << std::endl;
    out << indent << "  a_, b_, C_, A_ = " << a_ << ", " << b_ << ", " << C_ << ", " << A_ << std::endl;
  }
protected:
  const Real a_, b_, C_, A_;
};



//----------------------------------------------------------------------------//
// solution: sin(2*PI*x)*sin(2*PI*y)*sin(2*PI*t)

class ScalarFunction2D_SineSineSineUnsteady : public Function2DVirtualInterface<ScalarFunction2D_SineSineSineUnsteady,ScalarFunctionTraits>
{
public:
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  typedef ParamsNone ParamsType;

  explicit ScalarFunction2D_SineSineSineUnsteady( const PyDict& d ) {}
  ScalarFunction2D_SineSineSineUnsteady() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    return sin(2*PI*x)*sin(2*PI*y)*sin(2*PI*time);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_SineSineSineUnsteady: " << std::endl;
  }
};

//----------------------------------------------------------------------------//
// A decaying-in-time, periodically sinusoidal MMS:
// solution: exp(-lambda*t)*sin(2*pi*x)*cos(2*pi*y)
//----------------------------------------------------------------------------//
class ScalarFunction2D_SineCosineDecay :
    public Function2DVirtualInterface<ScalarFunction2D_SineCosineDecay, ScalarFunctionTraits>
{
public:
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ= T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> lambda{"lambda", 0.5, 0, NO_LIMIT, "decay rate constant."};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction2D_SineCosineDecay(PyDict& d) :
          lambda_(d.get(ParamsType::params.lambda))
  {
    // do nothing except for setting in privates.
  }

  explicit ScalarFunction2D_SineCosineDecay(const Real &lambda) :
          lambda_(lambda)
  {
    // do nothing except for setting in privates.
  }
  ~ScalarFunction2D_SineCosineDecay() {}

  template<class T> ArrayQ<T> operator() (const T &x, const T &y, const T &t) const
  {
    return exp(-lambda_*t)*sin(2.*PI*x)*cos(2.*PI*y);
  }

  void dump(int indentSize= 0, std::ostream& out= std::cout) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_SineCosineDecay:\tlambda_= " << lambda_ << std::endl;
  }

private:
  Real lambda_;
};

//----------------------------------------------------------------------------//
// A decaying-in-time, expanding circle
//----------------------------------------------------------------------------//
class ScalarFunction2D_CircularWaveDecay :
    public Function2DVirtualInterface<ScalarFunction2D_CircularWaveDecay, ScalarFunctionTraits>
{
public:
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ= T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> alpha{"alpha", 1.0 , 0, NO_LIMIT, "temporal decay rate of intensity."};
    const ParameterNumeric<Real> k0{"k0", 1.0 , 0, NO_LIMIT, "initial intensity."};
    const ParameterNumeric<Real> k1{"k1", 100.0 , 0, NO_LIMIT, "spatial decay rate of intensity."};
    const ParameterNumeric<Real> velocity{"velocity", 0.5 , 0, NO_LIMIT, "constant wave propagation velocity."};
    const ParameterNumeric<Real> radius0{"radius0", 0.4 , 0.00001, 0.999999, "initial wave radius."};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction2D_CircularWaveDecay(const PyDict& d) :
    alpha_(d.get(ParamsType::params.alpha)),
    k0_(d.get(ParamsType::params.k0)),
    k1_(d.get(ParamsType::params.k1)),
    velocity_(d.get(ParamsType::params.velocity)),
    radius0_(d.get(ParamsType::params.radius0))
  {}

  explicit ScalarFunction2D_CircularWaveDecay(const Real& alpha , const Real& k0 , const Real& k1 , const Real& velocity , const Real& radius0 ) :
    alpha_(alpha), k0_(k0), k1_(k1), velocity_(velocity), radius0_(radius0)
  {}

  ~ScalarFunction2D_CircularWaveDecay() {}

  template<class T>
  ArrayQ<T> radius( const T& t ) const
  {
    return radius0_ + velocity_*t;
  }

  template<class T>
  ArrayQ<T> operator() (const T &x, const T &y, const T &t) const
  {
    return k0_*exp(-alpha_*t)*exp( -k1_*pow(radius(t) - sqrt(x*x + y*y),2.) );
  }

  void dump(int indentSize= 0, std::ostream& out= std::cout) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_CircularWaveDecay:\t" << std::endl;
  }

private:
  Real alpha_;
  Real k0_;
  Real k1_;
  Real velocity_;
  Real radius0_;
};


//----------------------------------------------------------------------------//
// Polar Gauss: k0*exp( -k1*(r - r0_)^2 ) + k0/4*r^(2/3), r^2 = x^ + y^2
//----------------------------------------------------------------------------//
class ScalarFunction2D_PolarGauss :
    public Function2DVirtualInterface<ScalarFunction2D_PolarGauss, ScalarFunctionTraits>
{
public:
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ= T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> k0{"k0", 1.0 , 0, NO_LIMIT, "initial intensity."};
    const ParameterNumeric<Real> k1{"k1", 100.0 , 0, NO_LIMIT, "spatial decay rate of intensity."};
    const ParameterNumeric<Real> r0{"radius0", 0.5, 0.00001, 0.999999, "circle radius."};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction2D_PolarGauss(const PyDict& d) :
    k0_(d.get(ParamsType::params.k0)),
    k1_(d.get(ParamsType::params.k1)),
    r0_(d.get(ParamsType::params.r0))
  {}

  explicit ScalarFunction2D_PolarGauss(const Real& alpha , const Real& k0 , const Real& k1 , const Real& r0 ) :
    k0_(k0), k1_(k1), r0_(r0)
  {}

  ~ScalarFunction2D_PolarGauss() {}

  template<class T>
  ArrayQ<T> operator() (const T &x, const T &y, const T &t) const
  {
    T r = sqrt(x*x + y*y);
    return k0_*exp( -k1_*pow(r - r0_,2.) ) + k0_/4.*pow(r,2./3.);
  }

  void dump(int indentSize= 0, std::ostream& out= std::cout) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_PolarGauss:\t" << std::endl;
  }

private:
  Real k0_;
  Real k1_;
  Real r0_;
};

#if 0
//----------------------------------------------------------------------------//
// Linear radial variation: u = exp( alpha*sqrt( (x - x0)^2 + (y - y0)^2 ) )
//----------------------------------------------------------------------------//
class ScalarFunction2D_Radial :
    public Function2DVirtualInterface<ScalarFunction2D_Radial, ScalarFunctionTraits>
{
public:
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ= T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> alpha{"alpha", 1.0 , 0, NO_LIMIT, "Slope of radial variation"};
    const ParameterNumeric<Real> x0{"x0", 1.0 , 0, NO_LIMIT, "Center of radial variation"};
    const ParameterNumeric<Real> y0{"y0", 100.0 , 0, NO_LIMIT, "Center of radial variation"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction2D_Radial(PyDict& d) :
    alpha_(d.get(ParamsType::params.alpha)),
    x0_(d.get(ParamsType::params.x0)),
    y0_(d.get(ParamsType::params.y0))
  {}

  explicit ScalarFunction2D_Radial(const Real& alpha , const Real& x0 , const Real& y0 ) :
    alpha_(alpha), x0_(x0), y0_(y0)
  {}

  ~ScalarFunction2D_Radial() {}

  template<class T>
  ArrayQ<T> operator() (const T &x, const T &y, const T &t) const
  {
    return exp( alpha_*sqrt( (x - x0_)*(x - x0_) + (y - y0_)*(y - y0_) ) );
  }

  void dump(int indentSize= 0, std::ostream& out= std::cout) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_Radial:\t" << std::endl;
  }

private:
  Real alpha_;
  Real x0_;
  Real y0_;
};
#endif

//----------------------------------------------------------------------------//
// solution: double BL
//
// u(0,0) = 1  u(x,1) = 0  u(1,y) = 0
//
// NOTE: exact solution for 2D advection-diffusion: a ux + b uy - nu (uxx + uyy) = 0

class ScalarFunction2D_DoubleBL : public Function2DVirtualInterface<ScalarFunction2D_DoubleBL,ScalarFunctionTraits>
{
public:
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> a{"a", NO_DEFAULT, NO_RANGE, "a x-velocity"};
    const ParameterNumeric<Real> b{"b", NO_DEFAULT, NO_RANGE, "b y-velocity"};
    const ParameterNumeric<Real> nu{"nu", NO_DEFAULT, NO_RANGE, "viscosity"};
    const ParameterNumeric<Real> offset{"offset", 0.0, NO_RANGE, "constant offset"};
    const ParameterNumeric<Real> scale{"scale", 1.0, NO_RANGE, "scaling"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction2D_DoubleBL( const PyDict& d ) :
            a_(d.get(ParamsType::params.a)),
            b_(d.get(ParamsType::params.b)),
            nu_(d.get(ParamsType::params.nu)),
            offset_(d.get(ParamsType::params.offset)),
            scale_(d.get(ParamsType::params.scale)){}

  ScalarFunction2D_DoubleBL( const Real& a, const Real& b, const Real& nu,
                             const Real& offset = 0.0, const Real& scale = 1.0 )
  : a_(a), b_(b), nu_(nu), offset_(offset), scale_(scale) {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    T f, g;
    f = (1 - exp(-a_*(1 - x)/nu_)) / (1 - exp(-a_/nu_));
    g = (1 - exp(-b_*(1 - y)/nu_)) / (1 - exp(-b_/nu_));
    return scale_*f*g + offset_;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_DoubleBL: " << std::endl;
    out << indent << "  a_, b_, nu_, offset_, scale_ = " << a_ << ", " << b_ << ", "
        << nu_ << ", " << offset_ << ", " << scale_ << std::endl;
  }
private:
  Real a_, b_;    // advection velocity
  Real nu_;       // diffusion coefficient
  Real offset_;        // constant offset
  Real scale_;        // scaling
};

//Exact Adjoint for Double BL, 2D advection-diffusion: a ux + b uy - nu (uxx + uyy) = 0, integral volume output
class ScalarFunction2D_DoubleBLAdj : public Function2DVirtualInterface<ScalarFunction2D_DoubleBLAdj,ScalarFunctionTraits>
{
public:
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> a{"a", NO_DEFAULT, NO_RANGE, "a x-velocity"};
    const ParameterNumeric<Real> b{"b", NO_DEFAULT, NO_RANGE, "b y-velocity"};
    const ParameterNumeric<Real> nu{"nu", NO_DEFAULT, NO_RANGE, "viscosity"};
    const ParameterNumeric<Real> c{"c", 0.0, NO_RANGE, "constant offset"};
    const ParameterNumeric<Real> s{"s", 1.0, NO_RANGE, "scaling"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction2D_DoubleBLAdj( const PyDict& d ) :
    a_(d.get(ParamsType::params.a)),
    b_(d.get(ParamsType::params.b)),
    nu_(d.get(ParamsType::params.nu)),
    c_(d.get(ParamsType::params.c)),
    s_(d.get(ParamsType::params.s)){}

  ScalarFunction2D_DoubleBLAdj( const Real& a, const Real& b, const Real& nu,
                             const Real& c = 0.0, const Real& s = 1.0 ) : a_(a), b_(b), nu_(nu), c_(c), s_(s) {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    T f, g;
    f = -( -exp(a_/nu_*(1-x)) - exp(a_/nu_)*(x-1) + x  ) / (a_ - a_ * exp(a_/nu_));
    g = -( -exp(b_/nu_*(1-y)) - exp(b_/nu_)*(y-1) + y  ) / (b_ - b_ * exp(b_/nu_));
    return s_*f*g + c_;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_DoubleBLAdj: " << std::endl;
    out << indent << "  a_, b_, nu_, c_, s_ = " << a_ << ", " << b_ << ", " << nu_ << ", " << c_ << ", " << s_ << std::endl;
  }
private:
  Real a_, b_;    // advection velocity
  Real nu_;       // diffusion coefficient
  Real c_;        // constant offset
  Real s_;        // scaling
};



//----------------------------------------------------------------------------//
// solution: vortex
//
// u(x,y) = Log(r),  r^2 = (x - x0)^2 + (y - y0)^2
//
// NOTE: exact solution for 2D Laplacian: (uxx + uyy) = 0

class ScalarFunction2D_Vortex : public Function2DVirtualInterface<ScalarFunction2D_Vortex,ScalarFunctionTraits>
{
public:
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> x0{"x0", NO_DEFAULT, NO_RANGE, "x-component of vortex center"};
    const ParameterNumeric<Real> y0{"y0", NO_DEFAULT, NO_RANGE, "y-component of vortex center"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction2D_Vortex( const PyDict& d ) :
            x0_(d.get(ParamsType::params.x0)),
            y0_(d.get(ParamsType::params.y0)) {}

  ScalarFunction2D_Vortex( const Real& x0, const Real& y0 ) : x0_(x0), y0_(y0) {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    T r = sqrt((x - x0_)*(x - x0_) + (y - y0_)*(y - y0_));
    return log(r);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_Vortex: " << std::endl;
    out << indent << "  x0_, y0_ = " << x0_ << ", " << y0_ << std::endl;
  }
private:
  Real x0_, y0_;    // vortex center
};


//----------------------------------------------------------------------------//
// solution: sin(2*PI*xi)*sin(2*PI*eta)
//
//  {xi, eta} = {{cs, sn}, {-sn, cs}}.{x, y}  sn = Sin[th]  cs = Cos[th]

class ScalarFunction2D_SineSineRotated : public Function2DVirtualInterface<ScalarFunction2D_SineSineRotated,ScalarFunctionTraits>
{
public:
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> theta{"theta", NO_DEFAULT, NO_RANGE, "Rotation angle"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction2D_SineSineRotated( const Real th = PI/7. ) : th_(th) {}

  explicit ScalarFunction2D_SineSineRotated( const PyDict& d )
  :  th_(d.get(ParamsType::params.theta))
  {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    T xi, eta;

    xi  =  x*cos(th_) + y*sin(th_);
    eta = -x*sin(th_) + y*cos(th_);
    return sin(2*PI*xi)*sin(2*PI*eta);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_SineSineRotated: " << std::endl;
    out << indent << "  th_ = " << th_ << std::endl;
  }
protected:
  const Real th_;
};

//----------------------------------------------------------------------------//
// solution: r^alpha type corner singularity
//
// u(x,y) = r^alpha sin( alpha*(theta + theta0) ),  r^2 = x^2 + y^2, theta = atan2(y,x)

class ScalarFunction2D_CornerSingularity : public Function2DVirtualInterface<ScalarFunction2D_CornerSingularity,ScalarFunctionTraits>
{
public:
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> alpha{"alpha", 2.0/3.0, 0, NO_LIMIT, "strength of singularity"};
    const ParameterNumeric<Real> theta0{"theta0", PI/2.0, NO_RANGE, "offset angle"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction2D_CornerSingularity( const PyDict& d ) :
            alpha_(d.get(ParamsType::params.alpha)),
            theta0_(d.get(ParamsType::params.theta0)) {}

  ScalarFunction2D_CornerSingularity( const Real& alpha, const Real& theta0 ) : alpha_(alpha), theta0_(theta0)
  {
    SANS_ASSERT(alpha_ > 0);
  }

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    T r = sqrt( x*x + y*y);
    T theta = atan2(y,x);
    return pow(r,alpha_)*sin(alpha_*(theta + theta0_));
  }

  template<class T>
  ArrayQ<T> operator()( const DLA::VectorS<PhysDim::D,T>& X ) const
  {
    return operator()(X[0],X[1],0.0);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_CornerSingularity: " << std::endl;
    out << indent << "  alpha_, theta0_ = " << alpha_ << ", " << theta0_ << std::endl;
  }
private:
  Real alpha_, theta0_;    // singularity parameters
};

//----------------------------------------------------------------------------//
// solution: 2D boundary layer resulting from singular perturbation
//
// u(x,y) = exp(-x/e ) + beta/((p+1)!)*y^(p+1)
//
// Running with beta = 0 is to turn off the regularization
//
class ScalarFunction2D_BoundaryLayer : public Function2DVirtualInterface<ScalarFunction2D_BoundaryLayer,ScalarFunctionTraits>
{
public:
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = T;

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> epsilon{"epsilon", NO_DEFAULT, 0, NO_LIMIT, "characteristic length of singular perturbation"};
    const ParameterNumeric<Real> beta{"beta", NO_DEFAULT, 0, NO_LIMIT, "regularization constant"};
    const ParameterNumeric<int> p{"p", NO_DEFAULT, 0, NO_LIMIT, "solution order"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction2D_BoundaryLayer( const PyDict& d ) :
            epsilon_(d.get(ParamsType::params.epsilon)),
            beta_(d.get(ParamsType::params.beta)),
            p_(d.get(ParamsType::params.p)) {}

  ScalarFunction2D_BoundaryLayer( const Real& epsilon, const Real& beta, const int& p) : epsilon_(epsilon), beta_(beta), p_(p)
  {
    SANS_ASSERT(epsilon_ > 0);
    SANS_ASSERT(beta_ >= 0);
    SANS_ASSERT(p_ > 0);
  }

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    return exp(-x/epsilon_) + (beta_/factorial(p_+1))*pow(y,(p_+1));
  }

  static int
  factorial( int n )
  {
    int m;
    if (n <= 1)
      m = 1;
    else
      m = n * factorial(n - 1);

    return m;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_BoundaryLayer: " << std::endl;
    out << indent << "  epsilon_, beta_, p_ = " << epsilon_ << ", " << beta_ << ", " << p_ << std::endl;
  }
private:
  Real epsilon_, beta_;
  int p_;
};

//----------------------------------------------------------------------------//
// solution: 2D Gaussian function
//
// u(x,y) = a * exp( -1/2 * ( (x - x0)^2/sx^2 + (y - y0)^2/sy^2 ) )

class ScalarFunction2D_Gaussian : public Function2DVirtualInterface<ScalarFunction2D_Gaussian,ScalarFunctionTraits>
{
public:
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> a{"a", NO_DEFAULT, NO_RANGE, "amplitude of Gaussian"};
    const ParameterNumeric<Real> x0{"x0", NO_DEFAULT, NO_RANGE, "x-location of center"};
    const ParameterNumeric<Real> y0{"y0", NO_DEFAULT, NO_RANGE, "y-location of center"};
    const ParameterNumeric<Real> sx{"sx", NO_DEFAULT, NO_RANGE, "sigma-x - standard deviation in x-direction"};
    const ParameterNumeric<Real> sy{"sy", NO_DEFAULT, NO_RANGE, "sigma-y - standard deviation in y-direction"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction2D_Gaussian( const PyDict& d ) :
            a_(d.get(ParamsType::params.a)),
            x0_(d.get(ParamsType::params.x0)),
            y0_(d.get(ParamsType::params.y0)),
            sx_(d.get(ParamsType::params.sx)),
            sy_(d.get(ParamsType::params.sy)){}

  ScalarFunction2D_Gaussian( const Real& a, const Real& x0, const Real& y0, const Real& sx, const Real& sy ) :
    a_(a), x0_(x0), y0_(y0), sx_(sx), sy_(sy) {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    return a_ * exp( -0.5*( (x - x0_)*(x - x0_)/(sx_*sx_) + (y - y0_)*(y - y0_)/(sy_*sy_) ) );
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_BoundaryLayer: " << std::endl;
    out << indent << "  a_, x0_, y0_, sx_, sy_ = " << a_ << ", " << x0_ << ", " << y0_ << ", " << sx_ << ", " << sy_ << std::endl;
  }
private:
  Real a_; //amplitude
  Real x0_, y0_; //center location
  Real sx_, sy_; //standard deviations in x and y directions
};

//----------------------------------------------------------------------------//
// solution: 2D Gaussian function
//
// u(x,y) = a * exp( -1/2 * ( (y - yc(x))^2/sy^2 ) )
//  yc(x) = y0 + y1*cos(2*PI*x)

class ScalarFunction2D_GaussianCosine : public Function2DVirtualInterface<ScalarFunction2D_GaussianCosine,ScalarFunctionTraits>
{
public:
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> a{"a", NO_DEFAULT, NO_RANGE, "amplitude of Gaussian"};
    const ParameterNumeric<Real> y0{"y0", NO_DEFAULT, NO_RANGE, "y-location of center"};
    const ParameterNumeric<Real> y1{"y1", NO_DEFAULT, NO_RANGE, "amplitude of sine function"};
    const ParameterNumeric<Real> sy{"sy", NO_DEFAULT, NO_RANGE, "sigma-y - standard deviation in y-direction"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction2D_GaussianCosine( const PyDict& d ) :
            a_(d.get(ParamsType::params.a)),
            y0_(d.get(ParamsType::params.y0)),
            y1_(d.get(ParamsType::params.y1)),
            sy_(d.get(ParamsType::params.sy)){}

  ScalarFunction2D_GaussianCosine( const Real& a, const Real& y0, const Real& y1, const Real& sy ) :
    a_(a), y0_(y0), y1_(y1), sy_(sy) {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    T yc = y0_ + y1_*cos(2*PI*x);
    return a_ * exp( -0.5*( (y - yc)*(y - yc)/(sy_*sy_) ) );
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_BoundaryLayer: " << std::endl;
    out << indent << "  a_, y0_, y1_, sy_ = " << a_ << ", " << y0_ << ", " << y1_ << ", " << sy_ << std::endl;
  }

private:
  Real a_; //amplitude
  Real y0_; //center location
  Real y1_; //sine amplitude
  Real sy_; //standard deviations in y directions
};


//----------------------------------------------------------------------------//
// solution: 2D Gaussian function
//
// u(x,y) = a * exp( -1/2 * ( (x - x0)^2/sx^2 + (y - y0)^2/sy^2 ) )

class ScalarFunction2D_Ojeda : public Function2DVirtualInterface<ScalarFunction2D_Ojeda,ScalarFunctionTraits>
{
public:
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> a{"a", NO_DEFAULT, NO_RANGE, "Baseline value"};
    const ParameterNumeric<Real> b{"b", NO_DEFAULT, NO_RANGE, "amplitude of bump"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction2D_Ojeda( const PyDict& d ) :
    a_(d.get(ParamsType::params.a)),
    b_(d.get(ParamsType::params.b)) {}

  ScalarFunction2D_Ojeda( const Real& a, const Real& b ) :
    a_(a), b_(b) {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {

    //if (x >= 0.375 && y >= 0.375 && x <= 0.625 && y <= 0.625)
    //{
    //  return a_ + b_* pow(sin(4.0*PI*(0.625-x)),5)*pow(sin(4.0*PI*(0.625-y)),5);
    //}
    //else
    //{
    //  return a_;
    //}

    return a_ + b_*exp( -200.*(x-0.5)*(x-0.5) - 200*(y-0.5)*(y-0.5)  );
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_Ojeda: " << std::endl;
    out << indent << "  a_, b_," << a_ << ", " << b_ << std::endl;
  }
private:
  Real a_, b_; //amplitude
};


//----------------------------------------------------------------------------//
// solution: Laplacian with unit forcing function
//
// NOTE: exact solution for Poisson: -(uxx + uyy) = 1 with BC = 0 and (0,0) <= (x,y) <= (1,1)
//       solution via series solution (augmented with particular solution)
//
// ref: Elman-Syvester-Wathen, 2005
//
// Not using virtual interface due to explicit derivative calculations
class ScalarFunction2D_LaplacianUnitForcing
{
public:
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  typedef ParamsNone ParamsType;

  explicit ScalarFunction2D_LaplacianUnitForcing( const PyDict& d ) : ScalarFunction2D_LaplacianUnitForcing() {}

  ScalarFunction2D_LaplacianUnitForcing() : max_(-1)
  {
    // Find the number upper bound to truncate the infinite series
    // NOTE: worst case location is y=0 or y=1 and x=1/4 or x=3/4;
    // series is oscillatory, so adding two terms better reflects series convergence
    Real inc = 1;
    while ( fabs(inc) > inc_tol_ && max_ < max_upperlimit_ )
    {
      max_+= 4;
      inc = seriesTerm( max_, 0.25, 0. ) + seriesTerm( max_ + 2, 0.25, 0. );
    }
    //std::cout << "ScalarFunction2D_LaplacianUnitForcing::ctor  max_ = " << max_ << "  inc = " << inc << std::endl;
    SANS_ASSERT( max_ < max_upperlimit_ );
  }


  ArrayQ<Real> operator()( const Real& x, const Real& y, const Real& time ) const
  {
    SANS_ASSERT_MSG( x >= 0 && x <= 1, "x = %f", x );
    SANS_ASSERT_MSG( y >= 0 && y <= 1, "y = %f", y );

    Real u, up, us;

    up = 0.5*x*(1 - x);

    us = 0;
    for (int k = 1; k < max_; k += 2)
    {
      Real v = seriesTerm( k, x, y );
      us += v;
      if (fabs(v) < 1e-15)
      {
        break;
      }
    }

    u = up - 4*us;
    return u;
  }

  void operator()( const Real& x, const Real& y, const Real& t,
                   ArrayQ<Real>& q, ArrayQ<Real>& qx, ArrayQ<Real>& qy, ArrayQ<Real>& qt,
                   ArrayQ<Real>& qxx,
                   ArrayQ<Real>& qyx, ArrayQ<Real>& qyy ) const
  {
    SANS_ASSERT_MSG( x >= 0 && x <= 1, "x = %f", x );
    SANS_ASSERT_MSG( y >= 0 && y <= 1, "y = %f", y );

    //Steady
    qt = 0;

    Real u, ux, uy;
    Real up, upx;
    Real us, usx, usy;

    up  = 0.5*x*(1 - x);
    upx = 0.5 - x;

    us = usx = usy = 0;
    for (int k = 1; k < max_; k += 2)
    {
      Real vs, vsx, vsy;
      seriesTerm( k, x, y, vs, vsx, vsy );
      us  += vs;
      usx += vsx;
      usy += vsy;
      if ( (fabs(vs) < 1e-15) && (fabs(vsx) < 1e-15) && (fabs(vsy) < 1e-15) )
      {
        //        std::cout << "  2nd (x,y) = (" << x << ", " << y << "):  k = " << k << std::endl;
        break;
      }
    }

    u  = up  - 4*us;
    ux = upx - 4*usx;
    uy =     - 4*usy;

    q   = u;
    qx  = ux;
    qy  = uy;
    qxx = 0;
    qyx = 0;
    qyy = 0;
  }

  // This is needed because the need for an explicit derivative calculation
  static const int D = 2;
  typedef DLA::VectorS<D,Real> VectorX;

  ArrayQ<Real> operator()( const VectorX& X ) const { return operator()(X[0], X[1], 0.); }

  template<class T>
  void operator()( const VectorX& X, ArrayQ<T>& q,
                   DLA::VectorS< D, ArrayQ<T> >& gradq,
                   DLA::MatrixSymS< D, ArrayQ<T> >& hessianq ) const
  {
    operator()(X[0], X[1], 0., q, gradq[0], gradq[1],
               hessianq(0,0),
               hessianq(1,0), hessianq(1,1));
  }

  // Because the virtual interface isn't being used
  void gradient( const Real& x, const Real&y, const Real& time, ArrayQ<Real>&q,
                 ArrayQ<Real>& qx, ArrayQ<Real>& qy, ArrayQ<Real>& qt ) const
  {
    ArrayQ<Real> qxx = 0, qyx = 0, qyy = 0;
    operator()(x,y,time,q,qx,qy,qt,qxx,qyx,qyy);
  }

  void secondGradient( const Real& x, const Real&y, const Real& time, ArrayQ<Real>&q,
                       ArrayQ<Real>& qx, ArrayQ<Real>& qy, ArrayQ<Real>& qt,
                       ArrayQ<Real>& qxx,
                       ArrayQ<Real>& qyx, ArrayQ<Real>& qyy ) const
  {
    operator()(x,y,time,q,qx,qy,qt,qxx,qyx,qyy);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_LaplacianUnitForcing: " << std::endl;
  }
protected:
  Real seriesTerm( const int k, const Real& x, const Real& y ) const
  {
    Real kpi = k*PI;
    Real term1 = exp(-kpi*y) + exp(-kpi*(1-y));
    Real term2 = (kpi*kpi*kpi) * (1 + exp(-kpi));
    Real result = sin(kpi*x)*term1/term2;

    return result;
  }

  void seriesTerm( const int& k, const Real& x, const Real& y, Real& u, Real& ux, Real& uy ) const
  {
    Real kpi = k*PI;
    Real term1, term2;

    term1 = exp(-kpi*y) + exp(-kpi*(1-y));
    term2 = (kpi*kpi*kpi) * (1 + exp(-kpi));
    u = sin(kpi*x)*term1/term2;

    term2 = (kpi*kpi) * (1 + exp(-kpi));
    ux = cos(kpi*x)*term1/term2;   // NOTE: cancellation of k*PI

    term1 = - exp(-kpi*y) + exp(-kpi*(1-y));
    uy = sin(kpi*x)*term1/term2;   // NOTE: cancellation of k*PI between term1 & term2
  }

#if 0
  // Pade accelerated series sum: Cohen-Villegas-Zagier [2000], Algorithm 1
  Real seriesSum( const int& kmax, const Real& x, const Real& y ) const
  {
    Real b, c, d, s;
    Real sgn;

    d = pow(3 + sqrt(8), kmax);
    d = 0.5*(d + 1./d);
    b = -1;
    c = -d;
    s =  0;

    for (int k = 0; k < kmax; k++)
    {
      sgn = k % 2 == 0 ? 1 : -1;
      c = b - c;
      s += c*sgn*seriesTerm(2*k+1, x, y);
      b = b*(k + kmax)*(k - kmax) / ((k + 0.5)*(k + 1));
    }

    return s/d;
  }

  void seriesSum( const int& kmax, const Real& x, const Real& y, Real& u, Real& ux, Real& uy ) const
  {
    Real b, c, d, s, sx, sy;
    Real v, vx, vy;
    Real sgn;

    d = pow(3 + sqrt(8), kmax);
    d = 0.5*(d + 1./d);
    b = -1;
    c = -d;
    s = sx = sy = 0;

    for (int k = 0; k < kmax; k++)
    {
      sgn = k % 2 == 0 ? 1 : -1;
      c = b - c;
      seriesTerm( 2*k+1, x, y, v, vx, vy );
      s  += c*sgn*v;
      sx += c*sgn*vx;
      sy += c*sgn*vy;
      b = b*(k + kmax)*(k - kmax) / ((k + 0.5)*(k + 1));
    }

    u  = s/d;
    ux = sx/d;
    uy = sy/d;
  }
#endif

private:
  int max_;    // maximum number of summations for fourier series

  const int max_upperlimit_ = 1301;
  const Real inc_tol_ = 1e-11;
};


//----------------------------------------------------------------------------//
// solution: adjoint for advection-diffusion with average output functional
//
// NOTE: exact solution for - a ux - b uy - nu (uxx + uyy) = 1 with BC = 0 and (0,0) <= (x,y) <= (1,1)
//       solution via series solution (augmented with particular solution)
//
// Not using virtual interface due to explicit derivative calculations
class ScalarFunction2D_AdjointADAverage
{
public:
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  ScalarFunction2D_AdjointADAverage( const Real& a, const Real& b, const Real& nu ) :
    a_(a), b_(b), nu_(nu), kmax_(0)
  {
    // Find the number upper bound to truncate the infinite series
    Real us, usm;

    usm = 0;
    for (int kmax = 1; kmax < 120; kmax++)
    {
      us = seriesSum( kmax, 0.5, 0. );
      //  std::cout << "kmax = " << kmax << "  u = " << us << "  (" << us - usm << ")" << std::endl;
      if (fabs(us - usm) < 1e-15)
      {
        kmax_ = kmax;
        break;
      }
      usm = us;
    }
    //std::cout << "ScalarFunction2D_AdjointADAverage::ctor  kmax_ = " << kmax_ << "  inc = " << us - usm << std::endl;
    SANS_ASSERT_MSG( kmax_ > 0, "failure to initialize series upper limit" );
  }

  ArrayQ<Real> operator()( const Real& x, const Real& y, const Real& time ) const
  {
    Real u, us, up;

    up = -x/a_ + (1 - exp(-a_*x/nu_))/(a_*(1 - exp(-a_/nu_)));

    us = seriesSum( kmax_, x, y );

    u = up + 2*us;
    return u;
  }

  void operator()( const Real& x, const Real& y, const Real& t,
                   ArrayQ<Real>& q, ArrayQ<Real>& qx, ArrayQ<Real>& qy, ArrayQ<Real>& qt,
                   ArrayQ<Real>& qxx,
                   ArrayQ<Real>& qyx, ArrayQ<Real>& qyy ) const
  {
    Real u, ux, uy;
    Real up, upx;
    Real us, usx, usy;

    up  = -x/a_ + (1 - exp(-a_*x/nu_))/(a_*(1 - exp(-a_/nu_)));
    upx = -1./a_ + exp(-a_*x/nu_)/(nu_*(1 - exp(-a_/nu_)));

    seriesSum( kmax_, x, y, us, usx, usy );

    u  = up  + 2*us;
    ux = upx + 2*usx;
    uy =     + 2*usy;

    q   = u;
    qx  = ux;
    qy  = uy;
    qxx = 0;
    qyx = 0;
    qyy = 0;
  }


  // This is needed because the need for an explicit derivative calculation
  static const int D = 2;
  typedef DLA::VectorS<D,Real> VectorX;

  ArrayQ<Real> operator()( const VectorX& X ) const { return operator()(X[0], X[1], 0.); }

  template<class T>
  void operator()( const VectorX& X, ArrayQ<T>& q,
                   DLA::VectorS< D, ArrayQ<T> >& gradq,
                   DLA::MatrixSymS< D, ArrayQ<T> >& hessianq ) const
  {
    operator()(X[0], X[1], 0., q, gradq[0], gradq[1],
               hessianq(0,0),
               hessianq(1,0), hessianq(1,1));
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_AdjointADAverage: " << std::endl;
    out << indent << "  a_, b_, nu_ = " << a_ << ", " << b_ << ", " << nu_ << std::endl;
  }
protected:
  Real seriesTerm( const int k, const Real& x, const Real& y ) const
  {
    Real kpi  = k*PI;
    Real beta = sqrt(kpi*kpi + (a_*a_ + b_*b_)/(4*nu_*nu_));
    Real tmp  = kpi*kpi + a_*a_/(4*nu_*nu_);
    Real sgn  = k % 2 == 0 ? 1 : -1;
    Real sinh1 = exp(-beta*(2-y)) - exp(-beta*y);
    Real sinh2 = exp(-beta*(1+y)) - exp(-beta*(1-y));
    Real f = sin(kpi*x) * exp(-0.5*a_*x/nu_);
    Real c = kpi*(1 - sgn*exp(0.5*a_/nu_)) / (nu_*tmp*tmp * (1 - exp(-2*beta)));
    Real g = sinh1*exp(-0.5*b_*y/nu_) + sinh2*exp(0.5*b_*(1-y)/nu_);
    return c*f*g;
  }

  // Pade accelerated series sum: Cohen-Villegas-Zagier [2000], Algorithm 1
  Real seriesSum( const int& kmax, const Real& x, const Real& y ) const
  {
    Real b, c, d, s, u;
    Real sgn;

    d = pow(3 + sqrt(8), kmax);
    d = 0.5*(d + 1./d);
    b = -1;
    c = -d;
    s =  0;

    for (int k = 0; k < kmax; k++)
    {
      sgn = k % 2 == 0 ? 1 : -1;
      c = b - c;
      u = seriesTerm(k+1, x, y);
      s += c*sgn*u;
      //      if (fabs(c*u) < d*1e-15)
      //        break;
      b = b*(k + kmax)*(k - kmax) / ((k + 0.5)*(k + 1));
    }

    return s/d;
  }

  void seriesTerm( const int& k, const Real& x, const Real& y, Real& u, Real& ux, Real& uy ) const
  {
    Real kpi  = k*PI;
    Real beta = sqrt(kpi*kpi + (a_*a_ + b_*b_)/(4*nu_*nu_));
    Real tmp  = kpi*kpi + a_*a_/(4*nu_*nu_);
    Real sgn  = k % 2 == 0 ? 1 : -1;

    Real c = kpi*(1 - sgn*exp(0.5*a_/nu_)) / (nu_*tmp*tmp * (1 - exp(-2*beta)));

    Real sinh1  = exp(-beta*(2-y)) - exp(-beta*y);
    Real sinh2  = exp(-beta*(1+y)) - exp(-beta*(1-y));
    Real sinh1y =  beta*(exp(-beta*(2-y)) + exp(-beta*y));
    Real sinh2y = -beta*(exp(-beta*(1+y)) + exp(-beta*(1-y)));

    Real f  = sin(kpi*x) * exp(-0.5*a_*x/nu_);
    Real fx = ( kpi*cos(kpi*x) - 0.5*(a_/nu_)*sin(kpi*x) ) * exp(-0.5*a_*x/nu_);

    Real g  = sinh1*exp(-0.5*b_*y/nu_) + sinh2*exp(0.5*b_*(1-y)/nu_);
    Real gy = sinh1y*exp(-0.5*b_*y/nu_) + sinh2y*exp(0.5*b_*(1-y)/nu_) - 0.5*(b_/nu_)*g;

    u  = c*f*g;
    ux = c*fx*g;
    uy = c*f*gy;
  }

  void seriesSum( const int& kmax, const Real& x, const Real& y, Real& u, Real& ux, Real& uy ) const
  {
    Real b, c, d, s, sx, sy;
    Real v, vx, vy;
    Real sgn;

    d = pow(3 + sqrt(8), kmax);
    d = 0.5*(d + 1./d);
    b = -1;
    c = -d;
    s = sx = sy = 0;

    for (int k = 0; k < kmax; k++)
    {
      sgn = k % 2 == 0 ? 1 : -1;
      c = b - c;
      seriesTerm( k+1, x, y, v, vx, vy );
      s  += c*sgn*v;
      sx += c*sgn*vx;
      sy += c*sgn*vy;
      //      if ( (fabs(c*v) < d*1e-15) && (fabs(c*vx) < d*1e-15) && (fabs(c*vy) < d*1e-15) )
      //        break;
      b = b*(k + kmax)*(k - kmax) / ((k + 0.5)*(k + 1));
    }

    u  = s/d;
    ux = sx/d;
    uy = sy/d;
  }

private:
  Real a_, b_;    // advection velocity
  Real nu_;       // diffusion coefficient

  int kmax_;      // maximum number of summations for fourier series
};

//----------------------------------------------------------------------------//
// solution: pde forcing function

template<class PDE>
class ScalarFunction2D_ForcingFunction
{
public:
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  explicit ScalarFunction2D_ForcingFunction( const PDE &pde ) : pde_(pde) {}

  ArrayQ<Real> operator()( const Real& x, const Real& y, const Real& time ) const
  {
    ArrayQ<Real> q = 0;
    pde_.forcingFunction(x, y, time, q);
    return q;
  }

  // void gradient( const VectorXT& X, ArrayQ<Real>& q,
  //                DLA::VectorS< D, ArrayQ<Real> >& gradq ) const
  // {
  //   SANS_DEVELOPER_EXCEPTION("implement. -cfrontin");
  // }

  void gradient( const Real& x, const Real& y, const Real& time, ArrayQ<Real>& q,
                 ArrayQ<Real>& qx, ArrayQ<Real>& qy, ArrayQ<Real>& qt) const
  {
    SANS_DEVELOPER_EXCEPTION("implement. -cfrontin");
  }

  // void secondGradient( const VectorXT& X, ArrayQ<Real>& q,
  //                      DLA::VectorS< D, ArrayQ<Real> >& gradq,
  //                      DLA::MatrixSymS< D, ArrayQ<Real> >& hessianq ) const
  // {
  //   SANS_DEVELOPER_EXCEPTION("implement. -cfrontin");
  // }

  void secondGradient( const Real& x, const Real& y, const Real& time, ArrayQ<Real>& q,
                       ArrayQ<Real>& qx, ArrayQ<Real>& qy, ArrayQ<Real>& qt,
                       ArrayQ<Real>& qxx,
                       ArrayQ<Real>& qyx, ArrayQ<Real>& qyy ) const
  {
    SANS_DEVELOPER_EXCEPTION("implement. -cfrontin");
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_ForcingFunction:" << std::endl;
  }
protected:
  const PDE& pde_;
};

//----------------------------------------------------------------------------//
// solution: c*sin(theta) where theta = atan2(y,x) is the angle in polar coordinates

class ScalarFunction2D_SineTheta : public Function2DVirtualInterface<ScalarFunction2D_SineTheta,ScalarFunctionTraits>
{
public:
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> c{"c", NO_DEFAULT, NO_RANGE, "c c*sin(theta)"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };
  explicit ScalarFunction2D_SineTheta( const PyDict& d ) : c_(d.get(ParamsType::params.c)) {}

  explicit ScalarFunction2D_SineTheta( const Real& c ) : c_(c) {}

  ~ScalarFunction2D_SineTheta() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    return c_ * sin( atan2(y,x) );
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_SineTheta: c_*sin(theta) where c_ = " << c_ << std::endl;
  }
private:
  Real c_;
};

//----------------------------------------------------------------------------//
// solution: geometric series in theta
//           c*(1 + theta +... + theta^p) where theta = atan2(y,x) is the angle in polar coordinates

class ScalarFunction2D_ThetaGeometricSeries : public Function2DVirtualInterface<ScalarFunction2D_ThetaGeometricSeries,ScalarFunctionTraits>
{
public:
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> c{"c", NO_DEFAULT, NO_RANGE, "c c*theta^p"};
    const ParameterNumeric<int> p{"p", NO_DEFAULT, NO_RANGE, "p c*theta^p"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };
  explicit ScalarFunction2D_ThetaGeometricSeries( const PyDict& d ) : c_(d.get(ParamsType::params.c)), p_(d.get(ParamsType::params.p)) {}

  explicit ScalarFunction2D_ThetaGeometricSeries( const Real& c, const int& p ) : c_(c), p_(p) {}

  ~ScalarFunction2D_ThetaGeometricSeries() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    SANS_ASSERT_MSG( p_ >= 0, "p_ must be greater or equal to zero");
    T soln = 0;

    soln = 1;

    for (int i = 1; i <= p_; i++)
    {
      soln += pow( atan2(y,x), i );
    }
    soln = c_ * soln;

    return soln;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_ThetaMonomial: c*_theta^p_ where c_ = " << c_
        << " and p_ = " << p_ << std::endl;
  }
private:
  Real c_;
  int p_;
};


//----------------------------------------------------------------------------//
// solution: -tanh((x-0.5) /(2*nu))
class ScalarFunction2D_Tanh : public Function2DVirtualInterface<ScalarFunction2D_Tanh,ScalarFunctionTraits>
{
public:
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> nu{"nu", NO_DEFAULT, 0, NO_LIMIT, "Viscous coefficient"};
    const ParameterNumeric<Real> A{"A", 2.0, NO_RANGE, "Scale factor"};
    const ParameterNumeric<Real> B{"B", 0.0, NO_RANGE, "U offset"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction2D_Tanh( PyDict& d ) :
            nu_(d.get(ParamsType::params.nu)),
            A_(d.get(ParamsType::params.A)),
            B_(d.get(ParamsType::params.B))
  {
    //Nothing
  }

  explicit ScalarFunction2D_Tanh( const Real& nu, const Real& A, const Real& B ) :
            nu_(nu),
            A_{A},
            B_{B}
  {
    //Nothing
  }
  ~ScalarFunction2D_Tanh() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    if (nu_ != 0)
    {
      return -A_/2.0*tanh((x) / 2.0 / nu_) + B_;
    }
    else
    {
      if (x < 0)
      {
        return A_/2.0*1.0 + B_;
      }
      else
      {
        return -A_/2.0*1.0 + B_;
      }
    }
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_Tanh:  nu_ = " << nu_ << std::endl;
    out << indent << "ScalarFunction2D_Tanh:  A_ = " << A_ << std::endl;
    out << indent << "ScalarFunction2D_Tanh:  B_ = " << B_ << std::endl;
  }

private:
  Real nu_;
  Real A_;
  Real B_;
};


//----------------------------------------------------------------------------//
// solution: tahn2 based on tanh3 from a paper by Adrien Loseille (not sure which...)
//
// This can only be used for L2 projection as the gradient and hessian
// have singularities (i.e. 1/0 stuff)
//
// u = tanh(pow(x + 1.3, 20.0) * pow(y - 0.3, 9.0) )
//

class ScalarFunction2D_Tanh2 : public Function2DBase<Real>
{
public:
  typedef PhysD2 PhysDim;

  template<class T>
  using ArrayQ = T;    // solution/residual arrays

  typedef ParamsNone ParamsType;

  explicit ScalarFunction2D_Tanh2( const PyDict& d ) {}

  ScalarFunction2D_Tanh2() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    return tanh(pow(x + 1.3, 20.0) * pow(y - 0.3, 9.0) );
  }

  virtual ArrayQ<Real> operator()( const Real& x, const Real& y, const Real& time ) const override
  {
    return this->template operator()<Real>(x, y, time);
  }

  virtual void gradient( const Real& x, const Real& y, const Real& time, ArrayQ<Real>& q,
                         ArrayQ<Real>& qx, ArrayQ<Real>& qy, ArrayQ<Real>& qt ) const override
  {
    q = this->operator()(x, y, time);
    SANS_DEVELOPER_EXCEPTION("Gradient can be singular for tanh3");
    qx = qy = qt = 0;
    return;
#if 0
    auto sech = [](Real s) { return 2/(exp(s) + exp(-s)); };

    qx = 20.*pow(1.3 + x,19.)*pow(-0.3 + y,9.)*pow(sech(pow(1.3 + x,20.)*pow(-0.3 + y,9.)),2);
    qy =  9.*pow(1.3 + x,20.)*pow(-0.3 + y,8.)*pow(sech(pow(1.3 + x,20.)*pow(-0.3 + y,9.)),2);

    qt = 0;
#endif
  }

  virtual void secondGradient( const Real& x, const Real& y, const Real& time, ArrayQ<Real>& q,
                               ArrayQ<Real>& qx, ArrayQ<Real>& qy, ArrayQ<Real>& qt,
                               ArrayQ<Real>& qxx,
                               ArrayQ<Real>& qyx, ArrayQ<Real>& qyy ) const override
  {
    this->gradient(x, y, time, q, qx, qy, qt);
    SANS_DEVELOPER_EXCEPTION("Gradient can be singular for tanh3");
    qxx = qyx = qyy = 0;
    return;
#if 0
    auto sech = [](Real s) { return 2/(exp(s) + exp(-s)); };

    qxx = 380.*pow(1.3 + x,18.)*pow(-0.3 + y,9.)*
        pow(sech(pow(1.3 + x,20.)*pow(-0.3 + y,9.)),2) -
       800.*pow(1.3 + x,38.)*pow(-0.3 + y,18.)*pow(z,2)*
        pow(sech(pow(1.3 + x,20.)*pow(-0.3 + y,9.)),2)*
        tanh(pow(1.3 + x,20.)*pow(-0.3 + y,9.));

    qyx = 180.*pow(1.3 + x,19.)*pow(-0.3 + y,8.)*
        pow(sech(pow(1.3 + x,20.)*pow(-0.3 + y,9.)),2) -
       360.*pow(1.3 + x,39.)*pow(-0.3 + y,17.)*pow(z,2)*
        pow(sech(pow(1.3 + x,20.)*pow(-0.3 + y,9.)),2)*
        tanh(pow(1.3 + x,20.)*pow(-0.3 + y,9.));

    qyy = 72.*pow(1.3 + x,20.)*pow(-0.3 + y,7.)*
        pow(sech(pow(1.3 + x,20.)*pow(-0.3 + y,9.)),2) -
       162.*pow(1.3 + x,40.)*pow(-0.3 + y,16.)*pow(z,2)*
        pow(sech(pow(1.3 + x,20.)*pow(-0.3 + y,9.)),2)*
        tanh(pow(1.3 + x,20.)*pow(-0.3 + y,9.));
#endif
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const override
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_Tanh2: " << std::endl;
  }

private:
};

//----------------------------------------------------------------------------//
// solution: -tanh((x-0.5) /(2*nu))
class ScalarFunction2D_PiecewiseLinear : public Function2DVirtualInterface<ScalarFunction2D_PiecewiseLinear,ScalarFunctionTraits>
{
public:
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  typedef ParamsNone ParamsType;

  explicit ScalarFunction2D_PiecewiseLinear( PyDict& d )
  {
    //Nothing
  }

  explicit ScalarFunction2D_PiecewiseLinear()
  {
    //Nothing
  }
  ~ScalarFunction2D_PiecewiseLinear() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
#if 0
    if (y < 1.0)
    {
      T xhead = y / 2.0;
      T xfoot = y + 1.0;
      if (x < xhead)
      {
        return 2.0;
      }
      else if (x < xfoot)
      {
        return (2.0 - (xfoot-x)) / (1.0 - xhead);
      }
      else
      {
        return 1.0;
      }
    }
    else
    {
      T xs = 2.0/3.0 * (y + 2.0);
      if (x < xs)
      {
        return 2.0;
      }
      else
      {
        return 1.0;
      }
    }
#endif

    if (x < 0.0)
    {
      return 2.0;
    }
    else if (x < 1.0)
    {
      return (2.0 - x);
    }
    else
    {
      return 1.0;
    }
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_PiecewiseLinear: " << std::endl;
  }

};

//----------------------------------------------------------------------------//
// solution: BoxCornerInterpolant
// Linearly interpolates the values specified at the 4 corners of a box

class ScalarFunction2D_BoxCornerInterpolant : public Function2DVirtualInterface<ScalarFunction2D_BoxCornerInterpolant,ScalarFunctionTraits>
{
public:
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  //Unsteady case
  ScalarFunction2D_BoxCornerInterpolant( const Real& x0, const Real& x1,
                                         const Real& y0, const Real& y1,
                                         const Real& t0, const Real& t1,
                                         const Real& val000, const Real& val100, const Real& val110, const Real& val010,
                                         const Real& val001, const Real& val101, const Real& val111, const Real& val011 ) :
                                           x0_(x0), x1_(x1), y0_(y0), y1_(y1), t0_(t0), t1_(t1),
                                           val000_(val000), val100_(val100), val110_(val110), val010_(val010),
                                           val001_(val001), val101_(val101), val111_(val111), val011_(val011) {}

  //Steady case
  ScalarFunction2D_BoxCornerInterpolant( const Real& x0, const Real& x1,
                                         const Real& y0, const Real& y1,
                                         const Real& val00, const Real& val10, const Real& val11, const Real& val01) :
                                           ScalarFunction2D_BoxCornerInterpolant( x0, x1, y0, y1, 0.0, 1.0,
                                                                                  val00, val10, val11, val01,
                                                                                  val00, val10, val11, val01 ) {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& t ) const
  {
    T xf = (x - x0_)/(x1_ - x0_);
    T yf = (y - y0_)/(y1_ - y0_);
    T tf = (t - t0_)/(t1_ - t0_);

    //Interpolate in x
    ArrayQ<T> valx00 = val000_*(1.0 - xf) + val100_*xf;
    ArrayQ<T> valx10 = val010_*(1.0 - xf) + val110_*xf;
    ArrayQ<T> valx01 = val001_*(1.0 - xf) + val101_*xf;
    ArrayQ<T> valx11 = val011_*(1.0 - xf) + val111_*xf;

    //Interpolate in y
    ArrayQ<T> valxy0 = valx00*(1.0 - yf) + valx10*yf;
    ArrayQ<T> valxy1 = valx01*(1.0 - yf) + valx11*yf;

    //Interpolate in t
    ArrayQ<T> valxyz = valxy0*(1.0 - tf) + valxy1*tf;

    return valxyz;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_BoxCornerInterpolant: " << std::endl;
    out << indent << "  x0_, x1_, y0_, y1_, t0_, t1_ = " << x0_ << ", " << x1_ << ", "
        << y0_ << ", " << y1_ << ", "
        << t0_ << ", " << t1_ << std::endl;
    out << indent << "  val000_, val100_, val110_, val010_ = " << val000_ << ", " << val100_ << ", " << val110_ << ", " << val010_ << std::endl;
    out << indent << "  val001_, val101_, val111_, val011_ = " << val001_ << ", " << val101_ << ", " << val111_ << ", " << val011_ << std::endl;
  }
private:
  Real x0_, x1_, y0_, y1_, t0_, t1_;  // limits of the box domain, start and end times
  Real val000_, val100_, val110_, val010_; //values at the corners at time t0: val110 is at xmax, ymax, zmin
  Real val001_, val101_, val111_, val011_; //values at the corners at time t1
};


//----------------------------------------------------------------------------//
// A finite width rectangular pulse along the x-axis
//----------------------------------------------------------------------------//
class ScalarFunction2D_Pulse : public Function2DVirtualInterface<ScalarFunction2D_Pulse,ScalarFunctionTraits>
{
public:
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> h{"h", NO_DEFAULT, 0, NO_LIMIT, "Height of the pulse"};
    const ParameterNumeric<Real> x0{"x0", 2.0, NO_RANGE, "Centre of the pulse"};
    const ParameterNumeric<Real> w{"w", 0.0, NO_RANGE, "Width of the pulse"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction2D_Pulse( PyDict& d ) :
            h_(d.get(ParamsType::params.h)),
            x0_(d.get(ParamsType::params.x0)),
            w_(d.get(ParamsType::params.w))
  {
    //Nothing
  }

  explicit ScalarFunction2D_Pulse( const Real& h, const Real& x0, const Real& w ) :
            h_(h),
            x0_(x0),
            w_(w)
  {
    //Nothing
  }
  ~ScalarFunction2D_Pulse() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    if (x < (x0_ - w_/2.0))
    {
      return 0.0;
    }
    else if (x > (x0_ + w_/2.0))
    {
      return 0.0;
    }
    else
    {
      return h_;
    }

    SANS_DEVELOPER_EXCEPTION("ScalarFunction2D_Pulse::operator(): Undefined state");
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction2D_Pulse:  h_ = " << h_ << std::endl;
    out << indent << "ScalarFunction2D_Pulse:  x0_ = " << x0_ << std::endl;
    out << indent << "ScalarFunction2D_Pulse:  w_ = " << w_ << std::endl;
  }

private:
  Real h_;
  Real x0_;
  Real w_;
};

//----------------------------------------------------------------------------//
// Struct for creating a scalar functions
//
struct ScalarFunction2DParams : noncopyable
{
  typedef std::shared_ptr<Function2DBase<Real>> Function_ptr;

  struct FunctionOptions
  {
    typedef DictKeyPair ExtractType;
    const ParameterString Name{"Name", NO_DEFAULT, "Function name" };
    const ParameterString& key = Name;

    const DictOption Const{"Const", ScalarFunction2D_Const::ParamsType::checkInputs};
    const DictOption Linear{"Linear", ScalarFunction2D_Linear::ParamsType::checkInputs};
    const DictOption PiecewiseConstBlock{"PiecewiseConstBlock", ScalarFunction2D_PiecewiseConstBlock::ParamsType::checkInputs};
    const DictOption PiecewiseConstDiag{"PiecewiseConstDiag", ScalarFunction2D_PiecewiseConstDiag::ParamsType::checkInputs};
    const DictOption Monomial{"Monomial", ScalarFunction2D_Monomial::ParamsType::checkInputs};
    const DictOption Quadratic{"Quadratic", ScalarFunction2D_Quadratic::ParamsType::checkInputs};
    const DictOption Pringle{"Pringle", ScalarFunction2D_Pringle::ParamsType::checkInputs};
    const DictOption ASExp{"ASExp", ScalarFunction2D_ASExp::ParamsType::checkInputs};
    const DictOption ASExpCombo{"ASExpCombo", ScalarFunction2D_ASExpCombo::ParamsType::checkInputs};
    const DictOption VarExp{"VarExp", ScalarFunction2D_VarExp::ParamsType::checkInputs};
    const DictOption VarExp2{"VarExp2", ScalarFunction2D_VarExp2::ParamsType::checkInputs};
    const DictOption VarExp3{"VarExp3", ScalarFunction2D_VarExp3::ParamsType::checkInputs};
    const DictOption SineSine{"SineSine", ScalarFunction2D_SineSine::ParamsType::checkInputs};
    const DictOption SineCosineDecay{"SineCosineDecay", ScalarFunction2D_SineCosineDecay::ParamsType::checkInputs};
    const DictOption CircularWaveDecay{ "CircularWaveDecay", ScalarFunction2D_CircularWaveDecay::ParamsType::checkInputs};
    const DictOption DoubleBL{"DoubleBL", ScalarFunction2D_DoubleBL::ParamsType::checkInputs};
    const DictOption Vortex{"Vortex", ScalarFunction2D_Vortex::ParamsType::checkInputs};
    const DictOption SineTheta{"SineTheta", ScalarFunction2D_SineTheta::ParamsType::checkInputs};
    const DictOption CornerSingularity{"CornerSingularity", ScalarFunction2D_CornerSingularity::ParamsType::checkInputs};
    const DictOption BoundaryLayer{"BoundaryLayer", ScalarFunction2D_BoundaryLayer::ParamsType::checkInputs};
    const DictOption ThetaGeometricSeries{"ThetaGeometricSeries", ScalarFunction2D_ThetaGeometricSeries::ParamsType::checkInputs};
    const DictOption AdjointLaplacianAverage{"AdjointLaplacianAverage", ScalarFunction2D_LaplacianUnitForcing::ParamsType::checkInputs};
    const DictOption Tanh{"Tanh", ScalarFunction2D_Tanh::ParamsType::checkInputs};
    const DictOption PiecewiseLinear{"PiecewiseLinear", ScalarFunction2D_PiecewiseLinear::ParamsType::checkInputs};
    const DictOption Pulse{"Pulse", ScalarFunction2D_Pulse::ParamsType::checkInputs};
    const DictOption Gaussian{"Gaussian", ScalarFunction2D_Gaussian::ParamsType::checkInputs};
    const DictOption GaussianCosine{"GaussianCosine", ScalarFunction2D_GaussianCosine::ParamsType::checkInputs};
    const DictOption Ojeda{"Ojeda", ScalarFunction2D_Ojeda::ParamsType::checkInputs};
    const DictOption Tanh2{"Tanh2", ScalarFunction2D_Tanh2::ParamsType::checkInputs};

    const std::vector<DictOption> options
    { Const, Linear, PiecewiseConstBlock, PiecewiseConstDiag, Monomial, Quadratic, Pringle, ASExp, ASExpCombo,
      VarExp, VarExp2, VarExp3, SineSine, SineCosineDecay, CircularWaveDecay, DoubleBL, Vortex, SineTheta,
      CornerSingularity, BoundaryLayer, ThetaGeometricSeries,
      AdjointLaplacianAverage, Tanh, Tanh2, PiecewiseLinear, Pulse, Gaussian, GaussianCosine, Ojeda
    };
  };
  const ParameterOption<FunctionOptions> Function{"Function", NO_DEFAULT, "The 2D scalar function"};

  static Function_ptr getFunction(const PyDict& d);
};

} // namespace SANS

#endif  // SCALARFUNCTION2D_H
