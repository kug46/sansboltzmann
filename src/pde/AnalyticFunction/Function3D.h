// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FUNCTION3D_H
#define FUNCTION3D_H

#include "Surreal/SurrealS.h"

#include "Function.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"

namespace SANS
{

template <class ArrayQ_>
class Function3DBase : public Function<PhysD3>
{
public:
  typedef ArrayQ_ ArrayQ;

  virtual ArrayQ operator()( const Real& x, const Real& y, const Real& z, const Real& time ) const = 0;

  virtual void gradient( const Real& x, const Real& y, const Real& z, const Real& time, ArrayQ& q,
                         ArrayQ& qx, ArrayQ& qy, ArrayQ& qz, ArrayQ& qt ) const = 0;

  virtual void secondGradient( const Real& x, const Real& y, const Real& z, const Real& time, ArrayQ& q,
                               ArrayQ& qx, ArrayQ& qy, ArrayQ& qz, ArrayQ& qt,
                               ArrayQ& qxx,
                               ArrayQ& qyx, ArrayQ& qyy,
                               ArrayQ& qzx, ArrayQ& qzy, ArrayQ& qzz ) const = 0;

  virtual void dump( int indentSize=0, std::ostream& out = std::cout ) const = 0;

  virtual ~Function3DBase() {}
};

template<class Derived, class Traits>
class Function3DVirtualInterface : public Function3DBase<typename Traits::template ArrayQ<Real>>
{
public:
  template <class Z>
  using ArrayQ = typename Traits::template ArrayQ<Z>;

  template <class Z>
  using MatrixQ = typename Traits::template MatrixQ<Z>;

  virtual ArrayQ<Real> operator()( const Real& x, const Real& y, const Real& z, const Real& time ) const override
  {
    return reinterpret_cast<const Derived*>(this)->operator()(x,y,z,time);
  }

  virtual void gradient( const Real& x, const Real& y, const Real& z, const Real& time, ArrayQ<Real>& q,
                               ArrayQ<Real>& qx, ArrayQ<Real>& qy, ArrayQ<Real>& qz, ArrayQ<Real>& qt ) const override
  {
    SurrealS<4> xs = x, ys = y, zs = z, ts = time;

    xs.deriv(0) = 1;
    ys.deriv(1) = 1;
    zs.deriv(2) = 1;
    ts.deriv(3) = 1;

    ArrayQ< SurrealS<4> > qderiv = 0;

    qderiv = reinterpret_cast<const Derived*>(this)->operator()(xs, ys, zs, ts);

    // Extract all derivatives of the exact solution
    for (int n = 0; n < DLA::VectorSize< ArrayQ<Real> >::M; n++)
    {
      DLA::index(q,n) = DLA::index(qderiv,n).value();

      DLA::index(qx,n) = DLA::index(qderiv,n).deriv(0);
      DLA::index(qy,n) = DLA::index(qderiv,n).deriv(1);
      DLA::index(qz,n) = DLA::index(qderiv,n).deriv(2);
      DLA::index(qt,n) = DLA::index(qderiv,n).deriv(3);
    }
  }

  virtual void secondGradient( const Real& x, const Real& y, const Real& z, const Real& time, ArrayQ<Real>& q,
                               ArrayQ<Real>& qx, ArrayQ<Real>& qy, ArrayQ<Real>& qz, ArrayQ<Real>& qt,
                               ArrayQ<Real>& qxx,
                               ArrayQ<Real>& qyx, ArrayQ<Real>& qyy,
                               ArrayQ<Real>& qzx, ArrayQ<Real>& qzy, ArrayQ<Real>& qzz ) const override
  {
    SurrealS< 4, SurrealS<4> > xs = x, ys = y, zs = z, ts = time;

    xs.deriv(0) = 1;
    ys.deriv(1) = 1;
    zs.deriv(2) = 1;
    ts.deriv(3) = 1;

    xs.value().deriv(0) = 1;
    ys.value().deriv(1) = 1;
    zs.value().deriv(2) = 1;
    ts.value().deriv(3) = 1;

    ArrayQ< SurrealS< 4, SurrealS<4> > > qderiv = 0;

    qderiv = reinterpret_cast<const Derived*>(this)->operator()(xs, ys, zs, ts);

    // Extract all derivatives of the exact solution
    for (int n = 0; n < DLA::VectorSize< ArrayQ<Real> >::M; n++)
    {
      DLA::index(q,n) = DLA::index(qderiv,n).value().value();

      DLA::index(qx,n) = DLA::index(qderiv,n).deriv(0).value();
      DLA::index(qy,n) = DLA::index(qderiv,n).deriv(1).value();
      DLA::index(qz,n) = DLA::index(qderiv,n).deriv(2).value();
      DLA::index(qt,n) = DLA::index(qderiv,n).deriv(3).value();

      DLA::index(qxx,n) = DLA::index(qderiv,n).deriv(0).deriv(0);

      DLA::index(qyx,n) = DLA::index(qderiv,n).deriv(1).deriv(0);
      DLA::index(qyy,n) = DLA::index(qderiv,n).deriv(1).deriv(1);

      DLA::index(qzx,n) = DLA::index(qderiv,n).deriv(2).deriv(0);
      DLA::index(qzy,n) = DLA::index(qderiv,n).deriv(2).deriv(1);
      DLA::index(qzz,n) = DLA::index(qderiv,n).deriv(2).deriv(2);
    }
  }

  virtual void dump( int indentSize=0, std::ostream& out = std::cout ) const override
  {
    reinterpret_cast<const Derived*>(this)->dump(indentSize, out);
  }

  virtual ~Function3DVirtualInterface() {}
};

} // namespace SANS

#endif  // FUNCTION3D_H
