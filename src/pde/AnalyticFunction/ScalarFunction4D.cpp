// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "ScalarFunction4D.h"

#define PYDICT_INSTANTIATE
#include "Python/PyDict_impl.h"

namespace SANS
{

template ParameterOption<ScalarFunction4DParams::FunctionOptions>::ExtractType
PyDict::get(const ParameterType<ParameterOption<ScalarFunction4DParams::FunctionOptions>>&) const;


//===========================================================================//
ScalarFunction4DParams::Function_ptr ScalarFunction4DParams::getFunction(const PyDict& d)
{
  ScalarFunction4DParams params;

  DictKeyPair FunctionParam = d.get( params.Function );

  if (FunctionParam == params.Function.SinFun4)
    return Function_ptr( new ScalarFunction4D_SinFun4( FunctionParam ) );
  else if (FunctionParam == params.Function.SineSineSineSineUnsteady)
    return Function_ptr( new ScalarFunction4D_SineSineSineSineUnsteady( FunctionParam ) );

  // Should never get here if checkInputs was called
  SANS_DEVELOPER_EXCEPTION( "Unrecognized function specified" );
}

}
