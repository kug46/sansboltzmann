// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SCALARFUNCTIONTRAITS_H
#define SCALARFUNCTIONTRAITS_H

namespace SANS
{

struct ScalarFunctionTraits
{
  template <class T>
  using ArrayQ = T;     // solution arrays

  template <class T>
  using MatrixQ = T;    // solution matrices
};

} // namespace SANS

#endif  // SCALARFUNCTIONTRAITS_H
