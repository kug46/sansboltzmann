// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FUNCTION2D_H
#define FUNCTION2D_H

#include "Surreal/SurrealS.h"

#include "Function.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"

namespace SANS
{

template <class ArrayQ_>
class Function2DBase : public Function<PhysD2>
{
public:
  typedef ArrayQ_ ArrayQ;

  virtual ArrayQ operator()( const Real& x, const Real& y, const Real& time ) const = 0;

  virtual void gradient( const Real& x, const Real& y, const Real& time, ArrayQ& q,
                         ArrayQ& qx, ArrayQ& qy, ArrayQ& qt ) const = 0;

  virtual void secondGradient( const Real& x, const Real& y, const Real& time, ArrayQ& q,
                               ArrayQ& qx, ArrayQ& qy, ArrayQ& qt,
                               ArrayQ& qxx,
                               ArrayQ& qyx, ArrayQ& qyy ) const = 0;

  virtual void dump( int indentSize=0, std::ostream& out = std::cout ) const = 0;

  virtual ~Function2DBase() {}
};

template<class Derived, class Traits>
class Function2DVirtualInterface : public Function2DBase<typename Traits::template ArrayQ<Real>>
{
public:
  template <class Z>
  using ArrayQ = typename Traits::template ArrayQ<Z>;

  template <class Z>
  using MatrixQ = typename Traits::template MatrixQ<Z>;

  virtual ArrayQ<Real> operator()( const Real& x, const Real& y, const Real& time ) const override
  {
    return reinterpret_cast<const Derived*>(this)->operator()(x,y,time);
  }

  virtual void gradient( const Real& x, const Real& y, const Real& time, ArrayQ<Real>& q,
                               ArrayQ<Real>& qx, ArrayQ<Real>& qy, ArrayQ<Real>& qt ) const override
  {
    SurrealS<3> xs = x, ys = y, ts = time;

    xs.deriv(0) = 1;
    ys.deriv(1) = 1;
    ts.deriv(2) = 1;

    ArrayQ< SurrealS<3> > qderiv = 0;

    qderiv = reinterpret_cast<const Derived*>(this)->operator()(xs, ys, ts);

    // Extract all derivatives of the exact solution
    for (int n = 0; n < DLA::VectorSize< ArrayQ<Real> >::M; n++)
    {
      DLA::index(q,n) = DLA::index(qderiv,n).value();

      DLA::index(qx,n) = DLA::index(qderiv,n).deriv(0);
      DLA::index(qy,n) = DLA::index(qderiv,n).deriv(1);
      DLA::index(qt,n) = DLA::index(qderiv,n).deriv(2);
    }
  }

  virtual void secondGradient( const Real& x, const Real& y, const Real& time, ArrayQ<Real>& q,
                               ArrayQ<Real>& qx, ArrayQ<Real>& qy, ArrayQ<Real>& qt,
                               ArrayQ<Real>& qxx,
                               ArrayQ<Real>& qyx, ArrayQ<Real>& qyy ) const override
  {
    SurrealS< 3, SurrealS<3> > xs = x, ys = y, ts = time;

    xs.deriv(0) = 1;
    ys.deriv(1) = 1;
    ts.deriv(2) = 1;

    xs.value().deriv(0) = 1;
    ys.value().deriv(1) = 1;
    ts.value().deriv(2) = 1;

    ArrayQ< SurrealS< 3, SurrealS<3> > > qderiv = 0;

    qderiv = reinterpret_cast<const Derived*>(this)->operator()(xs, ys, ts);

    // Extract all derivatives of the exact solution
    for (int n = 0; n < DLA::VectorSize< ArrayQ<Real> >::M; n++)
    {
      DLA::index(q,n) = DLA::index(qderiv,n).value().value();

      DLA::index(qx,n) = DLA::index(qderiv,n).deriv(0).value();
      DLA::index(qy,n) = DLA::index(qderiv,n).deriv(1).value();
      DLA::index(qt,n) = DLA::index(qderiv,n).deriv(2).value();

      DLA::index(qxx,n) = DLA::index(qderiv,n).deriv(0).deriv(0);
      DLA::index(qyx,n) = DLA::index(qderiv,n).deriv(1).deriv(0);
      DLA::index(qyy,n) = DLA::index(qderiv,n).deriv(1).deriv(1);
    }
  }

  virtual void dump( int indentSize=0, std::ostream& out = std::cout ) const override
  {
    reinterpret_cast<const Derived*>(this)->dump(indentSize, out);
  }

  virtual ~Function2DVirtualInterface() {}
};

} // namespace SANS

#endif  // FUNCTION2D_H
