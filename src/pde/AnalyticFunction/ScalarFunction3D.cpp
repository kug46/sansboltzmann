// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "ScalarFunction3D.h"

#define PYDICT_INSTANTIATE
#include "Python/PyDict_impl.h"

namespace SANS
{

template ParameterOption<ScalarFunction3DParams::FunctionOptions>::ExtractType
PyDict::get(const ParameterType<ParameterOption<ScalarFunction3DParams::FunctionOptions>>&) const;

// cppcheck-suppress passedByValue
void ScalarFunction3D_LaplaceSphere::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back( d.checkInputs( params.thetamin ) );
  allParams.push_back( d.checkInputs( params.thetamax ) );
  d.checkUnknownInputs( allParams );
}
ScalarFunction3D_LaplaceSphere::ParamsType ScalarFunction3D_LaplaceSphere::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction3D_ASExp::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back( d.checkInputs( params.u0) );
  allParams.push_back( d.checkInputs( params.x0) );
  allParams.push_back( d.checkInputs( params.y0) );
  allParams.push_back( d.checkInputs( params.z0) );
  allParams.push_back( d.checkInputs( params.a ) );
  allParams.push_back( d.checkInputs( params.b ) );
  allParams.push_back( d.checkInputs( params.c ) );
  allParams.push_back( d.checkInputs( params.alpha ) );
  d.checkUnknownInputs( allParams );
}
ScalarFunction3D_ASExp::ParamsType ScalarFunction3D_ASExp::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction3D_BoundaryLayer::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back( d.checkInputs( params.epsilon ));
  allParams.push_back( d.checkInputs( params.beta ));
  allParams.push_back( d.checkInputs( params.p ));
  d.checkUnknownInputs( allParams );
}
ScalarFunction3D_BoundaryLayer::ParamsType ScalarFunction3D_BoundaryLayer::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction3D_BoundaryLayerSingularPerturbation::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back( d.checkInputs( params.epsilon ));
  allParams.push_back( d.checkInputs( params.betay ));
  allParams.push_back( d.checkInputs( params.betaz ));
  allParams.push_back( d.checkInputs( params.betat ));
  allParams.push_back( d.checkInputs( params.p ));
  d.checkUnknownInputs( allParams );
}
ScalarFunction3D_BoundaryLayerSingularPerturbation::ParamsType ScalarFunction3D_BoundaryLayerSingularPerturbation::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction3D_TripleBL::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back( d.checkInputs( params.a ) );
  allParams.push_back( d.checkInputs( params.b ) );
  allParams.push_back( d.checkInputs( params.c ) );
  allParams.push_back( d.checkInputs( params.nu ) );
  allParams.push_back( d.checkInputs( params.offset ) );
  allParams.push_back( d.checkInputs( params.scale ) );
  allParams.push_back( d.checkInputs( params.temporal ) );
  d.checkUnknownInputs( allParams );
}
ScalarFunction3D_TripleBL::ParamsType ScalarFunction3D_TripleBL::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction3D_TripleBLsine::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.a));
  allParams.push_back(d.checkInputs(params.b));
  allParams.push_back(d.checkInputs(params.c));
  allParams.push_back(d.checkInputs(params.nu));
  allParams.push_back(d.checkInputs(params.omega));
  allParams.push_back(d.checkInputs(params.offset));
  allParams.push_back(d.checkInputs(params.scale));
  d.checkUnknownInputs(allParams);
}
ScalarFunction3D_TripleBLsine::ParamsType ScalarFunction3D_TripleBLsine::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction3D_CornerSingularity::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back( d.checkInputs( params.alpha ) );
  allParams.push_back( d.checkInputs( params.theta0 ) );
  allParams.push_back( d.checkInputs( params.theta1 ) );
  allParams.push_back( d.checkInputs( params.theta2 ) );
  d.checkUnknownInputs( allParams );
}
ScalarFunction3D_CornerSingularity::ParamsType ScalarFunction3D_CornerSingularity::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction3D_Monomial::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back( d.checkInputs( params.i ) );
  allParams.push_back( d.checkInputs( params.j ) );
  allParams.push_back( d.checkInputs( params.k ) );
  d.checkUnknownInputs( allParams );
}
ScalarFunction3D_Monomial::ParamsType ScalarFunction3D_Monomial::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction3D_SineSineSineDecay::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back( d.checkInputs( params.lambda ) );
  d.checkUnknownInputs( allParams );
}
ScalarFunction3D_SineSineSineDecay::ParamsType ScalarFunction3D_SineSineSineDecay::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction3D_OscillatingGaussian::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back( d.checkInputs( params.sigma ) );
  allParams.push_back( d.checkInputs( params.x0 ) );
  allParams.push_back( d.checkInputs( params.y0 ) );
  allParams.push_back( d.checkInputs( params.z0 ) );
  allParams.push_back( d.checkInputs( params.tMin ) );
  allParams.push_back( d.checkInputs( params.tMax ) );
  allParams.push_back( d.checkInputs( params.f ) );
  d.checkUnknownInputs( allParams );
}
ScalarFunction3D_OscillatingGaussian::ParamsType ScalarFunction3D_OscillatingGaussian::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction3D_AdvectionDiffusionDecayingCosine::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back( d.checkInputs( params.nu ) );
  allParams.push_back( d.checkInputs( params.a_x ) );
  allParams.push_back( d.checkInputs( params.a_y ) );
  allParams.push_back( d.checkInputs( params.a_z ) );
  allParams.push_back( d.checkInputs( params.omega_x ) );
  allParams.push_back( d.checkInputs( params.omega_y ) );
  allParams.push_back( d.checkInputs( params.omega_z ) );
  d.checkUnknownInputs( allParams );
}
ScalarFunction3D_AdvectionDiffusionDecayingCosine::ParamsType
    ScalarFunction3D_AdvectionDiffusionDecayingCosine::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction3D_Gaussian::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back( d.checkInputs( params.sigma ) );
  allParams.push_back( d.checkInputs( params.x0 ) );
  allParams.push_back( d.checkInputs( params.y0 ) );
  allParams.push_back( d.checkInputs( params.z0 ) );
  d.checkUnknownInputs( allParams );
}
ScalarFunction3D_Gaussian::ParamsType ScalarFunction3D_Gaussian::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction3D_SineSineSine::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.a));
  allParams.push_back(d.checkInputs(params.b));
  allParams.push_back(d.checkInputs(params.c));
  allParams.push_back(d.checkInputs(params.C));
  allParams.push_back(d.checkInputs(params.A));
  d.checkUnknownInputs(allParams);
}
ScalarFunction3D_SineSineSine::ParamsType ScalarFunction3D_SineSineSine::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction3D_SphericalWaveDecay::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.alpha));
  allParams.push_back(d.checkInputs(params.k0));
  allParams.push_back(d.checkInputs(params.k1));
  allParams.push_back(d.checkInputs(params.velocity));
  allParams.push_back(d.checkInputs(params.radius0));

  d.checkUnknownInputs(allParams);
}
ScalarFunction3D_SphericalWaveDecay::ParamsType ScalarFunction3D_SphericalWaveDecay::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction3D_AdvectionDiffusionSquareChannel::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back( d.checkInputs( params.a ));
  allParams.push_back( d.checkInputs( params.b ));
  allParams.push_back( d.checkInputs( params.c ));
  allParams.push_back( d.checkInputs( params.lambda_z ));
  allParams.push_back( d.checkInputs( params.lambda_t ));
  d.checkUnknownInputs( allParams );
}
ScalarFunction3D_AdvectionDiffusionSquareChannel::ParamsType
    ScalarFunction3D_AdvectionDiffusionSquareChannel::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction3D_AdvectionDiffusionFourierDecay::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back( d.checkInputs( params.ax ));
  allParams.push_back( d.checkInputs( params.ay ));
  allParams.push_back( d.checkInputs( params.az ));
  allParams.push_back( d.checkInputs( params.nu ));
  allParams.push_back( d.checkInputs( params.c0 ));
  allParams.push_back( d.checkInputs( params.c1 ));
  // allParams.push_back( d.checkInputs( params.c_fourier ));
  // allParams.push_back( d.checkInputs( params.n_fourier ));
  // allParams.push_back( d.checkInputs( params.p_fourier ));
  // allParams.push_back( d.checkInputs( params.q_fourier ));
  d.checkUnknownInputs( allParams );
}
ScalarFunction3D_AdvectionDiffusionFourierDecay::ParamsType
    ScalarFunction3D_AdvectionDiffusionFourierDecay::ParamsType::params;

//===========================================================================//
ScalarFunction3DParams::Function_ptr ScalarFunction3DParams::getFunction(const PyDict& d)
{
  ScalarFunction3DParams params;

  DictKeyPair FunctionParam = d.get( params.Function );

  if (FunctionParam == params.Function.Exp)
    return Function_ptr( new ScalarFunction3D_ASExp( FunctionParam ) );
  else if (FunctionParam == params.Function.LaplaceSphere)
    return Function_ptr( new ScalarFunction3D_LaplaceSphere( FunctionParam ) );
  else if (FunctionParam == params.Function.BoundaryLayer)
    return Function_ptr( new ScalarFunction3D_BoundaryLayer( FunctionParam ) );
  else if (FunctionParam == params.Function.BoundaryLayerSingularPerturbation)
    return Function_ptr( new ScalarFunction3D_BoundaryLayerSingularPerturbation( FunctionParam ) );
  else if (FunctionParam == params.Function.TripleBL)
    return Function_ptr( new ScalarFunction3D_TripleBL( FunctionParam ) );
  else if (FunctionParam == params.Function.TripleBLsine)
    return Function_ptr( new ScalarFunction3D_TripleBLsine( FunctionParam ) );
  else if (FunctionParam == params.Function.CornerSingularity)
    return Function_ptr( new ScalarFunction3D_CornerSingularity( FunctionParam ) );
  else if (FunctionParam == params.Function.Gaussian)
    return Function_ptr( new ScalarFunction3D_Gaussian( FunctionParam ) );
  else if (FunctionParam == params.Function.SineSineSine)
    return Function_ptr( new ScalarFunction3D_SineSineSine( FunctionParam ) );
  else if (FunctionParam == params.Function.SineSineSineSineUnsteady)
      return Function_ptr( new ScalarFunction3D_SineSineSineSineUnsteady( FunctionParam ) );
  else if (FunctionParam == params.Function.SineSineSineDecay)
      return Function_ptr( new ScalarFunction3D_SineSineSineDecay( FunctionParam ) );
  else if (FunctionParam == params.Function.AdvectionDiffusionDecayingCosine)
      return Function_ptr( new ScalarFunction3D_AdvectionDiffusionDecayingCosine( FunctionParam ) );
  else if (FunctionParam == params.Function.Monomial)
      return Function_ptr( new ScalarFunction3D_Monomial( FunctionParam ) );
  else if (FunctionParam == params.Function.Tanh3)
    return Function_ptr( new ScalarFunction3D_Tanh3( FunctionParam ) );
  else if (FunctionParam == params.Function.SinATan3)
    return Function_ptr( new ScalarFunction3D_SinATan3( FunctionParam ) );
  else if (FunctionParam == params.Function.SinFun3)
    return Function_ptr( new ScalarFunction3D_SinFun3( FunctionParam ) );
  else if (FunctionParam == params.Function.SphericalWaveDecay)
    return Function_ptr( new ScalarFunction3D_SinFun3( FunctionParam ) );
else if (FunctionParam == params.Function.OscillatingGaussian)
    return Function_ptr( new ScalarFunction3D_OscillatingGaussian( FunctionParam ) );
else if (FunctionParam == params.Function.AdvectionDiffusionSquareChannel)
    return Function_ptr( new ScalarFunction3D_AdvectionDiffusionSquareChannel( FunctionParam ) );
  else if (FunctionParam == params.Function.AdvectionDiffusionFourierDecay)
      return Function_ptr( new ScalarFunction3D_AdvectionDiffusionFourierDecay( FunctionParam ) );

  // Should never get here if checkInputs was called
  SANS_DEVELOPER_EXCEPTION( "Unrecognized function specified" );
  return nullptr;
}

}
