// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef VECTORFUNCTION2D_H
#define VECTORFUNCTION2D_H

// Analytic solutions for systems of equations

// Python must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"

#include "Function2D.h"
#include "VectorFunctionTraits.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// solution class for systems with two variables

template<class SOL1, class SOL2>
class Vector2Function2D : public Function2DVirtualInterface<Vector2Function2D<SOL1, SOL2>,Vector2FunctionTraits>
{
public:
  typedef PhysD2 PhysDim;

  static_assert( std::is_same<PhysDim, typename SOL1::PhysDim>::value, "Physical dimensions should match" );
  static_assert( std::is_same<PhysDim, typename SOL2::PhysDim>::value, "Physical dimensions should match" );

  template <class T>
  using ArrayQ = Vector2FunctionTraits::template ArrayQ<T>; // solution array

  template <class T>
  using MatrixQ = Vector2FunctionTraits::template MatrixQ<T>; // solution array

  explicit Vector2Function2D( const SOL1& sol1, const SOL2& sol2 ) : sol1_(sol1), sol2_(sol2) {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    ArrayQ<T> val;
    val[0] = sol1_(x, y, time);
    val[1] = sol2_(x, y, time);
    return val;
  }

private:
  const SOL1& sol1_;
  const SOL2& sol2_;
};

// Vector function: constant-valued
template<size_t m>
class VectorFunction2D_const : public Function2DVirtualInterface<VectorFunction2D_const<m>,VectorFunctionTraits<m> >
{
public:
  typedef PhysD2 PhysDim;
  typedef VectorFunctionTraits<m> TraitsType;

  template <class T>
  using ArrayQ = typename TraitsType::template ArrayQ<T>; // solution array

  template <class T>
  using MatrixQ = typename TraitsType::template MatrixQ<T>; // solution array

  explicit VectorFunction2D_const( const ArrayQ<Real>& solIn ) : sol_(solIn) {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    return sol_;
  }

private:
  const ArrayQ<Real> sol_;
};

} // namespace SANS

#endif  // VECTORFUNCTION2D_H
