// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FUNCTION_H
#define FUNCTION_H

#include "Topology/Dimension.h"
namespace SANS
{

// meta class tag for functions
template< class PhysDim>
class Function {};

} // namespace SANS

#endif  // FUNCTION1D_H
