
INCLUDE( ${CMAKE_SOURCE_DIR}/CMakeInclude/ForceOutOfSource.cmake ) #This must be the first thing included

SET( ANALYTICFUNCTION_SRC
  ScalarFunction1D.cpp
  ScalarFunction2D.cpp
  ScalarFunction3D.cpp
  ScalarFunction4D.cpp
  )

ADD_LIBRARY( AnalyticFunctionLib STATIC ${ANALYTICFUNCTION_SRC} )

#Create the vera targest for this library
ADD_VERA_CHECKS( AnalyticFunctionLib *.h *.cpp )
ADD_HEADER_COMPILE_CHECK( AnalyticFunctionLib *.h )
ADD_CPPCHECK( AnalyticFunctionLib ${ANALYTICFUNCTION_SRC} )
