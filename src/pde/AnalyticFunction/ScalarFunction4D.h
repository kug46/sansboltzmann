// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SCALARFUNCTION4D_H
#define SCALARFUNCTION4D_H

// 3-D Advection-Diffusion PDE: exact and MMS solutions

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <cmath>
#include <memory> //shared_ptr

#include "tools/SANSnumerics.h"     // Real

#include "LinearAlgebra/DenseLinAlg/InverseLU.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "Topology/Dimension.h"

#include "Function4D.h"
#include "ScalarFunctionTraits.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// solution: const

class ScalarFunction4D_Const: public Function4DVirtualInterface<ScalarFunction4D_Const, ScalarFunctionTraits>
{
public:
  typedef PhysD3 PhysDim;

  template<class T>
  using ArrayQ = T;    // solution/residual arrays

  explicit ScalarFunction4D_Const(const Real& a0)
      : a0_( a0 )
  {
  }

  template<class T>
  ArrayQ<T> operator()(const T& x, const T& y, const T& z, const T& t) const
  {
    return a0_;
  }

private:
  Real a0_;
};

//----------------------------------------------------------------------------//
// solution: linear

class ScalarFunction4D_Linear: public Function4DVirtualInterface<ScalarFunction4D_Linear, ScalarFunctionTraits>
{
public:
  typedef PhysD3 PhysDim;

  template<class T>
  using ArrayQ = T;    // solution/residual arrays

  ScalarFunction4D_Linear(const Real& a0, const Real& ax, const Real& ay, const Real& az , const Real& at)
    : a0_( a0 ), ax_( ax ), ay_( ay ), az_( az ), at_( at )
  {
  }

  template<class T>
  ArrayQ<T> operator()(const T& x, const T& y, const T& z, const T& t) const
  {
    return a0_ + ax_ * x + ay_ * y + az_ * z + at_ * t;
  }

private:
  Real a0_, ax_, ay_, az_, at_;
};

//----------------------------------------------------------------------------//
// solution: linear

class ScalarFunction4D_Monomial: public Function4DVirtualInterface<ScalarFunction4D_Monomial, ScalarFunctionTraits>
{
public:
  typedef PhysD3 PhysDim;

  template<class T>
  using ArrayQ = T;    // solution/residual arrays

  ScalarFunction4D_Monomial(const Real& C, const std::vector<Real>& coeffs , const std::vector<int>& exponents )
    : C_(C), coeffs_(coeffs), exponents_(exponents)
  {
    SANS_ASSERT( coeffs.size()==PhysDim::D+1 );
    SANS_ASSERT( exponents.size()==PhysDim::D+1 );
  }

  template<class T>
  ArrayQ<T> operator()(const T& x, const T& y, const T& z, const T& t) const
  {
    T f = C_;
    f += coeffs_[0]*pow( x , exponents_[0] );
    f += coeffs_[1]*pow( y , exponents_[1] );
    f += coeffs_[2]*pow( z , exponents_[2] );
    f += coeffs_[3]*pow( t , exponents_[3] );
    return f;
  }

private:
  Real C_;
  const std::vector<Real>& coeffs_;
  const std::vector<int>& exponents_;
};


//----------------------------------------------------------------------------//
// solution: sinfun3 from a paper by Adrien Loseille (not sure which...)
//
// xyz = (x - 0.4) * (y - 0.4) * (z - 0.4); /* sphere2 */
//
// if (xyz <= (-1.0 * PI / 50.0))
//   u = 0.1 * sin(50. * xyz);
// else if (xyz <= (2.0 * PI / 50.0))
//   u = sin(50.0 * xyz);
// else
//   u = 0.1 * sin(50.0 * xyz);
//
// This can only be used for L2 projection as the gradient and hessian have been removed
//

class ScalarFunction4D_SinFun4 : public Function4DBase<Real>
{
public:
  typedef PhysD3 PhysDim;

  template<class T>
  using ArrayQ = T;    // solution/residual arrays

  typedef ParamsNone ParamsType;

  explicit ScalarFunction4D_SinFun4( const PyDict& d ) {}

  ScalarFunction4D_SinFun4() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& z, const T& t ) const
  {
    T xyz = (x - 0.4) * (y - 0.4) * (z - 0.4); /* sphere2 */

    if (xyz <= (-1.0 * PI / 50.0))
      return 0.1 * sin(50. * xyz);
    else if (xyz <= (2.0 * PI / 50.0))
      return sin(50.0 * xyz);
    else
      return 0.1 * sin(50.0 * xyz);
  }

  virtual ArrayQ<Real> operator()( const Real& x, const Real& y, const Real& z, const Real& t ) const override
  {
    return this->template operator()<Real>(x, y, z, t);
  }

  virtual void gradient( const Real& x, const Real& y, const Real& z, const Real& time, ArrayQ<Real>& q,
                         ArrayQ<Real>& qx, ArrayQ<Real>& qy, ArrayQ<Real>& qz, ArrayQ<Real>& qt ) const override
  {
    q = this->operator()(x, y, z, time);
    qx = qy = qz = qt = 0;
    return;
  }

  virtual void secondGradient( const Real& x, const Real& y, const Real& z, const Real& t, ArrayQ<Real>& q,
                               ArrayQ<Real>& qx, ArrayQ<Real>& qy, ArrayQ<Real>& qz, ArrayQ<Real>& qt,
                               ArrayQ<Real>& qxx,
                               ArrayQ<Real>& qyx, ArrayQ<Real>& qyy,
                               ArrayQ<Real>& qzx, ArrayQ<Real>& qzy, ArrayQ<Real>& qzz,
                               ArrayQ<Real>& qtx, ArrayQ<Real>& qty, ArrayQ<Real>& qtz, ArrayQ<Real>& qtt ) const override
  {
    q = this->operator()(x, y, z, t);
    qx = qy = qz = qt = 0;
    qxx = qyx = qyy = qzx = qzy = qzz = qtx = qty = qtz = qtt = 0;
    return;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const override
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction4D_SinFun4: " << std::endl;
  }

private:
};

class ScalarFunction4D_SineSineSineSineUnsteady: public Function4DVirtualInterface<ScalarFunction4D_SineSineSineSineUnsteady, ScalarFunctionTraits>
{
public:
  typedef PhysD3 PhysDim;

  template<class T>
  using ArrayQ = T;    // solution/residual arrays

  typedef ParamsNone ParamsType;

  ScalarFunction4D_SineSineSineSineUnsteady() {}
  explicit ScalarFunction4D_SineSineSineSineUnsteady( const PyDict& d ) {}

  template<class T>
  ArrayQ<T> operator()(const T& x, const T& y, const T& z, const T& time) const
  {
    return sin( 2 * PI * x ) * sin( 2 * PI * y ) * sin( 2 * PI * z ) * sin( 2 * PI * time );
  }
};


//----------------------------------------------------------------------------//
// solution: 4D boundary layer resulting from singular perturbation
//
// u(x,y,z,t) = exp(-x/e) + beta_y/((p+1)!)*y^(p+1) + beta_z/((p+1)!)*z^(p+1) + beta_t/((p+1)!)*t^(p+1)

class ScalarFunction4D_BoundaryLayer : public Function4DVirtualInterface<ScalarFunction4D_BoundaryLayer, ScalarFunctionTraits>
{
public:
  typedef PhysD3 PhysDim;

  template <class T>
  using ArrayQ = T;

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> epsilon{"epsilon", NO_DEFAULT, 0, NO_LIMIT, "mean characteristic length of singular perturbation"};
    const ParameterNumeric<Real> betay{"betay", NO_DEFAULT, 0, NO_LIMIT, "regularization constant in y"};
    const ParameterNumeric<Real> betaz{"betaz", NO_DEFAULT, 0, NO_LIMIT, "regularization constant in z"};
    const ParameterNumeric<Real> betat{"betat", NO_DEFAULT, 0, NO_LIMIT, "regularization constant in t"};
    const ParameterNumeric<int> p{"p", NO_DEFAULT, 0, NO_LIMIT, "solution order"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction4D_BoundaryLayer( const PyDict& d ) :
    ScalarFunction4D_BoundaryLayer(
      d.get(ParamsType::params.epsilon) ,
      d.get(ParamsType::params.betay),
      d.get(ParamsType::params.betaz),
      d.get(ParamsType::params.betat),
      d.get(ParamsType::params.p)
    )
  {}

  ScalarFunction4D_BoundaryLayer( const Real& epsilon ,
                                  const Real& betay , const Real& betaz , const Real& betat ,
                                  const int& p ) :
    epsilon_(epsilon),
    betay_(betay), betaz_(betaz) , betat_(betat) , p_(p)
  {
    SANS_ASSERT(epsilon_ > 0);
    SANS_ASSERT(betay_ > 0);
    SANS_ASSERT(betaz_ > 0);
    SANS_ASSERT(betat_ > 0);
    SANS_ASSERT(p_ > 0);
  }

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& z, const T& t ) const
  {
    return exp(-x/epsilon_) + (betay_/factorial(p_+1))*pow(y,(p_+1))
                            + (betaz_/factorial(p_+1))*pow(z,(p_+1))
                            + (betat_/factorial(p_+1))*pow(t,(p_+1));
  }

  static int
  factorial( int n )
  {
    int m;
    if (n <= 1)
      m = 1;
    else
      m = n * factorial(n - 1);

    return m;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction4D_BoundaryLayer: " << std::endl;
  }
private:
  Real epsilon_;
  Real betay_, betaz_, betat_;
  int p_;
};

//----------------------------------------------------------------------------//
// solution: 4D quadruple boundary layer
//
class ScalarFunction4D_QuadrupleBL : public Function4DVirtualInterface<ScalarFunction4D_QuadrupleBL, ScalarFunctionTraits>
{
public:
  typedef PhysD3 PhysDim;

  template <class T>
  using ArrayQ = T;

  ScalarFunction4D_QuadrupleBL( const Real& a, const Real& b , const Real& c , const Real& d , const Real& nu ) :
    a_(a), b_(b), c_(c), d_(d), nu_(nu)
  {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& z, const T& t ) const
  {
    T f, g, h, i;
    f = (1 - exp(-a_*(1 - x)/nu_)) / (1 - exp(-a_/nu_));
    g = (1 - exp(-b_*(1 - y)/nu_)) / (1 - exp(-b_/nu_));
    h = (1 - exp(-c_*(1 - z)/nu_)) / (1 - exp(-c_/nu_));
    i = (1 - exp(-d_*(1 - t)/nu_)) / (1 - exp(-d_/nu_));
    return f*g*h*i;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction4D_QuadrupleBL: " << std::endl;
  }
private:
  Real a_,b_,c_,d_,nu_;
};

//----------------------------------------------------------------------------//
// An expanding spherical wave with decaying (in time) intensity
//----------------------------------------------------------------------------//
class ScalarFunction4D_SphericalWaveDecay :
    public Function4DVirtualInterface<ScalarFunction4D_SphericalWaveDecay, ScalarFunctionTraits>
{
public:
  typedef PhysD3 PhysDim;

  template <class T>
  using ArrayQ= T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> alpha{"alpha", 1.0 , 0, NO_LIMIT, "temporal decay rate of intensity."};
    const ParameterNumeric<Real> k0{"k0", 1.0 , 0, NO_LIMIT, "initial intensity."};
    const ParameterNumeric<Real> k1{"k1", 100.0 , 0, NO_LIMIT, "spatial decay rate of intensity."};
    const ParameterNumeric<Real> velocity{"velocity", 0.5 , 0, NO_LIMIT, "constant wave propagation velocity."};
    const ParameterNumeric<Real> radius0{"radius0", 0.4 , 0.00001, 0.999999, "initial wave radius."};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction4D_SphericalWaveDecay(PyDict& d) :
    alpha_(d.get(ParamsType::params.alpha)),
    k0_(d.get(ParamsType::params.k0)),
    k1_(d.get(ParamsType::params.k1)),
    velocity_(d.get(ParamsType::params.velocity)),
    radius0_(d.get(ParamsType::params.radius0))
  {}

  explicit ScalarFunction4D_SphericalWaveDecay(const Real& alpha , const Real& k0 , const Real& k1 , const Real& velocity , const Real& radius0 ) :
    alpha_(alpha), k0_(k0), k1_(k1), velocity_(velocity), radius0_(radius0)
  {}

  ~ScalarFunction4D_SphericalWaveDecay() {}

  template<class T> ArrayQ<T> radius( const T& t ) const
  {
    return radius0_ +velocity_*t;
  }

  template<class T> ArrayQ<T> operator() (const T &x, const T &y, const T &z, const T &t) const
  {
    return k0_*exp(-alpha_*t)*exp( -k1_*pow(radius(t) - sqrt(x*x+y*y+z*z),2.) );
  }

  void dump(int indentSize= 0, std::ostream& out= std::cout) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction3D_SphericalWaveDecay:\talpha_= " << alpha_ << std::endl;
    // plus more parameters here i suppose...
  }

private:
  Real alpha_;
  Real k0_;
  Real k1_;
  Real velocity_;
  Real radius0_;
};


//----------------------------------------------------------------------------//
// Struct for creating a scalar functions
//
struct ScalarFunction4DParams: noncopyable
{
  typedef std::shared_ptr<Function4DBase<Real>> Function_ptr;

  struct FunctionOptions
  {
    typedef DictKeyPair ExtractType;
    const ParameterString Name { "Name", NO_DEFAULT,"Function name"};
    const ParameterString& key = Name;

    const DictOption SinFun4{ "SinFun4", ScalarFunction4D_SinFun4::ParamsType::checkInputs};
    const DictOption SineSineSineSineUnsteady{ "SineSineSineSineUnsteady", ScalarFunction4D_SineSineSineSineUnsteady::ParamsType::checkInputs};

    const std::vector<DictOption> options{SinFun4, SineSineSineSineUnsteady};
  };
  const ParameterOption<FunctionOptions> Function{ "Function", NO_DEFAULT, "The 4D scalar function"};

  static Function_ptr getFunction(const PyDict& d);
};

}

#endif  // SCALARFUNCTION3D_H
