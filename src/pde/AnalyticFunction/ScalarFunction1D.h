// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SCALARFUNCTION1D_H
#define SCALARFUNCTION1D_H

// 1-D Advection-Diffusion PDE: exact and MMS solutions

// Python must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <cmath>
#include <memory> //shared_ptr

#include "tools/SANSnumerics.h"     // Real

#include "Surreal/SurrealS.h"

#include "Topology/Dimension.h"

#include "Function1D.h"
#include "ScalarFunctionTraits.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// PDE class: exact/MMS solution functions
//
// member functions:
//   .()      functor returning solution
//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//
// solution: const
class ScalarFunction1D_Const : public Function1DVirtualInterface<ScalarFunction1D_Const,ScalarFunctionTraits>
{
public:
  typedef PhysD1 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> a0{"a0", NO_DEFAULT, NO_RANGE, "Constant of the function"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction1D_Const( const PyDict& d ) : a0_(d.get(ParamsType::params.a0)) {}
  explicit ScalarFunction1D_Const( const Real& a0 ) : a0_(a0) {}
  ~ScalarFunction1D_Const() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
    return a0_;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction1D_Const:  a0_ = " << a0_ << std::endl;
  }

private:
  Real a0_;
};

//----------------------------------------------------------------------------//
// solution: linear
class ScalarFunction1D_Linear : public Function1DVirtualInterface<ScalarFunction1D_Linear,ScalarFunctionTraits>
{
public:
  typedef PhysD1 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> a0{"a0", NO_DEFAULT, NO_RANGE, "Constant of the linear function"};
    const ParameterNumeric<Real> ax{"ax", NO_DEFAULT, NO_RANGE, "Linear of the linear function"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction1D_Linear( const PyDict& d ) :
    a0_(d.get(ParamsType::params.a0)), ax_(d.get(ParamsType::params.ax)) {}
  explicit ScalarFunction1D_Linear( const Real& a0 , const Real& ax ) :
    a0_(a0), ax_(ax) {}
  ~ScalarFunction1D_Linear() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
    return a0_ + ax_*x;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction1D_Linear:  a0_, ax_ = " << a0_ << ", " << ax_ << std::endl;
  }

private:
  Real a0_, ax_;
};

//----------------------------------------------------------------------------//
// solution: piecewise const
class ScalarFunction1D_PiecewiseConst : public Function1DVirtualInterface<ScalarFunction1D_PiecewiseConst,ScalarFunctionTraits>
{
public:
  typedef PhysD1 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> a0{"a0", NO_DEFAULT, NO_RANGE, "Constant value on left"};
    const ParameterNumeric<Real> a1{"a1", NO_DEFAULT, NO_RANGE, "Constant value on right"};
    const ParameterNumeric<Real> x0{"x0", NO_DEFAULT, NO_RANGE, "Discontinuity location"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction1D_PiecewiseConst( const PyDict& d ) :
      a0_(d.get(ParamsType::params.a0)), a1_(d.get(ParamsType::params.a1)), x0_(d.get(ParamsType::params.x0)) {}
  explicit ScalarFunction1D_PiecewiseConst( const Real& a0, const Real& a1, const Real& x0 ) : a0_(a0), a1_(a1), x0_(x0) {}
  ~ScalarFunction1D_PiecewiseConst() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
    if (x < x0_)
      return a0_;
    else
      return a1_;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction1D_PiecewiseConst: a0_ = " << a0_
        << " a1_ = " << a1_
        << " x0_ = " << x0_ << std::endl;
  }

private:
  Real a0_;
  Real a1_;
  Real x0_;
};

//----------------------------------------------------------------------------//
// solution: monomial
class ScalarFunction1D_Monomial : public Function1DVirtualInterface<ScalarFunction1D_Monomial,ScalarFunctionTraits>
{
public:
  typedef PhysD1 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> i{"i", NO_DEFAULT, NO_RANGE, "Monomial exponent"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction1D_Monomial( const PyDict& d ) : i_(d.get(ParamsType::params.i)) {}
  explicit ScalarFunction1D_Monomial( Real i ) : i_(i) {}
  ~ScalarFunction1D_Monomial() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
    return pow(x, i_);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction1D_Monomial: i_ = " << i_ << std::endl;
  }
private:
  Real i_;
};

//----------------------------------------------------------------------------//
// solution: 6*x*(1-x) Quadratic
class ScalarFunction1D_Quad : public Function1DVirtualInterface<ScalarFunction1D_Quad,ScalarFunctionTraits>
{
public:
  typedef PhysD1 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  typedef ParamsNone ParamsType;

  explicit ScalarFunction1D_Quad( const PyDict& d ) {}
  ScalarFunction1D_Quad() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
    return 6*x*(1-x);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction1D_Quad:" << std::endl;
  }
};

//----------------------------------------------------------------------------//
// solution: -x^2*(x - 1)*(x^3 - x^2 + x - 1) Hextic
// For debugging AWErrorEst
class ScalarFunction1D_Hex : public Function1DVirtualInterface<ScalarFunction1D_Hex,ScalarFunctionTraits>
{
public:
  typedef PhysD1 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  typedef ParamsNone ParamsType;

  explicit ScalarFunction1D_Hex( const PyDict& d ) {}
  ScalarFunction1D_Hex() {}
  ~ScalarFunction1D_Hex() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
    return -x*x*(x - 1)*(x*x*x - x*x + x - 1);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction1D_Hex:" << std::endl;
  }
};

//----------------------------------------------------------------------------//
// solution: -x^2*(x - 1)*(x^3 - x^2 + x - 1) Hextic
// For debugging AWErrorEst
class ScalarFunction1D_Hex2 : public Function1DVirtualInterface<ScalarFunction1D_Hex2,ScalarFunctionTraits>
{
public:
  typedef PhysD1 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  typedef ParamsNone ParamsType;

  explicit ScalarFunction1D_Hex2( const PyDict& d ) {}
  ScalarFunction1D_Hex2() {}
  ~ScalarFunction1D_Hex2() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
    return -x*x*(x - 2)*(x*x*x - x*x + x - 1);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction1D_Hex:" << std::endl;
  }
};

//----------------------------------------------------------------------------//
// solution: max(0, ax^3) MMS
class ScalarFunction1D_MaxCubic : public Function1DVirtualInterface<ScalarFunction1D_MaxCubic,ScalarFunctionTraits>
{
public:
  typedef PhysD1 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> a{"a", 1, NO_RANGE, "Scaling factor"};

    static void checkInputs(PyDict d);
    static ParamsType params;
  };

  explicit ScalarFunction1D_MaxCubic( const PyDict& d ) : a_(d.get(ParamsType::params.a)) {}
  explicit ScalarFunction1D_MaxCubic(const Real a = 1) : a_(a) {}
  ~ScalarFunction1D_MaxCubic() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
    return max(0.0, a_*x*x*x);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction1D_MaxCubic:" << std::endl;
  }
protected:
  const Real a_;
};

//----------------------------------------------------------------------------//
// solution: ae^x exponential MMS
class ScalarFunction1D_Exp : public Function1DVirtualInterface<ScalarFunction1D_Exp,ScalarFunctionTraits>
{
public:
  typedef PhysD1 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> a{"a", 1, NO_RANGE, "Scaling factor"};

    static void checkInputs(PyDict d);
    static ParamsType params;
  };

  explicit ScalarFunction1D_Exp( const PyDict& d ) : a_(d.get(ParamsType::params.a)) {}
  explicit ScalarFunction1D_Exp(const Real a = 1) : a_(a) {}
  ~ScalarFunction1D_Exp() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
    return a_*exp(x);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction1D_Exp:" << std::endl;
  }
protected:
  const Real a_;
};

//----------------------------------------------------------------------------//
// solution: Exponential MMS 1
class ScalarFunction1D_Exp1 : public Function1DVirtualInterface<ScalarFunction1D_Exp1,ScalarFunctionTraits>
{
public:
  typedef PhysD1 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  typedef ParamsNone ParamsType;

  explicit ScalarFunction1D_Exp1( const PyDict& d ) {}
  ScalarFunction1D_Exp1() {}
  ~ScalarFunction1D_Exp1() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
    return -(x*exp(7*x - 1)*(x - 1))/21.;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction1D_Exp1:" << std::endl;
  }
};

//----------------------------------------------------------------------------//
// solution: Exponential MMS 2
class ScalarFunction1D_Exp2 : public Function1DVirtualInterface<ScalarFunction1D_Exp2,ScalarFunctionTraits>
{
public:
  typedef PhysD1 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  typedef ParamsNone ParamsType;

  explicit ScalarFunction1D_Exp2( const PyDict& d ) {}
  ScalarFunction1D_Exp2() {}
  ~ScalarFunction1D_Exp2() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
    return -10*x*exp(1 - 10*x)*(x - 1);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction1D_Exp2:" << std::endl;
  }
};

//----------------------------------------------------------------------------//
// solution: Exponential MMS 3
class ScalarFunction1D_Exp3 : public Function1DVirtualInterface<ScalarFunction1D_Exp3,ScalarFunctionTraits>
{
public:
  typedef PhysD1 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> a{"a", 1, NO_RANGE, "Coefficient in exponent"};

    static void checkInputs(PyDict d);
    static ParamsType params;
  };

  explicit ScalarFunction1D_Exp3(const PyDict& d) : a_(d.get(ParamsType::params.a)) {}
  explicit ScalarFunction1D_Exp3(const Real a = 1) : a_(a) {}
  ~ScalarFunction1D_Exp3() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
    return expm1(a_*x)*(1. - x);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction1D_Exp3:" << std::endl;
  }
protected:
  const Real a_;
};

//----------------------------------------------------------------------------//
// solution: sin(a*PI*x)
class ScalarFunction1D_Sine : public Function1DVirtualInterface<ScalarFunction1D_Sine,ScalarFunctionTraits>
{
public:
  typedef PhysD1 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> a{"a", 2, NO_RANGE, "Frequency factor"};

    static void checkInputs(PyDict d);
    static ParamsType params;
  };

  explicit ScalarFunction1D_Sine(const PyDict& d) : a_(d.get(ParamsType::params.a)) {}
  explicit ScalarFunction1D_Sine(const Real a = 2.0) : a_(a) {}
  ~ScalarFunction1D_Sine() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
    return sin(a_*PI*x);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction1D_Sine:" << std::endl;
  }
protected:
  const Real a_;
};


//----------------------------------------------------------------------------//
// solution: t
class ScalarFunction1D_ConstLineUnsteady : public Function1DVirtualInterface<ScalarFunction1D_ConstLineUnsteady,ScalarFunctionTraits>
{
public:
  typedef PhysD1 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  typedef ParamsNone ParamsType;

  explicit ScalarFunction1D_ConstLineUnsteady( const PyDict& d ) {}
  ScalarFunction1D_ConstLineUnsteady() {}
  ~ScalarFunction1D_ConstLineUnsteady() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
    return time;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction1D_ConstLineUnsteady:" << std::endl;
  }
};

//----------------------------------------------------------------------------//
// solution: sin(2*PI*x)*cos(2*PI*t)
class ScalarFunction1D_SineCosUnsteady : public Function1DVirtualInterface<ScalarFunction1D_SineCosUnsteady,ScalarFunctionTraits>
{
public:
  typedef PhysD1 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  typedef ParamsNone ParamsType;

  explicit ScalarFunction1D_SineCosUnsteady( const PyDict& d ) {}
  ScalarFunction1D_SineCosUnsteady() {}
  ~ScalarFunction1D_SineCosUnsteady() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
    return sin(2*PI*x)*cos(2*PI*time);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction1D_SineCosUnsteady:" << std::endl;
  }
};

//----------------------------------------------------------------------------//
// solution: A*sin(a*PI*x)*sin(b*PI*t) + c
class ScalarFunction1D_SineSineUnsteady : public Function1DVirtualInterface<ScalarFunction1D_SineSineUnsteady,ScalarFunctionTraits>
{
public:
  typedef PhysD1 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> a{"a", 2, NO_RANGE, "Frequency factor"};
    const ParameterNumeric<Real> b{"b", 2, NO_RANGE, "Frequency factor"};
    const ParameterNumeric<Real> c{"c", 0, NO_RANGE, "Constant offset"};
    const ParameterNumeric<Real> A{"A", 1, NO_RANGE, "Amplitude"};

    static void checkInputs(PyDict d);
    static ParamsType params;
  };

  explicit ScalarFunction1D_SineSineUnsteady( const PyDict& d ) :
    a_(d.get(ParamsType::params.a)),
    b_(d.get(ParamsType::params.b)),
    c_(d.get(ParamsType::params.c)),
    A_(d.get(ParamsType::params.A)){}

  ScalarFunction1D_SineSineUnsteady(const Real a=2, const Real b=2, const Real c=0, const Real A=1) :
      a_(a), b_(b), c_(c), A_(A){}

  ~ScalarFunction1D_SineSineUnsteady() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
    return A_*sin(a_*PI*x)*sin(b_*PI*time) + c_;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction1D_SineSineUnsteady: a_ = " << a_
                  << ", b_ = " << b_ << ", " << c_ << ", A_ " << A_ << std::endl;
  }

protected:
  const Real a_;
  const Real b_;
  const Real c_;
  const Real A_;
};

//----------------------------------------------------------------------------//
// solution: sin(2*PI*t)
class ScalarFunction1D_ConstSineUnsteady : public Function1DVirtualInterface<ScalarFunction1D_ConstSineUnsteady,ScalarFunctionTraits>
{
public:
  typedef PhysD1 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  typedef ParamsNone ParamsType;

  explicit ScalarFunction1D_ConstSineUnsteady( const PyDict& d ) {}
  ScalarFunction1D_ConstSineUnsteady() {}
  ~ScalarFunction1D_ConstSineUnsteady() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
    return sin(2*PI*time);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction1D_ConstSineUnsteady:" << std::endl;
  }
};

//----------------------------------------------------------------------------//
// solution: cos(2*PI*t)

class ScalarFunction1D_ConstCosUnsteady : public Function1DVirtualInterface<ScalarFunction1D_ConstCosUnsteady,ScalarFunctionTraits>
{
public:
  typedef PhysD1 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  typedef ParamsNone ParamsType;

  explicit ScalarFunction1D_ConstCosUnsteady( const PyDict& d ) {}
  ScalarFunction1D_ConstCosUnsteady() {}
  ~ScalarFunction1D_ConstCosUnsteady() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
    return cos(2*PI*time);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction1D_ConstCosUnsteady:" << std::endl;
  }
};

//----------------------------------------------------------------------------//
// solution: BL
//
// u(0) = 1  u(1) = 0
//
// NOTE: exact solution for 1D advection-diffusion: a ux - nu uxx = 0
class ScalarFunction1D_BL : public Function1DVirtualInterface<ScalarFunction1D_BL,ScalarFunctionTraits>
{
public:
  typedef PhysD1 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> a{"a", NO_DEFAULT, NO_RANGE, "Advective coefficient"};
    const ParameterNumeric<Real> nu{"nu", NO_DEFAULT, 0, NO_LIMIT, "Viscous coefficient"};
    const ParameterNumeric<Real> src{"src", 0, NO_RANGE, "Reaction source term coefficient"};
    const ParameterNumeric<Real> offset{"offset", 0.0, NO_RANGE, "constant offset"};
    const ParameterNumeric<Real> scale{"scale", 1.0, NO_RANGE, "scaling"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction1D_BL( const PyDict& d ) : a_(d.get(ParamsType::params.a)), nu_(d.get(ParamsType::params.nu)),
                                                    src_(d.get(ParamsType::params.src)),
                                                    offset_(d.get(ParamsType::params.offset)),
                                                    scale_(d.get(ParamsType::params.scale))  {}
  ScalarFunction1D_BL( const Real& a, const Real& nu, const Real& src = 0,
                       const Real& offset = 0.0, const Real& scale = 1.0 ) :
                       a_(a), nu_(nu), src_(src), offset_(offset), scale_(scale) {}
  ~ScalarFunction1D_BL() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
    T num = -exp(0.5*x*(a_/nu_ + sqrt(a_*a_ + 4*nu_*src_)/nu_ ));
    num += exp(sqrt(a_*a_ + 4*nu_*src_)/nu_ + 0.5*x*(a_/nu_ - sqrt(a_*a_ + 4*nu_*src_)/nu_) );
    T denom = (-1 + exp(sqrt(a_*a_ + 4*nu_*src_)/nu_ ));

    return scale_*(num/denom) + offset_;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction1D_BL: a_ = " << a_
                  << ", nu_ = " << nu_ << std::endl;
  }

private:
  Real a_;    // advection velocity
  Real nu_;   // diffusion coefficient
  Real src_, offset_, scale_;
};

class ScalarFunction1D_BLAdj : public Function1DVirtualInterface<ScalarFunction1D_BLAdj,ScalarFunctionTraits>
{
public:
  typedef PhysD1 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> a{"a", NO_DEFAULT, NO_RANGE, "Advective coefficient"};
    const ParameterNumeric<Real> nu{"nu", NO_DEFAULT, 0, NO_LIMIT, "Viscous coefficient"};
    const ParameterNumeric<Real> src{"src", 0, NO_RANGE, "Viscous coefficient"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction1D_BLAdj( const PyDict& d ) : a_(d.get(ParamsType::params.a)), nu_(d.get(ParamsType::params.nu)),
                                                       src_(d.get(ParamsType::params.src)) {}
  ScalarFunction1D_BLAdj( const Real& a, const Real& nu, const Real& src = 0 ) : a_(a), nu_(nu), src_(src) {}
  ~ScalarFunction1D_BLAdj() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {

//    VOLUME FUNCTIONAL: return -( -exp(a_/nu_*(1-x)) - exp(a_/nu_)*(x-1) + x  ) / (a_ - a_ * exp(a_/nu_));
//    BOUNDARY FUNCTIONAL: return ( 1 - exp(-a_*x/nu_ ) ) / (1 - exp(-a_/nu_));

#if 0
    //Boundary Functional
    T C1 = -0.5*(x-1)*( a_ + sqrt(a_*a_ + 4*nu_*src_) )/nu_;
    T C2 = sqrt(a_*a_ + 4*nu_*src_) *x/nu_;
    T C3 = sqrt(a_*a_ + 4*nu_*src_)/nu_;

    T num = exp(C1)*(-1 + exp(C2) );
    T denom = -1 + exp(C3);
    return (num/denom);
#elif 1
    //Volume Functional
    T C1 = sqrt(a_*a_ + 4*nu_*src_)/nu_;
    T C2 = -0.5*(a_ + sqrt(a_*a_ + 4*nu_*src_))*(x-1)/nu_;
    T C3 = 0.5*(-a_ + sqrt(a_*a_ + 4*nu_*src_))*x/nu_;
    T C4 = - 0.5*(sqrt(a_*a_ + 4*nu_*src_)*(x-2) + a_*x)/nu_;
    T C5 = 0.5*(sqrt(a_*a_ + 4*nu_*src_)*(1+x) + a_*(1-x))/nu_;

    T denom = (-1 + exp(C1))*src_;
    T num = (-1 + exp(C1) + exp(C2) + exp(C3) - exp(C4) - exp(C5) );

    return (num/denom);

#elif 0
    // Neumann condition

    T C1 = sqrt(a_*a_ + 4*nu_*src_)/nu_;
    T C2 = -0.5*(a_ + sqrt(a_*a_ + 4*nu_*src_))*(x-1)/nu_;
    T C3 = 0.5*(a_ - a_*x +  sqrt(a_*a_ + 4*nu_*src_)*(x+1))/nu_;

    T num = a_*(-1 + exp(C1) + exp(C2) - exp(C3))+sqrt(a_*a_ + 4*nu_*src_)*(1 + exp(C1) - exp(C2) - exp(C3));
    T denom = src_*(a_*(-1 + exp(C1)) + (1 +exp(C1))*sqrt(a_*a_+4*nu_*src_));

    return (num/denom);

#else
    // volume functional g = (1 - BL solution); no source
    Real aon = a_/nu_;

    T C1 = nu_*(-1.+exp(aon))*(exp(aon) - exp(aon*x))*(-1. + exp(aon*x));
    T C2 = 2*a_*(exp(aon) + exp(aon*(1.+x))*(x-1.) - exp(aon*x)*x );

    T num = exp(-aon*x)*(C1 + C2);
    T denom = 2.*a_*a_*(-1. + exp(aon))*(-1. + exp(aon));

    return (num/denom);

#endif

  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction1D_BLAdj: a_ = " << a_
                  << ", nu_ = " << nu_ << std::endl;
  }

private:
  Real a_;    // advection velocity
  Real nu_;   // diffusion coefficient
  Real src_;
};


//----------------------------------------------------------------------------//
// solution: -tanh((x-0.5) /(2*nu))
class ScalarFunction1D_Tanh : public Function1DVirtualInterface<ScalarFunction1D_Tanh,ScalarFunctionTraits>
{
public:
  typedef PhysD1 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> nu{"nu", 0.01, 0, NO_LIMIT, "Viscous coefficient"};
    const ParameterNumeric<Real> A{"A", 2.0, NO_RANGE, "Scale factor"};
    const ParameterNumeric<Real> B{"B", 0.0, NO_RANGE, "U offset"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction1D_Tanh( PyDict& d ) :
    nu_(d.get(ParamsType::params.nu)),
    A_(d.get(ParamsType::params.A)),
    B_(d.get(ParamsType::params.B))
  {
    //Nothing
  }

  explicit ScalarFunction1D_Tanh( const Real& nu, const Real& A, const Real& B ) :
    nu_(nu),
    A_{A},
    B_{B}
  {
    //Nothing
  }
  ~ScalarFunction1D_Tanh() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
    if (nu_ != 0)
    {
      return -A_/2.0*tanh((x) / 2.0 / nu_) + B_;
    }
    else
    {
      if (x < 0)
      {
        return A_/2.0*1.0 + B_;
      }
      else
      {
        return -A_/2.0*1.0 + B_;
      }
    }
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
   {
     std::string indent(indentSize, ' ');
     out << indent << "ScalarFunction1D_Tanh:  nu_ = " << nu_ << std::endl;
     out << indent << "ScalarFunction1D_Tanh:  A_ = " << A_ << std::endl;
     out << indent << "ScalarFunction1D_Tanh:  B_ = " << B_ << std::endl;
   }

private:
  Real nu_;
  Real A_;
  Real B_;
};


//----------------------------------------------------------------------------//
class ScalarFunction1D_OliverOutputWeight : public Function1DVirtualInterface<ScalarFunction1D_OliverOutputWeight,ScalarFunctionTraits>
{
public:
  typedef PhysD1 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> nu{"nu", 1, 0, NO_LIMIT, "Viscous coefficient"};
    const ParameterNumeric<Real> c{"c", 0.5, NO_RANGE, "source strength"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction1D_OliverOutputWeight( PyDict& d ) :
    nu_(d.get(ParamsType::params.nu)),
    c_(d.get(ParamsType::params.c)) {}

  explicit ScalarFunction1D_OliverOutputWeight( const Real& nu, const Real& c ) :
    nu_(nu),
    c_{c} {}

  ~ScalarFunction1D_OliverOutputWeight() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {

    return (   (nu_ + sin(PI*x))*PI*PI*sin(PI*x)
             - (2*c_*PI*PI*sin(PI*x)*sin(PI*x))
             + (2*c_*PI*PI*cos(PI*x)*cos(PI*x))
             - (2*c_/(x*x))*sin(PI*x)*sin(PI*x));

//    return ( (nu + sin(PI*x))*PI*PI*sin(PI*x));
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction1D_OliverOutputWeight:  nu_ = " << nu_ << std::endl;
    out << indent << "ScalarFunction1D_OliverOutputWeight:  c_ = " << c_ << std::endl;
  }


private:
  Real nu_;
  Real c_;
};

//----------------------------------------------------------------------------//
// solution: 1 + 9/ln(2) * ln(1+x)
class ScalarFunction1D_OffsetLog : public Function1DVirtualInterface<ScalarFunction1D_OffsetLog,ScalarFunctionTraits>
{
public:
  typedef PhysD1 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction1D_OffsetLog( const PyDict& d ) {}
  ScalarFunction1D_OffsetLog() {}
  ~ScalarFunction1D_OffsetLog() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
    return 1 + 9.*log1p(x) / log(2);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction1D_OffsetLog:" << std::endl;
  }
};
//----------------------------------------------------------------------------//
class ScalarFunction1D_SSME : public Function1DVirtualInterface<ScalarFunction1D_SSME,ScalarFunctionTraits>
{
public:
  typedef PhysD1 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction1D_SSME( const PyDict& d ) {}
  ScalarFunction1D_SSME() {}
  ~ScalarFunction1D_SSME() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {

    if ( x<=-0.05617288)
    { // Chamber portion of converging nozzle
      return PI*pow(18.027506*pow(x,3) + 6.3263877*pow(x,2) + 0.089065500*x + 0.13178245, 2);//pi*pow(2.633603*pow(x,2) - 0.1551378*x + 0.1265217,2);
    }
    else if (x<=0)
    { // Throat portion of converging nozzle
      return PI*pow(4.0146220*pow(x,2) + 0.13087858, 2);//pi*pow(4.013907*pow(x,2) - 0.00004013907*x + 0.1308786,2);
    }
    else if (x<=0.03087576)
    { // Throat portion of diverging nozzle
      return PI*pow(-103.53197*pow(x,3) + 14.033457*pow(x,2) + 0.13087858, 2);//pi*pow(10.84034*pow(x,2) - 0.0001084034*x + 0.1308786,2);
    }
    else if (x<=3.0734)
    { // Bell (exit) portion of diverging nozzle
      return PI*pow(-0.07829450*pow(x,2) + 0.57532712*x + 0.12352044, 2);//pi*pow(-0.09914211* pow(x,2) + 0.6400439*x + 0.12154213,2);
    }
    else //constant area section for testing
    {
      return PI*pow(-0.09914211* pow(3.0734,2) + 0.6400439*3.0734 + 0.12154213,2);
    }

  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction1D_SSME:" << std::endl;
  }
};

//----------------------------------------------------------------------------//
class ScalarFunction1D_GilesPierce : public Function1DVirtualInterface<ScalarFunction1D_GilesPierce,ScalarFunctionTraits>
{
public:
  typedef PhysD1 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction1D_GilesPierce( const PyDict& d ) {}
  ScalarFunction1D_GilesPierce() {}
  ~ScalarFunction1D_GilesPierce() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
    if (x<=-0.5)
    {
      return 2;
    }
    else if (x<=0.5)
    {
      return 1+pow(sin(PI*x),2);
    }
    else
    {
      return 2;
    }

  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction1D_GilesPierce:" << std::endl;
  }
};
//----------------------------------------------------------------------------//
class ScalarFunction1D_ArtNozzle : public Function1DVirtualInterface<ScalarFunction1D_ArtNozzle,ScalarFunctionTraits>
{
public:
  typedef PhysD1 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction1D_ArtNozzle( const PyDict& d ) {}
  ScalarFunction1D_ArtNozzle() {}
  ~ScalarFunction1D_ArtNozzle() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
#if 0
// OLD FOR Arthur's 16.120
    ArrayQ<T> tmp = 0;

    Real a1, a2, a3, a4, a5, a6;
    Real b1, b2, b3, b4, b5, b6;
    Real c1, c2, c3, c4, c5, c6;

    a1 = 0.058940989981151;
    a2 = 1.761593257146927;
    a3 = -0.017689857049401;
    a4 =  0.730551133881064;
    a5 = 0.053671325253202;
    a6 = 1.100753635644865;

    b1= 0.008501867272084;
    b2=  -0.220551987242187;
    b3=  0.333532178325967;
    b4=   0.178079619318620;
    b5=   0.546303309570264;
    b6=  0.956617521538566;

    c1=   0.106878369187224;
    c2=   0.372799593009724;
    c3=  0.076693166119470;
    c4=  0.214533017114528;
    c5=   0.145276515508129;
    c6=  0.873375638414012;


    tmp +=   a1*exp(-((x-b1)/c1)*((x-b1)/c1)) + a2*exp(-((x-b2)/c2)*((x-b2)/c2)) +
             a3*exp(-((x-b3)/c3)*((x-b3)/c3)) + a4*exp(-((x-b4)/c4)*((x-b4)/c4)) +
             a5*exp(-((x-b5)/c5)*((x-b5)/c5)) + a6*exp(-((x-b6)/c6)*((x-b6)/c6));
//           +  a7*exp(-((x-b7)/c7)*((x-b7)/c7)) + a8*exp(-((x-b8)/c8)*((x-b8)/c8));

    return tmp - 0.01*x*x*x;
#else
    // Smooth nozzle: inlet area 2, area 1 throat at 0.5, area 1.2 exit
    return (-x*x*x/3. + 0.75*x*x - 0.5*x + 5./48.)*48./5. + 1.0 ;

#endif
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction1D_ArtNozzle:" << std::endl;
  }
};

//----------------------------------------------------------------------------//
class ScalarFunction1D_Constant : public Function1DVirtualInterface<ScalarFunction1D_Constant,ScalarFunctionTraits>
{
public:
  typedef PhysD1 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ScalarFunction1D_Constant( const PyDict& d ) {}
  ScalarFunction1D_Constant() {}
  ~ScalarFunction1D_Constant() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
    return 3;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction1D_Constant:" << std::endl;
  }
};

//----------------------------------------------------------------------------//
// Exact solution to non-linear poisson where the viscous flux is: Fv = exp(lam*q)*dq/dx
//
// solution: 1/lam * ln( (A + B*x)/ D)
class ScalarFunction1D_NonLinPoisson : public Function1DVirtualInterface<ScalarFunction1D_NonLinPoisson,ScalarFunctionTraits>
{
public:
  typedef PhysD1 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> lam{"lam", 1, NO_RANGE, "Viscosity Exponential"};
    const ParameterNumeric<Real> A{"A",  1, NO_RANGE, "A coefficient in U(x)=1/lam*ln((A+B*x)/D)"};
    const ParameterNumeric<Real> B{"B", 50, NO_RANGE, "B coefficient in U(x)=1/lam*ln((A+B*x)/D)"};
    const ParameterNumeric<Real> D{"D",  1, NO_RANGE, "D coefficient in U(x)=1/lam*ln((A+B*x)/D)"};

    static void checkInputs(PyDict d);
    static ParamsType params;
  };


  explicit ScalarFunction1D_NonLinPoisson( const PyDict& d ) :
    lam_(d.get(ParamsType::params.lam)),
    A_(d.get(ParamsType::params.A)),
    B_(d.get(ParamsType::params.B)),
    D_(d.get(ParamsType::params.D))
    {}
  ScalarFunction1D_NonLinPoisson(const Real lam, const Real A, const Real B, const Real D) :
    lam_(lam), A_(A), B_(B), D_(D) {}
  ~ScalarFunction1D_NonLinPoisson() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
    return 1./lam_*log((A_+B_*x)/D_);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction1D_NonLinPoisson:" << std::endl;
    out << indent << "lam_ = " << lam_ << std::endl;
    out << indent << "A_ = " << A_ << std::endl;
    out << indent << "B_ = " << B_ << std::endl;
    out << indent << "D_ = " << D_ << std::endl;
  }

private:
  Real lam_;
  Real A_;
  Real B_;
  Real D_;
};


//----------------------------------------------------------------------------//
// Exact solution to non-linear poisson where the viscous flux is: Fv = exp(lam*q)*dq/dx
//
// solution: 1/lam * ln( (A + B*x)/ D)
class ScalarFunction1D_PiecewiseLinear : public Function1DVirtualInterface<ScalarFunction1D_PiecewiseLinear,ScalarFunctionTraits>
{
public:
  typedef PhysD1 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  typedef ParamsNone ParamsType;

  explicit ScalarFunction1D_PiecewiseLinear( const PyDict& d )
  {
    // Nothing
  }
  ScalarFunction1D_PiecewiseLinear()
  {
    // Nothing
  }
  ~ScalarFunction1D_PiecewiseLinear() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
    if (x < 0.0)
    {
      return 2.0;
    }
    else if (x < 1.0)
    {
      return (2.0 - x);
    }
    else
    {
      return 1.0;
    }
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction1D_PiecewiseLinear:" << std::endl;
  }

private:
};


//----------------------------------------------------------------------------//
// Gaussian Bump solution
//
// solution: A*exp(- (x- mu)^2/ 2*sigma^2 )
class ScalarFunction1D_Gaussian : public Function1DVirtualInterface<ScalarFunction1D_Gaussian,ScalarFunctionTraits>
{
public:
  typedef PhysD1 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> A{"A",         1, NO_RANGE,    "a coefficient in A*exp(- (x- mu)^2/ 2*sigma^2 )"};
    const ParameterNumeric<Real> mu{"mu",       0, NO_RANGE,    "mu coefficient in A*exp(- (x- mu)^2/ 2*sigma^2 )"};
    const ParameterNumeric<Real> sigma{"sigma", 1, 0, NO_LIMIT, "sigma coefficient in A*exp(- (x- mu)^2/ 2*sigma^2 )"};

    static void checkInputs(PyDict d);
    static ParamsType params;
  };


  explicit ScalarFunction1D_Gaussian( const PyDict& d ) :
    A_(d.get(ParamsType::params.A)),
    mu_(d.get(ParamsType::params.mu)),
    sigma_(d.get(ParamsType::params.sigma))
    {}
  ScalarFunction1D_Gaussian(const Real A, const Real mu, const Real sigma) :
    A_(A), mu_(mu), sigma_(sigma) {}
  ~ScalarFunction1D_Gaussian() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
    return A_* exp( - (x-mu_)*(x-mu_)/(2* sigma_*sigma_) );
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction1D_Gaussian:" << std::endl;
    out << indent << "A_ = " << A_ << std::endl;
    out << indent << "mu_ = " << mu_ << std::endl;
    out << indent << "sigma_ = " << sigma_ << std::endl;
  }

private:
  Real A_;
  Real mu_;
  Real sigma_;
};

// exact solution to lorenz system with alpha_*= 0.
// u(0)= u0
// u(1)=
// u(2)=
class ScalarFunction1D_ZeroLorenz : public Function1DVirtualInterface<ScalarFunction1D_ZeroLorenz, ScalarFunctionTraits>
{
public:
  typedef PhysD1 PhysDim;

  template <class T>
  using ArrayQ= T; // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<int> eqno{"eqno", 0, 0, 2, "equation number (0, 1, or 2)"};
    const ParameterNumeric<Real> uB0{"uB0", 1, NO_RANGE, "uB0 coefficient in equation"};
    const ParameterNumeric<Real> uB1{"uB1", 1, NO_RANGE, "uB1 coefficient in equation"};
    const ParameterNumeric<Real> uB2{"uB2", 1, NO_RANGE, "uB2 coefficient in equation"};

    static void checkInputs(PyDict d);
    static ParamsType params;
  };

  explicit ScalarFunction1D_ZeroLorenz(const PyDict &d)
      : eqno_(d.get(ParamsType::params.eqno)), uB0_(d.get(ParamsType::params.uB0)),
        uB1_(d.get(ParamsType::params.uB1)), uB2_(d.get(ParamsType::params.uB2))
  {}

  ScalarFunction1D_ZeroLorenz(const int eqno, const Real uB0, const Real uB1,
      const Real uB2) : eqno_(eqno), uB0_(uB0), uB1_(uB1), uB2_(uB2)
  {}

  ~ScalarFunction1D_ZeroLorenz() {}

  template<class T>
  ArrayQ<T> operator()(const T& x, const T& time) const
  {
    Real omega= sqrt(uB0_*uB0_ - 0.25);
    Real C0= sqrt(pow(uB0_, 2) + pow(1./omega*uB0_*uB2_ + 0.5/omega*uB1_, 2));
    Real phi= PI - asin(uB1_/C0);
    Real D0= uB2_ + 2.0*C0*uB0_/(1.0 + 4.0*omega*omega)*(2.0*omega*cos(phi) + sin(phi));

    switch (eqno_)
    {
      case 0:
        return uB0_;
      case 1:
        return C0*exp(-0.5*x)*sin(omega*x + phi);
      case 2:
        return -2.0*C0*uB0_/(1.0 + 4.0*omega*omega)
            *exp(-0.5*x)*(2.0*omega*cos(omega*x + phi) + sin(omega*x + phi)) + D0;
      default:
        SANS_DEVELOPER_EXCEPTION("three equations in this one dummy");
        return 0;
    }
  }

  void dump(int indentSize= 0, std::ostream &out= std::cout) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction1D_ZeroLorenz:" << std::endl;
    out << indent << "eqno_ = " << eqno_ << std::endl;
    out << indent << "uB0_ = " << uB0_ << std::endl;
    out << indent << "uB1_ = " << uB1_ << std::endl;
    out << indent << "uB2_ = " << uB2_ << std::endl;
  }

private:
  int eqno_;
  Real uB0_;
  Real uB1_;
  Real uB2_;
};

//----------------------------------------------------------------------------//
// Unsteady Gaussian Bump solution
//
// solution: A*exp(- (x- mu(t))^2/ 2*sigma^2 )
//   with: mu(t)= mu_0 + C*t
class ScalarFunction1D_GaussianUnsteady : public Function1DVirtualInterface<ScalarFunction1D_GaussianUnsteady,ScalarFunctionTraits>
{
public:
  typedef PhysD1 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> A{"A",         1, NO_RANGE,    "a coefficient in A*exp(- (x- (mu0 + C*t))^2/ 2*sigma^2 )"};
    const ParameterNumeric<Real> mu0{"mu0",     0, NO_RANGE,    "mu0 coefficient in A*exp(- (x- (mu0 + C*t))^2/ 2*sigma^2 )"};
    const ParameterNumeric<Real> C{"C",         1, NO_RANGE,    "c coefficient in A*exp(- (x- (mu0 + C*t))^2/ 2*sigma^2 )"};
    const ParameterNumeric<Real> sigma{"sigma", 1, 0, NO_LIMIT, "sigma coefficient in A*exp(- (x- (mu0 + C*t))^2/ 2*sigma^2 )"};

    static void checkInputs(PyDict d);
    static ParamsType params;
  };

  explicit ScalarFunction1D_GaussianUnsteady( const PyDict& d ) :
    A_(d.get(ParamsType::params.A)),
    mu0_(d.get(ParamsType::params.mu0)),
    C_(d.get(ParamsType::params.C)),
    sigma_(d.get(ParamsType::params.sigma))
    {}
  ScalarFunction1D_GaussianUnsteady(const Real A, const Real mu, const Real C, const Real sigma) :
    A_(A), mu0_(mu), C_(C), sigma_(sigma) {}
  ~ScalarFunction1D_GaussianUnsteady() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
    return A_* exp( - (x - (mu0_ + C_*time))*(x-(mu0_ + C_*time))/(2* sigma_*sigma_) );
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ScalarFunction1D_GaussianUnsteady:" << std::endl;
    out << indent << "A_ = " << A_ << std::endl;
    out << indent << "mu0_ = " << mu0_ << std::endl;
    out << indent << "C_ = " << C_ << std::endl;
    out << indent << "sigma_ = " << sigma_ << std::endl;
  }

private:
  Real A_;
  Real mu0_;
  Real C_;
  Real sigma_;
};

//----------------------------------------------------------------------------//
// Struct for creating a scalar functions
//
struct ScalarFunction1DParams : noncopyable
{
  typedef std::shared_ptr<Function1DBase<Real>> Function_ptr;

  struct FunctionOptions
  {
    typedef DictKeyPair ExtractType;
    const ParameterString Name{"Name", NO_DEFAULT, "Function name" };
    const ParameterString& key = Name;

    const DictOption Const{"Const", ScalarFunction1D_Const::ParamsType::checkInputs};
    const DictOption Linear{"Linear", ScalarFunction1D_Linear::ParamsType::checkInputs};
    const DictOption PiecewiseConst{"PiecewiseConst", ScalarFunction1D_PiecewiseConst::ParamsType::checkInputs};
    const DictOption Monomial{"Monomial", ScalarFunction1D_Monomial::ParamsType::checkInputs};
    const DictOption Quad{"Quad", ScalarFunction1D_Quad::ParamsType::checkInputs};
    const DictOption Hex{"Hex", ScalarFunction1D_Hex::ParamsType::checkInputs};
    const DictOption Hex2{"Hex2", ScalarFunction1D_Hex2::ParamsType::checkInputs};
    const DictOption MaxCubic{"MaxCubic", ScalarFunction1D_MaxCubic::ParamsType::checkInputs};
    const DictOption Exp{"Exp", ScalarFunction1D_Exp::ParamsType::checkInputs};
    const DictOption Exp1{"Exp1", ScalarFunction1D_Exp1::ParamsType::checkInputs};
    const DictOption Exp2{"Exp2", ScalarFunction1D_Exp2::ParamsType::checkInputs};
    const DictOption Exp3{"Exp3", ScalarFunction1D_Exp3::ParamsType::checkInputs};
    const DictOption Sine{"Sine", ScalarFunction1D_Sine::ParamsType::checkInputs};
    const DictOption ConstLineUnsteady{"ConstLineUnsteady", ScalarFunction1D_ConstLineUnsteady::ParamsType::checkInputs};
    const DictOption SineCosUnsteady{"SineCosUnsteady", ScalarFunction1D_SineCosUnsteady::ParamsType::checkInputs};
    const DictOption SineSineUnsteady{"SineSineUnsteady", ScalarFunction1D_SineSineUnsteady::ParamsType::checkInputs};
    const DictOption ConstSineUnsteady{"ConstSineUnsteady", ScalarFunction1D_ConstSineUnsteady::ParamsType::checkInputs};
    const DictOption ConstCosUnsteady{"ConstCosUnsteady", ScalarFunction1D_ConstCosUnsteady::ParamsType::checkInputs};
    const DictOption BL{"BL", ScalarFunction1D_BL::ParamsType::checkInputs};
    const DictOption Tanh{"Tanh", ScalarFunction1D_Tanh::ParamsType::checkInputs};
    const DictOption NonLinPoisson{"NonLinPoisson", ScalarFunction1D_NonLinPoisson::ParamsType::checkInputs};
    const DictOption PiecewiseLinear{"PiecewiseLinear", ScalarFunction1D_PiecewiseLinear::ParamsType::checkInputs};
    const DictOption Gaussian{"Gaussian", ScalarFunction1D_Gaussian::ParamsType::checkInputs};
    const DictOption GaussianUnsteady{"GaussianUnsteady", ScalarFunction1D_GaussianUnsteady::ParamsType::checkInputs};
    const DictOption ZeroLorenz{"ZeroLorenz", ScalarFunction1D_ZeroLorenz::ParamsType::checkInputs};

    const std::vector<DictOption> options{ Const, Linear, PiecewiseConst, Monomial, Quad, Hex, Hex2,
                                           MaxCubic, Exp, Exp1, Exp2, Exp3, Sine,
                                           ConstLineUnsteady, SineCosUnsteady, SineSineUnsteady,
                                           ConstSineUnsteady, ConstCosUnsteady, BL, Tanh, NonLinPoisson,
                                           PiecewiseLinear, Gaussian, GaussianUnsteady,
                                           PiecewiseLinear, Gaussian, ZeroLorenz
                                         };
  };
  const ParameterOption<FunctionOptions> Function{"Function", NO_DEFAULT, "The 1D scalar function"};

  static Function_ptr getFunction(const PyDict& d);
};


} // namespace SANS

#endif  // SCALARFUNCTION1D_H
