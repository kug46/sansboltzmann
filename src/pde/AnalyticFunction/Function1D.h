// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FUNCTION1D_H
#define FUNCTION1D_H

#include "Surreal/SurrealS.h"

#include "Function.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"

namespace SANS
{

template <class ArrayQ_>
class Function1DBase : public Function<PhysD1>
{
public:
  typedef ArrayQ_ ArrayQ;

  virtual ArrayQ operator()( const Real& x, const Real& time ) const = 0;

  virtual void gradient( const Real& x, const Real& time, ArrayQ& q,
                         ArrayQ& qx, ArrayQ& qt ) const = 0;

  virtual void secondGradient( const Real& x, const Real& time, ArrayQ& q,
                               ArrayQ& qx, ArrayQ& qt,
                               ArrayQ& qxx ) const = 0;

  virtual void dump( int indentSize=0, std::ostream& out = std::cout ) const = 0;

  virtual ~Function1DBase() {}
};

template<class Derived, class Traits>
class Function1DVirtualInterface : public Function1DBase<typename Traits::template ArrayQ<Real>>
{
public:
  template <class Z>
  using ArrayQ = typename Traits::template ArrayQ<Z>;

  template <class Z>
  using MatrixQ = typename Traits::template MatrixQ<Z>;

  virtual ArrayQ<Real> operator()( const Real& x, const Real& time ) const override
  {
    return reinterpret_cast<const Derived*>(this)->operator()(x,time);
  }

  virtual void gradient( const Real& x, const Real& time, ArrayQ<Real>& q,
                         ArrayQ<Real>& qx, ArrayQ<Real>& qt ) const override
  {
    SurrealS<2> xs = x, ts = time;

    xs.deriv(0) = 1;
    ts.deriv(1) = 1;

    ArrayQ< SurrealS<2> > qderiv = 0;

    qderiv = reinterpret_cast<const Derived*>(this)->operator()(xs, ts);

    // Extract all derivatives of the exact solution
    for (int n = 0; n < DLA::VectorSize< ArrayQ<Real> >::M; n++)
    {
      DLA::index(q,n) = DLA::index(qderiv,n).value();

      DLA::index(qx,n) = DLA::index(qderiv,n).deriv(0);
      DLA::index(qt,n) = DLA::index(qderiv,n).deriv(1);
    }
  }

  virtual void secondGradient( const Real& x, const Real& time, ArrayQ<Real>& q,
                               ArrayQ<Real>& qx, ArrayQ<Real>& qt,
                               ArrayQ<Real>& qxx ) const override
  {
    SurrealS< 2, SurrealS<2> > xs = x, ts = time;

    xs.deriv(0) = 1;
    ts.deriv(1) = 1;

    xs.value().deriv(0) = 1;
    ts.value().deriv(1) = 1;

    ArrayQ< SurrealS< 2, SurrealS<2> > > qderiv = 0;

    qderiv = reinterpret_cast<const Derived*>(this)->operator()(xs, ts);

    // Extract all derivatives of the exact solution
    for (int n = 0; n < DLA::VectorSize< ArrayQ<Real> >::M; n++)
    {
      DLA::index(q,n) = DLA::index(qderiv,n).value().value();

      DLA::index(qx,n) = DLA::index(qderiv,n).deriv(0).value();
      DLA::index(qt,n) = DLA::index(qderiv,n).deriv(1).value();

      DLA::index(qxx,n) = DLA::index(qderiv,n).deriv(0).deriv(0);
    }
  }

  virtual void dump( int indentSize=0, std::ostream& out = std::cout ) const override
  {
    reinterpret_cast<const Derived*>(this)->dump(indentSize, out);
  }

  virtual ~Function1DVirtualInterface() {}
};

} // namespace SANS

#endif  // FUNCTION1D_H
