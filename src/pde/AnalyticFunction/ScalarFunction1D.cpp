// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "ScalarFunction1D.h"

#define PYDICT_INSTANTIATE
#include "Python/PyDict_impl.h"

namespace SANS
{

template ParameterOption<ScalarFunction1DParams::FunctionOptions>::ExtractType
PyDict::get(const ParameterType<ParameterOption<ScalarFunction1DParams::FunctionOptions>>&) const;

// cppcheck-suppress passedByValue
void ScalarFunction1D_Const::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.a0));
  d.checkUnknownInputs(allParams);
}
ScalarFunction1D_Const::ParamsType ScalarFunction1D_Const::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction1D_Linear::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.a0));
  allParams.push_back(d.checkInputs(params.ax));
  d.checkUnknownInputs(allParams);
}
ScalarFunction1D_Linear::ParamsType ScalarFunction1D_Linear::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction1D_PiecewiseConst::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.a0));
  allParams.push_back(d.checkInputs(params.a1));
  allParams.push_back(d.checkInputs(params.x0));
  d.checkUnknownInputs(allParams);
}
ScalarFunction1D_PiecewiseConst::ParamsType ScalarFunction1D_PiecewiseConst::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction1D_Monomial::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.i));
  d.checkUnknownInputs(allParams);
}
ScalarFunction1D_Monomial::ParamsType ScalarFunction1D_Monomial::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction1D_Sine::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.a));
  d.checkUnknownInputs(allParams);
}
ScalarFunction1D_Sine::ParamsType ScalarFunction1D_Sine::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction1D_MaxCubic::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.a));
  d.checkUnknownInputs(allParams);
}
ScalarFunction1D_MaxCubic::ParamsType ScalarFunction1D_MaxCubic::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction1D_Exp::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.a));
  d.checkUnknownInputs(allParams);
}
ScalarFunction1D_Exp::ParamsType ScalarFunction1D_Exp::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction1D_Exp3::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.a));
  d.checkUnknownInputs(allParams);
}
ScalarFunction1D_Exp3::ParamsType ScalarFunction1D_Exp3::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction1D_SineSineUnsteady::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.a));
  allParams.push_back(d.checkInputs(params.b));
  allParams.push_back(d.checkInputs(params.c));
  allParams.push_back(d.checkInputs(params.A));
  d.checkUnknownInputs(allParams);
}
ScalarFunction1D_SineSineUnsteady::ParamsType ScalarFunction1D_SineSineUnsteady::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction1D_BL::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.a));
  allParams.push_back(d.checkInputs(params.nu));
  allParams.push_back(d.checkInputs(params.src));
  allParams.push_back(d.checkInputs(params.offset));
  allParams.push_back(d.checkInputs(params.scale));
  d.checkUnknownInputs(allParams);
}
ScalarFunction1D_BL::ParamsType ScalarFunction1D_BL::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction1D_BLAdj::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.a));
  allParams.push_back(d.checkInputs(params.nu));
  allParams.push_back(d.checkInputs(params.src));
  d.checkUnknownInputs(allParams);
}
ScalarFunction1D_BLAdj::ParamsType ScalarFunction1D_BLAdj::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction1D_Tanh::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.nu));
  allParams.push_back(d.checkInputs(params.A));
  allParams.push_back(d.checkInputs(params.B));
  d.checkUnknownInputs(allParams);
}
ScalarFunction1D_Tanh::ParamsType ScalarFunction1D_Tanh::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction1D_OffsetLog::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  d.checkUnknownInputs(allParams);
}
ScalarFunction1D_OffsetLog::ParamsType ScalarFunction1D_OffsetLog::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction1D_NonLinPoisson::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.lam));
  allParams.push_back(d.checkInputs(params.A));
  allParams.push_back(d.checkInputs(params.B));
  allParams.push_back(d.checkInputs(params.D));
  d.checkUnknownInputs(allParams);
}
ScalarFunction1D_NonLinPoisson::ParamsType ScalarFunction1D_NonLinPoisson::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction1D_Gaussian::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.A));
  allParams.push_back(d.checkInputs(params.mu));
  allParams.push_back(d.checkInputs(params.sigma));
  d.checkUnknownInputs(allParams);
}
ScalarFunction1D_Gaussian::ParamsType ScalarFunction1D_Gaussian::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction1D_GaussianUnsteady::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.A));
  allParams.push_back(d.checkInputs(params.mu0));
  allParams.push_back(d.checkInputs(params.C));
  allParams.push_back(d.checkInputs(params.sigma));
  d.checkUnknownInputs(allParams);
}
ScalarFunction1D_GaussianUnsteady::ParamsType ScalarFunction1D_GaussianUnsteady::ParamsType::params;

// cppcheck-suppress passedByValue
void ScalarFunction1D_ZeroLorenz::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase *> allParams;
  allParams.push_back(d.checkInputs(params.eqno));
  allParams.push_back(d.checkInputs(params.uB0));
  allParams.push_back(d.checkInputs(params.uB1));
  allParams.push_back(d.checkInputs(params.uB2));
  d.checkUnknownInputs(allParams);
}
ScalarFunction1D_ZeroLorenz::ParamsType ScalarFunction1D_ZeroLorenz::ParamsType::params;

//===========================================================================//
ScalarFunction1DParams::Function_ptr
ScalarFunction1DParams::getFunction(const PyDict& d)
{
  ScalarFunction1DParams params;

  DictKeyPair FunctionParam = d.get(params.Function);

  if ( FunctionParam == params.Function.Const )
    return Function_ptr( new ScalarFunction1D_Const( FunctionParam ) );
  else if ( FunctionParam == params.Function.Linear )
    return Function_ptr( new ScalarFunction1D_Linear( FunctionParam ) );
  else if ( FunctionParam == params.Function.PiecewiseConst )
    return Function_ptr( new ScalarFunction1D_PiecewiseConst( FunctionParam ) );
  else if ( FunctionParam == params.Function.Monomial )
    return Function_ptr( new ScalarFunction1D_Monomial( FunctionParam ) );
  else if ( FunctionParam == params.Function.Quad )
    return Function_ptr( new ScalarFunction1D_Quad( FunctionParam ) );
  else if ( FunctionParam == params.Function.Hex )
    return Function_ptr( new ScalarFunction1D_Hex( FunctionParam ) );
  else if ( FunctionParam == params.Function.Hex2 )
    return Function_ptr( new ScalarFunction1D_Hex2( FunctionParam ) );
  else if ( FunctionParam == params.Function.MaxCubic )
    return Function_ptr( new ScalarFunction1D_MaxCubic( FunctionParam ) );
  else if ( FunctionParam == params.Function.Exp )
    return Function_ptr( new ScalarFunction1D_Exp( FunctionParam ) );
  else if ( FunctionParam == params.Function.Exp1 )
    return Function_ptr( new ScalarFunction1D_Exp1( FunctionParam ) );
  else if ( FunctionParam == params.Function.Exp2 )
    return Function_ptr( new ScalarFunction1D_Exp2( FunctionParam ) );
  else if ( FunctionParam == params.Function.Exp3 )
    return Function_ptr( new ScalarFunction1D_Exp3( FunctionParam ) );
  else if ( FunctionParam == params.Function.Sine )
    return Function_ptr( new ScalarFunction1D_Sine( FunctionParam ) );
  else if ( FunctionParam == params.Function.ConstLineUnsteady )
    return Function_ptr( new ScalarFunction1D_ConstLineUnsteady( FunctionParam ) );
  else if ( FunctionParam == params.Function.SineCosUnsteady )
    return Function_ptr( new ScalarFunction1D_SineCosUnsteady( FunctionParam ) );
  else if ( FunctionParam == params.Function.SineSineUnsteady )
    return Function_ptr( new ScalarFunction1D_SineSineUnsteady( FunctionParam ) );
  else if ( FunctionParam == params.Function.ConstSineUnsteady )
    return Function_ptr( new ScalarFunction1D_ConstSineUnsteady( FunctionParam ) );
  else if ( FunctionParam == params.Function.ConstCosUnsteady )
    return Function_ptr( new ScalarFunction1D_ConstCosUnsteady( FunctionParam ) );
  else if ( FunctionParam == params.Function.BL )
    return Function_ptr( new ScalarFunction1D_BL( FunctionParam ) );
  else if ( FunctionParam == params.Function.Tanh )
    return Function_ptr( new ScalarFunction1D_Tanh( FunctionParam ) );
  else if ( FunctionParam == params.Function.NonLinPoisson )
    return Function_ptr( new ScalarFunction1D_NonLinPoisson( FunctionParam ) );
  else if ( FunctionParam == params.Function.PiecewiseLinear )
    return Function_ptr( new ScalarFunction1D_PiecewiseLinear( FunctionParam ) );
  else if ( FunctionParam == params.Function.Gaussian )
    return Function_ptr( new ScalarFunction1D_Gaussian( FunctionParam ) );
  else if ( FunctionParam == params.Function.GaussianUnsteady )
    return Function_ptr( new ScalarFunction1D_GaussianUnsteady( FunctionParam ) );
  else if ( FunctionParam == params.Function.ZeroLorenz )
    return Function_ptr( new ScalarFunction1D_ZeroLorenz( FunctionParam ) );

  // Should never get here if checkInputs was called
  SANS_DEVELOPER_EXCEPTION("Unrecognized solution function specified");
}

}
