// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef VECTORFUNCTION1D_H
#define VECTORFUNCTION1D_H

// analytic solutions for systems of equations

// python must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "tools/SANSnumerics.h" // Real

#include "Topology/Dimension.h"

#include "Function1D.h"
#include "VectorFunctionTraits.h"

namespace SANS
{

// -------------------------------------------------------------------------- //
// solution class for systems with three variables

template <class SOL1, class SOL2, class SOL3>
class Vector3Function1D : public Function1DVirtualInterface<Vector3Function1D<SOL1, SOL2, SOL3>, VectorFunctionTraits<3>>
{
public:
  typedef PhysD1 PhysDim;

  static_assert(std::is_same<PhysDim, typename SOL1::PhysDim>::value, "Physical dimensions should match.");
  static_assert(std::is_same<PhysDim, typename SOL2::PhysDim>::value, "Physical dimensions should match.");
  static_assert(std::is_same<PhysDim, typename SOL3::PhysDim>::value, "Physical dimensions should match.");

  template <class T>
  using ArrayQ= VectorFunctionTraits<3>::template ArrayQ<T>; // solution array

  template <class T>
  using MatrixQ= VectorFunctionTraits<3>::template MatrixQ<T>; // solution matrix

  explicit Vector3Function1D(const SOL1 &sol1, const SOL2 &sol2, const SOL3 &sol3)
      : sol1_(sol1), sol2_(sol2), sol3_(sol3) {}

  template <class T>
  ArrayQ<T> operator()(const T &x, const T &time) const
  {
    ArrayQ<T> val;
    val[0]= sol1_(x, time);
    val[1]= sol2_(x, time);
    val[2]= sol3_(x, time);
    return val;
  }

private:
  const SOL1 &sol1_;
  const SOL2 &sol2_;
  const SOL3 &sol3_;
};

} // namespace SANS

#endif // VECTORFUNCTION1D_H
