// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FORCINGFUNCTIONBASE_H
#define FORCINGFUNCTIONBASE_H

#include "tools/SANSnumerics.h"     // Real
#include "Surreal/SurrealS.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include <iostream>

namespace SANS
{


//----------------------------------------------------------------------------//
// forcing function given a 1D analytical solution

template<class PDE>
class ForcingFunctionBase1D
{
public:
  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  static const int D = 1;                     // physical dimensions

  virtual bool hasForcingFunction() const = 0;

  // cppcheck-suppress noExplicitConstructor
  virtual ~ForcingFunctionBase1D() {}

  virtual void operator()( const PDE& pde, const Real &x, const Real &time, ArrayQ<Real>& forcing ) const = 0;

  virtual void dump( int indentSize=0, std::ostream& out = std::cout ) const = 0;

};


//----------------------------------------------------------------------------//
// forcing function given a 2D analytical solution

template<class PDE>
class ForcingFunctionBase2D
{
public:

  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  static const int D = 2;                     // physical dimensions

  virtual bool hasForcingFunction() const = 0;

  // cppcheck-suppress noExplicitConstructor
  virtual ~ForcingFunctionBase2D() {}

  virtual void operator()( const PDE& pde, const Real &x, const Real &y, const Real &time, ArrayQ<Real>& forcing ) const = 0;

  virtual void dump( int indentSize=0, std::ostream& out = std::cout ) const = 0;

};


//----------------------------------------------------------------------------//
// forcing function given a 3D analytical solution

template<class PDE>
class ForcingFunctionBase3D
{
public:
  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  static const int D = 3;                     // physical dimensions

  virtual bool hasForcingFunction() const = 0;

  // cppcheck-suppress noExplicitConstructor
  virtual ~ForcingFunctionBase3D() {}

  virtual void operator()
      ( const PDE& pde, const Real &x, const Real &y, const Real &z, const Real &time, ArrayQ<Real>& forcing )
      const = 0;

  virtual void dump( int indentSize=0, std::ostream& out = std::cout ) const = 0;

};

} //namespace SANS

#endif //FORCINGFUNCTIONBASE_H
