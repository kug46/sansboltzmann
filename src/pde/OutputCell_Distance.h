// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef OUTPUTCELL_DISTANCE_H
#define OUTPUTCELL_DISTANCE_H

// Python must be included first
#include "Python/PyDict.h"

#include "tools/SANSnumerics.h"     // Real

#include "OutputCategory.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Element cell integrand: solution error squared

template<class PDE_>
class OutputCell_Distance: public OutputType<OutputCell_Distance<PDE_> >
{
public:
  typedef PDE_ PDE;
  typedef typename PDE::PhysDim PhysDim;
  static const int D = PhysDim::D;

  //Array of solution variables
  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  // Array of outputs
  template<class T>
  using ArrayJ = typename PDE::template ArrayQ<T>;

  // Matrix required to represent the Jacobian of this output functional
  template<class T>
  using MatrixJ = typename PDE::template MatrixQ<T>;

  bool needsSolutionGradient() const
  {
    return false;
  }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
                  ArrayJ<To>& output) const
  {

    output = 2 * q / (sqrt( qx * qx + qy * qy ) + sqrt( qx * qx + qy * qy + 2 * q ));
  }

};

}

#endif //OUTPUTCELL_DISTANCE_H
