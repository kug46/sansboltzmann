// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDEADVECTIONDIFFUSION_ARTIFICIALVISCOSITY2D_H
#define PDEADVECTIONDIFFUSION_ARTIFICIALVISCOSITY2D_H

// 2-D Advection-Diffusion PDE class with artificial viscosity

#include <iostream>
#include <string>
#include <memory> // shared_ptr

#include "tools/SANSnumerics.h"     // Real
#include "tools/smoothmath.h"

#include "AdvectionDiffusion_Traits.h"
#include "PDEAdvectionDiffusion2D.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Exp.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "Topology/ElementTopology.h"
#include "Topology/Dimension.h"

#include "Field/Tuple/ParamTuple.h"


namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: 2-D Advection-Diffusion with artificial viscosity
//
// template parameters:
//   T                        solution DOF data type (e.g. double)
//
// member functions:
//   .hasFluxAdvectiveTime     T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective        T/F: PDE has advective/inviscid flux term
//   .hasFluxViscous          T/F: PDE has viscous flux term
//
//   .masterState             unsteady conservative fluxes: U(Q)
//   .fluxAdvective           advective/inviscid fluxes: F(Q)
//   .fluxViscous             viscous fluxes: Fv(Q, QX)
//   .diffusionViscous        viscous diffusion coefficient: d(Fv)/d(UX)
//   .isValidState            T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom              set from primitive variable array
//----------------------------------------------------------------------------//

template<class AdvectiveFlux, class ViscousFlux, class Source>
class PDEAdvectionDiffusion_ArtificialViscosity<PhysD2, AdvectiveFlux, ViscousFlux, Source> :
  public PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>
{
public:
  typedef PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source> BaseType;

  typedef PhysD2 PhysDim;
  static const int D = AdvectionDiffusionTraits<PhysD2>::D;   // physical dimensions
  static const int N = AdvectionDiffusionTraits<PhysD2>::N;   // total solution variables

  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD2>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD2>::template MatrixQ<T>; // matrices

  template <class T>
  using MatrixParam = T;         // e.g. jacobian of PDEs w.r.t. parameters

  typedef ForcingFunctionBase2D<PDEAdvectionDiffusion_ArtificialViscosity> ForcingFunctionType;

  PDEAdvectionDiffusion_ArtificialViscosity(const AdvectiveFlux& adv, const ViscousFlux& visc,
                                            const Source &src,
                                            const int order,
                                            const Real C_artvisc = 1.0 ) :
                                              BaseType(adv, visc, src), order_(order), C_artvisc_(C_artvisc) {}

  PDEAdvectionDiffusion_ArtificialViscosity(const AdvectiveFlux& adv, const ViscousFlux& visc,
                                            const Source &src, const std::shared_ptr<ForcingFunctionType>& force,
                                            const int order,
                                            const Real C_artvisc = 1.0 ) :
                                              BaseType(adv, visc, src), order_(order), C_artvisc_(C_artvisc), force_(force) {}

  ~PDEAdvectionDiffusion_ArtificialViscosity() {}

  PDEAdvectionDiffusion_ArtificialViscosity( const PDEAdvectionDiffusion_ArtificialViscosity& ) = delete;
  PDEAdvectionDiffusion_ArtificialViscosity& operator=( const PDEAdvectionDiffusion_ArtificialViscosity& ) = delete;

  // flux components
  bool hasFluxAdvectiveTime() const { return BaseType::hasFluxAdvectiveTime(); }
  bool hasFluxAdvective() const { return BaseType::hasFluxAdvective(); }
  bool hasFluxViscous() const { return true; }
  bool hasSource() const { return BaseType::hasSource(); }
  bool hasSourceTrace() const { return BaseType::hasSourceTrace(); }
  bool hasForcingFunction() const { return force_ != nullptr; }

  bool needsSolutionGradientforSource() const { return BaseType::needsSolutionGradientforSource(); }

  const AdvectiveFlux& getAdvectiveFlux() const { return BaseType::getAdvectiveFlux(); }
  const ViscousFlux& getViscousFlux() const { return BaseType::getViscousFlux(); }

  // unsteady temporal flux: Ft(Q)
  template <class L, class R, class T>
  void fluxAdvectiveTime(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, ArrayQ<T>& ft ) const
  {
    BaseType::fluxAdvectiveTime(x, y, time, q, ft);
  }

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template<class L, class R, class T>
  void jacobianFluxAdvectiveTime(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& J ) const
  {
    BaseType::jacobianFluxAdvectiveTime( x, y, time, q, J);
  }

  // master state: U(Q)
  template <class L, class R, class T>
  void masterState(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, ArrayQ<T>& uCons ) const
  {
    BaseType::masterState(x, y, time, q, uCons);
  }

  // jacobian of master state wrt q: dU(Q)/dQ
  template<class L, class R, class T>
  void jacobianMasterState(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
  {
    BaseType::jacobianMasterState( x, y, time, q, dudq);
  }

  // strong form of unsteady conservative flux: dU(Q)/dt
  template <class T>
  void strongFluxAdvectiveTime( const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& strongPDE ) const
  {
    BaseType::strongFluxAdvectiveTime(q, qt, strongPDE);
  }

  // advective flux: F(Q)
  template <class T, class L, class R, class Tf>
  void fluxAdvective(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& fy ) const
  {
    BaseType::fluxAdvective( x, y, time, q, fx, fy );
  }


  template <class T, class L, class R, class Tf>
  void fluxAdvectiveUpwind(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const
  {
    BaseType::fluxAdvectiveUpwind(x, y, time, qL, qR, nx, ny, f);
  }

  template <class T, class L, class R, class Tf>
  void fluxAdvectiveUpwindSpaceTime(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, const Real& nt, ArrayQ<Tf>& f ) const
  {
    BaseType::fluxAdvectiveUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  }


  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class T, class L, class R>
  void jacobianFluxAdvective(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q,
      MatrixQ<T>& ax, MatrixQ<T>& ay ) const
  {
    BaseType::jacobianFluxAdvective( x, y, time, q, ax, ay );
  }

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class T, class L, class R>
  void jacobianFluxAdvectiveAbsoluteValue(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const Real& nx, const Real& ny,
      MatrixQ<T>& mtx ) const
  {
    BaseType::jacobianFluxAdvectiveAbsoluteValue(x, y, time, q, nx, ny, mtx);
  }


  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class T, class L, class R>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const Real& nx, const Real& ny, const Real& nt,
      MatrixQ<T>& mtx ) const
  {
    BaseType::jacobianFluxAdvectiveAbsoluteValueSpaceTime(x, y, time, q, nx, ny, nt, mtx);
  }

  // strong form advective fluxes
  template <class T, class L, class R>
  void strongFluxAdvective(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
      ArrayQ<T>& strongPDE ) const
  {
    BaseType::strongFluxAdvective( x, y, time, q, qx, qy, strongPDE );
  }

  // viscous flux: Fv(Q, QX)
  template <class Th, class Tg, class Tq, class Tgq, class Tf>
  void fluxViscous(
      const ParamTuple<Th, Tg, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tgq>& qx, const ArrayQ<Tgq>& qy,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const;

  template <class Th, class Tg, class Tq, class Tgq, class Tf>
  void fluxViscous(
      const ParamTuple<Th, Tg, TupleClass<>>& paramL,
      const ParamTuple<Th, Tg, TupleClass<>>& paramR,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tgq>& qxL, const ArrayQ<Tgq>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tgq>& qxR, const ArrayQ<Tgq>& qyR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const;

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Th, class Ts, class Tg, class Tf>
  void diffusionViscous(
      const ParamTuple<Th, Ts, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tf>& kxx, MatrixQ<Tf>& kxy, MatrixQ<Tf>& kyx, MatrixQ<Tf>& kyy ) const;

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class T, class Th, class Tg, class Tf>
  void jacobianFluxViscous(
      const ParamTuple<Th, Tg, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& ax, MatrixQ<Tf>& ay ) const {}

  // strong form viscous fluxes
  template <class T, class L, class R>
  void strongFluxViscous(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
      const ArrayQ<T>& qxx,
      const ArrayQ<T>& qxy, const ArrayQ<T>& qyy,
      ArrayQ<T>& strongPDE ) const;


  // solution-dependent source: S(X, Q, QX)
  template <class L, class R, class Tq, class Tg, class Ts>
  void source(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ< Ts >& source ) const
  {
    BaseType::source(x, y, time, q, qx, qy, source);
  }

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX)
  template <class Tp, class Tlq, class Tq, class Tg, class Ts>
  void source(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Ts>& s ) const
  {
    source(param, x, y, time, q, qx, qy, s); //forward to regular source
  }


  // dual-consistent source
  template <class L, class R, class Tq, class Tg, class Ts>
  void sourceTrace(
      const ParamTuple<L, R, TupleClass<>>& paramL, const Real& xL, const Real& yL,
      const ParamTuple<L, R, TupleClass<>>& paramR, const Real& xR, const Real& yR,
      const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
  {
    BaseType::sourceTrace(xL, yL, xR, yR, time,
                     qL, qxL, qyL,
                     qR, qxR, qyR,
                     sourceL, sourceR);
  }

  // trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
  template <class Tp, class Tq, class Ts>
  void sourceLiftedQuantity(
      const Tp& paramL, const Real& xL, const Real& yL,
      const Tp& paramR, const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, ArrayQ<Ts>& s ) const
  {
    BaseType::sourceLiftedQuantity(xL, yL, xR, yR, time, qL,  qR, s);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class T, class L, class R>
  void jacobianSource(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
      MatrixQ<T>& dsdu ) const
  {
    BaseType::jacobianSource(x, y, time, q, qx, qy, dsdu);
  }


  // right-hand-side forcing function
  template <class T, class L, class R>
  void forcingFunction(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      ArrayQ<T>& forcing ) const;

  // characteristic speed (needed for timestep)
  template <class T, class L, class R>
  void speedCharacteristic(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const Real& dx, const Real& dy, const ArrayQ<T>& q, T& speed ) const
  {
    BaseType::speedCharacteristic(x, y, time, dx, dy, q, speed);
  }


  // characteristic speed
  template <class T, class L, class R>
  void speedCharacteristic(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, T& speed ) const
  {
    BaseType::speedCharacteristic(x, y, time, q, speed);
  }


  // is state physically valid
  template <class T>
  bool isValidState( const ArrayQ<T>& q ) const { return true; }

  // update fraction needed for physically valid state
  template <class T, class L, class R>
  void updateFraction(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& dq,
      const Real maxChangeFraction, Real& updateFraction ) const
  {
    BaseType::updateFraction(x, y, time, q, dq, maxChangeFraction, updateFraction);
  }

  // set from primitive variable array
  template <class T>
  void setDOFFrom(
      ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const
  {
    BaseType::setDOFFrom(q, data, name, nn);
  }


  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
  {
    BaseType::interpResidVariable(rsdPDEIn, rsdPDEOut);
  }

  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const
  {
    BaseType::interpResidGradient(rsdAuxIn, rsdAuxOut);
  }


  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const
  {
    BaseType::interpResidBC(rsdBCIn, rsdBCOut);
  }


  // how many residual equations are we dealing with
  int nMonitor() const
  {
    return BaseType::nMonitor();
  }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

  int getOrder() const { return order_; };

  // artificial viscosity calculation with generalized h-tensor
  template <class T, class Th, class Tg, class Ts>
  void artificialViscosity( const Real& x, const Real& y, const Real& time,
                            const ArrayQ<T>& q, const DLA::MatrixSymS<D,Th>& logH, const Tg& g,
                            MatrixQ<Ts>& Kxx, MatrixQ<Ts>& Kxy, MatrixQ<Ts>& Kyx, MatrixQ<Ts>& Kyy ) const;

private:
  const int order_;
  const Real C_artvisc_;
  const std::shared_ptr<ForcingFunctionType> force_;
};


// Artificial viscosity calculation with generalized h-tensor
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class T, class Th, class Tg, class Ts>
inline void
PDEAdvectionDiffusion_ArtificialViscosity<PhysD2, AdvectiveFlux, ViscousFlux, Source>::
artificialViscosity( const Real& x, const Real& y, const Real& time,
                     const ArrayQ<T>& q, const DLA::MatrixSymS<D,Th>& logH, const Tg& g,
                     MatrixQ<Ts>& Kxx, MatrixQ<Ts>& Kxy,
                     MatrixQ<Ts>& Kyx, MatrixQ<Ts>& Kyy ) const
{
  T lambda;

  BaseType::speedCharacteristic( x, y, time, q, lambda );

  DLA::MatrixSymS<D,Th> H = exp(logH); //generalized h-tensor

  Ts factor = C_artvisc_/(Real(order_)+1) * lambda * smoothabs0(g, 1.0e-5);

  Kxx = factor*H(0,0);
  Kxy = factor*H(0,1);
  Kyx = factor*H(1,0);
  Kyy = factor*H(1,1);
}

// viscous flux
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Th, class Tg, class Tq, class Tgq, class Tf>
inline void
PDEAdvectionDiffusion_ArtificialViscosity<PhysD2, AdvectiveFlux, ViscousFlux, Source>::fluxViscous(
    const ParamTuple<Th, Tg, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tgq>& qx, const ArrayQ<Tgq>& qy,
    ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
{
  const Th& logH = param.left();
  const Tg& sensor = param.right();

  // Add artificial viscosity
  MatrixQ<Tf> nu_xx = 0.0, nu_xy = 0.0;
  MatrixQ<Tf> nu_yx = 0.0, nu_yy = 0.0;
  artificialViscosity( x, y, time, q, logH, sensor, nu_xx, nu_xy, nu_yx, nu_yy );

  // Artificial viscous flux
  f -= nu_xx*qx + nu_xy*qy;
  g -= nu_yx*qx + nu_yy*qy;

  // Add any PDE viscosity
  BaseType::fluxViscous( x, y, time, q, qx, qy, f, g );
}


// viscous flux: normal flux with left and right states
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Th, class Tg, class Tq, class Tgq, class Tf>
inline void
PDEAdvectionDiffusion_ArtificialViscosity<PhysD2, AdvectiveFlux, ViscousFlux, Source>::fluxViscous(
    const ParamTuple<Th, Tg, TupleClass<>>& paramL,
    const ParamTuple<Th, Tg, TupleClass<>>& paramR,
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tgq>& qxL, const ArrayQ<Tgq>& qyL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tgq>& qxR, const ArrayQ<Tgq>& qyR,
    const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const
{
  ArrayQ<Tf> fL = 0, fR = 0;
  ArrayQ<Tf> gL = 0, gR = 0;

  fluxViscous(paramL, x, y, time, qL, qxL, qyL, fL, gL);
  fluxViscous(paramR, x, y, time, qR, qxR, qyR, fR, gR);

  f += 0.5*(fL + fR)*nx + 0.5*(gL + gR)*ny;
}

// viscous diffusion matrix: d(Fv)/d(UX)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Th, class Ts, class Tg, class Tf>
inline void
PDEAdvectionDiffusion_ArtificialViscosity<PhysD2, AdvectiveFlux, ViscousFlux, Source>::diffusionViscous(
    const ParamTuple<Th, Ts, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Tf>& kxx, MatrixQ<Tf>& kxy,
    MatrixQ<Tf>& kyx, MatrixQ<Tf>& kyy ) const
{
  const Th& logH = param.left();
  const Ts& sensor = param.right();

  // Add artificial viscosity
  MatrixQ<Tf> nu_xx = 0.0, nu_xy = 0.0;
  MatrixQ<Tf> nu_yx = 0.0, nu_yy = 0.0;
  artificialViscosity( x, y, time, q, logH, sensor, nu_xx, nu_xy, nu_yx, nu_yy );

  kxx += nu_xx;
  kxy += nu_xy;
  kyx += nu_yx;
  kyy += nu_yy;

  // Add the PDE diffusion matrix
  BaseType::diffusionViscous( x, y, time,
                              q, qx, qy,
                              kxx, kxy,
                              kyx, kyy );
}


// strong form of viscous flux: d(Fv)/d(X)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class T, class L, class R>
inline void
PDEAdvectionDiffusion_ArtificialViscosity<PhysD2, AdvectiveFlux, ViscousFlux, Source>::strongFluxViscous(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
    const ArrayQ<T>& qxx,
    const ArrayQ<T>& qyx, const ArrayQ<T>& qyy,
    ArrayQ<T>& strongPDE ) const
{
  SANS_DEVELOPER_EXCEPTION("PDEAdvectionDiffusion_ArtificialViscosity<PhysD2>::strongFluxViscous - Not implemented.");
}

// right-hand-side forcing function
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class T, class L, class R>
inline void
PDEAdvectionDiffusion_ArtificialViscosity<PhysD2, AdvectiveFlux, ViscousFlux, Source>::forcingFunction(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
    ArrayQ<T>& forcing ) const
{
  SANS_ASSERT(force_ != NULL);
  ArrayQ<Real> RHS = 0;
  (*force_)(*this, x, y, time, RHS );
  forcing += RHS;
}

template<class AdvectiveFlux, class ViscousFlux, class Source>
void
PDEAdvectionDiffusion_ArtificialViscosity<PhysD2, AdvectiveFlux, ViscousFlux, Source>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PDEAdvectionDiffusion_ArtificialViscosity2D: order_ = " << order_ << std::endl;
  out << indent << "PDEAdvectionDiffusion_ArtificialViscosity2D: C_artvisc_ = " << C_artvisc_ << std::endl;
  out << indent << "PDEAdvectionDiffusion_ArtificialViscosity2D: force_ =";
  if ( force_ != NULL )
    (*force_).dump(indentSize+2, out);
  else
    out << " NULL" << std::endl;
  BaseType::dump(indentSize+2, out);
}

} //namespace SANS

#endif  // PDEADVECTIONDIFFUSION_ARTIFICIALVISCOSITY2D_H
