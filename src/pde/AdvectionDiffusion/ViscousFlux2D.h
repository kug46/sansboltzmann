// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef DIFFUSIONMATRIX2D_H
#define DIFFUSIONMATRIX2D_H

// 2-D Advection-Diffusion PDE: diffusion matrix

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "AdvectionDiffusion_Traits.h"

#include <cmath> // pow
#include <iostream>
#include <utility> // forward

namespace SANS
{

class ViscousFlux2DBase
{
public:
  virtual void diffusionViscous(const Real& x, const Real& y, const Real& time,
                                const Real& q, const Real& qx, const Real& qy,
                                Real& kxx, Real& kxy,
                                Real& kyx, Real& kyy) const = 0;

  virtual ~ViscousFlux2DBase()
  {
  }
};

template<class Derived>
class ViscousFlux2DVirtualInterface: public ViscousFlux2DBase
{
public:
  virtual void diffusionViscous(const Real& x, const Real& y, const Real& time,
                                const Real& q, const Real& qx, const Real& qy,
                                Real& kxx, Real& kxy,
                                Real& kyx, Real& kyy ) const
  {
    reinterpret_cast<const Derived*>( this )->diffusionViscous( x, y, time, q, qx, qy, kxx, kxy, kyx, kyy );
  }

  virtual ~ViscousFlux2DVirtualInterface()
  {
  }
};


//----------------------------------------------------------------------------//
template<class DiffusionMatrix>
class ViscousFlux2D_LinearInGradient : public DiffusionMatrix,
                                       public ViscousFlux2DVirtualInterface<ViscousFlux2D_LinearInGradient<DiffusionMatrix>>
{
public:
  using DiffusionMatrix::diffusionViscous;

  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD1>::template MatrixQ<T>;  // matrices

  template< class... Args > // cppcheck-suppress noExplicitConstructor
  ViscousFlux2D_LinearInGradient(Args&&... args) : DiffusionMatrix(std::forward<Args>(args)...) {}

  // viscous flux is of the form K(q).gradu
  bool fluxViscousLinearInGradient() const { return true; }

  // viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
  {
    MatrixQ<Tq> kxx=0, kxy=0,
                kyx=0, kyy=0;

    diffusionViscous( x, y, time, q, qx, qy, kxx, kxy, kyx, kyy );

    f -= kxx*qx + kxy*qy;
    g -= kyx*qx + kyy*qy;
  }

};

//----------------------------------------------------------------------------//
// diffusion matrix: None
//
// member functions:
//   .()      functor returning diffusion coefficient matrix
//----------------------------------------------------------------------------//

class DiffusionMatrix2D_None
{
public:
  template<class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD2>::template ArrayQ<T>;    // solution/residual arrays

  template<class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD2>::template MatrixQ<T>;  // matrices

  static const int D = 1;                     // physical dimensions

  bool hasFluxViscous() const { return false; }

  DiffusionMatrix2D_None()
  {
  }

  template <class Tq, class Tg, class Tk>
  void diffusionViscous( const Real x, const Real y, const Real time,
                         const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
                         MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
                         MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const
  {}

  template <class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kyx_x,
      MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y ) const
  {}

  // strong form of viscous flux: d(Fv)/d(X)
  template <class Tq, class Tg, class Th, class Tf>
  inline void
  strongFluxViscous( const Real& x, const Real& y, const Real& time,
                     const ArrayQ<Tq>& q,
                     const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
                     const ArrayQ<Th>& qxx,
                     const ArrayQ<Th>& qyx, const ArrayQ<Th>& qyy,
                     ArrayQ<Tf>& strongPDE ) const
  {}

  // strong form of viscous flux: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay ) const {}

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

};

typedef ViscousFlux2D_LinearInGradient<DiffusionMatrix2D_None> ViscousFlux2D_None;

//----------------------------------------------------------------------------//
// diffusion matrix: uniform
//
// member functions:
//   .()      functor returning diffusion coefficient matrix
//----------------------------------------------------------------------------//

class DiffusionMatrix2D_Uniform
{
public:
  template<class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD2>::template ArrayQ<T>;    // solution/residual arrays

  template<class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD2>::template MatrixQ<T>;  // matrices

  static const int D = 2;                     // physical dimensions

  bool hasFluxViscous() const { return true; }

  DiffusionMatrix2D_Uniform(const Real kxx, const Real kxy, const Real kyx, const Real kyy)
      : kxx_( kxx ), kxy_( kxy ), kyx_( kyx ), kyy_( kyy )
  {
  }

  template <class Tq, class Tg, class Tk>
  void diffusionViscous( const Real x, const Real y, const Real time,
                         const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
                         MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
                         MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const
  {
    kxx += kxx_; kxy += kxy_;
    kyx += kyx_; kyy += kyy_;
  }

  template <class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kyx_x,
      MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y ) const
  {}

  // strong form of viscous flux: d(Fv)/d(X)
  template <class Tq, class Tg, class Th, class Tf>
  inline void
  strongFluxViscous( const Real& x, const Real& y, const Real& time,
                     const ArrayQ<Tq>& q,
                     const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
                     const ArrayQ<Th>& qxx,
                     const ArrayQ<Th>& qyx, const ArrayQ<Th>& qyy,
                     ArrayQ<Tf>& strongPDE ) const
  {
    strongPDE -= kxx_ * qxx + (kxy_ + kyx_) * qyx + kyy_ * qyy;
  }

  // strong form of viscous flux: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay ) const
  {
    // Fv not a function of U
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  const Real kxx_, kxy_, kyx_, kyy_;          // constant diffusion matrix
};

typedef ViscousFlux2D_LinearInGradient<DiffusionMatrix2D_Uniform> ViscousFlux2D_Uniform;


//----------------------------------------------------------------------------//
// Viscous Flux: P- laplacian
//          This is the diffusion term for the Distance equation
//
// member functions:
//   .()      functor returning diffusion coefficient matrix
//----------------------------------------------------------------------------//

class ViscousFlux2D_P_Laplacian : public ViscousFlux2DVirtualInterface<ViscousFlux2D_P_Laplacian>
{
public:
  template<class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD2>::template ArrayQ<T>;    // solution/residual arrays

  template<class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD2>::template MatrixQ<T>;  // matrices

  static const int D = 2;                     // physical dimensions

  bool hasFluxViscous() const { return true; }

  // viscous flux is of the form K(q).gradu
  bool fluxViscousLinearInGradient() const { return false; }

  explicit ViscousFlux2D_P_Laplacian(const Real P) : P_( P ) {}

  // viscous flux: Fv(Q, QX) = |gradq|^(P-2) . gradq
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
  {
    SANS_DEVELOPER_EXCEPTION("Not tested yet.");
    Tg ngradq = sqrt(qx*qx + qy*qy);

    Tg k = pow(ngradq, P_-2);

    f -= k*qx;
    g -= k*qy;
  }

  // viscous flux diffusion marix: d(Fv(Q, QX))/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscous( const Real x, const Real y, const Real time,
                         const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
                         MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
                         MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const
  {
    SANS_DEVELOPER_EXCEPTION("Not tested yet.");
    Tg ngradq = sqrt(qx*qx + qy*qy);
    Tg ngradq_qx = qx/ngradq;
    Tg ngradq_qy = qy/ngradq;

    Tg k = pow(ngradq, P_-2);
    Tg k_qx = (P_-2)*pow(ngradq, P_-3)*ngradq_qx;
    Tg k_qy = (P_-2)*pow(ngradq, P_-3)*ngradq_qy;

    kxx += k + k_qx*qx;  kxy +=     k_qy*qx;
    kyx +=     k_qx*qy;  kyy += k + k_qy*qy;
  }

  template <class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kyx_x,
      MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y ) const
  {
    SANS_DEVELOPER_EXCEPTION("Not tested yet.");
  }


  // strong form of viscous flux: d(Fv)/d(X)
  template<class T>
  inline void strongFluxViscous(const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
                                const ArrayQ<T>& qxx, const ArrayQ<T>& qyx, const ArrayQ<T>& qyy, ArrayQ<T>& strongPDE) const
  {
    SANS_DEVELOPER_EXCEPTION("Not tested yet.");
    T ngradq  = sqrt(qx*qx + qy*qy);
    T ngradqx = (qx*qxx + qy*qyx)/ngradq;
    T ngradqy = (qx*qyx + qy*qyy)/ngradq;

    T k = pow(ngradq, P_-2);
    T kx = (P_-2)*pow(ngradq, P_-3)*ngradqx;
    T ky = (P_-2)*pow(ngradq, P_-3)*ngradqy;

    strongPDE -= kx*qx + k*qxx + ky*qy + k*qyy;
  }

  bool isValidState(const ArrayQ<Real>& q) const
  {
    return true;
  }

  void dump(int indentSize = 0, std::ostream& out = std::cout) const;

private:
  const Real P_;          // constant diffusion matrix
};

//----------------------------------------------------------------------------//
// diffusion matrix: Hamiltonian - Jacobi
//          This is the diffusion term for the Distance equation
//
// member functions:
//   .()      functor returning diffusion coefficient matrix
//----------------------------------------------------------------------------//

class DiffusionMatrix2D_HJ
{
public:
  template<class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD2>::template ArrayQ<T>;

  template<class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD2>::template MatrixQ<T>;

  static const int D = 2;

  bool hasFluxViscous() const { return true; }

  explicit DiffusionMatrix2D_HJ(const Real E)
      : E_( E )
  {
  }

  template <class Tq, class Tg, class Tk>
  void diffusionViscous( const Real x, const Real y, const Real time,
                         const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
                         MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
                         MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const
  {
    kxx = E_ + 1;
    kyy = kxx;
    kxy = 0;
    kyx = 0;
  }

  template<class T>
  inline void strongFluxViscous(const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
                                const ArrayQ<T>& qxx, const ArrayQ<T>& qyx, const ArrayQ<T>& qyy, ArrayQ<T>& strongPDE) const
  {
    strongPDE += qx * qx + q * qxx + qy * qy + q * qyy - (E_ + 1.0) * (q * qxx + q * qyy);
  }

  bool isValidState(const ArrayQ<Real>& q) const
  {
    return true;
  }

  void dump(int indentSize = 0, std::ostream& out = std::cout) const;

private:
  const Real E_;
};

typedef ViscousFlux2D_LinearInGradient<DiffusionMatrix2D_HJ> ViscousFlux2D_HJ;

} //namespace SANS

#endif  // DIFFUSIONMATRIX2D_H
