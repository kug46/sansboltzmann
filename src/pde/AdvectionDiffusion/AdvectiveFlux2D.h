// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ADVECTIVEFLUX2D_H
#define ADVECTIVEFLUX2D_H

// 2-D Advection-Diffusion PDE: advective flux

#include "tools/SANSnumerics.h"     // Real
#include "AdvectionDiffusion_Traits.h"

#include <cmath>
#include <iostream>

namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: advection velocity
//
// member functions:
//   .flux       advective flux: F(Q)
//   .jacobian   advective flux jacboian: d(F)/d(U)
//----------------------------------------------------------------------------//

class AdvectiveFlux2D_None
{
public:
  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD2>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD2>::template MatrixQ<T>;  // matrices

  bool hasFluxAdvective() const { return false; }

  ~AdvectiveFlux2D_None() {}

  // advective flux: F(Q)
  template<class T, class Tf>
  void flux( const Real x, const Real y, const Real time,
             const ArrayQ<T>& q, ArrayQ<Tf>& fx, ArrayQ<Tf>& fy ) const
  {}

  // advective flux jacobian: d(F)/d(U)
  template<class Tq, class Tf>
  void jacobian( const Real x, const Real y, const Real time,
                 const ArrayQ<Tq>& q, MatrixQ<Tf>& ax, MatrixQ<Tf>& ay ) const
  {}

  template<class Tq, class Tf>
  void fluxUpwind( const Real x, const Real y, const Real time,
                   const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, const Real& nx, const Real& ny,
                   ArrayQ<Tf>& f, const Real& scale = 1  ) const
  {}

  template<class Tq, class Tf>
  void fluxUpwindSpaceTime( const Real x, const Real y, const Real time,
                            const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
                            const Real& nx, const Real& ny, const Real& nt,
                            ArrayQ<Tf>& f, const Real& scale= 1 ) const
  {
    f += 0.5*(nt)*(qR + qL) - 0.5*scale*fabs(nt)*(qR - qL);
  }

  // strong advective flux: div.F
  template<class Tq, class Tg, class Tf>
  void strongFlux( const Real x, const Real y, const Real time,
                   const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
                   ArrayQ<Tf>& strongPDE ) const
  {}

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;
};


//----------------------------------------------------------------------------//
// advection velocity: uniform
//
// member functions:
//   .flux         advective flux: F(Q)
//   .jacobian     advective flux jacboian: d(F)/d(U)
//   .strongFlux   advective flux : div.F
//----------------------------------------------------------------------------//

class AdvectiveFlux2D_Uniform
{
public:
  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD2>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD2>::template MatrixQ<T>;  // matrices

  bool hasFluxAdvective() const { return true; }

  AdvectiveFlux2D_Uniform( const Real u, const Real v ) : u_(u), v_(v) {}
  ~AdvectiveFlux2D_Uniform() {}

  // advective flux: F(Q)
  template<class Tq, class Tf>
  void flux( const Real x, const Real y, const Real time,
             const ArrayQ<Tq>& q, ArrayQ<Tf>& fx, ArrayQ<Tf>& fy ) const
  {
    fx += u_*q;
    fy += v_*q;
  }

  // advective flux jacobian: d(F)/d(U)
  template<class Tq, class Tf>
  void jacobian( const Real x, const Real y, const Real time,
                 const ArrayQ<Tq>& q, MatrixQ<Tf>& ax, MatrixQ<Tf>& ay ) const
  {
    ax += u_;
    ay += v_;
  }

  template<class Tq, class Tf>
  void fluxUpwind( const Real x, const Real y, const Real time,
                   const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, const Real& nx, const Real& ny,
                   ArrayQ<Tf>& f, const Real& scale = 1  ) const
  {
    f += 0.5*(nx*u_ + ny*v_)*(qR + qL) - 0.5*scale*fabs(nx*u_ + ny*v_)*(qR - qL);
  }

  template<class Tq, class Tf>
  void fluxUpwindSpaceTime( const Real x, const Real y, const Real time,
                            const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
                            const Real& nx, const Real& ny, const Real& nt,
                            ArrayQ<Tf>& f, const Real& scale= 1 ) const
  {
    f += 0.5*(nt*1 + nx*u_ + ny*v_)*(qR + qL) - 0.5*scale*fabs(nt*1 + nx*u_ + ny*v_)*(qR - qL);
  }

  // strong advective flux: div.F
  template<class Tq, class Tg, class Tf>
  void strongFlux( const Real x, const Real y, const Real time,
                   const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
                   ArrayQ<Tf>& strongPDE ) const
  {
    strongPDE += u_*qx + v_*qy;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  const Real u_, v_;      // uniform advection velocity
};

//----------------------------------------------------------------------------//
// advection velocity: piecewise uniform
//
// member functions:
//   .flux         advective flux: F(Q)
//   .jacobian     advective flux jacboian: d(F)/d(U)
//   .strongFlux   advective flux : div.F
//----------------------------------------------------------------------------//

class AdvectiveFlux2D_PiecewiseUniform
{
public:
  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD2>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD2>::template MatrixQ<T>;  // matrices

  bool hasFluxAdvective() const { return true; }

  AdvectiveFlux2D_PiecewiseUniform( const Real u00, const Real u01, const Real u10, const Real u11,
                                    const Real v00, const Real v01, const Real v10, const Real v11,
                                    const Real x0, const Real y0 ) :
                                      u00_(u00), u01_(u01), u10_(u10), u11_(u11),
                                      v00_(v00), v01_(v01), v10_(v10), v11_(v11),
                                      x0_(x0), y0_(y0) {}
  ~AdvectiveFlux2D_PiecewiseUniform() {}

  // advective flux: F(Q)
  template<class T, class Tf>
  void flux( const Real x, const Real y, const Real time,
             const ArrayQ<T>& q, ArrayQ<Tf>& fx, ArrayQ<Tf>& fy ) const
  {
    if (x < x0_)
    {
      if (y < y0_)
      {
        fx += u10_*q;
        fy += v10_*q;
      }
      else
      {
        fx += u00_*q;
        fy += v00_*q;
      }
    }
    else
    {
      if (y < y0_)
      {
        fx += u11_*q;
        fy += v11_*q;
      }
      else
      {
        fx += u01_*q;
        fy += v01_*q;
      }
    }
  }

  // advective flux jacobian: d(F)/d(U)
  template<class Tq, class Tf>
  void jacobian( const Real x, const Real y, const Real time,
                 const ArrayQ<Tq>& q, MatrixQ<Tf>& ax, MatrixQ<Tf>& ay ) const
  {
    if (x < x0_)
    {
      if (y < y0_)
      {
        ax += u10_;
        ay += v10_;
      }
      else
      {
        ax += u00_;
        ay += v00_;
      }
    }
    else
    {
      if (y < y0_)
      {
        ax += u11_;
        ay += v11_;
      }
      else
      {
        ax += u01_;
        ay += v01_;
      }
    }
  }

  template<class Tq, class Tf>
  void fluxUpwind( const Real x, const Real y, const Real time,
                   const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, const Real& nx, const Real& ny,
                   ArrayQ<Tf>& f, const Real& scale= 1 ) const
  {
    if (x < x0_)
    {
      if (y < y0_)
        f += 0.5*(nx*u10_ + ny*v10_)*(qR + qL) - 0.5*scale*fabs(nx*u10_ + ny*v10_)*(qR - qL);
      else
        f += 0.5*(nx*u00_ + ny*v00_)*(qR + qL) - 0.5*scale*fabs(nx*u00_ + ny*v00_)*(qR - qL);
    }
    else
    {
      if (y < y0_)
        f += 0.5*(nx*u11_ + ny*v11_)*(qR + qL) - 0.5*scale*fabs(nx*u11_ + ny*v11_)*(qR - qL);
      else
        f += 0.5*(nx*u01_ + ny*v01_)*(qR + qL) - 0.5*scale*fabs(nx*u01_ + ny*v01_)*(qR - qL);
    }
  }

  template<class Tq, class Tf>
  void fluxUpwindSpaceTime( const Real x, const Real y, const Real time,
                            const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
                            const Real& nx, const Real& ny, const Real& nt,
                            ArrayQ<Tf>& f, const Real& scale= 1 ) const
  {
    if (x < x0_)
    {
      if (y < y0_)
        f += 0.5*(nt + nx*u10_ + ny*v10_)*(qR + qL) - 0.5*scale*fabs(nt + nx*u10_ + ny*v10_)*(qR - qL);
      else
        f += 0.5*(nt + nx*u00_ + ny*v00_)*(qR + qL) - 0.5*scale*fabs(nt + nx*u00_ + ny*v00_)*(qR - qL);
    }
    else
    {
      if (y < y0_)
        f += 0.5*(nt + nx*u11_ + ny*v11_)*(qR + qL) - 0.5*scale*fabs(nt + nx*u11_ + ny*v11_)*(qR - qL);
      else
        f += 0.5*(nt + nx*u01_ + ny*v01_)*(qR + qL) - 0.5*scale*fabs(nt + nx*u01_ + ny*v01_)*(qR - qL);
    }
  }

  // strong advective flux: div.F
  template<class Tq, class Tg, class Tf>
  void strongFlux( const Real x, const Real y, const Real time,
                   const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
                   ArrayQ<Tf>& strongPDE ) const
  {
    if (x < x0_)
    {
      if (y < y0_)
        strongPDE += u10_*qx + v10_*qy;
      else
        strongPDE += u00_*qx + v00_*qy;
    }
    else
    {
      if (y < y0_)
        strongPDE += u11_*qx + v11_*qy;
      else
        strongPDE += u01_*qx + v01_*qy;
    }
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  const Real u00_, u01_, u10_, u11_; // uniform advection velocity
  const Real v00_, v01_, v10_, v11_;
  const Real x0_, y0_;
};

//----------------------------------------------------------------------------//
// Local Lax-Friedrichs upwind scheme
//
// member functions:
//   .fluxUpwind            advective flux: n.F(QL,QR)
//   .fluxUpwindSpaceTime   advective flux: n.F(QL,QR)
//----------------------------------------------------------------------------//
#if 0 // This is commented out until unit tests are written for it. It looks like it's correct, but needs tests.
template<class AdvectiveFlux2D>
class LocalLaxFriedrichs2D : public AdvectiveFlux2D
{
public:
  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD2>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD2>::template MatrixQ<T>;  // matrices

  template<class T>
  void fluxUpwind( const Real x, const Real y, const Real time,
                   const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx, const Real& ny,
                   ArrayQ<T>& f ) const
  {
    ArrayQ<T> fL = 0, gL = 0;
    ArrayQ<T> fR = 0, gR = 0;

    flux(x, y, time, qL, fL, gL);
    flux(x, y, time, qR, fR, gR);

    ArrayQ<T> axL = 0, ayL = 0;
    ArrayQ<T> axR = 0, ayR = 0;

    jacobian(x, y, time, qL, axL, ayL);
    jacobian(x, y, time, qR, axR, ayR);

    //TODO: Rather arbitrary smoothing constants here
    Real eps = 1e-8;
    Real alpha = 40;

    T anL = nx*axL[0] + ny*ayL[0];
    T anR = nx*axR[0] + ny*ayR[0];

    T a = smoothmax( smoothabs0(anL, eps), smoothabs0(anR, eps), alpha);

    // Local Lax-Friedrichs Flux
    f += 0.5*( (nx*fR[0] + ny*gR[0]) + (nx*fL[0] + ny*gL[0]) ) - 0.5*a*(qR[0] - qL[0]);
  }

  template<class T>
  void fluxUpwindSpaceTime( const Real x, const Real y, const Real time,
                   const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx, const Real& ny, const Real& nt,
                   ArrayQ<T>& f ) const
  {
    ArrayQ<T> fL = 0, gL = 0;
    ArrayQ<T> fR = 0, gR = 0;

    flux(x, y, time, qL, fL, gL);
    flux(x, y, time, qR, fR, gR);

    ArrayQ<T> axL = 0, ayL = 0;
    ArrayQ<T> axR = 0, ayR = 0;

    jacobian(x, y, time, qL, axL, ayL);
    jacobian(x, y, time, qR, axR, ayR);

    //TODO: Rather arbitrary smoothing constants here
    Real eps = 1e-8;
    Real alpha = 40;

    T anL = nt + nx*axL[0] + ny*ayL[0];
    T anR = nt + nx*axR[0] + ny*ayR[0];

    T a = smoothmax( smoothabs0(anL, eps), smoothabs0(anR, eps), alpha);

    // Local Lax-Friedrichs Flux
    f += 0.5*( (nt*qR + nx*fR + ny*gR) + (nt*qL + nx*fL + ny*gL) ) - 0.5*a*(qR - qL);
  }

};
#endif
//----------------------------------------------------------------------------//
// advection flux: cubic source bump (exact solution for Cauchy-Riemann)

class AdvectiveFlux2D_CubicSourceBump
{
public:
  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD2>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD2>::template MatrixQ<T>;  // matrices

  explicit AdvectiveFlux2D_CubicSourceBump( const Real tau );
  ~AdvectiveFlux2D_CubicSourceBump() {}

  // advective flux: F(Q)
  template<class T, class Tf>
  void flux( const Real x, const Real y, const Real time,
             const ArrayQ<T>& q, ArrayQ<Tf>& fx, ArrayQ<Tf>& fy ) const
  {
    Real u, v;
    velocity( x, y, u, v );

    // Compute the flux
    fx += u*q;
    fy += v*q;
  }

  // advective flux jacobian: d(F)/d(U)
  template<class Tq, class Tf>
  void jacobian( const Real x, const Real y, const Real time,
                 const ArrayQ<Tq>& q, MatrixQ<Tf>& ax, MatrixQ<Tf>& ay ) const
  {
    Real u, v;
    velocity( x, y, u, v );

    ax += u;
    ay += v;
  }


  template<class Tq, class Tf>
  void fluxUpwind( const Real x, const Real y, const Real time,
                   const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, const Real& nx, const Real& ny,
                   ArrayQ<Tf>& f, const Real& scale = 1  ) const
  {
    Real u, v;
    velocity( x, y, u, v );

    f += 0.5*(nx*u + ny*v)*(qR + qL) - 0.5*scale*fabs(nx*u + ny*v)*(qR - qL);
  }

  template<class Tq, class Tf>
  void fluxUpwindSpaceTime( const Real x, const Real y, const Real time,
                            const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
                            const Real& nx, const Real& ny, const Real& nt,
                            ArrayQ<Tf>& f, const Real& scale= 1 ) const
  {
    Real u, v;
    velocity( x, y, u, v );

    f += 0.5*(nt*1 + nx*u + ny*v)*(qR + qL) - 0.5*scale*fabs(nt*1 + nx*u + ny*v)*(qR - qL);
  }

  // strong advective flux: div.F
  template<class Tq, class Tg, class Tf>
  void strongFlux( const Real x, const Real y, const Real time,
                   const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
                   ArrayQ<Tf>& strongPDE ) const
  {
    Real u, v;
    velocity( x, y, u, v );

    strongPDE += u*qx + v*qy;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  void velocity( const Real x, const Real y, Real& u, Real& v ) const;

  Real tau_;      // bump height
  Real src_;      // source strength
};

//----------------------------------------------------------------------------//
// advection flux: rotation over cylinder with constant advective speed
//
// member functions:
//   .flux         advective flux: F(Q)
//   .jacobian     advective flux jacboian: d(F)/d(U)
//   .strongFlux   advective flux : div.F
//----------------------------------------------------------------------------//

class AdvectiveFlux2D_ConstRotation
{
public:
  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD2>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD2>::template MatrixQ<T>;  // matrices

  bool hasFluxAdvective() const { return true; }

  explicit AdvectiveFlux2D_ConstRotation( const Real& V ) : V_(V)  {}

  ~AdvectiveFlux2D_ConstRotation() {}

  // advective flux: F(Q)
  template<class Tq, class Tf>
  void flux( const Real x, const Real y, const Real time,
             const ArrayQ<Tq>& q, ArrayQ<Tf>& fx, ArrayQ<Tf>& fy ) const
  {
    const Real r = sqrt(x*x + y*y);
    Real u = - V_ * y / r;     // x-component of advection velocity
    Real v =   V_ * x / r;     // y-component of advection velocity
    fx += u*q;
    fy += v*q;
  }

  // advective flux jacobian: d(F)/d(U)
  template<class Tq, class Tf>
  void jacobian( const Real x, const Real y, const Real time,
                 const ArrayQ<Tq>& q, MatrixQ<Tf>& ax, MatrixQ<Tf>& ay ) const
  {
    Real r = sqrt(x*x + y*y);
    Real u = - V_ * y / r;     // x-component of advection velocity
    Real v =   V_ * x / r;     // y-component of advection velocity
    ax += u;
    ay += v;
  }

  template<class Tq, class Tf>
  void fluxUpwind( const Real x, const Real y, const Real time,
                   const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, const Real& nx, const Real& ny,
                   ArrayQ<Tf>& f, const Real& scale = 1  ) const
  {
    Real r = sqrt(x*x + y*y);
    Real u = - V_ * y / r;     // x-component of advection velocity
    Real v =   V_ * x / r;     // y-component of advection velocity

    Real tx = nx, ty = ny;
#if 0 // hack: construct the true trace normal vector on a circular manifold
    if ( nx*u + ny*v > 0 )
    {
      tx = u / V_;
      ty = v / V_;
    }
    else
    {
      tx = - u / V_;
      ty = - v / V_;
    }
#endif
#if 0  // hack: normalized bisector of analytic and left-biased trace unit normal
    tx = 0.5*(nx+tx), ty = 0.5*(ny+ty);
    tx = tx / sqrt( tx*tx + ty*ty );
    ty = ty / sqrt( tx*tx + ty*ty );
#endif

    f += 0.5*(tx*u + ty*v)*(qR + qL) - 0.5*scale*fabs(tx*u + ty*v)*(qR - qL);
  }

#if 0
  template<class T, class Tf>
  void fluxUpwindSpaceTime( const Real x, const Real y, const Real time,
                            const ArrayQ<T>& qL, const ArrayQ<T>& qR,
                            const Real& nx, const Real& ny, const Real& nt,
                            ArrayQ<Tf>& f ) const
  {
    f += 0.5*(nt*1 + nx*u_ + ny*v_)*(qR + qL) - 0.5*fabs(nt*1 + nx*u_ + ny*v_)*(qR - qL);
  }
#endif

  // strong advective flux: div.F
  template<class Tq, class Tg, class Tf>
  void strongFlux( const Real x, const Real y, const Real time,
                   const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
                   ArrayQ<Tf>& strongPDE ) const
  {
    Real r = sqrt(x*x + y*y);
    Real u = - V_ * y / r;     // x-component of advection velocity
    Real v =   V_ * x / r;     // y-component of advection velocity
    strongPDE += u*qx + v*qy;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  const Real V_;      // uniform advection speed
};

} //namespace SANS

#endif  // ADVECTIVEFLUX2D_H
