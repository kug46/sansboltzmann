// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// 2D diffusion matrix

#include <string>
#include <iostream>

#include "ViscousFlux2D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// diffusion matrix: uniform

void
DiffusionMatrix2D_Uniform::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "DiffusionMatrix2D_Uniform:"
            << "  kxx_ = " << kxx_
            << "  kxy_ = " << kxy_
            << "  kyx_ = " << kyx_
            << "  kyy_ = " << kyy_ << std::endl;
}

} //namespace SANS
