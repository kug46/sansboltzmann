// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDEADVECTIONDIFFUSION1D_H
#define PDEADVECTIONDIFFUSION1D_H

// 2-D Advection-Diffusion PDE class

#include "tools/SANSnumerics.h"     // Real
#include "AdvectionDiffusion_Traits.h"
#include "AdvectiveFlux1D.h"
#include "ViscousFlux1D.h"
#include "ForcingFunction1D.h"
#include "Source1D.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "Topology/ElementTopology.h"
#include "Topology/Dimension.h"
#include "pde/ForcingFunctionBase.h"

#include <cmath>              // sqrt
#include <iostream>
#include <string>
#include <memory>             // shared_ptr

namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: 1-D Advection-Diffusion
//
// template parameters:
//   T                        solution DOF data type (e.g. double)
//
// member functions:
//   .hasFluxAdvectiveTime        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//   .hasFluxViscous             T/F: PDE has viscous flux term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: U(Q)
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .fluxViscous                viscous fluxes: Fv(Q, QX)
//   .diffusionViscous           viscous diffusion coefficient: d(Fv)/d(UX)
//   .isValidState               T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom                 set from primitive variable array
//----------------------------------------------------------------------------//

template<class AdvectiveFlux, class ViscousFlux, class Source>
class PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>
{
public:
  typedef PhysD1 PhysDim;
  static const int D = AdvectionDiffusionTraits<PhysD1>::D;   // physical dimensions
  static const int N = AdvectionDiffusionTraits<PhysD1>::N;   // total solution variables

  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD1>::template MatrixQ<T>; // matrices

  typedef PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source> PDEClass;
  typedef ForcingFunctionBase1D<PDEClass> ForcingFunctionType;

  PDEAdvectionDiffusion( const AdvectiveFlux& adv, const ViscousFlux& visc,
                         const Source &src, const std::shared_ptr<ForcingFunctionType>& force = NULL ) :
      adv_(adv), visc_(visc), source_(src), force_(force) {}

  ~PDEAdvectionDiffusion() {}

  PDEAdvectionDiffusion& operator=( const PDEAdvectionDiffusion& ) = delete;

  // flux components
  bool hasFluxAdvectiveTime() const { return true; }
  bool hasFluxAdvective() const { return adv_.hasFluxAdvective(); }
  bool hasFluxViscous() const { return visc_.hasFluxViscous(); }
  bool hasSource() const { return source_.hasSourceTerm(); }
  bool hasSourceTrace() const { return source_.hasSourceTrace(); }
  bool hasSourceLiftedQuantity() const { return false; }
  bool hasForcingFunction() const { return force_ != NULL && force_->hasForcingFunction(); }

  // viscous flux is of the form K(q).gradu
  bool fluxViscousLinearInGradient() const { return visc_.fluxViscousLinearInGradient(); }
  bool needsSolutionGradientforSource() const { return source_.needsSolutionGradientforSource(); }

  const AdvectiveFlux& getAdvectiveFlux() const { return adv_; }
  const ViscousFlux& getViscousFlux() const { return visc_; }

  // temporal flux flux: Ft(Q)
  template <class T, class Tf>
  void fluxAdvectiveTime(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tf>& ft ) const
  {
    T ftLocal = 0;
    masterState(x, time, q, ftLocal);
    ft += ftLocal;
  }

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <class T>
  void jacobianFluxAdvectiveTime(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& J ) const
  {
    J += DLA::Identity();
  }

  // master state: U(Q)
  template <class T, class Tu>
  void masterState(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tu>& uCons ) const;


  // master state: U(Q)
  template<class Tq, class Tqp, class Tf>
  void perturbedMasterState(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& dq, ArrayQ<Tf>& du ) const { du = dq; }

  // master state Gradient: Ux(Q)
  template<class Tq, class Tg, class Tf>
  void masterStateGradient(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Tf>& uConsx  ) const
  {
    uConsx = qx;
  }

  // master state Hessian: Uxx(Q), Uxy(Q), Uyy(Q)
  template<class Tq, class Tg, class Th, class Tf>
  void masterStateHessian(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      const ArrayQ<Th>& qxx, ArrayQ<Tf>& uConsxx ) const
  {
    uConsxx = qxx;
  }

  // jacobian of master state wrt q: dU(Q)/dQ
  template<class T>
  void jacobianMasterState(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const;

  // unsteady conservative flux: d(U)/d(t)
  template <class Tq, class Tg, class Tf>
  void strongFluxAdvectiveTime(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qt, ArrayQ<Tf>& dudt ) const;

  // advective flux: F(Q)
  template <class Tq, class Tf>
  void fluxAdvective(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, ArrayQ<Tf>& f ) const;

  template <class Tq, class Tf>
  void fluxAdvectiveUpwind(
      const Real& x, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, ArrayQ<Tf>& f, const Real& scale = 1 ) const;

  template <class Tq, class Tf>
  void fluxAdvectiveUpwindSpaceTime(
      const Real& x, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, const Real& nt, ArrayQ<Tf>& f, const Real& scale = 1 ) const;

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class Tq, class Tf>
  void jacobianFluxAdvective(
      const Real& x,  const Real& time, const ArrayQ<Tq>& q,
      MatrixQ<Tf>& ax ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const Real& nx,
      MatrixQ<Tf>& a ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Real& x, const Real& time, const ArrayQ<Tq>& q, const Real& nx, const Real& nt,
      MatrixQ<Tf>& a ) const;

  // strong form advective fluxes
  template <class Tq, class Tg, class Tf>
  void strongFluxAdvective(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Tf>& strongPDE ) const;

  // viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Tf>& f ) const;

  // space-time viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const;

  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
      const Real& nx,
      ArrayQ<Tf>& f ) const;

  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qtL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qtR,
      const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const;

  // perturbation to viscous flux from dux, duy
  template <class Tq, class Tg, class Tgu, class Tf>
  void perturbedGradFluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      const ArrayQ<Tgu>& dqx,
      ArrayQ<Tf>& df ) const;

  // perturbation to viscous flux from dux, duy
  template <class Tk, class Tgu, class Tf>
  void perturbedGradFluxViscous(
      const Real& x, const Real& time,
      const MatrixQ<Tk>& kxx,
      const ArrayQ<Tgu>& dux,
      ArrayQ<Tf>& df ) const;

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Tk>& kxx) const;

  // space-time viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscousSpaceTime(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxt,
      MatrixQ<Tk>& ktx, MatrixQ<Tk>& ktt ) const;

  template <class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Th>& qxx, MatrixQ<Tk>& kxx_x ) const;

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Tf>& ax ) const;

  template<class Tq, class Tg, class Tf>
  void jacobianFluxViscousSpaceTime(
      const Real &x, const Real &time,
      const ArrayQ<Tq> &q, const ArrayQ<Tg> &qx, const ArrayQ<Tg> &qt,
      MatrixQ<Tf> &ax, MatrixQ<Tf> &at) const
  {
    jacobianFluxViscous(x, time, q, qx, ax);
  }

  // strong form viscous fluxes
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      const ArrayQ<Th>& qxx,
      ArrayQ<Tf>& strongPDE ) const;

  // space-time strong form viscous fluxes
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscousSpaceTime(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qtx, const ArrayQ<Th>& qtt,
      ArrayQ<Tf>& strongPDE ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial strongFluxViscous
    strongFluxViscous(x, time, q, qx, qxx, strongPDE);
  }

  // solution-dependent source: S(X, Q, QX)
  template <class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Ts>& source ) const;

  // Forward call to S(Q+QP, QX+QPX)
  template <class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void source(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tgp>& qpx,
      ArrayQ<Ts>& src ) const
  {
    typedef typename promote_Surreal<Tq, Tqp>::type Tqq;
    typedef typename promote_Surreal<Tg, Tgp>::type Tgg;

    ArrayQ<Tqq> qq = q + qp;
    ArrayQ<Tgg> qqx = qx + qpx;

    source(x, time, qq, qqx, src);
  }

  // Forward call to S(Q,QP, QX,QPX)
  template <class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void sourceCoarse(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tgp>& qpx,
      ArrayQ<Ts>& src ) const
  {
    source(x, time, q, qp, qx, qpx, src);
  }

  // Forward call to S(Q,QP, QX,QPX)
  template <class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void sourceFine(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tgp>& qpx,
      ArrayQ<Ts>& src ) const
  {
    source(x, time, q, qp, qx, qpx, src);
  }

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX)
  template <class Tlq, class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& time,
      const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Ts>& s ) const
  {
    source(x, time, q, qx, s); //forward to regular source
  }

  // linear change in source in response to linear perturbations du, dux, duy
  template <class Tq, class Tg, class Tu, class Tgu, class Ts>
  void perturbedSource(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      const ArrayQ<Tu>& du, const ArrayQ<Tgu>& dux, ArrayQ<Ts>& dS ) const;

  // dual-consistent source
  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& xL, const Real& xR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const;

  // trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
  template <class Tq, class Ts>
  void sourceLiftedQuantity(
      const Real& xL, const Real& xR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, Ts& s ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianSource(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Ts>& dsdu ) const;

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianSourceHACK(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Ts>& dsdu ) const
  {
    jacobianSource(x, time, q, qx, dsdu);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Ts>& dsdu ) const;

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSource(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Ts>& dsdux ) const;

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSourceHACK(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Ts>& dsdux ) const
  {
    jacobianGradientSource(x, time, q, qx, dsdux);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSourceAbsoluteValue(
      const Real& x, const Real& time, const Real& nx,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Ts>& dsdux ) const;

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Th, class Ts>
  void jacobianGradientSourceGradient(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Th>& qxx,
      MatrixQ<Ts>& dsdux_x ) const;

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Th, class Ts>
  void jacobianGradientSourceGradientHACK(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Th>& qxx,
      MatrixQ<Ts>& dsdux_x ) const
  {
    jacobianGradientSourceGradient(x, time, q, qx, qxx, dsdux_x);
  }

  // right-hand-side forcing function
  void forcingFunction( const Real& x, const Real& time, ArrayQ<Real>& force ) const;

  // characteristic speed (needed for timestep)
  template <class Tq, class Tc>
  void speedCharacteristic(
      const Real& x, const Real& time,
      const Real& dx, const ArrayQ<Tq>& q, Tc& speed ) const;

  // characteristic speed
  template <class Tq, class Tc>
  void speedCharacteristic(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, Tc& speed ) const;

  // update fraction needed for physically valid state
  void updateFraction(
      const Real& x, const Real& time, const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
      const Real maxChangeFraction, Real& updateFraction ) const;

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const;

  // set from primitive variable array
  template <class T>
  void setDOFFrom(
      ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const;

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const;

  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const;

  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const;

  // how many residual equations are we dealing with
  int nMonitor() const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const AdvectiveFlux adv_;
  const ViscousFlux visc_;
  const Source source_;
  const std::shared_ptr<ForcingFunctionType> force_;
};

template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class T, class Tu>
inline void
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::masterState(
    const Real& x, const Real& time,
    const ArrayQ<T>& q, ArrayQ<Tu>& uCons ) const
{
  uCons = q;
}

template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class T>
inline void
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::jacobianMasterState(
    const Real& x, const Real& time,
    const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
{
  dudq = DLA::Identity();
}


template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Tf>
inline void
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::strongFluxAdvectiveTime(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qt, ArrayQ<Tf>& dudt ) const
{
  dudt += qt;
}

template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tf>
inline void
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::fluxAdvective(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& q, ArrayQ<Tf>& f ) const
{
  adv_.flux( x, time, q, f );
}

template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tf>
inline void
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::fluxAdvectiveUpwind(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, const Real& nx,
    ArrayQ<Tf>& f, const Real& scale ) const
{
  adv_.fluxUpwind(x, time, qL, qR, nx, f, scale);
}

template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tf>
inline void
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::fluxAdvectiveUpwindSpaceTime(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, const Real& nx, const Real& nt,
    ArrayQ<Tf>& f, const Real& scale ) const
{
  adv_.fluxUpwindSpaceTime(x, time, qL, qR, nx, nt, f);
}


// advective flux jacobian: d(F)/d(U)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tf>
inline void
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::jacobianFluxAdvective(
    const Real& x, const Real& time, const ArrayQ<Tq>& q,
    MatrixQ<Tf>& ax ) const
{
  adv_.jacobian( x, time, q, ax );
}


// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tf>
inline void
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::jacobianFluxAdvectiveAbsoluteValue(
  const Real& x, const Real& time,
  const ArrayQ<Tq>& q, const Real& nx, MatrixQ<Tf>& mtx ) const
{
  MatrixQ<Tq> ax = 0;

  jacobianFluxAdvective(x, time, q, ax);

  Tq an = nx*ax;

  mtx += fabs( an );
}


// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tf>
inline void
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::jacobianFluxAdvectiveAbsoluteValueSpaceTime(
  const Real& x, const Real& time,
  const ArrayQ<Tq>& q, const Real& nx, const Real& nt, MatrixQ<Tf>& mtx ) const
{
  MatrixQ<Tq> ax = 0;

  jacobianFluxAdvective(x, time, q, ax);

  //TODO: Rather arbitrary smoothing constants here
  //Real eps = 1e-8;

  Tq an = nx*ax + nt*1;

  //mtx += smoothabs0( an, eps );
  mtx += fabs( an );
}


// strong form of advective flux: d(F)/d(X)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Tf>
inline void
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::strongFluxAdvective(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    ArrayQ<Tf>& strongPDE ) const
{
  adv_.strongFlux( x, time, q, qx, strongPDE );
}


// viscous flux
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Tf>
inline void
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::fluxViscous(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    ArrayQ<Tf>& f ) const
{
  visc_.fluxViscous( x, time, q, qx, f );
}

// space-time viscous flux
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Tf>
inline void
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::fluxViscousSpaceTime(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
    ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
{
  //Not adding temporal diffusion, so just forward the call to spatial viscous flux
  fluxViscous(x, time, q, qx, f);
}


// viscous flux: normal flux with left and right states
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Tf>
inline void
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::fluxViscous(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
    const Real& nx, ArrayQ<Tf>& f ) const
{
#if 1
  ArrayQ<Tf> fL = 0, fR = 0;

  fluxViscous(x, time, qL, qxL, fL);
  fluxViscous(x, time, qR, qxR, fR);

  f += 0.5*(fL + fR)*nx;

#else
  ArrayQ<Tq> q = 0.5*(qL + qR);
  ArrayQ<Tg> qx = 0.5*(qxL + qxR);

  MatrixQ<Tq> kxx = 0.0;

  visc_.diffusionViscous( x, time, q, kxx );

  f -= nx*kxx*qx;
#endif
}


// viscous flux
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Tgu, class Tf>
inline void
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::perturbedGradFluxViscous(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tgu>& dqx,
    ArrayQ<Tf>& df ) const
{
  fluxViscous(x, time, q, dqx, df);
}


// viscous flux
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tk, class Tgu, class Tf>
inline void
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::perturbedGradFluxViscous(
    const Real& x, const Real& time,
    const MatrixQ<Tk>& kxx, const ArrayQ<Tgu>& dux,
    ArrayQ<Tf>& f ) const
{
  f -= kxx*dux;
}

// space-time viscous flux: normal flux with left and right states
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Tf>
inline void
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::fluxViscousSpaceTime(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qtL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qtR,
    const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const
{
  //Not adding temporal diffusion, so just forward the call to spatial viscous flux
  fluxViscous( x, time, qL, qxL, qR, qxR, nx, f);
}

// viscous diffusion matrix: d(Fv)/d(UX)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Tk>
inline void
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::diffusionViscous(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    MatrixQ<Tk>& kxx ) const
{
  visc_.diffusionViscous( x, time, q, qx, kxx );
}

// space-time viscous diffusion matrix: d(Fv)/d(UX)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Tk>
inline void
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::diffusionViscousSpaceTime(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
    MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxt,
    MatrixQ<Tk>& ktx, MatrixQ<Tk>& ktt ) const
{
  //Not adding temporal diffusion, so just forward the call to spatial diffusion matrix
  diffusionViscous(x, time, q, qx, kxx);
}


template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Th, class Tk>
void
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::diffusionViscousGradient(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Th>& qxx, MatrixQ<Tk>& kxx_x ) const
{
  visc_.diffusionViscousGradient( x, time, q, qx, qxx, kxx_x );
}

template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Tf>
void
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::jacobianFluxViscous(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& q,
    const ArrayQ<Tg>& qx,
    MatrixQ<Tf>& ax ) const
{
  visc_.jacobianFluxViscous( x, time, q, qx, ax );
}

// strong form of viscous flux: d(Fv)/d(X)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Th, class Tf>
inline void
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::strongFluxViscous(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    const ArrayQ<Th>& qxx,
    ArrayQ<Tf>& strongPDE ) const
{
  visc_.strongFluxViscous( x, time, q, qx, qxx, strongPDE );
}

// solution-dependent source: S(X, Q, QX)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Ts>
inline void
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::source(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    ArrayQ<Ts>& source ) const
{
  source_.source( x, time, q, qx, source );
}


// jacobian of source wrt conservation variables: d(S)/d(U)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Tu, class Tgu, class Ts>
inline void
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::perturbedSource(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    const ArrayQ<Tu>& du, const ArrayQ<Tgu>& dux,
    ArrayQ<Ts>& dS ) const
{
  typedef typename promote_Surreal<Tq,Tg>::type T;

  MatrixQ<T> dsdu = 0;
  jacobianSource(x, time, q, qx, dsdu);

  dS += dsdu*du;

  MatrixQ<T> dsdux =0;
  jacobianGradientSource(x, time, q, qx, dsdux);

  dS += dsdux*dux;
}


// dual-consistent source
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Ts>
inline void
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::sourceTrace(
    const Real& xL, const Real& xR, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
    ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
{
  source_.sourceTrace(xL, xR, time,
                      qL, qxL,
                      qR, qxR,
                      sourceL, sourceR);
}

// jacobian of source wrt conservation variables: d(S)/d(U)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Ts>
inline void
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::jacobianSource(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    MatrixQ<Ts>& dsdu ) const
{
  source_.jacobianSource(x, time, q, qx, dsdu);
}


// jacobian of source wrt conservation variables: d(S)/d(U)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Ts>
inline void
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::jacobianSourceAbsoluteValue(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    MatrixQ<Ts>& dsdu ) const
{
  source_.jacobianSourceAbsoluteValue(x, time, q, qx, dsdu);
}

// jacobian of source wrt conservation variables: d(S)/d(U)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Ts>
inline void
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::jacobianGradientSource(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    MatrixQ<Ts>& dsdux ) const
{
  source_.jacobianGradientSource(x, time, q, qx, dsdux);
}


// jacobian of source wrt conservation variables: d(S)/d(U)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Th, class Ts>
inline void
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::jacobianGradientSourceGradient(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Th>& qxx,
    MatrixQ<Ts>& dsdux_x ) const
{
  source_.jacobianGradientSourceGradient(x, time, q, qx, qxx, dsdux_x);
}


// right-hand-side forcing function
template<class AdvectiveFlux, class ViscousFlux, class Source>
inline void
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::forcingFunction(
    const Real& x, const Real& time, ArrayQ<Real>& forcing ) const
{
  SANS_ASSERT(force_ != NULL);
  ArrayQ<Real> RHS = 0;
  (*force_)(*this, x, time, RHS );
  forcing += RHS;
}


// characteristic speed (needed for timestep)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tc>
inline void
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::speedCharacteristic(
    const Real& x, const Real& time,
    const Real& dx, const ArrayQ<Tq>& q, Tc& speed ) const
{
  MatrixQ<Tq> ax = 0;
  //Real eps = 1e-9;

  jacobianFluxAdvective(x, time, q, ax);

  Tq uh = dx*ax;

  speed = fabs(uh)/sqrt(dx*dx); // So this 'sqrt(dx*dx)' should be unnecessary
}


// characteristic speed
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tc>
inline void
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::speedCharacteristic(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& q, Tc& speed ) const
{
  MatrixQ<Tq> ax = 0;
  //Real eps = 1e-9;

  jacobianFluxAdvective(x, time, q, ax);

  speed = fabs(ax);
}

// update fraction needed for physically valid state
template<class AdvectiveFlux, class ViscousFlux, class Source>
void
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::updateFraction(
    const Real& x, const Real& time, const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
    const Real maxChangeFraction, Real& updateFraction ) const
{
  SANS_DEVELOPER_EXCEPTION("Not tested");
#if 0
  // Limit the update
  ArrayQ<Real> nq = q - dq;

  updateFraction = 1;

  //q - updateFraction*dq >= (1-maxChangeFraction)*q
  if (nq < (1-maxChangeFraction)*q)
    updateFraction =  maxChangeFraction*q/dq;

  //q - updateFraction*dq <= (1+maxChangeFraction)*q
  if (nq > (1+maxChangeFraction)*q)
    updateFraction = -maxChangeFraction*q/dq;
#endif
}

// is state physically valid
template<class AdvectiveFlux, class ViscousFlux, class Source>
inline bool
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::isValidState( const ArrayQ<Real>& q ) const
{
  return adv_.isValidState(q) && visc_.isValidState(q) && source_.isValidState(q);
}


// set from primitive variable array
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class T>
void
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::setDOFFrom(
    ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn >= N);
  q = data[0];
}

// interpret residuals of the solution variable
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class T>
void
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::interpResidVariable(
    const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
{
  SANS_ASSERT(rsdPDEOut.m() == 1);
  rsdPDEOut[0] = rsdPDEIn;
}

// interpret residuals of the gradients in the solution variable
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class T>
void
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::interpResidGradient(
    const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const
{
  SANS_ASSERT(rsdAuxOut.m() == 1);
  rsdAuxOut[0] = rsdAuxIn;
}

// interpret residuals at the boundary (should forward to BCs)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class T>
void
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::interpResidBC(
    const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const
{
  SANS_ASSERT(rsdBCOut.m() == 1);
  rsdBCOut[0] = rsdBCIn;
}

// how many residual equations are we dealing with
template<class AdvectiveFlux, class ViscousFlux, class Source>
inline int
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::nMonitor() const
{
  return 1;
}

template<class AdvectiveFlux, class ViscousFlux, class Source>
void
PDEAdvectionDiffusion<PhysD1, AdvectiveFlux, ViscousFlux, Source>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PDEAdvectionDiffusion1D: adv_ =" << std::endl;
  adv_.dump(indentSize+2, out);
  out << indent << "PDEAdvectionDiffusion1D: visc_ =" << std::endl;
  visc_.dump(indentSize+2, out);
  out << indent << "PDEAdvectionDiffusion1D: force_ =" << std::endl;
  if ( force_ != NULL )
    (*force_).dump(indentSize+2, out);
  else
    out << " NULL" << std::endl;
}

} //namespace SANS

#endif  // PDEADVECTIONDIFFUSION1D_H
