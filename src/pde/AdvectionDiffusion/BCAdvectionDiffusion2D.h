// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCADVECTIONDIFFUSION2D_H
#define BCADVECTIONDIFFUSION2D_H

// 2-D Advection-Diffusion BC class

//PyDict must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/BCCategory.h"
#include "pde/BCNone.h"
#include "Topology/Dimension.h"
#include "AdvectionDiffusion_Traits.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "ViscousFlux2D.h"

#include <iostream>

#include <boost/mpl/vector.hpp>

namespace SANS
{

//----------------------------------------------------------------------------//
// BC class: 2-D Advection-Diffusion
//
// template parameters:
//   T                    solution DOF data type (e.g. double)
//   BCType               BC type (e.g. BCTypeLinearRobin)
//----------------------------------------------------------------------------//

template <class PhysDim, class BCType>
class BCAdvectionDiffusion;

template <class PhysDim, class BCType>
struct BCAdvectionDiffusionParams;


//----------------------------------------------------------------------------//
// BC types
//----------------------------------------------------------------------------//

template<class AdvectiveFlux, class ViscousFlux>
class BCTypeTimeOut;
template<class AdvectiveFlux, class ViscousFlux>
class BCTypeTimeIC;
template<class AdvectiveFlux, class ViscousFlux>
class BCTypeTimeIC_Function;
class BCTypeNatural;
//class BCTypeStrong;
class BCTypeLinearRobin_mitLG;
class BCTypeLinearRobin_sansLG;

class BCTypeDirichlet_mitLG;

template<class AdvectiveFlux, class ViscousFlux>
class BCTypeDirichlet_mitState;

template<class AdvectiveFlux, class ViscousFlux>
class BCTypeFlux;
class BCTypeFluxParams;

template<class AdvectiveFlux, class ViscousFlux>
class BCTypeFunction_mitState;
//class BCTypeFunction_Strong;
class BCTypeFunctionLinearRobin_mitLG;
class BCTypeFunctionLinearRobin_sansLG;
class BCTypeFunctionDirichlet_mitLG;


// Define the vector of boundary conditions
template<class AdvectiveFlux, class ViscousFlux>
using BCAdvectionDiffusion2DVector = boost::mpl::vector14< BCNone<PhysD2,AdvectionDiffusionTraits<PhysD2>::N>,
    SpaceTimeBC<BCAdvectionDiffusion<PhysD2, BCTypeTimeOut<AdvectiveFlux, ViscousFlux>>>,
    SpaceTimeBC<BCAdvectionDiffusion<PhysD2, BCTypeTimeIC<AdvectiveFlux, ViscousFlux>>>,
    SpaceTimeBC<BCAdvectionDiffusion<PhysD2, BCTypeTimeIC_Function<AdvectiveFlux, ViscousFlux>>>,
    BCAdvectionDiffusion<PhysD2, BCTypeNatural>,
    BCAdvectionDiffusion<PhysD2, BCTypeFlux<AdvectiveFlux,ViscousFlux>>,
    BCAdvectionDiffusion<PhysD2, BCTypeLinearRobin_mitLG>,
    BCAdvectionDiffusion<PhysD2, BCTypeLinearRobin_sansLG>,
    BCAdvectionDiffusion<PhysD2, BCTypeDirichlet_mitLG>,
    BCAdvectionDiffusion<PhysD2, BCTypeDirichlet_mitState<AdvectiveFlux,ViscousFlux>>,
    BCAdvectionDiffusion<PhysD2, BCTypeFunction_mitState<AdvectiveFlux,ViscousFlux>>,
    BCAdvectionDiffusion<PhysD2, BCTypeFunctionLinearRobin_mitLG>,
    BCAdvectionDiffusion<PhysD2, BCTypeFunctionLinearRobin_sansLG>,
    BCAdvectionDiffusion<PhysD2, BCTypeFunctionDirichlet_mitLG>
>;


//----------------------------------------------------------------------------//
// time out boundary condition
class BCTypeTimeOutParam;

template <>
struct BCAdvectionDiffusionParams<PhysD2, BCTypeTimeOutParam> : noncopyable
{
  static constexpr const char * BCName{"TimeOut"};
  struct Option
  {
    const DictOption TimeOut{BCAdvectionDiffusionParams::BCName, BCAdvectionDiffusionParams::checkInputs};
  };

  static void checkInputs(PyDict d);
  static BCAdvectionDiffusionParams params;
};

template<class AdvectiveFlux, class ViscousFlux>
class BCAdvectionDiffusion<PhysD2, BCTypeTimeOut<AdvectiveFlux, ViscousFlux>> :
public BCType<BCAdvectionDiffusion<PhysD2, BCTypeTimeOut<AdvectiveFlux, ViscousFlux>>>
{
public:

  typedef PhysD2 PhysDim;
  typedef BCTypeTimeOut<AdvectiveFlux, ViscousFlux> BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCAdvectionDiffusionParams<PhysD2, BCTypeTimeOutParam> ParamsType;

  static const int D= AdvectionDiffusionTraits<PhysD2>::D;        // physical dimensions
  static const int N= AdvectionDiffusionTraits<PhysD2>::N;        // total solution variables

  static const int NBC= 0;        // total BCs

  template <class T>
  using ArrayQ= AdvectionDiffusionTraits<PhysD2>::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using MatrixQ= AdvectionDiffusionTraits<PhysD2>::template MatrixQ<T>;   // matrices

  template <class Source>
  BCAdvectionDiffusion(const PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source> &pde, const PyDict &d) :
  adv_(pde.getAdvectiveFlux()), visc_(pde.getViscousFlux())
  {}
  BCAdvectionDiffusion() {}

  virtual ~BCAdvectionDiffusion() {}

  BCAdvectionDiffusion(const BCAdvectionDiffusion &)= delete;
  BCAdvectionDiffusion &operator= (const BCAdvectionDiffusion &)= delete;

  // is there a viscous flux on the boundary
  bool hasFluxViscous() const
  {
    return false;
  }

  // BC data
  template <class T>
  void state(const Real &x, const Real &y, const Real &time,
             const Real &nx, const Real &ny, const Real &nt,
             const ArrayQ<T> &qI, ArrayQ<T> &qB) const
  {
    qB= qI;     // sets boundary state to interior state (outflow)
  }

  // BC data
  template <class Tp, class T>
  void state(const Tp &param, const Real &x, const Real &y, const Real &time,
             const Real &nx, const Real &ny, const Real &nt,
             const ArrayQ<T> &qI, ArrayQ<T> &qB) const
  {
    state(x, y, time, nx, ny, nt, qI, qB);
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormalSpaceTime(const Real &x, const Real &y, const Real &time,
                           const Real &nx, const Real &ny, const Real &nt,
                           const ArrayQ<T> &qI, const ArrayQ<T> &qIx, const ArrayQ<T> &qIy,
                           const ArrayQ<T> &qIt, const ArrayQ<T> &qB, ArrayQ<Tf> &Fn) const
  {
    adv_.fluxUpwindSpaceTime(x, y, time, qI, qB, nx, ny, nt, Fn);
  }

  // normal BC flux
  template <class T, class Tp, class Tf>
  void fluxNormalSpaceTime(const Tp &param, const Real &x, const Real &y, const Real &time,
                           const Real &nx, const Real &ny, const Real &nt,
                           const ArrayQ<T> &qI, const ArrayQ<T> &qIx, const ArrayQ<T> &qIy,
                           const ArrayQ<T> &qIt, const ArrayQ<T> &qB, ArrayQ<Tf> &Fn) const
  {
    adv_.fluxUpwindSpaceTime(x, y, time, qI, qB, nx, ny, nt, Fn);
  }

  bool isValidState(const Real &nx, const Real &ny, const Real &nt, const ArrayQ<Real> &qI) const
  {
    return true;
  }

  void dump(int indentSize, std::ostream &out= std::cout) const;

protected:
  const AdvectiveFlux& adv_;
  const ViscousFlux& visc_;
};

// time IC boundary condition
class BCTypeTimeICParam;

template <>
struct BCAdvectionDiffusionParams<PhysD2, BCTypeTimeICParam> : noncopyable
{
  const ParameterNumeric<Real> qB{"qB", NO_DEFAULT, NO_RANGE, "BC state"};

  static constexpr const char * BCName{"TimeIC"};
  struct Option
  {
    const DictOption TimeIC{BCAdvectionDiffusionParams::BCName, BCAdvectionDiffusionParams::checkInputs};
  };

  static void checkInputs(PyDict d);
  static BCAdvectionDiffusionParams params;
};

template <class AdvectiveFlux, class ViscousFlux>
class BCAdvectionDiffusion<PhysD2, BCTypeTimeIC<AdvectiveFlux, ViscousFlux>> :
public BCType<BCAdvectionDiffusion<PhysD2, BCTypeTimeIC<AdvectiveFlux, ViscousFlux>>>
{
public:

  typedef PhysD2 PhysDim;
  typedef BCTypeTimeIC<AdvectiveFlux, ViscousFlux> BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCAdvectionDiffusionParams<PhysD2, BCTypeTimeICParam> ParamsType;

  static const int D= AdvectionDiffusionTraits<PhysD2>::D;      // physical dimensions
  static const int N= AdvectionDiffusionTraits<PhysD2>::N;      // total solution variables

  static const int NBC= 1;        // total BCs

  template <class T>
  using ArrayQ= AdvectionDiffusionTraits<PhysD2>::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using MatrixQ= AdvectionDiffusionTraits<PhysD2>::template MatrixQ<T>;   // matrices

  template <class Source>
  BCAdvectionDiffusion(const PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source> &pde, const Real &data) :
  adv_(pde.getAdvectiveFlux()),
  visc_(pde.getViscousFlux()),
  qB_(data)
  {}

  template <class Source>
  BCAdvectionDiffusion(const PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source> &pde, const PyDict &d) :
  adv_(pde.getAdvectiveFlux()),
  visc_(pde.getViscousFlux()),
  qB_(d.get(ParamsType::params.qB))
  {}

  virtual ~BCAdvectionDiffusion() {}

  BCAdvectionDiffusion(const BCAdvectionDiffusion &)= delete;
  BCAdvectionDiffusion &operator= (const BCAdvectionDiffusion &)= delete;

  // is there a viscous flux on the boundary?
  bool hasFluxViscous() const
  {
    return false;
  }

  // BC data
  template <class T>
  void state(const Real &x, const Real &y, const Real &time,
             const Real &nx, const Real &ny, const Real &nt,
             const ArrayQ<T> &qI, ArrayQ<T> &qB) const
  {
    qB= qB_;
  }

  // BC data
  template <class Tp, class T>
  void state(const Tp &param, const Real &x, const Real &y, const Real &time,
             const Real &nx, const Real &ny, const Real &nt,
             const ArrayQ<T> &qI, ArrayQ<T> &qB) const
  {
    state(x, y, time, nx, ny, nt, qI, qB);
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormalSpaceTime(const Real &x, const Real &y, const Real &time,
                           const Real &nx, const Real &ny, const Real &nt,
                           const ArrayQ<T> &qI, const ArrayQ<T> &qIx, const ArrayQ<T> &qIy,
                           const ArrayQ<T> &qIt, const ArrayQ<T> &qB, ArrayQ<Tf> &Fn) const
  {
    adv_.fluxUpwindSpaceTime(x, y, time, qI, qB, nx, ny, nt, Fn);
  }

  // normal BC flux
  template <class T, class Tp, class Tf>
  void fluxNormalSpaceTime(const Tp &param, const Real &x, const Real &y, const Real &time,
                           const Real &nx, const Real &ny, const Real &nt,
                           const ArrayQ<T> &qI, const ArrayQ<T> &qIx, const ArrayQ<T> &qIy,
                           const ArrayQ<T> &qIt, const ArrayQ<T> &qB, ArrayQ<Tf> &Fn) const
  {
    adv_.fluxUpwindSpaceTime(x, y, time, qI, qB, nx, ny, nt, Fn);
  }

  bool isValidState(const Real &nx, const Real &ny, const Real &nt, const ArrayQ<Real> &qI) const
  {
    return true;
  }

  void dump(int indentSize, std::ostream &out= std::cout) const;

protected:
  const AdvectiveFlux &adv_;
  const ViscousFlux &visc_;
  const ArrayQ<Real> qB_;
};

// time in boundary condition with function!
class BCTypeTimeIC_FunctionParam;

template <>
struct BCAdvectionDiffusionParams<PhysD2, BCTypeTimeIC_FunctionParam> : ScalarFunction2DParams
{
  static constexpr const char *BCName{"TimeIC_Function"};
  struct Option
  {
    const DictOption TimeIC_Function{BCAdvectionDiffusionParams::BCName, BCAdvectionDiffusionParams::checkInputs};
  };

  static void checkInputs(PyDict d);
  static BCAdvectionDiffusionParams params;
};

template <class AdvectiveFlux, class ViscousFlux>
class BCAdvectionDiffusion<PhysD2, BCTypeTimeIC_Function<AdvectiveFlux, ViscousFlux>> :
public BCType<BCAdvectionDiffusion<PhysD2, BCTypeTimeIC_Function<AdvectiveFlux, ViscousFlux>>>
{
public:

  typedef PhysD2 PhysDim;
  typedef BCTypeTimeIC_Function<AdvectiveFlux, ViscousFlux> BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCAdvectionDiffusionParams<PhysD2, BCTypeTimeIC_FunctionParam> ParamsType;

  static const int D= AdvectionDiffusionTraits<PhysD2>::D;     // physical dimensions
  static const int N= AdvectionDiffusionTraits<PhysD2>::N;     // total solution variables

  static const int NBC= 1;        // total BCs

  template <class T>
  using ArrayQ= AdvectionDiffusionTraits<PhysD2>::template ArrayQ<T>;      // solution arrays

  template <class T>
  using MatrixQ= AdvectionDiffusionTraits<PhysD2>::template MatrixQ<T>;    // matrices

  typedef std::shared_ptr<Function2DBase<ArrayQ<Real>>> Function_ptr;

  template <class Source>
  BCAdvectionDiffusion(const PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source> &pde, const PyDict &d) :
  uexact_(ParamsType::getFunction(d)),
  adv_(pde.getAdvectiveFlux()),
  visc_(pde.getViscousFlux()) {}

  template <class Source>
  BCAdvectionDiffusion(const PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source> &pde, const Function_ptr &uexact) :
  uexact_(uexact),
  adv_(pde.getAdvectiveFlux()),
  visc_(pde.getViscousFlux()) {}

  virtual ~BCAdvectionDiffusion() {}

  BCAdvectionDiffusion(const BCAdvectionDiffusion &)= delete;
  BCAdvectionDiffusion &operator= (const BCAdvectionDiffusion &)= delete;

  // is there a viscous flux on the boundary
  bool hasFluxViscous() const
  {
    return false;
  }

  // BC data
  template <class T>
  void state(const Real &x, const Real &y, const Real &time,
             const Real &nx, const Real &ny, const Real &nt,
             const ArrayQ<T> &qI, ArrayQ<T> &qB) const
  {
    qB= (*uexact_)(x, y, time);
  }

  // BC data
  template <class T, class Tp>
  void state(const Tp& param, const Real &x, const Real &y, const Real &time,
             const Real &nx, const Real &ny, const Real &nt,
             const ArrayQ<T> &qI, ArrayQ<T> &qB) const
  {
    state(x, y, time, nx, ny, nt, qI, qB);
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormalSpaceTime(const Real &x, const Real &y, const Real &time,
                           const Real &nx, const Real &ny, const Real &nt,
                           const ArrayQ<T> &qI,
                           const ArrayQ<T> &qIx, const ArrayQ<T> &qIy, const ArrayQ<T> &qIt,
                           const ArrayQ<T> &qB,
                           ArrayQ<Tf> &Fn) const
  {
    adv_.fluxUpwindSpaceTime(x, y, time, qI, qB, nx, ny, nt, Fn);
  }

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormalSpaceTime(const Tp &param, const Real &x, const Real &y, const Real &time,
                           const Real &nx, const Real &ny, const Real &nt,
                           const ArrayQ<T> &qI,
                           const ArrayQ<T> &qIx, const ArrayQ<T> &qIy, const ArrayQ<T> &qIt,
                           const ArrayQ<T> &qB,
                           ArrayQ<Tf> &Fn) const
  {
    adv_.fluxUpwindSpaceTime(x, y, time, qI, qB, nx, ny, nt, Fn);
  }

  // is the boundary state valid
  bool isValidState(const Real &nx, const Real &ny, const Real &nt, const ArrayQ<Real> &q) const
  {
    return true;
  }

  void dump(int indentSize, std::ostream &out= std::cout);

protected:
  const Function_ptr uexact_;
  const AdvectiveFlux &adv_;
  const ViscousFlux &visc_;
};

//----------------------------------------------------------------------------//
// Robin BC:  A u + B (kn un + ks us) = bcdata
//
// coefficients A and B are arbitrary with the exception that they cannot
// simultaneously vanish
template<>
struct BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_mitLG> : noncopyable
{
  const ParameterNumeric<Real> A{"A", NO_DEFAULT, NO_RANGE, "Value coefficient"};
  const ParameterNumeric<Real> B{"B", NO_DEFAULT, NO_RANGE, "Viscous coefficient"};
  const ParameterNumeric<Real> bcdata{"bcdata", NO_DEFAULT, NO_RANGE, "BC data"};

  static constexpr const char* BCName{"LinearRobin_mitLG"};
  struct Option
  {
    const DictOption LinearRobin_mitLG{BCAdvectionDiffusionParams::BCName, BCAdvectionDiffusionParams::checkInputs};
  };

  static void checkInputs(PyDict d);
  static BCAdvectionDiffusionParams params;
};

template<>
struct BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG> : noncopyable
{
  const ParameterNumeric<Real> A{"A", NO_DEFAULT, NO_RANGE, "Value coefficient"};
  const ParameterNumeric<Real> B{"B", NO_DEFAULT, NO_RANGE, "Viscous coefficient"};
  const ParameterNumeric<Real> bcdata{"bcdata", NO_DEFAULT, NO_RANGE, "BC data"};

  static constexpr const char* BCName{"LinearRobin_sansLG"};
  struct Option
  {
    const DictOption LinearRobin_sansLG{BCAdvectionDiffusionParams::BCName, BCAdvectionDiffusionParams::checkInputs};
  };

  static void checkInputs(PyDict d);
  static BCAdvectionDiffusionParams params;
};

template <class PhysDim>
class BCAdvectionDiffusion_LinearRobinBase;

template <>
class BCAdvectionDiffusion_LinearRobinBase<PhysD2>
{
public:
  typedef PhysD2 PhysDim;
  static const int D = AdvectionDiffusionTraits<PhysD2>::D;   // physical dimensions
  static const int N = AdvectionDiffusionTraits<PhysD2>::N;   // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD2>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD2>::template MatrixQ<T>; // matrices

  BCAdvectionDiffusion_LinearRobinBase( const Real& A, const Real& B, const Real& bcdata ) :
    A_(A), B_(B), bcdata_(bcdata) {}

  virtual ~BCAdvectionDiffusion_LinearRobinBase() {}

  // BC coefficients:  A u + B (kn un + ks us)
  template <class T>
  void coefficients(
      const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      MatrixQ<T>& A, MatrixQ<T>& B ) const;

  template <class T, class L>
  void coefficients(
      const L& param, const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      MatrixQ<T>& A, MatrixQ<T>& B ) const;

  // BC data
  template <class T>
  void data( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny, ArrayQ<T>& bcdata ) const;

  template <class T, class L>
  void data( const L& param, const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny, ArrayQ<T>& bcdata ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  MatrixQ<Real> A_;
  MatrixQ<Real> B_;
  ArrayQ<Real> bcdata_;
};

template <class T>
inline void
BCAdvectionDiffusion_LinearRobinBase<PhysD2>::coefficients(
    const Real&, const Real&, const Real&, const Real&, const Real&,
    MatrixQ<T>& A, MatrixQ<T>& B ) const
    {
  A = A_;
  B = B_;
    }

template <class T, class L>
inline void
BCAdvectionDiffusion_LinearRobinBase<PhysD2>::coefficients(
    const L& param, const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    MatrixQ<T>& A, MatrixQ<T>& B ) const
    {
  coefficients(x, y, time, nx, ny, A, B);
    }

template <class T>
inline void
BCAdvectionDiffusion_LinearRobinBase<PhysD2>::data(
    const Real&, const Real&, const Real&, const Real& nx, const Real& ny, ArrayQ<T>& bcdata ) const
    {
  bcdata = bcdata_;
    }

template <class T, class L>
inline void
BCAdvectionDiffusion_LinearRobinBase<PhysD2>::data(
    const L& param, const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    ArrayQ<T>& bcdata ) const
    {
  data(x, y, time, nx, ny, bcdata);
    }

template <>
class BCAdvectionDiffusion<PhysD2, BCTypeLinearRobin_mitLG> : public BCType< BCAdvectionDiffusion<PhysD2, BCTypeLinearRobin_mitLG> >,
public BCAdvectionDiffusion_LinearRobinBase<PhysD2>
{
public:
  typedef BCAdvectionDiffusion_LinearRobinBase<PhysD2> BaseType;
  typedef BCCategory::LinearScalar_mitLG Category;
  typedef BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_mitLG> ParamsType;

  template<class AdvectiveFlux, class ViscousFlux, class Source>
  BCAdvectionDiffusion(const PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>& pde, const PyDict& d ) :
  BaseType( d.get(ParamsType::params.A), d.get(ParamsType::params.B), d.get(ParamsType::params.bcdata)) {}

  BCAdvectionDiffusion( const Real& A, const Real& B, const Real& bcdata ) : BaseType( A, B, bcdata ) {}
};

template <>
class BCAdvectionDiffusion<PhysD2, BCTypeLinearRobin_sansLG> : public BCType< BCAdvectionDiffusion<PhysD2, BCTypeLinearRobin_sansLG> >,
public BCAdvectionDiffusion_LinearRobinBase<PhysD2>
{
public:
  typedef BCAdvectionDiffusion_LinearRobinBase<PhysD2> BaseType;
  typedef BCCategory::LinearScalar_sansLG Category;
  typedef BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG> ParamsType;

  template<class AdvectiveFlux, class ViscousFlux, class Source>
  BCAdvectionDiffusion(const PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>& pde, const PyDict& d ) :
  BaseType( d.get(ParamsType::params.A), d.get(ParamsType::params.B), d.get(ParamsType::params.bcdata)) {}

  BCAdvectionDiffusion( const Real& A, const Real& B, const Real& bcdata ) : BaseType( A, B, bcdata ) {}
};

//----------------------------------------------------------------------------//
// Dirichlet mitLG BC u = uB
//

template<>
struct BCAdvectionDiffusionParams<PhysD2, BCTypeDirichlet_mitLG> : noncopyable
{
  const ParameterNumeric<Real> uB{"uB", NO_DEFAULT, NO_RANGE, "Boundary data"};
  const ParameterBool strong{"strong", false, "Strongly impose BC with collocation points"};

  static constexpr const char* BCName{"Dirichlet_mitLG"};
  struct Option
  {
    const DictOption Dirichlet_mitLG{BCAdvectionDiffusionParams::BCName, BCAdvectionDiffusionParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCAdvectionDiffusionParams params;
};

template <>
class BCAdvectionDiffusion<PhysD2, BCTypeDirichlet_mitLG> :
public BCType< BCAdvectionDiffusion<PhysD2, BCTypeDirichlet_mitLG> >
{
public:
  typedef BCCategory::Dirichlet_mitLG Category;
  typedef BCAdvectionDiffusionParams<PhysD2, BCTypeDirichlet_mitLG> ParamsType;

  typedef PhysD2 PhysDim;
  static const int D = AdvectionDiffusionTraits<PhysD2>::D;   // physical dimensions
  static const int N = AdvectionDiffusionTraits<PhysD2>::N;   // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD2>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD2>::template MatrixQ<T>; // matrices

  template <class PDE>
  BCAdvectionDiffusion( const PDE& pde, const PyDict& d ) :
  uB_(d.get(ParamsType::params.uB)),
  isStrongBC_(d.get(ParamsType::params.strong)) {}

  template <class PDE>
  BCAdvectionDiffusion( const PDE& pde, const ArrayQ<Real>& uB, const bool strongBC = false ) :
  uB_(uB), isStrongBC_(strongBC) {}

  virtual ~BCAdvectionDiffusion() {}

  BCAdvectionDiffusion& operator=( const BCAdvectionDiffusion& );

  // Should the BC be strongly imposed with collocation points
  bool isStrongBC() const { return isStrongBC_; }

  // BC lagrange multiplier term
  template <class T>
  void lagrangeTerm( const Real& x, const Real& y, const Real& time,
                     const Real& nx, const Real& ny,
                     const ArrayQ<T>& qI,
                     const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                     const ArrayQ<T>& lg,
                     ArrayQ<T>& lgterm) const
  {
    // Remove the residual using the lagrange multiplier
    lgterm = lg;
  }

  // normal BC flux
  template <class T>
  void strongBC( const Real& x, const Real& y, const Real& time,
                 const Real& nx, const Real& ny,
                 const ArrayQ<T>& qI,
                 const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                 const ArrayQ<T>& lg,
                 ArrayQ<T>& residualBC) const
  {
    // set boundary condition residual
    residualBC = qI - uB_;
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const ArrayQ<Real> uB_;
  const bool isStrongBC_;
};

//----------------------------------------------------------------------------//
// Natural BC < w, Flux. n> = <w,gB>
//

template<>
struct BCAdvectionDiffusionParams<PhysD2, BCTypeNatural> : noncopyable
{
  const ParameterNumeric<Real> gB{"gB", NO_DEFAULT, NO_RANGE, "Boundary flux data"};

  static constexpr const char* BCName{"Natural"};
  struct Option
  {
    const DictOption Natural{BCAdvectionDiffusionParams::BCName, BCAdvectionDiffusionParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCAdvectionDiffusionParams params;
};

template <>
class BCAdvectionDiffusion<PhysD2, BCTypeNatural> :
public BCType< BCAdvectionDiffusion<PhysD2, BCTypeNatural> >
{
public:
  typedef BCCategory::Flux_mitState Category;
  typedef BCAdvectionDiffusionParams<PhysD2, BCTypeNatural> ParamsType;

  typedef PhysD2 PhysDim;
  static const int D = AdvectionDiffusionTraits<PhysD2>::D;   // physical dimensions
  static const int N = AdvectionDiffusionTraits<PhysD2>::N;   // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD2>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD2>::template MatrixQ<T>; // matrices

  template <class PDE>
  BCAdvectionDiffusion( const PDE& pde, const PyDict& d ) :
  gB_(d.get(ParamsType::params.gB)) {}

  template <class PDE>
  BCAdvectionDiffusion( const PDE& pde, const ArrayQ<Real>& gB ) :
  gB_(gB) {}

  virtual ~BCAdvectionDiffusion() {}

  BCAdvectionDiffusion& operator=( const BCAdvectionDiffusion& );

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // BC state
  template <class T>
  void state( const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    qB = qI;
  }

  // normal BC flux
  template <class T>
  void fluxNormal( const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const
  {
    // set boundary flux
    Fn = gB_;
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  ArrayQ<Real> gB_;
};

//----------------------------------------------------------------------------//
// Characteristic Dirichlet BC:  u = bcstate
//
class BCTypeDirichlet_mitStateParam;

template<>
struct BCAdvectionDiffusionParams<PhysD2, BCTypeDirichlet_mitStateParam> : noncopyable
{
  const ParameterNumeric<Real> qB{"qB", NO_DEFAULT, NO_RANGE, "BC state"};

  static constexpr const char* BCName{"Dirichlet_mitState"};
  struct Option
  {
    const DictOption Dirichlet_mitState{BCAdvectionDiffusionParams::BCName, BCAdvectionDiffusionParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCAdvectionDiffusionParams params;
};

template <class AdvectiveFlux, class ViscousFlux>
class BCAdvectionDiffusion<PhysD2, BCTypeDirichlet_mitState<AdvectiveFlux,ViscousFlux>> :
public BCType< BCAdvectionDiffusion<PhysD2, BCTypeDirichlet_mitState<AdvectiveFlux,ViscousFlux>> >
{
public:
  typedef BCCategory::Flux_mitState Category;
  typedef BCAdvectionDiffusionParams<PhysD2, BCTypeDirichlet_mitStateParam> ParamsType;

  typedef PhysD2 PhysDim;
  static const int D = AdvectionDiffusionTraits<PhysD2>::D;   // physical dimensions
  static const int N = AdvectionDiffusionTraits<PhysD2>::N;   // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD2>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD2>::template MatrixQ<T>; // matrices

  template<class Source>
  BCAdvectionDiffusion(const PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>& pde, const PyDict& d ) :
  qB_(d.get(ParamsType::params.qB)),
  adv_( pde.getAdvectiveFlux() ),
  visc_( pde.getViscousFlux() ),
  fluxViscous_(pde.hasFluxViscous()) {}

  template<class Source>
  BCAdvectionDiffusion(const PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>& pde, const ArrayQ<Real>& qB ) :
  qB_(qB),
  adv_( pde.getAdvectiveFlux() ),
  visc_( pde.getViscousFlux() ),
  fluxViscous_(pde.hasFluxViscous()) {}

  virtual ~BCAdvectionDiffusion() {}

  BCAdvectionDiffusion& operator=( const BCAdvectionDiffusion& );

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return fluxViscous_; }

  // BC state
  template <class T>
  void state( const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    qB = qB_;
  }

  // normal BC flux
  template <class T>
  void fluxNormal( const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  ArrayQ<Real> qB_;
  const AdvectiveFlux& adv_;
  const ViscousFlux& visc_;
  bool fluxViscous_;
};

template<class AdvectiveFlux, class ViscousFlux>
template <class T>
inline void
BCAdvectionDiffusion<PhysD2, BCTypeDirichlet_mitState<AdvectiveFlux,ViscousFlux>>::
fluxNormal( const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
            const ArrayQ<T>& qB,
            ArrayQ<T>& Fn) const
            {
  // Add upwinded advective flux
  adv_.fluxUpwind(x, y, time, qI, qB, nx, ny, Fn);

  SANS_ASSERT(visc_.fluxViscousLinearInGradient());
  ArrayQ<T> kxx = 0, kxy = 0;
  ArrayQ<T> kyx = 0, kyy = 0;
  visc_.diffusionViscous(x, y, time, qB, qIx, qIy,
                         kxx, kxy,
                         kyx, kyy);

  ArrayQ<T> fv = 0, gv = 0;

  // Use gradient from interior
  fv = -kxx*qIx - kxy*qIy;
  gv = -kyx*qIx - kyy*qIy;

  // Add viscous flux
  Fn += fv*nx + gv*ny;
            }

template<class AdvectiveFlux, class ViscousFlux>
void
BCAdvectionDiffusion<PhysD2, BCTypeDirichlet_mitState<AdvectiveFlux,ViscousFlux>>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCAdvectionDiffusion<PhysD2, BCTypeDirichlet_mitState>:" << std::endl;
  out << indent << "  qB_ = " << qB_ << std::endl;
}

//----------------------------------------------------------------------------//
// Flux BC:  an u - (kn un + ks us) = bcdata | inflow
//                - (kn un + ks us) = bcdata | outflow
//
// an       normal advection velocity
// kn, ks   normal, tangential diffusion coefficients
//
template<>
struct BCAdvectionDiffusionParams< PhysD2, BCTypeFluxParams > : noncopyable
{
  const ParameterNumeric<Real> bcdata{"bcdata", NO_DEFAULT, NO_RANGE, "BC data"};
  const ParameterBool Inflow{"Inflow", true, "is this an inflow?"};

  static constexpr const char* BCName{"Flux"};
  struct Option
  {
    const DictOption Flux{BCAdvectionDiffusionParams::BCName, BCAdvectionDiffusionParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCAdvectionDiffusionParams params;
};

template<class AdvectiveFlux, class ViscousFlux>
class BCAdvectionDiffusion< PhysD2, BCTypeFlux< AdvectiveFlux, ViscousFlux>> :
public BCType< BCAdvectionDiffusion<PhysD2, BCTypeFlux<AdvectiveFlux, ViscousFlux>> >
{
public:
  typedef BCCategory::Flux_mitState Category;
  typedef BCAdvectionDiffusionParams< PhysD2, BCTypeFluxParams > ParamsType;

  typedef PhysD2 PhysDim;
  static const int D = AdvectionDiffusionTraits<PhysD2>::D;   // physical dimensions
  static const int N = AdvectionDiffusionTraits<PhysD2>::N;   // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD2>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD2>::template MatrixQ<T>; // matrices

  template<class Source>
  BCAdvectionDiffusion(const PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>& pde, const PyDict& d ) :
  adv_( pde.getAdvectiveFlux() ),
  visc_( pde.getViscousFlux() ),
  fluxViscous_( pde.hasFluxViscous() ),
  inflow_(d.get(ParamsType::params.Inflow)),
  bcdata_(d.get(ParamsType::params.bcdata)) {}

  //cppcheck-suppress noExplicitConstructor
  template<class Source>
  BCAdvectionDiffusion( const PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>& pde,
                        const ArrayQ<Real>& bcdata, const bool inflow ) :
                        adv_(pde.getAdvectiveFlux()),
                        visc_(pde.getViscousFlux()),
                        fluxViscous_(pde.hasFluxViscous()),
                        inflow_(inflow),
                        bcdata_(bcdata){}

  //cppcheck-suppress noExplicitConstructor
  BCAdvectionDiffusion( const AdvectiveFlux& adv, const ViscousFlux& visc,  const bool fluxViscous,
                        const ArrayQ<Real>& bcdata, const bool inflow ) :
                          adv_(adv),
                          visc_(visc),
                          fluxViscous_(fluxViscous),
                          inflow_(inflow),
                          bcdata_(bcdata){}

  virtual ~BCAdvectionDiffusion() {}

  BCAdvectionDiffusion& operator=( const BCAdvectionDiffusion& );

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return fluxViscous_; }

  // BC state vector
  template <class T>
  void state( const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // normal BC flux
  template <class T>
  void fluxNormal( const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const AdvectiveFlux& adv_;
  const ViscousFlux& visc_;
  const bool fluxViscous_;
  const bool inflow_;
  ArrayQ<Real> bcdata_;
};


template<class AdvectiveFlux, class ViscousFlux>
template <class T>
inline void
BCAdvectionDiffusion< PhysD2, BCTypeFlux<AdvectiveFlux, ViscousFlux> >::
state( const Real& x, const Real& y, const Real& time,
       const Real& nx, const Real& ny,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
       {
  qB = qI; // Use interior state, only the flux is being specified
       }


template<class AdvectiveFlux, class ViscousFlux>
template <class T>
inline void
BCAdvectionDiffusion< PhysD2, BCTypeFlux< AdvectiveFlux, ViscousFlux> >::
fluxNormal( const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
            const ArrayQ<T>& qB,
            ArrayQ<T>& Fn) const
            {
  // set the flux if there's viscosity, or it's inflow
  if ( fluxViscous_ || inflow_ )
    Fn += bcdata_;

  if ( !inflow_ ) // if outflow, don't specify the advective flux
  {
    ArrayQ<T> fx = 0, fy = 0;
    adv_.flux(x, y, time, qB, fx, fy);

    // Add advective flux using interior state
    Fn += fx*nx + fy*ny;
  }

            }

template<class AdvectiveFlux, class ViscousFlux>
inline void
BCAdvectionDiffusion< PhysD2, BCTypeFlux< AdvectiveFlux, ViscousFlux> >::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCAdvectionDiffusion<PhysD2, BCTypeFlux>:" << std::endl;
  out << indent << "  bcdata_ = :" << bcdata_;
  out << indent << "  inflow_ = " << inflow_ << std::endl;
}


//----------------------------------------------------------------------------//
// BCTypeFunction_mitState BC
//
class BCTypeFunction_mitStateParam;

template<>
struct BCAdvectionDiffusionParams<PhysD2, BCTypeFunction_mitStateParam> : ScalarFunction2DParams
{
  struct TypeOptions
  {
    typedef std::string ExtractType;
    const std::string Dirichlet = "Dirichlet";
    const std::string Neumann = "Neumann";
    const std::string Robin = "Robin";

    const std::vector<std::string> options{Dirichlet,Neumann,Robin};
  };
  const ParameterOption<TypeOptions> SolutionBCType{"SolutionBCType", NO_DEFAULT, "BC type to impose the exact solution"};

  const ParameterBool Upwind{"Upwind", true, "Use upwinding for advective flux"};

  static constexpr const char* BCName{"Function_mitState"};
  struct Option
  {
    const DictOption Function_mitState{BCAdvectionDiffusionParams::BCName, BCAdvectionDiffusionParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCAdvectionDiffusionParams params;
};

template<class AdvectiveFlux, class ViscousFlux>
class BCAdvectionDiffusion<PhysD2, BCTypeFunction_mitState<AdvectiveFlux,ViscousFlux>> :
public BCType< BCAdvectionDiffusion<PhysD2, BCTypeFunction_mitState<AdvectiveFlux,ViscousFlux>> >
{
public:
  typedef BCCategory::Flux_mitState Category;
  typedef BCAdvectionDiffusionParams<PhysD2, BCTypeFunction_mitStateParam> ParamsType;

  typedef PhysD2 PhysDim;
  static const int D = AdvectionDiffusionTraits<PhysD2>::D;   // physical dimensions
  static const int N = AdvectionDiffusionTraits<PhysD2>::N;   // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD2>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD2>::template MatrixQ<T>; // matrices

  typedef std::shared_ptr<Function2DBase<ArrayQ<Real>>> Function_ptr;

  enum BCCategoryType
  {
    Dirichlet, Neumann, Robin
  };

  template<class Source>
  BCAdvectionDiffusion(const PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>& pde, const PyDict& d ) :
  uexact_( ParamsType::getFunction(d) ),
  adv_( pde.getAdvectiveFlux() ),
  visc_( pde.getViscousFlux() ),
  upwind_(d.get(ParamsType::params.Upwind))
  {
    std::string bctype = d.get(ParamsType::params.SolutionBCType);
    if (bctype == ParamsType::params.SolutionBCType.Dirichlet)
      bctype_ = Dirichlet;
    else if (bctype == ParamsType::params.SolutionBCType.Neumann)
      bctype_ = Neumann;
    else if (bctype == ParamsType::params.SolutionBCType.Robin)
      bctype_ = Robin;
    else
      SANS_DEVELOPER_EXCEPTION("Unknown bctype = \"%s\".\n\nExpected \"Dirichlet\", \"Neumann\" or \"Robin\"");
  }

  BCAdvectionDiffusion( const Function_ptr& uexact, const AdvectiveFlux& adv, const ViscousFlux& visc,
                        const std::string& bctype, const bool& upwind ) :
                          uexact_(uexact), adv_(adv), visc_(visc), upwind_(upwind)
  {
    if (bctype == ParamsType::params.SolutionBCType.Dirichlet)
      bctype_ = Dirichlet;
    else if (bctype == ParamsType::params.SolutionBCType.Neumann)
      bctype_ = Neumann;
    else if (bctype == ParamsType::params.SolutionBCType.Robin)
      bctype_ = Robin;
    else
      SANS_DEVELOPER_EXCEPTION("Unknown bctype = \"%s\".\n\nExpected \"Dirichlet\", \"Neumann\" or \"Robin\"");
  }

  virtual ~BCAdvectionDiffusion() {}

  BCAdvectionDiffusion& operator=( const BCAdvectionDiffusion& );

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return true; }

  // BC state vector
  template <class T>
  void state( const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // normal BC flux
  template <class T>
  void fluxNormal( const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const Function_ptr uexact_;
  const AdvectiveFlux& adv_;
  const ViscousFlux& visc_;
  const bool upwind_;
  BCCategoryType bctype_;
};

template<class AdvectiveFlux, class ViscousFlux>
template <class T>
inline void
BCAdvectionDiffusion<PhysD2, BCTypeFunction_mitState<AdvectiveFlux,ViscousFlux>>::
state( const Real& x, const Real& y, const Real& time,
       const Real& nx, const Real& ny,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
       {
  if (bctype_ == Dirichlet || bctype_ == Robin)
    qB = (*uexact_)(x, y, time);
  else if (bctype_ == Neumann )
    qB = qI;
  else
    SANS_DEVELOPER_EXCEPTION("Something is really wrong, bctype_ = %d", bctype_);
       }

template<class AdvectiveFlux, class ViscousFlux>
template <class T>
inline void
BCAdvectionDiffusion<PhysD2, BCTypeFunction_mitState<AdvectiveFlux,ViscousFlux>>::
fluxNormal( const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
            const ArrayQ<T>& qB,
            ArrayQ<T>& Fn) const
            {
  if (upwind_)
  {
    Fn = 0;
    // Add upwinded advective flux
    adv_.fluxUpwind(x, y, time, qI, qB, nx, ny, Fn);
  }
  else
  {
    ArrayQ<T> fx = 0, fy = 0;
    adv_.flux(x, y, time, qB, fx, fy);

    // Add advective flux
    Fn += fx*nx + fy*ny;
  }

  SANS_ASSERT(visc_.fluxViscousLinearInGradient());
  ArrayQ<T> kxx = 0, kxy = 0;
  ArrayQ<T> kyx = 0, kyy = 0;
  visc_.diffusionViscous(x, y, time, qB, qIx, qIy,
                         kxx, kxy,
                         kyx, kyy);

  ArrayQ<T> fv = 0, gv = 0;

  if (bctype_ == Neumann || bctype_ == Robin)
  {
    Real qBtmp, qBx, qBy, qBt;
    (*uexact_).gradient(x, y, time, qBtmp, qBx, qBy, qBt);

    // Use exact gradient
    fv = kxx*qBx + kxy*qBy;
    gv = kyx*qBx + kyy*qBy;
  }
  else if (bctype_ == Dirichlet)
  {
    // Use gradient from interior
    fv = kxx*qIx + kxy*qIy;
    gv = kyx*qIx + kyy*qIy;
  }
  else
    SANS_DEVELOPER_EXCEPTION("Something is really wrong, bctype_ = %d", bctype_);

  // Add viscous flux
  Fn -= fv*nx + gv*ny;
            }

//----------------------------------------------------------------------------//
// function BC:  u = f(x,y)
//
template<>
struct BCAdvectionDiffusionParams< PhysD2, BCTypeFunctionLinearRobin_mitLG > : ScalarFunction2DParams
{
  static constexpr const char* BCName{"FunctionLinearRobin_mitLG"};
  struct Option
  {
    const DictOption FunctionLinearRobin_mitLG{BCAdvectionDiffusionParams::BCName, BCAdvectionDiffusionParams::checkInputs};
  };

  const ParameterNumeric<Real> A{"A", 1.0, NO_RANGE, "Value coefficient"};
  const ParameterNumeric<Real> B{"B", 0.0, NO_RANGE, "Viscous coefficient"};

  static void checkInputs(PyDict d);

  static BCAdvectionDiffusionParams params;
};

template<>
struct BCAdvectionDiffusionParams< PhysD2, BCTypeFunctionLinearRobin_sansLG > : ScalarFunction2DParams
{
  static constexpr const char* BCName{"FunctionLinearRobin_sansLG"};
  struct Option
  {
    const DictOption FunctionLinearRobin_sansLG{BCAdvectionDiffusionParams::BCName, BCAdvectionDiffusionParams::checkInputs};
  };

  const ParameterNumeric<Real> A{"A", 1.0, NO_RANGE, "Value coefficient"};
  const ParameterNumeric<Real> B{"B", 0.0, NO_RANGE, "Viscous coefficient"};

  static void checkInputs(PyDict d);

  static BCAdvectionDiffusionParams params;
};


template <class PhysDim>
class BCAdvectionDiffusion_FunctionBase;

template <>
class BCAdvectionDiffusion_FunctionBase<PhysD2>
{
public:
  typedef PhysD2 PhysDim;
  static const int D = AdvectionDiffusionTraits<PhysD2>::D;   // physical dimensions
  static const int N = AdvectionDiffusionTraits<PhysD2>::N;   // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD2>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD2>::template MatrixQ<T>; // matrices

  typedef std::shared_ptr<Function2DBase<ArrayQ<Real>>> Function_ptr;

  //cppcheck-suppress noExplicitConstructor
  BCAdvectionDiffusion_FunctionBase( const Function_ptr& uexact, const ViscousFlux2DBase& visc, Real A = 1, Real B = 0 ) :
    uexact_(uexact), visc_(visc), A_(A), B_(B) {}

  virtual ~BCAdvectionDiffusion_FunctionBase() {}

  // BC coefficients:  A u + B (kn un + ks us)
  template <class T>
  void coefficients(
      const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      MatrixQ<T>& A, MatrixQ<T>& B ) const;

  template <class T, class L>
  void coefficients(
      const L& param, const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      MatrixQ<T>& A, MatrixQ<T>& B ) const;

  // BC data
  template <class T>
  void data( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny, ArrayQ<T>& bcdata ) const;

  template <class T, class L>
  void data( const L& param, const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny, ArrayQ<T>& bcdata ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const Function_ptr uexact_;
  const ViscousFlux2DBase& visc_;
  Real A_, B_;
};

template <class T>
inline void
BCAdvectionDiffusion_FunctionBase< PhysD2 >::coefficients(
    const Real&, const Real&, const Real&, const Real&, const Real&,
    MatrixQ<T>& A, MatrixQ<T>& B ) const
    {
  A = A_;
  B = B_;
    }

template <class T, class L>
inline void
BCAdvectionDiffusion_FunctionBase<PhysD2>::coefficients(
    const L& param, const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    MatrixQ<T>& A, MatrixQ<T>& B ) const
    {
  coefficients(x, y, time, nx, ny, A, B);
    }

template <class T>
inline void
BCAdvectionDiffusion_FunctionBase< PhysD2 >::data(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny, ArrayQ<T>& bcdata ) const
    {
  Real u, ux, uy, ut;
  uexact_->gradient(x, y, time, u, ux, uy, ut);

  Real kxx = 0, kxy = 0, kyx = 0, kyy = 0;
  visc_.diffusionViscous(x, y, time, u, ux, uy, kxx, kxy, kyx, kyy);

  Real f = kxx*ux + kxy*uy;
  Real g = kyx*ux + kyy*uy;

  bcdata = A_*u + B_*(f*nx + g*ny);
    }

template <class T, class L>
inline void
BCAdvectionDiffusion_FunctionBase< PhysD2 >::data(
    const L& param, const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny, ArrayQ<T>& bcdata ) const
    {
  data(x, y, time, nx, ny, bcdata);
    }

template <>
class BCAdvectionDiffusion<PhysD2, BCTypeFunctionLinearRobin_mitLG> :
public BCType< BCAdvectionDiffusion<PhysD2, BCTypeFunctionLinearRobin_mitLG> >,
public BCAdvectionDiffusion_FunctionBase<PhysD2>
{
public:
  typedef BCAdvectionDiffusion_FunctionBase<PhysD2> BaseType;
  typedef BCCategory::LinearScalar_mitLG Category;
  typedef BCAdvectionDiffusionParams<PhysD2, BCTypeFunctionLinearRobin_mitLG> ParamsType;

  template<class AdvectiveFlux, class ViscousFlux, class Source>
  BCAdvectionDiffusion(const PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>& pde, const PyDict& d ) :
  BaseType( ParamsType::params.getFunction(d), pde.getViscousFlux(),
            d.get(ParamsType::params.A), d.get(ParamsType::params.B)) {}

  BCAdvectionDiffusion( const Function_ptr& uexact, const ViscousFlux2DBase& visc, Real A = 1, Real B = 0 ) :
    BaseType( uexact, visc, A, B ) {}
};

template <>
class BCAdvectionDiffusion<PhysD2, BCTypeFunctionLinearRobin_sansLG> :
public BCType< BCAdvectionDiffusion<PhysD2, BCTypeFunctionLinearRobin_sansLG> >,
public BCAdvectionDiffusion_FunctionBase<PhysD2>
{
public:
  typedef BCAdvectionDiffusion_FunctionBase<PhysD2> BaseType;
  typedef BCCategory::LinearScalar_sansLG Category;
  typedef BCAdvectionDiffusionParams<PhysD2, BCTypeFunctionLinearRobin_sansLG> ParamsType;

  template<class AdvectiveFlux, class ViscousFlux, class Source>
  BCAdvectionDiffusion(const PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>& pde, const PyDict& d ) :
  BaseType( ParamsType::params.getFunction(d), pde.getViscousFlux(),
            d.get(ParamsType::params.A), d.get(ParamsType::params.B)) {}

  BCAdvectionDiffusion( const Function_ptr& uexact, const ViscousFlux2DBase& visc, Real A = 1, Real B = 0 ) :
    BaseType( uexact, visc, A, B ) {}
};


//----------------------------------------------------------------------------//
// Function Dirichlet mitLG BC u = f(x,y)
//

template<>
struct BCAdvectionDiffusionParams<PhysD2, BCTypeFunctionDirichlet_mitLG> : ScalarFunction2DParams
{
  const ParameterBool strong{"strong", false, "Strongly impose BC with collocation points"};

  static constexpr const char* BCName{"FunctionDirichlet_mitLG"};
  struct Option
  {
    const DictOption FunctionDirichlet_mitLG{BCAdvectionDiffusionParams::BCName, BCAdvectionDiffusionParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCAdvectionDiffusionParams params;
};

template <>
class BCAdvectionDiffusion<PhysD2, BCTypeFunctionDirichlet_mitLG> :
public BCType< BCAdvectionDiffusion<PhysD2, BCTypeFunctionDirichlet_mitLG> >
{
public:
  typedef BCCategory::Dirichlet_mitLG Category;
  typedef BCAdvectionDiffusionParams<PhysD2, BCTypeFunctionDirichlet_mitLG> ParamsType;

  typedef PhysD2 PhysDim;
  static const int D = AdvectionDiffusionTraits<PhysD2>::D;   // physical dimensions
  static const int N = AdvectionDiffusionTraits<PhysD2>::N;   // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD2>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD2>::template MatrixQ<T>; // matrices

  typedef std::shared_ptr<Function2DBase<ArrayQ<Real>>> Function_ptr;

  template <class PDE>
  BCAdvectionDiffusion( const PDE& pde, const PyDict& d ) :
  uexact_( ParamsType::params.getFunction(d) ),
  isStrongBC_(d.get(ParamsType::params.strong))
  {
    SANS_ASSERT( uexact_ != nullptr );
  }

  template <class PDE>
  BCAdvectionDiffusion( const PDE& pde, const Function_ptr& uexact, const bool strongBC = false ) :
  uexact_( uexact ), isStrongBC_(strongBC)
  {
    SANS_ASSERT( uexact_ != nullptr );
  }

  virtual ~BCAdvectionDiffusion() {}

  BCAdvectionDiffusion& operator=( const BCAdvectionDiffusion& );

  // Should the BC be strongly imposed with collocation points
  bool isStrongBC() const { return isStrongBC_; }

  // BC lagrange multiplier term
  template <class T>
  void lagrangeTerm( const Real& x, const Real& y, const Real& time,
                     const Real& nx, const Real& ny,
                     const ArrayQ<T>& qI,
                     const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                     const ArrayQ<T>& lg,
                     ArrayQ<T>& lgterm) const
  {
    // Remove the residual using the lagrange multiplier
    lgterm = lg;
  }

  // normal BC flux
  template <class T>
  void strongBC( const Real& x, const Real& y, const Real& time,
                 const Real& nx, const Real& ny,
                 const ArrayQ<T>& qI,
                 const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                 const ArrayQ<T>& lg,
                 ArrayQ<T>& residualBC) const
  {
    // set boundary condition residual
    residualBC = qI - (*uexact_)(x, y, time);
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const Function_ptr uexact_;
  const bool isStrongBC_;
};

} //namespace SANS

#endif  // BCADVECTIONDIFFUSION2D_H
