// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCAdvectionDiffusion2D.h"

#include "AdvectiveFlux2D.h"
#include "ViscousFlux2D.h"
#include "Source2D.h"
#include "ForcingFunction2D.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{

template ParameterOption<BCAdvectionDiffusionParams<PhysD2, BCTypeFunction_mitStateParam>::TypeOptions>::ExtractType
PyDict::get(ParameterType<ParameterOption<BCAdvectionDiffusionParams<PhysD2, BCTypeFunction_mitStateParam>::TypeOptions> > const&) const;

//===========================================================================//
//
// Parameter classes
//
// cppcheck-suppress passedByValue
void BCAdvectionDiffusionParams<PhysD2, BCTypeTimeOutParam>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase *> allParams;
  d.checkUnknownInputs(allParams);
}
BCAdvectionDiffusionParams<PhysD2, BCTypeTimeOutParam> BCAdvectionDiffusionParams<PhysD2, BCTypeTimeOutParam>::params;

// cppcheck-suppress passedByValue
void BCAdvectionDiffusionParams<PhysD2, BCTypeTimeICParam>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase *> allParams;
  allParams.push_back(d.checkInputs(params.qB));
  d.checkUnknownInputs(allParams);
}
BCAdvectionDiffusionParams<PhysD2, BCTypeTimeICParam> BCAdvectionDiffusionParams<PhysD2, BCTypeTimeICParam>::params;

// cppcheck-suppress passedByValue
void BCAdvectionDiffusionParams<PhysD2, BCTypeTimeIC_FunctionParam>::checkInputs(PyDict d)
{
    std::vector<const ParameterBase *> allParams;
    allParams.push_back(d.checkInputs(params.Function));
    d.checkUnknownInputs(allParams);
}
BCAdvectionDiffusionParams<PhysD2, BCTypeTimeIC_FunctionParam> BCAdvectionDiffusionParams<PhysD2, BCTypeTimeIC_FunctionParam>::params;

// cppcheck-suppress passedByValue
void BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.A));
  allParams.push_back(d.checkInputs(params.B));
  allParams.push_back(d.checkInputs(params.bcdata));
  d.checkUnknownInputs(allParams);
}
BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG> BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_sansLG>::params;

// cppcheck-suppress passedByValue
void BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_mitLG>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.A));
  allParams.push_back(d.checkInputs(params.B));
  allParams.push_back(d.checkInputs(params.bcdata));
  d.checkUnknownInputs(allParams);
}
BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_mitLG> BCAdvectionDiffusionParams<PhysD2, BCTypeLinearRobin_mitLG>::params;

// cppcheck-suppress passedByValue
void BCAdvectionDiffusionParams<PhysD2, BCTypeDirichlet_mitLG>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.uB));
  allParams.push_back(d.checkInputs(params.strong));
  d.checkUnknownInputs(allParams);
}
BCAdvectionDiffusionParams<PhysD2, BCTypeDirichlet_mitLG> BCAdvectionDiffusionParams<PhysD2, BCTypeDirichlet_mitLG>::params;

// cppcheck-suppress passedByValue
void BCAdvectionDiffusionParams<PhysD2, BCTypeNatural>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.gB));
  d.checkUnknownInputs(allParams);
}
BCAdvectionDiffusionParams<PhysD2, BCTypeNatural> BCAdvectionDiffusionParams<PhysD2, BCTypeNatural>::params;

// cppcheck-suppress passedByValue
void BCAdvectionDiffusionParams<PhysD2, BCTypeDirichlet_mitStateParam>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.qB));
  d.checkUnknownInputs(allParams);
}
BCAdvectionDiffusionParams<PhysD2, BCTypeDirichlet_mitStateParam> BCAdvectionDiffusionParams<PhysD2, BCTypeDirichlet_mitStateParam>::params;

// cppcheck-suppress passedByValue
void BCAdvectionDiffusionParams<PhysD2, BCTypeFluxParams>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.bcdata));
  allParams.push_back(d.checkInputs(params.Inflow));
  d.checkUnknownInputs(allParams);
}
BCAdvectionDiffusionParams<PhysD2, BCTypeFluxParams> BCAdvectionDiffusionParams<PhysD2, BCTypeFluxParams>::params;

// cppcheck-suppress passedByValue
void BCAdvectionDiffusionParams<PhysD2, BCTypeFunction_mitStateParam>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Function));
  allParams.push_back(d.checkInputs(params.SolutionBCType));
  allParams.push_back(d.checkInputs(params.Upwind));
  d.checkUnknownInputs(allParams);
}
BCAdvectionDiffusionParams<PhysD2, BCTypeFunction_mitStateParam> BCAdvectionDiffusionParams<PhysD2, BCTypeFunction_mitStateParam>::params;

// cppcheck-suppress passedByValue
void BCAdvectionDiffusionParams<PhysD2, BCTypeFunctionLinearRobin_mitLG>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Function));
  allParams.push_back(d.checkInputs(params.A));
  allParams.push_back(d.checkInputs(params.B));
  d.checkUnknownInputs(allParams);
}
BCAdvectionDiffusionParams<PhysD2, BCTypeFunctionLinearRobin_mitLG> BCAdvectionDiffusionParams<PhysD2, BCTypeFunctionLinearRobin_mitLG>::params;

// cppcheck-suppress passedByValue
void BCAdvectionDiffusionParams<PhysD2, BCTypeFunctionLinearRobin_sansLG>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Function));
  allParams.push_back(d.checkInputs(params.A));
  allParams.push_back(d.checkInputs(params.B));
  d.checkUnknownInputs(allParams);
}
BCAdvectionDiffusionParams<PhysD2, BCTypeFunctionLinearRobin_sansLG> BCAdvectionDiffusionParams<PhysD2, BCTypeFunctionLinearRobin_sansLG>::params;

// cppcheck-suppress passedByValue
void BCAdvectionDiffusionParams<PhysD2, BCTypeFunctionDirichlet_mitLG>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Function));
  allParams.push_back(d.checkInputs(params.strong));
  d.checkUnknownInputs(allParams);
}
BCAdvectionDiffusionParams<PhysD2, BCTypeFunctionDirichlet_mitLG> BCAdvectionDiffusionParams<PhysD2, BCTypeFunctionDirichlet_mitLG>::params;


//===========================================================================//
// Instantiate the BC parameters

typedef BCAdvectionDiffusion2DVector< AdvectiveFlux2D_None         , ViscousFlux2D_Uniform    > BCVector_ANDU;
typedef BCAdvectionDiffusion2DVector< AdvectiveFlux2D_Uniform      , ViscousFlux2D_Uniform    > BCVector_AUDU;
typedef BCAdvectionDiffusion2DVector< AdvectiveFlux2D_ConstRotation, ViscousFlux2D_None       > BCVector_ACRDN;
typedef BCAdvectionDiffusion2DVector< AdvectiveFlux2D_Uniform      , ViscousFlux2D_None       > BCVector_AUDN;
typedef BCAdvectionDiffusion2DVector< AdvectiveFlux2D_None         , ViscousFlux2D_P_Laplacian> BCVector_ANDP;

BCPARAMETER_INSTANTIATE(BCVector_ANDU)
BCPARAMETER_INSTANTIATE(BCVector_AUDU)
BCPARAMETER_INSTANTIATE(BCVector_ACRDN)
BCPARAMETER_INSTANTIATE(BCVector_AUDN)
BCPARAMETER_INSTANTIATE(BCVector_ANDP)


//===========================================================================//
void
BCAdvectionDiffusion_LinearRobinBase<PhysD2>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCAdvectionDiffusion<PhysD2, BCTypeLinearRobin>:" << std::endl;
  out << indent << "  A_ = " << A_ << std::endl;
  out << indent << "  B_ = " << B_ << std::endl;
  out << indent << "  bcdata_ = " << bcdata_ << std::endl;
}

} //namespace SANS
