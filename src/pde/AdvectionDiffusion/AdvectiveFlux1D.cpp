// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// 1D advection velocity

#include <string>
#include <iostream>

#include "AdvectiveFlux1D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// advective flux: uniform flow

void
AdvectiveFlux1D_Uniform::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "AdvectiveFlux1D_Uniform:"
            << "  u_ = " << u_ << std::endl;
}


//----------------------------------------------------------------------------//
// advective flux: Burgers

void
AdvectiveFlux1D_Burgers::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "AdvectiveFlux1D_Burgers:" << std::endl;
}

} //namespace SANS
