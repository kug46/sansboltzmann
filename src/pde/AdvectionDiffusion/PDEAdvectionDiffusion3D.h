// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDEADVECTIONDIFFUSION3D_H
#define PDEADVECTIONDIFFUSION3D_H

// 3-D Advection-Diffusion PDE class

#include "tools/SANSnumerics.h"     // Real
#include "tools/smoothmath.h"     // smoothabs
#include "AdvectionDiffusion_Traits.h"
#include "AdvectiveFlux3D.h"
#include "ViscousFlux3D.h"
#include "ForcingFunction3D.h"
#include "Source3D.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "pde/AnalyticFunction/ScalarFunction3D.h"
#include "Topology/ElementTopology.h"
#include "Topology/Dimension.h"
#include "pde/ForcingFunctionBase.h"

#include <cmath>              // sqrt
#include <iostream>
#include <string>

namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: 3-D Advection-Diffusion
//
// template parameters:
//   T                        solution DOF data type (e.g. double)
//
// member functions:
//   .hasFluxAdvectiveTime        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//   .hasFluxViscous             T/F: PDE has viscous flux term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: dU/dQ
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .fluxViscous                viscous fluxes: Fv(Q, QX)
//   .diffusionViscous           viscous diffusion coefficient: d(Fv)/d(UX)
//   .isValidState               T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom                 set from primitive variable array
//----------------------------------------------------------------------------//

template<class AdvectiveFlux, class ViscousFlux, class Source>
class PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>
{
public:
  typedef PhysD3 PhysDim;
  static const int D = AdvectionDiffusionTraits<PhysD3>::D;   // physical dimensions
  static const int N = AdvectionDiffusionTraits<PhysD3>::N;   // total solution variables

  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD3>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD3>::template MatrixQ<T>; // matrices

  typedef PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source> PDEClass;
  typedef ForcingFunctionBase3D<PDEClass> ForcingFunctionType;

  PDEAdvectionDiffusion( const AdvectiveFlux& adv, const ViscousFlux& visc,
                         const Source &src, const std::shared_ptr<ForcingFunctionType>& force = NULL  ) :
      adv_(adv), visc_(visc), source_(src), force_(force) {}

  ~PDEAdvectionDiffusion() {}

  PDEAdvectionDiffusion( const PDEAdvectionDiffusion& ) = delete;
  PDEAdvectionDiffusion& operator=( const PDEAdvectionDiffusion& ) = delete;

  // flux components
  bool hasFluxAdvectiveTime() const { return true; }
  bool hasFluxAdvective() const { return adv_.hasFluxAdvective(); }
  bool hasFluxViscous() const { return visc_.hasFluxViscous(); }
  bool hasSource() const { return source_.hasSourceTerm(); }
  bool hasSourceTrace() const { return source_.hasSourceTrace(); }
  bool hasSourceLiftedQuantity() const { return false; }
  bool hasForcingFunction() const { return force_ != NULL && force_->hasForcingFunction(); }

  // viscous flux is of the form K(q).gradu
  bool fluxViscousLinearInGradient() const { return visc_.fluxViscousLinearInGradient(); }
  bool needsSolutionGradientforSource() const { return source_.needsSolutionGradientforSource(); }

  const AdvectiveFlux& getAdvectiveFlux() const { return adv_; }
  const ViscousFlux& getViscousFlux() const { return visc_; }

  // unsteady temporal flux: Ft(Q)
  template <class T, class Tf>
  void fluxAdvectiveTime(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tf>& ft ) const
  {
    T ftLocal = 0;
    masterState(x, y, z, time, q, ftLocal);
    ft += ftLocal;
  }

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <class T>
  void jacobianFluxAdvectiveTime(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& J ) const
  {
    J += DLA::Identity();
  }

  // master state: U(Q)
  template <class T>
  void masterState(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, ArrayQ<T>& uCons ) const;


  // master state: U(Q)
  template<class Tq, class Tqp, class Tf>
  void perturbedMasterState(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& dq, ArrayQ<Tf>& du ) const { du = dq; }

  // master state Gradient: Ux(Q)
  template<class Tq, class Tg, class Tf>
  void masterStateGradient(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      ArrayQ<Tf>& Ux, ArrayQ<Tf>& Uy, ArrayQ<Tf>& Uz  ) const
  {
    Ux = qx; Uy = qy; Uz = qz;
  }

  // master state Hessian: Uxx(Q), Uxy(Q), Uyy(Q)
  template<class Tq, class Tg, class Th, class Tf>
  void masterStateHessian(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qxz,
      const ArrayQ<Th>& qyy, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
      ArrayQ<Tf>& Uxx, ArrayQ<Tf>& Uxy, ArrayQ<Tf>& Uxz,
      ArrayQ<Tf>& Uyy, ArrayQ<Tf>& Uyz, ArrayQ<Tf>& Uzz ) const

  {
    Uxx = qxx; Uxy = qxy; Uyy = qyy;
    Uxz = qxz; Uxy = qyz; Uyy = qzz;
  }

  // unsteady conservative flux Jacobian: dU(Q)/dQ
  template<class T>
  void jacobianMasterState(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const;

  // unsteady conservative flux: d(U)/d(t)
  template <class Tq, class Tg, class Tf>
  void strongFluxAdvectiveTime(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qt, ArrayQ<Tf>& strongPDE ) const;

  // advective flux: F(Q)
  template <class T, class Tf>
  void fluxAdvective(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& fz ) const;

  template <class Tq, class Tf>
  void fluxAdvectiveUpwind(
      const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, const Real& ny, const Real& nz, ArrayQ<Tf>& f, const Real& scale = 1  ) const;

  template <class Tq, class Tf>
  void fluxAdvectiveUpwindSpaceTime(
      const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, const Real& ny, const Real& nz, const Real& nt, ArrayQ<Tf>& f, const Real& scale= 1 ) const;

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class Tq, class Tf>
  void jacobianFluxAdvective(
      const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<Tq>& q,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& az ) const;


  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const Real& nx, const Real& ny, const Real& nz,
      MatrixQ<Tf>& a ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const Real& nx, const Real& ny, const Real& nz, const Real& nt,
      MatrixQ<Tf>& a ) const;

  // strong form advective fluxes
  template <class Tq, class Tg, class Tf>
  void strongFluxAdvective(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      ArrayQ<Tf>& strongPDE ) const;

  // viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g, ArrayQ<Tf>& h ) const;

  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qzL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qzR,
      const Real& nx, const Real& ny, const Real& nz,
      ArrayQ<Tf>& f ) const;


  // perturbation to viscous flux from dux, duy
  template <class Tq, class Tg, class Tgu, class Tf>
  void perturbedGradFluxViscous(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Tgu>& dqx, const ArrayQ<Tgu>& dqy, const ArrayQ<Tgu>& dqz,
      ArrayQ<Tf>& df, ArrayQ<Tf>& dg, ArrayQ<Tf>& dh ) const;

    // perturbation to viscous flux from dux, duy
  template <class Tk, class Tgu, class Tf>
  void perturbedGradFluxViscous(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const MatrixQ<Tk>& kxx, const MatrixQ<Tk>& kxy, const MatrixQ<Tk>& kxz,
      const MatrixQ<Tk>& kyx, const MatrixQ<Tk>& kyy, const MatrixQ<Tk>& kyz,
      const MatrixQ<Tk>& kzx, const MatrixQ<Tk>& kzy, const MatrixQ<Tk>& kzz,
      const ArrayQ<Tgu>& dux, const ArrayQ<Tgu>& duy, const ArrayQ<Tgu>& duz,
      ArrayQ<Tf>& df, ArrayQ<Tf>& dg, ArrayQ<Tf>& dh ) const;

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxz,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyz,
      MatrixQ<Tk>& kzx, MatrixQ<Tk>& kzy, MatrixQ<Tk>& kzz) const;

  template <class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
      MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kxz_x, MatrixQ<Tk>& kyx_x, MatrixQ<Tk>& kzx_x,
      MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y, MatrixQ<Tk>& kyz_y, MatrixQ<Tk>& kzy_y,
      MatrixQ<Tk>& kxz_z, MatrixQ<Tk>& kyz_z, MatrixQ<Tk>& kzx_z, MatrixQ<Tk>& kzy_z, MatrixQ<Tk>& kzz_z ) const;

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscous(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& az ) const;

  // strong form viscous fluxes: -d(Fv)/dx - d(Fv)/d(y) - d(Fv)/d(z)
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscous(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
      ArrayQ<Tf>& strongPDE ) const;

  // space-time viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz, const ArrayQ<Tg>& qt,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& fz, ArrayQ<Tf>& ft ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial viscous flux
    fluxViscous(x, y, z, time, q, qx, qy, qz, fx, fy, fz);
  }

  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qzL, const ArrayQ<Tg>& qtL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qzR, const ArrayQ<Tg>& qtR,
      const Real& nx, const Real& ny, const Real& nz, const Real& nt,
      ArrayQ<Tf>& f ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial viscous flux
    fluxViscous(x, y, z, time, qL, qxL, qyL, qzL, qR, qxR, qyR, qzR, nx, ny, nz, f);
  }

  // space-time viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscousSpaceTime(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz, const ArrayQ<Tg>& qt,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxz, MatrixQ<Tk>& kxt,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyz, MatrixQ<Tk>& kyt,
      MatrixQ<Tk>& kzx, MatrixQ<Tk>& kzy, MatrixQ<Tk>& kzz, MatrixQ<Tk>& kzt,
      MatrixQ<Tk>& ktx, MatrixQ<Tk>& kty, MatrixQ<Tk>& ktz, MatrixQ<Tk>& ktt ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial diffusion matrix
    diffusionViscous(x, y, z, time, q, qx, qy, qz,
                     kxx, kxy, kxz,
                     kyx, kyy, kyz,
                     kzx, kzy, kzz);
  }

  // space-time jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz, const ArrayQ<Tg>& qt,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& az, MatrixQ<Tf>& at ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial jacobianFluxViscous
    jacobianFluxViscous(x, y, z, time, q, qx, qy, qz, ax, ay, az);
  }

  // space-time strong form viscous fluxes: -d(Fv)/dx - d(Fv)/d(y)
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz, const ArrayQ<Tg>& qt,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
      const ArrayQ<Th>& qxt, const ArrayQ<Th>& qyt, const ArrayQ<Th>& qzt, const ArrayQ<Th>& qtt,
      ArrayQ<Tf>& strongPDE ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial strongFluxViscous
    strongFluxViscous(x, y, z, time, q, qx, qy, qz, qxx, qxy, qyy, qxz, qyz, qzz, strongPDE);
  }

  // solution-dependent source: S(X, Q, QX)
  template <class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      ArrayQ<Ts>& source ) const;

  // Forward call to S(Q+QP, QX+QPX)
  template <class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void source(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy, const ArrayQ<Tgp>& qpz,
      ArrayQ<Ts>& src ) const
  {
    typedef typename promote_Surreal<Tq, Tqp>::type Tqq;
    typedef typename promote_Surreal<Tg, Tgp>::type Tgg;

    ArrayQ<Tqq> qq = q + qp;
    ArrayQ<Tgg> qqx = qx + qpx;
    ArrayQ<Tgg> qqy = qy + qpy;
    ArrayQ<Tgg> qqz = qz + qpz;

    source(x, y, z, time, qq, qqx, qqy, qqz, src);
  }

  // Forward call to S(Q,QP, QX,QPX)
  template <class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void sourceCoarse(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy, const ArrayQ<Tgp>& qpz,
      ArrayQ<Ts>& src ) const
  {
    source(x, y, z, time, q, qp, qx, qy, qz, qpx, qpy, qpz, src);
  }

  // Forward call to S(Q,QP, QX,QPX)
  template <class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void sourceFine(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy, const ArrayQ<Tgp>& qpz,
      ArrayQ<Ts>& src ) const
  {
    source(x, y, z, time, q, qp, qx, qy, qz, qpx, qpy, qpz, src);
  }

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX)
  template <class Tlq, class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      ArrayQ<Ts>& s ) const
  {
    source(x, y, z, time, q, qx, qy, qz, s); //forward to regular source
  }

  // linear change in source in response to linear perturbations du, dux, duy
  template <class Tq, class Tg, class Tu, class Tgu, class Ts>
  void perturbedSource(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Tu>& du, const ArrayQ<Tgu>& dux, const ArrayQ<Tgu>& duy, const ArrayQ<Tgu>& duz, ArrayQ<Ts>& dS ) const;

  // dual-consistent source
  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& xL, const Real& yL, const Real& zL,
      const Real& xR, const Real& yR, const Real& zR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qzL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qzR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const;

  // trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
  template <class Tq, class Ts>
  void sourceLiftedQuantity(
      const Real& xL, const Real& yL, const Real& zL,
      const Real& xR, const Real& yR, const Real& zR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, Ts& s ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianSource(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdu ) const;

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianSourceHACK(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdu ) const
  {
    jacobianSource(x, y, z, time, q, qx, qy, qz, dsdu);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdu ) const;

  // jacobian of source wrt conservation variables: d(S)/d(Ux), d(S)/d(Uy)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSourceHACK(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy, MatrixQ<Ts>& dsduz   ) const
  {
    jacobianGradientSource(x, y, z, time, q, qx, qy, qz, dsdux, dsduy, dsduz);
  }

  // jacobian of source wrt conservation variables: d(S)/d(Ux), d(S)/d(Uy)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSource(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy, MatrixQ<Ts>& dsduz   ) const;

  template <class Tq, class Tg, class Ts>
  void jacobianGradientSourceAbsoluteValue(
      const Real& x, const Real& y, const Real& z, const Real& time, const Real& nx, const Real& ny, const Real& nz,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& divSdotN ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Th, class Ts>
  void jacobianGradientSourceGradient(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
      MatrixQ<Ts>& div_dsdgradu ) const;

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Th, class Ts>
  void jacobianGradientSourceGradientHACK(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
      MatrixQ<Ts>& div_dsdgradu ) const
  {
    jacobianGradientSourceGradient(x, y, z, time, q, qx, qy, qz, qxx, qxy, qyy, qxz, qyz, qzz, div_dsdgradu);
  }

  // right-hand-side forcing function
  template <class T>
  void forcingFunction( const Real& x, const Real& y, const Real& z, const Real& time, ArrayQ<T>& source ) const;

  // characteristic speed (needed for timestep)
  template <class T>
  void speedCharacteristic(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const Real& dx, const Real& dy, const Real& dz, const ArrayQ<T>& q, Real& speed ) const;

  // characteristic speed
  template <class T>
  void speedCharacteristic(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, Real& speed ) const;

  // PDE specific stabilization matrix
  template <class Tq, class Tf>
  void stabMatrix(
      const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<Tq>& q,
      const Tq& cn1, const Tq& cn2, const Tq& cn3, MatrixQ<Tf>& dfdu ) const;

  // PDE specific function to accumulate basis functions for stabilization matrix
  template <class Tq>
  void getCN( const Real& x, const Real& y,  const Real& z, const Real& time, const ArrayQ<Tq>& q,
              const Real& nx, const Real& ny, const Real& nz, Tq& cn1, Tq& cn2, Tq& cn3 ) const;

  // update fraction needed for physically valid state
  void updateFraction(
      const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
      const Real maxChangeFraction, Real& updateFraction ) const;

  // is state physically valid
  template <class T>
  bool isValidState( const ArrayQ<T>& q ) const;

  // set from primitive variable array
  template <class T>
  void setDOFFrom(
      ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const;

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const;

  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const;

  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const;

  // how many residual equations are we dealing with
  int nMonitor() const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const AdvectiveFlux adv_;
  const ViscousFlux visc_;
  const Source source_;
  const std::shared_ptr<ForcingFunctionType> force_;
};

template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class T>
inline void
PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>::masterState(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<T>& q, ArrayQ<T>& uCons ) const
{
  uCons = q;
}

template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class T>
inline void
PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>::jacobianMasterState(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
{
  dudq = DLA::Identity();
}

template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Tf>
inline void
PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>::strongFluxAdvectiveTime(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qt, ArrayQ<Tf>& ft ) const
{
  ft += qt;
}

template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class T, class Tf>
inline void
PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>::fluxAdvective(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<T>& q, ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& fz ) const
{
  adv_.flux( x, y, z, time, q, fx, fy, fz );
}

template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tf>
inline void
PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>::fluxAdvectiveUpwind(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, const Real& nx, const Real& ny, const Real& nz,
    ArrayQ<Tf>& f, const Real& scale  ) const
{
  adv_.fluxUpwind(x, y, z, time, qL, qR, nx, ny, nz, f, scale);
}

template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tf>
inline void
PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>::fluxAdvectiveUpwindSpaceTime(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, const Real& nx, const Real& ny, const Real& nz, const Real& nt,
    ArrayQ<Tf>& f, const Real& scale ) const
{
  adv_.fluxUpwindSpaceTime(x, y, z, time, qL, qR, nx, ny, nz, nt, f, scale);
}


// advective flux jacobian: d(F)/d(U)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tf>
inline void
PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>::jacobianFluxAdvective(
    const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<Tq>& q,
    MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& az ) const
{
  adv_.jacobian( x, y, z, time, q, ax, ay, az );
}


// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tf>
inline void
PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>::jacobianFluxAdvectiveAbsoluteValue(
  const Real& x, const Real& y, const Real& z, const Real& time,
  const ArrayQ<Tq>& q, const Real& nx, const Real& ny, const Real& nz, MatrixQ<Tf>& mtx ) const
{
  MatrixQ<Tq> ax = 0, ay = 0, az = 0;

  jacobianFluxAdvective(x, y, z, time, q, ax, ay, az);

  //TODO: Rather arbitrary smoothing constants here
  //Real eps = 1e-8;

  //mtx += smoothabs0( nx*ax + ny*ay + nz*az, eps );
  mtx += fabs( nx*ax + ny*ay + nz*az );
}

// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tf>
inline void
PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>::jacobianFluxAdvectiveAbsoluteValueSpaceTime(
  const Real& x, const Real& y, const Real& z, const Real& time,
  const ArrayQ<Tq>& q, const Real& nx, const Real& ny, const Real& nz, const Real& nt, MatrixQ<Tf>& mtx ) const
{
  MatrixQ<Tq> ax = 0, ay = 0, az = 0;

  jacobianFluxAdvective(x, y, z, time, q, ax, ay, az);

  //TODO: Rather arbitrary smoothing constants here
  //Real eps = 1e-8;

  //mtx += smoothabs0( nx*ax + ny*ay + nz*az + nt*1, eps );
  mtx += fabs( nx*ax + ny*ay + nz*az + nt*1 );
}

// strong form of advective flux: div.F
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Tf>
inline void
PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>::strongFluxAdvective(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    ArrayQ<Tf>& strongPDE ) const
{
  adv_.strongFlux( x, y, z, time, q, qx, qy, qz, strongPDE );
}


// viscous flux
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Tf>
inline void
PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>::fluxViscous(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    ArrayQ<Tf>& f, ArrayQ<Tf>& g, ArrayQ<Tf>& h ) const
{
  visc_.fluxViscous( x, y, z, time, q, qx, qy, qz, f, g, h );
}

// viscous flux: normal flux with left and right states
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Tf>
inline void
PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>::fluxViscous(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qzL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qzR,
    const Real& nx, const Real& ny, const Real& nz,
    ArrayQ<Tf>& f ) const
{
#if 1
  ArrayQ<Tf> fL = 0, fR = 0;
  ArrayQ<Tf> gL = 0, gR = 0;
  ArrayQ<Tf> hL = 0, hR = 0;

  fluxViscous(x, y, z, time, qL, qxL, qyL, qzL, fL, gL, hL);
  fluxViscous(x, y, z, time, qR, qxR, qyR, qzR, fR, gR, hR);

  f += 0.5*(fL + fR)*nx + 0.5*(gL + gR)*ny + 0.5*(hL + hR)*nz;
#else
  ArrayQ<Tq> q = 0.5*(qL + qR);
  ArrayQ<Tg> qx = 0.5*(qxL + qxR);
  ArrayQ<Tg> qy = 0.5*(qyL + qyR);
  ArrayQ<Tg> qz = 0.5*(qzL + qzR);

  MatrixQ<Tq> kxx=0, kxy=0, kxz=0,
              kyx=0, kyy=0, kyz=0,
              kzx=0, kzy=0, kzz=0;

  visc_.diffusionViscous( x, y, z, time, q, kxx, kxy, kxz, kyx, kyy, kyz, kzx, kzy, kzz );

  f -= ( (nx*kxx + ny*kyx + nz*kzx)*qx +
         (nx*kxy + ny*kyy + nz*kzy)*qy +
         (nx*kxz + ny*kyz + nz*kzz)*qz );
#endif
}


// viscous flux
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Tgu, class Tf>
inline void
PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>::perturbedGradFluxViscous(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,  const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    const ArrayQ<Tgu>& dqx, const ArrayQ<Tgu>& dqy, const ArrayQ<Tgu>& dqz,
    ArrayQ<Tf>& f, ArrayQ<Tf>& g, ArrayQ<Tf>& h  ) const
{
  MatrixQ<Tf> kxx = 0, kxy =0, kxz = 0;
  MatrixQ<Tf> kyx = 0, kyy =0, kyz = 0;
  MatrixQ<Tf> kzx = 0, kzy =0, kzz = 0;

  diffusionViscous(x, y, z, time, q, qx, qy, qz,
                   kxx, kxy, kxz, kyx, kyy, kyz, kzx, kzy, kzz);

  f -= kxx*dqx + kxy*dqy + kxz*dqz;
  g -= kyx*dqx + kyy*dqy + kyz*dqz;
  h -= kzx*dqx + kzy*dqy + kzz*dqz;
}


// viscous flux
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tk, class Tgu, class Tf>
inline void
PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>::perturbedGradFluxViscous(
    const Real& x, const Real& y,  const Real& z, const Real& time,
    const MatrixQ<Tk>& kxx, const MatrixQ<Tk>& kxy,  const MatrixQ<Tk>& kxz,
    const MatrixQ<Tk>& kyx, const MatrixQ<Tk>& kyy,  const MatrixQ<Tk>& kyz,
    const MatrixQ<Tk>& kzx, const MatrixQ<Tk>& kzy,  const MatrixQ<Tk>& kzz,
    const ArrayQ<Tgu>& dux, const ArrayQ<Tgu>& duy,  const ArrayQ<Tgu>& duz,
    ArrayQ<Tf>& f, ArrayQ<Tf>& g, ArrayQ<Tf>& h ) const
{
  f -= kxx*dux + kxy*duy + kxz*duz;
  g -= kyx*dux + kyy*duy + kyz*duz;
  h -= kzx*dux + kzy*duy + kzz*duz;
}

// viscous diffusion matrix: d(Fv)/d(UX)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Tk>
inline void
PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>::diffusionViscous(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxz,
    MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyz,
    MatrixQ<Tk>& kzx, MatrixQ<Tk>& kzy, MatrixQ<Tk>& kzz ) const
{
  visc_.diffusionViscous( x, y, z, time,
                          q, qx, qy, qz,
                          kxx, kxy, kxz,
                          kyx, kyy, kyz,
                          kzx, kzy, kzz );
}

template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Th, class Tk>
inline void
PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>::diffusionViscousGradient(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    const ArrayQ<Th>& qxx,
    const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
    MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kxz_x, MatrixQ<Tk>& kyx_x, MatrixQ<Tk>& kzx_x,
    MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y, MatrixQ<Tk>& kyz_y, MatrixQ<Tk>& kzy_y,
    MatrixQ<Tk>& kxz_z, MatrixQ<Tk>& kyz_z, MatrixQ<Tk>& kzx_z, MatrixQ<Tk>& kzy_z, MatrixQ<Tk>& kzz_z ) const
{
  visc_.diffusionViscousGradient( x, y, z, time,
                                  q, qx, qy, qz,
                                  qxx,
                                  qxy, qyy,
                                  qxz, qyz, qzz,
                                  kxx_x, kxy_x, kxz_x, kyx_x, kzx_x,
                                  kxy_y, kyx_y, kyy_y, kyz_y, kzy_y,
                                  kxz_z, kyz_z, kzx_z, kzy_z, kzz_z );
}

// jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Tf>
void
PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>::jacobianFluxViscous(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q,
    const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& az ) const
{
  visc_.jacobianFluxViscous( x, y, z, time, q, qx, qy, qz, ax, ay, az );
}

// strong form of viscous flux: d(Fv)/d(X)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Th, class Tf>
inline void
PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>::strongFluxViscous(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    const ArrayQ<Th>& qxx,
    const ArrayQ<Th>& qyx, const ArrayQ<Th>& qyy,
    const ArrayQ<Th>& qzx, const ArrayQ<Th>& qzy, const ArrayQ<Th>& qzz,
    ArrayQ<Tf>& strongPDE ) const
{
  visc_.strongFluxViscous( x, y, z, time, q, qx, qy, qz,
                           qxx,
                           qyx, qyy,
                           qzx, qzy, qzz, strongPDE );
}

// solution-dependent source: S(X, Q, QX)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Ts>
inline void
PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>::source(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    ArrayQ<Ts>& source ) const
{
  source_.source(x, y, z, time, q, qx, qy, qz, source);
}

// dual-consistent source
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Ts>
inline void
PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>::sourceTrace(
    const Real& xL, const Real& yL, const Real& zL,
    const Real& xR, const Real& yR, const Real& zR, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qzL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qzR,
    ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
{
  source_.sourceTrace(xL, yL, zL,
                      xR, yR, zR, time,
                      qL, qxL, qyL, qzL,
                      qR, qxR, qyR, qzR,
                      sourceL, sourceR);
}


// jacobian of source wrt conservation variables: d(S)/d(U)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Tu, class Tgu, class Ts>
inline void
PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>::perturbedSource(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    const ArrayQ<Tu>& du, const ArrayQ<Tgu>& dux, const ArrayQ<Tgu>& duy, const ArrayQ<Tgu>& duz,
    ArrayQ<Ts>& dS ) const
{
  typedef typename promote_Surreal<Tq,Tg>::type T;

  MatrixQ<T> dsdu = 0;
  jacobianSource(x, y, z, time, q, qx, qy, qz, dsdu);

  dS += dsdu*du;

  MatrixQ<T> dsdux =0, dsduy = 0, dsduz=0;
  jacobianGradientSource(x, y, z, time, q, qx, qy, qz, dsdux, dsduy, dsduz);
  dS += dsdux*dux + dsduy*duy + dsduz*duz;
}


// jacobian of source wrt conservation variables: d(S)/d(U)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Ts>
inline void
PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>::jacobianSource(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    MatrixQ<Ts>& dsdu ) const
{
  source_.jacobianSource(x, y, z, time, q, qx, qy, qz, dsdu);
}

// jacobian of source wrt conservation variables: d(S)/d(U)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Ts>
inline void
PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>::jacobianSourceAbsoluteValue(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    MatrixQ<Ts>& dsdu ) const
{
  source_.jacobianSourceAbsoluteValue(x, y, z, time, q, qx, qy, qz, dsdu);
}




// jacobian of source wrt conservation variables: d(S)/d(U)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Ts>
inline void
PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>::jacobianGradientSource(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy, MatrixQ<Ts>& dsduz ) const
{
  source_.jacobianGradientSource(x, y, z, time, q, qx, qy, qz, dsdux, dsduy, dsduz);
}

// jacobian of source wrt conservation variables: d(S)/d(U)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Th, class Ts>
inline void
PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>::jacobianGradientSourceGradient(
    const Real& x, const Real& y, const Real& z, const Real& time,
        const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
        const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
        const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
        MatrixQ<Ts>& div_dsdgradu ) const
{
  source_.jacobianGradientSourceGradient(x, y, z, time, q, qx, qy, qz,
                                         qxx, qxy, qyy, qxz, qyz, qzz, div_dsdgradu);
}

// right-hand-side forcing function
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class T>
inline void
PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>::forcingFunction(
    const Real& x, const Real& y, const Real& z, const Real& time, ArrayQ<T>& forcing ) const
{
  SANS_ASSERT(force_ != NULL);
  ArrayQ<Real> RHS = 0;
  (*force_)(*this, x, y, z, time, RHS );
  forcing += RHS;
}


// characteristic speed (needed for timestep)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class T>
inline void
PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>::speedCharacteristic(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const Real& dx, const Real& dy, const Real& dz, const ArrayQ<T>& q, Real& speed ) const
{
  MatrixQ<T> ax = 0, ay = 0, az = 0;
  //Real eps = 1e-9;

  jacobianFluxAdvective(x, y, z, time, q, ax, ay, az);

  T uh = dx*ax + dy*ay + dz*az;

  speed = fabs(uh)/sqrt(dx*dx + dy*dy + dz*dz);
}


// characteristic speed (needed for timestep)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class T>
inline void
PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>::speedCharacteristic(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<T>& q, Real& speed ) const
{
  MatrixQ<T> ax = 0, ay = 0, az = 0;

  jacobianFluxAdvective(x, y, z, time, q, ax, ay, az);

  speed = sqrt(ax*ax + ay*ay + az*az);
}

// PDE specific stabilization matrix
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tf>
inline void
PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>::stabMatrix(
    const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<Tq>& q,
    const Tq& cn1, const Tq& cn2, const Tq& cn3, MatrixQ<Tf>& dfdu ) const
{
//  SANS_DEVELOPER_EXCEPTION("AD PDE SPECIFIC STABILIZATION NOT IMPLEMENTED");
}

// PDE specific function to accumulate basis functions for stabilization matrix
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq>
inline void
PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>::getCN(
    const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<Tq>& q,
    const Real& nx, const Real& ny, const Real& nz, Tq& cn1, Tq& cn2, Tq& cn3 ) const
{
//  SANS_DEVELOPER_EXCEPTION("AD PDE SPECIFIC STABILIZATION NOT IMPLEMENTED");
}


// update fraction needed for physically valid state
template<class AdvectiveFlux, class ViscousFlux, class Source>
void
PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>::updateFraction(
    const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
    const Real maxChangeFraction, Real& updateFraction ) const
{
  SANS_DEVELOPER_EXCEPTION("Not tested");
#if 0
  // Limit the update
  ArrayQ<Real> nq = q - dq;

  updateFraction = 1;

  //q - updateFraction*dq >= (1-maxChangeFraction)*q
  if (nq < (1-maxChangeFraction)*q)
    updateFraction =  maxChangeFraction*q/dq;

  //q - updateFraction*dq <= (1+maxChangeFraction)*q
  if (nq > (1+maxChangeFraction)*q)
    updateFraction = -maxChangeFraction*q/dq;
#endif
}


// is state physically valid
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class T>
inline bool
PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>::isValidState( const ArrayQ<T>& q ) const
{
  return true;
}


// set from primitive variable array
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class T>
inline void
PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>::setDOFFrom(
    ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn >= N);
  q = data[0];
}

// interpret residuals of the solution variable
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class T>
void
PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>::interpResidVariable(
    const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
{
  SANS_ASSERT(rsdPDEOut.m() == 1);
  rsdPDEOut[0] = rsdPDEIn;
}

// interpret residuals of the gradients in the solution variable
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class T>
void
PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>::interpResidGradient(
    const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const
{
  SANS_ASSERT(rsdAuxOut.m() == 1);
  rsdAuxOut[0] = rsdAuxIn;
}

// interpret residuals at the boundary (should forward to BCs)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class T>
void
PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>::interpResidBC(
    const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const
{
  SANS_ASSERT(rsdBCOut.m() == 1);
  rsdBCOut[0] = rsdBCIn;
}

// how many residual equations are we dealing with
template<class AdvectiveFlux, class ViscousFlux, class Source>
inline int
PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>::nMonitor() const
{
  return 1;
}

template<class AdvectiveFlux, class ViscousFlux, class Source>
void
PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PDEAdvectionDiffusion3D: adv_ =" << std::endl;
  adv_.dump(indentSize+2, out);
  out << indent << "PDEAdvectionDiffusion3D: visc_ =" << std::endl;
  visc_.dump(indentSize+2, out);
  out << indent << "PDEAdvectionDiffusion3D: force_ =" << std::endl;
  if (force_ != NULL)
    (*force_).dump(indentSize+2, out);
  else
    out << " NULL" << std::endl;
}

} //namespace SANS

#endif  // PDEADVECTIONDIFFUSION3D_H
