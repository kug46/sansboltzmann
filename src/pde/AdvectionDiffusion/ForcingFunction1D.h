// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FORCINGFUNCTION1D_H
#define FORCINGFUNCTION1D_H

// 1-D Advection-Diffusion PDE: RHS forcing function

#include "tools/SANSnumerics.h"     // Real
#include "AdvectionDiffusion_Traits.h"
#include "pde/ForcingFunctionBase.h"

#include <cmath>
#include <iostream>

namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: RHS forcing function
//
// member functions:
//   .()      functor returning forcing function source term
//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//
// forcing function: constant
template<class PDE>
class ForcingFunction1D_Const : public ForcingFunctionBase1D<PDE>
{
public:
  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  static const int D = 1;                     // physical dimensions

  explicit ForcingFunction1D_Const( Real c ) : c_(c) {}
  ~ForcingFunction1D_Const() {}

  bool hasForcingFunction() const override
  {
    return true;
  }

  void operator()( const PDE& pde, const Real &x, const Real &time, ArrayQ<Real>& forcing ) const override
  {
    forcing += c_;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const override;

private:
  Real c_;
};

template<class PDE>
void ForcingFunction1D_Const<PDE>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "ForcingFunction1D_Const:  c_ = " << c_ << std::endl;
}


//----------------------------------------------------------------------------//
// forcing function: monomial
// NOTE: code for Laplacian only
template<class PDE>
class ForcingFunction1D_Monomial : public ForcingFunctionBase1D<PDE>
{
public:
  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  static const int D = 2;                     // physical dimensions

  explicit ForcingFunction1D_Monomial( int i, Real nu ) : i_(i), nu_(nu) {}
  ~ForcingFunction1D_Monomial() {}

  bool hasForcingFunction() const override
  {
    return true;
  }

  void operator()( const PDE& pde, const Real &x, const Real &time, ArrayQ<Real>& forcing ) const override
  {
    forcing += -nu_*( pow(x, i_-2)*i_*(i_ - 1) );
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const override;

private:
  int i_;
  Real nu_;
};

template<class PDE>
void ForcingFunction1D_Monomial<PDE>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "ForcingFunction1D_Monomial:" << std::endl;
}

//----------------------------------------------------------------------------//
// forcing function: for solution sin(2*PI*x)

template<class PDE>
class ForcingFunction1D_Sine : public ForcingFunctionBase1D<PDE>
{
public:
  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  static const int D = 1;                     // physical dimensions

  ForcingFunction1D_Sine( Real a, Real kxx ) : a_(a), kxx_(kxx) {}
  ~ForcingFunction1D_Sine() {}

  bool hasForcingFunction() const override
  {
    return true;
  }

  void operator()( const PDE& pde, const Real &x, const Real &time, ArrayQ<Real>& forcing ) const override
  {
    Real snx = sin(2*PI*x), csx = cos(2*PI*x);

    forcing += 2*PI*(csx*(a_) + snx*(2*PI*kxx_));
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const override;


private:
  Real a_;      // advection velocity
  Real kxx_;    // diffusion coefficients
};

template< class PDE>
void ForcingFunction1D_Sine<PDE>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "ForcingFunction1D_Sine:" << std::endl;
}


//----------------------------------------------------------------------------//
// forcing function: for solution sin(2*PI*x)*sin(2*PI*t)

template<class PDE>
class ForcingFunction1D_SineUnsteady : public ForcingFunctionBase1D<PDE>
{
public:
  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  static const int D = 1;                     // physical dimensions

  ForcingFunction1D_SineUnsteady( Real a, Real kxx ) : a_(a), kxx_(kxx) {}
  ~ForcingFunction1D_SineUnsteady() {}

  bool hasForcingFunction() const override
  {
    return true;
  }

  void operator()( const PDE& pde, const Real &x, const Real &time, ArrayQ<Real>& forcing ) const override
  {
    Real snx = sin(2*PI*x), csx = cos(2*PI*x);
    Real snt = sin(2*PI*time), cst = cos(2*PI*time);

    forcing += 2*PI*( csx*(a_) + snx*(2*PI*kxx_) )*snt + 2*PI*snx*cst;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const override;


private:
  Real a_;      // advection velocity
  Real kxx_;    // diffusion coefficients
};

template<class PDE>
void ForcingFunction1D_SineUnsteady<PDE>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "ForcingFunction1D_SineUnsteady:"
                << "  a_ = " << a_
                << "  kxx_ = " << kxx_ << std::endl;
}


//----------------------------------------------------------------------------//
// forcing function: for solution sin(2*PI*x)*sin(2*PI*t)

template<class PDE>
class ForcingFunction1D_SineUnsteady_Const : public ForcingFunctionBase1D<PDE>
{
public:
  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  static const int D = 1;                     // physical dimensions

  ForcingFunction1D_SineUnsteady_Const( Real a, Real kxx) {}
  ~ForcingFunction1D_SineUnsteady_Const() {}

  bool hasForcingFunction() const override { return true; }

  void operator()( const PDE& pde, const Real &x, const Real &time, ArrayQ<Real>& forcing ) const override
  {
    forcing += 2*PI*cos(2*PI*time);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const override;


private:
};

template<class PDE>
void ForcingFunction1D_SineUnsteady_Const<PDE>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "ForcingFunction1D_SineUnsteady_Const:";
}

//----------------------------------------------------------------------------//
// forcing function: b*tanh(x / 2nu)

template<class PDE>
class ForcingFunction1D_Tanh : public ForcingFunctionBase1D<PDE>
{
public:
  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  static const int D = 1;                     // physical dimensions

  explicit ForcingFunction1D_Tanh( Real b, Real nu ) : b_(b), nu_(nu) {}
  ~ForcingFunction1D_Tanh() {}

  bool hasForcingFunction() const override { return true;}

  void operator()( const PDE& pde, const Real &x, const Real &time, ArrayQ<Real>& forcing ) const override
  {
    forcing -= b_*tanh((x)/2/nu_);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const override;

private:
  Real b_;
  Real nu_;
};

template<class PDE>
void ForcingFunction1D_Tanh<PDE>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "ForcingFunction1D_Tanh:"
      << "  b_ = "  << b_
      << "  nu_ = " << nu_ << std::endl;
}


} //namespace SANS

#endif  // FORCINGFUNCTION1D_H
