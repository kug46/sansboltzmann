// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOLUTIONFUNCTION1D_H
#define SOLUTIONFUNCTION1D_H

// 1-D Advection-Diffusion PDE: exact and MMS solutions

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// solution: pde forcing function
template<class PDE>
class SolutionFunction1D_ForcingFunction
{
public:
  typedef PhysD1 PhysDim;

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  typedef ParamsNone ParamsType;

  //cppcheck-suppress noExplicitConstructor
  SolutionFunction1D_ForcingFunction( const PDE &pde ) : pde_(pde) {}
  ~SolutionFunction1D_ForcingFunction() {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
    ArrayQ<T> q = 0;
    pde_.forcingFunction(x, time, q);
    return q;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "SolutionFunction1D_ForcingFunction:" << std::endl;
  }
protected:
  const PDE& pde_;
};

} // namespace SANS

#endif  // SOLUTIONFUNCTION1D_H
