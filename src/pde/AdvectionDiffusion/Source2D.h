// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOURCE2D_H
#define SOURCE2D_H

// 2-D Advection-Diffusion PDE: source term

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "Surreal/PromoteSurreal.h"
#include "AdvectionDiffusion_Traits.h"

#include <iostream>
#include <cmath>

namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: source
//
// member functions:
//   .()      functor returning source
//----------------------------------------------------------------------------//

class Source2D_None
{
public:
  template<class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD2>::ArrayQ<T>;

  template<class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD2>::MatrixQ<T>;

  bool hasSourceTerm() const { return false; }
  bool hasSourceTrace() const { return false; }
  bool needsSolutionGradientforSource() const { return false; }

  template<class Tq, class Tg, class Ts>
  void source( const Real& x, const Real& y, const Real& time,
               const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
               ArrayQ<Ts>& source ) const
  {
  }

  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& xL, const Real& yL,
      const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
  {
  }

  template <class Tq, class Tg, class Ts>
  void jacobianSource(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const
  {
  }

  template <class Tq, class Tg, class Ts>
  void jacobianGradientSource(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy  ) const
  {
  }

  template <class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const  {}

  // div of jacobian of source wrt gradient: div.d(S)/d(Ux)
  template <class Tq, class Tg, class Th, class Ts>
  void jacobianGradientSourceGradient(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      MatrixQ<Ts>& div_dsdgradu ) const {}

//  void dump( int indentSize=0, std::ostream& out = std::cout ) const = 0;

};

//----------------------------------------------------------------------------//
// Source: Unform
//
// source = a*q where 'a' is a constant
//----------------------------------------------------------------------------//

class Source2D_Uniform
{
public:
  template<class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD2>::ArrayQ<T>;

  template<class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD2>::MatrixQ<T>;

  explicit Source2D_Uniform( const Real a) : a_(a) {}

  bool hasSourceTerm() const { return true; }
  bool hasSourceTrace() const { return false; }
  bool needsSolutionGradientforSource() const { return false; }

  template<class Tq, class Tg, class Ts>
  void source( const Real& x, const Real& y, const Real& time,
               const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
               ArrayQ<Ts>& source ) const
  {
    source += a_*q;
  }

  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& xL, const Real& yL, const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
  {
    sourceL += a_*qL;
    sourceR += a_*qR;
  }

  template <class Tq, class Tg, class Ts>
  void jacobianSource(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const
  {
    dsdu += a_;
  }

  template <class Tq, class Tg, class Ts>
  void jacobianGradientSource(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy  ) const
  {
  }


  template <class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const  {}


  // div of jacobian of source wrt gradient: div.d(S)/d(Ux)
  template <class Tq, class Tg, class Th, class Ts>
  void jacobianGradientSourceGradient(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      MatrixQ<Ts>& div_dsdgradu ) const {}


protected:
  const Real a_;
};

//----------------------------------------------------------------------------//
// Source: Uniform with Gradient
//
// source = a*q + b*qx + c*qy where a, b, and c are constants
//----------------------------------------------------------------------------//

class Source2D_UniformGrad
{
public:
  template<class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD2>::ArrayQ<T>;

  template<class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD2>::MatrixQ<T>;

  explicit Source2D_UniformGrad( const Real a, const Real b, const Real c) : a_(a), b_(b), c_(c) {}

  bool hasSourceTerm() const { return true; }
  bool hasSourceTrace() const { return false; }
  bool needsSolutionGradientforSource() const { return true; }

  template<class Tq, class Tg, class Ts>
  void source( const Real& x, const Real& y, const Real& time,
               const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
               ArrayQ<Ts>& source ) const
  {
    source += a_*q + b_*qx + c_*qy;
  }

  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& xL, const Real& yL, const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
  {
    sourceL += a_*qL + b_*qxL + c_*qyL;
    sourceR += a_*qR + b_*qxR + c_*qyR;
  }

  template <class Tq, class Tg, class Ts>
  void jacobianSource(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const
  {
    dsdu += a_;
  }
//  void dump( int indentSize=0, std::ostream& out = std::cout ) const = 0;
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSource(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy  ) const
  {
    dsdux += b_;
    dsduy += c_;
  }

  template <class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const
  {
    dsdu += fabs(a_);
  }

  // div of jacobian of source wrt gradient: div.d(S)/d(Ux)
  template <class Tq, class Tg, class Th, class Ts>
  void jacobianGradientSourceGradient(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      MatrixQ<Ts>& div_dsdgradu ) const {}

protected:
  const Real a_;
  const Real b_;
  const Real c_;
};

} //namespace SANS

#endif  // SOURCE2D_H
