// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// 3D diffusion matrix

#include <string>
#include <iostream>

#include "ViscousFlux3D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// diffusion matrix: uniform

void
DiffusionMatrix3D_Uniform::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "DiffusionMatrix3D_Uniform:"
            << "  kxx_ = " << kxx_ << "  kxy_ = " << kxy_ << "  kxz_ = " << kxz_
            << "  kyx_ = " << kyx_ << "  kyy_ = " << kyy_ << "  kyz_ = " << kyz_
            << "  kzx_ = " << kzx_ << "  kzy_ = " << kzy_ << "  kzz_ = " << kzz_
            << std::endl;
}
void
DiffusionMatrix3D_Poly::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "DiffusionMatrix3D_Poly:"
      << " A_ = " << A_
      << " B_ = " << B_
      << std::endl;
}

} //namespace SANS
