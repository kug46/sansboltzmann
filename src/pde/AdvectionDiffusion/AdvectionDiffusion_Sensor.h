// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ADVECTIONDIFFUSION_SENSOR_H
#define ADVECTIONDIFFUSION_SENSOR_H

#include "tools/SANSnumerics.h"     // Real

#include "AdvectionDiffusion_Traits.h"

#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include <iostream>
#include <string>

namespace SANS
{

class AdvectionDiffusion_Sensor
{
public:
  template<class T> using ArrayQ  = T; // solution/flux arrays

  template<class PDE>
  explicit AdvectionDiffusion_Sensor(const PDE& pde)
  {
    // Nothing
  }

  ~AdvectionDiffusion_Sensor() {}

  // variable used in calculating jump switch for artificial viscosity
  template<class T, class Tg>
  void jumpQuantity( const ArrayQ<T>& q, Tg& g ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:

};

template<class T, class Tg>
void inline
AdvectionDiffusion_Sensor::
jumpQuantity( const ArrayQ<T>& q, Tg& g ) const
{
  g = DLA::index(q, 0);
}

void inline
AdvectionDiffusion_Sensor::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "AdvectionDiffusion_Sensor" << std::endl;
}

} //namespace SANS

#endif  // ADVECTIONDIFFUSION_SENSOR_H
