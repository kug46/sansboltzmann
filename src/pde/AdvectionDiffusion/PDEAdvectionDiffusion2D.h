// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDEADVECTIONDIFFUSION2D_H
#define PDEADVECTIONDIFFUSION2D_H

// 2-D Advection-Diffusion PDE class

#include "tools/SANSnumerics.h"     // Real
#include "AdvectionDiffusion_Traits.h"
#include "AdvectiveFlux2D.h"
#include "ViscousFlux2D.h"
#include "ForcingFunction2D.h"
#include "Source2D.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "Topology/ElementTopology.h"
#include "Topology/Dimension.h"
#include "pde/ForcingFunctionBase.h"

#include <cmath>              // sqrt
#include <iostream>
#include <string>

namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: 2-D Advection-Diffusion
//
// template parameters:
//   T                        solution DOF data type (e.g. double)
//
// member functions:
//   .hasFluxAdvectiveTime        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//   .hasFluxViscous             T/F: PDE has viscous flux term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: U(Q)
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .fluxViscous                viscous fluxes: Fv(Q, QX)
//   .diffusionViscous           viscous diffusion coefficient: d(Fv)/d(UX)
//   .isValidState               T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom                 set from primitive variable array
//----------------------------------------------------------------------------//

template<class AdvectiveFlux, class ViscousFlux, class Source>
class PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>
{
public:
  typedef PhysD2 PhysDim;
  static const int D = AdvectionDiffusionTraits<PhysD2>::D;   // physical dimensions
  static const int N = AdvectionDiffusionTraits<PhysD2>::N;   // total solution variables

  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD2>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD2>::template MatrixQ<T>; // matrices

  typedef ForcingFunctionBase2D<PDEAdvectionDiffusion> ForcingFunctionType;

  PDEAdvectionDiffusion( const AdvectiveFlux& adv, const ViscousFlux& visc,
                         const Source &src, const std::shared_ptr<ForcingFunctionType>& force = NULL  ) :
      adv_(adv), visc_(visc), source_(src), force_(force) {}

  ~PDEAdvectionDiffusion() {}

  PDEAdvectionDiffusion( const PDEAdvectionDiffusion& ) = delete;
  PDEAdvectionDiffusion& operator=( const PDEAdvectionDiffusion& ) = delete;

  // flux components
  bool hasFluxAdvectiveTime() const { return true; }
  bool hasFluxAdvective() const { return adv_.hasFluxAdvective(); }
  bool hasFluxViscous() const { return visc_.hasFluxViscous(); }
  bool hasSource() const { return source_.hasSourceTerm(); }
  bool hasSourceTrace() const { return source_.hasSourceTrace(); }
  bool hasSourceLiftedQuantity() const { return false; }
  bool hasForcingFunction() const { return force_ != NULL && force_->hasForcingFunction(); }

  // viscous flux is of the form K(q).gradu
  bool fluxViscousLinearInGradient() const { return visc_.fluxViscousLinearInGradient(); }
  bool needsSolutionGradientforSource() const { return source_.needsSolutionGradientforSource(); }

  const AdvectiveFlux& getAdvectiveFlux() const { return adv_; }
  const ViscousFlux& getViscousFlux() const { return visc_; }

  // unsteady temporal flux: Ft(Q)
  template <class T, class Tf>
  void fluxAdvectiveTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tf>& ft ) const
  {
    T ftLocal = 0;
    masterState(x, y, time, q, ftLocal);
    ft += ftLocal;
  }

  // unsteady temporal flux: Ft(Q)
  template <class Tp, class T, class Tf>
  void fluxAdvectiveTime(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tf>& ft ) const
  {
    fluxAdvectiveTime(x, y, time, q, ft);
  }

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <class T>
  void jacobianFluxAdvectiveTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& J ) const
  {
    J += DLA::Identity();
  }

  // master state: U(Q)
  template <class T>
  void masterState(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, ArrayQ<T>& uCons ) const;

  // master state: U(Q)
  template <class Tp, class T>
  void masterState(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, ArrayQ<T>& uCons ) const
  {
    masterState(x, y, time, q, uCons);
  }


  // master state: U(Q)
  template<class Tq, class Tqp, class Tf>
  void perturbedMasterState(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& dq, ArrayQ<Tf>& du ) const { du = dq; }

  // master state Gradient: Ux(Q)
  template<class Tq, class Tg, class Tf>
  void masterStateGradient(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& uConsx, ArrayQ<Tf>& uConsy  ) const
  {
    uConsx = qx; uConsy = qy;
  }

  // master state Hessian: Uxx(Q), Uxy(Q), Uyy(Q)
  template<class Tq, class Tg, class Th, class Tf>
  void masterStateHessian(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      ArrayQ<Tf>& uConsxx, ArrayQ<Tf>& uConsxy, ArrayQ<Tf>& uConsyy ) const

  {
    uConsxx = qxx; uConsxy = qxy; uConsyy = qyy;
  }


  // master state: U(Q)
  template<class Tq, class Tf>
  void adjointState(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, ArrayQ<Tf>& V ) const
  {
    V = q;
  }

  // master state Gradient: Ux(Q)
  template<class Tq, class Tg, class Tf>
  void adjointStateGradient(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& Vx, ArrayQ<Tf>& Vy  ) const
  {
    Vx = qx; Vy = qy;
  }

  // master state Hessian: Uxx(Q), Uxy(Q), Uyy(Q)
  template<class Tq, class Tg, class Th, class Tf>
  void adjointStateHessian(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      ArrayQ<Tf>& Vxx, ArrayQ<Tf>& Vxy, ArrayQ<Tf>& Vyy ) const
  {
    Vxx = qxx; Vxy = qxy; Vyy = qyy;
  }

  // unsteady conservative flux Jacobian: dU(Q)/dQ
  template<class T>
  void jacobianMasterState(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const;


  // unsteady conservative flux Jacobian: dU(Q)/dQ
  template<class Tp, class T>
  void jacobianMasterState(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
  {
    jacobianMasterState(x, y, time, q, dudq);
  }

  // strong form of unsteady conservative flux: dU(Q)/dt
  template <class Tq, class Tg, class Tf>
  void strongFluxAdvectiveTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qt, ArrayQ<Tf>& strongPDE ) const;


  // advective flux: F(Q)
  template <class T, class Tf>
  void fluxAdvective(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tf>& fx, ArrayQ<Tf>& fy ) const;

  template <class Tq, class Tf>
  void fluxAdvectiveUpwind(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f, const Real& scale = 1  ) const;

  template <class Tq, class Tf>
  void fluxAdvectiveUpwindSpaceTime(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, const Real& ny, const Real& nt, ArrayQ<Tf>& f, const Real& scale= 1 ) const;

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class Tq, class Tf>
  void jacobianFluxAdvective(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const Real& nx, const Real& ny,
      MatrixQ<Tf>& a ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const Real& nx, const Real& ny, const Real& nt,
      MatrixQ<Tf>& a ) const;

  // strong form advective fluxes
  template <class Tq, class Tg, class Tf>
  void strongFluxAdvective(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& strongPDE ) const;


  // viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const;

  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      const Real& nx, const Real& ny,
      ArrayQ<Tf>& f ) const;


  // perturbation to viscous flux from dux, duy
  template <class Tq, class Tg, class Tgu, class Tf>
  void perturbedGradFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Tgu>& dqx, const ArrayQ<Tgu>& dqy,
      ArrayQ<Tf>& df, ArrayQ<Tf>& dg ) const;

  // perturbation to viscous flux from dux, duy
  template <class Tk, class Tgu, class Tf>
  void perturbedGradFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const MatrixQ<Tk>& kxx, const MatrixQ<Tk>& kxy, const MatrixQ<Tk>& kyx, const MatrixQ<Tk>& kyy,
      const ArrayQ<Tgu>& dux, const ArrayQ<Tgu>& duy,
      ArrayQ<Tf>& df, ArrayQ<Tf>& dg ) const;

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const;

  template <class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kyx_x,
      MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y ) const;

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay ) const;

  // strong form viscous fluxes: -d(Fv)/dx - d(Fv)/d(y)
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      ArrayQ<Tf>& strongPDE ) const;

  // space-time viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& ft ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial viscous flux
    fluxViscous(x, y, time, q, qx, qy, fx, fy);
  }

  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qtL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qtR,
      const Real& nx, const Real& ny, const Real& nt,
      ArrayQ<Tf>& f ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial viscous flux
    fluxViscous(x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, f);
  }

  // space-time viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxt,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyt,
      MatrixQ<Tk>& ktx, MatrixQ<Tk>& kty, MatrixQ<Tk>& ktt ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial diffusion matrix
    diffusionViscous(x, y, time, q, qx, qy, kxx, kxy, kyx, kyy);
  }

  // space-time jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& at ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial jacobianFluxViscous
    jacobianFluxViscous(x, y, time, q, qx, qy, ax, ay);
  }

  // space-time strong form viscous fluxes: -d(Fv)/dx - d(Fv)/d(y)
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxt, const ArrayQ<Th>& qyt, const ArrayQ<Th>& qtt,
      ArrayQ<Tf>& strongPDE ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial strongFluxViscous
    strongFluxViscous(x, y, time, q, qx, qy, qxx, qxy, qyy, strongPDE);
  }

  // solution-dependent source: S(X, Q, QX)
  template <class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Ts>& source ) const;

  // Forward call to S(Q+QP, QX+QPX)
  template <class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void source(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy,
      ArrayQ<Ts>& src ) const
  {
    typedef typename promote_Surreal<Tq, Tqp>::type Tqq;
    typedef typename promote_Surreal<Tg, Tgp>::type Tgg;

    ArrayQ<Tqq> qq = q + qp;
    ArrayQ<Tgg> qqx = qx + qpx;
    ArrayQ<Tgg> qqy = qy + qpy;

    source(x, y, time, qq, qqx, qqy, src);
  }

  // Forward call to S(Q,QP, QX,QPX)
  template <class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void sourceCoarse(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy,
      ArrayQ<Ts>& src ) const
  {
    source(x, y, time, q, qp, qx, qy, qpx, qpy, src);
  }

  // Forward call to S(Q,QP, QX,QPX)
  template <class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void sourceFine(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy,
      ArrayQ<Ts>& src ) const
  {
    source(x, y, time, q, qp, qx, qy, qpx, qpy, src);
  }

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX)
  template <class Tlq, class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& y, const Real& time,
      const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Ts>& s ) const
  {
    source(x, y, time, q, qx, qy, s); //forward to regular source
  }

  // linear change in source in response to linear perturbations du, dux, duy
  template <class Tq, class Tg, class Tu, class Tgu, class Ts>
  void perturbedSource(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Tu>& du, const ArrayQ<Tgu>& dux, const ArrayQ<Tgu>& duy, ArrayQ<Ts>& dS ) const;

  // dual-consistent source
  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& xL, const Real& yL, const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const;

  // trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
  template <class Tq, class Ts>
  void sourceLiftedQuantity(
      const Real& xL, const Real& yL, const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, Ts& s ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianSource(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const;

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianSourceHACK(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const
  {
    jacobianSource(x, y, time, q, qx, qy, dsdu);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const;

  // jacobian of source wrt conservation variables: d(S)/d(Ux), d(S)/d(Uy)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSource(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy ) const;

  // jacobian of source wrt conservation variables: d(S)/d(Ux), d(S)/d(Uy)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSourceHACK(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy ) const
  {
    jacobianGradientSource(x, y, time, q, qx, qy, dsdux, dsduy);
  }

  template <class Tq, class Tg, class Ts>
  void jacobianGradientSourceAbsoluteValue(
      const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& divSdotN ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Th, class Ts>
  void jacobianGradientSourceGradient(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      MatrixQ<Ts>& dsdux_x ) const;

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Th, class Ts>
  void jacobianGradientSourceGradientHACK(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      MatrixQ<Ts>& dsdux_x ) const
  {
    jacobianGradientSourceGradient(x, y, time, q, qx, qy, qxx, qxy, qyy, dsdux_x);
  }

  // right-hand-side forcing function
  template <class T>
  void forcingFunction( const Real& x, const Real& y, const Real& time, ArrayQ<T>& forcing ) const;

  // characteristic speed (needed for timestep)
  template <class Tq, class Tc>
  void speedCharacteristic(
      const Real& x, const Real& y, const Real& time, const Real& dx, const Real& dy, const ArrayQ<Tq>& q, Tc& speed ) const;

  // characteristic speed
  template <class Tq, class Tc>
  void speedCharacteristic(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q, Tc& speed ) const;

  // characteristic speed
  template <class Tp, class Tq, class Tc>
  void speedCharacteristic(
      const Tp& param, const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q, Tc& speed ) const;

  // update fraction needed for physically valid state
  void updateFraction(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
      const Real maxChangeFraction, Real& updateFraction ) const;

  template <class Tq, class Tf>
  void getCN(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q,
      const Real nx, const Real ny, Tf Cn1, Tf Cn2 ) const {}

  template <class Tq, class Tc, class Tf>
  void stabMatrix(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const Tc Cn1, const Tc Cn2, MatrixQ<Tf>& dfdu ) const {}

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const { return true; }

  // set from primitive variable array
  template <class T>
  void setDOFFrom(
      ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const;

  // set from variable
  ArrayQ<Real> setDOFFrom( const PyDict& d ) const;

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const;

  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const;

  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const;

  // how many residual equations are we dealing with
  int nMonitor() const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const AdvectiveFlux adv_;
  const ViscousFlux visc_;
  const Source source_;
  const std::shared_ptr<ForcingFunctionType> force_;
};

template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class T>
inline void
PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::masterState(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, ArrayQ<T>& uCons ) const
{
  uCons = q;
}

template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class T>
inline void
PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::jacobianMasterState(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
{
  dudq = DLA::Identity();
}

template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Tf>
inline void
PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::strongFluxAdvectiveTime(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qt, ArrayQ<Tf>& ft ) const
{
  ft += qt;
}


template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class T, class Tf>
inline void
PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::fluxAdvective(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, ArrayQ<Tf>& fx, ArrayQ<Tf>& fy) const
{
  adv_.flux( x, y, time, q, fx, fy );
}


template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class T, class Tf>
inline void
PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::fluxAdvectiveUpwind(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx, const Real& ny,
    ArrayQ<Tf>& f, const Real& scale  ) const
{
  adv_.fluxUpwind(x, y, time, qL, qR, nx, ny, f, scale);
}

template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class T, class Tf>
inline void
PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::fluxAdvectiveUpwindSpaceTime(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx, const Real& ny, const Real& nt,
    ArrayQ<Tf>& f, const Real& scale ) const
{
  adv_.fluxUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f, scale);
}


// advective flux jacobian: d(F)/d(U)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tf>
inline void
PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::jacobianFluxAdvective(
    const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q,
    MatrixQ<Tf>& ax, MatrixQ<Tf>& ay ) const
{
  adv_.jacobian( x, y, time, q, ax, ay );
}


// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tf>
inline void
PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::jacobianFluxAdvectiveAbsoluteValue(
  const Real& x, const Real& y, const Real& time,
  const ArrayQ<Tq>& q, const Real& nx, const Real& ny, MatrixQ<Tf>& mtx ) const
{
  MatrixQ<Tq> ax = 0, ay = 0;

  jacobianFluxAdvective(x, y, time, q, ax, ay);

  //TODO: Rather arbitrary smoothing constants here
  //Real eps = 1e-8;

  Tq an = nx*ax + ny*ay;

  //mtx += smoothabs0( an, eps );
  mtx += fabs( an );
}

// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tf>
inline void
PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::jacobianFluxAdvectiveAbsoluteValueSpaceTime(
  const Real& x, const Real& y, const Real& time,
  const ArrayQ<Tq>& q, const Real& nx, const Real& ny, const Real& nt, MatrixQ<Tf>& mtx ) const
{
  MatrixQ<Tq> ax = 0, ay = 0;

  jacobianFluxAdvective(x, y, time, q, ax, ay);

  //TODO: Rather arbitrary smoothing constants here
  //Real eps = 1e-8;

  Tq an = nx*ax + ny*ay + nt*1;

  //mtx += smoothabs0( an, eps );
  mtx += fabs( an );
}


// strong form of advective flux: div.F
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Tf>
inline void
PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::strongFluxAdvective(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Tf>& strongPDE ) const
{
  adv_.strongFlux( x, y, time, q, qx, qy, strongPDE );
}


// viscous flux
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Tf>
inline void
PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::fluxViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
{
  visc_.fluxViscous( x, y, time, q, qx, qy, f, g );
}

// viscous flux: normal flux with left and right states
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Tf>
inline void
PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::fluxViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
    const Real& nx, const Real& ny,
    ArrayQ<Tf>& f ) const
{
#if 1
  ArrayQ<Tf> fL = 0, fR = 0;
  ArrayQ<Tf> gL = 0, gR = 0;

  fluxViscous(x, y, time, qL, qxL, qyL, fL, gL);
  fluxViscous(x, y, time, qR, qxR, qyR, fR, gR);

  f += 0.5*(fL + fR)*nx + 0.5*(gL + gR)*ny;
#else
  ArrayQ<Tq> q = 0.5*(qL + qR);
  ArrayQ<Tg> qx = 0.5*(qxL + qxR);
  ArrayQ<Tg> qy = 0.5*(qyL + qyR);

  MatrixQ<Tq> kxx=0, kxy=0,
              kyx=0, kyy=0;

  visc_.diffusionViscous( x, y, time, q, kxx, kxy, kyx, kyy );

  f -= ((nx*kxx + kyx*ny)*qx + (nx*kxy + ny*kyy)*qy);
#endif
}


// viscous flux
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Tgu, class Tf>
inline void
PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::perturbedGradFluxViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,  const ArrayQ<Tg>& qy,
    const ArrayQ<Tgu>& dqx, const ArrayQ<Tgu>& dqy,
    ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
{
//  MatrixQ<Tf> kxx = 0, kxy =0, kyx = 0, kyy =0;
//  diffusionViscous(x, y, time, q, qx, qy, kxx, kxy, kyx, kyy);
//
//  f -= kxx*dqx + kxy*dqy;
//  g -= kyx*dqx + kyy*dqy;
  fluxViscous(x, y, time, q, dqx, dqy, f, g);
}


// viscous flux
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tk, class Tgu, class Tf>
inline void
PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::perturbedGradFluxViscous(
    const Real& x, const Real& y, const Real& time,
    const MatrixQ<Tk>& kxx, const MatrixQ<Tk>& kxy, const MatrixQ<Tk>& kyx, const MatrixQ<Tk>& kyy,
    const ArrayQ<Tgu>& dux, const ArrayQ<Tgu>& duy,
    ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
{
  f -= kxx*dux + kxy*duy;
  g -= kyx*dux + kyy*duy;
}

// viscous diffusion matrix: d(Fv)/d(UX)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Tk>
inline void
PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::diffusionViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
    MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const
{
  visc_.diffusionViscous( x, y, time, q, qx, qy, kxx, kxy, kyx, kyy );
}



template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Th, class Tk>
void
PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::diffusionViscousGradient(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kyx_x,
    MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y ) const
{
  visc_.diffusionViscousGradient( x, y, time, q, qx, qy, qxx, qxy, qyy, kxx_x, kxy_x, kyx_x, kxy_y, kyx_y, kyy_y );
}

template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Tf>
void
PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::jacobianFluxViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q,
    const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Tf>& ax, MatrixQ<Tf>& ay ) const
{
  visc_.jacobianFluxViscous( x, y, time, q, qx, qy, ax, ay );
}

// strong form of viscous flux: d(Fv)/d(X)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Th, class Tf>
inline void
PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::strongFluxViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q,
    const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Th>& qxx,
    const ArrayQ<Th>& qyx, const ArrayQ<Th>& qyy,
    ArrayQ<Tf>& strongPDE ) const
{
  visc_.strongFluxViscous( x, y, time, q, qx, qy, qxx, qyx, qyy, strongPDE );
}

// solution-dependent source: S(X, Q, QX)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Ts>
inline void
PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::source(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Ts>& source ) const
{
  source_.source(x, y, time, q, qx, qy, source);
}

// jacobian of source wrt conservation variables: d(S)/d(U)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Tu, class Tgu, class Ts>
inline void
PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::perturbedSource(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Tu>& du, const ArrayQ<Tgu>& dux, const ArrayQ<Tgu>& duy,
    ArrayQ<Ts>& dS ) const
{
  typedef typename promote_Surreal<Tq,Tg>::type T;

  MatrixQ<T> dsdu = 0;
  jacobianSource(x, y, time, q, qx, qy, dsdu);

  dS += dsdu*du;

  MatrixQ<T> dsdux =0, dsduy = 0;
  jacobianGradientSource(x, y, time, q, qx, qy, dsdux, dsduy);

  dS += dsdux*dux + dsduy*duy;
}


// dual-consistent source
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Ts>
inline void
PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::sourceTrace(
    const Real& xL, const Real& yL, const Real& xR, const Real& yR, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
    ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
{
  source_.sourceTrace(xL, yL, xR, yR, time,
                      qL, qxL, qyL,
                      qR, qxR, qyR,
                      sourceL, sourceR);
}

// jacobian of source wrt conservation variables: d(S)/d(U)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Ts>
inline void
PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::jacobianSource(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Ts>& dsdu ) const
{
  source_.jacobianSource(x, y, time, q, qx, qy, dsdu);
}

// jacobian of source wrt conservation variables: d(S)/d(U)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Ts>
inline void
PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::jacobianSourceAbsoluteValue(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Ts>& dsdu ) const
{
  source_.jacobianSourceAbsoluteValue(x, y, time, q, qx, qy, dsdu);
}

// jacobian of source wrt conservation variables: d(S)/d(U)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Ts>
inline void
PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::jacobianGradientSource(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy ) const
{
  source_.jacobianGradientSource(x, y, time, q, qx, qy, dsdux, dsduy);
}

// jacobian of source wrt conservation variables: d(S)/d(U)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tg, class Th, class Ts>
inline void
PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::jacobianGradientSourceGradient(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    MatrixQ<Ts>& div_dsdgradu ) const
{
  source_.jacobianGradientSourceGradient(x, y, time, q, qx, qy, qxx, qxy, qyy, div_dsdgradu);
}

// right-hand-side forcing function
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class T>
inline void
PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::forcingFunction(
    const Real& x, const Real& y, const Real& time, ArrayQ<T>& forcing ) const
{
  SANS_ASSERT(force_ != NULL);
  ArrayQ<Real> RHS = 0;
  (*force_)(*this, x, y, time, RHS );
  forcing += RHS;
}


// characteristic speed (needed for timestep)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tc>
inline void
PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::speedCharacteristic(
    const Real& x, const Real& y, const Real& time,
    const Real& dx, const Real& dy, const ArrayQ<Tq>& q, Tc& speed ) const
{
  MatrixQ<Tq> ax = 0, ay = 0;
  //Real eps = 1e-9;

  jacobianFluxAdvective(x, y, time, q, ax, ay);

  Tq uh = dx*ax + dy*ay;

  speed = fabs(uh)/sqrt(dx*dx + dy*dy);
}


// characteristic speed
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class Tc>
inline void
PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::speedCharacteristic(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, Tc& speed ) const
{
  MatrixQ<Tq> ax = 0, ay = 0;
  //Real eps = 1e-9;

  jacobianFluxAdvective(x, y, time, q, ax, ay);

  speed = sqrt(ax*ax + ay*ay);
}


// characteristic speed
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tp, class Tq, class Tc>
inline void
PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::speedCharacteristic(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, Tc& speed ) const
{
  MatrixQ<Tq> ax = 0, ay = 0;
  //Real eps = 1e-9;

  jacobianFluxAdvective(x, y, time, q, ax, ay);

  speed = sqrt(ax*ax + ay*ay);
}

// update fraction needed for physically valid state
template<class AdvectiveFlux, class ViscousFlux, class Source>
void
PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::updateFraction(
    const Real& x, const Real& y, const Real& time, const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
    const Real maxChangeFraction, Real& updateFraction ) const
{
  SANS_DEVELOPER_EXCEPTION("Not tested");
#if 0
  // Limit the update
  ArrayQ<Real> nq = q - dq;

  updateFraction = 1;

  //q - updateFraction*dq >= (1-maxChangeFraction)*q
  if (nq < (1-maxChangeFraction)*q)
    updateFraction =  maxChangeFraction*q/dq;

  //q - updateFraction*dq <= (1+maxChangeFraction)*q
  if (nq > (1+maxChangeFraction)*q)
    updateFraction = -maxChangeFraction*q/dq;
#endif
}

// set from primitive variable array
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class T>
inline void
PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::setDOFFrom(
    ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn >= N);
  q = data[0];
}

// set from primitive variable array
template<class AdvectiveFlux, class ViscousFlux, class Source>
inline typename PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::template ArrayQ<Real>
PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::setDOFFrom(const PyDict& d) const
{
  SANS_DEVELOPER_EXCEPTION("PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::setDOFFrom(const PyDict& d) - Not implemented");

  // This is to prevent warnings...
  return 0;
}

// interpret residuals of the solution variable
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class T>
void
PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::interpResidVariable(
    const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
{
  SANS_ASSERT(rsdPDEOut.m() == 1);
  rsdPDEOut[0] = rsdPDEIn;
}

// interpret residuals of the gradients in the solution variable
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class T>
void
PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::interpResidGradient(
    const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const
{
  SANS_ASSERT(rsdAuxOut.m() == 1);
  rsdAuxOut[0] = rsdAuxIn;
}

// interpret residuals at the boundary (should forward to BCs)
template<class AdvectiveFlux, class ViscousFlux, class Source>
template <class T>
void
PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::interpResidBC(
    const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const
{
  SANS_ASSERT(rsdBCOut.m() == 1);
  rsdBCOut[0] = rsdBCIn;
}

// how many residual equations are we dealing with
template<class AdvectiveFlux, class ViscousFlux, class Source>
inline int
PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::nMonitor() const
{
  return 1;
}

template<class AdvectiveFlux, class ViscousFlux, class Source>
void
PDEAdvectionDiffusion<PhysD2, AdvectiveFlux, ViscousFlux, Source>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PDEAdvectionDiffusion2D: adv_ =" << std::endl;
  adv_.dump(indentSize+2, out);
  out << indent << "PDEAdvectionDiffusion2D: visc_ =" << std::endl;
  visc_.dump(indentSize+2, out);
  out << indent << "PDEAdvectionDiffusion2D: force_ =" << std::endl;
  if ( force_ != NULL )
    (*force_).dump(indentSize+2, out);
  else
    out << " NULL" << std::endl;
}

} //namespace SANS

#endif  // PDEADVECTIONDIFFUSION2D_H
