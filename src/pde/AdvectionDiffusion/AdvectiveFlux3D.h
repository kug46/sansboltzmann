// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ADVECTIVEFLUX3D_H
#define ADVECTIVEFLUX3D_H

// 3-D Advection-Diffusion PDE: advective flux

#include "tools/SANSnumerics.h"     // Real
#include "AdvectionDiffusion_Traits.h"

#include <cmath>
#include <iostream>
#include <random>

namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: advection velocity
//
// member functions:
//   .flux       advective flux: F(Q)
//   .jacobian   advective flux jacboian: d(F)/d(U)
//----------------------------------------------------------------------------//

class AdvectiveFlux3D_None
{
public:
  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD3>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD3>::template MatrixQ<T>;  // matrices

  bool hasFluxAdvective() const { return false; }

  ~AdvectiveFlux3D_None() {}

  // advective flux: F(Q)
  template<class Tq, class Tf>
  void flux( const Real x, const Real y, const Real z, const Real time,
             const ArrayQ<Tq>& q, ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& fz ) const
  {}

  // advective flux jacobian: d(F)/d(U)
  template<class Tq, class Tf>
  void jacobian( const Real x, const Real y, const Real z, const Real time,
                 const ArrayQ<Tq>& q, MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& az ) const
  {}

  template<class Tq, class Tf>
  void fluxUpwind( const Real x, const Real y, const Real z, const Real time,
                   const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, const Real& nx, const Real& ny, const Real& nz,
                   ArrayQ<Tf>& f, const Real& scale = 1  ) const
  {}

  template<class Tq, class Tf>
  void fluxUpwindSpaceTime( const Real x, const Real y, const Real z, const Real time,
                            const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
                            const Real& nx, const Real& ny, const Real& nz, const Real& nt,
                            ArrayQ<Tf>& f, const Real& scale= 1 ) const
  {
    f += 0.5*(nt)*(qR + qL) - 0.5*scale*fabs(nt)*(qR - qL);
  }

  // strong advective flux: div.F
  template<class Tq, class Tg, class Tf>
  void strongFlux( const Real x, const Real y, const Real z, const Real time,
                   const ArrayQ<Tq>& q,
                   const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
                   ArrayQ<Tf>& strongPDE ) const
  {}

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;
};


//----------------------------------------------------------------------------//
// advection velocity: uniform
//
// member functions:
//   .flux         advective flux: F(Q)
//   .jacobian     advective flux jacboian: d(F)/d(U)
//   .strongFlux   advective flux : div.F
//----------------------------------------------------------------------------//

class AdvectiveFlux3D_Uniform
{
public:
  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD3>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD3>::template MatrixQ<T>;  // matrices

  bool hasFluxAdvective() const { return true; }

  AdvectiveFlux3D_Uniform( const Real u, const Real v, const Real w ) : u_(u), v_(v), w_(w) {}
  ~AdvectiveFlux3D_Uniform() {}

  // advective flux: F(Q)
  template<class Tq, class Tf>
  void flux( const Real x, const Real y, const Real z, const Real time,
             const ArrayQ<Tq>& q, ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& fz ) const
  {
    fx += u_*q;
    fy += v_*q;
    fz += w_*q;
  }

  // advective flux jacobian: d(F)/d(U)
  template<class Tq, class Tf>
  void jacobian( const Real x, const Real y, const Real z, const Real time,
                 const ArrayQ<Tq>& q, MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& az ) const
  {
    ax += u_;
    ay += v_;
    az += w_;
  }

  template<class Tq, class Tf>
  void fluxUpwind( const Real x, const Real y, const Real z, const Real time,
                   const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, const Real& nx, const Real& ny, const Real& nz,
                   ArrayQ<Tf>& f, const Real& scale = 1  ) const
  {
    f += 0.5*(nx*u_ + ny*v_ + nz*w_)*(qR + qL) - 0.5*scale*fabs(nx*u_ + ny*v_ + nz*w_)*(qR - qL);
  }

  template<class Tq, class Tf>
  void fluxUpwindSpaceTime( const Real x, const Real y, const Real z, const Real time,
                            const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
                            const Real& nx, const Real& ny, const Real& nz, const Real& nt,
                            ArrayQ<Tf>& f, const Real& scale= 1 ) const
  {
    f += 0.5*(nt*1 + nx*u_ + ny*v_ + nz*w_)*(qR + qL) - 0.5*scale*fabs(nt*1 + nx*u_ + ny*v_ + nz*w_)*(qR - qL);
  }

  // strong advective flux: div.F
  template<class Tq, class Tg, class Tf>
  void strongFlux( const Real x, const Real y, const Real z, const Real time,
                   const ArrayQ<Tq>& q,
                   const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
                   ArrayQ<Tf>& strongPDE ) const
  {
    strongPDE += u_*qx + v_*qy + w_*qz;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  const Real u_, v_, w_;      // uniform advection velocity
};

//----------------------------------------------------------------------------//
// advection velocity: uniform
//
// member functions:
//   .flux         advective flux: F(Q)
//   .jacobian     advective flux jacboian: d(F)/d(U)
//   .strongFlux   advective flux : div.F
//----------------------------------------------------------------------------//

class AdvectiveFlux3D_Radial
{
public:
  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD3>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD3>::template MatrixQ<T>;  // matrices

  bool hasFluxAdvective() const { return true; }

  explicit AdvectiveFlux3D_Radial( const Real V ) : V_(V) {}
  ~AdvectiveFlux3D_Radial() {}

  // advective flux: F(Q)
  template<class Tq, class Tf>
  void flux( const Real x, const Real y, const Real z, const Real time,
             const ArrayQ<Tq>& q, ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& fz ) const
  {
    Real u,v,w;
    velocity(x,y,z,u,v,w);
    fx += u*q;
    fy += v*q;
    fz += w*q;
  }

  // advective flux jacobian: d(F)/d(U)
  template<class Tq, class Tf>
  void jacobian( const Real x, const Real y, const Real z, const Real time,
                 const ArrayQ<Tq>& q, MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& az ) const
  {
    Real u,v,w;
    velocity(x,y,z,u,v,w);
    ax += u;
    ay += v;
    az += w;
  }

  template<class Tq, class Tf>
  void fluxUpwind( const Real x, const Real y, const Real z, const Real time,
                   const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, const Real& nx, const Real& ny, const Real& nz,
                   ArrayQ<Tf>& f, const Real& scale = 1  ) const
  {
    Real u,v,w;
    velocity(x,y,z,u,v,w);
    f += 0.5*(nx*u + ny*v + nz*w)*(qR + qL) - 0.5*scale*fabs(nx*u + ny*v + nz*w)*(qR - qL);
  }

  template<class Tq, class Tf>
  void fluxUpwindSpaceTime( const Real x, const Real y, const Real z, const Real time,
                            const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
                            const Real& nx, const Real& ny, const Real& nz, const Real& nt,
                            ArrayQ<Tf>& f, const Real& scale= 1 ) const
  {
    Real u,v,w;
    velocity(x,y,z,u,v,w);
    f += 0.5*(nt*1 + nx*u + ny*v + nz*w)*(qR + qL) - 0.5*scale*fabs(nt*1 + nx*u + ny*v + nz*w)*(qR - qL);
  }

  // strong advective flux: div.F
  template<class Tq, class Tg, class Tf>
  void strongFlux( const Real x, const Real y, const Real z, const Real time,
                   const ArrayQ<Tq>& q,
                   const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
                   ArrayQ<Tf>& strongPDE ) const
  {
    Real u,v,w;
    velocity(x,y,z,u,v,w);
    strongPDE += u*qx + v*qy + w*qz;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  void velocity(const Real x , const Real y , const Real z , Real& u , Real& v , Real& w ) const;

  const Real V_; // radial advective velocity
  //const Real u_, v_, w_;      // uniform advection velocity
};

//----------------------------------------------------------------------------//
// advection velocity: square pipe
//
// member functions:
//   .flux         advective flux: F(Q)
//   .jacobian     advective flux jacboian: d(F)/d(U)
//   .strongFlux   advective flux : div.F
//----------------------------------------------------------------------------//

class AdvectiveFlux3D_SquareChannel
{
public:
  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD3>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD3>::template MatrixQ<T>;  // matrices

  bool hasFluxAdvective() const { return true; }

  explicit AdvectiveFlux3D_SquareChannel( const Real b,
                                          const Real c,
                                          const Real nu,
                                          const Real lambda_z,
                                          const Real lambda_t) :
      b_(b), c_(c), nu_(nu), lambda_z_(lambda_z), lambda_t_(lambda_t)
  {
  }

  ~AdvectiveFlux3D_SquareChannel() {}

  // advective flux: F(Q)
  template<class Tq, class Tf>
  void flux( const Real x, const Real y, const Real z, const Real time,
             const ArrayQ<Tq>& q, ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& fz ) const
  {
    Real u,v,w;
    velocity(x,y,z,u,v,w);
    fx += u*q;
    fy += v*q;
    fz += w*q;
  }

  // advective flux jacobian: d(F)/d(U)
  template<class Tq, class Tf>
  void jacobian( const Real x, const Real y, const Real z, const Real time,
                 const ArrayQ<Tq>& q, MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& az ) const
  {
    Real u,v,w;
    velocity(x,y,z,u,v,w);
    ax += u;
    ay += v;
    az += w;
  }

  template<class Tq, class Tf>
  void fluxUpwind( const Real x, const Real y, const Real z, const Real time,
                   const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, const Real& nx, const Real& ny, const Real& nz,
                   ArrayQ<Tf>& f, const Real& scale = 1  ) const
  {
    Real u,v,w;
    velocity(x,y,z,u,v,w);
    f += 0.5*(nx*u + ny*v + nz*w)*(qR + qL) - 0.5*scale*fabs(nx*u + ny*v + nz*w)*(qR - qL);
  }

  template<class Tq, class Tf>
  void fluxUpwindSpaceTime( const Real x, const Real y, const Real z, const Real time,
                            const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
                            const Real& nx, const Real& ny, const Real& nz, const Real& nt,
                            ArrayQ<Tf>& f, const Real& scale= 1 ) const
  {
    Real u,v,w;
    velocity(x,y,z,u,v,w);
    f += 0.5*(nt*1 + nx*u + ny*v + nz*w)*(qR + qL) - 0.5*scale*fabs(nt*1 + nx*u + ny*v + nz*w)*(qR - qL);
  }

  // strong advective flux: div.F
  template<class Tq, class Tg, class Tf>
  void strongFlux( const Real x, const Real y, const Real z, const Real time,
                   const ArrayQ<Tq>& q,
                   const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
                   ArrayQ<Tf>& strongPDE ) const
  {
    Real u,v,w;
    velocity(x,y,z,u,v,w);
    strongPDE += u*qx + v*qy + w*qz;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  void velocity(const Real x , const Real y , const Real z , Real& u , Real& v , Real& w ) const;

  const Real b_;  // scaling constant
  const Real c_;  // shape constant on radial gaussian pulse
  const Real nu_; // diffusive flux
  const Real lambda_z_;   // decay/growth constant on axial direction
  const Real lambda_t_;   // decay/growth constant on temporal direction

};

//----------------------------------------------------------------------------//
// advection velocity: Spatial dependence (9th degree)
// _------_----__----- FIXING ONLY 'w'. u,v set to zero. ---_---_----__---_-- TESTING
// member functions:
//   .flux         advective flux: F(Q)
//   .jacobian     advective flux jacboian: d(F)/d(U)
//   .strongFlux   advective flux : div.F
//----------------------------------------------------------------------------//

class AdvectiveFlux3D_PolyZ
{
public:
  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD3>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD3>::template MatrixQ<T>;  // matrices

  bool hasFluxAdvective() const { return true; }

  AdvectiveFlux3D_PolyZ( const Real a0, const Real a1, const Real a2, const Real a3, const Real a4, const Real a5, const Real a6, const Real a7,
                           const Real a8, const Real a9) : a0_(a0), a1_(a1),a2_(a2),a3_(a3),a4_(a4),a5_(a5),a6_(a6),a7_(a7),a8_(a8),a9_(a9) {}
  ~AdvectiveFlux3D_PolyZ() {}


  // advective flux: F(Q)
  template<class Tq, class Tf>
  void flux( const Real x, const Real y, const Real z, const Real time,
             const ArrayQ<Tq>& q, ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& fz ) const
  {
    Real w = w_poly(z);

    fx += 0; // u*q
    fy += 0; // v*q
    fz += w*q;
  }

  // advective flux jacobian: d(F)/d(U)
  template<class Tq, class Tf>
  void jacobian( const Real x, const Real y, const Real z, const Real time,
                 const ArrayQ<Tq>& q, MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& az ) const
  {
    Real w = w_poly(z);

    ax += 0; // u
    ay += 0; // v
    az += w;
  }

  template<class Tq, class Tf>
  void fluxUpwind( const Real x, const Real y, const Real z, const Real time,
                   const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, const Real& nx, const Real& ny, const Real& nz,
                   ArrayQ<Tf>& f, const Real& scale = 1  ) const
  {
    //f += 0.5*(nx*u + ny*v + nz*w)*(qR + qL) - 0.5*fabs(nx*u + ny*v + nz*w)*(qR - qL)
    Real w = w_poly(z);

    f += 0.5*(nz*w)*(qR + qL) - 0.5*fabs(nz*w)*scale*(qR - qL);
  }

  template<class Tq, class Tf>
  void fluxUpwindSpaceTime( const Real x, const Real y, const Real z, const Real time,
                            const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
                            const Real& nx, const Real& ny, const Real& nz, const Real& nt,
                            ArrayQ<Tf>& f, const Real& scale= 1 ) const
  {
    //f += 0.5*(nt*1 + nx*u + ny*v + nz*w)*(qR + qL) - 0.5*fabs(nt*1 + nx*u + ny*v + nz*w)*(qR - qL)
    Real w = w_poly(z);

    f += 0.5*(nt*1 + nz*w)*(qR + qL) - 0.5*scale*fabs(nt*1 + nz*w)*(qR - qL);
  }

  // strong advective flux: div.F
  template<class Tq, class Tg, class Tf>
  void strongFlux( const Real x, const Real y, const Real z, const Real time,
                   const ArrayQ<Tq>& q,
                   const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
                   ArrayQ<Tf>& strongPDE ) const
  {
    //strongPDE += div . (u*q, v*q, w*q)
    Real w = w_poly(z);
    Real dwdz = w_poly_z(z);

    strongPDE += w*qz + dwdz*q;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  // velocity in z direction
  Real w_poly(const Real& z) const
  {
    return (a0_+a1_*z+  a2_*pow(z,2)+  a3_*pow(z,3)+  a4_*pow(z,4)+  a5_*pow(z,5)+  a6_*pow(z,6)+  a7_*pow(z,7)+  a8_*pow(z,8)+  a9_*pow(z,9));
  }
  // derivative of velocity wrt z
  Real w_poly_z(const Real& z) const
  {
    return (    a1_  +2*a2_*z       +3*a3_*pow(z,2)+4*a4_*pow(z,3)+5*a5_*pow(z,4)+6*a6_*pow(z,5)+7*a7_*pow(z,6)+8*a8_*pow(z,7)+9*a9_*pow(z,8));
  }

  // z-velocity polynomial coefficients
  const Real a0_,a1_,a2_,a3_,a4_,a5_,a6_,a7_,a8_,a9_;
};

class AdvectiveFlux3D_Random
{
public:
  template <class T>
  using ArrayQ= AdvectionDiffusionTraits<PhysD3>::template ArrayQ<T>; // solution/residual arrays

  template <class T>
  using MatrixQ= AdvectionDiffusionTraits<PhysD3>::template MatrixQ<T>; // matrices

  bool hasFluxAdvective() const { return true; }

  AdvectiveFlux3D_Random( const Real u0, const Real v0, const Real w0,
      const Real N_fourier ) : u0_(u0), v0_(v0), w0_(w0), N_fourier_(N_fourier)
  {
    // initialize a uniform distribution between -1 and 1
    std::mt19937_64 rng;
    std::uniform_real_distribution<double> unif(-1, 1);

    for (int i= 0; i < N_fourier_; i++)
    {
      // draw three directions at random
      kx_fourier_[i]= unif(rng);
      ky_fourier_[i]= unif(rng);
      kz_fourier_[i]= unif(rng);
      u_fourier_[i]= unif(rng);
      v_fourier_[i]= unif(rng);
      w_fourier_[i]= unif(rng);

      // calculate magnitudes
      Real kMag= sqrt(kx_fourier_[i]*kx_fourier_[i]
          + ky_fourier_[i]*ky_fourier_[i] + kz_fourier_[i]*kz_fourier_[i]);
      Real vMag= sqrt(u_fourier_[i]*u_fourier_[i]
          + v_fourier_[i]*v_fourier_[i] + w_fourier_[i]*w_fourier_[i]);

      // normalize so ||k|| = i for all values (except for zero, sad)
      if (i == 0)
      {
        kx_fourier_[i]= kx_fourier_[i]/kMag;
        ky_fourier_[i]= kx_fourier_[i]/kMag;
        kz_fourier_[i]= kx_fourier_[i]/kMag;
      }
      else
      {
        kx_fourier_[i]= kx_fourier_[i]*i/kMag;
        ky_fourier_[i]= ky_fourier_[i]*i/kMag;
        kz_fourier_[i]= kz_fourier_[i]*i/kMag;
      }

      // now handle coefficients- first normalize to one magnitude
      u_fourier_[i]= u_fourier_[i]/vMag;
      v_fourier_[i]= v_fourier_[i]/vMag;
      w_fourier_[i]= w_fourier_[i]/vMag;

      // next, subtract off curlfree part of V, leaving only divergence free part
      Real common_term= (kx_fourier_[i]*u_fourier_[i]
          + ky_fourier_[i]*v_fourier_[i] + kz_fourier_[i]*w_fourier_[i])
          /(kMag*kMag);
      u_fourier_[i]= u_fourier_[i] - common_term*kx_fourier_[i];
      v_fourier_[i]= v_fourier_[i] - common_term*ky_fourier_[i];
      w_fourier_[i]= w_fourier_[i] - common_term*kz_fourier_[i];

      // now it's divergence free! last but not least, scale to -5/3 power law
      u_fourier_[i] *= pow(kMag, -5./6.);
      v_fourier_[i] *= pow(kMag, -5./6.);
      w_fourier_[i] *= pow(kMag, -5./6.);
    }

    // set the constant mode
    u_fourier_[0]= u0_;
    v_fourier_[0]= v0_;
    w_fourier_[0]= w0_;
  }
  ~AdvectiveFlux3D_Random() {}

  // advective flux: F(Q)
  template<class Tq, class Tf>
  void flux( const Real x, const Real y, const Real z, const Real time,
             const ArrayQ<Tq> &q, ArrayQ<Tf> &fx, ArrayQ<Tf> &fy, ArrayQ<Tf> &fz ) const
  {
    Real u, v, w;
    getV(x, y, z, u, v, w);

    fx += u*q;
    fx += v*q;
    fx += w*q;
  }

  // advective flux jacobian
  template<class Tq, class Tf>
  void jacobian( const Real x, const Real y, const Real z, const Real time,
                 const ArrayQ<Tq> &q, MatrixQ<Tf> &ax, MatrixQ<Tf> &ay, MatrixQ<Tf> &az ) const
  {
    Real u, v, w;
    getV(x, y, z, u, v, w);

    ax += u;
    ay += v;
    az += w;
  }

  template<class Tq, class Tf>
  void fluxUpwind( const Real x, const Real y, const Real z, const Real time,
                   const ArrayQ<Tq> &qL, const ArrayQ<Tq> &qR,
                   const Real &nx, const Real &ny, const Real &nz,
                   ArrayQ<Tf> &f, const Real &scale= 1 ) const
  {
    Real u, v, w;
    getV(x, y, z, u, v, w);

    f += 0.5*(nx*u + ny*v + nz*w)*(qR + qL) - 0.5*scale*fabs(nx*u + ny*v
        + nz*w)*(qR - qL);
  }

  template<class Tq, class Tf>
  void fluxUpwindSpaceTime( const Real x, const Real y, const Real z, const Real time,
                            const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
                            const Real& nx, const Real& ny, const Real& nz, const Real& nt,
                            ArrayQ<Tf>& f, const Real& scale= 1 ) const
  {
    Real u, v, w;
    getV(x, y, z, u, v, w);

    f += 0.5*(nt*1 + nx*u + ny*v + nz*w)*(qR + qL) - 0.5*scale*fabs(nt*1
        + nx*u + ny*v + nz*w)*(qR - qL);
  }

  // strong advective flux: div.F
  template<class Tq, class Tg, class Tf>
  void strongFlux( const Real x, const Real y, const Real z, const Real time,
                   const ArrayQ<Tq> &q,
                   const ArrayQ<Tg> &qx, const ArrayQ<Tg> &qy, const ArrayQ<Tg> &qz,
                   ArrayQ<Tf> &strongPDE ) const
  {
    Real u, v, w;
    getV(x, y, z, u, v, w);

    strongPDE += u*qx + v*qy + w*qz;
  }

  void dump( int indentSize= 0, std::ostream &out= std::cout ) const;

private:
  const Real u0_;           // baseline mean advection, x
  const Real v0_;           // baseline mean advection, y
  const Real w0_;           // baseline mean advection, z
  const int N_fourier_;     // number of fourier modes
  std::vector<Real> kx_fourier_;  // fourier wavevectors
  std::vector<Real> ky_fourier_;  // fourier wavevectors
  std::vector<Real> kz_fourier_;  // fourier wavevectors
  std::vector<Real> u_fourier_;   // fourier coefficients
  std::vector<Real> v_fourier_;   // fourier coefficients
  std::vector<Real> w_fourier_;   // fourier coefficients

  void getV(const Real x, const Real y, const Real z, Real &u, Real &v, Real &w) const
  {
    u= 0;
    v= 0;
    w= 0;

    for (int i= 0; i < N_fourier_; i++)
    {
      u += u_fourier_[i]*cos(2*PI*kx_fourier_[i]*x)*cos(2*PI*ky_fourier_[i]*y)
          *cos(2*PI*kz_fourier_[i]*z);
      v += v_fourier_[i]*cos(2*PI*kx_fourier_[i]*x)*cos(2*PI*ky_fourier_[i]*y)
          *cos(2*PI*kz_fourier_[i]*z);
      w += w_fourier_[i]*cos(2*PI*kx_fourier_[i]*x)*cos(2*PI*ky_fourier_[i]*y)
          *cos(2*PI*kz_fourier_[i]*z);
    }
  }

};

//----------------------------------------------------------------------------//
// Local Lax-Friedrichs upwind scheme
//
// member functions:
//   .fluxUpwind            advective flux: n.F(QL,QR)
//   .fluxUpwindSpaceTime   advective flux: n.F(QL,QR)
//----------------------------------------------------------------------------//
#if 0 // This is commented out until unit tests are written for it. It looks like it's correct, but needs tests.
template<class AdvectiveFlux3D>
class LocalLaxFriedrichs3D : public AdvectiveFlux3D
{
public:
  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD3>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD3>::template MatrixQ<T>;  // matrices

  template<class T>
  void fluxUpwind( const Real x, const Real y, const Real z, const Real time,
                   const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx, const Real& ny, const Real& nz,
                   ArrayQ<T>& f ) const
  {
    ArrayQ<T> fL = 0, gL = 0, hL = 0;
    ArrayQ<T> fR = 0, gR = 0, hR = 0;

    flux(x, y, z, time, qL, fL, gL, hL);
    flux(x, y, z, time, qR, fR, gR, hR);

    ArrayQ<T> axL = 0, ayL = 0, azL = 0;
    ArrayQ<T> axR = 0, ayR = 0, azR = 0;

    jacobian(x, y, z, time, qL, axL, ayL, azL);
    jacobian(x, y, z, time, qR, axR, ayR, azR);

    //TODO: Rather arbitrary smoothing constants here
    Real eps = 1e-8;
    Real alpha = 40;

    T anL = nx*axL[0] + ny*ayL[0] + nz*azL[0];
    T anR = nx*axR[0] + ny*ayR[0] + nz*azR[0];

    T a = smoothmax( smoothabs0(anL, eps), smoothabs0(anR, eps), alpha);

    // Local Lax-Friedrichs Flux
    f += 0.5*( (nx*fR[0] + ny*gR[0] + nz*hR[0]) + (nx*fL[0] + ny*gL[0] + nz*hL[0]) ) - 0.5*a*(qR[0] - qL[0]);
  }

  template<class T>
  void fluxUpwindSpaceTime( const Real x, const Real y, const Real z, const Real time,
                   const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx, const Real& ny, const Real& nz, const Real& nt,
                   ArrayQ<T>& f ) const
  {
    ArrayQ<T> fL = 0, gL = 0, hL = 0;
    ArrayQ<T> fR = 0, gR = 0, hR = 0;

    flux(x, y, z, time, qL, fL, gL, hL);
    flux(x, y, z, time, qR, fR, gR, hR);

    ArrayQ<T> axL = 0, ayL = 0, azL = 0;
    ArrayQ<T> axR = 0, ayR = 0, azR = 0;

    jacobian(x, y, z, time, qL, axL, ayL, azL);
    jacobian(x, y, z, time, qR, axR, ayR, azR);

    //TODO: Rather arbitrary smoothing constants here
    Real eps = 1e-8;
    Real alpha = 40;

    T anL = nt + nx*axL[0] + ny*ayL[0] + nz*azL[0];
    T anR = nt + nx*axR[0] + ny*ayR[0] + nz*azR[0];

    T a = smoothmax( smoothabs0(anL, eps), smoothabs0(anR, eps), alpha);

    // Local Lax-Friedrichs Flux
    f += 0.5*( (nt*qR + nx*fR + ny*gR + nz*hR) + (nt*qL + nx*fL + ny*gL + nz*hL) ) - 0.5*a*(qR - qL);
  }

};
#endif
} //namespace SANS

#endif  // ADVECTIVEFLUX3D_H
