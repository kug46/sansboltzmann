// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// 2D advection flux

#include <string>
#include <iostream>
#include <cmath> // log, fabs

#include "AdvectiveFlux2D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// advection flux: nothing

void
AdvectiveFlux2D_None::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "AdvectiveFlux2D: NONE" << std::endl;
}

//----------------------------------------------------------------------------//
// advection flux: uniform flow

void
AdvectiveFlux2D_Uniform::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "AdvectiveFlux2D_Uniform:"
            << "  u_ = " << u_
            << "  v_ = " << v_ << std::endl;
}


//----------------------------------------------------------------------------//
// advection velocity: cubic source bump
//
// exact solution of Cauchy-Riemann for cubic source defined for x in [0,1]
// bump symmetric about y=0 with half-height equal tau
// freestream velocity: u = 1, v = 0

AdvectiveFlux2D_CubicSourceBump::AdvectiveFlux2D_CubicSourceBump( const Real tau )
{
  Real pi = 4*atan(1.);

  tau_ = tau;
  src_ = tau*15000 / (1875*pi - 160 - 3750*atan(0.2) - 3243*atan(5.));
}

void
AdvectiveFlux2D_CubicSourceBump::velocity( const Real x, const Real y, Real& u, Real& v ) const
{
  if (y == 0)
  {
    if ((x == 0) || (x == 1))
    {
      u = 1 - src_/12.;
    }
    else
    {
      u = 1 - (src_/12.)*(1 + 12*x*(x - 1)
          - 12*x*(x - 1)*(x - 0.5)*log(fabs(x/(x - 1))));
    }

    if ((x > 0) && (x < 1))
    {
      Real pi = 4*atan(1.);
      v = pi*src_*x*(x - 1)*(x - 0.5);
    }
    else
    {
      v = 0;
    }
  }
  else
  {
    u = 1 - (src_/12.)*(1 + 12*x*(x - 1) - 12*y*y
        + 6*y*(1 + 6*x*(x - 1) - 2*y*y)*(atan((x-1)/y) - atan(x/y))
        - 6*(x - 0.5)*(x*(x - 1) - 3*y*y)*log((x*x + y*y)/((x - 1)*(x - 1) + y*y)));

    v = (src_/4.)*(8*(x - 0.5)*y - 4*(x - 0.5)*(x*(x - 1) - 3*y*y)*(atan((x-1)/y) - atan(x/y))
        - y*(1 + 6*x*(x - 1) - 2*y*y)*log((x*x + y*y)/((x - 1)*(x - 1) + y*y)));
  }
}


void
AdvectiveFlux2D_CubicSourceBump::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "AdvectiveFlux2D_CubicSourceBump:"
            << "  tau_ = " << tau_
            << "  src_ = " << src_ << std::endl;
}

void
AdvectiveFlux2D_ConstRotation::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "AdvectiveFlux2D_ConstRotation:"
            << "  V_ = " << V_ << std::endl;
}

} //namespace SANS
