// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef OUTPUTADVECTIONDIFFUSION2D_H
#define OUTPUTADVECTIONDIFFUSION2D_H

// Python must be included first
#include "Python/PyDict.h"

#include "tools/SANSnumerics.h"

#include "pde/OutputCategory.h"
#include "Topology/Dimension.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//                                    2D
//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//
//  Function Weighted Residual
//
template< class WeightFunction >
class OutputAdvectionDiffusion2D_FunctionWeightedResidual
    : public OutputType< OutputAdvectionDiffusion2D_FunctionWeightedResidual<WeightFunction> >
{
public:
  typedef PhysD2 PhysDim;
  typedef OutputCategory::WeightedResidual Category;

  template<class T>
  using ArrayQ = T;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the transpose Jacobian of this output functional


  explicit OutputAdvectionDiffusion2D_FunctionWeightedResidual( const WeightFunction& w ) : wfcn_(w) {}
  explicit OutputAdvectionDiffusion2D_FunctionWeightedResidual( const PyDict& d ) : wfcn_(d) {}

  void operator()(const Real& x, const Real& y, const Real& time, ArrayQ<Real>& weight ) const
  {
    weight = wfcn_(x,y, time);
//    std::cout<< "weight[0]" << weight << std::endl;
  }

private:
  WeightFunction wfcn_;
};

//----------------------------------------------------------------------------//
//  Weighted Residual
//
class OutputAdvectionDiffusion2D_WeightedResidual : public OutputType< OutputAdvectionDiffusion2D_WeightedResidual >
{
public:
  typedef PhysD2 PhysDim;
  typedef OutputCategory::WeightedResidual Category;

  template<class T>
  using ArrayQ = T;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the transpose Jacobian of this output functional

  explicit OutputAdvectionDiffusion2D_WeightedResidual( const Real& w ) : w_(w) {}

  void operator()(const Real& x, const Real& y, const Real& time, ArrayQ<Real>& weight ) const
  {
    weight = w_;
  }

private:
  Real w_;
};

//----------------------------------------------------------------------------//
//  Function Weighted Boundary Flux
//
template <class PDE_, class WeightFunction>
class OutputAdvectionDiffusion2D_FunctionWeightedFlux
    : public OutputType< OutputAdvectionDiffusion2D_FunctionWeightedFlux<PDE_,WeightFunction> >
{
public:
  typedef PhysD2 PhysDim;
  typedef PDE_ PDE;
  typedef OutputCategory::Functional Category;

  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputAdvectionDiffusion2D_FunctionWeightedFlux( const PDE& pde, const WeightFunction& w,
                                                            const bool adv = true, const bool visc = true ) :
    pde_(pde), wfcn_(w), adv_(adv), visc_(visc) {}

  explicit OutputAdvectionDiffusion2D_FunctionWeightedFlux( const PDE& pde, const PyDict& d,
                                                            const bool adv = true, const bool visc = true ) :
    pde_(pde), wfcn_(d), adv_(adv), visc_(visc) {}

  bool needsSolutionGradient() const { return true; }

  template<class T>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayJ<T>& output ) const
  {
    SANS_DEVELOPER_EXCEPTION("THIS IS A BOUNDARY OUTPUT");
  }

  template<class T>
  void operator()(const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayJ<T>& output ) const
  {
    Real w = wfcn_(x,y,time);

    ArrayQ<T> Fn = 0;
    ArrayQ<T> Fx = 0, Fy = 0;

    if ( pde_.hasFluxAdvective() && adv_)
      pde_.fluxAdvective( x, y, time, q, Fx, Fy );

    // viscous flux
    if ( pde_.hasFluxViscous() && visc_)
      pde_.fluxViscous( x, y, time, q, qx, qy, Fx, Fy );

    Fn = Fx*nx + Fy*ny;

    output = w*Fn;

  }
private:
  const PDE& pde_;
  const WeightFunction wfcn_;
  const bool adv_;
  const bool visc_;
};

//----------------------------------------------------------------------------//
//  Weighted Boundary Flux
//
template <class PDE_>
class OutputAdvectionDiffusion2D_WeightedFlux
    : public OutputType< OutputAdvectionDiffusion2D_WeightedFlux<PDE_> >
{
public:
  typedef PhysD2 PhysDim;
  typedef PDE_ PDE;
  typedef OutputCategory::Functional Category;

  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputAdvectionDiffusion2D_WeightedFlux( const PDE& pde, const Real& w, const bool adv = true, const bool visc = true ) :
    pde_(pde), w_(w), adv_(adv), visc_(visc) {}


  bool needsSolutionGradient() const { return true; }

  template<class T>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayJ<T>& output ) const
  {
    SANS_DEVELOPER_EXCEPTION("THIS IS A BOUNDARY OUTPUT");
  }

  template<class T>
  void operator()(const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayJ<T>& output ) const
  {
    ArrayQ<T> Fn = 0;
    ArrayQ<T> Fx = 0, Fy = 0;

    if ( pde_.hasFluxAdvective() && adv_ )
      pde_.fluxAdvective( x, y, time, q, Fx, Fy );

    // viscous flux
    if ( pde_.hasFluxViscous() && visc_ )
      pde_.fluxViscous( x, y, time, q, qx, qy, Fx, Fy );

    Fn = Fx*nx + Fy*ny;

    output = w_*Fn;

  }
private:
  const PDE& pde_;
  const Real& w_;
  const bool adv_;
  const bool visc_;
};


//----------------------------------------------------------------------------//
//  Function Weighted Boundary State
//
template <class PDE_, class WeightFunction>
class OutputAdvectionDiffusion2D_FunctionWeightedState
    : public OutputType< OutputAdvectionDiffusion2D_FunctionWeightedState<PDE_,WeightFunction> >
{
public:
  typedef PhysD2 PhysDim;
  typedef PDE_ PDE;
  typedef OutputCategory::Functional Category;

  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputAdvectionDiffusion2D_FunctionWeightedState( const PDE& pde, const WeightFunction& w ) :
    pde_(pde), wfcn_(w) {}

  explicit OutputAdvectionDiffusion2D_FunctionWeightedState( const PDE& pde, const PyDict& d ) :
    pde_(pde), wfcn_(d) {}

  bool needsSolutionGradient() const { return true; }

  template<class T>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayJ<T>& output ) const
  {
    Real w = wfcn_(x,y,time);
    output = w*q;
  }

  template<class T>
  void operator()(const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayJ<T>& output ) const
  {
    operator()(x,y,time,q,qx,qy,output);
  }
private:
  const PDE& pde_;
  const WeightFunction& wfcn_;
};

//----------------------------------------------------------------------------//
//  Weighted Boundary State
//
template <class PDE_>
class OutputAdvectionDiffusion2D_WeightedState
    : public OutputType< OutputAdvectionDiffusion2D_WeightedState<PDE_> >
{
public:
  typedef PhysD2 PhysDim;
  typedef PDE_ PDE;
  typedef OutputCategory::Functional Category;

  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputAdvectionDiffusion2D_WeightedState( const PDE& pde, const Real& w ) :
    pde_(pde), w_(w) {}


  bool needsSolutionGradient() const { return true; }

  template<class T>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayJ<T>& output ) const
  {
    output = w_*q;
  }

  template<class T>
  void operator()(const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayJ<T>& output ) const
  {
    operator()(x,y,time,q,qx,qy,output);
  }
private:
  const PDE& pde_;
  const Real& w_;
};


}

#endif //OUTPUTADVECTIONDIFFUSION_H
