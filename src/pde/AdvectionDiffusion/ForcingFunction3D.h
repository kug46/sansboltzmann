// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FORCINGFUNCTION3D_H
#define FORCINGFUNCTION3D_H

// 3-D Advection-Diffusion PDE: RHS forcing function

#include "tools/SANSnumerics.h"     // Real
#include "AdvectionDiffusion_Traits.h"
#include "pde/ForcingFunctionBase.h"

#include <cmath>
#include <iostream>

namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: RHS forcing function
//
// member functions:
//   .()      functor returning forcing function source term
//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//
// forcing function: constant

template<class PDE>
class ForcingFunction3D_Const : public ForcingFunctionBase3D<PDE>
{
public:
  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  static const int D = 3;                     // physical dimensions

  bool hasForcingFunction() const override { return true; }

  explicit ForcingFunction3D_Const( Real c ) : c_(c) {}
  ~ForcingFunction3D_Const() {}

  void operator()
      ( const PDE& pde, const Real &x, const Real &y, const Real &z, const Real &time, ArrayQ<Real>& forcing ) const override
      { forcing += c_; }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const override
      {
        std::string indent(indentSize, ' ');
        out << indent << "ForcingFunction3D_Const:  c_ = " << c_ << std::endl;
      }

private:
  Real c_;
};


//----------------------------------------------------------------------------//
// forcing function: monomial
// NOTE: code for Laplacian only

template <class PDE>
class ForcingFunction3D_Monomial : public ForcingFunctionBase3D<PDE>
{
public:
  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  static const int D = 3;                     // physical dimensions

  bool hasForcingFunction() const override { return true; }

  explicit ForcingFunction3D_Monomial( int i, int j, int k, Real nu ) : i_(i), j_(j), k_(k), nu_(nu) {}
  ~ForcingFunction3D_Monomial() {}

  void operator()
      ( const PDE& pde, const Real &x, const Real &y, const Real &z, const Real &time,
          ArrayQ<Real>& forcing ) const override
  {
    forcing += -nu_*( pow(x, i_-2) * pow(y, j_)   * pow(z, k_)   * i_*(i_-1)
                    + pow(x, i_)   * pow(y, j_-2) * pow(z, k_)   * j_*(j_-1)
                    + pow(x, i_)   * pow(y, j_)   * pow(z, k_-2) * k_*(k_-1) );
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const override
      {
        std::string indent(indentSize, ' ');
        out << indent << "ForcingFunction3D_Monomial:" << std::endl;
      }


private:
  int i_, j_, k_;
  Real nu_;
};

//----------------------------------------------------------------------------//
// forcing function: for solution sin(2*PI*x)*sin(2*PI*y)*sin(2*PI*z)

template<class PDE>
class ForcingFunction3D_SineSineSine : public ForcingFunctionBase3D<PDE>
{
public:
  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  static const int D = 3;                     // physical dimensions

  bool hasForcingFunction() const override { return true; }

  ForcingFunction3D_SineSineSine( Real a, Real b, Real c,
                                  Real kxx, Real kxy, Real kxz,
                                  Real kyx, Real kyy, Real kyz,
                                  Real kzx, Real kzy, Real kzz )
      : a_(a), b_(b), c_(c),
        kxx_(kxx), kxy_(kxy), kxz_(kxz),
        kyx_(kyx), kyy_(kyy), kyz_(kyz),
        kzx_(kzx), kzy_(kzy), kzz_(kzz) {}
  ~ForcingFunction3D_SineSineSine() {}

  void operator()( const PDE& pde, const Real &x, const Real &y, const Real &z,
      const Real &time, ArrayQ<Real>& forcing ) const override
  {
    Real snx = sin(2*PI*x), csx = cos(2*PI*x);
    Real sny = sin(2*PI*y), csy = cos(2*PI*y);
    Real snz = sin(2*PI*z), csz = cos(2*PI*z);

    forcing += 2*PI*((a_*csx*sny*snz + 2*PI*( kxx_*snx*sny*snz - kxy_*csx*csy*snz - kxz_*csx*sny*csz)) +
                     (b_*snx*csy*snz + 2*PI*(-kyx_*csx*csy*snz + kyy_*snx*sny*snz - kyz_*snx*csy*csz)) +
                     (c_*snx*sny*csz + 2*PI*(-kzx_*csx*sny*csz - kzy_*snx*csy*csz + kzz_*snx*sny*snz)));
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const override
    {
      std::string indent(indentSize, ' ');
      out << indent << "ForcingFunction3D_SineSine:"
                    << "  a_ = " << a_
                    << "  b_ = " << b_
                    << "  b_ = " << b_
                    << "  kxx_ = " << kxx_
                    << "  kxy_ = " << kxy_
                    << "  kxz_ = " << kxz_
                    << "  kyx_ = " << kyx_
                    << "  kyy_ = " << kyy_
                    << "  kyz_ = " << kyz_
                    << "  kzx_ = " << kzx_
                    << "  kzy_ = " << kzy_
                    << "  kzz_ = " << kzz_
                    << std::endl;
    }

private:
  Real a_, b_, c_;                                              // advection velocity
  Real kxx_, kxy_, kxz_, kyx_, kyy_, kyz_, kzx_, kzy_, kzz_;    // diffusion coefficients
};

//---------------------------------------------------------------------------------//
// forcing function: gaussian

template <class PDE>
class ForcingFunction3D_Gaussian : public ForcingFunctionBase3D<PDE>
{
public:
  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  static const int D = 3;         // physical dimensions

  bool hasForcingFunction() const override { return true; }

  explicit ForcingFunction3D_Gaussian( Real fa, Real fc, Real fx0, Real fy0, Real fz0, Real fsigma) :
      fa_(fa), fc_(fc), fx0_(fx0), fy0_(fy0), fz0_(fz0), fsigma_(fsigma) {}
  ~ForcingFunction3D_Gaussian() {}

  void operator() (const PDE& pde, const Real &x, const Real &y, const Real &z,
       const Real &time, ArrayQ<Real>& forcing ) const override
  {
    forcing += (fa_*(1/pow(sqrt(2*PI)*fsigma_,1))*exp((-1/(2*fsigma_*fsigma_))*(pow(x-fx0_,2))+pow(y-fy0_,2)+pow(z-fz0_,2)))+fc_;
  }

  void dump (int indentSize=0, std::ostream& out = std::cout ) const override
  {
    std::string indent(indentSize, ' ');
    out << indent << "ForcingFunction3D_Gaussian:" << std::endl;
  }

private:
  Real fa_,fc_,fx0_,fy0_,fz0_,fsigma_;

};

//---------------------------------------------------------------------------------//
// forcing function: expanding spherical wave
template <class PDE>
class ForcingFunction3D_SphericalWaveDecay : public ForcingFunctionBase3D<PDE>
{
public:
  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  static const int D = 3;         // physical dimensions

  bool hasForcingFunction() const override { return true; }

  explicit ForcingFunction3D_SphericalWaveDecay( const Real& alpha , const Real& k0 , const Real& k1 ,
                                      const Real& velocity , const Real& radius0  , const Real& nu ) :
    alpha_(alpha), k0_(k0), k1_(k1), velocity_(velocity), radius0_(radius0), nu_(nu)
  {}

  void operator() (const PDE& pde, const Real &x, const Real &y, const Real &z,
       const Real &t, ArrayQ<Real>& forcing ) const override
  {
    const Real r = std::sqrt(x*x+y*y+z*z);
    const Real& V = velocity_;
    const Real& k0 = k0_;
    const Real& k1 = k1_;
    const Real& r0 = radius0_;
    const Real& nu = nu_;
    const Real& alpha = alpha_;
    if (r<1e-3) return;
    Real s = -(k0*exp(-alpha*t-k1*pow(-r+r0+V*t,2.0))*(V*-2.0+alpha*r+(k1*k1)*nu*(r*r*r)*
               4.0-k1*nu*r*6.0+k1*nu*r0*4.0+V*k1*nu*t*4.0+(k1*k1)*nu*r*(r0*r0)*4.0-(k1*k1)*
               nu*(r*r)*r0*8.0-V*(k1*k1)*nu*(r*r)*t*8.0+(V*V)*(k1*k1)*nu*r*(t*t)*4.0+V*(k1*k1)*nu*r*r0*t*8.0))/r;
    forcing += s;
  }

  void dump (int indentSize=0, std::ostream& out = std::cout ) const override
  {
    std::string indent(indentSize, ' ');
    out << indent << "ForcingFunction3D_SphericalWaveDecay:" << std::endl;
  }

private:
  Real alpha_;
  Real k0_,k1_;
  Real velocity_;
  Real radius0_;
  Real nu_;
};


} //namespace SANS

#endif  // FORCINGFUNCTION3D_H
