// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOLUTIONFUNCTION2D_H
#define SOLUTIONFUNCTION2D_H

// 2-D Advection-Diffusion PDE: exact and MMS solutions

#include "tools/SANSnumerics.h"     // Real
#include "AdvectionDiffusion_Traits.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "PDEAdvectionDiffusion2D.h"

namespace SANS
{

# if 1 // Unused other than in unit test/can't use the BaseClass interface
//----------------------------------------------------------------------------//
// solution: Lagrange multiplier for diffusion with Dirichlet BCs
//
// lambda(x,y) = - (K grad(u)).n
//
// where u is exact solution
//
// NOTE: temporary placement; this depends on BCs
//   assuming Dirichlet
//   implemented by returning (K grad(u)) via 2nd operator()
template<class PDE, class ScalarFunction>
class ScalarFunction2D_LaplaceLagrangeDirichlet;

template<class ViscousFlux, class Source, class ScalarFunction>
class ScalarFunction2D_LaplaceLagrangeDirichlet< PDEAdvectionDiffusion<PhysD2, AdvectiveFlux2D_None,
                                                                         ViscousFlux, Source>, ScalarFunction >
{
public:
  typedef PhysD2 PhysDim;

  template<class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD2>::ArrayQ<T>;

  typedef PDEAdvectionDiffusion<PhysD2, AdvectiveFlux2D_None, ViscousFlux, Source> PDEAdvectionDiffusion2D;

  ScalarFunction2D_LaplaceLagrangeDirichlet( const PDEAdvectionDiffusion2D& pde, const ScalarFunction& soln )
      : pde_(pde), soln_(soln) {}
  ~ScalarFunction2D_LaplaceLagrangeDirichlet() {}

  ArrayQ<Real> operator()( const Real& x, const Real& y, const Real& t,
                           const Real& nx, const Real& ny ) const
  {
    ArrayQ<Real> u, ux, uy, ut, uxx, uxy, uyy;

    soln_.secondGradient( x, y, t, u, ux, uy, ut, uxx, uxy, uyy );

    ArrayQ<Real> fx = 0, fy = 0;
    pde_.fluxViscous( x, y, t, u, ux, uy, fx, fy );

    return nx*fx + ny*fy;
  }

private:
  const PDEAdvectionDiffusion2D& pde_;
  const ScalarFunction& soln_;    // exact solution u
};



//----------------------------------------------------------------------------//
// solution: adjoint Lagrange multiplier for advection-diffusion with Dirichlet BCs
//
// lambda(x,y) = - (K grad(psi)).n
//
// where psi is exact adjoint solution
//
// NOTE: temporary placement; this depends on BCs
//   assuming Dirichlet
//   implemented by returning (K grad(psi)) via 2nd operator()
//
// NOTE: assumes viscous flux is self adjoint; if not (or advective fluxes included),
// then must define adjoint PDE class!!
template<class PDE, class ScalarFunction>
class ScalarFunction2D_AdjointLaplaceLagrangeDirichlet;

template<class ViscousFlux, class Source, class ScalarFunction>
class ScalarFunction2D_AdjointLaplaceLagrangeDirichlet< PDEAdvectionDiffusion<PhysD2, AdvectiveFlux2D_None,
                                                                                ViscousFlux, Source>, ScalarFunction >
{
public:
  typedef PhysD2 PhysDim;

  template<class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD2>::ArrayQ<T>;

  typedef PDEAdvectionDiffusion<PhysD2, AdvectiveFlux2D_None, ViscousFlux, Source> PDEAdvectionDiffusion2D;

  ScalarFunction2D_AdjointLaplaceLagrangeDirichlet( const PDEAdvectionDiffusion2D& pde,
        const ScalarFunction& adjSolution )
      : pde_(pde), adjSolution_(adjSolution) {}
  ~ScalarFunction2D_AdjointLaplaceLagrangeDirichlet() {}

  ArrayQ<Real> operator()( const Real& x, const Real& y, const Real& t,
                           const Real& nx, const Real& ny ) const
  {
    ArrayQ<Real> u, ux, uy, ut, uxx, uxy, uyy;

    adjSolution_( x, y, t, u, ux, uy, ut, uxx, uxy, uyy );

    ArrayQ<Real> fx = 0, fy = 0;
    pde_.fluxViscous( x, y, t, u, ux, uy, fx, fy );

    return nx*fx + ny*fy;
  }

private:
  const PDEAdvectionDiffusion2D& pde_;
  const ScalarFunction& adjSolution_;
};

#endif
} // namespace SANS

#endif  // SOLUTIONFUNCTION2D_H
