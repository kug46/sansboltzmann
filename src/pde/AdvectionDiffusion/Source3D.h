// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOURCE3D_H
#define SOURCE3D_H

// 3-D Advection-Diffusion PDE: source term

#include "tools/SANSnumerics.h"     // Real
#include "Surreal/PromoteSurreal.h"
#include "AdvectionDiffusion_Traits.h"

#include <iostream>
#include <cmath>

namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: source
//
// member functions:
//   .()      functor returning source
//----------------------------------------------------------------------------//

class Source3D_None
{
public:
  template<class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD3>::ArrayQ<T>;

  template<class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD3>::MatrixQ<T>;

  bool hasSourceTerm() const { return false; }
  bool hasSourceTrace() const { return false; }
  bool needsSolutionGradientforSource() const { return false; }

  template<class Tq, class Tg, class Ts>
  void source( const Real& x, const Real& y, const Real& z, const Real& time,
               const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
               ArrayQ<Ts>& source ) const
  {
  }

  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& xL, const Real& yL, const Real& zL,
      const Real& xR, const Real& yR, const Real& zR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qzL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qzR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
  {
  }

  template <class Tq, class Tg, class Ts>
  void jacobianSource(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdu ) const
  {
  }

  template <class Tq, class Tg, class Ts>
  void jacobianGradientSource(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy, MatrixQ<Ts>& dsduz ) const
  {
  }

  template <class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdu ) const  {}

  // div of jacobian of source wrt gradient: div.d(S)/d(Ux)
  template <class Tq, class Tg, class Th, class Ts>
  void jacobianGradientSourceGradient(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qyx, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qzx, const ArrayQ<Th>& qzy, const ArrayQ<Th>& qzz,
      MatrixQ<Ts>& div_dsdgradu ) const {}

//  void dump( int indentSize=0, std::ostream& out = std::cout ) const = 0;

};

//----------------------------------------------------------------------------//
// Source: Uniform
//
// source = a_*q where a_ is a constant
//----------------------------------------------------------------------------//

class Source3D_Uniform
{
public:
  template<class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD3>::ArrayQ<T>;

  template<class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD3>::MatrixQ<T>;

  explicit Source3D_Uniform( const Real a) : a_(a) {}

  bool hasSourceTerm() const { return true; }
  bool hasSourceTrace() const { return false; }
  bool needsSolutionGradientforSource() const { return false; }

  template<class Tq, class Tg, class Ts>
  void source( const Real& x, const Real& y, const Real& z, const Real& time,
               const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
               ArrayQ<Ts>& source ) const
  {
    source += a_*q;
  }

  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& xL, const Real& yL, const Real& zL,
      const Real& xR, const Real& yR, const Real& zR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qzL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qzR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
  {
    sourceL += a_*qL;
    sourceR += a_*qR;
  }

  template <class Tq, class Tg, class Ts>
  void jacobianSource(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdu ) const
  {
    dsdu += a_;
  }

  template <class Tq, class Tg, class Ts>
  void jacobianGradientSource(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy, MatrixQ<Ts>& dsduz ) const
  {
  }

  template <class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdu ) const  {}

  // div of jacobian of source wrt gradient: div.d(S)/d(Ux)
  template <class Tq, class Tg, class Th, class Ts>
  void jacobianGradientSourceGradient(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qyx, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qzx, const ArrayQ<Th>& qzy, const ArrayQ<Th>& qzz,
      MatrixQ<Ts>& div_dsdgradu ) const {}

//  void dump( int indentSize=0, std::ostream& out = std::cout ) const = 0;

protected:
  const Real a_;
};

//----------------------------------------------------------------------------//
// Source: Gradient
//
// source = a_*q + b_*qx + c_*qy + d_*qz where a_, b_, c_ and d_ are constants
//----------------------------------------------------------------------------//

class Source3D_UniformGrad
{
public:
  template<class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD3>::ArrayQ<T>;

  template<class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD3>::MatrixQ<T>;

  explicit Source3D_UniformGrad( const Real a, const Real b, const Real c, const Real d)
  : a_(a), b_(b), c_(c), d_(d) {}

  bool hasSourceTerm() const { return true; }
  bool hasSourceTrace() const { return false; }
  bool needsSolutionGradientforSource() const { return true; }

  template<class Tq, class Tg, class Ts>
  void source( const Real& x, const Real& y, const Real& z, const Real& time,
               const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
               ArrayQ<Ts>& source ) const
  {
    source += a_*q + b_*qx + c_*qy + d_*qz;
  }

  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& xL, const Real& yL, const Real& zL,
      const Real& xR, const Real& yR, const Real& zR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qzL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qzR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
  {
    sourceL += a_*qL + b_*qxL + c_*qyL + d_*qzL;
    sourceR += a_*qR + b_*qxR + c_*qyR + d_*qzR;
  }

  template <class Tq, class Tg, class Ts>
  void jacobianSource(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdu ) const
  {
    dsdu += a_;
  }

  template <class Tq, class Tg, class Ts>
  void jacobianGradientSource(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy, MatrixQ<Ts>& dsduz ) const
  {
    dsdux += b_;
    dsduy += c_;
    dsduz += d_;
  }


  template <class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdu ) const
  {
    dsdu += fabs(a_);
  }

  // div of jacobian of source wrt gradient: div.d(S)/d(Ux)
  template <class Tq, class Tg, class Th, class Ts>
  void jacobianGradientSourceGradient(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qyx, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qzx, const ArrayQ<Th>& qzy, const ArrayQ<Th>& qzz,
      MatrixQ<Ts>& div_dsdgradu ) const {}

//  void dump( int indentSize=0, std::ostream& out = std::cout ) const = 0;

protected:
  const Real a_;
  const Real b_;
  const Real c_;
  const Real d_;
};

} //namespace SANS

#endif  // SOURCE3D_H
