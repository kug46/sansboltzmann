// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// 3D advection flux

#include <string>
#include <iostream>

#include "AdvectiveFlux3D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// advection flux: nothing

void
AdvectiveFlux3D_None::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "AdvectiveFlux3D: NONE" << std::endl;
}

//----------------------------------------------------------------------------//
// advection flux: uniform flow

void
AdvectiveFlux3D_Uniform::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "AdvectiveFlux3D_Uniform:"
            << "  u_ = " << u_
            << "  v_ = " << v_
            << "  w_ = " << w_ << std::endl;
}

//----------------------------------------------------------------------------//
// advection flux: radial
void
AdvectiveFlux3D_Radial::dump( int indentSize , std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "AdvectiveFlux3D_Radial"
            << "  V_ = " << V_  << std::endl;
}

void
AdvectiveFlux3D_Radial::velocity( const Real x , const Real y , const Real z , Real& u , Real& v , Real& w ) const
{
  // compute radial coordinate
  Real r = std::sqrt( x*x + y*y + z*z );
  if (r<1e-3)
  {
    u = V_;
    v = V_;
    w = V_;
    return;
  }
  u = V_*x/r;
  v = V_*y/r;
  w = V_*z/r;
}

// //----------------------------------------------------------------------------//
// // advection flux: pipe
// void
// AdvectiveFlux3D_SquareChannel::dump( int indentSize , std::ostream& out ) const
// {
//   std::string indent(indentSize, ' ');
//   out << indent << "AdvectiveFlux3D_SquareChannel" << "  ... finish me!"
//       << std::endl;
//   // SANS_DEVELOPER_EXCEPTION("implement. -cvf");
// }

void
AdvectiveFlux3D_SquareChannel::velocity( const Real x,
                                         const Real y,
                                         const Real z,
                                         Real& u, Real& v, Real& w) const
{
  u= 0.0;
  v= 0.0;
  w= (nu_*lambda_z_*lambda_z_ - 2*nu_/b_ - 2*nu_/c_ - lambda_t_)/lambda_z_;
}

//----------------------------------------------------------------------------//
// advection flux: Spatially dependent

void
AdvectiveFlux3D_PolyZ::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "AdvectiveFlux3D_PolyZ:"
            << "  a0_ = " << a0_
            << "  a1_ = " << a1_
            << "  a2_ = " << a2_
            << "  a3_ = " << a3_
            << "  a4_ = " << a4_
            << "  a5_ = " << a5_
            << "  a6_ = " << a6_
            << "  a7_ = " << a7_
            << "  a8_ = " << a8_
            << "  a9_ = " << a9_<< std::endl;
}

//----------------------------------------------------------------------------//
// advective flux: random

void
AdvectiveFlux3D_Random::dump( int indentSize, std::ostream &out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "AdvectiveFlux3D_Random:"
      << "  u0_= " << u0_
      << "  v0_= " << v0_
      << "  w0_= " << w0_ << std::endl;
}

} //namespace SANS
