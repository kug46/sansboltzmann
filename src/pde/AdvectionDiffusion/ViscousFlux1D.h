// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef DIFFUSIONMATRIX1D_H
#define DIFFUSIONMATRIX1D_H

// 1-D Advection-Diffusion PDE: diffusion matrix

#include "tools/SANSnumerics.h"     // Real
#include "AdvectionDiffusion_Traits.h"

#include <iostream>
#include <cmath>   //exp, isnan
#include <utility> // forward

namespace SANS
{

class ViscousFlux1DBase
{
public:
  virtual void diffusionViscous( const Real& x, const Real& time, const Real& q, const Real& qx,
                                 Real& kxx ) const = 0;

  virtual ~ViscousFlux1DBase() {}
};

template<class Derived>
class ViscousFlux1DVirtualInterface : public ViscousFlux1DBase
{
public:
  virtual void diffusionViscous( const Real& x, const Real& time, const Real& q, const Real& qx,
                                 Real& kxx ) const override
  {
    reinterpret_cast<const Derived&>(*this).diffusionViscous(x, time, q, qx, kxx);
  }

  virtual ~ViscousFlux1DVirtualInterface() {}
};


//----------------------------------------------------------------------------//
template<class DiffusionMatrix>
class ViscousFlux1D_LinearInGradient : public DiffusionMatrix,
                                       public ViscousFlux1DVirtualInterface<ViscousFlux1D_LinearInGradient<DiffusionMatrix>>
{
public:
  using DiffusionMatrix::diffusionViscous;

  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD1>::template MatrixQ<T>;  // matrices

  template< class... Args > // cppcheck-suppress noExplicitConstructor
  ViscousFlux1D_LinearInGradient(Args&&... args) : DiffusionMatrix(std::forward<Args>(args)...) {}

  // viscous flux is of the form K(q).gradu
  bool fluxViscousLinearInGradient() const { return true; }

  // viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Tf>& f ) const
  {
    MatrixQ<Tq> kxx = 0.0;

    diffusionViscous( x, time, q, qx, kxx );

    f -= kxx*qx;
  }

};


//----------------------------------------------------------------------------//
// diffusion matrix: None
//
// member functions:
//   .()      functor returning diffusion coefficient matrix
//----------------------------------------------------------------------------//

class DiffusionMatrix1D_None
{
public:
  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD1>::template MatrixQ<T>;  // matrices

  static const int D = 1;                     // physical dimensions

  bool hasFluxViscous() const { return false; }

  DiffusionMatrix1D_None() {}

  template <class Tq, class Tg, class Tk>
  void diffusionViscous( const Real x, const Real time, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, MatrixQ<Tk>& kxx ) const {}


  template <class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Th>& qxx, MatrixQ<Tk>& kxx_x ) const {}

  // strong form of viscous flux: d(Fv)/d(X)
  template <class Tq, class Tg, class Th, class Tf>
  inline void
  strongFluxViscous( const Real& x, const Real& time,
                     const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
                     const ArrayQ<Th>& qxx,
                     ArrayQ<Tf>& strongPDE ) const {}

  // strong form of viscous flux: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx,
      MatrixQ<Tf>& ax ) const {}

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;
};

typedef ViscousFlux1D_LinearInGradient<DiffusionMatrix1D_None> ViscousFlux1D_None;

//----------------------------------------------------------------------------//
// diffusion matrix: uniform
//
// member functions:
//   .()      functor returning diffusion coefficient matrix
//----------------------------------------------------------------------------//

class DiffusionMatrix1D_Uniform
{
public:
  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD1>::template MatrixQ<T>;  // matrices

  static const int D = 1;                     // physical dimensions

  bool hasFluxViscous() const { return true; }

  explicit DiffusionMatrix1D_Uniform( const Real kxx ) :
      kxx_(kxx) {}

  template <class Tq, class Tg, class Tk>
  void diffusionViscous( const Real x, const Real time,
                         const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
                         MatrixQ<Tk>& kxx ) const
  {
    kxx += kxx_;
  }


  template <class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Th>& qxx, MatrixQ<Tk>& kxx_x ) const {}

  // strong form of viscous flux: d(Fv)/d(X)
  template <class Tq, class Tg, class Th, class Tf>
  inline void
  strongFluxViscous( const Real& x, const Real& time,
                     const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
                     const ArrayQ<Th>& qxx,
                     ArrayQ<Tf>& strongPDE ) const
  {
    strongPDE -= kxx_*qxx;
  }

  // strong form of viscous flux: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx,
      MatrixQ<Tf>& ax ) const
  {
    // Fv not a function of U
  }

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  const Real kxx_;          // constant diffusion matrix
};

typedef ViscousFlux1D_LinearInGradient<DiffusionMatrix1D_Uniform> ViscousFlux1D_Uniform;

//----------------------------------------------------------------------------//
// diffusion matrix: exponential exp(lam*q)
//
// member functions:
//   .()      functor returning diffusion coefficient matrix
//
// Useful for testing a non-linear poisson implementation with a diffusive flux:
//
// Fv = exp(lam*q)*dq/dx
//
// The exact solution is:
//
// Q(x) = 1/lam * ln( (A + B*x)/ D)
//
// where A, B, and D are arbitrary coefficients determined completely by the BC's.
//----------------------------------------------------------------------------//

class DiffusionMatrix1D_Exp
{
public:
  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD1>::template MatrixQ<T>;  // matrices

  static const int D = 1;                     // physical dimensions

  bool hasFluxViscous() const { return true; }

  explicit DiffusionMatrix1D_Exp( const Real lam ) :
    lam_(lam) {}

  template <class Tq, class Tg, class Tk>
  void diffusionViscous( const Real x, const Real time,
                         const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
                         MatrixQ<Tk>& kxx ) const
  {
    kxx += exp(q*lam_);
  }


  template <class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Th>& qxx, MatrixQ<Tk>& kxx_x ) const
  {
    kxx_x += exp(q*lam_)*lam_*qx;
  }

  // strong form of viscous flux: d(Fv)/d(X)
  template <class Tq, class Tg, class Th, class Tf>
  inline void
  strongFluxViscous( const Real& x, const Real& time,
                     const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
                     const ArrayQ<Th>& qxx,
                     ArrayQ<Tf>& strongPDE ) const
  {
    strongPDE -= lam_*exp(q*lam_)*qx*qx + exp(q*lam_)*qxx;
  }


  // strong form of viscous flux: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx,
      MatrixQ<Tf>& ax ) const
  {
    ax -= lam_*exp(q*lam_)*qx;
  }

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const
  {
    return q < 10;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  const Real lam_;          // exponential coefficient
};

typedef ViscousFlux1D_LinearInGradient<DiffusionMatrix1D_Exp> ViscousFlux1D_Exp;


class DiffusionMatrix1D_Oliver
{
public:
  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD1>::template MatrixQ<T>;  // matrices

  static const int D = 1;                     // physical dimensions

  bool hasFluxViscous() const { return true; }

  explicit DiffusionMatrix1D_Oliver( const Real nu) :
    nu_(nu) {}

  template <class Tq, class Tg, class Tk>
  void diffusionViscous( const Real x, const Real time,
                         const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
                         MatrixQ<Tk>& kxx ) const
  {
    kxx += nu_ + q;
//    kxx += nu_;
  }


  template <class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Th>& qxx, MatrixQ<Tk>& kxx_x ) const
  {
    kxx_x += qx;
  }

  // strong form of viscous flux: d(Fv)/d(X)
  template <class Tq, class Tg, class Th, class Tf>
  inline void
  strongFluxViscous( const Real& x, const Real& time,
                     const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
                     const ArrayQ<Th>& qxx,
                     ArrayQ<Tf>& strongPDE ) const
  {
    strongPDE -= qx*qx + (nu_ + q)*qxx;
//    strongPDE -= nu_*qxx;
  }


  // strong form of viscous flux: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx,
      MatrixQ<Tf>& ax ) const
  {
    ax -= qx;
  }

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const
  {
    return true;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  const Real nu_;          // exponential coefficient
};

typedef ViscousFlux1D_LinearInGradient<DiffusionMatrix1D_Oliver> ViscousFlux1D_Oliver;

} //namespace SANS

#endif  // DIFFUSIONMATRIX1D_H
