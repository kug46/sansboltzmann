// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FORCINGFUNCTION2D_H
#define FORCINGFUNCTION2D_H

// 2-D Advection-Diffusion PDE: RHS forcing function

#include "tools/SANSException.h"
#include "tools/SANSnumerics.h"     // Real
#include "AdvectionDiffusion_Traits.h"
#include "pde/ForcingFunctionBase.h"

#include <cmath>
#include <iostream>

namespace SANS
{


//----------------------------------------------------------------------------//
// forcing function: constant

template <class PDE>
class ForcingFunction2D_Const : public ForcingFunctionBase2D<PDE>
{
public:
  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  bool hasForcingFunction() const override { return true; }

  explicit ForcingFunction2D_Const( Real c ) : c_(c) {}


  void operator()
    ( const PDE& pde, const Real &x, const Real &y, const Real &time, ArrayQ<Real>& forcing ) const override
  { forcing += c_; }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const override;

private:
  Real c_;
};

template<class PDE>
void ForcingFunction2D_Const<PDE>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "ForcingFunction2D_Const:  c_ = " << c_ << std::endl;
}

#if 0
//----------------------------------------------------------------------------//
// forcing function: monomial
// NOTE: code for Laplacian only
template<class PDE>
class ForcingFunction2D_Monomial : public ForcingFunction2D
{
public:
  static const int D = 2;                     // physical dimensions

  explicit ForcingFunction2D_Monomial( int i, int j, Real nu ) : i_(i), j_(j), nu_(nu) {}
  virtual ~ForcingFunction2D_Monomial() {}

  virtual void operator()( const Real x, const Real y, const Real time, Real& src ) const
  {
    src = -nu_*( pow(x, i_)*pow(y, j_-2)*j_*(j_ - 1) + pow(x, i_-2)*pow(y, j_)*i_*(i_ - 1) );
  }

  virtual void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  int i_, j_;
  Real nu_;
};
//----------------------------------------------------------------------------//
/*
 * Forcing Functions for the 4 Exponential based Solutions, for use with laplacian
 *
 * Exponential Exact Solutions
 *
 * Family of the form:
 * z(1) = (exp(x)-1)*(1-x)*(exp(y)-1)*(1-y);
 * z(2) = x*(1-exp(1-x))*(exp(y)-1)*(1-y);
 * z(3) = (exp(x)-1)*(1-x)*y*(exp(1-y)-1);
 * z(4) = x*(1-exp(1-x))*y*(exp(1-y)-1);
 *
 * The maxima dots for each are in slightly different locations
 */
class ForcingFunction2D_Exp1 : public ForcingFunction2D
{
public:
  static const int D = 2;                     // physical dimensions

  ForcingFunction2D_Exp1() {}
  virtual ~ForcingFunction2D_Exp1() {}

  virtual void operator()( const Real x, const Real y, const Real time, Real& src ) const
  {
    Real qxx, qyy;
    qxx = exp(x)*expm1(y)*(x + 1)*(y - 1);
    qyy = exp(y)*expm1(x)*(x - 1)*(y + 1);
    src = -qxx -qyy;
  }
  virtual void dump( int indentSize=0, std::ostream& out = std::cout ) const;

};
class ForcingFunction2D_Exp2 : public ForcingFunction2D
{
public:
  static const int D = 2;                     // physical dimensions

  ForcingFunction2D_Exp2() {}
  virtual ~ForcingFunction2D_Exp2() {}

  virtual void operator()( const Real x, const Real y, const Real time, Real& src ) const
  {
    Real qxx, qyy;
    qxx = exp(1 - x)*expm1(y)*(x - 2)*(y - 1);
    qyy = -x*exp(1 - x)*exp(y)*expm1(x-1)*(y + 1);
    src = -qxx -qyy;
  }
  virtual void dump( int indentSize=0, std::ostream& out = std::cout ) const;
};
class ForcingFunction2D_Exp3 : public ForcingFunction2D
{
public:
  static const int D = 2;                     // physical dimensions

  ForcingFunction2D_Exp3() {}
  virtual ~ForcingFunction2D_Exp3() {}

  virtual void operator()( const Real x, const Real y, const Real time, Real& src ) const
  {
    Real qxx, qyy;
    qxx =y*exp(1 - y)*exp(x)*expm1(y-1)*(x + 1);
    qyy =-exp(1 - y)*expm1(x)*(x - 1)*(y - 2);
    src = -qxx -qyy;
  }
  virtual void dump( int indentSize=0, std::ostream& out = std::cout ) const;
};
class ForcingFunction2D_Exp4 : public ForcingFunction2D
{
public:
  static const int D = 2;                     // physical dimensions

  ForcingFunction2D_Exp4() {}
  virtual ~ForcingFunction2D_Exp4() {}

  virtual void operator()( const Real x, const Real y, const Real time, Real& src ) const
  {
    Real qxx, qyy;
    qxx =y*exp(1 - x)*exp(1 - y)*expm1(y-1)*(x - 2);
    qyy =x*exp(1 - x)*exp(1 - y)*expm1(x-1)*(y - 2);
    src = -qxx -qyy;
  }
  virtual void dump( int indentSize=0, std::ostream& out = std::cout ) const;
};

//----------------------------------------------------------------------------//
// Constructable Forcing Function
class ForcingFunction2D_VarExp : public ForcingFunction2D
{
public:
  static const int D = 2;                     // physical dimensions

  explicit ForcingFunction2D_VarExp( Real a, Real b, Real c, Real d, Real e) : a_(a), b_(b), c_(c), d_(d), e_(e) {}
  virtual ~ForcingFunction2D_VarExp() {}

  virtual void operator()( const Real x, const Real y, const Real time, Real& src ) const
  {
    Real qxx, qyy;
    qxx = y*exp(a_*x*x + c_*x + b_*y*y + d_*y + e_)*(y - 1)*
            (4*a_*a_*x*x*x*x - 4*a_*a_*x*x*x + 4*a_*c_*x*x*x - 4*a_*c_*x*x + 10*a_*x*x - 6*a_*x + c_*c_*x*x - c_*c_*x + 4*c_*x - 2*c_ + 2);
    qyy = x*exp(a_*x*x + c_*x + b_*y*y + d_*y + e_)*(x - 1)*
            (4*b_*b_*y*y*y*y - 4*b_*b_*y*y*y + 4*b_*d_*y*y*y - 4*b_*d_*y*y + 10*b_*y*y - 6*b_*y + d_*d_*y*y - d_*d_*y + 4*d_*y - 2*d_ + 2);

    src = -qxx-qyy;
  }

  virtual void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  Real a_, b_, c_, d_, e_;
};
#endif

//----------------------------------------------------------------------------//
// forcing function: for solution sin(2*PI*x)*sin(2*PI*y)

template<class PDE>
class ForcingFunction2D_SineSine : public ForcingFunctionBase2D<PDE>
{
public:
  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  bool hasForcingFunction() const override { return true; }

  ForcingFunction2D_SineSine( Real a, Real b, Real kxx, Real kxy, Real kyx, Real kyy )
      : a_(a), b_(b), kxx_(kxx), kxy_(kxy), kyx_(kyx), kyy_(kyy) {}
  ~ForcingFunction2D_SineSine() {}

  void operator()
    ( const PDE& pde, const Real &x, const Real &y, const Real &time, ArrayQ<Real>& forcing ) const override
  {
    Real snx = sin(2*PI*x), csx = cos(2*PI*x);
    Real sny = sin(2*PI*y), csy = cos(2*PI*y);

    forcing = 2*PI*(csx*(a_*sny - 2*PI*(kxy_ + kyx_)*csy) + snx*(b_*csy + 2*PI*(kxx_ + kyy_)*sny));
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const override;

private:
  Real a_, b_;                    // advection velocity
  Real kxx_, kxy_, kyx_, kyy_;    // diffusion coefficients
};

template<class PDE>
void ForcingFunction2D_SineSine<PDE>::dump( int indentSize, std::ostream& out ) const
{
std::string indent(indentSize, ' ');
out << indent << "ForcingFunction2D_SineSine:"
            << "  a_ = " << a_
            << "  b_ = " << b_
            << "  kxx_ = " << kxx_
            << "  kxy_ = " << kxy_
            << "  kyx_ = " << kyx_
            << "  kyy_ = " << kyy_ << std::endl;
}

#if 0
//----------------------------------------------------------------------------//
// forcing function: for solution sin(2*PI*x)*sin(2*PI*y)

class ForcingFunction2D_SineSineRotated : public ForcingFunction2D
{
public:
  static const int D = 2;                     // physical dimensions

  ForcingFunction2D_SineSineRotated( Real a, Real b, Real kxx, Real kxy, Real kyx, Real kyy )
      : a_(a), b_(b), kxx_(kxx), kxy_(kxy), kyx_(kyx), kyy_(kyy) {}
  virtual ~ForcingFunction2D_SineSineRotated() {}

  virtual void operator()( const Real x, const Real y, const Real time, Real& src ) const
  {
    Real th  = PI/7.;
    Real xi  =  x*cos(th) + y*sin(th);
    Real eta = -x*sin(th) + y*cos(th);

    Real snt = sin(th), cst = cos(th), sntt = sin(2*th), cstt = cos(2*th);
    Real snx = sin(2*PI*xi),  csx = cos(2*PI*xi);
    Real sny = sin(2*PI*eta), csy = cos(2*PI*eta);

    src = 2*PI*( a_*(cst*csx*sny - snt*snx*csy) + b_*(cst*snx*csy + snt*snx*sny)
        - 2*PI*( - kxx_*(sntt*csx*csy + snx*sny) + kyy_*(sntt*csx*csy - snx*sny)
                 + (kxy_ + kyx_)*(cstt*csx*csy) ) );
  }

  virtual void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  Real a_, b_;                    // advection velocity
  Real kxx_, kxy_, kyx_, kyy_;    // diffusion coefficients
};
#endif

//----------------------------------------------------------------------------//
// forcing function: c * cos(theta) where theta is the polar angle

template<class PDE>
class ForcingFunction2D_SineTheta : public ForcingFunctionBase2D<PDE>
{
public:
  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  bool hasForcingFunction() const override { return true; }

  ForcingFunction2D_SineTheta() : c_(1.0) {}
  explicit ForcingFunction2D_SineTheta( Real c ) : c_(c) {}

  ~ForcingFunction2D_SineTheta() {}

  void operator()
    ( const PDE& pde, const Real &x, const Real &y, const Real &time, ArrayQ<Real>& forcing ) const override
  {
    forcing = c_ * cos( atan2(y,x) );
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const override;

private:
  Real c_;                    // advection velocity
};

template<class PDE>
void ForcingFunction2D_SineTheta<PDE>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "ForcingFunction2D_SineTheta:"
      << "  c_ = " << c_ << std::endl;
}

//----------------------------------------------------------------------------//
// forcing function: c * cos(theta) / r,  where (r,theta) are the polar coordinates

template<class PDE>
class ForcingFunction2D_SineThetaTopo2D : public ForcingFunctionBase2D<PDE>
{
public:
  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  bool hasForcingFunction() const override { return true; }

  ForcingFunction2D_SineThetaTopo2D() : c_(1.0) {}
  explicit ForcingFunction2D_SineThetaTopo2D( Real c ) : c_(c) {}

  ~ForcingFunction2D_SineThetaTopo2D() {}

  void operator()
    ( const PDE& pde, const Real &x, const Real &y, const Real &time, ArrayQ<Real>& forcing ) const override
  {
    Real r = sqrt(x*x + y*y);
    forcing = c_ * cos( atan2(y,x) ) / r;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const override;

private:
  Real c_;                    // advection velocity
};

template<class PDE>
void ForcingFunction2D_SineThetaTopo2D<PDE>::dump(int indentSize, std::ostream& out) const
{
  std::string indent(indentSize, ' ');
  out << indent << "ForcingFunction2D_SineThetaTopo2D:";
}

//----------------------------------------------------------------------------//
// forcing function: c * cos(theta) - (2D wall flux terms) where theta is the polar angle
// 2D wall flux terms arise because the actual geometry is faceted, i.e. piecewise linear

template<class PDE>
class ForcingFunction2D_SineThetaFacet : public ForcingFunctionBase2D<PDE>
{
public:
  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  bool hasForcingFunction() const override { return true; }

  ForcingFunction2D_SineThetaFacet( const int& n, const Real& dhdR1, const Real& thetaRan, const Real& V, const Real& c, const Real& R1 )
      : n_(n), dhdR1_(dhdR1), thetaRan_(thetaRan), V_(V), c_(c), R1_(R1) {}

  ~ForcingFunction2D_SineThetaFacet() {}

  void operator()
    ( const PDE& pde, const Real &x, const Real &y, const Real &time, ArrayQ<Real>& forcing ) const override
  {
    SANS_ASSERT_MSG( thetaRan_ > 0, "thetaRan_ is not strictly positive" );

    const Real theta = atan2(y,x);     // polar angle of (x,y)
    const Real r = sqrt( x*x + y*y );
    const Real dtheta = thetaRan_*PI/180.0/static_cast<Real>(n_);  // [radian] increment of theta per element
    Real intpart, fracpart;
    fracpart = modf( theta/dtheta, &intpart );
    Real cosalpha, cosalpha2;
    if ( fracpart >= 0)
    {
      Real drx = cos( (intpart+1)*dtheta ) - cos( intpart*dtheta );
      Real dry = sin( (intpart+1)*dtheta ) - sin( intpart*dtheta );
      Real dr = sqrt( drx*drx + dry*dry );
      cosalpha = - ( x*drx + y*dry ) / ( r*dr );

      Real nwTx = cos( (intpart+0.5)*dtheta );
      Real nwTy = sin( (intpart+0.5)*dtheta );
      Real u = - V_ * y / r;     // x-component of advection velocity
      Real v =   V_ * x / r;     // y-component of advection velocity
      cosalpha2 = (u*nwTx + v*nwTy) / ( sqrt(u*u+v*v)*sqrt(nwTx*nwTx+nwTy*nwTy) );

      SANS_ASSERT_MSG( fabs(cosalpha - cosalpha2) <= 1.e-12, "cosalpha != cosalpha2" );
      SANS_ASSERT_MSG( cosalpha * cosalpha2 >= -1.e-15, "cosalpha %f and cosalpha2 %f have different signs", cosalpha, cosalpha2 );
      // [above] it's fine if both are zero but have different signs
    }
    else
      SANS_DEVELOPER_EXCEPTION("fracpart cannot be negative");

//    src = c_*cos(theta) + sin(theta) * V_*cosalpha2 * (r/R1_-1);
    forcing = c_*cos(theta) + sin(theta) * V_*cosalpha2 * r/R1_*dhdR1_;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const override;

private:
  const int n_;          // number of elements
  const Real dhdR1_;     // R_2/R_1
  const Real thetaRan_;  // [degree] range of theta starting from 0, i.e. []
  const Real V_;         // advection speed
  const Real c_;         // c * cos(theta)
  const Real R1_;        // R_1
};

template<class PDE>
void ForcingFunction2D_SineThetaFacet<PDE>::dump(int indentSize, std::ostream& out) const
{
  std::string indent(indentSize, ' ');
  out << indent << "ForcingFunction2D_SineThetaFacet:";
}

//----------------------------------------------------------------------------//
// forcing function: c * p*theta^(p-1) where theta is the polar angle
// solution: first derivative of geometric series in theta
//           c*(0 + theta +... + p*theta^(p-1)) where theta = atan2(y,x) is the angle in polar coordinates

template<class PDE>
class ForcingFunction2D_ThetaGeometricSeries : public ForcingFunctionBase2D<PDE>
{
public:
  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  bool hasForcingFunction() const override { return true; }

  ForcingFunction2D_ThetaGeometricSeries() : c_(1.0), p_(1.0) {}
  ForcingFunction2D_ThetaGeometricSeries( Real c, int p ) : c_(c), p_(p) {}

  ~ForcingFunction2D_ThetaGeometricSeries() {}

  void operator()
    ( const PDE& pde, const Real &x, const Real &y, const Real &time, ArrayQ<Real>& forcing ) const override
  {
    SANS_ASSERT_MSG( p_ >= 0, "Exponent p_ must be greater or equal to 0.");
    Real soln = 0;

    if ( p_ >= 1)
      soln = 1;

    for (int i = 2; i <= p_; i++)
    {
      soln += static_cast<Real>(i) * pow( atan2(y,x), i-1 );
    }
    forcing = c_ * soln;

//    src = c_ * p_ * pow( atan2(y,x), p_-1 );
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const override;

private:
  Real c_;
  int p_;
};

template<class PDE>
void ForcingFunction2D_ThetaGeometricSeries<PDE>::dump(int indentSize, std::ostream& out) const
{
  std::string indent(indentSize, ' ');
  out << indent << "ForcingFunction2D_ThetaGeometricSeries:"
      << "  c_ = " << c_ << std::endl
      << "  p_ = " << p_ << std::endl;
}

//----------------------------------------------------------------------------//
// forcing function: c * p*theta^(p-1) where theta is the polar angle
// solution: first derivative of geometric series in theta
//           c*(0 + theta +... + p*theta^(p-1)) where theta = atan2(y,x) is the angle in polar coordinates

template<class PDE>
class ForcingFunction2D_FakeSensor : public ForcingFunctionBase2D<PDE>
{
public:
  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  bool hasForcingFunction() const override { return true; }

  ForcingFunction2D_FakeSensor() :
    c_(1.0),
    eta_(1.0)
  {
    // Nothing
  }
  ForcingFunction2D_FakeSensor( Real c, Real eta ) :
    c_(c),
    eta_(eta)
  {
    // Nothing
  }

  ~ForcingFunction2D_FakeSensor() {}

  void operator()
    ( const PDE& pde, const Real &x, const Real &y, const Real &time, ArrayQ<Real>& forcing ) const override
  {
    if (y < 1.0)
    {
      forcing = c_*exp(-fabs(x - 1.0) / eta_) * exp(y - 1.0);
      return;
    }

    Real xs = (1.0 + y) / 2.0;

    forcing = c_*exp(-fabs(x - xs) / eta_);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const override;

private:
  Real c_;
  Real eta_;
};

template<class PDE>
void ForcingFunction2D_FakeSensor<PDE>::dump(int indentSize, std::ostream& out) const
{
  std::string indent(indentSize, ' ');
  out << indent << "ForcingFunction2D_FakeSensor:"
      << "  c_ = " << c_ << std::endl
      << "  eta_ = " << eta_ << std::endl;
}


//----------------------------------------------------------------------------//
// forcing function: constant

template <class PDE>
class ForcingFunction2D_Gaussian : public ForcingFunctionBase2D<PDE>
{
public:
  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  bool hasForcingFunction() const override { return true; }

  explicit ForcingFunction2D_Gaussian( ) :
      xc_(0.1),
      yc_(0.5),
      sigx_(0.05),
      sigy_(0.05)
  {
    // Nothing
  }


  void operator()
    ( const PDE& pde, const Real &x, const Real &y, const Real &time, ArrayQ<Real>& forcing ) const override
  {
    forcing += 1./(2.*PI*sigx_*sigy_) * exp(-0.5*(pow((x-xc_)/sigx_,2) + pow((y-yc_)/sigy_,2)));
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const override;

private:
  Real xc_;
  Real yc_;
  Real sigx_;
  Real sigy_;
};

template<class PDE>
void ForcingFunction2D_Gaussian<PDE>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "ForcingFunction2D_Gaussian:  " << std::endl;
}



} //namespace SANS

#endif  // FORCINGFUNCTION2D_H
