// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// 1D viscous flux

#include <string>
#include <iostream>

#include "ViscousFlux1D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// diffusion matrix: uniform

void
DiffusionMatrix1D_Uniform::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "DiffusionMatrix1D_Uniform:"
            << "  kxx_ = " << kxx_ << std::endl;
}

} //namespace SANS
