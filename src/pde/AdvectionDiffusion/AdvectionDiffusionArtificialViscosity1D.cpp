// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define AVSENSOR1D_INSTANTIATE

#include "TraitsAdvectionDiffusionArtificialViscosity.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux1D_impl.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux1D_impl.h"
#include "pde/ArtificialViscosity/AVSensor_Source1D_impl.h"

namespace SANS
{

//Instantiations for the Artificial Viscosity PDE with AD

template class AVSensor_AdvectiveFlux1D_Uniform<TraitsSizeADArtificialViscosity>;

template class AVSensor_DiffusionMatrix1D_Uniform<TraitsSizeADArtificialViscosity>;
template class AVSensor_DiffusionMatrix1D_GridDependent<TraitsSizeADArtificialViscosity>;
template class AVSensor_DiffusionMatrix1D_GenHScale<TraitsSizeADArtificialViscosity>;

template class AVSensor_Source1D_Uniform<TraitsSizeADArtificialViscosity>;

}
