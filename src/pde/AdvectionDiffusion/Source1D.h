// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOURCE1D_H
#define SOURCE1D_H

// 1-D Advection-Diffusion PDE: source term

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "Surreal/PromoteSurreal.h"
#include "AdvectionDiffusion_Traits.h"

#include <iostream>
#include <cmath>

namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: source
//
// member functions:
//   .source                    source term
//   .sourceTrace      dual-consistent source term
//----------------------------------------------------------------------------//


//----------------------------------------------------------------------------//
// Source: None
//
// No source term
//----------------------------------------------------------------------------//

class Source1D_None
{
public:
  template<class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD1>::ArrayQ<T>;

  template<class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD1>::MatrixQ<T>;

  bool hasSourceTerm() const { return false; }
  bool hasSourceTrace() const { return false; }
  bool needsSolutionGradientforSource() const { return false; }

  template<class Tq, class Tg, class Ts>
  void source( const Real& x, const Real& time,
               const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
               ArrayQ<Ts>& source ) const  {}

  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& xL, const Real xR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const {}

  template <class Tq, class Tg, class Ts>
  void jacobianSource(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Ts>& dsdu ) const {}

  template <class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Ts>& dsdu ) const  {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSource(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Ts>& dsdux ) const {}

  // jacobian of source wrt gradient: d(S)/d(Ux)
  template <class Tq, class Tg, class Th, class Ts>
  void jacobianGradientSourceGradient(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Th>& qxx,
      MatrixQ<Ts>& dsdux_x ) const {}

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const { return true; }

//  void dump( int indentSize=0, std::ostream& out = std::cout ) const = 0;

};

//----------------------------------------------------------------------------//
// Source: Uniform
//
// source = a_*q where a_ is a constant
//----------------------------------------------------------------------------//

class Source1D_Uniform
{
public:
  template<class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD1>::ArrayQ<T>;

  template<class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD1>::MatrixQ<T>;

  explicit Source1D_Uniform( const Real a) : a_(a) {}

  bool hasSourceTerm() const { return true; }
  bool hasSourceTrace() const { return false; }
  bool needsSolutionGradientforSource() const { return false; }

  template<class Tq, class Tg, class Ts>
  void source( const Real& x, const Real& time,
               const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
               ArrayQ<Ts>& source ) const
  {
    source += a_*q;
  }

  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& xL, const Real xR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
  {
    sourceL += a_*qL;
    sourceR += a_*qR;
  }

  template <class Tq, class Tg, class Ts>
  void jacobianSource(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Ts>& dsdu ) const
  {
    dsdu += a_;
  }

  template <class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Ts>& dsdu ) const
  {
    dsdu += fabs(a_);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSource(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Ts>& dsdux ) const {}

  // jacobian of source wrt gradient: d(S)/d(Ux)
  template <class Tq, class Tg, class Th, class Ts>
  void jacobianGradientSourceGradient(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Th>& qxx,
      MatrixQ<Ts>& dsdux_x ) const {}

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const { return true; }

protected:
  const Real a_;
};


//----------------------------------------------------------------------------//
// Source: Uniform with Gradient
//
// source = a*q + b*qx where a and b are constants
//----------------------------------------------------------------------------//

class Source1D_UniformGrad
{
public:
  template<class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD1>::ArrayQ<T>;

  template<class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD1>::MatrixQ<T>;

  explicit Source1D_UniformGrad( const Real a, const Real b ) : a_(a), b_(b) {}

  bool hasSourceTerm() const { return true; }
  bool hasSourceTrace() const { return false; }
  bool needsSolutionGradientforSource() const { return true; }

  template<class Tq, class Tg, class Ts>
  void source( const Real& x, const Real& time,
               const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
               ArrayQ<Ts>& source ) const
  {
    source += a_*q + b_*qx;
  }

  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& xL, const Real xR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
  {
    sourceL += a_*qL + b_*qxL;
    sourceR += a_*qR + b_*qxR;
  }

  template <class Tq, class Tg, class Ts>
  void jacobianSource(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Ts>& dsdu ) const
  {
    dsdu += a_;
  }

  template <class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Ts>& dsdu ) const
  {
    dsdu += fabs(a_);
  }
//  void dump( int indentSize=0, std::ostream& out = std::cout ) const = 0;

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSource(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Ts>& dsdux ) const
  {
    dsdux += b_;
  }


  // jacobian of source wrt gradient: d(S)/d(Ux)
  template <class Tq, class Tg, class Th, class Ts>
  void jacobianGradientSourceGradient(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Th>& qxx,
      MatrixQ<Ts>& dsdux_x ) const
  { }

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const { return true; }

protected:
  const Real a_;
  const Real b_;
};


//----------------------------------------------------------------------------//
// Source: Uniform with Gradient
//
// source = a*q + b*qx where a and b are constants
//----------------------------------------------------------------------------//

class Source1D_Oliver
{
public:
  template<class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD1>::ArrayQ<T>;

  template<class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD1>::MatrixQ<T>;

  explicit Source1D_Oliver( const Real c ) : c_(c) {}

  bool hasSourceTerm() const { return true; }
  bool hasSourceTrace() const { return false; }
  bool needsSolutionGradientforSource() const { return true; }

  template<class Tq, class Tg, class Ts>
  void source( const Real& x, const Real& time,
               const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
               ArrayQ<Ts>& source ) const
  {
    source -= c_*qx*qx + c_*q*q/(x*x);
  }

  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& xL, const Real xR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
  {
    sourceL += c_*qxL*qxL;
    sourceR += c_*qxR*qxR;
  }

  // jacobian of source wrt u: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianSource(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Ts>& dsdu ) const
  {
    dsdu -= 2*c_*q/(x*x);
  }

  template <class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Ts>& dsdu ) const
  {
    dsdu += fabs(2*c_*q/(x*x));
  }
//  void dump( int indentSize=0, std::ostream& out = std::cout ) const = 0;

  // jacobian of source wrt gradient: d(S)/d(Ux)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSource(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Ts>& dsdux ) const
  {
    dsdux -= 2*c_*qx;
  }


  // jacobian of source wrt gradient: d(S)/d(Ux)
  template <class Tq, class Tg, class Th, class Ts>
  void jacobianGradientSourceGradient(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Th>& qxx,
      MatrixQ<Ts>& dsdux_x ) const
  {
    dsdux_x -= 2*c_*qxx;
  }

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const { return true; }

protected:
  const Real c_;
};

} //namespace SANS

#endif  // SOURCE1D_H
