// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCAdvectionDiffusion3D.h"

#include "AdvectiveFlux3D.h"
#include "ViscousFlux3D.h"
#include "Source3D.h"
#include "ForcingFunction3D.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{

template ParameterOption<BCAdvectionDiffusionParams<PhysD3, BCTypeFunction_mitStateParam>::TypeOptions>::ExtractType
PyDict::get(ParameterType<ParameterOption<BCAdvectionDiffusionParams<PhysD3, BCTypeFunction_mitStateParam>::TypeOptions> > const&) const;


//===========================================================================//
//
// Parameter classes
//
// cppcheck-suppress passedByValue
void BCAdvectionDiffusionParams<PhysD3, BCTypeTimeOutParam>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  d.checkUnknownInputs(allParams);
}
BCAdvectionDiffusionParams<PhysD3, BCTypeTimeOutParam> BCAdvectionDiffusionParams<PhysD3, BCTypeTimeOutParam>::params;

// cppcheck-suppress passedByValue
void BCAdvectionDiffusionParams<PhysD3, BCTypeTimeICParam>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.qB));
  d.checkUnknownInputs(allParams);
}
BCAdvectionDiffusionParams<PhysD3, BCTypeTimeICParam> BCAdvectionDiffusionParams<PhysD3, BCTypeTimeICParam>::params;

// cppcheck-suppress passedByValue
void BCAdvectionDiffusionParams<PhysD3, BCTypeTimeIC_FunctionParam>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Function));
  d.checkUnknownInputs(allParams);
}
BCAdvectionDiffusionParams<PhysD3, BCTypeTimeIC_FunctionParam> BCAdvectionDiffusionParams<PhysD3, BCTypeTimeIC_FunctionParam>::params;

// cppcheck-suppress passedByValue
void BCAdvectionDiffusionParams<PhysD3, BCTypeNatural>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.gB));
  d.checkUnknownInputs(allParams);
}
BCAdvectionDiffusionParams<PhysD3, BCTypeNatural> BCAdvectionDiffusionParams<PhysD3, BCTypeNatural>::params;

// cppcheck-suppress passedByValue
void BCAdvectionDiffusionParams<PhysD3, BCTypeDirichlet_mitStateParam>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.qB));
  d.checkUnknownInputs(allParams);
}
BCAdvectionDiffusionParams<PhysD3, BCTypeDirichlet_mitStateParam> BCAdvectionDiffusionParams<PhysD3, BCTypeDirichlet_mitStateParam>::params;

// cppcheck-suppress passedByValue
void BCAdvectionDiffusionParams<PhysD3, BCTypeFunction_mitStateParam>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Function));
  allParams.push_back(d.checkInputs(params.SolutionBCType));
  allParams.push_back(d.checkInputs(params.Upwind));
  d.checkUnknownInputs(allParams);
}
BCAdvectionDiffusionParams<PhysD3, BCTypeFunction_mitStateParam> BCAdvectionDiffusionParams<PhysD3, BCTypeFunction_mitStateParam>::params;

// cppcheck-suppress passedByValue
void BCAdvectionDiffusionParams<PhysD3, BCTypeFluxParams>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.bcdata));
  allParams.push_back(d.checkInputs(params.Inflow));
  d.checkUnknownInputs(allParams);
}
BCAdvectionDiffusionParams<PhysD3, BCTypeFluxParams> BCAdvectionDiffusionParams<PhysD3, BCTypeFluxParams>::params;

// cppcheck-suppress passedByValue
void BCAdvectionDiffusionParams<PhysD3, BCTypeLinearRobin_mitLG>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.A));
  allParams.push_back(d.checkInputs(params.B));
  allParams.push_back(d.checkInputs(params.bcdata));
  d.checkUnknownInputs(allParams);
}
BCAdvectionDiffusionParams<PhysD3, BCTypeLinearRobin_mitLG> BCAdvectionDiffusionParams<PhysD3, BCTypeLinearRobin_mitLG>::params;

// cppcheck-suppress passedByValue
void BCAdvectionDiffusionParams<PhysD3, BCTypeLinearRobin_sansLG>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.A));
  allParams.push_back(d.checkInputs(params.B));
  allParams.push_back(d.checkInputs(params.bcdata));
  d.checkUnknownInputs(allParams);
}
BCAdvectionDiffusionParams<PhysD3, BCTypeLinearRobin_sansLG> BCAdvectionDiffusionParams<PhysD3, BCTypeLinearRobin_sansLG>::params;

// cppcheck-suppress passedByValue
void BCAdvectionDiffusionParams<PhysD3, BCTypeFunctionLinearRobin_mitLG>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Function));
  allParams.push_back(d.checkInputs(params.A));
  allParams.push_back(d.checkInputs(params.B));
  d.checkUnknownInputs(allParams);
}
BCAdvectionDiffusionParams<PhysD3, BCTypeFunctionLinearRobin_mitLG> BCAdvectionDiffusionParams<PhysD3, BCTypeFunctionLinearRobin_mitLG>::params;

// cppcheck-suppress passedByValue
void BCAdvectionDiffusionParams<PhysD3, BCTypeFunctionLinearRobin_sansLG>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Function));
  allParams.push_back(d.checkInputs(params.A));
  allParams.push_back(d.checkInputs(params.B));
  d.checkUnknownInputs(allParams);
}
BCAdvectionDiffusionParams<PhysD3, BCTypeFunctionLinearRobin_sansLG> BCAdvectionDiffusionParams<PhysD3, BCTypeFunctionLinearRobin_sansLG>::params;


//===========================================================================//
// Instantiate the BC parameters
typedef BCAdvectionDiffusion3DVector< AdvectiveFlux3D_None   , ViscousFlux3D_Uniform> BCVector_ANDU;
typedef BCAdvectionDiffusion3DVector< AdvectiveFlux3D_Uniform, ViscousFlux3D_Uniform> BCVector_AUDU;
typedef BCAdvectionDiffusion3DVector< AdvectiveFlux3D_Radial, ViscousFlux3D_Uniform>  BCVector_AUDU_R;
typedef BCAdvectionDiffusion3DVector< AdvectiveFlux3D_Uniform, ViscousFlux3D_None   > BCVector_AUDN;
typedef BCAdvectionDiffusion3DVector< AdvectiveFlux3D_None   , ViscousFlux3D_Poly   > BCVector_ANDP;
typedef BCAdvectionDiffusion3DVector< AdvectiveFlux3D_PolyZ  , ViscousFlux3D_Poly   > BCVector_APDP;

BCPARAMETER_INSTANTIATE(BCVector_ANDU)
BCPARAMETER_INSTANTIATE(BCVector_AUDU)
BCPARAMETER_INSTANTIATE(BCVector_AUDU_R)
BCPARAMETER_INSTANTIATE(BCVector_AUDN)
BCPARAMETER_INSTANTIATE(BCVector_ANDP)
BCPARAMETER_INSTANTIATE(BCVector_APDP)

// 3D channel flow
typedef BCAdvectionDiffusion3DVector< AdvectiveFlux3D_SquareChannel, ViscousFlux3D_Uniform> BCVector_ACDU;
BCPARAMETER_INSTANTIATE(BCVector_ACDU)

//===========================================================================//
void
BCAdvectionDiffusion_LinearRobinBase<PhysD3>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCAdvectionDiffusion<PhysD3, BCTypeLinearRobin>:" << std::endl;
  out << indent << "  A_ = " << A_ << std::endl;
  out << indent << "  B_ = " << B_ << std::endl;
  out << indent << "  bcdata_ = " << bcdata_ << std::endl;
}

#if 0
void
BCAdvectionDiffusion<PhysD3, BCTypeFlux>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCAdvectionDiffusion<PhysD3, BCTypeFlux>:" << std::endl;
  out << indent << "  pde_:" << std::endl;
  pde_.dump( indentSize+2, out );
  out << indent << "  bcdata_ = " << bcdata_ << std::endl;
}
#endif
} //namespace SANS
