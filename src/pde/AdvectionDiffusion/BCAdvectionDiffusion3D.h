// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCADVECTIONDIFFUSION3D_H
#define BCADVECTIONDIFFUSION3D_H

// 3-D Advection-Diffusion BC class

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/BCCategory.h"
#include "pde/BCNone.h"
#include "Topology/Dimension.h"
#include "AdvectionDiffusion_Traits.h"
#include "pde/AnalyticFunction/ScalarFunction3D.h"
#include "ViscousFlux3D.h"

#include <iostream>

#include <boost/mpl/vector.hpp>

namespace SANS
{

//----------------------------------------------------------------------------//
// BC class: 3-D Advection-Diffusion
//
// template parameters:
//   T                    solution DOF data type (e.g. double)
//   BCType               BC type (e.g. BCTypeLinearRobin)
//----------------------------------------------------------------------------//

template <class PhysDim, class BCType>
class BCAdvectionDiffusion;

template <class PhysDim, class BCType>
struct BCAdvectionDiffusionParams;

//----------------------------------------------------------------------------//
// BC types
//----------------------------------------------------------------------------//

template<class AdvectiveFlux, class ViscousFlux>
class BCTypeTimeOut;
template<class AdvectiveFlux, class ViscousFlux>
class BCTypeTimeIC;
template<class AdvectiveFlux, class ViscousFlux>
class BCTypeTimeIC_Function;
class BCTypeNatural;
class BCTypeLinearRobin_mitLG;
class BCTypeLinearRobin_sansLG;

template<class AdvectiveFlux, class ViscousFlux>
class BCTypeDirichlet_mitState;

template<class AdvectiveFlux, class ViscousFlux>
class BCTypeFlux;
class BCTypeFluxParams;

template<class AdvectiveFlux, class ViscousFlux>
class BCTypeFunction_mitState;
class BCTypeFunctionLinearRobin_mitLG;
class BCTypeFunctionLinearRobin_sansLG;
class BCTypeUniformNeumann;


// Define the vector of boundary conditions
template<class AdvectiveFlux, class ViscousFlux>
using BCAdvectionDiffusion3DVector = boost::mpl::vector12< BCNone<PhysD3,AdvectionDiffusionTraits<PhysD3>::N>,
    SpaceTimeBC<BCAdvectionDiffusion<PhysD3, BCTypeTimeOut<AdvectiveFlux, ViscousFlux>>>,
    SpaceTimeBC<BCAdvectionDiffusion<PhysD3, BCTypeTimeIC<AdvectiveFlux, ViscousFlux>>>,
    SpaceTimeBC<BCAdvectionDiffusion<PhysD3, BCTypeTimeIC_Function<AdvectiveFlux, ViscousFlux>>>,
    BCAdvectionDiffusion<PhysD3, BCTypeNatural>,
    BCAdvectionDiffusion<PhysD3, BCTypeFlux<AdvectiveFlux,ViscousFlux>>,
    BCAdvectionDiffusion<PhysD3, BCTypeLinearRobin_mitLG>,
    BCAdvectionDiffusion<PhysD3, BCTypeLinearRobin_sansLG>,
    BCAdvectionDiffusion<PhysD3, BCTypeDirichlet_mitState<AdvectiveFlux,ViscousFlux>>,
    BCAdvectionDiffusion<PhysD3, BCTypeFunction_mitState<AdvectiveFlux,ViscousFlux>>,
    BCAdvectionDiffusion<PhysD3, BCTypeFunctionLinearRobin_mitLG>,
    BCAdvectionDiffusion<PhysD3, BCTypeFunctionLinearRobin_sansLG>
  >;

// time out boundary condition
class BCTypeTimeOutParam;

template <>
struct BCAdvectionDiffusionParams<PhysD3, BCTypeTimeOutParam> : noncopyable
{
  static constexpr const char * BCName{"TimeOut"};
  struct Option
  {
    const DictOption TimeOut{BCAdvectionDiffusionParams::BCName, BCAdvectionDiffusionParams::checkInputs};
  };

  static void checkInputs(PyDict d);
  static BCAdvectionDiffusionParams params;
};

template<class AdvectiveFlux, class ViscousFlux>
class BCAdvectionDiffusion<PhysD3, BCTypeTimeOut<AdvectiveFlux, ViscousFlux>> :
public BCType<BCAdvectionDiffusion<PhysD3, BCTypeTimeOut<AdvectiveFlux, ViscousFlux>>>
{
public:

  typedef PhysD3 PhysDim;
  typedef BCTypeTimeOut<AdvectiveFlux, ViscousFlux> BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCAdvectionDiffusionParams<PhysD3, BCTypeTimeOutParam> ParamsType;

  static const int D= AdvectionDiffusionTraits<PhysD3>::D;        // physical dimensions
  static const int N= AdvectionDiffusionTraits<PhysD3>::N;        // total solution variables

  static const int NBC= 0;        // total BCs

  template <class T>
  using ArrayQ= AdvectionDiffusionTraits<PhysD3>::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using MatrixQ= AdvectionDiffusionTraits<PhysD3>::template MatrixQ<T>;   // matrices

  template <class Source>
  BCAdvectionDiffusion(const PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source> &pde, const PyDict &d) :
  adv_(pde.getAdvectiveFlux()), visc_(pde.getViscousFlux())
  {}
  BCAdvectionDiffusion() {}

  virtual ~BCAdvectionDiffusion() {}

  BCAdvectionDiffusion(const BCAdvectionDiffusion &)= delete;
  BCAdvectionDiffusion &operator= (const BCAdvectionDiffusion &)= delete;

  // is there a viscous flux on the boundary
  bool hasFluxViscous() const
  {
    return false;
  }

  // BC data
  template <class T>
  void state(const Real &x, const Real &y, const Real &z, const Real &time,
             const Real &nx, const Real &ny, const Real &nz, const Real &nt,
             const ArrayQ<T> &qI, ArrayQ<T> &qB) const
  {
    qB= qI;     // sets boundary state to interior state (outflow)
  }

  // BC data
  template <class Tp, class T>
  void state(const Tp &param, const Real &x, const Real &y, const Real &z, const Real &time,
             const Real &nx, const Real &ny, const Real &nz, const Real &nt,
             const ArrayQ<T> &qI, ArrayQ<T> &qB) const
  {
    state(x, y, z, time, nx, ny, nz, nt, qI, qB);
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormalSpaceTime(const Real &x, const Real &y, const Real &z, const Real &time,
                           const Real &nx, const Real &ny, const Real &nz, const Real &nt,
                           const ArrayQ<T> &qI, const ArrayQ<T> &qIx, const ArrayQ<T> &qIy, const ArrayQ<T> &qIz,
                           const ArrayQ<T> &qIt, const ArrayQ<T> &qB, ArrayQ<Tf> &Fn) const
  {
    adv_.fluxUpwindSpaceTime(x, y, z, time, qI, qB, nx, ny, nz, nt, Fn);
  }

  // normal BC flux
  template <class T, class Tp, class Tf>
  void fluxNormalSpaceTime(const Tp &param, const Real &x, const Real &y, const Real &z, const Real &time,
                           const Real &nx, const Real &ny, const Real &nz, const Real &nt,
                           const ArrayQ<T> &qI, const ArrayQ<T> &qIx, const ArrayQ<T> &qIy, const ArrayQ<T> &qIz,
                           const ArrayQ<T> &qIt, const ArrayQ<T> &qB, ArrayQ<Tf> &Fn) const
  {
    adv_.fluxUpwindSpaceTime(x, y, z, time, qI, qB, nx, ny, nz, nt, Fn);
  }

  bool isValidState(const Real &nx, const Real &ny, const Real &nz, const Real &nt, const ArrayQ<Real> &qI) const
  {
    return true;
  }

  void dump(int indentSize, std::ostream &out= std::cout) const;

protected:
  const AdvectiveFlux& adv_;
  const ViscousFlux& visc_;
};

// time IC boundary condition
class BCTypeTimeICParam;

template <>
struct BCAdvectionDiffusionParams<PhysD3, BCTypeTimeICParam> : noncopyable
{
  const ParameterNumeric<Real> qB{"qB", NO_DEFAULT, NO_RANGE, "BC state"};

  static constexpr const char * BCName{"TimeIC"};
  struct Option
  {
    const DictOption TimeIC{BCAdvectionDiffusionParams::BCName, BCAdvectionDiffusionParams::checkInputs};
  };

  static void checkInputs(PyDict d);
  static BCAdvectionDiffusionParams params;
};

template <class AdvectiveFlux, class ViscousFlux>
class BCAdvectionDiffusion<PhysD3, BCTypeTimeIC<AdvectiveFlux, ViscousFlux>> :
public BCType<BCAdvectionDiffusion<PhysD3, BCTypeTimeIC<AdvectiveFlux, ViscousFlux>>>
{
public:

  typedef PhysD3 PhysDim;
  typedef BCTypeTimeIC<AdvectiveFlux, ViscousFlux> BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCAdvectionDiffusionParams<PhysD3, BCTypeTimeICParam> ParamsType;

  static const int D= AdvectionDiffusionTraits<PhysD3>::D;      // physical dimensions
  static const int N= AdvectionDiffusionTraits<PhysD3>::N;      // total solution variables

  static const int NBC= 1;        // total BCs

  template <class T>
  using ArrayQ= AdvectionDiffusionTraits<PhysD3>::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using MatrixQ= AdvectionDiffusionTraits<PhysD3>::template MatrixQ<T>;   // matrices

  template <class Source>
  BCAdvectionDiffusion(const PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source> &pde, const Real &data) :
  adv_(pde.getAdvectiveFlux()),
  visc_(pde.getViscousFlux()),
  qB_(data)
  {}

  template <class Source>
  BCAdvectionDiffusion(const PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source> &pde, const PyDict &d) :
  adv_(pde.getAdvectiveFlux()),
  visc_(pde.getViscousFlux()),
  qB_(d.get(ParamsType::params.qB))
  {}

  virtual ~BCAdvectionDiffusion() {}

  BCAdvectionDiffusion(const BCAdvectionDiffusion &)= delete;
  BCAdvectionDiffusion &operator= (const BCAdvectionDiffusion &)= delete;

  // is there a viscous flux on the boundary?
  bool hasFluxViscous() const
  {
    return false;
  }

  // BC data
  template <class T>
  void state(const Real &x, const Real &y, const Real &z, const Real &time,
             const Real &nx, const Real &ny, const Real &nz, const Real &nt,
             const ArrayQ<T> &qI, ArrayQ<T> &qB) const
  {
    qB= qB_;
  }

  // BC data
  template <class Tp, class T>
  void state(const Tp &param, const Real &x, const Real &y, const Real &z, const Real &time,
             const Real &nx, const Real &ny, const Real &nz, const Real &nt,
             const ArrayQ<T> &qI, ArrayQ<T> &qB) const
  {
    state(x, y, z, time, nx, ny, nz, nt, qI, qB);
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormalSpaceTime(const Real &x, const Real &y, const Real &z, const Real &time,
                           const Real &nx, const Real &ny, const Real &nz, const Real &nt,
                           const ArrayQ<T> &qI, const ArrayQ<T> &qIx, const ArrayQ<T> &qIy, const ArrayQ<T> &qIz,
                           const ArrayQ<T> &qIt, const ArrayQ<T> &qB, ArrayQ<Tf> &Fn) const
  {
    adv_.fluxUpwindSpaceTime(x, y, z, time, qI, qB, nx, ny, nz, nt, Fn);
  }

  // normal BC flux
  template <class T, class Tp, class Tf>
  void fluxNormalSpaceTime(const Tp &param, const Real &x, const Real &y, const Real &z, const Real &time,
                           const Real &nx, const Real &ny, const Real &nz, const Real &nt,
                           const ArrayQ<T> &qI, const ArrayQ<T> &qIx, const ArrayQ<T> &qIy, const ArrayQ<T> &qIz,
                           const ArrayQ<T> &qIt, const ArrayQ<T> &qB, ArrayQ<Tf> &Fn) const
  {
    adv_.fluxUpwindSpaceTime(x, y, z, time, qI, qB, nx, ny, nz, nt, Fn);
  }

  bool isValidState(const Real &nx, const Real &ny, const Real &nz, const Real &nt, const ArrayQ<Real> &qI) const
  {
    return true;
  }

  void dump(int indentSize, std::ostream &out= std::cout) const;

protected:
  const AdvectiveFlux &adv_;
  const ViscousFlux &visc_;
  const ArrayQ<Real> qB_;
};

// time in boundary condition with function!
class BCTypeTimeIC_FunctionParam;

template <>
struct BCAdvectionDiffusionParams<PhysD3, BCTypeTimeIC_FunctionParam> : ScalarFunction3DParams
{
  static constexpr const char *BCName{"TimeIC_Function"};
  struct Option
  {
    const DictOption TimeIC_Function{BCAdvectionDiffusionParams::BCName, BCAdvectionDiffusionParams::checkInputs};
  };

  static void checkInputs(PyDict d);
  static BCAdvectionDiffusionParams params;
};

template <class AdvectiveFlux, class ViscousFlux>
class BCAdvectionDiffusion<PhysD3, BCTypeTimeIC_Function<AdvectiveFlux, ViscousFlux>> :
public BCType<BCAdvectionDiffusion<PhysD3, BCTypeTimeIC_Function<AdvectiveFlux, ViscousFlux>>>
{
public:

  typedef PhysD3 PhysDim;
  typedef BCTypeTimeIC_Function<AdvectiveFlux, ViscousFlux> BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCAdvectionDiffusionParams<PhysD3, BCTypeTimeIC_FunctionParam> ParamsType;

  static const int D= AdvectionDiffusionTraits<PhysD3>::D;     // physical dimensions
  static const int N= AdvectionDiffusionTraits<PhysD3>::N;     // total solution variables

  static const int NBC= 1;        // total BCs

  template <class T>
  using ArrayQ= AdvectionDiffusionTraits<PhysD3>::template ArrayQ<T>;      // solution arrays

  template <class T>
  using MatrixQ= AdvectionDiffusionTraits<PhysD3>::template MatrixQ<T>;    // matrices

  typedef std::shared_ptr<Function3DBase<ArrayQ<Real>>> Function_ptr;

  template <class Source>
  BCAdvectionDiffusion(const PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source> &pde, const PyDict &d) :
  uexact_(ParamsType::getFunction(d)),
  adv_(pde.getAdvectiveFlux()),
  visc_(pde.getViscousFlux()) {}

  template <class Source>
  BCAdvectionDiffusion(const PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source> &pde, const Function_ptr &uexact) :
  uexact_(uexact),
  adv_(pde.getAdvectiveFlux()),
  visc_(pde.getViscousFlux()) {}

  virtual ~BCAdvectionDiffusion() {}

  BCAdvectionDiffusion(const BCAdvectionDiffusion &)= delete;
  BCAdvectionDiffusion &operator= (const BCAdvectionDiffusion &)= delete;

  // is there a viscous flux on the boundary
  bool hasFluxViscous() const
  {
    return false;
  }

  // BC data
  template <class T>
  void state(const Real &x, const Real &y, const Real &z, const Real &time,
             const Real &nx, const Real &ny, const Real &nz, const Real &nt,
             const ArrayQ<T> &qI, ArrayQ<T> &qB) const
  {
    qB= (*uexact_)(x, y, z, time);
  }

  // BC data
  template <class T, class Tp>
  void state(const Tp& param, const Real &x, const Real &y, const Real &z, const Real &time,
             const Real &nx, const Real &ny, const Real &nz, const Real &nt,
             const ArrayQ<T> &qI, ArrayQ<T> &qB) const
  {
    state(x, y, z, time, nx, ny, nz, nt, qI, qB);
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormalSpaceTime(const Real &x, const Real &y, const Real &z, const Real &time,
                           const Real &nx, const Real &ny, const Real &nz, const Real &nt,
                           const ArrayQ<T> &qI,
                           const ArrayQ<T> &qIx, const ArrayQ<T> &qIy, const ArrayQ<T> &qIz, const ArrayQ<T> &qIt,
                           const ArrayQ<T> &qB,
                           ArrayQ<Tf> &Fn) const
  {
    adv_.fluxUpwindSpaceTime(x, y, z, time, qI, qB, nx, ny, nz, nt, Fn);
  }

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormalSpaceTime(const Tp &param, const Real &x, const Real &y, const Real &z, const Real &time,
                           const Real &nx, const Real &ny, const Real &nz, const Real &nt,
                           const ArrayQ<T> &qI,
                           const ArrayQ<T> &qIx, const ArrayQ<T> &qIy, const ArrayQ<T> &qIz, const ArrayQ<T> &qIt,
                           const ArrayQ<T> &qB,
                           ArrayQ<Tf> &Fn) const
  {
    adv_.fluxUpwindSpaceTime(x, y, z, time, qI, qB, nx, ny, nz, nt, Fn);
  }

  // is the boundary state valid
  bool isValidState(const Real &nx, const Real &ny, const Real &nz, const Real &nt, const ArrayQ<Real> &q) const
  {
    return true;
  }

  void dump(int indentSize, std::ostream &out= std::cout);

protected:
  const Function_ptr uexact_;
  const AdvectiveFlux &adv_;
  const ViscousFlux &visc_;
};

//----------------------------------------------------------------------------//
// Natural Bc < w, Flux. n> = <w,gB>
//
template<>
struct BCAdvectionDiffusionParams<PhysD3, BCTypeNatural> : noncopyable
{
  const ParameterNumeric<Real> gB{"gB", NO_DEFAULT, NO_RANGE, "Boundary flux data"};

  static constexpr const char* BCName{"Natural"};
  struct Option
  {
    const DictOption Natural{BCAdvectionDiffusionParams::BCName, BCAdvectionDiffusionParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCAdvectionDiffusionParams params;
};

template <>
class BCAdvectionDiffusion<PhysD3, BCTypeNatural> :
  public BCType< BCAdvectionDiffusion<PhysD3, BCTypeNatural> >
{
public:
  typedef BCCategory::Flux_mitState Category;
  typedef BCAdvectionDiffusionParams<PhysD3, BCTypeNatural> ParamsType;

  typedef PhysD3 PhysDim;
  static const int D = AdvectionDiffusionTraits<PhysD3>::D;   // physical dimensions
  static const int N = AdvectionDiffusionTraits<PhysD3>::N;   // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD3>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD3>::template MatrixQ<T>; // matrices

  template <class PDE>
  BCAdvectionDiffusion( const PDE& pde, const PyDict& d ) :
    gB_(d.get(ParamsType::params.gB)),
    fluxViscous_(pde.hasFluxViscous()) {}

  template <class PDE>
  BCAdvectionDiffusion( const PDE& pde, const ArrayQ<Real>& gB ) :
    gB_(gB),
    fluxViscous_(pde.hasFluxViscous()) {}

  virtual ~BCAdvectionDiffusion() {}

  BCAdvectionDiffusion& operator=( const BCAdvectionDiffusion& );

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return fluxViscous_; }

  // BC data
  template <class T>
  void state( const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // normal BC flux
  template <class T>
  void fluxNormal( const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const Real& nz, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  ArrayQ<Real> gB_;
  bool fluxViscous_;
};

template <class T>
inline void
BCAdvectionDiffusion<PhysD3, BCTypeNatural>::
state( const Real& x, const Real& y, const Real& z, const Real& time,
       const Real& nx, const Real& ny, const Real& nz,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  qB = qI;
}

template <class T>
inline void
BCAdvectionDiffusion<PhysD3, BCTypeNatural>::
fluxNormal( const Real& x, const Real& y, const Real& z, const Real& time,
            const Real& nx, const Real& ny, const Real& nz,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
            const ArrayQ<T>& qB,
            ArrayQ<T>& Fn) const
{
  // set boundary flux
  Fn = gB_;
}


//----------------------------------------------------------------------------//
// Characteristic Dirichlet BC:  u = bcstate
//
class BCTypeDirichlet_mitStateParam;

template<>
struct BCAdvectionDiffusionParams<PhysD3, BCTypeDirichlet_mitStateParam> : noncopyable
{
  const ParameterNumeric<Real> qB{"qB", NO_DEFAULT, NO_RANGE, "BC state"};

  static constexpr const char* BCName{"Dirichlet_mitState"};
  struct Option
  {
    const DictOption Dirichlet_mitState{BCAdvectionDiffusionParams::BCName, BCAdvectionDiffusionParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCAdvectionDiffusionParams params;
};

template <class AdvectiveFlux, class ViscousFlux>
class BCAdvectionDiffusion<PhysD3, BCTypeDirichlet_mitState<AdvectiveFlux,ViscousFlux>> :
  public BCType< BCAdvectionDiffusion<PhysD3, BCTypeDirichlet_mitState<AdvectiveFlux,ViscousFlux>> >
{
public:
  typedef BCCategory::Flux_mitState Category;
  typedef BCAdvectionDiffusionParams<PhysD3, BCTypeDirichlet_mitStateParam> ParamsType;

  typedef PhysD3 PhysDim;
  static const int D = AdvectionDiffusionTraits<PhysD3>::D;   // physical dimensions
  static const int N = AdvectionDiffusionTraits<PhysD3>::N;   // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD3>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD3>::template MatrixQ<T>; // matrices

  template<class Source>
  BCAdvectionDiffusion(const PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>& pde, const PyDict& d ) :
    qB_(d.get(ParamsType::params.qB)),
    adv_( pde.getAdvectiveFlux() ),
    visc_( pde.getViscousFlux() ),
    fluxViscous_(pde.hasFluxViscous()) {}

  template<class Source>
  BCAdvectionDiffusion(const PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>& pde, const ArrayQ<Real>& qB ) :
    qB_(qB),
    adv_( pde.getAdvectiveFlux() ),
    visc_( pde.getViscousFlux() ),
    fluxViscous_(pde.hasFluxViscous()) {}

  virtual ~BCAdvectionDiffusion() {}

  BCAdvectionDiffusion& operator=( const BCAdvectionDiffusion& );

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return fluxViscous_; }

  // BC data
  template <class T>
  void state( const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // normal BC flux
  template <class T>
  void fluxNormal( const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const Real& nz, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  ArrayQ<Real> qB_;
  const AdvectiveFlux& adv_;
  const ViscousFlux& visc_;
  bool fluxViscous_;
};


template <class AdvectiveFlux, class ViscousFlux>
template <class T>
inline void
BCAdvectionDiffusion<PhysD3, BCTypeDirichlet_mitState<AdvectiveFlux,ViscousFlux>>::
state( const Real& x, const Real& y, const Real& z, const Real& time,
       const Real& nx, const Real& ny, const Real& nz,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  qB = qB_;
}


template<class AdvectiveFlux, class ViscousFlux>
template <class T>
inline void
BCAdvectionDiffusion<PhysD3, BCTypeDirichlet_mitState<AdvectiveFlux,ViscousFlux>>::
fluxNormal( const Real& x, const Real& y, const Real& z, const Real& time,
            const Real& nx, const Real& ny, const Real& nz,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
            const ArrayQ<T>& qB,
            ArrayQ<T>& Fn) const
{
  // Add upwinded advective flux
  adv_.fluxUpwind(x, y, z, time, qI, qB, nx, ny, nz, Fn);

  SANS_ASSERT(visc_.fluxViscousLinearInGradient());
  ArrayQ<T> kxx = 0, kxy = 0, kxz = 0;
  ArrayQ<T> kyx = 0, kyy = 0, kyz = 0;
  ArrayQ<T> kzx = 0, kzy = 0, kzz = 0;
  visc_.diffusionViscous(x, y, z, time,
                         qB, qIx, qIy, qIz,
                         kxx, kxy, kxz,
                         kyx, kyy, kyz,
                         kzx, kzy, kzz);

  ArrayQ<T> fv = 0, gv = 0, hv = 0;

  // Use gradient from interior
  fv = -kxx*qIx - kxy*qIy - kxz*qIz;
  gv = -kyx*qIx - kyy*qIy - kyz*qIz;
  hv = -kzx*qIx - kzy*qIy - kzz*qIz;

  // Add viscous flux
  Fn += fv*nx + gv*ny + hv*nz;
}

template<class AdvectiveFlux, class ViscousFlux>
void
BCAdvectionDiffusion<PhysD3, BCTypeDirichlet_mitState<AdvectiveFlux,ViscousFlux>>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCAdvectionDiffusion<PhysD3, BCTypeDirichlet_mitState>:" << std::endl;
  out << indent << "  qB_ = " << qB_ << std::endl;
}


//----------------------------------------------------------------------------//
// BCTypeFunction_mitState BC
//
class BCTypeFunction_mitStateParam;

template<>
struct BCAdvectionDiffusionParams<PhysD3, BCTypeFunction_mitStateParam> : ScalarFunction3DParams
{
  struct TypeOptions
  {
    typedef std::string ExtractType;
    const std::string Dirichlet = "Dirichlet";
    const std::string Neumann = "Neumann";
    const std::string Robin = "Robin";

    const std::vector<std::string> options{Dirichlet,Neumann,Robin};
  };
  const ParameterOption<TypeOptions> SolutionBCType{"SolutionBCType", NO_DEFAULT, "BC type to impose the exact solution"};

  const ParameterBool Upwind{"Upwind", true, "Use upwinding for advective flux"};

  static constexpr const char* BCName{"Function_mitState"};
  struct Option
  {
    const DictOption Function_mitState{BCAdvectionDiffusionParams::BCName, BCAdvectionDiffusionParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCAdvectionDiffusionParams params;
};

template<class AdvectiveFlux, class ViscousFlux>
class BCAdvectionDiffusion<PhysD3, BCTypeFunction_mitState<AdvectiveFlux,ViscousFlux>> :
  public BCType< BCAdvectionDiffusion<PhysD3, BCTypeFunction_mitState<AdvectiveFlux,ViscousFlux>> >
{
public:
  typedef BCCategory::Flux_mitState Category;
  typedef BCAdvectionDiffusionParams<PhysD3, BCTypeFunction_mitStateParam> ParamsType;

  typedef PhysD3 PhysDim;
  static const int D = AdvectionDiffusionTraits<PhysD3>::D;   // physical dimensions
  static const int N = AdvectionDiffusionTraits<PhysD3>::N;   // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD3>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD3>::template MatrixQ<T>; // matrices

  typedef std::shared_ptr<Function3DBase<ArrayQ<Real>>> Function_ptr;

  enum BCCategoryType
  {
    Dirichlet, Neumann, Robin
  };

  template<class Source>
  BCAdvectionDiffusion(const PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>& pde, const PyDict& d ) :
    uexact_( ParamsType::getFunction(d) ),
    adv_( pde.getAdvectiveFlux() ),
    visc_( pde.getViscousFlux() ),
    upwind_(d.get(ParamsType::params.Upwind))
  {
    std::string bctype = d.get(ParamsType::params.SolutionBCType);
    if (bctype == ParamsType::params.SolutionBCType.Dirichlet)
      bctype_ = Dirichlet;
    else if (bctype == ParamsType::params.SolutionBCType.Neumann)
      bctype_ = Neumann;
    else if (bctype == ParamsType::params.SolutionBCType.Robin)
      bctype_ = Robin;
    else
      SANS_DEVELOPER_EXCEPTION("Unknown bctype = \"%s\".\n\nExpected \"Dirichlet\", \"Neumann\" or \"Robin\"");
  }

  BCAdvectionDiffusion( const Function_ptr& uexact, const AdvectiveFlux& adv, const ViscousFlux& visc,
                        const std::string& bctype, const bool& upwind ) :
    uexact_(uexact), adv_(adv), visc_(visc), upwind_(upwind)
  {
    if (bctype == ParamsType::params.SolutionBCType.Dirichlet)
      bctype_ = Dirichlet;
    else if (bctype == ParamsType::params.SolutionBCType.Neumann)
      bctype_ = Neumann;
    else if (bctype == ParamsType::params.SolutionBCType.Robin)
      bctype_ = Robin;
    else
      SANS_DEVELOPER_EXCEPTION("Unknown bctype = \"%s\".\n\nExpected \"Dirichlet\", \"Neumann\" or \"Robin\"");
  }

  virtual ~BCAdvectionDiffusion() {}

  BCAdvectionDiffusion& operator=( const BCAdvectionDiffusion& );

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return true; }

  // BC state vector
  template <class T>
  void state( const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // normal BC flux
  template <class T>
  void fluxNormal( const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const Real& nz, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const Function_ptr uexact_;
  const AdvectiveFlux& adv_;
  const ViscousFlux& visc_;
  const bool upwind_;
  BCCategoryType bctype_;
};

template<class AdvectiveFlux, class ViscousFlux>
template <class T>
inline void
BCAdvectionDiffusion<PhysD3, BCTypeFunction_mitState<AdvectiveFlux,ViscousFlux>>::
state( const Real& x, const Real& y, const Real& z, const Real& time,
       const Real& nx, const Real& ny, const Real& nz,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  if (bctype_ == Dirichlet || bctype_ == Robin)
    qB = (*uexact_)(x, y, z, time);
  else if (bctype_ == Neumann )
    qB = qI;
  else
    SANS_DEVELOPER_EXCEPTION("Something is really wrong, bctype_ = %d", bctype_);
}

template<class AdvectiveFlux, class ViscousFlux>
template <class T>
inline void
BCAdvectionDiffusion<PhysD3, BCTypeFunction_mitState<AdvectiveFlux,ViscousFlux>>::
fluxNormal( const Real& x, const Real& y, const Real& z, const Real& time,
            const Real& nx, const Real& ny, const Real& nz,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
            const ArrayQ<T>& qB,
            ArrayQ<T>& Fn) const
{
  if (upwind_)
  {
    Fn = 0;
    // Add upwinded advective flux
    adv_.fluxUpwind(x, y, z, time, qI, qB, nx, ny, nz, Fn);
  }
  else
  {
    ArrayQ<T> fx = 0, fy = 0, fz = 0;
    adv_.flux(x, y, z, time, qB, fx, fy, fz);

    // Add advective flux
    Fn += fx*nx + fy*ny + fz*nz;
  }

  SANS_ASSERT(visc_.fluxViscousLinearInGradient());
  ArrayQ<T> kxx = 0, kxy = 0, kxz = 0;
  ArrayQ<T> kyx = 0, kyy = 0, kyz = 0;
  ArrayQ<T> kzx = 0, kzy = 0, kzz = 0;
  visc_.diffusionViscous(x, y, z, time,
                         qB, qIx, qIy, qIz,
                         kxx, kxy, kxz,
                         kyx, kyy, kyz,
                         kzx, kzy, kzz);

  ArrayQ<T> fv = 0, gv = 0, hv = 0;

  if (bctype_ == Neumann || bctype_ == Robin)
  {
    Real qBtmp, qBx, qBy, qBz, qBt;
    (*uexact_).gradient(x, y, z, time, qBtmp, qBx, qBy, qBz, qBt);

    // Use exact gradient
    fv = kxx*qBx + kxy*qBy + kxz*qBz;
    gv = kyx*qBx + kyy*qBy + kyz*qBz;
    hv = kzx*qBx + kzy*qBy + kzz*qBz;
  }
  else if (bctype_ == Dirichlet)
  {
    // Use gradient from interior
    fv = kxx*qIx + kxy*qIy + kxz*qIz;
    gv = kyx*qIx + kyy*qIy + kyz*qIz;
    hv = kzx*qIx + kzy*qIy + kzz*qIz;
  }
  else
    SANS_DEVELOPER_EXCEPTION("Something is really wrong, bctype_ = %d", bctype_);

  // Add viscous flux
  Fn -= fv*nx + gv*ny + hv*nz;
}

//----------------------------------------------------------------------------//
// Robin BC:  A u + B (kn un + ks us) = bcdata
//
// coefficients A and B are arbitrary with the exception that they cannot
// simultaneously vanish
template<>
struct BCAdvectionDiffusionParams<PhysD3, BCTypeLinearRobin_mitLG> : noncopyable
{
  const ParameterNumeric<Real> A{"A", NO_DEFAULT, NO_RANGE, "Value coefficient"};
  const ParameterNumeric<Real> B{"B", NO_DEFAULT, NO_RANGE, "Viscous coefficient"};
  const ParameterNumeric<Real> bcdata{"bcdata", NO_DEFAULT, NO_RANGE, "BC data"};

  static constexpr const char* BCName{"LinearRobin_mitLG"};
  struct Option
  {
    const DictOption LinearRobin_mitLG{BCAdvectionDiffusionParams::BCName, BCAdvectionDiffusionParams::checkInputs};
  };

  static void checkInputs(PyDict d);
  static BCAdvectionDiffusionParams params;
};

template<>
struct BCAdvectionDiffusionParams<PhysD3, BCTypeLinearRobin_sansLG> : noncopyable
{
  const ParameterNumeric<Real> A{"A", NO_DEFAULT, NO_RANGE, "Value coefficient"};
  const ParameterNumeric<Real> B{"B", NO_DEFAULT, NO_RANGE, "Viscous coefficient"};
  const ParameterNumeric<Real> bcdata{"bcdata", NO_DEFAULT, NO_RANGE, "BC data"};

  static constexpr const char* BCName{"LinearRobin_sansLG"};
  struct Option
  {
    const DictOption LinearRobin_sansLG{BCAdvectionDiffusionParams::BCName, BCAdvectionDiffusionParams::checkInputs};
  };

  static void checkInputs(PyDict d);
  static BCAdvectionDiffusionParams params;
};

template <class PhysDim>
class BCAdvectionDiffusion_LinearRobinBase;

template <>
class BCAdvectionDiffusion_LinearRobinBase<PhysD3>
{
public:
  typedef PhysD3 PhysDim;
  static const int D = AdvectionDiffusionTraits<PhysD3>::D;   // physical dimensions
  static const int N = AdvectionDiffusionTraits<PhysD3>::N;   // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD3>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD3>::template MatrixQ<T>; // matrices

  BCAdvectionDiffusion_LinearRobinBase( const Real A, const Real B, const Real bcdata ) :
      A_(A), B_(B), bcdata_(bcdata) {}
  ~BCAdvectionDiffusion_LinearRobinBase() {}

  BCAdvectionDiffusion_LinearRobinBase& operator=( const BCAdvectionDiffusion_LinearRobinBase& );

  // BC coefficients:  A u + B (kn un + ks us)
  template <class T>
  void coefficients(
      const Real& x, const Real& y, const Real& z, const Real& time, const Real& nx, const Real& ny, const Real& nz,
      MatrixQ<T>& A, MatrixQ<T>& B ) const;

  // BC data
  template <class T>
  void data(
      const Real& x, const Real& y, const Real& z, const Real& time, const Real& nx, const Real& ny, const Real& nz,
      ArrayQ<T>& bcdata ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const Real& nz, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  MatrixQ<Real> A_;
  MatrixQ<Real> B_;
  ArrayQ<Real> bcdata_;
};


template <class T>
inline void
BCAdvectionDiffusion_LinearRobinBase<PhysD3>::coefficients(
    const Real&, const Real&, const Real&, const Real&, const Real&, const Real&, const Real&,
    MatrixQ<T>& A, MatrixQ<T>& B ) const
{
  A = A_;
  B = B_;
}


template <class T>
inline void
BCAdvectionDiffusion_LinearRobinBase<PhysD3>::data(
    const Real& x, const Real& y, const Real& z, const Real& time, const Real& nx, const Real& ny, const Real& nz,
    ArrayQ<T>& bcdata ) const
{
  bcdata = bcdata_;
}

template <>
class BCAdvectionDiffusion<PhysD3, BCTypeLinearRobin_mitLG> : public BCType< BCAdvectionDiffusion<PhysD3, BCTypeLinearRobin_mitLG> >,
                                                              public BCAdvectionDiffusion_LinearRobinBase<PhysD3>
{
public:
  typedef BCAdvectionDiffusion_LinearRobinBase<PhysD3> BaseType;
  typedef BCCategory::LinearScalar_mitLG Category;
  typedef BCAdvectionDiffusionParams<PhysD3, BCTypeLinearRobin_mitLG> ParamsType;

  template<class AdvectiveFlux, class ViscousFlux, class Source>
  BCAdvectionDiffusion(const PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>& pde, const PyDict& d ) :
    BaseType( d.get(ParamsType::params.A), d.get(ParamsType::params.B), d.get(ParamsType::params.bcdata)) {}

  BCAdvectionDiffusion( const Real& A, const Real& B, const Real& bcdata ) : BaseType( A, B, bcdata ) {}
};

template <>
class BCAdvectionDiffusion<PhysD3, BCTypeLinearRobin_sansLG> : public BCType< BCAdvectionDiffusion<PhysD3, BCTypeLinearRobin_sansLG> >,
                                                               public BCAdvectionDiffusion_LinearRobinBase<PhysD3>
{
public:
  typedef BCAdvectionDiffusion_LinearRobinBase<PhysD3> BaseType;
  typedef BCCategory::LinearScalar_sansLG Category;
  typedef BCAdvectionDiffusionParams<PhysD3, BCTypeLinearRobin_sansLG> ParamsType;

  template<class AdvectiveFlux, class ViscousFlux, class Source>
  BCAdvectionDiffusion(const PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>& pde, const PyDict& d ) :
    BaseType( d.get(ParamsType::params.A), d.get(ParamsType::params.B), d.get(ParamsType::params.bcdata)) {}

  BCAdvectionDiffusion( const Real& A, const Real& B, const Real& bcdata ) : BaseType( A, B, bcdata ) {}
};

//----------------------------------------------------------------------------//
// Flux BC:  an u - (kn un + ks us) = bcdata | inflow
//                - (kn un + ks us) = bcdata | outflow
//
// an       normal advection velocity
// kn, ks   normal, tangential diffusion coefficients
//
template<>
struct BCAdvectionDiffusionParams< PhysD3, BCTypeFluxParams > : noncopyable
{
  const ParameterNumeric<Real> bcdata{"bcdata", NO_DEFAULT, NO_RANGE, "BC data"};
  const ParameterBool Inflow{"Inflow", true, "is this an inflow?"};

  static constexpr const char* BCName{"Flux"};
  struct Option
  {
    const DictOption Flux{BCAdvectionDiffusionParams::BCName, BCAdvectionDiffusionParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCAdvectionDiffusionParams params;
};

template<class AdvectiveFlux, class ViscousFlux>
class BCAdvectionDiffusion< PhysD3, BCTypeFlux< AdvectiveFlux, ViscousFlux>> :
  public BCType< BCAdvectionDiffusion<PhysD3, BCTypeFlux<AdvectiveFlux, ViscousFlux>> >
{
public:
  typedef BCCategory::Flux_mitState Category;
  typedef BCAdvectionDiffusionParams< PhysD3, BCTypeFluxParams > ParamsType;

  typedef PhysD3 PhysDim;
  static const int D = AdvectionDiffusionTraits<PhysD3>::D;   // physical dimensions
  static const int N = AdvectionDiffusionTraits<PhysD3>::N;   // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD3>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD3>::template MatrixQ<T>; // matrices

  template<class Source>
  BCAdvectionDiffusion(const PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>& pde, const PyDict& d ) :
    adv_( pde.getAdvectiveFlux() ),
    visc_( pde.getViscousFlux() ),
    fluxViscous_( pde.hasFluxViscous() ),
    inflow_(d.get(ParamsType::params.Inflow)),
    bcdata_(d.get(ParamsType::params.bcdata)) {}

  //cppcheck-suppress noExplicitConstructor
  template<class Source>
  BCAdvectionDiffusion( const PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>& pde,
                        const ArrayQ<Real>& bcdata, const bool inflow ) :
                        adv_(pde.getAdvectiveFlux()),
                        visc_(pde.getViscousFlux()),
                        fluxViscous_(pde.hasFluxViscous()),
                        inflow_(inflow),
                        bcdata_(bcdata){}

  //cppcheck-suppress noExplicitConstructor
  BCAdvectionDiffusion( const AdvectiveFlux& adv, const ViscousFlux& visc,  const bool fluxViscous,
                        const ArrayQ<Real>& bcdata, const bool inflow ) :
                        adv_(adv),
                        visc_(visc),
                        fluxViscous_(fluxViscous),
                        inflow_(inflow),
                        bcdata_(bcdata){}

    virtual ~BCAdvectionDiffusion() {}

  BCAdvectionDiffusion& operator=( const BCAdvectionDiffusion& );

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return fluxViscous_; }

  // BC state vector
  template <class T>
  void state( const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // normal BC flux
  template <class T>
  void fluxNormal( const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const Real& nz, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const AdvectiveFlux& adv_;
  const ViscousFlux& visc_;
  const bool fluxViscous_;
  const bool inflow_;
  ArrayQ<Real> bcdata_;
};


template<class AdvectiveFlux, class ViscousFlux>
template <class T>
inline void
BCAdvectionDiffusion< PhysD3, BCTypeFlux<AdvectiveFlux, ViscousFlux> >::
state( const Real& x, const Real& y, const Real& z, const Real& time,
       const Real& nx, const Real& ny, const Real& nz,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  qB = qI; // Use interior state, only the flux is being specified
}


template<class AdvectiveFlux, class ViscousFlux>
template <class T>
inline void
BCAdvectionDiffusion< PhysD3, BCTypeFlux< AdvectiveFlux, ViscousFlux> >::
fluxNormal( const Real& x, const Real& y, const Real& z, const Real& time,
            const Real& nx, const Real& ny, const Real& nz,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
            const ArrayQ<T>& qB,
            ArrayQ<T>& Fn) const
{
  // set the flux if there's viscosity, or it's inflow
  if ( fluxViscous_ || inflow_ )
    Fn += bcdata_;

  if ( !inflow_ ) // if outflow, don't specify the advective flux
  {
    ArrayQ<T> fx = 0, fy = 0, fz = 0;
    adv_.flux(x, y, z, time, qB, fx, fy, fz);

    // Add advective flux using interior state
    Fn += fx*nx + fy*ny + fz*nz;
  }

}

template<class AdvectiveFlux, class ViscousFlux>
inline void
BCAdvectionDiffusion< PhysD3, BCTypeFlux< AdvectiveFlux, ViscousFlux> >::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCAdvectionDiffusion<PhysD3, BCTypeFlux>:" << std::endl;
  out << indent << "  bcdata_ = :" << bcdata_;
  out << indent << "  inflow_ = " << inflow_ << std::endl;
}

//----------------------------------------------------------------------------//
// Uniform Neumann BC:  d(u)/dn = - d(u_uniform)/dn
//
template <>
class BCAdvectionDiffusion<PhysD3, BCTypeUniformNeumann> : public BCType< BCAdvectionDiffusion<PhysD3, BCTypeUniformNeumann> >
{
public:
  typedef BCCategory::LinearScalar_mitLG Category;

  typedef PhysD3 PhysDim;
  static const int D = AdvectionDiffusionTraits<PhysD3>::D;   // physical dimensions
  static const int N = AdvectionDiffusionTraits<PhysD3>::N;   // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD3>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD3>::template MatrixQ<T>; // matrices

  BCAdvectionDiffusion( const Real u, const Real v, const Real w ) : u_(u), v_(v), w_(w) {}
  ~BCAdvectionDiffusion() {}

  BCAdvectionDiffusion& operator=( const BCAdvectionDiffusion& );

  // BC coefficients:  A u + B d(u)/dn
  template <class T>
  void coefficients(
      const Real& x, const Real& y, const Real& z, const Real& time, const Real& nx, const Real& ny, const Real& nz,
      MatrixQ<T>& A, MatrixQ<T>& B ) const;

  // BC data
  template <class T>
  void data(
      const Real& x, const Real& y, const Real& z, const Real& time, const Real& nx, const Real& ny, const Real& nz,
      ArrayQ<T>& bcdata ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const Real& nz, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  Real u_, v_, w_;      // velocity
};


template <class T>
inline void
BCAdvectionDiffusion<PhysD3, BCTypeUniformNeumann>::coefficients(
    const Real&, const Real&, const Real&, const Real&, const Real&, const Real&, const Real&,
    MatrixQ<T>& A, MatrixQ<T>& B ) const
{
  A = 0;
  B = 1;
}


template <class T>
inline void
BCAdvectionDiffusion<PhysD3, BCTypeUniformNeumann>::data(
    const Real& x, const Real& y, const Real& z, const Real& time, const Real& nx, const Real& ny, const Real& nz,
    ArrayQ<T>& bcdata ) const
{
  bcdata = -(nx*u_ + ny*v_ + nz*w_);
}

//----------------------------------------------------------------------------//
// Solution BC:  u = uexact(x,y,z)
//
// A = 1; B = 0
template<>
struct BCAdvectionDiffusionParams< PhysD3, BCTypeFunctionLinearRobin_mitLG > : ScalarFunction3DParams
{
  static constexpr const char* BCName{"FunctionLinearRobin_mitLG"};
  struct Option
  {
    const DictOption FunctionLinearRobin_mitLG{BCAdvectionDiffusionParams::BCName, BCAdvectionDiffusionParams::checkInputs};
  };

  const ParameterNumeric<Real> A{"A", 1.0, NO_RANGE, "Value coefficient"};
  const ParameterNumeric<Real> B{"B", 0.0, NO_RANGE, "Viscous coefficient"};

  static void checkInputs(PyDict d);

  static BCAdvectionDiffusionParams params;
};

template<>
struct BCAdvectionDiffusionParams< PhysD3, BCTypeFunctionLinearRobin_sansLG > : ScalarFunction3DParams
{
  static constexpr const char* BCName{"FunctionLinearRobin_sansLG"};
  struct Option
  {
    const DictOption FunctionLinearRobin_sansLG{BCAdvectionDiffusionParams::BCName, BCAdvectionDiffusionParams::checkInputs};
  };

  const ParameterNumeric<Real> A{"A", 1.0, NO_RANGE, "Value coefficient"};
  const ParameterNumeric<Real> B{"B", 0.0, NO_RANGE, "Viscous coefficient"};

  static void checkInputs(PyDict d);

  static BCAdvectionDiffusionParams params;
};


template <class PhysDim>
class BCAdvectionDiffusion_SolutionBase;

template <>
class BCAdvectionDiffusion_SolutionBase<PhysD3>
{
public:
  typedef PhysD3 PhysDim;
   static const int D = AdvectionDiffusionTraits<PhysD3>::D;   // physical dimensions
   static const int N = AdvectionDiffusionTraits<PhysD3>::N;   // total solution variables

   static const int NBC = 1;                   // total BCs

   template <class T>
   using ArrayQ = AdvectionDiffusionTraits<PhysD3>::template ArrayQ<T>;    // solution/residual arrays

   template <class T>
   using MatrixQ = AdvectionDiffusionTraits<PhysD3>::template MatrixQ<T>; // matrices

   typedef std::shared_ptr<Function3DBase<ArrayQ<Real>>> Function_ptr;

   //cppcheck-suppress noExplicitConstructor
   BCAdvectionDiffusion_SolutionBase( const Function_ptr& uexact, const ViscousFlux3DBase& visc, Real A = 1, Real B = 0 ) :
       uexact_(uexact), visc_(visc), A_(A), B_(B) {}

   virtual ~BCAdvectionDiffusion_SolutionBase() {}

   // BC coefficients:  A u + B (kn un + ks us)
   template <class T>
   void coefficients(
       const Real& x, const Real& y, const Real& z, const Real& time, const Real& nx, const Real& ny, const Real& nz,
       MatrixQ<T>& A, MatrixQ<T>& B ) const;

   // BC data
   template <class T>
   void data(
       const Real& x, const Real& y, const Real& z, const Real& time, const Real& nx, const Real& ny, const Real& nz,
       ArrayQ<T>& bcdata ) const;

   // is the boundary state valid
   bool isValidState( const Real& nx, const Real& ny, const Real& nz, const ArrayQ<Real>& q ) const { return true; }

   void dump( int indentSize, std::ostream& out = std::cout ) const;

 private:
   const Function_ptr uexact_;
   const ViscousFlux3DBase& visc_;
   Real A_, B_;
};

template <class T>
inline void
BCAdvectionDiffusion_SolutionBase<PhysD3 >::coefficients(
    const Real&, const Real&, const Real&, const Real&, const Real&, const Real&, const Real&,
    MatrixQ<T>& A, MatrixQ<T>& B ) const
{
  A = A_;
  B = B_;
}

template <class T>
inline void
BCAdvectionDiffusion_SolutionBase<PhysD3 >::data(
    const Real& x, const Real& y, const Real& z, const Real& time, const Real& nx, const Real& ny, const Real& nz,
    ArrayQ<T>& bcdata ) const
{
  Real u, ux, uy, uz, ut;
  uexact_->gradient(x, y, z, time, u, ux, uy, uz, ut);

  Real kxx = 0, kxy = 0, kxz = 0;
  Real kyx = 0, kyy = 0, kyz = 0;
  Real kzx = 0, kzy = 0, kzz = 0;

  visc_.diffusionViscous(x, y, z, time,
                         u, ux, uy, uz,
                         kxx, kxy, kxz,
                         kyx, kyy, kyz,
                         kzx, kzy, kzz);

  Real f = kxx*ux + kxy*uy + kxz*uz;
  Real g = kyx*ux + kyy*uy + kyz*uz;
  Real h = kzx*ux + kzy*uy + kzz*uz;

  bcdata = A_*u + B_*(f*nx + g*ny + h*nz);
}

template <>
class BCAdvectionDiffusion<PhysD3, BCTypeFunctionLinearRobin_mitLG> :
  public BCType< BCAdvectionDiffusion<PhysD3, BCTypeFunctionLinearRobin_mitLG> >,
  public BCAdvectionDiffusion_SolutionBase<PhysD3>
{
public:
  typedef BCAdvectionDiffusion_SolutionBase<PhysD3> BaseType;
  typedef BCCategory::LinearScalar_mitLG Category;
  typedef BCAdvectionDiffusionParams<PhysD3, BCTypeFunctionLinearRobin_mitLG> ParamsType;

  template<class AdvectiveFlux, class ViscousFlux, class Source>
  BCAdvectionDiffusion(const PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>& pde, const PyDict& d ) :
    BaseType( ParamsType::params.getFunction(d), pde.getViscousFlux(),
    d.get(ParamsType::params.A), d.get(ParamsType::params.B)) {}

  BCAdvectionDiffusion( const Function_ptr& uexact, const ViscousFlux3DBase& visc, Real A = 1, Real B = 0 ) :
    BaseType( uexact, visc, A, B ) {}
};

template <>
class BCAdvectionDiffusion<PhysD3, BCTypeFunctionLinearRobin_sansLG> :
  public BCType< BCAdvectionDiffusion<PhysD3, BCTypeFunctionLinearRobin_sansLG> >,
  public BCAdvectionDiffusion_SolutionBase<PhysD3>
{
public:
  typedef BCAdvectionDiffusion_SolutionBase<PhysD3> BaseType;
  typedef BCCategory::LinearScalar_sansLG Category;
  typedef BCAdvectionDiffusionParams<PhysD3, BCTypeFunctionLinearRobin_sansLG> ParamsType;

  template<class AdvectiveFlux, class ViscousFlux, class Source>
  BCAdvectionDiffusion(const PDEAdvectionDiffusion<PhysD3, AdvectiveFlux, ViscousFlux, Source>& pde, const PyDict& d ) :
    BaseType( ParamsType::params.getFunction(d), pde.getViscousFlux(),
    d.get(ParamsType::params.A), d.get(ParamsType::params.B)) {}

  BCAdvectionDiffusion( const Function_ptr& uexact, const ViscousFlux3DBase& visc, Real A = 1, Real B = 0 ) :
    BaseType( uexact, visc, A, B ) {}
};

} //namespace SANS

#endif  // BCADVECTIONDIFFUSION3D_H
