// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ADVECTIVEFLUX1D_H
#define ADVECTIVEFLUX1D_H

// 1-D Advection-Diffusion PDE: advective flux

#include "tools/SANSnumerics.h"     // Real
#include "AdvectionDiffusion_Traits.h"
#include "tools/smoothmath.h"
#include "tools/minmax.h"

#include <cmath> // fabs
#include <iostream>

namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: advection velocity
//
// member functions:
//   .flux       advective flux: F(Q)
//   .jacobian   advective flux jacboian: d(F)/d(U)
//----------------------------------------------------------------------------//

class AdvectiveFlux1D_None
{
public:
  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD1>::template MatrixQ<T>;  // matrices

  bool hasFluxAdvective() const { return false; }

  ~AdvectiveFlux1D_None() {}

  // advective flux: F(Q)
  template<class T, class Tf>
  void flux( const Real x, const Real time,
             const ArrayQ<T>& q, ArrayQ<Tf>& fx ) const
  {}

  // advective flux jacobian: d(F)/d(U)
  template<class Tq, class Tf>
  void jacobian( const Real x, const Real time,
                 const ArrayQ<Tq>& q, MatrixQ<Tf>& ax ) const
  {}

  template<class T, class Tf>
  void fluxUpwind( const Real x, const Real time,
                   const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx,
                   ArrayQ<Tf>& f, const Real& scale = 1 ) const
  {}

  template<class T, class Tf>
  void fluxUpwindSpaceTime( const Real x, const Real time,
                            const ArrayQ<T>& qL, const ArrayQ<T>& qR,
                            const Real& nx, const Real& nt,
                            ArrayQ<Tf>& f ) const
  {
    f += 0.5*(nt)*(qR + qL) - 0.5*fabs(nt)*(qR - qL);
  }

  // strong advective flux: div.F
  template<class Tq, class Tg, class Tf>
  void strongFlux( const Real x, const Real time,
                   const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
                   ArrayQ<Tf>& strongPDE ) const
  {}

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;
};


//----------------------------------------------------------------------------//
// advection velocity: uniform
//
// member functions:
//   .flux       advective flux: F(Q)
//   .jacobian   advective flux jacboian: d(F)/d(U)
//----------------------------------------------------------------------------//

class AdvectiveFlux1D_Uniform
{
public:
  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD1>::template MatrixQ<T>;  // matrices

  bool hasFluxAdvective() const { return true; }

  explicit AdvectiveFlux1D_Uniform( const Real u ) : u_(u) {}
  ~AdvectiveFlux1D_Uniform() {}

  template<class T, class Tf>
  void flux( const Real x, const Real time,
             const ArrayQ<T>& q, ArrayQ<Tf>& fx ) const
  {
    fx += u_*q;
  }

  // advective flux jacobian: d(F)/d(U)
  template<class Tq, class Tf>
  void jacobian( const Real x, const Real time,
                 const ArrayQ<Tq>& q, MatrixQ<Tf>& ax ) const
  {
    ax += u_;
  }

  template<class T, class Tf>
  void fluxUpwind( const Real x, const Real time,
                   const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx,
                   ArrayQ<Tf>& f, const Real& scale = 1 ) const
  {
    f += 0.5*(nx*u_)*(qR + qL) - 0.5*scale*fabs(nx*u_)*(qR - qL);
  }

  template<class T, class Tf>
  void fluxUpwindSpaceTime( const Real x, const Real time,
                            const ArrayQ<T>& qL, const ArrayQ<T>& qR,
                            const Real& nx, const Real& nt,
                            ArrayQ<Tf>& f ) const
  {
    f += 0.5*(nt*1 + nx*u_)*(qR + qL) - 0.5*fabs(nt*1 + nx*u_)*(qR - qL);
  }

  // strong advective flux: div.F
  template<class Tq, class Tg, class Tf>
  void strongFlux( const Real x, const Real time,
                   const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
                   ArrayQ<Tf>& strongPDE ) const
  {
    strongPDE += u_*qx;
  }

  // characteristic speed
  template<class Tq, class Tc>
  void speedCharacteristic( Real, Real, Real, const ArrayQ<Tq>& q, Tc& speed ) const
  {
    speed = u_;
  }


  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  const Real u_;      // uniform advection velocity
};


//----------------------------------------------------------------------------//
// advective flux: Burgers
//
// member functions:
//   .flux       advective flux: F(Q)
//   .jacobian   advective flux jacboian: d(F)/d(U)
//----------------------------------------------------------------------------//

class AdvectiveFlux1D_Burgers
{
public:
  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD1>::template MatrixQ<T>;  // matrices

  bool hasFluxAdvective() const { return true; }

  AdvectiveFlux1D_Burgers() {}
  ~AdvectiveFlux1D_Burgers() {}

  template<class T, class Tf>
  void flux( const Real x, const Real time,
             const ArrayQ<T>& q, ArrayQ<Tf>& fx ) const
  {
    fx += 0.5*q*q;
  }

  // advective flux jacobian: d(F)/d(U)
  template<class Tq, class Tf>
  void jacobian( const Real x, const Real time,
                 const ArrayQ<Tq>& q, MatrixQ<Tf>& ax ) const
  {
    ax += q;
  }
//  void jacobian( const Real x, const Real time,
//                 const ArrayQ<SurrealS<1, Real> >& q, MatrixQ<SurrealS<1, Real> >& ax ) const
//  {
//    SurrealS<1, Real> fs = 0.5*q*q;
//
//    ax += fs.deriv();
//  }
//  void jacobian( const Real x, const Real time,
//                 const ArrayQ<Real>& q, MatrixQ<Real>& ax ) const
//  {
//    SurrealS<1, Real> qs = q;
//    SurrealS<1, Real> fs = 0.5*qs*qs;
//
//    ax += fs.deriv();
//  }

  // strong advective flux: div.F
  template<class Tq, class Tg, class Tf>
  void strongFlux( const Real x, const Real time,
                   const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
                   ArrayQ<Tf>& strongPDE ) const
  {
    strongPDE += q*qx;
  }

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

};

//----------------------------------------------------------------------------//
// Local Lax-Friedrichs upwind scheme
//
// member functions:
//   .fluxUpwind            advective flux: n.F(QL,QR)
//   .fluxUpwindSpaceTime   advective flux: n.F(QL,QR)
//----------------------------------------------------------------------------//

template<class AdvectiveFlux1D>
class LocalLaxFriedrichs1D : public AdvectiveFlux1D
{
public:
  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD1>::template MatrixQ<T>;  // matrices

  template<class Tq, class Tf>
  void fluxUpwind( const Real x, const Real time,
                   const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, const Real& nx,
                   ArrayQ<Tf>& f, const Real& scale = 1  ) const
  {
    ArrayQ<Tq> fL = 0;
    ArrayQ<Tq> fR = 0;

    AdvectiveFlux1D::flux(x, time, qL, fL);
    AdvectiveFlux1D::flux(x, time, qR, fR);

    ArrayQ<Tq> axL = 0;
    ArrayQ<Tq> axR = 0;

    AdvectiveFlux1D::jacobian(x, time, qL, axL);
    AdvectiveFlux1D::jacobian(x, time, qR, axR);

    //TODO: Rather arbitrary smoothing constants here
//    Real eps = 1e-10;
//    Real alpha = 40;

    Tq anL = nx*axL;
    Tq anR = nx*axR;

//    T a = smoothmax( smoothabs0(anL, eps), smoothabs0(anR, eps), alpha);
    Tq a = max( fabs(anL), fabs(anR) );

    // Local Lax-Friedrichs Flux
    f += 0.5*( (nx*fR) + (nx*fL) ) - 0.5*scale*a*(qR - qL);
  }

  template<class Tq, class Tf>
  void fluxUpwindSpaceTime( const Real x, const Real time,
                            const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, const Real& nx, const Real& nt,
                            ArrayQ<Tf>& f ) const
  {
    ArrayQ<Tq> fL = 0;
    ArrayQ<Tq> fR = 0;

    AdvectiveFlux1D::flux(x, time, qL, fL);
    AdvectiveFlux1D::flux(x, time, qR, fR);

    ArrayQ<Tq> axL = 0;
    ArrayQ<Tq> axR = 0;

    AdvectiveFlux1D::jacobian(x, time, qL, axL);
    AdvectiveFlux1D::jacobian(x, time, qR, axR);

    //TODO: Rather arbitrary smoothing constants here
    Real eps = 1e-8;
    Real alpha = 40;

    Tq anL = nt + nx*axL;
    Tq anR = nt + nx*axR;

    Tq a = smoothmax( smoothabs0(anL, eps), smoothabs0(anR, eps), alpha);

    // Local Lax-Friedrichs Flux
    f += 0.5*( (nt*qR + nx*fR) + (nt*qL + nx*fL) ) - 0.5*a*(qR - qL);
  }

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const { return true; }

};
} //namespace SANS

#endif  // AdvectiveFlux1D_H
