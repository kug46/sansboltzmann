// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCADVECTIONDIFFUSION_ARTIFICIALVISCOSITY2D_H
#define BCADVECTIONDIFFUSION_ARTIFICIALVISCOSITY2D_H

// 2-D Advection-Diffusion BC class with artificial viscosity

#include "pde/BCNone.h"
#include "BCAdvectionDiffusion2D.h"

#include <boost/mpl/vector.hpp>

namespace SANS
{

//----------------------------------------------------------------------------//
// BC types
//----------------------------------------------------------------------------//

// Define the vector of boundary conditions
template<class AdvectiveFlux, class ViscousFlux>
using BCAdvectionDiffusion_ArtificialViscosity2DVector
  = boost::mpl::vector3< BCNone<PhysD2,AdvectionDiffusionTraits<PhysD2>::N>,
                         BCAdvectionDiffusion<PhysD2, BCTypeLinearRobin_sansLG>,
                         BCAdvectionDiffusion<PhysD2, BCTypeFunctionLinearRobin_sansLG>
                       >;

} //namespace SANS

#endif  // BCADVECTIONDIFFUSION_ARTIFICIALVISCOSITY2D_H
