// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCAdvectionDiffusion_ArtificialViscosity2D.h"

#include "AdvectiveFlux2D.h"
#include "ViscousFlux2D.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{

//===========================================================================//
//
// Parameter classes
//

//===========================================================================//
// Instantiate the BC parameters
typedef BCAdvectionDiffusion_ArtificialViscosity2DVector<AdvectiveFlux2D_Uniform, ViscousFlux2D_Uniform> BCVector;
BCPARAMETER_INSTANTIATE( BCVector )

//===========================================================================//

} //namespace SANS
