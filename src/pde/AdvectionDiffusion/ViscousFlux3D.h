// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef DIFFUSIONMATRIX3D_H
#define DIFFUSIONMATRIX3D_H

// 3-D Advection-Diffusion PDE: diffusion matrix

#include "tools/SANSnumerics.h"     // Real
#include "AdvectionDiffusion_Traits.h"

#include <iostream>
#include <cmath>
#include <utility> // forward

namespace SANS
{

class ViscousFlux3DBase
{
public:
  virtual void diffusionViscous(const Real& x, const Real& y, const Real& z, const Real& time,
                                const Real& q, const Real& qx, const Real& qy, const Real& qz,
                                Real& kxx, Real& kxy, Real& kxz,
                                Real& kyx, Real& kyy, Real& kyz,
                                Real& kzx, Real& kzy, Real& kzz) const = 0;

  virtual ~ViscousFlux3DBase()
  {
  }
};

template<class Derived>
class ViscousFlux3DVirtualInterface: public ViscousFlux3DBase
{
public:
  virtual void diffusionViscous(const Real& x, const Real& y, const Real& z, const Real& time,
                                const Real& q, const Real& qx, const Real& qy, const Real& qz,
                                Real& kxx, Real& kxy, Real& kxz,
                                Real& kyx, Real& kyy, Real& kyz,
                                Real& kzx, Real& kzy, Real& kzz) const override
  {
    reinterpret_cast<const Derived&>( *this ).diffusionViscous( x, y, z, time,
                                                                q, qx, qy, qz,
                                                                kxx, kxy, kxz,
                                                                kyx, kyy, kyz,
                                                                kzx, kzy, kzz );
  }

  virtual ~ViscousFlux3DVirtualInterface()
  {
  }
};


//----------------------------------------------------------------------------//
template<class DiffusionMatrix>
class ViscousFlux3D_LinearInGradient : public DiffusionMatrix,
                                       public ViscousFlux3DVirtualInterface<ViscousFlux3D_LinearInGradient<DiffusionMatrix>>
{
public:
  using DiffusionMatrix::diffusionViscous;

  template <class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD1>::template MatrixQ<T>;  // matrices

  template< class... Args > // cppcheck-suppress noExplicitConstructor
  ViscousFlux3D_LinearInGradient(Args&&... args) : DiffusionMatrix(std::forward<Args>(args)...) {}

  // viscous flux is of the form K(q).gradu
  bool fluxViscousLinearInGradient() const { return true; }

  // viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g, ArrayQ<Tf>& h ) const
  {
    MatrixQ<Tq> kxx=0, kxy=0, kxz=0,
                kyx=0, kyy=0, kyz=0,
                kzx=0, kzy=0, kzz=0;

    diffusionViscous( x, y, z, time, q, qx, qy, qz,
                      kxx, kxy, kxz,
                      kyx, kyy, kyz,
                      kzx, kzy, kzz );

    f -= kxx*qx + kxy*qy + kxz*qz;
    g -= kyx*qx + kyy*qy + kyz*qz;
    h -= kzx*qx + kzy*qy + kzz*qz;
  }

};

//----------------------------------------------------------------------------//
// diffusion matrix: None
//
// member functions:
//   .()      functor returning diffusion coefficient matrix
//----------------------------------------------------------------------------//

class DiffusionMatrix3D_None
{
public:
  template<class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD3>::template ArrayQ<T>;    // solution/residual arrays

  template<class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD3>::template MatrixQ<T>;  // matrices

  static const int D = 1;                     // physical dimensions

  bool hasFluxViscous() const
  {
    return false;
  }

  DiffusionMatrix3D_None()
  {
  }

  template <class Tq, class Tg, class Tk>
  void diffusionViscous( const Real x, const Real y, const Real z, const Real& time,
                         const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
                         MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxz,
                         MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyz,
                         MatrixQ<Tk>& kzx, MatrixQ<Tk>& kzy, MatrixQ<Tk>& kzz ) const
  {}

  template <class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient(
      const Real x, const Real y, const Real z, const Real time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
      MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kxz_x, MatrixQ<Tk>& kyx_x, MatrixQ<Tk>& kzx_x,
      MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y, MatrixQ<Tk>& kyz_y, MatrixQ<Tk>& kzy_y,
      MatrixQ<Tk>& kxz_z, MatrixQ<Tk>& kyz_z, MatrixQ<Tk>& kzx_z, MatrixQ<Tk>& kzy_z, MatrixQ<Tk>& kzz_z ) const
  {}

  // strong form of viscous flux: d(Fv)/d(X)
  template <class Tq, class Tg, class Th, class Tf>
  inline void
  strongFluxViscous( const Real& x, const Real& y, const Real& z, const Real& time,
                     const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
                     const ArrayQ<Th>& qxx,
                     const ArrayQ<Th>& qyx, const ArrayQ<Th>& qyy,
                     const ArrayQ<Th>& qzx, const ArrayQ<Th>& qzy, const ArrayQ<Th>& qzz,
                     ArrayQ<Tf>& strongPDE ) const
  {}

  // strong form of viscous flux: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscous(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& az ) const {}

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

};

typedef ViscousFlux3D_LinearInGradient<DiffusionMatrix3D_None> ViscousFlux3D_None;

//----------------------------------------------------------------------------//
// diffusion matrix: uniform
//
// member functions:
//   .()      functor returning diffusion coefficient matrix
//----------------------------------------------------------------------------//

class DiffusionMatrix3D_Uniform
{
public:
  template<class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD3>::template ArrayQ<T>;    // solution/residual arrays

  template<class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD3>::template MatrixQ<T>;  // matrices

  static const int D = 3;                     // physical dimensions

  bool hasFluxViscous() const
  {
    return true;
  }

  DiffusionMatrix3D_Uniform(const Real kxx, const Real kxy, const Real kxz,
                            const Real kyx, const Real kyy, const Real kyz,
                            const Real kzx, const Real kzy, const Real kzz)
      : kxx_( kxx ), kxy_( kxy ), kxz_( kxz ),
        kyx_( kyx ), kyy_( kyy ), kyz_( kyz ),
        kzx_( kzx ), kzy_( kzy ), kzz_( kzz ) {}

  ~DiffusionMatrix3D_Uniform() {}

  template <class Tq, class Tg, class Tk>
  void diffusionViscous( const Real x, const Real y, const Real z, const Real& time,
                         const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
                         MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxz,
                         MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyz,
                         MatrixQ<Tk>& kzx, MatrixQ<Tk>& kzy, MatrixQ<Tk>& kzz ) const
  {
    kxx += kxx_; kxy += kxy_; kxz += kxz_;
    kyx += kyx_; kyy += kyy_; kyz += kyz_;
    kzx += kzx_; kzy += kzy_; kzz += kzz_;
  }

  template <class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient(
      const Real x, const Real y, const Real z, const Real time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
      MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kxz_x, MatrixQ<Tk>& kyx_x, MatrixQ<Tk>& kzx_x,
      MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y, MatrixQ<Tk>& kyz_y, MatrixQ<Tk>& kzy_y,
      MatrixQ<Tk>& kxz_z, MatrixQ<Tk>& kyz_z, MatrixQ<Tk>& kzx_z, MatrixQ<Tk>& kzy_z, MatrixQ<Tk>& kzz_z ) const
  {}

  // strong form of viscous flux: d(Fv)/d(X)
  template <class Tq, class Tg, class Th, class Tf>
  inline void
  strongFluxViscous( const Real& x, const Real& y, const Real& z, const Real& time,
                     const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
                     const ArrayQ<Th>& qxx,
                     const ArrayQ<Th>& qyx, const ArrayQ<Th>& qyy,
                     const ArrayQ<Th>& qzx, const ArrayQ<Th>& qzy, const ArrayQ<Th>& qzz,
                     ArrayQ<Tf>& strongPDE ) const
  {
    strongPDE -= kxx_ * qxx + (kxy_ + kyx_) * qyx + kyy_ * qyy + (kxz_ + kzx_) * qzx + (kyz_ + kzy_) * qzy + kzz_ * qzz;
  }

  // strong form of viscous flux: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscous(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& az ) const {}

  void dump(int indentSize = 0, std::ostream& out = std::cout) const;

private:
  const Real kxx_, kxy_, kxz_,
             kyx_, kyy_, kyz_,
             kzx_, kzy_, kzz_;          // constant diffusion matrix
};

typedef ViscousFlux3D_LinearInGradient<DiffusionMatrix3D_Uniform> ViscousFlux3D_Uniform;

//----------------------------------------------------------------------------//
// diffusion matrix: Polynomial
// FOR THE EXAMPLE, A SCALAR FUNCTION, ARRAYQ SET TO D1 INSTEAD OF D3
// Fv = (A+B*q^2)*dq/dx
//
// member functions:
//   .()      functor returning diffusion coefficient matrix
//----------------------------------------------------------------------------//

class DiffusionMatrix3D_Poly
{
public:
  template<class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD3>::template ArrayQ<T>; // solution/residual arrays

  template<class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD3>::template MatrixQ<T>; // matrices

  static const int D = 3;                         // physical dimensions

  bool hasFluxViscous() const
  {
    return true;
  }

  explicit DiffusionMatrix3D_Poly(const Real A, const Real B)
   : A_( A ), B_( B )
   {
   }

  template <class Tq, class Tg, class Tk>
  void diffusionViscous( const Real x, const Real y, const Real z, const Real& time,
                         const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
                         MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxz,
                         MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyz,
                         MatrixQ<Tk>& kzx, MatrixQ<Tk>& kzy, MatrixQ<Tk>& kzz ) const
  {
    Tq k = A_ + B_ * pow( q, 1 );      //Linear;
    kxx += k;
    kyy += k;
    kzz += k;
    kxy += 0;
    kxz += 0;
    kyx += 0;
    kyz += 0;
    kzx += 0;
    kzy += 0;

  }

  // strong for of viscous flux: d(Fv)/d(X)
  template<class T>//, class Tf>
  inline void strongFluxViscous(const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<T>& q, const ArrayQ<T>& qx,
                                const ArrayQ<T>& qy, const ArrayQ<T>& qz, const ArrayQ<T>& qxx, const ArrayQ<T>& qyx, const ArrayQ<T>& qyy,
                                const ArrayQ<T>& qzx, const ArrayQ<T>& qzy, const ArrayQ<T>& qzz, ArrayQ<T>& strongPDE) const
  {

    strongPDE -= B_ * qx * qx + (A_ + B_ * q) * qxx
                +B_ * qy * qy + (A_ + B_ * q) * qyy
                +B_ * qz * qz + (A_ + B_ * q) * qzz;
  }

  // is state physically valid
  bool isValidState(const ArrayQ<Real>& q) const
  {
    return true; //q < 10;
  }


  // strong form of viscous flux: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscous(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& az ) const
  {
    ax -= B_*qx;
    ay -= B_*qy;
    az -= B_*qz;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;


private:
  const Real A_; // coefficient for A+B*q^2
  const Real B_;
};

typedef ViscousFlux3D_LinearInGradient<DiffusionMatrix3D_Poly> ViscousFlux3D_Poly;

} //namespace SANS

#endif  // DIFFUSIONMATRIX3D_H
