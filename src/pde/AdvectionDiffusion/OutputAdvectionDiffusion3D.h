// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef OUTPUTADVECTIONDIFFUSION3D_H
#define OUTPUTADVECTIONDIFFUSION3D_H

// Python must be included first
#include "Python/PyDict.h"

#include "tools/SANSnumerics.h"

#include "pde/OutputCategory.h"
#include "Topology/Dimension.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//                                    3D
//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//
//  Function Weighted Residual
//
template< class WeightFunction >
class OutputAdvectionDiffusion3D_FunctionWeightedResidual
    : public OutputType< OutputAdvectionDiffusion3D_FunctionWeightedResidual<WeightFunction> >
{
public:
  typedef PhysD3 PhysDim;
  typedef OutputCategory::WeightedResidual Category;

  template<class T>
  using ArrayQ = T;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the transpose Jacobian of this output functional

  explicit OutputAdvectionDiffusion3D_FunctionWeightedResidual( const WeightFunction& w ) : wfcn_(w) {}
  explicit OutputAdvectionDiffusion3D_FunctionWeightedResidual( const PyDict& d ) : wfcn_(d) {}

  void operator()(const Real& x, const Real& y, const Real& z, const Real& time, ArrayQ<Real>& weight ) const
  {
    weight = wfcn_(x,y,z, time);
  }

private:
  WeightFunction wfcn_;
};

//----------------------------------------------------------------------------//
//  Weighted Residual
//
class OutputAdvectionDiffusion3D_WeightedResidual : public OutputType< OutputAdvectionDiffusion3D_WeightedResidual >
{
public:
  typedef PhysD3 PhysDim;
  typedef OutputCategory::WeightedResidual Category;

  template<class T>
  using ArrayQ = T;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the transpose Jacobian of this output functional

  explicit OutputAdvectionDiffusion3D_WeightedResidual( const Real& w ) : w_(w) {}

  void operator()(const Real& x, const Real& y, const Real& z, const Real& time, ArrayQ<Real>& weight ) const
  {
    weight = w_;
  }

private:
  Real w_;
};

//----------------------------------------------------------------------------//
//  Function Weighted Boundary Flux
//
template <class PDENDConvert, class WeightFunction>
class OutputAdvectionDiffusion3D_FunctionWeightedFlux
    : public OutputType< OutputAdvectionDiffusion3D_FunctionWeightedFlux<PDENDConvert,WeightFunction> >
{
public:
  typedef PhysD3 PhysDim;
  typedef PDENDConvert PDE;
  typedef OutputCategory::Functional Category;

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputAdvectionDiffusion3D_FunctionWeightedFlux( const PDENDConvert& pde, const WeightFunction& w ) :
    pde_(pde), wfcn_(w) {}

  explicit OutputAdvectionDiffusion3D_FunctionWeightedFlux( const PDENDConvert& pde, const PyDict& d ) :
    pde_(pde), wfcn_(d) {}

  bool needsSolutionGradient() const { return true; }

  template<class T>
  void operator()(const Real& x, const Real& y, const Real& z, const Real& time,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz,
                  ArrayJ<T>& output ) const
  {
    SANS_DEVELOPER_EXCEPTION("THIS IS A BOUNDARY OUTPUT");
  }

  template<class T>
  void operator()(const Real& x, const Real& y, const Real& z, const Real& time,
                  const Real& nx, const Real& ny, const Real& nz,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz,
                  ArrayJ<T>& output ) const
  {
    Real w = wfcn_(x,y,z,time);

    ArrayQ<T> Fn = 0;
    ArrayQ<T> Fx = 0, Fy = 0, Fz = 0;

    if ( pde_.hasFluxAdvective() )
      pde_.fluxAdvective( x, y, z, time, q, Fx, Fy, Fz );

    // viscous flux
    if ( pde_.hasFluxViscous() )
      pde_.fluxViscous( x, y, z, time, q, qx, qy, qz, Fx, Fy, Fz );

    Fn = Fx*nx + Fy*ny + Fz*nz;

    output = w*Fn;

  }
private:
  const PDENDConvert& pde_;
  const WeightFunction& wfcn_;
};

//----------------------------------------------------------------------------//
//  Weighted Boundary Flux
//
template <class PDENDConvert>
class OutputAdvectionDiffusion3D_WeightedFlux
    : public OutputType< OutputAdvectionDiffusion3D_WeightedFlux<PDENDConvert> >
{
public:
  typedef PhysD3 PhysDim;
  typedef PDENDConvert PDE;
  typedef OutputCategory::Functional Category;

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputAdvectionDiffusion3D_WeightedFlux( const PDENDConvert& pde, const Real& w ) :
    pde_(pde), w_(w) {}


  bool needsSolutionGradient() const { return true; }

  template<class T>
  void operator()(const Real& x, const Real& y, const Real& z, const Real& time,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz,
                  ArrayJ<T>& output ) const
  {
    SANS_DEVELOPER_EXCEPTION("THIS IS A BOUNDARY OUTPUT");
  }

  template<class T>
  void operator()(const Real& x, const Real& y, const Real& z, const Real& time,
                  const Real& nx, const Real& ny, const Real& nz,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz,
                  ArrayJ<T>& output ) const
  {
    ArrayQ<T> Fn = 0;
    ArrayQ<T> Fx = 0, Fy = 0, Fz = 0;

    if ( pde_.hasFluxAdvective() )
      pde_.fluxAdvective( x, y, z, time, q, Fx, Fy, Fz );

    // viscous flux
    if ( pde_.hasFluxViscous() )
      pde_.fluxViscous( x, y, z, time, q, qx, qy, qz, Fx, Fy, Fz );

    Fn = Fx*nx + Fy*ny + Fz*nz;

    output = w_*Fn;

  }
private:
  const PDENDConvert& pde_;
  const Real& w_;
};


//----------------------------------------------------------------------------//
//  Function Weighted Boundary State
//
template <class PDENDConvert, class WeightFunction>
class OutputAdvectionDiffusion3D_FunctionWeightedState
    : public OutputType< OutputAdvectionDiffusion3D_FunctionWeightedState<PDENDConvert,WeightFunction> >
{
public:
  typedef PhysD3 PhysDim;
  typedef PDENDConvert PDE;
  typedef OutputCategory::Functional Category;

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputAdvectionDiffusion3D_FunctionWeightedState( const PDENDConvert& pde, const WeightFunction& w ) :
    pde_(pde), wfcn_(w) {}

  explicit OutputAdvectionDiffusion3D_FunctionWeightedState( const PDENDConvert& pde, const PyDict& d ) :
    pde_(pde), wfcn_(d) {}

  bool needsSolutionGradient() const { return true; }

  template<class T>
  void operator()(const Real& x, const Real& y, const Real& z, const Real& time,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz,
                  ArrayJ<T>& output ) const
  {
    Real w = wfcn_(x,y,z,time);
    output = w*q;
  }

  template<class T>
  void operator()(const Real& x, const Real& y, const Real& z, const Real& time,
                  const Real& nx, const Real& ny, const Real& nz,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz,
                  ArrayJ<T>& output ) const
  {
    operator()(x,y,z,time,q,qx,qy,qz,output);
  }
private:
  const PDENDConvert& pde_;
  const WeightFunction& wfcn_;
};

//----------------------------------------------------------------------------//
//  Weighted Boundary State
//
template <class PDENDConvert>
class OutputAdvectionDiffusion3D_WeightedState
    : public OutputType< OutputAdvectionDiffusion3D_WeightedState<PDENDConvert> >
{
public:
  typedef PhysD3 PhysDim;
  typedef PDENDConvert PDE;
  typedef OutputCategory::Functional Category;

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputAdvectionDiffusion3D_WeightedState( const PDENDConvert& pde, const Real& w ) :
    pde_(pde), w_(w) {}


  bool needsSolutionGradient() const { return true; }

  template<class T>
  void operator()(const Real& x, const Real& y, const Real& z, const Real& time,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz,
                  ArrayJ<T>& output ) const
  {
    output = w_*q;
  }

  template<class T>
  void operator()(const Real& x, const Real& y, const Real& z, const Real& time,
                  const Real& nx, const Real& ny, const Real& nz,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz,
                  ArrayJ<T>& output ) const
  {
    operator()(x,y,z,time,q,qx,qy,qz,output);
  }
private:
  const PDENDConvert& pde_;
  const Real& w_;
};




}

#endif //OUTPUTADVECTIONDIFFUSION_H
