// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCAdvectionDiffusion1D.h"

#include "AdvectiveFlux1D.h"
#include "ViscousFlux1D.h"
#include "Source1D.h"
#include "ForcingFunction1D.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{

template ParameterOption<BCAdvectionDiffusionParams<PhysD1, BCTypeFunction_mitStateParam>::TypeOptions>::ExtractType
PyDict::get(ParameterType<ParameterOption<BCAdvectionDiffusionParams<PhysD1, BCTypeFunction_mitStateParam>::TypeOptions> > const&) const;

//===========================================================================//
//
// Parameter classes
//

// cppcheck-suppress passedByValue
void BCAdvectionDiffusionParams<PhysD1, BCTypeTimeOutParam>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  d.checkUnknownInputs(allParams);
}
BCAdvectionDiffusionParams<PhysD1, BCTypeTimeOutParam> BCAdvectionDiffusionParams<PhysD1, BCTypeTimeOutParam>::params;

// cppcheck-suppress passedByValue
void BCAdvectionDiffusionParams<PhysD1, BCTypeTimeICParam>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.qB));
  d.checkUnknownInputs(allParams);
}
BCAdvectionDiffusionParams<PhysD1, BCTypeTimeICParam> BCAdvectionDiffusionParams<PhysD1, BCTypeTimeICParam>::params;

// cppcheck-suppress passedByValue
void BCAdvectionDiffusionParams<PhysD1, BCTypeTimeIC_FunctionParam>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Function));
  d.checkUnknownInputs(allParams);
}
BCAdvectionDiffusionParams<PhysD1, BCTypeTimeIC_FunctionParam> BCAdvectionDiffusionParams<PhysD1, BCTypeTimeIC_FunctionParam>::params;

// cppcheck-suppress passedByValue
void BCAdvectionDiffusionParams<PhysD1, BCTypeNatural>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.gB));
  d.checkUnknownInputs(allParams);
}
BCAdvectionDiffusionParams<PhysD1, BCTypeNatural> BCAdvectionDiffusionParams<PhysD1, BCTypeNatural>::params;

// cppcheck-suppress passedByValue
void BCAdvectionDiffusionParams<PhysD1, BCTypeLinearRobin_mitLG>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.A));
  allParams.push_back(d.checkInputs(params.B));
  allParams.push_back(d.checkInputs(params.bcdata));
  d.checkUnknownInputs(allParams);
}
BCAdvectionDiffusionParams<PhysD1, BCTypeLinearRobin_mitLG> BCAdvectionDiffusionParams<PhysD1, BCTypeLinearRobin_mitLG>::params;

// cppcheck-suppress passedByValue
void BCAdvectionDiffusionParams<PhysD1, BCTypeLinearRobin_sansLG>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.A));
  allParams.push_back(d.checkInputs(params.B));
  allParams.push_back(d.checkInputs(params.bcdata));
  d.checkUnknownInputs(allParams);
}
BCAdvectionDiffusionParams<PhysD1, BCTypeLinearRobin_sansLG> BCAdvectionDiffusionParams<PhysD1, BCTypeLinearRobin_sansLG>::params;

// cppcheck-suppress passedByValue
void BCAdvectionDiffusionParams<PhysD1, BCTypeDirichlet_mitStateParam>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.qB));
  d.checkUnknownInputs(allParams);
}
BCAdvectionDiffusionParams<PhysD1, BCTypeDirichlet_mitStateParam> BCAdvectionDiffusionParams<PhysD1, BCTypeDirichlet_mitStateParam>::params;

// cppcheck-suppress passedByValue
void BCAdvectionDiffusionParams<PhysD1, BCTypeFluxParams>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.bcdata));
  allParams.push_back(d.checkInputs(params.Inflow));
  d.checkUnknownInputs(allParams);
}
BCAdvectionDiffusionParams<PhysD1, BCTypeFluxParams> BCAdvectionDiffusionParams<PhysD1, BCTypeFluxParams>::params;

// cppcheck-suppress passedByValue
void BCAdvectionDiffusionParams<PhysD1, BCTypeFunction_mitStateParam>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Function));
  allParams.push_back(d.checkInputs(params.SolutionBCType));
  allParams.push_back(d.checkInputs(params.Upwind));
  d.checkUnknownInputs(allParams);
}
BCAdvectionDiffusionParams<PhysD1, BCTypeFunction_mitStateParam> BCAdvectionDiffusionParams<PhysD1, BCTypeFunction_mitStateParam>::params;

// cppcheck-suppress passedByValue
void BCAdvectionDiffusionParams<PhysD1, BCTypeFunctionLinearRobin_mitLG>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Function));
  allParams.push_back(d.checkInputs(params.A));
  allParams.push_back(d.checkInputs(params.B));
  d.checkUnknownInputs(allParams);
}
BCAdvectionDiffusionParams<PhysD1, BCTypeFunctionLinearRobin_mitLG> BCAdvectionDiffusionParams<PhysD1, BCTypeFunctionLinearRobin_mitLG>::params;

// cppcheck-suppress passedByValue
void BCAdvectionDiffusionParams<PhysD1, BCTypeFunctionLinearRobin_sansLG>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Function));
  allParams.push_back(d.checkInputs(params.A));
  allParams.push_back(d.checkInputs(params.B));
  d.checkUnknownInputs(allParams);
}
BCAdvectionDiffusionParams<PhysD1, BCTypeFunctionLinearRobin_sansLG> BCAdvectionDiffusionParams<PhysD1, BCTypeFunctionLinearRobin_sansLG>::params;

//===========================================================================//
// Instantiate the BC parameters
typedef BCAdvectionDiffusion1DVector<AdvectiveFlux1D_None   , ViscousFlux1D_Uniform> BCVector_ANDU; BCPARAMETER_INSTANTIATE( BCVector_ANDU )
typedef BCAdvectionDiffusion1DVector<AdvectiveFlux1D_Uniform, ViscousFlux1D_Uniform> BCVector_AUDU; BCPARAMETER_INSTANTIATE( BCVector_AUDU )
typedef BCAdvectionDiffusion1DVector<AdvectiveFlux1D_Uniform, ViscousFlux1D_None   > BCVector_AUDN; BCPARAMETER_INSTANTIATE( BCVector_AUDN )
typedef BCAdvectionDiffusion1DVector<AdvectiveFlux1D_None   , ViscousFlux1D_Exp    > BCVector_ANDE; BCPARAMETER_INSTANTIATE( BCVector_ANDE )
typedef BCAdvectionDiffusion1DVector<AdvectiveFlux1D_None   , ViscousFlux1D_Oliver    > BCVector_ANDO; BCPARAMETER_INSTANTIATE( BCVector_ANDO )

typedef BCAdvectionDiffusion1DVector<LocalLaxFriedrichs1D<AdvectiveFlux1D_Burgers>, ViscousFlux1D_Uniform> BCVector_ALaxDU;
BCPARAMETER_INSTANTIATE( BCVector_ALaxDU )

//===========================================================================//
void
BCAdvectionDiffusion_LinearRobinBase<PhysD1>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCAdvectionDiffusion<PhysD1, BCTypeLinearRobin>:" << std::endl;
  out << indent << "  A_ = " << A_ << std::endl;
  out << indent << "  B_ = " << B_ << std::endl;
  out << indent << "  bcdata_ = " << bcdata_ << std::endl;
}

void
BCAdvectionDiffusion_SolutionBase<PhysD1>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCAdvectionDiffusion<PhysD1, BCTypeFunction>" << std::endl;
}

#if 0
void
BCAdvectionDiffusion<PhysD1, BCTypeFlux>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCAdvectionDiffusion<PhysD1, BCTypeFlux>:" << std::endl;
  out << indent << "  pde_:" << std::endl;
  pde_.dump( indentSize+2, out );
  out << indent << "  bcdata_ = " << bcdata_ << std::endl;
}
#endif

} //namespace SANS
