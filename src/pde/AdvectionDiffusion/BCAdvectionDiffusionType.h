// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCADVECTIONDIFFUSIONTYPE_H
#define BCADVECTIONDIFFUSIONTYPE_H

// Advection-Diffusion BC types

namespace SANS
{

//----------------------------------------------------------------------------//
// BC class: Advection-Diffusion
//----------------------------------------------------------------------------//

class BCTypeNatural;
class BCTypeLinearRobin_mitLG;
class BCTypeLinearRobin_sansLG;

template<class PDE>
class BCTypeFlux;

class BCTypeFunction;

} //namespace SANS

#endif  // BCADVECTIONDIFFUSIONTYPE_H
