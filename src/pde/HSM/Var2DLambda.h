// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef VAR2DLAMBDA_H
#define VAR2DLAMBDA_H

// solution interpreter class
// HSM2D: primitive input variables (rx, ry, lam3, f11, f22, f12)

#include "tools/SANSnumerics.h"     // Real
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "Topology/Dimension.h"
#include "HSMVariable2D.h"

#include <cmath>              // sin, cos
#include <iostream>

namespace SANS
{

//----------------------------------------------------------------------------//
// solution variable interpreter: HSM2D
// primitive variables {position, lambda, forces}
//
// template parameters:
//   T                      solution DOF data type (e.g. Surreal)
//   VarType                state vector type (e.g. VarTypeLambda)
//
// member functions:
//   .getPosition           get position as a VectorS {rx, ry}
//   .getPositionRefDerivs  get derivatives of position wrt reference coords (xi,eta)
//   .getPositionGrad       get derivatives of position wrt global coords (x,y)
//   .getForces             get force resultants as a VectorS {f11, f22, f12}
//   .calc_ehat             calculate local basis from log-quaternion
//   .calc_ehat_derivs      calcualte local basis and derivatives from log-quaternion
//   .isValidState          T/F: determine if state is physically valid
//
//   .dump                  debug dump of private data
//----------------------------------------------------------------------------//


class VarTypeLambda;

// primary template
template <class VarType>
class VarInterpret2D;


template <>
class VarInterpret2D<VarTypeLambda>
{
public:
  typedef PhysD2 PhysDim;
  static const int D = PhysDim::D;   // physical dimensions
  static const int N = 6;            // total solution variables

  template <class T>
  using ArrayQ = DLA::VectorS<N,T>;  // solution/residual arrays

  template <class T>
  using VectorX = DLA::VectorS<D,T>; // vectors in global x,y coordinates

  template <class T>
  using VectorE = DLA::VectorS<D,T>; // vectors in local xi,eta coordinates

  template <class T>
  using Vector3 = DLA::VectorS<3,T>; // 3-vectors

  template <class T>
  using MatrixSymS2 = DLA::MatrixSymS<2,T>; // symmetric matrix

  explicit VarInterpret2D() {}
  ~VarInterpret2D() {}

  VarInterpret2D( const VarInterpret2D& ) = delete;
  VarInterpret2D& operator=( const VarInterpret2D& );

  template<class T>
  void getPosition( const ArrayQ<T>& var, VectorX<T>& r ) const;

  template<class Tv, class Tx>
  void getPositionRefDerivs( const ArrayQ<Tv>& var,
                             const ArrayQ<Tx>& varxi, const ArrayQ<Tx>& vareta,
                             VectorX<Tx>& rxi, VectorX<Tx>& reta) const;

  template<class T>
  void getPositionGrad( const ArrayQ<T>& var,
                        const ArrayQ<T>& varx, const ArrayQ<T>& vary,
                        VectorX<T>& rx, VectorX<T>& ry) const;

  template<class T>
  void getForces( const ArrayQ<T>& var, Vector3<T>& floc ) const;

  template<class T>
  void getForces( const ArrayQ<T>& var, MatrixSymS2<T>& floc ) const;

  template<class T>
  void calc_ehat( const ArrayQ<T>& var, VectorX<T>& e1, VectorX<T>& e2 ) const;

  template<class Tv, class Tg, class Te>
  void calc_ehat_derivs( const ArrayQ<Tv>& var,
                         const ArrayQ<Tg>& varx, const ArrayQ<Tg>& vary,
                         VectorX<Te>& e1,  VectorX<Te>& e2,
                         VectorX<Te>& e1x, VectorX<Te>& e1y,
                         VectorX<Te>& e2x, VectorX<Te>& e2y) const;

  // is state physically valid (for now, say true)
  bool isValidState( const ArrayQ<Real>& var ) const;

  // Set state vector from an input vector using log-quaternions
  template<class T>
  void setFromVector( ArrayQ<T>& var, const PositionLambdaForces2D& data ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

  static const int irx   = 0;
  static const int iry   = 1;
  static const int ilam3 = 2;
  static const int if11  = 3;
  static const int if22  = 4;
  static const int if12  = 5;
};


template <class T>
inline void
VarInterpret2D<VarTypeLambda>::getPosition(
    const ArrayQ<T>& var, VectorX<T>& r ) const
{
  r = {var[irx], var[iry]};
}

template <class Tv, class Tx>
inline void
VarInterpret2D<VarTypeLambda>::getPositionRefDerivs(
    const ArrayQ<Tv>& var, const ArrayQ<Tx>& varxi, const ArrayQ<Tx>& vareta,
    VectorX<Tx>& rxi, VectorX<Tx>& reta) const
{
  rxi  = {varxi[irx], varxi[iry]};
  reta = {vareta[irx], vareta[iry]};
}

template <class T>
inline void
VarInterpret2D<VarTypeLambda>::getPositionGrad(
    const ArrayQ<T>& var, const ArrayQ<T>& varx, const ArrayQ<T>& vary,
    VectorX<T>& rx, VectorX<T>& ry) const
{
  rx = {varx[irx], varx[iry]};
  ry = {vary[irx], vary[iry]};
}

template <class T>
inline void
VarInterpret2D<VarTypeLambda>::getForces(
    const ArrayQ<T>& var, Vector3<T>& floc ) const
{
  // floc = { f11, f22, f12 }
  floc = {var[if11], var[if22], var[if12]};
}

template <class T>
inline void
VarInterpret2D<VarTypeLambda>::getForces(
    const ArrayQ<T>& var, MatrixSymS2<T>& flocbar ) const
{
  // flocbar = {{ f11 },
  //            { f12, f22 }}
  flocbar = {{var[if11]},
             {var[if12], var[if22]}};
}

template <class T>
inline void
VarInterpret2D<VarTypeLambda>::calc_ehat(
    const ArrayQ<T>& var, VectorX<T>& e1, VectorX<T>& e2 ) const
{
  // Get log-quaternion lam3 from state vector
  const T& lam3  = var[ilam3];

  // Calculate nonzero quaternion components and derivatives (Eq.6 in hsm2D)
  T p0  = cos( lam3 );
  T p3  = sin( lam3 );

  // Deformed nodal basis vectors e1 and e2
  e1  = { 1-2*pow(p3,2) , 2*p0*p3       };
  e2  = { -2*p0*p3      , 1-2*pow(p3,2) };

}

template <class Tv, class Tg, class Te>
inline void
VarInterpret2D<VarTypeLambda>::calc_ehat_derivs(
    const ArrayQ<Tv>& var, const ArrayQ<Tg>& varx, const ArrayQ<Tg>& vary,
    VectorX<Te>& e1,  VectorX<Te>& e2,
    VectorX<Te>& e1x, VectorX<Te>& e1y,
    VectorX<Te>& e2x, VectorX<Te>& e2y) const
{
  // Get log-quaternion lam3 and derivatives from state vector
  const Tv& lam3  = var[ilam3];
  const Tg& lam3x = varx[ilam3];
  const Tg& lam3y = vary[ilam3];

  // Calculate nonzero quaternion components and derivatives (Eq.6 in hsm2D)
  Te p0  =  cos( lam3 );
  Te p0x = -sin( lam3 )*lam3x;
  Te p0y = -sin( lam3 )*lam3y;

  Te p3  = sin( lam3 );
  Te p3x = cos( lam3 )*lam3x;
  Te p3y = cos( lam3 )*lam3y;

  // Deformed nodal basis vectors e1 and e2, and derivatives
  e1  = { 1-2*pow(p3,2)      , 2*p0*p3           };
  e1x = {  -4*    p3*p3x     , 2*(p0x*p3+p0*p3x) };
  e1y = {  -4*    p3*p3y     , 2*(p0y*p3+p0*p3y) };

  e2  = { -2*p0*p3           , 1-2*pow(p3,2)     };
  e2x = { -2*(p0x*p3+p0*p3x) ,  -4*    p3*p3x    };
  e2y = { -2*(p0y*p3+p0*p3y) ,  -4*    p3*p3y    };

}

// is state physically valid: for now assume yes
inline bool
VarInterpret2D<VarTypeLambda>::isValidState( const ArrayQ<Real>& var ) const
{
  return true;
}

// Set state vector from an input vector using log-quaternions
template <class T>
inline void
VarInterpret2D<VarTypeLambda>::setFromVector(
    ArrayQ<T>& var, const PositionLambdaForces2D& data ) const
{
  var[irx] = data.rx;
  var[iry] = data.ry;
  var[ilam3] = data.lam3;
  var[if11] = data.f11;
  var[if22] = data.f22;
  var[if12] = data.f12;
}


}

#endif  // VAR2DLAMBDA_H
