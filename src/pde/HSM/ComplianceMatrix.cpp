// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "ComplianceMatrix.h"
#include "LinearAlgebra/DenseLinAlg/InverseLU.h"
#include "Surreal/SurrealS.h"

namespace SANS
{

// Calculate extensional compliance matrix, Aij^(-1)
template<class T>
DLA::MatrixS<3,3,T> calcComplianceMatrix(const Real& E, const Real& nu, const T& t)
{
  // Calculate cij, material property matrix
  // Implements equation [11]
   DLA::MatrixS<3,3,Real> mat = {{ 1 , nu ,  0   },
                                 { nu,  1 ,  0   },
                                 { 0 ,  0 , 1-nu }};

   DLA::MatrixS<3,3,Real> cij = E/(1-pow(nu,2)) * mat;

   // Calculate Aij, extensional stiffness submatrix
   // Assumes constant cij through material thickness
   // Implements equation [10]
   DLA::MatrixS<3,3,T> Aij = t * cij;

   // Compute the extensional compliance matrix (inverse of Aij)
   DLA::MatrixS<3,3,T> AijInv = DLA::InverseLU::Inverse(Aij);

   return AijInv;
}

template DLA::MatrixS<3,3,Real> calcComplianceMatrix<Real>(const Real& E, const Real& nu, const Real& t);
template DLA::MatrixS<3,3,SurrealS<1>> calcComplianceMatrix<SurrealS<1>>(const Real& E, const Real& nu, const SurrealS<1>& t);
}
