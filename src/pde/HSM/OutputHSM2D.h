// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef OUTPUTHSM2D_H
#define OUTPUTHSM2D_H

#include "PDEHSM2D.h"

#include "pde/OutputCategory.h"

namespace SANS
{


//----------------------------------------------------------------------------//
// Subfunctions to compute single output from four HSM variables

template <class VarType>
class OutputHSM2D_Displacement : public OutputType< OutputHSM2D_Displacement<VarType> >
{
public:
  typedef PhysD2 PhysDim;
  typedef PDEHSM2D<VarType> PDE;
  typedef typename OutputCategory::Functional Category;

  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputHSM2D_Displacement( const PDE& pde ) :
    pde_(pde) {}

  bool needsSolutionGradient() const { return false; }

  template<class T>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const Real& nx, const Real& ny,
                  const ArrayQ<T>& var, const ArrayQ<T>& varx, const ArrayQ<T>& vary, ArrayJ<T>& output ) const
  {
    // The undeformed position
    typename PDE::template VectorX<Real> r0 = {x, y};

    // Deformed position
    typename PDE::template VectorX<T> dr, r;
    pde_.varInterpret_.getPosition(var, r);

    dr = r - r0;

    // Distance
    output = sqrt(dot(dr,dr));
  }

private:
  const PDE& pde_;
};

}

#endif //OUTPUTHSM2D_H
