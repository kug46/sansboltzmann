// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef VAR3DLAMBDA_H
#define VAR3DLAMBDA_H

// solution interpreter class
// HSM2D: primitive input variables (rx, ry, rz, lam1, lam2, lam3, f11, f22, f12, m11, m22, m12)

#include "tools/SANSnumerics.h"     // Real
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "Topology/Dimension.h"
#include "HSMVariable3D.h"

#include <cmath>              // sin, cos
#include <iostream>

namespace SANS
{

//----------------------------------------------------------------------------//
// solution variable interpreter: HSM2D
// primitive variables {position, lambda, forces}
//
// template parameters:
//   T                      solution DOF data type (e.g. Surreal)
//   VarType                state vector type (e.g. VarTypeLambda)
//
// member functions:
//   .getPosition           get position as a VectorS {rx, ry}
//   .getPositionRefDerivs  get derivatives of position wrt reference coords (xi,eta)
//   .getPositionGrad       get derivatives of position wrt global coords (x,y)
//   .getForces             get force resultants as a VectorS {f11, f22, f12}
//   .getMoments            get moment resultants as a VectorS {m11, m22, m12}
//   .calc_ehat             calculate local basis from log-quaternion
//   .calc_ehat_derivs      calcualte local basis and derivatives from log-quaternion
//   .isValidState          T/F: determine if state is physically valid
//
//   .dump                  debug dump of private data
//----------------------------------------------------------------------------//


class VarTypeLambda;

// primary template
template <class VarType>
class VarInterpret3D;


template <>
class VarInterpret3D<VarTypeLambda>
{
public:
  typedef PhysD3 PhysDim;
  static const int D = PhysDim::D;   // physical dimensions
  static const int N = 12;           // total solution variables

  template <class T>
  using ArrayQ = DLA::VectorS<N,T>;  // solution/residual arrays

  template <class T>
  using VectorX = DLA::VectorS<D,T>; // vectors in global x,y coordinates

  template <class T>
  using VectorE = DLA::VectorS<D,T>; // vectors in local xi,eta coordinates

  template <class T>
  using Vector3 = DLA::VectorS<3,T>; // 3-vectors

  template <class T>
  using MatrixSymS3 = DLA::MatrixSymS<3,T>; // 3-symmetric matrix

  explicit VarInterpret3D() {}
  ~VarInterpret3D() {}

  VarInterpret3D( const VarInterpret3D& ) = delete;
  VarInterpret3D& operator=( const VarInterpret3D& );

  template<class T>
  void getPosition( const ArrayQ<T>& var, VectorX<T>& r ) const;

  template<class T>
  void getPositionRefDerivs( const ArrayQ<T>& var,
                             const ArrayQ<T>& varxi, const ArrayQ<T>& vareta,
                             VectorX<T>& rxi, VectorX<T>& reta) const;

  template<class T>
  void getPositionGrad( const ArrayQ<T>& var,
                        const ArrayQ<T>& varx, const ArrayQ<T>& vary, const ArrayQ<T>& varz,
                        VectorX<T>& rx, VectorX<T>& ry, VectorX<T>& rz) const;

  template<class T>
  void getForces( const ArrayQ<T>& var, Vector3<T>& floc ) const;

  template<class T>
  void getForces( const ArrayQ<T>& var, MatrixSymS3<T>& floc ) const;

  template<class T>
  void getMoments( const ArrayQ<T>& var, Vector3<T>& mloc ) const;

  template<class T>
  void getMoments( const ArrayQ<T>& var, MatrixSymS3<T>& mloc ) const;

  template<class T>
  void calc_ehat( const ArrayQ<T>& var, VectorX<T>& e1, VectorX<T>& e2, VectorX<T>& n ) const;

  template<class T>
  void calc_ehat_derivs( const ArrayQ<T>& var,
                         const ArrayQ<T>& varx, const ArrayQ<T>& vary, const ArrayQ<T>& varz,
                         VectorX<T>& e1,  VectorX<T>& e2,  VectorX<T>& n,
                         VectorX<T>& e1x, VectorX<T>& e1y, VectorX<T>& e1z,
                         VectorX<T>& e2x, VectorX<T>& e2y, VectorX<T>& e2z,
                         VectorX<T>& nx,  VectorX<T>& ny,  VectorX<T>& nz) const;

  // is state physically valid (for now, say true)
  bool isValidState( const ArrayQ<Real>& var ) const;

  // Set state vector from an input vector using log-quaternions
  template<class T>
  void setFromVector( ArrayQ<T>& var, const PositionLambdaForces3D& data ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

  static const int irx   = 0;
  static const int iry   = 1;
  static const int irz   = 2;
  static const int ilam1 = 3;
  static const int ilam2 = 4;
  static const int ilam3 = 5;
  static const int if11  = 6;
  static const int if22  = 7;
  static const int if12  = 8;
  static const int im11  = 9;
  static const int im22  = 10;
  static const int im12  = 11;


  // Machine espilon to test for zero in quaternian calculations
  static const Real machine_eps;
};


template <class T>
inline void
VarInterpret3D<VarTypeLambda>::getPosition(
    const ArrayQ<T>& var, VectorX<T>& r ) const
{
  r = {var[irx], var[iry], var[irz]};
}

template <class T>
inline void
VarInterpret3D<VarTypeLambda>::getPositionRefDerivs(
    const ArrayQ<T>& var, const ArrayQ<T>& varxi, const ArrayQ<T>& vareta,
    VectorX<T>& rxi, VectorX<T>& reta) const
{
  rxi  = {varxi[irx], varxi[iry], varxi[irz]};
  reta = {vareta[irx], vareta[iry], vareta[irz]};
}

template <class T>
inline void
VarInterpret3D<VarTypeLambda>::getPositionGrad(
    const ArrayQ<T>& var,
    const ArrayQ<T>& varx, const ArrayQ<T>& vary, const ArrayQ<T>& varz,
    VectorX<T>& rx, VectorX<T>& ry, VectorX<T>& rz) const
{
  rx = {varx[irx], varx[iry], varx[irz]};
  ry = {vary[irx], vary[iry], vary[irz]};
  rz = {varz[irx], varz[iry], varz[irz]};
}

template <class T>
inline void
VarInterpret3D<VarTypeLambda>::getForces(
    const ArrayQ<T>& var, Vector3<T>& floc ) const
{
  // floc = { f11, f22, f12 }
  floc = {var[if11], var[if22], var[if12]};
}

template <class T>
inline void
VarInterpret3D<VarTypeLambda>::getForces(
    const ArrayQ<T>& var, MatrixSymS3<T>& flocbar ) const
{
  // flocbar = {{ f11 },
  //            { f12, f22 },
  //            {   0,   0, 0 }}
  flocbar = {{var[if11]},
             {var[if12], var[if22]},
             {        0,         0,      0 }};
}

template <class T>
inline void
VarInterpret3D<VarTypeLambda>::getMoments(
    const ArrayQ<T>& var, Vector3<T>& mloc ) const
{
  // mloc = { m11, m22, m12 }
  mloc = {var[im11], var[im22], var[im12]};
}

template <class T>
inline void
VarInterpret3D<VarTypeLambda>::getMoments(
    const ArrayQ<T>& var, MatrixSymS3<T>& mlocbar ) const
{
  // mlocbar = {{ m11 },
  //            { m12, m22 },
  //            {   0,   0, 0 }}
  mlocbar = {{var[im11]},
             {var[im12], var[im22]},
             {        0,         0,      0 }};
}

template <class T>
inline void
VarInterpret3D<VarTypeLambda>::calc_ehat(
    const ArrayQ<T>& var, VectorX<T>& e1, VectorX<T>& e2, VectorX<T>& n ) const
{
  // Get the unit log-quaternion from state vector
  const T& lam1  = var[ilam1];
  const T& lam2  = var[ilam2];
  const T& lam3  = var[ilam3];

  T p0, p1, p2, p3;
  T mag2 = lam1*lam1 + lam2*lam2 + lam3*lam3;

  // Calculate quaternion components
  if ( mag2 < machine_eps )
  {
    // Use approximate expressions to avoid division by zero
    T sinm = 1 - 1./6.*mag2;

    p0 = 1 - 0.5*mag2;
    p1 = sinm*lam1;
    p2 = sinm*lam2;
    p3 = sinm*lam3;
  }
  else
  {
    // Use exact quaternion equations
    T mag = sqrt(mag2);
    T sinm = sin( mag )/mag;

    p0 = cos( mag );
    p1 = sinm*lam1;
    p2 = sinm*lam2;
    p3 = sinm*lam3;
  }

  // Deformed nodal basis vectors e1, e2, and n
  e1  = { 1-2*(p2*p2 + p3*p3),
            2*(p1*p2 + p0*p3),
            2*(p1*p3 - p0*p2) };

  e2  = {   2*(p1*p2 - p0*p3),
          1-2*(p1*p1 + p3*p3),
            2*(p2*p3 + p0*p1) };

  n   = {   2*(p1*p3 + p0*p2),
            2*(p2*p3 - p0*p1),
          1-2*(p1*p1 + p2*p2) };
}

template <class T>
inline void
VarInterpret3D<VarTypeLambda>::calc_ehat_derivs(
    const ArrayQ<T>& var,
    const ArrayQ<T>& varx, const ArrayQ<T>& vary, const ArrayQ<T>& varz,
    VectorX<T>& e1,  VectorX<T>& e2,  VectorX<T>& n,
    VectorX<T>& e1x, VectorX<T>& e1y, VectorX<T>& e1z,
    VectorX<T>& e2x, VectorX<T>& e2y, VectorX<T>& e2z,
    VectorX<T>& nx,  VectorX<T>& ny,  VectorX<T>& nz) const
{
  // Get unit log-quaternion and derivatives from state vector
  const T& lam1  = var[ilam1];
  const T& lam1x = varx[ilam1];
  const T& lam1y = vary[ilam1];
  const T& lam1z = varz[ilam1];

  const T& lam2  = var[ilam2];
  const T& lam2x = varx[ilam2];
  const T& lam2y = vary[ilam2];
  const T& lam2z = varz[ilam2];

  const T& lam3  = var[ilam3];
  const T& lam3x = varx[ilam3];
  const T& lam3y = vary[ilam3];
  const T& lam3z = varz[ilam3];


  T p0, p1, p2, p3;
  T p0x, p0y, p0z;
  T p1x, p1y, p1z;
  T p2x, p2y, p2z;
  T p3x, p3y, p3z;

  T mag2  = lam1*lam1 + lam2*lam2 + lam3*lam3;
  T mag2x = 2*(lam1x*lam1 + lam2x*lam2 + lam3x*lam3);
  T mag2y = 2*(lam1y*lam1 + lam2y*lam2 + lam3y*lam3);
  T mag2z = 2*(lam1z*lam1 + lam2z*lam2 + lam3z*lam3);

  // Calculate quaternion components
  if ( mag2 < machine_eps )
  {
    // Use approximate expressions to avoid division by zero

    T sinm  = 1 - 1./6.*mag2;
    T sinmx =   - 1./6.*mag2x;
    T sinmy =   - 1./6.*mag2y;
    T sinmz =   - 1./6.*mag2z;

    p0  = 1 - 0.5*mag2;
    p0x =   - 0.5*mag2x;
    p0y =   - 0.5*mag2y;
    p0z =   - 0.5*mag2z;

    p1  = sinm*lam1;
    p1x = sinmx*lam1 + sinm*lam1x;
    p1y = sinmy*lam1 + sinm*lam1y;
    p1z = sinmz*lam1 + sinm*lam1z;

    p2  = sinm*lam2;
    p2x = sinmx*lam2 + sinm*lam2x;
    p2y = sinmy*lam2 + sinm*lam2y;
    p2z = sinmz*lam2 + sinm*lam2z;

    p3  = sinm*lam3;
    p3x = sinmx*lam3 + sinm*lam3x;
    p3y = sinmy*lam3 + sinm*lam3y;
    p3z = sinmz*lam3 + sinm*lam3z;
  }
  else
  {
    // Use exact quaternion equations
    T mag = sqrt(mag2);
    T magx = mag2x/(0.5*mag);
    T magy = mag2y/(0.5*mag);
    T magz = mag2z/(0.5*mag);

    T sinm = sin( mag )/mag;
    T sinmx = cos( mag )*magx/mag - sinm*magx/mag;
    T sinmy = cos( mag )*magy/mag - sinm*magy/mag;
    T sinmz = cos( mag )*magz/mag - sinm*magz/mag;

    p0  =  cos( mag );
    p0x = -sin( mag )*magx;
    p0y = -sin( mag )*magy;
    p0z = -sin( mag )*magz;

    p1  = sinm*lam1;
    p1x = sinmx*lam1 + sinm*lam1x;
    p1y = sinmy*lam1 + sinm*lam1y;
    p1z = sinmz*lam1 + sinm*lam1z;

    p2  = sinm*lam2;
    p2x = sinmx*lam2 + sinm*lam2x;
    p2y = sinmy*lam2 + sinm*lam2y;
    p2z = sinmz*lam2 + sinm*lam2z;

    p3  = sinm*lam3;
    p3x = sinmx*lam3 + sinm*lam3x;
    p3y = sinmy*lam3 + sinm*lam3y;
    p3z = sinmz*lam3 + sinm*lam3z;
  }

  // Deformed nodal basis vectors e1 and e2, and derivatives
  e1  = { 1-2*(p2*p2  + p3*p3 ),   2*(p1*p2         + p0*p3        ),   2*(p1*p3         - p0*p2        ) };
  e1x = {  -4*(p2x*p2 + p3x*p3),   2*(p1x*p2+p1*p2x + p0x*p3+p0*p3x),   2*(p1x*p3+p1*p3x - p0x*p2-p0*p2x) };
  e1y = {  -4*(p2y*p2 + p3y*p3),   2*(p1y*p2+p1*p2y + p0y*p3+p0*p3y),   2*(p1y*p3+p1*p3y - p0y*p2-p0*p2y) };
  e1z = {  -4*(p2z*p2 + p3z*p3),   2*(p1z*p2+p1*p2z + p0z*p3+p0*p3z),   2*(p1z*p3+p1*p3z - p0z*p2-p0*p2z) };

  e2  = {   2*(p1*p2         - p0*p3        ), 1-2*(p1*p1  + p3*p3 ),   2*(p2*p3         + p0*p1        ) };
  e2x = {   2*(p1x*p2+p1*p2x - p0x*p3-p0*p3x),  -4*(p1x*p1 + p3x*p3),   2*(p2x*p3+p2*p3x + p0x*p1+p0*p1x) };
  e2y = {   2*(p1y*p2+p1*p2y - p0y*p3-p0*p3y),  -4*(p1y*p1 + p3y*p3),   2*(p2y*p3+p2*p3y + p0y*p1+p0*p1y) };
  e2z = {   2*(p1z*p2+p1*p2z - p0z*p3-p0*p3z),  -4*(p1z*p1 + p3z*p3),   2*(p2z*p3+p2*p3z + p0z*p1+p0*p1z) };

  n   = {   2*(p1*p3         + p0*p2        ),   2*(p2*p3         - p0*p1        ), 1-2*(p1*p1  + p2*p2 ) };
  nx  = {   2*(p1x*p3+p1*p3x + p0x*p2+p0*p2x),   2*(p2x*p3+p2*p3x - p0x*p1-p0*p1x),  -4*(p1x*p1 + p2x*p2) };
  ny  = {   2*(p1y*p3+p1*p3y + p0y*p2+p0*p2y),   2*(p2y*p3+p2*p3y - p0y*p1-p0*p1y),  -4*(p1y*p1 + p2y*p2) };
  nz  = {   2*(p1z*p3+p1*p3z + p0z*p2+p0*p2z),   2*(p2z*p3+p2*p3z - p0z*p1-p0*p1z),  -4*(p1z*p1 + p2z*p2) };
}

// is state physically valid: for now assume yes
inline bool
VarInterpret3D<VarTypeLambda>::isValidState( const ArrayQ<Real>& var ) const
{
  return true;
}

// Set state vector from an input vector using log-quaternions
template <class T>
inline void
VarInterpret3D<VarTypeLambda>::setFromVector(
    ArrayQ<T>& var, const PositionLambdaForces3D& data ) const
{
  var[irx] = data.rx;
  var[iry] = data.ry;
  var[irz] = data.rz;
  var[ilam1] = data.lam1;
  var[ilam2] = data.lam2;
  var[ilam3] = data.lam3;
  var[if11] = data.f11;
  var[if22] = data.f22;
  var[if12] = data.f12;
  var[im11] = data.m11;
  var[im22] = data.m22;
  var[im12] = data.m12;
}


}

#endif  // VAR2DLAMBDA_H
