// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef VAR2DTHETA_H
#define VAR2DTHETA_H

// solution interpreter class
// HSM2D: primitive input variables (rx, ry, theta, f11, f22, f12)

#include "tools/SANSnumerics.h"     // Real
#include "DenseLinAlg/StaticSize/VectorS.h"
#include "DenseLinAlg/StaticSize/MatrixS.h"
#include "Topology/Dimension.h"
#include "HSMVariable2D.h"

#include <cmath>              // sqrt, sin, cos
#include <iostream>

namespace SANS
{

//----------------------------------------------------------------------------//
// solution variable interpreter: HSM2D
// primitive variables {position, theta, forces}
//
// template parameters:
//   T                      solution DOF data type (e.g. Surreal)
//   VarType                state vector type (e.g. VarTypeQuaternion)
//
// member functions:
//   .getPosition           get position as a VectorS {rx, ry}
//   .getPositionRefDerivs  get derivatives of position wrt reference coords (xi,eta)
//   .getPositionGrad       get derivatives of position wrt global coords (x,y)
//   .getForces             get force resultants as a VectorS {f11, f22, f12}
//   .calcTbar              calculate Fbar                 (using theta from in-plane rotation)
//   .calcTbarDerivs        calcualte Fbar and derivatives (using theta from in-plane rotation)
//   .isValidState          T/F: determine if state is physically valid
//
//   .dump                  debug dump of private data
//----------------------------------------------------------------------------//


class VarTypeTheta;

// primary template
template <class VarType>
class VarInterpret2D;


template <>
class VarInterpret2D<VarTypeTheta>
{
public:
  typedef PhysD2 PhysDim;
  static const int D = PhysDim::D;            // physical dimensions
  static const int N = 6;                     // total solution variables

  template <class T>
  using ArrayQ = DLA::VectorS<N,T>;    // solution/residual arrays

  explicit VarInterpret2D() {}
  ~VarInterpret2D() {}

  VarInterpret2D( const VarInterpret2D& ) = delete;
  VarInterpret2D& operator=( const VarInterpret2D& );

  template<class T>
  void getPosition( const ArrayQ<T>& var, DLA::VectorS<D,T>& r ) const;

  template<class T>
  void getPositionRefDerivs( const ArrayQ<T>& var, const ArrayQ<T>& varxi, const ArrayQ<T>& vareta,
      DLA::VectorS<D,T>& rxi, DLA::VectorS<D,T>& reta) const;

  template<class T>
  void getPositionGrad( const ArrayQ<T>& var, const ArrayQ<T>& varx, const ArrayQ<T>& vary,
      DLA::VectorS<D,T>& rx, DLA::VectorS<D,T>& ry) const;

  template<class T>
  void getForces( const ArrayQ<T>& var, DLA::VectorS<3,T>& floc ) const;

  template<class T>
  void calcTbar( const ArrayQ<T>& var, DLA::MatrixS<D,D,T>& Tbar ) const;

  template<class T>
  void calcTbarDerivs( const ArrayQ<T>& var, const ArrayQ<T>& varx, const ArrayQ<T>& vary,
                       DLA::MatrixS<D,D,T>& Tbar, DLA::MatrixS<D,D,T>& Tbarx, DLA::MatrixS<D,D,T>& Tbary ) const;

  // is state physically valid (for now, say true)
  bool isValidState( const ArrayQ<Real>& var ) const;

  // Set state vector from an input vector using quaternions
  template<class T>
  void setFromVector( ArrayQ<T>& var, const PositionQuaternionForces2D& data ) const;

  // Set state vector from an input vector using angles
  template<class T>
  void setFromVector( ArrayQ<T>& var, const PositionAngleForces2D& data ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

};


template <class T>
inline void
VarInterpret2D<VarTypeTheta>::getPosition(
    const ArrayQ<T>& var, DLA::VectorS<D,T>& r ) const
{
  r = {var[0], var[1]};
}

template <class T>
inline void
VarInterpret2D<VarTypeTheta>::getPositionRefDerivs(
    const ArrayQ<T>& var, const ArrayQ<T>& varxi, const ArrayQ<T>& vareta,
    DLA::VectorS<D,T>& rxi, DLA::VectorS<D,T>& reta) const
{
  rxi  = {varxi[0], varxi[1]};
  reta = {vareta[0], vareta[1]};
}

template <class T>
inline void
VarInterpret2D<VarTypeTheta>::getPositionGrad(
    const ArrayQ<T>& var, const ArrayQ<T>& varx, const ArrayQ<T>& vary,
    DLA::VectorS<D,T>& rx, DLA::VectorS<D,T>& ry) const
{
  rx = {varx[0], varx[1]};
  ry = {vary[0], vary[1]};
}

template <class T>
inline void
VarInterpret2D<VarTypeTheta>::getForces(
    const ArrayQ<T>& var, DLA::VectorS<3,T>& floc ) const
{
  floc = {var[3], var[4], var[5]};
}

template <class T>
inline void
VarInterpret2D<VarTypeTheta>::calcTbar(
    const ArrayQ<T>& var, DLA::MatrixS<D,D,T>& Tbar ) const
{
  // Get theta from state vector
  const T& theta = var[2];

  // Rotation matrix Tbar
  Tbar = {{ cos(theta), -sin(theta) },
          { sin(theta),  cos(theta) }};
}

template <class T>
inline void
VarInterpret2D<VarTypeTheta>::calcTbarDerivs(
    const ArrayQ<T>& var, const ArrayQ<T>& varx, const ArrayQ<T>& vary,
    DLA::MatrixS<D,D,T>& Tbar, DLA::MatrixS<D,D,T>& Tbarx, DLA::MatrixS<D,D,T>& Tbary ) const
{
  // Get theta and derivs from state vector
  const T& theta = var[2];
  const T& thetax = varx[2];
  const T& thetay = vary[2];

  // Rotation matrix Tbar and derivs
  Tbar =  {{  cos(theta)       , -sin(theta)        },
           {  sin(theta)       ,  cos(theta)        }};
  Tbarx = {{ -sin(theta)*thetax, -cos(theta)*thetax },
           {  cos(theta)*thetax, -sin(theta)*thetax }};
  Tbary = {{ -sin(theta)*thetay, -cos(theta)*thetay },
           {  cos(theta)*thetay, -sin(theta)*thetay }};
}

// is state physically valid: for now assume yes
inline bool
VarInterpret2D<VarTypeTheta>::isValidState( const ArrayQ<Real>& var ) const
{
  return true;
}

// Set state vector from an input vector using quaternions
template <class T>
inline void
VarInterpret2D<VarTypeTheta>::setFromVector(
    ArrayQ<T>& var, const PositionQuaternionForces2D& data ) const
{
  var[0] = data.rx_;
  var[1] = data.ry_;
  var[2] = 2*acos( data.p0_ ); // Convert p0 to theta
  var[3] = data.f11_;
  var[4] = data.f22_;
  var[5] = data.f12_;
}

// Set state vector from an input vector using angles
template <class T>
inline void
VarInterpret2D<VarTypeTheta>::setFromVector(
    ArrayQ<T>& var, const PositionAngleForces2D& data ) const
{
  var[0] = data.rx_;
  var[1] = data.ry_;
  var[2] = data.theta_;
  var[3] = data.f11_;
  var[4] = data.f22_;
  var[5] = data.f12_;
}




}

#endif  // VAR2DTHETA_H
