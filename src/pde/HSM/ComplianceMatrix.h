// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef COMPLIANCEMATRIX_H

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"

namespace SANS
{

// Calculate extensional compliance matrix, Aij^(-1)
template<class T>
DLA::MatrixS<3,3,T> calcComplianceMatrix(const Real& E, const Real& nu, const T& t);

}

#endif
