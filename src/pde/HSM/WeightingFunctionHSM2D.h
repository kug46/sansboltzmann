// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef WEIGHTINGFUNCTIONHSM2D_H
#define WEIGHTINGFUNCTIONHSM2D_H

// HSM2D PDE: weighting functions for edge integrals

// Python must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <cmath> // pow, exp

#include "tools/SANSnumerics.h"     // Real

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "pde/HSM/PDEHSM2D.h"

namespace SANS
{


//----------------------------------------------------------------------------//
// Gaussian distribution in x0 with standard deviation s (sigma) and mean mu
//
class WeightingFunction2D_Gaussian_x0
{
public:

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> s{"s", NO_DEFAULT, NO_RANGE, "Standard deviation (sigma)"};
    const ParameterNumeric<Real> mu{"mu", NO_DEFAULT, NO_RANGE, "Mean (mu)"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit WeightingFunction2D_Gaussian_x0( PyDict d ) :
    s_(d.get(ParamsType::params.s)),
    mu_(d.get(ParamsType::params.mu)) {}

  WeightingFunction2D_Gaussian_x0(Real s, Real mu) : s_(s), mu_(mu) {}

  template<class T>
  Real operator()( const Real& x0, const Real& y0, const Real& time ) const
  {
    return pow( 2*pow(s_,2)*PI, -1./2. ) *
           exp( -pow(x0-mu_,2)/(2*pow(s_,2)) );
  }

private:
  Real s_, mu_;
};


//----------------------------------------------------------------------------//
// Gaussian distribution in y0 with standard deviation s (sigma) and mean mu
//
class WeightingFunction2D_Gaussian_y0
{
public:

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> s{"s", NO_DEFAULT, NO_RANGE, "Standard deviation (sigma)"};
    const ParameterNumeric<Real> mu{"mu", NO_DEFAULT, NO_RANGE, "Mean (mu)"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit WeightingFunction2D_Gaussian_y0( PyDict d ) :
    s_(d.get(ParamsType::params.s)),
    mu_(d.get(ParamsType::params.mu)) {}

  WeightingFunction2D_Gaussian_y0(Real s, Real mu) : s_(s), mu_(mu) {}

  template<class T>
  Real operator()( const Real& x0, const Real& y0, const Real& time ) const
  {
    return pow( 2*pow(s_,2)*PI, -1./2. ) *
           exp( -pow(y0-mu_,2)/(2*pow(s_,2)) );
  }

private:
  Real s_, mu_;
};



} // namespace SANS

#endif  // WEIGHTINGFUNCTIONHSM2D_H
