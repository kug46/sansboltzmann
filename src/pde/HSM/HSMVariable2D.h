// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef HSMVARIABLE2D_H
#define HSMVARIABLE2D_H

#include <initializer_list>

#include "tools/SANSnumerics.h" // Real
#include "tools/SANSException.h"

namespace SANS
{

template<class Derived>
struct HSMVariableType2D
{
  //A convenient method for casting to the derived type
  inline const Derived& cast() const { return static_cast<const Derived&>(*this); }
};


struct PositionLambdaForces2D : public HSMVariableType2D<PositionLambdaForces2D>
{
  PositionLambdaForces2D() {}
  PositionLambdaForces2D( const Real& rx, const Real& ry, const Real& lam3,
                          const Real& f11, const Real& f22, const Real& f12 )
    : rx(rx), ry(ry), lam3(lam3), f11(f11), f22(f22), f12(f12) {}
  PositionLambdaForces2D& operator=( const std::initializer_list<Real>& s )
  {
    SANS_ASSERT(s.size() == 6);
    auto q = s.begin();
    rx   = *q; q++;
    ry   = *q; q++;
    lam3 = *q; q++;
    f11  = *q; q++;
    f22  = *q; q++;
    f12  = *q;

    return *this;
  }

  Real rx;
  Real ry;
  Real lam3;
  Real f11;
  Real f22;
  Real f12;
};

}

#endif //HSMVARIABLE2D_H
