// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef HSMVARIABLE3D_H
#define HSMVARIABLE3D_H

#include <initializer_list>

#include "tools/SANSnumerics.h" // Real
#include "tools/SANSException.h"

namespace SANS
{

template<class Derived>
struct HSMVariableType3D
{
  //A convenient method for casting to the derived type
  inline const Derived& cast() const { return static_cast<const Derived&>(*this); }
};


struct PositionLambdaForces3D : public HSMVariableType3D<PositionLambdaForces3D>
{
  PositionLambdaForces3D() {}
  PositionLambdaForces3D( const Real& rx, const Real& ry, const Real& rz,
                          const Real& lam1, const Real& lam2, const Real& lam3,
                          const Real& f11, const Real& f22, const Real& f12,
                          const Real& m11, const Real& m22, const Real& m12)
    : rx(rx), ry(ry), rz(rz),
      lam1(lam1), lam2(lam2), lam3(lam3),
      f11(f11), f22(f22), f12(f12),
      m11(m11), m22(m22), m12(m12) {}

  PositionLambdaForces3D& operator=( const std::initializer_list<Real>& s )
  {
    SANS_ASSERT(s.size() == 12);
    auto q = s.begin();
    rx   = *q; q++;
    ry   = *q; q++;
    rz   = *q; q++;
    lam1 = *q; q++;
    lam2 = *q; q++;
    lam3 = *q; q++;
    f11  = *q; q++;
    f22  = *q; q++;
    f12  = *q; q++;
    m11  = *q; q++;
    m22  = *q; q++;
    m12  = *q;

    return *this;
  }

  Real rx;
  Real ry;
  Real rz;
  Real lam1;
  Real lam2;
  Real lam3;
  Real f11;
  Real f22;
  Real f12;
  Real m11;
  Real m22;
  Real m12;
};

}

#endif //HSMVARIABLE3D_H
