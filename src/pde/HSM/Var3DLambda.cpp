// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "Var3DLambda.h"
#include <ostream>
#include <limits>

namespace SANS
{

void
VarInterpret3D<VarTypeLambda>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "VarInterpret3D<VarTypeLambda>" << std::endl;
}

const Real VarInterpret3D<VarTypeLambda>::machine_eps = 100*std::numeric_limits<Real>::epsilon();
}
