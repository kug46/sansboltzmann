// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "Var2DLambda.h"
#include <ostream>

namespace SANS
{

void
VarInterpret2D<VarTypeLambda>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "VarInterpret2D<VarTypeLambda>" << std::endl;
}


}
