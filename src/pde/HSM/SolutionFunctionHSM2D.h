// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOLUTIONFUNCTIONHSM2D_H
#define SOLUTIONFUNCTIONHSM2D_H

// PDEHSM2D PDE: exact solution

// Python must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <cmath> // sqrt

#include "tools/SANSnumerics.h"     // Real

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "pde/HSM/PDEHSM2D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// solution: Linear Combination
// template for combining two solution functions to make another

template <class SOLN1, class SOLN2>
class SolutionFunction2D_Combination
{
public:

  template<class T>
  using ArrayQ = PDEHSM2D<VarTypeLambda>::ArrayQ<T>;

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> a{"a", 1, NO_RANGE, "Coefficient for Function 1"};
    const ParameterNumeric<Real> b{"b", 1, NO_RANGE, "Coefficient for Function 2"};
    const ParameterDict SolutionArgs1{"SolutionArgs1", EMPTY_DICT, SOLN1::ParamsType::checkInputs, "Solution function arguments 1"};
    const ParameterDict SolutionArgs2{"SolutionArgs2", EMPTY_DICT, SOLN2::ParamsType::checkInputs, "Solution function arguments 2"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit SolutionFunction2D_Combination( const PyDict& d ) :
      a_(d.get(ParamsType::params.a)),
      f1_(d.get(ParamsType::params.SolutionArgs1)),
      b_(d.get(ParamsType::params.b)),
      f2_(d.get(ParamsType::params.SolutionArgs1)) {}
  explicit SolutionFunction2D_Combination( const Real& a, const SOLN1& f1, const Real& b, const SOLN2& f2 ) :
      a_(a), f1_(f1), b_(b), f2_(f2) {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    return a_*f1_(x,y,time) + b_*f2_(x,y,time);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "SolutionFunction2D_Combination:  a_, b_ = " << a_ << ", " << b_ << std::endl;
  }

private:
  Real a_;
  SOLN1 f1_;
  Real b_;
  SOLN2 f2_;
};


//----------------------------------------------------------------------------//
// Solution: (mu*g)/(2*E) y0^2
// Bar hanging under its own weight (uni-axial loading) with Poisson ratio nu = 0
// Rotate the domain by angle theta (rotates CW)
//
class SolutionFunctionHSM2D_HangingBarZeroPoisson
{
public:
  typedef PhysD2 PhysDim;
  typedef PDEHSM2D<VarTypeLambda>::VectorX<Real> VectorX;
  typedef PDEHSM2D<VarTypeLambda>::MatrixX<Real> MatrixX;

  template<class T>
  using ArrayQ = PDEHSM2D<VarTypeLambda>::ArrayQ<T>;

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> mu{"mu", NO_DEFAULT, NO_RANGE, "Lumped shell density"};
    const ParameterNumeric<Real> g{"g", NO_DEFAULT, NO_RANGE, "Gravity"};
    const ParameterNumeric<Real> E{"E", NO_DEFAULT, NO_RANGE, "Young's Modulus"};
    const ParameterNumeric<Real> L{"L", NO_DEFAULT, NO_RANGE, "Length of bar"};
    const ParameterNumeric<Real> t{"t", NO_DEFAULT, NO_RANGE, "Plate thickness"};
    const ParameterNumeric<Real> theta{"theta", NO_DEFAULT, NO_RANGE, "Domain rotation angle"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit SolutionFunctionHSM2D_HangingBarZeroPoisson( const PyDict& d ) :
    mu_(d.get(ParamsType::params.mu)),
    g_(d.get(ParamsType::params.g)),
    E_(d.get(ParamsType::params.E)),
    L_(d.get(ParamsType::params.L)),
    t_(d.get(ParamsType::params.t)),
    theta_(d.get(ParamsType::params.theta)) {}

  SolutionFunctionHSM2D_HangingBarZeroPoisson(Real mu, Real g, Real E, Real L, Real t, Real theta) :
    mu_( mu ), g_( g ), E_( E ), L_( L ), t_( t ), theta_(theta) {}

  template<class T>
  ArrayQ<T> operator()( const T& x0, const T& y0, const T& time ) const
  {
    // Rotation matrices (R is CW, Rinv is CCW)
    MatrixX R = {{ cos(theta_), sin(theta_) },
                 {-sin(theta_), cos(theta_) }};
    MatrixX Rinv = {{ cos(theta_),-sin(theta_) },
                    { sin(theta_), cos(theta_) }};

    // De-rotate the grid
    VectorX r0 = {x0, y0};
    VectorX r0_untwist = Rinv * r0;
    Real x0u = r0_untwist[0];
    Real y0u = r0_untwist[1];

    // Analytic displacement and stress for untwisted grid
    Real k = 2*mu_*g_/(E_*t_);
    Real tmp1 = sqrt( 1 + k* L_ );
    Real tmp2 = sqrt( 1 + k*(L_-y0u) );
    Real coef = 2./(3.*k);

    VectorX r_untwist = { x0u, coef*( tmp1 - tmp2 + k*( y0u*tmp2 + L_*(tmp1-tmp2) )) };

    MatrixX f_untwist = {{ 0 ,         0         },
                         { 0 , mu_*g_*( L_-y0u ) }};

    // Rotate back to actual grid
    VectorX r = R * r_untwist;
    Real x = r[0];
    Real y = r[1];
    Real lam3 = 0;
    MatrixX f = R * f_untwist * Rinv;
    Real f11 = f(0,0);
    Real f22 = f(1,1);
    Real f12 = f(1,0);

    return { x, y, lam3, f11, f22, f12 };

  }

private:
  Real mu_, g_, E_, L_, t_, theta_;
};


// Other classes for reference below

//----------------------------------------------------------------------------//
// solution: const

class SolutionFunction2D_Const
{
public:

  template<class T>
  using ArrayQ = PDEHSM2D<VarTypeLambda>::ArrayQ<T>;

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> a0{"a0", NO_DEFAULT, NO_RANGE, "Constant of the function"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit SolutionFunction2D_Const( const PyDict& d ) : a0_(d.get(ParamsType::params.a0)) {}
  explicit SolutionFunction2D_Const( const Real& a0 ) : a0_(a0) {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    return a0_;
  }

private:
  Real a0_;
};


//----------------------------------------------------------------------------//
// solution: linear

class SolutionFunction2D_Linear
{
public:

  template<class T>
  using ArrayQ = PDEHSM2D<VarTypeLambda>::ArrayQ<T>;

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> a0{"a0", NO_DEFAULT, NO_RANGE, "Constant of the linear function"};
    const ParameterNumeric<Real> ax{"ax", NO_DEFAULT, NO_RANGE, "x-Linear of the linear function"};
    const ParameterNumeric<Real> ay{"ay", NO_DEFAULT, NO_RANGE, "y-Linear of the linear function"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit SolutionFunction2D_Linear( const PyDict& d ) :
    a0_(d.get(ParamsType::params.a0)), ax_(d.get(ParamsType::params.ax)), ay_(d.get(ParamsType::params.ay)) {}
  SolutionFunction2D_Linear( const Real& a0, const Real& ax, const Real& ay ) : a0_(a0), ax_(ax), ay_(ay) {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    return a0_ + ax_*x + ay_*y;
  }

private:
  const Real a0_, ax_, ay_;
};


//----------------------------------------------------------------------------//
// solution: monomial

class SolutionFunction2D_Monomial
{
public:

  template<class T>
  using ArrayQ = PDEHSM2D<VarTypeLambda>::ArrayQ<T>;

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<int> i{"i", NO_DEFAULT, NO_RANGE, "x-monomial exponent"};
    const ParameterNumeric<int> j{"j", NO_DEFAULT, NO_RANGE, "y-monomial exponent"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit SolutionFunction2D_Monomial( const PyDict& d ) :
      i_(d.get(ParamsType::params.i)), j_(d.get(ParamsType::params.j)) {}
  SolutionFunction2D_Monomial( int i, int j ) : i_(i), j_(j) {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    return pow(x, i_)*pow(y, j_);
  }

private:
  int i_, j_;
};


//----------------------------------------------------------------------------//
// (1-exp(a*x))*(1-exp(b*y))*(1-exp(c*(1-x)))*(1-exp(d*(1-y)));
//
class SolutionFunction2D_VarExp2
{
public:
  typedef PhysD2 PhysDim;

  template<class T>
  using ArrayQ = PDEHSM2D<VarTypeLambda>::ArrayQ<T>;

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> a{"a", NO_DEFAULT, NO_RANGE, "a exponent"};
    const ParameterNumeric<Real> b{"b", NO_DEFAULT, NO_RANGE, "b exponent"};
    const ParameterNumeric<Real> c{"c", NO_DEFAULT, NO_RANGE, "c exponent"};
    const ParameterNumeric<Real> d{"d", NO_DEFAULT, NO_RANGE, "d exponent"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit SolutionFunction2D_VarExp2( const PyDict& d ) :
    a_(d.get(ParamsType::params.a)),
    b_(d.get(ParamsType::params.b)),
    c_(d.get(ParamsType::params.c)),
    d_(d.get(ParamsType::params.d)) {}

  SolutionFunction2D_VarExp2(Real a, Real b, Real c, Real d) : a_(a), b_(b), c_(c), d_(d) {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    return (1-exp(a_*x))*(1-exp(b_*y))*(1-exp(c_*(1-x)))*(1-exp(d_*(1-y))) ;
  }

private:
  Real a_, b_, c_, d_;
};


} // namespace SANS

#endif  // SOLUTIONFUNCTIONHSM2D_H
