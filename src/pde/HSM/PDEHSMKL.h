// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDEHSMKL_H
#define PDEHSMKL_H

// 3-D Hybrid Shell Model PDE class

#include "tools/SANSnumerics.h"  // Real
#include "tools/Tuple.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"
#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/InverseLUP.h"
#include "Topology/ElementTopology.h"
#include "Topology/Dimension.h"
#include "Surreal/PromoteSurreal.h"
#include "Field/Tuple/ParamTuple.h"

#include "Var2DLambda.h"
#include "HSMVariable2D.h"

#include <iostream>
#include <string>

namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: 3-D Hybrid Shell Model (HSM) with Kirchoff-Love (KL) simplification
//
// Template parameters:
//   T                           Solution DOF data type (e.g. double)
//   Tr                          Geometry data type
//   Tc                          Lumped shell compliance matrix data type
//   Tm                          Lumped shell mass (mass/area density) data type
//   Tq                          Shell-following applied force/area data type
//
// Parameters (in ParamsType):
//   <0> AijInv                  Lumped shell compliance matrix
//   <1> mu                      Lumped shell mass (mass/area density)
//   <2> qe                      Shell-following applied force/area
//   <3> qx                      Fixed-direction (cartesian) applied force/area
//   <4> a                       Local acceleration data type
//
// Residual vector (R):
//  [0] Reps11                   R^(eps_11) : 1-component longitudinal strain residual
//  [1] Reps22                   R^(eps_22) : 2-component longitudinal strain residual
//  [2] Reps12                   R^(eps_12) : Planar shear strain residual
//  [3] Rkap11                   R^(eps_11) : 1-component curvature residual
//  [4] Rkap22                   R^(eps_22) : 2-component curvature residual
//  [5] Rkap12                   R^(eps_12) : Planar curvature residual
//  [6] Rf1                      R^(f_1)    : 1-component force equilibrium residual
//  [7] Rf2                      R^(f_2)    : 2-component force equilibrium residual
//  [8] Rfn                      R^(f_n)    : normal force equilibrium residual
//  [9] Rlam1                    R^(lam_1)  : 1-component normal vector constraint residual
//  [10] Rlam2                   R^(lam_2)  : 2-component normal vector constraint residual
//  [11] Rlam3                   R^(lam_3)  : In-plane rotation of local coordinates (ehat) constraint residual
//
// Global variables:
//   .g                          Gravity
//
// Member functions:
//   .hasFluxAdvectiveTime        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//   .hasFluxViscous             T/F: PDE has viscous flux term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: dU(Q)/dQ
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .fluxViscous                viscous fluxes: Fv(Q, QX)
//   .diffusionViscous           viscous diffusion coefficient: d(Fv)/d(UX)
//   .isValidState               T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom                 set from primitive variable array
//
// Protected helper functions:
//   .loc2cart                   convert a local matrix to a cartesian matrix
//   .cart2loc                   convert a cartesiam matrix to a local matrix
//   .calc_fbar
//   .calc_epsij_force           calculate the force dependent strain tensor from Section 4.2.3
//   .calc_epsloc_geom_metric    calculate the geometry dependent strain tensor from Section 4.2.1
//   .calc_epsloc_geom_cart      calculate the geometry dependent strain tensor from Section 4.2.2
//
//----------------------------------------------------------------------------//

template <class VarType>
class PDEHSMKL
{
public:

  enum HSMResidualInterpCategory
  {
    HSM_ResidInterp_Separate,
  };

  typedef PhysD2 PhysDim;
  static const int D = PhysDim::D; // physical dimensions
  static const int N = 6;          // total solution variables

  typedef VarInterpret2D<VarType> VarInterpret;  // solution variable interpreter type

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using ArrayQ = DLA::VectorS<N,T>; // solution/residual arrays

  template <class T>
  using MatrixQ = DLA::MatrixS<N,N,T>; // matrices

  template <class Tr>
  using VectorX = DLA::VectorS<PhysDim::D,Tr>; // vectors in global x,y,z coordinates

  template <class Tr>
  using VectorE = DLA::VectorS<TopoD2::D,Tr>; // vectors in local xi,eta coordinates

  template <class Tr>
  using MatrixX = DLA::MatrixS<PhysDim::D,PhysDim::D,Tr>; // matrices in global x,y,z coordinates

  template <class Tr>
  using MatrixSymX = DLA::MatrixSymS<PhysDim::D,Tr>; // symmetric matrices in global x,y,z coordinates

  template <class Tr>
  using MatrixE = DLA::MatrixS<TopoD2::D,TopoD2::D,Tr>; // matrices in local xi,eta coordinates

  template <class Tc>
  using ComplianceMatrix = DLA::MatrixS<3,3,Tc>; // Structural matrices (either stiffness or compliance)

// Build up the parameter tuple
protected:
  template<class Tc>
  using Param0 = ParamTuple< MatrixX<Real>, ComplianceMatrix<Tc>, TupleClass<>>;
  // < e0, AijInv >

  template<class Tc, class Tm>
  using Param1 = ParamTuple< Param0<Tc>, Tm, TupleClass<>>;
  // < e0, AijInv, mu >

  template<class Tc, class Tm, class Tq>
  using Param2 = ParamTuple< Param1<Tc,Tm>, VectorE<Tq>, TupleClass<>>;
  // < e0, AijInv, mu, qe >

  template<class Tc, class Tm, class Tq>
  using Param3 = ParamTuple< Param2<Tc,Tm,Tq>, VectorX<Real>, TupleClass<>>;
  // < e0, AijInv, mu, qe, qx >


public:

  static const int ie0     = 0;
  static const int iAijInv = 1;
  static const int imu     = 2;
  static const int iqe     = 3;
  static const int iqx     = 4;
  static const int ia      = 5;

  template<class Tc, class Tm, class Tq>
  using ParamsType = ParamTuple< Param3<Tc,Tm,Tq>, VectorX<Real>, TupleClass<>>;
  // < e0, AijInv, mu, qe, qx, a >

  PDEHSMKL( const VectorX<Real>& g,
         HSMResidualInterpCategory cat ):
   g_(g),
   cat_(cat)
  {
    // Nothing
  }

  ~PDEHSMKL() {}

  PDEHSMKL( const PDEHSMKL& ) = delete;
  PDEHSMKL& operator=( const PDEHSMKL& )  = delete;

  // right-hand-side forcing function
  template <class T, class Tc, class Tm, class Tq, class Tr>
  void forcingFunction( const ParamsType<Tc,Tm,Tq>& params,
                        const Tr& x0, const Tr& y0, const Real& time,
                        ArrayQ<T>& source ) const {}

  // characteristic speed (needed for timestep)
  template <class T, class Tc, class Tm, class Tq, class Tr>
  void speedCharacteristic( const ParamsType<Tc,Tm,Tq>& params,
                            const Tr& x0, const Tr& y0, const Real& time,
                            const Real& dx, const Real& dy, const ArrayQ<T>& var, Real& speed ) const {}

  // characteristic speed
  template <class T, class Tc, class Tm, class Tq, class Tr>
  void speedCharacteristic( const ParamsType<Tc,Tm,Tq>& params,
                            const Tr& x0, const Tr& y0, const Real& time,
                            const ArrayQ<T>& var, Real& speed ) const {}


  template<class T>
  void getPositionRefDerivs( const ArrayQ<T>& var,
                             const DLA::VectorS<TopoD2::D,ArrayQ<T>>& var_sRef,
                             DLA::VectorS<TopoD2::D,VectorX<T>>& r_sRef) const;

  template<class T>
  void calc_ehat( const ArrayQ<T>& var, MatrixX<T>& e ) const;

  // is state physically valid
  template <class T>
  bool isValidState( const ArrayQ<T>& var ) const { return true; }

  // set from variable array
  template<class T, class HSMVariable>
  void setDOFFrom( ArrayQ<T>& var, const HSMVariableType2D<HSMVariable>& data ) const;

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const;

  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const;

  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const;

  // return the interp type
  HSMResidualInterpCategory category() const;

  // how many residual equations are we dealing with
  int nMonitor() const;

  // set from variable array
  template<class HSMVariable>
  ArrayQ<Real> setDOFFrom( const HSMVariableType2D<HSMVariable>& data ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

  template <class T>
  inline void calc_fbar( const ArrayQ<T>& var, const MatrixX<T>& e,
                         MatrixSymX<T>& fbar) const;

  template <class T, class Tc, class Tm, class Tq>
  inline void calc_epsbar_force( const ParamsType<Tc,Tm,Tq>& params,
                                 const ArrayQ<T>& var,
                                 const MatrixX<T>& e,
                                 MatrixSymX< typename promote_Surreal<T,Tc>::type >& epsloc_force ) const;

#if 0
  template <class T>
  inline void calc_mbar( const ArrayQ<T>& var, const MatrixX<T>& e,
                         DLA::MatrixSymS<D,T>& mbar) const;
#endif

  template <class T, class Tc, class Tm, class Tq>
  inline void calc_load( const ParamsType<Tc,Tm,Tq>& params,
                         const MatrixX<T>& e,
                         VectorX< typename promote_Surreal<T,Tm,Tq>::type >& q ) const;

#if 0
  template <class T, class Tr>
  inline void calc_epsloc_geom_metric(
                        const VectorX<Tr>& r0xi, const VectorX<Tr>& r0eta,
                        const VectorX<Tr>& rxi,  const VectorX<Tr>& reta,
                        DLA::VectorS<3,T>& epsloc_geom ) const;
#endif

  template <class T>
  inline void calc_epsbar_geom(
                        const DLA::VectorS<TopoD2::D, VectorX<Real> >& r0_deriv,
                        const DLA::VectorS<TopoD2::D, VectorX<T> >& r_deriv,
                        MatrixSymX<T>& epsbar_geom ) const;

  template <class T, class Tr>
  inline void loc2cart( const MatrixX<T>& e,
                        const MatrixX<Tr>& matloc, MatrixX< typename promote_Surreal<T,Tr>::type >& matcart ) const;

  template <class T, class Tr>
  inline void loc2cart( const MatrixX<T>& e,
                        const MatrixSymX<Tr>& matloc, MatrixSymX< typename promote_Surreal<T,Tr>::type >& matcart ) const;

  template <class T>
  inline void cart2loc( const MatrixX<T>& e,
                        const MatrixX<T>& matcart, MatrixX<T>& matloc ) const;

  template <class T>
  inline void cart2loc( const MatrixX<T>& e,
                        const MatrixSymX<T>& matcart, MatrixSymX<T>& matloc ) const;

#if 0
  static const int iReps11 = 0;
  static const int iReps22 = 1;
  static const int iReps12 = 2;
  static const int iRkap11 = 3;
  static const int iRkap22 = 4;
  static const int iRkap12 = 5;
  static const int iRf1    = 6;
  static const int iRf2    = 7;
  static const int iRfn    = 8;
  static const int iRlam1  = 9;
  static const int iRlam2  = 10;
  static const int iRlam3  = 11;
#endif

  static const int iReps11 = 0;
  static const int iReps22 = 1;
  static const int iReps12 = 2;
  static const int iRf1    = 3;
  static const int iRf2    = 4;
  static const int iRlam3  = 5;

  const VarInterpret varInterpret_;  // solution variable interpreter class

  const VectorX<Real> g_; // gravity
  HSMResidualInterpCategory cat_;

};


template <class VarType>
template <class T>
inline void
PDEHSMKL<VarType>::getPositionRefDerivs( const ArrayQ<T>& var,
                                         const DLA::VectorS<TopoD2::D,ArrayQ<T>>& var_sRef,
                                         DLA::VectorS<TopoD2::D,VectorX<T>>& r_sRef) const
{
  varInterpret_.getPositionRefDerivs( var, var_sRef[0], var_sRef[1], r_sRef[0], r_sRef[1]);
}

template <class VarType>
template <class T>
inline void
PDEHSMKL<VarType>::calc_ehat( const ArrayQ<T>& var, MatrixX<T>& e ) const
{
  // Deformed local basis vectors
  VectorX<T> e1, e2;
  // Calculate ehat_1, ehat_2
  varInterpret_.calc_ehat( var, e1, e2 );
  // Local basis
  e = {{ e1[0], e2[0] },
       { e1[1], e2[1] }};
}


#if 0
template <class VarType>
template <class T, class Tc, class Tm, class Tq, class Tr>
inline void
PDEHSMKL<VarType>::masterState( const ParamsType<Tc,Tm,Tq>& params,
                                     const Tr& x0, const Tr& y0, const Real& time,
                                     const ArrayQ<T>& var, ArrayQ<T>& uCons ) const
{
  uCons += var;
}

// First term of residual equations [28]
//   Gives R^f1 = 3rd component of residual vector
//   Gives R^f2 = 4th component of residual vector
// fluxAdvective = (-ehat_i . fbar)
template <class VarType>
template <class T, class Tc, class Tm, class Tq, class Tr>
inline void
PDEHSMKL<VarType>::fluxAdvective( const ParamsType<Tc,Tm,Tq>& params,
                                  const Tr& x0, const Tr& y0, const Tr& z0, const Real& time,
                                  const ArrayQ<T>& var,
                                  ArrayQ< typename promote_Surreal<T,Tc,Tm,Tq,Tr>::type >& fx,
                                  ArrayQ< typename promote_Surreal<T,Tc,Tm,Tq,Tr>::type >& fy,
                                  ArrayQ< typename promote_Surreal<T,Tc,Tm,Tq,Tr>::type >& fz) const
{

  // Deformed local basis vectors
  VectorX<T> e1, e2, n;
  // Calculate ehat_1, ehat_2
  varInterpret_.calc_ehat( var, e1, e2, n );
  // Local basis
  MatrixX<T> e = {{ e1[0], e2[0], n[0] },
                  { e1[1], e2[1], n[1] },
                  { e1[2], e2[2], n[2] }};

  // Force resultant matrix
  DLA::MatrixSymS<D,T> fbar;
  // Calculate fbar
  calc_fbar(var, e, fbar);

  // Components of fbar
  T& fxx = fbar(0,0);
  T& fxy = fbar(1,0); T& fyy = fbar(1,1);
  T& fxz = fbar(2,0); T& fyz = fbar(2,1); T& fzz = fbar(2,2);

  // Components of ehat
  T& e1x = e1[0];
  T& e1y = e1[1];
  T& e1z = e1[2];

  T& e2x = e2[0];
  T& e2y = e2[1];
  T& e2z = e2[2];

  // Vector-tensor dot in [28]
  // R^f1 flux terms
  T e1fbarx = e1x*fxx + e1y*fxy + e1z*fxz;
  T e1fbary = e1x*fxy + e1y*fyy + e1y*fyz;
  T e1fbarz = e1x*fxz + e1y*fyz + e1z*fzz;

  // R^f2 flux terms
  T e2fbarx = e2x*fxx + e2y*fxy + e2z*fxz;
  T e2fbary = e2x*fxy + e2y*fyy + e2z*fyz;
  T e2fbarz = e2x*fxz + e2y*fyz + e2z*fzz;

  // The writeup includes a negation, but the
  // negation is associate with grad(W), not the pde flux.
  // Hence, no negation here.

  // Inhabit flux term corresponding to R^f1
  // x-component
  fx[Rf1] += e1fbarx;
  // y-component
  fy[Rf1] += e1fbary;
  // z-component
  fz[Rf1] += e1fbarz;

  // Inhabit flux term corresponding to R^f2
  // x-component
  fx[Rf2] += e2fbarx;
  // y-component
  fy[Rf2] += e2fbary;
  // z-component
  fz[Rf2] += e2fbarz;

}

template <class VarType>
template <class T, class Tc, class Tm, class Tq, class Tr>
void
PDEHSMKL<VarType>::fluxViscous(
    const ParamsType<Tc,Tm,Tq>& params,
    const Tr& x0, const Tr& y0, const Tr& z0, const Real& time,
    const ArrayQ<T>& var,
    const ArrayQ<T>& varx, const ArrayQ<T>& vary, const ArrayQ<T>& varz,
    ArrayQ<T>& fx, ArrayQ<T>& fy, ArrayQ<T>& fz ) const
{
  // Deformed local basis vectors
  VectorX<T> e1, e2, n;
  VectorX<T> e1x, e1y, e1z, e2x, e2y, e2z, nx, ny, nz;
  varInterpret_.calc_ehat_derivs( var,
                                  varx, vary, varz,
                                  e1, e2, n,
                                  e1x, e1y, e1z,
                                  e2x, e2y, e2z,
                                  nx,  ny,  nz);
  // Local basis
  MatrixX<T> e = {{ e1[0], e2[0], n[0] },
                  { e1[1], e2[1], n[1] },
                  { e1[2], e2[2], n[2] }};

  MatrixX<T> ex = {{ e1x[0], e2x[0], nx[0] },
                   { e1x[1], e2x[1], nx[1] },
                   { e1x[2], e2x[2], nx[2] }};

  MatrixX<T> ey = {{ e1y[0], e2y[0], ny[0] },
                   { e1y[1], e2y[1], ny[1] },
                   { e1y[2], e2y[2], ny[2] }};

  MatrixX<T> ez = {{ e1z[0], e2z[0], nz[0] },
                   { e1z[1], e2z[1], nz[1] },
                   { e1z[2], e2z[2], nz[2] }};

  DLA::MatrixSymS<D,T> mlocbar, mlocbarx, mlocbary, mlocbarz;
  varInterpret_.getMoments( var , mlocbar  );
  varInterpret_.getMoments( varx, mlocbarx );
  varInterpret_.getMoments( vary, mlocbary );
  varInterpret_.getMoments( varz, mlocbarz );

  // Moment resultant matrix
  DLA::MatrixSymS<D,T> mbarx, mbary, mbarz;

  // Calculate mbar gradients
  mbarx = ex * mlocbar * Transpose(e) + e * mlocbarx * Transpose(e) + e * mlocbar * Transpose(ex);
  mbary = ey * mlocbar * Transpose(e) + e * mlocbary * Transpose(e) + e * mlocbar * Transpose(ey);
  mbarz = ez * mlocbar * Transpose(e) + e * mlocbarz * Transpose(e) + e * mlocbar * Transpose(ez);

  // Inhabit flux term corresponding to R^fn
  // x-component
  fx[Rfn] += mbarx(0,0) + mbary(1,0) + mbarz(2,0);
  // y-component
  fy[Rfn] += mbarx(1,0) + mbary(1,1) + mbarz(2,1);
  // z-component
  fz[Rfn] += mbarx(2,0) + mbary(2,1) + mbarz(2,2);
}
#endif

// Rotate local matrix to cartesian
template <class VarType>
template <class T, class Tr>
inline void
PDEHSMKL<VarType>::loc2cart( const MatrixX<T>& e,
                             const MatrixX<Tr>& matloc, MatrixX< typename promote_Surreal<T,Tr>::type >& matcart ) const
{
  matcart = e * matloc * Transpose(e);
}

// Rotate symmetric local matrix to cartesian
template <class VarType>
template <class T, class Tr>
inline void
PDEHSMKL<VarType>::loc2cart( const MatrixX<T>& e,
                             const MatrixSymX<Tr>& matloc, MatrixSymX< typename promote_Surreal<T,Tr>::type >& matcart ) const
{
  matcart = e * matloc * Transpose(e);
}

// Rotate cartesian matrix to local
template <class VarType>
template <class T>
inline void
PDEHSMKL<VarType>::cart2loc( const MatrixX<T>& e,
                             const MatrixX<T>& matcart, MatrixX<T>& matloc ) const
{
  matloc = Transpose(e) * matcart * e;
}

// Rotate symmetric cartesian matrix to local
template <class VarType>
template <class T>
inline void
PDEHSMKL<VarType>::cart2loc( const MatrixX<T>& e,
                             const MatrixSymX<T>& matcart, MatrixSymX<T>& matloc ) const
{
  matloc = Transpose(e) * matcart * e;
}

// Calculate fbar, stress-resultant matrix in global Cartesian coordinates
template <class VarType>
template <class T>
inline void
PDEHSMKL<VarType>::calc_fbar( const ArrayQ<T>& var, const MatrixX<T>& e,
                              MatrixSymX<T>& fbar) const
{
  // Get stress resultants in local material axes from state vector
  MatrixSymX<T> flocbar;
  varInterpret_.getForces( var, flocbar );

  // Convert stress resultants to global coordinates for use in residual eqs
  loc2cart( e, flocbar, fbar );
}

#if 0
// Calculate mbar, moment-resultant matrix in global Cartesian coordinates
template <class VarType>
template <class T>
inline void
PDEHSMKL<VarType>::calc_mbar( const ArrayQ<T>& var, const MatrixX<T>& e,
                              DLA::MatrixSymS<D,T>& mbar) const
{
  // Get stress resultants in local material axes from state vector
  DLA::MatrixSymS<D,T> mlocbar;
  varInterpret_.getMoments( var, mlocbar );

  // Convert stress resultants to global coordinates for use in residual eqs
  loc2cart( e, mlocbar, mbar );
}
#endif

// Calculate the force dependent strain tensor
template <class VarType>
template <class T, class Tc, class Tm, class Tq>
inline void
PDEHSMKL<VarType>::calc_epsbar_force( const ParamsType<Tc,Tm,Tq>& params,
                                      const ArrayQ<T>& var,
                                      const MatrixX<T>& e,
                                      MatrixSymX< typename promote_Surreal<T,Tc>::type >& epsbar_force ) const
{
  typedef typename promote_Surreal<T,Tc>::type Ti;

  // Get compliance matrix from params
  const ComplianceMatrix<Tc>& AijInv = get<iAijInv>(params);

  // Get force resultants in local material axes from state vector
  DLA::VectorS<3,T> floc;
  varInterpret_.getForces( var, floc );

  // Force-dependent straings in local 1-2-n coordinates
  DLA::VectorS<3,Ti> epsloc = AijInv * floc;

  MatrixSymX<Ti> epslocbar = { {epsloc[0]},
                               {epsloc[2], epsloc[1]} };

  // Convert from local 1-2-n coordinates to Cartesian
  loc2cart( e, epslocbar, epsbar_force );
}


// Calculate the force dependent strain tensor
template <class VarType>
template <class T, class Tc, class Tm, class Tq>
inline void
PDEHSMKL<VarType>::calc_load( const ParamsType<Tc,Tm,Tq>& params,
                              const MatrixX<T>& e,
                              VectorX< typename promote_Surreal<T,Tm,Tq>::type >& q ) const
{
  // Get load terms from params
  const Tm& mu            = get<imu>(params);
  const VectorE<Tq>&   qe = get<iqe>(params);
  const VectorX<Real>& qx = get<iqx>(params);
  const VectorX<Real>& a  = get<ia>(params);

  // Calculate overall applied load in Cartesian axes
  q = qx + e * qe + mu*(g_-a);
}

#if 0
// Calculate the geometry dependent strain tensor from Section 4.2.1
template <class VarType>
template <class T, class Tr>
inline void
PDEHSMKL<VarType>::calc_epsloc_geom_metric( const VectorX<Tr>& r0xi, const VectorX<Tr>& r0eta,
                                         const VectorX<Tr>& rxi,  const VectorX<Tr>& reta,
                                         DLA::VectorS<3,T>& epsloc_geom ) const
{
  // Metric tensors gij, g0ij (Eq. 13,14)
  DLA::MatrixSymS<D,T> gij  = {{ dot(rxi,rxi) },
                               { dot(rxi,reta) , dot(reta,reta) }};

  DLA::MatrixSymS<D,Tr> g0ij = {{ dot(r0xi,r0xi) },
                                { dot(r0xi,r0eta) , dot(r0eta,r0eta) }};

  // Undeformed metric tensor inverse
  Tr a = g0ij(0,0);
  Tr b = g0ij(1,0);
  Tr d = g0ij(1,1);
  Tr InvDet_g0ij = 1.0 / (a*d-b*b);
  DLA::MatrixSymS<2,Tr> g0ijAdj = {{ d },{ -b, a }};
  DLA::MatrixSymS<2,Tr> g0ijInv = InvDet_g0ij * g0ijAdj;

  // Undeformed contravariant vectors (Eq. 16,17)
  VectorX<Tr> bxi0  = g0ijInv * r0xi;
  VectorX<Tr> beta0 = g0ijInv * r0eta;
  // Components of contravariant vectors
  Tr& bxi01  = bxi0[0];
  Tr& bxi02  = bxi0[1];
  Tr& beta01 = beta0[0];
  Tr& beta02 = beta0[1];

  // Strain tensor associated with the covariant tangential basis vectors (Eq. 7)
  //   (parametric derivates of the shell geometry function)
  DLA::MatrixSymS<D,T> epsbrv = 0.5 * (gij - g0ij);
  T& epsbrv11 = epsbrv(0,0);
  T& epsbrv21 = epsbrv(1,0);
  T& epsbrv22 = epsbrv(1,1);

  // Convert to 12 basis (Eq. 8)
  T eps11 = epsbrv11 * bxi01  * bxi01
          + epsbrv21 * bxi01  * beta01
          + epsbrv21 * beta01 * bxi01
          + epsbrv22 * beta01 * beta01;
  T eps21 = epsbrv11 * bxi02  * bxi01
          + epsbrv21 * bxi02  * beta01
          + epsbrv21 * beta02 * bxi01
          + epsbrv22 * beta02 * beta01;
  T eps22 = epsbrv11 * bxi02  * bxi02
          + epsbrv21 * bxi02  * beta02
          + epsbrv21 * beta02 * bxi02
          + epsbrv22 * beta02 * beta02;

  epsloc_geom = { eps11, eps22, eps21 };
}
#endif

// Calculate the geometry dependent strain tensor from Section 4.2.2
template <class VarType>
template <class T>
inline void
PDEHSMKL<VarType>::calc_epsbar_geom( const DLA::VectorS<TopoD2::D, VectorX<Real> >& r0_deriv,
                                     const DLA::VectorS<TopoD2::D, VectorX<T> >& r_deriv,
                                     MatrixSymX<T>& epsbar_geom ) const
{
  const VectorX<Real>& r0xi = r0_deriv[0];
  const VectorX<Real>& r0eta = r0_deriv[1];

  const VectorX<T>& rxi = r_deriv[0];
  const VectorX<T>& reta = r_deriv[1];

  // Geometry parametric derivatives
  MatrixX<Real> dr0 = {{ r0xi[0] , r0xi[1]  },
                       { r0eta[0], r0eta[1] }};
  MatrixX<T > dr  = {{ rxi[0]  , rxi[1]   },
                     { reta[0] , reta[1]  }};

#if 1
  // Lagrangian strain
  // ~> Deformed position surface gradient wrt undeformed geometry

  // Solve 2x2 system
  MatrixX<Real> dr0inv = DLA::InverseLUP::Inverse( dr0 );
  MatrixX<T > deltld0r = dr0inv * dr;

  // Undeformed geometry surface gradient is identity by construction
  DLA::MatrixSymS<D,Real> deltld0r0 = DLA::Identity();

  // Calculate strains in cartesian coordinates
  // epsbar_geom = 0.5 * (deltld0r * Transpose(deltld0r) - deltld0r0 * Transpose(deltld0r0));
  epsbar_geom = deltld0r * Transpose(deltld0r);
  epsbar_geom = 0.5 *(epsbar_geom - deltld0r0);

#else

  // Eulerian strain
  // ~> Undeformed position surface gradient wrt deformed geometry

  // Solve 2x2 system
  MatrixX<Tr> drinv = DLA::InverseLUP::Inverse( dr );
  MatrixX<T > deltldr0 = drinv * dr0;

  // Undeformed geometry surface gradient is identity by construction
  DLA::MatrixSymS<D,Tr> deltldr = DLA::Identity();

  // Calculate strains in cartesian coordinates
  // epsbar_geom = 0.5 * (deltldr * Transpose(deltldr) - deltldr0 * Transpose(deltldr0));
  epsbar_geom = 0.5 * (deltldr - deltldr0 * Transpose(deltldr0));

#endif
}

// set from variable array
template <class VarType>
template <class T, class HSMVariable>
inline void
PDEHSMKL<VarType>::setDOFFrom( ArrayQ<T>& var, const HSMVariableType2D<HSMVariable>& data ) const
{
  varInterpret_.setFromVector( var, data.cast() );
}

// set from variable array
template <class VarType>
template <class HSMVariable>
inline typename PDEHSMKL<VarType>::template ArrayQ<Real>
PDEHSMKL<VarType>::setDOFFrom( const HSMVariableType2D<HSMVariable>& data ) const
{
  ArrayQ<Real> var;
  varInterpret_.setFromVector( var, data.cast() );
  return var;
}

// interpret residuals of the solution variable
template <class VarType>
template <class T>
void
PDEHSMKL<VarType>::interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
{

  int nOut = rsdPDEOut.m();

  if (cat_ == HSM_ResidInterp_Separate) // separate residuals for all equations
  {
    SANS_ASSERT( nOut == PDEHSMKL<VarType>::N );
    for (int i = 0; i < PDEHSMKL<VarType>::N; i++)
      rsdPDEOut[i] = rsdPDEIn[i];
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );
  }
}

// interpret residuals of the gradients in the solution variable
template <class VarType>
template <class T>
void
PDEHSMKL<VarType>::interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const
{
  SANS_DEVELOPER_EXCEPTION( "HSMKL<VarType>::interpResidGradient not implemented" );
}

// interpret residuals at the boundary (should forward to BCs)
template <class VarType>
template <class T>
void
PDEHSMKL<VarType>::interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const
{
  int nOut = rsdBCOut.m();

  if (cat_ == HSM_ResidInterp_Separate) // separate residuals for all equations
  {
    SANS_ASSERT( nOut == PDEHSMKL<VarType>::N );
    for (int i = 0; i < PDEHSMKL<VarType>::N; i++)
      rsdBCOut[i] = rsdBCIn[i];
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );
  }
}

// return the interp type
template <class VarType>
typename PDEHSMKL<VarType>::HSMResidualInterpCategory
PDEHSMKL<VarType>::category() const
{
  return cat_;
}

// how many residual equations are we dealing with
template <class VarType>
int
PDEHSMKL<VarType>::nMonitor() const
{
  int nMon;

  if (cat_ == HSM_ResidInterp_Separate) // separate residuals for all equations
    nMon = PDEHSMKL<VarType>::N;
  else
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );

  return nMon;
}

} //namespace SANS

#endif  // PDEHSMKL_H
