// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCHSM2D.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{

// cppcheck-suppress passedByValue
void BCHSM2DParams<BCTypeDisplacements>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.rshiftx));
  allParams.push_back(d.checkInputs(params.rshifty));
  d.checkUnknownInputs(allParams);
}
BCHSM2DParams<BCTypeDisplacements> BCHSM2DParams<BCTypeDisplacements>::params;

// cppcheck-suppress passedByValue
void BCHSM2DParams<BCTypeForces>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.f1BC));
  allParams.push_back(d.checkInputs(params.f2BC));
  d.checkUnknownInputs(allParams);
}
BCHSM2DParams<BCTypeForces> BCHSM2DParams<BCTypeForces>::params;

//===========================================================================//
// Instantiate the BC parameters
BCPARAMETER_INSTANTIATE( BCHSM2DVector<VarTypeLambda> )

} //namespace SANS
