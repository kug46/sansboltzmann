// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "SolutionFunction2D.h"

namespace SANS
{

void SolutionFunction2D_Const::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.a0));
  d.checkUnknownInputs(allParams);
}
SolutionFunction2D_Const::ParamsType SolutionFunction2D_Const::ParamsType::params;

void SolutionFunction2D_Linear::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.a0));
  allParams.push_back(d.checkInputs(params.ax));
  allParams.push_back(d.checkInputs(params.ay));
  d.checkUnknownInputs(allParams);
}
SolutionFunction2D_Linear::ParamsType SolutionFunction2D_Linear::ParamsType::params;

void SolutionFunction2D_Monomial::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.i));
  allParams.push_back(d.checkInputs(params.j));
  d.checkUnknownInputs(allParams);
}
SolutionFunction2D_Monomial::ParamsType SolutionFunction2D_Monomial::ParamsType::params;

void SolutionFunction2D_VarExp2::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.a));
  allParams.push_back(d.checkInputs(params.b));
  allParams.push_back(d.checkInputs(params.c));
  allParams.push_back(d.checkInputs(params.d));
  d.checkUnknownInputs(allParams);
}
SolutionFunction2D_VarExp2::ParamsType SolutionFunction2D_VarExp2::ParamsType::params;

void SolutionFunctionHSM2D_HangingBarZeroPoisson::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.mu));
  allParams.push_back(d.checkInputs(params.g));
  allParams.push_back(d.checkInputs(params.E));
  allParams.push_back(d.checkInputs(params.L));
  allParams.push_back(d.checkInputs(params.t));
  allParams.push_back(d.checkInputs(params.theta));
  d.checkUnknownInputs(allParams);
}
SolutionFunctionHSM2D_HangingBarZeroPoisson::ParamsType SolutionFunctionHSM2D_HangingBarZeroPoisson::ParamsType::params;



}
