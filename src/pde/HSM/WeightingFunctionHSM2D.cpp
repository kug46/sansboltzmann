// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "WeightingFunctionHSM2D.h"

namespace SANS
{

void WeightingFunction2D_Gaussian_x0::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.s));
  allParams.push_back(d.checkInputs(params.mu));
  d.checkUnknownInputs(allParams);
}
WeightingFunction2D_Gaussian_x0::ParamsType WeightingFunction2D_Gaussian_0::ParamsType::params;

void WeightingFunction2D_Gaussian_y0::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.s));
  allParams.push_back(d.checkInputs(params.mu));
  d.checkUnknownInputs(allParams);
}
WeightingFunction2D_Gaussian_y0::ParamsType WeightingFunction2D_Gaussian_y0::ParamsType::params;

}
x
