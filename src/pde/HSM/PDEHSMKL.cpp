// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "pde/HSM/PDEHSMKL.h"

namespace SANS
{

template <class VarType>
void
PDEHSMKL<VarType>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PDEHSMKL" << std::endl;
}

// Explicitly instantiate for VarTypeLambda (Generate the functions for the log-quaternion-based class)
template class PDEHSMKL<VarTypeLambda>;

} //namespace SANS
