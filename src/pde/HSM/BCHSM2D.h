// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCHSM2D_H
#define BCHSM2D_H

// 2-D HSM BC class

//PyDict must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <boost/mpl/vector.hpp>

#include "tools/SANSnumerics.h"     // Real
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"
#include "pde/BCCategory.h"
#include "Surreal/PromoteSurreal.h"
#include "Topology/Dimension.h"
#include "pde/HSM/PDEHSM2D.h"

#include <iostream>
#include <string>

namespace SANS
{

//----------------------------------------------------------------------------//
// BC class: HSM 2D
//
// template parameters:
//   BCType               BC type (e.g. BCTypeDisplacements)
//----------------------------------------------------------------------------//

class BCTypeDisplacements;
class BCTypeForces;

template <class BCType, class PDEHSM2D>
class BCHSM2D;

template <class BCType>
struct BCHSM2DParams;


template<typename VarType>
using BCHSM2DVector = boost::mpl::vector2< BCHSM2D<BCTypeDisplacements, PDEHSM2D<VarType>>,
                                           BCHSM2D<BCTypeForces       , PDEHSM2D<VarType>>  >;

//----------------------------------------------------------------------------//
// Displacement BC:
//
// Specify:
// rshiftx - x-component of vector specifying position shift of node (Dirichlet BC)
// rshifty - y-component of vector specifying position shift of node (Dirichlet BC)
//
//----------------------------------------------------------------------------//

template<>
struct BCHSM2DParams<BCTypeDisplacements> : noncopyable
{
  const ParameterNumeric<Real> rshiftx{ "rshiftx", NO_DEFAULT, NO_RANGE,
                                       "x-component of position shift of node (Dirichlet BC)"
                                      };

  const ParameterNumeric<Real> rshifty{ "rshifty", NO_DEFAULT, NO_RANGE,
                                        "y-component of position shift of node (Dirichlet BC)"
                                      };

  static constexpr const char* BCName{"Displacements"};
  struct Option
  {
    const DictOption Displacements{BCHSM2DParams::BCName, BCHSM2DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCHSM2DParams params;
};


template <class VarType>
class BCHSM2D<BCTypeDisplacements, PDEHSM2D<VarType>> :
    public BCType< BCHSM2D<BCTypeDisplacements, PDEHSM2D<VarType>> >
{
public:
  typedef BCTypeDisplacements BCType;
  typedef typename BCCategory::Dirichlet_sansLG Category;
  typedef BCHSM2DParams<BCTypeDisplacements> ParamsType;

  typedef typename PDEHSM2D<VarType>::VarInterpret VarInterpret;  // solution variable interpreter type

  typedef PhysD2 PhysDim;
  static const int D = PhysDim::D; // physical dimensions
  static const int N = 6;          // total solution variables (x, y, lam3, f11, f22, f12)

  template <class T>
  using ArrayQ = DLA::VectorS<N,T>;      // solution/residual arrays

  template <class T>
  using VectorX = DLA::VectorS<D,T>;     // coordinate vectors (also used in dyadics)

  template <class T>
  using MatrixQ = DLA::MatrixS<N,N,T>;   // DOF matrices

  template <class T>
  using MatrixX = DLA::MatrixS<D,D,T>;   // coordinate matrices (used in dyadics)

  template<class Tc, class Tm, class Tq>
  using PDEParamsType = typename PDEHSM2D<VarType>::template ParamsType<Tc, Tm, Tq>;

  static const int NBC = 2; // total BCs

  // cppcheck-suppress noExplicitConstructor
  BCHSM2D( const PDEHSM2D<VarType>& pde, const Real& rshiftx, const Real& rshifty ) :
      pde_(pde),
      varInterpret_(pde_.varInterpret_),
      rshift_( {rshiftx, rshifty} ) {}
  ~BCHSM2D() {}

  // Python constructor
  BCHSM2D(const PDEHSM2D<VarType>& pde, PyDict& d ) :
    pde_(pde),
    varInterpret_(pde_.varInterpret_),
    rshift_( {d.get(ParamsType::params.rshiftx),
              d.get(ParamsType::params.rshifty)} ) {}

  BCHSM2D& operator=( const BCHSM2D& ) = delete;

  // BC residual: a(u) - b
  template <class T, class Tc, class Tm, class Tq, class Tr>
  void strongBC( const PDEParamsType<Tc, Tm, Tq>& param,
                   const Tr& x0,   const Tr& y0, const Real& time,
                   const Tr& x0xi, const Tr& x0eta,
                   const Tr& y0xi, const Tr& y0eta,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& var,
                   const ArrayQ<T>& varx, const ArrayQ<T>& vary,
                   const ArrayQ<T>& varxi, const ArrayQ<T>& vareta,
                   ArrayQ< typename promote_Surreal<T,Tc,Tm,Tq,Tr>::type >& rsdBC ) const;

  template <class T>
  void strongBC( const Real& x0, const Real& y0, const Real& time, const Real& nx, const Real& ny,
                 const ArrayQ<T>& var, const ArrayQ<T>& varx, const ArrayQ<T>& vary, const ArrayQ<T>& lg, MatrixQ<T>& rsdBC ) const
  {
    // TODO: implement...
    SANS_DEVELOPER_EXCEPTION("not implemented...");
  }


  // conventional formulation BC weighting function
  // B^t = Fn M A^t ( A M A^t )^{-1}
  template <class T, class Tc, class Tm, class Tq, class Tr>
  void weightBC( const PDEParamsType<Tc, Tm, Tq>& param,
                   const Tr& x0,   const Tr& y0, const Real& time,
                   const Tr& x0xi, const Tr& x0eta,
                   const Tr& y0xi, const Tr& y0eta,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& var,
                   const ArrayQ<T>& varx, const ArrayQ<T>& vary,
                   const ArrayQ<T>& varxi, const ArrayQ<T>& vareta,
                   MatrixQ< typename promote_Surreal<T,Tc,Tm,Tq,Tr>::type >& wghtBC ) const;

  // Lagrange multiplier weighting function
  // \bar{A}^t = Fn N
  template <class T>
  void weightLagrange( const Real& x0, const Real& y0, const Real& time, const Real& nx, const Real& ny,
                       const ArrayQ<T>& var, const ArrayQ<T>& varx, const ArrayQ<T>& vary, MatrixQ<T>& wghtLG ) const {}

  // Lagrange multiplier rhs
  // B = ( N^t M^{-1} N )^{-1} N^t M^{-1}
  template <class T>
  void rhsLagrange( const Real& x0, const Real& y0, const Real& time, const Real& nx, const Real& ny,
                    const ArrayQ<T>& var, const ArrayQ<T>& varx, const ArrayQ<T>& vary, ArrayQ<T>& rhsLG ) const {}

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const {}

protected:
  const PDEHSM2D<VarType>& pde_;
  const VarInterpret& varInterpret_; // solution variable interpreter class
  const VectorX<Real> rshift_; // Specified shift in node location (Dirichlet BC)
};


template <class VarType>
template <class T, class Tc, class Tm, class Tq, class Tr>
inline void
BCHSM2D<BCTypeDisplacements, PDEHSM2D<VarType>>::strongBC(
    const PDEParamsType<Tc, Tm, Tq>& param,
    const Tr& x0,   const Tr& y0, const Real& time,
    const Tr& x0xi, const Tr& x0eta,
    const Tr& y0xi, const Tr& y0eta,
    const Real& nx, const Real& ny,
    const ArrayQ<T>& var,
    const ArrayQ<T>& varx, const ArrayQ<T>& vary,
    const ArrayQ<T>& varxi, const ArrayQ<T>& vareta,
    ArrayQ< typename promote_Surreal<T,Tc,Tm,Tq,Tr>::type >& rsdBC ) const
{
  // rshift_ = { rx_shift_BC, ry_shift_BC }
  //
  //          | x - x0 - rx_shift_BC |
  //          | y - y0 - ry_shift_BC |
  // rsdBC_ = |            0         |
  //          |            0         |
  //          |            0         |
  //          |            0         |

  // Components of rshift
  Real rx_shift_BC = rshift_[0];
  Real ry_shift_BC = rshift_[1];

  VectorX<T> r;
  varInterpret_.getPosition( var, r );
  T x = r[0];
  T y = r[1];

  // Only inhabit first two indices of 6-vector
  rsdBC = 0;
  rsdBC(0) = x - (x0 + rx_shift_BC); // x-component of strong BC in [76a,b,c,f]
  rsdBC(1) = y - (y0 + ry_shift_BC); // y-component of strong BC in [76a,b,c,f]

  // Currently this is only set up for a single location specification along the entire boundary
  // Eventually implement a boundary profile that uses x/xmax and y/ymax as parameters
  //   ~> Use the fact that xi is along x0 and eta is along y0

}

template <class VarType>
template <class T, class Tc, class Tm, class Tq, class Tr>
inline void
BCHSM2D<BCTypeDisplacements, PDEHSM2D<VarType>>::weightBC(
    const PDEParamsType<Tc, Tm, Tq>& param,
    const Tr& x0,   const Tr& y0, const Real& time,
    const Tr& x0xi, const Tr& x0eta,
    const Tr& y0xi, const Tr& y0eta,
    const Real& nx, const Real& ny,
    const ArrayQ<T>& var,
    const ArrayQ<T>& varx, const ArrayQ<T>& vary,
    const ArrayQ<T>& varxi, const ArrayQ<T>& vareta,
    MatrixQ< typename promote_Surreal<T,Tc,Tm,Tq,Tr>::type >& wghtBC ) const
{
  // wghtBC = | wx(R_0), wy(R_0), 0, 0, 0, 0 | <~ R^eps11 residual equation BC weights
  //          | wx(R_1), wy(R_1), 0, 0, 0, 0 | <~ R^eps22 residual equation BC weights
  //          | wx(R_2), wy(R_2), 0, 0, 0, 0 | <~ R^eps12 residual equation BC weights
  //          | wx(R_3), wy(R_3), 0, 0, 0, 0 | <~ R^fx    residual equation BC weights
  //          | wx(R_4), wy(R_4), 0, 0, 0, 0 | <~ R^fy    residual equation BC weights
  //          | wx(R_5), wy(R_5), 0, 0, 0, 0 | <~ R^b     residual equation BC weights
  //
  // \hat{t} = { nx, ny }
  //
  //   Assuming xi is along x0 and eta is along y0
  //     ~> The unit vectors along xi and eta are the same as the rx0 and ry0 unit vectors
  //          xi_hat  = x_hat = {1,0}
  //          eta_hat = y_hat = {0,1}
  //     ~> The Jacobian is the product of maximum x and y components of r0 (since xi_max = eta_max = 1)
  //          J0 = r0x_max/xi_max * r0y_max/eta_max
  //             = r0x_max * r0ymax
  //     ~> xi derivates are found with the chain rule
  //          d_xi() = d_x() d_xi(x) + d_y() + d_xi(y)
  //                 = d_x() xmax    + 0
  //     ~> Parametric derivatives of undeformed geometry are just the scaling factors
  //          d_xi(r0) = {xmax,0}       d_eta(r0) = {0,ymax}

  // Element boundary normal vector
  VectorX<T> t = { nx, ny };

  // Dyadics
  VectorX<T> i = { 1, 0 };
  VectorX<T> j = { 0, 1 };
  MatrixX<T> ii = {{ 1, 0 },{ 0, 0 }};
  MatrixX<T> jj = {{ 0, 0 },{ 0, 1 }};
  MatrixX<T> ij = {{ 0, 1 },{ 1, 0 }};

  // Deformed local basis vectors
  VectorX<T> e1, e2;
  // Calculate ehat_1, ehat_2
  varInterpret_.calc_ehat( var, e1, e2 );
  // Local basis
  MatrixX<T> e = {{ e1[0], e2[0] },
                  { e1[1], e2[1] }};
  MatrixX<T> eT = Transpose(e);

  // Position vector gradient
  VectorX<T> x, y;
  varInterpret_.getPositionGrad( var, varx, vary, x, y );

  // Jacobian of element coordinates xi,eta, expressed as 1/J0^2 (Eq.36)
  Tr J0 = fabs( x0xi*y0eta - x0eta*y0xi );

  // In-plane rotation constraint

  // Dotting with xi_hat just takes first index (see 76f \hat{\xi} \cdot \hat{t})
  Real coef = nx/J0;

  // Postion derivatives w.r.t. geometry xi, eta
  // Deformed
  VectorX<T> rxi, reta;
  varInterpret_.getPositionRefDerivs( var, varxi, vareta, rxi, reta );
  // Undeformed
  VectorX<Tr> r0xi   = { x0xi , y0xi };
  VectorX<Tr> r0eta  = { x0eta, y0eta };

  // Geometry-dependent strains
  DLA::VectorS<3,T> epsloc_geom;
  pde_.calc_epsloc_geom_cart( e, r0xi, r0eta, rxi, reta, epsloc_geom );

  // Break into components
  T& eps11r = epsloc_geom[0];
  T& eps22r = epsloc_geom[1];
  T& eps12r = epsloc_geom[2];

  // Undeformed material line vector
  VectorX<T> s0 = r0xi;

  // Use geometry-residual-defined strains in calculating s-tilde (fewer Jacobian elements)
  DLA::MatrixSymS<D,T> epsbar  = {{ 1+eps11r },
                                  {  eps12r   , 1+eps22r }};
  VectorX<T> s0tld = epsbar * s0;


  // Use [149] to solve cross-product of ( n_hat . (s0_tld x emat^T) )
  // n_hat = k_hat in [149], premultiplication by k_hat reduces [149] to
  // i(Ca-Ab) + j(Da-Bb)

  // Decompose s0_tld and emat^T
  T& A = eT(0,0);
  T& B = eT(0,1);
  T& C = eT(1,0);
  T& D = eT(1,1);
  T& a = s0tld[0];
  T& b = s0tld[1];

  // n_hat . (s0_tld x emat^T)
  VectorX<T> dotcross = { C*a-A*b, D*a-B*b }; // [149]

  // BC weight vectors
  // Note: these weights get multiplied by the N_k basis functions in
  // src/Discretization/Galerkin/IntegrandBoundaryTrace_Dirichlet_sansLG_Galerkin.h as "phi[k]"
#if 0   // original that works
  MatrixX<T> deltld0r = {{ x[0], x[1]},
                         { y[0], y[1] }};

  DLA::MatrixS<1,2,T> tmp0 = Transpose(t) * e * ii * eT * deltld0r;
  DLA::MatrixS<1,2,T> tmp1 = Transpose(t) * e * jj * eT * deltld0r;
  DLA::MatrixS<1,2,T> tmp2 = Transpose(t) * e * ij * eT * deltld0r;

  VectorX<T> wBCvec0 = Transpose( tmp0 );     // R^eps11 [76a]
  VectorX<T> wBCvec1 = Transpose( tmp1 );     // R^eps22 [76b]
  VectorX<T> wBCvec2 = Transpose( tmp2 );     // R^eps12 [76c]
  VectorX<T> wBCvec3 = 0;                          // R^f1    [76d]
  VectorX<T> wBCvec4 = 0;                          // R^f2    [76e]
  VectorX<T> wBCvec5 = coef * dotcross;            // R^e3    [76f]
#elif 1   // original transposed in a cleaner form

  MatrixX<T> deltld0r = {{ x[0], y[0]},
                         { x[1], y[1] }};

  VectorX<T> wBCvec0 = deltld0r * e * ii * eT * t; // R^eps11 [76a]
  VectorX<T> wBCvec1 = deltld0r * e * jj * eT * t; // R^eps22 [76b]
  VectorX<T> wBCvec2 = deltld0r * e * ij * eT * t; // R^eps12 [76c]
  VectorX<T> wBCvec3 = 0;                          // R^f1    [76d]
  VectorX<T> wBCvec4 = 0;                          // R^f2    [76e]
  VectorX<T> wBCvec5 = coef * dotcross;            // R^e3    [76f]

#else   // SRA: hacked and really ugly
  DLA::MatrixS<1,2,T> tT = {{ nx, ny }};    // boundary normal written as row matrix
  DLA::MatrixS<1,2,T> tmp0, tmp1, tmp2;

  MatrixX<T> deltld0r = {{ x[0], x[1]},
                         { y[0], y[1] }};

  tmp0 = tT * e * ii * eT * deltld0r; // R^eps11 [76a]
  tmp1 = tT * e * jj * eT * deltld0r; // R^eps22 [76b]
  tmp2 = tT * e * ij * eT * deltld0r; // R^eps12 [76c]

  VectorX<T> wBCvec0 = { tmp0(0,0), tmp0(0,1) };
  VectorX<T> wBCvec1 = { tmp1(0,0), tmp1(0,1) };
  VectorX<T> wBCvec2 = { tmp2(0,0), tmp2(0,1) };
  VectorX<T> wBCvec3 = 0;                              // R^f1    [76d]
  VectorX<T> wBCvec4 = 0;                              // R^f2    [76e]
  VectorX<T> wBCvec5 = coef * dotcross;                // R^e3    [76f]
#endif

  // BC weights
  wghtBC = 0;
  //     x-component                   y-component
  wghtBC(0,0) = wBCvec0[0];     wghtBC(0,1) = wBCvec0[1];
  wghtBC(1,0) = wBCvec1[0];     wghtBC(1,1) = wBCvec1[1];
  wghtBC(2,0) = wBCvec2[0];     wghtBC(2,1) = wBCvec2[1];
  wghtBC(3,0) = wBCvec3[0];     wghtBC(3,1) = wBCvec3[1];
  wghtBC(4,0) = wBCvec4[0];     wghtBC(4,1) = wBCvec4[1];
  wghtBC(5,0) = wBCvec5[0];     wghtBC(5,1) = wBCvec5[1];
}



//----------------------------------------------------------------------------//
// Force BC:
//
// Specify:
// f1BC - Local 1-direction component of imposed force vector (Dirichlet BC)
// f2BC - Local 2-direction component of imposed force vector (Dirichlet BC)
//
//----------------------------------------------------------------------------//

template<>
struct BCHSM2DParams<BCTypeForces> : noncopyable
{

    const ParameterNumeric<Real> f1BC{ "f1BC", NO_DEFAULT, NO_RANGE,
                                       "imposed 1-direction extensional local force (Dirichlet BC)"
                                     };

    const ParameterNumeric<Real> f2BC{ "f2BC", NO_DEFAULT, NO_RANGE,
                                       "imposed 2-direction extensional local force (Dirichlet BC)"
                                     };

    static constexpr const char* BCName{"Forces"};
    struct Option
    {
      const DictOption Forces{BCHSM2DParams::BCName, BCHSM2DParams::checkInputs};
    };

    static void checkInputs(PyDict d);

    static BCHSM2DParams params;
};


template <class VarType>
class BCHSM2D<BCTypeForces, PDEHSM2D<VarType>> :
    public BCType< BCHSM2D<BCTypeForces, PDEHSM2D<VarType>> >
{
public:
  typedef PhysD2 PhysDim;
  typedef BCTypeForces BCType;
  typedef typename BCCategory::Dirichlet_sansLG Category;
  typedef BCHSM2DParams<BCTypeForces> ParamsType;

  typedef typename PDEHSM2D<VarType>::VarInterpret VarInterpret;  // solution variable interpreter type

  static const int D = PhysDim::D; // physical dimensions
  static const int N = 6;          // total solution variables (x, y, lam3, f11, f22, f12)

  template <class T>
  using ArrayQ = DLA::VectorS<N,T>;        // solution/residual arrays

  template <class T>
  using VectorX = DLA::VectorS<D,T>;       // Cartesian-coordinate vectors (also used in dyadics)

  template <class T>
  using VectorE = DLA::VectorS<D,T>;       // local-coordinate vectors

  template <class T>
  using MatrixQ = DLA::MatrixS<N,N,T>;     // DOF matrices

  template <class T>
  using MatrixX = DLA::MatrixS<D,D,T>;     // coordinate matrices (used in dyadics)

  template <class T>
  using MatrixSymX = DLA::MatrixSymS<D,T>; // force/stress matrices (symmetric)

  template<class Tc, class Tm, class Tq>
  using PDEParamsType = typename PDEHSM2D<VarType>::template ParamsType<Tc, Tm, Tq>;

  static const int NBC = 2; // total BCs

  // cppcheck-suppress noExplicitConstructor
  BCHSM2D( const PDEHSM2D<VarType>& pde, const Real& f1BC, const Real& f2BC ) :
    pde_(pde),
    varInterpret_(pde_.varInterpret_),
    flocBC_( {f1BC, f2BC} ) {}
  ~BCHSM2D() {}

  // Python constructor
  BCHSM2D( const PDEHSM2D<VarType>& pde, PyDict& d ) :
    pde_(pde),
    varInterpret_(pde_.varInterpret_),
    flocBC_( {d.get(ParamsType::params.f1BC),
              d.get(ParamsType::params.f2BC)} ) {}

  BCHSM2D& operator=( const BCHSM2D& ) = delete;

  // BC residual: a(u) - b
  template <class T, class Tc, class Tm, class Tq, class Tr>
  void strongBC( const PDEParamsType<Tc, Tm, Tq>& param,
                 const Tr& x0,   const Tr& y0, const Real& time,
                 const Tr& x0xi, const Tr& x0eta,
                 const Tr& y0xi, const Tr& y0eta,
                 const Real& nx, const Real& ny,
                 const ArrayQ<T>& var,
                 const ArrayQ<T>& varx, const ArrayQ<T>& vary,
                 const ArrayQ<T>& varxi, const ArrayQ<T>& vareta,
                 ArrayQ< typename promote_Surreal<T,Tc,Tm,Tq,Tr>::type >& rsdBC ) const;

  template <class T>
  void strongBC( const Real& x0, const Real& y0, const Real& time, const Real& nx, const Real& ny,
                 const ArrayQ<T>& var, const ArrayQ<T>& varx, const ArrayQ<T>& vary, const ArrayQ<T>& lg, MatrixQ<T>& rsdBC ) const {}


  // conventional formulation BC weighting function
  // B^t = Fn M A^t ( A M A^t )^{-1}
  template <class T, class Tc, class Tm, class Tq, class Tr>
  void weightBC( const PDEParamsType<Tc, Tm, Tq>& param,
                 const Tr& x0,   const Tr& y0, const Real& time,
                 const Tr& x0xi, const Tr& x0eta,
                 const Tr& y0xi, const Tr& y0eta,
                 const Real& nx, const Real& ny,
                 const ArrayQ<T>& var,
                 const ArrayQ<T>& varx, const ArrayQ<T>& vary,
                 const ArrayQ<T>& varxi, const ArrayQ<T>& vareta,
                 MatrixQ< typename promote_Surreal<T,Tc,Tm,Tq,Tr>::type >& wghtBC ) const;

  // Lagrange multiplier weighting function
  // \bar{A}^t = Fn N
  template <class T>
  void weightLagrange( const Real& x0, const Real& y0, const Real& time, const Real& nx, const Real& ny,
                       const ArrayQ<T>& var, const ArrayQ<T>& varx, const ArrayQ<T>& vary, MatrixQ<T>& wghtLG ) const {}

  // Lagrange multiplier rhs
  // B = ( N^t M^{-1} N )^{-1} N^t M^{-1}
  template <class T>
  void rhsLagrange( const Real& x0, const Real& y0, const Real& time, const Real& nx, const Real& ny,
                    const ArrayQ<T>& var, const ArrayQ<T>& varx, const ArrayQ<T>& vary, ArrayQ<T>& rhsLG ) const {}

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const {}

protected:
  const PDEHSM2D<VarType>& pde_;
  const VarInterpret& varInterpret_; // solution variable interpreter class
  const VectorE<Real> flocBC_; // Imposed local forces (1,2 axes)  (Dirichlet BC)
};


template <class VarType>
template <class T, class Tc, class Tm, class Tq, class Tr>
inline void
BCHSM2D<BCTypeForces, PDEHSM2D<VarType>>::strongBC(
    const PDEParamsType<Tc, Tm, Tq>& param,
    const Tr& x0,   const Tr& y0, const Real& time,
    const Tr& x0xi, const Tr& x0eta,
    const Tr& y0xi, const Tr& y0eta,
    const Real& nx, const Real& ny,
    const ArrayQ<T>& var,
    const ArrayQ<T>& varx, const ArrayQ<T>& vary,
    const ArrayQ<T>& varxi, const ArrayQ<T>& vareta,
    ArrayQ< typename promote_Surreal<T,Tc,Tm,Tq,Tr>::type >& rsdBC ) const
{
  // flocBC_ = | f1_BC |
  //           | f2_BC |
  //
  //          | f1^(II) - f1^(II)_BC |
  //          | f2^(II) - f2^(II)_BC |
  // rsdBC_ = |           0          |
  //          |           0          |
  //          |           0          |
  //          |           0          |
  //
  // \hat{t} = { nx, ny }

  // Element boundary normal vector
  VectorX<T> t = { nx, ny };

  // Deformed local basis vectors
  VectorX<T> e1, e2;
  // Calculate ehat_1, ehat_2
  varInterpret_.calc_ehat( var, e1, e2 );
  // Local basis
  MatrixX<T> e = {{ e1[0], e2[0] },
                  { e1[1], e2[1] }};
  MatrixX<T> eT = Transpose(e);

  // Convert local force BC matrix in 1,2 axes (f12) to global matrix in x,y axes (fxy)
  //MatrixSymX<T> fxyBC;
  //MatrixSymX<T> flocBC(flocBC_);
  //pde_.loc2cart( e, flocBC, fxyBC );

  // Get global force matrix
  MatrixSymX<T> fxy;
  pde_.calc_fbar( var, e, fxy );

  // Calculate f^(II) = e^T.fxy.t and break into components
  VectorX<T> fII = eT * fxy * t;
  T fII1 = fII[0];
  T fII2 = fII[1];

  // Only inhabit first two indices of 6-vector
  rsdBC = 0;
  rsdBC(0) = fII1 - flocBC_[0]; // 1-component of strong BC in [76d,e]
  rsdBC(1) = fII2 - flocBC_[1]; // 2-component of strong BC in [76d,e]

  // Currently this is only set up for a single location specification along the entire boundary
  // Eventually implement a boundary profile that uses x/xmax and y/ymax as parameters
  //   ~> Use the fact that xi is along x0 and eta is along y0
}

template <class VarType>
template <class T, class Tc, class Tm, class Tq, class Tr>
inline void
BCHSM2D<BCTypeForces, PDEHSM2D<VarType>>::weightBC(
    const PDEParamsType<Tc, Tm, Tq>& param,
    const Tr& x0,   const Tr& y0, const Real& time,
    const Tr& x0xi, const Tr& x0eta,
    const Tr& y0xi, const Tr& y0eta,
    const Real& nx, const Real& ny,
    const ArrayQ<T>& var,
    const ArrayQ<T>& varx, const ArrayQ<T>& vary,
    const ArrayQ<T>& varxi, const ArrayQ<T>& vareta,
    MatrixQ< typename promote_Surreal<T,Tc,Tm,Tq,Tr>::type >& wghtBC ) const
{
  // Unit vectors
  VectorX<T> ihat = { 1, 0 };
  VectorX<T> jhat = { 0, 1 };

  // BC weight vectors
  // Note: these weights get multiplied by the N_k basis functions in
  // src/Discretization/Galerkin/IntegrandBoundaryTrace_Dirichlet_sansLG_Galerkin.h as "phi[k]"
  VectorX<T> wBCvec0 = 0;    // R^eps11 [76a]
  VectorX<T> wBCvec1 = 0;    // R^eps22 [76b]
  VectorX<T> wBCvec2 = 0;    // R^eps12 [76c]
  VectorX<T> wBCvec3 = ihat; // R^f1    [76d]
  VectorX<T> wBCvec4 = jhat; // R^f2    [76e]
  VectorX<T> wBCvec5 = 0;    // R^e3    [76f]

  // BC weights
  wghtBC = 0;
  //     x-component                   y-component
  wghtBC(0,0) = wBCvec0[0];     wghtBC(0,1) = wBCvec0[1];
  wghtBC(1,0) = wBCvec1[0];     wghtBC(1,1) = wBCvec1[1];
  wghtBC(2,0) = wBCvec2[0];     wghtBC(2,1) = wBCvec2[1];
  wghtBC(3,0) = wBCvec3[0];     wghtBC(3,1) = wBCvec3[1];
  wghtBC(4,0) = wBCvec4[0];     wghtBC(4,1) = wBCvec4[1];
  wghtBC(5,0) = wBCvec5[0];     wghtBC(5,1) = wBCvec5[1];
}

} //namespace SANS

#endif  // BCHSM2D_H
