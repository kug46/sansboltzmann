// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef HSM2D_H
#define HSM2D_H

// 2-D Hybrid Shell Model PDE class

#include "tools/SANSnumerics.h"  // Real
#include "tools/Tuple.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"
#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/InverseLUP.h"
#include "Topology/ElementTopology.h"
#include "Topology/Dimension.h"
#include "Surreal/PromoteSurreal.h"
#include "Field/Tuple/ParamTuple.h"

#include "Var2DLambda.h"
#include "HSMVariable2D.h"

#include <iostream>
#include <string>

namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: 2-D Hybrid Shell Model (HSM)
//
// Template parameters:
//   T                           Solution DOF data type (e.g. double)
//   Tr                          Geometry data type
//   Tc                          Lumped shell compliance matrix data type
//   Tm                          Lumped shell mass (mass/area density) data type
//   Tq                          Shell-following applied force/area data type
//
// Parameters (in ParamsType):
//   <0> AijInv                  Lumped shell compliance matrix
//   <1> mu                      Lumped shell mass (mass/area density)
//   <2> qe                      Shell-following applied force/area
//   <3> qx                      Fixed-direction (cartesian) applied force/area
//   <4> a                       Local acceleration data type
//
// Nodal state vector (var):
//  [0] rx                       x-component of deformed position vector
//  [1] ry                       y-component of deformed position vector
//  [2] lam3                     nonzero component of log-quaternion deformed orientation
//  [3] f11                      1-component longitudinal stress resultant (local material coords)
//  [4] f22                      2-component longitudinal stress resultant (local material coords)
//  [5] f12                      Planar shear stress resultant (local material coords)
//
// Residual vector (R):
//  [0] Reps11                   R^(eps_11) : 1-component longitudinal strain residual (Eq 3 top)
//  [1] Reps22                   R^(eps_22) : 2-component longitudinal strain residual (Eq 3 middle)
//  [2] Reps12                   R^(eps_12) : Planar shear strain residual (Eq 3 bottom)
//  [3] Rf1                      R^(f_1)    : 1-component force equilibrium residual (Eq 13 top)
//  [4] Rf2                      R^(f_2)    : 2-component force equilibrium residual (Eq 13 bottom)
//  [5] Re3                      R^(e_3)    : In-plane rotation of local coordinates (ehat) constraint residual (Eq 24)
//
// Global variables:
//   .g                          Gravity
//
// Member functions:
//   .hasFluxAdvectiveTime        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//   .hasFluxViscous             T/F: PDE has viscous flux term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: dU(Q)/dQ
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .fluxViscous                viscous fluxes: Fv(Q, QX)
//   .diffusionViscous           viscous diffusion coefficient: d(Fv)/d(UX)
//   .isValidState               T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom                 set from primitive variable array
//
// Protected helper functions:
//   .loc2cart                   convert a local matrix to a cartesian matrix
//   .cart2loc                   convert a cartesiam matrix to a local matrix
//   .calc_fbar
//   .calc_epsij_force           calculate the force dependent strain tensor from Section 4.2.3
//   .calc_epsloc_geom_metric    calculate the geometry dependent strain tensor from Section 4.2.1
//   .calc_epsloc_geom_cart      calculate the geometry dependent strain tensor from Section 4.2.2
//
//----------------------------------------------------------------------------//

template <class VarType>
class PDEHSM2D
{
public:

  enum HSMResidualInterpCategory
  {
    HSM_ResidInterp_Separate,
  };

  typedef PhysD2 PhysDim;
  static const int D = PhysDim::D; // physical dimensions
  static const int N = 6;          // total solution variables (rx, ry, lam3, f11, f22, f12)

  typedef VarInterpret2D<VarType> VarInterpret;  // solution variable interpreter type

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using ArrayQ = DLA::VectorS<N,T>; // solution/residual arrays

  template <class T>
  using MatrixQ = DLA::MatrixS<N,N,T>; // matrices

  template <class Tr>
  using VectorX = DLA::VectorS<D,Tr>; // vectors in global x,y coordinates

  template <class Tr>
  using VectorE = DLA::VectorS<D,Tr>; // vectors in local xi,eta coordinates

  template <class Tr>
  using MatrixX = DLA::MatrixS<D,D,Tr>; // matrices in global x,y coordinates

  template <class Tr>
  using MatrixE = DLA::MatrixS<D,D,Tr>; // matrices in local xi,eta coordinates

  template <class Tc>
  using ComplianceMatrix = DLA::MatrixS<3,3,Tc>; // Structural matrices (either stiffness or compliance)

// Build up the parameter tuple
protected:
  template<class Tc, class Tm>
  using Param1 = ParamTuple< ComplianceMatrix<Tc>, Tm, TupleClass<>>;
  // < AijInv , mu >

  template<class Tc, class Tm, class Tq>
  using Param2 = ParamTuple< Param1<Tc,Tm>, VectorE<Tq>, TupleClass<>>;
  // < AijInv , mu, qe >

  template<class Tc, class Tm, class Tq>
  using Param3 = ParamTuple< Param2<Tc,Tm,Tq>, VectorX<Real>, TupleClass<>>;
  // < AijInv , mu, qe, qx >


public:

  static const int iAijInv = 0;
  static const int imu     = 1;
  static const int iqe     = 2;
  static const int iqx     = 3;
  static const int ia      = 4;

  template<class Tc, class Tm, class Tq>
  using ParamsType = ParamTuple< Param3<Tc,Tm,Tq>, VectorX<Real>, TupleClass<>>;
  // < AijInv , mu, qe, qx, a >

  PDEHSM2D( const VectorX<Real>& g,
         HSMResidualInterpCategory cat ):
   g_(g),
   cat_(cat)
  {
    // Nothing
  }

  ~PDEHSM2D() {}

  PDEHSM2D( const PDEHSM2D& ) = delete;
  PDEHSM2D& operator=( const PDEHSM2D& )  = delete;

  // flux components
  bool hasFluxAdvectiveTime() const { return true; }
  bool hasFluxAdvective() const { return true; }
  bool hasFluxViscous() const { return false; }
  bool hasSource() const { return true; }
  bool hasSourceTrace() const { return false; }
  bool hasForcingFunction() const { return false; }

  bool needsSolutionGradientforSource() const { return true; }

  // unsteady temporal flux: Ft(Q)
  template <class T, class Tc, class Tm, class Tq, class Tr>
  void fluxAdvectiveTime( const ParamsType<Tc,Tm,Tq>& params,
                          const Tr& x0, const Tr& y0, const Real& time,
                          const ArrayQ<T>& var, ArrayQ<T>& ft ) const
  {
    ArrayQ<T>& ftTemp = 0;
    masterState(params, x0, y0, time, var, ftTemp);
    ft += ftTemp;
  }

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <class T>
  void jacobianFluxAdvectiveTime(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& J ) const
  {
    J += DLA::Identity();
  }

  // master state: U(Q)
  template <class T, class Tc, class Tm, class Tq, class Tr>
  void masterState( const ParamsType<Tc,Tm,Tq>& params,
                         const Tr& x0, const Tr& y0, const Real& time,
                         const ArrayQ<T>& var, ArrayQ<T>& uCons ) const;

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <class T, class Tc, class Tm, class Tq, class Tr>
  void jacobianMasterState( const ParamsType<Tc,Tm,Tq>& params,
                            const Tr& x0, const Tr& y0, const Real& time,
                            const ArrayQ<T>& var, MatrixQ<T>& dudq ) const;

  // advective flux: F(Q)
  template <class Tv, class Tc, class Tm, class Tq, class Tr, class Tf>
  void fluxAdvective( const ParamsType<Tc,Tm,Tq>& params,
                      const Tr& x0, const Tr& y0, const Real& time,
                      const ArrayQ<Tv>& var,
                      ArrayQ<Tf>& fx, ArrayQ<Tf>& fy ) const;

  template <class Tv, class Tc, class Tm, class Tq, class Tr, class Tf>
  void fluxAdvective( const ParamsType<Tc,Tm,Tq>& params,
                      const Tr& x0, const Tr& y0, const Real& time,
                      const ArrayQ<Tv>& varL, const ArrayQ<Tv>& varR,
                      const Tr& nx, const Tr& ny, ArrayQ<Tf>& f ) const {}

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class T, class Tc, class Tm, class Tq, class Tr>
  void jacobianFluxAdvective( const ParamsType<Tc,Tm,Tq>& params,
                              const Tr& x0, const Tr& y0, const Real& time,
                              const ArrayQ<T>& var, MatrixQ<T>& ax, MatrixQ<T>& ay ) const {}

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class T, class Tc, class Tm, class Tq, class Tr>
  void jacobianFluxAdvectiveAbsoluteValue( const ParamsType<Tc,Tm,Tq>& params,
                                           const Tr& x0, const Tr& y0, const Real& time,
                                           const ArrayQ<T>& var, const Tr& nx, const Tr& ny,
                                           MatrixQ<T>& a ) const {}

  // strong form advective fluxes
  template <class T, class Tc, class Tm, class Tq, class Tr>
  void strongFluxAdvective( const ParamsType<Tc,Tm,Tq>& params,
                            const Tr& x0, const Tr& y0, const Real& time,
                            const ArrayQ<T>& var, const ArrayQ<T>& varx, const ArrayQ<T>& vary,
                            ArrayQ<T>& strongPDE ) const {}

  // viscous flux: Fv(Q, QX)
  template <class T, class Tc, class Tm, class Tq, class Tr>
  void fluxViscous( const ParamsType<Tc,Tm,Tq>& params,
                    const Tr& x0, const Tr& y0, const Real& time,
                    const ArrayQ<T>& var, const ArrayQ<T>& varx, const ArrayQ<T>& vary,
                    ArrayQ<T>& f, ArrayQ<T>& g ) const {}

  template <class T, class Tc, class Tm, class Tq, class Tr>
  void fluxViscous( const ParamsType<Tc,Tm,Tq>& params,
                    const Tr& x0, const Tr& y0, const Real& time,
                    const ArrayQ<T>& varL, const ArrayQ<T>& varxL, const ArrayQ<T>& varyL,
                    const ArrayQ<T>& varR, const ArrayQ<T>& varxR, const ArrayQ<T>& varyR,
                    const Tr& nx, const Tr& ny, ArrayQ<T>& f ) const {}

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class T, class Tc, class Tm, class Tq, class Tr>
  void diffusionViscous( const ParamsType<Tc,Tm,Tq>& params,
                         const Tr& x0, const Tr& y0, const Real& time,
                         const ArrayQ<T>& var, const ArrayQ<T>& varx, const ArrayQ<T>& vary,
                         MatrixQ<T>& kxx, MatrixQ<T>& kxy,
                         MatrixQ<T>& kyx, MatrixQ<T>& kyy ) const {}

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class T, class Tc, class Tm, class Tq, class Tr>
  void jacobianFluxViscous( const ParamsType<Tc,Tm,Tq>& params,
                            const Tr& x0, const Tr& y0, const Real& time,
                            const ArrayQ<T>& var, MatrixQ<T>& ax, MatrixQ<T>& ay ) const {}

  // strong form viscous fluxes
  template <class T, class Tc, class Tm, class Tq, class Tr>
  void strongFluxViscous( const ParamsType<Tc,Tm,Tq>& params,
                          const Tr& x0, const Tr& y0, const Real& time,
                          const ArrayQ<T>& var, const ArrayQ<T>& varx, const ArrayQ<T>& vary,
                          const ArrayQ<T>& varxx, const ArrayQ<T>& varxy, const ArrayQ<T>& varyy,
                          ArrayQ<T>& strongPDE ) const {}

  // solution-dependent source: S(X, Xxi, Q, QX, Qxi)
  template <class Tv, class Tg, class Tx, class Tc, class Tm, class Tq, class Tr, class Ts>
  void source( const ParamsType<Tc,Tm,Tq>& params,
               const Tr& x0, const Tr& y0, const Real& time,
               const Tr& xxi, const Tr& xeta,
               const Tr& yxi, const Tr& yeta,
               const ArrayQ<Tv>& var,
               const ArrayQ<Tg>& varx, const ArrayQ<Tg>& vary,
               const ArrayQ<Tx>& varxi, const ArrayQ<Tx>& vareta,
               ArrayQ<Ts>& source ) const;

  // dual-consistent source
  template <class T, class Tc, class Tm, class Tq, class Tr>
  void sourceTrace( const ParamsType<Tc,Tm,Tq>& params,
                    const Tr& x0L, const Tr& y0L,
                    const Tr& x0R, const Tr& y0R, const Real& time,
                    const ArrayQ<T>& varL, const ArrayQ<T>& varxL, const ArrayQ<T>& varyL,
                    const ArrayQ<T>& varR, const ArrayQ<T>& varxR, const ArrayQ<T>& varyR,
                    ArrayQ<T>& sourceL, ArrayQ<T>& sourceR ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class T, class Tc, class Tm, class Tq, class Tr>
  void jacobianSource( const ParamsType<Tc,Tm,Tq>& params,
                       const Tr& x0, const Tr& y0, const Real& time,
                       const ArrayQ<T>& var, const ArrayQ<T>& varx, const ArrayQ<T>& vary,
                       MatrixQ<T>& dsdu ) const {}

  // right-hand-side forcing function
  template <class T, class Tc, class Tm, class Tq, class Tr>
  void forcingFunction( const ParamsType<Tc,Tm,Tq>& params,
                        const Tr& x0, const Tr& y0, const Real& time,
                        ArrayQ<T>& source ) const {}

  // characteristic speed (needed for timestep)
  template <class T, class Tc, class Tm, class Tq, class Tr>
  void speedCharacteristic( const ParamsType<Tc,Tm,Tq>& params,
                            const Tr& x0, const Tr& y0, const Real& time,
                            const Real& dx, const Real& dy, const ArrayQ<T>& var, Real& speed ) const {}

  // characteristic speed
  template <class T, class Tc, class Tm, class Tq, class Tr>
  void speedCharacteristic( const ParamsType<Tc,Tm,Tq>& params,
                            const Tr& x0, const Tr& y0, const Real& time,
                            const ArrayQ<T>& var, Real& speed ) const {}

  // is state physically valid
  template <class T>
  bool isValidState( const ArrayQ<T>& var ) const { return true; }

  // update fraction needed for physically valid state
  template <class T, class Tc, class Tm, class Tq, class Tr>
  void updateFraction( const ParamsType<Tc,Tm,Tq>& params,
                       const Tr& x0, const Tr& y0, const Real& time,
                       const ArrayQ<T>& var, const ArrayQ<T>& dvar,
                       Real maxChangeFraction, Real& updateFraction ) const {}

  // set from variable array
  template<class T, class HSMVariable>
  void setDOFFrom( ArrayQ<T>& var, const HSMVariableType2D<HSMVariable>& data ) const;

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const;

  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const;

  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const;

  // return the interp type
  HSMResidualInterpCategory category() const;

  // how many residual equations are we dealing with
  int nMonitor() const;

  // set from variable array
  template<class HSMVariable>
  ArrayQ<Real> setDOFFrom( const HSMVariableType2D<HSMVariable>& data ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

  template <class Tv, class Te, class T>
  inline void calc_fbar( const ArrayQ<Tv>& var, const MatrixX<Te>& e,
                         DLA::MatrixSymS<D,T>& fbar) const;

  template <class Tv, class Tc, class Tm, class Tq, class T>
  inline void calc_epsloc_force( const ParamsType<Tc,Tm,Tq>& params,
                                 const ArrayQ<Tv>& var,
                                 DLA::VectorS<3,T>& epsloc_force ) const;

#if 0
  template <class T, class Tr>
  inline void calc_epsloc_geom_metric(
                        const VectorX<Tr>& r0xi, const VectorX<Tr>& r0eta,
                        const VectorX<Tr>& rxi,  const VectorX<Tr>& reta,
                        DLA::VectorS<3,T>& epsloc_geom ) const;
#endif

  template <class Te, class Tr, class Tx, class T>
  inline void calc_epsloc_geom_cart(
                        const MatrixX<Te>& e,
                        const VectorX<Tr>& r0xi, const VectorX<Tr>& r0eta,
                        const VectorX<Tx>& rxi,  const VectorX<Tx>& reta,
                        DLA::VectorS<3,T>& epsloc_geom ) const;

  template <class T, class Tr>
  inline void loc2cart( const MatrixX<T>& e,
                        const MatrixX<Tr>& matloc, MatrixX<T>& matcart ) const;

  template <class T, class Tr>
  inline void loc2cart( const MatrixX<T>& e,
                        const DLA::MatrixSymS<D,Tr>& matloc, DLA::MatrixSymS<D,T>& matcart ) const;

  template <class T>
  inline void cart2loc( const MatrixX<T>& e,
                        const MatrixX<T>& matcart, MatrixX<T>& matloc ) const;

  template <class T>
  inline void cart2loc( const MatrixX<T>& e,
                        const DLA::MatrixSymS<D,T>& matcart, DLA::MatrixSymS<D,T>& matloc ) const;

  const VarInterpret varInterpret_;  // solution variable interpreter class

  const VectorX<Real> g_; // gravity
  HSMResidualInterpCategory cat_;

};

template <class VarType>
template <class T, class Tc, class Tm, class Tq, class Tr>
inline void
PDEHSM2D<VarType>::masterState( const ParamsType<Tc,Tm,Tq>& params,
                                const Tr& x0, const Tr& y0, const Real& time,
                                const ArrayQ<T>& var, ArrayQ<T>& uCons ) const
{
  uCons = var;
}

template <class VarType>
template <class T, class Tc, class Tm, class Tq, class Tr>
inline void
PDEHSM2D<VarType>::jacobianMasterState( const ParamsType<Tc,Tm,Tq>& params,
                                        const Tr& x0, const Tr& y0, const Real& time,
                                        const ArrayQ<T>& var, MatrixQ<T>& dudq ) const
{
  dudq = DLA::Identity();
}

// First term of residual equations [28]
//   Gives R^f1 = 3rd component of residual vector
//   Gives R^f2 = 4th component of residual vector
// fluxAdvective = (-ehat_i . fbar)
template <class VarType>
template <class Tv, class Tc, class Tm, class Tq, class Tr, class Tf>
inline void
PDEHSM2D<VarType>::
fluxAdvective( const ParamsType<Tc,Tm,Tq>& params,
               const Tr& x0, const Tr& y0, const Real& time,
               const ArrayQ<Tv>& var,
               ArrayQ<Tf>& fx, ArrayQ<Tf>& fy) const
{

  // Deformed local basis vectors
  VectorX<Tv> e1, e2;
  // Calculate ehat_1, ehat_2
  varInterpret_.calc_ehat( var, e1, e2 );
  // Local basis
  MatrixX<Tv> e = {{ e1[0], e2[0] },{ e1[1], e2[1] }};

  // Force resultant matrix
  DLA::MatrixSymS<2,Tv> fbar;
  // Calculate fbar
  calc_fbar(var, e, fbar);
  // Components of fbar
  Tv& fxx = fbar(0,0);
  Tv& fxy = fbar(1,0);
  Tv& fyy = fbar(1,1);
  // Components of ehat
  Tv& e1x = e1[0];
  Tv& e1y = e1[1];
  Tv& e2x = e2[0];
  Tv& e2y = e2[1];

  // Vector-tensor dot in [28]
  // R^f1 flux terms
  Tv e1fbarx = e1x*fxx + e1y*fxy;
  Tv e1fbary = e1x*fxy + e1y*fyy;
  // R^f2 flux terms
  Tv e2fbarx = e2x*fxx + e2y*fxy;
  Tv e2fbary = e2x*fxy + e2y*fyy;

  // The writeup includes a negation, but the
  // negation is associate with grad(W), not the pde flux.
  // Hence, no negation here.

  // Inhabit flux term corresponding to R^f1
  // x-component
  fx[3] += e1fbarx;
  // y-component
  fy[3] += e1fbary;
  // Inhabit flux term corresponding to R^f2
  // x-component
  fx[4] += e2fbarx;
  // y-component
  fy[4] += e2fbary;

  // Turn off flux
  // fx = {0,0,0,0,0,0};
  // fy = {0,0,0,0,0,0};

}

// Rotate local matrix to cartesian
template <class VarType>
template <class T, class Tr>
inline void
PDEHSM2D<VarType>::loc2cart( const MatrixX<T>& e, const MatrixX<Tr>& matloc, MatrixX<T>& matcart ) const
{
  matcart = e * matloc * Transpose(e);
}

// Rotate symmetric local matrix to cartesian
template <class VarType>
template <class T, class Tr>
inline void
PDEHSM2D<VarType>::loc2cart( const MatrixX<T>& e,
                          const DLA::MatrixSymS<D,Tr>& matloc, DLA::MatrixSymS<D,T>& matcart ) const
{
  matcart = e * matloc * Transpose(e);
}

// Rotate cartesian matrix to local
template <class VarType>
template <class T>
inline void
PDEHSM2D<VarType>::cart2loc( const MatrixX<T>& e,
                          const DLA::MatrixS<D,D,T>& matcart, DLA::MatrixS<D,D,T>& matloc ) const
{
  matloc = Transpose(e) * matcart * e;
}

// Rotate symmetric cartesian matrix to local
template <class VarType>
template <class T>
inline void
PDEHSM2D<VarType>::cart2loc( const MatrixX<T>& e,
                          const DLA::MatrixSymS<D,T>& matcart, DLA::MatrixSymS<D,T>& matloc ) const
{
  matloc = Transpose(e) * matcart * e;
}

// Calculate fbar, stress-resultant matrix in global coordinates (Eq. 33)
template <class VarType>
template <class Tv, class Te, class T>
inline void
PDEHSM2D<VarType>::calc_fbar( const ArrayQ<Tv>& var, const MatrixX<Te>& e,
                              DLA::MatrixSymS<D,T>& fbar) const
{
  // Get stress resultants in local material axes from state vector
  DLA::VectorS<3,Tv> floc;
  varInterpret_.getForces( var, floc );
  // Components of floc
  Tv& f11 = floc[0];
  Tv& f22 = floc[1];
  Tv& f12 = floc[2];

  // Put forces in a matrix
  DLA::MatrixSymS<D,Tv> flocbar = {{ f11 },
                                   { f12, f22 }};

  // Convert stress resultants to global coordinates for use in residual eqs
  loc2cart( e, flocbar, fbar );

}

// Calculate the force dependent strain tensor from Section 4.2.3
template <class VarType>
template <class Tv, class Tc, class Tm, class Tq, class T>
inline void
PDEHSM2D<VarType>::calc_epsloc_force( const ParamsType<Tc,Tm,Tq>& params,
                                   const ArrayQ<Tv>& var,
                                   DLA::VectorS<3,T>& epsloc_force ) const
{
  // Get compliance matrix from params
  const ComplianceMatrix<Tc>& AijInv = get<iAijInv>(params);

  // Get force resultants in local material axes from state vector
  DLA::VectorS<3,Tv> floc;
  varInterpret_.getForces( var, floc );

  // Force-dependent straings from Eq. 24
  epsloc_force = AijInv * floc;

}

#if 0
// Calculate the geometry dependent strain tensor from Section 4.2.1
template <class VarType>
template <class T, class Tr>
inline void
PDEHSM2D<VarType>::calc_epsloc_geom_metric( const VectorX<Tr>& r0xi, const VectorX<Tr>& r0eta,
                                         const VectorX<Tr>& rxi,  const VectorX<Tr>& reta,
                                         DLA::VectorS<3,T>& epsloc_geom ) const
{
  // Metric tensors gij, g0ij (Eq. 13,14)
  DLA::MatrixSymS<D,T> gij  = {{ dot(rxi,rxi) },
                               { dot(rxi,reta) , dot(reta,reta) }};

  DLA::MatrixSymS<D,Tr> g0ij = {{ dot(r0xi,r0xi) },
                                { dot(r0xi,r0eta) , dot(r0eta,r0eta) }};

  // Undeformed metric tensor inverse
  Tr a = g0ij(0,0);
  Tr b = g0ij(1,0);
  Tr d = g0ij(1,1);
  Tr InvDet_g0ij = 1.0 / (a*d-b*b);
  DLA::MatrixSymS<2,Tr> g0ijAdj = {{ d },{ -b, a }};
  DLA::MatrixSymS<2,Tr> g0ijInv = InvDet_g0ij * g0ijAdj;

  // Undeformed contravariant vectors (Eq. 16,17)
  VectorX<Tr> bxi0  = g0ijInv * r0xi;
  VectorX<Tr> beta0 = g0ijInv * r0eta;
  // Components of contravariant vectors
  Tr& bxi01  = bxi0[0];
  Tr& bxi02  = bxi0[1];
  Tr& beta01 = beta0[0];
  Tr& beta02 = beta0[1];

  // Strain tensor associated with the covariant tangential basis vectors (Eq. 7)
  //   (parametric derivates of the shell geometry function)
  DLA::MatrixSymS<D,T> epsbrv = 0.5 * (gij - g0ij);
  T& epsbrv11 = epsbrv(0,0);
  T& epsbrv21 = epsbrv(1,0);
  T& epsbrv22 = epsbrv(1,1);

  // Convert to 12 basis (Eq. 8)
  T eps11 = epsbrv11 * bxi01  * bxi01
          + epsbrv21 * bxi01  * beta01
          + epsbrv21 * beta01 * bxi01
          + epsbrv22 * beta01 * beta01;
  T eps21 = epsbrv11 * bxi02  * bxi01
          + epsbrv21 * bxi02  * beta01
          + epsbrv21 * beta02 * bxi01
          + epsbrv22 * beta02 * beta01;
  T eps22 = epsbrv11 * bxi02  * bxi02
          + epsbrv21 * bxi02  * beta02
          + epsbrv21 * beta02 * bxi02
          + epsbrv22 * beta02 * beta02;

  epsloc_geom = { eps11, eps22, eps21 };
}
#endif

// Calculate the geometry dependent strain tensor from Section 4.2.2
template <class VarType>
template <class Te, class Tr, class Tx, class T>
inline void
PDEHSM2D<VarType>::calc_epsloc_geom_cart( const MatrixX<Te>& e,
                                          const VectorX<Tr>& r0xi, const VectorX<Tr>& r0eta,
                                          const VectorX<Tx>& rxi,  const VectorX<Tx>& reta,
                                          DLA::VectorS<3,T>& epsloc_geom ) const
{
  // Geometry parametric derivatives
  MatrixX<Tr> dr0 = {{ r0xi[0] , r0xi[1]  },
                     { r0eta[0], r0eta[1] }};
  MatrixX<Tx> dr  = {{ rxi[0]  , rxi[1]   },
                     { reta[0] , reta[1]  }};

#if 1
  // Lagrangian strain
  // ~> Deformed position surface gradient wrt undeformed geometry

  // Solve 2x2 system
  MatrixX<Tr> dr0inv = DLA::InverseLUP::Inverse( dr0 );
  MatrixX<Tx> deltld0r = dr0inv * dr;

  // Undeformed geometry surface gradient is identity by construction
  DLA::MatrixSymS<D,Tr> deltld0r0 = DLA::Identity();

  // Calculate strains in cartesian coordinates
  // MatrixX<T > epscart = 0.5 * (deltld0r * Transpose(deltld0r) - deltld0r0 * Transpose(deltld0r0));
  MatrixX<T > epscart = 0.5 * (deltld0r * Transpose(deltld0r) - deltld0r0);

#else

  // Eulerian strain
  // ~> Undeformed position surface gradient wrt deformed geometry

  // Solve 2x2 system
  MatrixX<Tr> drinv = DLA::InverseLUP::Inverse( dr );
  MatrixX<T > deltldr0 = drinv * dr0;

  // Undeformed geometry surface gradient is identity by construction
  DLA::MatrixSymS<D,Tr> deltldr = DLA::Identity();

  // Calculate strains in cartesian coordinates
  // MatrixX<T > epscart = 0.5 * (deltldr * Transpose(deltldr) - deltldr0 * Transpose(deltldr0));
  MatrixX<T > epscart = 0.5 * (deltldr - deltldr0 * Transpose(deltldr0));

#endif

  // Rotate to local frame for use in residual eqs (Eq.23)
  MatrixX<T > epsloc;
  cart2loc( e, epscart, epsloc );

  // Return local strains as a 3-vector {eps11, eps22, eps12}
  epsloc_geom = { epsloc(0,0), epsloc(1,1), epsloc(1,0) };
}


// solution-dependent source: S(X, Xxi, Q, QX, Qxi)
template <class VarType>
template <class Tv, class Tg, class Tx, class Tc, class Tm, class Tq, class Tr, class Tf>
inline void
PDEHSM2D<VarType>::source( const ParamsType<Tc,Tm,Tq>& params,
                        const Tr& x0, const Tr& y0, const Real& time,
                        const Tr& x0xi, const Tr& x0eta,
                        const Tr& y0xi, const Tr& y0eta,
                        const ArrayQ<Tv>& var,
                        const ArrayQ<Tg>& varx, const ArrayQ<Tg>& vary,
                        const ArrayQ<Tx>& varxi, const ArrayQ<Tx>& vareta,
                        ArrayQ<Tf>& source ) const
{
  // STRAIN-DISPLACEMENT CONSISTENCY -------------------------------------------------------
  typedef typename promote_Surreal<Tv,Tg,Tx>::type T;
  typedef typename promote_Surreal<T,Tc,Tm,Tq,Tr>::type Ts;

  // Deformed local basis vectors and derivs
  VectorX<T> e1, e1x, e1y, e2, e2x, e2y;
  // Calculate ehat_1, ehat_2
  varInterpret_.calc_ehat_derivs(var, varx, vary,
                                 e1 , e2,
                                 e1x, e1y,
                                 e2x, e2y);
  // Local basis
  MatrixX<T> e = {{ e1[0], e2[0] },
                  { e1[1], e2[1] }};

  // Postion derivatives w.r.t. geometry xi, eta
  // Deformed
  VectorX<Tx> rxi, reta;
  varInterpret_.getPositionRefDerivs( var, varxi, vareta, rxi, reta );
  // Undeformed
  VectorX<Tr> r0xi   = { x0xi , y0xi };
  VectorX<Tr> r0eta  = { x0eta, y0eta };

  // Two definitions of strain
  DLA::VectorS<3,Ts> epsloc_force;
  DLA::VectorS<3,T> epsloc_geom;
  // Force-dependent strains
  calc_epsloc_force( params, var, epsloc_force );
  // Geometry-dependent strains
  calc_epsloc_geom_cart( e, r0xi, r0eta, rxi, reta, epsloc_geom );

  // Break into components
  T& eps11r = epsloc_geom[0];
  T& eps22r = epsloc_geom[1];
  T& eps12r = epsloc_geom[2];
  Ts& eps11f = epsloc_force[0];
  Ts& eps22f = epsloc_force[1];
  Ts& eps12f = epsloc_force[2];

  // Inhabit source term corresponding to R^eps11
  source[0] += eps11r - eps11f;
  // Inhabit source term corresponding to R^eps22
  source[1] += eps22r - eps22f;
  // Inhabit source term corresponding to R^eps11
  source[2] += eps12r - eps12f;


  // IN-PLANE FORCE EQUILIBRIUM -----------------------------------------------------------

  // Get forcing terms from params
  const Tm& mu            = get<imu>(params);
  const VectorE<T>     qe = get<iqe>(params);
  const VectorX<Real>& qx = get<iqx>(params);
  const VectorX<Real>& a  = get<ia>(params);

  // Force resultant matrix
  DLA::MatrixSymS<2,T> fbar;
  // Calculate fbar
  calc_fbar(var, e, fbar);
  // Components of fbar
  T& fxx = fbar(0,0);
  T& fxy = fbar(1,0);
  T& fyy = fbar(1,1);

  // Force-curvature (2nd) term in (Eq.28)
  // R^f1 source term
  T fbardele1 = fxx*e1x[0] + fxy*e1y[0]
              + fxy*e1x[1] + fyy*e1y[1];
  // R^f2 source term
  T fbardele2 = fxx*e2x[0] + fxy*e2y[0]
              + fxy*e2x[1] + fyy*e2y[1];

  // Calculate overall applied force in cartesian axes
  VectorX<T> q = qx + e * qe;

  // Inhabit source term corresponding to R^f1 (2nd + 3rd term)
  source[3] += -fbardele1 + dot( q, e1 ) + dot( mu*(g_-a), e1 );

  // Inhabit source term corresponding to R^f2 (2nd + 3rd term)
  source[4] += -fbardele2 + dot( q, e2 ) + dot( mu*(g_-a), e2 );


  // IN-PLANE ROTATION CONSTRAINT -------------------------------------------------------

  // Calculate material line vectors
  // Deformed (Eq.37-38)
  T s1tld = dot(e1,rxi);
  T s2tld = dot(e2,rxi);
  // Undeformed (Eq.39-40)
  Tr s01 = x0xi;
  Tr s02 = y0xi;

  // Use geometry-residual-defined strains in calculating s-tilde (fewer Jacobian elements)
  VectorX<T> s0 = { s01, s02 };
  DLA::MatrixSymS<D,T> epsbar  = {{ 1+eps11r },
                                  {  eps12r   , 1+eps22r }};
  VectorX<T> s0tld = epsbar * s0;
  T& s01tld = s0tld[0];
  T& s02tld = s0tld[1];

  // Jacobian of element coordinates xi,eta, expressed as 1/J0^2 (Eq.36)
  Tr J0 = fabs( x0xi*y0eta - x0eta*y0xi );
  Tr J0invsq = pow( J0, -2 );

  // Inhabit source term corresponding to R^e3
  source[5] += J0invsq * (s1tld*s02tld - s2tld*s01tld);
}

// set from variable array
template <class VarType>
template <class T, class HSMVariable>
inline void
PDEHSM2D<VarType>::setDOFFrom( ArrayQ<T>& var, const HSMVariableType2D<HSMVariable>& data ) const
{
  varInterpret_.setFromVector( var, data.cast() );
}

// set from variable array
template <class VarType>
template <class HSMVariable>
inline typename PDEHSM2D<VarType>::template ArrayQ<Real>
PDEHSM2D<VarType>::setDOFFrom( const HSMVariableType2D<HSMVariable>& data ) const
{
  ArrayQ<Real> var;
  varInterpret_.setFromVector( var, data.cast() );
  return var;
}

// interpret residuals of the solution variable
template <class VarType>
template <class T>
void
PDEHSM2D<VarType>::interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
{

  int nOut = rsdPDEOut.m();

  if (cat_ == HSM_ResidInterp_Separate) // separate residuals for all equations
  {
    SANS_ASSERT( nOut == PDEHSM2D<VarType>::N );
    for (int i = 0; i < PDEHSM2D<VarType>::N; i++)
      rsdPDEOut[i] = rsdPDEIn[i];
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );
  }
}

// interpret residuals of the gradients in the solution variable
template <class VarType>
template <class T>
void
PDEHSM2D<VarType>::interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const
{
  SANS_DEVELOPER_EXCEPTION( "HSM2D<VarType>::interpResidGradient not implemented" );
}

// interpret residuals at the boundary (should forward to BCs)
template <class VarType>
template <class T>
void
PDEHSM2D<VarType>::interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const
{
  int nOut = rsdBCOut.m();

  if (cat_ == HSM_ResidInterp_Separate) // separate residuals for all equations
  {
    SANS_ASSERT( nOut == PDEHSM2D<VarType>::N );
    for (int i = 0; i < PDEHSM2D<VarType>::N; i++)
      rsdBCOut[i] = rsdBCIn[i];
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );
  }
}

// return the interp type
template <class VarType>
typename PDEHSM2D<VarType>::HSMResidualInterpCategory
PDEHSM2D<VarType>::category() const
{
  return cat_;
}

// how many residual equations are we dealing with
template <class VarType>
int
PDEHSM2D<VarType>::nMonitor() const
{
  int nMon;

  if (cat_ == HSM_ResidInterp_Separate) // separate residuals for all equations
    nMon = PDEHSM2D<VarType>::N;
  else
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );

  return nMon;
}

} //namespace SANS

#endif  // HSM2D_H
