// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_SHALLOWWATER_SHALLOWWATERVARIABLE2D_H_
#define SRC_PDE_SHALLOWWATER_SHALLOWWATERVARIABLE2D_H_

#include "tools/SANSnumerics.h" // Real

#include "ShallowWater_Traits.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Root struct for 2D shallow water input variable types
//
// template parameters:
//   Derived                variable type (e.g.. primitive variable set)
//
template<class Derived>
struct ShallowWater2DVariableType
{
  // A convenient method for casting to the derived type
  inline const Derived& cast() const { return static_cast<const Derived&>(*this); }
};

//----------------------- Derived = VarTypeHVelocity2D -----------------------//
// input variables:
//   H_                 H: water height
//   vx_                v_x: x-component of flow velocity
//   vy_                v_y: y-component of flow velocity
//
struct VarTypeHVelocity2D : public ShallowWater2DVariableType<VarTypeHVelocity2D>
{
  static const int N = ShallowWaterTraits<PhysD2>::N;

  VarTypeHVelocity2D(const Real& H, const Real& vx, const Real& vy)
      : H_( H ), vx_( vx ), vy_( vy ) {}

  VarTypeHVelocity2D& operator=( const std::initializer_list<Real>& s )
  {
    SANS_ASSERT(s.size() == N);
    auto q = s.begin();
    H_  = *q; q++;
    vx_ = *q; q++;
    vy_ = *q;

    return *this;
  }

  Real H_;
  Real vx_;
  Real vy_;
};

} // namespace SANS


#endif /* SRC_PDE_SHALLOWWATER_SHALLOWWATERVARIABLE2D_H_ */
