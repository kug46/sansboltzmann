// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_SHALLOWWATER_SHALLOWWATERSOLUTION2D_MANIFOLD_H_
#define SRC_PDE_SHALLOWWATER_SHALLOWWATERSOLUTION2D_MANIFOLD_H_

// 2-D shallow water manifold solutions

#include "VarInterp2D_manifold.h"

// Python must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// For ellipse
//  x = a * cos(theta)
//  y = b * sin(theta)
//  where a, b are major and minor semi-axes of an ellipse
//
// solution:
//  H(theta) = 1 + theta + ... + theta^p, p >= 0
//  H*v = q0
//
class ShallowWaterSolutionFunction2D_manifold_Ellipse_GeometricSeriesTheta
{
public:
  typedef PhysD2 PhysDim; // required for solnNDSteadyConvert
  typedef VarTypeHVelocity2D_manifold VarType;
  typedef VarInterpret2D_manifold<VarType> QInterpret;            // solution variable interpreter type

  template<class T>
  using ArrayQ = QInterpret::template ArrayQ<T>;

  using VectorX = ShallowWaterTraits_manifold<PhysDim>::VectorX;

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> g{"g", 9.81, 0, NO_LIMIT, "GravitationalAcceleration [m/s^2]"};
    const ParameterNumeric<Real> q0{"q0", NO_DEFAULT, NO_RANGE, "H*Vtheta"};
    const ParameterNumeric<int> p{"p", NO_DEFAULT, 0, NO_LIMIT, "PolynomialDegree"};
    const ParameterNumeric<Real> a{"a", NO_DEFAULT, 1.e-15, NO_LIMIT, "Major semi-axis"};
    const ParameterNumeric<Real> b{"b", NO_DEFAULT, 1.e-15, NO_LIMIT, "Minor semi-axis"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ShallowWaterSolutionFunction2D_manifold_Ellipse_GeometricSeriesTheta( const PyDict& d ) :
      qinterp_( ),
      g_(d.get(ParamsType::params.g)),
      q0_(d.get(ParamsType::params.q0)),
      p_(d.get(ParamsType::params.p)),
      a_(d.get(ParamsType::params.a)),
      b_(d.get(ParamsType::params.b)) {}


  // overloaded operator ()
  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    if ( p_ < 0 )
      SANS_DEVELOPER_EXCEPTION( "p_ >= 0 is violated." );

    const T theta = atan2(a_*y,b_*x);

    // Flat line
    T H = 0;

    // p = 0
    H = 1;

    // p > 0
    for ( int i = 1; i <= p_; i++ )
    {
      H += pow(theta,i);
    }

    T vs = q0_/H;

    ArrayQ<T> solnq = 0;
    VarType vardata( H, vs );
    qinterp_.setFromPrimitive( vardata, solnq );

    return solnq;
  }

  template<class T>
  void gradient( const T& x, const T& y, const T& time, ArrayQ<T>& q, ArrayQ<T>& qx, ArrayQ<T>& qy, ArrayQ<T>& qt ) const
  {
    //TODO: This is a hack to get it working with the generic output functional classes. Gradients are not computed.
    //      This solution function class needs to be derived from Function2DVirtualInterface, so that gradients can be computed with
    //      automatic differentiation, if needed. Refer to SolutionFunction_NavierStokes2D_OjedaMMS for an example.
    q = this->operator ()(x, y, time);
  }

  // compute surface gradient of bathymetry: i.e. grad b
  void getBathymetryGrad( const Real& x, const Real& y, const Real& time, VectorX& nablasb ) const
  {
    const Real theta = atan2(a_*y,b_*x);
    const Real d = sqrt( a_*a_*sin(theta)*sin(theta) + b_*b_*cos(theta)*cos(theta) );

    ArrayQ<Real> soln = (*this)(x,y,time);
    Real H;
    qinterp_.getH( soln, H );
    Real H_theta = 0; // dH/dtheta

    // dH/dtheta
    if ( p_ < 0 )
      SANS_DEVELOPER_EXCEPTION( "p_ >= 0 is violated." );
      // this is in effect unnecessary because the overloaded operator() already throws an exception
    else if ( p_ == 0 )
      H_theta = 0;
    else if ( p_ == 1 )
      H_theta = 1;
    else
    {
      H_theta = 1;
      for ( int i = 2; i <= p_; i++ )
        H_theta += static_cast<Real>(i) * pow(theta,i-1);
    }

    // db/dtheta = (1 - q0^2/(g*H^3))*dH/dtheta
    // db/ds = db/dtheta * dtheta/ds
    const Real b_s = ( 1 - q0_*q0_/(g_*pow(H,3)) ) * H_theta / d;

    nablasb[0] = (-a_*sin(theta)/d) * b_s; // db/dx
    nablasb[1] = ( b_*cos(theta)/d) * b_s; // db/dy
  }

  VectorX getBathymetryGrad( const Real& x, const Real& y, const Real& time ) const
  {
    VectorX nablasb;
    getBathymetryGrad( x, y, time, nablasb );
    return nablasb;
  }

  void getab( Real& a, Real& b) const
  {
    a = a_;
    b = b_;
  }

  // print out parameters
  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ShallowWaterSolutionFunction2D_GeometricSeriesSlopeRef: g_ = " << g_
        << ", q0_ = " << q0_
        << " and p_ = " << p_
        << " and a_ = " << a_
        << " and a_ = " << b_ << std::endl;
  }

private:
  const QInterpret qinterp_;
  const Real g_;
  const Real q0_;
  const int p_;
  const Real a_;
  const Real b_;
}; // class ShallowWaterSolutionFunction2D_manifold_Ellipse_GeometricSeriesTheta
}

#endif /* SRC_PDE_SHALLOWWATER_SHALLOWWATERSOLUTION2D_MANIFOLD_H_ */
