// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "PDEShallowWater2D_manifold.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Explicit instantiation

template class PDEShallowWater_manifold<PhysD2,VarTypeHVelocity2D_manifold,
                                        ShallowWaterSolutionFunction2D_manifold_Ellipse_GeometricSeriesTheta
                                       >;
}
