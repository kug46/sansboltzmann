// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCShallowWater2D_manifold.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{

// cppcheck-suppress passedByValue
void BCShallowWater2DParams_manifold<BCTypeInflowSupercritical>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.H));
  allParams.push_back(d.checkInputs(params.vs));
  d.checkUnknownInputs(allParams);
}
BCShallowWater2DParams_manifold<BCTypeInflowSupercritical> BCShallowWater2DParams_manifold<BCTypeInflowSupercritical>::params;


//===========================================================================//
// Instantiate the BC parameters
typedef BCShallowWater2DVector_manifold< VarTypeHVelocity2D_manifold,
                                         ShallowWaterSolutionFunction2D_manifold_Ellipse_GeometricSeriesTheta
                                       > BCVector;
BCPARAMETER_INSTANTIATE( BCVector )

} //namespace SANS
