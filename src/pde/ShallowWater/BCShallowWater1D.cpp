// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCShallowWater1D.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{

// cppcheck-suppress passedByValue
void BCShallowWater1DParams<BCTypeInflowSupercritical>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.H));
  allParams.push_back(d.checkInputs(params.u));
  d.checkUnknownInputs(allParams);
}
BCShallowWater1DParams<BCTypeInflowSupercritical> BCShallowWater1DParams<BCTypeInflowSupercritical>::params;


//===========================================================================//
// Instantiate the BC parameters
typedef BCShallowWater1DVector<VarTypeHVelocity1D,ShallowWaterSolutionFunction1D_GeometricSeriesx_Forcing> BCVector;
BCPARAMETER_INSTANTIATE( BCVector )

} //namespace SANS
