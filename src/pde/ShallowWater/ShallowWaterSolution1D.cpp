// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "ShallowWaterSolution1D.h"

namespace SANS
{

// cppcheck-suppress passedByValue
void ShallowWaterSolutionFunction1D_GeometricSeriesx::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.g));
  allParams.push_back(d.checkInputs(params.q0));
  allParams.push_back(d.checkInputs(params.p));
  d.checkUnknownInputs(allParams);
}
ShallowWaterSolutionFunction1D_GeometricSeriesx::ParamsType
    ShallowWaterSolutionFunction1D_GeometricSeriesx::ParamsType::params;

// cppcheck-suppress passedByValue
void ShallowWaterSolutionFunction1D_GeometricSeriesx_Forcing::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.g));
  allParams.push_back(d.checkInputs(params.pH));
  allParams.push_back(d.checkInputs(params.cH));
  allParams.push_back(d.checkInputs(params.pu));
  d.checkUnknownInputs(allParams);
}
ShallowWaterSolutionFunction1D_GeometricSeriesx_Forcing::ParamsType
    ShallowWaterSolutionFunction1D_GeometricSeriesx_Forcing::ParamsType::params;


} // namespace SANS
