// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_SHALLOWWATER_PDESHALLOWWATER2D_MANIFOLD_H_
#define SRC_PDE_SHALLOWWATER_PDESHALLOWWATER2D_MANIFOLD_H_

#define PDEShallowWaterManifold_sansgH2fluxAdvective2D 1
//  // does not incorporate grad H in advective flux; i.e. semi-divergence form as opposed to full-divergence form
////
////#define ShallowWater_IsCircle_NotFlat 1          // shallow water equations on circle as opposed to tilted flat line
////

#define PDEShallowWaterManifold2D_manifold 1        // whether member function has arguments compatible with integrand Galerkin manifold

#include <limits>

#include "ShallowWaterSolution2D_manifold.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "tools/minmax.h"

#include <limits> // std::numeric_limits

namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: 2-D manifold shallow water equations
//
// Reference: 2D_Manifold_PDE.pdf
//   T                           solution DOF data type (e.g. double, SurrealS)
//
// Nodal state vector (var):
//  Depends on the definition in the solution interpreter class.
//
// Residual vector R
//  [0] R^(H)                    water height (or mass) conservation residual
//  [1] R^(u)                    x-component of momentum conservation residual
//
// member functions:
//   .hasFluxAdvectiveTime        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//   .hasFluxViscous             T/F: PDE has viscous flux term
//   .hasSource                  T/F: PDE has source term
//   .hasForcingFunction         T/F: PDE has forcing function term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: dU(Q)/dQ
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .fluxViscous                viscous fluxes: Fv(Q, QX)
//   .diffusionViscous           viscous diffusion coefficient: d(Fv)/d(UX)
//   .source                     solution-dependent sources: S(X, Q, QX)
//   .forcingFunction            forcing function

//   .isValidState               T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom                 set from primitive variable array
//
//   .getg
//----------------------------------------------------------------------------//

template <class VarType, class SolutionType>   // required for compiler to understand that a template is used
class PDEShallowWater_manifold<PhysD2, VarType, SolutionType>
{
public:

  enum ShallowWaterResidualInterpCategory
  {
    ShallowWater_ResidInterp_Raw
  };

  typedef PhysD2 PhysDim;

  //-----------------------Fields-----------------------//
  static const int D = ShallowWaterTraits_manifold<PhysDim>::D;      // physical dimensions
  static const int N = ShallowWaterTraits_manifold<PhysDim>::N;  // total solution variables; 2D manifold has the same number of variables as 1D

  //-----------------------Types-----------------------//
  template <class T>
  using ArrayQ = ShallowWaterTraits_manifold<PhysDim>::template ArrayQ<T>;    // solution/residual array type
  // [Our compilers seem fine with missing or adding the keyword "template" before ArrayQ<T>, but it's good practice to keep it.]

  template <class T>
  using MatrixQ = ShallowWaterTraits_manifold<PhysDim>::template MatrixQ<T>;  // matrix type

  template <class T>
  using VectorD = DLA::VectorD<T>;

  using VectorX = ShallowWaterTraits_manifold<PhysDim>::VectorX;

  typedef VarInterpret2D_manifold<VarType> VarInterpret;            // solution variable interpreter type

  //-----------------------Constructors-----------------------//
  PDEShallowWater_manifold( const PyDict& solnd, ShallowWaterResidualInterpCategory cat ):
    g_( 9.81 ),
    varInterpret_(),
    solution_( solnd ),
    cat_(cat)
  {
    // Nothing
  }

  //-----------------------Destructors-----------------------//
  ~PDEShallowWater_manifold() {}
  PDEShallowWater_manifold( const PDEShallowWater_manifold& ) = delete;
  PDEShallowWater_manifold& operator=( const PDEShallowWater_manifold& )  = delete;

  //-----------------------Members-----------------------//
  // flux components
  bool hasFluxAdvectiveTime() const { return true; } // turned on for DG lifting operators though assuming steady-state for now
  bool hasFluxAdvective() const { return true; }
  bool hasFluxViscous() const { return false; }
  bool hasSource() const { return true; }
  bool hasSourceTrace() const { return false; }
  bool hasSourceLiftedQuantity() const { return false; }
  bool hasForcingFunction() const { return false; }

  bool needsSolutionGradientforSource() const
  {
#if PDEShallowWaterManifold_sansgH2fluxAdvective2D
    return true;
#else
    return false;
#endif
  }  // required for surface gradient of solution variables


  // unsteady temporal flux: Ft(Q)
  template <class T, class Tf>
  void fluxAdvectiveTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tf>& ft ) const
  {
    ArrayQ<Tf> ftLocal = 0;
    masterState(x, y, time, q, ftLocal);
    ft += ftLocal;
  }

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <class T, class Tf>
  void jacobianFluxAdvectiveTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& J ) const
  {
    J += DLA::Identity();
  }

  // master state: U(Q)
  template <class T>
  void masterState(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& var, ArrayQ<T>& uCons ) const;

  // mater state jacobian wrt q: dU(Q)/dQ
  template<class T>
  void jacobianMasterState( const ArrayQ<T>& q, MatrixQ<T>& dudq ) const;

  // strong form of unsteady conservative flux: dU(Q)/dt
  template <class T>
  void strongFluxAdvectiveTime( const ArrayQ<T>& var, const ArrayQ<T>& vart, ArrayQ<T>& strongPDE ) const {}

  // advective flux: F(Q)
#if PDEShallowWaterManifold2D_manifold
  template <class Tq, class Tf>
  void fluxAdvective(
      const VectorX& e01, const VectorX& e01x, const VectorX& e01y,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& fy ) const;

  template <class Tq, class Tf>
  void fluxAdvectiveUpwind(
      const VectorX& e01L, const VectorX& e01R,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const;

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class T>
  void jacobianFluxAdvective(
      const VectorX& e01,
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q,
      MatrixQ<T>& ax, MatrixQ<T>& ay ) const;

  // viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const {}

  // viscous diffusion matrix: d(Fv)/d(UX)
    template <class Tq, class Tg, class Tk>
    void diffusionViscous(
        const Real& x, const Real& y, const Real& time,
        const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
        MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const{}

  // solution-dependent source: S(X, Q, QX)
  template <class Tq, class Tg>
  void source(
      const VectorX& e01, const VectorX& e01x, const VectorX& e01y,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ< typename promote_Surreal<Tq,Tg>::type >& source ) const;
#else

  template <class T>
  void fluxAdvective(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q,
      ArrayQ<T>& fx, ArrayQ<T>& fy ) const;

  template <class T>
  void fluxAdvectiveUpwind(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, ArrayQ<T>& f ) const;

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class T>
  void jacobianFluxAdvective(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q,
      MatrixQ<T>& ax, MatrixQ<T>& ay ) const;

  // viscous flux: Fv(Q, QX)
  template <class T>
  void fluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
      ArrayQ<T>& f, ArrayQ<T>& g ) const {}

  // solution-dependent source: S(X, Q, QX)
  template <class T>
  void source(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
      ArrayQ<T>& source ) const;

#endif

  // right-hand-side forcing function
  template <class T>
  void forcingFunction( const Real& x, const Real& y, const Real& time, ArrayQ<T>& forcing ) const {}

  // is state physically valid
  template <class T>
  bool isValidState( const ArrayQ<T>& q ) const { return varInterpret_.isValidState(q); }

  // set from variable array
  template<class T>
  void setDOFFrom( const ShallowWater2DVariableType_manifold<VarType>& data,
                   ArrayQ<T>& var ) const
  {
    varInterpret_.setFromPrimitive( data.cast(), var );
  }

  // set from variable array
  ArrayQ<Real> setDOFFrom( const ShallowWater2DVariableType_manifold<VarType>& data ) const
  {
    ArrayQ<Real> var;
    varInterpret_.setFromPrimitive( data.cast(), var );
    return var;
  }

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const;

  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const;

  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const;

  // return the interp type
  ShallowWaterResidualInterpCategory category() const;

  // how many residual equations are we dealing with
  int nMonitor() const;

  void dump( int indentSize, std::ostream& out = std::cout ) const {}

  // accessors for parameters g_
  Real getg() const { return g_; }

  // return variable interpreter
  const VarInterpret& getVarInterpreter() const { return varInterpret_; }

protected:
  const Real g_;                                 // g: gravitational acceleration
  const VarInterpret varInterpret_;              // solution variable interpreter class
  const SolutionType solution_;                  // exact solution
  const ShallowWaterResidualInterpCategory cat_; // ResidInterp type

}; // class PDEShallowWater_manifold<PhysD2, VarType, SolutionType>


//--------------------- Member function definitions ---------------------//

// unsteady conservative flux: U(Q)
template <class VarType, class SolutionType>
template <class T>
inline void
PDEShallowWater_manifold<PhysD2, VarType, SolutionType>::masterState(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, ArrayQ<T>& uCons ) const
{
  T H = 0, vs = 0;  // Initialize to suppress clang analyzer warnings
  varInterpret_.getH( q, H );
  varInterpret_.getVelocity( q, vs );

  uCons(0) = H;
  uCons(1) = H*vs;
}

// unsteady conservative flux Jacobian: dU(Q)/dQ
template <class VarType, class SolutionType>
template <class T>
inline void
PDEShallowWater_manifold<PhysD2, VarType, SolutionType>::jacobianMasterState(
    const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
{
  SANS_DEVELOPER_EXCEPTION("Not implemented...");
}

// advective flux: F(Q)
template <class VarType, class SolutionType>
template <class Tq, class Tf>
inline void
PDEShallowWater_manifold<PhysD2, VarType, SolutionType>::
fluxAdvective(
#if PDEShallowWaterManifold2D_manifold
    const VectorX& e01, const VectorX& e01x, const VectorX& e01z,
#else
#endif
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& var,
    ArrayQ<Tf>& fx, ArrayQ<Tf>& fy ) const
{
  Tq H = 0, vs = 0;  // Initialize to suppress clang analyzer warnings
  varInterpret_.getH( var, H );
  varInterpret_.getVelocity( var, vs );

#if PDEShallowWaterManifold2D_manifold
  VectorX s; // streamwise unit vector
  Real a = 0, b = 0;
  solution_.getab( a, b );

  const Real theta = atan2(a*y,b*x);

  const Real d = sqrt( pow( -a*sin(theta), 2 ) + pow( b*cos(theta), 2 ) );
  s[0] = -a*sin(theta)/d;
  s[1] =  b*cos(theta)/d;

  VectorX e01eff;
  e01eff = e01;
#else // use hard-coded analytic basis vector e01
  Real a = 0, b = 0;
  solution_.getab( a, b );

  VectorX e01eff, s;
  const Real theta = atan2(a*y,b*x);

  const Real d = sqrt( pow( -a*sin(theta), 2 ) + pow( b*cos(theta), 2 ) );
  e01eff[0] = -a*sin(theta)/d;
  e01eff[1] =  b*cos(theta)/d;

  s = e01eff;
#endif

  Tq vx = vs*s[0], vy = vs*s[1]; // x, y (Cartesian) components of velocity
  DLA::VectorS<D,Tq> v = { vx, vy };

  // advective flux terms in continuity residual
  fx[0] += H*vx;
  fy[0] += H*vy;

#if PDEShallowWaterManifold_sansgH2fluxAdvective2D  // 0.5*g*H^2 not included in advective flux
  // advective flux terms in momentum residual
  fx[1] += H*vx*dot(v,e01eff);
  fy[1] += H*vy*dot(v,e01eff);
#else  // 0.5*g*H^2 included in advective flux
#endif

}

template <class VarType, class SolutionType>
template<class Tq, class Tf>
inline void
PDEShallowWater_manifold<PhysD2, VarType, SolutionType>::fluxAdvectiveUpwind(
#if PDEShallowWaterManifold2D_manifold
    const VectorX& e01L, const VectorX& e01R,
#else
#endif
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
    const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const
{
  Tq HL = 0, HR = 0;             // Initialized to suppress compiler warnings
  Tq vsL = 0, vsR = 0;
  Tq vxL, vyL, vxR, vyR;
  Tq alpha;                      // upwind coefficient

  varInterpret_.getH( qL, HL );
  varInterpret_.getVelocity( qL, vsL );
  varInterpret_.getH( qR, HR );
  varInterpret_.getVelocity( qR, vsR );

#if PDEShallowWaterManifold2D_manifold
  VectorX sL, sR;

  Real a = 0, b = 0;
  solution_.getab( a, b );

  const Real theta = atan2(a*y,b*x);

  const Real d = sqrt( pow( -a*sin(theta), 2 ) + pow( b*cos(theta), 2 ) );
  sL[0] = -a*sin(theta)/d;
  sL[1] =  b*cos(theta)/d;

  sR = sL;

  VectorX e01effL, e01effR;
  e01effL = e01L;
  e01effR = e01R;
#else
  Real a = 0, b = 0;
  solution_.getab( a, b );

  VectorX e01effL, e01effR;
  const Real theta = atan2(a*y,b*x);

  const Real d = sqrt( pow( -a*sin(theta), 2 ) + pow( b*cos(theta), 2 ) );
  e01effL[0] = -a*sin(theta)/d;
  e01effL[1] =  b*cos(theta)/d;

  e01effR = e01effL;

  VectorX sL = e01effL, sR = e01effR;
#endif

  vxL = vsL*sL[0];  vyL = vsL*sL[1];
  vxR = vsR*sR[0];  vyR = vsR*sR[1];

  DLA::VectorS<D,Tq> vL = { vxL, vyL };
  DLA::VectorS<D,Tq> vR = { vxR, vyR };

  const Real tx = nx, ty = ny;

#if PDEShallowWaterManifold_sansgH2fluxAdvective2D  // 0.5*g*H^2 not included in advective flux
  alpha = std::max( fabs(vsL), fabs(vsR) ); // largest eigenvalue in absolute value of flux jacobians

  // local Lax-Friedrichs upwind flux: 0.5*(FL + FR).n + 0.5*(UL - UR)*alpha
  f[0] += 0.5*( (HL*vxL + HR*vxR)*tx + (HL*vyL + HR*vyR)*ty )
        + 0.5*(HL - HR)*alpha;
  f[1] += 0.5*( (HL*dot(vL,e01effL)*vxL + HR*dot(vR,e01effR)*vxR)*tx + (HL*dot(vL,e01effL)*vyL + HR*dot(vR,e01effR)*vyR)*ty )
        + 0.5*(HL*dot(vL,e01effL) - HR*dot(vR,e01effR))*alpha;
#else  // 0.5*g*H^2 included in advective flux
#endif
}

template<class VarType, class SolutionType>
template<class T>
inline void
PDEShallowWater_manifold<PhysD2, VarType, SolutionType>::jacobianFluxAdvective(
#if PDEShallowWaterManifold2D_manifold
    const VectorX& e01,
#else
#endif
    const Real& x, const Real& y, const Real& time, const ArrayQ<T>& var,
    MatrixQ<T>& dfdu, MatrixQ<T>& dgdu ) const
{
  T H = 0, vs = 0;
  varInterpret_.getH( var, H );
  varInterpret_.getVelocity( var, vs );

#if PDEShallowWaterManifold2D_manifold
  VectorX s;

  Real a = 0, b = 0;
  solution_.getab( a, b );

  const Real theta = atan2(a*y,b*x);

  const Real d = sqrt( pow( -a*sin(theta), 2 ) + pow( b*cos(theta), 2 ) );
  s[0] = -a*sin(theta)/d;
  s[1] =  b*cos(theta)/d;

  VectorX e01eff;
  e01eff = e01;
#else
  Real a = 0, b = 0;
  solution_.getab( a, b );

  VectorX e01eff;
  const Real theta = atan2(a*y,b*x);

  const Real d = sqrt( pow( -a*sin(theta), 2 ) + pow( b*cos(theta), 2 ) );
  e01eff[0] = -a*sin(theta)/d;
  e01eff[1] =  b*cos(theta)/d;

  VectorX s = e01eff;
#endif

  DLA::VectorS<D,T> v = { vs*s[0], vs*s[1] };

#if PDEShallowWaterManifold_sansgH2fluxAdvective2D  // 0.5*g*H^2 not included in advective flux
  dfdu(0,0) += 0;
  if ( fabs(e01eff[0]) < 5*std::numeric_limits<Real>::epsilon()) // e01[0] approximately zero
  {
    dfdu(0,1) += 0;
    dfdu(1,0) += 0;
    dfdu(1,1) += 0;
  }
  else
  {
    dfdu(0,1) += 1/e01eff[0];
    dfdu(1,0) += - pow(dot(v,e01eff),2) / e01eff[0];
    dfdu(1,1) += 2 * dot(v,e01eff) /e01eff[0];
  }

  dgdu(0,0) += 0;
  if ( fabs(e01eff[1]) < 5*std::numeric_limits<Real>::epsilon()) // e01[1] approximately zero
  {
    dgdu(0,1) += 0;
    dgdu(1,0) += 0;
    dgdu(1,1) += 0;
  }
  else
  {
    dgdu(0,1) += 1/e01eff[1];
    dgdu(1,0) += - pow(dot(v,e01eff),2) / e01eff[1];
    dgdu(1,1) += 2 * dot(v,e01eff) / e01eff[1];
  }
#else  // 0.5*g*H^2 included in advective flux
#endif
}


// solution-dependent source: S(X, Q, QX)
template <class VarType, class SolutionType>
template <class Tq, class Tg>
inline void
#if PDEShallowWaterManifold2D_manifold
PDEShallowWater_manifold<PhysD2, VarType, SolutionType>::source(
    const VectorX& e01, const VectorX& e01x, const VectorX& e01y,
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ< typename promote_Surreal<Tq,Tg>::type >& source ) const
#else
PDEShallowWater_manifold<PhysD2, VarType, SolutionType>::source(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
    ArrayQ<T>& source ) const
#endif

{
  Tq H = 0;
  Tg H_x = 0, H_y = 0;
  varInterpret_.getH( q, H );
  varInterpret_.getH( qx, H_x );
  varInterpret_.getH( qy, H_y );
  Tq vs = 0;
  varInterpret_.getVelocity( q, vs );

  // compute gradient of bathymetry b(x,y)
  VectorX nablasb; //surface gradient of bathymetry b
  Real b_x = 0, b_y = 0;        // components of surface gradient of bathymetry b: db/dx & db/dy

  solution_.getBathymetryGrad(x,y,time,nablasb);

  b_x = nablasb[0];
  b_y = nablasb[1];

#if PDEShallowWaterManifold2D_manifold
  VectorX s;

  Real a = 0, b = 0;
  solution_.getab( a, b );

  const Real theta = atan2(a*y,b*x);

  const Real d = sqrt( pow( -a*sin(theta), 2 ) + pow( b*cos(theta), 2 ) );
  s[0] = -a*sin(theta)/d;
  s[1] =  b*cos(theta)/d;

  VectorX e01eff, e01xeff, e01yeff;
  e01eff = e01;

  e01xeff = e01x;
  e01yeff = e01y;
#else
  Real a = 0, b = 0;
  solution_.getab( a, b );

  VectorX e01eff, e01xeff, e01yeff;
  const Real theta = atan2(a*y,b*x);

  const Real d = sqrt( pow( -a*sin(theta), 2 ) + pow( b*cos(theta), 2 ) );
  e01eff[0] = -a*sin(theta)/d;
  e01eff[1] =  b*cos(theta)/d;

  e01xeff[0] = -a*b*b*cos(theta)/pow(d,5) * (-a*sin(theta));
  e01xeff[1] = -a*b*b*cos(theta)/pow(d,5) * ( b*cos(theta));

  e01yeff[0] = -a*a*b*sin(theta)/pow(d,5) * (-a*sin(theta));
  e01yeff[1] = -a*a*b*sin(theta)/pow(d,5) * ( b*cos(theta));

  VectorX s = e01eff;
#endif

  // Update source terms
  source[0] += 0;

#if PDEShallowWaterManifold_sansgH2fluxAdvective2D  // 0.5*g*H^2 not included in advective flux
  DLA::VectorS<D,Tq> v = { vs*s[0], vs*s[1] };

  source[1] += v[0] * H*dot(v,e01xeff) + v[1] * H*dot(v,e01yeff)
             + g_ * H * ( ( H_x - b_x ) * e01eff[0] + ( H_y - b_y ) * e01eff[1] );
#else  // 0.5*g*H^2 included in advective flux
#endif
}

// interpret residuals of the solution variable
template <class VarType, class SolutionType>
template<class T>
inline void
PDEShallowWater_manifold<PhysD2, VarType, SolutionType>::interpResidVariable(
    const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
{
  SANS_ASSERT( rsdPDEIn.M == N );

  int nOut = rsdPDEOut.m();

  if ( cat_ == ShallowWater_ResidInterp_Raw ) // raw residual
  {
    SANS_ASSERT_MSG( nOut == N, "nOut = %d whereas N = %d", nOut, N );
    for ( int i = 0; i < N; i++ )
      rsdPDEOut[i] = rsdPDEIn[i];
  }
  else
  {
    std::cout << "Invalid Residual Interp category" << std::endl;
    SANS_ASSERT(false);
  }
}

// interpret residuals of the gradients in the solution variable
template <class VarType, class SolutionType>
template <class T>
inline void
PDEShallowWater_manifold<PhysD2, VarType, SolutionType>::interpResidGradient(
    const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const
{
  SANS_ASSERT( rsdAuxIn.M == N );

  int nOut = rsdAuxOut.m();

  if ( cat_ == ShallowWater_ResidInterp_Raw ) // raw residual
  {
    SANS_ASSERT_MSG( nOut == N, "nOut = %d whereas N = %d", nOut, N );
    for ( int i = 0; i < N; i++ )
      rsdAuxOut[i] = rsdAuxIn[i];
  }
  else
  {
    std::cout << "Invalid Residual Interp category" << std::endl;
    SANS_ASSERT(false);
  }
}

// interpret residuals at the boundary (should forward to BCs)
template <class VarType, class SolutionType>
template <class T>
inline void
PDEShallowWater_manifold<PhysD2, VarType, SolutionType>::interpResidBC(
    const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const
{
  SANS_ASSERT( rsdBCIn.M == N );

  int nOut = rsdBCOut.m();

  if ( cat_ == ShallowWater_ResidInterp_Raw )  // raw residual
  {
    SANS_ASSERT_MSG( nOut == N, "nOut = %d whereas N = %d", nOut, N );
    for ( int i = 0; i < N; i++ )
      rsdBCOut[i] = rsdBCIn[i];
  }
  else
  {
    std::cout << "Invalid Residual Interp category" << std::endl;
    SANS_ASSERT(false);
  }
}

// return the interp type
template <class VarType, class SolutionType>
typename PDEShallowWater_manifold<PhysD2, VarType, SolutionType>::ShallowWaterResidualInterpCategory
PDEShallowWater_manifold<PhysD2, VarType, SolutionType>::category() const
{
  return cat_;
}

// how many residual equations are we dealing with
template <class VarType, class SolutionType>
inline int
PDEShallowWater_manifold<PhysD2, VarType, SolutionType>::nMonitor() const
{
  int nMon;

  if ( cat_ == ShallowWater_ResidInterp_Raw ) // raw residual
    nMon = N;
  else
  {
    std::cout << "Invalid Residual Interp category" << std::endl;
    SANS_ASSERT(false);
  }

  return nMon;
}

} // namespace SANS

#endif /* SRC_PDE_SHALLOWWATER_PDESHALLOWWATER2D_MANIFOLD_H_ */
