// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_SHALLOWWATER_SHALLOWWATERVARIABLE2D_MANIFOLD_H_
#define SRC_PDE_SHALLOWWATER_SHALLOWWATERVARIABLE2D_MANIFOLD_H_

#include "tools/SANSnumerics.h" // Real

#include "ShallowWater_Traits_manifold.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Root struct for 2D shallow water input variable types
//
// template parameters:
//   Derived                variable type (e.g.. primitive variable set)
//
template<class Derived>
struct ShallowWater2DVariableType_manifold
{
  // A convenient method for casting to the derived type
  inline const Derived& cast() const { return static_cast<const Derived&>(*this); }
};

//----------------------- Derived = VarTypeHVelocity2D_manifold -----------------------//
// input variables:
//   H_                 H: water height
//   vs_                v_s: flow velocity component along manifold
//
struct VarTypeHVelocity2D_manifold : public ShallowWater2DVariableType_manifold<VarTypeHVelocity2D_manifold>
{
  static const int N = ShallowWaterTraits_manifold<PhysD2>::N;

  VarTypeHVelocity2D_manifold( const Real& H, const Real& vs )
      : H_( H ), vs_( vs ) {}

  VarTypeHVelocity2D_manifold& operator=( const std::initializer_list<Real>& s )
  {
    SANS_ASSERT(s.size() == N);
    auto q = s.begin();
    H_  = *q; q++;
    vs_ = *q;

    return *this;
  }

  Real H_;
  Real vs_;
};

} // namespace SANS

#endif /* SRC_PDE_SHALLOWWATER_SHALLOWWATERVARIABLE2D_MANIFOLD_H_ */
