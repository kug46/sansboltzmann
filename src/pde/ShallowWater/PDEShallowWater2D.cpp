// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "PDEShallowWater2D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Explicit instantiation

template class PDEShallowWater<PhysD2,VarTypeHVelocity2D,ShallowWaterSolutionFunction2D_CosineTheta>;

template class PDEShallowWater<PhysD2,VarTypeHVelocity2D,ShallowWaterSolutionFunction2D_GeometricSeriesx>;

template class PDEShallowWater<PhysD2,VarTypeHVelocity2D,ShallowWaterSolutionFunction2D_GeometricSeriesTheta>;

template class PDEShallowWater<PhysD2,VarTypeHVelocity2D,ShallowWaterSolutionFunction2D_GeometricSeriesSlopeRef>;
}
