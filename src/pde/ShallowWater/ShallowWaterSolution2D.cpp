// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "ShallowWaterSolution2D.h"

namespace SANS
{
// cppcheck-suppress passedByValue
void ShallowWaterSolutionFunction2D_CosineTheta::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.g));
  allParams.push_back(d.checkInputs(params.q0));
  d.checkUnknownInputs(allParams);
}
ShallowWaterSolutionFunction2D_CosineTheta::ParamsType
    ShallowWaterSolutionFunction2D_CosineTheta::ParamsType::params;

// cppcheck-suppress passedByValue
void ShallowWaterSolutionFunction2D_GeometricSeriesx::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.g));
  allParams.push_back(d.checkInputs(params.q0));
  allParams.push_back(d.checkInputs(params.p));
  d.checkUnknownInputs(allParams);
}
ShallowWaterSolutionFunction2D_GeometricSeriesx::ParamsType ShallowWaterSolutionFunction2D_GeometricSeriesx::ParamsType::params;

// cppcheck-suppress passedByValue
void ShallowWaterSolutionFunction2D_GeometricSeriesSlopeRef::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.g));
  allParams.push_back(d.checkInputs(params.q0));
  allParams.push_back(d.checkInputs(params.p));
  d.checkUnknownInputs(allParams);
}
ShallowWaterSolutionFunction2D_GeometricSeriesSlopeRef::ParamsType
    ShallowWaterSolutionFunction2D_GeometricSeriesSlopeRef::ParamsType::params;

// cppcheck-suppress passedByValue
void ShallowWaterSolutionFunction2D_GeometricSeriesTheta::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.g));
  allParams.push_back(d.checkInputs(params.q0));
  allParams.push_back(d.checkInputs(params.p));
  d.checkUnknownInputs(allParams);
}
ShallowWaterSolutionFunction2D_GeometricSeriesTheta::ParamsType
    ShallowWaterSolutionFunction2D_GeometricSeriesTheta::ParamsType::params;

} // namespace SANS
