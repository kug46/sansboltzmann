// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_SHALLOWWATER_PDESHALLOWWATER2D_H_
#define SRC_PDE_SHALLOWWATER_PDESHALLOWWATER2D_H_

#define PDEShallowWater_sansgH2fluxAdvective2D 0
  // does not incorporate grad H in advective flux; i.e. semi-divergence form as opposed to full-divergence form

#define ShallowWater_IsCircle_NotFlat 1          // shallow water equations on circle as opposed to tilted flat line

#define ShallowWaterIntegrandManifold2D 0        // whether member function has arguments compatible with integrand Galerkin manifold
#define ShallowWaterHasStrongFluxAdvective2D 0   // whether has strong flux advective member function

#include "ShallowWaterSolution2D.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "tools/minmax.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: 2-D Shallow water equations
//
// Reference: 2D_Manifold_PDE.pdf
//
// template parameters:
//   T                           solution DOF data type (e.g. double, SurrealS)
//
// Nodal state vector (var):
//  Depends on the definition in the solution interpreter class.
//
// Residual vector R
//  [0] R^(H)                    water height (or mass) conservation residual
//  [1] R^(u)                    x-component of momentum conservation residual
//
// member functions:
//   .hasFluxAdvectiveTime        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//   .hasFluxViscous             T/F: PDE has viscous flux term
//   .hasSource                  T/F: PDE has source term
//   .hasForcingFunction         T/F: PDE has forcing function term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: dU(Q)/dQ
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .fluxViscous                viscous fluxes: Fv(Q, QX)
//   .diffusionViscous           viscous diffusion coefficient: d(Fv)/d(UX)
//   .source                     solution-dependent sources: S(X, Q, QX)
//   .forcingFunction            forcing function

//   .isValidState               T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom                 set from primitive variable array
//
//   .getg
//----------------------------------------------------------------------------//

template <class VarType, class SolutionType>   // required for compiler to understand that a template is used
class PDEShallowWater<PhysD2, VarType, SolutionType>
{
public:

  enum ShallowWaterResidualInterpCategory
  {
    ShallowWater_ResidInterp_Raw
  };

  //-----------------------Fields-----------------------//
  static const int D = ShallowWaterTraits<PhysD2>::D;               // physical dimensions
  static const int N = ShallowWaterTraits<PhysD2>::N;               // total solution variables

  //-----------------------Types-----------------------//
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = ShallowWaterTraits<PhysDim>::template ArrayQ<T>;    // solution/residual array type
  // [Our compilers seem fine with missing or adding the keyword "template" before ArrayQ<T>, but it's good practice to keep it.]

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using MatrixQ = ShallowWaterTraits<PhysDim>::template MatrixQ<T>;  // matrix type

  using VectorX = ShallowWaterTraits<PhysDim>::VectorX;

  // TODO: clean up
#if ShallowWaterIntegrandManifold2D
  using VectorEIF = DLA::VectorS<4,Real>; // dummy; just to be compatible with current manifold integrand framework
#endif

  typedef VarInterpret2D<VarType> VarInterpret;            // solution variable interpreter type

  //-----------------------Constructors-----------------------//
  PDEShallowWater( const Real& q0, const PyDict& solnd, ShallowWaterResidualInterpCategory cat ):
    g_( 9.81 ),
    q0_( q0 ),
    varInterpret_(),
    solution_( solnd ),
    cat_(cat)
  {
    // Nothing
  }

//  PDEShallowWater( const Real& g, const Real& q0, const SolutionType& soln )
//      : g_( g ), q0_( q0 ), varInterpret_(), solution_( soln ) {}

  //-----------------------Destructors-----------------------//
  ~PDEShallowWater() {}
  PDEShallowWater( const PDEShallowWater& ) = delete;
  PDEShallowWater& operator=( const PDEShallowWater& )  = delete;

  //-----------------------Members-----------------------//
  // flux components
  bool hasFluxAdvectiveTime() const { return true; } // turned on for DG lifting operators though assuming steady-state for now
  bool hasFluxAdvective() const { return true; }
  bool hasFluxViscous() const { return false; }
  bool hasSource() const { return true; }
  bool hasSourceTrace() const { return false; }
  bool hasSourceLiftedQuantity() const { return false; }
  bool hasForcingFunction() const { return false; }

  bool needsSolutionGradientforSource() const
  {
#if PDEShallowWater_sansgH2fluxAdvective2D
    return true;
#else
    return false;
#endif
  }  // required for surface gradient of solution variables


  // unsteady temporal flux: Ft(Q)
  template <class T, class Tf>
  void fluxAdvectiveTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tf>& ft ) const
  {
    ArrayQ<Tf> ftLocal = 0;
    masterState(x, y, time, q, ftLocal);
    ft += ftLocal;
  }

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <class T, class Tf>
  void jacobianFluxAdvectiveTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& J ) const
  {
    J += DLA::Identity();
  }

  // master state: U(Q)
  template <class T>
  void masterState(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& var, ArrayQ<T>& uCons ) const;

  // master state jacobian wrt q: dU(Q)/dQ
  template<class T>
  void jacobianMasterState(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const;

  // strong form of unsteady conservative flux: dU(Q)/dt
  template <class T>
  void strongFluxAdvectiveTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& var, const ArrayQ<T>& vart, ArrayQ<T>& strongPDE ) const {}


  // advective flux: F(Q)
#if ShallowWaterIntegrandManifold2D // use integrand_galerkin_manifold
  template <class T>
  void fluxAdvective(
      const VectorEIF& EIF,
      const DLA::VectorS<D,Real>& e1,
      const DLA::VectorS<D,Real>& e1x, const DLA::VectorS<D,Real>& e1y,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& var,
      ArrayQ<T>& fx, ArrayQ<T>& fy ) const;
#else
  template <class Tq, class Tf>
  void fluxAdvective(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& var,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& fy ) const;
#endif

  template <class Tq, class Tf>
  void fluxAdvectiveUpwind(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const;

  template <class T>
  void fluxAdvectiveUpwindSpaceTime(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, const Real& nt, ArrayQ<T>& f ) const {}

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class T>
  void jacobianFluxAdvective(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& var,
      MatrixQ<T>& ax, MatrixQ<T>& ay ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class T>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& var, const Real& nx, const Real& ny,
      MatrixQ<T>& a ) const {}

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class T>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q, const Real& nx, const Real& ny, const Real& nt,
      MatrixQ<T>& a ) const {}

  // strong form advective fluxes
  template <class T>
  void strongFluxAdvective(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& var, const ArrayQ<T>& varx, const ArrayQ<T>& vary,
      ArrayQ<T>& strongPDE ) const {}


  // viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& var, const ArrayQ<Tg>& varx, const ArrayQ<Tg>& vary,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const {}

  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& varL, const ArrayQ<Tg>& varxL, const ArrayQ<Tg>& varyL,
      const ArrayQ<Tq>& varR, const ArrayQ<Tg>& varxR, const ArrayQ<Tg>& varyR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const {}

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& var, const ArrayQ<Tg>& varx, const ArrayQ<Tg>& vary,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const {}

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class T>
  void jacobianFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& var, MatrixQ<T>& ax, MatrixQ<T>& ay ) const {}

  // strong form viscous fluxes
  template <class T>
  void strongFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& var, const ArrayQ<T>& varx, const ArrayQ<T>& vary,
      const ArrayQ<T>& varxx,
      const ArrayQ<T>& varxy, const ArrayQ<T>& varyy,
      ArrayQ<T>& strongPDE ) const {}


  // solution-dependent source: S(X, Q, QX)
#if ShallowWaterIntegrandManifold2D // use integrand_galerkin_manifold
  template <class T>
  void source(
      const VectorEIF& EIF,
      const DLA::VectorS<D,Real>& e1,
      const DLA::VectorS<D,Real>& e1x, const DLA::VectorS<D,Real>& e1z,
      const Real& x, const Real& y, const Real& time,
      const Real& xxi, const Real& yxi,
      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
      const ArrayQ<T>& qxi,
      ArrayQ<T>& source ) const;
#else
  template <class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Ts>& source ) const;
#endif

  // dual-consistent source
  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& xL, const Real& yL,
      const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& varL, const ArrayQ<Tg>& varxL, const ArrayQ<Tg>& varyL,
      const ArrayQ<Tq>& varR, const ArrayQ<Tg>& varxR, const ArrayQ<Tg>& varyR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class T>
  void jacobianSource(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& var, const ArrayQ<T>& varx, const ArrayQ<T>& vary,
      MatrixQ<T>& dsdu ) const {}


  // right-hand-side forcing function
  template <class T>
  void forcingFunction( const Real& x, const Real& y, const Real& time, ArrayQ<T>& forcing ) const {}

  // characteristic speed (needed for timestep)
  template <class T>
  void speedCharacteristic(
      const Real& x, const Real& y, const Real& time, const Real& dx, const Real& dy, const ArrayQ<T>& q, Real& speed ) const {}

  // characteristic speed
  template <class T>
  void speedCharacteristic(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q, Real& speed ) const {}


  // is state physically valid
  template <class T>
  bool isValidState( const ArrayQ<T>& q ) const { return varInterpret_.isValidState(q); }

  // update fraction needed for physically valid state
  template <class T>
  void updateFraction(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q, const ArrayQ<T>& dq,
      Real maxChangeFraction, Real& updateFraction ) const {}


  // set from variable array
  template<class T>
  void setDOFFrom( const ShallowWater2DVariableType<VarType>& data, ArrayQ<T>& var ) const
  {
    varInterpret_.setFromPrimitive( data.cast(), var );
  }

  // set from variable array
  ArrayQ<Real> setDOFFrom( const ShallowWater2DVariableType<VarType>& data ) const
  {
    ArrayQ<Real> var;
    varInterpret_.setFromPrimitive( data.cast(), var );
    return var;
  }

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const;

  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const;

  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const;

  // return the interp type
  ShallowWaterResidualInterpCategory category() const;

  // how many residual equations are we dealing with
  int nMonitor() const;

  void dump( int indentSize, std::ostream& out = std::cout ) const {}


  // accessors for parameters and variable interpreter
  Real getg() const { return g_; }

  const VarInterpret& getVarInterpreter() const { return varInterpret_; }

protected:
  const Real g_;                         // g: gravitational acceleration [m/s^2]
  const Real q0_;                        // q0 = v*H [m^2/s^2]
  const VarInterpret varInterpret_;      // solution variable interpreter class
  const SolutionType solution_;          // exact solution
  ShallowWaterResidualInterpCategory cat_; // ResidInterp type

}; // class PDEShallowWater<PhysD2, VarType, SolutionType>


//--------------------- Member function definitions ---------------------//

// unsteady conservative flux: U(Q)
template <class VarType, class SolutionType>
template <class T>
inline void
PDEShallowWater<PhysD2, VarType, SolutionType>::masterState(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, ArrayQ<T>& uCons ) const
{
  T H = 0;             // water height. Initialized to suppress compiler warnings
  DLA::VectorS<D,T> V; // velocity size-2 vector
  varInterpret_.getH( q, H );
  varInterpret_.getVelocity( q, V );

  uCons(0) = H;
  uCons(1) = H*V[0];
  uCons(2) = H*V[1];
}

// unsteady conservative flux Jacobian: dU(Q)/dQ
template <class VarType, class SolutionType>
template <class T>
inline void
PDEShallowWater<PhysD2, VarType, SolutionType>::jacobianMasterState(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
{
  SANS_DEVELOPER_EXCEPTION("Not implemented...");
}


// advective flux: F(Q)
#if ShallowWaterIntegrandManifold2D // use integrand_galerkin_manifold
  template <class VarType, class SolutionType>
  template <class T>
  inline void
  PDEShallowWater<PhysD2, VarType, SolutionType>::fluxAdvective(
      const VectorEIF& EIF,
      const DLA::VectorS<D,Real>& e1,
      const DLA::VectorS<D,Real>& e1x, const DLA::VectorS<D,Real>& e1z,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& var,
      ArrayQ<T>& fx, ArrayQ<T>& fy ) const
#else
  template <class VarType, class SolutionType>
  template <class Tq, class Tf>
  inline void
  PDEShallowWater<PhysD2, VarType, SolutionType>::fluxAdvective(
                          const Real& x, const Real& y, const Real& time,
                          const ArrayQ<Tq>& var,
                          ArrayQ<Tf>& fx, ArrayQ<Tf>& fy ) const
#endif
{
  Tq H = 0; // Initialized to suppress compiler warnings
  Tq vx, vy;             // x, y velocity components
  DLA::VectorS<D,Tq> V;
  varInterpret_.getH( var, H );
  varInterpret_.getVelocity( var, V );

  vx = V[0];
  vy = V[1];

  // advective flux terms in continuity residual
  fx[0] += H*vx;
  fy[0] += H*vy;

#if PDEShallowWater_sansgH2fluxAdvective2D  // 0.5*g*H^2 not included in advective flux
  // advective flux terms in momentum residual
  fx[1] += H*vx*vx;
  fy[1] += H*vx*vy;

  fx[2] += H*vy*vx;
  fy[2] += H*vy*vy;
#else  // 0.5*g*H^2 included in advective flux

#if ShallowWater_IsCircle_NotFlat
  Real theta = atan2(y,x);
  // advective flux terms in momentum residual
  fx[1] += H*vx*vx + 0.5*g_*H*H*(-sin(theta)) * (-sin(theta));
  fy[1] += H*vx*vy + 0.5*g_*H*H*(-sin(theta)) * (cos(theta));

  fx[2] += H*vy*vx + 0.5*g_*H*H*(-sin(theta)) * (cos(theta));
  fy[2] += H*vy*vy + 0.5*g_*H*H*(cos(theta)) * (cos(theta));

// The follow also works
//  fx[1] += H*vx*vx + 0.5*g_*H*H;
//  fy[1] += H*vx*vy;
//
//  fx[2] += H*vy*vx;
//  fy[2] += H*vy*vy + 0.5*g_*H*H;
#else
//  // advective flux terms in momentum residual
  fx[1] += H*vx*vx + 0.5*g_*H*H;
  fy[1] += H*vx*vy;

  fx[2] += H*vy*vx;
  fy[2] += H*vy*vy + 0.5*g_*H*H;

// The following also works
//  Real theta = atan2(0.1,0.2); // slope angle is hard-coded here
////  SANS_ASSERT_MSG( (theta - atan2(y,x)) < 1.e-15, "thetaTrue = %f but thetaCalc = %f @ x = %f, y = %f", theta, atan2(y,x), x, y );
//
//  fx[1] += H*vx*vx + 0.5*g_*H*H * cos(theta)*cos(theta);
//  fy[1] += H*vx*vy + 0.5*g_*H*H * cos(theta)*sin(theta);
//
//  fx[2] += H*vy*vx + 0.5*g_*H*H * sin(theta)*cos(theta);
//  fy[2] += H*vy*vy + 0.5*g_*H*H * sin(theta)*sin(theta);
#endif

#endif

}


template <class VarType, class SolutionType>
template<class Tq, class Tf>
inline void
PDEShallowWater<PhysD2, VarType, SolutionType>::fluxAdvectiveUpwind(
          const Real& x, const Real& y, const Real& time,
          const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
          const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const
{
  Tq HL = 0, HR = 0;  // Initialized to suppress compiler warnings
  DLA::VectorS<D,Tq> vL, vR;
  Tq vxL, vyL, vxR, vyR;
  Tq vnL, vnR;                     // velocity components normal to trace
  Tq alpha;                        // upwind coefficient

  varInterpret_.getH( qL, HL );
  varInterpret_.getVelocity( qL, vL );
  varInterpret_.getH( qR, HR );
  varInterpret_.getVelocity( qR, vR );

  vxL = vL[0];
  vyL = vL[1];
  vxR = vR[0];
  vyR = vR[1];

  Real tx = nx, ty = ny;
#if 0 // hack: construct the true trace normal vector on a circular manifold
  Real theta1 = atan2(y,x);

  if ( nx*vxL + ny*vyL > 0 )
  {
    tx = -sin(theta1);
    ty =  cos(theta1);
  }
  else
  {
    tx =  sin(theta1);
    ty = -cos(theta1);
  }
#endif

  vnL = vxL*tx + vyL*ty;
  vnR = vxR*tx + vyR*ty;

#if PDEShallowWater_sansgH2fluxAdvective2D  // 0.5*g*H^2 not included in advective flux
  alpha = std::max( fabs(vnL), fabs(vnR) ); // largest eigenvalue in absolute value of flux jacobians

  // local Lax-Friedrichs upwind flux: 0.5*(FL + FR).n + 0.5*(UL - UR)*alpha
  f[0] += 0.5*(HL*vnL + HR*vnR) + 0.5*(HL - HR)*alpha;
  f[1] += 0.5*(HL*vxL*vnL + HR*vxR*vnR) + 0.5*(HL*vxL - HR*vxR)*alpha;
  f[2] += 0.5*(HL*vyL*vnL + HR*vyR*vnR) + 0.5*(HL*vyL - HR*vyR)*alpha;
#else  // 0.5*g*H^2 included in advective flux

  alpha = std::max( (fabs(vnL) + sqrt(g_*HL)), (fabs(vnR) + sqrt(g_*HR)) ); // largest eigenvalue in absolute value of flux jacobians
  //  alpha = std::max( fabs(vnL), fabs(vnR) ); // this does not change results much

  // local Lax-Friedrichs upwind flux: 0.5*(FL + FR).n + 0.5*(UL - UR)*alpha
  f[0] += 0.5*(HL*vnL + HR*vnR) + 0.5*(HL - HR)*alpha;

#if ShallowWater_IsCircle_NotFlat
  Real theta = atan2(y,x);

  f[1] += 0.5*(HL*vxL*vnL - sin(theta)*0.5*g_*HL*HL*( -sin(theta)*tx + cos(theta)*ty )
             + HR*vxR*vnR - sin(theta)*0.5*g_*HR*HR*( -sin(theta)*tx + cos(theta)*ty ) )
        + 0.5*(HL*vxL - HR*vxR)*alpha;
  f[2] += 0.5*(HL*vyL*vnL + cos(theta)*0.5*g_*HL*HL*( -sin(theta)*tx + cos(theta)*ty )
             + HR*vyR*vnR + cos(theta)*0.5*g_*HR*HR*( -sin(theta)*tx + cos(theta)*ty ) )
        + 0.5*(HL*vyL - HR*vyR)*alpha;

// Alternative:
//  f[1] += 0.5*(HL*vxL*vnL + 0.5*g_*HL*HL*tx + HR*vxR*vnR + 0.5*g_*HR*HR*tx) + 0.5*(HL*vxL - HR*vxR)*alpha;
//  f[2] += 0.5*(HL*vyL*vnL + 0.5*g_*HL*HL*ty + HR*vyR*vnR + 0.5*g_*HR*HR*ty) + 0.5*(HL*vyL - HR*vyR)*alpha;

#else
  f[1] += 0.5*(HL*vxL*vnL + 0.5*g_*HL*HL*tx + HR*vxR*vnR + 0.5*g_*HR*HR*tx) + 0.5*(HL*vxL - HR*vxR)*alpha;
  f[2] += 0.5*(HL*vyL*vnL + 0.5*g_*HL*HL*ty + HR*vyR*vnR + 0.5*g_*HR*HR*ty) + 0.5*(HL*vyL - HR*vyR)*alpha;

// Alternative:
//  Real theta = atan2(0.1,0.2); // slope angle is hard-coded here
////SANS_ASSERT_MSG( (theta - atan2(y,x)) < 1.e-15, "thetaTrue = %f but thetaCalc = %f @ x = %f, y = %f", theta, atan2(y,x), x, y );
//
//  // local Lax-Friedrichs upwind flux: 0.5*(FL + FR).n + 0.5*(UL - UR)*alpha
//  f[1] += 0.5*(HL*vxL*vnL + 0.5*g_*HL*HL*( cos(theta)*cos(theta)*tx + cos(theta)*sin(theta)*ty )
//             + HR*vxR*vnR + 0.5*g_*HR*HR*( cos(theta)*cos(theta)*tx + cos(theta)*sin(theta)*ty ) )
//        + 0.5*(HL*vxL - HR*vxR)*alpha;
//  f[2] += 0.5*(HL*vyL*vnL + 0.5*g_*HL*HL*( sin(theta)*cos(theta)*tx + sin(theta)*sin(theta)*ty )
//             + HR*vyR*vnR + 0.5*g_*HR*HR*( sin(theta)*cos(theta)*tx + sin(theta)*sin(theta)*ty ) )
//        + 0.5*(HL*vyL - HR*vyR)*alpha;
#endif

#endif
}


template<class VarType, class SolutionType>
template<class T>
inline void
PDEShallowWater<PhysD2, VarType, SolutionType>::jacobianFluxAdvective(
          const Real& x, const Real& y, const Real& time, const ArrayQ<T>& var,
          MatrixQ<T>& dfdu, MatrixQ<T>& dgdu ) const
{
  T H = 0, vx, vy;
  DLA::VectorS<D,T> v;
  varInterpret_.getH( var, H );
  varInterpret_.getVelocity( var, v );

  vx = v[0];
  vy = v[1];

#if PDEShallowWater_sansgH2fluxAdvective2D  // 0.5*g*H^2 not included in advective flux
  dfdu(0,0) += 0;
  dfdu(0,1) += 1;
  dfdu(0,2) += 0;
  dfdu(1,0) += -vx*vx;
  dfdu(1,1) += 2*vx;
  dfdu(1,2) += 0;
  dfdu(2,0) += -vx*vy;
  dfdu(2,1) += vy;
  dfdu(2,2) += vx;

  dgdu(0,0) += 0;
  dgdu(0,1) += 0;
  dgdu(0,2) += 1;
  dgdu(1,0) += -vx*vy;
  dgdu(1,1) += vy;
  dgdu(1,2) += vx;
  dgdu(2,0) += -vy*vy;
  dgdu(2,1) += 0;
  dgdu(2,2) += 2*vy;
#else  // 0.5*g*H^2 included in advective flux

#if ShallowWater_IsCircle_NotFlat
  Real theta = atan2(y,x);

  dfdu(0,0) += 0;
  dfdu(0,1) += 1;
  dfdu(0,2) += 0;
  dfdu(1,0) += -vx*vx + g_*H * sin(theta)*sin(theta);
  dfdu(1,1) += 2*vx;
  dfdu(1,2) += 0;
  dfdu(2,0) += -vx*vy - g_*H * sin(theta)*cos(theta);
  dfdu(2,1) += vy;
  dfdu(2,2) += vx;

  dgdu(0,0) += 0;
  dgdu(0,1) += 0;
  dgdu(0,2) += 1;
  dgdu(1,0) += -vx*vy - g_*H * sin(theta)*cos(theta);
  dgdu(1,1) += vy;
  dgdu(1,2) += vx;
  dgdu(2,0) += -vy*vy + g_*H * cos(theta)*cos(theta);
  dgdu(2,1) += 0;
  dgdu(2,2) += 2*vy;

// Alternative:
//  dfdu(0,0) += 0;
//  dfdu(0,1) += 1;
//  dfdu(0,2) += 0;
//  dfdu(1,0) += -vx*vx + g_*H;
//  dfdu(1,1) += 2*vx;
//  dfdu(1,2) += 0;
//  dfdu(2,0) += -vx*vy;
//  dfdu(2,1) += vy;
//  dfdu(2,2) += vx;
//
//  dgdu(0,0) += 0;
//  dgdu(0,1) += 0;
//  dgdu(0,2) += 1;
//  dgdu(1,0) += -vx*vy;
//  dgdu(1,1) += vy;
//  dgdu(1,2) += vx;
//  dgdu(2,0) += -vy*vy + g_*H;
//  dgdu(2,1) += 0;
//  dgdu(2,2) += 2*vy;
#else
  dfdu(0,0) += 0;
  dfdu(0,1) += 1;
  dfdu(0,2) += 0;
  dfdu(1,0) += -vx*vx + g_*H;
  dfdu(1,1) += 2*vx;
  dfdu(1,2) += 0;
  dfdu(2,0) += -vx*vy;
  dfdu(2,1) += vy;
  dfdu(2,2) += vx;

  dgdu(0,0) += 0;
  dgdu(0,1) += 0;
  dgdu(0,2) += 1;
  dgdu(1,0) += -vx*vy;
  dgdu(1,1) += vy;
  dgdu(1,2) += vx;
  dgdu(2,0) += -vy*vy + g_*H;
  dgdu(2,1) += 0;
  dgdu(2,2) += 2*vy;

// Alternative:
//  Real theta = atan2(0.1,0.2); // slope angle is hard-coded here
////  SANS_ASSERT_MSG( (theta - atan2(y,x)) < 1.e-15, "thetaTrue = %f but thetaCalc = %f @ x = %f, y = %f", theta, atan2(y,x), x, y );
//
//  dfdu(0,0) += 0;
//  dfdu(0,1) += 1;
//  dfdu(0,2) += 0;
//  dfdu(1,0) += -vx*vx + g_*H * cos(theta)*cos(theta);
//  dfdu(1,1) += 2*vx;
//  dfdu(1,2) += 0;
//  dfdu(2,0) += -vx*vy + g_*H * cos(theta)*sin(theta);
//  dfdu(2,1) += vy;
//  dfdu(2,2) += vx;
//
//  dgdu(0,0) += 0;
//  dgdu(0,1) += 0;
//  dgdu(0,2) += 1;
//  dgdu(1,0) += -vx*vy + g_*H * sin(theta)*cos(theta);
//  dgdu(1,1) += vy;
//  dgdu(1,2) += vx;
//  dgdu(2,0) += -vy*vy + g_*H * sin(theta)*sin(theta);
//  dgdu(2,1) += 0;
//  dgdu(2,2) += 2*vy;
#endif

#endif
}


#if ShallowWaterHasStrongFluxAdvective2D
template <class VarType, class SolutionType>
template<class T>
inline void
PDEShallowWater<PhysD2, VarType, SolutionType>::strongFluxAdvective(
          const Real& x, const Real& y, const Real& time,
          const ArrayQ<T>& var, const ArrayQ<T>& varx, const ArrayQ<T>& vary,
          ArrayQ<T>& strongPDE ) const
{
  T H, vx, vy;
  DLA::VectorS<D,T> v;

  T H_x, vx_x, vy_x;            // derivatives w.r.t. x
  T H_y, vx_y, vy_y;            // derivatives w.r.t. y
  DLA::VectorS<D,T> v_x, v_y;

  varInterpret_.getH( var, H );
  varInterpret_.getVelocity( var, v );
  varInterpret_.getH( varx, H_x );
  varInterpret_.getVelocity( varx, v_x );
  varInterpret_.getH( vary, H_y );
  varInterpret_.getVelocity( vary, v_y );

  vx = v[0];
  vy = v[1];
  vx_x = v_x[0];
  vy_x = v_x[1];
  vx_y = v_y[0];
  vy_y = v_y[1];

#if PDEShallowWater_sansgH2fluxAdvective2D  // 0.5*g*H^2 not included in advective flux
  // d/dx parts
  strongPDE(0) += H_x*vx + H*vx_x;
  strongPDE(1) += H_x*vx*vx + 2*H*vx*vx_x;
  strongPDE(2) += H_x*vy*vx + H*vy_x*vx + H*vy*vx_x;

  // d/dy parts
  strongPDE(0) += H_y*vy + H*vy_y;
  strongPDE(1) += H_y*vy*vx + H*vy_y*vx + H*vy*vx_y;
  strongPDE(2) += H_y*vy*vy + 2*H*vy*vy_y;
#else  // 0.5*g*H^2 included in advective flux

//  // d/dx parts
//  strongPDE(0) += H_x*vx + H*vx_x;
//  strongPDE(1) += H_x*vx*vx + 2*H*vx*vx_x + g_*H*H_x;
//  strongPDE(2) += H_x*vy*vx + H*vy_x*vx + H*vy*vx_x;
//
//  // d/dy parts
//  strongPDE(0) += H_y*vy + H*vy_y;
//  strongPDE(1) += H_y*vy*vx + H*vy_y*vx + H*vy*vx_y;
//  strongPDE(2) += H_y*vy*vy + 2*H*vy*vy_y + g_*H*H_y;

  Real theta = atan2(y,x);

  // d/dx parts
  strongPDE(0) += H_x*vx + H*vx_x;
  strongPDE(1) += H_x*vx*vx + 2*H*vx*vx_x + g_*H*H_x*cos(theta)*cos(theta);
  strongPDE(2) += H_x*vy*vx + H*vy_x*vx + H*vy*vx_x + g_*H*H_x*sin(theta)*cos(theta);

  // d/dy parts
  strongPDE(0) += H_y*vy + H*vy_y;
  strongPDE(1) += H_y*vy*vx + H*vy_y*vx + H*vy*vx_y + g_*H*H_y*sin(theta)*cos(theta);
  strongPDE(2) += H_y*vy*vy + 2*H*vy*vy_y + g_*H*H_y*sin(theta)*sin(theta);

#endif

}
#endif


// solution-dependent source: S(X, Q, QX)
#if ShallowWaterIntegrandManifold2D // use integrand_galerkin_manifold
  template <class VarType, class SolutionType>
  template <class T>
  inline void
  PDEShallowWater<PhysD2, VarType, SolutionType>::source(
      const VectorEIF& EIF,
      const DLA::VectorS<D,Real>& e1,
      const DLA::VectorS<D,Real>& e1x, const DLA::VectorS<D,Real>& e1z,
      const Real& x, const Real& y, const Real& time,
      const Real& xxi, const Real& yxi,
      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
      const ArrayQ<T>& qxi,
      ArrayQ<T>& source ) const
#else
  template <class VarType, class SolutionType>
  template <class Tq, class Tg, class Ts>
  inline void
  PDEShallowWater<PhysD2, VarType, SolutionType>::source(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Ts>& source ) const
#endif
{
  Tq H = 0;
  Tg H_x = 0, H_y = 0;
  varInterpret_.getH( q, H );
  varInterpret_.getH( qx, H_x );
  varInterpret_.getH( qy, H_y );

  // compute gradient of bathymetry b(x,y)
  DLA::VectorS<D,Real> nablasb; //surface gradient of bathymetry b
  Real b_x = 0, b_y = 0;        // components of surface gradient of bathymetry b: db/dx & db/dy

  solution_.getBathymetryGrad(x,y,time,nablasb);

  b_x = nablasb[0];
  b_y = nablasb[1];

  // Update source terms
#if PDEShallowWater_sansgH2fluxAdvective2D  // 0.5*g*H^2 not included in advective flux
  source[0] += 0;

#if ShallowWater_IsCircle_NotFlat
  const Real r = sqrt(x*x+y*y);

  DLA::VectorS<D,T> V;
  varInterpret_.getVelocity( q, V );

  const T Speed = sqrt( dot(V,V) );

  source[1] += g_ * H * ( H_x - b_x ) + H*Speed*Speed*x/(r*r);
  source[2] += g_ * H * ( H_y - b_y ) + H*Speed*Speed*y/(r*r);
#else
  source[1] += g_ * H * ( H_x - b_x );
  source[2] += g_ * H * ( H_y - b_y );
#endif

#else  // 0.5*g*H^2 included in advective flux
  source[0] += 0;

#if ShallowWater_IsCircle_NotFlat
  const Real r = sqrt(x*x+y*y);

  DLA::VectorS<D,Tq> V;
  varInterpret_.getVelocity( q, V );

  const Tq Speed = sqrt( dot(V,V) );

  Real theta = atan2(y,x);

  source[1] += - g_ * H * b_x + H*Speed*Speed*x/(r*r) + 0.5*g_*H*H*cos(theta)/r;
  source[2] += - g_ * H * b_y + H*Speed*Speed*y/(r*r) + 0.5*g_*H*H*sin(theta)/r;
#else
  source[1] += - g_ * H * b_x;
  source[2] += - g_ * H * b_y;
#endif

#endif
}

// interpret residuals of the solution variable
template <class VarType, class SolutionType>
template<class T>
inline void
PDEShallowWater<PhysD2, VarType, SolutionType>::interpResidVariable(
    const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
{
  SANS_ASSERT( rsdPDEIn.M == N );

  int nOut = rsdPDEOut.m();

  if (cat_==ShallowWater_ResidInterp_Raw) // raw residual
  {
    SANS_ASSERT( nOut == 3 );
    for ( int i = 0; i < 3; i++ )
      rsdPDEOut[i] = rsdPDEIn[i];
  }
  else
  {
    std::cout << "Invalid Residual Interp category" << std::endl;
    SANS_ASSERT(false);
  }
}

// interpret residuals of the gradients in the solution variable
template <class VarType, class SolutionType>
template <class T>
inline void
PDEShallowWater<PhysD2, VarType, SolutionType>::interpResidGradient(
    const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const
{
  SANS_ASSERT( rsdAuxIn.M == N );

    int nOut = rsdAuxOut.m();

    if (cat_==ShallowWater_ResidInterp_Raw) // raw residual
    {
      SANS_ASSERT( nOut == 3 );
      for ( int i = 0; i < 3; i++ )
        rsdAuxOut[i] = rsdAuxIn[i];
    }
    else
    {
      std::cout << "Invalid Residual Interp category" << std::endl;
      SANS_ASSERT(false);
    }
}

// interpret residuals at the boundary (should forward to BCs)
template <class VarType, class SolutionType>
template <class T>
inline void
PDEShallowWater<PhysD2, VarType, SolutionType>::interpResidBC(
    const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const
{
  SANS_ASSERT( rsdBCIn.M == N );

  int nOut = rsdBCOut.m();

  if (cat_==ShallowWater_ResidInterp_Raw) // raw residual
  {
    SANS_ASSERT( nOut == 3 );
    for ( int i = 0; i < 3; i++ )
      rsdBCOut[i] = rsdBCIn[i];
  }
  else
  {
    std::cout << "Invalid Residual Interp category" << std::endl;
    SANS_ASSERT(false);
  }
}

// return the interp type
template <class VarType, class SolutionType>
typename PDEShallowWater<PhysD2, VarType, SolutionType>::ShallowWaterResidualInterpCategory
PDEShallowWater<PhysD2, VarType, SolutionType>::category() const
{
  return cat_;
}

// how many residual equations are we dealing with
template <class VarType, class SolutionType>
inline int
PDEShallowWater<PhysD2, VarType, SolutionType>::nMonitor() const
{
  int nMon;

  if (cat_==ShallowWater_ResidInterp_Raw) // raw residual
    nMon = 3;
  else
  {
    std::cout << "Invalid Residual Interp category" << std::endl;
    SANS_ASSERT(false);
  }

  return nMon;
}

} // namespace SANS


#endif /* SRC_PDE_SHALLOWWATER_PDESHALLOWWATER2D_H_ */
