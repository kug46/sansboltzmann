// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_SHALLOWWATER_VARINTERP1D_H_
#define SRC_PDE_SHALLOWWATER_VARINTERP1D_H_

// 2-D PDEShallowWater solution interpreter class

#include <iostream>

#include "ShallowWater_Traits.h"
#include "ShallowWaterVariable1D.h"

namespace SANS
{

//------------------------- VarInterpret1D<VarTypeHVelocity1D> -------------------------//
//
// template parameters:
//   T                      solution DOF data type (e.g. Surreal)
//
// primary variable vector/state vector (var):
//  [0] H:         water height
//  [1] u:         x-component of flow velocity
//
// member functions:
//   .getH
//   .getVelocity
//   .getHgrad
//   .isValidState
//   .setFromPrimitive
//
//----------------------------------------------------------------------------//
template <>
class VarInterpret1D<VarTypeHVelocity1D>
{
public:
  //-----------------------Fields-----------------------//
  typedef PhysD1 PhysDim;
  static const int D = ShallowWaterTraits<PhysDim>::D;               // physical dimensions
  static const int N = ShallowWaterTraits<PhysDim>::N;               // total number of solution variables

  //-----------------------Types-----------------------//
  template <class T>
  using ArrayQ = ShallowWaterTraits<PhysDim>::template ArrayQ<T>;    // solution/residual arrays
  // [Our compilers seem fine with missing or adding the keyword "template" before ArrayQ<T>, but it's good practice to keep it.]

  //-----------------------Constructors-----------------------//
  VarInterpret1D() = default;

  //-----------------------Destructors-----------------------//
  ~VarInterpret1D() {}
  explicit VarInterpret1D( const VarInterpret1D& ) = delete;
  VarInterpret1D& operator=( const VarInterpret1D& ) = delete;

  //-----------------------Member functions-----------------------//
  // get water height H
  template <class T>
  void getH( const ArrayQ<T>& var, T& H ) const;

  // get flow velocity v = {v_x, v_y}
  template <class T>
  void getVelocity( const ArrayQ<T>& var, DLA::VectorS<D,T>& v ) const;

  // check if state is physically valid
  bool isValidState( const ArrayQ<Real>& var ) const;

  // set state vector from primitive variable vector
  template<class T>
  void setFromPrimitive( const VarTypeHVelocity1D& data, ArrayQ<T>& var ) const;

  // print out parameters
  void dump( int indentSize, std::ostream& out = std::cout ) const;

}; // class VarInterpret1D<VarTypeHVelocity1D>


//--------------------- Inline member function definitions for VarInterpret1D<VarTypeHVelocity1D> ---------------------//
template <class T>
inline void
VarInterpret1D<VarTypeHVelocity1D>::getH( const ArrayQ<T>& var, T& H ) const
{
  H = var[0];
}


template <class T>
inline void
VarInterpret1D<VarTypeHVelocity1D>::getVelocity( const ArrayQ<T>& var, DLA::VectorS<D,T>& v ) const
{
  v = {var[1]};
}


inline bool
VarInterpret1D<VarTypeHVelocity1D>::isValidState( const ArrayQ<Real>& var ) const
{
  bool StateIsValid;
  Real H;

  H = var[0];

#if 0
  printf( "%s: H = %e \n", __PRETTY_FUNCTION__, H );
#endif

  // water height H should be positive
  if ( H >= 0 )
    StateIsValid = true;
  else
    StateIsValid = false;

  return StateIsValid;
}


template <class T>
inline void
VarInterpret1D<VarTypeHVelocity1D>::setFromPrimitive( const VarTypeHVelocity1D& data, ArrayQ<T>& var ) const
{
  var[0] = data.H_;
  var[1] = data.u_;
}

} // namespace SANS


#endif /* SRC_PDE_SHALLOWWATER_VARINTERP1D_H_ */
