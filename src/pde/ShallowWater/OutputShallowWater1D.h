// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef OUTPUTSHALLOWWATER1D_H_
#define OUTPUTSHALLOWWATER1D_H_


#include "PDEShallowWater1D.h"

#include "pde/OutputCategory.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Subfunctions to compute single output from shallow water solution variables

template <class QType, class PDENDConvert>
class OutputShallowWater1D_H : public OutputType< OutputShallowWater1D_H<QType,PDENDConvert> >
{
public:
  typedef PhysD1 PhysDim;

  typedef VarInterpret1D<QType> QInterpret;            // solution variable interpreter type

  template<class T>
  using ArrayQ = ShallowWaterTraits<PhysDim>::ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputShallowWater1D_H( const PDENDConvert& pde ) :
      pde_(pde), qInterpret_(pde_.getVarInterpreter()) {}

  bool needsSolutionGradient() const { return false; }

  template<class T>
  void operator()(const Real& x, const Real& time, const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayJ<T>& output) const
  {
    T H;

    qInterpret_.getH( q, H );

    output = H;
  }

private:
  const PDENDConvert& pde_;
  const QInterpret& qInterpret_;
};


template <class QType, class PDENDConvert>
class OutputShallowWater1D_V : public OutputType< OutputShallowWater1D_V<QType, PDENDConvert> >
{
public:
  typedef PhysD1 PhysDim;

  typedef VarInterpret1D<QType> QInterpret;            // solution variable interpreter type

  template<class T>
  using ArrayQ = ShallowWaterTraits<PhysDim>::ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputShallowWater1D_V( const PDENDConvert& pde ) :
      pde_(pde), qInterpret_(pde_.getVarInterpreter()) {}

  bool needsSolutionGradient() const { return false; }

  template<class T>
  void operator()(const Real& x, const Real& time, const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayJ<T>& output) const
  {
      DLA::VectorS<1,T> V;

      qInterpret_.getVelocity( q, V );

      output = V[0];
  }

private:
  const PDENDConvert& pde_;
  const QInterpret& qInterpret_;
};

} // namespace SANS


#endif /* OUTPUTSHALLOWWATER1D_H_ */
