// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_SHALLOWWATER_PDESHALLOWWATER1D_H_
#define SRC_PDE_SHALLOWWATER_PDESHALLOWWATER1D_H_

#define PDEShallowWater_sansgH2fluxAdvective1D 0
  // does not incorporate grad H in advective flux; i.e. semi-divergence form as opposed to full-divergence form

#define ShallowWaterHasStrongFluxAdvective1D 0  // has strong flux advective member function

#include "ShallowWaterSolution1D.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: 1-D Shallow water equations
//
// Reference: 2D_Manifold_PDE.pdf
//
// template parameters:
//   T                           solution DOF data type (e.g. double, SurrealS)
//
// Nodal state vector (var):
//  Depends on the definition in the solution interpreter class.
//
// Residual vector R
//  [0] R^(H)                    water height (or mass) conservation residual
//  [1] R^(u)                    x-component of momentum conservation residual
//
// member functions:
//   .hasFluxAdvectiveTime        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//   .hasFluxViscous             T/F: PDE has viscous flux term
//   .hasSource                  T/F: PDE has source term
//   .hasForcingFunction         T/F: PDE has forcing function term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: dU(Q)/dQ
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .fluxViscous                viscous fluxes: Fv(Q, QX)
//   .diffusionViscous           viscous diffusion coefficient: d(Fv)/d(UX)
//   .source                     solution-dependent sources: S(X, Q, QX)
//   .forcingFunction            forcing function

//   .isValidState               T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom                 set from primitive variable array
//
//   .getg
//----------------------------------------------------------------------------//

template <class VarType, class SolutionType>   // required for compiler to understand that a template is used
class PDEShallowWater<PhysD1, VarType, SolutionType>
{
public:

  enum ShallowWaterResidualInterpCategory
  {
    ShallowWater_ResidInterp_Raw
  };

  typedef PhysD1 PhysDim;

  //-----------------------Fields-----------------------//
  static const int D = ShallowWaterTraits<PhysDim>::D;               // physical dimensions
  static const int N = ShallowWaterTraits<PhysDim>::N;               // total solution variables

  //-----------------------Types-----------------------//
  template <class T>
  using ArrayQ = ShallowWaterTraits<PhysDim>::template ArrayQ<T>;    // solution/residual array type
  // [Our compilers seem fine with missing or adding the keyword "template" before ArrayQ<T>, but it's good practice to keep it.]

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using MatrixQ = ShallowWaterTraits<PhysDim>::template MatrixQ<T>;  // matrix type

  typedef VarInterpret1D<VarType> VarInterpret;            // solution variable interpreter type

  //-----------------------Constructors-----------------------//
  PDEShallowWater( const Real& q0, const PyDict& solnd, ShallowWaterResidualInterpCategory cat ):
    g_( 9.81 ),
    q0_( q0 ),
    varInterpret_(),
    solution_( solnd ),
    cat_(cat)
  {
    // Nothing
  }

  //-----------------------Destructors-----------------------//
  ~PDEShallowWater() {}
  PDEShallowWater( const PDEShallowWater& ) = delete;
  PDEShallowWater& operator=( const PDEShallowWater& )  = delete;

  //-----------------------Members-----------------------//
  // flux components
  bool hasFluxAdvectiveTime() const { return true; } // turned on for DG lifting operators though assuming steady-state for now
  bool hasFluxAdvective() const { return true; }
  bool hasFluxViscous() const { return false; }
  bool hasSource() const { return true; }
  bool hasSourceTrace() const { return false; }
  bool hasSourceLiftedQuantity() const { return false; }
  bool hasForcingFunction() const { return solution_.hasForcingFunction(); }

  bool needsSolutionGradientforSource() const
  {
#if PDEShallowWater_sansgH2fluxAdvective1D
    return true;
#else
    return false;
#endif
  }  // required for surface gradient of solution variables


  // unsteady temporal flux: Ft(Q)
  template <class T, class Tf>
  void fluxAdvectiveTime(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tf>& ft ) const
  {
    ArrayQ<Tf> ftLocal = 0;
    masterState(x, time, q, ftLocal);
    ft += ftLocal;
  }

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <class T, class Tf>
  void jacobianFluxAdvectiveTime(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& J ) const
  {
    J += DLA::Identity();
  }

  // master state: U(Q)
  template <class T>
  void masterState(
      const Real& x, const Real& time,
      const ArrayQ<T>& var, ArrayQ<T>& uCons ) const;

  // master state jacobian wrt q: dU(Q)/dQ
  template<class T>
  void jacobianMasterState(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const;

  // strong form of unsteady conservative flux: dU(Q)/dt
  template <class T>
  void strongFluxAdvectiveTime(
      const Real& x, const Real& time,
      const ArrayQ<T>& var, const ArrayQ<T>& vart, ArrayQ<T>& strongPDE ) const {}


  // advective flux: F(Q)
  template <class Tq, class Tf>
  void fluxAdvective( const Real& x, const Real& time, const ArrayQ<Tq>& var,
                      ArrayQ<Tf>& f ) const;

  template <class Tq, class Tf>
  void fluxAdvectiveUpwind(
      const Real& x, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, ArrayQ<Tf>& f ) const;

  template <class T>
  void fluxAdvectiveUpwindSpaceTime(
      const Real& x, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& nt, ArrayQ<T>& f ) const {}

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class T>
  void jacobianFluxAdvective(
      const Real& x, const Real& time, const ArrayQ<T>& var,
      MatrixQ<T>& a ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class T>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Real& x, const Real& time, const ArrayQ<T>& var,
      const Real& nx, MatrixQ<T>& a ) const {}

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class T>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Real& x, const Real& time, const ArrayQ<T>& q, const Real& nx, const Real& nt,
      MatrixQ<T>& a ) const {}

  // strong form advective fluxes
  template <class T>
  void strongFluxAdvective(
      const Real& x, const Real& time,
      const ArrayQ<T>& var, const ArrayQ<T>& varx,
      ArrayQ<T>& strongPDE ) const;


  // viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& var, const ArrayQ<Tg>& varx,
      ArrayQ<Tf>& f ) const {}

  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& varL, const ArrayQ<Tg>& varxL,
      const ArrayQ<Tq>& varR, const ArrayQ<Tg>& varxR,
      const Real& nx,  ArrayQ<Tf>& f ) const {}

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& var, const ArrayQ<Tg>& varx,
      MatrixQ<Tk>& kxx ) const {}

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class T>
  void jacobianFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& var, MatrixQ<T>& ax ) const {}

  // strong form viscous fluxes
  template <class T>
  void strongFluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<T>& var, const ArrayQ<T>& varx,
      const ArrayQ<T>& varxx,
      ArrayQ<T>& strongPDE ) const {}


  // solution-dependent source: S(X, Q, QX)
  template <class Tq, class Tg, class Ts>
  void source( const Real& x, const Real& time, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
               ArrayQ<Ts>& source ) const;

  // dual-consistent source
  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& xL,
      const Real& xR, const Real& time,
      const ArrayQ<Tq>& varL, const ArrayQ<Tg>& varxL,
      const ArrayQ<Tq>& varR, const ArrayQ<Tg>& varxR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class T>
  void jacobianSource(
      const Real& x, const Real& time,
      const ArrayQ<T>& var, const ArrayQ<T>& varx,
      MatrixQ<T>& dsdu ) const {}


  // right-hand-side forcing function
  void forcingFunction( const Real& x, const Real& time, ArrayQ<Real>& forcing ) const;

  // characteristic speed (needed for timestep)
  template <class T>
  void speedCharacteristic(
      const Real& x, const Real& time, const Real& dx, const ArrayQ<T>& q,
      Real& speed ) const {}

  // characteristic speed
  template <class T>
  void speedCharacteristic(
      const Real& x, const Real& time, const ArrayQ<T>& q,
      Real& speed ) const {}


  // is state physically valid
  template <class T>
  bool isValidState( const ArrayQ<T>& q ) const { return varInterpret_.isValidState(q); }

  // update fraction needed for physically valid state
  template <class T>
  void updateFraction(
      const Real& x, const Real& time, const ArrayQ<T>& q, const ArrayQ<T>& dq,
      Real maxChangeFraction, Real& updateFraction ) const {}


  // set from variable array
  template<class T>
  void setDOFFrom( const ShallowWater1DVariableType<VarType>& data, ArrayQ<T>& var ) const
  {
    varInterpret_.setFromPrimitive( data.cast(), var );
  }

  // set from variable array
  ArrayQ<Real> setDOFFrom( const ShallowWater1DVariableType<VarType>& data ) const
  {
    ArrayQ<Real> var;
    varInterpret_.setFromPrimitive( data.cast(), var );
    return var;
  }

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const;

  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const;

  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const;

  // return the interp type
  ShallowWaterResidualInterpCategory category() const;

  // how many residual equations are we dealing with
  int nMonitor() const;

  void dump( int indentSize, std::ostream& out = std::cout ) const {}


  // accessors for parameters and variable interpreter
  Real getg() const { return g_; }

  const VarInterpret& getVarInterpreter() const { return varInterpret_; }

protected:
  const Real g_;                         // g: gravitational acceleration [m/s^2]
  const Real q0_;                        // q0 = v*H [m^2/s^2]
  const VarInterpret varInterpret_;      // solution variable interpreter class
  const SolutionType solution_;          // exact solution
  ShallowWaterResidualInterpCategory cat_; // ResidInterp type

}; // class PDEShallowWater<PhysD1, VarType, SolutionType>


//--------------------- Member function definitions ---------------------//

// unsteady conservative flux: U(Q)
template <class VarType, class SolutionType>
template <class T>
inline void
PDEShallowWater<PhysD1, VarType, SolutionType>::masterState(
    const Real& x, const Real& time,
    const ArrayQ<T>& q, ArrayQ<T>& uCons ) const
{
  T H = 0;             // water height. Initialized to suppress compiler warnings
  DLA::VectorS<D,T> V; // velocity size-1 vector
  varInterpret_.getH( q, H );
  varInterpret_.getVelocity( q, V );

  uCons(0) = H;
  uCons(1) = H*V[0];
}

// unsteady conservative flux Jacobian: dU(Q)/dQ
template <class VarType, class SolutionType>
template <class T>
inline void
PDEShallowWater<PhysD1, VarType, SolutionType>::jacobianMasterState(
    const Real& x, const Real& time,
    const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
{
  SANS_DEVELOPER_EXCEPTION("Not implemented...");
}

// advective flux: F(Q)
template <class VarType, class SolutionType>
template <class Tq, class Tf>
inline void
PDEShallowWater<PhysD1, VarType, SolutionType>::fluxAdvective(
    const Real& x, const Real& time, const ArrayQ<Tq>& var,
    ArrayQ<Tf>& f ) const
{
  Tq H = 0;         // water height. Initialized to suppress compiler warnings
  Tq u;             // x velocity components.
  DLA::VectorS<D,Tq> V; // velocity size-1 vector
  varInterpret_.getH( var, H );
  varInterpret_.getVelocity( var, V );

  u = V[0];

  // advective flux terms in continuity residual
  f[0] += H*u;

#if PDEShallowWater_sansgH2fluxAdvective1D  // 0.5*g*H^2 not included in advective flux
  // advective flux terms in momentum residual
  f[1] += H*u*u;
#else  // 0.5*g*H^2 included in advective flux
  // advective flux terms in momentum residual
  f[1] += H*u*u + 0.5*g_*H*H;
#endif
}


template <class VarType, class SolutionType>
template<class Tq, class Tf>
inline void
PDEShallowWater<PhysD1, VarType, SolutionType>::fluxAdvectiveUpwind(
          const Real& x, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
          const Real& nx, ArrayQ<Tf>& f ) const
{
  Tq HL = 0, HR = 0;  // Initialized to suppress compiler warnings
  DLA::VectorS<D,Tq> VL, VR;
  Tq uL, uR;
  Tq alpha;                        // upwind coefficient

  varInterpret_.getH( qL, HL );
  varInterpret_.getVelocity( qL, VL );
  varInterpret_.getH( qR, HR );
  varInterpret_.getVelocity( qR, VR );

  uL = VL[0];
  uR = VR[0];

#if PDEShallowWater_sansgH2fluxAdvective1D  // 0.5*g*H^2 not included in advective flux

//  alpha = std::max( (fabs(uL*nx) + sqrt(g_*HL)), (fabs(uR*nx) + sqrt(g_*HR)) ); // largest eigenvalue in absolute value of flux jacobians
  alpha = std::max( fabs(uL*nx), fabs(uR*nx) ); // largest eigenvalue in absolute value of flux jacobians
  // both alpha leads to similar results in the sense that optimal convergence rate p+1 is achieved using lifting operators

  // local Lax-Friedrichs upwind flux: 0.5*(FL + FR).n + 0.5*(UL - UR)*alpha
  f[0] += 0.5*( HL*uL + HR*uR )*nx + 0.5*( HL - HR )*alpha;
  f[1] += 0.5*( HL*uL*uL + HR*uR*uR )*nx
        + 0.5*( HL*uL - HR*uR )*alpha;

#else  // 0.5*g*H^2 included in advective flux

  alpha = std::max( (fabs(uL*nx) + sqrt(g_*HL)), (fabs(uR*nx) + sqrt(g_*HR)) ); // largest eigenvalue in absolute value of flux jacobians

  // local Lax-Friedrichs upwind flux: 0.5*(FL + FR).n + 0.5*(UL - UR)*alpha
  f[0] += 0.5*( HL*uL + HR*uR )*nx + 0.5*(HL - HR)*alpha;
  f[1] += 0.5*( HL*uL*uL + 0.5*g_*HL*HL + HR*uR*uR + 0.5*g_*HR*HR )*nx
        + 0.5*(HL*uL - HR*uR)*alpha;

#endif
}


template<class VarType, class SolutionType>
template<class T>
inline void
PDEShallowWater<PhysD1, VarType, SolutionType>::jacobianFluxAdvective(
          const Real& x, const Real& time, const ArrayQ<T>& var,
          MatrixQ<T>& dfdu ) const
{
  T H, u;
  DLA::VectorS<D,T> V;
  varInterpret_.getH( var, H );
  varInterpret_.getVelocity( var, V );

  u = V[0];

#if PDEShallowWater_sansgH2fluxAdvective1D  // 0.5*g*H^2 not included in advective flux

  dfdu(0,0) += 0;
  dfdu(0,1) += 1;
  dfdu(1,0) += -u*u;
  dfdu(1,1) += 2*u;

#else  // 0.5*g*H^2 included in advective flux

  dfdu(0,0) += 0;
  dfdu(0,1) += 1;
  dfdu(1,0) += -u*u + g_*H;
  dfdu(1,1) += 2*u;

#endif
}

#if ShallowWaterHasStrongFluxAdvective1D
template <class VarType, class SolutionType>
template<class T>
inline void
PDEShallowWater<PhysD1, VarType, SolutionType>::strongFluxAdvective(
          const Real& x, const Real& time,
          const ArrayQ<T>& var, const ArrayQ<T>& varx,
          ArrayQ<T>& strongPDE ) const
{
  T H, u;
  DLA::VectorS<D,T> V;

  T H_x, u_x;            // derivatives w.r.t. x
  DLA::VectorS<D,T> V_x;

  varInterpret_.getH( var, H );
  varInterpret_.getVelocity( var, V );
  varInterpret_.getH( varx, H_x );
  varInterpret_.getVelocity( varx, V_x );

  u = V[0];
  u_x = V_x[0];

#if PDEShallowWater_sansgH2fluxAdvective1D  // 0.5*g*H^2 not included in advective flux
  // d/dx parts
  strongPDE(0) += H_x*u + H*u_x;
  strongPDE(1) += H_x*u*u + 2*H*u*u_x;
#else  // 0.5*g*H^2 included in advective flux
  // d/dx parts
  strongPDE(0) += H_x*u + H*u_x;
  strongPDE(1) += H_x*u*u + 2*H*u*u_x + g_*H*H_x;
#endif
}
#endif


// solution-dependent source: S(X, Q, QX)
template <class VarType, class SolutionType>
template <class Tq, class Tg, class Ts>
inline void
PDEShallowWater<PhysD1, VarType, SolutionType>::source(
    const Real& x, const Real& time, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    ArrayQ<Ts>& source ) const
{
  Tq H;
  Tg H_x;
  varInterpret_.getH( q, H );
  varInterpret_.getH( qx, H_x );

  // compute gradient of bathymetry b(x,y)
  DLA::VectorS<D,Real> nablasb; //surface gradient of bathymetry b
  Real b_x = 0;                 // components of surface gradient of bathymetry b: db/dx

  solution_.getBathymetryGrad(x,time,nablasb);

  b_x = nablasb[0];

  // Update source terms
#if PDEShallowWater_sansgH2fluxAdvective1D  // 0.5*g*H^2 not included in advective flux

  source[0] += 0;
  source[1] += g_ * H * ( H_x - b_x);

#else  // 0.5*g*H^2 included in advective flux

  source[0] += 0;
  source[1] += -g_ * H * b_x;

#endif
}

// right-hand-side forcing function
template <class VarType, class SolutionType>
inline void
PDEShallowWater<PhysD1, VarType, SolutionType>::forcingFunction(
    const Real& x, const Real& time, ArrayQ<Real>& forcing ) const
{
  ArrayQ<Real> RHS = 0;
  solution_.forcingFunction( x, time, RHS );
  forcing += RHS;
}

// interpret residuals of the solution variable
template <class VarType, class SolutionType>
template <class T>
inline void
PDEShallowWater<PhysD1, VarType, SolutionType>::interpResidVariable(
    const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
{
  SANS_ASSERT( rsdPDEIn.M == N );

  int nOut = rsdPDEOut.m();

  if (cat_==ShallowWater_ResidInterp_Raw) // raw residual
  {
    SANS_ASSERT_MSG( nOut == N, "nOut = %d whereas N = %d", nOut, N );
    for ( int i = 0; i < N; i++ )
      rsdPDEOut[i] = rsdPDEIn[i];
  }
  else
  {
    std::cout << "Invalid Residual Interp category" << std::endl;
    SANS_ASSERT(false);
  }
}

// interpret residuals of the gradients in the solution variable
template <class VarType, class SolutionType>
template <class T>
inline void
PDEShallowWater<PhysD1, VarType, SolutionType>::interpResidGradient(
    const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const
{
  SANS_ASSERT( rsdAuxIn.M == N );

  int nOut = rsdAuxOut.m();

  if (cat_==ShallowWater_ResidInterp_Raw) // raw residual
  {
    SANS_ASSERT_MSG( nOut == N, "nOut = %d whereas N = %d", nOut, N );
    for ( int i = 0; i < N; i++ )
      rsdAuxOut[i] = rsdAuxIn[i];
  }
  else
  {
    std::cout << "Invalid Residual Interp category" << std::endl;
    SANS_ASSERT(false);
  }
}

// interpret residuals at the boundary (should forward to BCs)
template <class VarType, class SolutionType>
template <class T>
inline void
PDEShallowWater<PhysD1, VarType, SolutionType>::interpResidBC(
    const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const
{
  SANS_ASSERT( rsdBCIn.M == N );

  int nOut = rsdBCOut.m();

  if (cat_==ShallowWater_ResidInterp_Raw) // raw residual
  {
    SANS_ASSERT_MSG( nOut == N, "nOut = %d whereas N = %d", nOut, N );
    for ( int i = 0; i < N; i++ )
      rsdBCOut[i] = rsdBCIn[i];
  }
  else
  {
    std::cout << "Invalid Residual Interp category" << std::endl;
    SANS_ASSERT(false);
  }
}

// return the interp type
template <class VarType, class SolutionType>
typename PDEShallowWater<PhysD1, VarType, SolutionType>::ShallowWaterResidualInterpCategory
PDEShallowWater<PhysD1, VarType, SolutionType>::category() const
{
  return cat_;
}

// how many residual equations are we dealing with
template <class VarType, class SolutionType>
inline int
PDEShallowWater<PhysD1, VarType, SolutionType>::nMonitor() const
{
  int nMon;

  if (cat_==ShallowWater_ResidInterp_Raw) // raw residual
    nMon = 2;
  else
  {
    std::cout << "Invalid Residual Interp category" << std::endl;
    SANS_ASSERT(false);
  }

  return nMon;
}

} // namespace SANS


#endif /* SRC_PDE_SHALLOWWATER_PDESHALLOWWATER1D_H_ */
