// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_SHALLOWWATER_SHALLOWWATERSOLUTION2D_H_
#define SRC_PDE_SHALLOWWATER_SHALLOWWATERSOLUTION2D_H_

// 2-D shallow water solutions

#include "VarInterp2D.h"

// Python must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// solution:
//  H(theta) = cos(theta)
//  H*v = q0
//
class ShallowWaterSolutionFunction2D_CosineTheta
{
public:
  typedef PhysD2 PhysDim; // required for solnNDSteadyConvert
  typedef VarInterpret2D<VarTypeHVelocity2D> QInterpret;            // solution variable interpreter type

  template<class T>
  using ArrayQ = ShallowWaterTraits<PhysD2>::ArrayQ<T>;

  using VectorX = ShallowWaterTraits<PhysD2>::VectorX;

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> g{"g", 9.81, 0, NO_LIMIT, "GravitationalAcceleration[m/s^2]"};
    const ParameterNumeric<Real> q0{"q0", NO_DEFAULT, NO_RANGE, "H*Vtheta"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

//  ShallowWaterSolutionFunction2D_CosineTheta() : qinterp_( ), g_(9.81), q0_(0) {}

  explicit ShallowWaterSolutionFunction2D_CosineTheta( const PyDict& d ) :
      qinterp_( ), g_(d.get(ParamsType::params.g)), q0_(d.get(ParamsType::params.q0)) {}

  explicit ShallowWaterSolutionFunction2D_CosineTheta( const Real& q0 ) :
      qinterp_( ), g_(9.81), q0_(q0) {}

  // overloaded operator ()
  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    const Real theta = atan2(y,x);

    Real H = cos(theta);
    Real vx = - sin(theta) * q0_/H;
    Real vy =   cos(theta) * q0_/H;

    ArrayQ<T> solnq = 0;
    VarTypeHVelocity2D vardata( H, vx, vy );
    qinterp_.setFromPrimitive( vardata, solnq );

    return solnq;
  }

  // compute surface gradient of bathymetry: i.e. grad b
  void getBathymetryGrad( const Real& x, const Real& y, const Real& time, VectorX& nablasb ) const
  {
    // polar coordinates (r,theta)
    const Real theta = atan2(y,x);
    const Real r = sqrt(x*x+y*y);

    // db/dx
    nablasb[0] = - sin(theta)*sin(theta) * ( -1 + q0_*q0_/( g_*pow(cos(theta),3) ) ) / r;

    // db/dy
    nablasb[1] =   cos(theta)*sin(theta) * ( -1 + q0_*q0_/( g_*pow(cos(theta),3) ) ) / r;
  }

  VectorX getBathymetryGrad( const Real& x, const Real& y, const Real& time ) const
  {
    VectorX nablasb;
    getBathymetryGrad( x, y, time, nablasb );
    return nablasb;
  }

  // print out parameters
  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ShallowWaterSolutionFunction2D_CosineTheta: g_ = " << g_
        << " and q0_ = " << q0_ << std::endl;
  }

private:
  const QInterpret qinterp_;
  const Real g_;
  const Real q0_;
}; // class ShallowWaterSolutionFunction2D_CosineTheta

//----------------------------------------------------------------------------//
// solution:
//  H(x,y) = 1 + x + ... + x^p, p >= 1
//  H*v = q0
//
class ShallowWaterSolutionFunction2D_GeometricSeriesx
{
public:
  typedef PhysD2 PhysDim; // required for solnNDSteadyConvert

  typedef VarInterpret2D<VarTypeHVelocity2D> QInterpret;            // solution variable interpreter type

  template<class T>
  using ArrayQ = ShallowWaterTraits<PhysD2>::ArrayQ<T>;

  using VectorX = ShallowWaterTraits<PhysD2>::VectorX;

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> g{"g", 9.81, 0, NO_LIMIT, "GravitationalAcceleration [m/s^2]"};
    const ParameterNumeric<Real> q0{"q0", NO_DEFAULT, NO_RANGE, "H*Vtheta"};
    const ParameterNumeric<int> p{"p", NO_DEFAULT, NO_RANGE, "PolynomialDegree"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ShallowWaterSolutionFunction2D_GeometricSeriesx( const PyDict& d ) :
      qinterp_( ), g_(d.get(ParamsType::params.g)), q0_(d.get(ParamsType::params.q0)),
      p_(d.get(ParamsType::params.p)) {}

  ShallowWaterSolutionFunction2D_GeometricSeriesx( const Real& q0, const int& p ) :
        qinterp_( ), g_(9.81), q0_(q0), p_(p) {}


  // overloaded operator ()
  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    if ( p_ < 0 )
      SANS_DEVELOPER_EXCEPTION( "p_ >= 0 is violated." );

    // Flat line
    T H = 0;

    // p = 0
    H = 1;

    // p > 0
    for ( int i = 1; i <= p_; i++ )
    {
      H += pow(x,i);
    }

    Real vx = q0_/H;
    Real vy = 0;

    ArrayQ<T> solnq = 0;
    VarTypeHVelocity2D vardata( H, vx, vy );
    qinterp_.setFromPrimitive( vardata, solnq );

    return solnq;
  }

  // compute surface gradient of bathymetry: i.e. grad b
  void getBathymetryGrad( const Real& x, const Real& y, const Real& time, VectorX& nablasb ) const
  {
    ArrayQ<Real> soln = (*this)(x,y,time);
    Real H;
    qinterp_.getH( soln, H );
    Real H_x = 0; // dH/dx

    // dH/dx
    if ( p_ < 0 )
      SANS_DEVELOPER_EXCEPTION( "p_ >= 0 is violated." );
    // this is in effect unnecessary because the overloaded operator() already throws an exception
    else if ( p_ == 0 )
      H_x = 0;
    else if ( p_ == 1 )
      H_x = 1;
    else
    {
      H_x = 1;
      for ( int i = 2; i <= p_; i++ )
        H_x += static_cast<Real>(i) * pow(x,i-1);
    }

    // db/dx = (1 - q0^2/(g*H^3))*dH/dx
    nablasb[0] = ( 1 - q0_*q0_/(g_*pow(H,3)) ) * H_x;

    // db/dy
    nablasb[1] = 0;
  }

  VectorX getBathymetryGrad( const Real& x, const Real& y, const Real& time ) const
  {
    VectorX nablasb;
    getBathymetryGrad( x, y, time, nablasb );
    return nablasb;
  }

  // print out parameters
  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ShallowWaterSolutionFunction2D_GeometricSeriesx: g_ = " << g_
        << ", q0_ = " << q0_
        << " and p_ = " << p_ << std::endl;
  }

private:
  const QInterpret qinterp_;
  const Real g_;
  const Real q0_;
  const int p_;
}; // class ShallowWaterSolutionFunction2D_GeometricSeriesx


//----------------------------------------------------------------------------//
// solution:
//  x = s * cos(alpha)
//  y = s * sin(alpha) where alpha is fixed
//
//  H(x,y) = 1 + s + ... + s^p, p >= 0
//  H*v = q0
//
class ShallowWaterSolutionFunction2D_GeometricSeriesSlopeRef
{
public:
  typedef PhysD2 PhysDim; // required for solnNDSteadyConvert

  typedef VarInterpret2D<VarTypeHVelocity2D> QInterpret;            // solution variable interpreter type

  template<class T>
  using ArrayQ = ShallowWaterTraits<PhysD2>::ArrayQ<T>;

  using VectorX = ShallowWaterTraits<PhysD2>::VectorX;

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> g{"g", 9.81, 0, NO_LIMIT, "GravitationalAcceleration [m/s^2]"};
    const ParameterNumeric<Real> q0{"q0", NO_DEFAULT, NO_RANGE, "H*Vtheta"};
    const ParameterNumeric<int> p{"p", NO_DEFAULT, NO_RANGE, "PolynomialDegree"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ShallowWaterSolutionFunction2D_GeometricSeriesSlopeRef( const PyDict& d ) :
      qinterp_( ), g_(d.get(ParamsType::params.g)), q0_(d.get(ParamsType::params.q0)),
      p_(d.get(ParamsType::params.p)) {}

  ShallowWaterSolutionFunction2D_GeometricSeriesSlopeRef( const Real& q0, const int& p ) :
        qinterp_( ), g_(9.81), q0_(q0), p_(p) {}


  // overloaded operator ()
  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    if ( p_ < 0 )
      SANS_DEVELOPER_EXCEPTION( "p_ >= 0 is violated." );

    const T s = sqrt(x*x+y*y);
    const T theta = atan2(y,x);

    // Flat line
    T H = 0;

    // p = 0
    H = 1;

    // p > 0
    for ( int i = 1; i <= p_; i++ )
    {
      H += pow(s,i);
    }

    Real vx = q0_/H * cos(theta);
    Real vy = q0_/H * sin(theta);

    ArrayQ<T> solnq = 0;
    VarTypeHVelocity2D vardata( H, vx, vy );
    qinterp_.setFromPrimitive( vardata, solnq );

    return solnq;
  }

  // compute surface gradient of bathymetry: i.e. grad b
  void getBathymetryGrad( const Real& x, const Real& y, const Real& time, VectorX& nablasb ) const
  {
    const Real s = sqrt(x*x+y*y);
    const Real theta = atan2(y,x);

    ArrayQ<Real> soln = (*this)(x,y,time);
    Real H;
    qinterp_.getH( soln, H );
    Real H_s = 0; // dH/ds

    // dH/dx
    if ( p_ < 0 )
      SANS_DEVELOPER_EXCEPTION( "p_ >= 0 is violated." );
    // this is in effect unnecessary because the overloaded operator() already throws an exception
    else if ( p_ == 0 )
      H_s = 0;
    else if ( p_ == 1 )
      H_s = 1;
    else
    {
      H_s = 1;
      for ( int i = 2; i <= p_; i++ )
        H_s += static_cast<Real>(i) * pow(s,i-1);
    }

    // db/ds = (1 - q0^2/(g*H^3))*dH/ds
    const Real b_s = ( 1 - q0_*q0_/(g_*pow(H,3)) ) * H_s;

    nablasb[0] = b_s * cos(theta); // db/dx
    nablasb[1] = b_s * sin(theta); // db/dy
  }

  VectorX getBathymetryGrad( const Real& x, const Real& y, const Real& time ) const
  {
    VectorX nablasb;
    getBathymetryGrad( x, y, time, nablasb );
    return nablasb;
  }

  // print out parameters
  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ShallowWaterSolutionFunction2D_GeometricSeriesSlopeRef: g_ = " << g_
        << ", q0_ = " << q0_
        << " and p_ = " << p_ << std::endl;
  }

private:
  const QInterpret qinterp_;
  const Real g_;
  const Real q0_;
  const int p_;
}; // class ShallowWaterSolutionFunction2D_GeometricSeriesSlopeRef


//----------------------------------------------------------------------------//
// solution:
//  x = r * cos(theta)
//  y = r * sin(theta) where r is fixed
//
//  H(theta) = 1 + theta + ... + theta^p, p >= 0
//  H*v = q0
//
class ShallowWaterSolutionFunction2D_GeometricSeriesTheta
{
public:
  typedef PhysD2 PhysDim; // required for solnNDSteadyConvert

  typedef VarInterpret2D<VarTypeHVelocity2D> QInterpret;            // solution variable interpreter type

  template<class T>
  using ArrayQ = ShallowWaterTraits<PhysD2>::ArrayQ<T>;

  using VectorX = ShallowWaterTraits<PhysD2>::VectorX;

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> g{"g", 9.81, 0, NO_LIMIT, "GravitationalAcceleration [m/s^2]"};
    const ParameterNumeric<Real> q0{"q0", NO_DEFAULT, NO_RANGE, "H*Vtheta"};
    const ParameterNumeric<int> p{"p", NO_DEFAULT, NO_RANGE, "PolynomialDegree"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ShallowWaterSolutionFunction2D_GeometricSeriesTheta( const PyDict& d ) :
      qinterp_( ), g_(d.get(ParamsType::params.g)), q0_(d.get(ParamsType::params.q0)),
      p_(d.get(ParamsType::params.p)) {}

  ShallowWaterSolutionFunction2D_GeometricSeriesTheta( const Real& q0, const int& p ) :
        qinterp_( ), g_(9.81), q0_(q0), p_(p) {}


  // overloaded operator ()
  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    if ( p_ < 0 )
      SANS_DEVELOPER_EXCEPTION( "p_ >= 0 is violated." );

    const T theta = atan2(y,x);

    // Flat line
    T H = 0;

    // p = 0
    H = 1;

    // p > 0
    for ( int i = 1; i <= p_; i++ )
    {
      H += pow(theta,i);
    }

    Real vx = - sin(theta) * q0_/H;
    Real vy =   cos(theta) * q0_/H;

    ArrayQ<T> solnq = 0;
    VarTypeHVelocity2D vardata( H, vx, vy );
    qinterp_.setFromPrimitive( vardata, solnq );

    return solnq;
  }

  template<class T>
  void gradient( const T& x, const T& y, const T& time, ArrayQ<T>& q, ArrayQ<T>& qx, ArrayQ<T>& qy, ArrayQ<T>& qt ) const
  {
    //TODO: This is a hack to get it working with the generic output functional classes. Gradients are not computed.
    //      This solution function class needs to be derived from Function2DVirtualInterface, so that gradients can be computed with
    //      automatic differentiation, if needed. Refer to SolutionFunction_NavierStokes2D_OjedaMMS for an example.
    q = this->operator ()(x, y, time);
  }

  // compute surface gradient of bathymetry: i.e. grad b
  void getBathymetryGrad( const Real& x, const Real& y, const Real& time, VectorX& nablasb ) const
  {
    const Real theta = atan2(y,x);
    const Real r = sqrt(x*x+y*y);

    ArrayQ<Real> soln = (*this)(x,y,time);
    Real H;
    qinterp_.getH( soln, H );
    Real H_theta = 0; // dH/dtheta

    // dH/dtheta
    if ( p_ < 0 )
      SANS_DEVELOPER_EXCEPTION( "p_ >= 0 is violated." );
      // this is in effect unnecessary because the overloaded operator() already throws an exception
    else if ( p_ == 0 )
      H_theta = 0;
    else if ( p_ == 1 )
      H_theta = 1;
    else
    {
      H_theta = 1;
      for ( int i = 2; i <= p_; i++ )
        H_theta += static_cast<Real>(i) * pow(theta,i-1);
    }

    // db/dtheta = (1 - q0^2/(g*H^3))*dH/dtheta
    const Real b_s = ( 1 - q0_*q0_/(g_*pow(H,3)) ) * H_theta / r;

    nablasb[0] = - sin(theta) * b_s; // db/dx
    nablasb[1] =   cos(theta) * b_s; // db/dy
  }

  VectorX getBathymetryGrad( const Real& x, const Real& y, const Real& time ) const
  {
    VectorX nablasb;
    getBathymetryGrad( x, y, time, nablasb );
    return nablasb;
  }

  // print out parameters
  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ShallowWaterSolutionFunction2D_GeometricSeriesSlopeRef: g_ = " << g_
        << ", q0_ = " << q0_
        << " and p_ = " << p_ << std::endl;
  }

private:
  const QInterpret qinterp_;
  const Real g_;
  const Real q0_;
  const int p_;
}; // class ShallowWaterSolutionFunction2D_GeometricSeriesTheta

} // namespace SANS



#endif /* SRC_PDE_SHALLOWWATER_SHALLOWWATERSOLUTION2D_H_ */
