// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_SHALLOWWATER_VARINTERP2D_H_
#define SRC_PDE_SHALLOWWATER_VARINTERP2D_H_

// 2-D PDEShallowWater solution interpreter class

#include <iostream>

#include "ShallowWater_Traits.h"
#include "ShallowWaterVariable2D.h"

namespace SANS
{

//------------------------- VarInterpret2D<VarTypeHVelocity2D> -------------------------//
//
// template parameters:
//   T                      solution DOF data type (e.g. Surreal)
//
// primary variable vector/state vector (var):
//  [0] H:         water height
//  [1] v_x:       x-component of flow velocity
//  [2] v_y:       y-component of flow velocity
//
// member functions:
//   .getH
//   .getVelocity
//   .getHgrad
//   .isValidState
//   .setFromPrimitive
//
//----------------------------------------------------------------------------//
template <>
class VarInterpret2D<VarTypeHVelocity2D>
{
public:
  //-----------------------Fields-----------------------//
  static const int D = ShallowWaterTraits<PhysD2>::D;               // physical dimensions

  //-----------------------Types-----------------------//
  template <class T>
  using ArrayQ = ShallowWaterTraits<PhysD2>::template ArrayQ<T>;    // solution/residual arrays
  // [Our compilers seem fine with missing or adding the keyword "template" before ArrayQ<T>, but it's good practice to keep it.]

  //-----------------------Constructors-----------------------//
  VarInterpret2D() = default;

  //-----------------------Destructors-----------------------//
  ~VarInterpret2D() {}
  explicit VarInterpret2D( const VarInterpret2D& ) = delete;
  VarInterpret2D& operator=( const VarInterpret2D& ) = delete;

  //-----------------------Member functions-----------------------//
  // get water height H
  template <class T>
  void getH( const ArrayQ<T>& var, T& H ) const;

  // get flow velocity v = {v_x, v_y}
  template <class T>
  void getVelocity( const ArrayQ<T>& var, DLA::VectorS<D,T>& v ) const;

  // check if state is physically valid
  bool isValidState( const ArrayQ<Real>& var ) const;

  // set state vector from primitive variable vector
  template<class T>
  void setFromPrimitive( const VarTypeHVelocity2D& data, ArrayQ<T>& var ) const;

  // print out parameters
  void dump( int indentSize, std::ostream& out = std::cout ) const;

}; // class VarInterpret2D<VarTypeHVelocity2D>


//--------------------- Inline member function definitions for VarInterpret2D<VarTypeHVelocity2D> ---------------------//
template <class T>
inline void
VarInterpret2D<VarTypeHVelocity2D>::getH( const ArrayQ<T>& var, T& H ) const
{
  H = var[0];
}


template <class T>
inline void
VarInterpret2D<VarTypeHVelocity2D>::getVelocity( const ArrayQ<T>& var, DLA::VectorS<D,T>& v ) const
{
  v = {var[1], var[2]};
}


inline bool
VarInterpret2D<VarTypeHVelocity2D>::isValidState( const ArrayQ<Real>& var ) const
{
  bool StateIsValid;
  Real H;

  H = var[0];

#if 0
  printf( "%s: H = %e \n", __PRETTY_FUNCTION__, H );
#endif

  // water height H should be positive
  if ( H >= 0 )
    StateIsValid = true;
  else
    StateIsValid = false;

  return StateIsValid;
}


template <class T>
inline void
VarInterpret2D<VarTypeHVelocity2D>::setFromPrimitive( const VarTypeHVelocity2D& data, ArrayQ<T>& var ) const
{
  var[0] = data.H_;
  var[1] = data.vx_;
  var[2] = data.vy_;
}

} // namespace SANS



#endif /* SRC_PDE_SHALLOWWATER_VARINTERP2D_H_ */
