// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_SHALLOWWATER_BCSHALLOWWATER2D_H_
#define SRC_PDE_SHALLOWWATER_BCSHALLOWWATER2D_H_

#define ShallowWaterBC_ConvervativeDirichlet2D 0 // set Dirchlet data on conservative variables (as opposed to primitive variables)

#define ShallowWaterHasLGStrongBC2D 0 // has strongBC lagrange multiplier version

//PyDict must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <string>

#include "pde/BCCategory.h"
#include "pde/BCNone.h"

#include <boost/mpl/vector.hpp>

#include "PDEShallowWater2D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// 2-D shallow water BC class
//
// template parameters:
//   BCType               BC type (e.g. BCTypeInflowSupercritical)
//   PDEShallowWater2D    PDE class
//     --> QType          solution variable type (e.g. primitive variable set)
//----------------------------------------------------------------------------//

template <class BCType, class PDEShallowWater2D>
class BCShallowWater2D;

// BCParam: BC parameters
template <class BCType>
struct BCShallowWater2DParams;

// BCType: BC types
class BCTypeInflowSupercritical;

//----------------------------------------------------------------------------//
// supercritical inflow BC:
//     analogous to supersonic inflow BC for Euler
//
// specify full state: water height, velocity
//----------------------------------------------------------------------------//

template<>
struct BCShallowWater2DParams<BCTypeInflowSupercritical> : noncopyable
{
  const ParameterNumeric<Real> H{"H", NO_DEFAULT, NO_RANGE, "WaterHeight"}; // TODO: maybe specify range [0,inf)
  const ParameterNumeric<Real> vx{"vx", NO_DEFAULT, NO_RANGE, "VelocityX"};
  const ParameterNumeric<Real> vy{"vy", NO_DEFAULT, NO_RANGE, "VelocityY"};

  static constexpr const char* BCName{"InflowSupercritical"};
  struct Option
  {
    const DictOption InflowSupercritical{BCShallowWater2DParams::BCName, BCShallowWater2DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCShallowWater2DParams params;
};


template <class QType, class SolutionType>
class BCShallowWater2D< BCTypeInflowSupercritical, PDEShallowWater<PhysD2,QType,SolutionType> > :
    public BCType< BCShallowWater2D<BCTypeInflowSupercritical, PDEShallowWater<PhysD2,QType,SolutionType>> >
{
public:
  typedef PhysD2 PhysDim;
  typedef typename BCCategory::Dirichlet_sansLG Category;
  typedef BCShallowWater2DParams<BCTypeInflowSupercritical> ParamsType;
  typedef typename PDEShallowWater<PhysDim,QType,SolutionType>::VarInterpret QInterpret;      // solution variable interpreter type

  static const int D = ShallowWaterTraits<PhysDim>::D;   // physical dimensions
  static const int N = ShallowWaterTraits<PhysDim>::N;   // total solution variables

  static const int NBC = 3;                       // total BCs

  template <class T>
  using ArrayQ = ShallowWaterTraits<PhysDim>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = ShallowWaterTraits<PhysDim>::template MatrixQ<T>;  // matrices

  // cppcheck-suppress noExplicitConstructor
  BCShallowWater2D( const PDEShallowWater<PhysDim,QType,SolutionType>& pde, const ArrayQ<Real>& bcdata ) :
      pde_(pde), qInterpret_(pde_.getVarInterpreter()), bcdata_(bcdata),
      H_(bcdata(0)), vx_(bcdata(1)), vy_(bcdata(2)) {}

  BCShallowWater2D(const PDEShallowWater<PhysDim,QType,SolutionType>& pde, PyDict& d ) :
      pde_(pde), qInterpret_(pde_.getVarInterpreter()),
      bcdata_({d.get(ParamsType::params.H), d.get(ParamsType::params.vx), d.get(ParamsType::params.vy)} ),
      H_(bcdata_(0)), vx_(bcdata_(1)), vy_(bcdata_(2)) {}

  ~BCShallowWater2D() {}
  BCShallowWater2D& operator=( const BCShallowWater2D& ) = delete;

  // BC residual: a(u) - b
  template <class T>
  void strongBC( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<T>& rsdBC ) const;

  template <class T>
  void strongBC( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& lg, ArrayQ<T>& rsdBC ) const;

  // conventional formulation BC weighting function
  // B^t = Fn M A^t ( A M A^t )^{-1}
  template <class T>
  void weightBC( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<T>& wghtBC ) const;

  // Lagrange multiplier weighting function
  // \bar{A}^t = Fn N
  template <class T>
  void weightLagrange( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                       const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<T>& wghtLG ) const;

  // Lagrange multiplier rhs
  // B = ( N^t M^{-1} N )^{-1} N^t M^{-1}
  template <class T>
  void rhsLagrange( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<T>& rhsLG ) const;

  // BC data
  template <class T>
  void data( const Real& x, const Real& y, const Real& time,
             const Real& nx, const Real& ny, ArrayQ<T>& bcdata ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDEShallowWater<PhysD2,QType,SolutionType>& pde_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const ArrayQ<Real> bcdata_;
  const Real H_;
  const Real vx_;
  const Real vy_;

};


template <class QType, class SolutionType>
template <class T>
inline void
BCShallowWater2D<BCTypeInflowSupercritical, PDEShallowWater<PhysD2,QType,SolutionType>>::strongBC(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<T>& rsdBC ) const
{
  T H = 0;
  DLA::VectorS<D,T> v = 0;

  qInterpret_.getH( q, H );
  qInterpret_.getVelocity( q, v );

  rsdBC(0) = H - H_;

#if ShallowWaterBC_ConvervativeDirichlet2D
  rsdBC(1) = H*v[0] - H_*vx_;
  rsdBC(2) = H*v[1] - H_*vy_;
#else
  rsdBC(1) = v[0] - vx_;
  rsdBC(2) = v[1] - vy_;
#endif
}


#if ShallowWaterHasLGStrongBC2D
template <class QType, class SolutionType>
template <class T>
inline void
BCShallowWater2D<BCTypeInflowSupercritical, PDEShallowWater<PhysD2,QType,SolutionType>>::strongBC(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& lg, ArrayQ<T>& rsdBC ) const
{
  T H = 0;
  DLA::VectorS<D,T> v = 0;

  qInterpret_.getH( q, H );
  qInterpret_.getVelocity( q, v );

  rsdBC(0) = H - H_;

#if ShallowWaterBC_ConvervativeDirichlet2D
  rsdBC(1) = H*v[0] - H_*vx_;
  rsdBC(2) = H*v[1] - H_*vy_;
#else
  rsdBC(1) = v[0] - vx_;
  rsdBC(2) = v[1] - vy_;
#endif
}
#endif


template <class QType, class SolutionType>
template <class T>
inline void
BCShallowWater2D<BCTypeInflowSupercritical, PDEShallowWater<PhysD2,QType,SolutionType>>::weightBC(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<T>& weightBC ) const
{
  MatrixQ<T> F = 0, G = 0;

  pde_.jacobianFluxAdvective( x, y, time, q, F, G );

  weightBC = nx*F + ny*G;
}


template <class QType, class SolutionType>
template <class T>
inline void
BCShallowWater2D<BCTypeInflowSupercritical, PDEShallowWater<PhysD2,QType,SolutionType>>::data(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    ArrayQ<T>& bcdata ) const
{
  bcdata = bcdata_;
}

//template <class QType>
//void
//BCShallowWater2D<BCTypeInflowSupercritical, PDEShallowWater<PhysD2,QType>>::dump( int indentSize, std::ostream& out ) const
//{
//  std::string indent(indentSize, ' ');
//
//  out << indent << "BCShallowWater2D<BCTypeInflowSupercritical, PDEShallowWater<PhysD2,QType>>: qInterpret_ = " << std::endl;
//  qInterpret_.dump(indentSize+2, out);
//
//  out << indent << "BCShallowWater2D<BCTypeInflowSupercritical, PDEShallowWater<PhysD2,QType>>: bcdata_ = " << std::endl;
//  bcdata_.dump(indentSize+2, out);
//}
//
template<class QType, class SolutionType>
using BCShallowWater2DVector = boost::mpl::vector2< BCShallowWater2D< BCTypeInflowSupercritical, PDEShallowWater<PhysD2,QType,SolutionType> >,
                                                    BCNone<PhysD2,PDEShallowWater<PhysD2,QType,SolutionType>::N>
                                                  >;

} // namespace SANS

#endif /* SRC_PDE_SHALLOWWATER_BCSHALLOWWATER2D_H_ */
