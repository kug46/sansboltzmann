// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "ShallowWaterSolution2D_manifold.h"

namespace SANS
{

// cppcheck-suppress passedByValue
void ShallowWaterSolutionFunction2D_manifold_Ellipse_GeometricSeriesTheta::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.g));
  allParams.push_back(d.checkInputs(params.q0));
  allParams.push_back(d.checkInputs(params.p));
  allParams.push_back(d.checkInputs(params.a));
  allParams.push_back(d.checkInputs(params.b));
  d.checkUnknownInputs(allParams);
}
ShallowWaterSolutionFunction2D_manifold_Ellipse_GeometricSeriesTheta::ParamsType
ShallowWaterSolutionFunction2D_manifold_Ellipse_GeometricSeriesTheta::ParamsType::params;

} // namespace SANS
