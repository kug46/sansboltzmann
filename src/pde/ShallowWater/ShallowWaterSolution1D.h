// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_SHALLOWWATER_SHALLOWWATERSOLUTION1D_H_
#define SRC_PDE_SHALLOWWATER_SHALLOWWATERSOLUTION1D_H_

// 1-D shallow water solutions

#include "VarInterp1D.h"

// Python must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "pde/AnalyticFunction/Function1D.h"

namespace SANS
{


//----------------------------------------------------------------------------//
// solution:
//  H(x) = 1 + x + ... + x^p, p >= 0
//  H*v = q0
//
class ShallowWaterSolutionFunction1D_GeometricSeriesx
{
public:
  typedef PhysD1 PhysDim; // required for solnNDSteadyConvert

  typedef VarInterpret1D<VarTypeHVelocity1D> QInterpret;            // solution variable interpreter type

  template<class T>
  using ArrayQ = ShallowWaterTraits<PhysDim>::ArrayQ<T>;

  using VectorX = ShallowWaterTraits<PhysDim>::VectorX;

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> g{"g", 9.81, NO_RANGE, "GravitationalAcceleration [m/s^2]"};
    const ParameterNumeric<Real> q0{"q0", NO_DEFAULT, NO_RANGE, "H*u"};
    const ParameterNumeric<int> p{"p", NO_DEFAULT, NO_RANGE, "PolynomialDegree"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ShallowWaterSolutionFunction1D_GeometricSeriesx( const PyDict& d ) :
      qinterp_( ), g_(d.get(ParamsType::params.g)), q0_(d.get(ParamsType::params.q0)),
      p_(d.get(ParamsType::params.p)) {}

  ShallowWaterSolutionFunction1D_GeometricSeriesx( const Real& q0, const int& p ) :
        qinterp_( ), g_(9.81), q0_(q0), p_(p) {}

//  ShallowWaterSolutionFunction1D_GeometricSeriesx( const Real& g, const Real& q0, const int& p ) :
//      qinterp_( ), g_(g), q0_(q0), p_(p) {}

  // overloaded operator ()
  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
    if ( p_ < 0 )
      SANS_DEVELOPER_EXCEPTION( "p_ >= 0 is violated." );

    // Flat line
    T H = 0;

    // p = 0
    H = 1;

    // p > 0
    for ( int i = 1; i <= p_; i++ )
    {
      H += pow(x,i);
    }

    Real vx = q0_/H;

    ArrayQ<T> solnq = 0;
    VarTypeHVelocity1D vardata( H, vx );
    qinterp_.setFromPrimitive( vardata, solnq );

    return solnq;
  }

  // compute surface gradient of bathymetry: i.e. grad b
  void getBathymetryGrad( const Real& x, const Real& time, VectorX& nablasb ) const;

  VectorX getBathymetryGrad( const Real& x, const Real& time ) const;


  bool hasForcingFunction() const { return false; }

  // compute forcing function
  void forcingFunction( const Real& x, const Real& time, DLA::VectorS<2,Real>& forcing ) const;

  // print out parameters
  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ShallowWaterSolutionFunction1D_GeometricSeriesx: g_ = " << g_
        << ", q0_ = " << q0_
        << " and p_ = " << p_ << std::endl;
  }

private:
  const QInterpret qinterp_;
  const Real g_;
  const Real q0_;
  const int p_;
}; // class ShallowWaterSolutionFunction1D_GeometricSeriesx


inline ShallowWaterSolutionFunction1D_GeometricSeriesx::VectorX
ShallowWaterSolutionFunction1D_GeometricSeriesx::getBathymetryGrad(
    const Real& x, const Real& time ) const
{
  VectorX nablasb;
  getBathymetryGrad( x, time, nablasb );
  return nablasb;
}


// compute surface gradient of bathymetry: i.e. grad b
inline void
ShallowWaterSolutionFunction1D_GeometricSeriesx::getBathymetryGrad(
    const Real& x, const Real& time, VectorX& nablasb ) const
{
  ArrayQ<Real> soln = (*this)(x,time);
  Real H;
  qinterp_.getH( soln, H );
  Real H_x = 0; // dH/dx

  // dH/dx
  if ( p_ < 0 )
    SANS_DEVELOPER_EXCEPTION( "p_ >= 0 is violated." );
    // this is in effect unnecessary because the overloaded operator() already throws an exception
  else if ( p_ == 0 )
    H_x = 0;
  else if ( p_ == 1 )
    H_x = 1;
  else
  {
    H_x = 1;
    for ( int i = 2; i <= p_; i++ )
      H_x += static_cast<Real>(i) * pow(x,i-1);
  }

  // db/dx = (1 - q0^2/(g*H^3))*dH/dx
  nablasb[0] = ( 1 - q0_*q0_/(g_*pow(H,3)) ) * H_x;
}

inline void
ShallowWaterSolutionFunction1D_GeometricSeriesx::forcingFunction(
    const Real& x, const Real& time, DLA::VectorS<2,Real>& forcing ) const
{
  forcing[0] = 0;
  forcing[1] = 0;
}

//----------------------------------------------------------------------------//
// solution:
//  H(x) = cH*( 1 + x + ... + x^pH ), pH >= 0
//  u(x) = 1 + x + ... + x^pu, pu >= 0
//  db/dx = 0
//
class ShallowWaterSolutionFunction1D_GeometricSeriesx_Forcing
{
public:
  typedef PhysD1 PhysDim; // required for solnNDSteadyConvert

  typedef VarInterpret1D<VarTypeHVelocity1D> QInterpret;            // solution variable interpreter type

  template<class T>
  using ArrayQ = ShallowWaterTraits<PhysDim>::ArrayQ<T>;

  using VectorX = ShallowWaterTraits<PhysDim>::VectorX;

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> g{"g", 9.81, NO_RANGE, "GravitationalAcceleration"};
    const ParameterNumeric<int> pH{"pH", NO_DEFAULT, NO_RANGE, "PolynomialDegreeH"};
    const ParameterNumeric<Real> cH{"cH", NO_DEFAULT, NO_RANGE, "MultiplePolynomialH"};
    const ParameterNumeric<int> pu{"pu", NO_DEFAULT, NO_RANGE, "PolynomialDegreeH"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit ShallowWaterSolutionFunction1D_GeometricSeriesx_Forcing( const PyDict& d ) :
      qinterp_( ), g_(d.get(ParamsType::params.g)), pH_(d.get(ParamsType::params.pH)),
      cH_(d.get(ParamsType::params.cH)), pu_(d.get(ParamsType::params.pu)) {}

  ShallowWaterSolutionFunction1D_GeometricSeriesx_Forcing( const int& pH, const Real& cH, const int& pu ) :
        qinterp_( ), g_(9.81), pH_(pH), cH_(cH), pu_(pu) {}

  // overloaded operator ()
  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
    if ( (pH_ < 0) || (pu_ < 0) )
      SANS_DEVELOPER_EXCEPTION( "pH_ >= 0 or pu_ >= 0 is violated." );

    // Flat line
    T H = 0, u = 0;

    // pH = 0
    H = 1;

    // pH > 0
    for ( int i = 1; i <= pH_; i++ )
    {
      H += pow(x,i);
    }

    H *= cH_;

    // pu = 0
    u = 1;

    // pH > 0
    for ( int i = 1; i <= pu_; i++ )
    {
      u += pow(x,i);
    }

    ArrayQ<T> solnq = 0;
    VarTypeHVelocity1D vardata( H, u );
    qinterp_.setFromPrimitive( vardata, solnq );

    return solnq;
  }

  template<class T>
  void gradient( const T& x, const T& time, ArrayQ<T>& q, ArrayQ<T>& qx, ArrayQ<T>& qt ) const
  {
    //TODO: This is a hack to get it working with the generic output functional classes. Gradients are not computed.
    //      This solution function class needs to be derived from Function1DVirtualInterface, so that gradients can be computed with
    //      automatic differentiation, if needed. Refer to SolutionFunction_NavierStokes2D_OjedaMMS for an example.
    q = this->operator ()(x, time);
  }

  // compute surface gradient of bathymetry: i.e. grad b
  void getBathymetryGrad( const Real& x, const Real& time, VectorX& nablasb ) const;

  VectorX getBathymetryGrad( const Real& x, const Real& time ) const;

  // whether has forcing function
  bool hasForcingFunction() const { return true; }

  // compute forcing function
  void forcingFunction( const Real& x, const Real& time, DLA::VectorS<2,Real>& forcing ) const;

  // print out parameters
  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "ShallowWaterSolutionFunction1D_GeometricSeriesx: g_ = " << g_
        << ", pH_ = " << pH_
        << ", cH_ = " << cH_
        << " and pu_ = " << pu_ << std::endl;
  }

private:
  const QInterpret qinterp_;
  const Real g_;
  const int pH_;
  const Real cH_;
  const int pu_;
}; // class ShallowWaterSolutionFunction1D_GeometricSeriesx


// compute surface gradient of bathymetry: i.e. grad b
inline void
ShallowWaterSolutionFunction1D_GeometricSeriesx_Forcing::getBathymetryGrad(
    const Real& x, const Real& time, VectorX& nablasb ) const
{
  // db/dx = 0
  nablasb[0] = 1;
}


inline
ShallowWaterSolutionFunction1D_GeometricSeriesx_Forcing::VectorX
ShallowWaterSolutionFunction1D_GeometricSeriesx_Forcing::getBathymetryGrad(
    const Real& x, const Real& time ) const
{
  VectorX nablasb;
  getBathymetryGrad( x, time, nablasb );
  return nablasb;
}


// forcing function
inline void
ShallowWaterSolutionFunction1D_GeometricSeriesx_Forcing::forcingFunction(
    const Real& x, const Real& time, DLA::VectorS<2,Real>& forcing ) const
{
  ArrayQ<Real> RHS = 0;
  VectorX nablasb = 0;

  ArrayQ<Real> soln = (*this)(x,time);
  VectorX V;
  Real H, u;
  qinterp_.getH( soln, H );
  qinterp_.getVelocity( soln, V );

  u = V[0];

  // dH/dx
  Real H_x = 0; // dH/dx
  if ( pH_ < 0 )
    SANS_DEVELOPER_EXCEPTION( "pH_ >= 0 is violated." );
  // this is in effect unnecessary because the overloaded operator() already throws an exception
  else if ( pH_ == 0 )
    H_x = 0;
  else if ( pH_ == 1 )
    H_x = 1;
  else
  {
    H_x = 1;
    for ( int i = 2; i <= pH_; i++ )
      H_x += static_cast<Real>(i) * pow(x,i-1);
  }

  H_x *= cH_;

  // du/dx
  Real u_x = 0; // du/dx
  if ( pu_ < 0 )
    SANS_DEVELOPER_EXCEPTION( "pu_ >= 0 is violated." );
  // this is in effect unnecessary because the overloaded operator() already throws an exception
  else if ( pu_ == 0 )
    u_x = 0;
  else if ( pu_ == 1 )
    u_x = 1;
  else
  {
    u_x = 1;
    for ( int i = 2; i <= pu_; i++ )
      u_x += static_cast<Real>(i) * pow(x,i-1);
  }

  // db/dx
  getBathymetryGrad(x,time,nablasb);

  // forcing
  forcing[0] = u*H_x + H*u_x;
  forcing[1] = (u*u + g_*H)*H_x + 2*H*u*u_x - g_*H*nablasb[0];
}


} // namespace SANS


#endif /* SRC_PDE_SHALLOWWATER_SHALLOWWATERSOLUTION1D_H_ */
