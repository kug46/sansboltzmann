// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_SHALLOWWATER_BCSHALLOWWATER2D_MANIFOLD_H_
#define SRC_PDE_SHALLOWWATER_BCSHALLOWWATER2D_MANIFOLD_H_

//PyDict must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <string>

#include "pde/BCCategory.h"
#include "pde/BCNone.h"

#include <boost/mpl/vector.hpp>

#include "PDEShallowWater2D_manifold.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// 2-D shallow water manifold BC class
//
// template parameters:
//   BCType               BC type (e.g. BCTypeInflowSupercritical)
//   PDEShallowWater2D    PDE class
//     --> QType          solution variable type (e.g. primitive variable set)
//----------------------------------------------------------------------------//

template <class BCType, class PDEShallowWater2D_manifold>
class BCShallowWater2D_manifold;

// BCParam: BC parameters
template <class BCType>
struct BCShallowWater2DParams_manifold;

// BCType: BC types
class BCTypeInflowSupercritical;

//----------------------------------------------------------------------------//
// supercritical inflow BC:
//     analogous to supersonic inflow BC for Euler
//
// specify full state: water height, velocity
//----------------------------------------------------------------------------//

template<>
struct BCShallowWater2DParams_manifold<BCTypeInflowSupercritical> : noncopyable
{
  const ParameterNumeric<Real> H{"H", NO_DEFAULT, 0, NO_LIMIT, "WaterHeight"};
  const ParameterNumeric<Real> vs{"vs", NO_DEFAULT, NO_RANGE, "Velocity manifold component"};

  static constexpr const char* BCName{"InflowSupercritical"};
  struct Option
  {
    const DictOption InflowSupercritical{BCShallowWater2DParams_manifold::BCName, BCShallowWater2DParams_manifold::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCShallowWater2DParams_manifold params;
};


template <class QType, class SolutionType>
class BCShallowWater2D_manifold< BCTypeInflowSupercritical, PDEShallowWater_manifold<PhysD2,QType,SolutionType> > :
    public BCType< BCShallowWater2D_manifold<BCTypeInflowSupercritical, PDEShallowWater_manifold<PhysD2,QType,SolutionType>> >
{
public:
  typedef PhysD2 PhysDim;
  typedef typename BCCategory::Dirichlet_sansLG Category;
  typedef BCShallowWater2DParams_manifold<BCTypeInflowSupercritical> ParamsType;
  typedef PDEShallowWater_manifold<PhysDim,QType,SolutionType> PDEClass;
  typedef typename PDEClass::VarInterpret QInterpret;      // solution variable interpreter type

  static const int D = PDEClass::D;   // physical dimensions
  static const int N = PDEClass::N;   // total solution variables

  static const int NBC = 2;                       // total BCs

  template <class T>
  using ArrayQ = typename PDEClass::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDEClass::template MatrixQ<T>;  // matrices

  using VectorX = typename PDEClass::VectorX;

  // cppcheck-suppress noExplicitConstructor
  BCShallowWater2D_manifold( const PDEShallowWater_manifold<PhysDim,QType,SolutionType>& pde,
                             const ArrayQ<Real>& bcdata ) :
      pde_(pde),
      qInterpret_(pde_.getVarInterpreter()),
      bcdata_(bcdata),
      H_(bcdata(0)),
      vs_(bcdata(1)) {}

  BCShallowWater2D_manifold(const PDEShallowWater_manifold<PhysDim,QType,SolutionType>& pde,
                            const PyDict& d ) :
      pde_(pde),
      qInterpret_(pde_.getVarInterpreter()),
      bcdata_( {d.get(ParamsType::params.H), d.get(ParamsType::params.vs)} ),
      H_(bcdata_(0)),
      vs_(bcdata_(1)) {}

  ~BCShallowWater2D_manifold() {}
  BCShallowWater2D_manifold& operator=( const BCShallowWater2D_manifold& ) = delete;

  // BC residual: a(u) - b
  template <class T>
  void strongBC( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<T>& rsdBC ) const;

  template <class T>
  void strongBC( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& lg, ArrayQ<T>& rsdBC ) const;

  // conventional formulation BC weighting function
  // B^t = Fn M A^t ( A M A^t )^{-1}
#if PDEShallowWaterManifold2D_manifold
  // variation for topo1D manifold
  template <class T>
  void weightBC( const VectorX& e1,
                 const Real& x, const Real& y, const Real& time,
                 const Real& nx, const Real& ny,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
                 MatrixQ<T>& wghtBC ) const;
#else
  template <class T>
  void weightBC( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<T>& wghtBC ) const;
#endif

  // Lagrange multiplier weighting function
  // \bar{A}^t = Fn N
  template <class T>
  void weightLagrange( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                       const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<T>& wghtLG ) const;

  // Lagrange multiplier rhs
  // B = ( N^t M^{-1} N )^{-1} N^t M^{-1}
  template <class T>
  void rhsLagrange( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<T>& rhsLG ) const;

  // BC data
  template <class T>
  void data( const Real& x, const Real& y, const Real& time,
             const Real& nx, const Real& ny, ArrayQ<T>& bcdata ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDEClass& pde_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const ArrayQ<Real> bcdata_;
  const Real H_;
  const Real vs_;
};


template <class QType, class SolutionType>
template <class T>
inline void
BCShallowWater2D_manifold<BCTypeInflowSupercritical, PDEShallowWater_manifold<PhysD2,QType,SolutionType>>::strongBC(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<T>& rsdBC ) const
{
  T H = 0, vs = 0;

  qInterpret_.getH( q, H );
  qInterpret_.getVelocity( q, vs );

  rsdBC(0) = H - H_;
  rsdBC(1) = vs - vs_;
}

#if PDEShallowWaterManifold2D_manifold
template <class QType, class SolutionType>
template <class T>
inline void
BCShallowWater2D_manifold<BCTypeInflowSupercritical, PDEShallowWater_manifold<PhysD2,QType,SolutionType>>::weightBC(
    const VectorX& e1,
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<T>& weightBC ) const
{
  MatrixQ<T> F = 0, G = 0;

  pde_.jacobianFluxAdvective( e1, x, y, time, q, F, G );

  weightBC = nx*F + ny*G;
}
#else
template <class QType, class SolutionType>
template <class T>
inline void
BCShallowWater2D_manifold<BCTypeInflowSupercritical, PDEShallowWater_manifold<PhysD2,QType,SolutionType>>::weightBC(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<T>& weightBC ) const
{
  MatrixQ<T> F = 0, G = 0;

  pde_.jacobianFluxAdvective( x, y, time, q, F, G );

  weightBC = nx*F + ny*G;
}
#endif


template <class QType, class SolutionType>
template <class T>
inline void
BCShallowWater2D_manifold<BCTypeInflowSupercritical, PDEShallowWater_manifold<PhysD2,QType,SolutionType>>::data(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    ArrayQ<T>& bcdata ) const
{
  bcdata = bcdata_;
}

//template <class QType>
//void
//BCShallowWater2D_manifold<BCTypeInflowSupercritical, PDEShallowWater_manifold<PhysD2,QType>>::dump( int indentSize, std::ostream& out ) const
//{
//  std::string indent(indentSize, ' ');
//
//  out << indent << "BCShallowWater2D_manifold<BCTypeInflowSupercritical, PDEShallowWater_manifold<PhysD2,QType>>: qInterpret_ = " << std::endl;
//  qInterpret_.dump(indentSize+2, out);
//
//  out << indent << "BCShallowWater2D_manifold<BCTypeInflowSupercritical, PDEShallowWater_manifold<PhysD2,QType>>: bcdata_ = " << std::endl;
//  bcdata_.dump(indentSize+2, out);
//}


//----------------------------------------------------------------------------//

template<class QType, class SolutionType>
using BCShallowWater2DVector_manifold
    = boost::mpl::vector2< BCShallowWater2D_manifold< BCTypeInflowSupercritical, PDEShallowWater_manifold<PhysD2,QType,SolutionType> >,
                           BCNone<PhysD2,PDEShallowWater_manifold<PhysD2,QType,SolutionType>::N> >;

} // namespace SANS

#endif /* SRC_PDE_SHALLOWWATER_BCSHALLOWWATER2D_MANIFOLD_H_ */
