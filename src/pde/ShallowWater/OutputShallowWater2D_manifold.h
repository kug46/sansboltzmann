// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef OUTPUTSHALLOWWATER2D_MANIFOLD_H_
#define OUTPUTSHALLOWWATER2D_MANIFOLD_H_

#include "pde/ShallowWater/PDEShallowWater2D_manifold.h"
#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Subfunctions to compute single output from shallow water solution variables

template <class QType, class PDENDConvert>
class OutputShallowWater2D_manifold_H
{
public:
  typedef PhysD2 PhysDim;

  typedef VarInterpret2D_manifold<QType> QInterpret;            // solution variable interpreter type

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputShallowWater2D_manifold_H( const PDENDConvert& pde ) :
      pde_(pde), qInterpret_(pde_.getVarInterpreter()) {}

  bool needsSolutionGradient() const { return false; }

  template<class T>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayJ<T>& output ) const
  {
    T H;
    qInterpret_.getH( q, H );

    output = H;
  }

private:
  const PDENDConvert& pde_;
  const QInterpret& qInterpret_;
};

} // namespace SANS

#endif /* OUTPUTSHALLOWWATER2D_MANIFOLD_H_ */
