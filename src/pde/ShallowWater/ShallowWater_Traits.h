// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_SHALLOWWATER_SHALLOWWATER_TRAITS_H_
#define SRC_PDE_SHALLOWWATER_SHALLOWWATER_TRAITS_H_

#include "Topology/Dimension.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Shallow water PDE class forward declaration
//
// template parameters:
//   PhysDim                      Physical dimension
//   VarType                      Primary variable type
//   SolutionType                 Exact solution type

template<class PhysDim, class VarType, class SolutionType>
class PDEShallowWater;

//----------------------------------------------------------------------------//
// Shallow water input variable type forward declaration
//   1) is used for taking in convenient variable data
//   2) will be passed into variable interpreter to set the state vector (i.e. primary variables)
//
// template parameters:
//   Derived                      Type specialization

template<class Derived>
struct ShallowWater1DVariableType;

template<class Derived>
struct ShallowWater2DVariableType;

//----------------------------------------------------------------------------//
// Shallow water variable interpreter class forward declaration
//   1) is used to parse input variables and set/get the state vector (i.e. primary variables)
//
// template parameters:
//   VarType                solution variable type (i.e. primitive variable set).

template <class VarType>
class VarInterpret1D;

template <class VarType>
class VarInterpret2D;

//----------------------------------------------------------------------------//
// Shallow water traits struct for non-manifolds
template<class PhysDim>
struct ShallowWaterTraits
{
  // Note: struct members are public by default

  static const int D = PhysDim::D;             // physical dimension
  static const int N = D + 1;                  // total number of primitive variables

  template<class T>
  using ArrayQ = DLA::VectorS<N,T>;            // solution/residual array type

  template<class T>
  using MatrixQ = DLA::MatrixS<N,N,T>;         // elemental mass matrix type

  using VectorX = DLA::VectorS<D,Real>;        // Cartesian vector type
};

} // namespace SANS


#endif /* SRC_PDE_SHALLOWWATER_SHALLOWWATER_TRAITS_H_ */
