// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_SHALLOWWATER_VARINTERP2D_MANIFOLD_H_
#define SRC_PDE_SHALLOWWATER_VARINTERP2D_MANIFOLD_H_

// 2-D PDEShallowWater manifold solution interpreter class

#include <iostream>

#include "ShallowWater_Traits_manifold.h"
#include "ShallowWaterVariable2D_manifold.h"

namespace SANS
{

//------------------------- VarInterpret2D_manifold<VarTypeHVelocity2D_manifold> -------------------------//
//
// template parameters:
//   T                      solution DOF data type (e.g. Surreal)
//
// primary variable vector/state vector (var):
//  [0] H:         water height
//  [1] v_s:       flow velocity component along manifold
//
// member functions:
//   .getH
//   .getVelocity
//   .getHgrad
//   .isValidState
//   .setFromPrimitive
//
//----------------------------------------------------------------------------//
template <>
class VarInterpret2D_manifold<VarTypeHVelocity2D_manifold>
{
public:
  //-----------------------Types-----------------------//
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = ShallowWaterTraits_manifold<PhysDim>::template ArrayQ<T>;    // solution/residual arrays
  // [Our compilers seem fine with missing or adding the keyword "template" before ArrayQ<T>, but it's good practice to keep it.]

  //-----------------------Constructors-----------------------//
  VarInterpret2D_manifold() = default;

  //-----------------------Destructors-----------------------//
  ~VarInterpret2D_manifold() {}
  explicit VarInterpret2D_manifold( const VarInterpret2D_manifold& ) = delete;
  VarInterpret2D_manifold& operator=( const VarInterpret2D_manifold& ) = delete;

  //-----------------------Member functions-----------------------//
  // get water height H
  template <class T>
  void getH( const ArrayQ<T>& var, T& H ) const;

  // get flow velocity v = {v_x, v_y}
  template <class T>
  void getVelocity( const ArrayQ<T>& var, T& v ) const;

  // check if state is physically valid
  bool isValidState( const ArrayQ<Real>& var ) const;

  // set state vector from primitive variable vector
  template<class T>
  void setFromPrimitive( const VarTypeHVelocity2D_manifold& data, ArrayQ<T>& var ) const;

  // print out parameters
  void dump( int indentSize, std::ostream& out = std::cout ) const;

}; // class VarInterpret2D<VarTypeHVelocity2D_manifold>


//--------------------- Inline member function definitions for VarInterpret2D<VarTypeHVelocity2D_manifold> ---------------------//
template <class T>
inline void
VarInterpret2D_manifold<VarTypeHVelocity2D_manifold>::getH( const ArrayQ<T>& var, T& H ) const
{
  H = var[0];
}

template <class T>
inline void
VarInterpret2D_manifold<VarTypeHVelocity2D_manifold>::getVelocity( const ArrayQ<T>& var, T& v ) const
{
  v = var[1];
}

inline bool
VarInterpret2D_manifold<VarTypeHVelocity2D_manifold>::isValidState( const ArrayQ<Real>& var ) const
{
  bool StateIsValid;
  Real H;

  H = var[0];

#if 0
  printf( "%s: H = %e \n", __PRETTY_FUNCTION__, H );
#endif

  // water height H should be positive
  if ( H >= 0 )
    StateIsValid = true;
  else
    StateIsValid = false;

  return StateIsValid;
}

template <class T>
inline void
VarInterpret2D_manifold<VarTypeHVelocity2D_manifold>::setFromPrimitive( const VarTypeHVelocity2D_manifold& data, ArrayQ<T>& var ) const
{
  var[0] = data.H_;
  var[1] = data.vs_;
}

} // namespace SANS

#endif /* SRC_PDE_SHALLOWWATER_VARINTERP2D_MANIFOLD_H_ */
