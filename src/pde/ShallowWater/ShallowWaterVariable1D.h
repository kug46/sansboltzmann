// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_SHALLOWWATER_SHALLOWWATERVARIABLE1D_H_
#define SRC_PDE_SHALLOWWATER_SHALLOWWATERVARIABLE1D_H_

#include "tools/SANSnumerics.h" // Real

#include "ShallowWater_Traits.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Root struct for 1D shallow water input variable types
//
// template parameters:
//   Derived                solution variable type (i.e. primitive variable set)
//
template<class Derived>
struct ShallowWater1DVariableType
{
  // A convenient method for casting to the derived type
  inline const Derived& cast() const { return static_cast<const Derived&>(*this); }
};

//----------------------- Derived = VarTypeHVelocity1D -----------------------//
// input variables:
//   H_                 H: water height
//   u_                 u: x-component of flow velocity
//
struct VarTypeHVelocity1D : public ShallowWater1DVariableType<VarTypeHVelocity1D>
{
  typedef PhysD1 PhysDim;
  static const int N = ShallowWaterTraits<PhysDim>::N;

  VarTypeHVelocity1D(const Real& H, const Real& u)
      : H_( H ), u_( u ) {}

  VarTypeHVelocity1D& operator=( const std::initializer_list<Real>& s )
  {
    SANS_ASSERT(s.size() == N);
    auto q = s.begin();
    H_ = *q; q++;
    u_ = *q;

    return *this;
  }

  Real H_;
  Real u_;
}; // struct VarTypeHVelocity1D

} // namespace SANS


#endif /* SRC_PDE_SHALLOWWATER_SHALLOWWATERVARIABLE1D_H_ */
