// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define PYDICT_INSTANTIATE
#include "PyDict_impl.h"

#include <ios>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>

namespace SANS
{

//=============================================================================
// Explicit instantiation of common parameter types
#define PYDICT_INSTANTIATE_METHODS( Param ) \
  template typename Param::ExtractType PyDict::get(const ParameterType<Param>& paramtype) const; \
  template const ParameterBase* PyDict::checkInputs(const ParameterType<Param>& paramtype);

PYDICT_INSTANTIATE_METHODS( ParameterNumeric<int> )
PYDICT_INSTANTIATE_METHODS( ParameterNumeric<Real> )
PYDICT_INSTANTIATE_METHODS( ParameterNumericList<int> )
PYDICT_INSTANTIATE_METHODS( ParameterNumericList<Real> )
PYDICT_INSTANTIATE_METHODS( ParameterBool )
PYDICT_INSTANTIATE_METHODS( ParameterDict )
PYDICT_INSTANTIATE_METHODS( ParameterString )
PYDICT_INSTANTIATE_METHODS( ParameterFileName )

//===========================================================================//
template< class T >
T
ExtractParam< T, ParameterNumeric< T > >::
extract(const ParameterNumeric< T >& param, const boost::python::dict& d, const std::string& parent)
{
  //Take over the GIL so threads can call python
  ThreadCallPython PyGIL;

  std::string k = param;

  boost::python::extract<T> val(d[k]);
  if ( val.check() )
  {
    //Check that val is within the range
    if ( param.hasMin && (T)val < param.min )
      BOOST_THROW_EXCEPTION( NumericOutOfRangeException(k, parent, stringify((T)val),
                                                        stringify(param.min), param.hasMin,
                                                        stringify(param.max), param.hasMax,
                                                        param.help ) );

    if ( param.hasMax && (T)val > param.max )
      BOOST_THROW_EXCEPTION( NumericOutOfRangeException(k, parent, stringify((T)val),
                                                        stringify(param.min), param.hasMin,
                                                        stringify(param.max), param.hasMax,
                                                        param.help ) );

    return val;
  }

  boost::python::extract<std::string> strval( (boost::python::str(d[k])) );
  BOOST_THROW_EXCEPTION( DictValueTypeException(k, parent, strval, Type2String<T>::str(), param.help ) );

  return 0;
}

template< class T >
void ExtractParam< T, ParameterNumeric< T > >::checkInputs(const ParameterNumeric<T>& param, const PyDict& d) { d.get(param); }

template struct ExtractParam< int, ParameterNumeric< int > >;
template struct ExtractParam< Real, ParameterNumeric< Real > >;


//===========================================================================//
template< class T >
std::vector<T>
ExtractParam< std::vector<T>, ParameterNumericList< T > >::
extract(const ParameterNumericList< T >& param, const boost::python::dict& d, const std::string& parent)
{
  //Take over the GIL so threads can call python
  ThreadCallPython PyGIL;

  std::vector<T> values;

  std::string k = param;

  boost::python::extract<boost::python::list> listval(d[k]);
  if ( listval.check() )
  {
    boost::python::list numlist = listval;

    // Resize the output vector
    values.resize( boost::python::len(numlist) );

    for (int i = 0; i < boost::python::len(numlist); i++)
    {
      boost::python::extract<T> val(numlist[i]);
      if ( val.check() )
      {
        T num = (T)val;

        //Check that val is within the range
        if ( param.hasMin && num < param.min )
          BOOST_THROW_EXCEPTION( NumericOutOfRangeException(i, k, parent, stringify(num),
                                                            stringify(param.min), param.hasMin,
                                                            stringify(param.max), param.hasMax,
                                                            param.help ) );

        if ( param.hasMax && num > param.max )
          BOOST_THROW_EXCEPTION( NumericOutOfRangeException(i, k, parent, stringify(num),
                                                            stringify(param.min), param.hasMin,
                                                            stringify(param.max), param.hasMax,
                                                            param.help ) );

        values[i] = num;
      }
    }
    // Return the vector of values
    return values;
  }

  boost::python::extract<std::string> strval( (boost::python::str(d[k])) );
  BOOST_THROW_EXCEPTION( DictValueTypeException(k, parent, strval, Type2String<boost::python::list>::str(), param.help ) );

  return values;
}

template< class T >
void
ExtractParam< std::vector<T>, ParameterNumericList< T > >::checkInputs(const ParameterNumericList<T>& param, const PyDict& d) { d.get(param); }

template struct ExtractParam< std::vector<int>, ParameterNumericList< int > >;
template struct ExtractParam< std::vector<Real>, ParameterNumericList< Real > >;


//===========================================================================//
ParameterDict::ExtractType
ExtractParam<ParameterDict::ExtractType, ParameterDict>::
extract(const ParameterDict& param, const boost::python::dict& d, const std::string& parent)
{
  //Take over the GIL so threads can call python
  ThreadCallPython PyGIL;

  std::string k = param;

  boost::python::extract<boost::python::dict> val(d[k]);
  if ( val.check() )
  {
    boost::python::api::object obj = d[k];
    boost::python::dict& d_dict = static_cast<boost::python::dict&>(obj);
    return PyDict(d_dict, parent, k);
  }

  boost::python::extract<std::string> strval( (boost::python::str(d[k])) );
  BOOST_THROW_EXCEPTION( DictValueTypeException(k, parent, strval, Type2String<T>::str(), param.help ) );

  return PyDict();
}

void
ExtractParam<ParameterDict::ExtractType, ParameterDict>::
checkInputs(const ParameterDict& param, const PyDict& d)
{
  PyDict d_sub = d.get(param);
  param.checkInputs(d_sub);
}

//===========================================================================//
ParameterFileName::ExtractType
ExtractParam<ParameterFileName::ExtractType, ParameterFileName>::
extract(const ParameterFileName& param, const boost::python::dict& d, const std::string& parent)
{
  //Take over the GIL so threads can call python
  ThreadCallPython PyGIL;

  std::string k = param;

  boost::python::extract<T> val(d[k]);
  if ( val.check() )
    return val;

  boost::python::extract<std::string> strval( (boost::python::str(d[k])) );
  BOOST_THROW_EXCEPTION( DictValueTypeException(k, parent, strval, Type2String<T>::str(), param.help ) );

  return "";
}

void
ExtractParam<ParameterFileName::ExtractType, ParameterFileName>::
checkInputs(const ParameterFileName& param, const PyDict& d)
{
  // This checks that the filename is a valid string
  std::string filename = d.get(param);

  // Nothing to check if the string is empty
  if (filename.empty()) return;

  bool good = true;

  // if it's an input file, make sure it exists
  if (param.mode == std::ios_base::in)
  {
    good = boost::filesystem::exists( filename );
  }
  else if (param.mode == std::ios_base::out ||
           param.mode == std::ios_base::app )
  {
    // check that the path has a file name (i.e. it's not a directory) and the parent directory exists
    boost::filesystem::path filepath(filename);
    boost::filesystem::path parent_path = filepath.parent_path();

    good = !boost::filesystem::is_directory( filepath ) && boost::filesystem::exists( parent_path );

    //TODO: This is not perfect because it does not check if there are permissions to create the file
  }
  else
    SANS_DEVELOPER_EXCEPTION("Unknown file mode = %d", param.mode);

  // throw an error if the file is not good
  if (!good)
  {
    std::string k = param;
    BOOST_THROW_EXCEPTION( FileNameException(k, d.getParent(), filename, param.mode, param.help ) );
  }
}

} // namespace SANS
