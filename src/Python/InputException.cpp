// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/python/str.hpp>
#include <boost/python/extract.hpp>
#include <boost/python/dict.hpp>

#include "tools/stringify.h"

#include "PyDict.h"
#include "Parameter.h"
#include "InputException.h"

#include <sstream> // std::stringstream
#include <set>

namespace SANS
{
struct PyDict;

//=============================================================================
std::string VariablePath(const std::string& parent, const std::string& key ) { return parent.size() ? parent + "." + key : key; }
std::string VariablePath(const std::string& parent, const int& key ) { SANS_ASSERT(parent.size() > 0); return parent + "[" + stringify(key) + "]"; }


//=============================================================================
NumericOutOfRangeException::NumericOutOfRangeException( const std::string& param, const std::string& parent, const std::string& val,
                                                        const std::string& min, const bool hasMin,
                                                        const std::string& max, const bool hasMax,
                                                        const std::string& help )
{
  std::string name = VariablePath(parent, param);
  errString += "The parameter value\n\n" + name + " = " + val + "\n\n";
  errString += "is outside the valid range: ( ";
  errString += hasMin ? min : "-inf" ;
  errString += " <= ";
  errString += param;
  errString += " <= ";
  errString += hasMax ? max : "inf" ;
  errString += " )\n\n";
  errString += "Parameter help: " + help;
}

//=============================================================================
NumericOutOfRangeException::NumericOutOfRangeException( const int i, const std::string& param, const std::string& parent, const std::string& val,
                                                        const std::string& min, const bool hasMin,
                                                        const std::string& max, const bool hasMax,
                                                        const std::string& help )
{
  std::string name = VariablePath(VariablePath(parent, param), i);
  errString += "The parameter value\n\n" + name + " = " + val + "\n\n";
  errString += "is outside the valid range: ( ";
  errString += hasMin ? min : "-inf" ;
  errString += " <= ";
  errString += param;
  errString += " <= ";
  errString += hasMax ? max : "inf" ;
  errString += " )\n\n";
  errString += "Parameter help: " + help;
}

//=============================================================================
NoDefaultException::NoDefaultException(const std::string& param, const std::string& parent, const std::string& help)
{
  errString += "No default value available for the parameter\n\n";
  errString += VariablePath(parent, param);
  errString += "\n\n";
  errString += "Parameter help: " + help;
}

//=============================================================================
DictValueTypeException::DictValueTypeException(const std::string& param, const std::string& parent,
                                               const std::string& val, const std::string& type, const std::string& help)
{
  errString += "The parameter\n\n";
  errString += VariablePath(parent, param) + " = " + val;
  errString += "\n\nis not the expected type '";
  errString += type;
  errString += "'.\n\n";
  errString += "Parameter help: " + help;
}

//=============================================================================
KeyTypeException::KeyTypeException(const std::string& key, const std::string& parent, const std::string& type)
{
  errString += "The key '" + key + "' in\n\n";
  errString += parent;
  errString += "\n\nis not the expected type '";
  errString += type;
  errString += "'.";
}

//=============================================================================
FileNameException::FileNameException(const std::string& param, const std::string& parent,
                                     const std::string& filename, const std::ios_base::openmode mode, const std::string& help)
{
  std::string name = VariablePath(parent, param);
  errString += "The filename\n\n" + name + " = '" + filename + "'\n\n";
  if (mode & std::ios_base::in)
    errString += "cannot be opened as an input file.";
  else if (mode & std::ios_base::out)
    errString += "cannot be opened as an output file.";
  else
    errString += "cannot be opened with mode: " + std::to_string(mode) + ".";
  errString += "\n\nParameter help: " + help;
}

//=============================================================================
template<class T>
OptionException<T>::OptionException(const std::string& param, const std::string& parent, const T& val,
                                    const std::vector<T>& options, const std::string& help)
{
  errString += "The parameter\n\n";
  errString += VariablePath(parent, param) + " = " + stringify(val);
  errString += "\n\ndoes not match any of the available options:\n\n";
  for ( std::size_t i = 0; i < options.size(); i++ )
    errString += stringify(options[i]) + "\n";
  errString += "\n";
  errString += "Parameter help: " + help;
}

template struct OptionException<DictOption>;
template struct OptionException<std::string>;
template struct OptionException<int>;

//=============================================================================
//exception when aditoinal entries in a dict exist that are not used
UnknownInputsException::UnknownInputsException(PyDict& d, const std::vector<const ParameterBase*>& allParams)
{

  std::string parent = VariablePath(d.getParent(), "");
  std::string Unkowns;

  while ( d.length() )
  {
    boost::python::tuple keyval = d.popitem();
    Unkowns += parent + (std::string)boost::python::extract<std::string>( boost::python::str( keyval[0] ) ) + " = "
                      + (std::string)boost::python::extract<std::string>( boost::python::str( keyval[1] ) ) + "\n";
  }

  errString += "The parameters\n\n";
  errString += Unkowns;
  errString += "\nare unexpected.\n\n";

  if (allParams.size() > 0)
  {
    std::string Name("Name");
    std::string Type("Type");
    std::string Default("Default");
    std::string Help("Help");

    // Compute the maximum parameter name length
    std::size_t maxnamewidth = Name.size();
    std::size_t maxtypewidth = Type.size();
    std::size_t maxdefaultwidth = Default.size();
    std::size_t maxhelptwidth = Help.size();

    for ( std::size_t i = 0; i < allParams.size(); i++ )
    {
      maxnamewidth = std::max(maxnamewidth, allParams[i]->name.size());
      maxtypewidth = std::max(maxtypewidth, allParams[i]->type.size());
      maxdefaultwidth = std::max(maxdefaultwidth, allParams[i]->default_str().size());
      maxhelptwidth = std::max(maxhelptwidth, allParams[i]->help.size());
    }
    maxnamewidth++;
    maxtypewidth++;
    maxdefaultwidth++;
    maxhelptwidth++;

    std::stringstream paramNames;

    // Start with a table header
    paramNames << std::left << std::setw(maxnamewidth) << std::setfill(' ') << Name
               << std::setw(0) << "  "
               << std::left << std::setw(maxtypewidth) << std::setfill(' ') << Type
               << std::setw(0) << "  "
               << std::left << std::setw(maxdefaultwidth) << std::setfill(' ') << Default
               << std::setw(0) << "  "
               << std::left << std::setw(maxhelptwidth) << std::setfill(' ') << Help << std::endl;

    paramNames << std::setw(maxnamewidth+2+maxtypewidth+2+maxdefaultwidth+2+maxhelptwidth) << std::setfill('-') << "-" << std::endl;

    // Get the parameter names and help string with a pretty formatting
    for ( std::size_t i = 0; i < allParams.size(); i++ )
      paramNames << std::left << std::setw(maxnamewidth) << std::setfill(' ') << allParams[i]->name
                 << std::setw(0) << ": "
                 << std::left << std::setw(maxtypewidth) << std::setfill(' ') << allParams[i]->type
                 << std::setw(0) << ": "
                 << std::left << std::setw(maxdefaultwidth) << std::setfill(' ') << (allParams[i]->hasDefault ? allParams[i]->default_str() : " ")
                 << std::setw(0) << ": "
                 << allParams[i]->help << std::endl;

    errString += "Expected parameters are:\n\n";
    errString += paramNames.str();
  }
  else
  {
    errString += "Empty dictionary expected.";
  }
}


//=============================================================================
//exception when an incorrect type of a dictionary key is given
BCException::
BCException(const PyDict& BCList,
            const std::map< std::string, std::vector<int> >& BoundaryGroups)
{
  errString += "Boundary Condition 'Type' and 'Group' keys must match.\n\n";

  std::set<std::string> stypekeys, sgroupkeys;

  std::vector<std::string> typekeys = BCList.stringKeys();
  std::sort(typekeys.begin(), typekeys.end());
  stypekeys.insert(typekeys.begin(), typekeys.end());

  std::set<std::string> groupkeys;
  for (const std::pair<std::string, std::vector<int>>& group : BoundaryGroups)
    groupkeys.insert(group.first);
  sgroupkeys.insert(groupkeys.begin(), groupkeys.end());

  errString += "Boundary Condition Type Keys:\n";
  for (const std::string& key : typekeys)
  {
    errString += "'" + key + "'\n";
    sgroupkeys.erase(key);
  }
  errString += "\n";


  errString += "Boundary Condition Group Keys:\n";
  for (const std::string& key : groupkeys)
  {
    errString += "'" + key + "'\n";
    stypekeys.erase(key);
  }
  errString += "\n";

  if (!stypekeys.empty())
  {
    errString += "Unmatched Type keys:\n";
    for (const std::string& key : stypekeys)
      errString += "'" + key + "'\n";
  }

  if (!sgroupkeys.empty())
  {
    if (!stypekeys.empty())
      errString += "\n";

    errString += "Unmatched Group keys:\n";
    for (const std::string& key : sgroupkeys)
      errString += "'" + key + "'\n";
  }
  // Remove the last return character
  errString.erase(errString.end()-1);
}


//----------------------------------------------------------------------------//
void checkBCInputs( PyDict& BCList,
                    const std::map< std::string, std::vector<int> >& BoundaryGroups)
{
  std::map< std::string, std::vector<int> > copy(BoundaryGroups);
  std::vector<std::string> keys = BCList.stringKeys();

  // Make sure all keys are in the Boundary groups list
  for (const std::string& key : keys)
    if (copy.erase(key) != 1)
      BOOST_THROW_EXCEPTION( BCException(BCList, BoundaryGroups) );

  // Make sure there are no boundary groups left over without boundary condition types
  if (!copy.empty())
    BOOST_THROW_EXCEPTION( BCException(BCList, BoundaryGroups) );
}


} // namepsace SANS

//=============================================================================
// Reduce compile time by explicitly instantiating these
template class boost::exception_detail::clone_impl<SANS::NumericOutOfRangeException>;
template class boost::exception_detail::clone_impl<SANS::NoDefaultException>;
template class boost::exception_detail::clone_impl<SANS::DictValueTypeException>;
template class boost::exception_detail::clone_impl<SANS::KeyTypeException>;
template class boost::exception_detail::clone_impl<SANS::FileNameException>;
template class boost::exception_detail::clone_impl<SANS::OptionException<SANS::DictOption>>;
template class boost::exception_detail::clone_impl<SANS::OptionException<std::string>>;
template class boost::exception_detail::clone_impl<SANS::OptionException<int>>;
template class boost::exception_detail::clone_impl<SANS::UnknownInputsException>;

template void boost::throw_exception<SANS::NumericOutOfRangeException>(SANS::NumericOutOfRangeException const&);
template void boost::throw_exception<SANS::NoDefaultException>(SANS::NoDefaultException const&);
template void boost::throw_exception<SANS::DictValueTypeException>(SANS::DictValueTypeException const&);
template void boost::throw_exception<SANS::KeyTypeException>(SANS::KeyTypeException const&);
template void boost::throw_exception<SANS::FileNameException>(SANS::FileNameException const&);
template void boost::throw_exception<SANS::OptionException<SANS::DictOption>>(SANS::OptionException<SANS::DictOption> const&);
template void boost::throw_exception<SANS::OptionException<std::string>>(SANS::OptionException<std::string> const&);
template void boost::throw_exception<SANS::OptionException<int>>(SANS::OptionException<int> const&);
template void boost::throw_exception<SANS::UnknownInputsException>(SANS::UnknownInputsException const&);

template void boost::exception_detail::throw_exception_<SANS::NumericOutOfRangeException>(SANS::NumericOutOfRangeException const&, char const*, char const*, int);
template void boost::exception_detail::throw_exception_<SANS::NoDefaultException>(SANS::NoDefaultException const&, char const*, char const*, int);
template void boost::exception_detail::throw_exception_<SANS::DictValueTypeException>(SANS::DictValueTypeException const&, char const*, char const*, int);
template void boost::exception_detail::throw_exception_<SANS::KeyTypeException>(SANS::KeyTypeException const&, char const*, char const*, int);
template void boost::exception_detail::throw_exception_<SANS::FileNameException>(SANS::FileNameException const&, char const*, char const*, int);
template void boost::exception_detail::throw_exception_<SANS::OptionException<SANS::DictOption>>(SANS::OptionException<SANS::DictOption> const&, char const*, char const*, int);
template void boost::exception_detail::throw_exception_<SANS::OptionException<std::string>>(SANS::OptionException<std::string> const&, char const*, char const*, int);
template void boost::exception_detail::throw_exception_<SANS::OptionException<int>>(SANS::OptionException<int> const&, char const*, char const*, int);
template void boost::exception_detail::throw_exception_<SANS::UnknownInputsException>(SANS::UnknownInputsException const&, char const*, char const*, int);
