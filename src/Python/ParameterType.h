// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PARAMETERTYPE_H
#define PARAMETERTYPE_H

#include <vector>

namespace SANS
{

//A class used to identify parameter types
template< class Derived >
struct ParameterType
{
  //A convenient method for casting to the derived type
  inline const Derived& cast() const { return static_cast<const Derived&>(*this); }
};


//Forward declare
struct DictOption;

// Forward declare
struct ParameterBase;

}

// This version of std::vector is used for all checkInputs functions
extern template class std::vector<const SANS::ParameterBase*>;

#endif //PARAMETERTYPE_H
