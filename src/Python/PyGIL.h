// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PYGIL_H
#define PYGIL_H

#include <boost/python/detail/prefix.hpp>
#include "tools/SANSException.h"
#include "PyInit.h"

namespace SANS
{
  //Need this so that python allows multiple threads to execute concurrently
  struct GILRelease
  {
    GILRelease() : gil_state(PyEval_SaveThread()) {}
    ~GILRelease() { PyEval_RestoreThread(gil_state);  }

  private:
    PyThreadState *gil_state;
  };

  //This will allow for python calls from a thread within the scope of it's creation
  struct ThreadCallPython :public  InitPythonBase
  {
    ThreadCallPython() : gil_state( PyGILState_Ensure() ) {}

    ~ThreadCallPython()
    {
      PyGILState_Release(gil_state);
    }

  private:
    PyGILState_STATE gil_state;
  };

  //This will allow for pyhon calls from a thread within the scope of it's creation
  struct ThreadCallPythonBase
  {
    ThreadCallPythonBase() : gil_state( PyGILState_Ensure() ) {}

    // This must be called from the construtor to give back the GIL
    void ReleaseGIL()
    {
      PyGILState_Release(gil_state);
    }

    ~ThreadCallPythonBase()
    {
      // Make sure the GIL was returned
      SANS_ASSERT_MSG(gil_state == 0, "Failed to call ReleaseGIL() in the constructor");
    }

  private:
    PyGILState_STATE gil_state;
  };

}

#endif //PYGIL_H
