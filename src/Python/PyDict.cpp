// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/python/dict.hpp>
#include <boost/python/str.hpp>
#include <boost/python/extract.hpp>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <string>
#include <sstream> // istringstream

#include "PyGIL.h"
#include "PyDict.h"
#include "Parameter.h"
#include "InputException.h"

#include "tools/stringify.h"
#include "tools/Type2String.h"

#include "tools/output_std_vector.h"

namespace SANS
{

#if 0
//=============================================================================
  template<typename T>
  void WarnDefault(const std::string& key, const std::string& parent_, const T& def)
  {
    std::string Wstr;
    Wstr  = "INFO: Unable to find the dictionary key '";
    Wstr += parent_;
    Wstr += "[\"";
    Wstr += key;
    Wstr += "\"]'. A default value of '";
    Wstr += tools::cast_to_string<T>(def);
    Wstr += "' has been applied.";
    std::cout << Wstr << std::endl;
  }

  template void WarnDefault<bool>       (const std::string& key, const std::string& parent_, const bool&        def);
  template void WarnDefault<int>        (const std::string& key, const std::string& parent_, const int&         def);
  template void WarnDefault<float>      (const std::string& key, const std::string& parent_, const float&       def);
  template void WarnDefault<double>     (const std::string& key, const std::string& parent_, const double&      def);
  template void WarnDefault<std::string>(const std::string& key, const std::string& parent_, const std::string& def);
#endif

//=============================================================================
PyDict::PyDict()
  : pdict_(nullptr), parent_("") //ThreadCallPythonBase(),
{
  //Take over the GIL so threads can call python
  ThreadCallPython PyGIL;

  pdict_ = new boost::python::dict;
}

PyDict::PyDict(const empty_PyDict&) : PyDict() {}

PyDict::PyDict(const PyDict& dict) : pdict_(nullptr), parent_(dict.parent_)
{
  //Take over the GIL so threads can call python
  ThreadCallPython PyGIL;

  pdict_ = new boost::python::dict(dict.pdict_->copy());
}

PyDict&
PyDict::operator=(const PyDict& dict)
{
  //Take over the GIL so threads can call python
  ThreadCallPython PyGIL;

  parent_ = dict.parent_;
  *pdict_ = *dict.pdict_;

  return *this;
}

PyDict::PyDict(const boost::python::dict& d_in)
  : pdict_(nullptr), parent_("")
{
  //Take over the GIL so threads can call python
  ThreadCallPython PyGIL;

  pdict_ = new boost::python::dict(d_in);
}

PyDict::PyDict(boost::python::dict& d_in, const std::string& ParentString, const std::string& ParentKey)
  : pdict_(nullptr), parent_(ParentString.size() ? ParentString + "." + ParentKey : ParentKey)
{
  //Take over the GIL so threads can call python
  ThreadCallPython PyGIL;

  pdict_ = new boost::python::dict(d_in);
}

PyDict::PyDict(boost::python::dict& d_in, const std::string& ParentString, const int ParentKey)
  : pdict_(nullptr), parent_(ParentString + "[" + stringify(ParentKey) + "]")
{
  //Take over the GIL so threads can call python
  ThreadCallPython PyGIL;

  pdict_ = new boost::python::dict(d_in);
}

//-----------------------------------------------------------------------------
PyDict::~PyDict()
{
  //Take over the GIL so threads can call python
  ThreadCallPython PyGIL;

  delete pdict_;
}

//-----------------------------------------------------------------------------
bool PyDict::has_key(const int k) const
{
  //Take over the GIL so threads can call python
  ThreadCallPython PyGIL;

  return pdict_->has_key(k);
}

bool PyDict::has_key(const std::string& k) const
{
  //Take over the GIL so threads can call python
  ThreadCallPython PyGIL;

  return pdict_->has_key(k);
}

//-----------------------------------------------------------------------------
ssize_t PyDict::length() const
{
  //Take over the GIL so threads can call python
  ThreadCallPython PyGIL;

  return boost::python::len( pdict_->keys() );
}

//-----------------------------------------------------------------------------
boost::python::tuple PyDict::popitem()
{
  //Take over the GIL so threads can call python
  ThreadCallPython PyGIL;

  return pdict_->popitem();
}

//-----------------------------------------------------------------------------
boost::python::list PyDict::keys() const
{
  //Take over the GIL so threads can call python
  ThreadCallPython PyGIL;

  return pdict_->keys();
}

//-----------------------------------------------------------------------------
std::string PyDict::str() const
{
  //Take over the GIL so threads can call python
  ThreadCallPython PyGIL;

  return (std::string)boost::python::extract<std::string>( boost::python::str( *pdict_ ) );
}

//-----------------------------------------------------------------------------
std::string PyDict::json() const
{
  std::string json = str();
  std::replace( json.begin(), json.end(), '\'', '\"');

  return json;
}

//-----------------------------------------------------------------------------
void ptree_parse(const boost::property_tree::ptree& pt, PyDict& d)
{
  // loop over all entries in the ptree string and convert them to a python dictionary
  for (const boost::property_tree::ptree::value_type& it : pt)
  {
    std::string key = it.first;

    boost::optional<int> value_int = pt.get_optional<int>(it.first);
    if (value_int)
    {
      d[key] = value_int.get();
      continue;
    }

    boost::optional<double> value_double = pt.get_optional<double>(it.first);
    if (value_double)
    {
      d[key] = value_double.get();
      continue;
    }

    boost::optional<bool> value_bool = pt.get_optional<bool>(it.first);
    if (value_bool)
    {
      d[key] = value_bool.get();
      continue;
    }

    try
    {
      // Try to extract an array of ints
      std::vector<int> array_int;
      for (auto& item : pt.get_child(it.first))
      {
        boost::optional<int> val_int = item.second.get_value_optional<int>();
        if (!val_int) break;
        array_int.push_back(val_int.get());
      }

      if ( array_int.size() > 0 && array_int.size() == it.second.size() )
      {
        d[key] = array_int;
        continue;
      }
    }
    catch (const std::exception& e) {}

    try
    {
      // Try to extract an array of doubles
      std::vector<double> array_real;
      for (auto& item : pt.get_child(it.first))
      {
        boost::optional<double> value_real = item.second.get_value_optional<double>();
        if (!value_real) break;
        array_real.push_back(value_real.get());
      }

      if (array_real.size() > 0  && array_real.size() == it.second.size())
      {
        d[key] = array_real;
        continue;
      }
    }
    catch (const std::exception& e) {}

    // string
    if (it.second.empty())
    {
      d[key] = it.second.data();
      continue;
    }

    // dictionary
    PyDict child;
    ptree_parse(it.second, child);
    d[key] = child;
  }
}

//-----------------------------------------------------------------------------
void PyDict::json_parse(const std::string& json_str)
{
  try
  {
    namespace pt = boost::property_tree;
    pt::ptree json;
    std::istringstream ss(json_str.c_str());
    pt::json_parser::read_json(ss, json);

    ptree_parse(json, *this);
  }
  catch (const std::exception& e)
  {
    std::cout << "Failed to parse json string : " << std::endl << std::endl;
    std::cout << json_str << std::endl << std::endl;
    throw;
  }
}

//-----------------------------------------------------------------------------
void PyDict::checkUnknownInputs(const std::vector<const ParameterBase*>& allParams)
{
  //The check inputs should have removed all keys. Hence this dict should be empty now
  if ( length() > 0 )
    BOOST_THROW_EXCEPTION( UnknownInputsException( *this, allParams ) );
}

//-----------------------------------------------------------------------------
std::vector<std::string> PyDict::stringKeys() const
{
  //Take over the GIL so threads can call python
  ThreadCallPython PyGIL;
  
  std::vector<std::string> keys;

  // Extract the keys from the dictionary
  boost::python::list pyKeys = pdict_->keys();

  for (int i = 0; i < boost::python::len(pyKeys); i++)
  {
    // The key should be a string
    boost::python::extract<std::string> key(pyKeys[i]);
    if ( !key.check() )
    {
      boost::python::extract<std::string> strkey( (boost::python::str(pyKeys[i])) );
      BOOST_THROW_EXCEPTION( KeyTypeException(strkey, parent_, Type2String<std::string>::str() ) );
    }

    // Save off the key as a string
    keys.push_back( (std::string)key );
  }

  return keys;
}

//-----------------------------------------------------------------------------
PyDict::PyDict_object_item::PyDict_object_item( const PyDict_object_item& obj ) : pobj_(nullptr)
{
  //Take over the GIL so threads can call python
  ThreadCallPython PyGIL;

  pobj_ = new boost::python::api::object_item(*obj.pobj_);
}

PyDict::PyDict_object_item&
PyDict::PyDict_object_item::operator=( const PyDict_object_item& obj )
{
  //Take over the GIL so threads can call python
  ThreadCallPython PyGIL;

  *pobj_ = *obj.pobj_;

  return *this;
}

PyDict::PyDict_object_item::PyDict_object_item( const boost::python::api::object_item& obj ) : pobj_(nullptr)
{
  //Take over the GIL so threads can call python
  ThreadCallPython PyGIL;

  pobj_ = new boost::python::api::object_item(obj);
}

PyDict::PyDict_object_item::~PyDict_object_item()
{
  delete pobj_;
}

PyDict::PyDict_object_item&
PyDict::PyDict_object_item::operator=( const std::string& val ) { ThreadCallPython PyGIL; (*pobj_) = val; return *this; }
PyDict::PyDict_object_item&
PyDict::PyDict_object_item::operator=( const int& val )         { ThreadCallPython PyGIL; (*pobj_) = val; return *this; }
PyDict::PyDict_object_item&
PyDict::PyDict_object_item::operator=( const Real& val )        { ThreadCallPython PyGIL; (*pobj_) = val; return *this; }
PyDict::PyDict_object_item&
PyDict::PyDict_object_item::operator=( const PyDict& val )      { ThreadCallPython PyGIL; (*pobj_) = (*val.pdict_); return *this; }

PyDict::PyDict_object_item&
PyDict::PyDict_object_item::operator=( const std::initializer_list<int>& val )  { ThreadCallPython PyGIL; to_list(val); return *this; }
PyDict::PyDict_object_item&
PyDict::PyDict_object_item::operator=( const std::initializer_list<Real>& val ) { ThreadCallPython PyGIL; to_list(val); return *this; }
PyDict::PyDict_object_item&
PyDict::PyDict_object_item::operator=( const std::vector<int>& val )            { ThreadCallPython PyGIL; to_list(val); return *this; }
PyDict::PyDict_object_item&
PyDict::PyDict_object_item::operator=( const std::vector<Real>& val )           { ThreadCallPython PyGIL; to_list(val); return *this; }

void PyDict::PyDict_object_item::del() { ThreadCallPython PyGIL; pobj_->del(); }

PyDict::PyDict_object_item::operator       boost::python::api::object()       { return *pobj_; }
PyDict::PyDict_object_item::operator const boost::python::api::object() const { return *pobj_; }

template<class Sequence>
void PyDict::PyDict_object_item::to_list(const Sequence& val)
{
  typename Sequence::const_iterator iter;
  boost::python::list list;
  for (iter = val.begin(); iter != val.end(); ++iter) { list.append(*iter); }
  (*pobj_) = list;
}


//-----------------------------------------------------------------------------
PyDict::PyDict_object_item PyDict::operator[]( const std::string& key )
{
  //Take over the GIL so threads can call python
  ThreadCallPython PyGIL;

  return PyDict::PyDict_object_item( (*pdict_)[boost::python::object(key)] );
}

boost::python::api::const_object_item PyDict::operator[]( const std::string& key ) const
{
  //Take over the GIL so threads can call python
  ThreadCallPython PyGIL;

  return static_cast<const boost::python::dict>(*pdict_)[boost::python::object(key)];
}

//-----------------------------------------------------------------------------
PyDict::PyDict_object_item PyDict::operator[]( const int& key )
{
  //Take over the GIL so threads can call python
  ThreadCallPython PyGIL;

  return PyDict::PyDict_object_item( (*pdict_)[boost::python::object(key)] );
}

boost::python::api::const_object_item PyDict::operator[]( const int& key ) const
{
  //Take over the GIL so threads can call python
  ThreadCallPython PyGIL;

  return static_cast<const boost::python::dict>(*pdict_)[boost::python::object(key)];
}

//-----------------------------------------------------------------------------
PyDict::operator       boost::python::dict&()       { return *pdict_; }
PyDict::operator const boost::python::dict&() const { return *pdict_; }

//-----------------------------------------------------------------------------
std::ostream&
operator<<( std::ostream& os, const PyDict& d )
{
  //Take over the GIL so threads can call python
  ThreadCallPython PyGIL;

  const boost::python::dict& dict = static_cast<const boost::python::dict&>(d);
  os << (std::string)boost::python::extract<std::string>( boost::python::str( dict ) );
  return os;
}

//Define the equality operators
bool operator==( const char* val, const DictKeyPair& p ) { return val == p.key(); }
bool operator==( const DictKeyPair& p, const char* val ) { return val == p.key(); }

bool operator==( const std::string& val, const DictKeyPair& p ) { return val == p.key(); }
bool operator==( const DictKeyPair& p, const std::string& val ) { return val == p.key(); }

bool operator==( const DictOption& d  , const DictKeyPair& kp ) { return d.name == kp.key(); }
bool operator==( const DictKeyPair& kp, const DictOption& d   ) { return d.name == kp.key(); }

std::ostream&
operator<<( std::ostream& os, const DictKeyPair& kp )
{
  //Take over the GIL so threads can call python
  ThreadCallPython PyGIL;

  const boost::python::dict& dict = *static_cast<const PyDict&>(kp).pdict_;
  os << (std::string)boost::python::extract<std::string>( boost::python::str( dict ) );
  return os;
}

}
