// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "Parameter.h"

#include "tools/SANSException.h"
#include "tools/output_std_vector.h"

#include <boost/python/dict.hpp>

#include <algorithm> // std::sort
#include <set>
#include <sstream> // stringstream

namespace SANS
{

//----------------------------------------------------------------------------//
ParameterDict::ParameterDict(const std::string& name, const empty_dict, void (*checkInputs)(PyDict), const std::string& help ) :
  ParameterBase(name, true, Type2String<ExtractType>::str(), help), checkInputs(checkInputs),
  Default() {}

ParameterDict::ParameterDict(const std::string& name, const no_default, void (*checkInputs)(PyDict), const std::string& help ) :
  ParameterBase(name, false, Type2String<ExtractType>::str(), help), checkInputs(checkInputs),
  Default() {}

//This constructor is intended for ExtractParam with dictionary options
ParameterDict::ParameterDict(const std::string& name, const bool hasDefault, void (*checkInputs)(PyDict), const std::string& help ) :
  ParameterBase(name, hasDefault, Type2String<ExtractType>::str(), help), checkInputs(checkInputs),
  Default() {}

ParameterDict::~ParameterDict() {}


//----------------------------------------------------------------------------//
template<class Type>
std::string
ParameterNumericList<Type>::default_str() const
{
  std::stringstream ss;
  ss << "[" << Default << "]";
  return ss.str();
}
template struct ParameterNumericList<int>;
template struct ParameterNumericList<double>;

//----------------------------------------------------------------------------//
//Define the equality operators
bool operator==( const char* val, const ParameterBase& p ) { return val == p.name; }
bool operator==( const ParameterBase& p, const char* val ) { return val == p.name; }

bool operator==( const std::string& val, const ParameterBase& p ) { return val == p.name; }
bool operator==( const ParameterBase& p, const std::string& val ) { return val == p.name; }


//Define the equality operators
bool operator==( const char* val, const DictOption& p ) { return val == p.name; }
bool operator==( const DictOption& p, const char* val ) { return val == p.name; }

bool operator==( const std::string& val, const DictOption& p ) { return val == p.name; }
bool operator==( const DictOption& p, const std::string& val ) { return val == p.name; }

bool operator!=( const std::string& val, const DictOption& p ) { return val != p.name; }
bool operator!=( const DictOption& p, const std::string& val ) { return val != p.name; }

//----------------------------------------------------------------------------//
// cppcheck-suppress passedByValue
void ParamsNone::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  d.checkUnknownInputs(allParams);
}

} // namepsace SANS

//----------------------------------------------------------------------------//
// instantiate this very common version of std::vector
template class std::vector<const SANS::ParameterBase*>;
