// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "PyInit.h"
#include <boost/python/detail/prefix.hpp>

namespace SANS
{

InitPythonBase::InitPythonBase()
{
  //This is needed if any static python objects are created in the code
  if (!Py_IsInitialized())
  {
    Py_InitializeEx(0); //This allows Ctrl+C to kill unit tests
    PyEval_InitThreads();
  }
}


}
