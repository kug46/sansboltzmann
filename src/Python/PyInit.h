// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SANS_PYINIT_H
#define SANS_PYINIT_H

namespace SANS
{

//This simply ensures that python has been initialized. Needed for global python objects
struct InitPythonBase
{
  InitPythonBase();
};

} // namespace SANS

#endif //SANS_PYINIT_H
