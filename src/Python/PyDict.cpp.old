// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "PyDict.h"

#include "tools/stringify.h"

namespace SANS
{

#if 0
//=============================================================================
  template<typename T>
  void WarnDefault(const std::string& key, const std::string& parent, const T& def)
  {
    std::string Wstr;
    Wstr  = "INFO: Unable to find the dictionary key '";
    Wstr += parent;
    Wstr += "[\"";
    Wstr += key;
    Wstr += "\"]'. A default value of '";
    Wstr += tools::cast_to_string<T>(def);
    Wstr += "' has been applied.";
    std::cout << Wstr << std::endl;
  }

  template void WarnDefault<bool>       (const std::string& key, const std::string& parent, const bool&        def);
  template void WarnDefault<int>        (const std::string& key, const std::string& parent, const int&         def);
  template void WarnDefault<float>      (const std::string& key, const std::string& parent, const float&       def);
  template void WarnDefault<double>     (const std::string& key, const std::string& parent, const double&      def);
  template void WarnDefault<std::string>(const std::string& key, const std::string& parent, const std::string& def);
#endif

//=============================================================================
  PyDict::PyDict()
    : ThreadCallPythonBase(), base(), parent("")
  {
    //Now release the GIL that was taken over by ThreadCallPythonBase
    ThreadCallPythonBase::ReleaseGIL();
  }

  PyDict::PyDict(const PyDict& dict) : ThreadCallPythonBase(), base(dict), parent(dict.parent)
  {
    //Now release the GIL that was taken over by ThreadCallPythonBase
    ThreadCallPythonBase::ReleaseGIL();
  }

  PyDict::PyDict(boost::python::dict& d_in)
    : ThreadCallPythonBase(), base(d_in), parent("")
  {
    //Now release the GIL that was taken over by ThreadCallPythonBase
    ThreadCallPythonBase::ReleaseGIL();
  }

  PyDict::PyDict(boost::python::dict& d_in, const std::string ParentString, const std::string ParentKey)
    : ThreadCallPythonBase(), base(d_in), parent(ParentString.size() ? ParentString + "." + ParentKey : ParentKey)
  {
    //Now release the GIL that was taken over by ThreadCallPythonBase
    ThreadCallPythonBase::ReleaseGIL();
  }

  PyDict::PyDict(boost::python::dict& d_in, const std::string ParentString, const int ParentKey)
    : ThreadCallPythonBase(), base(d_in), parent(ParentString + "[" + stringify(ParentKey) + "]")
  {
    //Now release the GIL that was taken over by ThreadCallPythonBase
    ThreadCallPythonBase::ReleaseGIL();
  }

  bool PyDict::has_key(const int k) const

  {
    //Take over the GIL so threads can call python
    ThreadCallPython PyGIL;

    return base::has_key(k);
  }

  bool PyDict::has_key(const std::string k) const
  {
    //Take over the GIL so threads can call python
    ThreadCallPython PyGIL;

    return base::has_key(k);
  }

  ssize_t PyDict::length() const
  {
    //Take over the GIL so threads can call python
    ThreadCallPython PyGIL;

    return boost::python::len( base::keys() );
  }

//-----------------------------------------------------------------------------
  PyDict::PyDict_object_item PyDict::operator[]( const std::string& key )
  {
    //Take over the GIL so threads can call python
    ThreadCallPython PyGIL;

    return PyDict::PyDict_object_item( base::operator[](boost::python::object(key)) );
  }

  boost::python::api::const_object_item PyDict::operator[]( const std::string& key ) const
  {
    //Take over the GIL so threads can call python
    ThreadCallPython PyGIL;

    return base::operator[](boost::python::object(key));
  }

//-----------------------------------------------------------------------------
  PyDict::PyDict_object_item PyDict::operator[]( const int& key )
  {
    //Take over the GIL so threads can call python
    ThreadCallPython PyGIL;

    return PyDict::PyDict_object_item( base::operator[](boost::python::object(key)) );
  }

  boost::python::api::const_object_item PyDict::operator[]( const int& key ) const
  {
    //Take over the GIL so threads can call python
    ThreadCallPython PyGIL;

    return base::operator[](boost::python::object(key));
  }

  DictKeyPairRef
  operator,(PyDict& d, std::string& k) { return DictKeyPairRef(d,k); }

#if 0
  XDGList PyDict::keys()
  {
    //Take over the GIL so threads can call python
    ThreadCallPython PyGIL;

    return XDGList(d.keys());
  }

  XDGList PyDict::items()
  {
    //Take over the GIL so threads can call python
    ThreadCallPython PyGIL;

    return XDGList(d.items());
  }


//=============================================================================
  //PyDict TOOLS
//=============================================================================
  unsigned int GetNumKeysWithName(const PyDict dict, const std::string& Prefix, const std::string& Suffix, const unsigned int istart)
  {
    //Take over the GIL so threads can call python
    tools::ScopedThreadPythonCall PyGIL;

    unsigned int i = istart;
    while ( dict.has_key(Prefix + tools::cast_to_string(i) + Suffix) )
      i++;

    return i - istart;
  }
#endif

}
