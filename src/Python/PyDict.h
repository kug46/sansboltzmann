// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PYDICT_H
#define PYDICT_H

#include <iostream>
#include <vector>
#include <initializer_list>

#include "ParameterType.h"
#include "ExtractParam_fwd.h"

#include "tools/SANSnumerics.h"

// Forward declare python dictionary to reduce compile times
namespace boost
{
namespace python
{
class dict;
class list;
class tuple;

namespace api
{
template <class Policies> class proxy;

struct const_item_policies;
struct item_policies;

typedef proxy<const_item_policies> const_object_item;
typedef proxy<item_policies> object_item;

class object;
}
using api::object;
}
}

namespace SANS
{

//template<typename T>
//void WarnDefaultApplied(const std::string& key, const std::string& parent, const T& def);

class DictKeyPair;
struct empty_PyDict;

//=============================================================================
struct PyDict  //: public ThreadCallPythonBase
{
  PyDict();
  PyDict(const PyDict& dict);
  PyDict& operator=(const PyDict& dict);

  // cppcheck-suppress noExplicitConstructor
  PyDict(const empty_PyDict&); //Used to initialize an empty dictionary

  // cppcheck-suppress noExplicitConstructor
  PyDict(const boost::python::dict& d_in);
  PyDict(boost::python::dict& d_in, const std::string& ParentString, const std::string& ParentKey);
  PyDict(boost::python::dict& d_in, const std::string& ParentString, const int ParentKey);

  ~PyDict();

//-----------------------------------------------------------------------------
  bool has_key(const int k) const;
  bool has_key(const std::string& k) const;
  ssize_t length() const;
  boost::python::tuple popitem();
  boost::python::list keys() const;

  std::string str() const;
  std::string json() const;

  void json_parse(const std::string& json_str);

  inline std::string getParent() const { return parent_; }

  template<typename T>
  void setdefault(const std::string& k, const T& val);

//-----------------------------------------------------------------------------
  template<typename T, typename key_type>
  bool check(const key_type& k) const;

  template<typename Param>
  typename Param::ExtractType get(const ParameterType<Param>& paramtype) const;

  template<typename Param>
  const ParameterBase* checkInputs(const ParameterType<Param>& paramtype);

  void checkUnknownInputs(const std::vector<const ParameterBase*>& allParams);

  std::vector<std::string> stringKeys() const;

  struct PyDict_object_item
  {
    explicit PyDict_object_item( const boost::python::api::object_item& obj );
    PyDict_object_item(const PyDict_object_item& obj);
    PyDict_object_item& operator=(const PyDict_object_item& obj);
    ~PyDict_object_item();

    void del();

    PyDict_object_item& operator=( const std::string& val );
    PyDict_object_item& operator=( const int& val );
    PyDict_object_item& operator=( const Real& val );
    PyDict_object_item& operator=( const PyDict& val );
    PyDict_object_item& operator=( const std::initializer_list<int>& val );
    PyDict_object_item& operator=( const std::initializer_list<Real>& val );
    PyDict_object_item& operator=( const std::vector<int>& val );
    PyDict_object_item& operator=( const std::vector<Real>& val );

    operator       boost::python::api::object();
    operator const boost::python::api::object() const;

  protected:
    template<class Sequence>
    void to_list(const Sequence& val);

    boost::python::api::object_item *pobj_;
  };

//-----------------------------------------------------------------------------
  PyDict_object_item operator[]( const std::string& key );
  boost::python::api::const_object_item operator[]( const std::string& key ) const;

//-----------------------------------------------------------------------------
  PyDict_object_item operator[]( const int& key );
  boost::python::api::const_object_item operator[]( const int& key ) const;

  friend std::ostream& operator<<( std::ostream& os, const DictKeyPair& kp );

  operator       boost::python::dict&();
  operator const boost::python::dict&() const;

  // This use of pointer allows for a simple forward
  // declaration of boost::python::dict instead of including boost/python/dict.hpp which greatly
  // increases compile time
protected:
  boost::python::dict* pdict_;
  std::string parent_;
};

std::ostream&
operator<<( std::ostream& os, const PyDict& kp );


//=============================================================================
//A simple structure for storing a dictionary and a string key together
class DictKeyPair
{
public:
  DictKeyPair() {}
  // Needed for ParameterOptions defaults
  // cppcheck-suppress noExplicitConstructor
  DictKeyPair(const std::string& k) : key_(k) {}

  DictKeyPair(PyDict& d, const std::string& k) : dict_(d), key_(k) {}

  //Allow for conversion to either the string or dictionary
  operator std::string() const   { return key_; }
  operator       PyDict&()       { return dict_; }
  operator const PyDict&() const { return dict_; }

  //An explicit way of getting the key value
  const std::string& key() const { return key_; }

  //Expose some functions to the dict
  bool has_key(const int k) const { return dict_.has_key(k); }
  bool has_key(const std::string& k) const { return dict_.has_key(k); }
  ssize_t length() const { return dict_.length(); }
  template<typename T>
  typename T::ExtractType get(const ParameterType<T>& paramtype) const { return dict_.get(paramtype); }

protected:
  PyDict dict_;
  std::string key_;
};

//Operators so that a DictOption can be compared with a const char*
bool operator==( const char* val, const DictKeyPair& p );
bool operator==( const DictKeyPair& p, const char* val );

bool operator==( const std::string& val, const DictKeyPair& p );
bool operator==( const DictKeyPair& p, const std::string& val );

bool operator==( const DictOption& d, const DictKeyPair& kp );
bool operator==( const DictKeyPair& kp, const DictOption& d );

std::ostream&
operator<<( std::ostream& os, const DictKeyPair& kp );

}

#endif //PYDICT_H
