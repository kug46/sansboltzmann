// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INPUTEXCEPTION_H
#define INPUTEXCEPTION_H

#include "tools/SANSException.h"

#include "ParameterType.h"

#include <vector>
#include <string>
#include <map>
#include <ios>

namespace SANS
{
  //Forward declare
  struct PyDict;

  std::string ParentString(const std::string& parent, const std::string& key );
  std::string ParentString(const std::string& parent, const int& key );

//=============================================================================
  //exception a numeric value is outside the specified range
  struct NumericOutOfRangeException : SANSException
  {
    NumericOutOfRangeException( const std::string& param, const std::string& parent, const std::string& val,
                                const std::string& min, const bool hasMin,
                                const std::string& max, const bool hasMax,
                                const std::string& help );

    NumericOutOfRangeException( const int i, const std::string& param, const std::string& parent, const std::string& val,
                                const std::string& min, const bool hasMin,
                                const std::string& max, const bool hasMax,
                                const std::string& help );

    virtual ~NumericOutOfRangeException() throw() {}
  };


//=============================================================================
  //exception when a key is not found in the dictionary
  struct NoDefaultException : SANSException
  {
    NoDefaultException(const std::string& param, const std::string& parent, const std::string& help);

    virtual ~NoDefaultException() throw() {}
  };

//=============================================================================
  //exception when an incorrect type of value was given for a corresponding dictionary key
  struct DictValueTypeException : SANSException
  {
    DictValueTypeException(const std::string& param, const std::string& parent,
                           const std::string& val, const std::string& type, const std::string& help);

    virtual ~DictValueTypeException() throw() {}
  };

//=============================================================================
  //exception when an incorrect type of a dictionary key is given
  struct KeyTypeException : SANSException
  {
    KeyTypeException(const std::string& key, const std::string& parent, const std::string& type);

    virtual ~KeyTypeException() throw() {}
  };

//=============================================================================
  //exception when an invalid file name is provided
  struct FileNameException : SANSException
  {
    FileNameException(const std::string& param, const std::string& parent,
                      const std::string& filename, const std::ios_base::openmode mode, const std::string& help);

    virtual ~FileNameException() throw() {}
  };

//=============================================================================
  //exception when a string option does not match any of the available options
  template<class T>
  struct OptionException : SANSException
  {
    OptionException(const std::string& param, const std::string& parent, const T& val,
                    const std::vector<T>& options, const std::string& help);

    virtual ~OptionException() throw() {}
  };

//=============================================================================
  //exception when additional entries in a dict exist that are not used
  struct UnknownInputsException : SANSException
  {
    explicit UnknownInputsException(PyDict& d, const std::vector<const ParameterBase*>& allParams);

    virtual ~UnknownInputsException() throw() {}
  };

//=============================================================================
  //exception when an incorrect type of a dictionary key is given
  struct BCException : SANSException
  {
    BCException(const PyDict& BCList,
                const std::map< std::string, std::vector<int> >& BoundaryGroups);

    virtual ~BCException() throw() {}
  };

//----------------------------------------------------------------------------//
  // A function for checking consistency between BC groups and BC types
  void checkBCInputs( PyDict& BCList,
                      const std::map< std::string, std::vector<int> >& BoundaryGroups);

}

// Reduce compile time by explicitly instantiating these
extern template class boost::exception_detail::clone_impl<SANS::NumericOutOfRangeException>;
extern template class boost::exception_detail::clone_impl<SANS::NoDefaultException>;
extern template class boost::exception_detail::clone_impl<SANS::DictValueTypeException>;
extern template class boost::exception_detail::clone_impl<SANS::KeyTypeException>;
extern template class boost::exception_detail::clone_impl<SANS::FileNameException>;
extern template class boost::exception_detail::clone_impl<SANS::OptionException<SANS::DictOption>>;
extern template class boost::exception_detail::clone_impl<SANS::OptionException<std::string>>;
extern template class boost::exception_detail::clone_impl<SANS::OptionException<int>>;
extern template class boost::exception_detail::clone_impl<SANS::UnknownInputsException>;

extern template void boost::throw_exception<SANS::NumericOutOfRangeException>(SANS::NumericOutOfRangeException const&);
extern template void boost::throw_exception<SANS::NoDefaultException>(SANS::NoDefaultException const&);
extern template void boost::throw_exception<SANS::DictValueTypeException>(SANS::DictValueTypeException const&);
extern template void boost::throw_exception<SANS::KeyTypeException>(SANS::KeyTypeException const&);
extern template void boost::throw_exception<SANS::FileNameException>(SANS::FileNameException const&);
extern template void boost::throw_exception<SANS::OptionException<SANS::DictOption>>(SANS::OptionException<SANS::DictOption> const&);
extern template void boost::throw_exception<SANS::OptionException<std::string>>(SANS::OptionException<std::string> const&);
extern template void boost::throw_exception<SANS::OptionException<int>>(SANS::OptionException<int> const&);
extern template void boost::throw_exception<SANS::UnknownInputsException>(SANS::UnknownInputsException const&);

extern template void boost::exception_detail::throw_exception_<SANS::NumericOutOfRangeException>(SANS::NumericOutOfRangeException const&, char const*, char const*, int);
extern template void boost::exception_detail::throw_exception_<SANS::NoDefaultException>(SANS::NoDefaultException const&, char const*, char const*, int);
extern template void boost::exception_detail::throw_exception_<SANS::DictValueTypeException>(SANS::DictValueTypeException const&, char const*, char const*, int);
extern template void boost::exception_detail::throw_exception_<SANS::KeyTypeException>(SANS::KeyTypeException const&, char const*, char const*, int);
extern template void boost::exception_detail::throw_exception_<SANS::FileNameException>(SANS::FileNameException const&, char const*, char const*, int);
extern template void boost::exception_detail::throw_exception_<SANS::OptionException<SANS::DictOption>>(SANS::OptionException<SANS::DictOption> const&, char const*, char const*, int);
extern template void boost::exception_detail::throw_exception_<SANS::OptionException<std::string>>(SANS::OptionException<std::string> const&, char const*, char const*, int);
extern template void boost::exception_detail::throw_exception_<SANS::OptionException<int>>(SANS::OptionException<int> const&, char const*, char const*, int);
extern template void boost::exception_detail::throw_exception_<SANS::UnknownInputsException>(SANS::UnknownInputsException const&, char const*, char const*, int);

#endif //INPUTEXCEPTION_H
