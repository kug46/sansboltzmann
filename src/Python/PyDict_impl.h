// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#if !defined(PYDICT_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include <boost/python/dict.hpp>
#include <boost/python/extract.hpp>
#include <boost/python/str.hpp>

#include "PyDict.h"

#include <string>
#include <type_traits>

#include "PyGIL.h"
#include "Parameter.h"
#include "InputException.h"
#include "ExtractParam_fwd.h"

#include "tools/SANSException.h"
#include "tools/stringify.h"
#include "tools/Type2String.h"

// A convenient macro for instantiation PyDict::get function for ParameterOptions
#define PYDICT_PARAMETER_OPTION_INSTANTIATE( OPTIONS ) \
template ParameterOption<OPTIONS>::ExtractType \
PyDict::get(ParameterType< ParameterOption<OPTIONS> > const&) const;

namespace SANS
{

//-----------------------------------------------------------------------------
// Make sure common parameter instantiations are declared extern
#define PYDICT_EXTERN( Param ) \
  extern template typename Param::ExtractType PyDict::get(const ParameterType<Param>& paramtype) const; \
  extern template const ParameterBase* PyDict::checkInputs(const ParameterType<Param>& paramtype);

PYDICT_EXTERN( ParameterNumeric<int> )
PYDICT_EXTERN( ParameterNumeric<Real> )
PYDICT_EXTERN( ParameterNumericList<int> )
PYDICT_EXTERN( ParameterNumericList<Real> )
PYDICT_EXTERN( ParameterBool )
PYDICT_EXTERN( ParameterDict )
PYDICT_EXTERN( ParameterString )
PYDICT_EXTERN( ParameterFileName )

//-----------------------------------------------------------------------------
template<typename T>
void PyDict::setdefault(const std::string& k, const T& val)
{
  //Take over the GIL so threads can call python
  ThreadCallPython PyGIL;

  if (!has_key(k))
    pdict_->setdefault(k, val);
}

//-----------------------------------------------------------------------------
template<typename T, typename key_type>
bool PyDict::check(const key_type& k) const
{
  //Take over the GIL so threads can call python
  ThreadCallPython PyGIL;

  boost::python::extract<T> val((*pdict_)[k]);
  return val.check();
}

//-----------------------------------------------------------------------------
template<typename Param>
typename Param::ExtractType PyDict::get(const ParameterType<Param>& paramtype) const
{
  //Take over the GIL so threads can call python
  ThreadCallPython PyGIL;

  const Param& param = paramtype.cast();

  if ( !has_key(param) )
  {
    if ( param.hasDefault )
      return param.Default;
    else
      BOOST_THROW_EXCEPTION( NoDefaultException(param, parent_, param.help) );
  }

  return ExtractParam<typename Param::ExtractType, Param>::extract(param, *this, parent_);
}

//-----------------------------------------------------------------------------
template<typename Param>
const ParameterBase* PyDict::checkInputs(const ParameterType<Param>& paramtype)
{
  const Param& param = paramtype.cast();

  //This will do all the checking
  ExtractParam<typename Param::ExtractType, Param>::checkInputs(param, *this);

  //Remove the key from the dictionary
  if ( has_key(param) )
    (*this)[param].del();

  // Return the base pointer to the parameter so it can be used in checkUnknownInputs
  return &param;
}


//===========================================================================//
// A generic extraction class
template< class ExtractType, class paramtype >
struct ExtractParam
{
  typedef typename paramtype::ExtractType T;
  static_assert( std::is_same<ExtractType,T>::value, "Should be the same." );

  static T extract(const paramtype& param, const boost::python::dict& d, const std::string& parent)
  {
    //Take over the GIL so threads can call python
    ThreadCallPython PyGIL;

    std::string k = param;

    boost::python::extract<T> val(d[k]);
    if ( val.check() )
      return val;

    boost::python::extract<std::string> strval( (boost::python::str(d[k])) );
    BOOST_THROW_EXCEPTION( DictValueTypeException(k, parent, strval, Type2String<T>::str(), param.help ) );

    return T();
  }

  static void checkInputs(const paramtype& param, const PyDict& d) { d.get(param); }
};

//===========================================================================//
template< class T >
struct ExtractParam< T, ParameterNumeric< T > >
{
  static T extract(const ParameterNumeric< T >& param, const boost::python::dict& d, const std::string& parent);

  static void checkInputs(const ParameterNumeric<T>& param, const PyDict& d);
};

//===========================================================================//
template< class T >
struct ExtractParam< std::vector<T>, ParameterNumericList< T > >
{
  static std::vector<T> extract(const ParameterNumericList< T >& param, const boost::python::dict& d, const std::string& parent);

  static void checkInputs(const ParameterNumericList<T>& param, const PyDict& d);
};

//===========================================================================//
template<>
struct ExtractParam<ParameterDict::ExtractType, ParameterDict>
{
  typedef ParameterDict::ExtractType T;
  static T extract(const ParameterDict& param, const boost::python::dict& d, const std::string& parent);

  static void checkInputs(const ParameterDict& param, const PyDict& d);
};

//===========================================================================//
template<>
struct ExtractParam<ParameterFileName::ExtractType, ParameterFileName>
{
  typedef ParameterFileName::ExtractType T;
  static T extract(const ParameterFileName& param, const boost::python::dict& d, const std::string& parent);

  static void checkInputs(const ParameterFileName& param, const PyDict& d);
};

//===========================================================================//
template< class ExtractType, class options >
struct ExtractParam< ExtractType, ParameterOption<options> >
{
  typedef typename ParameterOption<options>::ExtractType T;
  static_assert( std::is_same<ExtractType,T>::value, "Should be the same." );

  static T extract(const ParameterOption<options>& param, const boost::python::dict& d, const std::string& parent)
  {
    //Take over the GIL so threads can call python
    ThreadCallPython PyGIL;

    std::string k = param;

    boost::python::extract<T> val(d[k]);
    if ( val.check() )
    {
      T opt = val;
      for ( std::size_t i = 0; i < param.options.size(); i++ )
        if ( param.options[i] == opt )
          return opt;

      BOOST_THROW_EXCEPTION( OptionException<T>(param, parent, opt, param.options, param.help ) );
    }

    boost::python::extract<std::string> strval( (boost::python::str(d[k])) );
    BOOST_THROW_EXCEPTION( DictValueTypeException(k, parent, strval, Type2String<T>::str(), param.help ) );

    return T();
  }

  static void checkInputs(const ParameterOption<options>& param, const PyDict& d) { d.get(param); }
};

//===========================================================================//
template< class options >
struct ExtractParam< DictKeyPair, ParameterOption<options> >
{
  typedef typename options::ExtractType T;
  static_assert( std::is_same<DictKeyPair,T>::value, "Should be the same." );

  static DictKeyPair extract(const ParameterOption<options>& param, const PyDict& d, const std::string& parent)
  {
    //Take over the GIL so threads can call python
    ThreadCallPython PyGIL;

    //There must be at least one option, or the parameter is pointless
    SANS_ASSERT( param.options.size() > 0 );

    ParameterDict paramdict(param.name, param.hasDefault, param.options[0].checkInputs, param.help);

    PyDict optiondict = d.get(paramdict);
    std::string optionkey = optiondict.get(param.key);

    for ( std::size_t i = 0; i < param.options.size(); i++ )
      if ( optionkey == param.options[i] )
        return DictKeyPair(optiondict, optionkey);

    //Make a temporary variable for the exception
    DictOption dval(optionkey, param.options[0].checkInputs);

    BOOST_THROW_EXCEPTION( OptionException<DictOption>(param, parent, dval, param.options, param.help ) );

    return DictKeyPair(optiondict, optionkey); //Just for compiler warnings
  }

  static void checkInputs(const ParameterOption<options>& param, const PyDict& d)
  {
    //Get the dict/key pair
    DictKeyPair pair = d.get(param);

    //Get the key option, and find which option has been selected so the appropriate
    //checkInputs is called
    std::string optionkey = pair.get(param.key);

    //Remove the key from the dict so the check for Unknown keys is not triggered
    if ( pair.has_key(param.key) )
      ((PyDict&)pair)[param.key].del();

    //Check the correct dictionary now depending on the selected option
    int checkCount = 0;
    for ( std::size_t i = 0; i < param.options.size(); i++ )
      if ( param.options[i] == optionkey )
      {
        checkCount++;
        param.options[i].checkInputs(pair);
      }
    SANS_ASSERT_MSG( checkCount == 1, "Duplicate options detected" );
  }
};

} //namespace SANS
