// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PARAMETER_H
#define PARAMETER_H

#include "tools/Type2String.h"
#include "tools/stringify.h"
#include "tools/noncopyable.h"

#include "ParameterType.h"
#include "PyInit.h"
#include "PyDict.h"

#include <string>
#include <vector>
#include <ios> // ios_base

#if 0
#include <boost/preprocessor/repetition/enum.hpp>
#include <boost/preprocessor/repetition/repeat.hpp>
#include <boost/preprocessor/repetition/repeat_from_to.hpp>

#include <boost/preprocessor/stringize.hpp>

#include <boost/preprocessor/tuple/elem.hpp>

#include <boost/preprocessor/seq/size.hpp>
#include <boost/preprocessor/seq/elem.hpp>
#include <boost/preprocessor/seq/for_each.hpp>
#include <boost/preprocessor/seq/enum.hpp>
#include <boost/preprocessor/seq/seq.hpp>
#endif

namespace SANS
{

struct ParameterBase
{
  ParameterBase( const std::string& name, const bool hasDefault, const std::string& type, const std::string& help ) :
    name(name), hasDefault(hasDefault), type(type), help(help) {}

  operator std::string() const { return name; }
  operator const char*() const { return name.c_str(); }

  virtual ~ParameterBase() {}

  virtual std::string default_str() const = 0;

  const std::string name;
  const bool hasDefault;
  const std::string type;
  const std::string help;
};

//Operators so that a parameter can be compared with a const char*
bool operator==( const char* val, const ParameterBase& p );
bool operator==( const ParameterBase& p, const char* val );

bool operator==( const std::string& val, const ParameterBase& p );
bool operator==( const ParameterBase& p, const std::string& val );

struct no_default {};
struct no_limit {};
struct no_range {};
struct empty_dict {};
struct empty_list {};
struct empty_PyDict {};

#define NO_DEFAULT ::SANS::no_default()
#define NO_LIMIT ::SANS::no_limit()
#define NO_RANGE ::SANS::no_range()
#define EMPTY_DICT ::SANS::empty_dict()
#define EMPTY_LIST ::SANS::empty_list()

//----------------------------------------------------------------------------//
//A class for a numeric parameter
template<class Type>
struct ParameterNumeric : public ParameterType< ParameterNumeric<Type> >, public ParameterBase
{
  typedef Type ExtractType;

  ParameterNumeric(const std::string& name, const ExtractType Default,
                   const ExtractType min, const ExtractType max,
                   const std::string& help ) :
    ParameterBase(name, true, Type2String<ExtractType>::str(), help), Default(Default),
    hasMin(true), min(min),
    hasMax(true), max(max) {}

  ParameterNumeric(const std::string& name, const ExtractType Default,
                   const no_limit, const ExtractType max,
                   const std::string& help ) :
    ParameterBase(name, true, Type2String<ExtractType>::str(), help), Default(Default),
    hasMin(false), min(0),
    hasMax(true), max(max) {}

  ParameterNumeric(const std::string& name, const ExtractType Default,
                   const ExtractType min, const no_limit,
                   const std::string& help ) :
    ParameterBase(name, true, Type2String<ExtractType>::str(), help), Default(Default),
    hasMin(true), min(min),
    hasMax(false), max(0) {}



  ParameterNumeric(const std::string& name, const no_default,
                   const ExtractType min, const ExtractType max,
                   const std::string& help ) :
    ParameterBase(name, false, Type2String<ExtractType>::str(), help), Default(0),
    hasMin(true), min(min),
    hasMax(true), max(max) {}

  ParameterNumeric(const std::string& name, const no_default,
                   const no_limit, const ExtractType max,
                   const std::string& help ) :
    ParameterBase(name, false, Type2String<ExtractType>::str(), help), Default(0),
    hasMin(false), min(0),
    hasMax(true), max(max) {}

  ParameterNumeric(const std::string& name, const no_default,
                   const ExtractType min, const no_limit,
                   const std::string& help ) :
    ParameterBase(name, false, Type2String<ExtractType>::str(), help), Default(0),
    hasMin(true), min(min),
    hasMax(false), max(0) {}



  ParameterNumeric(const std::string& name, const ExtractType Default,
                   const no_range,
                   const std::string& help ) :
    ParameterBase(name, true, Type2String<ExtractType>::str(), help), Default(Default),
    hasMin(false), min(0),
    hasMax(false), max(0) {}

  ParameterNumeric(const std::string& name, const no_default,
                   const no_range,
                   const std::string& help ) :
    ParameterBase(name, false, Type2String<ExtractType>::str(), help), Default(0),
    hasMin(false), min(0),
    hasMax(false), max(0) {}

  virtual ~ParameterNumeric() {}

  virtual std::string default_str() const override { return stringify( Default ); }

  const ExtractType Default;

  const bool hasMin;
  const ExtractType min;

  const bool hasMax;
  const ExtractType max;
};


//----------------------------------------------------------------------------//
//A class for a list of numeric parameters
template<class Type>
struct ParameterNumericList : public ParameterType< ParameterNumericList<Type> >, public ParameterBase
{
  typedef std::vector<Type> ExtractType;

  ParameterNumericList(const std::string& name, const empty_list,
                       const Type min, const Type max,
                       const std::string& help ) :
    ParameterBase(name, true, Type2String<ExtractType>::str(), help),
    hasMin(true), min(min),
    hasMax(true), max(max) {}

  ParameterNumericList(const std::string& name, const empty_list,
                       const no_limit, const Type max,
                       const std::string& help ) :
    ParameterBase(name, true, Type2String<ExtractType>::str(), help),
    hasMin(false), min(0),
    hasMax(true), max(max) {}

  ParameterNumericList(const std::string& name, const empty_list,
                       const Type min, const no_limit,
                       const std::string& help ) :
    ParameterBase(name, true, Type2String<ExtractType>::str(), help),
    hasMin(true), min(min),
    hasMax(false), max(0) {}



  ParameterNumericList(const std::string& name, const no_default,
                       const Type min, const Type max,
                       const std::string& help ) :
    ParameterBase(name, false, Type2String<ExtractType>::str(), help),
    hasMin(true), min(min),
    hasMax(true), max(max) {}

  ParameterNumericList(const std::string& name, const no_default,
                       const no_limit, const Type max,
                       const std::string& help ) :
    ParameterBase(name, false, Type2String<ExtractType>::str(), help),
    hasMin(false), min(0),
    hasMax(true), max(max) {}

  ParameterNumericList(const std::string& name, const no_default,
                       const Type min, const no_limit,
                       const std::string& help ) :
    ParameterBase(name, false, Type2String<ExtractType>::str(), help),
    hasMin(true), min(min),
    hasMax(false), max(0) {}


  ParameterNumericList(const std::string& name, const ExtractType& Default,
                       const Type min, const no_limit,
                       const std::string& help ) :
    ParameterBase(name, true, Type2String<ExtractType>::str(), help),
    Default(Default),
    hasMin(true), min(min),
    hasMax(false), max(0) {}



  ParameterNumericList(const std::string& name, const empty_list,
                       const no_range,
                       const std::string& help ) :
    ParameterBase(name, true, Type2String<ExtractType>::str(), help),
    hasMin(false), min(0),
    hasMax(false), max(0) {}

  ParameterNumericList(const std::string& name, const no_default,
                       const no_range,
                       const std::string& help ) :
    ParameterBase(name, false, Type2String<ExtractType>::str(), help),
    hasMin(false), min(0),
    hasMax(false), max(0) {}

  virtual ~ParameterNumericList() {}

  virtual std::string default_str() const override;

  const ExtractType Default;

  const bool hasMin;
  const Type min;

  const bool hasMax;
  const Type max;
};


//----------------------------------------------------------------------------//
//A class for a string parameter
struct ParameterString : public ParameterType<ParameterString>, public ParameterBase
{
  typedef std::string ExtractType;

  ParameterString(const std::string& name, const std::string& Default, const std::string& help ) :
    ParameterBase(name, true, Type2String<ExtractType>::str(), help), Default(Default) {}

  ParameterString(const std::string& name, const no_default, const std::string& help ) :
    ParameterBase(name, false, Type2String<ExtractType>::str(), help), Default("") {}

  virtual ~ParameterString() {}

  virtual std::string default_str() const override { return Default; }

  const std::string Default;
};

//----------------------------------------------------------------------------//
//A class for a filename parameter
struct ParameterFileName : public ParameterType<ParameterFileName>, public ParameterBase
{
  typedef std::string ExtractType;

  ParameterFileName(const std::string& name, std::ios_base::openmode mode, const std::string& Default, const std::string& help ) :
    ParameterBase(name, true, Type2String<ExtractType>::str(), help), mode(mode), Default(Default) {}

  ParameterFileName(const std::string& name, std::ios_base::openmode mode, const no_default, const std::string& help ) :
    ParameterBase(name, false, Type2String<ExtractType>::str(), help), mode(mode), Default("") {}

  virtual ~ParameterFileName() {}

  virtual std::string default_str() const override { return Default; }

  const std::ios_base::openmode mode;
  const std::string Default;
};

//----------------------------------------------------------------------------//
//A class for a boolean parameter
struct ParameterBool : public ParameterType<ParameterBool>, public ParameterBase
{
  typedef bool ExtractType;

  ParameterBool(const std::string& name, const bool Default, const std::string& help ) :
    ParameterBase(name, true, Type2String<ExtractType>::str(), help), Default(Default) {}

  ParameterBool(const std::string& name, const no_default, const std::string& help ) :
    ParameterBase(name, false, Type2String<ExtractType>::str(), help), Default(false) {}

  virtual ~ParameterBool() {}

  virtual std::string default_str() const override { return stringify( Default ); }

  const bool Default;
};


//----------------------------------------------------------------------------//
//A class for a dictionary parameter
struct ParameterDict : public ParameterType<ParameterDict>, public ParameterBase
{
  typedef PyDict ExtractType;

  ParameterDict(const std::string& name, const empty_dict, void (*checkInputs)(PyDict), const std::string& help );

  ParameterDict(const std::string& name, const no_default, void (*checkInputs)(PyDict), const std::string& help );

  //This constructor is intended for ExtractParam with dictionary options
  ParameterDict(const std::string& name, const bool hasDefault, void (*checkInputs)(PyDict), const std::string& help );

  virtual ~ParameterDict();

  virtual std::string default_str() const override { return "{}"; }

  void (*checkInputs)(PyDict);

  const empty_PyDict Default;
};

// Chose a the default type so we don't have a static PyDict that would be destroyed after calling PyFinalize
template<class ExtractType>
struct ParameterDefaultType
{
  typedef ExtractType type;
};

template<>
struct ParameterDefaultType<PyDict>
{
  typedef empty_PyDict type;
};

template<>
struct ParameterDefaultType<DictKeyPair>
{
  typedef std::string type;
};

//----------------------------------------------------------------------------//
//This is a generic class for a parameter with specific options
template<class options>
struct ParameterOption : public ParameterType< ParameterOption<options> >, public ParameterBase, public options
{
  typedef typename options::ExtractType ExtractType;
  typedef typename ParameterDefaultType<ExtractType>::type DefaultType;

  //Special case for dictionary options with a default empty dict
  ParameterOption(const std::string& name, const empty_dict, const char* help ) :
    ParameterBase(name, true, Type2String<ExtractType>::str(), help),  Default() {}

  //General interface for a default option
  ParameterOption(const std::string& name, const DefaultType Default, const char* help ) :
    ParameterBase(name, true, Type2String<ExtractType>::str(), help),  Default(Default) {}

  //General interface for no default option
  ParameterOption(const std::string& name, const no_default, const char* help ) :
    ParameterBase(name, false, Type2String<ExtractType>::str(), help), Default() {}

  virtual ~ParameterOption() {}

  virtual std::string default_str() const override { return stringify( Default ); }

  const DefaultType Default;
};

//----------------------------------------------------------------------------//
struct DictOption
{
  DictOption(const std::string& name, void (*checkInputs)(PyDict) ) :
    name(name), checkInputs(checkInputs) {}

  const std::string name;
  operator std::string() const { return name; }
  operator const char*() const { return name.c_str(); }

  void (*checkInputs)(PyDict);
};

//Operators so that a DictOption can be compared with a const char*
bool operator==( const char* val, const DictOption& p );
bool operator==( const DictOption& p, const char* val );

bool operator==( const std::string& val, const DictOption& p );
bool operator==( const DictOption& p, const std::string& val );

bool operator!=( const std::string& val, const DictOption& p );
bool operator!=( const DictOption& p, const std::string& val );


//----------------------------------------------------------------------------//
// A convenient parameter struct for classes without any parameters
struct ParamsNone : noncopyable
{
  static void checkInputs(PyDict d);
};


#if 0
//----------------------------------------------------------------------------//
//A generic macro for declaring parameters with options
#define PARAMETER_OPTION_DECL( NAME, OPTIONS, TYPE, OPTION_DECL ) \
struct BOOST_PP_CAT( NAME, Options )  \
{ \
  typedef TYPE ExtractType; \
  \
  BOOST_PP_REPEAT( BOOST_PP_SEQ_SIZE(OPTIONS), OPTION_DECL, OPTIONS ) \
  \
  static const int noptions = BOOST_PP_SEQ_SIZE(OPTIONS); \
  static const TYPE options[noptions]; \
  \
}; \
static const ParameterOption< BOOST_PP_CAT( NAME, Options ) > NAME;


//----------------------------------------------------------------------------//
//Use this to declare a string parameter with specific options
#define PARAMETER_OPTION_DEF( PARAM, NAME, DEFAULT, OPTIONS, HELP, TYPE, OPTION_DEF, OPTION_HELP_DEF ) \
BOOST_PP_REPEAT( BOOST_PP_SEQ_SIZE(OPTIONS), OPTION_DEF, (PARAM::BOOST_PP_CAT( NAME, Options ))(OPTIONS) ) \
  \
const TYPE PARAM::BOOST_PP_CAT( NAME, Options )::options[] = { \
  BOOST_PP_ENUM( BOOST_PP_SEQ_SIZE(OPTIONS), OPTION_HELP_DEF, OPTIONS ) \
  }; \
\
const ParameterOption< PARAM::BOOST_PP_CAT( NAME, Options ) > PARAM::NAME( BOOST_PP_STRINGIZE(NAME), DEFAULT, HELP );



//----------------------------------------------------------------------------//
//String option declaration
#define OPTION_STRING_DECL( z, n, OPTIONS ) static const std::string BOOST_PP_SEQ_ELEM(n, OPTIONS);

#define PARAMETER_STRING_OPTION_DECL( NAME, OPTIONS ) \
  PARAMETER_OPTION_DECL( NAME, OPTIONS, std::string, OPTION_STRING_DECL )

//String option definition
#define OPTION_STRING_DEF( z, n, OPTIONS ) const parameter_string_option \
BOOST_PP_SEQ_ELEM(0, OPTIONS)::BOOST_PP_SEQ_ELEM(n, BOOST_PP_SEQ_ELEM(1, OPTIONS) ) \
  ( BOOST_PP_STRINGIZE( BOOST_PP_SEQ_ELEM(n, BOOST_PP_SEQ_ELEM(1, OPTIONS) ) ) );

#define OPTION_STRING_HELP_DEF( z, n, OPTIONS ) BOOST_PP_STRINGIZE( BOOST_PP_SEQ_ELEM(n, OPTIONS) )

#define PARAMETER_STRING_OPTION_DEF( PARAM, NAME, DEFAULT, OPTIONS, HELP ) \
  PARAMETER_OPTION_DEF( PARAM, NAME, DEFAULT, OPTIONS, HELP, std::string, OPTION_STRING_DEF, OPTION_STRING_HELP_DEF )



//----------------------------------------------------------------------------//
//integer option declaration
#define OPTION_INT_DECL( z, n, OPTIONS ) static const int BOOST_PP_CAT( v, BOOST_PP_SEQ_ELEM(n, OPTIONS) ) = BOOST_PP_SEQ_ELEM(n, OPTIONS);

#define PARAMETER_INT_OPTION_DECL( NAME, OPTIONS ) \
  PARAMETER_OPTION_DECL( NAME, OPTIONS,int, OPTION_INT_DECL )

//integer option definition
#define OPTION_INT_DEF( z, n, OPTIONS )

#define OPTION_INT_HELP_DEF( z, n, OPTIONS ) BOOST_PP_SEQ_ELEM(n, OPTIONS)

#define PARAMETER_INT_OPTION_DEF( PARAM, NAME, DEFAULT, OPTIONS, HELP ) \
  PARAMETER_OPTION_DEF( PARAM, NAME, DEFAULT, OPTIONS, HELP, int, OPTION_INT_DEF, OPTION_INT_HELP_DEF )

//This is a help macro to specify a range of integers for the an integer parameter with options
#define SEQUENCE( z, n, _ ) (n)
#define INT_RANGE( FROM, TO ) BOOST_PP_REPEAT_FROM_TO( FROM, TO, SEQUENCE, _ )


//----------------------------------------------------------------------------//
#define DICT_OPTION(r, _, ELEM ) \
  const DictOption BOOST_PP_TUPLE_ELEM( 2, 0, ELEM ) = DictOption BOOST_PP_TUPLE_ELEM( 2, 1, ELEM );

#define DICT_VECTOR( z, n, OPTIONS ) BOOST_PP_TUPLE_ELEM( 2, 0, BOOST_PP_SEQ_ELEM(n, OPTIONS) )

//This is a macro for a parameter with specific options of dictionaries
#define PARAMETER_DICT_OPTION( PARAM, PARAM_ARGS, KEY, KEY_ARGS, OPTIONS ) \
struct BOOST_PP_CAT( PARAM, Options )  \
{ \
  typedef DictKeyPair ExtractType; \
  const ParameterString KEY = ParameterString KEY_ARGS; \
  const ParameterString& key = KEY; \
  \
  BOOST_PP_SEQ_FOR_EACH( DICT_OPTION, _, OPTIONS ) \
  \
  const std::vector<DictOption> options{ BOOST_PP_ENUM( BOOST_PP_SEQ_SIZE(OPTIONS), DICT_VECTOR, OPTIONS) }; \
}; \
const ParameterOption< BOOST_PP_CAT( PARAM, Options ) > PARAM = \
    ParameterOption< BOOST_PP_CAT( PARAM, Options ) > PARAM_ARGS;
#endif

} // namepsace SANS

/*
 * The OPTIONS argument to PARAMETER_DICT_OPTION is a sequence of tuples.
 * A sequence is defined as
 *
 * (a)(b)(c)
 *
 * whereas a tuple is
 *
 * (a, b, c)
 *
 * The tuples in OPTIONS are the variable name followed by the arguments to the variable ExtractType, i.e.
 *
 * (VAR1, ARGS1)
 *
 * The OPTIONS sequence of tuples is
 *
 * ( (VAR1, ARGS1) )( (VAR2, ARGS2) )( (VAR3, ARGS3) )
 *
 * Note that ARGS is a also a tuple. Hence, the OPTIONS will have the following pattern
 *
 * ( (VAR1, (arg11, arg12)) )( (VAR2, (arg21, arg22)) )( (VAR3, (arg31, arg32)) )
 *
 * The PARAM_ARGS and KEY_ARGS in PARAMETER_DICT_OPTION are also tuples of arguments.
 *
 * For more detauls on sequences and tuples see:
 * http://www.boost.org/doc/libs/1_55_0/libs/preprocessor/doc/index.html
 * http://www.boost.org/doc/libs/1_55_0/libs/preprocessor/doc/index.html
 *
 * Example usage of PARAMETER_DICT_OPTION
 *
 *  PARAMETER_DICT_OPTION( Viscosity, ("Viscosity", NO_DEFAULT, "Viscosity Dictionary"),
 *                       Equation, ("Equation", "Constant", "Viscosity Equation"),
 *                       ((Constant, ("Constant", ConstantViscosityParam::checkInputs)))
 *                       ((Sutherland, ("Sutherland", SutherlandParam::checkInputs))) );
 *
 * expands into
 *
 *  struct ViscosityOptions
 *  {
 *    typedef DictKeyPair ExtractType;
 *    const ParameterString Equation = ParameterString("Equation", "Constant", "Viscosity Equation" );
 *    const ParameterString& key = Equation;
 *
 *    const DictOption Constant   = DictOption("Constant", ConstantViscosityParam::checkInputs);
 *    const DictOption Sutherland = DictOption("Sutherland", SutherlandParam::checkInputs);
 *
 *    const std::vector<DictOption> options{Constant,Sutherland};
 *  };
 *  const ParameterOption<ViscosityOptions> Viscosity =
 *      ParameterOption<ViscosityOptions>("Viscosity", NO_DEFAULT, "Viscosity Dictionary");
 *
 */

#endif //PARAMETER_H
