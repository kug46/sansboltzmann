INCLUDE( ${CMAKE_SOURCE_DIR}/CMakeInclude/ForceOutOfSource.cmake ) #This must be the first thing included

SET( METRIC_SRC
  
  )

#ADD_LIBRARY( MetricLib STATIC ${METRIC_SRC} )

# This should be deleted and replaced with the library if cpp files are added
ADD_CUSTOM_TARGET( MetricLib ) 

#Create the vera targets for this library
ADD_VERA_CHECKS_RECURSE( MetricLib *.h *.cpp )
ADD_HEADER_COMPILE_CHECK_RECURSE( MetricLib *.h )
#ADD_CPPCHECK( MetricLib ${METRIC_SRC} )