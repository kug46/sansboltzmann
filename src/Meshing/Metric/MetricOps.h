// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef METRICOPS_H
#define METRICOPS_H

#include <cmath> // sqrt, log
#include <vector>

#include "tools/SANSnumerics.h"
#include "tools/smoothmath.h"

#include "Topology/Dimension.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Diag.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Trace.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Pow.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Log.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Exp.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_FrobNorm.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS_OuterProduct.h"

#include "LinearAlgebra/DenseLinAlg/InverseCholesky.h"

#include "Surreal/PromoteSurreal.h"

namespace SANS
{

struct OptResult
{
  OptResult() : converged(false), resid(0.0) {};
  OptResult(bool conv, Real res) : converged(conv), resid(res) {};

  bool converged;
  Real resid;
};

namespace Metric
{

//=============================================================================
// Returns the step matrix (logarithmic map) from metric M0 to M1
// S = log ( M0^(-1/2) * M1 * M0^(-1/2) )
template< int D, class T0, class T1 >
inline
DLA::MatrixSymS<D,typename promote_Surreal<T0,T1>::type>
computeStepMatrix(const DLA::MatrixSymS<D,T0>& M0, const DLA::MatrixSymS<D,T1>& M1)
{
  typedef typename promote_Surreal<T0,T1>::type T;

  DLA::MatrixSymS<D,T0> invsqrt_M0 = pow(M0, -0.5);
  DLA::MatrixSymS<D,T > S = invsqrt_M0*M1*invsqrt_M0;

  return log(S);
}

//=============================================================================
// Returns a step matrix that is rotated from the step matrix on the equilateral
// reference element (S_eq), according to the transformation given by U and VT.
// S = (V*U^T)^T * S_eq * (V*U^T)
template< int D, class T >
inline
DLA::MatrixSymS<D,T>
rotateRefStepMatrix(const DLA::MatrixSymS<D,T>& S_eq,
                    const DLA::MatrixS<D,D,T>& U,
                    const DLA::MatrixS<D,D,T>& VT)
{
  DLA::MatrixS<D,D,T> UVT = U * VT; // U*V^T
  DLA::MatrixSymS<D,T> S = UVT*S_eq*Transpose(UVT);

  return S;
}

//=============================================================================
// Returns a step matrix that is rotated from the step matrix on the equilateral
// reference element (S_eq), according to the transformation given by UVT.
// S = (V*U^T)^T * S_eq * (V*U^T)
// Same as function above, but for if UVT is stored pre-multiplied
template< int D, class T >
inline
DLA::MatrixSymS<D,T>
rotateRefStepMatrix(const DLA::MatrixSymS<D,T>& S_eq,
                    const DLA::MatrixS<D,D,T>& UVT)
{
  DLA::MatrixSymS<D,T> S = UVT*S_eq*Transpose(UVT);

  return S;
}

//=============================================================================
// Returns the exponential map of the step matrix S, about metric M0
template< int D, class T1, class T2 >
inline
DLA::MatrixSymS<D, typename promote_Surreal<T1,T2>::type>
computeExponentialMap(const DLA::MatrixSymS<D,T1>& M0, const DLA::MatrixSymS<D,T2>& S)
{
  typedef typename promote_Surreal<T1,T2>::type T;

  DLA::MatrixSymS<D,T1> sqrt_M0 = sqrt(M0);
  DLA::MatrixSymS<D,T2> expS = exp(S);
  DLA::MatrixSymS<D,T> M = sqrt_M0*expS*sqrt_M0;

  return M;
}

//=============================================================================
// Returns the affine invariant interpolation between metric M0 and M1, parameterized by t.
// M(t) = M0^(1/2) exp(t log(M0^(-1/2) M1 M0^(-1/2))) M0^(1/2)
template< int D, class T >
DLA::MatrixSymS<D,T>
computeAffineInvInterp(const DLA::MatrixSymS<D,T>& M0,
                       const DLA::MatrixSymS<D,T>& M1,
                       const Real t)
{
  //Compute scaled step matrix from M0 to M1
  DLA::MatrixSymS<D,T> St = t*computeStepMatrix(M0,M1);

  DLA::MatrixSymS<D,T> tmpM1 = exp(St);
  DLA::MatrixSymS<D,T> sqrt_M0 = pow(M0, 0.5);

  DLA::MatrixSymS<D,T> M = sqrt_M0*tmpM1*sqrt_M0;

  return M;
}

//=============================================================================
// Returns the log-Euclidean average of the metrics in Mvec[], weighted by the weights in wvec[]
template< int D, class T >
DLA::EigenSystemPair<D,T>
computeLogEuclideanAvg(const std::vector< DLA::MatrixSymS<D,T> >& Mvec,
                       const std::vector<Real>& wvec)
{
  SANS_ASSERT(Mvec.size() == wvec.size());
  std::size_t N = Mvec.size();

  DLA::MatrixSymS<D,T> weightedlogM = 0.0;

  for (std::size_t i = 0; i < N; i++)
    weightedlogM += wvec[i]*log(Mvec[i]);

  return exp(DLA::EigenSystemPair<D,T>(weightedlogM));
}

//=============================================================================
// Returns the log-Euclidean average of the metrics in Mvec[], with equal weighting.
template< int D, class T >
DLA::EigenSystemPair<D,T>
computeLogEuclideanAvg(const std::vector< DLA::MatrixSymS<D,T> >& Mvec)
{
  std::size_t N = Mvec.size();
  std::vector<Real> wvec(N, 1.0/((Real)N));

  return computeLogEuclideanAvg(Mvec, wvec);
}

template< int D, class T >
std::pair<DLA::MatrixSymS<D,T>,OptResult>
computeAffineInvAvgResult(const std::vector< DLA::MatrixSymS<D,T> >& Mvec, const std::vector<Real>& wvec )
{
  SANS_ASSERT(Mvec.size() == wvec.size());
  int N = (int) Mvec.size();

  OptResult opt;

  if (N == 1) // no need to average only 1
  {
    // std::cout<<std::endl;
    opt.converged=true;
    return std::pair<DLA::MatrixSymS<D,T>,OptResult>(Mvec[0],opt);
  }

  if (N == 2) //Can be computed directly if there are only 2 metrics to average
  {
    // std::cout<< std::endl;
    opt.converged=true;
    return std::pair<DLA::MatrixSymS<D,T>,OptResult>(computeAffineInvInterp(Mvec[0], Mvec[1], 0.5),opt);
  }

  const int nIterMax = 100; // Should take nowhere near this number most often
  const Real tol = 1e-10; // somewhat arbitrary, but should be much smaller than all the other noise
  Real S_frobnorm = 1.0; //Frobenius norm of step matrix

  //Set the initial guess to the log-Euclidean average
  DLA::MatrixSymS<D,T> Mavg = computeLogEuclideanAvg(Mvec, wvec);

  int iter = 0;

  // MatrixS<D,D,T> U,D,VT; // temporaries for storing decompositions
  DLA::MatrixSymS<D,T> sqrt_Mavg, invsqrt_Mavg, Savg, Stmp;
  while ( S_frobnorm > tol && iter < nIterMax )
  {
    iter++;
    sqrt_Mavg = pow(Mavg,0.5);
    invsqrt_Mavg = pow(Mavg,-0.5);

    Savg = 0;
    for (std::size_t i = 0; i < Mvec.size(); i++)
    {
      Stmp = invsqrt_Mavg*Mvec[i]*invsqrt_Mavg;
      Savg += wvec[i]*log( Stmp );
    }

    Mavg = sqrt_Mavg*exp(Savg)*sqrt_Mavg;
    S_frobnorm = DLA::FrobNorm(Savg);
  }
  opt.resid = S_frobnorm;

  if ( S_frobnorm <= tol )
  {
    // std::cout << std::scientific << "warning: unconverged AffineInvariant average, norm = "
    //           << S_frobnorm << ", tol = " << tol << std::fixed << std::endl;
    opt.converged = true;
  }

  return std::pair<DLA::MatrixSymS<D,T>,OptResult>(Mavg,opt);
}

//=============================================================================
// Returns the affine invariant average of the metrics in Mvec[], with equal weighting.
template< int D, class T >
DLA::MatrixSymS<D,T>
computeAffineInvAvg(const std::vector< DLA::MatrixSymS<D,T> >& Mvec)
{
  int N = (int) Mvec.size();
  std::vector<Real> wvec(N, 1.0/((Real)N));

  std::pair<DLA::MatrixSymS<D,T>,OptResult> Mpair = computeAffineInvAvgResult(Mvec,wvec);
  return Mpair.first;
}


//=============================================================================
// Returns the affine invariant average of the metrics in Mvec[], weighted by the weights in wvec[]
// Ignores the secondary argument and just forwards the metric
template< int D, class T >
inline
DLA::MatrixSymS<D,T>
computeAffineInvAvg(const std::vector< DLA::MatrixSymS<D,T> >& Mvec,
                    const std::vector<Real>& wvec )
{
  std::pair<DLA::MatrixSymS<D,T>,OptResult> Mpair = computeAffineInvAvgResult(Mvec,wvec);
  return Mpair.first;
}

//=============================================================================
// Returns the affine invariant average of the metrics in Mvec[], with equal weighting.
template< int D, class T >
std::pair<DLA::MatrixSymS<D,T>,OptResult>
computeAffineInvAvgResult(const std::vector< DLA::MatrixSymS<D,T> >& Mvec )
{
    int N = (int) Mvec.size();
    std::vector<Real> wvec(N, 1.0/((Real)N));
    return computeAffineInvAvgResult(Mvec,wvec);
}

//=============================================================================
// Compute the inverse of the edge length distribution tensor,
template< int D, class T>
inline DLA::MatrixSymS<D,T>
computeInverseLengthDistributionTensor( const std::vector< DLA::VectorS<D,T> >& edges )
{
  // need at least this many edges. Actually need this many unique edges, but that is costly to check
  SANS_ASSERT( edges.size() >= D );
  DLA::MatrixSymS<D,T> distTensor=0; // distribution tensor

  for ( const auto& edge : edges )
  {
    DLA::MatrixSymS<D,T> Mtemp = SANS::DLA::OuterProduct(edge);
    // std::cout<< "Mtemp = " << std::endl << Mtemp << std::endl;
    distTensor += SANS::DLA::OuterProduct(edge);
  }

  distTensor /= static_cast<int>(edges.size());


  // DLA::MatrixSymS<D,T> M = SANS::DLA::InverseCholesky::Inverse(distTensor);

  DLA::MatrixS<D,D,T> Mtemp = SANS::DLA::InverseCholesky::Inverse(distTensor);

  // std::cout << "Mtemp = " << std::endl << Mtemp << std::endl;

  DLA::MatrixSymS<D,T> M;
  for (int i = 0; i < D; i++)
    for (int j = i; j < D; j++)
      M(i,j) = Mtemp(i,j);

  M /= D;

  return M;
}

//=============================================================================
// Returns the intersection between two metrics based on simultaneous reduction
// (algorithm from Adrien Loseille and below references)
// F. Alauzet, Size gradation control of anisotropic meshes, Finite Elem. Anal. Des. (2009)
// P.J. Frey and P.-L. George. Mesh generation. Application to finite elements.
// ISTE Ltd and John Wiley & Sons, 2nd edition, 2008.
template< int D, class T >
DLA::MatrixSymS<D,T>
intersection(const DLA::MatrixSymS<D,T>& A, const DLA::MatrixSymS<D,T>& B)
{
  // Not using MatrixSymS pow function here to reduce the number of eigen system calculations
  DLA::VectorS<D,T> L, invsqrt_L, sqrt_L;
  DLA::MatrixS<D,D,T> E;

  //Compute the eigensystem with normalized eigenvectors
  DLA::EigenSystem( A, L, E );

  //Compute the power of the eigenvalues
  for (int i = 0; i < D; i++ )
  {
    invsqrt_L[i] = pow(L[i], -0.5);
       sqrt_L[i] = pow(L[i],  0.5);
  }

  //Compute the matrix powers
  DLA::MatrixSymS<D,T> invsqrt_A = E*DLA::diag(invsqrt_L)*Transpose(E);
  DLA::MatrixSymS<D,T>    sqrt_A = E*DLA::diag(   sqrt_L)*Transpose(E);

  // Compute a temporary metric for the intersection
  DLA::MatrixSymS<D,T> M = invsqrt_A*B*invsqrt_A;

  //Compute the eigensystem with normalized eigenvectors
  DLA::EigenSystem( M, L, E );

  //Compute the max of 1 and abs(L[i])
  for (int i = 0; i < D; i++ )
    L[i] = max(Real(1),fabs(L[i]));

  // Replace M with modified Eigen values
  M = E*DLA::diag(L)*Transpose(E);

  // Return the intersection
  return sqrt_A*M*sqrt_A;
}


//=============================================================================
// Smooth version of the above intersection algorithm
template< int D, class T >
DLA::MatrixSymS<D,T>
smoothintersection(const DLA::MatrixSymS<D,T>& A, const DLA::MatrixSymS<D,T>& B, const Real a)
{
  // Not using MatrixSymS pow function here to reduce the number of eigen system calculations
  SANS::DLA::VectorS<D,T> L, invsqrt_L, sqrt_L;
  SANS::DLA::MatrixS<D,D,T> E;

  //Compute the eigensystem with normalized eigenvectors
  SANS::DLA::EigenSystem( A, L, E );

  //Compute the power of the eigenvalues
  for (int i = 0; i < D; i++ )
  {
    invsqrt_L[i] = pow(L[i], -0.5);
       sqrt_L[i] = pow(L[i],  0.5);
  }

  //Compute the matrix powers
  DLA::MatrixSymS<D,T> invsqrt_A = E*DLA::diag(invsqrt_L)*Transpose(E);
  DLA::MatrixSymS<D,T>    sqrt_A = E*DLA::diag(   sqrt_L)*Transpose(E);

  // Compute a temporary metric for the intersection
  DLA::MatrixSymS<D,T> M = invsqrt_A*B*invsqrt_A;

  //Compute the eigensystem with normalized eigenvectors
  DLA::EigenSystem( M, L, E );

  //Compute the max of 1 and abs(L[i])
  for (int i = 0; i < D; i++ )
    L[i] = smoothmax(T(1),T(fabs(L[i])),a);

  // Replace M with modified Eigen values
  M = E*DLA::diag(L)*Transpose(E);

  // Return the intersection
  return sqrt_A*M*sqrt_A;
}

//=============================================================================
template< int D, class T >
DLA::MatrixSymS<D,T>
smoothintersection(const DLA::EigenSystemPair<D,T>& A, const DLA::MatrixSymS<D,T>& B, const Real a)
{
  // Not using MatrixSymS pow function here to reduce the number of eigen system calculations
  SANS::DLA::VectorS<D,T> L, invsqrt_L, sqrt_L;
  SANS::DLA::MatrixS<D,D,T> E;

  //Compute the power of the eigenvalues
  for (int i = 0; i < D; i++ )
  {
    invsqrt_L[i] = pow(A.L[i], -0.5);
       sqrt_L[i] = pow(A.L[i],  0.5);
  }

  //Compute the matrix powers
  DLA::MatrixSymS<D,T> invsqrt_A = A.E*DLA::diag(invsqrt_L)*Transpose(A.E);
  DLA::MatrixSymS<D,T>    sqrt_A = A.E*DLA::diag(   sqrt_L)*Transpose(A.E);

  // Compute a temporary metric for the intersection
  DLA::MatrixSymS<D,T> M = invsqrt_A*B*invsqrt_A;

  //Compute the eigensystem with normalized eigenvectors
  DLA::EigenSystem( M, L, E );

  //Compute the max of 1 and abs(L[i])
  for (int i = 0; i < D; i++ )
    L[i] = smoothmax(T(1),T(fabs(L[i])),a);

  // Replace M with modified Eigen values
  M = E*DLA::diag(L)*Transpose(E);

  // Return the intersection
  return sqrt_A*M*sqrt_A;
}


}
}

#endif //METRICOPS_H
