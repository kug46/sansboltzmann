// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD_LIBMESHB_H_
#define XFIELD_LIBMESHB_H_

#include "Topology/Dimension.h"

#include "Field/XField.h"

namespace SANS
{

// forward declare
template <class PhysDim>
class libMeshb_Reader;

template <class PhysDim, class TopoDim>
class XField_libMeshb : public XField<PhysDim,TopoDim>
{
public:
  XField_libMeshb(mpi::communicator comm, const std::string& filename);

protected:

  void readNodes(libMeshb_Reader<PhysDim>& reader, XField_Lagrange<PhysDim>& xfldin);
  void readElements(libMeshb_Reader<PhysDim>& reader, XField_Lagrange<PhysDim>& xfldin);
};

}

#endif /* XFIELD_LIBMESHB_H_ */
