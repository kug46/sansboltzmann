// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef WRITEMESH_LIBMESHB_H_
#define WRITEMESH_LIBMESHB_H_

#include "Field/XField.h"

namespace SANS
{

//Mesh version = 2 => 32-bit integers, 64-bit reals

template <class PhysDim, class TopoDim>
void WriteMesh_libMeshb(const XField<PhysDim,TopoDim>& xfld, const std::string& filename);

}

#endif /* WRITEMESH_LIBMESHB_H_ */
