// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "ReadSolution_libMeshb.h"

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "Topology/Dimension.h"
#include "Topology/ElementTopology.h"

#include "Field/XFieldArea.h"
#include "Field/XFieldVolume.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldVolume_CG_Cell.h"

#include "libMeshb_Interface.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_MPI
#include "MPI/serialize_DenseLinAlg_MatrixS.h"

#include <boost/mpi/collectives/all_reduce.hpp>
#include <boost/mpi/collectives/all_to_all.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#endif

namespace SANS
{

namespace
{
//---------------------------------------------------------------------------//
template<class T>
struct ReadSolAtVertices;

template<>
struct ReadSolAtVertices<Real>
{
  template<class PhysDim>
  static void gotoKeyword(libMeshb_Reader<PhysDim>& reader, const int nDOF )
  {
    int nSol, nTypes, nReals, Types[GmfMaxTyp];
    nSol = reader.getKeyword( GmfSolAtVertices, &nTypes, &nReals, Types );

    SANS_ASSERT_MSG( nDOF == nSol, "%d == %d", nDOF, nSol );
    SANS_ASSERT_MSG( nTypes == 1, "%d == 1", nTypes );
    SANS_ASSERT_MSG( nReals == 1, "%d == 1", nReals );
    SANS_ASSERT_MSG( Types[0] == GmfVec, "%d == GmfSca", Types[0] );

    reader.gotoKeyword( GmfSolAtVertices );
  }

  template<class PhysDim>
  static void getLine(libMeshb_Reader<PhysDim>& reader, const Real& q )
  {
    reader.getLine( GmfSolAtVertices, &q );
  }
};

template<>
struct ReadSolAtVertices<DLA::VectorS<1,Real>>
{
  typedef DLA::VectorS<1,Real> Vector;

  template<class PhysDim>
  static void gotoKeyword(libMeshb_Reader<PhysDim>& reader, const int nDOF )
  {
    int nSol, nTypes, nReals, Types[GmfMaxTyp];
    nSol = reader.getKeyword( GmfSolAtVertices, &nTypes, &nReals, Types );

    SANS_ASSERT_MSG( nDOF == nSol, "%d == %d", nDOF, nSol );
    SANS_ASSERT_MSG( nTypes == 1, "%d == 1", nTypes );
    SANS_ASSERT_MSG( nReals == 1, "%d == 1", nReals );
    SANS_ASSERT_MSG( Types[0] == GmfVec, "%d == GmfSca", Types[0] );

    reader.gotoKeyword( GmfSolAtVertices );
  }

  template<class PhysDim>
  static void getLine(libMeshb_Reader<PhysDim>& reader, const Vector& q )
  {
    reader.getLine( GmfSolAtVertices, &q[0] );
  }
};

template<>
struct ReadSolAtVertices<DLA::VectorS<2,Real>>
{
  typedef DLA::VectorS<2,Real> Vector;

  template<class PhysDim>
  static void gotoKeyword(libMeshb_Reader<PhysDim>& reader, const int nDOF )
  {
    int nSol, nTypes, nReals, Types[GmfMaxTyp];
    nSol = reader.getKeyword( GmfSolAtVertices, &nTypes, &nReals, Types );

    SANS_ASSERT_MSG( nDOF == nSol, "%d == %d", nDOF, nSol );
    SANS_ASSERT_MSG( nTypes == 1, "%d == 1", nTypes );
    SANS_ASSERT_MSG( nReals == 2, "%d == 2", nReals );
    SANS_ASSERT_MSG( Types[0] == GmfVec, "%d == GmfVec", Types[0] );

    reader.gotoKeyword( GmfSolAtVertices );
  }

  template<class PhysDim>
  static void getLine(libMeshb_Reader<PhysDim>& reader, const Vector& q )
  {
    reader.getLine( GmfSolAtVertices, &q[0], &q[1] );
  }
};

template<>
struct ReadSolAtVertices<DLA::VectorS<3,Real>>
{
  typedef DLA::VectorS<3,Real> Vector;

  template<class PhysDim>
  static void gotoKeyword(libMeshb_Reader<PhysDim>& reader, const int nDOF )
  {
    int nSol, nTypes, nReals, Types[GmfMaxTyp];
    nSol = reader.getKeyword( GmfSolAtVertices, &nTypes, &nReals, Types );

    SANS_ASSERT_MSG( nDOF == nSol, "%d == %d", nDOF, nSol );
    SANS_ASSERT_MSG( nTypes == 1, "%d == 1", nTypes );
    SANS_ASSERT_MSG( nReals == 3, "%d == 3", nReals );
    SANS_ASSERT_MSG( Types[0] == GmfVec, "%d == GmfVec", Types[0] );

    reader.gotoKeyword( GmfSolAtVertices );
  }

  template<class PhysDim>
  static void getLine(libMeshb_Reader<PhysDim>& reader, const Vector& q )
  {
    reader.getLine( GmfSolAtVertices, &q[0], &q[1], &q[2] );
  }
};

template<>
struct ReadSolAtVertices<DLA::VectorS<5,Real>>
{
  typedef DLA::VectorS<5,Real> Vector;

  template<class PhysDim>
  static void gotoKeyword(libMeshb_Reader<PhysDim>& reader, const int nDOF )
  {
    int nSol, nTypes, nReals, Types[GmfMaxTyp];
    nSol = reader.getKeyword( GmfSolAtVertices, &nTypes, &nReals, Types );

    SANS_ASSERT_MSG( nDOF == nSol, "%d == %d", nDOF, nSol );
    SANS_ASSERT_MSG( nTypes == 1, "%d == 1", nTypes );
    SANS_ASSERT_MSG( nReals == 6, "%d == 6", nReals );
    SANS_ASSERT_MSG( Types[0] == GmfSymMat, "%d == GmfSymMat", Types[0] );

    reader.gotoKeyword( GmfSolAtVertices );
  }

  template<class PhysDim>
  static void getLine(libMeshb_Reader<PhysDim>& reader, const Vector& q )
  {
    reader.getLine( GmfSolAtVertices, &q[0], &q[1], &q[2], &q[3], &q[4] );
  }
};

template<>
struct ReadSolAtVertices<DLA::VectorS<6,Real>>
{
  typedef DLA::VectorS<6,Real> Vector;

  template<class PhysDim>
  static void gotoKeyword(libMeshb_Reader<PhysDim>& reader, const int nDOF )
  {
    //using GmfSymMat is a hack to dump more than 3 DOF
    int nSol, nTypes, nReals, Types[GmfMaxTyp];
    nSol = reader.getKeyword( GmfSolAtVertices, &nTypes, &nReals, Types );

    SANS_ASSERT_MSG( nDOF == nSol, "%d == %d", nDOF, nSol );
    SANS_ASSERT_MSG( nTypes == 1, "%d == 1", nTypes );
    SANS_ASSERT_MSG( nReals == 6, "%d == 6", nReals );
    SANS_ASSERT_MSG( Types[0] == GmfSymMat, "%d == GmfSymMat", Types[0] );

    reader.gotoKeyword( GmfSolAtVertices );
  }

  template<class PhysDim>
  static void getLine(libMeshb_Reader<PhysDim>& reader, const Vector& q )
  {
    reader.getLine( GmfSolAtVertices, &q[0], &q[1], &q[2], &q[3], &q[4], &q[5] );
  }
};

template<>
struct ReadSolAtVertices<DLA::MatrixSymS<2,Real>>
{
  typedef DLA::MatrixSymS<2,Real> MatrixSym;

  template<class PhysDim>
  static void gotoKeyword(libMeshb_Reader<PhysDim>& reader, const int nDOF )
  {
    int nSol, nTypes, nReals, Types[GmfMaxTyp];
    nSol = reader.getKeyword( GmfSolAtVertices, &nTypes, &nReals, Types );

    SANS_ASSERT_MSG( nDOF == nSol, "%d == %d", nDOF, nSol );
    SANS_ASSERT_MSG( nTypes == 1, "%d == 1", nTypes );
    SANS_ASSERT_MSG( nReals == 3, "%d == 3", nReals );
    SANS_ASSERT_MSG( Types[0] == GmfSymMat, "%d == GmfSymMat", Types[0] );

    reader.gotoKeyword( GmfSolAtVertices );
  }

  template<class PhysDim>
  static void getLine(libMeshb_Reader<PhysDim>& reader, const MatrixSym& M )
  {
    reader.getLine( GmfSolAtVertices, &M(0,0),
                                      &M(1,0), &M(1,1) );
  }
};

template<>
struct ReadSolAtVertices<DLA::MatrixSymS<3,Real>>
{
  typedef DLA::MatrixSymS<3,Real> MatrixSym;

  template<class PhysDim>
  static void gotoKeyword(libMeshb_Reader<PhysDim>& reader, const int nDOF )
  {
    int nSol, nTypes, nReals, Types[GmfMaxTyp];
    nSol = reader.getKeyword( GmfSolAtVertices, &nTypes, &nReals, Types );

    SANS_ASSERT_MSG( nDOF == nSol, "%d == %d", nDOF, nSol );
    SANS_ASSERT_MSG( nTypes == 1, "%d == 1", nTypes );
    SANS_ASSERT_MSG( nReals == 6, "%d == 6", nReals );
    SANS_ASSERT_MSG( Types[0] == GmfSymMat, "%d == GmfSymMat", Types[0] );

    reader.gotoKeyword( GmfSolAtVertices );
  }

  template<class PhysDim>
  static void getLine(libMeshb_Reader<PhysDim>& reader, const MatrixSym& M )
  {
    reader.getLine( GmfSolAtVertices, &M(0,0),
                                      &M(1,0), &M(1,1),
                                      &M(2,0), &M(2,1), &M(2,2) );
  }
};

}

#define DOF_TAG         0

//---------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
void ReadSolution_libMeshb(Field_CG_Cell<PhysDim,TopoDim,T>& fld, const std::string& filename)
{
  int comm_rank = fld.comm()->rank();

  const int nCellGroup = fld.nCellGroups();

  //TODO : Currently only works with P1 solution fields
  for (int group = 0; group < nCellGroup; group++)
    SANS_ASSERT( fld.getCellGroupBase(group).order() == 1 );

  int nDOFnative = fld.nDOFnative();  //No. of nodes in linear mesh

  libMeshb_Reader<PhysDim> reader;

  if (comm_rank == 0)
  {
    reader.open(filename);

    //Read out solution keyword
    ReadSolAtVertices<T>::gotoKeyword(reader, nDOFnative);
  }

#ifdef SANS_MPI
  int comm_size = fld.comm()->size();

  // Compute the number of DOFs on each processor
  int DOFperProc = nDOFnative / comm_size;

  std::map<int,T> DOF;

  if (fld.comm()->rank() == 0)
  {
    // Make sure the buffer is large enough for the last processor
    std::map<int,T> DOF_buffer;
    T dof = 0;

    for (int idof = 0; idof < nDOFnative; idof++)
    {
      // Compute the rank that should receive the DOF
      int rank = MIN(comm_size-1, idof / DOFperProc);

      ReadSolAtVertices<T>::getLine(reader, dof);

      if (rank == 0)
      {
        // Save the DOF
        DOF[idof] = dof;
      }
      else
      {
        // Cache the DOF to be sent
        DOF_buffer[idof] = dof;
      }

      if (idof+1 == (rank+1)*DOFperProc && rank > 0 && rank < comm_size-1)
      {
        // send the DOFs to the appropriate rank
        fld.comm()->send(rank, DOF_TAG, DOF_buffer);
        DOF_buffer.clear();
      }
      else if (idof+1 == nDOFnative)
      {
        // send the DOFs to the last rank if needed
        if (rank > 0)
          fld.comm()->send(rank, DOF_TAG, DOF_buffer);
      }
    }
  }
  else
  {
    // All other ranks will receive DOFs sent from rank 0
    fld.comm()->recv(0, DOF_TAG, DOF);
  }

  std::vector<std::vector<int>> needDOF(comm_size);

  for (int i = 0; i < fld.nDOF(); i++)
  {
    int idof = fld.local2nativeDOFmap(i);

    // Compute the rank that should receive the DOF
    int rank = MIN(comm_size-1, idof / DOFperProc);

    needDOF[rank].push_back(idof);
  }

  // send the requested DOFs to processors where they reside
  std::vector<std::vector<int>> sendDOF;
  boost::mpi::all_to_all(*fld.comm(), needDOF, sendDOF);
  needDOF.clear();

  // gather the DOFs to send to other processors
  std::vector<std::map<int,T>> sendBuffer(comm_size);
  for (std::size_t rank = 0; rank < sendDOF.size(); rank++)
    for (const int& idof : sendDOF[rank])
      sendBuffer[rank][idof] = DOF.at(idof);

  // done with the dofs now
  DOF.clear();

  // send the DOFs to the processors that need them
  std::vector<std::map<int,T>> recvBuffer(comm_size);
  boost::mpi::all_to_all(*fld.comm(), sendBuffer, recvBuffer);
  sendBuffer.clear();

  // collapse down the dofs from all processors
  std::map<int,T> buffer;
  for (std::size_t rank = 0; rank < recvBuffer.size(); rank++)
    buffer.insert(recvBuffer[rank].begin(), recvBuffer[rank].end());

  recvBuffer.clear();

  for (int i = 0; i < fld.nDOF(); i++)
  {
    int idof = fld.local2nativeDOFmap(i);
    fld.DOF(i) = buffer.at(idof);
  }

#else
  //Read solution
  for (int k = 0; k < nDOFnative; k++)
    ReadSolAtVertices<T>::getLine(reader, fld.DOF(k));
#endif
}

//------------------------------------------------
//Explicit instantiations

typedef DLA::VectorS<1,Real> Vector1;
typedef DLA::VectorS<2,Real> Vector2;
typedef DLA::VectorS<3,Real> Vector3;
typedef DLA::VectorS<5,Real> Vector5;
typedef DLA::VectorS<6,Real> Vector6;

typedef DLA::MatrixSymS<2,Real> MatrixSym2;
typedef DLA::MatrixSymS<3,Real> MatrixSym3;

//2D - scalar field
template void ReadSolution_libMeshb<PhysD2, TopoD2, Real   >(Field_CG_Cell<PhysD2,TopoD2,Real   >& fld, const std::string& filename);
template void ReadSolution_libMeshb<PhysD2, TopoD2, Vector1>(Field_CG_Cell<PhysD2,TopoD2,Vector1>& fld, const std::string& filename);

//2D - vector field
template void ReadSolution_libMeshb<PhysD2, TopoD2, Vector2>(Field_CG_Cell<PhysD2,TopoD2,Vector2>& fld, const std::string& filename);

//2D - symmetric matrix field
template void ReadSolution_libMeshb<PhysD2, TopoD2, MatrixSym2>(Field_CG_Cell<PhysD2,TopoD2,MatrixSym2>& fld, const std::string& filename);

//3D - scalar field
template void ReadSolution_libMeshb<PhysD3, TopoD3, Real   >(Field_CG_Cell<PhysD3,TopoD3,Real   >& fld, const std::string& filename);
template void ReadSolution_libMeshb<PhysD3, TopoD3, Vector1>(Field_CG_Cell<PhysD3,TopoD3,Vector1>& fld, const std::string& filename);

//3D - vector field
template void ReadSolution_libMeshb<PhysD3, TopoD3, Vector3>(Field_CG_Cell<PhysD3,TopoD3,Vector3>& fld, const std::string& filename);
template void ReadSolution_libMeshb<PhysD3, TopoD3, Vector5>(Field_CG_Cell<PhysD3,TopoD3,Vector5>& fld, const std::string& filename);
template void ReadSolution_libMeshb<PhysD3, TopoD3, Vector6>(Field_CG_Cell<PhysD3,TopoD3,Vector6>& fld, const std::string& filename);

//3D - symmetric matrix field
template void ReadSolution_libMeshb<PhysD3, TopoD3, MatrixSym3>(Field_CG_Cell<PhysD3,TopoD3,MatrixSym3>& fld, const std::string& filename);

}
