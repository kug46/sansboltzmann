// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef READSOLUTION_LIBMESHB_H_
#define READSOLUTION_LIBMESHB_H_

#include "Field/Field.h"

namespace SANS
{

// solution field
template <class PhysDim, class TopoDim, class T>
void ReadSolution_libMeshb(Field_CG_Cell<PhysDim,TopoDim,T>& fld, const std::string& filename);

}

#endif /* READSOLUTION_LIBMESHB_H_ */
