// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <vector>

#include "XField_libMeshb.h"

#include "Topology/ElementTopology.h"

#include "Field/XFieldVolume.h"
#include "Field/Partition/XField_Lagrange.h"

#include "libMeshb_Interface.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

//---------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
XField_libMeshb<PhysDim, TopoDim>::
XField_libMeshb( mpi::communicator comm, const std::string& filename)
{
  libMeshb_Reader<PhysDim> reader;
  XField_Lagrange<PhysDim> xfldin(comm);

  // only rank 0 will read the grid
  if (comm.rank() == 0)
    reader.open(filename);

  // Read in the grid coordinates
  readNodes(reader, xfldin);

  // Read the element groups
  readElements(reader, xfldin);

  // Connect all the elements
  this->buildFrom( xfldin );
}

//---------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
void
XField_libMeshb<PhysDim, TopoDim>::
readNodes(libMeshb_Reader<PhysDim>& reader, XField_Lagrange<PhysDim>& xfldin)
{
  std::shared_ptr<mpi::communicator> comm = xfldin.comm();

  typename XField_Lagrange<PhysDim>::VectorX X;

  int nVertex = 0;

  // only rank 0 will read the grid
  if (comm->rank() == 0)
  {
    nVertex = reader.getKeyword( GmfVertices );

    SANS_ASSERT_MSG( nVertex > 0, "ReadMesh<PhysDim, TopoDim, FeFloa>::readNodes - No nodes in mesh file!" );
  }

  // start the process for reading points on all processors
  xfldin.sizeDOF( nVertex );

  if (comm->rank() == 0)
  {
    int reference;

    //Read in node coordinates
    reader.gotoKeyword( GmfVertices );
    for (int i = 0; i < nVertex; i++)
    {
      reader.getLine( GmfVertices, &X[0], &X[1], &X[2], &reference );
      xfldin.addDOF(X);
    }
  }
}

//---------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
void
XField_libMeshb<PhysDim, TopoDim>::
readElements(libMeshb_Reader<PhysDim>& reader, XField_Lagrange<PhysDim>& xfldin)
{
  std::shared_ptr<mpi::communicator> comm = xfldin.comm();

  //Read in tetrahedra (if any)
  int nTets = 0;
  int nHexes = 0;
  int nCellTotal = 0;
  if (comm->rank() == 0)
  {
    nTets  = reader.getKeyword( GmfTetrahedra );
    nHexes = reader.getKeyword( GmfHexahedra );

    SANS_ASSERT_MSG(nTets == 0 || nHexes == 0, "Can't handle mixed element meshes");

    // total number of cells in the mesh
    nCellTotal = nTets + nHexes;
  }

  // Start the process of adding cells on all processors
  xfldin.sizeCells(nCellTotal);

  if (comm->rank() == 0)
  {
    int cellgroup;
    int order = 1;
    if (nTets > 0)
    {
      reader.gotoKeyword( GmfTetrahedra );
      std::vector<int> tetNodes(Tet::NNode);

      for (int i = 0; i < nTets; i++)
      {
        reader.getLine( GmfTetrahedra,
                        &tetNodes[0], &tetNodes[1], &tetNodes[2], &tetNodes[3],
                        &cellgroup );

        for (int j = 0; j < Tet::NNode; j++)
          tetNodes[j]--; //transform to 0-based indexing

        //TODO: Uncomment after FeFloa reference number bug is fixed
        //cellgroup--; //transform to 0-based indexing

        // set the indices for this element
        xfldin.addCell(cellgroup, eTet, order, tetNodes);
      }
    }

    if (nHexes > 0)
    {
      reader.gotoKeyword( GmfHexahedra );
      std::vector<int> hexNodes(Hex::NNode);

      for (int i = 0; i < nHexes; i++)
      {
        reader.getLine( GmfHexahedra,
                        &hexNodes[0], &hexNodes[1], &hexNodes[2], &hexNodes[3],
                        &hexNodes[4], &hexNodes[5], &hexNodes[6], &hexNodes[7],
                        &cellgroup );

        for (int j = 0; j < Hex::NNode; j++)
          hexNodes[j]--; //transform to 0-based indexing

        cellgroup--; //transform to 0-based indexing

        // set the indices for this element
        xfldin.addCell(cellgroup, eHex, order, hexNodes);
      }
    }
  }

  //=======================
  //
  // Boundary trace groups
  //
  //=======================

  //Read in triangles (if any)
  int nTriangles = 0;
  int nQuads = 0;
  int nBoundaryTraceTotal = 0;
  if (comm->rank() == 0)
  {
    nTriangles = reader.getKeyword( GmfTriangles );
    nQuads     = reader.getKeyword( GmfQuadrilaterals );

    nBoundaryTraceTotal = nTriangles + nQuads;
  }

  // Start the process of boundary trace elements
  xfldin.sizeBoundaryTrace(nBoundaryTraceTotal);


  if (comm->rank() == 0)
  {
    int btracegroup;
    if (nTriangles > 0)
    {
      reader.gotoKeyword( GmfTriangles );
      std::vector<int> triangleNodes(Triangle::NNode);

      for (int i = 0; i < nTriangles; i++)
      {
        reader.getLine( GmfTriangles,
                        &triangleNodes[0], &triangleNodes[1], &triangleNodes[2],
                        &btracegroup );

        for (int j = 0; j < Triangle::NNode; j++)
          triangleNodes[j]--; //transform to 0-based indexing

        btracegroup--; //transform to 0-based indexing

        // set the indices for this element
        xfldin.addBoundaryTrace(btracegroup, eTriangle, triangleNodes);
      }
    }

    if (nQuads > 0)
    {
      reader.gotoKeyword( GmfQuadrilaterals );
      std::vector<int> quadNodes(Quad::NNode);

      for (int i = 0; i < nQuads; i++)
      {
        reader.getLine( GmfQuadrilaterals,
                        &quadNodes[0], &quadNodes[1], &quadNodes[2], &quadNodes[3],
                        &btracegroup );

        for (int j = 0; j < Quad::NNode; j++)
          quadNodes[j]--; //transform to 0-based indexing

        btracegroup--; //transform to 0-based indexing

        // set the indices for this element
        xfldin.addBoundaryTrace(btracegroup, eQuad, quadNodes);
      }
    }
  }
}

// explicit instantiation
template class XField_libMeshb<PhysD3, TopoD3>;

}
