// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "WriteSolution_libMeshb.h"

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "Topology/Dimension.h"
#include "Topology/ElementTopology.h"

#include "Field/XFieldArea.h"
#include "Field/XFieldVolume.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldVolume_CG_Cell.h"

#include "libMeshb_Interface.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_MPI
#include "MPI/serialize_DenseLinAlg_MatrixS.h"

#include <boost/mpi/collectives/all_reduce.hpp>
#include <boost/mpi/collectives/reduce.hpp>
#include <boost/mpi/collectives/gather.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#endif

namespace SANS
{

namespace
{
//---------------------------------------------------------------------------//
template<class T>
struct WriteSolAtVertices;

template<>
struct WriteSolAtVertices<Real>
{
  template<class PhysDim>
  static void writeKeyword(libMeshb_Writer<PhysDim>& writer, const int nDOF )
  {
    int solution_type = GmfSca;
    writer.setKeyword( GmfSolAtVertices, nDOF, 1, &solution_type );
  }

  template<class PhysDim>
  static void writeLine(libMeshb_Writer<PhysDim>& writer, const Real& q )
  {
    writer.setLine( GmfSolAtVertices, &q );
  }
};

template<>
struct WriteSolAtVertices<DLA::VectorS<1,Real>>
{
  typedef DLA::VectorS<1,Real> Vector;

  template<class PhysDim>
  static void writeKeyword(libMeshb_Writer<PhysDim>& writer, const int nDOF )
  {
    int solution_type = GmfSca;
    writer.setKeyword( GmfSolAtVertices, nDOF, 1, &solution_type );
  }

  template<class PhysDim>
  static void writeLine(libMeshb_Writer<PhysDim>& writer, const Vector& q )
  {
    writer.setLine( GmfSolAtVertices, &q[0] );
  }
};

template<>
struct WriteSolAtVertices<DLA::VectorS<2,Real>>
{
  typedef DLA::VectorS<2,Real> Vector;

  template<class PhysDim>
  static void writeKeyword(libMeshb_Writer<PhysDim>& writer, const int nDOF )
  {
    int solution_type = GmfVec;
    writer.setKeyword( GmfSolAtVertices, nDOF, 1, &solution_type );
  }

  template<class PhysDim>
  static void writeLine(libMeshb_Writer<PhysDim>& writer, const Vector& q )
  {
    writer.setLine( GmfSolAtVertices, &q[0], &q[1] );
  }
};

template<>
struct WriteSolAtVertices<DLA::VectorS<3,Real>>
{
  typedef DLA::VectorS<3,Real> Vector;

  template<class PhysDim>
  static void writeKeyword(libMeshb_Writer<PhysDim>& writer, const int nDOF )
  {
    int solution_type = GmfVec;
    writer.setKeyword( GmfSolAtVertices, nDOF, 1, &solution_type );
  }

  template<class PhysDim>
  static void writeLine(libMeshb_Writer<PhysDim>& writer, const Vector& q )
  {
    writer.setLine( GmfSolAtVertices, &q[0], &q[1], &q[2] );
  }
};

template<>
struct WriteSolAtVertices<DLA::VectorS<5,Real>>
{
  typedef DLA::VectorS<5,Real> Vector;

  template<class PhysDim>
  static void writeKeyword(libMeshb_Writer<PhysDim>& writer, const int nDOF )
  {
    //using GmfSymMat is a hack to dump more than 3 DOF
    int solution_type = GmfSymMat;
    writer.setKeyword( GmfSolAtVertices, nDOF, 1, &solution_type );
  }

  template<class PhysDim>
  static void writeLine(libMeshb_Writer<PhysDim>& writer, const Vector& q )
  {
    writer.setLine( GmfSolAtVertices, &q[0], &q[1], &q[2], &q[3], &q[4] );
  }
};


template<>
struct WriteSolAtVertices<DLA::VectorS<6,Real>>
{
  typedef DLA::VectorS<6,Real> Vector;

  template<class PhysDim>
  static void writeKeyword(libMeshb_Writer<PhysDim>& writer, const int nDOF )
  {
    int solution_type = GmfSymMat;
    writer.setKeyword( GmfSolAtVertices, nDOF, 1, &solution_type );
  }

  template<class PhysDim>
  static void writeLine(libMeshb_Writer<PhysDim>& writer, const Vector& q )
  {
    writer.setLine( GmfSolAtVertices, &q[0], &q[1], &q[2], &q[3], &q[4], &q[5] );
  }
};

template<>
struct WriteSolAtVertices<DLA::MatrixSymS<2,Real>>
{
  typedef DLA::MatrixSymS<2,Real> MatrixSym;

  template<class PhysDim>
  static void writeKeyword(libMeshb_Writer<PhysDim>& writer, const int nDOF )
  {
    int solution_type = GmfSymMat;
    writer.setKeyword( GmfSolAtVertices, nDOF, 1, &solution_type );
  }

  template<class PhysDim>
  static void writeLine(libMeshb_Writer<PhysDim>& writer, const MatrixSym& M )
  {
    writer.setLine( GmfSolAtVertices, &M(0,0),
                                      &M(1,0), &M(1,1) );
  }
};

template<>
struct WriteSolAtVertices<DLA::MatrixSymS<3,Real>>
{
  typedef DLA::MatrixSymS<3,Real> MatrixSym;

  template<class PhysDim>
  static void writeKeyword(libMeshb_Writer<PhysDim>& writer, const int nDOF )
  {
    int solution_type = GmfSymMat;
    writer.setKeyword( GmfSolAtVertices, nDOF, 1, &solution_type );
  }

  template<class PhysDim>
  static void writeLine(libMeshb_Writer<PhysDim>& writer, const MatrixSym& M )
  {
    writer.setLine( GmfSolAtVertices, &M(0,0),
                                      &M(1,0), &M(1,1),
                                      &M(2,0), &M(2,1), &M(2,2) );
  }
};

}

//---------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
void WriteSolution_libMeshb(const Field_CG_Cell<PhysDim,TopoDim,T>& fld, const std::string& filename)
{
  int comm_rank = fld.comm()->rank();

  const int nCellGroup = fld.nCellGroups();

  //TODO : Currently only works with P1 solution fields
  for (int group = 0; group < nCellGroup; group++)
    SANS_ASSERT( fld.getCellGroupBase(group).order() == 1 );

  int nDOFnative = fld.nDOFnative();  //No. of nodes in exported linear mesh

  libMeshb_Writer<PhysDim> writer;

  if (comm_rank == 0)
  {
    writer.open(filename);

    //Write out solution keyword
    WriteSolAtVertices<T>::writeKeyword(writer, nDOFnative);
  }

#ifdef SANS_MPI
  int comm_size = fld.comm()->size();

  // maximum DOF chunk size that rank 0 will write at any given time
  int DOFchunk = nDOFnative / comm_size + nDOFnative % comm_size;

  for (int rank = 0; rank < comm_size; rank++)
  {
    int DOFlow = rank*DOFchunk;
    int DOFhigh = (rank+1)*DOFchunk;

    std::map<int,T> buffer;

    for (int i = 0; i < fld.nDOFpossessed(); i++)
    {
      int iDOF = fld.local2nativeDOFmap(i);
      if (iDOF >= DOFlow && iDOF < DOFhigh)
        buffer[iDOF] = fld.DOF(i);
    }

    if (comm_rank == 0)
    {
      std::vector<std::map<int,T>> bufferOnRank;
      boost::mpi::gather(*fld.comm(), buffer, bufferOnRank, 0 );

      // collapse down the buffer from all ranks (this sorts and remove any possible duplicates)
      for (std::size_t i = 0; i < bufferOnRank.size(); i++)
        buffer.insert(bufferOnRank[i].begin(), bufferOnRank[i].end());

      //Write the node data
      for ( const auto& DOFpair : buffer )
        WriteSolAtVertices<T>::writeLine(writer, DOFpair.second);
    }
#ifndef __clang_analyzer__
    else // send the buffer to rank 0
      boost::mpi::gather(*fld.comm(), buffer, 0 );
#endif
  } // for rank

#else
  //Write solution
  for (int k = 0; k < nDOFnative; k++)
    WriteSolAtVertices<T>::writeLine(writer, fld.DOF(k));
#endif
}


//---------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
void WriteSolution_libMeshb(const Field_DG_Cell<PhysDim,TopoDim,T>& fld, const std::string& filename)
{
  int comm_rank = fld.comm()->rank();

  const int nCellGroup = fld.nCellGroups();

  //TODO : Currently only works with P1 solution fields
  for (int group = 0; group < nCellGroup; group++)
    SANS_ASSERT( fld.getCellGroupBase(group).order() == 1 );

  int nDOFnative = fld.nDOFnative();  //No. of nodes in exported linear mesh

  libMeshb_Writer<PhysDim> writer;

  if (comm_rank == 0)
  {
    writer.open(filename);

    //Write out solution keyword
    WriteSolAtVertices<T>::writeKeyword(writer, nDOFnative);
  }

#ifdef SANS_MPI
  int comm_size = fld.comm()->size();

  // maximum DOF chunk size that rank 0 will write at any given time
  int DOFchunk = nDOFnative / comm_size + nDOFnative % comm_size;

  for (int rank = 0; rank < comm_size; rank++)
  {
    int DOFlow = rank*DOFchunk;
    int DOFhigh = (rank+1)*DOFchunk;

    std::map<int,T> buffer;

    for (int i = 0; i < fld.nDOFpossessed(); i++)
    {
      int iDOF = fld.local2nativeDOFmap(i);
      if (iDOF >= DOFlow && iDOF < DOFhigh)
        buffer[iDOF] = fld.DOF(i);
    }

    if (comm_rank == 0)
    {
      std::vector<std::map<int,T>> bufferOnRank;
      boost::mpi::gather(*fld.comm(), buffer, bufferOnRank, 0 );

      // collapse down the buffer from all ranks (this sorts and remove any possible duplicates)
      for (std::size_t i = 0; i < bufferOnRank.size(); i++)
        buffer.insert(bufferOnRank[i].begin(), bufferOnRank[i].end());

      //Write the node data
      for ( const auto& DOFpair : buffer )
        WriteSolAtVertices<T>::writeLine(writer, DOFpair.second);
    }
#ifndef __clang_analyzer__
    else // send the buffer to rank 0
      boost::mpi::gather(*fld.comm(), buffer, 0 );
#endif
  } // for rank

#else
  //Write solution
  for (int k = 0; k < nDOFnative; k++)
    WriteSolAtVertices<T>::writeLine(writer, fld.DOF(k));
#endif
}


//---------------------------------------------------------------------------//
typedef DLA::MatrixSymS<4,Real> MatrixSym4;
template <>
void WriteSolution_libMeshb<PhysD4,TopoD4,MatrixSym4>(const Field_CG_Cell<PhysD4,TopoD4,MatrixSym4>& fld, const std::string& filename)
{
  SANS_DEVELOPER_EXCEPTION("libMeshb does not support 4D solution files.");
}

//------------------------------------------------
//Explicit instantiations

typedef DLA::VectorS<1,Real> Vector1;
typedef DLA::VectorS<2,Real> Vector2;
typedef DLA::VectorS<3,Real> Vector3;
typedef DLA::VectorS<5,Real> Vector5;
typedef DLA::VectorS<6,Real> Vector6;

typedef DLA::MatrixSymS<2,Real> MatrixSym2;
typedef DLA::MatrixSymS<3,Real> MatrixSym3;

//2D - scalar field
template void WriteSolution_libMeshb<PhysD2, TopoD2, Real   >(const Field_CG_Cell<PhysD2,TopoD2,Real   >& fld, const std::string& filename);
template void WriteSolution_libMeshb<PhysD2, TopoD2, Vector1>(const Field_CG_Cell<PhysD2,TopoD2,Vector1>& fld, const std::string& filename);

//2D - vector field
template void WriteSolution_libMeshb<PhysD2, TopoD2, Vector2>(const Field_CG_Cell<PhysD2,TopoD2,Vector2>& fld, const std::string& filename);

//2D - symmetric matrix field
template void WriteSolution_libMeshb<PhysD2, TopoD2, MatrixSym2>(const Field_CG_Cell<PhysD2,TopoD2,MatrixSym2>& fld, const std::string& filename);

//3D - scalar field
template void WriteSolution_libMeshb<PhysD3, TopoD3, Real   >(const Field_CG_Cell<PhysD3,TopoD3,Real   >& fld, const std::string& filename);
template void WriteSolution_libMeshb<PhysD3, TopoD3, Vector1>(const Field_CG_Cell<PhysD3,TopoD3,Vector1>& fld, const std::string& filename);

//3D - vector field
template void WriteSolution_libMeshb<PhysD3, TopoD3, Vector3>(const Field_CG_Cell<PhysD3,TopoD3,Vector3>& fld, const std::string& filename);
template void WriteSolution_libMeshb<PhysD3, TopoD3, Vector5>(const Field_CG_Cell<PhysD3,TopoD3,Vector5>& fld, const std::string& filename);
template void WriteSolution_libMeshb<PhysD3, TopoD3, Vector6>(const Field_CG_Cell<PhysD3,TopoD3,Vector6>& fld, const std::string& filename);

//3D - symmetric matrix field
template void WriteSolution_libMeshb<PhysD3, TopoD3, MatrixSym3>(const Field_CG_Cell<PhysD3,TopoD3,MatrixSym3>& fld, const std::string& filename);

}
