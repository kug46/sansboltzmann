// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "WriteMesh_libMeshb.h"

#include <boost/version.hpp>

#include <map>
#include <vector>
#include <ostream>

#include "tools/output_std_vector.h"

#include "tools/SANSnumerics.h"     // Real
#include "Topology/Dimension.h"
#include "Topology/ElementTopology.h"

#include "Field/XFieldArea.h"
#include "Field/XFieldVolume.h"

#include "Field/tools/for_each_CellGroup.h"
#include "Field/tools/for_each_BoundaryTraceGroup.h"

#include "libMeshb_Interface.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_MPI
#include "tools/plus_std_vector.h"
#include "MPI/serialize_DenseLinAlg_MatrixS.h"

#include <boost/mpi/collectives/all_reduce.hpp>
#include <boost/mpi/collectives/gather.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#endif

namespace SANS
{

namespace //private to this file
{
template<class Topology>
struct WriteElement;

template<>
struct WriteElement<Line>
{
  template<class PhysDim>
  static void write(libMeshb_Writer<PhysDim>& writer, const std::vector<int>& nodeMap)
  {
    SANS_ASSERT_MSG(nodeMap.size() == 3, "nodeMap.size() = %d", nodeMap.size());
    writer.setLine( GmfEdges,
                    nodeMap[0], nodeMap[1],
                    nodeMap[2] ); // group
  }
};

template<>
struct WriteElement<Triangle>
{
  template<class PhysDim>
  static void write(libMeshb_Writer<PhysDim>& writer, const std::vector<int>& nodeMap)
  {
    SANS_ASSERT_MSG(nodeMap.size() == 4, "nodeMap.size() = %d", nodeMap.size());
    writer.setLine( GmfTriangles,
                    nodeMap[0], nodeMap[1], nodeMap[2],
                    nodeMap[3] ); // group
  }
};

template<>
struct WriteElement<Quad>
{
  template<class PhysDim>
  static void write(libMeshb_Writer<PhysDim>& writer, const std::vector<int>& nodeMap)
  {
    SANS_ASSERT_MSG(nodeMap.size() == 5, "nodeMap.size() = %d", nodeMap.size());
    writer.setLine( GmfQuadrilaterals,
                    nodeMap[0], nodeMap[1], nodeMap[2], nodeMap[3],
                    nodeMap[4] ); // group
  }
};

template<>
struct WriteElement<Tet>
{
  template<class PhysDim>
  static void write(libMeshb_Writer<PhysDim>& writer, const std::vector<int>& nodeMap)
  {
    SANS_ASSERT_MSG(nodeMap.size() == 5, "nodeMap.size() = %d", nodeMap.size());
    writer.setLine( GmfTetrahedra,
                    nodeMap[0], nodeMap[1], nodeMap[2], nodeMap[3],
                    nodeMap[4] ); // group
  }
};

template<>
struct WriteElement<Hex>
{
  template<class PhysDim>
  static void write(libMeshb_Writer<PhysDim>& writer, const std::vector<int>& nodeMap)
  {
    SANS_ASSERT_MSG(nodeMap.size() == 9, "nodeMap.size() = %d", nodeMap.size());
    writer.setLine( GmfHexahedra,
                    nodeMap[0], nodeMap[1], nodeMap[2], nodeMap[3],
                    nodeMap[4], nodeMap[5], nodeMap[6], nodeMap[7],
                    nodeMap[8] ); // group
  }
};

//---------------------------------------------------------------------------//
template <class PhysDim>
class countCellElements : public GroupFunctorCellType< countCellElements<PhysDim> >
{
public:
  countCellElements( std::vector<int>& cellCount, int comm_rank, const int nCellGroups )
    : cellCount_(cellCount), comm_rank_(comm_rank), nCellGroups_(nCellGroups) {}

  std::size_t nCellGroups() const          { return nCellGroups_; }
  std::size_t cellGroup(const int n) const { return n;            }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology >
  void
  apply(const typename XField<PhysDim,typename Topology::TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
        const int cellGroupGlobal)
  {
    // number of elements on the local processor
    int nElem = xfldCellGroup.nElem();

    int topo_index;
         if ( typeid(Topology) == typeid(Triangle) || typeid(Topology) == typeid(Tet)) topo_index = 0;
    else if ( typeid(Topology) == typeid(Quad)     || typeid(Topology) == typeid(Hex)) topo_index = 1;
    else SANS_DEVELOPER_EXCEPTION( "Unknown cell topology." );

    // loop over cells within group
    for (int elem = 0; elem < nElem; elem++)
      if ( xfldCellGroup.associativity(elem).rank() == comm_rank_ )
        cellCount_[topo_index]++;
  }

protected:
  std::vector<int>& cellCount_;
  const int comm_rank_;
  const int nCellGroups_;
};

//---------------------------------------------------------------------------//
template <class PhysDim>
class countBoundaryTraceElements : public GroupFunctorBoundaryTraceType< countBoundaryTraceElements<PhysDim> >
{
public:
  countBoundaryTraceElements( std::vector<int>& bndCount, int comm_rank, const int nBTraceGroups )
    : bndCount_(bndCount), comm_rank_(comm_rank), nBTraceGroups_(nBTraceGroups) {}

  std::size_t nBoundaryTraceGroups() const          { return nBTraceGroups_; }
  std::size_t boundaryTraceGroup(const int n) const { return n;                     }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class TopologyTrace >
  void
  apply(const typename XField<PhysDim,typename TopologyTrace::CellTopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTraceGroup,
        const int traceGroupGlobal)
  {
    // number of elements on the local processor
    int nElem = xfldTraceGroup.nElem();

    int topo_index;
         if ( typeid(TopologyTrace) == typeid(Line) || typeid(TopologyTrace) == typeid(Triangle)) topo_index = 0;
    else if ( typeid(TopologyTrace) == typeid(Quad)                                        ) topo_index = 1;
    else SANS_DEVELOPER_EXCEPTION( "Unknown boundary trace topology." );

    // loop over cells within group
    for (int elem = 0; elem < nElem; elem++)
      if ( xfldTraceGroup.associativity(elem).rank() == comm_rank_ )
        bndCount_[topo_index]++;
  }

protected:
  std::vector<int>& bndCount_;
  const int comm_rank_;
  const int nBTraceGroups_;
};


//---------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
class gatherCellElems : public GroupFunctorCellType< gatherCellElems<PhysDim, TopoDim> >
{
public:
  gatherCellElems( const XField<PhysDim,TopoDim>& xfld, std::vector<std::vector<int>>& buffer,
                   const int elemLow, const int elemHigh, TopologyTypes topo )
    : xfld_(xfld), buffer_(buffer), elemLow_(elemLow), elemHigh_(elemHigh), topo_(topo), nCellGroups_(xfld.nCellGroups()) {}

  std::size_t nCellGroups() const          { return nCellGroups_; }
  std::size_t cellGroup(const int n) const { return n;            }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology >
  void
  apply(const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
        const int cellGroupGlobal)
  {
    // only write the requested topology
    if (Topology::Topology != topo_) return;

    int reference = cellGroupGlobal + 1;

    if (Topology::Topology == eTet)
      reference--;  //TODO: remove after FeFloa reference number bug is fixed

    // number of elements on the local processor
    int nElem = xfldCellGroup.nElem();
    int nBasis = xfldCellGroup.nBasis();

    mpi::communicator& comm = *xfld_.comm();
    int comm_rank = comm.rank();

    std::vector<int> nodeDOFmap(nBasis+1);

    const std::vector<int>& cellIDs = xfld_.cellIDs(cellGroupGlobal);

#ifdef SANS_MPI
    std::map<int,std::vector<int>> buffer;
#endif

    for (int elem = 0; elem < nElem; elem++)
    {
      if (cellIDs[elem] >= elemLow_ && cellIDs[elem] < elemHigh_ &&
          xfldCellGroup.associativity(elem).rank() == comm_rank)
      {
        xfldCellGroup.associativity(elem).getGlobalMapping(nodeDOFmap.data(), nBasis);

        // transform back to native DOF indexing and 1-based
        for (int i = 0; i < nBasis; i++)
          nodeDOFmap[i] = xfld_.local2nativeDOFmap(nodeDOFmap[i]) + 1;

        // save off the group index
        nodeDOFmap[nBasis] = reference;

#ifdef SANS_MPI
        buffer[cellIDs[elem]] = nodeDOFmap;
#else
        buffer_[cellIDs[elem]] = nodeDOFmap;
#endif
      }
    }

#ifdef SANS_MPI
    if (comm_rank == 0)
    {
      std::vector<std::map<int,std::vector<int>>> bufferOnRank;
      boost::mpi::gather(comm, buffer, bufferOnRank, 0 );

      // collapse down the buffer from all other ranks
      for (std::size_t i = 1; i < bufferOnRank.size(); i++)
        buffer.insert(bufferOnRank[i].begin(), bufferOnRank[i].end());

      //Write the node data
      for ( const auto& nodesPair : buffer)
        buffer_[nodesPair.first - elemLow_] = nodesPair.second;
    }
#ifndef __clang_analyzer__
    else // send the buffer to rank 0
      boost::mpi::gather(comm, buffer, 0 );
#endif

    comm.barrier();
#endif
  }

protected:
  const XField<PhysDim,TopoDim>& xfld_;
  std::vector<std::vector<int>>& buffer_;
  const int elemLow_;
  const int elemHigh_;
  TopologyTypes topo_;
  const int nCellGroups_;
};

//---------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
class gatherBoundaryTraceElems : public GroupFunctorBoundaryTraceType< gatherBoundaryTraceElems<PhysDim, TopoDim> >
{
public:
  gatherBoundaryTraceElems( const XField<PhysDim,TopoDim>& xfld, std::vector<std::vector<int>>& buffer,
                            const int elemLow, const int elemHigh, TopologyTypes topo )
    : xfld_(xfld), buffer_(buffer), elemLow_(elemLow), elemHigh_(elemHigh), topo_(topo), nBTraceGroups_(xfld.nBoundaryTraceGroups())
  {}

  std::size_t nBoundaryTraceGroups() const { return nBTraceGroups_; }
  std::size_t boundaryTraceGroup(const int n) const { return n; }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology >
  void
  apply(const typename XField<PhysDim,TopoDim>::template FieldTraceGroupType<Topology>& xfldBTrace,
        const int boundaryTraceGroupGlobal)
  {
    // only write the requested topology
    if (Topology::Topology != topo_) return;

    int reference = boundaryTraceGroupGlobal+1; // change to 1-based indexing

    // number of elements on the local processor
    int nElem = xfldBTrace.nElem();

    mpi::communicator& comm = *xfld_.comm();
    int comm_rank = comm.rank();

    std::vector<int> nodeDOFmap(Topology::NNode+1);

    const std::vector<int>& boundaryTraceIDs = xfld_.boundaryTraceIDs(boundaryTraceGroupGlobal);

#ifdef SANS_MPI
    std::map<int,std::vector<int>> buffer;
#endif

    for (int elem = 0; elem < nElem; elem++)
    {
      if (boundaryTraceIDs[elem] >= elemLow_ && boundaryTraceIDs[elem] < elemHigh_ &&
          xfldBTrace.associativity(elem).rank() == comm_rank)
      {
        // grm format only wants linear component of boundary elements
        xfldBTrace.associativity(elem).getNodeGlobalMapping(nodeDOFmap.data(), Topology::NNode);

        // transform back to native DOF indexing and increment the indices because they start at 1
        for (int i = 0; i < Topology::NNode; i++)
          nodeDOFmap[i] = xfld_.local2nativeDOFmap(nodeDOFmap[i]) + 1;

        // save off the group index
        nodeDOFmap[Topology::NNode] = reference;

#ifdef SANS_MPI
        buffer[boundaryTraceIDs[elem]] = nodeDOFmap;
#else
        buffer_[boundaryTraceIDs[elem]] = nodeDOFmap;
#endif
      }
    }

#ifdef SANS_MPI
    if (comm_rank == 0)
    {
      std::vector<std::map<int,std::vector<int>>> bufferOnRank;
      boost::mpi::gather(comm, buffer, bufferOnRank, 0 );

      // collapse down the buffer from all other ranks
      for (std::size_t i = 1; i < bufferOnRank.size(); i++)
        buffer.insert(bufferOnRank[i].begin(), bufferOnRank[i].end());

      //save the node data
      for ( const auto& nodesPair : buffer)
        buffer_[nodesPair.first - elemLow_] = nodesPair.second;
    }
#ifndef __clang_analyzer__
    else // send the buffer to rank 0
      boost::mpi::gather(comm, buffer, 0 );
#endif
#endif
  }

protected:
  const XField<PhysDim,TopoDim>& xfld_;
  std::vector<std::vector<int>>& buffer_;
  const int elemLow_;
  const int elemHigh_;
  TopologyTypes topo_;
  const int nBTraceGroups_;
};

template<class Topology>
struct GmfTopology;

template<> struct GmfTopology<Line>     { static int code() { return GmfEdges; } };
template<> struct GmfTopology<Triangle> { static int code() { return GmfTriangles; } };
template<> struct GmfTopology<Quad>     { static int code() { return GmfQuadrilaterals; } };
template<> struct GmfTopology<Tet>      { static int code() { return GmfTetrahedra; } };
template<> struct GmfTopology<Hex>      { static int code() { return GmfHexahedra; } };

//---------------------------------------------------------------------------//
template<class Topology, class PhysDim, class TopoDim>
void WriteCellGroups( libMeshb_Writer<PhysDim>& writer,
                      const XField<PhysDim,TopoDim>& xfld,
                      const int nElemTotal, const int ElemOffset )
{
  int comm_rank = xfld.comm()->rank();
  int comm_size = xfld.comm()->size();

  if (comm_rank == 0)
    writer.setKeyword( GmfTopology<Topology>::code(), nElemTotal );

  for (int rank = 0; rank < comm_size; rank++)
  {
    // maximum element chunk size that rank 0 will write at any given time
    int nElemChunk = nElemTotal / comm_size + nElemTotal % comm_size;

    // send one chunk of elements at a time to rank 0
    int elemLow  =  rank   *nElemChunk + ElemOffset;
    int elemHigh = (rank+1)*nElemChunk + ElemOffset;

    // All elements have been written if this happens
    if (elemLow >= nElemTotal + ElemOffset) break;

    // Cap if trying to write beyond the total number of elements
    if (elemHigh > nElemTotal + ElemOffset)
    {
      elemHigh = nElemTotal + ElemOffset;
      nElemChunk = elemHigh - elemLow;
    }

    std::vector<std::vector<int>> buffer(nElemChunk);

    for_each_CellGroup<TopoDim>::apply(
        gatherCellElems<PhysDim,TopoDim>( xfld, buffer, elemLow, elemHigh, Topology::Topology),
        xfld );

    // write the elements to the file
    if (comm_rank == 0)
      for (const std::vector<int>& nodeDOFmap : buffer)
        WriteElement<Topology>::write(writer, nodeDOFmap);
  }
}

//---------------------------------------------------------------------------//
template<class Topology, class PhysDim, class TopoDim>
void WriteBoundaryTraceGroups( libMeshb_Writer<PhysDim>& writer,
                               const XField<PhysDim,TopoDim>& xfld,
                               const int nElemTotal, const int ElemOffset )
{
  int comm_rank = xfld.comm()->rank();
  int comm_size = xfld.comm()->size();
  int nElemWrite = 0;

  if (comm_rank == 0)
    writer.setKeyword( GmfTopology<Topology>::code(), nElemTotal );

  for (int rank = 0; rank < comm_size; rank++)
  {
    // maximum element chunk size that rank 0 will write at any given time
    int nElemChunk = nElemTotal / comm_size + nElemTotal % comm_size;

    // send one chunk of elements at a time to rank 0
    int elemLow  =  rank   *nElemChunk + ElemOffset;
    int elemHigh = (rank+1)*nElemChunk + ElemOffset;

    // All elements have been written if this happens
    if (elemLow >= nElemTotal + ElemOffset) break;

    // Cap if trying to write beyond the total number of elements
    if (elemHigh > nElemTotal + ElemOffset)
    {
      elemHigh = nElemTotal + ElemOffset;
      nElemChunk = elemHigh - elemLow;
    }

    std::vector<std::vector<int>> buffer(nElemChunk);

    for_each_BoundaryTraceGroup<TopoDim>::apply(
        gatherBoundaryTraceElems<PhysDim,TopoDim>(xfld, buffer, elemLow, elemHigh, Topology::Topology),
        xfld );

    // write the elements to the file
    if (comm_rank == 0)
    {
      for (const std::vector<int>& nodeDOFmap : buffer)
      {
        WriteElement<Topology>::write(writer, nodeDOFmap);
        nElemWrite++;
      }
    }
  }
}

//---------------------------------------------------------------------------//
template<class PhysDim>
struct WriteVertex;

template<>
struct WriteVertex<PhysD2>
{
  typedef DLA::VectorS<PhysD2::D,Real> VectorX;

  static void write(libMeshb_Writer<PhysD2>& writer, const VectorX& X )
  {
    writer.setLine( GmfVertices, X[0], X[1], 1 );
  }
};

template<>
struct WriteVertex<PhysD3>
{
  typedef DLA::VectorS<PhysD3::D,Real> VectorX;

  static void write(libMeshb_Writer<PhysD3>& writer, const VectorX& X )
  {
    writer.setLine( GmfVertices, X[0], X[1], X[2], 1 );
  }
};

//---------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
void WriteMeshVerticies( const XField<PhysDim,TopoDim>& xfld, libMeshb_Writer<PhysDim>& writer )
{
  int comm_rank = xfld.comm()->rank();

  //Write out vertex coordinates
  int nDOFnative = xfld.nDOFnative();

  if (comm_rank == 0)
    writer.setKeyword( GmfVertices, nDOFnative );

#ifdef SANS_MPI
  typedef DLA::VectorS<PhysDim::D,Real> VectorX;

  int comm_size = xfld.comm()->size();

  // maximum DOF chunk size that rank 0 will write at any given time
  int DOFchunk = nDOFnative / comm_size + nDOFnative % comm_size;

  for (int rank = 0; rank < comm_size; rank++)
  {
    int DOFlow = rank*DOFchunk;
    int DOFhigh = (rank+1)*DOFchunk;

    std::map<int,VectorX> buffer;

    for (int i = 0; i < xfld.nDOF(); i++)
    {
      int iDOF = xfld.local2nativeDOFmap(i);
      if (iDOF >= DOFlow && iDOF < DOFhigh)
        buffer[iDOF] = xfld.DOF(i);
    }

    if (comm_rank == 0)
    {
      std::vector<std::map<int,VectorX>> bufferOnRank;
      boost::mpi::gather(*xfld.comm(), buffer, bufferOnRank, 0 );

      // collapse down the buffer from all ranks (this sorts and remove any possible duplicates)
      for (std::size_t i = 0; i < bufferOnRank.size(); i++)
        buffer.insert(bufferOnRank[i].begin(), bufferOnRank[i].end());

      //Write the node data
      for ( const auto& DOFpair : buffer)
        WriteVertex<PhysDim>::write( writer, DOFpair.second );
    }
#ifndef __clang_analyzer__
    else // send the buffer to rank 0
      boost::mpi::gather(*xfld.comm(), buffer, 0 );
#endif
  } // for rank

#else
  //Write the node data
  for (int k = 0; k < nDOFnative; k++)
    WriteVertex<PhysDim>::write( writer, xfld.DOF(k) );
#endif
}

} // namespace

//---------------------------------------------------------------------------//
template<>
void WriteMesh_libMeshb<PhysD1, TopoD1>(
    const XField<PhysD1,TopoD1>& xfld, const std::string& filename)
{
  SANS_DEVELOPER_EXCEPTION("libMeshb cannot write 1D mesh files.");
}

//---------------------------------------------------------------------------//
template<>
void WriteMesh_libMeshb<PhysD2, TopoD2>(
    const XField<PhysD2,TopoD2>& xfld, const std::string& filename)
{
  int comm_rank = xfld.comm()->rank();

  libMeshb_Writer<PhysD2> writer;

  if (comm_rank == 0)
    writer.open(filename);

  const int nCellGroups = xfld.nCellGroups();
  const int nBTraceGroups = xfld.nBoundaryTraceGroups();

  std::vector<int> cellCount(2,0);
  std::vector<int> bndCount(1,0);

  // count the total number of different element types in each cell group
  for_each_CellGroup<TopoD2>::apply(
      countCellElements<PhysD2>(cellCount, comm_rank, nCellGroups), xfld );

  // count boundary trace elements
  for_each_BoundaryTraceGroup<TopoD2>::apply(
      countBoundaryTraceElements<PhysD2>(bndCount, comm_rank, nBTraceGroups), xfld );

#ifdef SANS_MPI
  // reduce the counts to all ranks
  cellCount = boost::mpi::all_reduce(*xfld.comm(), cellCount, std::plus<std::vector<int>>() );
  bndCount  = boost::mpi::all_reduce(*xfld.comm(), bndCount , std::plus<std::vector<int>>() );
#endif

  //Write out vertex coordinates
  WriteMeshVerticies( xfld, writer );

  int nTriangles = cellCount[0];
  int nQuads     = cellCount[1];
  int ElemOffset = 0;

  //Write the cell connectivity data for all triangles (if any)
  if ( nTriangles > 0 )
    WriteCellGroups<Triangle>( writer, xfld, nTriangles, ElemOffset );

  ElemOffset += nTriangles;

  //Write the cell connectivity data for all quads (if any)
  if ( nQuads > 0 )
    WriteCellGroups<Quad>( writer, xfld, nQuads, ElemOffset );

  int nEdges = bndCount[0];
  ElemOffset = 0;

  //Write the trace connectivity data for all edges (if any)
  if ( nEdges > 0 )
    WriteBoundaryTraceGroups<Line>( writer, xfld, nEdges, ElemOffset );
}

//---------------------------------------------------------------------------//
template<>
void WriteMesh_libMeshb<PhysD3, TopoD3>(
    const XField<PhysD3,TopoD3>& xfld, const std::string& filename)
{
  int comm_rank = xfld.comm()->rank();

  libMeshb_Writer<PhysD3> writer;

  if (comm_rank == 0)
    writer.open(filename);

  const int nCellGroups = xfld.nCellGroups();
  const int nBTraceGroups = xfld.nBoundaryTraceGroups();

  std::vector<int> cellCount(2,0);
  std::vector<int> bndCount(2,0);

  // count the total number of different element types in each cell group
  for_each_CellGroup<TopoD3>::apply( countCellElements<PhysD3>(cellCount, comm_rank, nCellGroups), xfld );

  // count boundary trace elements
  for_each_BoundaryTraceGroup<TopoD3>::apply(
      countBoundaryTraceElements<PhysD3>(bndCount, comm_rank, nBTraceGroups), xfld );

#ifdef SANS_MPI
  // reduce the counts to all ranks
  cellCount = boost::mpi::all_reduce(*xfld.comm(), cellCount, std::plus<std::vector<int>>() );
  bndCount  = boost::mpi::all_reduce(*xfld.comm(), bndCount , std::plus<std::vector<int>>() );
#endif

  //Write out vertex coordinates
  WriteMeshVerticies( xfld, writer );

  int nTets  = cellCount[0];
  int nHexes = cellCount[1];
  int ElemOffset = 0;

  //Write the cell connectivity data for all tets (if any)
  if ( nTets > 0 )
    WriteCellGroups<Tet>( writer, xfld, nTets, ElemOffset );

  ElemOffset += nTets;

  //Write the cell connectivity data for all hexes (if any)
  if ( nHexes > 0 )
    WriteCellGroups<Hex>( writer, xfld, nHexes, ElemOffset );

  int nTriangles = bndCount[0];
  int nQuads     = bndCount[1];
  ElemOffset = 0;

  //Write the boundary trace connectivity data for all triangles (if any)
  if ( nTriangles > 0 )
    WriteBoundaryTraceGroups<Triangle>( writer, xfld, nTriangles, ElemOffset );

  ElemOffset += nTriangles;

  //Write the boundary trace connectivity data for all quads (if any)
  if ( nQuads > 0 )
    WriteBoundaryTraceGroups<Quad>( writer, xfld, nQuads, ElemOffset );
}

//---------------------------------------------------------------------------//
template<>
void WriteMesh_libMeshb<PhysD4, TopoD4>(
    const XField<PhysD4,TopoD4>& xfld, const std::string& filename)
{
  SANS_DEVELOPER_EXCEPTION("libMeshb cannot write 4D mesh files.");
}

}
