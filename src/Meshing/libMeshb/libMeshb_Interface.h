// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef LIBMESHB_INTERFACE_H_
#define LIBMESHB_INTERFACE_H_

#include <libmeshb.h>
#include "tools/SANSException.h"

namespace SANS
{

template <class PhysDim>
class libMeshb_Writer
{
public:
  explicit libMeshb_Writer(const int meshVersion = 2) : fileID_(0), meshVersion_(meshVersion) {}

  libMeshb_Writer(const std::string& filename, const int meshVersion = 2) : fileID_(0), meshVersion_(meshVersion)
  {
    open(filename);
  }

  void open(const std::string& filename)
  {
    if (fileID_ != 0)
      GmfCloseMesh(fileID_);

    fileID_ = GmfOpenMesh(const_cast<char*>(filename.c_str()), GmfWrite, meshVersion_, PhysDim::D);

    if (!fileID_)
      SANS_RUNTIME_EXCEPTION( "libMeshb_Writer<PhysDim> - Error opening file: %s", filename.c_str() );
  }

  ~libMeshb_Writer()
  {
    if (fileID_ != 0)
      GmfCloseMesh(fileID_);
  }

  template<class... Args>
  int setKeyword(int keyword, Args... args)
  {
    int ret;
    ret = GmfSetKwd(fileID_, keyword, args...);
    SANS_ASSERT_MSG(ret > 0, "libMeshb_Writer::setKeyword - Error calling GmfSetKwd!");
    return ret;
  }

  template<class... Args>
  int setLine(int keyword, Args... args)
  {
#ifdef LIBMESH7
    int ret;
    ret = GmfSetLin(fileID_, keyword, args...);
    SANS_ASSERT_MSG(ret > 0, "libMeshb_Writer::setLine - Error calling GmfSetLin!");
    return ret;
#else
    GmfSetLin(fileID_, keyword, args...);
    return 1;
#endif
  }

protected:
  long long int fileID_;
  const int meshVersion_; //Mesh version = 2 => 32-bit integers, 64-bit reals

};

template <class PhysDim>
class libMeshb_Reader
{
public:

  explicit libMeshb_Reader(const int meshVersion = 2) : fileID_(0), meshVersion_(meshVersion) {}

  libMeshb_Reader(const std::string& filename, const int meshVersion = 2) : fileID_(0), meshVersion_(meshVersion)
  {
    open(filename);
  }

  void open(const std::string& filename)
  {
    int version, dim;

    if (fileID_ != 0)
      GmfCloseMesh(fileID_);

    fileID_ = GmfOpenMesh(const_cast<char*>(filename.c_str()), GmfRead, &version, &dim);

    if (!fileID_)
      SANS_RUNTIME_EXCEPTION( "libMeshb_Reader<PhysDim> - Error opening file: %s", filename.c_str() );

    if (meshVersion_ != version)
      SANS_RUNTIME_EXCEPTION( "libMeshb_Reader<PhysDim> - "
                              "Mesh version in file (=%d) does not match the expected version (=%d)", version, meshVersion_ );

    if (PhysDim::D != dim)
      SANS_RUNTIME_EXCEPTION( "libMeshb_Reader<PhysDim> - "
                              "Mesh dimension in file (=%d) does not match the expected dimension (=%d)", dim, PhysDim::D );
  }

  ~libMeshb_Reader()
  {
    if (fileID_ != 0)
      GmfCloseMesh(fileID_);
  }

  template<class... Args>
  int getKeyword(int keyword, Args... args)
  {
    return GmfStatKwd(fileID_, keyword, args...);
  }

  int gotoKeyword(int keyword)
  {
    int ret;
    ret = GmfGotoKwd(fileID_, keyword);
    SANS_ASSERT_MSG(ret > 0, "libMeshb_Reader::gotoKeyword - Error calling GmfGotoKwd!");
    return ret;
  }

  template<class... Args>
  int getLine(int keyword, Args... args)
  {
    int ret;
    ret = GmfGetLin(fileID_, keyword, args...);
    SANS_ASSERT_MSG(ret > 0, "libMeshb_Reader::getLine - Error calling GmfGetLin!");
    return ret;
  }

protected:
  long long int fileID_;
  const int meshVersion_; //Mesh version = 2 => 32-bit integers, 64-bit reals

};

}

#endif /* LIBMESHB_INTERFACE_H_ */
