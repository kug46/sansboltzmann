// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef WRITESOLUTION_LIBMESHB_H_
#define WRITESOLUTION_LIBMESHB_H_

#include "Field/Field.h"

namespace SANS
{

// solution field
template <class PhysDim, class TopoDim, class T>
void WriteSolution_libMeshb(const Field_CG_Cell<PhysDim,TopoDim,T>& fld, const std::string& filename);

}

#endif /* WRITESOLUTION_LIBMESHB_H_ */
