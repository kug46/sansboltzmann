// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <vector>

#include "XField_libMeshb.h"

#include "Topology/ElementTopology.h"

#include "Field/XFieldArea.h"
#include "Field/Partition/XField_Lagrange.h"

#include "libMeshb_Interface.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

//---------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
XField_libMeshb<PhysDim, TopoDim>::
XField_libMeshb( mpi::communicator comm, const std::string& filename)
{
  libMeshb_Reader<PhysDim> reader;
  XField_Lagrange<PhysDim> xfldin(comm);

  // only rank 0 will read the grid
  if (comm.rank() == 0)
    reader.open(filename);

  // Read in the grid coordinates
  readNodes(reader, xfldin);

  // Read the element groups
  readElements(reader, xfldin);

  // Connect all the elements
  this->buildFrom( xfldin );
}

//---------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
void
XField_libMeshb<PhysDim, TopoDim>::
readNodes(libMeshb_Reader<PhysDim>& reader, XField_Lagrange<PhysDim>& xfldin)
{
  std::shared_ptr<mpi::communicator> comm = xfldin.comm();

  typename XField_Lagrange<PhysDim>::VectorX X;

  int nVertex = 0;

  // only rank 0 will read the grid
  if (comm->rank() == 0)
  {
    nVertex = reader.getKeyword( GmfVertices );
    SANS_ASSERT_MSG( nVertex > 0, "ReadMesh<PhysDim, TopoDim, FeFloa>::readNodes - No nodes in mesh file!" );
  }

  // start the process for reading points on all processors
  xfldin.sizeDOF( nVertex );

  if (comm->rank() == 0)
  {
    int reference;

    //Read in node coordinates
    reader.gotoKeyword( GmfVertices );
    for (int i = 0; i < nVertex; i++)
    {
      reader.getLine( GmfVertices, &X[0], &X[1], &reference );
      xfldin.addDOF(X);
    }
  }
}

//---------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
void
XField_libMeshb<PhysDim, TopoDim>::
readElements(libMeshb_Reader<PhysDim>& reader, XField_Lagrange<PhysDim>& xfldin)
{
  std::shared_ptr<mpi::communicator> comm = xfldin.comm();

  //Read in triangles (if any)
  int nTriangles = 0;
  int nQuads = 0;
  int nCellTotal = 0;

  if (comm->rank() == 0)
  {
    //Read number triangles and quads
    nTriangles = reader.getKeyword( GmfTriangles );
    nQuads = reader.getKeyword( GmfQuadrilaterals );

    // total number of cells in the mesh
    nCellTotal = nTriangles + nQuads;
  }

  // Start the process of adding cells on all processors
  xfldin.sizeCells(nCellTotal);

  if (comm->rank() == 0)
  {
    int order = 1;
    int cellgroup;
    if (nTriangles > 0)
    {
      reader.gotoKeyword( GmfTriangles );
      std::vector<int> triangleNodes(Triangle::NNode);

      for (int i = 0; i < nTriangles; i++)
      {
        reader.getLine( GmfTriangles,
                         &triangleNodes[0], &triangleNodes[1], &triangleNodes[2],
                         &cellgroup );

        for (int j = 0; j < Triangle::NNode; j++)
          triangleNodes[j]--; //transform to 0-based indexing

        cellgroup--; //transform to 0-based indexing

        // set the indices for this element
        xfldin.addCell(cellgroup, eTriangle, order, triangleNodes);
      }
    }

    if (nQuads > 0)
    {
      reader.gotoKeyword( GmfQuadrilaterals );
      std::vector<int> quadNodes(Quad::NNode);

      for (int i = 0; i < nQuads; i++)
      {
        reader.getLine( GmfQuadrilaterals,
                        &quadNodes[0], &quadNodes[1], &quadNodes[2], &quadNodes[3],
                        &cellgroup );

        for (int j = 0; j < Quad::NNode; j++)
          quadNodes[j]--; //transform to 0-based indexing

        cellgroup--; //transform to 0-based indexing

        // set the indices for this element
        xfldin.addCell(cellgroup, eQuad, order, quadNodes);
      }
    }
  }

  //=======================
  //
  // Boundary trace groups
  //
  //=======================

  //Read in edges (if any)
  int nEdges = 0;
  if (comm->rank() == 0)
    nEdges = reader.getKeyword( GmfEdges );

  // Start the process of boundary trace elements
  xfldin.sizeBoundaryTrace(nEdges);

  if (comm->rank() == 0)
  {
    if (nEdges > 0)
    {
      int btracegroup;
      reader.gotoKeyword( GmfEdges );
      std::vector<int> lineNodes(Line::NNode);

      for (int i = 0; i < nEdges; i++)
      {
        reader.getLine( GmfEdges,
                        &lineNodes[0], &lineNodes[1],
                        &btracegroup );

        for (int j = 0; j < Line::NNode; j++)
          lineNodes[j]--; //transform to 0-based indexing

        btracegroup--; //transform to 0-based indexing

        // set the indices for this element
        xfldin.addBoundaryTrace(btracegroup, eLine, lineNodes);
      }
    }
  }
}

// explicit instantiation
template class XField_libMeshb<PhysD2, TopoD2>;

}
