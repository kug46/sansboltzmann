// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <iostream>
#include <sstream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "Topology/Dimension.h"
#include "Topology/ElementTopology.h"

#include "MesherInterface_Embedding.h"

#include "Field/XFieldLine.h"
#include "Field/XFieldArea.h"
#include "Field/XFieldVolume.h"

#include "Field/FieldLine_CG_Cell.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldVolume_CG_Cell.h"

#include "Quadrature/Quadrature.h"
#include "Quadrature/QuadratureLine.h"

#include "Meshing/XField1D/XField1D.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

// cppcheck-suppress passedByValue
void EmbeddingMesherParams::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.FilenameBase));
  allParams.push_back(d.checkInputs(params.DisableCall));
  d.checkUnknownInputs(allParams);
}
EmbeddingMesherParams EmbeddingMesherParams::params;

template <class PhysDim, class TopoDim>
MesherInterface<PhysDim, TopoDim, EmbeddingMesher>::MesherInterface(int adapt_iter, const PyDict& paramsDict) :
  adapt_iter_(adapt_iter) {}

int
findElementContaining( Real Uj , std::vector<Real>& u )
{
  for (size_t i=0;i<u.size();i++)
  {
    if (Uj<(u[i+1]+1e-12) && Uj>u[i])
      return i;
  }
  return u.size(); // will go out of bounds
}

template <class PhysDim, class TopoDim>
std::shared_ptr<XField<PhysDim, TopoDim>>
MesherInterface<PhysDim, TopoDim, EmbeddingMesher>::
adapt( const Field_CG_Cell<PhysDim,TopoDim,MatrixSym>& metric_request ) const
{
  typedef typename XField<PhysDim,TopoDim           >::template FieldCellGroupType<Line> XFieldCellGroupType;

  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef Element<Real, TopoD1, Line> ElementMFieldClass;

  const XField<PhysDim,TopoDim>& xfld = metric_request.getXField();

  // Use 3rd order quadrature consistent with EPIC
  Quadrature<TopoD1, Line> quadrature(3);
  const int nquad = quadrature.nQuadrature();
  Real weight;       // quadrature weight

  SANS_ASSERT(xfld.nCellGroups() == 1);

  int n = xfld.nDOF();
  int nElem = xfld.getCellGroupBaseGlobal(0).nElem();

  const XFieldCellGroupType& xfldCell = xfld.template getCellGroupGlobal<Line>(0);

  ElementXFieldClass xfldElem( xfldCell.basis() );
  ElementMFieldClass mfldElem( 1, BasisFunctionCategory_Lagrange );

  // stage 1: measure edge lengths
  std::vector<Real> lengths(nElem);
  for (int elem = 0; elem < nElem; elem++)
  {
    xfldCell.getElement(xfldElem, elem);

    const DLA::MatrixSymS<1,Real>& Mi = metric_request.DOF(elem  );
    const DLA::MatrixSymS<1,Real>& Mj = metric_request.DOF(elem+1);

    DLA::VectorS<1,Real> eij = xfld.DOF(elem+1) - xfld.DOF(elem);

#if 0
    Real lni = Transpose(eij)*Mi*eij;
    Real lnj = Transpose(eij)*Mj*eij;

    lni = sqrt(lni);
    lnj = sqrt(lnj);

    Real L = 0;
    if ( fabs(lni-lnj) <= 1.0e-6*lni )
      L = lni;
    else
      L = (lni-lnj)/(log(lni/lnj));
#else
    mfldElem.DOF(0) = log(Mi(0,0));
    mfldElem.DOF(1) = log(Mj(0,0));

    Real L = 0;
    Real M;
    for (int iquad = 0; iquad < nquad; iquad++)
    {
      quadrature.weight( iquad, weight );
      QuadraturePoint<TopoD1> sRef = quadrature.coordinates_cache( iquad );

      mfldElem.eval( sRef, M );

      L += weight*sqrt(Transpose(eij)*exp(M)*eij);
    }
#endif
    // save length
    lengths[elem] = L;
    printf("l%d = %g\n",elem,L);
  }

  // stage 2: embed (stretch), pinning u(0) = 0
  std::vector<Real> u( n , 0. );
  Real uTotal = 0.;
  for (int i = 1;i < n; i++)
  {
    u[i] = u[i-1] +lengths[i-1];
    uTotal += lengths[i-1];
  }

  // stage 3: generate unit mesh
  int nedges = round(uTotal);
  int N = nedges +1;
  std::vector<Real> U( N , 0. );
  Real du = uTotal/Real(nedges);

  for (int i = 1; i < N; i++)
    U[i] = U[i-1] +du;

  // stage 4: map back via interpolation
  std::vector<Real> X(N,0.);

  // geometry-conformity
  X[0]   = xfld.DOF(0)[0];
  X[N-1] = xfld.DOF(n-1)[0];

  // interpolate the grid coordinates from the embedded ones
  for (int i = 1; i < N-1 ;i++)
  {
    int elem = findElementContaining( U[i] , u );
    DLA::VectorS<1,Real> x0 = xfld.DOF(elem);
    DLA::VectorS<1,Real> x1 = xfld.DOF(elem+1);

    // interpolate
    Real s = ( U[i] -u[elem] ) / ( u[elem+1] -u[elem] );
    DLA::VectorS<1,Real> X0 = x0 +s*(x1 -x0);
    X[i] = X0(0);
    //printf("U = %g, s = %g in elem %d (%g,%g) -> x[%d] = %g\n",U[i],s,elem,u[elem],u[elem+1],i,X[i]);
  }

  return std::make_shared<XField1D>(X);
}


//Explicit instantiations
template class MesherInterface<PhysD1, TopoD1, EmbeddingMesher>;

}
