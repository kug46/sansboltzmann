// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef MESHERINTERFACE_FEFLOA_H_
#define MESHERINTERFACE_FEFLOA_H_

//Python must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <memory>

#include "Field/XField.h"
#include "Field/Field.h"

namespace SANS
{

template <class PhysDim, class TopoDim, class Mesher>
class MesherInterface;

class FeFloa;

//Class for transferring meshes and metric requests between SANS and the FeFloa mesher

struct FeFloaParams : noncopyable
{
  const ParameterString FilenameBase{"FilenameBase", "tmp/", "Default filepath prefix for generated files"};
  const ParameterBool DisableCall{"DisableCall", false, "Disable FeFloa call for testing"};

  static void checkInputs(PyDict d);
  static FeFloaParams params;
};

template <class PhysDim, class TopoDim>
class MesherInterface<PhysDim, TopoDim, FeFloa>
{
public:
  static const int D = PhysDim::D;
  typedef DLA::MatrixSymS<D,Real> MatrixSym;

  MesherInterface(int adapt_iter, const PyDict& paramsDict);

  std::shared_ptr<XField<PhysDim, TopoDim>> adapt(const Field_CG_Cell<PhysDim,TopoDim,MatrixSym>& metric_request) const;

protected:
  int adapt_iter_;
  const PyDict paramsDict_;
};

}

#endif /* MESHERINTERFACE_FEFLOA_H_ */
