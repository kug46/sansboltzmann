// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <iostream>
#include <sstream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/system_call.h"

#include "Topology/Dimension.h"
#include "Topology/ElementTopology.h"

#include "MesherInterface_FeFloa.h"

#include "Field/XFieldArea.h"
#include "Field/XFieldVolume.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldVolume_CG_Cell.h"

#include "Meshing/libMeshb/WriteMesh_libMeshb.h"
#include "Meshing/libMeshb/WriteSolution_libMeshb.h"
#include "Meshing/libMeshb/XField_libMeshb.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"
#include "MPI/MPI_sleep.h"

namespace SANS
{

// cppcheck-suppress passedByValue
void FeFloaParams::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.FilenameBase));
  allParams.push_back(d.checkInputs(params.DisableCall));
  d.checkUnknownInputs(allParams);
}
FeFloaParams FeFloaParams::params;


template <class PhysDim, class TopoDim>
MesherInterface<PhysDim, TopoDim, FeFloa>::MesherInterface(int adapt_iter, const PyDict& paramsDict) :
  adapt_iter_(adapt_iter), paramsDict_(paramsDict) {}

template <class PhysDim, class TopoDim>
std::shared_ptr<XField<PhysDim, TopoDim>>
MesherInterface<PhysDim, TopoDim, FeFloa>::
adapt( const Field_CG_Cell<PhysDim,TopoDim,MatrixSym>& metric_request) const
{
  std::string filename_base = paramsDict_.get(FeFloaParams::params.FilenameBase);
  bool DisableCall = paramsDict_.get(FeFloaParams::params.DisableCall);

  std::string mesh_filename = filename_base + "mesh_a" + std::to_string(adapt_iter_) + ".mesh";
  std::string metric_filename = filename_base + "metric_request_a" + std::to_string(adapt_iter_) + ".sol";
  std::string outputname = filename_base + "fefloa_out_a" + std::to_string(adapt_iter_);
  std::string fefloa_log = filename_base + "fefloa_log.txt";
  std::string outputname_filename = outputname + ".mesh";

  //Write current mesh and metric request
  WriteMesh_libMeshb(metric_request.getXField(), mesh_filename);
  WriteSolution_libMeshb(metric_request, metric_filename);

  if (DisableCall == false)
  {
    if (metric_request.comm()->rank() == 0)
    {
      /*--------------------------*/
      /** system call to feflo.a **/
      /*--------------------------*/
      std::stringstream args;
      args << "-in "<<mesh_filename<<" -met "<<metric_filename<<" -out "<<outputname;
      args << " -qunchk-smoothing 5 -qunchk-swap 5 -nordg"; // -hgrad 2 - gradation
      args << " >> " << fefloa_log; //Append fefloa output to log file

      system_call("feflo.a", "fefloa", args.str());
      wait_for_file(outputname_filename);
    }

    // wait for rank 0 (1 second at a time) for fefloa to finish
    // using barrier causes waiting processor to use 100% CPU
    MPI_sleep(*metric_request.getXField().comm(), 0, 1000);

    //Read in new mesh
    return std::shared_ptr<XField<PhysDim, TopoDim>>( new XField_libMeshb<PhysDim, TopoDim>( *metric_request.comm(), outputname_filename ) );
  }
  else
    return nullptr;
}


//Explicit instantiations
template class MesherInterface<PhysD2, TopoD2, FeFloa>;
template class MesherInterface<PhysD3, TopoD3, FeFloa>;

}
