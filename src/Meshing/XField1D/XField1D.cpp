// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField1D.h"
#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

XField1D::XField1D(const int ii, Real xmin, Real xmax)
{
  std::vector<Real> xvec(ii+1);

  SANS_ASSERT_MSG( xmin < xmax, "xmin = %f, xmax = %f", xmin, xmax );

  // construct vectors
  for (int i = 0; i < ii+1; i++)
    xvec[i] = xmin + (xmax - xmin)*i/Real(ii);

  generateGrid(xvec);
}

XField1D::XField1D(const std::vector<Real>& xvec)
{
  std::size_t ii = xvec.size() - 1;

  //Check ascending order
  for (std::size_t i = 0; i < ii; i++)
    SANS_ASSERT_MSG( xvec[i] < xvec[i+1], "xvec[%d] = %f, xvec[%d] = %f", i, xvec[i], i+1, xvec[i+1] );

  generateGrid(xvec);
}

void
XField1D::generateGrid(const std::vector<Real>& xvec)
{
  SANS_ASSERT( xvec.size() > 0 );

  int ii = (int) xvec.size() - 1;

  int comm_rank = comm_->rank();

  //Create the DOF arrays
  resizeDOF(ii+1);

  //Create the element groups
  resizeInteriorTraceGroups( ii == 1 ? 0 : 1 );
  resizeBoundaryTraceGroups(2);
  resizeCellGroups(1);

  // nodal coordinates for the lines
  for ( int i = 0; i < ii+1; i++ )
    DOF(i) = xvec[i];

  // area field variable
  FieldCellGroupType<Line>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionLineBase::HierarchicalP1, ii );

  //element cell associativity
  for ( int i = 0; i < ii; i++ )
  {
    fldAssocCell.setAssociativity( i ).setRank( comm_rank );
    fldAssocCell.setAssociativity( i ).setNodeGlobalMapping( {i, i+1} );
  }


  cellGroups_[0] = new FieldCellGroupType<Line>( fldAssocCell );
  cellGroups_[0]->setDOF(DOF_, nDOF_);

  nElem_ = fldAssocCell.nElem();

  // interior-trace field variable

  FieldTraceGroupType<Node>::FieldAssociativityConstructorType fldAssocInode( BasisFunctionNodeBase::P0, ii-1 );

  fldAssocInode.setGroupLeft( 0 );
  fldAssocInode.setGroupRight( 0 );

  // edge-element associativity
  for ( int i = 0; i < ii-1; i++ )
  {
    fldAssocInode.setAssociativity( i ).setRank( comm_rank );
    fldAssocInode.setAssociativity( i ).setNodeGlobalMapping( {i+1} );

    fldAssocInode.setAssociativity( i ).setNormalSignL( 1 );
    fldAssocInode.setAssociativity( i ).setNormalSignR( -1 );

    // edge-to-cell connectivity
    fldAssocInode.setElementLeft( i, i );
    fldAssocInode.setElementRight( i+1, i );
    fldAssocInode.setCanonicalTraceLeft( CanonicalTraceToCell(0, 0), i );
    fldAssocInode.setCanonicalTraceRight( CanonicalTraceToCell(1, 0), i );
  }

  if ( ii > 1 )
  {
    interiorTraceGroups_[0] = new FieldTraceGroupType<Line>( fldAssocInode );
    interiorTraceGroups_[0]->setDOF(DOF_, nDOF_);
  }

  // boundary-trace field variable

  FieldTraceGroupType<Node>::FieldAssociativityConstructorType fldAssocBnode0( BasisFunctionNodeBase::P0, 1 );
  FieldTraceGroupType<Node>::FieldAssociativityConstructorType fldAssocBnode1( BasisFunctionNodeBase::P0, 1 );

  // boundary node processor ranks
  fldAssocBnode0.setAssociativity( 0 ).setRank( comm_rank );
  fldAssocBnode1.setAssociativity( 0 ).setRank( comm_rank );

  // node-element associativity
  fldAssocBnode0.setAssociativity( 0 ).setNodeGlobalMapping( {0} );
  fldAssocBnode1.setAssociativity( 0 ).setNodeGlobalMapping( {ii} );

  fldAssocBnode0.setAssociativity( 0 ).setNormalSignL( -1 );
  fldAssocBnode1.setAssociativity( 0 ).setNormalSignL(  1 );

  // node-to-cell connectivity
  fldAssocBnode0.setGroupLeft( 0 );
  fldAssocBnode0.setElementLeft( 0, 0 );
  fldAssocBnode0.setCanonicalTraceLeft( CanonicalTraceToCell(1, 0), 0 );

  fldAssocBnode1.setGroupLeft( 0 );
  fldAssocBnode1.setElementLeft( ii-1, 0 );
  fldAssocBnode1.setCanonicalTraceLeft( CanonicalTraceToCell(0, 0), 0 );

  boundaryTraceGroups_[0] = new FieldTraceGroupType<Node>( fldAssocBnode0 );
  boundaryTraceGroups_[1] = new FieldTraceGroupType<Node>( fldAssocBnode1 );

  boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[1]->setDOF(DOF_, nDOF_);

  //Check that the grid is correct
  checkGrid();
}

}
