// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD1D_H
#define XFIELD1D_H

#include <vector>

#include "Field/XFieldLine.h"

namespace SANS
{
/*
   A unit grid that consists of ii lines within a single group
*/

class XField1D : public XField<PhysD1,TopoD1>
{
public:
  explicit XField1D(const int ii, Real xL = 0, Real xR = 1);

  explicit XField1D(const std::vector<Real>& xvec);

protected:
  void generateGrid(const std::vector<Real>& xvec);
};

}

#endif //XFIELD1D_H
