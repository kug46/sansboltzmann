// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define XFIELD_AVRO_INSTANTIATE
#include "XField_avro_impl.h"

#include "Meshing/TetGen/EGTetGen.h"

#include "Field/XFieldVolume.h"

namespace SANS
{

template<>
void
XField_avro<PhysD3,TopoD3>::fill( const std::vector<double>& params )
{
  using avro::coord_t;
  using avro::index_t;

  const coord_t dim = PhysD3::D;
  // create avro vertices to map boundary groups and vertex on geometry
  avro::Vertices vertices( dim );

  XField_Lagrange<PhysD3> xfld(*this->comm());

  int nDOF = 0;
  int nCell = 0, nBnd = 0, order = 1;
  std::shared_ptr<EGADS::EGTetGen> xfld_tetgen;

  if (this->comm_->rank() == 0)
  {
    std::vector<EGADS::EGBody<PhysD3::D>> bodies;

    for (index_t k = 0; k < amodel_->nb_bodies(); k++ )
      bodies.emplace_back( EGADS::EGObject(amodel_->body(k)->object()) );

    EGADS::EGTessModel tessModel(bodies, params);

    xfld_tetgen = std::make_shared<EGADS::EGTetGen>( tessModel );
    nDOF = xfld_tetgen->nDOF();

    std::vector<Real> x( dim );
    udim_ = vertices.udim();
    std::vector<Real> u( udim_ );
    vertices.reserve(nDOF);
    for (int k = 0; k < nDOF; k++)
    {
      for (coord_t d = 0;d < dim; d++)
        x[d] = xfld_tetgen->DOF(k)[d];
      vertices.create( x );
    }

    const std::map<ego,ego> egoMap = tessModel.alltoUniqueMap();

    // TetGen/AFLR leaves geometry inputs on the geometry
    std::size_t nvert = tessModel.xyzs().size();
    for (std::size_t k = 0; k < nvert; k++)
    {
      ego geom = egoMap.at( tessModel.modelTopoFromVertex(k) );
      avro::Entity* entity = amodel_->lookup( geom );
      vertices.setEntity(k,entity);
      for (coord_t d = 0; d < udim_; d++)
        u[d] = tessModel.uvt()[k][d];

      if (entity->objectClass() == EDGE)
        u[1] = vertices.INFTY;

      vertices.setParam(k,u);
    }
  }

  // size the number of DOFs
  xfld.sizeDOF( nDOF );

  if (this->comm_->rank() == 0)
  {
    // set the X DOFs
    for (int k = 0;k < nDOF; k++)
      xfld.addDOF( xfld_tetgen->DOF(k) );

    // count the number of cell elements
    for (int group = 0; group < xfld_tetgen->nCellGroups(); group++)
      nCell += xfld_tetgen->getCellGroupBase(group).nElem();
  }

  // size the number of cell elements
  xfld.sizeCells( nCell );

  if (this->comm_->rank() == 0)
  {
    typedef typename XField<PhysD3,TopoD3>::template FieldCellGroupType<Tet> XFieldCellGroupType;

    std::vector<int> tetNodes( Tet::NNode );

    for (int group = 0; group < xfld_tetgen->nCellGroups(); group++)
    {
      const XFieldCellGroupType& xfldCellGroup = xfld_tetgen->getCellGroup<Tet>(group);

      const int nElem = xfldCellGroup.nElem();
      for (int elem = 0; elem < nElem; elem++)
      {
        xfldCellGroup.associativity(elem).getNodeGlobalMapping(tetNodes);
        xfld.addCell(group, eTet, order, tetNodes);
      }
    }

    // cout the number of boundary elements
    for (int group = 0; group < xfld_tetgen->nBoundaryTraceGroups(); group++)
      nBnd += xfld_tetgen->getBoundaryTraceGroupBase(group).nElem();
  }

  // size the number of boundary elements
  xfld.sizeBoundaryTrace(nBnd);

  if (this->comm_->rank() == 0)
  {
    typedef typename XField<PhysD3,TopoD3>::template FieldTraceGroupType<Triangle> XFieldTraceGroupType;

    std::vector<int> triNodes( Triangle::NNode );

    for (int group = 0; group < xfld_tetgen->nBoundaryTraceGroups(); group++)
    {
      const XFieldTraceGroupType& xfldTraceGroup = xfld_tetgen->getBoundaryTraceGroup<Triangle>(group);

      const int nElem = xfldTraceGroup.nElem();
      for (int elem = 0; elem < nElem; elem++)
      {
        xfldTraceGroup.associativity(elem).getNodeGlobalMapping(triNodes);
        xfld.addBoundaryTrace(group, eTriangle, triNodes);
      }
    }
  }

  // construct the grid
  this->buildFrom(xfld);

  // construct the map between XField groups and ego's
  this->mapBoundaryGroups(vertices);
  this->importVertexOnGeometry(xfld, vertices);
}

template class XFieldBase_avro<XField<PhysD3,TopoD3>>;
template class XField_avro<PhysD3,TopoD3>;

}
