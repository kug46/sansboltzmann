// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(XFIELD_AVRO_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include <map>
#include <sstream>

#include "XField_avro.h"

#include "tools/linspace.h"
#include "tools/minmax.h"
#include "tools/safe_at.h"

#include "Field/Partition/XField_Lagrange.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/tools/for_each_BoundaryTraceGroup.h"

#include "Meshing/EGADS/EGObject.h"
#include "Meshing/EGADS/EGNode.h"
#include "Meshing/EGADS/EGEdge.h"
#include "Meshing/EGADS/EGFace.h"
#include "Meshing/EGADS/EGBody.h"

// This should always be in a cpp file or impl file
#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#include <mesh/boundary.h>
#include <mesh/local/topology.h>

#ifdef SANS_MPI
#include "MPI/continuousElementMap.h"
#include "MPI/serialize_DenseLinAlg_MatrixS.h"

#include <boost/mpi/collectives/all_reduce.hpp>
#include <boost/mpi/collectives/gather.hpp>
#include <boost/mpi/collectives/all_gather.hpp>
#include <boost/mpi/collectives/scatter.hpp>
#include <boost/serialization/map.hpp>
#endif

namespace SANS
{

//---------------------------------------------------------------------------//
template<class PhysDim>
void
AvroModel::initGeometry()
{
  if (PhysDim::D == 4)
  {
    SANS_ASSERT( this->nb_bodies() == 1 );
    geometry_ = this->body(0)->getChildren();
    nbnd_ = 8;
  }
  else
  {
    static const int Dim = MIN(PhysDim::D, 3);

    std::vector<EGADS::EGBody<Dim>> bodies;

    for (avro::index_t k = 0; k < this->nb_bodies(); k++ )
      bodies.emplace_back( EGADS::EGObject(this->body(k)->object()) );

    for (size_t i = 0; i < bodies.size(); i++)
    {
      // we need to add the lower-dimensional entities in the same
      // order so that the list of lower-dimensional entities is the
      // same across multiple processors.
      std::vector<EGADS::EGNode<Dim>> nodes = bodies[i].getNodes();
      std::vector<EGADS::EGEdge<Dim>> edges = bodies[i].getEdges();
      std::vector<EGADS::EGFace<Dim>> faces = bodies[i].getFaces();

      for (size_t j = 0; j < nodes.size(); j++) geometry_.emplace_back( nodes[j] );
      for (size_t j = 0; j < edges.size(); j++) geometry_.emplace_back( edges[j] );
      for (size_t j = 0; j < faces.size(); j++) geometry_.emplace_back( faces[j] );

      if (PhysDim::D == 2) nbnd_ += edges.size();
      if (PhysDim::D == 3) nbnd_ += faces.size();
    }
  }
}

//---------------------------------------------------------------------------//
template< class BaseType >
XFieldBase_avro<BaseType>::
XFieldBase_avro( mpi::communicator& comm,
                 const std::shared_ptr<avro::Model>& amodel ) :
  BaseType(comm),
  amodel_(std::make_shared<AvroModel>(*amodel)),
  udim_(-1)
{
}

//---------------------------------------------------------------------------//
template< class BaseType >
XFieldBase_avro<BaseType>::
XFieldBase_avro( const std::shared_ptr<mpi::communicator> comm,
                 const std::shared_ptr<avro::Model>& amodel ) :
  BaseType(comm),
  amodel_(std::make_shared<AvroModel>(*amodel)),
  udim_(-1)
{
}

//---------------------------------------------------------------------------//
template< class BaseType >
XFieldBase_avro<BaseType>::
XFieldBase_avro( std::shared_ptr<mpi::communicator> comm,
                 const std::shared_ptr<AvroModel>& amodel ) :
  BaseType(comm),
  amodel_(amodel),
  udim_(-1)
{
}

//---------------------------------------------------------------------------//
template< class BaseType >
XFieldBase_avro<BaseType>::
XFieldBase_avro( const std::shared_ptr<AvroModel>& amodel ) :
  amodel_(amodel),
  udim_(-1)
{
}

//---------------------------------------------------------------------------//
template< class BaseType >
int
XFieldBase_avro<BaseType>::label( avro::Entity* entity ) const
{
  if (entity == NULL) return -1; // interior (volume) point

  return label( entity->object() );
}

//---------------------------------------------------------------------------//
template< class BaseType >
int
XFieldBase_avro<BaseType>::label( ego object ) const
{
  const std::vector<ego>& geometry = amodel_->geometry();
  for (std::size_t k = 0; k < geometry.size(); k++)
  {
    if (geometry[k] == object)
      return k;
  }

  std::stringstream msg;
  msg << "Could not find geometry object!" << std::endl;
  msg << "nb geometry = " << geometry.size() << std::endl;
  for (std::size_t i = 0; i < geometry.size(); i++)
    msg << "geometry(" << i << ") = " << (void*)geometry[i] << std::endl;
  msg << "could not find ego " << (void*)object;
  SANS_DEVELOPER_EXCEPTION(msg.str());
  return -2; // suppress warning
}

//---------------------------------------------------------------------------//
template< class BaseType >
ego
XFieldBase_avro<BaseType>::object( const int k ) const
{
  if (k<0) return NULL;
  return amodel_->geometry()[k];
}

//----------------------------------------------------------------------------//
//  Maps geometry objects to XField boundary groups
template<class PhysDim>
class MapBoundaryGroups :
    public GroupFunctorBoundaryTraceType< MapBoundaryGroups<PhysDim> >
{
public:

  // Save off the boundary trace integrand and the residual vectors
  MapBoundaryGroups( const avro::Vertices& vertices,
                    const std::vector<ego>& geometry,
                    const std::vector<int>& traceGroups,
                    std::map<int,int>& bndGroupMap ) :
   vertices_(vertices),
   geometry_(geometry),
   traceGroups_(traceGroups),
   bndGroupMap_(bndGroupMap) {}

  std::size_t nBoundaryTraceGroups() const          { return traceGroups_.size(); }
  std::size_t boundaryTraceGroup(const int n) const { return traceGroups_[n];     }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the trace group
  template <class TopologyTrace>
  void
  apply( const typename XField<PhysDim, typename TopologyTrace::CellTopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
         const int traceGroupGlobal )
  {
    int nodeDOFmap[TopologyTrace::NNode];
    avro::index_t simplex[TopologyTrace::NNode];

    // loop just one element at the most
    const int nelem = MIN(xfldTrace.nElem(),1);
    for (int elem = 0; elem < nelem; elem++)
    {
      // get the global node element indices
      xfldTrace.associativity(elem).getNodeGlobalMapping(nodeDOFmap, TopologyTrace::NNode );

      // copy to avro data structure
      for (int i = 0; i < TopologyTrace::NNode; i++)
        simplex[i] = nodeDOFmap[i];

      // there must be an entity on the boundary...
      avro::Entity *entity = avro::BoundaryUtils::geometryFacet( vertices_, simplex, TopologyTrace::NNode );
      SANS_ASSERT (entity != nullptr);

      // find the ego that represents the boundary with this element
      ego bndGeom = entity->object();

      bool found = false;
      for (std::size_t i = 0; i < geometry_.size(); i++)
      {
        if (geometry_[i] == bndGeom)
        {
          bndGroupMap_[i] = traceGroupGlobal;
          found = true;
        }
      }
      SANS_ASSERT_MSG(found, "Did not find a boundary trace group!?!?!?");
    }
  }

protected:
  const avro::Vertices& vertices_;
  const std::vector<ego>& geometry_;
  const std::vector<int>& traceGroups_;
  std::map<int,int>& bndGroupMap_;
};

//---------------------------------------------------------------------------//
template< class BaseType >
void
XFieldBase_avro<BaseType>::mapBoundaryGroups(const avro::Vertices& vertices)
{
  // Geometry must be set before generating the map
  SANS_ASSERT(amodel_->geometry().size() > 0);

  std::vector<int> boundaryTraceGroups;
  if (this->nBoundaryTraceGroups() > 0)
    boundaryTraceGroups = linspace(0,this->nBoundaryTraceGroups()-1);

  // Assign the value to the trace
  for_each_BoundaryTraceGroup<typename BaseType::TopoDim>::apply(
      MapBoundaryGroups<typename BaseType::PhysDim>(vertices, amodel_->geometry(), boundaryTraceGroups, bndGroupMap_),
      *this );

  if (amodel_->nBoundary() != this->nBoundaryTraceGroups())
  {
    SANS_RUNTIME_EXCEPTION( "Number of XField boundary traces (%d)\n"
                            "does not match number of EGADS boundaries (%d)",
                            this->nBoundaryTraceGroups(), amodel_->nBoundary() );
  }

#ifdef SANS_MPI
  // synchronize the map across all processors
  std::vector<std::map<int,int>> bndGroupMaps(this->comm_->size());
  boost::mpi::all_gather(*this->comm_, bndGroupMap_, bndGroupMaps );

  for (const std::map<int,int>& bnd : bndGroupMaps)
    bndGroupMap_.insert(bnd.begin(), bnd.end());
#endif
}

//---------------------------------------------------------------------------//
template< class PhysDim, class TopoDim >
XField_avro<PhysDim, TopoDim>::XField_avro(
  mpi::communicator& comm,
  const std::shared_ptr<avro::Model>& amodel ) : BaseType(comm, amodel)
{
  amodel_->template initGeometry<PhysDim>();
}

//---------------------------------------------------------------------------//
template< class PhysDim, class TopoDim >
XField_avro<PhysDim, TopoDim>::XField_avro(
  const XField_avro& xfld,
  const XFieldEmpty ) : BaseType(xfld.comm(), xfld.model())
{
  // set the boundary group map
  bndGroupMap_ = xfld.bndGroupMap_;
}

//---------------------------------------------------------------------------//
template< class PhysDim, class TopoDim >
XField_avro<PhysDim, TopoDim>::XField_avro(
  const XField<PhysDim,TopoDim>& xfld,
  const std::shared_ptr<avro::Model>& _amodel ) : BaseType(xfld.comm(), _amodel)
{
  amodel_->template initGeometry<PhysDim>();
  this->cloneFrom(xfld);
  attachToGeometry();
}

//---------------------------------------------------------------------------//
template< class PhysDim, class TopoDim >
void
XField_avro<PhysDim, TopoDim>::attachToGeometry()
{
  using avro::coord_t;
  using avro::index_t;

  const coord_t dim = BaseType::D;

  // create avro vertices
  avro::Vertices vertices( dim );
  vertices.reserve(this->nDOF());
  std::vector<Real> x( dim );
  for (int k = 0; k< this->nDOF(); k++)
  {
    for (coord_t d = 0;d < dim; d++)
      x[d] = this->DOF(k)[d];
    vertices.create( x );
  }

  // attach them to the geometry
  vertices.findGeometry( *amodel_ );

  // retrieve the geometry information
  udim_ = vertices.udim();
  vertexOnGeometry_.resize( this->nDOF() );
  paramOnGeometry_.resize( udim_*this->nDOF() );
  for (index_t k = 0; k < vertices.nb(); k++)
  {
    avro::Entity* entity = vertices.entity(k);
    vertexOnGeometry_[k] = this->label( entity );

    for (coord_t d = 0; d < udim_; d++)
      paramOnGeometry_[k*udim_+d] = vertices.u(k,d);
  }

  // construct the map between XField groups and ego's
  this->mapBoundaryGroups(vertices);
}

//---------------------------------------------------------------------------//
template< class PhysDim, class TopoDim >
void
XField_avro<PhysDim, TopoDim>::import( avro::Mesh<avro::Simplex>& mesh )
{
  using avro::index_t;
  using avro::coord_t;

  SANS_ASSERT( PhysDim::D == mesh.vertices().dim() );

  XField_Lagrange<PhysDim> xfld(*this->comm());
  typename XField_Lagrange<PhysDim>::VectorX X;

  // read the vertices
  index_t nVertex = 0;
  nVertex = mesh.vertices().nb();
  xfld.sizeDOF( nVertex );

  if (this->comm_->rank() == 0)
  {
    for (index_t k = 0; k < nVertex; k++)
    {
      //SANS_ASSERT_MSG( k < mesh.vertices().nb_ghost() , "k = %lu, nb_ghost = %lu",k,mesh.vertices().nb_ghost() );
      for (coord_t d = 0; d < mesh.vertices().dim(); d++)
        X[d] = mesh.vertices()[k][d];
      xfld.addDOF( X );
    }
  }

#if 1
  avro::Topology<avro::Simplex> topology( mesh.vertices() , mesh.number() );
  mesh.retrieveElements( mesh.number() , topology );
  topology.orient();
#else
  avro::Topology<avro::Simplex>& topology = mesh.topology(0);
  if (!topology.isclosed())
    SANS_ASSERT(false);
  topology.orient();
#endif

  xfld.sizeCells( topology.nb() );

  // read the simplices
  std::vector<int> simplex( mesh.number()+1 );

  int order = 1;
  int cellgroup = 0;

  if (this->comm_->rank() == 0)
  {
    for (index_t k=0;k<topology.nb();k++)
    {
      for (index_t j=0;j<topology.nv(k);j++)
        simplex[j] = topology(k,j);

      if (topology.number()==2)
        xfld.addCell(cellgroup, eTriangle, order, simplex );
      else if (topology.number()==3)
        xfld.addCell(cellgroup, eTet , order , simplex );
      else if (TopoDim::D==4)
        xfld.addCell( cellgroup , ePentatope , order , simplex );
      else
        SANS_DEVELOPER_EXCEPTION( "what?" );
    }
  }

  // get the boundary groups
  avro::Boundary<avro::Simplex> bnd( topology );
  bnd.extract();

  // count how many d-1 boundaries there are
  int nbnd = 0;
  for (index_t k=0;k<bnd.nb_children();k++)
  {
    if (bnd.child(k)->number()==topology.number()-1)
      nbnd += bnd.child(k)->nb();
  }
  xfld.sizeBoundaryTrace(nbnd);

  std::vector<int> facet( mesh.number() );

  if (this->comm_->rank() == 0)
  {
    // check all vertices on geometries are accounted for in the boundary topology
    avro::Vertices& verts = mesh.vertices();
    std::vector<bool> accounted( verts.nb() , false );
    avro::Topology<avro::Simplex> bnd_topology( bnd.vertices() , mesh.number()-1 );
    bnd.retrieveElements( mesh.number()-1 , bnd_topology );
    for (index_t k = 0; k < accounted.size(); k++)
    {
      if (verts.entity(k)==NULL)
        accounted[k] = true;
    }

    for (index_t k = 0;k < bnd_topology.nb(); k++)
    {
      for (index_t j = 0; j < bnd_topology.nv(k); j++)
        accounted[ bnd_topology(k,j) ] = true;
    }

    // there should be no vertices leftover that were not accounted for
    index_t nerr = 0;
    for (index_t k = 0; k < accounted.size(); k++)
    {
      if (verts.entity(k)!=NULL && !accounted[k])
      {
        printf("error! vertex %lu is tagged on geometry!\n",k);
        verts.print(k,true);
        nerr++;
      }
    }
    //SANS_ASSERT(nerr==0);
    printf("incoming avro mesh is clean!\n");

    if (bndGroupMap_.size() == 0)
    {
#if 1
      // construct a boundary map based on the stacking bodies and using the bodyIndex of each
      // body within the stack
      std::vector<index_t> bodyOffset(amodel_->nb_bodies(),0);
      for (index_t k = 0; k < bnd.nb_children(); k++)
      {
        if (bnd.child(k)->number()!=topology.number()-1) continue;

        // determine the entity this boundary is on
        avro::Entity* entity = bnd.entity(k);

        std::size_t ibody = 0;
        for (ibody = 0; ibody < bodyOffset.size(); ibody++)
        {
          if (entity->body()->object() == amodel_->body(ibody)->object())
            break;
        }
        if (ibody == bodyOffset.size()-1) continue;

        // find the maximum body index
        bodyOffset[ibody+1] = MAX(bodyOffset[ibody+1], entity->bodyIndex()-1);
      }

      for (index_t k = 0; k < bnd.nb_children(); k++)
      {
        if (bnd.child(k)->number()!=topology.number()-1) continue;

        // determine the entity this boundary is on
        avro::Entity* entity = bnd.entity(k);

        std::size_t ibody = 0;
        for (ibody = 0; ibody < bodyOffset.size(); ibody++)
        {
          if (entity->body()->object() == amodel_->body(ibody)->object())
            break;
        }

        // stack the groups consistent with the facet numbering of the body offset by each body
        bndGroupMap_[ this->label(entity) ] = bodyOffset[ibody] + entity->bodyIndex()-1;
      }
#else
      for (index_t k=0;k<bnd.nb_children();k++)
        bndGroupMap_[k] = k;
#endif
    }

    int group = 0;
    for (index_t k=0;k<bnd.nb_children();k++)
    {
      if (bnd.child(k)->number()!=topology.number()-1) continue;

      avro::Topology<avro::Simplex>& bndk = *bnd.child(k);

      // determine the entity this boundary is on
      avro::Entity* entity = bnd.entity(k);

      // retrieve the boundary group of the associated ego
      group = safe_at(bndGroupMap_, this->label(entity) );

      for (index_t j=0;j<bndk.nb();j++)
      {
        // retrieve the facet indices
        for (index_t i=0;i<bndk.nv(j);i++)
          facet[i] = bndk(j,i);

        if (bndk.number()==3)
          xfld.addBoundaryTrace(group, eTet , facet);
        else if (bndk.number()==2)
          xfld.addBoundaryTrace(group, eTriangle, facet);
        else if (bndk.number()==1)
          xfld.addBoundaryTrace(group, eLine , facet );
        else
          SANS_DEVELOPER_EXCEPTION( "what?" );
      }

      printf("-> imported boundary group %d\n",group);
    }
  }

  // construct the grid which will generate the local2nativeDOFmap
  this->buildFrom(xfld);
  this->importVertexOnGeometry(xfld, mesh.vertices());
}

//---------------------------------------------------------------------------//
template< class PhysDim, class TopoDim >
void
XField_avro<PhysDim, TopoDim>::importVertexOnGeometry(const XField_Lagrange<PhysDim>& xfld, avro::Vertices& vertices)
{
  // get the paramter dimension
  udim_ = vertices.udim();

#ifdef SANS_MPI
  using avro::coord_t;
  using avro::index_t;

  SANS_ASSERT(udim_ == PhysDim::D-1);
  typedef DLA::VectorS<PhysDim::D-1,Real> VectorP;

  std::map<int,int> vertexOnGeometry_local;
  std::map<int,VectorP> paramOnGeometry_local;

  if (this->comm_->rank() == 0)
  {
    // send global2local maps to rank 0
    std::vector<std::map<int,int>> global2local(this->comm_->size());
    boost::mpi::gather(*this->comm_, xfld.global2localDOFmap(), global2local, 0 );

    // fill vertexOnGeometry for each processor
    std::vector<std::map<int,int>>     vertexOnGeometry_buffer(this->comm_->size());
    std::vector<std::map<int,VectorP>> paramOnGeometry_buffer(this->comm_->size());
    for (int rank = 0; rank < this->comm_->size(); rank++)
    {
      for (const std::pair<int,int>& gl : global2local[rank])
      {
        avro::Entity* entity = vertices.entity(gl.first);
        vertexOnGeometry_buffer[rank][gl.second] = this->label( entity );

        VectorP param;
        for (int d = 0; d < VectorP::M; d++)
          param[d] = vertices.u(gl.first,d);

        paramOnGeometry_buffer[rank][gl.second] = param;
      }
    }

    // scatter back vertexOnGeometry to each processor
    boost::mpi::scatter(*this->comm_, vertexOnGeometry_buffer, vertexOnGeometry_local, 0 );
    boost::mpi::scatter(*this->comm_, paramOnGeometry_buffer , paramOnGeometry_local , 0 );
  }
  else // send the to and receive from rank 0
  {
    boost::mpi::gather(*this->comm_, xfld.global2localDOFmap(), 0 );

    boost::mpi::scatter(*this->comm_, vertexOnGeometry_local, 0 );
    boost::mpi::scatter(*this->comm_, paramOnGeometry_local , 0 );
  }

  // populate the local array
  vertexOnGeometry_.resize( vertexOnGeometry_local.size() );
  for (const std::pair<int,int>& vOnG : vertexOnGeometry_local)
    vertexOnGeometry_[vOnG.first] = vOnG.second;

  paramOnGeometry_.resize( udim_*paramOnGeometry_local.size() );
  for (const std::pair<int,VectorP>& pOnG : paramOnGeometry_local)
    for (coord_t d = 0; d < udim_; d++)
      paramOnGeometry_[udim_*pOnG.first+d] = pOnG.second[d];

#else
  int nVertex = vertices.nb();

  vertexOnGeometry_.resize( nVertex );
  paramOnGeometry_.resize( udim_*nVertex );
  for (int n = 0; n < nVertex; n++)
  {
    avro::Entity* entity = vertices.entity(n);
    vertexOnGeometry_[n] = this->label( entity );
    for (avro::coord_t d = 0; d < udim_; d++)
      paramOnGeometry_[n*udim_+d] = vertices.u(n,d);
  }

#endif
}

template <class PhysDim, class TopoDim>
class addCellGroupTopology : public GroupFunctorCellType< addCellGroupTopology<PhysDim,TopoDim> >
{
public:
  addCellGroupTopology( const XField_avro<PhysDim,TopoDim>& xfld,
                        avro::Topology<avro::Simplex>& topology,
                        const int nCellGroups )
    : xfld_(xfld), topology_(topology), nCellGroups_(nCellGroups) {}

  std::size_t nCellGroups() const          { return nCellGroups_; }
  std::size_t cellGroup(const int n) const { return n;            }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology >
  void
  apply(const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldCell,
        const int cellGroupGlobal)
  {
    SANS_ASSERT( topology_.number()==TopoDim::D );
    using avro::index_t;

    int nElem = xfldCell.nElem();
    int nBasis = xfldCell.nBasis();
    SANS_ASSERT( nBasis == TopoDim::D+1 );

    std::vector<int> nodeDOFmap( nBasis );
    std::vector<index_t> simplex( nBasis );

#ifdef SANS_MPI

    int comm_rank = xfld_.comm()->rank();

    // construct a continuous element ID map for the elements in the current group
    const std::vector<int>& cellIDs = xfld_.cellIDs(cellGroupGlobal);
    std::map<int,int> globalElemMap;
    continuousElementMap( *xfld_.comm(), cellIDs, xfldCell, nElem, globalElemMap);

    int comm_size = xfld_.comm()->size();
    int nElemLocal = xfldCell.nElem();

    // maximum element chunk size that rank 0 will write at any given time
    int Elemchunk = nElem / comm_size + nElem % comm_size;

    // send one chunk of elements at a time to rank 0
    for (int rank = 0; rank < comm_size; rank++)
    {
      int elemLow  =  rank   *Elemchunk;
      int elemHigh = (rank+1)*Elemchunk;

      std::map<int,std::vector<int>> buffer;

      for (int elem = 0; elem < nElemLocal; elem++)
      {
        int elemID = globalElemMap[cellIDs[elem]];

        if (elemID >= elemLow && elemID < elemHigh &&
            xfldCell.associativity(elem).rank() == comm_rank)
        {
          xfldCell.associativity(elem).getGlobalMapping( nodeDOFmap.data() , nodeDOFmap.size() );

          // transform back to native DOF indexing
          for (std::size_t i = 0; i < nodeDOFmap.size(); i++)
            nodeDOFmap[i] = xfld_.local2nativeDOFmap(nodeDOFmap[i]);

          buffer[cellIDs[elem]] = nodeDOFmap;
        }
      }

      if (comm_rank == 0)
      {
        std::vector<std::map<int,std::vector<int>>> bufferOnRank;
        boost::mpi::gather(*xfld_.comm(), buffer, bufferOnRank, 0 );

        // collapse down the buffer from all other ranks
        for (std::size_t i = 1; i < bufferOnRank.size(); i++)
          buffer.insert(bufferOnRank[i].begin(), bufferOnRank[i].end());

        // create the simplex and add it to the topology
        for ( const auto& nodesPair : buffer)
        {
          for (int n = 0; n < nBasis; n++)
            simplex[n] = nodesPair.second[n];

          topology_.add( simplex.data() , simplex.size() );
        }
      }
      else // send the buffer to rank 0
        boost::mpi::gather(*xfld_.comm(), buffer, 0 );

    } // loop over ranks

#else // SANS_MPI

    for (int elem=0;elem<nElem;elem++)
    {
      // get the global element indices
      xfldCell.associativity(elem).getGlobalMapping(nodeDOFmap.data(), nodeDOFmap.size() );
      for (int i=0;i<nBasis;i++)
        simplex[i] = nodeDOFmap[i];

      // add the simplex to the topology
      topology_.add( simplex.data() , simplex.size() );
    }

#endif // SANS_MPI

  }

private:
  const XField_avro<PhysDim,TopoDim>& xfld_;
  avro::Topology<avro::Simplex>& topology_;
  int nCellGroups_;
};

//---------------------------------------------------------------------------//
template< class PhysDim, class TopoDim >
void
XField_avro<PhysDim, TopoDim>::convert( avro::Mesh<avro::Simplex>& mesh ) const
{
  using avro::index_t;
  using avro::coord_t;

  // all processors can clear the mesh
  mesh.clear();

  const coord_t dim = mesh.vertices().dim();

  std::vector<Real> x( PhysDim::D );
  SANS_ASSERT( dim == PhysDim::D );
  SANS_ASSERT( int(vertexOnGeometry_.size()) == this->nDOF() );

  index_t nDOFnative = this->nDOFnative();

#ifdef SANS_MPI
  typedef DLA::VectorS<PhysDim::D,Real> VectorX;
  typedef DLA::VectorS<PhysDim::D-1,Real> VectorP;

  std::map<int,VectorX> buffer_coord;
  std::map<int,int>     buffer_geom;
  std::map<int,VectorP> buffer_param;

  int comm_rank = this->comm()->rank();

  for (int i = 0; i < this->nDOF(); i++)
  {
    int iDOF_native = this->local2nativeDOFmap(i);
    buffer_coord[iDOF_native] = this->DOF(i);
    buffer_geom[iDOF_native]  = vertexOnGeometry_[i];

    VectorP param;
    for (int d = 0; d < VectorP::M; d++)
      param[d] = this->paramOnGeometry(i)[d];
    buffer_param[iDOF_native] = param;
  }

  if (comm_rank == 0)
  {
    mesh.vertices().reserve(nDOFnative);
    std::vector<Real> x0(dim,0.0);
    for (index_t k = 0; k < nDOFnative; k++)
      mesh.vertices().create(x0.data());

    std::vector<std::map<int,VectorX>> bufferCoordOnRank;
    std::vector<std::map<int,int>>     bufferGeomOnRank;
    std::vector<std::map<int,VectorP>> bufferParamOnRank;
    boost::mpi::gather(*this->comm(), buffer_coord, bufferCoordOnRank, 0 );
    boost::mpi::gather(*this->comm(), buffer_geom,  bufferGeomOnRank , 0 );
    boost::mpi::gather(*this->comm(), buffer_param, bufferParamOnRank, 0 );

    // collapse down the buffer from all ranks (this removes duplicates)
    int nbuffer = bufferCoordOnRank.size();
    SANS_ASSERT( int(bufferGeomOnRank.size())==nbuffer );
    for (std::size_t i = 0; i < std::size_t(nbuffer); i++)
    {
      buffer_coord.insert(bufferCoordOnRank[i].begin(), bufferCoordOnRank[i].end());
      buffer_geom.insert( bufferGeomOnRank[i].begin(),  bufferGeomOnRank[i].end() );
      buffer_param.insert(bufferParamOnRank[i].begin(), bufferParamOnRank[i].end());
    }

    // create the vertices
    for ( const auto& DOFpair : buffer_coord)
    {
      // set the coordinates
      for (coord_t d=0;d<dim;d++)
        mesh.vertices()[DOFpair.first][d] = DOFpair.second[d];
    }

    // attach the geometry
    for ( const auto& geom : buffer_geom )
    {
      avro::Entity* entity = NULL;
      if (geom.second>=0)
        entity = amodel_->lookup( amodel_->geometry()[geom.second] );
      mesh.vertices().setEntity( geom.first , entity );
      //mesh.vertices().print( geom.first , true );
    }

    // set the paramter values
    for ( const auto& param : buffer_param )
      mesh.vertices().setParam( param.first , &param.second[0] );

  }
  else // send the buffer to rank 0
  {
    boost::mpi::gather(*this->comm(), buffer_coord, 0 );
    boost::mpi::gather(*this->comm(), buffer_geom,  0 );
    boost::mpi::gather(*this->comm(), buffer_param, 0 );
  }

#else // SANS_MPI

  mesh.vertices().reserve(nDOFnative);
  std::vector<Real> x0(dim,0.0);
  for (index_t k = 0; k < nDOFnative; k++)
    mesh.vertices().create(x0.data());

  // create the vertices
  for (index_t k = 0; k < index_t(this->nDOFnative()); k++)
  {
    // set the coordinates
    for (coord_t d = 0; d < mesh.vertices().dim(); d++)
      mesh.vertices()[k][d] = this->DOF(k)[d];

    // lookup the geometric entity and attach it to the vertex
    if (vertexOnGeometry_[k] >= 0)
    {
      avro::Entity* entity = amodel_->lookup( amodel_->geometry()[vertexOnGeometry_[k]] );
      mesh.vertices().setEntity( k , entity );
      mesh.vertices().setParam( k , this->paramOnGeometry(k) );
    }
    else
      mesh.vertices().setEntity( k , NULL ); // volume point
  }

#endif // SANS_MPI

  // create the simplices
  smart_ptr(avro::Topology<avro::Simplex>) topology
                = smart_new(avro::Topology<avro::Simplex>)
                                              (mesh.vertices(),mesh.number());
  mesh.addTopology(topology);

  // loop over cell groups and write out the connectivity
  int nCellGroup = this->nCellGroups();
  for_each_CellGroup<TopoDim>::apply( addCellGroupTopology<PhysDim, TopoDim>(*this,*smart_raw(topology), nCellGroup), *this );

  // put a barrier for processor communication
  this->comm()->barrier();
}

} // SANS
