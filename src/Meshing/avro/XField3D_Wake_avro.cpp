// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define XFIELD_AVRO_INSTANTIATE
#include "XField_avro_impl.h"

#include "tools/timer.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "Meshing/avro/XField3D_Wake_avro.h"

#include "Meshing/EGTess/EGTessModel.h"
#include "Meshing/TetGen/EGTetGen.h"
#include "Meshing/AFLR/AFLR3.h"

#include "Field/Element/ElementXFieldArea.h"
#include "Field/Element/UniqueElem.h"

// include avro
#include <geometry/body.h>
#include <mesh/geometrics.h>
#include <mesh/tecplot.h>
#include <mesh/local/primitive.h>
#include <graphics/plotter.h>

#include <set>
#include <vector>
#include <array>
#include <algorithm> //std::sort

namespace SANS
{

//----------------------------------------------------------------------------//
XField3D_Wake_avro::XField3D_Wake_avro( const EGADS::EGTessModel& tessModel )
  : BaseType( std::make_shared<AvroWakeModel>(tessModel) )
{
  using avro::coord_t;
  using avro::index_t;

  // generate an initial mesh
  EGADS::EGTetGen xfld(tessModel);
  //AFLR3 xfld(tessModel);

  XField3D_Wake::cloneFrom(xfld);

  const coord_t dim = PhysD3::D;

  // create avro vertices to map boundary groups
  avro::Vertices vertices( dim );
  std::vector<Real> x( dim );
  udim_ = vertices.udim();
  std::vector<Real> u( udim_ );
  vertices.reserve(this->nDOF());
  for (int k = 0; k< this->nDOF(); k++)
  {
    for (coord_t d = 0;d < dim; d++)
      x[d] = this->DOF(k)[d];
    vertices.create( x );
  }

  const std::map<ego,ego> egoMap = tessModel.alltoUniqueMap();

  // retrieve the geometry information
  vertexOnGeometry_.resize( this->nDOF(), -1 );
  paramOnGeometry_.resize(udim_*this->nDOF(), vertices.INFTY);

  // TetGen/AFLR leaves geometry inputs on the geometry
  std::size_t nvert = tessModel.xyzs().size();
  for (std::size_t k = 0; k < nvert; k++)
  {
    ego geom = egoMap.at( tessModel.modelTopoFromVertex(k) );
    vertexOnGeometry_[k] = label( geom );
    avro::Entity* entity = amodel_->lookup( geom );
    vertices.setEntity(k,entity);
    for (coord_t d = 0; d < udim_; d++)
    {
      u[d] = tessModel.uvt()[k][d];
      paramOnGeometry_[udim_*k+d] = u[d];
    }
    if (entity->objectClass() == EDGE)
    {
      u[1] = vertices.INFTY;
      paramOnGeometry_[udim_*k+1] = u[1];
    }
    vertices.setParam(k,u);
  }

  // also set geometry for duplicated points
  for (std::size_t i = 0; i < xfld.invPointMap_.size(); i++)
  {
    int k = xfld.invPointMap_[i];
    int iDup = xfld.dupPointOffset_ + i;
    ego geom = egoMap.at( tessModel.modelTopoFromVertex(k) );
    vertexOnGeometry_[iDup] = label( geom );
    avro::Entity* entity = amodel_->lookup( geom );
    vertices.setEntity(iDup,entity);
    for (coord_t d = 0; d < udim_; d++)
    {
      u[d] = tessModel.uvt()[k][d];
      paramOnGeometry_[udim_*iDup+d] = u[d];
    }
    if (entity->objectClass() == EDGE)
    {
      u[1] = vertices.INFTY;
      paramOnGeometry_[udim_*iDup+1] = u[1];
    }
    vertices.setParam(iDup,u);
  }

  // construct the map between XField groups and ego's
  this->mapBoundaryGroups(vertices);
}

//---------------------------------------------------------------------------//
XField3D_Wake_avro::XField3D_Wake_avro(
  const XField3D_Wake_avro& xfld,
  const XFieldEmpty ) : BaseType(xfld.comm(), xfld.model())
{
  // set the boundary group map
  bndGroupMap_ = xfld.bndGroupMap_;

  SANS_ASSERT(amodel_->derivedTypeID() == typeid(AvroWakeModel));
}

//----------------------------------------------------------------------------//
XField3D_Wake_avro::AvroWakeModel::AvroWakeModel( const EGADS::EGTessModel& tessModel )
{
  using namespace EGADS;

  // track all the unique ego's from the tessModel
  std::vector<EGADS::EGNode<3>> nodes = tessModel.getUniqueNodes();
  std::vector<EGADS::EGEdge<3>> edges = tessModel.getUniqueEdges();
  std::vector<EGADS::EGFace<3>> faces = tessModel.getUniqueFaces();

  std::size_t offset = 0;
  geometry_.resize(nodes.size() + edges.size() + faces.size());
  for (std::size_t j = 0; j < nodes.size(); j++) { geometry_[offset+j] = nodes[j]; } offset += nodes.size();
  for (std::size_t j = 0; j < edges.size(); j++) { geometry_[offset+j] = edges[j]; } offset += edges.size();
  for (std::size_t j = 0; j < faces.size(); j++) { geometry_[offset+j] = faces[j]; }

  const std::map<ego,std::vector<ego>> faceEdgeMap = tessModel.getFaceChildren();
  const std::map<ego,std::vector<ego>> edgeNodeMap = tessModel.getEdgeChildren();

  // get all the bodies
  std::vector< EGBody<3> > bodies = tessModel.getBodies();

  SANS_ASSERT(bodies.size() > 0);
  ego context;
  EG_STATUS( EG_getContext((ego)bodies[0], &context) );
  EG_setOutLevel(context, 0);

  std::map<ego,std::shared_ptr<avro::Entity>> ego2entity;

  for (std::size_t k = 0; k < bodies.size(); k++)
  {
    std::shared_ptr<avro::Body> body = std::make_shared<avro::Body>(bodies[k],0,this);

    // get all the Nodes, Edges and Faces in this body
    std::vector< EGFace<3> > Faces = bodies[k].getFaces();

    // we basically need to emulate 'buildHierarchy' in avro
    // so everything below is just an unrolled version of buildHierarchy

    nbnd_ += Faces.size(); // count the faces as EGADS boundaries

    // let the body hold all the faces
    for ( EGFace<3>& face : Faces)
    {
      std::shared_ptr<avro::Entity> Face;

      // add the child
      auto it = ego2entity.find(face);
      if (it != ego2entity.end())
        Face = it->second;
      else
      {
        Face = std::make_shared<avro::Entity>(static_cast<ego>(face), body.get());
        Face->getInfo();
#ifndef __clang_analyzer__ // clang is confused
        ego2entity[face] = Face;
#endif
      }
      body->addEntity(Face); // add the face entity as a child to the body

      // now associate the edges of this face
      // avro doesn't care about loops, only things that are tessellatable (like Edges)
      const std::vector< ego >& Edges = faceEdgeMap.at(face);
      for (const ego edge : Edges)
      {
        // we need to figure out which edge this was mapped to
        // look for the edge in the list of egos

        // check if the egoEdge is already associated with an entity
        std::shared_ptr<avro::Entity> Edge;
        auto it = ego2entity.find(edge);
        if (it != ego2entity.end())
          Edge = it->second;
        else
        {
          Edge = std::make_shared<avro::Entity>(edge, body.get());
          Edge->getInfo();
#ifndef __clang_analyzer__ // clang is confused
          ego2entity[edge] = Edge;
#endif
        }
        Face->addChild(Edge);

        // get the Nodes of this edge
        const std::vector< ego >& Nodes = edgeNodeMap.at(edge);
        for ( const ego node : Nodes)
        {
          // check if the egoNode is already associated with an entity
          std::shared_ptr<avro::Entity> Node;
          auto it = ego2entity.find(node);
          if (it != ego2entity.end())
            Node = it->second;
          else
          {
            Node = std::make_shared<avro::Entity>(node, body.get());
            Node->getInfo();
#ifndef __clang_analyzer__ // clang is confused
            ego2entity[node] = Node;
#endif
          }
          Edge->addChild(Node);
        }
      }
    } // loop over faces

    this->addBody(body,true);
    body->buildParents();
  } // loop over bodies


  // set the wake entitites
  const std::vector< int >& sheetBodies = tessModel.modelSheetBodyIndex();

  for ( std::size_t ibody = 0; ibody < sheetBodies.size(); ibody++ )
  {
    const int bodyIndex = sheetBodies[ibody];
#if 1
    std::vector< EGEdge<3> > edges = bodies[bodyIndex].getEdges();
    for ( const EGEdge<3>& edge : edges )
    {
      auto it = ego2entity.find(edge);
      if (it != ego2entity.end())
        it->second->setWake(true);
    }
#endif
    std::vector< EGFace<3> > faces = bodies[bodyIndex].getFaces();
    for ( const EGFace<3>& face : faces )
    {
      auto it = ego2entity.find(face);
      if (it != ego2entity.end())
        it->second->setWake(true);
    }
  }

  // initialize all the maps for tracking duplicity
  initDuplicityMaps( tessModel );

  // update the avro entities to mark wakesheets
  std::vector<avro::Entity*> entities;
  this->listEntities(entities);
  for (std::size_t k=0;k<entities.size();k++)
  {
    if (modelDuplicity_.find(entities[k]->object())->second==true)
      entities[k]->setWake(true);
  }
}

//----------------------------------------------------------------------------//
void
XField3D_Wake_avro::AvroWakeModel::initDuplicityMaps( const EGADS::EGTessModel& tessModel )
{
  using namespace EGADS;

  // Get all the bodies in the model
  std::vector< EGBody<3> > bodies = tessModel.getBodies();

  const std::vector< int >& solidBodies = tessModel.modelSolidBodyIndex();
  const std::vector< int >& sheetBodies = tessModel.modelSheetBodyIndex();

  const std::vector< std::vector< int > >& sheetEdgeIndex = tessModel.modelSheetEdgeIndex();
  const std::vector< std::vector< int > >& sheetNodeIndex = tessModel.modelSheetNodeIndex();

  const std::map<ego,ego> egoMap = tessModel.alltoUniqueMap();

  //--------------------------------
  std::vector< EGEdge<3> > edgesUnique = tessModel.getUniqueEdges();
  std::set<std::string> wakeNames;
  enum WakeEdge
  {
    Null,
    Kutta,
    Trefftz
  };

  std::map<ego,WakeEdge> wakeEdges;
  int nKuttaEdge = 0;

  // Gather all the wake names from the Kutta/Trefftz planes
  for ( const EGEdge<3>& edge : edgesUnique )
  {
    std::string wakename;
    if ( edge.hasAttribute(XField3D_Wake::WAKESHEET) )
    {
      edge.getAttribute(XField3D_Wake::WAKESHEET, wakename);
      wakeNames.insert(wakename);
      wakeEdges[edge] = WakeEdge::Kutta;
      nKuttaEdge++;
    }
    else if ( edge.hasAttribute(XField3D_Wake::TREFFTZ) )
    {
      edge.getAttribute(XField3D_Wake::TREFFTZ, wakename);
      wakeNames.insert(wakename);
      wakeEdges[edge] = WakeEdge::Trefftz;
    }
  }

  int igroupKutta = 0;
  int igroupTrefftz = nKuttaEdge;
  for ( const std::pair<ego,WakeEdge>& edge : wakeEdges )
  {
    if ( edge.second == WakeEdge::Kutta )
      frameGroupKutta_[edge.first] = igroupKutta++;

    if ( edge.second == WakeEdge::Trefftz )
      frameGroupTrefftz_[edge.first] = igroupTrefftz++;
  }


  //--------------------------------
  // Create maps of which edges could have points duplicated (i.e. for wakes in full potential)
  int modelFace = 0;
  int modelEdge = 0;
  int modelNode = 0;
  for ( std::size_t ibody = 0; ibody < bodies.size(); ibody++ )
  {
    modelFace += bodies[ibody].nFaces();
    modelEdge += bodies[ibody].nEdges();
    modelNode += bodies[ibody].nNodes();

    // get all the Nodes, Edges and Faces in this body
    std::vector< EGNode<3> > Nodes = bodies[ibody].getNodes();
    std::vector< EGEdge<3> > Edges = bodies[ibody].getEdges();
    std::vector< EGFace<3> > Faces = bodies[ibody].getFaces();

    // default them all to be duplicate
    for ( const EGNode<3>& node : Nodes)
      modelDuplicity_[node] = false;

    for ( const EGEdge<3>& edge : Edges)
    {
      modelDuplicity_[edge] = false;
      modelEdgeKutta_[edge] = false;
    }

    for ( const EGFace<3>& face : Faces)
    {
      modelDuplicity_[face] = false;
      modelFaceKutta_[face] = false;
    }
  }


  std::vector< EGEdge<3> > modelEdges = tessModel.getAllEdges();
  std::vector< EGNode<3> > modelNodes = tessModel.getAllNodes();

  for ( std::size_t ibody = 0; ibody < sheetBodies.size(); ibody++ )
  {
    const int bodyIndex = sheetBodies[ibody];

    // set sheet nodes to true
    std::vector< EGNode<3> > nodes = bodies[bodyIndex].getNodes();
    for ( std::size_t inode = 0; inode < nodes.size(); inode++ )
    {
      modelNode = sheetNodeIndex[ibody][inode];
      modelDuplicity_[modelNodes[modelNode]] = true;
    }

    std::vector< EGEdge<3> > edges = bodies[bodyIndex].getEdges();
    for ( std::size_t iedge = 0; iedge < edges.size(); iedge++ )
    {
      modelEdge = sheetEdgeIndex[ibody][iedge];

      // set sheet edges to true
      modelDuplicity_[modelEdges[modelEdge]] = true;

      // Edges with only one face attached cannot be duplicated, unless they are floating edges
      if ( bodies[bodyIndex].getFaces( modelEdges[modelEdge] ).size() == 1 &&
          !modelEdges[modelEdge].hasAttribute(EGTessModel_FLOATINGEDGE) &&
          !modelEdges[modelEdge].hasAttribute(XField3D_Wake::WAKESHEET) &&
          !modelEdges[modelEdge].hasAttribute(XField3D_Wake::TREFFTZ) )
        modelDuplicity_[modelEdges[modelEdge]] = false;
    }
  }

  //--------------------------------
  // Create maps of which nodes could have points duplicated
  for ( std::size_t ibody = 0; ibody < sheetBodies.size(); ibody++ )
  {
    const int bodyIndex = sheetBodies[ibody];
    const std::vector< EGNode<3> > nodes = bodies[bodyIndex].getNodes();

    for ( std::size_t inode = 0; inode < nodes.size(); inode++ )
    {
      const int bodyNode = bodies[bodyIndex].getBodyIndex(nodes[inode])-1;

      // Extract all edges that reference the node
      const std::vector< EGEdge<3> > edges = bodies[bodyIndex].getEdges( nodes[inode] );

      // All edges with two faces can have points that are duplicated
      for ( std::size_t iedge = 0; iedge < edges.size(); iedge++ )
      {
        const int bodyEdge = bodies[bodyIndex].getBodyIndex(edges[iedge])-1;

        modelEdge = sheetEdgeIndex[ibody][bodyEdge];
        modelNode = sheetNodeIndex[ibody][bodyNode];

        // All model Edges must be duplicable if the node can be duplicated
        modelDuplicity_[modelNodes[modelNode]] = modelDuplicity_[modelNodes[modelNode]] &&
                                                 modelDuplicity_[modelEdges[modelEdge]];
      }
    }
  }

  // Check all faces in the sheet bodies
  for ( std::size_t ibody = 0; ibody < sheetBodies.size(); ibody++ )
  {
    const int bodyIndex = sheetBodies[ibody];
    std::vector< EGFace<3> > faces = bodies[bodyIndex].getFaces();

    for ( const EGFace<3>& face : faces)
    {
      // Check if the sheet face is considered a boundary condition
      if ( face.hasAttribute(XField3D_Wake::BCNAME) )
      {
        std::string BCName;
        face.getAttribute(XField3D_Wake::BCNAME, BCName);

        // If the BC is a wake sheet, then mark it as a duplication face
        if ( std::find(wakeNames.begin(), wakeNames.end(), BCName) != wakeNames.end() )
          modelDuplicity_[face] = true;
      }
    }
  }


  //--------------------------------
  // Identify ego's that are not duplicated on the wakes
  for ( std::size_t ibody = 0; ibody < sheetBodies.size(); ibody++ )
  {
    const int bodyIndex = sheetBodies[ibody];

    const std::vector< EGNode<3> > nodes = bodies[bodyIndex].getNodes();
    for ( std::size_t bodyNode = 0; bodyNode < nodes.size(); bodyNode++ )
    {
      modelNode = sheetNodeIndex[ibody][bodyNode];
      if (!modelDuplicity_[modelNodes[modelNode]])
        wakeNonDup_.insert(modelNodes[modelNode]);
    }

    const std::vector< EGEdge<3> > edges = bodies[bodyIndex].getEdges();
    for ( std::size_t bodyEdge = 0; bodyEdge < edges.size(); bodyEdge++ )
    {
      modelEdge = sheetEdgeIndex[ibody][bodyEdge];
      if (!modelDuplicity_[modelEdges[modelEdge]])
        wakeNonDup_.insert(modelEdges[modelEdge]);
    }
  }

  //--------------------------------
  for ( std::size_t ibody = 0; ibody < solidBodies.size(); ibody++ )
  {
    const int bodyIndex = solidBodies[ibody];
    std::vector< EGNode<3> > nodes = bodies[bodyIndex].getNodes();

    for ( std::size_t inode = 0; inode < nodes.size(); inode++ )
    {
      // Extract all edges that reference the node
      const std::vector< EGEdge<3> > edges = bodies[bodyIndex].getEdges( nodes[inode] );

      bool wakeEdge = false;
      std::string WakeName;

      for ( std::size_t iedge = 0; iedge < edges.size(); iedge++ )
      {
        // Count how many edges connected to the node are wake nodes
        if ( edges[iedge].hasAttribute(XField3D_Wake::WAKESHEET) )
        {
          wakeEdge = true;
          edges[iedge].getAttribute(XField3D_Wake::WAKESHEET, WakeName);
          modelEdgeKutta_[edges[iedge]] = true;

          // Mark the faces connected to the edge as Kutta faces
          const std::vector< EGFace<3> > faces = bodies[bodyIndex].getFaces( edges[iedge] );
          for ( std::size_t iface = 0; iface < faces.size(); iface++ )
            modelFaceKutta_[ faces[iface] ] = true;
        }
      }

      if ( wakeEdge && modelDuplicity_[nodes[inode]] )
      {
        //std::cout << "Marking Wake Node : " << WakeName << std::endl;
        nodes[inode].addAttribute(XField3D_Wake::WAKESHEET, WakeName);
        modelEdgeKutta_[nodes[inode]] = true;
      }
    }
  }


}

//----------------------------------------------------------------------------//
bool
XField3D_Wake_avro::AvroWakeModel::modelEdgeKutta( const ego obj ) const
{
  std::map<ego,bool>::const_iterator it = modelEdgeKutta_.find(obj);
  return (it != modelEdgeKutta_.end()) && it->second;
}

//----------------------------------------------------------------------------//
bool
XField3D_Wake_avro::AvroWakeModel::modelFaceKutta( const ego obj ) const
{
  std::map<ego,bool>::const_iterator it = modelFaceKutta_.find(obj);
  return (it != modelFaceKutta_.end()) && it->second;
}

//----------------------------------------------------------------------------//
bool
XField3D_Wake_avro::AvroWakeModel::wakeNonDup( const ego obj ) const
{
  return wakeNonDup_.find(obj) != wakeNonDup_.end();
}

//---------------------------------------------------------------------------//
void
XField3D_Wake_avro::import( avro::Mesh<avro::Simplex>& mesh )
{
  timer importTimer;

  SANS_ASSERT( PhysD3::D == mesh.vertices().dim() );
  udim_ = mesh.vertices().udim();

  using avro::index_t;
  using avro::coord_t;
  using avro::real;

  std::shared_ptr<AvroWakeModel> amodel = std::static_pointer_cast<AvroWakeModel>(amodel_);

  XField_Lagrange<PhysD3> xfld(*this->comm());
  typename XField_Lagrange<PhysD3>::VectorX X;


  // extract the topology
  SANS_ASSERT (mesh.topologies().size() == 1);
  avro::local::MeshTopology<avro::Simplex> topology( mesh.vertices(), mesh.number() );

  //avro::Topology<avro::Simplex>& topology = mesh.topology(0);

#if 1
  mesh.retrieveElements( mesh.number(), topology );
  // mesh.topology(0) should hold all the elements
  SANS_ASSERT( mesh.topology(0).nb() == topology.nb() );
#else
  // insert a vertex on any edge that spans the width of the wake
  {
    avro::local::MeshTopology<avro::Simplex> mesh_topology( mesh.vertices(), mesh.number() );
    mesh.retrieveElements( mesh.number(), mesh_topology );
    SANS_ASSERT( mesh.topology(0).nb() == mesh_topology.nb() );

    mesh_topology.close();
    mesh_topology.orient();
    mesh_topology.neighbours().compute();
    mesh_topology.inverse().build();

    // get the edges in the mesh
    std::vector<index_t> edges;
    mesh_topology.getEdges(edges);

    mesh_topology.edges().compute();

    avro::local::Insert<avro::Simplex> inserter(mesh_topology);

    std::vector<index_t> shell; // shell of elements
    std::vector<real> x(mesh.number());
    index_t nb_ghost = topology.vertices().nb_ghost();
    for (index_t k = 0; k < edges.size()/2; k++)
    {
      index_t p0 = edges[2*k  ];
      index_t p1 = edges[2*k+1];

      if ( p0 < nb_ghost || p1 < nb_ghost ) continue;

      avro::Entity *e0 = mesh_topology.vertices().entity(p0);
      avro::Entity *e1 = mesh_topology.vertices().entity(p1);

      // check if both ends othe edge are on non-duplicated wake egos
      if ( e0 == nullptr || e1 == nullptr ) continue;
      if ( e0 == e1 ) continue;
      if ( e0->hasChild(e1) || e1->hasChild(e0) ) continue;
      if ( avro::BoundaryUtils::geometryFacet( mesh.vertices(), edges.data()+2*k, Line::NNode ) == nullptr ) continue;
      if ( !amodel->wakeNonDup(e0->object()) || !amodel->wakeNonDup(e1->object()) ) continue;

      // the edge connect two non-duplicated ego, split the edge
      avro::geometrics::centroid( edges.data()+2*k, 2, topology.vertices(), x );

      mesh_topology.inverse().shell( p0, p1, shell );

      mesh_topology.inverse().create(1);
      inserter.apply( p0, p1, x.data(), shell );
    }

    // identify ghost elements
    std::vector<index_t> ghost;
    for (index_t k = 0; k < mesh_topology.nb(); k++)
      if (mesh_topology.ghost(k))
        ghost.push_back(k);

    // remove ghost elements
    for (index_t k = 0; k < ghost.size(); k++)
      mesh_topology.remove( ghost[k]-k );

    // remove ghost vertices
    nb_ghost = mesh_topology.vertices().nb_ghost();
    for (index_t k = 0; k < nb_ghost; k++)
      mesh_topology.removeVertex( k );

    mesh_topology.retrieveElements( mesh.number(), topology );
  }
#endif

  topology.orient();
  double neighboursTime = 0;
  double inverseTime = 0;
  if (!topology.neighbours().computed())
  {
    timer neighboursTimer;
    topology.neighbours().fromscratch() = true;
    topology.neighbours().compute();
    neighboursTime = neighboursTimer.elapsed();
  }
  if (!topology.inverse().created())
  {
    timer inverseTimer;
    topology.inverse().build();
    inverseTime = inverseTimer.elapsed();
  }

  // get the initial vertex count without duplication
  int nVertex = mesh.vertices().nb();
  dupPointOffset_ = nVertex;

  avro::Mesh<avro::Simplex> meshDup(PhysD3::D, mesh.number());
  avro::Vertices& vertices = meshDup.vertices(); //(mesh.vertices().dim());
  mesh.vertices().copy(vertices);

  // count vertices that need duplication
  int nDup = 0;
  for (index_t k = 0; k < vertices.nb(); k++)
  {
    avro::Entity* entity = vertices.entity(k);
    if ( entity != nullptr && amodel->modelDuplicity(entity->object()) )
      nDup++;
  }

  // pre-allocate space for the duplicate vertices
  vertices.reserve(nVertex + nDup);


  // duplicated topology with modified tetrahedron
  std::shared_ptr<avro::Topology<avro::Simplex>> ptopologyDup =
               std::make_shared<avro::Topology<avro::Simplex>>( meshDup.vertices(), meshDup.number() );
  meshDup.addTopology(ptopologyDup);
  avro::Topology<avro::Simplex>& topologyDup = meshDup.topology(0); //( vertices, mesh.number() );
  mesh.retrieveElements( mesh.number(), topologyDup );

  invPointMap_.resize(nDup);
  std::vector<index_t> B; // ball of elements
  DLA::VectorS<TopoD2::D,Real> sRef = Triangle::centerRef;
  ElementXField<PhysD3, TopoD2, Triangle> elemTrace( 1, BasisFunctionCategory_Lagrange );
  const int (*TraceNodes)[ Triangle::NTrace ] = TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes;
  typename XField_Lagrange<PhysD3>::VectorX Ntri, Nface;

  enum WakeCell
  {
    Null,
    Left,
    Right,
  };
  std::vector<WakeCell> wakeCellFlags;

  std::map<int,int> newPointMap;         //Map from original points to new points

  nDup = 0;
  for (index_t ivert = 0; ivert < mesh.vertices().nb(); ivert++)
  {
    avro::Entity* entity = mesh.vertices().entity(ivert);
    if ( entity != nullptr && amodel->modelDuplicity(entity->object()) )
    {
      // track the duplicated point
      invPointMap_[nDup] = ivert;

      newPointMap[ivert] = nVertex + nDup;

      // set the duplicated vertex
      vertices.create(mesh.vertices()[ivert]);
      vertices.setEntity(nVertex + nDup, entity);
      vertices.setParam(nVertex + nDup, mesh.vertices().u(ivert));
      SANS_ASSERT( (int)vertices.nb() == nVertex + nDup +1 );

      //vertices[nVertex + nDup][2] -= 0.5; // for debugging...

      //Save off the trailing edge points to be used with the Kutta condition
      if ( amodel->modelEdgeKutta(entity->object()) )
        KuttaPoints_.push_back( nVertex + nDup );

      // get ball of elements
      topology.inverse().ball( ivert, B );
      SANS_ASSERT(B.size() > 1);
      wakeCellFlags.assign(B.size(), WakeCell::Null);

      index_t tri[Triangle::NNode];
      for (std::size_t iL = 0; iL < B.size(); iL++)
      {
        index_t iTetL = B[iL];

        for (int itrace = 0; itrace < Tet::NFace; itrace++)
        {
          int nGeom = 0;
          for (int n = 0; n < Triangle::NNode; n++ )
          {
            tri[n] = topology(iTetL, TraceNodes[itrace][n]);

            avro::Entity* entity = mesh.vertices().entity( tri[n] );
            if (entity != nullptr)
              nGeom++;
          }

          if (nGeom != Triangle::NNode) continue;
          std::vector<index_t> facet( Triangle::NNode );
          topology.neighbours().facet(iTetL,itrace,facet);

          avro::Entity* entity = avro::BoundaryUtils::geometryFacet( mesh.vertices(), tri, Triangle::NNode );
          if (entity == nullptr) continue;
          if (!amodel->modelDuplicity(entity->object())) continue;

          EGADS::EGFace<3> face( EGADS::EGObject(entity->object()) );

          SANS_ASSERT(entity->number()==2);
          //printf("SANS: (%lu,%lu,%lu), avro (%lu,%lu,%lu)\n",tri[0],tri[1],tri[2],facet[0],facet[1],facet[2]);

          //Create the face element
          for ( int node = 0; node < Triangle::NNode; node++)
            for (coord_t d = 0; d < PhysD3::D; d++)
              elemTrace.DOF(node)[d] = mesh.vertices()[tri[node]][d];

          // Ntri always points towards the left tet
          elemTrace.unitNormal( sRef, Ntri );
          elemTrace.coordinates( sRef, X );

          Nface = face.unitNormal( X );

          // find the right element in the ball
          int iTetR = topology.neighbours()(iTetL, itrace);
          if (iTetR < 0) continue;
          std::size_t iR = 0;
          for (; iR < B.size(); iR++)
            if (B[iR] == (index_t)iTetR)
              break;

          if (iR == B.size()) continue; // skip if the element isn't in the ball

          std::size_t jL = iL;
          std::size_t jR = iR;
          if ( dot(Ntri, Nface) < 0 ) // Negative Ntri dot Nface means left tet is on left side of the face
            std::swap(jL, jR);

          // Tag all Tets as left or right now that they have been sorted
          wakeCellFlags[jL] = WakeCell::Left;
          wakeCellFlags[jR] = WakeCell::Right;
        }
      }

      // keep updating WakeCell::Null flags until none are left
      bool hasNull = true;
      while (hasNull)
      {
        hasNull = false;

        // finalize the null tets based on their neighbor
        // as test that share a trace on the wake have already been marked
        // this loop cannot reach across a wake
        for (std::size_t iL = 0; iL < wakeCellFlags.size(); iL++)
        {
          if (wakeCellFlags[iL] == WakeCell::Null)
          {
            hasNull = true;
            index_t iTetL = B[iL];
            for (int itrace = 0; itrace < Tet::NFace; itrace++)
            {
              // get the neighboring element
              int iTetR = topology.neighbours()(iTetL, itrace);
              if (iTetR < 0) continue;

              // look for it in the ball
              std::size_t iR = 0;
              for (; iR < B.size(); iR++)
                if (B[iR] == (index_t)iTetR)
                  break;

              // check if the neighbor is in the ball and is not null
              if (iR == B.size()) continue;
              if (wakeCellFlags[iR] == WakeCell::Null) continue;

              // update the flag
              wakeCellFlags[iL] = wakeCellFlags[iR];
              break;
            }
          }
        }
      }

      // update the connectivity with the new node
      for (std::size_t i = 0; i < B.size(); i++)
      {
        SANS_ASSERT(wakeCellFlags[i] != WakeCell::Null);

        if (wakeCellFlags[i] == WakeCell::Right)
        {
          index_t iTetR = B[i];
#if 0 // for debugging
          index_t simplex[Tet::NNode];
          for (index_t j = 0; j < topologyDup.nv(iTetR); j++)
            simplex[j] = topologyDup(iTetR, j);
#endif
          for (index_t j = 0; j < topologyDup.nv(iTetR); j++)
          {
            if (topologyDup(iTetR, j) == ivert)
            {
              topologyDup(iTetR, j) = nVertex + nDup;
              break;
            }
          }
        }
      }

      nDup++;
    } // if duplicate
  }
  KuttaPoints_.shrink_to_fit();
  topologyDup.orient();

  //avro::writeTecplot( meshDup, "tmp/avroWakeDup.dat" );

  //---------------------------------------------//
  xfld.sizeDOF( vertices.nb() );

  // add the original points
  for (index_t k = 0; k < vertices.nb(); k++)
  {
    for (coord_t d = 0; d < vertices.dim(); d++)
      X[d] = vertices[k][d];
    xfld.addDOF( X );
  }


  //---------------------------------------------//
  xfld.sizeCells( topologyDup.nb() );

  // read the simplices
  int order = 1;
  int cellgroup = 0;
  std::vector<int> simplex( mesh.number()+1 );

  if (this->comm_->rank() == 0)
  {
    for (index_t k = 0; k < topologyDup.nb(); k++)
    {
      for (index_t j = 0; j < topologyDup.nv(k); j++)
        simplex[j] = topologyDup(k, j);

      xfld.addCell( cellgroup, eTet, order, simplex );
    }
  }

  //---------------------------------------------//
  // get the boundary groups
  avro::Boundary<avro::Simplex> bnd( topologyDup );
  double boundaryTime = 0;
  {
    timer boundaryTimer;
    bnd.extract(true);
    boundaryTime = boundaryTimer.elapsed();
  }


  // count how many d-1 boundary elements there are
  int nbnd = 0;
  int nBndGroup = 0;
  for (index_t k = 0; k < bnd.nb_children(); k++)
  {
    if (bnd.child(k)->number() == topologyDup.number()-1)
    {
      nbnd += bnd.child(k)->nb();
      nBndGroup++;
    }
  }
  xfld.sizeBoundaryTrace(nbnd);

  // construct a map to duplicated boundary groups
  std::map<int,int> wakeDupGroup;
  for (index_t k = 0; k < bnd.nb_children(); k++)
  {
    if (bnd.child(k)->number() == topologyDup.number()-1)
    {
      // determine the entity this boundary is on
      avro::Entity* entity = bnd.entity(k);
      if ( amodel->modelDuplicity(entity->object()) )
      {
        // retrieve the boundary group of the associated ego
        int group = bndGroupMap_.at( this->label(entity) );

        int nWakeDup = wakeDupGroup.size();

        // account for extra temporary boundary group for duplicated wake sheets
        wakeDupGroup[group] = nBndGroup + nWakeDup;
      }
    }
  }

  if (this->comm_->rank() == 0)
  {
    {
      // check all vertices on geometries are accounted for in the boundary topology
      std::vector<bool> accounted( vertices.nb() , false );
      avro::Topology<avro::Simplex> bnd_topology( bnd.vertices(), mesh.number()-1 );
      bnd.retrieveElements( mesh.number()-1, bnd_topology );
      for (index_t k = 0; k < vertices.nb(); k++)
      {
        if (vertices.entity(k) == NULL)
          accounted[k] = true;
      }

      for (index_t k = 0; k < bnd_topology.nb(); k++)
      {
        for (index_t j = 0; j < bnd_topology.nv(k); j++)
          accounted[ bnd_topology(k,j) ] = true;
      }

      // there should be no vertices leftover that were not accounted for
      index_t nerr = 0;
      for (index_t k = 0; k < accounted.size(); k++)
      {
        if (vertices.entity(k) != NULL && !accounted[k])
        {
          printf("error! vertex %lu is tagged on geometry!\n",k);
          vertices.print(k,true);
          nerr++;
        }
      }

#if 0
  {
    avro::library::Plottable<avro::Simplex> plot( bnd );
    avro::Server server;
    avro::Plotter* plotter = new avro::Plotter(&server);
    __plotter__ = (void*) plotter;
    plotter->addPlot(plot);
    plotter->run();
    delete plotter;
    __plotter__ = nullptr;
  }
#endif
      SANS_ASSERT_MSG(nerr==0, "nerr=%d", nerr);
      printf("incoming avro mesh is clean!\n");
    }

    // Get the frame node map for the segment
    const int (*FrameNodes)[ Line::NTrace ] = TraceToCellRefCoord<Line, TopoD2, Triangle>::TraceNodes;

    std::vector<int> tri( mesh.number() );
    index_t line[Line::NNode];
    int lineL[Line::NNode];
    std::map<ego,int> KuttaLineCount;
    std::map<ego,int> TrefftzLineCount;
    std::map<UniqueElem,int> KuttaLine;
    for (index_t k = 0; k < bnd.nb_children(); k++)
    {
      if (bnd.child(k)->number() != topologyDup.number()-1) continue;

      avro::Topology<avro::Simplex>& bndk = *bnd.child(k);
      SANS_ASSERT(bndk.number() == 2);

      // determine the entity this boundary is on
      avro::Entity* entity = bnd.entity(k);
      if ( !amodel->modelDuplicity(entity->object()) ) continue;

      for (index_t j = 0; j < bndk.nb(); j++)
      {
        // retrieve the facet indices
        for (index_t i = 0; i < bndk.nv(j); i++)
          tri[i] = bndk(j,i);

        // Count segments that are on Kutta or Trefftz edges.
        for (int seg = 0; seg < Triangle::NEdge; seg ++)
        {
          line[0] = tri[FrameNodes[seg][0]];
          line[1] = tri[FrameNodes[seg][1]];

          // only consider lines from the original topology
          if ( (int)line[0] >= dupPointOffset_ ||
               (int)line[1] >= dupPointOffset_ ) continue;

          avro::Entity* entity = avro::BoundaryUtils::geometryFacet( vertices, line, Line::NNode );
          if (entity == nullptr) continue;
          if (entity->objectClass() != EDGE) continue;

          EGADS::EGObject obj(entity->object());

          if ( obj.hasAttribute(XField3D_Wake::WAKESHEET) )
          {
            KuttaLine[UniqueElem(line[0],line[1])] = KuttaLineCount[obj];
            KuttaLineCount[obj] += 1;
          }
          else if ( obj.hasAttribute(XField3D_Wake::TREFFTZ) )
            TrefftzLineCount[obj] += 1;
        }
      }
    }

    //Create the boundary association constructors
    std::vector< FieldFrameGroupType::FieldAssociativityConstructorType >
        fldAssocBframe(amodel->frameGroupKutta_size() + amodel->frameGroupTrefftz_size());

    for ( std::pair<const ego,int>& count : KuttaLineCount )
    {
      fldAssocBframe[amodel->frameGroupKutta(count.first)].resize(BasisFunctionLineBase::HierarchicalP1, count.second);
      count.second = 0;
    }

    std::cout << "Trefftz Frames (" << TrefftzLineCount.size() << ") : ";
    for ( std::pair<const ego,int>& count : TrefftzLineCount )
    {
      std::cout << amodel->frameGroupTrefftz(count.first) << " ";
      fldAssocBframe[amodel->frameGroupTrefftz(count.first)].resize(BasisFunctionLineBase::HierarchicalP1, count.second);
      count.second = 0;
    }
    std::cout << std::endl;


    // used to connect the top and bottom wake BC groups
    std::vector<PeriodicBCNodeMap> wakePeriodicity;

    int group = 0;
    for (index_t k = 0; k < bnd.nb_children(); k++)
    {
      if (bnd.child(k)->number() != topologyDup.number()-1) continue;

      avro::Topology<avro::Simplex>& bndk = *bnd.child(k);
      SANS_ASSERT(bndk.number() == 2);

      // determine the entity this boundary is on
      avro::Entity* entity = bnd.entity(k);

      // retrieve the boundary group of the associated ego
      group = bndGroupMap_.at( this->label(entity) );

      // the face is duplicated, split it into two groups
      if ( amodel->modelDuplicity(entity->object()) )
      {
        wakePeriodicity.emplace_back(group, wakeDupGroup[group]);
        PeriodicBCNodeMap& period = wakePeriodicity.back();

        // create the regular boundary group
        for (index_t itrace = 0; itrace < bndk.nb(); itrace++)
        {
          bool isDup = false;

          // retrieve the facet indices
          for (index_t i = 0; i < bndk.nv(itrace); i++)
          {
            tri[i] = bndk(itrace,i);
            if ( tri[i] >= dupPointOffset_ )
              isDup = true;
          }

          // even non-duplicated points need to be added to the perodicity
          if (isDup)
            for (index_t i = 0; i < bndk.nv(itrace); i++)
            {
              if ( tri[i] >= dupPointOffset_ ) period.mapLtoR[invPointMap_[tri[i] - dupPointOffset_]] = tri[i];
              else                             period.mapLtoR[tri[i]                                ] = tri[i];
            }

          // add the triangle to the appropriate group
          if (isDup) xfld.addBoundaryTrace( wakeDupGroup[group], eTriangle, tri);
          else       xfld.addBoundaryTrace(               group, eTriangle, tri);
        }
      }
      else
      {

        // create the regular boundary group
        for (index_t itrace = 0; itrace < bndk.nb(); itrace++)
        {

          // retrieve the facet indices
          for (index_t i = 0; i < bndk.nv(itrace); i++)
            tri[i] = bndk(itrace,i);

          xfld.addBoundaryTrace(group, eTriangle, tri);

          // Only consider faces attached to kutta edges
          if ( !amodel->modelFaceKutta(entity->object()) ) continue;

          // Mark all trianges on the wing attached to trailing edge for Kiutta BC
          for (int n = 0; n < Triangle::NNode; n++)
          {
            bool isDup = false;
            avro::index_t v = tri[n];
            if ( (int)v >= dupPointOffset_ )
            {
              v = invPointMap_[v - dupPointOffset_];
              isDup = true;
            }

            avro::Entity* entity = vertices.entity(v);
            SANS_ASSERT(entity != nullptr);

            EGADS::EGObject obj(entity->object());
            if ( !((entity->objectClass() == EDGE || entity->objectClass() == NODE) &&
                   obj.hasAttribute(XField3D_Wake::WAKESHEET)) ) continue;

            if ( isDup )
            {
              KuttaBCTraceElemRight_[group][tri[n]].KuttaPoint = tri[n];
              KuttaBCTraceElemRight_[group][tri[n]].elems.push_back(itrace);
            }
            else
            {
              KuttaBCTraceElemLeft_[group][tri[n]].KuttaPoint = newPointMap.at(tri[n]);
              KuttaBCTraceElemLeft_[group][tri[n]].elems.push_back(itrace);
            }
          }
        }
      }

      // Trefftz lines attached to wakes
      if ( amodel->modelDuplicity(entity->object()) )
      {
        int iTraceElem = 0;
        for (index_t itrace = 0; itrace < bndk.nb(); itrace++)
        {
          // retrieve the facet indices
          for (index_t i = 0; i < bndk.nv(itrace); i++)
            tri[i] = bndk(itrace,i);

          // Look for line elements on Trefftz edges.
          for (int seg = 0; seg < Triangle::NEdge; seg ++)
          {
            lineL[0] = line[0] = tri[FrameNodes[seg][0]];
            lineL[1] = line[1] = tri[FrameNodes[seg][1]];

            // only consider lines from the original topology
            if ( lineL[0] >= dupPointOffset_ ||
                 lineL[1] >= dupPointOffset_ ) continue;

            avro::Entity* entity = avro::BoundaryUtils::geometryFacet( vertices, line, Line::NNode );
            if (entity == nullptr) continue;

            EGADS::EGObject obj(entity->object());
            if ( !obj.hasAttribute(XField3D_Wake::TREFFTZ) ) continue;

            // The canonical Frame is the seg number and always left so orientation is 1
            CanonicalTraceToCell canonicalFrameL(seg, 1);
            const int BCframe = amodel->frameGroupTrefftz(obj);
            const int elemLine = TrefftzLineCount.at(obj);

            // Get the nodes with the triangle with the updated nodes
            fldAssocBframe[BCframe].setGroupLeft( group );
            fldAssocBframe[BCframe].setElementLeft( iTraceElem, elemLine );

            fldAssocBframe[BCframe].setCanonicalTraceLeft( canonicalFrameL, elemLine );

            fldAssocBframe[BCframe].setAssociativity( elemLine ).setRank( 0 );

            //Set the nodes for the line
            fldAssocBframe[BCframe].setAssociativity( elemLine ).setNodeGlobalMapping( lineL, Line::NNode );

            TrefftzLineCount[obj] += 1;
            iTraceElem++;
          }
        }
      }

      // Face attached to Kutta lines
      if ( amodel->modelFaceKutta(entity->object()) )
      {
        for (index_t itrace = 0; itrace < bndk.nb(); itrace++)
        {
          // retrieve the facet indices
          for (index_t i = 0; i < bndk.nv(itrace); i++)
            tri[i] = bndk(itrace,i);

          // Count segments that are on Kutta or Trefftz edges.
          for (int seg = 0; seg < Triangle::NEdge; seg ++)
          {
            lineL[0] = line[0] = tri[FrameNodes[seg][0]];
            lineL[1] = line[1] = tri[FrameNodes[seg][1]];

            avro::Entity* entity = avro::BoundaryUtils::geometryFacet( vertices, line, Line::NNode );
            if (entity == nullptr) continue;

            EGADS::EGObject obj(entity->object());
            if ( !obj.hasAttribute(XField3D_Wake::WAKESHEET) ) continue;

            const int BCframe = amodel->frameGroupKutta(obj);

            // connect right triangle if duplicated topology
            if ( lineL[0] >= dupPointOffset_ ||
                 lineL[1] >= dupPointOffset_ )
            {
              CanonicalTraceToCell canonicalFrameR = TraceToCellRefCoord<Line, TopoD2, Triangle>::
                                                       getCanonicalTrace(lineL, Line::NNode, tri.data(), tri.size());

              if ( lineL[0] >= dupPointOffset_ ) lineL[0] = invPointMap_[lineL[0] - dupPointOffset_];
              if ( lineL[1] >= dupPointOffset_ ) lineL[1] = invPointMap_[lineL[1] - dupPointOffset_];

              int elemLine = KuttaLine.at(UniqueElem(lineL[0],lineL[1]));

              fldAssocBframe[BCframe].setGroupRight( group );

              fldAssocBframe[BCframe].setElementRight( itrace, elemLine );
              fldAssocBframe[BCframe].setCanonicalTraceRight( canonicalFrameR, elemLine );
              continue;
            }

            // set the left element for non-duplicated topology
            const int elemLine = KuttaLine.at(UniqueElem(line[0],line[1]));

            fldAssocBframe[BCframe].setAssociativity( elemLine ).setRank( 0 );

            //Set the nodes for the line
            fldAssocBframe[BCframe].setAssociativity( elemLine ).setNodeGlobalMapping( lineL, Line::NNode );

            // The canonical Frame is the seg number and always left so orientation is 1
            CanonicalTraceToCell canonicalFrameL(seg,1);

            fldAssocBframe[BCframe].setGroupLeft( group );
            fldAssocBframe[BCframe].setElementLeft( itrace, elemLine );

            fldAssocBframe[BCframe].setCanonicalTraceLeft( canonicalFrameL, elemLine );
          }
        }
      }

      printf("-> imported boundary group %d\n",group);
    }

    resizeBoundaryFrameGroups( fldAssocBframe.size() );

    // Create the boundary frame groups
    for ( std::size_t BCframe = 0; BCframe < fldAssocBframe.size(); BCframe++ )
      boundaryFrameGroups_[BCframe] = new FieldFrameGroupType( fldAssocBframe[BCframe] );

    // add the boundary periodicity for the wakes
    xfld.addBoundaryPeriodicity(wakePeriodicity);
  }

  // construct the grid
  this->buildFrom(xfld);

  // Set DOFs after it has been allocated
  for ( int BCframe = 0; BCframe < boundaryFrameGroups_.size(); BCframe++ )
    boundaryFrameGroups_[BCframe]->setDOF(DOF_, nDOF_);

  vertexOnGeometry_.resize( meshDup.vertices().nb() );
  paramOnGeometry_.resize( udim_*meshDup.vertices().nb() );
  for (index_t n = 0; n < meshDup.vertices().nb(); n++)
  {
    avro::Entity* entity = meshDup.vertices().entity(n);
    vertexOnGeometry_[n] = this->label( entity );

    for (coord_t d = 0; d < udim_; d++)
      paramOnGeometry_[n*udim_+d] = meshDup.vertices().u(n,d);
  }

  std::cout << "Import time: " << importTimer.elapsed() << " s" << std::endl;
  std::cout << " Neighbours: " << neighboursTime << " s" << std::endl;
  std::cout << "    Inverse: " << inverseTime << " s" << std::endl;
  std::cout << "   Boundary: " << boundaryTime << " s" << std::endl;

}

//----------------------------------------------------------------------------//
class addWakeCellGroupTopology : public GroupFunctorCellType< addWakeCellGroupTopology >
{
public:
  addWakeCellGroupTopology( const XField3D_Wake_avro& xfld,
                            avro::Topology<avro::Simplex>& topology,
                            const int nCellGroups )
    : xfld_(xfld), topology_(topology), nCellGroups_(nCellGroups) {}

  std::size_t nCellGroups() const          { return nCellGroups_; }
  std::size_t cellGroup(const int n) const { return n;            }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology >
  void
  apply(const typename XField<PhysD3,TopoD3>::template FieldCellGroupType<Topology>& xfldCell,
        const int cellGroupGlobal)
  {
    SANS_ASSERT( topology_.number() == TopoD3::D );
    using avro::index_t;

    int nElem = xfldCell.nElem();
    int nBasis = xfldCell.nBasis();
    SANS_ASSERT( nBasis == TopoD3::D+1 );

    std::vector<int> nodeDOFmap( nBasis );
    std::vector<index_t> simplex( nBasis );

    for (int elem=0;elem<nElem;elem++)
    {
      // get the global element indices
      xfldCell.associativity(elem).getGlobalMapping(nodeDOFmap.data(), nodeDOFmap.size() );
      for (int i = 0; i < nBasis; i++)
      {
        if (nodeDOFmap[i] >= xfld_.dupPointOffset_)
          simplex[i] = xfld_.invPointMap_[ nodeDOFmap[i] - xfld_.dupPointOffset_ ];
        else
          simplex[i] = nodeDOFmap[i];
      }

      // add the simplex to the topology
      topology_.add( simplex.data() , simplex.size() );
    }

  }

private:
  const XField3D_Wake_avro& xfld_;
  avro::Topology<avro::Simplex>& topology_;
  int nCellGroups_;
};

//---------------------------------------------------------------------------//
void
XField3D_Wake_avro::convert( avro::Mesh<avro::Simplex>& mesh ) const
{
  using avro::index_t;
  using avro::coord_t;

  // all processors can clear the mesh
  mesh.clear();

  const coord_t dim = mesh.vertices().dim();

  std::vector<Real> x( PhysD3::D );
  SANS_ASSERT( dim == PhysD3::D );
  SANS_ASSERT( int(vertexOnGeometry_.size()) == this->nDOF() );

  std::vector<Real> x0(dim,0.0);
  mesh.vertices().reserve(dupPointOffset_);
  for (int k = 0; k < dupPointOffset_; k++)
    mesh.vertices().create(x0.data());

  // create the vertices
  for (index_t k = 0; k < index_t(dupPointOffset_); k++)
  {
    // set the coordinates
    for (coord_t d = 0; d < mesh.vertices().dim(); d++)
      mesh.vertices()[k][d] = this->DOF(k)[d];

    // lookup the geometric entity and attach it to the vertex
    if (vertexOnGeometry_[k] >= 0)
    {
      avro::Entity* entity = amodel_->lookup( amodel_->geometry()[vertexOnGeometry_[k]] );
      mesh.vertices().setEntity( k , entity );
      mesh.vertices().setParam( k , paramOnGeometry(k) );
    }
    else
      mesh.vertices().setEntity( k , NULL ); // volume point
  }

  // create the simplices
  smart_ptr(avro::Topology<avro::Simplex>) topology
                = smart_new(avro::Topology<avro::Simplex>)
                                              (mesh.vertices(),mesh.number());
  mesh.addTopology(topology);

  // loop over cell groups and write out the connectivity
  int nCellGroup = this->nCellGroups();
  for_each_CellGroup<TopoDim>::apply( addWakeCellGroupTopology(*this,*smart_raw(topology), nCellGroup), *this );
}

template class XFieldBase_avro< XField3D_Wake >;

} // SANS
