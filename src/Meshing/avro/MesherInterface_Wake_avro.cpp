// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <iostream>
#include <sstream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "Topology/Dimension.h"
#include "Topology/ElementTopology.h"

#include "MesherInterface_Wake_avro.h"

#include "XField3D_Wake_avro.h"

#include "Meshing/libMeshb/WriteSolution_libMeshb.h"
#include "Meshing/Metric/MetricOps.h"

#include "Field/XFieldVolume.h"

#include "Field/FieldVolume_CG_Cell.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#include <avro.h>
#include <mesh/boundary.h>
#include <mesh/tecplot.h>
#include <library/mesh.h>
#include <graphics/plotter.h>

#ifdef SANS_MPI
#include "MPI/serialize_DenseLinAlg_MatrixS.h"

#include <boost/mpi/collectives/all_reduce.hpp>
#include <boost/mpi/collectives/gather.hpp>
#include <boost/serialization/map.hpp>
#endif

namespace SANS
{

// cppcheck-suppress passedByValue
void avroWakeParams::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.FilenameBase));
  allParams.push_back(d.checkInputs(params.DisableCall));
  d.checkUnknownInputs(allParams);
}
avroWakeParams avroWakeParams::params;

MesherInterface<PhysD3, TopoD3, avroWakeMesher>::MesherInterface(
  int adapt_iter, const PyDict& paramsDict ) :
  adapt_iter_(adapt_iter),
  paramsDict_(paramsDict)
{}

std::shared_ptr<XField3D_Wake>
MesherInterface<PhysD3, TopoD3, avroWakeMesher>::
adapt( const Field_CG_Cell<PhysD3,TopoD3,MatrixSym>& metric_request ,
       const XField3D_Wake& xfld_in ) const
{
  SANS_ASSERT(xfld_in.derivedTypeID() == typeid(XField3D_Wake_avro));
  SANS_ASSERT(&xfld_in == &metric_request.getXField());

  const XField3D_Wake_avro& xfld = static_cast<const XField3D_Wake_avro&>(xfld_in);

  using avro::index_t;
  using avro::real;

  // create the output xfld
  std::shared_ptr< XField3D_Wake_avro > xfld_out;
  xfld_out = std::make_shared< XField3D_Wake_avro>(xfld, XFieldEmpty());

  /// create the mesh avro will use and overwrite
  avro::Mesh<avro::Simplex> avro_mesh( D, D );

  // create the list of nodal metrics
  int nDOF = xfld.dupPointOffset_;

  // size the metric field for avro
  avro::VertexField< avro::numerics::SPDT<Real> > fld;

  // place dummy metrics into the field according to the number of dof
  for (int k = 0; k < nDOF; k++)
    fld.add( avro::numerics::SPDT<real>(D) );

  for (int k = 0; k < nDOF; k++)
  {
    const MatrixSym& m = metric_request.DOF(k);
    for (int i = 0; i < D; i++)
      for (int j = 0; j <= i; j++)
        fld[k](i,j) = m(i,j);
  }

  // take the average of the metric on the wakes
  for (int kR = xfld.dupPointOffset_; kR < metric_request.nDOF(); kR++)
  {
    int kL = xfld.invPointMap_[ kR - xfld.dupPointOffset_ ];
    const MatrixSym& mL = metric_request.DOF(kL);
    const MatrixSym& mR = metric_request.DOF(kR);

    MatrixSym m = Metric::computeAffineInvInterp(mL, mR, 0.5);
    for (int i = 0; i < D; i++)
      for (int j = 0; j <= i; j++)
        fld[kL](i,j) = m(i,j);
  }

#if 0
  {
    SANS_ASSERT(avro_mesh.topologies().size() == 1);
    avro::Boundary<avro::Simplex> bnd(avro_mesh.topology(0));
    bnd.extract(true);

    avro::library::Plottable<avro::Simplex> plot( bnd );
    avro::Server server;
    avro::Plotter* plotter = new avro::Plotter(&server);
    __plotter__ = (void*) plotter;
    plotter->addPlot(plot);
    plotter->run();
    delete plotter;
  }
#endif

  // setup some parameters
  avro::AdaptationParameters params;
  std::string filename_base = paramsDict_.get(avroWakeParams::params.FilenameBase);
  params.directory() = filename_base;
  params.adapt_iter() = adapt_iter_;
  params.curved() = true;
  params.boundarySubdirectory() = ".";
  params.insertion_volume_factor() = std::sqrt(2.0);
  params.write_mesh() = paramsDict_.get(avroWakeParams::params.WriteMesh);
  params.write_conformity() = paramsDict_.get(avroWakeParams::params.WriteConformity);
  params.has_interior_boundaries() = true;
  params.has_uv() = true;

  // convert the XField to a mesh
  params.prepared() = false;
  xfld.convert( avro_mesh );

  // create a dummy mesh until the avro interface is removed
  avro::Mesh<avro::Simplex> meshDummy(D,D);
  const int TopoD = TopoD3::D;
  meshDummy.addTopology(std::make_shared<avro::Topology<avro::Simplex>>(meshDummy.vertices(),TopoD));

  avro::AdaptationProblem<avro::Simplex> problem = {avro_mesh,fld,params,meshDummy};

  // adapt the mesh
  int result = 0;
  try
  {
    result = avro::adapt(problem);
  }
  catch (const std::exception& e)
  {
    SANS_DEVELOPER_EXCEPTION(e.what());
  }
  SANS_ASSERT_MSG( result==AVRO_SUCCESS || result==AVRO_NOT_CONFORMING, "error in avro!\n" );
  if (result==AVRO_NOT_CONFORMING)
    printf("avro was not happy about conformity...\n");

  //avro::writeTecplot( avro_mesh, "tmp/avroWake.dat" );

  // TODO: Gamma writer uses bodyIndex which does not work with wakes!
  //avro::Gamma<avro::Simplex> gamma;
  //gamma.writeMesh( avro_mesh , "tmp/avroWake.mesh" );

#if 0
  {
    SANS_ASSERT(avro_mesh.topologies().size() == 1);
    avro::Boundary<avro::Simplex> bnd(avro_mesh.topology(0));
    bnd.extract(true);

    avro::library::Plottable<avro::Simplex> plot( bnd );
    avro::Server server;
    avro::Plotter* plotter = new avro::Plotter(&server);
    __plotter__ = (void*) plotter;
    plotter->addPlot(plot);
    plotter->run();
    delete plotter;
  }
#endif

  // store the mesh into the output XField
  xfld_out->import( avro_mesh );

  return xfld_out;
}

}
