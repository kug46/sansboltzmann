// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD_AVRO_H
#define XFIELD_AVRO_H

#ifndef SANS_AVRO
#error "XField_avro.h should only be included if SANS_AVRO is defined"
#endif

#include <vector>
#include <memory> //shared_ptr

#include "MPI/communicator_fwd.h"

#include "Field/XField.h"

// forward declaration of ego
struct egObject;
typedef egObject* ego;

namespace avro
{
template<typename type> class Mesh;
class Simplex;
class Model;
class Body;
class Entity;
}
#include <avro.h>

namespace SANS
{

class AvroModel : public avro::Model
{
public:
  AvroModel() : nbnd_(0) {}
  AvroModel(const AvroModel& amodel) : avro::Model(amodel), geometry_(amodel.geometry_), nbnd_(amodel.nbnd_) {}
  AvroModel( const avro::Model& amodel ) : avro::Model(amodel), nbnd_(0) {}
  virtual ~AvroModel() {}

  template<class PhysDim>
  void initGeometry();

  virtual const std::type_info& derivedTypeID() const { return typeid(AvroModel); }

  // the geometry vector cannot change
  const std::vector<ego>& geometry() const { return geometry_; }
  int nBoundary() const { return nbnd_; }
protected:
  std::vector<ego> geometry_;
  int nbnd_; // number of boundary entities in the geometry (edges in 2D, faces in 3D)
};

template< class BaseType >
class XFieldBase_avro : public BaseType
{
public:
  XFieldBase_avro( mpi::communicator& comm, const std::shared_ptr<avro::Model>& amodel );
  XFieldBase_avro( const std::shared_ptr<mpi::communicator> comm, const std::shared_ptr<avro::Model>& amodel );

  virtual ~XFieldBase_avro() {}

  std::vector<int>& vertexOnGeometry() { return vertexOnGeometry_; }

  // functions to go back and forth between ego/integer labeling
  ego object( const int k ) const;
  int label( ego object ) const;

  const std::shared_ptr<AvroModel>& model() const { return amodel_; }
  int label( avro::Entity* entity ) const;

  const std::map<int,int>& getBndGroupMap() const { return bndGroupMap_; }

protected:
  XFieldBase_avro( const std::shared_ptr<AvroModel>& amodel );
  XFieldBase_avro( std::shared_ptr<mpi::communicator> comm, const std::shared_ptr<AvroModel>& amodel );

  // constructs the ego to boundary group map
  void mapBoundaryGroups(const avro::Vertices& vertices);

  const std::shared_ptr<AvroModel> amodel_;

  std::vector<int> vertexOnGeometry_;

  avro::coord_t udim_;
  std::vector<avro::real> paramOnGeometry_;

  const avro::real* paramOnGeometry(avro::index_t k) const { return paramOnGeometry_.data() + udim_*k; }

  // maps ego's in geometry_ to XField boundary group
  std::map<int,int> bndGroupMap_;
};

class XFieldEmpty {};

template< class PhysDim, class TopoDim >
class XField_avro : public XFieldBase_avro< XField<PhysDim,TopoDim> >
{
public:
  typedef XFieldBase_avro< XField<PhysDim,TopoDim> > BaseType;

  XField_avro( const XField_avro& xfld, const XFieldEmpty);

  XField_avro( mpi::communicator& comm, const std::shared_ptr<avro::Model>& amodel);
  XField_avro( const XField<PhysDim,TopoDim>& xfld, const std::shared_ptr<avro::Model>& _amodel );

  void import( avro::Mesh<avro::Simplex>& mesh );
  void convert( avro::Mesh<avro::Simplex>& mesh ) const;

  // use inverse evaluation to attach the XField to the avro::Model
  void attachToGeometry();

  virtual ~XField_avro() {}

  void fill( const std::vector<double>& params = {0.025,0.001,15.} );

  virtual const std::type_info& derivedTypeID() const { return typeid(XField_avro); }

protected:
  void importVertexOnGeometry(const XField_Lagrange<PhysDim>& xfld, avro::Vertices& vertices);

  using BaseType::amodel_;

  using BaseType::vertexOnGeometry_;
  using BaseType::udim_;
  using BaseType::paramOnGeometry_;
  using BaseType::bndGroupMap_;
};

} // SANS

#endif // XFIELD_AVRO_H
