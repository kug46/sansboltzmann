// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef MESHERINTERFACE_WAKE_AVRO_H_
#define MESHERINTERFACE_WAKE_AVRO_H_

//Python must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "Field/XField3D_Wake.h"

namespace SANS
{

template<class PhysDim,class TopoDim> class XField_avro;

template <class PhysDim, class TopoDim, class Mesher>
class MesherInterface;

class avroWakeMesher;

struct avroWakeParams : noncopyable
{
  const ParameterString FilenameBase{"FilenameBase", "tmp/", "Default filepath prefix for generated files"};
  const ParameterBool DisableCall{"DisableCall", false, "Disable avroMesher call for debugging?"};
  const ParameterBool WriteMesh{"WriteMesh",false,"option to ask avro to write the output mesh"};
  const ParameterBool WriteConformity{"WriteConformity",false,"option to ask avro to write the output mesh-metric conformity"};
  const ParameterBool HasInteriorBoundaries{"HasInteriorBoundaries",false,"whether avro should extract interior boundary groups (used for wakes)"};

  static void checkInputs(PyDict d);
  static avroWakeParams params;
};

//Class for transferring meshes and metric requests between SANS and avro for full potential with Wakes

template <>
class MesherInterface<PhysD3, TopoD3, avroWakeMesher>
{
public:
  static const int D = PhysD3::D;
  typedef DLA::MatrixSymS<D,Real> MatrixSym;

  MesherInterface(int adapt_iter, const PyDict& paramsDict);

  std::shared_ptr<XField3D_Wake>
  adapt( const Field_CG_Cell<PhysD3,TopoD3,MatrixSym>& metric_request,
         const XField3D_Wake& xfld ) const;

protected:
  int adapt_iter_;
  const PyDict paramsDict_;
};

}

#endif /* MESHERINTERFACE_WAKE_AVRO_H_ */
