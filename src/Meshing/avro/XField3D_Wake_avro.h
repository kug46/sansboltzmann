// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifdef SANS_AVRO

#ifndef XFIELD_AVROWAKE_H_
#define XFIELD_AVROWAKE_H_

#include "Field/XField3D_Wake.h"

#include "Meshing/avro/XField_avro.h"

#include <unordered_set>

namespace SANS
{

namespace EGADS
{
class EGTessModel;
}

class XField3D_Wake_avro : public XFieldBase_avro< XField3D_Wake >
{
public:
  typedef XFieldBase_avro< XField3D_Wake > BaseType;

protected:
  class AvroWakeModel : public AvroModel
  {
  public:
    AvroWakeModel( const EGADS::EGTessModel& model );
    virtual ~AvroWakeModel() { delete this->context_; } // free leaking memory from avro

    virtual const std::type_info& derivedTypeID() const { return typeid(AvroWakeModel); }

    bool modelDuplicity( const ego obj ) const { return modelDuplicity_.at(obj); }
    bool modelEdgeKutta( const ego obj ) const;
    bool modelFaceKutta( const ego obj ) const;

    bool wakeNonDup( const ego obj ) const;

    int frameGroupKutta  ( const ego obj ) const { return frameGroupKutta_.at(obj);   }
    int frameGroupTrefftz( const ego obj ) const { return frameGroupTrefftz_.at(obj); }

    std::size_t frameGroupKutta_size  () const { return frameGroupKutta_.size();   }
    std::size_t frameGroupTrefftz_size() const { return frameGroupTrefftz_.size(); }
  protected:
    void initDuplicityMaps( const EGADS::EGTessModel& tessModel );

    std::map<ego,bool> modelEdgeKutta_;
    std::map<ego,bool> modelFaceKutta_;
    std::map<ego,bool> modelDuplicity_;

    std::unordered_set<ego> wakeNonDup_;

    std::map<ego,int> frameGroupKutta_, frameGroupTrefftz_;
  };

public:
  XField3D_Wake_avro( const EGADS::EGTessModel& tessModel );
  XField3D_Wake_avro( const XField3D_Wake_avro& xfld, const XFieldEmpty);

  void import( avro::Mesh<avro::Simplex>& mesh );
  void convert( avro::Mesh<avro::Simplex>& mesh ) const;

  virtual const std::type_info& derivedTypeID() const { return typeid(XField3D_Wake_avro); }

protected:
  using BaseType::vertexOnGeometry_;
  using BaseType::amodel_;
};

} // SANS

#endif // XFIELD_AVROWAKE_H_

#endif // SANS_AVRO
