// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD_PX_H
#define XFIELD_PX_H

/*\
 *
 *  XField class used to reading ProjectX grm, and gri grid files
 *
\*/

#include "Field/XField.h"

#include <string>

namespace SANS
{

template<class PhysDim>
class XField_Lagrange;

template<class PhysDim, class TopoDim>
class XField_PX : public XField< PhysDim, TopoDim>
{
public:
  // constructors from a file
  XField_PX( mpi::communicator& comm, const std::string& filename );
  XField_PX(XField<PhysDim,TopoDim>& xfld, XFieldBalance graph, const std::string& filename);

  // writer functions
  void writeTo(const std::string& filename);

protected:

  // reader functions
  void readGrm( const std::string& filename, XField_Lagrange<PhysDim>& xfldin);
  void readGri( const std::string& filename, XField_Lagrange<PhysDim>& xfldin);

  // read the DOFs from a file
  void readDOFs( std::ifstream& input, const int nDOF, const int dim, XField_Lagrange<PhysDim>& xfldin );
};

template<class PhysDim, class TopoDim>
void WriteMeshGrm(const XField<PhysDim,TopoDim>& xfld, const std::string& filename);

} // namespace SANS

#endif // XFIELD_PX_H
