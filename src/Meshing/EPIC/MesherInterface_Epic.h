// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef MESHERINTERFACE_EPIC_H_
#define MESHERINTERFACE_EPIC_H_

//Python must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <iostream>
#include <memory>
#include <vector>

#include "Field/XField.h"
#include "Field/Field.h"

namespace SANS
{

template <class PhysDim, class TopoDim, class Mesher>
class MesherInterface;

class Epic;

//Class for transferring meshes and metric requests between SANS and Boeing's Epic mesher


struct EpicParams : noncopyable
{
  const ParameterString madcap{"madcap", "madcap", "The madcap script to invoke. May be a full path."};
  const ParameterString Version{"Version", "madcap_devel", "Version of the Madcap Epic mesher"};
  const ParameterString FilenameBase{"FilenameBase", "tmp/", "Default filepath prefix for generated files"};
  const ParameterBool SaveInputs{"SaveInputs", false, "Save Epic input files?"};
  const ParameterNumeric<int> nAdaptIter{"nAdaptIter", 25, 0, NO_LIMIT, "Number of adaptation refine/coarsen passes to run"};
  const ParameterString Shell{"Shell", "None", "Name of the mesh boundary surface unstructured surface object (object in 'csfname')"};
  const ParameterString BoundaryObject{"BoundaryObject", "None", "Name of boundary connection object used by Epic"};
  const ParameterFileName CSF{"CSF", std::ios_base::in, "", "Commmon Surface File containing 'Shell' and 'BoundaryObject' objects"};
  const ParameterString MeshNet{"MeshNet", "None", "Name of the boundary surface meshnet (object in 'csfname')"};
  const ParameterString GeometryList2D{"GeometryList2D", "",
                                   "Geometry definition: A list of unstructured curve objects for 2D"
                                      };
  const ParameterFileName GeometryFile3D{"GeometryFile3D", std::ios_base::in, "",
                                   "Geometry definition: IGES filename for 3D"
                                        };

  const ParameterNumericList<int> SymmetricSurf
  {"SymmetricSurf", EMPTY_LIST, 0, NO_LIMIT, "List of symmetry surfaces in grid: boundary group index (0 to nGroup-1)"};

  const ParameterNumericList<int> DisabledSurf
  {"DisabledSurf", EMPTY_LIST, 0, NO_LIMIT, "List of disabled surfaces in grid: boundary group index (0 to nGroup-1)"};

  const ParameterNumericList<int> SlipSurf
  {"SlipSurf", EMPTY_LIST, 0, NO_LIMIT, "List of slip-wall surfaces in grid: boundary group index (0 to nGroup-1)"};

  const ParameterNumericList<int> ViscSurf
  {"ViscSurf", EMPTY_LIST, 0, NO_LIMIT, "List of viscous wall surfaces in grid: boundary group index (0 to nGroup-1)"};

  const ParameterNumeric<Real> minH{"minH", -1, -1.0, NO_LIMIT, "Minimum size for mesh adaptation (global limit, not just on geometry surfaces)"};
  const ParameterNumeric<Real> maxH{"maxH", -1, -1.0, NO_LIMIT, "Maximum size for mesh adaptation (global limit, not just on geometry surfaces)"};
  const ParameterNumeric<Real> minGeom{"minGeom", -1, -1.0, NO_LIMIT, "Minimum size for geometry limiting"};
  const ParameterNumeric<Real> maxGeom{"maxGeom", -1, -1.0, NO_LIMIT, "Maximum size for geometry limiting"};

  const ParameterNumeric<int> nPointGeom
  {"nPointGeom", 24, -1, NO_LIMIT, "Target for geometric adaptation. Number of points on a circle matching surface radius of curvature"};

  const ParameterNumeric<Real> FoldedFaceAngle{"FoldedFaceAngle", 70.0, 0.0, 180.0, "Maximum permitted angle between two boundary faces"};
  const ParameterNumeric<Real> CornerEdgeAngle
  {"CornerEdgeAngle", 70.0, 0.0, 180.0, "Maximum angle change between two curve segments before treating as a corner"};

  const ParameterNumeric<Real> NormSize{"NormSize", -1.0, -1.0, NO_LIMIT, "Maximum normal spacing for boundary layer mesh"};
  const ParameterNumeric<int> CurvedQOrder{"CurvedQOrder", 2, 0, NO_LIMIT, "Flag to curve a Q1 mesh to specified Q inside Epic"};

  struct ProjectionMethodOptions
  {
    typedef std::string ExtractType;
    const std::string Elastic = "Elastic";
    const std::string CavityRegrid = "CavityRegrid";

    const std::vector<std::string> options{Elastic, CavityRegrid};
  };
  const ParameterOption<ProjectionMethodOptions> ProjectionMethod =
        ParameterOption<ProjectionMethodOptions>("ProjectionMethod", "Elastic", "Method to move new boundary nodes onto geometry "
                                                                                "(linear grid on curved geometry)");

  struct CurvingMethodOptions
  {
    typedef std::string ExtractType;
    const std::string Elastic = "Elastic";
    const std::string Distortion = "Distortion";

    const std::vector<std::string> options{Elastic, Distortion};
  };
  const ParameterOption<CurvingMethodOptions> CurvingMethod =
        ParameterOption<CurvingMethodOptions>("CurvingMethod", "Distortion", "Method generating curved elements");


  struct CurvingGeometryOptions
  {
    typedef std::string ExtractType;
    const std::string CADGeometry = "CADGeometry";
    const std::string ProjectDeformFile = "ProjectDeformFile";

    const std::vector<std::string> options{CADGeometry, ProjectDeformFile};
  };
  const ParameterOption<CurvingGeometryOptions> CurvingGeometry =
        ParameterOption<CurvingGeometryOptions>("CurvingGeometry", "CADGeometry", "Method generating curved elements");


  const ParameterBool Reconnection{"Reconnection", true, "Turn on cell reconnection capability?"};
  const ParameterBool NodeMovement{"NodeMovement", true, "Turn on node movement capability?"};
  const ParameterBool BoundaryNodeMovement{"BoundaryNodeMovement", true, "Turn on node movement on surfaces?"};

  const ParameterNumeric<Real> CellQualityRatio{"CellQualityRatio",  1.0, -1.0, NO_LIMIT, "Cell quality ratio"};
  const ParameterNumeric<Real> FaceQualityRatio{"FaceQualityRatio", -1.0, -1.0, NO_LIMIT, "Face quality ratio"};

  const ParameterNumeric<Real> CellInitialQualityTol{"CellInitialQualityTol", -1.0, -1.0, 1.0, "Initial cell quality tolerance"};

  const ParameterNumeric<Real> GrowthLimit{"GrowthLimit", 0.0, NO_RANGE, "Metric growth limit from the geometry curvature metric"};
  const ParameterNumeric<int> nGrowthIter{"nGrowthIter", 0, 0, NO_LIMIT, "Number of iterations in the metric growth limit cycling"};

  const ParameterNumeric<int> nBLayer{"nBLayer", 0, 0, NO_LIMIT, "Number of metric-conforming boundary layers to insert"};
  const ParameterNumeric<Real> BLayerGrowth{"BLayerGrowth", 1.0, 1, NO_LIMIT, "Boundary layer growth factor"};
  const ParameterNumeric<Real> BLayerAnisoCutOff{"BLayerAnisoCutOff", 5.0, 1, NO_LIMIT, "Boundary layer anisotropic lower cutoff"};
  const ParameterNumeric<Real> BLNormSize{"BLNormSize", -1.0, 0, NO_LIMIT, "Boundary layer set wall normal spacing"};

  const ParameterBool RemoveTrapped{"RemoveTrapped", true, "Enable Epic's remove trapped elements (edge, face, cell),"
                                                           " e.g. edge with two nodes on boundaries"
                                   };

  struct MetricNormOptions
  {
    typedef std::string ExtractType;
    const std::string L2 = "L2";
    const std::string Linf = "Linf";

    const std::vector<std::string> options{L2, Linf};
  };
  const ParameterOption<MetricNormOptions> MetricNorm =
        ParameterOption<MetricNormOptions>("MetricNorm", "L2", "Metric norm type");

  struct QualityModeOptions
  {
    typedef std::string ExtractType;
    // Box uses a bounding box oriented with the target metric and
    // node movement works better with Box
    const std::string Box = "Box";
    // Step is the step matrix change between the implied metric of an element
    // and the target metric
    const std::string Step = "Step";
    const std::string Shape0 = "Shape0";
    const std::string Shape = "Shape";
    const std::string Shape3 = "Shape3";
    const std::string Distortion = "Distortion";

    const std::vector<std::string> options{Box, Step, Shape0, Shape, Shape3, Distortion};
  };
  const ParameterOption<QualityModeOptions> QualityMode =
        ParameterOption<QualityModeOptions>("QualityMode", "Step", "Measure used to evaluate the quality of an element");

  struct QualityCombineModeOptions
  {
    typedef std::string ExtractType;
    const std::string Minimum = "Minimum";
    const std::string Average = "Average";
    const std::string ObjFunc = "ObjFunc";

    const std::vector<std::string> options{Minimum, Average, ObjFunc};
  };
  const ParameterOption<QualityCombineModeOptions> QualityCombineMode =
        ParameterOption<QualityCombineModeOptions>("QualityCombineMode", "ObjFunc", "Method to combine multiple element qualities "
                                                                                    "to a single value");

  const ParameterBool BackgroundMesh{"BackgroundMesh", true, "Use fixed background mesh for metric interpolation?"};

  const ParameterNumeric<Real> MaxAnisotropy{"MaxAnisotropy", 1e6, 0.0, NO_LIMIT, "Maximum anisotropy allowed in the adaptation process"};

  const ParameterNumeric<int> nThread{"nThread", 1, 1, NO_LIMIT, "Number of threads used by EPIC."};

  const ParameterBool DisableCall{"DisableCall", false, "Disable Epic call for debugging?"};

  static void checkInputs(PyDict d);
  static EpicParams params;
};


template <class PhysDim, class TopoDim>
class MesherInterface<PhysDim, TopoDim, Epic>
{
public:
  static const int D = PhysDim::D;
  typedef DLA::MatrixSymS<D,Real> MatrixSym;

  MesherInterface(int adapt_iter, const PyDict& paramsDict);

  std::pair<std::shared_ptr<XField<PhysDim, TopoDim>>,std::shared_ptr<XField<PhysDim, TopoDim>>>
  adapt(const Field_CG_Cell<PhysDim,TopoDim,MatrixSym>& metric_request) const;

protected:
  bool isNotNone( const std::string& str ) const;

  int adapt_iter_;
  const PyDict paramsDict_;
};


}

#endif /* MESHERINTERFACE_EPIC_H_ */
