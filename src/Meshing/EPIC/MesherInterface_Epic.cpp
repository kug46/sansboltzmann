// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <sstream>
#include <fstream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/split_string.h"
#include "tools/timer.h"
#include "tools/system_call.h"

#include "Topology/Dimension.h"
#include "Topology/ElementTopology.h"

#include "MesherInterface_Epic.h"
#include "WriteMeshMetric_Epic.h"

#include "Field/XFieldArea.h"
#include "Field/XFieldVolume.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldVolume_CG_Cell.h"

#include "XField_PX.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"
#include "MPI/MPI_sleep.h"

#define PYDICT_INSTANTIATE
#include "Python/PyDict_impl.h"

namespace SANS
{

PYDICT_PARAMETER_OPTION_INSTANTIATE(EpicParams::MetricNormOptions)
PYDICT_PARAMETER_OPTION_INSTANTIATE(EpicParams::QualityModeOptions)
PYDICT_PARAMETER_OPTION_INSTANTIATE(EpicParams::QualityCombineModeOptions)
PYDICT_PARAMETER_OPTION_INSTANTIATE(EpicParams::ProjectionMethodOptions)
PYDICT_PARAMETER_OPTION_INSTANTIATE(EpicParams::CurvingMethodOptions)
PYDICT_PARAMETER_OPTION_INSTANTIATE(EpicParams::CurvingGeometryOptions)

// cppcheck-suppress passedByValue
void EpicParams::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.madcap));
  allParams.push_back(d.checkInputs(params.Version));
  allParams.push_back(d.checkInputs(params.FilenameBase));
  allParams.push_back(d.checkInputs(params.SaveInputs));
  allParams.push_back(d.checkInputs(params.nAdaptIter));
  allParams.push_back(d.checkInputs(params.Shell));
  allParams.push_back(d.checkInputs(params.BoundaryObject));
  allParams.push_back(d.checkInputs(params.CSF));
  allParams.push_back(d.checkInputs(params.MeshNet));
  allParams.push_back(d.checkInputs(params.GeometryList2D));
  allParams.push_back(d.checkInputs(params.GeometryFile3D));
  allParams.push_back(d.checkInputs(params.SymmetricSurf));
  allParams.push_back(d.checkInputs(params.DisabledSurf));
  allParams.push_back(d.checkInputs(params.SlipSurf));
  allParams.push_back(d.checkInputs(params.ViscSurf));
  allParams.push_back(d.checkInputs(params.minH));
  allParams.push_back(d.checkInputs(params.maxH));
  allParams.push_back(d.checkInputs(params.minGeom));
  allParams.push_back(d.checkInputs(params.maxGeom));
  allParams.push_back(d.checkInputs(params.nPointGeom));
  allParams.push_back(d.checkInputs(params.FoldedFaceAngle));
  allParams.push_back(d.checkInputs(params.CornerEdgeAngle));
  allParams.push_back(d.checkInputs(params.NormSize));
  allParams.push_back(d.checkInputs(params.CurvedQOrder));
  allParams.push_back(d.checkInputs(params.CurvingMethod));
  allParams.push_back(d.checkInputs(params.CurvingGeometry));
  allParams.push_back(d.checkInputs(params.ProjectionMethod));
  allParams.push_back(d.checkInputs(params.Reconnection));
  allParams.push_back(d.checkInputs(params.NodeMovement));
  allParams.push_back(d.checkInputs(params.BoundaryNodeMovement));
  allParams.push_back(d.checkInputs(params.CellQualityRatio));
  allParams.push_back(d.checkInputs(params.FaceQualityRatio));
  allParams.push_back(d.checkInputs(params.CellInitialQualityTol));
  allParams.push_back(d.checkInputs(params.GrowthLimit));
  allParams.push_back(d.checkInputs(params.nGrowthIter));
  allParams.push_back(d.checkInputs(params.nBLayer));
  allParams.push_back(d.checkInputs(params.BLayerGrowth));
  allParams.push_back(d.checkInputs(params.BLayerAnisoCutOff));
  allParams.push_back(d.checkInputs(params.BLNormSize));
  allParams.push_back(d.checkInputs(params.RemoveTrapped));
  allParams.push_back(d.checkInputs(params.MetricNorm));
  allParams.push_back(d.checkInputs(params.QualityMode));
  allParams.push_back(d.checkInputs(params.QualityCombineMode));
  allParams.push_back(d.checkInputs(params.BackgroundMesh));
  allParams.push_back(d.checkInputs(params.MaxAnisotropy));
  allParams.push_back(d.checkInputs(params.nThread));
  allParams.push_back(d.checkInputs(params.DisableCall));
  d.checkUnknownInputs(allParams);
}
EpicParams EpicParams::params;

template <class PhysDim, class TopoDim>
MesherInterface<PhysDim, TopoDim, Epic>::MesherInterface(int adapt_iter, const PyDict& paramsDict) :
adapt_iter_(adapt_iter), paramsDict_(paramsDict) {}

template <class PhysDim, class TopoDim>
std::pair<std::shared_ptr<XField<PhysDim, TopoDim>>,std::shared_ptr<XField<PhysDim, TopoDim>>>
MesherInterface<PhysDim, TopoDim, Epic>::adapt(const Field_CG_Cell<PhysDim,TopoDim,MatrixSym>& metric_request) const
{
  std::string madcap = paramsDict_.get(EpicParams::params.madcap);
  std::string Version = paramsDict_.get(EpicParams::params.Version);
  std::string filename_base = paramsDict_.get(EpicParams::params.FilenameBase);
  bool SaveInputs = paramsDict_.get(EpicParams::params.SaveInputs);
  int nAdaptIter = paramsDict_.get(EpicParams::params.nAdaptIter);
  std::string Shell = paramsDict_.get(EpicParams::params.Shell);
  std::string BoundaryObject = paramsDict_.get(EpicParams::params.BoundaryObject);
  std::string CSF = paramsDict_.get(EpicParams::params.CSF);
  std::string MeshNet = paramsDict_.get(EpicParams::params.MeshNet);
  std::string GeometryList2D = paramsDict_.get(EpicParams::params.GeometryList2D);
  std::string GeometryFile3D = paramsDict_.get(EpicParams::params.GeometryFile3D);
  std::vector<int> SymmetricSurf = paramsDict_.get(EpicParams::params.SymmetricSurf);
  std::vector<int> DisabledSurf = paramsDict_.get(EpicParams::params.DisabledSurf);
  std::vector<int> SlipSurf = paramsDict_.get(EpicParams::params.SlipSurf);
  std::vector<int> ViscSurf = paramsDict_.get(EpicParams::params.ViscSurf);
  Real minH = paramsDict_.get(EpicParams::params.minH);
  Real maxH = paramsDict_.get(EpicParams::params.maxH);
  Real minGeom = paramsDict_.get(EpicParams::params.minGeom);
  Real maxGeom = paramsDict_.get(EpicParams::params.maxGeom);
  int nPointGeom = paramsDict_.get(EpicParams::params.nPointGeom);
  Real FoldedFaceAngle = paramsDict_.get(EpicParams::params.FoldedFaceAngle);
  Real CornerEdgeAngle = paramsDict_.get(EpicParams::params.CornerEdgeAngle);
  Real NormSize = paramsDict_.get(EpicParams::params.NormSize);
  int CurvedQOrder = paramsDict_.get(EpicParams::params.CurvedQOrder);
  std::string CurvingMethod = paramsDict_.get(EpicParams::params.CurvingMethod);
  std::string CurvingGeometry = paramsDict_.get(EpicParams::params.CurvingGeometry);
  bool Reconnection = paramsDict_.get(EpicParams::params.Reconnection);
  bool NodeMovement = paramsDict_.get(EpicParams::params.NodeMovement);
  bool BoundaryNodeMovement = paramsDict_.get(EpicParams::params.BoundaryNodeMovement);
  Real CellQualityRatio = paramsDict_.get(EpicParams::params.CellQualityRatio);
  Real FaceQualityRatio = paramsDict_.get(EpicParams::params.FaceQualityRatio);
  Real CellInitialQualityTol = paramsDict_.get(EpicParams::params.CellInitialQualityTol);
  Real GrowthLimit = paramsDict_.get(EpicParams::params.GrowthLimit);
  int nGrowthIter = paramsDict_.get(EpicParams::params.nGrowthIter);
  int nBLayer = paramsDict_.get(EpicParams::params.nBLayer);
  Real BLayerGrowth = paramsDict_.get(EpicParams::params.BLayerGrowth);
  Real BLayerAnisoCutOff = paramsDict_.get(EpicParams::params.BLayerAnisoCutOff);
  Real BLNormSize = paramsDict_.get(EpicParams::params.BLNormSize);
  bool RemoveTrapped = paramsDict_.get(EpicParams::params.RemoveTrapped);
  bool BackgroundMesh = paramsDict_.get(EpicParams::params.BackgroundMesh);
  Real MaxAnisotropy = paramsDict_.get(EpicParams::params.MaxAnisotropy);
  int nThread = paramsDict_.get(EpicParams::params.nThread);
  bool DisableCall = paramsDict_.get(EpicParams::params.DisableCall);

  bool EpicGeomFlag = false;

  if (D == 3)
  {
    if ( isNotNone(BoundaryObject) && isNotNone(CSF) && isNotNone(GeometryFile3D) && isNotNone(Shell) )
      EpicGeomFlag = true;
  }
  else if (D == 2)
  {
    if ( isNotNone(CSF) && isNotNone(GeometryList2D) )
      EpicGeomFlag = true;
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION("MesherInterface<PhysDim, TopoDim, Epic>::adapt - Unsupported dimension.");
    return {nullptr, nullptr};
  }

  std::vector<std::string> curvedGeometryList;
  if (D == 2 && EpicGeomFlag)
  {
    //Get list of curved geometries
    curvedGeometryList = split_string(GeometryList2D, " ");
  }
  int nCurvedGeometry = (int) curvedGeometryList.size();

  //Epic requires a valid q1 grid as the background mesh
  std::string mesh_filename_base = filename_base + "mesh_a" + std::to_string(adapt_iter_);
  std::string mesh_filename = mesh_filename_base + "_in.grm";
  std::string metric_filename = filename_base + "metric_request_a" + std::to_string(adapt_iter_) + ".bnmetric";

  std::string meshQ1_out_filename = mesh_filename_base + "_outQ1.grm";
  std::string meshQcurved_out_filename = mesh_filename_base + "_outQ" + std::to_string(CurvedQOrder) + ".grm";

  // name of the EPIC script
  std::string script_filename = filename_base + "epic_script.com";

  //Write out the mesh and nodal metric request
  WriteMeshMetric_Epic(metric_request, mesh_filename, metric_filename);

  if (SaveInputs)
  {
    SANS_DEVELOPER_EXCEPTION("MesherInterface<PhysDim, TopoDim, Epic>::adapt - Cannot save Epic input files yet.");
    return {nullptr, nullptr};
  }


  if (metric_request.getXField().comm()->rank() == 0)
  {

    //Set file type based on dimension
    std::string fileType = D == 2 ? "csf" : "cgd";

    std::string case_filename = filename_base + "case_a" + std::to_string(adapt_iter_) + "." + fileType;

    // EPIC appends to the case file, which can cause errors, so remove it
    std::remove(case_filename.c_str());

    //-----------------------------------------------
    // Write the Epic script
    //-----------------------------------------------

    std::fstream fscript( script_filename, std::fstream::out );
    fscript << std::setprecision(8) << std::scientific;

    fscript << "! Auto-generated script for communication between SANS and EPIC" << std::endl;
    fscript << std::endl;

    fscript << "lua ORIGNAME = \"" + case_filename + "\"" << std::endl;
    fscript << "lua INGRM = \"" + mesh_filename + "\"" << std::endl;
    fscript << "lua OUTQ1GRM = \"" + meshQ1_out_filename + "\"" << std::endl;

    fscript << "file import grm $INGRM$ " + fileType + " $ORIGNAME$ 'INPUTOBJ'" << std::endl;
    fscript << "file open " + fileType + " $ORIGNAME$" << std::endl;
    fscript << "file set working " + fileType + " $ORIGNAME$" << std::endl;
    fscript << "om set file " + fileType + " $ORIGNAME$" << std::endl;
    fscript << "file set units inches file " + fileType + " $ORIGNAME$" << std::endl;
    fscript << std::endl;

    if (D == 3)
    {
      if (EpicGeomFlag == true)
      {
        if (CurvedQOrder > 1)
        {
          fscript << "lua OUTQ" << CurvedQOrder << "GRM_NOPROJ = \"" + mesh_filename_base + "_outQ" << CurvedQOrder << "_noproj.grm\"" << std::endl;
          fscript << "lua OUTQ" << CurvedQOrder << "GRM = \"" + meshQcurved_out_filename + "\"" << std::endl;
        }

        fscript << "lua CSFFILE = \"" + CSF + "\"" << std::endl;
        fscript << "lua IGSFILE = \"" + GeometryFile3D + "\"" << std::endl;
        fscript << "lua SHELLNAME = \"" + Shell + "\"" << std::endl;
        fscript << "lua BONAME = \"" + BoundaryObject + "\"" << std::endl;
        if ( isNotNone(MeshNet) )
        {
          fscript << "lua MESHNETNAME= \"" + MeshNet + "\"" << std::endl;
        }
        fscript << std::endl;
      }

      int nSymmetricSurf = (int) SymmetricSurf.size();
      int nSlipSurf = (int) SlipSurf.size();
      int nViscSurf = (int) ViscSurf.size();

      for (int i = 0; i < nSymmetricSurf; i++)
        fscript << "lua SYMSURF" << i << " = \"U" << (SymmetricSurf[i]+1) << "\"" << std::endl;

      for (int i = 0; i < nSlipSurf; i++)
        fscript << "lua SLIPSURF" << i << " = \"U" << (SlipSurf[i]+1) << "\"" << std::endl;

      for (int i = 0; i < nViscSurf; i++)
        fscript << "lua VISCSURF" << i << " = \"U" << (ViscSurf[i]+1) << "\"" << std::endl;

      fscript << std::endl;

      // Ensure that the bc's (symmetry) are set on the input linear grid
      if ( nSymmetricSurf + nSlipSurf + nViscSurf > 0 )
      {
        fscript << "file set working cgd $ORIGNAME$" << std::endl;
        fscript << "om set file cgd $ORIGNAME$" << std::endl;
        fscript << "ol set cell vertex" << std::endl;
        fscript << "bc couple mode bilinear best" << std::endl;
        fscript << "bc file name $ORIGNAME$" << std::endl;
        fscript << "ol file name $ORIGNAME$" << std::endl;

        if ( nSymmetricSurf > 0 )
        {
          /* Ensure that the bc's (symmetry) are set on the input linear grid */
          fscript << "bc zone 1" << std::endl;
          fscript << "bc group start guigroup" << std::endl;
          for ( int i = 0; i < nSymmetricSurf; i++ )
            fscript << "bc boundary $SYMSURF" << i << "$" << std::endl;
          fscript << "bc group end" << std::endl;
          fscript << "bc set from_type UNDEFINED to REFLECTION" << std::endl;
          fscript << "bc update" << std::endl;
        }
        if ( nSlipSurf > 0 )
        {
          /* Ensure that the bc's (flow tan wall) are set on the input linear grid */
          fscript << "bc zone 1" << std::endl;
          fscript << "bc group start guigroup" << std::endl;
          for ( int i = 0; i < nSlipSurf; i++ )
            fscript << "bc boundary $SLIPSURF" << i << "$" << std::endl;
          fscript << "bc group end" << std::endl;
          fscript << "bc set from_type UNDEFINED to INVISCID WALL" << std::endl;
          fscript << "bc update" << std::endl;
        }
        if ( nViscSurf > 0 )
        {
          /* Ensure that the bc's (viscous wall) are set on the input linear grid */
          fscript << "bc zone 1" << std::endl;
          fscript << "bc group start guigroup" << std::endl;
          for ( int i = 0; i < nViscSurf; i++ )
            fscript << "bc boundary $VISCSURF" << i << "$" << std::endl;
          fscript << "bc group end" << std::endl;
          fscript << "bc set from_type UNDEFINED to VISCOUS WALL" << std::endl;
          fscript << "bc update" << std::endl;
        }
        fscript << std::endl;
      } // nSymmetricSurf + nSlipSurf + nViscSurf > 0

      // Register mesh and geometry
      if ( EpicGeomFlag  )
      {
        fscript << "file open igs $IGSFILE$ mode apptopen" << std::endl;
        fscript << "file open csf $CSFFILE$" << std::endl;
        fscript << std::endl;
        fscript << "om select clear" << std::endl;
        fscript << "om select usurface object $BONAME$ file csf $CSFFILE$" << std::endl;
        fscript << "geom register boundarydata" << std::endl;
        fscript << "om select clear" << std::endl;
        fscript << "geom autoproject on" << std::endl;
        fscript << "om select clear" << std::endl;
        fscript << "om select usurface object $SHELLNAME$ file csf $CSFFILE$" << std::endl;
        fscript << "geom register associatedmesh" << std::endl;

        if ( isNotNone(MeshNet) )
        {
          fscript << std::endl;
          fscript << "! register the meshnet geometry model" << std::endl;
          fscript << "om select clear" << std::endl;
          fscript << "om select usurface object \"$MESHNETNAME$\" file csf \"$CSFFILE$\"" << std::endl;
          fscript << "create meshnet interpolation mode meshnet" << std::endl;
        }
      } // EpicProjectFlag
    }
    else if (D == 2)
    {
      if ( EpicGeomFlag )
      {
        if (CurvedQOrder > 1)
        {
          fscript << "lua OUTQ" << CurvedQOrder << "GRM_NOPROJ = \"" + mesh_filename_base + "_outQ" << CurvedQOrder << "_noproj.grm\"" << std::endl;
          fscript << "lua OUTQ" << CurvedQOrder << "GRM = \"" + mesh_filename_base + "_outQ" << CurvedQOrder << ".grm\"" << std::endl;
        }

        fscript << "lua CSFFILE = \"" + CSF + "\"" << std::endl;

        for (int i = 0; i < nCurvedGeometry; i++)
          fscript << "lua UCRV" << i << " = \"" + curvedGeometryList[i] + "\"" << std::endl;

        fscript << std::endl;
        fscript << "! Load geometry curves" << std::endl;

        if (nCurvedGeometry > 0)
        {
          //Set and register the geometry curves
          fscript << "file open csf $CSFFILE$" << std::endl;
          fscript << "om select clear" << std::endl;

          for (int i = 0; i < nCurvedGeometry; i++)
            fscript << "om select usurf object $UCRV" << i << "$ file csf $CSFFILE$" << std::endl;
        }
        else
        {
          //Use grid curves as geometry
          fscript << "om select clear" << std::endl;
          fscript << "om select usurf object 'INPUTOBJ' file csf $ORIGNAME$" << std::endl;
        }

        fscript << "geom register curvedata" << std::endl;
      } // EpicProjectFlag
    }

    // print out the MADCAP target elem
    fscript << std::endl;
    fscript << "! Adaptation parameters" << std::endl;
    fscript << "GGU ADAPT NCELL_REQUEST 0" << std::endl;
    fscript << "GGU ADAPT SET_ERROR_PARAM METHOD FILE" << std::endl;
    fscript << "GGU ADAPT SET_ERROR_PARAM FILE \"" << metric_filename << "\"" << std::endl;


    fscript << "GGU ADAPT SET_METRIC_PARAM HMIN " << minH << " HMAX " << maxH << std::endl;

    fscript << "GGU ADAPT SET_METRIC_PARAM MAXANISOTROPY " << MaxAnisotropy << std::endl;

    fscript << "GGU ADAPT SET_METRIC_PARAM TRANSFER AVERAGE AVERAGE LOG" << std::endl;
    fscript << "GGU ADAPT SET_METRIC_PARAM GROWTH_LIMIT " << GrowthLimit << std::endl;
    fscript << "GGU ADAPT SET_METRIC_PARAM GROWTH_LIMIT_INITIAL " << GrowthLimit << std::endl;
    fscript << "GGU ADAPT SET_METRIC_PARAM GROWTH_NLAYER " << 0 << std::endl;
    fscript << "GGU ADAPT SET_METRIC_PARAM GROWTH_NITER " << nGrowthIter << std::endl;

    if (EpicGeomFlag == true)
    {
      if ( nPointGeom > 0 )
       {
         // Attach geometry (curvature) data to select object
         fscript << "GGU ADAPT SET_METRIC_PARAM GEOM_CURVELIMIT_NPTS " << nPointGeom
                 << " GEOM_CURVELIMIT_MIN " << minGeom
                 << " GEOM_CURVELIMIT_MAX " << maxGeom << std::endl;
         fscript << "GGU ADAPT SET_METRIC_PARAM GEOM_CURVELIMIT_NORMSIZE " << NormSize << std::endl;
       }

      // Project and deform the linear grid after each refinement/coarsening pass
      fscript << "GGU ADAPT SET_EPIC_PARAM PROJECT_MODE " + paramsDict_.get(EpicParams::params.ProjectionMethod) << std::endl;

      // This will need to change if we want to use MESHNET geometry
     if (D == 3)
         fscript << "GGU ADAPT SET_EPIC_PARAM PROJECT_TO_GEOM IGES" << std::endl;
    }

    if (D == 2)
      fscript << "GGU ADAPT SET_EPIC_PARAM PROJECT_TO_GEOM CURVE" << std::endl;

    // 1 (refine) + 2 (coarsen)
    int coarsenRefineBitmap = 3;

    coarsenRefineBitmap += 4; // +4 (pre-coarsening pass)

    // cell reconnection capability
    if ( Reconnection == true )
    {
      coarsenRefineBitmap += 16; // +16 (reconnect)
      coarsenRefineBitmap += 64; // +64 (extra reconnect)
    }

    // node movement capability
    if ( NodeMovement == true )
    {
      coarsenRefineBitmap +=  32; //  +32 (node movement)
      coarsenRefineBitmap += 128; // +128 (extra node movement)
    }

    fscript << "GGU ADAPT SET_EPIC_PARAM COARSENREFINE_BITMAP " << coarsenRefineBitmap << std::endl;
    fscript << "GGU ADAPT SET_EPIC_PARAM COARSEN_MODE 1" << std::endl;
    fscript << "GGU ADAPT SET_EPIC_PARAM NITER " << nAdaptIter << std::endl;
    fscript << "GGU ADAPT SET_EPIC_PARAM MESSAGE_LEVEL 4" << std::endl;

    // Boundary node movement defaults to ON in EPIC, so only need to turn it off if requested
    if ( !BoundaryNodeMovement )
      fscript << "GGU ADAPT SET_EPIC_PARAM SMOOTHBOUND OFF" << std::endl;

    if (BackgroundMesh == true)
      fscript << "GGU ADAPT SET_EPIC_PARAM BACKGROUND_MESH INITIAL" << std::endl; //Use a static background mesh


    // metric norm
    int metricNormVal = -1;
    if ( paramsDict_.get(EpicParams::params.MetricNorm) == EpicParams::params.MetricNorm.L2 )
      metricNormVal = 2;
    else if ( paramsDict_.get(EpicParams::params.MetricNorm) == EpicParams::params.MetricNorm.Linf )
      metricNormVal = 0;

    fscript << "GGU ADAPT SET_EPIC_QUALPARAM NORM " << metricNormVal << std::endl;

    // quality measures
    fscript << "GGU ADAPT SET_EPIC_QUALPARAM QUALMODE " + paramsDict_.get(EpicParams::params.QualityMode) << std::endl;
    fscript << "GGU ADAPT SET_EPIC_QUALPARAM QUALCOMBMODE " + paramsDict_.get(EpicParams::params.QualityCombineMode) << std::endl;

    //cell quality ratio
    fscript << "GGU ADAPT SET_EPIC_QUALPARAM CELLQUALITY_RATIO " << CellQualityRatio << std::endl;
    fscript << "GGU ADAPT SET_EPIC_QUALPARAM INITIAL_QUALTOL " << CellInitialQualityTol << std::endl;

    //face quality ratio
    fscript << "GGU ADAPT SET_EPIC_QUALPARAM FACEQUALITY_RATIO " << FaceQualityRatio << std::endl;

    fscript << "GGU ADAPT SET_EPIC_QUALPARAM FOLDED_FACEANGLE " << FoldedFaceAngle << std::endl;
    fscript << "GGU ADAPT SET_EPIC_QUALPARAM CORNER_EDGEANGLE " << CornerEdgeAngle << std::endl;
    fscript << "GGU ADAPT SET_EPIC_QUALPARAM REMOVETRAPPED " << (RemoveTrapped ? "ON" : "OFF") << std::endl;


    int nDisabledSurf = (int) DisabledSurf.size();
    if (nDisabledSurf > 0)
    {
      fscript << "GGU ADAPT SET_ADAPT_PARAM DISABLE_SURF";

      for (int i = 0; i < nDisabledSurf; i++)
      {
        if (i == 0)
          fscript << " " << (DisabledSurf[i]+1);
        else
          fscript << ", " << (DisabledSurf[i]+1);
      }
      fscript << std::endl;
    }

    if (nBLayer > 0)
    {
      fscript << "GGU ADAPT SET_EPIC_BLAYERPARAM NORMSIZE " << BLNormSize << std::endl;
      fscript << "GGU ADAPT SET_EPIC_BLAYERPARAM ANISOCUTOFF " << BLayerAnisoCutOff << std::endl;
      fscript << "GGU ADAPT SET_EPIC_BLAYERPARAM NLAYER1 " << nBLayer << " GROW1 " << BLayerGrowth << std::endl;
      fscript << "GGU ADAPT SET_EPIC_BLAYERPARAM NLAYER2 0 GROW2 0" << std::endl;
      fscript << "GGU ADAPT SET_EPIC_BLAYERPARAM EDGEQUALMODE REINSERT RECONNECT" << std::endl;
    }

    fscript << std::endl;
    fscript << "! Select object to be adapted" << std::endl;
    fscript << "om select clear" << std::endl;
    if (D == 3)
    {
      if ( EpicGeomFlag  )
      {
        fscript << "om select usurface object $SHELLNAME$ file csf $CSFFILE$" << std::endl;
        fscript << "om select uzone object \'ZONE   1\' file cgd $ORIGNAME$" << std::endl;
        fscript << "manip umergeoptdata" << std::endl;
        fscript << "om select $result" << std::endl;
      }
      else
      {
        fscript << "om select uzone object 'ZONE   1' file cgd $ORIGNAME$" << std::endl;
      }
    }
    else if (D == 2)
    {
      fscript << "om select usurf object 'INPUTOBJ' file csf $ORIGNAME$" << std::endl;
    }

    fscript << std::endl;
    fscript << "! Metric preprocessing" << std::endl;
    fscript << "GGU ADAPT ATTACH_TARGET_SIZE" << std::endl;
    fscript << std::endl;
    fscript << "! Adaptation" << std::endl;
    fscript << "om select $result" << std::endl;
    fscript << "GGU ADAPT EXECUTE USING EPIC" << std::endl;
    fscript << "file save name 'ADAPTED' overwrite file " + fileType + " $ORIGNAME$" << std::endl;
    fscript << std::endl;

    if (EpicGeomFlag == true)
    {
      fscript << "om select clear" << std::endl;
      if (CurvedQOrder > 1)
      {
        if (D == 3)
        {
          fscript << "om select usurface object $SHELLNAME$ file csf $CSFFILE$" << std::endl;
          fscript << "om select uzone object 'ZONE   2' file cgd $ORIGNAME$" << std::endl;
          fscript << "manip umergeoptdata" << std::endl;
          fscript << "om select $result" << std::endl;
        }
        else
        {
          fscript << "om select usurface object 'ADAPTED' file csf $ORIGNAME$" << std::endl;
        }

        fscript << "manip deform solver cg precond sgs geometry" << std::endl;
        fscript << "file save name 'ADAPTED PROJECTED' overwrite file " + fileType + " $ORIGNAME$" << std::endl;
      }

      fscript << "om select $result" << std::endl;
      fscript << "file export to file grm $OUTQ1GRM$ overwrite optionalarg" << std::endl;

      if (CurvedQOrder > 1)
      {
        fscript << "om select clear" << std::endl;
        if (D == 3)
        {
          fscript << "om select usurface object $SHELLNAME$ file csf $CSFFILE$" << std::endl;
          fscript << "om select uzone object \'ZONE   2\' file cgd $ORIGNAME$" << std::endl;
          fscript << "manip umergeoptdata" << std::endl;
          fscript << "om select $result" << std::endl;
        }
        else
        {
          fscript << "om select usurface object 'ADAPTED' file csf $ORIGNAME$" << std::endl;
        }
        std::string projectdeformfile = filename_base + "projectdeform_a" + std::to_string(adapt_iter_) + ".disp";

        fscript << "ggu higherorder qorder " << CurvedQOrder << " projectdelta '" + projectdeformfile + "'" << std::endl;

        fscript << "file save name 'ADAPTED Q" << CurvedQOrder << " NOPROJ' overwrite file " + fileType + " $ORIGNAME$" << std::endl;
        fscript << "om select $result" << std::endl;
        fscript << "file export to file grm $OUTQ" << CurvedQOrder << "GRM_NOPROJ$ overwrite optionalarg" << std::endl;

        if (CurvingGeometry == EpicParams::params.CurvingGeometry.CADGeometry)
          CurvingGeometry = "geometry";
        else if (CurvingGeometry == EpicParams::params.CurvingGeometry.ProjectDeformFile)
          CurvingGeometry = "file '" + projectdeformfile + "'";
        else
          SANS_DEVELOPER_EXCEPTION("Unknown Curving Geometry option");

        if (CurvingMethod == EpicParams::params.CurvingMethod.Elastic)
          fscript << "manip deform solver cg precond sgs subdivide " << CurvingGeometry << std::endl; //linear elastic approach
        else if (CurvingMethod == EpicParams::params.CurvingMethod.Distortion)
          fscript << "manip deform solver local model quality " << CurvingGeometry << std::endl; //distortion-based approach
        else
          SANS_DEVELOPER_EXCEPTION("Unknown Curving Method option");

        fscript << "file save name 'ADAPTED Q" << CurvedQOrder << " PROJECTED' overwrite file " + fileType + " $ORIGNAME$" << std::endl;

        fscript << "om select $result" << std::endl;
        fscript << "file export to file grm $OUTQ" << CurvedQOrder << "GRM$ overwrite optionalarg" << std::endl;
      }
      fscript << std::endl;
    }
    else
    {
      fscript << "om select $result" << std::endl;
      fscript << "file export to file grm $OUTQ1GRM$ overwrite optionalarg" << std::endl;
    }

    fscript << std::flush;
    fscript.close();
  } // if rank == 0

  /*------------------------*/
  /* System call to MADCAP  */
  /*------------------------*/
  if (DisableCall == false)
  {
    if (metric_request.getXField().comm()->rank() == 0)
    {
      /*--------------------------*/
      /** system call to madcap  **/
      /*--------------------------*/
      std::stringstream args;
      std::string epic_log = filename_base + "epic_log.txt";
      args << "-v " << Version << " -cpu " << nThread << " -s " << script_filename;
      args <<" >> "<< epic_log; //Append Epic output to log file

      system_call("EPIC", madcap, args.str());
      wait_for_file(meshQ1_out_filename);

      if (EpicGeomFlag == true)
        wait_for_file(meshQcurved_out_filename);
    }

    // wait for rank 0 (1 second at a time) to finish executing EPIC before reading in the grid
    // using barrier causes waiting processor to use 100% CPU
    MPI_sleep(*metric_request.getXField().comm(), 0, 1000);

    //Read in new meshes generated by Epic
    std::shared_ptr<XField<PhysDim, TopoDim>> pxfld_linear( new XField_PX<PhysDim, TopoDim>(*metric_request.comm(), meshQ1_out_filename) );
    std::shared_ptr<XField<PhysDim, TopoDim>> pxfld_curved;

    if (EpicGeomFlag && CurvedQOrder > 1)
      pxfld_curved = std::shared_ptr<XField<PhysDim, TopoDim>>(
        new XField_PX<PhysDim, TopoDim>(*pxfld_linear, XFieldBalance::CellPartitionCopy, meshQcurved_out_filename) );
    else
      pxfld_curved = pxfld_linear;

    return {pxfld_linear, pxfld_curved};
  }
  else
    return {nullptr, nullptr};
}

template <class PhysDim, class TopoDim>
bool
MesherInterface<PhysDim, TopoDim, Epic>::isNotNone(const std::string& str) const
{
  return !(str.empty() || str == "None");
}

//Explicit instantiations
template class MesherInterface<PhysD2, TopoD2, Epic>;
template class MesherInterface<PhysD3, TopoD3, Epic>;

}
