// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef WRITEMESHMETRIC_EPIC_H_
#define WRITEMESHMETRIC_EPIC_H_

#include <string>

#include "Field/Field.h"

namespace SANS
{

template <class PhysDim, class TopoDim>
void WriteMeshMetric_Epic(
    const Field_CG_Cell< PhysDim, TopoDim, DLA::MatrixSymS<PhysDim::D,Real> >& fld,
    const std::string& meshfilename, const std::string& metricfilename);

}

#endif /* WRITEMESHMETRIC_EPIC_H_ */
