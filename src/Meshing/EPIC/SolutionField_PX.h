// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOLFIELDCG_PX_H
#define SOLFIELDCG_PX_H

/*\
 *
 *  XField class used to reading ProjectX grm, and gri grid files
 *
\*/

#include "Field/FieldTypes.h"

#include <string>

namespace SANS
{

// constructors from a file
template <class PhysDim, class TopoDim, class T>
void WriteSolution_PX(const Field<PhysDim,TopoDim,T>& fld, const std::string& filename);

// constructors from a file
template <class PhysDim, class TopoDim, class T>
void ReadSolution_PX(const std::string& filename, Field<PhysDim,TopoDim,T>& fld );

template<class T>
void writeLine_PX(std::fstream& writer, const T& array);

template<class T>
void readLine_PX(std::ifstream& reader, T& array);



} // namespace SANS

#endif // SOLFIELDCG_PX_H
