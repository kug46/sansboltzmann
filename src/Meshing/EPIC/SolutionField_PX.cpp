// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "SolutionField_PX.h"
#include "XField_PX.h"

#include <fstream>
#include <cstring>
#include <algorithm>
#include <iomanip> // std::setprecision
#include <map>
#include <vector>
#include <sstream>
#include <array>

#include "Field/XFieldLine.h"   //Needed to instantiate an area XField
#include "Field/XFieldArea.h"   //Needed to instantiate an area XField
#include "Field/XFieldVolume.h" //Needed to instantiate a volume XField
#include "Field/XFieldSpacetime.h" //Needed to instantiate a volume XField
#include "Field/Partition/XField_Lagrange.h"

#include "Field/tools/for_each_CellGroup.h"
#include "Field/tools/for_each_BoundaryTraceGroup.h"

#include "Field/Field.h"
#include "Field/FieldLine.h"
#include "Field/FieldArea.h"
#include "Field/FieldVolume.h"
#include "Field/FieldSpacetime.h"

#include "BasisFunction/LagrangeDOFMap.h"

#include "tools/file_extension.h"
#include "tools/file_skiplines.h"
#include "tools/split_string.h"

#include "UserVariables/BoltzmannNVar.h"  // for state vector in Boltzmann

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_MPI
#include "MPI/continuousElementMap.h"
#include "MPI/serialize_DenseLinAlg_MatrixS.h"

#include <boost/mpi/collectives/all_reduce.hpp>
#include <boost/mpi/collectives/gather.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/vector.hpp>

// handles serialization of arrays, depending on version of boost
#include "MPI/boost_serialization_array.h"
#endif


#define DOF_TAG         0

namespace SANS
{


/*\
 *
 *  Reader utilities (internal namespace not meant to be used by client code)
 *  for making life easier when reading files.
 *  Exposes functions for getting the next (non-empty) line, splitting a
 *  line by a delimiter.
 *  Functions intended to be used are get_value<type> and get_list<type>.
 *
\*/
namespace _readerUtils
{


  // utility function for getting the next (non-empty) line in a file as a vector
  // of strings broken by the spaces in the line
  inline std::vector<std::string>
  get_spaced_line(std::ifstream& input)
  {
    std::string line;
    while (std::getline(input,line))
      if (!line.empty()) break;

    return split_string(line," ");
  }

  template <typename type> inline type convert2( const std::string& s );

  template<>
  inline int
  convert2<int>( const std::string &s ) { return std::stoi( s ); }

  template<>
  inline double
  convert2<double>( const std::string &s ) { return std::stod( s ); }

  // utility function for getting a typed list as the next line in the file
  template <typename type>
  inline std::vector<type>
  get_list(std::ifstream& input)
  {
    std::vector<std::string> sdata = get_spaced_line(input);
    std::vector<type> tdata(sdata.size());
    for (std::size_t i = 0; i < sdata.size(); i++)
      tdata[i] = convert2<type>(sdata[i]);

    return tdata;
  }

  template <typename type>
  inline type
  get_value(std::ifstream& input)
  {
    std::vector<type> vals = get_list<type>(input);
    SANS_ASSERT( vals.size() == 1 );
    return vals[0];
  }

  inline std::string
  get_string( std::ifstream& input )
  {
    std::vector<std::string> vals = get_spaced_line(input);
    SANS_ASSERT( vals.size() == 1 );
    return vals[0];
  }

} // namespace _readerUtils

//---------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
void WriteSolution_PX(const Field<PhysDim,TopoDim,T>& qfld, const std::string& filename)
{
  int comm_rank = qfld.getXField().comm()->rank();
  std::fstream writer;

  if (comm_rank == 0)
  {
    writer.open( filename, std::fstream::out );

    if (!writer.good())
      SANS_RUNTIME_EXCEPTION( "Error writing to file: %s", filename.c_str() );
  }

  const int nCellGroup = qfld.nCellGroups();

  //--------------------------------------------------//

  int nDOFnative = qfld.nDOFnative();

  if (comm_rank == 0)
  {
    //Write the header
    writer << PhysDim::D << " " << nDOFnative << " " << nCellGroup;

    for (int group = 0; group < nCellGroup; group++)
    {
      //write out basis category and order to check consistency
      writer << " " << qfld.getCellGroupBase(group).basisCategory();
      writer << " " << qfld.getCellGroupBase(group).order();
    }

    writer << std::endl;
    writer << std::scientific << std::setprecision(16);
  }

#ifdef SANS_MPI

  int comm_size = qfld.getXField().comm()->size();

  // maximum DOF chunk size that rank 0 will write at any given time
  int DOFchunk = nDOFnative / comm_size + nDOFnative % comm_size;

  for (int rank = 0; rank < comm_size; rank++)
  {
    int DOFlow = rank*DOFchunk;
    int DOFhigh = (rank+1)*DOFchunk;

    std::map<int,T> buffer;

    for (int i = 0; i < qfld.nDOF(); i++)
    {
      int iDOF = qfld.local2nativeDOFmap(i);
      if (iDOF >= DOFlow && iDOF < DOFhigh)
        buffer[iDOF] = qfld.DOF(i);
    }

    if (comm_rank == 0)
    {
      std::vector<std::map<int,T>> bufferOnRank;
      boost::mpi::gather(*qfld.getXField().comm(), buffer, bufferOnRank, 0 );

      // collapse down the buffer from all ranks (this remove any possible duplicates)
      for (std::size_t i = 0; i < bufferOnRank.size(); i++)
        buffer.insert(bufferOnRank[i].begin(), bufferOnRank[i].end());

      //Write the node data
      for ( const auto& DOFpair : buffer)
      {
        writeLine_PX<T>(writer, DOFpair.second);
      }

    }
#ifndef __clang_analyzer__
    else // send the buffer to rank 0
      boost::mpi::gather(*qfld.getXField().comm(), buffer, 0 );
#endif
  } // for rank

#else
  //Write the node data
  for (int k = 0; k < nDOFnative; k++)
  {
    writeLine_PX<T>(writer, qfld.DOF(k));
  }
#endif

  if (comm_rank == 0)
  {
    writer << std::flush;
    writer.close();
  }

  // wait for the writing to finish
  qfld.getXField().comm()->barrier();

}

//---------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
void ReadSolution_PX(const std::string& filename, Field<PhysDim,TopoDim,T>& qfld)
{
  int comm_rank = qfld.getXField().comm()->rank();
  std::ifstream grm;

  const int nCellGroup = qfld.nCellGroups();

  //--------------------------------------------------//

  int nDOFnative = qfld.nDOFnative();
  // only rank 0 will read the grid
  int dim = 0;
  int nPoints = 0;
  if (comm_rank == 0)
  {
    // open the file
    grm.open(filename.c_str());
    SANS_ASSERT_MSG( grm.is_open() , "XField<PhysDim,TopoDim>::readSolGrm - Error opening file: %s", filename.c_str() );

    // read the header
    std::vector<int> data = _readerUtils::get_list<int>(grm);
    //    SANS_ASSERT_MSG(data.size() == 4, "Expected 4 integers in first line of %s, found %d", filename.c_str(), (int)data.size());
    dim = data[0];
    SANS_ASSERT(dim == PhysDim::D);
    nPoints = data[1];
    SANS_ASSERT(nPoints == nDOFnative);
    SANS_ASSERT(nCellGroup == data[2]);

    int ii = 3;
    for (int group=0; group< nCellGroup; group++)
    {
      SANS_ASSERT( data[ii] == qfld.getCellGroupBase(group).basisCategory()  );
      ii++;
      SANS_ASSERT( data[ii] == qfld.getCellGroupBase(group).order() );
      ii++;
    }
  }

#ifdef SANS_MPI
  int comm_size = qfld.getXField().comm()->size();

  // Compute the number of DOFs on each processor
  int DOFperProc = nDOFnative / comm_size;

  std::map<int,T> DOF;

  if (comm_rank == 0)
  {
    // Make sure the buffer is large enough for the last processor
    std::map<int,T> DOF_buffer;
    T dof = 0;

    for (int idof = 0; idof < nDOFnative; idof++)
    {
      // Compute the rank that should receive the DOF
      int rank = MIN(comm_size-1, idof / DOFperProc);

      readLine_PX<T>(grm, dof);

      if (rank == 0)
      {
        // Save the DOF
        DOF[idof] = dof;
      }
      else
      {
        // Cache the DOF to be sent
        DOF_buffer[idof] = dof;
      }

      if (idof+1 == (rank+1)*DOFperProc && rank > 0 && rank < comm_size-1)
      {
        // send the DOFs to the appropriate rank
        qfld.getXField().comm()->send(rank, DOF_TAG, DOF_buffer);
        DOF_buffer.clear();
      }
      else if (idof+1 == nDOFnative)
      {
        // send the DOFs to the last rank if needed
        if (rank > 0)
          qfld.getXField().comm()->send(rank, DOF_TAG, DOF_buffer);
      }
    }
  }
  else
  {
    // All other ranks will receive DOFs sent from rank 0
    qfld.getXField().comm()->recv(0, DOF_TAG, DOF);
  }

  std::vector<std::vector<int>> needDOF(comm_size);

  for (int i = 0; i < qfld.nDOF(); i++)
  {
    int idof = qfld.local2nativeDOFmap(i);

    // Compute the rank that should receive the DOF
    int rank = MIN(comm_size-1, idof / DOFperProc);

    needDOF[rank].push_back(idof);
  }

  // send the requested DOFs to processors where they reside
  std::vector<std::vector<int>> sendDOF;
  boost::mpi::all_to_all(*qfld.getXField().comm(), needDOF, sendDOF);
  needDOF.clear();

  // gather the DOFs to send to other processors
  std::vector<std::map<int,T>> sendBuffer(comm_size);
  for (std::size_t rank = 0; rank < sendDOF.size(); rank++)
    for (const int& idof : sendDOF[rank])
      sendBuffer[rank][idof] = DOF.at(idof);

  // done with the dofs now
  DOF.clear();

  // send the DOFs to the processors that need them
  std::vector<std::map<int,T>> recvBuffer(comm_size);
  boost::mpi::all_to_all(*qfld.getXField().comm(), sendBuffer, recvBuffer);
  sendBuffer.clear();

  // collapse down the dofs from all processors
  std::map<int,T> buffer;
  for (std::size_t rank = 0; rank < recvBuffer.size(); rank++)
    buffer.insert(recvBuffer[rank].begin(), recvBuffer[rank].end());

  recvBuffer.clear();

  for (int i = 0; i < qfld.nDOF(); i++)
  {
    int idof = qfld.local2nativeDOFmap(i);
    qfld.DOF(i) = buffer.at(idof);
  }

#else
  //Read solution
  for (int k = 0; k < nDOFnative; k++)
    readLine_PX<T>(grm, qfld.DOF(k));
#endif


}

template<>
void writeLine_PX(std::fstream& writer, const Real& array)
{
  writer << array << std::endl;
}


template<>
void writeLine_PX(std::fstream& writer, const DLA::VectorS<1,Real>& array)
{
  for (int i=0; i<1; i++)
    writer << array[i] << " ";

  writer << std::endl;
}

template<>
void writeLine_PX(std::fstream& writer, const DLA::VectorS<2,Real>& array)
{
  for (int i=0; i<2; i++)
    writer << array[i] << " ";

  writer << std::endl;
}

template<>
void writeLine_PX(std::fstream& writer, const DLA::VectorS<3,Real>& array)
{
  for (int i=0; i<3; i++)
    writer << array[i] << " ";

  writer << std::endl;
}

template<>
void writeLine_PX(std::fstream& writer, const DLA::VectorS<4,Real>& array)
{
  for (int i=0; i<4; i++)
    writer << array[i] << " ";

  writer << std::endl;
}

template<>
void writeLine_PX(std::fstream& writer, const DLA::VectorS<5,Real>& array)
{
  for (int i=0; i<5; i++)
    writer << array[i] << " ";

  writer << std::endl;
}

template<>
void writeLine_PX(std::fstream& writer, const DLA::VectorS<6,Real>& array)
{
  for (int i=0; i<6; i++)
    writer << array[i] << " ";

  writer << std::endl;
}

template<>
void writeLine_PX(std::fstream& writer, const DLA::VectorS<7,Real>& array)
{
  for (int i=0; i<7; i++)
    writer << array[i] << " ";

  writer << std::endl;
}
// Boltzmann
template<>
void writeLine_PX(std::fstream& writer, const DLA::VectorS<NVar,Real>& array)
{
  for (int i=0; i<NVar; i++)
    writer << array[i] << " ";

  writer << std::endl;
}

template<>
void readLine_PX(std::ifstream& reader, Real& array)
{
  std::vector<double> data = _readerUtils::get_list<double>(reader);

  array = data[0];
}

template<>
void readLine_PX(std::ifstream& reader, DLA::VectorS<1,Real>& array)
{
  std::vector<double> data = _readerUtils::get_list<double>(reader);

  for (int i=0; i<1; i++)
    array[i] = data[i];
}

template<>
void readLine_PX(std::ifstream& reader, DLA::VectorS<2,Real>& array)
{
  std::vector<double> data = _readerUtils::get_list<double>(reader);

  for (int i=0; i<2; i++)
    array[i] = data[i];
}

template<>
void readLine_PX(std::ifstream& reader, DLA::VectorS<3,Real>& array)
{
  std::vector<double> data = _readerUtils::get_list<double>(reader);

  for (int i=0; i<3; i++)
    array[i] = data[i];
}

template<>
void readLine_PX(std::ifstream& reader, DLA::VectorS<4,Real>& array)
{
  std::vector<double> data = _readerUtils::get_list<double>(reader);

  for (int i=0; i<4; i++)
    array[i] = data[i];
}

template<>
void readLine_PX(std::ifstream& reader, DLA::VectorS<5,Real>& array)
{
  std::vector<double> data = _readerUtils::get_list<double>(reader);

  for (int i=0; i<5; i++)
    array[i] = data[i];
}

template<>
void readLine_PX(std::ifstream& reader, DLA::VectorS<6,Real>& array)
{
  std::vector<double> data = _readerUtils::get_list<double>(reader);

  for (int i=0; i<6; i++)
    array[i] = data[i];
}

template<>
void readLine_PX(std::ifstream& reader, DLA::VectorS<7,Real>& array)
{
  std::vector<double> data = _readerUtils::get_list<double>(reader);

  for (int i=0; i<7; i++)
    array[i] = data[i];
}
// Boltzmann
template<>
void readLine_PX(std::ifstream& reader, DLA::VectorS<NVar,Real>& array)
{
  std::vector<double> data = _readerUtils::get_list<double>(reader);

  for (int i=0; i<NVar; i++)
    array[i] = data[i];
}

//------------------------------------------------
//Explicit instantiations

typedef DLA::VectorS<1,Real> Vector1;
typedef DLA::VectorS<2,Real> Vector2;
typedef DLA::VectorS<3,Real> Vector3;
typedef DLA::VectorS<4,Real> Vector4;
typedef DLA::VectorS<5,Real> Vector5;
typedef DLA::VectorS<6,Real> Vector6;
typedef DLA::VectorS<7,Real> Vector7;

//Bolt
typedef DLA::VectorS<NVar,Real> VectorNVar;


//1D - scalar field
template void WriteSolution_PX<PhysD1, TopoD1, Real   >(const Field<PhysD1,TopoD1,Real   >& fld, const std::string& filename);
template void WriteSolution_PX<PhysD1, TopoD1, Vector1>(const Field<PhysD1,TopoD1,Vector1>& fld, const std::string& filename);
template void WriteSolution_PX<PhysD1, TopoD1, Vector2>(const Field<PhysD1,TopoD1,Vector2>& fld, const std::string& filename);
template void WriteSolution_PX<PhysD1, TopoD1, Vector3>(const Field<PhysD1,TopoD1,Vector3>& fld, const std::string& filename);
template void WriteSolution_PX<PhysD1, TopoD1, Vector4>(const Field<PhysD1,TopoD1,Vector4>& fld, const std::string& filename);

//2D - scalar field
template void WriteSolution_PX<PhysD2, TopoD2, Real   >(const Field<PhysD2,TopoD2,Real   >& fld, const std::string& filename);
template void WriteSolution_PX<PhysD2, TopoD2, Vector1>(const Field<PhysD2,TopoD2,Vector1>& fld, const std::string& filename);

//2D - vector field
template void WriteSolution_PX<PhysD2, TopoD2, Vector2>(const Field<PhysD2,TopoD2,Vector2>& fld, const std::string& filename);
template void WriteSolution_PX<PhysD2, TopoD2, Vector3>(const Field<PhysD2,TopoD2,Vector3>& fld, const std::string& filename);
template void WriteSolution_PX<PhysD2, TopoD2, Vector4>(const Field<PhysD2,TopoD2,Vector4>& fld, const std::string& filename);
template void WriteSolution_PX<PhysD2, TopoD2, Vector5>(const Field<PhysD2,TopoD2,Vector5>& fld, const std::string& filename);
template void WriteSolution_PX<PhysD2, TopoD2, Vector6>(const Field<PhysD2,TopoD2,Vector6>& fld, const std::string& filename);
template void WriteSolution_PX<PhysD2, TopoD2, VectorNVar>(const Field<PhysD2,TopoD2,VectorNVar>& fld, const std::string& filename);

//3D - scalar field
template void WriteSolution_PX<PhysD3, TopoD3, Real   >(const Field<PhysD3,TopoD3,Real   >& fld, const std::string& filename);
template void WriteSolution_PX<PhysD3, TopoD3, Vector1>(const Field<PhysD3,TopoD3,Vector1>& fld, const std::string& filename);

//3D - vector field
template void WriteSolution_PX<PhysD3, TopoD3, Vector2>(const Field<PhysD3,TopoD3,Vector2>& fld, const std::string& filename);
template void WriteSolution_PX<PhysD3, TopoD3, Vector3>(const Field<PhysD3,TopoD3,Vector3>& fld, const std::string& filename);
template void WriteSolution_PX<PhysD3, TopoD3, Vector4>(const Field<PhysD3,TopoD3,Vector4>& fld, const std::string& filename);
template void WriteSolution_PX<PhysD3, TopoD3, Vector5>(const Field<PhysD3,TopoD3,Vector5>& fld, const std::string& filename);
template void WriteSolution_PX<PhysD3, TopoD3, Vector6>(const Field<PhysD3,TopoD3,Vector6>& fld, const std::string& filename);
template void WriteSolution_PX<PhysD3, TopoD3, Vector7>(const Field<PhysD3,TopoD3,Vector7>& fld, const std::string& filename);

//4D - scalar field
template void WriteSolution_PX<PhysD4, TopoD4, Real   >(const Field<PhysD4,TopoD4,Real   >& fld, const std::string& filename);
template void WriteSolution_PX<PhysD4, TopoD4, Vector1>(const Field<PhysD4,TopoD4,Vector1>& fld, const std::string& filename);


//1D - scalar field
template void ReadSolution_PX<PhysD1, TopoD1, Real   >(const std::string& filename, Field<PhysD1,TopoD1,Real   >& fld);
template void ReadSolution_PX<PhysD1, TopoD1, Vector1>(const std::string& filename, Field<PhysD1,TopoD1,Vector1>& fld);
template void ReadSolution_PX<PhysD1, TopoD1, Vector2>(const std::string& filename, Field<PhysD1,TopoD1,Vector2>& fld);
template void ReadSolution_PX<PhysD1, TopoD1, Vector3>(const std::string& filename, Field<PhysD1,TopoD1,Vector3>& fld);
template void ReadSolution_PX<PhysD1, TopoD1, Vector4>(const std::string& filename, Field<PhysD1,TopoD1,Vector4>& fld);

//2D - scalar field
template void ReadSolution_PX<PhysD2, TopoD2, Real   >(const std::string& filename, Field<PhysD2,TopoD2,Real   >& fld);
template void ReadSolution_PX<PhysD2, TopoD2, Vector1>(const std::string& filename, Field<PhysD2,TopoD2,Vector1>& fld);

//2D - vector field
template void ReadSolution_PX<PhysD2, TopoD2, Vector2>(const std::string& filename, Field<PhysD2,TopoD2,Vector2>& fld);
template void ReadSolution_PX<PhysD2, TopoD2, Vector3>(const std::string& filename, Field<PhysD2,TopoD2,Vector3>& fld);
template void ReadSolution_PX<PhysD2, TopoD2, Vector4>(const std::string& filename, Field<PhysD2,TopoD2,Vector4>& fld);
template void ReadSolution_PX<PhysD2, TopoD2, Vector5>(const std::string& filename, Field<PhysD2,TopoD2,Vector5>& fld);
template void ReadSolution_PX<PhysD2, TopoD2, Vector6>(const std::string& filename, Field<PhysD2,TopoD2,Vector6>& fld);
template void ReadSolution_PX<PhysD2, TopoD2, VectorNVar>(const std::string& filename, Field<PhysD2,TopoD2,VectorNVar>& fld);

//3D - scalar field
template void ReadSolution_PX<PhysD3, TopoD3, Real   >(const std::string& filename, Field<PhysD3,TopoD3,Real   >& fld );
template void ReadSolution_PX<PhysD3, TopoD3, Vector1>(const std::string& filename, Field<PhysD3,TopoD3,Vector1>& fld );

//3D - vector field
template void ReadSolution_PX<PhysD3, TopoD3, Vector2>(const std::string& filename, Field<PhysD3,TopoD3,Vector2>& fld );
template void ReadSolution_PX<PhysD3, TopoD3, Vector3>(const std::string& filename, Field<PhysD3,TopoD3,Vector3>& fld );
template void ReadSolution_PX<PhysD3, TopoD3, Vector4>(const std::string& filename, Field<PhysD3,TopoD3,Vector4>& fld );
template void ReadSolution_PX<PhysD3, TopoD3, Vector5>(const std::string& filename, Field<PhysD3,TopoD3,Vector5>& fld );
template void ReadSolution_PX<PhysD3, TopoD3, Vector6>(const std::string& filename, Field<PhysD3,TopoD3,Vector6>& fld );
template void ReadSolution_PX<PhysD3, TopoD3, Vector7>(const std::string& filename, Field<PhysD3,TopoD3,Vector7>& fld );

//4D - scalar field
template void ReadSolution_PX<PhysD4, TopoD4, Real   >(const std::string& filename, Field<PhysD4,TopoD4,Real   >& fld );
template void ReadSolution_PX<PhysD4, TopoD4, Vector1>(const std::string& filename, Field<PhysD4,TopoD4,Vector1>& fld );

}
