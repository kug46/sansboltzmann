// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField_PX.h"

#include <fstream>
#include <cstring>
#include <algorithm>
#include <iomanip> // std::setprecision
#include <map>
#include <vector>
#include <sstream>
#include <array>

#include "Field/XFieldLine.h"   //Needed to instantiate an area XField
#include "Field/XFieldArea.h"   //Needed to instantiate an area XField
#include "Field/XFieldVolume.h" //Needed to instantiate a volume XField
#include "Field/XFieldSpacetime.h" //Needed to instantiate a volume XField
#include "Field/Partition/XField_Lagrange.h"

#include "Field/tools/for_each_CellGroup.h"
#include "Field/tools/for_each_BoundaryTraceGroup.h"

#include "BasisFunction/LagrangeDOFMap.h"

#include "tools/file_extension.h"
#include "tools/file_skiplines.h"
#include "tools/split_string.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_MPI
#include "MPI/continuousElementMap.h"
#include "MPI/serialize_DenseLinAlg_MatrixS.h"

#include <boost/mpi/collectives/all_reduce.hpp>
#include <boost/mpi/collectives/gather.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/vector.hpp>

// handles serialization of arrays, depending on version of boost
#include "MPI/boost_serialization_array.h"

#endif // SANS_MPI

namespace SANS
{

template<class Topology>
struct GrmShape;

template<> struct GrmShape<Node>     { constexpr static const char* name = "PXE_Shape_Node"; };
template<> struct GrmShape<Line>     { constexpr static const char* name = "PXE_Shape_Edge"; };
template<> struct GrmShape<Triangle> { constexpr static const char* name = "PXE_Shape_Triangle"; };
template<> struct GrmShape<Quad>     { constexpr static const char* name = "PXE_Shape_Quad"; };
template<> struct GrmShape<Tet>      { constexpr static const char* name = "PXE_Shape_Tet"; };
template<> struct GrmShape<Hex>      { constexpr static const char* name = "PXE_Shape_Hex"; };
template<> struct GrmShape<Pentatope>      { constexpr static const char* name = "PXE_Shape_Pentatope"; };

/*\
 *
 *  Reader utilities (internal namespace not meant to be used by client code)
 *  for making life easier when reading files.
 *  Exposes functions for getting the next (non-empty) line, splitting a
 *  line by a delimiter.
 *  Functions intended to be used are get_value<type> and get_list<type>.
 *
\*/
namespace _readerUtils
{


  // utility function for getting the next (non-empty) line in a file as a vector
  // of strings broken by the spaces in the line
  inline std::vector<std::string>
  get_spaced_line(std::ifstream& input)
  {
    std::string line;
    while (std::getline(input,line))
      if (!line.empty()) break;

    return split_string(line," ");
  }

  template <typename type> inline type convert2( const std::string& s );

  template<>
  inline int
  convert2<int>( const std::string &s ) { return std::stoi( s ); }

  template<>
  inline double
  convert2<double>( const std::string &s ) { return std::stod( s ); }

  // utility function for getting a typed list as the next line in the file
  template <typename type>
  inline std::vector<type>
  get_list(std::ifstream& input)
  {
    std::vector<std::string> sdata = get_spaced_line(input);
    std::vector<type> tdata(sdata.size());
    for (std::size_t i = 0; i < sdata.size(); i++)
      tdata[i] = convert2<type>(sdata[i]);

    return tdata;
  }

  template <typename type>
  inline type
  get_value(std::ifstream& input)
  {
    std::vector<type> vals = get_list<type>(input);
    SANS_ASSERT( vals.size() == 1 );
    return vals[0];
  }

  inline std::string
  get_string( std::ifstream& input )
  {
    std::vector<std::string> vals = get_spaced_line(input);
    SANS_ASSERT( vals.size() == 1 );
    return vals[0];
  }

  // utility to convert PX indexing to SANS
  inline std::vector<int>
  getPXtoSANSmap( const TopologyTypes topo, const int order)
  {
    if (topo==eLine)
    {
      // PX   :  0----1----2---3
      // SANS : n0----2----3---n1
      std::size_t n = LagrangeDOFMap<Line>::nBasis(order);
      std::vector<int> PXtoSANS(n);
      PXtoSANS[0] = 0;
      PXtoSANS[1] = n-1;
      for (std::size_t i = 2; i < n; i++) PXtoSANS[i] = i-1;
      return PXtoSANS;
    }
    else if (topo == eTriangle)
    {
      // PX and SANS mapping is the same for triangles
      std::size_t n = LagrangeDOFMap<Triangle>::nBasis(order);
      std::vector<int> PXtoSANS(n);
      for (std::size_t i = 0; i < n; i++) PXtoSANS[i] = i;
      return PXtoSANS;
    }
    else if (topo == eTet)
    {
      // PX numbering is like a structured grid
      // see
      // PXShape.c : PXShapeLagrange3d
      // PXOrder.c : PXNodeTet
      // In SANS, see BasisFunctionVolume_Tetrahedon_Legendre.cpp
      if (order == 1)
      {
        // Identity
        return {0, 1, 2, 3};
      }
      else if (order == 2)
      {
        // Structured nodes reordered to SANS
        return {0, 2, 5, 9, 8, 7, 4, 3, 6, 1};
      }
      else if (order == 3)
      {
        // Structured nodes reordered to SANS
        return {0, 3, 9, 19, 15, 18, 17, 12, 6, 8, 7, 4, 10, 16, 1, 2, 14, 13, 11, 5};
      }
      else if (order == 4)
      {
        // Structured nodes reordered to SANS
        return {0, 4, 14, 34, 24, 30, 33, 32, 27, 18, 8, 11, 13, 12, 9, 5, 15, 25, 31, 1, 2, 3, 21, 23, 29, 19, 28, 22, 16, 17, 26, 6, 10, 7, 20};
      }
      else
        SANS_DEVELOPER_EXCEPTION( "PX-to-SANS indexing not implemented for tets of order = %d", order );
    }
    else if (topo == eQuad)
    {
      // PX numbering is tensor project
      // see PXShape.c: PXShapeQuadUniformLagrange2d
      if (order == 1)
      {
        return {0, 1, 3, 2};
      }
      else if (order == 2)
      {
        return {0,2,8, 6,1,5, 7,3,4};
      }
      else if (order == 3)
      {
        return {0,3,15,12, 1,2,7,11, 14,13,8,4, 5,6,10,9};
      }
      else if (order == 4)
      {
        return {0,4,24,20, 1,2,3, 9,14,19, 23,22,21, 15,10,5, 6,8,18,16, 7,13,17,11, 12};
      }
      else
        SANS_DEVELOPER_EXCEPTION( "PX-to-SANS indexing not implemented for quads of order = %d", order );
    }
    else if (topo == eHex)
    {
      SANS_DEVELOPER_EXCEPTION( "PX-to-SANS indexing not implemented for hexs" );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown shape type" );

    return {};
  }
} // namespace _readerUtils


template <class PhysDim, class TopoDim>
void
XField_PX<PhysDim,TopoDim>::readDOFs( std::ifstream& input, const int nDOF, const int dim, XField_Lagrange<PhysDim>& xfldin )
{
  typename XField_Lagrange<PhysDim>::VectorX X;
  SANS_ASSERT( PhysDim::D == dim );

  for (int i = 0; i < nDOF; i++)
  {
    std::vector<double> data = _readerUtils::get_list<double>(input);

    if (data.size() != PhysDim::D)
    {
      std::stringstream ss;
      ss << "Expected to read grid coordinate of dimension " << PhysDim::D << "." << std::endl;
      ss << "Instead read: ";
      for (std::size_t i = 0; i < data.size(); i++)
        ss << data[i] << " ";
      ss << std::endl;

      SANS_RUNTIME_EXCEPTION( ss.str() );
    }

    for (int k = 0; k < PhysDim::D; k++)
      X[k] = data[k];

    // add the DOF to the parallel grid
    xfldin.addDOF(X);
  }
}

/*\
 *
 *  Read the cell group from the file.
 *
\*/
template<class PhysDim>
inline void
readCellGroup( std::ifstream& input, const int group, const TopologyTypes& shape,
               const int ncells, const int order, XField_Lagrange<PhysDim>& xfldin )
{
  std::vector<int> PXtoSANS = _readerUtils::getPXtoSANSmap(shape, order);
  std::vector<int> SANSnodes(PXtoSANS.size());

  for (int i = 0; i < ncells; i++)
  {
    // read the cell indices from the file
    std::vector<int> PXnodes = _readerUtils::get_list<int>(input);

    // decrement the indices because they start at 1
    for (std::size_t j = 0; j < PXtoSANS.size(); j++)
      SANSnodes[j] = PXnodes[PXtoSANS[j]] - 1;

    // set the indices for this element
    xfldin.addCell(group, shape, order, SANSnodes);
  }
}

template<class PhysDim>
inline void
readBndGroup( std::ifstream& input, const int group, const TopologyTypes& shape,
              const int nElem, XField_Lagrange<PhysDim>& xfldin )
{
  for (int i = 0; i < nElem; i++)
  {
    // read the boundary node indices from the file
    std::vector<int> nodes = _readerUtils::get_list<int>(input);

    // decrement the indices because they start at 1
    for (std::size_t j = 0; j < nodes.size(); j++)
      nodes[j]--;

    // set the indices for this element
    xfldin.addBoundaryTrace(group, shape, nodes);
  }
}


template<class PhysDim, class TopoDim>
void
XField_PX<PhysDim,TopoDim>::readGrm( const std::string& filename, XField_Lagrange<PhysDim>& xfldin )
{
  std::shared_ptr<mpi::communicator> comm = xfldin.comm();
  std::ifstream grm;

  int dim = 0;
  int nPoints = 0;
  int nCellGroup = 0;
  int nBTraceGroup = 0;

  // only rank 0 will read the grid
  if (comm->rank() == 0)
  {
    // open the file
    grm.open(filename.c_str());
    if (!grm.is_open())
      SANS_RUNTIME_EXCEPTION( "XField<PhysDim,TopoDim>::readGrm - Error opening file: %s", filename.c_str() );

    // read the header
    std::vector<int> data = _readerUtils::get_list<int>(grm);
    if (data.size() != 4)
      SANS_RUNTIME_EXCEPTION("Expected 4 integers in first line of %s, found %d", filename.c_str(), (int)data.size());
    dim = data[0];
    nPoints = data[1];
    nCellGroup = data[2];
    nBTraceGroup = data[3];
    printf("dim = %d, nPoints = %d, nCellGroup = %d, nBTraceGroup = %d\n",dim,nPoints,nCellGroup,nBTraceGroup);
    SANS_ASSERT(dim == PhysDim::D);
  }

  // start the process for reading points
  xfldin.sizeDOF( nPoints );

  // only rank 0 reads the grid points
  if (comm->rank() == 0)
    readDOFs( grm, nPoints, dim, xfldin );

  // we need to count the total number of cell and boundary elements before reading them
  // would be nice if those numbers were in the header...
  int posBnd = 0, posCell = 0;
  int nBoundaryTraceTotal = 0;
  int nCellTotal = 0;

  if (comm->rank() == 0)
  {
    // save the file position of the boundary elements
    posBnd = grm.tellg();

    // skip the boundary groups to count the total number of boundary elements
    for (int group = 0; group < nBTraceGroup; group++)
    {
      // get how many elements to read
      int nElem = _readerUtils::get_value<int>(grm);

      // add to the total number of boundary trace elements
      nBoundaryTraceTotal += nElem;

      // get which shape
      _readerUtils::get_string(grm);

      // skip the boundary trace group
      file_skiplines( grm, nElem );
    }

    // save the position of the cell elements
    posCell = grm.tellg();

    // skip the cell groups to count the total number of cells
    for (int group = 0; group < nCellGroup; group++)
    {
      // get how many cells to read
      int ncells = _readerUtils::get_value<int>(grm);

      // add to the total number of cell elements
      nCellTotal += ncells;

      if ( group == nCellGroup - 1) break; // don't need to read the last group, just need the count

      // get the order of the cells
      _readerUtils::get_value<int>(grm);

      // get which shape we will read
      _readerUtils::get_string(grm);

      // assert we have UniformNodeDistribution
      std::string nodeDistribution = _readerUtils::get_string(grm);
      SANS_ASSERT( nodeDistribution == "UniformNodeDistribution" );

      // skip the cell group based on the particular shape
      file_skiplines( grm, ncells );
    }
  }

  // Start the process of adding cells on all processors
  xfldin.sizeCells(nCellTotal);

  if (comm->rank() == 0)
  {
    // go back and read the cells
    grm.seekg( posCell );

    // read the cell groups
    for (int group = 0; group < nCellGroup; group++)
    {
      // get how many cells to read
      int ncells = _readerUtils::get_value<int>(grm);

      // get the order of the cells
      int order = _readerUtils::get_value<int>(grm);

      // get which shape we will read
      std::string shape = _readerUtils::get_string(grm);

      printf("Cell group %d reading %d %s cells of order %d\n", group, ncells, shape.c_str(), order);

      // assert we have UniformNodeDistribution
      std::string nodeDistribution = _readerUtils::get_string(grm);
      SANS_ASSERT( nodeDistribution == "UniformNodeDistribution" );

      // read the cell group based on the particular shape
      if (shape == GrmShape<Triangle>::name)
      {
        readCellGroup(grm, group, eTriangle, ncells, order, xfldin );
      }
      else if (shape == GrmShape<Quad>::name)
      {
        readCellGroup(grm, group, eQuad, ncells, order, xfldin );
      }
      else if (shape == GrmShape<Tet>::name)
      {
        readCellGroup(grm, group, eTet, ncells, order, xfldin );
      }
      else if (shape == GrmShape<Pentatope>::name)
      {
        readCellGroup(grm, group, ePentatope, ncells, order, xfldin );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown element group shape type" );
    }
  }

  // Start the process of boundary trace elements
  xfldin.sizeBoundaryTrace(nBoundaryTraceTotal);

  if (comm->rank() == 0)
  {
    // go back and read the boundary elements
    grm.seekg( posBnd );

    for (int group = 0; group < nBTraceGroup; group++)
    {
      // get how many buondary elements to read
      int nElem = _readerUtils::get_value<int>(grm);

      // get which shape we will read
      std::string shape = _readerUtils::get_string(grm);

      printf("Boundary %d reading %d %s traces\n", group, nElem, shape.c_str());

      // read the boundary trace group based on the particular shape
      if (shape == GrmShape<Line>::name)
      {
        readBndGroup( grm, group, eLine, nElem, xfldin );
      }
      else if (shape == GrmShape<Triangle>::name)
      {
        readBndGroup( grm, group, eTriangle, nElem, xfldin );
      }
      else if (shape == GrmShape<Quad>::name)
      {
        readBndGroup( grm, group, eQuad, nElem, xfldin );
      }
      else if (shape == GrmShape<Tet>::name)
      {
        readBndGroup( grm, group, eTet, nElem, xfldin );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown boundary group shape type: %s", shape.c_str() );
    }
  }
}

template<class PhysDim, class TopoDim>
void
XField_PX<PhysDim,TopoDim>::readGri( const std::string& filename, XField_Lagrange<PhysDim>& xfldin )
{
  std::shared_ptr<mpi::communicator> comm = xfldin.comm();

  int dim = 0;
  int nPoints = 0;
  int nCells = 0;

  std::ifstream gri;

  // only rank 0 will read the grid
  if (comm->rank() == 0)
  {
    gri.open(filename.c_str());
    SANS_ASSERT_MSG( gri.is_open() , "XField<PhysDim,TopoDim>::readGri - Error opening file: %s", filename.c_str() );

    // read the header
    std::vector<int> data = _readerUtils::get_list<int>(gri);

    SANS_ASSERT_MSG( data.size() == 3, "data.size() = %d", data.size() );

    nPoints = data[0];
    nCells = data[1];
    dim = data[2];
    printf("dim = %d, nPoints = %d, nCells = %d\n",dim,nPoints,nCells);
    SANS_ASSERT(dim == PhysDim::D);
  }

  // start the process for reading points on all processors
  xfldin.sizeDOF( nPoints );

  // only rank 0 reads the grid points
  if (comm->rank() == 0)
    readDOFs( gri, nPoints, dim, xfldin );

  // read in the number of boundary trace groups
  int nBTraceGroup = 0;
  if (comm->rank() == 0)
    nBTraceGroup = _readerUtils::get_value<int>(gri);

  // we need to count the total number of cell and boundary elements before reading them
  // would be nice if those numbers were in the header...
  int posBnd = 0;
  int nBoundaryTraceTotal = 0;

  if (comm->rank() == 0)
  {
    // save the file position of the boundary elements
    posBnd = gri.tellg();

    // skip the boundary groups to count the total number of boundary elements
    for (int i = 0; i < nBTraceGroup; i++)
    {
      // get how many elements to read
      int nElem = _readerUtils::get_value<int>(gri);

      // add to the total number of boundary trace elements
      nBoundaryTraceTotal += nElem;

      // skip the boundary trace group
      file_skiplines( gri, nElem );
    }
  }

  // Start the process of adding cells
  xfldin.sizeCells(nCells);

  if (comm->rank() == 0)
  {
    int cellGlobal = 0;
    int group = 0;
    while (cellGlobal < nCells)
    {
      // get how many cells to read and order
      std::vector<int> data = _readerUtils::get_list<int>(gri);

      SANS_ASSERT_MSG( data.size() == 2, "data.size() = %d", data.size() );

      int ncells = data[0];
      int order  = data[1];
      // read the cell group based on the dimension
      if (dim == 2)
      {
        readCellGroup(gri, group++, eTriangle, ncells, order, xfldin );
      }
      else if (dim == 3)
      {
        readCellGroup(gri, group++, eTet, ncells, order, xfldin );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unsupported dimension: %d", dim );

      // increment the number of cells that have been read
      cellGlobal += ncells;
    }
  }

  // Start the process of boundary trace elements
  xfldin.sizeBoundaryTrace(nBoundaryTraceTotal);

  if (comm->rank() == 0)
  {
    gri.seekg( posBnd ); // go back to the boundary groups

    for (int i = 0; i < nBTraceGroup; i++)
    {
      // get how many boundary elements to read
      int nElem = _readerUtils::get_value<int>(gri);
      if (nElem == 0) continue; // just in case

      // read the boundary trace group based on the particular shape
      if (dim == 2)
      {
        readBndGroup( gri, i, eLine, nElem, xfldin );
      }
      else if (dim == 3)
      {
        readBndGroup( gri, i, eTriangle, nElem, xfldin );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unsupported dimension: %d", dim );
    }
  }
}

//===========================================================================//
// Routines for writing grm file
//===========================================================================//

template <class PhysDim, class TopoDim>
class writeBoundaryTraceGroupsGrm : public GroupFunctorBoundaryTraceType< writeBoundaryTraceGroupsGrm<PhysDim,TopoDim> >
{
public:
  writeBoundaryTraceGroupsGrm( const XField<PhysDim,TopoDim>& xfld, std::fstream& writer, const int nBTraceGroups )
    : xfld_(xfld), writer_(writer), nBTraceGroups_(nBTraceGroups) {}

  std::size_t nBoundaryTraceGroups() const { return nBTraceGroups_; }
  std::size_t boundaryTraceGroup(const int n) const { return n; }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology >
  void
  apply(const typename XField<PhysDim,TopoDim>::template FieldTraceGroupType<Topology>& xfldBTrace,
        const int boundaryTraceGroupGlobal)
  {
    int comm_rank = xfld_.comm()->rank();

    // number of elements on the local processor
    int nElem = xfldBTrace.nElem();

#ifdef SANS_MPI
    const std::vector<int>& boundaryTraceIDs = xfld_.boundaryTraceIDs(boundaryTraceGroupGlobal);
    std::map<int,int> globalElemMap;
    continuousElementMap( *xfld_.comm(), boundaryTraceIDs, xfldBTrace, nElem, globalElemMap);
#endif

    if (comm_rank == 0)
    {
      writer_ << nElem << std::endl;
      writer_ << GrmShape<Topology>::name << std::endl;
    }

#ifdef SANS_MPI
    int comm_size = xfld_.comm()->size();
    int nElemLocal = xfldBTrace.nElem();

    // maximum element chunk size that rank 0 will write at any given time
    int Elemchunk = nElem / comm_size + nElem % comm_size;

    std::array<int,Topology::NNode> nodeDOFmap;

    // send one chunk of elements at a time to rank 0
    for (int rank = 0; rank < comm_size; rank++)
    {
      int elemLow  =  rank   *Elemchunk;
      int elemHigh = (rank+1)*Elemchunk;

      std::map<int,std::array<int,Topology::NNode>> buffer;

      for (int elem = 0; elem < nElemLocal; elem++)
      {
        int elemID = globalElemMap[boundaryTraceIDs[elem]];

        if (elemID >= elemLow && elemID < elemHigh &&
            xfldBTrace.associativity(elem).rank() == comm_rank)
        {
          // grm format only wants linear component of boundary elements
          xfldBTrace.associativity(elem).getNodeGlobalMapping(nodeDOFmap.data(), Topology::NNode);

          // transform back to native DOF indexing and increment the indices because they start at 1
          for (int i = 0; i < Topology::NNode; i++)
            nodeDOFmap[i] = xfld_.local2nativeDOFmap(nodeDOFmap[i]) + 1;

          buffer[boundaryTraceIDs[elem]] = nodeDOFmap;
        }
      }

      if (comm_rank == 0)
      {
        std::vector<std::map<int,std::array<int,Topology::NNode>>> bufferOnRank;
        boost::mpi::gather(*xfld_.comm(), buffer, bufferOnRank, 0 );

        // collapse down the buffer from all other ranks
        for (std::size_t i = 1; i < bufferOnRank.size(); i++)
          buffer.insert(bufferOnRank[i].begin(), bufferOnRank[i].end());

        //Write the node data
        for ( const auto& PXnodesPair : buffer)
        {
          for (int n = 0; n < Topology::NNode-1; n++)
            writer_ << PXnodesPair.second[n] << " ";
          writer_ << PXnodesPair.second[Topology::NNode-1] << std::endl;
        }
      }
#ifndef __clang_analyzer__
      else // send the buffer to rank 0
        boost::mpi::gather(*xfld_.comm(), buffer, 0 );
#endif
    } // for rank
#else
    // loop over traces within group
    for (int elem = 0; elem < nElem; elem++)
    {
      // grm format only wants linear component of boundary elements
      int nodeDOFmap[Topology::NNode];
      xfldBTrace.associativity(elem).getNodeGlobalMapping(nodeDOFmap, Topology::NNode);

      for (int n = 0; n < Topology::NNode-1; n++)
        writer_ << nodeDOFmap[n]+1 << " ";
      writer_ << nodeDOFmap[Topology::NNode-1]+1 << std::endl;
    }
#endif
  }

protected:
  const XField<PhysDim,TopoDim>& xfld_;
  std::fstream& writer_;
  const int nBTraceGroups_;
};

template <class PhysDim, class TopoDim>
class writeCellGroupsGrm : public GroupFunctorCellType< writeCellGroupsGrm<PhysDim,TopoDim> >
{
public:
  writeCellGroupsGrm( const XField<PhysDim,TopoDim>& xfld, std::fstream& writer, const int nCellGroups )
    : xfld_(xfld), writer_(writer), nCellGroups_(nCellGroups) {}

  std::size_t nCellGroups() const          { return nCellGroups_; }
  std::size_t cellGroup(const int n) const { return n;            }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology>
  void
  apply(const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
        const int cellGroupGlobal)
  {
    int comm_rank = xfld_.comm()->rank();

    // number of elements on the local processor
    int nElem = xfldCellGroup.nElem();
    int Qorder = xfldCellGroup.order();
    int nBasis = xfldCellGroup.nBasis();

#ifdef SANS_MPI
    // construct a continuous element ID map for the elements in the current group
    const std::vector<int>& cellIDs = xfld_.cellIDs(cellGroupGlobal);
    std::map<int,int> globalElemMap;
    continuousElementMap( *xfld_.comm(), cellIDs, xfldCellGroup, nElem, globalElemMap);
#endif

    std::vector<int> PXtoSANS = _readerUtils::getPXtoSANSmap(Topology::Topology, Qorder);
    std::vector<int> SANSnodes(PXtoSANS.size());
    std::vector<int> PXnodes(PXtoSANS.size());

    if (comm_rank == 0)
    {
      writer_ << nElem << std::endl;
      writer_ << Qorder << std::endl;
      writer_ << GrmShape<Topology>::name << std::endl;
      writer_ << "UniformNodeDistribution" << std::endl;
    }

#ifdef SANS_MPI
    int comm_size = xfld_.comm()->size();
    int nElemLocal = xfldCellGroup.nElem();

    // maximum element chunk size that rank 0 will write at any given time
    int Elemchunk = nElem / comm_size + nElem % comm_size;

    // send one chunk of elements at a time to rank 0
    for (int rank = 0; rank < comm_size; rank++)
    {
      int elemLow  =  rank   *Elemchunk;
      int elemHigh = (rank+1)*Elemchunk;

      std::map<int,std::vector<int>> buffer;

      for (int elem = 0; elem < nElemLocal; elem++)
      {
        int elemID = globalElemMap[cellIDs[elem]];

        if (elemID >= elemLow && elemID < elemHigh &&
            xfldCellGroup.associativity(elem).rank() == comm_rank)
        {
          xfldCellGroup.associativity(elem).getGlobalMapping(SANSnodes.data(), SANSnodes.size());

          // transform back to native DOF indexing
          for (std::size_t i = 0; i < SANSnodes.size(); i++)
            SANSnodes[i] = xfld_.local2nativeDOFmap(SANSnodes[i]);

          // transform to PX order and increment the indices because they start at 1
          for (std::size_t j = 0; j < PXtoSANS.size(); j++)
            PXnodes[PXtoSANS[j]] = SANSnodes[j] + 1;

          buffer[cellIDs[elem]] = PXnodes;
        }
      }

      if (comm_rank == 0)
      {
        std::vector<std::map<int,std::vector<int>>> bufferOnRank;
        boost::mpi::gather(*xfld_.comm(), buffer, bufferOnRank, 0 );

        // collapse down the buffer from all other ranks
        for (std::size_t i = 1; i < bufferOnRank.size(); i++)
          buffer.insert(bufferOnRank[i].begin(), bufferOnRank[i].end());

        //Write the node data
        for ( const auto& PXnodesPair : buffer)
        {
          for (int n = 0; n < nBasis-1; n++)
            writer_ << PXnodesPair.second[n] << " ";
          writer_ << PXnodesPair.second[nBasis-1] << std::endl;
        }
      }
#ifndef __clang_analyzer__
      else // send the buffer to rank 0
        boost::mpi::gather(*xfld_.comm(), buffer, 0 );
#endif
    } // for rank

#else
    // loop over cells within group
    for (int elem = 0; elem < nElem; elem++)
    {
      xfldCellGroup.associativity(elem).getGlobalMapping(SANSnodes.data(), SANSnodes.size());

      // transform and increment the indices because they start at 1
      for (std::size_t j = 0; j < PXtoSANS.size(); j++)
        PXnodes[PXtoSANS[j]] = SANSnodes[j] + 1;

      for (int n = 0; n < nBasis-1; n++)
        writer_ << PXnodes[n] << " ";
      writer_ << PXnodes[nBasis-1] << std::endl;
    }
#endif
  }

protected:
  const XField<PhysDim,TopoDim>& xfld_;
  std::fstream& writer_;
  const int nCellGroups_;
};


template<class PhysDim, class TopoDim>
void WriteMeshGrm(const XField<PhysDim,TopoDim>& xfld, const std::string& filename)
{
  int comm_rank = xfld.comm()->rank();
  std::fstream writer;

  if (comm_rank == 0)
  {
    writer.open( filename, std::fstream::out );

    if (!writer.good())
      SANS_RUNTIME_EXCEPTION( "Error writing to file: %s", filename.c_str() );
  }

  const int nCellGroup = xfld.nCellGroups();
  const int nBTraceGroup = xfld.nBoundaryTraceGroups();

  //Only works for Lagrange basis functions or P1 Hierarchical (which is identical to Lagrange P1)
  for (int group = 0; group < nCellGroup; group++)
    SANS_ASSERT(  xfld.getCellGroupBase(group).basisCategory() == BasisFunctionCategory_Lagrange ||
                 (xfld.getCellGroupBase(group).basisCategory() == BasisFunctionCategory_Hierarchical &&
                  xfld.getCellGroupBase(group).order() == 1) );

  //--------------------------------------------------//

  int nDOFnative = xfld.nDOFnative();

  if (comm_rank == 0)
  {
    //Write the header
    writer << PhysDim::D << " " << nDOFnative << " " << nCellGroup << " " << nBTraceGroup << std::endl;
    writer << std::scientific << std::setprecision(16);
  }

#ifdef SANS_MPI
  typedef DLA::VectorS<PhysDim::D,Real> VectorX;

  int comm_size = xfld.comm()->size();

  // maximum DOF chunk size that rank 0 will write at any given time
  int DOFchunk = nDOFnative / comm_size + nDOFnative % comm_size;

  for (int rank = 0; rank < comm_size; rank++)
  {
    int DOFlow = rank*DOFchunk;
    int DOFhigh = (rank+1)*DOFchunk;

    std::map<int,VectorX> buffer;

    for (int i = 0; i < xfld.nDOF(); i++)
    {
      int iDOF = xfld.local2nativeDOFmap(i);
      if (iDOF >= DOFlow && iDOF < DOFhigh)
        buffer[iDOF] = xfld.DOF(i);
    }

    if (comm_rank == 0)
    {
      std::vector<std::map<int,VectorX>> bufferOnRank;
      boost::mpi::gather(*xfld.comm(), buffer, bufferOnRank, 0 );

      // collapse down the buffer from all ranks (this remove any possible duplicates)
      for (std::size_t i = 0; i < bufferOnRank.size(); i++)
        buffer.insert(bufferOnRank[i].begin(), bufferOnRank[i].end());

      //Write the node data
      for ( const auto& DOFpair : buffer)
      {
        for (int d = 0; d < PhysDim::D-1; d++)
          writer << DOFpair.second[d] << " ";
        writer << DOFpair.second[PhysDim::D-1] << std::endl;
      }

    }
#ifndef __clang_analyzer__
    else // send the buffer to rank 0
      boost::mpi::gather(*xfld.comm(), buffer, 0 );
#endif
  } // for rank

#else
  //Write the node data
  for (int k = 0; k < nDOFnative; k++)
  {
    for (int d = 0; d < PhysDim::D-1; d++)
      writer << xfld.DOF(k)[d] << " ";
    writer << xfld.DOF(k)[PhysDim::D-1] << std::endl;
  }
#endif

  // loop over boundary groups and write out the connectivity
  for_each_BoundaryTraceGroup<TopoDim>::apply(
      writeBoundaryTraceGroupsGrm<PhysDim, TopoDim>(xfld, writer, nBTraceGroup), xfld );

  // loop over cell groups and write out the connectivity
  for_each_CellGroup<TopoDim>::apply( writeCellGroupsGrm<PhysDim, TopoDim>(xfld, writer, nCellGroup), xfld );

  if (comm_rank == 0)
  {
    writer << std::flush;
    writer.close();
  }

  // wait for the writing to finish
  xfld.comm()->barrier();
}


template<class PhysDim, class TopoDim>
XField_PX<PhysDim,TopoDim>::XField_PX(mpi::communicator& comm, const std::string& filename)
  : XField<PhysDim,TopoDim>(comm)
{
  std::string ext = get_file_extension(filename);
  XField_Lagrange<PhysDim> xfldin(comm);

  if (ext == "grm")
    readGrm(filename, xfldin);
  else if (ext == "gri")
    readGri(filename, xfldin);
  else
    SANS_DEVELOPER_EXCEPTION( "unknown file format: %s", filename.c_str() );

  // construct the grid
  this->buildFrom(xfldin);
}

template<class PhysDim, class TopoDim>
XField_PX<PhysDim,TopoDim>::XField_PX(XField<PhysDim,TopoDim>& xfld, XFieldBalance graph, const std::string& filename)
 : XField<PhysDim,TopoDim>(xfld.comm())
{
  std::string ext = get_file_extension(filename);
  XField_Lagrange<PhysDim> xfldin(xfld, graph);

  if (ext == "grm")
    readGrm(filename, xfldin);
  else if (ext == "gri")
    readGri(filename, xfldin);
  else
    SANS_DEVELOPER_EXCEPTION( "unknown file format: %s", filename.c_str() );

  // construct the grid
  this->buildFrom(xfldin);
}


template<class PhysDim, class TopoDim>
void XField_PX<PhysDim,TopoDim>::writeTo(const std::string& filename)
{
  std::string ext = get_file_extension(filename);

  if (ext == "grm")
    WriteMeshGrm(*this, filename);
  //else if (ext=="gri")
  //  WriteMeshGri(filename);
  else
    SANS_DEVELOPER_EXCEPTION( "unknown file format: %s", filename.c_str() );
}


// Explicitly instantiate the PX grid reader/writer
template class XField_PX<PhysD1, TopoD1>;
template class XField_PX<PhysD2, TopoD2>;
template class XField_PX<PhysD3, TopoD3>;
template class XField_PX<PhysD4, TopoD4>;

template void WriteMeshGrm<PhysD1, TopoD1>(const XField<PhysD1,TopoD1>& xfld, const std::string& filename);
template void WriteMeshGrm<PhysD2, TopoD2>(const XField<PhysD2,TopoD2>& xfld, const std::string& filename);
template void WriteMeshGrm<PhysD3, TopoD3>(const XField<PhysD3,TopoD3>& xfld, const std::string& filename);
template void WriteMeshGrm<PhysD4, TopoD4>(const XField<PhysD4,TopoD4>& xfld, const std::string& filename);


} // namespace SANS
