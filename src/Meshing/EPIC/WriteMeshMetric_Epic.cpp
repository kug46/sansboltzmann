// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "WriteMeshMetric_Epic.h"

#include <vector>
#include <iomanip>
#include <fstream>

#include "tools/SANSnumerics.h"     // Real
#include "Topology/Dimension.h"
#include "Topology/ElementTopology.h"

#include "XField_PX.h" // GrmShape

#include "Field/tools/for_each_CellGroup.h"
#include "Field/Tuple/FieldTuple.h"

#include "Field/XFieldArea.h"
#include "Field/XFieldVolume.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldVolume_CG_Cell.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_MPI
#include "MPI/serialize_DenseLinAlg_MatrixS.h"

#include <boost/mpi/collectives/all_reduce.hpp>
#include <boost/mpi/collectives/gather.hpp>
#include <boost/serialization/map.hpp>
#endif

namespace SANS
{

namespace // private to this file
{
template <class PhysDim>
class checkCellGroups : public GroupFunctorCellType< checkCellGroups<PhysDim> >
{
public:
  typedef typename DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  checkCellGroups( const int nCellGroups,
                   const int* xfld_local2nativeDOFmap, const int* mfld_local2nativeDOFmap )
    : nCellGroups_(nCellGroups),
      xfld_local2nativeDOFmap_(xfld_local2nativeDOFmap), mfld_local2nativeDOFmap_(mfld_local2nativeDOFmap) {}

  std::size_t nCellGroups() const          { return nCellGroups_; }
  std::size_t cellGroup(const int n) const { return n;            }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology >
  void
  apply( const typename FieldTuple<XField<PhysDim, typename Topology::TopoDim>,
                                    Field<PhysDim, typename Topology::TopoDim, MatrixSym>, TupleClass<>>::
                        template FieldCellGroupType<Topology>& fldsCell,
        const int cellGroupGlobal)
  {
    typedef typename XField<PhysDim, typename Topology::TopoDim           >::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field <PhysDim, typename Topology::TopoDim, MatrixSym>::template FieldCellGroupType<Topology> MFieldCellGroupType;

    const XFieldCellGroupType& xfldCell = get<0>(fldsCell);
    const MFieldCellGroupType& mfldCell = get<1>(fldsCell);

    SANS_ASSERT( xfldCell.nElem() == mfldCell.nElem() );

    int nElem = xfldCell.nElem();

    // loop over cells within group
    for (int elem = 0; elem < nElem; elem++)
    {
      int xnodeDOFmap[Topology::NNode];
      xfldCell.associativity(elem).getGlobalMapping(xnodeDOFmap, Topology::NNode);

      int mnodeDOFmap[Topology::NNode];
      mfldCell.associativity(elem).getGlobalMapping(mnodeDOFmap, Topology::NNode);

      // assert that the xfld DOF mapping is identical to the possessed metric field mapping
      for (int n = 0; n < Topology::NNode; n++)
        SANS_ASSERT(xfld_local2nativeDOFmap_[xnodeDOFmap[n]] == mfld_local2nativeDOFmap_[mnodeDOFmap[n]]);
    }
  }

protected:
  const int nCellGroups_;
  const int* xfld_local2nativeDOFmap_;
  const int* mfld_local2nativeDOFmap_;
};

} //namespace

//===========================================================================//
// Epic Mesh-Metric writeing function
//===========================================================================//

template <class PhysDim, class TopoDim>
void WriteMeshMetric_Epic(
    const Field_CG_Cell< PhysDim, TopoDim, DLA::MatrixSymS<PhysDim::D,Real> >& mfld,
    const std::string& meshfilename, const std::string& metricfilename)
{
  typedef typename DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  const XField<PhysDim,TopoDim>& xfld = mfld.getXField();

  const int nCellGroup = xfld.nCellGroups();

  // Assuming one-to-one mapping between the linear grid and linear metric field
  SANS_ASSERT( xfld.nDOF() == mfld.nDOF() );

  for (int group = 0; group < nCellGroup; group++)
  {
    // Epic only takes linear grids
    SANS_ASSERT( mfld.getCellGroupBase(group).order() == 1 );

    //Epic only takes P1 Lagrange or Hierarchical basis functions
    SANS_ASSERT( (mfld.getCellGroupBase(group).basisCategory() == BasisFunctionCategory_Lagrange ||
                  mfld.getCellGroupBase(group).basisCategory() == BasisFunctionCategory_Hierarchical) &&
                  mfld.getCellGroupBase(group).order() == 1 );
  }

  //--------------------------------------------------
  // Assert that the associativity between the grid and field are identical
  //--------------------------------------------------

  for_each_CellGroup<TopoDim>::apply( checkCellGroups<PhysDim>(nCellGroup, xfld.local2nativeDOFmap(), mfld.local2nativeDOFmap()),
                                      (xfld, mfld) );

  //--------------------------------------------------
  // Write the mesh file
  //--------------------------------------------------

  WriteMeshGrm(xfld, meshfilename);

  //--------------------------------------------------
  // Open metric file
  //--------------------------------------------------

  int comm_rank = xfld.comm()->rank();
  std::fstream metricfile;

  if (comm_rank == 0)
  {
    metricfile.open( metricfilename, std::fstream::out );

    if (!metricfile.good())
      SANS_RUNTIME_EXCEPTION( "Error writing to file: %s", metricfilename.c_str() );
  }

  //--------------------------------------------------
  // Write metric file headers
  //--------------------------------------------------

  int nDOFnative = xfld.nDOFnative();

  if (comm_rank == 0)
  {
    metricfile << nDOFnative << " " << PhysDim::D << std::endl;
    metricfile << std::setprecision(16) << std::scientific;
  }

  //--------------------------------------------------
  // Write out the nodal metric values
  //--------------------------------------------------

#ifdef SANS_MPI
  int comm_size = mfld.comm()->size();

  // maximum DOF chunk size that rank 0 will write at any given time
  int DOFchunk = nDOFnative / comm_size + nDOFnative % comm_size;

  for (int rank = 0; rank < comm_size; rank++)
  {
    int DOFlow = rank*DOFchunk;
    int DOFhigh = (rank+1)*DOFchunk;

    std::map<int,MatrixSym> buffer;

    for (int i = 0; i < mfld.nDOFpossessed(); i++)
    {
      int iDOF = mfld.local2nativeDOFmap(i);
      if (iDOF >= DOFlow && iDOF < DOFhigh)
        buffer[iDOF] = mfld.DOF(i);
    }

    if (comm_rank == 0)
    {
      std::vector<std::map<int,MatrixSym>> bufferOnRank;
      boost::mpi::gather(*mfld.comm(), buffer, bufferOnRank, 0 );

      // collapse down the buffer from all ranks (this remove any possible duplicates)
      for (std::size_t i = 0; i < bufferOnRank.size(); i++)
        buffer.insert(bufferOnRank[i].begin(), bufferOnRank[i].end());

      // Write out the metric values
      for ( const auto& DOFpair : buffer)
      {
        // Node index
        metricfile << (DOFpair.first+1);

        // Epic expects an upper triangular representation of the symmetric matrix
        for (int i = 0; i < PhysDim::D; i++ )
          for (int j = i; j < PhysDim::D; j++ )
            metricfile << " " << DOFpair.second(i,j);
        metricfile << std::endl;
      }

    }
#ifndef __clang_analyzer__
    else // send the buffer to rank 0
      boost::mpi::gather(*xfld.comm(), buffer, 0 );
#endif
  } // for rank

#else
  for (int n = 0; n < mfld.nDOFnative(); n++)
  {
    // Wite out the metric values
    metricfile << (n+1);
    const MatrixSym& m = mfld.DOF(n);

    // Epic expects an upper triangular representation of the symmetric matrix
    for (int i = 0; i < PhysDim::D; i++ )
      for (int j = i; j < PhysDim::D; j++ )
        metricfile << " " << m(i,j);
    metricfile << std::endl;
  }
#endif

  if (comm_rank == 0)
  {
    metricfile << std::flush;
    metricfile.close();
  }
}

// Explicit instantiations
template void WriteMeshMetric_Epic<PhysD2,TopoD2>(
    const Field_CG_Cell< PhysD2, TopoD2, DLA::MatrixSymS<PhysD2::D,Real> >& fld,
    const std::string& meshfilename, const std::string& metricfilename);

template void WriteMeshMetric_Epic<PhysD3,TopoD3>(
    const Field_CG_Cell< PhysD3, TopoD3, DLA::MatrixSymS<PhysD3::D,Real> >& fld,
    const std::string& meshfilename, const std::string& metricfilename);

}
