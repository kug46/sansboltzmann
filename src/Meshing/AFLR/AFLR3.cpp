// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "AFLR3.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "Field/XFieldLine_Traits.h"

#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include <memory> //shared_ptr
#include <algorithm>
#include <vector>
#include <list>
#include <limits>
#include <set>

// AFLR3_API Library include
#include <aflr3/AFLR3_LIB.h>
#include <ug3/UG3_LIB.h>
#include <ug_gq/UG_GQ_LIB_INC.h>

#include <anbl3/ANBL3_LIB.h>

namespace SANS
{
// cppcheck-suppress passedByValue
void AFLRParams::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.FilenameBase));
  d.checkUnknownInputs(allParams);
}
AFLRParams AFLRParams::params;

using namespace EGADS;

namespace // private to this file
{
//A deleter for AFLR allocated arrays
struct AFLR_free
{
  explicit AFLR_free(INT_ Error_Flag) { SANS_ASSERT(Error_Flag == 0); }
  inline void operator()(void* p)
  {
    ug_free(p);
  }
};

struct TriFace
{
  int tri[3];
  int iTetL;
  int iTetR;
  int trace;
  int face;
};


typedef DLA::VectorS<3,Real> VectorX;

enum WakeCell
{
  Null,
  Left,
  Right,
};

// Updates the nodes of triangles due to node duplication
void updateTetNodes( INT_4D *Vol_Tet_Connectivity, INT_4D *Vol_Tet_Neighbor, const std::vector<WakeCell>& wakeCellFlags, const int newPoint,
                     const int iTetL, const int iTetR, const int oldPoint )
{
  SANS_ASSERT( wakeCellFlags[iTetR] == WakeCell::Null || wakeCellFlags[iTetR] == WakeCell::Right );

  bool updateneighbor = false;

  INT_4D *tetR = Vol_Tet_Connectivity + iTetR + 1;

  // Update the nodes for a tet right of the wake
  for ( int n = 0; n < Tet::NNode; n++ )
    if ( (*tetR)[n] == oldPoint )
    {
      (*tetR)[n] = newPoint;
      updateneighbor = true;
    }

  // Only update the neighbors if if a node was updated on the current tet
  if ( updateneighbor )
  {
    // Update the points on neighboring triangles
    for (int side = 0; side < Tet::NFace; side++ )
    {
      const int iTetN = Vol_Tet_Neighbor[iTetR+1][side]-1;
      if (iTetN == iTetL || iTetN < 0) continue;  //Don't modify the left tet or step out of bounds on a boundary
      if (wakeCellFlags[iTetN] == WakeCell::Left) continue; // Don't try changing a tet left of the wake

      updateTetNodes( Vol_Tet_Connectivity, Vol_Tet_Neighbor, wakeCellFlags, newPoint, iTetR, iTetN, oldPoint );
    }
  }
}

void splitBoundaryTets(INT_ Number_of_Surf_Nodes,
                       INT_& Number_of_Nodes, DOUBLE_3D **Coordinates,
                       INT_& Number_of_Vol_Tets, INT_4D **Vol_Tet_Connectivity)
{
  INT_ Error_Flag = 0;

  std::list<int> split_tets;

  // count the number of tetrahedra that have all nodes in boundaries
  int ntetcenter = 0;
  for ( int itet = 0; itet < Number_of_Vol_Tets; itet++ )
  {
    const int tet[] = {(*Vol_Tet_Connectivity)[itet+1][0], (*Vol_Tet_Connectivity)[itet+1][1],
                       (*Vol_Tet_Connectivity)[itet+1][2], (*Vol_Tet_Connectivity)[itet+1][3]};

    // if all points are less than in.numberofpoints, they came from the inputs
    if ( (tet[0] <= Number_of_Surf_Nodes) &&
         (tet[1] <= Number_of_Surf_Nodes) &&
         (tet[2] <= Number_of_Surf_Nodes) &&
         (tet[3] <= Number_of_Surf_Nodes) )
    {
      ntetcenter++;
      split_tets.push_back(itet);
    }
  }

  std::cout << "Adding " << ntetcenter << " tet centers" << std::endl;

  (*Coordinates) = (DOUBLE_3D *) ug_realloc (&Error_Flag, (void*)(*Coordinates), (Number_of_Nodes+ntetcenter+1) * sizeof (DOUBLE_3D));

  (*Vol_Tet_Connectivity) = (INT_4D *) ug_realloc (&Error_Flag,
                                                   (void*)(*Vol_Tet_Connectivity),
                                                   (Number_of_Vol_Tets+3*ntetcenter+1) * sizeof (INT_4D));

  // Generate the new points and tets
  ntetcenter = 0;
  for ( auto split_tet = split_tets.begin(); split_tet != split_tets.end(); split_tet++ )
  {
    const int itet = *split_tet;

    const int tet[] = {(*Vol_Tet_Connectivity)[itet+1][0], (*Vol_Tet_Connectivity)[itet+1][1],
                       (*Vol_Tet_Connectivity)[itet+1][2], (*Vol_Tet_Connectivity)[itet+1][3]};

    // The original nodes {0-3}, and the new node {4}
    /*
            2
            |\
            | \
            |  \
            |   \
            |    \
            | (4) \
            |      \
            0 ----- 1
           /     .
          /   .
         / .
         3
    */
    // The new tetrahedra are
    // tet0 = {4, 1, 2, 3}
    // tet1 = {0, 4, 2, 3}
    // tet2 = {0, 1, 4, 3}
    // tet3 = {0, 1, 2, 4}

    // Add the tetcenters to the list of nodes
    int newpointindex = Number_of_Nodes + ntetcenter + 1;

    DOUBLE_3D tetcenter = {0,0,0};
    for (int v = 0; v < 4; v++)
      for (int n = 0; n < 3; n++)
        tetcenter[n] += (*Coordinates)[tet[v]][n];

    for (int n = 0; n < 3; n++)
      (*Coordinates)[newpointindex][n] = tetcenter[n]/4.;

    // Mofigy the existing tet for the first subdivided tet
    (*Vol_Tet_Connectivity)[itet+1][0] = newpointindex;

    // Create the new subdivided tetrahedron
    for (int v = 1; v < 4; v++)
    {
      int newtet[4] = {tet[0], tet[1], tet[2], tet[3]};
      newtet[v] = newpointindex;

      int newitet = Number_of_Vol_Tets + 3*ntetcenter + v-1;

      // add the new tet
      for (int n = 0; n < 4; n++)
        (*Vol_Tet_Connectivity)[newitet+1][n] = newtet[n];
    }

    ntetcenter++;
  }


  Number_of_Nodes += ntetcenter;
  Number_of_Vol_Tets += 3*ntetcenter;
}

}

AFLR3::AFLR3( const EGADS::EGTessModel& tessModel )
{
  // A trianglular field element used to check normal vectors
  FieldTraceGroupType<Triangle>::ElementType<> elemTrace( BasisFunctionAreaBase<Triangle>::HierarchicalP1 );
  FieldTraceGroupType<Triangle>::ElementType<>::RefCoordType sRef = Triangle::centerRef;
  FieldTraceGroupType<Triangle>::ElementType<>::VectorX Ntri, Nface, X, TetCenter, CenterVector;

  const int (*TraceNodes)[ Triangle::NTrace ] = TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes;
  const int (*FrameNodes)[ Line::NTrace ] = TraceToCellRefCoord<Line, TopoD2, Triangle>::TraceNodes;


  // Get all the bodies in the model
  std::vector< EGBody<3> > bodies = tessModel.getBodies();
  std::vector< EGEdge<3> > edges = tessModel.getAllEdges();

  std::set<int> duplicatePoints;

  const std::vector< int >* modelBodyIndex = tessModel.modelBodyIndex();
  const std::vector< int >& solidBodies = tessModel.modelSolidBodyIndex();
  const std::vector< int >& sheetBodies = tessModel.modelSheetBodyIndex();

  const std::vector< std::vector< int > >& sheetEdgeIndex = tessModel.modelSheetEdgeIndex();
  const std::vector< std::vector< int > >& sheetNodeIndex = tessModel.modelSheetNodeIndex();

  //--------------------------------
  // Create maps of which edges could have points duplicated (i.e. for wakes in full potential)
  int modelFace = 0;
  int modelEdge = 0;
  int modelNode = 0;
  for ( std::size_t ibody = 0; ibody < bodies.size(); ibody++ )
  {
    modelFace += bodies[ibody].nFaces();
    modelEdge += bodies[ibody].nEdges();
    modelNode += bodies[ibody].nNodes();
  }

  std::vector<bool> modelFaceKutta(modelFace, false);
  std::vector<bool> modelEdgeDuplicity(modelEdge, true);
  std::vector<bool> modelNodeDuplicity(modelNode, true);

  // Get the starting indexes for the sheet bodies
  modelEdge = 0;
  modelNode = 0;
  for ( std::size_t ibody = 0; ibody < solidBodies.size(); ibody++ )
  {
    const int bodyIndex = solidBodies[ibody];
    modelEdge += bodies[bodyIndex].nEdges();
    modelNode += bodies[bodyIndex].nNodes();
  }

  for ( std::size_t ibody = 0; ibody < sheetBodies.size(); ibody++ )
  {
    const int bodyIndex = sheetBodies[ibody];
    std::vector< EGEdge<3> > edges = bodies[bodyIndex].getEdges();

    for ( std::size_t iedge = 0; iedge < edges.size(); iedge++ )
    {
      // Edges with only one face attached cannot be duplicated, unless they are floating edges
      if ( bodies[bodyIndex].getFaces( edges[iedge] ).size() == 1 && !edges[iedge].hasAttribute(EGTessModel_FLOATINGEDGE) )
        modelEdgeDuplicity[modelEdge] = false;

      modelEdge++;
    }
  }

  //--------------------------------
  // Create maps of which nodes could have points duplicated (i.e. for wakes in full potential)
  for ( std::size_t ibody = 0; ibody < sheetBodies.size(); ibody++ )
  {
    const int bodyIndex = sheetBodies[ibody];
    std::vector< EGNode<3> > nodes = bodies[bodyIndex].getNodes();

    for ( std::size_t inode = 0; inode < nodes.size(); inode++ )
    {
      const int bodyNode = bodies[bodyIndex].getBodyIndex(nodes[inode])-1;
      // Extract all edges that reference the node
      const std::vector< EGEdge<3> > edges = bodies[bodyIndex].getEdges( nodes[inode] );

      // All edges with two faces can have points that are duplicated
      for ( std::size_t iedge = 0; iedge < edges.size(); iedge++ )
      {
        const int bodyEdge = bodies[bodyIndex].getBodyIndex(edges[iedge])-1;

        modelEdge = sheetEdgeIndex[ibody][bodyEdge];
        modelNode = sheetNodeIndex[ibody][bodyNode];

        // All model Edges must be duplicable if the node can be duplicated
        modelNodeDuplicity[modelNode] = modelNodeDuplicity[modelNode] && modelEdgeDuplicity[modelEdge];
      }
    }
  }

  //--------------------------------
  for ( std::size_t ibody = 0; ibody < solidBodies.size(); ibody++ )
  {
    const int bodyIndex = solidBodies[ibody];
    std::vector< EGNode<3> > nodes = bodies[bodyIndex].getNodes();

    for ( std::size_t inode = 0; inode < nodes.size(); inode++ )
    {
      // Extract all edges that reference the node
      const std::vector< EGEdge<3> > edges = bodies[bodyIndex].getEdges( nodes[inode] );

      bool wakeEdge = false;
      std::string WakeName;

      for ( std::size_t iedge = 0; iedge < edges.size(); iedge++ )
      {
        // Count how many edges connected to the node are wake nodes
        if ( edges[iedge].hasAttribute(AFLR3::WAKESHEET) )
        {
          wakeEdge = true;
          edges[iedge].getAttribute(AFLR3::WAKESHEET, WakeName);

          // Mark the faces connected to the edge as Kutta faces
          const std::vector< EGFace<3> > faces = bodies[bodyIndex].getFaces( edges[iedge] );
          for ( std::size_t iface = 0; iface < faces.size(); iface++ )
            modelFaceKutta[ bodies[bodyIndex].getBodyIndex(faces[iface])-1 ] = true;
        }
      }

      if ( wakeEdge && modelNodeDuplicity[inode] )
      {
        //std::cout << "Marking Wake Node : " << WakeName << std::endl;
        nodes[inode].addAttribute(AFLR3::WAKESHEET, WakeName);
      }
    }
  }

  // Declare AFLR3 grid generation variables.
  // This is required in all implementations!
  INT_ Error_Flag = 0;

  int prog_argc = 1;
  char **prog_argv = NULL;

  Error_Flag = ug_add_new_arg (&prog_argv, (char*)"allocate_and_initialize_argv"); SANS_ASSERT( Error_Flag == 0 );

  Error_Flag = ug_add_flag_arg ((char*)"-h", &prog_argc, &prog_argv); SANS_ASSERT( Error_Flag == 0 );

  bool BLfaces = false;
  for ( std::size_t ibody = 0; ibody < sheetBodies.size(); ibody++ )
  {
    const int bodyIndex = sheetBodies[ibody];
    int nface = bodies[bodyIndex].nFaces();
    std::vector< EGFace<3> > faces = bodies[bodyIndex].getFaces();

    for (int j = 0; j < nface && !BLfaces; j++)
    {
      if ( !faces[j].hasAttribute(AFLR3::BCNAME) ) continue;
      BLfaces = true;
      break;
    }
  }

  BLfaces = false;
  if (BLfaces)
  {
    Error_Flag = ug_add_flag_arg ((char*)"mbl=2"   , &prog_argc, &prog_argv); SANS_ASSERT( Error_Flag == 0 );
    Error_Flag = ug_add_flag_arg ((char*)"mblelc=0", &prog_argc, &prog_argv); SANS_ASSERT( Error_Flag == 0 );
    Error_Flag = ug_add_flag_arg ((char*)"mrecm=3" , &prog_argc, &prog_argv); SANS_ASSERT( Error_Flag == 0 );
    Error_Flag = ug_add_flag_arg ((char*)"mrecqm=3", &prog_argc, &prog_argv); SANS_ASSERT( Error_Flag == 0 );
    Error_Flag = ug_add_flag_arg ((char*)"angqbf=0", &prog_argc, &prog_argv); SANS_ASSERT( Error_Flag == 0 );
  }

#if 0
  Error_Flag = ug_add_flag_arg ((char*)"mblchki=1", &prog_argc, &prog_argv); SANS_ASSERT( Error_Flag == 0 );
#endif

  // set the last argument to 1 to print what arguments have been set
  Error_Flag = ug_check_prog_param (prog_argv, prog_argc, 0); SANS_ASSERT( Error_Flag == 0 );

  INT_1D *Surf_Error_Flag = NULL;
  INT_1D *Surf_Grid_BC_Flag = NULL;
  INT_1D *Surf_ID_Flag = NULL;
  INT_1D *Surf_Reconnection_Flag = NULL;
  INT_3D *Surf_Tria_Connectivity = NULL;
  INT_4D *Surf_Quad_Connectivity = NULL;
  INT_4D *Vol_Tet_Connectivity = NULL;
  INT_5D *Vol_Pent_5_Connectivity = NULL;
  INT_6D *Vol_Pent_6_Connectivity = NULL;
  INT_8D *Vol_Hex_Connectivity = NULL;

  INT_4D *Vol_Tet_Neighbor = NULL;

  DOUBLE_3D *Coordinates = NULL;
  DOUBLE_1D *Initial_Normal_Spacing = NULL;
  DOUBLE_1D *BL_Thickness = NULL;

  INT_4D *BG_Vol_Tet_Neighbors = NULL;
  INT_4D *BG_Vol_Tet_Connectivity = NULL;

  DOUBLE_3D *BG_Coordinates = NULL;
  DOUBLE_1D *BG_Spacing = NULL;
  DOUBLE_6D *BG_Metric = NULL;

  DOUBLE_3D *Source_Coordinates = NULL;
  DOUBLE_1D *Source_Spacing = NULL;
  DOUBLE_6D *Source_Metric = NULL;

  INT_ Message_Flag = 0;
  INT_ Number_of_BL_Vol_Tets = 0;
  INT_ Number_of_Nodes = 0;
  INT_ Number_of_Surf_Quads = 0;
  INT_ Number_of_Surf_Trias = 0;
  INT_ Number_of_Vol_Hexs = 0;
  INT_ Number_of_Vol_Pents_5 = 0;
  INT_ Number_of_Vol_Pents_6 = 0;
  INT_ Number_of_Vol_Tets = 0;

  INT_ Number_of_BG_Nodes = 0;
  INT_ Number_of_BG_Vol_Tets = 0;

  INT_ Number_of_Source_Nodes = 0;


  Number_of_Nodes = tessModel.xyzs().size();

  Coordinates = (DOUBLE_3D *) ug_malloc (&Error_Flag, (Number_of_Nodes+1) * sizeof (DOUBLE_3D));
  SANS_ASSERT( Error_Flag == 0 ); // TODO: Better error handling

  // Copy over the points into the AFLR3 input
  for (int i = 0; i < Number_of_Nodes; i++)
    for (int n = 0; n < 3; n++)
      Coordinates[i+1][n] = tessModel.xyzs()[i][n]; // +1 for 1-based indexing

  Initial_Normal_Spacing = (DOUBLE_1D *) ug_malloc (&Error_Flag, (Number_of_Nodes+1) * sizeof (DOUBLE_1D));
  SANS_ASSERT( Error_Flag == 0 ); // TODO: Better error handling

  BL_Thickness = (DOUBLE_1D *) ug_malloc (&Error_Flag, (Number_of_Nodes+1) * sizeof (DOUBLE_1D));
  SANS_ASSERT( Error_Flag == 0 ); // TODO: Better error handling


  Number_of_Surf_Trias = tessModel.ntri();

  Surf_Tria_Connectivity = (INT_3D *) ug_malloc (&Error_Flag, (Number_of_Surf_Trias+1) * sizeof (INT_3D));
  SANS_ASSERT( Error_Flag == 0 ); // TODO: Better error handling

  Surf_ID_Flag = (INT_1D *) ug_malloc (&Error_Flag, (Number_of_Surf_Trias+1) * sizeof (INT_1D));
  SANS_ASSERT( Error_Flag == 0 ); // TODO: Better error handling

  Surf_Grid_BC_Flag = (INT_1D *) ug_malloc (&Error_Flag, (Number_of_Surf_Trias+1) * sizeof (INT_1D));
  SANS_ASSERT( Error_Flag == 0 ); // TODO: Better error handling

  Surf_Reconnection_Flag = (INT_1D *) ug_malloc (&Error_Flag, (Number_of_Surf_Trias+1) * sizeof (INT_1D));
  SANS_ASSERT( Error_Flag == 0 ); // TODO: Better error handling

  const std::vector< std::vector< std::vector< std::array<int,3> > > >& tris = tessModel.tris();

  //Indexes to form the sides of a triangle
  const int segnodes[3][2] = {{1,2}, {2,0}, {0,1}};
  std::vector<DOUBLE_1D> BL_Thickness_Face(tessModel.nFaces(), std::numeric_limits<double>::max());

  int face = 0;
  for ( std::size_t ibody = 0; ibody < solidBodies.size(); ibody++ )
    face += bodies[solidBodies[ibody]].nFaces();

  for ( std::size_t ibody = 0; ibody < sheetBodies.size(); ibody++ )
  {
    const int bodyIndex = sheetBodies[ibody];
    int nface = bodies[bodyIndex].nFaces();
    std::vector< EGFace<3> > faces = bodies[bodyIndex].getFaces();

    for (int j = 0; j < nface; j++)
    {
      if ( !faces[j].hasAttribute(AFLR3::BCNAME) ) continue;

      for (int i = 0; i < (int)tris[bodyIndex][j].size(); i++)
      {
        // compute a surface metric as the smallest segment connected to any vertex
        for (int seg = 0; seg < 3; seg++)
        {
          const int n0 = tris[bodyIndex][j][i][segnodes[seg][0]];
          const int n1 = tris[bodyIndex][j][i][segnodes[seg][1]];

          DLA::VectorS<3,DOUBLE_1D> X0 = { tessModel.xyzs()[n0][0], tessModel.xyzs()[n0][1], tessModel.xyzs()[n0][2] };
          DLA::VectorS<3,DOUBLE_1D> X1 = { tessModel.xyzs()[n1][0], tessModel.xyzs()[n1][1], tessModel.xyzs()[n1][2] };

          DLA::VectorS<3,DOUBLE_1D> dX = X1 - X0;
          DOUBLE_1D length = sqrt( dot(dX,dX) );

          // Find the smallest segment length
          BL_Thickness_Face[face] = std::min( length, BL_Thickness_Face[face] );
        }
      }
      face++;
    }
  }


  face = 0;
  int BCface = 1;
  int Iface = 1;
  int globaltri = 0;
  std::vector<int> SurfIDFaceMap(tessModel.nFaces(), 0);

  for ( int ibodyTopo = 0; ibodyTopo < EGTessModel::NBODYTOPO; ibodyTopo++ )
  {
    for ( std::size_t ibody = 0; ibody < modelBodyIndex[ibodyTopo].size(); ibody++ )
    {
      const int bodyIndex = modelBodyIndex[ibodyTopo][ibody];
      int nface = bodies[bodyIndex].nFaces();
      std::vector< EGFace<3> > faces = bodies[bodyIndex].getFaces();

      for (int j = 0; j < nface; j++)
      {
        INT_ BC_Flag = STD_UG3_GBC;
        if ( ibodyTopo == 1 )
        {
          if ( faces[j].hasAttribute(AFLR3::BCNAME) )
            BC_Flag = -TRANSP_UG3_GBC;
          else
            BC_Flag = TRANSP_UG3_GBC;
        }

        bool dupPoints = false;
        if ( ibodyTopo == 1 && faces[j].hasAttribute(AFLR3::BCNAME) )
          dupPoints = true;

        std::set<int> dupSet;

        // Negate the marker for sheet faces unless it is a wake
        if ( ibodyTopo == 1 && !faces[j].hasAttribute(AFLR3::BCNAME) )
        {
          SurfIDFaceMap[face] = -Iface;
          Iface++;
        }
        else
        {
          SurfIDFaceMap[face] = BCface;
          BCface++;
        }


        for (std::size_t i = 0; i < tris[bodyIndex][j].size(); i++)
        {
          // +1 for 1-based indexing
          Surf_ID_Flag[globaltri+1] = face+1;
          Surf_Grid_BC_Flag[globaltri+1] = BC_Flag;
          Surf_Reconnection_Flag[globaltri+1] = 7; // Do not allow swapping on the surface triangles

          for (int n = 0; n < Triangle::NNode; n++)
          {
            // Current triangle vertex
            const int vertex = tris[bodyIndex][j][i][n];

            // +1 for 1-based indexing
            Surf_Tria_Connectivity[globaltri+1][n] = vertex+1;

            Initial_Normal_Spacing[vertex+1] = BL_Thickness_Face[face]/2;
            BL_Thickness[vertex+1]           = BL_Thickness_Face[face];

            if ( dupPoints )
            {
              bool duplicateMarker = false;
              int topoType = tessModel.modelTopoType(vertex);

              //Use model global indexing to build up the table
              if ( topoType == 0 )
              {
                int nodeIndex = tessModel.modelTopoIndex(vertex)-1;
                duplicateMarker = modelNodeDuplicity[nodeIndex];
              }
              else if ( topoType > 0 )
              {
                int edgeIndex = tessModel.modelTopoIndex(vertex)-1;
                duplicateMarker = modelEdgeDuplicity[edgeIndex];
              }
              else
              {
                duplicateMarker = true; // Points interior on a face are always allowed to be duplicated
              }

              if (duplicateMarker)
                dupSet.insert(vertex);
            }
          }
          globaltri++;
        }

        if (dupPoints)
          duplicatePoints.insert( dupSet.begin(), dupSet.end() );

        face++;
      }
    }
  }

  ug_set_prog_param_n_dim (3);

  ug_set_prog_param_function1 (ug_initialize_aflr_param);
  ug_set_prog_param_function1 (ug_gq_initialize_param); // optional
  ug_set_prog_param_function2 (aflr3_initialize_param);
  ug_set_prog_param_function2 (aflr3_anbl3_initialize_param);
  ug_set_prog_param_function2 (ice3_initialize_param);
  ug_set_prog_param_function2 (ug3_qchk_initialize_param); // optional

  aflr3_register_anbl3 (anbl3_grid_generator, anbl3_initialize_param, anbl3_reset_ibcibf);

  Error_Flag = aflr3_grid_generator (prog_argc, prog_argv,
                                     Message_Flag,
                                     &Number_of_BL_Vol_Tets,
                                     &Number_of_BG_Nodes,
                                     &Number_of_BG_Vol_Tets,
                                     &Number_of_Nodes,
                                     &Number_of_Source_Nodes,
                                     &Number_of_Surf_Quads,
                                     &Number_of_Surf_Trias,
                                     &Number_of_Vol_Hexs,
                                     &Number_of_Vol_Pents_5,
                                     &Number_of_Vol_Pents_6,
                                     &Number_of_Vol_Tets,
                                     &Surf_Error_Flag, &Surf_Grid_BC_Flag,
                                     &Surf_ID_Flag, &Surf_Reconnection_Flag,
                                     &Surf_Quad_Connectivity,
                                     &Surf_Tria_Connectivity,
                                     &Vol_Hex_Connectivity,
                                     &Vol_Pent_5_Connectivity,
                                     &Vol_Pent_6_Connectivity,
                                     &Vol_Tet_Connectivity,
                                     &BG_Vol_Tet_Neighbors,
                                     &BG_Vol_Tet_Connectivity,
                                     &Coordinates,
                                     &Initial_Normal_Spacing, &BL_Thickness,
                                     &BG_Coordinates,
                                     &BG_Spacing,
                                     &BG_Metric,
                                     &Source_Coordinates,
                                     &Source_Spacing,
                                     &Source_Metric);
  // TODO: Better error handling
  SANS_ASSERT( Error_Flag == 0 );

  //Free the arguments
  ug_free_argv (prog_argv);

  // Split all tets that have 4 nodes on boundaries
  splitBoundaryTets( tessModel.xyzs().size(),
                     Number_of_Nodes, &Coordinates,
                     Number_of_Vol_Tets, &Vol_Tet_Connectivity);


  std::unique_ptr<INT_1D[],AFLR_free> delSurf_Error_Flag(Surf_Error_Flag, AFLR_free(0));
  std::unique_ptr<DOUBLE_3D[],AFLR_free> delCoordinates(Coordinates, AFLR_free(0));
  std::unique_ptr<INT_4D[],AFLR_free> delVol_Tet_Connectivity(Vol_Tet_Connectivity, AFLR_free(0));

  std::unique_ptr<INT_3D[],AFLR_free> delSurf_Tria_Connectivity(Surf_Tria_Connectivity, AFLR_free(0));
  std::unique_ptr<INT_1D[],AFLR_free> delSurf_ID_Flag(Surf_ID_Flag, AFLR_free(0));
  std::unique_ptr<INT_1D[],AFLR_free> delSurf_Grid_BC_Flag(Surf_Grid_BC_Flag, AFLR_free(0));

  std::unique_ptr<DOUBLE_1D[],AFLR_free> delInitial_Normal_Spacing(Initial_Normal_Spacing, AFLR_free(0));
  std::unique_ptr<DOUBLE_1D[],AFLR_free> delBL_Thickness(BL_Thickness, AFLR_free(0));
  std::unique_ptr<INT_1D[],AFLR_free> delSurf_Reconnection_Flag(Surf_Reconnection_Flag, AFLR_free(0));

  // TODO: Better error handling
  SANS_ASSERT( Error_Flag == 0 );

  // Get neighbor information
  Error_Flag = ug3_ieliel2(Number_of_Vol_Tets, Number_of_Nodes, Vol_Tet_Connectivity, &Vol_Tet_Neighbor);
  std::unique_ptr<INT_4D[],AFLR_free> delVol_Tet_Neighbor(Vol_Tet_Neighbor, AFLR_free(Error_Flag));

  // Count the number of trace triangles in the mesh
  int ntri = 0;

  for ( int iTetL = 0; iTetL < Number_of_Vol_Tets; iTetL++ )
  {
    for (int n = 0; n < Tet::NFace; n ++)
    {
      int iTetR = Vol_Tet_Neighbor[iTetL+1][n];

      // Only consider right test's with a higher index
      if (iTetR < iTetL+1 && iTetR > 0) continue;

      ntri++;
    }
  }

  std::vector<TriFace> trifaces(ntri);
  ntri = 0;

  for ( int iTetL = 0; iTetL < Number_of_Vol_Tets; iTetL++ )
  {
    for (int n = 0; n < Tet::NFace; n ++)
    {
      int iTetR = Vol_Tet_Neighbor[iTetL+1][n];

      // Only consider right test's with a higher index
      if (iTetR < iTetL+1 && iTetR > 0) continue;

      // Triangle nodes
      for (int k = 0; k < Triangle::NNode; k++ )
        trifaces[ntri].tri[k] = Vol_Tet_Connectivity[iTetL+1][TraceNodes[n][k]]-1;

      // Set neighbor information
      trifaces[ntri].iTetL = iTetL;
      trifaces[ntri].iTetR = iTetR-1; // iTetR might be zero, which will be changed to a BC
      trifaces[ntri].trace = n;
      trifaces[ntri].face = 0; // BC's will be marked after the neighbor information is updated

      ntri++;
    }
  }


  // Update neighbor information with BC markers
  Error_Flag = ug3_ieliel2b(Number_of_Surf_Trias, Number_of_Vol_Tets, Number_of_Nodes,
                            Surf_ID_Flag, Surf_Tria_Connectivity, Vol_Tet_Connectivity, Vol_Tet_Neighbor);

  // TODO: Better error handling
  SANS_ASSERT( Error_Flag == 0 );

  // Used to get the original trace element numbering on each face
  //std::map<UniqueElem,std::array<int,3>> traceElemNodeMap = tessModel.getUniqueElemTraceNodeMap();

  // Update all face markers
  for ( int itri = 0; itri < ntri; itri++ )
  {
    int iTetL = trifaces[itri].iTetL;
    int iTetR = Vol_Tet_Neighbor[iTetL+1][trifaces[itri].trace];

    // Mark all the BC faces
    if (iTetR < 0)
    {
      trifaces[itri].face = SurfIDFaceMap[Surf_ID_Flag[-iTetR]-1];
#if 0
      // Triangle nodes
      std::array<int,3> triL;
      for (int k = 0; k < Triangle::NNode; k++ )
        triL[k] = Vol_Tet_Connectivity[iTetL+1][TraceNodes[trifaces[itri].trace][k]]-1;

      // Get the original triangle
      std::array<int,3> tri = traceElemNodeMap.at(UniqueElem(eTriangle, triL));

      // Order the trace nodes of the tetrahedron consistent with the original triangle
      for (int k = 0; k < Triangle::NNode; k++ )
      {
        trifaces[itri].tri[k] = tri[k];
        Vol_Tet_Connectivity[iTetL+1][TraceNodes[trifaces[itri].trace][k]] = tri[k]+1;
      }
#endif
    }

    // Restore the neighbor information on interior EGADS faces
    if (trifaces[itri].iTetR >= 0)
      Vol_Tet_Neighbor[iTetL+1][trifaces[itri].trace] = trifaces[itri].iTetR+1;
  }


  for ( int iTet = 0; iTet < Number_of_Vol_Tets; iTet++ )
    for (int n = 0; n < Tet::NNode; n ++)
      Vol_Tet_Connectivity[iTet+1][n]--; // Change to 0-based indexing


  int nBCFaces = 0;
  int nInteriorFaces = 0;
  int nDuplicatePoints = 0;

  // A vector for tracking which faces require duplication of vertexes
  std::vector<bool> duplicateFace(tessModel.nFaces(), false);

  std::set<std::string> wakeNames;

  std::map< int, int > KuttaLineCount, TrefftzLineCount;

  // Gather all the wake names from the Trefftz planes
  for ( std::size_t iedge = 0; iedge < edges.size(); iedge++ )
  {
    std::string wakename;
    if ( edges[iedge].hasAttribute(AFLR3::WAKESHEET) )
    {
      edges[iedge].getAttribute(AFLR3::WAKESHEET, wakename);
      wakeNames.insert(wakename);
      KuttaLineCount[iedge] = 0;
    }
    else if ( edges[iedge].hasAttribute(AFLR3::TREFFTZ) )
    {
      edges[iedge].getAttribute(AFLR3::TREFFTZ, wakename);
      wakeNames.insert(wakename);
      TrefftzLineCount[iedge] = 0;
    }
  }

  std::map<int,int> KuttaFrameGroupIndexes, TrefftzFrameGroupIndexes;
  {
    int igroup = 0;
    for (auto count = KuttaLineCount.begin(); count != KuttaLineCount.end(); count++)
      KuttaFrameGroupIndexes[(*count).first] = igroup++;

    for (auto count = TrefftzLineCount.begin(); count != TrefftzLineCount.end(); count++)
      TrefftzFrameGroupIndexes[(*count).first] = igroup++;
  }

  std::vector< EGFace<3> > modelBCFaces;

  modelFace = 0;
  for ( std::size_t ibody = 0; ibody < solidBodies.size(); ibody++ )
  {
    int nface = bodies[solidBodies[ibody]].nFaces();
    nBCFaces += nface; // All solid body faces must be BC faces
    modelFace += nface;
    std::vector< EGFace<3> > faces = bodies[solidBodies[ibody]].getFaces();
    modelBCFaces.insert( modelBCFaces.end(), faces.begin(), faces.end() );
  }

  // Check all faces in the sheet bodies
  for ( std::size_t ibody = 0; ibody < sheetBodies.size(); ibody++ )
  {
    const int bodyIndex = sheetBodies[ibody];
    int nface = bodies[bodyIndex].nFaces();
    std::vector< EGFace<3> > faces = bodies[bodyIndex].getFaces();

    for (int j = 0; j < nface; j++)
    {
      // Check if the sheet face is considered a boundary condition
      if ( faces[j].hasAttribute(AFLR3::BCNAME) )
      {
        std::string BCName;
        faces[j].getAttribute(AFLR3::BCNAME, BCName);

        // If the BC is a wake sheet, then mark it as a duplication face
        if ( std::find(wakeNames.begin(), wakeNames.end(), BCName) != wakeNames.end() )
        {
          nDuplicatePoints = duplicatePoints.size();
          duplicateFace[nBCFaces] = true;
        }

        modelBCFaces.push_back( faces[j] );
        nBCFaces++;
      }
      else
        nInteriorFaces++;

      modelFace++;
    }
  }


  dupPointOffset_ = Number_of_Nodes;

  //Create the DOF arrays
  resizeDOF(Number_of_Nodes + nDuplicatePoints);

  // One Cell group for now...
  resizeCellGroups(1);
  resizeInteriorTraceGroups(nInteriorFaces+1); // +1 for the volume interior trace group
  resizeBoundaryTraceGroups(nBCFaces);

  //Copy over the nodal values
  for ( int i = 0; i < Number_of_Nodes; i++ )
    DOF(i) = {Coordinates[i+1][0], Coordinates[i+1][1], Coordinates[i+1][2]};

  // Duplicate points
  std::map<int,int> newPointMap;         //Map from original points to new points
  invPointMap_.resize(nDuplicatePoints); //Map from new to points to original points
  std::vector<int> invKuttaPoints;
  int ii = 0;
  for (auto point = duplicatePoints.begin(); point != duplicatePoints.end(); point++)
  {
    DOF(Number_of_Nodes+ii) = DOF(*point);
    //DOF(Number_of_Nodes+ii)[2] -= 0.5; // HACK to visualize the duplicate nodes
    newPointMap[*point] = Number_of_Nodes+ii;
    invPointMap_[ii] = *point;
    if ( tessModel.modelTopoType(*point) >= 0 && tessModel.modelTopoFromVertex(*point).hasAttribute(AFLR3::WAKESHEET) )
    {
      KuttaPoints_.push_back( Number_of_Nodes+ii ); //Save off the trailing edge points to be used with the Kutta condition
      invKuttaPoints.push_back( *point );
    }
    ii++;
  }
  KuttaPoints_.shrink_to_fit();

  // Count the number of triangles on each face
  std::vector< int > BCFaceTriCount(nBCFaces, 0);
  std::vector< int > IFaceTriCount(nInteriorFaces+1, 0);
  for ( int itri = 0; itri < ntri; itri++ )
  {
    face = trifaces[itri].face;
    if ( face > 0 )
      BCFaceTriCount[face-1]++; // Boundary EGADS faces are positive numbers > 0
    else
      IFaceTriCount[-face]++; // Interior EGADS (and tet) faces are a negative number or 0
  }

  // volume field variable
  FieldCellGroupType<Tet>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionVolumeBase<Tet>::HierarchicalP1, Number_of_Vol_Tets );

  std::vector< FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType > fldAssocIface(nInteriorFaces+1);

  for ( int I = 0; I < nInteriorFaces+1; I++ ) // +1 for the volume interior trace group
  {
    fldAssocIface[I].resize( BasisFunctionAreaBase<Triangle>::HierarchicalP1, IFaceTriCount[I] );

    //Only one cell group for the interior trace group
    fldAssocIface[I].setGroupLeft ( 0 );
    fldAssocIface[I].setGroupRight( 0 );

    IFaceTriCount[I] = 0;
  }

  //Create the boundary association constructors
  std::vector< FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType > fldAssocBface(nBCFaces);

  for ( int BC = 0; BC < nBCFaces; BC++ )
  {
    fldAssocBface[BC].resize(BasisFunctionAreaBase<Triangle>::HierarchicalP1, BCFaceTriCount[BC]);

    //Only one cell group for the interior trace group
    fldAssocBface[BC].setGroupLeft ( 0 );
    if ( duplicateFace[BC] )
      fldAssocBface[BC].setGroupRight( 0 );

    BCFaceTriCount[BC] = 0;
  }

  // Vector to color cells where they are relative to wakes
  std::vector<WakeCell> wakeCellFlags(Number_of_Vol_Tets, WakeCell::Null);

  for ( int itri = 0; itri < ntri; itri++ )
  {
    int iTetL = trifaces[itri].iTetL;
    int iTetR = trifaces[itri].iTetR;

    // Only interested in interior boundary faces
    if ( !(trifaces[itri].face > 0 && iTetR >= 0) ) continue;

    // Triangle nodes
    std::array<int,3> tri = {{trifaces[itri].tri[0], trifaces[itri].tri[1], trifaces[itri].tri[2]}};

    // Count segments that are on Kutta or Trefftz edges.
    for (int seg = 0; seg < Triangle::NEdge; seg ++)
    {
      if ( tessModel.isEdgeSegment(tri,seg) )
      {
        int iedge = tessModel.getEdgeIndexFromSegment(tri,seg);
        if ( edges[iedge].hasAttribute(AFLR3::WAKESHEET) )
          KuttaLineCount[iedge] += 1;
        else if ( edges[iedge].hasAttribute(AFLR3::TREFFTZ) )
          TrefftzLineCount[iedge] += 1;
      }
    }

    const int BC = trifaces[itri].face-1;

    //Create the face element
    for ( int node = 0; node < 3; node++)
      elemTrace.DOF(node) = DOF(tri[node]);

    // Ntri always points towards the left tet
    elemTrace.unitNormal( sRef, Ntri );
    elemTrace.coordinates( sRef, X );

    Nface = modelBCFaces[BC].unitNormal( X );

    // Negative Ntri dot Nface means left tet is on left side of the face
    if ( dot(Ntri, Nface) < 0 )
    {
      std::swap(iTetL, iTetR);  // Swap tetL and tetR so they correspond to the left and right side of the BC

      std::vector<int> tetL = {Vol_Tet_Connectivity[iTetL+1][0], Vol_Tet_Connectivity[iTetL+1][1],
                               Vol_Tet_Connectivity[iTetL+1][2], Vol_Tet_Connectivity[iTetL+1][3]};

      CanonicalTraceToCell canonicalL = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTraceLeft(&tri[0], 3,
                                                                                                          &tetL[0], tetL.size(),
                                                                                                          trifaces[itri].tri, 3);

      trifaces[itri].iTetL = iTetL;
      trifaces[itri].iTetR = iTetR;
      trifaces[itri].trace = canonicalL.trace;
    }

    // Tag all Tets as left or right now that they have been sorted
    wakeCellFlags[iTetL] = WakeCell::Left;
    wakeCellFlags[iTetR] = WakeCell::Right;
  }

  for ( int itri = 0; itri < ntri; itri++ )
  {
    int iTetL = trifaces[itri].iTetL;
    int iTetR = trifaces[itri].iTetR;

    // Only interested in interior boundary faces
    if ( !(trifaces[itri].face > 0 && iTetR >= 0) ) continue;

    // Triangle nodes
    std::array<int,3> tri = {{trifaces[itri].tri[0], trifaces[itri].tri[1], trifaces[itri].tri[2]}};

    for ( int node = 0; node < 3; node++)
    {
      auto newPoint = newPointMap.find(tri[node]);
      if ( newPoint != newPointMap.end() )
        updateTetNodes( Vol_Tet_Connectivity, Vol_Tet_Neighbor, wakeCellFlags, newPoint->second, iTetL, iTetR, tri[node] );
    }
  }


  //element volume associativity after duplicate nodes have been updated
  for ( int iTet = 0; iTet < Number_of_Vol_Tets; iTet++ )
  {
    fldAssocCell.setAssociativity( iTet ).setRank( 0 );
    fldAssocCell.setAssociativity( iTet ).setNodeGlobalMapping( Vol_Tet_Connectivity[iTet+1], 4 );
  }


  //find all tets that have a Kutta point
  for ( int iTet = 0; iTet < Number_of_Vol_Tets; iTet++ )
  {
    INT_4D *tet = Vol_Tet_Connectivity + iTet + 1;

    for ( int node = 0; node < 4; node++)
    {
      if ( std::find(invKuttaPoints.begin(), invKuttaPoints.end(), (*tet)[node]) != invKuttaPoints.end() )
      {
        KuttaCellElemLeft_[0][(*tet)[node]].KuttaPoint = newPointMap.at((*tet)[node]);
        KuttaCellElemLeft_[0][(*tet)[node]].elems.push_back(iTet);
      }

      if ( std::find(KuttaPoints_.begin(), KuttaPoints_.end(), (*tet)[node]) != KuttaPoints_.end() )
      {
        KuttaCellElemRight_[0][(*tet)[node]].KuttaPoint = (*tet)[node];
        KuttaCellElemRight_[0][(*tet)[node]].elems.push_back(iTet);
      }
    }
  }


  //Create the boundary association constructors
  std::vector< FieldFrameGroupType::FieldAssociativityConstructorType >
      fldAssocBframe(KuttaFrameGroupIndexes.size() + TrefftzFrameGroupIndexes.size());

  for ( auto count = KuttaLineCount.begin(); count != KuttaLineCount.end(); count++ )
  {
    fldAssocBframe[KuttaFrameGroupIndexes[(*count).first]].resize(BasisFunctionLineBase::HierarchicalP1, (*count).second);
    (*count).second = 0;
  }

  std::cout << "Trefftz Frames (" << TrefftzLineCount.size() << ") : ";
  for ( auto count = TrefftzLineCount.begin(); count != TrefftzLineCount.end(); count++ )
  {
    std::cout << TrefftzFrameGroupIndexes[(*count).first] << " ";
    fldAssocBframe[TrefftzFrameGroupIndexes[(*count).first]].resize(BasisFunctionLineBase::HierarchicalP1, (*count).second);
    (*count).second = 0;
  }
  std::cout << std::endl;

  // Used to get the original trace element numbering on each face
  std::map<UniqueElem,int> traceIDMap = tessModel.getUniqueElemMap();

  // trace associativity
  for ( int itri = 0; itri < ntri; itri++ )
  {
    int iTetL = trifaces[itri].iTetL;
    int iTetR = trifaces[itri].iTetR;

    // Triangle nodes
    std::array<int,3> triL = {{trifaces[itri].tri[0], trifaces[itri].tri[1], trifaces[itri].tri[2]}};

    // Left tet nodes
    std::vector<int> tetL = {Vol_Tet_Connectivity[iTetL+1][0], Vol_Tet_Connectivity[iTetL+1][1],
                             Vol_Tet_Connectivity[iTetL+1][2], Vol_Tet_Connectivity[iTetL+1][3]};

    std::vector<int> tetLorig = tetL;

    // Find the original node numbers before duplication
    for ( int node = 0; node < 4; node++)
      if (tetLorig[node] >= Number_of_Nodes)
        tetLorig[node] = invPointMap_[tetLorig[node]-Number_of_Nodes];

    int marker = trifaces[itri].face;

    if ( iTetR >= 0 && marker <= 0) //Volume Interior triangle, or interior face
    {
      const int I = -marker;
      const int faceCount = IFaceTriCount[I];

      std::vector<int> tetR = {Vol_Tet_Connectivity[iTetR+1][0], Vol_Tet_Connectivity[iTetR+1][1],
                               Vol_Tet_Connectivity[iTetR+1][2], Vol_Tet_Connectivity[iTetR+1][3]};

      std::vector<int> tetRorig = tetR;
      for ( int node = 0; node < 4; node++)
        if (tetRorig[node] >= Number_of_Nodes)
          tetRorig[node] = invPointMap_[tetRorig[node]-Number_of_Nodes];


      fldAssocIface[I].setElementLeft ( iTetL, faceCount );
      fldAssocIface[I].setElementRight( iTetR, faceCount );

      // The left canonical trace was used to construct the triangle
      CanonicalTraceToCell canonicalL(trifaces[itri].trace, 1);

      CanonicalTraceToCell canonicalR = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(&triL[0], 3,
                                                                                                      &tetRorig[0], tetRorig.size());

      // Get the canonical triangle with the updated tetrahedral nodes
      for ( int n = 0; n < 3; n++)
        triL[n] = tetL[TraceNodes[canonicalL.trace][n]];

      // Face signs for elements (L is +, R is -)
      fldAssocCell.setAssociativity( iTetL ).setFaceSign( +1, canonicalL.trace );
      fldAssocCell.setAssociativity( iTetR ).setFaceSign( canonicalR.orientation, canonicalR.trace );

      // Set the canonical faces
      fldAssocIface[I].setCanonicalTraceLeft( canonicalL, faceCount );
      fldAssocIface[I].setCanonicalTraceRight( canonicalR, faceCount );

      fldAssocIface[I].setAssociativity( faceCount ).setRank( 0 );

      //Set the nodes for the triangle face
      fldAssocIface[I].setAssociativity( faceCount ).setNodeGlobalMapping( &triL[0], 3 );

      IFaceTriCount[I]++;
    }
    else if ( iTetR >= 0 && marker > 0) //Interior boundary face
    {
      const int BC = marker-1;
      int itrace = -1;
      try
      {
        itrace = traceIDMap.at(UniqueElem(eTriangle, triL));
      }
      catch (const std::exception& e)
      {
        SANS_DEVELOPER_EXCEPTION("Could not find triangle (%d, %d, %d)", triL[0], triL[1], triL[2]);
      }

      // assert that the tet does not contain any duplicated nodes
      for ( int node = 0; node < 4; node++)
        SANS_ASSERT( tetL[node] < Number_of_Nodes );

      std::vector<int> tetR = {Vol_Tet_Connectivity[iTetR+1][0], Vol_Tet_Connectivity[iTetR+1][1],
                               Vol_Tet_Connectivity[iTetR+1][2], Vol_Tet_Connectivity[iTetR+1][3]};
      std::vector<int> tetRorig = tetR;
      for ( int node = 0; node < 4; node++)
        if (tetRorig[node] >= Number_of_Nodes)
          tetRorig[node] = invPointMap_[tetRorig[node]-Number_of_Nodes];

      // The left canonical trace was used to construct the triangle
      CanonicalTraceToCell canonicalL(trifaces[itri].trace, 1);

      // Get the left and right canonical faces
      CanonicalTraceToCell canonicalR = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(&triL[0], 3,
                                                                                                      &tetRorig[0], tetRorig.size());

      // Get the canonical triangle with the updated tetrahedral nodes
      for ( int n = 0; n < 3; n++)
        SANS_ASSERT(triL[n] == tetL[TraceNodes[canonicalL.trace][n]]);

      fldAssocBface[BC].setElementLeft ( iTetL, itrace );
      fldAssocBface[BC].setElementRight( iTetR, itrace );

      // Face signs for elements (L is +, R is -)
      fldAssocCell.setAssociativity( iTetL ).setFaceSign( +1, canonicalL.trace );
      fldAssocCell.setAssociativity( iTetR ).setFaceSign( canonicalR.orientation, canonicalR.trace );

      // Set the canonical faces
      fldAssocBface[BC].setCanonicalTraceLeft( canonicalL, itrace );
      fldAssocBface[BC].setCanonicalTraceRight( canonicalR, itrace );

      fldAssocBface[BC].setAssociativity( itrace ).setRank( 0 );

      //Set the nodes for the triangle face
      fldAssocBface[BC].setAssociativity( itrace ).setNodeGlobalMapping( &triL[0], 3 );


      for (int seg = 0; seg < 3; seg ++)
      {
        int iedge = -1;

        if ( tessModel.isEdgeSegment(triL,seg) )
        {
          iedge = tessModel.getEdgeIndexFromSegment(triL,seg);
          if ( !edges[iedge].hasAttribute(AFLR3::TREFFTZ) )
            continue; // Not a Trefftz segment
        }
        else
          continue; // Not an Edge segment

        // The canonical Frame is the seg number and always left so orientation is 1
        CanonicalTraceToCell canonicalFrameL(seg,1);
        const int BCframe = TrefftzFrameGroupIndexes[iedge];
        const int lineCount = TrefftzLineCount[iedge];

        // Get the nodes with the triangle with the updated nodes
        int lineL[2] = {triL[FrameNodes[seg][0]], triL[FrameNodes[seg][1]]};

        fldAssocBframe[BCframe].setGroupLeft( BC );
        fldAssocBframe[BCframe].setElementLeft( itrace, lineCount );

        fldAssocBframe[BCframe].setCanonicalTraceLeft( canonicalFrameL, lineCount );

        fldAssocBframe[BCframe].setAssociativity( lineCount ).setRank( 0 );

        //Set the nodes for the line
        fldAssocBframe[BCframe].setAssociativity( lineCount ).setNodeGlobalMapping( lineL, 2 );

        TrefftzLineCount[iedge]++;
      }
    }
    else  //Now we are on a boundary.
    {
      const int BC = marker-1;
      int itrace = -1;
      try
      {
        itrace = traceIDMap.at(UniqueElem(eTriangle, triL));
      }
      catch (const std::exception& e)
      {
        SANS_DEVELOPER_EXCEPTION("Could not find triangle (%d, %d, %d)", triL[0], triL[1], triL[2]);
      }

      fldAssocBface[BC].setElementLeft ( iTetL, itrace );

      // The left canonical triangle by construction
      CanonicalTraceToCell canonicalL(trifaces[itri].trace, 1);

      // Face signs for elements (L is +, R is -)
      fldAssocCell.setAssociativity( iTetL ).setFaceSign( +1, canonicalL.trace );

      // Set the canonical faces
      fldAssocBface[BC].setCanonicalTraceLeft( canonicalL, itrace );

      // Get the canonical triangle with the updated tetrahedral nodes
      int triLnew[Triangle::NNode];
      WakeCell BCframSide = WakeCell::Left;
      for ( int n = 0; n < Triangle::NNode; n++)
      {
        triLnew[n] = tetL[TraceNodes[canonicalL.trace][n]];
        if ( triLnew[n] >= Number_of_Nodes )
          BCframSide = WakeCell::Right;
      }

      fldAssocBface[BC].setAssociativity( itrace ).setRank( 0 );

      //Set the nodes for the triangle face
      fldAssocBface[BC].setAssociativity( itrace ).setNodeGlobalMapping( &triLnew[0], 3 );

      // Wake sheets cannot be BC faces for the Kutta condition
      if ( !modelBCFaces[BC].hasAttribute(AFLR3::WAKESHEET) && nDuplicatePoints > 0 )
      {

        // Only include faces that are connected to Kutta edges
        if (modelFaceKutta[BC] )
        {
          for (int n = 0; n < Triangle::NNode; n++)
          {
            if ( tessModel.modelTopoType(triL[n]) >= 0 && tessModel.modelTopoFromVertex(triL[n]).hasAttribute(AFLR3::WAKESHEET) )
            {
              if ( BCframSide == WakeCell::Left )
              {
                KuttaBCTraceElemLeft_[BC][triL[n]].KuttaPoint = newPointMap.at(triL[n]);
                KuttaBCTraceElemLeft_[BC][triL[n]].elems.push_back(itrace);
              }
              else
              {
                KuttaBCTraceElemRight_[BC][triLnew[n]].KuttaPoint = triLnew[n];
                KuttaBCTraceElemRight_[BC][triLnew[n]].elems.push_back(itrace);
              }
            }
          }
        }

        for (int seg = 0; seg < Triangle::NEdge; seg++)
        {
          int iedge;

          if ( tessModel.isEdgeSegment(triL, seg) )
          {
            iedge = tessModel.getEdgeIndexFromSegment(triL,seg);
            if ( !edges[iedge].hasAttribute(AFLR3::WAKESHEET) )
              continue; // Not a Kutta segment
          }
          else
            continue; // Not an Edge segment

          const int BCframe = KuttaFrameGroupIndexes[iedge];

          // Only use one BC face to set the frame connectivity
          if ( BCframSide == WakeCell::Left )
          {
            const int lineCount = KuttaLineCount[iedge];

            fldAssocBframe[BCframe].setGroupLeft( BC );

            // Get the nodes from the triangle with the updated nodes
            int lineL[2] = {triLnew[FrameNodes[seg][0]], triLnew[FrameNodes[seg][1]]};

            // Left should be original nodes
            SANS_ASSERT(lineL[0] < Number_of_Nodes);
            SANS_ASSERT(lineL[1] < Number_of_Nodes);

            fldAssocBframe[BCframe].setAssociativity( lineCount ).setRank( 0 );

            //Set the nodes for the line
            fldAssocBframe[BCframe].setAssociativity( lineCount ).setNodeGlobalMapping( lineL, 2 );

            // The canonical Frame is the seg number and always left so orientation is 1
            CanonicalTraceToCell canonicalFrameL(seg,1);

            fldAssocBframe[BCframe].setGroupLeft( BC );
            fldAssocBframe[BCframe].setElementLeft( itrace, lineCount );

            fldAssocBframe[BCframe].setCanonicalTraceLeft( canonicalFrameL, lineCount );

            KuttaLineCount[iedge]++;
          }
          else
            fldAssocBframe[BCframe].setGroupRight( BC );
        }
      }
    }
  }


  // Zipper up boundary frames
  for (std::size_t BCframe = 0; BCframe < fldAssocBframe.size(); BCframe++)
  {
    //Only zipper up if there is a right group
    if (!fldAssocBframe[BCframe].isGroupRightSet()) continue;

    const int BCR = fldAssocBframe[BCframe].getGroupRight();

    for (int elemTri = 0; elemTri < fldAssocBface[BCR].nElem(); elemTri++)
    {
      int triR[Triangle::NNode];
      fldAssocBface[BCR].getAssociativity( elemTri ).getNodeGlobalMapping( triR, Triangle::NNode );

      std::array<int,3> triOrigR;
      WakeCell BCframSide = WakeCell::Left;
      for ( int node = 0; node < Triangle::NNode; node++)
      {
        triOrigR[node] = triR[node];
        if (triOrigR[node] >= Number_of_Nodes)
        {
          BCframSide = WakeCell::Right;
          triOrigR[node] = invPointMap_[triR[node]-Number_of_Nodes];
        }
      }

      // Only try zippering with right triangles
      if (BCframSide == WakeCell::Left) continue;

      for (int seg = 0; seg < Triangle::NEdge; seg++)
      {
        if ( tessModel.isEdgeSegment(triOrigR, seg) )
        {
          int iedge = tessModel.getEdgeIndexFromSegment(triOrigR,seg);
          if ( !edges[iedge].hasAttribute(AFLR3::WAKESHEET) )
            continue; // Not a Kutta segment
        }
        else
          continue; // Not an Edge segment

        int lineR[2] = {triOrigR[FrameNodes[seg][0]], triOrigR[FrameNodes[seg][1]]};

        // Find the line element that matches the right triangle line
        for (int elemLine = 0; elemLine < fldAssocBframe[BCframe].nElem(); elemLine++)
        {
          // Get the nodes for the line
          int lineL[2];
          fldAssocBframe[BCframe].getAssociativity( elemLine ).getNodeGlobalMapping( lineL, 2 );

          // Left should be original nodes
          SANS_ASSERT(lineL[0] < Number_of_Nodes);
          SANS_ASSERT(lineL[1] < Number_of_Nodes);

          if ( ( lineL[0] == lineR[0] && lineL[1] == lineR[1] ) ||
               ( lineL[1] == lineR[0] && lineL[0] == lineR[1] ) )
          {
#if 0
            std::cout << "elemLine " << elemLine << std::endl;
            std::cout << "line  = {" << line[0] << ", " << line[1] << "}" << std::endl;
            std::cout << "lineR = {" << lineR[0] << ", " << lineR[1] << "}" << std::endl;
#endif
            // Get the right info
            CanonicalTraceToCell canonicalFrameR = TraceToCellRefCoord<Line, TopoD2, Triangle>::getCanonicalTrace(lineL, 2, &triOrigR[0], 3);

            fldAssocBframe[BCframe].setElementRight( elemTri, elemLine );
            fldAssocBframe[BCframe].setCanonicalTraceRight( canonicalFrameR, elemLine );
            break;
          }
        }
      }
    }
  }

  // Create cell group
  cellGroups_[0] = new FieldCellGroupType<Tet>( fldAssocCell );
  cellGroups_[0]->setDOF(DOF_, nDOF_);
  nElem_ = fldAssocCell.nElem();

  // Create the interior group
  for ( int I = 0; I < nInteriorFaces+1; I++ )
  {
    interiorTraceGroups_[I] = new FieldTraceGroupType<Triangle>( fldAssocIface[I] );
    interiorTraceGroups_[I]->setDOF(DOF_, nDOF_);
  }

  // Create the boundary groups
  for ( int BC = 0; BC < nBCFaces; BC++ )
  {
    boundaryTraceGroups_[BC] = new FieldTraceGroupType<Triangle>( fldAssocBface[BC] );
    boundaryTraceGroups_[BC]->setDOF(DOF_, nDOF_);
  }

  resizeBoundaryFrameGroups( fldAssocBframe.size() );

  // Create the boundary frame groups
  for ( std::size_t BCframe = 0; BCframe < fldAssocBframe.size(); BCframe++ )
  {
    boundaryFrameGroups_[BCframe] = new FieldFrameGroupType( fldAssocBframe[BCframe] );
    boundaryFrameGroups_[BCframe]->setDOF(DOF_, nDOF_);
  }

  // Check that the grid is correct
  checkGrid();
}

//---------------------------------------------------------------------------//
AFLR3::AFLR3( const Field_CG_Cell<PhysD3,TopoD3,MatrixSym>& metric  )
{
  INT_ Error_Flag = 0;

  const int (*TraceNodes)[ Triangle::NTrace ] = TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes;

  // Declare AFLR3 grid generation variables.
  // This is required in all implementations!

  int prog_argc = 1;
  char **prog_argv = NULL;

  Error_Flag = ug_add_new_arg (&prog_argv, (char*)"allocate_and_initialize_argv"); SANS_ASSERT( Error_Flag == 0 );

  //Error_Flag = ug_add_flag_arg ((char*)"-met2", &prog_argc, &prog_argv); SANS_ASSERT( Error_Flag == 0 );

  Error_Flag = ug_add_flag_arg ((char*)"-met3", &prog_argc, &prog_argv); SANS_ASSERT( Error_Flag == 0 );
  Error_Flag = ug_add_flag_arg ((char*)"cdfn=0.65", &prog_argc, &prog_argv); SANS_ASSERT( Error_Flag == 0 );

  //Error_Flag = ug_add_flag_arg ((char*)"mmetb=1", &prog_argc, &prog_argv); SANS_ASSERT( Error_Flag == 0 );

  // set the last argument to 1 to print what arguments have been set
  Error_Flag = ug_check_prog_param (prog_argv, prog_argc, 0); SANS_ASSERT( Error_Flag == 0 );

  INT_1D *Surf_Error_Flag = NULL;
  INT_1D *Surf_Grid_BC_Flag = NULL;
  INT_1D *Surf_ID_Flag = NULL;
  INT_1D *Surf_Reconnection_Flag = NULL;
  INT_3D *Surf_Tria_Connectivity = NULL;
  INT_4D *Surf_Quad_Connectivity = NULL;
  INT_4D *Vol_Tet_Connectivity = NULL;
  INT_5D *Vol_Pent_5_Connectivity = NULL;
  INT_6D *Vol_Pent_6_Connectivity = NULL;
  INT_8D *Vol_Hex_Connectivity = NULL;

  INT_4D *Vol_Tet_Neighbor = NULL;

  DOUBLE_3D *Coordinates = NULL;
  DOUBLE_1D *Initial_Normal_Spacing = NULL;
  DOUBLE_1D *BL_Thickness = NULL;

  INT_4D *BG_Vol_Tet_Neighbors = NULL;
  INT_4D *BG_Vol_Tet_Connectivity = NULL;

  DOUBLE_3D *BG_Coordinates = NULL;
  DOUBLE_1D *BG_Spacing = NULL;
  DOUBLE_6D *BG_Metric = NULL;

  DOUBLE_3D *Source_Coordinates = NULL;
  DOUBLE_1D *Source_Spacing = NULL;
  DOUBLE_6D *Source_Metric = NULL;

  INT_ Message_Flag = 1;
  INT_ Number_of_BL_Vol_Tets = 0;
  INT_ Number_of_Nodes = 0;
  INT_ Number_of_Surf_Quads = 0;
  INT_ Number_of_Surf_Trias = 0;
  INT_ Number_of_Vol_Hexs = 0;
  INT_ Number_of_Vol_Pents_5 = 0;
  INT_ Number_of_Vol_Pents_6 = 0;
  INT_ Number_of_Vol_Tets = 0;

  INT_ Number_of_BG_Nodes = 0;
  INT_ Number_of_BG_Vol_Tets = 0;

  INT_ Number_of_Source_Nodes = 0;

  const XField<PhysD3,TopoD3>& BG_xfld = metric.getXField();

  // TODO: This constructor assumes linear tetrahedral grids
  for (int i = 0; i < BG_xfld.nCellGroups(); i++)
  {
    SANS_ASSERT( BG_xfld.getCellGroupBase(i).topoTypeID() == typeid(Tet) );
    SANS_ASSERT( BG_xfld.getCellGroupBase(i).order() == 1 );
    SANS_ASSERT( metric.getCellGroupBase(i).order() == 1 );
  }

  // Total number of background mesh nodes
  Number_of_BG_Nodes = BG_xfld.nDOF();

  // Number of triangles in the grid (assumption asserted above)
  Number_of_BG_Vol_Tets = BG_xfld.nElem();

  // Count the number of boundary triangles
  for (int i = 0; i < BG_xfld.nBoundaryTraceGroups(); i++)
    Number_of_Surf_Trias += BG_xfld.getBoundaryTraceGroupBase(i).nElem();

  BG_Coordinates = (DOUBLE_3D *) ug_malloc (&Error_Flag, (Number_of_BG_Nodes+1) * sizeof (DOUBLE_3D)); SANS_ASSERT( Error_Flag == 0 );
  BG_Spacing     = (DOUBLE_1D *) ug_malloc (&Error_Flag, (Number_of_BG_Nodes+1) * sizeof (DOUBLE_1D)); SANS_ASSERT( Error_Flag == 0 );
  BG_Metric      = (DOUBLE_6D *) ug_malloc (&Error_Flag, (Number_of_BG_Nodes+1) * sizeof (DOUBLE_6D)); SANS_ASSERT( Error_Flag == 0 );

  // Copy over the coordinates and metric values into the AFLR3 input
  for (int n = 0; n < Number_of_BG_Nodes; n++)
  {
    for (int d = 0; d < Dim; d++)
      BG_Coordinates[n+1][d] = BG_xfld.DOF(n)[d]; // +1 for 1-based indexing

    BG_Spacing[n+1] = 0;

    // copy over as upper triangular matrix
    int k = 0;
    for (int i = 0; i < MatrixSym::M; i++)
      for (int j = i; j < MatrixSym::N; j++)
        BG_Metric[n+1][k++] = metric.DOF(n)(i,j); // +1 for 1-based indexing
  }


  Surf_Tria_Connectivity = (INT_3D *) ug_malloc (&Error_Flag, (Number_of_Surf_Trias+1) * sizeof (INT_3D)); SANS_ASSERT( Error_Flag == 0 );
  Surf_ID_Flag           = (INT_1D *) ug_malloc (&Error_Flag, (Number_of_Surf_Trias+1) * sizeof (INT_1D)); SANS_ASSERT( Error_Flag == 0 );
  Surf_Grid_BC_Flag      = (INT_1D *) ug_malloc (&Error_Flag, (Number_of_Surf_Trias+1) * sizeof (INT_1D)); SANS_ASSERT( Error_Flag == 0 );
  Surf_Reconnection_Flag = (INT_1D *) ug_malloc (&Error_Flag, (Number_of_Surf_Trias+1) * sizeof (INT_1D)); SANS_ASSERT( Error_Flag == 0 );

  std::set<int> Surf_Nodes;
  int itri = 0;
  for (int i = 0; i < BG_xfld.nBoundaryTraceGroups(); i++)
  {
    int Bnd_ID = i+1;

    const FieldTraceGroupType<Triangle>& xfldBCTrace = BG_xfld.getBoundaryTraceGroup<Triangle>(i);
    const int nBCElem = xfldBCTrace.nElem();
    int mapNodeGlobal[Triangle::NNode];
    for (int elem = 0; elem < nBCElem; elem++)
    {
      // Set the boundary ID
      Surf_ID_Flag[itri+1] = Bnd_ID;

      Surf_Grid_BC_Flag[itri+1] = STD_UG2_GBC;

      Surf_Reconnection_Flag[itri+1] = 0;

      // Save off the unique nodes on the surfaces
      xfldBCTrace.associativity( elem ).getNodeGlobalMapping( mapNodeGlobal, Triangle::NNode );
      Surf_Nodes.insert(mapNodeGlobal, mapNodeGlobal+Triangle::NNode);

      itri++;
    }
  }
  SANS_ASSERT( itri == Number_of_Surf_Trias );

  // total number of boundary element nodes
  Number_of_Nodes = Surf_Nodes.size();
  Coordinates = (DOUBLE_3D *) ug_malloc (&Error_Flag, (Number_of_Nodes+1) * sizeof (DOUBLE_3D)); SANS_ASSERT( Error_Flag == 0 );

  std::map<int,int> Surf_Node_Map;

  // Extract coordinates on boundaries and crate a mapping from the BC mesh indexing to a
  // boundary only indexing
  int inode = 1; // +1 for 1-based indexing
  for (int i : Surf_Nodes)
  {
    for (int d = 0; d < Dim; d++)
      Coordinates[inode][d] = BG_xfld.DOF(i)[d];
    Surf_Node_Map[i] = inode;
    inode++;
  }

  // allocate the BC mesh tet indexing
  BG_Vol_Tet_Connectivity = (INT_4D *) ug_malloc (&Error_Flag, (Number_of_BG_Vol_Tets+1) * sizeof (INT_4D)); SANS_ASSERT( Error_Flag == 0 );

  itri = 0;
  for (int i = 0; i < BG_xfld.nBoundaryTraceGroups(); i++)
  {
    const FieldTraceGroupType<Triangle>& xfldBCTrace = BG_xfld.getBoundaryTraceGroup<Triangle>(i);
    const int nBCElem = xfldBCTrace.nElem();
    int mapNodeGlobal[Triangle::NNode];
    for (int elem = 0; elem < nBCElem; elem++)
    {
      // Set the connectivity
      xfldBCTrace.associativity( elem ).getNodeGlobalMapping( mapNodeGlobal, Triangle::NNode );
      for (int n = 0; n < Triangle::NNode; n++)
        Surf_Tria_Connectivity[itri+1][n] = Surf_Node_Map[mapNodeGlobal[n]];

      itri++;
    }
  }
  SANS_ASSERT( itri == Number_of_Surf_Trias );


  int itet = 0;
  for (int i = 0; i < BG_xfld.nCellGroups(); i++)
  {
    const FieldCellGroupType<Tet>& xfldCell = BG_xfld.getCellGroup<Tet>(i);
    const int nElem = xfldCell.nElem();
    int mapNodeGlobal[Tet::NNode];
    for (int elem = 0; elem < nElem; elem++)
    {
      // Set the connectivity
      xfldCell.associativity( elem ).getNodeGlobalMapping( mapNodeGlobal, Tet::NNode );
      for (int n = 0; n < Tet::NNode; n++)
        BG_Vol_Tet_Connectivity[itet+1][n] = mapNodeGlobal[n] + 1; // +1 for 1-based indexing

      itet++;
    }
  }
  SANS_ASSERT( itet == Number_of_BG_Vol_Tets );

  ug_set_prog_param_n_dim (3);

  ug_set_prog_param_function1 (ug_initialize_aflr_param);
  ug_set_prog_param_function1 (ug_gq_initialize_param); // optional
  ug_set_prog_param_function2 (aflr3_initialize_param);
  ug_set_prog_param_function2 (aflr3_anbl3_initialize_param);
  ug_set_prog_param_function2 (ice3_initialize_param);
  ug_set_prog_param_function2 (ug3_qchk_initialize_param); // optional

  Error_Flag = aflr3_grid_generator (prog_argc, prog_argv,
                                     Message_Flag,
                                     &Number_of_BL_Vol_Tets,
                                     &Number_of_BG_Nodes,
                                     &Number_of_BG_Vol_Tets,
                                     &Number_of_Nodes,
                                     &Number_of_Source_Nodes,
                                     &Number_of_Surf_Quads,
                                     &Number_of_Surf_Trias,
                                     &Number_of_Vol_Hexs,
                                     &Number_of_Vol_Pents_5,
                                     &Number_of_Vol_Pents_6,
                                     &Number_of_Vol_Tets,
                                     &Surf_Error_Flag,
                                     &Surf_Grid_BC_Flag,
                                     &Surf_ID_Flag,
                                     &Surf_Reconnection_Flag,
                                     &Surf_Quad_Connectivity,
                                     &Surf_Tria_Connectivity,
                                     &Vol_Hex_Connectivity,
                                     &Vol_Pent_5_Connectivity,
                                     &Vol_Pent_6_Connectivity,
                                     &Vol_Tet_Connectivity,
                                     &BG_Vol_Tet_Neighbors,
                                     &BG_Vol_Tet_Connectivity,
                                     &Coordinates,
                                     &Initial_Normal_Spacing, &BL_Thickness,
                                     &BG_Coordinates,
                                     &BG_Spacing,
                                     &BG_Metric,
                                     &Source_Coordinates,
                                     &Source_Spacing,
                                     &Source_Metric);
  // TODO: Better error handling
  SANS_ASSERT( Error_Flag == 0 );

  //Free the arguments
  ug_free_argv (prog_argv);

  ug_free(BG_Coordinates);
  ug_free(BG_Spacing);
  ug_free(BG_Metric);

  ug_free(BG_Vol_Tet_Neighbors);
  ug_free(BG_Vol_Tet_Connectivity);

  std::unique_ptr<INT_1D[],AFLR_free> delSurf_Error_Flag(Surf_Error_Flag, AFLR_free(0));
  std::unique_ptr<DOUBLE_3D[],AFLR_free> delCoordinates(Coordinates, AFLR_free(0));
  std::unique_ptr<INT_4D[],AFLR_free> delVol_Tet_Connectivity(Vol_Tet_Connectivity, AFLR_free(0));

  std::unique_ptr<INT_3D[],AFLR_free> delSurf_Tria_Connectivity(Surf_Tria_Connectivity, AFLR_free(0));
  std::unique_ptr<INT_1D[],AFLR_free> delSurf_ID_Flag(Surf_ID_Flag, AFLR_free(0));
  std::unique_ptr<INT_1D[],AFLR_free> delSurf_Grid_BC_Flag(Surf_Grid_BC_Flag, AFLR_free(0));

  std::unique_ptr<DOUBLE_1D[],AFLR_free> delInitial_Normal_Spacing(Initial_Normal_Spacing, AFLR_free(0));
  std::unique_ptr<DOUBLE_1D[],AFLR_free> delBL_Thickness(BL_Thickness, AFLR_free(0));
  std::unique_ptr<INT_1D[],AFLR_free> delSurf_Reconnection_Flag(Surf_Reconnection_Flag, AFLR_free(0));

  // TODO: Better error handling
  SANS_ASSERT( Error_Flag == 0 );

  // Get neighbor information
  Error_Flag = ug3_ieliel2(Number_of_Vol_Tets, Number_of_Nodes, Vol_Tet_Connectivity, &Vol_Tet_Neighbor);
  std::unique_ptr<INT_4D[],AFLR_free> delVol_Tet_Neighbor(Vol_Tet_Neighbor, AFLR_free(Error_Flag));

  // Count the number of trace triangles in the mesh
  int ntri = 0;

  for ( int iTetL = 0; iTetL < Number_of_Vol_Tets; iTetL++ )
  {
    for (int n = 0; n < 4; n ++)
    {
      int iTetR = Vol_Tet_Neighbor[iTetL+1][n];

      // Only consider right test's with a higher index
      if (iTetR < iTetL+1 && iTetR > 0) continue;

      ntri++;
    }
  }

  std::vector<TriFace> trifaces(ntri);
  ntri = 0;

  for ( int iTetL = 0; iTetL < Number_of_Vol_Tets; iTetL++ )
  {
    for (int n = 0; n < 4; n ++)
    {
      int iTetR = Vol_Tet_Neighbor[iTetL+1][n];

      // Only consider right test's with a higher index
      if (iTetR < iTetL+1 && iTetR > 0) continue;

      // Triangle nodes
      for (int k = 0; k < 3; k++ )
        trifaces[ntri].tri[k] = Vol_Tet_Connectivity[iTetL+1][TraceNodes[n][k]]-1;

      // Set neighbor information
      trifaces[ntri].iTetL = iTetL;
      trifaces[ntri].iTetR = iTetR-1; // iTetR might be zero, which will be changed to a BC
      trifaces[ntri].trace = n;
      trifaces[ntri].face = 0; // BC's will be marked after the neighbor information is updated

      ntri++;
    }
  }

  // Update neighbor information with BC markers
  Error_Flag = ug3_ieliel2b(Number_of_Surf_Trias, Number_of_Vol_Tets, Number_of_Nodes,
                            Surf_ID_Flag, Surf_Tria_Connectivity, Vol_Tet_Connectivity, Vol_Tet_Neighbor);

  // TODO: Better error handling
  SANS_ASSERT( Error_Flag == 0 );

  // Update all face markers
  for ( int itri = 0; itri < ntri; itri++ )
  {
    int iTetR = Vol_Tet_Neighbor[trifaces[itri].iTetL+1][trifaces[itri].trace];

    // Mark all the BC faces
    if (iTetR < 0)
      trifaces[itri].face = Surf_ID_Flag[-iTetR];

    // Restore the neighbor information on interior EGADS faces
    if (trifaces[itri].iTetR >= 0)
      Vol_Tet_Neighbor[trifaces[itri].iTetL+1][trifaces[itri].trace] = trifaces[itri].iTetR+1;
  }


  for ( int iTet = 0; iTet < Number_of_Vol_Tets; iTet++ )
    for (int n = 0; n < 4; n ++)
      Vol_Tet_Connectivity[iTet+1][n]--; // Change to 0-based indexing


  int nBCFaces = BG_xfld.nBoundaryTraceGroups();

  //Create the DOF arrays
  resizeDOF(Number_of_Nodes);

  // One Cell group for now...
  resizeCellGroups(1);
  resizeInteriorTraceGroups(1);
  resizeBoundaryTraceGroups(nBCFaces);

  //Copy over the nodal values
  for ( int i = 0; i < Number_of_Nodes; i++ )
    DOF(i) = {Coordinates[i+1][0], Coordinates[i+1][1], Coordinates[i+1][2]};

  // Count the number of triangles on each face
  std::vector< int > BCFaceTriCount(nBCFaces, 0);
  std::vector< int > IFaceTriCount(1, 0);
  for ( int itri = 0; itri < ntri; itri++ )
  {
    int face = trifaces[itri].face;
    if ( face > 0 )
      BCFaceTriCount[face-1]++; // Boundary EGADS faces are positive numbers > 0
    else
    {
      SANS_ASSERT(face == 0);
      IFaceTriCount[-face]++; // Interior EGADS (and tet) faces are a negative number or 0
    }
  }

  // volume field variable
  FieldCellGroupType<Tet>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionVolumeBase<Tet>::HierarchicalP1, Number_of_Vol_Tets );

  std::vector< FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType > fldAssocIface(1);

  for ( int I = 0; I < 1; I++ ) // +1 for the volume interior trace group
  {
    fldAssocIface[I].resize( BasisFunctionAreaBase<Triangle>::HierarchicalP1, IFaceTriCount[I] );

    //Only one cell group for the interior trace group
    fldAssocIface[I].setGroupLeft ( 0 );
    fldAssocIface[I].setGroupRight( 0 );

    IFaceTriCount[I] = 0;
  }

  //Create the boundary association constructors
  std::vector< FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType > fldAssocBface(nBCFaces);

  for ( int BC = 0; BC < nBCFaces; BC++ )
  {
    fldAssocBface[BC].resize(BasisFunctionAreaBase<Triangle>::HierarchicalP1, BCFaceTriCount[BC]);

    //Only one cell group for the interior trace group
    fldAssocBface[BC].setGroupLeft ( 0 );

    BCFaceTriCount[BC] = 0;
  }

  //element volume associativity after duplicate nodes have been updated
  for ( int iTet = 0; iTet < Number_of_Vol_Tets; iTet++ )
  {
    fldAssocCell.setAssociativity( iTet ).setRank( 0 );
    fldAssocCell.setAssociativity( iTet ).setNodeGlobalMapping( Vol_Tet_Connectivity[iTet+1], 4 );
  }

  // trace associativity
  for ( int itri = 0; itri < ntri; itri++ )
  {
    int iTetL = trifaces[itri].iTetL;
    int iTetR = trifaces[itri].iTetR;

    // Triangle nodes
    std::array<int,3> triL = {{trifaces[itri].tri[0], trifaces[itri].tri[1], trifaces[itri].tri[2]}};

    // Left tet nodes
    std::vector<int> tetL = {Vol_Tet_Connectivity[iTetL+1][0], Vol_Tet_Connectivity[iTetL+1][1],
                             Vol_Tet_Connectivity[iTetL+1][2], Vol_Tet_Connectivity[iTetL+1][3]};

    int marker = trifaces[itri].face;

    if ( iTetR >= 0 && marker <= 0) //Volume Interior triangle, or interior face
    {
      const int I = -marker;
      const int faceCount = IFaceTriCount[I];

      std::vector<int> tetR = {Vol_Tet_Connectivity[iTetR+1][0], Vol_Tet_Connectivity[iTetR+1][1],
                               Vol_Tet_Connectivity[iTetR+1][2], Vol_Tet_Connectivity[iTetR+1][3]};

      std::vector<int> tetRorig = tetR;
      for ( int node = 0; node < 4; node++)
        if (tetRorig[node] >= Number_of_Nodes)
          tetRorig[node] = invPointMap_[tetRorig[node]-Number_of_Nodes];


      fldAssocIface[I].setElementLeft ( iTetL, faceCount );
      fldAssocIface[I].setElementRight( iTetR, faceCount );

      // The left canonical trace was used to construct the triangle
      CanonicalTraceToCell canonicalL(trifaces[itri].trace, 1);

      CanonicalTraceToCell canonicalR = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(&triL[0], 3,
                                                                                                      &tetRorig[0], tetRorig.size());
      // Face signs for elements (L is +, R is -)
      fldAssocCell.setAssociativity( iTetL ).setFaceSign( +1, canonicalL.trace );
      fldAssocCell.setAssociativity( iTetR ).setFaceSign( canonicalR.orientation, canonicalR.trace );

      // Set the canonical faces
      fldAssocIface[I].setCanonicalTraceLeft( canonicalL, faceCount );
      fldAssocIface[I].setCanonicalTraceRight( canonicalR, faceCount );

      fldAssocIface[I].setAssociativity( faceCount ).setRank( 0 );

      //Set the nodes for the triangle face
      fldAssocIface[I].setAssociativity( faceCount ).setNodeGlobalMapping( triL.data(), 3 );

      IFaceTriCount[I]++;
    }
    else if ( iTetR >= 0 && marker > 0) //Interior boundary face
    {
      const int BC = marker-1;
      const int faceCount = BCFaceTriCount[BC];

      for ( int node = 0; node < 4; node++)
        SANS_ASSERT( tetL[node] < Number_of_Nodes );

      std::vector<int> tetR = {Vol_Tet_Connectivity[iTetR+1][0], Vol_Tet_Connectivity[iTetR+1][1],
                               Vol_Tet_Connectivity[iTetR+1][2], Vol_Tet_Connectivity[iTetR+1][3]};

      fldAssocBface[BC].setElementLeft ( iTetL, faceCount );
      fldAssocBface[BC].setElementRight( iTetR, faceCount );

      // The left canonical trace was used to construct the triangle
      CanonicalTraceToCell canonicalL(trifaces[itri].trace, 1);

      // Get the left and right canonical faces
      CanonicalTraceToCell canonicalR = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(triL.data(), 3,
                                                                                                      tetR.data(), tetR.size());

      // Get the canonical triangle with the updated tetrahedral nodes
      for ( int n = 0; n < 3; n++)
        SANS_ASSERT(triL[n] == tetL[TraceNodes[canonicalL.trace][n]]);

      // Face signs for elements (L is +, R is -)
      fldAssocCell.setAssociativity( iTetL ).setFaceSign( +1, canonicalL.trace );
      fldAssocCell.setAssociativity( iTetR ).setFaceSign( canonicalR.orientation, canonicalR.trace );

      // Set the canonical faces
      fldAssocBface[BC].setCanonicalTraceLeft( canonicalL, faceCount );
      fldAssocBface[BC].setCanonicalTraceRight( canonicalR, faceCount );

      fldAssocBface[BC].setAssociativity( faceCount ).setRank( 0 );

      //Set the nodes for the triangle face
      fldAssocBface[BC].setAssociativity( faceCount ).setNodeGlobalMapping( triL.data(), 3 );

      BCFaceTriCount[BC]++;
    }
    else  //Now we are on a boundary.
    {
      const int BC = marker-1;
      const int faceCount = BCFaceTriCount[BC];

      fldAssocBface[BC].setElementLeft ( iTetL, faceCount );

      // The left canonical triangle by construction
      CanonicalTraceToCell canonicalL(trifaces[itri].trace, 1);

      // Face signs for elements (L is +, R is -)
      fldAssocCell.setAssociativity( iTetL ).setFaceSign( +1, canonicalL.trace );

      // Set the canonical faces
      fldAssocBface[BC].setCanonicalTraceLeft( canonicalL, faceCount );

      fldAssocBface[BC].setAssociativity( faceCount ).setRank( 0 );

      //Set the nodes for the triangle face
      fldAssocBface[BC].setAssociativity( faceCount ).setNodeGlobalMapping( triL.data(), 3 );

      BCFaceTriCount[BC]++;
    }

  }

  // Create cell group
  cellGroups_[0] = new FieldCellGroupType<Tet>( fldAssocCell );
  cellGroups_[0]->setDOF(DOF_, nDOF_);
  nElem_ = fldAssocCell.nElem();

  // Create the interior group
  for ( int I = 0; I < 1; I++ )
  {
    interiorTraceGroups_[I] = new FieldTraceGroupType<Triangle>( fldAssocIface[I] );
    interiorTraceGroups_[I]->setDOF(DOF_, nDOF_);
  }

  // Create the boundary groups
  for ( int BC = 0; BC < nBCFaces; BC++ )
  {
    boundaryTraceGroups_[BC] = new FieldTraceGroupType<Triangle>( fldAssocBface[BC] );
    boundaryTraceGroups_[BC]->setDOF(DOF_, nDOF_);
  }

  // Check that the grid is correct
  checkGrid();
}

}
