// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SANS_AFLR3_H
#define SANS_AFLR3_H

//Generates a volume tetrahedron grid given a tessellated EGADS model using AFLR3

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "Meshing/EGTess/EGTessModel.h"

#include "Field/XField3D_Wake.h"
#include "Field/FieldVolume_CG_Cell.h"

namespace SANS
{

struct AFLRParams : noncopyable
{
  const ParameterString FilenameBase{"FilenameBase", "tmp/", "Default filepath prefix for generated files"};

  static void checkInputs(PyDict d);
  static AFLRParams params;
};


class AFLR3 : public XField3D_Wake
{
public:

  typedef XField3D_Wake BaseType;
  using BaseType::Dim;
  typedef BaseType::VectorX CartCoord;
  typedef DLA::MatrixSymS<Dim,Real> MatrixSym;

  AFLR3( const AFLR3& ) = delete;
  AFLR3& operator=( const AFLR3& ) = delete;

  //Constructor based on an EGADS model tessellation
  explicit AFLR3( const EGADS::EGTessModel& tessModel );

  // Constuctor based on a metric field
  explicit AFLR3( const Field_CG_Cell<PhysD3,TopoD3,MatrixSym>& metric );
};

}

#endif //SANS_AFLR3_H
