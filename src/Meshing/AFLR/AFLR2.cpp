// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "AFLR2.h"

#include <egads.h>

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "Field/XFieldLine_Traits.h"

#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include <memory> //shared_ptr
#include <algorithm>
#include <vector>
#include <set>

// AFLR2 Library include
#include <aflr2c/AFLR2_LIB.h>

namespace SANS
{

//----------------------------------------------------------------------------//
AFLR2::~AFLR2()
{
}


namespace // private to this file
{
//A deleter for AFLR allocated arrays
struct AFLR_free
{
  explicit AFLR_free(INT_ Error_Flag) { SANS_ASSERT(Error_Flag == 0); }
  inline void operator()(void* p)
  {
    ug_free(p);
  }
};

struct LineTrace
{
  int nodes[2];
  int iTriL;
  int iTriR;
  int trace;
  int ID;
};

typedef DLA::VectorS<2,Real> VectorX;

}

AFLR2::AFLR2( const Field_CG_Cell<PhysD2,TopoD2,MatrixSym>& metric )
{

  // need this to fix link error on OSX
  aflr2_xplt_inl((char*)"SANS");

  // A line field element used to check normal vectors
  FieldTraceGroupType<Line>::ElementType<> elemTrace( BasisFunctionLineBase::HierarchicalP1 );
  FieldTraceGroupType<Line>::ElementType<>::RefCoordType sRef = Line::centerRef;
  VectorX Ntri, Nface, X, TetCenter, CenterVector;

  const int (*TraceNodes)[ Line::NTrace ] = TraceToCellRefCoord<Line, TopoD2, Triangle>::TraceNodes;



  // Declare AFLR2 grid generation variables.
  // This is required in all implementations!

  int prog_argc = 0;
  char **prog_argv = NULL;

  prog_argc += 1;
  prog_argv = (char**)EG_reall(prog_argv, prog_argc*sizeof(char *));
  prog_argv[prog_argc-1] = (char*)EG_alloc((strlen ("SANS\0") + 1) * sizeof (char));
  sprintf(prog_argv[prog_argc-1],"%s","SANS\0");

  prog_argc += 1;
  prog_argv = (char**)EG_reall(prog_argv, prog_argc*sizeof(char *));
  prog_argv[prog_argc-1] = (char*)EG_alloc((strlen ("-met3\0") + 1) * sizeof (char));
  sprintf(prog_argv[prog_argc-1],"%s","-met3\0");

#if 1
  // Split interior edges with both nodes on boundaries
  prog_argc += 1;
  prog_argv = (char**)EG_reall(prog_argv, prog_argc*sizeof(char *));
  prog_argv[prog_argc-1] = (char*)EG_alloc((strlen ("-mier=1\0") + 1) * sizeof (char));
  sprintf(prog_argv[prog_argc-1],"%s","-mier=1\0");
#endif

  INT_1D *Bnd_Edge_Grid_BC_Flag = NULL;
  INT_1D *Bnd_Edge_ID_Flag = NULL;
  INT_1D *Bnd_Edge_Error_Flag  =  NULL;
  INT_2D *Bnd_Edge_Connectivity = NULL;
  INT_3D *Tria_Connectivity = NULL;
  INT_4D *Quad_Connectivity = NULL;

  DOUBLE_2D *Coordinates = NULL;
  DOUBLE_1D *Initial_Normal_Spacing = NULL;

  INT_1D *BG_Bnd_Edge_Grid_BC_Flag = NULL;
  INT_1D *BG_Bnd_Edge_ID_Flag = NULL;
  INT_2D *BG_Bnd_Edge_Connectivity = NULL;
  INT_3D *BG_Tria_Neighbors = NULL;
  INT_3D *BG_Tria_Connectivity = NULL;

  DOUBLE_1D *BG_Spacing = NULL;
  DOUBLE_2D *BG_Coordinates = NULL;
  DOUBLE_3D *BG_Metric = NULL;

  DOUBLE_1D *Source_Spacing = NULL;
  DOUBLE_2D *Source_Coordinates = NULL;
  DOUBLE_3D *Source_Metric = NULL;

  INT_ Error_Flag = 0;
  INT_ Message_Flag = 0;
  INT_ Number_of_Bnd_Edges = 0;
  INT_ Number_of_Nodes = 0;
  INT_ Number_of_Quads = 0;
  INT_ Number_of_Trias = 0;

  INT_ Number_of_BG_Bnd_Edges = 0;
  INT_ Number_of_BG_Nodes = 0;
  INT_ Number_of_BG_Trias = 0;

  INT_ Number_of_Source_Nodes = 0;

  const XField<PhysD2,TopoD2>& BG_xfld = metric.getXField();

  // TODO: This constructor assumes linear triangular grids
  for (int i = 0; i < BG_xfld.nCellGroups(); i++)
  {
    SANS_ASSERT( BG_xfld.getCellGroupBase(i).topoTypeID() == typeid(Triangle) );
    SANS_ASSERT( BG_xfld.getCellGroupBase(i).order() == 1 );
    SANS_ASSERT( metric.getCellGroupBase(i).order() == 1 );
  }

  ug_set_prog_param_n_dim (2);

  ug_set_prog_param_function1 (ug_initialize_aflr_param);
  ug_set_prog_param_function2 (aflr2_initialize_param);

  // Total number of nodes
  Number_of_BG_Nodes = BG_xfld.nDOF();

  // Number of triangles in the grid (assumption asserted above)
  Number_of_BG_Trias = BG_xfld.nElem();

  // Count the number of boundary edges
  for (int i = 0; i < BG_xfld.nBoundaryTraceGroups(); i++)
  {
    Number_of_Bnd_Edges    += BG_xfld.getBoundaryTraceGroupBase(i).nElem();
    Number_of_BG_Bnd_Edges += BG_xfld.getBoundaryTraceGroupBase(i).nElem();
  }

  BG_Coordinates = (DOUBLE_2D *) ug_malloc (&Error_Flag, (Number_of_BG_Nodes+1) * sizeof (DOUBLE_2D)); SANS_ASSERT( Error_Flag == 0 );

  Bnd_Edge_Connectivity    = (INT_2D *) ug_malloc (&Error_Flag, (Number_of_Bnd_Edges+1)    * sizeof (INT_2D)); SANS_ASSERT( Error_Flag == 0 );
  BG_Bnd_Edge_Connectivity = (INT_2D *) ug_malloc (&Error_Flag, (Number_of_BG_Bnd_Edges+1) * sizeof (INT_2D)); SANS_ASSERT( Error_Flag == 0 );

  Bnd_Edge_ID_Flag    = (INT_1D *) ug_malloc (&Error_Flag, (Number_of_Bnd_Edges+1)    * sizeof (INT_1D)); SANS_ASSERT( Error_Flag == 0 );
  BG_Bnd_Edge_ID_Flag = (INT_1D *) ug_malloc (&Error_Flag, (Number_of_BG_Bnd_Edges+1) * sizeof (INT_1D)); SANS_ASSERT( Error_Flag == 0 );

  Bnd_Edge_Grid_BC_Flag    = (INT_1D *) ug_malloc (&Error_Flag, (Number_of_Bnd_Edges+1)    * sizeof (INT_1D)); SANS_ASSERT( Error_Flag == 0 );
  BG_Bnd_Edge_Grid_BC_Flag = (INT_1D *) ug_malloc (&Error_Flag, (Number_of_BG_Bnd_Edges+1) * sizeof (INT_1D)); SANS_ASSERT( Error_Flag == 0 );

  BG_Tria_Connectivity = (INT_3D *) ug_malloc (&Error_Flag, (Number_of_BG_Trias+1) * sizeof (INT_3D)); SANS_ASSERT( Error_Flag == 0 );

  BG_Spacing = (DOUBLE_1D *) ug_malloc (&Error_Flag, (Number_of_BG_Nodes+1) * sizeof (DOUBLE_1D));  SANS_ASSERT( Error_Flag == 0 );
  BG_Metric  = (DOUBLE_3D *) ug_malloc (&Error_Flag, (Number_of_BG_Nodes+1) * sizeof (DOUBLE_3D));  SANS_ASSERT( Error_Flag == 0 );

  // Copy over the coordinates and metric values into the AFLR2 input
  for (int i = 0; i < Number_of_BG_Nodes; i++)
  {
    for (int d = 0; d < Dim; d++)
      BG_Coordinates[i+1][d] = BG_xfld.DOF(i)[d]; // +1 for 1-based indexing

    BG_Spacing[i+1] = 0;
    for (int n = 0; n < MatrixSym::SIZE; n++)
      BG_Metric[i+1][n] = metric.DOF(i).value(n); // +1 for 1-based indexing
  }

  std::set<int> Bnd_Nodes;
  int iedge = 0;
  for (int i = 0; i < BG_xfld.nBoundaryTraceGroups(); i++)
  {
    int Bnd_ID = i+1;

    const FieldTraceGroupType<Line>& xfldBCTrace = BG_xfld.getBoundaryTraceGroup<Line>(i);
    const int nBCElem = xfldBCTrace.nElem();
    int mapNodeGlobal[Line::NNode];
    for (int elem = 0; elem < nBCElem; elem++)
    {
      // Set the boundary ID
      Bnd_Edge_ID_Flag[iedge+1]    = Bnd_ID;
      BG_Bnd_Edge_ID_Flag[iedge+1] = Bnd_ID;

      Bnd_Edge_Grid_BC_Flag[iedge+1]    = STD_UG2_GBC;
      BG_Bnd_Edge_Grid_BC_Flag[iedge+1] = STD_UG2_GBC;

      // Set the connectivity
      xfldBCTrace.associativity( elem ).getNodeGlobalMapping( mapNodeGlobal, Line::NNode );
      for (int n = 0; n < Line::NNode; n++)
      {
        BG_Bnd_Edge_Connectivity[iedge+1][n] = mapNodeGlobal[n] + 1; // +1 for 1-based indexing

        Bnd_Nodes.insert(mapNodeGlobal[n]);
      }
      iedge++;
    }
  }
  SANS_ASSERT( iedge == Number_of_BG_Bnd_Edges );

  // total number of boundary element nodes
  Number_of_Nodes = Bnd_Nodes.size();
  Coordinates = (DOUBLE_2D *) ug_malloc (&Error_Flag, (Number_of_Nodes+1) * sizeof (DOUBLE_2D)); SANS_ASSERT( Error_Flag == 0 );

  std::map<int,int> Bnd_Node_Map;

  int inode = 1; // +1 for 1-based indexing
  for (int i : Bnd_Nodes)
  {
    for (int d = 0; d < Dim; d++)
      Coordinates[inode][d] = BG_xfld.DOF(i)[d];
    Bnd_Node_Map[i] = inode;
    inode++;
  }

  iedge = 0;
  for (int i = 0; i < BG_xfld.nBoundaryTraceGroups(); i++)
  {
    const FieldTraceGroupType<Line>& xfldBCTrace = BG_xfld.getBoundaryTraceGroup<Line>(i);
    const int nBCElem = xfldBCTrace.nElem();
    int mapNodeGlobal[Line::NNode];
    for (int elem = 0; elem < nBCElem; elem++)
    {
      // Set the connectivity
      xfldBCTrace.associativity( elem ).getNodeGlobalMapping( mapNodeGlobal, Line::NNode );
      for (int n = 0; n < Line::NNode; n++)
      {
        Bnd_Edge_Connectivity[iedge+1][n] = Bnd_Node_Map[mapNodeGlobal[n]];
      }
      iedge++;
    }
  }
  SANS_ASSERT( iedge == Number_of_BG_Bnd_Edges );


  int itria = 0;
  for (int i = 0; i < BG_xfld.nCellGroups(); i++)
  {
    const FieldCellGroupType<Triangle>& xfldCell = BG_xfld.getCellGroup<Triangle>(i);
    const int nElem = xfldCell.nElem();
    int mapNodeGlobal[Triangle::NNode];
    for (int elem = 0; elem < nElem; elem++)
    {
      // Set the connectivity
      xfldCell.associativity( elem ).getNodeGlobalMapping( mapNodeGlobal, Triangle::NNode );
      for (int n = 0; n < Triangle::NNode; n++)
        BG_Tria_Connectivity[itria+1][n] = mapNodeGlobal[n] + 1; // +1 for 1-based indexing

      itria++;
    }
  }
  SANS_ASSERT( itria == Number_of_BG_Trias );

  Error_Flag = aflr2_grid_generator (prog_argc, prog_argv,
                                     Message_Flag,
                                     &Number_of_Bnd_Edges, &Number_of_Nodes,
                                     &Number_of_Quads, &Number_of_Trias,
                                     &Number_of_BG_Bnd_Edges,
                                     &Number_of_BG_Nodes, &Number_of_BG_Trias,
                                     &Number_of_Source_Nodes,
                                     &Bnd_Edge_Connectivity,
                                     &Bnd_Edge_Error_Flag,
                                     &Bnd_Edge_Grid_BC_Flag,
                                     &Bnd_Edge_ID_Flag,
                                     &Quad_Connectivity, &Tria_Connectivity,
                                     &BG_Bnd_Edge_Connectivity,
                                     &BG_Bnd_Edge_Grid_BC_Flag,
                                     &BG_Bnd_Edge_ID_Flag,
                                     &BG_Tria_Neighbors,
                                     &BG_Tria_Connectivity,
                                     &Coordinates,
                                     Initial_Normal_Spacing,
                                     &BG_Coordinates,
                                     &BG_Spacing,
                                     &BG_Metric,
                                     &Source_Coordinates,
                                     &Source_Spacing,
                                     &Source_Metric);
  // TODO: Better error handling
  SANS_ASSERT( Error_Flag == 0 );

  std::unique_ptr<INT_1D[],AFLR_free> delBnd_Edge_Grid_BC_Flag(Bnd_Edge_Grid_BC_Flag, AFLR_free(0));
  std::unique_ptr<INT_1D[],AFLR_free> delBnd_Edge_ID_Flag(Bnd_Edge_ID_Flag, AFLR_free(0));
  std::unique_ptr<INT_1D[],AFLR_free> delBnd_Edge_Error_Flag(Bnd_Edge_Error_Flag, AFLR_free(0));
  std::unique_ptr<INT_2D[],AFLR_free> delBnd_Edge_Connectivity(Bnd_Edge_Connectivity, AFLR_free(0));
  std::unique_ptr<INT_3D[],AFLR_free> delTria_Connectivity(Tria_Connectivity, AFLR_free(0));
  std::unique_ptr<INT_4D[],AFLR_free> delQuad_Connectivity(Quad_Connectivity, AFLR_free(0));

  std::unique_ptr<DOUBLE_2D[],AFLR_free> delCoordinates(Coordinates, AFLR_free(0));
  std::unique_ptr<DOUBLE_1D[],AFLR_free> delInitial_Normal_Spacing(Initial_Normal_Spacing, AFLR_free(0));

  std::unique_ptr<INT_1D[],AFLR_free> delBG_Bnd_Edge_Grid_BC_Flag(BG_Bnd_Edge_Grid_BC_Flag, AFLR_free(0));
  std::unique_ptr<INT_1D[],AFLR_free> delBG_Bnd_Edge_ID_Flag(BG_Bnd_Edge_ID_Flag, AFLR_free(0));
  std::unique_ptr<INT_2D[],AFLR_free> delBG_Bnd_Edge_Connectivity(BG_Bnd_Edge_Connectivity, AFLR_free(0));

  // if BG_Tria_Neighbors is allocated by an external routine prior to calling
  // aflr2_grid_generator or aflr2_tria_grid2 then BG_Tria_Neighbors is NOT
  // free'd within aflr2 routines and the following should be uncommented
  //ug_free (BG_Tria_Neighbors);
  BG_Tria_Neighbors = NULL;

  std::unique_ptr<INT_3D[],AFLR_free> delBG_Tria_Connectivity(BG_Tria_Connectivity, AFLR_free(0));

  std::unique_ptr<DOUBLE_1D[],AFLR_free> delBG_Spacing(BG_Spacing, AFLR_free(0));
  std::unique_ptr<DOUBLE_2D[],AFLR_free> delBG_Coordinates(BG_Coordinates, AFLR_free(0));
  std::unique_ptr<DOUBLE_3D[],AFLR_free> delBG_Metric(BG_Metric, AFLR_free(0));

  std::unique_ptr<DOUBLE_1D[],AFLR_free> delSource_Spacing(Source_Spacing, AFLR_free(0));
  std::unique_ptr<DOUBLE_2D[],AFLR_free> delSource_Coordinates(Source_Coordinates, AFLR_free(0));
  std::unique_ptr<DOUBLE_3D[],AFLR_free> delSource_Metric(Source_Metric, AFLR_free(0));

  // TODO: Better error handling
  SANS_ASSERT( Error_Flag == 0 );

  INT_ ielem0 = 1;
  INT_ nelpntd = 0; // will be reset
  INT_ nelpnt = 0; // will be reset

  INT_1D *madd = NULL; // not needed
  INT_1D *ielin = NULL; // will be allocated

  INT_1D *lielin = (INT_1D*) ug_malloc (&Error_Flag, (Number_of_Nodes+2)*sizeof (INT_1D)); // the +2 is not a typo (no need to initialize or set)
  SANS_ASSERT( Error_Flag == 0 ); // TODO: Better error handling

  INT_3D *Tri_Neighbor = (INT_3D *) ug_malloc (&Error_Flag, 3*(Number_of_Trias+1) * sizeof (INT_3D));
  SANS_ASSERT( Error_Flag == 0 ); // TODO: Better error handling


  // Initial call before getting neigbor information
  Error_Flag = ug2_ielin(ielem0,  Number_of_Trias, &nelpntd, Number_of_Nodes, &nelpnt,
                         Tria_Connectivity, madd, &ielin, lielin);
  SANS_ASSERT( Error_Flag == 0 ); // TODO: Better error handling

  std::unique_ptr<INT_1D[],AFLR_free> delielin(ielin, AFLR_free(0));
  std::unique_ptr<INT_1D[],AFLR_free> dellielin(lielin, AFLR_free(0));
  std::unique_ptr<INT_3D[],AFLR_free> delTri_Neighbor(Tri_Neighbor, AFLR_free(0));

  // Get interior neighbor information
  Error_Flag = ug2_ieliel(Number_of_Trias, ielin, Tria_Connectivity, lielin, Tri_Neighbor);
  SANS_ASSERT( Error_Flag == 0 ); // TODO: Better error handling


  INT_1D * ibejbe = (INT_1D*) ug_malloc (&Error_Flag, (Number_of_Bnd_Edges+1)*sizeof (INT_1D));
  std::unique_ptr<INT_1D[],AFLR_free> delibejbe(ibejbe, AFLR_free(Error_Flag));
  INT_1D * ibejn = (INT_1D*) ug_malloc (&Error_Flag, (Number_of_Nodes+1)*sizeof (INT_1D));
  std::unique_ptr<INT_1D[],AFLR_free> delibejn(ibejn, AFLR_free(Error_Flag));
  INT_1D * jelibe = (INT_1D*) ug_malloc (&Error_Flag, (Number_of_Bnd_Edges+1)*sizeof (INT_1D));
  std::unique_ptr<INT_1D[],AFLR_free> deljelibe(jelibe, AFLR_free(Error_Flag));
  INT_1D * ielibe = (INT_1D*) ug_malloc (&Error_Flag, (Number_of_Bnd_Edges+1)*sizeof (INT_1D));
  std::unique_ptr<INT_1D[],AFLR_free> delielibe(ielibe, AFLR_free(Error_Flag));
  INT_1D * ierribe = (INT_1D*) ug_malloc (&Error_Flag, (Number_of_Bnd_Edges+1)*sizeof (INT_1D));
  std::unique_ptr<INT_1D[],AFLR_free> delierribe(ierribe, AFLR_free(Error_Flag));

  INT_ minl = 0;
  INT_ nbedgeu = 0;

  // Update neighbor information with BC markers
  Error_Flag = ug2_ielibe(minl, Message_Flag, Number_of_Bnd_Edges,
                          Number_of_Trias, Number_of_Nodes, &nbedgeu,
                          Tri_Neighbor, Bnd_Edge_Connectivity, Tria_Connectivity,
                          ibejbe, ibejn, jelibe, ielibe, ierribe);
  SANS_ASSERT( Error_Flag == 0 ); // TODO: Better error handling

  //Free the arguments
  for (int i = 0; i < prog_argc; i++)
    EG_free(prog_argv[i]);
  EG_free(prog_argv);


  // Count the number of lines in the mesh
  int nline = 0;

  for ( int iTriL = 0; iTriL < Number_of_Trias; iTriL++ )
  {
    for (int n = 0; n < Triangle::NTrace; n ++)
    {
      int iTriR = Tri_Neighbor[iTriL+1][n];

      // Only consider right tris with a higher index
      if (iTriR < iTriL+1 && iTriR > 0) continue;

      nline++;
    }
  }

  std::vector<LineTrace> lines(nline);
  nline = 0;

  for ( int iTriL = 0; iTriL < Number_of_Trias; iTriL++ )
  {
    for (int n = 0; n < Triangle::NTrace; n ++)
    {
      int iTriR = Tri_Neighbor[iTriL+1][n];

      // Only consider right test's with a higher index
      if (iTriR < iTriL+1 && iTriR > 0) continue;

      // Triangle nodes
      for (int k = 0; k < Line::NNode; k++ )
        lines[nline].nodes[k] = Tria_Connectivity[iTriL+1][TraceNodes[n][k]]-1;

      // Set neighbor information
      lines[nline].iTriL = iTriL;   // is already 0-based indexing
      lines[nline].iTriR = iTriR-1; // Remove 1-based indexing
      lines[nline].trace = n;
      lines[nline].ID = 0;

      // Mark all the BC lines
      if (iTriR < 0)
        lines[nline].ID = Bnd_Edge_ID_Flag[-iTriR];

      nline++;
    }
  }

  for ( int iTri = 0; iTri < Number_of_Trias; iTri++ )
    for (int n = 0; n < Triangle::NNode; n ++)
      Tria_Connectivity[iTri+1][n]--; // Change to 0-based indexing

  int nBCEdges = BG_xfld.nBoundaryTraceGroups();
  int nInteriorEdges = 0; // TODO: Add capability for interior geometric edges

  //Create the DOF arrays
  resizeDOF(Number_of_Nodes);

  // One Cell group for now...
  resizeCellGroups(1);
  resizeInteriorTraceGroups(nInteriorEdges+1); // +1 for the interior trace group
  resizeBoundaryTraceGroups(nBCEdges);

  //Copy over the nodal values
  for ( int i = 0; i < Number_of_Nodes; i++ )
    DOF(i) = {Coordinates[i+1][0], Coordinates[i+1][1]};

  // Count the number of triangles on each face
  std::vector< int > BCLineCount(nBCEdges, 0);
  std::vector< int > ILineCount(nInteriorEdges+1, 0);
  for ( int iline = 0; iline < nline; iline++ )
  {
    int ID = lines[iline].ID;
    if ( ID > 0 )
      BCLineCount[ID-1]++; // Boundary EGADS edges are positive numbers > 0
    else
      ILineCount[-ID]++; // Interior EGADS (and tet) edges are a negative number or 0
  }

  // volume field variable
  FieldCellGroupType<Triangle>::FieldAssociativityConstructorType
    fldAssocCell( BasisFunctionAreaBase<Triangle>::HierarchicalP1, Number_of_Trias );

  std::vector< FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType > fldAssocIline(nInteriorEdges+1);

  for ( int I = 0; I < nInteriorEdges+1; I++ ) // +1 for the volume interior trace group
  {
    fldAssocIline[I].resize( BasisFunctionLineBase::HierarchicalP1, ILineCount[I] );

    //Only one cell group for the interior trace group
    fldAssocIline[I].setGroupLeft ( 0 );
    fldAssocIline[I].setGroupRight( 0 );

    ILineCount[I] = 0;
  }

  //Create the boundary association constructors
  std::vector< FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType > fldAssocBline(nBCEdges);

  for ( int BC = 0; BC < nBCEdges; BC++ )
  {
    fldAssocBline[BC].resize(BasisFunctionLineBase::HierarchicalP1, BCLineCount[BC]);

    //Only one cell group for the interior trace group
    fldAssocBline[BC].setGroupLeft ( 0 );

    BCLineCount[BC] = 0;
  }

  //cell associativity
  for ( int iTri = 0; iTri < Number_of_Trias; iTri++ )
  {
    fldAssocCell.setAssociativity( iTri ).setRank( 0 );
    fldAssocCell.setAssociativity( iTri ).setNodeGlobalMapping( Tria_Connectivity[iTri+1], Triangle::NNode );
  }

  // trace associativity
  for ( int iline = 0; iline < nline; iline++ )
  {
    int iTriL = lines[iline].iTriL;
    int iTriR = lines[iline].iTriR;

    // Line nodes
    std::array<int,Line::NNode> lineL = {{lines[iline].nodes[0], lines[iline].nodes[1]}};

#if 0
    // Left triangle nodes
    std::vector<int> triL = {Tria_Connectivity[iTriL+1][0], Tria_Connectivity[iTriL+1][1],
                             Tria_Connectivity[iTriL+1][2]};
#endif

    // The left canonical trace was used to construct the line
    CanonicalTraceToCell canonicalL(lines[iline].trace, 1);

    int marker = lines[iline].ID;

    if ( iTriR >= 0 && marker <= 0) //Interior line, or interior edge
    {
      const int I = -marker;
      const int lineCount = ILineCount[I];

      std::vector<int> triR = {Tria_Connectivity[iTriR+1][0], Tria_Connectivity[iTriR+1][1],
                               Tria_Connectivity[iTriR+1][2]};

      fldAssocIline[I].setElementLeft ( iTriL, lineCount );
      fldAssocIline[I].setElementRight( iTriR, lineCount );

      CanonicalTraceToCell canonicalR = TraceToCellRefCoord<Line, TopoD2, Triangle>::getCanonicalTrace(&lineL[0], Line::NNode,
                                                                                                       triR.data(), triR.size());

      // Face signs for elements (L is +, R is -)
      fldAssocCell.setAssociativity( iTriL ).setEdgeSign( +1, canonicalL.trace );
      fldAssocCell.setAssociativity( iTriR ).setEdgeSign( canonicalR.orientation, canonicalR.trace );

      // Set the canonical faces
      fldAssocIline[I].setCanonicalTraceLeft( canonicalL, lineCount );
      fldAssocIline[I].setCanonicalTraceRight( canonicalR, lineCount );

      fldAssocIline[I].setAssociativity( lineCount ).setRank( 0 );

      //Set the nodes for the triangle face
      fldAssocIline[I].setAssociativity( lineCount ).setNodeGlobalMapping( &lineL[0], Line::NNode );

      ILineCount[I]++;
    }
    else if ( iTriR >= 0 && marker > 0) //Interior boundary nodes
    {
      SANS_DEVELOPER_EXCEPTION("Need to implement interior edges for AFLR2");
#if 0
      const int BC = marker-1;
      const int faceCount = BCLineCount[BC];

      std::vector<int> triR = {Tria_Connectivity[iTriR+1][0], Tria_Connectivity[iTriR+1][1],
                               Tria_Connectivity[iTriR+1][2]};

      fldAssocBline[BC].setElementLeft ( iTriL, faceCount );
      fldAssocBline[BC].setElementRight( iTriR, faceCount );

      // The left canonical trace was used to construct the triangle
      CanonicalTraceToCell canonicalL(lines[iline].trace, 1);

      // Get the left and right canonical faces
      CanonicalTraceToCell canonicalR = TraceToCellRefCoord<Line, TopoD2, Triangle>::getCanonicalTrace(&lineL[0], Line::NNode,
                                                                                                       triR.data(), triR.size());

      // Get the canonical triangle with the updated tetrahedral nodes
      for ( int n = 0; n < Line::NNode; n++)
        SANS_ASSERT(triL[n] == tetL[TraceNodes[canonicalL.trace][n]]);

      // Face signs for elements (L is +, R is -)
      fldAssocCell.setAssociativity( iTriL ).setFaceSign( +1, canonicalL.trace );
      fldAssocCell.setAssociativity( iTriR ).setFaceSign( canonicalR.orientation, canonicalR.trace );

      // Set the canonical faces
      fldAssocBline[BC].setCanonicalTraceLeft( canonicalL, faceCount );
      fldAssocBline[BC].setCanonicalTraceRight( canonicalR, faceCount );

      //Set the nodes for the triangle face
      fldAssocBline[BC].setAssociativity( faceCount ).setNodeGlobalMapping( &triL[0], 3 );

      BCLineCount[BC]++;
#endif
    }
    else  //Now we are on a boundary.
    {
      const int BC = marker-1;
      const int lineCount = BCLineCount[BC];

      fldAssocBline[BC].setElementLeft ( iTriL, lineCount );

      // Face signs for elements (L is +, R is -)
      fldAssocCell.setAssociativity( iTriL ).setEdgeSign( +1, canonicalL.trace );

      // Set the canonical faces
      fldAssocBline[BC].setCanonicalTraceLeft( canonicalL, lineCount );

      fldAssocBline[BC].setAssociativity( lineCount ).setRank( 0 );

      //Set the nodes for the triangle face
      fldAssocBline[BC].setAssociativity( lineCount ).setNodeGlobalMapping( &lineL[0], Line::NNode );

      BCLineCount[BC]++;
    }

  }

  // Create cell group
  cellGroups_[0] = new FieldCellGroupType<Triangle>( fldAssocCell );
  cellGroups_[0]->setDOF(DOF_, nDOF_);
  nElem_ = fldAssocCell.nElem();

  // Create the interior group
  for ( int I = 0; I < nInteriorEdges+1; I++ )
  {
    interiorTraceGroups_[I] = new FieldTraceGroupType<Line>( fldAssocIline[I] );
    interiorTraceGroups_[I]->setDOF(DOF_, nDOF_);
  }

  // Create the boundary groups
  for ( int BC = 0; BC < nBCEdges; BC++ )
  {
    boundaryTraceGroups_[BC] = new FieldTraceGroupType<Line>( fldAssocBline[BC] );
    boundaryTraceGroups_[BC]->setDOF(DOF_, nDOF_);
  }

  // Check that the grid is correct
  checkGrid();
}


}
