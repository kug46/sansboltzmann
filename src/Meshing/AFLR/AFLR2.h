// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SANS_AFLR2_H
#define SANS_AFLR2_H

//Generates a volume tetrahedron grid given a tessellated EGADS model using AFLR2

#include "Field/XFieldArea.h"
#include "Field/FieldArea_CG_Cell.h"

namespace SANS
{

class AFLR2 : public XField<PhysD2, TopoD2>
{
public:

  static const int Dim = PhysD2::D;
  typedef XField<PhysD2, TopoD2> BaseType;
  typedef BaseType::VectorX CartCoord;

  typedef DLA::MatrixSymS<Dim,Real> MatrixSym;

  AFLR2( const AFLR2& ) = delete;
  AFLR2& operator=( const AFLR2& ) = delete;
  ~AFLR2();

  // Constuctor based on a metric field
  explicit AFLR2( const Field_CG_Cell<PhysD2,TopoD2,MatrixSym>& metric );

  int nVertex() { return nDOF_; }
  CartCoord& X(const int n) { return DOF_[n]; }

};

}

#endif //SANS_AFLR2_H
