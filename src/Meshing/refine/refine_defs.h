// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SANS_REFINE_DEFS_H
#define SANS_REFINE_DEFS_H

#ifdef SANS_REFINE

#include <ref_defs.h>

#include "tools/SANSException.h"

// SANSify the refine macros
#ifdef RSS
#undef RSS
#endif

#define RSS(fcn,msg)              \
  {                 \
    REF_STATUS ref_private_macro_code;          \
    ref_private_macro_code = (fcn);         \
    if (REF_SUCCESS != ref_private_macro_code){       \
      SANS_DEVELOPER_EXCEPTION("%s: %d: %s: %d %s\n", __FILE__,__LINE__,__func__, \
                              ref_private_macro_code,(msg));  \
    }                 \
  }

#ifdef RAS
#undef RAS
#endif

#define RAS(a, msg)       \
  {                       \
    if (!(a)) {           \
      REF_WHERE(msg);     \
      SANS_DEVELOPER_EXCEPTION("%s: %d: %s: %s\n", __FILE__,__LINE__,__func__, \
                               (msg));  \
    }                     \
  }

#ifdef RNS
#undef RNS
#endif

#define RNS(ptr, msg)            \
  {                              \
    if (NULL == (void *)(ptr)) { \
      SANS_DEVELOPER_EXCEPTION("%s: %d: %s: %s\n", __FILE__,__LINE__,__func__, \
                               (msg));  \
    }                            \
  }

#endif // SANS_REFINE

#endif // SANS_REFINE_DEFS_H
