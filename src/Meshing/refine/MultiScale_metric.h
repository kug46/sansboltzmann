// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "tools/SANSnumerics.h" // Real

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Type.h"
#include "Field/FieldTypes.h"

namespace SANS
{

enum refine_recon_reconstructions
{ /* 0 */ refine_recon_L2projection,
  /* 1 */ refine_recon_Kexact,
  /* 2 */ refine_recon_last
};

//
// Computes a multi-scale metric field using refine
//
template<class PhysDim, class TopoDim>
void MultiScale_metric(
    refine_recon_reconstructions recon,
    int p,
    Real gradation,
    Real complexity,
    const Field_CG_Cell<PhysDim,TopoDim,Real>& sfld,
    Field_CG_Cell<PhysDim, TopoDim, DLA::MatrixSymS<PhysDim::D, Real>>& metric_req );


} // namespace SANS
