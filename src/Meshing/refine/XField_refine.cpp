// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "Meshing/refine/XField_refine.h"

#include "LinearAlgebra/DenseLinAlg/tools/cross.h"

#include "Field/XFieldArea.h"
#include "Field/XFieldVolume.h"

#include "Field/Partition/XField_Lagrange.h"

#include "Field/output_Tecplot.h"
#include "Meshing/libMeshb/WriteMesh_libMeshb.h"

#include <memory> //shared_ptr
#include <numeric> // std::accumulate
#include <vector>

// refine includes
#include <ref_grid.h>
#include <ref_malloc.h>
#include <ref_face.h>
#include <ref_validation.h>
#include <ref_export.h>
#include <ref_egads.h>
#include <ref_metric.h>
#include <ref_histogram.h>
#include <ref_split.h>
#include <ref_part.h>
#include <ref_import.h>

// SANSified refine macros
#include "refine_defs.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_gather.hpp>
#include <boost/mpi/collectives/broadcast.hpp>
#endif

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
XField_refine<PhysDim,TopoDim>::~XField_refine()
{
}

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
XField_refine<PhysDim,TopoDim>::XField_refine( mpi::communicator comm,
                                               const std::string& EGADSfilename )
  : BaseType(comm), EGADSfilename_(EGADSfilename), flipTri_(false)
{
  MPI_Comm mpi_comm = (MPI_Comm)(comm);
  REF_MPI_PTR ref_mpi((void*)&mpi_comm);

  REF_GRID_PTR ref_grid(ref_mpi);

  RSS(ref_egads_load(ref_grid_geom(ref_grid), EGADSfilename.c_str()), "ld egads");

  REF_INT auto_tparams = REF_EGADS_ALL_TPARAM;
  REF_DBL *global_params = NULL;
  RSS(ref_egads_tess(ref_grid, auto_tparams, global_params), "tess egads");
  RSS(ref_geom_verify_topo(ref_grid), "adapt topo");
  RSS(ref_geom_verify_param(ref_grid), "egads params");


  REF_INT passes = 15;
  RSS(ref_adapt_surf_to_geom(ref_grid, passes), "ad");

  RSS(ref_geom_report_tri_area_normdev(ref_grid), "tri status");
  RSS(ref_geom_verify_topo(ref_grid), "adapt topo");
  RSS(ref_geom_verify_param(ref_grid), "adapt params");

  ref_grid_partitioner(ref_grid) = REF_MIGRATE_SINGLE;
  RSS(ref_migrate_to_balance(ref_grid), "migrate to single part");
  RSS(ref_grid_pack(ref_grid), "pack");

  if (ref_geom_manifold(ref_grid_geom(ref_grid)))
  {
    char mesher[] = "t";
    if (strncmp(mesher, "t", 1) == 0)
    {
      if (ref_mpi_once(ref_mpi))
      {
        printf("fill volume with TetGen\n");
        RSS(ref_geom_tetgen_volume(ref_grid), "tetgen surface to volume");
      }
      ref_mpi_stopwatch_stop(ref_mpi, "tetgen volume");
    }
    else if (strncmp(mesher, "a", 1) == 0)
    {
      if (ref_mpi_once(ref_mpi))
      {
        printf("fill volume with AFLR3\n");
        RSS(ref_geom_aflr_volume(ref_grid), "aflr surface to volume");
      }
      ref_mpi_stopwatch_stop(ref_mpi, "aflr volume");
    }
    else
    {
      printf("mesher '%s' not implemented\n", mesher);
    }
    ref_grid_surf(ref_grid) = REF_FALSE; /* needed until vol mesher para */

    RSS(ref_split_edge_geometry(ref_grid), "split geom");
    ref_mpi_stopwatch_stop(ref_grid_mpi(ref_grid), "split geom");
  }
  else
  {
    ref_grid_twod(ref_grid) = REF_TRUE; /* assume flat facebody */
    ref_grid_surf(ref_grid) = REF_TRUE;
  }
  RSS(ref_node_synchronize_globals(ref_grid_node(ref_grid)), "sync glob");
  RSS(ref_validation_cell_volume(ref_grid), "vol");
  RSS(ref_metric_interpolated_curvature(ref_grid), "interp curve");
  RSS(ref_histogram_quality(ref_grid), "gram");
  RSS(ref_histogram_ratio(ref_grid), "gram");

  std::string::size_type idx = EGADSfilename.rfind('.'); // find the '.' in reverse order
  MeshCADfilename_ = EGADSfilename.substr(0, idx) + ".meshb";

  RSS( ref_gather_by_extension( ref_grid, MeshCADfilename_.c_str() ), MeshCADfilename_.c_str());

  build(ref_grid);
}

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
XField_refine<PhysDim,TopoDim>::XField_refine( mpi::communicator comm,
                                               const std::string& EGADSfilename,
                                               const std::string& MeshCADfilename)
  : BaseType(comm), EGADSfilename_(EGADSfilename), MeshCADfilename_(MeshCADfilename), flipTri_(false)
{
  MPI_Comm mpi_comm = (MPI_Comm)(comm);
  REF_MPI_PTR ref_mpi((void*)&mpi_comm);

  REF_GRID ref_grid = NULL;

  if (ref_mpi_para(ref_mpi))
  {
    RSS(ref_part_by_extension(&ref_grid, ref_mpi, MeshCADfilename.c_str()), "part");
  }
  else
  {
    RSS(ref_import_by_extension(&ref_grid, ref_mpi, MeshCADfilename.c_str()), "import");
  }

  // Check consistency in the CAD data
  RSS( ref_part_cad_data( ref_grid, MeshCADfilename.c_str() ), MeshCADfilename.c_str() );

  RSS( ref_part_cad_association( ref_grid, MeshCADfilename.c_str() ), MeshCADfilename.c_str() );

  // Do some EGADS initialization. TODO: Mike wants to remove this eventually
  RSS( ref_egads_load( ref_grid_geom(ref_grid), EGADSfilename.c_str() ), EGADSfilename.c_str() );

  // This must happen after ref_part_cad_association and ref_geom_egads_load
  RSS( ref_egads_mark_jump_degen(ref_grid), "T and UV jumps; UV degen" );
  RSS( ref_geom_verify_topo(ref_grid), "geom topo");
  RSS( ref_geom_verify_param(ref_grid), "geom param");

  if (!ref_geom_manifold(ref_grid_geom(ref_grid)))
  {
    ref_grid_twod(ref_grid) = REF_TRUE; /* assume flat facebody */
    ref_grid_surf(ref_grid) = REF_TRUE;
  }

  REF_GRID_PTR ref_grid_ptr(ref_mpi, ref_grid);

  build(ref_grid_ptr);
}

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
XField_refine<PhysDim,TopoDim>::XField_refine( std::shared_ptr<mpi::communicator> comm,
                                               REF_GRID_PTR& ref_grid,
                                               const std::string& EGADSfilename,
                                               const std::string& MeshCADfilename)
  : BaseType(comm), EGADSfilename_(EGADSfilename), MeshCADfilename_(MeshCADfilename), flipTri_(false)
{
  build(ref_grid);
}

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
void
XField_refine<PhysDim,TopoDim>::build( REF_GRID_PTR& ref_grid )
{
  typedef typename Simplex<TopoDim>::type Topology;
  typedef typename Topology::TopologyTrace TopologyTrace;

  typedef typename XField_Lagrange<PhysDim>::VectorX VectorX;

  if (PhysDim::D == 2 && ref_grid_twod(ref_grid) != REF_TRUE)
    SANS_DEVELOPER_EXCEPTION("Trying to import 3D ref_grid into 2D XField!");

  REF_NODE ref_node = ref_grid_node(ref_grid);

  XField_Lagrange<PhysDim> xfldin(*this->comm());
  int comm_rank = this->comm()->rank();

  // collapse sparse arrays into dense continuous arrays
  // this might renumber the nodes if the grid is already packed
  RSS( ref_node_synchronize_globals(ref_node), "sync" );
  RSS( ref_grid_pack(ref_grid), "pack" );

  // get the group with cells
  REF_CELL ref_cell_cell;
  if (typeid(Topology) == typeid(Tet))
    ref_cell_cell = ref_grid_tet(ref_grid);
  else if (typeid(Topology) == typeid(Triangle))
    ref_cell_cell = ref_grid_tri(ref_grid);
  else
    SANS_DEVELOPER_EXCEPTION("Unknown topology!");

  REF_CELL ref_cell_trace;
  if (typeid(TopologyTrace) == typeid(Triangle))
    ref_cell_trace = ref_grid_tri(ref_grid);
  else if (typeid(TopologyTrace) == typeid(Line))
    ref_cell_trace = ref_grid_edg(ref_grid);
  else
    SANS_DEVELOPER_EXCEPTION("Unknown trace topology!");

  REF_INT ncell = ref_cell_n(ref_cell_cell);

  // count the total nodes possessed by this processor
  int nDOF_local = 0;
  REF_INT local;
  each_ref_node_valid_node(ref_node, local)
  {
    if (ref_node_owned(ref_node,local))
      nDOF_local++;
  }

  //Create the DOF arrays
  xfldin.sizeDOF_parallel(nDOF_local);

  //Copy over the nodal values
  each_ref_node_valid_node(ref_node, local)
  {
    if (!ref_node_owned(ref_node,local)) continue;

    VectorX X;

    for (int d = 0; d < PhysDim::D; d++)
      X[d] = ref_node_xyz(ref_node,d,local);

    // Extract the native numbering
    int iDOF_native = ref_node_global(ref_node,local);

    xfldin.addDOF_parallel(iDOF_native, X);
  }

  // count the number of possessed cells
  int nCell_local = 0;
  for (REF_INT cell = 0; cell < ncell; cell++)
  {
    // get the 'rank' that possesses the cell
    REF_INT part = 0;
    RSS( ref_cell_part( ref_cell_cell, ref_node, cell, &part ), "get part" );

    if (part == comm_rank)
      nCell_local++;
  }

  int order = 1; // refine only supports Q=1
  int igroup = 0; // refine does not really have groups...

  REF_INT nodes[REF_CELL_MAX_SIZE_PER];
  std::vector<int> elemMap(Topology::NNode);

#ifdef SANS_MPI
  std::vector<int> nCellOnRank;
  boost::mpi::all_gather(*this->comm(), nCell_local, nCellOnRank);

  // compute the node offset from lower ranks
  int iCell_native = std::accumulate(nCellOnRank.begin(), nCellOnRank.begin()+comm_rank, 0);
#else
  int iCell_native = 0;
#endif

  //Create the cell arrays
  xfldin.sizeCells_parallel(nCell_local);

  flipTri_ = false;
  if (PhysDim::D == 2)
  {
    // make sure we use a rank with at least one cell
    int check_rank = 0;
#ifdef SANS_MPI
    while (check_rank < (int)nCellOnRank.size() && nCellOnRank[check_rank] == 0) check_rank++;
    SANS_ASSERT(check_rank < (int)nCellOnRank.size());
#endif
    if (comm_rank == check_rank)
    {
      // Look for the first cell on first processor
      for (REF_INT cell = 0; cell < ncell; cell++)
      {
        // get the 'rank' that possesses the cell
        REF_INT part = 0;
        RSS( ref_cell_part( ref_cell_cell, ref_node, cell, &part ), "get part" );

        if (part != comm_rank) continue;

        // get the local nodes
        RSS( ref_cell_nodes( ref_cell_cell, cell, nodes ), "cell nodes" );

        VectorX X[3], dX1, dX2;

        // extract the coordinates
        for (int i = 0; i < Triangle::NNode; i++)
        {
          elemMap[i] = nodes[i];
          for (int d = 0; d < PhysDim::D; d++)
            X[i][d] = ref_node_xyz(ref_node,d,elemMap[i]);
        }

        dX1 = X[1] - X[0];
        dX2 = X[2] - X[0];

        // cross will return a scalar in 2D and vector in 3D
        // only 2D is used, but the vector is needed to compile
        VectorX N = cross(dX1,dX2);

        if (N[0] < 0) flipTri_ = true;
#ifdef SANS_MPI
        boost::mpi::broadcast(*this->comm(), flipTri_, check_rank);
#endif
        break;
      }
    }
    else
    {
#ifdef SANS_MPI
      boost::mpi::broadcast(*this->comm(), flipTri_, check_rank);
#endif
    }
  }

  // add cell elements
  for (REF_INT cell = 0; cell < ncell; cell++)
  {
    // get the 'rank' that possesses the cell
    REF_INT part = 0;
    RSS( ref_cell_part( ref_cell_cell, ref_node, cell, &part ), "get part" );

    if (part != comm_rank) continue;

    // get the local nodes
    RSS( ref_cell_nodes( ref_cell_cell, cell, nodes ), "cell nodes" );

    // convert to native numbering
    for (int i = 0; i < Topology::NNode; i++)
      elemMap[i] = ref_node_global(ref_node,nodes[i]);

    // flip triangles to make them right handed
    // REMOVE THIS WHEN refine IS FIXED!!!
    if (flipTri_) std::swap(elemMap[0], elemMap[1]);

    // add the cell
    xfldin.addCell_parallel(iCell_native, igroup, Topology::Topology, order, elemMap);
    iCell_native++;
  }


  // Count the number of boundary traces in the mesh
  REF_INT ntrace = ref_cell_n(ref_cell_trace);

  // count the number of possessed traces
  int nBnd_local = 0;
  for (REF_INT itrace = 0; itrace < ntrace; itrace++)
  {
    // get the 'rank' that possesses the trace
    REF_INT part = 0;
    RSS( ref_cell_part( ref_cell_trace, ref_node, itrace, &part ), "get part" );

    if (part == comm_rank)
      nBnd_local++;
  }

#ifdef SANS_MPI
  std::vector<int> nBndOnRank;
  boost::mpi::all_gather(*this->comm(), nBnd_local, nBndOnRank);

  // compute the node offset from lower ranks
  int iBnd_native = std::accumulate(nBndOnRank.begin(), nBndOnRank.begin()+comm_rank, 0);
#else
  int iBnd_native = 0;
#endif

  elemMap.resize(TopologyTrace::NNode);

  // size the number of boundary trace elements
  xfldin.sizeBoundaryTrace_parallel(nBnd_local);

  // add cell elements
  for (REF_INT itri = 0; itri < ntrace; itri++)
  {
    // get the 'rank' that possesses the cell
    REF_INT part = 0;
    RSS( ref_cell_part( ref_cell_trace, ref_node, itri, &part ), "get part" );

    if (part != comm_rank) continue;

    // get the local nodes
    RSS( ref_cell_nodes( ref_cell_trace, itri, nodes ), "tri nodes" );

    // convert to native numbering
    for (int i = 0; i < TopologyTrace::NNode; i++)
      elemMap[i] = ref_node_global(ref_node, nodes[i]);

    int face = nodes[TopologyTrace::NNode]-1; // extract the 1 based BC index

    // add the boundary trace element
    xfldin.addBoundaryTrace_parallel(iBnd_native, face, TopologyTrace::Topology, elemMap);
    iBnd_native++;
  }

  try
  {
    // construct the grid
    this->buildFrom(xfldin);
  }
  catch (const XFieldException&)
  {
    RSS( ref_gather_by_extension(ref_grid, "refine_ref_grid_debug.meshb"), "refine_ref_grid_debug.meshb" );
    RSS( ref_export_tec(ref_grid, "refine_ref_grid_debug.tec"), "refine_ref_grid_debug.tec" );

    WriteMesh_libMeshb( *this , "refine_SANS_debug.meshb" );
    output_Tecplot( *this , "refine_SANS_debug_rank" + std::to_string(comm_rank) + ".dat" );
    throw;
  }
}

// instantiate the class
template class XField_refine<PhysD2, TopoD2>;
template class XField_refine<PhysD3, TopoD3>;

}
