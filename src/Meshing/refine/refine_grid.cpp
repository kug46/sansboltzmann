// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "refine_ptr.h"

#include "BasisFunction/BasisFunctionVolume_Tetrahedron.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/XFieldArea.h"

#include "Field/FieldVolume_CG_Cell.h"
#include "Field/XFieldVolume.h"

#include "XField_refine.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#endif

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

template<class PhysDim, class TopoDim>
REF_GRID_PTR REF_GRID_PTR::XField_to_ref_grid(const XField<PhysDim,TopoDim>& xfld)
{
  typedef typename Simplex<TopoDim>::type Topology;
  typedef typename Topology::TopologyTrace TopologyTrace;

  typedef XField<PhysDim, TopoDim> XFieldType;
  typedef typename XFieldType::template FieldCellGroupType<Topology> XFieldCellType;
  typedef typename XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceType;

  REF_INT comm_rank = xfld.comm()->rank();

  // This constructor assumes linear slimplicial grids
  for (int i = 0; i < xfld.nCellGroups(); i++)
  {
    SANS_ASSERT( xfld.getCellGroupBase(i).topoTypeID() == typeid(Topology) );
    SANS_ASSERT( xfld.getCellGroupBase(i).order() == 1 );
  }

  std::string MeshCADfilename;
  bool flipTri = false;
  if (xfld.derivedTypeID() == typeid(XField_refine<PhysDim,TopoDim>))
  {
    MeshCADfilename = static_cast<const XField_refine<PhysDim,TopoDim>&>(xfld).getMeshCADfilename();
    flipTri = static_cast<const XField_refine<PhysDim,TopoDim>&>(xfld).flipTri();
  }

  // xfld does not have possessiveness, so construct a field to get that info
  const Field_CG_Cell<PhysDim,TopoDim,Real> sfld(xfld, 1, BasisFunctionCategory_Lagrange);

  MPI_Comm mpi_comm = (MPI_Comm)(*xfld.comm());
  REF_MPI_PTR ref_mpi((void*)&mpi_comm);

  REF_GRID_PTR ref_grid(ref_mpi);
  REF_NODE ref_node = ref_grid_node(ref_grid);

  if (PhysDim::D == 2)
  {
    ref_grid_twod(ref_grid) = REF_TRUE;
    if (!MeshCADfilename.empty())
      ref_grid_surf(ref_grid) = REF_TRUE;
  }

  // Total number of nodes
  REF_INT nnodes = xfld.nDOF();

  // Copy over the coordinates into refine
  // must use xfld.local2nativeDOFmap here
  for ( REF_INT i = 0; i < nnodes; i++ )
  {
    REF_INT pos;
    RSS( ref_node_add( ref_node, xfld.local2nativeDOFmap(i), &pos ), "add node");

    for ( int d = 0; d < PhysDim::D; d++ )
      ref_node_xyz(ref_node,d,pos) = xfld.DOF(i)[d];

    if (PhysDim::D == 2)
      ref_node_xyz(ref_node,2,pos) = 0.0;
  }

  // Fill in DOF possession. All nodes up to nDOFpossessed are possessed by the current processor
  for ( int i = 0; i < sfld.nDOFpossessed(); i++ )
  {
    REF_INT pos;
    RSS( ref_node_local( ref_node, sfld.local2nativeDOFmap(i), &pos ), "local node");
    ref_node_part(ref_node,pos) = comm_rank;
  }

  // Copy over the rank for ghost/zombie DOFs
  for ( int i = sfld.nDOFpossessed(); i < sfld.nDOF(); i++ )
  {
    REF_INT pos;
    RSS( ref_node_local( ref_node, sfld.local2nativeDOFmap(i), &pos ), "local node");
    ref_node_part(ref_node,pos) = sfld.DOFghost_rank(i - sfld.nDOFpossessed());
  }

  // Compute the total number of nodes in the grid
  REF_INT nnodeGlobal = sfld.nDOFpossessed();
#ifdef SANS_MPI
  nnodeGlobal = boost::mpi::all_reduce(*xfld.comm(), nnodeGlobal, std::plus<int>());
#endif

  // initialize the nodes
  RSS( ref_node_initialize_n_global( ref_node, nnodeGlobal ), "init global");

  // get the group with cells
  REF_CELL ref_cell_cell;
  if (typeid(Topology) == typeid(Tet))
    ref_cell_cell = ref_grid_tet(ref_grid);
  else if (typeid(Topology) == typeid(Triangle))
    ref_cell_cell = ref_grid_tri(ref_grid);
  else
    SANS_DEVELOPER_EXCEPTION("Unknown topology!");

  for (int group = 0; group < xfld.nCellGroups(); group++)
  {
    const XFieldCellType& xfldCell = xfld.template getCellGroup<Topology>(group);
    const int nElem = xfldCell.nElem();
    int nodeMapGlobal[Topology::NNode];
    int nodeMapPos[REF_CELL_MAX_SIZE_PER];
    nodeMapPos[Topology::NNode] = group+1;
    for (int elem = 0; elem < nElem; elem++)
    {
      // Set the connectivity using local DOF numbering
      xfldCell.associativity( elem ).getNodeGlobalMapping( nodeMapGlobal, Topology::NNode );

      if (flipTri) std::swap(nodeMapGlobal[0], nodeMapGlobal[1]);

      for (int n = 0; n < Topology::NNode; n++)
      {
        // Convert the cell indexing to native numbering
        nodeMapGlobal[n] = xfld.local2nativeDOFmap(nodeMapGlobal[n]);

        // Convert to refine local numbering
        RSS( ref_node_local(ref_node, nodeMapGlobal[n], &nodeMapPos[n]), "demap" );
      }

      // new cell location is not needed here
      REF_INT new_cell;
      RSS( ref_cell_add( ref_cell_cell, nodeMapPos, &new_cell ), "add cell");
    }
  }

  // Get the number of boundary trace groups
  REF_INT nface_groups = xfld.nBoundaryTraceGroups();

  // get the group of trace elements
  REF_CELL ref_cell_trace;
  if (typeid(TopologyTrace) == typeid(Triangle))
    ref_cell_trace = ref_grid_tri(ref_grid);
  else if (typeid(TopologyTrace) == typeid(Line))
    ref_cell_trace = ref_grid_edg(ref_grid);
  else
    SANS_DEVELOPER_EXCEPTION("Unknown trace topology!");

  for (REF_INT group = 0; group < nface_groups; group++)
  {
    REF_INT new_face;
    int nodeMapGlobal[Triangle::NNode];
    int nodeMapPos[REF_CELL_MAX_SIZE_PER];
    REF_INT face_group_size = xfld.getBoundaryTraceGroupBase(group).nElem();

    const XFieldTraceType& xfldBCTrace = xfld.template getBoundaryTraceGroup<TopologyTrace>(group);

    // set the edge connectivity
    for ( REF_INT i = 0; i < face_group_size; i++ )
    {
      xfldBCTrace.associativity( i ).getNodeGlobalMapping( nodeMapGlobal, TopologyTrace::NNode );

      for (int n = 0; n < TopologyTrace::NNode; n++)
      {
        // Convert the cell indexing to native numbering
        nodeMapGlobal[n] = xfld.local2nativeDOFmap(nodeMapGlobal[n]);

        // Convert to refine numbering
        RSS( ref_node_local(ref_node, nodeMapGlobal[n], &nodeMapPos[n]), "demap" );
      }

      // One based indexing assuming the group matches Face indexing with EGADS
      nodeMapPos[TopologyTrace::NNode] = group+1;
      RSS( ref_cell_add( ref_cell_trace, nodeMapPos, &new_face ), "add face");
    }
  }

  //===========================================
  // TODO: Add 'edge'/'bar' elements to give to refine
  //===========================================

  // return the grid
  return ref_grid;
}

template REF_GRID_PTR REF_GRID_PTR::XField_to_ref_grid<PhysD2,TopoD2>(const XField<PhysD2,TopoD2>& xfld);
template REF_GRID_PTR REF_GRID_PTR::XField_to_ref_grid<PhysD3,TopoD3>(const XField<PhysD3,TopoD3>& xfld);


}
