// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "MultiScale_metric.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/XFieldArea.h"

#include "Field/FieldVolume_CG_Cell.h"
#include "Field/XFieldVolume.h"

// refine includes
#include <ref_malloc.h>
#include <ref_grid.h>
#include <ref_metric.h>

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#endif

// SANSified refine macros
#include "refine_defs.h"
#include "refine_ptr.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

//
// Computes a multi-scale metric field using refine
//
template<class PhysDim, class TopoDim>
void MultiScale_metric(
    refine_recon_reconstructions recon,
    int p,
    Real gradation,
    Real complexity,
    const Field_CG_Cell<PhysDim,TopoDim,Real>& sfld,
    Field_CG_Cell<PhysDim, TopoDim, DLA::MatrixSymS<PhysDim::D, Real>>& metric_request)
{
  typedef XField<PhysDim,TopoDim> XFieldType;
  typedef DLA::MatrixSymS<PhysDim::D, Real> MatrixSym;

  const XFieldType& BG_xfld = sfld.getXField();
  SANS_ASSERT(&sfld.getXField() == &metric_request.getXField());

  // convert the xfld to ref_grid
  REF_GRID_PTR ref_grid(BG_xfld);

  // This constructor assumes linear fields
  for (int i = 0; i < BG_xfld.nCellGroups(); i++)
  {
    SANS_ASSERT( sfld.getCellGroupBase(i).order() == 1 );
    SANS_ASSERT( metric_request.getCellGroupBase(i).order() == 1 );
  }

  // Total number of nodes
  REF_INT nnodes = BG_xfld.nDOF();
  REF_NODE ref_node = ref_grid_node(ref_grid);

  // Copy over the scalar values
  // must use sfld.local2nativeDOFmap here
  std::vector<REF_DBL> scalar(sfld.nDOF());
  for ( REF_INT i = 0; i < nnodes; i++ )
  {
    REF_INT pos;
    RSS( ref_node_local( ref_node, sfld.local2nativeDOFmap(i), &pos ), "local node");

    scalar[pos] = sfld.DOF(i);
  }

  REF_DBL* weight = NULL;

  // only need if you did not set ghost nodes
  ref_node_ghost_dbl(ref_node, scalar.data(), 1);

  REF_DBL *metric = NULL;
  ref_malloc(metric, 6 * ref_node_max(ref_grid_node(ref_grid)), REF_DBL);

  REF_RECON_RECONSTRUCTION reconstruction;
  switch ( recon )
  {
  case refine_recon_L2projection:
    reconstruction = REF_RECON_L2PROJECTION;
    break;
  case refine_recon_Kexact:
    reconstruction = REF_RECON_KEXACT;
    break;
  default:
    SANS_DEVELOPER_EXCEPTION("Unknown reconstruction");
  }

  RSS(ref_metric_lp(metric, ref_grid, scalar.data(), weight,
                    reconstruction, p, gradation, complexity), "lp norm");
  RSS(ref_metric_to_node(metric, ref_grid_node(ref_grid)), "set node");
  ref_free(metric); metric = NULL;
  REF_DBL ref_m[6];

  // Copy over the metric values
  // must use metric_request.local2nativeDOFmap here
  for ( REF_INT i = 0; i < nnodes; i++ )
  {
    REF_INT pos;
    RSS( ref_node_local( ref_node, metric_request.local2nativeDOFmap(i), &pos ), "local node");

    MatrixSym m;
    ref_node_metric_get(ref_node, pos, ref_m);

    // refine expects an upper triangular matrix
    if (PhysDim::D == 2)
    {
      m(0,0) = ref_m[0]; m(0,1) = ref_m[1];
                         m(1,1) = ref_m[3];
    }
    else
    {
      m(0,0) = ref_m[0]; m(0,1) = ref_m[1]; m(0,2) = ref_m[2];
                         m(1,1) = ref_m[3]; m(1,2) = ref_m[4];
                                            m(2,2) = ref_m[5];
    }

    metric_request.DOF(i) = m;
  }

  // just to make sure everything is consistent
  metric_request.syncDOFs_MPI_noCache();
}

template
void MultiScale_metric(
    refine_recon_reconstructions recon,
    int p,
    Real gradation,
    Real complexity,
    const Field_CG_Cell<PhysD2,TopoD2,Real>& sfld,
    Field_CG_Cell<PhysD2, TopoD2, DLA::MatrixSymS<PhysD2::D, Real>>& metric_request);

template
void MultiScale_metric(
    refine_recon_reconstructions recon,
    int p,
    Real gradation,
    Real complexity,
    const Field_CG_Cell<PhysD3,TopoD3,Real>& sfld,
    Field_CG_Cell<PhysD3, TopoD3, DLA::MatrixSymS<PhysD3::D, Real>>& metric_request);

} // namespace SANS
