// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "MesherInterface_refine.h"

#define PYDICT_INSTANTIATE
#include "Python/PyDict_impl.h"

namespace SANS
{
#ifdef SANS_REFINE
PYDICT_PARAMETER_OPTION_INSTANTIATE(refineParams::CallOptions)
#endif

// cppcheck-suppress passedByValue
void refineParams::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.ref_driver));
  allParams.push_back(d.checkInputs(params.FilenameBase));
  allParams.push_back(d.checkInputs(params.CADfilename));
  allParams.push_back(d.checkInputs(params.DisableCall));
  allParams.push_back(d.checkInputs(params.DumpRefineDebugFiles));
  allParams.push_back(d.checkInputs(params.nIter));
#ifdef SANS_REFINE
  allParams.push_back(d.checkInputs(params.CallMethod));
#endif
  d.checkUnknownInputs(allParams);
}
refineParams refineParams::params;


}
