// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef MESHERINTERFACE_REFINE_H_
#define MESHERINTERFACE_REFINE_H_

//Python must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <memory>

#include "Field/XField.h"
#include "Field/Field.h"

namespace SANS
{

template <class PhysDim, class TopoDim, class Mesher>
class MesherInterface;

class refine;

//Class for transferring meshes and metric requests between SANS and the NASA refine mesher

struct refineParams : noncopyable
{
  const ParameterString ref_driver{"ref_driver", "ref_driver", "ref_driver executable to invoke. May be a full path."};
  const ParameterString FilenameBase{"FilenameBase", "tmp/", "Default file path prefix for generated files"};
  const ParameterFileName CADfilename{"CADfilename", std::ios_base::in, "", "CAD file readable by EGADS" };
  const ParameterBool DisableCall{"DisableCall", false, "Disable refine call for testing"};
  const ParameterBool DumpRefineDebugFiles{"DumpRefineDebugFiles", false, "Export refine debug files"};
  const ParameterNumeric<int> nIter{"nIter", 15, 0, NO_LIMIT, "Number of refine iterations"};


#ifdef SANS_REFINE
  struct CallOptions
  {
    typedef std::string ExtractType;
    const std::string system = "system";
    const std::string API = "API";

    const std::vector<std::string> options{system, API};
  };
  const ParameterOption<CallOptions> CallMethod =
        ParameterOption<CallOptions>("CallMethod", "system", "Method for calling refine ");
#endif

  static void checkInputs(PyDict d);
  static refineParams params;
};

template <class PhysDim, class TopoDim>
class MesherInterface<PhysDim, TopoDim, refine>
{
public:
  static const int D = PhysDim::D;
  typedef DLA::MatrixSymS<D,Real> MatrixSym;
  typedef XField<PhysDim, TopoDim> XFieldType;

  MesherInterface(int adapt_iter, const PyDict& paramsDict);

  std::shared_ptr<XFieldType> adapt(const Field_CG_Cell<PhysDim,TopoDim,MatrixSym>& metric_request) const;

protected:

  std::shared_ptr<XFieldType> systemcall(const Field_CG_Cell<PhysDim,TopoDim,MatrixSym>& metric_request) const;
#ifdef SANS_REFINE
  std::shared_ptr<XFieldType> API(const Field_CG_Cell<PhysDim,TopoDim,MatrixSym>& metric_request) const;
#endif

  int adapt_iter_;
  const PyDict paramsDict_;
};

}

#endif // MESHERINTERFACE_REFINE_H_
