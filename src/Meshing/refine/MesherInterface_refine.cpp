// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <iostream>
#include <sstream>
#include <vector>

#include <boost/filesystem/operations.hpp>

#include "tools/SANSnumerics.h"     // Real
#include "tools/system_call.h"

#include "Topology/Dimension.h"
#include "Topology/ElementTopology.h"

#include "Field/XFieldArea.h"
#include "Field/FieldArea_CG_Cell.h"

#include "Field/XFieldVolume.h"
#include "Field/FieldVolume_CG_Cell.h"

#include "Field/output_Tecplot.h"

#include "Meshing/libMeshb/WriteMesh_libMeshb.h"
#include "Meshing/libMeshb/WriteSolution_libMeshb.h"
#include "Meshing/libMeshb/XField_libMeshb.h"

#include "MesherInterface_refine.h"
#include "XField_refine.h"

#ifdef SANS_REFINE
// refine includes
#include <ref_malloc.h>
#include <ref_grid.h>
#include <ref_metric.h>
#include <ref_adapt.h>
#include <ref_validation.h>
#include <ref_histogram.h>
#include <ref_export.h>
#include <ref_gather.h>
#include <ref_migrate.h>
#include <ref_part.h>
#include <ref_egads.h>

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#endif

// SANSified refine macros and pointers
// must be included after including refine headers
#include "refine_defs.h"
#include "refine_ptr.h"

#endif

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"
#include "MPI/MPI_sleep.h"

namespace SANS
{

template <class PhysDim, class TopoDim>
MesherInterface<PhysDim, TopoDim, refine>::MesherInterface(int adapt_iter, const PyDict& paramsDict) :
  adapt_iter_(adapt_iter), paramsDict_(paramsDict) {}

template <class PhysDim, class TopoDim>
std::shared_ptr<XField<PhysDim, TopoDim>>
MesherInterface<PhysDim, TopoDim, refine>::adapt(const Field_CG_Cell<PhysDim,TopoDim,MatrixSym>& metric_request) const
{
#ifdef SANS_REFINE
  std::string CallMethod = paramsDict_.get(refineParams::params.CallMethod);

  if (CallMethod == refineParams::params.CallMethod.system)
    return systemcall(metric_request);
  else if (CallMethod == refineParams::params.CallMethod.API)
    return API(metric_request);
  else
    SANS_DEVELOPER_EXCEPTION("Unknown CallMethod");

  return nullptr;
#else
  return systemcall(metric_request);
#endif
}

template <class PhysDim, class TopoDim>
std::shared_ptr<XField<PhysDim, TopoDim>>
MesherInterface<PhysDim, TopoDim, refine>::systemcall(const Field_CG_Cell<PhysDim,TopoDim,MatrixSym>& metric_request) const
{
  std::string ref_driver = paramsDict_.get(refineParams::params.ref_driver);
  std::string filename_base = paramsDict_.get(refineParams::params.FilenameBase);
  bool DisableCall = paramsDict_.get(refineParams::params.DisableCall);
  std::string CADfilename = paramsDict_.get(refineParams::params.CADfilename);
  int passes = paramsDict_.get(refineParams::params.nIter);

  std::string mesh_filename = filename_base + "mesh_a" + std::to_string(adapt_iter_) + ".meshb";
  std::string metric_filename = filename_base + "metric_request_a" + std::to_string(adapt_iter_) + ".solb";
  std::string outputname = filename_base + "refine_a" + std::to_string(adapt_iter_+1) + ".meshb";
  std::string refine_log = filename_base + "refine_log.txt";

  if (!CADfilename.empty())
    CADfilename = " -g " + CADfilename;

  //Write current mesh and metric request
  WriteMesh_libMeshb(metric_request.getXField(), mesh_filename);
  WriteSolution_libMeshb(metric_request, metric_filename);

  if (DisableCall == false)
  {
    if (metric_request.comm()->rank() == 0)
    {
      /*-----------------------------*/
      /** system call to ref_driver **/
      /*-----------------------------*/
      std::stringstream args;
      args << "-i " << mesh_filename << " -m " << metric_filename << CADfilename << " -x " << outputname << " -s " << passes;
      args << " >> " << refine_log; //Append refine output to log file

      system_call("refine", ref_driver, args.str());
      wait_for_file(outputname);
    }

    // wait for rank 0 (1 second at a time)
    // using barrier causes waiting processor to use 100% CPU
    MPI_sleep(*metric_request.getXField().comm(), 0, 1000);

    //Read in new mesh
    return std::shared_ptr<XField<PhysDim, TopoDim>>( new XField_libMeshb<PhysDim, TopoDim>( *metric_request.comm(), outputname ) );
  }
  else
    return nullptr;
}

#ifdef SANS_REFINE
template <class PhysDim, class TopoDim>
std::shared_ptr<XField<PhysDim, TopoDim>>
MesherInterface<PhysDim, TopoDim, refine>::API(const Field_CG_Cell<PhysDim,TopoDim,MatrixSym>& metric_request) const
{
  std::string filename_base = paramsDict_.get(refineParams::params.FilenameBase);
  REF_INT passes = paramsDict_.get(refineParams::params.nIter);

  const bool DumpRefineDebugFiles = paramsDict_.get(refineParams::params.DumpRefineDebugFiles);

  // This constructor assumes linear metric field
  for (int i = 0; i < metric_request.nCellGroups(); i++)
    SANS_ASSERT( metric_request.getCellGroupBase(i).order() == 1 );

  const XField<PhysDim,TopoDim>& BG_xfld = metric_request.getXField();

  std::string EGADSfilename;
  std::string MeshCADfilename;
  if (BG_xfld.derivedTypeID() == typeid(XField_refine<PhysDim,TopoDim>))
  {
    EGADSfilename   = static_cast<const XField_refine<PhysDim,TopoDim>&>(BG_xfld).getEGADSfilename();
    MeshCADfilename = static_cast<const XField_refine<PhysDim,TopoDim>&>(BG_xfld).getMeshCADfilename();
  }

  // convert the xfld to ref_grid
  REF_GRID_PTR ref_grid(BG_xfld);

  // Extract MPI
  REF_MPI ref_mpi = ref_grid_mpi(ref_grid);

  // Total number of nodes
  REF_INT nnodes = metric_request.nDOF();
  REF_NODE ref_node = ref_grid_node(ref_grid);

  REF_DBL ref_m[6];

  // Copy over the metric values
  // must use metric_request.local2nativeDOFmap here
  for ( REF_INT i = 0; i < nnodes; i++ )
  {
    REF_INT pos;
    RSS( ref_node_local( ref_node, metric_request.local2nativeDOFmap(i), &pos ), "local node");

    const MatrixSym& m = metric_request.DOF(i);

    // refine expects an upper triangular matrix
    if (PhysDim::D == 2)
    {
      ref_m[0] = m(0,0); ref_m[1] = m(0,1); ref_m[2] = 0.0;
                         ref_m[3] = m(1,1); ref_m[4] = 0.0;
                                            ref_m[5] = 1.0;
    }
    else if (PhysDim::D == 3)
    {
      ref_m[0] = m(0,0); ref_m[1] = m(0,1); ref_m[2] = m(0,2);
                         ref_m[3] = m(1,1); ref_m[4] = m(1,2);
                                            ref_m[5] = m(2,2);
    }
    else
      SANS_DEVELOPER_EXCEPTION("Unsupported dimension!");

    ref_node_metric_set(ref_node,pos,ref_m);
  }

  // get consistent possession and ghost for refines assumptions
  // this also invalidates the 'pos' numbering
  RSS( ref_migrate_shufflin(ref_grid), "shufflin" );

  // TODO: This does not yet work in parallel
  //RSS( ref_validation_all( ref_grid ), "all validation" );

  if ( DumpRefineDebugFiles )
  {
    std::string debug_mesh = filename_base + "refine_debug_a" + stringify(adapt_iter_) + ".meshb";
    if (ref_mpi_once(ref_mpi))
      std::cout << "Writing " << debug_mesh << std::endl;
    RSS( ref_gather_by_extension( ref_grid, debug_mesh.c_str() ), debug_mesh.c_str() );

    std::string debug_metric = filename_base + "refine_debug_a" + stringify(adapt_iter_) + ".solb";
    if (ref_mpi_once(ref_mpi))
      std::cout << "Writing " << debug_metric << std::endl;
    RSS( ref_gather_metric( ref_grid, debug_metric.c_str() ), debug_metric.c_str() );

    std::string debug_tec = filename_base + "refine_debug_a" + stringify(adapt_iter_) + ".tec";
    output_Tecplot_Metric(metric_request, debug_tec);
  }

  try
  {
    if (!MeshCADfilename.empty())
    {
      //Read back in information from the last iteration. TODO: This should be stored in memory
      if (ref_mpi_once(ref_mpi))
        std::cout << "Reading CAD data from: " << MeshCADfilename << std::endl;
      RSS( ref_part_cad_data( ref_grid, MeshCADfilename.c_str() ), MeshCADfilename.c_str() );

      if (ref_mpi_once(ref_mpi))
        std::cout << "Reading CAD association from: " << MeshCADfilename << std::endl;
      RSS( ref_part_cad_association( ref_grid, MeshCADfilename.c_str() ), MeshCADfilename.c_str() );

      // Do some EGADS initialization. TODO: Mike wants to remove this eventually
      RSS( ref_egads_load( ref_grid_geom(ref_grid), EGADSfilename.c_str() ), EGADSfilename.c_str() );

      // Read in bar elements
      if (PhysDim::D == 3)
        RSS( ref_part_cad_discrete_edge(ref_grid, MeshCADfilename.c_str()), "bars" );

      // This must happen after ref_part_cad_association and ref_geom_egads_load
      RSS( ref_egads_mark_jump_degen(ref_grid), "T and UV jumps; UV degen" );
      RSS( ref_geom_verify_topo(ref_grid), "geom topo");
      RSS( ref_geom_verify_param(ref_grid), "geom param");
    }

    // FOR DEBUGGING
    //RSS( ref_export_tec_metric_ellipse( ref_grid, "tmp/refine_ellipse.dat" ), "tec ellipse");
    //RSS( ref_export_tec( ref_grid, (char*)"tmp/refine.dat" ), "tec dump" );

    // TODO: Make this optional
    if (!MeshCADfilename.empty())
      RSS( ref_metric_constrain_curvature( ref_grid ), "curvature constraint");

    RSS(ref_grid_cache_background(ref_grid), "cache");
    RSS(ref_validation_cell_volume(ref_grid), "vol");
    RSS(ref_histogram_quality(ref_grid), "gram");
    RSS(ref_histogram_ratio(ref_grid), "gram");

    // perform load balancing for refines preferences
    RSS(ref_migrate_to_balance(ref_grid), "balance");
    RSS(ref_grid_pack(ref_grid), "pack");

    ref_mpi_stopwatch_start(ref_mpi);

    REF_BOOL all_done = REF_FALSE, all_done0 = REF_FALSE, all_done1 = REF_FALSE;
    for (REF_INT pass = 0; !all_done && pass < passes; pass++ )
    {
      if (ref_mpi_once(ref_mpi))
        printf("\n pass %d of %d with %d ranks\n", pass + 1, passes,
               ref_mpi_n(ref_grid_mpi(ref_grid)));
      all_done1 = all_done0;

      // this will cause sparse globals to be out of sync
      RSS(ref_adapt_pass(ref_grid, &all_done0), "pass");
      all_done = all_done0 && all_done1 && (pass > MIN(5, passes));
      ref_mpi_stopwatch_stop(ref_grid_mpi(ref_grid), "pass");

      RSS(ref_metric_synchronize(ref_grid), "sync with background");
      ref_mpi_stopwatch_stop(ref_mpi, "metric sync");

      // Check the volume of all cells
      RSS( ref_validation_cell_volume( ref_grid ),"vol");
      ref_mpi_stopwatch_stop(ref_grid_mpi(ref_grid), "vol");

      RSS(ref_adapt_tattle_faces(ref_grid), "tattle");

      //  balance resyncs globals
      RSS( ref_migrate_to_balance( ref_grid ), "balance");
      ref_mpi_stopwatch_stop(ref_grid_mpi(ref_grid), "vol");
      RSS(ref_grid_pack(ref_grid), "pack");
      ref_mpi_stopwatch_stop(ref_grid_mpi(ref_grid), "pack");

      //RSS( ref_histogram_ratio( ref_grid ), "gram");
      //RSS( ref_histogram_quality( ref_grid ), "mean ratio");
    }
    // Optional: Prints to the screen
    RSS( ref_histogram_quality( ref_grid ), "mean ratio");
    RSS( ref_histogram_ratio( ref_grid ), "gram");

    RSS(ref_geom_verify_param(ref_grid), "final params");
    ref_mpi_stopwatch_stop(ref_grid_mpi(ref_grid), "verify final params");

    //TODO: Save off CAD association for next iteration. This should be stored in memory
    if (!MeshCADfilename.empty())
    {
      MeshCADfilename = filename_base + "refine_a" + stringify(adapt_iter_) + ".meshb";
      RSS( ref_gather_by_extension( ref_grid, MeshCADfilename.c_str() ), MeshCADfilename.c_str());
    }

    // FOR DEBUGGING
    //RSS( ref_export_tec( ref_grid, "tmp/refined.dat" ), "tec dump" );

    //RSS( ref_node_synchronize_globals(ref_grid_node(ref_grid)), "synchronize globals" );
  }
  catch (const DeveloperException&)
  {
    std::string debug_mesh = filename_base + "refine_debug_a" + stringify(adapt_iter_) + ".meshb";
    if (ref_mpi_once(ref_mpi))
      std::cout << "Writing " << debug_mesh << std::endl;
    RSS( ref_gather_by_extension( ref_grid, debug_mesh.c_str() ), debug_mesh.c_str() );

    std::string debug_metric = filename_base + "refine_debug_a" + stringify(adapt_iter_) + ".solb";
    if (ref_mpi_once(ref_mpi))
      std::cout << "Writing " << debug_metric << std::endl;
    RSS( ref_gather_metric( ref_grid, debug_metric.c_str() ), debug_metric.c_str() );

    std::string debug_tec = filename_base + "refine_debug_a" + stringify(adapt_iter_) + ".tec";
    output_Tecplot_Metric(metric_request, debug_tec);
    throw;
  }

  //return the new mesh
  return std::shared_ptr<XField<PhysDim, TopoDim>>( new XField_refine<PhysDim, TopoDim>( BG_xfld.comm(), ref_grid, EGADSfilename, MeshCADfilename ) );
}
#endif

//Explicit instantiation
template class MesherInterface<PhysD2, TopoD2, refine>;
template class MesherInterface<PhysD3, TopoD3, refine>;

}
