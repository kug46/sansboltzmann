// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SANS_REFINE_PTR_H
#define SANS_REFINE_PTR_H

// create smart versions of refine pointers to simplify deallocation logic

#ifdef SANS_REFINE

#include <memory> // std::shared_ptr

#include <ref_mpi.h>
#include <ref_grid.h>

#include "refine_defs.h"

#include "Field/XField.h"

namespace SANS
{

struct REF_MPI_PTR
{
  REF_MPI_PTR(const REF_MPI_PTR& ref_mpi) : ref_mpi_(ref_mpi.ref_mpi_) {}

  REF_MPI_PTR(void* mpi_comm)
  {
    REF_MPI ref_mpi = NULL;
    RSS( ref_mpi_create_from_comm( &ref_mpi, mpi_comm ), "create ref mpi" );

    ref_mpi_ = std::shared_ptr<REF_MPI_STRUCT>( ref_mpi, [](REF_MPI_STRUCT* ref_mpi) { RSS( ref_mpi_free( ref_mpi ), "free mpi" ); } );
  }

  operator       REF_MPI_STRUCT*()       { return ref_mpi_.get(); }
  operator const REF_MPI_STRUCT*() const { return ref_mpi_.get(); }

        REF_MPI_STRUCT* operator->()       { return ref_mpi_.get(); }
  const REF_MPI_STRUCT* operator->() const { return ref_mpi_.get(); }

protected:
  std::shared_ptr<REF_MPI_STRUCT> ref_mpi_;
};

struct REF_GRID_PTR
{
  REF_GRID_PTR(REF_MPI_PTR ref_mpi) : ref_mpi_(ref_mpi)
  {
    REF_GRID ref_grid = NULL;
    RSS( ref_grid_create( &ref_grid, ref_mpi ), "create grid" );

    ref_grid_ = std::shared_ptr<REF_GRID_STRUCT>( ref_grid, [](REF_GRID_STRUCT* ref_grid) { RSS( ref_grid_free( ref_grid ), "free grid"); } );
  }

  REF_GRID_PTR(const REF_GRID_PTR& ref_grid) : ref_mpi_(ref_grid.ref_mpi_), ref_grid_(ref_grid.ref_grid_)
  {
//    REF_GRID ref_copy_grid = NULL;
//    RSS( ref_grid_deep_copy( &ref_copy_grid, const_cast<REF_GRID_PTR&>(ref_grid) ), "copy grid" );
//
//    ref_grid_ = std::shared_ptr<REF_GRID_STRUCT>( ref_copy_grid, [](REF_GRID_STRUCT* ref_grid) { RSS( ref_grid_free( ref_grid ), "free grid"); } );
  }

  REF_GRID_PTR(REF_MPI_PTR ref_mpi, REF_GRID ref_grid) : ref_mpi_(ref_mpi)
  {
    // make sure the mpi pointer is the same
    SANS_ASSERT(ref_grid_mpi(ref_grid) == ref_mpi);

    ref_grid_ = std::shared_ptr<REF_GRID_STRUCT>( ref_grid, [](REF_GRID_STRUCT* ref_grid) { RSS( ref_grid_free( ref_grid ), "free grid"); } );
  }

  // construct a ref_grid from xfld
  template<class PhysDim, class TopoDim>
  REF_GRID_PTR(const XField<PhysDim,TopoDim>& xfld) : REF_GRID_PTR(XField_to_ref_grid(xfld)) {}

  operator       REF_GRID_STRUCT*()       { return ref_grid_.get(); }
  operator const REF_GRID_STRUCT*() const { return ref_grid_.get(); }

        REF_GRID_STRUCT* operator->()       { return ref_grid_.get(); }
  const REF_GRID_STRUCT* operator->() const { return ref_grid_.get(); }

protected:
  template<class PhysDim, class TopoDim>
  static
  REF_GRID_PTR XField_to_ref_grid(const XField<PhysDim,TopoDim>& xfld);

  REF_MPI_PTR ref_mpi_;
  std::shared_ptr<REF_GRID_STRUCT> ref_grid_;
};

}

#endif // SANS_REFINE

#endif
