// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SANS_REFINE_H
#define SANS_REFINE_H

//Generates a volume tetrahedron grid using refine

#include "Field/XField.h"

#include <cstdio>
#include "refine_ptr.h"

namespace SANS
{

template<class PhysDim, class TopoDim>
class XField_refine : public XField<PhysDim, TopoDim>
{
public:
  typedef XField<PhysDim, TopoDim> BaseType;
  using BaseType::D;

  typedef DLA::MatrixSymS<D,Real> MatrixSym;

  virtual const std::type_info& derivedTypeID() const { return typeid(XField_refine); }

  XField_refine( const XField_refine& ) = delete;
  XField_refine& operator=( const XField_refine& ) = delete;
  virtual ~XField_refine();

#ifdef SANS_REFINE
  // constructor based on a refine grid
  explicit XField_refine( std::shared_ptr<mpi::communicator> comm,
                          REF_GRID_PTR& ref_grid,
                          const std::string& EGADSfilename,
                          const std::string& MeshCADfilename);
#endif

  // constructor based on an egads file
  explicit XField_refine( mpi::communicator comm,
                          const std::string& EGADSfilename );

  // constructor from an egads file and existing meshb file
  explicit XField_refine( mpi::communicator comm,
                          const std::string& EGADSfilename,
                          const std::string& MeshCADfilename);

  std::string getEGADSfilename() const { return EGADSfilename_; }
  std::string getMeshCADfilename() const { return MeshCADfilename_; }
  bool flipTri() const { return flipTri_; }

protected:
#ifdef SANS_REFINE
  void build(REF_GRID_PTR& ref_grid);
#endif

  std::string EGADSfilename_;
  std::string MeshCADfilename_;
  bool flipTri_;
};

}

#endif //SANS_REFINE_H
