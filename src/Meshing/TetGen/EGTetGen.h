// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EG_TETGEN_H
#define EG_TETGEN_H

//Generates a volume tetrahedron grid given an EGADS model using TetGen

#include "Field/XField3D_Wake.h"

#include "Meshing/EGTess/EGTessModel.h"

namespace SANS
{
namespace EGADS
{

class EGTetGen : public XField3D_Wake
{
public:
  typedef XField3D_Wake BaseType;
  typedef BaseType::VectorX CartCoord;

  EGTetGen( const EGTetGen& tet ) = delete; //: BaseType(tet), nBoundaryFrameGroups_(0), boundaryFrameGroups_(NULL), dupPointOffset_(0) {}

  //Constructor based on an EGADS model
  EGTetGen( const EGModel<Dim>& model,
            std::vector<double> EGADSparams = {0.25, 0.001, 15.0},    //EGADS global tessellation parameters
            Real maxRadiusEdgeRatio = 2, Real minDihedralAngle = 0 ); //TetGen quality parameters

  //Constructor based on an EGADS model tessellation
  EGTetGen( const EGTessModel& tessModel,
            const Real maxRadiusEdgeRatio = 2, const Real minDihedralAngle = 0,  //TetGen quality parameters
            const bool potential_adjoint = false);
};

}
}

#endif //EG_TETGEN_H
