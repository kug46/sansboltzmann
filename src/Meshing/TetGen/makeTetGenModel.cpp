// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define TETLIBRARY

#include <cmath> // sqrt
#include <vector>
#include <limits>
#include <egads.h>

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "makeTetGenModel.h"
#include "Meshing/EGADS/isEquivalent.h"
#include "Meshing/EGADS/isSame.h"
#include "tools/stringify.h"
#include "BasisFunction/TraceToCellRefCoord.h"
#include "EGTetGen.h"

#define CROSS(a,b,c)  a[0] = (b[1]*c[2]) - (b[2]*c[1]);\
                      a[1] = (b[2]*c[0]) - (b[0]*c[2]);\
                      a[2] = (b[0]*c[1]) - (b[1]*c[0])
#define DOT(a,b)     (a[0]*b[0] + a[1]*b[1] + a[2]*b[2])

namespace SANS
{
namespace EGADS
{

void
saveTecplotPLC(tetgenio &in, const std::string& filename)
{
  std::cout << "Writing " << filename << std::endl;

  FILE* fp = fopen( filename.c_str(), "w" );

  int nelem = 0;

  for ( int i = 0; i < in.numberoffacets; i++ )
    nelem += in.facetlist[i].numberofpolygons;

  fprintf( fp, "\"\"\n" );
  fprintf( fp, "VARIABLES = \"X\", \"Y\", \"Z\"\n" );
  fprintf( fp, "ZONE T=\"Total PLC\", N=%d, E=%d, F=FEPOINT, ET=TRIANGLE\n", in.numberofpoints, nelem );

  // Write the nodes
  for ( int i = 0; i < in.numberofpoints; i++ )
    fprintf( fp, "%22.15e %22.15e %22.15e\n", in.pointlist[3*i+0], in.pointlist[3*i+1], in.pointlist[3*i+2] );


  std::vector<int> nfaceelem;
  std::map<int,int> marker_map;
  int marker = 0;
  for ( int i = 0; i < in.numberoffacets; i++ )
  {
    if ( marker != in.facetmarkerlist[i] )
    {
      marker = in.facetmarkerlist[i];
      marker_map[marker] = nfaceelem.size();
      nfaceelem.push_back(0);
    }

    for ( int j = 0; j < in.facetlist[i].numberofpolygons; j++ )
    {
      fprintf( fp, "%d %d %d\n", in.facetlist[i].polygonlist[j].vertexlist[0]+1,
                                 in.facetlist[i].polygonlist[j].vertexlist[1]+1,
                                 in.facetlist[i].polygonlist[j].vertexlist[2]+1 );
      nfaceelem[marker_map[marker]]++;
    }
  }

  marker = 0;
  for ( int i = 0; i < in.numberoffacets; i++ )
  {
    if ( marker != in.facetmarkerlist[i] )
    {
      marker = in.facetmarkerlist[i];
      fprintf( fp, "ZONE T=\"PLC %d\", N=%d, E=%d, F=FEPOINT, ET=TRIANGLE, VARSHARELIST=([1,2,3]=1)\n",
               marker, in.numberofpoints, nfaceelem[marker_map[marker]] );
    }

    // Write the connectivity
    for ( int j = 0; j < in.facetlist[i].numberofpolygons; j++ )
      fprintf( fp, "%d %d %d\n", in.facetlist[i].polygonlist[j].vertexlist[0]+1,
                                 in.facetlist[i].polygonlist[j].vertexlist[1]+1,
                                 in.facetlist[i].polygonlist[j].vertexlist[2]+1 );
  }
  fclose( fp );
  std::cout << "Done writing " << filename << std::endl;
}

void modelIndexMap(const std::vector< EGBody<3> >& bodies, const std::vector< int >& bodymap,
                   int& modelIndex, std::vector< std::vector<int> >& topoIndexMap, const int TOPO)
{
  //Builds up a model indexing for a topology type inside bodies
  topoIndexMap.resize(bodymap.size());
  for ( std::size_t ibody = 0; ibody < bodymap.size(); ibody++ )
  {
    const int nTopo = bodies[bodymap[ibody]].nTopos(TOPO);
    topoIndexMap[ibody].resize(nTopo);

    for ( int itopo = 0; itopo < nTopo; itopo++ ) //Starting at 1 because indexes are 1 based in EGADS
      topoIndexMap[ibody][itopo] = modelIndex++;
  }
}

void tessSheetBodyEdges(const std::vector< EGBody<3> >& bodies, const std::vector< int >* modelBodyIndex,
                        const std::vector<ego>& tess,
                        std::vector< std::vector< int > >* modelEdgeIndex,
                        std::vector< std::vector< int > >* modelNodeIndex )
{
  const std::vector< int >& solidBodies = modelBodyIndex[0];
  const std::vector< int >& sheetBodies = modelBodyIndex[1];

  // Initialze the edge index map to identity. This map will be used to identify "edge equality" in the zipper process
  std::vector< std::vector< int > >& solidEdgeIndex = modelEdgeIndex[0];
  std::vector< std::vector< int > >& sheetEdgeIndex = modelEdgeIndex[1];
  int edgeIndex = 0;

  modelIndexMap(bodies, solidBodies, edgeIndex, solidEdgeIndex, EDGE);
  modelIndexMap(bodies, sheetBodies, edgeIndex, sheetEdgeIndex, EDGE);

  std::vector< std::vector< int > >& solidNodeIndex = modelNodeIndex[0];
  std::vector< std::vector< int > >& sheetNodeIndex = modelNodeIndex[1];
  int nodeIndex = 0;

  modelIndexMap(bodies, solidBodies, nodeIndex, solidNodeIndex, NODE);
  modelIndexMap(bodies, sheetBodies, nodeIndex, sheetNodeIndex, NODE);

  //Find all sheet body edges that also exist in solid bodies
  for ( std::size_t isheet = 0; isheet < sheetBodies.size(); isheet++ )
  {
    std::vector< EGEdge<3> > sheetEdges = bodies[sheetBodies[isheet]].getEdges();
    //Loop over the solid bodies for each sheet body
    for ( std::size_t isolid = 0; isolid < solidBodies.size(); isolid++ )
    {
      std::vector< EGEdge<3> > solidEdges = bodies[solidBodies[isolid]].getEdges();
      //Loop over all edges in the sheet body and solid body
      for ( int isheetedge = 0; isheetedge < (int)sheetEdges.size(); isheetedge++ )
        for ( int isolidedge = 0; isolidedge < (int)solidEdges.size(); isolidedge++ )
        {

          if (isSame(sheetEdges[isheetedge], solidEdges[isolidedge]))
          {
            //Grab the vertexes from both edges and use the edge with the larger count
            int solidlen;
            const double *psolidxyz, *psolidt;
            EG_STATUS( EG_getTessEdge(tess[solidBodies[isolid]], isolidedge+1, &solidlen, &psolidxyz, &psolidt) );

            int sheetlen;
            const double *psheetxyz, *psheett;
            EG_STATUS( EG_getTessEdge(tess[sheetBodies[isheet]], isheetedge+1, &sheetlen, &psheetxyz, &psheett) );

            if (solidlen > sheetlen)
            {
              // Transfer the tessellation from the solid body
              if ( solidlen >= 3 )
                sheetEdges[isheetedge].addAttribute(".tPos", &psolidt[1], solidlen-2);
              else
                sheetEdges[isheetedge].addAttribute(".tPos", 0);
            }
            else
            {
              // Transfer the tessellation from the sheet body
              if ( sheetlen >= 3 )
                solidEdges[isolidedge].addAttribute(".tPos", &psheett[1], sheetlen-2);
              else
                solidEdges[isolidedge].addAttribute(".tPos", 0);
            }

            //Update the index map to register the equality
            sheetEdgeIndex[isheet][isheetedge] = solidEdgeIndex[isolid][isolidedge];

            const EGBody<3>& solidBody = bodies[solidBodies[isolid]];
            const EGBody<3>& sheetBody = bodies[sheetBodies[isheet]];

//            std::cout << "Same Edges = " << isheetedge << " " << isolidedge << std::endl;

            // Update the node index map by finding which nodes are the same
            std::vector< EGNode<3> > solidNodes = solidEdges[isolidedge].getNodes();
            std::vector< EGNode<3> > sheetNodes = sheetEdges[isheetedge].getNodes();

            for (auto sheetnode = sheetNodes.begin(); sheetnode != sheetNodes.end(); sheetnode++ )
              for (auto solidnode = solidNodes.begin(); solidnode != solidNodes.end(); solidnode++ )
                if ( isSame(*sheetnode, *solidnode) )
                {
                  sheetNodeIndex[isheet][sheetBody.getBodyIndex(*sheetnode)-1] =
                      solidNodeIndex[isolid][solidBody.getBodyIndex(*solidnode)-1];
                  break;
                }

            break;
          }
        }
    }
  }
}

/*
 *  calculates and returns a TetGen mesh
 *
 */
int
makeTetGenModel(const EGModel<3>& model, std::vector< int > modelBodyIndex[2],
                std::map<int, std::set<int> >& duplicatePoints,
                tetgenio &out,
                const std::vector<double>& EGADSparams,
                Real maxRadiusEdgeRatio, Real minDihedralAngle)
{
  int          i, j, k, base, npts, ntri;
  int          plen, tlen;
  const int    *tris, *tric, *ptype, *pindex;
  REAL       *xyzs;
  const double *points, *uv;

  SANS_ASSERT( EGADSparams.size() == 3 );

  tetgenio in;

  //Initialize tetgen
  in.firstnumber = 0;
  out.firstnumber = 0;

  // Get all the bodies in the model
  std::vector< EGBody<3> > bodies = model.getBodies();
  const int nbody = (int)bodies.size();

  std::vector< int >& solidBodies = modelBodyIndex[0];
  std::vector< int >& sheetBodies = modelBodyIndex[1];

  std::vector< std::vector< int > > modelEdgeIndex[2];

  std::vector< std::vector< int > > modelNodeIndex[2];

  std::vector< int > modelFaceTlen(model.nFaces()); //Number of triangles on each face

  // Sort out all sheet/face bodies from the solid bodies
  for ( std::size_t ibody = 0; ibody < bodies.size(); ibody++ )
  {
    if (bodies[ibody].isSolid())
      solidBodies.push_back(ibody);
    else if ( bodies[ibody].isSheet() || bodies[ibody].isFace() )
      sheetBodies.push_back(ibody);
    else
      SANS_ASSERT_MSG(false, "Only expected solid and sheet bodies");
  }

  std::vector<ego> tess(nbody, NULL);

  //double tessparams[3] = {0.15, 0.01, 90};
  double size = 1; //model.getSize();
  //double tessparams[3] = {0.025*size, 0.001*size, 15.0};
  //double tessparams[3] = {0.025*size, 0.001*size, 10.0};
  //double tessparams[3] = {size, 0.001*size, 15.0};
  //double tessparams[3] = {2*0.15*size, 0.001*size, 15.0};
  //double tessparams[3] = {0.025*size, 0.001*size, 8.0};

  // Scale the paramters buy the size of the model
  double tessparams[3] = {EGADSparams[0]*size, EGADSparams[1]*size, EGADSparams[2]};

  // Negating the first paramter triggers EGADS to only put vertexes on edges
  double edgeparams[3] = {-tessparams[0], tessparams[1], tessparams[2]};

  // Tesselate all the edges
  for ( std::size_t ibody = 0; ibody < bodies.size(); ibody++ )
    EG_STATUS( EG_makeTessBody(bodies[ibody], edgeparams, &tess[ibody]) );

  // Transfer edge tessellation between solid bodies to coincident sheet body edges and find global edge/node indexes
  tessSheetBodyEdges(bodies, modelBodyIndex, tess, modelEdgeIndex, modelNodeIndex );

  //Release all the edge tessellation memory
  for (auto t = tess.begin(); t != tess.end(); t++)
  {
    EG_STATUS( EG_deleteObject(*t) );
    *t = NULL;
  }

  // Create a tessellation for each solid body and count the number of points and triangles
  npts = ntri = 0;
  int face = 0;
  for ( std::size_t ibody = 0; ibody < bodies.size(); ibody++ )
  {
    //Create the body tessellation
    EG_STATUS( EG_makeTessBody(bodies[ibody], tessparams, &tess[ibody]) );

    int nface = bodies[ibody].nFaces();

    for (i = 1; i <= nface; i++)
    {
      EG_STATUS( EG_getTessFace(tess[ibody], i, &plen, &points, &uv, &ptype, &pindex,
                              &tlen, &tris, &tric) );
      npts += plen;
      ntri += tlen;
      face++;
    }
  }


  //--------------------------------
  // Create maps of which edges could have points duplicated (i.e. for wakes in full potential)
  int modelEdge = 0;
  int modelNode = 0;
  for ( std::size_t ibody = 0; ibody < bodies.size(); ibody++ )
  {
    modelEdge += bodies[ibody].nEdges();
    modelNode += bodies[ibody].nNodes();
  }

  std::vector<int> modelTrefftzNodes(modelNode, 0);
  std::vector<int> modelTrefftzEdges(modelEdge, 0);
  std::vector<bool> modelEdgeDuplicity(modelEdge, true);
  std::vector<bool> modelNodeDuplicity(modelNode, true);

  // Get the starting indexes for the sheet bodies
  modelEdge = 0;
  modelNode = 0;
  for ( std::size_t ibody = 0; ibody < solidBodies.size(); ibody++ )
  {
    const int bodyIndex = solidBodies[ibody];
    modelEdge += bodies[bodyIndex].nEdges();
    modelNode += bodies[bodyIndex].nNodes();
  }

  for ( std::size_t ibody = 0; ibody < sheetBodies.size(); ibody++ )
  {
    const int bodyIndex = sheetBodies[ibody];
    std::vector< EGEdge<3> > edges = bodies[bodyIndex].getEdges();

    for ( std::size_t iedge = 0; iedge < edges.size(); iedge++ )
    {

      // Edges with only one face attached cannot be duplicated
      if ( bodies[bodyIndex].getFaces( edges[iedge] ).size() == 1 )
        modelEdgeDuplicity[modelEdge] = false;

      // Check if the edge has been marked as a Trefftz edge
      if ( edges[iedge].hasAttribute("Trefftz") )
      {
        int TrefftzNo = 0;
        edges[iedge].getAttribute("Trefftz", TrefftzNo);
        SANS_ASSERT( TrefftzNo > 0 );
        modelTrefftzEdges[modelEdgeIndex[1][ibody][iedge]] = TrefftzNo;

        // Mark the nodes on the Trefftz edge as -1
        std::vector< EGNode<3> > nodes = edges[iedge].getNodes();
        for ( std::size_t n = 0; n < nodes.size(); n++)
        {
          int nodeIndex = bodies[bodyIndex].getBodyIndex(nodes[n])-1;
          modelTrefftzNodes[modelNodeIndex[1][ibody][nodeIndex]] = -1;
        }
      }

      if ( edges[iedge].hasAttribute("Kutta") )
      {
        int KuttaNo = 0;
        edges[iedge].getAttribute("Kutta", KuttaNo);
        SANS_ASSERT( KuttaNo > 0 );
        modelTrefftzEdges[modelEdgeIndex[1][ibody][iedge]] = KuttaNo;

        // Mark the nodes on the Kutta edge as -1
        std::vector< EGNode<3> > nodes = edges[iedge].getNodes();
        for ( std::size_t n = 0; n < nodes.size(); n++)
        {
          int nodeIndex = bodies[bodyIndex].getBodyIndex(nodes[n])-1;
          modelTrefftzNodes[modelNodeIndex[1][ibody][nodeIndex]] = -1;
        }
      }


      modelEdge++;
    }
  }

  //--------------------------------
  // Create maps of which nodes could have points duplicated (i.e. for wakes in full potential)
  for ( std::size_t ibody = 0; ibody < sheetBodies.size(); ibody++ )
  {
    const int bodyIndex = sheetBodies[ibody];
    std::vector< EGNode<3> > nodes = bodies[bodyIndex].getNodes();

    for ( std::size_t inode = 0; inode < nodes.size(); inode++ )
    {
      const int bodyNode = bodies[bodyIndex].getBodyIndex(nodes[inode])-1;
      // Extract all edges that reference the node
      const std::vector< EGEdge<3> > edges = bodies[bodyIndex].getEdges( nodes[inode] );

      // All edges with two faces can have points that are duplicated
      for ( std::size_t iedge = 0; iedge < edges.size(); iedge++ )
      {
        const int bodyEdge = bodies[bodyIndex].getBodyIndex(edges[iedge])-1;

        modelEdge = modelEdgeIndex[1][ibody][bodyEdge];
        modelNode = modelNodeIndex[1][ibody][bodyNode];

        // All model Edges must be duplicable if the node can be duplicated
        modelNodeDuplicity[modelNode] = modelNodeDuplicity[modelNode] && modelEdgeDuplicity[modelEdge];
      }
    }
  }


  // allocate memory associated with the points

  std::vector<int> table(2*npts);
  std::vector<bool> duplicateMarker(npts, false); //Indicates of a point is allowed to be duplicated

  in.numberofpointmtrs = 0; // One metric per point

  in.numberofpoints = npts;
  SANS_ASSERT( in.numberofpoints > 0 );
  in.pointlist = new REAL[3*in.numberofpoints];
  in.pointmtrlist = new REAL[in.numberofpoints];
  in.pointmarkerlist = new int[in.numberofpoints];
  xyzs = in.pointlist;

  npts = 0;

  /* zipper up the edges -- one Face at a time */
  for ( int ibodyTopo = 0; ibodyTopo < 2; ibodyTopo++ )
  {
    for ( std::size_t ibody = 0; ibody < modelBodyIndex[ibodyTopo].size(); ibody++ )
    {
      const int bodyIndex = modelBodyIndex[ibodyTopo][ibody];
      int nface = bodies[bodyIndex].nFaces();
      const std::vector< EGFace<3> > faces = bodies[bodyIndex].getFaces();

      for (j = 1; j <= nface; j++)
      {
        EG_STATUS( EG_getTessFace(tess[bodyIndex], j, &plen, &points, &uv, &ptype, &pindex,
                                  &tlen, &tris, &tric) );

        Real metric = 0;
        if (faces[j-1].hasAttribute(".tParams"))
        {
          std::vector<double> tParams;
          faces[j-1].getAttribute(".tParams", tParams);
          metric = tParams[0];
        }

        for (i = 0; i < plen; i++)
        {
          in.pointmtrlist[npts] = metric;
          in.pointmarkerlist[npts] = 0;

          //Use model global indexing to build up the table
          if ( ptype[i] == 0 ) // Node
          {
            table[2*npts  ] = ptype[i];
            table[2*npts+1] = modelNodeIndex[ibodyTopo][ibody][pindex[i]-1]+1;
            duplicateMarker[npts] = modelNodeDuplicity[table[2*npts+1]-1];
            in.pointmarkerlist[npts] = modelTrefftzNodes[table[2*npts+1]-1];
          }
          else if ( ptype[i] > 0 ) // Edge
          {
            table[2*npts  ] = ptype[i];
            table[2*npts+1] = modelEdgeIndex[ibodyTopo][ibody][pindex[i]-1]+1;
            duplicateMarker[npts] = modelEdgeDuplicity[table[2*npts+1]-1];
            in.pointmarkerlist[npts] = modelTrefftzEdges[table[2*npts+1]-1];
          }
          else
          {
            table[2*npts  ] = ptype[i];
            table[2*npts+1] = pindex[i];
            duplicateMarker[npts] = true; // Points interior on a face are always allowed to be duplicated
          }
          xyzs[3*npts  ]  = points[3*i  ];
          xyzs[3*npts+1]  = points[3*i+1];
          xyzs[3*npts+2]  = points[3*i+2];
          /* for non-interior pts -- try to match with others */
          if (ptype[i] != -1)
            for (k = 0; k < npts; k++)
              if ((table[2*k]==table[2*npts]) && (table[2*k+1]==table[2*npts+1]))
              {
                table[2*npts  ] = k;
                table[2*npts+1] = 0;
                break;
              }
          npts++;
        }
      }
    }
  }
  /* fill up the whole triangle list -- a Face at a time */

  std::vector<int> tri(3*ntri);

  ntri = base = 0;
  face = 0;
  for ( int ibodyTopo = 0; ibodyTopo < 2; ibodyTopo++ )
  {
    for ( std::size_t ibody = 0; ibody < modelBodyIndex[ibodyTopo].size(); ibody++ )
    {
      const int bodyIndex = modelBodyIndex[ibodyTopo][ibody];
      int nface = bodies[bodyIndex].nFaces();

      for (j = 1; j <= nface; j++)
      {
        /* get the face tessellation and store it away */
        EG_STATUS( EG_getTessFace(tess[bodyIndex], j, &plen, &points, &uv, &ptype, &pindex,
                                  &tlen, &tris, &tric) );

        for (i = 0; i < tlen; i++, ntri++)
        {
          for (int n = 0; n < 3; n++)
          {
            k = tris[3*i+n] + base;
            if (table[2*k-1] == 0)
              tri[3*ntri+n] = table[2*k-2] + 1;
            else
              tri[3*ntri+n] = k;
          }
        }
        base += plen;
        face++;
      }
    }
  }



  /* remove the unused points -- crunch the point list
   *    NOTE: the returned pointer verts has the full length (not realloc'ed)
   */

  for (i = 0; i <   npts; i++) table[i] = 0;
  for (i = 0; i < 3*ntri; i++) table[tri[i]-1]++;
  for (plen = i = 0; i < npts; i++)
  {
    if (table[i] == 0) continue;
    xyzs[3*plen  ] = xyzs[3*i  ];
    xyzs[3*plen+1] = xyzs[3*i+1];
    xyzs[3*plen+2] = xyzs[3*i+2];
    duplicateMarker[plen] = duplicateMarker[i];
    in.pointmtrlist[plen] = in.pointmtrlist[i];
    in.pointmarkerlist[plen] = in.pointmarkerlist[i];
    plen++;
    table[i] = plen;
  }

  //Simply shorten the list to "remove" duplicate points
  in.numberofpoints = plen;

  in.numberoffacets = ntri;
  SANS_ASSERT( in.numberoffacets > 0 );
  in.facetlist = new tetgenio::facet[in.numberoffacets];
  in.facetmarkerlist = new int[in.numberoffacets];

  base = 0;
  face = 0;
  int BCface = 1;
  int Iface = 1;
  int globaltri = 0;
  for ( int ibodyTopo = 0; ibodyTopo < 2; ibodyTopo++ )
  {
    for ( int ibody = 0; ibody < (int)modelBodyIndex[ibodyTopo].size(); ibody++ )
    {
      const int bodyIndex = modelBodyIndex[ibodyTopo][ibody];
      int nface = bodies[bodyIndex].nFaces();
      std::vector< EGFace<3> > faces = bodies[bodyIndex].getFaces();

      for (j = 1; j <= nface; j++)
      {

        EG_STATUS( EG_getTessFace(tess[bodyIndex], j, &plen, &points, &uv, &ptype, &pindex,
                                  &tlen, &tris, &tric) );

        bool dupPoints = false;
        if ( ibodyTopo == 1 && faces[j-1].hasAttribute("Wake") )
          dupPoints = true;

        std::set<int> dupSet;

        //tlen = modelFaceTlen[face];
        int marker = 0;

        // Negate the marker for sheet faces unless it is a wake
        if ( ibodyTopo == 1 && !faces[j-1].hasAttribute("Wake") )
        {
          marker = -Iface;
          Iface++;
        }
        else
        {
          marker = BCface;
          BCface++;
        }

        for (i = 0; i < tlen; i++)
        {
          in.facetmarkerlist[globaltri] = marker;

          tetgenio::facet *f = in.facetlist + globaltri;
          f->numberofpolygons = 1;
          f->polygonlist = new tetgenio::polygon[f->numberofpolygons];
          f->numberofholes = 0;
          f->holelist = NULL;

          tetgenio::polygon *p = f->polygonlist;
          p->numberofvertices = 3;
          p->vertexlist = new int[3];

          for (int n = 0; n < 3; n++)
          {
            k = tri[3*(i + base)+n] - 1;
            p->vertexlist[n] = table[k]-1;

            if (dupPoints && duplicateMarker[p->vertexlist[n]])
              dupSet.insert(p->vertexlist[n]);
          }
          globaltri++;
        }

        if (dupPoints)
          duplicatePoints[face] = dupSet;

        base += tlen;
        face++;
      }
    }
  }

  //Release all the tessellation memory
  for (auto t = tess.begin(); t != tess.end(); t++)
    EG_STATUS( EG_deleteObject(*t) );

//  saveTecplotPLC(in, "tmp/GMGW_PLC.dat");
// saveTecplotPLC(in, "tmp/NACAWake3DPLC.dat");

//   model.save("tmp/RAEPLC.egads");

//  in.save_nodes("tmp/tetgen_fail");
//  in.save_poly("tmp/tetgen_fail");
//  in.save_faces2smesh("tmp/tetgen_uninit");
//  in.save_elements("tmp/tetgen_uninit");
//  in.save_faces("tmp/tetgen_uninit");
//  in.save_edges("tmp/tetgen_fail");
//
//
  //saveTecplotPLC(in, "tmp/tmp.dat");

  //Create an "empty mesh" where only surface nodes are connected to create the volume
  //Tet centers of the empty mesh will be used to identify holes
  //
  // -p Generate tetrahedra
  // -Y Preserves the input surface mesh (does not modify it).
  tetgenio emptymesh;
  tetrahedralize((char*)"pYQ", &in, &emptymesh);

  std::vector<double> holepoints;

  // Only solid bodies can have holes
  face = 0;
  globaltri = 0;
  for ( std::size_t isolid = 0; isolid < solidBodies.size(); isolid++ )
  {
    int nface = bodies[solidBodies[isolid]].nFaces();

    for (j = 0; j < nface; j++)
    {
      while ( globaltri < ntri && abs(in.facetmarkerlist[globaltri]) != face+1 ) globaltri++;
      tetgenio::facet *f = in.facetlist + globaltri;
      tetgenio::polygon *p = f->polygonlist;

      // Look for two tets attached to a polygon
      int tet = 0;
      int twotets[2][4] = {{-1, -1, -1, -1}, {-1, -1, -1, -1}};
      for ( k = 0; k < emptymesh.numberoftetrahedra; k++ )
      {
        bool match[3] = {false, false, false};
        for (int v0 = 0; v0 < 3; v0++)
        {
          for (int n0 = 0; n0 < 4; n0++)
          {
            if ( p->vertexlist[v0] == emptymesh.tetrahedronlist[4*k+n0] )
            {
              match[v0] = true;
              break;
            }
          }
          //Found a match, save it of. There should be two tets for a face with holes
          if ( match[0] && match[1] && match[2] )
          {
            for (int n0 = 0; n0 < 4; n0++)
              twotets[tet][n0] = emptymesh.tetrahedronlist[4*k+n0];
            tet++;
          }
        }
        if ( tet == 2 )
          break;
      }


      if ( tet == 2 )
      {
        //Found two tets, the one with a postive normal vector to the cell center is a hole

        //Compute the center of the polygon on the surface
        double polycenter[3] = {0, 0, 0};
        double polynormal[3] = {0, 0, 0};
        double polyedge0[3] = {0, 0, 0};
        double polyedge1[3] = {0, 0, 0};
        for (int n = 0; n < 3; n++)
        {
          for (int v = 0; v < 3; v++)
            polycenter[n] += in.pointlist[p->vertexlist[v]*3 + n];
          polycenter[n] /= 3;

          polyedge0[n] = in.pointlist[p->vertexlist[1]*3 + n] - in.pointlist[p->vertexlist[0]*3 + n];
          polyedge1[n] = in.pointlist[p->vertexlist[2]*3 + n] - in.pointlist[p->vertexlist[0]*3 + n];
        }
        CROSS(polynormal, polyedge0, polyedge1);

        //Compute the center of the two tetrahedra
        double tetcenters[2][3] = {{0, 0, 0}, {0, 0, 0}};
        for (tet = 0; tet < 2; tet++)
          for (int n = 0; n < 3; n++)
          {
            for (int v = 0; v < 4; v++)
              tetcenters[tet][n] += emptymesh.pointlist[twotets[tet][v]*3+n];
            tetcenters[tet][n] /= 4;
          }

        double diffc[3] = {0, 0, 0};
        //Compute the dot product between the normal vector and the vector to the tet-center
        for (tet = 0; tet < 2; tet++)
        {
          for (int n = 0; n < 3; n++)
            diffc[n] = tetcenters[tet][n] - polycenter[n];

          //Positive dot product means hole
          if ( DOT(diffc, polynormal) > 0 )
          {
            for (int n = 0; n < 3; n++)
              holepoints.push_back(tetcenters[tet][n]);
          }
        }
      }
      face++;
    }
  }

  //Add the points inside the holes if any
  if ( holepoints.size() > 0 )
  {
    in.numberofholes = holepoints.size()/3;
    in.holelist = new double[holepoints.size()];
    for ( std::size_t n = 0; n < holepoints.size(); n++ )
      in.holelist[n] = holepoints[n];
  }

  //emptymesh.deinitialize();

  // -p Generate tetrahedra
  // -Y Preserves the input surface mesh (does not modify it).
  // -V verbose
  // -q mesh quality (maximum radius-edge ratio)/(minimum dihedral angle)
  // -a maximum volume constraint
  // -f provides the interior+boundry triangular faces
  // -nn to get tet neighbors for each triangular face
  // -k dumps to paraview when last argument is NULL
  // -m Applies a mesh sizing function
  // -Q quiet

  std::string qual = "q" + stringify(maxRadiusEdgeRatio) + "/" + stringify(minDihedralAngle);

  std::string tetgenparams ="pYfnn" + qual + "m";

  std::cout << "Tetgen input string = " << tetgenparams << std::endl;
  tetrahedralize((char*)tetgenparams.c_str(), &in, &out);
  //tetrahedralize((char*)"pYfnnk", &in, &out);

  return EGADS_SUCCESS;
}


/*
 *  calculates and returns a TetGen mesh
 *
 */
int
makeTetGenModel(const EGTessModel& tessModel,
                std::set<int>& duplicatePoints,
                std::vector<bool>& modelFaceKutta,
                tetgenio &out,
                Real maxRadiusEdgeRatio, Real minDihedralAngle)
{
  int          i, j, k, ntri;

  tetgenio in;

  //Initialize tetgen
  in.firstnumber = 0;
  out.firstnumber = 0;

  // Get all the bodies in the model
  std::vector< EGBody<3> > bodies = tessModel.getBodies();
  //const int nbody = (int)bodies.size();

  const std::vector< int >* modelBodyIndex = tessModel.modelBodyIndex();
  const std::vector< int >& solidBodies = tessModel.modelSolidBodyIndex();
  const std::vector< int >& sheetBodies = tessModel.modelSheetBodyIndex();

  const std::vector< std::vector< int > >& sheetEdgeIndex = tessModel.modelSheetEdgeIndex();
  const std::vector< std::vector< int > >& sheetNodeIndex = tessModel.modelSheetNodeIndex();

  //--------------------------------
  // Create maps of which edges could have points duplicated (i.e. for wakes in full potential)
  int modelFace = 0;
  int modelEdge = 0;
  int modelNode = 0;
  for ( std::size_t ibody = 0; ibody < bodies.size(); ibody++ )
  {
    modelFace += bodies[ibody].nFaces();
    modelEdge += bodies[ibody].nEdges();
    modelNode += bodies[ibody].nNodes();
  }

  modelFaceKutta.resize(modelFace, false);
  std::vector<bool> modelEdgeDuplicity(modelEdge, true);
  std::vector<bool> modelNodeDuplicity(modelNode, true);

  // Get the starting indexes for the sheet bodies
  modelEdge = 0;
  modelNode = 0;
  for ( std::size_t ibody = 0; ibody < solidBodies.size(); ibody++ )
  {
    const int bodyIndex = solidBodies[ibody];
    modelEdge += bodies[bodyIndex].nEdges();
    modelNode += bodies[bodyIndex].nNodes();
  }

  for ( std::size_t ibody = 0; ibody < sheetBodies.size(); ibody++ )
  {
    const int bodyIndex = sheetBodies[ibody];
    std::vector< EGEdge<3> > edges = bodies[bodyIndex].getEdges();

    for ( std::size_t iedge = 0; iedge < edges.size(); iedge++ )
    {
      // Edges with only one face attached cannot be duplicated, unless they are floating edges
      if ( bodies[bodyIndex].getFaces( edges[iedge] ).size() == 1 && !edges[iedge].hasAttribute(EGTessModel_FLOATINGEDGE) )
        modelEdgeDuplicity[modelEdge] = false;

      modelEdge++;
    }
  }

  //--------------------------------
  // Create maps of which nodes could have points duplicated (i.e. for wakes in full potential)
  for ( std::size_t ibody = 0; ibody < sheetBodies.size(); ibody++ )
  {
    const int bodyIndex = sheetBodies[ibody];
    std::vector< EGNode<3> > nodes = bodies[bodyIndex].getNodes();

    for ( std::size_t inode = 0; inode < nodes.size(); inode++ )
    {
      const int bodyNode = bodies[bodyIndex].getBodyIndex(nodes[inode])-1;
      // Extract all edges that reference the node
      const std::vector< EGEdge<3> > edges = bodies[bodyIndex].getEdges( nodes[inode] );

      // All edges with two faces can have points that are duplicated
      for ( std::size_t iedge = 0; iedge < edges.size(); iedge++ )
      {
        const int bodyEdge = bodies[bodyIndex].getBodyIndex(edges[iedge])-1;

        modelEdge = sheetEdgeIndex[ibody][bodyEdge];
        modelNode = sheetNodeIndex[ibody][bodyNode];

        // All model Edges must be duplicable if the node can be duplicated
        modelNodeDuplicity[modelNode] = modelNodeDuplicity[modelNode] && modelEdgeDuplicity[modelEdge];
      }
    }
  }


  //--------------------------------
  for ( std::size_t ibody = 0; ibody < solidBodies.size(); ibody++ )
  {
    const int bodyIndex = solidBodies[ibody];
    std::vector< EGNode<3> > nodes = bodies[bodyIndex].getNodes();

    for ( std::size_t inode = 0; inode < nodes.size(); inode++ )
    {
      // Extract all edges that reference the node
      const std::vector< EGEdge<3> > edges = bodies[bodyIndex].getEdges( nodes[inode] );

      //int nWakeEdge = 0;
      bool nWakeEdge = false;
      std::string WakeName;

      for ( std::size_t iedge = 0; iedge < edges.size(); iedge++ )
      {
        // Count how many edges connected to the node are wake nodes
        if ( edges[iedge].hasAttribute(EGTetGen::WAKESHEET) )
        {
          nWakeEdge = true;
          edges[iedge].getAttribute(EGTetGen::WAKESHEET, WakeName);

          // Mark the faces connected to the edge as Kutta faces
          const std::vector< EGFace<3> > faces = bodies[bodyIndex].getFaces( edges[iedge] );
          for ( std::size_t iface = 0; iface < faces.size(); iface++ )
            modelFaceKutta[ bodies[bodyIndex].getBodyIndex(faces[iface])-1 ] = true;
        }
      }

      //std::cout << "nWakeEdge : " << nWakeEdge << std::endl;
      if ( nWakeEdge && modelNodeDuplicity[inode] )
      {
        //std::cout << "Marking Wake Node : " << WakeName << std::endl;
        nodes[inode].addAttribute(EGTetGen::WAKESHEET, WakeName);
      }

      //if ( nWakeEdge > 2 )
      //  SANS_DEVELOPER_EXCEPTION("Only coded for nWakeEdge = 2: nWakeEdge = %d", nWakeEdge );
    }
  }


  in.numberofpoints = tessModel.xyzs().size();
  in.pointlist = new REAL[3*in.numberofpoints];

  in.numberoffacets = ntri = tessModel.ntri();
  in.facetlist = new tetgenio::facet[in.numberoffacets];
  in.facetmarkerlist = new int[in.numberoffacets];

  in.numberofpointmtrs = 1; // metric per point
  in.pointmtrlist = new REAL[in.numberofpoints];

  const std::vector< std::vector< std::vector< std::array<int,3> > > >& tris = tessModel.tris();

  // Copy over the points into the tetgen input
  for (int i = 0; i < in.numberofpoints; i++)
  {
    for (int n = 0; n < 3; n++)
      in.pointlist[3*i+n] = tessModel.xyzs()[i][n];

    // set the metric to 'infinity'
    in.pointmtrlist[i] = std::numeric_limits<REAL>::max();
  }

  //Indexes to form the sides of a triangle
  const int segnodes[3][2] = {{1,2}, {2,0}, {0,1}};

  int face = 0;
  int BCface = 1;
  int Iface = 1;
  int globaltri = 0;
  for ( int ibodyTopo = 0; ibodyTopo < EGTessModel::NBODYTOPO; ibodyTopo++ )
  {
    for ( std::size_t ibody = 0; ibody < modelBodyIndex[ibodyTopo].size(); ibody++ )
    {
      const int bodyIndex = modelBodyIndex[ibodyTopo][ibody];
      int nface = bodies[bodyIndex].nFaces();
      std::vector< EGFace<3> > faces = bodies[bodyIndex].getFaces();

      for (j = 0; j < nface; j++)
      {
        bool dupPoints = false;
        if ( ibodyTopo == 1 && faces[j].hasAttribute("BCName") )
          dupPoints = true;

        std::set<int> dupSet;

        int marker = 0;

        // Negate the marker for sheet faces unless it is a wake
        if ( ibodyTopo == 1 && !faces[j].hasAttribute("BCName") )
        {
          marker = -Iface;
          Iface++;
        }
        else
        {
          marker = BCface;
          BCface++;
        }

        for (i = 0; i < (int)tris[bodyIndex][j].size(); i++)
        {
          in.facetmarkerlist[globaltri] = marker;

          tetgenio::facet *f = in.facetlist + globaltri;
          f->numberofpolygons = 1;
          f->polygonlist = new tetgenio::polygon[f->numberofpolygons];
          f->numberofholes = 0;
          f->holelist = NULL;

          tetgenio::polygon *p = f->polygonlist;
          p->numberofvertices = 3;
          p->vertexlist = new int[3];

          // compute a surface metric as the smallest segment connected to any vertex
          for (int seg = 0; seg < Triangle::NEdge; seg++)
          {
            const int n0 = tris[bodyIndex][j][i][segnodes[seg][0]];
            const int n1 = tris[bodyIndex][j][i][segnodes[seg][1]];

            DLA::VectorS<3,REAL> X0 = { in.pointlist[3*n0+0], in.pointlist[3*n0+1], in.pointlist[3*n0+2] };
            DLA::VectorS<3,REAL> X1 = { in.pointlist[3*n1+0], in.pointlist[3*n1+1], in.pointlist[3*n1+2] };

            DLA::VectorS<3,REAL> dX = X1 - X0;
            REAL metric = sqrt( dot(dX,dX) );

            in.pointmtrlist[n0] = std::min( metric, in.pointmtrlist[n0] );
            in.pointmtrlist[n1] = std::min( metric, in.pointmtrlist[n1] );
          }


          for (int n = 0; n < Triangle::NNode; n++)
          {
            p->vertexlist[n] = tris[bodyIndex][j][i][n];

            if ( dupPoints )
            {
              bool duplicateMarker = false;

              //Use model global indexing to build up the table
              if ( tessModel.modelTopoType(p->vertexlist[n]) == 0 )
              {
                int nodeIndex = tessModel.modelTopoIndex(p->vertexlist[n])-1;
                duplicateMarker = modelNodeDuplicity[nodeIndex];
              }
              else if ( tessModel.modelTopoType(p->vertexlist[n]) > 0 )
              {
                int edgeIndex = tessModel.modelTopoIndex(p->vertexlist[n])-1;
                duplicateMarker = modelEdgeDuplicity[edgeIndex];
              }
              else
              {
                duplicateMarker = true; // Points interior on a face are always allowed to be duplicated
              }

              if (duplicateMarker)
                dupSet.insert(p->vertexlist[n]);
            }
          }
          globaltri++;
        }

        if (dupPoints)
          duplicatePoints.insert( dupSet.begin(), dupSet.end() );

        face++;
      }
    }
  }

//  saveTecplotPLC(in, "tmp/RAEWakePLC.dat");
//  saveTecplotPLC(in, "tmp/NACAWake3DPLC.dat");
//  saveTecplotPLC(in, "tmp/GMGWPLC.dat");

  //saveTecplotPLC(in, "tmp/tmp.dat");
//   model.save("tmp/RAEPLC.egads");

//  in.save_nodes("tmp/tetgen_fail");
//  in.save_poly("tmp/tetgen_fail");
//  in.save_faces2smesh("tmp/tetgen_uninit");
//  in.save_elements("tmp/tetgen_uninit");
//  in.save_faces("tmp/tetgen_uninit");
//  in.save_edges("tmp/tetgen_fail");
//
//

  //Create an "empty mesh" where only surface nodes are connected to create the volume
  //Tet centers of the empty mesh will be used to identify holes
  //
  // -p Generate tetrahedra
  // -Y Preserves the input surface mesh (does not modify it).
  tetgenio emptymesh;
  tetrahedralize((char*)"pYQ", &in, &emptymesh);

  std::vector<double> holepoints;

  // Only solid bodies can have holes
  face = 0;
  globaltri = 0;
  for ( std::size_t isolid = 0; isolid < solidBodies.size(); isolid++ )
  {
    int nface = bodies[solidBodies[isolid]].nFaces();

    for (j = 0; j < nface; j++)
    {
      while ( globaltri < ntri && abs(in.facetmarkerlist[globaltri]) != face+1 ) globaltri++;
      tetgenio::facet *f = in.facetlist + globaltri;
      tetgenio::polygon *p = f->polygonlist;

      // Look for two tets attached to a polygon
      int tet = 0;
      int twotets[2][4] = {{-1, -1, -1, -1}, {-1, -1, -1, -1}};
      for ( k = 0; k < emptymesh.numberoftetrahedra; k++ )
      {
        bool match[3] = {false, false, false};
        for (int v0 = 0; v0 < 3; v0++)
        {
          for (int n0 = 0; n0 < 4; n0++)
          {
            if ( p->vertexlist[v0] == emptymesh.tetrahedronlist[4*k+n0] )
            {
              match[v0] = true;
              break;
            }
          }
          //Found a match, save it of. There should be two tets for a face with holes
          if ( match[0] && match[1] && match[2] )
          {
            for (int n0 = 0; n0 < 4; n0++)
              twotets[tet][n0] = emptymesh.tetrahedronlist[4*k+n0];
            tet++;

            if ( tet == 2 )
              break;
          }
        }
      }


      if ( tet == 2 )
      {
        //Found two tets, the one with a postive normal vector to the cell center is a hole

        //Compute the center of the polygon on the surface
        double polycenter[3] = {0, 0, 0};
        double polynormal[3] = {0, 0, 0};
        double polyedge0[3] = {0, 0, 0};
        double polyedge1[3] = {0, 0, 0};
        for (int n = 0; n < 3; n++)
        {
          for (int v = 0; v < 3; v++)
            polycenter[n] += in.pointlist[p->vertexlist[v]*3 + n];
          polycenter[n] /= 3;

          polyedge0[n] = in.pointlist[p->vertexlist[1]*3 + n] - in.pointlist[p->vertexlist[0]*3 + n];
          polyedge1[n] = in.pointlist[p->vertexlist[2]*3 + n] - in.pointlist[p->vertexlist[0]*3 + n];
        }
        CROSS(polynormal, polyedge0, polyedge1);

        //Compute the center of the two tetrahedra
        double tetcenters[2][3] = {{0, 0, 0}, {0, 0, 0}};
        for (tet = 0; tet < 2; tet++)
          for (int n = 0; n < 3; n++)
          {
            for (int v = 0; v < 4; v++)
              tetcenters[tet][n] += emptymesh.pointlist[twotets[tet][v]*3+n];
            tetcenters[tet][n] /= 4;
          }

        double diffc[3] = {0};
        //Compute the dot product between the normal vector and the vector to the tet-center
        for (tet = 0; tet < 2; tet++)
        {
          for (int n = 0; n < 3; n++)
            diffc[n] = tetcenters[tet][n] - polycenter[n];

          //Positive dot product means hole
          if ( DOT(diffc, polynormal) > 0 )
          {
            for (int n = 0; n < 3; n++)
              holepoints.push_back(tetcenters[tet][n]);
          }
        }
      }
      face++;
    }
  }

  //Add the points inside the holes if any
  if ( holepoints.size() > 0 )
  {
    in.numberofholes = holepoints.size()/3;
    in.holelist = new double[holepoints.size()];
    for ( std::size_t n = 0; n < holepoints.size(); n++ )
      in.holelist[n] = holepoints[n];
  }

  //emptymesh.deinitialize();

  // -p Tetrahedralizes a piecewise linear complex (PLC).
  // -Y Preserves the input surface mesh (does not modify it).
  // -V verbose
  // -q mesh quality (maximum radius-edge ratio)/(minimum dihedral angle)
  // -a maximum volume constraint
  // -f provides the interior+boundry triangular faces
  // -nn to get tet neighbors for each triangular face
  // -k dumps to paraview when last argument is NULL
  // -m Applies a mesh sizing function
  // -C  Checks the consistency of the final mesh.

  std::string qual = "q" + stringify(maxRadiusEdgeRatio) + "/" + stringify(minDihedralAngle);

  std::string tetgenparams ="pYfnn" + qual + "m";

  std::cout << "Tetgen input string = " << tetgenparams << std::endl;
#if 0

  tetrahedralize((char*)tetgenparams.c_str(), &in, &out);

  return EGADS_SUCCESS;

#else
  tetgenio grid1;
  grid1.firstnumber = 0;

  tetrahedralize((char*)tetgenparams.c_str(), &in, &grid1);


  // count the number of tetrahedra that have all nodes in boundaries
  int ntetcenter = 0;
  for ( int itet = 0; itet < grid1.numberoftetrahedra; itet++ )
  {
    int tet[] = {grid1.tetrahedronlist[4*itet+0], grid1.tetrahedronlist[4*itet+1],
                 grid1.tetrahedronlist[4*itet+2], grid1.tetrahedronlist[4*itet+3]};

    // if all points are less than in.numberofpoints, they came from the inputs
    if ( (tet[0] < in.numberofpoints) &&
         (tet[1] < in.numberofpoints) &&
         (tet[2] < in.numberofpoints) &&
         (tet[3] < in.numberofpoints) )
      ntetcenter++;
  }


  out.numberofpoints = grid1.numberofpoints + ntetcenter;
  out.pointlist      = new REAL[3*out.numberofpoints];

  out.numberoftetrahedra = grid1.numberoftetrahedra + 3*ntetcenter; // Split into 4 tetst, but the oringinal is reused, so just 3 extra
  out.tetrahedronlist    = new int[4*out.numberoftetrahedra];
  out.neighborlist       = new int[4*out.numberoftetrahedra];

  out.numberoftrifaces  = grid1.numberoftrifaces + 6*ntetcenter;
  out.trifacelist       = new int[3*out.numberoftrifaces];
  out.adjtetlist        = new int[2*out.numberoftrifaces];
  out.trifacemarkerlist = new int[  out.numberoftrifaces];

  std::vector<int> split_tets(ntetcenter);

  // copy over the point list
  for ( int i = 0; i < 3*grid1.numberofpoints; i++ )
    out.pointlist[i] = grid1.pointlist[i];

  // Copy over all tet nodes and neighbors and store the tet's that need to be split
  ntetcenter = 0;
  for ( int itet = 0; itet < grid1.numberoftetrahedra; itet++ )
  {

    const int tet[] = {grid1.tetrahedronlist[4*itet+0], grid1.tetrahedronlist[4*itet+1],
                       grid1.tetrahedronlist[4*itet+2], grid1.tetrahedronlist[4*itet+3]};

    for (int n = 0; n < 4; n++)
    {
      out.tetrahedronlist[4*itet+n] = grid1.tetrahedronlist[4*itet+n];
      out.neighborlist[4*itet+n]    = grid1.neighborlist[4*itet+n];
    }

    // save of tests that need to be split
    if ( (tet[0] < in.numberofpoints) &&
         (tet[1] < in.numberofpoints) &&
         (tet[2] < in.numberofpoints) &&
         (tet[3] < in.numberofpoints) )
      split_tets[ntetcenter++] = itet;
  }


  int (*tetfacemap)[4] = new int[grid1.numberoftetrahedra][4];

  // loop over all the faces to copy face data and build up the tet to face map
  for ( int face = 0; face < grid1.numberoftrifaces; face++ )
  {
    for (int n = 0; n < 3; n++)
      out.trifacelist[3*face + n] = grid1.trifacelist[3*face + n];

    for (int n = 0; n < 2; n++)
      out.adjtetlist[2*face + n] = grid1.adjtetlist[2*face + n];

    out.trifacemarkerlist[face] = grid1.trifacemarkerlist[face];


    const int iTetL = grid1.adjtetlist[2*face + 0];
    const int iTetR = grid1.adjtetlist[2*face + 1];

    // Triangle nodes
    int tri[] = {grid1.trifacelist[3*face + 0], grid1.trifacelist[3*face + 1], grid1.trifacelist[3*face + 2]};

    // Left tet nodes
    int tetL[] = {grid1.tetrahedronlist[4*iTetL+0], grid1.tetrahedronlist[4*iTetL+1],
                  grid1.tetrahedronlist[4*iTetL+2], grid1.tetrahedronlist[4*iTetL+3]};

    CanonicalTraceToCell canonicalL = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(tri, 3, tetL, 4);

    // save off the trinagle face index for the trace of the tet
    tetfacemap[iTetL][canonicalL.trace] = face;

    if (iTetR >= 0)
    {
      // right tet nodes
      int tetR[] = {grid1.tetrahedronlist[4*iTetR+0], grid1.tetrahedronlist[4*iTetR+1],
                    grid1.tetrahedronlist[4*iTetR+2], grid1.tetrahedronlist[4*iTetR+3]};

      CanonicalTraceToCell canonicalR = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(tri, 3, tetR, 4);

      // save off the trinagle face index for the trace of the tet
      tetfacemap[iTetR][canonicalR.trace] = face;
    }
  }

  std::cout << "Adding " << ntetcenter << " tet centers" << std::endl;

  // update the tet neigbor information first, this is needed for the new tets
  ntetcenter = 0;
  for ( auto split = split_tets.begin(); split != split_tets.end(); split++ )
  {
    const int itet = *split;

    // update the tet neighbor information with the new tets
    for (int v = 1; v < 4; v++)
    {
      int newitet = grid1.numberoftetrahedra + 3*ntetcenter + v-1;

      // Get the neighboring tet
      int iTetN = grid1.neighborlist[4*itet+v];

      // Skip if on a boundary
      if ( iTetN < 0 ) continue;

      // Find the index pointing to itet, and update it with the new tet index
      for (int n = 0; n < 4; n++)
      {
        if (grid1.neighborlist[4*iTetN+n] == itet)
        {
          out.neighborlist[4*iTetN+n] = newitet;
          break;
        }
      }
    }

    ntetcenter++;
  }


  // Copy back neighbors which is needed to set neighbors of the new tets
  for ( auto split = split_tets.begin(); split != split_tets.end(); split++ )
  {
    const int itet = *split;

    for (int n = 0; n < 4; n++)
      grid1.neighborlist[4*itet+n] = out.neighborlist[4*itet+n];
  }

  // Generate the new points and tets
  ntetcenter = 0;
  for ( auto split = split_tets.begin(); split != split_tets.end(); split++ )
  {
    const int itet = *split;

    const int tet[] = {grid1.tetrahedronlist[4*itet+0], grid1.tetrahedronlist[4*itet+1],
                       grid1.tetrahedronlist[4*itet+2], grid1.tetrahedronlist[4*itet+3]};

    // Add the tetcenters to the list of nodes
    int newpointindex = grid1.numberofpoints + ntetcenter;

    REAL tetcenter[] = {0,0,0};
    for (int v = 0; v < 4; v++)
      for (int n = 0; n < 3; n++)
        tetcenter[n] += grid1.pointlist[3*tet[v] + n];

    for (int n = 0; n < 3; n++)
      out.pointlist[3*newpointindex + n] = tetcenter[n]/4.;

    // Create the new subdivided tetrahedron and update the adjtetlist
    out.tetrahedronlist[4*itet+0] = newpointindex;
    for (int v = 1; v < 4; v++)
    {
      int newtet[4] = {tet[0], tet[1], tet[2], tet[3]};
      newtet[v] = newpointindex;

      int newitet = grid1.numberoftetrahedra + 3*ntetcenter + v-1;

      // add the new tet
      for (int n = 0; n < 4; n++)
        out.tetrahedronlist[4*newitet+n] = newtet[n];

      // update the old adjacency with the new tets
      int face = tetfacemap[itet][v];
      if ( out.adjtetlist[2*face + 0] == itet )
        out.adjtetlist[2*face + 0] = newitet;
      else if ( out.adjtetlist[2*face + 1] == itet )
        out.adjtetlist[2*face + 1] = newitet;
      else
        SANS_ASSERT(false);
    }

    // The original nodes {0-3}, and the new node {4}
    /*
            2
            |\
            | \
            |  \
            |   \
            |    \
            | (4) \
            |      \
            0 ----- 1
           /     .
          /   .
         / .
         3
    */
    // The new tetrahedra are
    // tet0 = {4, 1, 2, 3}
    // tet1 = {0, 4, 2, 3}
    // tet2 = {0, 1, 4, 3}
    // tet3 = {0, 1, 2, 4}
    //
    // The new faces and adjacent tets with face normals pointing to the left tet
    // f0 = {0, 4, 3}, {tet1, tet2}
    // f1 = {1, 4, 3}, {tet0, tet2}
    // f2 = {2, 4, 1}, {tet0, tet3}
    // f3 = {0, 4, 2}, {tet1, tet3}
    // f4 = {0, 4, 1}, {tet3, tet2}
    // f5 = {2, 4, 3}, {tet1, tet0}
    //
    // tet neighbor list
    // tet  -> {n0, n1, n2, n3}  <- original neighbors
    // tet0 -> {n0  , tet1, tet2, tet3}
    // tet1 -> {tet0, n1  , tet2, tet3}
    // tet2 -> {tet0, tet1, n2  , tet3}
    // tet3 -> {tet0, tet1, tet2, n3  }


    int pointindex[] = {tet[0], tet[1], tet[2], tet[3], newpointindex};

    int tetindex[] = {itet,
                      grid1.numberoftetrahedra + 3*ntetcenter + 0,
                      grid1.numberoftetrahedra + 3*ntetcenter + 1,
                      grid1.numberoftetrahedra + 3*ntetcenter + 2};

    int facemap[][3] = {{0,4,3},
                        {1,4,3},
                        {2,4,1},
                        {0,4,2},
                        {0,4,1},
                        {2,4,3}};

    int tetmap[][2] = { {tetindex[1],tetindex[2]},
                        {tetindex[0],tetindex[2]},
                        {tetindex[0],tetindex[3]},
                        {tetindex[1],tetindex[3]},
                        {tetindex[3],tetindex[2]},
                        {tetindex[1],tetindex[0]} };

    for (int f = 0; f < 6; f++)
    {
      int face = grid1.numberoftrifaces + 6*ntetcenter + f;

      // create the triangle indices
      for (int n = 0; n < 3; n++)
        out.trifacelist[3*face + n] = pointindex[facemap[f][n]];

      // set the face adjacency
      for (int n = 0; n < 2; n++)
        out.adjtetlist[2*face + n] = tetmap[f][n];

      // these are by definition interior faces, so the marker is 0
      out.trifacemarkerlist[face] = 0;
    }

    // update the neighbor information
    for (int t = 0; t < 4; t++)
      for (int n = 0; n < 4; n++)
      {
        if ( n == t )
          out.neighborlist[4*tetindex[t]+n] = grid1.neighborlist[4*itet+n];
        else
          out.neighborlist[4*tetindex[t]+n] = tetindex[n];
      }

    ntetcenter++;
  }

  delete [] tetfacemap;


#if 1
  // Checks to assert the connectivity is updated correctly
  for ( int face = 0; face < out.numberoftrifaces; face++ )
  {
    int iTetL = out.adjtetlist[2*face + 0];
    int iTetR = out.adjtetlist[2*face + 1];

    // Triangle nodes
    int tri[] = {out.trifacelist[3*face + 0], out.trifacelist[3*face + 1], out.trifacelist[3*face + 2]};

    // Left tet nodes
    int tetL[] = {out.tetrahedronlist[4*iTetL+0], out.tetrahedronlist[4*iTetL+1],
                  out.tetrahedronlist[4*iTetL+2], out.tetrahedronlist[4*iTetL+3]};

    CanonicalTraceToCell canonicalL = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(tri, 3, tetL, 4);

    SANS_ASSERT_MSG( out.neighborlist[4*iTetL+canonicalL.trace] == iTetR, "face = %d", face );

    if (iTetR >= 0)
    {
      // right tet nodes
      int tetR[] = {out.tetrahedronlist[4*iTetR+0], out.tetrahedronlist[4*iTetR+1],
                    out.tetrahedronlist[4*iTetR+2], out.tetrahedronlist[4*iTetR+3]};

      CanonicalTraceToCell canonicalR = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(tri, 3, tetR, 4);

      SANS_ASSERT_MSG( out.neighborlist[4*iTetR+canonicalR.trace] == iTetL, "face = %d", face );
    }
  }
#endif


  return EGADS_SUCCESS;
#endif
}


} //namespace EGADS
} //namespace SANS
