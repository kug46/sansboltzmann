// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "EGTetGen.h"
#include "Meshing/EGADS/tools.h"
#include "makeTetGenModel.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "Field/XFieldLine_Traits.h"

#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include <memory> //shared_ptr
#include <algorithm>
#include <vector>

namespace SANS
{
namespace EGADS
{

namespace // private to this file
{
typedef DLA::VectorS<3,Real> VectorX;

enum WakeCell
{
  Null,
  Left,
  Right,
};
#if 0
void centroid(const std::vector<int>& tet, const VectorX* DOF, VectorX& TetCenter)
{
  TetCenter = 0;
  for ( int node = 0; node < 4; node++)
    TetCenter += DOF[tet[node]];

  TetCenter /= 4.;
}
#endif

// Updates the nodes of triangles due to node duplication
void updateTetNodes( tetgenio& grid, const std::vector<WakeCell>& wakeCellFlags, const int newPoint,
                     const int iTetL, const int iTetR, const int oldPoint )
{
  SANS_ASSERT( wakeCellFlags[iTetR] == WakeCell::Null || wakeCellFlags[iTetR] == WakeCell::Right );

  bool updateneighbor = false;

  std::vector<int> tetR = {grid.tetrahedronlist[4*iTetR+0], grid.tetrahedronlist[4*iTetR+1],
                           grid.tetrahedronlist[4*iTetR+2], grid.tetrahedronlist[4*iTetR+3]};

  // Update the nodes for a tet right of the wake
  for ( int n = 0; n < 4; n++ )
    if ( tetR[n] == oldPoint )
    {
      grid.tetrahedronlist[4*iTetR + n] = newPoint;
      updateneighbor = true;
    }

  // Only update the neighbors if if a node was updated on the current tet
  if ( updateneighbor )
  {
    // Update the points on neighboring triangles
    for (int side = 0; side < 4; side++ )
    {
      const int iTetN = grid.neighborlist[4*iTetR + side];
      if (iTetN == iTetL || iTetN < 0) continue;  //Don't modify the left tet or step out of bounds on a boundary
      if (wakeCellFlags[iTetN] == WakeCell::Left) continue; // Don't try changing a tet left of the wake

      updateTetNodes( grid, wakeCellFlags, newPoint, iTetR, iTetN, oldPoint );
    }
  }
}

}


EGTetGen::EGTetGen( const EGTessModel& tessModel,
                    const Real maxRadiusEdgeRatio, const Real minDihedralAngle, //TetGen quality parameters
                    const bool potential_adjoint )
{
  tetgenio grid;
  std::set<int> duplicatePoints;
  std::vector<bool> modelFaceKutta;
  makeTetGenModel(tessModel, duplicatePoints, modelFaceKutta, grid,
                  maxRadiusEdgeRatio, minDihedralAngle);

  int nBCFaces = 0;
  int nInteriorFaces = 0;
  int nDuplicatePoints = 0;

  // A vector for tracking which faces require duplication of vertexes
  std::vector<bool> duplicateFace(tessModel.nFaces(), false);

  // Get all the bodies in the model
  std::vector< EGBody<3> > bodies = tessModel.getBodies();
  std::vector< EGEdge<3> > edges = tessModel.getAllEdges();


  const std::vector< int >& solidBodies = tessModel.modelSolidBodyIndex();
  const std::vector< int >& sheetBodies = tessModel.modelSheetBodyIndex();

  std::set<std::string> wakeNames;

  std::map< int, int > KuttaLineCount, TrefftzLineCount;

  // Gather all the wake names from the Trefftz planes
  for ( std::size_t iedge = 0; iedge < edges.size(); iedge++ )
  {
    std::string wakename;
    if ( edges[iedge].hasAttribute(EGTetGen::WAKESHEET) )
    {
      edges[iedge].getAttribute(EGTetGen::WAKESHEET, wakename);
      wakeNames.insert(wakename);
      KuttaLineCount[iedge] = 0;
    }
    else if ( edges[iedge].hasAttribute(EGTetGen::TREFFTZ) )
    {
      edges[iedge].getAttribute(EGTetGen::TREFFTZ, wakename);
      wakeNames.insert(wakename);
      TrefftzLineCount[iedge] = 0;
    }
  }

  std::map<int,int> KuttaFrameGroupIndexes, TrefftzFrameGroupIndexes;
  {
    int igroup = 0;
    for (auto count = KuttaLineCount.begin(); count != KuttaLineCount.end(); count++)
      KuttaFrameGroupIndexes[(*count).first] = igroup++;

    for (auto count = TrefftzLineCount.begin(); count != TrefftzLineCount.end(); count++)
      TrefftzFrameGroupIndexes[(*count).first] = igroup++;
  }

  std::vector< EGFace<3> > modelBCFaces;

  for ( std::size_t ibody = 0; ibody < solidBodies.size(); ibody++ )
  {
    int nface = bodies[solidBodies[ibody]].nFaces();
    nBCFaces += nface; // All solid body faces must be BC faces
    std::vector< EGFace<3> > faces = bodies[solidBodies[ibody]].getFaces();
    modelBCFaces.insert( modelBCFaces.end(), faces.begin(), faces.end() );
  }

  // Check all faces in the sheet bodies
  for ( std::size_t ibody = 0; ibody < sheetBodies.size(); ibody++ )
  {
    const int bodyIndex = sheetBodies[ibody];
    int nface = bodies[bodyIndex].nFaces();
    std::vector< EGFace<3> > faces = bodies[bodyIndex].getFaces();

    for (int j = 0; j < nface; j++)
    {
      // Check if the sheet face is considered a boundary condition
      if ( faces[j].hasAttribute(EGTetGen::BCNAME) )
      {
        std::string BCName;
        faces[j].getAttribute(EGTetGen::BCNAME, BCName);

        // If the BC is a wake sheet, then mark it as a duplication face
        if ( std::find(wakeNames.begin(), wakeNames.end(), BCName) != wakeNames.end() )
        {
          nDuplicatePoints = duplicatePoints.size();
          duplicateFace[nBCFaces] = true;
        }

        modelBCFaces.push_back( faces[j] );
        nBCFaces++;
      }
      else
        nInteriorFaces++;

    }
  }


  const int numberofpoints = grid.numberofpoints;
  const int numberoftrifaces = grid.numberoftrifaces;
  const int numberoftetrahedra = grid.numberoftetrahedra;

  dupPointOffset_ = numberofpoints;

  //Create the DOF arrays
  resizeDOF(numberofpoints + nDuplicatePoints);

  // One Cell group for now...
  resizeCellGroups(1);
  resizeInteriorTraceGroups(nInteriorFaces+1); // +1 for the volume interior trace group
  resizeBoundaryTraceGroups(nBCFaces);

  //Copy over the nodal values
  for ( int i = 0; i < numberofpoints; i++ )
    DOF(i) = {grid.pointlist[3*i+0], grid.pointlist[3*i+1], grid.pointlist[3*i+2]};

  // Duplicate points
  std::map<int,int> newPointMap;         //Map from original points to new points
  invPointMap_.resize(nDuplicatePoints); //Map from new to points to original points
  std::vector<int> invKuttaPoints;
  int ii = 0;
  for (auto point = duplicatePoints.begin(); point != duplicatePoints.end(); point++)
  {
    DOF(numberofpoints+ii) = DOF(*point);
    //DOF(numberofpoints+ii)[2] -= 0.5; // HACK to visualize the duplicate nodes
    newPointMap[*point] = numberofpoints+ii;
    invPointMap_[ii] = *point;
    if ( tessModel.modelTopoType(*point) >= 0 && tessModel.modelTopoFromVertex(*point).hasAttribute(EGTetGen::WAKESHEET) )
    {
      KuttaPoints_.push_back( numberofpoints+ii ); //Save off the trailing edge points to be used with the Kutta condition
      invKuttaPoints.push_back( *point );
    }
    ii++;
  }

  // Count the number of triangles on each face
  std::vector< int > BCFaceTriCount(nBCFaces, 0);
  std::vector< int > IFaceTriCount(nInteriorFaces+1, 0);
  for ( int face = 0; face < numberoftrifaces; face++ )
  {
    int marker = grid.trifacemarkerlist[face];
    if ( marker > 0 )
      BCFaceTriCount[grid.trifacemarkerlist[face]-1]++; // Boundary faces are positive numbers > 0
    else
      IFaceTriCount[ -marker ]++;                       // Interior faces are 0 or a negative number
  }

  // volume field variable
  FieldCellGroupType<Tet>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionVolumeBase<Tet>::HierarchicalP1, numberoftetrahedra );

  std::vector< FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType > fldAssocIface(nInteriorFaces+1);

  for ( int I = 0; I < nInteriorFaces+1; I++ ) // +1 for the volume interior trace group
  {
    fldAssocIface[I].resize( BasisFunctionAreaBase<Triangle>::HierarchicalP1, IFaceTriCount[I] );

    //Only one cell group for the interior trace group
    fldAssocIface[I].setGroupLeft ( 0 );
    fldAssocIface[I].setGroupRight( 0 );

    IFaceTriCount[I] = 0;
  }

  //Create the boundary association constructors
  std::vector< FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType > fldAssocBface(nBCFaces);

  for ( int BC = 0; BC < nBCFaces; BC++ )
  {
    fldAssocBface[BC].resize(BasisFunctionAreaBase<Triangle>::HierarchicalP1, BCFaceTriCount[BC]);

    //Only one cell group for the interior trace group
    fldAssocBface[BC].setGroupLeft ( 0 );
    if ( duplicateFace[BC] )
      fldAssocBface[BC].setGroupRight( 0 );

    BCFaceTriCount[BC] = 0;
  }

  // A trianglular field element used to check normal vectors
  FieldTraceGroupType<Triangle>::ElementType<> elemTrace( BasisFunctionAreaBase<Triangle>::HierarchicalP1 );
  FieldTraceGroupType<Triangle>::ElementType<>::RefCoordType sRef = Triangle::centerRef;
  FieldTraceGroupType<Triangle>::ElementType<>::VectorX Ntri, Nface, X, TetCenter, CenterVector;

  const int (*TraceNodes)[ Triangle::NTrace ] = TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes;
  const int (*FrameNodes)[ Line::NTrace ] = TraceToCellRefCoord<Line, TopoD2, Triangle>::TraceNodes;

  // Vector to color cells where they are relative to wakes
  std::vector<WakeCell> wakeCellFlags(numberoftetrahedra, WakeCell::Null);

  for ( int face = 0; face < numberoftrifaces; face++ )
  {
    int iTetL = grid.adjtetlist[2*face + 0];
    int iTetR = grid.adjtetlist[2*face + 1];

    const int marker = grid.trifacemarkerlist[face];
    if ( iTetR >= 0 && marker > 0) //Interior boundary face
    {
      // Triangle nodes
      std::array<int,3> tri = {{grid.trifacelist[3*face + 0], grid.trifacelist[3*face + 1], grid.trifacelist[3*face + 2]}};

      // Count segments that are on Kutta or Trefftz edges.
      for (int seg = 0; seg < 3; seg ++)
      {
        if ( tessModel.isEdgeSegment(tri,seg) )
        {
          int iedge = tessModel.getEdgeIndexFromSegment(tri,seg);
          if ( edges[iedge].hasAttribute(EGTetGen::WAKESHEET) )
            KuttaLineCount[iedge] += 1;
          else if ( edges[iedge].hasAttribute(EGTetGen::TREFFTZ) )
            TrefftzLineCount[iedge] += 1;
        }
      }

      const int BC = marker-1;

      //Create the face element
      for ( int node = 0; node < 3; node++)
        elemTrace.DOF(node) = DOF(tri[node]);

      // Ntri always points towards the left tet
      elemTrace.unitNormal( sRef, Ntri );
      elemTrace.coordinates( sRef, X );

#if 0
      // Left tet nodes
      std::vector<int> tetL = {grid.tetrahedronlist[4*iTetL+0], grid.tetrahedronlist[4*iTetL+1],
                               grid.tetrahedronlist[4*iTetL+2], grid.tetrahedronlist[4*iTetL+3]};

      //Calculate the centroid of the left tet and the vector pointing to the center from the center of the triangle
      centroid(tetL, DOF_, TetCenter);
      CenterVector = TetCenter - X;

      SANS_ASSERT( dot(Ntri, CenterVector) > 0 );
#endif

      Nface = modelBCFaces[BC].unitNormal( X );

      // Positive Ntri dot Nface means left tet is on right side of the face
      if ( dot(Ntri, Nface) > 0 )
        std::swap(iTetL, iTetR);  // Swap tetL and tetR so they are always on the same side of the BC

      wakeCellFlags[iTetL] = WakeCell::Left;
      wakeCellFlags[iTetR] = WakeCell::Right;
    }
  }


  for ( int face = 0; face < numberoftrifaces; face++ )
  {
    int iTetL = grid.adjtetlist[2*face + 0];
    int iTetR = grid.adjtetlist[2*face + 1];

    const int marker = grid.trifacemarkerlist[face];
    if ( iTetR >= 0 && marker > 0) //Interior boundary face
    {
      const int BC = marker-1;

      // Triangle nodes
      std::array<int,3> tri = {{grid.trifacelist[3*face + 0], grid.trifacelist[3*face + 1], grid.trifacelist[3*face + 2]}};

      //Create the face element
      for ( int node = 0; node < 3; node++)
        elemTrace.DOF(node) = DOF(tri[node]);

      // Ntri always points towards the left tet
      elemTrace.unitNormal( sRef, Ntri );
      elemTrace.coordinates( sRef, X );

#if 0
      // Left tet nodes
      std::vector<int> tetL = {grid.tetrahedronlist[4*iTetL+0], grid.tetrahedronlist[4*iTetL+1],
                               grid.tetrahedronlist[4*iTetL+2], grid.tetrahedronlist[4*iTetL+3]};

      //Calculate the centroid of the left tet and the vector pointing to the center from the center of the triangle
      centroid(tetL, DOF_, TetCenter);
      CenterVector = TetCenter - X;

      SANS_ASSERT( dot(Ntri, CenterVector) > 0 );
#endif

      Nface = modelBCFaces[BC].unitNormal( X );

      // Positive Ntri dot Nface means left tet is on right side of the face
      if ( dot(Ntri, Nface) > 0 )
        std::swap(iTetL, iTetR);  // Swap tetL and tetR so they are always on the same side of the BC

      for ( int node = 0; node < 3; node++)
      {
        auto newPoint = newPointMap.find(tri[node]);
        if ( newPoint != newPointMap.end() )
          updateTetNodes( grid, wakeCellFlags, newPoint->second, iTetL, iTetR, tri[node] );
      }
    }
  }



  //element volume associativity after duplicate nodes have been updated
  for ( int iTet = 0; iTet < numberoftetrahedra; iTet++ )
  {
    fldAssocCell.setAssociativity( iTet ).setRank( 0 );
    fldAssocCell.setAssociativity( iTet ).setNodeGlobalMapping( grid.tetrahedronlist + 4*iTet, 4 );
  }

  //find all tets that have a Kutta point
  for ( int iTet = 0; iTet < numberoftetrahedra; iTet++ )
  {
    int *tet = grid.tetrahedronlist + 4*iTet;

    for ( int node = 0; node < 4; node++)
    {
      if ( std::find(invKuttaPoints.begin(), invKuttaPoints.end(), tet[node]) != invKuttaPoints.end() )
      {
        KuttaCellElemLeft_[0][tet[node]].KuttaPoint = newPointMap.at(tet[node]);
        KuttaCellElemLeft_[0][tet[node]].elems.push_back(iTet);
      }

      if ( std::find(KuttaPoints_.begin(), KuttaPoints_.end(), tet[node]) != KuttaPoints_.end() )
      {
        KuttaCellElemRight_[0][tet[node]].KuttaPoint = tet[node];
        KuttaCellElemRight_[0][tet[node]].elems.push_back(iTet);
      }
    }
  }

  //Create the boundary association constructors
  std::vector< FieldFrameGroupType::FieldAssociativityConstructorType >
      fldAssocBframe(KuttaFrameGroupIndexes.size() + TrefftzFrameGroupIndexes.size());

  for ( auto count = KuttaLineCount.begin(); count != KuttaLineCount.end(); count++ )
  {
    fldAssocBframe[KuttaFrameGroupIndexes[(*count).first]].resize(BasisFunctionLineBase::HierarchicalP1, (*count).second);
    (*count).second = 0;
  }

  std::cout << "Trefftz Frames : ";
  for ( auto count = TrefftzLineCount.begin(); count != TrefftzLineCount.end(); count++ )
  {
    std::cout << TrefftzFrameGroupIndexes[(*count).first] << " ";
    fldAssocBframe[TrefftzFrameGroupIndexes[(*count).first]].resize(BasisFunctionLineBase::HierarchicalP1, (*count).second);
    (*count).second = 0;
  }
  std::cout << std::endl;


  // Used to get the original trace element numbering on each face
  std::map<UniqueElem,int> traceIDMap = tessModel.getUniqueElemMap();

  // trace associativity
  for ( int face = 0; face < numberoftrifaces; face++ )
  {
    int iTetL = grid.adjtetlist[2*face + 0];
    int iTetR = grid.adjtetlist[2*face + 1];

    // Triangle nodes
    std::vector<int> tri = {grid.trifacelist[3*face + 0], grid.trifacelist[3*face + 1], grid.trifacelist[3*face + 2]};
    // Left tet nodes
    std::vector<int> tetL = {grid.tetrahedronlist[4*iTetL+0], grid.tetrahedronlist[4*iTetL+1],
                             grid.tetrahedronlist[4*iTetL+2], grid.tetrahedronlist[4*iTetL+3]};

    std::vector<int> tetLorig = tetL;

    // Find the original node numbers before duplication
    for ( int node = 0; node < 4; node++)
      if (tetLorig[node] >= numberofpoints)
        tetLorig[node] = invPointMap_[tetLorig[node]-numberofpoints];

    const int marker = grid.trifacemarkerlist[face];

    if ( iTetR >= 0 && marker <= 0) //Volume Interior triangle, or interior face
    {
      const int I = -marker;
      const int faceCount = IFaceTriCount[I];

      std::vector<int> tetR = {grid.tetrahedronlist[4*iTetR+0], grid.tetrahedronlist[4*iTetR+1],
                               grid.tetrahedronlist[4*iTetR+2], grid.tetrahedronlist[4*iTetR+3]};
      std::vector<int> tetRorig = tetR;
      for ( int node = 0; node < 4; node++)
        if (tetRorig[node] >= numberofpoints)
          tetRorig[node] = invPointMap_[tetRorig[node]-numberofpoints];

      fldAssocIface[I].setElementLeft ( iTetL, faceCount );
      fldAssocIface[I].setElementRight( iTetR, faceCount );

      int triL[3];
      // Get the left canonical triangle original nodes
      CanonicalTraceToCell canonicalL = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTraceLeft(&tri[0], tri.size(),
                                                                                                          &tetLorig[0], tetLorig.size(),
                                                                                                          triL, 3);

      CanonicalTraceToCell canonicalR = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(triL, 3,
                                                                                                      &tetRorig[0], tetRorig.size());

      // Get the canonical triangle with the updated tetrahedral nodes
      for ( int n = 0; n < 3; n++)
        triL[n] = tetL[TraceNodes[canonicalL.trace][n]];

      // Face signs for elements (L is +, R is -)
      fldAssocCell.setAssociativity( iTetL ).setFaceSign( +1, canonicalL.trace );
      fldAssocCell.setAssociativity( iTetR ).setFaceSign( canonicalR.orientation, canonicalR.trace );

      // Set the canonical faces
      fldAssocIface[I].setCanonicalTraceLeft( canonicalL, faceCount );
      fldAssocIface[I].setCanonicalTraceRight( canonicalR, faceCount );

      fldAssocIface[I].setAssociativity( faceCount ).setRank( 0 );

      //Set the nodes for the triangle face
      fldAssocIface[I].setAssociativity( faceCount ).setNodeGlobalMapping( triL, 3 );

      IFaceTriCount[I]++;
    }
    else if ( iTetR >= 0 && marker > 0) //Interior boundary face
    {
      const int BC = marker-1;
      int itrace = -1;
      try
      {
        itrace = traceIDMap.at(UniqueElem(eTriangle, tri));
      }
      catch (const std::exception& e)
      {
        SANS_DEVELOPER_EXCEPTION("Could not find triangle (%d, %d, %d)", tri[0], tri[1], tri[2]);
      }

      //Create the face element
      for ( int node = 0; node < 3; node++)
        elemTrace.DOF(node) = DOF(tri[node]);

      // Ntri always points towards the left tet (reverse of what we want in SANS)
      elemTrace.unitNormal( sRef, Ntri );
      elemTrace.coordinates( sRef, X );

      Nface = modelBCFaces[BC].unitNormal( X );

#if 0
      //Calculate the centroid of the left tet and the vector pointing to the center from the center of the triangle
      centroid(tetL, DOF_, TetCenter);
      CenterVector = TetCenter - X;

      SANS_ASSERT( dot(Ntri, CenterVector) > 0 );
#endif

      // Positive Ntri dot Nface means left tet is on right side of the face
      if ( dot(Ntri, Nface) > 0 )
      {
        // Swap tetL and tetR so they are always on the same side of the BC
        std::swap(iTetL, iTetR);

        tetL = {grid.tetrahedronlist[4*iTetL+0], grid.tetrahedronlist[4*iTetL+1],
                grid.tetrahedronlist[4*iTetL+2], grid.tetrahedronlist[4*iTetL+3]};

        tetLorig = tetL;
        for ( int node = 0; node < 4; node++)
          if (tetLorig[node] >= numberofpoints)
            tetLorig[node] = invPointMap_[tetLorig[node]-numberofpoints];
      }

      std::vector<int> tetR = {grid.tetrahedronlist[4*iTetR+0], grid.tetrahedronlist[4*iTetR+1],
                               grid.tetrahedronlist[4*iTetR+2], grid.tetrahedronlist[4*iTetR+3]};
      std::vector<int> tetRorig = tetR;
      for ( int node = 0; node < 4; node++)
        if (tetRorig[node] >= numberofpoints)
          tetRorig[node] = invPointMap_[tetRorig[node]-numberofpoints];


      std::array<int,3> triOrigL;
      // Get the left canonical triangle nodes
      CanonicalTraceToCell canonicalL = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTraceLeft(&tri[0], tri.size(),
                                                                                                          &tetLorig[0], tetLorig.size(),
                                                                                                          &triOrigL[0], 3);

      // Get the left and right canonical faces
      CanonicalTraceToCell canonicalR = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTrace(&triOrigL[0], 3,
                                                                                                      &tetRorig[0], tetRorig.size());


      // Get the canonical triangle with the updated tetrahedral nodes
      std::array<int,3> triL;
      for ( int n = 0; n < 3; n++)
        triL[n] = tetL[TraceNodes[canonicalL.trace][n]];

      fldAssocBface[BC].setElementLeft ( iTetL, itrace );
      fldAssocBface[BC].setElementRight( iTetR, itrace );

      // Face signs for elements (L is +, R is -)
      fldAssocCell.setAssociativity( iTetL ).setFaceSign( +1, canonicalL.trace );
      fldAssocCell.setAssociativity( iTetR ).setFaceSign( canonicalR.orientation, canonicalR.trace );

      // Set the canonical faces
      fldAssocBface[BC].setCanonicalTraceLeft( canonicalL, itrace );
      fldAssocBface[BC].setCanonicalTraceRight( canonicalR, itrace );

      fldAssocBface[BC].setAssociativity( itrace ).setRank( 0 );

      //Set the nodes for the triangle face
      fldAssocBface[BC].setAssociativity( itrace ).setNodeGlobalMapping( triL.data(), 3 );


      for (int seg = 0; seg < 3; seg ++)
      {
        int iedge = -1;

        if ( tessModel.isEdgeSegment(triOrigL,seg) )
        {
          iedge = tessModel.getEdgeIndexFromSegment(triOrigL,seg);
          if ( !edges[iedge].hasAttribute(EGTetGen::TREFFTZ) &&
               !(edges[iedge].hasAttribute(EGTetGen::WAKESHEET) && potential_adjoint) )
            continue; // Not a Trefftz segment
        }
        else
          continue; // Not an Edge segment

        // The canonical Frame is the seg number and always left so orientation is 1
        CanonicalTraceToCell canonicalFrameL(seg,1);
        int BCframe;

        if (edges[iedge].hasAttribute(EGTetGen::WAKESHEET))
          BCframe = KuttaFrameGroupIndexes[iedge];
        else
          BCframe = TrefftzFrameGroupIndexes[iedge];
        const int lineCount = TrefftzLineCount[iedge];

        // Get the nodes with the triangle with the updated nodes
        int lineL[2] = {triL[FrameNodes[seg][0]], triL[FrameNodes[seg][1]]};

        fldAssocBframe[BCframe].setGroupLeft( BC );
        fldAssocBframe[BCframe].setElementLeft( itrace, lineCount );

        fldAssocBframe[BCframe].setCanonicalTraceLeft( canonicalFrameL, lineCount );

        fldAssocBframe[BCframe].setAssociativity( lineCount ).setRank( 0 );

        //Set the nodes for the line
        fldAssocBframe[BCframe].setAssociativity( lineCount ).setNodeGlobalMapping( lineL, 2 );

        TrefftzLineCount[iedge]++;
      }

      BCFaceTriCount[BC]++;
    }
    else  //Now we are on a boundary.
    {
      const int BC = marker-1;
      int itrace = -1;
      try
      {
        itrace = traceIDMap.at(UniqueElem(eTriangle, tri));
      }
      catch (const std::exception& e)
      {
        SANS_DEVELOPER_EXCEPTION("Could not find triangle (%d, %d, %d)", tri[0], tri[1], tri[2]);
      }

      std::array<int,3> triOrigL;
      // Get the left canonical triangle nodes
      CanonicalTraceToCell canonicalL = TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTraceLeft(&tri[0], tri.size(),
                                                                                                  &tetLorig[0], tetLorig.size(),
                                                                                                  &triOrigL[0], 3);

      // Get the canonical triangle with the updated tetrahedral nodes
      std::array<int,3> triL;
      WakeCell BCframSide = WakeCell::Left;
      for ( int n = 0; n < 3; n++)
      {
        triL[n] = tetL[TraceNodes[canonicalL.trace][n]];
        if ( triL[n] >= numberofpoints )
          BCframSide = WakeCell::Right;
      }

      fldAssocBface[BC].setElementLeft ( iTetL, itrace );

      // Face signs for elements (L is +, R is -)
      fldAssocCell.setAssociativity( iTetL ).setFaceSign( +1, canonicalL.trace );

      // Set the canonical faces
      fldAssocBface[BC].setCanonicalTraceLeft( canonicalL, itrace );

      fldAssocBface[BC].setAssociativity( itrace ).setRank( 0 );

      //Set the nodes for the triangle face
      fldAssocBface[BC].setAssociativity( itrace ).setNodeGlobalMapping( triL.data(), 3 );


      // Wake sheets cannot be BC faces for the kutta condition
      if ( !modelBCFaces[BC].hasAttribute(EGTetGen::WAKESHEET) && nDuplicatePoints > 0 )
      {
        // Only include faces that are connected to Kutta edges
        if (modelFaceKutta[BC] )
        {
          for (int n = 0; n < Triangle::NNode; n ++)
          {
            if ( tessModel.modelTopoType(triOrigL[n]) >= 0 &&
                 tessModel.modelTopoFromVertex(triOrigL[n]).hasAttribute(EGTetGen::WAKESHEET) )
            {
              if ( BCframSide == WakeCell::Left )
              {
                SANS_ASSERT(triL[n] == triOrigL[n]);
                KuttaBCTraceElemLeft_[BC][triL[n]].KuttaPoint = newPointMap.at(triL[n]);
                KuttaBCTraceElemLeft_[BC][triL[n]].elems.push_back(itrace);
              }
              else
              {
                KuttaBCTraceElemRight_[BC][triL[n]].KuttaPoint = triL[n];
                KuttaBCTraceElemRight_[BC][triL[n]].elems.push_back(itrace);
              }
            }
          }
        }

        for (int seg = 0; seg < Triangle::NEdge; seg ++)
        {
          int iedge;

          if ( tessModel.isEdgeSegment(triOrigL, seg) )
          {
            iedge = tessModel.getEdgeIndexFromSegment(triOrigL,seg);
            if ( potential_adjoint || !edges[iedge].hasAttribute(EGTetGen::WAKESHEET) )
              continue; // Not a Kutta segment
          }
          else
            continue; // Not an Edge segment

          const int BCframe = KuttaFrameGroupIndexes[iedge];

          // Only use one BC face to set the frame connectivity
          if ( BCframSide == WakeCell::Left )
          {
            const int lineCount = KuttaLineCount[iedge];

            fldAssocBframe[BCframe].setAssociativity( lineCount ).setRank( 0 );

            // Get the nodes with the triangle with the updated nodes
            int lineL[2] = {triL[FrameNodes[seg][0]], triL[FrameNodes[seg][1]]};

            //Set the nodes for the line
            fldAssocBframe[BCframe].setAssociativity( lineCount ).setNodeGlobalMapping( lineL, 2 );

            // The canonical Frame is the seg number and always left so orientation is 1
            CanonicalTraceToCell canonicalFrameL(seg,1);

            fldAssocBframe[BCframe].setGroupLeft( BC );
            fldAssocBframe[BCframe].setElementLeft( itrace, lineCount );

            fldAssocBframe[BCframe].setCanonicalTraceLeft( canonicalFrameL, lineCount );

            KuttaLineCount[iedge]++;
          }
          else
            fldAssocBframe[BCframe].setGroupRight( BC );
        }
      }

      BCFaceTriCount[BC]++;
    }
  }

  // Zipper up boundary frames
  for (std::size_t BCframe = 0; BCframe < fldAssocBframe.size(); BCframe++)
  {
    //Only zipper up if there is a right group
    if (!fldAssocBframe[BCframe].isGroupRightSet()) continue;

    const int BCR = fldAssocBframe[BCframe].getGroupRight();

    for (int elemTri = 0; elemTri < fldAssocBface[BCR].nElem(); elemTri++)
    {
      int triR[3];
      fldAssocBface[BCR].getAssociativity( elemTri ).getNodeGlobalMapping( triR, 3 );

      std::array<int,3> triOrigR;
      WakeCell BCframSide = WakeCell::Left;
      for ( int node = 0; node < 3; node++)
      {
        triOrigR[node] = triR[node];
        if (triOrigR[node] >= numberofpoints)
        {
          BCframSide = WakeCell::Right;
          triOrigR[node] = invPointMap_[triR[node]-numberofpoints];
        }
      }

      // Only try zippering with right triangles
      if (BCframSide == WakeCell::Left) continue;

      for (int seg = 0; seg < 3; seg ++)
      {
        if ( tessModel.isEdgeSegment(triOrigR, seg) )
        {
          int iedge = tessModel.getEdgeIndexFromSegment(triOrigR,seg);
          if ( !edges[iedge].hasAttribute(EGTetGen::WAKESHEET) )
            continue; // Not a Kutta segment
        }
        else
          continue; // Not an Edge segment


        int lineR[2] = {triOrigR[FrameNodes[seg][0]], triOrigR[FrameNodes[seg][1]]};

        // Find the line element that matches the right triangle line
        for (int elemLine = 0; elemLine < fldAssocBframe[BCframe].nElem(); elemLine++)
        {
          // Get the nodes for the line
          int line[2];
          fldAssocBframe[BCframe].getAssociativity( elemLine ).getNodeGlobalMapping( line, 2 );

          for ( int node = 0; node < 2; node++)
          {
            if (line[node] >= numberofpoints)
              line[node] = invPointMap_[line[node]-numberofpoints];
          }

          if ( ( line[0] == lineR[0] && line[1] == lineR[1] ) ||
               ( line[1] == lineR[0] && line[0] == lineR[1] ) )
          {
#if 0
            std::cout << "elemLine " << elemLine << std::endl;
            std::cout << "line  = {" << line[0] << ", " << line[1] << "}" << std::endl;
            std::cout << "lineR = {" << lineR[0] << ", " << lineR[1] << "}" << std::endl;
#endif
            // Get the right info
            CanonicalTraceToCell canonicalFrameR = TraceToCellRefCoord<Line, TopoD2, Triangle>::getCanonicalTrace(line, 2, &triOrigR[0], 3);

            fldAssocBframe[BCframe].setElementRight( elemTri, elemLine );
            fldAssocBframe[BCframe].setCanonicalTraceRight( canonicalFrameR, elemLine );
            break;
          }
        }
      }
    }
  }


  // Create cell group
  cellGroups_[0] = new FieldCellGroupType<Tet>( fldAssocCell );
  cellGroups_[0]->setDOF(DOF_, nDOF_);
  nElem_ += fldAssocCell.nElem();

  // Create the interior group
  for ( int I = 0; I < nInteriorFaces+1; I++ )
  {
    interiorTraceGroups_[I] = new FieldTraceGroupType<Triangle>( fldAssocIface[I] );
    interiorTraceGroups_[I]->setDOF(DOF_, nDOF_);
  }

  // Create the boundary groups
  for ( int BC = 0; BC < nBCFaces; BC++ )
  {
    boundaryTraceGroups_[BC] = new FieldTraceGroupType<Triangle>( fldAssocBface[BC] );
    boundaryTraceGroups_[BC]->setDOF(DOF_, nDOF_);
  }

  resizeBoundaryFrameGroups( fldAssocBframe.size() );

  // Create the boundary frame groups
  for ( std::size_t BCframe = 0; BCframe < fldAssocBframe.size(); BCframe++ )
  {
    boundaryFrameGroups_[BCframe] = new FieldFrameGroupType( fldAssocBframe[BCframe] );
    boundaryFrameGroups_[BCframe]->setDOF(DOF_, nDOF_);
  }

  // Check that the grid is correct
  checkGrid();
}


}
}
