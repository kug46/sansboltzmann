// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EG_MAKETETGENMODEL_H
#define EG_MAKETETGENMODEL_H

#include <map>
#include <set>

#include "Meshing/EGADS/EGModel.h"
#include "Meshing/EGTess/EGTessModel.h"

#define TETLIBRARY
#include "tetgen.h" // Defined tetgenio, tetrahedralize().

namespace SANS
{
namespace EGADS
{

/*
 *  calculates and returns a TetGen mesh
 *
 */
int
makeTetGenModel(const EGModel<3>& model, std::vector< int > modelBodyIndex[2], std::map<int, std::set<int> >& duplicatePoints,
                tetgenio &out,
                const std::vector<double>& EGADSparams = {0.025, 0.001, 15.0},
                Real maxRadiusEdgeRatio = 2,
                Real minDihedralAngle = 0 );

int
makeTetGenModel(const EGTessModel& tessModel,
                std::set<int>& duplicatePoints,
                std::vector<bool>& modelFaceKutta,
                tetgenio &out,
                Real maxRadiusEdgeRatio = 2,
                Real minDihedralAngle = 0 );

} //namespace EGADS
} //namespace SANS

#endif //EG_MAKETETGENMODEL_H
