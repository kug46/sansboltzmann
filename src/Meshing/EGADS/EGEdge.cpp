// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "EGEdge.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/tools/norm.h"

namespace SANS
{
namespace EGADS
{

template<int Dim>
EGEdge<Dim>::EGEdge(const EGObject& edge) : EGTopologyBase(edge), range_(0), mtype_(NOMTYPE)
{
  ego ref;
  int oclass, mtype, nchild;
  double trange[4];
  ego* pchldrn;
  int *psens;

  //Check that the ego is the correct oclass
  EG_STATUS( EG_getTopology( obj_, &ref, &oclass, &mtype, trange, &nchild, &pchldrn, &psens ) );

  SANS_ASSERT_MSG( oclass == EDGE, "Class should be EDGE: oclass = %d", oclass );

  //Set the range_ of the edge
  range_[0] = trange[0];
  range_[1] = trange[1];

  // Set the type
  mtype_ = mtype;
}

template<int Dim>
EGEdge<Dim>::EGEdge(const EGNode<Dim>& n0, const EGNode<Dim>& n1)
  : EGEdge( std::vector<EGNode<Dim>>{n0,n1} )
{
}

template<>
EGEdge<2>::EGEdge(const std::vector< EGNode<Dim> >& nodes)
  : EGTopologyBase(nodes[0].getContext()), range_(0), mtype_(NOMTYPE)
{
  SANS_ASSERT( nodes.size() == 2 );

  const EGNode<Dim>& n0 = nodes[0];
  const EGNode<Dim>& n1 = nodes[1];

  CartCoord X0 = n0;
  CartCoord X1 = n1;

  //Create the geometric infinite line
  EGLine<Dim> line(n0, n1);

  double dum[3];
  double xyz0[3] = {X0[0], X0[1], 0};
  double xyz1[3] = {X1[0], X1[1], 0};

  //Get the parametric range_ of the line
  EG_STATUS( EG_invEvaluate(line, xyz0, &range_[0], dum) );
  EG_STATUS( EG_invEvaluate(line, xyz1, &range_[1], dum) );

  ego objs[2] = {n0, n1};

  mtype_ = TWONODE;

  //Create the edge
  EG_STATUS( EG_makeTopology(context_, line, EDGE, mtype_, &range_[0], 2, objs, NULL, &obj_) );
}

template<>
EGEdge<3>::EGEdge(const std::vector< EGNode<Dim> >& nodes)
  : EGTopologyBase(nodes[0].getContext()), range_(0), mtype_(NOMTYPE)
{
  SANS_ASSERT( nodes.size() == 2 );

  const EGNode<Dim>& n0 = nodes[0];
  const EGNode<Dim>& n1 = nodes[1];

  CartCoord X0 = n0;
  CartCoord X1 = n1;

  //Create the geometric infinite line
  EGLine<Dim> line(n0, n1);

  double dum[3];
  double xyz0[3] = {X0[0], X0[1], X0[2]};
  double xyz1[3] = {X1[0], X1[1], X1[2]};

  //Get the parametric range_ of the line
  EG_STATUS( EG_invEvaluate(line, xyz0, &range_[0], dum) );
  EG_STATUS( EG_invEvaluate(line, xyz1, &range_[1], dum) );

  ego objs[2] = {n0, n1};

  mtype_ = TWONODE;

  //Create the edge
  EG_STATUS( EG_makeTopology(context_, line, EDGE, mtype_, &range_[0], 2, objs, NULL, &obj_) );
}


template<>
EGEdge<2>::EGEdge(const EGCircle<Dim>& circle)
  : EGTopologyBase(circle.getContext()), range_(0)
{
  //Get the parametric range_ of the circle
  int per;
  EG_STATUS( EG_getRange(circle, &range_[0], &per) );

  //Get the radius
  int oclass, mtype;
  ego rGeom;
  int *pinfo;
  double *prv;
  EG_STATUS( EG_getGeometry(circle, &oclass, &mtype, &rGeom, &pinfo, &prv) );

  //Create one node on the circle
  EGNode<Dim> n0(context_, {prv[0]+prv[9],prv[1]});

  //Free up the memory
  EG_free(pinfo);
  EG_free(prv);

  mtype_ = ONENODE;

  //Create the edge
  EG_STATUS( EG_makeTopology(context_, circle, EDGE, mtype_, &range_[0], 1, n0, NULL, &obj_) );
}

template<>
EGEdge<3>::EGEdge(const EGCircle<Dim>& circle)
  : EGTopologyBase(circle.getContext()), range_(0)
{
  //Get the parametric range_ of the circle
  int per;
  EG_STATUS( EG_getRange(circle, &range_[0], &per) );

  //Get the radius
  int oclass, mtype;
  ego rGeom;
  int *pinfo;
  double *prv;
  EG_STATUS( EG_getGeometry(circle, &oclass, &mtype, &rGeom, &pinfo, &prv) );

  CartCoord   X = {prv[0], prv[1], prv[2]};
  CartCoord dX1 = {prv[3], prv[4], prv[5]};
  Real r = prv[9];

  dX1 /= norm(dX1, 2); // normalize

  //Create one node on the circle
  EGNode<Dim> n0(context_, X + dX1*r);

  //Free up the memory
  EG_free(pinfo);
  EG_free(prv);

  mtype_ = ONENODE;

  //Create the edge
  EG_STATUS( EG_makeTopology(context_, circle, EDGE, mtype_, &range_[0], 1, n0, NULL, &obj_) );
}

template<int Dim>
EGEdge<Dim>::EGEdge(const EGApproximate<Dim>& spline)
  : EGTopologyBase(spline.getContext()), range_({0,1})
{
  //This assumes that the spline is closed so only one node is needed at the end points
  EGNode<Dim> n0(context_, spline(range_[0]));

  mtype_ = ONENODE;

  //Create the edge
  EG_STATUS( EG_makeTopology(spline.getContext(), spline, EDGE, mtype_, &range_[0], 1, n0, NULL, &obj_) );
}

template<int Dim>
EGEdge<Dim>::EGEdge(const EGApproximate<Dim>& spline, const EGNode<Dim>& n0)
  : EGTopologyBase(spline.getContext()), range_({0,1})
{
  //This assumes that the spline is closed so only one node is needed at the end points

  mtype_ = ONENODE;

  //Create the edge
  EG_STATUS( EG_makeTopology(spline.getContext(), spline, EDGE, mtype_, &range_[0], 1, n0, NULL, &obj_) );
}

template<int Dim>
EGEdge<Dim>::EGEdge(const EGApproximate<Dim>& spline, const ParamRange& trange)
  : EGTopologyBase(spline.getContext()), range_(trange)
{
  EGNode<Dim> n0(context_, spline(trange[0]));
  EGNode<Dim> n1(context_, spline(trange[1]));

  ego cldrn[2] = {n0, n1};
  mtype_ = TWONODE;

  //Create the edge
  EG_STATUS( EG_makeTopology(context_, spline, EDGE, mtype_, &range_[0], 2, cldrn, NULL, &obj_) );
}

template<int Dim>
EGEdge<Dim>::EGEdge(const EGApproximate<Dim>& spline, const EGNode<Dim>& n0, const EGNode<Dim>& n1)
  : EGTopologyBase(spline.getContext()), range_(0)
{
  CartCoord X0 = n0;
  CartCoord X1 = n1;

  double dum[3];
  double xyz0[3] = {X0[0], X0[1], Dim == 2 ? 0 : X0[2]};
  double xyz1[3] = {X1[0], X1[1], Dim == 2 ? 0 : X1[2]};

  //Get the parametric range_ of the line
  EG_STATUS( EG_invEvaluate(spline, xyz0, &range_[0], dum) );

  Real dist2 = 0;
  for (int i = 0; i < Dim; i++)
    dist2 = (dum[i]-xyz0[i])*(dum[i]-xyz0[i]);

  SANS_ASSERT( sqrt(dist2) < 1e-10 );

  EG_STATUS( EG_invEvaluate(spline, xyz1, &range_[1], dum) );

  for (int i = 0; i < Dim; i++)
    dist2 = (dum[i]-xyz1[i])*(dum[i]-xyz1[i]);

  SANS_ASSERT( sqrt(dist2) < 1e-10 );

  ego cldrn[2] = {n0, n1};
  mtype_ = TWONODE;

  //Create the edge
  EG_STATUS( EG_makeTopology(context_, spline, EDGE, mtype_, &range_[0], 2, cldrn, NULL, &obj_) );
}

template<int Dim>
EGEdge<Dim>::EGEdge(const EGApproximate<Dim>& spline, const ParamRange& trange, const EGNode<Dim>& n0, const EGNode<Dim>& n1)
  : EGTopologyBase(spline.getContext()), range_(trange)
{
  ego cldrn[2] = {n0, n1};
  mtype_ = TWONODE;

  //Create the edge
  EG_STATUS( EG_makeTopology(context_, spline, EDGE, mtype_, &range_[0], 2, cldrn, NULL, &obj_) );
}


template<int Dim_>
std::vector< EGNode<Dim_> >
EGEdge<Dim_>::getNodes() const
{
  ego ref;
  int oclass, mtype, nchild;
  double data[4];
  ego* pchldrn;
  int *psens;

  EG_STATUS( EG_getTopology( *this, &ref, &oclass, &mtype, data, &nchild, &pchldrn, &psens ) );

  SANS_ASSERT( oclass == EDGE );

  std::vector< EGNode<Dim> > nodes;
  for (int i = 0; i < nchild; i++)
    nodes.emplace_back( EGObject(pchldrn[i]) );

  nodes.shrink_to_fit();
  return nodes;
}


template<int Dim>
typename EGEdge<Dim>::CartCoord
EGEdge<Dim>::operator()( const Real t ) const
{
  double xyz[9];
  EG_STATUS( EG_evaluate(obj_, &t, xyz) );
  return CartCoord(xyz, Dim);
}

template<int Dim>
typename EGEdge<Dim>::JType
EGEdge<Dim>::J( const Real t ) const
{
  double xyz[9];
  EG_STATUS( EG_evaluate(obj_, &t, xyz) );

  //The first derivatives are the jacobian
  JType dXdt(&xyz[3], Dim);

  return dXdt;
}


template<int Dim>
typename EGEdge<Dim>::JinvType
EGEdge<Dim>::Jinv( const Real t ) const
{
  double xyz[9];
  EG_STATUS( EG_evaluate(obj_, &t, xyz) );

  //Get the first derivatives
  DLA::VectorS<Dim, Real> dXdt(&xyz[3], Dim);
  JinvType T(&xyz[3], Dim);

  // The increment is computed by solving for dt from the relationship
  // dot( dX, T ) = dot( dX/dt, T )*dt
  // where T is the tangent vector to the edge (i.e. dX/dt)
  // So the inverse Jacobian is simply
  // Jinv = T/dot( dX/dt, T )

  T /= T*dXdt;

  return T;
}

#if 0
template<int Dim>
EGEdge<Dim>&
EGEdge<Dim>::operator=( const EGEdge& edge )
{
  if ( this != &edge )
  {
    EGTopologyBase::operator=(edge);
    range_ = edge.range_;
  }
  return *this;
}

template<int Dim>
void
EGEdge<Dim>::reference( const EGEdge& ref )
{
  if ( obj_ != ref.obj_ )
  {
    EGObject::reference(ref);
    int per;
    EG_STATUS( EG_getRange(obj_, &range_[0], &per) );
  }
}
#endif

template<int Dim_>
Real
EGEdge<Dim_>::getArcLength() const
{
  double alen = 0;
  EG_STATUS( EG_arcLength(obj_, range_[0], range_[1], &alen) );
  return alen;
}

template<int Dim>
Real
EGEdge<Dim>::CartIncrement2Param( const Real t, const CartCoord& dX ) const
{
  //Compute the inverse Jacobian
  JinvType Jinv = this->Jinv(t);

  //Compute the new t value
  Real tnew = t + Jinv*dX;

  //Keep the parameter within the parametric range_
  return MAX(MIN(range_[1],tnew),range_[0]);
}


//Explicitly instantiate the class
template class EGEdge<2>;
template class EGEdge<3>;

}
}
