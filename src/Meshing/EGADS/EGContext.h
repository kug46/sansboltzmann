// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EG_CONTEXT_H
#define EG_CONTEXT_H

#include <vector>

#include "EGStatus.h"

// EG context store a global context for EGADS objects

namespace SANS
{
namespace EGADS
{

//Forward declare
template<int Dim_>
class EGBody;

class CreateContext {};

class EGContext
{
public:

  // No default constructor to avoid accidentally creating a context
  EGContext() = delete;

  explicit EGContext(const CreateContext&, const int outLevel = 0) : context_(NULL), reference_(false)
  {
    EG_STATUS( EG_open(&context_) );
    EG_setOutLevel(context_, outLevel); //Turn off all EGADS print statements
  }

  explicit EGContext(ego obj) : context_(NULL), reference_(true)
  {
    EG_STATUS( EG_getContext(obj, &context_) );
  }

  EGContext(const EGContext& context) : context_(context.context_), reference_(true)
  {
  }

  EGContext& operator=( const EGContext& context)
  {
    context_ = context.context_;
    reference_ = true;
    return *this;
  }

  ~EGContext();

  bool operator==( const EGContext& cntxt ) const
  {
    return context_ == cntxt.context_;
  }

  bool isReference() const { return reference_; }

  operator ego()       { return context_; }
  operator ego() const { return context_; }

  // Removed unused objects
  void cleanup();

  // Generates basic solid bodies
  EGBody<3> makeSolidBody( int stype, const std::vector<double>& data ) const;

protected:
//The actual context
  ego context_;
private:
  bool reference_;
};

}
}

#endif //EG_CONTEXT_H
