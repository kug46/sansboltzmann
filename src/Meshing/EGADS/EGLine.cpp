// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "EGLine.h"

namespace SANS
{
namespace EGADS
{

template<>
EGLine<2>::EGLine(EGContext& context, const DLA::VectorS<Dim,Real>& X, const DLA::VectorS<Dim,Real>& dX)
 : EGGeometryBase(context)
{
  double pts[6] = { X[0], X[1], 0, dX[0], dX[1], 0 };

  EG_STATUS( EG_makeGeometry(context, CURVE, LINE, NULL, NULL, pts, &obj_) );
}

template<>
EGLine<2>::EGLine(const EGNode<Dim>& n0, const EGNode<Dim>& n1)
 : EGGeometryBase(n0.getContext())
{
  //Use the first node as the starting point for the line
  CartCoord X = n0;

  //Compute the directional vector for the line
  CartCoord dX = (CartCoord)n1 - X;

  double pts[6] = { X[0], X[1], 0, dX[0], dX[1], 0 };

  EG_STATUS( EG_makeGeometry(context_, CURVE, LINE, NULL, NULL, pts, &obj_) );
}

//Instantiate the class
template class EGLine<2>;



template<>
EGLine<3>::EGLine(EGContext& context, const DLA::VectorS<Dim,Real>& X, const DLA::VectorS<Dim,Real>& dX)
 : EGGeometryBase(context)
{
  double pts[6] = { X[0], X[1], X[2], dX[0], dX[1], dX[2] };

  EG_STATUS( EG_makeGeometry(context, CURVE, LINE, NULL, NULL, pts, &obj_) );
}

template<>
EGLine<3>::EGLine(const EGNode<Dim>& n0, const EGNode<Dim>& n1)
 : EGGeometryBase(n0.getContext())
{
  //Use the first node as the starting point for the line
  CartCoord X = n0;

  //Compute the directional vector for the line
  CartCoord dX = (CartCoord)n1 - X;

  double pts[6] = { X[0], X[1], X[2], dX[0], dX[1], dX[2] };

  EG_STATUS( EG_makeGeometry(context_, CURVE, LINE, NULL, NULL, pts, &obj_) );
}

//Instantiate the class
template class EGLine<3>;
}
}
