// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "EGTopologyBase.h"

namespace SANS
{
namespace EGADS
{

EGTopologyBase::~EGTopologyBase()
{
}

EGTopologyBase&
EGTopologyBase::operator=(const EGTopologyBase& copy)
{
  EGObject::operator=(copy);
  return *this;
}

double
EGTopologyBase::getTolerance() const
{
  double tol;
  EG_STATUS( EG_getTolerance(obj_, &tol) );
  return tol;
}

Real
EGTopologyBase::getSize() const
{
  double  box[6], size;
  EG_STATUS( EG_getBoundingBox(obj_, box) );

                            size = box[3]-box[0];
  if (size < box[4]-box[1]) size = box[4]-box[1];
  if (size < box[5]-box[2]) size = box[5]-box[2];

  return size;
}

std::vector<double>
EGTopologyBase::getBoundingBox() const
{
  std::vector<double> box(6);
  EG_STATUS( EG_getBoundingBox(obj_, &box[0]) );
  return box;
}

std::vector<double>
EGTopologyBase::getCG() const
{
  std::vector<double> prop(14);
  EG_STATUS( EG_getMassProperties(obj_, &prop[0]) );
  // prop[0] - Volume
  // prop[1] - Area
  std::vector<double> CG = {prop[2], prop[3], prop[4]};
  // prop[5-13] inertia matrix
  return CG;
}

bool
EGTopologyBase::isTopoType( const int TOPO ) const
{
  int oclass, mtype;
  ego topRef, prev, next;

  //Check that the ego is the correct oclass
  EG_STATUS( EG_getInfo(obj_, &oclass, &mtype, &topRef, &prev, &next) );

  return mtype == TOPO;
}


}
}
