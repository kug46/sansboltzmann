// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EG_PLANE_H
#define EG_PLANE_H

//EG Plane represents a line

#include "EGGeometryBase.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

namespace SANS
{
namespace EGADS
{

template<int Dim_>
class EGPlane : public EGGeometryBase
{
public:
  static const int Dim = Dim_;

  EGPlane( const EGPlane& plane ) : EGGeometryBase(plane) {}
  EGPlane& operator=( const EGPlane& plane ) { EGGeometryBase::operator=(plane); return *this; }
  EGPlane( const EGContext& context, const DLA::VectorS<Dim,Real>& X,
                                     const DLA::VectorS<Dim,Real>& dX,
                                     const DLA::VectorS<Dim,Real>& dY);

  explicit EGPlane(const EGContext& context);

  //operator()(const Real t)
};

}
}

#endif //EG_PLANE_H
