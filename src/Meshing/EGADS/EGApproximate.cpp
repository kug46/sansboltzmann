// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "EGApproximate.h"
#include <fstream>

namespace SANS
{
namespace EGADS
{

template<int Dim>
EGApproximate<Dim>::EGApproximate(const EGContext& context, const endConditions mDeg, const double tol, const std::string& filename, bool Surface)
 : EGGeometryBase(context)
{
  int sizes[2] = {0, Surface ? 1 : 0 };

  std::ifstream splinefile(filename);

  splinefile >> sizes[0];

  std::vector<double> xyz(3*sizes[0]);

  for (int i = 0; i < sizes[0]; i++ )
  {
    splinefile >> xyz[3*i+0];
    splinefile >> xyz[3*i+1];
    if (Dim == 3)
      splinefile >> xyz[3*i+2];
    else
      xyz[3*i+2] = 0;
  }

  EG_STATUS( EG_approximate(context, (int)mDeg, tol, sizes, &xyz[0], &obj_) );
}

template<int Dim>
EGApproximate<Dim>::EGApproximate(const EGContext& context, const endConditions mDeg, const double tol,
                                  const std::vector<CartCoord>& pts, bool Surface)
 : EGGeometryBase(context)
{
  int sizes[2] = {(int)pts.size(), Surface ? 1 : 0 };

  std::vector<double> xyz(3*pts.size());

  for (int i = 0; i < sizes[0]; i++ )
  {
    xyz[3*i+0] = pts[i][0];
    xyz[3*i+1] = pts[i][1];
    xyz[3*i+2] = Dim == 2 ? 0 : pts[i][2];
  }

  EG_STATUS( EG_approximate(context, (int)mDeg, tol, sizes, &xyz[0], &obj_) );
}

template<int Dim>
typename EGApproximate<Dim>::CartCoord
EGApproximate<Dim>::operator()( const double t ) const
{
  SANS_ASSERT( t >= 0 && t <= 1);
  double xyz[9];
  EG_STATUS( EG_evaluate(obj_, &t, xyz) );
  return CartCoord(xyz, Dim);
}

template<int Dim>
Real
EGApproximate<Dim>::operator()( const CartCoord& X ) const
{
  double tt;
  double dum[3];
  double xyz[3] = {X[0], X[1], Dim == 2 ? 0 : X[2]};
  EG_STATUS( EG_invEvaluate(obj_, xyz, &tt, dum) );

//  Real dist2 = 0;
//  for (int i = 0; i < Dim; i++)
//    dist2 = (dum[i]-xyz[i])*(dum[i]-xyz[i]);

  //SANS_ASSERT_MSG( sqrt(dist2) < 1e-10, "sqrt(dist2) = %lf, tol = %lf", sqrt(dist2), getTolerance() );

  return tt;
}


//Instantiate the class
template class EGApproximate<2>;
template class EGApproximate<3>;
}
}
