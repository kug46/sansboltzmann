// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EGADS_CLONE_H
#define EGADS_CLONE_H

#include <vector>

#include "EGBody.h"

namespace SANS
{
namespace EGADS
{

// Create a clone of the vector
template<int Dim>
std::vector< EGBody<Dim> > clone( const std::vector< EGBody<Dim> >& bodies )
{
  std::vector< EGBody<Dim> > clones;
  for (auto body = bodies.begin(); body != bodies.end(); body++ )
    clones.emplace_back( body->clone() );

  return clones;
}

}
}

#endif //EGADS_CLONE_H
