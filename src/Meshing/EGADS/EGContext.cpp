// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "EGContext.h"
#include "EGBody.h"

namespace SANS
{
namespace EGADS
{

EGBody<3>
EGContext::makeSolidBody( int stype, const std::vector<double>& data ) const
{
  switch (stype)
  {
  case BOX:
    SANS_ASSERT( data.size() == 6 );
    break;
  case SPHERE:
    SANS_ASSERT( data.size() == 4 );
    break;
  case CONE:
    SANS_ASSERT( data.size() == 7 );
    break;
  case CYLINDER:
    SANS_ASSERT( data.size() == 7 );
    break;
  case TORUS:
    SANS_ASSERT( data.size() == 8 );
    break;
  default:
    SANS_DEVELOPER_EXCEPTION( "Unknown Solid body type: %d", stype);
  }

  ego body;
  EG_STATUS( EG_makeSolidBody( context_, stype, &data[0], &body) );
  return EGBody<3>( EGObject(body) );
}

EGContext::~EGContext()
{
  // close the context if this is not a reference to another context
  if (!reference_)
    EG_STATUS( EG_close(context_) );
}

void
EGContext::cleanup()
{
  //Removes any objects not connected to bodies or models
  EG_STATUS( EG_deleteObject(context_) );
}

} // namespace EGADS
} // namespace SANS
