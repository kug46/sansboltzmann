// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EG_TOOLS_H
#define EG_TOOLS_H

#include "egads.h"

namespace SANS
{
namespace EGADS
{

//A deleter for EGADS allocated arrays
struct EGADS_free
{
  inline void operator()(void* p)
  {
    EG_free(p);
  }
};

struct EGADS_deleteObject
{
  inline void operator()(void* p)
  {
    EG_deleteObject((ego)p);
  }
};

}
}

#endif //EG_TOOLS_H
