// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EG_SOLIDBOOLEAN_H
#define EG_SOLIDBOOLEAN_H

#include "EGBody.h"
#include "EGModel.h"

namespace SANS
{
namespace EGADS
{

template<int Dim>
EGModel<Dim> fuse( const EGBody<Dim>& L, const EGBody<Dim>& R )
{
  EGModel<Dim> result(L.getContext());
  EG_STATUS( EG_solidBoolean(L, R, FUSION, (ego*)result) );
  return result;
}

template<int Dim>
EGModel<Dim> intersect( const EGBody<Dim>& L, const EGBody<Dim>& R )
{
  EGModel<Dim> result(L.getContext());
  EG_STATUS( EG_solidBoolean(L, R, INTERSECTION, (ego*)result) );
  return result;
}

template<int Dim>
EGModel<Dim> subtract( const EGBody<Dim>& L, const EGBody<Dim>& R )
{
  EGModel<Dim> result(L.getContext());
  EG_STATUS( EG_solidBoolean(L, R, SUBTRACTION, (ego*)result) );
  return result;
}

template<int Dim>
EGModel<Dim> fuse( const EGModel<Dim>& L, const EGBody<Dim>& R )
{
  EGModel<Dim> result(L.getContext());
  EG_STATUS( EG_solidBoolean(L, R, FUSION, (ego*)result) );
  return result;
}

template<int Dim>
EGModel<Dim> intersect( const EGModel<Dim>& L, const EGBody<Dim>& R )
{
  EGModel<Dim> result(L.getContext());
  EG_STATUS( EG_solidBoolean(L, R, INTERSECTION, (ego*)result) );
  return result;
}

}
}


#endif //EG_SOLIDBOOLEAN_H
