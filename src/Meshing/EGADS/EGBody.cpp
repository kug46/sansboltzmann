// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "EGBody.h"
#include "EGIntersection.h"

namespace SANS
{
namespace EGADS
{

template<int Dim>
EGBodyBase<Dim>::EGBodyBase( const EGObject& body ) : EGTopologyBase(body)
{
  int oclass, mtype;
  ego topRef, prev, next;

  //Check that the ego is the correct oclass
  EG_STATUS( EG_getInfo(obj_, &oclass, &mtype, &topRef, &prev, &next) );

  SANS_ASSERT_MSG( oclass == BODY, "Class should be BODY: oclass = %d", oclass );
}

template<int Dim>
void
EGBodyBase<Dim>::save( const std::string& name ) const
{
  //Save the body to a file
  EG_STATUS( EG_saveModel(obj_, name.c_str()) );
}

template<int Dim>
template<class EGTopo>
std::vector< EGTopo >
EGBodyBase<Dim>::getTopos(const int TOPO) const
{
  int ntopo;
  ego *ptopos;

  //Extract all topology objects
  EG_STATUS( EG_getBodyTopos(obj_, NULL, TOPO, &ntopo, &ptopos) );

  std::vector< EGTopo > topos;
  for (int i = 0; i < ntopo; i++)
    topos.emplace_back( EGObject( ptopos[i]) );

  topos.shrink_to_fit();
  EG_free(ptopos); ptopos = NULL;

  return topos;
}

template<int Dim> std::vector< EGNode<Dim> > EGBodyBase<Dim>::getNodes () const { return getTopos< EGNode<Dim> >(NODE); }
template<int Dim> std::vector< EGEdge<Dim> > EGBodyBase<Dim>::getEdges () const { return getTopos< EGEdge<Dim> >(EDGE); }
template<int Dim> std::vector< EGLoop<Dim> > EGBodyBase<Dim>::getLoops () const { return getTopos< EGLoop<Dim> >(LOOP); }
template<int Dim> std::vector< EGFace<Dim> > EGBodyBase<Dim>::getFaces () const { return getTopos< EGFace<Dim> >(FACE); }
//template<int Dim> std::vector< EGFace<Dim> > EGBodyBase<Dim>::getShells() const { return getTopos< EGShell<Dim> >(SHELL); }


template<int Dim>
template<class EGTopo>
std::vector< EGTopo >
EGBodyBase<Dim>::getTopos(const int TOPO, const EGTopologyBase& ref) const
{
  int ntopo;
  ego *ptopos;

  //Extract all topology objects
  EG_STATUS( EG_getBodyTopos(obj_, ref, TOPO, &ntopo, &ptopos) );

  std::vector< EGTopo > topos;
  for (int i = 0; i < ntopo; i++)
    topos.emplace_back( EGObject( ptopos[i]) );

  topos.shrink_to_fit();
  EG_free(ptopos); ptopos = NULL;

  return topos;
}

template<int Dim> std::vector<EGNode<Dim>> EGBodyBase<Dim>::getNodes (const EGTopologyBase& ref) const { return getTopos<EGNode<Dim>>(NODE, ref); }
template<int Dim> std::vector<EGEdge<Dim>> EGBodyBase<Dim>::getEdges (const EGTopologyBase& ref) const { return getTopos<EGEdge<Dim>>(EDGE, ref); }
template<int Dim> std::vector<EGLoop<Dim>> EGBodyBase<Dim>::getLoops (const EGTopologyBase& ref) const { return getTopos<EGLoop<Dim>>(LOOP, ref); }
template<int Dim> std::vector<EGFace<Dim>> EGBodyBase<Dim>::getFaces (const EGTopologyBase& ref) const { return getTopos<EGFace<Dim>>(FACE, ref); }


template<int Dim>
int
EGBodyBase<Dim>::nTopos(const int TOPO) const
{
  int ntopo;
  ego *ptopos;

  //Extract all topology objects
  EG_STATUS( EG_getBodyTopos(obj_, NULL, TOPO, &ntopo, &ptopos) );
  EG_free(ptopos); ptopos = NULL;

  return ntopo;
}

//Explicitly instantiate the class
template class EGBodyBase<2>;
template class EGBodyBase<3>;



EGBody<2>::EGBody( const EGFace<Dim>& face ) : EGBodyBase(face.getContext())
{
  //Create the face body
  EG_STATUS( EG_makeTopology(face.getContext(), NULL, BODY, FACEBODY, NULL, 1, face, NULL, &obj_) );
}

EGBody<2>::EGBody(const EGContext& context, const EGSense< EGLoop<Dim> >& loop, const int mtype ) : EGBodyBase(context)
{
  //Create a face
  EGFace<Dim> face(context,loop,mtype);

  //Create the face body
  EG_STATUS( EG_makeTopology(face.getContext(), NULL, BODY, FACEBODY, NULL, 1, face, NULL, &obj_) );

}

EGBody<2>::EGBody(const EGContext& context, const loop_vector& loops, const int mtype ) : EGBodyBase(context)
{
  EGFace<Dim> face(context,loops,mtype);

  //Create the face body
  EG_STATUS( EG_makeTopology(context, NULL, BODY, FACEBODY, NULL, 1, face, NULL, &obj_) );
}

EGBody<2>::EGBody( const EGIntersection<2>& intersection ) : EGBodyBase(intersection.getContext())
{
  EG_STATUS( EG_imprintBody(intersection.body(), intersection.nEdge(), intersection.facEdg(), &obj_) );
}

EGBody<2>::~EGBody()
{
}



//=================================================================================================//
EGBody<3>::EGBody( const EGLoop<Dim>& loop ) : EGBodyBase(loop.getContext())
{
  //Create the wire body
  EG_STATUS( EG_makeTopology(context_, NULL, BODY, WIREBODY, NULL, 1, loop, NULL, &obj_) );
}

EGBody<3>::EGBody( const EGFace<Dim>& face ) : EGBodyBase(face.getContext())
{
  //Create the face body
  EG_STATUS( EG_makeTopology(context_, NULL, BODY, FACEBODY, NULL, 1, face, NULL, &obj_) );
}


EGBody<3>::EGBody(const EGGeometryBase& geom, const EGSense< EGLoop<Dim> >& loop, const int mtype ) : EGBodyBase(geom.getContext())
{
  //Create a face
  EGFace<Dim> face(geom,loop,mtype);

  //Create the face body
  EG_STATUS( EG_makeTopology(context_, NULL, BODY, FACEBODY, NULL, 1, face, NULL, &obj_) );

}

EGBody<3>::EGBody(const EGGeometryBase& geom, const loop_vector& loops, const int mtype ) : EGBodyBase(geom.getContext())
{
  EGFace<Dim> face(geom,loops,mtype);

  //Create the face body
  EG_STATUS( EG_makeTopology(context_, NULL, BODY, FACEBODY, NULL, 1, face, NULL, &obj_) );
}

EGBody<3>::EGBody( const EGIntersection<3>& intersection ) : EGBodyBase(intersection.getContext())
{
  EG_STATUS( EG_imprintBody(intersection.body(), intersection.nEdge(), intersection.facEdg(), &obj_) );
}

EGBody<3>::~EGBody()
{
}

EGBody<4>::~EGBody()
{
}

}
}
