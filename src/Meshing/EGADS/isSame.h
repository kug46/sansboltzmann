// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EG_ISSAME_H
#define EG_ISSAME_H

#include "EGObject.h"

namespace SANS
{
namespace EGADS
{

inline bool isSame( const EGObject& obj1, const EGObject& obj2 )
{
  return EG_isSame( obj1, obj2 ) == EGADS_SUCCESS;
}

}
}


#endif //EG_ISSAME_H
