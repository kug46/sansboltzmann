// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EG_SEWFACES_H
#define EG_SEWFACES_H

#include "EGModel.h"
#include "EGFace.h"

namespace SANS
{
namespace EGADS
{

enum sewFaceFlags
{
  Manifold,
  NonManifold
};

template<int Dim>
EGModel<Dim> sewFaces( const std::vector< EGBody<Dim> >& Faces, double tol, sewFaceFlags flag )
{
  SANS_ASSERT( Faces.size() > 0 );

  EGModel<Dim> model(Faces[0].getContext());
  std::vector<ego> faces(Faces.size());
  for (std::size_t i = 0; i < Faces.size(); i++)
    faces[i] = Faces[i];

  EG_STATUS( EG_sewFaces( (int)Faces.size(), &faces[0], tol, (int)flag, (ego*)model) );

  return model;
}

template<int Dim>
EGModel<Dim> sewFaces( const std::vector< EGFace<Dim> >& Faces, double tol, sewFaceFlags flag )
{
  SANS_ASSERT( Faces.size() > 0 );

  EGModel<Dim> model(Faces[0].getContext());
  std::vector<ego> faces(Faces.size());
  for (std::size_t i = 0; i < Faces.size(); i++)
    faces[i] = Faces[i];

  EG_STATUS( EG_sewFaces( (int)Faces.size(), &faces[0], tol, (int)flag, (ego*)model) );

  return model;
}

}
}


#endif //EG_SEWFACES_H
