// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EG_ISEQUIVALENT_H
#define EG_ISEQUIVALENT_H

#include "EGTopologyBase.h"

namespace SANS
{
namespace EGADS
{

inline bool isEquivalent( const EGTopologyBase& topo1, const EGTopologyBase& topo2 )
{
  return EG_isEquivalent( topo1, topo2 ) == EGADS_SUCCESS;
}

}
}


#endif //EG_ISEQUIVALENT_H
