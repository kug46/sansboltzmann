// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "EGObject.h"

#include <algorithm>
#include <cmath>

#include "tools/stringify.h"
#include "tools/output_std_vector.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"

namespace SANS
{
namespace EGADS
{

EGObject::EGObject(const EGObject& copy) : context_(copy.context_), obj_(NULL), reference_(copy.reference_)
{
  //if ( reference_ )
    obj_ = copy;
  //else
  //  EG_STATUS( EG_copyObject(copy, NULL, &obj_) );
}

//Move constructor
EGObject::EGObject(const EGObject&& copy) : context_(copy.context_), obj_(copy), reference_(copy.reference_)
{
}

EGObject&
EGObject::operator=(const EGObject& copy)
{
  if ( this != &copy )
  {
    SANS_ASSERT( context_ == copy.context_ );

    //if ( copy.reference_ )
      obj_ = copy;
    //else
    //  EG_STATUS( EG_copyObject(copy, NULL, &obj_) );

    reference_ = copy.reference_;
  }
  return *this;
}

//Move assignement operator
EGObject&
EGObject::operator=(const EGObject&& copy)
{
  if ( this != &copy )
  {
    SANS_ASSERT( context_ == copy.context_ );

    //This is a weak copy that is about to get destroyed.
    //Shallow copy is just fine here
    obj_ = copy;

    reference_ = copy.reference_;
  }
  return *this;
}

#if 0
void
EGObject::reference(const EGObject& ref)
{
  if ( obj_ != ref.obj_ )
  {
    SANS_ASSERT( context_ == ref.context_ );

    //Delete();
    obj_ = ref.obj_;
    reference_ = true;
  }
}
#endif

EGObject
EGObject::clone( const DLA::VectorS<3,Real>& translate ) const
{
  DLA::MatrixS<3,4,double> xform;

  xform = DLA::Identity();

  // Set the translation
  for (int i = 0; i < 3; i++)
    xform(i,3) = translate[i];

  //Create the transformation object
  ego oform;
  EG_STATUS( EG_makeTransform(context_, &xform(0,0), &oform) );

  //Create the transformed clone
  ego clone;
  EG_STATUS( EG_copyObject(obj_, oform, &clone) );

  return EGObject(clone);
}

EGObject
EGObject::clone( double theta, DLA::VectorS<3,Real> axis ) const
{
  // Make sure it's a unit axis
  axis /= sqrt(dot(axis,axis));

  // Convert to radians
  theta *= PI/180.;

  double c = cos(theta);
  double s = sin(theta);

  DLA::MatrixS<3,3,double> I = DLA::Identity();

  double ux = axis[0];
  double uy = axis[1];
  double uz = axis[2];

  // cross product matrix of the axis
  DLA::MatrixS<3,3,double> uX = { {  0, -uz,  uy},
                                  { uz,   0, -ux},
                                  {-uy,  ux,   0} };

  // Compute the rotation matrix about an arbitrary axis
  DLA::MatrixS<3,3,double> R = c*I + s*uX + (1-c)*axis*Transpose(axis);

  DLA::MatrixS<3,4,double> xform(0);

  // Set the rotation matrix
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      xform(i,j) = R(i,j);

  //Create the transformation object
  ego oform;
  EG_STATUS( EG_makeTransform(context_, &xform(0,0), &oform) );

  //Create the transformed clone
  ego clone;
  EG_STATUS( EG_copyObject(obj_, oform, &clone) );

  return EGObject(clone);
}

EGObject
EGObject::clone( const double scale ) const
{
  DLA::MatrixS<3,4,double> xform;

  xform = DLA::Identity();

  // Set the scale factor
  xform *= scale;

  //Create the transformation object
  ego oform;
  EG_STATUS( EG_makeTransform(context_, &xform(0,0), &oform) );

  //Create the transformed clone
  ego clone;
  EG_STATUS( EG_copyObject(obj_, oform, &clone) );

  return EGObject(clone);
}

void
EGObject::Delete()
{
  // Only delete the object if SANS is responsible for the context
  if ( obj_ != NULL )
    EG_STATUS( EG_deleteObject(obj_) );
  obj_ = NULL;
  reference_ = false;
}

int
EGObject::objectClass() const
{
  ego ref;
  int oclass, mtype, nchild;
  double range[4];
  ego* pchldrn;
  int *psens;

  //Get the object class
  EG_STATUS( EG_getTopology( obj_, &ref, &oclass, &mtype, range, &nchild, &pchldrn, &psens ) );

  return oclass;
}

EGObject::~EGObject()
{
/*
  if ( obj_ != NULL ) // && !reference_ )
  {
    int status = EG_deleteObject(obj_);
    if ( !(status == EGADS_SUCCESS || status == EGADS_REFERCE) )
      BOOST_THROW_EXCEPTION( EGADSException(status) );
  }
*/
}

//-----------------------------------------------------------------------------
void
EGObject::addAttribute(const std::string& name, const int attr)
{
  try
  {
    EG_STATUS( EG_attributeAdd( obj_, name.c_str(), ATTRINT, 1, &attr, NULL, NULL) );
  }
  catch (EGADS::EGADSException& e)
  {
    e.during("While trying to add 'int' attribute : " + name + " = " + stringify(attr));
    throw;
  }
}
void
EGObject::addAttribute(const std::string& name, const double attr)
{
  try
  {
    EG_STATUS( EG_attributeAdd( obj_, name.c_str(), ATTRREAL, 1, NULL, &attr, NULL) );
  }
  catch (EGADS::EGADSException& e)
  {
    e.during("While trying to add 'double' attribute : " + name + " = " + stringify(attr));
    throw;
  }
}
void
EGObject::addAttribute(const std::string& name, const std::vector<int>& attr)
{
  try
  {
    EG_STATUS( EG_attributeAdd( obj_, name.c_str(), ATTRINT, attr.size(), attr.data(), NULL, NULL) );
  }
  catch (EGADS::EGADSException& e)
  {
    e.during("While trying to add 'int array' attribute : " + name);
    throw;
  }
}
void
EGObject::addAttribute(const std::string& name, const std::initializer_list<int>& attr)
{
  addAttribute(name, std::vector<int>(attr));
}
void
EGObject::addAttribute(const std::string& name, const int* attr, const int len)
{
  try
  {
    EG_STATUS( EG_attributeAdd( obj_, name.c_str(), ATTRINT, len, attr, NULL, NULL) );
  }
  catch (EGADS::EGADSException& e)
  {
    e.during("While trying to add 'int array' attribute : " + name);
    throw;
  }
}
void
EGObject::addAttribute(const std::string& name, const std::vector<double>& attr)
{
  try
  {
    EG_STATUS( EG_attributeAdd( obj_, name.c_str(), ATTRREAL, attr.size(), NULL, attr.data(), NULL) );
  }
  catch (EGADS::EGADSException& e)
  {
    e.during("While trying to add 'double array' attribute : " + name);
    throw;
  }
}
void
EGObject::addAttribute(const std::string& name, const std::initializer_list<double>& attr)
{
  addAttribute(name, std::vector<double>(attr));
}
void
EGObject::addAttribute(const std::string& name, const double* attr, const int len)
{
  try
  {
    EG_STATUS( EG_attributeAdd( obj_, name.c_str(), ATTRREAL, len, NULL, attr, NULL) );
  }
  catch (EGADS::EGADSException& e)
  {
    e.during("While trying to add 'double array' attribute : " + name);
    throw;
  }
}
void
EGObject::addAttribute(const std::string& name, const std::string& attr)
{
  try
  {
    EG_STATUS( EG_attributeAdd( obj_, name.c_str(), ATTRSTRING, attr.size(), NULL, NULL, attr.c_str()) );
  }
  catch (EGADS::EGADSException& e)
  {
    e.during("While trying to add 'string' attribute : " + name + " = " + attr);
    throw;
  }
}


//-----------------------------------------------------------------------------
void
EGObject::delAttribute(const std::string& name)
{
  try
  {
    EG_STATUS( EG_attributeDel( obj_, name.c_str() ) );
  }
  catch (EGADS::EGADSException& e)
  {
    e.during("While trying to delete attribute : " + name);
    throw;
  }
}


//-----------------------------------------------------------------------------
int
EGObject::numAttributes() const
{
  int nattr = 0;
  EG_STATUS( EG_attributeNum( obj_, &nattr ) );
  return nattr;
}


//-----------------------------------------------------------------------------
bool
EGObject::hasAttribute(const std::string& name) const
{
  int atype, len;
  const int *pints;
  const double *preals;
  const char *string;
  return EG_attributeRet( obj_, name.c_str(), &atype, &len, &pints, &preals, &string) == EGADS_SUCCESS;
}
//-----------------------------------------------------------------------------
int
EGObject::getAttributeSize(const std::string& name) const
{
  int atype, len;
  const int *pints;
  const double *preals;
  const char *string;

  try
  {
    EG_STATUS( EG_attributeRet( obj_, name.c_str(), &atype, &len, &pints, &preals, &string) );
  }
  catch (EGADS::EGADSException& e)
  {
    e.during("While trying to retrieve size of attribute : " + name);
    throw;
  }

  return len;
}
int
EGObject::getAttributeSize(const int index) const
{
  int atype, len;
  const int *pints;
  const double *preals;
  const char *cname, *string;

  try
  {
    EG_STATUS( EG_attributeGet( obj_, index, &cname, &atype, &len, &pints, &preals, &string) );
  }
  catch (EGADS::EGADSException& e)
  {
    e.during("While trying to retrieve size buy index : " + stringify(index));
    throw;
  }

  return len;
}

//-----------------------------------------------------------------------------
int
EGObject::getAttributeType(const std::string& name) const
{
  int atype, len;
  const int *pints;
  const double *preals;
  const char *string;

  try
  {
    EG_STATUS( EG_attributeRet( obj_, name.c_str(), &atype, &len, &pints, &preals, &string) );
  }
  catch (EGADS::EGADSException& e)
  {
    e.during("While trying to retrieve 'int' attribute : " + name);
    throw;
  }

  return atype;
}
int
EGObject::getAttributeType(const int index) const
{
  int atype, len;
  const int *pints;
  const double *preals;
  const char *cname, *string;

  try
  {
    EG_STATUS( EG_attributeGet( obj_, index, &cname, &atype, &len, &pints, &preals, &string) );
  }
  catch (EGADS::EGADSException& e)
  {
    e.during("While trying to retrieve attribute 'type' buy index : " + stringify(index));
    throw;
  }

  return atype;
}

//=============================================================================
EGADSAttributeException::EGADSAttributeException(const std::string name,
                                                 int expectType, int expectLen)
{
  errString += "EGADS Error\n\n";
  errString += "Could not find attribute : " + name + "\n\n";
  errString += "Expected ";
  if (expectLen > 0)
    errString += attrstr(expectType) + " array of length " + std::to_string(expectLen);
  else
    errString += "array of " + attrstr(expectType);
}

//-----------------------------------------------------------------------------
EGADSAttributeException::EGADSAttributeException(int index,
                                                 int expectType, int expectLen)
{
  errString += "EGADS Error\n\n";
  errString += "Could not find attribute with index " + std::to_string(index) + "\n\n";
  errString += "Expected ";
  if (expectLen > 0)
    errString += attrstr(expectType) + " array of length " + std::to_string(expectLen);
  else
    errString += "array of " + attrstr(expectType);
}

//-----------------------------------------------------------------------------
EGADSAttributeException::EGADSAttributeException(const std::string name,
                                                 int atype, int len,
                                                 const int *pints,
                                                 const double *preals,
                                                 const char *string,
                                                 int expectType, int expectLen)
{
  errString += "EGADS Error\n\n";
  errString += "Expected attribute '" + name + "'";
  errString += " of type " + attrstr(expectType);
  if (expectLen > 0)
    errString += " and length " + std::to_string(expectLen);
  errString += ".\n\n";

  found(atype, len, pints, preals, string);
}

//-----------------------------------------------------------------------------
EGADSAttributeException::EGADSAttributeException(int index,
                                                 const char *cname,
                                                 int atype, int len,
                                                 const int *pints,
                                                 const double *preals,
                                                 const char *string,
                                                 int expectType, int expectLen)
{
  errString += "EGADS Error\n\n";
  errString += "Expected attribute index " + std::to_string(index);
  errString += " with name '" + std::string(cname) + "'";
  errString += " of type " + attrstr(expectType);
  if (expectLen > 0)
    errString += " and length " + std::to_string(expectLen);
  errString += ".\n\n";

  found(atype, len, pints, preals, string);
}

//-----------------------------------------------------------------------------
void
EGADSAttributeException::found(int atype, int len,
                               const int *pints,
                               const double *preals,
                               const char *string)
{
  errString += "Found attribute of type " + attrstr(atype);
  errString += " and length " + std::to_string(len) + " with data:\n\n";
  if (atype == ATTRSTRING)
    errString += std::string(string);
  else
  {
    for (int i = 0; i < len; i++)
    {
      switch (atype)
      {
      case ATTRINT:
        errString += std::to_string(pints[i]);
        break;
      case ATTRREAL:
        errString += std::to_string(preals[i]);
        break;
      default:
        errString += "'unknown'";
      }
      if ( i < len-1 ) errString += ", ";
      if ( i == 10   ) errString += "\n";
    }
  }
}

//-----------------------------------------------------------------------------
std::string
EGADSAttributeException::attrstr(const int atype)
{
  switch (atype)
  {
  case ATTRINT:
    return "'int'";
  case ATTRREAL:
    return "'real'";
  case ATTRSTRING:
    return "'string'";
  case ATTRCSYS:
    return "'csystem'";
//  case ATTRPTR:
//    return "'pointer'";
  default:
    return "'unknown'";
  }
}

//-----------------------------------------------------------------------------
void
EGObject::getAttribute(const std::string& name,
                       int& atype, int& len,
                       const int *&pints,
                       const double *&preals,
                       const char *&string,
                       int expectType, int expectLen) const
{
  int status = EG_attributeRet( obj_, name.c_str(), &atype, &len, &pints, &preals, &string );

  // check if the attribute was not found
  if ( status == EGADS_NOTFOUND )
    BOOST_THROW_EXCEPTION( EGADSAttributeException(name, expectType, expectLen) );

  // check if some other error occured
  if ( status < EGADS_SUCCESS )
    BOOST_THROW_EXCEPTION( EGADSException(status) );

  // check if it's the expected type and length
  if ( expectType != atype || (len != expectLen && expectLen > 0) )
    BOOST_THROW_EXCEPTION( EGADSAttributeException(name, atype, len, pints, preals, string, expectType, expectLen) );
}

//-----------------------------------------------------------------------------
void
EGObject::getAttribute(const int index,
                       const char *&cname,
                       int& atype, int& len,
                       const int *&pints,
                       const double *&preals,
                       const char *&string,
                       int expectType, int expectLen) const
{
  int status = EG_attributeGet( obj_, index, &cname, &atype, &len, &pints, &preals, &string);

  // check if the attribute was not found
  if ( status == EGADS_NOTFOUND )
    BOOST_THROW_EXCEPTION( EGADSAttributeException(index, expectType, expectLen) );

  // check if some other error occured
  if ( status < EGADS_SUCCESS )
    BOOST_THROW_EXCEPTION( EGADSException(status) );

  // check if it's the expected type and length
  if ( expectType != atype || (len != expectLen && expectLen > 0) )
    BOOST_THROW_EXCEPTION( EGADSAttributeException(index, cname, atype, len, pints, preals, string, expectType, expectLen) );
}

//-----------------------------------------------------------------------------
void
EGObject::getAttribute(const std::string& name, int& attr) const
{
  int atype, len;
  const int *pints;
  const double *preals;
  const char *string;

  getAttribute(name, atype, len, pints, preals, string,
               ATTRINT, 1);

  attr = *pints;
}
void
EGObject::getAttribute(const std::string& name, double& attr) const
{
  int atype, len;
  const int *pints;
  const double *preals;
  const char *string;

  getAttribute(name, atype, len, pints, preals, string,
               ATTRREAL, 1);

  attr = *preals;
}
void
EGObject::getAttribute(const std::string& name, std::vector<int>& attr) const
{
  int atype, len;
  const int *pints;
  const double *preals;
  const char *string;

  getAttribute(name, atype, len, pints, preals, string,
               ATTRINT, -1);

  attr = std::vector<int>(pints, pints+len);
}
void
EGObject::getAttribute(const std::string& name, std::vector<double>& attr) const
{
  int atype, len;
  const int *pints;
  const double *preals;
  const char *string;

  getAttribute(name, atype, len, pints, preals, string,
               ATTRREAL, -1);

  attr = std::vector<double>(preals, preals+len);
}
void EGObject::getAttribute(const std::string& name, std::string& attr) const
{
  int atype, len;
  const int *pints;
  const double *preals;
  const char *string;

  getAttribute(name, atype, len, pints, preals, string,
               ATTRSTRING, -1);

  attr = std::string(string, string+len);
}


//-----------------------------------------------------------------------------
void
EGObject::getAttribute(const int index, std::string& name, int& attr) const
{
  int atype = -1, len = 0;
  const int *pints = NULL;
  const double *preals = NULL;
  const char *cname = NULL, *string = NULL;

  getAttribute(index, cname, atype, len, pints, preals, string,
               ATTRINT, 1);

  name = std::string(cname); //Intel compiler needs constructor called
  attr = *pints;
}
void
EGObject::getAttribute(const int index, std::string& name, double& attr) const
{
  int atype = -1, len = 0;
  const int *pints = NULL;
  const double *preals = NULL;
  const char *cname = NULL, *string = NULL;

  getAttribute(index, cname, atype, len, pints, preals, string,
               ATTRREAL, 1);

  name = std::string(cname); //Intel compiler needs constructor called
  attr = *preals;
}
void
EGObject::getAttribute(const int index, std::string& name, std::vector<int>& attr) const
{
  int atype = -1, len = 0;
  const int *pints = NULL;
  const double *preals = NULL;
  const char *cname = NULL, *string = NULL;

  getAttribute(index, cname, atype, len, pints, preals, string,
               ATTRINT, -1);

  name = std::string(cname); //Intel compiler needs constructor called
  attr = std::vector<int>(pints, pints+len);
}
void
EGObject::getAttribute(const int index, std::string& name, std::vector<double>& attr) const
{
  int atype = -1, len = 0;
  const int *pints = NULL;
  const double *preals = NULL;
  const char *cname = NULL, *string = NULL;

  getAttribute(index, cname, atype, len, pints, preals, string,
               ATTRREAL, -1);

  name = std::string(cname); //Intel compiler needs constructor called
  attr = std::vector<double>(preals, preals+len);
}
void
EGObject::getAttribute(const int index, std::string& name, std::string& attr) const
{
  int atype = -1, len = 0;
  const int *pints = NULL;
  const double *preals = NULL;
  const char *cname = NULL, *string = NULL;

  getAttribute(index, cname, atype, len, pints, preals, string,
               ATTRSTRING, -1);

  name = std::string(cname); //Intel compiler needs constructor called
  attr = std::string(string, string+len);
}


//-----------------------------------------------------------------------------
void
EGObject::dupAttributes(const EGObject& src)
{
  EG_STATUS( EG_attributeDup( src, obj_ ) );
}

//-----------------------------------------------------------------------------
void
EGObject::printAttributes() const
{
  std::string name;
  int nattr = numAttributes();
  for (int iattr = 1; iattr <= nattr; iattr++)
  {
    int atype = getAttributeType(iattr);
    switch (atype)
    {
    case ATTRINT:
    {
      std::vector<int> attr;
      getAttribute(iattr, name, attr);
      std::cout << "'int' " << name << " : " << attr << std::endl;
    }
    break;
    case ATTRREAL:
    {
      std::vector<double> attr;
      getAttribute(iattr, name, attr);
      std::cout << "'double' " << name << " : " << attr << std::endl;
    }
    break;
    case ATTRSTRING:
    {
      std::string attr;
      getAttribute(iattr, name, attr);
      std::cout << "'string' " << name << " : " << attr << std::endl;
    }
    break;
    default:
      SANS_DEVELOPER_EXCEPTION("Unknown attribute type : %d", atype);
    }
  }
}

}
}
