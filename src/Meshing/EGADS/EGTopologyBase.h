// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EG_TOPOLOGY_BASE_H
#define EG_TOPOLOGY_BASE_H

// A base class for all topology objects

#include "EGContext.h"
#include "EGObject.h"

namespace SANS
{
namespace EGADS
{

class EGTopologyBase : public EGObject
{
public:
  explicit EGTopologyBase( const EGContext& context ) : EGObject(context) {}
  // cppcheck-suppress noExplicitConstructor
  EGTopologyBase( const EGObject& topo ) : EGObject(topo) {}
  EGTopologyBase( const EGTopologyBase& copy ) : EGObject(copy) {}
  ~EGTopologyBase();

  double getTolerance() const;
  Real getSize() const;
  std::vector<double> getBoundingBox() const;
  std::vector<double> getCG() const;

  EGTopologyBase& operator=( const EGTopologyBase& copy );

protected:
  bool isTopoType( const int TOPO ) const;
};

}
}

#endif //EG_TOPOLOGY_BASE_H
