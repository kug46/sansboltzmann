// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EG_LOOP_H
#define EG_LOOP_H

//A topological loop

#include <vector>
#include "EGEdge.h"
#include "EGSense.h"

namespace SANS
{
namespace EGADS
{

template<int Dim_>
class EGLoop : public EGTopologyBase
{
public:
  static const int Dim = Dim_;
  typedef DLA::VectorS<Dim, Real> CartCoord;

  typedef std::vector< EGSense< EGEdge<Dim> > > edge_vector;

  // cppcheck-suppress noExplicitConstructor
  EGLoop( const EGObject& loop );
  EGLoop( const EGLoop& loop ) : EGTopologyBase(loop) {}
  EGLoop& operator=( const EGLoop& loop ) { EGTopologyBase::operator=(loop); return *this; }
  EGLoop( const EGContext& context, const edge_vector& edges, const int mtype );
  EGLoop(const EGSense< EGEdge<Dim> >& edge, const int mtype );

  std::vector< EGEdge<Dim_> > getEdges() const;
  std::vector<int> getEdgeSenses() const;

  EGSense< EGLoop > operator,(const int sense) { return EGSense< EGLoop >(*this, sense); }
};

}
}

#endif //EG_LOOP_H
