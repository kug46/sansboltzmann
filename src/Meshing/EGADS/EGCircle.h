// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EG_CIRCLE_H
#define EG_CIRCLE_H

//EG Line represents a line

#include "EGGeometryBase.h"
#include "EGNode.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

namespace SANS
{
namespace EGADS
{

template<int Dim_>
class EGCircle;

template<>
class EGCircle<2> : public EGGeometryBase
{
public:
  static const int Dim = 2;

  typedef DLA::VectorS<Dim,Real> CartCoord;

  EGCircle( const EGCircle& circle ) : EGGeometryBase(circle) {}

  EGCircle( const EGContext& context, const DLA::VectorS<Dim,Real>& X, const Real& r );
};

template<>
class EGCircle<3> : public EGGeometryBase
{
public:
  static const int Dim = 3;

  typedef DLA::VectorS<Dim,Real> CartCoord;

  EGCircle( const EGCircle& circle ) : EGGeometryBase(circle) {}

  EGCircle( const EGContext& context, const DLA::VectorS<Dim,Real>& X,
            const DLA::VectorS<Dim,Real>& dX1, const DLA::VectorS<Dim,Real>& dX2, const Real& r );
};


}
}

#endif //EG_CIRCLE_H
