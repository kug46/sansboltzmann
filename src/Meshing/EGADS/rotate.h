// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EG_ROTATE_H
#define EG_ROTATE_H

#include "EGBody.h"
#include "EGFace.h"
#include "EGLoop.h"

namespace SANS
{
namespace EGADS
{

template<int Dim>
EGBody<Dim> rotate( const EGBody<Dim>& src, double angle, DLA::VectorS<6,double> axis )
{
  EGBody<Dim> result(src.getContext());

  EG_STATUS( EG_rotate( src, angle, &axis[0], (ego*)result) );

  return result;
}

template<int Dim>
EGBody<Dim> rotate( const EGFace<Dim>& src, double angle, DLA::VectorS<6,double> axis )
{
  EGBody<Dim> result(src.getContext());

  EG_STATUS( EG_rotate( src, angle, &axis[0], (ego*)result) );

  return result;
}

template<int Dim>
EGBody<Dim> rotate( const EGFace<Dim>& src, double angle, DLA::VectorS<3,double> point, DLA::VectorS<3,double> axis )
{
  EGBody<Dim> result(src.getContext());

  double inputs[6] = {point[0], point[1], point[2], axis[0], axis[1], axis[2]};

  EG_STATUS( EG_rotate( src, angle, inputs, (ego*)result) );

  return result;
}

template<int Dim>
EGBody<Dim> rotate( const EGLoop<Dim>& src, double angle, DLA::VectorS<6,double> axis )
{
  EGBody<Dim> result(src.getContext());

  EG_STATUS( EG_rotate( src, angle, &axis[0], (ego*)result) );

  return result;
}

}
}


#endif //EG_ROTATE_H
