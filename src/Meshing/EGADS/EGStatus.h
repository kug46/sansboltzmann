// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EG_STATUS_H
#define EG_STATUS_H

// Macto for error handling of EGADS API calls

#include <egads.h>
#include "tools/SANSException.h"
#include "tools/SANSnumerics.h"

namespace SANS
{
namespace EGADS
{

//=============================================================================
struct EGADSException : public BackTraceException
{
  explicit EGADSException(const int status);

  virtual ~EGADSException() throw() {}

  // allows for adding an explanation of the EGADS operation to the error message
  void during(const std::string& message);

  const int status;
};

#define EG_STATUS( API_call ) \
  {  \
    int _eg_status_from_api_call_ = API_call; \
    if ( _eg_status_from_api_call_ < EGADS_SUCCESS ) \
      BOOST_THROW_EXCEPTION( SANS::EGADS::EGADSException(_eg_status_from_api_call_) ); \
  }

}
}

#endif //EG_STATUS_H
