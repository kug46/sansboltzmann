// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "EGLoop.h"

namespace SANS
{
namespace EGADS
{

template<int Dim>
EGLoop<Dim>::EGLoop( const EGObject& loop ) : EGTopologyBase(loop)
{
  int oclass, mtype;
  ego topRef, prev, next;

  //Check that the ego is the correct oclass
  EG_STATUS( EG_getInfo(obj_, &oclass, &mtype, &topRef, &prev, &next) );

  SANS_ASSERT( oclass == LOOP );
  //TODO: Should do something with mtype
}


template<int Dim>
EGLoop<Dim>::EGLoop(const EGContext& context, const edge_vector& edges, const int mtype )
 : EGTopologyBase(context)
{
  std::vector<int> senses( edges.size() );
  std::vector<ego> edge_ojects( edges.size() );

  for ( std::size_t i = 0; i < edges.size(); i++ )
  {
    senses[i] = edges[i];
    edge_ojects[i] = edges[i];
  }

  //Create the Loop
  EG_STATUS( EG_makeTopology(context, NULL, LOOP, mtype, NULL, edges.size(), edge_ojects.data(), senses.data(), &obj_) );
}

template<int Dim>
EGLoop<Dim>::EGLoop(const EGSense< EGEdge<Dim> >& edge, const int mtype )
 : EGTopologyBase(edge.getContext())
{
  int senses[1] = {edge};
  ego edge_ojects[1] = {edge};

  //Create the Loop
  EG_STATUS( EG_makeTopology(context_, NULL, LOOP, mtype, NULL, 1, edge_ojects, senses, &obj_) );
}

template<int Dim>
std::vector< EGEdge<Dim> >
EGLoop<Dim>::getEdges() const
{
  ego ref;
  int oclass, mtype, nchild;
  double data[4];
  ego* pchldrn;
  int *psens;

  EG_STATUS( EG_getTopology( obj_, &ref, &oclass, &mtype, data, &nchild, &pchldrn, &psens ) );

  SANS_ASSERT( oclass == LOOP );

  std::vector< EGEdge<Dim> > edges;
  for (int i = 0; i < nchild; i++)
    edges.emplace_back( EGObject(pchldrn[i]) );

  edges.shrink_to_fit();
  return edges;
}

template<int Dim>
std::vector<int>
EGLoop<Dim>::getEdgeSenses() const
{
  ego ref;
  int oclass, mtype, nchild;
  double data[4];
  ego* pchldrn;
  int *psens;

  EG_STATUS( EG_getTopology( obj_, &ref, &oclass, &mtype, data, &nchild, &pchldrn, &psens ) );

  SANS_ASSERT( oclass == LOOP );

  std::vector< int > senses(nchild);
  for (int i = 0; i < nchild; i++)
  {
    int lor = 1;
    if (abs(psens[i]) == 2) lor = -1; // the inner face of a scribe needs to be flipped again
    senses[i] = lor*psens[i];
  }

  return senses;
}

//Explicitly instantiate the class
template class EGLoop<2>;
template class EGLoop<3>;

}
}
