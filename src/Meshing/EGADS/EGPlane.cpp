// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "EGPlane.h"

namespace SANS
{
namespace EGADS
{

//A two diensional plane is imply always z = 0
template<>
EGPlane<2>::EGPlane( const EGContext& context) : EGGeometryBase(context)
{
  double pts[9] = { 0, 0, 0,
                    1, 0, 0,
                    0, 1, 0 };

  EG_STATUS( EG_makeGeometry(context, SURFACE, PLANE, NULL, NULL, pts, &obj_) );
}

template<>
EGPlane<3>::EGPlane(const EGContext& context, const DLA::VectorS<Dim,Real>& X,
                                              const DLA::VectorS<Dim,Real>& dX,
                                              const DLA::VectorS<Dim,Real>& dY) : EGGeometryBase(context)
{
  double pts[9] = {  X[0],  X[1],  X[2],
                    dX[0], dX[1], dX[2],
                    dY[0], dY[1], dY[2] };

  EG_STATUS( EG_makeGeometry(context, SURFACE, PLANE, NULL, NULL, pts, &obj_) );
}

//Explicitly instantiate the plane
template class EGPlane<2>;
template class EGPlane<3>;

}
}
