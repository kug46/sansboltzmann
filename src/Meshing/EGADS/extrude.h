// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EG_EXTRUDE_H
#define EG_EXTRUDE_H

#include "EGBody.h"
#include "EGFace.h"
#include "EGLoop.h"

namespace SANS
{
namespace EGADS
{

template<int Dim>
EGBody<Dim> extrude( const EGBody<Dim>& src, double length, DLA::VectorS<3,double> dir )
{
  EGBody<Dim> result(src.getContext());

  EG_STATUS( EG_extrude( src, length, &dir[0], (ego*)result) );

  return result;
}

template<int Dim>
EGBody<Dim> extrude( const EGLoop<Dim>& src, double length, DLA::VectorS<3,double> dir )
{
  EGBody<Dim> result(src.getContext());

  EG_STATUS( EG_extrude( src, length, &dir[0], (ego*)result) );

  return result;
}

template<int Dim>
EGBody<Dim> extrude( const EGFace<Dim>& src, double length, DLA::VectorS<3,double> dir )
{
  EGBody<Dim> result(src.getContext());

  EG_STATUS( EG_extrude( src, length, &dir[0], (ego*)result) );

  return result;
}

}
}


#endif //EG_EXTRUDE_H
