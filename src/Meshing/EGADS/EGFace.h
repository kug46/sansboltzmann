// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EG_FACE_H
#define EG_FACE_H

//A topological face

#include <vector>
#include "EGEdge.h"
#include "EGLoop.h"

namespace SANS
{
namespace EGADS
{

class EGFaceBase : public EGTopologyBase
{
public:
  typedef DLA::VectorS<2, Real> ParamCoord;
  typedef DLA::VectorS<2, Real> ParamRange;

protected:
  explicit EGFaceBase( const EGContext& context ) : EGTopologyBase(context), u_range(0), v_range(0) {}

  //Constructor from an already existing ego
  explicit EGFaceBase( const EGObject& face );

  EGFaceBase( const EGFaceBase& face );
  EGFaceBase& operator=( const EGFaceBase& face );

public:
#if 0
  void reference( const EGFaceBase& ref );
#endif

  ParamRange getURange() const;
  ParamRange getVRange() const;

  Real getArea() const;
  bool inFace(const ParamCoord& uv) const;

  // Reveal the underlying geometry type
  bool isPlane()       const { return isGeomType(PLANE);       }
  bool isSpherical()   const { return isGeomType(SPHERICAL);   }
  bool isCylindrical() const { return isGeomType(CYLINDRICAL); }
  bool isRevolution()  const { return isGeomType(REVOLUTION);  }
  bool isToroidal()    const { return isGeomType(TOROIDAL);    }
  bool isTrimmed()     const { return isGeomType(TRIMMED);     }
  bool isBezier()      const { return isGeomType(BEZIER);      }
  bool isBspline()     const { return isGeomType(BSPLINE);     }
  bool isOffset()      const { return isGeomType(OFFSET);      }
  bool isConical()     const { return isGeomType(CONICAL);     }
  bool isExtrusion()   const { return isGeomType(EXTRUSION);   }

protected:
  void setRanges();
  bool isGeomType(const int GEOM) const;

  ParamRange u_range;
  ParamRange v_range;
};


template<int Dim_>
class EGFace;

//---------------------------------------------------------------------------//
// Cartesian 2D Face
//---------------------------------------------------------------------------//

template<>
class EGFace<2> : public EGFaceBase
{
public:
  static const int Dim = 2;
  typedef DLA::VectorS<Dim, Real> CartCoord;
  typedef typename EGFaceBase::ParamCoord ParamCoord;
  typedef typename EGFaceBase::ParamRange ParamRange;
  typedef DLA::MatrixS<Dim, 2, Real> JType;
  typedef DLA::MatrixS<2, Dim,Real> JinvType;

  typedef std::vector< EGSense< EGLoop<Dim> > > loop_vector;

  //Constructor from an already existing ego
  // cppcheck-suppress noExplicitConstructor
  EGFace( const EGObject& face ) : EGFaceBase(face) {}

  EGFace( const EGFace& face ) : EGFaceBase(face) {}

  EGFace( const EGContext& context, const EGSense< EGLoop<Dim> >& loop, const int mtype = SFORWARD );
  EGFace( const EGContext& context, const loop_vector& loops, const int mtype = SFORWARD );

  EGFace& operator=( const EGFace& face ) { EGFaceBase::operator=(face); return *this; }

  ParamCoord getEdgeUV(const EGEdge<2>& edge, const int sense, const Real t) const;

  CartCoord operator()( const ParamCoord& uv ) const;
  JType J( const ParamCoord& uv ) const;
  JinvType Jinv( const ParamCoord& uv ) const;

  std::vector< EGLoop<2> > getLoops() const;

protected:
  using EGFaceBase::u_range;
  using EGFaceBase::v_range;
};


//---------------------------------------------------------------------------//
// Cartesian 3D Face
//---------------------------------------------------------------------------//

template<>
class EGFace<3> : public EGFaceBase
{
public:
  static const int Dim = 3;
  typedef DLA::VectorS<Dim, Real> CartCoord;
  typedef typename EGFaceBase::ParamCoord ParamCoord;
  typedef typename EGFaceBase::ParamRange ParamRange;
  typedef DLA::MatrixS<Dim, 2, Real> JType;
  typedef DLA::MatrixS<2, Dim,Real> JinvType;

  typedef std::vector< EGSense< EGLoop<Dim> > > loop_vector;

  //Constructor from an already existing ego
  // cppcheck-suppress noExplicitConstructor
  EGFace( const EGObject& face) : EGFaceBase(face) {}

  EGFace( const EGFace& face ) : EGFaceBase(face) {}

  EGFace( const EGGeometryBase& geom, const EGSense< EGLoop<Dim> >& loop, const int mtype = SFORWARD );
  EGFace( const EGGeometryBase& geom, const loop_vector& loops, const int mtype = SFORWARD );
  EGFace( const EGGeometryBase& geom, const ParamRange& u_range, const ParamRange& v_range, const int mtype = SFORWARD );

  EGFace& operator=( const EGFace& face ) { EGFaceBase::operator=(face); return *this; }

  ParamCoord getEdgeUV(const EGEdge<3>& edge, const int sense, const Real t) const;
  void getEdgeUV(const EGEdge<3>& edge, const int sense, const Real t, ParamCoord& uv) const;

  CartCoord operator()( const ParamCoord& uv ) const;
  ParamCoord operator()( const CartCoord& X ) const;
  void invEvaluate( const CartCoord& X, ParamCoord& uv, CartCoord& Xclosest ) const;
  JType J( const ParamCoord& uv ) const;
  JinvType Jinv( const ParamCoord& uv ) const;

  CartCoord unitNormal( const ParamCoord& uv ) const;
  CartCoord unitNormal( const CartCoord& X ) const;

  std::vector< EGLoop<Dim> > getLoops() const;

protected:
  using EGFaceBase::u_range;
  using EGFaceBase::v_range;
};

}
}

#endif //EG_FACE_H
