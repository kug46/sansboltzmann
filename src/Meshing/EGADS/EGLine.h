// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EG_LINE_H
#define EG_LINE_H

//EG Line represents a line

#include "EGGeometryBase.h"
#include "EGNode.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

namespace SANS
{
namespace EGADS
{

template<int Dim_>
class EGLine : public EGGeometryBase
{
public:
  static const int Dim = Dim_;

  typedef DLA::VectorS<Dim,Real> CartCoord;

  EGLine(EGContext& context, const DLA::VectorS<Dim,Real>& X, const DLA::VectorS<Dim,Real>& dX);
  EGLine(const EGNode<Dim>& n0, const EGNode<Dim>& n1);
};

}
}

#endif //EG_LINE_H
