// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "EGStatus.h"
#include "tools/stringify.h"

namespace SANS
{
namespace EGADS
{

//=============================================================================
EGADSException::EGADSException( const int status ) : status(status)
{
  errString += "EGADS Error\n";

  switch (status)
  {
  case EGADS_EXISTS:
    errString += "EGADS: File exists";
    break;
  case EGADS_ATTRERR:
    errString += "EGADS: Attribute error";
    break;
  case EGADS_TOPOCNT:
    errString += "EGADS: Topological connection";
    break;
  case EGADS_OCSEGFLT:
    errString += "EGADS: OpenCASCADE segmentation fault";
    break;
  case EGADS_BADSCALE:
    errString += "EGADS: Bad scale";
    break;
  case EGADS_NOTORTHO:
    errString += "EGADS: Not orthogonal";
    break;
  case EGADS_DEGEN:
    errString += "EGADS: Degenerate";
    break;
  case EGADS_CONSTERR:
    errString += "EGADS: Construction Error";
    break;
  case EGADS_TOPOERR:
    errString += "EGADS: Topology error";
    break;
  case EGADS_GEOMERR:
    errString += "EGADS: Geometry error";
    break;
  case EGADS_NOTBODY:
    errString += "EGADS: Not a body";
    break;
  case EGADS_WRITERR:
    errString += "EGADS: Write error";
    break;
  case EGADS_NOTMODEL:
    errString += "EGADS: Not a model";
    break;
  case EGADS_NOLOAD:
    errString += "EGADS: No load";
    break;
  case EGADS_RANGERR:
    errString += "EGADS: Range error";
    break;
  case EGADS_NOTGEOM:
    errString += "EGADS: Not a geometry";
    break;
  case EGADS_NOTTESS:
    errString += "EGADS: Not tessilated";
    break;
  case EGADS_EMPTY:
    errString += "EGADS: Empty. A deleted ego was used in an EGADS call.";
    break;
  case EGADS_NOTTOPO:
    errString += "EGADS: Not a topology";
    break;
  case EGADS_REFERCE:
    errString += "EGADS: Reference Object Error (you really should never see this one.)";
    break;
  case EGADS_NOTXFORM:
    errString += "EGADS: Not transformed";
    break;
  case EGADS_NOTCNTX:
    errString += "EGADS: Not connected";
    break;
  case EGADS_MIXCNTX:
    errString += "EGADS: Mixed connection";
    break;
  case EGADS_NODATA:
    errString += "EGADS: No data";
    break;
  case EGADS_NONAME:
    errString += "EGADS: No name";
    break;
  case EGADS_INDEXERR:
    errString += "EGADS: Index error";
    break;
  case EGADS_MALLOC:
    errString += "EGADS: Malloc error";
    break;
  case EGADS_NOTOBJ:
    errString += "EGADS: Not an object";
    break;
  case EGADS_NULLOBJ:
    errString += "EGADS: Null object";
    break;
  case EGADS_NOTFOUND:
    errString += "EGADS: Not found";
    break;
  case EGADS_OUTSIDE:
    errString += "EGADS: Outside";
    break;
  case EGADS_TESSTATE:
    errString += "EGADS: Tessellation state error";
    break;
  default:
    errString += "EGADS: Unkown Error Status: " + stringify(status);
    break;
  }

}

//=============================================================================
void
EGADSException::during( const std::string& message )
{
  errString += "\n\n" + message;
}


} //namespace EGADS
} //namespace SANS
