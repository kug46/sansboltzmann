// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EG_BODY_H
#define EG_BODY_H

//A topological loop

#include <vector>
#include "EGNode.h"
#include "EGEdge.h"
#include "EGLoop.h"
#include "EGFace.h"

namespace SANS
{
namespace EGADS
{

//Forward declare EGModel so it can be a friend
template<int Dim_>
class EGModel;

template<int Dim_>
class EGBody;

template<int Dim>
class EGIntersection;

template<int Dim>
class EGBodyBase : public EGTopologyBase
{
public:
  explicit EGBodyBase( const EGContext& context ) : EGTopologyBase(context) {}
  // cppcheck-suppress noExplicitConstructor
  EGBodyBase( const EGObject& body );
  // cppcheck-suppress noExplicitConstructor
  EGBodyBase( const EGBodyBase& body ) : EGTopologyBase(body) {}

  EGBodyBase& operator=( const EGBodyBase& body ) { EGTopologyBase::operator=(body); return *this; }

  std::vector< EGNode<Dim> > getNodes() const;
  std::vector< EGEdge<Dim> > getEdges() const;
  std::vector< EGLoop<Dim> > getLoops() const;
  std::vector< EGFace<Dim> > getFaces() const;
  //std::vector< EGNode<Dim> > getShells() const;

  std::vector< EGNode<Dim> > getNodes( const EGTopologyBase& ref ) const;
  std::vector< EGEdge<Dim> > getEdges( const EGTopologyBase& ref ) const;
  std::vector< EGLoop<Dim> > getLoops( const EGTopologyBase& ref ) const;
  std::vector< EGFace<Dim> > getFaces( const EGTopologyBase& ref ) const;
  //std::vector< EGNode<Dim> > getShells( const EGTopologyBase& ref ) const;

  int nNodes() const { return nTopos(NODE); }
  int nEdges() const { return nTopos(EDGE); }
  int nLoops() const { return nTopos(LOOP); }
  int nFaces() const { return nTopos(FACE); }
  int nTopos(const int TOPO) const;

  bool isWire()  const { return isTopoType( WIREBODY ); }
  bool isFace()  const { return isTopoType( FACEBODY ); }
  bool isSheet() const { return isTopoType( SHEETBODY ); }
  bool isSolid() const { return isTopoType( SOLIDBODY ); }

  void save( const std::string& name ) const;

  int getBodyIndex( const EGTopologyBase& topo ) const { return EG_indexBodyTopo(obj_, topo); }

  template<class EGTopo>
  std::vector< EGTopo > getTopos(const int TOPO) const;

  friend class EGModel<Dim>;
protected:

  template<class EGTopo>
  std::vector< EGTopo > getTopos(const int TOPO, const EGTopologyBase& ref) const;

  void setModel(ego model) const { const_cast<EGBodyBase*>(this)->reference_ = true; }
};

template<>
class EGBody<2> : public EGBodyBase<2>
{
public:
  static const int Dim = 2;
  typedef DLA::VectorS<Dim, Real> CartCoord;
  typedef typename EGFace<Dim>::loop_vector loop_vector;

  EGBody( const EGBody& body ) : EGBodyBase(body) {}
  ~EGBody();

  explicit EGBody( const EGContext& context ) : EGBodyBase(context) {}
  explicit EGBody( const EGIntersection<Dim>& edges );

  // cppcheck-suppress noExplicitConstructor
  EGBody( const EGObject& body ) : EGBodyBase(body) {}
  // cppcheck-suppress noExplicitConstructor
  EGBody( const EGFace<Dim>& face ); //Constructors for making a FACEBODY
  EGBody( const EGContext& context, const EGSense< EGLoop<Dim> >& loop, const int mtype = SFORWARD );
  EGBody( const EGContext& context, const loop_vector& loops, const int mtype = SFORWARD );

};


template<>
class EGBody<3> : public EGBodyBase<3>
{
public:
  static const int Dim = 3;
  typedef DLA::VectorS<Dim, Real> CartCoord;
  typedef typename EGFace<Dim>::loop_vector loop_vector;

  EGBody( const EGBody& body ) : EGBodyBase(body) {}
  ~EGBody();

  EGBody& operator=( const EGBody& body ) { EGBodyBase::operator=(body); return *this; }

  explicit EGBody( const EGContext& context ) : EGBodyBase(context) {}
  explicit EGBody( const EGIntersection<Dim>& edges );

  // cppcheck-suppress noExplicitConstructor
  EGBody( const EGObject& body ) : EGBodyBase(body) {}
  // cppcheck-suppress noExplicitConstructor
  EGBody( const EGLoop<Dim>& loop ); //Constructors for making a WIREBODY
  // cppcheck-suppress noExplicitConstructor
  EGBody( const EGFace<Dim>& face ); //Constructors for making a FACEBODY
  EGBody( const EGGeometryBase& geom, const EGSense< EGLoop<Dim> >& loop, const int mtype = SFORWARD );
  EGBody( const EGGeometryBase& geom, const loop_vector& loops, const int mtype = SFORWARD );
};

template<>
class EGBody<4> : public EGTopologyBase
{
public:
  static const int Dim = 4;
  typedef DLA::VectorS<Dim, Real> CartCoord;

  EGBody( const EGBody& body ) : EGTopologyBase(body) {}
  ~EGBody();

  explicit EGBody( const EGContext& context ) : EGTopologyBase(context) {}
  EGBody( const EGObject& body ) : EGTopologyBase(body) {}
};

}
}

#endif //EG_BODY_H
