// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EG_MODEL_H
#define EG_MODEL_H

//A model

#include <vector>
#include "EGBody.h"

namespace SANS
{
namespace EGADS
{

template<int Dim_>
class EGModel : public EGObject
{
public:
  static const int Dim = Dim_;
  typedef DLA::VectorS<Dim, Real> CartCoord;

  explicit EGModel( const EGContext& context ) : EGObject(context), autoDelete_(true) {}
  explicit EGModel( const ego obj );
  explicit EGModel( const EGObject& obj );

  EGModel( const EGModel& model ) : EGObject(model) {}
  EGModel( EGModel&& model ) : EGObject(model), autoDelete_(model.autoDelete_)
  { model.obj_ = NULL; } // Prevents the temporary object from deleting the model

  EGModel( const EGContext& context, const std::string& name, int flags = 0 ) : EGObject(context) { load(name, flags); }

  EGModel& operator=( const EGModel& model );
  EGModel& operator=( EGModel&& model );

  //Constructor for making a model from a body
  explicit EGModel(EGBody<Dim>& body);
  //Constructor for making a model from multiple bodies
  explicit EGModel( const std::vector< EGBody<Dim> >& bodies);

  //Only models should automatically delete them selves
  ~EGModel() { if (autoDelete_) Delete(); }

  std::vector< EGBody<Dim_> > getBodies() const;
  Real getSize() const;

  void load( const std::string& name, int flags = 0 ); // flags = 1 - Don't split closed and periodic entities
  void save( const std::string& name ) const;

  std::vector<ego> getChildren() const;

  // counts the total number of faces in the model
  int nFaces() const;

  void setAutoDelete(const bool autoDelete) { autoDelete_ = autoDelete; }
protected:
  bool autoDelete_;
};

template<>
class EGModel<4> : public EGObject
{
public:
  static const int Dim = 4;

  explicit EGModel( const ego obj ) : EGObject(obj) {}
  // TODO SANS_AVRO constructor from tesseract model

  explicit EGModel( const std::vector< EGBody<4> >& bodies) : EGObject(bodies[0].getContext())
  {
    for (std::size_t i = 0; i < bodies.size(); i++)
      children_.push_back( bodies[i] );
  }

  void setChildren( const std::vector<ego>& children )
  {
    children_.clear();
    for (std::size_t i = 0; i < children.size(); i++)
      children_.push_back( children[i] );
  }

  std::vector<ego> getChildren() const
  {
    //std::vector<EGObject> children;
    //SANS_DEVELOPER_EXCEPTION("implement");
    return children_;
  }

  void setAutoDelete(const bool autoDelete) {}

private:
  std::vector<ego> children_;

};

}
}

#endif //EG_MODEL_H
