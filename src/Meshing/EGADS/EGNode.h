// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EG_NODE_H
#define EG_NODE_H

//A topological node

#include "EGTopologyBase.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

namespace SANS
{
namespace EGADS
{

template<int Dim_>
class EGNode : public EGTopologyBase
{
public:
  static const int Dim = Dim_;

  typedef DLA::VectorS<Dim,Real> CartCoord;

  // cppcheck-suppress noExplicitConstructor
  EGNode(const EGObject& node);
  EGNode(const EGContext& context, const CartCoord& X);

  operator       CartCoord&()       { return X_; }
  operator const CartCoord&() const { return X_; }

        Real& operator[](const int n)       { return X_[n]; }
  const Real& operator[](const int n) const { return X_[n]; }
protected:
  CartCoord X_;
};

}
}

#endif //EG_NODE_H
