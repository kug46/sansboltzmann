// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EG_APPROXIMATE_H
#define EG_APPROXIMATE_H

//EG Approximate creates a geometric B-Spline

#include "EGGeometryBase.h"
#include "EGNode.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

namespace SANS
{
namespace EGADS
{

template<int Dim_>
class EGApproximate : public EGGeometryBase
{
public:
  static const int Dim = Dim_;

  enum endConditions
  {
    Natural,
    Slope,
    SlopeQuadratic,
  };

  typedef DLA::VectorS<Dim,Real> CartCoord;

  EGApproximate(const EGContext& context, const endConditions mDeg, const double tol, const std::string& filename, bool Surface = false);
  EGApproximate(const EGContext& context, const endConditions mDeg, const double tol, const std::vector<CartCoord>& pts, bool Surface = false);

  CartCoord operator()( const double t ) const; // evaluate
  Real operator()( const CartCoord& X ) const;  // inverse evaluate
};

}
}

#endif //EG_APPROXIMATE_H
