// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EG_SENSE_H
#define EG_SENSE_H

//A class for attaching a sense to an EGObject

#include <type_traits>

#include "EGObject.h"

namespace SANS
{
namespace EGADS
{

template< class EGType >
class EGSense
{
public:

  EGSense( const EGSense& copy ) : obj_(copy.obj_), sense_(copy.sense_) {}
  EGSense( const EGType& obj, const int sense ) : obj_(obj), sense_(sense) {}

  EGSense& operator=( const EGSense& copy )
  {
    if ( this != &copy )
    {
      obj_ = copy.obj_;
      sense_ = copy.sense_;
    }
    return *this;
  }

  operator ego() const { return obj_; }
  operator ego*() const { return obj_; }
  const EGType& obj() const { return obj_; }

  const EGContext& getContext() const { return obj_.getContext(); }

  operator int() const { return sense_; }
protected:
  EGType obj_;
  int sense_;
};

}
}

#endif //EG_SENSE_H
