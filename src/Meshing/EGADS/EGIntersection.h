// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EG_INTERSECTION_H
#define EG_INTERSECTION_H

// A base class for all EGADS objects

#include "EGStatus.h"
#include "EGContext.h"
#include "EGEdge.h"
#include "EGFace.h"
#include <vector>

namespace SANS
{
namespace EGADS
{

template<int Dim>
class EGBody;

template<int Dim>
class EGIntersection
{
public:
  EGIntersection( const EGIntersection& copy ) = delete;
  EGIntersection( const EGIntersection&& copy ) = delete;
  EGIntersection( const EGBody<Dim>& src, const EGBody<Dim>& tool ) : context_(src.getContext()), src_(src), nEdge_(0), pFacEdg_(NULL), model_(NULL)
  {
    EG_STATUS( EG_intersection(src, tool, &nEdge_, &pFacEdg_, &model_) );
  }

  ~EGIntersection() { EG_free(pFacEdg_); EG_STATUS( EG_deleteObject(model_) );}

  EGIntersection& operator=( const EGIntersection& copy ) = delete;
  EGIntersection& operator=( const EGIntersection&& copy ) = delete;

  const EGContext& getContext() const { return context_; }
  ego body() const { return src_; }
  int nEdge() const { return nEdge_; }
  EGFace<Dim> face(const int n) const { return EGObject( pFacEdg_[2*n+0]); }
  EGEdge<Dim> edge(const int n) const { return EGObject( pFacEdg_[2*n+1]); }
  ego* facEdg() const { return pFacEdg_; }
  std::vector<EGFace<Dim>> uniqueFaces() const
  {
    std::vector<EGADS::EGFace<Dim>> faces;
    for (int i = 0; i < nEdge_; i++)
    {
      EGFace<Dim> faceIntersect = this->face(i);
      // Don't add in the same face twice
      bool found = false;
      for ( EGADS::EGFace<Dim> face : faces )
        if ((ego)face == (ego)faceIntersect)
        {
          found = true;
          break;
        }
      if (!found)
        faces.emplace_back(faceIntersect);
    }
    return faces;
  }

  //This is mostly useful for debugging purposes
  void saveModel( const std::string& name ) { EG_STATUS( EG_saveModel(model_, name.c_str()) ); }
protected:
  const EGContext context_;
  ego src_;
  int nEdge_;
  ego* pFacEdg_;
  ego model_;
};

}
}

#endif //EG_OBJECT_H
