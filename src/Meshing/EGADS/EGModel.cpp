// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "EGModel.h"

namespace SANS
{
namespace EGADS
{

template<int Dim_>
EGModel<Dim_>::EGModel(const ego obj) : EGModel(EGObject(obj))
{
}

template<int Dim_>
EGModel<Dim_>::EGModel(const EGObject& obj) : EGObject(obj), autoDelete_(true)
{
  int oclass, mtype;
  ego topRef, prev, next;

  //Create the face body
  EG_STATUS( EG_getInfo(obj_, &oclass, &mtype, &topRef, &prev, &next) );

  if (oclass == BODY)
  {
    EGBody<Dim> body(obj);

    //Create the model
    EG_STATUS( EG_makeTopology(body.getContext(), NULL, MODEL, 0, NULL, 1, body, NULL, &obj_) );

    //Let the body know it now belongs to a model
    body.setModel(obj_);
  }
  else
    SANS_ASSERT_MSG( oclass == MODEL, "Class should be BODY (%d) or MODEL (%d): oclass = %d", BODY, MODEL, oclass );
}

template<int Dim_>
EGModel<Dim_>::EGModel( EGBody<Dim>& body ) : EGObject(body.getContext()), autoDelete_(true)
{
  //Create the model
  EG_STATUS( EG_makeTopology(body.getContext(), NULL, MODEL, 0, NULL, 1, body, NULL, &obj_) );

  //Let the body know it now belongs to a model
  body.setModel(obj_);
}

template<int Dim_>
EGModel<Dim_>::EGModel( const std::vector< EGBody<Dim> >& bodies ) : EGObject(bodies[0].getContext()), autoDelete_(true)
{
  std::vector<ego> bods(bodies.size());
  for ( std::size_t i = 0; i < bodies.size(); i++ )
    bods[i] = bodies[i];

  //Create the face body
  EG_STATUS( EG_makeTopology(bodies[0].getContext(), NULL, MODEL, 0, NULL, (int)bodies.size(), bods.data(), NULL, &obj_) );

  //Let the bodies know they now belongs to a model
  for ( std::size_t i = 0; i < bodies.size(); i++ )
    bodies[i].setModel(obj_);
}

template<int Dim_>
EGModel<Dim_>&
EGModel<Dim_>::operator=( const EGModel& model )
{
  EGObject::operator=(model);
  return *this;
}

template<int Dim_>
EGModel<Dim_>&
EGModel<Dim_>::operator=( EGModel&& model )
{
  EGObject::operator=(model);
  model.autoDelete_ = false;
  model.obj_ = NULL;  // Prevents the temporary object from deleting the model
  return *this;
}

template<int Dim_>
void
EGModel<Dim_>::load( const std::string& name, int flags )
{
  //Load the model from the file
  EG_STATUS( EG_loadModel(context_, flags, name.c_str(), &obj_) );
}

template<int Dim_>
void
EGModel<Dim_>::save( const std::string& name ) const
{
  //Save the model to a file
  EG_STATUS( EG_saveModel(obj_, name.c_str()) );
}

template<int Dim_>
std::vector< EGBody<Dim_> >
EGModel<Dim_>::getBodies() const
{
  ego ref;
  int oclass, mtype, nchild;
  double data[4];
  ego* pchldrn;
  int *psens;

  EG_STATUS( EG_getTopology( *this, &ref, &oclass, &mtype, data, &nchild, &pchldrn, &psens ) );

  SANS_ASSERT( oclass == MODEL );

  std::vector< EGBody<Dim> > bodies;
  for (int i = 0; i < nchild; i++)
    bodies.emplace_back( EGObject( pchldrn[i]) );

  return bodies;
}

template<int Dim_>
std::vector<ego>
EGModel<Dim_>::getChildren() const
{
  std::vector<ego> objects;
  std::vector< EGBody<Dim_> > bodies = getBodies();
  for (size_t i = 0; i < bodies.size(); i++)
  {
    // we need to add the lower-dimensional entities in the same
    // order so that the list of lower-dimensional entities is the
    // same across multiple processors running with the same EGADS model
    // but different instances of the model itself.
    std::vector<EGNode<Dim_>> nodes = bodies[i].getNodes();
    std::vector<EGEdge<Dim_>> edges = bodies[i].getEdges();
    std::vector<EGFace<Dim_>> faces = bodies[i].getFaces();

    for (size_t j = 0; j < nodes.size(); j++) objects.emplace_back( nodes[j] );
    for (size_t j = 0; j < edges.size(); j++) objects.emplace_back( edges[j] );
    for (size_t j = 0; j < faces.size(); j++) objects.emplace_back( faces[j] );
  }

  return objects;
}

template<>
std::vector<ego>
EGModel<1>::getChildren() const
{
  SANS_DEVELOPER_EXCEPTION( "should not be reached in 1d" );
}

template<int Dim_>
Real
EGModel<Dim_>::getSize() const
{
  double  box[6], size;
  EG_STATUS( EG_getBoundingBox(obj_, box) );

                            size = box[3]-box[0];
  if (size < box[4]-box[1]) size = box[4]-box[1];
  if (size < box[5]-box[2]) size = box[5]-box[2];

  return size;
}


template<int Dim_>
int
EGModel<Dim_>::nFaces() const
{
  ego ref;
  int oclass, mtype, nbody;
  double data[4];
  ego* pbody;
  int *psens;
  int totface = 0;

  // Get all the bodies in the model
  EG_STATUS( EG_getTopology( obj_, &ref, &oclass, &mtype, data, &nbody, &pbody, &psens ) );

  for ( int ibody = 0; ibody < nbody; ibody++ )
  {
    ego body = pbody[ibody];

    int nface;
    ego *pfaces;

    //Extract all faces
    EG_STATUS( EG_getBodyTopos(body, NULL, FACE, &nface, &pfaces) );
    EG_free(pfaces); pfaces = NULL;

    totface += nface;
  }

  return totface;
}

//Explicitly instantiate the class
template class EGModel<2>;
template class EGModel<3>;

} // EGADS

} // SANS
