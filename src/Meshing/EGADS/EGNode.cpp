// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "EGNode.h"

namespace SANS
{
namespace EGADS
{

template<int Dim>
EGNode<Dim>::EGNode( const EGObject& node ) : EGTopologyBase(node)
{
  ego ref;
  int oclass, mtype, nchild;
  double xyz[4];
  ego* pchldrn;
  int *psens;

  //Check that the ego is the correct oclass
  EG_STATUS( EG_getTopology( obj_, &ref, &oclass, &mtype, xyz, &nchild, &pchldrn, &psens ) );

  SANS_ASSERT( oclass == NODE );
  X_ = CartCoord(xyz, Dim);
}


template<>
EGNode<2>::EGNode(const EGContext& context, const DLA::VectorS<Dim,Real>& X) : EGTopologyBase(context), X_(X)
{
  double xyz[3] = { X[0], X[1], 0 };

  EG_STATUS( EG_makeTopology(context, NULL, NODE, 0, xyz, 0, NULL, NULL, &obj_) );
}

template<>
EGNode<3>::EGNode(const EGContext& context, const DLA::VectorS<Dim,Real>& X) : EGTopologyBase(context), X_(X)
{
  double xyz[3] = { X[0], X[1], X[2] };

  EG_STATUS( EG_makeTopology(context, NULL, NODE, 0, xyz, 0, NULL, NULL, &obj_) );
}

template class EGNode<2>;
template class EGNode<3>;
}
}
