// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EG_REPLACEFACES_H
#define EG_REPLACEFACES_H

#include "EGBody.h"

namespace SANS
{
namespace EGADS
{

template<int Dim>
EGBody<Dim> replaceFaces( const EGBody<Dim>& body, const std::vector< ego >& faces )
{
  EGBody<Dim> result(body.getContext());
  // Must be an even count of face/replacement
  SANS_ASSERT( faces.size() % 2 == 0);

  EG_STATUS( EG_replaceFaces( body, (int)faces.size()/2, (ego*)faces.data(), (ego*)result) );

  return result;
}

}
}


#endif //EG_REPLACEFACES_H
