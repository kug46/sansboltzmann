// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "EGFace.h"
#include "EGPlane.h"

#include <cmath> // sqrt

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/tools/cross.h"

namespace SANS
{
namespace EGADS
{

EGFaceBase::EGFaceBase( const EGObject& face) : EGTopologyBase(face), u_range(0), v_range(0)
{
  ego ref;
  int oclass, mtype, nchild;
  double uvrange[4];
  ego* pchldrn;
  int *psens;

  //Check that the ego is the correct oclass
  EG_STATUS( EG_getTopology( obj_, &ref, &oclass, &mtype, uvrange, &nchild, &pchldrn, &psens ) );

  SANS_ASSERT( oclass == FACE );

  u_range[0] = uvrange[0];
  u_range[1] = uvrange[1];
  v_range[0] = uvrange[2];
  v_range[1] = uvrange[3];
}

EGFaceBase::EGFaceBase( const EGFaceBase& face ) : EGTopologyBase(face)
{
  setRanges();
}

EGFaceBase&
EGFaceBase::operator=( const EGFaceBase& face )
{
  EGTopologyBase::operator=(face);
  setRanges();
  return *this;
}

void
EGFaceBase::setRanges()
{
  double range[4];
  int per;
  EG_STATUS( EG_getRange(obj_, range, &per));
  u_range[0] = range[0];
  u_range[1] = range[1];
  v_range[0] = range[2];
  v_range[1] = range[3];
}

Real
EGFaceBase::getArea() const
{
  double area;
  EG_STATUS( EG_getArea(obj_, NULL, &area));
  return area;
}

bool
EGFaceBase::inFace(const ParamCoord& uv) const
{
  return EG_inFace(obj_, &uv[0]) == EGADS_SUCCESS;
}

#if 0
void
EGFaceBase::reference( const EGFaceBase& ref )
{
  if ( obj_ != ref.obj_ )
  {
    EGObject::reference(ref);
    setRanges();
  }
}
#endif

typename EGFaceBase::ParamRange
EGFaceBase::getURange() const
{
  return u_range;
}

typename EGFaceBase::ParamRange
EGFaceBase::getVRange() const
{
  return v_range;
}

bool
EGFaceBase::isGeomType(const int GEOM) const
{
  ego ref;
  int oclass, mtype, nchild;
  double uvrange[4];
  ego* pchldrn;
  int *psens;

  //Check that the ego is the correct oclass
  EG_STATUS( EG_getTopology( obj_, &ref, &oclass, &mtype, uvrange, &nchild, &pchldrn, &psens ) );

  // No geometry, so it can't be the GEOM type
  if (ref == NULL) return false;

  ego topRef, prev, next;

  //Get the mtype for the reference geometry
  EG_STATUS( EG_getInfo(ref, &oclass, &mtype, &topRef, &prev, &next) );

  SANS_ASSERT(oclass == SURFACE); // Just for sanity's sake

  return mtype == GEOM;
}


//---------------------------------------------------------------------------//
// Cartesian 2D Face
//---------------------------------------------------------------------------//

EGFace<2>::EGFace(const EGContext& context, const EGSense< EGLoop<Dim> >& loop, const int mtype )
 : EGFaceBase(context)
{
  //Create the Loop
  EGPlane<Dim> plane(context);

  int sense = loop;

  //Create the face
  EG_STATUS( EG_makeTopology(context, plane, FACE, mtype, NULL, 1, loop, &sense, &obj_) );

  setRanges();
}

EGFace<2>::EGFace(const EGContext& context, const loop_vector& loops, const int mtype )
 : EGFaceBase(context)
{
  std::vector<int> senses( loops.size() );
  std::vector<ego> loop_ojects( loops.size() );

  for ( std::size_t i = 0; i < loops.size(); i++ )
  {
    senses[i] = loops[i];
    loop_ojects[i] = loops[i];
  }

  EGPlane<Dim> plane(context);

  //Create the face
  EG_STATUS( EG_makeTopology(context, plane, FACE, mtype, NULL, loops.size(), &loop_ojects[0], &senses[0], &obj_) );

  setRanges();
}

typename EGFace<2>::ParamCoord
EGFace<2>::getEdgeUV(const EGEdge<2>& edge, const int sense, const Real t) const
{
  ParamCoord uv;
  EG_STATUS( EG_getEdgeUV(obj_, edge, sense, t, &uv[0]) );
  return uv;
}

typename EGFace<2>::CartCoord
EGFace<2>::operator()( const ParamCoord& uv ) const
{
  double UV[2] = {uv[0], uv[1]};
  double xyz[18];
  EG_STATUS( EG_evaluate(obj_, UV, xyz) );
  return CartCoord(xyz, Dim);
}


typename EGFace<2>::JType
EGFace<2>::J( const ParamCoord& uv ) const
{
  double UV[2] = {uv[0], uv[1]};
  double xyz[18];
  EG_STATUS( EG_evaluate(obj_, UV, xyz) );

  //Get the first derivatives
  JType J;
  for (int i = 0; i < Dim; i++)
  {
    J(i,0) = xyz[3 + i];
    J(i,1) = xyz[6 + i];
  }

  //Return the Jacobian
  return J;
}

typename EGFace<2>::JinvType
EGFace<2>::Jinv( const ParamCoord& uv ) const
{
  //This is a simple inverse if the Jacobian is in 2D

  // The formula for inverting a 2-by-2 matrix is used:
  // For A = [ [a, b]
  //           [c, d] ],
  // its inverse A^(-1) is
  //   A^(-1) = 1/det(A) * [ [ d, -b]
  //                         [-c,  a] ]

  JType J = this->J(uv);

  JinvType Jinv;

  Real det = J(0,0)*J(1,1) - J(0,1)*J(1,0);
  Jinv(0,0) =  J(1,1)/det; Jinv(0,1) = -J(0,1)/det;
  Jinv(1,0) = -J(1,0)/det; Jinv(1,1) =  J(0,0)/det;

  return Jinv;
}

std::vector< EGLoop<2> >
EGFace<2>::getLoops() const
{
  ego ref;
  int oclass, mtype, nchild;
  double data[4];
  ego* pchldrn;
  int *psens;

  EG_STATUS( EG_getTopology( obj_, &ref, &oclass, &mtype, data, &nchild, &pchldrn, &psens ) );

  SANS_ASSERT( oclass == FACE );

  std::vector< EGLoop<Dim> > loops;
  for (int i = 0; i < nchild; i++)
    loops.emplace_back( EGObject( pchldrn[i]) );

  loops.shrink_to_fit();
  return loops;
}

//---------------------------------------------------------------------------//
// Cartesian 3D Face
//---------------------------------------------------------------------------//

EGFace<3>::EGFace(const EGGeometryBase& geom, const EGSense< EGLoop<Dim> >& loop, const int mtype )
 : EGFaceBase(geom.getContext())
{
  int sense = loop;

  //Create the Loop
  EG_STATUS( EG_makeTopology(context_, geom, FACE, mtype, NULL, 1, loop, &sense, &obj_) );

  setRanges();
}

EGFace<3>::EGFace(const EGGeometryBase& geom, const loop_vector& loops, const int mtype )
 : EGFaceBase(geom.getContext())
{
  std::vector<int> senses( loops.size() );
  std::vector<ego> loop_ojects( loops.size() );

  for ( std::size_t i = 0; i < loops.size(); i++ )
  {
    senses[i] = loops[i];
    loop_ojects[i] = loops[i];
  }

  //Create the Loop
  EG_STATUS( EG_makeTopology(context_, geom, FACE, mtype, NULL, loops.size(), &loop_ojects[0], &senses[0], &obj_) );

  setRanges();
}

EGFace<3>::EGFace( const EGGeometryBase& geom, const ParamRange& u_range, const ParamRange& v_range, const int mtype )
 : EGFaceBase(geom.getContext())
{
  double uvrange[4];
  uvrange[0] = u_range[0];
  uvrange[1] = u_range[1];
  uvrange[2] = v_range[0];
  uvrange[3] = v_range[1];

  //Create the Loop
  EG_STATUS( EG_makeFace(geom, mtype, uvrange, &obj_) );

  setRanges();
}

typename EGFace<3>::ParamCoord
EGFace<3>::getEdgeUV(const EGEdge<3>& edge, const int sense, const Real t) const
{
  ParamCoord uv;
  getEdgeUV(edge, sense, t, uv );
  return uv;
}

void
EGFace<3>::getEdgeUV(const EGEdge<3>& edge, const int sense, const Real t, ParamCoord& uv) const
{
  EG_STATUS( EG_getEdgeUV(obj_, edge, sense, t, &uv[0]) );
}

typename EGFace<3>::CartCoord
EGFace<3>::operator()( const ParamCoord& uv ) const
{
  double UV[2] = {uv[0], uv[1]};
  double xyz[18];
  EG_STATUS( EG_evaluate(obj_, UV, xyz) );
  return CartCoord(xyz, Dim);
}

typename EGFace<3>::ParamCoord
EGFace<3>::operator()( const CartCoord& X ) const
{
  double xyz[3] = {X[0], X[1], X[2]};
  double dum[3];
  double uv[2];
  EG_STATUS( EG_invEvaluate(obj_, xyz, uv, dum) );
  return ParamCoord(uv, ParamCoord::M);
}

void EGFace<3>::invEvaluate( const CartCoord& X, ParamCoord& uv, CartCoord& Xclosest ) const
{
  double xyz[3] = {X[0], X[1], X[2]};
  double result[3];
  double uvs[2];
  EG_STATUS( EG_invEvaluate(obj_, xyz, uvs, result) );

  uv[0] = uvs[0];
  uv[1] = uvs[1];

  Xclosest[0] = result[0];
  Xclosest[1] = result[1];
  Xclosest[2] = result[2];
}

typename EGFace<3>::JType
EGFace<3>::J( const ParamCoord& uv ) const
{
  double UV[2] = {uv[0], uv[1]};
  double xyz[18];
  EG_STATUS( EG_evaluate(obj_, UV, xyz) );

  //Get the first derivatives
  JType J;
  for (int i = 0; i < Dim; i++)
  {
    J(i,0) = xyz[3 + i];
    J(i,1) = xyz[6 + i];
  }

  //Return the Jacobian
  return J;
}

typename EGFace<3>::JinvType
EGFace<3>::Jinv( const ParamCoord& uv ) const
{
  double UV[2] = {uv[0], uv[1]};
  double xyz[18];
  EG_STATUS( EG_evaluate(obj_, UV, xyz) );

  //Get the first derivatives
  JinvType JT;
  for (int i = 0; i < Dim; i++)
  {
    JT(0,i) = xyz[3 + i];
    JT(1,i) = xyz[6 + i];
  }

  // Solving the system
  // J*[dX] = J*Transpose(J)*[dUV]
  // where J projects dX onto the tangent of the Face

  DLA::MatrixS<2,2,Real> JJT = JT*Transpose(JT);

  // The formula for inverting a 2-by-2 matrix is used:
  // For A = [ [a, b]
  //           [c, d] ],
  // its inverse A^(-1) is
  //   A^(-1) = 1/det(A) * [ [ d, -b]
  //                         [-c,  a] ]

  DLA::MatrixS<2,2,Real> JJTinv;

  Real det = JJT(0,0)*JJT(1,1) - JJT(0,1)*JJT(1,0);
  JJTinv(0,0) =  JJT(1,1)/det; JJTinv(0,1) = -JJT(0,1)/det;
  JJTinv(1,0) = -JJT(1,0)/det; JJTinv(1,1) =  JJT(0,0)/det;

  return JJTinv*JT;
}

typename EGFace<3>::CartCoord
EGFace<3>::unitNormal( const ParamCoord& uv ) const
{
  double UV[2] = {uv[0], uv[1]};
  double xyz[18];
  EG_STATUS( EG_evaluate(obj_, UV, xyz) );

  //Get the first derivatives
  CartCoord Xu, Xv;
  for (int i = 0; i < Dim; i++)
  {
    Xu[i] = xyz[3+i];
    Xv[i] = xyz[6+i];
  }

  // Cross the first derivatives to get the normal
  CartCoord N = cross(Xu, Xv);

  // Normalize
  N /= sqrt(dot(N,N));

  int oclass, mtype;
  ego topRef, prev, next;

  // Get the mtype to correct the direction of the normal
  EG_STATUS( EG_getInfo(obj_, &oclass, &mtype, &topRef, &prev, &next) );

  N *= mtype;

  //Return the unit normal
  return N;
}

typename EGFace<3>::CartCoord
EGFace<3>::unitNormal( const CartCoord& X ) const
{
  // Get the parametric coordinate and compute the unit normal
  return unitNormal( (*this)(X) );
}

std::vector< EGLoop<3> >
EGFace<3>::getLoops() const
{
  ego ref;
  int oclass, mtype, nchild;
  double data[4];
  ego* pchldrn;
  int *psens;

  EG_STATUS( EG_getTopology( obj_, &ref, &oclass, &mtype, data, &nchild, &pchldrn, &psens ) );

  SANS_ASSERT( oclass == FACE );

  std::vector< EGLoop<Dim> > loops;
  for (int i = 0; i < nchild; i++)
    loops.emplace_back( EGObject( pchldrn[i]) );

  loops.shrink_to_fit();
  return loops;
}

}
}
