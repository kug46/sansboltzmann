// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "EGCircle.h"

namespace SANS
{
namespace EGADS
{

EGCircle<2>::EGCircle( const EGContext& context, const DLA::VectorS<Dim,Real>& X, const Real& r )
 : EGGeometryBase(context)
{
  double prv[] = { X[0], X[1], 0, //[x,y,z]
                      1,    0, 0, //[dx1,dx2,dx3]
                      0,    1, 0, //[dy1,dy1,dy3]
                      r         }; //radius

  EG_STATUS( EG_makeGeometry(context, CURVE, CIRCLE, NULL, NULL, prv, &obj_) );
}

EGCircle<3>::EGCircle( const EGContext& context, const DLA::VectorS<Dim,Real>& X,
                       const DLA::VectorS<Dim,Real>& dX1, const DLA::VectorS<Dim,Real>& dX2, const Real& r )
 : EGGeometryBase(context)
{
  double prv[] = {   X[0],   X[1],   X[2],  //[x,y,z]
                   dX1[0], dX1[1], dX1[2],  //[dx1,dx2,dx3]
                   dX2[0], dX2[1], dX2[2],  //[dy1,dy1,dy3]
                      r         }; //radius

  EG_STATUS( EG_makeGeometry(context, CURVE, CIRCLE, NULL, NULL, prv, &obj_) );
}

}
}
