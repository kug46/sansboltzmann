// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EG_EDGE_H
#define EG_EDGE_H

//A topological edge

#include "EGTopologyBase.h"
#include "EGNode.h"
#include "EGLine.h"
#include "EGCircle.h"
#include "EGSense.h"
#include "EGApproximate.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

namespace SANS
{
namespace EGADS
{

template<int Dim_>
class EGEdge : public EGTopologyBase
{
public:
  static const int Dim = Dim_;
  typedef DLA::VectorS<Dim, Real> CartCoord;
  typedef Real ParamCoord;
  typedef DLA::VectorS<2, Real> ParamRange;
  typedef DLA::MatrixS<Dim,1, Real> JType;
  typedef DLA::MatrixS<1,Dim, Real> JinvType;

  EGEdge( const EGEdge& edge ) : EGTopologyBase(edge), range_(edge.range_), mtype_(edge.mtype_) {}
  EGEdge& operator=( const EGEdge& edge ) { EGTopologyBase::operator=(edge); return *this; }

  // cppcheck-suppress noExplicitConstructor
  EGEdge(const EGContext& context) : EGTopologyBase(context), range_(0), mtype_(-1) {}

  //Constructor from an already existing ego
  // cppcheck-suppress noExplicitConstructor
  EGEdge(const EGObject& edge);

  //Constructor for a simple line
  EGEdge(const EGNode<Dim>& n0, const EGNode<Dim>& n1);

  //Constructor for a simple line based on a vector of nodes (must be lenght 2)
  explicit EGEdge(const std::vector< EGNode<Dim> >& nodes);

  //Constructor for a circle
  explicit EGEdge(const EGCircle<Dim>& circle);

  //Constructor for a closed B-Spline. The a node is constructed at the closing point
  explicit EGEdge(const EGApproximate<Dim>& spline);

  //Constructor for a closed B-Spline. The node is assumed to be at the closing point
  EGEdge(const EGApproximate<Dim>& spline, const EGNode<Dim>& n0);

  //Constructor for an open B-Spline.
  EGEdge(const EGApproximate<Dim>& spline, const EGNode<Dim>& n0, const EGNode<Dim>& n1);

  //Constructor for an open B-Spline. Generates nodes at the trange end points
  EGEdge(const EGApproximate<Dim>& spline, const ParamRange& trange);

  //Constructor for an open B-Spline. This assuems that trange matches the end nodes
  EGEdge(const EGApproximate<Dim>& spline, const ParamRange& trange, const EGNode<Dim>& n0, const EGNode<Dim>& n1);

#if 0
  EGEdge& operator=( const EGEdge& edge );
  void reference( const EGEdge& ref );
#endif

  std::vector< EGNode<Dim_> > getNodes() const;
  int nNode() const { return mtype_ == TWONODE ? 2 : 1; }

  Real getArcLength() const;
  CartCoord operator()( const Real t ) const;
  bool isOneNode() const { return mtype_ == ONENODE; }
  bool isTwoNode() const { return mtype_ == TWONODE; }
  bool isDegenerate() const { return mtype_ == DEGENERATE; }
  ParamRange getParamRange() const { return range_; }
  Real midRange() const { return (range_[0] + range_[1])/2.; }

  JType J( const Real t ) const;
  JinvType Jinv( const Real t ) const;
  Real CartIncrement2Param( const Real t, const CartCoord& dX ) const;

  EGSense< EGEdge > operator,(const int sense) { return EGSense< EGEdge >(*this, sense); }

protected:
  ParamRange range_;
  int mtype_;
};

}
}

#endif //EG_EDGE_H
