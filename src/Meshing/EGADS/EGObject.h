// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EG_OBJECT_H
#define EG_OBJECT_H

// A base class for all EGADS objects

#include "tools/SANSException.h"

#include "Meshing/EGADS/EGContext.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include <vector>

namespace SANS
{
namespace EGADS
{

//=============================================================================
struct EGADSAttributeException : public SANSException
{
  explicit EGADSAttributeException(const std::string name,
                                   int expectType, int expectLen);

  explicit EGADSAttributeException(int index,
                                   int expectType, int expectLen);

  explicit EGADSAttributeException(const std::string name,
                                   int atype, int len,
                                   const int *pints,
                                   const double *preals,
                                   const char *string,
                                   int expectType, int expectLen);

  explicit EGADSAttributeException(int index,
                                   const char *cname,
                                   int atype, int len,
                                   const int *pints,
                                   const double *preals,
                                   const char *string,
                                   int expectType, int expectLen);

  void found(int atype, int len,
             const int *pints,
             const double *preals,
             const char *string);

  std::string attrstr(const int atype);

  virtual ~EGADSAttributeException() throw() {}
};


//=============================================================================
class EGObject
{
public:
  EGObject( const EGObject& copy );
  EGObject( const EGObject&& copy );
  explicit EGObject( const EGContext& context ) : context_(context), obj_(NULL), reference_(false) {}
  explicit EGObject( const ego obj ) : context_(obj), obj_(obj), reference_(true)
  {
    SANS_ASSERT(obj_ != NULL);
  }

  ~EGObject();

  EGObject& operator=( const EGObject& copy );
  EGObject& operator=( const EGObject&& copy );

#if 0
  void reference( const EGObject& ref );
#endif

  EGObject clone() const { ego clone; EG_STATUS( EG_copyObject(obj_, NULL, &clone) ); return EGObject(clone); }
  EGObject clone( const DLA::VectorS<3,Real>& translate) const;
  EGObject clone( double theta, DLA::VectorS<3,Real> axis ) const;
  EGObject clone( const double theta ) const;

  void Delete();
  int objectClass() const;

  const EGContext& getContext() const { return context_; }

  operator ego()       { return obj_; }
  operator ego() const { return obj_; }

  operator ego*() const { return const_cast<ego*>(&obj_); }

  //Methods to add attributions
  void addAttribute(const std::string& name, const int attr);
  void addAttribute(const std::string& name, const double attr);
  void addAttribute(const std::string& name, const std::vector<int>& attr);
  void addAttribute(const std::string& name, const std::initializer_list<int>& attr);
  void addAttribute(const std::string& name, const int* attr, const int len);
  void addAttribute(const std::string& name, const std::vector<double>& attr);
  void addAttribute(const std::string& name, const std::initializer_list<double>& attr);
  void addAttribute(const std::string& name, const double* attr, const int len);
  void addAttribute(const std::string& name, const std::string& attr);

  //Deletes an attribute
  void delAttribute(const std::string& name);

  //Returns the number of attributes
  int numAttributes() const;

  //Checks if an attribute exists
  bool hasAttribute(const std::string& name) const;

  //Returns the size of an attribute
  int getAttributeSize(const std::string& name) const;
  int getAttributeSize(const int index) const;

  //Extracts the type for an attribute ATTRINT, ATTRREAL, ATTRSTRING, ATTRCSYS, ATTRPTR
  int getAttributeType(const std::string& name) const;
  int getAttributeType(const int index) const;

  //Extract attributes. An exception is thrown if the type is wrong
  void getAttribute(const std::string& name, int& attr) const;
  void getAttribute(const std::string& name, double& attr) const;
  void getAttribute(const std::string& name, std::vector<int>& attr) const;
  void getAttribute(const std::string& name, std::vector<double>& attr) const;
  void getAttribute(const std::string& name, std::string& attr) const;

  void getAttribute(const int index, std::string& name, int& attr) const;
  void getAttribute(const int index, std::string& name, double& attr) const;
  void getAttribute(const int index, std::string& name, std::vector<int>& attr) const;
  void getAttribute(const int index, std::string& name, std::vector<double>& attr) const;
  void getAttribute(const int index, std::string& name, std::string& attr) const;

  //Duplicates the attributes from src
  void dupAttributes(const EGObject& src);

  // prints all attributes in the object
  void printAttributes() const;

protected:

  void getAttribute(const std::string& name,
                    int& atype, int& len,
                    const int *&pints,
                    const double *&preals,
                    const char *&string,
                    int expectType, int expectLen) const;

  void getAttribute(const int index,
                    const char *&cname,
                    int& atype, int& len,
                    const int *&pints,
                    const double *&preals,
                    const char *&string,
                    int expectType, int expectLen) const;

  const EGContext context_;
  ego obj_;
  bool reference_;
};

}
}

#endif //EG_OBJECT_H
