// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EG_GEOMETRY_BASE_H
#define EG_GEOMETRY_BASE_H

// A base class for all geometry objects

#include "EGContext.h"
#include "EGObject.h"

namespace SANS
{
namespace EGADS
{

class EGGeometryBase : public EGObject
{
public:
  explicit EGGeometryBase( const EGContext& context ) : EGObject(context) {}
  // cppcheck-suppress noExplicitConstructor
  EGGeometryBase( const EGObject& topo ) : EGObject(topo) {}
  EGGeometryBase( const EGGeometryBase& copy ) : EGObject(copy) {}
  EGGeometryBase& operator=( const EGGeometryBase& copy ) { EGObject::operator=(copy); return *this; }

  ~EGGeometryBase();
};

}
}

#endif //EG_GEOMETRY_BASE_H
