// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EGADS_NACA4_H
#define EGADS_NACA4_H

// NACA4 generates a NACA 4-series airfoil with continuous definition of the parameters

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "Meshing/EGADS/EGApproximate.h"

#include <vector>

namespace SANS
{
namespace EGADS
{

// Default to NACA0012 airfoil
template<int Dim>
EGApproximate<Dim> NACA4(EGContext& context,
                         Real t = 0.12,   //Thickness
                         Real p = 0.40,   //Maximum camber location
                         Real m = 0.00 ); //Camber

}
}

#endif //EGADS_NACA4_H
