// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "NACA4.h"

#include <cmath> // cos, sin

namespace SANS
{
namespace EGADS
{

template<int Dim>
std::vector< DLA::VectorS<Dim,Real> >
NACA4_pts(const int npnt,       //Number of points
          Real t,  //Thickness
          Real p,  //Maximum camber location
          Real m ) //Camber
{

  Real TWOPI = 2*PI;
  Real s, zeta, yt, yc, theta;

  std::vector< DLA::VectorS<Dim,Real> > pts(npnt, 0);

  for (int ipnt = 0; ipnt < npnt; ipnt++)
  {
    zeta = TWOPI * ipnt / (npnt-1);
    s    = (1 + cos(zeta)) / 2;
    yt   = t*0.594689181/0.12 * (0.298222773 * sqrt(s) + s * (-0.127125232 + s * (-0.357907906 + s * ( 0.291984971 + s * (-0.105174606)))));

    if (s < p)
    {
      yc    =      m / (  p)/(  p) * (s * (2*p -   s));
      theta = atan(m / (  p)/(  p) * (    (2*p - 2*s)));
    }
    else
    {
      yc    =      m / (1-p)/(1-p) * ((1-2*p) + s * (2*p -   s));
      theta = atan(m / (1-p)/(1-p) * (              (2*p - 2*s)));
    }

    if (ipnt < npnt/2)
    {
      pts[ipnt][0] = s  - yt * sin(theta);
      pts[ipnt][1] = yc + yt * cos(theta);
    }
    else if (ipnt == npnt/2)
      pts[ipnt] = 0;
    else
    {
      pts[ipnt][0] = s  + yt * sin(theta);
      pts[ipnt][1] = yc - yt * cos(theta);
    }

  }

  return pts;
}


template<int Dim>
EGApproximate<Dim> NACA4(EGContext& context,
                         Real t, //Thickness
                         Real p, //Maximum camber location
                         Real m  //Camber
                        )
{
  EGApproximate<Dim> NACA_spline(context, EGApproximate<Dim>::Slope, 1e-6, NACA4_pts<Dim>(101, t, p, m));
  return NACA_spline;
}

//Explicit instantiation of the function
template EGApproximate<2> NACA4(EGContext&, Real, Real, Real );
template EGApproximate<3> NACA4(EGContext&, Real, Real, Real );

}
}
