// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField_gmsh.h"

#include <fstream>
#include <cstring>
#include <algorithm>
#include <iomanip> // std::setprecision
#include <map>
#include <vector>
#include <sstream>
#include <array>

#include <boost/version.hpp>

#include "Field/XFieldArea.h"   //Needed to instantiate an area XField
#include "Field/XFieldVolume.h" //Needed to instantiate a volume XField
#include "Field/Partition/XField_Lagrange.h"

#include "Field/tools/for_each_CellGroup.h"
#include "Field/tools/for_each_BoundaryTraceGroup.h"

#include "BasisFunction/LagrangeDOFMap.h"

#include "Topology/ElementTopology.h"

#include "tools/file_extension.h"
#include "tools/file_skiplines.h"
#include "tools/split_string.h"
#include "tools/output_std_vector.h"
#include "tools/linspace.h"

#include "Meshing/gmsh/GMSHtoSANS.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/broadcast.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/vector.hpp>
#endif // SANS_MPI

namespace SANS
{

//---------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
int
XField_gmsh<PhysDim, TopoDim>::countElementsInSection(std::ifstream& msh, const int sectionIdx)
{
  bool foundElementsSection = false;
  int nElements = 0;
  std::string line;
  int accumulator = 0;
  int elementIdx = 0;
  int iElement = 0;
  int elementType = 0;
  int numberOfTags = 0;

  // Save the location
  std::ifstream::pos_type pos = msh.tellg();

  // Be kind: Rewind
  msh.clear();
  msh.seekg( 0 );

  // Seek Elements section
  while (msh.good())
  {
    getline( msh, line );
    if (line == "$Nodes")
    {
      int nNode;
      getline( msh, line );
      std::istringstream ss( line );
      ss >> nNode;
      file_skiplines( msh, nNode );
    }
    else if (line == "$Elements")
    {
      foundElementsSection = true;
      break;
    }
  }
  SANS_ASSERT_MSG( foundElementsSection, "XField_gmsh - Error finding elements section" );
  // We are now at the top of the elements section
  getline( msh, line );
  std::istringstream ss( line );
  ss >> nElements;
  std::cout<<"NELEMENTS***********"<<nElements;
  SANS_ASSERT_MSG( nElements > 0, "XField_gmsh - Error: Found '%i' elements to read (should be >0)", nElements );

  for (int i = 1; i <= nElements; i++)
  {
    getline( msh, line );
    SANS_ASSERT_MSG( line != "$EndElements", "XField_gmsh - Reached end of elements section too early" );
    std::istringstream ss( line );
    ss >> iElement >> elementType >> numberOfTags >> elementIdx;
    SANS_ASSERT_MSG( numberOfTags > 0, "XField_gmsh - No tag associated with element %i", iElement );
    if (elementIdx == sectionIdx)
    {
      accumulator++;
    }
  }
  getline( msh, line );
  SANS_ASSERT_MSG( line == "$EndElements", "XField_gmsh - Error finding elements section end" );

  // Be kind: Rewind-ish
  msh.clear();
  msh.seekg( pos );

  return accumulator;
}

//---------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
inline void
XField_gmsh<PhysDim, TopoDim>::
readElementSection(std::ifstream& msh, const int sectionIdx, const int group,
                   XField_Lagrange<PhysDim>& xfldin, std::set<int>& orders)
{
  bool foundElementsSection = false;
  int nElements = 0;
  std::string line;
  std::vector<std::string> tokenList;
  std::vector<int> nodeList;
  int elementGroup = 0;
  int iElement = 0;
  int elementTypeGMSH = 0;
  int numberOfTags = 0;
  std::vector<int> gmshtoSANS, SANSnodes;
  TopologyTypes shape;
  int order = -1;

  // Save the location
  int pos = msh.tellg();

  // Be kind: Rewind
  msh.clear();
  msh.seekg( 0 );

  // Seek Elements section
  while (msh.good())
  {
    getline( msh, line );
    if (line == "$Nodes")
    {
      int nNode;
      getline( msh, line );
      std::istringstream ss( line );
      ss >> nNode;
      file_skiplines( msh, nNode );
    }
    else if (line == "$Elements")
    {
      foundElementsSection = true;
      break;
    }
  }
  SANS_ASSERT_MSG( foundElementsSection, "XField_gmsh - Error finding elements section" );
  // We are now at the top of the elements section
  getline( msh, line );
  std::istringstream ss( line );
  ss >> nElements;

  for (int i = 1; i <= nElements; i++)
  {
    getline( msh, line );
    SANS_ASSERT_MSG( line != "$EndElements", "XField_gmsh - Reached end of elements section too early" );
    tokenList = split_string( line, " " );
    SANS_ASSERT_MSG( tokenList.size() >= 4, "XField_gmsh - tokenList.size() = %d", tokenList.size());
    iElement = std::stoi( tokenList[0] );
    elementTypeGMSH = std::stoi( tokenList[1] );
    numberOfTags = std::stoi( tokenList[2] );
    elementGroup = std::stoi( tokenList[3] );
    SANS_ASSERT_MSG( numberOfTags > 0, "XField_gmsh - No tag associated with element %d", iElement );
    if (elementGroup == sectionIdx)
    {
      // Remove the front of the token list, leaving the nodes list
      tokenList.erase( tokenList.begin(), tokenList.begin() + (3 + numberOfTags) );
      for (size_t i = 0; i < tokenList.size(); i++)
      {
        nodeList.push_back( std::stoi( tokenList[i] ) );
      }

      // Pass the info to another function to do the actual reading
      getGMSHtoSANSmap(elementTypeGMSH, shape, order, gmshtoSANS);
      orders.insert(order);

      // decrement the indices because they start at 1
      SANSnodes.resize(gmshtoSANS.size());
      for (std::size_t j = 0; j < gmshtoSANS.size(); j++)
        SANSnodes[j] = nodeList[gmshtoSANS[j]] - 1;

      // set the indices for this element
      xfldin.addCell(group, shape, order, SANSnodes);

      // Clean up
      nodeList.clear();
      gmshtoSANS.clear();
      SANSnodes.clear();
    }
  }
  getline( msh, line );
  SANS_ASSERT_MSG( line == "$EndElements", "XField_gmsh - Error finding elements section end" );

  // Be kind: Rewind-ish
  msh.clear();
  msh.seekg( pos );
}

//---------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
inline void
XField_gmsh<PhysDim, TopoDim>::readBoundarySection(std::ifstream& msh, const int sectionIdx, const int group, XField_Lagrange<PhysDim>& xfldin)
{
  bool foundElementsSection = false;
  int nElements = 0;
  std::string line;
  std::vector<std::string> tokenList;
  std::vector<int> nodeList;
  int elementIdx = 0;
  int geomEntityIdx = 0;
  int iElement = 0;
  int elementType = 0;
  int numberOfTags = 0;
  std::vector<int> gmshtoSANS, SANSnodes;
  TopologyTypes shape;
  int order = -1;

  // Save the location
  int pos = msh.tellg();

  // Be kind: Rewind
  msh.clear();
  msh.seekg( 0 );

  // Seek Elements section
  while (msh.good())
  {
    getline( msh, line );
    if (line == "Nodes")
    {
      int nNode;
      getline( msh, line );
      std::istringstream ss( line );
      ss >> nNode;
      file_skiplines( msh, nNode );
    }
    else if (line == "$Elements")
    {
      foundElementsSection = true;
      break;
    }
  }
  SANS_ASSERT_MSG( foundElementsSection, "XField_gmsh - Error finding elements section" );
  // We are now at the top of the elements section
  getline( msh, line );
  std::istringstream ss( line );
  ss >> nElements;

  for (int i = 1; i <= nElements; i++)
  {
    getline( msh, line );
    SANS_ASSERT_MSG( line != "$EndElements", "XField_gmsh - Reached end of elements section too early" );
    tokenList = split_string( line, " " );
    SANS_ASSERT_MSG( tokenList.size() >= 4, "XField_gmsh - tokenList.size() = %d", tokenList.size());
    iElement = std::stoi( tokenList[0] );
    elementType = std::stoi( tokenList[1] );
    numberOfTags = std::stoi( tokenList[2] );
    elementIdx = std::stoi( tokenList[3] );
    geomEntityIdx = std::stoi( tokenList[4] );
    SANS_ASSERT_MSG( numberOfTags > 0, "XField_gmsh - No tag associated with element %i", iElement );
    if (elementIdx == sectionIdx)
    {
      // Remove the front of the token list, leaving the nodes list
      tokenList.erase( tokenList.begin(), tokenList.begin() + (3 + numberOfTags) );
      for (size_t i = 0; i < tokenList.size(); i++)
      {
        nodeList.push_back( std::stoi( tokenList[i] ) );
      }

      // Pass the info to another function to do the actual reading
      getGMSHtoSANSmap(elementType, shape, order, gmshtoSANS);

      // decrement the indices because they start at 1
      SANSnodes.reserve(gmshtoSANS.size());
      for (std::size_t j = 0; j < gmshtoSANS.size(); j++)
      {
        SANSnodes.push_back( nodeList[gmshtoSANS[j]] - 1 );
      }

      // Note, the boundary traces are Q1 ONLY
      // fortunately gmsh ordering puts the Q1 nodes first, so we can just shrink to the correct size
      int nnode = topoNNode(shape);
      SANSnodes.resize(nnode);

      // set the indices for this element
      xfldin.addBoundaryTrace(group, shape, SANSnodes);

      // Now, add the geometric entity tag ID to our map incase this is periodic
      geomEntityToSANSIdx_[geomEntityIdx] = group;

      // Clean up
      nodeList.clear();
      gmshtoSANS.clear();
      SANSnodes.clear();
    }
  }
  getline( msh, line );
  SANS_ASSERT_MSG( line == "$EndElements", "XField_gmsh - Error finding elements section end" );

  // Be kind: Rewind-ish
  msh.clear();
  msh.seekg( pos );
}

//---------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
inline bool
XField_gmsh<PhysDim, TopoDim>::
hasPeriodicSection(std::ifstream& msh)
{
  bool foundPeriodicSection = false;
  std::string line;

  // Save the location
  int pos = msh.tellg();

  // Be kind: Rewind
  msh.clear();
  msh.seekg( 0 );

  // Seek Elements section
  while (msh.good())
  {
    getline( msh, line );
    if (line == "$Periodic")
    {
      foundPeriodicSection = true;
      break;
    }
  }

  // Be kind: Rewind-ish
  msh.clear();
  msh.seekg( pos );

  return foundPeriodicSection;
}

//---------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
inline void
XField_gmsh<PhysDim, TopoDim>::
readPeriodicSection(std::ifstream& msh, XField_Lagrange<PhysDim>& xfldin)
{
  bool foundPeriodicSection = false;
  int nNodes = 0;
  std::string line;
  std::vector<std::string> tokenList;
  int groupL = 0;
  int groupR = 0;
  int nL = 0;
  int nR = 0;

  // Save the location
  int pos = msh.tellg();

  // Be kind: Rewind
  msh.clear();
  msh.seekg( 0 );

  // Seek Elements section
  while (msh.good())
  {
    getline( msh, line );
    if (line == "$Periodic")
    {
      foundPeriodicSection = true;
      break;
    }
  }
  SANS_ASSERT_MSG( foundPeriodicSection, "XField_gmsh - Error finding periodic section" );
  // We are now at the top of the periodic section
  getline( msh, line );
  std::istringstream ss( line );
  ss >> nNodes;
  std::cout << " " << nNodes << " periodic entries to test" << std::endl;

  // Seek Elements section
  while (msh.good())
  {
    getline( msh, line );
    if (line == "$EndPeriodic")
    {
      break;
    }
    tokenList = split_string( line, " " );

    // 3 tokens, and the first one says we're a trace...
    if ((tokenList.size() == 3) && (std::stoi(tokenList[0]) == PhysDim::D-1))
    {
      groupL = geomEntityToSANSIdx_[std::stoi(tokenList[1])];
      groupR = geomEntityToSANSIdx_[std::stoi(tokenList[2])];

      std::cout << "Adding periodic BC connecting traces groups: (" << groupL << ", " << groupR << ")" << std::endl;
      periodicity_.emplace_back(groupL, groupR);
      PeriodicBCNodeMap& period = periodicity_.back();

      // Read the next line
      getline( msh, line );
      tokenList = split_string( line, " " );
      // If the first line is "Affine" ignore it
      if (tokenList.size() > 1)
      {
        getline( msh, line );
      }
      tokenList = split_string( line, " " );
      // How many node pairs do we have?
      nNodes = std::stoi(tokenList[0]);
      //std::cout << " node pairs in connection: " << nNodes << std::endl;
      // Read the node pairs
      for (int i = 1; i <= nNodes; i++)
      {
        getline( msh, line );
        tokenList = split_string( line, " " );

        nL = std::stoi(tokenList[0]) - 1;
        nR = std::stoi(tokenList[1]) - 1;
        //std::cout << "  adding pair: (" << nL << ", " << nR << ")" << std::endl;
        period.mapLtoR[nL] = nR;
      }
    }
  }

  // Be kind: Rewind-ish
  msh.clear();
  msh.seekg( pos );
}

//---------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
XField_gmsh<PhysDim,TopoDim>::XField_gmsh( mpi::communicator& comm, const std::string& filename ):
  XField<PhysDim,TopoDim>(comm)
{
  XField_Lagrange<PhysDim> xfldin(comm);
  XField_gmsh_build(filename, xfldin);
}

//---------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
XField_gmsh<PhysDim,TopoDim>::XField_gmsh( XField<PhysDim,TopoDim>& xfld, XFieldBalance graph, const std::string& filename ):
  XField<PhysDim,TopoDim>(xfld.comm())
{
  XField_Lagrange<PhysDim> xfldin(xfld, graph);
  XField_gmsh_build(filename, xfldin);
}

//---------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
XField_gmsh<PhysDim,TopoDim>::XField_gmsh( mpi::communicator& comm, const std::string& filename, const std::string& partitionfile ):
  XField<PhysDim,TopoDim>(comm)
{
    XField_Lagrange<PhysDim> xfldin(comm, XFieldBalance::CellPartitionRead, partitionfile);
    XField_gmsh_build(filename, xfldin);
}

//---------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
void
XField_gmsh<PhysDim,TopoDim>::XField_gmsh_build(const std::string& filename, XField_Lagrange<PhysDim>& xfldin )
{
  std::shared_ptr<mpi::communicator> comm = xfldin.comm();

  typename XField_Lagrange<PhysDim>::VectorX X;
  std::ifstream msh;
  std::string line;
  std::vector<std::string> token;
  bool foundNodesSection = false;
//  bool foundElementsSection = false;
  bool foundPhysicalNamesSection = false;
  int nNodes = 0;
//  int nElements = 0;
  int nPhyscialSections = 0;
  int iNode;
  int dim, sectionIdx;
  std::string sectionName;
  Real x[3];
  std::map<int, std::pair<std::string, int>> elementsMap, boundariesMap;
  int elementCounter = 0;
  int boundaryFaceCounter = 0;
  bool foundPeriodicSection = false;

  // only rank 0 will read the grid
  if (comm->rank() == 0)
  {
    // Open the file
    msh.open(filename.c_str());
    SANS_ASSERT_MSG( msh.is_open() , "XField_gmsh - Error opening file: %s", filename.c_str() );

    ////////////////////////////////////////////////////////////////////////////////////////
    //  Header
    ////////////////////////////////////////////////////////////////////////////////////////
    std::getline(msh, line);
    while (msh.good() && line != "$MeshFormat") std::getline(msh, line);
    SANS_ASSERT_MSG( line == "$MeshFormat", "XField_gmsh - Error finding header in file: %s", filename.c_str() );
    std::getline(msh, line);
    token = split_string( line, " " );
    SANS_ASSERT_MSG( token[0] == "2.2" , "XField_gmsh<PhysDim,TopoDim>::XField_gmsh - can only read version 2.2" );
    SANS_ASSERT_MSG( token[1] == "0" , "XField_gmsh<PhysDim,TopoDim>::XField_gmsh - can only read ASCII files" );
    SANS_ASSERT_MSG( token[2] == "8" , "XField_gmsh<PhysDim,TopoDim>::XField_gmsh - can only read double width data" );
    std::getline(msh, line);
    SANS_ASSERT_MSG( line == "$EndMeshFormat", "XField_gmsh - Error finding header end in file: %s", filename.c_str() );
    ////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    //  Nodes
    ////////////////////////////////////////////////////////////////////////////////////////
    // Seek Nodes section
    while (msh.good())
    {
      getline(msh, line);
      if (line == "$Elements")
      {
        int nElements;
        getline( msh, line );
        std::istringstream ss( line );
        ss >> nElements;
        file_skiplines( msh, nElements );
      }
      else if (line == "$Nodes")
      {
        foundNodesSection = true;
        break;
      }
    }
    SANS_ASSERT_MSG( foundNodesSection , "XField_gmsh<PhysDim,TopoDim>::XField_gmsh - Error finding nodes section" );
    // We are now at the top of the nodes section
    getline(msh, line);
    std::istringstream ss( line );
    ss >> nNodes;
    SANS_ASSERT_MSG( nNodes > 0 , "XField_gmsh<PhysDim,TopoDim>::XField_gmsh - %i nodes to read (should be >0)", nNodes );
  }

  // start the process for reading points on all processors
  xfldin.sizeDOF( nNodes );

  if (comm->rank() == 0)
  {
    std::cout << "Reading " << nNodes << " nodes from file" << std::endl;
    for (int i = 1; i <= nNodes; i++)
    {
      getline(msh, line);
      SANS_ASSERT_MSG( line != "$EndNodes" , "XField_gmsh<PhysDim,TopoDim>::XField_gmsh - Reached end of nodes section too early" );
      std::istringstream ss(line);
      ss >> iNode >> x[0] >> x[1] >> x[2];
      SANS_ASSERT_MSG( i == iNode , "XField_gmsh<PhysDim,TopoDim>::XField_gmsh - Mismatching node numbers:\n iNode = %i  i = %i", iNode, i );
      for (int k = 0; k < PhysDim::D; k++)
      {
        X[k] = x[k];
      }
      // add the DOF to the parallel grid
      xfldin.addDOF(X);
    }
    getline(msh, line);
    SANS_ASSERT_MSG( line == "$EndNodes" , "XField_gmsh<PhysDim,TopoDim>::XField_gmsh - Error finding nodes section end" );
    // Be kind: Rewind
    msh.clear();
    msh.seekg(0);
    ////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    //  Physical Names
    ////////////////////////////////////////////////////////////////////////////////////////
    // Seek Elements section
    while (msh.good())
    {
      getline(msh, line);
      if (line == "$Nodes")
      {
        int nNodes;
        getline( msh, line );
        std::istringstream ss( line );
        ss >> nNodes;
        file_skiplines( msh, nNodes );
      }
      else if (line == "$Elements")
      {
        int nElements;
        getline( msh, line );
        std::istringstream ss( line );
        ss >> nElements;
        file_skiplines( msh, nElements );
      }
      else if (line == "$PhysicalNames")
      {
        foundPhysicalNamesSection = true;
        break;
      }
    }
    SANS_ASSERT_MSG( foundPhysicalNamesSection , "XField_gmsh - Error finding physical names section" );
    // We are now at the top of the physical name section
    getline(msh, line);
    std::istringstream ss(line);
    ss >> nPhyscialSections;
    SANS_ASSERT_MSG( nPhyscialSections > 0 ,
                     "XField_gmsh - %i physical sections to read (should be >0)", nPhyscialSections );
    std::cout << "Reading " << nPhyscialSections << " physical sections from file" << std::endl;
    for (int i = 1; i <= nPhyscialSections; i++)
    {
      getline( msh, line );
      SANS_ASSERT_MSG( line != "$EndPhysicalNames", "XField_gmsh - Reached end of physical section too early" );
      std::istringstream ss( line );
      ss >> dim >> sectionIdx >> sectionName;
      sectionName.erase( remove( sectionName.begin(), sectionName.end(), '\"' ), sectionName.end() );
      if (dim == PhysDim::D)
      {
        std::cout << "Found elemental section: \"" << sectionName << "\", with from physical id: " << sectionIdx << std::endl;
        // Read (i, idx, name) into a table
        elementsMap[i] = std::make_pair( sectionName, sectionIdx );
        // Accumulate number of elements in this section
        elementCounter += countElementsInSection( msh, sectionIdx );
      }
      else if (dim == PhysDim::D - 1)
      {
        std::cout << "Found boundary section: \"" << sectionName << "\", with from physical id: " << sectionIdx << std::endl;
        // Read (i, sectionIdx, name) into a table
        boundariesMap[i] = std::make_pair( sectionName, sectionIdx );
        // Accumulate number of faces in this section
        boundaryFaceCounter += countElementsInSection( msh, sectionIdx );
      }
    }
    getline( msh, line );
    SANS_ASSERT_MSG( line == "$EndPhysicalNames", "XField_gmsh<PhysDim,TopoDim>::XField_gmsh - Error finding physical section end" );
    // Allocate elements and faces in the arrays here
    // Be kind: Rewind
    msh.clear();
    msh.seekg(0);
    // Report the number of elements and traces, and allocate space in the xfield
    std::cout << "There are " << elementCounter << " elements and " << boundaryFaceCounter << " boundary faces" << std::endl;
    SANS_ASSERT_MSG( elementCounter > 0, "XField_gmsh<PhysDim,TopoDim>::XField_gmsh - found no interior elements" );
    SANS_ASSERT_MSG( boundaryFaceCounter > 0, "XField_gmsh<PhysDim,TopoDim>::XField_gmsh - found no boundary faces" );
  }

  xfldin.sizeCells( elementCounter );
  xfldin.sizeBoundaryTrace( boundaryFaceCounter );

  if (comm->rank() == 0)
  {
    ////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    //  Elements
    ////////////////////////////////////////////////////////////////////////////////////////
    std::cout << "Reading interior cell groups:" << std::endl;
    // Read elements by iterating through tables created
    for (auto it = elementsMap.begin(); it != elementsMap.end(); ++it)
    {
      int group = std::distance(elementsMap.begin(), it);
      sectionName = it->second.first;
      sectionIdx = it->second.second;
      std::cout << " Cell group: " << group << ", region name: \"" << sectionName << "\", from physical id: " << sectionIdx;
      std::set<int> orders;
      readElementSection( msh, sectionIdx, group, xfldin, orders );
      std::cout << ", orders: ";
      for (int order : orders)
        std::cout << order << " ";
      std::cout << std::endl;
    }
    std::cout << "Finished reading interior cell groups" << std::endl;
    ////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    //  Boundaries
    ////////////////////////////////////////////////////////////////////////////////////////
    std::cout << "Reading boundary trace groups:" << std::endl;
    // Read elements by iterating through tables created
    for (auto it = boundariesMap.begin(); it != boundariesMap.end(); ++it)
    {
      int group = std::distance(boundariesMap.begin(), it);
      sectionName = it->second.first;
      sectionIdx = it->second.second;
      std::cout << " Boundary group: " << group << ", region name: \"" << sectionName << "\", from physical id: " << sectionIdx << std::endl;
      readBoundarySection( msh, sectionIdx, group, xfldin );
    }
    std::cout << "Finished reading boundary trace groups" << std::endl;
    ////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    //  BC Boundary Groups
    ////////////////////////////////////////////////////////////////////////////////////////
    std::cout << "Generating BC boundary groups" << std::endl;
    BCBoundaryGroups_.clear();
    for (auto it = boundariesMap.begin(); it != boundariesMap.end(); ++it)
    {
      int group = std::distance(boundariesMap.begin(), it);
      sectionName = it->second.first;
      sectionIdx = it->second.second;
      BCBoundaryGroups_[sectionName] = { group };
      std::cout << " Added bc boundary group named \"" << sectionName << "\" with boundary group number " << group<< std::endl;
    }
    std::cout << "Finished generating BC boundary groups" << std::endl;
    ////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////
    //  Periodic Trace Groups
    ////////////////////////////////////////////////////////////////////////////////////////
    std::cout << "Seeking periodic section" << std::endl;
    foundPeriodicSection = hasPeriodicSection(msh);
    if (foundPeriodicSection)
    {
      std::cout << "Reading periodic trace groups" << std::endl;
      readPeriodicSection(msh, xfldin);
      std::cout << "Finished reading periodic trace groups" << std::endl;
    }
    else
    {
      std::cout << "Periodic section not found" << std::endl;
    }
    ////////////////////////////////////////////////////////////////////////////////////////

    msh.close();
  }
#ifdef SANS_MPI
  boost::mpi::broadcast(*comm, foundPeriodicSection, 0);
#endif
  if (foundPeriodicSection)
  {
    if (comm->rank() == 0) std::cout << "Adding periodic boundaries to trace group" << std::endl;
    // Create the boundary periodicity
    xfldin.addBoundaryPeriodicity(periodicity_);
    if (comm->rank() == 0) std::cout << "Added periodic boundaries to trace group" << std::endl;
  }

  ////////////////////////////////////////////////////////////////////////////////////////
  //  Build up the grid from the maps provided
  ////////////////////////////////////////////////////////////////////////////////////////
  if (comm->rank() == 0) std::cout << "Constructing grid" << std::endl;

  this->buildFrom(xfldin);

  if (comm->rank() == 0) std::cout << "Finished grid build" << std::endl;
  ////////////////////////////////////////////////////////////////////////////////////////

#ifdef SANS_MPI
  boost::mpi::broadcast(*comm, BCBoundaryGroups_, 0);
#endif

}

//---------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
std::map<std::string, std::vector<int>>
XField_gmsh<PhysDim,TopoDim>::getBCBoundaryGroups() const
{
  return BCBoundaryGroups_;
}

// Explicitly instantiate the msh grid reader
template class XField_gmsh<PhysD2, TopoD2>;
template class XField_gmsh<PhysD3, TopoD3>;


} // namespace SANS
