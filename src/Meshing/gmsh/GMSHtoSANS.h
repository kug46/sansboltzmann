// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef GMSH_TO_SANS_H
#define GMSH_TO_SANS_H

#include <vector>

#include "Topology/ElementTopology.h"

namespace SANS
{

// Returns the GMSH ElementType ID given a shape and order
int
getGMSH_ElementType(const TopologyTypes& shape, const int& order);

// Gets the SANS representation given a GMSH element type number
void
getGMSHtoSANSmap(const int elementTypeGMSH, TopologyTypes& shape, int& order, std::vector<int>& nodeMap);

}

#endif //GMSH_TO_SANS_H
