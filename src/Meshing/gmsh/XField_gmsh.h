// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD_GMSH_H
#define XFIELD_GMSH_H

/*\
 *
 *  XField class used to read gmesh msh files
 *
\*/

#include "Field/XField.h"
#include "Field/Partition/XField_Lagrange.h"

#include <string>
#include <set>

namespace SANS
{

template<class PhysDim>
class XField_Lagrange;

// Notes:
//  2D meshes must be in the x-y plane
//  A $PhysicalNames section must be present
//  Each individual cell group must be a D-dimensional section in $PhysicalNames
//  Each boundary trace group must be a (D-1)-dimensional section in $PhysicalNames
//  Each

template<class PhysDim, class TopoDim>
class XField_gmsh : public XField< PhysDim, TopoDim>
{
public:
  XField_gmsh( mpi::communicator& comm, const std::string& filename ); // construct from a file
  XField_gmsh( XField<PhysDim,TopoDim>& xfld, XFieldBalance graph, const std::string& filename );
  XField_gmsh( mpi::communicator& comm, const std::string& filename, const std::string& partitionfile );
  std::map<std::string, std::vector<int>> getBCBoundaryGroups() const;

protected:
  void XField_gmsh_build( const std::string& filename, XField_Lagrange<PhysDim>& xfldin );

  int countElementsInSection(std::ifstream& msh, const int sectionIdx); // Count the number of elements (of any dimension) in a section
  void readElementSection(std::ifstream& msh, const int sectionIdx, const int group, XField_Lagrange<PhysDim>& xfldin, std::set<int>& orders);
  void readBoundarySection(std::ifstream& msh, const int sectionIdx, const int group, XField_Lagrange<PhysDim>& xfldin);
  bool hasPeriodicSection(std::ifstream& msh);
  void readPeriodicSection(std::ifstream& msh, XField_Lagrange<PhysDim>& xfldin);

  std::map<std::string, std::vector<int>> BCBoundaryGroups_; // Stores out BCBoundary groups map for access
  std::vector<PeriodicBCNodeMap> periodicity_; // We will store all our periodic BC information here
  std::map<int, int> geomEntityToSANSIdx_; // This is how we go from the tags in the periodic boundary to SANS groups
};


} // namespace SANS

#endif // XFIELD_GMSH_H
