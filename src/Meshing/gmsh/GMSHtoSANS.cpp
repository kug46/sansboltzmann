// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "Meshing/gmsh/GMSHtoSANS.h"

#include "tools/SANSException.h"
#include "tools/linspace.h"

/*  gmsh numberings

 Line:                   Line3:           Line4:

 0----------1 --> u      0-----2----1     0----2----3----1

 Triangle:               Triangle6:          Triangle9/10:          Triangle12/15:

 v
 ^                                                                   2
 |                                                                   | \
 2                       2                    2                      9   8
 |`\                     |`\                  | \                    |     \
 |  `\                   |  `\                7   6                 10 (14)  7
 |    `\                 5    `4              |     \                |         \
 |      `\               |      `\            8  (9)  5             11 (12) (13) 6
 |        `\             |        `\          |         \            |             \
 0----------1 --> u      0-----3----1         0---3---4---1          0---3---4---5---1


 Quadrangle:            Quadrangle8:            Quadrangle9:

       v
       ^
       |
 3-----------2          3-----6-----2           3-----6-----2
 |     |     |          |           |           |           |
 |     |     |          |           |           |           |
 |     +---- | --> u    7           5           7     8     5
 |           |          |           |           |           |
 |           |          |           |           |           |
 0-----------1          0-----4-----1           0-----4-----1


 Tetrahedron:                          Tetrahedron10:

                    v
                  .
                ,/
               /
            2                                     2
          ,/|`\                                 ,/|`\
        ,/  |  `\                             ,/  |  `\
      ,/    '.   `\                         ,6    '.   `5
    ,/       |     `\                     ,/       8     `\
  ,/         |       `\                 ,/         |       `\
 0-----------'.--------1 --> u         0--------4--'.--------1
  `\.         |      ,/                 `\.         |      ,/
     `\.      |    ,/                      `\.      |    ,9
        `\.   '. ,/                           `7.   '. ,/
           `\. |/                                `\. |/
              `3                                    `3
                 `\.
                    ` w


 Hexahedron:             Hexahedron20:          Hexahedron27:

        v
 3----------2            3----13----2           3----13----2
 |\     ^   |\           |\         |\          |\         |\
 | \    |   | \          | 15       | 14        |15    24  | 14
 |  \   |   |  \         9  \       11 \        9  \ 20    11 \
 |   7------+---6        |   7----19+---6       |   7----19+---6
 |   |  +-- |-- | -> u   |   |      |   |       |22 |  26  | 23|
 0---+---\--1   |        0---+-8----1   |       0---+-8----1   |
  \  |    \  \  |         \  17      \  18       \ 17    25 \  18
   \ |     \  \ |         10 |        12|        10 |  21    12|
    \|      w  \|           \|         \|          \|         \|
     4----------5            4----16----5           4----16----5


 MHexahedronN

   3----13----2
   |\         |\
   |15    24  | 14
   9  \ 20    11 \
   |   7----19+---6
   |22 |  26  | 23|
   0---+-8----1   |
    \ 17    25 \  18
    10 |  21    12|
      \|         \|
       4----16----5

    N = 0RDER - 1
    8 := 7        + 1 --> 7 +    N
    9 := 7 +    N + 1 --> 7 +  2*N
   10 := 7 +  2*N + 1 --> 7 +  3*N
   11 := 7 +  3*N + 1 --> 7 +  4*N
   12 := 7 +  4*N + 1 --> 7 +  5*N
   13 := 7 +  5*N + 1 --> 7 +  6*N
   14 := 7 +  6*N + 1 --> 7 +  7*N
   15 := 7 +  7*N + 1 --> 7 +  8*N
   16 := 7 +  8*N + 1 --> 7 +  9*N
   17 := 7 +  9*N + 1 --> 7 + 10*N
   18 := 7 + 10*N + 1 --> 7 + 11*N
   19 := 7 + 11*N + 1 --> 7 + 12*N

   20 := 7 + 12*N +         1 --> 7 + 12*N + 1*N^2
   21 := 7 + 12*N +   N^2 + 1 --> 7 + 12*N + 2*N^2
   22 := 7 + 12*N + 2*N^2 + 1 --> 7 + 12*N + 3*N^2
   23 := 7 + 12*N + 3*N^2 + 1 --> 7 + 12*N + 4*N^2
   24 := 7 + 12*N + 4*N^2 + 1 --> 7 + 12*N + 5*N^2
   25 := 7 + 12*N + 5*N^2 + 1 --> 7 + 12*N + 6*N^2
   26 := 7 + 12*N + 6*N^2 + 1 --> 7 + 12*N + 6*N^2 + N^3

 */



// Defines taken from gmsh/Common/MeshDefines.h in the gmsh source
#define MSH_LIN_2    1
#define MSH_TRI_3    2
#define MSH_QUA_4    3
#define MSH_TET_4    4
#define MSH_HEX_8    5
#define MSH_PRI_6    6
#define MSH_PYR_5    7
#define MSH_LIN_3    8
#define MSH_TRI_6    9
#define MSH_QUA_9    10
#define MSH_TET_10   11
#define MSH_HEX_27   12
#define MSH_PRI_18   13
#define MSH_PYR_14   14
#define MSH_PNT      15
#define MSH_QUA_8    16
#define MSH_HEX_20   17
#define MSH_PRI_15   18
#define MSH_PYR_13   19
#define MSH_TRI_9    20
#define MSH_TRI_10   21
#define MSH_TRI_12   22
#define MSH_TRI_15   23
#define MSH_TRI_15I  24
#define MSH_TRI_21   25
#define MSH_LIN_4    26
#define MSH_LIN_5    27
#define MSH_LIN_6    28
#define MSH_TET_20   29
#define MSH_TET_35   30
#define MSH_TET_56   31
#define MSH_TET_22   32
#define MSH_TET_28   33
#define MSH_POLYG_   34
#define MSH_POLYH_   35
#define MSH_QUA_16   36
#define MSH_QUA_25   37
#define MSH_QUA_36   38
#define MSH_QUA_12   39
#define MSH_QUA_16I  40
#define MSH_QUA_20   41
#define MSH_TRI_28   42
#define MSH_TRI_36   43
#define MSH_TRI_45   44
#define MSH_TRI_55   45
#define MSH_TRI_66   46
#define MSH_QUA_49   47
#define MSH_QUA_64   48
#define MSH_QUA_81   49
#define MSH_QUA_100  50
#define MSH_QUA_121  51
#define MSH_TRI_18   52
#define MSH_TRI_21I  53
#define MSH_TRI_24   54
#define MSH_TRI_27   55
#define MSH_TRI_30   56
#define MSH_QUA_24   57
#define MSH_QUA_28   58
#define MSH_QUA_32   59
#define MSH_QUA_36I  60
#define MSH_QUA_40   61

#define MSH_HEX_64   92
#define MSH_HEX_125  93

namespace SANS
{

int
getGMSH_ElementType(const TopologyTypes& shape, const int& order)
{
  switch (shape)
  {
  case eNode:
    return MSH_PNT;
  case eLine:
    switch (order)
    {
    case 1: return MSH_LIN_2;
    case 2: return MSH_LIN_3;
    case 3: return MSH_LIN_4;
    case 4: return MSH_LIN_5;
    case 5: return MSH_LIN_6;
    default: SANS_DEVELOPER_EXCEPTION("Unknown Line order = %d", order);
    }
    break;
  case eTriangle:
    switch (order)
    {
    case 1: return MSH_TRI_3;
    case 2: return MSH_TRI_6;
    default: SANS_DEVELOPER_EXCEPTION("Unknown Triangle order = %d", order);
    }
    break;
  case eQuad:
    switch (order)
    {
    case 1: return MSH_QUA_4;
    case 2: return MSH_QUA_9;
    case 3: return MSH_QUA_16;
    case 4: return MSH_QUA_25;
    default: SANS_DEVELOPER_EXCEPTION("Unknown Quad order = %d", order);
    }
    break;
  case eTet:
    switch (order)
    {
    case 1: return MSH_TET_4;
    case 2: return MSH_TET_10;
    default: SANS_DEVELOPER_EXCEPTION("Unknown Tet order = %d", order);
    }
    break;
  case eHex:
    switch (order)
    {
    case 1: return MSH_HEX_8;
    case 2: return MSH_HEX_27;
    case 3: return MSH_HEX_64;
    default: SANS_DEVELOPER_EXCEPTION("Unknown Hex order = %d", order);
    }
    break;
  case ePentatope:
    SANS_DEVELOPER_EXCEPTION("gmsh does not support pentatopes");
    break;
  }

  SANS_DEVELOPER_EXCEPTION("Unknown shape = %d", shape);
  return 0;
}

void
getGMSHtoSANSmap(const int elementTypeGMSH, TopologyTypes& shape, int& order, std::vector<int>& nodeMap)
{
  // set defaults
  shape = eNode;
  order = -1;
  nodeMap.clear();

  switch (elementTypeGMSH)
  {
  case MSH_PNT: // Node
    shape = eNode;
    order = 1;
    nodeMap = { 0 };
    return;
    break;
  case MSH_LIN_2: // Line: Q1 2-Nodes
    shape = eLine;
    order = 1;
    nodeMap = { 0, 1 };
    return;
    break;
  case MSH_LIN_3: // Line: Q2 3-Nodes
    shape = eLine;
    order = 2;
    nodeMap = { 0, 1, 2 };
    return;
    break;
  case MSH_LIN_4: // Line Q3 4-Nodes
    shape = eLine;
    order = 3;
    nodeMap = { 0, 1, 2, 3 };
    return;
    break;
  case MSH_LIN_5: // Line Q4 5-Nodes
    shape = eLine;
    order = 4;
    nodeMap = { 0, 1, 2, 3, 4 };
    return;
    break;

  case MSH_TRI_3: // Triangle: Q1 3-Nodes
    shape = eTriangle;
    order = 1;
    nodeMap = { 0, 1, 2 };
    return;
    break;
  case MSH_TRI_6: // Triangle: Q2  6-Nodes
    shape = eTriangle;
    order = 2;
    nodeMap = { 0, 1, 2, 4, 5, 3 };
    return;
    break;

  case MSH_QUA_4:  // Quad: Q1 4-Nodes
    shape = eQuad;
    order = 1;
    nodeMap =  { 0, 1, 2, 3 };
    return;
    break;
  case MSH_QUA_8: // Quad: Q2 8-Nodes
    SANS_DEVELOPER_EXCEPTION( "SANS does not support Q2 8-Node Quads, and likely never will...");
    return;
    break;
  case MSH_QUA_9:  // Quad: Q2 9-Nodes
    shape = eQuad;
    order = 2;
    nodeMap =  { 0, 1, 2, 3, 4, 5, 6, 7, 8 };
    return;
    break;
  case MSH_QUA_16: // Quad: Q3 16-Nodes
    shape = eQuad;
    order = 3;
    nodeMap = linspace(0, 15);

#if 0 // FIX for incorrect gmsh files for HOW5_CI1 grids
    // edge 3
    nodeMap[10] = 11;
    nodeMap[11] = 10;

    // face
    nodeMap[14] = 15;
    nodeMap[15] = 14;
#endif
    return;
    break;
  case MSH_QUA_25: // Quad: Q4 25-Nodes
    shape = eQuad;
    order = 4;
    nodeMap =  linspace(0, 24);

#if 0 // FIX for incorrect gmsh files for HOW5_CI1 grids
    // edge 3
    nodeMap[13] = 15;
    nodeMap[14] = 14;
    nodeMap[15] = 13;

    // face
    nodeMap[17] = 18;
    nodeMap[18] = 24;
    nodeMap[19] = 22;
    nodeMap[20] = 17;
    nodeMap[21] = 21;
    nodeMap[22] = 23;
    nodeMap[23] = 19;
    nodeMap[24] = 20;
#endif
    return;
    break;

  case MSH_TET_4:  // Tet: Q1 4-Nodes
    shape = eTet;
    order = 1;
    nodeMap =  { 0, 1, 2, 3 };
    return;
    break;
  case MSH_TET_10: // Tet: Q2 10-Nodes
    shape = eTet;
    order = 2;
        // SANS: 0  1  2  3
    nodeMap = { 0, 1, 2, 3,         // Nodes
        //      4  5  6  7  8  9
                8, 9, 5, 6, 7, 4 }; // Edges
    return;
    break;

  case MSH_HEX_8: // Hex: Q1 8-Nodes
    shape = eHex;
    order = 1;
    nodeMap = { 0, 1, 2, 3, 4, 5, 6, 7 };
    return;
    break;
  case MSH_HEX_20: // Hex: Q2 20-Nodes
    SANS_DEVELOPER_EXCEPTION( "SANS does not support Q2 20-Node Hex, and likely never will...");
    return;
    break;
  case MSH_HEX_27: // Hex: Q2 27-Nodes
    shape = eHex;
    order = 2;
       // SANS: 0  1  2  3  4  5  6  7
    nodeMap = { 0, 1, 2, 3, 4, 5, 6, 7,                        // Nodes
       //       8   9  10  11  12  13  14  15  16  17  18  19
                9, 13, 11,  8, 12, 16, 10, 14, 18, 17, 15, 19, // Edges
       //      20  21  22  23  24  25
               20, 21, 23, 24, 22, 25,                         // Faces
       //      26
               26                                              // Cell
              };
    return;
    break;
  case MSH_HEX_64: // Hex: Q3 64-Nodes
    shape = eHex;
    order = 3;
#define N 2 // order - 1
#define N2 N*N
       // SANS: 0  1  2  3  4  5  6  7
    nodeMap = { 0, 1, 2, 3, 4, 5, 6, 7,  // Nodes
    //  gmsh Q1
    /* 9*/      7 +  1*N + 1, 7 +  2*N,  // Edge  0
    /*13*/                               7 +  6*N, 7 +  5*N + 1,  // Edge  1 reversed
    /*11*/                               7 +  4*N, 7 +  3*N + 1,  // Edge  2 reversed
    /* 8*/                               7 +  1*N, 7 +  0*N + 1,  // Edge  3 reversed
    /*12*/      7 +  4*N + 1, 7 +  5*N,  // Edge  4
    /*16*/                               7 +  9*N, 7 +  8*N + 1,  // Edge  5 reversed
    /*10*/                               7 +  3*N, 7 +  2*N + 1,  // Edge  6 reversed
    /*14*/      7 +  6*N + 1, 7 +  7*N,  // Edge  7
    /*18*/                               7 + 11*N, 7 + 10*N + 1,  // Edge  8 reversed
    /*17*/      7 +  9*N + 1, 7 + 10*N,  // Edge  9
    /*15*/      7 +  7*N + 1, 7 +  8*N,  // Edge 10
    /*19*/                               7 + 12*N, 7 + 11*N + 1,  // Edge 11 reversed

    /*20*/      7 + 12*N + 0*N2 + 1, 7 + 12*N + 0*N2 + 2, 7 + 12*N + 0*N2 + 3, 7 + 12*N + 1*N2,  // Face 0
    /*21*/      7 + 12*N + 1*N2 + 1, 7 + 12*N + 1*N2 + 2, 7 + 12*N + 1*N2 + 3, 7 + 12*N + 2*N2,  // Face 1
    /*23*/      7 + 12*N + 3*N2 + 1, 7 + 12*N + 3*N2 + 2, 7 + 12*N + 3*N2 + 3, 7 + 12*N + 4*N2,  // Face 2
    /*24*/      7 + 12*N + 4*N2 + 2, 7 + 12*N + 4*N2 + 3, 7 + 12*N + 5*N2, 7 + 12*N + 4*N2 + 1,  // Face 3  rotated
    /*22*/      7 + 12*N + 2*N2 + 1, 7 + 12*N + 2*N2 + 2, 7 + 12*N + 2*N2 + 3, 7 + 12*N + 3*N2,  // Face 4
    /*25*/      7 + 12*N + 5*N2 + 1, 7 + 12*N + 5*N2 + 2, 7 + 12*N + 5*N2 + 3, 7 + 12*N + 6*N2,  // Face 5

    /*26*/      7 + 12*N + 6*N2 + 1, 7 + 12*N + 6*N2 + 2,
                7 + 12*N + 6*N2 + 3, 7 + 12*N + 6*N2 + 4,

                7 + 12*N + 6*N2 + 5, 7 + 12*N + 6*N2 + 6,
                7 + 12*N + 6*N2 + 7, 7 + 12*N + 6*N2 + 8   // Cell
              };
#undef N
#undef N2
    return;
    break;

  case MSH_PRI_6: // Prism: Q1
    SANS_DEVELOPER_EXCEPTION( "Prisms not supported");
    return;
    break;
  case MSH_PYR_5: // Pyramid: Q1
    SANS_DEVELOPER_EXCEPTION( "Pyramids not supported");
    return;
    break;

  case MSH_PRI_18: // Prism: Q2
    SANS_DEVELOPER_EXCEPTION( "Prisms Q2 not supported");
    return;
    break;
  case MSH_PYR_14: // Pyramid: Q2
    SANS_DEVELOPER_EXCEPTION( "Pyramids Q2 not supported");
    return;
    break;
  case MSH_PRI_15: // Prism: Q2
    SANS_DEVELOPER_EXCEPTION( "Prisms Q2 not supported");
    return;
    break;
  case MSH_PYR_13: // Pyramid: Q2
    SANS_DEVELOPER_EXCEPTION( "Pyramids Q2 not supported");
    return;
    break;

  default:
    // Error
    SANS_DEVELOPER_EXCEPTION( "Unrecognized / unsupported element type, elementType %d",elementTypeGMSH );
    return;
  }

  SANS_DEVELOPER_EXCEPTION( "Entered \'impossible\' state, elementType %d", elementTypeGMSH );
}


} // namespace SANS
