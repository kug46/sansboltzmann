// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "Meshing/gmsh/WriteMesh_gmsh.h"

#include <boost/version.hpp>

#include <map>
#include <vector>
#include <ostream>
#include <iomanip> //std::setprecision
#include <fstream>
#include <array>

#include "tools/output_std_vector.h"

#include "tools/SANSnumerics.h"     // Real
#include "Topology/Dimension.h"
#include "Topology/ElementTopology.h"

#include "Field/XFieldArea.h"
#include "Field/XFieldVolume.h"

#include "Field/tools/for_each_CellGroup.h"
#include "Field/tools/for_each_BoundaryTraceGroup.h"

#include "Meshing/gmsh/GMSHtoSANS.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#include "MPI/continuousElementMap.h"

#ifdef SANS_MPI
#include "tools/plus_std_vector.h"
#include "MPI/serialize_DenseLinAlg_MatrixS.h"

#include <boost/mpi/collectives/all_reduce.hpp>
#include <boost/mpi/collectives/gather.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#endif

namespace SANS
{

//===========================================================================//
// Routines for writing gmsh file
//===========================================================================//

template <class PhysDim, class TopoDim>
class writeBoundaryTraceGroupsGmsh : public GroupFunctorBoundaryTraceType< writeBoundaryTraceGroupsGmsh<PhysDim,TopoDim> >
{
public:
  writeBoundaryTraceGroupsGmsh( const XField<PhysDim,TopoDim>& xfld, std::fstream& writer,
                                const int nBTraceGroups, int& ElemID, int& elementGroup )
    : xfld_(xfld), writer_(writer), nBTraceGroups_(nBTraceGroups), ElemID_(ElemID), elementGroup_(elementGroup) {}

  std::size_t nBoundaryTraceGroups() const { return nBTraceGroups_; }
  std::size_t boundaryTraceGroup(const int n) const { return n; }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology >
  void
  apply(const typename XField<PhysDim,TopoDim>::template FieldTraceGroupType<Topology>& xfldBTrace,
        const int boundaryTraceGroupGlobal)
  {
    // number of elements on the local processor
    int nElem = xfldBTrace.nElem();
    int Qorder = xfldBTrace.order();
    int nBasis = xfldBTrace.nBasis();
    int numberOfTags = 2;

    int elementTypeGMSH = getGMSH_ElementType(Topology::Topology, Qorder);

#ifdef SANS_MPI
    int comm_rank = xfld_.comm()->rank();

    const std::vector<int>& boundaryTraceIDs = xfld_.boundaryTraceIDs(boundaryTraceGroupGlobal);
    std::map<int,int> globalElemMap;
    continuousElementMap( *xfld_.comm(), boundaryTraceIDs, xfldBTrace, nElem, globalElemMap);
#endif

    std::vector<int> GMSHtoSANS;
    TopologyTypes shape;
    int order;
    getGMSHtoSANSmap(elementTypeGMSH, shape, order, GMSHtoSANS);
    std::vector<int> SANSnodes(GMSHtoSANS.size());
    std::vector<int> GMSHnodes(GMSHtoSANS.size());

#ifdef SANS_MPI
    int comm_size = xfld_.comm()->size();
    int nElemLocal = xfldBTrace.nElem();

    // maximum element chunk size that rank 0 will write at any given time
    int Elemchunk = nElem / comm_size + nElem % comm_size;

    // send one chunk of elements at a time to rank 0
    for (int rank = 0; rank < comm_size; rank++)
    {
      int elemLow  =  rank   *Elemchunk;
      int elemHigh = (rank+1)*Elemchunk;

      std::map<int,std::vector<int>> buffer;

      for (int elem = 0; elem < nElemLocal; elem++)
      {
        int elemID = globalElemMap[boundaryTraceIDs[elem]];

        if (elemID >= elemLow && elemID < elemHigh &&
            xfldBTrace.associativity(elem).rank() == comm_rank)
        {
          // gmsh format only wants linear component of boundary elements
          xfldBTrace.associativity(elem).getGlobalMapping(SANSnodes.data(), SANSnodes.size());

          // transform back to native DOF indexing
          for (std::size_t i = 0; i < SANSnodes.size(); i++)
            SANSnodes[i] = xfld_.local2nativeDOFmap(SANSnodes[i]);

          // transform to GMSH order and increment the indices because they start at 1
          for (std::size_t j = 0; j < GMSHtoSANS.size(); j++)
            GMSHnodes[GMSHtoSANS[j]] = SANSnodes[j] + 1;

          buffer[boundaryTraceIDs[elem]] = GMSHnodes;
        }
      }

      if (comm_rank == 0)
      {
        std::vector<std::map<int,std::vector<int>>> bufferOnRank;
#ifndef __clang_analyzer__
        boost::mpi::gather(*xfld_.comm(), buffer, bufferOnRank, 0 );
#endif

        // collapse down the buffer from all other ranks
        for (std::size_t i = 1; i < bufferOnRank.size(); i++)
          buffer.insert(bufferOnRank[i].begin(), bufferOnRank[i].end());

        //Write the node data
        for ( const auto& GMSHnodesPair : buffer)
        {
          // Write element info. The elmenetGroup is the Tag
          writer_ << ElemID_ << " " << elementTypeGMSH << " " << numberOfTags << " " << elementGroup_ << " " << elementGroup_ << " ";

          for (int n = 0; n < nBasis-1; n++)
            writer_ << GMSHnodesPair.second[n] << " ";
          writer_ << GMSHnodesPair.second[nBasis-1] << std::endl;

          ElemID_++; // Increment the ID
        }
      }
#ifndef __clang_analyzer__
      else // send the buffer to rank 0
        boost::mpi::gather(*xfld_.comm(), buffer, 0 );
#endif
    } // for rank
#else
    // loop over traces within group
    for (int elem = 0; elem < nElem; elem++)
    {
      // Write element info. The elmenetGroup is the Tag
      writer_ << ElemID_ << " " << elementTypeGMSH << " " << numberOfTags << " " << elementGroup_ << " " << elementGroup_ << " ";

      xfldBTrace.associativity(elem).getGlobalMapping(SANSnodes.data(), SANSnodes.size());

      // transform and increment the indices because they start at 1
      for (std::size_t j = 0; j < GMSHtoSANS.size(); j++)
        GMSHnodes[GMSHtoSANS[j]] = SANSnodes[j] + 1;

      for (int n = 0; n < nBasis-1; n++)
        writer_ << GMSHnodes[n] << " ";
      writer_ << GMSHnodes[nBasis-1] << std::endl;

      ElemID_++; // Increment the ID
    }
#endif

    // Increment the group
    elementGroup_++;
  }

protected:
  const XField<PhysDim,TopoDim>& xfld_;
  std::fstream& writer_;
  const int nBTraceGroups_;
  int& ElemID_;
  int& elementGroup_;
};

template <class PhysDim, class TopoDim>
class writeCellGroupsGmsh : public GroupFunctorCellType< writeCellGroupsGmsh<PhysDim,TopoDim> >
{
public:
  writeCellGroupsGmsh( const XField<PhysDim,TopoDim>& xfld, std::fstream& writer,
                       const int nCellGroups, int& ElemID, int& elementGroup )
    : xfld_(xfld), writer_(writer), nCellGroups_(nCellGroups), ElemID_(ElemID), elementGroup_(elementGroup) {}

  std::size_t nCellGroups() const          { return nCellGroups_; }
  std::size_t cellGroup(const int n) const { return n;            }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology>
  void
  apply(const typename XField<PhysDim,typename Topology::TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
        const int cellGroupGlobal)
  {
    // number of elements on the local processor
    int nElem = xfldCellGroup.nElem();
    int Qorder = xfldCellGroup.order();
    int nBasis = xfldCellGroup.nBasis();
    int numberOfTags = 2;

    int elementTypeGMSH = getGMSH_ElementType(Topology::Topology, Qorder); // Topology::Topology WHAT?

#ifdef SANS_MPI
    int comm_rank = xfld_.comm()->rank();

    // construct a continuous element ID map for the elements in the current group
    const std::vector<int>& cellIDs = xfld_.cellIDs(cellGroupGlobal);
    std::map<int,int> globalElemMap;
    continuousElementMap( *xfld_.comm(), cellIDs, xfldCellGroup, nElem, globalElemMap);
#endif

    std::vector<int> GMSHtoSANS;
    TopologyTypes shape;
    int order;
    getGMSHtoSANSmap(elementTypeGMSH, shape, order, GMSHtoSANS);
    std::vector<int> SANSnodes(GMSHtoSANS.size());
    std::vector<int> GMSHnodes(GMSHtoSANS.size());

#ifdef SANS_MPI
    int comm_size = xfld_.comm()->size();
    int nElemLocal = xfldCellGroup.nElem();

    // maximum element chunk size that rank 0 will write at any given time
    int Elemchunk = nElem / comm_size + nElem % comm_size;

    // send one chunk of elements at a time to rank 0
    for (int rank = 0; rank < comm_size; rank++)
    {
      int elemLow  =  rank   *Elemchunk;
      int elemHigh = (rank+1)*Elemchunk;

      std::map<int,std::vector<int>> buffer;

      for (int elem = 0; elem < nElemLocal; elem++)
      {
        int elemID = globalElemMap[cellIDs[elem]];

        if (elemID >= elemLow && elemID < elemHigh &&
            xfldCellGroup.associativity(elem).rank() == comm_rank)
        {
          xfldCellGroup.associativity(elem).getGlobalMapping(SANSnodes.data(), SANSnodes.size());

          // transform back to native DOF indexing
          for (std::size_t i = 0; i < SANSnodes.size(); i++)
            SANSnodes[i] = xfld_.local2nativeDOFmap(SANSnodes[i]);

          // transform to GMSH order and increment the indices because they start at 1
          for (std::size_t j = 0; j < GMSHtoSANS.size(); j++)
            GMSHnodes[GMSHtoSANS[j]] = SANSnodes[j] + 1;

          buffer[cellIDs[elem]] = GMSHnodes;
        }
      }

      if (comm_rank == 0)
      {
        std::vector<std::map<int,std::vector<int>>> bufferOnRank;
#ifndef __clang_analyzer__
        boost::mpi::gather(*xfld_.comm(), buffer, bufferOnRank, 0 );
#endif

        // collapse down the buffer from all other ranks
        for (std::size_t i = 1; i < bufferOnRank.size(); i++)
          buffer.insert(bufferOnRank[i].begin(), bufferOnRank[i].end());

        //Write the node data
        for ( const auto& GMSHnodesPair : buffer)
        {
          // Write element info. The elmenetGroup is the Tag
          writer_ << ElemID_ << " " << elementTypeGMSH << " " << numberOfTags << " " << elementGroup_ << " " << elementGroup_ << " ";

          for (int n = 0; n < nBasis-1; n++)
            writer_ << GMSHnodesPair.second[n] << " ";
          writer_ << GMSHnodesPair.second[nBasis-1] << std::endl;

          ElemID_++; // Increment the ID
        }
      }
#ifndef __clang_analyzer__
      else // send the buffer to rank 0
        boost::mpi::gather(*xfld_.comm(), buffer, 0 );
#endif
    } // for rank

#else
    // loop over cells within group
    for (int elem = 0; elem < nElem; elem++)
    {
      // Write element info. The elmenetGroup is the Tag
      writer_ << ElemID_ << " " << elementTypeGMSH << " " << numberOfTags << " " << elementGroup_ << " " << elementGroup_ << " ";

      xfldCellGroup.associativity(elem).getGlobalMapping(SANSnodes.data(), SANSnodes.size());

      // transform and increment the indices because they start at 1
      for (std::size_t j = 0; j < GMSHtoSANS.size(); j++)
        GMSHnodes[GMSHtoSANS[j]] = SANSnodes[j] + 1;

      for (int n = 0; n < nBasis-1; n++)
        writer_ << GMSHnodes[n] << " ";
      writer_ << GMSHnodes[nBasis-1] << std::endl;

      ElemID_++; // Increment the ID
    }
#endif

    // Increment the group
    elementGroup_++;
  }

protected:
  const XField<PhysDim,TopoDim>& xfld_;
  std::fstream& writer_;
  const int nCellGroups_;
  int& ElemID_;
  int& elementGroup_;
};


//---------------------------------------------------------------------------//
template<class TopoDim>
struct CountPossessedElements;

template<>
struct CountPossessedElements<TopoD2>
{
  template<class PhysDim>
  static int count(const XField<PhysDim,TopoD2>& xfld )
  {
    int nElemTotal = 0;

    // Count cell elements
    for (int igroup = 0; igroup < xfld.nCellGroups(); igroup++)
           if ( xfld.getCellGroupBaseGlobal(igroup).topoTypeID() == typeid(Triangle) )
        nElemTotal += nElementsPossessed(*xfld.comm(), xfld.template getCellGroupGlobal<Triangle>(igroup));
      else if ( xfld.getCellGroupBaseGlobal(igroup).topoTypeID() == typeid(Quad) )
        nElemTotal += nElementsPossessed(*xfld.comm(), xfld.template getCellGroupGlobal<Quad>(igroup));
      else
        SANS_DEVELOPER_EXCEPTION("Unknown topology");

    // Count boundary elements
    for (int igroup = 0; igroup < xfld.nBoundaryTraceGroups(); igroup++)
      if ( xfld.getBoundaryTraceGroupBaseGlobal(igroup).topoTypeID() == typeid(Line) )
        nElemTotal += nElementsPossessed(*xfld.comm(), xfld.template getBoundaryTraceGroupGlobal<Line>(igroup));
      else
        SANS_DEVELOPER_EXCEPTION("Unknown topology");

    return nElemTotal;
  }
};

template<>
struct CountPossessedElements<TopoD3>
{
  template<class PhysDim>
  static int count(const XField<PhysDim,TopoD3>& xfld )
  {
    int nElemTotal = 0;

    // Count cell elements
    for (int igroup = 0; igroup < xfld.nCellGroups(); igroup++)
           if ( xfld.getCellGroupBaseGlobal(igroup).topoTypeID() == typeid(Tet) )
        nElemTotal += nElementsPossessed(*xfld.comm(), xfld.template getCellGroupGlobal<Tet>(igroup));
      else if ( xfld.getCellGroupBaseGlobal(igroup).topoTypeID() == typeid(Hex) )
        nElemTotal += nElementsPossessed(*xfld.comm(), xfld.template getCellGroupGlobal<Hex>(igroup));
      else
        SANS_DEVELOPER_EXCEPTION("Unknown topology");

    // Count boundary elements
    for (int igroup = 0; igroup < xfld.nBoundaryTraceGroups(); igroup++)
           if ( xfld.getBoundaryTraceGroupBaseGlobal(igroup).topoTypeID() == typeid(Triangle) )
        nElemTotal += nElementsPossessed(*xfld.comm(), xfld.template getBoundaryTraceGroupGlobal<Triangle>(igroup));
      else if ( xfld.getBoundaryTraceGroupBaseGlobal(igroup).topoTypeID() == typeid(Quad) )
        nElemTotal += nElementsPossessed(*xfld.comm(), xfld.template getBoundaryTraceGroupGlobal<Quad>(igroup));
      else
        SANS_DEVELOPER_EXCEPTION("Unknown topology");

    return nElemTotal;
  }
};


//===========================================================================//
template<class PhysDim, class TopoDim>
void WriteMesh_gmsh(const XField<PhysDim,TopoDim>& xfld, const std::string& filename,
                    const std::map<std::string, std::vector<int>>& BCBoundaryGroups )
{
  int comm_rank = xfld.comm()->rank();
  std::fstream writer;

  if (comm_rank == 0)
  {
    writer.open( filename, std::fstream::out );
    writer << std::scientific << std::setprecision(16);

    if (!writer.good())
      SANS_RUNTIME_EXCEPTION( "Error writing to file: %s", filename.c_str() );
  }

  const int nCellGroup = xfld.nCellGroups();
  const int nBTraceGroup = xfld.nBoundaryTraceGroups();

  //Only works for Lagrange basis functions or P1 Hierarchical (which is identical to Lagrange P1)
  for (int group = 0; group < nCellGroup; group++)
    SANS_ASSERT(  xfld.getCellGroupBase(group).basisCategory() == BasisFunctionCategory_Lagrange ||
                 (xfld.getCellGroupBase(group).basisCategory() == BasisFunctionCategory_Hierarchical &&
                  xfld.getCellGroupBase(group).order() == 1) );

  //--------------------------------------------------//

  int nDOFnative = xfld.nDOFnative();

  if (comm_rank == 0)
  {
    //Write the header
    writer << "$MeshFormat" << std::endl;
    writer << "2.2 0 " << sizeof(double) << std::endl;
    writer << "$EndMeshFormat" << std::endl;

    if (BCBoundaryGroups.size() > 0)
    {
      writer << "$PhysicalNames" << std::endl;
      writer << nBTraceGroup+nCellGroup << std::endl;

      // Write boundary zones
      for (int igroup = 0; igroup < nBTraceGroup; igroup++)
      {
        for (const std::pair<std::string, std::vector<int>>& BC : BCBoundaryGroups)
          if (std::find(BC.second.begin(), BC.second.end(), igroup) != BC.second.end())
          {
            writer << TopoDim::D-1 << " " << nCellGroup+igroup+1 << " \"" << BC.first << "\"" << std::endl;
            break;
          }
      }

      // Write cell zones
      for (int igroup = 0; igroup < nCellGroup; igroup++)
        writer << TopoDim::D << " " << igroup+1 << " \"Zone " << igroup+1 << "\""  << std::endl;

      writer << "$EndPhysicalNames" << std::endl;
    }

    // start the node section
    writer << "$Nodes" << std::endl;
    writer << nDOFnative << std::endl;
  }

#ifdef SANS_MPI
  typedef DLA::VectorS<PhysDim::D,Real> VectorX;

  int comm_size = xfld.comm()->size();

  // maximum DOF chunk size that rank 0 will write at any given time
  int DOFchunk = nDOFnative / comm_size + nDOFnative % comm_size;
  int iNode = 1;

  for (int rank = 0; rank < comm_size; rank++)
  {
    int DOFlow = rank*DOFchunk;
    int DOFhigh = (rank+1)*DOFchunk;

    std::map<int,VectorX> buffer;

    for (int i = 0; i < xfld.nDOF(); i++)
    {
      int iDOF = xfld.local2nativeDOFmap(i);
      if (iDOF >= DOFlow && iDOF < DOFhigh)
        buffer[iDOF] = xfld.DOF(i);
    }

    if (comm_rank == 0)
    {
      std::vector<std::map<int,VectorX>> bufferOnRank;
#ifndef __clang_analyzer__
      boost::mpi::gather(*xfld.comm(), buffer, bufferOnRank, 0 );
#endif

      // collapse down the buffer from all ranks (this remove any possible duplicates)
      for (std::size_t i = 0; i < bufferOnRank.size(); i++)
        buffer.insert(bufferOnRank[i].begin(), bufferOnRank[i].end());

      //Write the node data
      for ( const auto& DOFpair : buffer)
      {
        writer << iNode++ << " ";
        for (int d = 0; d < PhysDim::D-1; d++)
          writer << DOFpair.second[d] << " ";
        writer << DOFpair.second[PhysDim::D-1];
        if (PhysDim::D == 2) writer << " 0.0";
        writer << std::endl;
      }

    }
#ifndef __clang_analyzer__
    else // send the buffer to rank 0
      boost::mpi::gather(*xfld.comm(), buffer, 0 );
#endif
  } // for rank

#else
  //Write the node data
  for (int k = 0; k < nDOFnative; k++)
  {
    writer << k+1 << " ";
    for (int d = 0; d < PhysDim::D-1; d++)
      writer << xfld.DOF(k)[d] << " ";
    writer << xfld.DOF(k)[PhysDim::D-1];
    if (PhysDim::D == 2) writer << " 0.0";
    writer << std::endl;
  }
#endif

  if (comm_rank == 0)
  {
    writer << "$EndNodes" << std::endl;
    writer << "$Elements" << std::endl;
  }

  int nElemTotal = CountPossessedElements<TopoDim>::count(xfld);

  if (comm_rank == 0)
    writer << nElemTotal << std::endl;

  // Global element ID
  int ElemID = 1;
  int elementGroup = nCellGroup+1;  //Not sure the group numbering really has to be like this

  // loop over boundary groups and write out the connectivity
  for_each_BoundaryTraceGroup<TopoDim>::apply(
      writeBoundaryTraceGroupsGmsh<PhysDim, TopoDim>(xfld, writer, nBTraceGroup, ElemID, elementGroup), xfld );

  elementGroup = 1;

  // loop over cell groups and write out the connectivity
  for_each_CellGroup<TopoDim>::apply( writeCellGroupsGmsh<PhysDim, TopoDim>(xfld, writer, nCellGroup, ElemID, elementGroup), xfld );

  if (comm_rank == 0)
  {
    writer << "$EndElements" << std::endl;
    writer << std::flush;
    writer.close();
  }
}

template void WriteMesh_gmsh<PhysD2, TopoD2>(const XField<PhysD2,TopoD2>&, const std::string&, const std::map<std::string, std::vector<int>>& );
template void WriteMesh_gmsh<PhysD3, TopoD3>(const XField<PhysD3,TopoD3>&, const std::string&, const std::map<std::string, std::vector<int>>& );

}
