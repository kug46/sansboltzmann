// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef WRITEMESH_GMSH_H_
#define WRITEMESH_GMSH_H_

#include "Field/XField.h"

namespace SANS
{

template <class PhysDim, class TopoDim>
void WriteMesh_gmsh(const XField<PhysDim,TopoDim>& xfld, const std::string& filename,
                    const std::map<std::string, std::vector<int>>& BCBoundaryGroups );

}

#endif /* WRITEMESH_GMSH_H_ */
