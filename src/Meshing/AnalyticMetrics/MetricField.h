// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SANS_MESHING_ADAPTIVE_ANALYTIC_METRICS_METRIC_FIELD_H
#define SANS_MESHING_ADAPTIVE_ANALYTIC_METRICS_METRIC_FIELD_H

#include <vector>

#include "BasisFunction/BasisFunctionCategory.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"

#include "Field/FieldTypes.h"

namespace SANS
{

template< class PhysDim,class TopoDim >
class MetricField : public Field_CG_Cell< PhysDim,TopoDim,DLA::MatrixSymS<PhysDim::D,Real> >
{
public:
  static const int Dim = PhysDim::D;

  template<typename Function>
  MetricField( const XField<PhysDim,TopoDim>& xfld , const std::vector<int>& cellGroups , Function& f ) :
    Field_CG_Cell< PhysDim,TopoDim,DLA::MatrixSymS<Dim,Real> >(xfld, 1, BasisFunctionCategory_Lagrange, cellGroups)
  {
    for (int i = 0; i < xfld.nDOF(); i++)
      this->DOF_[i] = f( xfld.DOF(i) );
  }

  virtual ~MetricField() {}
};

} // SANS

#endif //METRIC_CORNER_SINGULARITY_H
