// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef METRIC_ANALYTICAL_H
#define METRIC_ANALYTICAL_H

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

//This is a base class for analytical metric fields

namespace SANS
{

namespace Metric
{

template<int Dim, class T = Real>
class MetricAnalytical
{
public:
  virtual ~MetricAnalytical() {};

  //virtual DLA::MatrixSymS<Dim, T> operator()( const DLA::VectorS<Dim, T>& X ) const = 0;
};

}
}

#endif //METRIC_ANALYTICAL_H
