// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef METRIC_CORNER_SINGULARITY_H
#define METRIC_CORNER_SINGULARITY_H

#include "MetricAnalytical.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Diag.h"

#include "Surreal/SurrealS.h"

namespace SANS
{

namespace Metric
{

template< int Dim_ >
class CornerSingularity;

template< class Topology >
struct CornerSingularity_TopoIntegral
{
  static const int Dim = Topology::Dim;
  static Real getReferenceLength(const Topology& topo, const CornerSingularity<Dim>& M);
};

template< int Dim_ >
class CornerSingularity : public MetricAnalytical< Dim_ >
{
public:
  static const int Dim = Dim_;
  CornerSingularity( const Real alpha, const int p, const Real C );
  CornerSingularity( const CornerSingularity<Dim>& M ) : alpha(M.alpha), k(M.k), C(M.C) {}
  CornerSingularity& operator=( const CornerSingularity<Dim>& ) = delete;

  virtual ~CornerSingularity() {}

  template<class T>
  DLA::MatrixSymS<Dim, T> operator()( const DLA::VectorS<Dim, T>& X ) const
  {
    T r = sqrt(dot(X,X));
    DLA::MatrixSymS<Dim, T> I = DLA::Identity();
    T h = (r == 0 ? 1.0e16 : C*pow(1.0*r, -2.0*k)); // catch for the origin
    // std::cout << "r = " << r << ", k = " << k << ", h = " << h << std::endl;

    return h*I;
  }

  template<class T>
  T Lp( const DLA::VectorS<Dim, T>& X, const DLA::VectorS<Dim, T>& dX, int p ) const
  {
    T r = sqrt(dot(X,X));
    T h = (r == 0 ? 1.0e16 : sqrt(C)*pow(r, -k) ); // catch for the origin
    Real theta = atan2(X[1],X[0]); //PI/4.;
    DLA::MatrixS<2,2,Real> R = { { cos(theta), sin(theta)},
                                 {-sin(theta), cos(theta)} };

    DLA::VectorS<Dim,T> dXi = R*dX;
    dXi *= h;

    T Lp = 0;
    for (int d = 0; d < Dim; d++)
      Lp += pow(dXi[d],p);

    return pow(Lp,1./Real(p));
  }

  template<class Topology>
  Real getMetricArea(const Topology& topo) const { return CornerSingularity_TopoIntegral<Topology>::getReferenceLength(topo, *this); }

protected:
  Real alpha;
  Real k;
  Real C;
};

}
}

#endif //METRIC_CORNER_SINGULARITY_H
