// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "CornerSingularity.h"

#include "Quadrature/QuadratureLine.h"
#include "Quadrature/QuadratureArea.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "Meshing/EGADS/EGEdge.h"
#include "Meshing/EGADS/EGFace.h"

#include "Surreal/SurrealS.h"

class Quad; //TODO: This really should be in a file...

namespace SANS
{

namespace Metric
{

template< int Dim_ >
CornerSingularity< Dim_ >::CornerSingularity( const Real alpha, const int p, const Real C ) // cppcheck-suppress selfInitialization
    : alpha(alpha), k(1 - (alpha + 1.)/(p + 2.)), C(C)
{
  /*
  //TODO:: This assumes a line, rather than a general edge
  //Corner Singularity metric is:
  // M = C*r^(-2k)
  // Total number of elements are
  // nElem = \Int_r0^r1{sqrt(C^2*r^(-4k))}
  // Hence,
  // C = (nElem/\Int_r0^r1{r^-2*k})
  */
}

template class CornerSingularity< 2 >;
template class CornerSingularity< 3 >;


template< >
Real
CornerSingularity_TopoIntegral< EGADS::EGEdge<2> >::getReferenceLength(const EGADS::EGEdge<2>& topo, const CornerSingularity<2>& M)
{
  typename EGADS::EGEdge<Dim>::ParamRange range;
  typename EGADS::EGEdge<Dim>::ParamRange range_edge = topo.getParamRange();
  typename EGADS::EGEdge<Dim>::CartCoord X;
  typename EGADS::EGEdge<Dim>::JType J;

  //TODO: The integral should be subdivided into smaller segments.
  //      Maybe Bob knows a good way to do it
  QuadratureLine QL(9);
  //Real len = topo.getArcLength();

  int Intervals = 500*ceil(range_edge[1]-range_edge[0]);
  Real Int = 0;
  range[0] = range_edge[0];
  range[1] = (range_edge[1]-range_edge[0])/Real(Intervals);
  for ( int nInt = 0; nInt < Intervals; nInt++ )
  {
    for ( int n = 0; n < QL.nQuadrature(); n++ )
    {
      Real t = (range[1]-range[0])*QL.coordinate(n) + range[0];

      X = topo( t );
      J = topo.J( t );
      //J /= sqrt(J[0]*J[0] + J[1]*J[1]);

  //      Real r = sqrt(dot(X,X));
  //
  //      Int += sqrt(C*pow(r, -2*k))*QL.weight(n);
      Int += sqrt( Transpose(J)*M(X)*J )*QL.weight(n);
    }
    range[0] += (range_edge[1]-range_edge[0])/Real(Intervals);
    range[1] += (range_edge[1]-range_edge[0])/Real(Intervals);
  }

  Int /= 500;

  return Int;
}


template< >
Real
CornerSingularity_TopoIntegral< EGADS::EGFace<2> >::getReferenceLength(const EGADS::EGFace<2>& topo, const CornerSingularity<2>& M)
{
  typename EGADS::EGFace<Dim>::ParamRange range_u = topo.getURange();
  typename EGADS::EGFace<Dim>::ParamRange range_v = topo.getURange();
  typename EGADS::EGFace<Dim>::CartCoord X;

  DLA::VectorS<2, Real> Qx;
  QuadratureArea<Quad> QA(9);

  Real area = topo.getArea();

  Real Int = 0;
  for ( int n = 0; n < QA.nQuadrature(); n++ )
  {
    Qx = QA.coordinates(n);
    Real u = (range_u[1]-range_u[0])*Qx[0] + range_u[0];
    Real v = (range_v[1]-range_v[0])*Qx[1] + range_v[0];

    X = topo( {u,v} );

    Int += sqrt(DLA::Det(M(X)))*QA.weight(n);
  }

  Int *= area;

  return Int;
}


template struct CornerSingularity_TopoIntegral< EGADS::EGEdge<2> >;
template struct CornerSingularity_TopoIntegral< EGADS::EGFace<2> >;


}
}
