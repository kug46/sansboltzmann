// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField.h"

#include "Quadrature/QuadratureLine.h"
#include "Quadrature/QuadratureArea.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "Meshing/EGADS/EGEdge.h"
#include "Meshing/EGADS/EGFace.h"

#include "Surreal/SurrealS.h"

class Quad; //TODO: This really should be in a file...

namespace SANS
{

namespace Metric
{


template< >
Real
XField_TopoIntegral< EGADS::EGEdge<2> >::getReferenceLength(const EGADS::EGEdge<2>& topo, const XField<2>& M)
{
  typename EGADS::EGEdge<Dim>::ParamRange range;
  typename EGADS::EGEdge<Dim>::ParamRange range_edge = topo.getParamRange();
  typename EGADS::EGEdge<Dim>::CartCoord X;
  typename EGADS::EGEdge<Dim>::JType J;

  //TODO: The integral should be subdivided into smaller segments.
  //      Maybe Bob knows a good way to do it
  QuadratureLine QL(9);
  //Real len = topo.getArcLength();

  int Intervals = 1000*ceil(range_edge[1]-range_edge[0]);
  Real Int = 0;
  range[0] = range_edge[0];
  range[1] = (range_edge[1]-range_edge[0])/Real(Intervals);
  for ( int nInt = 0; nInt < Intervals; nInt++ )
  {
    for ( int n = 0; n < QL.nQuadrature(); n++ )
    {
      Real t = (range[1]-range[0])*QL.coordinate(n) + range[0];

      X = topo( t );
      J = topo.J( t );
      //J /= sqrt(J[0]*J[0] + J[1]*J[1]);

  //      Real r = sqrt(dot(X,X));
  //
  //      Int += sqrt(C*pow(r, -2*k))*QL.weight(n);
      Int += sqrt( Transpose(J)*M(X)*J )*QL.weight(n);
    }
    range[0] += (range_edge[1]-range_edge[0])/Real(Intervals);
    range[1] += (range_edge[1]-range_edge[0])/Real(Intervals);
  }

  Int /= 1000;

  return Int;
}


template< >
Real
XField_TopoIntegral< EGADS::EGFace<2> >::getReferenceLength(const EGADS::EGFace<2>& topo, const XField<2>& M)
{
  typename EGADS::EGFace<Dim>::ParamRange range_u, range_v;
  typename EGADS::EGFace<Dim>::ParamRange range_face_u = topo.getURange();
  typename EGADS::EGFace<Dim>::ParamRange range_face_v = topo.getURange();
  typename EGADS::EGFace<Dim>::CartCoord X;
  typename EGADS::EGFace<Dim>::JType J;

  DLA::VectorS<2, Real> Qx;
  QuadratureArea<Quad> QA(9);

  //Real area = topo.getArea();

  Real Int = 0;
  int Intervals_u = ceil(range_face_u[1]-range_face_u[0])*500;
  int Intervals_v = ceil(range_face_v[1]-range_face_v[0])*500;
  range_v[0] = range_face_v[0];
  range_v[1] = (range_face_v[1]-range_face_v[0])/Real(Intervals_v);

  for ( int nInt_v = 0; nInt_v < Intervals_v; nInt_v++ )
  {
    range_u[0] = range_face_u[0];
    range_u[1] = (range_face_u[1]-range_face_u[0])/Real(Intervals_u);
    for ( int nInt_u = 0; nInt_u < Intervals_u; nInt_u++ )
    {
      for ( int n = 0; n < QA.nQuadrature(); n++ )
      {
        Qx = QA.coordinates(n);
        Real u = (range_u[1]-range_u[0])*Qx[0] + range_u[0];
        Real v = (range_v[1]-range_v[0])*Qx[1] + range_v[0];

        X = topo( {u,v} );
        J = topo.J( {u,v} );

        Int += sqrt(DLA::Det(M(X)))*DLA::Det(J)*QA.weight(n);
      }
      range_u[0] += (range_face_u[1]-range_face_u[0])/Real(Intervals_u);
      range_u[1] += (range_face_u[1]-range_face_u[0])/Real(Intervals_u);
    }
    range_v[0] += (range_face_v[1]-range_face_v[0])/Real(Intervals_v);
    range_v[1] += (range_face_v[1]-range_face_v[0])/Real(Intervals_v);
  }

  //Int *= area;
  Int /= 500*500;

  return Int;
}

} //Metric
} //SANS
