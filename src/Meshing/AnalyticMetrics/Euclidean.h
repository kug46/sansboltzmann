// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef METRIC_EUCLIDEAN_H
#define METRIC_EUCLIDEAN_H

#include "MetricAnalytical.h"
#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/tools/Identity.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Diag.h"

#include "Surreal/SurrealS.h"

namespace SANS
{

namespace Metric
{

template< int Dim_ >
class Euclidean : public MetricAnalytical<Dim_>
{
public:
  static const int Dim = Dim_;
  explicit Euclidean(const int nElem) : nElem(nElem) {}

  virtual ~Euclidean() {}

  template<class T>
  DLA::MatrixSymS<Dim, T> operator()( const DLA::VectorS<Dim, T>& X ) const
  {
    DLA::MatrixSymS<Dim, T> I = DLA::Identity();
    I *= nElem*nElem;
    return I;
  }

  template<class T>
  T Lp( const DLA::VectorS<Dim, T>& X, const DLA::VectorS<Dim, T>& dX, int p ) const
  {
    Real h = Real(nElem);
    //Real pi = -4.*atan(1.);
    Real theta = 0; //atan2(X[1],X[0]); //pi/4.; //
    DLA::MatrixS<2,2,Real> R = { {cos(theta), sin(theta)}, {-sin(theta), cos(theta)} };

    DLA::VectorS<Dim,T> dXi = h*R*dX;

    T Lp = 0;
    for (int d = 0; d < Dim; d++)
      Lp += pow(dXi[d],p);

    return pow(Lp,1./Real(p));
  }

  template<class Topology>
  Real getMetricArea(const Topology& topo) const { return nElem; }

protected:
  int nElem;
};

}
}

#endif //METRIC_EUCLIDEAN_H
