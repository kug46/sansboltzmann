
INCLUDE( ${CMAKE_SOURCE_DIR}/CMakeInclude/ForceOutOfSource.cmake ) #This must be the first thing included

SET( ANALYTICMETRIC_SRC
     BoundaryLayer.cpp
     CornerSingularity.cpp
     XField.cpp
  )

ADD_LIBRARY( AnalyticMetricLib STATIC ${ANALYTICMETRIC_SRC} )

#Create the vera targest for this library
ADD_VERA_CHECKS_RECURSE( AnalyticMetricLib *.h *.cpp )
ADD_HEADER_COMPILE_CHECK_RECURSE( AnalyticMetricLib *.h )
ADD_CPPCHECK( AnalyticMetricLib ${ANALYTICMETRIC_SRC} )
