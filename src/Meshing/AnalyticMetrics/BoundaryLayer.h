// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef METRIC_BOUNDARY_LAYER_H
#define METRIC_BOUNDARY_LAYER_H

#include <cmath> // sin, cos

#include "MetricAnalytical.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Diag.h"

namespace SANS
{

namespace Metric
{

template< int Dim_>
class BoundaryLayer;

template< class Topology >
struct BoundaryLayer_TopoIntegral
{
  static const int Dim = Topology::Dim;
  static Real getReferenceLength(const Topology& topo, const BoundaryLayer<Dim>& M);
};

template< int Dim_ >
class BoundaryLayer : public MetricAnalytical< Dim_ >
{
public:
  static const int Dim = Dim_;
  BoundaryLayer( const Real epsilon, const Real beta, const int p, const Real C );
  BoundaryLayer( const BoundaryLayer& M ) : k1(M.k1), AR0(M.AR0), kAR(M.kAR), C(M.C) {}
  BoundaryLayer& operator=( const BoundaryLayer& ) = delete;

  virtual ~BoundaryLayer() {}

  template<class T>
  DLA::MatrixSymS<Dim, T> operator()( const DLA::VectorS<Dim, T>& X ) const
  {
    DLA::MatrixSymS<Dim, T> M(0);
    M(1,1) = C*pow(exp(k1*X[1]),-2.);
    M(0,0) = M(1,1)*pow(AR0*exp(kAR*X[1]),-2.);
    return M;
  }

  template<class T>
  T Lp( const DLA::VectorS<Dim, T>& X, const DLA::VectorS<Dim, T>& dX, int p ) const
  {
    Real theta = 0; //atan(-1)/4.;
    DLA::MatrixS<2,2,T> R = { {cos(theta), sin(theta)}, {-sin(theta), cos(theta)} };

    DLA::MatrixSymS<Dim, T> S(0);
    S(1,1) = sqrt(C)*pow(exp(k1*X[1]),-1.);
    S(0,0) = S(1,1)*pow(AR0*exp(kAR*X[1]),-1.);

    DLA::VectorS<Dim,T> dXi = S*R*dX;

    T Lp = 0;
    for (int d = 0; d < Dim; d++)
      Lp += pow(dXi[d],p);

    return pow(Lp,1./Real(p));
  }

  template<class Topology>
  Real getMetricArea(const Topology& topo) const { return BoundaryLayer_TopoIntegral<Topology>::getReferenceLength(topo, *this); }

protected:
  Real k1;
  Real AR0;
  Real kAR;
  Real C;
};

}
}

#endif //METRIC_BOUNDARY_LAYER_H
