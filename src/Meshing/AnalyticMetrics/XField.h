// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef METRIC_XFIELD_H
#define METRIC_XFIELD_H

#include "MetricAnalytical.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Diag.h"

#include "Surreal/SurrealS.h"

namespace SANS
{

namespace Metric
{

template< int Dim_ >
class XField;

template< class Topology >
struct XField_TopoIntegral
{
  static const int Dim = Topology::Dim;
  static Real getReferenceLength(const Topology& topo, const XField<Dim>& M);
};

template< int Dim_ >
class XField : public MetricAnalytical< Dim_ >
{
public:
  static const int Dim = Dim_;
  XField( const Real alpha, const Real C ) : alpha(alpha), C(C) {}
  XField( const XField<Dim>& M ) : alpha(M.alpha), C(M.C) {}
  XField& operator=( const XField<Dim>& M ) = delete;

  virtual ~XField() {}

  template<class T>
  DLA::MatrixSymS<Dim, T> operator()( const DLA::VectorS<Dim, T>& X ) const
  {
    DLA::VectorS<2,T> h;
    h[0] = C/(2.*pow(1. - alpha*exp(-fabs(X[0] + X[1])/sqrt(2)), 2) );
    h[1] = C/(2.*pow(1. - alpha*exp(-fabs(X[0] - X[1])/sqrt(2)), 2) );
    //DLA::MatrixSymS<Dim, T> M = {h[0], 0, h[1]};
    //return M;
    DLA::MatrixS<2,2,T> R = { { 1, 1},
                              {-1, 1} };
    return Transpose(R)*DLA::diag(h)*R;
  }

  template<class T>
  T Lp( const DLA::VectorS<Dim, T>& X, const DLA::VectorS<Dim, T>& dX, int p ) const
  {
    T r = sqrt(dot(X,X));
    DLA::VectorS<Dim, T> h( C*pow(r, -1) );
    Real theta = atan(-1)/4.;
    DLA::MatrixS<2,2,T> R = { {cos(theta), sin(theta)}, {-sin(theta), cos(theta)} };

    DLA::VectorS<Dim,T> dXi = DLA::diag(h)*R*dX;

    T Lp = 0;
    for (int d = 0; d < Dim; d++)
      Lp += pow(dXi[d],p);

    return pow(Lp,1./Real(p));
  }

  template<class Topology>
  Real getMetricArea(const Topology& topo) const { return XField_TopoIntegral<Topology>::getReferenceLength(topo, *this); }

protected:
  Real alpha;
  Real C;
};

}
}

#endif //METRIC_CORNER_SINGULARITY_H
