// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <vector>
#include <iostream>

#include "tools/output_std_vector.h"

#include "XField_ugrid.h"

#include "Topology/ElementTopology.h"

#include "Field/XFieldVolume.h"
#include "Field/Partition/XField_Lagrange.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

template<class T>
inline
void byteswap(T& x)
{
  T y;
  char *xp = (char *)&(x);
  char *yp = (char *)&(y);

  for (std::size_t i = 0; i < sizeof(T); i++)
    *(yp + sizeof(T)-1-i) = *(xp + i);

  x = y;
}

//---------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
XField_ugrid<PhysDim, TopoDim>::
XField_ugrid( mpi::communicator comm, const std::string& filename) : XField<PhysDim, TopoDim>(comm)
{
  XField_Lagrange<PhysDim> xfldin(comm);
  read(xfldin, filename);
}

//---------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
XField_ugrid<PhysDim,TopoDim>::XField_ugrid( XField<PhysDim,TopoDim>& xfld, XFieldBalance graph, const std::string& filename ):
  XField<PhysDim,TopoDim>(xfld.comm())
{
  XField_Lagrange<PhysDim> xfldin(xfld, graph);
  read(xfldin, filename);
}

//---------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
void
XField_ugrid<PhysDim, TopoDim>::
read( XField_Lagrange<PhysDim>& xfldin, const std::string& filename )
{
  std::size_t len = filename.length();

  if (filename.substr(len-10) == ".lb8.ugrid" )
    read(xfldin, std::ifstream::in | std::ifstream::binary, false, filename);
  else if (filename.substr(len-9) == ".b8.ugrid" )
    read(xfldin, std::ifstream::in | std::ifstream::binary, true, filename);
  else if (filename.substr(len-6) == ".ugrid" )
    read(xfldin, std::ifstream::in, false, filename);
  else
    SANS_DEVELOPER_EXCEPTION("Unknown file format");
}

//---------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
void
XField_ugrid<PhysDim, TopoDim>::
read( XField_Lagrange<PhysDim>& xfldin, const std::ios_base::openmode mode, const bool swap, const std::string& filename )
{
  std::shared_ptr<mpi::communicator> comm = xfldin.comm();

  std::ifstream ugrid, faceID;
  int nnode=0, ntri=0, nquad=0, ntet=0, npyr=0, npri=0, nhex=0, tmp=0;
  //int fortran_record_size=0;

  // only rank 0 will read the grid
  if (comm->rank() == 0)
  {
    ugrid.open(filename.c_str(), mode);
    SANS_ASSERT_MSG( ugrid.is_open() , "Error opening file: %s", filename.c_str() );
    faceID.open(filename.c_str(), mode);
    SANS_ASSERT_MSG( faceID.is_open() , "Error opening file: %s", filename.c_str() );

    //if (mode & std::ifstream::binary) ugrid.read((char*)&fortran_record_size, sizeof(int));
    //std::cout << "fortran_record_size = " << fortran_record_size << std::endl;

    if (mode & std::ifstream::binary)
    {
      ugrid.read((char*)&nnode, sizeof(int));
      ugrid.read((char*)&ntri, sizeof(int));
      ugrid.read((char*)&nquad, sizeof(int));
      ugrid.read((char*)&ntet, sizeof(int));
      ugrid.read((char*)&npyr, sizeof(int));
      ugrid.read((char*)&npri, sizeof(int));
      ugrid.read((char*)&nhex, sizeof(int));
      faceID.ignore(7*sizeof(int));
    }
    else
    {
      ugrid  >> nnode >> ntri >> nquad >> ntet >> npyr >> npri >> nhex;
      faceID >> tmp   >> tmp  >> tmp   >> tmp  >> tmp  >> tmp  >> tmp;
    }

    if (swap)
    {
      byteswap(nnode);
      byteswap(ntri);
      byteswap(nquad);
      byteswap(ntet);
      byteswap(npyr);
      byteswap(npri);
      byteswap(nhex);
    }

    if (ntet > 0)
    {
      SANS_ASSERT(ntri > 0 && nhex == 0 && nquad == 0);
    }
    else if (nhex > 0)
    {
      SANS_ASSERT(nquad > 0 && ntet == 0 && ntri == 0);
    }
    else
      SANS_RUNTIME_EXCEPTION("Trying to read a 2D grid with the 3D grid reader...");
  }

  // Read in the grid coordinates
  readNodes(ugrid, faceID, mode, swap, nnode, xfldin);

  // Read the element groups
  readElements(ugrid, faceID, mode, swap, nnode, ntri, nquad, ntet, nhex, xfldin);

  // Connect all the elements
  this->buildFrom( xfldin );
}

//---------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
void
XField_ugrid<PhysDim, TopoDim>::
readNodes(std::ifstream& ugrid, std::ifstream& faceID, const std::ios_base::openmode mode,
          const bool swap, const int nnode, XField_Lagrange<PhysDim>& xfldin)
{
  std::shared_ptr<mpi::communicator> comm = xfldin.comm();

  typename XField_Lagrange<PhysDim>::VectorX X, tmp;

  // start the process for reading points on all processors
  xfldin.sizeDOF( nnode );

  // only rank 0 will read the grid
  if (comm->rank() == 0)
  {
    if (mode & std::ifstream::binary)
    {
      //Read in node coordinates
      for (int i = 0; i < nnode; i++)
      {
        ugrid.read((char*)&X[0], sizeof(Real));
        ugrid.read((char*)&X[1], sizeof(Real));
        ugrid.read((char*)&X[2], sizeof(Real));

        if (swap)
        {
          byteswap(X[0]);
          byteswap(X[1]);
          byteswap(X[2]);
        }

        xfldin.addDOF(X);
      }

      // advance the faceID reader
      faceID.ignore(nnode*3*sizeof(double));
    }
    else
    {
      //Read in node coordinates
      for (int i = 0; i < nnode; i++)
      {
        ugrid >> X[0] >> X[1] >> X[2];
        xfldin.addDOF(X);

        // advance the faceID reader
        faceID >> tmp[0] >> tmp[1] >> tmp[2];
      }
    }
  }
}

//---------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
void
XField_ugrid<PhysDim, TopoDim>::
readElements(std::ifstream& ugrid, std::ifstream& faceID, const std::ios_base::openmode mode,
             const bool swap, const int nnode,
             const int ntri, const int nquad,
             const int ntet, const int nhex,
             XField_Lagrange<PhysDim>& xfldin)
{
  std::shared_ptr<mpi::communicator> comm = xfldin.comm();
  std::ifstream::pos_type pos = 0;

  int tmp = 0;

  // Save the where the faces and face IDs start, and skip them
  if (comm->rank() == 0)
  {
    // save the position of the boundary data
    pos = ugrid.tellg();

    if (mode & std::ifstream::binary)
    {
      int nbyte = 0;
      // Skip ahead to the face ID section of the file
      // Skip triangles
      nbyte += 3*ntri*sizeof(int);

      // Skip quads
      nbyte += 4*nquad*sizeof(int);

      // advance the faceID file
      faceID.ignore(nbyte);

      // Skip the face ID section
      nbyte += ntri*sizeof(int);
      nbyte += nquad*sizeof(int);

      // actually advance the file
      ugrid.ignore(nbyte);
    }
    else
    {
      // Skip ahead to the face ID section of the file
      // Skip triangles
      for (int i = 0; i < ntri; i++)
      {
        ugrid >> tmp >> tmp >> tmp;
        faceID >> tmp >> tmp >> tmp;
      }

      // Skip quads
      for (int i = 0; i < nquad; i++)
      {
        ugrid >> tmp >> tmp >> tmp >> tmp;
        faceID >> tmp >> tmp >> tmp >> tmp;
      }

      // skip the face ID section
      for (int i = 0; i < ntri; i++)
        ugrid >> tmp;

      for (int i = 0; i < nquad; i++)
        ugrid >> tmp;
    }
  }

  //=======================
  //
  // Cell elements
  //
  //=======================

  int nCellTotal = ntet + nhex;

  // Start the process of adding cells on all processors
  xfldin.sizeCells(nCellTotal);

  if (comm->rank() == 0)
  {
    // only 1 cell group is supported
    int cellgroup = 0;
    int order = 1;
    if (ntet > 0)
    {
      std::vector<int> tetNodes(Tet::NNode);

      for (int i = 0; i < ntet; i++)
      {
        if (mode & std::ifstream::binary)
          ugrid.read((char*)tetNodes.data(), Tet::NNode*sizeof(int));
        else
          ugrid >> tetNodes[0] >> tetNodes[1] >> tetNodes[2] >> tetNodes[3];

        for (int j = 0; j < Tet::NNode; j++)
        {
          if (swap) byteswap(tetNodes[j]);
          tetNodes[j]--; //transform to 0-based indexing
        }

        // set the indices for this element
        xfldin.addCell(cellgroup, eTet, order, tetNodes);
      }
    }

    // assume no pyramids
    // assume no prisms

    if (nhex > 0)
    {
      std::vector<int> hexNodes(Hex::NNode);

      for (int i = 0; i < nhex; i++)
      {
        if (mode & std::ifstream::binary)
          ugrid.read((char*)hexNodes.data(), Hex::NNode*sizeof(int));
        else
          ugrid >> hexNodes[0] >> hexNodes[1] >> hexNodes[2] >> hexNodes[3]
                >> hexNodes[4] >> hexNodes[5] >> hexNodes[6] >> hexNodes[7];

        for (int j = 0; j < Hex::NNode; j++)
        {
          if (swap) byteswap(hexNodes[j]);
          hexNodes[j]--; //transform to 0-based indexing
        }

        // set the indices for this element
        xfldin.addCell(cellgroup, eHex, order, hexNodes);
      }
    }
  }


  //=======================
  //
  // Boundary trace groups
  //
  //=======================

  // Start the process of boundary trace elements
  xfldin.sizeBoundaryTrace(ntri + nquad);

  if (comm->rank() == 0)
  {
    // rewind to the boundary data
    ugrid.seekg(pos);

    int btracegroup;
    if (ntri > 0)
    {
      std::vector<int> triangleNodes(Triangle::NNode);

      for (int i = 0; i < ntri; i++)
      {
        if (mode & std::ifstream::binary)
          ugrid.read((char*)triangleNodes.data(), Triangle::NNode*sizeof(int));
        else
          ugrid >> triangleNodes[0] >> triangleNodes[1] >> triangleNodes[2];

        for (int j = 0; j < Triangle::NNode; j++)
        {
          if (swap) byteswap(triangleNodes[j]);
          triangleNodes[j]--; //transform to 0-based indexing
        }

        if (mode & std::ifstream::binary)
          faceID.read((char*)&btracegroup, sizeof(int));
        else
          faceID >> btracegroup;
        if (swap) byteswap(btracegroup);
        btracegroup--; //transform to 0-based indexing

        // set the indices for this element
        xfldin.addBoundaryTrace(btracegroup, eTriangle, triangleNodes);
      }
    }

    if (nquad > 0)
    {
      std::vector<int> quadNodes(Quad::NNode);

      for (int i = 0; i < nquad; i++)
      {
        if (mode & std::ifstream::binary)
          ugrid.read((char*)quadNodes.data(), Quad::NNode*sizeof(int));
        else
          ugrid >> quadNodes[0] >> quadNodes[1] >> quadNodes[2] >> quadNodes[3];

        for (int j = 0; j < Quad::NNode; j++)
        {
          if (swap) byteswap(quadNodes[j]);
          quadNodes[j]--; //transform to 0-based indexing
        }

        if (mode & std::ifstream::binary)
          faceID.read((char*)&btracegroup, sizeof(int));
        else
          faceID >> btracegroup;
        if (swap) byteswap(btracegroup);
        btracegroup--; //transform to 0-based indexing

        // set the indices for this element
        xfldin.addBoundaryTrace(btracegroup, eQuad, quadNodes);
      }
    }
  }

}

// explicit instantiation
template class XField_ugrid<PhysD3, TopoD3>;

}
