// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD_UGRID_H_
#define XFIELD_UGRID_H_

#include <ios>

#include "Topology/Dimension.h"

#include "Field/XField.h"

#include "MPI/communicator_fwd.h"

namespace SANS
{


template <class PhysDim, class TopoDim>
class XField_ugrid : public XField<PhysDim,TopoDim>
{
public:
  XField_ugrid(mpi::communicator comm, const std::string& filename);
  XField_ugrid( XField<PhysDim,TopoDim>& xfld, XFieldBalance graph, const std::string& filename );

protected:

  void read( XField_Lagrange<PhysDim>& xfldin, const std::string& filename );
  void read( XField_Lagrange<PhysDim>& xfldin, const std::ios_base::openmode mode, const bool swap, const std::string& filename );
  void readNodes(std::ifstream& input, std::ifstream& faceID, const std::ios_base::openmode mode,
                 const bool swap, const int nnode, XField_Lagrange<PhysDim>& xfldin);
  void readElements(std::ifstream& ugrid, std::ifstream& faceID, const std::ios_base::openmode mode,
                    const bool swap, const int nnode,
                    const int ntri, const int nquad,
                    const int ntet, const int nhex,
                    XField_Lagrange<PhysDim>& xfldin);
};

}

#endif /* XFIELD_UGRID_H_ */
