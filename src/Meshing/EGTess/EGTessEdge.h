// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EG_TESSEDGE_H
#define EG_TESSEDGE_H

//Represents a tessellated edge

#include "Meshing/EGADS/EGEdge.h"
#include "Meshing/EGADS/EGBody.h"
#include "EGVertexEdge.h"

#include <vector>

namespace SANS
{
namespace EGADS
{

template<int Dim_>
class EGTessEdge
{
public:
  static const int Dim = Dim_;
  typedef DLA::VectorS<Dim, Real> CartCoord;
  typedef EGVertexEdge<Dim> VertexType;

  EGTessEdge(const EGTessEdge& te);

  //A constructor that is mostly useful for testing
  EGTessEdge(const EGEdge<Dim>& edge, const int n, CartCoord* X);

  //Constructor for extracting the tessellation on edges from
  //a body tessellation
  EGTessEdge(const EGBody<Dim>& body, const EGEdge<Dim>& edge, ego tess, int eIndex,
             CartCoord* X, CartCoord* Xnodes);

  EGTessEdge(const EGBody<Dim>& body, const EGEdge<Dim>& edge, const int *ptype,
             const double *puvt,
             std::vector<int>& nodeMap,
             CartCoord* X, CartCoord* Xnodes);

  EGTessEdge& operator=(const EGTessEdge& te);

  int nVertex() const { return static_cast<int>(Vertexes_.size()); }
  int nElem() const { return nVertex()-1; }

  //1 parameter per vertex for edges
  //exclude end points which are attached to one or two nodes
  int nFreeParameters() const { return nVertex()-2; }

  //Number of free vertexes (happens to be the same as number of free parameters for an edge)
  int nFreeVertex() const { return nVertex()-2; }

  //Populates x with the current t values
  void fillFreeParameters(std::vector<double>& x) const;

  //Updates the movable vertexes at the new t locations
  void evaluate(const int n, const double* pt);

  VertexType& getVertex(const int n) { return Vertexes_[n]; }
  VertexType& getFreeVertex(const int n) { return Vertexes_[n+1]; }

  Real getReferenceLength() const;

  //TODO: Probably need a better fudge factor here...
  double lowerParamBound() const { return edge_.getParamRange()[0]+1.e-6; }
  double upperParamBound() const { return edge_.getParamRange()[1]-1.e-6; }

  void sort();

  EGEdge<Dim>& edge() { return edge_; }
  EGEdge<Dim>& getTopo() { return edge_; }
  //XField2DLineBase* getXFieldLine() { return xfldedge_; }

protected:
  EGEdge<Dim> edge_;
  std::vector< VertexType > Vertexes_;
  //XField2DLineBase*& xfldedge_;
};

}
}

#endif //EG_TESSEDGE_H
