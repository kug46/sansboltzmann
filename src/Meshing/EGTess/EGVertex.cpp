// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "EGVertex.h"

namespace SANS
{
namespace EGADS
{

template<int Dim_>
EGVertex<Dim_>::~EGVertex()
{
}

//Explicitly instantiate the class
template class EGVertex<2>;
template class EGVertex<3>;

}
}
