// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EG_TESSELLATION_H
#define EG_TESSELLATION_H

//A tessellation

#include <vector>

#include "Field/XFieldArea.h"
#include "Meshing/EGADS/EGBody.h"
#include "EGTessEdge.h"
#include "EGTessFace.h"

#include "Topology/ElementTopology.h"

namespace SANS
{
namespace EGADS
{

template<int Dim_>
class EGTessellation : public XField<PhysD2, TopoD2>
{
public:
  static const int Dim = Dim_;
  typedef XField<PhysD2, TopoD2> BaseType;
  typedef BaseType::VectorX CartCoord;

  EGTessellation( const EGTessellation& tess, const FieldCopy& tag ) : BaseType(tess, tag),
                                                                       tessEdges_(tess.tessEdges_),
                                                                       tessFaces_(tess.tessFaces_),
                                                                       ntri_(tess.ntri_) {}

  //Constructor for tessBody
  EGTessellation( const EGBody<Dim>& body, std::vector<int>& edge_vertex_count, std::vector<int>& face_vertex_count );

  int nEdges() { return static_cast<int>(tessEdges_.size()); }
  EGTessEdge<Dim>& getTessEdge( const int n ) { return tessEdges_[n]; }
  EGTessFace<Dim>& getTessFace( const int n ) { return tessFaces_[n]; }
  int ntri() { return ntri_; }

  int nVertex() { return nDOF_; }
  CartCoord& X(const int n) { return DOF_[n]; }

protected:
  std::vector< EGTessEdge<Dim> > tessEdges_;
  std::vector< EGTessFace<Dim> > tessFaces_;
  int ntri_;
};

}
}

#endif //EG_TESSELATION_H
