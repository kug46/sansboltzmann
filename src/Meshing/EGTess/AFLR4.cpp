// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "AFLR4.h"

#include <ug/UG_LIB.h>
#include <aflr4/AFLR4_LIB.h>

#include "tools/SANSException.h"

namespace SANS
{

struct AFLR4Defaults
{
protected:
  UG_Param_Struct *AFLR4_Param_Struct_Ptr_ = NULL; // AFRL4 input structure used to get default values

public:
  AFLR4Defaults()
  {
    INT_ iget; // error code

    iget = ug_malloc_param (&AFLR4_Param_Struct_Ptr_);
    if ((iget != 0) || (AFLR4_Param_Struct_Ptr_ == NULL))
    {
      printf("ug_malloc_param status = %d\n", iget);
      SANS_ASSERT(false);
    }

    iget = ug_initialize_param (AFLR4_Param_Struct_Ptr_);
    if (iget != 0)
    {
        printf("ug_initialize_param status = %d\n", iget);
        SANS_ASSERT(false);
    }

    iget = ug_initialize_aflr_param (4, AFLR4_Param_Struct_Ptr_);
    if (iget != 0)
    {
        printf("ug_initialize_aflr_param status = %d\n", iget);
        SANS_ASSERT(false);
    }

    iget = aflr4_initialize_param (AFLR4_Param_Struct_Ptr_);
    if (iget != 0)
    {
      printf("aflr4_initialize_param status = %d\n", iget);
      SANS_ASSERT(false);
    }
  }

  void get_ug_defaults(const std::string& param_name, const ParameterNumeric<int>& param)
  {
    int idef, imin, imax;
    std::string def = param_name + "@def";
    SANS_ASSERT( 1 == ug_get_int_param ((char*)def.c_str(), &idef, AFLR4_Param_Struct_Ptr_) );

    std::string min = param_name + "@min";
    SANS_ASSERT( 1 == ug_get_int_param ((char*)min.c_str(), &imin, AFLR4_Param_Struct_Ptr_) );

    std::string max = param_name + "@max";
    SANS_ASSERT( 1 == ug_get_int_param ((char*)max.c_str(), &imax, AFLR4_Param_Struct_Ptr_) );

    const_cast<int&>(const_cast<ParameterNumeric<int>&>(param).Default) = idef;
    const_cast<int&>(const_cast<ParameterNumeric<int>&>(param).min) = imin;
    const_cast<int&>(const_cast<ParameterNumeric<int>&>(param).max) = imax;
  }

  void get_ug_defaults(const std::string& param_name, const ParameterNumeric<double>& param)
  {
    double ddef, dmin, dmax;
    std::string def = param_name + "@def";
    SANS_ASSERT( 1 == ug_get_double_param ((char*)def.c_str(), &ddef, AFLR4_Param_Struct_Ptr_) );

    std::string min = param_name + "@min";
    SANS_ASSERT( 1 == ug_get_double_param ((char*)min.c_str(), &dmin, AFLR4_Param_Struct_Ptr_) );

    std::string max = param_name + "@max";
    SANS_ASSERT( 1 == ug_get_double_param ((char*)max.c_str(), &dmax, AFLR4_Param_Struct_Ptr_) );

    const_cast<double&>(const_cast<ParameterNumeric<double>&>(param).Default) = ddef;
    const_cast<double&>(const_cast<ParameterNumeric<double>&>(param).min) = dmin;
    const_cast<double&>(const_cast<ParameterNumeric<double>&>(param).max) = dmax;
  }

  ~AFLR4Defaults() { ug_free_param(AFLR4_Param_Struct_Ptr_); }
};


//---------------------------------------------------------------------------//
AFLR4Params::AFLR4Params()
{
  // extract the default values from AFLR4 and assign them here with lots of casting
  AFLR4Defaults def;

  def.get_ug_defaults("min_ncell"    , min_ncell    );
  def.get_ug_defaults("mer_all"      , mer_all      );

  def.get_ug_defaults("ff_cdfr"      , ff_cdfr);
  def.get_ug_defaults("abs_min_scale", abs_min_scale);
  def.get_ug_defaults("BL_thickness" , BL_thickness );
  def.get_ug_defaults("curv_factor"  , curv_factor  );
  def.get_ug_defaults("length_ratio" , length_ratio );
  def.get_ug_defaults("erw_all"      , erw_all      );
  def.get_ug_defaults("max_scale"    , max_scale    );
  def.get_ug_defaults("min_scale"    , min_scale    );

  def.get_ug_defaults("cdfr"         , cdfr         );
}

//---------------------------------------------------------------------------//
void
AFLR4Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;

  allParams.push_back(d.checkInputs(params.min_ncell));
  allParams.push_back(d.checkInputs(params.mer_all));

  allParams.push_back(d.checkInputs(params.ff_cdfr));
  allParams.push_back(d.checkInputs(params.abs_min_scale));
  allParams.push_back(d.checkInputs(params.BL_thickness));
  allParams.push_back(d.checkInputs(params.curv_factor));
  allParams.push_back(d.checkInputs(params.length_ratio));
  allParams.push_back(d.checkInputs(params.erw_all));
  allParams.push_back(d.checkInputs(params.max_scale));
  allParams.push_back(d.checkInputs(params.min_scale));

  allParams.push_back(d.checkInputs(params.ref_len));

  allParams.push_back(d.checkInputs(params.cdfr));
  allParams.push_back(d.checkInputs(params.mdf));

  d.checkUnknownInputs(allParams);
}
AFLR4Params AFLR4Params::params;

}
