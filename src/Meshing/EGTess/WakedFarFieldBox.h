// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php


#include "Meshing/EGADS/EGBody.h"

#include <vector>

namespace SANS
{
namespace EGADS
{

std::vector< EGBody<3> >
WakedFarFieldBox( const std::vector< EGBody<3> >& bodies,
                  const std::vector<double>& boxsize,
                  std::vector<int>& TrefftzFrames );

} // namespace EGADS
} // namespace SANS
