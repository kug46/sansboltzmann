// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

/*
 *      EGADS: Electronic Geometry Aircraft Design System
 *
 *      Copyright 2011-2014, Massachusetts Institute of Technology
 *      Licensed under The GNU Lesser General Public License, version 2.1
 *      See http://www.opensource.org/licenses/lgpl-2.1.php
 *
 */

#include "egads.h"

/*
 *  returns a complete tessellation of a body with all face tessellations zipperd with complete connectivity between faces
 *
 *  where:  tess          - tessellation of a body
 *          len           - Number of vertices (returned)
 *          pxyz          - cartesian coordinates (returned) 3*len in length -- freeable
 *          puvt          - parametric coordinates on faces and edges (returned) 2*len in length -- freeable
 *          ptype         - type of each vertex (returned) len in length -- freeable
 *          pindex        - index of each vertex (returned) len in length -- freeable
 *          ntri          - number of triangles (returned)
 *          ptris         - triangle indices (returned) 3*ntri in length -- freeable
 *          ptric         - triangle neighbor information 3*ntri in length -- freeable
 *          nbndedges     - number of edges on the boundary of the solid body
 *          pbndedgeindex - array if edge inexes that define the boundary of the soid body -- freeable
 */

int
EG_getTessBody(ego tess, int *len, double **pxyz, double **puvt,
                         int **ptype, int **pindex,
                         int *ntri, int **ptris, int **ptric,
                         int **ntriface,
                         int *nbndedges, int **pbndedgeindex)
{
  int     status, i, j, k, n, m, base, npts, ipts, nftri, *tri = NULL, *ftric = NULL, *table = NULL;
  int     oclass, mtype, atype, alen;
  int     nedge = 0, nface, plen, elen, tlen, *types = NULL, *indexs = NULL;
  int     n0L, n1L, n0R, n1R;
  int     *nedgetable = NULL, **edgetable = NULL;
  int     nbndedge, *pbndedgeindx = NULL;
  const int    *tris = NULL, *tric = NULL, *type = NULL, *index = NULL, *pints = NULL;
  const double *points = NULL, *uv = NULL, *preals = NULL;
  const double *exyz = NULL, *pt = NULL;
  double *xyzs = NULL, *uvts = NULL;
  const char *string = NULL;
  ego *pfaces = NULL, *pedges = NULL, topRef = NULL, prev = NULL, next = NULL;
  ego body = NULL;
  egTessel *btess = NULL;

  const int sides[3][2] = {{1,2}, {2,0}, {0,1}};

  /* This is a potential memory leak! */
  *len  = *ntri = *nbndedges = 0;
  *pxyz  = *puvt = NULL;
  *ptype = *pindex = NULL;
  *ptris = *ptric = NULL;
  *ntriface = NULL;
  *pbndedgeindex = NULL;

  if (tess == NULL)                 return EGADS_NULLOBJ;
  if (tess->magicnumber != MAGIC)   return EGADS_NOTOBJ;
  if (tess->oclass != TESSELLATION) return EGADS_NOTTESS;
  if (tess->blind == NULL)          return EGADS_NODATA;

  //int outLevel = EG_outLevel(tess);

  btess = (egTessel *)tess->blind;
  body = btess->src;

  if (body->oclass != BODY)       return EGADS_NOTBODY;

  /* Get the number of faces and edges */
  status = EG_getBodyTopos(body, NULL, FACE, &nface, &pfaces);
  if (status != EGADS_SUCCESS) {
    printf(" EGADS Error: EG_getBodyTopos status = %d (EG_getTessBody)!\n", status);
    return status;
  }


  /* Count the total number of points and triangles (points are duplicated on edges) */
  npts = nftri = 0;
  for (j = 1; j <= nface; j++) {
    status = EG_getTessFace(tess, j, &plen, &points, &uv, &type, &index,
                            &tlen, &tris, &tric);
    if (status != EGADS_SUCCESS) {
      printf(" EGADS Error: Face %d: EG_getTessFace status = %d (EG_getTessBody)!\n", j, status);
      goto cleanup;
    }
    npts += plen;
    nftri += tlen;
  }

  /* get the memory associated with the points */

  table = (int *) EG_alloc(2*npts*sizeof(int));
  if (table == NULL) {
    printf(" EGADS Error: Can not allocate node table (EG_getTessBody)!\n");
    status = EGADS_MALLOC;
    goto cleanup;
  }
  types = (int *) EG_alloc(npts*sizeof(int));
  if (types == NULL) {
    printf(" EGADS Error: Can not allocate types (EG_getTessBody)!\n");
    status = EGADS_MALLOC;
    goto cleanup;
  }
  indexs = (int *) EG_alloc(npts*sizeof(int));
  if (indexs == NULL) {
    printf(" EGADS Error: Can not allocate indexs (EG_getTessBody)!\n");
    status = EGADS_MALLOC;
    goto cleanup;
  }
  xyzs = (double *) EG_alloc(3*npts*sizeof(double));
  if (xyzs == NULL) {
    printf(" EGADS Error: Can not allocate XYZs (EG_getTessBody)!\n");
    status = EGADS_MALLOC;
    goto cleanup;
  }
  uvts = (double *) EG_alloc(2*npts*sizeof(double));
  if (uvts == NULL) {
    printf(" EGADS Error: Can not allocate UVTs (EG_getTessBody)!\n");
    status = EGADS_MALLOC;
    goto cleanup;
  }

  /* zipper up the edges -- a Face at a time */

  k = npts = 0;
  for (j = 1; j <= nface; j++) {
    status = EG_getTessFace(tess, j, &plen, &points, &uv, &type, &index,
                            &tlen, &tris, &tric);
    if (status != EGADS_SUCCESS) {
      printf(" EGADS Error: Face %d: EG_getTessFace status = %d (EG_getTessBody)!\n", j, status);
      goto cleanup;
    }

    ipts = npts;
    if ( EG_attributeRet(pfaces[j-1], "->uvt", &atype, &alen, &pints, &preals, &string) == EGADS_SUCCESS ) {
      // only use the list if it is int's
      if ( !(atype == ATTRREAL && alen == 2*plen) ) {
        printf(" EGADS Error: Attribute '->uvt' should be ATTRREAL and len = %d (EG_getTessBody)!\n", plen);
        status = EGADS_OUTSIDE;
        goto cleanup;
      }
      for ( i = 0; i < plen; i++ ) {
        uvts[ 2*ipts     ] = preals[2*i+0];
        uvts[ 2*ipts + 1 ] = preals[2*i+1];
        ipts++;
      }
    }
    else
    {
      for ( i = 0; i < plen; i++ ) {
        if ( type[i] == 0 ) {
          //Just set the node parametric coordinates to zero
          uvts[2*ipts + 0] = uvts[2*ipts + 1] = 0;

        } else if ( type[i] > 0 ) {

          //Extract the edge tessellation
          status = EG_getTessEdge(tess, index[i], &elen, &exyz, &pt);
          if (status != EGADS_SUCCESS) {
            printf(" EGADS Error: Face %d: Edge %d: EG_getTessEdge status = %d (EG_getTessBody)!\n", j, index[i], status);
            goto cleanup;
          }

          //Update the parametric coordinates with the edge t values
          uvts[ 2*ipts     ] = pt[type[i]-1];
          uvts[ 2*ipts + 1 ] = 0;

        } else {

          //Simply copy over the interior parametric coordinates
          for ( n = 0; n < 2; n++)
            uvts[2*ipts + n] = uv[2*i + n];
        }
        ipts++;
      }
    }

    // Use a custom type/index field if requested. This should be used carefully and could break the process
    if ( EG_attributeRet(pfaces[j-1], "->type", &atype, &alen, &pints, &preals, &string) == EGADS_SUCCESS ) {
      // only use the list if it is int's
      if ( !(atype == ATTRINT && alen == plen) ) {
        printf(" EGADS Error: Attribute '->type' should be ATTRINT and len = %d (EG_getTessBody)!\n", plen);
        status = EGADS_OUTSIDE;
        goto cleanup;
      }
      type = pints;
    }
    if ( EG_attributeRet(pfaces[j-1], "->index", &atype, &alen, &pints, &preals, &string) == EGADS_SUCCESS ) {
      // only use the list if it is int's
      if ( !(atype == ATTRINT && alen == plen) ) {
        printf(" EGADS Error: Attribute '->index' should be ATTRINT and len = %d (EG_getTessBody)!\n", plen);
        status = EGADS_OUTSIDE;
        goto cleanup;
      }
      index = pints;
    }

    for (i = 0; i < plen; i++) {
      table[2*npts  ] = type[i];
      table[2*npts+1] = index[i];
      indexs[npts] = (type[i] < 0) ? j : index[i];
      types[npts] = type[i];
      for ( n = 0; n < 3; n++)
        xyzs[3*npts + n] = points[3*i + n];
      /* for non-interior pts -- try to match with others */
      if (type[i] >= 0)
        for (k = 0; k < npts; k++)
          if ((table[2*k]==table[2*npts]) && (table[2*k+1]==table[2*npts+1])) {
            table[2*npts  ] = k;
            table[2*npts+1] = 0;
            break;
          }
      npts++;
    }
  }

  /* fill up the whole triangle list -- a Face at a time */

  tri = (int *) EG_alloc(3*nftri*sizeof(int));
  if (tri == NULL) {
    printf(" EGADS Error: Can not allocate triangles (EG_getTessBody)!\n");
    status = EGADS_MALLOC;
    goto cleanup;
  }
  ftric = (int *) EG_alloc(3*nftri*sizeof(int));
  if (ftric == NULL) {
    printf(" EGADS Error: Can not allocate triangle neighbors (EG_getTessBody)!\n");
    status = EGADS_MALLOC;
    goto cleanup;
  }


  int tribase = 0;
  nftri = base = 0;
  for (j = 1; j <= nface; j++) {

    /* get the face tessellation and store it away */
    status = EG_getTessFace(tess, j, &plen, &points, &uv, &type, &index,
                            &tlen, &tris, &tric);
    if (status != EGADS_SUCCESS) {
      printf(" EGADS Error: Face %d: EG_getTessFace status = %d (EG_getTessBody)!\n", j, status);
      goto cleanup;
    }
    for (i = 0; i < tlen; i++, nftri++) {
      for (n = 0; n < 3; n++) {
        k = tris[3*i+n] + base;
        if (table[2*k-1] == 0) {
          tri[3*nftri+n] = table[2*k-2] + 1;
        } else {
          tri[3*nftri+n] = k;
        }
        ftric[3*nftri+n] = tric[3*i+n];
        if ( ftric[3*nftri+n] > 0 ) ftric[3*nftri+n] += tribase;
      }
    }
    base += plen;
    tribase += tlen;
  }

  /* remove the unused points -- crunch the point list
   *    NOTE: the returned pointer verts has the full length (not realloc'ed)
   */

  for (i = 0; i <   npts; i++) table[i] = 0;
  for (i = 0; i < 3*nftri; i++) table[tri[i]-1]++;
  for (plen = i = 0; i < npts; i++) {
    if (table[i] == 0) continue;
    xyzs[3*plen  ] = xyzs[3*i  ];
    xyzs[3*plen+1] = xyzs[3*i+1];
    xyzs[3*plen+2] = xyzs[3*i+2];
    uvts[2*plen  ] = uvts[2*i  ];
    uvts[2*plen+1] = uvts[2*i+1];
    types[plen] = types[i];
    indexs[plen] = indexs[i];
    plen++;
    table[i] = plen;
  }
  npts = plen;

  /* reset the triangle indices */
  for (i = 0; i < 3*nftri; i++) {
    k      = tri[i]-1;
    tri[i] = table[k];
  }
  EG_free(table); table = NULL;

  /* now fix the triangle neighbor information */

  status = EG_getBodyTopos(body, NULL, EDGE, &nedge, &pedges);
  if (status != EGADS_SUCCESS) {
    printf(" EGADS Error: EG_getBodyTopos status = %d (EG_getTessBody)!\n", status);
    goto cleanup;
  }

  /* The edge table contains all triangles that share a common edge */
  edgetable = (int **) EG_alloc(nedge*sizeof(int*));
  if (edgetable == NULL) {
    printf(" EGADS Error: Can not allocate edgetable (EG_getTessBody)!\n");
    status = EGADS_MALLOC;
    goto cleanup;
  }
  for (j = 0; j < nedge; j++) {
    edgetable[j] = NULL;
  }

  nedgetable = (int *) EG_alloc(nedge*sizeof(int));
  if (nedgetable == NULL) {
    printf(" EGADS Error: Can not allocate edgetable (EG_getTessBody)!\n");
    status = EGADS_MALLOC;
    goto cleanup;
  }

  /* zero out the segment count */
  for (j = 1; j <= nedge; j++) {
    nedgetable[j-1] = 0;
  }


  /* count the number of segments on each edge */
  for (i = 0; i < nftri; i++) {
    for ( n = 0; n < 3; n++) {
      if (ftric[3*i+n] < 0) {
        j = abs(ftric[3*i+n])-1;
        nedgetable[j]++;
      }
    }
  }

  /* Allocate individual arrays in the edge table */
  for (j = 1; j <= nedge; j++) {
    status = EG_getTessEdge(tess, j, &plen, &points, &uv);
    if (status != EGADS_SUCCESS) {
      printf(" EGADS Error: Edge %d: EG_getTessEdge status = %d (EG_getTessBody)!\n", j, status);
      goto cleanup;
    }

    if ( nedgetable[j-1] == 0) {
      edgetable[j-1] = NULL;

      /* no segments on edge must be a degenerate edge, so check just to make sure */
      status = EG_getInfo(pedges[j-1], &oclass, &mtype, &topRef, &prev, &next);
      if (status != EGADS_SUCCESS) {
        printf(" EGADS Error: Edge %d: EG_getInfo status = %d (EG_getTessBody)!\n", j, status);
        goto cleanup;
      }

      if (mtype != DEGENERATE) {
        printf(" EGADS Error: Edge %d: Should be degenerate, but it's not!! (EG_getTessBody)!\n", j);
        goto cleanup;
      }

    } else {
      edgetable[j-1] = (int *) EG_alloc(nedgetable[j-1]*sizeof(int));
      if (edgetable[j-1] == NULL ) {
        printf(" EGADS Error: Can not allocate edgetable (EG_getTessBody)!\n");
        status = EGADS_MALLOC;
        goto cleanup;
      }
    }
    /* reset the counter again */
    nedgetable[j-1] = 0;
  }

  /* relese the edge memory now */
  EG_free(pedges); pedges = NULL;

  /* Gather up all triangles that are on edges */

  for (i = 0; i < nftri; i++) {
    for ( n = 0; n < 3; n++) {
      if (ftric[3*i+n] < 0) {
        j = abs(ftric[3*i+n])-1;
        edgetable[j][nedgetable[j]++] = i;
      }
    }
  }

  nbndedge = 0;
  for (j = 0; j < nedge; j++) {
    status = EG_getTessEdge(tess, j+1, &plen, &points, &uv);
    if (status != EGADS_SUCCESS) {
      printf(" EGADS Error: Edge %d: EG_getTessEdge status = %d (EG_getTessBody)!\n", j+1, status);
      goto cleanup;
    }

    /* Count the number of boundary edges that cannot be zippered */
    if ( nedgetable[j] != 2*(plen-1) ) {
      nbndedge++;
    }
  }

  pbndedgeindx = (int *) EG_alloc(nbndedge*sizeof(int*));
  if ( pbndedgeindx == NULL && nbndedge > 0) {
    printf(" EGADS Error: Can not allocate pbndedgeindex (EG_getTessBody)!\n");
    status = EGADS_MALLOC;
    goto cleanup;
  }

  nbndedge = 0;
  for (j = 0; j < nedge; j++) {
    status = EG_getTessEdge(tess, j+1, &plen, &points, &uv);
    if (status != EGADS_SUCCESS) {
      printf(" EGADS Error: Edge %d: EG_getTessEdge status = %d (EG_getTessBody)!\n", j+1, status);
      goto cleanup;
    }

    /* Don't zipper if the edge does not have duplicate triangles (could be more for non-manifold)*/
    if ( nedgetable[j] != 2*(plen-1) ) {
      pbndedgeindx[nbndedge++] = j+1;
      continue;
    }

    for (i = 0; i < nedgetable[j]/2; i++) {
      for (n = 0; n < 3; n++) {
        if ( ftric[3*edgetable[j][i]+n] < 0 ) {

          n0L = tri[3*edgetable[j][i]+sides[n][0]];
          n1L = tri[3*edgetable[j][i]+sides[n][1]];

          for (k = 0; k < nedgetable[j]; k++) {

            /* Can't be a neigbor to it self */
            if ( i == k ) continue;

            for (m = 0; m < 3; m++) {
              /* Check if the two triangles share a common nodes */
              n0R = tri[3*edgetable[j][k]+sides[m][0]];
              n1R = tri[3*edgetable[j][k]+sides[m][1]];

              /* Set as neighbors if the two triangles share common nodes */
              if ( (n0L == n0R && n1L == n1R) ||
                   (n0L == n1R && n1L == n0R) ) {
                ftric[3*edgetable[j][i]+n] = edgetable[j][k]+1;
                ftric[3*edgetable[j][k]+m] = edgetable[j][i]+1;
              }
            }
          }
        }
      }
    }
  }


  EG_free(pfaces); pfaces = NULL;
  EG_free(nedgetable); nedgetable = NULL;
  for (i = 0; i < nedge; i++) {
    EG_free(edgetable[i]); edgetable[i] = NULL;
  }
  EG_free(edgetable); edgetable = NULL;


  // Crete the ranges of triangles for each face
  *ntriface = (int *) EG_alloc((nface+1)*sizeof(int*));
  if ( *ntriface == NULL) {
    printf(" EGADS Error: Can not allocate pbndedgeindex (EG_getTessBody)!\n");
    status = EGADS_MALLOC;
    goto cleanup;
  }

  (*ntriface)[0] = 0;

  for (j = 1; j <= nface; j++) {
    status = EG_getTessFace(tess, j, &plen, &points, &uv, &type, &index,
                            &tlen, &tris, &tric);
    if (status != EGADS_SUCCESS) {
      printf(" EGADS Error: Face %d: EG_getTessFace status = %d (EG_getTessBody)!\n", j, status);
      goto cleanup;
    }

    (*ntriface)[j] = (*ntriface)[j-1] + tlen;
  }

  /* Resize the memory and assing to ouput pointers */
  *len    = npts;
  *pxyz   = (double*)EG_reall(xyzs, 3*npts*sizeof(double));
  *puvt   = (double*)EG_reall(uvts, 2*npts*sizeof(double));
  *ptype  = (int*)EG_reall(types, npts*sizeof(int));
  *pindex = (int*)EG_reall(indexs, npts*sizeof(int));
  *ntri   = nftri;
  *ptris  = (int*)EG_reall(tri, 3*nftri*sizeof(int));
  *ptric  = (int*)EG_reall(ftric, 3*nftri*sizeof(int));
  *nbndedges = nbndedge;
  *pbndedgeindex = pbndedgeindx;

  return EGADS_SUCCESS;

cleanup:
  EG_free(pfaces);
  EG_free(pedges);
  EG_free(tri);
  EG_free(ftric);
  EG_free(xyzs);
  EG_free(uvts);
  EG_free(types);
  EG_free(indexs);
  EG_free(nedgetable);
  for (i = 0; i < nedge; i++)
    EG_free(edgetable[i]);
  EG_free(edgetable);
  EG_free(pbndedgeindx);

  return status;
}

