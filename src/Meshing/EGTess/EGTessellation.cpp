// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "EGTessellation.h"
#include "Meshing/EGADS/tools.h"

#include "BasisFunction/BasisFunctionArea_Triangle.h"

#include <memory> //shared_ptr

extern "C"
{
int
EG_limitTessBody(ego object, int *elimits, int *flimits, ego *tess);

int
EG_getTessBody(ego tess, int *len, double **pxyz, double **puvt,
                         int **ptype, int **pindex,
                         int *ntri, int **ptris, int **ptric,
                         int **ntriface,
                         int *nbndedges, int **pbndedgeindex);
}

namespace SANS
{
namespace EGADS
{


template<int Dim_>
EGTessellation<Dim_>::EGTessellation( const EGBody<Dim>& body, std::vector<int>& edge_vertex_count, std::vector<int>& face_vertex_count )
{
  //Extract all faces
  std::vector< EGFace<Dim> > faces = body.getFaces();

  //Extract all edges of the body and tessellate them
  std::vector< EGEdge<Dim> > edges = body.getEdges();

  //Extract all nodes of the body to be used in the edge tessellation
  std::vector< EGNode<Dim> > nodes = body.getNodes();


  ego tess;
#if 1
  double params[] = {0.2, 0.001, 15};

  //Create the body tessellation
  EG_STATUS( EG_makeTessBody(body, params, &tess) );
#else
  SANS_ASSERT(face_vertex_count.size() == faces.size());
  SANS_ASSERT(edge_vertex_count.size() == edges.size());
  EG_STATUS( EG_limitTessBody(body, &edge_vertex_count[0], &face_vertex_count[0], &tess) );
#endif

  std::unique_ptr<egObject,EGADS_deleteObject> deltess(tess, EGADS_deleteObject());

  //Get the complete body tessellation
  int nbndedges;
  int *ptype, *pindex, *ptris, *ptric, *ntriface, *pbndedgeindex;
  double *pxyz, *puvt;
  EG_STATUS( EG_getTessBody(tess, &nDOF_, &pxyz, &puvt,
                            &ptype, &pindex,
                            &ntri_, &ptris, &ptric,
                            &ntriface,
                            &nbndedges, &pbndedgeindex) );

  std::unique_ptr<int[],EGADS_free> deltype (ptype, EGADS_free());
  std::unique_ptr<int[],EGADS_free> delindex (pindex, EGADS_free());
  std::unique_ptr<int[],EGADS_free> deltris (ptris, EGADS_free());
  std::unique_ptr<int[],EGADS_free> deltric (ptric, EGADS_free());
  std::unique_ptr<int[],EGADS_free> delntriface (ntriface, EGADS_free());

  std::unique_ptr<double[],EGADS_free> delxyz (pxyz, EGADS_free());
  std::unique_ptr<double[],EGADS_free> deluvt (puvt, EGADS_free());

  std::unique_ptr<int[],EGADS_free> delbndedgeindex (pbndedgeindex, EGADS_free());


  std::vector<int> nodeMap(nodes.size());

  this->resizeDOF(nDOF_);

  for (int i = 0; i < nDOF_; i++)
  {
    for (int d = 0; d < Dim; d++)
      DOF_[i][d] = pxyz[i*3 + d];

//    std::cout << "pindex[" << i << "] = " << pindex[i]
//              << " ptype[" << i << "] = " << ptype[i]
//              << " puv[" << i << "] = " << puv[2*i+0]  << ", " << puv[2*i+1] << std::endl;

    if (ptype[i] == 0)
      nodeMap[pindex[i]-1] = i;
  }


  //All edges are treated as boundary groups for now...
  resizeBoundaryTraceGroups(nbndedges);
  resizeInteriorTraceGroups(edges.size()-nbndedges+faces.size());

  typedef FieldTraceGroupType<Line>::FieldAssociativityConstructorType TraceConstructorType;

  std::vector< TraceConstructorType > fldAssocBedge( nbndedges );
  std::vector< TraceConstructorType > fldAssocIedge( edges.size()-nbndedges );

  std::vector< std::pair<int,bool> > edgeIndexMap(edges.size());

  int eBndIndex = 0;
  int eIntIndex = 0;
  for ( int vert = 0; vert < nDOF_; vert++ )
  {
    if ( ptype[vert] > 0 ) // Edge type
    {
      int eIndex = pindex[vert]; // Edge index
      bool bndEdge = false;

      tessEdges_.emplace_back(body, edges[eIndex-1], &ptype[vert], &puvt[vert], nodeMap, &DOF_[vert], DOF_);

      //Check to see if this edge is a boundary edge
      for ( int ibnd = 0; ibnd < nbndedges; ibnd++ )
        if ( pbndedgeindex[ibnd] == eIndex )
        {
          fldAssocBedge[eBndIndex].resize( BasisFunctionLineBase::HierarchicalP1, tessEdges_.back().nElem() );

          edgeIndexMap[eIndex-1] = std::pair<int,bool>(eBndIndex, true);
          eBndIndex++;
          bndEdge = true;
          break;
        }

      //Not a boundary edge, so it's an interior edge
      if (!bndEdge)
      {
        fldAssocIedge[eIntIndex].resize( BasisFunctionLineBase::HierarchicalP1, tessEdges_.back().nElem() );

        edgeIndexMap[eIndex-1] = std::pair<int,bool>(eIntIndex, false);
        eIntIndex++;
      }


      //Increment the vertex until we have skipped all vertexes on the edge
      while ( vert < nDOF_ && pindex[vert] == eIndex && ptype[vert] > 0 )
        vert++;
    }
  }

  //Just to make sure there is no extra memory
  tessEdges_.shrink_to_fit();


  // Create the area associativity constructor classes
  std::vector< FieldCellGroupType<Triangle>::FieldAssociativityConstructorType > xfldAreas( faces.size() );

  for (std::size_t fIndex = 0; fIndex < faces.size(); fIndex++ )
  {
    //Extract the number of triangles for this face
    int nftri = 0;
    {
      int zlen;
      const double *zxyz, *zuv;
      const int *ztype, *zindex, *ztris, *ztric;
      EG_STATUS( EG_getTessFace(tess, fIndex+1, &zlen, &zxyz, &zuv, &ztype, &zindex, &nftri, &ztris, &ztric) );
    }

    xfldAreas[fIndex].resize( BasisFunctionAreaBase<Triangle>::HierarchicalP1, nftri );
  }


  //Indexes to form the sides of a triangle
  const int segnodes[3][2] = {{1,2}, {2,0}, {0,1}};

  std::vector<int> nsegs(edges.size(),0);

  //Loop over all triangles and extract the segments that reside on edges
  for (int itriL = 0; itriL < ntri_; itriL++)
  {

    for (int segL = 0; segL < 3; segL++)
    {

      int n1 = ptris[3*itriL+segnodes[segL][0]]-1;
      int n2 = ptris[3*itriL+segnodes[segL][1]]-1;

      if ( ((ptype[n1] > 0 && ptype[n2] > 0) && //Two vertexes on edges?
           (pindex[n1] == pindex[n2]) )      || //and same edge?
           (ptype[n1] > 0 && ptype[n2] == 0) || //Vertex on edge and vertex on node?
           (ptype[n1] == 0 && ptype[n2] > 0)    //Vertex on node and vertex on edge?
          )
      {
        int eIndex = ptype[n1] > 0 ? pindex[n1] : pindex[n2];
        int nseg = nsegs[eIndex-1];
        nsegs[eIndex-1]++;

        if ( edgeIndexMap[eIndex-1].second ) //Boundary edge?
        {
          TraceConstructorType& xfldBedge = fldAssocBedge[edgeIndexMap[eIndex-1].first];

          // Find the face index associated with the cell group next to the edge
          int findexL = 0;
          while ( findexL <= (int)faces.size() && !(ntriface[findexL] <= itriL && itriL < ntriface[findexL+1]) ) findexL++;

          // set the left group
          xfldBedge.setGroupLeft( findexL );

          xfldBedge.setAssociativity( nseg ).setRank( 0 );

          // grid-coordinate DOF association (edge-to-node connectivity)
          xfldBedge.setAssociativity( nseg ).setNodeGlobalMapping( {n1, n2} );

          // L is +1, and R is -1
          xfldAreas[findexL].setAssociativity( itriL - ntriface[findexL] ).setEdgeSign( +1, segL );

          // edge-to-cell connectivity
          xfldBedge.setElementLeft( itriL - ntriface[findexL], nseg );
          xfldBedge.setCanonicalTraceLeft( CanonicalTraceToCell(segL, 1), nseg );
        }
        else
        {
          const int itriR = ptric[3*itriL+segL]-1;

          // Only count segments where the right triangle has a higher index
          if ( itriR < itriL ) continue;

          std::vector<int> triR = {ptris[3*itriR+0]-1, ptris[3*itriR+1]-1, ptris[3*itriR+2]-1};

          TraceConstructorType& xfldIedge = fldAssocIedge[edgeIndexMap[eIndex-1].first];

          // Find the face index associated with the L and R cell groups
          int findexL = 0, findexR = 0;
          while ( findexL <= (int)faces.size() && !(ntriface[findexL] <= itriL && itriL < ntriface[findexL+1]) ) findexL++;
          while ( findexR <= (int)faces.size() && !(ntriface[findexR] <= itriR && itriR < ntriface[findexR+1]) ) findexR++;

          // set the left and right group
          xfldIedge.setGroupLeft ( findexL );
          xfldIedge.setGroupRight( findexR );

          // Find the nodes for an edge on both triangles
          for (int segR = 0; segR < 3; segR++ )
          {
            if ( (n1 == triR[segnodes[segR][0]] &&
                  n2 == triR[segnodes[segR][1]]) ||
                 (n1 == triR[segnodes[segR][1]] &&
                  n2 == triR[segnodes[segR][0]]) )
            {
              xfldIedge.setAssociativity( nseg ).setRank( 0 );

              // grid-coordinate DOF association (edge-to-node connectivity)
              xfldIedge.setAssociativity( nseg ).setNodeGlobalMapping( {n1, n2} );

              // L is +1, and R is -1
              xfldAreas[findexL].setAssociativity( itriL - ntriface[findexL] ).setEdgeSign( +1, segL );
              xfldAreas[findexR].setAssociativity( itriR - ntriface[findexR] ).setEdgeSign( -1, segR );

              // edge-to-cell connectivity
              xfldIedge.setElementLeft ( itriL - ntriface[findexL], nseg );
              xfldIedge.setElementRight( itriR - ntriface[findexR], nseg );
              xfldIedge.setCanonicalTraceLeft ( CanonicalTraceToCell(segL,  1), nseg );
              xfldIedge.setCanonicalTraceRight( CanonicalTraceToCell(segR, -1), nseg );
            }
          }
        }
      }
    }
  }

  // Create trace groups for interior edges
  for ( std::size_t i = 0; i < fldAssocIedge.size(); i++ )
  {
    interiorTraceGroups_[i] = new FieldTraceGroupType<Line>( fldAssocIedge[i] );
    interiorTraceGroups_[i]->setDOF(DOF_, nDOF_);
  }

  // Create boundary trace groups
  for ( std::size_t i = 0; i < fldAssocBedge.size(); i++ )
  {
    boundaryTraceGroups_[i] = new FieldTraceGroupType<Line>( fldAssocBedge[i] );
    boundaryTraceGroups_[i]->setDOF(DOF_, nDOF_);
  }


  // create the element groups
  resizeCellGroups(faces.size());

  //int tri = 0;
  for (std::size_t fIndex = 0; fIndex < faces.size(); fIndex++ )
  {
    FieldTraceGroupType<Line>::FieldAssociativityConstructorType xfldIedge;

    tessFaces_.emplace_back( body, faces[fIndex], fIndex,
                             nDOF_, puvt, DOF_,
                             ptype, pindex,
                             ntriface, ptris, ptric,
                             nodeMap, xfldAreas, xfldIedge, tessEdges_ );

    interiorTraceGroups_[eIntIndex] = new FieldTraceGroupType<Line>(xfldIedge);
    interiorTraceGroups_[eIntIndex]->setDOF(DOF_, nDOF_);

    eIntIndex++;
   // tri += nftri;
  }

  // Lastly create all the cell groups
  for (int fIndex = 0; fIndex < cellGroups_.size(); fIndex++ )
  {
    cellGroups_[fIndex] = new FieldCellGroupType<Triangle>(xfldAreas[fIndex]);
    cellGroups_[fIndex]->setDOF(DOF_, nDOF_);
    nElem_ += xfldAreas[fIndex].nElem();
  }

  //Just to make sure there is no extra memory
  tessFaces_.shrink_to_fit();


  // Check that the grid is correct
  checkGrid();
}

//Explicitly instantiate the class
template class EGTessellation<2>;
//template class EGTessellation<3>;

}
}
