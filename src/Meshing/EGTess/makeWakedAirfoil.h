// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef MAKEWAKEDAIRFOIL_H
#define MAKEWAKEDAIRFOIL_H

#include "Meshing/EGADS/EGBody.h"
#include "Meshing/EGADS/EGModel.h"

namespace SANS
{
namespace EGADS
{

EGBody<2>
makeWakedAirfoil( const EGContext& context, const std::string& filename, const Real splinetol,
                  const int nHalfAirfoil, const int nWake, const int nFarField, const Real farfielddistance );

EGBody<2>
makeWakedAirfoil( const EGApproximate<2>& airfoil_spline, const int nHalfAirfoil, const int nWake, const int nFarField, const Real farfielddistance );

EGModel<3>
makeWakedAirfoil( const EGApproximate<3>& airfoil_spline,
                  std::vector<int>& TrefftzFrames,
                  Real span = 1,
                  const std::vector<double>& boxsize = {-2,-2,-2, 4,4,4},
                  const int nChord = 10,
                  const int nSpan = 10,
                  const int nWake = 10,
                  const std::vector<double>& outflowParams = {2, 0.001, 15},
                  bool withWake = true );
}
}

#endif //MAKEWAKEDAIRFOIL_H
