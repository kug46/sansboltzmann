// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php


#include "tools/stringify.h"

#include "WakedFarFieldBox.h"
#include "Field/XField3D_Wake.h"

#include "Meshing/EGADS/EGBody.h"
#include "Meshing/EGADS/EGIntersection.h"

#include "Meshing/EGADS/isSame.h"
#include "Meshing/EGADS/clone.h"
#include "Meshing/EGADS/solidBoolean.h"

#include "EGTessModel.h"
#include "intersectWake.h"

#include <vector>

namespace SANS
{
namespace EGADS
{

static const char* BOXFACE = "->BoxFace";

std::vector< EGBody<3> >
WakedFarFieldBox( const std::vector< EGBody<3> >& bodies,
                  const std::vector<double>& boxsize,
                  std::vector<int>& TrefftzFrames )
{
  SANS_ASSERT( bodies.size() > 0 );

  TrefftzFrames.clear();

  const EGContext& context = bodies[0].getContext();

  //-------------------------------------------------------------------------//

  // Sort bodies as solids, wakes, and sheets
  std::vector< EGBody<3> > solids;
  std::vector< EGBody<3> > sheets;
  std::vector< EGBody<3> > wakeslong;
  for (std::size_t ibody = 0; ibody < bodies.size(); ibody++)
  {
    if (bodies[ibody].isSolid())
      solids.emplace_back(bodies[ibody]);
    else if (bodies[ibody].isSheet() ) //)&& bodies[ibody].hasAttribute("Wake"))
      wakeslong.emplace_back(bodies[ibody]);
    //else if (bodies[ibody].isSheet())
    //  sheets.emplace_back(bodies[ibody]);
  }

  //-------------------------------------------------------------------------//

  int nKuttaEdge = 0;

  //Find all sheet body edges that also exist in solid bodies
  //Then mark those as kutta edges if the sheet is a wake sheet
  for ( std::size_t isheet = 0; isheet < wakeslong.size(); isheet++ )
  {
    if (!wakeslong[isheet].hasAttribute(XField3D_Wake::WAKESHEET)) continue;
    std::string wakeName;
    wakeslong[isheet].getAttribute(XField3D_Wake::WAKESHEET, wakeName);

    std::vector< EGEdge<3> > sheetEdges = wakeslong[isheet].getEdges();

    //Loop over the solid bodies for each sheet body
    for ( std::size_t isolid = 0; isolid < solids.size(); isolid++ )
    {
      std::vector< EGEdge<3> > solidEdges = solids[isolid].getEdges();

      //Loop over all edges in the sheet body and solid body
      for ( std::size_t isheetedge = 0; isheetedge < sheetEdges.size(); isheetedge++ )
      {
        for ( std::size_t isolidedge = 0; isolidedge < solidEdges.size(); isolidedge++ )
        {
          // Check if the sheet edge and solid edge are on the same geometry
          if (isSame(sheetEdges[isheetedge], solidEdges[isolidedge]))
          {
            // Get the nodes on the two edges
            std::vector< EGNode<3> > solidNodes = solidEdges[isolidedge].getNodes();
            std::vector< EGNode<3> > sheetNodes = sheetEdges[isheetedge].getNodes();

            // Check that the nodes match on the edges
            if (solidEdges[isolidedge].isOneNode())
            {
              if ( !isSame(solidNodes[0], sheetNodes[0]) ) continue;
            }
            else
            {
              if ( !( (isSame(solidNodes[0], sheetNodes[0]) && isSame(solidNodes[1], sheetNodes[1])) ||
                      (isSame(solidNodes[0], sheetNodes[1]) && isSame(solidNodes[1], sheetNodes[0])) ) ) continue;
            }

            // Mark the solid edge as a wake edge
            solidEdges[isolidedge].addAttribute(XField3D_Wake::WAKESHEET, wakeName);

            std::cout << "Marking Solid Edge " << isolidedge+1 << " : " << wakeName << std::endl;
            nKuttaEdge++;
          }
        }
      }
    }
  }

  //-------------------------------------------------------------------------//

  //Create the farfield box and mark all faces as farfield
  EGBody<3> box = context.makeSolidBody( BOX, boxsize );

  std::vector< EGFace<3> > boxfaces = box.getFaces();
  boxfaces[0].addAttribute("BCName", "Inflow");
  boxfaces[0].addAttribute(BOXFACE, 0);
  boxfaces[1].addAttribute("BCName", "Outflow");
  boxfaces[1].addAttribute(BOXFACE, 1);

  // TODO: Need to be a bit smarter about if triangle is needed
  boxfaces[1].addAttribute(EGTessModel::TRIANGLE, 33.); // Use triangle mesh generator on the outflow

  for ( std::size_t face = 2; face < boxfaces.size(); face++ )
  {
    //Set lateral faces to Lateral
    boxfaces[face].addAttribute("BCName", "Outflow");
    boxfaces[face].addAttribute(BOXFACE, (int)face);
  }

  // Default to two triangle on the shortest farfield
  std::vector<double> boxParams = { std::min(std::min(boxsize[3]/2,boxsize[4]/2),boxsize[5]/2), 0.1, 30. };
  for ( auto face = boxfaces.begin(); face != boxfaces.end(); face++ )
    face->addAttribute(".tParam", boxParams);

  // Must have at least one solid for now
  SANS_ASSERT( solids.size() > 0 );

  // Subtract all solids from the box
  EGBody<3> boxedsolids = subtract(box, solids[0]).getBodies()[0].clone();
  for (std::size_t isolid = 1; isolid < solids.size(); isolid++)
    boxedsolids = subtract(boxedsolids, solids[isolid]).getBodies()[0].clone();

  // All the faces in the box with solids subtracted
  std::vector< EGFace<3> > boxedsolidFaces = boxedsolids.getFaces();

  // Remove all the portion of the wakes extended beyond the box
  std::vector< EGBody<3> > wakes;
  if (wakeslong.size() > 0)
    wakes = clone( intersect( EGModel<3>(clone(wakeslong)), box ).getBodies() );
  //for ( const EGBody<3>& wakelong : wakeslong )
  //  wakes.emplace_back( intersectWake( box, wakelong ) );

  // The intersect wakes should be the same count as the long wakes
  SANS_ASSERT( wakes.size() == wakeslong.size() );

  int nTrefftzEdge = nKuttaEdge;

  // Scribe wake intersections onto the box
  for (std::size_t iwakelong = 0; iwakelong < wakeslong.size(); iwakelong++)
  {
    EGIntersection<3> box_wake_intersect(boxedsolids, wakeslong[iwakelong]);

    //box_wake_intersect.saveModel("wakeIntersection_" + stringify(iwakelong) + ".egads");
    try
    {
      // Scribe the box-wake intersection onto the box
      boxedsolids = EGBody<3>( box_wake_intersect );
    }
    catch ( const EGADSException& e )
    {
      // EAGDS generates a construction error when the scribe fails, which means there are only floating edges
      if ( e.status != EGADS_CONSTERR )
        throw;
    }

    // Any edge in the box-wake intersection not in the box is a floating edge
    std::vector< EGEdge<3> > boxEdges = boxedsolids.getEdges();
    std::string wakeName;
    wakeslong[iwakelong].getAttribute(XField3D_Wake::WAKESHEET, wakeName);

    for (std::size_t iwake = 0; iwake < wakes.size(); iwake++)
    {
      std::vector< EGEdge<3> > wakeEdges = wakes[iwake].getEdges();

      for (int iIntesectEdge = 0; iIntesectEdge < box_wake_intersect.nEdge(); iIntesectEdge++)
      {
        EGEdge<3> intesectEdge = box_wake_intersect.edge(iIntesectEdge);

        // Look for the intersected edge on the box
        bool found = false;
        for (std::size_t iBoxEdge = 0; iBoxEdge < boxEdges.size(); iBoxEdge++)
          if ( isSame(intesectEdge, boxEdges[iBoxEdge]) )
          {
            found = true;
            break;
          }
        // If the edge was found, then this is not a floating edge
        if (found) continue;

        EGFace<3> boxFace = box_wake_intersect.face(iIntesectEdge);

        for (std::size_t iWakeEdge = 0; iWakeEdge < wakeEdges.size(); iWakeEdge++)
          if ( isSame(intesectEdge, wakeEdges[iWakeEdge]) )
          {
            // Mark the edge that is used for Trefftz plane integrals
            // TODO: This assumes the edge is only on the outflow plane
            wakeEdges[iWakeEdge].addAttribute(XField3D_Wake::TREFFTZ, wakeName);

            if ( !wakes[iwake].hasAttribute(XField3D_Wake::WAKESHEET) )
            {
              std::cout << "Wake " << iwake << " " << wakeName << std::endl;
              wakes[iwake].addAttribute(XField3D_Wake::WAKESHEET, wakeName);
            }

            // The first index is the body with the face in the model.
            // The second index is the face index in the body.
            // By construction, the boxedsolid is placed first in the model below
            wakeEdges[iWakeEdge].addAttribute(EGTessModel_FLOATINGEDGE, {1, boxedsolids.getBodyIndex( boxFace )});

            std::cout << "Wake Trefftz edge  :" << iWakeEdge << " " << iwake << std::endl;
            std::cout << "Solid face :" << boxedsolids.getBodyIndex( boxFace ) << std::endl;

            // Save off the counts to the trefftz frames in the grid
            TrefftzFrames.push_back(nTrefftzEdge);
            nTrefftzEdge++;
            break;
#if 0
            // Find the original box face in the box with solids subtracted and mark the edge as floating
            for (std::size_t iface = 0; iface < boxedsolidFaces.size(); iface++)
            {
              if (boxedsolidFaces[iface].hasAttribute(BOXFACE))
              {
                int boxedFace = -1;
                boxedsolidFaces[iface].getAttribute(BOXFACE, boxedFace);
                if ( boxedFace == boxFace )
                {
                  // The first index is the body with the face in the model.
                  // The second index is the face index in the body.
                  // By construction, the boxedsolid is placed first in the model below
                  wakeEdges[iWakeEdge].addAttribute(EGTessModel_FLOATINGEDGE, {1, (int)iface+1});
                  break;
                }
              }
            }
            break;
#endif
          }
      }
    }
  }

  // Insert the boxed solids to be the beginning of the list
  // The floating edge body index above assumes the boxed solids are first in the model
  wakes.insert(wakes.begin(), boxedsolids);

  // Return a model of the boxed solids and wakes
  //EGModel<3> model(wakes);

  //model.save("gliderBox.egads");

  return wakes;
}

}
}
