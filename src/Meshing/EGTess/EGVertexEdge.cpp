// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "EGVertexEdge.h"

namespace SANS
{
namespace EGADS
{

template<int Dim>
EGVertexEdge<Dim>::EGVertexEdge( const EGEdge<Dim>& edge, const Real t, CartCoord* X, const bool fixed )
  : EGVertex<Dim>(X, fixed), edge_(&edge), t_(t)
{

}

template<int Dim_>
EGVertexEdge<Dim_>&
EGVertexEdge<Dim_>::operator=( const EGVertexEdge& ve )
{
  if ( this != &ve )
  {
    EGVertex<Dim>::operator=(ve);
    edge_ = ve.edge_;
    t_ = ve.t_;
  }
  return *this;
}


template<int Dim_>
void
EGVertexEdge<Dim_>::update( const Real t)
{
  t_ = t;
  *EGVertex<Dim>::X_ = (*edge_)(t);
}

template<int Dim_>
EGVertexEdge<Dim_>::~EGVertexEdge()
{
}

//Explicitly instantiate the class
template class EGVertexEdge<2>;
template class EGVertexEdge<3>;

}
}
