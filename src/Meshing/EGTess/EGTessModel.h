// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EG_TESSMODEL_H
#define EG_TESSMODEL_H

#include <array>
#include <map>

#include "tools/noncopyable.h"

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "EGTessBody.h"

#include "Meshing/EGADS/EGModel.h"
#ifdef SANS_AFLR
#include "AFLR4.h"
#endif
#include "Topology/ElementTopology.h"

#include "Field/Element/UniqueElem.h"

//----------------------------------------------------------------------------//
// EGTessModel
//
// Represents a complete EGADS tessellation of a model consisting of both
// SOLIDBIDY and SHEETBODY
//----------------------------------------------------------------------------//


namespace SANS
{

namespace EGADS
{

//---------------------------------------------------------------------------//
struct EGADSTessParams : noncopyable
{
  const ParameterNumericList<Real> Tess_Params{"Tess_Params", {0.025, 0.001, 15.0}, 0, NO_LIMIT, "[Maximum edge length, Max sag, Max angle]"};

  static void checkInputs(PyDict d);
  static EGADSTessParams params;
};

//---------------------------------------------------------------------------//
struct EGTessModelParams : noncopyable
{
  struct SurfaceMesherOptions
  {
    typedef DictKeyPair ExtractType;
    const ParameterString Name{"Name", "EGADS", "Surface Mesh Generator Name"};
    const ParameterString& key = Name;

    const DictOption EGADS{"EGADS", EGADSTessParams::checkInputs};
#ifdef SANS_AFLR
    const DictOption AFLR4{"AFLR4", AFLR4Params::checkInputs};
#endif

    const std::vector<DictOption> options{EGADS
#ifdef SANS_AFLR
                                         , AFLR4
#endif
                                         };
  };
  const ParameterOption<SurfaceMesherOptions> SurfaceMesher{"SurfaceMesher", "EGADS", "Surface mesh generator"};

  static EGTessModelParams params;
};

//---------------------------------------------------------------------------//
class EGTessModel
{
public:
  EGTessModel(const EGModel<3>& model, const std::vector<double>& tessparams = {0.025,  // Maximum segment length
                                                                                0.001,  // Maximum curvature
                                                                                15.0}); // Maximum dihedral angle between triangle facets
  EGTessModel(const std::vector<EGBody<3>>& bodies, const std::vector<double>& tessparams = {0.025,  // Maximum segment length
                                                                                             0.001,  // Maximum curvature
                                                                                             15.0}); // Maximum dihedral angle between triangle facets
  EGTessModel(const EGModel<3>& model, const PyDict& dict );

  EGTessModel(const std::vector<EGBody<3>>& bodies, const PyDict& dict);

  ~EGTessModel();

protected:
  void init(const PyDict& dict);

public:
  int nFaces() const;

  const std::vector< EGBody<3> > getBodies() const { return bodies_; }

  std::vector< EGNode<3> > getAllNodes() const;
  std::vector< EGEdge<3> > getAllEdges() const;
  std::vector< EGFace<3> > getAllFaces() const;

  std::vector< EGNode<3> > getUniqueNodes() const;
  std::vector< EGEdge<3> > getUniqueEdges() const;
  std::vector< EGFace<3> > getUniqueFaces() const;

  // Maps 'All' Face/Edge/Node egos to the 'Unique' egos
  std::map<ego,ego> alltoUniqueMap() const;

  // Returns a map of all edges that belong to each face (including floating edges)
  std::map<ego,std::vector<ego>> getFaceChildren() const;

  // Returns a map of all nodes that belong to each edge
  std::map<ego,std::vector<ego>> getEdgeChildren() const;

  const std::vector< int >& modelSolidBodyIndex() const { return modelBodyIndex_[SOLIDINDEX]; }
  const std::vector< int >& modelSheetBodyIndex() const { return modelBodyIndex_[SHEETINDEX]; }

  const std::vector< std::vector< int > >& modelSolidFaceIndex() const { return modelFaceIndex_[SOLIDINDEX]; }
  const std::vector< std::vector< int > >& modelSheetFaceIndex() const { return modelFaceIndex_[SHEETINDEX]; }

  const std::vector< std::vector< int > >& modelSolidEdgeIndex() const { return modelEdgeIndex_[SOLIDINDEX]; }
  const std::vector< std::vector< int > >& modelSheetEdgeIndex() const { return modelEdgeIndex_[SHEETINDEX]; }

  const std::vector< std::vector< int > >& modelSolidNodeIndex() const { return modelNodeIndex_[SOLIDINDEX]; }
  const std::vector< std::vector< int > >& modelSheetNodeIndex() const { return modelNodeIndex_[SHEETINDEX]; }

  const std::vector< int >* modelBodyIndex() const { return modelBodyIndex_; }

  const std::vector< std::vector< int > >* modelFaceIndex() const { return modelFaceIndex_; }
  const std::vector< std::vector< int > >* modelEdgeIndex() const { return modelEdgeIndex_; }
  const std::vector< std::vector< int > >* modelNodeIndex() const { return modelNodeIndex_; }

  bool isEdgeSegment(const std::array<int,3>& tri, const int seg) const;
  int getEdgeIndexFromSegment(const std::array<int,3>& tri, const int seg) const;

  // Returns the topological object associated with a given tessellation point
  EGTopologyBase modelTopoFromVertex(const std::size_t i) const;
  bool isEdgeFromVertex(const std::size_t i) const;

  int modelTopoType(const std::size_t i) const { return modelTopoType_[i]; }
  int modelTopoIndex(const std::size_t i) const { return modelTopoIndex_[i]; }
  const std::vector< std::array<double,3> >& xyzs() const { return xyzs_; }
  const std::vector< std::array<double,2> >& uvt() const { return uvt_; }

  // tris - Nodes that define a triangle
  // tric - Triangle element index opposite to the triangle node
  int ntri() const { return ntri_; }
  const std::vector< std::vector< std::vector< std::array<int,3> > > >& tris() const { return tris_; }
  const std::vector< std::vector< std::vector< std::array<int,3> > > >& tric() const { return tric_; }

  ego getTessEgo(const ego body) const;

  // elemMap[elemUnique] = itri on a face
  std::map<UniqueElem,int> getUniqueElemMap() const;
  std::map<UniqueElem,std::array<int,3>> getUniqueElemTraceNodeMap() const;

  Real getSize() const;

  // dumps the surface mesh on the egads bodies to a tecplot file
  void outputTecplot(const std::string& filename) const;

  // Solid index should always be first here
  enum { SOLIDINDEX, SHEETINDEX, NBODYTOPO };
  static const char* TRIANGLE;
#define EGTessModel_FLOATINGEDGE "->Floating"

protected:
  template<class EGTopo>
  std::vector< EGTopo > getAllTopos(const int TOPO) const;
  template<class EGTopo>
  std::vector< EGTopo > getTopos(const std::vector< std::vector<int> > topoIndexMap[], const int TOPO) const;
  template<class EGTopo>
  void addMapEgos(const std::vector< std::vector<int> > topoIndexMap[], const int TOPO, std::map<ego,ego>& egomap) const;

  void modelIndexMap(const std::vector< EGBody<3> >& bodies, const std::vector< int >& bodymap,
                     int& modelIndex, std::vector< std::vector<int> >& topoIndexMap, const int TOPO);
  void initModelIndexes(const std::vector< EGBody<3> >& bodies);
  void mapSameEdges(const std::vector< EGBody<3> >& bodies, const std::vector<ego>& tess );

  // Use Shewchuk's Triangle mesh generator on the face
  void TriangleFaces(const std::vector< EGBody<3> >& bodies, std::vector<ego>& tess);

#ifdef SANS_AFLR
#define AFLR_FARFIELD "AFLRfarfield"

  // Use AFLR4 mesh generator on the face
  void AFLR4Faces(const PyDict& dict, const std::vector< EGBody<3> >& bodies, std::vector<ego>& tess);
#endif

public:
  // Custom data type to connect floating edges between different bodies
  // needs to be public for AFLR
  struct FloatingEdge
  {
    int bodyIndex;
    int edgeIndex;
  };
  typedef std::vector< std::vector< std::vector<FloatingEdge> > > FloatingEdgeMap; // [ibody][iface][floating]

  void fillFloatingEdgeMap( FloatingEdgeMap& floatingEdges, std::vector< std::pair<int,int> >& modelBodyInvIndex ) const;

protected:
  void faceEdgeSegments(const int ibody, const int iface,
                        const std::vector< EGBody<3> >& bodies,
                        std::vector<ego>& tess,
                        const std::vector< std::pair<int,int> >& modelBodyInvIndex,
                        const FloatingEdgeMap& floatingEdges,
                        std::vector< std::array<Real,3> >& xyz,
                        std::vector< std::array<Real,2> >& uv,
                        std::vector< std::array<Real,2> >& uvt,
                        std::vector< std::array<int,2> >& segments,
                        std::vector< int >& index,
                        std::vector< int >& type);

  const std::vector<EGBody<3>> bodies_;
  std::vector<ego> tess_;

  std::vector< int > modelBodyIndex_[NBODYTOPO];                 // Body index in the context of the model
  std::vector< std::vector< int > > modelFaceIndex_[NBODYTOPO];  // Face index in the context of the model (0-based indexing)
  std::vector< std::vector< int > > modelEdgeIndex_[NBODYTOPO];  // Edge index in the context of the model (0-based indexing)
  std::vector< std::vector< int > > modelNodeIndex_[NBODYTOPO];  // Node index in the context of the model (0-based indexing)

  std::vector< std::array<int,2> > edgeNodesIndex_; // Stores the index's of the two nodes that define an edge.
  std::map< std::pair<int,int>, int > nodesEdgeIndex_; // Given two nodes, returns the model edge index

  std::vector<int> modelTopoType_;
  std::vector<int> modelTopoIndex_; //(1-based indexing)
  std::vector< std::array<double,3> > xyzs_;
  std::vector< std::array<double,2> > uvt_;

  // Stored references to the model nodes, edges, and faces
  std::vector< EGNode<3> > modelNodes_;
  std::vector< EGEdge<3> > modelEdges_;
  std::vector< EGFace<3> > modelFaces_;

  // tris - Nodes that define a triangle
  // tric - Triangle element index opposite to the triangle node
  int ntri_;
  std::vector< std::vector< std::vector< std::array<int,3> > > > tris_; // [ibody][iface][ielem][inode]
  std::vector< std::vector< std::vector< std::array<int,3> > > > tric_; // [ibody][iface][ielem][inode]
};


} //namespace EGADS
} //namespace SANS


#endif //EG_TESSMODEL_H
