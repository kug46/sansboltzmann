// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "EGTessFace.h"

#include "BasisFunction/BasisFunctionArea_Triangle.h"

namespace SANS
{
namespace EGADS
{

template<int Dim_>
EGTessFace<Dim_>::EGTessFace(const EGTessFace& tf)
  : face_(tf.face_), ntri_(tf.ntri_), nFreeVertex_(tf.nFreeVertex_), tessEdges_(tf.tessEdges_)
{
  const int size = tf.Vertexes_.size();
  for (int i = 0; i < size; i++)
    Vertexes_.emplace_back( face_, tf.Vertexes_[i].getParam(), tf.Vertexes_[i].getVertex_ptr(), tf.Vertexes_[i].fixed() );
}

template<int Dim_>
EGTessFace<Dim_>::EGTessFace(const EGBody<Dim>& body, const EGFace<Dim>& face, const int fIndexL,
                             const int nVertex, const double *puv,
                             CartCoord* X,
                             const int *ptype, const int *pindex,
                             const int *ntriface, const int *ptris, const int *ptric,
                             std::vector<int>& nodeMap,
                             std::vector< XField<PhysD2, TopoD2>::FieldCellGroupType<Triangle>::FieldAssociativityConstructorType >& xfldArea,
                             XField<PhysD2, TopoD2>::FieldTraceGroupType<Line>::FieldAssociativityConstructorType& xfldIedge,
                             std::vector< EGTessEdge<Dim> >& tessEdges)
  : face_( face ), ntri_(ntriface[1]-ntriface[0]), nFreeVertex_(0)
{

  //Add the interior nodes that can move on a face
  nFreeVertex_ = 0;
  for (int i = 0; i < nVertex; i++)
    if (pindex[i] == -(fIndexL+1))
    {
      Vertexes_.emplace_back( face_, ParamCoord({puv[i*2+0],puv[i*2+1]}), &X[i], false );

      nFreeVertex_++;
    }

  // Create all the node associativities for triangles
  for (int j = ntriface[fIndexL]; j < ntriface[fIndexL+1]; j++)
  {
    std::vector<int> tri = {ptris[3*j+0]-1, ptris[3*j+1]-1, ptris[3*j+2]-1};
    xfldArea[fIndexL].setAssociativity( j-ntriface[fIndexL] ).setRank( 0 );
    xfldArea[fIndexL].setAssociativity( j-ntriface[fIndexL] ).setNodeGlobalMapping( tri );
  }

  //Indexes to form the sides of a triangle
  const int segnodes[3][2] = {{1,2}, {2,0}, {0,1}};

  //Compute the number of segments in the tessellation that reside on the face
  int nseg = 0;
  for (int itriL = ntriface[fIndexL]; itriL < ntriface[fIndexL+1]; itriL++)
  {
    for (int segL = 0; segL < 3; segL++)
    {
      const int itriR = ptric[3*itriL+segL]-1;

      // Only count segments where the right triangle has a higher index
      if ( itriR < itriL ) continue;

      int fIndexR = 0;
      while ( fIndexR <= (int)xfldArea.size() && !(ntriface[fIndexR] <= itriR && itriR < ntriface[fIndexR+1]) ) fIndexR++;

      if ( fIndexR != fIndexL ) continue;

      nseg++;
    }
  }

  xfldIedge.resize( BasisFunctionLineBase::HierarchicalP1, nseg );

  // set the left/right group, which is always the current face
  xfldIedge.setGroupLeft( fIndexL );
  xfldIedge.setGroupRight( fIndexL );

  nseg = 0;
  for (int itriL = ntriface[fIndexL]; itriL < ntriface[fIndexL+1]; itriL++)
  {
    for (int segL = 0; segL < 3; segL++)
    {
      const int itriR = ptric[3*itriL+segL]-1;

      // Only count segments where the right triangle has a higher index
      if ( itriR < itriL ) continue;

      std::vector<int> triL = {ptris[3*itriL+0]-1, ptris[3*itriL+1]-1, ptris[3*itriL+2]-1};
      std::vector<int> triR = {ptris[3*itriR+0]-1, ptris[3*itriR+1]-1, ptris[3*itriR+2]-1};

      int fIndexR = 0;
      while ( fIndexR <= (int)xfldArea.size() && !(ntriface[fIndexR] <= itriR && itriR < ntriface[fIndexR+1]) ) fIndexR++;

      if ( fIndexR != fIndexL ) continue;

      int n1 = triL[segnodes[segL][0]];
      int n2 = triL[segnodes[segL][1]];

      // Find the nodes for an edge on both triangles
      for (int segR = 0; segR < 3; segR++ )
      {
        if ( (n1 == triR[segnodes[segR][0]] &&
              n2 == triR[segnodes[segR][1]]) ||
             (n1 == triR[segnodes[segR][1]] &&
              n2 == triR[segnodes[segR][0]]) )
        {
          xfldIedge.setElementLeft ( itriL - ntriface[fIndexL], nseg );
          xfldIedge.setElementRight( itriR - ntriface[fIndexL], nseg );

          xfldIedge.setCanonicalTraceLeft ( CanonicalTraceToCell(segL,  1), nseg );
          xfldIedge.setCanonicalTraceRight( CanonicalTraceToCell(segR, -1), nseg );

          // L is +1, and R is -1
          xfldArea[fIndexL].setAssociativity( itriL - ntriface[fIndexL] ).setEdgeSign( +1, segL );
          xfldArea[fIndexL].setAssociativity( itriR - ntriface[fIndexL] ).setEdgeSign( -1, segR );

          xfldIedge.setAssociativity( nseg ).setRank( 0 );
          xfldIedge.setAssociativity( nseg ).setNodeGlobalMapping( {n1, n2} );

          nseg++;
        }
      }
    }
  }

  //Add all the remaining vertexes in the nodes and edges
  for (int i = 0; i < nVertex; i++)
  {
    if ( ptype[i] == 0 )
    {
      //std::cout << "node " << pindex[i]-1 << " = " << Xnode[pindex[i]-1] << std::endl;
      Vertexes_.emplace_back( face_, ParamCoord({puv[2*i+0],puv[2*i+1]}), &X[nodeMap[pindex[i]-1]], true );
    }
    else if ( ptype[i] > 0 )
    {
      //std::cout << "edge " << pindex[i]-1 << ", " << ptype[i]-1 << " = " << (CartCoord)tessEdges[pindex[i]-1].getVertex(ptype[i]-1) << std::endl;
      Vertexes_.emplace_back( face_, ParamCoord({puv[2*i+0],puv[2*i+1]}), tessEdges[pindex[i]-1].getVertex(ptype[i]-1).getVertex_ptr(), true );
    }
  }

  //Just to make sure there is no extra memory
  Vertexes_.shrink_to_fit();

  std::vector< EGLoop<Dim> > loops = face.getLoops();

  for ( auto loop = loops.begin(); loop != loops.end(); loop++ )
  {
    //Get the edges of the loop
    std::vector< EGEdge<Dim> > edges = loop->getEdges();

    //Loop over the edges of the face and save off pointers to the tessEdges
    for ( auto edge = edges.begin(); edge != edges.end(); edge++ )
      tessEdges_.push_back( &tessEdges[body.getBodyIndex(*edge)-1] );
  }

}

template<int Dim_>
EGTessFace<Dim_>& // cppcheck-suppress operatorEqVarError
EGTessFace<Dim_>::operator=(const EGTessFace& tf)
{
  if ( this != &tf )
  {
    face_ = tf.face_;
    nFreeVertex_ = tf.nFreeVertex_;
    tessEdges_ = tf.tessEdges_;
    ntri_ = tf.ntri_;
  }
  return *this;
}

template<int Dim_>
void
EGTessFace<Dim_>::evaluate(const int n, const double* puv)
{
  SANS_ASSERT( n == nFreeParameters() );

  //Only the interior vertexes can move
  for ( int i = 0; i < n/2; i++ )
    Vertexes_[i].update( puv[2*i], puv[2*i+1] );
}

template<int Dim_>
void
EGTessFace<Dim_>::fillFreeParameters(std::vector<double>& x) const
{
  x.resize( nFreeParameters() );

  const int nFree = nFreeVertex();

  //Only get interior vertexes
  for ( int i = 0; i < nFree; i++ )
  {
    ParamCoord uv = Vertexes_[i].getParam();
    x[2*i  ] = uv[0];
    x[2*i+1] = uv[1];
  }
}

template<int Dim_>
std::vector<double>
EGTessFace<Dim_>::lowerParamBound() const
{
  std::vector<double> lower(nFreeVertex()*2);

  double lower_u = face_.getURange()[0];
  double lower_v = face_.getVRange()[0];

  for ( int i = 0; i < nFreeVertex(); i++ )
  {
    lower[2*i+0] = lower_u;
    lower[2*i+1] = lower_v;
  }

  return lower;
}

template<int Dim_>
std::vector<double>
EGTessFace<Dim_>::upperParamBound() const
{
  std::vector<double> upper(nFreeVertex()*2);

  double upper_u = face_.getURange()[1];
  double upper_v = face_.getVRange()[1];

  for ( int i = 0; i < nFreeVertex(); i++ )
  {
    upper[2*i+0] = upper_u;
    upper[2*i+1] = upper_v;
  }

  return upper;
}


//Explicitly instantiate the class
template class EGTessFace<2>;
//template class EGTessFace<3>;

}
}
