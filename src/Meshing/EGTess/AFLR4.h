// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SANS_AFLR4_H
#define SANS_AFLR4_H

#include "tools/noncopyable.h"

#include "Python/Parameter.h"

namespace SANS
{

struct AFLR4Params : noncopyable
{
  const ParameterNumeric<int> min_ncell{"min_ncell", -1, 0, 0, "Minimum number of cells between two components/bodies."};
  const ParameterNumeric<int> mer_all{"mer_all", -1, 0, 0, "Global edge mesh spacing scale factor flag."};

  const ParameterNumeric<double> ff_cdfr{"ff_cdfr", -1, 1, NO_LIMIT, "Farfield growth rate for field point spacing."};
  const ParameterNumeric<double> abs_min_scale{"abs_min_scale", -1, 0, 0, "Relative scale of absolute minimum spacing to reference length."};
  const ParameterNumeric<double> BL_thickness{"BL_thickness", -1, 0, 0, "Boundary layer thickness for proximity checking."};
  const ParameterNumeric<double> curv_factor{"curv_factor", -1, 0, 0, "Curvature factor."};
  const ParameterNumeric<double> length_ratio{"length_ratio", -1, 0, 0, "Curvature length ratio threshold."};
  const ParameterNumeric<double> erw_all{"erw_all", -1, 0, 0, "Global edge mesh spacing scale factor weight."};
  const ParameterNumeric<double> max_scale{"max_scale", -1, 0, 0, "Relative scale of maximum spacing to reference length."};
  const ParameterNumeric<double> min_scale{"min_scale", -1, 0, 0, "Relative scale of minimum spacing to reference length."};
  const ParameterNumeric<double> ref_len{"ref_len", NO_DEFAULT, 0, NO_LIMIT, "Reference length for components/bodies in grid units."};

  const ParameterNumeric<double> cdfr{"cdfr", -1, 0, 0, "Maximum geometric growth rate."};

  const ParameterNumeric<int> mdf{"mdf", 2, 1, 2, "Distribution function flag.\n"
                                                  "If mdf = 1 then interpolate the node distribution function for new nodes "
                                                  "from the containing element.\n"
                                                  "If mdf = 2 then use geometric growth from the boundaries to determine the "
                                                  "distribution function for new nodes."
                                 };

  AFLR4Params();
  static void checkInputs(PyDict d);
  static AFLR4Params params;
};

}

#endif
