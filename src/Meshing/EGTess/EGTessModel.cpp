// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "EGTessModel.h"
#include "Meshing/EGADS/isSame.h"
#include "Meshing/EGADS/isEquivalent.h"

#include "tools/stringify.h"

#include "BasisFunction/TraceToCellRefCoord.h"

#define PYDICT_INSTANTIATE
#include "Python/PyDict_impl.h"

#include <set>
#include <vector>
#include <algorithm> //std::sort
#include <iomanip> //std::setprecision
#include <fstream>

#include <egads.h>

#define VOID void
#define ANSI_DECLARATORS
#define TRILIBRARY
#define REAL double
extern "C"
{
#include <triangle.h>
}

namespace SANS
{

PYDICT_PARAMETER_OPTION_INSTANTIATE(EGADS::EGTessModelParams::SurfaceMesherOptions)

namespace EGADS
{

//---------------------------------------------------------------------------//
void
EGADSTessParams::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Tess_Params));
  d.checkUnknownInputs(allParams);
}
EGADSTessParams EGADSTessParams::params;

EGTessModelParams EGTessModelParams::params;

//---------------------------------------------------------------------------//
const char* EGTessModel::TRIANGLE = "->Triangle";

EGTessModel::EGTessModel(const EGModel<3>& model, const std::vector<double>& tessparams) : bodies_(model.getBodies())
{
  PyDict EGADSdict;
  EGADSdict[EGTessModelParams::params.SurfaceMesher.Name] = EGTessModelParams::params.SurfaceMesher.EGADS;
  EGADSdict[EGADSTessParams::params.Tess_Params] = tessparams;

  PyDict dict;
  dict[EGTessModelParams::params.SurfaceMesher] = EGADSdict;

  init(dict);
}


EGTessModel::EGTessModel(const std::vector<EGBody<3>>& bodies, const std::vector<double>& tessparams) : bodies_(bodies)
{
  PyDict EGADSdict;
  EGADSdict[EGTessModelParams::params.SurfaceMesher.Name] = EGTessModelParams::params.SurfaceMesher.EGADS;
  EGADSdict[EGADSTessParams::params.Tess_Params] = tessparams;

  PyDict dict;
  dict[EGTessModelParams::params.SurfaceMesher] = EGADSdict;

  init(dict);
}

EGTessModel::EGTessModel(const EGModel<3>& model, const PyDict& dict) : bodies_(model.getBodies())
{
  init(dict);
}

EGTessModel::EGTessModel(const std::vector<EGBody<3>>& bodies, const PyDict& dict) : bodies_(bodies)
{
  init(dict);
}

void
EGTessModel::init(const PyDict& dict)
{
  const int nbody = (int)bodies_.size();

  // Initialize the model index maps
  initModelIndexes(bodies_);

  // All the egads body tessellations
  tess_.resize(nbody, NULL);
  std::vector<EGTessBody> tessBodies;

  DictKeyPair SurfaceMesher = dict.get(EGTessModelParams::params.SurfaceMesher);

  //=================================
  if ( SurfaceMesher == EGTessModelParams::params.SurfaceMesher.EGADS )
  {
    PyDict EGADStess = SurfaceMesher;

    std::vector<double> tessparams = EGADStess.get(EGADSTessParams::params.Tess_Params);

    double size = getSize();

    // Scale the egads parameters relative to the model size
    tessparams[0] *= size;
    tessparams[1] *= size;

    // First tessellate all edges and unify the tessellation between solid and sheet bodies

    // Negating the first parameter triggers EGADS to only put vertexes on edges
    double edgeparams[3] = {-tessparams[0], tessparams[1], tessparams[2]};

    // Tessellate all the edges
    for ( std::size_t ibody = 0; ibody < bodies_.size(); ibody++ )
      EG_STATUS( EG_makeTessBody(bodies_[ibody], edgeparams, &tess_[ibody]) );

    // Transfer edge tessellation between solid bodies to coincident sheet body edges and find global edge/node indexes
    mapSameEdges(bodies_, tess_);

    //Release all the edge tessellation memory
    for (ego& t : tess_)
    {
      EG_STATUS( EG_deleteObject(t) );
      t = NULL;
    }

    // Generate the tessellation for each body
    for ( std::size_t ibody = 0; ibody < bodies_.size(); ibody++ )
    {
      //std::cout << "Making Tess Body" << std::endl;
      EG_STATUS( EG_makeTessBody(bodies_[ibody], tessparams.data(), &tess_[ibody]) );
      //std::cout << "Tess Body done" << std::endl;
#if 0
      ego tess = tess_[ibody];
      /* disable regularization in EGADS */
      EG_STATUS( EG_attributeAdd(tess, ".qRegular", ATTRSTRING, 3, NULL, NULL, "Off") );

      if (EG_quadTess(tess, &tess_[ibody]) != EGADS_SUCCESS)
      {
        tess_[ibody] = tess;
        continue;
      }
      EG_deleteObject(tess);
#endif
    }
  }
#ifdef SANS_AFLR
  else if ( SurfaceMesher == EGTessModelParams::params.SurfaceMesher.AFLR4 )
  {
    mapSameEdges( bodies_, tess_ );
    AFLR4Faces(SurfaceMesher, bodies_, tess_);
  }
#endif
  else
    SANS_DEVELOPER_EXCEPTION( "Unknown surface mesh generator" );

//  if ( mesher == EGADS )
     // Replace requested faces with a 'Triangle' tessellation
    TriangleFaces(bodies_, tess_);

  // tris - Nodes that define a triangle
  // tric - Triangle element index opposite to the triangle node
  tris_.resize( bodies_.size() ); // [ibody][iface][ielem][inode]
  tric_.resize( bodies_.size() ); // [ibody][iface][ielem][inode]

  // Total number of points in the tessellation
  int npts = 0;

  for ( std::size_t ibody = 0; ibody < bodies_.size(); ibody++ )
  {
    tessBodies.emplace_back(tess_[ibody]);

    npts += tessBodies.back().nvert;

    // We actually need to hang on to the EGADS tess objects
    //EG_STATUS( EG_deleteObject(tess_[ibody]) );
    //tess_[ibody] = NULL;
  }


  modelTopoType_.resize(npts);
  modelTopoIndex_.resize(npts);
  xyzs_.resize(npts, {{1e20,1e20,1e20}});
  uvt_.resize(npts, {{1e20,1e20}});

  npts = 0;

  // build up a table of edge indexes so the nodes and triangles can be zippered
  for ( int ibodyTopo = 0; ibodyTopo < NBODYTOPO; ibodyTopo++ )
  {
    for ( std::size_t ibody = 0; ibody < modelBodyIndex_[ibodyTopo].size(); ibody++ )
    {
      const int bodyIndex = modelBodyIndex_[ibodyTopo][ibody];

      const EGTessBody& tessBody = tessBodies[bodyIndex];

      for (int i = 0; i < tessBody.nvert; i++)
      {
        modelTopoType_[npts] = tessBody.ptype[i];

        int index = tessBody.pindex[i];

        if ( index < 0 )
        {
          // A negative index means it was already set as a model index
          modelTopoIndex_[npts] = -index;
        }
        else
        {
          //Use model global indexing to build up the table
          if ( tessBody.ptype[i] == 0 ) // Node
            modelTopoIndex_[npts] = modelNodeIndex_[ibodyTopo][ibody][index-1]+1;
          else if ( tessBody.ptype[i] > 0 ) // Edge
            modelTopoIndex_[npts] = modelEdgeIndex_[ibodyTopo][ibody][index-1]+1;
          else // Face
            modelTopoIndex_[npts] = modelFaceIndex_[ibodyTopo][ibody][index-1]+1;
        }

        xyzs_[npts][0] = tessBody.pxyz[3*i+0];
        xyzs_[npts][1] = tessBody.pxyz[3*i+1];
        xyzs_[npts][2] = tessBody.pxyz[3*i+2];

        uvt_[npts][0] = tessBody.puvt[2*i+0];
        uvt_[npts][1] = tessBody.puvt[2*i+1];

        // for non-face points -- try to match with others
        if (tessBody.ptype[i] >= 0)
        {
          for (int k = 0; k < npts; k++)
            if ((modelTopoType_[k] == modelTopoType_[npts]) && (modelTopoIndex_[k] == modelTopoIndex_[npts]))
            {
              // Change the index to 0 to make as a duplicate,
              // and set type to indicate the duplicate point number
              modelTopoType_[npts] = k;
              modelTopoIndex_[npts] = 0;
              break;
            }
        }
        npts++;
      }

    }
  }

  int base = 0;
  ntri_ = 0;
  // A table to make all points still used by the triangulation
  std::vector<int> table(npts, 0);

  // Zipper up the triangulation (except for the neighbor information across edges)
  for ( int ibodyTopo = 0; ibodyTopo < NBODYTOPO; ibodyTopo++ )
  {
    for ( std::size_t ibody = 0; ibody < modelBodyIndex_[ibodyTopo].size(); ibody++ )
    {
      const int bodyIndex = modelBodyIndex_[ibodyTopo][ibody];
      const int nface = bodies_[bodyIndex].nFaces();
      tris_[bodyIndex].resize(nface);
      tric_[bodyIndex].resize(nface);

      const EGTessBody& tessBody = tessBodies[bodyIndex];

      ntri_ += tessBody.ntri;

      for (int iface = 0; iface < nface; iface++)
      {
        int ntri = tessBody.ntriface[iface+1] - tessBody.ntriface[iface];
        tris_[bodyIndex][iface].resize(ntri);
        tric_[bodyIndex][iface].resize(ntri);
        for (int itri = tessBody.ntriface[iface]; itri < tessBody.ntriface[iface+1]; itri++)
        {
          int ielem = itri - tessBody.ntriface[iface];
          // Update the point numbering to the single unique list of points
          for (int n = 0; n < 3; n++)
          {
            if ( tessBody.ptric[3*itri+n] < 0 )
              tric_[bodyIndex][iface][ielem][n] = modelEdgeIndex_[ibodyTopo][ibody][-tessBody.ptric[3*itri+n]-1]+1; //Grab the model edge index
            else
              tric_[bodyIndex][iface][ielem][n] = tessBody.ptric[3*itri+n] - 1; // Change to 0-based indexing

            int k = tessBody.ptris[3*itri+n] - 1 + base; // -1 to remove the 1-based indexing
            if (modelTopoIndex_[k] == 0)
              tris_[bodyIndex][iface][ielem][n] = modelTopoType_[k];
            else
              tris_[bodyIndex][iface][ielem][n] = k;

            // Mark in the table the node that should be kept
            table[tris_[bodyIndex][iface][ielem][n]]++;
          }
        }
      }

      base += tessBody.nvert;
    }
  }

  // Count the number of solid edges and nodes
  int nSolidEdge = 0, nSolidNode = 0;
  for ( std::size_t ibody = 0; ibody < modelBodyIndex_[SOLIDINDEX].size(); ibody++ )
  {
    const int bodyIndex = modelBodyIndex_[SOLIDINDEX][ibody];
    nSolidEdge += bodies_[bodyIndex].nEdges();
    nSolidNode += bodies_[bodyIndex].nNodes();
  }

  // remove the unused points -- crunch the point list
  int ipnt = 0;
  for (int i = 0; i < npts; i++)
  {
    if (table[i] == 0) continue;

    // Don't overwrite solid edges with sheet edge t values
    if ( !(modelTopoType_[i] == 0 && modelTopoIndex_[i] > nSolidNode) || // Node
          (modelTopoType_[i] >  0 && modelTopoIndex_[i] > nSolidEdge ) ) // Edge
    {
      xyzs_[ipnt] = xyzs_[i];
      uvt_[ipnt]  = uvt_[i];
    }

    modelTopoType_[ipnt]  = modelTopoType_[i];
    modelTopoIndex_[ipnt] = modelTopoIndex_[i];

    table[i] = ipnt;
    ipnt++;
  }

  // update the triangle indices
  for ( std::size_t ibody = 0; ibody < tris_.size(); ibody++ )
    for ( std::size_t iface = 0; iface < tris_[ibody].size(); iface++)
      for ( std::size_t itri = 0; itri < tris_[ibody][iface].size(); itri++)
        for (int n = 0; n < 3; n++)
          tris_[ibody][iface][itri][n] = table[tris_[ibody][iface][itri][n]];

  // resize the vectors
  xyzs_.resize(ipnt);
  xyzs_.shrink_to_fit();

  uvt_.resize(ipnt);
  uvt_.shrink_to_fit();

  modelTopoType_.resize(ipnt);
  modelTopoType_.shrink_to_fit();

  modelTopoIndex_.resize(ipnt);
  modelTopoIndex_.shrink_to_fit();
}

EGTessModel::~EGTessModel()
{
}
namespace
{

void triangulateioInit( triangulateio& io )
{
  io.pointlist = NULL;
  io.pointattributelist = NULL;
  io.pointmarkerlist = NULL;
  io.numberofpoints = 0;
  io.numberofpointattributes = 0;

  io.trianglelist = NULL;
  io.triangleattributelist = NULL;
  io.trianglearealist = NULL;
  io.neighborlist = NULL;
  io.numberoftriangles = 0;
  io.numberofcorners = 0;
  io.numberoftriangleattributes = 0;

  io.segmentlist = NULL;
  io.segmentmarkerlist = NULL;
  io.numberofsegments = 0;

  io.holelist = NULL;
  io.numberofholes = 0;

  io.regionlist = NULL;
  io.numberofregions = 0;

  io.edgelist = NULL;
  io.edgemarkerlist = NULL;
  io.normlist = NULL;
  io.numberofedges = 0;
}

void triangulateioDestroy( triangulateio& io )
{
  trifree( (void*)io.pointlist );
  trifree( (void*)io.pointattributelist );
  trifree( (void*)io.pointmarkerlist );

  trifree( (void*)io.trianglelist );
  trifree( (void*)io.triangleattributelist );
  trifree( (void*)io.trianglearealist );
  trifree( (void*)io.neighborlist );

  trifree( (void*)io.segmentlist );
  trifree( (void*)io.segmentmarkerlist );

  trifree( (void*)io.holelist );

  trifree( (void*)io.regionlist );

  trifree( (void*)io.edgelist );
  trifree( (void*)io.edgemarkerlist );
  trifree( (void*)io.normlist );

  // Set all pointers to NULL and counts to 0
  triangulateioInit( io );
}

#if 0 // for debugging
void triangulateioTecplot( const triangulateio& io, const std::string filename )
{
  FILE* fp = fopen( filename.c_str(), "w" );

  fprintf( fp, "VARIBALES=U,V\n" );
  fprintf( fp, "ZONE T=\"Boundary\", N=%d, E=%d, F=FEPOINT, ET=LINESEG\n", io.numberofpoints, io.numberofsegments );

  // write coordinates
  for (int n = 0; n < io.numberofpoints; n++)
  {
    for (int d = 0; d < 2; d++)
      fprintf( fp, "%22.15e ", io.pointlist[2*n+d] );
    fprintf( fp, "\n");
  }

  // connectivity

  for (int elem = 0; elem < io.numberofsegments; elem++)
  {
    for (int n = 0; n < 2; n++ )
      fprintf( fp, "%d ", io.segmentlist[2*elem+n] );
    fprintf( fp, "\n" );
  }

  fclose(fp);
}
#endif

}

Real
EGTessModel::getSize() const
{
  double box[6], size = 0;

  for (std::size_t i = 0; i < bodies_.size(); i++)
  {
    EG_STATUS( EG_getBoundingBox(bodies_[i], box) );

    size = MAX(box[3]-box[0], size);
    size = MAX(box[4]-box[1], size);
    size = MAX(box[5]-box[2], size);
  }

  return size;
}

int
EGTessModel::nFaces() const
{
  int nFace = 0;

  for (std::size_t i = 0; i < bodies_.size(); i++)
    nFace += bodies_[i].nFaces();

  return nFace;
}

void
EGTessModel::TriangleFaces(const std::vector< EGBody<3> >& bodies, std::vector<ego>& tess)
{
  // Construct an inverse map so that the body-type and type specific index can be retrieved from a global body index
  // Construct an inverse map so that the body-type and type specific index can be retrieved from a global body index
  FloatingEdgeMap floatingEdges; // [ibody][iface][floating]
  std::vector< std::pair<int,int> > modelBodyInvIndex;
  fillFloatingEdgeMap(floatingEdges, modelBodyInvIndex);

  for (std::size_t ibody = 0; ibody < bodies.size(); ibody++)
  {
    EG_STATUS( EG_openTessBody(tess[ibody]) );

    typedef EGFace<3>::ParamCoord ParamCoord;
    typedef EGFace<3>::CartCoord CartCoord;

    const EGBody<3>& body = bodies[ibody];
    std::vector< EGFace<3> > faces = body.getFaces();

    std::vector< EGEdge<3> > edges = bodies[ibody].getEdges();

    for (std::size_t iface = 0; iface < faces.size(); iface++ )
    {
      if ( !faces[iface].hasAttribute(EGTessModel::TRIANGLE) ) continue;

      double minAngle = 28.5;
      faces[iface].getAttribute(EGTessModel::TRIANGLE, minAngle);

      triangulateio in, emptymesh, out;

      triangulateioInit(in);
      triangulateioInit(emptymesh);
      triangulateioInit(out);

      int nloop = 0;
      const int *lIndices = NULL;
      EG_STATUS( EG_getTessLoops( tess[ibody], iface+1, &nloop, &lIndices ) );

      int plen, tlen;
      const int *pindex = NULL, *ptype = NULL;
      const double *ppoints = NULL, *puv = NULL;
      const int *ptris = NULL, *ptric = NULL;
      EG_STATUS( EG_getTessFace(tess[ibody], iface+1, &plen, &ppoints, &puv, &ptype, &pindex,
                                &tlen, &ptris, &ptric) );

      std::vector< std::array<Real,3> > xyz;
      std::vector< std::array<Real,2> > uv;
      std::vector< std::array<Real,2> > uvt;
      std::vector< std::array<int,2> > segments;
      std::vector< int > index;
      std::vector< int > type;

      faceEdgeSegments(ibody, iface, bodies, tess, modelBodyInvIndex,
                       floatingEdges, xyz, uv, uvt, segments, index, type);

      // set the number of points and segments
      in.numberofpoints   = (int)uv.size();
      in.numberofsegments = (int)segments.size();

      in.numberofpointattributes = 4; // Use attributes to store type, index and uvt

      in.pointlist          = (REAL *) malloc( in.numberofpoints * 2 * sizeof(REAL) );
      in.pointattributelist = (REAL *) malloc( in.numberofpoints * in.numberofpointattributes * sizeof(REAL) );
      in.segmentlist        = (int *)  malloc( in.numberofsegments * 2 * sizeof(int) );

      for (std::size_t i = 0; i < uv.size(); i++)
      {
        in.pointlist[2*i+0] = uv[i][0];
        in.pointlist[2*i+1] = uv[i][1];

        in.pointattributelist[4*i+0] = type[i];
        in.pointattributelist[4*i+1] = index[i];
        in.pointattributelist[4*i+2] = uvt[i][0];
        in.pointattributelist[4*i+3] = uvt[i][1];
      }

      for (std::size_t seg = 0; seg < segments.size(); seg++)
      {
        in.segmentlist[2*seg+0] = segments[seg][0] +1;//1-based indexing
        in.segmentlist[2*seg+1] = segments[seg][1] +1;//1-based indexing
      }

      const int numberofpoints = in.numberofpoints;

      //triangulateioTecplot(in, "face.dat");

      // -p Triangulates a Planar Straight Line Graph
      // -e Outputs a list of edges of the triangulation.
      // -z Numbers all items starting from zero (rather than one). This switch is useful when calling Triangle from another program.
      // -n Outputs a list of triangles neighboring each triangle.
      // -Y Prohibits the insertion of Steiner points on the mesh boundary.
      //    If specified twice (-YY), it prohibits the insertion of Steiner points on any segment, including internal segments.
      // -V Verbose: Gives detailed information about what Triangle is doing. Add more `V's for increasing amount of detail.
      // -Q Quiet: Suppresses all explanation of what Triangle is doing, unless an error occurs.

      // Compute an empty mesh that only connects nodes, but does not create any vertecies on the interior of the face
      triangulate((char*)"pYYQ", &in, &emptymesh, NULL);

      std::vector<double> holepoints;

      // Find all the holes in the empty mesh by checking the centroids of all triangles in the empty mesh
      //std::vector<int> senses = body.getEdgeSenses();
      //int seg = 0;
      ParamCoord UV;
      for (int k = 0; k < emptymesh.numberoftriangles; k++ )
      {
        int trinodes[3] = {emptymesh.trianglelist[3*k+0]-1,
                           emptymesh.trianglelist[3*k+1]-1,
                           emptymesh.trianglelist[3*k+2]-1};

        // Compute the triangle centroid in UV space
        UV = 0;
        for (int n = 0; n < 3; n++)
        {
          UV[0] += emptymesh.pointlist[2*trinodes[n]+0];
          UV[1] += emptymesh.pointlist[2*trinodes[n]+1];
        }
        UV /= 3;

        // If the centroid of the triangle is not inside the face, mark it as a hole
        if ( !faces[iface].inFace(UV) )
        {
          for (int n = 0; n < 2; n++)
            holepoints.push_back(UV[n]);
        }
      }

      //Done with the empty mesh
      triangulateioDestroy(emptymesh);

      //Add the points inside the holes if any
      if ( holepoints.size() > 0 )
      {
        in.numberofholes = holepoints.size()/2;
        in.holelist = (REAL *) malloc(in.numberofholes * 2 * sizeof(REAL));
        for ( std::size_t n = 0; n < holepoints.size(); n++ )
          in.holelist[n] = holepoints[n];
      }

      // -p Triangulates a Planar Straight Line Graph
      // -D  Conforming Delaunay:  all triangles are truly Delaunay.
      // -z Numbers all items starting from zero (rather than one). This switch is useful when calling Triangle from another program.
      // -Y Prohibits the insertion of Steiner points on the mesh boundary.
      //    If specified twice (-YY), it prohibits the insertion of Steiner points on any segment, including internal segments.
      // -V Verbose: Gives detailed information about what Triangle is doing. Add more `V's for increasing amount of detail.
      // -Q Quiet: Suppresses all explanation of what Triangle is doing, unless an error occurs.
      // -q Quality mesh generation with no angles smaller than 20 degrees. An alternate minimum angle may be specified after the `q'.

      std::string qual = "q" + stringify(minAngle);

      std::string triangleparams ="pYYQ" + qual;

      //std::cout << "Running triangle" << std::endl;
      triangulate((char*)triangleparams.c_str(), &in, &out, NULL);
      //std::cout << "Triangle done" << std::endl;

      in.holelist = NULL; //pointer has been moved to 'out'
      triangulateioDestroy(in);

      // Compute the xyz values for the Triangle mesh
      std::vector<double> outxyz(3*out.numberofpoints);
      std::vector<int> outtype(out.numberofpoints, -1);
      std::vector<int> outindex(out.numberofpoints, -1);
      std::vector<double> outuvt(2*out.numberofpoints, -1);

      // first copy the xyz values from the original points
      int ipnt = 0;
      int istart = 0;
      for (int loop = 0; loop < nloop; loop++)
      {
        for (int i = istart; i < lIndices[loop]; i++)
        {
          outxyz[3*ipnt+0] = ppoints[3*ipnt+0];
          outxyz[3*ipnt+1] = ppoints[3*ipnt+1];
          outxyz[3*ipnt+2] = ppoints[3*ipnt+2];

          if ( ipnt < numberofpoints )
          {
            outtype[ipnt]    = ptype[ipnt];
            outindex[ipnt]   = pindex[ipnt];
            outuvt[2*ipnt+0] = uvt[ipnt][0];
            outuvt[2*ipnt+1] = uvt[ipnt][1];
          }

          ipnt++;
        }

        istart = lIndices[loop];
      }

      // evaluate all remaining interior points at the uv values
      CartCoord X;
      for (; ipnt < out.numberofpoints; ipnt++)
      {
        UV = {out.pointlist[2*ipnt+0], out.pointlist[2*ipnt+1]};
        X = faces[iface](UV);

        outxyz[3*ipnt+0] = X[0];
        outxyz[3*ipnt+1] = X[1];
        outxyz[3*ipnt+2] = X[2];

        if ( ipnt < numberofpoints )
        {
          // These are from floating edges, so mark that with a negative index
          outtype[ipnt]    = (int)out.pointattributelist[4*ipnt+0];
          outindex[ipnt]   = -(int)out.pointattributelist[4*ipnt+1];
          outuvt[2*ipnt+0] = out.pointattributelist[4*ipnt+2];
          outuvt[2*ipnt+1] = out.pointattributelist[4*ipnt+3];
        }
        else
        {
          outuvt[2*ipnt+0] = UV[0];
          outuvt[2*ipnt+1] = UV[1];
        }
      }

      ego geom, *pchld;
      int oclass, mtype, nchld, *sens;
      double range[4];
      EG_STATUS( EG_getTopology(faces[iface],&geom,&oclass,&mtype,range,&nchld,&pchld,&sens) );

      if ( mtype == SREVERSE )
      {
        // If the face is reversed, the triangle orientation needs to be swapped.
        // This is done by swapping any two nodes on the triangles (the first two are part of any)
        for (int i = 0; i < out.numberoftriangles; i++)
          std::swap(out.trianglelist[3*i+0], out.trianglelist[3*i+1]);
      }

      //std::cout << "Setting tess face" << std::endl;
      EG_STATUS( EG_setTessFace(tess[ibody], iface+1, out.numberofpoints, &outxyz[0],
                                out.pointlist, out.numberoftriangles, out.trianglelist) );
      //std::cout << "Setting tess face done" << std::endl;

      if ( floatingEdges[ibody][iface].size() > 0 )
      {
        // Store the type inedex for the points so they can be overwritten in the body zippering process
        faces[iface].addAttribute("->type",outtype);
        faces[iface].addAttribute("->index",outindex);
        faces[iface].addAttribute("->uvt",outuvt);
      }

      // destroy the Triangle mesh
      triangulateioDestroy(out);
    }

    // close the EGADS tessellation
    ego dummyBody;
    int state, np;
    EG_STATUS( EG_statusTessBody(tess[ibody], &dummyBody, &state, &np) );
  }
}


void
EGTessModel::faceEdgeSegments(const int ibody, const int iface,
                              const std::vector< EGBody<3> >& bodies,
                              std::vector<ego>& tess,
                              const std::vector< std::pair<int,int> >& modelBodyInvIndex,
                              const FloatingEdgeMap& floatingEdges,
                              std::vector< std::array<Real,3> >& xyz,
                              std::vector< std::array<Real,2> >& uv,
                              std::vector< std::array<Real,2> >& uvt,
                              std::vector< std::array<int,2> >& segments,
                              std::vector< int >& index,
                              std::vector< int >& type)
{
  typedef EGFace<3>::ParamCoord ParamCoord;
  typedef EGFace<3>::CartCoord CartCoord;

  const std::vector< FloatingEdge >& floatingFaceEdges = floatingEdges[ibody][iface];

  std::vector< EGFace<3> > faces = bodies[ibody].getFaces();
  std::vector< EGEdge<3> > edges = bodies[ibody].getEdges();

  int nloop = 0;
  const int *lIndices = NULL;
  EG_STATUS( EG_getTessLoops( tess[ibody], iface+1, &nloop, &lIndices ) );

  int plen, tlen;
  const int *pindex = NULL, *ptype = NULL;
  const double *ppoints = NULL, *puv = NULL;
  const int *ptris = NULL, *ptric = NULL;
  EG_STATUS( EG_getTessFace(tess[ibody], iface+1, &plen, &ppoints, &puv, &ptype, &pindex,
                            &tlen, &ptris, &ptric) );

  // set the number of points on the loops (closed loops will have same number of segments and points)
  int npoints   = lIndices[nloop-1];
  int nsegments = lIndices[nloop-1];

  std::set<int> floatingNodeIndex;

  // Save the unique node indexes for the body donating the floating edge
  for ( auto floatingEdge = floatingFaceEdges.begin(); floatingEdge != floatingFaceEdges.end(); floatingEdge++ )
  {
    std::vector< EGNode<3> > nodes = bodies[floatingEdge->bodyIndex].getEdges()[floatingEdge->edgeIndex].getNodes();

    std::pair<int,int> model_body = modelBodyInvIndex[floatingEdge->bodyIndex]; // Model index of the body

    for (std::size_t inode = 0; inode < nodes.size(); inode++)
    {
      int body_node = bodies[floatingEdge->bodyIndex].getBodyIndex(nodes[inode]); // Body index of the node
      floatingNodeIndex.insert( modelNodeIndex_[model_body.first][model_body.second][body_node-1] );
    }
  }

  // construct a node map to map the nodes into the triangle input
  std::map<int,int> floatingNodes;
  std::map<int,bool> floatingNodeUV;

  {
    // Look for any nodes in the loop, and get their Triangle input point index
    int istart = 0;
    int jpnt = 0;
    for (int loop = 0; loop < nloop; loop++)
    {
      for (int i = istart; i < lIndices[loop]; i++)
      {
        if (ptype[jpnt] == 0 && (floatingNodeIndex.find(pindex[jpnt]-1) != floatingNodeIndex.end()) )
        {
          int model_node = *floatingNodeIndex.find(pindex[jpnt]-1);
          floatingNodes[ model_node ] = jpnt;
          floatingNodeUV[ model_node ] = false;
          floatingNodeIndex.erase(pindex[jpnt]-1);
        }
        jpnt++;
      }
      istart = lIndices[loop];
    }

    // Add any remaining nodes not in the loops
    for (auto model_node = floatingNodeIndex.begin(); model_node != floatingNodeIndex.end(); model_node++ )
    {
      floatingNodes[*model_node] = jpnt++;
      floatingNodeUV[ *model_node ] = true;
    }
  }

  // Add the floating nodes that do not exist in the loops to the point count
  npoints += floatingNodeIndex.size();

  // Add segments counts from floating edges
  for ( auto floatingEdge = floatingFaceEdges.begin(); floatingEdge != floatingFaceEdges.end(); floatingEdge++ )
  {
    if (edges[floatingEdge->edgeIndex].isDegenerate()) continue;

    int elen;
    const double *pxyz, *pt;
    EG_STATUS( EG_getTessEdge(tess[floatingEdge->bodyIndex], floatingEdge->edgeIndex+1, &elen, &pxyz, &pt) );

    int nNode = edges[floatingEdge->edgeIndex].nNode();

    // add the floating edge point count and segment count (excluding nodes)
    npoints   += elen-nNode;
    nsegments += elen - 1;
  }

  xyz.resize(npoints);
  uv.resize(npoints);
  uvt.resize(npoints);
  segments.resize(nsegments);
  index.resize(npoints);
  type.resize(npoints);

  // Add all points and segments from the face loops
  int istart = 0;
  int ipnt = 0;
  int seg = 0;
  for (int loop = 0; loop < nloop; loop++)
  {
    for (int i = istart; i < lIndices[loop]; i++)
    {
      xyz[ipnt][0] = ppoints[3*ipnt+0];
      xyz[ipnt][1] = ppoints[3*ipnt+1];
      xyz[ipnt][2] = ppoints[3*ipnt+2];

      uv[ipnt][0] = puv[2*ipnt+0];
      uv[ipnt][1] = puv[2*ipnt+1];

      type[ipnt] = ptype[ipnt];
      index[ipnt] = pindex[ipnt];

      if ( type[ipnt] == 0 )
      {
        //Just set the node parametric coordinates to zero
        uvt[ipnt][0] = uvt[ipnt][1] = 0;
      }
      else if ( type[ipnt] > 0 )
      {
        //Extract the edge tessellation
        int elen;
        const double *exyz, *pt;
        EG_STATUS( EG_getTessEdge(tess[ibody], index[ipnt], &elen, &exyz, &pt) );

        //Update the parametric coordinates with the edge t values
        uvt[ipnt][0] = pt[type[ipnt]-1];
        uvt[ipnt][1] = 0;
      }
      else
      {
        //Simply copy over the interior parametric coordinates
        uvt[ipnt][0] = puv[2*ipnt+0];
        uvt[ipnt][1] = puv[2*ipnt+1];
      }

      segments[seg][0] = ipnt+0;
      segments[seg][1] = ipnt+1;

      ipnt++;
      seg++;
    }
    // Close the loop by overwriting the last incorrect segment
    segments[seg-1][1] = istart;

    istart = lIndices[loop];
  }

  // Offset the current point index to skip floating nodes
  ipnt += floatingNodeIndex.size();

  // Add segments from floating segments
  for ( auto floatingEdge = floatingFaceEdges.begin(); floatingEdge != floatingFaceEdges.end(); floatingEdge++ )
  {
    std::vector< EGNode<3> > nodes = bodies[floatingEdge->bodyIndex].getEdges()[floatingEdge->edgeIndex].getNodes();

    int elen;
    const double *pxyz, *pt;
    EG_STATUS( EG_getTessEdge(tess[floatingEdge->bodyIndex], floatingEdge->edgeIndex+1, &elen, &pxyz, &pt) );

    ParamCoord UV;
    CartCoord X;

    int body_node = bodies[floatingEdge->bodyIndex].getBodyIndex(nodes[0]);            // Body index of the node
    std::pair<int,int> model_body = modelBodyInvIndex[floatingEdge->bodyIndex];        // Model index of the body
    int model_node = modelNodeIndex_[model_body.first][model_body.second][body_node-1];// Model index for the node
    int ipnt_node = floatingNodes[ model_node ]; // Triangle input point index for the node

    // Set the first node coordinates (which may overwite what was set by another edge)
    int i = 0;
    if (floatingNodeUV[ model_node ]) // Only compute UV values on nodes that are not in the face loops
    {
      X = {pxyz[3*i+0], pxyz[3*i+1], pxyz[3*i+2]};
      xyz[ipnt_node][0] = X[0];
      xyz[ipnt_node][1] = X[1];
      xyz[ipnt_node][2] = X[2];

      UV = faces[iface](X);
      uv[ipnt_node][0] = UV[0];
      uv[ipnt_node][1] = UV[1];

      uvt[ipnt_node][0] = uvt[ipnt_node][1] = 0;
    }

    type[ipnt_node] = 0; // Node type
    index[ipnt_node] = model_node+1; // model index of the node

    segments[seg][0] = ipnt_node+0;
    segments[seg][1] = ipnt+0;
    seg++;

    int model_edge = modelEdgeIndex_[model_body.first][model_body.second][floatingEdge->edgeIndex]+1; // model index of the Edge

    for (i = 1; i < elen-1; i++)
    {
      X = {pxyz[3*i+0], pxyz[3*i+1], pxyz[3*i+2]};

      xyz[ipnt][0] = X[0];
      xyz[ipnt][1] = X[1];
      xyz[ipnt][2] = X[2];

      UV = faces[iface](X);
      uv[ipnt][0] = UV[0];
      uv[ipnt][1] = UV[1];

      type[ipnt] = i+1; // Edge type
      index[ipnt] = model_edge;

      //Update the parametric coordinates with the edge t values
      uvt[ipnt][0] = pt[i];
      uvt[ipnt][1] = 0;

      segments[seg][0] = ipnt+0;
      segments[seg][1] = ipnt+1;

      ipnt++;
      seg++;
    }

    body_node = bodies[floatingEdge->bodyIndex].getBodyIndex(nodes[nodes.size()-1]); // Body index of the 2nd (or only) node
    model_node = modelNodeIndex_[model_body.first][model_body.second][body_node-1];  // Model index for the node

    // get the point index for the 2nd (or only) node
    ipnt_node = floatingNodes[ model_node ];

    // Set the first node coordinates (which may overwrite what was set by another edge)
    i = elen-1;
    if (floatingNodeUV[ model_node ]) // Only compute UV values on nodes that are not in the face loops
    {
      X = {pxyz[3*i+0], pxyz[3*i+1], pxyz[3*i+2]};
      xyz[ipnt_node][0] = X[0];
      xyz[ipnt_node][1] = X[1];
      xyz[ipnt_node][2] = X[2];

      UV = faces[iface](X);
      uv[ipnt_node][0] = UV[0];
      uv[ipnt_node][1] = UV[1];

      uvt[ipnt_node][0] = uvt[ipnt_node][1] = 0;
    }

    type[ipnt_node] = 0; // Node type
    index[ipnt_node] = model_node+1; // model index of the node

    // Correct the last segment by connecting it to the 2nd (or only) node
    segments[seg-1][1] = ipnt_node;
  }

  SANS_ASSERT( ipnt == npoints );
  SANS_ASSERT( seg == nsegments );
}

std::vector< EGNode<3> > EGTessModel::getAllNodes() const { return modelNodes_; }
std::vector< EGEdge<3> > EGTessModel::getAllEdges() const { return modelEdges_; }
std::vector< EGFace<3> > EGTessModel::getAllFaces() const { return modelFaces_; }

std::vector< EGNode<3> > EGTessModel::getUniqueNodes() const { return getTopos< EGNode<3> >(modelNodeIndex_, NODE); }
std::vector< EGEdge<3> > EGTessModel::getUniqueEdges() const { return getTopos< EGEdge<3> >(modelEdgeIndex_, EDGE); }
std::vector< EGFace<3> > EGTessModel::getUniqueFaces() const { return getTopos< EGFace<3> >(modelFaceIndex_, FACE); }

template<class EGTopo>
std::vector< EGTopo >
EGTessModel::getTopos(const std::vector< std::vector<int> > topoIndexMap[], const int TOPO) const
{
  // Find the maximum number of topologies
  int ntopo = 0;
  for ( int ibodyTopo = 0; ibodyTopo < NBODYTOPO; ibodyTopo++ )
    for ( std::size_t ibody = 0; ibody < modelBodyIndex_[ibodyTopo].size(); ibody++ )
      for ( std::size_t itopo = 0; itopo < topoIndexMap[ibodyTopo][ibody].size(); itopo++ )
        ntopo = std::max(ntopo, topoIndexMap[ibodyTopo][ibody][itopo]+1);

  std::vector< ego > topos(ntopo, NULL);

  // Loop in reverse order so the solid body topo's overwrite any other body topos
  for ( int ibodyTopo = NBODYTOPO-1; ibodyTopo >= 0; ibodyTopo-- )
  {
    for ( std::size_t ibody = 0; ibody < modelBodyIndex_[ibodyTopo].size(); ibody++ )
    {
      const int bodyIndex = modelBodyIndex_[ibodyTopo][ibody];
      int ntopo;
      ego *ptopos;

      //Extract all topology objects
      EG_STATUS( EG_getBodyTopos(bodies_[bodyIndex], NULL, TOPO, &ntopo, &ptopos) );

      for ( int itopo = 0; itopo < ntopo; itopo++ )
        topos[topoIndexMap[ibodyTopo][ibody][itopo]] = ptopos[itopo];

      EG_free(ptopos); ptopos = NULL;
    }
  }

  // Return non-null ego objects.
  // This crunches the list so duplicate nodes/edges in the model are removed
  std::vector< EGTopo > modelTopos;
  for ( std::size_t itopo = 0; itopo < topos.size(); itopo++ )
    if ( topos[itopo] != NULL )
      modelTopos.emplace_back( EGObject(topos[itopo]) );

  modelTopos.shrink_to_fit();

  return modelTopos;
}

template<class EGTopo>
std::vector< EGTopo >
EGTessModel::getAllTopos(const int TOPO) const
{
  std::vector< EGTopo > modelTopos;

  for ( int ibodyTopo = 0; ibodyTopo < NBODYTOPO; ibodyTopo++ )
  {
    for ( std::size_t ibody = 0; ibody < modelBodyIndex_[ibodyTopo].size(); ibody++ )
    {
      const int bodyIndex = modelBodyIndex_[ibodyTopo][ibody];
      int ntopo;
      ego *ptopos;

      //Extract all topology objects
      EG_STATUS( EG_getBodyTopos(bodies_[bodyIndex], NULL, TOPO, &ntopo, &ptopos) );

      for ( int itopo = 0; itopo < ntopo; itopo++ )
        modelTopos.emplace_back( EGObject(ptopos[itopo]) );

      EG_free(ptopos); ptopos = NULL;
    }
  }

  modelTopos.shrink_to_fit();

  return modelTopos;
}

//-----------------------------------------------------------------------------//
std::map<ego,ego>
EGTessModel::alltoUniqueMap() const
{
  std::map<ego,ego> egomap;

  addMapEgos< EGNode<3> >(modelNodeIndex_, NODE, egomap);
  addMapEgos< EGEdge<3> >(modelEdgeIndex_, EDGE, egomap);
  addMapEgos< EGFace<3> >(modelFaceIndex_, FACE, egomap);

  return egomap;
}

template<class EGTopo>
void
EGTessModel::addMapEgos(const std::vector< std::vector<int> > topoIndexMap[], const int TOPO, std::map<ego,ego>& egomap) const
{
  std::vector< EGTopo > topos = this->template getAllTopos<EGTopo>(TOPO);

  int jtopo = 0;
  for ( int ibodyTopo = 0; ibodyTopo < NBODYTOPO; ibodyTopo++ )
    for ( std::size_t ibody = 0; ibody < modelBodyIndex_[ibodyTopo].size(); ibody++ )
      for ( std::size_t itopo = 0; itopo < topoIndexMap[ibodyTopo][ibody].size(); itopo++ )
      {
        // map the topology from All topo's to Unique topos
        egomap[topos[jtopo]] = topos[topoIndexMap[ibodyTopo][ibody][itopo]];
        jtopo++;
      }
}

//-----------------------------------------------------------------------------//
std::map<ego,std::vector<ego>>
EGTessModel::getFaceChildren() const
{
  std::map<ego,std::vector<ego>> faceEdgeMap;

  // Construct an inverse map so that the body-type and type specific index can be retrieved from a global body index
  FloatingEdgeMap floatingEdges; // [ibody][iface][floating]
  std::vector< std::pair<int,int> > modelBodyInvIndex;
  fillFloatingEdgeMap(floatingEdges, modelBodyInvIndex);

  for ( std::size_t ibodyTopo = 0; ibodyTopo < EGTessModel::NBODYTOPO; ibodyTopo++ )
  {
    for ( std::size_t ibody = 0; ibody < modelBodyIndex_[ibodyTopo].size(); ibody++ )
    {
      int bodyIndex = modelBodyIndex_[ibodyTopo][ibody];
      std::vector< EGFace<3> > faces = bodies_[bodyIndex].getFaces();
      const int nfaces = bodies_[bodyIndex].nFaces();

      for (int iface = 0; iface < nfaces; iface++)
      {
        std::vector< EGLoop<3> > loops = faces[iface].getLoops();
        std::vector<ego>& faceEdges = faceEdgeMap[faces[iface]];

        for (std::size_t iloop = 0; iloop < loops.size(); iloop++)
        {
          std::vector< EGEdge<3> > loopEdges = loops[iloop].getEdges();

          for (std::size_t iloopEdge = 0; iloopEdge < loopEdges.size(); iloopEdge++)
          {
            int ibodyEdge = bodies_[bodyIndex].getBodyIndex(loopEdges[iloopEdge]) - 1;
            int imodelEdge = modelEdgeIndex_[ibodyTopo][ibody][ibodyEdge];

            faceEdges.push_back(modelEdges_[imodelEdge]);
          }
        }

        // floating edges
        const std::vector< EGTessModel::FloatingEdge >& floatingFaceEdges = floatingEdges[bodyIndex][iface];

        // construct the floating ploops (no need to traverse twice)
        for ( auto floatingEdge = floatingFaceEdges.begin(); floatingEdge != floatingFaceEdges.end(); floatingEdge++ )
        {
          std::pair<int,int> model_body = modelBodyInvIndex[floatingEdge->bodyIndex];        // Model index of the body

          int imodelEdge = modelEdgeIndex_[model_body.first][model_body.second][floatingEdge->edgeIndex]; // model index of the Edge

          faceEdges.push_back(modelEdges_[imodelEdge]);
        }
      }
    }
  }

  return faceEdgeMap;
}

//-----------------------------------------------------------------------------//
std::map<ego,std::vector<ego>>
EGTessModel::getEdgeChildren() const
{
  std::map<ego,std::vector<ego>> edgeNodeMap;

  for ( int ibodyTopo = 0; ibodyTopo < EGTessModel::NBODYTOPO; ibodyTopo++ )
  {
    for ( std::size_t ibody = 0; ibody < modelEdgeIndex_[ibodyTopo].size(); ibody++ )
    {
      for ( std::size_t ibodyEdge = 0; ibodyEdge < modelEdgeIndex_[ibodyTopo][ibody].size(); ibodyEdge++)
      {
        int imodelEdge = modelEdgeIndex_[ibodyTopo][ibody][ibodyEdge];

        int index0 = edgeNodesIndex_[imodelEdge][0]-1;
        int index1 = edgeNodesIndex_[imodelEdge][1]-1;

        std::vector<ego>& nodes = edgeNodeMap[modelEdges_[imodelEdge]];

        // don't process the same edge twice
        if (nodes.size() !=0 ) continue;

        if (modelEdges_[imodelEdge].nNode() == 1)
        {
          nodes.push_back(modelNodes_[index0]);
        }
        else
        {
          nodes.push_back(modelNodes_[index0]);
          nodes.push_back(modelNodes_[index1]);
        }
      }
    }
  }

  return edgeNodeMap;
}

//-----------------------------------------------------------------------------//
void
EGTessModel::modelIndexMap(const std::vector< EGBody<3> >& bodies, const std::vector< int >& bodymap,
                           int& modelIndex, std::vector< std::vector<int> >& topoIndexMap, const int TOPO)
{
  //Builds up a model indexing for a topology type inside bodies
  topoIndexMap.resize(bodymap.size());
  for ( std::size_t ibody = 0; ibody < bodymap.size(); ibody++ )
  {
    const int nTopo = bodies[bodymap[ibody]].nTopos(TOPO);
    topoIndexMap[ibody].resize(nTopo);

    for ( int itopo = 0; itopo < nTopo; itopo++ )
      topoIndexMap[ibody][itopo] = modelIndex++;
  }
}

void
EGTessModel::initModelIndexes(const std::vector< EGBody<3> >& bodies)
{
  std::vector< int >& solidBodies = modelBodyIndex_[SOLIDINDEX];
  std::vector< int >& sheetBodies = modelBodyIndex_[SHEETINDEX];

  // Sort out all sheet/face bodies from the solid bodies
  for ( std::size_t ibody = 0; ibody < bodies.size(); ibody++ )
  {
    if (bodies[ibody].isSolid())
      solidBodies.push_back(ibody);
    else if ( bodies[ibody].isSheet() || bodies[ibody].isFace() )
      sheetBodies.push_back(ibody);
    else
      SANS_DEVELOPER_EXCEPTION("Only expected solid and sheet bodies");
  }

  // Initialize the face index map
  std::vector< std::vector< int > >& solidFaceIndex = modelFaceIndex_[SOLIDINDEX];
  std::vector< std::vector< int > >& sheetFaceIndex = modelFaceIndex_[SHEETINDEX];

  int faceIndex = 0;
  modelIndexMap(bodies, solidBodies, faceIndex, solidFaceIndex, FACE);
  modelIndexMap(bodies, sheetBodies, faceIndex, sheetFaceIndex, FACE);

  // These indexes will be updated in tessSameEdges to provide a unique index for edges and node that are isSame()

  // Initialize the edge index map to identity. This map will be used to identify "edge equality" in the zipper process
  std::vector< std::vector< int > >& solidEdgeIndex = modelEdgeIndex_[SOLIDINDEX];
  std::vector< std::vector< int > >& sheetEdgeIndex = modelEdgeIndex_[SHEETINDEX];

  int edgeIndex = 0;
  modelIndexMap(bodies, solidBodies, edgeIndex, solidEdgeIndex, EDGE);
  modelIndexMap(bodies, sheetBodies, edgeIndex, sheetEdgeIndex, EDGE);

  // Initialize the node index map
  std::vector< std::vector< int > >& solidNodeIndex = modelNodeIndex_[SOLIDINDEX];
  std::vector< std::vector< int > >& sheetNodeIndex = modelNodeIndex_[SHEETINDEX];

  int nodeIndex = 0;
  modelIndexMap(bodies, solidBodies, nodeIndex, solidNodeIndex, NODE);
  modelIndexMap(bodies, sheetBodies, nodeIndex, sheetNodeIndex, NODE);

  // Save off all the model topologies
  modelNodes_ = getAllTopos< EGNode<3> >(NODE);
  modelEdges_ = getAllTopos< EGEdge<3> >(EDGE);
  modelFaces_ = getAllTopos< EGFace<3> >(FACE);
}

void
EGTessModel::mapSameEdges(const std::vector< EGBody<3> >& bodies,
                          const std::vector<ego>& tess )
{
  const std::vector< int >& solidBodies = modelBodyIndex_[SOLIDINDEX];
  const std::vector< int >& sheetBodies = modelBodyIndex_[SHEETINDEX];

  std::vector< std::vector< int > >& solidEdgeIndex = modelEdgeIndex_[SOLIDINDEX];
  std::vector< std::vector< int > >& sheetEdgeIndex = modelEdgeIndex_[SHEETINDEX];

  std::vector< std::vector< int > >& solidNodeIndex = modelNodeIndex_[SOLIDINDEX];
  std::vector< std::vector< int > >& sheetNodeIndex = modelNodeIndex_[SHEETINDEX];

  //Find all sheet body edges that also exist in solid bodies
  for ( std::size_t isheet = 0; isheet < sheetBodies.size(); isheet++ )
  {
    std::vector< EGEdge<3> > sheetEdges = bodies[sheetBodies[isheet]].getEdges();

    //Loop over the solid bodies for each sheet body
    for ( std::size_t isolid = 0; isolid < solidBodies.size(); isolid++ )
    {
      std::vector< EGEdge<3> > solidEdges = bodies[solidBodies[isolid]].getEdges();

      //Loop over all edges in the sheet body and solid body
      for ( int isheetedge = 0; isheetedge < (int)sheetEdges.size(); isheetedge++ )
      {
        for ( int isolidedge = 0; isolidedge < (int)solidEdges.size(); isolidedge++ )
        {
          // Check if the sheet edge and solid edge are on the same geometry
          if (isSame(sheetEdges[isheetedge], solidEdges[isolidedge]))
          {
            // Get the nodes on the two edges
            std::vector< EGNode<3> > solidNodes = solidEdges[isolidedge].getNodes();
            std::vector< EGNode<3> > sheetNodes = sheetEdges[isheetedge].getNodes();

            // Check that the nodes match on the edges
            if (solidEdges[isolidedge].isOneNode())
            {
              if ( !isSame(solidNodes[0], sheetNodes[0]) ) continue;
            }
            else
            {
              if ( !( (isSame(solidNodes[0], sheetNodes[0]) && isSame(solidNodes[1], sheetNodes[1])) ||
                      (isSame(solidNodes[0], sheetNodes[1]) && isSame(solidNodes[1], sheetNodes[0])) ) ) continue;
            }

            if (tess[solidBodies[isolid]] != nullptr)
            {
              //Grab the vertexes from both edges and use the edge with the larger count
              int solidlen;
              const double *psolidxyz, *psolidt;
              EG_STATUS( EG_getTessEdge(tess[solidBodies[isolid]], isolidedge+1, &solidlen, &psolidxyz, &psolidt) );

              int sheetlen;
              const double *psheetxyz, *psheett;
              EG_STATUS( EG_getTessEdge(tess[sheetBodies[isheet]], isheetedge+1, &sheetlen, &psheetxyz, &psheett) );

              if (solidlen > sheetlen)
              {
                // Transfer the tessellation from the solid body
                if ( solidlen >= 3 )
                  sheetEdges[isheetedge].addAttribute(".tPos", &psolidt[1], solidlen-2);
                else
                  sheetEdges[isheetedge].addAttribute(".tPos", 0);
              }
              else
              {
                // Transfer the tessellation from the sheet body
                if ( sheetlen >= 3 )
                  solidEdges[isolidedge].addAttribute(".tPos", &psheett[1], sheetlen-2);
                else
                  solidEdges[isolidedge].addAttribute(".tPos", 0);
              }
            }

//            std::cout << "Connecting : " << isheet << " " << isheetedge << " " << sheetEdgeIndex[isheet][isheetedge] << std::endl;
//            std::cout << "           : " << isolid << " " << isolidedge << " " << solidEdgeIndex[isolid][isolidedge] << std::endl;

            //Update the index map to register the equality
            sheetEdgeIndex[isheet][isheetedge] = solidEdgeIndex[isolid][isolidedge];

            const EGBody<3>& solidBody = bodies[solidBodies[isolid]];
            const EGBody<3>& sheetBody = bodies[sheetBodies[isheet]];

            // Update the node index map by finding which nodes are the same
            for (auto sheetnode = sheetNodes.begin(); sheetnode != sheetNodes.end(); sheetnode++ )
              for (auto solidnode = solidNodes.begin(); solidnode != solidNodes.end(); solidnode++ )
                if ( isSame(*sheetnode, *solidnode) )
                {
                  sheetNodeIndex[isheet][sheetBody.getBodyIndex(*sheetnode)-1] =
                      solidNodeIndex[isolid][solidBody.getBodyIndex(*solidnode)-1];
                  break;
                }

            break;
          }
        }
      }
    }
  }


  // Save off all the node index's for all edges in the model
  edgeNodesIndex_.resize(modelEdges_.size(), {{-1,-1}});

  int jedge = 0;
  for ( int ibodyTopo = 0; ibodyTopo < NBODYTOPO; ibodyTopo++ )
  {
    for ( std::size_t ibody = 0; ibody < modelBodyIndex_[ibodyTopo].size(); ibody++ )
    {
      const int bodyIndex = modelBodyIndex_[ibodyTopo][ibody];

      std::vector< EGEdge<3> > edges = bodies[bodyIndex].getEdges();

      for (std::size_t iedge = 0; iedge < edges.size(); iedge++)
      {
        std::vector< EGNode<3> > nodes = edges[iedge].getNodes();

        // Duplicate the node if this is a ONENODE edge
        if (nodes.size() == 1) nodes.push_back(nodes[0]);

        for (std::size_t inode = 0; inode < nodes.size(); inode++)
        {
          int body_index = bodies[bodyIndex].getBodyIndex(nodes[inode]);
          edgeNodesIndex_[jedge][inode] = modelNodeIndex_[ibodyTopo][ibody][body_index-1]+1;
        }

        int body_index0 = bodies[bodyIndex].getBodyIndex(nodes[0]);
        int body_index1 = bodies[bodyIndex].getBodyIndex(nodes[1]);

        int model_index0 = modelNodeIndex_[ibodyTopo][ibody][body_index0-1]+1;
        int model_index1 = modelNodeIndex_[ibodyTopo][ibody][body_index1-1]+1;

        // Make sure the nodes are sorted
        if ( model_index0 > model_index1 ) std::swap(model_index0, model_index1);

        // Store the node pair that gives the global model edge number (not the unique edge number)
        nodesEdgeIndex_[{model_index0,model_index1}] = jedge;

        jedge++;
      }
    }
  }
}

void
EGTessModel::fillFloatingEdgeMap( FloatingEdgeMap& floatingEdges, std::vector< std::pair<int,int> >& modelBodyInvIndex ) const
{
  // Construct an inverse map so that the body-type and type specific index can be retrieved from a global body index
  modelBodyInvIndex.resize(bodies_.size());

  for ( int ibodyTopo = 0; ibodyTopo < NBODYTOPO; ibodyTopo++ )
  {
    for ( std::size_t ibody = 0; ibody < modelBodyIndex_[ibodyTopo].size(); ibody++ )
    {
      const int bodyIndex = modelBodyIndex_[ibodyTopo][ibody];
      modelBodyInvIndex[bodyIndex] = std::pair<int,int>(ibodyTopo,ibody);
    }
  }

  // construct the floating edge map
  floatingEdges.resize(bodies_.size()); // [ibody][iface][floating]

  for ( std::size_t ibody = 0; ibody < bodies_.size(); ibody++ )
    floatingEdges[ibody].resize(bodies_[ibody].nFaces());

  // Store away any floating edges that will be added to faces
  for ( std::size_t ibody = 0; ibody < bodies_.size(); ibody++ )
  {
    if ( bodies_[ibody].isSolid() ) continue;

    std::vector< EGFace<3> > faces = bodies_[ibody].getFaces();

    for ( std::size_t iface = 0; iface < faces.size(); iface++ )
    {
      std::vector< EGLoop<3> > loops = faces[iface].getLoops();

      for ( std::size_t iloop = 0; iloop < loops.size(); iloop++ )
      {
        std::vector< EGEdge<3> > edges = loops[iloop].getEdges();

        for ( std::size_t iedge = 0; iedge < edges.size(); iedge++ )
        {
          if ( edges[iedge].hasAttribute(EGTessModel_FLOATINGEDGE) )
          {
            std::vector<int> bodyface;
            edges[iedge].getAttribute(EGTessModel_FLOATINGEDGE, bodyface);
            SANS_ASSERT( bodyface.size() == 2 );

            std::size_t solidBody = 0;
            while ( solidBody < bodies_.size() && !bodies_[solidBody].isSolid() ) solidBody++;
            bodyface[0] = solidBody+1;

            SANS_ASSERT( bodyface[0] > 0 && bodyface[0] <= (int)bodies_.size() );
            SANS_ASSERT_MSG( bodyface[1] > 0 && bodyface[1] <= (int)floatingEdges[bodyface[0]-1].size(),
                             "bodyface[1]=%d, floatingEdges[%d].size()=%d", bodyface[1], bodyface[0]-1, floatingEdges[bodyface[0]-1].size() );

            FloatingEdge floating;
            floating.bodyIndex = ibody;
            floating.edgeIndex = bodies_[ibody].getBodyIndex( edges[iedge] )-1;

            // Set triangle for now as AFLR4 can't seem to handle the floating edges...
            EGFace<3> face = bodies_[bodyface[0]-1].getFaces()[bodyface[1]-1];
            if ( !face.hasAttribute(EGTessModel::TRIANGLE) )
            {
              // Quote from Triangle help:
              // If the minimum angle is 28.6
              // degrees or smaller, Triangle is mathematically guaranteed to
              // terminate (assuming infinite precision arithmetic--Triangle may
              // fail to terminate if you run out of precision).
              face.addAttribute(EGTessModel::TRIANGLE, 28.5);
            }

            floatingEdges[bodyface[0]-1][bodyface[1]-1].push_back(floating);
          }
        }
      }
    }
  }
}

//-----------------------------------------------------------------------------//
EGTopologyBase
EGTessModel::modelTopoFromVertex(const std::size_t i) const
{
  SANS_ASSERT( i < modelTopoIndex_.size() );

  if ( modelTopoType_[i] == 0 )            // Node
    return modelNodes_[modelTopoIndex_[i]-1];
  else if ( modelTopoType_[i] > 0 )        // Edge
    return modelEdges_[modelTopoIndex_[i]-1];
  else                                      //Face index is negated...
    return modelFaces_[modelTopoIndex_[i]-1];
}

//-----------------------------------------------------------------------------//
bool
EGTessModel::isEdgeFromVertex(const std::size_t i) const
{
  SANS_ASSERT( i < modelTopoIndex_.size() );
  return modelTopoType_[i] > 0;
}

//-----------------------------------------------------------------------------//
bool
EGTessModel::isEdgeSegment(const std::array<int,3>& tri, const int seg) const
{
  // Get the frame node map for the segment
  const int (*FrameNodes)[ Line::NTrace ] = TraceToCellRefCoord<Line, TopoD2, Triangle>::TraceNodes;

  SANS_ASSERT(seg >= 0 && seg < 3);
  const int segIndex[2] = {tri[FrameNodes[seg][0]], tri[FrameNodes[seg][1]]};

  // If the triangle node index is not within the tessellation, then the segment is not in the tessellation
  // (i.e. it's an interior segment from a mesh generator)
  if ( segIndex[0] >= (int)xyzs_.size() || segIndex[1] >= (int)xyzs_.size() )
    return false;

  int segTopoType[2] = {modelTopoType_[segIndex[0]], modelTopoType_[segIndex[1]]};
  int segTopoIndex[2] = {modelTopoIndex_[segIndex[0]], modelTopoIndex_[segIndex[1]]};

  // One point on a node, other on an edge, and node is part of edge
  if ( segTopoType[0] == 0 && segTopoType[1] > 0 )
  {
    if ( edgeNodesIndex_[segTopoIndex[1]-1][0] == segTopoIndex[0] ||
         edgeNodesIndex_[segTopoIndex[1]-1][1] == segTopoIndex[0] )
    return true;
  }
  // reverse of the previous
  else if ( segTopoType[0] > 0 && segTopoType[1] == 0 )
  {
    if ( edgeNodesIndex_[segTopoIndex[0]-1][0] == segTopoIndex[1] ||
         edgeNodesIndex_[segTopoIndex[0]-1][1] == segTopoIndex[1] )
      return true;
  }
  // Both points on the same edge
  else if ( segTopoType[0] > 0 && segTopoType[1] > 0 && segTopoIndex[0] == segTopoIndex[1] )
    return true;

  // Both points on nodes of an edge
  else if ( segTopoType[0] == 0 && segTopoType[1] == 0 )
  {
    if ( segTopoIndex[0] > segTopoIndex[1] ) std::swap(segTopoIndex[0],segTopoIndex[1]);
    return nodesEdgeIndex_.find({segTopoIndex[0],segTopoIndex[1]}) != nodesEdgeIndex_.end();
  }

  // the segment is not on an edge, i.e at last one vertex is on a Face, or nodes are on two different edges
  return false;
}

//-----------------------------------------------------------------------------//
int
EGTessModel::getEdgeIndexFromSegment(const std::array<int,3>& tri, const int seg) const
{
  // Get the frame node map for the segment
  const int (*FrameNodes)[ Line::NTrace ] = TraceToCellRefCoord<Line, TopoD2, Triangle>::TraceNodes;

  SANS_ASSERT(seg >= 0 && seg < 3);
  const int segIndex[2] = {tri[FrameNodes[seg][0]], tri[FrameNodes[seg][1]]};

  // If the triangle node index is not within the tessellation, then the segment is not in the tessellation
  // (i.e. it's an interior segment from a mesh generator)
  SANS_ASSERT ( segIndex[0] < (int)xyzs_.size() && segIndex[1] < (int)xyzs_.size() );

  int segTopoType[2] = {modelTopoType_[segIndex[0]], modelTopoType_[segIndex[1]]};
  int segTopoIndex[2] = {modelTopoIndex_[segIndex[0]], modelTopoIndex_[segIndex[1]]};

  // One point on a node, other on an edge, this is an edge segment
  if ( segTopoType[0] == 0 && segTopoType[1] > 0 )
  {
    if ( edgeNodesIndex_[segTopoIndex[1]-1][0] == segTopoIndex[0] ||
         edgeNodesIndex_[segTopoIndex[1]-1][1] == segTopoIndex[0] )
      return segTopoIndex[1]-1;
  }
  // Other point on node, and one an edge
  else if ( segTopoType[0] > 0 && segTopoType[1] == 0 )
  {
    if ( edgeNodesIndex_[segTopoIndex[0]-1][0] == segTopoIndex[1] ||
         edgeNodesIndex_[segTopoIndex[0]-1][1] == segTopoIndex[1] )
      return segTopoIndex[0]-1;
  }
  // Both points on an edge
  else if ( segTopoType[0] > 0 && segTopoType[1] > 0 && segTopoIndex[0] == segTopoIndex[1] )
    return segTopoIndex[0]-1;

  // Both points on nodes of an edge
  else if ( segTopoType[0] == 0 && segTopoType[1] == 0 )
  {
    if ( segTopoIndex[0] > segTopoIndex[1] ) std::swap(segTopoIndex[0],segTopoIndex[1]);
    return nodesEdgeIndex_.at({segTopoIndex[0],segTopoIndex[1]});
  }

  // the segment is not on an edge, i.e at last one vertex is on a Face, or nodes are on two different edges
  SANS_ASSERT( false );
  return 0; // Just to suppress compiler warnings
}

//-----------------------------------------------------------------------------//
ego
EGTessModel::getTessEgo(const ego body) const
{
  int state, npts;
  ego tessbody;

  for (std::size_t i = 0; i < tess_.size(); i++)
  {
    EG_STATUS( EG_statusTessBody(tess_[i], &tessbody, &state, &npts) );
    if (tessbody == body) return tess_[i];
  }

  SANS_DEVELOPER_EXCEPTION("Could not find tessellation!");
  return nullptr;
}

//-----------------------------------------------------------------------------//
std::map<UniqueElem,int>
EGTessModel::getUniqueElemMap() const
{
  // This map can retrieve a triangle index on a face given the uniqune numbers of the triangle
  std::map<UniqueElem,int> elemMap;

  for (std::size_t ibody = 0; ibody < tris_.size(); ibody++)
  {
    for (std::size_t iface = 0; iface < tris_[ibody].size(); iface++)
    {
      for (std::size_t itri = 0; itri < tris_[ibody][iface].size(); itri++)
      {
        UniqueElem elemID(eTriangle, tris_[ibody][iface][itri]);
        elemMap[elemID] = itri;
      }
    }
  }

  return elemMap;
}

//-----------------------------------------------------------------------------//
std::map<UniqueElem,std::array<int,3>>
EGTessModel::getUniqueElemTraceNodeMap() const
{
  // This map can retrieve a triangle index on a face given the uniqune numbers of the triangle
  std::map<UniqueElem,std::array<int,3>> elemMap;

  for (std::size_t ibody = 0; ibody < tris_.size(); ibody++)
  {
    for (std::size_t iface = 0; iface < tris_[ibody].size(); iface++)
    {
      for (std::size_t itri = 0; itri < tris_[ibody][iface].size(); itri++)
      {
        UniqueElem elemID(eTriangle, tris_[ibody][iface][itri]);
        elemMap[elemID] = tris_[ibody][iface][itri];
      }
    }
  }

  return elemMap;
}

//-----------------------------------------------------------------------------//
void
EGTessModel::outputTecplot(const std::string& filename) const
{
  std::ofstream file(filename);
  file << std::setprecision(14) << std::scientific;

  std::cout << "Writing " << filename << std::endl;

  file << "VARIABLES = X, Y, Z, u, v, "
          "curv_dir11, "
          "curv_dir12, "
          "curv_dir13, "
          "curv_dir21, "
          "curv_dir22, "
          "curv_dir23, "
          "curvature1, "
          "curvature2, "
          "topoType, "
          "topoIndex"
      << std::endl;


  for (std::size_t ibody = 0; ibody < bodies_.size(); ibody++)
  {
    const EGBody<3>& body = bodies_[ibody];
    std::vector< EGFace<3> > faces = body.getFaces();

    for (std::size_t iface = 0; iface < faces.size(); iface++ )
    {
      int plen, tlen;
      const int *pindex = NULL, *ptype = NULL;
      const double *pxyz = NULL, *puv = NULL;
      const int *ptris = NULL, *ptric = NULL;
      EG_STATUS( EG_getTessFace(tess_[ibody], iface+1, &plen, &pxyz, &puv, &ptype, &pindex,
                                &tlen, &ptris, &ptric) );

      file << "ZONE T=\"Body " << ibody+1 << " Face " << iface+1 << "\" "
           << "N = " << plen << " E = " << tlen << " ET = TRIANGLE F=FEPOINT" << std::endl;

      for (int n = 0; n < plen; n++)
      {
        file << pxyz[3*n+0] << " "
             << pxyz[3*n+1] << " "
             << pxyz[3*n+2] << " "
             << puv[2*n+0] << " "
             << puv[2*n+1] << std::endl;

        double curv_dir11;
        double curv_dir12;
        double curv_dir13;
        double curv_dir21;
        double curv_dir22;
        double curv_dir23;
        double curvature1;
        double curvature2;

        double result[8];

        int status = EG_curvature((ego)faces[iface], puv + 2*n, result);

        if (status == EGADS_DEGEN)
        {
          for (int i = 0; i < 8; i++) result[i] = 0;
        }
        else
          EG_STATUS(status);

        curv_dir11 = result[1];
        curv_dir12 = result[2];
        curv_dir13 = result[3];
        curv_dir21 = result[5];
        curv_dir22 = result[6];
        curv_dir23 = result[7];

        curvature1 = result[0];
        curvature2 = result[4];

        int topoType = ptype[n];
        int topoIndex = pindex[n];

        file << curv_dir11 << " "
             << curv_dir12 << " "
             << curv_dir13 << " "
             << curv_dir21 << " "
             << curv_dir22 << " "
             << curv_dir23 << " "
             << curvature1 << " "
             << curvature2 << " "
             << topoType   << " "
             << topoIndex  << std::endl;
      }

      for (int n = 0; n < tlen; n++)
        file << ptris[3*n+0] << " "
             << ptris[3*n+1] << " "
             << ptris[3*n+2] << std::endl;
    }
  }


  // write out the composite surface
  file << "ZONE T=\"Composite\" "
       << "N = " << xyzs_.size() << " E = " << ntri_ << " ET = TRIANGLE F=FEPOINT" << std::endl;

  int curv_dir11 = 0;
  int curv_dir12 = 0;
  int curv_dir13 = 0;
  int curv_dir21 = 0;
  int curv_dir22 = 0;
  int curv_dir23 = 0;
  int curvature1 = 0;
  int curvature2 = 0;

  for (std::size_t n = 0; n < xyzs_.size(); n++)
  {
    file << xyzs_[n][0] << " "
         << xyzs_[n][1] << " "
         << xyzs_[n][2] << " "
         << uvt_[n][0] << " "
         << uvt_[n][1] << std::endl;

    int topoType = modelTopoType_[n];
    int topoIndex = modelTopoIndex_[n];

    file << curv_dir11 << " "
         << curv_dir12 << " "
         << curv_dir13 << " "
         << curv_dir21 << " "
         << curv_dir22 << " "
         << curv_dir23 << " "
         << curvature1 << " "
         << curvature2 << " "
         << topoType   << " "
         << topoIndex  << std::endl;
  }

  for ( int ibodyTopo = 0; ibodyTopo < EGTessModel::NBODYTOPO; ibodyTopo++ )
    for ( std::size_t ibody = 0; ibody < modelBodyIndex_[ibodyTopo].size(); ibody++ )
    {
      const int bodyIndex = modelBodyIndex_[ibodyTopo][ibody];
      int nface = bodies_[bodyIndex].nFaces();

      for (int iface = 0; iface < nface; iface++)
        for (std::size_t itri = 0; itri < tris_[bodyIndex][iface].size(); itri++)
        {
          file << tris_[bodyIndex][iface][itri][0]+1 << " "
               << tris_[bodyIndex][iface][itri][1]+1 << " "
               << tris_[bodyIndex][iface][itri][2]+1 << std::endl;
        }
    }

}

} //namespace EGADS
} //namespace SANS
