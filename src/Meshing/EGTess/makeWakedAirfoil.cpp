// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "makeWakedAirfoil.h"

#include "tools/SANSnumerics.h"

#include "Meshing/EGADS/EGApproximate.h"
#include "Meshing/EGADS/EGEdge.h"
#include "Meshing/EGADS/EGNode.h"
#include "Meshing/EGADS/EGLoop.h"
#include "Meshing/EGADS/EGPlane.h"

#include "Meshing/EGADS/extrude.h"
#include "Meshing/EGADS/rotate.h"
#include "Meshing/EGADS/solidBoolean.h"
#include "Meshing/EGADS/isSame.h"
#include "Meshing/EGADS/isEquivalent.h"
#include "Meshing/EGADS/clone.h"
#include "Meshing/EGADS/sewFaces.h"

#include "EGTriangle.h"
#include "EGTessModel.h"

#include "WakedFarFieldBox.h"

namespace SANS
{
namespace EGADS
{
EGBody<2>
makeWakedAirfoil( const EGContext& context, const std::string& filename, const Real splinetol,
                  const int nHalfAirfoil, const int nWake, const int nFarField, const Real farfielddistance )
{
  EGApproximate<2> airfoil_spline(context, EGApproximate<2>::Slope, splinetol, filename);

  return makeWakedAirfoil( airfoil_spline, nHalfAirfoil, nWake, nFarField, farfielddistance );
}

EGBody<2>
makeWakedAirfoil( const EGApproximate<2>& airfoil_spline,
                  const int nHalfAirfoil,
                  const int nWake,
                  const int nFarField,
                  const Real farfielddistance )
{
  const EGContext& context = airfoil_spline.getContext();

  double xle = 0;
  double xte = 1;

  // Assume the airfoil t-space between 0 and 1 and has a closed trailing edge
  EGNode<2> LE(context, {xle,0});
  EGNode<2> TE(context, {xte,0});

  Real LEt = airfoil_spline(LE);

  //Create the upper and lower edges on the airfoil
  EGEdge<2> Upper(airfoil_spline, {0,LEt}, TE, LE);
  EGEdge<2> Lower(airfoil_spline, {LEt,1}, LE, TE);

  // Create all the far field nodes
  EGNode<2> FF00(context, {-farfielddistance+0.5, -farfielddistance});
  EGNode<2> FF10(context, { farfielddistance+0.5, -farfielddistance});
  EGNode<2> FF11(context, { farfielddistance+0.5,  farfielddistance});
  EGNode<2> FF01(context, {-farfielddistance+0.5,  farfielddistance});
  EGNode<2> FFWK(context, { farfielddistance+0.5,  0               });

  // Create the far field edges and the wake edge
  EGEdge<2> edge0(FF00, FF10);
  EGEdge<2> edge1(FF10, FFWK);
  EGEdge<2> edgeWK(TE, FFWK);
  //Lower
  //Upper
  //FFWK
  EGEdge<2> edge6(FFWK, FF11);
  EGEdge<2> edge7(FF11, FF01);
  EGEdge<2> edge8(FF01, FF00);

  edgeWK.addAttribute(EGTriangle::WAKEEDGE, 1);

  //Add a cosine distribution of nodes to the airfoil
  std::vector<double> r(nHalfAirfoil-2);
  for ( int i = 1; i < nHalfAirfoil-1; i++ )
    r[i-1] = 1-0.5*(1+cos(PI*i/Real(nHalfAirfoil-1)));

  Upper.addAttribute(".rPos",r);
  Lower.addAttribute(".rPos",r);


#if 1
  // stagnation line: quadratic increments w/ dx1 and dx2 matching airfoil
  double x1 = r[r.size() - 1];
  double x2 = r[r.size() - 2];

  double dx1 = xte - x1;
  double dx2 = x1 - x2;

  double rffd = farfielddistance-0.5;
  const int iiwake = max(nWake,3);

  double a = (2*((rffd + (xle+xte)/2) - xte) + iiwake*((iiwake - 3)*dx1 - (iiwake - 1)*dx2)) / ((double) (2*iiwake*(2 - iiwake*(3 - iiwake))));
  double b = 0.5*(dx2 - dx1) - 3*a;
  double c = 0.5*(3*dx1 - dx2) + 2*a;

  int iim = iiwake-1;
  std::vector<double> rwk(iiwake-2);
  for (int i = 1; i < iiwake-1; i++)
    rwk[i-1] = i*(c + i*(b + i*a))/(iim*(c + iim*(b + iim*a)));
#else
  //const int iiwake = max(farfielddistance*nHalfAirfoil,3.);
  const int iiwake = max(nFarField/2+1,3);
  std::vector<double> rwk(iiwake-2);
  for ( int i = 1; i < iiwake-1; i++ )
    rwk[i-1] = i/Real(iiwake-1);
#endif

  edgeWK.addAttribute(".rPos", rwk);
  edgeWK.addAttribute(EGTriangle::INTERNALEDGE, 1);


  //Set a uniform distrbution on the far field
  const int nff = max(nFarField,3);
  std::vector<double> rff(nff-2);
  for ( int i = 1; i < nff-1; i++ )
    rff[i-1] = i/Real(nff-1);

  //Set a uniform distrbution on the far field outflow boundary
  const int nffwk = max(nFarField/2+1,3);
  std::vector<double> rffwk(nffwk-2);
  for ( int i = 1; i < nffwk-1; i++ )
    rffwk[i-1] = i/Real(nffwk-1);

  // Set the far field node count
  edge0.addAttribute(".rPos", rff);
  edge1.addAttribute(".rPos", rffwk);
  edge6.addAttribute(".rPos", rffwk);
  edge7.addAttribute(".rPos", rff);
  edge8.addAttribute(".rPos", rff);


  EGLoop<2> loop( context, {(edge0,1),(edge1,1),(edgeWK,1),(Lower,-1),(Upper,-1),(edgeWK,1),(edge6,1),(edge7,1),(edge8,1)}, CLOSED );

  EGBody<2> facebody(context, (loop,1) );

  facebody.getFaces()[0].addAttribute(".tParams", {2.*farfielddistance/(nFarField-1),0.01,33.});
  //facebody.addAttribute(".tParams", {2.*farfielddistance/(nFarField-1),0.01,15.});

  //facebody.getFaces()[0].addAttribute(".tParams", {2.*farfielddistance,0.01,15.});
  //facebody.addAttribute(".tParams", {2.*farfielddistance,0.01,15.});

  return facebody;
}


EGModel<3>
makeWakedAirfoil( const EGApproximate<3>& airfoil_spline,
                  std::vector<int>& TrefftzFrames,
                  Real span,
                  const std::vector<double>& boxsize,
                  const int nChord,
                  const int nSpan,
                  const int nWake,
                  const std::vector<double>& outflowParams,
                  bool withWake )
{
  SANS_ASSERT( outflowParams.size() == 3 );
  SANS_ASSERT( boxsize.size() == 6 );

  const EGContext& context = airfoil_spline.getContext();

  double xle = 0;
  double xte = 1;

  // Assume the airfoil spance between 0 and 1 and has a closed trailing edge
  EGNode<3> LE(context, {xle,0,0});
  EGNode<3> TE(context, {xte,0,0});

  Real LEt = airfoil_spline(LE);

  //Create the upper and lower edges on the airfoil
  EGEdge<3> Upper(airfoil_spline, {0,LEt}, TE, LE);
  EGEdge<3> Lower(airfoil_spline, {LEt,1}, LE, TE);

  EGLoop<3> airfoil_loop(context, {(Upper,1),(Lower,1)}, CLOSED );
  EGPlane<3> plane(context, {0,0,0}, {1,0,0}, {0,1,0});
  EGFace<3> airfoil_Face( plane, (airfoil_loop,1) );
  EGBody<3> airfoil_body( airfoil_Face );

#if 1

  EGBody<3> Wing0 = extrude(airfoil_body, span, {0.,0.,1.});

  std::vector< EGFace<3> > wing0faces = Wing0.getFaces();
  wing0faces[0].addAttribute("wingface",1);
  wing0faces[1].addAttribute("wingface",1);

  //EGBody<3> Wing1 = extrude( EGBody<3>(wing0faces[3]), span/2., {0.,0.,1.});

  //EGBody<3> Wing2 = fuse(Wing0, Wing1).getBodies()[0].clone();

  // Rotate so z is up
  EGBody<3> Wing = Wing0.clone(90., {1.,0.,0.});
  //EGModel<3>(Wing).save("tmp/test.egads");

#else
  //EGEdge<3> split_edge( Upper.getNodes() );
  //EGLoop<3> airfoil_split_loop(context, {(Upper,1),(split_edge,1)}, CLOSED );
  //EGFace<3> airfoil_split_Face( plane, (airfoil_split_loop,1) );

  EGBody<3> Tip0 = rotate( airfoil_Face, 180., {0.,0.,0.}, {1.,0.,0.} );
  EGBody<3> Tip1 = Tip0.clone({0.,0.,span});

  EGBody<3> Wing0 = extrude(airfoil_body, span, {0.,0.,1.});

  //EGModel<3>({Tip0, Tip1, Wing0}).save("tmp/test.egads");
  //fuse(Tip1, Wing0).save("tmp/test.egads");

  //EGBody<3> Wing = fuse(Tip1, Wing0).getBodies()[0].clone();

  std::vector< EGFace<3> > tip0faces = Tip0.getFaces();
  std::vector< EGFace<3> > tip1faces = Tip1.getFaces();
  std::vector< EGFace<3> > wing0faces = Wing0.getFaces();

  wing0faces[0].addAttribute("wingface",1);
  wing0faces[1].addAttribute("wingface",1);

  std::vector< EGFace<3> > wingSewfaces = {tip0faces[1], tip1faces[0], wing0faces[0], wing0faces[1]};

  EGBody<3> Wing1 = sewFaces(wingSewfaces, 1e-7, sewFaceFlags::Manifold).getBodies()[0].clone();

  EGPlane<3> planecutter(context, {0.5,0.,0.}, {0,1,0}, {0,0,1});
  EGFace<3> tip0cutter(planecutter, {-1,1}, {-1,0.01});
  EGFace<3> tip1cutter(planecutter, {-1,1}, {span-0.01,span+1});

  EGBody<3> Wing2( EGIntersection<3>(Wing1, EGBody<3>(tip0cutter)) );
  EGBody<3> Wing3( EGIntersection<3>(Wing2, EGBody<3>(tip1cutter)) );

  // Rotate so z is up
  EGBody<3> Wing = Wing3.clone(90., {1.,0.,0.});
  //EGModel<3>(Wing).save("tmp/test.egads");

#endif

  /*
  Real wingSize = Wing.getSize();
  for (int n = 0; n < 2; n++ )
  {
    airfoilParams[n] *= wingSize;
    wakeParams[n] *= wingSize;
  }
*/

  //std::vector<double> airfoilParams = {1./(nChord-1), 0.001, 10};
  std::vector<double> airfoilParams = {1./(nChord-1), 0.001, 15};
  //Increase the tess resolution on the wing and lable the faces as walls
  std::vector< EGFace<3> > wingfaces = Wing.getFaces();
  for ( auto face = wingfaces.begin(); face != wingfaces.end(); face++ )
  {
    face->addAttribute(".tParams", airfoilParams);
    //if (face->hasAttribute("wingface"))
    //face->addAttribute(".qParams", "junk");
    //face->addAttribute(EGTessModel::TRIANGLE, 33.);
    face->addAttribute("BCName", "Wall");
  }
  // Set triangle to the end caps
  //wingfaces[wingfaces.size()-2].addAttribute(EGTessModel::TRIANGLE, 33.);
  //wingfaces[wingfaces.size()-1].addAttribute(EGTessModel::TRIANGLE, 33.);

#if 0
  std::vector<double> wingbbox = Wing.getBoundingBox();

  DLA::VectorS<3,Real> dbox = {wingbbox[3]-wingbbox[0], wingbbox[4]-wingbbox[1], wingbbox[5]-wingbbox[2]};
  DLA::VectorS<3,Real> size = {FarDist[0]*dbox[0],FarDist[1]*dbox[1],FarDist[2]*dbox[2]};
  DLA::VectorS<3,Real> centroid = {wingbbox[0], wingbbox[1], wingbbox[2]};
  centroid += dbox/2;

  std::vector<double> boxsize = {centroid[0]-size[0], centroid[1]-size[1], centroid[2]-size[2],
                                 2*size[0], 2*size[1], 2*size[2]};
#endif
  DLA::VectorS<3,Real> size = {boxsize[3],boxsize[4],boxsize[5]};


#if 1
  //const int nSpan = std::max(AR/2/airfoilParams[0],2.);
  std::vector<double> rSpan(nSpan-2);
  for ( int i = 1; i < nSpan-1; i++ )
    //rSpan[i-1] = i/Real(nSpan-1);
    rSpan[i-1] = 1-0.5*(1+cos(PI*i/Real(nSpan-1)));
#endif
#if 1
  //const int nChord = std::max(1./airfoilParams[0],2.);
  std::vector<double> rChord(nChord-2);
  for ( int i = 1; i < nChord-1; i++ )
    //rChord[i-1] = i/Real(nChord-1);
    rChord[i-1] = 1-0.5*(1+cos(PI*i/Real(nChord-1)));
#endif

#if 1
  // stagnation line: quadratic increments w/ dx1 and dx2 matching airfoil
  double x1 = rChord[rChord.size() - 1];
  double x2 = rChord[rChord.size() - 2];

  double dx1 = xte - x1;
  double dx2 = x1 - x2;

  double rffd = size[0] - xte/2.;
  const int iiwake = max(nWake,3);
  //const int iiwake = max(rffd/4.*nChord,3.);

  double a = (2*((rffd + (xle+xte)/2) - xte) + iiwake*((iiwake - 3)*dx1 - (iiwake - 1)*dx2)) / ((double) (2*iiwake*(2 - iiwake*(3 - iiwake))));
  double b = 0.5*(dx2 - dx1) - 3*a;
  double c = 0.5*(3*dx1 - dx2) + 2*a;

  int iim = iiwake-1;
  std::vector<double> rwk(iiwake-2);
  for (int i = 1; i < iiwake-1; i++)
    rwk[i-1] = i*(c + i*(b + i*a))/(iim*(c + iim*(b + iim*a)));
#elif 1
  //const int iiwake = max(farfielddistance*nHalfAirfoil,3.);
  const int iiwake = max(nWake,3);
  std::vector<double> rwk(iiwake-2);
  for ( int i = 1; i < iiwake-1; i++ )
    rwk[i-1] = i/Real(iiwake-1);
#elif 0
  const int iiwake = max(nWake,3);
  std::vector<double> rwk(iiwake-2);
  for ( int i = 1; i < iiwake-1; i++ )
    rwk[i-1] = 1-0.5*(1+cos(PI*i/Real(iiwake-1)));
#endif

  //Find the trailing edge
  typedef EGNode<3>::CartCoord CartCoord;
  std::vector< EGEdge<3> > WingEdges = Wing.getEdges();
  EGEdge<3> edgeTE = WingEdges[0];
  for ( auto edge = WingEdges.begin(); edge != WingEdges.end(); edge++ )
  {
    std::vector<EGNode<3>> nodes = edge->getNodes();

    if (nodes.size() == 1 ) continue;

    if ( ((CartCoord)nodes[0])[0] > 0.9 && ((CartCoord)nodes[0])[0] < 1.1 &&
         ((CartCoord)nodes[1])[0] > 0.9 && ((CartCoord)nodes[1])[0] < 1.1 )
    {
      edgeTE = *edge;
      edgeTE.addAttribute(".rPos", rSpan); //Trailing Edge
    }
    else if ( ((CartCoord)nodes[0])[0] > -0.1 && ((CartCoord)nodes[0])[0] < 0.1 &&
              ((CartCoord)nodes[1])[0] > -0.1 && ((CartCoord)nodes[1])[0] < 0.1 )
    {
      edge->addAttribute(".rPos", rSpan); //Leading Edge
    }
    else if ( fabs( ((CartCoord)nodes[0])[2] - ((CartCoord)nodes[1])[2] ) < 0.1 )
    {
      edge->addAttribute(".rPos", rChord); //Side edges
    }
  }

  //std::vector< EGNode<3> > TEnodes = edgeTE.getNodes();

  //int nodeL = 1;
  //int nodeR = 0;

  //if ( ((CartCoord)TEnodes[nodeL])[2] < ((CartCoord)TEnodes[nodeR])[2] )
  //{
  //  nodeL = 0;
  //  nodeR = 1;
  //}

  Real len = 2*size[0];
  DLA::VectorS<3,Real> dir = {1, 0.0, 0};

  //Nodes outside the box
  //EGNode<3> TEnodeR(context, {1.0, 0.0, -2*size[2]});
  //EGNode<3> TEnodeL(context, {1.0, 0.0,  2*size[2]});

  //Edges that extend from the wing tips outside the box
  //EGEdge<3> edgeTipL (TEnodes[nodeL], TEnodeL);
  //EGEdge<3> edgeTipR (TEnodeR, TEnodes[nodeR]);

  //EGLoop<3>::edge_vector wakeEdges;
  //wakeEdges = {(edgeTipL,1),(edgeTE,1),(edgeTipR,1)};

  // Surface used to create the wake and cut the farfield
  //EGBody<3> wakecutter = extrude( EGLoop<3>(context, wakeEdges, OPEN), len, dir );

  // The actual wake, but extruded way too long
  std::cout << "WingEdges size " << WingEdges.size() << std::endl;
  //EGBody<3> wakelong = extrude( EGLoop<3>(context, {(WingEdges[0],1), (WingEdges[6],1)}, OPEN), len, dir );
  EGBody<3> wakelong = extrude( EGLoop<3>( (edgeTE,1), OPEN ), len, dir );
  EGBody<3> wakelong2 = wakelong.clone();

  //EGModel<3>({Wing,wakelong}).save("tmp/test.egads");
  //EGBody<3> wingboxscribed = subtract(box, Wing).getBodies()[0].clone();

#if 0
  EGBody<3> wingbox = subtract(box, Wing).getBodies()[0].clone();

  // Create a 'fuselage'
  EGCircle<3> circle_geom(context, {0.25, 0., span/2}, {0., 1., 0.}, {0., 0., 1.}, 0.05);
  EGPlane<3> circle_plane(context, {0.25, 0., span/2}, {0., 1., 0.}, {0., 0., 1.});
  EGEdge<3> circle_edge( circle_geom );
  EGLoop<3> circle_loop( (circle_edge,1), CLOSED );
  EGFace<3> circle_face(circle_plane, (circle_loop,1));

  EGBody<3> fuse = extrude(circle_face, 100, {1,0,0});

  // Set grid parameter on the 'fuselage'
  std::vector<double> fuseParams = {4./(nChord-1), 0.1, 20.};
  std::vector< EGFace<3> > fusefaces = fuse.getFaces();
  for ( auto face = fusefaces.begin(); face != fusefaces.end(); face++ )
  {
    face->addAttribute(".tParam", fuseParams);
    face->addAttribute("BCName", "Wall");
    face->addAttribute(".qParams", "junk");
  }

  EGBody<3> wingboxfuse = subtract(wingbox, fuse).getBodies()[0].clone();
  //EGBody<3> wingboxscribed( EGIntersection<3>(wingboxfuse, wakecutter) );
  EGBody<3> wingboxscribed( EGIntersection<3>(wingboxfuse, wakelong) );

  Wing = EGBody<3>( EGIntersection<3>(Wing, wakelong) );

  //EGBody<3> wingboxscribed = subtract(wingbox, fuse).getBodies()[0].clone();
  std::vector< EGBody<3> > wakes = clone( intersect( EGModel<3>(wakelong), wingboxscribed ).getBodies() );
#else
  std::vector< EGBody<3> > wakes = {wakelong};
#endif

  std::vector<double> wakeParams = {1./(nChord-1), 0.001, 15};
  //std::vector<double> wakeParams = {span/(nSpan-1), 0.001, 15};
  //Set the tess resolution on the wake
  for (std::size_t i = 0; i < wakes.size(); i++)
  {
    wakes[i].addAttribute("Wake", "Wing_Wake");
    wakes[i].addAttribute("BCName", "Wing_Wake");
    std::vector< EGFace<3> > wakefaces = wakes[i].getFaces();
    for ( auto face = wakefaces.begin(); face != wakefaces.end(); face++ )
    {
      //face->addAttribute(".tParams", wakeParams);
      //face->addAttribute(EGTessModel::TRIANGLE, 33.);
      //face->addAttribute(".qParams", "junk");
      if (withWake)
      {
        face->addAttribute("BCName", "Wing_Wake");
        //face->addAttribute("Wake", "Wing_Wake");
      }
    }
  }


  std::vector< EGEdge<3> > wingEdges = Wing.getEdges();

  for (std::size_t i = 0; i < wakes.size(); i++)
  {
    std::vector< EGEdge<3> > WakeEdges = wakes[i].getEdges();

    for ( auto edge = WakeEdges.begin(); edge != WakeEdges.end(); edge++ )
    {
      std::vector<EGNode<3>> nodes = edge->getNodes();

      if (nodes.size() == 1 ) continue;

      if ( (((CartCoord)nodes[0])[0] > 0.9 && ((CartCoord)nodes[0])[0] < 1.1) &&
           (((CartCoord)nodes[1])[0] > 0.9 && ((CartCoord)nodes[1])[0] < 1.1) )
      {
        //if (withWake) edge->addAttribute("Kutta", 2); // Mark the edge that is used for Kutta condition integrals
        for ( auto sedge = wingEdges.begin(); sedge != wingEdges.end(); sedge++ )
        {
          if (isSame(*edge,*sedge))
          {
            if (withWake) sedge->addAttribute("Wake", "Wing_Wake"); // Mark the edge that is used for Kutta condition integrals
             edge->addAttribute(".rPos", rSpan); //Trailing Edge
            sedge->addAttribute(".rPos", rSpan); //Trailing Edge
          }
        }
      }
    }
  }



  // Construct small fences to ensure that no tet connects the wing to the wake directly
#if 0
  EGPlane<3> vertplane(context, {1,0,0}, {0,1,0}, {0,0,1});

  EGNode<3> FenceNodeUL(context, { 1,max(dx1,0.06), span} );
  EGNode<3> FenceNodeUR(context, { 1,max(dx1,0.06),    0} );

  EGEdge<3> FenceEdgeU (FenceNodeUL, FenceNodeUR );
  EGEdge<3> FenceEdgeUL(TEnodes[nodeL], FenceNodeUL );
  EGEdge<3> FenceEdgeUR(FenceNodeUR, TEnodes[nodeR]);

  FenceEdgeU.addAttribute(".rPos", rSpan); //Span

  EGLoop<3> FenceU_loop(context, {(edgeTE,1),(FenceEdgeUL,1),(FenceEdgeU,1),(FenceEdgeUR,1)}, CLOSED );
  EGFace<3> FenceU_face( vertplane, (FenceU_loop,1) );
  EGBody<3> FenceU_body( FenceU_face );

  EGNode<3> FenceNodeLL(context, { 1,-max(dx1,0.06), span} );
  EGNode<3> FenceNodeLR(context, { 1,-max(dx1,0.06),    0} );

  EGEdge<3> FenceEdgeL (FenceNodeLL, FenceNodeLR );
  EGEdge<3> FenceEdgeLL(TEnodes[nodeL], FenceNodeLL );
  EGEdge<3> FenceEdgeLR(FenceNodeLR, TEnodes[nodeR]);

  FenceEdgeL.addAttribute(".rPos", rSpan); //Span

  EGLoop<3> FenceL_loop(context, {(edgeTE,1),(FenceEdgeLL,1),(FenceEdgeL,1),(FenceEdgeLR,1)}, CLOSED );
  EGFace<3> FenceL_face( vertplane, (FenceL_loop,1) );
  EGBody<3> FenceL_body( FenceL_face );

#if 1
  wakes.push_back(wingboxscribed);
  wakes.push_back(FenceU_body);
  wakes.push_back(FenceL_body);
  return EGModel<3>(wakes);
#else

  // Scribe the fences onto the fuselage
  EGBody<3> wingboxscribed1( EGIntersection<3>(wingboxscribed , FenceU_body) );
  EGBody<3> wingboxscribed2( EGIntersection<3>(wingboxscribed1, FenceL_body) );


  std::vector< EGBody<3> > FenceU_clone = clone( intersect( EGModel<3>(FenceU_body), wingboxscribed2 ).getBodies() );

  //FenceU_clone[0].getEdges()[4-1].addAttribute(".rPos", rSpan); //Span
  //FenceU_clone[1].getEdges()[4-1].addAttribute(".rPos", rSpan); //Span

  std::vector< EGBody<3> > FenceL_clone = clone( intersect( EGModel<3>(FenceL_body), wingboxscribed2 ).getBodies() );

  //FenceL_clone[0].getEdges()[2-1].addAttribute(".rPos", rSpan); //Span
  //FenceL_clone[1].getEdges()[2-1].addAttribute(".rPos", rSpan); //Span


  wakes.push_back(wingboxscribed2);
  wakes.insert(wakes.end(), FenceU_clone.begin(), FenceU_clone.end());
  wakes.insert(wakes.end(), FenceL_clone.begin(), FenceL_clone.end());
  return EGModel<3>(wakes);
#endif
#else

  wakes.push_back(Wing);
  EGModel<3> boxedModel = EGModel<3>( WakedFarFieldBox(wakes, boxsize, TrefftzFrames) );

#if 1
  {
    std::vector<EGBody<3>> bodies = boxedModel.getBodies();
    for (std::size_t i = 0; i < bodies.size(); i++)
    {
      if ( !bodies[i].isSolid() )
      {
        std::vector< EGEdge<3> > WakeEdges = bodies[i].getEdges();

        for ( auto edge = WakeEdges.begin(); edge != WakeEdges.end(); edge++ )
        {
          if (edge->hasAttribute("Trefftz"))
          {
            edge->addAttribute(".rPos", rSpan);
          }

          std::vector<EGNode<3>> nodes = edge->getNodes();
          if (nodes.size() == 1 ) continue;

          if ( fabs( ((CartCoord)nodes[0])[1] - ((CartCoord)nodes[1])[1] ) < 0.1 )
          {
            edge->addAttribute(".rPos", rwk); //Side edges
          }
        }
      }
    }
  }
#endif

  return boxedModel;
#endif
}

}
}
