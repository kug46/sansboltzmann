// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EG_TRIANGLE_H
#define EG_TRIANGLE_H

//Generates an area triangulation grid given an EGADS body using Triangle (https://www.cs.cmu.edu/~quake/triangle.html)

#include "Field/XFieldArea.h"
#include "Meshing/EGADS/EGBody.h"

#include <map>

// forward declaration of ego
struct egObject;
typedef struct egObject* ego;

//Forward declare
extern "C" { struct triangulateio; }

namespace SANS
{
namespace EGADS
{

class EGTriangle : public XField<PhysD2, TopoD2>
{
public:

  static const char* INTERNALEDGE; //Used to mark that an EGADS edges is an internal edge of the domain
  static const char* WAKEEDGE;     //Used to mark that an EGADS edges is an wake edge for full-potential

  static const int Dim = PhysD2::D;
  typedef XField<PhysD2, TopoD2> BaseType;
  typedef BaseType::VectorX CartCoord;

  EGTriangle( const EGTriangle& fld, const FieldCopy& tag ) : BaseType(fld, tag) {}

  //Constructor based on an EGADS body
  explicit EGTriangle( const EGBody<Dim>& body );

  CartCoord& X(const int n) { return DOF_[n]; }

  ego vertexGeometry( const int k );

private:
  std::map<int,ego> marker2ego_;
  std::vector<int> pointmarkerlist_;
};

}
}

#endif //EG_TRIANGLE_H
