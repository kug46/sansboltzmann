// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EG_VERTEX_H
#define EG_VERTEX_H

//A vertex in a tessellation

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

namespace SANS
{
namespace EGADS
{

template<int Dim_>
class EGVertex
{
public:
  static const int Dim = Dim_;
  typedef DLA::VectorS<Dim, Real> CartCoord;

  EGVertex( const EGVertex& V ) : X_(V.X_), fixed_(V.fixed_) {}
  EGVertex& operator=( const EGVertex& V ) { X_ = V.X_; fixed_ = V.fixed_; return *this; }
  EGVertex( CartCoord* X, const bool fixed ) : X_(X), fixed_(fixed) {}
  virtual ~EGVertex();

  virtual const std::type_info& typeID() const = 0;

  operator       CartCoord&()       { return *X_; }
  operator const CartCoord&() const { return *X_; }

  Real operator[](const int n) const { return (*X_)[n]; }

  CartCoord* getVertex_ptr() const { return X_; }

  //Determines if the vertex is fixed (can't move)
  bool fixed() const { return fixed_; }
protected:
  CartCoord* X_;
  bool fixed_;
};

}
}

#endif //EG_VERTEX_H
