// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EG_TESSFACE_H
#define EG_TESSFACE_H

//Represents a tessellated edge

#include "Field/XFieldArea.h"
#include "Meshing/EGADS/EGFace.h"
#include "EGVertexFace.h"
#include "EGTessEdge.h"

#include "Topology/ElementTopology.h"

namespace SANS
{
namespace EGADS
{

template<int Dim_>
class EGTessFace
{
public:
  static const int Dim = Dim_;
  typedef DLA::VectorS<Dim, Real> CartCoord;
  typedef DLA::VectorS<2, Real> ParamCoord;
  typedef EGVertexFace<Dim> VertexType;

  EGTessFace(const EGTessFace& tf);

  //Constructor for extracting the tessellation on edges from
  //a body tessellation
  EGTessFace(const EGBody<Dim>& body, const EGFace<Dim>& face, const int fIndexL,
             const int nVertex, const double *puv,
             CartCoord* X,
             const int *ptype, const int *pindex,
             const int *ntriface, const int *ptris, const int *ptric,
             std::vector<int>& nodeMap,
             std::vector< XField<PhysD2, TopoD2>::FieldCellGroupType<Triangle>::FieldAssociativityConstructorType >& xfldArea,
             XField<PhysD2, TopoD2>::FieldTraceGroupType<Line>::FieldAssociativityConstructorType& xfldIedge,
             std::vector< EGTessEdge<Dim> >& tessEdges);


  EGTessFace& operator=(const EGTessFace& tf);

  int nVertex() const { return static_cast<int>(Vertexes_.size()); }
  int nElem() const { return ntri_; }

  int nFreeParameters() const { return nFreeVertex_*2; } //2 parameters for faces
  int nFreeVertex() const { return nFreeVertex_; } //Number of free vertexes

  //Populates with the current uv values
  void fillFreeParameters(std::vector<double>& x) const;

  //Updates the movable vertexes at the new uv locations
  void evaluate(const int n, const double* puv);

  VertexType& getVertex(const int n) { return Vertexes_[n]; }
  VertexType& getFreeVertex(const int n) { return Vertexes_[n]; }

  std::vector<double> lowerParamBound() const;
  std::vector<double> upperParamBound() const;

  EGFace<Dim>& face() { return face_; }
  EGFace<Dim>& getTopo() { return face_; }

protected:
  EGFace<Dim> face_;
  int ntri_;
  int nFreeVertex_;
  std::vector< const EGTessEdge<Dim>* > tessEdges_;
  std::vector< VertexType > Vertexes_;
};

}
}

#endif //EG_TESSFACE_H
