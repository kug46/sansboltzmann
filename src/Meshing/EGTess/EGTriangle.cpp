// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "EGTriangle.h"

#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Field/output_Tecplot.h"

#include <algorithm> // std::sort

#define VOID void
#define ANSI_DECLARATORS
#define TRILIBRARY
#define REAL double
extern "C"
{
#include <triangle.h>
}

#include <egads.h>

namespace SANS
{
namespace EGADS
{


const char* EGTriangle::INTERNALEDGE = ".internal";
const char* EGTriangle::WAKEEDGE = "Wake";

namespace //private to this file
{
void centroid( double* pointlist, std::vector<int>& nodes, double centroid[2] )
{
  centroid[0] = centroid[1] = 0;
  for ( std::size_t i = 0; i < nodes.size(); i++ )
  {
    centroid[0] += pointlist[2*nodes[i]+0];
    centroid[1] += pointlist[2*nodes[i]+1];
  }

  centroid[0] /= nodes.size();
  centroid[1] /= nodes.size();
}

void edgeNormal( double* pointlist, std::vector<int>& edge, double normal[2] )
{
  double tx, ty;

  tx = pointlist[2*edge[1]+0]-pointlist[2*edge[0]+0];
  ty = pointlist[2*edge[1]+1]-pointlist[2*edge[0]+1];

  normal[0] =  ty;
  normal[1] = -tx;
}

void centroid( DLA::VectorS<2,Real>* DOF, std::vector<int>& nodes, double centroid[2] )
{
  centroid[0] = centroid[1] = 0;
  for ( std::size_t i = 0; i < nodes.size(); i++ )
  {
    centroid[0] += DOF[nodes[i]][0];
    centroid[1] += DOF[nodes[i]][1];
  }

  centroid[0] /= nodes.size();
  centroid[1] /= nodes.size();
}

void edgeNormal( DLA::VectorS<2,Real>* DOF, std::vector<int>& edge, double normal[2] )
{
  double tx, ty;

  tx = DOF[edge[1]][0]-DOF[edge[0]][0];
  ty = DOF[edge[1]][1]-DOF[edge[0]][1];

  normal[0] =  ty;
  normal[1] = -tx;
}

double dot( double v1[2], double v2[2] )
{
  return v1[0]*v2[0] + v1[1]*v2[1];
}

int nEdgePoints( const EGEdge<2>& edge )
{
  // Get the point count. tPos and rPos are vertex locations excluding end node(s).
  // So the point count is .tPos or .rPos excluding the nodes
  try
  {
    return edge.getAttributeSize(".tPos");
  }
  catch (SANSException& e)
  {
    return edge.getAttributeSize(".rPos");
  }
}

//Returns the edge number based on the point markers of a triangle segment. Edge number of 0 indicates an interior segment
int segmentEdgeNumber( const std::vector<int>& pointmarkerlist, const std::vector<int>& tri, const int seg )
{
  const int (*TraceNodes)[3][2] = &TraceToCellRefCoord<Line, TopoD2, Triangle>::TraceNodes;

  const int trimark[3] = {pointmarkerlist[tri[0]],
                          pointmarkerlist[tri[1]],
                          pointmarkerlist[tri[2]]};

  // Assume interior segment
  int edge = 0;

#if 0
  // One point on a node, other on an edge
  if ( trimark[(*TraceNodes)[seg][0]] == -1 && trimark[(*TraceNodes)[seg][1]] > 0)
    edge = trimark[(*TraceNodes)[seg][1]];

  // Other point on node, and one an edge
  else if ( trimark[(*TraceNodes)[seg][0]] > 0 && trimark[(*TraceNodes)[seg][1]] == -1)
    edge = trimark[(*TraceNodes)[seg][0]];
#else
  // One point on a node, other on an edge
  if ( trimark[(*TraceNodes)[seg][0]] < 0 && trimark[(*TraceNodes)[seg][1]] > 0)
    edge = trimark[(*TraceNodes)[seg][1]];

  // Other point on node, and one an edge
  else if ( trimark[(*TraceNodes)[seg][0]] > 0 && trimark[(*TraceNodes)[seg][1]] < 0)
    edge = trimark[(*TraceNodes)[seg][0]];
#endif

  // Both points on an edge (or interior)
  else if ( trimark[(*TraceNodes)[seg][0]] == trimark[(*TraceNodes)[seg][1]] )
    edge = trimark[(*TraceNodes)[seg][0]];

  return edge;
}

bool sameSegment( const std::vector<int>& triL, const int segL, const std::vector<int>& triR, const int segR )
{
  const int (*TraceNodes)[3][2] = &TraceToCellRefCoord<Line, TopoD2, Triangle>::TraceNodes;

  // Check if the point indexes of triL[segL] matches the indexes of triR[segR]
  return (triL[(*TraceNodes)[segL][0]] == triR[(*TraceNodes)[segR][0]] &&
          triL[(*TraceNodes)[segL][1]] == triR[(*TraceNodes)[segR][1]])
          ||
         (triL[(*TraceNodes)[segL][0]] == triR[(*TraceNodes)[segR][1]] &&
          triL[(*TraceNodes)[segL][1]] == triR[(*TraceNodes)[segR][0]]);
}

// Updates the nodes of triangles due to node duplication
void updateTriangleNodes( triangulateio& out, DLA::VectorS<2,Real>* DOF, const std::vector<int>& newPointMap,
                          const int iTriL, const int iTriR, double normal[2], const int point )
{

  bool updateneighbor = false;

  std::vector<int> tri = {out.trianglelist[3*iTriR+0],
                          out.trianglelist[3*iTriR+1],
                          out.trianglelist[3*iTriR+2]};

  double centroidT[2], centroidVector[2];
  centroid(DOF, tri, centroidT);

  // Create a vector from the point to the right triangle centroid
  centroidVector[0] = centroidT[0] - DOF[point][0];
  centroidVector[1] = centroidT[1] - DOF[point][1];

  if ( dot( centroidVector, normal ) > 0 )
    for ( int n = 0; n < 3; n++ )
      if ( tri[n] == point  )
      {
        out.trianglelist[3*iTriR + n] = newPointMap[tri[n]];
        updateneighbor = true;
      }

  if ( updateneighbor )
  {
    // Update the points on neighboring triangles
    for (int side = 0; side < 3; side++ )
    {
      const int iTriN = out.neighborlist[3*iTriR + side];
      if (iTriN == iTriL || iTriN < 0) continue; //Don't modify the left triangle or step out of bounds on a boundary

      updateTriangleNodes( out, DOF, newPointMap, iTriR, iTriN, normal, point );
    }
  }

}


std::vector<double> edgeParamCoord( const EGEdge<2>& edge )
{
  std::vector<double> t;
  try
  {
    edge.getAttribute(".tPos", t);
  }
  catch (SANSException& e)
  {
    EGEdge<2>::ParamRange tlimit = edge.getParamRange();
    std::vector<double> r;
    edge.getAttribute(".rPos", r);
    t.resize(r.size());

    //Only need t values in between nodes
    for (int i = 0; i < (int)r.size(); i++)
      t[i] = tlimit[0] + r[i]*(tlimit[1]-tlimit[0]);
  }
  return t;
}

void triangulateioInit( triangulateio& io )
{

  io.pointlist = NULL;
  io.pointattributelist = NULL;
  io.pointmarkerlist = NULL;
  io.numberofpoints = 0;
  io.numberofpointattributes = 0;

  io.trianglelist = NULL;
  io.triangleattributelist = NULL;
  io.trianglearealist = NULL;
  io.neighborlist = NULL;
  io.numberoftriangles = 0;
  io.numberofcorners = 0;
  io.numberoftriangleattributes = 0;

  io.segmentlist = NULL;
  io.segmentmarkerlist = NULL;
  io.numberofsegments = 0;

  io.holelist = NULL;
  io.numberofholes = 0;

  io.regionlist = NULL;
  io.numberofregions = 0;

  io.edgelist = NULL;
  io.edgemarkerlist = NULL;
  io.normlist = NULL;
  io.numberofedges = 0;
}

void triangulateioDestroy( triangulateio& io )
{
  trifree( (void*)io.pointlist );
  trifree( (void*)io.pointattributelist );
  trifree( (void*)io.pointmarkerlist );

  trifree( (void*)io.trianglelist );
  trifree( (void*)io.triangleattributelist );
  trifree( (void*)io.trianglearealist );
  trifree( (void*)io.neighborlist );

  trifree( (void*)io.segmentlist );
  trifree( (void*)io.segmentmarkerlist );

  trifree( (void*)io.holelist );

  trifree( (void*)io.regionlist );

  trifree( (void*)io.edgelist );
  trifree( (void*)io.edgemarkerlist );
  trifree( (void*)io.normlist );

  // Set all pointers to NULL and counts to 0
  triangulateioInit( io );
}

template<int Dim>
void triangulateEGADS( const EGBody<Dim>& body, triangulateio& out , std::map<int,ego>& marker2ego )
{
  triangulateio in, emptymesh;

  triangulateioInit(in);
  triangulateioInit(emptymesh);

  typedef EGNode<2>::CartCoord CartCoord;
  std::vector< EGNode<2> > nodes = body.getNodes();
  std::vector< EGEdge<2> > edges = body.getEdges();

  // Start by counting the nodes
  in.numberofpoints = (int)nodes.size();

  //Count the total number of vertexes on all edges
  for (auto edge = edges.begin(); edge != edges.end(); edge++ )
  {
    EGEdge<2>::ParamRange tlimit = edge->getParamRange();

    try
    {
      std::vector<double> t;
      edge->getAttribute(".tPos", t);

      for ( int i = 0; i < (int)t.size(); i++)
        SANS_ASSERT_MSG((t[i] > tlimit[0]) && (t[i] < tlimit[1]), " Error: tPos[%d] = %lf [%lf-%lf]",
                        i, t[i], t[0], t[1]);

      in.numberofpoints += t.size();
      in.numberofsegments += t.size()+1;
    }
    catch (SANSException& e)
    {
      std::vector<double> r;
      edge->getAttribute(".rPos", r);

      for (int i = 0; i < (int)r.size(); i++)
        SANS_ASSERT_MSG((r[i] > 0.0) && (r[i] < 1.0), "  Error: rPos[%d] = %lf [0 - 1]\n",
                        i, r[i]);

      in.numberofpoints += r.size();
      in.numberofsegments += r.size()+1;
    }
  }

  SANS_ASSERT( in.numberofsegments > 0 );

  in.pointlist = (REAL *) malloc(in.numberofpoints * 2 * sizeof(REAL));
  in.pointmarkerlist = (int *) malloc(in.numberofpoints  * sizeof(int));

  in.segmentlist = (int *) malloc( in.numberofsegments * 2 * sizeof(int));

#if 0
  in.numberofholes = 1;
  in.holelist = (REAL *) malloc(in.numberofholes * 2 * sizeof(REAL));
  in.holelist[0] = 0.5;
  in.holelist[1] = 1e-4;
#endif

#define xvec( jj ) in.pointlist[2*(jj)+0]
#define yvec( jj ) in.pointlist[2*(jj)+1]

  //First add the nodes to the list of points
  int ii = 0;
  for (auto node = nodes.begin(); node != nodes.end(); node++ )
  {
    const CartCoord& X = (CartCoord)(*node);
    xvec(ii) = X[0];
    yvec(ii) = X[1];
    //in.pointmarkerlist[ii] = -1;
    in.pointmarkerlist[ii] = -ii -1;
    marker2ego.insert( std::pair<int,ego>(-ii-1,*node) );
    ii++;
  }

  // Now add points on each edge and build up the segment list
  int seg = 0;
  std::vector<int> segidx(edges.size());
  for (std::size_t edge = 0; edge < edges.size(); edge++ )
  {
    segidx[edge] = seg;
    std::vector<double> t = edgeParamCoord(edges[edge]);

    std::vector< EGNode<2> > edgenodes = edges[edge].getNodes();

    in.segmentlist[2*seg+0] = body.getBodyIndex(edgenodes[0])-1;
    in.segmentlist[2*seg+1] = ii+0;
    seg++;

    for (int i = 0; i < (int)t.size(); i++)
    {
      CartCoord X = edges[edge](t[i]);
      xvec( ii ) = X[0];
      yvec( ii ) = X[1];
      in.pointmarkerlist[ii] = edge+1;

      marker2ego.insert( std::pair<int,ego>(edge+1,edges[edge] ) );

      in.segmentlist[2*seg+0] = ii+0;
      in.segmentlist[2*seg+1] = ii+1;

      ii++;
      seg++;
    }

    //The last segment in the loop is incorrect, fix it here
    seg--;

    in.segmentlist[2*seg+0] = ii-1;
    if ( edges[edge].isOneNode() )
      in.segmentlist[2*seg+1] = body.getBodyIndex(edgenodes[0])-1;
    else
      in.segmentlist[2*seg+1] = body.getBodyIndex(edgenodes[1])-1;
    seg++;
  }


  // -p Triangulates a Planar Straight Line Graph
  // -e Outputs a list of edges of the triangulation.
  // -z Numbers all items starting from zero (rather than one). This switch is useful when calling Triangle from another program.
  // -n Outputs a list of triangles neighboring each triangle.
  // -Y Prohibits the insertion of Steiner points on the mesh boundary.
  // -V Verbose: Gives detailed information about what Triangle is doing. Add more `V's for increasing amount of detail.
  // -Q Quiet: Suppresses all explanation of what Triangle is doing, unless an error occurs.
  // -q Quality mesh generation with no angles smaller than 20 degrees. An alternate minimum angle may be specified after the `q'.

  //Generate an empty mesh that will be used to locate holes
  triangulate((char*)"peznYQ", &in, &emptymesh, NULL);


  std::vector<double> holepoints;

  // Find all the holes in the empty mesh
  std::vector< EGLoop<2> > loops = body.getLoops();

  for (auto loop = loops.begin(); loop != loops.end(); loop++ )
  {
    std::vector< EGEdge<2> > edges = loop->getEdges();
    std::vector<int> senses = loop->getEdgeSenses();

    for ( std::size_t edge = 0; edge < edges.size(); edge++ )
    {
      // get the starting segment of this edge
      seg = segidx[body.getBodyIndex(edges[edge])-1];

      //Don't consider edges marked as internal
      if ( !edges[edge].hasAttribute(EGTriangle::INTERNALEDGE) )
      {
        // Look for two triangles attached to a segment
        int tri = 0;
        int twotri[2][4] = {{-1, -1, -1, -1}, {-1, -1, -1, -1}};
        for (int k = 0; k < emptymesh.numberoftriangles; k++ )
        {
          bool match[2] = {false, false};
          for (int v0 = 0; v0 < 2; v0++)
          {
            for (int n0 = 0; n0 < 3; n0++)
            {
              if ( in.segmentlist[2*seg+v0] == emptymesh.trianglelist[3*k+n0] )
              {
                match[v0] = true;
                break;
              }
            }
            //Found a match, save it of. There should be two triangles for an edge with holes
            if ( match[0] && match[1] )
            {
              for (int n0 = 0; n0 < 3; n0++)
                twotri[tri][n0] = emptymesh.trianglelist[3*k+n0];
              tri++;

              if ( tri == 2 )
                break;
            }
          }
        }


        if ( tri == 2 )
        {
          //Found two triangles, the one with a positive normal vector to the cell center is a hole
          double tricentroids[2][2];
          for (int i = 0; i < 2; i++)
          {
            std::vector<int> trinodes = {twotri[i][0], twotri[i][1], twotri[i][2]};
            centroid(emptymesh.pointlist, trinodes, tricentroids[i]);
          }
          //Use in.segmentlist because triangle can change the order
          std::vector<int> segnodes = {in.segmentlist[2*seg+0], in.segmentlist[2*seg+1]};
          double normal[2], centroidE[2], centroidVector[2];
          edgeNormal( emptymesh.pointlist, segnodes, normal );
          centroid( emptymesh.pointlist, segnodes, centroidE);

          // Create a vector from the edge centroid to the triangle centroid
          centroidVector[0] = tricentroids[0][0] - centroidE[0];
          centroidVector[1] = tricentroids[0][1] - centroidE[1];

          // if the dot is positive, then the first triangle is in the hole, otherwise use the 2nd triangle
          if ( dot( centroidVector, normal )*senses[edge] > 0 )
            for (int n = 0; n < 2; n++)
              holepoints.push_back(tricentroids[0][n]);
          else
            for (int n = 0; n < 2; n++)
              holepoints.push_back(tricentroids[1][n]);
        }
      }
    }
  }

  //Done with the empty mesh
  triangulateioDestroy(emptymesh);

  //Add the points inside the holes if any
  if ( holepoints.size() > 0 )
  {
    in.numberofholes = holepoints.size()/2;
    in.holelist = (REAL *) malloc(in.numberofholes * 2 * sizeof(REAL));
    for ( std::size_t n = 0; n < holepoints.size(); n++ )
      in.holelist[n] = holepoints[n];
  }


  // -p Triangulates a Planar Straight Line Graph
  // -e Outputs a list of edges of the triangulation.
  // -z Numbers all items starting from zero (rather than one). This switch is useful when calling Triangle from another program.
  // -n Outputs a list of triangles neighboring each triangle.
  // -Y Prohibits the insertion of Steiner points on the mesh boundary.
  //    If specified twice (-YY), it prohibits the insertion of Steiner points on any segment, including internal segments.
  // -V Verbose: Gives detailed information about what Triangle is doing. Add more `V's for increasing amount of detail.
  // -Q Quiet: Suppresses all explanation of what Triangle is doing, unless an error occurs.
  // -q Quality mesh generation with no angles smaller than 20 degrees. An alternate minimum angle may be specified after the `q'.
  triangulate((char*)"peznYYQq33", &in, &out, NULL);

  in.holelist = NULL; //pointer has been moved to 'out'
  triangulateioDestroy(in);
}

} //namespace

ego
EGTriangle::vertexGeometry( const int k )
{
  SANS_ASSERT( k < nDOF_ );
  SANS_ASSERT( k < int(pointmarkerlist_.size()) );
  return marker2ego_[ pointmarkerlist_[k] ];
}

EGTriangle::EGTriangle( const EGBody<Dim>& body )
{
  triangulateio out;
  triangulateioInit(out);

  triangulateEGADS( body, out , marker2ego_ );

  std::vector< EGEdge<2> > edges = body.getEdges();

  int maxDupPoints = 0;
  int duplicatePoints = 0;
  std::vector<bool> duplicateEdge(edges.size(), false);
  int nBCEdges = 0;

  std::vector<int> groupMapInterior( edges.size()+1, -1 );
  std::vector<int> groupMapBC( edges.size(), -1 );
  std::vector< std::map<int,int> > groupEdgePointOrder( edges.size() );

  int ngroupsInterior = 1;
  groupMapInterior[0] = 0;

  for ( std::size_t edge = 0; edge < edges.size(); edge++ )
  {
    //An internal edge might be artificial, or something special
    if ( edges[edge].hasAttribute(EGTriangle::INTERNALEDGE) )
    {
      if ( edges[edge].hasAttribute(EGTriangle::WAKEEDGE) )
      {
        groupMapBC[edge] = nBCEdges++; //Wake edges are counted as BC edges
        duplicatePoints += nEdgePoints(edges[edge]) + edges[edge].nNode();
        duplicateEdge[edge] = true;
      }
      else //This is an interior artificial edge group
        groupMapInterior[edge+1] = ngroupsInterior++;
    }
    else
      groupMapBC[edge] = nBCEdges++; //Not internal, so this edge is a boundary edge

    // Maximum possible number of duplicate points (all edges and nodes)
    maxDupPoints += nEdgePoints(edges[edge]) + edges[edge].nNode();
  }

  // Resize DOF and pointmarkers to match the number of points
  resizeDOF( out.numberofpoints + duplicatePoints );
  pointmarkerlist_.resize( nDOF() );

  //Create the element groups
  resizeCellGroups(1);

  nElem_ = out.numberoftriangles;

  // Copy over the points
  for ( int i = 0; i < out.numberofpoints; i++ )
  {
    DOF(i) = {out.pointlist[2*i+0], out.pointlist[2*i+1]};
    pointmarkerlist_[i] = out.pointmarkerlist[i];
  }
  trifree( (void*)out.pointlist ); out.pointlist = NULL;
  trifree( (void*)out.pointmarkerlist ); out.pointmarkerlist = NULL;


  // Duplicate points on edges that need duplication
  int ipoint = body.nNodes();
  std::vector<int> newPointMap(maxDupPoints, -1);
  int ii = 0;
  for (std::size_t edge = 0; edge < edges.size(); edge++ )
  {
    int npoint = nEdgePoints(edges[edge]);

    if ( duplicateEdge[edge] )
    {
      std::vector< EGNode<2> > edgenodes = edges[edge].getNodes();

      {
        int inode = body.getBodyIndex(edgenodes[0])-1;
        DOF(out.numberofpoints+ii) = DOF(inode);
        pointmarkerlist_[out.numberofpoints+ii] = pointmarkerlist_[inode];
        newPointMap[inode] = out.numberofpoints+ii;
        groupEdgePointOrder[edge].insert( std::pair<int,int>(inode, 0) );
        ii++;
      }

      for (int i = 0; i < npoint; i++)
      {
        DOF(out.numberofpoints+ii) = DOF(ipoint);
        pointmarkerlist_[out.numberofpoints+ii] = pointmarkerlist_[ipoint];
        newPointMap[ipoint] = out.numberofpoints+ii;
        groupEdgePointOrder[edge].insert( std::pair<int,int>(ipoint, i+1) );

        ii++;
        ipoint++;
      }

      //If the edge is a TWONODE, the last point on the last segment must be added
      if ( edges[edge].isTwoNode() )
      {
        int inode = body.getBodyIndex(edgenodes[1])-1;
        DOF(out.numberofpoints+ii) = DOF(inode);
        pointmarkerlist_[out.numberofpoints+ii] = pointmarkerlist_[inode];
        newPointMap[inode] = out.numberofpoints+ii;
        groupEdgePointOrder[edge].insert( std::pair<int,int>(inode, npoint+1) );
        ii++;
      }
    }
    else
    {
      ipoint += npoint;
    }
  }

  std::vector< FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType > fldAssocIedge( ngroupsInterior );
  std::vector< FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType > fldAssocBedge( nBCEdges );

  // interior-edge field variable
  resizeInteriorTraceGroups(ngroupsInterior);

  // Boundary condition groups
  resizeBoundaryTraceGroups( nBCEdges );


  // area field associativity variable
  FieldCellGroupType<Triangle>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionAreaBase<Triangle>::HierarchicalP1, nElem_ );


  // Construct the interior segments
  const int (*TraceNodes)[3][2] = &TraceToCellRefCoord<Line, TopoD2, Triangle>::TraceNodes;

  std::vector<int> groupSizeInterior( ngroupsInterior, 0 );
  std::vector<int> groupSegInterior( ngroupsInterior, 0 );

  // Create the mapping, BCelem, so elements on edges are ordered based on the vertex number
  std::vector< std::vector< std::pair<int,int> > > elemBC(nBCEdges);
  std::vector<int> groupSegBC(nBCEdges, 0);


  // Count the total number of interior segments
  for ( int iTriL = 0; iTriL < out.numberoftriangles; iTriL++ )
  {

    std::vector<int> triL = {out.trianglelist[3*iTriL+0],
                             out.trianglelist[3*iTriL+1],
                             out.trianglelist[3*iTriL+2]};

    for ( int segL = 0; segL < 3; segL++ )
    {
      const int edge = segmentEdgeNumber(pointmarkerlist_, triL, segL);

      std::vector<int> segnodes = {triL[(*TraceNodes)[segL][0]], triL[(*TraceNodes)[segL][1]]};

      const int iTriR = out.neighborlist[3*iTriL + segL];
      if ( iTriR > iTriL ) //Right triangle always has larger index
      {

        std::vector<int> triR = {out.trianglelist[3*iTriR+0],
                                 out.trianglelist[3*iTriR+1],
                                 out.trianglelist[3*iTriR+2]};

        // Find the nodes for an edge on both triangles
        for (int segR = 0; segR < 3; segR++ )
        {
          if ( sameSegment(triL, segL, triR, segR) )
          {
            if ( groupMapInterior[edge] > -1 )
              groupSizeInterior[groupMapInterior[edge]]++;
            else if ( edge > 0 && groupMapBC[edge-1] > -1 )
            {
              double centroidL[2];
              centroid(DOF_, triL, centroidL);

              double normal[2], centroidE[2], centroidVector[2];
              edgeNormal( DOF_, segnodes, normal );
              centroid(DOF_, segnodes, centroidE);

              // Create a vector from the edge centroid to the right triangle centroid
              centroidVector[0] = centroidL[0] - centroidE[0];
              centroidVector[1] = centroidL[1] - centroidE[1];

              // if the dot is negative, the normal points from left to right. Otherwise swap the nodes.
              const int lessernode = dot( centroidVector, normal ) < 0 ? segnodes[0] : segnodes[1];

              const int group = groupMapBC[edge-1];
              elemBC[group].emplace_back( groupEdgePointOrder[group][lessernode], groupSegBC[group] );
              groupSegBC[group]++;
            }
            break;
          }
        }
      }
      else if ( edge > 0 && iTriR < 0 )
      {
        double centroidL[2];
        centroid(DOF_, triL, centroidL);

        double normal[2], centroidE[2], centroidVector[2];
        edgeNormal( DOF_, segnodes, normal );
        centroid(DOF_, segnodes, centroidE);

        // Create a vector from the edge centroid to the right triangle centroid
        centroidVector[0] = centroidL[0] - centroidE[0];
        centroidVector[1] = centroidL[1] - centroidE[1];

        // if the dot is negative, the normal points from left to right. Otherwise swap the nodes.
        const int lessernode = dot( centroidVector, normal ) < 0 ? segnodes[0] : segnodes[1];

        const int group = groupMapBC[edge-1];

        elemBC[group].emplace_back( groupEdgePointOrder[group][lessernode], groupSegBC[group] );
        groupSegBC[group]++;
      }
    }
  }


  //Resize the groups constructors
  for (int group = 0; group < ngroupsInterior; group++ )
  {
    fldAssocIedge[group].resize( BasisFunctionLineBase::HierarchicalP1, groupSizeInterior[group] );
    fldAssocIedge[group].setGroupLeft( 0 );
    fldAssocIedge[group].setGroupRight( 0 );
  }

  //Resize the number of elements on all BC groups
  for (int edge = 0; edge < nBCEdges; edge++ )
  {
    fldAssocBedge[edge].resize( BasisFunctionLineBase::HierarchicalP1, groupSegBC[edge] );
    fldAssocBedge[edge].setGroupLeft( 0 );

    groupSegBC[edge] = 0;

    //Sort all the elements based on the node index for the element
    //Use a lambda function for the sorting
    std::sort(elemBC[edge].begin(), elemBC[edge].end(),
        [](const std::pair<int,int>& a, const std::pair<int,int>& b) -> bool { return a.first < b.first; });
  }

  // Set the right group for interior BC edges
  for (std::size_t edge = 0; edge < edges.size(); edge++ )
    if ( duplicateEdge[edge] )
      fldAssocBedge[groupMapBC[edge]].setGroupRight( 0 );

  // Create the sorting map for BC's
  std::vector< std::vector<int> > segSortMap(nBCEdges);
  for (int group = 0; group < nBCEdges; group++ )
  {
    int nseg = (int)elemBC[group].size();
    segSortMap[group].resize(elemBC[group].size());
    for ( int seg = 0; seg < nseg; seg++ )
      segSortMap[group][elemBC[group][seg].second] = seg;
  }

  // Count the total number of interior segments
  for ( int iTriL = 0; iTriL < out.numberoftriangles; iTriL++ )
  {

    std::vector<int> triL = {out.trianglelist[3*iTriL+0],
                             out.trianglelist[3*iTriL+1],
                             out.trianglelist[3*iTriL+2]};

    for ( int segL = 0; segL < 3; segL++ )
    {
      const int edge = segmentEdgeNumber(pointmarkerlist_, triL, segL);
      if ( edge == 0 || !duplicateEdge[edge-1] ) continue;

      const int iTriR = out.neighborlist[3*iTriL + segL];
      if ( iTriR > iTriL ) //This prevents adding the same edge twice, but L and R might be swapped
      {

        std::vector<int> triR = {out.trianglelist[3*iTriR+0],
                                 out.trianglelist[3*iTriR+1],
                                 out.trianglelist[3*iTriR+2]};

        // Find the nodes for an edge on both triangles
        for (int segR = 0; segR < 3; segR++ )
        {
          if ( sameSegment(triL, segL, triR, segR) )
          {
            const int group = groupMapBC[edge-1];

            std::vector<int> segnodes = {triL[(*TraceNodes)[segL][0]], triL[(*TraceNodes)[segL][1]]};

            // Sort the edge nodes based on increasing order so the normals are consistent for all edges
            if ( groupEdgePointOrder[group][segnodes[0]] > groupEdgePointOrder[group][segnodes[1]] )
              std::swap(segnodes[0], segnodes[1]);

            double centroidL[2];
            double normal[2], centroidE[2], centroidVector[2];
            edgeNormal( DOF_, segnodes, normal );
            centroid(DOF_, triL, centroidL);
            centroid(DOF_, segnodes, centroidE);

            // Create a vector from the edge centroid to the left triangle centroid
            centroidVector[0] = centroidL[0] - centroidE[0];
            centroidVector[1] = centroidL[1] - centroidE[1];

            const int seg = segSortMap[group][groupSegBC[group]];
            groupSegBC[group]++;

            fldAssocBedge[group].setAssociativity( seg ).setRank( 0 );

            // The node order is fixed for an interior BC edge
            fldAssocBedge[group].setAssociativity( seg ).setNodeGlobalMapping( {segnodes[0], segnodes[1]} );

            // if the dot is negative, the normal points from left to right. Otherwise swap the triangles.
            if ( dot( centroidVector, normal ) < 0 )
            {
              fldAssocBedge[group].setElementLeft( iTriL, seg );
              fldAssocBedge[group].setElementRight( iTriR, seg );

              fldAssocBedge[group].setCanonicalTraceLeft(  CanonicalTraceToCell(segL,  1), seg );
              fldAssocBedge[group].setCanonicalTraceRight( CanonicalTraceToCell(segR, -1), seg );

              // L is +1, and R is -1
              fldAssocCell.setAssociativity( iTriL ).setEdgeSign( +1, segL );
              fldAssocCell.setAssociativity( iTriR ).setEdgeSign( -1, segR );
            }
            else
            {
              // Swap L and R so that normal is pointing towards R
              fldAssocBedge[group].setElementLeft( iTriR, seg );
              fldAssocBedge[group].setElementRight( iTriL, seg );

              fldAssocBedge[group].setCanonicalTraceLeft(  CanonicalTraceToCell(segR,  1), seg );
              fldAssocBedge[group].setCanonicalTraceRight( CanonicalTraceToCell(segL, -1), seg );

              // L is +1, and R is -1
              fldAssocCell.setAssociativity( iTriR ).setEdgeSign( +1, segR );
              fldAssocCell.setAssociativity( iTriL ).setEdgeSign( -1, segL );
            }

            break;
          }
        }
      }
    }
  }


  for (int edge = 0; edge < nBCEdges; edge++ )
  {
    if ( duplicateEdge[edge] )
    {
      for (int seg = 0; seg < fldAssocBedge[edge].nElem(); seg++)
      {
        const int iTriL = fldAssocBedge[edge].getElementLeft( seg );
        const int iTriR = fldAssocBedge[edge].getElementRight( seg );

        std::vector<int> segnodes(2);
        fldAssocBedge[edge].getAssociativity( seg ).getNodeGlobalMapping( &segnodes[0], 2 );

        double normal[2];
        edgeNormal( DOF_, segnodes, normal );

        // Update the node mapping of the right triangle to the duplicate nodes
        updateTriangleNodes( out, DOF_, newPointMap, iTriL, iTriR, normal, segnodes[0] );
        updateTriangleNodes( out, DOF_, newPointMap, iTriL, iTriR, normal, segnodes[1] );
      }
    }
  }

  // Create the interior segment associations and cell-to-cell connectivity
  for ( int iTriL = 0; iTriL < out.numberoftriangles; iTriL++ )
  {
    fldAssocCell.setAssociativity( iTriL ).setRank( 0 );

    //element area associativity of the triangles
    fldAssocCell.setAssociativity( iTriL ).setNodeGlobalMapping( out.trianglelist + 3*iTriL, 3 );

    // Get the left triangle nodes
    std::vector<int> triL = {out.trianglelist[3*iTriL+0],
                             out.trianglelist[3*iTriL+1],
                             out.trianglelist[3*iTriL+2]};

    double centroidL[2];
    centroid(DOF_, triL, centroidL);

    for ( int segL = 0; segL < 3; segL++ )
    {
      const int edge = segmentEdgeNumber(pointmarkerlist_, triL, segL);
      const int iTriR = out.neighborlist[3*iTriL + segL];
      std::vector<int> segnodes = {triL[(*TraceNodes)[segL][0]], triL[(*TraceNodes)[segL][1]]};

      double normal[2], centroidE[2], centroidVector[2];
      edgeNormal( DOF_, segnodes, normal );
      centroid(DOF_, segnodes, centroidE);

      // Create a vector from the edge centroid to the left triangle centroid
      centroidVector[0] = centroidL[0] - centroidE[0];
      centroidVector[1] = centroidL[1] - centroidE[1];

      if ( iTriR > iTriL ) //Right triangle always has larger index
      {

        std::vector<int> triR = {out.trianglelist[3*iTriR+0],
                                 out.trianglelist[3*iTriR+1],
                                 out.trianglelist[3*iTriR+2]};


        // Find the nodes for an edge on both triangles
        for (int segR = 0; segR < 3; segR++ )
        {
          if ( sameSegment(triL, segL, triR, segR) )
          {
            if ( groupMapInterior[edge] > -1 )
            {
              const int group = groupMapInterior[edge];

              const int seg = groupSegInterior[group];
              groupSegInterior[group]++;

              fldAssocIedge[group].setElementLeft( iTriL, seg );
              fldAssocIedge[group].setElementRight( iTriR, seg );

              fldAssocIedge[group].setCanonicalTraceLeft(  CanonicalTraceToCell(segL,  1), seg );
              fldAssocIedge[group].setCanonicalTraceRight( CanonicalTraceToCell(segR, -1), seg );

              // L is +1, and R is -1
              fldAssocCell.setAssociativity( iTriL ).setEdgeSign( +1, segL );
              fldAssocCell.setAssociativity( iTriR ).setEdgeSign( -1, segR );

              fldAssocIedge[group].setAssociativity( seg ).setRank( 0 );

              // if the dot is negative, the normal points from left to right. Otherwise swap the nodes.
              if ( dot( centroidVector, normal ) < 0 )
                fldAssocIedge[group].setAssociativity( seg ).setNodeGlobalMapping( {segnodes[0], segnodes[1]} );
              else
                fldAssocIedge[group].setAssociativity( seg ).setNodeGlobalMapping( {segnodes[1], segnodes[0]} );

            }
          }
        }
      }
      else if ( edge > 0 && iTriR < 0 )
      {
        const int group = groupMapBC[edge-1];

        const int seg = segSortMap[group][groupSegBC[group]];

        fldAssocBedge[group].setElementLeft( iTriL, seg );
        fldAssocBedge[group].setCanonicalTraceLeft( CanonicalTraceToCell(segL, 1), seg );

        fldAssocCell.setAssociativity( iTriL ).setEdgeSign( +1, segL );

        fldAssocBedge[group].setAssociativity( seg ).setRank( 0 );

        // if the dot is negative, the normal points from left to right. Otherwise swap the nodes.
        if ( dot( centroidVector, normal ) < 0 )
          fldAssocBedge[group].setAssociativity( seg ).setNodeGlobalMapping( {segnodes[0], segnodes[1]} );
        else
          fldAssocBedge[group].setAssociativity( seg ).setNodeGlobalMapping( {segnodes[1], segnodes[0]} );

        groupSegBC[group]++;
      }
    }
  }


  // clean up memory
  triangulateioDestroy(out);

  for (int group = 0; group < ngroupsInterior; group++ )
  {
    interiorTraceGroups_[group] = new FieldTraceGroupType<Line>( fldAssocIedge[group] );
    interiorTraceGroups_[group]->setDOF(DOF_, nDOF_);
  }


  for (int edge = 0; edge < nBCEdges; edge++ )
  {
    boundaryTraceGroups_[edge] = new FieldTraceGroupType<Line>( fldAssocBedge[edge] );
    boundaryTraceGroups_[edge]->setDOF(DOF_, nDOF_);
  }

  cellGroups_[0] = new FieldCellGroupType<Triangle>( fldAssocCell );
  cellGroups_[0]->setDOF(DOF_, nDOF_);

  //Check for any errors in the grid
  checkGrid();
}

}
}
