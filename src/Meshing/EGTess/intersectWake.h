// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTERSECT_WAKE_H_
#define INTERSECT_WAKE_H_

#include "Meshing/EGADS/EGBody.h"

namespace SANS
{

// The OCC intersect command returns edges/faces in radom order
// This function will always return edges/faces in a consistent order

EGADS::EGBody<3>
intersectWake(const EGADS::EGBody<3>& solid, const EGADS::EGBody<3>& wakelong);

}

#endif // INTERSECT_WAKE_H_
