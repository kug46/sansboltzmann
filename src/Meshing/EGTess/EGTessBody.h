// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EG_TESSBODY_H
#define EG_TESSBODY_H

#include "Meshing/EGADS/EGStatus.h"

extern "C"
{
int
EG_getTessBody(ego tess, int *len, double **pxyz, double **puvt,
                         int **ptype, int **pindex,
                         int *ntri, int **ptris, int **ptric,
                         int **ntriface,
                         int *nbndedges, int **pbndedgeindex);
}

namespace SANS
{
namespace EGADS
{

class EGTessBody
{
public:

  EGTessBody() = delete;
  EGTessBody(const EGTessBody&) = delete;
  EGTessBody& operator=(const EGTessBody&) = delete;

  explicit EGTessBody(ego tess)
  {
    EG_STATUS( EG_getTessBody(tess, &nvert, &pxyz, &puvt,
                              &ptype, &pindex,
                              &ntri, &ptris, &ptric,
                              &ntriface,
                              &nbndedges, &pbndedgeindex) );
  }

  EGTessBody(EGTessBody&& tess)
  : nvert(tess.nvert), ntri(tess.ntri), nbndedges(tess.nbndedges),
    ptype(tess.ptype), pindex(tess.pindex), ptris(tess.ptris), ptric(tess.ptric), ntriface(tess.ntriface), pbndedgeindex(tess.pbndedgeindex),
    pxyz(tess.pxyz), puvt(tess.puvt)
  {
    tess.nvert = 0;
    tess.ntri = 0;
    tess.nbndedges = 0;
    tess.ptype = NULL;
    tess.pindex = NULL;
    tess.ptris = NULL;
    tess.ptric = NULL;
    tess.ntriface = NULL;
    tess.pbndedgeindex = NULL;
    tess.pxyz = NULL;
    tess.puvt = NULL;
  }


  ~EGTessBody()
  {
    EG_free(ptype);
    EG_free(pindex);
    EG_free(ptris);
    EG_free(ptric);
    EG_free(ntriface);
    EG_free(pbndedgeindex);
    EG_free(pxyz);
    EG_free(puvt);
  }

  int nvert, ntri, nbndedges;
  int *ptype, *pindex, *ptris, *ptric, *ntriface, *pbndedgeindex;
  double *pxyz, *puvt;
};

} //namespace EGADS
} //namespace SANS

#endif //EG_TESSBODY_H
