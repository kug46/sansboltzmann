// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "EGVertexFace.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

namespace SANS
{
namespace EGADS
{

template<int Dim>
EGVertexFace<Dim>::EGVertexFace( const EGFace<Dim>& face, const ParamCoord& uv, CartCoord* X, const bool fixed )
  : EGVertex<Dim>(X, fixed), face_(&face), uv_(uv)
{

}

template<int Dim_>
EGVertexFace<Dim_>&
EGVertexFace<Dim_>::operator=( const EGVertexFace& vf )
{
  if ( this != &vf )
  {
    EGVertex<Dim>::operator=(vf);
    face_ = vf.face_;
    uv_ = vf.uv_;
  }
  return *this;
}

template<int Dim_>
void
EGVertexFace<Dim_>::update( const ParamCoord& uv)
{
  uv_ = uv;
  *EGVertex<Dim>::X_ = (*face_)(uv);
}

template<int Dim_>
void
EGVertexFace<Dim_>::update( const Real u, const Real v)
{
  uv_[0] = u;
  uv_[1] = v;
  *EGVertex<Dim>::X_ = (*face_)(uv_);
}

template<int Dim_>
EGVertexFace<Dim_>::~EGVertexFace()
{
}

//Explicitly instantiate the class
template class EGVertexFace<2>;
template class EGVertexFace<3>;

}
}
