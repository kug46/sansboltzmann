// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EG_VERTEX_EDGE_NODE_H
#define EG_VERTEX_EDGE_NODE_H

//A vertex associated with a node and an edge

#include "EGVertexEdge.h"

namespace SANS
{
namespace EGADS
{

template<int Dim_>
class EGVertexEdgeNode : public EGVertexEdge<Dim_>
{
public:
  static const int Dim = Dim_;
  typedef DLA::VectorS<Dim, Real> CartCoord;

  EGVertexEdgeNode( const EGNode<Dim>& node, const EGEdge<Dim>& edge, const Real t, CartCoord* X );

  virtual void update( Real t );
protected:
  const EGNode<Dim>& node_;
};

}
}

#endif //EG_VERTEX_EDGE_NODE_H
