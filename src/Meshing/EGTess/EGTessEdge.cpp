// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <algorithm>

#include "EGTessEdge.h"


namespace SANS
{
namespace EGADS
{

template<int Dim_>
EGTessEdge<Dim_>::EGTessEdge(const EGTessEdge& te)
  : edge_( te.edge_ ) //, xfldedge_(te.xfldedge_)
{
  const int size = te.Vertexes_.size();
  for (int i = 0; i < size; i++)
    Vertexes_.push_back( VertexType(edge_, te.Vertexes_[i].getParam(), te.Vertexes_[i].getVertex_ptr(), te.Vertexes_[i].fixed() ) );
}

template<int Dim_>
EGTessEdge<Dim_>::EGTessEdge(const EGEdge<Dim>& edge, const int n, CartCoord* X) : edge_(edge)
{
  typename EGEdge<Dim>::ParamRange range = edge.getParamRange();

  //Insert all the vertexes (first and last are nodes)
  for ( int i = 0; i < n; i++ )
  {
    bool fixed = (i == 0 || i == n-1);
    Real t = (range[1] - range[0])/(n-1)*i + range[0];
    X[i] = edge(t);
    Vertexes_.push_back( EGVertexEdge<Dim>(edge_, t, &X[i], fixed) );
  }
}

template<int Dim_>
EGTessEdge<Dim_>::EGTessEdge(const EGBody<Dim>& body, const EGEdge<Dim>& edge, ego tess, int eIndex,
                             CartCoord* X, CartCoord* Xnodes) : edge_( edge )
{
  int n = 0;
  const double *pxyz = NULL, *pt = NULL;

  //Extract the edge tessellation
  EG_STATUS( EG_getTessEdge(tess, eIndex, &n, &pxyz, &pt) );

  typename EGEdge<Dim>::ParamRange range = edge.getParamRange();
  std::vector< EGNode<Dim> > nodes = edge.getNodes();

  //Copy over all the cartesian coordinates (excluding the nodes)
  for ( int i = 1; i < n-1; i++ )
    for ( int d = 0; d < Dim; d++)
      X[i-1][d] = pxyz[3*i + d];

  //Add the first node
  Vertexes_.emplace_back( edge_, range[0], &Xnodes[body.getBodyIndex(nodes[0])-1], true );

  //Insert all the free vertexes
  for ( int i = 1; i < n-1; i++ )
    Vertexes_.emplace_back( edge_, pt[i], &X[i-1], false );

  //Add the second node
  if (edge.isOneNode())
    Vertexes_.emplace_back( edge_, range[1], &Xnodes[body.getBodyIndex(nodes[0])-1], true );
  else if (edge.isTwoNode())
    Vertexes_.emplace_back( edge_, range[1], &Xnodes[body.getBodyIndex(nodes[1])-1], true );
  else
    SANS_ASSERT( false );

}

template<int Dim_>
EGTessEdge<Dim_>::EGTessEdge(const EGBody<Dim>& body, const EGEdge<Dim>& edge, const int *ptype,
                             const double *puvt,
                             std::vector<int>& nodeMap,
                             CartCoord* X, CartCoord* Xnodes) : edge_( edge )
{
  typename EGEdge<Dim>::ParamRange range = edge.getParamRange();
  std::vector< EGNode<Dim> > nodes = edge.getNodes();

  //Add the first node
  Vertexes_.emplace_back( edge_, range[0], &Xnodes[nodeMap[body.getBodyIndex(nodes[0])-1]], true );

  //Insert all the free vertexes
  int i = 0;
  while ( ptype[i] > 0 )
  {
    Vertexes_.emplace_back( edge_, puvt[i], &X[i], false );
    i++;
  }

  //Add the second node
  if (edge.isOneNode())
    Vertexes_.emplace_back( edge_, range[1], &Xnodes[nodeMap[body.getBodyIndex(nodes[0])-1]], true );
  else if (edge.isTwoNode())
    Vertexes_.emplace_back( edge_, range[1], &Xnodes[nodeMap[body.getBodyIndex(nodes[1])-1]], true );
  else
    SANS_ASSERT( false );

}


template<int Dim_>
EGTessEdge<Dim_>&
EGTessEdge<Dim_>::operator=(const EGTessEdge& et)
{
  if ( this != &et )
  {
    edge_ = et.edge_;
    Vertexes_ = et.Vertexes_;
//    xfldedge_ = et.xfldedge_;
  }
  return *this;
}

template<int Dim_>
void
EGTessEdge<Dim_>::evaluate(const int n, const double* pt)
{
  SANS_ASSERT( n == nFreeParameters() );

  //Only the interior vertexes can move
  for ( int i = 1; i <= n; i++ )
    Vertexes_[i].update(pt[i-1]);
}

template<int Dim_>
void
EGTessEdge<Dim_>::fillFreeParameters(std::vector<double>& x) const
{
  x.resize( nFreeParameters() );

  //Only get interior vertexes
  for ( std::size_t i = 0; i < x.size(); i++ )
    x[i] = Vertexes_[i+1].getParam();
}

template<int Dim_>
Real
EGTessEdge<Dim_>::getReferenceLength() const
{
  typename EGEdge<Dim>::ParamRange range = edge_.getParamRange();

  double alen = 0;
  EG_STATUS( EG_arcLength(edge_, range[0], range[1], &alen) );

  return alen/(nVertex()-1);
}

template<int Dim_>
void
EGTessEdge<Dim_>::sort()
{
  std::sort(Vertexes_.begin(), Vertexes_.end());
}

//Explicitly instantiate the class
template class EGTessEdge<2>;
template class EGTessEdge<3>;

}
}
