// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EG_VERTEX_FACE_H
#define EG_VERTEX_FACE_H

//A vertex associated with a face

#include <typeinfo>

#include "Meshing/EGADS/EGFace.h"
#include "EGVertex.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

namespace SANS
{
namespace EGADS
{

template<int Dim_>
class EGVertexFace : public EGVertex<Dim_>
{
public:
  using EGVertex<Dim_>::Dim;
  typedef DLA::VectorS<Dim, Real> CartCoord;
  typedef DLA::VectorS<2, Real> ParamCoord;
  typedef DLA::VectorS<2, Real> ParamRange;
  typedef typename EGFace<Dim>::JinvType JinvType;

  EGVertexFace( const EGVertexFace& vf ) : EGVertex<Dim>(vf), face_(vf.face_), uv_(vf.uv_) {}
  EGVertexFace( const EGFace<Dim>& face, const ParamCoord& uv, CartCoord* X, const bool fixed );
  virtual ~EGVertexFace();

  EGVertexFace& operator=( const EGVertexFace& vf );

  virtual const std::type_info& typeID() const { return typeid(*this); }

  void update( const ParamCoord& uv );
  void update( const Real u, const Real v );
  const ParamCoord& getParam() const { return uv_; }
  JinvType Jinv() const { return face_->Jinv(uv_); }

  const EGFace<Dim>& face() const { return *face_; }

protected:
  const EGFace<Dim>* face_;
  ParamCoord uv_;
};

}
}

#endif //EG_VERTEX_FACE_H
