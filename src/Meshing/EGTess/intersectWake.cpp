// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "intersectWake.h"

#include "Meshing/EGADS/EGIntersection.h"
#include "Meshing/EGADS/replaceFaces.h"

namespace SANS
{

EGADS::EGBody<3>
intersectWake(const EGADS::EGBody<3>& solid, const EGADS::EGBody<3>& wakelong)
{
  EGADS::EGIntersection<3> box_wake_intersect(solid, wakelong);

  // Scribe the outflow face on the wake
  std::vector<EGADS::EGFace<3>> faces( box_wake_intersect.uniqueFaces() );
  SANS_ASSERT_MSG( faces.size() == 1, "faces.size() = %d", faces.size() );
  EGADS::EGBody<3> tool( faces[0] );
  EGADS::EGIntersection<3> wake_face_intersect( wakelong, tool );
  EGADS::EGBody<3> scribed_wake( wake_face_intersect );

  std::vector<double> bbox = solid.getBoundingBox();
  std::vector<EGADS::EGFace<3>> wake_faces = scribed_wake.getFaces();
  std::vector<ego> removeFaces;
  for ( const EGADS::EGFace<3>& face : wake_faces )
  {
    // Skip faces that are inside the box and need to be retained
    std::vector<double> CG = face.getCG();
    if ( (bbox[0] < CG[0] && CG[0] < bbox[3]) &&
         (bbox[1] < CG[1] && CG[1] < bbox[4]) &&
         (bbox[2] < CG[2] && CG[2] < bbox[5]) )
      continue;

    // indicate that the face should be removed
    removeFaces.push_back(face);
    removeFaces.push_back(nullptr);
  }

  // remove the faces outside of the domain
  return replaceFaces(scribed_wake, removeFaces);
}

}
