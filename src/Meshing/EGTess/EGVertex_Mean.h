// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EG_VERTEX_MEAN_H
#define EG_VERTEX_MEAN_H

//Computes the Cartesian coordinate based on the mean
//parametric coordinates of two vertexes

#include "EGVertexEdge.h"
#include "EGVertexFace.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

namespace SANS
{

template<int Dim>
DLA::VectorS<Dim, Real>
mean( const EGADS::EGVertexEdge<Dim>& Xi, const EGADS::EGVertexEdge<Dim>& Xj )
{
  SANS_ASSERT( (ego)Xi.edge() == (ego)Xj.edge() );
  Real t = (Xi.getParam() + Xj.getParam())/2;
  return Xi.edge()(t);
}

template<int Dim>
DLA::VectorS<Dim, Real>
mean( const EGADS::EGVertexEdge<Dim>& Xi, const DLA::VectorS<Dim, Real>& Xj )
{
  typedef typename EGADS::EGVertexEdge<Dim>::CartCoord CartCoord;
  return ((CartCoord)Xi + Xj)/2;
}

template<int Dim>
DLA::VectorS<Dim, Real>
mean( const EGADS::EGVertexFace<Dim>& Xi, const EGADS::EGVertexFace<Dim>& Xj )
{
  typedef typename EGADS::EGVertexFace<Dim>::CartCoord CartCoord;
  SANS_ASSERT( (ego)Xi.face() == (ego)Xj.face() );
  return ((CartCoord)Xi + (CartCoord)Xj)/2;
}

template<int Dim>
DLA::VectorS<Dim, Real>
mean( const EGADS::EGVertexFace<Dim>& Xi, const DLA::VectorS<Dim, Real>& Xj )
{
  typedef typename EGADS::EGVertexFace<Dim>::CartCoord CartCoord;
  return ((CartCoord)Xi + Xj)/2;
}

}

#endif //EG_VERTEX_MEAN_H
