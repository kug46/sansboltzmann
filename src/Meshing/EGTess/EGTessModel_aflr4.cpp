// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifdef SANS_AFLR
#include "EGTessModel.h"

#include <ug/UG_LIB.h> // Must be included first

#include <aflr4/AFLR4_LIB.h>
#include <surf_auto/SURF_AUTO_LIB.h>

#include "tools/SANSnumerics.h"
#include "Field/XField3D_Wake.h"
#include "AFLR4.h"

#include <vector>
#include <array>
#include <limits>
#include <memory> // std::shared_ptr

#if 0
#include <iomanip> //std::setprecision
#include <fstream>
#endif

namespace SANS
{
namespace EGADS
{
// No-name namespace to make things private to this file
namespace
{

#define MAGICNUMBER  1234321

struct egads_cad_data
{
  const int magic = MAGICNUMBER;

  std::vector< int > modelBodyIndex[EGTessModel::NBODYTOPO];
  std::vector< std::vector< int > > modelEdgeIndex[EGTessModel::NBODYTOPO];
  std::vector< std::vector< int > > modelNodeIndex[EGTessModel::NBODYTOPO];

  std::vector< EGBody<3> > bodies;
  std::vector< EGFace<3> > modelFaces;
  std::vector< EGEdge<3> > modelEdges;
  std::vector< EGNode<3> > modelNodes;

  std::vector< std::pair<int,int> > modelBodyInvIndex;
  EGTessModel::FloatingEdgeMap floatingEdges;

  std::vector< std::array<int,2> > edgeNodesIndex;

  struct FloatingFaceEdge
  {
    int imodelFace;
    int imodelEdge;
  };
  std::vector<FloatingFaceEdge> floatingFaceEdge;
};

INT_ egads_normalize (void)
{
  // set normalization flag

#ifdef NO_EGADS_NORMALIZATION
  return 0;
#else
  return 1;
#endif
}

INT_ egads_eval_uv_to_uvn (INT_ idef,
                           void *ext_cad_data,
                           double uv[2])
{
  // normalize u,v coordinates

  double uvbox[4];
  CHAR_133 Text;

  int status, per;

  if (egads_normalize())
  {
    egads_cad_data *ptr = (egads_cad_data*)ext_cad_data;
    if (ptr == NULL || ptr->magic != MAGICNUMBER)
    {
      sprintf(Text,"*** EGADS ERROR : invalid ext_cad_data : %d ***", __LINE__);
      ug_error_message(Text);
      return 99;
    }

    status = EG_getRange((ego)ptr->modelFaces[idef-1], uvbox, &per);

    if (status != EGADS_SUCCESS)
      return 9921;

    uv[0] = (uv[0]-uvbox[0]) / (uvbox[1] - uvbox[0]);
    uv[1] = (uv[1]-uvbox[2]) / (uvbox[3] - uvbox[2]);
  }

  return 0;
}

INT_ egads_eval_uvn_to_uv (INT_ idef,
                           void *ext_cad_data,
                           double uv[2])
{
  // un-normalize u,v coordinates

  double uvbox[4];
  CHAR_133 Text;

  int status, per;

  if (egads_normalize())
  {
    egads_cad_data *ptr = (egads_cad_data*)ext_cad_data;
    if (ptr == NULL || ptr->magic != MAGICNUMBER)
    {
      sprintf(Text,"*** EGADS ERROR : invalid ext_cad_data : %d ***", __LINE__);
      ug_error_message(Text);
      return 99;
    }

    status = EG_getRange((ego)ptr->modelFaces[idef-1], uvbox, &per);

    if (status != EGADS_SUCCESS)
      return 9922;

    uv[0] = uvbox[0] + uv[0] * (uvbox[1] - uvbox[0]);
    uv[1] = uvbox[2] + uv[1] * (uvbox[3] - uvbox[2]);
  }

  return 0;
}

INT_ egads_eval_uv_bounds
 (INT_ idef,
  void *ext_cad_data,
  double *umax,
  double *umin,
  double *vmax,
  double *vmin)

{

/*
 *
 * Set uv-bounding box for a given CAD geometry definition. This is the bounding
 * box for the full definition (not that for a trimming curve). It is used to
 * determine if the mapping has singular bounding curves in physical space or
 * requires scaling.
 *
 * Input Parameters
 *
 *  idef  : identification label for surface definition
 *  ext_cad_data: data of unknown type that is used and defined within the
 *
 * Output Parameters
 *
 *  umax  : u-coordinate maximum value
 *  umin  : u-coordinate minimum value
 *  vmax  : v-coordinate maximum value
 *  vmin  : v-coordinate minimum value
 *
 * Return Value
 *
 *  0     : no errors
 *  > 0   : there were errors
 *
 */

  double uvbox[4];
  int    status, per;
  CHAR_133 Text;

  if (egads_normalize())
  {
    *umin = 0.0;
    *vmin = 0.0;
    *umax = 1.0;
    *vmax = 1.0;
  }
  else
  {
    egads_cad_data *ptr = (egads_cad_data*)ext_cad_data;
    if (ptr == NULL || ptr->magic != MAGICNUMBER)
    {
      sprintf(Text,"*** EGADS ERROR : invalid ext_cad_data : %d ***", __LINE__);
      ug_error_message(Text);
      return 99;
    }

    int iface = idef-1;

    status = EG_getRange((ego)ptr->modelFaces[iface], uvbox, &per);
    if (status != EGADS_SUCCESS)
    {
      sprintf(Text,"*** EGADS ERROR %d : EG_getRange for Face %d ***", status, idef);
      ug_error_message(Text);
      return -status;
    }

    *umin = uvbox[0];
    *umax = uvbox[1];
    *vmin = uvbox[2];
    *vmax = uvbox[3];
  }

  return 0;
}

INT_ egads_eval_xyz_at_u (INT_ idef_,
                          void *ext_cad_data,
                          double s,
                          double *x,
                          double *y,
                          double *z)
{
  /* idef: index, starts with 1
   * Evaluate Cartesian coordinates (x,y,z) on a given edge (idef) at a given
   * parametric coordinate location (s).
   */

  double coord[18];
  int status;
  CHAR_133 Text;

  egads_cad_data *ptr = (egads_cad_data*)ext_cad_data;
  if (ptr == NULL || ptr->magic != MAGICNUMBER)
  {
    sprintf(Text,"*** EGADS ERROR : invalid ext_cad_data : %d ***", __LINE__);
    ug_error_message(Text);
    return 99;
  }

  int iedge = idef_-1;

  status = EG_evaluate((ego)ptr->modelEdges[iedge], &s, coord);
  if (status != EGADS_SUCCESS)
  {
    sprintf(Text,"*** EGADS ERROR %d : EG_evaluate for Edge %d ***", status, idef_);
    ug_error_message(Text);
    return 99;
  }

  (*x) = coord[0];
  (*y) = coord[1];
  (*z) = coord[2];

  return (0);
}

INT_ egads_eval_xyz_at_uv
 (INT_ idef,
  void *ext_cad_data,
  double u1,
  double u2,
  double *x1,
  double *x2,
  double *x3)

{
  /* idef: index, starts with 1
   * Evaluate xyz coordinates (x1,x2,x3) on a given surface (idef) at a given
   * uv location (u1,u2).
   */

  int    ierr, status;
  double uv[2], result[18];
  CHAR_133 Text;

  egads_cad_data *ptr = (egads_cad_data*)ext_cad_data;
  if (ptr == NULL || ptr->magic != MAGICNUMBER)
  {
    sprintf(Text,"*** EGADS ERROR : invalid ext_cad_data : %d ***", __LINE__);
    ug_error_message(Text);
    return 99;
  }

  int iface = idef-1;

  uv[0] = u1;
  uv[1] = u2;

  ierr = egads_eval_uvn_to_uv(idef, ext_cad_data, uv);

  if (ierr)
    return ierr;

  status = EG_evaluate((ego)ptr->modelFaces[iface], uv, result);
  if (status != EGADS_SUCCESS)
  {
    sprintf(Text,"*** EGADS ERROR %d : EG_evaluate for Face %d ***", status, idef);
    ug_error_message(Text);
    return -status;
  }

  *x1 = result[0];
  *x2 = result[1];
  *x3 = result[2];

  return 0;
}


INT_ egads_eval_curv_at_uv (INT_ idef,
                            void *ext_cad_data,
                            double u1,
                            double u2,
                            double uv_curv_lim,
                            double *curv_dir11,
                            double *curv_dir12,
                            double *curv_dir13,
                            double *curv_dir21,
                            double *curv_dir22,
                            double *curv_dir23,
                            double *curvature1,
                            double *curvature2)
{
  // For a given surface (idef) at a given uv location (u1,u2) evaluate the
  // surface curvature, curvature1 and curvature2, in principle directions 1
  // and 2. Also, evaluate the principle direction vectors
  // (curv_dir11,curv_dir12,curv_dir13 and curv_dir21,curv_dir22,curv_dir23).

  int ierr, status;

  double result[8], uv[2];
  CHAR_133 Text;

  egads_cad_data *ptr = (egads_cad_data*)ext_cad_data;
  if (ptr == NULL || ptr->magic != MAGICNUMBER)
  {
    sprintf(Text,"*** EGADS ERROR : invalid ext_cad_data : %d ***", __LINE__);
    ug_error_message(Text);
    return 99;
  }
  // curvature evaluation on the surface boundaries at uv=0,1 is unreliable
  // to compensate we limit the min/max uv values uv>lim && uv <1-lim

  uv[0] = MIN(1.0-uv_curv_lim, MAX(uv_curv_lim, u1));
  uv[1] = MIN(1.0-uv_curv_lim, MAX(uv_curv_lim, u2));

  ierr = egads_eval_uvn_to_uv(idef, ext_cad_data, uv);

  if (ierr)
    return ierr;

  status = EG_curvature ((ego)ptr->modelFaces[idef-1], uv, result);

  if (status == EGADS_DEGEN)
    return -1;
  else if (status != EGADS_SUCCESS)
  {
    ug_error_message ((char*)"*** EGADS ERROR 3209 : unable to evaluate curvature ***");
    return 3209;
  }

  *curv_dir11 = result[1];
  *curv_dir12 = result[2];
  *curv_dir13 = result[3];
  *curv_dir21 = result[5];
  *curv_dir22 = result[6];
  *curv_dir23 = result[7];

  *curvature1 = result[0];
  *curvature2 = result[4];

  return 0;
}

INT_ egads_eval_edge_uv (INT_ idef,
                         void *ext_cad_data,
                         INT_ edge_id,
                         INT_ sense_edge,
                         double t,
                         double uv[2])
{
  /*
    given parameter t of the edge, find the uv value of face.
    idef: face id
    edge_id: edge id
    sense_edge: can be 0, but must be specified  if the EDGE is found in the FACE twice.
    Value +1/-1 specifies which position in the LOOP to use.
  */

  int ierr, status;
  CHAR_133 Text;

  egads_cad_data *ptr = (egads_cad_data*)ext_cad_data;
  if (ptr == NULL || ptr->magic != MAGICNUMBER)
  {
    sprintf(Text,"*** EGADS ERROR : invalid ext_cad_data : %d ***", __LINE__);
    ug_error_message(Text);
    return 99;
  }

  int imodelFace = idef-1;
  int imodelEdge = edge_id -1;

  int ifaceCnt = 0;
  int iedgeCnt = 0;
  for ( const int bodyIndex : ptr->modelBodyIndex[EGTessModel::SOLIDINDEX] )
  {
    ifaceCnt += ptr->bodies[bodyIndex].nFaces();
    iedgeCnt += ptr->bodies[bodyIndex].nEdges();
  }

  // it's a sheet face
  if ( idef > ifaceCnt )
  {
    bool found = false;
    for ( std::size_t ibody = 0; ibody < ptr->modelEdgeIndex[EGTessModel::SHEETINDEX].size(); ibody++ )
    {
      for ( std::size_t ibodyEdge = 0; ibodyEdge < ptr->modelEdgeIndex[EGTessModel::SHEETINDEX][ibody].size(); ibodyEdge++)
      {
        imodelEdge = ptr->modelEdgeIndex[EGTessModel::SHEETINDEX][ibody][ibodyEdge];

        if (imodelEdge == edge_id-1)
        {
          imodelEdge = iedgeCnt;
          found = true;
          break;
        }
        iedgeCnt++;
      }
      if (found) break;
    }
    SANS_ASSERT(found);
  }

  typedef EGFace<3>::ParamCoord ParamCoord;
  typedef EGFace<3>::CartCoord CartCoord;

  for (const egads_cad_data::FloatingFaceEdge& floating : ptr->floatingFaceEdge)
  {
    if (floating.imodelFace == imodelFace && floating.imodelEdge == imodelEdge)
    {
      CartCoord X = ptr->modelEdges[imodelEdge](t);
      ParamCoord UV = ptr->modelFaces[imodelFace](X);
      uv[0] = UV[0];
      uv[1] = UV[1];

      ierr = egads_eval_uv_to_uvn (idef, ext_cad_data, uv);

      if (ierr)
        return ierr;

      return 0;
    }
  }

  status = EG_getEdgeUV((ego)ptr->modelFaces[imodelFace], (ego)ptr->modelEdges[imodelEdge], sense_edge, t, uv);

  if (status != EGADS_SUCCESS)
    return 9914;

  ierr = egads_eval_uv_to_uvn(idef, ext_cad_data, uv);

  if (ierr)
    return ierr;

  return 0;
}

INT_ egads_eval_arclen (INT_ edge_id,
                        void *ext_cad_data,
                        double u1,
                        double u2,
                        double *len)
{
  // Determine the arclen given parameter u1, u2 for an EGADS edge with edge_id.

  egads_cad_data *ptr = NULL;

  int stat;

  ptr = (egads_cad_data *) ext_cad_data;

  stat = EG_arcLength (ptr->modelEdges[edge_id-1], u1, u2, len);

  if (stat != EGADS_SUCCESS)
  {
    ug_error_message ((char*)"*** EGADS ERROR 3236 : unable to evaluate arc length on edge ***");
    return 3240;
  }

  return 0;
}

INT_ egads_cad_geom_setup (INT_ mmsg,
                           UG_Param_Struct *AFLR4_Param_Struct_Ptr,
                           INT_ *ncad,
                           INT_1D ** ibcicad,
                           INT_1D ** icmpicad,
                           INT_1D ** idicad,
                           DOUBLE_1D ** erwicad,
                           DOUBLE_1D ** sficad)
{

  /*
   *
   * Setup CAD data, read EGADS attributes, and set AFLR4 parameters.
   *
   * Input Parameters
   *
   *  mmsg      : input message flag
   *          mmsg =  0 : no messages
   *          mmsg = -1 : minimize messages
   *          mmsg =  1 : normal messages
   *          mmsg =  2 : maximize messages
   *  AFLR4_Param_Struct_Ptr  : AFLR4 input parameter structure
   *
   * Output Parameters
   *
   *  ncad      : number of unique CAD surface ID values
   *  ibcicad     : list of AFLR Grid BC values
   *          icad varies from 1 to ncad
   *  icmpicad      : list of sequential component ID values
   *          that vary from one to the number of components
   *          icad varies from 1 to ncad
   *  idicad      : list of actual unique surface ID values
   *          icad varies from 1 to ncad
   *  erwicad     : list of edge mesh spacing scale factor
   *          weights for each CAD surface
   *          icad varies from 1 to ncad
   *  sficad      : list of mesh spacing scale factors for each
   *          CAD surface
   *          icad varies from 1 to ncad
   *
   * Return Value
   *
   *  0     : no errors
   *  > 0   : there were errors
   *
   */

  CHAR_133 Text;
  INT_ idef, ierr;
  void *ext_cad_data=NULL;
  int AFLR_Cmp_ID_Offset = 1;

  aflr4_get_ext_cad_data (&ext_cad_data);

  egads_cad_data *ptr = (egads_cad_data*)ext_cad_data;
  if (ptr == NULL || ptr->magic != MAGICNUMBER)
  {
    sprintf(Text,"*** EGADS ERROR : invalid ext_cad_data : %d ***", __LINE__);
    ug_error_message(Text);
    return 99;
  }

  // get AFLR4 overall parameters
  double ff_cdfr, ref_len, ff_spacing;
  ug_get_double_param ((char*)"ff_cdfr", &ff_cdfr, AFLR4_Param_Struct_Ptr);
  ug_get_double_param ((char*)"ref_len", &ref_len, AFLR4_Param_Struct_Ptr);

  int i_ref_bbox_len;
  double bbox[6], bbox_all[6];

  // initialize overall bounding box

  bbox_all[0] = 1.0e300;;
  bbox_all[1] = 1.0e300;
  bbox_all[2] = 1.0e300;
  bbox_all[3] = -1.0e300;
  bbox_all[4] = -1.0e300;
  bbox_all[5] = -1.0e300;

  // initialize internal surface bounding box

  i_ref_bbox_len = 0;

  bbox[0] = 1.0e300;;
  bbox[1] = 1.0e300;
  bbox[2] = 1.0e300;
  bbox[3] = -1.0e300;
  bbox[4] = -1.0e300;
  bbox[5] = -1.0e300;

  // set number of unique CAD surface ID values

  *ncad = 0;
  for ( std::size_t ibody = 0; ibody < ptr->bodies.size(); ibody++ )
  {
    *ncad += ptr->bodies[ibody].nFaces();
    std::vector< EGFace<3> > faces = ptr->bodies[ibody].getFaces();
    for (std::size_t iface = 0; iface < faces.size(); iface++)
    {
      if ( faces[iface].hasAttribute("AFLR_Cmp_ID") )
      {
        int AFLR_Cmp_ID;
        faces[iface].getAttribute("AFLR_Cmp_ID", AFLR_Cmp_ID);
        AFLR_Cmp_ID_Offset = MAX(AFLR_Cmp_ID+1, AFLR_Cmp_ID_Offset);
      }

      std::vector<double> box = faces[iface].getBoundingBox();

      // set max/min bounding box of all surfaces

      bbox_all[0] = MIN (bbox_all[0], box[0]);
      bbox_all[1] = MIN (bbox_all[1], box[1]);
      bbox_all[2] = MIN (bbox_all[2], box[2]);
      bbox_all[3] = MAX (bbox_all[3], box[3]);
      bbox_all[4] = MAX (bbox_all[4], box[4]);
      bbox_all[5] = MAX (bbox_all[5], box[5]);

      if ( faces[iface].hasAttribute(AFLR_FARFIELD) ) continue;

      if ( faces[iface].hasAttribute("AFLR_GBC") )
      {
        std::string AFLR_GBC;
        faces[iface].getAttribute("AFLR_GBC", AFLR_GBC);
        if (AFLR_GBC.find("FARFIELD_UG3_GBC") != std::string::npos) continue;
      }

      ++i_ref_bbox_len;

      bbox[0] = MIN (bbox[0], box[0]);
      bbox[1] = MIN (bbox[1], box[1]);
      bbox[2] = MIN (bbox[2], box[2]);
      bbox[3] = MAX (bbox[3], box[3]);
      bbox[4] = MAX (bbox[4], box[4]);
      bbox[5] = MAX (bbox[5], box[5]);
    }
  }

  // set overall bounding box
  double bbox_len;
  bbox_len = bbox_all[3] - bbox_all[0];
  bbox_len = MAX (bbox_len, bbox_all[4] - bbox_all[1]);
  bbox_len = MAX (bbox_len, bbox_all[5] - bbox_all[2]);

  // set internal reference length

  double ref_bbox_len;
  if (i_ref_bbox_len > 0)
  {
    ref_bbox_len = bbox[3] - bbox[0];
    ref_bbox_len = MAX (ref_bbox_len, bbox[4] - bbox[1]);
    ref_bbox_len = MAX (ref_bbox_len, bbox[5] - bbox[2]);
  }
  else
    ref_bbox_len = bbox_len;

  double max_scale, min_scale;
  ug_get_double_param ((char*)"max_scale", &max_scale, AFLR4_Param_Struct_Ptr);
  ug_get_double_param ((char*)"min_scale", &min_scale, AFLR4_Param_Struct_Ptr);

  double min_spacing = min_scale*ref_len;
  double max_spacing = max_scale*ref_len;

  // set farfield spacing

  ff_spacing = (ff_cdfr - 1.0) * 0.5 * (bbox_len - ref_bbox_len)
             + 0.5 * (min_spacing + max_spacing);

  ug_set_double_param ((char*)"ff_spacing", ff_spacing, AFLR4_Param_Struct_Ptr);

  // allocate list of unique CAD surface ID values

  ierr = 0;

  *ibcicad  = (INT_1D    *) ug_malloc (&ierr, (*ncad+1) * sizeof (INT_1D));
  *icmpicad = (INT_1D    *) ug_malloc (&ierr, (*ncad+1) * sizeof (INT_1D));
  *idicad   = (INT_1D    *) ug_malloc (&ierr, (*ncad+1) * sizeof (INT_1D));
  *erwicad  = (DOUBLE_1D *) ug_malloc (&ierr, (*ncad+1) * sizeof (DOUBLE_1D));
  *sficad   = (DOUBLE_1D *) ug_malloc (&ierr, (*ncad+1) * sizeof (DOUBLE_1D));

  if (ierr != 0)
  {
    ug_free (*ibcicad);
    ug_free (*icmpicad);
    ug_free (*idicad);
    ug_free (*erwicad);
    ug_free (*sficad);
    SANS_DEVELOPER_EXCEPTION ("*** ERROR 102017 : unable to allocate required memory ***");
  }

  // set list of unique CAD surface geometry ID values and
  // set AFLR3 Grid BC values for each ID

  // predefined AFLR3 Grid BC values

  // STD_UG3_GBC    : standard surface
  // -STD_UG3_GBC   : standard BL generating surface
  // BL_INT_UG3_GBC   : surface that intersects BL region
  // TRANSP_SRC_UG3_GBC   : embedded/transparent surface that is converted
  //          to source nodes by AFLR3
  // TRANSP_BL_INT_UG3_GBC  : embedded/transparent surface that intersects
  //          BL region
  // TRANSP_UG3_GBC   : embedded/transparent surface
  // -TRANSP_UG3_GBC    : embedded/transparent BL generating surface
  // TRANSP_INTRNL_UG3_GBC  : embedded/transparent surface that is converted
  //          to internal faces by AFLR3
  // -TRANSP_INTRNL_UG3_GBC : embedded/transparent BL generating surface
  //          that is converted to internal faces by AFLR3
  // FIXED_BL_INT_UG3_GBC : fixed surface with a BL region that intersects
  //          volume BL region

  idef = 1; // Face index (1-based)

  for ( int ibodyTopo = 0; ibodyTopo < EGTessModel::NBODYTOPO; ibodyTopo++ )
  {
    INT_ BC_Flag = STD_UG3_GBC;
    if ( ibodyTopo == EGTessModel::SHEETINDEX )
      BC_Flag = TRANSP_UG3_GBC;

    for ( std::size_t ibody = 0; ibody < ptr->modelBodyIndex[ibodyTopo].size(); ibody++ )
    {
      const int bodyIndex = ptr->modelBodyIndex[ibodyTopo][ibody];

      int nface = ptr->bodies[bodyIndex].nFaces();
      std::vector< EGFace<3> > faces = ptr->bodies[bodyIndex].getFaces();

      for (int iface = 0; iface < nface; iface++)
      {
        if ( faces[iface].hasAttribute(AFLR_FARFIELD) )
          (*ibcicad)[idef] = FARFIELD_UG3_GBC;
        else
        {
          (*ibcicad)[idef] = BC_Flag;
        }

        // get and set component identifier for surface "icad"
        // if it is not set by the AFLR4_Cmp_ID attribute then it will default
        // to either to the body index, ibody, if their are multiple bodies
        // that are made up of standard faces/surfaces or to the surface index
        // determined for topologically closed surfaces, if there are multiple
        // closed surfaces that are made up of standard faces/surfaces

        if ( faces[iface].hasAttribute("AFLR_Cmp_ID") )
        {
          if (faces[iface].getAttributeType("AFLR_Cmp_ID") == ATTRINT)
          {
            faces[iface].getAttribute("AFLR_Cmp_ID", (*icmpicad)[idef]);
          }
          else if (faces[iface].getAttributeType("AFLR_Cmp_ID") == ATTRREAL)
          {
            double cmpID;
            faces[iface].getAttribute("AFLR_Cmp_ID", cmpID);
            (*icmpicad)[idef] = cmpID;
          }
        }
        else
          (*icmpicad)[idef] = bodyIndex + AFLR_Cmp_ID_Offset;

        // get and set edge mesh spacing refinement weight for surface "icad"
        // if it is not set by the AFLR4_Edge_Refinement_Weight attribute then
        // the edge mesh spacing scale factor weight will be set to the default
        // value of 0

        if ( faces[iface].hasAttribute("AFLR4_Edge_Refinement_Weight") )
          faces[iface].getAttribute("AFLR4_Edge_Refinement_Weight", (*erwicad)[idef]);
        else
          (*erwicad)[idef] = 0.0;

        // get and set surface mesh spacing scale factor for surface "icad"
        // if it is not set by the AFLR4_Scale_Factor attribute then the
        // surface mesh spacing scale factor will be set to the default value
        // of 1

        if ( faces[iface].hasAttribute("AFLR4_Scale_Factor") )
          faces[iface].getAttribute("AFLR4_Scale_Factor", (*sficad)[idef]);
        else
          (*sficad)[idef] = 1.0;

        (*idicad)[idef] = idef;
        idef++;
      }
    }
  }

  return 0;
}

INT_ egads_auto_cad_geom_setup ()
{
  // CAD geometry setup specific to setting up surfgen data struture for
  // automatic point spacing mode.

  void *ext_cad_data = nullptr;
  INT_1D *ibcicad = nullptr;
  INT_1D *icmpicad = nullptr;
  INT_1D *idicad = nullptr;
  DOUBLE_1D *sficad = nullptr;
  DOUBLE_1D *sfeicad = nullptr;
  INT_ ncad;
  INT_ icad, id, ibc, isurf; //local variables
  double sf = 1, sfe = 0;
  INT_ itype = 1; //surf type, component, composite, etc. ????
  INT_ ierr = 0;
  int      cnt;
  CHAR_133 Text;

  //get cad data
  aflr4_get_cad_id_list (&ncad, &ibcicad, &icmpicad, &idicad, &sficad, &sfeicad);
  aflr4_get_ext_cad_data (&ext_cad_data);

  egads_cad_data *ptr = (egads_cad_data*)ext_cad_data;
  if (ptr == NULL || ptr->magic != MAGICNUMBER)
  {
    sprintf(Text,"*** EGADS ERROR : invalid ext_cad_data : %d ***", __LINE__);
    ug_error_message(Text);
    return 99;
  }

  //add surfs
  for ( icad = 1; icad <= ncad; ++icad)
  {
    id  = idicad[icad];
    ibc = ibcicad[icad];
    sf  = sficad[icad];
    sfe = sfeicad[icad];
    surfgen_add_surf(id, id, ibc, itype, sf, sfe);
  }

  //add edges
  int imodelCnt = 0;
  for ( int ibodyTopo = 0; ibodyTopo < EGTessModel::NBODYTOPO; ibodyTopo++ )
  {
    for ( std::size_t ibody = 0; ibody < ptr->modelEdgeIndex[ibodyTopo].size(); ibody++ )
    {
      for ( std::size_t ibodyEdge = 0; ibodyEdge < ptr->modelEdgeIndex[ibodyTopo][ibody].size(); ibodyEdge++)
      {
        int imodelEdge = ptr->modelEdgeIndex[ibodyTopo][ibody][ibodyEdge];

        id = imodelEdge+1;
        if (imodelCnt++ != imodelEdge) continue; //Equivalent sheet edge with solid edge cannot be initialized twice

        surfgen_add_line_id(id);
      }
    }
  }

  //add nodes
  imodelCnt = 0;
  for ( int ibodyTopo = 0; ibodyTopo < EGTessModel::NBODYTOPO; ibodyTopo++ )
  {
    for ( std::size_t ibody = 0; ibody < ptr->modelBodyIndex[ibodyTopo].size(); ibody++ )
    {
      for ( std::size_t ibodyNode = 0; ibodyNode < ptr->modelNodeIndex[ibodyTopo][ibody].size(); ibodyNode++ )
      {
        int imodelNode = ptr->modelNodeIndex[ibodyTopo][ibody][ibodyNode];

        id = imodelNode+1;
        if (imodelCnt++ != imodelNode) continue; //Equivalent sheet node with solid node cannot be initialized twice

        const EGADS::EGNode<3>& node = ptr->modelNodes[imodelNode];
        surfgen_add_node(id, node[0], node[1], node[2]);
      }
    }
  }

  // set up edge2node and tlimits
  imodelCnt = 0;
  for ( int ibodyTopo = 0; ibodyTopo < EGTessModel::NBODYTOPO; ibodyTopo++ )
  {
    for ( std::size_t ibody = 0; ibody < ptr->modelEdgeIndex[ibodyTopo].size(); ibody++ )
    {
      for ( std::size_t ibodyEdge = 0; ibodyEdge < ptr->modelEdgeIndex[ibodyTopo][ibody].size(); ibodyEdge++)
      {
        int imodelEdge = ptr->modelEdgeIndex[ibodyTopo][ibody][ibodyEdge];

        id = imodelEdge+1;
        if (imodelCnt++ != imodelEdge) continue; //Equivalent sheet edge with solid edge cannot be initialized twice

#if 0
        // add the scale factor to the edge if present
        if (ptr->modelEdges[imodelEdge].hasAttribute("AFLR4_Scale_Factor"))
        {
          Real AFLR_Scale_Factor;
          ptr->modelEdges[imodelEdge].getAttribute("AFLR4_Scale_Factor", AFLR_Scale_Factor);

          SurfGen_Line* line = surfgen_find_line(id);
          SANS_ASSERT( line != nullptr );
          line->sf = AFLR_Scale_Factor;
        }
#endif

        EGEdge<3>::ParamRange tlimits = ptr->modelEdges[imodelEdge].getParamRange();

        int index0 = ptr->edgeNodesIndex[imodelEdge][0];
        int index1 = ptr->edgeNodesIndex[imodelEdge][1];

        // indicate a one-node edge
        //if (index0 == index1) index1 = -1;

        int type = -1;
        if (ptr->modelEdges[imodelEdge].isTwoNode())
          type = 0;
        else if (ptr->modelEdges[imodelEdge].isOneNode())
          type = 1;
        else if (ptr->modelEdges[imodelEdge].isDegenerate())
          type = 2;
        else
          SANS_DEVELOPER_EXCEPTION("Unknown edge type");

        surfgen_set_edge_range_type(id, tlimits[0], tlimits[1], type);
        surfgen_set_ref_edge_node(id, index0, index1);//set up the ref from edge to nodes
      }
    }
  }

  // storage for all the loops
  std::unique_ptr<INT_2D[]> loop_edges( new INT_2D[ptr->modelEdges.size()+1] );

  //set up loops in surfs
  isurf = 1;
  for ( std::size_t ibodyTopo = 0; ibodyTopo < EGTessModel::NBODYTOPO; ibodyTopo++ )
  {
    for ( std::size_t ibody = 0; ibody < ptr->modelBodyIndex[ibodyTopo].size(); ibody++ )
    {
      int bodyIndex = ptr->modelBodyIndex[ibodyTopo][ibody];
      std::vector< EGFace<3> > faces = ptr->bodies[bodyIndex].getFaces();
      const int nfaces = ptr->bodies[bodyIndex].nFaces();

      for (int iface = 0; iface < nfaces; iface++)
      {
        std::vector< EGLoop<3> > loops = faces[iface].getLoops();

        surfgen_set_nloop(isurf, loops.size());

        cnt = 0;
        std::size_t iloop = 0;
        for (iloop = 0; iloop < loops.size(); iloop++)
        {
          std::vector< EGEdge<3> > loopEdges = loops[iloop].getEdges();
          std::vector< int >       senses    = loops[iloop].getEdgeSenses();

          for (std::size_t iloopEdge = 0; iloopEdge < loopEdges.size(); iloopEdge++)
          {
            int ibodyEdge = ptr->bodies[bodyIndex].getBodyIndex(loopEdges[iloopEdge]) - 1;
            int imodelEdge = ptr->modelEdgeIndex[ibodyTopo][ibody][ibodyEdge];

            //overall, sense_edge = 1 means face is to the left side of edge
            loop_edges[cnt][0] = imodelEdge+1;
            loop_edges[cnt][1] = senses[iloopEdge];
            cnt++;
            surfgen_set_ref_surf_edge(isurf, imodelEdge+1);//set up the ref from edge to surfs
          }
          surfgen_add_loop(isurf, iloop, cnt, loop_edges.get());
        }

#if 0  // TODO: AFLR4 does not work with a single edge on a face yet

        // loops from floating edges
        const std::vector< EGTessModel::FloatingEdge >& floatingFaceEdges = ptr->floatingEdges[bodyIndex][iface];

        // construct the floating ploops (no need to traverse twice)
        std::map<int, std::vector<int>> floating_loops;
        for ( auto floatingEdge = floatingFaceEdges.begin(); floatingEdge != floatingFaceEdges.end(); floatingEdge++ )
        {
          std::pair<int,int> model_body = ptr->modelBodyInvIndex[floatingEdge->bodyIndex];        // Model index of the body

          int imodelEdge = ptr->modelEdgeIndex[model_body.first][model_body.second][floatingEdge->edgeIndex]; // model index of the Edge

          floating_loops[model_body.second].push_back(imodelEdge);
          surfgen_set_ref_surf_edge(isurf, imodelEdge+1);//set up the ref from edge to surfs

          ptr->floatingFaceEdge.push_back({isurf-1, imodelEdge});
        }

        // increase the number of loops
        SurfGen_Surf* surf = surfgen_find_surf(isurf);
        surf->loops.resize(surf->loops.size()+floating_loops.size());

        // transfer over the loops
        for (const std::pair<int, std::vector<int>>& body_loop : floating_loops)
        {
          cnt = 0;
          for (const int imodelEdge : body_loop.second)
          {
            loop_edges[cnt][0] = imodelEdge+1;
            loop_edges[cnt][1] = 1;
            cnt++;
          }
          surfgen_add_loop(isurf, iloop, cnt, loop_edges.data());
          iloop++;
        }
#endif

        isurf++;
      }
    }
  }

  return ierr;
}

void egads_cad_geom_data_cleanup (int close_context)
{
  // C++ destructors clean up everything
  return;
}

} // namespace

//===========================================================================//

struct AFLRcleanup
{
  UG_Param_Struct *Param_Struct_Ptr; // AFRL4 input structure

  AFLRcleanup() : Param_Struct_Ptr(NULL)
  {
    INT_ mmsg = 0;
    int prog_argc = 1;
    char **prog_argv = NULL;

    SANS_ASSERT( 0 == ug_add_new_arg (&prog_argv, (char*)"allocate_and_initialize_argv") );

    // Malloc, initialize, and setup AFLR4 input parameter structure.

    SANS_ASSERT( 0 == aflr4_setup_param (&mmsg, 0, prog_argc, prog_argv, &Param_Struct_Ptr) );

    //Free the arguments
    ug_free_argv (prog_argv);

#if 0
    SANS_ASSERT( 0 == ug_malloc_param (&Param_Struct_Ptr) );
    SANS_ASSERT( Param_Struct_Ptr != NULL );

    SANS_ASSERT( 0 == ug_initialize_param (Param_Struct_Ptr) );
    SANS_ASSERT( 0 == ug_initialize_aflr_param (4, Param_Struct_Ptr) );
    SANS_ASSERT( 0 == aflr2_initialize_param (Param_Struct_Ptr) );
    SANS_ASSERT( 0 == aflr4_initialize_param (Param_Struct_Ptr) );
#endif
  }

  ~AFLRcleanup()
  {
    // Free all CAD related array space (and re-compress CAD input data file).
    // Modifications or replacement of this routine may be required for
    // alternative CAD systems and implementations.

//    aflr4_cad_geom_data_cleanup(0);
//    aflr4_discrete_geom_data_cleanup ();
//    dftr2_eval_free ();
//    dftr3_eval_free ();
//    aflr4_grid_data_cleanup ();
//    aflr4_free_surf_bg ();
//    dgeom_def_free_all ();
//    surfgen_free ();
//    dcurv_discrete_edge_geom_cleanup();
//    dcurv_discrete_curv_cleanup();

    aflr4_free_all (0);

    ug_free_param(Param_Struct_Ptr);
    ug_shutdown ();
  }

};


void
EGTessModel::AFLR4Faces(const PyDict& dict, const std::vector< EGBody<3> >& bodies, std::vector<ego>& tess)
{
  std::cout << "Generating AFLR4 surface mesh..." << std::endl;

  AFLRcleanup AFLR4;

  // Container for all the EGADS cad data used by AFLR4
  egads_cad_data cad_data;

  // Construct an inverse map so that the body-type and type specific index can be retrieved from a global body index
  FloatingEdgeMap floatingEdges; // [ibody][iface][floating]
  std::vector< std::pair<int,int> > modelBodyInvIndex;
  fillFloatingEdgeMap(floatingEdges, modelBodyInvIndex);

  for ( int ibodyTopo = 0; ibodyTopo < NBODYTOPO; ibodyTopo++ )
  {
    cad_data.modelBodyIndex[ibodyTopo] = modelBodyIndex_[ibodyTopo];
    cad_data.modelEdgeIndex[ibodyTopo] = modelEdgeIndex_[ibodyTopo];
    cad_data.modelNodeIndex[ibodyTopo] = modelNodeIndex_[ibodyTopo];
  }

  cad_data.bodies = bodies;
  cad_data.modelFaces = modelFaces_;
  cad_data.modelEdges = modelEdges_;
  cad_data.modelNodes = modelNodes_;
  cad_data.edgeNodesIndex = edgeNodesIndex_;

  cad_data.modelBodyInvIndex = modelBodyInvIndex;
  cad_data.floatingEdges = floatingEdges;


  int Message_Flag = 0; // AFRL4 message return flag

  INT_ min_ncell             = dict.get(AFLR4Params::params.min_ncell);
  INT_ mer_all               = dict.get(AFLR4Params::params.mer_all);

  double ff_cdfr             = dict.get(AFLR4Params::params.ff_cdfr);
  double abs_min_scale       = dict.get(AFLR4Params::params.abs_min_scale);
  double BL_thickness        = dict.get(AFLR4Params::params.BL_thickness);
  double curv_factor         = dict.get(AFLR4Params::params.curv_factor);
  double length_ratio        = dict.get(AFLR4Params::params.length_ratio);
  double max_scale           = dict.get(AFLR4Params::params.max_scale);
  double min_scale           = dict.get(AFLR4Params::params.min_scale);
  double ref_len             = dict.get(AFLR4Params::params.ref_len);
  double erw_all             = dict.get(AFLR4Params::params.erw_all);

  INT_ mdf                   = dict.get(AFLR4Params::params.mdf);
  double cdfr                = dict.get(AFLR4Params::params.cdfr);


  // register geometry type: CAD geom_type = 1, discrete geom_type = 2
  ug_set_int_param ((char*)"geom_type", 1, AFLR4.Param_Struct_Ptr);

  // set AFLR4 parameters

  ug_set_int_param ((char*)"min_ncell", min_ncell, AFLR4.Param_Struct_Ptr);
  ug_set_int_param ((char*)"mer_all", mer_all, AFLR4.Param_Struct_Ptr);

  ug_set_double_param ((char*)"ff_cdfr", ff_cdfr, AFLR4.Param_Struct_Ptr);
  ug_set_double_param ((char*)"abs_min_scale", abs_min_scale, AFLR4.Param_Struct_Ptr);
  ug_set_double_param ((char*)"BL_thickness", BL_thickness, AFLR4.Param_Struct_Ptr);
  ug_set_double_param ((char*)"curv_factor", curv_factor, AFLR4.Param_Struct_Ptr);
  ug_set_double_param ((char*)"length_ratio", length_ratio, AFLR4.Param_Struct_Ptr);
  ug_set_double_param ((char*)"max_scale", max_scale, AFLR4.Param_Struct_Ptr);
  ug_set_double_param ((char*)"min_scale", min_scale, AFLR4.Param_Struct_Ptr);
  ug_set_double_param ((char*)"ref_len", ref_len, AFLR4.Param_Struct_Ptr);
  ug_set_double_param ((char*)"erw_all", erw_all, AFLR4.Param_Struct_Ptr);

  ug_set_int_param ((char*)"mdf", mdf, AFLR4.Param_Struct_Ptr);
  //ug_set_double_param ((char*)"cdfs", 0.0, AFLR4.Param_Struct_Ptr);
  ug_set_double_param ((char*)"cdfr", cdfr, AFLR4.Param_Struct_Ptr);

  //ug_set_char_param ((char*)"-no_prox", (char*)"", AFLR4.Param_Struct_Ptr);

  INT_ nproc = 1, parallel_mode = 0;
  ug_set_int_param ((char*)"nproc", nproc, AFLR4.Param_Struct_Ptr);
  ug_set_int_param ((char*)"parallel_mode", parallel_mode, AFLR4.Param_Struct_Ptr);

  //ug_set_int_param ((char*)"auto_mode", 0, AFLR4.Param_Struct_Ptr);

  // Register EGADS routines for CAD related setup and cleanup along with
  // routines for cad evaluation, cad bounds and generating boundary edge grids.

  aflr4_register_cad_geom_setup(egads_cad_geom_setup);
  aflr4_register_cad_geom_data_cleanup (egads_cad_geom_data_cleanup);
  aflr4_register_auto_cad_geom_setup (egads_auto_cad_geom_setup);

  dgeom_register_cad_eval_curv_at_uv (egads_eval_curv_at_uv);
  dgeom_register_cad_eval_xyz_at_uv (egads_eval_xyz_at_uv);
  dgeom_register_cad_eval_uv_bounds (egads_eval_uv_bounds);
  dgeom_register_cad_eval_bedge (surfgen_cad_eval_bedge);

//  dgeom_register_discrete_eval_bedge (surfgen_discrete_eval_bedge);

  egen_auto_register_cad_eval_xyz_at_u (egads_eval_xyz_at_u);
  egen_auto_register_cad_eval_edge_uv (egads_eval_edge_uv);
  egen_auto_register_cad_eval_arclen (egads_eval_arclen);

  // save the external cad data
  aflr4_set_ext_cad_data (&cad_data);

  // Set labels and flags for all CAD surface component, discrete surface
  // component, standard composite, and one glue-only composite definitions.
  // Also set automatic point spacing data.
  SANS_ASSERT( 0 == aflr4_geom_setup(1, Message_Flag, AFLR4.Param_Struct_Ptr) );

  // Generate the initial edge grid.
  SANS_ASSERT( 0 == aflr4_auto_cad_geom_setup() );

  // Setup lines.
  surfgen_surf2line_setup ();

  // modify the scale factor on select edges and update the edge grid
  for ( int ibodyTopo = 0; ibodyTopo < EGTessModel::NBODYTOPO; ibodyTopo++ )
  {
    for ( std::size_t ibody = 0; ibody < modelBodyIndex_[ibodyTopo].size(); ibody++ )
    {
      for ( std::size_t ibodyEdge = 0; ibodyEdge < modelEdgeIndex_[ibodyTopo][ibody].size(); ibodyEdge++ )
      {
        int imodelEdge = modelEdgeIndex_[ibodyTopo][ibody][ibodyEdge];

        if (cad_data.modelEdges[imodelEdge].isDegenerate()) continue;

        if (cad_data.modelEdges[imodelEdge].hasAttribute("AFLR4_Edge_Scale_Factor"))
        {
          double AFLR_Scale_Factor;
          cad_data.modelEdges[imodelEdge].getAttribute("AFLR4_Edge_Scale_Factor", AFLR_Scale_Factor);

          SurfGen_Line* line = surfgen_find_line_cpp(imodelEdge+1);
          SANS_ASSERT(line != NULL);
          line->sf = AFLR_Scale_Factor;
        }
      }

#if 0
      for ( std::size_t ibodyNode = 0; ibodyNode < modelNodeIndex_[ibodyTopo][ibody].size(); ibodyNode++ )
      {
        int imodelNode = modelNodeIndex_[ibodyTopo][ibody][ibodyNode];

        if (cad_data.modelNodes[imodelNode].hasAttribute("AFLR4_Scale_Factor"))
        {
          double AFLR_Scale_Factor;
          cad_data.modelNodes[imodelNode].getAttribute("AFLR4_Scale_Factor", AFLR_Scale_Factor);

          SurfGen_Node *node = surfgen_find_node_cpp(imodelNode+1);
          SANS_ASSERT(node != NULL);
          node->sf = AFLR_Scale_Factor;
          sfUpdate = true;
        }
      }
#endif
    }
  }
  
  // Discretize edges according to curvature.

  surfgen_redistribute( AFLR4.Param_Struct_Ptr, 4, 1.0);

  // Define discrete and/or CAD geometry component surfaces within the DGEOM
  // data structure. Discrete component surface grids are extracted from a
  // given complete discrete surface grid with multiple components and a
  // uv-map is generated. Those definitions that are part of a composite are
  // added to the composite and a uv-map is generated for each standard
  // composite definition.

  SANS_ASSERT( 0 == aflr4_geom_def(Message_Flag, AFLR4.Param_Struct_Ptr) );

  // Generate optimal surface grids for all active discrete and/or CAD
  // geometry component surfaces along with all standard composite surfaces
  // and store data within the DGEOM data structure.

  SANS_ASSERT( 0 == aflr4_grid_gen(Message_Flag, AFLR4.Param_Struct_Ptr) );

  // Turn on option for background grid evaluation of length-scale.

  ug_set_int_param ((char*)"meval_opt", 1, AFLR4.Param_Struct_Ptr);

  // Modify the surface mesh spacing and re-generate the surface mesh
  // based on surface curvature and
  // locally modify the surface mesh spacing and re-generate the surface mesh
  // for cases with multiple components in close proximity.

  SANS_ASSERT( 0 == aflr4_auto_grid_gen (Message_Flag, AFLR4.Param_Struct_Ptr) );

  std::cout << "AFLR4 success!" << std::endl;

#if 0
  {
    std::string filename = "AFLR4_uv.dat";
    std::ofstream file(filename);
    file << std::setprecision(14) << std::scientific;

    std::cout << "Writing " << filename << std::endl;

    file << "VARIABLES = X, Y, Z, u, v,"
        "curv_dir11, "
        "curv_dir12, "
        "curv_dir13, "
        "curv_dir21, "
        "curv_dir22, "
        "curv_dir23, "
        "curvature1, "
        "curvature2, "
        << std::endl;

    for ( int ibodyTopo = 0; ibodyTopo < EGTessModel::NBODYTOPO; ibodyTopo++ )
    {
      for ( std::size_t ibody = 0; ibody < modelBodyIndex_[ibodyTopo].size(); ibody++ )
      {
        const int bodyIndex = modelBodyIndex_[ibodyTopo][ibody];

        const EGBody<3>& body = bodies[bodyIndex];
        std::vector< EGFace<3> > faces = body.getFaces();

        for (std::size_t iface = 0; iface < faces.size(); iface++ )
        {
          // AFRL4 output arrays
          INT_ numTriFace=0, numNodes=0, numQuadFace=0;
          INT_ noquad=1; // Convert quads to triangles
          INT_1D *bcFlag=NULL;
          INT_1D *blFlag=NULL;
          INT_3D *triCon=NULL;
          INT_4D *quadCon=NULL;
          DOUBLE_2D *uv=NULL;
          DOUBLE_3D *xyz=NULL;

          SANS_ASSERT( 0 == aflr4_get_def (idef, noquad, &numTriFace,
                                           &numNodes,
                                           &numQuadFace,
                                           &blFlag, &bcFlag, &triCon, &quadCon, &uv, &xyz) );

          SANS_ASSERT(numQuadFace == 0); //Not dealign with quads

          file << "ZONE T=\"idef " << idef << "\" "
              << "N = " << numNodes << " E = " << numTriFace << " ET = TRIANGLE F=FEPOINT" << std::endl;

          for (INT_ i = 0; i < numNodes; i++)
          {
            file << xyz[i+1][0] << " "
                << xyz[i+1][1] << " "
                << xyz[i+1][2] << " "
                << uv[i+1][0] << " "
                << uv[i+1][1] << " ";

            double curv_dir11;
            double curv_dir12;
            double curv_dir13;
            double curv_dir21;
            double curv_dir22;
            double curv_dir23;
            double curvature1;
            double curvature2;

            dgeom_eval_curv_at_uv (idef,
                                   uv[i+1][0],
                                   uv[i+1][1],
                                   &curv_dir11,
                                   &curv_dir12,
                                   &curv_dir13,
                                   &curv_dir21,
                                   &curv_dir22,
                                   &curv_dir23,
                                   &curvature1,
                                   &curvature2);

            file << curv_dir11 << " "
                << curv_dir12 << " "
                << curv_dir13 << " "
                << curv_dir21 << " "
                << curv_dir22 << " "
                << curv_dir23 << " "
                << curvature1 << " "
                << curvature2 << std::endl;
          }

          for (INT_ i = 0; i < numTriFace; i++)
            file << triCon[i+1][0] << " "
            << triCon[i+1][1] << " "
            << triCon[i+1][2] << std::endl;

          ug_free (bcFlag);
          ug_free (blFlag);
          ug_free (triCon);
          ug_free (quadCon);
          ug_free (uv);
          ug_free (xyz);
        }
      }
    }
  }
#endif

  INT_ idef = 1; // Face index (1-based)

  // put the AFLR4 tessellation back onto the EGADS objects
  for ( int ibodyTopo = 0; ibodyTopo < EGTessModel::NBODYTOPO; ibodyTopo++ )
  {
    for ( std::size_t ibody = 0; ibody < modelBodyIndex_[ibodyTopo].size(); ibody++ )
    {
      const int bodyIndex = modelBodyIndex_[ibodyTopo][ibody];

      //std::cout << "Body : " << bodyIndex << std::endl;

      const EGBody<3>& body = bodies[bodyIndex];
      std::vector< EGFace<3> > faces = body.getFaces();
      std::vector< EGEdge<3> > edges = body.getEdges();

      SANS_ASSERT( tess[bodyIndex] == nullptr );
      EG_STATUS( EG_initTessBody((ego)body, &tess[bodyIndex]) );

      for ( std::size_t ibodyEdge = 0; ibodyEdge < modelEdgeIndex_[ibodyTopo][ibody].size(); ibodyEdge++ )
      {
        int imodelEdge = modelEdgeIndex_[ibodyTopo][ibody][ibodyEdge];

        if (cad_data.modelEdges[imodelEdge].isDegenerate()) continue;

        int nnodes_gen;
        DOUBLE_1D *u_gen = NULL;
        DOUBLE_3D *x_gen = NULL;

        SANS_ASSERT(0 == surfgen_get_line_tess(imodelEdge+1, &nnodes_gen, &u_gen, &x_gen) );

        // xyz values for the edge mesh to be passed to EGADS
        std::vector<double> egads_xyz(3*nnodes_gen);
        std::vector<double> egads_t(nnodes_gen);

        for (int i = 0; i < nnodes_gen; i++)
        {
          egads_xyz[3*i+0] = x_gen[i+1][0];
          egads_xyz[3*i+1] = x_gen[i+1][1];
          egads_xyz[3*i+2] = x_gen[i+1][2];

          egads_t[i] = u_gen[i+1];
        }

        EG_STATUS( EG_setTessEdge(tess[bodyIndex], ibodyEdge+1,
                                  nnodes_gen, egads_xyz.data(), egads_t.data()) );
      }

      for (std::size_t iface = 0; iface < faces.size(); iface++ )
      {
        //if ( faces[iface].hasAttribute(EGTessModel::TRIANGLE) ) continue;

        // AFRL4 output arrays
        INT_ numTriFace=0, numNodes=0, numQuadFace=0;
        INT_ mchk_nrml=1;
        INT_ noquad=1; // Convert quads to triangles
        INT_1D *bcFlag=NULL;
        INT_1D *blFlag=NULL;
        INT_3D *triCon=NULL;
        INT_4D *quadCon=NULL;
        DOUBLE_2D *uv=NULL;
        DOUBLE_3D *xyz=NULL;

        SANS_ASSERT( 0 == aflr4_get_def (idef, mchk_nrml, noquad, &numTriFace,
                                         &numNodes,
                                         &numQuadFace,
                                         &blFlag, &bcFlag, &triCon, &quadCon, &uv, &xyz) );

        SANS_ASSERT(numQuadFace == 0); //Can't deal with quads... EGADS limitation

        // xyz values for the triangle mesh to be passed to EGADS
        std::vector<double> egads_xyz(3*numNodes);
        std::vector<double> egads_uv(2*numNodes);
        std::vector<int> egads_type(numNodes, -1);
        std::vector<int> egads_index(numNodes, -1);
        std::vector<int> egads_tris(3*numTriFace, -1);


        double uvbox[4] = {0, 1, 0, 1};

        if (egads_normalize())
        {
          int per;
          EG_STATUS( EG_getRange((ego)cad_data.modelFaces[idef-1], uvbox, &per) );
        }

        // Copy over physical and uv-coordinates
        for (INT_ i = 0; i < numNodes; i++)
        {
          egads_xyz[3*i+0] = xyz[i+1][0];
          egads_xyz[3*i+1] = xyz[i+1][1];
          egads_xyz[3*i+2] = xyz[i+1][2];

          if (egads_normalize())
          {
            // undo the uv normalization
            uv[i+1][0] = uvbox[0] + uv[i+1][0] * (uvbox[1] - uvbox[0]);
            uv[i+1][1] = uvbox[2] + uv[i+1][1] * (uvbox[3] - uvbox[2]);
          }

          egads_uv[2*i+0] = uv[i+1][0];
          egads_uv[2*i+1] = uv[i+1][1];
        }

        // copy over triangulation
        for (INT_ i = 0; i < numTriFace; i++)
        {
          egads_tris[3*i+0] = triCon[i+1][0];
          egads_tris[3*i+1] = triCon[i+1][1];
          egads_tris[3*i+2] = triCon[i+1][2];
        }

        ug_free (bcFlag);
        ug_free (blFlag);
        ug_free (triCon);
        ug_free (quadCon);
        ug_free (uv);
        ug_free (xyz);


        ego geom, *pchld;
        int oclass, mtype, nchld, *sens;
        double range[4];
        EG_STATUS( EG_getTopology((ego)faces[iface], &geom, &oclass, &mtype,
                                  range, &nchld, &pchld, &sens) );

        if ( mtype == SREVERSE )
        {
          // If the face is reversed, the triangle orientation needs to be swapped.
          // This is done by swapping any two nodes on the triangles (the first two are part of any)
          for (INT_ i = 0; i < numTriFace; i++)
            std::swap(egads_tris[3*i+0], egads_tris[3*i+1]);
        }

        //std::cout << "Setting tess face" << std::endl;
        EG_STATUS( EG_setTessFace(tess[bodyIndex], iface+1,
                                  numNodes, egads_xyz.data(), egads_uv.data(),
                                  numTriFace, egads_tris.data()) );
        //std::cout << "Setting tess face done" << std::endl;

#if 0
        for (std::size_t i = 0; i < cad_data.loops_index[idef-1].size(); i++)
        {
          egads_type[i]  = cad_data.loops_type[idef-1][i];
          egads_index[i] = cad_data.loops_index[idef-1][i];
        }

        if ( floatingEdges[bodyIndex][iface].size() > 0 )
        {
          // Store the type inedex for the points so they can be overwritten in the body zippering process
          faces[iface].addAttribute("->type",egads_type);
          faces[iface].addAttribute("->index",egads_index);
        }
#endif

        idef++;
      }

      // close the EGADS tessellation
      ego dummyBody;
      int state, np;
      EG_STATUS( EG_statusTessBody(tess[bodyIndex], &dummyBody, &state, &np) );
    }
  }
}

} // namepsace EGADS
} // namespace SANS

#endif
