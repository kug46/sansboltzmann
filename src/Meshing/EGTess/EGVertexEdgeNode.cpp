// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "EGVertexEdgeNode.h"

namespace SANS
{
namespace EGADS
{

template<int Dim_>
void
EGVertexEdgeNode<Dim_>::update(Real t)
{
  //Do nothing. A Node cannot be moved
}

//Explicitly instantiate the class
template class EGVertexEdgeNode<2>;

}
}
