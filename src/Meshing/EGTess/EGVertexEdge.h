// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EG_VERTEX_EDGE_H
#define EG_VERTEX_EDGE_H

//A vertex associated with an edge

#include <typeinfo>

#include "Meshing/EGADS/EGEdge.h"
#include "EGVertex.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

namespace SANS
{
namespace EGADS
{

template<int Dim_>
class EGVertexEdge : public EGVertex<Dim_>
{
public:
  using EGVertex<Dim_>::Dim;
  typedef DLA::VectorS<Dim, Real> CartCoord;
  typedef DLA::VectorS<2, Real> ParamRange;
  typedef typename EGEdge<Dim>::JinvType JinvType;

  EGVertexEdge( const EGVertexEdge& ve ) : EGVertex<Dim>(ve), edge_(ve.edge_), t_(ve.t_) {}
  EGVertexEdge( const EGEdge<Dim>& edge, const Real t, CartCoord* X, const bool fixed );
  virtual ~EGVertexEdge();

  EGVertexEdge& operator=( const EGVertexEdge& ve );

  virtual const std::type_info& typeID() const { return typeid(*this); }

  void update( const Real t);
  Real getParam() const { return t_; }
  JinvType Jinv() const { return edge_->Jinv(t_); }

  const EGEdge<Dim>& edge() const { return *edge_; }

  //Used to sort vertexes
  bool operator<( const EGVertexEdge& ve ) const { return t_ < ve.t_; }
protected:
  const EGEdge<Dim>* edge_;
  Real t_;
};

}
}

#endif //EG_VERTEX_EDGE_H
