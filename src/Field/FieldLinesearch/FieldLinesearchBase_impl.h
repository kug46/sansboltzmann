// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(FIELDLINESEARCHBASE_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "FieldLinesearchBase.h"

namespace SANS
{

template<class PhysDim, class TopoDim, class ArrayQ>
FieldLinesearchBase<PhysDim, TopoDim, ArrayQ>::FieldLinesearchBase( const XField<PhysDim, TopoDim>& xfld ) : BaseType(xfld)
{
  //Derived classes call createCellGroup
}

template<class PhysDim, class TopoDim, class ArrayQ>
template<class Topology>
void
FieldLinesearchBase<PhysDim, TopoDim, ArrayQ>::
createCellGroup(const int group, const int order, const BasisFunctionCategory& category)
{
  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupClass;
  typedef typename BaseType::template FieldCellGroupType<Topology> FieldCellGroupClass;
  typedef typename XFieldCellGroupClass::BasisType BasisType;

  const XFieldCellGroupClass& xfldGroup = xfld_.template getCellGroup<Topology>(group);

  const BasisType* basis = BasisType::getBasisFunction(order, category);

  // create field associativity constructor
  typename FieldCellGroupClass::FieldAssociativityConstructorType fldAssocCell( basis, xfldGroup.nElem() );

  // DOF ordering: Fortran-array DOF(basis,cell)
  const int nElem = xfldGroup.nElem();
  const int nBasis = basis->nBasis();
  std::vector<int> map( nBasis );
  for ( int elem = 0; elem < nElem; elem++ )
  {
    for ( int i = 0; i < nBasis; i++ )
      map[i] = nDOF_ + nBasis*elem + i;

    fldAssocCell.setAssociativity( elem ).setRank( xfldGroup.associativity( elem ).rank() );
    fldAssocCell.setAssociativity( elem ).setGlobalMapping( map );
  }

  int localGroup = localCellGroups_.at(group);

  // allocate group
  cellGroups_[localGroup] = new FieldCellGroupClass( fldAssocCell );

  // accumulate DOF count
  nDOF_ += nElem*nBasis;
  nElem_ += nElem;
}

//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class ArrayQ>
template<class Topology>
void
FieldLinesearchBase<PhysDim, TopoDim, ArrayQ>::
createInteriorTraceGroup(const int group, const int order, const BasisFunctionCategory& category)
{
  typedef typename XField<PhysDim, TopoDim>:: template FieldTraceGroupType<Topology> XFieldTraceClass;
  typedef typename BaseType::template FieldTraceGroupType<Topology> FieldTraceClass;
  typedef typename FieldTraceClass::BasisType BasisType;

  const XFieldTraceClass& xfldGroup = xfld_.template getInteriorTraceGroup<Topology>(group);

  const BasisType* basis = BasisType::getBasisFunction(order, category);

  // create field associativity constructor
  typename FieldTraceClass::FieldAssociativityConstructorType fldAssocTrace( basis, xfldGroup.nElem() );

  // DOF ordering: Fortran-array DOF(basis,elem)
  const int nElem = xfldGroup.nElem();
  const int nBasis = basis->nBasis();
  std::vector<int> map( nBasis );
  for (int elem = 0; elem < nElem; elem++)
  {
    for (int i = 0; i < nBasis; i++)
      map[i] = nDOF_ + nBasis*elem + i;

    fldAssocTrace.setAssociativity( elem ).setRank( xfldGroup.associativity( elem ).rank() );
    fldAssocTrace.setAssociativity( elem ).setGlobalMapping( map );
  }

  // allocate group
  interiorTraceGroups_[localInteriorTraceGroups_.at(group)] = new FieldTraceClass( fldAssocTrace );

  // accumulate DOF and Element count
  nDOF_  += nElem*nBasis;
  nElem_ += nElem;
}

//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class ArrayQ>
template<class Topology>
void
FieldLinesearchBase<PhysDim, TopoDim, ArrayQ>::
createBoundaryTraceGroup(const int groupGlobal, const int order, const BasisFunctionCategory& category)
{
  typedef typename XField<PhysDim, TopoDim>:: template FieldTraceGroupType<Topology> XFieldTraceClass;
  typedef typename BaseType::template FieldTraceGroupType<Topology> FieldTraceClass;
  typedef typename FieldTraceClass::BasisType BasisType;

  const XFieldTraceClass& xfldGroup = xfld_.template getBoundaryTraceGroup<Topology>(groupGlobal);

  const BasisType* basis = BasisType::getBasisFunction(order, category);

  // create field associativity constructor
  typename FieldTraceClass::FieldAssociativityConstructorType fldAssocTrace( basis, xfldGroup.nElem() );

  // DOF ordering: Fortran-array DOF(basis,elem)
  const int nElem = xfldGroup.nElem();
  const int nBasis = basis->nBasis();
  std::vector<int> map( nBasis );
  for (int elem = 0; elem < nElem; elem++)
  {
    for (int i = 0; i < nBasis; i++)
      map[i] = nDOF_ + nBasis*elem + i;

    fldAssocTrace.setAssociativity( elem ).setRank( xfldGroup.associativity( elem ).rank() );
    fldAssocTrace.setAssociativity( elem ).setGlobalMapping( map );
  }

  // allocate group
  boundaryTraceGroups_[localBoundaryTraceGroups_.at(groupGlobal)] = new FieldTraceClass( fldAssocTrace );

  // accumulate DOF and Element count
  nDOF_  += nElem*nBasis;
  nElem_ += nElem;
}

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim, class ArrayQ>
void
FieldLinesearchBase<PhysDim, TopoDim, ArrayQ>::createDOFs()
{
  // allocate the solution DOF array and assign it to groups

  this->resizeDOF(nDOF_);
  for (int n = 0; n < nDOF_; n++)
    DOF_[n] = 0;

  for (int group = 0; group < cellGroups_.size(); group++)
    if ( cellGroups_[group] != NULL )
      cellGroups_[group]->setDOF( DOF_, nDOF_ );

  for (int group = 0; group < interiorTraceGroups_.size(); group++)
    if ( interiorTraceGroups_[group] != NULL )
      interiorTraceGroups_[group]->setDOF( DOF_, nDOF_ );

  for (int group = 0; group < boundaryTraceGroups_.size(); group++)
    if ( boundaryTraceGroups_[group] != NULL )
      boundaryTraceGroups_[group]->setDOF( DOF_, nDOF_ );
}

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim, class ArrayQ>
int
FieldLinesearchBase<PhysDim, TopoDim, ArrayQ>::nDOFCellGroup(int cellgroup) const
{
  const int nElem = this->getCellGroupBase(cellgroup).nElem();
  const int nBasisCell = this->getCellGroupBase(cellgroup).nBasis();
  return nElem*nBasisCell;
}

template<class PhysDim, class TopoDim, class ArrayQ>
int
FieldLinesearchBase<PhysDim, TopoDim, ArrayQ>::nDOFInteriorTraceGroup(int tracegroup) const
{
  const int nElem = this->getInteriorTraceGroupBase(tracegroup).nElem();
  const int nBasisTrace = this->getInteriorTraceGroupBase(tracegroup).nBasis();
  return nElem*nBasisTrace;
}

template<class PhysDim, class TopoDim, class ArrayQ>
int
FieldLinesearchBase<PhysDim, TopoDim, ArrayQ>::nDOFBoundaryTraceGroup(int tracegroup) const
{
  const int nElem = this->getBoundaryTraceGroupBase(tracegroup).nElem();
  const int nBasisTrace = this->getBoundaryTraceGroupBase(tracegroup).nBasis();
  return nElem*nBasisTrace;
}

}
