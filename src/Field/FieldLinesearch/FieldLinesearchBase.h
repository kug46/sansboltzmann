// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDLINESEARCHBASE_H
#define FIELDLINESEARCHBASE_H

#include "BasisFunction/BasisFunctionCategory.h"
#include "Field/Field.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// DG field for representing line-search step sizes
//----------------------------------------------------------------------------//

template <class PhysDim, class TopoDim, class T>
class FieldLinesearch;

template <class PhysDim, class TopoDim, class ArrayQ>
class FieldLinesearchBase : public Field< PhysDim, TopoDim, ArrayQ >
{
public:
  typedef Field< PhysDim, TopoDim, ArrayQ > BaseType;

  FieldLinesearchBase() = delete;
  virtual ~FieldLinesearchBase() {};

  FieldLinesearchBase& operator=( const FieldLinesearchBase& ) = delete;

  virtual int nDOFCellGroup(int cellgroup) const override;
  virtual int nDOFInteriorTraceGroup(int tracegroup) const override;
  virtual int nDOFBoundaryTraceGroup(int tracegroup) const override;

  // Virtual class to denote the SpaceType of the Field
  virtual SpaceType spaceType() const override { return SpaceType::Technical; }

protected:
  //Protected constructor. This is a helper class
  explicit FieldLinesearchBase( const XField<PhysDim, TopoDim>& xfld );

  // Constructs a new cell group
  template<class Topology>
  void createCellGroup(const int group, const int order, const BasisFunctionCategory& category);

  // Constructs a new interior trace group
  template<class Topology>
  void createInteriorTraceGroup(const int group, const int order, const BasisFunctionCategory& category);

  // Constructs a new boundary trace group
  template<class Topology>
  void createBoundaryTraceGroup(const int group, const int order, const BasisFunctionCategory& category);

  void createDOFs();

  using BaseType::nDOF_;
  using BaseType::DOF_;
  using BaseType::nElem_;
  using BaseType::cellGroups_;
  using BaseType::interiorTraceGroups_;
  using BaseType::boundaryTraceGroups_;
  using BaseType::localCellGroups_;
  using BaseType::localInteriorTraceGroups_;
  using BaseType::localBoundaryTraceGroups_;
  using BaseType::xfld_;
};

}

#endif  // FIELDLINESEARCHBASE_H
