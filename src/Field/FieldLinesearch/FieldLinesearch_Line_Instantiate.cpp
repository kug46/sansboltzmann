// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define FIELDLINESEARCHBASE_INSTANTIATE
#include "FieldLinesearchBase_impl.h"

#define FIELDLINESEARCH_LINE_INSTANTIATE
#include "FieldLinesearch_Line_impl.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

namespace SANS
{

template class FieldLinesearch< PhysD1, TopoD1, Real >;
template class FieldLinesearch< PhysD1, TopoD1, DLA::VectorS<1, Real> >;
template class FieldLinesearch< PhysD1, TopoD1, DLA::VectorS<2, Real> >;
template class FieldLinesearch< PhysD1, TopoD1, DLA::VectorS<3, Real> >;
template class FieldLinesearch< PhysD1, TopoD1, DLA::VectorS<4, Real> >;
//template class FieldLinesearch< PhysD1, TopoD1, DLA::VectorS<5, Real> >;
template class FieldLinesearch< PhysD1, TopoD1, DLA::VectorS<6, Real> >;

template class FieldLinesearch< PhysD2, TopoD1, Real >;
template class FieldLinesearch< PhysD2, TopoD1, DLA::VectorS<2, Real> >;
template class FieldLinesearch< PhysD2, TopoD1, DLA::VectorS<3, Real> >;
template class FieldLinesearch< PhysD2, TopoD1, DLA::VectorS<4, Real> >;
template class FieldLinesearch< PhysD2, TopoD1, DLA::VectorS<6, Real> >;
template class FieldLinesearch< PhysD2, TopoD1, DLA::VectorS<8, Real> >;
}
