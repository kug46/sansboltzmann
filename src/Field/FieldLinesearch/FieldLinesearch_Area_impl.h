// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(FIELDLINESEARCH_AREA_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "FieldLinesearch_Area.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// 2-D solution field: Linesearch field
//----------------------------------------------------------------------------//

template<class PhysDim, class ArrayQ>
FieldLinesearch<PhysDim, TopoD2, ArrayQ>::FieldLinesearch( const Field<PhysDim, TopoD2, ArrayQ>& fld, const int order ) : BaseType(fld.getXField())
{
  init(order, BasisFunctionCategory_Legendre,
       fld.getGlobalCellGroups(), fld.getGlobalInteriorTraceGroups(), fld.getGlobalBoundaryTraceGroups() );
}

template<class PhysDim, class ArrayQ>
FieldLinesearch<PhysDim, TopoD2, ArrayQ>&
FieldLinesearch<PhysDim, TopoD2, ArrayQ>::operator=( const ArrayQ& q )
{
  Field< PhysDim, TopoD2, ArrayQ >::operator=(q);
  return *this;
}

template<class PhysDim, class ArrayQ>
void
FieldLinesearch<PhysDim, TopoD2, ArrayQ>::init( const int order, const BasisFunctionCategory& category,
                                                const std::vector<int>& CellGroups,
                                                const std::vector<int>& InteriorTraceGroups,
                                                const std::vector<int>& BoundaryTraceGroups )
{
  //allocate the groups
  this->resizeCellGroups( CellGroups );
  this->resizeInteriorTraceGroups( InteriorTraceGroups );
  if ( CellGroups.size() > 0 )
    this->resizeBoundaryTraceGroups( {} );
  else
    this->resizeBoundaryTraceGroups( BoundaryTraceGroups );

  for (int igroup = 0; igroup < this->nCellGroups(); igroup++)
  {
    const int group = CellGroups[igroup];

    if ( xfld_.getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
      this->template createCellGroup<Triangle>(group, order, category);
    else if ( xfld_.getCellGroupBase(group).topoTypeID() == typeid(Quad) )
      this->template createCellGroup<Quad>(group, order, category);
    else
      SANS_DEVELOPER_EXCEPTION( "FieldLinesearch<PhysDim, TopoD2, ArrayQ>::init - Unknown cell topology." );
  }

  for (int igroup = 0; igroup < this->nInteriorTraceGroups(); igroup++)
  {
    const int group = InteriorTraceGroups[igroup];

    if ( xfld_.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Line) )
      this->template createInteriorTraceGroup<Line>(group, order, category);
    else
      SANS_DEVELOPER_EXCEPTION( "FieldLinesearch<PhysDim, TopoD2, ArrayQ>::init - Unknown trace topology." );
  }

  for (int igroup = 0; igroup < this->nBoundaryTraceGroups(); igroup++)
  {
    const int group = BoundaryTraceGroups[igroup];

    if ( xfld_.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Line) )
      this->template createBoundaryTraceGroup<Line>(group, order, category);
    else
      SANS_DEVELOPER_EXCEPTION( "FieldLinesearch<PhysDim, TopoD2, ArrayQ>::init - Unknown trace topology." );
  }

  //Allocate the DOFs array
  this->createDOFs();
}

}
