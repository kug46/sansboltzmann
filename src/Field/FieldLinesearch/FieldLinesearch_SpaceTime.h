// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDLINESEARCH_SPACETIME_H
#define FIELDLINESEARCH_SPACETIME_H

#include "FieldLinesearchBase.h"
#include "Field/FieldSpacetime.h"
#include "Field/XFieldSpacetime.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// 4-D solution field: Linesearch field
//----------------------------------------------------------------------------//

template <class PhysDim, class ArrayQ>
class FieldLinesearch<PhysDim, TopoD4, ArrayQ> : public FieldLinesearchBase< PhysDim, TopoD4, ArrayQ >
{
public:
  typedef FieldLinesearchBase< PhysDim, TopoD4, ArrayQ > BaseType;

  FieldLinesearch( const Field<PhysDim, TopoD4, ArrayQ>& fld, const int order = 0 );

  FieldLinesearch& operator=( const ArrayQ& q );

protected:
  void init( const int order, const BasisFunctionCategory& category,
             const std::vector<int>& CellGroups,
             const std::vector<int>& InteriorTraceGroups,
             const std::vector<int>& BoundaryTraceGroups);

  using BaseType::nDOF_;
  using BaseType::DOF_;
  using BaseType::cellGroups_;
  using BaseType::xfld_;
};

}

#endif  // FIELDLINESEARCH_VOLUME_H
