// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define FIELDBASE_INSTANTIATE
#include "FieldBase_impl.h"

#define XFIELD_INSTANTIATE
#include "XField_impl.h"

#define XFIELDLINE_INSTANTIATE
#include "XFieldLine_impl.h"
#include "XFieldLine_buildFrom_impl.h"
#include "XFieldLine_checkGrid_impl.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

// Do not include XField_checkTraceNormal_impl.h because a specialized one is provided in this file

namespace SANS
{

//=============================================================================
template class FieldBase< XFieldTraits<PhysD2, TopoD1> >;


//------------------------------------------------------------------------------//
// Specializations for XField<PhysD2,TopoD1>
//------------------------------------------------------------------------------//

// check that trace normal vector points outward of the element
template<class PhysDim, class TopoDim>
template<class TopologyTrace, class TopologyL>
void
XField< PhysDim, TopoDim >::
checkTraceNormalL( const typename BaseType::template FieldTraceGroupType<TopologyTrace>& traceGroup,
                   int elem,
                   ElementXField<PhysDim, typename TopologyTrace::TopoDim, TopologyTrace>& elemTrace,
                   const ElementXField<PhysDim, TopoDim, TopologyL>& elemCellL ) const
{
  typedef typename BaseType::template FieldTraceGroupType<TopologyTrace>::template ElementType<> ElementXFieldTrace;
  typedef typename BaseType::template FieldCellGroupType<TopologyL>::template ElementType<>      ElementXFieldL;

  typedef typename ElementXFieldTrace::RefCoordType RefCoordTraceType;
  typedef typename ElementXFieldL::RefCoordType RefCoordCellType;

  VectorX cT, cL, dX, NL, Xs;        // Normal and centroids
  Real coNsignL;                     // conormal vector sign

  // Get the trace element
  traceGroup.getElement( elemTrace, elem );

  const CanonicalTraceToCell canonicalTraceL = traceGroup.getCanonicalTraceLeft(elem);

  RefCoordTraceType sRefTrace = TopologyTrace::centerRef;
  RefCoordCellType sRefL;

  TraceToCellRefCoord<TopologyTrace, typename TopologyL::TopoDim, TopologyL>::eval( canonicalTraceL, sRefTrace, sRefL );

  // Get the trace centroid and conormal sign
  elemTrace.coordinates( TopologyTrace::centerRef, cT );
  coNsignL = elemTrace.conormalSignL(TopologyTrace::centerRef);

  // Get the left cell centroid and tangent vector
  elemCellL.coordinates( TopologyL::centerRef, cL );
  elemCellL.unitTangent( sRefL, Xs );

  // Compute the 'cell-to-cell' normal from the left element
  NL = Xs * coNsignL;

  // Compute the vector from the trace-to-cell centroids
  dX = cL - cT;

  // normal should point outward
  XFIELD_CHECK( dot(NL,dX) < 0, "dot(NL,dX) = %e, Left cell center coordinates: (%e, %e) ", dot(NL,dX), cL[0], cL[1] );
  // TODO: this check may not be bullet-proof for manifolds
}


//-----------------------------------------------------------------------------
template<class PhysDim, class TopoDim>
template<class TopologyTrace, class TopologyR>
void
XField< PhysDim, TopoDim >::
checkTraceNormalR( const typename BaseType::template FieldTraceGroupType<TopologyTrace>& traceGroup,
                   int elem,
                   ElementXField<PhysDim, typename TopologyTrace::TopoDim, TopologyTrace>& elemTrace,
                   const ElementXField<PhysDim, TopoDim, TopologyR>& elemCellR ) const
{
  typedef typename BaseType::template FieldTraceGroupType<TopologyTrace>::template ElementType<> ElementXFieldTrace;
  typedef typename BaseType::template FieldCellGroupType<TopologyR>::template ElementType<>      ElementXFieldR;

  typedef typename ElementXFieldTrace::RefCoordType RefCoordTraceType;
  typedef typename ElementXFieldR::RefCoordType RefCoordCellType;

  VectorX cT, cR, dX, NR, Xs;       // Normal and centroids
  Real coNsignR;                    // conormal vector sign
  // Get the trace element
  traceGroup.getElement( elemTrace, elem );

  const CanonicalTraceToCell canonicalTraceR = traceGroup.getCanonicalTraceRight(elem);

  RefCoordTraceType sRefTrace = TopologyTrace::centerRef;
  RefCoordCellType sRefR;

  TraceToCellRefCoord<TopologyTrace, typename TopologyR::TopoDim, TopologyR>::eval( canonicalTraceR, sRefTrace, sRefR );

  // Get the trace centroid and conormal sign
  elemTrace.coordinates( TopologyTrace::centerRef, cT );
  coNsignR = elemTrace.conormalSignR(TopologyTrace::centerRef);

  // Get the left cell centroid and tangent vector
  elemCellR.coordinates( TopologyR::centerRef, cR );
  elemCellR.unitTangent( sRefR, Xs );

  // Compute the 'cell-to-cell' normal from the right element
  NR = Xs * coNsignR;

  // Compute the vector from the trace-to-cell centroids
  dX = cR - cT;

  // normal should outward
  XFIELD_CHECK( dot(NR,dX) < 0, "dot(NR,dX) = %e, Right cell center coordinates: (%e, %e) ", dot(NR,dX), cR[0], cR[1] );
  // TODO: this check may not be bullet-proof for manifolds
}


//=============================================================================
//Explicitly instantiate
template class XField< PhysD2, TopoD1 >;

} // namespace SANS
