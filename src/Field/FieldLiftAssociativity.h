// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDTRACEASSOCIATIVITY_H
#define FIELDTRACEASSOCIATIVITY_H

#include <iostream>
#include <typeinfo> //typeid

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "FieldLiftAssociativityConstructor.h"
#include "Field/Element/Element.h"
#include "Field/Element/ElementLift.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Field of elements associativities assigned to the trace of an element
// This is not used to represent trace elements. Instead this is multiple cell
// fields associated with each trace of an element, i.e. lifting operators.
//
// implemented as abstract base class with templated derived classes
//
// template parameters:
//   FieldTraits    Traits to specialize the field
//
//----------------------------------------------------------------------------//

struct NullBase;

template<class T, class CloneBase = NullBase >
class FieldLiftAssociativityBase
{
public:

  virtual ~FieldLiftAssociativityBase() {}

  //Choose the appropriate return type from the clone function
  typedef typename std::conditional< std::is_same< CloneBase, NullBase >::value,
                                     FieldLiftAssociativityBase,
                                     CloneBase
                                   >::type CloneType;

  virtual CloneType* clone() const = 0;

  virtual const std::type_info& topoTypeID() const = 0;
  virtual int nElem() const = 0;
  virtual int nElemTrace() const = 0; // Number of trace eloements of a cell element
  virtual int nDOF() const = 0;
  virtual int nBasis() const = 0;
  virtual int order() const = 0;

  virtual void setDOF( T* DOF, int nDOF ) = 0;

  virtual void dump( int indentSize=0, std::ostream& out = std::cout ) const = 0;

protected:
  FieldLiftAssociativityBase() {}     // abstract base class
};


template <class FieldTraits >
class FieldLiftAssociativity;

//This is needed so the correct clone operator is defined for different FieldBase classes
template<class FieldTraits, class FieldBase>
struct FieldLiftAssociativityClone : public FieldTraits::FieldBase
{
  virtual FieldBase* clone() const override { return NULL; }
  virtual ~FieldLiftAssociativityClone() {}
};

//Specialization for FieldLiftAssociativityBase<T> as the base class
template<class FieldTraits, class T>
struct FieldLiftAssociativityClone<FieldTraits, FieldLiftAssociativityBase<T> > : public FieldTraits::FieldBase
{
  typedef typename FieldTraits::FieldBase FieldBase;
  virtual FieldBase* clone() const override
  {
    return new FieldLiftAssociativity<FieldTraits>(*reinterpret_cast<const FieldLiftAssociativity<FieldTraits>*>(this));
  }
  virtual ~FieldLiftAssociativityClone() {}
};


template <class FieldTraits >
class FieldLiftAssociativity : public FieldLiftAssociativityClone<FieldTraits, typename FieldTraits::FieldBase >
{
public:
  typedef typename FieldTraits::T T;              // DOF type
  typedef typename FieldTraits::FieldBase FieldBase;
  typedef typename FieldTraits::BasisType BasisType;
  typedef typename FieldTraits::TopologyType TopologyType;
  typedef typename FieldTraits::ElementAssociativityType ElementAssociativityType;
  typedef typename FieldTraits::FieldLiftAssociativityConstructorType FieldLiftAssociativityConstructorType;
  typedef FieldLiftAssociativityConstructorType FieldAssociativityConstructorType;
  typedef typename TopologyType::TopoDim TopoDim;
  static const int NTrace = ElementAssociativityType::TopologyType::NTrace;

  template<class ElemT = Real>
  using ElementType = typename FieldTraits::template ElementType<ElemT>;

  FieldLiftAssociativity();   // needed for new []
  FieldLiftAssociativity( const FieldLiftAssociativity& a ) : FieldLiftAssociativity() { operator=(a); }
  explicit FieldLiftAssociativity( const FieldLiftAssociativityConstructorType& assoc ) : FieldLiftAssociativity() { resize(assoc); }
  virtual ~FieldLiftAssociativity();

  // needed to resize arrays of FieldLiftAssociativity
  void resize( const FieldLiftAssociativityConstructorType& assoc );

  FieldLiftAssociativity& operator=( const FieldLiftAssociativity& );

  //virtual FieldBase* clone() const override { return new FieldLiftAssociativity(*this); }

  virtual int nElem() const override { return nElem_; }
  virtual int nElemTrace() const override { return TopologyType::NTrace; }
  virtual int nDOF()       const override { return nDOF_; }
  virtual int nBasis()     const override { return basis_->nBasis(); }
  virtual int order()     const override { return basis_->order(); }

  // gets the toplogical type ID
  virtual const std::type_info& topoTypeID() const override { return typeid(TopologyType); }

  // basis function
  const BasisType* basis() const { return basis_; }

  // DOF accessors
        T& DOF( int n )       { return DOF_[n]; }
  const T& DOF( int n ) const { return DOF_[n]; }

  // set shared DOFs
  virtual void setDOF( T* DOF, const int nDOF ) override;

  // element DOF associations
  const ElementAssociativityType& associativity( const int elem, const int canonicalTrace ) const
  {
    SANS_ASSERT( elem >= 0 && elem < nElem_ && canonicalTrace >= 0 && canonicalTrace < NTrace );
    return assoc_[elem*NTrace + canonicalTrace];
  }

  // extract/set individual element
  template <class ElemT>
  void getElement(       Element<ElemT, TopoDim, TopologyType>& fldElem, const int elem, const int canonicalTrace ) const;
  template <class ElemT>
  void setElement( const Element<ElemT, TopoDim, TopologyType>& fldElem, const int elem, const int canonicalTrace );

  template <class ElemT>
  void getElement(       ElementLift<ElemT, TopoDim, TopologyType>& fldElem, const int elem ) const;
  template <class ElemT>
  void setElement( const ElementLift<ElemT, TopoDim, TopologyType>& fldElem, const int elem );

  // projects dofs onto FieldLiftAssociativity with possibly different basis functions
  void projectTo( FieldLiftAssociativity& ) const;

  virtual void dump( int indentSize=0, std::ostream& out = std::cout ) const override;

protected:
  int nElem_;                         // total elements in field
  int nDOF_;                          // total coordinate DOFs (for global array)

  const BasisType* basis_;

  T* DOF_;                             // DOFs

  ElementAssociativityType* assoc_;   // element DOF associations (local to global)
};


// default ctor needed for 'new []'
template <class FieldTraits>
FieldLiftAssociativity<FieldTraits>::FieldLiftAssociativity()
{
  nElem_ = 0;

  basis_ = NULL;

  DOF_ = NULL;
  nDOF_ = 0;

  assoc_ = NULL;
}


template <class FieldTraits>
void
FieldLiftAssociativity<FieldTraits>::resize( const FieldLiftAssociativityConstructorType& assoc )
{
  nElem_ = assoc.nElem();

  basis_ = assoc.basis();

  DOF_ = NULL;
  nDOF_ = 0;

  // edge-element DOF associativity
  assoc_ = new ElementAssociativityType[nElem_*NTrace];

  for (int n = 0; n < nElem_; n++)
    for (int canonicalTrace = 0; canonicalTrace < NTrace; canonicalTrace++)
      assoc_[n*NTrace + canonicalTrace] = assoc.getAssociativity(n, canonicalTrace);
}


template <class FieldTraits>
FieldLiftAssociativity<FieldTraits>::~FieldLiftAssociativity()
{
  // edge-element DOF associativity
  delete [] assoc_;
}

template <class FieldTraits>
FieldLiftAssociativity<FieldTraits>&
FieldLiftAssociativity<FieldTraits>::operator=( const FieldLiftAssociativity& a )
{
  if (this != &a)
  {
    nElem_ = a.nElem_;
    nDOF_  = a.nDOF_;

    basis_ = a.basis_;

    DOF_ = a.DOF_;

    delete [] assoc_; assoc_ = NULL;

    assoc_ = new ElementAssociativityType[nElem_*NTrace];

    for (int n = 0; n < nElem_*NTrace; n++)
      assoc_[n] = a.assoc_[n];
  }

  return *this;
}


// set shared DOFs
template <class FieldTraits>
void
FieldLiftAssociativity<FieldTraits>::setDOF( T* DOF, int nDOF )
{
  DOF_ = DOF;
  nDOF_ = nDOF;
}

// extract DOFs for individual element
template <class FieldTraits>
template <class ElemT>
void
FieldLiftAssociativity<FieldTraits>::getElement( Element<ElemT, TopoDim, TopologyType>& fldElem, const int elem, const int canonicalTrace ) const
{
  SANS_ASSERT( (elem >= 0) && (elem < nElem_) );
  SANS_ASSERT( (DOF_ != NULL) );
  SANS_ASSERT( basis_ == fldElem.basis() );

  assoc_[elem*NTrace + canonicalTrace].getElement(fldElem, DOF_, nDOF_);
}

// set DOFs for individual element
template <class FieldTraits>
template <class ElemT>
void
FieldLiftAssociativity<FieldTraits>::setElement( const Element<ElemT, TopoDim, TopologyType>& fldElem, const int elem, const int canonicalTrace )
{
  SANS_ASSERT( (elem >= 0) && (elem < nElem_) );
  SANS_ASSERT( (DOF_ != NULL) );
  SANS_ASSERT( basis_ == fldElem.basis() );

  assoc_[elem*NTrace + canonicalTrace].setElement(fldElem, DOF_, nDOF_);
}

template <class FieldTraits>
template <class ElemT>
void
FieldLiftAssociativity<FieldTraits>::getElement( ElementLift<ElemT, TopoDim, TopologyType>& fldElem, const int elem ) const
{
  SANS_ASSERT( (elem >= 0) && (elem < nElem_) );
  SANS_ASSERT( (DOF_ != NULL) );
  SANS_ASSERT( basis_ == fldElem.basis() );

  for (int canonicalTrace = 0; canonicalTrace < NTrace; canonicalTrace++)
    assoc_[elem*NTrace + canonicalTrace].getElement(fldElem[canonicalTrace], DOF_, nDOF_);
}

// set DOFs for individual element
template <class FieldTraits>
template <class ElemT>
void
FieldLiftAssociativity<FieldTraits>::setElement( const ElementLift<ElemT, TopoDim, TopologyType>& fldElem, const int elem )
{
  SANS_ASSERT( (elem >= 0) && (elem < nElem_) );
  SANS_ASSERT( (DOF_ != NULL) );
  SANS_ASSERT( basis_ == fldElem.basis() );

  for (int canonicalTrace = 0; canonicalTrace < NTrace; canonicalTrace++)
    assoc_[elem*NTrace + canonicalTrace].setElement(fldElem[canonicalTrace], DOF_, nDOF_);
}

// project DOF onto another polynomial order
template <class FieldTraits>
void
FieldLiftAssociativity<FieldTraits>::projectTo( FieldLiftAssociativity& fldTo ) const
{
  SANS_ASSERT( nElem_ == fldTo.nElem_ );

  ElementLift<T,TopoDim,TopologyType> ElemFrom( basis() );
  ElementLift<typename FieldLiftAssociativity::T, TopoDim, TopologyType> ElemTo( fldTo.basis() );

  for ( int elem = 0; elem < nElem_; elem++ )
  {
    getElement(ElemFrom, elem);
    ElemFrom.projectTo(ElemTo);
    fldTo.setElement(ElemTo, elem);
  }
}


template <class FieldTraits>
void
FieldLiftAssociativity<FieldTraits>::dump( int indentSize, std::ostream& out ) const
{
  int n;
  std::string indent(indentSize, ' ');
  out << indent << "FieldLiftAssociativity:"
      << "  nElem_ = " << nElem_
      << "  nDOF_ = " << nDOF_ << std::endl;
  out << indent << "  DOF_[] =";
  for (n = 0; n < nDOF_; n++)
    out << "(" << DOF_[n] << ") ";
  out << std::endl;
  for (n = 0; n < nElem_; n++)
  {
    for ( int canonicalTrace = 0; canonicalTrace < NTrace; canonicalTrace++ )
    {
      out << indent << "  assoc_[elem " << n << ", trace " << canonicalTrace <<"]:" << std::endl;
      assoc_[n*NTrace + canonicalTrace].dump( indentSize+4, out );
    }
  }
}

}

#endif  // FIELDTRACEASSOCIATIVITY_H
