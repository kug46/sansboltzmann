// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDBASE_H
#define FIELDBASE_H

#include <iostream>
#include <string>
#include <typeinfo>     // typeid

#include "tools/SANSException.h"
#include "tools/PointerArray.h"
#include "Topology/ElementTopology.h"
#include "FieldTypes.h"


namespace SANS
{


// A simple tag to indicate that the field copy constructor is called
class FieldCopy {};

//----------------------------------------------------------------------------//
// Base field data specialized based on the FieldTraits template argument
//----------------------------------------------------------------------------//

template <class FieldTraits>
class FieldBase
{
public:
  typedef typename FieldTraits::TopoDim TopoDim;
  typedef typename FieldTraits::T T;
  typedef typename FieldTraits::FieldTraceGroupBase FieldTraceGroupBase;
  typedef typename FieldTraits::FieldCellGroupBase FieldCellGroupBase;


  template<class Topology>
  using FieldTraceGroupType = typename FieldTraits::template FieldTraceGroupType<Topology>;

  template<class Topology>
  using FieldCellGroupType = typename FieldTraits::template FieldCellGroupType<Topology>;

  FieldBase( const FieldBase& fld ) = delete;            // No copy constructor to prevent accidental copies
  FieldBase& operator=( const FieldBase& fld ) = delete; // No copy assignment to prevent accidental copies

  FieldBase( const FieldBase& fld, const FieldCopy& ) : FieldBase() { cloneFrom(fld); }
  virtual ~FieldBase();

  void cloneFrom( const FieldBase& fld );
  FieldBase& operator=( const T& );

  // projects dofs onto a different Field with possibly different polynomial order
  void projectTo( FieldBase& ) const;

  int nElem() const { return nElem_; }
  int nDOF()  const { return nDOF_; }

  virtual int nDOFCellGroup(int cellgroup) const = 0;
  virtual int nDOFInteriorTraceGroup(int tracegroup) const = 0;
  virtual int nDOFBoundaryTraceGroup(int tracegroup) const = 0;

  int nHubTraceGroups() const      { return hubTraceGroups_.size(); }
  int nInteriorTraceGroups() const { return interiorTraceGroups_.size(); }
  int nBoundaryTraceGroups() const { return boundaryTraceGroups_.size(); }
  int nCellGroups() const          { return cellGroups_.size(); }

  // DOF accessors
        T& DOF( int n )       { return DOF_[n]; }
  const T& DOF( int n ) const { return DOF_[n]; }

  // group accessors
  FieldTraceGroupBase& getHubTraceGroupBase( const int group )
  {
    SANS_ASSERT_MSG(group >= 0 && group < hubTraceGroups_.size(),
                    "hubTraceGroups_.size() = %d, group = %d", hubTraceGroups_.size(), group);
    SANS_ASSERT_MSG( hubTraceGroups_[group] != NULL, "group = %d", group );
    return *hubTraceGroups_[group];
  }
  const FieldTraceGroupBase& getHubTraceGroupBase( const int group ) const
  {
    SANS_ASSERT_MSG(group >= 0 && group < hubTraceGroups_.size(),
                    "hubTraceGroups_.size() = %d, group = %d", hubTraceGroups_.size(), group);
    SANS_ASSERT_MSG( hubTraceGroups_[group] != NULL, "group = %d", group );
    return *hubTraceGroups_[group];
  }

  FieldTraceGroupBase& getInteriorTraceGroupBase( const int group )
  {
    SANS_ASSERT_MSG(group >= 0 && group < interiorTraceGroups_.size(),
                    "interiorTraceGroups_.size() = %d, group = %d", interiorTraceGroups_.size(), group);
    SANS_ASSERT_MSG( interiorTraceGroups_[group] != NULL, "group = %d", group );
    return *interiorTraceGroups_[group];
  }
  const FieldTraceGroupBase& getInteriorTraceGroupBase( const int group ) const
  {
    SANS_ASSERT_MSG(group >= 0 && group < interiorTraceGroups_.size(),
                    "interiorTraceGroups_.size() = %d, group = %d", interiorTraceGroups_.size(), group);
    SANS_ASSERT_MSG( interiorTraceGroups_[group] != NULL, "group = %d", group );
    return *interiorTraceGroups_[group];
  }

  FieldTraceGroupBase& getBoundaryTraceGroupBase( const int group )
  {
    SANS_ASSERT_MSG(group >= 0 && group < boundaryTraceGroups_.size(),
                    "boundaryTraceGroups_.size() = %d, group = %d", boundaryTraceGroups_.size(), group);
    SANS_ASSERT_MSG( boundaryTraceGroups_[group] != NULL, "group = %d", group );
    return *boundaryTraceGroups_[group];
  }
  const FieldTraceGroupBase& getBoundaryTraceGroupBase( const int group ) const
  {
    SANS_ASSERT_MSG(group >= 0 && group < boundaryTraceGroups_.size(),
                    "boundaryTraceGroups_.size() = %d, group = %d", boundaryTraceGroups_.size(), group);
    SANS_ASSERT_MSG( boundaryTraceGroups_[group] != NULL, "group = %d", group );
    return *boundaryTraceGroups_[group];
  }

  FieldCellGroupBase& getCellGroupBase( const int group )
  {
    SANS_ASSERT_MSG(group >= 0 && group < cellGroups_.size(), "cellGroups_.size() = %d, group = %d", cellGroups_.size(), group);
    SANS_ASSERT_MSG( cellGroups_[group] != NULL, "group = %d", group );
    return *cellGroups_[group];
  }
  const FieldCellGroupBase& getCellGroupBase( const int group ) const
  {
    SANS_ASSERT_MSG(group >= 0 && group < cellGroups_.size(), "cellGroups_.size() = %d, group = %d", cellGroups_.size(), group);
    SANS_ASSERT_MSG( cellGroups_[group] != NULL, "group = %d", group );
    return *cellGroups_[group];
  }

  template<class Topology>
        FieldTraceGroupType<Topology>& getHubTraceGroup( const int group );
  template<class Topology>
  const FieldTraceGroupType<Topology>& getHubTraceGroup( const int group ) const;

  template<class Topology>
        FieldTraceGroupType<Topology>& getInteriorTraceGroup( const int group );
  template<class Topology>
  const FieldTraceGroupType<Topology>& getInteriorTraceGroup( const int group ) const;

  template<class Topology>
        FieldTraceGroupType<Topology>& getBoundaryTraceGroup( const int group );
  template<class Topology>
  const FieldTraceGroupType<Topology>& getBoundaryTraceGroup( const int group ) const;

  template<class Topology>
        FieldCellGroupType<Topology>& getCellGroup( const int group );
  template<class Topology>
  const FieldCellGroupType<Topology>& getCellGroup( const int group ) const;

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

  int local2nativeDOFmap(const int i) const { return local2nativeDOFmap_[i]; }
  const int* local2nativeDOFmap() const { return local2nativeDOFmap_; }

protected:
  FieldBase() : nElem_(0),
    nDOF_(0), DOF_(nullptr), local2nativeDOFmap_(nullptr),
    hubTraceGroups_(0),
    interiorTraceGroups_(0),
    boundaryTraceGroups_(0),
    cellGroups_(0)
    {}

  void deallocate();

  void resizeDOF( const int nDOF );
  void resizeHubTraceGroups( const int nHubTraceGroups );
  void resizeInteriorTraceGroups( const int nInteriorTraceGroups );
  void resizeBoundaryTraceGroups( const int nBoundaryTraceGroups );
  void resizeCellGroups( const int nCellGroups );

  int nElem_;                                // total cell elements
  int nDOF_;                                 // total DOFs on the processors (possessed + ghost + zombies), i.e. size of DOF_
  T* DOF_;                                   // field DOFs
  int* local2nativeDOFmap_;                  // maps local DOF index to native global indexing across processors

  PointerArray<FieldTraceGroupBase> hubTraceGroups_;      // groups of trace elements possibly connected to many cell elements

  PointerArray<FieldTraceGroupBase> interiorTraceGroups_; // groups of interior-traces

  PointerArray<FieldTraceGroupBase> boundaryTraceGroups_; // groups of boundary-traces

  PointerArray<FieldCellGroupBase> cellGroups_;           // groups of cell elements
};

//----------------------------------------------------------------------------//
template <class FieldTraits>
template<class Topology>
typename FieldBase<FieldTraits>::template FieldTraceGroupType<Topology>&
FieldBase<FieldTraits>::getHubTraceGroup( const int group )
{
  typedef FieldTraceGroupType<Topology> FieldTraceGroupClass;
  SANS_ASSERT( group >=0 && group < hubTraceGroups_.size() );
  SANS_ASSERT( hubTraceGroups_[group] != NULL );
  SANS_ASSERT( hubTraceGroups_[group]->topoTypeID() == typeid(Topology) );
  return static_cast<FieldTraceGroupClass&>( *hubTraceGroups_[group] );
}

//----------------------------------------------------------------------------//
template <class FieldTraits>
template<class Topology>
const typename FieldBase<FieldTraits>::template FieldTraceGroupType<Topology>&
FieldBase<FieldTraits>::getHubTraceGroup( const int group ) const
{
  typedef FieldTraceGroupType<Topology> FieldTraceGroupClass;
  SANS_ASSERT( group >=0 && group < hubTraceGroups_.size() );
  SANS_ASSERT( hubTraceGroups_[group] != NULL );
  SANS_ASSERT( hubTraceGroups_[group]->topoTypeID() == typeid(Topology) );
  return static_cast<const FieldTraceGroupClass&>( *hubTraceGroups_[group] );
}

//----------------------------------------------------------------------------//
template <class FieldTraits>
template<class Topology>
typename FieldBase<FieldTraits>::template FieldTraceGroupType<Topology>&
FieldBase<FieldTraits>::getInteriorTraceGroup( const int group )
{
  typedef FieldTraceGroupType<Topology> FieldTraceGroupClass;
  SANS_ASSERT( group >=0 && group < interiorTraceGroups_.size() );
  SANS_ASSERT( interiorTraceGroups_[group] != NULL );
  SANS_ASSERT( interiorTraceGroups_[group]->topoTypeID() == typeid(Topology) );
  return static_cast<FieldTraceGroupClass&>( *interiorTraceGroups_[group] );
}

//----------------------------------------------------------------------------//
template <class FieldTraits>
template<class Topology>
const typename FieldBase<FieldTraits>::template FieldTraceGroupType<Topology>&
FieldBase<FieldTraits>::getInteriorTraceGroup( const int group ) const
{
  typedef FieldTraceGroupType<Topology> FieldTraceGroupClass;
  SANS_ASSERT( group >=0 && group < interiorTraceGroups_.size() );
  SANS_ASSERT( interiorTraceGroups_[group] != NULL );
  SANS_ASSERT( interiorTraceGroups_[group]->topoTypeID() == typeid(Topology) );
  return static_cast<const FieldTraceGroupClass&>( *interiorTraceGroups_[group] );
}


//----------------------------------------------------------------------------//
template <class FieldTraits>
template<class Topology>
typename FieldBase<FieldTraits>::template FieldTraceGroupType<Topology>&
FieldBase<FieldTraits>::getBoundaryTraceGroup( const int group )
{
  typedef FieldTraceGroupType<Topology> FieldTraceGroupClass;
  SANS_ASSERT( group >=0 && group < boundaryTraceGroups_.size() );
  SANS_ASSERT( boundaryTraceGroups_[group] != NULL );
  SANS_ASSERT( boundaryTraceGroups_[group]->topoTypeID() == typeid(Topology) );
  return static_cast<FieldTraceGroupClass&>( *boundaryTraceGroups_[group] );
}

//----------------------------------------------------------------------------//
template <class FieldTraits>
template<class Topology>
const typename FieldBase<FieldTraits>::template FieldTraceGroupType<Topology>&
FieldBase<FieldTraits>::getBoundaryTraceGroup( const int group ) const
{
  typedef FieldTraceGroupType<Topology> FieldTraceGroupClass;
  SANS_ASSERT( group >=0 && group < boundaryTraceGroups_.size() );
  SANS_ASSERT( boundaryTraceGroups_[group] != NULL );
  SANS_ASSERT( boundaryTraceGroups_[group]->topoTypeID() == typeid(Topology) );
  return static_cast<const FieldTraceGroupClass&>( *boundaryTraceGroups_[group] );
}


//----------------------------------------------------------------------------//
template <class FieldTraits>
template<class Topology>
typename FieldBase<FieldTraits>::template FieldCellGroupType<Topology>&
FieldBase<FieldTraits>::getCellGroup( const int group )
{
  typedef FieldCellGroupType<Topology> FieldCellGroupClass;
  SANS_ASSERT( group >=0 && group < cellGroups_.size() );
  SANS_ASSERT( cellGroups_[group] != NULL );
  SANS_ASSERT( cellGroups_[group]->topoTypeID() == typeid(Topology) );
  return static_cast<FieldCellGroupClass&>( *cellGroups_[group] );
}

//----------------------------------------------------------------------------//
template <class FieldTraits>
template<class Topology>
const typename FieldBase<FieldTraits>::template FieldCellGroupType<Topology>&
FieldBase<FieldTraits>::getCellGroup( const int group ) const
{
  typedef FieldCellGroupType<Topology> FieldCellGroupClass;
  SANS_ASSERT_MSG( group >=0 && group < cellGroups_.size(), "group = %d, cellGroups_.size() = %d", group, cellGroups_.size() );
  SANS_ASSERT( cellGroups_[group] != NULL );
  SANS_ASSERT( cellGroups_[group]->topoTypeID() == typeid(Topology) );
  return static_cast<const FieldCellGroupClass&>( *cellGroups_[group] );
}

}

#endif  //FIELDBASE_H
