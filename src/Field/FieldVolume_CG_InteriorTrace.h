// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDVOLUME_CG_INTERIORTRACE_H
#define FIELDVOLUME_CG_INTERIORTRACE_H

#include <vector>

#include "Topology/ElementTopology.h"
#include "Field.h"

namespace SANS
{

template <class PhysDim, class TopoDim, class T>
class Field_CG_InteriorTrace;

//----------------------------------------------------------------------------//
// solution field: CG interior-edge (e.g. interface solution)
//----------------------------------------------------------------------------//

template <class PhysDim, class T>
class Field_CG_InteriorTrace<PhysDim, TopoD3, T> : public Field< PhysDim, TopoD3, T >
{
public:
  typedef Field< PhysDim, TopoD3, T > BaseType;
  typedef T ArrayQ;

  Field_CG_InteriorTrace( const XField<PhysDim, TopoD3>& xfld, const int order, const BasisFunctionCategory category );

  Field_CG_InteriorTrace( const XField<PhysDim, TopoD3>& xfld, const int order, const BasisFunctionCategory category,
                          const std::vector<int>& InteriorGroups );

  Field_CG_InteriorTrace( const Field_CG_InteriorTrace& fld, const FieldCopy&tag );

  Field_CG_InteriorTrace& operator=( const ArrayQ& q );

  // Virtual class to denote the SpaceType of the Field
  virtual SpaceType spaceType() const override { return SpaceType::Continuous; }

protected:
  using BaseType::nDOF_;
  using BaseType::DOF_;
  using BaseType::nElem_;
  using BaseType::xfld_;
  using BaseType::interiorTraceGroups_;
  using BaseType::localInteriorTraceGroups_;

  void init( const int order, const BasisFunctionCategory& category, const std::vector<int>& InteriorGroups );
};

}

#endif  // FIELDVOLUME_CG_INTERIORTRACE_H
