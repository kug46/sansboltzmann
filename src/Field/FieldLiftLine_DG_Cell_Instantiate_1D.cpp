// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define FIELDBASE_INSTANTIATE
#include "FieldBase_impl.h"

#define FIELDSUBGROUP_INSTANTIATE
#include "FieldSubGroup_impl.h"

#define FIELDLIFT_DG_CELLBASE_INSTANTIATE
#include "FieldLift_DG_CellBase_impl.h"

#define FIELDLIFTLINE_DG_CELL_INSTANTIATE
#include "FieldLiftLine_DG_Cell_impl.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "XFieldLine.h"

namespace SANS
{

// Base class instantiation
template class FieldBase< FieldLiftTraits<TopoD1, Real > >;
template class FieldBase< FieldLiftTraits<TopoD1, DLA::VectorS<1, Real> > >;
template class FieldBase< FieldLiftTraits<TopoD1, DLA::VectorS<1, DLA::VectorS<1, Real>> > >;
template class FieldBase< FieldLiftTraits<TopoD1, DLA::VectorS<1, DLA::VectorS<2, Real>> > >;
template class FieldBase< FieldLiftTraits<TopoD1, DLA::VectorS<1, DLA::VectorS<3, Real>> > >;
template class FieldBase< FieldLiftTraits<TopoD1, DLA::VectorS<1, DLA::VectorS<4, Real>> > >;

// FieldSubGroup instantiation
template class FieldSubGroup<PhysD1, FieldLiftTraits<TopoD1, Real > >;
template class FieldSubGroup<PhysD1, FieldLiftTraits<TopoD1, DLA::VectorS<1, Real> > >;
template class FieldSubGroup<PhysD1, FieldLiftTraits<TopoD1, DLA::VectorS<1, DLA::VectorS<1, Real>> > >;
template class FieldSubGroup<PhysD1, FieldLiftTraits<TopoD1, DLA::VectorS<1, DLA::VectorS<2, Real>> > >;
template class FieldSubGroup<PhysD1, FieldLiftTraits<TopoD1, DLA::VectorS<1, DLA::VectorS<3, Real>> > >;
template class FieldSubGroup<PhysD1, FieldLiftTraits<TopoD1, DLA::VectorS<1, DLA::VectorS<4, Real>> > >;

// Field Lift instantiation
template class FieldLift_DG_Cell<PhysD1, TopoD1, Real>;
template class FieldLift_DG_Cell< PhysD1, TopoD1, DLA::VectorS<1, Real> >;
template class FieldLift_DG_Cell< PhysD1, TopoD1, DLA::VectorS<1, DLA::VectorS<1, Real>> >;
template class FieldLift_DG_Cell< PhysD1, TopoD1, DLA::VectorS<1, DLA::VectorS<2, Real>> >;
template class FieldLift_DG_Cell< PhysD1, TopoD1, DLA::VectorS<1, DLA::VectorS<3, Real>> >;
template class FieldLift_DG_Cell< PhysD1, TopoD1, DLA::VectorS<1, DLA::VectorS<4, Real>> >;

}
