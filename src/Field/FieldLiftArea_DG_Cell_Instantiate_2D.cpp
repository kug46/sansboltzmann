// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define FIELDBASE_INSTANTIATE
#include "FieldBase_impl.h"

#define FIELDSUBGROUP_INSTANTIATE
#include "FieldSubGroup_impl.h"

#define FIELDLIFT_DG_CELLBASE_INSTANTIATE
#include "FieldLift_DG_CellBase_impl.h"

#define FIELDLIFTAREA_DG_CELL_INSTANTIATE
#include "FieldLiftArea_DG_Cell_impl.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "UserVariables/BoltzmannNVar.h"
#include "XFieldArea.h"

namespace SANS
{

// Base class instantiation
template class FieldBase< FieldLiftTraits<TopoD2, Real > >;
template class FieldBase< FieldLiftTraits<TopoD2, DLA::VectorS<2, Real> > >;
template class FieldBase< FieldLiftTraits<TopoD2, DLA::VectorS<2, DLA::VectorS<1, Real>> > >;
template class FieldBase< FieldLiftTraits<TopoD2, DLA::VectorS<2, DLA::VectorS<2, Real>> > >;
template class FieldBase< FieldLiftTraits<TopoD2, DLA::VectorS<2, DLA::VectorS<3, Real>> > >;
template class FieldBase< FieldLiftTraits<TopoD2, DLA::VectorS<2, DLA::VectorS<4, Real>> > >;
template class FieldBase< FieldLiftTraits<TopoD2, DLA::VectorS<2, DLA::VectorS<5, Real>> > >;
// For Boltzmann Implementation
template class FieldBase< FieldLiftTraits<TopoD2, DLA::VectorS<2, DLA::VectorS<9, Real>> > >;
//template class FieldBase< FieldLiftTraits<TopoD2, DLA::VectorS<2, DLA::VectorS<13, Real>> > >;
//template class FieldBase< FieldLiftTraits<TopoD2, DLA::VectorS<2, DLA::VectorS<16, Real>> > >;
template class FieldBase< FieldLiftTraits<TopoD2, DLA::VectorS<2, DLA::VectorS<NVar, Real>> > >;

// FieldSubGroup instantiation
template class FieldSubGroup<PhysD2, FieldLiftTraits<TopoD2, Real > >;
template class FieldSubGroup<PhysD2, FieldLiftTraits<TopoD2, DLA::VectorS<2, Real> > >;
template class FieldSubGroup<PhysD2, FieldLiftTraits<TopoD2, DLA::VectorS<2, DLA::VectorS<2, Real>> > >;
template class FieldSubGroup<PhysD2, FieldLiftTraits<TopoD2, DLA::VectorS<2, DLA::VectorS<3, Real>> > >;
template class FieldSubGroup<PhysD2, FieldLiftTraits<TopoD2, DLA::VectorS<2, DLA::VectorS<4, Real>> > >;
template class FieldSubGroup<PhysD2, FieldLiftTraits<TopoD2, DLA::VectorS<2, DLA::VectorS<5, Real>> > >;
// For Boltzmann Implementation
template class FieldSubGroup<PhysD2, FieldLiftTraits<TopoD2, DLA::VectorS<2, DLA::VectorS<9, Real>> > >;
//template class FieldSubGroup<PhysD2, FieldLiftTraits<TopoD2, DLA::VectorS<2, DLA::VectorS<13, Real>> > >;
//template class FieldSubGroup<PhysD2, FieldLiftTraits<TopoD2, DLA::VectorS<2, DLA::VectorS<16, Real>> > >;
template class FieldSubGroup<PhysD2, FieldLiftTraits<TopoD2, DLA::VectorS<2, DLA::VectorS<NVar, Real>> > >;

// Field Lift instantiation
template class FieldLift_DG_Cell< PhysD2, TopoD2, Real >;
template class FieldLift_DG_Cell< PhysD2, TopoD2, DLA::VectorS<2, Real> >;
template class FieldLift_DG_Cell< PhysD2, TopoD2, DLA::VectorS<2, DLA::VectorS<1, Real>> >;
template class FieldLift_DG_Cell< PhysD2, TopoD2, DLA::VectorS<2, DLA::VectorS<2, Real>> >;
template class FieldLift_DG_Cell< PhysD2, TopoD2, DLA::VectorS<2, DLA::VectorS<3, Real>> >;
template class FieldLift_DG_Cell< PhysD2, TopoD2, DLA::VectorS<2, DLA::VectorS<4, Real>> >;
template class FieldLift_DG_Cell< PhysD2, TopoD2, DLA::VectorS<2, DLA::VectorS<5, Real>> >;
// For Boltzmann Implementation
template class FieldLift_DG_Cell< PhysD2, TopoD2, DLA::VectorS<2, DLA::VectorS<9, Real>> >;
//template class FieldLift_DG_Cell< PhysD2, TopoD2, DLA::VectorS<2, DLA::VectorS<13, Real>> >;
//template class FieldLift_DG_Cell< PhysD2, TopoD2, DLA::VectorS<2, DLA::VectorS<16, Real>> >;
template class FieldLift_DG_Cell< PhysD2, TopoD2, DLA::VectorS<2, DLA::VectorS<NVar, Real>> >;

}
