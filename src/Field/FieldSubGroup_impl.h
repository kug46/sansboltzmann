// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(FIELDSUBGROUP_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif


#include "FieldSubGroup.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template<class PhysDim, class FieldTraits>
FieldSubGroup<PhysDim, FieldTraits>&
FieldSubGroup<PhysDim, FieldTraits>::operator=( const T& q )
{
  BaseType::operator=(q);
  return *this;
}

//----------------------------------------------------------------------------//
template<class PhysDim, class FieldTraits>
void
FieldSubGroup<PhysDim, FieldTraits>::cloneFrom( const FieldSubGroup& fld )
{
  SANS_ASSERT( &xfld_ == &fld.xfld_ );

  localHubTraceGroups_      = fld.localHubTraceGroups_;
  localInteriorTraceGroups_ = fld.localInteriorTraceGroups_;
  localBoundaryTraceGroups_ = fld.localBoundaryTraceGroups_;
  localCellGroups_          = fld.localCellGroups_;

  globalHubTraceGroups_      = fld.globalHubTraceGroups_;
  globalInteriorTraceGroups_ = fld.globalInteriorTraceGroups_;
  globalBoundaryTraceGroups_ = fld.globalBoundaryTraceGroups_;
  globalCellGroups_          = fld.globalCellGroups_;

  BaseType::cloneFrom(fld);
}

//----------------------------------------------------------------------------//
template<class PhysDim, class FieldTraits>
typename FieldSubGroup<PhysDim, FieldTraits>::FieldTraceGroupBase&
FieldSubGroup<PhysDim, FieldTraits>::getHubTraceGroupBaseGlobal( const int groupGlobal )
{
  int groupLocal = localHubTraceGroups_.at(groupGlobal);
  SANS_ASSERT( groupLocal >= 0 );
  return BaseType::getHubTraceGroupBase(groupLocal);
}
template<class PhysDim, class FieldTraits>
const typename FieldSubGroup<PhysDim, FieldTraits>::FieldTraceGroupBase&
FieldSubGroup<PhysDim, FieldTraits>::getHubTraceGroupBaseGlobal( const int groupGlobal ) const
{
  int groupLocal = localHubTraceGroups_.at(groupGlobal);
  SANS_ASSERT( groupLocal >= 0 );
  return BaseType::getHubTraceGroupBase(groupLocal);
}


//----------------------------------------------------------------------------//
template<class PhysDim, class FieldTraits>
typename FieldSubGroup<PhysDim, FieldTraits>::FieldTraceGroupBase&
FieldSubGroup<PhysDim, FieldTraits>::getInteriorTraceGroupBaseGlobal( const int groupGlobal )
{
  int groupLocal = localInteriorTraceGroups_.at(groupGlobal);
  SANS_ASSERT( groupLocal >= 0 );
  return BaseType::getInteriorTraceGroupBase(groupLocal);
}
template<class PhysDim, class FieldTraits>
const typename FieldSubGroup<PhysDim, FieldTraits>::FieldTraceGroupBase&
FieldSubGroup<PhysDim, FieldTraits>::getInteriorTraceGroupBaseGlobal( const int groupGlobal ) const
{
  int groupLocal = localInteriorTraceGroups_.at(groupGlobal);
  SANS_ASSERT( groupLocal >= 0 );
  return BaseType::getInteriorTraceGroupBase(groupLocal);
}


//----------------------------------------------------------------------------//
template<class PhysDim, class FieldTraits>
typename FieldSubGroup<PhysDim, FieldTraits>::FieldTraceGroupBase&
FieldSubGroup<PhysDim, FieldTraits>::getBoundaryTraceGroupBaseGlobal( const int groupGlobal )
{
  int groupLocal = localBoundaryTraceGroups_.at(groupGlobal);
  SANS_ASSERT( groupLocal >= 0 );
  return BaseType::getBoundaryTraceGroupBase(groupLocal);
}
template<class PhysDim, class FieldTraits>
const typename FieldSubGroup<PhysDim, FieldTraits>::FieldTraceGroupBase&
FieldSubGroup<PhysDim, FieldTraits>::getBoundaryTraceGroupBaseGlobal( const int groupGlobal ) const
{
  int groupLocal = localBoundaryTraceGroups_.at(groupGlobal);
  SANS_ASSERT( groupLocal >= 0 );
  return BaseType::getBoundaryTraceGroupBase(groupLocal);
}


//----------------------------------------------------------------------------//
template<class PhysDim, class FieldTraits>
typename FieldSubGroup<PhysDim, FieldTraits>::FieldCellGroupBase&
FieldSubGroup<PhysDim, FieldTraits>::getCellGroupBaseGlobal( const int groupGlobal )
{
  int groupLocal = localCellGroups_.at(groupGlobal);
  SANS_ASSERT( groupLocal >= 0 );
  return BaseType::getCellGroupBase(groupLocal);
}
template<class PhysDim, class FieldTraits>
const typename FieldSubGroup<PhysDim, FieldTraits>::FieldCellGroupBase&
FieldSubGroup<PhysDim, FieldTraits>::getCellGroupBaseGlobal( const int groupGlobal ) const
{
  int groupLocal = localCellGroups_.at(groupGlobal);
  SANS_ASSERT( groupLocal >= 0 );
  return BaseType::getCellGroupBase(groupLocal);
}

//----------------------------------------------------------------------------//
template<class PhysDim, class FieldTraits>
void
FieldSubGroup<PhysDim, FieldTraits>::resizeHubTraceGroups( const std::vector<int>& HubTraceGroups )
{
  globalHubTraceGroups_ = HubTraceGroups;

  this->resizeHubTraceGroups(HubTraceGroups.size());

  localHubTraceGroups_.clear();
  localHubTraceGroups_.resize(xfld_.nHubTraceGroups(), -1);
  for (std::size_t group = 0; group < HubTraceGroups.size(); group++)
    localHubTraceGroups_[HubTraceGroups[group]] = group;
}

//----------------------------------------------------------------------------//
template<class PhysDim, class FieldTraits>
void
FieldSubGroup<PhysDim, FieldTraits>::resizeInteriorTraceGroups( const std::vector<int>& InteriorTraceGroups )
{
  globalInteriorTraceGroups_ = InteriorTraceGroups;

  this->resizeInteriorTraceGroups(InteriorTraceGroups.size());

  localInteriorTraceGroups_.clear();
  localInteriorTraceGroups_.resize(xfld_.nInteriorTraceGroups(), -1);
  for (std::size_t group = 0; group < InteriorTraceGroups.size(); group++)
    localInteriorTraceGroups_[InteriorTraceGroups[group]] = group;
}

//----------------------------------------------------------------------------//
template<class PhysDim, class FieldTraits>
void
FieldSubGroup<PhysDim, FieldTraits>::resizeBoundaryTraceGroups( const std::vector<int>& BoundaryTraceGroups )
{
  globalBoundaryTraceGroups_ = BoundaryTraceGroups;

  this->resizeBoundaryTraceGroups(BoundaryTraceGroups.size());

  localBoundaryTraceGroups_.clear();
  localBoundaryTraceGroups_.resize(xfld_.nBoundaryTraceGroups(), -1);
  for (std::size_t group = 0; group < BoundaryTraceGroups.size(); group++)
    localBoundaryTraceGroups_[BoundaryTraceGroups[group]] = group;
}

//----------------------------------------------------------------------------//
template<class PhysDim, class FieldTraits>
void
FieldSubGroup<PhysDim, FieldTraits>::resizeCellGroups( const std::vector<int>& CellGroups )
{
  globalCellGroups_ = CellGroups;

  this->resizeCellGroups(CellGroups.size());

  localCellGroups_.clear();
  localCellGroups_.resize(xfld_.nCellGroups(), -1);
  for (std::size_t group = 0; group < CellGroups.size(); group++)
    localCellGroups_[CellGroups[group]] = group;
}


//----------------------------------------------------------------------------//
template<class PhysDim, class FieldTraits>
std::vector<int>
FieldSubGroup<PhysDim, FieldTraits>::createHubGroupIndex()
{
  // By default use all hub groups
  std::vector<int> HubGroups(xfld_.nHubTraceGroups());
  for ( std::size_t i = 0; i < HubGroups.size(); i++ )
    HubGroups[i] = i;

  //allocate the hub trace groups and create global to local maps
  this->resizeHubTraceGroups( HubGroups );

  return HubGroups;
}

template<class PhysDim, class FieldTraits>
void
FieldSubGroup<PhysDim, FieldTraits>::checkHubGroupIndex(const std::vector<int>& HubGroups)
{
  // Check that the groups asked for are within the range of available groups
  for ( std::size_t i = 0; i < HubGroups.size(); i++ )
    SANS_ASSERT( HubGroups[i] >=0 && HubGroups[i] < xfld_.nHubTraceGroups());

  //allocate the hub trace groups and create global to local maps
  this->resizeHubTraceGroups( HubGroups );
}


//----------------------------------------------------------------------------//
template<class PhysDim, class FieldTraits>
std::vector<int>
FieldSubGroup<PhysDim, FieldTraits>::createInteriorGroupIndex()
{
  // By default all interior groups are used to construct lagrange multipliers
  std::vector<int> InteriorGroups(xfld_.nInteriorTraceGroups());
  for ( std::size_t i = 0; i < InteriorGroups.size(); i++ )
    InteriorGroups[i] = i;

  //allocate the interior trace groups and create global to local maps
  this->resizeInteriorTraceGroups( InteriorGroups );

  return InteriorGroups;
}

template<class PhysDim, class FieldTraits>
void
FieldSubGroup<PhysDim, FieldTraits>::checkInteriorGroupIndex(const std::vector<int>& InteriorGroups)
{
  // Check that the groups asked for are within the range of available groups
  for ( std::size_t i = 0; i < InteriorGroups.size(); i++ )
    SANS_ASSERT( InteriorGroups[i] >=0 && InteriorGroups[i] < xfld_.nInteriorTraceGroups());

  //allocate the interior trace groups and create global to local maps
  this->resizeInteriorTraceGroups( InteriorGroups );
}


//----------------------------------------------------------------------------//
template<class PhysDim, class FieldTraits>
std::vector<int>
FieldSubGroup<PhysDim, FieldTraits>::createBoundaryGroupIndex()
{
  // By default all boundary groups are used to construct lagrange multipliers
  std::vector<int> BoundaryGroups(xfld_.nBoundaryTraceGroups());
  for ( std::size_t i = 0; i < BoundaryGroups.size(); i++ )
    BoundaryGroups[i] = i;

  //allocate the boundary trace groups and create global to local maps
  this->resizeBoundaryTraceGroups( BoundaryGroups );

  return BoundaryGroups;
}

template<class PhysDim, class FieldTraits>
void
FieldSubGroup<PhysDim, FieldTraits>::checkBoundaryGroupIndex(const std::vector<int>& BoundaryGroups)
{
  // Check that the groups asked for are within the range of available groups
  for ( std::size_t i = 0; i < BoundaryGroups.size(); i++ )
    SANS_ASSERT( BoundaryGroups[i] >=0 && BoundaryGroups[i] < xfld_.nBoundaryTraceGroups());

  //allocate the boundary trace groups and create global to local maps
  this->resizeBoundaryTraceGroups( BoundaryGroups );
}


//----------------------------------------------------------------------------//
template<class PhysDim, class FieldTraits>
std::vector<int>
FieldSubGroup<PhysDim, FieldTraits>::createCellGroupIndex()
{
  // By default all boundary groups are used to construct lagrange multipliers
  std::vector<int> CellGroups(xfld_.nCellGroups());
  for ( std::size_t i = 0; i < CellGroups.size(); i++ )
    CellGroups[i] = i;

  //allocate the boundary trace groups and create global to local maps
  this->resizeCellGroups( CellGroups );

  return CellGroups;
}

template<class PhysDim, class FieldTraits>
void
FieldSubGroup<PhysDim, FieldTraits>::checkCellGroupIndex(const std::vector<int>& CellGroups)
{
  // Check that the groups asked for are within the range of available groups
  for ( std::size_t i = 0; i < CellGroups.size(); i++ )
    SANS_ASSERT( CellGroups[i] >=0 && CellGroups[i] < xfld_.nCellGroups());

  //allocate the boundary trace groups and create global to local maps
  this->resizeCellGroups( CellGroups );
}


} //namespace SANS
