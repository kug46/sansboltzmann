// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDSPACETIME_CG_CELL_H
#define FIELDSPACETIME_CG_CELL_H

#include <vector>

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "Field/Element/ElementAssociativitySpacetimeConstructor.h"
#include "FieldSpacetime.h"
#include "EmbeddedCGType.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// solution field: 4D CG element-field
//
// basis functions set to hierarchical
//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
class Field_CG_Cell;

/** Field_CG_Cell is a container for a 4D CG element-field
 *
 * Field_CG_Field is a Field object that is specialized specifically for use holding
 * continuous representations of a field!
 */
template <class PhysDim, class T>
class Field_CG_Cell< PhysDim, TopoD4, T > : public Field< PhysDim, TopoD4, T >
{
public:
  // typedef away the base type, data type
  typedef Field< PhysDim, TopoD4, T > BaseType;
  typedef T ArrayQ;

  // constructor to create a CG field of a given basis function order and category based on a mesh field
  Field_CG_Cell( const XField<PhysDim, TopoD4>& xfld, const int order, const BasisFunctionCategory& category,
                 const embeddedType eType = RegularCGField );

  // constructor to create a CG field of a given basis function order and category based on a mesh field
  // but with a set number of cell groups???
  explicit Field_CG_Cell( const XField<PhysDim, TopoD4>& xfld, const int order, const BasisFunctionCategory& category,
                 const std::vector<int>& CellGroups,
                 const embeddedType eType = RegularCGField ); //no default here - too easy to confused with the above constructor

  // external constructor to create a CG field of a given basis function order and category based on an input
  // mesh field and over a particular number of cellgroupsets
  explicit Field_CG_Cell( const std::vector<std::vector<int>>& CellGroupSets,
                          const XField<PhysDim, TopoD4>& xfld, const int order, const BasisFunctionCategory& category,
                          const embeddedType eType = RegularCGField );

  // constructor to create a copy of a CG field
  Field_CG_Cell( const Field_CG_Cell& fld, const FieldCopy&tag );

  // equality operator overload for setting up a CG field based on a data array q
  Field_CG_Cell& operator=( const ArrayQ& q );

  // virtual class to denote the SpaceType of the Field
  virtual SpaceType spaceType() const override { return SpaceType::Continuous; }

  bool needsEmbeddedGhosts() const { return needsEmbeddedGhosts_; }

protected:
  // initialization function for constructor
  void init( const int order, const BasisFunctionCategory& category, const std::vector<std::vector<int>>& CellGroupSetss );

  const bool needsEmbeddedGhosts_;

  // grandfather in a bunch of variables from the base class!
  using BaseType::nElem_;                         // number of elements
  using BaseType::nDOF_;                          // number of DOFs
  using BaseType::DOF_;                           // pointer to field DOFs
  using BaseType::xfld_;                          // pointer to the XField
  using BaseType::cellGroups_;                    // the cell group pointer array
  using BaseType::localCellGroups_;               // pointer to the local cell group pointer array
  using BaseType::interiorTraceGroups_;           // pointer to the interior trace group pointer array
  using BaseType::boundaryTraceGroups_;           // pointer to the boundary trace group pointer array
  using BaseType::localBoundaryTraceGroups_;      // pointer to the local boundary trace group pointer array
};

}

#endif  // FIELDSPACETIME_CG_CELL_H
