// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDDATAVECTORD_BOUNDARYCELL_H_
#define FIELDDATAVECTORD_BOUNDARYCELL_H_

#include <vector>
#include <array>
#include <map>

#include "Field/Field.h"
#include "Field/FieldData/FieldDataMatrixD_BoundaryCell.h"
#include "Field/tools/for_each_BoundaryTraceGroup.h"
#include "tools/linspace.h"

namespace SANS
{

template<class PhysDim>
class TrackBoundaryCells;

//-----------------------------------------------------------------------------
//
// Represents VectorD data associated with each boundary cell of a field
// i.e. useful for residuals of auxiliary variables on boundary cells in HDG
//
//-----------------------------------------------------------------------------

template<class T>
class FieldDataVectorD_BoundaryCell
{
public:

  template<class PhysDim, class TopoDim, class ArrayQ>
  explicit FieldDataVectorD_BoundaryCell( const Field<PhysDim, TopoDim, ArrayQ>& fld )
  {
    //Vector to store the number of boundary cells in each cell group
    std::vector<int> boundaryCellCount(fld.getXField().nCellGroups(), 0);

    //Get the cellgroup and elem indices of all boundary cells in the mesh
    if (fld.getXField().nBoundaryTraceGroups() > 0)
    {
      std::vector<int> globalBTraceGroups = linspace(0, fld.getXField().nBoundaryTraceGroups()-1);
      for_each_BoundaryTraceGroup<TopoDim>::apply( TrackBoundaryCells<PhysDim>(cellIndexMap_, boundaryCellCount, globalBTraceGroups),
                                                   fld.getXField() );
    }

    //Count the total number of elements required for the array
    int nData = 0;

    for (int group = 0; group < fld.getXField().nCellGroups(); group++)
    {
      const int nBoundaryCells = boundaryCellCount[group];
      const int nBasis = fld.getCellGroupBase(group).nBasis();
      nData += nBoundaryCells*nBasis;
    }

    // Allocate the memory for all the matrices
    data_ = new T[nData];

    // Create the MatrixDView_Array instances for each cell group
    int offset = 0;
    for (int group = 0; group < fld.getXField().nCellGroups(); group++)
    {
      const int nBoundaryCells = boundaryCellCount[group];
      const int nBasis = fld.getCellGroupBase(group).nBasis();

      cellData_.push_back(DLA::MatrixDView_Array<T>(data_ + offset, nBasis, 1, nBoundaryCells));
      offset += nBoundaryCells*nBasis;
    }
  }


  FieldDataVectorD_BoundaryCell( const FieldDataVectorD_BoundaryCell& ) = delete;
  FieldDataVectorD_BoundaryCell& operator=( const FieldDataVectorD_BoundaryCell& ) = delete;

  // Simple assignment operator
  T operator=(const T& val)
  {
    for (std::size_t i = 0; i < cellData_.size(); i++)
      cellData_[i] = val;

    return val;
  }

  ~FieldDataVectorD_BoundaryCell()
  {
    delete [] data_;
  }

  DLA::MatrixDView<T> getCell(const int cellgroupGlobal, const int cellelem )
  {
    int index = cellIndexMap_.at({{cellgroupGlobal, cellelem}});
    return cellData_.at(cellgroupGlobal)[index];
  }

  const DLA::MatrixDView<T> getCell(const int cellgroupGlobal, const int cellelem ) const
  {
    int index = cellIndexMap_.at({{cellgroupGlobal, cellelem}});
    return cellData_.at(cellgroupGlobal)[index];
  }

protected:
  // Protected constructor for derived classes
  FieldDataVectorD_BoundaryCell() : data_(NULL) {};

  std::map<std::array<int,2>, int> cellIndexMap_; //map from {cellgroup, cellelem} to index in data array

  std::vector<DLA::MatrixDView_Array<T>> cellData_;
  T *data_;
};

}

#endif // FIELDDATAVECTORD_BOUNDARYCELL_H_
