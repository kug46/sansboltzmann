// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDDATAREAL_CELL_H_
#define FIELDDATAREAL_CELL_H_

#include <vector>

#include "Field/Field.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Array.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

namespace SANS
{

//-----------------------------------------------------------------------------
//
// Represents MatrixD data associated with cell element of a field
//
//-----------------------------------------------------------------------------

class FieldDataReal_Cell
{
public:

  template<class FieldType>
  FieldDataReal_Cell( const FieldType& fld ) :
    globalCellGroups_(fld.getGlobalCellGroups()),
    localCellGroups_(fld.getLocalCellGroups())
  {
    //Count the total number of elements in the relevant cell groups
    for (int i = 0; i < fld.nCellGroups(); i++)
    {
      const int nCellElem = fld.getCellGroupBase(i).nElem();
      cellData_.push_back(DLA::VectorD<Real>(nCellElem));

      for (int k = 0; k < nCellElem; k++) //initialize
        cellData_[i][k] = 0;
    }
  }

  FieldDataReal_Cell( const FieldDataReal_Cell& ) = delete;
  FieldDataReal_Cell& operator=( const FieldDataReal_Cell& ) = delete;
//
//  ~FieldDataReal_Cell()
//  {  }

  DLA::VectorD<Real>& getCellGroupGlobal( const int groupGlobal )
  {
    int groupLocal = localCellGroups_.at(groupGlobal);
    SANS_ASSERT( groupLocal >= 0 );
    return cellData_.at(groupLocal);
  }

  const DLA::VectorD<Real>& getCellGroupGlobal( const int groupGlobal ) const
  {
    int groupLocal = localCellGroups_.at(groupGlobal);
    SANS_ASSERT( groupLocal >= 0 );
    return cellData_.at(groupLocal);
  }

protected:

  std::vector<DLA::VectorD<Real>> cellData_;

  // This map is for taking a local subset index to a global group index
  std::vector<int> globalCellGroups_;

  // Field groups can be constructed with a subset of the global XField groups.
  // These are maps for taking a global group index to a local subset index.
  std::vector<int> localCellGroups_;
};


}

#endif // FIELDDATAREAL_CELL_H_
