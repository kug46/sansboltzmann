// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDDATAMATRIXD_BOUNDARYCELL_H_
#define FIELDDATAMATRIXD_BOUNDARYCELL_H_

#include <vector>
#include <array>
#include <map>

#include "Field/Field.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Array.h"
#include "Field/tools/for_each_BoundaryTraceGroup.h"
#include "tools/linspace.h"

namespace SANS
{

template<class PhysDim>
class TrackBoundaryCells;

//-----------------------------------------------------------------------------
//
// Represents MatrixD data associated with each boundary cell of a field
// i.e. useful for jacobians of auxiliary variables on boundary cells in HDG
//
//-----------------------------------------------------------------------------

template<class T>
class FieldDataMatrixD_BoundaryCell
{
public:
  template<class PhysDim, class TopoDim, class ArrayQ>
  explicit FieldDataMatrixD_BoundaryCell( const Field<PhysDim, TopoDim, ArrayQ>& fld )
  {
    //Vector to store the number of boundary cells in each cell group
    std::vector<int> boundaryCellCount(fld.getXField().nCellGroups(), 0);

    //Get the cellgroup and elem indices of all boundary cells in the mesh
    if (fld.getXField().nBoundaryTraceGroups() > 0)
    {
      std::vector<int> globalBTraceGroups = linspace(0, fld.getXField().nBoundaryTraceGroups()-1);
      for_each_BoundaryTraceGroup<TopoDim>::apply( TrackBoundaryCells<PhysDim>(cellIndexMap_, boundaryCellCount, globalBTraceGroups),
                                                   fld.getXField() );
    }

    //Count the total number of elements required for the array
    int nData = 0;

    for (int group = 0; group < fld.getXField().nCellGroups(); group++)
    {
      const int nBoundaryCells = boundaryCellCount[group];
      const int nBasis = fld.getCellGroupBase(group).nBasis();
      nData += nBoundaryCells*nBasis*nBasis;
    }

    // Allocate the memory for all the matrices
    data_ = new T[nData];

    // Create the MatrixDView_Array instances for each cell group
    int offset = 0;
    for (int group = 0; group < fld.getXField().nCellGroups(); group++)
    {
      const int nBoundaryCells = boundaryCellCount[group];
      const int nBasis = fld.getCellGroupBase(group).nBasis();

      cellData_.push_back(DLA::MatrixDView_Array<T>(data_ + offset, nBasis, nBasis, nBoundaryCells));
      offset += nBoundaryCells*nBasis*nBasis;
    }
  }


  template<class PhysDim, class TopoDim, class ArrayQ, class ArrayQ2>
  explicit FieldDataMatrixD_BoundaryCell( const Field<PhysDim, TopoDim, ArrayQ>& fld0, const Field<PhysDim, TopoDim, ArrayQ2>& fld1)
  {

    SANS_ASSERT( &fld0.getXField() == &fld1.getXField());

    //Vector to store the number of boundary cells in each cell group
    std::vector<int> boundaryCellCount(fld0.getXField().nCellGroups(), 0);

    //Get the cellgroup and elem indices of all boundary cells in the mesh
    if (fld0.getXField().nBoundaryTraceGroups() > 0)
    {
      std::vector<int> globalBTraceGroups = linspace(0, fld0.getXField().nBoundaryTraceGroups()-1);
      for_each_BoundaryTraceGroup<TopoDim>::apply( TrackBoundaryCells<PhysDim>(cellIndexMap_, boundaryCellCount, globalBTraceGroups),
                                                   fld0.getXField() );
    }

    //Count the total number of elements required for the array
    int nData = 0;

    for (int group = 0; group < fld0.getXField().nCellGroups(); group++)
    {
      const int nBoundaryCells = boundaryCellCount[group];
      const int nBasis0 = fld0.getCellGroupBase(group).nBasis();
      const int nBasis1 = fld1.getCellGroupBase(group).nBasis();
      nData += nBoundaryCells*nBasis0*nBasis1;
    }

    // Allocate the memory for all the matrices
    data_ = new T[nData];

    // Create the MatrixDView_Array instances for each cell group
    int offset = 0;
    for (int group = 0; group < fld0.getXField().nCellGroups(); group++)
    {
      const int nBoundaryCells = boundaryCellCount[group];
      const int nBasis0 = fld0.getCellGroupBase(group).nBasis();
      const int nBasis1 = fld1.getCellGroupBase(group).nBasis();

      cellData_.push_back(DLA::MatrixDView_Array<T>(data_ + offset, nBasis0, nBasis1, nBoundaryCells));
      offset += nBoundaryCells*nBasis0*nBasis1;
    }
  }


  FieldDataMatrixD_BoundaryCell( const FieldDataMatrixD_BoundaryCell& ) = delete;
  FieldDataMatrixD_BoundaryCell& operator=( const FieldDataMatrixD_BoundaryCell& ) = delete;

  // Simple assignment operator
  T operator=(const T& val)
  {
    for (std::size_t i = 0; i < cellData_.size(); i++)
      for (int j = 0; j < cellData_[i].size(); j++)
        cellData_[i][j] = val;

    return val;
  }

  ~FieldDataMatrixD_BoundaryCell()
  {
    delete [] data_;
  }

  DLA::MatrixDView<T> getCell(const int cellgroupGlobal, const int cellelem )
  {
    int index = cellIndexMap_.at({{cellgroupGlobal, cellelem}});
    return cellData_.at(cellgroupGlobal)[index];
  }

  const DLA::MatrixDView<T> getCell(const int cellgroupGlobal, const int cellelem ) const
  {
    int index = cellIndexMap_.at({{cellgroupGlobal, cellelem}});
    return cellData_.at(cellgroupGlobal)[index];
  }

protected:
  // Protected constructor for derived classes
  FieldDataMatrixD_BoundaryCell() : data_(NULL) {};

  std::map<std::array<int,2>, int> cellIndexMap_; //map from {cellgroup, cellelem} to index in data array

  std::vector<DLA::MatrixDView_Array<T>> cellData_;
  T *data_;
};

template<class PhysDim>
class TrackBoundaryCells : public GroupFunctorBoundaryTraceType< TrackBoundaryCells<PhysDim> >
{
public:

  TrackBoundaryCells( std::map<std::array<int,2>, int>& cellIndexMap, std::vector<int>& boundaryCellCount,
                      const std::vector<int>& traceGroups ) :
    cellIndexMap_(cellIndexMap), boundaryCellCount_(boundaryCellCount), traceGroups_(traceGroups) {}

  std::size_t nBoundaryTraceGroups() const          { return traceGroups_.size(); }
  std::size_t boundaryTraceGroup(const int n) const { return traceGroups_[n];     }

  //----------------------------------------------------------------------------//
  // Integration function that integrates each element in the trace group
  template <class TopologyTrace>
  void
  apply( const typename XField<PhysDim, typename TopologyTrace::CellTopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
         const int traceGroupGlobal )
  {
    std::pair<std::map<std::array<int,2>, int>::iterator,bool> ret;

    const int cellgroupL = xfldTrace.getGroupLeft();

    // loop over elements within group
    const int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      const int elemL = xfldTrace.getElementLeft( elem );

      ret = cellIndexMap_.insert(std::pair<std::array<int,2>, int>({{cellgroupL, elemL}}, boundaryCellCount_[cellgroupL]));

      //increment the boundary cell count for this cellgroup if the insert succeeded
      if (ret.second == true) boundaryCellCount_[cellgroupL]++;
    }
  }

protected:
  std::map<std::array<int,2>, int>& cellIndexMap_;
  std::vector<int>& boundaryCellCount_;
  const std::vector<int> traceGroups_;
};

}

#endif // FIELDDATAMATRIXD_BOUNDARYCELL_H_
