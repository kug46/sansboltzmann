// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDDATAMASSMATRIX_CELL_H_
#define FIELDDATAMASSMATRIX_CELL_H_

#include "FieldDataMatrixD_Cell.h"

#include "Field/FieldTypes.h"

namespace SANS
{

//-----------------------------------------------------------------------------
//
// Cell element mass matrix data
//
//-----------------------------------------------------------------------------

class FieldDataInvMassMatrix_Cell : public FieldDataMatrixD_Cell<Real>
{
public:
  typedef FieldDataMatrixD_Cell<Real> BaseType;

  template<class PhysDim, class TopoDim, class T> // cppcheck-suppress noExplicitConstructor
  FieldDataInvMassMatrix_Cell( const Field<PhysDim,TopoDim,T>& fld );

  FieldDataInvMassMatrix_Cell( const FieldDataInvMassMatrix_Cell& ) = delete;
  FieldDataInvMassMatrix_Cell& operator=( const FieldDataInvMassMatrix_Cell& ) = delete;

protected:
  using BaseType::cellData_;
  using BaseType::data_;

  using BaseType::globalCellGroups_;
  using BaseType::localCellGroups_;
};

}

#endif // FIELDDATAMASSMATRIX_CELL_H_
