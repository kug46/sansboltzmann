// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDDATAMATRIXD_LIFT_BOUNDARYTRACE_H_
#define FIELDDATAMATRIXD_LIFT_BOUNDARYTRACE_H_

#include <vector>
#include <map>

#include "Field/XField.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Array.h"

namespace SANS
{

//-----------------------------------------------------------------------------
//
// Represents MatrixD data associated with cell element of a field
//
//-----------------------------------------------------------------------------

template<class T>
class FieldDataMatrixD_Lift_BoundaryTrace
{
public:

  template<class PhysDim, class TopoDim, class ArrayQ>
  FieldDataMatrixD_Lift_BoundaryTrace( const FieldLift<PhysDim,TopoDim,DLA::VectorS<PhysDim::D,ArrayQ>>& fld,
                                       const std::map< std::string, std::vector<int> >& BCBoundaryGroups )
  {
    //Count the total number of trace elements in the relevant boundary trace groups
    int nData = 0;
    int nLocalBoundaryGroups = 0;
    for (auto i = BCBoundaryGroups.begin(); i != BCBoundaryGroups.end(); ++i)
    {
      const std::vector<int>& boundryGroups = i->second;
      for ( std::size_t j = 0; j < boundryGroups.size(); ++j)
      {
        int traceGroupGlobal = boundryGroups[j];
        int cellGroupGlobalL = fld.getXField().getBoundaryTraceGroupBase(traceGroupGlobal).getGroupLeft();
        const int nTraceElem = fld.getXField().getBoundaryTraceGroupBase(traceGroupGlobal).nElem();
        const int nBasis = fld.getCellGroupBase(cellGroupGlobalL).nBasis();
        nData += nTraceElem*nBasis*nBasis;
        nLocalBoundaryGroups++;
      }
    }

    // Allocate the matrix memory
    data_ = new T[nData];

    globalBoundaryTraceGroups_.resize(nLocalBoundaryGroups);
    localBoundaryTraceGroups_.resize(fld.getXField().nBoundaryTraceGroups());

    int offset = 0;
    nLocalBoundaryGroups = 0;
    for (auto i = BCBoundaryGroups.begin(); i != BCBoundaryGroups.end(); ++i)
    {
      const std::vector<int>& boundryGroups = i->second;
      for ( std::size_t j = 0; j < boundryGroups.size(); ++j)
      {
        int traceGroupGlobal = boundryGroups[j];
        int cellGroupGlobalL = fld.getXField().getBoundaryTraceGroupBase(traceGroupGlobal).getGroupLeft();
        const int nTraceElem = fld.getXField().getBoundaryTraceGroupBase(traceGroupGlobal).nElem();
        const int nBasis = fld.getCellGroupBase(cellGroupGlobalL).nBasis();
        traceData_.push_back(DLA::MatrixDView_Array<T>(data_ + offset, nBasis, nBasis, nTraceElem));
        offset += nTraceElem*nBasis*nBasis;

        globalBoundaryTraceGroups_[nLocalBoundaryGroups] = traceGroupGlobal;
        localBoundaryTraceGroups_[traceGroupGlobal] = nLocalBoundaryGroups;

        nLocalBoundaryGroups++;
      }
    }
  }


  FieldDataMatrixD_Lift_BoundaryTrace( const FieldDataMatrixD_Lift_BoundaryTrace& ) = delete;
  FieldDataMatrixD_Lift_BoundaryTrace& operator=( const FieldDataMatrixD_Lift_BoundaryTrace& ) = delete;

  ~FieldDataMatrixD_Lift_BoundaryTrace()
  {
    delete [] data_;
  }

  DLA::MatrixDView_Array<T>& getBoundaryTraceGroupGlobal( const int groupGlobal )
  {
    int groupLocal = localBoundaryTraceGroups_.at(groupGlobal);
    SANS_ASSERT( groupLocal >= 0 );
    return traceData_.at(groupGlobal);
  }
  const DLA::MatrixDView_Array<T>& getBoundaryTraceGroupGlobal( const int groupGlobal ) const
  {
    int groupLocal = localBoundaryTraceGroups_.at(groupGlobal);
    SANS_ASSERT( groupLocal >= 0 );
    return traceData_.at(groupGlobal);
  }

protected:
  // Protected constructor for derived classes
  FieldDataMatrixD_Lift_BoundaryTrace() : data_(NULL) {};

  std::vector<DLA::MatrixDView_Array<T>> traceData_;
  T *data_;

  // This map is for taking a local subset index to a global group index
  std::vector<int> globalBoundaryTraceGroups_;

  // Field groups can be constructed with a subset of the global XField groups.
  // These are maps for taking a global group index to a local subset index.
  std::vector<int> localBoundaryTraceGroups_;
};


}

#endif // FIELDDATAMATRIXD_LIFT_BOUNDARYTRACE_H_
