// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDDATAMATRIXD_BOUNDARYTRACE_H_
#define FIELDDATAMATRIXD_BOUNDARYTRACE_H_

#include <vector>

#include "Field/XField.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Array.h"

namespace SANS
{

//-----------------------------------------------------------------------------
//
// Represents MatrixD data associated with boundary trace element of a field
//
//-----------------------------------------------------------------------------

template<class T>
class FieldDataMatrixD_BoundaryTrace
{
public:

  template<class PhysDim, class TopoDim, class ArrayQ>
  FieldDataMatrixD_BoundaryTrace( const Field<PhysDim,TopoDim,ArrayQ>& qfld,
                                  const Field<PhysDim,TopoDim,ArrayQ>& qIfld )
  {
    SANS_ASSERT( &qfld.getXField() == &qIfld.getXField() );

    //Count the total number of elements required for the array
    int nData = 0;

    for (int group = 0; group < qIfld.nBoundaryTraceGroups(); group++)
    {
      int cellGroupGlobalL = qIfld.getXField().getBoundaryTraceGroupBase(group).getGroupLeft();
      const int nTraceElem = qIfld.getXField().getBoundaryTraceGroupBase(group).nElem();

      const int nBasisL = qfld.getCellGroupBase(cellGroupGlobalL).nBasis();
      const int nBasisI = qIfld.getBoundaryTraceGroupBase(group).nBasis();

      nData += nTraceElem*nBasisL*nBasisI;
    }

    // Allocate the matrix memory
    data_ = new T[nData];

    int offset = 0;
    for (int group = 0; group < qIfld.nBoundaryTraceGroups(); group++)
    {
      int cellGroupGlobalL = qIfld.getXField().getBoundaryTraceGroupBase(group).getGroupLeft();
      const int nTraceElem = qIfld.getXField().getBoundaryTraceGroupBase(group).nElem();

      const int nBasisL = qfld.getCellGroupBase(cellGroupGlobalL).nBasis();
      const int nBasisI = qIfld.getBoundaryTraceGroupBase(group).nBasis();

      traceData_.push_back(DLA::MatrixDView_Array<T>(data_ + offset, nBasisL, nBasisI, nTraceElem));
      offset += nTraceElem*nBasisL*nBasisI;
    }
  }


  FieldDataMatrixD_BoundaryTrace( const FieldDataMatrixD_BoundaryTrace& ) = delete;
  FieldDataMatrixD_BoundaryTrace& operator=( const FieldDataMatrixD_BoundaryTrace& ) = delete;

  // Simple assignment operator
  T operator=(const T& val)
  {
    for (std::size_t i = 0; i < traceData_.size(); i++)
      traceData_[i] = val;

    return val;
  }

  ~FieldDataMatrixD_BoundaryTrace()
  {
    delete [] data_;
  }

  DLA::MatrixDView_Array<T>& getBoundaryTraceGroupGlobal( const int groupGlobal )
  {
    SANS_ASSERT( groupGlobal >= 0 );
    return traceData_.at(groupGlobal);
  }
  const DLA::MatrixDView_Array<T>& getBoundaryTraceGroupGlobal( const int groupGlobal ) const
  {
    SANS_ASSERT( groupGlobal >= 0 );
    return traceData_.at(groupGlobal);
  }

protected:
  // Protected constructor for derived classes
  FieldDataMatrixD_BoundaryTrace() : data_(NULL) {};

  std::vector<DLA::MatrixDView_Array<T>> traceData_;
  T *data_;
};


}

#endif // FIELDDATAMATRIXD_BOUNDARYTRACE_H_
