// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDDATAMATRIXD_TRACELIFT_H_
#define FIELDDATAMATRIXD_TRACELIFT_H_

#include <vector>

#include "Field/Field.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Array2D.h"

namespace SANS
{

//-----------------------------------------------------------------------------
//
// Represents a vector of MatrixD data associated with each trace of cell element of a field
// i.e. useful for jacobians of lifing operators
//
//-----------------------------------------------------------------------------

template<class T>
class FieldDataMatrixD_TraceLift
{
public:

  template<class PhysDim, class TopoDim, class ArrayQ>
  explicit FieldDataMatrixD_TraceLift( const FieldLift<PhysDim,TopoDim,DLA::VectorS<PhysDim::D,ArrayQ>>& fld ) :
    globalCellGroups_(fld.getGlobalCellGroups()),
    localCellGroups_(fld.getLocalCellGroups())
  {
    //Count the total number of trace elements in the relevant cell groups
    int nData = 0;
    for (int i = 0; i < fld.nCellGroups(); i++)
    {
      const int nCellElem = fld.getCellGroupBase(i).nElem();
      const int nTrace = fld.getCellGroupBase(i).nElemTrace();
      const int nBasis = fld.getCellGroupBase(i).nBasis();
      nData += nCellElem*nTrace*nBasis*nBasis;
    }

    // Allocate the memory for all the matrices
    data_ = new T[nData];

    // Create the MatrixDView_Array2D instances for each cell group
    int offset = 0;
    for (int i = 0; i < fld.nCellGroups(); i++)
    {
      const int nCellElem = fld.getCellGroupBase(i).nElem();
      const int nTrace = fld.getCellGroupBase(i).nElemTrace();
      const int nBasis = fld.getCellGroupBase(i).nBasis();
      cellData_.push_back(DLA::MatrixDView_Array2D<T>(data_ + offset, nBasis, nBasis, nCellElem, nTrace));
      offset += nCellElem*nTrace*nBasis*nBasis;
    }
  }


  FieldDataMatrixD_TraceLift( const FieldDataMatrixD_TraceLift& ) = delete;
  FieldDataMatrixD_TraceLift& operator=( const FieldDataMatrixD_TraceLift& ) = delete;

  // Simple assignent operator
  T operator=(const T& val)
  {
    for ( std::size_t i = 0; i < cellData_.size(); i++)
      cellData_[i] = val;

    return val;
  }

  ~FieldDataMatrixD_TraceLift()
  {
    delete [] data_;
  }

  DLA::MatrixDView_Array2D<T>& getCellGroupGlobal( const int groupGlobal )
  {
    int groupLocal = localCellGroups_.at(groupGlobal);
    SANS_ASSERT( groupLocal >= 0 );
    return cellData_.at(groupLocal);
  }
  const DLA::MatrixDView_Array2D<T>& getCellGroupGlobal( const int groupGlobal ) const
  {
    int groupLocal = localCellGroups_.at(groupGlobal);
    SANS_ASSERT( groupLocal >= 0 );
    return cellData_.at(groupLocal);
  }

protected:
  // Protected constructor for derived classes
  FieldDataMatrixD_TraceLift() : data_(NULL) {};

  std::vector<DLA::MatrixDView_Array2D<T>> cellData_;
  T *data_;

  // This map is for taking a local subset index to a global group index
  std::vector<int> globalCellGroups_;

  // Field groups can be constructed with a subset of the global XField groups.
  // These are maps for taking a global group index to a local subset index.
  std::vector<int> localCellGroups_;
};

}

#endif // FIELDDATAMATRIXD_TRACELIFT_H_
