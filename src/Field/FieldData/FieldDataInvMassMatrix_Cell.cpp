// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "FieldDataInvMassMatrix_Cell.h"

#include <vector>

#include "tools/SANSException.h"

#include "LinearAlgebra/DenseLinAlg/InverseLU.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Det.h"

#include "Quadrature/Quadrature.h"

#include "Field/Element/ElementalMassMatrix.h"

#include "Field/FieldLine.h"
#include "Field/FieldArea.h"
#include "Field/FieldVolume.h"
#include "Field/FieldSpacetime.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldSpacetime_DG_Cell.h"

#include "Field/FieldLine_CG_Cell.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldVolume_CG_Cell.h"

#include "Field/XFieldLine.h"
#include "Field/XFieldArea.h"
#include "Field/XFieldVolume.h"
#include "Field/XFieldSpacetime.h"

#include "Field/tools/for_each_CellGroup.h"
#include "Field/Tuple/FieldTuple.h"

namespace SANS
{


//----------------------------------------------------------------------------//
// A class for computing the mass matrix on each cell element of a field
//----------------------------------------------------------------------------//

template<class PhysDim, class T>
class MassMatrix_Cell_impl : public GroupFunctorCellType<MassMatrix_Cell_impl<PhysDim, T>>
{
public:

  MassMatrix_Cell_impl( FieldDataInvMassMatrix_Cell& mmfld, const std::vector<int>& cellGroups ) :
    mmfld_(mmfld), cellGroups_(cellGroups)
  {
    // Nothing
  }

  std::size_t nCellGroups() const          { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n];     }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology >
  void
  apply( const typename FieldTuple<XField<PhysDim, typename Topology::TopoDim>, Field<PhysDim, typename Topology::TopoDim, T>, TupleClass<>>::
                        template FieldCellGroupType<Topology>& fldsCell,
         const int cellGroupGlobal )
  {
    typedef typename XField<PhysDim, typename Topology::TopoDim>  ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field<PhysDim, typename Topology::TopoDim, T>::template FieldCellGroupType<Topology> QFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

    const XFieldCellGroupType& xfldCell = get<0>(fldsCell);
    const QFieldCellGroupType& qfldCell = get<1>(fldsCell);

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );

    // cell group with inverse mass matrices
    DLA::MatrixDView_Array<Real>& mmfldCell = mmfld_.getCellGroupGlobal(cellGroupGlobal);

    // temporary matrix
    const int nBasis = qfldElem.basis()->nBasis();
    DLA::MatrixD<Real> mtx( nBasis, nBasis );

    ElementalMassMatrix<typename Topology::TopoDim, Topology> massMtx(xfldElem, qfldElem);

    // loop over elements within group
    const int nelem = xfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid DOFs to element
      xfldCell.getElement( xfldElem, elem );

      // compute the scalar elemental mass matrix
      massMtx(xfldElem, mtx);

      // Store the inverse mass matrix
      mmfldCell[elem] = DLA::InverseLU::Inverse(mtx);
    }
  }

protected:
  FieldDataInvMassMatrix_Cell& mmfld_;
  const std::vector<int> cellGroups_;
};

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim, class T>
FieldDataInvMassMatrix_Cell::
FieldDataInvMassMatrix_Cell( const Field<PhysDim, TopoDim, T>& qfld )
{
  globalCellGroups_ = qfld.getGlobalCellGroups();
  localCellGroups_ = qfld.getLocalCellGroups();

  //Count the total number of elements in the relevant cell groups
  int nData = 0;
  for (int i = 0; i < qfld.nCellGroups(); i++)
  {
    const int nCellElem = qfld.getCellGroupBase(i).nElem();
    const int nBasis = qfld.getCellGroupBase(i).nBasis();
    nData += nCellElem*nBasis*nBasis;
  }

  // Allocate the continuous memory for the array of mass matrices
  data_ = new Real[nData];

  int offset = 0;
  for (int i = 0; i < qfld.nCellGroups(); i++)
  {
    const int nCellElem = qfld.getCellGroupBase(i).nElem();
    const int nBasis = qfld.getCellGroupBase(i).nBasis();
    cellData_.push_back(DLA::MatrixDView_Array<Real>(data_ + offset, nBasis, nBasis, nCellElem));
    cellData_.back() = 0;
    offset += nCellElem*nBasis*nBasis;
  }


  // Initialize the mass matrix field
  for_each_CellGroup<TopoDim>::apply( MassMatrix_Cell_impl<PhysDim, T>(*this, globalCellGroups_), (qfld.getXField(), qfld));
}

// Explicit instantiations
template FieldDataInvMassMatrix_Cell::FieldDataInvMassMatrix_Cell(const Field<PhysD1, TopoD1, Real>&);
template FieldDataInvMassMatrix_Cell::FieldDataInvMassMatrix_Cell(const Field<PhysD2, TopoD1, Real>&);
template FieldDataInvMassMatrix_Cell::FieldDataInvMassMatrix_Cell(const Field<PhysD2, TopoD2, Real>&);
template FieldDataInvMassMatrix_Cell::FieldDataInvMassMatrix_Cell(const Field<PhysD3, TopoD2, Real>&);
template FieldDataInvMassMatrix_Cell::FieldDataInvMassMatrix_Cell(const Field<PhysD3, TopoD3, Real>&);
template FieldDataInvMassMatrix_Cell::FieldDataInvMassMatrix_Cell(const Field<PhysD4, TopoD4, Real>&);

//Explicit instantiation a series of fields with VectorS
template FieldDataInvMassMatrix_Cell::FieldDataInvMassMatrix_Cell(const Field<PhysD1, TopoD1, DLA::VectorS<1,Real>>&);
template FieldDataInvMassMatrix_Cell::FieldDataInvMassMatrix_Cell(const Field<PhysD1, TopoD1, DLA::VectorS<2,Real>>&);
template FieldDataInvMassMatrix_Cell::FieldDataInvMassMatrix_Cell(const Field<PhysD1, TopoD1, DLA::VectorS<3,Real>>&);
template FieldDataInvMassMatrix_Cell::FieldDataInvMassMatrix_Cell(const Field<PhysD1, TopoD1, DLA::VectorS<4,Real>>&);

template FieldDataInvMassMatrix_Cell::FieldDataInvMassMatrix_Cell(const Field<PhysD2, TopoD1, DLA::VectorS<2,Real>>&);
template FieldDataInvMassMatrix_Cell::FieldDataInvMassMatrix_Cell(const Field<PhysD2, TopoD1, DLA::VectorS<6,Real>>&);

template FieldDataInvMassMatrix_Cell::FieldDataInvMassMatrix_Cell(const Field<PhysD2, TopoD2, DLA::VectorS<2,Real>>&);
template FieldDataInvMassMatrix_Cell::FieldDataInvMassMatrix_Cell(const Field<PhysD2, TopoD2, DLA::VectorS<3,Real>>&);
template FieldDataInvMassMatrix_Cell::FieldDataInvMassMatrix_Cell(const Field<PhysD2, TopoD2, DLA::VectorS<4,Real>>&);
template FieldDataInvMassMatrix_Cell::FieldDataInvMassMatrix_Cell(const Field<PhysD2, TopoD2, DLA::VectorS<5,Real>>&);
template FieldDataInvMassMatrix_Cell::FieldDataInvMassMatrix_Cell(const Field<PhysD2, TopoD2, DLA::VectorS<6,Real>>&);

template FieldDataInvMassMatrix_Cell::FieldDataInvMassMatrix_Cell(const Field<PhysD3, TopoD3, DLA::VectorS<2,Real>>&);
template FieldDataInvMassMatrix_Cell::FieldDataInvMassMatrix_Cell(const Field<PhysD3, TopoD3, DLA::VectorS<3,Real>>&);
template FieldDataInvMassMatrix_Cell::FieldDataInvMassMatrix_Cell(const Field<PhysD3, TopoD3, DLA::VectorS<4,Real>>&);
template FieldDataInvMassMatrix_Cell::FieldDataInvMassMatrix_Cell(const Field<PhysD3, TopoD3, DLA::VectorS<5,Real>>&);
template FieldDataInvMassMatrix_Cell::FieldDataInvMassMatrix_Cell(const Field<PhysD3, TopoD3, DLA::VectorS<6,Real>>&);
template FieldDataInvMassMatrix_Cell::FieldDataInvMassMatrix_Cell(const Field<PhysD3, TopoD3, DLA::VectorS<7,Real>>&);

} // namespace SANS
