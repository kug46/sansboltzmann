// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDDATAMATRIXD_CELL_H_
#define FIELDDATAMATRIXD_CELL_H_

#include <vector>

#include "Field/Field.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Array.h"

namespace SANS
{

//-----------------------------------------------------------------------------
//
// Represents MatrixD data associated with cell element of a field
//
//-----------------------------------------------------------------------------

template<class T>
class FieldDataMatrixD_Cell
{
public:

  template<class PhysDim, class TopoDim>
  FieldDataMatrixD_Cell( const Field<PhysDim,TopoDim,T>& fld, const int m, const int n, const int ndata = 1 ) :
    globalCellGroups_(fld.getGlobalCellGroups()),
    localCellGroups_(fld.getLocalCellGroups())
  {
    //Count the total number of elements in the relevant cell groups
    const int nData = fld.nElem()*ndata;
    data_ = new T[nData*m*n];

    int offset = 0;
    for (int i = 0; i < fld.nCellGroups(); i++)
    {
      const int nCellElem = fld.getCellGroupBase(i).nElem();
      cellData_.push_back(DLA::MatrixDView_Array<T>(data_ + offset, m, n, nCellElem*ndata));
      offset += nCellElem*ndata*m*n;
    }
  }

  // expose as dummy
  FieldDataMatrixD_Cell() :
    data_(NULL)
  {}

  //Storage for jacobian matrices where residuals and unknowns have different basis functions
  //----------------------------------------------------------------------------//
  template<class PhysDim, class TopoDim, class T0, class T1>
  FieldDataMatrixD_Cell( const Field<PhysDim, TopoDim, T0>& fld0, const Field<PhysDim, TopoDim, T1>& fld1 ) :
    globalCellGroups_( fld0.getGlobalCellGroups() ),
    localCellGroups_( fld0.getLocalCellGroups() )
  {

    for (int i = 0; i < (int) globalCellGroups_.size(); i++)
      SANS_ASSERT( globalCellGroups_[i] == fld1.getGlobalCellGroups()[i] );

    for (int i = 0; i < (int) localCellGroups_.size(); i++)
      SANS_ASSERT( localCellGroups_[i] == fld1.getLocalCellGroups()[i] );

    SANS_ASSERT( fld0.nCellGroups() == fld1.nCellGroups() );

    //Count the total number of elements in the relevant cell groups
    int nData = 0;
    for (int i = 0; i < fld0.nCellGroups(); i++)
    {
      const int nCellElem = fld0.getCellGroupBase(i).nElem();
      SANS_ASSERT(nCellElem == fld1.getCellGroupBase(i).nElem());

      const int nBasis0 = fld0.getCellGroupBase(i).nBasis();
      const int nBasis1 = fld1.getCellGroupBase(i).nBasis();
      nData += nCellElem*nBasis0*nBasis1;
    }

    // Allocate the continuous memory for the array of mass matrices
    data_ = new T[nData];

    int offset = 0;
    for (int i = 0; i < fld0.nCellGroups(); i++)
    {
      const int nCellElem = fld0.getCellGroupBase(i).nElem();
      const int nBasis0 = fld0.getCellGroupBase(i).nBasis();
      const int nBasis1 = fld1.getCellGroupBase(i).nBasis();
      cellData_.push_back(DLA::MatrixDView_Array<T>(data_ + offset, nBasis0, nBasis1, nCellElem));
      cellData_.back() = 0;
      offset += nCellElem*nBasis0*nBasis1;
    }
  }


  FieldDataMatrixD_Cell( const FieldDataMatrixD_Cell& ) = delete;
  FieldDataMatrixD_Cell& operator=( const FieldDataMatrixD_Cell& ) = delete;

  ~FieldDataMatrixD_Cell()
  {
    delete [] data_;
  }

  DLA::MatrixDView_Array<T>& getCellGroupGlobal( const int groupGlobal )
  {
    int groupLocal = localCellGroups_.at(groupGlobal);
    SANS_ASSERT( groupLocal >= 0 );
    return cellData_.at(groupLocal);
  }

  const DLA::MatrixDView_Array<T>& getCellGroupGlobal( const int groupGlobal ) const
  {
    int groupLocal = localCellGroups_.at(groupGlobal);
    SANS_ASSERT( groupLocal >= 0 );
    return cellData_.at(groupLocal);
  }

  int nCellGroups() const { return (int)globalCellGroups_.size(); }

protected:
  // Protected constructor for derived classes
//  FieldDataMatrixD_Cell() : data_(NULL) {};

  std::vector<DLA::MatrixDView_Array<T>> cellData_;
  T *data_;

  // This map is for taking a local subset index to a global group index
  std::vector<int> globalCellGroups_;

  // Field groups can be constructed with a subset of the global XField groups.
  // These are maps for taking a global group index to a local subset index.
  std::vector<int> localCellGroups_;
};


}

#endif // FIELDDATAMATRIXD_CELL_H_
