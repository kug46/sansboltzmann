// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDSPACETIME_DG_TRACE_H
#define FIELDSPACETIME_DG_TRACE_H

#include "Field_DG_TraceBase.h"
#include "FieldSpacetime.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// solution field: DG trace (e.g. interface solution)
//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//
// 3D solution field: DG trace
//----------------------------------------------------------------------------//

template <class PhysDim, class T>
class Field_DG_Trace< PhysDim, TopoD4, T > : public Field_DG_TraceBase< PhysDim, TopoD4, T >
{
public:
  typedef Field_DG_TraceBase< PhysDim, TopoD4, T > BaseType;
  typedef T ArrayQ;

  Field_DG_Trace( const XField<PhysDim, TopoD4>& xfld, const int order, const BasisFunctionCategory& category );

  Field_DG_Trace( const XField<PhysDim, TopoD4>& xfld, const int order,
                  const BasisFunctionCategory& category, const std::vector<int>& InteriorGroups,
                  const std::vector<int>& BoundaryGroups );

  // Groups are first to remove ambiguity with list initializers
  // DG Fields are already broken, can just cat the CellGroupSets and forward
  explicit Field_DG_Trace( const std::vector<std::vector<int>>& InteriorGroupSets, const std::vector<std::vector<int>>& BoundaryGroupSets,
                           const XField<PhysDim, TopoD4>& xfld, const int order, const BasisFunctionCategory& category )
  : Field_DG_Trace( xfld, order, category, SANS::cat(InteriorGroupSets), SANS::cat(BoundaryGroupSets) ) {}


  Field_DG_Trace( const Field_DG_Trace& fld, const FieldCopy& tag );
  Field_DG_Trace& operator=( const ArrayQ& q );

protected:
  using BaseType::nDOF_;
  using BaseType::DOF_;
  using BaseType::interiorTraceGroups_;
  using BaseType::boundaryTraceGroups_;

  void init(const XField<PhysDim, TopoD4>& xfld, const int order,
            const BasisFunctionCategory& category, const std::vector<int>& InteriorGroups,
            const std::vector<int>& BoundaryGroups);
};

}

#endif  // FIELDSPACETIME_DG_TRACE_H
