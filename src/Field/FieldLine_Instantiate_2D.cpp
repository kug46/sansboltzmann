// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define FIELDBASE_INSTANTIATE
#include "FieldBase_impl.h"

#define FIELDSUBGROUP_INSTANTIATE
#include "FieldSubGroup_impl.h"

#define FIELD_INSTANTIATE
#include "Field_impl.h"

#include "Surreal/SurrealS.h"
#ifdef SANS_MPI
#include "MPI/serialize_SurrealS.h"
#endif

#include "XFieldLine.h"
#include "FieldLine.h"

namespace SANS
{
// FieldSubGroup instantiation
template class FieldSubGroup<PhysD2, FieldTraits<TopoD1, Real> >;
template class FieldSubGroup<PhysD2, FieldTraits<TopoD1, SurrealS<1>> >;

template class FieldSubGroup<PhysD2, FieldTraits<TopoD1, DLA::VectorS<1, Real>> >;
template class FieldSubGroup<PhysD2, FieldTraits<TopoD1, DLA::VectorS<2, Real>> >;
template class FieldSubGroup<PhysD2, FieldTraits<TopoD1, DLA::VectorS<3, Real>> >;
template class FieldSubGroup<PhysD2, FieldTraits<TopoD1, DLA::VectorS<4, Real>> >;
template class FieldSubGroup<PhysD2, FieldTraits<TopoD1, DLA::VectorS<5, Real>> >;
template class FieldSubGroup<PhysD2, FieldTraits<TopoD1, DLA::VectorS<6, Real>> >;
template class FieldSubGroup<PhysD2, FieldTraits<TopoD1, DLA::VectorS<8, Real>> >;
template class FieldSubGroup<PhysD2, FieldTraits<TopoD1, DLA::VectorS<22, Real>> >;

template class FieldSubGroup<PhysD2, FieldTraits<TopoD1, DLA::VectorS<1, DLA::VectorS<2, Real>>> >;
template class FieldSubGroup<PhysD2, FieldTraits<TopoD1, DLA::VectorS<1, DLA::VectorS<4, Real>>> >;
template class FieldSubGroup<PhysD2, FieldTraits<TopoD1, DLA::VectorS<1, DLA::VectorS<8, Real>>> >;


// Field instantiation
template class Field<PhysD2, TopoD1, Real>;
template class Field<PhysD2, TopoD1, SurrealS<1>>;

template class Field<PhysD2, TopoD1, DLA::VectorS<1, Real> >;
template class Field<PhysD2, TopoD1, DLA::VectorS<2, Real> >;
template class Field<PhysD2, TopoD1, DLA::VectorS<3, Real> >;
template class Field<PhysD2, TopoD1, DLA::VectorS<4, Real> >;
template class Field<PhysD2, TopoD1, DLA::VectorS<5, Real> >;
template class Field<PhysD2, TopoD1, DLA::VectorS<6, Real> >;
template class Field<PhysD2, TopoD1, DLA::VectorS<8, Real> >;
template class Field<PhysD2, TopoD1, DLA::VectorS<22, Real> >;

template class Field<PhysD2, TopoD1, DLA::VectorS<1, DLA::VectorS<2, Real> > >;
template class Field<PhysD2, TopoD1, DLA::VectorS<1, DLA::VectorS<4, Real> > >;

}
