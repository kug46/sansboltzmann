// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ELEMENTCONNECTIVITY_H
#define ELEMENTCONNECTIVITY_H

#include <iostream>
#include <string>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "Topology/Dimension.h"
#include "FieldAssociativity.h"
#include "ElementConnectivityConstructor.h"
#include "BasisFunction/TraceToCellRefCoord.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Element connectivity between the canonical traces of two elements
//
// implemented as abstract base class with templated derived classes
//----------------------------------------------------------------------------//

template< class T >
class ElementConnectivityBase : public FieldAssociativityBase< T, ElementConnectivityBase<T> >
{
public:
  typedef FieldAssociativityBase< T, ElementConnectivityBase<T> > BaseType;

protected:
  ElementConnectivityBase();     // abstract base class

public:

  virtual ~ElementConnectivityBase();

  // adjacent area-element group/element connectivity
  int getGroupLeft() const { return GroupL_; }
  int getElementLeft( const int elem ) const;
  CanonicalTraceToCell& getCanonicalTraceLeft( const int elem ) const;

  int getGroupRight() const  { return GroupR_; }
  int getElementRight( const int elem ) const;
  CanonicalTraceToCell& getCanonicalTraceRight( const int elem ) const;

  GroupElmentType getGroupRightType() const { return GroupRType_; }

protected:
  using BaseType::nElem_;

  // Indicates if the right element group is a cell group or a hub trace group
  GroupElmentType GroupRType_;

  // left adjacent element connectivity
  int GroupL_;
  int* ElemL_;                        // list of elements in groupL
  CanonicalTraceToCell* CanonicalL_;  // list of canonical element traces

  // right adjacent element connectivity
  int GroupR_;
  int* ElemR_;                        // list of elements in groupR
  CanonicalTraceToCell* CanonicalR_;  // list of canonical element traces

};


// default ctor needed for 'new []'
template <class T>
ElementConnectivityBase<T>::ElementConnectivityBase()
{
  // Hub traces are only used for very special cases, defaulting it CellGroup is pretty safe
  GroupRType_ = eCellGroup;

  GroupL_ = -1;
  GroupR_ = -1;
  ElemL_ = NULL;
  ElemR_ = NULL;
  CanonicalL_ = NULL;
  CanonicalR_ = NULL;
}

template <class T>
ElementConnectivityBase<T>::~ElementConnectivityBase()
{
  // adjacent area-element connectivity
  delete [] ElemL_;
  delete [] ElemR_;
  delete [] CanonicalL_;
  delete [] CanonicalR_;
}

template <class T>
int
ElementConnectivityBase<T>::getElementLeft( const int elem ) const
{
  SANS_ASSERT( (elem >= 0) && (elem < nElem_) );

  return ElemL_[elem];
}

template <class T>
CanonicalTraceToCell&
ElementConnectivityBase<T>::getCanonicalTraceLeft( const int elem ) const
{
  SANS_ASSERT( (elem >= 0) && (elem < nElem_) );

  return CanonicalL_[elem];
}


template <class T>
int
ElementConnectivityBase<T>::getElementRight( const int elem ) const
{
  SANS_ASSERT( ( ElemR_ != nullptr) && (elem >= 0) && (elem < nElem_) );

  return ElemR_[elem];
}

template <class T>
CanonicalTraceToCell&
ElementConnectivityBase<T>::getCanonicalTraceRight( const int elem ) const
{
  SANS_ASSERT( (CanonicalR_ != nullptr) && (elem >= 0) && (elem < nElem_) );

  return CanonicalR_[elem];
}



template <class FieldTraits>
class ElementConnectivity : public FieldAssociativity< FieldTraits >
{
public:
  typedef FieldAssociativity< FieldTraits > BaseType;
  typedef typename BaseType::FieldAssociativityConstructorType FieldAssociativityConstructorType;
  typedef typename FieldTraits::FieldBase FieldBase;

  ElementConnectivity() : BaseType() {}   // needed for new []
  ElementConnectivity( const ElementConnectivity& cnct ) : BaseType(cnct) { operator=(cnct); }
  explicit ElementConnectivity( const FieldAssociativityConstructorType& assoc );
  virtual ~ElementConnectivity() {};

  ElementConnectivity& operator=( const ElementConnectivity& );

  virtual FieldBase* clone() const override { return new ElementConnectivity(*this); }

  // adjacent area-element group/element connectivity
  using BaseType::getGroupLeft;
  using BaseType::getElementLeft;
  using BaseType::getCanonicalTraceLeft;

  using BaseType::getGroupRight;
  using BaseType::getElementRight;
  using BaseType::getCanonicalTraceRight;

  using BaseType::getGroupRightType;

  virtual void dump( int indentSize=0, std::ostream& out = std::cout ) const override;

private:
  using BaseType::nElem_;

  // Indicates if the right element group is a cell group or a hub trace group
  using BaseType::GroupRType_;

  // left adjacent element connectivity
  using BaseType::GroupL_;
  using BaseType::ElemL_;             // list of elements in groupL
  using BaseType::CanonicalL_;        // list of canonical element traces

  // right adjacent element connectivity
  using BaseType::GroupR_;
  using BaseType::ElemR_;             // list of elements in groupR
  using BaseType::CanonicalR_;        // list of canonical element traces
};

template <class FieldTraits>
ElementConnectivity<FieldTraits>::ElementConnectivity( const FieldAssociativityConstructorType& assoc )
  : BaseType(assoc)
{
  SANS_ASSERT( nElem_ >=0 );

  // adjacent group is hub trace or cell
  GroupRType_ = assoc.getGroupRightType();

  // adjacent cell-element connectivity (might not be set for a hub trace)
  if ( assoc.isGroupLeftSet() )
    GroupL_ = assoc.getGroupLeft();

  // Group right is not always set if this field is on a domain boundary (or a hub trace)
  if ( assoc.isGroupRightSet() )
    GroupR_ = assoc.getGroupRight();

  // Empty group (needed on parallel partitions)
  if ( nElem_ == 0 ) return;

  ElemL_ = new int[nElem_];
  ElemR_ = new int[nElem_];
  CanonicalL_ = new CanonicalTraceToCell[nElem_];
  CanonicalR_ = new CanonicalTraceToCell[nElem_];

  for (int n = 0; n < nElem_; n++)
  {
    if ( assoc.isGroupLeftSet() )
    {
      ElemL_[n] = assoc.getElementLeft(n);
      CanonicalL_[n] = assoc.getCanonicalTraceLeft(n);
    }
    else
    {
      ElemL_[n] = -1;
    }

    if ( assoc.isGroupRightSet() )
    {
      ElemR_[n] = assoc.getElementRight(n);
      if ( GroupRType_ == eCellGroup )
        CanonicalR_[n] = assoc.getCanonicalTraceRight(n);
    }
    else
    {
      ElemR_[n] = -1;
    }
  }
}


template <class FieldTraits>
ElementConnectivity<FieldTraits>&
ElementConnectivity<FieldTraits>::operator=( const ElementConnectivity& a )
{
  if (this != &a)
  {
    BaseType::operator=(a);

    // adjacent cell-element connectivity

    delete [] ElemL_;
    delete [] ElemR_;
    delete [] CanonicalL_;
    delete [] CanonicalR_;

    GroupRType_ = a.GroupRType_;

    GroupL_ = a.GroupL_;
    ElemL_ = new int[nElem_];
    CanonicalL_ = new CanonicalTraceToCell[nElem_];

    GroupR_ = a.GroupR_;
    ElemR_ = new int[nElem_];
    CanonicalR_ = new CanonicalTraceToCell[nElem_];

    for (int n = 0; n < nElem_; n++)
    {
      ElemL_[n] = a.ElemL_[n];
      ElemR_[n] = a.ElemR_[n];
      CanonicalL_[n] = a.CanonicalL_[n];
      CanonicalR_[n] = a.CanonicalR_[n];
    }
  }

  return *this;
}


template <class FieldTraits>
void
ElementConnectivity<FieldTraits>::dump( int indentSize, std::ostream& out ) const
{
  BaseType::dump(indentSize, out);

  int n;
  std::string indent(indentSize, ' ');
  out << indent << "ElementConnectivity:";
  out << std::endl;

  out << indent << "GroupRType_ = " << GroupRType_ << std::endl;

  out << indent << "  L: areaGroupL_ = " << GroupL_ << "  areaElemL_[] =";
  for (n = 0; n < nElem_; n++)
    out << " " << ElemL_[n];
  out << "  areaCanonicalL_[] =";
  for (n = 0; n < nElem_; n++)
    out << " " << CanonicalL_[n];
  out << std::endl;

  out << indent << "  R: areaGroupR_ = " << GroupR_ << "  areaElemR_[] =";
  for (n = 0; n < nElem_; n++)
    out << " " << ElemR_[n];
  out << "  areaCanonicalR_[] =";
  for (n = 0; n < nElem_; n++)
    out << " " << CanonicalR_[n];
  out << std::endl;
}

} //namespace SANS

#endif  // ELEMENTCONNECTIVITY_H
