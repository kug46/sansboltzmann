// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELDLINE_BOUNDARYTRACE_H
#define XFIELDLINE_BOUNDARYTRACE_H

#include "XField_BoundaryTrace.h"
#include "XFieldLine.h"
#include "FieldAssociativity.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Topologically 1D grid
//----------------------------------------------------------------------------//

template<class PhysDim>
struct XFieldTraits_BoundaryTrace<PhysDim, TopoD1>
{
  typedef TopoD1 TopoDim;
  static const int D = PhysDim::D;                 // physical dimensions

  typedef DLA::VectorS<D,Real> VectorX;   // coordinates vector
  typedef VectorX T;

  typedef typename XFieldGroupNodeTraits<PhysDim, TopoD1>::FieldBase FieldTraceGroupBase;
  typedef typename XFieldGroupLineTraits<PhysDim, TopoD1>::FieldBase FieldCellGroupBase;

  template<class Topology>
  using FieldTraceGroupType = FieldAssociativity< XFieldGroupNodeTraits<PhysDim, TopoD1> >;

  template<class Topology>
  using FieldCellGroupType = FieldAssociativity< XFieldGroupLineTraits<PhysDim, TopoD1> >;
};

} // namespace SANS

#endif //XFIELDLINE_BOUNDARYTRACE_H
