// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDLIFTGROUPLINE_TRAITS_H
#define FIELDLIFTGROUPLINE_TRAITS_H

#include "BasisFunction/BasisFunctionLine.h"

#include "Field/Element/Element.h"
#include "Field/Element/ElementAssociativityLine.h"

#include "FieldLiftAssociativity.h"

#include "tools/Surrealize.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Traits for a field of lifting operators on lines
//----------------------------------------------------------------------------//

template<class Tin>
struct FieldLiftGroupLineTraits
{
  typedef FieldLiftAssociativityBase<Tin> FieldBase;

  typedef Tin T;                                    // DOF type
  typedef BasisFunctionLineBase BasisType;
  typedef Line TopologyType;
  typedef ElementAssociativity<TopoD1,Line> ElementAssociativityType;
  typedef FieldLiftAssociativityConstructor<ElementAssociativityType::Constructor> FieldLiftAssociativityConstructorType;

#if 0
  template<class ElemT>
  struct ElementType : public Element< typename Surrealize<ElemT,Tin>::T, TopoD1, Line >
  {
    typedef Element< typename Surrealize<ElemT,Tin>::T, TopoD1, Line > BaseType;
    ElementType() : BaseType() {}
    explicit ElementType( const BasisType* basis ) : BaseType(basis) {}
    ElementType( int order, const BasisFunctionCategory& category ) : BaseType(order,category) {}
    ElementType& operator=( const ElementType& elem ) { BaseType::operator=(elem); return *this; }
  };
#else
  template<class ElemT>
  using ElementType = Element< typename Surrealize<ElemT,Tin>::T, TopoD1, Line >;
#endif
};

}

#endif //FIELDLIFTGROUPLINE_TRAITS_H
