// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define XFIELD_CELLTOTRACE_INSTANTIATE
#include "XField_CellToTrace_impl.h"

#include "Field/XFieldSpacetime.h"

namespace SANS
{
//Explicitly instantiate
template class XField_CellToTrace_Base<PhysD4,TopoD4>;

template class XField_CellToTrace<PhysD4,TopoD4>;

}
