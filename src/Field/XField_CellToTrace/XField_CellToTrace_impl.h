// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(XFIELD_CELLTOTRACE_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "../XField_CellToTrace.h"

#include "Field/XField.h"
#include "Field/GroupElementType.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
template <class TopologyTrace>
void
XField_CellToTrace_Base<PhysDim, TopoDim>::loopInteriorTraces(int group)
{
  const typename XField<PhysDim,TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace =
      xfld_.template getInteriorTraceGroup<TopologyTrace>(group);

  const int nTrace = xfldTrace.nElem();
  const int cellGroupL = xfldTrace.getGroupLeft();
  const int cellGroupR = xfldTrace.getGroupRight();

  for (int j = 0; j < nTrace; j++) //loop over elements in InteriorTraceGroup
  {
    int cellElemL = xfldTrace.getElementLeft(j);
    int cellElemR = xfldTrace.getElementRight(j);

    int canonicalTraceL = xfldTrace.getCanonicalTraceLeft(j).trace;
    int canonicalTraceR = xfldTrace.getCanonicalTraceRight(j).trace;

    CelltoTraceAssociativity_[cellGroupL]->setTrace(cellElemL, canonicalTraceL, group, j, TraceInfo::Interior);
    CelltoTraceAssociativity_[cellGroupR]->setTrace(cellElemR, canonicalTraceR, group, j, TraceInfo::Interior);
  }
}

//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
template <class TopologyTrace>
void
XField_CellToTrace_Base<PhysDim, TopoDim>::loopBoundaryTraces(int group)
{
  const typename XField<PhysDim,TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace =
      xfld_.template getBoundaryTraceGroup<TopologyTrace>(group);

  const int nTrace = xfldTrace.nElem();
  const int cellGroupL = xfldTrace.getGroupLeft();
  const int cellGroupR = xfldTrace.getGroupRight();

  GroupElmentType GroupRightType = xfldTrace.getGroupRightType();

  for (int j = 0; j < nTrace; j++) //loop over traces in BoundaryTraceGroup
  {
    int cellElemL = xfldTrace.getElementLeft(j);

    int canonicalTraceL = xfldTrace.getCanonicalTraceLeft(j).trace;

    CelltoTraceAssociativity_[cellGroupL]->setTrace(cellElemL, canonicalTraceL, group, j, TraceInfo::Boundary);

    if (cellGroupR >= 0 && GroupRightType == eCellGroup)
    {
      int cellElemR = xfldTrace.getElementRight(j);
      int canonicalTraceR = xfldTrace.getCanonicalTraceRight(j).trace;
      CelltoTraceAssociativity_[cellGroupR]->setTrace(cellElemR, canonicalTraceR, group, j, TraceInfo::Boundary);
    }
  }
}

//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
template <class TopologyTrace>
void
XField_CellToTrace_Base<PhysDim, TopoDim>::loopGhostBoundaryTraces(int group)
{
  const typename XField<PhysDim,TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace =
      xfld_.template getGhostBoundaryTraceGroup<TopologyTrace>(group);

  const int nTrace = xfldTrace.nElem();
  const int cellGroupL = xfldTrace.getGroupLeft();
  const int cellGroupR = xfldTrace.getGroupRight();

  GroupElmentType GroupRightType = xfldTrace.getGroupRightType();

  for (int j = 0; j < nTrace; j++) //loop over traces in BoundaryTraceGroup
  {
    int cellElemL = xfldTrace.getElementLeft(j);

    int canonicalTraceL = xfldTrace.getCanonicalTraceLeft(j).trace;

    CelltoTraceAssociativity_[cellGroupL]->setTrace(cellElemL, canonicalTraceL, group, j, TraceInfo::GhostBoundary);

    if (cellGroupR >= 0 && GroupRightType == eCellGroup)
    {
      int cellElemR = xfldTrace.getElementRight(j);
      int canonicalTraceR = xfldTrace.getCanonicalTraceRight(j).trace;
      CelltoTraceAssociativity_[cellGroupR]->setTrace(cellElemR, canonicalTraceR, group, j, TraceInfo::GhostBoundary);
    }
  }
}

//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
const TraceInfo&
XField_CellToTrace_Base<PhysDim, TopoDim>::getTrace(const int cellGroup, const int cellElem, const int canonicalTrace) const
{
  SANS_ASSERT_MSG( !CelltoTraceAssociativity_.empty(),"Please initialize connectivity structure by passing in an XField");
  SANS_ASSERT_MSG( cellGroup >= 0 && cellGroup < nCellGroups_,
                   "cellGroup=%d, nCellGroups_=%d",cellGroup,nCellGroups_);
  SANS_ASSERT_MSG( cellElem >= 0 && cellElem < (int) CelltoTraceAssociativity_[cellGroup]->nElem(),
                   "cellElem=%d, CelltoTraceAssociativity_[cellGroup].size()=%d ", cellElem, CelltoTraceAssociativity_[cellGroup]->nElem());

  return CelltoTraceAssociativity_[cellGroup]->getTrace(cellElem, canonicalTrace);
}

//=============================================================================
template <class PhysDim>
XField_CellToTrace<PhysDim, TopoD1>::XField_CellToTrace(const XField<PhysDim, TopoD1>& xfld): XField_CellToTrace_Base<PhysDim, TopoD1>(xfld)
{

  CelltoTraceAssociativity_.clear();
  nCellGroups_ = xfld_.nCellGroups();

  if ( nCellGroups_ == 0 ) return; //No elements

  //Create associativity structure
  for (int i = 0; i < nCellGroups_; i++ )
  {
    int nElem = xfld_.getCellGroupBase(i).nElem();

    if (xfld_.getCellGroupBase(i).topoTypeID() == typeid(Line))
      CelltoTraceAssociativity_.push_back(std::make_shared<CellToTraceAssociativity<Line>>(nElem));
    else
      SANS_DEVELOPER_EXCEPTION("Unknown topology type for CellGroup.");
  }

  //Loop through interior traces and populate connectivity table
  for (int i = 0; i < xfld_.nInteriorTraceGroups(); i++ )
  {
    if (xfld_.getInteriorTraceGroupBase(i).topoTypeID() == typeid(Node))
    {
      BaseType::template loopInteriorTraces<Node>(i);
    }
    else
      SANS_DEVELOPER_EXCEPTION("Unknown topology type for InteriorTraceGroup.");
  }

  //Loop through boundary traces and populate connectivity table
  for (int i = 0; i < xfld_.nBoundaryTraceGroups(); i++ )
  {
    if (xfld_.getBoundaryTraceGroupBase(i).topoTypeID() == typeid(Node))
    {
      BaseType::template loopBoundaryTraces<Node>(i);
    }
    else
      SANS_DEVELOPER_EXCEPTION("Unknown topology type for BoundaryTraceGroup.");
  }

  //Loop through ghost boundary traces and populate connectivity table
  for (int i = 0; i < xfld_.nGhostBoundaryTraceGroups(); i++ )
  {
    if (xfld_.getGhostBoundaryTraceGroupBase(i).topoTypeID() == typeid(Node))
    {
      BaseType::template loopGhostBoundaryTraces<Node>(i);
    }
    else
      SANS_DEVELOPER_EXCEPTION("Unknown topology type for GhostBoundaryTraceGroup.");
  }
}


template <class PhysDim>
XField_CellToTrace<PhysDim, TopoD2>::XField_CellToTrace(const XField<PhysDim, TopoD2>& xfld): XField_CellToTrace_Base<PhysDim, TopoD2>(xfld)
{

  CelltoTraceAssociativity_.clear();
  nCellGroups_ = xfld_.nCellGroups();

  if ( nCellGroups_ == 0 ) return; //No elements

  //Create associativity structure
  for (int i = 0; i < nCellGroups_; i++ )
  {
    int nElem = xfld_.getCellGroupBase(i).nElem();

    if (xfld_.getCellGroupBase(i).topoTypeID() == typeid(Triangle))
      CelltoTraceAssociativity_.push_back(std::make_shared<CellToTraceAssociativity<Triangle>>(nElem));

    else if (xfld_.getCellGroupBase(i).topoTypeID() == typeid(Quad))
      CelltoTraceAssociativity_.push_back(std::make_shared<CellToTraceAssociativity<Quad>>(nElem));

    else
      SANS_DEVELOPER_EXCEPTION("Unknown topology type for CellGroup.");
  }

  //Loop through interior traces and populate connectivity table
  for (int i = 0; i < xfld_.nInteriorTraceGroups(); i++ )
  {
    if (xfld_.getInteriorTraceGroupBase(i).topoTypeID() == typeid(Line))
    {
      BaseType::template loopInteriorTraces<Line>(i);
    }
    else
      SANS_DEVELOPER_EXCEPTION("Unknown topology type for InteriorTraceGroup.");
  }

  //Loop through boundary traces and populate connectivity table
  for (int i = 0; i < xfld_.nBoundaryTraceGroups(); i++ )
  {
    if (xfld_.getBoundaryTraceGroupBase(i).topoTypeID() == typeid(Line))
    {
      BaseType::template loopBoundaryTraces<Line>(i);
    }
    else
      SANS_DEVELOPER_EXCEPTION("Unknown topology type for BoundaryTraceGroup.");
  }

  //Loop through ghost boundary traces and populate connectivity table
  for (int i = 0; i < xfld_.nGhostBoundaryTraceGroups(); i++ )
  {
    if (xfld_.getGhostBoundaryTraceGroupBase(i).topoTypeID() == typeid(Line))
    {
      BaseType::template loopGhostBoundaryTraces<Line>(i);
    }
    else
      SANS_DEVELOPER_EXCEPTION("Unknown topology type for GhostBoundaryTraceGroup.");
  }
}


template <class PhysDim>
XField_CellToTrace<PhysDim, TopoD3>::XField_CellToTrace(const XField<PhysDim, TopoD3>& xfld): XField_CellToTrace_Base<PhysDim, TopoD3>(xfld)
{

  CelltoTraceAssociativity_.clear();

  nCellGroups_ = xfld_.nCellGroups();

  if ( nCellGroups_ == 0 ) return; //No elements

  //Create associativity structure
  for (int i = 0; i < nCellGroups_; i++ )
  {
    int nElem = xfld_.getCellGroupBase(i).nElem();

    if (xfld_.getCellGroupBase(i).topoTypeID() == typeid(Tet))
      CelltoTraceAssociativity_.push_back(std::make_shared<CellToTraceAssociativity<Tet>>(nElem));

    else if (xfld_.getCellGroupBase(i).topoTypeID() == typeid(Hex))
      CelltoTraceAssociativity_.push_back(std::make_shared<CellToTraceAssociativity<Hex>>(nElem));
    else
      SANS_DEVELOPER_EXCEPTION("Unknown topology type for CellGroup.");
  }

  //Loop through interior traces and populate connectivity table
  for (int i = 0; i < xfld_.nInteriorTraceGroups(); i++ )
  {
    if (xfld_.getInteriorTraceGroupBase(i).topoTypeID() == typeid(Triangle))
    {
      BaseType::template loopInteriorTraces<Triangle>(i);
    }
    else if (xfld_.getInteriorTraceGroupBase(i).topoTypeID() == typeid(Quad))
    {
      BaseType::template loopInteriorTraces<Quad>(i);
    }
    else
      SANS_DEVELOPER_EXCEPTION("Unknown topology type for InteriorTraceGroup.");
  }

  //Loop through boundary traces and populate connectivity table
  for (int i = 0; i < xfld_.nBoundaryTraceGroups(); i++ )
  {
    if (xfld_.getBoundaryTraceGroupBase(i).topoTypeID() == typeid(Triangle))
    {
      BaseType::template loopBoundaryTraces<Triangle>(i);
    }
    else if (xfld_.getBoundaryTraceGroupBase(i).topoTypeID() == typeid(Quad))
    {
      BaseType::template loopBoundaryTraces<Quad>(i);
    }
    else
      SANS_DEVELOPER_EXCEPTION("Unknown topology type for BoundaryTraceGroup.");
  }

  //Loop through ghost boundary traces and populate connectivity table
  for (int i = 0; i < xfld_.nGhostBoundaryTraceGroups(); i++ )
  {
    if (xfld_.getGhostBoundaryTraceGroupBase(i).topoTypeID() == typeid(Triangle))
    {
      BaseType::template loopGhostBoundaryTraces<Triangle>(i);
    }
    else if (xfld_.getGhostBoundaryTraceGroupBase(i).topoTypeID() == typeid(Quad))
    {
      BaseType::template loopGhostBoundaryTraces<Quad>(i);
    }
    else
      SANS_DEVELOPER_EXCEPTION("Unknown topology type for GhostBoundaryTraceGroup.");
  }
}

template <class PhysDim>
XField_CellToTrace<PhysDim, TopoD4>::XField_CellToTrace(const XField<PhysDim, TopoD4>& xfld): XField_CellToTrace_Base<PhysDim, TopoD4>(xfld)
{

  CelltoTraceAssociativity_.clear();

  nCellGroups_ = xfld_.nCellGroups();

  if ( nCellGroups_ == 0 ) return; //No elements

  //Create associativity structure
  for (int i = 0; i < nCellGroups_; i++ )
  {
    int nElem = xfld_.getCellGroupBase(i).nElem();

    if (xfld_.getCellGroupBase(i).topoTypeID() == typeid(Pentatope))
      CelltoTraceAssociativity_.push_back(std::make_shared<CellToTraceAssociativity<Pentatope>>(nElem));
    else
      SANS_DEVELOPER_EXCEPTION("Unknown topology type for CellGroup.");
  }

  //Loop through interior traces and populate connectivity table
  for (int i = 0; i < xfld_.nInteriorTraceGroups(); i++ )
  {
    if (xfld_.getInteriorTraceGroupBase(i).topoTypeID() == typeid(Tet))
    {
      BaseType::template loopInteriorTraces<Tet>(i);
    }
    else
      SANS_DEVELOPER_EXCEPTION("Unknown topology type for InteriorTraceGroup.");
  }

  //Loop through boundary traces and populate connectivity table
  for (int i = 0; i < xfld_.nBoundaryTraceGroups(); i++ )
  {
    if (xfld_.getBoundaryTraceGroupBase(i).topoTypeID() == typeid(Tet))
    {
      BaseType::template loopBoundaryTraces<Tet>(i);
    }
    else
      SANS_DEVELOPER_EXCEPTION("Unknown topology type for BoundaryTraceGroup.");
  }

  //Loop through ghost boundary traces and populate connectivity table
  for (int i = 0; i < xfld_.nGhostBoundaryTraceGroups(); i++ )
  {
    if (xfld_.getGhostBoundaryTraceGroupBase(i).topoTypeID() == typeid(Tet))
    {
      BaseType::template loopGhostBoundaryTraces<Tet>(i);
    }
    else
      SANS_DEVELOPER_EXCEPTION("Unknown topology type for GhostBoundaryTraceGroup.");
  }
}

}
