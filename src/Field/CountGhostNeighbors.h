// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef COUNTGHOSTNEIGHBORS_H_
#define COUNTGHOSTNEIGHBORS_H_

#include "Field/tools/GroupFunctorType.h"
#include "Field/XField.h"

#include <vector>

namespace SANS
{

//----------------------------------------------------------------------------//
template<class PhysDim>
class CountGhostNeighborsInterior :
    public GroupFunctorInteriorTraceType<CountGhostNeighborsInterior<PhysDim>>
{
public:
  explicit CountGhostNeighborsInterior( const std::vector<int>& traceGroups, const int comm_rank,
                                        const std::vector<int>& CellGroups, std::vector<std::vector<int>>& nGhostNeigbor ) :
    traceGroups_(traceGroups), comm_rank_(comm_rank),
    CellGroups_(CellGroups), nGhostNeigbor_(nGhostNeigbor) {}

  std::size_t nInteriorTraceGroups() const { return traceGroups_.size(); }
  std::size_t interiorTraceGroup(const int n) const { return traceGroups_[n]; }

  std::size_t nPeriodicTraceGroups() const { return traceGroups_.size(); }
  std::size_t periodicTraceGroup(const int n) const { return traceGroups_[n]; }

  //----------------------------------------------------------------------------//
  // Distribution function that redistributes the error in each cell group
  template< class TopologyTrace, class TopologyL, class TopologyR>
  void
  apply( const typename XField<PhysDim, typename TopologyL::TopoDim>::template FieldCellGroupType<TopologyL>& xfldCellL, const int groupL,
         const typename XField<PhysDim, typename TopologyR::TopoDim>::template FieldCellGroupType<TopologyR>& xfldCellR, const int groupR,
         const typename XField<PhysDim, typename TopologyTrace::CellTopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
         const int traceGroupGlobal)
  {
    std::size_t igroupL = 0;
    std::size_t igroupR = 0;

    while (igroupL < CellGroups_.size() && groupL != CellGroups_[igroupL]) igroupL++;
    while (igroupR < CellGroups_.size() && groupR != CellGroups_[igroupR]) igroupR++;

    if (igroupL == CellGroups_.size()) return;
    if (igroupR == CellGroups_.size()) return;

    // loop over elements
    for (int elem = 0; elem < xfldTrace.nElem(); elem++)
    {
      int elemL = xfldTrace.getElementLeft( elem );
      int elemR = xfldTrace.getElementRight( elem );

      int rankL = xfldCellL.associativity( elemL ).rank();
      int rankR = xfldCellR.associativity( elemR ).rank();

      // Increment the count if both are ghost elements. Possessed elements can't be zombies
      if (rankL != comm_rank_ && rankR != comm_rank_)
      {
        nGhostNeigbor_[igroupR][elemR]++;
        nGhostNeigbor_[igroupL][elemL]++;
      }
    }
  }
protected:
  const std::vector<int> traceGroups_;
  const int comm_rank_;
  const std::vector<int>& CellGroups_;
  std::vector<std::vector<int>>& nGhostNeigbor_;
};


//----------------------------------------------------------------------------//
template<class PhysDim>
class CountGhostNeighborsBoundary:
    public GroupFunctorBoundaryCellType<CountGhostNeighborsBoundary<PhysDim>>
{
public:
  explicit CountGhostNeighborsBoundary( const std::vector<int>& boundaryTraceGroups, const int comm_rank,
                                        const std::vector<int>& CellGroups, std::vector<std::vector<int>>& nGhostNeigbor ) :
    boundaryTraceGroups_(boundaryTraceGroups), comm_rank_(comm_rank),
    CellGroups_(CellGroups), nGhostNeigbor_(nGhostNeigbor) {}

  std::size_t nBoundaryGroups() const { return boundaryTraceGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return boundaryTraceGroups_[n]; }

  //----------------------------------------------------------------------------//
  // Distribution function that redistributes the error in each cell group
  template< class TopologyTrace, class TopologyL>
  void
  apply( const typename XField<PhysDim, typename TopologyL::TopoDim>::template FieldCellGroupType<TopologyL>& xfldCellL, const int groupL,
         const typename XField<PhysDim, typename TopologyL::TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
         const int traceGroupGlobal)
  {
    std::size_t igroupL = 0;

    while (igroupL < CellGroups_.size() && groupL != CellGroups_[igroupL]) igroupL++;

    if (igroupL == CellGroups_.size()) return;

    // loop over elements
    for (int elem = 0; elem < xfldTrace.nElem();elem++)
    {
      int elemL = xfldTrace.getElementLeft( elem );
      int rankL = xfldCellL.associativity( elemL ).rank();

      // Increment the count of the neighbor if its not possessed
      // (a boundary always counts as a ghost neighbor)
      if (rankL != comm_rank_)
       nGhostNeigbor_[igroupL][elemL]++;
    }
  }
protected:
  const std::vector<int>& boundaryTraceGroups_;
  const int comm_rank_;
  const std::vector<int>& CellGroups_;
  std::vector<std::vector<int>>& nGhostNeigbor_;
};

} // namespace SANS

#endif //COUNTGHOSTNEIGHBORS_H_
