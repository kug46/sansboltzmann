// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDGROUPNODE_TRAITS_H
#define FIELDGROUPNODE_TRAITS_H

#include "FieldTypes.h"
#include "FieldAssociativity.h"
#include "Field/Element/Element.h"
#include "Field/Element/ElementAssociativityNode.h"

#include "tools/Surrealize.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Traits for a field group of nodes
//----------------------------------------------------------------------------//

template<class Tin>
struct FieldGroupNodeTraits
{
  typedef FieldAssociativityBase<Tin> FieldBase;

  typedef Tin T;                                    // DOF type
  typedef BasisFunctionNodeBase BasisType;
  typedef Node TopologyType;
  typedef ElementAssociativity<TopoD0,Node> ElementAssociativityType;
  typedef FieldAssociativityConstructor<ElementAssociativityType::Constructor> FieldAssociativityConstructorType;

#if 0
  template<class ElemT = Real>
  struct ElementType : public Element< typename Surrealize<ElemT,Tin>::T, TopoD0, Node >
  {
    typedef Element< typename Surrealize<ElemT,Tin>::T, TopoD0, Node > BaseType;
    explicit ElementType( const BasisType* basis ) : BaseType(basis) {}
    ElementType( int order, const BasisFunctionCategory& category ) : BaseType(order,category) {}
    ElementType& operator=( const ElementType& elem ) { BaseType::operator=(elem); return *this; }
  };
#else
  template<class ElemT = Real>
  using ElementType = Element< typename Surrealize<ElemT,Tin>::T, TopoD0, Node >;
#endif
};

} //namespace SANS

#endif //FIELDGROUPNODE_TRAITS_H
