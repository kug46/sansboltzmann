// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef OUTPUT_GRM_H
#define OUTPUT_GRM_H

#include <string>

#include "XField.h"

namespace SANS
{

template<class PhysDim, class TopoDim>
void
output_grm( const XField<PhysDim,TopoDim>& xgrid, const std::string& filename );

template<class PhysDim, class TopoDim>
void
output_grm( const XField<PhysDim,TopoDim>& xgrid, const int q, const std::string& filename );


} //namespace SANS

#endif  // OUTPUT_GRM_H
