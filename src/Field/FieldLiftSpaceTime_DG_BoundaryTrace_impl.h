// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(FIELDLIFTSPACETIME_DG_BOUNDARYTRACE_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "FieldLiftSpaceTime_DG_BoundaryTrace.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template <class PhysDim, class T>
FieldLift_DG_BoundaryTrace<PhysDim, TopoD4, T>::FieldLift_DG_BoundaryTrace( const XField<PhysDim, TopoD4>& xfld,
                                                                            const int order, const BasisFunctionCategory& category )
  : BaseType( xfld )
{
  init(xfld,order,category,BaseType::createBoundaryGroupIndex());
}

template <class PhysDim, class T>
FieldLift_DG_BoundaryTrace<PhysDim, TopoD4, T>::FieldLift_DG_BoundaryTrace( const XField<PhysDim, TopoD4>& xfld,
                                                                            const int order,
                                                                            const BasisFunctionCategory& category,
                                                                            const std::vector<int>& BoundaryGroups )
  : BaseType( xfld )
{
  BaseType::checkBoundaryGroupIndex(BoundaryGroups);
  init(xfld,order,category,BoundaryGroups);
}

template <class PhysDim, class T>
FieldLift_DG_BoundaryTrace<PhysDim, TopoD4, T>::FieldLift_DG_BoundaryTrace( const FieldLift_DG_BoundaryTrace& fld, const FieldCopy& tag )
  : BaseType(fld, tag) {}

template <class PhysDim, class T>
FieldLift_DG_BoundaryTrace<PhysDim, TopoD4, T>&
FieldLift_DG_BoundaryTrace<PhysDim, TopoD4, T>::operator=( const ArrayQ& q )
{
  Field< PhysDim, TopoD4, T >::operator=(q);
  return *this;
}

//----------------------------------------------------------------------------//
template <class PhysDim, class T>
void
FieldLift_DG_BoundaryTrace<PhysDim, TopoD4, T>::init(
    const XField<PhysDim, TopoD4>& xfld, const int order,
    const BasisFunctionCategory& category, const std::vector<int>& BoundaryGroups )
{
  // copy over the boundary group maps to cell maps, and clear the boundary maps
  boundaryTraceGroups_.resize(0);
  this->localCellGroups_ = this->localBoundaryTraceGroups_;
  this->globalCellGroups_ = this->globalBoundaryTraceGroups_;
  this->localBoundaryTraceGroups_.clear();
  this->globalBoundaryTraceGroups_.clear();
  this->localBoundaryTraceGroups_.shrink_to_fit();
  this->globalBoundaryTraceGroups_.shrink_to_fit();

  cellGroups_.resize(BoundaryGroups.size());

  for (std::size_t igroup = 0; igroup < BoundaryGroups.size(); igroup++)
  {
    const int bndGroup = BoundaryGroups[igroup];

    const int cellGroup = xfld.getBoundaryTraceGroupBase(bndGroup).getGroupLeft();

    if ( xfld.getBoundaryTraceGroupBase(bndGroup).topoTypeID() == typeid(Tet) )
    {
      if ( xfld.getCellGroupBase(cellGroup).topoTypeID() == typeid(Pentatope) )
        this->template createBoundaryTraceCellGroup<Pentatope, Tet>(bndGroup, order, category);
      else
        SANS_DEVELOPER_EXCEPTION( "Unknown cell element topology" );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "Unknown trace element topology" );
  }

  // allocate the solution DOF array and assign it to groups
  this->createDOFs();
}

}
