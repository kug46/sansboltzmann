// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "output_fluent.h"

#include "tools/SANSnumerics.h"     // Real

#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"

#include "XFieldLine.h"
#include "XFieldArea.h"
#include "XFieldVolume.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// output as P1
//
template<class PhysDim, class TopoDim, class Topology>
void
output_fluent_CellGroup( const std::string& shape, const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfld, FILE* fp )
{
//  int Q = 1; // Only outputting linear elements for now...
  int nelem = xfld.nElem();
  int type;

  if (shape=="PXE_Shape_Triangle")
    type = 1;
  else if (shape == "PXE_Shape_Quad")
    type = 3;

  fprintf( fp, "( 12 (1 1 %x 1 %d) )\n", nelem, type );
  fprintf( fp, "(\n" );
  int nodeMap[Topology::NNode];
  for (int elem = 0; elem < nelem; elem++)
  {
    xfld.associativity( elem ).getNodeGlobalMapping( nodeMap, Topology::NNode );
    for (int n = 0; n < Topology::NNode; n++ )
      fprintf( fp, "%x ", nodeMap[n]+1 );
    fprintf( fp, "\n" );
  }
  fprintf( fp, ")\n\n" );
  // cell-to-node connectivity

}


//----------------------------------------------------------------------------//
// output as P1
//
template<class PhysDim, class TopoDim, class Topology>
void
output_fluent_InteriorTraceGroup( const std::string& shape,
                       const typename XField<PhysDim,TopoDim>::template FieldTraceGroupType<Topology>& xfld,
                       const int group, int& itrace, FILE* fp )
{
  typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Topology> XFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;

  ElementXFieldTraceClass xfldElem( xfld.basis() );

  int nelem = xfld.nElem();
  int itraceEnd = itrace + nelem -1;
  fprintf( fp, "(13 (%d %x %x 2 2) \n ( \n", group+2, itrace, itraceEnd );
  itrace = itraceEnd+1;

  // trace-to-node connectivity
  int e1; int e2;

  int nodeMap[Topology::NNode];
  for (int elem = 0; elem < nelem; elem++)
  {
    xfld.associativity( elem ).getNodeGlobalMapping( nodeMap, Topology::NNode );
    for (int n = 0; n < Topology::NNode; n++ )
      fprintf( fp, "%x ", nodeMap[n]+1 );

    e1 = xfld.getElementLeft(elem)+1;
    e2 = xfld.getElementRight(elem)+1;

    fprintf( fp, "%x %x", e1, e2);

    fprintf( fp, "\n" );
  }
  fprintf( fp, "))\n\n" );


}


//----------------------------------------------------------------------------//
// output as P1
//
template<class PhysDim, class TopoDim, class Topology>
void
output_fluent_BoundaryTraceGroup( const std::string& shape,
                       const typename XField<PhysDim,TopoDim>::template FieldTraceGroupType<Topology>& xfld,
                       const int group, int& itrace, FILE* fp )
{
  int nelem = xfld.nElem();
  int itraceEnd = itrace + nelem -1;
  fprintf( fp, "(13 (%d %x %x 3 2) \n(\n", group+2, itrace, itraceEnd ); //all walls
  itrace = itraceEnd+1;

  // trace-to-node connectivity
  int e1;

  int nodeMap[Topology::NNode];
  for (int elem = 0; elem < nelem; elem++)
  {
    xfld.associativity( elem ).getNodeGlobalMapping( nodeMap, Topology::NNode );
    for (int n = 0; n < Topology::NNode; n++ )
      fprintf( fp, "%x ", nodeMap[n]+1 );

    e1 = xfld.getElementLeft(elem)+1;

    fprintf( fp, "%x 0", e1);

    fprintf( fp, "\n" );
  }
  fprintf( fp, "))\n\n" );

}


//----------------------------------------------------------------------------//
template<>
void
output_fluent( const XField<PhysD2,TopoD2>& xgrid, const std::string& filename )
{
  std::cout << "output_fluent: filename = " << filename << std::endl;
  FILE* fp = fopen( filename.c_str(), "w" );

  //HEADER
  fprintf( fp, "(0 \"SANS output_fluent\")\n\n" );

  //2D
  fprintf( fp, "(0 \"Dimensions\")\n" );
  fprintf( fp, "(2 2)\n\n");

  int iCell = 0;
  for (int group = 0; group < xgrid.nCellGroups(); group++)
  {
    // dispatch integration over cell groups
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
      iCell += xgrid.getCellGroup<Triangle>(group).nElem();
    else if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Quad) )
      iCell += xgrid.getCellGroup<Quad>(group).nElem();
    else
      SANS_DEVELOPER_EXCEPTION( "Error in output_fluent(XField<PhysD2,TopoD2>): unknown topology\n" );
  }
  fprintf( fp, "(12 (0 1 %x 0))\n", iCell);

  int itrace = 0;
  // loop over interior trace groups (skip the first one because it should mostly duplicate the entire grid)
  for (int group = 0; group < xgrid.nInteriorTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Line) )
    {
      itrace += xgrid.getInteriorTraceGroup<Line>(group).nElem();
    }
    else
    {
      const char msg[] = "Error in output_Fluent(XField<PhysD2,TopoD2>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  // loop over trace boundary groups
  for (int group = 0; group < xgrid.nBoundaryTraceGroups(); group++)
  {
    // dispatch integration over boundary group
    if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Line) )
      itrace += xgrid.getBoundaryTraceGroup<Line>(group).nElem();
    else
    {
      const char msg[] = "Error in output_fluent(XField<PhysD2,TopoD2>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }
  fprintf( fp, "(13 (0 1 %x 0))\n", itrace);


  //DUMP NODES
  const int nnode = xgrid.nDOF();

  fprintf( fp, "(10 (0 1 %x 0 2))\n\n", nnode);


  fprintf( fp, "(12 (1 1 %x 1 1))\n\n", iCell);

  itrace = 1;

  // loop over interior trace groups (skip the first one because it should mostly duplicate the entire grid)
  for (int group = 0; group < xgrid.nInteriorTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Line) )
    {
      output_fluent_InteriorTraceGroup<PhysD2, TopoD2, Line>("PXE_Shape_Edge", xgrid.getInteriorTraceGroup<Line>(group), group, itrace, fp );
    }
    else
    {
      const char msg[] = "Error in output_Fluent(XField<PhysD2,TopoD2>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  // loop over trace boundary groups
  for (int group = 0; group < xgrid.nBoundaryTraceGroups(); group++)
  {
    // dispatch integration over boundary group
    if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Line) )
      output_fluent_BoundaryTraceGroup<PhysD2, TopoD2, Line>("PXE_Shape_Edge", xgrid.getBoundaryTraceGroup<Line>(group),
          (group + xgrid.nInteriorTraceGroups()), itrace, fp );
    else
    {
      const char msg[] = "Error in output_fluent(XField<PhysD2,TopoD2>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

//  // loop over cell groups
//  for (int group = 0; group < xgrid.nCellGroups(); group++)
//  {
//    // dispatch integration over cell groups
//    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
//      output_fluent_CellGroup<PhysD2, TopoD2, Triangle>("PXE_Shape_Triangle", xgrid.getCellGroup<Triangle>(group), fp );
//    else if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Quad) )
//      output_fluent_CellGroup<PhysD2, TopoD2, Quad>("PXE_Shape_Quad", xgrid.getCellGroup<Quad>(group), fp );
//    else
//      SANS_DEVELOPER_EXCEPTION( "Error in output_fluent(XField<PhysD2,TopoD2>): unknown topology\n" );
//  }

  fprintf( fp, "(10 (9 1 %x 1 2)\n", nnode);
  fprintf( fp, "(\n");

  for (int n = 0; n < nnode; n++)
  {
    for (int d = 0; d < PhysD2::D; d++)
      fprintf( fp, "%22.15e ", xgrid.DOF(n)[d] );
    fprintf( fp, "\n");
  }

  fprintf( fp, "))\n\n");

  fclose( fp );
}

//Explicitly instantiate the function
// But instantiation after explicit specialization is not in effect
//template void
//output_grm( const XField<PhysD1,TopoD1>& xgrid, const std::string& filename );
//template void
//output_fluent( const XField<PhysD2,TopoD2>& xgrid, const std::string& filename );
//template void
//output_grm( const XField<PhysD2,TopoD2>& xgrid, const int order, const std::string& filename  );
//template void
//output_grm( const XField<PhysD3,TopoD3>& xgrid, const std::string& filename );


} //namespace SANS
