// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELDSPACETIME_TRAITS_H
#define XFIELDSPACETIME_TRAITS_H

#include "tools/SANSnumerics.h"     // Real

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "Field/Element/ElementAssociativitySpacetime.h"
#include "Field/Element/ElementXFieldSpacetime.h"

#include "XFieldTypes.h"
#include "ElementConnectivity.h"
#include "FieldAssociativityConstructor.h"


namespace SANS
{
//----------------------------------------------------------------------------//
// Spacetime grid field variable in a topologically 4D grid
//----------------------------------------------------------------------------//

template<class PhysDim>
struct XFieldSpacetimeTraits<PhysDim, TopoD4>
{
  static const int D = PhysDim::D;                       // physical dimensions

  typedef DLA::VectorS<D,Real> VectorX;        // coordinates vector

  typedef FieldAssociativityBase< VectorX > FieldBase;

  typedef VectorX T;                                  // DOF type
};

template<class PhysDim, class Topology>
struct XFieldGroupSpacetimeTraits<PhysDim, TopoD4, Topology> : public XFieldSpacetimeTraits<PhysDim, TopoD4>
{
  typedef typename XFieldSpacetimeTraits<PhysDim, TopoD4>::VectorX VectorX;        // coordinates vector

  typedef Topology TopologyType;

  typedef ElementAssociativity<TopoD4,Topology> ElementAssociativityType;

  typedef BasisFunctionSpacetimeBase<Topology> BasisType;

  typedef FieldAssociativityConstructor< typename ElementAssociativityType::Constructor > FieldAssociativityConstructorType;

  template<class ElemT>
  struct ElementType : public ElementXField<PhysDim, TopoD4, Topology>
  {
    typedef ElementXField<PhysDim, TopoD4, Topology> BaseType;

    explicit ElementType( const BasisType* basis ) : BaseType(basis) {}
    ElementType( int order, const BasisFunctionCategory& category ) : BaseType(order,category) {}
    ElementType& operator=( const ElementType& elem ) { BaseType::operator=(elem); return *this; }
  };
};

}

#endif  // XFIELDSPACETIME_TRAITS_H
