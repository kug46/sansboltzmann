// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SPACETYPE_H
#define SPACETYPE_H

namespace SANS
{

//----------------------------------------------------------------------------//
/*
 * An enum class for labeling fields
 */
//----------------------------------------------------------------------------//

enum class SpaceType { Continuous, Discontinuous, Unspecified, Technical };

// Tags - so that runtime versions can be seen
struct SpaceTypeContinuous    { static constexpr SpaceType spaceType = SpaceType::Continuous; };
struct SpaceTypeDiscontinuous { static constexpr SpaceType spaceType = SpaceType::Discontinuous; };
struct SpaceTypeUnspecified   { static constexpr SpaceType spaceType = SpaceType::Unspecified; };
struct SpaceTypeTechnical     { static constexpr SpaceType spaceType = SpaceType::Technical; };


} //namespace SANS

#endif //SPACETYPE_H
