// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD_BOUNDARYTRACE_H
#define XFIELD_BOUNDARYTRACE_H

#include <vector>
#include <memory> //shared_ptr

#include "BasisFunction/TraceToCellRefCoord.h"

#include "MPI/communicator_fwd.h"

#include "FieldBase.h"
#include "FieldAssociativityConstructor.h"
#include "FieldAssociativity.h"
#include "XField.h"

#include "Field/Element/BoundingBox.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// XField_BoundaryTrace represent a grid in any physical/topological dimension
//----------------------------------------------------------------------------//

template<class PhysDim, class TopoDim>
class XField_BoundaryTrace : public FieldBase< XFieldTraits_BoundaryTrace<PhysDim, TopoDim> >,
  public FieldType< XField_BoundaryTrace<PhysDim, TopoDim> >
{
public:
  typedef FieldBase< XFieldTraits_BoundaryTrace<PhysDim, TopoDim> > BaseType;
  typedef XField<PhysDim, TopoDim> XFieldType;
  typedef typename XField<PhysDim, TopoDim>::VectorX VectorX; // coordinates vector
  static const int D = PhysDim::D;                            // physical dimensions

  typedef typename BaseType::FieldTraceGroupBase FieldTraceGroupBase;
  typedef typename BaseType::FieldCellGroupBase FieldCellGroupBase;

  template<class Topology>
  using FieldTraceGroupType = typename BaseType::template FieldTraceGroupType<Topology>;

  template<class Topology>
  using FieldCellGroupType = typename BaseType::template FieldCellGroupType<Topology>;

  XField_BoundaryTrace( const XField_BoundaryTrace& ) = delete;
  XField_BoundaryTrace& operator=( const XField_BoundaryTrace& ) = delete;

  XField_BoundaryTrace( const XFieldType& xfld, const std::vector<int>& boundaryTraceGroups, bool global = true );
  virtual ~XField_BoundaryTrace() {}

  // This is needed for FieldTuple
  const XFieldType& getXField() const { return xfld_; }

  // The XField needs these "Global" accessors to be consistent with Fields in Tuples
        FieldTraceGroupBase& getHubTraceGroupBaseGlobal( const int group )       { return BaseType::getHubTraceGroupBase(group); }
  const FieldTraceGroupBase& getHubTraceGroupBaseGlobal( const int group ) const { return BaseType::getHubTraceGroupBase(group); }

        FieldTraceGroupBase& getInteriorTraceGroupBaseGlobal( const int group )       { return BaseType::getInteriorTraceGroupBase(group); }
  const FieldTraceGroupBase& getInteriorTraceGroupBaseGlobal( const int group ) const { return BaseType::getInteriorTraceGroupBase(group); }

        FieldTraceGroupBase& getBoundaryTraceGroupBaseGlobal( const int group )       { return BaseType::getBoundaryTraceGroupBase(group); }
  const FieldTraceGroupBase& getBoundaryTraceGroupBaseGlobal( const int group ) const { return BaseType::getBoundaryTraceGroupBase(group); }

        FieldCellGroupBase& getCellGroupBaseGlobal( const int group )       { return BaseType::getCellGroupBase(group); }
  const FieldCellGroupBase& getCellGroupBaseGlobal( const int group ) const { return BaseType::getCellGroupBase(group); }


  template<class Topology>
        FieldTraceGroupType<Topology>& getHubTraceGroupGlobal( const int group )
  { return BaseType::template getHubTraceGroup<Topology>(group); }
  template<class Topology>
  const FieldTraceGroupType<Topology>& getHubTraceGroupGlobal( const int group ) const
  { return BaseType::template getHubTraceGroup<Topology>(group); }

  template<class Topology>
        FieldTraceGroupType<Topology>& getInteriorTraceGroupGlobal( const int group )
  { return BaseType::template getInteriorTraceGroup<Topology>(group); }
  template<class Topology>
  const FieldTraceGroupType<Topology>& getInteriorTraceGroupGlobal( const int group ) const
  { return BaseType::template getInteriorTraceGroup<Topology>(group); }

  template<class Topology>
        FieldTraceGroupType<Topology>& getBoundaryTraceGroupGlobal( const int group )
  { return BaseType::template getBoundaryTraceGroup<Topology>(group); }
  template<class Topology>
  const FieldTraceGroupType<Topology>& getBoundaryTraceGroupGlobal( const int group ) const
  { return BaseType::template getBoundaryTraceGroup<Topology>(group); }

  template<class Topology>
        FieldCellGroupType<Topology>& getCellGroupGlobal( const int group )
  { return BaseType::template getCellGroup<Topology>(group); }
  template<class Topology>
  const FieldCellGroupType<Topology>& getCellGroupGlobal( const int group ) const
  { return BaseType::template getCellGroup<Topology>(group); }

  virtual int nDOFCellGroup(int cellgroup) const override
  {
    SANS_DEVELOPER_EXCEPTION("XField::nDOFCellGroup - Not implemented yet.");
    return -1;
  }

  virtual int nDOFInteriorTraceGroup(int tracegroup) const override
  {
    SANS_DEVELOPER_EXCEPTION("XField::nDOFInteriorTraceGroup - Not implemented yet.");
    return -1;
  }

  virtual int nDOFBoundaryTraceGroup(int tracegroup) const override
  {
    SANS_DEVELOPER_EXCEPTION("XField::nDOFBoundaryTraceGroup - Not implemented yet.");
    return -1;
  }

  int nGhostBoundaryTraceGroups() const { return ghostBoundaryTraceGroups_.size(); }

  const FieldTraceGroupBase& getGhostBoundaryTraceGroupBase( const int group ) const;

  template<class Topology>
  const FieldTraceGroupType<Topology>& getGhostBoundaryTraceGroup( const int group ) const;

  // communicator accessor for the XField
  std::shared_ptr<mpi::communicator> comm() const { return xfld_.comm(); }

  // a list of unique native element IDs
  //const std::vector<int>& boundaryTraceIDs(const int group) const { return boundaryTraceIDs_[group]; }

  const BoundingBox<PhysDim>& boundaryTraceElemBBox(const int group, const int elem) const
  {
    return boundaryTraceBBox_[group][elem];
  }

protected:
  const XFieldType& xfld_;

  template<class TopologyTrace>
  void getBoundaryTraceDOFs(std::map<int,VectorX>& bndDOF, const int boundaryTraceGroup );

  void createBoundaryTraceDOFs( std::map<int,VectorX>& bndDOF, std::map<int,int>& localDOFmap, const bool global );

  template<class TopologyTrace>
  void createBoundaryTraceElems(const int groupLocal, const std::map<int,int>& localDOFmap, const int boundaryTraceGroup, const bool global );

  //-------------------------------------------------------------------------//
  // Functions to help construct the XField

  void createBoundaryTraceGroup( const int group_id , FieldTraceGroupBase *grp )
  { this->boundaryTraceGroups_[group_id] = grp; this->boundaryTraceGroups_[group_id]->setDOF(this->DOF_ , this->nDOF_);  }

  void resizeGhostBoundaryTraceGroups( const int nGhostBoundaryTraceGroups );

  PointerArray<FieldTraceGroupBase> ghostBoundaryTraceGroups_; // groups of ghost boundary-trace elements for checkGrid

  std::vector<std::vector<BoundingBox<PhysDim>>> boundaryTraceBBox_;
};  // class XField


template <int k, class PhysDim, class TopoDim>
const XField_BoundaryTrace<PhysDim, TopoDim>&
get(const XField_BoundaryTrace<PhysDim, TopoDim>& xfld)
{
  static_assert( k == 0 || k == -1, "k should be zero or -1 if the argument to get is XField");
  return xfld;
}

} // namespace SANS


#endif // XFIELD_BOUNDARYTRACE_H
