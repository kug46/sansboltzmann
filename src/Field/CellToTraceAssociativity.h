// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef CELLTOTRACEASSOCIATIVITY_H
#define CELLTOTRACEASSOCIATIVITY_H

#include <vector>

#include "tools/SANSException.h"

namespace SANS
{

struct TraceInfo
{
  enum GroupType
  {
    Interior = 0,
    Boundary = 1,
    GhostBoundary = 2,
    Unspecified = -1
  };

  TraceInfo() : group(-1), elem(-1), type(Unspecified) {}
  TraceInfo(int group_, int elem_, GroupType type_) : group(group_), elem(elem_), type(type_) {}

  int group, elem; //global group index, and element index in that group
  GroupType type; // type as in GroupType
};

class CellToTraceAssociativityBase
{
public:
  virtual ~CellToTraceAssociativityBase(){};

  virtual void setTrace(int cellElem, int canonicalTrace, int traceGroup, int traceElem, TraceInfo::GroupType type) = 0;
  virtual int nElem() const = 0;
  virtual const TraceInfo& getTrace(int cellElem, int canonicalTrace) const = 0;
};

template<class Topology>
class CellToTraceAssociativity : public CellToTraceAssociativityBase
{
public:

  explicit CellToTraceAssociativity(const int nElem);

  virtual ~CellToTraceAssociativity();

  virtual void setTrace(int cellElem, int canonicalTrace, int traceGroup, int traceElem, TraceInfo::GroupType type) override;
  virtual int nElem() const override;
  virtual const TraceInfo& getTrace(int cellElem, int canonicalTrace) const override;

private:
  std::vector<std::vector<TraceInfo>> trace_map_; //contains trace information, indexing:[cell index][canonical trace]
};

}

#endif  // CELLTOTRACEASSOCIATIVITY_H
