// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(FIELDSPACETIME_CG_TRACE_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "FieldSpacetime_CG_Trace.h"

#include "BasisFunction/BasisFunctionCategory.h"

#include "Field_CG/Field_CG_TraceConstructor.h"

#include "tools/split_cat_std_vector.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// 3D solution field: CG trace
//----------------------------------------------------------------------------//

template <class PhysDim, class T>
Field_CG_Trace<PhysDim, TopoD4, T>::Field_CG_Trace( const XField<PhysDim, TopoD4>& xfld,
                                                    const int order, const BasisFunctionCategory& category )
  : BaseType( xfld )
{
  init(order,category,BaseType::createInteriorGroupIndex(),BaseType::createBoundaryGroupIndex());
}

template <class PhysDim, class T>
Field_CG_Trace<PhysDim, TopoD4, T>::Field_CG_Trace( const XField<PhysDim, TopoD4>& xfld,
                                                    const int order,
                                                    const BasisFunctionCategory& category,
                                                    const std::vector<int>& InteriorGroups,
                                                    const std::vector<int>& BoundaryGroups )
  : BaseType( xfld )
{
  BaseType::checkInteriorGroupIndex(InteriorGroups);
  BaseType::checkBoundaryGroupIndex(BoundaryGroups);
  init(order,category,InteriorGroups,BoundaryGroups);
}

template <class PhysDim, class T>
Field_CG_Trace<PhysDim, TopoD4, T>::Field_CG_Trace( const Field_CG_Trace& fld, const FieldCopy& tag )
  : BaseType( fld, tag )
{
}

template <class PhysDim, class T>
Field_CG_Trace<PhysDim, TopoD4, T>&
Field_CG_Trace<PhysDim, TopoD4, T>::operator=( const ArrayQ& q )
{
  Field< PhysDim, TopoD4, T >::operator=(q);
  return *this;
}

//----------------------------------------------------------------------------//
template <class PhysDim, class T>
void
Field_CG_Trace<PhysDim, TopoD4, T>::init( const int order, const BasisFunctionCategory& category,
    const std::vector<int>& interiorGroups, const std::vector<int>& boundaryGroups )
{
  SANS_ASSERT_MSG( order >= 1, "CG trace requires order >= 1" );
  SANS_ASSERT_MSG( category == BasisFunctionCategory_Hierarchical ||
                   category == BasisFunctionCategory_Lagrange,
                   "CG area must use Hierarchical or Lagrange Basis" );

  SANS_DEVELOPER_EXCEPTION("This is not tested, and is possibly wrong....");

  std::vector<std::vector<int>> interiorGroupSets = {interiorGroups};
  std::vector<std::vector<int>> boundaryGroupSets = {boundaryGroups};

  Field_CG_TraceConstructor<PhysDim, TopoD4> fldConstructor(xfld_, order, category, interiorGroupSets, boundaryGroupSets);

  nElem_ = fldConstructor.nElem();

  // allocate the solution DOF array
  this->resizeDOF(fldConstructor.nDOF());

  std::vector<int> ConcatInteriorGroups = cat(interiorGroupSets);

  this->resizeInteriorTraceGroups( ConcatInteriorGroups );

  // set solution DOF associativity for each interior-edge group

  for (std::size_t igroup = 0; igroup < ConcatInteriorGroups.size(); igroup++)
  {
    const int group = ConcatInteriorGroups[igroup];
    //int localGroup = localInteriorTraceGroups_.at(group);

    if ( xfld_.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Tet) )
    {
      typedef typename BaseType:: template FieldTraceGroupType<Tet> FieldTraceGroupClass;

      int localGroup = localInteriorTraceGroups_.at(group);
      // allocate group
      interiorTraceGroups_[localGroup] = fldConstructor.template createInteriorTraceGroup<FieldTraceGroupClass>( group, this->local2nativeDOFmap_ );
      interiorTraceGroups_[localGroup]->setDOF( DOF_, nDOF_ );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "Unknown interior-trace topology." );
  }


  // set solution DOF associativity for each boundary region

  std::vector<int> ConcatBoundaryGroups = cat(boundaryGroupSets);

  this->resizeBoundaryTraceGroups( ConcatBoundaryGroups );

  for (std::size_t igroup = 0; igroup < ConcatBoundaryGroups.size(); igroup++)
  {
    const int group = ConcatBoundaryGroups[igroup];
    int localGroup = localBoundaryTraceGroups_.at(group);

    if ( xfld_.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Tet) )
    {
      typedef typename BaseType:: template FieldTraceGroupType<Tet> FieldTraceGroupClass;

      // allocate group
      boundaryTraceGroups_[localGroup] = fldConstructor.template createBoundaryTraceGroup<FieldTraceGroupClass>( group, this->local2nativeDOFmap_ );
      boundaryTraceGroups_[localGroup]->setDOF( DOF_, nDOF_ );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "Unknown boundary-trace topology." );
  }

  // get the ghost/zombie DOF ranks
  // this must be last as it uses this->local2nativeDOFmap_
  this->nDOFpossessed_ = fldConstructor.nDOFpossessed();
  this->nDOFghost_     = fldConstructor.nDOFghost();

  this->resizeDOF_rank(fldConstructor.nDOF(), fldConstructor.nDOFpossessed());

  fldConstructor.getDOF_rank(this->DOFghost_rank_);
}

} // namespace SANS
