// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELD_DG_INTERIORTRACE_H
#define FIELD_DG_INTERIORTRACE_H

#include "Field.h"

#include "tools/split_cat_std_vector.h" // SANS::cat (in derived constructors)

namespace SANS
{
//----------------------------------------------------------------------------//
// solution field: DG interior trace-field constructor class
//----------------------------------------------------------------------------//

template <class PhysDim, class TopoDim, class T>
class Field_DG_InteriorTraceBase : public Field< PhysDim, TopoDim, T >
{
public:
  typedef Field< PhysDim, TopoDim, T > BaseType;
  typedef T ArrayQ;

  Field_DG_InteriorTraceBase() = delete;
  virtual ~Field_DG_InteriorTraceBase() {};

  virtual int nDOFCellGroup(int cellgroup) const override;
  virtual int nDOFInteriorTraceGroup(int tracegroup) const override;
  virtual int nDOFBoundaryTraceGroup(int tracegroup) const override;

  // Virtual class to denote the SpaceType of the Field
  virtual SpaceType spaceType() const override { return SpaceType::Discontinuous; }

protected:
  //Protected constructor. This is a helper class
  explicit Field_DG_InteriorTraceBase( const XField<PhysDim, TopoDim>& xfld );

  Field_DG_InteriorTraceBase( const Field_DG_InteriorTraceBase& fld, const FieldCopy& tag );

  // Constructs a new trace group
  template<class Topology>
  void createInteriorTraceGroup(const int group, const int order, const BasisFunctionCategory& category);

  void createDOFs();

  using BaseType::nDOF_;
  using BaseType::DOF_;
  using BaseType::nElem_;
  using BaseType::interiorTraceGroups_;
  using BaseType::localInteriorTraceGroups_;
  using BaseType::xfld_;
};

}

#endif  // FIELD_DG_INTERIORTRACE_H
