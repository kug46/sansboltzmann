// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(FIELD_DG_BOUNDARYTRACEBASE_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "Field_DG_BoundaryTraceBase.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
Field_DG_BoundaryTraceBase<PhysDim, TopoDim, T>::Field_DG_BoundaryTraceBase( const XField<PhysDim, TopoDim>& xfld )
  : BaseType(xfld)
{
  //Derived classes call createBoundaryTraceGroup
}

template <class PhysDim, class TopoDim, class T>
Field_DG_BoundaryTraceBase<PhysDim, TopoDim, T>::Field_DG_BoundaryTraceBase(
    const Field_DG_BoundaryTraceBase& fld, const FieldCopy& tag ) : BaseType(fld, tag) {}


//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
template<class Topology>
void
Field_DG_BoundaryTraceBase<PhysDim, TopoDim, T>::
createBoundaryTraceGroup(const int groupGlobal, const int order, const BasisFunctionCategory& category)
{
  typedef typename XField<PhysDim, TopoDim>:: template FieldTraceGroupType<Topology> XFieldTraceClass;
  typedef typename BaseType::template FieldTraceGroupType<Topology> FieldTraceClass;
  typedef typename FieldTraceClass::BasisType BasisType;

  const XFieldTraceClass& xfldGroup = xfld_.template getBoundaryTraceGroup<Topology>(groupGlobal);

  const BasisType* basis = BasisType::getBasisFunction(order, category);

  // create field associativity constructor
  typename FieldTraceClass::FieldAssociativityConstructorType fldAssocTrace( basis, xfldGroup.nElem() );

  // DOF ordering: Fortran-array DOF(basis,elem)
  const int nElem = xfldGroup.nElem();
  const int nBasis = basis->nBasis();
  std::vector<int> map( nBasis );
  for (int elem = 0; elem < nElem; elem++)
  {
    for (int i = 0; i < nBasis; i++)
      map[i] = nDOF_ + nBasis*elem + i;

    fldAssocTrace.setAssociativity( elem ).setRank( xfldGroup.associativity( elem ).rank() );
    fldAssocTrace.setAssociativity( elem ).setGlobalMapping( map );
  }

  // allocate group
  boundaryTraceGroups_[localBoundaryTraceGroups_.at(groupGlobal)] = new FieldTraceClass( fldAssocTrace );

  // accumulate DOF and Element count
  nDOF_  += nElem*nBasis;
  nElem_ += nElem;
}

//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
void
Field_DG_BoundaryTraceBase<PhysDim, TopoDim, T>::createDOFs()
{
  // allocate the solution DOF array and assign it to groups

  this->resizeDOF(nDOF_);

  for (int group = 0; group < boundaryTraceGroups_.size(); group++)
    if ( boundaryTraceGroups_[group] != NULL )
      boundaryTraceGroups_[group]->setDOF( DOF_, nDOF_ );
}

//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
int
Field_DG_BoundaryTraceBase<PhysDim, TopoDim, T>::nDOFCellGroup(int cellgroup) const
{
  SANS_DEVELOPER_EXCEPTION("Field_DG_BoundaryTraceBase::nDOFCellGroup - No cell groups!");
  return -1;
}

template <class PhysDim, class TopoDim, class T>
int
Field_DG_BoundaryTraceBase<PhysDim, TopoDim, T>::nDOFInteriorTraceGroup(int tracegroup) const
{
  SANS_DEVELOPER_EXCEPTION("Field_DG_BoundaryTraceBase::nDOFInteriorTraceGroup - No interior trace groups!");
  return -1;
}

template <class PhysDim, class TopoDim, class T>
int
Field_DG_BoundaryTraceBase<PhysDim, TopoDim, T>::nDOFBoundaryTraceGroup(int tracegroup) const
{
  const int nElem = this->getBoundaryTraceGroupBase(tracegroup).nElem();
  const int nBasisTrace = this->getBoundaryTraceGroupBase(tracegroup).nBasis();
  return nElem*nBasisTrace;
}

}
