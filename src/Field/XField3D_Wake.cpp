// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField3D_Wake.h"

#include "Field/XFieldLine_Traits.h"

#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

const char* XField3D_Wake::WAKESHEET = "Wake";
const char* XField3D_Wake::TREFFTZ = "Trefftz";
const char* XField3D_Wake::BCNAME = "BCName";

//----------------------------------------------------------------------------//
XField3D_Wake::XField3D_Wake() : dupPointOffset_(0)
{
}

//----------------------------------------------------------------------------//
XField3D_Wake::XField3D_Wake( mpi::communicator& comm )
  : XField<PhysD3, TopoD3>(comm), dupPointOffset_(0)
{
  SANS_ASSERT(comm.size() == 1);
}

//----------------------------------------------------------------------------//
XField3D_Wake::XField3D_Wake( const std::shared_ptr<mpi::communicator>& comm )
  : XField<PhysD3, TopoD3>(comm), dupPointOffset_(0)
{
  XField3D_Wake();
  SANS_ASSERT(comm->size() == 1);
}

//----------------------------------------------------------------------------//
void
XField3D_Wake::cloneFrom( const XField3D_Wake& xfld )
{
  XField<PhysD3, TopoD3>::cloneFrom(xfld);

  modelBodyIndex_[0] = xfld.modelBodyIndex_[0];
  modelBodyIndex_[1] = xfld.modelBodyIndex_[1];

  dupPointOffset_ = xfld.dupPointOffset_;
  invPointMap_ = xfld.invPointMap_;
  KuttaPoints_ = xfld.KuttaPoints_;

  KuttaBCTraceElemLeft_ = xfld.KuttaBCTraceElemLeft_;
  KuttaBCTraceElemRight_ = xfld.KuttaBCTraceElemRight_;

  KuttaCellElemLeft_ = xfld.KuttaCellElemLeft_;
  KuttaCellElemRight_ = xfld.KuttaCellElemRight_;

  resizeBoundaryFrameGroups(0); // first free old memory
  resizeBoundaryFrameGroups(xfld.boundaryFrameGroups_.size());

  for (int i = 0; i < xfld.boundaryFrameGroups_.size(); i++ )
  {
    if ( xfld.boundaryFrameGroups_[i] == nullptr ) continue;
#ifndef __clang_analyzer__ // clang thinks this leaks memory...
    boundaryFrameGroups_[i] = static_cast<FieldFrameGroupType*>(xfld.boundaryFrameGroups_[i]->clone()); // NOTE: invokes new []
#endif
    boundaryFrameGroups_[i]->setDOF(DOF_, nDOF_);
  }
}

//----------------------------------------------------------------------------//
void
XField3D_Wake::resizeBoundaryFrameGroups( const int nBoundaryFrameGroups )
{
  boundaryFrameGroups_.resize(nBoundaryFrameGroups);
}

//----------------------------------------------------------------------------//
XField3D_Wake::~XField3D_Wake()
{
}

}
