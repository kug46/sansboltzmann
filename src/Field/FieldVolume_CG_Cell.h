// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDVOLUME_CG_CELL_H
#define FIELDVOLUME_CG_CELL_H

#include <vector>

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "Field/Element/ElementAssociativityVolumeConstructor.h"
#include "FieldVolume.h"
#include "EmbeddedCGType.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// solution field: 3D CG element-field
//
// basis functions set to hierarchical
//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
class Field_CG_Cell;

template <class PhysDim, class T>
class Field_CG_Cell< PhysDim, TopoD3, T > : public Field< PhysDim, TopoD3, T >
{
public:
  typedef Field< PhysDim, TopoD3, T > BaseType;
  typedef T ArrayQ;

  Field_CG_Cell( const XField<PhysDim, TopoD3>& xfld, const int order, const BasisFunctionCategory& category,
                 const embeddedType eType = RegularCGField );

  explicit Field_CG_Cell( const XField<PhysDim, TopoD3>& xfld, const int order, const BasisFunctionCategory& category,
                 const std::vector<int>& CellGroups,
                 const embeddedType eType = RegularCGField ); //no default here - too easy to confused with the above constructor

  explicit Field_CG_Cell( const std::vector<std::vector<int>>& CellGroupSets,
                          const XField<PhysDim, TopoD3>& xfld, const int order, const BasisFunctionCategory& category,
                          const embeddedType eType = RegularCGField  );

  Field_CG_Cell( const Field_CG_Cell& fld, const FieldCopy&tag );

  Field_CG_Cell& operator=( const ArrayQ& q );

  // Virtual class to denote the SpaceType of the Field
  virtual SpaceType spaceType() const override { return SpaceType::Continuous; }

  bool needsEmbeddedGhosts() const { return needsEmbeddedGhosts_; }
protected:
  void init( const int order, const BasisFunctionCategory& category, const std::vector<std::vector<int>>& CellGroupSetss );

  const bool needsEmbeddedGhosts_;

  using BaseType::nElem_;
  using BaseType::nDOF_;
  using BaseType::DOF_;
  using BaseType::xfld_;
  using BaseType::cellGroups_;
  using BaseType::localCellGroups_;
  using BaseType::interiorTraceGroups_;
  using BaseType::boundaryTraceGroups_;
  using BaseType::localBoundaryTraceGroups_;
};

}

#endif  // FIELDVOLUME_CG_CELL_H
