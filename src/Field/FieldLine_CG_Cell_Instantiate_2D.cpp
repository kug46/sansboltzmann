// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define FIELDLINE_CG_CELL_INSTANTIATE
#include "FieldLine_CG_Cell_impl.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "Surreal/SurrealS.h"

#include "XFieldLine.h"
#include "FieldLine.h"

namespace SANS
{

template class Field_CG_Cell< PhysD2, TopoD1, Real >;

template class Field_CG_Cell< PhysD2, TopoD1, SurrealS<1> >;

template class Field_CG_Cell< PhysD2, TopoD1, DLA::VectorS< 2, Real > >;
template class Field_CG_Cell< PhysD2, TopoD1, DLA::VectorS< 3, Real > >;
template class Field_CG_Cell< PhysD2, TopoD1, DLA::VectorS< 4, Real > >;
template class Field_CG_Cell< PhysD2, TopoD1, DLA::VectorS< 6, Real > >;

template class Field_CG_Cell< PhysD2, TopoD1, DLA::VectorS< 1, DLA::VectorS<2, Real> > >;
}
