// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDASSOCIATIVITYSEQUENCE_H
#define FIELDASSOCIATIVITYSEQUENCE_H

#include <vector>
#include <typeinfo>
#include <algorithm> // rotate

#include "tools/SANSnumerics.h" // Real
#include "Field.h"
#include "Field/Element/Element.h"
#include "Field/Element/ElementSequence.h"

#include "tools/Surrealize.h"

namespace SANS
{

// Forward declare
template<class PhysDim, class TopoDim, class T, template<class,class,class> class BaseField>
class FieldSequence;

//----------------------------------------------------------------------------//
// Field of elements associativities sequence
//
// used to represent a sequence of field associativities
//
//----------------------------------------------------------------------------//

class FieldAssociativitySequenceBase
{
public:

  virtual ~FieldAssociativitySequenceBase() {}

  virtual const std::type_info& topoTypeID() const = 0;
  virtual int nElem() const = 0;
  virtual int nDOF() const = 0;

  // only let FieldSequence call protected functions, e.g. rotate
  template<class PhysDim, class TopoDim, class T, template<class,class,class> class BaseField> friend class FieldSequence;

protected:
  FieldAssociativitySequenceBase() {}     // abstract base class

  virtual void rotate() = 0;
};


template < class FieldAssociativityType >
class FieldAssociativitySequence : public FieldAssociativitySequenceBase
{
public:
  typedef typename FieldAssociativityType::T T;              // DOF type
  typedef typename FieldAssociativityType::FieldBase FieldBase;
  typedef typename FieldAssociativityType::BasisType BasisType;
  typedef typename FieldAssociativityType::TopologyType TopologyType;
  typedef typename FieldAssociativityType::ElementAssociativityType ElementAssociativityType;
  typedef typename FieldAssociativityType::FieldAssociativityConstructorType FieldAssociativityConstructorType;

  // only let FieldSequence call protected functions, e.g. rotate
  template<class PhysDim, class TopoDim, class T, template<class,class,class> class BaseField> friend class FieldSequence;

  template<class ElemT = Real>
  using ElementType = ElementSequence< typename Surrealize<ElemT,T>::T, typename TopologyType::TopoDim, TopologyType >;

  FieldAssociativitySequence(const BasisType* basis, const std::vector<FieldAssociativityType*>& assocs) :
    basis_(basis), assocs_(assocs) {}
  ~FieldAssociativitySequence() {}

  // gets the toplogical type ID
  virtual const std::type_info& topoTypeID() const override { return typeid(TopologyType); }
  virtual int nElem() const override { return assocs_[0]->nElem(); }
  virtual int nDOF() const override { return assocs_[0]->nDOF(); }

  // basis function
  const BasisFunctionSequence<BasisType> basis() const { return BasisFunctionSequence<BasisType>(assocs_.size(), basis_); }

  // extract/set individual element
  template <class ElemT>
  void getElement(       ElementSequence<ElemT, typename TopologyType::TopoDim, TopologyType>& fldElem, const int elem ) const;
  template <class ElemT>
  void setElement( const ElementSequence<ElemT, typename TopologyType::TopoDim, TopologyType>& fldElem, const int elem );

  int nFieldGroups() const { return assocs_.size(); }

        FieldAssociativityType& operator[](const int i)       { return *assocs_[i]; }
  const FieldAssociativityType& operator[](const int i) const { return *assocs_[i]; }

protected:
  const BasisType* basis_;

  std::vector<FieldAssociativityType*> assocs_; // pointers referencing associativities store in FieldSequence (do not deallocate here)

  // Rotates the last field so it's first. i.e. {Group0, Group1, Group2, Group3} -> {Group3, Group0, Group1, Group2}
  virtual void rotate() override { std::rotate(assocs_.begin(), assocs_.end()-1, assocs_.end()); }
};


// extract DOFs for individual element
template <class FieldAssociativityType>
template <class ElemT>
void
FieldAssociativitySequence<FieldAssociativityType>::
getElement( ElementSequence<ElemT, typename TopologyType::TopoDim, TopologyType>& fldElem, const int elem ) const
{
  SANS_ASSERT( (std::size_t)fldElem.nElem() == assocs_.size() );

  const int nElem = fldElem.nElem();
  for (int i = 0; i < nElem; i++)
    assocs_[i]->getElement(fldElem[i], elem);
}

// set DOFs for individual element
template <class FieldAssociativityType>
template <class ElemT>
void
FieldAssociativitySequence<FieldAssociativityType>::
setElement( const ElementSequence<ElemT, typename TopologyType::TopoDim, TopologyType>& fldElem, const int elem )
{
  SANS_ASSERT( (std::size_t)fldElem.nElem() == assocs_.size() );

  const int nElem = fldElem.nElem();
  for (int i = 0; i < nElem; i++)
    assocs_[i]->setElement(fldElem[i], elem);
}

}


#endif //FIELDASSOCIATIVITYSEQUENCE_H
