// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDVOLUME_CG_BOUNDARYFRAME_H
#define FIELDVOLUME_CG_BOUNDARYFRAME_H

#include <ostream>
#include <string>
#include <memory>
#include <set>
#include <map>
#include <typeinfo>     // typeid

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "FieldGroupLine_Traits.h"
#include "FieldVolume.h"

namespace SANS
{
// TODO: This needs some severe cleanup once we have P > 1

//----------------------------------------------------------------------------//
// solution field: CG frame elements
//
//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
class Field_CG_BoundaryFrame;

template <class PhysDim, class T>
class Field_CG_BoundaryFrame<PhysDim, TopoD3, T> : public Field< PhysDim, TopoD3, T >
{
public:
  typedef Field< PhysDim, TopoD3, T > BaseType;
  typedef T ArrayQ;
#if 0
  Field_CG_BoundaryFrame( const XField<PhysDim, TopoD3>& xfld, const int order )
    : BaseType( xfld )
  {
    // By default all boundary groups are used
    init(order, BaseType::createBoundaryGroupIndex() );
  }
#endif
  template<class XFieldType>
  Field_CG_BoundaryFrame( const XFieldType& xfld, const int order, const std::vector<int>& FrameGroups )
    : BaseType( xfld ), nBoundaryFrameGroups_(0), boundaryFrameGroups_(NULL)
  {
    // Check that the groups asked for are within the range of available groups
    //BaseType::checkBoundaryGroupIndex( FrameGroups );
    init(xfld, order, FrameGroups);
  }

  ~Field_CG_BoundaryFrame();

  Field_CG_BoundaryFrame& operator=( const ArrayQ& q ) { BaseType::operator=(q); return *this; }

  typedef FieldAssociativity< FieldGroupLineTraits<ArrayQ> > FieldFrameGroupType;

  int nBoundaryFrameGroups() const { return nBoundaryFrameGroups_; }
  const FieldFrameGroupType& getBoundaryFrameGroup( const int group ) const { return *boundaryFrameGroups_[group]; }
  const FieldFrameGroupType& getBoundaryFrameGroupGlobal( const int groupGlobal ) const;

  // Virtual class to denote the SpaceType of the Field
  virtual SpaceType spaceType() const override { return SpaceType::Continuous; }

protected:
  using BaseType::nDOF_;
  using BaseType::DOF_;
  using BaseType::xfld_;
  using BaseType::nBoundaryTraceGroups_;
  using BaseType::boundaryTraceGroups_;
  using BaseType::localBoundaryTraceGroups_;

  template<class XFieldType>
  void init( const XFieldType& xfld, const int order, const std::vector<int>& FrameGroups );

  template<class XFieldType>
  void resizeBoundaryFrameGroups( const XFieldType& xfld, const std::vector<int>& BoundaryFrameGroups );

  void resizeBoundaryFrameGroups( const int nBoundaryFrameGroups );

  std::vector<int> localBoundaryFrameGroups_;

  int nBoundaryFrameGroups_;
  FieldFrameGroupType** boundaryFrameGroups_;   // groups of boundary-frame elements
};


//----------------------------------------------------------------------------//
template <class PhysDim, class T>
template<class XFieldType>
void
Field_CG_BoundaryFrame<PhysDim, TopoD3, T>::init(const XFieldType& xfld, const int order, const std::vector<int>& FrameGroups )
{
  SANS_ASSERT_MSG( order >= 1, "CG frame boundary face requires order >= 1" );
  SANS_ASSERT_MSG( order == 1, "3D CG frame boundary hard coded for order == 1. FIX ME!" );


  nDOF_ = 0;
  std::set<int> Nodes;

  // determine total nodes on all frame regions
  for (std::size_t igroup = 0; igroup < FrameGroups.size(); igroup++)
  {
    const int group = FrameGroups[igroup];

    if ( xfld.getBoundaryFrameGroup(group).topoTypeID() == typeid(Line) )
    {
      const typename XFieldType::FieldFrameGroupType& xfldGroup = xfld.getBoundaryFrameGroup(group);

      int nElem = xfldGroup.nElem();

      // grid/solution DOF associativity
      int nodeMap[2];

      for (int edge = 0; edge < nElem; edge++)
      {
        xfldGroup.associativity( edge ).getNodeGlobalMapping( nodeMap, 2 );
        Nodes.insert(nodeMap[0]);
        Nodes.insert(nodeMap[1]);
      }
    }
  }

  nDOF_ = Nodes.size();

  // allocate the solution DOF array
  this->resizeDOF(nDOF_);
  for (int n = 0; n < nDOF_; n++)
    DOF_[n] = 0;

  resizeBoundaryFrameGroups(xfld, FrameGroups);

  std::map<int,int> nodeNumber;
  int cntNode = 0;
  for (auto k = Nodes.begin(); k != Nodes.end(); k++)
  {
    nodeNumber[*k] = cntNode;
    cntNode++;
  }


  // set solution DOF associativity for each boundary frame region

  for (std::size_t igroup = 0; igroup < FrameGroups.size(); igroup++)
  {
    const int group = FrameGroups[igroup];

    if ( xfld.getBoundaryFrameGroup(group).topoTypeID() == typeid(Line) )
    {
      const typename XFieldType::FieldFrameGroupType& xfldGroup = xfld.getBoundaryFrameGroup(group);

      const BasisFunctionLineBase* basis = BasisFunctionLineBase::getBasisFunction(order, BasisFunctionCategory_Hierarchical);

      // create field associativity constructor
      typename FieldFrameGroupType::FieldAssociativityConstructorType fldAssocFrame( basis, xfldGroup.nElem() );

      // grid/solution DOF associativity
      int nodeMap[2];

      // solution nodal DOFs numbering
      for (int edge = 0; edge < xfldGroup.nElem(); edge++)
      {
        xfldGroup.associativity( edge ).getNodeGlobalMapping( nodeMap, 2 );
        nodeMap[0] = nodeNumber.at(nodeMap[0]);
        nodeMap[1] = nodeNumber.at(nodeMap[1]);
        fldAssocFrame.setAssociativity( edge ).setNodeGlobalMapping( nodeMap, 2 );
      }

      int localGroup = localBoundaryFrameGroups_.at(group);
      // allocate group
      boundaryFrameGroups_[localGroup] = new FieldFrameGroupType( fldAssocFrame );
      boundaryFrameGroups_[localGroup]->setDOF( DOF_, nDOF_ );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "Field_CG_BoundaryFrame<PhysDim, TopoD3, T>::ctor: Unknown line element topology" );
  }
}

//----------------------------------------------------------------------------//
template<class PhysDim, class T>
template<class XFieldType>
void
Field_CG_BoundaryFrame<PhysDim, TopoD3, T>::resizeBoundaryFrameGroups( const XFieldType& xfld, const std::vector<int>& BoundaryFrameGroups )
{
  this->resizeBoundaryFrameGroups(BoundaryFrameGroups.size());
  localBoundaryFrameGroups_.clear();
  localBoundaryFrameGroups_.resize(xfld.nBoundaryFrameGroups(), -1);
  for (std::size_t group = 0; group < BoundaryFrameGroups.size(); group++)
    localBoundaryFrameGroups_[BoundaryFrameGroups[group]] = group;
}


//----------------------------------------------------------------------------//
template <class PhysDim, class T>
void
Field_CG_BoundaryFrame<PhysDim, TopoD3, T>::resizeBoundaryFrameGroups( const int nBoundaryFrameGroups )
{
  SANS_ASSERT( nBoundaryFrameGroups >= 0 );

  for (int i = 0; i < nBoundaryFrameGroups_; i++ )
    delete boundaryFrameGroups_[i];

  delete [] boundaryFrameGroups_;
  boundaryFrameGroups_= NULL;

  nBoundaryFrameGroups_ = nBoundaryFrameGroups;

  boundaryFrameGroups_ = new FieldFrameGroupType*[nBoundaryFrameGroups];

  for (int i = 0; i < nBoundaryFrameGroups_; i++ )
    boundaryFrameGroups_[i] = NULL;
}

//----------------------------------------------------------------------------//
template<class PhysDim, class T>
const typename Field_CG_BoundaryFrame<PhysDim, TopoD3, T>::FieldFrameGroupType&
Field_CG_BoundaryFrame<PhysDim, TopoD3, T>::getBoundaryFrameGroupGlobal( const int groupGlobal ) const
{
  int groupLocal = localBoundaryFrameGroups_.at(groupGlobal);
  SANS_ASSERT( groupLocal >= 0 );
  return getBoundaryFrameGroup(groupLocal);
}

//----------------------------------------------------------------------------//
template <class PhysDim, class T>
Field_CG_BoundaryFrame<PhysDim, TopoD3, T>::~Field_CG_BoundaryFrame()
{
  for (int i = 0; i < nBoundaryFrameGroups_; i++ )
    delete boundaryFrameGroups_[i];

  delete [] boundaryFrameGroups_;
}


}

#endif  // FIELDVOLUME_CG_BOUNDARYFRAME_H
