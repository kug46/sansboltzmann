// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(FIELDAREA_CG_CELL_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include <algorithm> // std::find

#include "FieldArea_CG_Cell.h"

#include "BasisFunction/BasisFunctionCategory.h"

#include "Field_CG/Field_CG_Topology.h"
#include "Field_CG/Field_CG_CellConstructor.h"

#include "tools/split_cat_std_vector.h"

namespace SANS
{

template <class PhysDim, class T>
Field_CG_Cell<PhysDim, TopoD2, T>::Field_CG_Cell( const XField<PhysDim, TopoD2>& xfld_,
                                                  const int order, const BasisFunctionCategory& category,
                                                  const embeddedType eType)  :
                                                  BaseType( xfld_ ), needsEmbeddedGhosts_(eType==EmbeddedCGField)
{
  // By default all boundary groups are used to construct lagrange multipliers
  init(order, category, {BaseType::createCellGroupIndex()} );
}

template <class PhysDim, class T>
Field_CG_Cell<PhysDim, TopoD2, T>::Field_CG_Cell( const XField<PhysDim, TopoD2>& xfld_,
                                                  const int order, const BasisFunctionCategory& category,
                                                  const std::vector<int>& CellGroups,
                                                  const embeddedType eType )  :
                                                  BaseType( xfld_ ), needsEmbeddedGhosts_(eType==EmbeddedCGField)
{
  // Check that the groups asked for are within the range of available groups
  BaseType::checkCellGroupIndex( CellGroups );
  init(order, category, {CellGroups} );
}

template <class PhysDim, class T>
Field_CG_Cell<PhysDim, TopoD2, T>::Field_CG_Cell( const std::vector<std::vector<int>>& CellGroupSets,
                                                  const XField<PhysDim, TopoD2>& xfld_,
                                                  const int order, const BasisFunctionCategory& category,
                                                  const embeddedType eType )  :
                                                  BaseType( xfld_ ), needsEmbeddedGhosts_(eType==EmbeddedCGField)
{
  // Check that the groups asked for are within the range of available groups
  for (auto it = CellGroupSets.begin(); it != CellGroupSets.end(); ++it)
  {
    BaseType::checkCellGroupIndex( *it );
    // Check that no cell group occurs twice
    for (auto it2 = std::next(it,1); it2 != CellGroupSets.end(); ++it2) // search the upper triangle of macro set
      SANS_ASSERT_MSG( std::find_first_of(it->begin(),it->end(),it2->begin(),it2->end())== it->end() , "Cell groups can only be in one macro group");
  }
  init(order, category, CellGroupSets);
}

template <class PhysDim, class T>
Field_CG_Cell<PhysDim, TopoD2, T>::Field_CG_Cell( const Field_CG_Cell& fld, const FieldCopy&tag ) :
BaseType(fld, tag), needsEmbeddedGhosts_(fld.needsEmbeddedGhosts()) {}

template <class PhysDim, class T>
Field_CG_Cell<PhysDim, TopoD2, T>& Field_CG_Cell<PhysDim, TopoD2, T>::operator=( const ArrayQ& q ) { BaseType::operator=(q); return *this; }

/** init initializes a Field_CG_Cell object!
 *
 * init creates and initializes a Field_CG_Cell object, building a constructor from the xfld,
 * populating the constructor correctly, and filling out the object with references to the
 * objects that were created in the constructor.
 */
template <class PhysDim, class T>
void
Field_CG_Cell<PhysDim, TopoD2, T>::init( const int order, const BasisFunctionCategory& category,
                                         const std::vector<std::vector<int>>& CellGroupSets )
{
  // doesn't make physical sense to have order < 1
  SANS_ASSERT_MSG( order >= 1, "CG area requires order >= 1" );
  // for CG, use basis functions that have sensible DOF connectivities
  SANS_ASSERT_MSG( category == BasisFunctionCategory_Hierarchical ||
                   category == BasisFunctionCategory_Lagrange,
                   "CG area must use Hierarchical or Lagrange Basis" );

  // create field constructor -> which carries _all_ the info needed to build a field, not just
  // the info needed to build one _cell_ of a field!
  Field_CG_CellConstructor<PhysDim, TopoD2> fldConstructor(xfld_, order, category, CellGroupSets, needsEmbeddedGhosts_);

  // grab the number of elements
  nElem_ = fldConstructor.nElem();

  // allocate the solution DOF array
  this->resizeDOF(fldConstructor.nDOF());

  // flatten the cell groups sets into one monolihic list of cell groups
  std::vector<int> ConcatCellGroups = cat(CellGroupSets);

  // allocate the area groups; set nodal & cell DOF associativity; set edge signs from grid
  this->resizeCellGroups( ConcatCellGroups );

  // loop over the cell group sets
  for (std::size_t i = 0; i < CellGroupSets.size(); i++)
  {
    // the current cell group set
    const std::vector<int>& CellGroups = CellGroupSets[i];
    // loop over the cell groups in this set!
    for (std::size_t igroup = 0; igroup < CellGroups.size(); igroup++)
    {
      // the current global cell group
      const int group = CellGroups[igroup];
      // the current local cell group
      int localGroup = localCellGroups_.at(group);

      if ( xfld_.getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
      {
        // if we're doing a triangle, typedef the details away
        typedef typename Field<PhysDim, TopoD2, T>::template FieldCellGroupType<Triangle> FieldCellGroupClass;

        // create the local cell group using the field constructor!
        cellGroups_[localGroup] = fldConstructor.template createCellGroup<FieldCellGroupClass>( group, this->local2nativeDOFmap_);
        // set the degrees of freedom for the cell groups
        cellGroups_[localGroup]->setDOF( DOF_, nDOF_ );
      }
      else if ( xfld_.getCellGroupBase(group).topoTypeID() == typeid(Quad) )
      {
        // if we're doing a quadrilateral, typedef the details away
        typedef typename Field<PhysDim, TopoD2, T>::template FieldCellGroupType<Quad> FieldCellGroupClass;

        // create the local cell group using the field constructor!
        cellGroups_[localGroup] = fldConstructor.template createCellGroup<FieldCellGroupClass>( group, this->local2nativeDOFmap_);
        // set the degrees of freedom for the cell groups
        cellGroups_[localGroup]->setDOF( DOF_, nDOF_ );
      }
      else
      {
        SANS_DEVELOPER_EXCEPTION( "Unknown element topology." );
      }
    }
  }

  // TODO: Decide if we really need trace groups in a Cell field. I don't think so...
  // Interior Traces are complicated by the internal boundaries introduced by CellGroupSets (some traces have multiple dofs).
  // As such, only adding in boundary trace information for the time being.
  // This is what will be necessary for setting essential bcs -- Hugh
  // How to figure

  // figure out the number of boundary trace groups necessary
  std::vector<int> boundaryTraceGroups = {};
  for (int group = 0; group < xfld_.nBoundaryTraceGroups(); group++)
  {
    int cellGroupL = xfld_.getBoundaryTraceGroupBase(group).getGroupLeft();
    const bool internal_L ( std::find(ConcatCellGroups.begin(), ConcatCellGroups.end(), cellGroupL)
                            != ConcatCellGroups.end() );
    if (internal_L)
      boundaryTraceGroups.push_back(group);
  }

  // allocate the data structure to store the boundary trace groups
  this->resizeBoundaryTraceGroups( boundaryTraceGroups );

  // need to loop macros separately, as all of fldAssocAreas must be built first
  // loop over the cell group sets
  for (std::size_t i = 0; i < CellGroupSets.size(); i++)
  {
    // this current cell group set
    const std::vector<int>& CellGroups = CellGroupSets[i];

    // loop over the boundary trace groups that exist
    for (int group = 0; group < xfld_.nBoundaryTraceGroups(); group++)
    {
      // the left cell group on this boundary trace group
      int cellGroupL = xfld_.getBoundaryTraceGroupBase(group).getGroupLeft();

      // is left cell group is in current cell groups??
      const bool internal_L ( std::find(CellGroups.begin(), CellGroups.end(), cellGroupL) != CellGroups.end() );

      // in other words, if it's internal....
      if (internal_L)
      {
        // the current boundary trace group
        int localGroup = localBoundaryTraceGroups_.at(group);

        if ( xfld_.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Line) )
        {
          // if the trace is a line, typedef away the details
          typedef typename BaseType::template FieldTraceGroupType<Line> FieldTraceGroupClass;

          // allocate the boundary trace group
          boundaryTraceGroups_[localGroup] = fldConstructor.template createBoundaryTraceGroup<FieldTraceGroupClass>( group );
          // set in the DOFs
          boundaryTraceGroups_[localGroup]->setDOF( DOF_, nDOF_ );
        }
        else
          SANS_DEVELOPER_EXCEPTION( "Unknown boundary-trace topology." );
      }
      // otherwise skip any boundary trace group that is not connected to the requested cell groups

    }
  }

  // get the ghost/zombie DOF ranks
  // this must be last as it uses this->local2nativeDOFmap_
  this->nDOFpossessed_ = fldConstructor.nDOFpossessed();
  this->nDOFghost_     = fldConstructor.nDOFghost();

  this->resizeDOF_rank(fldConstructor.nDOF(), fldConstructor.nDOFpossessed());

  fldConstructor.getDOF_rank(this->DOFghost_rank_);
}

}
