// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDSUBGROUP_H
#define FIELDSUBGROUP_H

#include "FieldBase.h"
#include "FieldTypes.h"
#include "XField.h"

#include <vector>

namespace SANS
{

//----------------------------------------------------------------------------//
// Structures so that a field can exist in subset of groups in an XField
//----------------------------------------------------------------------------//

template<class PhysDim, class FieldTraits>
class FieldSubGroup : public FieldBase< FieldTraits >
{
public:
  typedef FieldBase< FieldTraits > BaseType;
  typedef typename FieldTraits::T T;
  typedef typename FieldTraits::TopoDim TopoDim;
  typedef XField<PhysDim, TopoDim> XFieldType;
  typedef typename BaseType::FieldTraceGroupBase FieldTraceGroupBase;
  typedef typename BaseType::FieldCellGroupBase FieldCellGroupBase;

  template<class Topology>
  using FieldTraceGroupType = typename BaseType::template FieldTraceGroupType<Topology>;

  template<class Topology>
  using FieldCellGroupType = typename BaseType::template FieldCellGroupType<Topology>;

  FieldSubGroup( const FieldSubGroup& ) = delete;
  FieldSubGroup& operator=( const FieldSubGroup& ) = delete;

  // cppcheck-suppress noExplicitConstructor
  FieldSubGroup( const XField<PhysDim, TopoDim>& xfld ) : xfld_(xfld) {};
  FieldSubGroup( const FieldSubGroup& fld, const FieldCopy& tag )
    : BaseType(fld, tag),
      xfld_(fld.xfld_),
      localInteriorTraceGroups_(fld.localInteriorTraceGroups_),
      localBoundaryTraceGroups_(fld.localBoundaryTraceGroups_),
      localCellGroups_(fld.localCellGroups_),
      globalInteriorTraceGroups_(fld.globalInteriorTraceGroups_),
      globalBoundaryTraceGroups_(fld.globalBoundaryTraceGroups_),
      globalCellGroups_(fld.globalCellGroups_)
  {}

  //Constructor for local field from global field (given the local mesh)
  ~FieldSubGroup() {}

  FieldSubGroup& operator=( const T& q );
  void cloneFrom( const FieldSubGroup& fld );

  // communicator accessor
  std::shared_ptr<mpi::communicator> comm() const { return xfld_.comm(); }

  const XField<PhysDim, TopoDim>& getXField() const { return xfld_; }

  // group accessors
        FieldTraceGroupBase& getHubTraceGroupBaseGlobal( const int groupGlobal );
  const FieldTraceGroupBase& getHubTraceGroupBaseGlobal( const int groupGlobal ) const;

        FieldTraceGroupBase& getInteriorTraceGroupBaseGlobal( const int groupGlobal );
  const FieldTraceGroupBase& getInteriorTraceGroupBaseGlobal( const int groupGlobal ) const;

        FieldTraceGroupBase& getBoundaryTraceGroupBaseGlobal( const int groupGlobal );
  const FieldTraceGroupBase& getBoundaryTraceGroupBaseGlobal( const int groupGlobal ) const;

        FieldCellGroupBase& getCellGroupBaseGlobal( const int groupGlobal );
  const FieldCellGroupBase& getCellGroupBaseGlobal( const int groupGlobal ) const;


  template<class Topology>
        FieldTraceGroupType<Topology>& getHubTraceGroupGlobal( const int groupGlobal );
  template<class Topology>
  const FieldTraceGroupType<Topology>& getHubTraceGroupGlobal( const int groupGlobal ) const;

  template<class Topology>
        FieldTraceGroupType<Topology>& getInteriorTraceGroupGlobal( const int groupGlobal );
  template<class Topology>
  const FieldTraceGroupType<Topology>& getInteriorTraceGroupGlobal( const int groupGlobal ) const;

  template<class Topology>
        FieldTraceGroupType<Topology>& getBoundaryTraceGroupGlobal( const int groupGlobal );
  template<class Topology>
  const FieldTraceGroupType<Topology>& getBoundaryTraceGroupGlobal( const int groupGlobal ) const;

  template<class Topology>
        FieldCellGroupType<Topology>& getCellGroupGlobal( const int groupGlobal );
  template<class Topology>
  const FieldCellGroupType<Topology>& getCellGroupGlobal( const int groupGlobal ) const;

  int getLocalHubTraceGroupMap     (const int groupGlobal) const { return localHubTraceGroups_.at(groupGlobal); }
  int getLocalInteriorTraceGroupMap(const int groupGlobal) const { return localInteriorTraceGroups_.at(groupGlobal); }
  int getLocalBoundaryTraceGroupMap(const int groupGlobal) const { return localBoundaryTraceGroups_.at(groupGlobal); }
  int getLocalCellGroupMap         (const int groupGlobal) const { return localCellGroups_.at(groupGlobal); }

  int getGlobalHubTraceGroupMap     (const int groupLocal) const { return globalHubTraceGroups_.at(groupLocal); }
  int getGlobalInteriorTraceGroupMap(const int groupLocal) const { return globalInteriorTraceGroups_.at(groupLocal); }
  int getGlobalBoundaryTraceGroupMap(const int groupLocal) const { return globalBoundaryTraceGroups_.at(groupLocal); }
  int getGlobalCellGroupMap         (const int groupLocal) const { return globalCellGroups_.at(groupLocal); }

  std::vector<int> getLocalHubTraceGroups() const      { return localHubTraceGroups_; }
  std::vector<int> getLocalInteriorTraceGroups() const { return localInteriorTraceGroups_; }
  std::vector<int> getLocalBoundaryTraceGroups() const { return localBoundaryTraceGroups_; }
  std::vector<int> getLocalCellGroups() const          { return localCellGroups_; }

  std::vector<int> getGlobalHubTraceGroups() const      { return globalHubTraceGroups_; }
  std::vector<int> getGlobalInteriorTraceGroups() const { return globalInteriorTraceGroups_; }
  std::vector<int> getGlobalBoundaryTraceGroups() const { return globalBoundaryTraceGroups_; }
  std::vector<int> getGlobalCellGroups() const          { return globalCellGroups_; }

protected:
  const XField<PhysDim, TopoDim>& xfld_;

  // Field groups can be constructed with a subset of the global XField groups.
  // These are maps for taking a global group index to a local subset index.
  std::vector<int> localHubTraceGroups_;
  std::vector<int> localInteriorTraceGroups_;
  std::vector<int> localBoundaryTraceGroups_;
  std::vector<int> localCellGroups_;

  // These are maps for taking a local subset index to a global group index
  std::vector<int> globalHubTraceGroups_;
  std::vector<int> globalInteriorTraceGroups_;
  std::vector<int> globalBoundaryTraceGroups_;
  std::vector<int> globalCellGroups_;

  using BaseType::resizeHubTraceGroups;
  using BaseType::resizeInteriorTraceGroups;
  using BaseType::resizeBoundaryTraceGroups;
  using BaseType::resizeCellGroups;

  // Resize the field based on the subset of groups and generate the global to local mappings
  void resizeHubTraceGroups( const std::vector<int>& HubTraceGroups );
  void resizeInteriorTraceGroups( const std::vector<int>& InteriorTraceGroups );
  void resizeBoundaryTraceGroups( const std::vector<int>& BoundaryTraceGroups );
  void resizeCellGroups( const std::vector<int>& CellGroups );

  std::vector<int> createHubGroupIndex();
  void checkHubGroupIndex(const std::vector<int>& InteriorGroups);

  std::vector<int> createInteriorGroupIndex();
  void checkInteriorGroupIndex(const std::vector<int>& InteriorGroups);

  std::vector<int> createBoundaryGroupIndex();
  void checkBoundaryGroupIndex(const std::vector<int>& BoundaryGroups);

  std::vector<int> createCellGroupIndex();
  void checkCellGroupIndex(const std::vector<int>& CellGroups);
};


//----------------------------------------------------------------------------//
template<class PhysDim, class FieldTraits>
template<class Topology>
typename FieldSubGroup<PhysDim, FieldTraits>::template FieldTraceGroupType<Topology>&
FieldSubGroup<PhysDim, FieldTraits>::getHubTraceGroupGlobal( const int groupGlobal )
{
  SANS_ASSERT_MSG(groupGlobal >= 0 && groupGlobal < (int)localHubTraceGroups_.size(),
                  "localHubTraceGroups_.size() = %d, groupGlobal = %d", localHubTraceGroups_.size(), groupGlobal);
  int groupLocal = localHubTraceGroups_.at(groupGlobal);
  SANS_ASSERT( groupLocal >= 0 );
  return BaseType::template getHubTraceGroup<Topology>(groupLocal);
}
template<class PhysDim, class FieldTraits>
template<class Topology>
const typename FieldSubGroup<PhysDim, FieldTraits>::template FieldTraceGroupType<Topology>&
FieldSubGroup<PhysDim, FieldTraits>::getHubTraceGroupGlobal( const int groupGlobal ) const
{
  SANS_ASSERT_MSG(groupGlobal >= 0 && groupGlobal < (int)localHubTraceGroups_.size(),
                  "localHubTraceGroups_.size() = %d, groupGlobal = %d", localHubTraceGroups_.size(), groupGlobal);
  int groupLocal = localHubTraceGroups_.at(groupGlobal);
  SANS_ASSERT( groupLocal >= 0 );
  return BaseType::template getHubTraceGroup<Topology>(groupLocal);
}


//----------------------------------------------------------------------------//
template<class PhysDim, class FieldTraits>
template<class Topology>
typename FieldSubGroup<PhysDim, FieldTraits>::template FieldTraceGroupType<Topology>&
FieldSubGroup<PhysDim, FieldTraits>::getInteriorTraceGroupGlobal( const int groupGlobal )
{
  SANS_ASSERT_MSG(groupGlobal >= 0 && groupGlobal < (int)localInteriorTraceGroups_.size(),
                  "localInteriorTraceGroups_.size() = %d, groupGlobal = %d", localInteriorTraceGroups_.size(), groupGlobal);
  int groupLocal = localInteriorTraceGroups_.at(groupGlobal);
  SANS_ASSERT( groupLocal >= 0 );
  return BaseType::template getInteriorTraceGroup<Topology>(groupLocal);
}
template<class PhysDim, class FieldTraits>
template<class Topology>
const typename FieldSubGroup<PhysDim, FieldTraits>::template FieldTraceGroupType<Topology>&
FieldSubGroup<PhysDim, FieldTraits>::getInteriorTraceGroupGlobal( const int groupGlobal ) const
{
  SANS_ASSERT_MSG(groupGlobal >= 0 && groupGlobal < (int)localInteriorTraceGroups_.size(),
                  "localInteriorTraceGroups_.size() = %d, groupGlobal = %d", localInteriorTraceGroups_.size(), groupGlobal);
  int groupLocal = localInteriorTraceGroups_.at(groupGlobal);
  SANS_ASSERT( groupLocal >= 0 );
  return BaseType::template getInteriorTraceGroup<Topology>(groupLocal);
}


//----------------------------------------------------------------------------//
template<class PhysDim, class FieldTraits>
template<class Topology>
typename FieldSubGroup<PhysDim, FieldTraits>::template FieldTraceGroupType<Topology>&
FieldSubGroup<PhysDim, FieldTraits>::getBoundaryTraceGroupGlobal( const int groupGlobal )
{
  SANS_ASSERT_MSG(groupGlobal >= 0 && groupGlobal < (int)localBoundaryTraceGroups_.size(),
                  "localBoundaryTraceGroups_.size() = %d, groupGlobal = %d", localBoundaryTraceGroups_.size(), groupGlobal);
  int groupLocal = localBoundaryTraceGroups_.at(groupGlobal);
  SANS_ASSERT( groupLocal >= 0 );
  return BaseType::template getBoundaryTraceGroup<Topology>(groupLocal);
}
template<class PhysDim, class FieldTraits>
template<class Topology>
const typename FieldSubGroup<PhysDim, FieldTraits>::template FieldTraceGroupType<Topology>&
FieldSubGroup<PhysDim, FieldTraits>::getBoundaryTraceGroupGlobal( const int groupGlobal ) const
{
  SANS_ASSERT_MSG(groupGlobal >= 0 && groupGlobal < (int)localBoundaryTraceGroups_.size(),
                  "localBoundaryTraceGroups_.size() = %d, groupGlobal = %d", localBoundaryTraceGroups_.size(), groupGlobal);
  int groupLocal = localBoundaryTraceGroups_.at(groupGlobal);
  SANS_ASSERT( groupLocal >= 0 );
  return BaseType::template getBoundaryTraceGroup<Topology>(groupLocal);
}


//----------------------------------------------------------------------------//
template<class PhysDim, class FieldTraits>
template<class Topology>
typename FieldSubGroup<PhysDim, FieldTraits>::template FieldCellGroupType<Topology>&
FieldSubGroup<PhysDim, FieldTraits>::getCellGroupGlobal( const int groupGlobal )
{
  SANS_ASSERT_MSG(groupGlobal >= 0 && groupGlobal < (int)localCellGroups_.size(),
                  "localCellGroups_.size() = %d, groupGlobal = %d", localCellGroups_.size(), groupGlobal);
  int groupLocal = localCellGroups_.at(groupGlobal);
  SANS_ASSERT( groupLocal >= 0 );
  return BaseType::template getCellGroup<Topology>(groupLocal);
}
template<class PhysDim, class FieldTraits>
template<class Topology>
const typename FieldSubGroup<PhysDim, FieldTraits>::template FieldCellGroupType<Topology>&
FieldSubGroup<PhysDim, FieldTraits>::getCellGroupGlobal( const int groupGlobal ) const
{
  SANS_ASSERT_MSG(groupGlobal >= 0 && groupGlobal < (int)localCellGroups_.size(),
                  "localCellGroups_.size() = %d, groupGlobal = %d", localCellGroups_.size(), groupGlobal);
  int groupLocal = localCellGroups_.at(groupGlobal);
  SANS_ASSERT( groupLocal >= 0 );
  return BaseType::template getCellGroup<Topology>(groupLocal);
}

} //namespace SANS

#endif //FIELDSUBGROUP_H
