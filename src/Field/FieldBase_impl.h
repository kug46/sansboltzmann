// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(FIELDBASE_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "FieldBase.h"

namespace SANS
{

template <class TopoDim>
class FieldBase_ProjectTo;

//----------------------------------------------------------------------------//
template <class FieldTraits>
FieldBase<FieldTraits>::~FieldBase()
{
  deallocate();
}


//----------------------------------------------------------------------------//
template <class FieldTraits>
void
FieldBase<FieldTraits>::deallocate()
{
  nElem_ = 0;
  nDOF_ = 0;
  delete [] DOF_; DOF_ = nullptr;
  delete [] local2nativeDOFmap_; local2nativeDOFmap_ = nullptr;

  hubTraceGroups_.resize(0);
  interiorTraceGroups_.resize(0);
  boundaryTraceGroups_.resize(0);
  cellGroups_.resize(0);
}


//----------------------------------------------------------------------------//
template <class FieldTraits>
void
FieldBase<FieldTraits>::resizeDOF( const int nDOF )
{
  nDOF_ = nDOF;
  delete [] DOF_; DOF_ = nullptr;
  delete [] local2nativeDOFmap_; local2nativeDOFmap_ = nullptr;

  SANS_ASSERT( nDOF >= 0 );

  DOF_ = new T[nDOF];

  for (int i= 0; i < nDOF; i++)
    DOF_[i]= 0.0;

  local2nativeDOFmap_ = new int[nDOF];

  // initialize the local to native mapping as an identity
  for (int i = 0; i < nDOF; i++)
    local2nativeDOFmap_[i] = i;
}


//----------------------------------------------------------------------------//
template <class FieldTraits>
void
FieldBase<FieldTraits>::resizeHubTraceGroups( const int nHubTraceGroups )
{
  hubTraceGroups_.resize(nHubTraceGroups);
}


//----------------------------------------------------------------------------//
template <class FieldTraits>
void
FieldBase<FieldTraits>::resizeInteriorTraceGroups( const int nInteriorTraceGroups )
{
  interiorTraceGroups_.resize(nInteriorTraceGroups);
}


//----------------------------------------------------------------------------//
template <class FieldTraits>
void
FieldBase<FieldTraits>::resizeBoundaryTraceGroups( const int nBoundaryTraceGroups )
{
  boundaryTraceGroups_.resize(nBoundaryTraceGroups);
}


//----------------------------------------------------------------------------//
template <class FieldTraits>
void
FieldBase<FieldTraits>::resizeCellGroups( const int nCellGroups )
{
  cellGroups_.resize(nCellGroups);
}


//----------------------------------------------------------------------------//
template <class FieldTraits>
void
FieldBase<FieldTraits>::cloneFrom( const FieldBase& fld )
{
  if (this != &fld)
  {
    deallocate();

    nElem_ = fld.nElem_;
    resizeDOF( fld.nDOF_ );
    resizeHubTraceGroups( fld.nHubTraceGroups() );
    resizeInteriorTraceGroups( fld.nInteriorTraceGroups() );
    resizeBoundaryTraceGroups( fld.nBoundaryTraceGroups() );
    resizeCellGroups( fld.nCellGroups() );

    const int nDOF = nDOF_;
    for (int n = 0; n < nDOF; n++)
    {
      DOF_[n] = fld.DOF_[n];
      local2nativeDOFmap_[n] = fld.local2nativeDOFmap_[n];
    }

    for (int n = 0; n < nHubTraceGroups(); n++)
    {
      if ( fld.hubTraceGroups_[n] == nullptr ) continue;
      hubTraceGroups_[n] = fld.hubTraceGroups_[n]->clone(); // NOTE: invokes new []
      hubTraceGroups_[n]->setDOF(DOF_, nDOF_);
    }

    for (int n = 0; n < nInteriorTraceGroups(); n++)
    {
      if ( fld.interiorTraceGroups_[n] == nullptr ) continue;
      interiorTraceGroups_[n] = fld.interiorTraceGroups_[n]->clone(); // NOTE: invokes new []
      interiorTraceGroups_[n]->setDOF(DOF_, nDOF_);
    }

    for (int n = 0; n < nBoundaryTraceGroups(); n++)
    {
      if ( fld.boundaryTraceGroups_[n] == nullptr ) continue;
      boundaryTraceGroups_[n] = fld.boundaryTraceGroups_[n]->clone(); // NOTE: invokes new []
      boundaryTraceGroups_[n]->setDOF(DOF_, nDOF_);
    }

    for (int n = 0; n < nCellGroups(); n++)
    {
      if ( fld.cellGroups_[n] == nullptr ) continue;
      cellGroups_[n] = fld.cellGroups_[n]->clone(); // NOTE: invokes new []
      cellGroups_[n]->setDOF(DOF_, nDOF_);
    }
  }
}


//----------------------------------------------------------------------------//
template <class FieldTraits>
FieldBase<FieldTraits>&
FieldBase<FieldTraits>::operator=( const T& q )
{
  for (int n = 0; n < nDOF_; n++)
    DOF_[n] = q;

  return *this;
}


//----------------------------------------------------------------------------//
template <class FieldTraits>
void
FieldBase<FieldTraits>::projectTo( FieldBase& fld ) const
{

  //Nothing to do if projecting onto this...
  if (this != &fld)
  {
    if (this->nCellGroups() > 0)
    {
      SANS_ASSERT( this->nCellGroups() == fld.nCellGroups() );
    }
    else if ( this->nBoundaryTraceGroups() > 0)
    {
      SANS_ASSERT( this->nBoundaryTraceGroups() <= fld.nBoundaryTraceGroups() ||
                   fld.nBoundaryTraceGroups() == 0 );
    }
    else if ( this->nInteriorTraceGroups() > 0)
    {
      SANS_ASSERT( this->nInteriorTraceGroups() <= fld.nInteriorTraceGroups() ||
                   fld.nInteriorTraceGroups() == 0 );
    }

    FieldBase_ProjectTo<TopoDim>::projectTo(*this, fld);
  }
}


//----------------------------------------------------------------------------//
template <class FieldTraits>
void
FieldBase<FieldTraits>::dump( int indentSize, std::ostream& out ) const
{
  int n;
  std::string indent(indentSize, ' ');
  out << indent << "FieldBase<FieldTraits>:"
      << "  nDOF_ = " << nDOF_
      << "  nCellGroups_ = " << nCellGroups()
      << "  nInteriorTraceGroups_ = " << nInteriorTraceGroups()
      << "  nBoundaryTraceGroups_ = " << nBoundaryTraceGroups()
      << std::endl;
  out << indent << "  DOF_[] = ";
  for (n = 0; n < nDOF_; n++)
    out << "(" << DOF_[n] << ") ";
  out << std::endl;
  out << indent << "  local2nativeDOFmap_[] = ";
  for (n = 0; n < nDOF_; n++)
    out << "(" << n << ", " << local2nativeDOFmap_[n] << ") ";
  out << std::endl;
  for (n = 0; n < nCellGroups(); n++)
  {
    out << indent << "elemCellGroups_[" << n << "] = " << std::endl;
    if ( cellGroups_[n] != nullptr )
      cellGroups_[n]->dump(indentSize + 2, out);
    else
      out << "NULL" << std::endl;
  }
  for (n = 0; n < nInteriorTraceGroups(); n++)
  {
    out << indent << "elemInteriorTraceGroups_[" << n << "] = " << std::endl;
    if ( interiorTraceGroups_[n] != nullptr )
      interiorTraceGroups_[n]->dump(indentSize + 2, out);
    else
      out << "NULL" << std::endl;
  }
  for (n = 0; n < nBoundaryTraceGroups(); n++)
  {
    out << indent << "elemBoundaryTraceGroups_[" << n << "] = " << std::endl;
    if ( boundaryTraceGroups_[n] != nullptr )
      boundaryTraceGroups_[n]->dump(indentSize + 2, out);
    else
      out << "NULL" << std::endl;
  }
  for (n = 0; n < nHubTraceGroups(); n++)
  {
    out << indent << "elemHubTraceGroups_[" << n << "] = " << std::endl;
    if ( hubTraceGroups_[n] != nullptr )
      hubTraceGroups_[n]->dump(indentSize + 2, out);
    else
      out << "NULL" << std::endl;
  }
}

//----------------------------------------------------------------------------//
template <>
class FieldBase_ProjectTo<TopoD1>
{
public:
  //----------------------------------------------------------------------------//
  template <class FieldTraits>
  static void
  projectTo( const FieldBase<FieldTraits>& fldFrom, FieldBase<FieldTraits>& fldTo )
  {

    //Zero out the DOF in the receiving field
    fldTo = 0;

    // loop interior trace groups
    for (int group = 0; group < std::min(fldFrom.nInteriorTraceGroups(),fldTo.nInteriorTraceGroups()); group++)
    {
      if ( (fldFrom.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Node)) &&
           (  fldTo.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Node)) )
      {
        fldFrom.template getInteriorTraceGroup<Node>(group).projectTo(fldTo.template getInteriorTraceGroup<Node>(group));
      }
      else
      {
        char msg[] = "Error in FieldBase<FieldTraits>::projectTo: mixed or unknown topology.\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }

    // loop boundary trace groups
    for (int group = 0; group < std::min(fldFrom.nBoundaryTraceGroups(),fldTo.nBoundaryTraceGroups()); group++)
    {
      if ( (fldFrom.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Node)) &&
           (  fldTo.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Node)) )
      {
        fldFrom.template getBoundaryTraceGroup<Node>(group).projectTo(fldTo.template getBoundaryTraceGroup<Node>(group));
      }
      else
      {
        char msg[] = "Error in FieldBase<FieldTraits>::projectTo: mixed or unknown topology.\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }

    // loop hub trace groups
    for (int group = 0; group < std::min(fldFrom.nHubTraceGroups(),fldTo.nHubTraceGroups()); group++)
    {
      if ( (fldFrom.getHubTraceGroupBase(group).topoTypeID() == typeid(Node)) &&
           (  fldTo.getHubTraceGroupBase(group).topoTypeID() == typeid(Node)) )
      {
        fldFrom.template getHubTraceGroup<Node>(group).projectTo(fldTo.template getHubTraceGroup<Node>(group));
      }
      else
      {
        char msg[] = "Error in FieldBase<FieldTraits>::projectTo: mixed or unknown topology.\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }

    // loop over cell groups
    for (int group = 0; group < fldFrom.nCellGroups(); group++)
    {
      // dispatch projection over elements of group
      if ( (fldFrom.getCellGroupBase(group).topoTypeID() == typeid(Line)) &&
           (  fldTo.getCellGroupBase(group).topoTypeID() == typeid(Line)) )
      {
        fldFrom.template getCellGroup<Line>(group).projectTo(fldTo.template getCellGroup<Line>(group));
      }
      else
      {
        char msg[] = "Error in FieldBase<FieldTraits>::projectTo: mixed or unknown topology.\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }
  }
};

//----------------------------------------------------------------------------//
template <>
class FieldBase_ProjectTo<TopoD2>
{
public:
  //----------------------------------------------------------------------------//
  template <class FieldTraits>
  static void
  projectTo( const FieldBase<FieldTraits>& fldFrom, FieldBase<FieldTraits>& fldTo )
  {
    //Zero out the DOF in the receiving field
    fldTo = 0;

    // loop interior trace groups
    for (int group = 0; group < std::min(fldFrom.nInteriorTraceGroups(),fldTo.nInteriorTraceGroups()); group++)
    {
      if ( (fldFrom.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Line)) &&
           (  fldTo.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Line)) )
      {
        fldFrom.template getInteriorTraceGroup<Line>(group).projectTo(fldTo.template getInteriorTraceGroup<Line>(group));
      }
      else
      {
        char msg[] = "Error in FieldBase<FieldTraits>::projectTo: mixed or unknown topology.\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }

    // loop boundary trace groups
    for (int group = 0; group < std::min(fldFrom.nBoundaryTraceGroups(),fldTo.nBoundaryTraceGroups()); group++)
    {
      if ( (fldFrom.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Line)) &&
           (  fldTo.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Line)) )
      {
        fldFrom.template getBoundaryTraceGroup<Line>(group).projectTo(fldTo.template getBoundaryTraceGroup<Line>(group));
      }
      else
      {
        char msg[] = "Error in FieldBase<FieldTraits>::projectTo: mixed or unknown topology.\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }

    // loop hub trace groups
    for (int group = 0; group < std::min(fldFrom.nHubTraceGroups(),fldTo.nHubTraceGroups()); group++)
    {
      if ( (fldFrom.getHubTraceGroupBase(group).topoTypeID() == typeid(Line)) &&
           (  fldTo.getHubTraceGroupBase(group).topoTypeID() == typeid(Line)) )
      {
        fldFrom.template getHubTraceGroup<Line>(group).projectTo(fldTo.template getHubTraceGroup<Line>(group));
      }
      else
      {
        char msg[] = "Error in FieldBase<FieldTraits>::projectTo: mixed or unknown topology.\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }

    // loop over cell groups
    for (int group = 0; group < fldFrom.nCellGroups(); group++)
    {
      // dispatch projection over elements of group
      if ( (fldFrom.getCellGroupBase(group).topoTypeID() == typeid(Triangle)) &&
           (  fldTo.getCellGroupBase(group).topoTypeID() == typeid(Triangle)) )
      {
        fldFrom.template getCellGroup<Triangle>(group).projectTo(fldTo.template getCellGroup<Triangle>(group));
      }
      else if ( (fldFrom.getCellGroupBase(group).topoTypeID() == typeid(Quad)) &&
                (  fldTo.getCellGroupBase(group).topoTypeID() == typeid(Quad)) )
      {
        fldFrom.template getCellGroup<Quad>(group).projectTo(fldTo.template getCellGroup<Quad>(group));
      }
      else
      {
        char msg[] = "Error in FieldBase<FieldTraits>::projectTo: mixed or unknown topology.\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }
  }

};

//----------------------------------------------------------------------------//
template <>
class FieldBase_ProjectTo<TopoD3>
{
public:
  //----------------------------------------------------------------------------//
  template <class FieldTraits>
  static void
  projectTo( const FieldBase<FieldTraits>& fldFrom, FieldBase<FieldTraits>& fldTo )
  {
    //Zero out the DOF in the receiving field
    fldTo = 0;

    // loop interior trace groups
    for (int group = 0; group < std::min(fldFrom.nInteriorTraceGroups(),fldTo.nInteriorTraceGroups()); group++)
    {
      if ( (fldFrom.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Triangle)) &&
           (  fldTo.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Triangle)) )
      {
        fldFrom.template getInteriorTraceGroup<Triangle>(group).projectTo(fldTo.template getInteriorTraceGroup<Triangle>(group));
      }
      else if ( (fldFrom.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Quad)) &&
                (  fldTo.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Quad)) )
      {
        fldFrom.template getInteriorTraceGroup<Quad>(group).projectTo(fldTo.template getInteriorTraceGroup<Quad>(group));
      }
      else
      {
        char msg[] = "Error in FieldBase<FieldTraits>::projectTo: mixed or unknown topology.\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }

    // loop boundary trace groups
    for (int group = 0; group < std::min(fldFrom.nBoundaryTraceGroups(),fldTo.nBoundaryTraceGroups()); group++)
    {
      if ( (fldFrom.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Triangle)) &&
           (  fldTo.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Triangle)) )
      {
        fldFrom.template getBoundaryTraceGroup<Triangle>(group).projectTo(fldTo.template getBoundaryTraceGroup<Triangle>(group));
      }
      else if ( (fldFrom.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Quad)) &&
                (  fldTo.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Quad)) )
      {
        fldFrom.template getBoundaryTraceGroup<Quad>(group).projectTo(fldTo.template getBoundaryTraceGroup<Quad>(group));
      }
      else
      {
        char msg[] = "Error in FieldBase<FieldTraits>::projectTo: mixed or unknown topology.\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }

    // loop hub trace groups
    for (int group = 0; group < std::min(fldFrom.nHubTraceGroups(),fldTo.nHubTraceGroups()); group++)
    {
      if ( (fldFrom.getHubTraceGroupBase(group).topoTypeID() == typeid(Triangle)) &&
           (  fldTo.getHubTraceGroupBase(group).topoTypeID() == typeid(Triangle)) )
      {
        fldFrom.template getHubTraceGroup<Triangle>(group).projectTo(fldTo.template getHubTraceGroup<Triangle>(group));
      }
      else if ( (fldFrom.getHubTraceGroupBase(group).topoTypeID() == typeid(Quad)) &&
                (  fldTo.getHubTraceGroupBase(group).topoTypeID() == typeid(Quad)) )
      {
        fldFrom.template getHubTraceGroup<Quad>(group).projectTo(fldTo.template getHubTraceGroup<Quad>(group));
      }
      else
      {
        char msg[] = "Error in FieldBase<FieldTraits>::projectTo: mixed or unknown topology.\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }

    // loop over cell groups
    for (int group = 0; group < fldFrom.nCellGroups(); group++)
    {
      // dispatch projection over elements of group
      if ( (fldFrom.getCellGroupBase(group).topoTypeID() == typeid(Tet)) &&
           (  fldTo.getCellGroupBase(group).topoTypeID() == typeid(Tet)) )
      {
        fldFrom.template getCellGroup<Tet>(group).projectTo(fldTo.template getCellGroup<Tet>(group));
      }
      else if ( (fldFrom.getCellGroupBase(group).topoTypeID() == typeid(Hex)) &&
                (  fldTo.getCellGroupBase(group).topoTypeID() == typeid(Hex)) )
      {
        fldFrom.template getCellGroup<Hex>(group).projectTo(fldTo.template getCellGroup<Hex>(group));
      }
      else
      {
        char msg[] = "Error in FieldBase<FieldTraits>::projectTo: mixed or unknown topology.\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }
  }

};

//----------------------------------------------------------------------------//
template <>
class FieldBase_ProjectTo<TopoD4>
{
public:
  //----------------------------------------------------------------------------//
  template <class FieldTraits>
  static void
  projectTo( const FieldBase<FieldTraits>& fldFrom, FieldBase<FieldTraits>& fldTo )
  {
    //Zero out the DOF in the receiving field
    fldTo = 0;

    // loop interior trace groups
    for (int group = 0; group < std::min(fldFrom.nInteriorTraceGroups(),fldTo.nInteriorTraceGroups()); group++)
    {
      if ( (fldFrom.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Tet)) &&
           (  fldTo.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Tet)) )
      {
        fldFrom.template getInteriorTraceGroup<Tet>(group).projectTo(fldTo.template getInteriorTraceGroup<Tet>(group));
      }
      else
      {
        char msg[] = "Error in FieldBase<FieldTraits>::projectTo: mixed or unknown topology.\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }

    // loop boundary trace groups
    for (int group = 0; group < std::min(fldFrom.nBoundaryTraceGroups(),fldTo.nBoundaryTraceGroups()); group++)
    {
      if ( (fldFrom.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Tet)) &&
           (  fldTo.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Tet)) )
      {
        fldFrom.template getBoundaryTraceGroup<Tet>(group).projectTo(fldTo.template getBoundaryTraceGroup<Tet>(group));
      }
      else
      {
        char msg[] = "Error in FieldBase<FieldTraits>::projectTo: mixed or unknown topology.\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }

    // loop hub trace groups
    for (int group = 0; group < std::min(fldFrom.nHubTraceGroups(),fldTo.nHubTraceGroups()); group++)
    {
      if ( (fldFrom.getHubTraceGroupBase(group).topoTypeID() == typeid(Tet)) &&
           (  fldTo.getHubTraceGroupBase(group).topoTypeID() == typeid(Tet)) )
      {
        fldFrom.template getHubTraceGroup<Tet>(group).projectTo(fldTo.template getHubTraceGroup<Tet>(group));
      }
      else
      {
        char msg[] = "Error in FieldBase<FieldTraits>::projectTo: mixed or unknown topology.\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }

    // loop over cell groups
    for (int group = 0; group < fldFrom.nCellGroups(); group++)
    {
      // dispatch projection over elements of group
      if ( (fldFrom.getCellGroupBase(group).topoTypeID() == typeid(Pentatope)) &&
           (  fldTo.getCellGroupBase(group).topoTypeID() == typeid(Pentatope)) )
      {
        fldFrom.template getCellGroup<Pentatope>(group).projectTo(fldTo.template getCellGroup<Pentatope>(group));
      }
      else
      {
        char msg[] = "Error in FieldBase<FieldTraits>::projectTo: mixed or unknown topology.\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }
  }

};

}
