// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDVOLUME_H
#define FIELDVOLUME_H

#include "FieldGroupArea_Traits.h"
#include "FieldGroupVolume_Traits.h"

#include "Field.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Topologically 3D field variables
//----------------------------------------------------------------------------//

template<class Tin>
struct FieldTraits<TopoD3, Tin>
{
  typedef TopoD3 TopoDim;
  typedef Tin T;                                     // DOF type

  typedef typename FieldGroupAreaTraitsBase<T>::FieldBase FieldTraceGroupBase;
  typedef typename FieldGroupVolumeTraitsBase<T>::FieldBase FieldCellGroupBase;

  template<class Topology>
  using FieldTraceGroupType = FieldAssociativity< FieldGroupAreaTraits<Topology, T> >;

  template<class Topology>
  using FieldCellGroupType = FieldAssociativity< FieldGroupVolumeTraits<Topology, T> >;
};

} //namespace SANS

#endif //FIELDVOLUME_H
