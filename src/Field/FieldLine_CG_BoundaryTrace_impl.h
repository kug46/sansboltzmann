// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(FIELDLINE_CG_BOUNDARYTRACE_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "FieldLine_CG_BoundaryTrace.h"

#include "BasisFunction/BasisFunctionCategory.h"

#include "Field_CG/Field_CG_TraceConstructor.h"

#include "tools/split_cat_std_vector.h"

namespace SANS
{

// dummy basis so the abstraction looks the same as for higher dimensions - it just gets ignored
template <class PhysDim, class T>
Field_CG_BoundaryTrace<PhysDim, TopoD1, T>::Field_CG_BoundaryTrace( const XField<PhysDim, TopoD1>& xfld, const int order,
                                                                    const BasisFunctionCategory category ) : BaseType( xfld )
{
  // By default all boundary groups are used to construct lagrange multipliers
  init(order, category, BaseType::createBoundaryGroupIndex() );
}

// dummy basis so the abstraction looks the same as for higher dimensions - it just gets ignored
template <class PhysDim, class T>
Field_CG_BoundaryTrace<PhysDim, TopoD1, T>::Field_CG_BoundaryTrace( const XField<PhysDim, TopoD1>& xfld,
                                                                    const int order, const BasisFunctionCategory category,
                                                                    const std::vector<int>& BoundaryGroups ) : BaseType( xfld )
{
  // Check that the groups asked for are within the range of available groups
  BaseType::checkBoundaryGroupIndex( BoundaryGroups );
  init(order, category, BoundaryGroups);
}

  // Assumes that nodes are always broken groups.
  // Groups are first to remove ambiguity with list initializers
  // dummy basis so the abstraction looks the same as for higher dimensions - it just gets ignored
template <class PhysDim, class T>
Field_CG_BoundaryTrace<PhysDim, TopoD1, T>::Field_CG_BoundaryTrace( const std::vector<std::vector<int>>& BoundaryGroupSets,
                                                                    const XField<PhysDim, TopoD1>& xfld, const int order,
                                                                    const BasisFunctionCategory category ) : BaseType( xfld )
{
  std::vector<int> BoundaryGroups = SANS::cat(BoundaryGroupSets);
  init(order, category, BoundaryGroups);
}

template <class PhysDim, class T>
Field_CG_BoundaryTrace<PhysDim, TopoD1, T>::Field_CG_BoundaryTrace( const Field_CG_BoundaryTrace& fld, const FieldCopy&tag ) : BaseType(fld,tag) {}

template <class PhysDim, class T>
Field_CG_BoundaryTrace<PhysDim, TopoD1, T>&
Field_CG_BoundaryTrace<PhysDim, TopoD1, T>::operator=( const ArrayQ& q ) { BaseType::operator=(q); return *this; }

//----------------------------------------------------------------------------//
template <class PhysDim, class T>
void
Field_CG_BoundaryTrace<PhysDim, TopoD1, T>::init( const int order, const BasisFunctionCategory category, const std::vector<int>& BoundaryGroups )
{
  SANS_ASSERT_MSG( order >= 1, "CG boundary edge requires order >= 1" );
  SANS_ASSERT_MSG( category == BasisFunctionCategory_Hierarchical ||
                   category == BasisFunctionCategory_Lagrange,
                   "CG must use Hierarchical or Lagrange Basis" );

  // create an empty set of interior trace groups
  std::vector<std::vector<int>> boundaryGroupSets({BoundaryGroups});
  std::vector<std::vector<int>> interiorGroupSets(boundaryGroupSets.size());

  // order is always 1 for a 1D boundary trace
  int border = 1;
  Field_CG_TraceConstructor<PhysDim, TopoD1> fldConstructor(xfld_, border, category, interiorGroupSets, boundaryGroupSets);

  nElem_ = fldConstructor.nElem();

  // allocate the solution DOF array
  this->resizeDOF(fldConstructor.nDOF());

  // flatten the boundary sets
  std::vector<int> ConcatBoundaryGroups = cat(boundaryGroupSets);

  this->resizeBoundaryTraceGroups( ConcatBoundaryGroups );

  for (std::size_t igroup = 0; igroup < ConcatBoundaryGroups.size(); igroup++)
  {
    const int group = ConcatBoundaryGroups[igroup];
    int localGroup = localBoundaryTraceGroups_.at(group);

    if ( xfld_.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Node) )
    {
      typedef typename BaseType::template FieldTraceGroupType<Node> FieldTraceGroupClass;

      // allocate group
      boundaryTraceGroups_[localGroup] = fldConstructor.template createBoundaryTraceGroup<FieldTraceGroupClass>( group, this->local2nativeDOFmap_ );
      boundaryTraceGroups_[localGroup]->setDOF( DOF_, nDOF_ );
    }
    else
    {
      SANS_DEVELOPER_EXCEPTION( "Unknown trace topology." );
    }
  }

  // get the ghost/zombie DOF ranks
  // this must be last as it uses this->local2nativeDOFmap_
  this->nDOFpossessed_ = fldConstructor.nDOFpossessed();
  this->nDOFghost_     = fldConstructor.nDOFghost();

  this->resizeDOF_rank(fldConstructor.nDOF(), fldConstructor.nDOFpossessed());

  fldConstructor.getDOF_rank(this->DOFghost_rank_);
}

} // namespace SANS
