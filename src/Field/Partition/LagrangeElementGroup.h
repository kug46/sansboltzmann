// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef LAGRANGEELEMENTGROUP_H_
#define LAGRANGEELEMENTGROUP_H_

#include <vector>
#include <ostream>
#include <memory> // shared_ptr

#include <boost/functional/hash_fwd.hpp>
#include <unordered_map>

#include "Topology/ElementTopology.h"
#include "Field/Element/UniqueElem.h"

#include "tools/SANSException.h"

namespace SANS
{

/*\
 *
 *  Used to track indices of an element and a global element index number
 *
\*/
struct GlobalElement
{
  GlobalElement() : rank(-1), elemID(-1) {}
  // cppcheck-suppress noExplicitConstructor
  GlobalElement(const int rank, const int elemID, const std::vector<int>& elemMap) :
    rank(rank), elemID(elemID), elemMap(elemMap) {}

  int rank;       // rank that possesses the element
  int elemID;
  std::vector<int> elemMap;
};


/*\
 *
 *  Provides a unique representation of a trace element with it's sorted nodes
 *
\*/
class UniqueTrace
{
public:

  UniqueTrace( const int cellElem, const int canonicalTrace );

  UniqueTrace( const UniqueTrace& trace );
  UniqueTrace( const UniqueTrace&& trace );
  UniqueTrace& operator=( const UniqueTrace& trace ) = delete;

  friend std::ostream& operator<<( std::ostream& out, const UniqueTrace& trace );

  const int cellElem;            // Cell element index that the trace was extracted from
  const int canonicalTrace;      // The canonical trace index of the element the trace was extracted from
};

/*\
 *
 *  Element group class used to store element associativity and
 *  traces. Uses the master class to determine trace uniqueness.
 *  As long as the low-level master class provides a way to
 *  uniquely determine traces then the resultant mesh
 *  traces will be non-repeated.
 *
\*/
class LagrangeElementGroup
{
public:

  // constructor
  LagrangeElementGroup( const TopologyTypes& topo, const int order );

  // Inserts and element DOF map
  void addElement(const int rank, const int elem, const std::vector<int>& elemDOFs );

  // Performs some simple consistency checks
  bool check(const int icellgroup, std::stringstream& errmsg);

  // Extract the canonical (corner) nodes from an element
  std::vector<int> elemNodes( const int elem ) const;

  // Extracts all the DOFs associated with the trace of an element
  std::vector<int> traceDOFs( const int elem , const int canoncicaltrace ) const;

  struct ElemPair
  {
    ElemPair( const TopologyTypes topo, std::vector<int>&& nodes,
              const int cellElem, const int canonicalTrace ) : first(topo, std::move(nodes)), second(cellElem, canonicalTrace) {}

    UniqueElem first;
    UniqueTrace second;
  };

  typedef std::vector< ElemPair > UniqueTraceVector;

  // Retrieves all unique trace elements from an element
  UniqueTraceVector uniqueTraces(const int elem) const;
  void uniqueTraces(const int elem, UniqueTraceVector& traces) const;

  // This is used when the LagrangeElementGroup represents trace elements
  UniqueElem uniqueElem(const int elem) const;

  const GlobalElement& elem(const int ielem) const { return elems_[ielem]; }
  GlobalElement& elem( const int ielem ) { return elems_[ielem]; } // pcaplan

  void clear() { elems_.clear(); }

  int nElem()         const { return elems_.size(); }
  int nTracePerElem() const { return canonicalTracesQ1_.size(); }

  int topologyDim() const { return topoDim_;     }
  int order()       const { return order_; }

  // needed to change the order for empty boundary trace groups with MPI
  void setOrder(const int order);

  const std::vector< std::vector<int> >& canonicalTracesQ1()  const { return canonicalTracesQ1_; }
  const std::vector<int>&                canonicalNodes()     const { return canonicalNodes_;}
  const std::vector<TopologyTypes>&      canonicalTraceTopo() const { return canonicalTraceTopo_;}

  const TopologyTypes topo;

protected:
  template<class Topology>
  void setCanonicalVectors();
  void setCanonicalVectors();

  std::vector< GlobalElement > elems_;                // all the element indices
  std::vector< std::vector<int> > canonicalTracesQ1_; // indices for each trace of a canonical element
  std::vector<int> canonicalNodes_;                   // node indices of a canonical element
  std::vector<TopologyTypes> canonicalTraceTopo_;     // topology of a canonical trace element

private:
  int topoDim_;    // topological dimension of the elements
  int order_;      // geometry order
  int nBasis_;     // number of basis functions
};

typedef std::shared_ptr<LagrangeElementGroup> LagrangeElementGroup_ptr;


/*\
 *
 *  Represents a single trace element
 *
\*/
class LagrangeTraceElement
{
public:

  LagrangeTraceElement( int rank, int elemID,
                        TopologyTypes topo, const std::vector<int>& traceDOFs, int order,
                        int groupL, int elemL, int traceL,
                        int groupR, int elemR, int traceR ) :
    rank_(rank),
    elemGlobal_(elemID),
    topo_(topo),
    traceDOFs_(traceDOFs),
    order_ (order),
    groupL_(groupL),
    elemL_ (elemL),
    traceL_(traceL),
    groupR_(groupR),
    elemR_(elemR),
    traceR_(traceR)
  {}

  const std::vector<int>& traceDOFs() const { return traceDOFs_; }

  int rank() const  { return rank_; }
  int elemID() const  { return elemGlobal_; }

  TopologyTypes topo() const  { return topo_; }
  int order() const { return order_; }

  int elemL() const  { return elemL_;  }
  int groupL() const { return groupL_; }

  int elemR() const  { return elemR_;  }
  int groupR() const { return groupR_; }

  int traceL() const { return traceL_; }
  int traceR() const { return traceR_; }

private:
  int rank_;
  int elemGlobal_;
  TopologyTypes topo_;
  std::vector<int> traceDOFs_;
  int order_;
  int groupL_, elemL_, traceL_; // left element, group and trace index
  int groupR_, elemR_, traceR_; // right element, group and trace index
};


/*\
 *
 *  Information required to create a trace group
 *
\*/
class TraceGroupInfo
{
public:

  // Construct the trace group info from a trace element
  explicit TraceGroupInfo( const LagrangeTraceElement& elem )
  {
    topo_    = elem.topo();
    order_   = elem.order();
    groupL_  = elem.groupL();
    groupR_  = elem.groupR();
  }

  // Construct the trace group info explicitly
  TraceGroupInfo( const TopologyTypes& topo , int order , int groupL , int groupR ) :
    topo_(topo),
    order_(order),
    groupL_(groupL),
    groupR_(groupR)
  {}

  // Compares if this trace group info matches a trace element
  bool operator==( const LagrangeTraceElement& elem ) const
  {
    return (topo_   == elem.topo()) &&
           (order_  == elem.order()) &&
           (groupL_ == elem.groupL()) &&
           (groupR_ == elem.groupR());
  }

  TopologyTypes topo() const  { return topo_; }
  int order() const { return order_; }

  int groupL() const { return groupL_; }
  int groupR() const { return groupR_; }

private:
  TopologyTypes topo_;
  int order_;
  int groupL_; // left  group
  int groupR_; // right group
};


/*\
 *
 *  Trace group class used to store trace-node connectivities.
 *  Inherited from LagrangeElementGroup since it stores information in the same
 *  way as elems but extends the class to account for left/right
 *  element (cell) information.
 *
\*/
class LagrangeConnectedGroup : public LagrangeElementGroup
{
public:
  class Empty {};

  explicit LagrangeConnectedGroup( const Empty&, TopologyTypes topo ) :
        LagrangeElementGroup(topo, 1),
        groupL_(0),
        groupR_(-1) {}

  explicit LagrangeConnectedGroup( const TraceGroupInfo& traceGroup ) :
    LagrangeElementGroup(traceGroup.topo(), traceGroup.order()),
    groupL_(traceGroup.groupL()),
    groupR_(traceGroup.groupR()) {}

  void reset( const int groupR )
  {
    clear();
    groupR_ = groupR;
  }

  int elemL( const int i ) const  { return elemL_[i];  }
  int traceL( const int i ) const { return traceL_[i]; }

  int elemR( const int i ) const  { return elemR_[i];  }
  int traceR( const int i ) const { return traceR_[i]; }

  int groupL() const { return groupL_; }
  int groupR() const { return groupR_; }

  void add_trace( const LagrangeTraceElement& traceElem )
  {
    elems_ .emplace_back( traceElem.rank(), traceElem.elemID(), traceElem.traceDOFs() );
    elemL_ .emplace_back( traceElem.elemL()  );
    traceL_.emplace_back( traceElem.traceL() );
    elemR_ .emplace_back( traceElem.elemR()  );
    traceR_.emplace_back( traceElem.traceR() );
  }

  void clear()
  {
    LagrangeElementGroup::clear();
    elemL_.clear(); traceL_.clear();
    elemR_.clear(); traceR_.clear();
  }

private:
  std::vector<int> elemL_, traceL_; // left element, group and trace index
  std::vector<int> elemR_, traceR_; // right element, group and trace index

  int groupL_, groupR_; // left and right cell groups
};

namespace XField_Lagrange_Utils
{

int
traceGroupsIndex( const LagrangeTraceElement& trace, const std::vector< TraceGroupInfo >& traceGroupsInfo );

std::vector< TraceGroupInfo >
countTraceGroups( const std::vector<LagrangeTraceElement>& traces );

} // XField_Lagrange_Utils


typedef std::unordered_map< UniqueElem, UniqueTrace, boost::hash<UniqueElem> > UniqueTraceMap;
//typedef std::map< UniqueElem, UniqueTrace > UniqueTraceMap;

void connectInteriorGroups( const std::vector<LagrangeElementGroup_ptr>& cellGroups,
                            std::vector< UniqueTraceMap >& uniqueTraces,
                            std::vector<LagrangeConnectedGroup>& interiorTraceGroups );

} // namespace SANS

#endif // LAGRANGEELEMENTGROUP_H_
