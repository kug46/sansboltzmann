// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(XFIELD_LAGRANGE_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "XField_Lagrange.h"

#include "tools/SANSException.h"

#include <numeric> // std::accumulate

// This is ok because this impl file is only ever included in a cpp file
#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_MPI
#include <boost/serialization/vector.hpp>
#include <boost/serialization/list.hpp>
#include <boost/mpi/collectives/broadcast.hpp>
#include <boost/mpi/collectives/gather.hpp>
#endif

namespace SANS
{

//---------------------------------------------------------------------------//
#define DOF_TAG         0
#define CELL_TAG        1
#define CELLTOPOS_TAG   2
#define CELLORDERS_TAG  3
#define BND_TAG         4
#define BNDTOPOS_TAG    5
#define PERIODIC_TAG    6

//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::sizeDOF(const int nDOF_global)
{
  // Store the global DOF count across all processors
  nDOF_global_ = nDOF_global;

#ifdef SANS_MPI
  boost::mpi::broadcast(*comm_, nDOF_global_, 0);
#endif

  // Compute the number of DOFs on each processor
  int DOFperProc = nDOF_global_ / comm_size_;

  // Add rounding buffer to the last processor
  if ( comm_->rank() == comm_size_-1 )
    DOFperProc += nDOF_global_ % comm_size_;

  // Allocate the DOFs vector
  DOF_.resize(DOFperProc);

  if (comm_->rank() == 0)
  {
    // Only rank == 0 will return immediately and is responsible for calling addDOF
    // No other rank should call addDOF

    // Make sure the buffer is large enough for the last processor
    DOF_buffer_.resize(DOFperProc + nDOF_global_ % comm_size_);
  }
  else
  {
    // All other ranks will wait here for DOFs sent from rank 0 in addDOF
    comm_->recv(0, DOF_TAG, DOF_.data(), DOF_.size());
  }
}


//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::addDOF(const VectorX& X)
{
  // Make sure that only rank 0 is distributing nodes
  SANS_ASSERT( comm_->rank() == 0 );
  SANS_ASSERT_MSG( iDOF_global_ < nDOF_global_ ,
                   "nDOF_global_ = %d, iDOF_global_ = %d", nDOF_global_, iDOF_global_ );

  // Compute the number of DOFs on each processor
  int DOFperProc = nDOF_global_ / comm_size_;

  // sorry, but I don't know what to do with less than 1 DOF per processor..
  SANS_ASSERT(DOFperProc > 0);

  // Make sure the rank does not go beyond the
  // total number of processors due to the remainders in the partition
  int rank = MIN(comm_size_-1, iDOF_global_ / DOFperProc);

  if (rank == 0)
  {
    // Save the DOF
    DOF_[iDOF_global_] = GlobalDOFType(iDOF_global_, X);
  }
  else
  {
    // Cache the DOF to be sent
    DOF_buffer_[iDOF_global_ - rank*DOFperProc] = GlobalDOFType(iDOF_global_, X);
  }

  // increment the global DOF count that has been added
  iDOF_global_++;

  if (iDOF_global_ ==  (rank+1)*DOFperProc && rank > 0 && rank < comm_size_-1)
  {
    // send the DOFs to the appropriate rank
    comm_->send(rank, DOF_TAG, DOF_buffer_.data(), DOFperProc);
  }
  else if (iDOF_global_ == nDOF_global_)
  {
    if (rank > 0)
    {
      int nDOF = DOFperProc + nDOF_global_ % comm_size_;

      // send the DOFs to the last rank
      comm_->send(rank, DOF_TAG, DOF_buffer_.data(), nDOF);
    }

    // release the memory of the buffer
    DOF_buffer_.clear();
  }
}


//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::
sizeCells(const int nCell_global)
{
  // Save the total number of elements
  nCell_global_ = nCell_global;
  iCell_global_ = 0;

#ifdef SANS_MPI
  // send out the total number of elements to all the other processors
  boost::mpi::broadcast(*comm_, nCell_global_, 0);
#endif

  // Compute the average number of elements on each processor
  int ElemperProc = nCell_global_ / comm_size_;

  // sorry, but I don't know what to do with less than 1 element per processor..
  SANS_ASSERT( ElemperProc > 0 );

  // Add rounding buffer to the first processor
  if ( comm_->rank() == 0 )
    ElemperProc += nCell_global_ % comm_size_;

  // Allocate a buffer to store elements on rank 0
  cellBuffer_.resize(ElemperProc);

  if (comm_->rank() == 0)
  {
    // Only rank == 0 will return immediately and is responsible for calling addCell
    // No other rank should call addCell
  }
  else
  {
    // All other ranks will wait here for Cells sent from rank 0 in addCell
    comm_->recv(0, CELL_TAG, cellBuffer_.data(), cellBuffer_.size());

    // receive the group size info
    comm_->recv(0, CELLTOPOS_TAG, cellTopos_);
    comm_->recv(0, CELLORDERS_TAG, cellOrders_);

    // size the cell groups
    sizeCellGroups();
  }
}


//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::
addCell(const int igroup, const TopologyTypes topo, const int order, const std::vector<int>& elemMap)
{
  // Make sure that only rank 0 is distributing nodes
  SANS_ASSERT( comm_->rank() == 0 );
  SANS_ASSERT_MSG( igroup >= 0, "igroup = %d", igroup );
  SANS_ASSERT( iCell_global_ < nCell_global_ );

  if (igroup >= (int)cellTopos_.size())
  {
    cellTopos_.resize(igroup+1);
    cellOrders_.resize(igroup+1);
  }

  cellTopos_[igroup] = topo;
  cellOrders_[igroup] = order;

  // Compute the number of elements on each processor
  int ElemperProc = nCell_global_ / comm_size_;

  // Make sure the rank does not go beyond the
  // total number of processors due to the remainders in the partition
  int rank = MIN(comm_size_-1, iCell_global_ / ElemperProc);

  // Cache the DOF to be sent (or stored on rank 0)
  cellBuffer_[iCell_global_ - rank*ElemperProc] = GlobalMPIElement(rank, igroup, iCell_global_, elemMap);

  // increment the global element count that has been added
  iCell_global_++;

  if (iCell_global_ == (rank+1)*ElemperProc && rank < comm_size_-1)
  {
    // send to rank+1. The last set of elements will be stored on rank 0
    comm_->send(rank+1, CELL_TAG, cellBuffer_.data(), ElemperProc);
  }

  if (iCell_global_ == nCell_global_)
  {
    // send the group size info
    for (int rank = 1; rank < comm_size_; rank++)
    {
      comm_->send(rank, CELLTOPOS_TAG, cellTopos_);
      comm_->send(rank, CELLORDERS_TAG, cellOrders_);
    }

    // size the cell groups
    sizeCellGroups();
  }
}


//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::
sizeCellGroups()
{
  SANS_ASSERT(cellTopos_.size() == cellOrders_.size());

  // Allocate element groups on all processors
  cellGroups_.resize(cellTopos_.size());
  for (std::size_t igroup = 0; igroup < cellTopos_.size(); igroup++)
    cellGroups_[igroup] = LagrangeElementGroup_ptr( new LagrangeElementGroup( cellTopos_[igroup], cellOrders_[igroup] ) );
}


//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::
sizeBoundaryTrace(const int nBnd_global)
{
  SANS_ASSERT_MSG(nCell_global_ > 0, "Cells must be added before boundary groups");

  // Compute the total number of elements to size the buffer
  nBnd_global_ = nBnd_global;
  iBnd_global_ = 0;

#ifdef SANS_MPI
  // send out the total number of elements to all the other processors
  boost::mpi::broadcast(*comm_, nBnd_global_, 0);
#endif

  // Compute the average number of cell elements on each processor
  // There is likely too few boundary elements to warrant sending to all processors
  int ElemperProc = nCell_global_ / comm_size_;

  // compute the number of processors that will receive boundary trace elements
  bnd_comm_size_ = std::min(comm_size_, std::max(1, (nBnd_global_ - nBnd_global_ % ElemperProc) / ElemperProc));

  // Boundary elements will not exist on all ranks
  if ( comm_->rank()*ElemperProc >= nBnd_global_ - nBnd_global_ % ElemperProc )
    ElemperProc = 0;

  // size the buffer on rank 0
  if (comm_->rank() == 0)
    ElemperProc = std::max(ElemperProc, nBnd_global_ - ElemperProc*(bnd_comm_size_-1));

  // Allocate a buffer to store elements on rank 0
  bndBuffer_.resize(ElemperProc);

  if (comm_->rank() == 0)
  {
    // Only rank == 0 will return immediately and is responsible for calling addBoundaryTrace
    // No other rank should call addBoundaryTrace
  }
  else if (comm_->rank() < bnd_comm_size_)
  {
    // Other ranks that store the buffer will wait here for elements sent from rank 0 in addBoundaryTrace
    comm_->recv(0, BND_TAG, bndBuffer_.data(), bndBuffer_.size());
  }

  if (comm_->rank() > 0)
  {
    // all ranks other than 0 receive the group topologies
    comm_->recv(0, BNDTOPOS_TAG, bndTopos_);

    // size the boundary groups
    sizeBoundaryTraceGroups();
  }
}


//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::
addBoundaryTrace(const int igroup, const TopologyTypes topo, const std::vector<int>& elemMap)
{
  // Make sure that only rank 0 is distributing nodes
  SANS_ASSERT( comm_->rank() == 0 );
  SANS_ASSERT_MSG( igroup >= 0, "igroup = %d", igroup );
  SANS_ASSERT( iBnd_global_ < nBnd_global_ );
  SANS_ASSERT_MSG( topoNNode(topo) == (int)elemMap.size(), "Boundary Trace should always be Q1 element map" );

  if (igroup >= (int)bndTopos_.size())
    bndTopos_.resize(igroup+1);

  bndTopos_[igroup] = topo;

  // Compute the number of elements on each processor
  int ElemperProc = nCell_global_ / comm_size_;

  // Make sure the rank does not go beyond the
  // total number of processors due to the remainders in the partition
  int rank = MIN(bnd_comm_size_-1, iBnd_global_ / ElemperProc);

  // Cache the DOF to be sent (or stored on rank 0)
  bndBuffer_[iBnd_global_ - rank*ElemperProc] = GlobalMPIElement(rank, igroup, iBnd_global_, elemMap);

  // increment the global element count that has been added
  iBnd_global_++;

  if (iBnd_global_ == (rank+1)*ElemperProc && rank+1 < bnd_comm_size_)
  {
    // send to rank+1. The last set of elements will be stored on rank 0
    comm_->send(rank+1, BND_TAG, bndBuffer_.data(), ElemperProc);
  }

  if (iBnd_global_ == nBnd_global_)
  {
    // we are done, so shrink the buffer dpossess to the size of elements on rank 0
    bndBuffer_.resize( nBnd_global_ - ElemperProc*(bnd_comm_size_-1) );

    // send the group size info
    for (int rank = 1; rank < comm_size_; rank++)
      comm_->send(rank, BNDTOPOS_TAG, bndTopos_);

    // size the boundary groups
    sizeBoundaryTraceGroups();
  }
}

//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::
sizeBoundaryTraceGroups()
{
  // Allocate element groups on all processors
  bndGroups_.resize(bndTopos_.size());
  for (std::size_t igroup = 0; igroup < bndTopos_.size(); igroup++)
    bndGroups_[igroup] = LagrangeElementGroup_ptr( new LagrangeElementGroup( bndTopos_[igroup], 1 ) );

  // a simple sanity check
  for (std::size_t elem = 0; elem < bndBuffer_.size(); elem++ )
    SANS_ASSERT_MSG( bndBuffer_[elem].group < (int)bndTopos_.size(), "rank = %d, bndBuffer_[%d].group = %d, bndTopos_.size() = %d",
                     comm_->rank(), elem, bndBuffer_[elem].group, bndTopos_.size() );
}

//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::
addBoundaryPeriodicity(const std::vector<PeriodicBCNodeMap>& periodicity)
{
  if (comm_->rank() == 0)
  {
    for (const PeriodicBCNodeMap& period : periodicity )
    {
      SANS_ASSERT( period.bndGroupL >= 0 );
      SANS_ASSERT( period.bndGroupR >= 0 );
    }

    // TODO: Don't copy this to all processors...
    periodicity_ = periodicity;

    // send the peridocity information
    for (int rank = 1; rank < comm_size_; rank++)
      comm_->send(rank, PERIODIC_TAG, periodicity_);
  }
  else
  {
    // all ranks other than 0 receive the peridodic information (for now)
    comm_->recv(0, PERIODIC_TAG, periodicity_);
  }

  // construct the inverse map
  for ( PeriodicBCNodeMap& period : periodicity_ )
    for ( const std::pair<int,int>& nLnR : period.mapLtoR )
      period.mapRtoL[nLnR.second] = nLnR.first;
}


//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::serialize()
{
#if 0 // for debugging
  dumpBuffer("parallel cellBuffer_", cellBuffer_);
#endif

#if 0 // for debugging
  dumpBuffer("parallel bndBuffer_", bndBuffer_);
#endif

#ifdef SANS_MPI
  if (comm_->rank() == 0)
  {
    std::vector<GlobalMPIElement> cellBuffer(nCell_global_);
    std::vector<GlobalMPIElement> bndBuffer(nBnd_global_);
    std::vector<GlobalDOFType> DOF_buffer(nDOF_global_);

    std::vector<std::size_t> nCell;
    std::vector<std::size_t> nBnd;
    std::vector<std::size_t> nDOF;

    // receive the sizes of the buffers on the other ranks
    boost::mpi::gather(*comm_, cellBuffer_.size(), nCell, 0);
    boost::mpi::gather(*comm_, bndBuffer_.size(), nBnd, 0);
    boost::mpi::gather(*comm_, DOF_buffer_.size(), nDOF, 0);

    for (std::size_t elem = 0; elem < cellBuffer_.size(); elem++)
      cellBuffer[cellBuffer_[elem].elemID] = cellBuffer_[elem];

    cellBuffer_.clear();

    for (std::size_t elem = 0; elem < bndBuffer_.size(); elem++)
      bndBuffer[bndBuffer_[elem].elemID] = bndBuffer_[elem];

    bndBuffer_.clear();

    for (std::size_t i = 0; i < DOF_buffer_.size(); i++)
      DOF_buffer[DOF_buffer_[i].idof] = DOF_buffer_[i];

    DOF_buffer_.clear();

    for (int rank = 1; rank < comm_size_; rank++)
    {

      // cells elements
      std::vector<GlobalMPIElement> celltmp(nCell[rank]);
      comm_->recv(rank, CELL_TAG, celltmp.data(), celltmp.size());

      for (std::size_t elem = 0; elem < celltmp.size(); elem++)
        cellBuffer[celltmp[elem].elemID] = celltmp[elem];

      // boundary elements
      std::vector<GlobalMPIElement> bndtmp(nBnd[rank]);
      comm_->recv(rank, BND_TAG, bndtmp.data(), bndtmp.size());

      for (std::size_t elem = 0; elem < bndtmp.size(); elem++)
        bndBuffer[bndtmp[elem].elemID] = bndtmp[elem];

      // DOFs
      std::vector<GlobalDOFType> dofBuffer(nDOF[rank]);
      comm_->recv(rank, DOF_TAG, dofBuffer.data(), dofBuffer.size());

      for (std::size_t i = 0; i < dofBuffer.size(); i++)
        DOF_buffer[dofBuffer[i].idof] = dofBuffer[i];
    }

    cellBuffer_ = std::move(cellBuffer);
    bndBuffer_ = std::move(bndBuffer);
    DOF_ = std::move(DOF_buffer);

    // set the element ranks to this processor
    for (std::size_t elem = 0; elem < cellBuffer_.size(); elem++)
      cellBuffer_[elem].rank = 0;

    for (std::size_t elem = 0; elem < bndBuffer_.size(); elem++)
      bndBuffer_[elem].rank = 0;
  }
  else
  {
    // send the buffer sizes to rank 0
    boost::mpi::gather(*comm_, cellBuffer_.size(), 0);
    boost::mpi::gather(*comm_, bndBuffer_.size(), 0);
    boost::mpi::gather(*comm_, DOF_buffer_.size(), 0);

    // send the buffers to rank zero
    comm_->send(0, CELL_TAG, cellBuffer_.data(), cellBuffer_.size());
    comm_->send(0, BND_TAG, bndBuffer_.data(), bndBuffer_.size());
    comm_->send(0, DOF_TAG, DOF_buffer_.data(), DOF_buffer_.size());

    cellBuffer_.clear();
    bndBuffer_.clear();
    DOF_buffer_.clear();
    DOF_.clear();
  }
#else
  DOF_ = std::move(DOF_buffer_);
#endif

  // generate the global to local mapping (this is identity for the serializeation)
  for ( std::size_t iDOF = 0; iDOF < DOF_.size(); iDOF++ )
    global2localDOFmap_[iDOF] = iDOF;

#if 0 // for debugging
  dumpBuffer("serial cellBuffer_", cellBuffer_);
#endif

#if 0 // for debugging
  dumpBuffer("serial bndBuffer_", bndBuffer_);
#endif

  // construct the cell and boundary trace groups from the buffers
  buildGroups();
}

//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::sizeDOF_parallel(const int nDOF_local)
{
  // Store the local DOF count on this processors
  nDOF_global_ = nDOF_local;
  iDOF_global_ = 0;

  // Allocate the DOFs vector
  DOF_.resize(nDOF_local);

  // when all DOFs have been added, because there are none on this processor
  if (nDOF_global_ == 0)
    finalizeDOF_parallel();
}

//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::addDOF_parallel(const int iDOF_native, const VectorX& X)
{
  // Check that this is not called too many times
  SANS_ASSERT_MSG( iDOF_global_ < nDOF_global_ ,
                   "iDOF_global_ = %d, nDOF_global_ = %d", iDOF_global_, nDOF_global_ );

#ifdef SANS_MPI
  // Save the DOF in order it is received. finalizeDOF_parallel will sort them
  DOF_[iDOF_global_] = GlobalDOFType(iDOF_native, X);
#else
  // in serial, the DOF needs to be ordered according to iDOF_native
  SANS_ASSERT_MSG( iDOF_native < nDOF_global_ ,
                   "iDOF_native = %d, nDOF_global_ = %d", iDOF_native, nDOF_global_ );

  DOF_[iDOF_native] = GlobalDOFType(iDOF_native, X);
#endif

  // increment the DOF count that has been added
  iDOF_global_++;

  // when all DOFs have been added.
  if (iDOF_global_ == nDOF_global_)
    finalizeDOF_parallel();
}

//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::
finalizeDOF_parallel()
{
#ifdef SANS_MPI
  // to be consistent with addDOF, the DOFs must be sorted on
  // across processors using the iDOF_native index

  // Compute the actual global DOF count
  nDOF_global_ = boost::mpi::all_reduce(*comm_, nDOF_global_, std::plus<int>());
  iDOF_global_ = nDOF_global_;

  // Compute the number of DOFs on each processor
  int DOFperProc = nDOF_global_ / comm_size_;

  // sorry, but I don't know what to do with less than 1 DOF per processor..
  SANS_ASSERT(DOFperProc > 0);

  std::vector<std::list<GlobalDOFType>> sendDOF(comm_size_);
  std::vector<std::list<GlobalDOFType>> recvDOF(comm_size_);

  for (const GlobalDOFType& DOF : DOF_)
  {
    // compute the rank where the DOF should be sent
    int rank = MIN(comm_size_-1, DOF.idof / DOFperProc);

    sendDOF[rank].push_back(DOF);
  }
  DOF_.clear();

  // send the DOF to the other processors
  boost::mpi::all_to_all(*comm_, sendDOF, recvDOF);
  sendDOF.clear();

  // count the total DOF
  int nDOF_local = 0;
  for (int rank = 0; rank < comm_size_; rank++)
    nDOF_local += recvDOF[rank].size();

  std::vector<int> nDOFOnRank;
  boost::mpi::all_gather(*comm_, nDOF_local, nDOFOnRank);

  // compute the node offset from lower ranks
  int nDOF_offset = std::accumulate(nDOFOnRank.begin(), nDOFOnRank.begin()+comm_->rank(), 0);

  // collapse down ordered as syncDOFs expects it
  DOF_.resize(nDOF_local);
  for (int rank = 0; rank < comm_size_; rank++)
    for (const GlobalDOFType& DOF : recvDOF[rank])
      DOF_[DOF.idof - nDOF_offset] = DOF;
#endif
}

//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::
sizeCells_parallel(const int nCell_local)
{
  // Save the total number of elements
  nCell_global_ = nCell_local;
  iCell_global_ = 0;

  // resize the buffer for the unique cells
  cellBuffer_.resize(nCell_local);

  if (nCell_global_ == 0)
    finalizeCell_parallel();
}

//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::
addCell_parallel(const int iCell_native, const int igroup, const TopologyTypes topo, const int order, const std::vector<int>& elemMap)
{
  SANS_ASSERT_MSG( igroup >= 0, "igroup = %d", igroup );
  // Check that this is not called too many times
  SANS_ASSERT_MSG( iCell_global_ < nCell_global_ ,
                   "iCell_global_ = %d, nCell_global_ = %d", iCell_global_, nCell_global_ );

  if (igroup >= (int)cellTopos_.size())
  {
    cellTopos_.resize(igroup+1, eNode);
    cellOrders_.resize(igroup+1, -1);
  }

  cellTopos_[igroup] = topo;
  cellOrders_[igroup] = order;

  // Cache the DOF to be sent (or stored on rank 0)
  cellBuffer_[iCell_global_] = GlobalMPIElement(comm_->rank(), igroup, iCell_native, elemMap);

  // increment the local element count that has been added
  iCell_global_++;

  if (iCell_global_ == nCell_global_)
    finalizeCell_parallel();
}

//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::
finalizeCell_parallel()
{
#ifdef SANS_MPI
  // make cellOrders_ and cellTopos_ consistent across all processors
  std::vector<std::vector<int>> cellOrdersOnRank;
  boost::mpi::all_gather(*comm_, cellOrders_, cellOrdersOnRank);

  for ( const std::vector<int>& cellOrders : cellOrdersOnRank )
  {
    if ( cellOrders.size() > cellOrders_.size() )
      cellOrders_.resize(cellOrders.size(), -1);

    for ( std::size_t igroup = 0; igroup < cellOrders.size(); igroup++ )
    {
      if (cellOrders[igroup] == -1) continue;
      if (cellOrders_[igroup] == -1) cellOrders_[igroup] = cellOrders[igroup];
      // make sure they are otherwise the same
      SANS_ASSERT_MSG( cellOrders_[igroup] == cellOrders[igroup],
                       "cellOrders_[%d] = %d, cellOrders[%d] = %d", igroup, cellOrders_[igroup], igroup, cellOrders[igroup] );
    }
  }

  std::vector<std::vector<TopologyTypes>> cellToposOnRank;
  boost::mpi::all_gather(*comm_, cellTopos_, cellToposOnRank);

  for ( const std::vector<TopologyTypes>& cellTopos : cellToposOnRank )
  {
    if ( cellTopos.size() > cellTopos_.size() )
      cellTopos_.resize(cellTopos.size(), eNode);

    for ( std::size_t igroup = 0; igroup < cellTopos.size(); igroup++ )
    {
      if (cellTopos[igroup] == eNode) continue;
      if (cellTopos_[igroup] == eNode) cellTopos_[igroup] = cellTopos[igroup];
      // make sure they are otherwise the same
      SANS_ASSERT_MSG( cellTopos_[igroup] == cellTopos[igroup],
                       "cellTopos_[%d] = %d, cellTopos[%d] = %d", igroup, cellTopos_[igroup], igroup, cellTopos[igroup] );
    }
  }

  nCell_global_ = boost::mpi::all_reduce(*comm_, nCell_global_, std::plus<int>());
  iCell_global_ = nCell_global_;

#endif

  SANS_ASSERT(cellOrders_.size() == cellTopos_.size());

  // make sure they are all set
  for ( std::size_t igroup = 0; igroup < cellOrders_.size(); igroup++ )
  {
    SANS_ASSERT( cellOrders_[igroup] != -1 );
    SANS_ASSERT( cellTopos_[igroup] != eNode );
  }

  // size the cell groups
  sizeCellGroups();
}

//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::
sizeBoundaryTrace_parallel(const int nBnd_local)
{
  // Compute the total number of elements to size the buffer
  nBnd_global_ = nBnd_local;
  iBnd_global_ = 0;

  // Allocate a buffer to store elements
  bndBuffer_.resize(nBnd_local);

  // processors with no boundary groups will not end up calling addBoundaryTrace_parallel
  // so the finalization must occur here
  if (nBnd_global_ == 0)
    finalizeBoundaryTrace_parallel();
}


//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::
addBoundaryTrace_parallel(const int iBnd_native, const int igroup, const TopologyTypes topo, const std::vector<int>& elemMap)
{
  SANS_ASSERT_MSG( igroup >= 0, "igroup = %d", igroup );
  // Check that this is not called too many times
  SANS_ASSERT_MSG( iBnd_global_ < nBnd_global_ ,
                   "iBnd_global_ = %d, nBnd_global_ = %d", iBnd_global_, nBnd_global_ );

  SANS_ASSERT_MSG( topoNNode(topo) == (int)elemMap.size(), "Boundary Trace should always be Q1 element map" );

  if (igroup >= (int)bndTopos_.size())
    bndTopos_.resize(igroup+1, eNode);

  bndTopos_[igroup] = topo;

  // Store the boundary element
  bndBuffer_[iBnd_global_] = GlobalMPIElement(comm_->rank(), igroup, iBnd_native, elemMap);

  // increment the local element count that has been added
  iBnd_global_++;

  // finalize when all boundary elements have been added
  if (iBnd_global_ == nBnd_global_)
    finalizeBoundaryTrace_parallel();
}


//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::
finalizeBoundaryTrace_parallel()
{
#ifdef SANS_MPI
  // make bndTopos_ consistent across all processors
  std::vector<std::vector<TopologyTypes>> bndToposOnRank;
  boost::mpi::all_gather(*comm_, bndTopos_, bndToposOnRank);

  for ( const std::vector<TopologyTypes>& bndTopos : bndToposOnRank )
  {
    if ( bndTopos.size() > bndTopos_.size() )
      bndTopos_.resize(bndTopos.size(), eNode);

    for ( std::size_t igroup = 0; igroup < bndTopos.size(); igroup++ )
    {
      if (bndTopos[igroup] == eNode) continue;
      if (bndTopos_[igroup] == eNode) bndTopos_[igroup] = bndTopos[igroup];
      // make sure they are otherwise the same
      SANS_ASSERT_MSG( bndTopos_[igroup] == bndTopos[igroup],
                       "bndTopos_[%d] = %d, bndTopos[%d] = %d", igroup, bndTopos_[igroup], igroup, bndTopos[igroup] );
    }
  }

  nBnd_global_ = boost::mpi::all_reduce(*comm_, nBnd_global_, std::plus<int>());
  iBnd_global_ = nBnd_global_;
#endif

  // make sure they are all set
  for ( std::size_t igroup = 0; igroup < bndTopos_.size(); igroup++ )
    SANS_ASSERT( bndTopos_[igroup] != eNode );

  // size the boundary groups
  sizeBoundaryTraceGroups();
}

} // namespace SANS
