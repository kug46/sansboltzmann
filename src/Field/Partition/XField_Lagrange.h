// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD_LAGRANGE_H_
#define XFIELD_LAGRANGE_H_

#include <vector>
#include <memory> // shared_ptr
#include <sstream>
#include <map>
#include <set>
#include <ostream>
#include <string>
#include <fstream>

#ifdef SANS_MPI
#include <boost/serialization/access.hpp>
#endif

#include "Field/XField.h"

#include "LagrangeElementGroup.h"

#include "BasisFunction/BasisFunctionCategory.h"

#include "Topology/ElementTopology.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "MPI/communicator_fwd.h"

namespace SANS
{

struct GlobalMPIElement
{
  GlobalMPIElement() : rank(-1), group(-1), elemID(-1) {}
  GlobalMPIElement(const int group, const GlobalElement& elem) :
    rank(elem.rank), group(group), elemID(elem.elemID), elemMap(elem.elemMap) {}
  GlobalMPIElement(const int rank, const int group, const int elemID, const std::vector<int>& elemMap) :
    rank(rank), group(group), elemID(elemID), elemMap(elemMap) {}

  int rank;       // rank that possesses the element
  int group;
  int elemID;
  std::vector<int> elemMap;

  bool operator<( const GlobalMPIElement& elem ) const { return elemID < elem.elemID; }

#ifdef SANS_MPI
protected:
  friend class boost::serialization::access;

  template<class Archive>
  void serialize(Archive & ar, const unsigned int version)
  {
    ar & rank;
    ar & group;
    ar & elemID;
    ar & elemMap;
  }
#endif
};


/*\
 *
 * Used to track a global DOF in XField_Lagrange
 *
\*/
template<class PhysDim>
struct GlobalVectorX
{
  static const int D = PhysDim::D;
  typedef DLA::VectorS<D,Real> VectorX;

  GlobalVectorX() : idof(-1) {}
  GlobalVectorX(const int idof, const VectorX& X) : idof(idof), X(X) {}

  int idof;  // global DOF index
  VectorX X; // grid coordinate

#ifdef SANS_MPI
protected:
  friend class boost::serialization::access;

  template<class Archive>
  void serialize(Archive & ar, const unsigned int version)
  {
    ar & idof;
    for (int d = 0; d < D; d++)
      ar & X[d];
  }
#endif
};


/*\
 *
 * Used to specify periodic boundaries
 *
\*/
struct PeriodicBCNodeMap
{
  PeriodicBCNodeMap() : bndGroupL(-1), bndGroupR(-1) {}
  PeriodicBCNodeMap(const int bndGroupL, const int bndGroupR) :
    bndGroupL(bndGroupL), bndGroupR(bndGroupR) {}

  int bndGroupL;
  int bndGroupR;
  std::map<int,int> mapLtoR; // grid coordinate

protected:
  template<class PhysDim> friend class XField_Lagrange;
  std::map<int,int> mapRtoL; // inverse map

#ifdef SANS_MPI
protected:
  friend class boost::serialization::access;

  template<class Archive>
  void serialize(Archive & ar, const unsigned int version)
  {
    ar & bndGroupL;
    ar & bndGroupR;
    ar & mapLtoR;
  }
#endif
};


/*\
 *
 * A field capable of automatically discovering connectivity and
 * performing load balancing of elements across MPI processors
 *
\*/
template<class PhysDim>
class XField_Lagrange
{
public:
  static const int D = PhysDim::D;
  typedef DLA::VectorS<D,Real> VectorX;
  typedef GlobalVectorX<PhysDim> GlobalDOFType;

  XField_Lagrange(mpi::communicator& comm, XFieldBalance graph = Cell);
  XField_Lagrange(mpi::communicator& comm, XFieldBalance graph , const std::string& filename);
  template<class TopoDim>
  XField_Lagrange(const XField<PhysDim,TopoDim>& xfld, XFieldBalance graph);

  // Size and add DOF. In parallel, only rank == 0 calls addDOF
  void sizeDOF(const int nDOF_global);
  void addDOF(const VectorX& X);

  // Adds an Element to a cell element group. In parallel, only rank == 0 calls addCell
  void sizeCells(const int nCell_global);
  void addCell(const int igroup, const TopologyTypes topo, const int order, const std::vector<int>& elemMap);

  // Adds an Element to a boundary trace element group. In parallel, only rank == 0 calls addBoundaryTrace
  void sizeBoundaryTrace(const int nBnd_global);
  void addBoundaryTrace(const int igroup, const TopologyTypes topo, const std::vector<int>& elemMap);

  // Indicates which boundaries are periodic. In parallel, only rank == 0 calls this
  void addBoundaryPeriodicity(const std::vector<PeriodicBCNodeMap>& periodicity);

  // Size and add DOF. In parallel, all ranks call these with unique DOFs
  void sizeDOF_parallel(const int nDOF_local);
  void addDOF_parallel(const int iDOF_native, const VectorX& X);

  // Adds an Element to a cell element group. In parallel, only rank == 0 calls addCell
  void sizeCells_parallel(const int nCell_local);
  void addCell_parallel(const int iCell_native, const int igroup, const TopologyTypes topo, const int order, const std::vector<int>& elemMap);

  // Adds an Element to a boundary trace element group. In parallel, only rank == 0 calls addBoundaryTrace
  void sizeBoundaryTrace_parallel(const int nBnd_local);
  void addBoundaryTrace_parallel(const int iBnd_native, const int igroup, const TopologyTypes topo, const std::vector<int>& elemMap);

  int nDOF() const { return DOF_.size(); }
  const GlobalDOFType& DOF(const int n) const { return DOF_[n]; }

  int nCellGroups() const { return cellGroups_.size(); }
  int nBoundaryTraceGroups() const { return bndGroups_.size(); }
  int nGhostBoundaryTraceGroups() const { return ghostBndGroups_.size(); }

  const LagrangeElementGroup_ptr& cellGroup(const int igroup) const { return cellGroups_[igroup]; }
  const LagrangeElementGroup_ptr& getBoundaryTraceGroup(const int igroup) const { return bndGroups_[igroup]; }

  const std::vector<LagrangeElementGroup_ptr>& cellGroups() const { return cellGroups_; }
  const std::vector<LagrangeElementGroup_ptr>& bndGroups(const int igroup) const { return bndGroups_; }

  const std::map<int,int>& global2localDOFmap() const { return global2localDOFmap_; }
  int global2localDOFmap(const int iglobal) const { return global2localDOFmap_.at(iglobal); }
  int nNode_global() const { return nNode_global_; }
  const std::map<int,int>& nodeRank() const { return nativeNodeRank_; }

  const std::vector<PeriodicBCNodeMap>& periodicity() { return periodicity_; }

  // perform load balancing
  void balance();

  void getConnectedTraceGroups( std::vector<LagrangeConnectedGroup>& interiorTraceGroups,
                                std::vector<LagrangeConnectedGroup>& boundaryTraceGroups,
                                std::vector<LagrangeConnectedGroup>& ghostBoundaryTraceGroups ) const;
  void mergePeriodicGroups( std::vector<LagrangeConnectedGroup>& boundaryGroups ) const;

  template<class TopoDim>
  static void outputPartition(const XField<PhysDim,TopoDim>& xfld, const std::string& filename);

  std::shared_ptr<mpi::communicator> comm() const { return comm_; }

protected:
  std::shared_ptr<mpi::communicator> comm_;  // communicator for MPI processors
  int comm_size_;                            // size of the MPI communicator

  XFieldBalance graph_;                      // graph used to parition the mesh

  int nDOF_global_;
  int iDOF_global_;                          // current global DOF index
  std::vector<GlobalDOFType> DOF_buffer_;    // buffer to sent DOFs to MPI processors

  int nCell_global_;                         // global cell element count
  int iCell_global_;                         // total number of cells processed so far
  std::vector<GlobalMPIElement> cellBuffer_; // buffer to send cells to MPI processors
  std::vector<TopologyTypes> cellTopos_;     // topology of each cell group
  std::vector<int> cellOrders_;              // order of each cell

  int nBnd_global_;                         // global boundary trace element count
  int iBnd_global_;                         // total number of boundary elements processed so far
  int bnd_comm_size_;                       // the number of ranks storing boundary elements
  std::vector<GlobalMPIElement> bndBuffer_; // buffer to send boundary elements to MPI processors
  std::vector<TopologyTypes> bndTopos_;     // topology of each boundary group

  std::vector<GlobalDOFType> DOF_;
  std::vector<LagrangeElementGroup_ptr> cellGroups_;
  std::vector<LagrangeElementGroup_ptr> bndGroups_;
  std::vector<LagrangeElementGroup_ptr> ghostBndGroups_;

  std::vector<PeriodicBCNodeMap> periodicity_;

  std::map<int,int> global2localDOFmap_; // maps global DOF index to a local DOF index

  int nNode_global_;                     // total number of nodes in the mesh
  std::map<int,int> nativeNodeRank_;     // the rank assignment to node DOFs

  typedef std::vector<std::vector<std::pair<int,int>>> PartitionType;

  PartitionType cellpartition_; // list of processor ranks indicating a cell element partitioning
  PartitionType bndpartition_;  // list of processor ranks indicating a boundary element partitioning

  template<class TopoDim>
  void copyElements(const XField<PhysDim,TopoDim>& xfld);
  template<class TopoDim>
  void copyCellPartition(const XField<PhysDim,TopoDim>& xfld);

  void InputPartition(mpi::communicator& comm, const std::string& filename);

  void sizeCellGroups();
  void sizeBoundaryTraceGroups();

  void finalizeDOF_parallel();
  void finalizeCell_parallel();
  void finalizeBoundaryTrace_parallel();

  std::map<int,int> continuousNodeMap();
  std::map<int,int> continuousNodeMap(const std::vector<GlobalMPIElement>& buffer);
  std::map<int,int> continuousNodeRank( const std::map<int,int>& globalNodeMap );
  std::map<int,int> continuousNodeRank( const std::vector<GlobalMPIElement>& buffer );
  void continuousCellMap(std::map<int,int>& globalCellMap, std::vector<int>& nCellOnRank);

  void serialize(); // serialize to rank 0

  // Performs a crude element load balance (not DOF) based on node numbers in the grid
  void balanceWithNodes();

  void sendElementsWithNodes(const std::map<int,int>& nativeNodeRank,
                             const std::vector<LagrangeElementGroup_ptr>& groups,
                             const bool bndBuffer,
                             const std::vector<GlobalMPIElement>& buffer,
                             std::vector<std::vector<GlobalMPIElement>>& recvBuffer );
  void balanceElementsWithNodes(const std::map<int,int>& nativeNodeRank,
                                const std::vector<LagrangeElementGroup_ptr>& groups,
                                const bool bndBuffer,
                                std::vector<GlobalMPIElement>& buffer);
  void appendElementsWithNodes(const std::map<int,int>& nativeNodeRank,
                               const std::vector<LagrangeElementGroup_ptr>& groups,
                               const bool bndBuffer,
                               std::vector<GlobalMPIElement>& buffer);
  void getNodeRanking(const std::map<int,int>& nativeNodeCellMinRank,
                      std::map<int,int>& nativeNodeRank );

  void buildGroups();
  void resetGhostGroups();
  void syncDOFs();

  void cellAdjacencyGraph(const std::map<int,int>& globalCellMap, const std::vector<int>& nCellOnRank,
                          std::vector<std::set<int>>& Adj);
  void balanceWithCellParition(const std::vector<int>& cellpart, const std::vector<std::set<int>>& Adj,
                               const std::map<int,int>& globalCellMap, const std::vector<int>& nCellOnRank);
  void createCellGhostGroups();

  void balanceElementsWithPartition(const PartitionType& part,
                                    std::vector<GlobalMPIElement>& buffer);

  void balanceCellsWithCopyPartition();

  void balanceCellsWithParmetis();
  std::vector<int> callParmetis(const std::vector<std::set<int>>& Adj, const std::vector<int>& nVertexOnRank);

  void connectBoundaryGroups( const std::vector<LagrangeElementGroup_ptr>& bndGroups,
                              std::vector< UniqueTraceMap >& uniqueTraces,
                              std::vector<LagrangeConnectedGroup>& boundaryGroups,
                              const bool syncorder ) const;

  void getPeriodicElemMap( const std::vector<LagrangeConnectedGroup>& boundaryGroups,
                           std::vector<PeriodicBCNodeMap>& periodicElemMaps ) const;

  // for debugging
  void dumpBuffer(const std::string& name, const std::vector<GlobalMPIElement>& buffer);
};

namespace XField_Lagrange_Utils
{

  std::vector< TraceGroupInfo > countTraceGroups( const std::vector<LagrangeTraceElement>& traces );
  int traceGroupsIndex( const LagrangeTraceElement& trace, const std::vector< TraceGroupInfo >& traceGroupsInfo );

} // XField_Lagrange_Utils

} // namespace SANS

#endif // end include guard
