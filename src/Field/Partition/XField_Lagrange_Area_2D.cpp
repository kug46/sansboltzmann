// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField_Lagrange_impl.h"

namespace SANS
{
// Explicit instantiations
template XField_Lagrange<PhysD2>::XField_Lagrange(const XField<PhysD2,TopoD2>& xfld, XFieldBalance graph);

template void XField_Lagrange<PhysD2>::outputPartition<TopoD2>(const XField<PhysD2,TopoD2>& xfld, const std::string& filename);

template class XField_Lagrange<PhysD2>;

}
