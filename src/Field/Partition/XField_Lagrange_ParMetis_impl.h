// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(XFIELD_LAGRANGE_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "XField_Lagrange.h"

// This is ok because this impl file is only ever included in a cpp file
#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_MPI
#include <parmetis.h>

#if PARMETIS_MAJOR_VERSION == 3 && PARMETIS_MINOR_VERSION < 2
#error "Parmetis versions less than 3.2 have a memory allocation bug"
#endif

#if PARMETIS_MAJOR_VERSION == 3
#define PARM_INT  idxtype
#define PARM_REAL float
#elif  PARMETIS_MAJOR_VERSION == 4
#define PARM_INT  idx_t
#define PARM_REAL real_t
#else
#error "Unknown version of parmetis"
#endif
#endif // SANS_MPI

namespace SANS
{


//---------------------------------------------------------------------------//
template<class PhysDim>
std::vector<int>
XField_Lagrange<PhysDim>::
callParmetis(const std::vector<std::set<int>>& Adj, const std::vector<int>& nVertexOnRank)
{
#ifdef SANS_MPI

#if 0 // for debugging
  int vertex_offset = std::accumulate(nVertexOnRank.begin(), nVertexOnRank.begin() + comm_->rank(), 0);

  std::cout << std::flush;
  comm_->barrier();
  for (int rank = 0; rank < comm_size_; rank++)
  {
    std::cout << std::flush;
    comm_->barrier();
    if (comm_->rank() != rank) continue;

    std::cout << std::endl << " Adj rank " << rank << std::endl;
    for (std::size_t i = 0; i < Adj.size(); i++ )
    {
      std::cout << i + vertex_offset << " adj ";
      for (const int adj : Adj[i])
        std::cout << adj << " ";
      std::cout << std::endl;
    }
    std::cout << std::endl;
    std::cout << std::flush;
  }
  comm_->barrier();
#endif

  int nVertex = nVertexOnRank[comm_->rank()];

  // parmetis does not like being called with 1 partition, but the partition is also trivial...
  if (comm_size_ == 1)
    return std::vector<int>(nVertex, 0);

  std::size_t adj_size = 0;
  for (std::size_t i = 0; i < Adj.size(); i++ )
    adj_size += Adj[i].size();

  // -------------------------------------------------------//
  // construct the compressed adjacency format for parmetis //
  // -------------------------------------------------------//

  // deal with the rare situation where a rank does not possess any of it's elements
  // parmetis is not smart enough to have a rank with no graph vertices
  mpi::communicator parmcomm = comm_->split(nVertex > 0 ? 0 : 1);

  // inputs
  std::vector<PARM_INT> vtxdist(parmcomm.size()+1,0);    // Distribution of graph vertices across processors
  std::vector<PARM_INT> xadj(Adj.size()+1);              // Pointers to the locally stored graph vertices
  PARM_INT *vwgt=NULL;                                   // Graph vertex weights
  std::vector<PARM_INT> adjncy(adj_size);                // Array that stores the adjacency lists of nvtxs
  PARM_INT *adjwgt=NULL;                                 // Array that stores the weights of the adjacency lists
  PARM_INT wgtflag=0;                                    // No weights (vwgt == adjwgt == NULL)

  PARM_INT numflag=0;                                    // C-style numbering
  PARM_INT ncon=1;                                       // number of weights at each graph vertex and number of balance constraints
  PARM_INT nparts = comm_size_;                          // number of desired partitions (does not have to be comm size)
  std::vector<PARM_REAL> tpwgts(ncon*nparts, 1./nparts); // see the parmetis manual
  std::vector<PARM_REAL> ubvec(ncon, 1.05);              // imbalance tolerance (parmetis manual recommends 1.05)
  PARM_INT options[3];                                   // options (see below)

  MPI_Comm comm = parmcomm;                              // communicator used for parmetis

  // outputs
  PARM_INT edgecut=0;                                    // number of edges cut
  std::vector<PARM_INT> part(nVertex);                   // the rank where each vertex needs to be sent

  options[0] = 1; // 0 - use default, 1 - use numbers in options[1] and options[2]
  options[1] = 3; // Verbosity (see parmetis.h for more options)
  options[2] = 0; // random seed

  // indicates which processor stores which vertex
  // while skipping processor with 0 vertices
  auto it = nVertexOnRank.begin();
  for (int rank = 0; rank < parmcomm.size(); rank++)
  {
    while (it != nVertexOnRank.end() && *it == 0) it++;
    if (it == nVertexOnRank.end()) break;
    vtxdist[rank+1] = vtxdist[rank] + *it;
    it++;
  }

  // construct the index into adjncy for each row in the CRS
  int elem = 0;
  for (std::size_t i = 0; i < Adj.size(); i++ )
  {
    xadj[elem+1] = Adj[i].size() + xadj[elem];
    elem++;
  }

  // construct the CRS adjacency
  elem = 0;
  for (std::size_t i = 0; i < Adj.size(); i++ )
  {
    int k = xadj[elem++];
    for ( const int& elem_adj : Adj[i] )
      adjncy[k++] = elem_adj;
  }

#if 0 // for debugging
  for (int rank = 0; rank < comm_size_; rank++)
  {
    std::cout << std::flush;
    comm_->barrier();
    if (comm_->rank() != rank) continue;

    std::cout << "rank " << rank << std::endl;
    std::cout << "nVertex " << nVertex << std::endl;
    std::cout << "xadj " << xadj << std::endl;
    std::cout << "adjncy " << adjncy << std::endl;
    std::cout << "vtxdist " << vtxdist << std::endl;
    std::cout << std::endl;
    std::cout << std::flush;
  }
  comm_->barrier();
#endif

  if (nVertex > 0)
  {
#if PARMETIS_MAJOR_VERSION == 4
    int result =
#endif
    ParMETIS_V3_PartKway ( vtxdist.data(), xadj.data(), adjncy.data(),
                           vwgt, adjwgt, &wgtflag,
                           &numflag, &ncon, &nparts,
                           tpwgts.data(), ubvec.data(),
                           options,
                           &edgecut,
                           part.data(),
                           &comm );
#if  PARMETIS_MAJOR_VERSION == 4
    SANS_ASSERT_MSG( result == METIS_OK, "Something is wrong with parmetis..." );
#endif
  }

#if 0 // for debugging
  std::cout << std::flush;
  comm_->barrier();
  for (int rank = 0; rank < comm_size_; rank++)
  {
    std::cout << std::flush;
    comm_->barrier();
    if (comm_->rank() != rank) continue;

    std::cout << "rank " << rank << std::endl;
    std::cout << "edgecut " << edgecut << std::endl;
    std::cout << "part " << part << std::endl;
    std::cout << std::endl;
    std::cout << std::flush;
  }
  comm_->barrier();
#endif

  return part;

#else // SANS_MPI

  return std::vector<int>();

#endif
}

} // namespace SANS
