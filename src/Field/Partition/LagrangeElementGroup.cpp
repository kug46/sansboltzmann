// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <iostream>
#include <sstream>
#include <algorithm> // std::sort

#include "tools/output_std_vector.h"

#include "LagrangeElementGroup.h"

#include "BasisFunction/LagrangeDOFMap.h"

#include "Field/Element/UniqueElemHash.h"

// Function specifically for constructing cell/trace elements from LagrangeElementGroups

namespace SANS
{


std::ostream&
operator<<( std::ostream& out, const LagrangeTraceElement& elem )
{
  out << "topo ";
  out << elem.topo();
  out << " order ";
  out << elem.order();

  out << " elem ";
  out << elem.elemL();
  out << " ";
  out << elem.elemR();

  out << " group ";
  out << elem.groupL();
  out << " ";
  out << elem.groupR();

  out << " trace ";
  out << elem.traceL();
  out << " ";
  out << elem.traceR();

  out << " traceDOFs ";
  out << elem.traceDOFs();

  return out;
}

std::ostream&
operator<<( std::ostream& out, const UniqueTrace& trace )
{
  out << " elem ";
  out << trace.cellElem;

  out << " canonicalTrace ";
  out << trace.canonicalTrace;

  return out;
}

//===========================================================================//
//
// UniqueTrace
//
//===========================================================================//

//---------------------------------------------------------------------------//
UniqueTrace::UniqueTrace( const int cellElem, const int canonicalTrace )
  : cellElem(cellElem), canonicalTrace(canonicalTrace)
{
}

//---------------------------------------------------------------------------//
UniqueTrace::UniqueTrace( const UniqueTrace& trace )
  : cellElem(trace.cellElem), canonicalTrace(trace.canonicalTrace)
{
}

//---------------------------------------------------------------------------//
UniqueTrace::UniqueTrace( const UniqueTrace&& trace )
  : cellElem(trace.cellElem), canonicalTrace(trace.canonicalTrace)
{
}

//---------------------------------------------------------------------------//
#if 0
UniqueTrace&
UniqueTrace::operator=( const UniqueTrace& trace )
{
  cellElem = trace.cellElem;
  canonicalTrace = trace.canonicalTrace;
  return *this;
}
#endif

//===========================================================================//
//
// LagrangeElementGroup
//
//===========================================================================//

LagrangeElementGroup::LagrangeElementGroup( const TopologyTypes& topo, const int order ) :
    topo(topo), topoDim_(topoDim(topo)), order_(order)
{
  setCanonicalVectors();
}

//---------------------------------------------------------------------------//
// Inserts an element DOF map
void
LagrangeElementGroup::addElement(const int rank, const int elem, const std::vector<int>& elemDOFs )
{
  SANS_ASSERT_MSG( elem >= 0, "elem = %d", elem );
  SANS_ASSERT_MSG( (int)elemDOFs.size() == nBasis_, "elemDOFs.size() = %d, nBasis_ = %d", elemDOFs.size(), nBasis_ );

  // add the cell
  elems_.emplace_back(rank, elem, elemDOFs);
}

//---------------------------------------------------------------------------//
bool
LagrangeElementGroup::check(const int icellgroup, std::stringstream& errmsg)
{
  bool allgood = true;
  for (std::size_t elem = 0; elem < elems_.size(); elem++)
    if (elems_[elem].elemMap.size() == 0)
    {
      allgood = false;
      errmsg << "No DOF map set for element " << elem << " in cell group " << icellgroup << std::endl;
    }

  return allgood;
}

//---------------------------------------------------------------------------//
// Extract the canonical (corner) nodes from an element
std::vector<int>
LagrangeElementGroup::elemNodes( const int elem ) const
{
  //SANS_ASSERT_MSG( elem < nElem() , "elem = %d , nElem = %d" , elem , nElem() );

  std::vector<int> idx(canonicalNodes_.size());

  // map back to global indices
  for (std::size_t j = 0; j < canonicalNodes_.size(); j++)
    idx[j] = elems_[elem].elemMap[ canonicalNodes_[j] ];

  return idx;
}

//---------------------------------------------------------------------------//
// Extracts all the DOFs associated with the trace of an element
std::vector<int>
LagrangeElementGroup::traceDOFs( const int elem , const int canonicaltrace ) const
{
  std::vector<int> traceDOFs;

  // get the (full) local trace indices for the particular element topo
  if (topo == eLine)
    traceDOFs = LagrangeDOFMap<Line>::getCanonicalTraceMap(canonicaltrace, order_);
  else if (topo == eTriangle)
    traceDOFs = LagrangeDOFMap<Triangle>::getCanonicalTraceMap(canonicaltrace, order_);
  else if (topo == eQuad)
    traceDOFs = LagrangeDOFMap<Quad>::getCanonicalTraceMap(canonicaltrace, order_);
  else if (topo == eTet)
    traceDOFs = LagrangeDOFMap<Tet>::getCanonicalTraceMap(canonicaltrace, order_);
  else if (topo == eHex)
    traceDOFs = LagrangeDOFMap<Hex>::getCanonicalTraceMap(canonicaltrace, order_);
  else if (topo == ePentatope)
    traceDOFs = LagrangeDOFMap<Pentatope>::getCanonicalTraceMap(canonicaltrace,order_);
  else
    SANS_DEVELOPER_EXCEPTION( "unknown element toplogical type = %d", topo );

  // map back to global indices
  for (std::size_t i = 0; i < traceDOFs.size(); i++)
    traceDOFs[i] = elems_[elem].elemMap[ traceDOFs[i] ];

  return traceDOFs;
}

//---------------------------------------------------------------------------//
// Retrieves all unique trace elements from an element
LagrangeElementGroup::UniqueTraceVector
LagrangeElementGroup::uniqueTraces(const int elem) const
{
  UniqueTraceVector traces;
  uniqueTraces(elem, traces);
  return traces;
}

void
LagrangeElementGroup::uniqueTraces(const int elem, UniqueTraceVector& traces) const
{
  traces.clear();

  // add all the q1 traces associated with this element
  for (std::size_t itrace = 0; itrace < canonicalTracesQ1_.size(); itrace++)
  {
    std::vector<int> traceNodes(canonicalTracesQ1_[itrace].size());
    for (std::size_t j = 0; j < canonicalTracesQ1_[itrace].size(); j++)
      traceNodes[j] = elems_[elem].elemMap[ canonicalTracesQ1_[itrace][j] ];

    // Add the trace to the list of all element traces
    traces.emplace_back(canonicalTraceTopo_[itrace], std::move(traceNodes), elem, itrace);
  }
}

//---------------------------------------------------------------------------//
// This is used when the LagrangeElementGroup represents trace elements
UniqueElem
LagrangeElementGroup::uniqueElem(const int elem) const
{
  // add all the q1 traces associated with this element

  std::vector<int> elemNodes(canonicalNodes_.size());
  for (std::size_t j = 0; j < canonicalNodes_.size(); j++)
    elemNodes[j] = elems_[elem].elemMap[ canonicalNodes_[j] ];

  return {topo, std::move(elemNodes)};
}

//---------------------------------------------------------------------------//
void LagrangeElementGroup::setOrder(const int order)
{
  // change the order and updated nBasis_
  order_ = order;
  setCanonicalVectors();
}

//---------------------------------------------------------------------------//
// Copy over the Lagrange point maps
template<class Topology>
void LagrangeElementGroup::setCanonicalVectors()
{
  canonicalTracesQ1_  = LagrangeDOFMap<Topology>::getTracesCanonicalNodes();
  canonicalNodes_     = LagrangeDOFMap<Topology>::getCanonicalNodes();
  canonicalTraceTopo_ = LagrangeDOFMap<Topology>::getTracesTopo();
  nBasis_             = LagrangeDOFMap<Topology>::nBasis(order_);
}

// Determine the topology to get the maps from
void LagrangeElementGroup::setCanonicalVectors()
{
  switch (topo)
  {
  case eNode:
  {
    canonicalTracesQ1_  = {{}};
    canonicalNodes_     = {0};
    canonicalTraceTopo_ = {eNode};
    nBasis_             = 1;
    return;
  }
  case eLine:
  {
    setCanonicalVectors<Line>();
    return;
  }
  case eTriangle:
  {
    setCanonicalVectors<Triangle>();
    return;
  }
  case eQuad:
  {
    setCanonicalVectors<Quad>();
    return;
  }
  case eTet:
  {
    setCanonicalVectors<Tet>();
    return;
  }
  case eHex:
  {
    setCanonicalVectors<Hex>();
    return;
  }
  case ePentatope:
  {
    setCanonicalVectors<Pentatope>();
    return;
  }
  default:
    SANS_DEVELOPER_EXCEPTION("Unknown topology: %d", topo);
  }
}


#if 0
inline void
print_cells( const std::string& title, const std::string& prefix , const std::vector< std::vector<int> >& cells )
{
  printf("%s:\n", title.c_str());
  for (std::size_t i = 0; i < cells.size(); i++)
  {
    printf("%s[%d] = (", prefix.c_str(), (int)i);
    for (std::size_t j = 0; j < cells[i].size(); j++) printf(" %d",cells[i][j]);
    printf(" )\n");
  }
}
#endif


// =================== BEGIN GROUP FUNCTIONS ===================  //
namespace XField_Lagrange_Utils
{

int
traceGroupsIndex( const LagrangeTraceElement& trace, const std::vector< TraceGroupInfo >& traceGroupsInfo )
{
  // Check the group pair exist with the given order and topology
  for (std::size_t i = 0; i < traceGroupsInfo.size(); i++)
    if (traceGroupsInfo[i] == trace)
      return i;

  // Did not find a trace group that the trace element should belong to
  return -1;
}

std::vector< TraceGroupInfo >
countTraceGroups( const std::vector<LagrangeTraceElement>& traces )
{
  std::vector< TraceGroupInfo > traceGroupsInfo;

  for (std::size_t i = 0; i < traces.size(); i++)
  {
    if (traceGroupsIndex( traces[i], traceGroupsInfo ) >= 0) continue;

    traceGroupsInfo.emplace_back( traces[i] );
  }

  return traceGroupsInfo;
}

} // XField_Lagrange_Utils

// =================== END GROUP FUNCTIONS ===================  //


//===========================================================================//
//
// Functions for XField to construct trace/cell associativities
//
//===========================================================================//

void
checkElementGroupTypes( const std::vector<LagrangeElementGroup_ptr>& groups )
{
  if (groups.size() == 0) return;

  bool allgood = true;

  int topoDim = groups[0]->topologyDim();

  std::stringstream errmsg;

  // check that all groups have the same topology dimension
  for (std::size_t i = 0;i < groups.size(); i++)
  {
    // Perform some internal consistency checks
    allgood = allgood && groups[i]->check(i, errmsg);

    if (groups[i]->topologyDim() != topoDim)
    {
      errmsg << "group " << i << " has topology dimension " << groups[i]->topologyDim()
             << " but another group has dimension " << topoDim << std::endl;
      allgood = false;
    }
  }
  if (!allgood)
    SANS_DEVELOPER_EXCEPTION( errmsg.str().c_str() );

  // return if dim == 2 since mixed tri/quad meshes are supported
  if (topoDim == 2) return;

  // make sure all groups have topo X where X is either Tet or Hex (not mixed)
  TopologyTypes type = groups[0]->topo;

  for (std::size_t i = 0; i < groups.size(); i++)
  {
    if (groups[i]->topo != type)
    {
      printf("group %d has type %d but another group has type %d\n",
             (int)i, groups[i]->topo, type);
      allgood = false;
    }
  }
  SANS_ASSERT( allgood );
}
// ========================================================================  //

//---------------------------------------------------------------------------//
void
connectInteriorGroups( const std::vector<LagrangeElementGroup_ptr>& cellGroups,
                       std::vector<UniqueTraceMap>& uniqueTraces ,
                       std::vector<LagrangeConnectedGroup>& interiorTraceGroups )
{
  // The algorithm here is as follows:
  // 1) Visit an element and extract it's 'UniqueTraces' based on sorted nodes
  // 2) Check if each unique trace of the element already exists in the 'uniqueTraces' table
  // 3) If the trace does not exist in the table, add it to the table. If it does exist in the table,
  //    a match has been found and it's added to 'interiorTraces'
  // After the algorithm is completed, only the boundary traces will reside in 'uniqueTraces'

  // reserve the memory assuming each trace of an element is shared with a neighbor (a little small for boundaries)
  int nTraceBound = 0;
  for (std::size_t igroup = 0; igroup < cellGroups.size(); igroup++)
    nTraceBound += cellGroups[igroup]->nElem()*cellGroups[igroup]->nTracePerElem()/2;

  // A list of all unique interior traces in the grid
  std::vector<LagrangeTraceElement> interiorTraces;
  interiorTraces.reserve(nTraceBound);
  LagrangeElementGroup::UniqueTraceVector elemTraces;

  for (std::size_t igroup = 0; igroup < cellGroups.size(); igroup++)
  {
    for (int ielem = 0; ielem < cellGroups[igroup]->nElem(); ielem++)
    {
      cellGroups[igroup]->uniqueTraces(ielem, elemTraces);

      for ( LagrangeElementGroup::ElemPair& trace : elemTraces )
      {
        //std::cout << "trace" << elemTraces[itrace] << std::endl;

        bool traceFound = false;
        for (std::size_t jgroup = 0; jgroup < uniqueTraces.size(); jgroup++)
        {
          // Look for the current trace in the jth cell group
          UniqueTraceMap::iterator it = uniqueTraces[jgroup].find(trace.first);
          if ( it != uniqueTraces[jgroup].end() )
          {
            traceFound = true;

            // get left properties
            int groupL = igroup;
            int elemL  = ielem;
            int traceL = trace.second.canonicalTrace;
            int orderL = cellGroups[groupL]->order();
            int rankL  = cellGroups[groupL]->elem(elemL).rank;

            // get right properties
            int groupR = jgroup;
            int elemR  = it->second.cellElem;
            int traceR = it->second.canonicalTrace;
            int orderR = cellGroups[groupR]->order();
            int rankR  = cellGroups[groupR]->elem(elemR).rank;

            // Sort so that the left group is always the lower index or lower order
            if ( orderL > orderR || groupL > groupR )
            {
              std::swap(elemL, elemR);
              std::swap(groupL, groupR);
              std::swap(traceL, traceR);
              std::swap(orderL, orderR);
              std::swap(rankL, rankR);
            }

            // get the trace topology
            TopologyTypes topoT = trace.first.topo;

            // get the FULL trace with all the high-order nodes from the left element so that it is the canonicalTrace
            std::vector<int> traceDOFs = cellGroups[groupL]->traceDOFs( elemL, traceL );

            interiorTraces.emplace_back(rankL, -1, topoT, traceDOFs, orderL, groupL, elemL, traceL, groupR, elemR, traceR);

            // remove the trace since we found a match
            uniqueTraces[jgroup].erase(trace.first);
            break;
          }
        }

        // Add the trace so it may be found by a different element
        // use std::move to eliminate unecessary copying/allocations
        if (!traceFound)
          uniqueTraces[igroup].insert(std::pair<const UniqueElem,UniqueTrace>{std::move(trace.first), trace.second});

      } // itrace
    }// ielem
  } // igroup

  // separate the traces into different groups based on groupL, groupR, topoTrace and orderTrace
  std::vector< TraceGroupInfo > intTraceGroupsInfo = XField_Lagrange_Utils::countTraceGroups( interiorTraces );

  interiorTraceGroups.reserve(intTraceGroupsInfo.size());
  for (std::size_t i = 0; i < intTraceGroupsInfo.size(); i++)
    interiorTraceGroups.emplace_back( intTraceGroupsInfo[i] );

  // go back through the interior groups of the cell groups and add the ones that match the criteria into the actual interior groups
  for (std::size_t i = 0; i < interiorTraces.size(); i++)
  {
    int ipair = XField_Lagrange_Utils::traceGroupsIndex( interiorTraces[i], intTraceGroupsInfo );
    SANS_ASSERT( ipair >= 0 );

    interiorTraceGroups[ipair].add_trace( interiorTraces[i] );
  }
}


} // namespace SANS
