// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField_Lagrange_impl.h"

namespace SANS
{
// Explicit instantiations
template XField_Lagrange<PhysD3>::XField_Lagrange(const XField<PhysD3,TopoD2>& xfld, XFieldBalance graph);

}
