// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//#define SLEEP_FOR_OUTPUT
#ifdef SLEEP_FOR_OUTPUT
#define SLEEP_MILLISECONDS 500
#include <chrono>
#include <thread>
#endif
#include <ostream>
#include <string>
#include <fstream>
#include "XField_Lagrange.h"

#include "tools/output_std_vector.h"

#include "Field/Element/UniqueElemHash.h"

#include "Field/XFieldLine.h"
#include "Field/XFieldArea.h"
#include "Field/XFieldVolume.h"
#include "Field/XFieldSpacetime.h"

#include "Field/tools/for_each_CellGroup.h"
#include "Field/tools/for_each_BoundaryTraceGroup.h"
#include "pyrite_fstream.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#define XFIELD_LAGRANGE_INSTANTIATE
#include "XField_Lagrange_Distribute_impl.h"
#include "XField_Lagrange_ParMetis_impl.h"
#include "XField_Lagrange_balance_impl.h"


#ifdef SANS_MPI
#include "MPI/operations.h"

#include <boost/serialization/vector.hpp>
#include <boost/serialization/set.hpp>
#include <boost/serialization/map.hpp>
#include <boost/mpi/collectives/broadcast.hpp>
#include <boost/mpi/collectives/gather.hpp>
#include <boost/mpi/collectives/all_reduce.hpp>
#include <boost/mpi/collectives/all_gather.hpp>
#include <boost/mpi/collectives/all_to_all.hpp>

namespace boost
{
namespace mpi
{
  // This allows Boost.MPI to avoid extraneous copy operations
  // Only works when the datatype is of fixed size (no pointers or dynamically sized objects)
  template <class PhysDim>
  struct is_mpi_datatype< SANS::GlobalVectorX<PhysDim> > : mpl::true_ {};
} // namespace mpi
} // namespace boost

#endif // SANS_MPI

#include <numeric> // std::accumulate
#include <sstream>
#include <set>
#include <unordered_set>
#include <algorithm> // std::sort

namespace SANS
{

//===========================================================================//
//
// XField_Lagrange
//
//===========================================================================//

template<class PhysDim>
XField_Lagrange<PhysDim>::XField_Lagrange(mpi::communicator& comm, XFieldBalance graph)
  : comm_(std::make_shared<mpi::communicator>(comm)), comm_size_(comm_->size()), graph_(graph),
    nDOF_global_(0), iDOF_global_(0),
    nCell_global_(0), iCell_global_(0),
    nBnd_global_(0), iBnd_global_(0), bnd_comm_size_(0),
    nNode_global_(0)
{}

//---------------------------------------------------------------------------//
template<class PhysDim>
XField_Lagrange<PhysDim>::XField_Lagrange(mpi::communicator& comm, XFieldBalance graph, const std::string& filename)
  : comm_(std::make_shared<mpi::communicator>(comm)), comm_size_(comm_->size()), graph_(graph),
    nDOF_global_(0), iDOF_global_(0),
    nCell_global_(0), iCell_global_(0),
    nBnd_global_(0), iBnd_global_(0), bnd_comm_size_(0),
    nNode_global_(0)
{
  if (graph == XFieldBalance::CellPartitionRead)
     InputPartition(comm,filename);
  else
    SANS_DEVELOPER_EXCEPTION("Unkown graph partition.");
}

//---------------------------------------------------------------------------//
template<class PhysDim>
template<class TopoDim>
XField_Lagrange<PhysDim>::XField_Lagrange(const XField<PhysDim,TopoDim>& xfld, XFieldBalance graph)
  : comm_(xfld.comm()), comm_size_(comm_->size()), graph_(graph),
    nDOF_global_(0), iDOF_global_(0),
    nCell_global_(0), iCell_global_(0),
    nBnd_global_(0), iBnd_global_(0), bnd_comm_size_(0),
    nNode_global_(0)
{
  if (graph == XFieldBalance::Serial)
    copyElements(xfld);
  else if (graph == XFieldBalance::CellPartitionCopy)
    copyCellPartition(xfld);
  else
    SANS_DEVELOPER_EXCEPTION("Unkown graph partition.");
}

//---------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
class InitialCellBuffer : public GroupFunctorCellType<InitialCellBuffer<PhysDim,TopoDim>>
{
public:
  InitialCellBuffer( const XField<PhysDim, TopoDim>& xfld,
                     int& nCell_global,
                     std::vector<TopologyTypes>& cellTopos,
                     std::vector<int>& cellOrders,
                     std::vector<GlobalMPIElement>& cellBuffer )
    : xfld_(xfld), nCell_global_(nCell_global), cellTopos_(cellTopos), cellOrders_(cellOrders), cellBuffer_(cellBuffer) {}

  std::size_t nCellGroups() const          { return xfld_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return n;                   }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology >
  void
  apply(const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
        const int cellGroupGlobal)
  {
    int comm_rank = xfld_.comm()->rank();

    cellTopos_[cellGroupGlobal] = Topology::Topology;
    cellOrders_[cellGroupGlobal] = xfldCellGroup.order();

    int nElem = xfldCellGroup.nElem();
    const std::vector<int>& cellIDs = xfld_.cellIDs(cellGroupGlobal);

    std::vector<int> elemMap(xfldCellGroup.nBasis());

    std::size_t offset = cellBuffer_.size();
    cellBuffer_.resize(offset + nElem);

    for (int elem = 0; elem < nElem; elem++)
    {
      int rank = xfldCellGroup.associativity(elem).rank();
      xfldCellGroup.associativity(elem).getGlobalMapping(elemMap.data(), elemMap.size());

      // transform back to native DOF indexing
      for (std::size_t i = 0; i < elemMap.size(); i++)
        elemMap[i] = xfld_.local2nativeDOFmap(elemMap[i]);

      if (rank == comm_rank) nCell_global_++;

      cellBuffer_[offset + elem] = GlobalMPIElement( rank, cellGroupGlobal, cellIDs[elem], elemMap );
    }
  }

protected:
  const XField<PhysDim, TopoDim>& xfld_;
  int& nCell_global_;
  std::vector<TopologyTypes>& cellTopos_;
  std::vector<int>& cellOrders_;
  std::vector<GlobalMPIElement>& cellBuffer_;
};


//---------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
class InitialBndBuffer : public GroupFunctorBoundaryTraceType<InitialBndBuffer<PhysDim,TopoDim>>
{
public:
  InitialBndBuffer( const XField<PhysDim, TopoDim>& xfld,
                    int& nBnd_global,
                    std::vector<TopologyTypes>& bndTopos,
                    std::vector<GlobalMPIElement>& bndBuffer )
    : xfld_(xfld), nBnd_global_(nBnd_global), bndTopos_(bndTopos), bndBuffer_(bndBuffer) {}

  std::size_t nBoundaryTraceGroups() const          { return xfld_.nBoundaryTraceGroups(); }
  std::size_t boundaryTraceGroup(const int n) const { return n;                            }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology >
  void
  apply(const typename XField<PhysDim,TopoDim>::template FieldTraceGroupType<Topology>& xfldTraceGroup,
        const int traceGroupGlobal)
  {
    int comm_rank = xfld_.comm()->rank();

    bndTopos_[traceGroupGlobal] = Topology::Topology;

    int nElem = xfldTraceGroup.nElem();
    const std::vector<int>& boundaryTraceIDs = xfld_.boundaryTraceIDs(traceGroupGlobal);

    std::vector<int> nodeMap(Topology::NNode);

    std::size_t offset = bndBuffer_.size();
    bndBuffer_.resize(offset + nElem);

    for (int elem = 0; elem < nElem; elem++)
    {
      int rank = xfldTraceGroup.associativity(elem).rank();
      xfldTraceGroup.associativity(elem).getNodeGlobalMapping(nodeMap.data(), nodeMap.size());

      // transform back to native DOF indexing
      for (int i = 0; i < Topology::NNode; i++)
        nodeMap[i] = xfld_.local2nativeDOFmap(nodeMap[i]);

      if (rank == comm_rank) nBnd_global_++;

      bndBuffer_[offset + elem] = GlobalMPIElement( rank, traceGroupGlobal, boundaryTraceIDs[elem], nodeMap );
    }
  }

protected:
  const XField<PhysDim, TopoDim>& xfld_;
  int& nBnd_global_;
  std::vector<TopologyTypes>& bndTopos_;
  std::vector<GlobalMPIElement>& bndBuffer_;
};

//---------------------------------------------------------------------------//
template<class PhysDim>
template<class TopoDim>
void
XField_Lagrange<PhysDim>::copyElements(const XField<PhysDim,TopoDim>& xfld)
{
  cellTopos_.resize(xfld.nCellGroups(), eNode);
  cellOrders_.resize(xfld.nCellGroups(), -1);
  bndTopos_.resize(xfld.nBoundaryTraceGroups(), eNode);

  for_each_CellGroup<TopoDim>::apply(
      InitialCellBuffer<PhysDim, TopoDim>(xfld, nCell_global_, cellTopos_, cellOrders_, cellBuffer_),
      xfld );

  sizeCellGroups();

  for_each_BoundaryTraceGroup<TopoDim>::apply(
      InitialBndBuffer<PhysDim, TopoDim>(xfld, nBnd_global_, bndTopos_, bndBuffer_),
      xfld );

  sizeBoundaryTraceGroups();

  // copy over DOFs
  DOF_buffer_.resize(xfld.nDOF());

  for (int i = 0; i < xfld.nDOF(); i++)
  {
    int iDOF = xfld.local2nativeDOFmap(i);
    nDOF_global_ = max(iDOF, nDOF_global_);
    DOF_buffer_[i] = GlobalDOFType(iDOF, xfld.DOF(i));
  }

#ifdef SANS_MPI
  nCell_global_ = boost::mpi::all_reduce(*comm_, nCell_global_, std::plus<int>());
  nBnd_global_  = boost::mpi::all_reduce(*comm_, nBnd_global_, std::plus<int>());
  nDOF_global_  = boost::mpi::all_reduce(*comm_, nDOF_global_, boost::mpi::maximum<int>());
  nDOF_global_++; // change to a size rather than an index
#endif
}


//---------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
class CountPossessedCells : public GroupFunctorCellType<CountPossessedCells<PhysDim,TopoDim>>
{
public:
  CountPossessedCells( const XField<PhysDim, TopoDim>& xfld, int& nCell_global )
    : xfld_(xfld), nCell_global_(nCell_global) {}

  std::size_t nCellGroups() const          { return xfld_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return n;                   }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology >
  void
  apply(const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
        const int cellGroupGlobal)
  {
    const int comm_rank = xfld_.comm()->rank();

    const int nElem = xfldCellGroup.nElem();

    for (int elem = 0; elem < nElem; elem++)
      if (xfldCellGroup.associativity(elem).rank() == comm_rank)
        nCell_global_++;
  }

protected:
  const XField<PhysDim, TopoDim>& xfld_;
  int& nCell_global_;
};


//---------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
class CountPossessedBnd : public GroupFunctorBoundaryTraceType<CountPossessedBnd<PhysDim,TopoDim>>
{
public:
  CountPossessedBnd( const XField<PhysDim, TopoDim>& xfld, int& nBnd_global )
    : xfld_(xfld), nBnd_global_(nBnd_global) {}

  std::size_t nBoundaryTraceGroups() const          { return xfld_.nBoundaryTraceGroups(); }
  std::size_t boundaryTraceGroup(const int n) const { return n;                            }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology >
  void
  apply(const typename XField<PhysDim,TopoDim>::template FieldTraceGroupType<Topology>& xfldTraceGroup,
        const int traceGroupGlobal)
  {
    const int comm_rank = xfld_.comm()->rank();

    const int nElem = xfldTraceGroup.nElem();

    for (int elem = 0; elem < nElem; elem++)
      if (xfldTraceGroup.associativity(elem).rank() == comm_rank)
        nBnd_global_++;
  }

protected:
  const XField<PhysDim, TopoDim>& xfld_;
  int& nBnd_global_;
};


//---------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
class CopyCellPartition : public GroupFunctorCellType<CopyCellPartition<PhysDim,TopoDim>>
{
public:
  CopyCellPartition( const XField<PhysDim, TopoDim>& xfld,
                     const int nCell_global,
                     std::vector<std::vector<std::pair<int,int>>>& sendCellPartition )
    : xfld_(xfld), nCell_global_(nCell_global), sendCellPartition_(sendCellPartition) {}

  std::size_t nCellGroups() const          { return xfld_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return n;                   }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology >
  void
  apply(const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
        const int cellGroupGlobal)
  {
    const int comm_size = xfld_.comm()->size();

    const int nElem = xfldCellGroup.nElem();
    const std::vector<int>& cellIDs = xfld_.cellIDs(cellGroupGlobal);

    // Compute the number of elements on each processor
    int ElemperProc = nCell_global_ / comm_size;

    for (int elem = 0; elem < nElem; elem++)
    {
      int rank = xfldCellGroup.associativity(elem).rank();

      // Make sure the rank does not go beyond the
      // total number of processors due to the remainders in the partition
      int implicitRank = MIN(comm_size-1, cellIDs[elem] / ElemperProc);

      if (implicitRank == comm_size-1)
        implicitRank = 0; //The last set of elements is stored on rank 0
      else
        implicitRank++; // offset the rank

      // save off the rank for the cell indixed by the processor where it will be sent
      sendCellPartition_[implicitRank].emplace_back(cellIDs[elem], rank);
    }
  }

protected:
  const XField<PhysDim, TopoDim>& xfld_;
  const int nCell_global_;
  std::vector<std::vector<std::pair<int,int>>>& sendCellPartition_;
};

//---------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
class CopyBndPartition : public GroupFunctorBoundaryTraceType<CopyBndPartition<PhysDim,TopoDim>>
{
public:
  CopyBndPartition( const XField<PhysDim, TopoDim>& xfld,
                    const int nCell_global,
                    const int nBnd_global,
                    std::vector<std::vector<std::pair<int,int>>>& sendBndPartition )
    : xfld_(xfld), nCell_global_(nCell_global), nBnd_global_(nBnd_global), sendBndPartition_(sendBndPartition) {}

  std::size_t nBoundaryTraceGroups() const          { return xfld_.nBoundaryTraceGroups(); }
  std::size_t boundaryTraceGroup(const int n) const { return n;                            }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology >
  void
  apply(const typename XField<PhysDim,TopoDim>::template FieldTraceGroupType<Topology>& xfldTraceGroup,
        const int traceGroupGlobal)
  {
    int comm_size = xfld_.comm()->size();

    int nElem = xfldTraceGroup.nElem();
    const std::vector<int>& boundaryTraceIDs = xfld_.boundaryTraceIDs(traceGroupGlobal);

    int ElemperProc = nCell_global_ / comm_size;

    // compute the number of processors that will receive boundary trace elements
    const int bnd_comm_size = std::min(comm_size, std::max(1, (nBnd_global_ - nBnd_global_ % ElemperProc) / ElemperProc));

    for (int elem = 0; elem < nElem; elem++)
    {
      int rank = xfldTraceGroup.associativity(elem).rank();

      // Make sure the rank does not go beyond the
      // total number of processors due to the remainders in the partition
      int implicitRank = MIN(bnd_comm_size-1, boundaryTraceIDs[elem] / ElemperProc);

      if (implicitRank == bnd_comm_size-1)
        implicitRank = 0; //The last set of elements is stored on rank 0
      else
        implicitRank++; // offset the rank

      sendBndPartition_[implicitRank].emplace_back(boundaryTraceIDs[elem], rank);

    }
  }

protected:
  const XField<PhysDim, TopoDim>& xfld_;
  const int nCell_global_;
  const int nBnd_global_;
  std::vector<std::vector<std::pair<int,int>>>& sendBndPartition_;
};

//---------------------------------------------------------------------------//
template<class PhysDim>
template<class TopoDim>
void
XField_Lagrange<PhysDim>::copyCellPartition(const XField<PhysDim,TopoDim>& xfld)
{
  int nCell_global = 0, nBnd_global = 0;

  for_each_CellGroup<TopoDim>::apply( CountPossessedCells<PhysDim, TopoDim>(xfld, nCell_global), xfld );

  for_each_BoundaryTraceGroup<TopoDim>::apply(CountPossessedBnd<PhysDim, TopoDim>(xfld, nBnd_global), xfld );

#ifdef SANS_MPI
  nCell_global = boost::mpi::all_reduce(*comm_, nCell_global, std::plus<int>());
  nBnd_global  = boost::mpi::all_reduce(*comm_, nBnd_global , std::plus<int>());
#endif

  // populate vectors to send the partitioning to processors based on the implicit partitioning from
  // addCell and addBoundaryTrace
  PartitionType sendCellPartition(comm_size_);
  PartitionType sendBndPartition(comm_size_);

  for_each_CellGroup<TopoDim>::apply(
      CopyCellPartition<PhysDim, TopoDim>(xfld, nCell_global, sendCellPartition), xfld );

  for_each_BoundaryTraceGroup<TopoDim>::apply(
      CopyBndPartition<PhysDim, TopoDim>(xfld, nCell_global, nBnd_global, sendBndPartition), xfld );

#ifdef SANS_MPI
  PartitionType recvCellPartition;
  PartitionType recvBndPartition;

  // distribute the partitioning to all the processors
  boost::mpi::all_to_all(*comm_, sendCellPartition, cellpartition_);
  boost::mpi::all_to_all(*comm_, sendBndPartition, bndpartition_);

#else

  // partition is dummy in serial
  cellpartition_ = std::move(sendCellPartition);
  bndpartition_ = std::move(sendBndPartition);

#endif
}

// ----------------------------------------------------------------------------------------------------//
template<class PhysDim>
template<class TopoDim>
void
XField_Lagrange<PhysDim>::outputPartition(const XField<PhysDim,TopoDim>& xfld, const std::string& filename)
{
  std::shared_ptr<mpi::communicator> comm = xfld.comm();  // communicator for MPI processors

  int nCell_global = 0, nBnd_global = 0;

  for_each_CellGroup<TopoDim>::apply( CountPossessedCells<PhysDim, TopoDim>(xfld, nCell_global), xfld );

  for_each_BoundaryTraceGroup<TopoDim>::apply(CountPossessedBnd<PhysDim, TopoDim>(xfld, nBnd_global), xfld );

#ifdef SANS_MPI
  nCell_global = boost::mpi::all_reduce(*comm, nCell_global, std::plus<int>());
  nBnd_global  = boost::mpi::all_reduce(*comm, nBnd_global , std::plus<int>());
#endif

  // populate vectors to send the partitioning to processors based on the implicit partitioning from
  // addCell and addBoundaryTrace
  PartitionType sendCellPartition(comm->size());
  PartitionType sendBndPartition(comm->size());

  for_each_CellGroup<TopoDim>::apply(
      CopyCellPartition<PhysDim, TopoDim>(xfld, nCell_global, sendCellPartition), xfld );

  for_each_BoundaryTraceGroup<TopoDim>::apply(
      CopyBndPartition<PhysDim, TopoDim>(xfld, nCell_global, nBnd_global, sendBndPartition), xfld );

  int cellcount, bndcount;

  cellcount = 0;
  bndcount = 0;

  for (std::size_t i = 0; i < sendCellPartition.size(); i++)
    for (std::size_t j = 0; j < sendCellPartition[i].size(); j++)
      cellcount = cellcount + 1;

  for (std::size_t ii = 0; ii < sendBndPartition.size(); ii++)
    for (std::size_t jj = 0; jj < sendBndPartition[ii].size(); jj++)
      bndcount += 1;

  std::cout << "output Partition: filename = " << filename << std::endl;
  FILE* fp = fopen( filename.c_str(), "w" );

  fprintf(fp, "Cell Partition\n");
  fprintf(fp,"%i\n", cellcount);
  for (std::size_t k = 0; k < sendCellPartition.size(); k++)
    for (std::size_t kk = 0; kk < sendCellPartition[k].size(); kk++)
      fprintf( fp,"%d %d %d\n", (int)k, sendCellPartition[k][kk].first, sendCellPartition[k][kk].second);

  fprintf(fp, "Boundary Partition\n");
  fprintf(fp,"%i\n", bndcount);
  for (std::size_t k = 0; k < sendBndPartition.size(); k++)
    for (std::size_t kk = 0; kk < sendBndPartition[k].size(); kk++)
      fprintf( fp,"%d %d %d\n", (int)k, sendBndPartition[k][kk].first, sendBndPartition[k][kk].second);

  fprintf(fp,"end");
  fclose(fp);
}

// ----------------------------------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::InputPartition(mpi::communicator& comm, const std::string& filename)
{
  // populate vectors to send the partitioning to processors based on the implicit partitioning from
  // addCell and addBoundaryTrace
  PartitionType sendCellPartition(comm_size_);
  PartitionType sendBndPartition(comm_size_);
  int n = 0, m = 0, j = 0, li = 0, oi= 0;

  std::string line;
  std::ifstream f(filename);

  getline( f, line );
  getline( f, line );
  std::istringstream ss( line );
  ss >> n;
  for (int i = 0; i < n; i++)
  {
    getline( f, line );
    std::istringstream ss( line );
    ss >> j >> m >> oi;
    //std::cout << " Cell " << j << " " << m << " " << oi <<  std::endl;
    sendCellPartition[j].emplace_back(m,oi);
  }
  getline( f, line );
  getline( f, line );
  std::istringstream sss(line);
  sss >> li;
  for (int i = 0; i < li; i++)
  {
    getline( f, line );
    std::istringstream ss( line );
    ss >> j >> m >> oi;
    //std::cout << " boundary " << j << " " << m << " " << oi << std::endl;
    sendBndPartition[j].emplace_back(m,oi);
  }

  //dumpBuffer( "Buffer", cellBuffer_);
#ifdef SANS_MPI

  // distribute the partitioning to all the processors
  boost::mpi::all_to_all(comm, sendCellPartition, cellpartition_);
  boost::mpi::all_to_all(comm, sendBndPartition, bndpartition_);

#endif
  //copyElements(xfld);
}

#if 0
template<class PhysDim>
void
XField_Lagrange<PhysDim>::
listTraceElements( std::vector<LagrangeTraceElement>& traces ) const
{
  std::set<UniqueTrace> uniqueTraces( cellGroups_.size() );

  for (std::size_t igroup = 0; igroup < cellGroups_.size(); igroup++)
  {
    for (int ielem = 0; ielem < cellGroups_[igroup]->nElem(); ielem++)
    {
      std::vector< UniqueTrace > elemTraces = cellGroups_[igroup]->uniqueTraces(ielem);

      for (std::size_t itrace = 0; itrace < elemTraces.size(); itrace++ )
      {
        bool traceFound = false;
        for (std::size_t jgroup = 0; jgroup < uniqueTraces.size(); jgroup++)
        {
          // Look for the current trace in the jth cell group
          UniqueTraceSet::iterator it = uniqueTraces[jgroup].find(elemTraces[itrace]);
          if ( it != uniqueTraces[jgroup].end() )
          {
            SANS_ASSERT( *it == elemTraces[itrace] );

            traceFound = true;

            // get left properties
            int groupL = igroup;
            int elemL  = ielem;
            int traceL = itrace;
            int orderL = cellGroups_[groupL]->order();
            int rankL  = cellGroups_[groupL]->elem(elemL).rank;

            // get right properties
            int groupR = jgroup;
            int elemR  = it->cellElem;
            int traceR = it->canonicalTrace;
            int orderR = cellGroups_[groupR]->order();
            int rankR  = cellGroups_[groupR]->elem(elemR).rank;

            // Sort so that the left group is always the lower index or lower order
            if ( orderL > orderR || groupL > groupR )
            {
              std::swap(elemL, elemR);
              std::swap(groupL, groupR);
              std::swap(traceL, traceR);
              std::swap(orderL, orderR);
              std::swap(rankL, rankR);
            }

            // get the trace topology
            TopologyTypes topoT = elemTraces[itrace].topo;

            // get the FULL trace with all the high-order nodes from the left element so that it is the canonicalTrace
            std::vector<int> traceDOFs = cellGroups_[groupL]->traceDOFs( elemL, traceL );

            traces.emplace_back(rankL, -1, topoT, traceDOFs, orderL, groupL, elemL, traceL, groupR, elemR, traceR);

            // remove the trace since we found a match
            uniqueTraces[jgroup].erase(elemTraces[itrace]);
            break;
          }
        }

        // Add the trace so it may be found by a different element
        if (!traceFound)
          uniqueTraces[igroup].insert(elemTraces[itrace]);

      } // itrace
    }// ielem
  } // igroup

}
#endif

//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::
getConnectedTraceGroups( std::vector<LagrangeConnectedGroup>& interiorTraceGroups,
                         std::vector<LagrangeConnectedGroup>& boundaryTraceGroups,
                         std::vector<LagrangeConnectedGroup>& ghostBoundaryTraceGroups ) const
{
  // Nothing to do...
  if (cellGroups_.size()==0)
    return;

  interiorTraceGroups.clear();
  boundaryTraceGroups.clear();
  ghostBoundaryTraceGroups.clear();

  // buckets of unique trace elements
  std::vector< UniqueTraceMap > uniqueTraces(cellGroups_.size());

  // ===================
  //
  //  Interior groups
  //
  // ===================

  // build up a list of interior traces, unique traces and interior trace groups
  connectInteriorGroups(cellGroups_, uniqueTraces, interiorTraceGroups);

  // ===================
  //
  //  Boundary groups
  //
  // ===================
  connectBoundaryGroups(bndGroups_     , uniqueTraces, boundaryTraceGroups     , true );
  connectBoundaryGroups(ghostBndGroups_, uniqueTraces, ghostBoundaryTraceGroups, false);

  // Make sure that we have consumed all trace elements
  // TODO: Need much better error message here. This likely means the BC information is not complete
  for (std::size_t jgroup = 0; jgroup < uniqueTraces.size(); jgroup++)
  {
    for ( const std::pair<const UniqueElem, UniqueTrace>& trace : uniqueTraces[jgroup] )
      std::cout << comm_->rank() << " trace " << trace.second << trace.first << std::endl;

    SANS_ASSERT( uniqueTraces[jgroup].empty() );
  }
}

//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::
connectBoundaryGroups( const std::vector<LagrangeElementGroup_ptr>& bndGroups,
                       std::vector<UniqueTraceMap>& uniqueTraces,
                       std::vector<LagrangeConnectedGroup>& boundaryGroups,
                       const bool syncorder) const
{
  // Only boundary traces should remain in 'uniqueTraces'.
  // This makes connecting the specified boundary elements relatively simple

  for (std::size_t i = 0; i < bndGroups.size(); i++)
  {
    // might be nothing to do. This happens in particular for MPI partitions
    if (bndGroups[i]->nElem() == 0)
    {
      boundaryGroups.emplace_back( LagrangeConnectedGroup::Empty(), bndGroups[i]->topo );
      continue;
    }

    // Trace elements of the boundary group
    std::vector<LagrangeTraceElement> traces;

    // go through each linear trace and find the element it's attached to, and retrieve the high-order nodes
    for (int j = 0; j < bndGroups[i]->nElem(); j++)
    {
      // Get the sorted nodes from the boundary trace element
      UniqueElem uniqueTrace = bndGroups[i]->uniqueElem(j);

      // Find the interior cell
      bool traceFound = false;
      for (std::size_t jgroup = 0; jgroup < uniqueTraces.size(); jgroup++)
      {
        UniqueTraceMap::iterator it = uniqueTraces[jgroup].find(uniqueTrace);
        if (it != uniqueTraces[jgroup].end())
        {
          traceFound = true;

          // get left properties
          int groupL = jgroup;
          int elemL  = it->second.cellElem;
          int traceL = it->second.canonicalTrace;
          int orderL = cellGroups_[groupL]->order();
          int rank   = bndGroups[i]->elem(j).rank;
          int elemID = bndGroups[i]->elem(j).elemID;

          TopologyTypes topoL = it->first.topo;

          // Get all the trace DOFs
          std::vector<int> traceDOFs = cellGroups_[groupL]->traceDOFs( elemL , traceL );

          traces.emplace_back(rank, elemID,
                              topoL, traceDOFs,
                              orderL, groupL, elemL, traceL, -1, -1, -1); // no right element

          // remove the trace since we found a match
          uniqueTraces[jgroup].erase(uniqueTrace);
          break;
        }
      }

      if ( !traceFound )
      {
        std::stringstream errmsg;
        errmsg << "boundary trace should have been found..." << std::endl;
#ifdef SANS_MPI
        errmsg << " rank = " << comm_->rank() << std::endl;
#endif
        errmsg << " nodes = " << bndGroups[i]->elem(j).elemMap << std::endl;
        SANS_DEVELOPER_EXCEPTION(errmsg.str());
      }
    }

    // there should only be one trace group for each boundary group (at least for now)
    std::vector< TraceGroupInfo > traceGroups = XField_Lagrange_Utils::countTraceGroups( traces );
    if ( traceGroups.size() != 1 )
    {
      std::stringstream errmsg;
      errmsg << "boundary trace group should not have been split into multiple groups" << std::endl;
#ifdef SANS_MPI
      errmsg << " rank = " << comm_->rank() << std::endl;
#endif
      errmsg << "Boundary trace " << std::endl;
      for (int j = 0; j < bndGroups[i]->nElem(); j++)
        errmsg << "nodes " << bndGroups[i]->elem(j).elemMap
               << "ID " << bndGroups[i]->elem(j).elemID << std::endl;

      SANS_DEVELOPER_EXCEPTION(errmsg.str());
    }

    boundaryGroups.emplace_back( traceGroups[0] );

    // go back through the traces and add them to the boundaryGroup
    for (std::size_t j = 0; j < traces.size(); j++)
      boundaryGroups[i].add_trace( traces[j] );
  }

#ifdef SANS_MPI
  if (syncorder)
  {
    // Make sure all groups have the same order (particularly empty groups)
    std::vector<int> bndOrders(boundaryGroups.size());
    for (std::size_t i = 0; i < boundaryGroups.size(); i++)
      bndOrders[i] = boundaryGroups[i].order();

    // reduce down the maximum order accorss all processors
    bndOrders = boost::mpi::all_reduce(*comm_, bndOrders, boost::mpi::maximum<std::vector<int>>());

    for (std::size_t i = 0; i < boundaryGroups.size(); i++)
      if (boundaryGroups[i].order() != bndOrders[i])
      {
        // can only change the order if the group is empty
        SANS_ASSERT(boundaryGroups[i].nElem() == 0 );
        boundaryGroups[i].setOrder(bndOrders[i]);
      }
  }
#endif
}

//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::
getPeriodicElemMap( const std::vector<LagrangeConnectedGroup>& boundaryGroups,
                    std::vector<PeriodicBCNodeMap>& periodicElemMaps ) const
{
  periodicElemMaps.clear();
  periodicElemMaps.resize(periodicity_.size());

  std::vector<int> mappedNodes;

  // Loop over all periodic definitions and update the connectivity
  // in the left boundary group, and clear the right group
  for ( std::size_t ip = 0; ip < periodicity_.size(); ip++ )
  {
    const PeriodicBCNodeMap& periodNode = periodicity_[ip];

    PeriodicBCNodeMap& periodElem = periodicElemMaps[ip];

    int bndGroupIDL = periodNode.bndGroupL;
    int bndGroupIDR = periodNode.bndGroupR;

    periodElem.bndGroupL = bndGroupIDL;
    periodElem.bndGroupR = bndGroupIDR;

    const LagrangeConnectedGroup& bndGroupL = boundaryGroups[bndGroupIDL];
    const LagrangeConnectedGroup& bndGroupR = boundaryGroups[bndGroupIDR];

    int nElemL = bndGroupL.nElem();
    int nElemR = bndGroupR.nElem();

    // go through each linear trace and find the element it's attached to, and retrieve the high-order nodes
    for (int iL = 0; iL < nElemL; iL++)
    {
      // Get the sorted nodes from the boundary trace element
      UniqueElem uniqueTraceL = bndGroupL.uniqueElem(iL);

      // Convert the nodes based on the periodicity
      const std::vector<int>& sortedNodesL = uniqueTraceL.sortedNodes();
      mappedNodes.resize(sortedNodesL.size());
      for (std::size_t node = 0; node < sortedNodesL.size(); node++)
      {
        try
        {
          mappedNodes[node] = periodNode.mapLtoR.at(sortedNodesL[node]);
        }
        catch ( const std::range_error& )
        {
          std::stringstream errmsg;
          errmsg << "periodic node is missing for node: " << sortedNodesL[node] << std::endl;
#ifdef SANS_MPI
          errmsg << "  rank = " << comm_->rank() << std::endl;
#endif
          errmsg << "  Left  Periodic Group = " << bndGroupIDL << std::endl;
          errmsg << "  Right Periodic Group = " << bndGroupIDR << std::endl;
          SANS_RUNTIME_EXCEPTION( errmsg.str() );
        }
      }

      // Create a new unique trace based on the mapped nodes
      UniqueElem uniqueTraceRmapped(uniqueTraceL.topo, mappedNodes);

      bool traceFound = false;
      // Loop over all the right boundary elements and find the match
      for (int iR = 0; iR < nElemR; iR++)
      {
        UniqueElem uniqueTraceR = bndGroupR.uniqueElem(iR);

        // Check if the mapped trace from the right matches the trace on the left
        if (uniqueTraceR == uniqueTraceRmapped)
        {
          periodElem.mapLtoR[iL] = iR;

          // There should only be one match...
          traceFound = true;
          break;
        }
      }

      if (!traceFound)
      {
        std::stringstream errmsg;
        errmsg << "periodic trace should have been found..." << std::endl;
#ifdef SANS_MPI
        errmsg << " rank = " << comm_->rank() << std::endl;
#endif
        errmsg << " uniqueTraceL       = " << uniqueTraceL << std::endl;
        errmsg << " uniqueTraceRmapped = " << uniqueTraceRmapped << std::endl;

        SANS_RUNTIME_EXCEPTION( errmsg.str() );
      }
    }
  }
}

//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::
mergePeriodicGroups( std::vector<LagrangeConnectedGroup>& boundaryGroups ) const
{
  std::vector<PeriodicBCNodeMap> periodicElemMaps;
  getPeriodicElemMap( boundaryGroups, periodicElemMaps );

  // Loop over all periodic definitions and update the connectivity
  // in the left boundary group, and clear the right group
  for ( const PeriodicBCNodeMap& period : periodicElemMaps )
  {
    int bndGroupIDL = period.bndGroupL;
    int bndGroupIDR = period.bndGroupR;

    LagrangeConnectedGroup& bndGroupL = boundaryGroups[bndGroupIDL];
    LagrangeConnectedGroup& bndGroupR = boundaryGroups[bndGroupIDR];

    // Trace elements of the boundary group
    std::vector<LagrangeTraceElement> traces;

    // loop over all BC element pairs
    for ( const std::pair<int,int>& elemMap : period.mapLtoR )
    {
      // Get the right and left boundary elements
      int iL = elemMap.first;
      int iR = elemMap.second;

      // get left properties
      int groupL = bndGroupL.groupL();
      int elemL  = bndGroupL.elemL(iL);
      int traceL = bndGroupL.traceL(iL);
      int orderL = bndGroupL.order();
      int rank   = bndGroupL.elem(iL).rank;
      int elemID = bndGroupL.elem(iL).elemID;

      // get right properties
      // The left cell group of the right boundary
      // is the right cell group of the left boundary
      int groupR = bndGroupR.groupL();
      int elemR  = bndGroupR.elemL(iR);
      int traceR = bndGroupR.traceL(iR);

      TopologyTypes topoL = bndGroupL.topo;

      // Get all the trace DOFs
      std::vector<int> traceDOFs = bndGroupL.elem( iL ).elemMap;

      traces.emplace_back(rank, elemID, topoL, traceDOFs, orderL,
                          groupL, elemL, traceL,
                          groupR, elemR, traceR);
    }

    // Remove all the elements from the right boundary, but don't remove the boundary
    bndGroupR.clear();

    int cellGroupR = -1;
    if (traces.size() > 0)
      cellGroupR = traces[0].groupR();

#ifdef SANS_MPI
    // reduce down the maximum cell group number accorss all processrs
    cellGroupR = boost::mpi::all_reduce(*comm_, cellGroupR, boost::mpi::maximum<int>());
#endif

    // Replace all the information in the left group
    bndGroupL.reset(cellGroupR);

    // go back through the traces and add them to the boundaryGroup
    for (std::size_t j = 0; j < traces.size(); j++)
      bndGroupL.add_trace( traces[j] );
  }

}

//---------------------------------------------------------------------------//
template<class PhysDim>
std::map<int,int>
XField_Lagrange<PhysDim>::continuousNodeMap()
{
  return continuousNodeMap( cellBuffer_ );
}

//---------------------------------------------------------------------------//
template<class PhysDim>
std::map<int,int>
XField_Lagrange<PhysDim>::
continuousNodeMap( const std::vector<GlobalMPIElement>& buffer )
{
#ifdef SANS_MPI
  int comm_rank = comm_->rank();

  // Compute the number of DOFs on each processor
  int DOFperProc = nDOF_global_ / comm_size_;

  std::vector<std::set<int>> isNodeSend(comm_size_);
  std::vector<std::set<int>> isNodeRecv(comm_size_);

  // Find node DOFs on the current and other processor
  for (std::size_t elem = 0; elem < buffer.size(); elem++ )
  {
    const std::vector<int>& canonicalNodes = cellGroups_[buffer[elem].group]->canonicalNodes();
    for (std::size_t n = 0; n < canonicalNodes.size(); n++)
    {
      int nodeGlobal = buffer[elem].elemMap[canonicalNodes[n]];

      // get the rank that 'possess' the node
      int rank = MIN(comm_size_-1, nodeGlobal / DOFperProc);

      isNodeSend[rank].insert(nodeGlobal);
    }
  }

  // add nodes from boundary elements and check that the periodicity is valid
  for (std::size_t elem = 0; elem < bndBuffer_.size(); elem++ )
  {
    const int bndGroup = bndBuffer_[elem].group;
    const std::vector<int>& canonicalNodes = bndGroups_[bndGroup]->canonicalNodes();
    for (std::size_t n = 0; n < canonicalNodes.size(); n++)
    {
      int nodeGlobal = bndBuffer_[elem].elemMap[canonicalNodes[n]];

      // get the rank that 'possess' the node
      int rank = MIN(comm_size_-1, nodeGlobal / DOFperProc);

      isNodeSend[rank].insert(nodeGlobal);

      // Also check nodes based on periodicity of the domain
      for ( const PeriodicBCNodeMap& period : periodicity_ )
      {
        if (period.bndGroupL == bndGroup)
        {
          try
          {
            period.mapLtoR.at(nodeGlobal);
          }
          catch (std::exception& e)
          {
            SANS_DEVELOPER_EXCEPTION("Periodic information incomplete for boundary %d.\nMissing map for DOF %d.", bndGroup, nodeGlobal);
          }
        }
      }
    }
  }

  // Find any periodic mapping based on the nodes
  if (periodicity_.size() > 0)
  {
    std::vector<std::set<int>> isNodePeriod(comm_size_);

    for ( const std::set<int>& nodeRank : isNodeSend )
    {
      for ( const int& nodeGlobal : nodeRank )
      {
        // Also use nodes based on periodicity of the domain
        // this guarantees that periodic cells show up on the same rank
        for ( const PeriodicBCNodeMap& period : periodicity_ )
        {
          auto itR = period.mapRtoL.find(nodeGlobal);
          if (itR != period.mapRtoL.end())
          {
            int nodeGlobalL = itR->second;

            // get the rank that 'possess' the node on the left boundary
            int rank = MIN(comm_size_-1, nodeGlobalL / DOFperProc);

            isNodePeriod[rank].insert(nodeGlobalL);
          }
        }
      }
    }

    // add the periodic nodes to the send nodes
    for ( int rank = 0; rank < comm_size_; rank++ )
      isNodeSend[rank].insert(isNodePeriod[rank].begin(), isNodePeriod[rank].end());
  }


#if 0 // for debugging
  for (int rank = 0; rank < comm_size_; rank++)
  {
    comm_->barrier();
    if (comm_rank != rank) continue;

    std::cout << " send nodes rank " << rank << std::endl;
    for (const std::set<int>& globalNodeVec : isNodeSend )
    {
      for (int globalNode : globalNodeVec )
        std::cout << globalNode << " ";
      std::cout << std::endl;
    }
    std::cout << std::flush;
  }
#endif

  /*
    rank    send buf                        recv buf
    ----    --------                        --------
     0      a,b,c          MPI_Alltoall     a,A,#
     1      A,B,C        ---------------->  b,B,@
     2      #,@,%                           c,C,%

    (a more elaborate case with two elements per process)

    rank    send buf                        recv buf
    ----    --------                        --------
     0      a,b,c,d,e,f    MPI_Alltoall     a,b,A,B,#,@
     1      A,B,C,D,E,F  ---------------->  c,d,C,D,%,$
     2      #,@,%,$,&,*                     e,f,E,F,&,*
   */

  // Send node indices to other processors that 'possess' them
  boost::mpi::all_to_all(*comm_, isNodeSend, isNodeRecv);
  isNodeSend.clear();

  // collapse the nodes received from other processors
  std::set<int> isNode;
  for (int rank = 0; rank < comm_size_; rank++)
    isNode.insert(isNodeRecv[rank].begin(), isNodeRecv[rank].end());

  // compute the total number of nodes across all processors
  int nNode = isNode.size();
  std::vector<int> nNodeOnRank;
  boost::mpi::all_gather(*comm_, nNode, nNodeOnRank);

  // compute the total number of nodes
  if (nNode_global_ == 0)
    nNode_global_ = std::accumulate(nNodeOnRank.begin(), nNodeOnRank.end(), 0);

  // compute the node offset from lower ranks
  int node_offset = std::accumulate(nNodeOnRank.begin(), nNodeOnRank.begin()+comm_rank, 0);

  // create a map from sparse global node index to a continuous index
  std::map<int,int> globalNodeMap;
  for ( int globalNode : isNode )
    globalNodeMap[globalNode] = node_offset++;

  std::vector<std::map<int,int>> gloablNodeMapSend(comm_size_);
  std::vector<std::map<int,int>> globalNodeMapRecv(comm_size_);

  for (int rank = 0; rank < comm_size_; rank++)
    for (int globalNode : isNodeRecv[rank])
      gloablNodeMapSend[rank][globalNode] = globalNodeMap.at(globalNode);

  // set the node maps back to the other ranks
  boost::mpi::all_to_all(*comm_, gloablNodeMapSend, globalNodeMapRecv);
  gloablNodeMapSend.clear();

  // complete the global map from the other ranks
  globalNodeMap.clear();
  for (int rank = 0; rank < comm_size_; rank++)
    globalNodeMap.insert(globalNodeMapRecv[rank].begin(), globalNodeMapRecv[rank].end());

#if 0 // for debugging
  for (int rank = 0; rank < comm_size_; rank++)
  {
    comm_->barrier();
    if (comm_rank != rank) continue;

    std::cout << " globalNodeMap rank " << rank << std::endl;
    for (auto map : globalNodeMap )
      std::cout << map.first << " " << map.second << std::endl;
    std::cout << std::flush;
#ifdef SLEEP_FOR_OUTPUT
    std::this_thread::sleep_for( std::chrono::milliseconds(SLEEP_MILLISECONDS) );
#endif
  }
  std::cout << std::flush;
#ifdef SLEEP_FOR_OUTPUT
  std::this_thread::sleep_for( std::chrono::milliseconds(SLEEP_MILLISECONDS) );
#endif
  comm_->barrier();
#endif

#else // Serial implementation

  // Count the number of node DOFs
  std::set<int> isNode;
  for (std::size_t cellElem = 0; cellElem < buffer.size(); cellElem++ )
  {
    const std::vector<int>& canonicalNodes = cellGroups_[buffer[cellElem].group]->canonicalNodes();
    for (std::size_t n = 0; n < canonicalNodes.size(); n++)
       isNode.insert(buffer[cellElem].elemMap[canonicalNodes[n]]);
  }

  // compute the total number of nodes
  if (nNode_global_ == 0)
    nNode_global_ = isNode.size();

  int node_offset = 0;
  // create a map to map sparse global node index to a continuous index
  std::map<int,int> globalNodeMap;
  for ( int globalNode : isNode )
    globalNodeMap[globalNode] = node_offset++;

#endif

  return globalNodeMap;
}

//---------------------------------------------------------------------------//
template<class PhysDim>
std::map<int,int>
XField_Lagrange<PhysDim>::continuousNodeRank( const std::vector<GlobalMPIElement>& buffer )
{
  return continuousNodeRank(continuousNodeMap(buffer));
}

//---------------------------------------------------------------------------//
template<class PhysDim>
std::map<int,int>
XField_Lagrange<PhysDim>::continuousNodeRank( const std::map<int,int>& globalNodeMap )
{
  std::map<int,int> globalNodeRank;

  // Compute the number of nodes on each processor
  int NodePerProc = MAX(1, nNode_global_ / comm_size_);

  // Compute the implicit rank based on the continuous node numbering
  for (const std::pair<int,int>& node : globalNodeMap)
  {
    int rank = MIN(comm_size_-1, node.second / NodePerProc);
    globalNodeRank[node.first] = rank;
  }

#if 0
  // make sure periodic nodes have the same rank
  for ( const PeriodicBCNodeMap& period : periodicity_ )
  {
    for (const std::pair<int,int>& it : period.mapRtoL)
    {
      int nodeGlobalR = it.first;
      int nodeGlobalL = it.second;

      auto itR = globalNodeRank.find(nodeGlobalR);
      if (itR != globalNodeRank.end())
        globalNodeRank[nodeGlobalL] = itR->second;
    }
  }
#endif

  return globalNodeRank;
}

//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::
continuousCellMap(std::map<int,int>& globalCellMap, std::vector<int>& nCellOnRank)
{
  globalCellMap.clear();
  nCellOnRank.clear();

#ifdef SANS_MPI

  // construct a map from global element numbering to a continuous numbering
  // on each processor
  int comm_rank = comm_->rank();

  // used to inform which processors need which parts of the map
  std::vector<std::set<int>> needCell(comm_size_);
  std::vector<std::set<int>> sendCell(comm_size_);

  // compute the number of cells possessed by each processor
  int nCell = 0;
  for (std::size_t elem = 0; elem < cellBuffer_.size(); elem++)
  {
    const int rank   = cellBuffer_[elem].rank;
    const int elemID = cellBuffer_[elem].elemID;

    needCell[rank].insert(elemID);
    if (comm_rank == rank)
      nCell++;
  }

  // send the count to all other processors
  boost::mpi::all_gather(*comm_, nCell, nCellOnRank);

  // send cell numbers to other processors that 'possess' them
  boost::mpi::all_to_all(*comm_, needCell, sendCell);
  needCell.clear();

  // compute the cell offset from lower ranks
  int cell_offset = std::accumulate(nCellOnRank.begin(), nCellOnRank.begin() + comm_rank, 0);

  // create a map from sparse global cell index to a continuous index
  for (std::size_t elem = 0; elem < cellBuffer_.size(); elem++)
  {
    const int rank   = cellBuffer_[elem].rank;
    const int elemID = cellBuffer_[elem].elemID;

    if (comm_rank == rank)
      globalCellMap[elemID] = cell_offset++;
  }

  // redistribute relevant parts of the cell map to the other processors
  std::vector<std::map<int,int>> globalCellMapSend(comm_size_);
  std::vector<std::map<int,int>> globalCellMapRecv(comm_size_);

  for (int rank = 0; rank < comm_size_; rank++)
    for (const int& elemID : sendCell[rank] )
      globalCellMapSend[rank][elemID] = globalCellMap.at(elemID);

  // send the cell maps back to the other ranks
  boost::mpi::all_to_all(*comm_, globalCellMapSend, globalCellMapRecv);
  globalCellMapSend.clear();

  // complete the global map using parts of the map from the other ranks
  globalCellMap.clear();
  for (int rank = 0; rank < comm_size_; rank++)
    globalCellMap.insert(globalCellMapRecv[rank].begin(), globalCellMapRecv[rank].end());

#if 0 // for debugging
  for (int rank = 0; rank < comm_size_; rank++)
  {
    comm_->barrier();
    if (comm_->rank() != rank) continue;

    std::cout << " globalElemMap rank " << rank << std::endl;
    for (auto map : globalCellMap )
      std::cout << map.first << " " << map.second << std::endl;
    std::cout << std::flush;
  }
  comm_->barrier();
#endif

#else // Serial implementation

  nCellOnRank.resize(1);
  nCellOnRank[0] = cellBuffer_.size();

  // create an identity map for serial implementation
  for (std::size_t cellElem = 0; cellElem < cellBuffer_.size(); cellElem++)
    globalCellMap[cellElem] = cellElem;

#endif
}

//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::buildGroups()
{
  int comm_rank = comm_->rank();

  // custom sorting function object for sorting based on elemID
  struct
  {
    bool operator()(const GlobalMPIElement& a, const GlobalMPIElement& b) const
    { return a.elemID < b.elemID; }
  } elemIDless;

  // sort the buffers based on the elemID
  std::sort(cellBuffer_.begin(), cellBuffer_.end(), elemIDless);
  std::sort(bndBuffer_.begin(), bndBuffer_.end(), elemIDless);


  for (std::size_t igroup = 0; igroup < cellTopos_.size(); igroup++)
    cellGroups_[igroup]->clear();

  // Save the cell elements from the buffer
  // Put the cells on the current rank first, then add cells from other ranks
  for (int onrank = 0; onrank < 2; onrank++)
    for (std::size_t elem = 0; elem < cellBuffer_.size(); elem++)
    {
      const int rank = cellBuffer_[elem].rank;
      const int group = cellBuffer_[elem].group;
      const int elemID = cellBuffer_[elem].elemID;
      const std::vector<int>& elemMap = cellBuffer_[elem].elemMap;

      if ( (onrank == 0 && rank == comm_rank) ||
           (onrank == 1 && rank != comm_rank) )
        cellGroups_[group]->addElement(rank, elemID, elemMap);
    }

  for (std::size_t igroup = 0; igroup < bndTopos_.size(); igroup++)
    bndGroups_[igroup]->clear();

  // Save off the boundary elements from the buffer
  for (int onrank = 0; onrank < 2; onrank++)
    for (std::size_t elem = 0; elem < bndBuffer_.size(); elem++)
    {
      const int rank = bndBuffer_[elem].rank;
      const int group = bndBuffer_[elem].group;
      const int elemID = bndBuffer_[elem].elemID;
      const std::vector<int>& elemMap = bndBuffer_[elem].elemMap;

      if ( (onrank == 0 && rank == comm_rank) ||
           (onrank == 1 && rank != comm_rank) )
        bndGroups_[group]->addElement(rank, elemID, elemMap);
    }

  // only keep ghost boundary groups with elements
  for (auto group = ghostBndGroups_.begin(); group != ghostBndGroups_.end();)
    if ((*group)->nElem() == 0)
      group = ghostBndGroups_.erase(group);
    else
      group++;
}

//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::resetGhostGroups()
{
  ghostBndGroups_.clear();
  ghostBndGroups_.resize(cellTopos_.size());
  for (std::size_t igroup = 0; igroup < cellTopos_.size(); igroup++)
  {
    // Create ghost boundary groups, which currently assume all traces are the same topology
    const std::vector<TopologyTypes>& traceTopo = cellGroups_[igroup]->canonicalTraceTopo();
    for (std::size_t i = 1; i < traceTopo.size(); i++)
      SANS_ASSERT(traceTopo[0] == traceTopo[i]);

    ghostBndGroups_[igroup] = LagrangeElementGroup_ptr( new LagrangeElementGroup( traceTopo[0], 1 ) );
  }
}

//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::
dumpBuffer(const std::string& name, const std::vector<GlobalMPIElement>& buffer)
{
  int comm_rank = comm_->rank();

  std::cout << std::flush;
#ifdef SLEEP_FOR_OUTPUT
  std::this_thread::sleep_for( std::chrono::milliseconds(SLEEP_MILLISECONDS) );
#endif
  comm_->barrier();
  for (int rank = 0; rank < comm_size_; rank++)
  {
    std::cout << std::flush;
    comm_->barrier();
    if (comm_rank != rank) continue;

    std::cout << " " << name << " rank " << comm_rank << std::endl;
    std::cout << " rank group elemID" << std::endl;

    // print out info on the cells in the buffer
    for (std::size_t elem = 0; elem < buffer.size(); elem++)
    {
      const int rank   = buffer[elem].rank;
      const int group  = buffer[elem].group;
      const int elemID = buffer[elem].elemID;
      std::cout << " " << rank << " " << group << " " << elemID << " map " << buffer[elem].elemMap << std::endl;
    }
    std::cout << std::endl;
    std::cout << std::flush;
#ifdef SLEEP_FOR_OUTPUT
    std::this_thread::sleep_for( std::chrono::milliseconds(SLEEP_MILLISECONDS) );
#endif
  }
  comm_->barrier();

}

} // namespace SANS
