// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(XFIELD_LAGRANGE_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include <numeric> // std::accumulate

#include "XField_Lagrange.h"

#include "tools/SANSException.h"

#include "BasisFunction/ElementEdges.h"

#include "Field/Element/UniqueElemHash.h"
#include <unordered_set>

// This is ok because this impl file is only ever included in a cpp file
#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_MPI
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/set.hpp>
#include <boost/mpi/collectives/broadcast.hpp>
#include <boost/mpi/collectives/gather.hpp>
#include <boost/mpi/collectives/all_to_all.hpp>
#include <boost/mpi/collectives/all_reduce.hpp>
#endif

namespace SANS
{

//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::balance()
{
  if (graph_ == XFieldBalance::Serial)
  {
    // serialize the grid
    serialize();
  }
  else if (graph_ == XFieldBalance::CellPartitionCopy || graph_ == XFieldBalance::CellPartitionRead)
  {
    // balance cells based on a partition from another grid
    balanceCellsWithCopyPartition();

    // sync the DOFs once the elements have been blanced
    syncDOFs();
  }
  else
  {
    // first balance the cells based on node numbering
    balanceWithNodes();

    // hard coded for now to use parmetis
    balanceCellsWithParmetis();

    // sync the DOFs once the elements have been blanced
    syncDOFs();
  }
}


//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::balanceCellsWithCopyPartition()
{
#ifdef SANS_MPI

  SANS_ASSERT(!cellpartition_.empty());
  SANS_ASSERT(!bndpartition_.empty());
  //dumpBuffer(" Buffer " , bndBuffer_ );
  balanceElementsWithPartition(cellpartition_, cellBuffer_);
  //std::cout << " Boundary Construction" << std::endl;
  balanceElementsWithPartition(bndpartition_, bndBuffer_);

  // no need for these maps now
  cellpartition_.clear();
  bndpartition_.clear();

  // construct the ghost cells from the cell and bnd buffers
  createCellGhostGroups();

#endif // SANS_MPI

  // construct the cell and boundary trace groups from the buffers
  buildGroups();
}

//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::balanceCellsWithParmetis()
{
#ifdef SANS_MPI
  // This function assumes that elements have already been balanced based on the native node numbering

  std::vector<std::set<int>> Adj;
  std::map<int,int> globalCellMap;
  std::vector<int> nCellOnRank;

  // get a continuous numbering of the cells possessed by each processor
  continuousCellMap(globalCellMap, nCellOnRank);

  // the global cell numbering must now be renumbered to a compact continuous
  // numbering for parmetis
  cellAdjacencyGraph(globalCellMap, nCellOnRank, Adj);

  // call parmetis to get the cell partitioning
  std::vector<int> cellpart = callParmetis(Adj, nCellOnRank);

  // balance the cells with the partition from parmetis
  balanceWithCellParition(cellpart, Adj, globalCellMap, nCellOnRank);

  // construct the cell and boundary trace groups from the buffers
  buildGroups();

#else // SANS_MPI

  // nothing to do for serial

#endif
}


//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::
cellAdjacencyGraph(const std::map<int,int>& globalCellMap, const std::vector<int>& nCellOnRank,
                   std::vector<std::set<int>>& Adj)
{
#ifdef SANS_MPI

  int comm_rank = comm_->rank();

  // compute the cell offset from lower ranks
  int cell_offset = std::accumulate(nCellOnRank.begin(), nCellOnRank.begin() + comm_rank, 0);

  // used to construct the CSR graph
  std::vector<std::map<int,std::set<int>>> sendAdj(comm_size_);
  std::vector<std::map<int,std::set<int>>> recvAdj(comm_size_);

  std::vector<LagrangeConnectedGroup> interiorGroups;
  std::vector<LagrangeConnectedGroup> boundaryGroups;
  std::vector<LagrangeConnectedGroup> ghostBoundaryGroups;
  getConnectedTraceGroups(interiorGroups, boundaryGroups, ghostBoundaryGroups);
  mergePeriodicGroups( boundaryGroups );

  // periodic boundary groups impact the graph in the same way as interior trace groups,
  // so merge the two lists
  for ( const LagrangeConnectedGroup& bnd : boundaryGroups )
    if (bnd.groupR() >= 0)
      interiorGroups.emplace_back(bnd);

  // don't need boundary groups anymore
  boundaryGroups.clear();
  ghostBoundaryGroups.clear();


  // construct a graph based on the Dual Mesh (connect cell centers)

  for (std::size_t group = 0; group < interiorGroups.size(); group++)
  {
    const LagrangeConnectedGroup& traceGroup = interiorGroups[group];

    const int cellGroupL = traceGroup.groupL();
    const int cellGroupR = traceGroup.groupR();
    for (int elemT = 0; elemT < interiorGroups[group].nElem(); elemT++)
    {
      // get the local element index
      int elemL = traceGroup.elemL(elemT);
      int elemR = traceGroup.elemR(elemT);

      // get the ranks that possess the two elements
      int rankL = cellGroups_[cellGroupL]->elem(elemL).rank;
      int rankR = cellGroups_[cellGroupR]->elem(elemR).rank;

      // get the unique global element IDs mapped to a continuous numbering

      int elemGlobalL = globalCellMap.at(cellGroups_[cellGroupL]->elem(elemL).elemID);
      int elemGlobalR = globalCellMap.at(cellGroups_[cellGroupR]->elem(elemR).elemID);

      // this might happen for periodic connections
      if (elemGlobalL == elemGlobalR) continue;

      // set the adjacency on the different ranks
      sendAdj[rankL][elemGlobalL].insert(elemGlobalR);
      sendAdj[rankR][elemGlobalR].insert(elemGlobalL);
    }
  }

#if 0 // for debugging
  for (int rank = 0; rank < comm_size_; rank++)
  {
    comm_->barrier();
    if (comm_->rank() != rank) continue;

    std::cout << std::endl << std::endl << " sendAdj rank " << rank << std::endl;
    for (std::size_t i = 0; i < sendAdj.size(); i++ )
    {
      std::cout << " send to " << i << std::endl;
      for (auto map : sendAdj[i] )
      {
        std::cout << map.first << " adj ";
        for (const int adj : map.second)
          std::cout << adj << " ";
        std::cout << std::endl;
      }
    }
    std::cout << std::flush;
#ifdef SLEEP_FOR_OUTPUT
    std::this_thread::sleep_for( std::chrono::milliseconds(SLEEP_MILLISECONDS) );
#endif
  }
  comm_->barrier();
#endif

  // sync the adjacency graph with other ranks
  boost::mpi::all_to_all(*comm_, sendAdj, recvAdj);
  sendAdj.clear();

#if 0 // for debugging
  for (int rank = 0; rank < comm_size_; rank++)
  {
    comm_->barrier();
    if (comm_->rank() != rank) continue;

    std::cout << std::endl << std::endl << " recvAdj rank " << rank << std::endl;
    for (std::size_t i = 0; i < recvAdj.size(); i++ )
    {
      std::cout << " recv from " << i << std::endl;
      for (auto map : recvAdj[i] )
      {
        std::cout << map.first << " adj ";
        for (const int adj : map.second)
          std::cout << adj << " ";
        std::cout << std::endl;
      }
    }
    std::cout << std::flush;
#ifdef SLEEP_FOR_OUTPUT
    std::this_thread::sleep_for( std::chrono::milliseconds(SLEEP_MILLISECONDS) );
#endif
  }
  comm_->barrier();
#endif

  Adj.resize(nCellOnRank[comm_rank]); // the local part of the graph

  // complete the global map from the other ranks
  for (int rank = 0; rank < comm_size_; rank++)
    for (const auto& adj : recvAdj[rank] )
      Adj[adj.first - cell_offset].insert(adj.second.begin(), adj.second.end());

#endif // SANS_MPI
}


//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::
balanceWithCellParition(const std::vector<int>& cellpart, const std::vector<std::set<int>>& Adj,
                        const std::map<int,int>& globalCellMap, const std::vector<int>& nCellOnRank)
{
#if 0
#ifdef SANS_MPI
  int comm_rank = comm_->rank();

#if 0 // for debugging
  dumpBuffer("cellBuffer_ nodal ", cellBuffer_);
#endif

  // sort out which ranks need which elements
  std::vector<std::set<int>> needCell(comm_size_);
  std::vector<std::set<int>> sendCell(comm_size_);

  // a list of elements numbered in the continuous cell numbering locally
  std::vector<GlobalMPIElement> localBuffer(nCellOnRank[comm_rank]);

  // compute the cell offset from lower ranks
  int cell_offset = std::accumulate(nCellOnRank.begin(), nCellOnRank.begin() + comm_rank, 0);

  for (std::size_t elem = 0; elem < cellBuffer_.size(); elem++ )
  {
    // cellpart only contains elements possessed by this processor
    if (cellBuffer_[elem].rank != comm_rank) continue;

    const int elemID = globalCellMap.at(cellBuffer_[elem].elemID);
    const int elemLocal = elemID - cell_offset;
    const int rank = cellpart[elemLocal];

    // copy over the buffer to a continuous temporary buffer
    localBuffer[elemLocal] = cellBuffer_[elem];

    // set the new rank possession of this cell based on the partition
    localBuffer[elemLocal].rank = rank;

    // set that the current element and it's neighbors as needed on the rank from the partition
    needCell[rank].insert(elemID);
    for (const int elemID : Adj[elemLocal])
      needCell[rank].insert(elemID);
  }

  // free the cell buffer so it can be re-populated
  cellBuffer_.clear();

  // send the cell needs based on the parmetis partition to the ranks that ultimately need them
  boost::mpi::all_to_all(*comm_, needCell, sendCell);
  needCell.clear();
  needCell.resize(comm_size_);

  // sort out which rank has the needed cells
  for (int rank = 0; rank < comm_size_; rank++)
  {
    for (const int elemID : sendCell[rank])
    {
      int rankWithCell = 0;
      int cell_offset = nCellOnRank[rankWithCell];
      while (elemID >= cell_offset)
      {
        rankWithCell++;
        cell_offset += nCellOnRank[rankWithCell];
      }
      needCell[rankWithCell].insert(elemID);
    }
  }

  // send the cell needs to the ranks have the cells
  boost::mpi::all_to_all(*comm_, needCell, sendCell);
  needCell.clear();

  std::vector<std::vector<GlobalMPIElement>> sendBuffer(comm_size_);
  std::vector<std::vector<GlobalMPIElement>> recvBuffer(comm_size_);

  // Fill the buffer to send cells back to the processors that need them
  for (int rank = 0; rank < comm_size_; rank++)
    for (const int elemID : sendCell[rank])
      sendBuffer[rank].emplace_back(localBuffer[elemID - cell_offset]);

  // send the cells
  boost::mpi::all_to_all(*comm_, sendBuffer, recvBuffer);
  sendBuffer.clear();

  // add balanced cells to the buffer
  for (int rank = 0; rank < comm_size_; rank++)
    cellBuffer_.insert(cellBuffer_.end(), recvBuffer[rank].begin(), recvBuffer[rank].end());

  recvBuffer.clear();

#if 0 // for debugging
  dumpBuffer("cellBuffer_ part ", cellBuffer_);
#endif

  // Balance the local elements based on the node map in order to sort out which rank needs boundary traces.
  // Each processor now has a copy of the cell, and the cell is tagged with the rank that possess that cell.
  // Boundary traces can now look at neighboring cells to sort out what rank the boundary trace is needed on.
  balanceElementsWithNodes(nativeNodeRank_, cellGroups_, false, localBuffer);


  // Compute the minimum rank for the possessed nodes using the localBuffer
  // partitioned with the continuous node ranking.
  // Each processor will then have it's possessed nodes ranked
  std::map<int,int> localNodeRank;
  {
    std::map<int,int> globalNodeCellMinRank;
    std::map<int,std::set<int>> nativeNodeCellRanks;
    for (std::size_t elem = 0; elem < localBuffer.size(); elem++ )
    {
      // get the new rank of this cell based on the partition
      const int rank = localBuffer[elem].rank;

      const int group = localBuffer[elem].group;
      const std::vector<int>& elemMap = localBuffer[elem].elemMap;

      const std::vector<int>& canonicalNodes = cellGroups_[group]->canonicalNodes();
      for (std::size_t n = 0; n < canonicalNodes.size(); n++)
      {
        int nodeGlobal = elemMap[canonicalNodes[n]];

        // only consider nodes possessed by this processor
        if ( nativeNodeRank_.at(nodeGlobal) != comm_rank ) continue;

        // add to the list of all ranks that touch this node
        nativeNodeCellRanks[nodeGlobal].insert(rank);

        auto it = globalNodeCellMinRank.find(nodeGlobal);
        if ( it == globalNodeCellMinRank.end() )
          globalNodeCellMinRank[nodeGlobal] = rank;
        else
          it->second = std::min(it->second, rank);
      }
    }

    // get the map to processors that contain the node ranking from the cell elements
    std::map<int,int> globalNodeRank = continuousNodeRank(cellBuffer_);

    // get the cell min rank for this processor
    getNodeRanking( globalNodeCellMinRank, globalNodeRank );

    // append any missing elements based on the partitioned node ranking
    // this is required to complete any CG stencils
    appendElementsWithNodes(globalNodeRank, cellGroups_, false, cellBuffer_);

    // based on the new cellBuffer, retrieve the final node ranking for this processor
    nativeNodeRank_ = continuousNodeRank(cellBuffer_);

    // get the cell min rank for this processor
    getNodeRanking( globalNodeCellMinRank, nativeNodeRank_ );


    // get the map to processors that contain the node ranking from the localBuffer
    // this is needed to send boundary elements to all processors that need them for
    // a CG stencil
    localNodeRank = continuousNodeRank(localBuffer);
    getNodeRanking( globalNodeCellMinRank, localNodeRank );
  }

#if 0 // for debugging
  dumpBuffer("cellBuffer_ part node 3 ", cellBuffer_);
#endif

#if 0 // for debugging
  std::cout << std::flush;
#ifdef SLEEP_FOR_OUTPUT
  std::this_thread::sleep_for( std::chrono::milliseconds(SLEEP_MILLISECONDS) );
#endif
  comm_->barrier();
  for (int rank = 0; rank < comm_size_; rank++)
  {
    std::cout << std::flush;
    comm_->barrier();
    if (comm_rank != rank) continue;

    std::cout << " nativeNodeRank_ rank " << comm_rank << std::endl;
    std::cout << " nodeGlobal rank" << std::endl;

    for (const std::pair<int,int>& node : nativeNodeRank_)
      std::cout << " " << node.first << " " << node.second << std::endl;

    std::cout << std::endl;
    std::cout << std::flush;
#ifdef SLEEP_FOR_OUTPUT
    std::this_thread::sleep_for( std::chrono::milliseconds(SLEEP_MILLISECONDS) );
#endif
  }
  comm_->barrier();
#endif


  for (std::size_t igroup = 0; igroup < cellTopos_.size(); igroup++)
    cellGroups_[igroup]->clear();

  // Save the cell elements from the buffer
  for (std::size_t elem = 0; elem < localBuffer.size(); elem++)
  {
    const int rank = localBuffer[elem].rank;
    const int group = localBuffer[elem].group;
    const int elemID = localBuffer[elem].elemID;
    const std::vector<int>& elemMap = localBuffer[elem].elemMap;

    cellGroups_[group]->addElement(rank, elemID, elemMap);
  }

#if 0 // for debugging
  dumpBuffer("localBuffer nodal ", localBuffer);
#endif

#if 0 // for debugging
  dumpBuffer("bndBuffer_ nodal ", bndBuffer_);
#endif

  // done with the buffer
  localBuffer.clear();

  // collect up all nodes that are on boundaries
  std::set<int> boundaryNodes;
  for (std::size_t elem = 0; elem < bndBuffer_.size(); elem++ )
  {
    const int bndGroup = bndBuffer_[elem].group;
    const std::vector<int>& canonicalNodes = bndGroups_[bndGroup]->canonicalNodes();
    for (std::size_t n = 0; n < canonicalNodes.size(); n++)
      boundaryNodes.insert( bndBuffer_[elem].elemMap[canonicalNodes[n]] );
  }

  // connect boundary traces to the cells
  std::vector<LagrangeConnectedGroup> interiorGroups;
  std::vector<LagrangeConnectedGroup> boundaryGroups;
  std::vector<LagrangeConnectedGroup> ghostBoundaryGroups;
  getConnectedTraceGroups(interiorGroups, boundaryGroups, ghostBoundaryGroups);
  interiorGroups.clear();
  ghostBoundaryGroups.clear();

  // construct the periodic map
  std::vector<PeriodicBCNodeMap> periodicElemMaps;
  getPeriodicElemMap( boundaryGroups, periodicElemMaps );

  sendBuffer.resize(comm_size_);

  // sort out which rank needs each boundary trace element
  for (std::size_t group = 0; group < boundaryGroups.size(); group++)
  {
    const LagrangeConnectedGroup& traceGroup = boundaryGroups[group];

    bool isPeriodGroupR = false;
    for ( const PeriodicBCNodeMap& period : periodicElemMaps )
      if ( (int)group == period.bndGroupR )
      {
        isPeriodGroupR = true;
        break;
      }

    const int cellGroupL = traceGroup.groupL();
    const int nElemT = traceGroup.nElem();
    for (int elemT = 0; elemT < nElemT; elemT++)
    {
      int rankT = traceGroup.elem(elemT).rank;

      // only consider boundary traces possessed by this processor
      if ( rankT != comm_rank ) continue;

      // the set of ranks to send the boundary element to
      std::set<int> uniqueRanks;

      // get the local element index
      int elemL = traceGroup.elemL(elemT);

      // get the rank that possess the boundary neighboring cell
      // this is required for DG stencil
      int rankL = cellGroups_[cellGroupL]->elem(elemL).rank;
      uniqueRanks.insert(rankL);

      // TODO: This does not work for periodic elements. Need to rework periodicity for CG anyways...
      if (!isPeriodGroupR && periodicElemMaps.size() == 0)
      {
        // get the interior canonical nodes and send the boundary element to all processors
        // this is required for CG stencil
        const std::vector<int>& canonicalNodes = cellGroups_[cellGroupL]->canonicalNodes();
        const std::vector<int>& elemMap = cellGroups_[cellGroupL]->elem(elemL).elemMap;

        for (const int node : canonicalNodes)
          if (boundaryNodes.find(elemMap[node]) == boundaryNodes.end()) // don't use any boundary nodes
            uniqueRanks.insert( localNodeRank.at(elemMap[node]) );
      }

      // TODO: This does not work for periodic elements. Need to rework periodicity for CG anyways...
      if (!isPeriodGroupR && periodicElemMaps.size() == 0)
      {
        // get the boundary canonical nodes and send the boundary element to all processors
        // this is required for CG stencil
        const std::vector<int>& canonicalNodes = traceGroup.canonicalNodes();
        const std::vector<int>& elemMap = traceGroup.elem(elemT).elemMap;

        for (const int node : canonicalNodes)
          uniqueRanks.insert( localNodeRank.at(elemMap[node]) );
      }

      // rankL here indicates the processors that possesses the boundary element
      for (const int rank : uniqueRanks)
        sendBuffer[rank].emplace_back(rankL, group, traceGroup.elem(elemT).elemID, traceGroup.elemNodes(elemT));
    }
  }

  // Process periodic connections
  for ( const PeriodicBCNodeMap& period : periodicElemMaps )
  {
    int bndGroupIDL = period.bndGroupL;
    int bndGroupIDR = period.bndGroupR;

    const LagrangeConnectedGroup& bndGroupL = boundaryGroups[bndGroupIDL];
    const LagrangeConnectedGroup& bndGroupR = boundaryGroups[bndGroupIDR];

    const int cellGroupL = bndGroupL.groupL();
    const int cellGroupR = bndGroupR.groupL();

    // loop over all BC element pairs
    for ( const std::pair<int,int>& elemPair : period.mapLtoR )
    {
      // the set of ranks to send the boundary element to
      std::set<int> uniqueRanks;

      // Get the right and left boundary elements
      int iL = elemPair.first;
      int iR = elemPair.second;

      // get the local element index
      int elemL = bndGroupL.elemL(iL);
      int elemR = bndGroupR.elemL(iR);

      // get the rank that possess the boundary neighboring cell
      int rankL = cellGroups_[cellGroupL]->elem(elemL).rank;
      int rankR = cellGroups_[cellGroupR]->elem(elemR).rank;

      uniqueRanks.insert(rankL);
      uniqueRanks.insert(rankR);

      // This will produce duplication, but those duplicates are filtered out below

#if 0 // TODO: Periodicity for CG does not work

      // get the canonical nodes and send the boundary element to all processors
      // this is required for CG stencil
      const std::vector<int>& canonicalNodes = cellGroups_[cellGroupL]->canonicalNodes();
      const std::vector<int>& elemMap = cellGroups_[cellGroupL]->elem(elemL).elemMap;

      for (const int node : canonicalNodes)
        if (boundaryNodes.find(elemMap[node]) == boundaryNodes.end()) // don't use any boundary nodes
          uniqueRanks.insert( localNodeRank.at(elemMap[node]) );
#endif

      // make sure periodic information is available on both rankL and rankR
      for (const int rank : uniqueRanks)
      {
        sendBuffer[rank].emplace_back(rankL, bndGroupIDL, bndGroupL.elem(iL).elemID, bndGroupL.elemNodes(iL));
        sendBuffer[rank].emplace_back(rankR, bndGroupIDR, bndGroupR.elem(iR).elemID, bndGroupR.elemNodes(iR));
      }
    }
  }

  // done with this now
  localNodeRank.clear();

  // send the boundary trace elements
  boost::mpi::all_to_all(*comm_, sendBuffer, recvBuffer);
  sendBuffer.clear();

  // add cells to the buffer
  bndBuffer_.clear();
  for (int rank = 0; rank < comm_size_; rank++)
    bndBuffer_.insert(bndBuffer_.end(), recvBuffer[rank].begin(), recvBuffer[rank].end());

  recvBuffer.clear();

  // remove any possible duplicates from boundaries
  {
    std::set<GlobalMPIElement> uniqueBnd;
    uniqueBnd.insert(bndBuffer_.begin(), bndBuffer_.end());
    bndBuffer_.clear();
    bndBuffer_.insert(bndBuffer_.end(), uniqueBnd.begin(), uniqueBnd.end());
  }

#if 0 // for debugging
  dumpBuffer("bndBuffer_ part ", bndBuffer_);
#endif

  // Append any boundary elements needed for a CG stencil
  // (also removes any possible duplicates from periodic boundaries)
  //appendElementsWithNodes(nativeNodeRank_, bndGroups_, false, bndBuffer_);

#if 0 // for debugging
  dumpBuffer("bndBuffer_ part nodes ", bndBuffer_);
#endif

  //---------------------------------------------------------------------//
  // construct ghost boundaries
  //---------------------------------------------------------------------//

  createCellGhostGroups();

#endif // SANS_MPI
#else

#ifdef SANS_MPI
  int comm_rank = comm_->rank();

#if 0 // for debugging
  dumpBuffer("cellBuffer_ nodal ", cellBuffer_);
#endif

  // sort out which ranks need which elements
  std::vector<std::set<int>> needCell(comm_size_);
  std::vector<std::set<int>> sendCell(comm_size_);

  // a list of elements numbered in the continuous cell numbering locally
  std::vector<GlobalMPIElement> localBuffer(nCellOnRank[comm_rank]);

  // compute the cell offset from lower ranks
  int cell_offset = std::accumulate(nCellOnRank.begin(), nCellOnRank.begin() + comm_rank, 0);

  for (std::size_t elem = 0; elem < cellBuffer_.size(); elem++ )
  {
    // cellpart only contains elements possessed by this processor
    if (cellBuffer_[elem].rank != comm_rank) continue;

    const int elemID = globalCellMap.at(cellBuffer_[elem].elemID);
    const int elemLocal = elemID - cell_offset;
    const int rank = cellpart[elemLocal];

    // copy over the buffer to a continuous temporary buffer
    localBuffer[elemLocal] = cellBuffer_[elem];

    // set the new rank possession of this cell based on the partition
    localBuffer[elemLocal].rank = rank;
  }

  // Balance the local elements based on the node map in order to sort out which rank needs boundary traces.
  // Each processor now has a copy of the cell, and the cell is tagged with the rank that possess that cell.
  // Boundary traces can now look at neighboring cells to sort out what rank the boundary trace is needed on.
  balanceElementsWithNodes(nativeNodeRank_, cellGroups_, false, localBuffer);

#if 0 // for debugging
  dumpBuffer("localBuffer nodal ", localBuffer);
#endif

  // Unique set of ranks where the cells should be sent to
  std::map<int,std::set<int>> nativeNodeCellRanks;
  {
    // based on the localBuffer, get the continuous ranking for each node
    std::map<int,int> localBufferNodeRank = continuousNodeRank(localBuffer);

    // Count the cell ranks around each node on the current processor
    // This is needed for Edge local solves
    std::map<int,std::map<int,int>> nodeRankCount;
    for (std::size_t elem = 0; elem < localBuffer.size(); elem++ )
    {
      const int group = localBuffer[elem].group;
      const int rank = localBuffer[elem].rank;
      const std::vector<int>& elemMap = localBuffer[elem].elemMap;
      const std::vector<int>& canonicalNodes = cellGroups_[group]->canonicalNodes();
      for (const int node : canonicalNodes)
      {
        int nodeNative = elemMap[node];

        // only consider nodes on the current processor
        if (localBufferNodeRank.at(nodeNative) != comm_rank) continue;

        // increase the occurrence of the rank
        ++nodeRankCount[nodeNative][rank];
      }
    }

    // get a unique node partitioned ranking based on the maximum cell rank occurrence
    // This is needed for Edge local solves
    std::map<int, int> nodeRank;
    for (const std::pair<int, std::map<int,int>> node : nodeRankCount)
    {
      // maximum occurrences and rank of interest
      int maxoccur = 0;
      int node_rank = -1;

      // loop over the edge ranks
      for (const std::pair<const int, int>& occurance : node.second)
        if (occurance.second > maxoccur)
        {
          // add newly added occurrences to the list
          node_rank = occurance.first;
          maxoccur = occurance.second;
        }

      nodeRank[node.first] = node_rank;
    }

    // Fill the buffer to send cells to the processors that need them
    for (std::size_t elem = 0; elem < localBuffer.size(); elem++ )
    {
      const int group = localBuffer[elem].group;
      const int rank = localBuffer[elem].rank;
      const std::vector<int>& elemMap = localBuffer[elem].elemMap;
      const std::vector<int>& canonicalNodes = cellGroups_[group]->canonicalNodes();
      for (const int node : canonicalNodes)
      {
        int nodeNative = elemMap[node];

        // add all element ranks touching the node
        nativeNodeCellRanks[nodeNative].insert(rank);
      }

      const int (*EdgeNodes)[ Line::NNode ] = nullptr;
      int nEdge;
      elementEdges(cellGroups_[group]->topo, EdgeNodes, nEdge);

      for (int edge = 0; edge < nEdge; edge++)
      {
        int nodeNative_i = elemMap[canonicalNodes[ EdgeNodes[edge][0] ]];
        int nodeNative_j = elemMap[canonicalNodes[ EdgeNodes[edge][1] ]];

        if (nodeNative_i > nodeNative_j)
          std::swap(nodeNative_i, nodeNative_j);

        // only consider edges with the primary node on the current processor
        if (localBufferNodeRank.at(nodeNative_i) != comm_rank) continue;

        // use the node rank as the edge rank. This is needed for edge local solves
        int edge_rank = nodeRank.at(nodeNative_i);

        for (const int node : canonicalNodes)
        {
          int nodeNative = elemMap[node];

          // add the edge rank to all nodes of the element
          nativeNodeCellRanks[nodeNative].insert(edge_rank);
        }
      }
    }
  }


  for (std::size_t igroup = 0; igroup < cellTopos_.size(); igroup++)
    cellGroups_[igroup]->clear();

  // Save the cell elements from the buffer
  for (std::size_t elem = 0; elem < localBuffer.size(); elem++)
  {
    const int rank = localBuffer[elem].rank;
    const int group = localBuffer[elem].group;
    const int elemID = localBuffer[elem].elemID;
    const std::vector<int>& elemMap = localBuffer[elem].elemMap;

    cellGroups_[group]->addElement(rank, elemID, elemMap);
  }

  // connect boundary traces to the cells
  std::vector<LagrangeConnectedGroup> interiorGroups;
  std::vector<LagrangeConnectedGroup> boundaryGroups;
  std::vector<LagrangeConnectedGroup> ghostBoundaryGroups;
  getConnectedTraceGroups(interiorGroups, boundaryGroups, ghostBoundaryGroups);
  interiorGroups.clear();
  ghostBoundaryGroups.clear();

  // construct the periodic map
  std::vector<PeriodicBCNodeMap> periodicElemMaps;
  getPeriodicElemMap( boundaryGroups, periodicElemMaps );

  // Process periodic connections
  for ( const PeriodicBCNodeMap& period : periodicElemMaps )
  {
    int bndGroupIDL = period.bndGroupL;
    int bndGroupIDR = period.bndGroupR;

    const LagrangeConnectedGroup& bndGroupL = boundaryGroups[bndGroupIDL];
    const LagrangeConnectedGroup& bndGroupR = boundaryGroups[bndGroupIDR];

    const std::vector<int>& canonicalNodes = bndGroupL.canonicalNodes();

    // loop over all BC element pairs
    for ( const std::pair<int,int>& elemPair : period.mapLtoR )
    {
      // Get the right and left boundary elements
      int iL = elemPair.first;
      int iR = elemPair.second;

      // get the boundary canonical nodes and send the boundary element to all processors
      const std::vector<int>& elemMapL = bndGroupL.elem(iL).elemMap;
      const std::vector<int>& elemMapR = bndGroupR.elem(iR).elemMap;

      for (const int node : canonicalNodes)
      {
        std::set<int>& ranksL = nativeNodeCellRanks.at(elemMapL[node]);
        std::set<int>& ranksR = nativeNodeCellRanks.at(elemMapR[node]);

        ranksL.insert(ranksR.begin(), ranksR.end());
        ranksR = ranksL;
      }
    }
  }


  {
    std::vector<std::map<int,std::set<int>>> localRanks(comm_size_);
    for ( std::pair<const int,std::set<int>>& key_ranks : nativeNodeCellRanks )
    {
      int nativeNode = key_ranks.first;

      // get the rank that should receive the DOF for the Native indexing
      int rank = nativeNodeRank_.at(nativeNode);

      localRanks[rank][nativeNode] = key_ranks.second;
    }

    std::vector<std::map<int,std::set<int>>> implicitRanks(comm_size_);
    boost::mpi::all_to_all(*comm_, localRanks, implicitRanks);

    //Collapse down all ranks from all processors
    std::map<int,std::set<int>> partNodeRanks;
    for (std::size_t rank = 0; rank < implicitRanks.size(); rank++)
      for (std::pair<const int,std::set<int>>& key_ranks : implicitRanks[rank])
        partNodeRanks[key_ranks.first].insert(key_ranks.second.begin(), key_ranks.second.end());

    for ( std::pair<const int,std::set<int>>& key_ranks : partNodeRanks )
    {
      // update the ranks to send back to the processor where they originated
      for (std::size_t rank = 0; rank < implicitRanks.size(); rank++)
      {
        std::map<int,std::set<int>>::iterator
        it = implicitRanks[rank].find(key_ranks.first);

        if (it != implicitRanks[rank].end())
          it->second = key_ranks.second;
      }
    }

    // send the native indexing back to the originating processors
    boost::mpi::all_to_all(*comm_, implicitRanks, localRanks);

    // save off the DOFs with the native indexing
    for (std::size_t rank = 0; rank < localRanks.size(); rank++)
      for (std::pair<const int,std::set<int>>& key_ranks : localRanks[rank])
        nativeNodeCellRanks.at(key_ranks.first) = key_ranks.second;
  }


  // free the cell buffer so it can be re-populated
  cellBuffer_.clear();

  std::vector<std::set<GlobalMPIElement>> sendBuffer(comm_size_);
  std::vector<std::set<GlobalMPIElement>> recvBuffer(comm_size_);

  // Fill the buffer to send cells to the processors that need them
  for (std::size_t elem = 0; elem < localBuffer.size(); elem++ )
  {
    const int group = localBuffer[elem].group;
    const std::vector<int>& elemMap = localBuffer[elem].elemMap;
    const std::vector<int>& canonicalNodes = cellGroups_[group]->canonicalNodes();
    for (std::size_t n = 0; n < canonicalNodes.size(); n++)
    {
      int nodeNative = elemMap[canonicalNodes[n]];

      // add this element to all ranks touching the node
      for (const int rank : nativeNodeCellRanks.at(nodeNative))
        sendBuffer[rank].insert(localBuffer[elem]);
    }
  }

  // send the cells
  boost::mpi::all_to_all(*comm_, sendBuffer, recvBuffer);
  sendBuffer.clear();

  // add balanced cells to the buffer while remove any possible duplicates
  std::set<GlobalMPIElement> uniqueCell;
  for (int rank = 0; rank < comm_size_; rank++)
    uniqueCell.insert(recvBuffer[rank].begin(), recvBuffer[rank].end());

  cellBuffer_.insert(cellBuffer_.end(), uniqueCell.begin(), uniqueCell.end());
  uniqueCell.clear();

  // based on the cellBuffer, retrieve the final node ranking for this processor
  nativeNodeRank_ = continuousNodeRank(cellBuffer_);

  // get the cell min rank for this processor
  //getNodeRanking( nativeNodeCellRanks, nativeNodeRank_ );

#if 0 // for debugging
  dumpBuffer("cellBuffer_ part ", cellBuffer_);
#endif

#if 0 // for debugging
  dumpBuffer("bndBuffer_ nodal ", bndBuffer_);
#endif

  // done with the buffer
  localBuffer.clear();

  sendBuffer.clear();
  sendBuffer.resize(comm_size_);

  // sort out which rank needs each boundary trace element
  for (std::size_t group = 0; group < boundaryGroups.size(); group++)
  {
    const LagrangeConnectedGroup& traceGroup = boundaryGroups[group];

    bool isPeriodGroupR = false;
    for ( const PeriodicBCNodeMap& period : periodicElemMaps )
      if ( (int)group == period.bndGroupR )
      {
        isPeriodGroupR = true;
        break;
      }

    const int cellGroupL = traceGroup.groupL();
    const int nElemT = traceGroup.nElem();
    for (int elemT = 0; elemT < nElemT; elemT++)
    {
      int rankT = traceGroup.elem(elemT).rank;

      // only consider boundary traces possessed by this processor
      if ( rankT != comm_rank ) continue;

      // the set of ranks to send the boundary element to
      std::set<int> uniqueRanks;

      // get the local element index
      int elemL = traceGroup.elemL(elemT);

      // get the rank that possess the boundary neighboring cell
      // this is required for DG stencil
      int rankL = cellGroups_[cellGroupL]->elem(elemL).rank;
      uniqueRanks.insert(rankL);

#if 1
      // TODO: This does not work for periodic elements. Need to rework periodicity for CG anyways...
      if (!isPeriodGroupR && periodicElemMaps.size() == 0)
      {
        // get the interior canonical nodes and send the boundary element to all processors
        // this is required for CG stencil
        const std::vector<int>& canonicalNodes = cellGroups_[cellGroupL]->canonicalNodes();
        const std::vector<int>& elemMap = cellGroups_[cellGroupL]->elem(elemL).elemMap;

        for (const int node : canonicalNodes)
        {
          const std::set<int>& ranks = nativeNodeCellRanks.at(elemMap[node]);
          uniqueRanks.insert( ranks.begin(), ranks.end() );
        }
      }
#endif

      // rankL here indicates the processors that possesses the boundary element
      for (const int rank : uniqueRanks)
        sendBuffer[rank].emplace(rankL, group, traceGroup.elem(elemT).elemID, traceGroup.elemNodes(elemT));
    }
  }

#if 1
  // Process periodic connections
  for ( const PeriodicBCNodeMap& period : periodicElemMaps )
  {
    int bndGroupIDL = period.bndGroupL;
    int bndGroupIDR = period.bndGroupR;

    const LagrangeConnectedGroup& bndGroupL = boundaryGroups[bndGroupIDL];
    const LagrangeConnectedGroup& bndGroupR = boundaryGroups[bndGroupIDR];

    const int cellGroupL = bndGroupL.groupL();
    const int cellGroupR = bndGroupR.groupL();

    // loop over all BC element pairs
    for ( const std::pair<int,int>& elemPair : period.mapLtoR )
    {
      // the set of ranks to send the boundary element to
      std::set<int> uniqueRanks;

      // Get the right and left boundary elements
      int iL = elemPair.first;
      int iR = elemPair.second;

      // get the local element index
      int elemL = bndGroupL.elemL(iL);
      int elemR = bndGroupR.elemL(iR);

      // get the rank that possess the boundary neighboring cell
      int rankL = cellGroups_[cellGroupL]->elem(elemL).rank;
      int rankR = cellGroups_[cellGroupR]->elem(elemR).rank;

      uniqueRanks.insert(rankL);
      uniqueRanks.insert(rankR);

      // This will produce duplication, but those duplicates are filtered out below

#if 0 // TODO: Periodicity for CG does not work

      // get the canonical nodes and send the boundary element to all processors
      // this is required for CG stencil
      const std::vector<int>& canonicalNodes = cellGroups_[cellGroupL]->canonicalNodes();
      const std::vector<int>& elemMap = cellGroups_[cellGroupL]->elem(elemL).elemMap;

      for (const int node : canonicalNodes)
        if (boundaryNodes.find(elemMap[node]) == boundaryNodes.end()) // don't use any boundary nodes
          uniqueRanks.insert( localNodeRank.at(elemMap[node]) );
#endif

      // make sure periodic information is available on both rankL and rankR
      for (const int rank : uniqueRanks)
      {
        sendBuffer[rank].emplace(rankL, bndGroupIDL, bndGroupL.elem(iL).elemID, bndGroupL.elemNodes(iL));
        sendBuffer[rank].emplace(rankR, bndGroupIDR, bndGroupR.elem(iR).elemID, bndGroupR.elemNodes(iR));
      }
    }
  }
#endif
  // send the boundary trace elements
  boost::mpi::all_to_all(*comm_, sendBuffer, recvBuffer);
  sendBuffer.clear();

  // add cells to the buffer and remove any possible duplicates from boundaries
  bndBuffer_.clear();
  std::set<GlobalMPIElement> uniqueBnd;
  for (int rank = 0; rank < comm_size_; rank++)
    uniqueBnd.insert(recvBuffer[rank].begin(), recvBuffer[rank].end());

  recvBuffer.clear();
  bndBuffer_.insert(bndBuffer_.end(), uniqueBnd.begin(), uniqueBnd.end());
  uniqueBnd.clear();

#if 0 // for debugging
  dumpBuffer("bndBuffer_ part ", bndBuffer_);
#endif

  //---------------------------------------------------------------------//
  // construct ghost boundaries
  //---------------------------------------------------------------------//

  createCellGhostGroups();

#endif // SANS_MPI

#endif
}

//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::createCellGhostGroups()
{
#ifdef SANS_MPI

  int comm_rank = comm_->rank();

  typedef std::unordered_set<UniqueElem, boost::hash<UniqueElem>> UniqueTraceSet;
  std::vector< UniqueTraceSet > uniqueGhostTraces(cellGroups_.size());

  // add all traces on ghost cells
  for (std::size_t cellElem = 0; cellElem < cellBuffer_.size(); cellElem++)
  {
    // only consider ghost cells
    if ( cellBuffer_[cellElem].rank == comm_rank ) continue;

    const int group = cellBuffer_[cellElem].group;
    const std::vector<int>& elemMap = cellBuffer_[cellElem].elemMap;

    const std::vector<std::vector<int>>& canonicalTraceNodes = cellGroups_[group]->canonicalTracesQ1();
    const std::vector<TopologyTypes>& canonicalTraceTopo = cellGroups_[group]->canonicalTraceTopo();

    for (std::size_t trace = 0; trace < canonicalTraceNodes.size(); trace++)
    {
      std::vector<int> nodesTraceGlobal(canonicalTraceNodes[trace].size(),-1);

      for (std::size_t i = 0; i < canonicalTraceNodes[trace].size(); i++)
      {
        // extract the global nodes from the map
        nodesTraceGlobal[i] = elemMap[canonicalTraceNodes[trace][i]];
      }

      // add the ghost trace, or remove it if it already exists (this happens when two connected cells are ghost cells)
      UniqueElem ghostTrace(canonicalTraceTopo[trace], nodesTraceGlobal);

      bool found = false;
      for (std::size_t cellGroup = 0; cellGroup < uniqueGhostTraces.size(); cellGroup++)
      {
        typename UniqueTraceSet::iterator it = uniqueGhostTraces[cellGroup].find(ghostTrace);

        if ( it != uniqueGhostTraces[cellGroup].end() )
        {
          uniqueGhostTraces[cellGroup].erase( it );
          found = true;
          break;
        }
      }
      if (!found)
        uniqueGhostTraces[group].insert( ghostTrace );
    }
  }

  // remove all traces from cells on the processor from the ghost trace elements
  for (std::size_t cellElem = 0; cellElem < cellBuffer_.size(); cellElem++)
  {
    // skip ghost cells this time
    if ( cellBuffer_[cellElem].rank != comm_rank ) continue;

    const int group = cellBuffer_[cellElem].group;
    const std::vector<int>& elemMap = cellBuffer_[cellElem].elemMap;

    const std::vector<std::vector<int>>& canonicalTraceNodes = cellGroups_[group]->canonicalTracesQ1();
    const std::vector<TopologyTypes>& canonicalTraceTopo = cellGroups_[group]->canonicalTraceTopo();

    for (std::size_t trace = 0; trace < canonicalTraceNodes.size(); trace++)
    {
      std::vector<int> nodesTraceGlobal(canonicalTraceNodes[trace].size(),-1);
      for (std::size_t i = 0; i < canonicalTraceNodes[trace].size(); i++)
        nodesTraceGlobal[i] = elemMap[canonicalTraceNodes[trace][i]];

      // remove any cell trace that matches an existing ghost trace
      UniqueElem cellTrace(canonicalTraceTopo[trace], nodesTraceGlobal);

      for (std::size_t cellGroup = 0; cellGroup < uniqueGhostTraces.size(); cellGroup++)
      {
        typename UniqueTraceSet::iterator it = uniqueGhostTraces[cellGroup].find(cellTrace);

        if ( it != uniqueGhostTraces[cellGroup].end() )
          uniqueGhostTraces[cellGroup].erase( it );
      }
    }
  }

#if 0
  // remove periodic boundary elements from the ghost boundaries
  for (std::size_t elem = 0; elem < bndBuffer_.size(); elem++)
  {
    const int bndGroup = bndBuffer_[elem].group;
    const int bndrank = bndBuffer_[elem].rank;
    const std::vector<int>& elemMap = bndBuffer_[elem].elemMap;

    for ( const PeriodicBCNodeMap& period : periodicity_ )
    {
      int bndGroupIDL = period.bndGroupL;
      int bndGroupIDR = period.bndGroupR;

      // only consider periodic boundaries with elements that are not possessed by this
      // processor (i.e. ghost elements)
      if (bndrank != comm_rank &&
          (bndGroupIDL == bndGroup || bndGroupIDR == bndGroup))
      {
        UniqueTrace bndTrace(bndTopos_[bndGroup], elem, -1, elemMap);

        for (std::size_t group = 0; group < uniqueGhostTraces.size(); group++)
        {
          typename std::set< UniqueTrace >::iterator it = uniqueGhostTraces[group].find(bndTrace);

          if ( it != uniqueGhostTraces[group].end() )
            uniqueGhostTraces[group].erase( it );
        }
      }
    }
  }
#endif

  // remove boundary elements from the ghost boundaries
  for (std::size_t elem = 0; elem < bndBuffer_.size(); elem++)
  {
    const int bndGroup = bndBuffer_[elem].group;
    const std::vector<int>& elemMap = bndBuffer_[elem].elemMap;

    UniqueElem bndTrace(bndTopos_[bndGroup], elemMap);

    for (std::size_t group = 0; group < uniqueGhostTraces.size(); group++)
    {
      typename UniqueTraceSet::iterator it = uniqueGhostTraces[group].find(bndTrace);

      if ( it != uniqueGhostTraces[group].end() )
        uniqueGhostTraces[group].erase( it );
    }
  }

  // prepare the ghost groups for population
  resetGhostGroups();

  // add the ghost element (the element rank and element index are irrelevant)
  for (std::size_t group = 0; group < uniqueGhostTraces.size(); group++)
    for (const UniqueElem& trace : uniqueGhostTraces[group])
      ghostBndGroups_[group]->addElement(0, 0, trace.sortedNodes());

#endif
}


//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::
balanceElementsWithPartition(const PartitionType& part,
                             std::vector<GlobalMPIElement>& buffer)
{
#ifdef SANS_MPI
  std::vector<std::vector<GlobalMPIElement>> sendBuffer(comm_size_);
  std::vector<std::vector<GlobalMPIElement>> recvBuffer(comm_size_);

  // construct a global ID to local index map
  std::map<int,int> global2local;
  for (std::size_t elem = 0; elem < buffer.size(); elem++ )
    global2local[buffer[elem].elemID] = elem;


  // setup all the elements to send to other processors
  for (std::size_t rank = 0; rank < part.size(); rank++)
  {
    for (const std::pair<int,int>& elemrank : part[rank])
    {
      // copy out the element and set the possessed rank
      GlobalMPIElement element = buffer[global2local.at(elemrank.first)];
      element.rank = elemrank.second;
      // Add each element to the rank buffer where the element needs to be sent
      sendBuffer[rank].push_back(element);
    }
  }

  // send all local elements to all other processors they need to be on
  boost::mpi::all_to_all(*comm_, sendBuffer, recvBuffer);
  sendBuffer.clear();
  buffer.clear();

  // put the balanced elements back in the buffer so new node maps can be generated
  for (int rank = 0; rank < comm_size_; rank++)
    buffer.insert(buffer.end(), recvBuffer[rank].begin(), recvBuffer[rank].end());
#endif
}


//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::
sendElementsWithNodes(const std::map<int,int>& nativeNodeRank,
                      const std::vector<LagrangeElementGroup_ptr>& groups,
                      const bool bndBuffer,
                      const std::vector<GlobalMPIElement>& buffer,
                      std::vector<std::vector<GlobalMPIElement>>& recvBuffer )
{
#ifdef SANS_MPI
  std::vector<std::vector<GlobalMPIElement>> sendBuffer(comm_size_);
  recvBuffer.resize(comm_size_);

  for (std::size_t elem = 0; elem < buffer.size(); elem++ )
  {
    const int group = buffer[elem].group;
    const std::vector<int>& canonicalNodes = groups[group]->canonicalNodes();
    const std::vector<int>& elemMap = buffer[elem].elemMap;

    // extract the unique nodes from the element map
    std::set<int> uniqueRank;
    for (std::size_t n = 0; n < canonicalNodes.size(); n++)
    {
      int nativeNode = elemMap[canonicalNodes[n]];

      // get the rank where the element should be added
      // elements will now be duplicated to multiple different ranks
      int rank = nativeNodeRank.at(nativeNode);
      uniqueRank.insert(rank);

      // Also use nodes based on periodicity of the domain
      // this guarantees that periodic cells show up on the same rank
      for ( const PeriodicBCNodeMap& period : periodicity_ )
      {
        // only consider right groups if the buffer is boundary elements
        if ((period.bndGroupR != group) && bndBuffer) continue;

        auto itR = period.mapRtoL.find(nativeNode);
        if (itR != period.mapRtoL.end())
        {
          int nativeNodeL = itR->second;

          // get the rank that 'possess' the node
          int rankL = nativeNodeRank.at(nativeNodeL);
          uniqueRank.insert(rankL);
        }
      }
    }

    // Add each element to the rank buffer where the element needs to be sent
    for (const int rank : uniqueRank)
      sendBuffer[rank].push_back(buffer[elem]);
  }

  // send all local elements to all other processors they need to be on
  boost::mpi::all_to_all(*comm_, sendBuffer, recvBuffer);
#endif
}

//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::
balanceElementsWithNodes(const std::map<int,int>& nativeNodeRank,
                         const std::vector<LagrangeElementGroup_ptr>& groups,
                         const bool bndBuffer,
                         std::vector<GlobalMPIElement>& buffer )
{
#ifdef SANS_MPI
  std::vector<std::vector<GlobalMPIElement>> recvBuffer;

  // transfer the elements based on globalNodeRank
  sendElementsWithNodes(nativeNodeRank, groups, bndBuffer, buffer, recvBuffer);

  buffer.clear();

  // put the balanced elements back in the buffer so new node maps can be generated
  for (int rank = 0; rank < comm_size_; rank++)
    buffer.insert(buffer.end(), recvBuffer[rank].begin(), recvBuffer[rank].end());
#endif
}

//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::
appendElementsWithNodes(const std::map<int,int>& nativeNodeRank,
                        const std::vector<LagrangeElementGroup_ptr>& groups,
                        const bool bndBuffer,
                        std::vector<GlobalMPIElement>& buffer )
{
#ifdef SANS_MPI
  std::vector<std::vector<GlobalMPIElement>> recvBuffer;

  // transfer the elements based on globalNodeRank
  sendElementsWithNodes(nativeNodeRank, groups, bndBuffer, buffer, recvBuffer);

  std::set<GlobalMPIElement> uniqueBuffer;
  uniqueBuffer.insert(buffer.begin(), buffer.end());
  buffer.clear();

  // put the transfered elements into the unique buffer
  for (int rank = 0; rank < comm_size_; rank++)
    uniqueBuffer.insert(recvBuffer[rank].begin(), recvBuffer[rank].end());

  // re-populate the buffer with unique elements
  buffer.insert(buffer.end(), uniqueBuffer.begin(), uniqueBuffer.end());
#endif
}

//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::
getNodeRanking(const std::map<int,int>& nativeNodeCellMinRank,
               std::map<int,int>& nativeNodeRank )
{
#ifdef SANS_MPI
  std::vector<std::set<int>> needNodeRank(comm_size_);
  std::vector<std::set<int>> sendNodeRank(comm_size_);

  for (const std::pair<int,int>& node : nativeNodeRank)
    needNodeRank[node.second].insert(node.first);

  // send the node rank needs based on the cell min partition to the ranks that ultimately need them
  boost::mpi::all_to_all(*comm_, needNodeRank, sendNodeRank);
  needNodeRank.clear();

  std::vector<std::map<int,int>> sendNodeRankCellMin(comm_size_);
  std::vector<std::map<int,int>> recvNodeRankCellMin(comm_size_);

  // sort out which rank has the needed cells
  for (int rank = 0; rank < comm_size_; rank++)
    for (const int nodeGlobal : sendNodeRank[rank])
      sendNodeRankCellMin[rank][nodeGlobal] = nativeNodeCellMinRank.at(nodeGlobal);

  // send the cell needs to the ranks have the cells
  boost::mpi::all_to_all(*comm_, sendNodeRankCellMin, recvNodeRankCellMin);
  sendNodeRankCellMin.clear();

  // flatten the node rank information
  nativeNodeRank.clear();
  for (const std::map<int,int> nodemap : recvNodeRankCellMin)
    nativeNodeRank.insert(nodemap.begin(), nodemap.end());

  // Also use minimum rank nodes based on periodicity of the domain
  for ( const PeriodicBCNodeMap& period : periodicity_ )
  {
    for (const std::pair<int,int>& it : period.mapRtoL)
    {
      int nodeGlobalR = it.first;
      int nodeGlobalL = it.second;

      // compute the minimum rank based on both nodes
      auto itR = nativeNodeRank.find(nodeGlobalR);
      auto itL = nativeNodeRank.find(nodeGlobalL);
      if (itR != nativeNodeRank.end() && itL != nativeNodeRank.end())
      {
        int rankMin = std::min(itR->second, itL->second);
        nativeNodeRank[nodeGlobalR] = rankMin;
        nativeNodeRank[nodeGlobalL] = rankMin;
      }
    }
  }
#endif
}

//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::balanceWithNodes()
{
  SANS_ASSERT_MSG(nDOF_global_  > 0, "DOFs must be added before balancing");
  SANS_ASSERT_MSG(nCell_global_ > 0, "Cells must be added before balancing");
  SANS_ASSERT_MSG(nBnd_global_  > 0, "Boundary traces must be added before balancing");

#ifdef SANS_MPI

  // Use a local scope to destroy the temporary globalNodeRank
  {
    // Count the total number of nodes in the XField and crate a node global to continuous map
    std::map<int,int> globalNodeRank = continuousNodeRank(cellBuffer_);

    // balance the elements based on the node rank
    balanceElementsWithNodes(globalNodeRank, cellGroups_, false, cellBuffer_);
    balanceElementsWithNodes(globalNodeRank, bndGroups_,  true,  bndBuffer_);
  }

  // get the new node map since the elements have been balanced
  nativeNodeRank_ = continuousNodeRank(cellBuffer_);

  //---------------------------------------------------------------------//
  // set the rank for the elements now that they are balanced and construct ghost boundary elements
  for (std::size_t elem = 0; elem < cellBuffer_.size(); elem++)
  {
    const int group = cellBuffer_[elem].group;
    const std::vector<int>& elemMap = cellBuffer_[elem].elemMap;

    // get the minimum rank based on the nodes
    const std::vector<int>& canonicalNodes = cellGroups_[group]->canonicalNodes();
    int minrank = comm_size_;
    for (std::size_t n = 0; n < canonicalNodes.size(); n++)
    {
      int nodeGlobal = elemMap[canonicalNodes[n]];
      int rank = nativeNodeRank_.at(nodeGlobal);
      minrank = std::min(minrank, rank);
    }

    // set the rank that possess this element
    cellBuffer_[elem].rank = minrank;
  }

#if 0 // for debugging
  dumpBuffer("cellBuffer_", cellBuffer_);
#endif

  // Save off the boundary elements from the buffer
  for (std::size_t elem = 0; elem < bndBuffer_.size(); elem++)
  {
    const std::vector<int>& elemMap = bndBuffer_[elem].elemMap;

    int minrank = comm_size_;
    for (std::size_t n = 0; n < elemMap.size(); n++)
    {
      int rank = nativeNodeRank_.at(elemMap[n]);
      minrank = std::min(minrank, rank);
    }

    // set the rank that possess this element
    bndBuffer_[elem].rank = minrank;
  }

#if 0 // for debugging
  dumpBuffer("bndBuffer_", bndBuffer_);
#endif

  //---------------------------------------------------------------------//
  // construct ghost boundaries
  //---------------------------------------------------------------------//

  typedef std::unordered_set<UniqueElem, boost::hash<UniqueElem>> UniqueTraceSet;
  std::vector< UniqueTraceSet > uniqueGhostTraces(cellGroups_.size());

  // add all unique traces on cells (ghost and possessed)
  // this will leave only boundary traces in uniqueGhostTraces
  for (std::size_t elem = 0; elem < cellBuffer_.size(); elem++)
  {
    const int group = cellBuffer_[elem].group;
    const std::vector<int>& elemMap = cellBuffer_[elem].elemMap;

    const std::vector<std::vector<int>>& canonicalTraceNodes = cellGroups_[group]->canonicalTracesQ1();
    const std::vector<TopologyTypes>& canonicalTraceTopo = cellGroups_[group]->canonicalTraceTopo();

    for (std::size_t trace = 0; trace < canonicalTraceNodes.size(); trace++)
    {
      std::vector<int> nodesTraceGlobal(canonicalTraceNodes[trace].size(),-1);

      // extract the global nodes from the map
      for (std::size_t i = 0; i < canonicalTraceNodes[trace].size(); i++)
        nodesTraceGlobal[i] = elemMap[canonicalTraceNodes[trace][i]];

      // add the ghost trace, or remove it if it already exists (this happens when two connected cells are ghost cells)
      UniqueElem ghostTrace(canonicalTraceTopo[trace], nodesTraceGlobal);

      bool found = false;
      for (std::size_t cellGroup = 0; cellGroup < uniqueGhostTraces.size(); cellGroup++)
      {
        typename UniqueTraceSet::iterator it = uniqueGhostTraces[cellGroup].find(ghostTrace);

        if ( it != uniqueGhostTraces[cellGroup].end() )
        {
          uniqueGhostTraces[cellGroup].erase( it );
          found = true;
          break;
        }
      }
      if (!found)
        uniqueGhostTraces[group].insert( ghostTrace );
    }
  }

  // remove all boundary traces from the ghost trace elements
  for (std::size_t elem = 0; elem < bndBuffer_.size(); elem++)
  {
    const int bndGroup = bndBuffer_[elem].group;
    const std::vector<int>& elemMap = bndBuffer_[elem].elemMap;

    UniqueElem bndTrace(bndTopos_[bndGroup], elemMap);

    for (std::size_t group = 0; group < uniqueGhostTraces.size(); group++)
    {
      typename UniqueTraceSet::iterator it = uniqueGhostTraces[group].find(bndTrace);

      if ( it != uniqueGhostTraces[group].end() )
         uniqueGhostTraces[group].erase( it );
    }
  }

  // prepare the ghost groups for population
  resetGhostGroups();

  // add the ghost element (the element rank and element index are irrelevant)
  for (std::size_t group = 0; group < uniqueGhostTraces.size(); group++)
    for (const UniqueElem& trace : uniqueGhostTraces[group])
      ghostBndGroups_[group]->addElement(0, 0, trace.sortedNodes());

#else

  // Count the number of node DOFs and set all NodeRanks to 0
  std::set<int> isNode;
  for (std::size_t cellElem = 0; cellElem < cellBuffer_.size(); cellElem++ )
  {
    const std::vector<int>& canonicalNodes = cellGroups_[cellBuffer_[cellElem].group]->canonicalNodes();
    for (std::size_t n = 0; n < canonicalNodes.size(); n++)
    {
      int node = cellBuffer_[cellElem].elemMap[canonicalNodes[n]];
      isNode.insert(node);
      nativeNodeRank_[node] = 0;
    }
  }

  // get the total number of nodes
  nNode_global_ = isNode.size();

  // nothing else to do in serial

#endif

  // construct the cell and boundary trace groups from the buffers
  buildGroups();
}


//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Lagrange<PhysDim>::syncDOFs()
{
#ifdef SANS_MPI

  // Number of DOFs on each processor
  int DOFperProc = nDOF_global_ / comm_size_;

  std::set<int> globalDOFset;
  std::vector<std::vector<int>> needDOFidx(comm_size_);
  std::vector<std::vector<int>> sendDOFidx(comm_size_);

  //---------------------------------------------------------------------//
  // construct global to local mapping and determine which ranks are storing
  // the DOFs required by this rank
  for (std::size_t elem = 0; elem < cellBuffer_.size(); elem++)
  {
    const std::vector<int>& elemMap = cellBuffer_[elem].elemMap;

    for (std::size_t i = 0; i < elemMap.size(); i++)
    {
      // store the unique set of global DOFs used on this processor
      globalDOFset.insert(elemMap[i]);

      // the rank that is storing the needed DOF in elemMap[i]
      int rank = MIN(comm_size_-1, elemMap[i] / DOFperProc);

      // store the index local to the other rank of the DOF needed from that rank
      needDOFidx[rank].push_back( elemMap[i] - rank*DOFperProc );
    }
  }

  // generate the global to local mapping (this is identity for a single processor)
  int iDOF_local = 0;
  for ( const int iDOF_global : globalDOFset )
    global2localDOFmap_[iDOF_global] = iDOF_local++;

  // let the other ranks know where to send its DOFs
  boost::mpi::all_to_all(*comm_, needDOFidx, sendDOFidx);
  needDOFidx.clear();

  std::vector<std::vector<GlobalDOFType>> sendDOFBuffer(comm_size_);
  std::vector<std::vector<GlobalDOFType>> recvDOFBuffer(comm_size_);

  // Fill the buffer to send DOFs to other processors
  for (int rank = 0; rank < comm_size_; rank++)
  {
    sendDOFBuffer[rank].resize(sendDOFidx[rank].size());
    for (std::size_t i = 0; i < sendDOFidx[rank].size(); i++)
      sendDOFBuffer[rank][i] = DOF_[sendDOFidx[rank][i]];
  }

  // send all requested DOFs to other ranks
  boost::mpi::all_to_all(*comm_, sendDOFBuffer, recvDOFBuffer);
  sendDOFBuffer.clear();

  // resize the local DOF vector
  DOF_.resize(global2localDOFmap_.size());

  // store the DOFs locally
  for (int rank = 0; rank < comm_size_; rank++)
  {
    for (std::size_t i = 0; i < recvDOFBuffer[rank].size(); i++)
    {
      const GlobalDOFType& DOF = recvDOFBuffer[rank][i];
      DOF_[global2localDOFmap_.at(DOF.idof)] = DOF;
    }
  }

#else

  // DOFs are already on the processor, nothing to do in serial

  // the global to local map is trivial in serial (just identity)
  for (std::size_t idof = 0; idof < DOF_.size(); idof++ )
    global2localDOFmap_[idof] = idof;

#endif

}

}
