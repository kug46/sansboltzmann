// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define FIELDBASE_INSTANTIATE
#include "FieldBase_impl.h"

#include "FieldArea.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"

#include "Surreal/SurrealS.h"

#include "UserVariables/BoltzmannNVar.h"
namespace SANS
{

template class FieldBase< FieldTraits<TopoD2, Real> >;
template class FieldBase< FieldTraits<TopoD2, SurrealS<1>> >;

template class FieldBase< FieldTraits<TopoD2, DLA::VectorS<1, Real>> >;
template class FieldBase< FieldTraits<TopoD2, DLA::VectorS<2, Real>> >;
template class FieldBase< FieldTraits<TopoD2, DLA::VectorS<3, Real>> >;
template class FieldBase< FieldTraits<TopoD2, DLA::VectorS<4, Real>> >;
template class FieldBase< FieldTraits<TopoD2, DLA::VectorS<5, Real>> >;
template class FieldBase< FieldTraits<TopoD2, DLA::VectorS<6, Real>> >;
// For Boltzmann implementation
template class FieldBase< FieldTraits<TopoD2, DLA::VectorS<9, Real>> >;
//template class FieldBase< FieldTraits<TopoD2, DLA::VectorS<13, Real>> >;
//template class FieldBase< FieldTraits<TopoD2, DLA::VectorS<16, Real>> >;
template class FieldBase< FieldTraits<TopoD2, DLA::VectorS<NVar, Real>> >;

template class FieldBase< FieldTraits<TopoD2, DLA::MatrixS<3, 3, Real>> >;
template class FieldBase< FieldTraits<TopoD2, DLA::MatrixS<3, 3, SurrealS<1>> > >;

template class FieldBase< FieldTraits<TopoD2, DLA::MatrixSymS<2, Real>> >;

template class FieldBase< FieldTraits<TopoD2, DLA::VectorS<2, DLA::VectorS<2, Real>>> >;
template class FieldBase< FieldTraits<TopoD2, DLA::VectorS<2, DLA::VectorS<3, Real>>> >;
template class FieldBase< FieldTraits<TopoD2, DLA::VectorS<2, DLA::VectorS<4, Real>>> >;
template class FieldBase< FieldTraits<TopoD2, DLA::VectorS<2, DLA::VectorS<5, Real>>> >;
//template class FieldBase< FieldTraits<TopoD2, DLA::VectorS<2, DLA::VectorS<9, Real>>> >;

}
