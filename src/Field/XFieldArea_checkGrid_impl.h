// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(XFIELDAREA_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include <sstream>

#include "XFieldArea.h"

namespace SANS
{

namespace XField_
{
//=============================================================================
template<class PhysDim, class TopoDim, class TopologyTrace, class TopologyL>
void
check_RightTopology<PhysDim, TopoDim, TopologyTrace, TopologyL>::
interiorTrace( const XFieldType& xfld, const int itraceGroup,
               const typename XFieldType::template FieldTraceGroupType<TopologyTrace>& traceGroup,
               std::vector< std::vector<int> >& cellCount )
{
  // determine topology for Right cell group
  const int groupR = traceGroup.getGroupRight();

  XFIELD_CHECK( groupR >= 0 && groupR < xfld.nCellGroups(), "groupR = %d, this->nCellGroups() = %d ", groupR, xfld.nCellGroups() );

  // dispatch check over elements of group
  if ( xfld.getCellGroupBase(groupR).topoTypeID() == typeid(Triangle) )
  {
    xfld.template checkInteriorTrace<TopologyTrace, TopologyL, Triangle>( itraceGroup, traceGroup, cellCount );
  }
  else if ( xfld.getCellGroupBase(groupR).topoTypeID() == typeid(Quad) )
  {
    xfld.template checkInteriorTrace<TopologyTrace, TopologyL, Quad>( itraceGroup, traceGroup, cellCount );
  }
  else
  {
    char msg[] = "Error in XField< PhysD2, TopoD2 >::checkcheck_RightTopology: unknown cell topology\n";
    SANS_DEVELOPER_EXCEPTION( msg );
  }
}

template<class PhysDim, class TopoDim, class TopologyTrace, class TopologyL>
void
check_RightTopology<PhysDim, TopoDim, TopologyTrace, TopologyL>::
boundaryTrace( const XFieldType& xfld, const int itraceGroup,
               const typename XFieldType::template FieldTraceGroupType<TopologyTrace>& traceGroup,
               std::vector< std::vector<int> >& cellCount )
{
  // determine topology for Right cell group
  const int groupR = traceGroup.getGroupRight();

  XFIELD_CHECK( groupR >= 0 && groupR < xfld.nCellGroups(), "groupR = %d, this->nCellGroups() = %d ", groupR, xfld.nCellGroups() );

  // dispatch check over elements of group
  if ( xfld.getCellGroupBase(groupR).topoTypeID() == typeid(Triangle) )
  {
    xfld.template checkBoundaryTraceConnected<TopologyTrace, TopologyL, Triangle>( itraceGroup, traceGroup, cellCount );
  }
  else if ( xfld.getCellGroupBase(groupR).topoTypeID() == typeid(Quad) )
  {
    xfld.template checkBoundaryTraceConnected<TopologyTrace, TopologyL, Quad>( itraceGroup, traceGroup, cellCount );
  }
  else
  {
    char msg[] = "Error in XField< PhysD2, TopoD2 >::checkcheck_RightTopology: unknown cell topology\n";
    SANS_DEVELOPER_EXCEPTION( msg );
  }
}

//=============================================================================
template<class PhysDim, class TopoDim, class TopologyTrace>
void
check_LeftTopology<PhysDim, TopoDim, TopologyTrace>::
interiorTrace( const XFieldType& xfld, const int itraceGroup,
               const typename XFieldType::template FieldTraceGroupType<TopologyTrace>& traceGroup,
               std::vector< std::vector<int> >& cellCount )
{
  // determine topology for Left cell group
  const int groupL = traceGroup.getGroupLeft();

  XFIELD_CHECK( groupL >= 0 && groupL < xfld.nCellGroups(), "groupL = %d, this->nCellGroups() = %d ", groupL, xfld.nCellGroups() );

  // determine topology for R
  if ( xfld.getCellGroupBase(groupL).topoTypeID() == typeid(Triangle) )
  {
    check_RightTopology<PhysDim, TopoDim, TopologyTrace, Triangle>::interiorTrace( xfld, itraceGroup, traceGroup, cellCount );
  }
  else if ( xfld.getCellGroupBase(groupL).topoTypeID() == typeid(Quad) )
  {
    check_RightTopology<PhysDim, TopoDim, TopologyTrace, Quad>::interiorTrace( xfld, itraceGroup, traceGroup, cellCount );
  }
  else
  {
    char msg[] = "Error in XField< PhysD2, TopoD2 >::checkInteriorTrace_LeftTopology: unknown cell topology\n";
    SANS_DEVELOPER_EXCEPTION( msg );
  }
}

template<class PhysDim, class TopoDim, class TopologyTrace>
void
check_LeftTopology<PhysDim, TopoDim, TopologyTrace>::
boundaryTrace( const XFieldType& xfld, const int itraceGroup,
               const typename XFieldType::template FieldTraceGroupType<TopologyTrace>& traceGroup,
               std::vector< std::vector<int> >& cellCount )
{
  // determine topology for Left cell group
  const int groupL = traceGroup.getGroupLeft();
  const int groupR = traceGroup.getGroupRight();

  XFIELD_CHECK( groupL >= 0 && groupL < xfld.nCellGroups(), "groupL = %d, this->nCellGroups() = %d ", groupL, xfld.nCellGroups() );

  if ( xfld.getCellGroupBase(groupL).topoTypeID() == typeid(Triangle) )
  {
    if ( groupR < 0 )
      // dispatch check over elements of group
      xfld.template checkBoundaryTrace<TopologyTrace, Triangle>( itraceGroup, traceGroup, cellCount );
    else
      check_RightTopology<PhysDim, TopoDim, TopologyTrace, Triangle>::boundaryTrace( xfld, itraceGroup, traceGroup, cellCount );
  }
  else if ( xfld.getCellGroupBase(groupL).topoTypeID() == typeid(Quad) )
  {
    if ( groupR < 0 )
      // dispatch check over elements of group
      xfld.template checkBoundaryTrace<TopologyTrace, Quad>( itraceGroup, traceGroup, cellCount );
    else
      check_RightTopology<PhysDim, TopoDim, TopologyTrace, Quad>::boundaryTrace( xfld, itraceGroup, traceGroup, cellCount );
  }
  else
  {
    char msg[] = "Error in XField< PhysD2, TopoD2 >::checkBoundaryTrace_LeftTopology: unknown cell topology\n";
    SANS_DEVELOPER_EXCEPTION( msg );
  }
}

} //namespace XField_


//=============================================================================
template<class PhysDim, class TopoDim>
void
XField< PhysDim, TopoDim >::checkGrid()
{
  initDefaultElmentIDs();

  std::vector< std::vector<int> > cellCount(this->nCellGroups());

  int nCellElem = 0;

  // loop over element groups
  for (int group = 0; group < this->nCellGroups(); group++)
  {
    nCellElem += this->getCellGroupBase(group).nElem();
    cellCount[group].resize( this->getCellGroupBase(group).nElem() );

    // dispatch integration over elements of group
    if ( this->getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      checkPositiveJacobianDeterminant<Triangle>( this->template getCellGroup<Triangle>(group) );
    }
    else if ( this->getCellGroupBase(group).topoTypeID() == typeid(Quad) )
    {
      checkPositiveJacobianDeterminant<Quad>( this->template getCellGroup<Quad>(group) );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
  }

  // loop over interior trace element groups
  for (int group = 0; group < this->nInteriorTraceGroups(); group++)
  {
    if ( this->getInteriorTraceGroupBase(group).topoTypeID() == typeid(Line) )
    {
      XField_::check_LeftTopology<PhysDim, TopoDim, Line>::
          interiorTrace( *this, group, this->template getInteriorTraceGroup<Line>(group), cellCount );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown trace topology" );
  }

  // loop over boundary trace element groups
  for (int group = 0; group < this->nBoundaryTraceGroups(); group++)
  {
    if ( this->getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Line) )
    {
      XField_::check_LeftTopology<PhysDim, TopoDim, Line>::
          boundaryTrace( *this, group, this->template getBoundaryTraceGroup<Line>(group), cellCount );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown trace topology" );
  }

  // loop over ghost boundary trace element groups
  for (int group = 0; group < ghostBoundaryTraceGroups_.size(); group++)
  {
    SANS_ASSERT( ghostBoundaryTraceGroups_[group] != NULL );
    if ( ghostBoundaryTraceGroups_[group]->topoTypeID() == typeid(Line) )
    {
      XField_::check_LeftTopology<PhysDim, TopoDim, Line>::
          boundaryTrace( *this, group, this->template getGhostBoundaryTraceGroup<Line>(group), cellCount );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown trace topology" );
  }

  // loop over element groups and check the cell counts
  for (int group = 0; group < this->nCellGroups(); group++)
  {
    const int nelem = this->getCellGroupBase(group).nElem();

    // Check that each cell is touched the same number of times as they have edges
    if ( this->getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      for (int elem = 0; elem < nelem; elem++)
        XFIELD_CHECK( cellCount[group][elem] == Triangle::NEdge, "cellCount[%d][%d] = %d", group, elem, cellCount[group][elem] );
    }
    else if ( this->getCellGroupBase(group).topoTypeID() == typeid(Quad) )
    {
      for (int elem = 0; elem < nelem; elem++)
        XFIELD_CHECK( cellCount[group][elem] == Quad::NEdge, "cellCount[%d][%d] = %d", group, elem, cellCount[group][elem] );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
  }

  // Check that the number of elements is set correctly
  XFIELD_CHECK( nCellElem == this->nElem_, "nCellElem = %d, nElem_ = %d", nCellElem, this->nElem_ );
}


//=============================================================================
template<class PhysDim, class TopoDim>
template <class TopologyTrace, class TopologyL, class TopologyR>
void
XField< PhysDim, TopoDim >::
checkInteriorTrace( const int itraceGroup,
                    const typename BaseType::template FieldTraceGroupType<TopologyTrace>& traceGroup,
                    std::vector< std::vector<int> >& cellCount ) const
{
  typedef typename BaseType::template FieldTraceGroupType<TopologyTrace> FieldTraceClass;
  typedef typename BaseType::template FieldCellGroupType<TopologyL> FieldCellLClass;
  typedef typename BaseType::template FieldCellGroupType<TopologyR> FieldCellRClass;

  int nodeMapT[ TopologyTrace::NNode ];
  int nodeMapL[ TopologyL::NNode ];
  int nodeMapR[ TopologyR::NNode ];

  const int groupL = traceGroup.getGroupLeft();
  const int groupR = traceGroup.getGroupRight();

  const int (*TraceNodesL)[ TopologyTrace::NTrace ] = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyL>::TraceNodes;
  const int (*TraceNodesR)[ TopologyTrace::NTrace ] = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyR>::TraceNodes;

  const int *OrientPosR = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyR>::OrientPos;
  const int *OrientNegR = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyR>::OrientNeg;

  const FieldCellLClass& cellGroupL = this->template getCellGroup<TopologyL>(groupL);
  const FieldCellRClass& cellGroupR = this->template getCellGroup<TopologyR>(groupR);

  typename FieldTraceClass::template ElementType<> elemTrace( traceGroup.basis() );
  typename FieldCellLClass::template ElementType<> elemCellL( cellGroupL.basis() );
  typename FieldCellRClass::template ElementType<> elemCellR( cellGroupR.basis() );

  const int nelem = traceGroup.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    const int cellL                       = traceGroup.getElementLeft( elem );
    const CanonicalTraceToCell canonicalL = traceGroup.getCanonicalTraceLeft( elem );

    const int cellR                       = traceGroup.getElementRight( elem );
    const CanonicalTraceToCell canonicalR = traceGroup.getCanonicalTraceRight( elem );

    XFIELD_CHECK( cellL >= 0 && cellL < cellGroupL.nElem(), "cellL = %d, cellGroupL.nElem() = %d", cellL, cellGroupL.nElem() );
    XFIELD_CHECK( cellR >= 0 && cellR < cellGroupR.nElem(), "cellR = %d, cellGroupR.nElem() = %d", cellR, cellGroupR.nElem() );

    cellCount[groupL][cellL]++;
    cellCount[groupR][cellR]++;

    XFIELD_CHECK( canonicalL.trace >= 0 && canonicalL.trace < TopologyL::NTrace,
                  "canonicalL.trace = %d, TopologyL::NTrace = %d", canonicalL.trace, TopologyL::NTrace );
    XFIELD_CHECK( canonicalR.trace >= 0 && canonicalR.trace < TopologyR::NTrace,
                  "canonicalR.trace = %d, TopologyR::NTrace = %d", canonicalR.trace, TopologyR::NTrace );

    XFIELD_CHECK( canonicalL.orientation ==  1, "canonicalL.orientation = %d", canonicalL.orientation );
    XFIELD_CHECK( canonicalR.orientation == -1, "canonicalR.orientation = %d", canonicalR.orientation );

    traceGroup.getElement( elemTrace, elem );
    cellGroupL.getElement( elemCellL, cellL );
    cellGroupR.getElement( elemCellR, cellR );

    traceGroup.associativity( elem ).getNodeGlobalMapping( nodeMapT, TopologyTrace::NNode );
    cellGroupL.associativity( cellL ).getNodeGlobalMapping( nodeMapL, TopologyL::NNode );
    cellGroupR.associativity( cellR ).getNodeGlobalMapping( nodeMapR, TopologyR::NNode );

    // Check the node mappings are consistent
    for (int i = 0; i < TopologyTrace::NNode; i++ )
    {
      const int *Orient = canonicalR.orientation > 0 ? OrientPosR : OrientNegR;
      bool error = false;

      if (nodeMapL[TraceNodesL[canonicalL.trace][i]] != nodeMapT[i])
        error = true;

      if (nodeMapR[TraceNodesR[canonicalR.trace][Orient[i]]] != nodeMapT[i] )
        error = true;

      if (error)
      {
        // only construct std::stringstream if there is an error.
        // The constructor is very expensive time-wise for local solves
        std::stringstream msg;
        if (nodeMapL[TraceNodesL[canonicalL.trace][i]] != nodeMapT[i])
        {
          msg << std::endl;
          msg << "Interior trace nodes do not match left cell canonical trace nodes." << std::endl;
        }

        if (nodeMapR[TraceNodesR[canonicalR.trace][Orient[i]]] != nodeMapT[i] )
        {
          msg << std::endl;
          msg << "Interior trace nodes do not match right cell canonical trace nodes." << std::endl;
        }

        msg << std::endl;
        msg << "--- Trace ---" << std::endl;
        msg << "Trace Group = " << itraceGroup << std::endl;
        msg << "Group Element number = " << elem << std::endl;
        msg << "Order = " << traceGroup.basis()->order() << std::endl;
        msg << "Nodes = { ";
        for (int n = 0; n < TopologyTrace::NNode-1; n++)
          msg << nodeMapT[n] << ", ";
        msg << nodeMapT[TopologyTrace::NNode-1] << " } " << std::endl;

        msg << std::endl;
        msg << "--- Left Cell ---" << std::endl;
        msg << "Canonical Trace = " << canonicalL.trace << std::endl;
        msg << "Canonical Orientation = " << canonicalL.orientation << std::endl;
        msg << "Order = " << cellGroupL.basis()->order() << std::endl;
        msg << "Canonical Trace nodes = { ";
        for (int n = 0; n < TopologyTrace::NNode-1; n++)
          msg << nodeMapL[TraceNodesL[canonicalL.trace][n]] << ", ";
        msg << nodeMapL[TraceNodesL[canonicalL.trace][TopologyTrace::NNode-1]] << " } " << std::endl;

        msg << "Element number = " << cellL << std::endl;
        msg << "Nodes = { ";
        for (int n = 0; n < TopologyL::NNode-1; n++)
          msg << nodeMapL[n] << ", ";
        msg << nodeMapL[TopologyL::NNode-1] << " } " << std::endl;

        msg << std::endl;
        msg << "--- Right Cell ---" << std::endl;
        msg << "Canonical Trace = " << canonicalR.trace << std::endl;
        msg << "Canonical Orientation = " << canonicalR.orientation << std::endl;
        msg << "Order = " << cellGroupR.basis()->order() << std::endl;
        msg << "Canonical Trace nodes = { ";
        for (int n = 0; n < TopologyTrace::NNode-1; n++)
          msg << nodeMapR[TraceNodesR[canonicalR.trace][Orient[n]]] << ", ";
        msg << nodeMapR[TraceNodesR[canonicalR.trace][Orient[TopologyTrace::NNode-1]]] << " } " << std::endl;

        msg << "Element number = " << cellR << std::endl;
        msg << "Nodes = { ";
        for (int n = 0; n < TopologyR::NNode-1; n++)
          msg << nodeMapR[n] << ", ";
        msg << nodeMapR[TopologyR::NNode-1] << " } " << std::endl;

        XFIELD_EXCEPTION( msg.str() );
      }
    }

    // check edge signs for area elements (L is +, R is -)
    XFIELD_CHECK( elemCellL.edgeSign()[canonicalL.trace] == +1,
                  "elemCellL(%d).edgeSign()[%d] = %d", cellL, canonicalL.trace, elemCellL.edgeSign()[canonicalL.trace] );
    XFIELD_CHECK( elemCellR.edgeSign()[canonicalR.trace] == -1,
                  "elemCellR(%d).edgeSign()[%d] = %d", cellR, canonicalR.trace, elemCellR.edgeSign()[canonicalR.trace] );

    // Check the normal vector
    checkTraceNormal<TopologyTrace, TopologyL>(traceGroup, elem, elemTrace, elemCellL);
  }
}

//=============================================================================
template<class PhysDim, class TopoDim>
template <class TopologyTrace, class TopologyL>
void
XField< PhysDim, TopoDim >::
checkBoundaryTrace( const int itraceGroup,
                    const typename BaseType::template FieldTraceGroupType<TopologyTrace>& traceGroup,
                    std::vector< std::vector<int> >& cellCount ) const
{
  typedef typename BaseType::template FieldTraceGroupType<TopologyTrace> FieldTraceClass;
  typedef typename BaseType::template FieldCellGroupType<TopologyL> FieldCellLClass;

  int nodeMapT[ TopologyTrace::NNode ];
  int nodeMapL[ TopologyL::NNode ];

  const int groupL = traceGroup.getGroupLeft();

  const int (*TraceNodesL)[ TopologyTrace::NTrace ] = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyL>::TraceNodes;

  const FieldCellLClass& cellGroupL = this->template getCellGroup<TopologyL>(groupL);

  typename FieldTraceClass::template ElementType<> elemTrace( traceGroup.basis() );
  typename FieldCellLClass::template ElementType<> elemCellL( cellGroupL.basis() );

  const int nelem = traceGroup.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    const int cellL                       = traceGroup.getElementLeft( elem );
    const CanonicalTraceToCell canonicalL = traceGroup.getCanonicalTraceLeft( elem );

    XFIELD_CHECK( cellL >= 0 && cellL < cellGroupL.nElem(), "cellL = %d, cellGroupL.nElem() = %d", cellL, cellGroupL.nElem() );

    cellCount[groupL][cellL]++;

    XFIELD_CHECK( canonicalL.trace >= 0 && canonicalL.trace < TopologyL::NTrace,
                  "canonicalL.trace = %d, TopologyL::NTrace = %d", canonicalL.trace, TopologyL::NTrace );

    XFIELD_CHECK( canonicalL.orientation == 1,
                  "canonicalL.orientation = %d", canonicalL.orientation );

    traceGroup.getElement( elemTrace, elem );
    cellGroupL.getElement( elemCellL, cellL );

    traceGroup.associativity( elem ).getNodeGlobalMapping( nodeMapT, TopologyTrace::NNode );
    cellGroupL.associativity( cellL ).getNodeGlobalMapping( nodeMapL, TopologyL::NNode );

    // Check the node mappings are consistent
    for (int i = 0; i < TopologyTrace::NNode; i++ )
      if (nodeMapL[TraceNodesL[canonicalL.trace][i]] != nodeMapT[i])
      {
        std::stringstream msg;
        msg << std::endl;
        msg << "Boundary trace nodes do not match left cell canonical trace nodes." << std::endl;

        msg << std::endl;
        msg << "--- Trace ---" << std::endl;
        msg << "Trace Group = " << itraceGroup << std::endl;
        msg << "Group Element number = " << elem << std::endl;
        msg << "Nodes = { ";
        for (int n = 0; n < TopologyTrace::NNode-1; n++)
          msg << nodeMapT[n] << ", ";
        msg << nodeMapT[TopologyTrace::NNode-1] << " } " << std::endl;

        msg << std::endl;
        msg << "--- Left Cell ---" << std::endl;
        msg << "Canonical Trace = " << canonicalL.trace << std::endl;
        msg << "Canonical Orientation = " << canonicalL.orientation << std::endl;
        msg << "Canonical Trace nodes = { ";
        for (int n = 0; n < TopologyTrace::NNode-1; n++)
          msg << nodeMapL[TraceNodesL[canonicalL.trace][n]] << ", ";
        msg << nodeMapL[TraceNodesL[canonicalL.trace][TopologyTrace::NNode-1]] << " } " << std::endl;

        msg << "Element number = " << cellL << std::endl;
        msg << "Nodes = { ";
        for (int n = 0; n < TopologyL::NNode-1; n++)
          msg << nodeMapL[n] << ", ";
        msg << nodeMapL[TopologyL::NNode-1] << " } " << std::endl;

        XFIELD_EXCEPTION( msg.str() );
      }

    // check edge signs for area elements (L is +, R is -)
    XFIELD_CHECK( elemCellL.edgeSign()[canonicalL.trace] == +1,
                  "elemCellL.edgeSign()[%d] = %e", canonicalL.trace, elemCellL.edgeSign()[canonicalL.trace] );

    // Check the normal vector
    checkTraceNormal<TopologyTrace, TopologyL>(traceGroup, elem, elemTrace, elemCellL);
  }
}


//=============================================================================
template<class PhysDim, class TopoDim>
template <class TopologyTrace, class TopologyL, class TopologyR>
void
XField< PhysDim, TopoDim >::
checkBoundaryTraceConnected( const int itraceGroup,
                             const typename BaseType::template FieldTraceGroupType<TopologyTrace>& traceGroup,
                             std::vector< std::vector<int> >& cellCount ) const
{
  typedef typename BaseType::template FieldTraceGroupType<TopologyTrace> FieldTraceClass;
  typedef typename BaseType::template FieldCellGroupType<TopologyL> FieldCellLClass;
  typedef typename BaseType::template FieldCellGroupType<TopologyR> FieldCellRClass;

  int nodeMapT[ TopologyTrace::NNode ];
  int nodeMapL[ TopologyL::NNode ];

  const int groupL = traceGroup.getGroupLeft();
  const int groupR = traceGroup.getGroupRight();

  const int (*TraceNodesL)[ TopologyTrace::NTrace ] = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyL>::TraceNodes;

  const FieldCellLClass& cellGroupL = this->template getCellGroup<TopologyL>(groupL);
  const FieldCellRClass& cellGroupR = this->template getCellGroup<TopologyR>(groupR);

  typename FieldTraceClass::template ElementType<> elemTrace( traceGroup.basis() );
  typename FieldCellLClass::template ElementType<> elemCellL( cellGroupL.basis() );
  typename FieldCellRClass::template ElementType<> elemCellR( cellGroupR.basis() );

  const int nelem = traceGroup.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    const int cellL                       = traceGroup.getElementLeft( elem );
    const CanonicalTraceToCell canonicalL = traceGroup.getCanonicalTraceLeft( elem );

    const int cellR                       = traceGroup.getElementRight( elem );
    const CanonicalTraceToCell canonicalR = traceGroup.getCanonicalTraceRight( elem );

    XFIELD_CHECK( cellL >= 0 && cellL < cellGroupL.nElem(), "cellL = %d, cellGroupL.nElem() = %d", cellL, cellGroupL.nElem() );
    XFIELD_CHECK( cellR >= 0 && cellR < cellGroupR.nElem(), "cellR = %d, cellGroupR.nElem() = %d", cellR, cellGroupR.nElem() );

    cellCount[groupL][cellL]++;
    cellCount[groupR][cellR]++;

    XFIELD_CHECK( canonicalL.trace >= 0 && canonicalL.trace < TopologyL::NTrace,
                  "canonicalL.trace = %d, TopologyL::NTrace = %d", canonicalL.trace, TopologyL::NTrace );
    XFIELD_CHECK( canonicalR.trace >= 0 && canonicalR.trace < TopologyR::NTrace,
                  "canonicalR.trace = %d, TopologyR::NTrace = %d", canonicalR.trace, TopologyR::NTrace );

    XFIELD_CHECK( canonicalL.orientation ==  1, "canonicalL.orientation = %d", canonicalL.orientation );
    XFIELD_CHECK( canonicalR.orientation == -1, "canonicalR.orientation = %d", canonicalR.orientation );

    traceGroup.getElement( elemTrace, elem );
    cellGroupL.getElement( elemCellL, cellL );
    cellGroupR.getElement( elemCellR, cellR );

    traceGroup.associativity( elem ).getNodeGlobalMapping( nodeMapT, TopologyTrace::NNode );
    cellGroupL.associativity( cellL ).getNodeGlobalMapping( nodeMapL, TopologyL::NNode );

    // Check the node mappings are consistent
    for (int i = 0; i < TopologyTrace::NNode; i++ )
      if (nodeMapL[TraceNodesL[canonicalL.trace][i]] != nodeMapT[i])
      {
        std::stringstream msg;
        msg << std::endl;
        msg << "Connected boundary trace nodes do not match left cell canonical trace nodes." << std::endl;

        msg << std::endl;
        msg << "--- Trace ---" << std::endl;
        msg << "Trace Group = " << itraceGroup << std::endl;
        msg << "Group Element number = " << elem << std::endl;
        msg << "Nodes = { ";
        for (int n = 0; n < TopologyTrace::NNode-1; n++)
          msg << nodeMapT[n] << ", ";
        msg << nodeMapT[TopologyTrace::NNode-1] << " } " << std::endl;

        msg << "Canonical Trace = " << canonicalL.trace << std::endl;
        msg << "Canonical Orientation = " << canonicalL.orientation << std::endl;
        msg << "Canonical Trace nodes = { ";
        for (int n = 0; n < TopologyTrace::NNode-1; n++)
          msg << nodeMapL[TraceNodesL[canonicalL.trace][n]] << ", ";
        msg << nodeMapL[TraceNodesL[canonicalL.trace][TopologyTrace::NNode-1]] << " } " << std::endl;

        msg << std::endl;
        msg << "--- Left Cell ---" << std::endl;
        msg << "Element number = " << cellL << std::endl;
        msg << "Nodes = { ";
        for (int n = 0; n < TopologyL::NNode-1; n++)
          msg << nodeMapL[n] << ", ";
        msg << nodeMapL[TopologyL::NNode-1] << " } " << std::endl;

        XFIELD_EXCEPTION( msg.str() );
      }

    // check edge signs for area elements (L is +, R is -)
    XFIELD_CHECK( elemCellL.edgeSign()[canonicalL.trace] == +1,
                  "elemCellL.edgeSign()[%d] = %d", canonicalL.trace, elemCellL.edgeSign()[canonicalL.trace] );
    XFIELD_CHECK( elemCellR.edgeSign()[canonicalR.trace] == -1,
                  "elemCellR.edgeSign()[%d] = %d", canonicalR.trace, elemCellR.edgeSign()[canonicalR.trace] );

    // Check the normal vector
    checkTraceNormal<TopologyTrace, TopologyL>(traceGroup, elem, elemTrace, elemCellL);
  }
}

}
