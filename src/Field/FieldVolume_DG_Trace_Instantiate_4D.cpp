// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define FIELD_DG_TRACEBASE_INSTANTIATE
#include "Field_DG_TraceBase_impl.h"

#define FIELDVOLUME_DG_TRACE_INSTANTIATE
#include "FieldVolume_DG_Trace_impl.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "XFieldVolume.h"

namespace SANS
{

template class Field_DG_Trace< PhysD4, TopoD3, Real >;

template class Field_DG_Trace< PhysD4, TopoD3, DLA::VectorS<1, Real> >;
template class Field_DG_Trace< PhysD4, TopoD3, DLA::VectorS<2, Real> >;
template class Field_DG_Trace< PhysD4, TopoD3, DLA::VectorS<3, Real> >;
template class Field_DG_Trace< PhysD4, TopoD3, DLA::VectorS<4, Real> >;
template class Field_DG_Trace< PhysD4, TopoD3, DLA::VectorS<5, Real> >;

}
