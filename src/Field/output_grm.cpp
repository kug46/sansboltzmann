// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "output_grm.h"

#include "tools/SANSnumerics.h"     // Real

#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"

#include "XFieldLine.h"
#include "XFieldArea.h"
#include "XFieldVolume.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// output as P1
//
template<class PhysDim, class TopoDim, class Topology>
void
output_grm_CellGroup( const std::string& shape, const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfld, FILE* fp )
{
  int Q = 1; // Only outputting linear elements for now...
  int nelem = xfld.nElem();
  fprintf( fp, "%d\n", nelem );
  fprintf( fp, "%d\n", Q );
  fprintf( fp, "%s\n", shape.c_str() );
  fprintf( fp, "UniformNodeDistribution\n");

  // cell-to-node connectivity

  int nodeMap[Topology::NNode];
  for (int elem = 0; elem < nelem; elem++)
  {
    xfld.associativity( elem ).getNodeGlobalMapping( nodeMap, Topology::NNode );
    for (int n = 0; n < Topology::NNode; n++ )
      fprintf( fp, "%d ", nodeMap[n]+1 );
    fprintf( fp, "\n" );
  }
}


//----------------------------------------------------------------------------//
// output as P1
//
template<class PhysDim, class TopoDim, class Topology>
void
output_grm_TraceGroup( const std::string& shape,
                       const typename XField<PhysDim,TopoDim>::template FieldTraceGroupType<Topology>& xfld, FILE* fp )
{
  int nelem = xfld.nElem();
  fprintf( fp, "%d\n", nelem );
  fprintf( fp, "%s\n", shape.c_str() );

  // trace-to-node connectivity

  int nodeMap[Topology::NNode];
  for (int elem = 0; elem < nelem; elem++)
  {
    xfld.associativity( elem ).getNodeGlobalMapping( nodeMap, Topology::NNode );
    for (int n = 0; n < Topology::NNode; n++ )
      fprintf( fp, "%d ", nodeMap[n]+1 );
    fprintf( fp, "\n" );
  }

}

//----------------------------------------------------------------------------//
// Higher Order Node Output
//
void
output_grm_Nodes( const typename XField<PhysD2,TopoD2>::template FieldCellGroupType<Triangle>& xfldCell, int q, FILE* fp )
{
  typedef Triangle Topology;
  int nelem = xfldCell.nElem();
  typedef DLA::VectorS<PhysD2::D, Real> VectorX;
  const int nnode = xfldCell.nDOF();

  typedef typename XField<PhysD2, TopoD2>::template FieldCellGroupType<Topology> XFieldCellGroupClass;
  typedef typename XFieldCellGroupClass::template ElementType<> ElementType;


  std::vector<VectorX> nodelist(xfldCell.nDOF(),0);

  ElementType xfldElem( xfldCell.basis() );

  int nbnode = xfldCell.basis()->nBasisNode();
  int nbedge = xfldCell.basis()->nBasisEdge();
  int nbcell = xfldCell.basis()->nBasisCell();


  //set node, edge, cell DOF reference locations
  DLA::VectorD<Real> srefnode(nbnode);
  DLA::VectorD<Real> trefnode(nbnode);
  DLA::VectorD<Real> srefedge(nbedge);
  DLA::VectorD<Real> trefedge(nbedge);
  DLA::VectorD<Real> srefcell(nbcell);
  DLA::VectorD<Real> trefcell(nbcell);

  if (q==1)
  {
    srefnode = {0.0, 1.0, 0.0};
    trefnode = {0.0, 0.0, 1.0};

    srefedge = {};
    trefedge = {};

    srefcell = {};
    trefcell = {};
  }
  else if (q==2)
  {
    srefnode = {0.0, 1.0, 0.0};
    trefnode = {0.0, 0.0, 1.0};

    srefedge = {0.5, 0.0, 0.5};
    trefedge = {0.5, 0.5, 0.0};

    srefcell = {};
    trefcell = {};
  }
  else if (q==3)
  {
    srefnode = {0.0, 1.0, 0.0};
    trefnode = {0.0, 0.0, 1.0};

    srefedge = {2.0/3.0, 1.0/3.0, 0.0, 0.0, 1.0/3.0, 2.0/3.0};
    trefedge = {1.0/3.0, 2.0/3.0, 2.0/3.0, 1.0/3.0, 0.0, 0.0};

    srefcell = {1.0/3.0};
    trefcell = {1.0/3.0};
  }
  else if (q==4)
  {
    srefnode = {0.0, 1.0, 0.0};
    trefnode = {0.0, 0.0, 1.0};

    srefedge = {0.75, 0.5, 0.25, 0, 0, 0, 0.25, 0.5, 0.75};
    trefedge = {0.25, 0.5, 0.75, 0.75, 0.5, 0.25, 0, 0, 0};

    srefcell = {0.25, 0.5, 0.25};
    trefcell = {0.25, 0.25, 0.5};
  }
  else
  {
    const char msg[] = "desired q not implemented\n";
    SANS_DEVELOPER_EXCEPTION( msg );
  }



  Real s,t;
  for (int elem=0; elem < nelem; elem++)
  {
    xfldCell.getElement(xfldElem,elem);

    for (int n=0; n<nbnode; n++)
    {
      int node = xfldCell.associativity(elem).nodeGlobal( n );

      s = srefnode[n];
      t = trefnode[n];

      nodelist[node] = xfldElem.eval(s,t);
    }

    int nnodeedge = nbedge/Topology::NEdge;
    for (int n=0; n< Topology::NEdge; n++)
    {
      int edgesign = xfldCell.associativity(elem).edgeSign()[n];

      for (int m=0; m<nnodeedge; m++)
      {

        int edge = xfldCell.associativity(elem).edgeGlobal( n*nnodeedge + m );

        int ind = 0;
        if (edgesign>0) //flip node ordering if sign is negative
          ind = n*nnodeedge + m;
        else
          ind = (n+1)*nnodeedge-(m+1);

        s = srefedge[ind];
        t = trefedge[ind];

        nodelist[edge] = xfldElem.eval(s,t);
      }
    }


    for (int n=0; n<nbcell; n++)
    {
      int cell = xfldCell.associativity(elem).cellGlobal( n );

      s = srefcell[n];
      t = trefcell[n];

      nodelist[cell] = xfldElem.eval(s,t);
    }

  }

  for (int n = 0; n < nnode; n++)
  {
    for (int d = 0; d < PhysD2::D; d++)
      fprintf( fp, "%22.15e ", nodelist[n][d] );
    fprintf( fp, "\n");
  }


}

template<class PhysDim, class TopoDim, class Topology>
void
output_grm_CellGroup(
const std::string& shape, const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfld, int q, FILE* fp )
{
  int nelem = xfld.nElem();
  fprintf( fp, "%d\n", nelem );
  fprintf( fp, "%d\n", q );
  fprintf( fp, "%s\n", shape.c_str() );
  fprintf( fp, "UniformNodeDistribution\n");

  int nbnode = xfld.basis()->nBasisNode();
  int nbedge = xfld.basis()->nBasisEdge();
  int nbcell = xfld.basis()->nBasisCell();

  // cell-to-node connectivity

  for (int elem=0; elem < nelem; elem++)
  {

    for (int n=0; n<nbnode; n++)
    {
      int node = xfld.associativity(elem).nodeGlobal( n );
      fprintf( fp, "%d ", node+1 );
    }


    int nnodeedge = nbedge/Topology::NEdge;
    for (int n=0; n< Topology::NEdge; n++)
    {
      int edgesign = xfld.associativity(elem).edgeSign()[n];

      for (int m=0; m<nnodeedge; m++)
      {
        int ind = 0;
        if (edgesign>0)  //flip node ordering if sign is negative
          ind = n*nnodeedge + m;
        else
          ind = (n+1)*nnodeedge-(m+1);

        int edge = xfld.associativity(elem).edgeGlobal( ind );

        fprintf( fp, "%d ", edge+1 );

      }
    }

    for (int n=0; n<nbcell; n++)
    {
      int cell = xfld.associativity(elem).cellGlobal( n );
      fprintf( fp, "%d ", cell+1 );
    }

    fprintf( fp, "\n" );
  }
}

//----------------------------------------------------------------------------//
template<>
void
output_grm( const XField<PhysD2,TopoD2>& xgrid, const std::string& filename )
{
  std::cout << "output_grm: filename = " << filename << std::endl;
  FILE* fp = fopen( filename.c_str(), "w" );

  const int nnode = xgrid.nDOF();

  fprintf( fp, "%d %d %d %d\n", TopoD2::D, nnode, xgrid.nCellGroups(), xgrid.nBoundaryTraceGroups() );

  // grid coordinates

  for (int n = 0; n < nnode; n++)
  {
    for (int d = 0; d < PhysD2::D; d++)
      fprintf( fp, "%22.15e ", xgrid.DOF(n)[d] );
    fprintf( fp, "\n");
  }

  // loop over trace boundary groups
  for (int group = 0; group < xgrid.nBoundaryTraceGroups(); group++)
  {
    // dispatch integration over boundary group
    if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Line) )
      output_grm_TraceGroup<PhysD2, TopoD2, Line>("PXE_Shape_Edge", xgrid.getBoundaryTraceGroup<Line>(group), fp );
    else
    {
      const char msg[] = "Error in output_grm(XField<PhysD2,TopoD2>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  // loop over cell groups
  for (int group = 0; group < xgrid.nCellGroups(); group++)
  {
    // dispatch integration over cell groups
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
      output_grm_CellGroup<PhysD2, TopoD2, Triangle>("PXE_Shape_Triangle", xgrid.getCellGroup<Triangle>(group), fp );
    else if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Quad) )
      output_grm_CellGroup<PhysD2, TopoD2, Quad>("PXE_Shape_Quad", xgrid.getCellGroup<Quad>(group), fp );
    else
      SANS_DEVELOPER_EXCEPTION( "Error in output_grm(XField<PhysD2,TopoD2>): unknown topology\n" );
  }

  fclose( fp );
}

//----------------------------------------------------------------------------//
// Output to higher Q Lagrange Basis
template<>
void
output_grm( const XField<PhysD2,TopoD2>& xgrid, const int q, const std::string& filename )
{
  std::cout << "output_grm: filename = " << filename << std::endl;
  FILE* fp = fopen( filename.c_str(), "w" );

  const int nnode = xgrid.nDOF();

  fprintf( fp, "%d %d %d %d\n", TopoD2::D, nnode, xgrid.nCellGroups(), xgrid.nBoundaryTraceGroups() );

  // grid coordinates

  for (int group = 0; group < xgrid.nCellGroups(); group++)
  {
    // dispatch integration over cell groups
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
      output_grm_Nodes(xgrid.getCellGroup<Triangle>(group), q, fp );
    //else if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Quad) )
    //  output_grm_Nodes<PhysD2, TopoD2, Quad>(xgrid.getCellGroup<Quad>(group), q, fp );
    else
      SANS_DEVELOPER_EXCEPTION( "unknown topology\n" );
  }

  // loop over trace boundary groups
  for (int group = 0; group < xgrid.nBoundaryTraceGroups(); group++)
  {
    // dispatch integration over boundary group
    if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Line) )
      output_grm_TraceGroup<PhysD2, TopoD2, Line>("PXE_Shape_Edge", xgrid.getBoundaryTraceGroup<Line>(group), fp );
    else
      SANS_DEVELOPER_EXCEPTION( "unknown topology\n" );
  }

  // loop over cell groups
  for (int group = 0; group < xgrid.nCellGroups(); group++)
  {
    // dispatch integration over cell groups
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
      output_grm_CellGroup<PhysD2, TopoD2, Triangle>("PXE_Shape_Triangle", xgrid.getCellGroup<Triangle>(group), q, fp );
    else if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Quad) )
      output_grm_CellGroup<PhysD2, TopoD2, Quad>("PXE_Shape_Quad", xgrid.getCellGroup<Quad>(group), q, fp );
    else
      SANS_DEVELOPER_EXCEPTION( "unknown topology\n" );
  }

  fclose( fp );
}

//----------------------------------------------------------------------------//
#if 0
template<>
void
output_grm( const XField<PhysD3,TopoD3>& xgrid, const std::string& filename )
{
  std::cout << "output_grm: filename = " << filename << std::endl;
  FILE* fp = fopen( filename.c_str(), "w" );

  fprintf( fp, "\"\"\n" );
  fprintf( fp, "VARIABLES = \"X\", \"Y\", \"Z\"\n" );

  const int ngroup = xgrid.nCellGroups();

  // loop over cell groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Tet) )
    {
      if (group == 0)
        output_grm_CellGroup_Nodes<PhysD3, TopoD3, Tet>( xgrid.getCellGroup<Tet>(group), fp );
      else
        output_grm_CellGroup<PhysD3, TopoD3, Tet>( xgrid.getCellGroup<Tet>(group), fp );
    }
    else if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Hex) )
    {
      if (group == 0)
        output_grm_CellGroup_Nodes<PhysD3, TopoD3, Hex>( xgrid.getCellGroup<Hex>(group), fp );
      else
        output_grm_CellGroup<PhysD3, TopoD3, Hex>( xgrid.getCellGroup<Hex>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_grm(XField<PhysD3,TopoD3>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  // loop over trace boundary groups
  for (int group = 0; group < xgrid.nBoundaryTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      output_grm_TraceGroup<PhysD3, TopoD3, Triangle>("Boundary", group, xgrid.getBoundaryTraceGroup<Triangle>(group), fp );
    }
    else if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Quad) )
    {
      output_grm_TraceGroup<PhysD3, TopoD3, Quad>("Boundary", group, xgrid.getBoundaryTraceGroup<Quad>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_grm(XField<PhysD3,TopoD3>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  fclose( fp );
}
#endif

//Explicitly instantiate the function
// But instantiation after explicit specialization is not in effect
//template void
//output_grm( const XField<PhysD1,TopoD1>& xgrid, const std::string& filename );
//template void
//output_grm( const XField<PhysD2,TopoD2>& xgrid, const std::string& filename );
//template void
//output_grm( const XField<PhysD2,TopoD2>& xgrid, const int order, const std::string& filename  );
//template void
//output_grm( const XField<PhysD3,TopoD3>& xgrid, const std::string& filename );


} //namespace SANS
