// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDASSOCIATIVITYCONSTRUCTOR_H
#define FIELDASSOCIATIVITYCONSTRUCTOR_H

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Constructor class for FieldAssociativity
//
// An instances of FieldAssociativityConstructor must be constructed and
// populated in order to construct or resize a FieldAssociativity
//
//----------------------------------------------------------------------------//

class FieldAssociativityConstructorBase
{
public:
  // The virtual destructor is needed here so the derived class destructor is called
  // when a base class pointer is deallocated
  virtual ~FieldAssociativityConstructorBase() {}

  virtual int nBasis() const = 0;
};

template<class ElementAssociativityConstructor>
class FieldAssociativityConstructor : public FieldAssociativityConstructorBase
{
public:

  typedef typename ElementAssociativityConstructor::TopologyType TopologyType;
  typedef typename ElementAssociativityConstructor::BasisType BasisType;
  typedef ElementAssociativityConstructor ElementAssociativityConstructorType;

  // needed for new []
  FieldAssociativityConstructor() : nElem_(0), basis_(NULL), assoc_(NULL), isSetassoc_(NULL) {}

  // Constructor for the line associativity
  FieldAssociativityConstructor( const BasisType* basis, const int nelem ) : FieldAssociativityConstructor() { resize(basis, nelem); }
  virtual ~FieldAssociativityConstructor();

  int nElem() const { return nElem_; }

  // basis function
  const BasisType* basis() const { return basis_; }
  virtual int nBasis() const override { return basis_->nBasis(); }

  //Used to resize the in case an array is allocated
  void resize( const BasisType* basis, const int nelem );

  //No copy constructors
  FieldAssociativityConstructor( const FieldAssociativityConstructor& ) = delete;
  FieldAssociativityConstructor& operator=( const FieldAssociativityConstructor& ) = delete;

  void setAssociativity( const ElementAssociativityConstructor& elemassoc, const int elem );
  ElementAssociativityConstructor& setAssociativity( const int elem );
  const ElementAssociativityConstructor& getAssociativity( const int elem ) const
  {
    SANS_ASSERT_MSG( elem >= 0 && elem < nElem_, "elem = %d, nElem_ = %d", elem, nElem_ );
    SANS_ASSERT_MSG( isSetassoc_[elem], "elem = %d, nElem_ = %d", elem, nElem_ );
    return assoc_[elem];
  }

protected:
  int nElem_;                         // number of elements

  const BasisType* basis_;

  ElementAssociativityConstructor* assoc_;       // element DOF associations (local to global)
  bool* isSetassoc_;                  // flag: is DOF associations set for each element?
};

template<class ElementAssociativityConstructor>
void
FieldAssociativityConstructor<ElementAssociativityConstructor>::resize( const BasisType* basis, const int nelem )
{
  SANS_ASSERT( nelem >= 0 );

  nElem_ = nelem;

  basis_ = basis;

  delete [] assoc_; assoc_ = NULL;
  delete [] isSetassoc_; isSetassoc_ = NULL;

  // element DOF associativity
  assoc_ = new ElementAssociativityConstructor[nelem];
  isSetassoc_ = new bool[nelem];

  for (int n = 0; n < nelem; n++)
  {
    assoc_[n].resize(basis);
    isSetassoc_[n] = false;
  }
}

template<class ElementAssociativityConstructor>
FieldAssociativityConstructor<ElementAssociativityConstructor>::~FieldAssociativityConstructor()
{
  // edge-element DOF associativity
  delete [] assoc_;
  delete [] isSetassoc_;
}


template<class ElementAssociativityConstructor>
void
FieldAssociativityConstructor<ElementAssociativityConstructor>::setAssociativity( const ElementAssociativityConstructor& elemassoc, const int elem )
{
  SANS_ASSERT_MSG( elem >= 0 && elem < nElem_ && !isSetassoc_[elem], "with elem = %d, nElem_ = %d", elem, nElem_ );
  assoc_[elem] = elemassoc;
  isSetassoc_[elem] = true;
}

template<class ElementAssociativityConstructor>
ElementAssociativityConstructor&
FieldAssociativityConstructor<ElementAssociativityConstructor>::setAssociativity( const int elem )
{
  SANS_ASSERT_MSG( elem >= 0 && elem < nElem_, "with elem = %d, nElem_ = %d", elem, nElem_ );
  isSetassoc_[elem] = true;
  return assoc_[elem];
}

}

#endif //FIELDASSOCIATIVITYCONSTRUCTOR_H
