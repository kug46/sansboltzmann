// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define FIELDBASE_INSTANTIATE
#include "FieldBase_impl.h"

#define XFIELD_INSTANTIATE
#include "XField_impl.h"

#define XFIELDAREA_INSTANTIATE
#include "XFieldArea.h"
#include "XFieldArea_impl.h"
#include "XFieldArea_buildFrom_impl.h"
#include "XFieldArea_checkGrid_impl.h"

#include "BasisFunction/TraceToCellRefCoord.h"
#include "LinearAlgebra/DenseLinAlg/tools/cross.h"
#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

// Do not include XField_checkTraceNormal_impl.h as a special one is provided in this file

namespace SANS
{

//=============================================================================
template class FieldBase< XFieldTraits<PhysD3, TopoD2> >;


//=============================================================================
template<class PhysDim, class TopoDim>
template<class TopologyTrace, class TopologyL>
void
XField< PhysDim, TopoDim >::
checkTraceNormal( const typename BaseType::template FieldTraceGroupType<TopologyTrace>& traceGroup, int elem,
                  ElementXField<PhysDim, typename TopologyTrace::TopoDim, TopologyTrace>& elemTrace,
                  const ElementXField<PhysDim, TopoDim, TopologyL>& elemCellL ) const
{
  typedef typename BaseType::template
                   FieldCellGroupType<TopologyL>::template
                   ElementType<>::RefCoordType RefCoordCellLType;

  VectorX N, cT, cL, dX, NL, Xs; // Normal and centroids

  // Get the trace element
  traceGroup.getElement( elemTrace, elem );

  RefCoordCellLType sRef;              // reference-element coordinates (s,t)

  const CanonicalTraceToCell canonicalL = traceGroup.getCanonicalTraceLeft( elem );

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyL>::eval( canonicalL, TopologyTrace::centerRef, sRef );

  // Get the normal vector on the left cell
  elemCellL.unitNormal(sRef, N);

  // Get the trace tangent and centroid
  elemTrace.tangent( TopologyTrace::centerRef, Xs);
  elemTrace.coordinates( TopologyTrace::centerRef, cT );

  //Get the left cell centroid
  elemCellL.coordinates( TopologyL::centerRef, cL );

  // Compute the 'cell-to-cell' normal from the left element
  NL = cross( Xs, N );

  // Compute the vector from the trace-to-cell centroids
  dX = cL - cT;

  // normal should point from left to right
  XFIELD_CHECK( dot(NL,dX) < 0, "dot(NL,dX) = %e ", dot(NL,dX) );
}



//=============================================================================
//Explicitly instantiate
template class XField< PhysD3, TopoD2 >;

}
