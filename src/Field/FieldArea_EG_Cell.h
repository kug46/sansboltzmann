// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDAREA_EG_CELL_H
#define FIELDAREA_EG_CELL_H

#include "Field_DG_CellBase.h"
#include "FieldArea_CG_Cell.h"
#include "FieldArea.h"
#include "XFieldArea.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// 2-D solution field: EG cell-field with dependence on neighbors
//----------------------------------------------------------------------------//

template <class PhysDim, class T>
class Field_EG_Cell<PhysDim, TopoD2, T> : public Field_DG_CellBase< PhysDim, TopoD2, T >
{
public:
  typedef Field_DG_CellBase< PhysDim, TopoD2, T > BaseType;
  typedef Field_CG_Cell<PhysDim, TopoD2, T> CGFieldType;

  typedef T ArrayQ;

  Field_EG_Cell( const Field_CG_Cell<PhysDim, TopoD2, T>& cgfld, const int order, const BasisFunctionCategory& category );

  Field_EG_Cell( const Field_CG_Cell<PhysDim, TopoD2, T>& cgfld, const int order,
                 const BasisFunctionCategory& category, const std::vector<int>& CellGroups );

  // Groups are first to remove ambiguity with list initializers
  // DG Fields are already broken, can just cat the CellGroupSets and forward
  explicit Field_EG_Cell( const std::vector<std::vector<int>>& CellGroupSets,
                          const Field_CG_Cell<PhysDim, TopoD2, T>& cgfld, const int order, const BasisFunctionCategory& category )
  : Field_EG_Cell( cgfld, order, category, SANS::cat(CellGroupSets)) {}

  Field_EG_Cell( const Field_EG_Cell& fld, const FieldCopy& tag );

  Field_EG_Cell& operator=( const ArrayQ& q );

  virtual ~Field_EG_Cell() {}

protected:
  void init( const int order, const BasisFunctionCategory& category, const std::vector<int>& CellGroups );

  const Field_CG_Cell<PhysDim, TopoD2, T>& cgfld_;
  using BaseType::nDOF_;
  using BaseType::DOF_;
  using BaseType::cellGroups_;
  using BaseType::globalCellGroups_;
  using BaseType::xfld_;
};


}

#endif  // FIELDAREA_EG_CELL_H
