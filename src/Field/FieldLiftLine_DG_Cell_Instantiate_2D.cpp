// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define FIELDBASE_INSTANTIATE
#include "FieldBase_impl.h"

#define FIELDSUBGROUP_INSTANTIATE
#include "FieldSubGroup_impl.h"

#define FIELDLIFT_DG_CELLBASE_INSTANTIATE
#include "FieldLift_DG_CellBase_impl.h"

#define FIELDLIFTLINE_DG_CELL_INSTANTIATE
#include "FieldLiftLine_DG_Cell_impl.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "UserVariables/BoltzmannNVar.h"
#include "XFieldLine.h"

namespace SANS
{

// Base class instantiation

template class FieldBase< FieldLiftTraits<TopoD1, DLA::VectorS<2, DLA::VectorS<2, Real>> > >;


// FieldSubGroup instantiation

template class FieldSubGroup< PhysD2, FieldLiftTraits<TopoD1, DLA::VectorS<2, DLA::VectorS<2, Real>> > >;


// Field Lift instantiation

// 2-D shallow water equations
template class FieldLift_DG_Cell< PhysD2, TopoD1, DLA::VectorS<2, DLA::VectorS<2, Real>> >;

// IBL2D
template class FieldLift_DG_Cell< PhysD2, TopoD1, DLA::VectorS<2, DLA::VectorS<6, Real>> >;

// For Boltzmann Implementation
template class FieldLift_DG_Cell< PhysD2, TopoD1, DLA::VectorS<2, DLA::VectorS<9, Real>> >;
//template class FieldLift_DG_Cell< PhysD2, TopoD1, DLA::VectorS<2, DLA::VectorS<13, Real>> >;
//template class FieldLift_DG_Cell< PhysD2, TopoD1, DLA::VectorS<2, DLA::VectorS<16, Real>> >;
template class FieldLift_DG_Cell< PhysD2, TopoD1, DLA::VectorS<2, DLA::VectorS<NVar, Real>> >;

}
