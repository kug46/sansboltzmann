// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELDLINE_TRAITS_H
#define XFIELDLINE_TRAITS_H

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "Field/Element/ElementAssociativityLine.h"
#include "Field/Element/ElementXFieldLine.h"

#include "XFieldTypes.h"
#include "ElementConnectivity.h"
#include "FieldAssociativityConstructor.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Line grid field traits in a topologically 1D grid
//----------------------------------------------------------------------------//

template<class PhysDim>
struct XFieldGroupLineTraits<PhysDim, TopoD1>
{
  static const int D = PhysDim::D;                  // physical dimensions

  typedef DLA::VectorS<D,Real> VectorX;        // coordinates vector

  typedef Line TopologyType;

  typedef ElementAssociativity<TopoD1,Line> ElementAssociativityType;

  typedef VectorX T;                                  // DOF type
  typedef BasisFunctionLineBase BasisType;

  typedef FieldAssociativityBase<VectorX> FieldBase;
  typedef FieldAssociativityConstructor<ElementAssociativityType::Constructor> FieldAssociativityConstructorType;

  template<class ElemT = Real>
  struct ElementType : public ElementXField<PhysDim, TopoD1, Line>
  {
    typedef ElementXField<PhysDim, TopoD1, Line> BaseType;
    explicit ElementType( const BasisFunctionLineBase* basis ) : BaseType(basis) {}
    ElementType( int order, const BasisFunctionCategory& category ) : BaseType(order,category) {}
    ElementType( const ElementType& Elem ) : BaseType(Elem) {}

    ElementType& operator=( const ElementType& line ) { BaseType::operator=(line); return *this; }
  };
};


//----------------------------------------------------------------------------//
// Line grid field traits in a topologically 2D grid
//----------------------------------------------------------------------------//

template<class PhysDim>
struct XFieldGroupLineTraits<PhysDim, TopoD2>
{
  static const int D = PhysDim::D;                  // physical dimensions

  typedef DLA::VectorS<D,Real> VectorX;        // coordinates vector

  typedef Line TopologyType;

  typedef ElementAssociativity<TopoD1,Line> ElementAssociativityType;

  typedef VectorX T;                                  // DOF type
  typedef BasisFunctionLineBase BasisType;

  typedef ElementConnectivityBase<VectorX> FieldBase;
  typedef ElementConnectivityConstructor<ElementAssociativityType::Constructor> FieldAssociativityConstructorType;

  template<class ElemT = Real>
  struct ElementType :public ElementXField<PhysDim, TopoD1, Line>
  {
    typedef ElementXField<PhysDim, TopoD1, Line> BaseType;
    explicit ElementType( const BasisFunctionLineBase* basis ) : BaseType(basis) {}
    ElementType( int order, const BasisFunctionCategory& category ) : BaseType(order,category) {}
    ElementType( const ElementType& Elem ) : BaseType(Elem) {}

    ElementType& operator=( const ElementType& line ) { BaseType::operator=(line); return *this; }
  };
};

}

#endif  // XFIELDLINE_TRAITS_H
