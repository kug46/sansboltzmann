// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField.h"
#include <cstdarg>

namespace SANS
{

//=============================================================================
XFieldException::XFieldException( const std::string& message )
{
  errString += "Invalid grid.\n";
  errString += message;
}

//=============================================================================
XFieldException::XFieldException( const std::string& check, const std::string& message )
{
  errString += "Invalid grid.\n";
  errString += "Check \"" + check + "\" failed ";
  errString += message;
}

//=============================================================================
XFieldException::XFieldException(const std::string& check, const char *fmt, ...)
{
  char buffer[1024];
  va_list argp;

  va_start(argp, fmt);
  vsprintf(buffer, fmt, argp);
  va_end(argp);

  errString += "Invalid grid.\n";
  errString += "Check \"" + check + "\" failed ";
  errString += std::string( buffer );
}

//=============================================================================
XFieldException::~XFieldException() noexcept {}


}
