// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "Field_NodalView_impl.h"
#include "XFieldLine.h"
#include "FieldLine_CG_Cell.h"

#include <algorithm> //std::sort

namespace SANS
{

//=============================================================================
template <class FieldTraits>
void
Field_NodalView::init(const FieldBase<FieldTraits>& fld, const std::vector<int>& cellgroup_list)
{
  std::size_t nCellGroups = cellgroup_list.size();

  std::map<int, std::set<GroupElemIndex>> NodeDOF2CellMap;
  std::map<int, std::set<std::pair<int, int>>> NodeDOF2EdgeMap;

  //Create connectivity structure
  for (std::size_t group = 0; group < nCellGroups; group++ )
  {
    const int cellGroupGlobal = cellgroup_list[group];

    int nElem = fld.getCellGroupBase(cellGroupGlobal).nElem();

    if (fld.getCellGroupBase(cellGroupGlobal).topoTypeID() == typeid(Line))
    {
      typedef typename FieldBase<FieldTraits>:: template FieldCellGroupType<Line> FieldCellGroupType;
      const FieldCellGroupType& xfld_cellgrp = fld.template getCellGroup<Line>(cellGroupGlobal);

      int nodeDOFMap[Line::NNode];
      const int (*TraceNodes)[1] = TraceToCellRefCoord<Node, TopoD1, Line>::TraceNodes;

      for (int elem = 0; elem < nElem; elem++)
      {
        xfld_cellgrp.associativity(elem).getNodeGlobalMapping(nodeDOFMap, Line::NNode);

        this->insertCellNodes(nodeDOFMap, Line::NNode, cellGroupGlobal, elem, NodeDOF2CellMap);
        this->insertEdge(nodeDOFMap[TraceNodes[0][0]], nodeDOFMap[TraceNodes[1][0]], NodeDOF2EdgeMap);
      }
    }
    else
      SANS_DEVELOPER_EXCEPTION("Field_NodalView<PhysDim, TopoD1> - Unknown Topology type for CellGroup.");
  }

  std::sort(NodeDOFList_.begin(), NodeDOFList_.end());

  this->setMaps( NodeDOF2CellMap, NodeDOF2EdgeMap );
  this->updateInverseNodeMap(fld.nDOF());
}

//=============================================================================
//Explicitly instantiate
template Field_NodalView::Field_NodalView(const XField<PhysD1, TopoD1>& xfld, const std::vector<int>& cellgroup_list);

template Field_NodalView::Field_NodalView(const Field_CG_Cell<PhysD1, TopoD1, Real>& fld);
template Field_NodalView::Field_NodalView(const Field_CG_Cell<PhysD1, TopoD1, DLA::MatrixSymS<1,Real>>& fld);
template Field_NodalView::Field_NodalView(const Field_CG_Cell<PhysD1, TopoD1, DLA::MatrixSymS<1,Real>>& fld,
                                          const std::vector<int>& cellgroup_list);

}
