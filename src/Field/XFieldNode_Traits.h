// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELDNODE_TRAITS_H
#define XFIELDNODE_TRAITS_H

#include <ostream>
#include <string>
#include <memory>
#include <typeinfo>     // typeid

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "Topology/Dimension.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "Field/Element/ElementAssociativityNode.h"
#include "Field/Element/ElementXFieldNode.h"
#include "XFieldTypes.h"
#include "ElementConnectivity.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Node grid field traits for a topologically 1D mesh
//----------------------------------------------------------------------------//

template<class PhysDim>
struct XFieldGroupNodeTraits<PhysDim, TopoD1>
{
  static const int D = PhysDim::D;                  // physical dimensions

  typedef DLA::VectorS<D,Real> VectorX;        // coordinates vector

  typedef Node TopologyType;

  typedef ElementAssociativity<TopoD0,Node> ElementAssociativityType;

  typedef VectorX T;                                  // DOF type
  typedef BasisFunctionNodeBase BasisType;

  typedef ElementConnectivityBase<VectorX> FieldBase;
  typedef ElementConnectivityConstructor<ElementAssociativityType::Constructor> FieldAssociativityConstructorType;

  template<class ElemT = VectorX>
  struct ElementType : public ElementXField<PhysDim, TopoD0, Node>
  {
    typedef ElementXField<PhysDim, TopoD0, Node> BaseType;
    explicit ElementType( const BasisFunctionNodeBase* basis ) : BaseType(basis) {}
    ElementType( int order, const BasisFunctionCategory& category ) : BaseType(order,category) {}
    ElementType( const ElementType& Elem ) : BaseType(Elem) {}

    ElementType& operator=( const ElementType& line ) { BaseType::operator=(line); return *this; }
  };
};

}

#endif  // XFIELDNODE_TRAITS_H
