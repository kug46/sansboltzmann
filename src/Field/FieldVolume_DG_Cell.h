// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDVOLUME_DG_CELL_H
#define FIELDVOLUME_DG_CELL_H

#include "Field_DG_CellBase.h"
#include "FieldVolume.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// 3-D solution field: DG cell-field
//----------------------------------------------------------------------------//

template <class PhysDim, class T>
class Field_DG_Cell<PhysDim, TopoD3, T> : public Field_DG_CellBase< PhysDim, TopoD3, T >
{
public:
  typedef Field_DG_CellBase< PhysDim, TopoD3, T > BaseType;
  typedef T ArrayQ;

  Field_DG_Cell( const XField<PhysDim, TopoD3>& xfld, const int order, const BasisFunctionCategory& category );

  Field_DG_Cell( const XField<PhysDim, TopoD3>& xfld, const int order, const BasisFunctionCategory& category, const std::vector<int>& CellGroups );

  // Groups are first to remove ambiguity with list initializers
  // DG Fields are already broken, can just cat the CellGroupSets and forward
  explicit Field_DG_Cell( const std::vector<std::vector<int>>& CellGroupSets,
                          const XField<PhysDim, TopoD3>& xfld, const int order, const BasisFunctionCategory& category )
  : Field_DG_Cell( xfld, order, category, SANS::cat(CellGroupSets)) {}

  Field_DG_Cell( const Field_DG_Cell& fld, const FieldCopy& tag );

  Field_DG_Cell& operator=( const ArrayQ& q );

protected:
  void init( const int order, const BasisFunctionCategory& category, const std::vector<int>& CellGroups );

  using BaseType::nDOF_;
  using BaseType::DOF_;
  using BaseType::cellGroups_;
  using BaseType::globalCellGroups_;
  using BaseType::xfld_;
};

}

#endif  // FIELDVOLUME_DG_CELL_H
