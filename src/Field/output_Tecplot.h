// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef OUTPUT_TECPLOT_H
#define OUTPUT_TECPLOT_H

#include <ostream>
#include <string>

#include "tools/SANSnumerics.h"     // Real

#include "pde/NDConvert/FunctionNDConvert_fwd.h"

#include "Field.h"
#include "FieldLift.h"
#include "XField.h"

namespace SANS
{

template<class PhysDim, class TopoDim>
void
output_Tecplot( const XField<PhysDim,TopoDim>& xgrid, const std::string& filename );

template<class PhysDim, class T>
void
output_Tecplot( const Field< PhysDim, TopoD1, T >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names = std::vector<std::string>(),
                const bool partitioned = false );

template<class PhysDim, class T>
void
output_Tecplot( const Field< PhysDim, TopoD2, T >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names = std::vector<std::string>(),
                const bool partitioned = false );

template<class T>
void
output_Tecplot( const Field< PhysD3, TopoD3, T >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names = std::vector<std::string>(),
                const bool partitioned = false );

template<class T>
void
output_Tecplot( const Field< PhysD4, TopoD4, T >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names = std::vector<std::string>(),
                const bool partitioned = false );

//---------------------------------------------------------------------------//

template<class PhysDim, class T>
void
output_Tecplot( const FunctionNDConvertSpace<PhysDim, T>& sln,
                const Field< PhysDim, TopoD1, T >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names = std::vector<std::string>(),
                const bool partitioned = false );

template<class PhysDim, class T>
void
output_Tecplot( const FunctionNDConvertSpace<PhysDim, T>& sln,
                const Field< PhysDim, TopoD2, T >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names = std::vector<std::string>(),
                const bool partitioned = false );

template<class T>
void
output_Tecplot( const FunctionNDConvertSpace<PhysD3, T>& sln,
                const Field< PhysD3, TopoD3, T >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names = std::vector<std::string>(),
                const bool partitioned = false );

//---------------------------------------------------------------------------//
// specialization for Lifting Operator Field
template<class PhysDim, class T>
void
output_Tecplot_LO( const FieldLift<PhysDim, TopoD1, DLA::VectorS<PhysDim::D,T> >& rfld, const std::string& filename,
                   const std::vector<std::string>& state_names = std::vector<std::string>(),
                   const bool partitioned = false );

template<class PhysDim, class T>
void
output_Tecplot_LO( const FieldLift<PhysDim, TopoD2, DLA::VectorS<PhysDim::D,T> >& rfld, const std::string& filename,
                   const std::vector<std::string>& state_names = std::vector<std::string>(),
                   const bool partitioned = false );

template<class PhysDim, class T>
void
output_Tecplot_LO( const FieldLift<PhysDim, TopoD3, DLA::VectorS<PhysDim::D,T> >& rfld, const std::string& filename,
                   const std::vector<std::string>& state_names = std::vector<std::string>(),
                   const bool partitioned = false );

//---------------------------------------------------------------------------//
// output metric fields as ellipses
template<class PhysDim, class TopoDim>
void
output_Tecplot_Metric( const Field< PhysDim, TopoDim, DLA::MatrixSymS<PhysDim::D, Real> >& mfld, const std::string& filename );

//---------------------------------------------------------------------------//
template <class Tg>
class PDELinearizedIncompressiblePotential3D;

// Specialized dump for linearized incompressible potential
void
output_Tecplot_LIP( const PDELinearizedIncompressiblePotential3D<Real>& pde,
                    const Field< PhysD3, TopoD3, Real >& qfld,
                    const std::string& filename );

template<class Tg>
class PDEFullPotential3D;

// Specialized dump for full potential
void
output_Tecplot_FP( const PDEFullPotential3D<Real>& pde,
                   const Field< PhysD3, TopoD3, DLA::VectorS<2,Real> >& qfld,
                   const std::string& filename );

} //namespace SANS

#endif  // OUTPUT_TECPLOT_H
