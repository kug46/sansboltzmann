// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELD_NODALVIEW_H
#define FIELD_NODALVIEW_H

#include <set>
#include <vector>
#include <map>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "tools/GroupElemIndex.h"

#include "CellToTraceAssociativity.h"
#include "FieldTypes.h"

namespace SANS
{

template <class PhysDim, class TopoDim, class T>
class Field_CG_Cell;

class Field_NodalView
{
public:
  typedef std::vector<GroupElemIndex> IndexVector;

  template <class PhysDim, class TopoDim>
  Field_NodalView(const XField<PhysDim, TopoDim>& xfld, const std::vector<int>& cellgroup_list);

  template <class PhysDim, class TopoDim, class T> // cppcheck-suppress noExplicitConstructor
  Field_NodalView( const Field_CG_Cell<PhysDim, TopoDim, T>& fld );

  template <class PhysDim, class TopoDim, class T>
  Field_NodalView( const Field_CG_Cell<PhysDim, TopoDim, T>& fld, const std::vector<int>& cellgroup_list );

  ~Field_NodalView();

  Field_NodalView(const Field_NodalView&) = delete;
  Field_NodalView& operator=(const Field_NodalView&) = delete;

  //List of node DOFs in xfld, in encountered order
  const std::vector<int>& getNodeDOFList()        const { return NodeDOFList_; } //returns the nodeDOF list
  const std::vector<int>& getInverseNodeDOFList() const { return invNodeDOFList_; } //returns the inverse mapping of the nodeDOF list

  const IndexVector getCellList(int nodeDOF) const; //NodeDOF to Cell
  const std::vector<std::pair<int, int>> getEdgeList(int nodeDOF) const; //NodeDOF to Edge
  const std::vector<int> getNodeList( GroupElemIndex cell ) const;

  // nodeDOFs = {0} -> #of cells attached to 0
  // nodeDOFs = {0,1} -> # cells attached to edge 0-1
  // nodeDOFs = {0,1,2} -> # cells attached to triangle 0-1-2
  // nodeDOFs = {0,1,2,3} -> # cells attached to quad 0-1-2-3 OR # cells attached to tet 0-1-2-3
  int getTopologicalShare( const std::vector<int>& nodeDOFs ) const; // returns number of cells that share object defined by nodeDOFs

  std::set<std::pair<int,int>> getEdgeSet() const
  {
    std::set<std::pair<int,int>> edgeList;
    for (auto it = NodeDOF2EdgeMap_.begin(); it != NodeDOF2EdgeMap_.end(); ++it) // loop over nodes edge list
      for (auto itt = it->second.begin(); itt != it->second.end(); ++itt) // loop over the list of edges
        edgeList.insert(*itt); // insert into the set

    return edgeList;
  }
protected:
  template <class FieldTraits>
  void init(const FieldBase<FieldTraits>& fld, const std::vector<int>& cellgroup_list);

  void insertCellNodes( int nodeDOFMap[], int nnode, int cell_group, int cell_elem,
                        std::map<int, std::set<GroupElemIndex>>& NodeDOF2CellMap );
  void insertEdge( int nodeDOF0, int nodeDOF1, std::map<int, std::set<std::pair<int, int>>>& NodeDOF2EdgeMap );
  void setMaps( std::map<int, std::set<GroupElemIndex>>& NodeDOF2CellMap,
                std::map<int, std::set<std::pair<int, int>>>& NodeDOF2EdgeMap);
  void updateInverseNodeMap(const int nDOF);

  //contains the list of global nodeDOF indices in the fld
  std::vector<int> NodeDOFList_;

  //contains the mapping from a global nodeDOF index to its index in NodeDOFList_ (i.e. the inverse mapping of NodeDOFList_)
  std::vector<int> invNodeDOFList_;

  //contains the mapping from a given nodeDOF index to a set of edges (edge key = 2 nodeDOFs of edge sorted in ascending order)
  std::map<int, std::vector<std::pair<int, int>>> NodeDOF2EdgeMap_;

  //contains the mapping from a given nodeDOF index to a set of (cellgroup,cellelem) pairs
  std::map<int, IndexVector> NodeDOF2CellMap_;

  // contains the mapping from cell to node dofs - this bypasses the cellgroup interface. Useful in local grid construction
  std::map<GroupElemIndex, std::vector<int> > Cell2NodeDOFMap_;

};

}


#endif  // FIELD_NODALVIEW_H
