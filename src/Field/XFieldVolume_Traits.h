// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELDVOLUME_TRAITS_H
#define XFIELDVOLUME_TRAITS_H

#include "tools/SANSnumerics.h"     // Real

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "Field/Element/ElementAssociativityVolume.h"
#include "Field/Element/ElementXFieldVolume.h"

#include "XFieldTypes.h"
#include "ElementConnectivity.h"
#include "FieldAssociativityConstructor.h"


namespace SANS
{
//----------------------------------------------------------------------------//
// Volume grid field variable in a topologically 3D grid
//----------------------------------------------------------------------------//

template<class PhysDim>
struct XFieldVolumeTraits<PhysDim, TopoD3>
{
  static const int D = PhysDim::D;                       // physical dimensions

  typedef DLA::VectorS<D,Real> VectorX;        // coordinates vector

  typedef FieldAssociativityBase< VectorX > FieldBase;

  typedef VectorX T;                                  // DOF type
};

template<class PhysDim, class Topology>
struct XFieldGroupVolumeTraits<PhysDim, TopoD3, Topology> : public XFieldVolumeTraits<PhysDim, TopoD3>
{
  typedef typename XFieldVolumeTraits<PhysDim, TopoD3>::VectorX VectorX;        // coordinates vector

  typedef Topology TopologyType;

  typedef ElementAssociativity<TopoD3,Topology> ElementAssociativityType;

  typedef BasisFunctionVolumeBase<Topology> BasisType;

  typedef FieldAssociativityConstructor< typename ElementAssociativityType::Constructor > FieldAssociativityConstructorType;

  template<class ElemT>
  struct ElementType : public ElementXField<PhysDim, TopoD3, Topology>
  {
    typedef ElementXField<PhysDim, TopoD3, Topology> BaseType;

    ElementType( const ElementType& elem ) : BaseType(elem) {}
    explicit ElementType( const BasisType* basis ) : BaseType(basis) {}
    ElementType( int order, const BasisFunctionCategory& category ) : BaseType(order,category) {}
    ElementType& operator=( const ElementType& elem ) { BaseType::operator=(elem); return *this; }
  };
};

//----------------------------------------------------------------------------//
// Volume grid field variable in a topologically 4D grid
//----------------------------------------------------------------------------//

template<class PhysDim>
struct XFieldVolumeTraits<PhysDim, TopoD4>
{
  static const int D = PhysDim::D;                       // physical dimensions

  typedef DLA::VectorS<D,Real> VectorX;        // coordinates vector

  typedef ElementConnectivityBase< VectorX > FieldBase;

  typedef VectorX T;                                  // DOF type
};

template<class PhysDim, class Topology>
struct XFieldGroupVolumeTraits<PhysDim, TopoD4, Topology> : public XFieldVolumeTraits<PhysDim, TopoD4>
{
  typedef typename XFieldVolumeTraits<PhysDim, TopoD4>::VectorX VectorX;        // coordinates vector

  typedef Topology TopologyType;

  typedef ElementAssociativity<TopoD3,Topology> ElementAssociativityType;

  typedef BasisFunctionVolumeBase<Topology> BasisType;

  typedef ElementConnectivityConstructor< typename ElementAssociativityType::Constructor > FieldAssociativityConstructorType;

  template<class ElemT = Real>
  struct ElementType : public ElementXField<PhysDim, TopoD3, Topology>
  {
    typedef ElementXField<PhysDim, TopoD3, Topology> BaseType;

    ElementType( const ElementType& elem ) : BaseType(elem) {}
    explicit ElementType( const BasisType* basis ) : BaseType(basis) {}
    ElementType( int order, const BasisFunctionCategory& category ) : BaseType(order,category) {}
    ElementType& operator=( const ElementType& elem ) { BaseType::operator=(elem); return *this; }
  };
};

}

#endif  // XFIELDVOLUME_TRAITS_H
