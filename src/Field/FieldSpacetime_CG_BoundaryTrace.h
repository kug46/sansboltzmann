// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDSPACETIME_CG_BOUNDARYTRACE_H
#define FIELDSPACETIME_CG_BOUNDARYTRACE_H

#include <vector>

#include "FieldSpacetime.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// solution field: CG boundary-faces (e.g. Lagrange multiplier)
//
// DOFs for all boundary regions determined together
//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
class Field_CG_BoundaryTrace;

template <class PhysDim, class T>
class Field_CG_BoundaryTrace<PhysDim, TopoD4, T> : public Field< PhysDim, TopoD4, T >
{
public:
  typedef Field< PhysDim, TopoD4, T > BaseType;
  typedef T ArrayQ;

  Field_CG_BoundaryTrace( const XField<PhysDim, TopoD4>& xfld, const int order,
                          const BasisFunctionCategory category );

  Field_CG_BoundaryTrace( const XField<PhysDim, TopoD4>& xfld, const int order,
                          const BasisFunctionCategory category, const std::vector<int>& BoundaryGroups );

  explicit Field_CG_BoundaryTrace( const std::vector<std::vector<int>>& BoundaryGroupSets,
                                   const XField<PhysDim, TopoD4>& xfld, const int order,
                                   const BasisFunctionCategory category );

  Field_CG_BoundaryTrace( const Field_CG_BoundaryTrace& fld, const FieldCopy&tag );

  Field_CG_BoundaryTrace& operator=( const ArrayQ& q );

  // Virtual class to denote the SpaceType of the Field
  virtual SpaceType spaceType() const override { return SpaceType::Continuous; }

protected:
  using BaseType::nDOF_;
  using BaseType::DOF_;
  using BaseType::nElem_;
  using BaseType::xfld_;
  using BaseType::boundaryTraceGroups_;
  using BaseType::localBoundaryTraceGroups_;

  void init( const int order, const BasisFunctionCategory& category,
             const std::vector<std::vector<int>>& boundaryGroupSets );
};

}

#endif  // FIELDSPACETIME_CG_BOUNDARYTRACE_H
