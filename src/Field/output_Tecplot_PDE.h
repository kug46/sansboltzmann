// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef OUTPUT_TECPLOT_PDE_H
#define OUTPUT_TECPLOT_PDE_H

#include <ostream>
#include <string>

#include "tools/SANSnumerics.h"     // Real
#include "tools/Tuple.h"

#define OUTPUT_TECPLOT_FIELD_INSTANTIATE // TODO: This is very bad
#include "output_Tecplot/output_Tecplot_Field_impl.h"

#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"

#include "Field/Element/ReferenceElementMesh.h"
#include "Field/Element/Element.h"

#include "Field/Tuple/ParamTuple.h"
#include "pde/NDConvert/PDENDConvert_fwd.h"

#define MPI_COMMUNICATOR_IN_CPP //TODO: Hacky..
#include "MPI/communicator.h"
#include "MPI/continuousElementMap.h"

#include "Field.h"
#include "XField.h"

namespace SANS
{

template<class PDE, class ParamT, class T, class PhysDim>
void
evalDerivedQuantity(const PDE& pde, const ParamT& param,
                    const T& q, const DLA::VectorS<PhysDim::D, T>& gradq, Real& J);

//Partial specializations for spatial PDEs
template<class PDE, class L, class T>
void
evalDerivedQuantity(const int& index, const PDENDConvertSpace<PhysD1, PDE>& pde,
                    const ParamTuple<L, DLA::VectorS<PhysD1::D,Real>, TupleClass<>>& param,
                    const T& q, const DLA::VectorS<PhysD1::D, T>& gradq, Real& J)
{
  const Real dummyTime = 0;
  const DLA::VectorS<PhysD1::D,Real>& X = param.right();
  pde.derivedQuantity(index, param.left(), X[0], dummyTime, q, gradq[0], J);
}

template<class PDE, class L, class T>
void
evalDerivedQuantity(const int& index, const PDENDConvertSpace<PhysD2, PDE>& pde,
                    const ParamTuple<L, DLA::VectorS<PhysD2::D,Real>, TupleClass<>>& param,
                    const T& q, const DLA::VectorS<PhysD2::D, T>& gradq, Real& J)
{
  const Real dummyTime = 0;
  const DLA::VectorS<PhysD2::D,Real>& X = param.right();
  pde.derivedQuantity(index, param.left(), X[0], X[1], dummyTime, q, gradq[0], gradq[1], J);
}

template<class PDE, class T>
void
evalDerivedQuantity(const int& index, const PDENDConvertSpace<PhysD2, PDE>& pde,
                    const DLA::VectorS<PhysD2::D,Real>& X,
                    const T& q, const DLA::VectorS<PhysD2::D, T>& gradq, Real& J)
{
  const Real dummyTime = 0;
  pde.derivedQuantity(index, X[0], X[1], dummyTime, q, gradq[0], gradq[1], J);
}

//Partial specializations for space-time PDEs
template<class PDE, class L, class T>
void
evalDerivedQuantity(const int& index, const PDENDConvertSpaceTime<PhysD1, PDE>& pde,
                    const ParamTuple<L, DLA::VectorS<PhysD2::D,Real>, TupleClass<>>& param,
                    const T& q, const DLA::VectorS<PhysD2::D, T>& gradq, Real& J)
{
  const DLA::VectorS<PhysD2::D,Real>& X = param.right();
  pde.derivedQuantity(index, param.left(), X[0], X[1], q, gradq[0], J);
}

template<class PDE, class L, class T>
void
evalDerivedQuantity(const int& index, const PDENDConvertSpaceTime<PhysD2, PDE>& pde,
                    const ParamTuple<L, DLA::VectorS<PhysD3::D,Real>, TupleClass<>>& param,
                    const T& q, const DLA::VectorS<PhysD3::D, T>& gradq, Real& J)
{
  const DLA::VectorS<PhysD3::D,Real>& X = param.right();
  pde.derivedQuantity(index, param.left(), X[0], X[1], X[2], q, gradq[0], gradq[1], J);
}

//----------------------------------------------------------------------------//
template<class PDE, class T, class PhysDim, class TopoDim, class Topology, class ParamFieldType>
void
output_Tecplot_CellGroup_CG( const PDE& pde, mpi::communicator& comm, const std::vector<int>& cellIDs, const bool partitioned,
                          const typename ParamFieldType::template FieldCellGroupType<Topology>& paramfldGroup,
                          const typename  Field<PhysDim, TopoDim, T>::template FieldCellGroupType<Topology>& qfldGroup,
                          FILE* fp )
{
  typedef DLA::VectorS<PhysDim::D,T> VectorArrayQ;
  typedef typename ParamFieldType::template FieldCellGroupType<Topology> ParamFieldCellGroupType;
  typedef typename Field<PhysDim, TopoDim, T>::template FieldCellGroupType<Topology> QFieldCellGroupType;

  typedef typename ParamFieldCellGroupType::template ElementType<> ElementParamFieldClass;
  typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldClass;
  typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

  // element field variables
  ElementParamFieldClass paramfldElem( paramfldGroup.basis() );
  ElementQFieldClass qfldElem( qfldGroup.basis() );

  static const int N = DLA::VectorSize<T>::M;

  const int order = 5;//max(get<-1>(paramfldElem).order(), qfldElem.order());
  int nRefine = max(order-1,0);
  ReferenceElementMesh<Topology> refMesh(nRefine);
  const int nRefnode = refMesh.nodes.size();
  const int nRefelem = refMesh.elems.size();

  int nElem_global;
  std::map<int,int> globalElemMap;
  continuousElementMap( comm, cellIDs, get<-1>(paramfldGroup), nElem_global, globalElemMap);

  int nElemLocal = paramfldGroup.nElem();

  // tecplot does not like empty groups
  if ( nElem_global == 0 ) return;
  if ( nElemLocal == 0 && partitioned ) return;

  if (partitioned) nElem_global = nElemLocal;

  int comm_rank = comm.rank();

  if (comm_rank == 0 || partitioned)
    fprintf( fp, "ZONE T=\"grid\", N=%d, E=%d, F=FEPOINT, ET=%s\n",
             nRefnode*nElem_global, nRefelem*nElem_global, TecTopo<Topology>::name() );

  // grid coordinates and solution

  typename ElementQFieldClass::RefCoordType sRefCell;
  typedef typename ElementParamFieldClass::T ParamT;
  typedef typename ElementXFieldClass::VectorX VectorX;
  ParamT param = 0;
  T q = 0;
  VectorArrayQ gradq = 0;

  const int D = PhysDim::D;
  const int nDerivedQty = pde.derivedQuantityNames().size();

#ifdef SANS_MPI
  const int nData = D + N + N*D + nDerivedQty;

  if (!partitioned)
  {
    int comm_size = comm.size();

    // maximum element chunk size that rank 0 will write at any given time
    int Elemchunk = nElem_global / comm_size + nElem_global % comm_size;

    // send one chunk of elements at a time to rank 0
    for (int rank = 0; rank < comm_size; rank++)
    {
      int elemLow  =  rank   *Elemchunk;
      int elemHigh = (rank+1)*Elemchunk;

      std::map<int,std::vector<std::vector<double>>> buffer;

      // loop over elements within group
      for (int elem = 0; elem < nElemLocal; elem++)
      {
        int elemID = globalElemMap[cellIDs[elem]];

        if (elemID >= elemLow && elemID < elemHigh &&
            get<-1>(paramfldGroup).associativity(elem).rank() == comm_rank)
        {
          // copy global grid/solution DOFs to element
          paramfldGroup.getElement( paramfldElem, elem );
          ElementXFieldClass xfldElem(get<-1>(paramfldElem));

          qfldGroup.getElement( qfldElem, elem );


          std::vector<std::vector<double>> nodeValues(nRefnode, std::vector<double>(nData));

          for ( int i = 0; i < nRefnode; i++ )
          {
            sRefCell = refMesh.nodes[i];

            paramfldElem.eval( sRefCell, param );
            const VectorX& X = get<-1>(param);
            qfldElem.eval( sRefCell, q );
            xfldElem.evalGradient( sRefCell, qfldElem, gradq );

            int n = 0;
            for (int k = 0; k < D; k++)
              nodeValues[i][n++] = X[k];

            for (int k = 0; k < N; k++)
              nodeValues[i][n++] = DLA::index(q,k);

            for (int d = 0; d < D; d++)
              for (int k = 0; k < N; k++)
                nodeValues[i][n++] = DLA::index(gradq[d],k);

            for (int k = 0; k < nDerivedQty; k++)
            {
              Real J = 0;
              evalDerivedQuantity(k, pde, param, q, gradq, J);
              nodeValues[i][n++] = J;
            }
          }

          buffer[elemID] = std::move(nodeValues);
        }
      }


      if (comm_rank == 0)
      {
        std::vector<std::map<int,std::vector<std::vector<double>>>> bufferOnRank;
        boost::mpi::gather(comm, buffer, bufferOnRank, 0 );

        // collapse down the buffer from all other ranks
        for (std::size_t i = 1; i < bufferOnRank.size(); i++)
          buffer.insert(bufferOnRank[i].begin(), bufferOnRank[i].end());

        //Write the node data
        for ( const auto& nodesPair : buffer)
          for ( int i = 0; i < nRefnode; i++ )
          {
            int n = 0;

            //coordinates
            for (int d = 0; d < D; d++)
              fprintf( fp, "%22.15e ", nodesPair.second[i][n++] );

            //states
            for (int k = 0; k < N; k++)
              fprintf( fp, "%22.15e ", nodesPair.second[i][n++] );

            //state gradients
            for (int d = 0; d < D; d++)
              for (int k = 0; k < N; k++)
                fprintf( fp, "%22.15e ", nodesPair.second[i][n++] );

            //derived quantities
            for (int k = 0; k < nDerivedQty; k++)
              fprintf( fp, "%22.15e ", nodesPair.second[i][n++] );

            fprintf( fp, "\n" );
          }
      }
      else // send the buffer to rank 0
        boost::mpi::gather(comm, buffer, 0 );

    } // for rank

  }
  else // if partitioned
  {
#endif

    // loop over elements within group
    for (int elem = 0; elem < nElemLocal; elem++)
    {
      // copy global grid/solution DOFs to element
      paramfldGroup.getElement( paramfldElem, elem );
      ElementXFieldClass xfldElem(get<-1>(paramfldElem));

      qfldGroup.getElement( qfldElem, elem );

      for ( int i = 0; i < nRefnode; i++ )
      {
        sRefCell = refMesh.nodes[i];

        paramfldElem.eval( sRefCell, param );
        const VectorX& X = get<-1>(param);
        qfldElem.eval( sRefCell, q );
        xfldElem.evalGradient( sRefCell, qfldElem, gradq );

        //coordinates
        for (int d = 0; d < D; d++)
          fprintf( fp, "%22.15e ", X[d] );

        //states
        for (int k = 0; k < N; k++)
          fprintf( fp, "%22.15e ", DLA::index(q,k) );

        //state gradients
        for (int d = 0; d < D; d++)
          for (int k = 0; k < N; k++)
            fprintf( fp, "%22.15e ", DLA::index(gradq[d],k) );

        //derived quantities
        for (int k = 0; k < nDerivedQty; k++)
        {
          Real J = 0;
          evalDerivedQuantity(k, pde, param, q, gradq, J);
          fprintf( fp, "%22.15e ", J );
        }

        fprintf( fp, "\n" );
      }
    }

#ifdef SANS_MPI
  } // if !partitioned
#endif

  // cell-to-node connectivity

  if (comm_rank == 0 || partitioned)
    for (int elem = 0; elem < nElem_global; elem++)
    {
      int offset = nRefnode*elem;
      for (int relem = 0; relem < nRefelem; relem++)
      {
        for (int n = 0; n < Topology::NNode; n++ )
          fprintf( fp, "%d ", offset + refMesh.elems[relem][n] + 1 ); // +1 for one based indexing
        fprintf( fp, "\n" );
      }
    }
}

//----------------------------------------------------------------------------//
template<class PDE, class T, class PhysDim, class TopoDim, class Topology, class ParamFieldType>
void
output_Tecplot_CellGroup( const PDE& pde, mpi::communicator& comm, const std::vector<int>& cellIDs, const bool partitioned,
                          const typename ParamFieldType::template FieldCellGroupType<Topology>& paramfldGroup,
                          const typename  Field<PhysDim, TopoDim, T>::template FieldCellGroupType<Topology>& qfldGroup,
                          const typename FieldLift< PhysDim, TopoDim, DLA::VectorS<PhysDim::D,T> >::template FieldCellGroupType<Topology>& rfldGroup,
                          FILE* fp )
{
  typedef DLA::VectorS<PhysDim::D,T> VectorArrayQ;
  typedef typename ParamFieldType::template FieldCellGroupType<Topology> ParamFieldCellGroupType;
  typedef typename Field<PhysDim, TopoDim, T>::template FieldCellGroupType<Topology> QFieldCellGroupType;
//  typedef typename FieldLift< PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<Topology> RFieldCellGroupType;

  typedef typename ParamFieldCellGroupType::template ElementType<> ElementParamFieldClass;
  typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldClass;
  typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;
  typedef ElementLift<VectorArrayQ, TopoDim, Topology> ElementrFieldClass;
  typedef Element<VectorArrayQ, TopoDim, Topology> ElementRFieldClass;

  // element field variables
  ElementParamFieldClass paramfldElem( paramfldGroup.basis() );
  ElementQFieldClass qfldElem( qfldGroup.basis() );
  ElementrFieldClass rfldElems(rfldGroup.basis() );
  ElementRFieldClass RfldElem( rfldGroup.basis() );

  static const int N = DLA::VectorSize<T>::M;

  const int order = max(get<-1>(paramfldElem).order(), qfldElem.order());
  int nRefine = max(order-1,0);
  ReferenceElementMesh<Topology> refMesh(nRefine);
  const int nRefnode = refMesh.nodes.size();
  const int nRefelem = refMesh.elems.size();

  int nElem_global;
  std::map<int,int> globalElemMap;
  continuousElementMap( comm, cellIDs, get<-1>(paramfldGroup), nElem_global, globalElemMap);

  int nElemLocal = paramfldGroup.nElem();

  // tecplot does not like empty groups
  if ( nElem_global == 0 ) return;
  if ( nElemLocal == 0 && partitioned ) return;

  if (partitioned) nElem_global = nElemLocal;

  int comm_rank = comm.rank();

  if (comm_rank == 0 || partitioned)
    fprintf( fp, "ZONE T=\"grid\", N=%d, E=%d, F=FEPOINT, ET=%s\n",
             nRefnode*nElem_global, nRefelem*nElem_global, TecTopo<Topology>::name() );

  // grid coordinates and solution

  typename ElementQFieldClass::RefCoordType sRefCell;
  typedef typename ElementParamFieldClass::T ParamT;
  typedef typename ElementXFieldClass::VectorX VectorX;
  ParamT param = 0;
  T q = 0;
  VectorArrayQ gradq = 0, Rlift = 0, gradqlifted = 0;

  const int D = PhysDim::D;
  const int nDerivedQty = pde.derivedQuantityNames().size();

#ifdef SANS_MPI
  const int nData = D + N + N*D + nDerivedQty;

  if (!partitioned)
  {
    int comm_size = comm.size();

    // maximum element chunk size that rank 0 will write at any given time
    int Elemchunk = nElem_global / comm_size + nElem_global % comm_size;

    // send one chunk of elements at a time to rank 0
    for (int rank = 0; rank < comm_size; rank++)
    {
      int elemLow  =  rank   *Elemchunk;
      int elemHigh = (rank+1)*Elemchunk;

      std::map<int,std::vector<std::vector<double>>> buffer;

      // loop over elements within group
      for (int elem = 0; elem < nElemLocal; elem++)
      {
        int elemID = globalElemMap[cellIDs[elem]];

        if (elemID >= elemLow && elemID < elemHigh &&
            get<-1>(paramfldGroup).associativity(elem).rank() == comm_rank)
        {
          // copy global grid/solution DOFs to element
          paramfldGroup.getElement( paramfldElem, elem );
          ElementXFieldClass xfldElem(get<-1>(paramfldElem));

          qfldGroup.getElement( qfldElem, elem );
          rfldGroup.getElement( rfldElems, elem );

          // accumulate the lifting operator DOFs into a single element
          RfldElem.vectorViewDOF() = 0;
          for (int trace = 0; trace < Topology::NTrace; trace++)
            RfldElem.vectorViewDOF() += rfldElems[trace].vectorViewDOF();

          std::vector<std::vector<double>> nodeValues(nRefnode, std::vector<double>(nData));

          for ( int i = 0; i < nRefnode; i++ )
          {
            sRefCell = refMesh.nodes[i];

            paramfldElem.eval( sRefCell, param );
            const VectorX& X = get<-1>(param);
            qfldElem.eval( sRefCell, q );
            xfldElem.evalGradient( sRefCell, qfldElem, gradq );
            RfldElem.eval( sRefCell, Rlift );

            gradqlifted = gradq + Rlift;

            int n = 0;
            for (int k = 0; k < D; k++)
              nodeValues[i][n++] = X[k];

            for (int k = 0; k < N; k++)
              nodeValues[i][n++] = DLA::index(q,k);

            for (int d = 0; d < D; d++)
              for (int k = 0; k < N; k++)
                nodeValues[i][n++] = DLA::index(gradqlifted[d],k);

            for (int k = 0; k < nDerivedQty; k++)
            {
              Real J = 0;
              evalDerivedQuantity(k, pde, param, q, gradqlifted, J);
              nodeValues[i][n++] = J;
            }
          }

          buffer[elemID] = std::move(nodeValues);
        }
      }


      if (comm_rank == 0)
      {
        std::vector<std::map<int,std::vector<std::vector<double>>>> bufferOnRank;
        boost::mpi::gather(comm, buffer, bufferOnRank, 0 );

        // collapse down the buffer from all other ranks
        for (std::size_t i = 1; i < bufferOnRank.size(); i++)
          buffer.insert(bufferOnRank[i].begin(), bufferOnRank[i].end());

        //Write the node data
        for ( const auto& nodesPair : buffer)
          for ( int i = 0; i < nRefnode; i++ )
          {
            int n = 0;

            //coordinates
            for (int d = 0; d < D; d++)
              fprintf( fp, "%22.15e ", nodesPair.second[i][n++] );

            //states
            for (int k = 0; k < N; k++)
              fprintf( fp, "%22.15e ", nodesPair.second[i][n++] );

            //state gradients
            for (int d = 0; d < D; d++)
              for (int k = 0; k < N; k++)
                fprintf( fp, "%22.15e ", nodesPair.second[i][n++] );

            //derived quantities
            for (int k = 0; k < nDerivedQty; k++)
              fprintf( fp, "%22.15e ", nodesPair.second[i][n++] );

            fprintf( fp, "\n" );
          }
      }
      else // send the buffer to rank 0
        boost::mpi::gather(comm, buffer, 0 );

    } // for rank

  }
  else // if partitioned
  {
#endif

    // loop over elements within group
    for (int elem = 0; elem < nElemLocal; elem++)
    {
      // copy global grid/solution DOFs to element
      paramfldGroup.getElement( paramfldElem, elem );
      ElementXFieldClass xfldElem(get<-1>(paramfldElem));

      qfldGroup.getElement( qfldElem, elem );
      rfldGroup.getElement( rfldElems, elem );

      // accumulate the lifting operator DOFs into a single element
      RfldElem.vectorViewDOF() = 0;
      for (int trace = 0; trace < Topology::NTrace; trace++)
        RfldElem.vectorViewDOF() += rfldElems[trace].vectorViewDOF();

      for ( int i = 0; i < nRefnode; i++ )
      {
        sRefCell = refMesh.nodes[i];

        paramfldElem.eval( sRefCell, param );
        const VectorX& X = get<-1>(param);
        qfldElem.eval( sRefCell, q );
        xfldElem.evalGradient( sRefCell, qfldElem, gradq );
        RfldElem.eval( sRefCell, Rlift );

        gradqlifted = gradq + Rlift;

        //coordinates
        for (int d = 0; d < D; d++)
          fprintf( fp, "%22.15e ", X[d] );

        //states
        for (int k = 0; k < N; k++)
          fprintf( fp, "%22.15e ", DLA::index(q,k) );

        //state gradients
        for (int d = 0; d < D; d++)
          for (int k = 0; k < N; k++)
            fprintf( fp, "%22.15e ", DLA::index(gradqlifted[d],k) );

        //derived quantities
        for (int k = 0; k < nDerivedQty; k++)
        {
          Real J = 0;
          evalDerivedQuantity(k, pde, param, q, gradqlifted, J);
          fprintf( fp, "%22.15e ", J );
        }

        fprintf( fp, "\n" );
      }
    }

#ifdef SANS_MPI
  } // if !partitioned
#endif

  // cell-to-node connectivity

  if (comm_rank == 0 || partitioned)
    for (int elem = 0; elem < nElem_global; elem++)
    {
      int offset = nRefnode*elem;
      for (int relem = 0; relem < nRefelem; relem++)
      {
        for (int n = 0; n < Topology::NNode; n++ )
          fprintf( fp, "%d ", offset + refMesh.elems[relem][n] + 1 ); // +1 for one based indexing
        fprintf( fp, "\n" );
      }
    }
}

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
// CG Specialization
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//TopoD2 specialization
template<class PDE, class PhysDim, class T, class ParamFieldType>
void
output_Tecplot( const PDE& pde, const ParamFieldType& paramfld, const Field< PhysDim, TopoD2, T >& qfld,
                const std::string& filename,
                const std::vector<std::string>& state_names = std::vector<std::string>(),
                const bool partitioned = false )
{
  SANS_ASSERT( &paramfld.getXField() == &qfld.getXField() );

  FILE* fp = nullptr;

  if (qfld.comm()->rank() == 0 || partitioned)
  {
    std::cout << "output_Tecplot: filename = " << filename << std::endl;
    fp = fopen( filename.c_str(), "w" );
    if (fp == NULL)
      SANS_DEVELOPER_EXCEPTION("output_Tecplot<PhysDim,TopoD2> - Error opening file: %s", filename.c_str());

    fprintf( fp, "\"\"\n" );
    fprintf( fp, "VARIABLES = \"X\"" );

    const char XYZ[] = "XYZ";
    const char xyz[] = "xyz";

    for (int d = 1; d < PhysDim::D; d++)
      fprintf( fp, ", \"%c\"", XYZ[d] );

    static const int N = DLA::VectorSize<T>::M;
    if (state_names.empty())
    {
      for (int k = 0; k < N; k++)
        fprintf( fp, ", \"q%d\"", k+1 );

      for (int d = 0; d < PhysDim::D; d++)
        for (int k = 0; k < N; k++)
          fprintf( fp, ", \"q%d_%c\"", k+1, xyz[d] );
    }
    else
    {
      SANS_ASSERT((int) state_names.size() == N);
      for (int k = 0; k < N; k++)
        fprintf( fp, ", \"%s\"", state_names[k].c_str());

      for (int d = 0; d < PhysDim::D; d++)
        for (int k = 0; k < N; k++)
          fprintf( fp, ", \"%s_%c\"", state_names[k].c_str(), xyz[d] );
    }

    std::vector<std::string> derivedQtyNames = pde.derivedQuantityNames();
    for (int k = 0; k < (int) derivedQtyNames.size(); k++)
      fprintf( fp, ", \"%s\"", derivedQtyNames[k].c_str() );

    fprintf( fp, "\n" );
  }

  const XField<PhysDim,TopoD2>& xgrid = qfld.getXField();

  const int ngroup = qfld.nCellGroups();

  // loop over cell groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      output_Tecplot_CellGroup_CG<PDE, T, PhysDim, TopoD2, Triangle, ParamFieldType>(
          pde, *xgrid.comm(), xgrid.cellIDs(group), partitioned,
          paramfld.template getCellGroup<Triangle>(group),
          qfld.template getCellGroup<Triangle>(group), fp );
    }
    else if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Quad) )
    {
      output_Tecplot_CellGroup_CG<PDE, T, PhysDim, TopoD2, Quad, ParamFieldType>(
          pde, *xgrid.comm(), xgrid.cellIDs(group), partitioned,
          paramfld.template getCellGroup<Quad>(group),
          qfld.template getCellGroup<Quad>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(PDE): Unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  if (qfld.comm()->rank() == 0 || partitioned)
    fclose( fp );
}

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
// DGBR2 Specialization
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------


//--------------------------------------------------------------------------------
//TopoD1 specialization
template<class PDE, class PhysDim, class T, class ParamFieldType>
void
output_Tecplot( const PDE& pde, const ParamFieldType& paramfld, const Field< PhysDim, TopoD1, T >& qfld,
                const FieldLift<PhysDim, TopoD1, DLA::VectorS<PhysDim::D, T> >& rfld,
                const std::string& filename,
                const std::vector<std::string>& state_names = std::vector<std::string>(),
                const bool partitioned = false )
{
  SANS_ASSERT( &paramfld.getXField() == &qfld.getXField() );
  SANS_ASSERT( &paramfld.getXField() == &rfld.getXField() );

  FILE* fp = nullptr;

  if (qfld.comm()->rank() == 0 || partitioned)
  {
    std::cout << "output_Tecplot: filename = " << filename << std::endl;
    fp = fopen( filename.c_str(), "w" );
    if (fp == NULL)
      SANS_DEVELOPER_EXCEPTION("output_Tecplot<PhysDim,TopoD1> - Error opening file: %s", filename.c_str());

    fprintf( fp, "\"\"\n" );
    fprintf( fp, "VARIABLES = \"X\"" );

    const char XYZ[] = "XYZ";
    const char xyz[] = "xyz";

    for (int d = 1; d < PhysDim::D; d++)
      fprintf( fp, ", \"%c\"", XYZ[d] );

    static const int N = DLA::VectorSize<T>::M;
    if (state_names.empty())
    {
      for (int k = 0; k < N; k++)
        fprintf( fp, ", \"q%d\"", k+1 );

      for (int d = 0; d < PhysDim::D; d++)
        for (int k = 0; k < N; k++)
          fprintf( fp, ", \"q%d_%c\"", k+1, xyz[d] );
    }
    else
    {
      SANS_ASSERT((int) state_names.size() == N);
      for (int k = 0; k < N; k++)
        fprintf( fp, ", \"%s\"", state_names[k].c_str());

      for (int d = 0; d < PhysDim::D; d++)
        for (int k = 0; k < N; k++)
          fprintf( fp, ", \"%s_%c\"", state_names[k].c_str(), xyz[d] );
    }

    std::vector<std::string> derivedQtyNames = pde.derivedQuantityNames();
    for (int k = 0; k < (int) derivedQtyNames.size(); k++)
      fprintf( fp, ", \"%s\"", derivedQtyNames[k].c_str() );

    fprintf( fp, "\n" );
  }

  const XField<PhysDim,TopoD1>& xgrid = qfld.getXField();

  const int ngroup = qfld.nCellGroups();

  // loop over cell groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Line) )
    {
      output_Tecplot_CellGroup<PDE, T, PhysDim, TopoD1, Line, ParamFieldType>(
          pde, *xgrid.comm(), xgrid.cellIDs(group), partitioned,
          paramfld.template getCellGroup<Line>(group),
          qfld.template getCellGroup<Line>(group),
          rfld.template getCellGroup<Line>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(PDE): Unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  if (qfld.comm()->rank() == 0 || partitioned)
    fclose( fp );
}

//--------------------------------------------------------------------------------
//TopoD2 specialization
template<class PDE, class PhysDim, class T, class ParamFieldType>
void
output_Tecplot( const PDE& pde, const ParamFieldType& paramfld, const Field< PhysDim, TopoD2, T >& qfld,
                const FieldLift<PhysDim, TopoD2, DLA::VectorS<PhysDim::D, T> >& rfld,
                const std::string& filename,
                const std::vector<std::string>& state_names = std::vector<std::string>(),
                const bool partitioned = false )
{
  SANS_ASSERT( &paramfld.getXField() == &qfld.getXField() );
  SANS_ASSERT( &paramfld.getXField() == &rfld.getXField() );

  FILE* fp = nullptr;

  if (qfld.comm()->rank() == 0 || partitioned)
  {
    std::cout << "output_Tecplot: filename = " << filename << std::endl;
    fp = fopen( filename.c_str(), "w" );
    if (fp == NULL)
      SANS_DEVELOPER_EXCEPTION("output_Tecplot<PhysDim,TopoD2> - Error opening file: %s", filename.c_str());

    fprintf( fp, "\"\"\n" );
    fprintf( fp, "VARIABLES = \"X\"" );

    const char XYZ[] = "XYZ";
    const char xyz[] = "xyz";

    for (int d = 1; d < PhysDim::D; d++)
      fprintf( fp, ", \"%c\"", XYZ[d] );

    static const int N = DLA::VectorSize<T>::M;
    if (state_names.empty())
    {
      for (int k = 0; k < N; k++)
        fprintf( fp, ", \"q%d\"", k+1 );

      for (int d = 0; d < PhysDim::D; d++)
        for (int k = 0; k < N; k++)
          fprintf( fp, ", \"q%d_%c\"", k+1, xyz[d] );
    }
    else
    {
      SANS_ASSERT((int) state_names.size() == N);
      for (int k = 0; k < N; k++)
        fprintf( fp, ", \"%s\"", state_names[k].c_str());

      for (int d = 0; d < PhysDim::D; d++)
        for (int k = 0; k < N; k++)
          fprintf( fp, ", \"%s_%c\"", state_names[k].c_str(), xyz[d] );
    }

    std::vector<std::string> derivedQtyNames = pde.derivedQuantityNames();
    for (int k = 0; k < (int) derivedQtyNames.size(); k++)
      fprintf( fp, ", \"%s\"", derivedQtyNames[k].c_str() );

    fprintf( fp, "\n" );
  }

  const XField<PhysDim,TopoD2>& xgrid = qfld.getXField();

  const int ngroup = qfld.nCellGroups();

  // loop over cell groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      output_Tecplot_CellGroup<PDE, T, PhysDim, TopoD2, Triangle, ParamFieldType>(
          pde, *xgrid.comm(), xgrid.cellIDs(group), partitioned,
          paramfld.template getCellGroup<Triangle>(group),
          qfld.template getCellGroup<Triangle>(group),
          rfld.template getCellGroup<Triangle>(group), fp );
    }
    else if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Quad) )
    {
      output_Tecplot_CellGroup<PDE, T, PhysDim, TopoD2, Quad, ParamFieldType>(
          pde, *xgrid.comm(), xgrid.cellIDs(group), partitioned,
          paramfld.template getCellGroup<Quad>(group),
          qfld.template getCellGroup<Quad>(group),
          rfld.template getCellGroup<Quad>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(PDE): Unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  if (qfld.comm()->rank() == 0 || partitioned)
    fclose( fp );
}

//--------------------------------------------------------------------------------
//TopoD3 specialization
template<class PDE, class PhysDim, class T, class ParamFieldType>
void
output_Tecplot( const PDE& pde, const ParamFieldType& paramfld, const Field< PhysDim, TopoD3, T >& qfld,
                const FieldLift<PhysDim, TopoD3, DLA::VectorS<PhysDim::D, T> >& rfld,
                const std::string& filename,
                const std::vector<std::string>& state_names = std::vector<std::string>(),
                const bool partitioned = false )
{
  SANS_ASSERT( &paramfld.getXField() == &qfld.getXField() );
  SANS_ASSERT( &paramfld.getXField() == &rfld.getXField() );

  FILE* fp = nullptr;

  if (qfld.comm()->rank() == 0 || partitioned)
  {
    std::cout << "output_Tecplot: filename = " << filename << std::endl;
    fp = fopen( filename.c_str(), "w" );
    if (fp == NULL)
      SANS_DEVELOPER_EXCEPTION("output_Tecplot<PhysDim,TopoD2> - Error opening file: %s", filename.c_str());

    fprintf( fp, "\"\"\n" );
    fprintf( fp, "VARIABLES = \"X\"" );

    const char XYZ[] = "XYZ";
    const char xyz[] = "xyz";

    for (int d = 1; d < PhysDim::D; d++)
      fprintf( fp, ", \"%c\"", XYZ[d] );

    static const int N = DLA::VectorSize<T>::M;
    if (state_names.empty())
    {
      for (int k = 0; k < N; k++)
        fprintf( fp, ", \"q%d\"", k+1 );

      for (int d = 0; d < PhysDim::D; d++)
        for (int k = 0; k < N; k++)
          fprintf( fp, ", \"q%d_%c\"", k+1, xyz[d] );
    }
    else
    {
      SANS_ASSERT((int) state_names.size() == N);
      for (int k = 0; k < N; k++)
        fprintf( fp, ", \"%s\"", state_names[k].c_str());

      for (int d = 0; d < PhysDim::D; d++)
        for (int k = 0; k < N; k++)
          fprintf( fp, ", \"%s_%c\"", state_names[k].c_str(), xyz[d] );
    }

    std::vector<std::string> derivedQtyNames = pde.derivedQuantityNames();
    for (int k = 0; k < (int) derivedQtyNames.size(); k++)
      fprintf( fp, ", \"%s\"", derivedQtyNames[k].c_str() );

    fprintf( fp, "\n" );
  }

  const XField<PhysDim,TopoD3>& xgrid = qfld.getXField();

  const int ngroup = qfld.nCellGroups();

  // loop over cell groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Tet) )
    {
      output_Tecplot_CellGroup<PDE, T, PhysDim, TopoD3, Tet, ParamFieldType>(
          pde, *xgrid.comm(), xgrid.cellIDs(group), partitioned,
          paramfld.template getCellGroup<Tet>(group),
          qfld.template getCellGroup<Tet>(group),
          rfld.template getCellGroup<Tet>(group), fp );
    }
    else if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Hex) )
    {
      output_Tecplot_CellGroup<PDE, T, PhysDim, TopoD3, Hex, ParamFieldType>(
          pde, *xgrid.comm(), xgrid.cellIDs(group), partitioned,
          paramfld.template getCellGroup<Hex>(group),
          qfld.template getCellGroup<Hex>(group),
          rfld.template getCellGroup<Hex>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(PDE): Unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  if (qfld.comm()->rank() == 0 || partitioned)
    fclose( fp );
}


} //namespace SANS

#endif  // OUTPUT_TECPLOT_PDE_H
