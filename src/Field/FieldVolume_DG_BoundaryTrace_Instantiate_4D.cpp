// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define FIELD_DG_BOUNDARYTRACEBASE_INSTANTIATE
#include "Field_DG_BoundaryTraceBase_impl.h"

#define FIELDVOLUME_DG_BOUNDARYTRACE_INSTANTIATE
#include "FieldVolume_DG_BoundaryTrace_impl.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "XFieldVolume.h"

namespace SANS
{

template class Field_DG_BoundaryTrace< PhysD4, TopoD3, Real >;

template class Field_DG_BoundaryTrace< PhysD4, TopoD3, DLA::VectorS<2, Real> >;
template class Field_DG_BoundaryTrace< PhysD4, TopoD3, DLA::VectorS<3, Real> >;
template class Field_DG_BoundaryTrace< PhysD4, TopoD3, DLA::VectorS<4, Real> >;
template class Field_DG_BoundaryTrace< PhysD4, TopoD3, DLA::VectorS<5, Real> >;
template class Field_DG_BoundaryTrace< PhysD4, TopoD3, DLA::VectorS<6, Real> >;
template class Field_DG_BoundaryTrace< PhysD4, TopoD3, DLA::VectorS<7, Real> >;

}
