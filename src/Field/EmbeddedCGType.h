// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_FIELD_EMBEDDEDCGTYPE_H_
#define SRC_FIELD_EMBEDDEDCGTYPE_H_


enum embeddedType
{
  EmbeddedCGField,
  RegularCGField
};



#endif /* SRC_FIELD_EMBEDDEDCGTYPE_H_ */
