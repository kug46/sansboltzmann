// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "Field/output_Tecplot.h"
#include "TecTopo.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "pde/FullPotential/PDEFullPotential3D.h"

#include "Field/XFieldVolume.h"
#include "Field/FieldVolume.h"

namespace SANS
{

typedef typename PDEFullPotential3D<Real>::ArrayQ<Real> ArrayQ;

//----------------------------------------------------------------------------//
// output as P1
//
#if 1
template<class Topology>
void
output_Tecplot_CellGroup_FP( const PDEFullPotential3D<Real>& pde,
                             const typename XField<PhysD3, TopoD3>::FieldCellGroupType<Topology>& xfld,
                             const typename Field< PhysD3, TopoD3, ArrayQ >::FieldCellGroupType<Topology>& qfld,
                             FILE* fp )
{
  typedef typename XField<PhysD3, TopoD3>::FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename Field<PhysD3, TopoD3, ArrayQ >::FieldCellGroupType<Topology> QFieldCellGroupType;

  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfld.basis() );
  ElementQFieldClass qfldElem( qfld.basis() );

  int nelem = xfld.nElem();
  int nnode = Topology::NNode*nelem;
  fprintf( fp, "ZONE T=\"grid\", N=%d, E=%d, F=FEPOINT, ET=%s\n", nnode, nelem, TecTopo<Topology>::name() );

  // grid coordinates and solution

  //Real V2 = 0;
  typename ElementXFieldClass::RefCoordType stu;
  typename ElementXFieldClass::VectorX X;
  ArrayQ q;
  Real Cp = 0, rho, rho_sh, p, M;
  DLA::VectorS<3, ArrayQ > gradq;
  DLA::VectorS<3, Real > U, V, gradPhi;

  pde.freestream(U);
  Real Vinf2 = dot(U,U);

  Real rhoinf = pde.rhoinf();
  Real pinf = pde.pinf();

  // loop over elements within group
  for (int elem = 0; elem < nelem; elem++)
  {
    // copy global grid/solution DOFs to element
    xfld.getElement( xfldElem, elem );
    qfld.getElement( qfldElem, elem );

    for ( int i = 0; i < Topology::NNode; i++ )
    {
      stu = 0;
      if ( Topology::NNode == 4)
      {
        if (i > 0 ) stu[i-1] = 1;
      }
      else
      {
        if ( i == 1 ) stu = {1,0,0};
        if ( i == 2 ) stu = {1,1,0};
        if ( i == 3 ) stu = {0,1,0};
        if ( i == 4 ) stu = {0,0,1};
        if ( i == 5 ) stu = {1,0,1};
        if ( i == 6 ) stu = {1,1,1};
        if ( i == 7 ) stu = {0,1,1};
      }

      xfldElem.eval( stu, X );
      qfldElem.eval( stu, q );
      xfldElem.evalGradient( stu, qfldElem, gradq );

      for (int d = 0; d < PhysD3::D; d++)
        gradPhi[d] = gradq[d][0];

      rho = q[1];
      V = U + gradPhi;

      p = pde.pressure(V);
      Cp = (p-pinf)/(0.5*rhoinf*Vinf2);

      M = pde.mach( q, gradq[0], gradq[1], gradq[2] ); //sqrt(dot(V,V))/sqrt(gamma*p/rho);

      rho_sh = pde.density_sh(gradq);

      fprintf( fp, "%22.15e %22.15e %22.15e", X[0], X[1], X[2] );
      fprintf( fp, " %22.15e", rho );
      fprintf( fp, " %22.15e %22.15e %22.15e %22.15e", q[0], gradPhi[0], gradPhi[1], gradPhi[2] );
      fprintf( fp, " %14.7e %14.7e %14.7e", V[0], V[1], V[2] );
      fprintf( fp, " %14.7e", rho_sh );
      fprintf( fp, " %14.7e", Cp );
      fprintf( fp, " %14.7e", M );
      fprintf( fp, "\n" );
    }
  }

  // cell-to-node connectivity

  for (int elem = 0; elem < nelem; elem++)
  {
    for (int n = 0; n < Topology::NNode; n++ )
      fprintf( fp, "%d ", Topology::NNode*elem+n+1 );
    fprintf( fp, "\n" );
  }


}
#else
template<class Topology>
void
output_Tecplot_CellGroup_FP( const PDEFullPotential3D<Real>& pde,
                              const typename XField<PhysD3, TopoD3>::FieldCellGroupType<Topology>& xfld,
                              const typename Field< PhysD3, TopoD3, ArrayQ >::FieldCellGroupType<Topology>& qfld,
                              FILE* fp )
{
  typedef typename XField<PhysD3, TopoD3>::template FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename Field<PhysD3, TopoD3, ArrayQ >::template FieldCellGroupType<Topology> QFieldCellGroupType;

  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfld.basis() );
  ElementQFieldClass qfldElem( qfld.basis() );

  int nelem = xfld.nElem();
  int nnode = xfld.nDOF();
  fprintf( fp, "ZONE T=\"grid\", N=%d, E=%d, F=FEPOINT, ET=%s\n", nnode, nelem, TecTopo<Topology>::name() );

  // grid coordinates and solution

  //Real V2 = 0;
  typename ElementXFieldClass::RefCoordType stu;
  typename ElementXFieldClass::VectorX X;
  ArrayQ q;
  Real Cp = 0;
  DLA::VectorS<3, ArrayQ > gradq = 0;
  DLA::VectorS<3, Real > V = 0, gradPhi = 0;

  // loop over elements within group
  for (int node = 0; node < nnode; node++)
  {
    // copy global grid/solution DOFs
    X = xfld.DOF( node );
    q = qfld.DOF( node );

    fprintf( fp, "%22.15e %22.15e %22.15e", X[0], X[1], X[2] );
    fprintf( fp, " %22.15e", q[1] );
    fprintf( fp, " %22.15e %22.15e %22.15e %22.15e", q[0], gradPhi[0], gradPhi[1], gradPhi[2] );
    fprintf( fp, " %14.7e %14.7e %14.7e", V[0], V[1], V[2] );
    fprintf( fp, " %14.7e", Cp );
    fprintf( fp, "\n" );
  }

  // cell-to-node connectivity

  int globalNodeMap[Topology::NNode];
  for (int elem = 0; elem < nelem; elem++)
  {
    qfld.associativity( elem ).getNodeGlobalMapping(globalNodeMap, Topology::NNode);
    for (int n = 0; n < Topology::NNode; n++)
      fprintf( fp, "%d ", globalNodeMap[n]+1);
    fprintf( fp, "\n");
  }


}
#endif
#if 0
template
void
output_Tecplot_CellGroup_FP( const PDEFullPotential3D<Real>& pde,
                              const typename XField<PhysD3, TopoD3>::FieldCellGroupType<Tet>& xfld,
                              const typename Field< PhysD3, TopoD3, ArrayQ >::FieldCellGroupType<Tet>& qfld,
                              FILE* fp );

template
void
output_Tecplot_CellGroup_FP( const PDEFullPotential3D<Real>& pde,
                              const typename XField<PhysD3, TopoD3>::FieldCellGroupType<Tet>& xfld,
                              const typename Field< PhysD3, TopoD3, ArrayQ >::FieldCellGroupType<Tet>& qfld,
                              FILE* fp );
#endif


//----------------------------------------------------------------------------//
// output as P1
//
template<class TopologyTrace, class TopologyCell>
void
output_Tecplot_TraceGroup_FP( const int group, const PDEFullPotential3D<Real>& pde,
                               const XField<PhysD3,TopoD3>& xfld,
                               const typename XField<PhysD3,TopoD3>::FieldTraceGroupType<TopologyTrace>& xfldTrace,
                               const Field< PhysD3, TopoD3, ArrayQ >& qfld,
                               const typename Field< PhysD3, TopoD3, ArrayQ >::FieldTraceGroupType<TopologyTrace>& qfldTrace,
                               FILE* fp )
{
  typedef typename XField<PhysD3, TopoD3>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename Field<PhysD3, TopoD3, ArrayQ >::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;

  typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldClass;
  typedef typename QFieldTraceGroupType::template ElementType<> ElementQFieldClass;

  typedef typename XField<PhysD3, TopoD3>::template FieldCellGroupType<TopologyCell> XFieldCellGroupTypeL;
  typedef typename Field<PhysD3, TopoD3, ArrayQ>::template FieldCellGroupType<TopologyCell> QFieldCellGroupTypeL;

  typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
  typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;

  // element field variables
  ElementXFieldClass xfldElem( xfldTrace.basis() );
  ElementQFieldClass qfldElem( qfldTrace.basis() );

  const XFieldCellGroupTypeL& xfldCellL = xfld.getCellGroup<TopologyCell>(xfldTrace.getGroupLeft());
  const QFieldCellGroupTypeL& qfldCellL = qfld.getCellGroup<TopologyCell>(xfldTrace.getGroupLeft());

  ElementXFieldClassL xfldElemL( xfldCellL.basis() );
  ElementQFieldClassL qfldElemL( qfldCellL.basis() );

  int nelem = xfldTrace.nElem();
  int nnode = TopologyTrace::NNode*nelem;
  fprintf( fp, "ZONE T=\"Boundary %d\", N=%d, E=%d, F=FEPOINT, ET=%s\n", group, nnode, nelem, TecTopo<TopologyTrace>::name() );

  typename ElementXFieldClassL::RefCoordType sRefCell;
  typename ElementXFieldClass::RefCoordType sRefTrace;
  typename ElementXFieldClass::VectorX X, N;
  ArrayQ q;
  Real Cp = 0, rho, rho_sh, p, M;
  DLA::VectorS<3, ArrayQ > gradq = 0;
  DLA::VectorS<3, Real > U, V, gradPhi;

  pde.freestream(U);
  Real Vinf2 = dot(U,U);

  Real rhoinf = pde.rhoinf();
  Real pinf = pde.pinf();

  for (int elem = 0; elem < nelem; elem++)
  {
    xfldTrace.getElement( xfldElem, elem );
    qfldTrace.getElement( qfldElem, elem );

    int elemL = xfldTrace.getElementLeft( elem );
    CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

    xfldCellL.getElement( xfldElemL, elemL );
    qfldCellL.getElement( qfldElemL, elemL );


    for ( int i = 0; i < Triangle::NNode; i++ )
    {
      sRefTrace = 0;
      if ( TopologyTrace::NNode == 3)
      {
        if (i > 0 ) sRefTrace[i-1] = 1;
      }
      else
      {
        if ( i == 1 ) sRefTrace = {1,0};
        if ( i == 2 ) sRefTrace = {1,1};
        if ( i == 3 ) sRefTrace = {0,1};
      }
      xfldElem.unitNormal( sRefTrace, N );

      xfldElem.eval( sRefTrace, X );
      qfldElem.eval( sRefTrace, q );

      TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalTraceL, sRefTrace, sRefCell );

      xfldElemL.evalGradient( sRefCell, qfldElemL, gradq );  //Just dumping lagrange multipliers

      for (int d = 0; d < PhysD3::D; d++)
        gradPhi[d] = gradq[d][0];

      rho = q[1];
      V = U + gradPhi;

      p = pde.pressure(V);
      Cp = (p-pinf)/(0.5*rhoinf*Vinf2);

      M = pde.mach( q, gradq[0], gradq[1], gradq[2] ); //sqrt(dot(V,V))/sqrt(gamma*p/rho);

      rho_sh = pde.density_sh(gradq);

      fprintf( fp, "%22.15e %22.15e %22.15e", X[0], X[1], X[2] );
      fprintf( fp, " %22.15e", rho );
      fprintf( fp, " %22.15e %22.15e %22.15e %22.15e", q[0], gradPhi[0], gradPhi[1], gradPhi[2] );
      fprintf( fp, " %14.7e %14.7e %14.7e", V[0], V[1], V[2] );
      fprintf( fp, " %14.7e", rho_sh );
      fprintf( fp, " %14.7e", Cp );
      fprintf( fp, " %14.7e", M );
      fprintf( fp, "\n" );
    }
  }

  // cell-to-node connectivity

  for (int elem = 0; elem < nelem; elem++)
  {
    for (int n = 0; n < TopologyTrace::NNode; n++ )
      fprintf( fp, "%d ", TopologyTrace::NNode*elem+n+1 );
    fprintf( fp, "\n" );
  }


}

//----------------------------------------------------------------------------//
// output as P1
//
template<class TopologyTrace, class TopologyCell>
void
output_Tecplot_TraceGroup_FP( const int group, const PDEFullPotential3D<Real>& pde,
                               const typename XField<PhysD3,TopoD3>::FieldTraceGroupType<TopologyTrace>& xfldTrace,
                               const typename XField<PhysD3,TopoD3>::FieldCellGroupType<TopologyCell>& xfld,
                               const typename Field< PhysD3, TopoD3, ArrayQ >::FieldCellGroupType<TopologyCell>& qfld,
                               FILE* fp, bool leftElement = true )
{
  typedef typename XField<PhysD3, TopoD3>::FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename XField<PhysD3, TopoD3>::FieldCellGroupType<TopologyCell> XFieldCellGroupType;
  typedef typename Field<PhysD3, TopoD3, ArrayQ >::template FieldCellGroupType<TopologyCell> QFieldCellGroupType;

  typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;


  // element field variables
  ElementXFieldTraceClass xfldTraceElem( xfldTrace.basis() );
  ElementXFieldClass xfldElem( xfld.basis() );
  ElementQFieldClass qfldElem( qfld.basis() );

  int nelem = xfldTrace.nElem();
  int nnode = TopologyTrace::NNode*nelem;
  fprintf( fp, "ZONE T=\"Boundary %s %d\", N=%d, E=%d, F=FEPOINT, ET=%s\n",
           leftElement ? "L" : "R", group, nnode, nelem, TecTopo<TopologyTrace>::name() );

  typename ElementXFieldTraceClass::RefCoordType sRefTrace;
  typename ElementQFieldClass::RefCoordType stu;
  typename ElementXFieldTraceClass::VectorX X, N;
  ArrayQ q;
  Real Cp = 0, rho, rho_sh, p, M;
  DLA::VectorS<3, ArrayQ > gradq;
  DLA::VectorS<3, Real > U, V, gradPhi;
  int elemCell;
  CanonicalTraceToCell canonicalTrace;

  pde.freestream(U);
  Real Vinf2 = dot(U,U);
  //Real V2 = 0;

  Real rhoinf = pde.rhoinf();
  Real pinf = pde.pinf();

  for (int elem = 0; elem < nelem; elem++)
  {
    xfldTrace.getElement( xfldTraceElem, elem );

    if ( leftElement )
    {
      elemCell = xfldTrace.getElementLeft( elem );
      canonicalTrace = xfldTrace.getCanonicalTraceLeft( elem );
    }
    else
    {
      elemCell = xfldTrace.getElementRight( elem );
      canonicalTrace = xfldTrace.getCanonicalTraceRight( elem );
    }

    xfld.getElement( xfldElem, elemCell );
    qfld.getElement( qfldElem, elemCell );

    for ( int i = 0; i < TopologyTrace::NNode; i++ )
    {
      sRefTrace = 0;
      if ( TopologyTrace::NNode == 3)
      {
        if (i > 0 ) sRefTrace[i-1] = 1;
      }
      else
      {
        if ( i == 1 ) sRefTrace = {1,0};
        if ( i == 2 ) sRefTrace = {1,1};
        if ( i == 3 ) sRefTrace = {0,1};
      }

      xfldTraceElem.eval( sRefTrace, X );
      xfldTraceElem.unitNormal( sRefTrace, N );

      TraceToCellRefCoord<TopologyTrace, TopoD3, TopologyCell>::eval( canonicalTrace, sRefTrace, stu );
      qfldElem.eval( stu, q );

      xfldElem.evalGradient( stu, qfldElem, gradq );

      for (int d = 0; d < PhysD3::D; d++)
        gradPhi[d] = gradq[d][0];

      rho = q[1];
      V = U + gradPhi;

      p = pde.pressure(V);
      Cp = (p-pinf)/(0.5*rhoinf*Vinf2);

      M = pde.mach( q, gradq[0], gradq[1], gradq[2] ); //sqrt(dot(V,V))/sqrt(gamma*p/rho);

      rho_sh = pde.density_sh(gradq);

      fprintf( fp, "%22.15e %22.15e %22.15e", X[0], X[1], X[2] );
      fprintf( fp, " %22.15e", rho );
      fprintf( fp, " %22.15e %22.15e %22.15e %22.15e", q[0], gradPhi[0], gradPhi[1], gradPhi[2] );
      fprintf( fp, " %14.7e %14.7e %14.7e", V[0], V[1], V[2] );
      fprintf( fp, " %14.7e", rho_sh );
      fprintf( fp, " %14.7e", Cp );
      fprintf( fp, " %14.7e", M );
      fprintf( fp, "\n" );
    }
  }

  // cell-to-node connectivity

  for (int elem = 0; elem < nelem; elem++)
  {
    for (int n = 0; n < TopologyTrace::NNode; n++ )
      fprintf( fp, "%d ", TopologyTrace::NNode*elem+n+1 );
    fprintf( fp, "\n" );
  }


}


//----------------------------------------------------------------------------//
void
output_Tecplot_FP( const PDEFullPotential3D<Real>& pde,
                   const Field< PhysD3, TopoD3, DLA::VectorS<2,Real> >& qfld,
                   const std::string& filename )
{
  std::cout << "output_Tecplot: filename = " << filename << std::endl;
  FILE* fp = fopen( filename.c_str(), "w" );
  SANS_ASSERT( fp != NULL );

  fprintf( fp, "\"\"\n" );
  fprintf( fp, "VARIABLES = \"X\", \"Y\", \"Z\"" );
  fprintf( fp, ", \"rho\", \"phi\", \"phix\", \"phiy\", \"phiz\", \"u\", \"v\", \"w\", \"rho_sh\", \"Cp\", \"M\"" );
  fprintf( fp, "\n" );


  const XField<PhysD3,TopoD3>& xgrid = qfld.getXField();

  const int ngroup = qfld.nCellGroups();

  // loop over cell groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Tet) )
    {
      output_Tecplot_CellGroup_FP( pde,
                                    xgrid.getCellGroup<Tet>(group),
                                    qfld.getCellGroup<Tet>(group), fp );
    }
    else if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Hex) )
    {
      output_Tecplot_CellGroup_FP( pde,
                                    xgrid.getCellGroup<Hex>(group),
                                    qfld.getCellGroup<Hex>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot_FP(XField<PhysD3,TopoD3>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }


#if 0
  if ( qfld.nBoundaryTraceGroups() > 0 )
  {
    // loop over trace boundary groups
    for (int group = 0; group < qfld.nBoundaryTraceGroups(); group++)
    {
      // dispatch integration over cell of group
      if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Triangle) )
      {
        output_Tecplot_TraceGroup_FP<Triangle,Tet>( group, pde, xgrid, xgrid.getBoundaryTraceGroup<Triangle>(group),
                                                     qfld, qfld.getBoundaryTraceGroup<Triangle>(group), fp );
      }
      else if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Quad) )
      {
        output_Tecplot_TraceGroup_FP<Quad,Hex>( group, pde, xgrid, xgrid.getBoundaryTraceGroup<Quad>(group),
                                                 qfld, qfld.getBoundaryTraceGroup<Quad>(group), fp );
      }
      else
      {
        const char msg[] = "Error in output_Tecplot_FP(XField<PhysD3,TopoD3>): unknown topology\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }
  }
#endif

  // loop over trace element groups
  for (int group = 0; group < xgrid.nBoundaryTraceGroups(); group++)
  {
    if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      int groupL = xgrid.getBoundaryTraceGroup<Triangle>(group).getGroupLeft();
      int groupR = xgrid.getBoundaryTraceGroup<Triangle>(group).getGroupRight();
      output_Tecplot_TraceGroup_FP( group, pde, xgrid.getBoundaryTraceGroup<Triangle>(group),
                                    xgrid.getCellGroup<Tet>(groupL),
                                    qfld.getCellGroup<Tet>(groupL), fp );

      // Dump out the trace from the elements on the right if a right group exists
      if (groupR >= 0)
        output_Tecplot_TraceGroup_FP( group, pde, xgrid.getBoundaryTraceGroup<Triangle>(group),
                                      xgrid.getCellGroup<Tet>(groupR),
                                      qfld.getCellGroup<Tet>(groupR), fp, false );

    }
    else if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Quad) )
    {
      int groupL = xgrid.getBoundaryTraceGroup<Quad>(group).getGroupLeft();
      int groupR = xgrid.getBoundaryTraceGroup<Quad>(group).getGroupRight();
      output_Tecplot_TraceGroup_FP( group, pde, xgrid.getBoundaryTraceGroup<Quad>(group),
                                    xgrid.getCellGroup<Hex>(groupL),
                                    qfld.getCellGroup<Hex>(groupL), fp );

      // Dump out the trace from the elements on the right if a right group exists
      if (groupR >= 0)
        output_Tecplot_TraceGroup_FP( group, pde, xgrid.getBoundaryTraceGroup<Quad>(group),
                                      xgrid.getCellGroup<Hex>(groupR),
                                      qfld.getCellGroup<Hex>(groupR), fp, false );

    }
    else
    {
      char msg[] = "Error in output_Tecplot_FP<TopoD3>::integrate: unknown trace topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  fclose( fp );
}


} //namespace SANS
