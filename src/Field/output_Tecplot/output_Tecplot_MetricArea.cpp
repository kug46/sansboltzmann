// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define OUTPUT_TECPLOT_METRIC_INSTANTIATE
#include "output_Tecplot_Metric_impl.h"

#include "Field/XFieldArea.h"

#include "Field/FieldArea.h"

namespace SANS
{

//----------------------------------------------------------------------------//
void
output_Tecplot_Metric_Nodes(
    const XField<PhysD2, TopoD2>& xfld,
    const  Field<PhysD2, TopoD2, DLA::MatrixSymS<PhysD2::D, Real>>& mfld,
    FILE* fp )
{
  static const int npts = 36; // Number of points to draw each ellipse
  double scale = 0.5; // so the ellipses touch for an ideal grid
  double dt = PI/180.*(360.0 / (double)npts);

  DLA::VectorS<PhysD2::D,double> L, e, X;
  DLA::MatrixS<PhysD2::D,PhysD2::D,double> E;

  // This function assumes linear grids and metric field
  for (int i = 0; i < xfld.nCellGroups(); i++)
  {
    SANS_ASSERT( xfld.getCellGroupBase(i).order() == 1 );
    SANS_ASSERT( mfld.getCellGroupBase(i).order() == 1 );
  }

  // Compute the total number of nodes in the grid
  int nNodeGlobal = mfld.nDOFpossessed();
#ifdef SANS_MPI
  nNodeGlobal = boost::mpi::all_reduce(*xfld.comm(), nNodeGlobal, std::plus<int>());
#endif

  int comm_rank = xfld.comm()->rank();

  int nElem = nNodeGlobal * npts;

  if (comm_rank == 0)
    fprintf( fp, "ZONE T=\"metric\", N=%d, E=%d, F=FEPOINT, ET=LINESEG\n",
             nElem, nElem );

  // grid coordinates and solution

#ifdef SANS_MPI
  typedef std::pair<DLA::VectorS<PhysD2::D,double>, DLA::MatrixSymS<PhysD2::D, double>> NodeMetricType;

  int comm_size = xfld.comm()->size();

  // maximum node chunk size that rank 0 will write at any given time
  int nodeChunk = nNodeGlobal / comm_size + nNodeGlobal % comm_size;

  // send one chunk of elements at a time to rank 0
  for (int rank = 0; rank < comm_size; rank++)
  {
    int nodeLow  =  rank   *nodeChunk;
    int nodeHigh = (rank+1)*nodeChunk;

    std::map<int,NodeMetricType> buffer;

    // loop over elements within group
    for (int node = 0; node < mfld.nDOFpossessed(); node++)
    {
      int nodeID = mfld.local2nativeDOFmap(node);

      if (nodeID >= nodeLow && nodeID < nodeHigh)
      {
        // copy global grid/solution DOFs to the buffer
        NodeMetricType nodeValues;

        nodeValues.first = xfld.DOF(node);
        nodeValues.second = mfld.DOF(node);

        buffer[nodeID] = nodeValues;
      }
    }

    if (comm_rank == 0)
    {
      std::vector<std::map<int,NodeMetricType>> bufferOnRank;
      boost::mpi::gather(*xfld.comm(), buffer, bufferOnRank, 0 );

      // collapse down the buffer from all other ranks
      for (std::size_t i = 1; i < bufferOnRank.size(); i++)
        buffer.insert(bufferOnRank[i].begin(), bufferOnRank[i].end());

      //Write the node elipse data
      for ( const auto& nodePair : buffer)
      {
        const NodeMetricType& XM = nodePair.second;

        // compute the eigen system
        DLA::EigenSystem(XM.second, L, E );

        for (int i = 0; i < npts; i++)
        {
          e[0] = scale * cos(i * dt) / sqrt(L[0]);
          e[1] = scale * sin(i * dt) / sqrt(L[1]);
          X = E*e;
          fprintf(fp, " %.16e %.16e\n",
                  XM.first[0] + X[0],
                  XM.first[1] + X[1]);
        }
      }
    }
#ifndef __clang_analyzer__
    else // send the buffer to rank 0
      boost::mpi::gather(*xfld.comm(), buffer, 0 );
#endif

  } // for rank

#else

  // loop over elements within group
  for (int node = 0; node < nNodeGlobal; node++)
  {
    DLA::VectorS<PhysD2::D,double> Xn = xfld.DOF(node);
    DLA::MatrixSymS<PhysD2::D,double> m = mfld.DOF(node);

    // compute the eigen system
    DLA::EigenSystem(m, L, E );

    for (int i = 0; i < npts; i++)
    {
      e[0] = scale * cos(i * dt) / sqrt(L[0]);
      e[1] = scale * sin(i * dt) / sqrt(L[1]);
      X = E*e;
      fprintf(fp, " %.16e %.16e\n",
              Xn[0] + X[0],
              Xn[1] + X[1]);
    }
  }

#endif

  // cell-to-node connectivity

  if (comm_rank == 0)
  {
    for (int node = 0; node < nNodeGlobal; node++)
    {
      for (int i = 0; i < npts - 1; i++)
        fprintf(fp, " %d %d\n", i + node * npts + 1, i + 1 + node * npts + 1);
      fprintf(fp, " %d %d\n", npts + node * npts, 1 + node * npts);
    }
  }
}

//----------------------------------------------------------------------------//
template<>
void
output_Tecplot_Metric( const Field< PhysD2, TopoD2, DLA::MatrixSymS<PhysD2::D, Real> >& mfld,
                       const std::string& filename )
{
  FILE* fp = nullptr;

  if (mfld.comm()->rank() == 0)
  {
    std::cout << "output_Tecplot: filename = " << filename << std::endl;
    fp = fopen( filename.c_str(), "w" );
    if (fp == NULL)
      SANS_RUNTIME_EXCEPTION("Error opening file: %s", filename.c_str());

    fprintf( fp, "\"\"\n" );
    fprintf( fp, "VARIABLES = \"X\", \"Y\"\n" );
  }

  const XField<PhysD2,TopoD2>& xgrid = mfld.getXField();

  output_Tecplot_Metric_Nodes( xgrid, mfld, fp );

  const int ngroup = xgrid.nCellGroups();

  // loop over element groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over elements of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      if (group == 0)
        output_Tecplot_CellGroup_Nodes<PhysD2, TopoD2, Triangle>( xgrid, group, xgrid.getCellGroup<Triangle>(group), fp );
      else
        output_Tecplot_CellGroup<PhysD2, TopoD2, Triangle>( xgrid, group, xgrid.getCellGroup<Triangle>(group), fp );
    }
    else if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Quad) )
    {
      if (group == 0)
        output_Tecplot_CellGroup_Nodes<PhysD2, TopoD2, Quad>( xgrid, group, xgrid.getCellGroup<Quad>(group), fp );
      else
        output_Tecplot_CellGroup<PhysD2, TopoD2, Quad>( xgrid, group, xgrid.getCellGroup<Quad>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD2,TopoD2>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

#if 0
  // loop over interior trace groups (skip the first one because it should mostly duplicate the entire grid)
  for (int group = 1; group < xgrid.nInteriorTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Line) )
    {
      output_Tecplot_TraceGroup<PhysD2, TopoD2, Line>(xgrid, "Interior", group, xgrid.getInteriorTraceGroup<Line>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD2,TopoD2>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }
#endif

  // loop over trace boundary groups
  for (int group = 0; group < xgrid.nBoundaryTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Line) )
    {
      output_Tecplot_TraceGroup<PhysD2, TopoD2, Line>(xgrid, "Boundary", group, xgrid.getBoundaryTraceGroup<Line>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD2,TopoD2>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

#if 0
  // loop over ghost trace boundary groups
  for (int group = 0; group < xgrid.nGhostBoundaryTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Line) )
    {
      output_Tecplot_TraceGroup<PhysD2, TopoD2, Line>(xgrid, "Ghost", group, xgrid.getGhostBoundaryTraceGroup<Line>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD2,TopoD2>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }
#endif

  if (mfld.comm()->rank() == 0)
    fclose( fp );
}

} //namespace SANS
