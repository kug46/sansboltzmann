// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define OUTPUT_TECPLOT_METRIC_INSTANTIATE
#include "output_Tecplot_Metric_impl.h"

#include "Field/XFieldLine.h"

#include "Field/FieldLine.h"

namespace SANS
{

//----------------------------------------------------------------------------//
void
output_Tecplot_Metric_Nodes(
    const XField<PhysD1, TopoD1>& xfld,
    const  Field<PhysD1, TopoD1, DLA::MatrixSymS<PhysD1::D, Real>>& mfld,
    FILE* fp )
{
  // This function assumes linear grids and metric field
  for (int i = 0; i < xfld.nCellGroups(); i++)
  {
    SANS_ASSERT( xfld.getCellGroupBase(i).order() == 1 );
    SANS_ASSERT( mfld.getCellGroupBase(i).order() == 1 );
  }

  // Compute the total number of nodes in the grid
  int nNodeGlobal = mfld.nDOFpossessed();
#ifdef SANS_MPI
  nNodeGlobal = boost::mpi::all_reduce(*xfld.comm(), nNodeGlobal, std::plus<int>());
#endif

  int comm_rank = xfld.comm()->rank();

  int nElem = nNodeGlobal-1;

  if (comm_rank == 0)
    fprintf( fp, "ZONE T=\"metric\", N=%d, E=%d, F=FEPOINT, ET=LINESEG\n",
             nNodeGlobal, nElem );

  // grid coordinates and solution

#ifdef SANS_MPI
  typedef std::pair<DLA::VectorS<PhysD1::D,double>, DLA::MatrixSymS<PhysD1::D, double>> NodeMetricType;

  int comm_size = xfld.comm()->size();

  // maximum node chunk size that rank 0 will write at any given time
  int nodeChunk = nNodeGlobal / comm_size + nNodeGlobal % comm_size;

  // send one chunk of elements at a time to rank 0
  for (int rank = 0; rank < comm_size; rank++)
  {
    int nodeLow  =  rank   *nodeChunk;
    int nodeHigh = (rank+1)*nodeChunk;

    std::map<int,NodeMetricType> buffer;

    // loop over elements within group
    for (int node = 0; node < mfld.nDOFpossessed(); node++)
    {
      int nodeID = mfld.local2nativeDOFmap(node);

      if (nodeID >= nodeLow && nodeID < nodeHigh)
      {
        // copy global grid/solution DOFs to the buffer
        NodeMetricType nodeValues;

        nodeValues.first = xfld.DOF(node);
        nodeValues.second = mfld.DOF(node);

        buffer[nodeID] = nodeValues;
      }
    }

    if (comm_rank == 0)
    {
      std::vector<std::map<int,NodeMetricType>> bufferOnRank;
      boost::mpi::gather(*xfld.comm(), buffer, bufferOnRank, 0 );

      // collapse down the buffer from all other ranks
      for (std::size_t i = 1; i < bufferOnRank.size(); i++)
        buffer.insert(bufferOnRank[i].begin(), bufferOnRank[i].end());

      //Write the node elipse data
      for ( const auto& nodePair : buffer)
      {
        const NodeMetricType& XM = nodePair.second;

        fprintf(fp, " %.16e %.16e\n", XM.first[0], sqrt(1/XM.second(0,0)));
      }
    }
#ifndef __clang_analyzer__
    else // send the buffer to rank 0
      boost::mpi::gather(*xfld.comm(), buffer, 0 );
#endif

  } // for rank

#else

  // loop over elements within group
  for (int node = 0; node < nNodeGlobal; node++)
  {
    DLA::VectorS<PhysD1::D,double> Xn = xfld.DOF(node);
    DLA::MatrixSymS<PhysD1::D,double> m = mfld.DOF(node);

    fprintf(fp, " %.16e %.16e\n", Xn[0], sqrt(1/m(0,0)));
  }

#endif

  // cell-to-node connectivity

  if (comm_rank == 0)
  {
    for (int node = 0; node < nNodeGlobal-1; node++)
      fprintf(fp, " %d %d\n", 1 + node,  2 + node);
  }
}

//----------------------------------------------------------------------------//
template<>
void
output_Tecplot_Metric( const Field< PhysD1, TopoD1, DLA::MatrixSymS<PhysD1::D, Real> >& mfld,
                       const std::string& filename )
{
  FILE* fp = nullptr;

  if (mfld.comm()->rank() == 0)
  {
    std::cout << "output_Tecplot: filename = " << filename << std::endl;
    fp = fopen( filename.c_str(), "w" );
    if (fp == NULL)
      SANS_RUNTIME_EXCEPTION("Error opening file: %s", filename.c_str());

    fprintf( fp, "\"\"\n" );
    fprintf( fp, "VARIABLES = \"X\", \"h\"\n" );
  }

  const XField<PhysD1,TopoD1>& xgrid = mfld.getXField();

  output_Tecplot_Metric_Nodes( xgrid, mfld, fp );

  if (mfld.comm()->rank() == 0)
    fclose( fp );
}

} //namespace SANS
