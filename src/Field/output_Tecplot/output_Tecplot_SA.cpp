// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "Field/output_Tecplot.h"
#include "TecTopo.h"

#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"

#include "Field/XFieldArea.h"
#include "Field/XFieldVolume.h"

#include "Field/FieldArea.h"
#include "Field/FieldVolume.h"

#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/FieldLiftVolume_DG_Cell.h"

#include "Field/Element/ReferenceElementMesh.h"

#include "pde/NS/PDERANSSA3D.h"
#include "pde/NS/TraitsRANSSA.h"
#include "pde/NS/Q3DPrimitiveRhoPressure.h"
#include "pde/NS/QRANSSA3D.h"
#include "Discretization/DG/DiscretizationDGBR2.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"
#include "MPI/continuousElementMap.h"

namespace SANS
{

#if 0
//----------------------------------------------------------------------------//
template<class T, class PhysDim, class TopoDim, class Topology>
void
output_Tecplot_CellGroup( mpi::communicator& comm, const std::vector<int>& cellIDs, const bool partitioned,
                          const typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology>& xfldGroup,
                          const typename  Field<PhysDim, TopoDim, T>::template FieldCellGroupType<Topology>& qfldGroup,
                          FILE* fp )
{
  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename  Field<PhysDim, TopoDim, T>::template FieldCellGroupType<Topology> QFieldCellGroupType;

  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldTrace;
  typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldCell;

  // element field variables
  ElementXFieldTrace xfldElem( xfldGroup.basis() );
  ElementQFieldCell qfldElem( qfldGroup.basis() );

  static const int N = DLA::VectorSize<T>::M;

  const int order = max(xfldElem.order(), qfldElem.order());
  int nRefine = max(order-1,0);
  ReferenceElementMesh<Topology> refMesh(nRefine);
  const int nRefnode = refMesh.nodes.size();
  const int nRefelem = refMesh.elems.size();

  int nElem_global;
  std::map<int,int> globalElemMap;
  continuousElementMap( comm, cellIDs, xfldGroup, nElem_global, globalElemMap);

  int nElemLocal = xfldGroup.nElem();

  // tecplot does not like empty groups
  if ( nElem_global == 0 ) return;
  if ( nElemLocal == 0 && partitioned ) return;

  if (partitioned) nElem_global = nElemLocal;

  int comm_rank = comm.rank();

  if (comm_rank == 0 || partitioned)
    fprintf( fp, "ZONE T=\"grid\", N=%d, E=%d, F=FEPOINT, ET=%s\n",
             nRefnode*nElem_global, nRefelem*nElem_global, TecTopo<Topology>::name() );

  // grid coordinates and solution

  typename ElementQFieldCell::RefCoordType sRefCell;
  typename ElementXFieldTrace::VectorX X;
  T q;

#ifdef SANS_MPI
  if (!partitioned)
  {
    int comm_size = comm.size();

    // maximum element chunk size that rank 0 will write at any given time
    int Elemchunk = nElem_global / comm_size + nElem_global % comm_size;

    // send one chunk of elements at a time to rank 0
    for (int rank = 0; rank < comm_size; rank++)
    {
      int elemLow  =  rank   *Elemchunk;
      int elemHigh = (rank+1)*Elemchunk;

      std::map<int,std::vector<DLA::VectorS<PhysDim::D+N,double>>> buffer;

      // loop over elements within group
      for (int elem = 0; elem < nElemLocal; elem++)
      {
        int elemID = globalElemMap[cellIDs[elem]];

        if (elemID >= elemLow && elemID < elemHigh &&
            xfldGroup.associativity(elem).rank() == comm_rank)
        {
          // copy global grid/solution DOFs to element
          xfldGroup.getElement( xfldElem, elem );
          qfldGroup.getElement( qfldElem, elem );

          std::vector<DLA::VectorS<PhysDim::D+N,double>> nodeValues(nRefnode);

          for ( int i = 0; i < nRefnode; i++ )
          {
            sRefCell = refMesh.nodes[i];

            xfldElem.eval( sRefCell, X );
            qfldElem.eval( sRefCell, q );

            int n = 0;
            for (int k = 0; k < PhysDim::D; k++)
              nodeValues[i][n++] = X[k];
            for (int k = 0; k < N; k++)
              nodeValues[i][n++] = DLA::index(q,k);
          }

          buffer[elemID] = std::move(nodeValues);
        }
      }


      if (comm_rank == 0)
      {
        std::vector<std::map<int,std::vector<DLA::VectorS<PhysDim::D+N,double>>>> bufferOnRank;
        boost::mpi::gather(comm, buffer, bufferOnRank, 0 );

        // collapse down the buffer from all other ranks
        for (std::size_t i = 1; i < bufferOnRank.size(); i++)
          buffer.insert(bufferOnRank[i].begin(), bufferOnRank[i].end());

        //Write the node data
        for ( const auto& nodesPair : buffer)
          for ( int i = 0; i < nRefnode; i++ )
          {
            for (int n = 0; n < PhysDim::D+N; n++)
              fprintf( fp, "%22.15e ", nodesPair.second[i][n] );
            fprintf( fp, "\n" );
          }
      }
      else // send the buffer to rank 0
        boost::mpi::gather(comm, buffer, 0 );

    } // for rank

  }
  else // if partitioned
  {
#endif

    // loop over elements within group
    for (int elem = 0; elem < nElemLocal; elem++)
    {
      // copy global grid/solution DOFs to element
      xfldGroup.getElement( xfldElem, elem );
      qfldGroup.getElement( qfldElem, elem );

      for ( int i = 0; i < nRefnode; i++ )
      {
        sRefCell = refMesh.nodes[i];

        xfldElem.eval( sRefCell, X );
        qfldElem.eval( sRefCell, q );

        for (int k = 0; k < PhysDim::D; k++)
          fprintf( fp, "%22.15e ", X[k] );
        for (int k = 0; k < N; k++)
          fprintf( fp, "%22.15e ", DLA::index(q,k) );
        fprintf( fp, "\n" );
      }
    }

#ifdef SANS_MPI
  } // if !partitioned
#endif

  // cell-to-node connectivity

  if (comm_rank == 0 || partitioned)
    for (int elem = 0; elem < nElem_global; elem++)
    {
      int offset = nRefnode*elem;
      for (int relem = 0; relem < nRefelem; relem++)
      {
        for (int n = 0; n < Topology::NNode; n++ )
          fprintf( fp, "%d ", offset + refMesh.elems[relem][n] + 1 ); // +1 for one based indexing
        fprintf( fp, "\n" );
      }
    }
}
#endif

//----------------------------------------------------------------------------//
template<template <class> class PDETraitsSize, class PDETraitsModel, class PhysDim, class TopoDim, class TopologyCell, class TopologyTrace>
void
output_Tecplot_TraceCellGroup( const PDERANSSA3D<PDETraitsSize, PDETraitsModel>& pde, const int groupTrace,
                               mpi::communicator& comm, const std::vector<int>& boundaryTraceIDs, const bool partitioned,
                               const typename XField<PhysDim, TopoDim                      >::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
                               const typename XField<PhysDim, TopoDim                      >::template FieldCellGroupType<TopologyCell>& xfldCellL,
                               const typename  Field<PhysDim, TopoDim, DLA::VectorS<6,Real>>::template FieldCellGroupType<TopologyCell>& qfldCellL,
                               const typename FieldLift< PhysDim, TopoDim, DLA::VectorS<PhysDim::D,DLA::VectorS<6,Real>> >::
                                  template FieldCellGroupType<TopologyCell>& rfldCellL,
                               const DiscretizationDGBR2& disc,
                               FILE* fp )
{
  typedef PDERANSSA3D<PDETraitsSize, PDETraitsModel> PDE;
  typedef typename PDE::template ArrayQ<Real> T;
  typedef typename XField<PhysDim, TopoDim   >::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename XField<PhysDim, TopoDim   >::template FieldCellGroupType<TopologyCell> XFieldCellGroupType;
  typedef typename  Field<PhysDim, TopoDim, T>::template FieldCellGroupType<TopologyCell> QFieldCellGroupType;
  typedef typename FieldLift< PhysDim, TopoDim, DLA::VectorS<PhysDim::D,T> >::template FieldCellGroupType<TopologyCell> RFieldCellGroupType;

  typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTrace;
  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldCell;
  typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldCell;
  typedef typename RFieldCellGroupType::template ElementType<> ElementRFieldCell;

  // element field variables
  ElementXFieldTrace xfldElemTrace( xfldTrace.basis() );
  ElementXFieldCell xfldElemCell( xfldCellL.basis() );
  ElementQFieldCell qfldElemCell( qfldCellL.basis() );
  ElementRFieldCell rfldElemCell( rfldCellL.basis() );

  static const int N = DLA::VectorSize<T>::M;

  const int order = max(xfldElemTrace.order(), qfldElemCell.order());
  int nRefine = max(order-1,0);
  ReferenceElementMesh<TopologyTrace> refMesh(nRefine);
  const int nRefnode = refMesh.nodes.size();
  const int nRefelem = refMesh.elems.size();

  int nElem_global;
  std::map<int,int> globalElemMap;
  continuousElementMap( comm, boundaryTraceIDs, xfldTrace, nElem_global, globalElemMap);

  int nElemLocal = xfldTrace.nElem();

  // tecplot does not like empty groups
  if ( nElem_global == 0 ) return;
  if ( nElemLocal == 0 && partitioned ) return;

  if (partitioned) nElem_global = nElemLocal;

  int comm_rank = comm.rank();

  if (comm_rank == 0 || partitioned)
    fprintf( fp, "ZONE T=\"Boundary %d\", N=%d, E=%d, F=FEPOINT, ET=%s\n",
             groupTrace, nRefnode*nElem_global, nRefelem*nElem_global, TecTopo<TopologyTrace>::name() );

  typename ElementQFieldCell::RefCoordType sRefCell;
  typename ElementXFieldTrace::RefCoordType sRefTrace;
  typename ElementXFieldTrace::VectorX X, nL;
  T q;

#ifdef SANS_MPI
  T fv=0, hv=0, gv=0;
  Real F[PhysDim::D];
  DLA::VectorS<3, T> gradq = 0, rlift = 0;

  if (!partitioned)
  {
    int comm_size = comm.size();

    // maximum element chunk size that rank 0 will write at any given time
    int Elemchunk = nElem_global / comm_size + nElem_global % comm_size;

    // send one chunk of elements at a time to rank 0
    for (int rank = 0; rank < comm_size; rank++)
    {
      int elemLow  =  rank   *Elemchunk;
      int elemHigh = (rank+1)*Elemchunk;

      std::map<int,std::vector<DLA::VectorS<PhysDim::D+N+2,double>>> buffer;

      // loop over elements within group
      for (int elem = 0; elem < nElemLocal; elem++)
      {
        int elemID = globalElemMap[boundaryTraceIDs[elem]];

        if (elemID >= elemLow && elemID < elemHigh &&
            xfldTrace.associativity(elem).rank() == comm_rank)
        {
           const int elemL = xfldTrace.getElementLeft( elem );
           CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

           xfldTrace.getElement( xfldElemTrace, elem );
           xfldCellL.getElement( xfldElemCell, elemL );
           qfldCellL.getElement( qfldElemCell, elemL );
           rfldCellL.getElement( rfldElemCell, elemL, canonicalTraceL.trace );

           Real eta = disc.viscousEta( xfldElemTrace, xfldElemCell, xfldElemCell, canonicalTraceL.trace, canonicalTraceL.trace );

           std::vector<DLA::VectorS<PhysDim::D+N+2,double>> nodeValues(nRefnode);

           for ( int i = 0; i < nRefnode; i++ )
           {
             sRefTrace = refMesh.nodes[i];

             // adjacent cell-element reference coords
             TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyCell>::eval( canonicalTraceL, sRefTrace, sRefCell );

             xfldElemTrace.eval( sRefTrace, X );
             qfldElemCell.eval( sRefCell, q );
             rfldElemCell.eval( sRefCell, rlift );

             xfldElemCell.evalGradient( sRefCell, qfldElemCell, gradq );

             xfldElemTrace.unitNormal( sRefTrace, nL );

             int n = 0;
             for (int k = 0; k < PhysDim::D; k++)
               nodeValues[i][n++] = X[k];
             for (int k = 0; k < N; k++)
               nodeValues[i][n++] = q[k];

             Real rho, u, v, w, t;

             pde.variableInterpreter().eval( q, rho, u, v, w, t );

             gradq += eta*rlift;

             fv = gv = hv = 0;
             pde.fluxViscous(X[0], X[1], X[2], 0,
                             q, gradq[0], gradq[1], gradq[2],
                             fv, gv, hv);

             Real ntx=0, nty=0, ntz=0;
             pde.variableInterpreter().evalSAGradient( q, gradq[0], ntx );
             pde.variableInterpreter().evalSAGradient( q, gradq[1], nty );
             pde.variableInterpreter().evalSAGradient( q, gradq[2], ntz );


             // remove the normal component of the momentum flux
             Real Fvn = fv[PDE::ixMom]*nL[0] + gv[PDE::iyMom]*nL[1] + hv[PDE::izMom]*nL[2];
             F[0] = fv[PDE::ixMom] - Fvn*nL[0];
             F[1] = gv[PDE::iyMom] - Fvn*nL[1];
             F[2] = hv[PDE::izMom] - Fvn*nL[2];

             // compute the wall shear stress
             Real Tau_wall = 0;
             for (int d = 0; d < PhysDim::D; d++)
               Tau_wall += F[d]*F[d];
             Tau_wall = sqrt(Tau_wall);

             Real mu = pde.viscosityModel().viscosity( t );

             Real nu = mu/rho;

             Real u_tau = sqrt(Tau_wall/rho);

             Real y = xfldElemCell.volume()/xfldElemTrace.area();

             Real yplus = u_tau*y/nu;

             // normal vector points out of the domain, hence negation
             Real turb_ind = -1/(pde.vk()*u_tau) * (ntx*nL[0] + nty*nL[1] + ntz*nL[2]);

             nodeValues[i][n++] = yplus;
             nodeValues[i][n++] = turb_ind;
           }

          buffer[elemID] = std::move(nodeValues);
        }
      }


      if (comm_rank == 0)
      {
        std::vector<std::map<int,std::vector<DLA::VectorS<PhysDim::D+N+2,double>>>> bufferOnRank;
        boost::mpi::gather(comm, buffer, bufferOnRank, 0 );

        // collapse down the buffer from all other ranks
        for (std::size_t i = 1; i < bufferOnRank.size(); i++)
          buffer.insert(bufferOnRank[i].begin(), bufferOnRank[i].end());

        //Write the node data
        for ( const auto& nodesPair : buffer)
          for ( int i = 0; i < nRefnode; i++ )
          {
            for (int n = 0; n < PhysDim::D+N+2; n++)
              fprintf( fp, "%22.15e ", nodesPair.second[i][n] );
            fprintf( fp, "\n" );
          }
      }
#ifndef __clang_analyzer__
      else // send the buffer to rank 0
        boost::mpi::gather(comm, buffer, 0 );
#endif

    } // for rank

  }
  else // if partitioned
  {
#endif

  SANS_DEVELOPER_EXCEPTION("Not implemented");

  for (int elem = 0; elem < nElemLocal; elem++)
  {
    const int elemL = xfldTrace.getElementLeft( elem );
    CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

    xfldTrace.getElement( xfldElemTrace, elem );
    qfldCellL.getElement( qfldElemCell, elemL );

    for ( int i = 0; i < nRefnode; i++ )
    {
      sRefTrace = refMesh.nodes[i];

      // adjacent cell-element reference coords
      TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyCell>::eval( canonicalTraceL, sRefTrace, sRefCell );

      xfldElemTrace.eval( sRefTrace, X );
      qfldElemCell.eval( sRefCell, q );

      for (int k = 0; k < PhysDim::D; k++)
        fprintf( fp, "%22.15e ", X[k] );
      for (int k = 0; k < N; k++)
        fprintf( fp, "%22.15e ", q[k] );
      fprintf( fp, "\n" );

    }
  }

#ifdef SANS_MPI
  } // if !partitioned
#endif

  // cell-to-node connectivity

  if (comm_rank == 0 || partitioned)
    for (int elem = 0; elem < nElem_global; elem++)
    {
      int offset = nRefnode*elem;
      for (int relem = 0; relem < nRefelem; relem++)
      {
        for (int n = 0; n < TopologyTrace::NNode; n++ )
          fprintf( fp, "%d ", offset + refMesh.elems[relem][n] + 1 ); // +1 for one based indexing
        fprintf( fp, "\n" );
      }
    }
}

template<class QType>
struct StateNames;

template<>
struct StateNames<QTypePrimitiveRhoPressure>
{
  static std::vector<std::string> names() { return {"rho", "u", "v", "w", "p", "nt"}; }
};



//----------------------------------------------------------------------------//
template <template <class> class PDETraitsSize, class PDETraitsModel>
void
output_Tecplot_RANS_Wall( const PDERANSSA3D<PDETraitsSize, PDETraitsModel>& pde,
                          const Field< PhysD3, TopoD3, DLA::VectorS<6,Real>>& qfld,
                          const FieldLift< PhysD3, TopoD3, DLA::VectorS<PhysD3::D,DLA::VectorS<6,Real>> >& rfld,
                          const DiscretizationDGBR2& disc,
                          const std::string& filename,
                          const bool partitioned )
{
  typedef PDERANSSA3D<PDETraitsSize, PDETraitsModel> PDE;
  typedef typename PDE::QType QType;

  const std::vector<std::string> state_names = StateNames<QType>::names();

  FILE* fp = nullptr;

  if (qfld.comm()->rank() == 0 || partitioned)
  {
    std::cout << "output_Tecplot: filename = " << filename << std::endl;
    fp = fopen( filename.c_str(), "w" );
    if (fp == NULL)
      SANS_DEVELOPER_EXCEPTION("output_Tecplot<PhysD3,TopoD3> - Error opening file: %s", filename.c_str());

    fprintf( fp, "\"\"\n" );
    fprintf( fp, "VARIABLES = \"X\", \"Y\", \"Z\"" );

    for (std::size_t k = 0; k < state_names.size(); k++)
      fprintf( fp, ", \"%s\"", state_names[k].c_str());

    fprintf( fp, ", \"y+\", \"TurbInd\"");

    fprintf( fp, "\n" );
  }

  const XField<PhysD3,TopoD3>& xgrid = qfld.getXField();

  // loop over trace boundary groups
  for (int groupTrace = 0; groupTrace < xgrid.nBoundaryTraceGroups(); groupTrace++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(groupTrace).topoTypeID() == typeid(Triangle) )
    {
      // determine topology for Left cell group
      int groupL = xgrid.template getBoundaryTraceGroup<Triangle>(groupTrace).getGroupLeft();

      if ( xgrid.getCellGroupBase(groupL).topoTypeID() == typeid(Tet) )
        output_Tecplot_TraceCellGroup<PDETraitsSize,PDETraitsModel, PhysD3, TopoD3, Tet, Triangle>(
            pde, groupTrace,
            *xgrid.comm(), xgrid.boundaryTraceIDs(groupTrace), partitioned,
            xgrid.template getBoundaryTraceGroup<Triangle>(groupTrace),
            xgrid.template getCellGroup<Tet>(groupL),
            qfld.template getCellGroup<Tet>(groupL),
            rfld.template getCellGroup<Tet>(groupL), disc, fp );
      else
        SANS_DEVELOPER_EXCEPTION("Unknown topology");
    }
    else if ( xgrid.getBoundaryTraceGroupBase(groupTrace).topoTypeID() == typeid(Quad) )
    {
      // determine topology for Left cell group
      int groupL = xgrid.template getBoundaryTraceGroup<Triangle>(groupTrace).getGroupLeft();

      if ( xgrid.getCellGroupBase(groupL).topoTypeID() == typeid(Hex) )
        output_Tecplot_TraceCellGroup<PDETraitsSize,PDETraitsModel, PhysD3, TopoD3, Hex, Quad>(
            pde, groupTrace,
            *xgrid.comm(), xgrid.boundaryTraceIDs(groupTrace), partitioned,
            xgrid.template getBoundaryTraceGroup<Quad>(groupTrace),
            xgrid.template getCellGroup<Hex>(groupL),
            qfld.template getCellGroup<Hex>(groupL),
            rfld.template getCellGroup<Hex>(groupL), disc, fp );
      else
        SANS_DEVELOPER_EXCEPTION("Unknown topology");
    }
    else
      SANS_DEVELOPER_EXCEPTION("Unknown topology");
  }

#if 0
  // loop over trace boundary groups
  for (int group = 0; group < qfld.nBoundaryTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      output_Tecplot_TraceGroup<T, PhysD3, TopoD3, Triangle>( group, xgrid.template getBoundaryTraceGroup<Triangle>(group),
                                                              qfld.template getBoundaryTraceGroup<Triangle>(group), fp );
    }
    else if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Quad) )
    {
      output_Tecplot_TraceGroup<T, PhysD3, TopoD3, Quad>( group, xgrid.template getBoundaryTraceGroup<Quad>(group),
                                                          qfld.template getBoundaryTraceGroup<Quad>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD3,TopoD3>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }
#endif

  if (qfld.comm()->rank() == 0 || partitioned)
    fclose( fp );
}

// Explicit instantiation

typedef QTypePrimitiveRhoPressure QType;
typedef ViscosityModel_Sutherland ViscosityModelType;
typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
typedef PDERANSSA3D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;

template void
output_Tecplot_RANS_Wall( const PDEClass& pde,
                          const Field< PhysD3, TopoD3, DLA::VectorS<6,Real>>& qfld,
                          const FieldLift< PhysD3, TopoD3, DLA::VectorS<PhysD3::D,DLA::VectorS<6,Real>> >& rfld,
                          const DiscretizationDGBR2& disc,
                          const std::string& filename,
                          const bool partitioned );


} //namespace SANS
