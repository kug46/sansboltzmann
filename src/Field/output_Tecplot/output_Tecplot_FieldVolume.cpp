// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define OUTPUT_TECPLOT_FIELD_INSTANTIATE
#include "output_Tecplot_Field_impl.h"

#include "Field/XFieldVolume.h"

#include "Field/FieldVolume.h"
#include "Field/FieldVolume_CG_Cell.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template<>
void
output_Tecplot( const XField<PhysD3,TopoD3>& xgrid, const std::string& filename )
{
  std::cout << "output_Tecplot: filename = " << filename << std::endl;
  FILE* fp = fopen( filename.c_str(), "w" );
  if (fp == NULL)
    SANS_DEVELOPER_EXCEPTION("output_Tecplot<PhysD3,TopoD3> - Error opening file: %s", filename.c_str());

  fprintf( fp, "\"\"\n" );
  fprintf( fp, "VARIABLES = \"X\", \"Y\", \"Z\", \"Native Index\", \"Node Rank\", \"Cell Rank\"\n" );

  const int ngroup = xgrid.nCellGroups();
  std::map<int,int> dofRank;

  int order = xgrid.getCellGroupBase(0).order();
  for (int group = 0; group < ngroup; group++)
    SANS_ASSERT(xgrid.getCellGroupBase(group).order() == order);

  Field_CG_Cell<PhysD3,TopoD3,Real> qfld(xgrid, order, BasisFunctionCategory_Lagrange);

  // loop over element groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over elements of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Tet) )
    {
      dofRank_CellGroup<PhysD3, TopoD3, Tet>( xgrid.local2nativeDOFmap(), xgrid.getCellGroup<Tet>(group),
                                              qfld, qfld.getCellGroup<Tet>(group), dofRank );
    }
    else if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Hex) )
    {
      dofRank_CellGroup<PhysD3, TopoD3, Hex>( xgrid.local2nativeDOFmap(), xgrid.getCellGroup<Hex>(group),
                                              qfld, qfld.getCellGroup<Hex>(group), dofRank );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD1,TopoD1>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }


  // loop over cell groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Tet) )
    {
      if (group == 0)
        output_Tecplot_CellGroup_Nodes<PhysD3, TopoD3, Tet>( xgrid.local2nativeDOFmap(), xgrid.getCellGroup<Tet>(group), dofRank, fp );
      else
        output_Tecplot_CellGroup_XField<PhysD3, TopoD3, Tet>( xgrid.getCellGroup<Tet>(group), fp );
    }
    else if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Hex) )
    {
      if (group == 0)
        output_Tecplot_CellGroup_Nodes<PhysD3, TopoD3, Hex>( xgrid.local2nativeDOFmap(), xgrid.getCellGroup<Hex>(group), dofRank, fp );
      else
        output_Tecplot_CellGroup_XField<PhysD3, TopoD3, Hex>( xgrid.getCellGroup<Hex>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD3,TopoD3>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  // loop over trace boundary groups
  for (int group = 0; group < xgrid.nBoundaryTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      output_Tecplot_TraceGroup_XField<PhysD3, TopoD3, Triangle>("Boundary", group, xgrid.getBoundaryTraceGroup<Triangle>(group), fp );
    }
    else if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Quad) )
    {
      output_Tecplot_TraceGroup_XField<PhysD3, TopoD3, Quad>("Boundary", group, xgrid.getBoundaryTraceGroup<Quad>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD3,TopoD3>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  // loop over ghost trace boundary groups
  for (int group = 0; group < xgrid.nGhostBoundaryTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      output_Tecplot_TraceGroup_XField<PhysD3, TopoD3, Triangle>("Ghost", group, xgrid.getGhostBoundaryTraceGroup<Triangle>(group), fp );
    }
    else if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Quad) )
    {
      output_Tecplot_TraceGroup_XField<PhysD3, TopoD3, Quad>("Ghost", group, xgrid.getGhostBoundaryTraceGroup<Quad>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD3,TopoD3>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  fclose( fp );
}

//----------------------------------------------------------------------------//
template<class T>
void
output_Tecplot( const Field< PhysD3, TopoD3, T >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names,
                const bool partitioned )
{
  FILE* fp = nullptr;

  if (qfld.comm()->rank() == 0 || partitioned)
  {
    std::cout << "output_Tecplot: filename = " << filename << std::endl;
    fp = fopen( filename.c_str(), "w" );
    if (fp == NULL)
      SANS_RUNTIME_EXCEPTION("output_Tecplot<PhysD3,TopoD3> - Error opening file: %s", filename.c_str());

    fprintf( fp, "\"\"\n" );
    fprintf( fp, "VARIABLES = \"X\", \"Y\", \"Z\"" );

    static const int N = DLA::VectorSize<T>::M;
    if (state_names.empty())
    {
      for (int k = 0; k < N; k++)
        fprintf( fp, ", \"q%d\"", k+1 );
    }
    else
    {
      SANS_ASSERT((int) state_names.size() == N);
      for (int k = 0; k < N; k++)
        fprintf( fp, ", \"%s\"", state_names[k].c_str());
    }
    fprintf( fp, "\n" );
  }

  const XField<PhysD3,TopoD3>& xgrid = qfld.getXField();

  const int ngroup = qfld.nCellGroups();

  // loop over cell groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Tet) )
    {
      output_Tecplot_CellGroup<T, PhysD3, TopoD3, Tet>( *xgrid.comm(), xgrid.cellIDs(group), partitioned,
                                                        xgrid.template getCellGroup<Tet>(group),
                                                        qfld.template getCellGroup<Tet>(group), fp );
    }
    else if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Hex) )
    {
      output_Tecplot_CellGroup<T, PhysD3, TopoD3, Hex>( *xgrid.comm(), xgrid.cellIDs(group), partitioned,
                                                        xgrid.template getCellGroup<Hex>(group),
                                                        qfld.template getCellGroup<Hex>(group), fp );
    }
    else
      SANS_DEVELOPER_EXCEPTION("Unknown topology");
  }

  if (ngroup > 0)
  {
    // loop over trace boundary groups
    for (int groupTrace = 0; groupTrace < xgrid.nBoundaryTraceGroups(); groupTrace++)
    {

      // dispatch integration over cell of group
      if ( xgrid.getBoundaryTraceGroupBase(groupTrace).topoTypeID() == typeid(Triangle) )
      {
        // determine topology for Left cell group
        int groupL = xgrid.template getBoundaryTraceGroup<Triangle>(groupTrace).getGroupLeft();

        if ( xgrid.getCellGroupBase(groupL).topoTypeID() == typeid(Tet) )
          output_Tecplot_TraceCellGroup<T, PhysD3, TopoD3, Tet, Triangle>( groupTrace,
                                                                           *xgrid.comm(), xgrid.boundaryTraceIDs(groupTrace), partitioned,
                                                                           xgrid.template getBoundaryTraceGroup<Triangle>(groupTrace),
                                                                            qfld.template getCellGroup<Tet>(groupL), fp );
        else
          SANS_DEVELOPER_EXCEPTION("Unknown topology");
      }
      else if ( xgrid.getBoundaryTraceGroupBase(groupTrace).topoTypeID() == typeid(Quad) )
      {
        // determine topology for Left cell group
        int groupL = xgrid.template getBoundaryTraceGroup<Quad>(groupTrace).getGroupLeft();

        if ( xgrid.getCellGroupBase(groupL).topoTypeID() == typeid(Hex) )
          output_Tecplot_TraceCellGroup<T, PhysD3, TopoD3, Hex, Quad>( groupTrace,
                                                                       *xgrid.comm(), xgrid.boundaryTraceIDs(groupTrace), partitioned,
                                                                       xgrid.template getBoundaryTraceGroup<Quad>(groupTrace),
                                                                        qfld.template getCellGroup<Hex>(groupL), fp );
        else
          SANS_DEVELOPER_EXCEPTION("Unknown topology");
      }
      else
        SANS_DEVELOPER_EXCEPTION("Unknown topology");
    }
  }

  if (ngroup  == 0)
  {
    SANS_ASSERT_MSG(qfld.comm()->size() == 1, "Pure trace fields cannot be written in parallel yet");

    // loop over trace boundary groups
    for (int group = 0; group < qfld.nBoundaryTraceGroups(); group++)
    {
      const int groupGlobal = qfld.getGlobalBoundaryTraceGroupMap(group);

      // dispatch integration over cell of group
      if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Triangle) )
      {
        output_Tecplot_TraceGroup<T, PhysD3, TopoD3, Triangle>( groupGlobal, xgrid.template getBoundaryTraceGroup<Triangle>(groupGlobal),
                                                                qfld.template getBoundaryTraceGroup<Triangle>(group), fp );
      }
      else if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Quad) )
      {
        output_Tecplot_TraceGroup<T, PhysD3, TopoD3, Quad>( groupGlobal, xgrid.template getBoundaryTraceGroup<Quad>(groupGlobal),
                                                            qfld.template getBoundaryTraceGroup<Quad>(group), fp );
      }
      else
      {
        const char msg[] = "Error in output_Tecplot(XField<PhysD3,TopoD3>): unknown topology\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }
  }

  if (qfld.comm()->rank() == 0 || partitioned)
    fclose( fp );
}

//Explicitly instantiate the function
template void
output_Tecplot( const Field< PhysD3, TopoD3, Real >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD3, TopoD3, DLA::VectorS<1,Real> >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD3, TopoD3, DLA::VectorS<2,Real> >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD3, TopoD3, DLA::VectorS<3,Real> >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD3, TopoD3, DLA::VectorS<4,Real> >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD3, TopoD3, DLA::VectorS<5,Real> >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD3, TopoD3, DLA::VectorS<6,Real> >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD3, TopoD3, DLA::VectorS<7,Real> >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
} //namespace SANS
