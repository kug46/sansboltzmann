// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(OUTPUT_TECPLOT_LIFT_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "Field/output_Tecplot.h"
#include "TecTopo.h"

#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"

#include "Field/XField.h"

#include "Field/FieldLift.h"

#include "Field/Element/ReferenceElementMesh.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"
#include "MPI/continuousElementMap.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_to_all.hpp>
#include <boost/mpi/collectives/all_reduce.hpp>
#endif

namespace SANS
{

//----------------------------------------------------------------------------//
template< class T, class PhysDim, class TopoDim, class Topology>
void
output_Tecplot_CellGroup( const typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology>& xfld,
                          const typename FieldLift< PhysDim, TopoDim, DLA::VectorS<PhysDim::D,T> >::template FieldCellGroupType<Topology>& rfld,
                          FILE* fp )
{
  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
  // typedef typename FieldLift<PhysDim, TopoDim, DLA::VectorS<PhysDim::D,T> >::template FieldCellGroupType<Topology> RFieldCellGroupType;

  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef ElementLift<DLA::VectorS<PhysDim::D,T>, TopoDim, Topology> ElementRFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfld.basis() );
  ElementRFieldClass rfldElems( rfld.basis() );

  std::vector<Real> phi(rfldElems.nDOFElem()); // vector of basis functions

  static const int N = DLA::VectorSize<T>::M;

  const int order = max(xfldElem.order(), rfldElems.order());
  int nRefine = max(order-1,0);
  ReferenceElementMesh<Topology> refMesh(nRefine);
  const int nRefnode = refMesh.nodes.size();
  const int nRefelem = refMesh.elems.size();

  int nelem = xfld.nElem();

  // tecplot does not like empty groups
  if ( nelem == 0 ) return;

  fprintf( fp, "ZONE T=\"grid\", N=%d, E=%d, F=FEPOINT, ET=%s\n", nRefnode*nelem, nRefelem*nelem, TecTopo<Topology>::name() );

  // grid coordinates and solution

  typename ElementRFieldClass::RefCoordType sRefCell;
  typename ElementXFieldClass::VectorX X;
  DLA::VectorS<PhysDim::D,T> r;
  T q;

  // loop over elements within group
  for (int elem = 0; elem < nelem; elem++)
  {
    // copy global grid/solution DOFS to element
    xfld.getElement( xfldElem, elem );
    rfld.getElement( rfldElems, elem );

    for (int i = 0; i < nRefnode; i++)
    {
      sRefCell = refMesh.nodes[i];

      rfldElems.evalBasis( sRefCell, phi.data(), phi.size() );
      xfldElem.eval( sRefCell, X );
      q = 0;

      for (int trace = 0; trace < Topology::NTrace; trace++)
      {
        rfldElems[trace].evalFromBasis( phi.data(), phi.size(), r );

        T tmp = 0;
        for (int k = 0; k < DLA::VectorSize<T>::M; k++) // loop over state components
        {
          for (int d = 0; d < PhysDim::D; d++)
            DLA::index(tmp,k) += DLA::index(r[d],k)*DLA::index(r[d],k);
          DLA::index(tmp,k) = pow(DLA::index(tmp,k),0.5); // L2 norm of the vector
        }
        q+=tmp; // accumulates the norms of each state component
      }
      for (int k = 0; k < PhysDim::D; k++)
        fprintf( fp, "%22.15e ", X[k] );
      for (int k = 0; k < N; k++)
        fprintf( fp, "%22.15e ", DLA::index(q,k) );
      fprintf( fp, "\n" );
    }
  }

  // cell-to-node connectivity

  for (int elem = 0; elem < nelem; elem++)
  {
    int offset = nRefnode*elem;
    for (int relem = 0; relem < nRefelem; relem++)
    {
      for (int n = 0; n < Topology::NNode; n++ )
        fprintf( fp, "%d ", offset + refMesh.elems[relem][n] + 1 ); // +1 for one based indexing
      fprintf( fp, "\n" );
    }
  }
}

} //namespace SANS
