// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "Field/output_Tecplot.h"
#include "TecTopo.h"
#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "pde/FullPotential/PDELinearizedIncompressiblePotential3D.h"

#include "Field/XFieldVolume.h"
#include "Field/FieldVolume.h"

#define RELEASE_TECPLOT

namespace SANS
{


//----------------------------------------------------------------------------//
// output as P1
//
template<class Topology>
void
output_Tecplot_CellGroup_Nodes_LIP(
    const PDELinearizedIncompressiblePotential3D<Real>& pde,
    const typename XField<PhysD3, TopoD3>::FieldCellGroupType<Topology>& xfld,
    const typename Field< PhysD3, TopoD3, Real >::FieldCellGroupType<Topology>& qfld,
    FILE* fp )
{
  typedef typename XField<PhysD3, TopoD3>::FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename Field<PhysD3, TopoD3, Real >::FieldCellGroupType<Topology> QFieldCellGroupType;

  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

  typedef typename PDELinearizedIncompressiblePotential3D<Real>::ArrayQ<Real> ArrayQ;

  int nnode = xfld.nDOF();
  int nelem = xfld.nElem();

  // tecplot does not like empty groups
  if ( nelem == 0 ) return;

  // Node values
  // x, y, z, phi
  int nNodeVar = 4;
  // Element values
  // u, v, w, Cp
  int nElemVar = 4;

  fprintf( fp, "ZONE T=\"grid\", N=%d, E=%d, F=FEBLOCK, ET=%s", nnode, nelem, TecTopo<Topology>::name() );

  fprintf( fp, ", VARLOCATION=([");
  int ivar = 1;
  for (int i = 0; i < nNodeVar; i++)
  {
    fprintf( fp, "%d", ivar++);
    if (i < nNodeVar-1 )
      fprintf( fp, ",");
  }
  fprintf( fp, "]=NODAL,[");
  for (int i = 0; i < nElemVar; i++)
  {
    fprintf( fp, "%d", ivar++);
    if (i < nElemVar-1 )
      fprintf( fp, ",");
  }
  fprintf( fp, "]=CELLCENTERED)\n");

  // grid coordinates

  for (int d = 0; d < PhysD3::D; d++)
  {
    for (int n = 0; n < nnode; n++)
    {
      if (n % 5 == 0) fprintf( fp, "\n");
      fprintf( fp, "%22.15e ", xfld.DOF(n)[d] );
    }
    fprintf( fp, "\n");
  }

  // phi

  for (int n = 0; n < nnode; n++)
  {
    if (n % 5 == 0) fprintf( fp, "\n");
    fprintf( fp, "%14.7e ", qfld.DOF(n) );
  }
  fprintf( fp, "\n");

  // element field variables
  typename ElementXFieldClass::VectorX X;
  ArrayQ magq = 0, Cp = 0;
  DLA::VectorS<3, ArrayQ > gradq = 0;
  DLA::VectorS<3, Real > U, V;

  U = pde.freestream();
  Real Vinf2 = dot(U,U);

  ElementXFieldClass xfldElem( xfld.basis() );
  ElementQFieldClass qfldElem( qfld.basis() );

  // u

  for (int elem = 0; elem < nelem; elem++)
  {
    xfld.getElement( xfldElem, elem );
    qfld.getElement( qfldElem, elem );
    xfldElem.evalGradient( Topology::centerRef, qfldElem, gradq );
    V = U + gradq;
    if (elem % 5 == 0) fprintf( fp, "\n");
    fprintf( fp, "%14.7e ", V[0] );
  }
  fprintf( fp, "\n");

  // v

  for (int elem = 0; elem < nelem; elem++)
  {
    xfld.getElement( xfldElem, elem );
    qfld.getElement( qfldElem, elem );
    xfldElem.evalGradient( Topology::centerRef, qfldElem, gradq );
    V = U + gradq;
    if (elem % 5 == 0) fprintf( fp, "\n");
    fprintf( fp, "%14.7e ", V[1] );
  }
  fprintf( fp, "\n");

  // w

  for (int elem = 0; elem < nelem; elem++)
  {
    xfld.getElement( xfldElem, elem );
    qfld.getElement( qfldElem, elem );
    xfldElem.evalGradient( Topology::centerRef, qfldElem, gradq );
    V = U + gradq;
    if (elem % 5 == 0) fprintf( fp, "\n");
    fprintf( fp, "%14.7e ", V[2] );
  }
  fprintf( fp, "\n");

  // Cp

  for (int elem = 0; elem < nelem; elem++)
  {
    xfld.getElement( xfldElem, elem );
    qfld.getElement( qfldElem, elem );
    xfldElem.evalGradient( Topology::centerRef, qfldElem, gradq );
    magq = dot(gradq,gradq);
    Cp = (-0.5*magq - dot(gradq,U))/(0.5*Vinf2);
    if (elem % 5 == 0) fprintf( fp, "\n");
    fprintf( fp, "%14.7e ", Cp );
  }
  fprintf( fp, "\n");

  // cell-to-node connectivity

  int nodeMap[Topology::NNode];
  for (int elem = 0; elem < nelem; elem++)
  {
    xfld.associativity( elem ).getNodeGlobalMapping( nodeMap, Topology::NNode );
    for (int n = 0; n < Topology::NNode; n++ )
      fprintf( fp, "%d ", nodeMap[n]+1 );
    fprintf( fp, "\n" );
  }
}


//----------------------------------------------------------------------------//
// output as P1
//
#if 0
template<class Topology>
void
output_Tecplot_CellGroup_LIP( const PDELinearizedIncompressiblePotential3D<Real>& pde,
                              const typename XField<PhysD3, TopoD3>::FieldCellGroupType<Topology>& xfld,
                              const typename Field< PhysD3, TopoD3, Real >::FieldCellGroupType<Topology>& qfld,
                              FILE* fp )
{
  typedef typename XField<PhysD3, TopoD3>::FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename Field<PhysD3, TopoD3, Real >::FieldCellGroupType<Topology> QFieldCellGroupType;

  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

  typedef typename PDELinearizedIncompressiblePotential3D<Real>::ArrayQ<Real> ArrayQ;

  // element field variables
  ElementXFieldClass xfldElem( xfld.basis() );
  ElementQFieldClass qfldElem( qfld.basis() );

  int nelem = xfld.nElem();
  int nnode = Topology::NNode*nelem;
  fprintf( fp, "ZONE T=\"grid\", N=%d, E=%d, F=FEPOINT, ET=%s\n", nnode, nelem, TecTopo<Topology>::name() );

  // grid coordinates and solution

  //Real V2 = 0;
  typename ElementXFieldClass::RefCoordType stu;
  typename ElementXFieldClass::VectorX X;
  ArrayQ q, magq = 0, Cp = 0;
  DLA::VectorS<3, ArrayQ > gradq = 0;
  DLA::VectorS<3, Real > U, V;

  pde.freestream(U);
  Real Vinf2 = dot(U,U);

  // loop over elements within group
  for (int elem = 0; elem < nelem; elem++)
  {
    // copy global grid/solution DOFs to element
    xfld.getElement( xfldElem, elem );
    qfld.getElement( qfldElem, elem );

    for ( int i = 0; i < Topology::NNode; i++ )
    {
      stu = 0;
      if ( Topology::NNode == 4)
      {
        if (i > 0 ) stu[i-1] = 1;
      }
      else
      {
        if ( i == 1 ) stu = {1,0,0};
        if ( i == 2 ) stu = {1,1,0};
        if ( i == 3 ) stu = {0,1,0};
        if ( i == 4 ) stu = {0,0,1};
        if ( i == 5 ) stu = {1,0,1};
        if ( i == 6 ) stu = {1,1,1};
        if ( i == 7 ) stu = {0,1,1};
      }

      xfldElem.eval( stu, X );
      qfldElem.eval( stu, q );
      xfldElem.evalGradient( stu, qfldElem, gradq );
      magq = dot(gradq,gradq);
      V = U + gradq;
      //V2 = dot(V,V);
      Cp = (-0.5*magq - dot(gradq,U))/(0.5*Vinf2);
      //Cp = (0.5*Vinf2 - 0.5*V2)/(0.5*Vinf2);

      fprintf( fp, "%22.15e %22.15e %22.15e", X[0], X[1], X[2] );
      fprintf( fp, " %22.15e %22.15e %22.15e %22.15e", q, gradq[0], gradq[1], gradq[2] );
      fprintf( fp, " %14.7e %14.7e %14.7e", V[0], V[1], V[2] );
      fprintf( fp, " %14.7e", sqrt( magq ) );
      fprintf( fp, " %14.7e", q + U[0]*X[0] + U[1]*X[1] + U[2]*X[2] );
      fprintf( fp, " %14.7e", Cp );
      fprintf( fp, " %14.7e", -dot(gradq,U)/(0.5*Vinf2) );
      fprintf( fp, " %d", 0 );
      fprintf( fp, "\n" );
    }
  }

  // cell-to-node connectivity

  for (int elem = 0; elem < nelem; elem++)
  {
    for (int n = 0; n < Topology::NNode; n++ )
      fprintf( fp, "%d ", Topology::NNode*elem+n+1 );
    fprintf( fp, "\n" );
  }


}
#elif 0
template<class Topology>
void
output_Tecplot_CellGroup_LIP( const PDELinearizedIncompressiblePotential3D<Real>& pde,
                              const typename XField<PhysD3, TopoD3>::FieldCellGroupType<Topology>& xfld,
                              const typename Field< PhysD3, TopoD3, Real >::FieldCellGroupType<Topology>& qfld,
                              FILE* fp )
{
  typedef typename XField<PhysD3, TopoD3>::template FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename Field<PhysD3, TopoD3, Real >::template FieldCellGroupType<Topology> QFieldCellGroupType;

  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

  typedef typename PDELinearizedIncompressiblePotential3D<Real>::template ArrayQ<Real> ArrayQ;

  // element field variables
  ElementXFieldClass xfldElem( xfld.basis() );
  ElementQFieldClass qfldElem( qfld.basis() );

  int nelem = xfld.nElem();
  int nnode = xfld.nDOF();
  fprintf( fp, "ZONE T=\"grid\", N=%d, E=%d, F=FEPOINT, ET=%s\n", nnode, nelem, TecTopo<Topology>::name() );

  // grid coordinates and solution

  //Real V2 = 0;
  typename ElementXFieldClass::RefCoordType stu;
  typename ElementXFieldClass::VectorX X;
  ArrayQ q, magq = 0, Cp = 0;
  DLA::VectorS<3, ArrayQ > gradq = 0;
  DLA::VectorS<3, Real > U = 0, V = 0;

  pde.freestream(U);
  Real Vinf2 = dot(U,U);

  // loop over elements within group
  for (int node = 0; node < nnode; node++)
  {
    // copy global grid/solution DOFs
    X = xfld.DOF( node );
    q = qfld.DOF( node );

    magq = dot(gradq,gradq);
    V = U + gradq;
    //V2 = dot(V,V);
    Cp = (-0.5*magq - dot(gradq,U))/(0.5*Vinf2);
    //Cp = (0.5*Vinf2 - 0.5*V2)/(0.5*Vinf2);

    fprintf( fp, "%22.15e %22.15e %22.15e", X[0], X[1], X[2] );
    fprintf( fp, " %22.15e %22.15e %22.15e %22.15e", q, gradq[0], gradq[1], gradq[2] );
    fprintf( fp, " %14.7e %14.7e %14.7e", V[0], V[1], V[2] );
    fprintf( fp, " %14.7e", sqrt( magq ) );
    fprintf( fp, " %14.7e", q + U[0]*X[0] + U[1]*X[1] + U[2]*X[2] );
    fprintf( fp, " %14.7e", Cp );
    fprintf( fp, " %14.7e", -dot(gradq,U)/(0.5*Vinf2) );
    fprintf( fp, " %d", 0 );
    fprintf( fp, "\n" );
  }

  // cell-to-node connectivity

  int globalNodeMap[Topology::NNode];
  for (int elem = 0; elem < nelem; elem++)
  {
    qfld.associativity( elem ).getNodeGlobalMapping(globalNodeMap, Topology::NNode);
    for (int n = 0; n < Topology::NNode; n++)
      fprintf( fp, "%d ", globalNodeMap[n]+1);
    fprintf( fp, "\n");
  }


}
#endif
#if 0
template
void
output_Tecplot_CellGroup_LIP( const PDELinearizedIncompressiblePotential3D<Real>& pde,
                              const typename XField<PhysD3, TopoD3>::FieldCellGroupType<Tet>& xfld,
                              const typename Field< PhysD3, TopoD3, Real >::FieldCellGroupType<Tet>& qfld,
                              FILE* fp );

template
void
output_Tecplot_CellGroup_LIP( const PDELinearizedIncompressiblePotential3D<Real>& pde,
                              const typename XField<PhysD3, TopoD3>::FieldCellGroupType<Tet>& xfld,
                              const typename Field< PhysD3, TopoD3, Real >::FieldCellGroupType<Tet>& qfld,
                              FILE* fp );
#endif


#if 1

//----------------------------------------------------------------------------//
// output as P1
//
template<class TopologyTrace, class TopologyCell>
void
output_Tecplot_TraceGroup_LIP(
    const int group, const PDELinearizedIncompressiblePotential3D<Real>& pde,
    const XField<PhysD3,TopoD3>& xfld,
    const typename XField<PhysD3,TopoD3>::FieldTraceGroupType<TopologyTrace>& xfldTrace,
    const Field< PhysD3, TopoD3, Real >& qfld,
    const typename Field< PhysD3, TopoD3, Real >::FieldTraceGroupType<TopologyTrace>& qfldTrace,
    FILE* fp )
{
  typedef typename XField<PhysD3, TopoD3>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename Field<PhysD3, TopoD3, Real >::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;

  typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldClass;
  typedef typename QFieldTraceGroupType::template ElementType<> ElementQFieldClass;

  typedef typename PDELinearizedIncompressiblePotential3D<Real>::ArrayQ<Real> ArrayQ;

  typedef typename XField<PhysD3, TopoD3>::template FieldCellGroupType<TopologyCell> XFieldCellGroupTypeL;
  typedef typename Field<PhysD3, TopoD3, ArrayQ>::template FieldCellGroupType<TopologyCell> QFieldCellGroupTypeL;

  typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
  typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;

  // element field variables
  ElementXFieldClass xfldElem( xfldTrace.basis() );
  ElementQFieldClass qfldElem( qfldTrace.basis() );

  const XFieldCellGroupTypeL& xfldCellL = xfld.getCellGroup<TopologyCell>(xfldTrace.getGroupLeft());
  const QFieldCellGroupTypeL& qfldCellL = qfld.getCellGroup<TopologyCell>(xfldTrace.getGroupLeft());

  ElementXFieldClassL xfldElemL( xfldCellL.basis() );
  ElementQFieldClassL qfldElemL( qfldCellL.basis() );

  int nnode = xfld.nDOF();
  int nelem = xfldTrace.nElem();

  // tecplot does not like empty groups
  if ( nelem == 0 ) return;

  // Node values
  // x, y, z, phi
  int nNodeVar = 4;
  // Element values
  // u, v, w, Cp
  int nElemVar = 4;

  fprintf( fp, "ZONE T=\"Boundary %d\", N=%d, E=%d, F=FEBLOCK, ET=%s",
           group, nnode, nelem, TecTopo<TopologyTrace>::name() );

  fprintf( fp, ", VARLOCATION=([");
  int ivar = 1;
  for (int i = 0; i < nNodeVar; i++)
  {
    fprintf( fp, "%d", ivar++);
    if (i < nNodeVar-1 )
      fprintf( fp, ",");
  }
  fprintf( fp, "]=NODAL,[");
  for (int i = 0; i < nElemVar; i++)
  {
    fprintf( fp, "%d", ivar++);
    if (i < nElemVar-1 )
      fprintf( fp, ",");
  }
  fprintf( fp, "]=CELLCENTERED)");

  fprintf( fp, ", VARSHARELIST=([");
  for (int i = 0; i < nNodeVar-1; i++)
    fprintf( fp, "%d,", i+1);
  fprintf( fp, "%d]=1)\n", nNodeVar);


  typename ElementXFieldClassL::RefCoordType sRefCell;
  typename ElementXFieldClass::VectorX X;
  ArrayQ magq = 0, Cp = 0;
  DLA::VectorS<3, ArrayQ > gradq = 0;
  DLA::VectorS<3, Real > U, V;

  pde.freestream(U);
  Real Vinf2 = dot(U,U);

#if 0
  // phix

  for (int elem = 0; elem < nelem; elem++)
  {
    int elemL = xfldTrace.getElementLeft( elem );
    CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalTraceL, TopologyTrace::centerRef, sRefCell );

    xfldCellL.getElement( xfldElemL, elemL );
    qfldCellL.getElement( qfldElemL, elemL );

    xfldElemL.evalGradient( sRefCell, qfldElemL, gradq );

    if (elem % 5 == 0) fprintf( fp, "\n");
    fprintf( fp, "%14.7e ", gradq[0] );
  }
  fprintf( fp, "\n");

  // phiy

  for (int elem = 0; elem < nelem; elem++)
  {
    int elemL = xfldTrace.getElementLeft( elem );
    CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalTraceL, TopologyTrace::centerRef, sRefCell );

    xfldCellL.getElement( xfldElemL, elemL );
    qfldCellL.getElement( qfldElemL, elemL );

    xfldElemL.evalGradient( sRefCell, qfldElemL, gradq );

    if (elem % 5 == 0) fprintf( fp, "\n");
    fprintf( fp, "%14.7e ", gradq[1] );
  }
  fprintf( fp, "\n");

  // phiz

  for (int elem = 0; elem < nelem; elem++)
  {
    int elemL = xfldTrace.getElementLeft( elem );
    CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalTraceL, TopologyTrace::centerRef, sRefCell );

    xfldCellL.getElement( xfldElemL, elemL );
    qfldCellL.getElement( qfldElemL, elemL );

    xfldElemL.evalGradient( sRefCell, qfldElemL, gradq );

    if (elem % 5 == 0) fprintf( fp, "\n");
    fprintf( fp, "%14.7e ", gradq[2] );
  }
  fprintf( fp, "\n");
#endif
  // u

  for (int elem = 0; elem < nelem; elem++)
  {
    int elemL = xfldTrace.getElementLeft( elem );
    CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalTraceL, TopologyTrace::centerRef, sRefCell );

    xfldCellL.getElement( xfldElemL, elemL );
    qfldCellL.getElement( qfldElemL, elemL );

    xfldElemL.evalGradient( sRefCell, qfldElemL, gradq );
    V = U + gradq;
    if (elem % 5 == 0) fprintf( fp, "\n");
    fprintf( fp, "%14.7e ", V[0] );
  }
  fprintf( fp, "\n");

  // v

  for (int elem = 0; elem < nelem; elem++)
  {
    int elemL = xfldTrace.getElementLeft( elem );
    CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalTraceL, TopologyTrace::centerRef, sRefCell );

    xfldCellL.getElement( xfldElemL, elemL );
    qfldCellL.getElement( qfldElemL, elemL );

    xfldElemL.evalGradient( sRefCell, qfldElemL, gradq );
    V = U + gradq;
    if (elem % 5 == 0) fprintf( fp, "\n");
    fprintf( fp, "%14.7e ", V[1] );
  }
  fprintf( fp, "\n");

  // w

  for (int elem = 0; elem < nelem; elem++)
  {
    int elemL = xfldTrace.getElementLeft( elem );
    CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalTraceL, TopologyTrace::centerRef, sRefCell );

    xfldCellL.getElement( xfldElemL, elemL );
    qfldCellL.getElement( qfldElemL, elemL );

    xfldElemL.evalGradient( sRefCell, qfldElemL, gradq );
    V = U + gradq;
    if (elem % 5 == 0) fprintf( fp, "\n");
    fprintf( fp, "%14.7e ", V[2] );
  }
  fprintf( fp, "\n");

  // |grad(phi)|
#if 0
  for (int elem = 0; elem < nelem; elem++)
  {
    int elemL = xfldTrace.getElementLeft( elem );
    CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalTraceL, TopologyTrace::centerRef, sRefCell );

    xfldCellL.getElement( xfldElemL, elemL );
    qfldCellL.getElement( qfldElemL, elemL );

    xfldElemL.evalGradient( sRefCell, qfldElemL, gradq );
    magq = dot(gradq,gradq);
    if (elem % 5 == 0) fprintf( fp, "\n");
    fprintf( fp, "%14.7e ", sqrt( magq ) );
  }
  fprintf( fp, "\n");
#endif

  // Cp

  for (int elem = 0; elem < nelem; elem++)
  {
    int elemL = xfldTrace.getElementLeft( elem );
    CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalTraceL, TopologyTrace::centerRef, sRefCell );

    xfldCellL.getElement( xfldElemL, elemL );
    qfldCellL.getElement( qfldElemL, elemL );

    xfldElemL.evalGradient( sRefCell, qfldElemL, gradq );
    magq = dot(gradq,gradq);
    Cp = (-0.5*magq - dot(gradq,U))/(0.5*Vinf2);
    if (elem % 5 == 0) fprintf( fp, "\n");
    fprintf( fp, "%14.7e ", Cp );
  }
  fprintf( fp, "\n");

  // cell-to-node connectivity

  int nodeMap[TopologyTrace::NNode];
  for (int elem = 0; elem < nelem; elem++)
  {
    xfldTrace.associativity( elem ).getNodeGlobalMapping( nodeMap, TopologyTrace::NNode );
    for (int n = 0; n < TopologyTrace::NNode; n++ )
      fprintf( fp, "%d ", nodeMap[n]+1 );
    fprintf( fp, "\n" );
  }
}
#else
//----------------------------------------------------------------------------//
// output as P1
//
template<class TopologyTrace, class TopologyCell>
void
output_Tecplot_TraceGroup_LIP( const int group, const PDELinearizedIncompressiblePotential3D<Real>& pde,
                               const XField<PhysD3,TopoD3>& xfld,
                               const typename XField<PhysD3,TopoD3>::FieldTraceGroupType<TopologyTrace>& xfldTrace,
                               const Field< PhysD3, TopoD3, Real >& qfld,
                               const typename Field< PhysD3, TopoD3, Real >::FieldTraceGroupType<TopologyTrace>& qfldTrace,
                               FILE* fp )
{
  typedef typename XField<PhysD3, TopoD3>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename Field<PhysD3, TopoD3, Real >::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;

  typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldClass;
  typedef typename QFieldTraceGroupType::template ElementType<> ElementQFieldClass;

  typedef typename PDELinearizedIncompressiblePotential3D<Real>::ArrayQ<Real> ArrayQ;

  typedef typename XField<PhysD3, TopoD3>::template FieldCellGroupType<TopologyCell> XFieldCellGroupTypeL;
  typedef typename Field<PhysD3, TopoD3, ArrayQ>::template FieldCellGroupType<TopologyCell> QFieldCellGroupTypeL;

  typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
  typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;

  // element field variables
  ElementXFieldClass xfldElem( xfldTrace.basis() );
  ElementQFieldClass qfldElem( qfldTrace.basis() );

  const XFieldCellGroupTypeL& xfldCellL = xfld.getCellGroup<TopologyCell>(xfldTrace.getGroupLeft());
  const QFieldCellGroupTypeL& qfldCellL = qfld.getCellGroup<TopologyCell>(xfldTrace.getGroupLeft());

  ElementXFieldClassL xfldElemL( xfldCellL.basis() );
  ElementQFieldClassL qfldElemL( qfldCellL.basis() );

  int nelem = xfldTrace.nElem();
  int nnode = TopologyTrace::NNode*nelem;
  fprintf( fp, "ZONE T=\"Boundary %d\", N=%d, E=%d, F=FEPOINT, ET=%s\n", group, nnode, nelem, TecTopo<TopologyTrace>::name() );

  typename ElementXFieldClassL::RefCoordType sRefCell;
  typename ElementXFieldClass::RefCoordType sRefTrace;
  typename ElementXFieldClass::VectorX X, N;
  ArrayQ q, magq = 0, Cp = 0, dPhidN = 0;
  DLA::VectorS<3, ArrayQ > gradq = 0;
  DLA::VectorS<3, Real > U, V;

  pde.freestream(U);
  Real Vinf2 = dot(U,U);

  for (int elem = 0; elem < nelem; elem++)
  {
    xfldTrace.getElement( xfldElem, elem );
    qfldTrace.getElement( qfldElem, elem );

    int elemL = xfldTrace.getElementLeft( elem );
    CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

    xfldCellL.getElement( xfldElemL, elemL );
    qfldCellL.getElement( qfldElemL, elemL );


    for ( int i = 0; i < Triangle::NNode; i++ )
    {
      sRefTrace = 0;
      if ( TopologyTrace::NNode == 3)
      {
        if (i > 0 ) sRefTrace[i-1] = 1;
      }
      else
      {
        if ( i == 1 ) sRefTrace = {1,0};
        if ( i == 2 ) sRefTrace = {1,1};
        if ( i == 3 ) sRefTrace = {0,1};
      }
      xfldElem.unitNormal( sRefTrace, N );

      xfldElem.eval( sRefTrace, X );
      qfldElem.eval( sRefTrace, q );

      TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalTraceL, sRefTrace, sRefCell );

      xfldElemL.evalGradient( sRefCell, qfldElemL, gradq );

      magq = dot(gradq,gradq);
      V = U + gradq;
      //V2 = dot(V,V);
      Cp = (-0.5*magq - dot(gradq,U))/(0.5*Vinf2);
      //Cp = (0.5*Vinf2 - 0.5*V2)/(0.5*Vinf2);

      dPhidN = dot(N,V);

      fprintf( fp, "%22.15e %22.15e %22.15e", X[0], X[1], X[2] );
      fprintf( fp, " %22.15e %22.15e %22.15e %22.15e", q, gradq[0], gradq[1], gradq[2] );
      fprintf( fp, " %14.7e %14.7e %14.7e", U[0] + gradq[0], U[1] + gradq[1], U[2] + gradq[2] );
      fprintf( fp, " %14.7e", sqrt( magq ) );
      fprintf( fp, " %14.7e", q + U[0]*X[0] + U[1]*X[1] + U[2]*X[2] );
      fprintf( fp, " %14.7e", Cp );
      fprintf( fp, " %14.7e", -dot(gradq,U)/(0.5*Vinf2) );
      fprintf( fp, " %14.7e", dPhidN );
      fprintf( fp, "\n" );
    }
  }

  // cell-to-node connectivity

  for (int elem = 0; elem < nelem; elem++)
  {
    for (int n = 0; n < TopologyTrace::NNode; n++ )
      fprintf( fp, "%d ", TopologyTrace::NNode*elem+n+1 );
    fprintf( fp, "\n" );
  }
}
#endif

#if 1
//----------------------------------------------------------------------------//
// output as P1
//
template<class TopologyTrace, class TopologyCell>
void
output_Tecplot_TraceGroup_LIP(
    const int group, const PDELinearizedIncompressiblePotential3D<Real>& pde,
    const typename XField<PhysD3,TopoD3>::FieldTraceGroupType<TopologyTrace>& xfldTrace,
    const typename XField<PhysD3,TopoD3>::FieldCellGroupType<TopologyCell>& xfld,
    const typename Field< PhysD3, TopoD3, Real >::FieldCellGroupType<TopologyCell>& qfld,
    FILE* fp, bool leftElement = true )
{
  typedef typename XField<PhysD3, TopoD3>::FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename XField<PhysD3, TopoD3>::FieldCellGroupType<TopologyCell> XFieldCellGroupType;
  typedef typename Field<PhysD3, TopoD3, Real >::template FieldCellGroupType<TopologyCell> QFieldCellGroupType;

  typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

  typedef typename PDELinearizedIncompressiblePotential3D<Real>::ArrayQ<Real> ArrayQ;

  // Node values
  // x, y, z, phi
  int nNodeVar = 4;
  // Element values
  // u, v, w, Cp
  int nElemVar = 4;

  int nnode = xfld.nDOF();
  int nelem = xfldTrace.nElem();

  if (nelem == 0) return;

  fprintf( fp, "ZONE T=\"Boundary %s %d\", N=%d, E=%d, F=FEBLOCK, ET=%s\n",
           leftElement ? "L" : "R", group, nnode, nelem, TecTopo<TopologyTrace>::name() );


  fprintf( fp, ", VARLOCATION=([");
  int ivar = 1;
  for (int i = 0; i < nNodeVar; i++)
  {
    fprintf( fp, "%d", ivar++);
    if (i < nNodeVar-1 )
      fprintf( fp, ",");
  }
  fprintf( fp, "]=NODAL,[");
  for (int i = 0; i < nElemVar; i++)
  {
    fprintf( fp, "%d", ivar++);
    if (i < nElemVar-1 )
      fprintf( fp, ",");
  }
  fprintf( fp, "]=CELLCENTERED)");

  fprintf( fp, ", VARSHARELIST=([");
  for (int i = 0; i < nNodeVar-1; i++)
    fprintf( fp, "%d,", i+1);
  fprintf( fp, "%d]=1)\n", nNodeVar);


  // element field variables
  ElementXFieldTraceClass xfldTraceElem( xfldTrace.basis() );
  ElementXFieldClass xfldElem( xfld.basis() );
  ElementQFieldClass qfldElem( qfld.basis() );

  typename ElementXFieldTraceClass::RefCoordType sRefTrace;
  typename ElementQFieldClass::RefCoordType sRefCell;
  typename ElementXFieldTraceClass::VectorX X, N;
  ArrayQ magq, Cp;
  DLA::VectorS<3, ArrayQ > gradq;
  DLA::VectorS<3, Real > U, V;
  int elemCell;
  CanonicalTraceToCell canonicalTrace;

  pde.freestream(U);
  Real Vinf2 = dot(U,U);
  //Real V2 = 0;

#if 0
  // phix

  for (int elem = 0; elem < nelem; elem++)
  {
    if ( leftElement )
    {
      elemCell = xfldTrace.getElementLeft( elem );
      canonicalTrace = xfldTrace.getCanonicalTraceLeft( elem );
    }
    else
    {
      elemCell = xfldTrace.getElementRight( elem );
      canonicalTrace = xfldTrace.getCanonicalTraceRight( elem );
    }

    xfld.getElement( xfldElem, elemCell );
    qfld.getElement( qfldElem, elemCell );

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalTrace, TopologyTrace::centerRef, sRefCell );

    xfldElem.evalGradient( sRefCell, qfldElem, gradq );

    if (elem % 5 == 0) fprintf( fp, "\n");
    fprintf( fp, "%14.7e ", gradq[0] );
  }
  fprintf( fp, "\n");

  // phiy

  for (int elem = 0; elem < nelem; elem++)
  {
    if ( leftElement )
    {
      elemCell = xfldTrace.getElementLeft( elem );
      canonicalTrace = xfldTrace.getCanonicalTraceLeft( elem );
    }
    else
    {
      elemCell = xfldTrace.getElementRight( elem );
      canonicalTrace = xfldTrace.getCanonicalTraceRight( elem );
    }

    xfld.getElement( xfldElem, elemCell );
    qfld.getElement( qfldElem, elemCell );

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalTrace, TopologyTrace::centerRef, sRefCell );

    xfldElem.evalGradient( sRefCell, qfldElem, gradq );

    if (elem % 5 == 0) fprintf( fp, "\n");
    fprintf( fp, "%14.7e ", gradq[1] );
  }
  fprintf( fp, "\n");

  // phiz

  for (int elem = 0; elem < nelem; elem++)
  {
    if ( leftElement )
    {
      elemCell = xfldTrace.getElementLeft( elem );
      canonicalTrace = xfldTrace.getCanonicalTraceLeft( elem );
    }
    else
    {
      elemCell = xfldTrace.getElementRight( elem );
      canonicalTrace = xfldTrace.getCanonicalTraceRight( elem );
    }

    xfld.getElement( xfldElem, elemCell );
    qfld.getElement( qfldElem, elemCell );

    TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalTrace, TopologyTrace::centerRef, sRefCell );

    xfldElem.evalGradient( sRefCell, qfldElem, gradq );

    if (elem % 5 == 0) fprintf( fp, "\n");
    fprintf( fp, "%14.7e ", gradq[2] );
  }
  fprintf( fp, "\n");
#endif
  // u

  for (int elem = 0; elem < nelem; elem++)
  {
    if ( leftElement )
    {
      elemCell = xfldTrace.getElementLeft( elem );
      canonicalTrace = xfldTrace.getCanonicalTraceLeft( elem );
    }
    else
    {
      elemCell = xfldTrace.getElementRight( elem );
      canonicalTrace = xfldTrace.getCanonicalTraceRight( elem );
    }

    xfld.getElement( xfldElem, elemCell );
    qfld.getElement( qfldElem, elemCell );

    TraceToCellRefCoord<TopologyTrace, TopoD3, TopologyCell>::eval( canonicalTrace, TopologyTrace::centerRef, sRefCell );

    xfldElem.evalGradient( sRefCell, qfldElem, gradq );
    V = U + gradq;
    if (elem % 5 == 0) fprintf( fp, "\n");
    fprintf( fp, "%14.7e ", V[0] );
  }
  fprintf( fp, "\n");

  // v

  for (int elem = 0; elem < nelem; elem++)
  {
    if ( leftElement )
    {
      elemCell = xfldTrace.getElementLeft( elem );
      canonicalTrace = xfldTrace.getCanonicalTraceLeft( elem );
    }
    else
    {
      elemCell = xfldTrace.getElementRight( elem );
      canonicalTrace = xfldTrace.getCanonicalTraceRight( elem );
    }

    xfld.getElement( xfldElem, elemCell );
    qfld.getElement( qfldElem, elemCell );

    TraceToCellRefCoord<TopologyTrace, TopoD3, TopologyCell>::eval( canonicalTrace, TopologyTrace::centerRef, sRefCell );

    xfldElem.evalGradient( sRefCell, qfldElem, gradq );
    V = U + gradq;
    if (elem % 5 == 0) fprintf( fp, "\n");
    fprintf( fp, "%14.7e ", V[1] );
  }
  fprintf( fp, "\n");

  // w

  for (int elem = 0; elem < nelem; elem++)
  {
    if ( leftElement )
    {
      elemCell = xfldTrace.getElementLeft( elem );
      canonicalTrace = xfldTrace.getCanonicalTraceLeft( elem );
    }
    else
    {
      elemCell = xfldTrace.getElementRight( elem );
      canonicalTrace = xfldTrace.getCanonicalTraceRight( elem );
    }

    xfld.getElement( xfldElem, elemCell );
    qfld.getElement( qfldElem, elemCell );

    TraceToCellRefCoord<TopologyTrace, TopoD3, TopologyCell>::eval( canonicalTrace, TopologyTrace::centerRef, sRefCell );

    xfldElem.evalGradient( sRefCell, qfldElem, gradq );
    V = U + gradq;
    if (elem % 5 == 0) fprintf( fp, "\n");
    fprintf( fp, "%14.7e ", V[2] );
  }
  fprintf( fp, "\n");

  // |grad(phi)|
#if 0
  for (int elem = 0; elem < nelem; elem++)
  {
    if ( leftElement )
    {
      elemCell = xfldTrace.getElementLeft( elem );
      canonicalTrace = xfldTrace.getCanonicalTraceLeft( elem );
    }
    else
    {
      elemCell = xfldTrace.getElementRight( elem );
      canonicalTrace = xfldTrace.getCanonicalTraceRight( elem );
    }

    xfld.getElement( xfldElem, elemCell );
    qfld.getElement( qfldElem, elemCell );

    TraceToCellRefCoord<TopologyTrace, TopoD3, TopologyCell>::eval( canonicalTrace, TopologyTrace::centerRef, sRefCell );

    xfldElem.evalGradient( sRefCell, qfldElem, gradq );
    magq = dot(gradq,gradq);
    if (elem % 5 == 0) fprintf( fp, "\n");
    fprintf( fp, "%14.7e ", sqrt( magq ) );
  }
  fprintf( fp, "\n");
#endif
  // Cp

  for (int elem = 0; elem < nelem; elem++)
  {
    if ( leftElement )
    {
      elemCell = xfldTrace.getElementLeft( elem );
      canonicalTrace = xfldTrace.getCanonicalTraceLeft( elem );
    }
    else
    {
      elemCell = xfldTrace.getElementRight( elem );
      canonicalTrace = xfldTrace.getCanonicalTraceRight( elem );
    }

    xfld.getElement( xfldElem, elemCell );
    qfld.getElement( qfldElem, elemCell );

    TraceToCellRefCoord<TopologyTrace, TopoD3, TopologyCell>::eval( canonicalTrace, TopologyTrace::centerRef, sRefCell );

    xfldElem.evalGradient( sRefCell, qfldElem, gradq );
    magq = dot(gradq,gradq);
    Cp = (-0.5*magq - dot(gradq,U))/(0.5*Vinf2);
    if (elem % 5 == 0) fprintf( fp, "\n");
    fprintf( fp, "%14.7e ", Cp );
  }
  fprintf( fp, "\n");

  // cell-to-node connectivity

  const int (*TraceNodes)[ TopologyTrace::NNode ] = TraceToCellRefCoord<TopologyTrace, TopoD3, TopologyCell>::TraceNodes;

  int nodeMap[TopologyCell::NNode];
  for (int elem = 0; elem < nelem; elem++)
  {
    if ( leftElement )
    {
      elemCell = xfldTrace.getElementLeft( elem );
      canonicalTrace = xfldTrace.getCanonicalTraceLeft( elem );
    }
    else
    {
      elemCell = xfldTrace.getElementRight( elem );
      canonicalTrace = xfldTrace.getCanonicalTraceRight( elem );
    }

    xfld.associativity( elemCell ).getNodeGlobalMapping( nodeMap, TopologyCell::NNode );
    for (int n = 0; n < TopologyTrace::NNode; n++ )
      fprintf( fp, "%d ", nodeMap[TraceNodes[canonicalTrace.trace][n]]+1 );
    fprintf( fp, "\n" );
  }
}

#else
//----------------------------------------------------------------------------//
// output as P1
//
template<class TopologyTrace, class TopologyCell>
void
output_Tecplot_TraceGroup_LIP( const int group, const PDELinearizedIncompressiblePotential3D<Real>& pde,
                               const typename XField<PhysD3,TopoD3>::FieldTraceGroupType<TopologyTrace>& xfldTrace,
                               const typename XField<PhysD3,TopoD3>::FieldCellGroupType<TopologyCell>& xfld,
                               const typename Field< PhysD3, TopoD3, Real >::FieldCellGroupType<TopologyCell>& qfld,
                               FILE* fp, bool leftElement = true )
{
  typedef typename XField<PhysD3, TopoD3>::FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename XField<PhysD3, TopoD3>::FieldCellGroupType<TopologyCell> XFieldCellGroupType;
  typedef typename Field<PhysD3, TopoD3, Real >::template FieldCellGroupType<TopologyCell> QFieldCellGroupType;

  typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

  typedef typename PDELinearizedIncompressiblePotential3D<Real>::ArrayQ<Real> ArrayQ;

  // element field variables
  ElementXFieldTraceClass xfldTraceElem( xfldTrace.basis() );
  ElementXFieldClass xfldElem( xfld.basis() );
  ElementQFieldClass qfldElem( qfld.basis() );

  int nelem = xfldTrace.nElem();
  int nnode = TopologyTrace::NNode*nelem;

  if (nelem == 0) return;

  fprintf( fp, "ZONE T=\"Boundary %s %d\", N=%d, E=%d, F=FEPOINT, ET=%s\n",
           leftElement ? "L" : "R", group, nnode, nelem, TecTopo<TopologyTrace>::name() );

  typename ElementXFieldTraceClass::RefCoordType sRefTrace;
  typename ElementQFieldClass::RefCoordType stu;
  typename ElementXFieldTraceClass::VectorX X, N;
  ArrayQ q, magq, Cp;
  DLA::VectorS<3, ArrayQ > gradq;
  DLA::VectorS<3, Real > U, V;
  int elemCell;
  CanonicalTraceToCell canonicalTrace;

  pde.freestream(U);
  Real Vinf2 = dot(U,U);
  //Real V2 = 0;

  for (int elem = 0; elem < nelem; elem++)
  {
    xfldTrace.getElement( xfldTraceElem, elem );

    if ( leftElement )
    {
      elemCell = xfldTrace.getElementLeft( elem );
      canonicalTrace = xfldTrace.getCanonicalTraceLeft( elem );
    }
    else
    {
      elemCell = xfldTrace.getElementRight( elem );
      canonicalTrace = xfldTrace.getCanonicalTraceRight( elem );
    }

    xfld.getElement( xfldElem, elemCell );
    qfld.getElement( qfldElem, elemCell );

    for ( int i = 0; i < TopologyTrace::NNode; i++ )
    {
      sRefTrace = 0;
      if ( TopologyTrace::NNode == 3)
      {
        if (i > 0 ) sRefTrace[i-1] = 1;
      }
      else
      {
        if ( i == 1 ) sRefTrace = {1,0};
        if ( i == 2 ) sRefTrace = {1,1};
        if ( i == 3 ) sRefTrace = {0,1};
      }

      xfldTraceElem.eval( sRefTrace, X );
      xfldTraceElem.unitNormal( sRefTrace, N );

      TraceToCellRefCoord<TopologyTrace, TopoD3, TopologyCell>::eval( canonicalTrace, sRefTrace, stu );
      qfldElem.eval( stu, q );

      xfldElem.evalGradient( stu, qfldElem, gradq );
      magq = dot(gradq,gradq);
      V = U + gradq;
      //V2 = dot(V,V);
      Cp = (-0.5*magq - dot(gradq,U))/(0.5*Vinf2);
      //Cp = (0.5*Vinf2 - 0.5*V2)/(0.5*Vinf2);

      fprintf( fp, "%22.15e %22.15e %22.15e", X[0], X[1], X[2] );
      fprintf( fp, " %22.15e %22.15e %22.15e %22.15e", q, gradq[0], gradq[1], gradq[2] );
      fprintf( fp, " %14.7e %14.7e %14.7e", V[0], V[1], V[2] );
      fprintf( fp, " %14.7e", sqrt( magq ) );
      fprintf( fp, " %14.7e", q + U[0]*X[0] + U[1]*X[1] + U[2]*X[2] );
      fprintf( fp, " %14.7e", Cp );
      fprintf( fp, " %14.7e", -dot(gradq,U)/(0.5*Vinf2) );
      fprintf( fp, " %14.7e", dot(N,V) );
      fprintf( fp, "\n" );
    }
  }

  // cell-to-node connectivity

  for (int elem = 0; elem < nelem; elem++)
  {
    for (int n = 0; n < TopologyTrace::NNode; n++ )
      fprintf( fp, "%d ", TopologyTrace::NNode*elem+n+1 );
    fprintf( fp, "\n" );
  }
}
#endif


//----------------------------------------------------------------------------//
void
output_Tecplot_LIP( const PDELinearizedIncompressiblePotential3D<Real>& pde,
                    const Field< PhysD3, TopoD3, Real >& qfld,
                    const std::string& filename )
{
  std::cout << "output_Tecplot: filename = " << filename << std::endl;
  FILE* fp = fopen( filename.c_str(), "w" );

  fprintf( fp, "\"\"\n" );
  fprintf( fp, "VARIABLES = \"X\", \"Y\", \"Z\"" );
  //fprintf( fp, ", \"phi\", \"phitot\", \"phix\", \"phiy\", \"phiz\", \"u\", \"v\", \"w\", \"|grad(phi)|\", \"Cp\"" );
  fprintf( fp, ", \"phi\", \"u\", \"v\", \"w\", \"Cp\"" );
  fprintf( fp, "\n" );


  const XField<PhysD3,TopoD3>& xgrid = qfld.getXField();

  const int ngroup = qfld.nCellGroups();
  SANS_ASSERT(ngroup == 1); // for now...

  // loop over cell groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Tet) )
    {
      output_Tecplot_CellGroup_Nodes_LIP( pde,
                                          xgrid.getCellGroup<Tet>(group),
                                          qfld.getCellGroup<Tet>(group), fp );
    }
    else if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Hex) )
    {
      output_Tecplot_CellGroup_Nodes_LIP( pde,
                                          xgrid.getCellGroup<Hex>(group),
                                          qfld.getCellGroup<Hex>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot_LIP(XField<PhysD3,TopoD3>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

#if 0
  if ( qfld.nBoundaryTraceGroups() > 0 )
  {
    // loop over trace boundary groups
    for (int group = 0; group < qfld.nBoundaryTraceGroups(); group++)
    {
      // dispatch integration over cell of group
      if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Triangle) )
      {
        output_Tecplot_TraceGroup_LIP<Triangle,Tet>( group, pde, xgrid, xgrid.getBoundaryTraceGroup<Triangle>(group),
                                                     qfld, qfld.getBoundaryTraceGroup<Triangle>(group), fp );
      }
      else if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Quad) )
      {
        output_Tecplot_TraceGroup_LIP<Quad,Hex>( group, pde, xgrid, xgrid.getBoundaryTraceGroup<Quad>(group),
                                                 qfld, qfld.getBoundaryTraceGroup<Quad>(group), fp );
      }
      else
      {
        const char msg[] = "Error in output_Tecplot_LIP(XField<PhysD3,TopoD3>): unknown topology\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }
  }
#endif

  // loop over trace element groups
  for (int group = 0; group < xgrid.nBoundaryTraceGroups(); group++)
  {
    if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      int groupL = xgrid.getBoundaryTraceGroup<Triangle>(group).getGroupLeft();
      int groupR = xgrid.getBoundaryTraceGroup<Triangle>(group).getGroupRight();
      output_Tecplot_TraceGroup_LIP( group, pde, xgrid.getBoundaryTraceGroup<Triangle>(group),
                                     xgrid.getCellGroup<Tet>(groupL),
                                     qfld.getCellGroup<Tet>(groupL), fp );

      // Dump out the trace from the elements on the right if a right group exists
      if (groupR >= 0)
        output_Tecplot_TraceGroup_LIP( group, pde, xgrid.getBoundaryTraceGroup<Triangle>(group),
                                       xgrid.getCellGroup<Tet>(groupR),
                                       qfld.getCellGroup<Tet>(groupR), fp, false );

    }
    else if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Quad) )
    {
      int groupL = xgrid.getBoundaryTraceGroup<Quad>(group).getGroupLeft();
      int groupR = xgrid.getBoundaryTraceGroup<Quad>(group).getGroupRight();
      output_Tecplot_TraceGroup_LIP( group, pde, xgrid.getBoundaryTraceGroup<Quad>(group),
                                     xgrid.getCellGroup<Hex>(groupL),
                                     qfld.getCellGroup<Hex>(groupL), fp );

      // Dump out the trace from the elements on the right if a right group exists
      if (groupR >= 0)
        output_Tecplot_TraceGroup_LIP( group, pde, xgrid.getBoundaryTraceGroup<Quad>(group),
                                       xgrid.getCellGroup<Hex>(groupR),
                                       qfld.getCellGroup<Hex>(groupR), fp, false );

    }
    else
    {
      char msg[] = "Error in output_Tecplot_LIP<TopoD3>::integrate: unknown trace topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  fclose( fp );
}


} //namespace SANS
