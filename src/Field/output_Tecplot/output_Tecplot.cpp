// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "output_Tecplot.h"
#include "Field/output_Tecplot/TecTopo.h"

#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"

#include "XFieldLine.h"
#include "XFieldArea.h"
#include "XFieldVolume.h"
#include "XFieldSpacetime.h"

#include "FieldLine.h"
#include "FieldArea.h"
#include "FieldVolume.h"
#include "FieldSpacetime.h"

#include "FieldLiftLine_DG_Cell.h"
#include "FieldLiftArea_DG_Cell.h"
#include "FieldLiftVolume_DG_Cell.h"
#include "FieldLiftSpaceTime_DG_Cell.h"

#include "Field/Element/ReferenceElementMesh.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"
#include "MPI/continuousElementMap.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_to_all.hpp>
#include <boost/mpi/collectives/all_reduce.hpp>
#endif

namespace SANS
{

//----------------------------------------------------------------------------//
// output as P1
//
template<class PhysDim, class TopoDim, class Topology>
void
dofRank_CellGroup( const int* local2nativeDOFmap,
                   const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldGroup,
                   const Field<PhysDim,TopoDim,Real>& qfld,
                   const typename  Field<PhysDim,TopoDim,Real>::template FieldCellGroupType<Topology>& qfldGroup,
                   std::map<int,int>& dofRank )
{
  SANS_ASSERT(xfldGroup.nDOF() == qfldGroup.nDOF());

  const int comm_rank = qfld.comm()->rank();
  int nelem = xfldGroup.nElem();

  const int nDOF = xfldGroup.nDOF();
  std::vector<int> xDOFMap(nDOF);
  std::vector<int> qDOFMap(nDOF);
  for (int elem = 0; elem < nelem; elem++)
  {
    int rank = comm_rank;

    xfldGroup.associativity( elem ).getGlobalMapping( xDOFMap.data(), xDOFMap.size() );
    qfldGroup.associativity( elem ).getGlobalMapping( qDOFMap.data(), qDOFMap.size() );
    for (int n = 0; n < nDOF; n++ )
    {
      int nativeNode = local2nativeDOFmap[xDOFMap[n]];

      if (qDOFMap[n] >= qfld.nDOFpossessed()) rank = qfld.DOFghost_rank(qDOFMap[n]);

      dofRank[nativeNode] = rank;
    }
  }
}

//----------------------------------------------------------------------------//
// output as P1
//
template<class PhysDim, class TopoDim, class Topology>
void
output_Tecplot_CellGroup_Nodes(
    const int* local2nativeDOFmap,
    const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfld,
    const std::map<int,int>& nodeMinRank,
    FILE* fp )
{
  int nnode = xfld.nDOF();
  int nelem = xfld.nElem();

  // tecplot does not like empty groups
  if ( nelem == 0 ) return;

  fprintf( fp, "ZONE T=\"grid\", N=%d, E=%d, F=FEBLOCK, ET=%s", nnode, nelem, TecTopo<Topology>::name() );

  fprintf( fp, ", VARLOCATION=([");
  for (int d = 0; d < PhysDim::D+1; d++)
    fprintf( fp, "%d,", d+1);
  fprintf( fp, "%d]=NODAL,[%d]=CELLCENTERED)\n", PhysDim::D+2, PhysDim::D+3);

  // grid coordinates

  for (int d = 0; d < PhysDim::D; d++)
  {
    for (int n = 0; n < nnode; n++)
    {
      if (n % 5 == 0) fprintf( fp, "\n");
      fprintf( fp, "%22.15e ", xfld.DOF(n)[d] );
    }
    fprintf( fp, "\n");
  }

  // native DOF index

  for (int n = 0; n < nnode; n++)
  {
    if (n % 5 == 0) fprintf( fp, "\n");
    fprintf( fp, "%d ", local2nativeDOFmap[n]);
  }
  fprintf( fp, "\n");

  // node rank

  for (int n = 0; n < nnode; n++)
  {
    if (n % 5 == 0) fprintf( fp, "\n");
    fprintf( fp, "%d ", nodeMinRank.at(local2nativeDOFmap[n]) );
  }
  fprintf( fp, "\n");

  // element rank

  for (int elem = 0; elem < nelem; elem++)
  {
    if (elem % 5 == 0) fprintf( fp, "\n");
    fprintf( fp, "%d ", xfld.associativity( elem ).rank() );
  }
  fprintf( fp, "\n" );

  // cell-to-node connectivity

  int nodeMap[Topology::NNode];
  for (int elem = 0; elem < nelem; elem++)
  {
    xfld.associativity( elem ).getNodeGlobalMapping( nodeMap, Topology::NNode );
    for (int n = 0; n < Topology::NNode; n++ )
      fprintf( fp, "%d ", nodeMap[n]+1 );
    fprintf( fp, "\n" );
  }
}

//----------------------------------------------------------------------------//
// output as P1
//
template<class PhysDim, class TopoDim, class Topology>
void
output_Tecplot_CellGroup( const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfld, FILE* fp )
{
  int nnode = xfld.nDOF();
  int nelem = xfld.nElem();

  // tecplot does not like empty groups
  if ( nelem == 0 ) return;

  fprintf( fp, "ZONE T=\"grid\", N=%d, E=%d, F=FEBLOCK, ET=%s", nnode, nelem, TecTopo<Topology>::name() );

  fprintf( fp, ", VARLOCATION=([");
  for (int d = 0; d < PhysDim::D+1; d++)
    fprintf( fp, "%d,", d+1);
  fprintf( fp, "%d]=NODAL,[%d]=CELLCENTERED)", PhysDim::D+2, PhysDim::D+3);

  fprintf( fp, ", VARSHARELIST=([");
  for (int d = 0; d < PhysDim::D+1; d++)
    fprintf( fp, "%d,", d+1);
  fprintf( fp, "%d]=1)\n", PhysDim::D+2);

  // element rank

  for (int elem = 0; elem < nelem; elem++)
  {
    if (elem % 5 == 0) fprintf( fp, "\n");
    fprintf( fp, "%d ", xfld.associativity( elem ).rank() );
  }
  fprintf( fp, "\n" );

  // cell-to-node connectivity

  int nodeMap[Topology::NNode];
  for (int elem = 0; elem < nelem; elem++)
  {
    xfld.associativity( elem ).getNodeGlobalMapping( nodeMap, Topology::NNode );
    for (int n = 0; n < Topology::NNode; n++ )
      fprintf( fp, "%d ", nodeMap[n]+1 );
    fprintf( fp, "\n" );
  }
}


//----------------------------------------------------------------------------//
// output as P1
//
template<class PhysDim, class TopoDim, class Topology>
void
output_Tecplot_TraceGroup( const std::string& type, const int group,
                           const typename XField<PhysDim,TopoDim>::template FieldTraceGroupType<Topology>& xfld, FILE* fp )
{
  int nnode = xfld.nDOF();
  int nelem = xfld.nElem();

  // tecplot does not like empty groups
  if ( nelem == 0 ) return;

  fprintf( fp, "ZONE T=\"%s %d\", N=%d, E=%d, F=FEBLOCK, ET=%s",
           type.c_str(), group, nnode, nelem, TecTopo<Topology>::name() );

  fprintf( fp, ", VARLOCATION=([");
  for (int d = 0; d < PhysDim::D+1; d++)
    fprintf( fp, "%d,", d+1);
  fprintf( fp, "%d]=NODAL,[%d]=CELLCENTERED)", PhysDim::D+2, PhysDim::D+3);

  fprintf( fp, ", VARSHARELIST=([");
  for (int d = 0; d < PhysDim::D+1; d++)
    fprintf( fp, "%d,", d+1);
  fprintf( fp, "%d]=1)\n", PhysDim::D+2);

  // element rank

  for (int elem = 0; elem < nelem; elem++)
  {
    if (elem % 5 == 0) fprintf( fp, "\n");
    fprintf( fp, "%d ", xfld.associativity( elem ).rank() );
  }
  fprintf( fp, "\n" );

  // cell-to-node connectivity

  int nodeMap[Topology::NNode];
  for (int elem = 0; elem < nelem; elem++)
  {
    xfld.associativity( elem ).getNodeGlobalMapping( nodeMap, Topology::NNode );
    for (int n = 0; n < Topology::NNode; n++ )
      fprintf( fp, "%d ", nodeMap[n]+1 );
    fprintf( fp, "\n" );
  }

}

//----------------------------------------------------------------------------//
template<>
void
output_Tecplot( const XField<PhysD1,TopoD1>& xgrid, const std::string& filename )
{
  std::cout << "output_Tecplot: filename = " << filename << std::endl;
  FILE* fp = fopen( filename.c_str(), "w" );
  if (fp == NULL)
    SANS_DEVELOPER_EXCEPTION("output_Tecplot<PhysD1,TopoD1> - Error opening file: %s", filename.c_str());

  fprintf( fp, "\"\"\n" );
  fprintf( fp, "VARIABLES = \"X\", \"Native Index\", \"Node Rank\", \"Cell Rank\"\n" );

  const int ngroup = xgrid.nCellGroups();
  std::map<int,int> nodeMinRank;

  // loop over element groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over elements of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Line) )
    {
      nodeRank_CellGroup<PhysD1, TopoD1, Line>( xgrid.local2nativeDOFmap(), xgrid.getCellGroup<Line>(group), nodeMinRank );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD1,TopoD1>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }
  finishNodeRanking( *xgrid.comm(), nodeMinRank );


  // loop over element groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over elements of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Line) )
    {
      if (group == 0)
        output_Tecplot_CellGroup_Nodes<PhysD1, TopoD1, Line>( xgrid.local2nativeDOFmap(), xgrid.getCellGroup<Line>(group), nodeMinRank, fp );
      else
        output_Tecplot_CellGroup<PhysD1, TopoD1, Line>( xgrid.getCellGroup<Line>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD1,TopoD1>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  // loop over interior trace groups (skip the first one because it should mostly duplicate the entire grid)
//  for (int group = 1; group < xgrid.nInteriorTraceGroups(); group++)
//  {
//    // dispatch integration over cell of group
//    if ( xgrid.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Node) )
//    {
//      output_Tecplot_TraceGroup<PhysD1, TopoD1, Node>("Interior", group, xgrid.getInteriorTraceGroup<Node>(group), fp );
//    }
//    else
//    {
//      const char msg[] = "Error in output_Tecplot(XField<PhysD1,TopoD1>): unknown topology\n";
//      SANS_DEVELOPER_EXCEPTION( msg );
//    }
//  }

  // loop over trace boundary groups
  for (int group = 0; group < xgrid.nBoundaryTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Line) ) // TODO: should this be node?
    {
      output_Tecplot_TraceGroup<PhysD1, TopoD1, Line>("Boundary", group, xgrid.getBoundaryTraceGroup<Line>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD1,TopoD1>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  fclose( fp );
}

//----------------------------------------------------------------------------//
#if 1
// TODO: working on it
template<>
void
output_Tecplot( const XField<PhysD2,TopoD1>& xgrid, const std::string& filename )
{
  std::cout << "output_Tecplot: filename = " << filename << std::endl;
  FILE* fp = fopen( filename.c_str(), "w" );
  if (fp == NULL)
    SANS_DEVELOPER_EXCEPTION("output_Tecplot<PhysD2,TopoD1> - Error opening file: %s", filename.c_str());

  fprintf( fp, "\"\"\n" );
  fprintf( fp, "VARIABLES = \"X\", \"Y\", \"Native Index\", \"Node Rank\", \"Cell Rank\"\n" );

  const int ngroup = xgrid.nCellGroups();
  std::map<int,int> nodeMinRank;

  // loop over element groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over elements of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Line) )
    {
      nodeRank_CellGroup<PhysD2, TopoD1, Line>( xgrid.local2nativeDOFmap(), xgrid.getCellGroup<Line>(group), nodeMinRank );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD1,TopoD1>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }
  finishNodeRanking( *xgrid.comm(), nodeMinRank );


  // loop over element groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over elements of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Line) )
    {
      if (group == 0)
        output_Tecplot_CellGroup_Nodes<PhysD2, TopoD1, Line>( xgrid.local2nativeDOFmap(), xgrid.getCellGroup<Line>(group), nodeMinRank, fp );
      else
        output_Tecplot_CellGroup<PhysD2, TopoD1, Line>( xgrid.getCellGroup<Line>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD2,TopoD1>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }
#if 0 // TODO: to be fixed
  // loop over interior trace groups (skip the first one because it should mostly duplicate the entire grid)
  for (int group = 1; group < xgrid.nInteriorTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Line) )
    {
      output_Tecplot_TraceGroup<PhysD2, TopoD1, Line>("Interior", group, xgrid.getInteriorTraceGroup<Line>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD2,TopoD1>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  // loop over trace boundary groups
  for (int group = 0; group < xgrid.nBoundaryTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Node) )
    {
      output_Tecplot_TraceGroup<PhysD2, TopoD1, Line>("Boundary", group, xgrid.getBoundaryTraceGroup<Line>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD2,TopoD1>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }
#endif

  fclose( fp );
}
#endif

//----------------------------------------------------------------------------//
template<>
void
output_Tecplot( const XField<PhysD2,TopoD2>& xgrid, const std::string& filename )
{
  std::cout << "output_Tecplot: filename = " << filename << std::endl;
  FILE* fp = fopen( filename.c_str(), "w" );
  if (fp == NULL)
    SANS_DEVELOPER_EXCEPTION("output_Tecplot<PhysD2,TopoD2> - Error opening file: %s", filename.c_str());

  fprintf( fp, "\"\"\n" );
  fprintf( fp, "VARIABLES = \"X\", \"Y\", \"Native Index\", \"Node Rank\", \"Cell Rank\"\n" );

  const int ngroup = xgrid.nCellGroups();
  std::map<int,int> nodeMinRank;

  // loop over element groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over elements of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      nodeRank_CellGroup<PhysD2, TopoD2, Triangle>( xgrid.local2nativeDOFmap(), xgrid.getCellGroup<Triangle>(group), nodeMinRank );
    }
    else if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Quad) )
    {
      nodeRank_CellGroup<PhysD2, TopoD2, Quad>( xgrid.local2nativeDOFmap(), xgrid.getCellGroup<Quad>(group), nodeMinRank );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD1,TopoD1>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }
  finishNodeRanking( *xgrid.comm(), nodeMinRank );


  // loop over element groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over elements of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      if (group == 0)
        output_Tecplot_CellGroup_Nodes<PhysD2, TopoD2, Triangle>( xgrid.local2nativeDOFmap(), xgrid.getCellGroup<Triangle>(group), nodeMinRank, fp );
      else
        output_Tecplot_CellGroup<PhysD2, TopoD2, Triangle>( xgrid.getCellGroup<Triangle>(group), fp );
    }
    else if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Quad) )
    {
      if (group == 0)
        output_Tecplot_CellGroup_Nodes<PhysD2, TopoD2, Quad>( xgrid.local2nativeDOFmap(), xgrid.getCellGroup<Quad>(group), nodeMinRank, fp );
      else
        output_Tecplot_CellGroup<PhysD2, TopoD2, Quad>( xgrid.getCellGroup<Quad>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD2,TopoD2>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  // loop over interior trace groups (skip the first one because it should mostly duplicate the entire grid)
  for (int group = 1; group < xgrid.nInteriorTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Line) )
    {
      output_Tecplot_TraceGroup<PhysD2, TopoD2, Line>("Interior", group, xgrid.getInteriorTraceGroup<Line>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD2,TopoD2>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  // loop over trace boundary groups
  for (int group = 0; group < xgrid.nBoundaryTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Line) )
    {
      output_Tecplot_TraceGroup<PhysD2, TopoD2, Line>("Boundary", group, xgrid.getBoundaryTraceGroup<Line>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD2,TopoD2>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  // loop over ghost trace boundary groups
  for (int group = 0; group < xgrid.nGhostBoundaryTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Line) )
    {
      output_Tecplot_TraceGroup<PhysD2, TopoD2, Line>("Ghost", group, xgrid.getGhostBoundaryTraceGroup<Line>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD2,TopoD2>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  fclose( fp );
}

//----------------------------------------------------------------------------//
template<>
void
output_Tecplot( const XField<PhysD3,TopoD3>& xgrid, const std::string& filename )
{
  std::cout << "output_Tecplot: filename = " << filename << std::endl;
  FILE* fp = fopen( filename.c_str(), "w" );
  if (fp == NULL)
    SANS_DEVELOPER_EXCEPTION("output_Tecplot<PhysD3,TopoD3> - Error opening file: %s", filename.c_str());

  fprintf( fp, "\"\"\n" );
  fprintf( fp, "VARIABLES = \"X\", \"Y\", \"Z\", \"Native Index\", \"Node Rank\", \"Cell Rank\"\n" );

  const int ngroup = xgrid.nCellGroups();
  std::map<int,int> nodeMinRank;

  // loop over element groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over elements of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Tet) )
    {
      nodeRank_CellGroup<PhysD3, TopoD3, Tet>( xgrid.local2nativeDOFmap(), xgrid.getCellGroup<Tet>(group), nodeMinRank );
    }
    else if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Hex) )
    {
      nodeRank_CellGroup<PhysD3, TopoD3, Hex>( xgrid.local2nativeDOFmap(), xgrid.getCellGroup<Hex>(group), nodeMinRank );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD1,TopoD1>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }
  finishNodeRanking( *xgrid.comm(), nodeMinRank );


  // loop over cell groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Tet) )
    {
      if (group == 0)
        output_Tecplot_CellGroup_Nodes<PhysD3, TopoD3, Tet>( xgrid.local2nativeDOFmap(), xgrid.getCellGroup<Tet>(group), nodeMinRank, fp );
      else
        output_Tecplot_CellGroup<PhysD3, TopoD3, Tet>( xgrid.getCellGroup<Tet>(group), fp );
    }
    else if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Hex) )
    {
      if (group == 0)
        output_Tecplot_CellGroup_Nodes<PhysD3, TopoD3, Hex>( xgrid.local2nativeDOFmap(), xgrid.getCellGroup<Hex>(group), nodeMinRank, fp );
      else
        output_Tecplot_CellGroup<PhysD3, TopoD3, Hex>( xgrid.getCellGroup<Hex>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD3,TopoD3>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  // loop over trace boundary groups
  for (int group = 0; group < xgrid.nBoundaryTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      output_Tecplot_TraceGroup<PhysD3, TopoD3, Triangle>("Boundary", group, xgrid.getBoundaryTraceGroup<Triangle>(group), fp );
    }
    else if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Quad) )
    {
      output_Tecplot_TraceGroup<PhysD3, TopoD3, Quad>("Boundary", group, xgrid.getBoundaryTraceGroup<Quad>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD3,TopoD3>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  // loop over ghost trace boundary groups
  for (int group = 0; group < xgrid.nGhostBoundaryTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      output_Tecplot_TraceGroup<PhysD3, TopoD3, Triangle>("Ghost", group, xgrid.getGhostBoundaryTraceGroup<Triangle>(group), fp );
    }
    else if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Quad) )
    {
      output_Tecplot_TraceGroup<PhysD3, TopoD3, Quad>("Ghost", group, xgrid.getGhostBoundaryTraceGroup<Quad>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD3,TopoD3>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  fclose( fp );
}

//----------------------------------------------------------------------------//
template<class T, class PhysDim, class TopoDim, class Topology>
void
output_Tecplot_CellGroup( mpi::communicator& comm, const std::vector<int>& cellIDs, const bool partitioned,
                          const typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology>& xfldGroup,
                          const typename  Field<PhysDim, TopoDim, T>::template FieldCellGroupType<Topology>& qfldGroup,
                          FILE* fp )
{
  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename  Field<PhysDim, TopoDim, T>::template FieldCellGroupType<Topology> QFieldCellGroupType;

  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfldGroup.basis() );
  ElementQFieldClass qfldElem( qfldGroup.basis() );

  static const int N = DLA::VectorSize<T>::M;

  const int order = max(xfldElem.order(), qfldElem.order());
  int nRefine = max(order-1,0);
  ReferenceElementMesh<Topology> refMesh(nRefine);
  const int nRefnode = refMesh.nodes.size();
  const int nRefelem = refMesh.elems.size();

  int nElem_global;
  std::map<int,int> globalElemMap;
  continuousElementMap( comm, cellIDs, xfldGroup, nElem_global, globalElemMap);

  int nElemLocal = xfldGroup.nElem();

  // tecplot does not like empty groups
  if ( nElem_global == 0 ) return;
  if ( nElemLocal == 0 && partitioned ) return;

  if (partitioned) nElem_global = nElemLocal;

  int comm_rank = comm.rank();

  if (comm_rank == 0 || partitioned)
    fprintf( fp, "ZONE T=\"grid\", N=%d, E=%d, F=FEPOINT, ET=%s\n",
             nRefnode*nElem_global, nRefelem*nElem_global, TecTopo<Topology>::name() );

  // grid coordinates and solution

  typename ElementQFieldClass::RefCoordType sRefCell;
  typename ElementXFieldClass::VectorX X;
  T q;

#ifdef SANS_MPI
  if (!partitioned)
  {
    int comm_size = comm.size();

    // maximum element chunk size that rank 0 will write at any given time
    int Elemchunk = nElem_global / comm_size + nElem_global % comm_size;

    // send one chunk of elements at a time to rank 0
    for (int rank = 0; rank < comm_size; rank++)
    {
      int elemLow  =  rank   *Elemchunk;
      int elemHigh = (rank+1)*Elemchunk;

      std::map<int,std::vector<DLA::VectorS<PhysDim::D+N,double>>> buffer;

      // loop over elements within group
      for (int elem = 0; elem < nElemLocal; elem++)
      {
        int elemID = globalElemMap[cellIDs[elem]];

        if (elemID >= elemLow && elemID < elemHigh &&
            xfldGroup.associativity(elem).rank() == comm_rank)
        {
          // copy global grid/solution DOFs to element
          xfldGroup.getElement( xfldElem, elem );
          qfldGroup.getElement( qfldElem, elem );

          std::vector<DLA::VectorS<PhysDim::D+N,double>> nodeValues(nRefnode);

          for ( int i = 0; i < nRefnode; i++ )
          {
            sRefCell = refMesh.nodes[i];

            xfldElem.eval( sRefCell, X );
            qfldElem.eval( sRefCell, q );

            int n = 0;
            for (int k = 0; k < PhysDim::D; k++)
              nodeValues[i][n++] = X[k];
            for (int k = 0; k < N; k++)
              nodeValues[i][n++] = DLA::index(q,k);
          }

          buffer[elemID] = std::move(nodeValues);
        }
      }


      if (comm_rank == 0)
      {
        std::vector<std::map<int,std::vector<DLA::VectorS<PhysDim::D+N,double>>>> bufferOnRank;
        boost::mpi::gather(comm, buffer, bufferOnRank, 0 );

        // collapse down the buffer from all other ranks
        for (std::size_t i = 1; i < bufferOnRank.size(); i++)
          buffer.insert(bufferOnRank[i].begin(), bufferOnRank[i].end());

        //Write the node data
        for ( const auto& nodesPair : buffer)
          for ( int i = 0; i < nRefnode; i++ )
          {
            for (int n = 0; n < PhysDim::D+N; n++)
              fprintf( fp, "%22.15e ", nodesPair.second[i][n] );
            fprintf( fp, "\n" );
          }
      }
      else // send the buffer to rank 0
        boost::mpi::gather(comm, buffer, 0 );

    } // for rank

  }
  else // if partitioned
  {
#endif

    // loop over elements within group
    for (int elem = 0; elem < nElemLocal; elem++)
    {
      // copy global grid/solution DOFs to element
      xfldGroup.getElement( xfldElem, elem );
      qfldGroup.getElement( qfldElem, elem );

      for ( int i = 0; i < nRefnode; i++ )
      {
        sRefCell = refMesh.nodes[i];

        xfldElem.eval( sRefCell, X );
        qfldElem.eval( sRefCell, q );

        for (int k = 0; k < PhysDim::D; k++)
          fprintf( fp, "%22.15e ", X[k] );
        for (int k = 0; k < N; k++)
          fprintf( fp, "%22.15e ", DLA::index(q,k) );
        fprintf( fp, "\n" );
      }
    }

#ifdef SANS_MPI
  } // if !partitioned
#endif

  // cell-to-node connectivity

  if (comm_rank == 0 || partitioned)
    for (int elem = 0; elem < nElem_global; elem++)
    {
      int offset = nRefnode*elem;
      for (int relem = 0; relem < nRefelem; relem++)
      {
        for (int n = 0; n < Topology::NNode; n++ )
          fprintf( fp, "%d ", offset + refMesh.elems[relem][n] + 1 ); // +1 for one based indexing
        fprintf( fp, "\n" );
      }
    }
}

//----------------------------------------------------------------------------//
template< class T, class PhysDim, class TopoDim, class Topology>
void
output_Tecplot_CellGroup( const typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology>& xfld,
                          const typename FieldLift< PhysDim, TopoDim, DLA::VectorS<PhysDim::D,T> >::template FieldCellGroupType<Topology>& rfld,
                          FILE* fp )
{
  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
  // typedef typename FieldLift<PhysDim, TopoDim, DLA::VectorS<PhysDim::D,T> >::template FieldCellGroupType<Topology> RFieldCellGroupType;

  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef ElementLift<DLA::VectorS<PhysDim::D,T>, TopoDim, Topology> ElementRFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfld.basis() );
  ElementRFieldClass rfldElems( rfld.basis() );

  std::vector<Real> phi(rfldElems.nDOFElem()); // vector of basis functions

  static const int N = DLA::VectorSize<T>::M;

  const int order = max(xfldElem.order(), rfldElems.order());
  int nRefine = max(order-1,0);
  ReferenceElementMesh<Topology> refMesh(nRefine);
  const int nRefnode = refMesh.nodes.size();
  const int nRefelem = refMesh.elems.size();

  int nelem = xfld.nElem();

  // tecplot does not like empty groups
  if ( nelem == 0 ) return;

  fprintf( fp, "ZONE T=\"grid\", N=%d, E=%d, F=FEPOINT, ET=%s\n", nRefnode*nelem, nRefelem*nelem, TecTopo<Topology>::name() );

  // grid coordinates and solution

  typename ElementRFieldClass::RefCoordType sRefCell;
  typename ElementXFieldClass::VectorX X;
  DLA::VectorS<PhysDim::D,T> r;
  T q;

  // loop over elements within group
  for (int elem = 0; elem < nelem; elem++)
  {
    // copy global grid/solution DOFS to element
    xfld.getElement( xfldElem, elem );
    rfld.getElement( rfldElems, elem );

    for (int i = 0; i < nRefnode; i++)
    {
      sRefCell = refMesh.nodes[i];

      rfldElems.evalBasis( sRefCell, phi.data(), phi.size() );
      xfldElem.eval( sRefCell, X );
      q = 0;

      for (int trace = 0; trace < Topology::NTrace; trace++)
      {
        rfldElems[trace].evalFromBasis( phi.data(), phi.size(), r );

        T tmp = 0;
        for (int k = 0; k < DLA::VectorSize<T>::M; k++) // loop over state components
        {
          for (int d = 0; d < PhysDim::D; d++)
            DLA::index(tmp,k) += DLA::index(r[d],k)*DLA::index(r[d],k);
          DLA::index(tmp,k) = pow(DLA::index(tmp,k),0.5); // L2 norm of the vector
        }
        q+=tmp; // accumulates the norms of each state component
      }
      for (int k = 0; k < PhysDim::D; k++)
        fprintf( fp, "%22.15e ", X[k] );
      for (int k = 0; k < N; k++)
        fprintf( fp, "%22.15e ", DLA::index(q,k) );
      fprintf( fp, "\n" );
    }
  }

  // cell-to-node connectivity

  for (int elem = 0; elem < nelem; elem++)
  {
    int offset = nRefnode*elem;
    for (int relem = 0; relem < nRefelem; relem++)
    {
      for (int n = 0; n < Topology::NNode; n++ )
        fprintf( fp, "%d ", offset + refMesh.elems[relem][n] + 1 ); // +1 for one based indexing
      fprintf( fp, "\n" );
    }
  }
}

//----------------------------------------------------------------------------//
template<class T, class PhysDim, class TopoDim, class TopologyCell, class TopologyTrace>
void
output_Tecplot_TraceCellGroup( const int groupTrace,
                               mpi::communicator& comm, const std::vector<int>& boundaryTraceIDs, const bool partitioned,
                               const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
                               const typename  Field<PhysDim, TopoDim, T>::template FieldCellGroupType<TopologyCell>& qfldCellL,
                               FILE* fp )
{
  typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename  Field<PhysDim, TopoDim, T>::template FieldCellGroupType<TopologyCell> QFieldCellGroupType;

  typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldClass;
  typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfldTrace.basis() );
  ElementQFieldClass qfldElem( qfldCellL.basis() );

  static const int N = DLA::VectorSize<T>::M;

  const int order = max(xfldElem.order(), qfldElem.order());
  int nRefine = max(order-1,0);
  ReferenceElementMesh<TopologyTrace> refMesh(nRefine);
  const int nRefnode = refMesh.nodes.size();
  const int nRefelem = refMesh.elems.size();

  int nElem_global;
  std::map<int,int> globalElemMap;
  continuousElementMap( comm, boundaryTraceIDs, xfldTrace, nElem_global, globalElemMap);

  int nElemLocal = xfldTrace.nElem();

  // tecplot does not like empty groups
  if ( nElem_global == 0 ) return;
  if ( nElemLocal == 0 && partitioned ) return;

  if (partitioned) nElem_global = nElemLocal;

  int comm_rank = comm.rank();

  if (comm_rank == 0 || partitioned)
    fprintf( fp, "ZONE T=\"Boundary %d\", N=%d, E=%d, F=FEPOINT, ET=%s\n",
             groupTrace, nRefnode*nElem_global, nRefelem*nElem_global, TecTopo<TopologyTrace>::name() );

  typename ElementQFieldClass::RefCoordType sRefCell;
  typename ElementXFieldClass::RefCoordType sRefTrace;
  typename ElementXFieldClass::VectorX X;
  T q;

#ifdef SANS_MPI
  if (!partitioned)
  {
    int comm_size = comm.size();

    // maximum element chunk size that rank 0 will write at any given time
    int Elemchunk = nElem_global / comm_size + nElem_global % comm_size;

    // send one chunk of elements at a time to rank 0
    for (int rank = 0; rank < comm_size; rank++)
    {
      int elemLow  =  rank   *Elemchunk;
      int elemHigh = (rank+1)*Elemchunk;

      std::map<int,std::vector<DLA::VectorS<PhysDim::D+N,double>>> buffer;

      // loop over elements within group
      for (int elem = 0; elem < nElemLocal; elem++)
      {
        int elemID = globalElemMap[boundaryTraceIDs[elem]];

        if (elemID >= elemLow && elemID < elemHigh &&
            xfldTrace.associativity(elem).rank() == comm_rank)
        {
           const int elemL = xfldTrace.getElementLeft( elem );
           CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

           xfldTrace.getElement( xfldElem, elem );
           qfldCellL.getElement( qfldElem, elemL );

           std::vector<DLA::VectorS<PhysDim::D+N,double>> nodeValues(nRefnode);

           for ( int i = 0; i < nRefnode; i++ )
           {
             sRefTrace = refMesh.nodes[i];

             // adjacent cell-element reference coords
             TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyCell>::eval( canonicalTraceL, sRefTrace, sRefCell );

             xfldElem.eval( sRefTrace, X );
             qfldElem.eval( sRefCell, q );

             int n = 0;
             for (int k = 0; k < PhysDim::D; k++)
               nodeValues[i][n++] = X[k];
             for (int k = 0; k < N; k++)
               nodeValues[i][n++] = DLA::index(q,k);
           }

          buffer[elemID] = std::move(nodeValues);
        }
      }


      if (comm_rank == 0)
      {
        std::vector<std::map<int,std::vector<DLA::VectorS<PhysDim::D+N,double>>>> bufferOnRank;
        boost::mpi::gather(comm, buffer, bufferOnRank, 0 );

        // collapse down the buffer from all other ranks
        for (std::size_t i = 1; i < bufferOnRank.size(); i++)
          buffer.insert(bufferOnRank[i].begin(), bufferOnRank[i].end());

        //Write the node data
        for ( const auto& nodesPair : buffer)
          for ( int i = 0; i < nRefnode; i++ )
          {
            for (int n = 0; n < PhysDim::D+N; n++)
              fprintf( fp, "%22.15e ", nodesPair.second[i][n] );
            fprintf( fp, "\n" );
          }
      }
      else // send the buffer to rank 0
        boost::mpi::gather(comm, buffer, 0 );

    } // for rank

  }
  else // if partitioned
  {
#endif

  for (int elem = 0; elem < nElemLocal; elem++)
  {
    const int elemL = xfldTrace.getElementLeft( elem );
    CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

    xfldTrace.getElement( xfldElem, elem );
    qfldCellL.getElement( qfldElem, elemL );

    for ( int i = 0; i < nRefnode; i++ )
    {
      sRefTrace = refMesh.nodes[i];

      // adjacent cell-element reference coords
      TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyCell>::eval( canonicalTraceL, sRefTrace, sRefCell );

      xfldElem.eval( sRefTrace, X );
      qfldElem.eval( sRefCell, q );

      for (int k = 0; k < PhysDim::D; k++)
        fprintf( fp, "%22.15e ", X[k] );
      for (int k = 0; k < N; k++)
        fprintf( fp, "%22.15e ", DLA::index(q,k) );
      fprintf( fp, "\n" );

    }
  }

#ifdef SANS_MPI
  } // if !partitioned
#endif

  // cell-to-node connectivity

  if (comm_rank == 0 || partitioned)
    for (int elem = 0; elem < nElem_global; elem++)
    {
      int offset = nRefnode*elem;
      for (int relem = 0; relem < nRefelem; relem++)
      {
        for (int n = 0; n < TopologyTrace::NNode; n++ )
          fprintf( fp, "%d ", offset + refMesh.elems[relem][n] + 1 ); // +1 for one based indexing
        fprintf( fp, "\n" );
      }
    }
}

//----------------------------------------------------------------------------//
template<class T, class PhysDim, class TopoDim, class Topology>
void
output_Tecplot_TraceGroup( const int group,
                           const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Topology>& xfldTrace,
                           const typename  Field<PhysDim, TopoDim, T>::template FieldTraceGroupType<Topology>& qfldTrace,
                           FILE* fp )
{
  typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Topology> XFieldTraceGroupType;
  typedef typename  Field<PhysDim, TopoDim, T>::template FieldTraceGroupType<Topology> QFieldTraceGroupType;

  typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldClass;
  typedef typename QFieldTraceGroupType::template ElementType<> ElementQFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfldTrace.basis() );
  ElementQFieldClass qfldElem( qfldTrace.basis() );

  static const int N = DLA::VectorSize<T>::M;

  const int order = max(xfldElem.order(), qfldElem.order());
  int nRefine = max(order-1,0);
  ReferenceElementMesh<Topology> refMesh(nRefine);
  const int nRefnode = refMesh.nodes.size();
  const int nRefelem = refMesh.elems.size();

  SANS_ASSERT(xfldTrace.nElem() == qfldTrace.nElem());

  int nelem = xfldTrace.nElem();

  // tecplot does not like empty groups
  if ( nelem == 0 ) return;

  fprintf( fp, "ZONE T=\"Boundary TraceElem %d\", N=%d, E=%d, F=FEPOINT, ET=%s\n", group, nRefnode*nelem, nRefelem*nelem, TecTopo<Topology>::name() );

  typename ElementQFieldClass::RefCoordType sRefTrace;
  typename ElementXFieldClass::VectorX X;

  T q;
  for (int elem = 0; elem < nelem; elem++)
  {
    xfldTrace.getElement( xfldElem, elem );
    qfldTrace.getElement( qfldElem, elem );

    for ( int i = 0; i < nRefnode; i++ )
    {
      sRefTrace = refMesh.nodes[i];

      xfldElem.eval( sRefTrace, X );
      qfldElem.eval( sRefTrace, q );

      for (int k = 0; k < PhysDim::D; k++)
        fprintf( fp, "%22.15e ", X[k] );
      for (int k = 0; k < N; k++)
        fprintf( fp, "%22.15e ", DLA::index(q,k) );
      fprintf( fp, "\n" );

    }
  }

  // cell-to-node connectivity

  for (int elem = 0; elem < nelem; elem++)
  {
    int offset = nRefnode*elem;
    for (int relem = 0; relem < nRefelem; relem++)
    {
      for (int n = 0; n < Topology::NNode; n++ )
        fprintf( fp, "%d ", offset + refMesh.elems[relem][n] + 1 ); // +1 for one based indexing
      fprintf( fp, "\n" );
    }
  }
}

//----------------------------------------------------------------------------//
template<class PhysDim, class T>
void
output_Tecplot( const Field< PhysDim, TopoD1, T >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names,
                const bool partitioned )
{
  FILE* fp = nullptr;

  if (qfld.comm()->rank() == 0 || partitioned)
  {
    std::cout << "output_Tecplot: filename = " << filename << std::endl;
    fp = fopen( filename.c_str(), "w" );
    if (fp == NULL)
      SANS_DEVELOPER_EXCEPTION("output_Tecplot<PhysDim,TopoD1> - Error opening file: %s", filename.c_str());

    fprintf( fp, "\"\"\n" );
    fprintf( fp, "VARIABLES = \"X\"" );

    const char XYZ[] = "XYZ";
    for (int k = 1; k < PhysDim::D; k++)
      fprintf( fp, ", \"%c\"", XYZ[k] );

    static const int N = DLA::VectorSize<T>::M;
    if (state_names.empty())
    {
      for (int k = 0; k < N; k++)
        fprintf( fp, ", \"q%d\"", k+1 );
    }
    else
    {
      SANS_ASSERT((int) state_names.size() == N);
      for (int k = 0; k < N; k++)
        fprintf( fp, ", \"%s\"", state_names[k].c_str());
    }
    fprintf( fp, "\n" );
  }

  const XField<PhysDim,TopoD1>& xgrid = qfld.getXField();

  const int ngroup = qfld.nCellGroups();

  // loop over cell groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Line) )
    {
      output_Tecplot_CellGroup<T, PhysDim, TopoD1, Line>( *xgrid.comm(), xgrid.cellIDs(group), partitioned,
                                                          xgrid.template getCellGroup<Line>(group),
                                                           qfld.template getCellGroup<Line>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD1,TopoD1>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

#if 0
  // loop over trace boundary groups
  for (int group = 0; group < qfld.nBoundaryTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      output_Tecplot_TraceGroup<N>( group, xgrid.template getBoundaryTraceGroup<Triangle>(group),
                                            qfld.template getBoundaryTraceGroup<Triangle>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD3,TopoD3>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }
#endif

  if (qfld.comm()->rank() == 0 || partitioned)
    fclose( fp );
}

//----------------------------------------------------------------------------//
template<class PhysDim, class T>
void
output_Tecplot( const Field< PhysDim, TopoD2, T >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names,
                const bool partitioned )
{
  FILE* fp = nullptr;

  if (qfld.comm()->rank() == 0 || partitioned)
  {
    std::cout << "output_Tecplot: filename = " << filename << std::endl;
    fp = fopen( filename.c_str(), "w" );
    if (fp == NULL)
      SANS_RUNTIME_EXCEPTION("output_Tecplot<PhysDim,TopoD2> - Error opening file: %s", filename.c_str());

    fprintf( fp, "\"\"\n" );
    fprintf( fp, "VARIABLES = \"X\"" );

    const char XYZ[] = "XYZ";
    for (int k = 1; k < PhysDim::D; k++)
      fprintf( fp, ", \"%c\"", XYZ[k] );

    static const int N = DLA::VectorSize<T>::M;
    if (state_names.empty())
    {
      for (int k = 0; k < N; k++)
        fprintf( fp, ", \"q%d\"", k+1 );
    }
    else
    {
      SANS_ASSERT((int) state_names.size() == N);
      for (int k = 0; k < N; k++)
        fprintf( fp, ", \"%s\"", state_names[k].c_str());
    }
    fprintf( fp, "\n" );
  }

  const XField<PhysDim,TopoD2>& xgrid = qfld.getXField();

  const int ngroup = qfld.nCellGroups();

  // loop over cell groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      output_Tecplot_CellGroup<T, PhysDim, TopoD2, Triangle>( *xgrid.comm(), xgrid.cellIDs(group), partitioned,
                                                              xgrid.template getCellGroup<Triangle>(group),
                                                               qfld.template getCellGroup<Triangle>(group), fp );
    }
    else if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Quad) )
    {
      output_Tecplot_CellGroup<T, PhysDim, TopoD2, Quad>( *xgrid.comm(), xgrid.cellIDs(group), partitioned,
                                                          xgrid.template getCellGroup<Quad>(group),
                                                           qfld.template getCellGroup<Quad>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD2,TopoD2>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }


  // loop over trace boundary groups
  for (int groupTrace = 0; groupTrace < xgrid.nBoundaryTraceGroups(); groupTrace++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(groupTrace).topoTypeID() == typeid(Line) )
    {
      // determine topology for Left cell group
      int groupL = xgrid.template getBoundaryTraceGroup<Line>(groupTrace).getGroupLeft();

      if ( xgrid.getCellGroupBase(groupL).topoTypeID() == typeid(Triangle) )
        output_Tecplot_TraceCellGroup<T, PhysDim, TopoD2, Triangle, Line>( groupTrace,
                                                                           *xgrid.comm(), xgrid.boundaryTraceIDs(groupTrace), partitioned,
                                                                           xgrid.template getBoundaryTraceGroup<Line>(groupTrace),
                                                                           qfld.template getCellGroup<Triangle>(groupL), fp );
      else if ( xgrid.getCellGroupBase(groupL).topoTypeID() == typeid(Quad) )
        output_Tecplot_TraceCellGroup<T, PhysDim, TopoD2, Quad, Line>( groupTrace,
                                                                       *xgrid.comm(), xgrid.boundaryTraceIDs(groupTrace), partitioned,
                                                                       xgrid.template getBoundaryTraceGroup<Line>(groupTrace),
                                                                       qfld.template getCellGroup<Quad>(groupL), fp );
      else
        SANS_DEVELOPER_EXCEPTION("Unknown topology");
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD2,TopoD2>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  if (ngroup  == 0)
  {
    SANS_ASSERT_MSG(qfld.comm()->size() == 0, "Pure trace fields cannot be written in parallel yet");

    // loop over trace boundary groups
    for (int group = 0; group < qfld.nBoundaryTraceGroups(); group++)
    {
      // dispatch integration over cell of group
      if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Line) )
      {
        output_Tecplot_TraceGroup<T, PhysDim, TopoD2, Line>( group,
                                                             xgrid.template getBoundaryTraceGroup<Line>(group),
                                                             qfld.template getBoundaryTraceGroup<Line>(group), fp );
      }
      else
      {
        const char msg[] = "Error in output_Tecplot(XField<PhysD2,TopoD2>): unknown topology\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }
  }

  if (qfld.comm()->rank() == 0 || partitioned)
    fclose( fp );
}

//----------------------------------------------------------------------------//
template<class T>
void
output_Tecplot( const Field< PhysD3, TopoD3, T >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names,
                const bool partitioned )
{
  FILE* fp = nullptr;

  if (qfld.comm()->rank() == 0 || partitioned)
  {
    std::cout << "output_Tecplot: filename = " << filename << std::endl;
    fp = fopen( filename.c_str(), "w" );
    if (fp == NULL)
      SANS_RUNTIME_EXCEPTION("output_Tecplot<PhysD3,TopoD3> - Error opening file: %s", filename.c_str());

    fprintf( fp, "\"\"\n" );
    fprintf( fp, "VARIABLES = \"X\", \"Y\", \"Z\"" );

    static const int N = DLA::VectorSize<T>::M;
    if (state_names.empty())
    {
      for (int k = 0; k < N; k++)
        fprintf( fp, ", \"q%d\"", k+1 );
    }
    else
    {
      SANS_ASSERT((int) state_names.size() == N);
      for (int k = 0; k < N; k++)
        fprintf( fp, ", \"%s\"", state_names[k].c_str());
    }
    fprintf( fp, "\n" );
  }

  const XField<PhysD3,TopoD3>& xgrid = qfld.getXField();

  const int ngroup = qfld.nCellGroups();

  // loop over cell groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Tet) )
    {
      output_Tecplot_CellGroup<T, PhysD3, TopoD3, Tet>( *xgrid.comm(), xgrid.cellIDs(group), partitioned,
                                                        xgrid.template getCellGroup<Tet>(group),
                                                        qfld.template getCellGroup<Tet>(group), fp );
    }
    else if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Hex) )
    {
      output_Tecplot_CellGroup<T, PhysD3, TopoD3, Hex>( *xgrid.comm(), xgrid.cellIDs(group), partitioned,
                                                        xgrid.template getCellGroup<Hex>(group),
                                                        qfld.template getCellGroup<Hex>(group), fp );
    }
    else
      SANS_DEVELOPER_EXCEPTION("Unknown topology");
  }

  // loop over trace boundary groups
  for (int groupTrace = 0; groupTrace < xgrid.nBoundaryTraceGroups(); groupTrace++)
  {

    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(groupTrace).topoTypeID() == typeid(Triangle) )
    {
      // determine topology for Left cell group
      int groupL = xgrid.template getBoundaryTraceGroup<Triangle>(groupTrace).getGroupLeft();

      if ( xgrid.getCellGroupBase(groupL).topoTypeID() == typeid(Tet) )
        output_Tecplot_TraceCellGroup<T, PhysD3, TopoD3, Tet, Triangle>( groupTrace,
                                                                         *xgrid.comm(), xgrid.boundaryTraceIDs(groupTrace), partitioned,
                                                                         xgrid.template getBoundaryTraceGroup<Triangle>(groupTrace),
                                                                          qfld.template getCellGroup<Tet>(groupL), fp );
      else
        SANS_DEVELOPER_EXCEPTION("Unknown topology");
    }
    else if ( xgrid.getBoundaryTraceGroupBase(groupTrace).topoTypeID() == typeid(Quad) )
    {
      // determine topology for Left cell group
      int groupL = xgrid.template getBoundaryTraceGroup<Quad>(groupTrace).getGroupLeft();

      if ( xgrid.getCellGroupBase(groupL).topoTypeID() == typeid(Hex) )
        output_Tecplot_TraceCellGroup<T, PhysD3, TopoD3, Hex, Quad>( groupTrace,
                                                                     *xgrid.comm(), xgrid.boundaryTraceIDs(groupTrace), partitioned,
                                                                     xgrid.template getBoundaryTraceGroup<Quad>(groupTrace),
                                                                      qfld.template getCellGroup<Hex>(groupL), fp );
      else
        SANS_DEVELOPER_EXCEPTION("Unknown topology");
    }
    else
      SANS_DEVELOPER_EXCEPTION("Unknown topology");
  }

  if (ngroup  == 0)
  {
    SANS_ASSERT_MSG(qfld.comm()->size() == 0, "Pure trace fields cannot be written in parallel yet");

    // loop over trace boundary groups
    for (int group = 0; group < qfld.nBoundaryTraceGroups(); group++)
    {
      // dispatch integration over cell of group
      if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Triangle) )
      {
        output_Tecplot_TraceGroup<T, PhysD3, TopoD3, Triangle>( group, xgrid.template getBoundaryTraceGroup<Triangle>(group),
                                                                qfld.template getBoundaryTraceGroup<Triangle>(group), fp );
      }
      else if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Quad) )
      {
        output_Tecplot_TraceGroup<T, PhysD3, TopoD3, Quad>( group, xgrid.template getBoundaryTraceGroup<Quad>(group),
                                                            qfld.template getBoundaryTraceGroup<Quad>(group), fp );
      }
      else
      {
        const char msg[] = "Error in output_Tecplot(XField<PhysD3,TopoD3>): unknown topology\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }
  }

  if (qfld.comm()->rank() == 0 || partitioned)
    fclose( fp );
}


//----------------------------------------------------------------------------//
template<class T>
void
output_Tecplot( const Field< PhysD4, TopoD4, T >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names,
                const bool partitioned )
{
  FILE* fp = nullptr;

  if (qfld.comm()->rank() == 0 || partitioned)
  {
    std::cout << "output_Tecplot: filename = " << filename << std::endl;
    fp = fopen( filename.c_str(), "w" );
    if (fp == NULL)
      SANS_RUNTIME_EXCEPTION("output_Tecplot<PhysD4,TopoD4> - Error opening file: %s", filename.c_str());

    fprintf( fp, "\"\"\n" );
    fprintf( fp, "VARIABLES = \"X\", \"Y\", \"Z\", \"T\"" );

    static const int N = DLA::VectorSize<T>::M;
    if (state_names.empty())
    {
      for (int k = 0; k < N; k++)
        fprintf( fp, ", \"q%d\"", k+1 );
    }
    else
    {
      SANS_ASSERT((int) state_names.size() == N);
      for (int k = 0; k < N; k++)
        fprintf( fp, ", \"%s\"", state_names[k].c_str());
    }
    fprintf( fp, "\n" );
  }

  const XField<PhysD4,TopoD4>& xgrid = qfld.getXField();

  const int ngroup = qfld.nCellGroups();

  // loop over cell groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Pentatope) )
    {
      output_Tecplot_CellGroup<T, PhysD4, TopoD4, Pentatope>( *xgrid.comm(), xgrid.cellIDs(group), partitioned,
                                                              xgrid.template getCellGroup<Pentatope>(group),
                                                              qfld.template getCellGroup<Pentatope>(group), fp );
    }
    else
      SANS_DEVELOPER_EXCEPTION("Unknown topology");
  }

  // loop over trace boundary groups
  for (int groupTrace = 0; groupTrace < xgrid.nBoundaryTraceGroups(); groupTrace++)
  {

    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(groupTrace).topoTypeID() == typeid(Tet) )
    {
      // determine topology for Left cell group
      int groupL = xgrid.template getBoundaryTraceGroup<Tet>(groupTrace).getGroupLeft();

      if ( xgrid.getCellGroupBase(groupL).topoTypeID() == typeid(Pentatope) )
        output_Tecplot_TraceCellGroup<T, PhysD4, TopoD4, Pentatope, Tet>( groupTrace,
                                                                          *xgrid.comm(), xgrid.boundaryTraceIDs(groupTrace), partitioned,
                                                                           xgrid.template getBoundaryTraceGroup<Tet>(groupTrace),
                                                                           qfld.template getCellGroup<Pentatope>(groupL), fp );
      else
        SANS_DEVELOPER_EXCEPTION("Unknown topology");
    }
    else
      SANS_DEVELOPER_EXCEPTION("Unknown topology");
  }

  if (ngroup  == 0)
  {
    SANS_ASSERT_MSG(qfld.comm()->size() == 0, "Pure trace fields cannot be written in parallel yet");

    // loop over trace boundary groups
    for (int group = 0; group < qfld.nBoundaryTraceGroups(); group++)
    {
      // dispatch integration over cell of group
      if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Tet) )
      {
        output_Tecplot_TraceGroup<T, PhysD4, TopoD4, Tet>( group, xgrid.template getBoundaryTraceGroup<Tet>(group),
                                                                qfld.template getBoundaryTraceGroup<Tet>(group), fp );
      }
      else
      {
        const char msg[] = "Error in output_Tecplot(XField<PhysD4,TopoD4>): unknown topology\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }
  }

  if (qfld.comm()->rank() == 0 || partitioned)
    fclose( fp );
}

//----------------------------------------------------------------------------//
//
template<class PhysDim, class T>
void
output_Tecplot_LO( const FieldLift< PhysDim, TopoD1, DLA::VectorS<PhysDim::D,T> >& rfld, const std::string& filename,
                   const std::vector<std::string>& state_names,
                   const bool partitioned )
{
  FILE* fp = nullptr;

  if (rfld.comm()->rank() == 0 || partitioned)
  {
    std::cout << "output_Tecplot: filename = " << filename << std::endl;
    fp = fopen( filename.c_str(), "w" );

    fprintf( fp, "\"\"\n" );
    fprintf( fp, "VARIABLES = \"X\"" );

    const char XYZ[] = "XYZ";
    for (int k = 1; k < PhysDim::D; k++)
      fprintf( fp, ", \"%c\"", XYZ[k] );

    static const int N = DLA::VectorSize<T>::M;
    if (state_names.empty())
    {
      for (int k = 0; k < N; k++)
        fprintf( fp, ", \"q%d\"", k+1 );
    }
    else
    {
      SANS_ASSERT((int) state_names.size() == N);
      for (int k = 0; k < N; k++)
        fprintf( fp, ", \"%s\"", state_names[k].c_str());
    }
    fprintf( fp, "\n" );
  }

  const XField<PhysDim,TopoD1>& xgrid = rfld.getXField();

  const int ngroup = rfld.nCellGroups();

  // loop over cell groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Line) )
    {
      output_Tecplot_CellGroup<T, PhysDim, TopoD1, Line>( xgrid.template getCellGroup<Line>(group),
                                                          rfld.template getCellGroup<Line>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD,TopoD1>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  if (rfld.comm()->rank() == 0 || partitioned)
      fclose( fp );
}

//----------------------------------------------------------------------------//
//
template<class PhysDim, class T>
void
output_Tecplot_LO( const FieldLift< PhysDim, TopoD2, DLA::VectorS<PhysDim::D,T> >& rfld, const std::string& filename,
                   const std::vector<std::string>& state_names,
                   const bool partitioned )
{
  FILE* fp = nullptr;

  if (rfld.comm()->rank() == 0 || partitioned)
  {
    std::cout << "output_Tecplot: filename = " << filename << std::endl;
    fp = fopen( filename.c_str(), "w" );

    fprintf( fp, "\"\"\n" );
    fprintf( fp, "VARIABLES = \"X\"" );

    const char XYZ[] = "XYZ";
    for (int k = 1; k < PhysDim::D; k++)
      fprintf( fp, ", \"%c\"", XYZ[k] );

    static const int N = DLA::VectorSize<T>::M;
    if (state_names.empty())
    {
      for (int k = 0; k < N; k++)
        fprintf( fp, ", \"q%d\"", k+1 );
    }
    else
    {
      SANS_ASSERT((int) state_names.size() == N);
      for (int k = 0; k < N; k++)
        fprintf( fp, ", \"%s\"", state_names[k].c_str());
    }
    fprintf( fp, "\n" );
  }

  const XField<PhysDim,TopoD2>& xgrid = rfld.getXField();

  const int ngroup = rfld.nCellGroups();

  // loop over cell groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      output_Tecplot_CellGroup<T, PhysDim, TopoD2, Triangle>( xgrid.template getCellGroup<Triangle>(group),
                                                               rfld.template getCellGroup<Triangle>(group), fp );
    }
    else if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Quad) )
    {
      output_Tecplot_CellGroup<T, PhysDim, TopoD2, Quad>( xgrid.template getCellGroup<Quad>(group),
                                                           rfld.template getCellGroup<Quad>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD2,TopoD2>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  if (rfld.comm()->rank() == 0 || partitioned)
      fclose( fp );
}

//----------------------------------------------------------------------------//
//
template<class PhysDim, class T>
void
output_Tecplot_LO( const FieldLift< PhysDim, TopoD3, DLA::VectorS<PhysDim::D,T> >& rfld, const std::string& filename,
                   const std::vector<std::string>& state_names,
                   const bool partitioned )
{
  FILE* fp = nullptr;

  if (rfld.comm()->rank() == 0 || partitioned)
  {
    std::cout << "output_Tecplot: filename = " << filename << std::endl;
    fp = fopen( filename.c_str(), "w" );

    fprintf( fp, "\"\"\n" );
    fprintf( fp, "VARIABLES = \"X\"" );

    const char XYZ[] = "XYZ";
    for (int k = 1; k < PhysDim::D; k++)
      fprintf( fp, ", \"%c\"", XYZ[k] );

    static const int N = DLA::VectorSize<T>::M;
    if (state_names.empty())
    {
      for (int k = 0; k < N; k++)
        fprintf( fp, ", \"q%d\"", k+1 );
    }
    else
    {
      SANS_ASSERT((int) state_names.size() == N);
      for (int k = 0; k < N; k++)
        fprintf( fp, ", \"%s\"", state_names[k].c_str());
    }
    fprintf( fp, "\n" );
  }

  const XField<PhysDim,TopoD3>& xgrid = rfld.getXField();

  const int ngroup = rfld.nCellGroups();

  // loop over cell groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Tet) )
    {
      output_Tecplot_CellGroup<T, PhysDim, TopoD3, Tet>( xgrid.template getCellGroup<Tet>(group),
                                                         rfld.template getCellGroup<Tet>(group), fp );
    }
    else if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Hex) )
    {
      output_Tecplot_CellGroup<T, PhysDim, TopoD3, Hex>( xgrid.template getCellGroup<Hex>(group),
                                                         rfld.template getCellGroup<Hex>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD,TopoD3>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  if (rfld.comm()->rank() == 0 || partitioned)
      fclose( fp );
}



//Explicitly instantiate the function
// but explicit instantiation that occurs after an explicit specialization has no effect
//template void
//output_Tecplot( const XField<PhysD1,TopoD1>& xgrid, const std::string& filename );
//template void
//output_Tecplot( const XField<PhysD2,TopoD1>& xgrid, const std::string& filename );
//template void
//output_Tecplot( const XField<PhysD2,TopoD2>& xgrid, const std::string& filename );
//template void
//output_Tecplot( const XField<PhysD3,TopoD3>& xgrid, const std::string& filename );


template void
output_Tecplot( const Field< PhysD1, TopoD1, Real >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD2, TopoD1, Real >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD2, TopoD2, Real >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD3, TopoD2, Real >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD3, TopoD3, Real >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD4, TopoD4, Real >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );


template void
output_Tecplot( const Field< PhysD1, TopoD1, DLA::VectorS<1,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD1, TopoD1, DLA::VectorS<2,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD1, TopoD1, DLA::VectorS<3,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD1, TopoD1, DLA::VectorS<4,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD1, TopoD1, DLA::VectorS<5,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );

template void
output_Tecplot( const Field< PhysD2, TopoD1, DLA::VectorS<1,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD2, TopoD1, DLA::VectorS<2,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD2, TopoD1, DLA::VectorS<3,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD2, TopoD1, DLA::VectorS<4,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD2, TopoD1, DLA::VectorS<6,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD2, TopoD1, DLA::VectorS<8,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD2, TopoD1, DLA::VectorS<22,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );

template void
output_Tecplot( const Field< PhysD2, TopoD2, DLA::VectorS<1,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD2, TopoD2, DLA::VectorS<2,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD2, TopoD2, DLA::VectorS<3,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD2, TopoD2, DLA::VectorS<4,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD2, TopoD2, DLA::VectorS<5,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD2, TopoD2, DLA::VectorS<6,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );

template void
output_Tecplot( const Field< PhysD3, TopoD2, DLA::VectorS<1,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );

template void
output_Tecplot( const Field< PhysD3, TopoD3, DLA::VectorS<1,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD3, TopoD3, DLA::VectorS<2,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD3, TopoD3, DLA::VectorS<3,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD3, TopoD3, DLA::VectorS<4,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD3, TopoD3, DLA::VectorS<5,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD3, TopoD3, DLA::VectorS<6,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD3, TopoD3, DLA::VectorS<7,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );

template void
output_Tecplot( const Field< PhysD4, TopoD4, DLA::VectorS<1,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD4, TopoD4, DLA::VectorS<2,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD4, TopoD4, DLA::VectorS<3,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD4, TopoD4, DLA::VectorS<4,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD4, TopoD4, DLA::VectorS<5,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD4, TopoD4, DLA::VectorS<6,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD4, TopoD4, DLA::VectorS<7,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );


// template void
// output_Tecplot_LO( const FieldLift< PhysD2, TopoD2, DLA::VectorS<1,Real> >& Q, const std::string& filename,
//                    const std::vector<std::string>& state_names );

// // 1D
// template void
// output_Tecplot_LO( const FieldLift< PhysD1, TopoD1, DLA::VectorS<PhysD1::D,Real> >& Q, const std::string& filename,
//                    const std::vector<std::string>& state_names );
// template void
// output_Tecplot_LO( const FieldLift< PhysD1, TopoD1, DLA::VectorS<PhysD1::D,DLA::VectorS<1,Real>> >& Q, const std::string& filename,
//                    const std::vector<std::string>& state_names );
// template void
// output_Tecplot_LO( const FieldLift< PhysD1, TopoD1, DLA::VectorS<PhysD1::D,DLA::VectorS<2,Real>> >& Q, const std::string& filename,
//                    const std::vector<std::string>& state_names );
// template void
// output_Tecplot_LO( const FieldLift< PhysD1, TopoD1, DLA::VectorS<PhysD1::D,DLA::VectorS<3,Real>> >& Q, const std::string& filename,
//                    const std::vector<std::string>& state_names );
// template void
// output_Tecplot_LO( const FieldLift< PhysD1, TopoD1, DLA::VectorS<PhysD1::D,DLA::VectorS<4,Real>> >& Q, const std::string& filename,
//                    const std::vector<std::string>& state_names );
// template void
// output_Tecplot_LO( const FieldLift< PhysD1, TopoD1, DLA::VectorS<PhysD1::D,DLA::VectorS<5,Real>> >& Q, const std::string& filename,
//                    const std::vector<std::string>& state_names );

// 2D
template void
output_Tecplot_LO( const FieldLift< PhysD2, TopoD2, DLA::VectorS<PhysD2::D,Real> >& Q, const std::string& filename,
                  const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot_LO( const FieldLift< PhysD2, TopoD2, DLA::VectorS<PhysD2::D,DLA::VectorS<1,Real>> >& Q, const std::string& filename,
                  const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot_LO( const FieldLift< PhysD2, TopoD2, DLA::VectorS<PhysD2::D,DLA::VectorS<2,Real>> >& Q, const std::string& filename,
                  const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot_LO( const FieldLift< PhysD2, TopoD2, DLA::VectorS<PhysD2::D,DLA::VectorS<3,Real>> >& Q, const std::string& filename,
                  const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot_LO( const FieldLift< PhysD2, TopoD2, DLA::VectorS<PhysD2::D,DLA::VectorS<4,Real>> >& Q, const std::string& filename,
                  const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot_LO( const FieldLift< PhysD2, TopoD2, DLA::VectorS<PhysD2::D,DLA::VectorS<5,Real>> >& Q, const std::string& filename,
                  const std::vector<std::string>& state_names, const bool partitioned );

// // 3D
// template void
// output_Tecplot_LO( const FieldLift< PhysD3, TopoD3, DLA::VectorS<PhysD3::D,Real> >& Q, const std::string& filename,
//                  const std::vector<std::string>& state_names );
// template void
// output_Tecplot_LO( const FieldLift< PhysD3, TopoD3, DLA::VectorS<PhysD3::D,DLA::VectorS<1,Real>> >& Q, const std::string& filename,
//                  const std::vector<std::string>& state_names );
// template void
// output_Tecplot_LO( const FieldLift< PhysD3, TopoD3, DLA::VectorS<PhysD3::D,DLA::VectorS<2,Real>> >& Q, const std::string& filename,
//                  const std::vector<std::string>& state_names );
// template void
// output_Tecplot_LO( const FieldLift< PhysD3, TopoD3, DLA::VectorS<PhysD3::D,DLA::VectorS<3,Real>> >& Q, const std::string& filename,
//                  const std::vector<std::string>& state_names );
// template void
// output_Tecplot_LO( const FieldLift< PhysD3, TopoD3, DLA::VectorS<PhysD3::D,DLA::VectorS<4,Real>> >& Q, const std::string& filename,
//                  const std::vector<std::string>& state_names );
// template void
// output_Tecplot_LO( const FieldLift< PhysD3, TopoD3, DLA::VectorS<PhysD3::D,DLA::VectorS<5,Real>> >& Q, const std::string& filename,
//                  const std::vector<std::string>& state_names );

} //namespace SANS
