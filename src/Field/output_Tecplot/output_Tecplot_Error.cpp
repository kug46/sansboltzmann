// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <sstream>

#include "Field/output_Tecplot.h"
#include "TecTopo.h"

#include "tools/output_std_vector.h"

#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"

#include "pde/NDConvert/FunctionNDConvertSpace1D.h"
#include "pde/NDConvert/FunctionNDConvertSpace2D.h"
#include "pde/NDConvert/FunctionNDConvertSpace3D.h"

#include "Field/XFieldLine.h"
#include "Field/XFieldArea.h"
#include "Field/XFieldVolume.h"

#include "Field/FieldLine.h"
#include "Field/FieldArea.h"
#include "Field/FieldVolume.h"

#include "Field/Element/ReferenceElementMesh.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"
#include "MPI/continuousElementMap.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template<class T, class PhysDim, class TopoDim, class Topology>
void
output_Tecplot_CellGroup( mpi::communicator& comm, const FunctionNDConvertSpace<PhysDim, T>& sln,
                          const std::vector<int>& cellIDs, const bool partitioned,
                          const typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology>& xfldGroup,
                          const typename  Field<PhysDim, TopoDim, T>::template FieldCellGroupType<Topology>& qfldGroup,
                          FILE* fp )
{
  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename  Field<PhysDim, TopoDim, T>::template FieldCellGroupType<Topology> QFieldCellGroupType;

  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfldGroup.basis() );
  ElementQFieldClass qfldElem( qfldGroup.basis() );

  static const int N = DLA::VectorSize<T>::M;

  const int order = max(xfldElem.order(), qfldElem.order());
  int nRefine = max(order-1,0);
  ReferenceElementMesh<Topology> refMesh(nRefine);
  const int nRefnode = refMesh.nodes.size();
  const int nRefelem = refMesh.elems.size();

  int nElem_global;
  std::map<int,int> globalElemMap;
  continuousElementMap( comm, cellIDs, xfldGroup, nElem_global, globalElemMap);

  int nElemLocal = xfldGroup.nElem();

  // tecplot does not like empty groups
  if ( nElem_global == 0 ) return;
  if ( nElemLocal == 0 && partitioned ) return;

  if (partitioned) nElem_global = nElemLocal;

  int comm_rank = comm.rank();

  if (comm_rank == 0 || partitioned)
    fprintf( fp, "ZONE T=\"grid\", N=%d, E=%d, F=FEPOINT, ET=%s\n",
             nRefnode*nElem_global, nRefelem*nElem_global, TecTopo<Topology>::name() );

  // grid coordinates and solution

  typename ElementQFieldClass::RefCoordType sRefCell;
  typename ElementXFieldClass::VectorX X;
  T q, qTrue;

#ifdef SANS_MPI
  if (!partitioned)
  {
    int comm_size = comm.size();

    // maximum element chunk size that rank 0 will write at any given time
    int Elemchunk = nElem_global / comm_size + nElem_global % comm_size;

    // send one chunk of elements at a time to rank 0
    for (int rank = 0; rank < comm_size; rank++)
    {
      int elemLow  =  rank   *Elemchunk;
      int elemHigh = (rank+1)*Elemchunk;

      std::map<int,std::vector<DLA::VectorS<PhysDim::D+N,double>>> buffer;

      // loop over elements within group
      for (int elem = 0; elem < nElemLocal; elem++)
      {
        int elemID = globalElemMap[cellIDs[elem]];

        if (elemID >= elemLow && elemID < elemHigh &&
            xfldGroup.associativity(elem).rank() == comm_rank)
        {
          // copy global grid/solution DOFs to element
          xfldGroup.getElement( xfldElem, elem );
          qfldGroup.getElement( qfldElem, elem );

          std::vector<DLA::VectorS<PhysDim::D+N,double>> nodeValues(nRefnode);

          for ( int i = 0; i < nRefnode; i++ )
          {
            sRefCell = refMesh.nodes[i];

            xfldElem.eval( sRefCell, X );
            qfldElem.eval( sRefCell, q );

            int n = 0;
            for (int k = 0; k < PhysDim::D; k++)
              nodeValues[i][n++] = X[k];
            for (int k = 0; k < N; k++)
              nodeValues[i][n++] = DLA::index(q,k);
          }

          buffer[elemID] = std::move(nodeValues);
        }
      }


      if (comm_rank == 0)
      {
        std::vector<std::map<int,std::vector<DLA::VectorS<PhysDim::D+N,double>>>> bufferOnRank;
        boost::mpi::gather(comm, buffer, bufferOnRank, 0 );

        // collapse down the buffer from all other ranks
        for (std::size_t i = 1; i < bufferOnRank.size(); i++)
          buffer.insert(bufferOnRank[i].begin(), bufferOnRank[i].end());

        //Write the node data
        for ( const auto& nodesPair : buffer)
          for ( int i = 0; i < nRefnode; i++ )
          {
            for (int k = 0; k < PhysDim::D; k++)
              X[k] = nodesPair.second[i][k];

            for (int n = 0; n < N; n++)
              DLA::index(q,n) = nodesPair.second[i][PhysDim::D+n];

            qTrue = sln(X);

            for (int n = 0; n < PhysDim::D+N; n++)
              fprintf( fp, "%22.15e ", nodesPair.second[i][n] );

            for (int k = 0; k < N; k++)
              fprintf( fp, "%22.15e ", DLA::index(q,k)-DLA::index(qTrue,k) );

            fprintf( fp, "\n" );
          }
      }
#ifndef __clang_analyzer__
      else // send the buffer to rank 0
        boost::mpi::gather(comm, buffer, 0 );
#endif

    } // for rank

  }
  else // if partitioned
  {
#endif

    // loop over elements within group
    for (int elem = 0; elem < nElemLocal; elem++)
    {
      // copy global grid/solution DOFs to element
      xfldGroup.getElement( xfldElem, elem );
      qfldGroup.getElement( qfldElem, elem );

      for ( int i = 0; i < nRefnode; i++ )
      {
        sRefCell = refMesh.nodes[i];

        xfldElem.eval( sRefCell, X );
        qfldElem.eval( sRefCell, q );
        qTrue = sln(X);

        for (int k = 0; k < PhysDim::D; k++)
          fprintf( fp, "%22.15e ", X[k] );
        for (int k = 0; k < N; k++)
          fprintf( fp, "%22.15e ", DLA::index(q,k) );
        for (int k = 0; k < N; k++)
          fprintf( fp, "%22.15e ", DLA::index(q,k)-DLA::index(qTrue,k) );
        fprintf( fp, "\n" );
      }
    }

#ifdef SANS_MPI
  } // if !partitioned
#endif

  // cell-to-node connectivity

  if (comm_rank == 0 || partitioned)
    for (int elem = 0; elem < nElem_global; elem++)
    {
      int offset = nRefnode*elem;
      for (int relem = 0; relem < nRefelem; relem++)
      {
        for (int n = 0; n < Topology::NNode; n++ )
          fprintf( fp, "%d ", offset + refMesh.elems[relem][n] + 1 ); // +1 for one based indexing
        fprintf( fp, "\n" );
      }
    }
}

//----------------------------------------------------------------------------//
template<class T, class PhysDim, class TopoDim, class TopologyCell, class TopologyTrace>
void
output_Tecplot_TraceCellGroup( const int groupTrace,
                               mpi::communicator& comm, const FunctionNDConvertSpace<PhysDim, T>& sln,
                               const std::vector<int>& boundaryTraceIDs, const bool partitioned,
                               const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
                               const typename  Field<PhysDim, TopoDim, T>::template FieldCellGroupType<TopologyCell>& qfldCellL,
                               FILE* fp )
{
  typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename  Field<PhysDim, TopoDim, T>::template FieldCellGroupType<TopologyCell> QFieldCellGroupType;

  typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldClass;
  typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfldTrace.basis() );
  ElementQFieldClass qfldElem( qfldCellL.basis() );

  static const int N = DLA::VectorSize<T>::M;

  const int order = max(xfldElem.order(), qfldElem.order());
  int nRefine = max(order-1,0);
  ReferenceElementMesh<TopologyTrace> refMesh(nRefine);
  const int nRefnode = refMesh.nodes.size();
  const int nRefelem = refMesh.elems.size();

  int nElem_global;
  std::map<int,int> globalElemMap;
  continuousElementMap( comm, boundaryTraceIDs, xfldTrace, nElem_global, globalElemMap);

  int nElemLocal = xfldTrace.nElem();

  // tecplot does not like empty groups
  if ( nElem_global == 0 ) return;
  if ( nElemLocal == 0 && partitioned ) return;

  if (partitioned) nElem_global = nElemLocal;

  int comm_rank = comm.rank();

  if (comm_rank == 0 || partitioned)
    fprintf( fp, "ZONE T=\"Boundary %d\", N=%d, E=%d, F=FEPOINT, ET=%s\n",
             groupTrace, nRefnode*nElem_global, nRefelem*nElem_global, TecTopo<TopologyTrace>::name() );

  typename ElementQFieldClass::RefCoordType sRefCell;
  typename ElementXFieldClass::RefCoordType sRefTrace;
  typename ElementXFieldClass::VectorX X;
  T q, qTrue;

#ifdef SANS_MPI
  if (!partitioned)
  {
    int comm_size = comm.size();

    // maximum element chunk size that rank 0 will write at any given time
    int Elemchunk = nElem_global / comm_size + nElem_global % comm_size;

    // send one chunk of elements at a time to rank 0
    for (int rank = 0; rank < comm_size; rank++)
    {
      int elemLow  =  rank   *Elemchunk;
      int elemHigh = (rank+1)*Elemchunk;

      std::map<int,std::vector<DLA::VectorS<PhysDim::D+N,double>>> buffer;

      // loop over elements within group
      for (int elem = 0; elem < nElemLocal; elem++)
      {
        int elemID = globalElemMap[boundaryTraceIDs[elem]];

        if (elemID >= elemLow && elemID < elemHigh &&
            xfldTrace.associativity(elem).rank() == comm_rank)
        {
           const int elemL = xfldTrace.getElementLeft( elem );
           CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

           xfldTrace.getElement( xfldElem, elem );
           qfldCellL.getElement( qfldElem, elemL );

           std::vector<DLA::VectorS<PhysDim::D+N,double>> nodeValues(nRefnode);

           for ( int i = 0; i < nRefnode; i++ )
           {
             sRefTrace = refMesh.nodes[i];

             // adjacent cell-element reference coords
             TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyCell>::eval( canonicalTraceL, sRefTrace, sRefCell );

             xfldElem.eval( sRefTrace, X );
             qfldElem.eval( sRefCell, q );

             int n = 0;
             for (int k = 0; k < PhysDim::D; k++)
               nodeValues[i][n++] = X[k];
             for (int k = 0; k < N; k++)
               nodeValues[i][n++] = DLA::index(q,k);
           }

          buffer[elemID] = std::move(nodeValues);
        }
      }


      if (comm_rank == 0)
      {
        std::vector<std::map<int,std::vector<DLA::VectorS<PhysDim::D+N,double>>>> bufferOnRank;
        boost::mpi::gather(comm, buffer, bufferOnRank, 0 );

        // collapse down the buffer from all other ranks
        for (std::size_t i = 1; i < bufferOnRank.size(); i++)
          buffer.insert(bufferOnRank[i].begin(), bufferOnRank[i].end());

        //Write the node data
        for ( const auto& nodesPair : buffer)
          for ( int i = 0; i < nRefnode; i++ )
          {
            for (int k = 0; k < PhysDim::D; k++)
              X[k] = nodesPair.second[i][k];

            for (int n = 0; n < N; n++)
              DLA::index(q,n) = nodesPair.second[i][PhysDim::D+n];

            qTrue = sln(X);

            for (int n = 0; n < PhysDim::D+N; n++)
              fprintf( fp, "%22.15e ", nodesPair.second[i][n] );
            for (int k = 0; k < N; k++)
              fprintf( fp, "%22.15e ", DLA::index(q,k)-DLA::index(qTrue,k) );
            fprintf( fp, "\n" );
          }
      }
#ifndef __clang_analyzer__
      else // send the buffer to rank 0
        boost::mpi::gather(comm, buffer, 0 );
#endif

    } // for rank

  }
  else // if partitioned
  {
#endif

  for (int elem = 0; elem < nElemLocal; elem++)
  {
    const int elemL = xfldTrace.getElementLeft( elem );
    CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

    xfldTrace.getElement( xfldElem, elem );
    qfldCellL.getElement( qfldElem, elemL );

    for ( int i = 0; i < nRefnode; i++ )
    {
      sRefTrace = refMesh.nodes[i];

      // adjacent cell-element reference coords
      TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyCell>::eval( canonicalTraceL, sRefTrace, sRefCell );

      xfldElem.eval( sRefTrace, X );
      qfldElem.eval( sRefCell, q );
      qTrue = sln(X);

      for (int k = 0; k < PhysDim::D; k++)
        fprintf( fp, "%22.15e ", X[k] );
      for (int k = 0; k < N; k++)
        fprintf( fp, "%22.15e ", DLA::index(q,k) );
      for (int k = 0; k < N; k++)
        fprintf( fp, "%22.15e ", DLA::index(q,k)-DLA::index(qTrue,k) );
      fprintf( fp, "\n" );

    }
  }

#ifdef SANS_MPI
  } // if !partitioned
#endif

  // cell-to-node connectivity

  if (comm_rank == 0 || partitioned)
    for (int elem = 0; elem < nElem_global; elem++)
    {
      int offset = nRefnode*elem;
      for (int relem = 0; relem < nRefelem; relem++)
      {
        for (int n = 0; n < TopologyTrace::NNode; n++ )
          fprintf( fp, "%d ", offset + refMesh.elems[relem][n] + 1 ); // +1 for one based indexing
        fprintf( fp, "\n" );
      }
    }
}

//----------------------------------------------------------------------------//
template<class T, class PhysDim, class TopoDim, class Topology>
void
output_Tecplot_TraceGroup( const int group, const FunctionNDConvertSpace<PhysDim, T>& sln,
                           const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Topology>& xfld,
                           const typename  Field<PhysDim, TopoDim, T>::template FieldTraceGroupType<Topology>& qfld,
                           FILE* fp )
{
  typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Topology> XFieldTraceGroupType;
  typedef typename  Field<PhysDim, TopoDim, T>::template FieldTraceGroupType<Topology> QFieldTraceGroupType;

  typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldClass;
  typedef typename QFieldTraceGroupType::template ElementType<> ElementQFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfld.basis() );
  ElementQFieldClass qfldElem( qfld.basis() );

  static const int N = DLA::VectorSize<T>::M;

  const int order = max(xfldElem.order(), qfldElem.order());
  int nRefine = max(order-1,0);
  ReferenceElementMesh<Topology> refMesh(nRefine);
  const int nRefnode = refMesh.nodes.size();
  const int nRefelem = refMesh.elems.size();

  int nelem = xfld.nElem();

  // tecplot does not like empty groups
  if ( nelem == 0 ) return;

  fprintf( fp, "ZONE T=\"Boundary TraceElem %d\", N=%d, E=%d, F=FEPOINT, ET=%s\n", group, nRefnode*nelem, nRefelem*nelem, TecTopo<Topology>::name() );

  typename ElementQFieldClass::RefCoordType sRefTrace;
  typename ElementXFieldClass::VectorX X;

  T q, qTrue;
  for (int elem = 0; elem < nelem; elem++)
  {
    xfld.getElement( xfldElem, elem );
    qfld.getElement( qfldElem, elem );

    for ( int i = 0; i < Topology::NNode; i++ )
    {
      sRefTrace = refMesh.nodes[i];

      xfldElem.eval( sRefTrace, X );
      qfldElem.eval( sRefTrace, q );
      qTrue = sln(X);

      for (int k = 0; k < PhysDim::D; k++)
        fprintf( fp, "%22.15e ", X[k] );
      for (int k = 0; k < N; k++)
        fprintf( fp, "%22.15e ", DLA::index(q,k) );
      for (int k = 0; k < N; k++)
        fprintf( fp, "%22.15e ", DLA::index(q,k)-DLA::index(qTrue,k) );
      fprintf( fp, "\n" );

    }
  }

  // cell-to-node connectivity

  for (int elem = 0; elem < nelem; elem++)
  {
    int offset = nRefnode*elem;
    for (int relem = 0; relem < nRefelem; relem++)
    {
      for (int n = 0; n < Topology::NNode; n++ )
        fprintf( fp, "%d ", offset + refMesh.elems[relem][n] + 1 ); // +1 for one based indexing
      fprintf( fp, "\n" );
    }
  }
}

//----------------------------------------------------------------------------//
template<class PhysDim, class T>
void tecplotVariables(const std::vector<std::string>& state_names, FILE* fp)
{
  fprintf( fp, "\"\"\n" );
  fprintf( fp, "VARIABLES = \"X\"" );

  const char XYZ[] = "XYZ";
  for (int k = 1; k < PhysDim::D; k++)
    fprintf( fp, ", \"%c\"", XYZ[k] );

  static const int N = DLA::VectorSize<T>::M;
  if (state_names.empty())
  {
    for (int k = 0; k < N; k++)
      fprintf( fp, ", \"q%d\"", k+1 );

    for (int k = 0; k < N; k++)
      fprintf( fp, ", \"q%d Error\"", k+1 );
  }
  else
  {
    if (state_names.size() != N)
    {
      std::stringstream str;
      str << "state_names is not lenth " << N << std::endl;
      str << "state_names = " << state_names << std::endl;
      SANS_DEVELOPER_EXCEPTION( str.str() );
    }

    for (int k = 0; k < N; k++)
      fprintf( fp, ", \"%s\"", state_names[k].c_str());

    for (int k = 0; k < N; k++)
      fprintf( fp, ", \"%s Error\"", state_names[k].c_str());
  }
  fprintf( fp, "\n" );
}

//----------------------------------------------------------------------------//
template<class PhysDim, class T>
void
output_Tecplot( const FunctionNDConvertSpace<PhysDim, T>& sln,
                const Field< PhysDim, TopoD1, T >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names,
                const bool partitioned )
{
  FILE* fp = nullptr;

  if (qfld.comm()->rank() == 0 || partitioned)
  {
    std::cout << "output_Tecplot: filename = " << filename << std::endl;
    fp = fopen( filename.c_str(), "w" );
    if (fp == NULL)
      SANS_DEVELOPER_EXCEPTION("output_Tecplot<PhysDim,TopoD1> - Error opening file: %s", filename.c_str());

    tecplotVariables<PhysDim, T>(state_names, fp);
  }

  const XField<PhysDim,TopoD1>& xgrid = qfld.getXField();

  const int ngroup = qfld.nCellGroups();

  // loop over cell groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Line) )
    {
      output_Tecplot_CellGroup<T, PhysDim, TopoD1, Line>( *xgrid.comm(), sln, xgrid.cellIDs(group), partitioned,
                                                          xgrid.template getCellGroup<Line>(group),
                                                           qfld.template getCellGroup<Line>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD1,TopoD1>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

#if 0
  // loop over trace boundary groups
  for (int group = 0; group < qfld.nBoundaryTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      output_Tecplot_TraceGroup<N>( group, xgrid.template getBoundaryTraceGroup<Triangle>(group),
                                            qfld.template getBoundaryTraceGroup<Triangle>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD3,TopoD3>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }
#endif

  if (qfld.comm()->rank() == 0 || partitioned)
    fclose( fp );
}

//----------------------------------------------------------------------------//
template<class PhysDim, class T>
void
output_Tecplot( const FunctionNDConvertSpace<PhysDim, T>& sln,
                const Field< PhysDim, TopoD2, T >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names,
                const bool partitioned )
{
  FILE* fp = nullptr;

  if (qfld.comm()->rank() == 0 || partitioned)
  {
    std::cout << "output_Tecplot: filename = " << filename << std::endl;
    fp = fopen( filename.c_str(), "w" );
    if (fp == NULL)
      SANS_DEVELOPER_EXCEPTION("output_Tecplot<PhysDim,TopoD2> - Error opening file: %s", filename.c_str());

    tecplotVariables<PhysDim, T>(state_names, fp);
  }

  const XField<PhysDim,TopoD2>& xgrid = qfld.getXField();

  const int ngroup = qfld.nCellGroups();

  // loop over cell groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      output_Tecplot_CellGroup<T, PhysDim, TopoD2, Triangle>( *xgrid.comm(), sln, xgrid.cellIDs(group), partitioned,
                                                              xgrid.template getCellGroup<Triangle>(group),
                                                               qfld.template getCellGroup<Triangle>(group), fp );
    }
    else if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Quad) )
    {
      output_Tecplot_CellGroup<T, PhysDim, TopoD2, Quad>( *xgrid.comm(), sln, xgrid.cellIDs(group), partitioned,
                                                          xgrid.template getCellGroup<Quad>(group),
                                                           qfld.template getCellGroup<Quad>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD2,TopoD2>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  // loop over trace boundary groups
  for (int groupTrace = 0; groupTrace < xgrid.nBoundaryTraceGroups(); groupTrace++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(groupTrace).topoTypeID() == typeid(Line) )
    {
      // determine topology for Left cell group
      int groupL = xgrid.template getBoundaryTraceGroup<Line>(groupTrace).getGroupLeft();

      if ( xgrid.getCellGroupBase(groupL).topoTypeID() == typeid(Triangle) )
        output_Tecplot_TraceCellGroup<T, PhysDim, TopoD2, Triangle, Line>( groupTrace,
                                                                           *xgrid.comm(), sln, xgrid.boundaryTraceIDs(groupTrace), partitioned,
                                                                           xgrid.template getBoundaryTraceGroup<Line>(groupTrace),
                                                                           qfld.template getCellGroup<Triangle>(groupL), fp );
      else if ( xgrid.getCellGroupBase(groupL).topoTypeID() == typeid(Quad) )
        output_Tecplot_TraceCellGroup<T, PhysDim, TopoD2, Quad, Line>( groupTrace,
                                                                       *xgrid.comm(), sln, xgrid.boundaryTraceIDs(groupTrace), partitioned,
                                                                       xgrid.template getBoundaryTraceGroup<Line>(groupTrace),
                                                                       qfld.template getCellGroup<Quad>(groupL), fp );
      else
        SANS_DEVELOPER_EXCEPTION("Unknown topology");
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD2,TopoD2>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }


  // loop over trace boundary groups
  for (int group = 0; group < qfld.nBoundaryTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Line) )
    {
      output_Tecplot_TraceGroup<T, PhysDim, TopoD2, Line>( group, sln,
                                                           xgrid.template getBoundaryTraceGroup<Line>(group),
                                                            qfld.template getBoundaryTraceGroup<Line>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD2,TopoD2>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  if (qfld.comm()->rank() == 0 || partitioned)
    fclose( fp );
}

//----------------------------------------------------------------------------//
template<class T>
void
output_Tecplot( const FunctionNDConvertSpace<PhysD3, T>& sln,
                const Field< PhysD3, TopoD3, T >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names,
                const bool partitioned )
{
  FILE* fp = nullptr;

  if (qfld.comm()->rank() == 0 || partitioned)
  {
    std::cout << "output_Tecplot: filename = " << filename << std::endl;
    fp = fopen( filename.c_str(), "w" );
    if (fp == NULL)
      SANS_DEVELOPER_EXCEPTION("output_Tecplot<PhysD3,TopoD3> - Error opening file: %s", filename.c_str());

    tecplotVariables<PhysD3, T>(state_names, fp);
  }

  const XField<PhysD3,TopoD3>& xgrid = qfld.getXField();

  const int ngroup = qfld.nCellGroups();

  // loop over cell groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Tet) )
    {
      output_Tecplot_CellGroup<T, PhysD3, TopoD3, Tet>( *xgrid.comm(), sln, xgrid.cellIDs(group), partitioned,
                                                        xgrid.template getCellGroup<Tet>(group),
                                                        qfld.template getCellGroup<Tet>(group), fp );
    }
    else if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Hex) )
    {
      output_Tecplot_CellGroup<T, PhysD3, TopoD3, Hex>( *xgrid.comm(), sln, xgrid.cellIDs(group), partitioned,
                                                        xgrid.template getCellGroup<Hex>(group),
                                                        qfld.template getCellGroup<Hex>(group), fp );
    }
    else
      SANS_DEVELOPER_EXCEPTION("Unknown topology");
  }

  // loop over trace boundary groups
  for (int groupTrace = 0; groupTrace < xgrid.nBoundaryTraceGroups(); groupTrace++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(groupTrace).topoTypeID() == typeid(Triangle) )
    {
      // determine topology for Left cell group
      int groupL = xgrid.template getBoundaryTraceGroup<Triangle>(groupTrace).getGroupLeft();

      if ( xgrid.getCellGroupBase(groupL).topoTypeID() == typeid(Tet) )
        output_Tecplot_TraceCellGroup<T, PhysD3, TopoD3, Tet, Triangle>( groupTrace,
                                                                         *xgrid.comm(), sln, xgrid.boundaryTraceIDs(groupTrace), partitioned,
                                                                         xgrid.template getBoundaryTraceGroup<Triangle>(groupTrace),
                                                                          qfld.template getCellGroup<Tet>(groupL), fp );
      else
        SANS_DEVELOPER_EXCEPTION("Unknown topology");
    }
    else if ( xgrid.getBoundaryTraceGroupBase(groupTrace).topoTypeID() == typeid(Quad) )
    {
      // determine topology for Left cell group
      int groupL = xgrid.template getBoundaryTraceGroup<Triangle>(groupTrace).getGroupLeft();

      if ( xgrid.getCellGroupBase(groupL).topoTypeID() == typeid(Hex) )
        output_Tecplot_TraceCellGroup<T, PhysD3, TopoD3, Hex, Quad>( groupTrace,
                                                                     *xgrid.comm(), sln, xgrid.boundaryTraceIDs(groupTrace), partitioned,
                                                                     xgrid.template getBoundaryTraceGroup<Quad>(groupTrace),
                                                                      qfld.template getCellGroup<Hex>(groupL), fp );
      else
        SANS_DEVELOPER_EXCEPTION("Unknown topology");
    }
    else
      SANS_DEVELOPER_EXCEPTION("Unknown topology");
  }

  // loop over trace boundary groups
  for (int group = 0; group < qfld.nBoundaryTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      output_Tecplot_TraceGroup<T, PhysD3, TopoD3, Triangle>( group, sln, xgrid.template getBoundaryTraceGroup<Triangle>(group),
                                                              qfld.template getBoundaryTraceGroup<Triangle>(group), fp );
    }
    else if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Quad) )
    {
      output_Tecplot_TraceGroup<T, PhysD3, TopoD3, Quad>( group, sln, xgrid.template getBoundaryTraceGroup<Quad>(group),
                                                          qfld.template getBoundaryTraceGroup<Quad>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD3,TopoD3>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  if (qfld.comm()->rank() == 0 || partitioned)
    fclose( fp );
}

//Explicitly instantiate the function
template void
output_Tecplot( const FunctionNDConvertSpace<PhysD1, Real>& sln, const Field< PhysD1, TopoD1, Real >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const FunctionNDConvertSpace<PhysD2, Real>& sln, const Field< PhysD2, TopoD1, Real >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const FunctionNDConvertSpace<PhysD2, Real>& sln, const Field< PhysD2, TopoD2, Real >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const FunctionNDConvertSpace<PhysD3, Real>& sln, const Field< PhysD3, TopoD2, Real >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const FunctionNDConvertSpace<PhysD3, Real>& sln, const Field< PhysD3, TopoD3, Real >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );


template void
output_Tecplot( const FunctionNDConvertSpace<PhysD1,   DLA::VectorS<1,Real>>& sln,
                const Field< PhysD1, TopoD1, DLA::VectorS<1,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const FunctionNDConvertSpace<PhysD1,   DLA::VectorS<2,Real>>& sln,
                const Field< PhysD1, TopoD1, DLA::VectorS<2,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const FunctionNDConvertSpace<PhysD1,   DLA::VectorS<3,Real>>& sln,
                const Field< PhysD1, TopoD1, DLA::VectorS<3,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const FunctionNDConvertSpace<PhysD1,   DLA::VectorS<4,Real>>& sln,
                const Field< PhysD1, TopoD1, DLA::VectorS<4,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const FunctionNDConvertSpace<PhysD1,   DLA::VectorS<5,Real>>& sln,
                const Field< PhysD1, TopoD1, DLA::VectorS<5,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );

template void
output_Tecplot( const FunctionNDConvertSpace<PhysD2,   DLA::VectorS<1,Real>>& sln,
                const Field< PhysD2, TopoD1, DLA::VectorS<1,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const FunctionNDConvertSpace<PhysD2,   DLA::VectorS<2,Real>>& sln,
                const Field< PhysD2, TopoD1, DLA::VectorS<2,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const FunctionNDConvertSpace<PhysD2,   DLA::VectorS<3,Real>>& sln,
                const Field< PhysD2, TopoD1, DLA::VectorS<3,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );

template void
output_Tecplot( const FunctionNDConvertSpace<PhysD2,   DLA::VectorS<1,Real>>& sln,
                const Field< PhysD2, TopoD2, DLA::VectorS<1,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const FunctionNDConvertSpace<PhysD2,   DLA::VectorS<2,Real>>& sln,
                const Field< PhysD2, TopoD2, DLA::VectorS<2,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const FunctionNDConvertSpace<PhysD2,   DLA::VectorS<3,Real>>& sln,
                const Field< PhysD2, TopoD2, DLA::VectorS<3,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const FunctionNDConvertSpace<PhysD2,   DLA::VectorS<4,Real>>& sln,
                const Field< PhysD2, TopoD2, DLA::VectorS<4,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const FunctionNDConvertSpace<PhysD2,   DLA::VectorS<5,Real>>& sln,
                const Field< PhysD2, TopoD2, DLA::VectorS<5,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const FunctionNDConvertSpace<PhysD2,   DLA::VectorS<6,Real>>& sln,
                const Field< PhysD2, TopoD2, DLA::VectorS<6,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );

#if 0
template void
output_Tecplot( const FunctionNDConvertSpace<PhysD3,   DLA::VectorS<1,Real>>& sln,
                const Field< PhysD3, TopoD2, DLA::VectorS<1,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );

template void
output_Tecplot( const FunctionNDConvertSpace<PhysD3,   DLA::VectorS<1,Real>>& sln,
                const Field< PhysD3, TopoD3, DLA::VectorS<1,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const FunctionNDConvertSpace<PhysD3,   DLA::VectorS<2,Real>>& sln,
                const Field< PhysD3, TopoD3, DLA::VectorS<2,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const FunctionNDConvertSpace<PhysD3,   DLA::VectorS<3,Real>>& sln,
                const Field< PhysD3, TopoD3, DLA::VectorS<3,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const FunctionNDConvertSpace<PhysD3,   DLA::VectorS<4,Real>>& sln,
                const Field< PhysD3, TopoD3, DLA::VectorS<4,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const FunctionNDConvertSpace<PhysD3,   DLA::VectorS<5,Real>>& sln,
                const Field< PhysD3, TopoD3, DLA::VectorS<5,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const FunctionNDConvertSpace<PhysD3,   DLA::VectorS<6,Real>>& sln,
                const Field< PhysD3, TopoD3, DLA::VectorS<6,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const FunctionNDConvertSpace<PhysD3,   DLA::VectorS<7,Real>>& sln,
                const Field< PhysD3, TopoD3, DLA::VectorS<7,Real> >& Q, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
#endif

} //namespace SANS
