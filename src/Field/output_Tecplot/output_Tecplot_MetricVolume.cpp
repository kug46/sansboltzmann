// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define OUTPUT_TECPLOT_METRIC_INSTANTIATE
#include "output_Tecplot_Metric_impl.h"

#include "Field/XFieldVolume.h"

#include "Field/FieldVolume.h"

namespace SANS
{

//----------------------------------------------------------------------------//
void
output_Tecplot_Metric_Nodes(
    const XField<PhysD3, TopoD3>& xfld,
    const  Field<PhysD3, TopoD3, DLA::MatrixSymS<PhysD3::D, Real>>& mfld,
    FILE* fp )
{
  static const int npts = 36; // Number of points to draw each ellipse
  double scale = 0.5; // so the ellipses touch for an ideal grid
  double dt = PI/180.*(360.0 / (double)npts);

  DLA::VectorS<PhysD3::D,double> L, e, X;
  DLA::MatrixS<PhysD3::D,PhysD3::D,double> E;

  // This function assumes linear grids and metric field
  for (int i = 0; i < xfld.nCellGroups(); i++)
  {
    SANS_ASSERT( xfld.getCellGroupBase(i).order() == 1 );
    SANS_ASSERT( mfld.getCellGroupBase(i).order() == 1 );
  }

  // Compute the total number of nodes in the grid
  int nNodeGlobal = mfld.nDOFpossessed();
#ifdef SANS_MPI
  nNodeGlobal = boost::mpi::all_reduce(*xfld.comm(), nNodeGlobal, std::plus<int>());
#endif

  int comm_rank = xfld.comm()->rank();

  int nElem = nNodeGlobal * npts;

  if (comm_rank == 0)
    fprintf( fp, "ZONE T=\"metric\", N=%d, E=%d, F=FEPOINT, ET=LINESEG\n",
             3*nElem, 3*nElem );

  // grid coordinates and solution

#ifdef SANS_MPI
  typedef std::pair<DLA::VectorS<PhysD3::D,double>, DLA::MatrixSymS<PhysD3::D, double>> NodeMetricType;

  int comm_size = xfld.comm()->size();

  // maximum node chunk size that rank 0 will write at any given time
  int nodeChunk = nNodeGlobal / comm_size + nNodeGlobal % comm_size;

  // send one chunk of elements at a time to rank 0
  for (int rank = 0; rank < comm_size; rank++)
  {
    int nodeLow  =  rank   *nodeChunk;
    int nodeHigh = (rank+1)*nodeChunk;

    std::map<int,NodeMetricType> buffer;

    // loop over elements within group
    for (int node = 0; node < mfld.nDOFpossessed(); node++)
    {
      int nodeID = mfld.local2nativeDOFmap(node);

      if (nodeID >= nodeLow && nodeID < nodeHigh)
      {
        // copy global grid/solution DOFs to the buffer
        NodeMetricType nodeValues;

        nodeValues.first = xfld.DOF(node);
        nodeValues.second = mfld.DOF(node);

        buffer[nodeID] = nodeValues;
      }
    }

    if (comm_rank == 0)
    {
      std::vector<std::map<int,NodeMetricType>> bufferOnRank;
      boost::mpi::gather(*xfld.comm(), buffer, bufferOnRank, 0 );

      // collapse down the buffer from all other ranks
      for (std::size_t i = 1; i < bufferOnRank.size(); i++)
        buffer.insert(bufferOnRank[i].begin(), bufferOnRank[i].end());

      //Write the node elipse data
      for ( const auto& nodePair : buffer)
      {
        const NodeMetricType& XM = nodePair.second;

        // compute the eigen system
        DLA::EigenSystem(XM.second, L, E );

        for (int e0 = 0; e0 < 3; e0++)
        {
          int e1 = e0 + 1;
          if (e1 == 3) e1 = 0;
          for (int i = 0; i < npts; i++)
          {
            double ex = scale * cos(i * dt) / sqrt(L[e0]);
            double ey = scale * sin(i * dt) / sqrt(L[e1]);
            X[0] = E(0,e0)*ex + E(0,e1)*ey;
            X[1] = E(1,e0)*ex + E(1,e1)*ey;
            X[2] = E(2,e0)*ex + E(2,e1)*ey;
            fprintf(fp, " %.16e %.16e %.16e\n",
                    XM.first[0] + X[0],
                    XM.first[1] + X[1],
                    XM.first[2] + X[2]);
          }
        }
      }
    }
#ifndef __clang_analyzer__
    else // send the buffer to rank 0
      boost::mpi::gather(*xfld.comm(), buffer, 0 );
#endif

  } // for rank

#else

  // loop over elements within group
  for (int node = 0; node < nNodeGlobal; node++)
  {
    DLA::VectorS<PhysD3::D,double> Xn = xfld.DOF(node);
    DLA::MatrixSymS<PhysD3::D,double> m = mfld.DOF(node);

    // compute the eigen system
    DLA::EigenSystem(m, L, E );

    for (int e0 = 0; e0 < 3; e0++)
    {
      int e1 = e0 + 1;
      if (e1 == 3) e1 = 0;
      for (int i = 0; i < npts; i++)
      {
        double ex = scale * cos(i * dt) / sqrt(L[e0]);
        double ey = scale * sin(i * dt) / sqrt(L[e1]);
        X[0] = E(0,e0)*ex + E(0,e1)*ey;
        X[1] = E(1,e0)*ex + E(1,e1)*ey;
        X[2] = E(2,e0)*ex + E(2,e1)*ey;
        fprintf(fp, " %.16e %.16e %.16e\n",
                Xn[0] + X[0],
                Xn[1] + X[1],
                Xn[2] + X[2]);
      }
    }
  }

#endif

  // cell-to-node connectivity

  if (comm_rank == 0)
  {
    for (int e0 = 0; e0 < 3; e0++)
      for (int node = 0; node < nNodeGlobal; node++)
      {
        for (int i = 0; i < npts - 1; i++)
          fprintf(fp, " %d %d\n", i + node * npts + 1 + nElem * e0,
                  i + 1 + node * npts + 1 + nElem * e0);
        fprintf(fp, " %d %d\n", npts + node * npts + nElem * e0,
                1 + node * npts + nElem * e0);
      }
  }
}

//----------------------------------------------------------------------------//
template<>
void
output_Tecplot_Metric( const Field< PhysD3, TopoD3, DLA::MatrixSymS<PhysD3::D, Real> >& mfld,
                       const std::string& filename )
{
  FILE* fp = nullptr;

  if (mfld.comm()->rank() == 0)
  {
    std::cout << "output_Tecplot: filename = " << filename << std::endl;
    fp = fopen( filename.c_str(), "w" );
    if (fp == NULL)
      SANS_RUNTIME_EXCEPTION("Error opening file: %s", filename.c_str());

    fprintf( fp, "\"\"\n" );
    fprintf( fp, "VARIABLES = \"X\", \"Y\", \"Z\"\n" );
  }

  const XField<PhysD3,TopoD3>& xgrid = mfld.getXField();

  output_Tecplot_Metric_Nodes( xgrid, mfld, fp );

  const int ngroup = xgrid.nCellGroups();

  // loop over cell groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Tet) )
    {
      if (group == 0)
        output_Tecplot_CellGroup_Nodes<PhysD3, TopoD3, Tet>( xgrid, group,
                                                             xgrid.getCellGroup<Tet>(group), fp );
      else
        output_Tecplot_CellGroup<PhysD3, TopoD3, Tet>( xgrid, group,
                                                       xgrid.getCellGroup<Tet>(group), fp );
    }
    else if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Hex) )
    {
      if (group == 0)
        output_Tecplot_CellGroup_Nodes<PhysD3, TopoD3, Hex>( xgrid, group,
                                                             xgrid.getCellGroup<Hex>(group), fp );
      else
        output_Tecplot_CellGroup<PhysD3, TopoD3, Hex>( xgrid, group,
                                                       xgrid.getCellGroup<Hex>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD3,TopoD3>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  // loop over trace boundary groups
  for (int group = 0; group < xgrid.nBoundaryTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      output_Tecplot_TraceGroup<PhysD3, TopoD3, Triangle>(xgrid, "Boundary", group,
                                                          xgrid.getBoundaryTraceGroup<Triangle>(group), fp );
    }
    else if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Quad) )
    {
      output_Tecplot_TraceGroup<PhysD3, TopoD3, Quad>(xgrid, "Boundary", group,
                                                      xgrid.getBoundaryTraceGroup<Quad>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD3,TopoD3>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

#if 0
  // loop over ghost trace boundary groups
  for (int group = 0; group < xgrid.nGhostBoundaryTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      output_Tecplot_TraceGroup<PhysD3, TopoD3, Triangle>(xgrid, "Ghost", group,
                                                          xgrid.getGhostBoundaryTraceGroup<Triangle>(group), fp );
    }
    else if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Quad) )
    {
      output_Tecplot_TraceGroup<PhysD3, TopoD3, Quad>(xgrid, "Ghost", group,
                                                      xgrid.getGhostBoundaryTraceGroup<Quad>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD3,TopoD3>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }
#endif

  if (mfld.comm()->rank() == 0)
    fclose( fp );
}

} //namespace SANS
