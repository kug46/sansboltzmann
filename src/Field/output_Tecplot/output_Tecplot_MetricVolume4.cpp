// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define OUTPUT_TECPLOT_METRIC_INSTANTIATE
#include "output_Tecplot_Metric_impl.h"

#include "Field/XFieldSpacetime.h"

#include "Field/FieldSpacetime.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template<>
void
output_Tecplot_Metric( const Field< PhysD4, TopoD4, DLA::MatrixSymS<PhysD4::D, Real> >& mfld,
                       const std::string& filename )
{
  SANS_DEVELOPER_EXCEPTION("implement");
}


} //namespace SANS
