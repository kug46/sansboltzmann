// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define OUTPUT_TECPLOT_LIFT_INSTANTIATE
#include "output_Tecplot_Lift_impl.h"

#include "Field/XFieldArea.h"

#include "Field/FieldLiftArea_DG_Cell.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//
template<class PhysDim, class T>
void
output_Tecplot_LO( const FieldLift< PhysDim, TopoD2, DLA::VectorS<PhysDim::D,T> >& rfld, const std::string& filename,
                   const std::vector<std::string>& state_names,
                   const bool partitioned )
{
  FILE* fp = nullptr;

  if (rfld.comm()->rank() == 0 || partitioned)
  {
    std::cout << "output_Tecplot: filename = " << filename << std::endl;
    fp = fopen( filename.c_str(), "w" );

    fprintf( fp, "\"\"\n" );
    fprintf( fp, "VARIABLES = \"X\"" );

    const char XYZ[] = "XYZ";
    for (int k = 1; k < PhysDim::D; k++)
      fprintf( fp, ", \"%c\"", XYZ[k] );

    static const int N = DLA::VectorSize<T>::M;
    if (state_names.empty())
    {
      for (int k = 0; k < N; k++)
        fprintf( fp, ", \"q%d\"", k+1 );
    }
    else
    {
      SANS_ASSERT((int) state_names.size() == N);
      for (int k = 0; k < N; k++)
        fprintf( fp, ", \"%s\"", state_names[k].c_str());
    }
    fprintf( fp, "\n" );
  }

  const XField<PhysDim,TopoD2>& xgrid = rfld.getXField();

  const int ngroup = rfld.nCellGroups();

  // loop over cell groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      output_Tecplot_CellGroup<T, PhysDim, TopoD2, Triangle>( xgrid.template getCellGroup<Triangle>(group),
                                                               rfld.template getCellGroup<Triangle>(group), fp );
    }
    else if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Quad) )
    {
      output_Tecplot_CellGroup<T, PhysDim, TopoD2, Quad>( xgrid.template getCellGroup<Quad>(group),
                                                           rfld.template getCellGroup<Quad>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD2,TopoD2>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  if (rfld.comm()->rank() == 0 || partitioned)
      fclose( fp );
}


//Explicitly instantiate the function
template void
output_Tecplot_LO( const FieldLift< PhysD2, TopoD2, DLA::VectorS<PhysD2::D,Real> >& rfld, const std::string& filename,
                   const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot_LO( const FieldLift< PhysD2, TopoD2, DLA::VectorS<PhysD2::D,DLA::VectorS<1,Real>> >& rfld, const std::string& filename,
                   const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot_LO( const FieldLift< PhysD2, TopoD2, DLA::VectorS<PhysD2::D,DLA::VectorS<2,Real>> >& rfld, const std::string& filename,
                   const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot_LO( const FieldLift< PhysD2, TopoD2, DLA::VectorS<PhysD2::D,DLA::VectorS<3,Real>> >& rfld, const std::string& filename,
                   const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot_LO( const FieldLift< PhysD2, TopoD2, DLA::VectorS<PhysD2::D,DLA::VectorS<4,Real>> >& rfld, const std::string& filename,
                   const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot_LO( const FieldLift< PhysD2, TopoD2, DLA::VectorS<PhysD2::D,DLA::VectorS<5,Real>> >& rfld, const std::string& filename,
                   const std::vector<std::string>& state_names, const bool partitioned );

} //namespace SANS
