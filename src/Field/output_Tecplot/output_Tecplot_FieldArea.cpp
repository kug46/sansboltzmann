// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define OUTPUT_TECPLOT_FIELD_INSTANTIATE
#include "output_Tecplot_Field_impl.h"

#include "Field/XFieldArea.h"

#include "Field/FieldArea.h"
#include "Field/FieldArea_CG_Cell.h"
#include "UserVariables/BoltzmannNVar.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template<>
void
output_Tecplot( const XField<PhysD2,TopoD2>& xgrid, const std::string& filename )
{
  std::cout << "output_Tecplot: filename = " << filename << std::endl;
  FILE* fp = fopen( filename.c_str(), "w" );
  if (fp == NULL)
    SANS_DEVELOPER_EXCEPTION("output_Tecplot<PhysD2,TopoD2> - Error opening file: %s", filename.c_str());

  fprintf( fp, "\"\"\n" );
  fprintf( fp, "VARIABLES = \"X\", \"Y\", \"Native Index\", \"Node Rank\", \"Cell Rank\"\n" );

  const int ngroup = xgrid.nCellGroups();
  std::map<int,int> dofRank;

  int order = xgrid.getCellGroupBase(0).order();
  for (int group = 0; group < ngroup; group++)
    SANS_ASSERT(xgrid.getCellGroupBase(group).order() == order);

  Field_CG_Cell<PhysD2,TopoD2,Real> qfld(xgrid, order, BasisFunctionCategory_Lagrange);

  // loop over element groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over elements of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      dofRank_CellGroup<PhysD2, TopoD2, Triangle>( xgrid.local2nativeDOFmap(), xgrid.getCellGroup<Triangle>(group),
                                                   qfld, qfld.getCellGroup<Triangle>(group), dofRank );
    }
    else if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Quad) )
    {
      dofRank_CellGroup<PhysD2, TopoD2, Quad>( xgrid.local2nativeDOFmap(), xgrid.getCellGroup<Quad>(group),
                                               qfld, qfld.getCellGroup<Quad>(group), dofRank );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD1,TopoD1>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }


  // loop over element groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over elements of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      if (group == 0)
        output_Tecplot_CellGroup_Nodes<PhysD2, TopoD2, Triangle>( xgrid.local2nativeDOFmap(), xgrid.getCellGroup<Triangle>(group), dofRank, fp );
      else
        output_Tecplot_CellGroup_XField<PhysD2, TopoD2, Triangle>( xgrid.getCellGroup<Triangle>(group), fp );
    }
    else if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Quad) )
    {
      if (group == 0)
        output_Tecplot_CellGroup_Nodes<PhysD2, TopoD2, Quad>( xgrid.local2nativeDOFmap(), xgrid.getCellGroup<Quad>(group), dofRank, fp );
      else
        output_Tecplot_CellGroup_XField<PhysD2, TopoD2, Quad>( xgrid.getCellGroup<Quad>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD2,TopoD2>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  // loop over interior trace groups (skip the first one because it should mostly duplicate the entire grid)
  for (int group = 1; group < xgrid.nInteriorTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Line) )
    {
      output_Tecplot_TraceGroup_XField<PhysD2, TopoD2, Line>("Interior", group, xgrid.getInteriorTraceGroup<Line>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD2,TopoD2>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  // loop over trace boundary groups
  for (int group = 0; group < xgrid.nBoundaryTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Line) )
    {
      output_Tecplot_TraceGroup_XField<PhysD2, TopoD2, Line>("Boundary", group, xgrid.getBoundaryTraceGroup<Line>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD2,TopoD2>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  // loop over ghost trace boundary groups
  for (int group = 0; group < xgrid.nGhostBoundaryTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Line) )
    {
      output_Tecplot_TraceGroup_XField<PhysD2, TopoD2, Line>("Ghost", group, xgrid.getGhostBoundaryTraceGroup<Line>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD2,TopoD2>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  fclose( fp );
}

//----------------------------------------------------------------------------//
template<class PhysDim, class T>
void
output_Tecplot( const Field< PhysDim, TopoD2, T >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names,
                const bool partitioned )
{
  FILE* fp = nullptr;

  if (qfld.comm()->rank() == 0 || partitioned)
  {
    std::cout << "output_Tecplot: filename = " << filename << std::endl;
    fp = fopen( filename.c_str(), "w" );
    if (fp == NULL)
      SANS_RUNTIME_EXCEPTION("output_Tecplot<PhysDim,TopoD2> - Error opening file: %s", filename.c_str());

    fprintf( fp, "\"\"\n" );
    fprintf( fp, "VARIABLES = \"X\"" );

    const char XYZ[] = "XYZ";
    for (int k = 1; k < PhysDim::D; k++)
      fprintf( fp, ", \"%c\"", XYZ[k] );

    static const int N = DLA::VectorSize<T>::M;
    if (state_names.empty())
    {
      for (int k = 0; k < N; k++)
        fprintf( fp, ", \"q%d\"", k+1 );
    }
    else
    {
      SANS_ASSERT((int) state_names.size() == N);
      for (int k = 0; k < N; k++)
        fprintf( fp, ", \"%s\"", state_names[k].c_str());
    }
    fprintf( fp, "\n" );
  }

  const XField<PhysDim,TopoD2>& xgrid = qfld.getXField();

  const int ngroup = qfld.nCellGroups();

  // loop over cell groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      output_Tecplot_CellGroup<T, PhysDim, TopoD2, Triangle>( *xgrid.comm(), xgrid.cellIDs(group), partitioned,
                                                              xgrid.template getCellGroup<Triangle>(group),
                                                               qfld.template getCellGroup<Triangle>(group), fp );
    }
    else if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Quad) )
    {
      output_Tecplot_CellGroup<T, PhysDim, TopoD2, Quad>( *xgrid.comm(), xgrid.cellIDs(group), partitioned,
                                                          xgrid.template getCellGroup<Quad>(group),
                                                           qfld.template getCellGroup<Quad>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD2,TopoD2>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }


  // loop over trace boundary groups
  for (int groupTrace = 0; groupTrace < xgrid.nBoundaryTraceGroups(); groupTrace++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(groupTrace).topoTypeID() == typeid(Line) )
    {
      // determine topology for Left cell group
      int groupL = xgrid.template getBoundaryTraceGroup<Line>(groupTrace).getGroupLeft();

      if ( xgrid.getCellGroupBase(groupL).topoTypeID() == typeid(Triangle) )
        output_Tecplot_TraceCellGroup<T, PhysDim, TopoD2, Triangle, Line>( groupTrace,
                                                                           *xgrid.comm(), xgrid.boundaryTraceIDs(groupTrace), partitioned,
                                                                           xgrid.template getBoundaryTraceGroup<Line>(groupTrace),
                                                                           qfld.template getCellGroup<Triangle>(groupL), fp );
      else if ( xgrid.getCellGroupBase(groupL).topoTypeID() == typeid(Quad) )
        output_Tecplot_TraceCellGroup<T, PhysDim, TopoD2, Quad, Line>( groupTrace,
                                                                       *xgrid.comm(), xgrid.boundaryTraceIDs(groupTrace), partitioned,
                                                                       xgrid.template getBoundaryTraceGroup<Line>(groupTrace),
                                                                       qfld.template getCellGroup<Quad>(groupL), fp );
      else
        SANS_DEVELOPER_EXCEPTION("Unknown topology");
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD2,TopoD2>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  if (ngroup  == 0)
  {
    SANS_ASSERT_MSG(qfld.comm()->size() == 0, "Pure trace fields cannot be written in parallel yet");

    // loop over trace boundary groups
    for (int group = 0; group < qfld.nBoundaryTraceGroups(); group++)
    {
      // dispatch integration over cell of group
      if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Line) )
      {
        output_Tecplot_TraceGroup<T, PhysDim, TopoD2, Line>( group,
                                                             xgrid.template getBoundaryTraceGroup<Line>(group),
                                                             qfld.template getBoundaryTraceGroup<Line>(group), fp );
      }
      else
      {
        const char msg[] = "Error in output_Tecplot(XField<PhysD2,TopoD2>): unknown topology\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }
  }

  if (qfld.comm()->rank() == 0 || partitioned)
    fclose( fp );
}

//----------------------------------------------------------------------------//
//
template<class PhysDim, class T>
void
output_Tecplot_LO( const FieldLift< PhysDim, TopoD1, DLA::VectorS<PhysDim::D,T> >& rfld, const std::string& filename,
                   const std::vector<std::string>& state_names,
                   const bool partitioned )
{
  FILE* fp = nullptr;

  if (rfld.comm()->rank() == 0 || partitioned)
  {
    std::cout << "output_Tecplot: filename = " << filename << std::endl;
    fp = fopen( filename.c_str(), "w" );

    fprintf( fp, "\"\"\n" );
    fprintf( fp, "VARIABLES = \"X\"" );

    const char XYZ[] = "XYZ";
    for (int k = 1; k < PhysDim::D; k++)
      fprintf( fp, ", \"%c\"", XYZ[k] );

    static const int N = DLA::VectorSize<T>::M;
    if (state_names.empty())
    {
      for (int k = 0; k < N; k++)
        fprintf( fp, ", \"q%d\"", k+1 );
    }
    else
    {
      SANS_ASSERT((int) state_names.size() == N);
      for (int k = 0; k < N; k++)
        fprintf( fp, ", \"%s\"", state_names[k].c_str());
    }
    fprintf( fp, "\n" );
  }

  const XField<PhysDim,TopoD1>& xgrid = rfld.getXField();

  const int ngroup = rfld.nCellGroups();

  // loop over cell groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Line) )
    {
      output_Tecplot_CellGroup<T, PhysDim, TopoD1, Line>( xgrid.template getCellGroup<Line>(group),
                                                          rfld.template getCellGroup<Line>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD,TopoD1>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  if (rfld.comm()->rank() == 0 || partitioned)
      fclose( fp );
}

//Explicitly instantiate the function

// PhysD2
template void
output_Tecplot( const Field< PhysD2, TopoD2, Real >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD3, TopoD2, Real >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD2, TopoD2, DLA::VectorS<1,Real> >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD2, TopoD2, DLA::VectorS<2,Real> >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD2, TopoD2, DLA::VectorS<3,Real> >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD2, TopoD2, DLA::VectorS<4,Real> >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD2, TopoD2, DLA::VectorS<5,Real> >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD2, TopoD2, DLA::VectorS<6,Real> >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );

// For Boltzmann implementation
template void
output_Tecplot( const Field< PhysD2, TopoD2, DLA::VectorS<9,Real> >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
/*
template void
output_Tecplot( const Field< PhysD2, TopoD2, DLA::VectorS<13,Real> >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD2, TopoD2, DLA::VectorS<16,Real> >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
*/
template void
output_Tecplot( const Field< PhysD2, TopoD2, DLA::VectorS<NVar,Real> >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );

// PhysD3
template void
output_Tecplot( const Field< PhysD3, TopoD2, DLA::VectorS<1,Real> >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );

} //namespace SANS
