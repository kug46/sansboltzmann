// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define OUTPUT_TECPLOT_FIELDLINE_INSTANTIATE
#include "output_Tecplot_FieldLine_impl.h"

namespace SANS
{

//Explicitly instantiate the function
template void
output_Tecplot( const Field< PhysD2, TopoD1, Real >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );

template void
output_Tecplot( const Field< PhysD2, TopoD1, DLA::VectorS<1,Real> >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD2, TopoD1, DLA::VectorS<2,Real> >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD2, TopoD1, DLA::VectorS<3,Real> >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD2, TopoD1, DLA::VectorS<4,Real> >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD2, TopoD1, DLA::VectorS<6,Real> >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD2, TopoD1, DLA::VectorS<8,Real> >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD2, TopoD1, DLA::VectorS<22,Real> >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );

} //namespace SANS
