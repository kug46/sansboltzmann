// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define OUTPUT_TECPLOT_FIELDLINE_INSTANTIATE
#include "output_Tecplot_FieldLine_impl.h"

#include "Field/FieldLine_CG_Cell.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template<>
void
output_Tecplot( const XField<PhysD1,TopoD1>& xgrid, const std::string& filename )
{
  std::cout << "output_Tecplot: filename = " << filename << std::endl;
  FILE* fp = fopen( filename.c_str(), "w" );
  if (fp == NULL)
    SANS_DEVELOPER_EXCEPTION("output_Tecplot<PhysD1,TopoD1> - Error opening file: %s", filename.c_str());

  fprintf( fp, "\"\"\n" );
  fprintf( fp, "VARIABLES = \"X\", \"Native Index\", \"Node Rank\", \"Cell Rank\"\n" );

  const int ngroup = xgrid.nCellGroups();
  std::map<int,int> dofRank;

  int order = xgrid.getCellGroupBase(0).order();
  for (int group = 0; group < ngroup; group++)
    SANS_ASSERT(xgrid.getCellGroupBase(group).order() == order);

  Field_CG_Cell<PhysD1,TopoD1,Real> qfld(xgrid, order, BasisFunctionCategory_Lagrange);

  // loop over element groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over elements of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Line) )
    {
      dofRank_CellGroup<PhysD1, TopoD1, Line>( xgrid.local2nativeDOFmap(), xgrid.getCellGroup<Line>(group),
                                               qfld, qfld.getCellGroup<Triangle>(group), dofRank );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD1,TopoD1>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }


  // loop over element groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over elements of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Line) )
    {
      if (group == 0)
        output_Tecplot_CellGroup_Nodes<PhysD1, TopoD1, Line>( xgrid.local2nativeDOFmap(), xgrid.getCellGroup<Line>(group), dofRank, fp );
      else
        output_Tecplot_CellGroup_XField<PhysD1, TopoD1, Line>( xgrid.getCellGroup<Line>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD1,TopoD1>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  // loop over interior trace groups (skip the first one because it should mostly duplicate the entire grid)
//  for (int group = 1; group < xgrid.nInteriorTraceGroups(); group++)
//  {
//    // dispatch integration over cell of group
//    if ( xgrid.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Node) )
//    {
//      output_Tecplot_TraceGroup<PhysD1, TopoD1, Node>("Interior", group, xgrid.getInteriorTraceGroup<Node>(group), fp );
//    }
//    else
//    {
//      const char msg[] = "Error in output_Tecplot(XField<PhysD1,TopoD1>): unknown topology\n";
//      SANS_DEVELOPER_EXCEPTION( msg );
//    }
//  }

  // loop over trace boundary groups
  for (int group = 0; group < xgrid.nBoundaryTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Line) ) // TODO: should this be node?
    {
      output_Tecplot_TraceGroup_XField<PhysD1, TopoD1, Line>("Boundary", group, xgrid.getBoundaryTraceGroup<Line>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD1,TopoD1>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  fclose( fp );
}

//Explicitly instantiate the function
template void
output_Tecplot( const Field< PhysD1, TopoD1, Real >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );

template void
output_Tecplot( const Field< PhysD1, TopoD1, DLA::VectorS<1,Real> >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD1, TopoD1, DLA::VectorS<2,Real> >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD1, TopoD1, DLA::VectorS<3,Real> >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD1, TopoD1, DLA::VectorS<4,Real> >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD1, TopoD1, DLA::VectorS<5,Real> >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );

} //namespace SANS
