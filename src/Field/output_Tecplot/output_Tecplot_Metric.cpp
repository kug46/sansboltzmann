// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "output_Tecplot.h"
#include "Field/output_Tecplot/TecTopo.h"

#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/Eigen.h"

#include "XFieldLine.h"
#include "XFieldArea.h"
#include "XFieldVolume.h"
#include "XFieldSpacetime.h"

#include "FieldLine.h"
#include "FieldArea.h"
#include "FieldVolume.h"
#include "FieldSpacetime.h"

#include "FieldLiftLine_DG_Cell.h"
#include "FieldLiftArea_DG_Cell.h"
#include "FieldLiftVolume_DG_Cell.h"
#include "FieldLiftSpaceTime_DG_Cell.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"
#include "MPI/continuousElementMap.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#include <boost/mpi/collectives/gather.hpp>
#endif

namespace SANS
{

namespace // no-name to make everything private to this file
{


//----------------------------------------------------------------------------//
void
output_Tecplot_Metric_Nodes(
    const XField<PhysD1, TopoD1>& xfld,
    const  Field<PhysD1, TopoD1, DLA::MatrixSymS<PhysD1::D, Real>>& mfld,
    FILE* fp )
{
  // This function assumes linear grids and metric field
  for (int i = 0; i < xfld.nCellGroups(); i++)
  {
    SANS_ASSERT( xfld.getCellGroupBase(i).order() == 1 );
    SANS_ASSERT( mfld.getCellGroupBase(i).order() == 1 );
  }

  // Compute the total number of nodes in the grid
  int nNodeGlobal = mfld.nDOFpossessed();
#ifdef SANS_MPI
  nNodeGlobal = boost::mpi::all_reduce(*xfld.comm(), nNodeGlobal, std::plus<int>());
#endif

  int comm_rank = xfld.comm()->rank();

  int nElem = nNodeGlobal-1;

  if (comm_rank == 0)
    fprintf( fp, "ZONE T=\"metric\", N=%d, E=%d, F=FEPOINT, ET=LINESEG\n",
             nNodeGlobal, nElem );

  // grid coordinates and solution

#ifdef SANS_MPI
  typedef std::pair<DLA::VectorS<PhysD1::D,double>, DLA::MatrixSymS<PhysD1::D, double>> NodeMetricType;

  int comm_size = xfld.comm()->size();

  // maximum node chunk size that rank 0 will write at any given time
  int nodeChunk = nNodeGlobal / comm_size + nNodeGlobal % comm_size;

  // send one chunk of elements at a time to rank 0
  for (int rank = 0; rank < comm_size; rank++)
  {
    int nodeLow  =  rank   *nodeChunk;
    int nodeHigh = (rank+1)*nodeChunk;

    std::map<int,NodeMetricType> buffer;

    // loop over elements within group
    for (int node = 0; node < mfld.nDOFpossessed(); node++)
    {
      int nodeID = mfld.local2nativeDOFmap(node);

      if (nodeID >= nodeLow && nodeID < nodeHigh)
      {
        // copy global grid/solution DOFs to the buffer
        NodeMetricType nodeValues;

        nodeValues.first = xfld.DOF(node);
        nodeValues.second = mfld.DOF(node);

        buffer[nodeID] = nodeValues;
      }
    }

    if (comm_rank == 0)
    {
      std::vector<std::map<int,NodeMetricType>> bufferOnRank;
      boost::mpi::gather(*xfld.comm(), buffer, bufferOnRank, 0 );

      // collapse down the buffer from all other ranks
      for (std::size_t i = 1; i < bufferOnRank.size(); i++)
        buffer.insert(bufferOnRank[i].begin(), bufferOnRank[i].end());

      //Write the node elipse data
      for ( const auto& nodePair : buffer)
      {
        const NodeMetricType& XM = nodePair.second;

        fprintf(fp, " %.16e %.16e\n", XM.first[0], sqrt(1/XM.second(0,0)));
      }
    }
#ifndef __clang_analyzer__
    else // send the buffer to rank 0
      boost::mpi::gather(*xfld.comm(), buffer, 0 );
#endif

  } // for rank

#else

  // loop over elements within group
  for (int node = 0; node < nNodeGlobal; node++)
  {
    DLA::VectorS<PhysD1::D,double> Xn = xfld.DOF(node);
    DLA::MatrixSymS<PhysD1::D,double> m = mfld.DOF(node);

    fprintf(fp, " %.16e %.16e\n", Xn[0], sqrt(1/m(0,0)));
  }

#endif

  // cell-to-node connectivity

  if (comm_rank == 0)
  {
    for (int node = 0; node < nNodeGlobal-1; node++)
      fprintf(fp, " %d %d\n", 1 + node,  2 + node);
  }
}

//----------------------------------------------------------------------------//
void
output_Tecplot_Metric_Nodes(
    const XField<PhysD2, TopoD2>& xfld,
    const  Field<PhysD2, TopoD2, DLA::MatrixSymS<PhysD2::D, Real>>& mfld,
    FILE* fp )
{
  static const int npts = 36; // Number of points to draw each ellipse
  double scale = 0.5; // so the ellipses touch for an ideal grid
  double dt = PI/180.*(360.0 / (double)npts);

  DLA::VectorS<PhysD2::D,double> L, e, X;
  DLA::MatrixS<PhysD2::D,PhysD2::D,double> E;

  // This function assumes linear grids and metric field
  for (int i = 0; i < xfld.nCellGroups(); i++)
  {
    SANS_ASSERT( xfld.getCellGroupBase(i).order() == 1 );
    SANS_ASSERT( mfld.getCellGroupBase(i).order() == 1 );
  }

  // Compute the total number of nodes in the grid
  int nNodeGlobal = mfld.nDOFpossessed();
#ifdef SANS_MPI
  nNodeGlobal = boost::mpi::all_reduce(*xfld.comm(), nNodeGlobal, std::plus<int>());
#endif

  int comm_rank = xfld.comm()->rank();

  int nElem = nNodeGlobal * npts;

  if (comm_rank == 0)
    fprintf( fp, "ZONE T=\"metric\", N=%d, E=%d, F=FEPOINT, ET=LINESEG\n",
             nElem, nElem );

  // grid coordinates and solution

#ifdef SANS_MPI
  typedef std::pair<DLA::VectorS<PhysD2::D,double>, DLA::MatrixSymS<PhysD2::D, double>> NodeMetricType;

  int comm_size = xfld.comm()->size();

  // maximum node chunk size that rank 0 will write at any given time
  int nodeChunk = nNodeGlobal / comm_size + nNodeGlobal % comm_size;

  // send one chunk of elements at a time to rank 0
  for (int rank = 0; rank < comm_size; rank++)
  {
    int nodeLow  =  rank   *nodeChunk;
    int nodeHigh = (rank+1)*nodeChunk;

    std::map<int,NodeMetricType> buffer;

    // loop over elements within group
    for (int node = 0; node < mfld.nDOFpossessed(); node++)
    {
      int nodeID = mfld.local2nativeDOFmap(node);

      if (nodeID >= nodeLow && nodeID < nodeHigh)
      {
        // copy global grid/solution DOFs to the buffer
        NodeMetricType nodeValues;

        nodeValues.first = xfld.DOF(node);
        nodeValues.second = mfld.DOF(node);

        buffer[nodeID] = nodeValues;
      }
    }

    if (comm_rank == 0)
    {
      std::vector<std::map<int,NodeMetricType>> bufferOnRank;
      boost::mpi::gather(*xfld.comm(), buffer, bufferOnRank, 0 );

      // collapse down the buffer from all other ranks
      for (std::size_t i = 1; i < bufferOnRank.size(); i++)
        buffer.insert(bufferOnRank[i].begin(), bufferOnRank[i].end());

      //Write the node elipse data
      for ( const auto& nodePair : buffer)
      {
        const NodeMetricType& XM = nodePair.second;

        // compute the eigen system
        DLA::EigenSystem(XM.second, L, E );

        for (int i = 0; i < npts; i++)
        {
          e[0] = scale * cos(i * dt) / sqrt(L[0]);
          e[1] = scale * sin(i * dt) / sqrt(L[1]);
          X = E*e;
          fprintf(fp, " %.16e %.16e\n",
                  XM.first[0] + X[0],
                  XM.first[1] + X[1]);
        }
      }
    }
    else // send the buffer to rank 0
      boost::mpi::gather(*xfld.comm(), buffer, 0 );

  } // for rank

#else

  // loop over elements within group
  for (int node = 0; node < nNodeGlobal; node++)
  {
    DLA::VectorS<PhysD2::D,double> Xn = xfld.DOF(node);
    DLA::MatrixSymS<PhysD2::D,double> m = mfld.DOF(node);

    // compute the eigen system
    DLA::EigenSystem(m, L, E );

    for (int i = 0; i < npts; i++)
    {
      e[0] = scale * cos(i * dt) / sqrt(L[0]);
      e[1] = scale * sin(i * dt) / sqrt(L[1]);
      X = E*e;
      fprintf(fp, " %.16e %.16e\n",
              Xn[0] + X[0],
              Xn[1] + X[1]);
    }
  }

#endif

  // cell-to-node connectivity

  if (comm_rank == 0)
  {
    for (int node = 0; node < nNodeGlobal; node++)
    {
      for (int i = 0; i < npts - 1; i++)
        fprintf(fp, " %d %d\n", i + node * npts + 1, i + 1 + node * npts + 1);
      fprintf(fp, " %d %d\n", npts + node * npts, 1 + node * npts);
    }
  }
}


//----------------------------------------------------------------------------//
void
output_Tecplot_Metric_Nodes(
    const XField<PhysD3, TopoD3>& xfld,
    const  Field<PhysD3, TopoD3, DLA::MatrixSymS<PhysD3::D, Real>>& mfld,
    FILE* fp )
{
  static const int npts = 36; // Number of points to draw each ellipse
  double scale = 0.5; // so the ellipses touch for an ideal grid
  double dt = PI/180.*(360.0 / (double)npts);

  DLA::VectorS<PhysD3::D,double> L, e, X;
  DLA::MatrixS<PhysD3::D,PhysD3::D,double> E;

  // This function assumes linear grids and metric field
  for (int i = 0; i < xfld.nCellGroups(); i++)
  {
    SANS_ASSERT( xfld.getCellGroupBase(i).order() == 1 );
    SANS_ASSERT( mfld.getCellGroupBase(i).order() == 1 );
  }

  // Compute the total number of nodes in the grid
  int nNodeGlobal = mfld.nDOFpossessed();
#ifdef SANS_MPI
  nNodeGlobal = boost::mpi::all_reduce(*xfld.comm(), nNodeGlobal, std::plus<int>());
#endif

  int comm_rank = xfld.comm()->rank();

  int nElem = nNodeGlobal * npts;

  if (comm_rank == 0)
    fprintf( fp, "ZONE T=\"metric\", N=%d, E=%d, F=FEPOINT, ET=LINESEG\n",
             3*nElem, 3*nElem );

  // grid coordinates and solution

#ifdef SANS_MPI
  typedef std::pair<DLA::VectorS<PhysD3::D,double>, DLA::MatrixSymS<PhysD3::D, double>> NodeMetricType;

  int comm_size = xfld.comm()->size();

  // maximum node chunk size that rank 0 will write at any given time
  int nodeChunk = nNodeGlobal / comm_size + nNodeGlobal % comm_size;

  // send one chunk of elements at a time to rank 0
  for (int rank = 0; rank < comm_size; rank++)
  {
    int nodeLow  =  rank   *nodeChunk;
    int nodeHigh = (rank+1)*nodeChunk;

    std::map<int,NodeMetricType> buffer;

    // loop over elements within group
    for (int node = 0; node < mfld.nDOFpossessed(); node++)
    {
      int nodeID = mfld.local2nativeDOFmap(node);

      if (nodeID >= nodeLow && nodeID < nodeHigh)
      {
        // copy global grid/solution DOFs to the buffer
        NodeMetricType nodeValues;

        nodeValues.first = xfld.DOF(node);
        nodeValues.second = mfld.DOF(node);

        buffer[nodeID] = nodeValues;
      }
    }

    if (comm_rank == 0)
    {
      std::vector<std::map<int,NodeMetricType>> bufferOnRank;
      boost::mpi::gather(*xfld.comm(), buffer, bufferOnRank, 0 );

      // collapse down the buffer from all other ranks
      for (std::size_t i = 1; i < bufferOnRank.size(); i++)
        buffer.insert(bufferOnRank[i].begin(), bufferOnRank[i].end());

      //Write the node elipse data
      for ( const auto& nodePair : buffer)
      {
        const NodeMetricType& XM = nodePair.second;

        // compute the eigen system
        DLA::EigenSystem(XM.second, L, E );

        for (int e0 = 0; e0 < 3; e0++)
        {
          int e1 = e0 + 1;
          if (e1 == 3) e1 = 0;
          for (int i = 0; i < npts; i++)
          {
            double ex = scale * cos(i * dt) / sqrt(L[e0]);
            double ey = scale * sin(i * dt) / sqrt(L[e1]);
            X[0] = E(0,e0)*ex + E(0,e1)*ey;
            X[1] = E(1,e0)*ex + E(1,e1)*ey;
            X[2] = E(2,e0)*ex + E(2,e1)*ey;
            fprintf(fp, " %.16e %.16e %.16e\n",
                    XM.first[0] + X[0],
                    XM.first[1] + X[1],
                    XM.first[2] + X[2]);
          }
        }
      }
    }
    else // send the buffer to rank 0
      boost::mpi::gather(*xfld.comm(), buffer, 0 );

  } // for rank

#else

  // loop over elements within group
  for (int node = 0; node < nNodeGlobal; node++)
  {
    DLA::VectorS<PhysD3::D,double> Xn = xfld.DOF(node);
    DLA::MatrixSymS<PhysD3::D,double> m = mfld.DOF(node);

    // compute the eigen system
    DLA::EigenSystem(m, L, E );

    for (int e0 = 0; e0 < 3; e0++)
    {
      int e1 = e0 + 1;
      if (e1 == 3) e1 = 0;
      for (int i = 0; i < npts; i++)
      {
        double ex = scale * cos(i * dt) / sqrt(L[e0]);
        double ey = scale * sin(i * dt) / sqrt(L[e1]);
        X[0] = E(0,e0)*ex + E(0,e1)*ey;
        X[1] = E(1,e0)*ex + E(1,e1)*ey;
        X[2] = E(2,e0)*ex + E(2,e1)*ey;
        fprintf(fp, " %.16e %.16e %.16e\n",
                Xn[0] + X[0],
                Xn[1] + X[1],
                Xn[2] + X[2]);
      }
    }
  }

#endif

  // cell-to-node connectivity

  if (comm_rank == 0)
  {
    for (int e0 = 0; e0 < 3; e0++)
      for (int node = 0; node < nNodeGlobal; node++)
      {
        for (int i = 0; i < npts - 1; i++)
          fprintf(fp, " %d %d\n", i + node * npts + 1 + nElem * e0,
                  i + 1 + node * npts + 1 + nElem * e0);
        fprintf(fp, " %d %d\n", npts + node * npts + nElem * e0,
                1 + node * npts + nElem * e0);
      }
  }
}

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim, class Topology>
void
output_Tecplot_CellGroup_Nodes(
    const int* local2nativeDOFmap,
    const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfld,
    FILE* fp )
{
  int nnode = xfld.nDOF();
  int nelem = xfld.nElem();

  // tecplot does not like empty groups
  if ( nelem == 0 ) return;

  fprintf( fp, "ZONE T=\"grid\", N=%d, E=%d, F=FEBLOCK, ET=%s", nnode, nelem, TecTopo<Topology>::name() );

  // grid coordinates

  for (int d = 0; d < PhysDim::D; d++)
  {
    for (int n = 0; n < nnode; n++)
    {
      if (n % 5 == 0) fprintf( fp, "\n");
      fprintf( fp, "%22.15e ", xfld.DOF(n)[d] );
    }
    fprintf( fp, "\n");
  }

  // cell-to-node connectivity

  int nodeMap[Topology::NNode];
  for (int elem = 0; elem < nelem; elem++)
  {
    xfld.associativity( elem ).getNodeGlobalMapping( nodeMap, Topology::NNode );
    for (int n = 0; n < Topology::NNode; n++ )
      fprintf( fp, "%d ", local2nativeDOFmap[nodeMap[n]]+1 );
    fprintf( fp, "\n" );
  }
}

//----------------------------------------------------------------------------//
// output as P1
//
template<class PhysDim, class TopoDim, class Topology>
void
output_Tecplot_CellGroup(
    const int* local2nativeDOFmap,
    const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfld,
    FILE* fp )
{
  int nnode = xfld.nDOF();
  int nelem = xfld.nElem();

  // tecplot does not like empty groups
  if ( nelem == 0 ) return;

  fprintf( fp, "ZONE T=\"grid\", N=%d, E=%d, F=FEBLOCK, ET=%s", nnode, nelem, TecTopo<Topology>::name() );

  fprintf( fp, ", VARSHARELIST=([");
  for (int d = 0; d < PhysDim::D-1; d++)
    fprintf( fp, "%d,", d+1);
  fprintf( fp, "%d]=2)\n", PhysDim::D);

  // cell-to-node connectivity

  int nodeMap[Topology::NNode];
  for (int elem = 0; elem < nelem; elem++)
  {
    xfld.associativity( elem ).getNodeGlobalMapping( nodeMap, Topology::NNode );
    for (int n = 0; n < Topology::NNode; n++ )
      fprintf( fp, "%d ", nodeMap[n]+1 );
    fprintf( fp, "\n" );
  }
}


//----------------------------------------------------------------------------//
// output as P1
//
template<class PhysDim, class TopoDim, class Topology>
void
output_Tecplot_TraceGroup(
    const int* local2nativeDOFmap,
    const std::string& type, const int group,
    const typename XField<PhysDim,TopoDim>::template FieldTraceGroupType<Topology>& xfld,
    FILE* fp )
{
  int nnode = xfld.nDOF();
  int nelem = xfld.nElem();

  // tecplot does not like empty groups
  if ( nelem == 0 ) return;

  fprintf( fp, "ZONE T=\"%s %d\", N=%d, E=%d, F=FEBLOCK, ET=%s",
           type.c_str(), group, nnode, nelem, TecTopo<Topology>::name() );

  fprintf( fp, ", VARSHARELIST=([");
  for (int d = 0; d < PhysDim::D-1; d++)
    fprintf( fp, "%d,", d+1);
  fprintf( fp, "%d]=2)\n", PhysDim::D);

  // cell-to-node connectivity

  int nodeMap[Topology::NNode];
  for (int elem = 0; elem < nelem; elem++)
  {
    xfld.associativity( elem ).getNodeGlobalMapping( nodeMap, Topology::NNode );
    for (int n = 0; n < Topology::NNode; n++ )
      fprintf( fp, "%d ", nodeMap[n]+1 );
    fprintf( fp, "\n" );
  }

}

} // namespace

//----------------------------------------------------------------------------//
template<>
void
output_Tecplot_Metric( const Field< PhysD1, TopoD1, DLA::MatrixSymS<PhysD1::D, Real> >& mfld,
                       const std::string& filename )
{
  FILE* fp = nullptr;

  if (mfld.comm()->rank() == 0)
  {
    std::cout << "output_Tecplot: filename = " << filename << std::endl;
    fp = fopen( filename.c_str(), "w" );
    if (fp == NULL)
      SANS_RUNTIME_EXCEPTION("Error opening file: %s", filename.c_str());

    fprintf( fp, "\"\"\n" );
    fprintf( fp, "VARIABLES = \"X\", \"h\"\n" );
  }

  const XField<PhysD1,TopoD1>& xgrid = mfld.getXField();

  output_Tecplot_Metric_Nodes( xgrid, mfld, fp );

  if (mfld.comm()->rank() == 0)
    fclose( fp );
}

//----------------------------------------------------------------------------//
template<>
void
output_Tecplot_Metric( const Field< PhysD2, TopoD2, DLA::MatrixSymS<PhysD2::D, Real> >& mfld,
                       const std::string& filename )
{
  FILE* fp = nullptr;

  if (mfld.comm()->rank() == 0)
  {
    std::cout << "output_Tecplot: filename = " << filename << std::endl;
    fp = fopen( filename.c_str(), "w" );
    if (fp == NULL)
      SANS_RUNTIME_EXCEPTION("Error opening file: %s", filename.c_str());

    fprintf( fp, "\"\"\n" );
    fprintf( fp, "VARIABLES = \"X\", \"Y\"\n" );
  }

  const XField<PhysD2,TopoD2>& xgrid = mfld.getXField();

  output_Tecplot_Metric_Nodes( xgrid, mfld, fp );

  const int ngroup = xgrid.nCellGroups();

  // loop over element groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over elements of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      if (group == 0)
        output_Tecplot_CellGroup_Nodes<PhysD2, TopoD2, Triangle>( xgrid.local2nativeDOFmap(), xgrid.getCellGroup<Triangle>(group), fp );
      else
        output_Tecplot_CellGroup<PhysD2, TopoD2, Triangle>( xgrid.local2nativeDOFmap(), xgrid.getCellGroup<Triangle>(group), fp );
    }
    else if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Quad) )
    {
      if (group == 0)
        output_Tecplot_CellGroup_Nodes<PhysD2, TopoD2, Quad>( xgrid.local2nativeDOFmap(), xgrid.getCellGroup<Quad>(group), fp );
      else
        output_Tecplot_CellGroup<PhysD2, TopoD2, Quad>( xgrid.local2nativeDOFmap(), xgrid.getCellGroup<Quad>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD2,TopoD2>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  // loop over interior trace groups (skip the first one because it should mostly duplicate the entire grid)
  for (int group = 1; group < xgrid.nInteriorTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Line) )
    {
      output_Tecplot_TraceGroup<PhysD2, TopoD2, Line>(xgrid.local2nativeDOFmap(), "Interior", group, xgrid.getInteriorTraceGroup<Line>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD2,TopoD2>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  // loop over trace boundary groups
  for (int group = 0; group < xgrid.nBoundaryTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Line) )
    {
      output_Tecplot_TraceGroup<PhysD2, TopoD2, Line>(xgrid.local2nativeDOFmap(), "Boundary", group, xgrid.getBoundaryTraceGroup<Line>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD2,TopoD2>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  // loop over ghost trace boundary groups
  for (int group = 0; group < xgrid.nGhostBoundaryTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Line) )
    {
      output_Tecplot_TraceGroup<PhysD2, TopoD2, Line>(xgrid.local2nativeDOFmap(), "Ghost", group, xgrid.getGhostBoundaryTraceGroup<Line>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD2,TopoD2>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  if (mfld.comm()->rank() == 0)
    fclose( fp );
}


//----------------------------------------------------------------------------//
template<>
void
output_Tecplot_Metric( const Field< PhysD3, TopoD3, DLA::MatrixSymS<PhysD3::D, Real> >& mfld,
                       const std::string& filename )
{
  FILE* fp = nullptr;

  if (mfld.comm()->rank() == 0)
  {
    std::cout << "output_Tecplot: filename = " << filename << std::endl;
    fp = fopen( filename.c_str(), "w" );
    if (fp == NULL)
      SANS_RUNTIME_EXCEPTION("Error opening file: %s", filename.c_str());

    fprintf( fp, "\"\"\n" );
    fprintf( fp, "VARIABLES = \"X\", \"Y\", \"Z\"\n" );
  }

  const XField<PhysD3,TopoD3>& xgrid = mfld.getXField();

  output_Tecplot_Metric_Nodes( xgrid, mfld, fp );

  const int ngroup = xgrid.nCellGroups();

  // loop over cell groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Tet) )
    {
      if (group == 0)
        output_Tecplot_CellGroup_Nodes<PhysD3, TopoD3, Tet>( xgrid.local2nativeDOFmap(),
                                                             xgrid.getCellGroup<Tet>(group), fp );
      else
        output_Tecplot_CellGroup<PhysD3, TopoD3, Tet>( xgrid.local2nativeDOFmap(),
                                                       xgrid.getCellGroup<Tet>(group), fp );
    }
    else if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Hex) )
    {
      if (group == 0)
        output_Tecplot_CellGroup_Nodes<PhysD3, TopoD3, Hex>( xgrid.local2nativeDOFmap(),
                                                             xgrid.getCellGroup<Hex>(group), fp );
      else
        output_Tecplot_CellGroup<PhysD3, TopoD3, Hex>( xgrid.local2nativeDOFmap(),
                                                       xgrid.getCellGroup<Hex>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD3,TopoD3>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  // loop over trace boundary groups
  for (int group = 0; group < xgrid.nBoundaryTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      output_Tecplot_TraceGroup<PhysD3, TopoD3, Triangle>(xgrid.local2nativeDOFmap(), "Boundary", group,
                                                          xgrid.getBoundaryTraceGroup<Triangle>(group), fp );
    }
    else if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Quad) )
    {
      output_Tecplot_TraceGroup<PhysD3, TopoD3, Quad>(xgrid.local2nativeDOFmap(), "Boundary", group,
                                                      xgrid.getBoundaryTraceGroup<Quad>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD3,TopoD3>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  // loop over ghost trace boundary groups
  for (int group = 0; group < xgrid.nGhostBoundaryTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      output_Tecplot_TraceGroup<PhysD3, TopoD3, Triangle>(xgrid.local2nativeDOFmap(), "Ghost", group,
                                                          xgrid.getGhostBoundaryTraceGroup<Triangle>(group), fp );
    }
    else if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Quad) )
    {
      output_Tecplot_TraceGroup<PhysD3, TopoD3, Quad>(xgrid.local2nativeDOFmap(), "Ghost", group,
                                                      xgrid.getGhostBoundaryTraceGroup<Quad>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD3,TopoD3>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  if (mfld.comm()->rank() == 0)
    fclose( fp );
}

//----------------------------------------------------------------------------//
template<>
void
output_Tecplot_Metric( const Field< PhysD4, TopoD4, DLA::MatrixSymS<PhysD4::D, Real> >& mfld,
                       const std::string& filename )
{
  SANS_DEVELOPER_EXCEPTION("implement");
}


} //namespace SANS
