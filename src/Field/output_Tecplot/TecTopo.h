// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef OUTPUT_TECPLOT_IMPL
#define OUTPUT_TECPLOT_IMPL

#include "Topology/ElementTopology.h"

namespace SANS
{

template<class Topology>
struct TecTopo;

template<>
struct TecTopo<Line>
{
  static const char* name() { return "LINESEG"; }
};

template<>
struct TecTopo<Triangle>
{
  static const char* name() { return "TRIANGLE"; }
};

template<>
struct TecTopo<Quad>
{
  static const char* name() { return "QUADRILATERAL"; }
};

template<>
struct TecTopo<Tet>
{
  static const char* name() { return "TETRAHEDRON"; }
};

template<>
struct TecTopo<Hex>
{
  static const char* name() { return "BRICK"; }
};

template<>
struct TecTopo<Pentatope>
{
  static const char* name() { return "PENTATOPE"; }
};

} // nwmespace SANS

#endif //OUTPUT_TECPLOT_IMPL
