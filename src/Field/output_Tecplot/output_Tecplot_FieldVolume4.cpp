// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define OUTPUT_TECPLOT_FIELD_INSTANTIATE
#include "output_Tecplot_Field_impl.h"

#include "Field/XFieldSpacetime.h"

#include "Field/FieldSpacetime.h"

namespace SANS
{

#if 0 // implement
//----------------------------------------------------------------------------//
template<>
void
output_Tecplot( const XField<PhysD4,TopoD4>& xgrid, const std::string& filename )
{
  std::cout << "output_Tecplot: filename = " << filename << std::endl;
  FILE* fp = fopen( filename.c_str(), "w" );
  if (fp == NULL)
    SANS_DEVELOPER_EXCEPTION("output_Tecplot<PhysD3,TopoD3> - Error opening file: %s", filename.c_str());

  fprintf( fp, "\"\"\n" );
  fprintf( fp, "VARIABLES = \"X\", \"Y\", \"Z\", \"T\", \"Native Index\", \"Node Rank\", \"Cell Rank\"\n" );

  const int ngroup = xgrid.nCellGroups();
  std::map<int,int> nodeMinRank;

  // loop over element groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over elements of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Pentatope) )
    {
      nodeRank_CellGroup<PhysD3, TopoD3, Tet>( xgrid.local2nativeDOFmap(), xgrid.getCellGroup<Pentatope>(group), nodeMinRank );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown topology" );
  }
  finishNodeRanking( *xgrid.comm(), nodeMinRank );


  // loop over cell groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Tet) )
    {
      if (group == 0)
        output_Tecplot_CellGroup_Nodes<PhysD3, TopoD3, Tet>( xgrid.local2nativeDOFmap(), xgrid.getCellGroup<Tet>(group), nodeMinRank, fp );
      else
        output_Tecplot_CellGroup_XField<PhysD3, TopoD3, Tet>( xgrid.getCellGroup<Tet>(group), fp );
    }
    else if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Hex) )
    {
      if (group == 0)
        output_Tecplot_CellGroup_Nodes<PhysD3, TopoD3, Hex>( xgrid.local2nativeDOFmap(), xgrid.getCellGroup<Hex>(group), nodeMinRank, fp );
      else
        output_Tecplot_CellGroup_XField<PhysD3, TopoD3, Hex>( xgrid.getCellGroup<Hex>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD3,TopoD3>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  // loop over trace boundary groups
  for (int group = 0; group < xgrid.nBoundaryTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      output_Tecplot_TraceGroup<PhysD3, TopoD3, Triangle>("Boundary", group, xgrid.getBoundaryTraceGroup<Triangle>(group), fp );
    }
    else if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Quad) )
    {
      output_Tecplot_TraceGroup<PhysD3, TopoD3, Quad>("Boundary", group, xgrid.getBoundaryTraceGroup<Quad>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD3,TopoD3>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  // loop over ghost trace boundary groups
  for (int group = 0; group < xgrid.nGhostBoundaryTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      output_Tecplot_TraceGroup<PhysD3, TopoD3, Triangle>("Ghost", group, xgrid.getGhostBoundaryTraceGroup<Triangle>(group), fp );
    }
    else if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Quad) )
    {
      output_Tecplot_TraceGroup<PhysD3, TopoD3, Quad>("Ghost", group, xgrid.getGhostBoundaryTraceGroup<Quad>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD3,TopoD3>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  fclose( fp );
}
#endif

//----------------------------------------------------------------------------//
template<class T>
void
output_Tecplot( const Field< PhysD4, TopoD4, T >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names,
                const bool partitioned )
{
  FILE* fp = nullptr;

  if (qfld.comm()->rank() == 0 || partitioned)
  {
    std::cout << "output_Tecplot: filename = " << filename << std::endl;
    fp = fopen( filename.c_str(), "w" );
    if (fp == NULL)
      SANS_RUNTIME_EXCEPTION("output_Tecplot<PhysD4,TopoD4> - Error opening file: %s", filename.c_str());

    fprintf( fp, "\"\"\n" );
    fprintf( fp, "VARIABLES = \"X\", \"Y\", \"Z\", \"T\"" );

    static const int N = DLA::VectorSize<T>::M;
    if (state_names.empty())
    {
      for (int k = 0; k < N; k++)
        fprintf( fp, ", \"q%d\"", k+1 );
    }
    else
    {
      SANS_ASSERT((int) state_names.size() == N);
      for (int k = 0; k < N; k++)
        fprintf( fp, ", \"%s\"", state_names[k].c_str());
    }
    fprintf( fp, "\n" );
  }

  const XField<PhysD4,TopoD4>& xgrid = qfld.getXField();

  const int ngroup = qfld.nCellGroups();

#ifdef TECPLOT_SUPPORTS_PENTATOPES // not happening any time soon
  // loop over cell groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Pentatope) )
    {
      output_Tecplot_CellGroup<T, PhysD4, TopoD4, Pentatope>( *xgrid.comm(), xgrid.cellIDs(group), partitioned,
                                                              xgrid.template getCellGroup<Pentatope>(group),
                                                              qfld.template getCellGroup<Pentatope>(group), fp );
    }
    else
      SANS_DEVELOPER_EXCEPTION("Unknown topology");
  }
#endif

  // loop over trace boundary groups
  for (int groupTrace = 0; groupTrace < xgrid.nBoundaryTraceGroups(); groupTrace++)
  {

    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(groupTrace).topoTypeID() == typeid(Tet) )
    {
      // determine topology for Left cell group
      int groupL = xgrid.template getBoundaryTraceGroup<Tet>(groupTrace).getGroupLeft();

      if ( xgrid.getCellGroupBase(groupL).topoTypeID() == typeid(Pentatope) )
        output_Tecplot_TraceCellGroup<T, PhysD4, TopoD4, Pentatope, Tet>( groupTrace,
                                                                          *xgrid.comm(), xgrid.boundaryTraceIDs(groupTrace), partitioned,
                                                                           xgrid.template getBoundaryTraceGroup<Tet>(groupTrace),
                                                                           qfld.template getCellGroup<Pentatope>(groupL), fp );
      else
        SANS_DEVELOPER_EXCEPTION("Unknown topology");
    }
    else
      SANS_DEVELOPER_EXCEPTION("Unknown topology");
  }

  if (ngroup  == 0)
  {
    SANS_ASSERT_MSG(qfld.comm()->size() == 0, "Pure trace fields cannot be written in parallel yet");

    // loop over trace boundary groups
    for (int group = 0; group < qfld.nBoundaryTraceGroups(); group++)
    {
      // dispatch integration over cell of group
      if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Tet) )
      {
        output_Tecplot_TraceGroup<T, PhysD4, TopoD4, Tet>( group, xgrid.template getBoundaryTraceGroup<Tet>(group),
                                                                qfld.template getBoundaryTraceGroup<Tet>(group), fp );
      }
      else
      {
        const char msg[] = "Error in output_Tecplot(XField<PhysD4,TopoD4>): unknown topology\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }
  }

  if (qfld.comm()->rank() == 0 || partitioned)
    fclose( fp );
}

//Explicitly instantiate the function
template void
output_Tecplot( const Field< PhysD4, TopoD4, Real >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD4, TopoD4, DLA::VectorS<1,Real> >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD4, TopoD4, DLA::VectorS<2,Real> >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD4, TopoD4, DLA::VectorS<3,Real> >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD4, TopoD4, DLA::VectorS<4,Real> >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD4, TopoD4, DLA::VectorS<5,Real> >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD4, TopoD4, DLA::VectorS<6,Real> >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Tecplot( const Field< PhysD4, TopoD4, DLA::VectorS<7,Real> >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names, const bool partitioned );

} //namespace SANS
