// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(OUTPUT_TECPLOT_FIELD_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "Field/output_Tecplot.h"
#include "TecTopo.h"

#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"

#include "Field/XField.h"

#include "Field/Field.h"

#include "Field/Element/ReferenceElementMesh.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"
#include "MPI/continuousElementMap.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_to_all.hpp>
#include <boost/mpi/collectives/all_reduce.hpp>
#endif

namespace SANS
{

//----------------------------------------------------------------------------//
// output as P1
//
template<class PhysDim, class TopoDim, class Topology>
void
dofRank_CellGroup( const int* local2nativeDOFmap,
                   const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldGroup,
                   const Field<PhysDim,TopoDim,Real>& qfld,
                   const typename Field<PhysDim,TopoDim,Real>::template FieldCellGroupType<Topology>& qfldGroup,
                   std::map<int,int>& dofRank )
{

  const int comm_rank = qfld.comm()->rank();
  int nelem = xfldGroup.nElem();

  if (nelem == 0) return;

  SANS_ASSERT(xfldGroup.nBasis() == qfldGroup.nBasis());

  const int nDOF = xfldGroup.nBasis();
  std::vector<int> xDOFMap(nDOF);
  std::vector<int> qDOFMap(nDOF);
  for (int elem = 0; elem < nelem; elem++)
  {
    xfldGroup.associativity( elem ).getGlobalMapping( xDOFMap.data(), xDOFMap.size() );
    qfldGroup.associativity( elem ).getGlobalMapping( qDOFMap.data(), qDOFMap.size() );
    for (int n = 0; n < nDOF; n++ )
    {
      int rank = comm_rank;
      int nativeNode = local2nativeDOFmap[xDOFMap[n]];

      if (qDOFMap[n] >= qfld.nDOFpossessed()) rank = qfld.DOFghost_rank(qDOFMap[n]);

      dofRank[nativeNode] = rank;
    }
  }
}

//----------------------------------------------------------------------------//
// output as P1
//
template<class PhysDim, class TopoDim, class Topology>
void
output_Tecplot_CellGroup_Nodes(
    const int* local2nativeDOFmap,
    const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfld,
    const std::map<int,int>& nodeMinRank,
    FILE* fp )
{
  int nnode = xfld.nDOF();
  int nelem = xfld.nElem();

  // tecplot does not like empty groups
  if ( nelem == 0 ) return;

  fprintf( fp, "ZONE T=\"grid\", N=%d, E=%d, F=FEBLOCK, ET=%s", nnode, nelem, TecTopo<Topology>::name() );

  fprintf( fp, ", VARLOCATION=([");
  for (int d = 0; d < PhysDim::D+1; d++)
    fprintf( fp, "%d,", d+1);
  fprintf( fp, "%d]=NODAL,[%d]=CELLCENTERED)\n", PhysDim::D+2, PhysDim::D+3);

  // grid coordinates

  for (int d = 0; d < PhysDim::D; d++)
  {
    for (int n = 0; n < nnode; n++)
    {
      if (n % 5 == 0) fprintf( fp, "\n");
      fprintf( fp, "%22.15e ", xfld.DOF(n)[d] );
    }
    fprintf( fp, "\n");
  }

  // native DOF index

  for (int n = 0; n < nnode; n++)
  {
    if (n % 5 == 0) fprintf( fp, "\n");
    fprintf( fp, "%d ", local2nativeDOFmap[n]);
  }
  fprintf( fp, "\n");

  // node rank

  for (int n = 0; n < nnode; n++)
  {
    if (n % 5 == 0) fprintf( fp, "\n");
    fprintf( fp, "%d ", nodeMinRank.at(local2nativeDOFmap[n]) );
  }
  fprintf( fp, "\n");

  // element rank

  for (int elem = 0; elem < nelem; elem++)
  {
    if (elem % 5 == 0) fprintf( fp, "\n");
    fprintf( fp, "%d ", xfld.associativity( elem ).rank() );
  }
  fprintf( fp, "\n" );

  // cell-to-node connectivity

  int nodeMap[Topology::NNode];
  for (int elem = 0; elem < nelem; elem++)
  {
    xfld.associativity( elem ).getNodeGlobalMapping( nodeMap, Topology::NNode );
    for (int n = 0; n < Topology::NNode; n++ )
      fprintf( fp, "%d ", nodeMap[n]+1 );
    fprintf( fp, "\n" );
  }
}

//----------------------------------------------------------------------------//
// output as P1
//
template<class PhysDim, class TopoDim, class Topology>
void
output_Tecplot_CellGroup_XField( const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfld, FILE* fp )
{
  int nnode = xfld.nDOF();
  int nelem = xfld.nElem();

  // tecplot does not like empty groups
  if ( nelem == 0 ) return;

  fprintf( fp, "ZONE T=\"grid\", N=%d, E=%d, F=FEBLOCK, ET=%s", nnode, nelem, TecTopo<Topology>::name() );

  fprintf( fp, ", VARLOCATION=([");
  for (int d = 0; d < PhysDim::D+1; d++)
    fprintf( fp, "%d,", d+1);
  fprintf( fp, "%d]=NODAL,[%d]=CELLCENTERED)", PhysDim::D+2, PhysDim::D+3);

  fprintf( fp, ", VARSHARELIST=([");
  for (int d = 0; d < PhysDim::D+1; d++)
    fprintf( fp, "%d,", d+1);
  fprintf( fp, "%d]=1)\n", PhysDim::D+2);

  // element rank

  for (int elem = 0; elem < nelem; elem++)
  {
    if (elem % 5 == 0) fprintf( fp, "\n");
    fprintf( fp, "%d ", xfld.associativity( elem ).rank() );
  }
  fprintf( fp, "\n" );

  // cell-to-node connectivity

  int nodeMap[Topology::NNode];
  for (int elem = 0; elem < nelem; elem++)
  {
    xfld.associativity( elem ).getNodeGlobalMapping( nodeMap, Topology::NNode );
    for (int n = 0; n < Topology::NNode; n++ )
      fprintf( fp, "%d ", nodeMap[n]+1 );
    fprintf( fp, "\n" );
  }
}


//----------------------------------------------------------------------------//
// output as P1
//
template<class PhysDim, class TopoDim, class Topology>
void
output_Tecplot_TraceGroup_XField( const std::string& type, const int group,
                                  const typename XField<PhysDim,TopoDim>::template FieldTraceGroupType<Topology>& xfld, FILE* fp )
{
  int nnode = xfld.nDOF();
  int nelem = xfld.nElem();

  // tecplot does not like empty groups
  if ( nelem == 0 ) return;

  fprintf( fp, "ZONE T=\"%s %d\", N=%d, E=%d, F=FEBLOCK, ET=%s",
           type.c_str(), group, nnode, nelem, TecTopo<Topology>::name() );

  fprintf( fp, ", VARLOCATION=([");
  for (int d = 0; d < PhysDim::D+1; d++)
    fprintf( fp, "%d,", d+1);
  fprintf( fp, "%d]=NODAL,[%d]=CELLCENTERED)", PhysDim::D+2, PhysDim::D+3);

  fprintf( fp, ", VARSHARELIST=([");
  for (int d = 0; d < PhysDim::D+1; d++)
    fprintf( fp, "%d,", d+1);
  fprintf( fp, "%d]=1)\n", PhysDim::D+2);

  // element rank

  for (int elem = 0; elem < nelem; elem++)
  {
    if (elem % 5 == 0) fprintf( fp, "\n");
    fprintf( fp, "%d ", xfld.associativity( elem ).rank() );
  }
  fprintf( fp, "\n" );

  // cell-to-node connectivity

  int nodeMap[Topology::NNode];
  for (int elem = 0; elem < nelem; elem++)
  {
    xfld.associativity( elem ).getNodeGlobalMapping( nodeMap, Topology::NNode );
    for (int n = 0; n < Topology::NNode; n++ )
      fprintf( fp, "%d ", nodeMap[n]+1 );
    fprintf( fp, "\n" );
  }

}


//----------------------------------------------------------------------------//
template<class T, class PhysDim, class TopoDim, class Topology>
void
output_Tecplot_CellGroup( mpi::communicator& comm, const std::vector<int>& cellIDs, const bool partitioned,
                          const typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology>& xfldGroup,
                          const typename  Field<PhysDim, TopoDim, T>::template FieldCellGroupType<Topology>& qfldGroup,
                          FILE* fp )
{
  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename  Field<PhysDim, TopoDim, T>::template FieldCellGroupType<Topology> QFieldCellGroupType;

  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfldGroup.basis() );
  ElementQFieldClass qfldElem( qfldGroup.basis() );

  static const int N = DLA::VectorSize<T>::M;

  const int order = max(xfldElem.order(), qfldElem.order());
  int nRefine = max(order-1,0);
  ReferenceElementMesh<Topology> refMesh(nRefine);
  const int nRefnode = refMesh.nodes.size();
  const int nRefelem = refMesh.elems.size();

  int nElem_global;
  std::map<int,int> globalElemMap;
  continuousElementMap( comm, cellIDs, xfldGroup, nElem_global, globalElemMap);

  int nElemLocal = xfldGroup.nElem();

  // tecplot does not like empty groups
  if ( nElem_global == 0 ) return;
  if ( nElemLocal == 0 && partitioned ) return;

  if (partitioned) nElem_global = nElemLocal;

  int comm_rank = comm.rank();

  if (comm_rank == 0 || partitioned)
    fprintf( fp, "ZONE T=\"grid\", N=%d, E=%d, F=FEPOINT, ET=%s\n",
             nRefnode*nElem_global, nRefelem*nElem_global, TecTopo<Topology>::name() );

  // grid coordinates and solution

  typename ElementQFieldClass::RefCoordType sRefCell;
  typename ElementXFieldClass::VectorX X;
  T q;

#ifdef SANS_MPI
  if (!partitioned)
  {
    int comm_size = comm.size();

    // maximum element chunk size that rank 0 will write at any given time
    int Elemchunk = nElem_global / comm_size + nElem_global % comm_size;

    // send one chunk of elements at a time to rank 0
    for (int rank = 0; rank < comm_size; rank++)
    {
      int elemLow  =  rank   *Elemchunk;
      int elemHigh = (rank+1)*Elemchunk;

      std::map<int,std::vector<DLA::VectorS<PhysDim::D+N,double>>> buffer;

      // loop over elements within group
      for (int elem = 0; elem < nElemLocal; elem++)
      {
        int elemID = globalElemMap[cellIDs[elem]];

        if (elemID >= elemLow && elemID < elemHigh &&
            xfldGroup.associativity(elem).rank() == comm_rank)
        {
          // copy global grid/solution DOFs to element
          xfldGroup.getElement( xfldElem, elem );
          qfldGroup.getElement( qfldElem, elem );

          std::vector<DLA::VectorS<PhysDim::D+N,double>> nodeValues(nRefnode);

          for ( int i = 0; i < nRefnode; i++ )
          {
            sRefCell = refMesh.nodes[i];

            xfldElem.eval( sRefCell, X );
            qfldElem.eval( sRefCell, q );

            int n = 0;
            for (int k = 0; k < PhysDim::D; k++)
              nodeValues[i][n++] = X[k];
            for (int k = 0; k < N; k++)
              nodeValues[i][n++] = DLA::index(q,k);
          }

          buffer[elemID] = std::move(nodeValues);
        }
      }


      if (comm_rank == 0)
      {
        std::vector<std::map<int,std::vector<DLA::VectorS<PhysDim::D+N,double>>>> bufferOnRank;
        boost::mpi::gather(comm, buffer, bufferOnRank, 0 );

        // collapse down the buffer from all other ranks
        for (std::size_t i = 1; i < bufferOnRank.size(); i++)
          buffer.insert(bufferOnRank[i].begin(), bufferOnRank[i].end());

        //Write the node data
        for ( const auto& nodesPair : buffer)
          for ( int i = 0; i < nRefnode; i++ )
          {
            for (int n = 0; n < PhysDim::D+N; n++)
              fprintf( fp, "%22.15e ", nodesPair.second[i][n] );
            fprintf( fp, "\n" );
          }
      }
      else // send the buffer to rank 0
        boost::mpi::gather(comm, buffer, 0 );

    } // for rank

  }
  else // if partitioned
  {
#endif

    // loop over elements within group
    for (int elem = 0; elem < nElemLocal; elem++)
    {
      // copy global grid/solution DOFs to element
      xfldGroup.getElement( xfldElem, elem );
      qfldGroup.getElement( qfldElem, elem );

      for ( int i = 0; i < nRefnode; i++ )
      {
        sRefCell = refMesh.nodes[i];

        xfldElem.eval( sRefCell, X );
        qfldElem.eval( sRefCell, q );

        for (int k = 0; k < PhysDim::D; k++)
          fprintf( fp, "%22.15e ", X[k] );
        for (int k = 0; k < N; k++)
          fprintf( fp, "%22.15e ", DLA::index(q,k) );
        fprintf( fp, "\n" );
      }
    }

#ifdef SANS_MPI
  } // if !partitioned
#endif

  // cell-to-node connectivity

  if (comm_rank == 0 || partitioned)
    for (int elem = 0; elem < nElem_global; elem++)
    {
      int offset = nRefnode*elem;
      for (int relem = 0; relem < nRefelem; relem++)
      {
        for (int n = 0; n < Topology::NNode; n++ )
          fprintf( fp, "%d ", offset + refMesh.elems[relem][n] + 1 ); // +1 for one based indexing
        fprintf( fp, "\n" );
      }
    }
}

//----------------------------------------------------------------------------//
template<class T, class PhysDim, class TopoDim, class TopologyCell, class TopologyTrace>
void
output_Tecplot_TraceCellGroup( const int groupTrace,
                               mpi::communicator& comm, const std::vector<int>& boundaryTraceIDs, const bool partitioned,
                               const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
                               const typename  Field<PhysDim, TopoDim, T>::template FieldCellGroupType<TopologyCell>& qfldCellL,
                               FILE* fp )
{
  typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename  Field<PhysDim, TopoDim, T>::template FieldCellGroupType<TopologyCell> QFieldCellGroupType;

  typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldClass;
  typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfldTrace.basis() );
  ElementQFieldClass qfldElem( qfldCellL.basis() );

  static const int N = DLA::VectorSize<T>::M;

  const int order = max(xfldElem.order(), qfldElem.order());
  int nRefine = max(order-1,0);
  ReferenceElementMesh<TopologyTrace> refMesh(nRefine);
  const int nRefnode = refMesh.nodes.size();
  const int nRefelem = refMesh.elems.size();

  int nElem_global;
  std::map<int,int> globalElemMap;
  continuousElementMap( comm, boundaryTraceIDs, xfldTrace, nElem_global, globalElemMap);

  int nElemLocal = xfldTrace.nElem();

  // tecplot does not like empty groups
  if ( nElem_global == 0 ) return;
  if ( nElemLocal == 0 && partitioned ) return;

  if (partitioned) nElem_global = nElemLocal;

  int comm_rank = comm.rank();

  if (comm_rank == 0 || partitioned)
    fprintf( fp, "ZONE T=\"Boundary %d\", N=%d, E=%d, F=FEPOINT, ET=%s\n",
             groupTrace, nRefnode*nElem_global, nRefelem*nElem_global, TecTopo<TopologyTrace>::name() );

  typename ElementQFieldClass::RefCoordType sRefCell;
  typename ElementXFieldClass::RefCoordType sRefTrace;
  typename ElementXFieldClass::VectorX X;
  T q;

#ifdef SANS_MPI
  if (!partitioned)
  {
    int comm_size = comm.size();

    // maximum element chunk size that rank 0 will write at any given time
    int Elemchunk = nElem_global / comm_size + nElem_global % comm_size;

    // send one chunk of elements at a time to rank 0
    for (int rank = 0; rank < comm_size; rank++)
    {
      int elemLow  =  rank   *Elemchunk;
      int elemHigh = (rank+1)*Elemchunk;

      std::map<int,std::vector<DLA::VectorS<PhysDim::D+N,double>>> buffer;

      // loop over elements within group
      for (int elem = 0; elem < nElemLocal; elem++)
      {
        int elemID = globalElemMap[boundaryTraceIDs[elem]];

        if (elemID >= elemLow && elemID < elemHigh &&
            xfldTrace.associativity(elem).rank() == comm_rank)
        {
           const int elemL = xfldTrace.getElementLeft( elem );
           CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

           xfldTrace.getElement( xfldElem, elem );
           qfldCellL.getElement( qfldElem, elemL );

           std::vector<DLA::VectorS<PhysDim::D+N,double>> nodeValues(nRefnode);

           for ( int i = 0; i < nRefnode; i++ )
           {
             sRefTrace = refMesh.nodes[i];

             // adjacent cell-element reference coords
             TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyCell>::eval( canonicalTraceL, sRefTrace, sRefCell );

             xfldElem.eval( sRefTrace, X );
             qfldElem.eval( sRefCell, q );

             int n = 0;
             for (int k = 0; k < PhysDim::D; k++)
               nodeValues[i][n++] = X[k];
             for (int k = 0; k < N; k++)
               nodeValues[i][n++] = DLA::index(q,k);
           }

          buffer[elemID] = std::move(nodeValues);
        }
      }


      if (comm_rank == 0)
      {
        std::vector<std::map<int,std::vector<DLA::VectorS<PhysDim::D+N,double>>>> bufferOnRank;
        boost::mpi::gather(comm, buffer, bufferOnRank, 0 );

        // collapse down the buffer from all other ranks
        for (std::size_t i = 1; i < bufferOnRank.size(); i++)
          buffer.insert(bufferOnRank[i].begin(), bufferOnRank[i].end());

        //Write the node data
        for ( const auto& nodesPair : buffer)
          for ( int i = 0; i < nRefnode; i++ )
          {
            for (int n = 0; n < PhysDim::D+N; n++)
              fprintf( fp, "%22.15e ", nodesPair.second[i][n] );
            fprintf( fp, "\n" );
          }
      }
      else // send the buffer to rank 0
        boost::mpi::gather(comm, buffer, 0 );

    } // for rank

  }
  else // if partitioned
  {
#endif

  for (int elem = 0; elem < nElemLocal; elem++)
  {
    const int elemL = xfldTrace.getElementLeft( elem );
    CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

    xfldTrace.getElement( xfldElem, elem );
    qfldCellL.getElement( qfldElem, elemL );

    for ( int i = 0; i < nRefnode; i++ )
    {
      sRefTrace = refMesh.nodes[i];

      // adjacent cell-element reference coords
      TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyCell>::eval( canonicalTraceL, sRefTrace, sRefCell );

      xfldElem.eval( sRefTrace, X );
      qfldElem.eval( sRefCell, q );

      for (int k = 0; k < PhysDim::D; k++)
        fprintf( fp, "%22.15e ", X[k] );
      for (int k = 0; k < N; k++)
        fprintf( fp, "%22.15e ", DLA::index(q,k) );
      fprintf( fp, "\n" );

    }
  }

#ifdef SANS_MPI
  } // if !partitioned
#endif

  // cell-to-node connectivity

  if (comm_rank == 0 || partitioned)
    for (int elem = 0; elem < nElem_global; elem++)
    {
      int offset = nRefnode*elem;
      for (int relem = 0; relem < nRefelem; relem++)
      {
        for (int n = 0; n < TopologyTrace::NNode; n++ )
          fprintf( fp, "%d ", offset + refMesh.elems[relem][n] + 1 ); // +1 for one based indexing
        fprintf( fp, "\n" );
      }
    }
}

//----------------------------------------------------------------------------//
template<class T, class PhysDim, class TopoDim, class Topology>
void
output_Tecplot_TraceGroup( const int group,
                           const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Topology>& xfldTrace,
                           const typename  Field<PhysDim, TopoDim, T>::template FieldTraceGroupType<Topology>& qfldTrace,
                           FILE* fp )
{
  typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Topology> XFieldTraceGroupType;
  typedef typename  Field<PhysDim, TopoDim, T>::template FieldTraceGroupType<Topology> QFieldTraceGroupType;

  typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldClass;
  typedef typename QFieldTraceGroupType::template ElementType<> ElementQFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfldTrace.basis() );
  ElementQFieldClass qfldElem( qfldTrace.basis() );

  static const int N = DLA::VectorSize<T>::M;

  const int order = max(xfldElem.order(), qfldElem.order());
  int nRefine = max(order-1,0);
  ReferenceElementMesh<Topology> refMesh(nRefine);
  const int nRefnode = refMesh.nodes.size();
  const int nRefelem = refMesh.elems.size();

  SANS_ASSERT(xfldTrace.nElem() == qfldTrace.nElem());

  int nelem = xfldTrace.nElem();

  // tecplot does not like empty groups
  if ( nelem == 0 ) return;

  fprintf( fp, "ZONE T=\"Boundary TraceElem %d\", N=%d, E=%d, F=FEPOINT, ET=%s\n", group, nRefnode*nelem, nRefelem*nelem, TecTopo<Topology>::name() );

  typename ElementQFieldClass::RefCoordType sRefTrace;
  typename ElementXFieldClass::VectorX X;

  T q;
  for (int elem = 0; elem < nelem; elem++)
  {
    xfldTrace.getElement( xfldElem, elem );
    qfldTrace.getElement( qfldElem, elem );

    for ( int i = 0; i < nRefnode; i++ )
    {
      sRefTrace = refMesh.nodes[i];

      xfldElem.eval( sRefTrace, X );
      qfldElem.eval( sRefTrace, q );

      for (int k = 0; k < PhysDim::D; k++)
        fprintf( fp, "%22.15e ", X[k] );
      for (int k = 0; k < N; k++)
        fprintf( fp, "%22.15e ", DLA::index(q,k) );
      fprintf( fp, "\n" );

    }
  }

  // cell-to-node connectivity

  for (int elem = 0; elem < nelem; elem++)
  {
    int offset = nRefnode*elem;
    for (int relem = 0; relem < nRefelem; relem++)
    {
      for (int n = 0; n < Topology::NNode; n++ )
        fprintf( fp, "%d ", offset + refMesh.elems[relem][n] + 1 ); // +1 for one based indexing
      fprintf( fp, "\n" );
    }
  }
}

} //namespace SANS
