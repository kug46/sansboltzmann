// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "Field/output_Tecplot.h"

#include "Field/XFieldArea.h"
#include "Field/FieldArea.h"

namespace SANS
{


//----------------------------------------------------------------------------//
// output as P1
//
void
output_Tecplot_CellGroup( const XField<PhysD3,TopoD2>::template FieldCellGroupType<Triangle>& xfld, FILE* fp )
{

  int nnode = xfld.nDOF();
  int nelem = xfld.nElem();
  fprintf( fp, "ZONE T=\"grid\", N=%d, E=%d, F=FEPOINT, ET=TRIANGLE\n", nnode, nelem );

  // grid coordinates

  Real x, y, z;
  for (int n = 0; n < nnode; n++)
  {
    x = xfld.DOF(n)[0];
    y = xfld.DOF(n)[1];
    z = xfld.DOF(n)[2];
    fprintf( fp, "%22.15e %22.15e %22.15e\n", x, y, z );
  }

  // cell-to-node connectivity

  int nodeMap[3];
  for (int elem = 0; elem < nelem; elem++)
  {
    xfld.associativity( elem ).getNodeGlobalMapping( nodeMap, 3 );
    fprintf( fp, "%d %d %d\n", nodeMap[0]+1, nodeMap[1]+1, nodeMap[2]+1 );
  }

}


//----------------------------------------------------------------------------//
// output as P1
//
void
output_Tecplot_TraceGroup( const std::string& type, const int group, const XField<PhysD3,TopoD2>::template FieldTraceGroupType<Line>& xfld, FILE* fp )
{

  int nnode = xfld.nDOF();
  int nelem = xfld.nElem();
  fprintf( fp, "ZONE T=\"%s %d\", N=%d, E=%d, F=FEPOINT, ET=LINESEG\n", type.c_str(), group, nnode, nelem );

  // grid coordinates

  Real x, y, z;
  for (int n = 0; n < nnode; n++)
  {
    x = xfld.DOF(n)[0];
    y = xfld.DOF(n)[1];
    z = xfld.DOF(n)[2];
    fprintf( fp, "%22.15e %22.15e %22.15e\n", x, y, z);
  }

  // cell-to-node connectivity

  int nodeMap[2];
  for (int elem = 0; elem < nelem; elem++)
  {
    xfld.associativity( elem ).getNodeGlobalMapping( nodeMap, 2 );
    fprintf( fp, "%d %d\n", nodeMap[0]+1, nodeMap[1]+1 );
  }

}


//----------------------------------------------------------------------------//
void
output_Tecplot( const XField<PhysD3,TopoD2>& xgrid, const std::string& filename )
{
  std::cout << "output_Tecplot: filename = " << filename << std::endl;
  FILE* fp = fopen( filename.c_str(), "w" );

  fprintf( fp, "\"\"\n" );
  fprintf( fp, "VARIABLES = \"X\", \"Y\", \"Z\"\n" );

  const int ngroup = xgrid.nCellGroups();

  // loop over element groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over elements of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      output_Tecplot_CellGroup( xgrid.getCellGroup<Triangle>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD3,TopoD2>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  // loop over interior trace groups (skip the first one because it should mostly duplicate the entire grid)
  for (int group = 1; group < xgrid.nInteriorTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Line) )
    {
      output_Tecplot_TraceGroup("Interior", group, xgrid.getInteriorTraceGroup<Line>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD3,TopoD2>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  // loop over trace boundary groups
  for (int group = 0; group < xgrid.nBoundaryTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Line) )
    {
      output_Tecplot_TraceGroup("Boundary", group, xgrid.getBoundaryTraceGroup<Line>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD3,TopoD2>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  fclose( fp );
}


//----------------------------------------------------------------------------//
// output as P1
//
template<int N>
void
output_Tecplot_CellGroup( const typename XField<PhysD3, TopoD2>::template FieldCellGroupType<Triangle>& xfld,
                          const typename Field< PhysD3, TopoD2, DLA::VectorS<N,Real> >::template FieldCellGroupType<Triangle>& qfld,
                          FILE* fp )
{
  typedef typename XField<PhysD3, TopoD2>::template FieldCellGroupType<Triangle> XFieldCellGroupType;
  typedef typename Field<PhysD3, TopoD2, DLA::VectorS<N,Real> >::template FieldCellGroupType<Triangle> QFieldCellGroupType;

  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfld.basis() );
  ElementQFieldClass qfldElem( qfld.basis() );

  int nelem = xfld.nElem();
  int nnode = 3*nelem;
  fprintf( fp, "ZONE T=\"grid\", N=%d, E=%d, F=FEPOINT, ET=TRIANGLE\n", nnode, nelem );

  // grid coordinates and solution

  Real s, t;
  ElementXFieldClass::VectorX X;
  DLA::VectorS<N,Real> q;
  // loop over elements within group
  for (int elem = 0; elem < nelem; elem++)
  {
    // copy global grid/solution DOFs to element
    xfld.getElement( xfldElem, elem );
    qfld.getElement( qfldElem, elem );

    s = 0;  t = 0;
    xfldElem.eval( s, t, X );
    qfldElem.eval( s, t, q );
    //qExact = (*solnExact)( x, y );

    fprintf( fp, "%22.15e %22.15e %22.15e", X[0], X[1], X[2] );
    for (int k = 0; k < N; k++)
      fprintf( fp, " %22.15e", q(k) );
//    for (int k = 0; k < N; k++)
//      fprintf( fp, " %22.15e", qExact(k) );
//    for (int k = 0; k < N; k++)
//      fprintf( fp, " %22.15e", q(k) - qExact(k) );
    fprintf( fp, "\n" );

    s = 1;  t = 0;
    xfldElem.eval( s, t, X );
    qfldElem.eval( s, t, q );
    //qExact = (*solnExact)( x, y );

    fprintf( fp, "%22.15e %22.15e %22.15e", X[0], X[1], X[2] );
    for (int k = 0; k < N; k++)
      fprintf( fp, " %22.15e", q(k) );
//    for (int k = 0; k < nvar; k++)
//      fprintf( fp, " %22.15e", qExact(k) );
//    for (int k = 0; k < nvar; k++)
//      fprintf( fp, " %22.15e", q(k) - qExact(k) );
    fprintf( fp, "\n" );

    s = 0;  t = 1;
    xfldElem.eval( s, t, X );
    qfldElem.eval( s, t, q );
    //qExact = (*solnExact)( x, y );

    fprintf( fp, "%22.15e %22.15e %22.15e", X[0], X[1], X[2] );
    for (int k = 0; k < N; k++)
      fprintf( fp, " %22.15e", q(k) );
//    for (int k = 0; k < nvar; k++)
//      fprintf( fp, " %22.15e", qExact(k) );
//    for (int k = 0; k < nvar; k++)
//      fprintf( fp, " %22.15e", q(k) - qExact(k) );
    fprintf( fp, "\n" );
  }

  // cell-to-node connectivity

  for (int elem = 0; elem < nelem; elem++)
  {
    fprintf( fp, "%d %d %d\n", 3*elem+1, 3*elem+2, 3*elem+3 );
  }


}

//----------------------------------------------------------------------------//
// output as P1
//
template<int N>
void
output_Tecplot_TraceGroup( const int group,
                           const typename XField<PhysD3,TopoD2>::template FieldTraceGroupType<Line>& xfldTrace,
                           const typename Field< PhysD3, TopoD2, DLA::VectorS<N,Real> >::template FieldTraceGroupType<Line>& qfldTrace,
                           FILE* fp )
{
  typedef typename XField<PhysD3, TopoD2>::template FieldTraceGroupType<Line> XFieldTraceGroupType;
  typedef typename Field<PhysD3, TopoD2, DLA::VectorS<N,Real> >::template FieldTraceGroupType<Line> QFieldTraceGroupType;

  typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldClass;
  typedef typename QFieldTraceGroupType::template ElementType<> ElementQFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfldTrace.basis() );
  ElementQFieldClass qfldElem( qfldTrace.basis() );

  SANS_ASSERT(xfldTrace.nElem() == qfldTrace.nElem());

  int nelem = xfldTrace.nElem();
  int nnode = 2*nelem;
  fprintf( fp, "ZONE T=\"Boundary %d\", N=%d, E=%d, F=FEPOINT, ET=LINESEG\n", group, nnode, nelem );

  Real s;
  ElementXFieldClass::VectorX X;
  DLA::VectorS<N,Real> q;

  for (int elem = 0; elem < nelem; elem++)
  {
    xfldTrace.getElement( xfldElem, elem );
    qfldTrace.getElement( qfldElem, elem );
#if 0
    std::cout << "output_Tecplot: qfldElem[" << elem << "] = " << std::endl;
    qfldElem.dump(2);
#endif

    s = 0;
    xfldElem.eval( s, X );
    qfldElem.eval( s, q );
    //qExact = (*solnExact)( x, y );

    fprintf( fp, "%22.15e %22.15e %22.15e", X[0], X[1], X[2] );
    for (int k = 0; k < N; k++)
      fprintf( fp, " %22.15e", q(k) );
//    for (int k = 0; k < nvar; k++)
//      fprintf( fp, " %22.15e", qExact(k) );
//    for (int k = 0; k < nvar; k++)
//      fprintf( fp, " %22.15e", q(k) - qExact(k) );
    fprintf( fp, "\n" );

    s = 1;
    xfldElem.eval( s, X );
    qfldElem.eval( s, q );
//    qExact = (*solnExact)( x, y );

    fprintf( fp, "%22.15e %22.15e %22.15e", X[0], X[1], X[2] );
    for (int k = 0; k < N; k++)
      fprintf( fp, " %22.15e", q(k) );
//    for (int k = 0; k < nvar; k++)
//      fprintf( fp, " %22.15e", qExact(k) );
//    for (int k = 0; k < nvar; k++)
//      fprintf( fp, " %22.15e", q(k) - qExact(k) );
    fprintf( fp, "\n" );
  }

  // cell-to-node connectivity

  for (int elem = 0; elem < nelem; elem++)
  {
    fprintf( fp, "%d %d\n", 2*elem+1, 2*elem+2 );
  }


}

//----------------------------------------------------------------------------//
template<int N>
void
output_Tecplot( const Field< PhysD3, TopoD2, DLA::VectorS<N,Real> >& qfld, const std::string& filename )
{
  std::cout << "output_Tecplot: filename = " << filename << std::endl;
  FILE* fp = fopen( filename.c_str(), "w" );

  fprintf( fp, "\"\"\n" );
  fprintf( fp, "VARIABLES = \"X\", \"Y\", \"Z\"" );

  for (int k = 0; k < N; k++)
    fprintf( fp, ", \"q%d\"", k+1 );
  fprintf( fp, "\n" );

  const XField<PhysD3,TopoD2>& xgrid = qfld.getXField();

  const int ngroup = qfld.nCellGroups();

  // loop over cell groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      output_Tecplot_CellGroup<N>( xgrid.template getCellGroup<Triangle>(group),
                                    qfld.template getCellGroup<Triangle>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD3,TopoD2>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  // loop over trace boundary groups
  for (int group = 0; group < qfld.nBoundaryTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Line) )
    {
      output_Tecplot_TraceGroup<N>( group, xgrid.template getBoundaryTraceGroup<Line>(group),
                                            qfld.template getBoundaryTraceGroup<Line>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Tecplot(XField<PhysD3,TopoD2>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  fclose( fp );
}

//Explicitly instantiate the function
template void
output_Tecplot( const Field< PhysD3, TopoD2, DLA::VectorS<1,Real> >& Q, const std::string& filename );


} //namespace SANS
