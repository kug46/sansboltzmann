// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(OUTPUT_TECPLOT_METRIC_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "Field/output_Tecplot.h"
#include "TecTopo.h"

#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/Eigen.h"

#include "Field/XField.h"

#include "Field/Field.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"
#include "MPI/continuousElementMap.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/gather.hpp>
#include <boost/version.hpp>

// handles serialization of arrays, depending on version of boost
#include "MPI/boost_serialization_array.h"
#endif

namespace SANS
{

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim, class Topology>
void
output_Tecplot_Cell_Connectivity(
    const XField<PhysDim,TopoDim>& xfld,
    const int cellGroupGlobal,
    const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
    FILE* fp )
{
  SANS_ASSERT_MSG( xfldCellGroup.order() == 1, "xfldCellGroup.order() = %d", xfldCellGroup.order() );

  // number of elements on the local processor
  int nElem = xfldCellGroup.nElem();

#ifdef SANS_MPI
  // construct a continuous element ID map for the elements in the current group
  const std::vector<int>& cellIDs = xfld.cellIDs(cellGroupGlobal);
  std::map<int,int> globalElemMap;
  continuousElementMap( *xfld.comm(), cellIDs, xfldCellGroup, nElem, globalElemMap);

  std::array<int,Topology::NNode> nodes;

  int comm_size = xfld.comm()->size();
  int comm_rank = xfld.comm()->rank();
  int nElemLocal = xfldCellGroup.nElem();

  // maximum element chunk size that rank 0 will write at any given time
  int Elemchunk = nElem / comm_size + nElem % comm_size;

  // send one chunk of elements at a time to rank 0
  for (int rank = 0; rank < comm_size; rank++)
  {
    int elemLow  =  rank   *Elemchunk;
    int elemHigh = (rank+1)*Elemchunk;

    std::map<int,std::array<int,Topology::NNode>> buffer;

    for (int elem = 0; elem < nElemLocal; elem++)
    {
      int elemID = globalElemMap[cellIDs[elem]];

      if (elemID >= elemLow && elemID < elemHigh &&
          xfldCellGroup.associativity(elem).rank() == comm_rank)
      {
        xfldCellGroup.associativity(elem).getGlobalMapping(nodes.data(), nodes.size());

        // transform back to native DOF indexing
        for (std::size_t i = 0; i < nodes.size(); i++)
          nodes[i] = xfld.local2nativeDOFmap(nodes[i]);

        buffer[cellIDs[elem]] = nodes;
      }
    }

    if (comm_rank == 0)
    {
      std::vector<std::map<int,std::array<int,Topology::NNode>>> bufferOnRank;
      boost::mpi::gather(*xfld.comm(), buffer, bufferOnRank, 0 );

      // collapse down the buffer from all other ranks
      for (std::size_t i = 1; i < bufferOnRank.size(); i++)
        buffer.insert(bufferOnRank[i].begin(), bufferOnRank[i].end());

      //Write the node data
      for ( const auto& nodesPair : buffer)
      {
        for (int n = 0; n < Topology::NNode; n++ )
          fprintf( fp, "%d ", nodesPair.second[n]+1 );
        fprintf( fp, "\n" );
      }
    }
    else // send the buffer to rank 0
      boost::mpi::gather(*xfld.comm(), buffer, 0 );
  } // for rank

#else
  int nodes[Topology::NNode];

  // loop over cells within group
  for (int elem = 0; elem < nElem; elem++)
  {
    xfldCellGroup.associativity(elem).getGlobalMapping(nodes, Topology::NNode);

    for (int n = 0; n < Topology::NNode; n++ )
      fprintf( fp, "%d ", nodes[n]+1 );
    fprintf( fp, "\n" );
  }
#endif
}

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim, class Topology>
void
output_Tecplot_CellGroup_Nodes(
    const XField<PhysDim,TopoDim>& xfld,
    const int cellGroupGlobal,
    const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
    FILE* fp )
{
  int comm_rank = xfld.comm()->rank();

  int nDOFnative = xfld.nDOFnative();
  int nElem = xfldCellGroup.nElem();

#ifdef SANS_MPI
  // construct a continuous element ID map for the elements in the current group
  const std::vector<int>& cellIDs = xfld.cellIDs(cellGroupGlobal);
  std::map<int,int> globalElemMap;
  continuousElementMap( *xfld.comm(), cellIDs, xfldCellGroup, nElem, globalElemMap);
#endif

  // tecplot does not like empty groups
  if ( nElem == 0 ) return;

  if (comm_rank == 0)
    fprintf( fp, "ZONE T=\"grid\", N=%d, E=%d, F=FEBLOCK, ET=%s", nDOFnative, nElem, TecTopo<Topology>::name() );

  // grid coordinates

#ifdef SANS_MPI
  int comm_size = xfld.comm()->size();

  // maximum DOF chunk size that rank 0 will write at any given time
  int DOFchunk = nDOFnative / comm_size + nDOFnative % comm_size;

  for (int d = 0; d < PhysDim::D; d++)
  {
    for (int rank = 0; rank < comm_size; rank++)
    {
      int DOFlow = rank*DOFchunk;
      int DOFhigh = (rank+1)*DOFchunk;

      std::map<int,Real> buffer;

      for (int i = 0; i < xfld.nDOF(); i++)
      {
        int iDOF = xfld.local2nativeDOFmap(i);
        if (iDOF >= DOFlow && iDOF < DOFhigh)
          buffer[iDOF] = xfld.DOF(i)[d];
      }

      if (comm_rank == 0)
      {
        std::vector<std::map<int,Real>> bufferOnRank;
        boost::mpi::gather(*xfld.comm(), buffer, bufferOnRank, 0 );

        // collapse down the buffer from all ranks (this sorts and remove any possible duplicates)
        for (std::size_t i = 0; i < bufferOnRank.size(); i++)
          buffer.insert(bufferOnRank[i].begin(), bufferOnRank[i].end());

        //Write the node data
        int n = 0;
        for ( const auto& DOFpair : buffer)
        {
          if (n++ % 5 == 0) fprintf( fp, "\n");
          fprintf( fp, "%22.15e ", DOFpair.second );
        }
      }
      else // send the buffer to rank 0
        boost::mpi::gather(*xfld.comm(), buffer, 0 );
    } // for rank
  } // PhysDim

#else
  for (int d = 0; d < PhysDim::D; d++)
  {
    for (int n = 0; n < nDOFnative; n++)
    {
      if (n % 5 == 0) fprintf( fp, "\n");
      fprintf( fp, "%22.15e ", xfld.DOF(n)[d] );
    }
    fprintf( fp, "\n");
  }
#endif

  // cell-to-node connectivity
  output_Tecplot_Cell_Connectivity<PhysDim, TopoDim, Topology>(xfld, cellGroupGlobal, xfldCellGroup, fp );
}

//----------------------------------------------------------------------------//
// output as P1
//
template<class PhysDim, class TopoDim, class Topology>
void
output_Tecplot_CellGroup(
    const XField<PhysDim,TopoDim>& xfld,
    const int cellGroupGlobal,
    const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
    FILE* fp )
{
  int nDOFnative = xfld.nDOFnative();
  int nElem = xfldCellGroup.nElem();

#ifdef SANS_MPI
  // construct a continuous element ID map for the elements in the current group
  const std::vector<int>& cellIDs = xfld.cellIDs(cellGroupGlobal);
  std::map<int,int> globalElemMap;
  continuousElementMap( *xfld.comm(), cellIDs, xfldCellGroup, nElem, globalElemMap);
#endif

  // tecplot does not like empty groups
  if ( nElem == 0 ) return;

  fprintf( fp, "ZONE T=\"grid\", N=%d, E=%d, F=FEBLOCK, ET=%s", nDOFnative, nElem, TecTopo<Topology>::name() );

  fprintf( fp, ", VARSHARELIST=([");
  for (int d = 0; d < PhysDim::D-1; d++)
    fprintf( fp, "%d,", d+1);
  fprintf( fp, "%d]=2)\n", PhysDim::D);

  // cell-to-node connectivity
  output_Tecplot_Cell_Connectivity<PhysDim, TopoDim, Topology>(xfld, cellGroupGlobal, xfldCellGroup, fp );
}


//----------------------------------------------------------------------------//
// output as P1
//
template<class PhysDim, class TopoDim, class Topology>
void
output_Tecplot_TraceGroup(
    const XField<PhysDim,TopoDim>& xfld,
    const std::string& type, const int boundaryTraceGroupGlobal,
    const typename XField<PhysDim,TopoDim>::template FieldTraceGroupType<Topology>& xfldBTrace,
    FILE* fp )
{
  int comm_rank = xfld.comm()->rank();

  int nDOFnative = xfld.nDOFnative();
  int nElem = xfldBTrace.nElem();

#ifdef SANS_MPI
  const std::vector<int>& boundaryTraceIDs = xfld.boundaryTraceIDs(boundaryTraceGroupGlobal);
  std::map<int,int> globalElemMap;
  continuousElementMap( *xfld.comm(), boundaryTraceIDs, xfldBTrace, nElem, globalElemMap);
#endif

  // tecplot does not like empty groups
  if ( nElem == 0 ) return;

  if (comm_rank == 0)
  {
    fprintf( fp, "ZONE T=\"%s %d\", N=%d, E=%d, F=FEBLOCK, ET=%s",
             type.c_str(), boundaryTraceGroupGlobal, nDOFnative, nElem, TecTopo<Topology>::name() );

    fprintf( fp, ", VARSHARELIST=([");
    for (int d = 0; d < PhysDim::D-1; d++)
      fprintf( fp, "%d,", d+1);
    fprintf( fp, "%d]=2)\n", PhysDim::D);
  }

  // cell-to-node connectivity

#ifdef SANS_MPI
  int comm_size = xfld.comm()->size();

  // number of elements on the local processor
  int nElemLocal = xfldBTrace.nElem();

  // maximum element chunk size that rank 0 will write at any given time
  int Elemchunk = nElem / comm_size + nElem % comm_size;

  std::array<int,Topology::NNode> nodes;

  // send one chunk of elements at a time to rank 0
  for (int rank = 0; rank < comm_size; rank++)
  {
    int elemLow  =  rank   *Elemchunk;
    int elemHigh = (rank+1)*Elemchunk;

    std::map<int,std::array<int,Topology::NNode>> buffer;

    for (int elem = 0; elem < nElemLocal; elem++)
    {
      int elemID = globalElemMap[boundaryTraceIDs[elem]];

      if (elemID >= elemLow && elemID < elemHigh &&
          xfldBTrace.associativity(elem).rank() == comm_rank)
      {
        // grm format only wants linear component of boundary elements
        xfldBTrace.associativity(elem).getNodeGlobalMapping(nodes.data(), Topology::NNode);

        // transform back to native DOF indexing and increment the indices because they start at 1
        for (int i = 0; i < Topology::NNode; i++)
          nodes[i] = xfld.local2nativeDOFmap(nodes[i]);

        buffer[boundaryTraceIDs[elem]] = nodes;
      }
    }

    if (comm_rank == 0)
    {
      std::vector<std::map<int,std::array<int,Topology::NNode>>> bufferOnRank;
      boost::mpi::gather(*xfld.comm(), buffer, bufferOnRank, 0 );

      // collapse down the buffer from all other ranks
      for (std::size_t i = 1; i < bufferOnRank.size(); i++)
        buffer.insert(bufferOnRank[i].begin(), bufferOnRank[i].end());

      //Write the node data
      for ( const auto& nodesPair : buffer)
      {
        for (int n = 0; n < Topology::NNode; n++)
          fprintf( fp, "%d ", nodesPair.second[n]+1 );
        fprintf( fp, "\n" );
      }
    }
    else // send the buffer to rank 0
      boost::mpi::gather(*xfld.comm(), buffer, 0 );
  } // for rank
#else
  // loop over traces within group
  for (int elem = 0; elem < nElem; elem++)
  {
    // grm format only wants linear component of boundary elements
    int nodes[Topology::NNode];
    xfldBTrace.associativity(elem).getNodeGlobalMapping(nodes, Topology::NNode);

    for (int n = 0; n < Topology::NNode; n++ )
      fprintf( fp, "%d ", nodes[n]+1 );
    fprintf( fp, "\n" );
  }
#endif
}

} //namespace SANS
