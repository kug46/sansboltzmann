// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define FIELD_DG_BOUNDARYTRACEBASE_INSTANTIATE
#include "Field_DG_BoundaryTraceBase_impl.h"

#define FIELDAREA_DG_BOUNDARYTRACE_INSTANTIATE
#include "FieldArea_DG_BoundaryTrace_impl.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "XFieldArea.h"

#include "UserVariables/BoltzmannNVar.h"
namespace SANS
{

template class Field_DG_BoundaryTrace< PhysD2, TopoD2, Real >;

template class Field_DG_BoundaryTrace< PhysD2, TopoD2, DLA::VectorS<1, Real> >;
template class Field_DG_BoundaryTrace< PhysD2, TopoD2, DLA::VectorS<2, Real> >;
template class Field_DG_BoundaryTrace< PhysD2, TopoD2, DLA::VectorS<3, Real> >;
template class Field_DG_BoundaryTrace< PhysD2, TopoD2, DLA::VectorS<4, Real> >;
template class Field_DG_BoundaryTrace< PhysD2, TopoD2, DLA::VectorS<5, Real> >;
template class Field_DG_BoundaryTrace< PhysD2, TopoD2, DLA::VectorS<6, Real> >;

// For Boltzmann implementation
template class Field_DG_BoundaryTrace< PhysD2, TopoD2, DLA::VectorS<9, Real> >;
//template class Field_DG_BoundaryTrace< PhysD2, TopoD2, DLA::VectorS<13, Real> >;
//template class Field_DG_BoundaryTrace< PhysD2, TopoD2, DLA::VectorS<16, Real> >;
template class Field_DG_BoundaryTrace< PhysD2, TopoD2, DLA::VectorS<NVar, Real> >;

template class Field_DG_BoundaryTrace< PhysD2, TopoD2, DLA::VectorS<2, DLA::VectorS<2, Real> > >;

//template class Field_DG_BoundaryTrace< PhysD2, TopoD2, DLA::VectorS<2, DLA::VectorS<9, Real> > >;

}
