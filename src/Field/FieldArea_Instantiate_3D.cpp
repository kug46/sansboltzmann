// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define FIELDBASE_INSTANTIATE
#include "FieldBase_impl.h"

#define FIELDSUBGROUP_INSTANTIATE
#include "FieldSubGroup_impl.h"

#define FIELD_INSTANTIATE
#include "Field_impl.h"

#include "XFieldArea.h"
#include "FieldArea.h"

namespace SANS
{

// FieldSubGroup instantiation
template class FieldSubGroup<PhysD3, FieldTraits<TopoD2, Real> >;

// Field instantiation
template class Field<PhysD3, TopoD2, Real>;

//template class Field<PhysD3, TopoD2, DLA::VectorS<1, Real> >;

}
