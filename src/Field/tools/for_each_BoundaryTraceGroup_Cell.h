// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FOR_EACH_BOUNDARYTRACEGROUP_CELL_H
#define FOR_EACH_BOUNDARYTRACEGROUP_CELL_H

// Applies a function to each boundary cell neighboring a boundary trace group

#include <typeinfo>

#include "tools/SANSException.h"
#include "Topology/ElementTopology.h"
#include "Field/FieldTypes.h"
#include "GroupFunctorType.h"

namespace SANS
{

template <class TopologyTrace, class TopologyL,
          class FunctorBoundaryCell, class FieldClass>
void
for_each_BoundaryTraceGroup_Cell_apply(
     FunctorBoundaryCell& functor,
     const typename FieldClass::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
     const FieldClass& fldCell, const int boundaryGroupGlobal )
{
  const int groupL = xfldTrace.getGroupLeft();

  // Integrate over the trace group
  functor.template apply<TopologyTrace, TopologyL>(
      fldCell.template getCellGroup<TopologyL>(groupL), groupL,
      xfldTrace, boundaryGroupGlobal );
}


//----------------------------------------------------------------------------//
template<class TopDim>
class for_each_BoundaryTraceGroup_Cell;


template<>
class for_each_BoundaryTraceGroup_Cell<TopoD1>
{
public:
  // typedef TopoD1 TopoDim;

protected:
  template <class TopologyTrace, class FunctorBoundaryCell, class FieldClass>
  static void
  LeftTopology(
      FunctorBoundaryCell& functor,
      const typename FieldClass::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const FieldClass& fldCell, const int boundaryGroupGlobal )
  {

    // determine topology for Left cell group
    const int groupL = xfldTrace.getGroupLeft();

    if ( fldCell.getCellGroupBaseGlobal(groupL).topoTypeID() == typeid(Line) )
      for_each_BoundaryTraceGroup_Cell_apply<TopologyTrace, Line>( functor, xfldTrace, fldCell, boundaryGroupGlobal );
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
  }

public:
  template <class FunctorBoundaryCell, class FieldClass>
  static void
  apply( GroupFunctorBoundaryCellType<FunctorBoundaryCell>&& functorType,
         const FieldType<FieldClass>& fldType )
  {
    FunctorBoundaryCell& functor = functorType.cast();
    const FieldClass& fldCell = fldType.cast();

    const typename FieldClass::XFieldType& xfld = fldCell.getXField();

    // loop over trace element groups
    for (std::size_t group = 0; group < functor.nBoundaryGroups(); group++)
    {
      const int boundaryGroupGlobal = functor.boundaryGroup(group);

      if ( xfld.getBoundaryTraceGroupBaseGlobal(boundaryGroupGlobal).topoTypeID() == typeid(Node) )
      {
        LeftTopology<Node>( functor,
            xfld.template getBoundaryTraceGroupGlobal<Node>(boundaryGroupGlobal),
            fldCell, boundaryGroupGlobal );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown trace topology" );
    }
  }
};


template<>
class for_each_BoundaryTraceGroup_Cell<TopoD2>
{
public:
  // typedef TopoD2 TopoDim;

protected:
  template <class TopologyTrace, class FunctorBoundaryCell, class FieldClass>
  static void
  LeftTopology(
      FunctorBoundaryCell& functor,
      const typename FieldClass::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const FieldClass& fldCell, const int boundaryGroupGlobal )
  {

    // determine topology for Left cell group
    const int groupL = xfldTrace.getGroupLeft();

    if ( fldCell.getCellGroupBaseGlobal(groupL).topoTypeID() == typeid(Triangle) )
    {
      // determine topology for R
      for_each_BoundaryTraceGroup_Cell_apply<TopologyTrace, Triangle>(
          functor, xfldTrace, fldCell, boundaryGroupGlobal );
    }
    else if ( fldCell.getCellGroupBaseGlobal(groupL).topoTypeID() == typeid(Quad) )
    {
      // determine topology for R
      for_each_BoundaryTraceGroup_Cell_apply<TopologyTrace, Quad>(
          functor, xfldTrace, fldCell, boundaryGroupGlobal );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
  }

public:
  template <class FunctorBoundaryCell, class FieldClass>
  static void
  apply( GroupFunctorBoundaryCellType<FunctorBoundaryCell>&& functorType,
         const FieldType<FieldClass>& fldType )
  {
    FunctorBoundaryCell& functor = functorType.cast();
    const FieldClass& fld = fldType.cast();

    const typename FieldClass::XFieldType& xfld = fld.getXField();

    // loop over trace element groups
    for (std::size_t group = 0; group < functor.nBoundaryGroups(); group++)
    {
      const int boundaryGroupGlobal = functor.boundaryGroup(group);

      if ( xfld.getBoundaryTraceGroupBaseGlobal(boundaryGroupGlobal).topoTypeID() == typeid(Line) )
      {
        LeftTopology<Line>( functor,
            xfld.template getBoundaryTraceGroupGlobal<Line>(boundaryGroupGlobal),
            fld, boundaryGroupGlobal );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown trace topology" );
    }
  }
};


template<>
class for_each_BoundaryTraceGroup_Cell<TopoD3>
{
public:
  // typedef TopoD3 TopoDim;

protected:
  template <class FunctorBoundaryCell, class FieldClass>
  static void
  LeftTopology_Triangle(
      FunctorBoundaryCell& functor,
      const typename FieldClass::XFieldType::template FieldTraceGroupType<Triangle>& xfldTrace,
      const FieldClass& fldCell, const int boundaryGroupGlobal )
  {

    // determine topology for Left cell group
    const int groupL = xfldTrace.getGroupLeft();

    if ( fldCell.getCellGroupBaseGlobal(groupL).topoTypeID() == typeid(Tet) )
    {
      // determine topology for R
      for_each_BoundaryTraceGroup_Cell_apply<Triangle, Tet>(
          functor, xfldTrace, fldCell, boundaryGroupGlobal );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );

  }

  template <class FunctorBoundaryCell, class FieldClass>
  static void
  LeftTopology_Quad(
      FunctorBoundaryCell& functor,
      const typename FieldClass::XFieldType::template FieldTraceGroupType<Quad>& xfldTrace,
      const FieldClass& fldCell, const int boundaryGroupGlobal )
  {

    // determine topology for Left cell group
    const int groupL = xfldTrace.getGroupLeft();

    if ( fldCell.getCellGroupBaseGlobal(groupL).topoTypeID() == typeid(Hex) )
    {
      // determine topology for R
      for_each_BoundaryTraceGroup_Cell_apply<Quad, Hex>(
          functor, xfldTrace, fldCell, boundaryGroupGlobal );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );

  }

public:
  template <class FunctorBoundaryCell, class FieldClass>
  static void
  apply( GroupFunctorBoundaryCellType<FunctorBoundaryCell>&& functorType,
         const FieldType<FieldClass>& fldType )
  {
    FunctorBoundaryCell& functor = functorType.cast();
    const FieldClass& fldCell = fldType.cast();

    const typename FieldClass::XFieldType& xfld = fldCell.getXField();

    // loop over trace element groups
    for (std::size_t group = 0; group < functor.nBoundaryGroups(); group++)
    {
      const int boundaryGroupGloabl = functor.boundaryGroup(group);

      if ( xfld.getBoundaryTraceGroupBaseGlobal(boundaryGroupGloabl).topoTypeID() == typeid(Triangle) )
      {
        LeftTopology_Triangle<>( functor,
            xfld.template getBoundaryTraceGroupGlobal<Triangle>(boundaryGroupGloabl),
            fldCell, boundaryGroupGloabl );
      }
      else if ( xfld.getBoundaryTraceGroupBaseGlobal(boundaryGroupGloabl).topoTypeID() == typeid(Quad) )
      {
        LeftTopology_Quad<>( functor,
            xfld.template getBoundaryTraceGroupGlobal<Quad>(boundaryGroupGloabl),
            fldCell, boundaryGroupGloabl );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown trace topology" );
    }
  }
};

template<>
class for_each_BoundaryTraceGroup_Cell<TopoD4>
{
public:
  // typedef TopoD4 TopoDim;

protected:
  template <class FunctorBoundaryCell, class FieldClass>
  static void
  LeftTopology_Tet(
      FunctorBoundaryCell& functor,
      const typename FieldClass::XFieldType::template FieldTraceGroupType<Tet>& xfldTrace,
      const FieldClass& fldCell, const int boundaryGroupGlobal )
  {

    // determine topology for Left cell group
    const int groupL = xfldTrace.getGroupLeft();

    if ( fldCell.getCellGroupBaseGlobal(groupL).topoTypeID() == typeid(Pentatope) )
    {
      // determine topology for R
      for_each_BoundaryTraceGroup_Cell_apply<Tet, Pentatope>(
          functor, xfldTrace, fldCell, boundaryGroupGlobal );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );

  }

public:
  template <class FunctorBoundaryCell, class FieldClass>
  static void
  apply( GroupFunctorBoundaryCellType<FunctorBoundaryCell>&& functorType,
         const FieldType<FieldClass>& fldType )
  {
    FunctorBoundaryCell& functor = functorType.cast();
    const FieldClass& fldCell = fldType.cast();

    const typename FieldClass::XFieldType& xfld = fldCell.getXField();

    // loop over trace element groups
    for (std::size_t group = 0; group < functor.nBoundaryGroups(); group++)
    {
      const int boundaryGroupGlobal = functor.boundaryGroup(group);

      if ( xfld.getBoundaryTraceGroupBaseGlobal(boundaryGroupGlobal).topoTypeID() == typeid(Tet) )
      {
        LeftTopology_Tet<>( functor,
            xfld.template getBoundaryTraceGroupGlobal<Tet>(boundaryGroupGlobal),
            fldCell, boundaryGroupGlobal );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown trace topology" );
    }
  }
};

}

#endif  // FOR_EACH_BOUNDARYTRACEGROUP_CELL_H
