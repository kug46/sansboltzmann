// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php


#ifndef FINDGROUPS_H
#define FINDGROUPS_H

#include <vector>
#include <map>

#include "Field/Field.h"
#include "Field/XField.h"

namespace SANS
{

  // a helper function for finding the broken interior trace group
  template< class PhysDim, class TopoDim >
  inline std::vector<int> findInteriorTraceGroup( const XField<PhysDim,TopoDim>& xfld,
                                                  const int leftGroup, const int rightGroup )
  {
    for (int igroup = 0; igroup < xfld.nInteriorTraceGroups(); igroup++)
      if ( xfld.getInteriorTraceGroupBase(igroup).getGroupLeft()  == leftGroup
        && xfld.getInteriorTraceGroupBase(igroup).getGroupRight() == rightGroup )
        return {igroup};

    return {}; // there is no interior trace group from left to right group!
  }

  // a helper function for creating a boundary map that only connects to leftGroup
  template <class PhysDim, class TopoDim>
  inline std::map<std::string, std::vector<int>> findAttachedBoundaryTraceMap( const XField<PhysDim,TopoDim>& xfld,
                                                                               const std::map<std::string, std::vector<int>>& oldMap,
                                                                               const int leftGroup )
  {
    std::map<std::string, std::vector<int>> newMap;
    std::vector<int> groupTemp; // a temp vector for storing the groups after checked
    for (const auto& keyVal: oldMap ) // check the map entries
    {
      groupTemp.reserve(keyVal.second.size()); // so can use push_back efficiently
      for (const auto& group : keyVal.second ) // loop over the groups corresponding to this string
        if ( xfld.getBoundaryTraceGroupBase(group).getGroupLeft() == leftGroup ) // if it is
          groupTemp.push_back( group ); // if the group was attached to the left group, it stays

      // set they value of the new map to exclude cell group 0
      newMap.insert( std::pair<std::string,std::vector<int>>(keyVal.first,groupTemp) );

      groupTemp.clear(); // clear it ready for the next loop
    }
    return newMap;
  }


} //namespace SANS

#endif //AlgebraicEquationSet_Project_H
