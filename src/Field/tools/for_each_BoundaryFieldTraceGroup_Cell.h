// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FOR_EACH_BOUNDARYFIELDTRACEGROUP_CELL_H
#define FOR_EACH_BOUNDARYFIELDTRACEGROUP_CELL_H

// Applies a function to each boundary cell neighboring a boundary trace group

#include <typeinfo>

#include "tools/SANSException.h"
#include "Topology/ElementTopology.h"
#include "Field/FieldTypes.h"
#include "GroupFunctorType.h"

namespace SANS
{

template <class TopologyTrace, class TopologyL, class FieldTraceClass,
          class FunctorBoundaryTrace, class FieldCellClass >
void
for_each_BoundaryFieldTraceGroup_Cell_apply(
     FunctorBoundaryTrace&& functor,
     const typename FieldCellClass::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
     const FieldCellClass& fldCell,
     const typename FieldTraceClass::template FieldTraceGroupType<TopologyTrace>& fldTrace,
     const int boundaryGroup )
{
  const int groupL = xfldTrace.getGroupLeft();

  // Integrate over the trace group
  functor.template apply<TopologyTrace, TopologyL>(
    fldCell.template getCellGroup<TopologyL>(groupL),
      fldTrace, xfldTrace, boundaryGroup );
}


//----------------------------------------------------------------------------//
template<class TopoDim>
class for_each_BoundaryFieldTraceGroup_Cell;

// base class interface
template<>
class for_each_BoundaryFieldTraceGroup_Cell<TopoD1>
{
public:
  // typedef TopoD1 TopoDim;

protected:
  template< class TopologyTrace, class FieldTraceClass,
            class FunctorBoundaryTrace, class FieldCellClass >
  static void
  LeftTopology( FunctorBoundaryTrace&& functor,
                const typename FieldCellClass::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
                const FieldCellClass& fldCell,
                const typename FieldTraceClass::template FieldTraceGroupType<TopologyTrace>& fldTrace,
                const int boundaryGroup )
  {
    // determine topology for Left cell group
    const int groupL = xfldTrace.getGroupLeft();

    if ( fldCell.getCellGroupBase(groupL).topoTypeID() == typeid(Line) )
      for_each_BoundaryFieldTraceGroup_Cell_apply<TopologyTrace,Line,FieldTraceClass>(
          functor, xfldTrace, fldCell, fldTrace, boundaryGroup );
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
  }

public:
  template <class FunctorBoundaryTrace, class FieldCellClass, class FieldTraceClass >
  static void
  apply( GroupFunctorBoundaryTraceType<FunctorBoundaryTrace>&& functorType,
         const FieldType<FieldCellClass>& fldCellType,
         const FieldType<FieldTraceClass>& fldTraceType)
  {
    FunctorBoundaryTrace& functor = functorType.cast();
    const FieldCellClass& fldCell = fldCellType.cast();
    const FieldTraceClass& fldTrace = fldTraceType.cast();

    // extract XField for associativity information
    const typename FieldCellClass::XFieldType& xfld = fldCell.getXField();

    // loop over trace element groups
    for (int boundaryGroup = 0; boundaryGroup < (int)functor.nBoundaryTraceGroups(); boundaryGroup++)
    {
      if ( xfld.getBoundaryTraceGroupBase(functor.boundaryTraceGroup(boundaryGroup)).topoTypeID() == typeid(Node) )
      {
        LeftTopology<Node,FieldTraceClass>( functor,
            xfld.template getBoundaryTraceGroup<Node>(functor.boundaryTraceGroup(boundaryGroup)),
            fldCell,
            fldTrace.template getBoundaryTraceGroupGlobal<Node>(functor.boundaryTraceGroup(boundaryGroup)),
            functor.boundaryTraceGroup(boundaryGroup) );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown trace topology" );
    }
  }
};


template<>
class for_each_BoundaryFieldTraceGroup_Cell<TopoD2>
{
public:
  // typedef TopoD2 TopoDim;

protected:
  template< class TopologyTrace, class FieldTraceClass,
            class FunctorBoundaryTrace, class FieldCellClass >
  static void
  LeftTopology( FunctorBoundaryTrace&& functor,
                const typename FieldCellClass::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
                const FieldCellClass& fldCell,
                const typename FieldTraceClass::template FieldTraceGroupType<TopologyTrace>& fldTrace,
                const int boundaryGroup )
  {

    // determine topology for Left cell group
    const int groupL = xfldTrace.getGroupLeft();

    if ( fldCell.getCellGroupBase(groupL).topoTypeID() == typeid(Triangle) )
    {
      // determine topology for R
      for_each_BoundaryFieldTraceGroup_Cell_apply<TopologyTrace,Triangle,FieldTraceClass>(
          functor, xfldTrace, fldCell, fldTrace, boundaryGroup );
    }
    else if ( fldCell.getCellGroupBase(groupL).topoTypeID() == typeid(Quad) )
    {
      // determine topology for R
      for_each_BoundaryFieldTraceGroup_Cell_apply<TopologyTrace,Quad,FieldTraceClass>(
          functor, xfldTrace, fldCell, fldTrace, boundaryGroup );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
  }

public:
  template < class FunctorBoundaryTrace, class FieldCellClass, class FieldTraceClass >
  static void
  apply( GroupFunctorBoundaryTraceType<FunctorBoundaryTrace>&& functorType,
         const FieldType<FieldCellClass>& fldCellType,
         const FieldType<FieldTraceClass>& fldTraceType)
  {
    FunctorBoundaryTrace& functor = functorType.cast();
    const FieldCellClass& fldCell = fldCellType.cast();
    const FieldTraceClass& fldTrace = fldTraceType.cast();

    // extract XField for associativity information
    const typename FieldCellClass::XFieldType& xfld = fldCell.getXField();

    // loop over trace element groups
    for (int boundaryGroup = 0; boundaryGroup < (int)functor.nBoundaryTraceGroups(); boundaryGroup++)
    {
      if ( xfld.getBoundaryTraceGroupBase(functor.boundaryTraceGroup(boundaryGroup)).topoTypeID() == typeid(Line) )
      {
        LeftTopology<Line,FieldTraceClass>( functor,
            xfld.template getBoundaryTraceGroup<Line>(functor.boundaryTraceGroup(boundaryGroup)),
            fldCell,
            fldTrace.template getBoundaryTraceGroupGlobal<Line>(functor.boundaryTraceGroup(boundaryGroup)),
            functor.boundaryTraceGroup(boundaryGroup)  );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown trace topology" );
    }
  }
};


template<>
class for_each_BoundaryFieldTraceGroup_Cell<TopoD3>
{
public:
  // typedef TopoD3 TopoDim;

protected:
  template< class FieldTraceClass, class FunctorBoundaryTrace, class FieldCellClass >
  static void
  LeftTopology_Triangle(  FunctorBoundaryTrace&& functor,
                          const typename FieldCellClass::XFieldType::template FieldTraceGroupType<Triangle>& xfldTrace,
                          const FieldCellClass& fldCell,
                          const typename FieldTraceClass::template FieldTraceGroupType<Triangle>& fldTrace,
                          const int boundaryGroup )
  {

    // determine topology for Left cell group
    const int groupL = xfldTrace.getGroupLeft();

    if ( fldCell.getCellGroupBase(groupL).topoTypeID() == typeid(Tet) )
    {
      // determine topology for R
      for_each_BoundaryFieldTraceGroup_Cell_apply<Triangle,Tet,FieldTraceClass>(
        functor, xfldTrace, fldCell, fldTrace, boundaryGroup );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );

  }

  template< class FieldTraceClass, class FunctorBoundaryTrace, class FieldCellClass >
  static void
  LeftTopology_Quad(  FunctorBoundaryTrace&& functor,
                      const typename FieldCellClass::XFieldType::template FieldTraceGroupType<Quad>& xfldTrace,
                      const FieldCellClass& fldCell,
                      const typename FieldTraceClass::template FieldTraceGroupType<Quad>& fldTrace,
                      const int boundaryGroup )
  {

    // determine topology for Left cell group
    const int groupL = xfldTrace.getGroupLeft();

    if ( fldCell.getCellGroupBase(groupL).topoTypeID() == typeid(Hex) )
    {
      // determine topology for R
      for_each_BoundaryFieldTraceGroup_Cell_apply<Quad,Hex,FieldTraceClass>(
        functor, xfldTrace, fldCell, fldTrace, boundaryGroup );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );

  }

public:
  template < class FunctorBoundaryTrace, class FieldCellClass, class FieldTraceClass >
  static void
  apply( GroupFunctorBoundaryTraceType<FunctorBoundaryTrace>&& functorType,
         const FieldType<FieldCellClass>& fldCellType,
         const FieldType<FieldTraceClass>& fldTraceType)
  {
    FunctorBoundaryTrace& functor = functorType.cast();
    const FieldCellClass& fldCell = fldCellType.cast();
    const FieldTraceClass& fldTrace = fldTraceType.cast();

    // extract XField for associativity information
    const typename FieldCellClass::XFieldType& xfld = fldCell.getXField();

    // loop over trace element groups
    for (int boundaryGroup = 0; boundaryGroup < (int)functor.nBoundaryTraceGroups(); boundaryGroup++)
    {
      if ( xfld.getBoundaryTraceGroupBase(boundaryGroup).topoTypeID() == typeid(Triangle) )
      {
        LeftTopology_Triangle<FieldTraceClass>( functor,
            xfld.template getBoundaryTraceGroup<Triangle>(functor.boundaryTraceGroup(boundaryGroup)),
            fldCell,
            fldTrace.template getBoundaryTraceGroupGlobal<Triangle>(functor.boundaryTraceGroup(boundaryGroup)),
            functor.boundaryTraceGroup(boundaryGroup) );
      }
      else if ( xfld.getBoundaryTraceGroupBase(boundaryGroup).topoTypeID() == typeid(Quad) )
      {
        LeftTopology_Quad<FieldTraceClass>(  functor,
            xfld.template getBoundaryTraceGroup<Quad>(functor.boundaryTraceGroup(boundaryGroup)),
            fldCell,
            fldTrace.template getBoundaryTraceGroupGlobal<Quad>(functor.boundaryTraceGroup(boundaryGroup)),
            functor.boundaryTraceGroup(boundaryGroup) );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown trace topology" );
    }
  }
};

template<>
class for_each_BoundaryFieldTraceGroup_Cell<TopoD4>
{
public:
  // typedef TopoD4 TopoDim;

protected:
  template<class FieldTraceClass, class FunctorBoundaryTrace, class FieldCellClass>
  static void
  LeftTopology_Tet(FunctorBoundaryTrace &&functor,
                        const typename FieldCellClass::XFieldType::template FieldTraceGroupType<Tet> &xfldTrace,
                        const FieldCellClass &fldCell,
                        const typename FieldTraceClass::template FieldTraceGroupType<Tet> &fldTrace,
                        const int boundaryGroup)
  {

    // determine topology for left cell group
    const int groupL= xfldTrace.getGroupLeft();

    if (fldCell.getCellGroupBase(groupL).topoTypeID() == typeid(Pentatope))
    {
      // determine topology for R
      for_each_BoundaryFieldTraceGroup_Cell_apply<Tet, Pentatope, FieldTraceClass>(
          functor, xfldTrace, fldCell, fldTrace, boundaryGroup);
    }
    else
      SANS_DEVELOPER_EXCEPTION("unknown cell topology");

  }

  // BELOW I WROTE UP TESSERACT EQUIVALENT OF THE 3D ONE, NOT SURE WHY BUT I THOUGHT IT WAS A GOOD IDEA

//  template<class FieldTraceClass, class PhysDim, class FunctorBoundaryTrace, class FieldCellClass>
//  static void
//  LeftTopology_Tessaract(FunctorBoundaryTrace &&functor,
//                         const typename XField<PhysDim,TopoDim>::template FieldTraceGroupType<Tesseract> &xfldTrace,
//                         const FieldCellClass &fldCell,
//                         const typename FieldTraceClass::template FieldTraceGroupType<Tesseract> &fldTrace,
//                         const in boundaryGroup)
//  {
//
//    // determine topology for Left cell group
//    int groupL= xfldTrace.getGroupLeft();
//
//    if (fldCell.getCellGroupBase(groupL).topoTypeID() == typeid(Tesseract))
//    {
//      // determine topology for R
//      for_each_BoundaryFieldTraceGroup_Cell_apply<Hex, Tesseract, FieldTraceClass, PhysDim>(
//          functor, xfldTrace, fldCell, fldTrace, boundaryGroup);
//    }
//    else
//      SANS_DEVELOPER_EXCEPTION("unknown cell topology");
//
//  }

public:
  template<class FunctorBoundaryTrace, class FieldCellClass, class FieldTraceClass>
  static void
  apply(GroupFunctorBoundaryTraceType<FunctorBoundaryTrace> &&functorType,
        const FieldType<FieldCellClass> &fldCellType,
        const FieldType<FieldTraceClass> &fldTraceType)
  {
    FunctorBoundaryTrace &functor= functorType.cast();
    const FieldCellClass &fldCell= fldCellType.cast();
    const FieldTraceClass &fldTrace= fldTraceType.cast();

    // extract XField for associativity information
    const typename FieldCellClass::XFieldType& xfld= fldCell.getXField();

    // loop over trace element groups
    for (int boundaryGroup= 0; boundaryGroup < (int) functor.nBoundaryTraceGroups(); boundaryGroup++)
    {
      if (xfld.getBoundaryTraceGroupBase(boundaryGroup).topoTypeID() == typeid(Tet))
      {
        LeftTopology_Tet<FieldTraceClass>( functor,
                                           xfld.template getBoundaryTraceGroup<Tet>(functor.boundaryTraceGroup(boundaryGroup)),
                                           fldCell,
                                           fldTrace.template getBoundaryTraceGroupGlobal<Tet>(functor.boundaryTraceGroup(boundaryGroup)),
                                           functor.boundaryTraceGroup(boundaryGroup));
      }
      else
        SANS_DEVELOPER_EXCEPTION("unknown trace topology");
    }
  }

};

}

#endif  // FOR_EACH_BOUNDARYFIELDTRACEGROUP_CELL_H
