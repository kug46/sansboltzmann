// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// #ifndef GROUPELEMINDEX_H
// #define GROUPELEMINDEX_H

#include "GroupElemIndex.h"
#include <boost/functional/hash.hpp> // so GroupElemIndex can be used in unordered_map

namespace SANS
{

// a hashing function for GroupElemIndex. Allows for using it with boost::hash
// Just forwards on to the boost::hash for pairs of int. Quick, slightly dirty, but very stable
std::size_t hash_value( GroupElemIndex const& groupElem )
{
  boost::hash<std::pair<int,int>> hasher;
  return hasher(std::pair<int,int>(groupElem.group,groupElem.elem));
}

} // namespace SANS
