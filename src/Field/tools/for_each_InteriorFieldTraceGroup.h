// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FOR_EACH_INTERIORFIELDTRACEGROUP_H
#define FOR_EACH_INTERIORFIELDTRACEGROUP_H

// Applies a function to each interior trace group

#include <typeinfo>

#include "tools/SANSException.h"
#include "Topology/ElementTopology.h"
#include "Field/FieldTypes.h"
#include "GroupFunctorType.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template<class TopDim>
class for_each_InteriorFieldTraceGroup;

// base class interface
template<>
class for_each_InteriorFieldTraceGroup<TopoD1>
{
public:
  // typedef TopoD1 TopoDim;

  template <class FunctorInteriorTrace, class FieldClass>
  static void
  apply( GroupFunctorInteriorTraceType<FunctorInteriorTrace>&& functorType,
         const FieldType<FieldClass>& fldType )
  {
    FunctorInteriorTrace& functor = functorType.cast();
    const FieldClass& fld = fldType.cast();

    // loop over element groups
    for (std::size_t group = 0; group < functor.nInteriorTraceGroups(); group++)
    {
      const int interiorTraceGroupGlobal = functor.interiorTraceGroup(group);

      // dispatch integration over elements of group
      if ( fld.getInteriorTraceGroupBaseGlobal(interiorTraceGroupGlobal).topoTypeID() == typeid(Node) )
      {
        functor.template apply<Node>(
            fld.template getInteriorTraceGroupGlobal<Node>(interiorTraceGroupGlobal),
            interiorTraceGroupGlobal );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown interior trace topology\n" );

    }
  }
};


template<>
class for_each_InteriorFieldTraceGroup<TopoD2>
{
public:
  // typedef TopoD2 TopoDim;

  template <class FunctorInteriorTrace, class FieldClass>
  static void
  apply( GroupFunctorInteriorTraceType<FunctorInteriorTrace>&& functorType,
         const FieldType<FieldClass>& fldType )
  {
    FunctorInteriorTrace& functor = functorType.cast();
    const FieldClass& fld = fldType.cast();

    // loop over element groups
    for (std::size_t group = 0; group < functor.nInteriorTraceGroups(); group++)
    {
      const int interiorTraceGroupGlobal = functor.interiorTraceGroup(group);

      // dispatch integration over elements of group
      if ( fld.getInteriorTraceGroupBaseGlobal(interiorTraceGroupGlobal).topoTypeID() == typeid(Line) )
      {
        functor.template apply<Line>(
            fld.template getInteriorTraceGroupGlobal<Line>(interiorTraceGroupGlobal),
            interiorTraceGroupGlobal );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown interior trace topology\n" );

    }
  }
};


template<>
class for_each_InteriorFieldTraceGroup<TopoD3>
{
public:
  // typedef TopoD3 TopoDim;

  template <class FunctorInteriorTrace, class FieldClass>
  static void
  apply( GroupFunctorInteriorTraceType<FunctorInteriorTrace>&& functorType,
         const FieldType<FieldClass>& fldType )
  {
    FunctorInteriorTrace& functor = functorType.cast();
    const FieldClass& fld = fldType.cast();

    // loop over element groups
    for (std::size_t group = 0; group < functor.nInteriorTraceGroups(); group++)
    {
      const int interiorTraceGroupGlobal = functor.interiorTraceGroup(group);

      // dispatch integration over elements of group
      if ( fld.getInteriorTraceGroupBaseGlobal(interiorTraceGroupGlobal).topoTypeID() == typeid(Triangle) )
      {
        functor.template apply<Triangle>(
            fld.template getInteriorTraceGroupGlobal<Triangle>(interiorTraceGroupGlobal),
            interiorTraceGroupGlobal );
      }
      else if ( fld.getInteriorTraceGroupBaseGlobal(interiorTraceGroupGlobal).topoTypeID() == typeid(Quad) )
      {
        functor.template apply<Quad>(
            fld.template getInteriorTraceGroupGlobal<Quad>(interiorTraceGroupGlobal),
            interiorTraceGroupGlobal );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown interior trace topology\n" );

    }
  }
};

template<>
class for_each_InteriorFieldTraceGroup<TopoD4>
{
public:
  // typedef TopoD4 TopoDim;

  template <class FunctorInteriorTrace, class FieldClass>
  static void
  apply( GroupFunctorInteriorTraceType<FunctorInteriorTrace>&& functorType,
         const FieldType<FieldClass>& fldType )
  {
    FunctorInteriorTrace& functor = functorType.cast();
    const FieldClass& fld = fldType.cast();

    // loop over element groups
    for (std::size_t group = 0; group < functor.nInteriorTraceGroups(); group++)
    {
      const int interiorTraceGroupGlobal = functor.interiorTraceGroup(group);

      // dispatch integration over elements of group
      if ( fld.getInteriorTraceGroupBaseGlobal(interiorTraceGroupGlobal).topoTypeID() == typeid(Tet) )
      {
        functor.template apply<Tet>(
            fld.template getInteriorTraceGroupGlobal<Tet>(interiorTraceGroupGlobal),
            interiorTraceGroupGlobal );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown interior trace topology\n" );

    }
  }
};

}

#endif  // FOR_EACH_INTERIORFIELDTRACEGROUP_H
