// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FOR_EACH_HUBTRACEGROUP_H
#define FOR_EACH_HUBTRACEGROUP_H

// Applies a function to each hub trace group

#include <typeinfo>

#include "tools/SANSException.h"
#include "Topology/ElementTopology.h"
#include "Field/FieldTypes.h"
#include "GroupFunctorType.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template<class TopDim>
class for_each_HubTraceGroup;

// base class interface
template<>
class for_each_HubTraceGroup<TopoD1>
{
public:
  // typedef TopoD1 TopoDim;

  template <class FunctorHubTrace, class FieldClass>
  static void
  apply( GroupFunctorHubTraceType<FunctorHubTrace>&& functorType,
         const FieldType<FieldClass>& fldType )
  {
    FunctorHubTrace& functor = functorType.cast();
    const FieldClass& fld = fldType.cast();

    // loop over element groups
    for (std::size_t group = 0; group < functor.nHubTraceGroups(); group++)
    {
      const int hubTraceGroupGlobal = functor.hubTraceGroup(group);

      // dispatch integration over elements of group
      if ( fld.getHubTraceGroupBaseGlobal(hubTraceGroupGlobal).topoTypeID() == typeid(Node) )
      {
        functor.template apply<Node>(
            fld.template getHubTraceGroupGlobal<Node>(hubTraceGroupGlobal),
            hubTraceGroupGlobal );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown hub trace topology\n" );

    }
  }
};


template<>
class for_each_HubTraceGroup<TopoD2>
{
public:
  // typedef TopoD2 TopoDim;

  template <class FunctorHubTrace, class FieldClass>
  static void
  apply( GroupFunctorHubTraceType<FunctorHubTrace>&& functorType,
         const FieldType<FieldClass>& fldType )
  {
    FunctorHubTrace& functor = functorType.cast();
    const FieldClass& fld = fldType.cast();

    // loop over element groups
    for (std::size_t group = 0; group < functor.nHubTraceGroups(); group++)
    {
      const int hubTraceGroupGlobal = functor.hubTraceGroup(group);

      // dispatch integration over elements of group
      if ( fld.getHubTraceGroupBaseGlobal(hubTraceGroupGlobal).topoTypeID() == typeid(Line) )
      {
        functor.template apply<Line>(
            fld.template getHubTraceGroupGlobal<Line>(hubTraceGroupGlobal),
            hubTraceGroupGlobal );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown hub trace topology\n" );

    }
  }
};


template<>
class for_each_HubTraceGroup<TopoD3>
{
public:
  // typedef TopoD3 TopoDim;

  template <class FunctorHubTrace, class FieldClass>
  static void
  apply( GroupFunctorHubTraceType<FunctorHubTrace>&& functorType,
         const FieldType<FieldClass>& fldType )
  {
    FunctorHubTrace& functor = functorType.cast();
    const FieldClass& fld = fldType.cast();

    // loop over element groups
    for (std::size_t group = 0; group < functor.nHubTraceGroups(); group++)
    {
      const int hubTraceGroupGlobal = functor.hubTraceGroup(group);

      // dispatch integration over elements of group
      if ( fld.getHubTraceGroupBaseGlobal(hubTraceGroupGlobal).topoTypeID() == typeid(Triangle) )
      {
        functor.template apply<Triangle>(
            fld.template getHubTraceGroupGlobal<Triangle>(hubTraceGroupGlobal),
            hubTraceGroupGlobal );
      }
      else if ( fld.getHubTraceGroupBaseGlobal(hubTraceGroupGlobal).topoTypeID() == typeid(Quad) )
      {
        functor.template apply<Quad>(
            fld.template getHubTraceGroupGlobal<Quad>(hubTraceGroupGlobal),
            hubTraceGroupGlobal );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown hub trace topology\n" );

    }
  }
};

template<>
class for_each_HubTraceGroup<TopoD4>
{
public:
  // typedef TopoD4 TopoDim;

  template <class FunctorHubTrace, class FieldClass>
  static void
  apply( GroupFunctorHubTraceType<FunctorHubTrace>&& functorType,
         const FieldType<FieldClass>& fldType )
  {
    FunctorHubTrace& functor = functorType.cast();
    const FieldClass& fld = fldType.cast();

    // loop over element groups
    for (std::size_t group = 0; group < functor.nHubTraceGroups(); group++)
    {
      const int hubTraceGroupGlobal = functor.hubTraceGroup(group);

      // dispatch integration over elements of group
      if ( fld.getHubTraceGroupBaseGlobal(hubTraceGroupGlobal).topoTypeID() == typeid(Tet) )
      {
        functor.template apply<Tet>(
            fld.template getHubTraceGroupGlobal<Tet>(hubTraceGroupGlobal),
            hubTraceGroupGlobal );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown hub trace topology\n" );

    }
  }
};

}

#endif  // FOR_EACH_HUBTRACEGROUP_H
