// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FOR_EACH_CELLGROUP_H
#define FOR_EACH_CELLGROUP_H

// Applies a function to each cell group

#include <typeinfo>

#include "tools/SANSException.h"
#include "Topology/ElementTopology.h"
#include "Field/FieldTypes.h"
#include "GroupFunctorType.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template<class TopDim>
class for_each_CellGroup;

// base class interface
template<>
class for_each_CellGroup<TopoD1>
{
public:
  // typedef TopoD1 TopoDim;

  template <class FunctorCell, class FieldClass>
  static void
  apply( GroupFunctorCellType<FunctorCell>&& functorType,
         const FieldType<FieldClass>& fldType )
  {
    FunctorCell& functor = functorType.cast();
    const FieldClass& fld = fldType.cast();

    // loop over element groups
    for (std::size_t group = 0; group < functor.nCellGroups(); group++)
    {
      const int cellGroupGlobal = functor.cellGroup(group);

      // dispatch integration over elements of group
      if ( fld.getCellGroupBaseGlobal(cellGroupGlobal).topoTypeID() == typeid(Line) )
      {
        functor.template apply<Line>(
            fld.template getCellGroupGlobal<Line>(cellGroupGlobal),
            cellGroupGlobal );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown cell topology\n" );

    }
  }
};


template<>
class for_each_CellGroup<TopoD2>
{
public:
  // typedef TopoD2 TopoDim;

  template <class FunctorCell, class FieldClass>
  static void
  apply( GroupFunctorCellType<FunctorCell>&& functorType,
         const FieldType<FieldClass>& fldType )
  {
    FunctorCell& functor = functorType.cast();
    const FieldClass& fld = fldType.cast();

    // loop over element groups
    for (std::size_t group = 0; group < functor.nCellGroups(); group++)
    {
      const int cellGroupGlobal = functor.cellGroup(group);

      // dispatch integration over elements of group
      if ( fld.getCellGroupBaseGlobal(cellGroupGlobal).topoTypeID() == typeid(Triangle) )
      {
        functor.template apply<Triangle>(
            fld.template getCellGroupGlobal<Triangle>(cellGroupGlobal),
            cellGroupGlobal );
      }
      else if ( fld.getCellGroupBaseGlobal(cellGroupGlobal).topoTypeID() == typeid(Quad) )
      {
        functor.template apply<Quad>(
            fld.template getCellGroupGlobal<Quad>(cellGroupGlobal),
            cellGroupGlobal );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown cell topology\n" );

    }
  }
};



template<>
class for_each_CellGroup<TopoD3>
{
public:
  // typedef TopoD3 TopoDim;

  template <class FunctorCell, class FieldClass>
  static void
  apply( GroupFunctorCellType<FunctorCell>&& functorType,
         const FieldType<FieldClass>& fldType )
  {
    FunctorCell& functor = functorType.cast();
    const FieldClass& fld = fldType.cast();

    // loop over element groups
    for (std::size_t group = 0; group < functor.nCellGroups(); group++)
    {
      const int cellGroupGlobal = functor.cellGroup(group);

      // dispatch integration over elements of group
      if ( fld.getCellGroupBaseGlobal(cellGroupGlobal).topoTypeID() == typeid(Tet) )
      {
        functor.template apply<Tet>(
            fld.template getCellGroupGlobal<Tet>(cellGroupGlobal),
            cellGroupGlobal );
      }
      else if ( fld.getCellGroupBaseGlobal(cellGroupGlobal).topoTypeID() == typeid(Hex) )
      {
        functor.template apply<Hex>(
            fld.template getCellGroupGlobal<Hex>(cellGroupGlobal),
            cellGroupGlobal );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown cell topology\n" );

    }
  }
};

template<>
class for_each_CellGroup<TopoD4>
{
public:
  // typedef TopoD4 TopoDim;

  template <class FunctorCell, class FieldClass>
  static void
  apply( GroupFunctorCellType<FunctorCell>&& functorType,
         const FieldType<FieldClass>& fldType )
  {
    FunctorCell& functor = functorType.cast();
    const FieldClass& fld = fldType.cast();

    // loop over element groups
    for (std::size_t group = 0; group < functor.nCellGroups(); group++)
    {
      const int cellGroupGlobal = functor.cellGroup(group);

      // dispatch integration over elements of group
      if ( fld.getCellGroupBaseGlobal(cellGroupGlobal).topoTypeID() == typeid(Pentatope) )
      {
        functor.template apply<Pentatope>(
            fld.template getCellGroupGlobal<Pentatope>(cellGroupGlobal),
            cellGroupGlobal );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown cell topology\n" );

    }
  }
};


}

#endif  // FOR_EACH_CELLGROUP_H
