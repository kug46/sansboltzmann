// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FOR_EACH_PERIODICFIELDTRACEGROUP_CELL_H
#define FOR_EACH_PERIODICFIELDTRACEGROUP_CELL_H

// Applies a function to each interior cell neighboring an interior trace group

#include <typeinfo>

#include "tools/SANSException.h"
#include "Topology/ElementTopology.h"
#include "Field/FieldTypes.h"
#include "GroupFunctorType.h"

namespace SANS
{

template <class TopologyTrace, class TopologyL, class TopologyR, class FieldTraceClass, class XFieldType,
          class FunctorInteriorTrace, class FieldCellClass >
void
for_each_PeriodicFieldTraceGroup_Cell_apply(
     FunctorInteriorTrace&& functor,
     const typename XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
     const FieldCellClass& fldCell,
     const typename FieldTraceClass::template FieldTraceGroupType<TopologyTrace>& fldTrace,
     const int periodicTraceGroupGlobal )
{
  const int groupL = xfldTrace.getGroupLeft();
  const int groupR = xfldTrace.getGroupRight();

  // Apply to cells adjacent to interior trace group
  functor.template apply<TopologyTrace, TopologyL, TopologyR>(
      fldCell.template getCellGroup<TopologyL>(groupL),
      fldCell.template getCellGroup<TopologyR>(groupR),
      fldTrace, xfldTrace, periodicTraceGroupGlobal );
}


//----------------------------------------------------------------------------//
template<class TopDim>
class for_each_PeriodicFieldTraceGroup_Cell;

// base class interface
template<>
class for_each_PeriodicFieldTraceGroup_Cell<TopoD1>
{
public:
  typedef TopoD1 TopoDim;
protected:

  template< class TopologyTrace, class TopologyL, class FieldTraceClass, class XFieldType,
            class FunctorInteriorTrace, class FieldCellClass>
  static void
  RightTopology(FunctorInteriorTrace&& functor,
                const typename XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
                const FieldCellClass& fldCell,
                const typename FieldTraceClass::template FieldTraceGroupType<TopologyTrace>& fldTrace,
                const int periodicTraceGroupGlobal )
  {
    // determine topology for Right cell group
    const int groupR = xfldTrace.getGroupRight();

    // dispatch operation over elements of groupR
    if ( fldCell.getCellGroupBase(groupR).topoTypeID() == typeid(Line) )
    {
      for_each_PeriodicFieldTraceGroup_Cell_apply<TopologyTrace,TopologyL,Line,FieldTraceClass,XFieldType>(
        functor, xfldTrace, fldCell, fldTrace, periodicTraceGroupGlobal );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );

  }


  template< class TopologyTrace, class FieldTraceClass, class XFieldType,
            class FunctorInteriorTrace, class FieldCellClass >
  static void
  LeftTopology( FunctorInteriorTrace&& functor,
                const typename XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
                const FieldCellClass& fldCell,
                const typename FieldTraceClass::template FieldTraceGroupType<TopologyTrace>& fldTrace,
                const int periodicTraceGroupGlobal)
  {
    // determine topology for Left Cell group
    const int groupL = xfldTrace.getGroupLeft();

    if ( fldCell.getCellGroupBase(groupL).topoTypeID() == typeid(Line) )
    {
      // determine topology for R
      RightTopology<TopologyTrace, Line, FieldTraceClass, XFieldType>(
          functor, xfldTrace, fldCell, fldTrace, periodicTraceGroupGlobal );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );

  }

public:
  template <class FunctorInteriorTrace, class FieldCellClass, class FieldTraceClass>
  static void
  apply( GroupFunctorInteriorTraceType<FunctorInteriorTrace>&& functorType,
         const FieldType<FieldCellClass>& fldCellType,
         const FieldType<FieldTraceClass>& fldTraceType )
  {
    typedef typename FieldCellClass::XFieldType XFieldType;
    FunctorInteriorTrace& functor = functorType.cast();
    const FieldCellClass& fldCell = fldCellType.cast();
    const FieldTraceClass& fldTrace = fldTraceType.cast();

    // extract Xfield for associativity information
    const XFieldType& xfld = fldCell.getXField();

    // loop over element groups
    for (std::size_t group = 0; group < functor.nPeriodicTraceGroups(); group++)
    {
      const int periodicTraceGroupGlobal = functor.periodicTraceGroup(group);

      // dispatch integration over elements of group
      if ( xfld.getBoundaryTraceGroupBaseGlobal(periodicTraceGroupGlobal).topoTypeID() == typeid(Node) )
      {
        LeftTopology<Node,FieldTraceClass,XFieldType>( functor,
            xfld.template getBoundaryTraceGroupGlobal<Node>(periodicTraceGroupGlobal),
            fldCell,
            fldTrace.template getBoundaryTraceGroupGlobal<Node>(periodicTraceGroupGlobal),
            periodicTraceGroupGlobal );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown interior trace topology\n" );

    }
  }
};


template<>
class for_each_PeriodicFieldTraceGroup_Cell<TopoD2>
{
public:
  typedef TopoD2 TopoDim;

protected:
  template< class TopologyTrace, class TopologyL, class FieldTraceClass, class XFieldType,
            class FunctorInteriorTrace, class FieldCellClass>
  static void
  RightTopology(FunctorInteriorTrace&& functor,
                const typename XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
                const FieldCellClass& fldCell,
                const typename FieldTraceClass::template FieldTraceGroupType<TopologyTrace>& fldTrace,
                const int periodicTraceGroupGlobal )
  {
    // determine topology for Right cell group
    const int groupR = xfldTrace.getGroupRight();

    // dispatch operation over elements of groupR
    if ( fldCell.getCellGroupBase(groupR).topoTypeID() == typeid(Triangle) )
    {
      for_each_PeriodicFieldTraceGroup_Cell_apply<TopologyTrace,TopologyL,Triangle,FieldTraceClass,XFieldType>(
        functor, xfldTrace, fldCell, fldTrace, periodicTraceGroupGlobal );
    }
    else if ( fldCell.getCellGroupBase(groupR).topoTypeID() == typeid(Quad) )
    {
      for_each_PeriodicFieldTraceGroup_Cell_apply<TopologyTrace,TopologyL,Quad,FieldTraceClass,XFieldType>(
        functor, xfldTrace, fldCell, fldTrace, periodicTraceGroupGlobal );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );

  }


  template< class TopologyTrace, class FieldTraceClass, class XFieldType,
            class FunctorInteriorTrace, class FieldCellClass >
  static void
  LeftTopology( FunctorInteriorTrace&& functor,
                const typename XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
                const FieldCellClass& fldCell,
                const typename FieldTraceClass::template FieldTraceGroupType<TopologyTrace>& fldTrace,
                const int periodicTraceGroupGlobal)
  {
    // determine topology for Left Cell group
    const int groupL = xfldTrace.getGroupLeft();

    if ( fldCell.getCellGroupBase(groupL).topoTypeID() == typeid(Triangle) )
    {
      // determine topology for R
      RightTopology<TopologyTrace, Triangle, FieldTraceClass, XFieldType>(
          functor, xfldTrace, fldCell, fldTrace, periodicTraceGroupGlobal );
    }
    else if ( fldCell.getCellGroupBase(groupL).topoTypeID() == typeid(Quad) )
    {
      // determine topology for R
      RightTopology<TopologyTrace, Quad, FieldTraceClass, XFieldType>(
          functor, xfldTrace, fldCell, fldTrace, periodicTraceGroupGlobal );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );

  }

public:
  template <class FunctorInteriorTrace, class FieldCellClass, class FieldTraceClass >
  static void
  apply( GroupFunctorInteriorTraceType<FunctorInteriorTrace>&& functorType,
         const FieldType<FieldCellClass>& fldCellType,
         const FieldType<FieldTraceClass>& fldTraceType )
  {
    typedef typename FieldCellClass::XFieldType XFieldType;
    FunctorInteriorTrace& functor = functorType.cast();
    const FieldCellClass& fldCell = fldCellType.cast();
    const FieldTraceClass& fldTrace = fldTraceType.cast();

    // extract Xfield for associativity information
    const XFieldType& xfld = fldCell.getXField();

    // loop over element groups
    for (std::size_t group = 0; group < functor.nPeriodicTraceGroups(); group++)
    {
      const int periodicTraceGroupGlobal = functor.periodicTraceGroup(group);

      // dispatch integration over elements of group
      if ( xfld.getBoundaryTraceGroupBaseGlobal(periodicTraceGroupGlobal).topoTypeID() == typeid(Line) )
      {
        LeftTopology<Line,FieldTraceClass,XFieldType>( functor,
            xfld.template getBoundaryTraceGroupGlobal<Line>(periodicTraceGroupGlobal),
            fldCell,
            fldTrace.template getBoundaryTraceGroupGlobal<Line>(periodicTraceGroupGlobal),
            periodicTraceGroupGlobal );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown interior trace topology\n" );
    }
  }
};


template<>
class for_each_PeriodicFieldTraceGroup_Cell<TopoD3>
{
public:
  typedef TopoD3 TopoDim;
protected:
  template< class TopologyL, class FieldTraceClass, class XFieldType,
            class FunctorInteriorTrace, class FieldCellClass>
  static void
  RightTopology_Quad( FunctorInteriorTrace& functor,
                      const typename XFieldType::template FieldTraceGroupType<Quad>& xfldTrace,
                      const FieldCellClass& fldCell,
                      const typename FieldTraceClass::template FieldTraceGroupType<Quad>& fldTrace,
                      const int periodicTraceGroupGlobal )
  {
    // determine topology for Right cell group
    const int groupR = xfldTrace.getGroupRight();

    // dispatch operation over elements of groupR
    if ( fldCell.getCellGroupBase(groupR).topoTypeID() == typeid(Hex) )
    {
      for_each_PeriodicFieldTraceGroup_Cell_apply<Quad,TopologyL,Hex,FieldTraceClass,XFieldType>(
        functor, xfldTrace, fldCell, fldTrace, periodicTraceGroupGlobal );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );

  }

  template< class TopologyL, class FieldTraceClass, class XFieldType,
            class FunctorInteriorTrace, class FieldCellClass>
  static void
  RightTopology_Triangle( FunctorInteriorTrace& functor,
                          const typename XFieldType::template FieldTraceGroupType<Triangle>& xfldTrace,
                          const FieldCellClass& fldCell,
                          const typename FieldTraceClass::template FieldTraceGroupType<Triangle>& fldTrace,
                          const int periodicTraceGroupGlobal )
  {
    // determine topology for Right cell group
    const int groupR = xfldTrace.getGroupRight();

    // dispatch operation over elements of groupR
    if ( fldCell.getCellGroupBase(groupR).topoTypeID() == typeid(Tet) )
    {
      for_each_PeriodicFieldTraceGroup_Cell_apply<Triangle,TopologyL,Tet,FieldTraceClass,XFieldType>(
        functor, xfldTrace, fldCell, fldTrace, periodicTraceGroupGlobal );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );

  }


  template< class FieldTraceClass, class XFieldType,
            class FunctorInteriorTrace, class FieldCellClass >
  static void
  LeftTopology_Quad( FunctorInteriorTrace& functor,
                     const typename XFieldType::template FieldTraceGroupType<Quad>& xfldTrace,
                     const FieldCellClass& fldCell,
                     const typename FieldTraceClass::template FieldTraceGroupType<Quad>& fldTrace,
                     const int periodicTraceGroupGlobal)
  {
    // determine topology for Left Cell group
    const int groupL = xfldTrace.getGroupLeft();

    if ( fldCell.getCellGroupBase(groupL).topoTypeID() == typeid(Hex) )
    {
      // determine topology for R
      RightTopology_Quad<Hex, FieldTraceClass, XFieldType>(
          functor, xfldTrace, fldCell, fldTrace, periodicTraceGroupGlobal );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
  }

  template< class FieldTraceClass, class XFieldType,
            class FunctorInteriorTrace, class FieldCellClass >
  static void
  LeftTopology_Triangle( FunctorInteriorTrace& functor,
                         const typename XFieldType::template FieldTraceGroupType<Triangle>& xfldTrace,
                         const FieldCellClass& fldCell,
                         const typename FieldTraceClass::template FieldTraceGroupType<Triangle>& fldTrace,
                         const int periodicTraceGroupGlobal)
  {
    // determine topology for Left Cell group
    const int groupL = xfldTrace.getGroupLeft();

    if ( fldCell.getCellGroupBase(groupL).topoTypeID() == typeid(Tet) )
    {
      // determine topology for R
      RightTopology_Triangle<Tet, FieldTraceClass, XFieldType>(
          functor, xfldTrace, fldCell, fldTrace, periodicTraceGroupGlobal );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
  }

public:
  template <class FunctorInteriorTrace, class FieldCellClass, class FieldTraceClass >
  static void
  apply( GroupFunctorInteriorTraceType<FunctorInteriorTrace>&& functorType,
         const FieldType<FieldCellClass>& fldCellType,
         const FieldType<FieldTraceClass>& fldTraceType )
  {
    typedef typename FieldCellClass::XFieldType XFieldType;
    FunctorInteriorTrace& functor = functorType.cast();
    const FieldCellClass& fldCell = fldCellType.cast();
    const FieldTraceClass& fldTrace = fldTraceType.cast();

    // extract Xfield for associativity information
    const XFieldType& xfld = fldCell.getXField();

    // loop over element groups
    for (std::size_t group = 0; group < functor.nPeriodicTraceGroups(); group++)
    {
      const int periodicTraceGroupGlobal = functor.periodicTraceGroup(group);

      // dispatch integration over elements of group
      if ( xfld.getBoundaryTraceGroupBaseGlobal(periodicTraceGroupGlobal).topoTypeID() == typeid(Triangle) )
      {
        LeftTopology_Triangle<FieldTraceClass,XFieldType>( functor,
            xfld.template getBoundaryTraceGroupGlobal<Triangle>(periodicTraceGroupGlobal),
            fldCell,
            fldTrace.template getBoundaryTraceGroupGlobal<Triangle>(periodicTraceGroupGlobal),
            periodicTraceGroupGlobal );
      }
      else if ( xfld.getBoundaryTraceGroupBaseGlobal(periodicTraceGroupGlobal).topoTypeID() == typeid(Quad) )
      {
        LeftTopology_Quad<FieldTraceClass,XFieldType>( functor,
            xfld.template getBoundaryTraceGroupGlobal<Quad>(periodicTraceGroupGlobal),
            fldCell,
            fldTrace.template getBoundaryTraceGroupGlobal<Quad>(periodicTraceGroupGlobal),
            periodicTraceGroupGlobal );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown interior trace topology\n" );
    }
  }
};

template<>
class for_each_PeriodicFieldTraceGroup_Cell<TopoD4>
{
public:
  typedef TopoD4 TopoDim;
protected:

  template< class TopologyL, class FieldTraceClass, class XFieldType,
            class FunctorInteriorTrace, class FieldCellClass>
  static void
  RightTopology_Tet( FunctorInteriorTrace& functor,
                     const typename XFieldType::template FieldTraceGroupType<Tet>& xfldTrace,
                     const FieldCellClass& fldCell,
                     const typename FieldTraceClass::template FieldTraceGroupType<Tet>& fldTrace,
                     const int periodicTraceGroupGlobal )
  {
    // determine topology for Right cell group
    const int groupR = xfldTrace.getGroupRight();

    // dispatch operation over elements of groupR
    if ( fldCell.getCellGroupBase(groupR).topoTypeID() == typeid(Pentatope) )
    {
      for_each_PeriodicFieldTraceGroup_Cell_apply<Tet,TopologyL,Pentatope,FieldTraceClass,XFieldType>(
        functor, xfldTrace, fldCell, fldTrace, periodicTraceGroupGlobal );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );

  }

  template< class FieldTraceClass, class XFieldType,
            class FunctorInteriorTrace, class FieldCellClass >
  static void
  LeftTopology_Tet( FunctorInteriorTrace& functor,
                    const typename XFieldType::template FieldTraceGroupType<Tet>& xfldTrace,
                    const FieldCellClass& fldCell,
                    const typename FieldTraceClass::template FieldTraceGroupType<Tet>& fldTrace,
                    const int periodicTraceGroupGlobal)
  {
    // determine topology for Left Cell group
    const int groupL = xfldTrace.getGroupLeft();

    if ( fldCell.getCellGroupBase(groupL).topoTypeID() == typeid(Pentatope) )
    {
      // determine topology for R
      RightTopology_Tet<Pentatope, FieldTraceClass, XFieldType>(
          functor, xfldTrace, fldCell, fldTrace, periodicTraceGroupGlobal );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
  }

public:
  template <class FunctorInteriorTrace, class FieldCellClass, class FieldTraceClass >
  static void
  apply( GroupFunctorInteriorTraceType<FunctorInteriorTrace>&& functorType,
         const FieldType<FieldCellClass>& fldCellType,
         const FieldType<FieldTraceClass>& fldTraceType )
  {
    typedef typename FieldCellClass::XFieldType XFieldType;
    FunctorInteriorTrace& functor = functorType.cast();
    const FieldCellClass& fldCell = fldCellType.cast();
    const FieldTraceClass& fldTrace = fldTraceType.cast();

    // extract Xfield for associativity information
    const XFieldType& xfld = fldCell.getXField();

    // loop over element groups
    for (std::size_t group = 0; group < functor.nPeriodicTraceGroups(); group++)
    {
      const int periodicTraceGroupGlobal = functor.periodicTraceGroup(group);

      // dispatch integration over elements of group
      if ( xfld.getBoundaryTraceGroupBaseGlobal(periodicTraceGroupGlobal).topoTypeID() == typeid(Tet) )
      {
        LeftTopology_Tet<FieldTraceClass,XFieldType>( functor,
            xfld.template getBoundaryTraceGroupGlobal<Tet>(periodicTraceGroupGlobal),
            fldCell,
            fldTrace.template getBoundaryTraceGroupGlobal<Tet>(periodicTraceGroupGlobal),
            periodicTraceGroupGlobal );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown interior trace topology\n" );
    }
  }
};

}

#endif  // FOR_EACH_PERIODICFIELDTRACEGROUP_CELL_H
