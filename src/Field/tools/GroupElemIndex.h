// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef GROUPELEMINDEX_H
#define GROUPELEMINDEX_H

#include <ostream>

namespace SANS
{

struct GroupElemIndex
{
  GroupElemIndex() : group(-1), elem(-1) {}
  GroupElemIndex(const int group, const int elem) : group(group), elem(elem) {}

  int group;
  int elem;

  // boiler plate stuff for the STL
  // lexicographic sort
  bool operator<(const GroupElemIndex& idx) const
  {
    if ( group < idx.group ) return true;
    if ( group == idx.group && elem  < idx.elem  ) return true;
    return false;
  }
  // syntactically consistent > operator
  bool operator>(const GroupElemIndex& idx) const { return !operator<(idx); }

  // Also allows for hashing when combined with below (also for sort algorithms)
  bool operator==(const GroupElemIndex& idx) const
  {
    if (group == idx.group && elem == idx.elem ) return true;
    return false;
  }
  bool operator!=(const GroupElemIndex& idx) const {return !operator==(idx); }

  // for debugging
  friend std::ostream &operator<<(std::ostream&os, const GroupElemIndex& idx )
  {
    os << "(group, elem) = (" << idx.group << ", "  << idx.elem << ")";
    return os;
  }
};

std::size_t hash_value( GroupElemIndex const& groupElem );

} // namespace SANS

#endif //GROUPELEMINDEX_H
