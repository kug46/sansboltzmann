// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SURREALIZE_H
#define SURREALIZE_H

#include "Surreal/SurrealS_Type.h"

namespace SANS
{

// Forward declare
namespace DLA
{
template<int M, class T>
class VectorS;
}

//----------------------------------------------------------------------------//
// A class for changing an element type to Surreal when requested
//----------------------------------------------------------------------------//
template<class T, class Tin>
struct Surrealize;

template<class Tin>
struct Surrealize<Real, Tin> { typedef Tin T; };

template<int NS>
struct Surrealize<SurrealS<NS>, Real>{ typedef SurrealS<NS> T; };

template<int NS, int NV>
struct Surrealize<SurrealS<NS>, DLA::VectorS<NV, Real> >
{
  typedef DLA::VectorS<NV, SurrealS<NS> > T;
};

template<int NS, int N1, int N2>
struct Surrealize<SurrealS<NS>, DLA::VectorS<N1, DLA::VectorS<N2, Real> > >
{
  typedef DLA::VectorS<N1, DLA::VectorS<N2, SurrealS<NS> > > T;
};

}

#endif // SURREALIZE_H
