// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FOR_EACH_INTERIORFIELDTRACEGROUP_CELL_H
#define FOR_EACH_INTERIORFIELDTRACEGROUP_CELL_H

// Applies a function to each interior cell neighboring an interior trace group

#include <typeinfo>

#include "tools/SANSException.h"
#include "Topology/ElementTopology.h"
#include "Field/FieldTypes.h"
#include "GroupFunctorType.h"

namespace SANS
{

template <class TopologyTrace, class TopologyL, class TopologyR, class FieldTraceClass, class XFieldType,
          class FunctorInteriorTrace, class FieldCellClass >
void
for_each_InteriorFieldTraceGroup_Cell_apply(
     FunctorInteriorTrace&& functor,
     const typename XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
     const FieldCellClass& fldCell,
     const typename FieldTraceClass::template FieldTraceGroupType<TopologyTrace>& fldTrace,
     const int interiorTraceGroupGlobal )
{
  int groupL = xfldTrace.getGroupLeft();
  int groupR = xfldTrace.getGroupRight();

  // Apply to cells adjacent to interior trace group
  functor.template apply<TopologyTrace, TopologyL, TopologyR>(
      fldCell.template getCellGroup<TopologyL>(groupL),
      fldCell.template getCellGroup<TopologyR>(groupR),
      fldTrace, xfldTrace, interiorTraceGroupGlobal );
}


//----------------------------------------------------------------------------//
template<class TopoDim>
class for_each_InteriorFieldTraceGroup_Cell;

// base class interface
template<>
class for_each_InteriorFieldTraceGroup_Cell<TopoD1>
{
public:
  // typedef TopoD1 TopoDim;
protected:

  template< class TopologyTrace, class TopologyL, class FieldTraceClass, class XFieldType,
            class FunctorInteriorTrace, class FieldCellClass>
  static void
  RightTopology(FunctorInteriorTrace&& functor,
                const typename XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
                const FieldCellClass& fldCell,
                const typename FieldTraceClass::template FieldTraceGroupType<TopologyTrace>& fldTrace,
                const int interiorTraceGroupGlobal )
  {
    // determine topology for Right cell group
    int groupR = xfldTrace.getGroupRight();

    // dispatch operation over elements of groupR
    if ( fldCell.getCellGroupBase(groupR).topoTypeID() == typeid(Line) )
    {
      for_each_InteriorFieldTraceGroup_Cell_apply<TopologyTrace,TopologyL,Line,FieldTraceClass,XFieldType>(
        functor, xfldTrace, fldCell, fldTrace, interiorTraceGroupGlobal );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );

  }


  template< class TopologyTrace, class FieldTraceClass, class XFieldType,
            class FunctorInteriorTrace, class FieldCellClass >
  static void
  LeftTopology( FunctorInteriorTrace&& functor,
                const typename XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
                const FieldCellClass& fldCell,
                const typename FieldTraceClass::template FieldTraceGroupType<TopologyTrace>& fldTrace,
                const int interiorTraceGroupGlobal)
  {
    // determine topology for Left Cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( fldCell.getCellGroupBase(groupL).topoTypeID() == typeid(Line) )
    {
      // determine topology for R
      RightTopology<TopologyTrace,Line,FieldTraceClass,XFieldType>(
          functor, xfldTrace, fldCell, fldTrace, interiorTraceGroupGlobal );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );

  }

public:
  template <class FunctorInteriorTrace, class FieldCellClass, class FieldTraceClass>
  static void
  apply( GroupFunctorInteriorTraceType<FunctorInteriorTrace>&& functorType,
         const FieldType<FieldCellClass>& fldCellType,
         const FieldType<FieldTraceClass>& fldTraceType )
  {
    typedef typename FieldCellClass::XFieldType XFieldType;
    FunctorInteriorTrace& functor = functorType.cast();
    const FieldCellClass& fldCell = fldCellType.cast();
    const FieldTraceClass& fldTrace = fldTraceType.cast();

    // extract Xfield for associativity information
    const XFieldType& xfld = fldCell.getXField();

    // loop over element groups
    for (std::size_t group = 0; group < functor.nInteriorTraceGroups(); group++)
    {
      const int interiorTraceGroupGlobal = functor.interiorTraceGroup(group);

      // dispatch integration over elements of group
      if ( xfld.getInteriorTraceGroupBaseGlobal(interiorTraceGroupGlobal).topoTypeID() == typeid(Node) )
      {
        LeftTopology<Node,FieldTraceClass,XFieldType>( functor,
            xfld.template getInteriorTraceGroupGlobal<Node>(interiorTraceGroupGlobal),
            fldCell,
            fldTrace.template getInteriorTraceGroupGlobal<Node>(interiorTraceGroupGlobal),
            interiorTraceGroupGlobal );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown interior trace topology\n" );

    }
  }
};


template<>
class for_each_InteriorFieldTraceGroup_Cell<TopoD2>
{
public:
  // typedef TopoD2 TopoDim;

protected:
  template< class TopologyTrace, class TopologyL, class FieldTraceClass, class XFieldType,
            class FunctorInteriorTrace, class FieldCellClass>
  static void
  RightTopology(FunctorInteriorTrace&& functor,
                const typename XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
                const FieldCellClass& fldCell,
                const typename FieldTraceClass::template FieldTraceGroupType<TopologyTrace>& fldTrace,
                const int interiorTraceGroupGlobal )
  {
    // determine topology for Right cell group
    int groupR = xfldTrace.getGroupRight();

    // dispatch operation over elements of groupR
    if ( fldCell.getCellGroupBase(groupR).topoTypeID() == typeid(Triangle) )
    {
      for_each_InteriorFieldTraceGroup_Cell_apply<TopologyTrace,TopologyL,Triangle,FieldTraceClass,XFieldType>(
        functor, xfldTrace, fldCell, fldTrace, interiorTraceGroupGlobal );
    }
    else if ( fldCell.getCellGroupBase(groupR).topoTypeID() == typeid(Quad) )
    {
      for_each_InteriorFieldTraceGroup_Cell_apply<TopologyTrace,TopologyL,Quad,FieldTraceClass,XFieldType>(
        functor, xfldTrace, fldCell, fldTrace, interiorTraceGroupGlobal );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );

  }


  template< class TopologyTrace, class FieldTraceClass, class XFieldType,
            class FunctorInteriorTrace, class FieldCellClass >
  static void
  LeftTopology( FunctorInteriorTrace&& functor,
                const typename XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
                const FieldCellClass& fldCell,
                const typename FieldTraceClass::template FieldTraceGroupType<TopologyTrace>& fldTrace,
                const int interiorTraceGroupGlobal)
  {
    // determine topology for Left Cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( fldCell.getCellGroupBase(groupL).topoTypeID() == typeid(Triangle) )
    {
      // determine topology for R
      RightTopology<TopologyTrace, Triangle, FieldTraceClass, XFieldType>(
          functor, xfldTrace, fldCell, fldTrace, interiorTraceGroupGlobal );
    }
    else if ( fldCell.getCellGroupBase(groupL).topoTypeID() == typeid(Quad) )
    {
      // determine topology for R
      RightTopology<TopologyTrace, Quad, FieldTraceClass, XFieldType>(
          functor, xfldTrace, fldCell, fldTrace, interiorTraceGroupGlobal );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );

  }

public:
  template <class FunctorInteriorTrace, class FieldCellClass, class FieldTraceClass >
  static void
  apply( GroupFunctorInteriorTraceType<FunctorInteriorTrace>&& functorType,
         const FieldType<FieldCellClass>& fldCellType,
         const FieldType<FieldTraceClass>& fldTraceType )
  {
    typedef typename FieldCellClass::XFieldType XFieldType;
    FunctorInteriorTrace& functor = functorType.cast();
    const FieldCellClass& fldCell = fldCellType.cast();
    const FieldTraceClass& fldTrace = fldTraceType.cast();

    // extract Xfield for associativity information
    const XFieldType& xfld = fldCell.getXField();

    // loop over element groups
    for (std::size_t group = 0; group < functor.nInteriorTraceGroups(); group++)
    {
      const int interiorTraceGroupGlobal = functor.interiorTraceGroup(group);

      // dispatch integration over elements of group
      if ( xfld.getInteriorTraceGroupBaseGlobal(interiorTraceGroupGlobal).topoTypeID() == typeid(Line) )
      {
        LeftTopology<Line,FieldTraceClass,XFieldType>( functor,
            xfld.template getInteriorTraceGroupGlobal<Line>(interiorTraceGroupGlobal),
            fldCell,
            fldTrace.template getInteriorTraceGroupGlobal<Line>(interiorTraceGroupGlobal),
            interiorTraceGroupGlobal );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown interior trace topology\n" );
    }
  }
};


template<>
class for_each_InteriorFieldTraceGroup_Cell<TopoD3>
{
public:
  // typedef TopoD3 TopoDim;
protected:
  template< class TopologyL, class FieldTraceClass, class XFieldType,
            class FunctorInteriorTrace, class FieldCellClass>
  static void
  RightTopology_Quad( FunctorInteriorTrace& functor,
                      const typename XFieldType::template FieldTraceGroupType<Quad>& xfldTrace,
                      const FieldCellClass& fldCell,
                      const typename FieldTraceClass::template FieldTraceGroupType<Quad>& fldTrace,
                      const int interiorTraceGroupGlobal )
  {
    // determine topology for Right cell group
    int groupR = xfldTrace.getGroupRight();

    // dispatch operation over elements of groupR
    if ( fldCell.getCellGroupBase(groupR).topoTypeID() == typeid(Hex) )
    {
      for_each_InteriorFieldTraceGroup_Cell_apply<Quad,TopologyL,Hex,FieldTraceClass,XFieldType>(
        functor, xfldTrace, fldCell, fldTrace, interiorTraceGroupGlobal );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );

  }

  template< class TopologyL, class FieldTraceClass, class XFieldType,
            class FunctorInteriorTrace, class FieldCellClass>
  static void
  RightTopology_Triangle( FunctorInteriorTrace& functor,
                          const typename XFieldType::template FieldTraceGroupType<Triangle>& xfldTrace,
                          const FieldCellClass& fldCell,
                          const typename FieldTraceClass::template FieldTraceGroupType<Triangle>& fldTrace,
                          const int interiorTraceGroupGlobal )
  {
    // determine topology for Right cell group
    int groupR = xfldTrace.getGroupRight();

    // dispatch operation over elements of groupR
    if ( fldCell.getCellGroupBase(groupR).topoTypeID() == typeid(Tet) )
    {
      for_each_InteriorFieldTraceGroup_Cell_apply<Triangle,TopologyL,Tet,FieldTraceClass,XFieldType>(
        functor, xfldTrace, fldCell, fldTrace, interiorTraceGroupGlobal );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );

  }


  template< class FieldTraceClass, class XFieldType,
            class FunctorInteriorTrace, class FieldCellClass >
  static void
  LeftTopology_Quad( FunctorInteriorTrace& functor,
                     const typename XFieldType::template FieldTraceGroupType<Quad>& xfldTrace,
                     const FieldCellClass& fldCell,
                     const typename FieldTraceClass::template FieldTraceGroupType<Quad>& fldTrace,
                     const int interiorTraceGroupGlobal)
  {
    // determine topology for Left Cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( fldCell.getCellGroupBase(groupL).topoTypeID() == typeid(Hex) )
    {
      // determine topology for R
      RightTopology_Quad<Hex, FieldTraceClass, XFieldType>(
          functor, xfldTrace, fldCell, fldTrace, interiorTraceGroupGlobal );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
  }

  template< class FieldTraceClass, class XFieldType,
            class FunctorInteriorTrace, class FieldCellClass >
  static void
  LeftTopology_Triangle( FunctorInteriorTrace& functor,
                         const typename XFieldType::template FieldTraceGroupType<Triangle>& xfldTrace,
                         const FieldCellClass& fldCell,
                         const typename FieldTraceClass::template FieldTraceGroupType<Triangle>& fldTrace,
                         const int interiorTraceGroupGlobal)
  {
    // determine topology for Left Cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( fldCell.getCellGroupBase(groupL).topoTypeID() == typeid(Tet) )
    {
      // determine topology for R
      RightTopology_Triangle<Tet, FieldTraceClass, XFieldType>(
          functor, xfldTrace, fldCell, fldTrace, interiorTraceGroupGlobal );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
  }

public:
  template <class FunctorInteriorTrace, class FieldCellClass, class FieldTraceClass >
  static void
  apply( GroupFunctorInteriorTraceType<FunctorInteriorTrace>&& functorType,
         const FieldType<FieldCellClass>& fldCellType,
         const FieldType<FieldTraceClass>& fldTraceType )
  {
    typedef typename FieldCellClass::XFieldType XFieldType;
    FunctorInteriorTrace& functor = functorType.cast();
    const FieldCellClass& fldCell = fldCellType.cast();
    const FieldTraceClass& fldTrace = fldTraceType.cast();

    // extract Xfield for associativity information
    const XFieldType& xfld = fldCell.getXField();

    // loop over element groups
    for (std::size_t group = 0; group < functor.nInteriorTraceGroups(); group++)
    {
      const int interiorTraceGroupGlobal = functor.interiorTraceGroup(group);

      // dispatch integration over elements of group
      if ( xfld.getInteriorTraceGroupBaseGlobal(interiorTraceGroupGlobal).topoTypeID() == typeid(Triangle) )
      {
        LeftTopology_Triangle<FieldTraceClass,XFieldType>( functor,
            xfld.template getInteriorTraceGroupGlobal<Triangle>(interiorTraceGroupGlobal),
            fldCell,
            fldTrace.template getInteriorTraceGroupGlobal<Triangle>(interiorTraceGroupGlobal),
            interiorTraceGroupGlobal );
      }
      else if ( xfld.getInteriorTraceGroupBaseGlobal(interiorTraceGroupGlobal).topoTypeID() == typeid(Quad) )
      {
        LeftTopology_Quad<FieldTraceClass,XFieldType>( functor,
            xfld.template getInteriorTraceGroupGlobal<Quad>(interiorTraceGroupGlobal),
            fldCell,
            fldTrace.template getInteriorTraceGroupGlobal<Quad>(interiorTraceGroupGlobal),
            interiorTraceGroupGlobal );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown interior trace topology\n" );
    }
  }
};


template<>
class for_each_InteriorFieldTraceGroup_Cell<TopoD4>
{
public:
  // typedef TopoD4 TopoDim;
protected:

  template< class TopologyL, class FieldTraceClass, class XFieldType,
            class FunctorInteriorTrace, class FieldCellClass>
  static void
  RightTopology_Tet( FunctorInteriorTrace& functor,
                          const typename XFieldType::template FieldTraceGroupType<Tet>& xfldTrace,
                          const FieldCellClass& fldCell,
                          const typename FieldTraceClass::template FieldTraceGroupType<Tet>& fldTrace,
                          const int interiorTraceGroupGlobal )
  {
    // determine topology for Right cell group
    int groupR = xfldTrace.getGroupRight();

    // dispatch operation over elements of groupR
    if ( fldCell.getCellGroupBase(groupR).topoTypeID() == typeid(Pentatope) )
    {
      for_each_InteriorFieldTraceGroup_Cell_apply<Tet,TopologyL,Pentatope,FieldTraceClass,XFieldType>(
        functor, xfldTrace, fldCell, fldTrace, interiorTraceGroupGlobal );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );

  }

  template< class FieldTraceClass, class XFieldType,
            class FunctorInteriorTrace, class FieldCellClass >
  static void
  LeftTopology_Tet( FunctorInteriorTrace& functor,
                         const typename XFieldType::template FieldTraceGroupType<Tet>& xfldTrace,
                         const FieldCellClass& fldCell,
                         const typename FieldTraceClass::template FieldTraceGroupType<Tet>& fldTrace,
                         const int interiorTraceGroupGlobal)
  {
    // determine topology for Left Cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( fldCell.getCellGroupBase(groupL).topoTypeID() == typeid(Pentatope) )
    {
      // determine topology for R
      RightTopology_Tet<Pentatope, FieldTraceClass, XFieldType>(
          functor, xfldTrace, fldCell, fldTrace, interiorTraceGroupGlobal );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
  }

public:
  template <class FunctorInteriorTrace, class FieldCellClass, class FieldTraceClass >
  static void
  apply( GroupFunctorInteriorTraceType<FunctorInteriorTrace>&& functorType,
         const FieldType<FieldCellClass>& fldCellType,
         const FieldType<FieldTraceClass>& fldTraceType )
  {
    typedef typename FieldCellClass::XFieldType XFieldType;
    FunctorInteriorTrace& functor = functorType.cast();
    const FieldCellClass& fldCell = fldCellType.cast();
    const FieldTraceClass& fldTrace = fldTraceType.cast();

    // extract Xfield for associativity information
    const XFieldType& xfld = fldCell.getXField();

    // loop over element groups
    for (std::size_t group = 0; group < functor.nInteriorTraceGroups(); group++)
    {
      const int interiorTraceGroupGlobal = functor.interiorTraceGroup(group);

      // dispatch integration over elements of group
      if ( xfld.getInteriorTraceGroupBaseGlobal(interiorTraceGroupGlobal).topoTypeID() == typeid(Tet) )
      {
        LeftTopology_Tet<FieldTraceClass,XFieldType>( functor,
            xfld.template getInteriorTraceGroupGlobal<Tet>(interiorTraceGroupGlobal),
            fldCell,
            fldTrace.template getInteriorTraceGroupGlobal<Tet>(interiorTraceGroupGlobal),
            interiorTraceGroupGlobal );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown interior trace topology\n" );
    }
  }
};

}

#endif  // FOR_EACH_INTERIORFIELDTRACEGROUP_CELL_H
