// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef GROUPFUNCTOR_TYPES_H
#define GROUPFUNCTOR_TYPES_H

namespace SANS
{

// A set of base classes to indicate a specific class is an implementation of an group functor

template<class Derived>
struct GroupFunctorCellType
{
  //A convenient method for casting to the derived type
  inline const Derived& cast() const { return static_cast<const Derived&>(*this); }
  inline       Derived& cast()       { return static_cast<      Derived&>(*this); }
};

template<class Derived>
struct GroupFunctorHubTraceType
{
  //A convenient method for casting to the derived type
  inline const Derived& cast() const { return static_cast<const Derived&>(*this); }
  inline       Derived& cast()       { return static_cast<      Derived&>(*this); }
};

template<class Derived>
struct GroupFunctorInteriorTraceType
{
  //A convenient method for casting to the derived type
  inline const Derived& cast() const { return static_cast<const Derived&>(*this); }
  inline       Derived& cast()       { return static_cast<      Derived&>(*this); }
};

template<class Derived>
struct GroupFunctorBoundaryTraceType
{
  //A convenient method for casting to the derived type
  inline const Derived& cast() const { return static_cast<const Derived&>(*this); }
  inline       Derived& cast()       { return static_cast<      Derived&>(*this); }
};

template<class Derived>
struct GroupFunctorBoundaryCellType
{
  //A convenient method for casting to the derived type
  inline const Derived& cast() const { return static_cast<const Derived&>(*this); }
  inline       Derived& cast()       { return static_cast<      Derived&>(*this); }
};

}

#endif //GROUPFUNCTOR_TYPES_H
