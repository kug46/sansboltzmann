// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FOR_EACH_BOUNDARYTRACEGROUP_H
#define FOR_EACH_BOUNDARYTRACEGROUP_H

// Applies a function to each boundary trace group

#include <typeinfo>

#include "tools/SANSException.h"
#include "Topology/ElementTopology.h"
#include "Field/FieldTypes.h"
#include "GroupFunctorType.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template<class TopDim>
class for_each_BoundaryTraceGroup;

// base class interface
template<>
class for_each_BoundaryTraceGroup<TopoD1>
{
public:
  // typedef TopoD1 TopoDim;

  template <class FunctorBoundaryTrace, class FieldClass>
  static void
  apply( GroupFunctorBoundaryTraceType<FunctorBoundaryTrace>&& functorType,
         const FieldType<FieldClass>& fldType )
  {
    FunctorBoundaryTrace& functor = functorType.cast();
    const FieldClass& fld = fldType.cast();

    // loop over element groups
    for (std::size_t group = 0; group < functor.nBoundaryTraceGroups(); group++)
    {
      const int boundaryTraceGroupGlobal = functor.boundaryTraceGroup(group);

      // dispatch integration over elements of group
      if ( fld.getBoundaryTraceGroupBaseGlobal(boundaryTraceGroupGlobal).topoTypeID() == typeid(Node) )
      {
        functor.template apply<Node>(
            fld.template getBoundaryTraceGroupGlobal<Node>(boundaryTraceGroupGlobal),
            boundaryTraceGroupGlobal );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown boundary trace topology\n" );

    }
  }
};


template<>
class for_each_BoundaryTraceGroup<TopoD2>
{
public:
  // typedef TopoD2 TopoDim;

  template <class FunctorBoundaryTrace, class FieldClass>
  static void
  apply( GroupFunctorBoundaryTraceType<FunctorBoundaryTrace>&& functorType,
         const FieldType<FieldClass>& fldType )
  {
    FunctorBoundaryTrace& functor = functorType.cast();
    const FieldClass& fld = fldType.cast();

    // loop over element groups
    for (std::size_t group = 0; group < functor.nBoundaryTraceGroups(); group++)
    {
      const int boundaryTraceGroupGlobal = functor.boundaryTraceGroup(group);

      // dispatch integration over elements of group
      if ( fld.getBoundaryTraceGroupBaseGlobal(boundaryTraceGroupGlobal).topoTypeID() == typeid(Line) )
      {
        functor.template apply<Line>(
            fld.template getBoundaryTraceGroupGlobal<Line>(boundaryTraceGroupGlobal),
            boundaryTraceGroupGlobal );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown boundary trace topology\n" );

    }
  }
};


template<>
class for_each_BoundaryTraceGroup<TopoD3>
{
public:
  // typedef TopoD3 TopoDim;

  template <class FunctorBoundaryTrace, class FieldClass>
  static void
  apply( GroupFunctorBoundaryTraceType<FunctorBoundaryTrace>&& functorType,
         const FieldType<FieldClass>& fldType )
  {
    FunctorBoundaryTrace& functor = functorType.cast();
    const FieldClass& fld = fldType.cast();

    // loop over element groups
    for (std::size_t group = 0; group < functor.nBoundaryTraceGroups(); group++)
    {
      const int boundaryTraceGroupGlobal = functor.boundaryTraceGroup(group);

      // dispatch integration over elements of group
      if ( fld.getBoundaryTraceGroupBaseGlobal(boundaryTraceGroupGlobal).topoTypeID() == typeid(Triangle) )
      {
        functor.template apply<Triangle>(
            fld.template getBoundaryTraceGroupGlobal<Triangle>(boundaryTraceGroupGlobal),
            boundaryTraceGroupGlobal );
      }
      else if ( fld.getBoundaryTraceGroupBaseGlobal(boundaryTraceGroupGlobal).topoTypeID() == typeid(Quad) )
      {
        functor.template apply<Quad>(
            fld.template getBoundaryTraceGroupGlobal<Quad>(boundaryTraceGroupGlobal),
            boundaryTraceGroupGlobal );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown boundary trace topology\n" );

    }
  }
};

template<>
class for_each_BoundaryTraceGroup<TopoD4>
{
public:
  // typedef TopoD4 TopoDim;

  template <class FunctorBoundaryTrace, class FieldClass>
  static void
  apply( GroupFunctorBoundaryTraceType<FunctorBoundaryTrace>&& functorType,
         const FieldType<FieldClass>& fldType )
  {
    FunctorBoundaryTrace& functor = functorType.cast();
    const FieldClass& fld = fldType.cast();

    // loop over element groups
    for (std::size_t group = 0; group < functor.nBoundaryTraceGroups(); group++)
    {
      const int boundaryTraceGroupGlobal = functor.boundaryTraceGroup(group);

      // dispatch integration over elements of group
      if ( fld.getBoundaryTraceGroupBaseGlobal(boundaryTraceGroupGlobal).topoTypeID() == typeid(Tet) )
      {
        functor.template apply<Tet>(
            fld.template getBoundaryTraceGroupGlobal<Tet>(boundaryTraceGroupGlobal),
            boundaryTraceGroupGlobal );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown boundary trace topology\n" );

    }
  }
};

}

#endif  // FOR_EACH_BOUNDARYTRACEGROUP_H
