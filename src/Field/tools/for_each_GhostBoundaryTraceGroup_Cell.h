// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FOR_EACH_GHOSTBOUNDARYTRACEGROUP_CELL_H
#define FOR_EACH_GHOSTBOUNDARYTRACEGROUP_CELL_H

// Applies a function to each ghost boundary cell neighboring a boundary trace group

#include <typeinfo>

#include "tools/SANSException.h"
#include "Topology/ElementTopology.h"
#include "Field/FieldTypes.h"
#include "GroupFunctorType.h"

namespace SANS
{

template <class TopologyTrace, class TopologyL, class XFieldType,
          class FunctorBoundaryCell, class FieldCellClass>
void
for_each_GhostBoundaryTraceGroup_Cell_apply(
     FunctorBoundaryCell& functor,
     const typename XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
     const FieldCellClass& fldCell, const int boundaryGroupGlobal )
{
  int groupL = xfldTrace.getGroupLeft();

  // Integrate over the trace group
  functor.template apply<TopologyTrace, TopologyL>(
      fldCell.template getCellGroup<TopologyL>(groupL), groupL,
      xfldTrace, boundaryGroupGlobal );
}


//----------------------------------------------------------------------------//
template<class TopDim>
class for_each_GhostBoundaryTraceGroup_Cell;


template<>
class for_each_GhostBoundaryTraceGroup_Cell<TopoD1>
{
public:
  // typedef TopoD1 TopoDim;

protected:
  template <class TopologyTrace, class XFieldType,
            class FunctorBoundaryCell, class FieldCellClass>
  static void
  LeftTopology(
      FunctorBoundaryCell& functor,
      const typename XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const FieldCellClass& fldCell, const int boundaryGroupGlobal )
  {

    // determine topology for Left cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( fldCell.getCellGroupBaseGlobal(groupL).topoTypeID() == typeid(Line) )
      for_each_GhostBoundaryTraceGroup_Cell_apply<TopologyTrace, Line, XFieldType>( functor, xfldTrace, fldCell, boundaryGroupGlobal );
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
  }

public:
  template <class FunctorBoundaryCell, class FieldClass>
  static void
  apply( GroupFunctorBoundaryCellType<FunctorBoundaryCell>&& functorType,
         const FieldType<FieldClass>& fldType )
  {
    typedef typename FieldClass::XFieldType XFieldType;
    FunctorBoundaryCell& functor = functorType.cast();
    const FieldClass& fldCell = fldType.cast();

    const XFieldType& xfld = fldCell.getXField();

    // loop over trace element groups
    for (std::size_t group = 0; group < functor.nBoundaryGroups(); group++)
    {
      const int boundaryGroupGlobal = functor.boundaryGroup(group);

      if ( xfld.getGhostBoundaryTraceGroupBase(boundaryGroupGlobal).topoTypeID() == typeid(Node) )
      {
        LeftTopology<Node, XFieldType>( functor,
            xfld.template getGhostBoundaryTraceGroup<Node>(boundaryGroupGlobal),
            fldCell, boundaryGroupGlobal );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown trace topology" );
    }
  }
};


template<>
class for_each_GhostBoundaryTraceGroup_Cell<TopoD2>
{
public:
  // typedef TopoD2 TopoDim;

protected:
  template <class TopologyTrace, class XFieldType,
            class FunctorBoundaryCell, class FieldCellClass>
  static void
  LeftTopology(
      FunctorBoundaryCell& functor,
      const typename XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const FieldCellClass& fldCell, const int boundaryGroupGlobal )
  {

    // determine topology for Left cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( fldCell.getCellGroupBaseGlobal(groupL).topoTypeID() == typeid(Triangle) )
    {
      // determine topology for R
      for_each_GhostBoundaryTraceGroup_Cell_apply<TopologyTrace, Triangle, XFieldType>(
          functor, xfldTrace, fldCell, boundaryGroupGlobal );
    }
    else if ( fldCell.getCellGroupBaseGlobal(groupL).topoTypeID() == typeid(Quad) )
    {
      // determine topology for R
      for_each_GhostBoundaryTraceGroup_Cell_apply<TopologyTrace, Quad, XFieldType>(
          functor, xfldTrace, fldCell, boundaryGroupGlobal );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
  }

public:
  template <class FunctorBoundaryCell, class FieldClass>
  static void
  apply( GroupFunctorBoundaryCellType<FunctorBoundaryCell>&& functorType,
         const FieldType<FieldClass>& fldType )
  {
    typedef typename FieldClass::XFieldType XFieldType;
    FunctorBoundaryCell& functor = functorType.cast();
    const FieldClass& fld = fldType.cast();

    const XFieldType& xfld = fld.getXField();

    // loop over trace element groups
    for (std::size_t group = 0; group < functor.nBoundaryGroups(); group++)
    {
      const int boundaryGroupGlobal = functor.boundaryGroup(group);

      if ( xfld.getGhostBoundaryTraceGroupBase(boundaryGroupGlobal).topoTypeID() == typeid(Line) )
      {
        LeftTopology<Line,XFieldType>( functor,
            xfld.template getGhostBoundaryTraceGroup<Line>(boundaryGroupGlobal),
            fld, boundaryGroupGlobal );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown trace topology" );
    }
  }
};


template<>
class for_each_GhostBoundaryTraceGroup_Cell<TopoD3>
{
public:
  // typedef TopoD3 TopoDim;

protected:
  template <class XFieldType, class FunctorBoundaryCell, class FieldCellClass>
  static void
  LeftTopology_Triangle(
      FunctorBoundaryCell& functor,
      const typename XFieldType::template FieldTraceGroupType<Triangle>& xfldTrace,
      const FieldCellClass& fldCell, const int boundaryGroupGlobal )
  {

    // determine topology for Left cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( fldCell.getCellGroupBaseGlobal(groupL).topoTypeID() == typeid(Tet) )
    {
      // determine topology for R
      for_each_GhostBoundaryTraceGroup_Cell_apply<Triangle, Tet, XFieldType>(
          functor, xfldTrace, fldCell, boundaryGroupGlobal );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );

  }

  template <class XFieldType, class FunctorBoundaryCell, class FieldCellClass>
  static void
  LeftTopology_Quad(
      FunctorBoundaryCell& functor,
      const typename XFieldType::template FieldTraceGroupType<Quad>& xfldTrace,
      const FieldCellClass& fldCell, const int boundaryGroupGlobal )
  {

    // determine topology for Left cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( fldCell.getCellGroupBaseGlobal(groupL).topoTypeID() == typeid(Hex) )
    {
      // determine topology for R
      for_each_GhostBoundaryTraceGroup_Cell_apply<Quad, Hex, XFieldType>(
          functor, xfldTrace, fldCell, boundaryGroupGlobal );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );

  }

public:
  template <class FunctorBoundaryCell, class FieldClass>
  static void
  apply( GroupFunctorBoundaryCellType<FunctorBoundaryCell>&& functorType,
         const FieldType<FieldClass>& fldType )
  {
    typedef typename FieldClass::XFieldType XFieldType;
    FunctorBoundaryCell& functor = functorType.cast();
    const FieldClass& fldCell = fldType.cast();

    const XFieldType& xfld = fldCell.getXField();

    // loop over trace element groups
    for (std::size_t group = 0; group < functor.nBoundaryGroups(); group++)
    {
      const int boundaryGroupGloabl = functor.boundaryGroup(group);

      if ( xfld.getGhostBoundaryTraceGroupBase(boundaryGroupGloabl).topoTypeID() == typeid(Triangle) )
      {
        LeftTopology_Triangle<XFieldType>( functor,
            xfld.template getGhostBoundaryTraceGroup<Triangle>(boundaryGroupGloabl),
            fldCell, boundaryGroupGloabl );
      }
      else if ( xfld.getGhostBoundaryTraceGroupBase(boundaryGroupGloabl).topoTypeID() == typeid(Quad) )
      {
        LeftTopology_Quad<XFieldType>( functor,
            xfld.template getGhostBoundaryTraceGroup<Quad>(boundaryGroupGloabl),
            fldCell, boundaryGroupGloabl );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown trace topology" );
    }
  }
};


template<>
class for_each_GhostBoundaryTraceGroup_Cell<TopoD4>
{
public:
  // typedef TopoD4 TopoDim;

protected:
  template <class XFieldType, class FunctorBoundaryCell, class FieldCellClass>
  static void
  LeftTopology_Tet(
      FunctorBoundaryCell& functor,
      const typename XFieldType::template FieldTraceGroupType<Tet>& xfldTrace,
      const FieldCellClass& fldCell, const int boundaryGroupGlobal )
  {

    // determine topology for Left cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( fldCell.getCellGroupBaseGlobal(groupL).topoTypeID() == typeid(Pentatope) )
    {
      // determine topology for R
      for_each_GhostBoundaryTraceGroup_Cell_apply<Tet, Pentatope, XFieldType>(
          functor, xfldTrace, fldCell, boundaryGroupGlobal );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );

  }

public:
  template <class FunctorBoundaryCell, class FieldClass>
  static void
  apply( GroupFunctorBoundaryCellType<FunctorBoundaryCell>&& functorType,
         const FieldType<FieldClass>& fldType )
  {
    typedef typename FieldClass::XFieldType XFieldType;
    FunctorBoundaryCell& functor = functorType.cast();
    const FieldClass& fldCell = fldType.cast();

    const XFieldType& xfld = fldCell.getXField();

    // loop over trace element groups
    for (std::size_t group = 0; group < functor.nBoundaryGroups(); group++)
    {
      const int boundaryGroupGloabl = functor.boundaryGroup(group);

      if ( xfld.getGhostBoundaryTraceGroupBase(boundaryGroupGloabl).topoTypeID() == typeid(Tet) )
      {
        LeftTopology_Tet<XFieldType>( functor,
            xfld.template getGhostBoundaryTraceGroup<Tet>(boundaryGroupGloabl),
            fldCell, boundaryGroupGloabl );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown trace topology" );
    }
  }
};

} // SANS

#endif  // FOR_EACH_GHOSTBOUNDARYTRACEGROUP_CELL_H
