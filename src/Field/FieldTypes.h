// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDTYPES_H
#define FIELDTYPES_H

namespace SANS
{

//----------------------------------------------------------------------------//
// Classes for identifying field classes
//----------------------------------------------------------------------------//

class FieldTypeBase {};

template< class Derived >
struct FieldType : FieldTypeBase
{
  //A convenient method for casting to the derived type
  const Derived& cast() const { return static_cast<const Derived&>(*this); }
        Derived& cast()       { return static_cast<      Derived&>(*this); }
};

//----------------------------------------------------------------------------//
// General field that is specialized based on the dimension
//----------------------------------------------------------------------------//

template <class FieldTraits>
class FieldBase;

//----------------------------------------------------------------------------//
// Grid field
//----------------------------------------------------------------------------//

template<class PhysDim, class TopoDim>
struct XFieldTraits;

template<class PhysDim, class TopoDim>
struct XFieldTraits_BoundaryTrace;

template <class PhysDim, class TopoDim>
class XField;

//----------------------------------------------------------------------------//
// Generic Variable field
//----------------------------------------------------------------------------//

template<class TopoDim, class T>
struct FieldTraits;

template<class PhysDim, class TopoDim, class T>
class Field;

//----------------------------------------------------------------------------//
// Generic Variable field for lifting operators
//----------------------------------------------------------------------------//

template<class TopoDim, class T>
struct FieldLiftTraits;

template<class PhysDim, class TopoDim, class T>
class FieldLift;

//----------------------------------------------------------------------------//
// A class for managing a sequence of fields
//----------------------------------------------------------------------------//

template<class PhysDim, class TopoDim, class T, template<class,class,class> class BaseField = Field>
class FieldSequence;

//----------------------------------------------------------------------------//
// DG field types
//----------------------------------------------------------------------------//

template <class PhysDim, class TopoDim, class T>
class Field_DG_Cell;

template <class PhysDim, class TopoDim, class T>
class Field_DG_HubTrace;

template <class PhysDim, class TopoDim, class T>
class Field_DG_InteriorTrace;

template <class PhysDim, class TopoDim, class T>
class Field_DG_BoundaryTrace;

template <class PhysDim, class TopoDim, class T>
class Field_DG_Trace;

template <class PhysDim, class TopoDim, class T>
class FieldLift_DG_Cell;

template <class PhysDim, class TopoDim, class T>
class FieldLift_DG_BoundaryTrace;

//----------------------------------------------------------------------------//
// EG field types
//----------------------------------------------------------------------------//

template <class PhysDim, class TopoDim, class T>
class Field_EG_Cell;

//----------------------------------------------------------------------------//
// CG field types
//----------------------------------------------------------------------------//

template <class PhysDim, class TopoDim, class T>
class Field_CG_Cell;

template <class PhysDim, class TopoDim, class T>
class Field_CG_InteriorTrace;

template <class PhysDim, class TopoDim, class T>
class Field_CG_Trace;

template <class PhysDim, class TopoDim, class T>
class Field_CG_BoundaryTrace;

}

#endif  // FIELDTYPES_H
