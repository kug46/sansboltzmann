// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDLINE_DG_INTERIORTRACE_H
#define FIELDLINE_DG_INTERIORTRACE_H

#include "Field_DG_InteriorTraceBase.h"
#include "FieldLine.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// solution field: DG interior trace (e.g. interface solution)
//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//
// 1D solution field: DG interior trace
//----------------------------------------------------------------------------//

template <class PhysDim, class T>
class Field_DG_InteriorTrace< PhysDim, TopoD1, T > : public Field_DG_InteriorTraceBase< PhysDim, TopoD1, T >
{
public:
  typedef Field_DG_InteriorTraceBase< PhysDim, TopoD1, T > BaseType;
  typedef T ArrayQ;

  Field_DG_InteriorTrace( const XField<PhysDim, TopoD1>& xfld, const int order, const BasisFunctionCategory& category );

  Field_DG_InteriorTrace( const XField<PhysDim, TopoD1>& xfld, const int order,
                          const BasisFunctionCategory& category, const std::vector<int>& InteriorGroups );

  // Groups are first to remove ambiguity with list initializers
  // DG Fields are already broken, can just cat the CellGroupSets and forward
  explicit Field_DG_InteriorTrace( const std::vector<std::vector<int>>& InteriorGroupSets,
                                   const XField<PhysDim, TopoD1>& xfld, const int order, const BasisFunctionCategory& category )
  : Field_DG_InteriorTrace( xfld, order, category, SANS::cat(InteriorGroupSets)) {}

  Field_DG_InteriorTrace( const Field_DG_InteriorTrace& fld, const FieldCopy& tag );
  Field_DG_InteriorTrace& operator=( const ArrayQ& q );

protected:
  using BaseType::nDOF_;
  using BaseType::DOF_;
  using BaseType::interiorTraceGroups_;

  void init(const XField<PhysDim, TopoD1>& xfld, const int order,
            const BasisFunctionCategory& category, const std::vector<int>& InteriorGroups );
};

}

#endif  // FIELDLINE_DG_INTERIORTRACE_H
