// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(XFIELD_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include <ostream>
#include <sstream>

#include "XField.h"
#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// This trace normal always works when PhysDim and TopoDim are the same
//----------------------------------------------------------------------------//

template<class PhysDim, class TopoDim>
template<class TopologyTrace, class TopologyL>
void
XField< PhysDim, TopoDim >::
checkTraceNormal( const typename BaseType::template FieldTraceGroupType<TopologyTrace>& traceGroup, int elem,
                  ElementXField<PhysDim, typename TopologyTrace::TopoDim, TopologyTrace>& elemTrace,
                  const ElementXField<PhysDim, TopoDim, TopologyL>& elemCellL ) const
{
#if 1
  // this check really only works for linear elements... we need something better for curved...
  if (elemTrace.order() != 1) return;

  VectorX N, cT, cL, dX; // Normal and centroids

  // Get the trace element
  traceGroup.getElement( elemTrace, elem );

  // Get the edge normal and centroid
  elemTrace.unitNormal( TopologyTrace::centerRef, N);
  elemTrace.coordinates( TopologyTrace::centerRef, cT );

  //Get the left cell centroid
  elemCellL.coordinates( TopologyL::centerRef, cL );

  // Compute the vector from the trace-to-cell centroids
  dX = cL - cT;

  // normal should point from left to right ->
  if (dot(dX, N) > 0)
  {
    std::stringstream msg;
    msg << std::endl;
    msg << " Error! dot(N,dx) > 0 " << std::endl;
    msg << " dot(N,dX) = " << dot( N, dX ) << " for elem " << elem << std::endl;
    msg << " Normal vector: " << N << std::endl;
    msg << " Trace Centroid: " << cT << std::endl;
    msg << " Cell Centroid: " << cL << std::endl;
    msg << std::endl;

    XFIELD_EXCEPTION( msg.str() );
  }
#endif
}

}
