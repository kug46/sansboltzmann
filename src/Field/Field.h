// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELD_H
#define FIELD_H

#include "FieldSubGroup.h"
#include "Field/Element/Element.h"

#include "XField.h"
#include "FieldTypes.h"
#include "SpaceTypes.h"

#include "LinearAlgebra/GlobalContinuousMap.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// General variable field
//----------------------------------------------------------------------------//

template<class PhysDim, class TopoDim, class T>
class Field : public FieldSubGroup< PhysDim, FieldTraits<TopoDim, T> >, public FieldType< Field<PhysDim,TopoDim,T> >
{
public:
  typedef FieldSubGroup< PhysDim, FieldTraits<TopoDim, T> > BaseType;
  typedef typename BaseType::XFieldType XFieldType;
  typedef typename BaseType::FieldTraceGroupBase FieldTraceGroupBase;
  typedef typename BaseType::FieldCellGroupBase FieldCellGroupBase;

  template<class Topology>
  using FieldTraceGroupType = typename BaseType::template FieldTraceGroupType<Topology>;

  template<class Topology>
  using FieldCellGroupType = typename BaseType::template FieldCellGroupType<Topology>;

  static const int D = PhysDim::D;

  Field( const Field& ) = delete;
  Field& operator=( const Field& ) = delete;

  // cppcheck-suppress noExplicitConstructor
  Field( const XField<PhysDim, TopoDim>& xfld );
  Field( const Field& fld, const FieldCopy& tag );

  virtual ~Field();

  Field& operator=( const T& q );
  void cloneFrom( const Field& fld );

  virtual int nDOFCellGroup(int cellgroup) const override;
  virtual int nDOFInteriorTraceGroup(int tracegroup) const override;
  virtual int nDOFBoundaryTraceGroup(int tracegroup) const override;

  virtual GlobalContinuousMap continuousGlobalMap(const int nDOFpre = 0, const int nDOFpost = 0) const;

  // An enum class to describe the space of the field, i.e. Continuous or Discontinuous
  // Also Unspecified for Fields which can be either/to facilitate lazy developers who forget to specify
  virtual SpaceType spaceType() const { return SpaceType::Unspecified; };

  int nDOFpossessed() const { return nDOFpossessed_;                        }
  int nDOFghost()     const { return nDOFghost_;                            }
  int nDOFzombie()    const { return nDOF_ - (nDOFpossessed_ + nDOFghost_); }

  // total number of DOF in a serial representation of the Field
  int nDOFnative() const;

        T& DOFpossessed( int n )       { return DOF_[n]; }
  const T& DOFpossessed( int n ) const { return DOF_[n]; }

        T& DOFghost( int n )       { return DOF_[n+nDOFpossessed_]; }
  const T& DOFghost( int n ) const { return DOF_[n+nDOFpossessed_]; }

  // give the rank of a ghost DOF
  int DOFghost_rank(const int i) const { return DOFghost_rank_[i]; }

  // synchronizes DOFs possessed by other processors
  void syncDOFs_MPI_noCache(); //Use if a single sync or rare sync is needed
  void syncDOFs_MPI_Cached();  //Use for repeated sync to reduce communication overhead

protected:
  void deallocate();

  void resizeDOF( const int nDOF );
  void resizeDOF_rank( const int nDOF, const int nDOFpossessed );

  void setupDOFsync(std::vector<std::vector<int>>& sendDOF);
  void syncDOFs(const std::vector<std::vector<int>>& sendDOF);

  int nDOFpossessed_;                        // DOFs possessed by the current processor (ghost DOFs are stored last in DOF_ after nDOFpossessed)
  int nDOFghost_;                            // number of ghost DOFs that follow the possessed DOFs, zombies are always at the end of the list

  int* DOFghost_rank_;                       // the rank assignment of each ghost DOF
  std::vector<std::vector<int>> sendDOF_;    // specifies which DOFs need to be sent to ranks

  using BaseType::nDOF_;
  using BaseType::DOF_;
  using BaseType::local2nativeDOFmap_;

  using BaseType::localHubTraceGroups_;
  using BaseType::localInteriorTraceGroups_;
  using BaseType::localBoundaryTraceGroups_;
  using BaseType::localCellGroups_;

  using BaseType::globalHubTraceGroups_;
  using BaseType::globalInteriorTraceGroups_;
  using BaseType::globalBoundaryTraceGroups_;
  using BaseType::globalCellGroups_;
};


} //namespace SANS

#endif //FIELD_H
