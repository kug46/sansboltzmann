// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELDTYPES_H
#define XFIELDTYPES_H

namespace SANS
{

// Topologically 1D XField types

template<class PhysDim, class TopoDim>
struct XFieldGroupNodeTraits;

// Topologically 1D XField types

template<class PhysDim, class TopoDim>
struct XFieldGroupLineTraits;

// Topologically 2D XField types

template<class PhysDim, class TopoDim>
struct XFieldAreaTraits;

template <class PhysDim, class TopoDim, class Topology>
struct XFieldGroupAreaTraits;

// Topologically 3D XField types

template<class PhysDim, class TopoDim>
struct XFieldVolumeTraits;

template <class PhysDim, class TopoDim, class Topology>
struct XFieldGroupVolumeTraits;

// Topologically 4D XField types

template<class PhysDim, class TopoDim>
struct XFieldSpacetimeTraits;

template<class PhysDim, class TopoDim, class Topology>
struct XFieldGroupSpacetimeTraits;

}

#endif
