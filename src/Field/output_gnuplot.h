// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef OUTPUT_GNPLUOT_H
#define OUTPUT_GNPLUOT_H

#include <ostream>
#include <string>

#include "tools/SANSnumerics.h"     // Real

#include "Field.h"
#include "XField.h"

namespace SANS
{

template<class T>
void
output_gnuplot( const Field< PhysD1, TopoD1, T >& qfld, const std::string& filename );


} //namespace SANS

#endif  // OUTPUT_GNPLUOT_H
