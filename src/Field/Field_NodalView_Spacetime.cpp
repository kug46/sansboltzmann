// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BasisFunction/ElementEdges.h"
#include "Field_NodalView_impl.h"
#include "XFieldSpacetime.h"
#include "FieldSpacetime_CG_Cell.h"

#include <algorithm> //std::sort

namespace SANS
{

//=============================================================================
template <class FieldTraits>
void
Field_NodalView::init(const FieldBase<FieldTraits>& fld, const std::vector<int>& cellgroup_list)
{
  std::size_t nCellGroups = cellgroup_list.size();

  std::map<int, std::set<GroupElemIndex>> NodeDOF2CellMap;
  std::map<int, std::set<std::pair<int, int>>> NodeDOF2EdgeMap;

  //Create connectivity structure
  for (std::size_t group = 0; group < nCellGroups; group++ )
  {
    const int cellGroupGlobal = cellgroup_list[group];

    int nElem = fld.getCellGroupBase(cellGroupGlobal).nElem();

    if (fld.getCellGroupBase(cellGroupGlobal).topoTypeID() == typeid(Pentatope))
    {
      typedef typename FieldBase<FieldTraits>:: template FieldCellGroupType<Pentatope> FieldCellGroupType;
      const FieldCellGroupType& xfld_cellgrp = fld.template getCellGroup<Pentatope>(cellGroupGlobal);

      int nodeDOFMap[Pentatope::NNode];
      const int (*EdgeNodes)[ Line::NNode ] = ElementEdges<Pentatope>::EdgeNodes;

      for (int elem = 0; elem < nElem; elem++)
      {
        //SANS_DEVELOPER_EXCEPTION("implement");
        xfld_cellgrp.associativity(elem).getNodeGlobalMapping(nodeDOFMap, Pentatope::NNode);

        this->insertCellNodes(nodeDOFMap, Pentatope::NNode, cellGroupGlobal, elem, NodeDOF2CellMap);
        this->insertEdge(nodeDOFMap[EdgeNodes[0][0]],  nodeDOFMap[EdgeNodes[0][1]],  NodeDOF2EdgeMap); //Canonical edge 0
        this->insertEdge(nodeDOFMap[EdgeNodes[1][0]],  nodeDOFMap[EdgeNodes[1][1]],  NodeDOF2EdgeMap); //Canonical edge 1
        this->insertEdge(nodeDOFMap[EdgeNodes[2][0]],  nodeDOFMap[EdgeNodes[2][1]],  NodeDOF2EdgeMap); //Canonical edge 2
        this->insertEdge(nodeDOFMap[EdgeNodes[3][0]],  nodeDOFMap[EdgeNodes[3][1]],  NodeDOF2EdgeMap); //Canonical edge 3
        this->insertEdge(nodeDOFMap[EdgeNodes[4][0]],  nodeDOFMap[EdgeNodes[4][1]],  NodeDOF2EdgeMap); //Canonical edge 4
        this->insertEdge(nodeDOFMap[EdgeNodes[5][0]],  nodeDOFMap[EdgeNodes[5][1]],  NodeDOF2EdgeMap); //Canonical edge 5
        this->insertEdge(nodeDOFMap[EdgeNodes[6][0]],  nodeDOFMap[EdgeNodes[6][1]],  NodeDOF2EdgeMap); //Canonical edge 6
        this->insertEdge(nodeDOFMap[EdgeNodes[7][0]],  nodeDOFMap[EdgeNodes[7][1]],  NodeDOF2EdgeMap); //Canonical edge 7
        this->insertEdge(nodeDOFMap[EdgeNodes[8][0]],  nodeDOFMap[EdgeNodes[8][1]],  NodeDOF2EdgeMap); //Canonical edge 8
        this->insertEdge(nodeDOFMap[EdgeNodes[9][0]],  nodeDOFMap[EdgeNodes[9][1]],  NodeDOF2EdgeMap); //Canonical edge 9
      }
    }
    else
      SANS_DEVELOPER_EXCEPTION("Field_NodalView<PhysDim, TopoD4> - Unknown Topology type for CellGroup.");
  }

  std::sort(NodeDOFList_.begin(), NodeDOFList_.end());

  this->setMaps( NodeDOF2CellMap, NodeDOF2EdgeMap );
  this->updateInverseNodeMap(fld.nDOF());
}

//=============================================================================
//Explicitly instantiate
template Field_NodalView::Field_NodalView(const XField<PhysD4, TopoD4>& xfld, const std::vector<int>& cellgroup_list);

template Field_NodalView::Field_NodalView(const Field_CG_Cell<PhysD4, TopoD4, Real>&);
template Field_NodalView::Field_NodalView(const Field_CG_Cell<PhysD4, TopoD4, DLA::MatrixSymS<4,Real>>&);
template Field_NodalView::Field_NodalView(const Field_CG_Cell<PhysD4, TopoD4, DLA::MatrixSymS<4,Real>>&, const std::vector<int>&);

template Field_NodalView::Field_NodalView(const Field_CG_Cell<PhysD4, TopoD4, DLA::VectorS<6,Real>>&);
template Field_NodalView::Field_NodalView(const Field_CG_Cell<PhysD4, TopoD4, DLA::VectorS<6,Real>>&, const std::vector<int>&);

}
