// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(FIELDAREA_EG_CELL_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "FieldArea_EG_Cell.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// 2-D solution field: DG cell-field
//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//
template <class PhysDim, class T>
Field_EG_Cell<PhysDim, TopoD2, T>::Field_EG_Cell( const Field_CG_Cell<PhysDim, TopoD2, T>& cgfld,
                                                  const int order, const BasisFunctionCategory& category ):
                                                  BaseType(cgfld.getXField()),
                                                  cgfld_(cgfld)
{
  // By use default all cell groups
  init(order, category, BaseType::createCellGroupIndex() );
}

//----------------------------------------------------------------------------//
template <class PhysDim, class T>
Field_EG_Cell<PhysDim, TopoD2, T>::Field_EG_Cell( const Field_CG_Cell<PhysDim, TopoD2, T>& cgfld,
                                                  const int order, const BasisFunctionCategory& category,
                                                  const std::vector<int>& CellGroups ) :
                                                  BaseType(cgfld.getXField()),
                                                  cgfld_(cgfld)
{
  // Check that the groups asked for are within the range of available groups
  BaseType::checkCellGroupIndex( CellGroups );
  init(order, category, CellGroups);
}

//----------------------------------------------------------------------------//
template <class PhysDim, class T>
Field_EG_Cell<PhysDim, TopoD2, T>::Field_EG_Cell( const Field_EG_Cell& fld, const FieldCopy& tag ) :
BaseType(fld, tag),
cgfld_(fld.cgfld_) {}

//----------------------------------------------------------------------------//
template <class PhysDim, class T>
Field_EG_Cell<PhysDim, TopoD2, T>&
Field_EG_Cell<PhysDim, TopoD2, T>::operator=( const ArrayQ& q )
{
  Field< PhysDim, TopoD2, T >::operator=(q);
  return *this;
}

//----------------------------------------------------------------------------//
template <class PhysDim, class T>
void
Field_EG_Cell<PhysDim, TopoD2, T>::
init( const int order, const BasisFunctionCategory& category, const std::vector<int>& CellGroups )
{

  std::vector<std::unique_ptr<FieldAssociativityConstructorBase>> fldAssocCells( CellGroups.size() );
  std::vector<int> nBasisGroups( CellGroups.size() );

  //allocate the cell groups
  this->resizeCellGroups( CellGroups );

  const int nCellGroups = CellGroups.size();
  std::vector<std::vector<bool>> isZombie;
  isZombie.resize( nCellGroups );

  for (std::size_t igroup = 0; igroup < CellGroups.size(); igroup++)
   {
     const int groupGlobal = CellGroups[igroup];
     isZombie[igroup].resize(xfld_.getCellGroupBase(groupGlobal).nElem(), false);


     if ( xfld_.getCellGroupBase(groupGlobal).topoTypeID() == typeid(Triangle) )
     {
       for (int elem = 0; elem < xfld_.getCellGroupBase(groupGlobal).nElem(); elem++)
       {

         int elemRank = xfld_.template getCellGroup<Triangle>(groupGlobal).associativity(elem).rank();
         if (elemRank == xfld_.comm()->rank() ) continue; //if element is owned by the processor, not a zombie

         int qDOF = cgfld_.template getCellGroup<Triangle>(igroup).basis()->nBasis();
         std::vector<int> mapDOFGlobal( qDOF, -1 );
         cgfld_.template getCellGroup<Triangle>(igroup).associativity( elem ).getGlobalMapping( mapDOFGlobal.data(), qDOF );

         bool zz = true;
         for (int i=0; i<qDOF; i++ )
         {
           if ( mapDOFGlobal[i] < cgfld_.nDOFpossessed() )
           {
             zz = false;
             break;
           }
         }

         isZombie[igroup][elem] = zz;
       }
     }
     else if ( xfld_.getCellGroupBase(groupGlobal).topoTypeID() == typeid(Quad) )
     {
       for (int elem = 0; elem < xfld_.getCellGroupBase(groupGlobal).nElem(); elem++)
       {

         int elemRank = xfld_.template getCellGroup<Quad>(groupGlobal).associativity(elem).rank();
         if (elemRank == xfld_.comm()->rank() ) continue; //if element is owned by the processor, not a zombie

         int qDOF = cgfld_.template getCellGroup<Quad>(igroup).basis()->nBasis();
         std::vector<int> mapDOFGlobal( qDOF, -1 );
         cgfld_.template getCellGroup<Quad>(igroup).associativity( elem ).getGlobalMapping( mapDOFGlobal.data(), qDOF );

         bool zz = true;
         for (int i=0; i<qDOF; i++ )
         {
           if ( mapDOFGlobal[i] < cgfld_.nDOFpossessed() )
           {
             zz = false;
             break;
           }
         }

         isZombie[igroup][elem] = zz;
       }

     }
     else
       SANS_DEVELOPER_EXCEPTION( "Unknown element topology." );

   }

  // Construct possessed cell groups
  int nDOFpossessed = 0;

  for (std::size_t igroup = 0; igroup < CellGroups.size(); igroup++)
  {
    const int groupGlobal = CellGroups[igroup];

    if (      xfld_.getCellGroupBase(groupGlobal).topoTypeID() == typeid(Triangle) )
    {
      fldAssocCells[igroup] = this->template createCellGroup<Triangle>(
                                groupGlobal, order, category, nDOFpossessed, nBasisGroups[igroup]);
    }
    else if ( xfld_.getCellGroupBase(groupGlobal).topoTypeID() == typeid(Quad) )
    {
      fldAssocCells[igroup] = this->template createCellGroup<Quad>(
                                groupGlobal, order, category, nDOFpossessed, nBasisGroups[igroup]);
    }
    else
      SANS_DEVELOPER_EXCEPTION( "Unknown element topology." );
  }

  int nDOF = nDOFpossessed;

  bool setZombie = false;
  for (int z = 0; z < 2; z++)
  {
    // first count regular ghost DOFs, then zombie ghost DOFs
    for (std::size_t igroup = 0; igroup < CellGroups.size(); igroup++)
    {
      const int groupGlobal = CellGroups[igroup];

      if (      xfld_.getCellGroupBase(groupGlobal).topoTypeID() == typeid(Triangle) )
      {
        this->template setGhostCellDOFs<Triangle>(
            groupGlobal, isZombie, setZombie, nDOF, fldAssocCells[igroup]);
      }
      else if ( xfld_.getCellGroupBase(groupGlobal).topoTypeID() == typeid(Quad) )
      {
        this->template setGhostCellDOFs<Quad>(
            groupGlobal, isZombie, setZombie, nDOF, fldAssocCells[igroup]);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Unknown element topology." );
    }
    setZombie = true;
  }

  //Allocate the DOFs array
  this->createDOFs(nDOF, nDOFpossessed, isZombie, nBasisGroups);
}

}
