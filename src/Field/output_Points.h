// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef OUTPUT_POINTS_H
#define OUTPUT_POINTS_H

#include <vector>

#include "tools/SANSnumerics.h"     // Real

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "Field.h"

namespace SANS
{

#if 0
template<class PhysDim, class T>
void
output_Points( const Field< PhysDim, TopoD1, T >& qfld, const std::string& filename,
               const std::vector<std::vector<DLA::VectorS<TopoD2::D,Real>>>& refPoints,
               const std::vector<std::string>& state_names = std::vector<std::string>(),
               const bool partitioned = false );
#endif

template<class PhysDim, class T>
void
output_Points( const Field< PhysDim, TopoD2, T >& qfld, const std::string& filename,
               const std::vector<std::vector<DLA::VectorS<TopoD2::D,Real>>>& refPoints,
               const std::vector<std::string>& state_names,
               const bool partitioned = false );

#if 0
template<class T>
void
output_Points( const Field< PhysD3, TopoD3, T >& qfld, const std::string& filename,
               const std::vector<std::vector<DLA::VectorS<TopoD2::D,Real>>>& refPoints,
               const std::vector<std::string>& state_names = std::vector<std::string>(),
               const bool partitioned = false );
#endif
} //namespace SANS

#endif  // OUTPUT_POINTS_H
