// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define FIELD_DG_CELLBASE_INSTANTIATE
#include "Field_DG_CellBase_impl.h"

#define FIELDLINE_EG_CELL_INSTANTIATE
#include "FieldLine_EG_Cell_impl.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"

#include "XFieldLine.h"

namespace SANS
{

template class Field_EG_Cell< PhysD1, TopoD1, Real >;

template class Field_EG_Cell< PhysD1, TopoD1, DLA::VectorS<1, Real> >;
template class Field_EG_Cell< PhysD1, TopoD1, DLA::VectorS<2, Real> >;
template class Field_EG_Cell< PhysD1, TopoD1, DLA::VectorS<3, Real> >;
template class Field_EG_Cell< PhysD1, TopoD1, DLA::VectorS<4, Real> >;
template class Field_EG_Cell< PhysD1, TopoD1, DLA::VectorS<5, Real> >;

template class Field_EG_Cell< PhysD1, TopoD1, DLA::VectorS< 1, DLA::VectorS<2, Real> > >;
template class Field_EG_Cell< PhysD1, TopoD1, DLA::VectorS< 1, DLA::VectorS<3, Real> > >;
template class Field_EG_Cell< PhysD1, TopoD1, DLA::VectorS< 1, DLA::VectorS<4, Real> > >;

template class Field_EG_Cell< PhysD1, TopoD1, DLA::MatrixSymS< 1, Real > >;

}
