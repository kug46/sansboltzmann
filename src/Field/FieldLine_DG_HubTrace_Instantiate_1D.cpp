// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define FIELD_DG_HUBTRACEBASE_INSTANTIATE
#include "Field_DG_HubTraceBase_impl.h"

#define FIELDLINE_DG_HUBTRACE_INSTANTIATE
#include "FieldLine_DG_HubTrace_impl.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "XFieldLine.h"

namespace SANS
{

template class Field_DG_HubTrace< PhysD1, TopoD1, Real >;

template class Field_DG_HubTrace< PhysD1, TopoD1, DLA::VectorS<1, Real> >;
template class Field_DG_HubTrace< PhysD1, TopoD1, DLA::VectorS<2, Real> >;
template class Field_DG_HubTrace< PhysD1, TopoD1, DLA::VectorS<3, Real> >;

}
