// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ELEMENTASSOCIATIVITYLINE_H
#define ELEMENTASSOCIATIVITYLINE_H

// Element line associativity (local to global mappings)

#include <ostream>
#include <vector>
#include <array>

#include "BasisFunction/BasisFunctionLine.h"
#include "ElementAssociativityLineConstructor.h"
#include "ElementLine.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// edge associativity: global numberings for single element nodes/edge
//
// member functions:
//   .order                     polynomial order
//   .nNode                     # node DOFs
//   .nEdge                     # edge interior DOFs
//   .node/edgeGlobal           global node/edge DOF accessors
//   .getNodeGlobalMapping      local-to-global node DOF mapping
//   .getEdgeGlobalMapping      local-to-global edge DOF mapping
//
//   .dump            debug dump of private data
//----------------------------------------------------------------------------//

template<class TopoDim, class Topology>
class ElementAssociativity;

template<>
class ElementAssociativity<TopoD1, Line>
{
public:
  static const int DElement = 1;                           // element dimensions

  typedef Line TopologyType;             // element topology
  typedef BasisFunctionLineBase BasisType;
  typedef std::array<int,Line::NNode> IntNNode;

  typedef ElementAssociativityConstructor<TopoD1, Line> Constructor;

  ElementAssociativity();
  explicit ElementAssociativity( int order );
  explicit ElementAssociativity( const BasisType* basis );
  ElementAssociativity( const ElementAssociativity& a );
  explicit ElementAssociativity( const Constructor& a) : ElementAssociativity() { operator=(a); }
  ~ElementAssociativity();

  ElementAssociativity& operator=( const ElementAssociativity& );
  ElementAssociativity& operator=( const Constructor& );

  int rank() const  { return rank_; }
  int order() const { return (order_); }
  int nNode() const { return (nNode_); }
  int nEdge() const { return (nEdge_); }
  int nBubble() const { return (nEdge_); }

  // node, edge maps
  int nodeGlobal( int n ) const { return (nodeList_[n]); }
  int edgeGlobal( int n ) const { return (edgeList_[n]); }

  void getNodeGlobalMapping( int node[], int nnode ) const;
  void getNodeGlobalMapping( std::vector<int>& node ) const;

  void getEdgeGlobalMapping( int edge[], int nedge ) const;
  void getEdgeGlobalMapping( std::vector<int>& edge ) const;

  void getGlobalMapping(     int map[] , int ndof ) const;
  void getBubbleMapping( int bubbleMap[], int nbubble ) const { getEdgeGlobalMapping(bubbleMap, nbubble); }

  // node sign accessors, these are dummies really
  const IntNNode& nodeSign() const { return nodeSign_; }
  const IntNNode& traceOrientation() const {return nodeSign_; }

  template<class ElemT, class T>
  void getElement(       Element<ElemT, TopoD1, Line>& fldElem, const T* DOF, const int nDOF  ) const;
  template<class ElemT, class T>
  void setElement( const Element<ElemT, TopoD1, Line>& fldElem,       T* DOF, const int nDOF  ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  int rank_;                      // the processor rank that possesses this element
  int order_;                     // polynomial order for node/edge DOFs
  int nNode_;                     // # node DOFs
  int nEdge_;                     // # edge DOFs
  int* nodeList_;                 // global ordering of node DOFs
  int* edgeList_;                 // global ordering of edge DOFs

  IntNNode nodeSign_;
};



// extract DOFs for individual element
template<class ElemT, class T>
void
ElementAssociativity<TopoD1,Line>::getElement( Element<ElemT, TopoD1, Line>& fldElem, const T* DOF, const int nDOF ) const
{
  SANS_ASSERT( (DOF != NULL) );
  SANS_ASSERT( fldElem.nDOF() == nNode_ + nEdge_ );

  int k, n, offset;
  offset = 0;
  for (n = 0; n < nNode_; n++)
  {
    k = nodeGlobal(n);
    fldElem.DOF(n + offset) = DOF[k];
  }
  offset += nNode_;

  for (n = 0; n < nEdge_; n++)
  {
    k = edgeGlobal(n);
    fldElem.DOF(n + offset) = DOF[k];
  }

  fldElem.setRank( rank_ );
}


// set DOFs for individual element
template<class ElemT, class T>
void
ElementAssociativity<TopoD1,Line>::setElement( const Element<ElemT, TopoD1, Line>& fldElem, T* DOF, const int nDOF  ) const
{
  SANS_ASSERT( (DOF != NULL) );
  SANS_ASSERT( fldElem.nDOF() == nNode_ + nEdge_ );

  int k, n, offset;

  offset = 0;
  for (n = 0; n < nNode_; n++)
  {
    k = nodeGlobal(n);
    DOF[k] = fldElem.DOF(n + offset);
  }
  offset += nNode_;

  for (n = 0; n < nEdge_; n++)
  {
    k = edgeGlobal(n);
    DOF[k] = fldElem.DOF(n + offset);
  }
}

}
#endif  // ELEMENTASSOCIATIVITYLINE_H
