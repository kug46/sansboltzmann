// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ELEMENTXFIELDLINE_H
#define ELEMENTXFIELDLINE_H

// line grid field element

#include <iostream>
#include <string>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BoundingBox.h"
#include "ElementLine.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Grid field element: line
//
// member functions:
//   .length          line length
//   .coordinates     physical coordinates given reference line coordinate
//----------------------------------------------------------------------------//

template< class PhysDim >
class ElementXFieldLineBase : public Element< DLA::VectorS<PhysDim::D,Real>, TopoD1, Line >
{
public:
  static const int D = PhysDim::D;            // physical dimensions
  static const int TopoD = TopoD1::D;         // topological dimensions
  typedef Element< DLA::VectorS<D,Real>, TopoD1, Line > BaseType;
  typedef DLA::VectorS<D,Real> VectorX;
  typedef DLA::MatrixSymS<D,Real> TensorSymX; // physical Hessian d(u)/d(x_i x_j) - e.g. implied metric
  typedef DLA::MatrixS<D,TopoD,Real> Matrix;  // element Jacobian d(x_i)/d(s_j)
  typedef DLA::VectorS<D, DLA::MatrixSymS<TopoD,Real>> TensorSymHessian; // element Hessian d^2(x_i)/d(s_j s_k)

  typedef DLA::VectorS<TopoD1::D,Real> RefCoordType;

  explicit ElementXFieldLineBase( const BasisFunctionLineBase* basis ) : BaseType(basis) {}
  ElementXFieldLineBase( int order, const BasisFunctionCategory& category ) : BaseType(order,category) {}
  ElementXFieldLineBase( const ElementXFieldLineBase& Elem ) : BaseType(Elem) {}
  ~ElementXFieldLineBase() {}

  ElementXFieldLineBase& operator=( const ElementXFieldLineBase& Elem ) { BaseType::operator=(Elem); return *this; }

  Real length() const;
  Real length( const Real& sRef ) const;
  Real length( const QuadraturePoint<TopoD1>& sRef ) const;

  VectorX coordinates( const Real& sRef ) const { return BaseType::eval( sRef ); }
  VectorX coordinates( const RefCoordType& sRef ) const { return BaseType::eval( sRef[0] ); }

  void coordinates( const Real& sRef, VectorX& X ) const { BaseType::eval( sRef, X ); }
  void coordinates( const RefCoordType& sRef, VectorX& X ) const { BaseType::eval( sRef[0], X ); }
  void coordinates( const QuadraturePoint<TopoD1>& sRef, VectorX& X ) const { BaseType::eval( sRef, X ); }
  void coordinates( const QuadratureCellTracePoint<TopoD1>& Ref, VectorX& X ) const { BaseType::eval( Ref, X ); }

  BoundingBox<PhysDim> boundingBox() const;

  Real jacobianDeterminant() const { return length(); }
  Real jacobianDeterminant( const Real& Ref ) const { return length(Ref); }
  Real jacobianDeterminant( const RefCoordType& Ref ) const { return length(Ref[0]); }
  Real jacobianDeterminant( const QuadraturePoint<TopoD1>& Ref ) const { return length(Ref); }

  void jacobian( Matrix& J ) const;
  void jacobian( const RefCoordType& Ref, Matrix& J ) const { jacobian(Ref[0], J); }
  void jacobian( const Real& sRef, Matrix& J ) const;

  void hessian( const RefCoordType& Ref, TensorSymHessian& H ) const { hessian(Ref[0], H); }
  void hessian( const Real& sRef, TensorSymHessian& H ) const;

  void impliedMetric( TensorSymX& M ) const;
  void impliedMetric( const RefCoordType& Ref, TensorSymX& M ) const { impliedMetric(Ref[0], M); }
  void impliedMetric( const Real& sRef, TensorSymX& M ) const;

  void tangent(const Real& sRef, VectorX& Xs) const;
  void unitTangent(const Real& sRef, VectorX& Xs) const;

  void tangent(const RefCoordType& sRef, VectorX& Xs) const { tangent(sRef[0], Xs); }
  void unitTangent(const RefCoordType& sRef, VectorX& Xs) const { unitTangent(sRef[0], Xs); }

  void tangent(const QuadraturePoint<TopoD1>& sRef, VectorX& Xs) const;
  void unitTangent(const QuadraturePoint<TopoD1>& sRef, VectorX& Xs) const;

  void tangent(const QuadratureCellTracePoint<TopoD1>& sRef, VectorX& Xs) const;
  void unitTangent(const QuadratureCellTracePoint<TopoD1>& sRef, VectorX& Xs) const;

  using BaseType::eval;
  using BaseType::evalBasisDerivative;

  template <int N>
  void evalBasisGradient( const SurrealS<N>& sRef,
                          const ElementBasis<TopoD1, Line>& fldElem,
                          DLA::VectorS<D,SurrealS<N>> gradphi[], int nphi ) const;

  void evalBasisGradient( const Real& sRef,
                          const ElementBasis<TopoD1, Line>& fldElem,
                          VectorX gradphi[], int nphi ) const;
  void evalBasisGradient( const RefCoordType& sRef,
                          const ElementBasis<TopoD1, Line>& fldElem,
                          VectorX gradphi[], int nphi ) const;
  void evalBasisGradient( const QuadraturePoint<TopoD1>& sRef,
                          const ElementBasis<TopoD1, Line>& fldElem,
                          VectorX gradphi[], int nphi ) const;
  void evalBasisGradient( const QuadratureCellTracePoint<TopoD1>& sRef,
                          const ElementBasis<TopoD1, Line>& fldElem,
                          VectorX gradphi[], int nphi ) const;
  void evalBasisGradient( const BasisPointDerivative<TopoD1::D>& dphi,
                          VectorX gradphi[], int nphi ) const;

  void evalBasisHessian( const RefCoordType& sRef,
                         const ElementBasis<TopoD1, Line>& fldElem,
                         TensorSymX hessphi[], int nphi ) const;
  void evalBasisHessian( const QuadraturePoint<TopoD1>& sRef,
                         const ElementBasis<TopoD1, Line>& fldElem,
                         TensorSymX hessphi[], int nphi ) const;
  void evalBasisHessian( const QuadratureCellTracePoint<TopoD1>& sRef,
                         const ElementBasis<TopoD1, Line>& fldElem,
                         TensorSymX hessphi[], int nphi ) const;

  template <int N>
  void evalReferenceCoordinateGradient(const SurrealS<N>& s, DLA::VectorS<D,SurrealS<N>>& sgrad ) const;

  void evalReferenceCoordinateGradient(const Real& s, VectorX& sgrad ) const;
  void evalReferenceCoordinateGradient(const RefCoordType& sRef, VectorX& sgrad ) const;
  void evalReferenceCoordinateGradient(const QuadraturePoint<TopoD1>& Ref, VectorX& sgrad ) const;
  void evalReferenceCoordinateGradient(const QuadratureCellTracePoint<TopoD1>& ref, VectorX& sgrad ) const;

  void evalReferenceCoordinateHessian(const Real& s, TensorSymX& shess )  const;

  template< class ElemT >
  void evalGradient( const Real& sRef,
                     const Element< ElemT, TopoD1, Line >& qfldElem,
                     DLA::VectorS<PhysDim::D, ElemT>& gradq ) const;
  template< class ElemT >
  void evalGradient( const RefCoordType& sRef,
                     const Element< ElemT, TopoD1, Line >& qfldElem,
                     DLA::VectorS<PhysDim::D, ElemT>& gradq ) const;
  template< class ElemT >
  void evalGradient( const QuadraturePoint<TopoD1>& sRef,
                     const Element< ElemT, TopoD1, Line >& qfldElem,
                     DLA::VectorS<PhysDim::D, ElemT>& gradq ) const;
  template< class ElemT >
  void evalGradient( const QuadratureCellTracePoint<TopoD1>& sRef,
                     const Element< ElemT, TopoD1, Line >& qfldElem,
                     DLA::VectorS<PhysDim::D, ElemT>& gradq ) const;

  template< class ElemT >
  void evalHessian( const RefCoordType& sRef,
                    const Element< ElemT, TopoD1, Line >& qfldElem,
                    DLA::MatrixSymS<PhysDim::D, ElemT>& hessq ) const;
  template< class ElemT >
  void evalHessian( const QuadraturePoint<TopoD1>& sRef,
                    const Element< ElemT, TopoD1, Line >& qfldElem,
                    DLA::MatrixSymS<PhysDim::D, ElemT>& hessq ) const;

  void dumpTecplot( std::ostream& out = std::cout ) const;

protected:

  Real length( const Real phis[] ) const;
  void jacobian( const Real* __restrict phis, Matrix& J ) const;
  void tangent( const Real* __restrict phis, VectorX& Xs ) const;
  template <int N>
  void evalReferenceCoordinateGradient(
      const SurrealS<N>* __restrict phis, DLA::VectorS<D, SurrealS<N>>& sgrad ) const;
  void evalReferenceCoordinateGradient(const Real* __restrict phis, VectorX& sgrad) const;

  template<class RefCoord>
  void evalBasisGradient( const RefCoord& sRef,
                          const ElementBasis<TopoD1, Line>& fldElem,
                          VectorX gradphi[], int nphi ) const;

  template<class RefCoord, class ElemT >
  void evalGradient( const RefCoord& sRef,
                     const Element< ElemT, TopoD1, Line >& qfldElem,
                     DLA::VectorS<PhysDim::D, ElemT>& gradq ) const;

  template<class RefCoord, class ElemT >
  void evalHessian( const RefCoord& sRef,
                    const Element< ElemT, TopoD1, Line >& qfldElem,
                    DLA::MatrixSymS<PhysDim::D, ElemT>& hessq ) const;

  using BaseType::nDOF_;
  using BaseType::DOF_;
  using BaseType::basis_;
  using BaseType::phi_;
  using BaseType::phis_;
  using BaseType::phiss_;
  using BaseType::pointStoreCell_;
  using BaseType::pointStoreTrace_;
  using BaseType::sgn_;
};

//----------------------------------------------------------------------------//
// computes the gradient of a solution
template <class PhysDim>
template< class ElemT >
void
ElementXFieldLineBase<PhysDim>::evalGradient( const Real& sRef,
                                              const Element< ElemT, TopoD1, Line >& qfldElem,
                                              DLA::VectorS<PhysDim::D, ElemT>& gradq ) const
{
  evalGradient<Real, ElemT>(sRef, qfldElem, gradq);
}

template <class PhysDim>
template< class ElemT >
void
ElementXFieldLineBase<PhysDim>::evalGradient( const RefCoordType& sRef,
                                              const Element< ElemT, TopoD1, Line >& qfldElem,
                                              DLA::VectorS<PhysDim::D, ElemT>& gradq ) const
{
  evalGradient<RefCoordType, ElemT>(sRef, qfldElem, gradq);
}

template <class PhysDim>
template< class ElemT >
void
ElementXFieldLineBase<PhysDim>::evalGradient( const QuadraturePoint<TopoD1>& sRef,
                                              const Element< ElemT, TopoD1, Line >& qfldElem,
                                              DLA::VectorS<PhysDim::D, ElemT>& gradq ) const
{
  evalGradient<QuadraturePoint<TopoD1>, ElemT>(sRef, qfldElem, gradq);
}

template <class PhysDim>
template< class ElemT >
void
ElementXFieldLineBase<PhysDim>::evalGradient( const QuadratureCellTracePoint<TopoD1>& sRef,
                                              const Element< ElemT, TopoD1, Line >& qfldElem,
                                              DLA::VectorS<PhysDim::D, ElemT>& gradq ) const
{
  evalGradient<QuadratureCellTracePoint<TopoD1>, ElemT>(sRef, qfldElem, gradq);
}

template <class PhysDim>
template<class RefCoord, class ElemT >
void
ElementXFieldLineBase<PhysDim>::evalGradient( const RefCoord& sRef,
                                              const Element< ElemT, TopoD1, Line >& qfldElem,
                                              DLA::VectorS<PhysDim::D, ElemT>& gradq ) const
{
  const int nBasis = qfldElem.nDOF();    // total solution DOFs in element
  std::vector<VectorX> gradphi(nBasis);  // Basis gradient

  evalBasisGradient( sRef, qfldElem, gradphi.data(), nBasis );
  qfldElem.evalFromBasis( gradphi.data(), nBasis, gradq );
}

//----------------------------------------------------------------------------//
// computes the Hessian of a solution
template <class PhysDim >
template< class ElemT >
void
ElementXFieldLineBase<PhysDim>::evalHessian( const RefCoordType& sRef,
                                             const Element< ElemT, TopoD1, Line >& qfldElem,
                                             DLA::MatrixSymS<PhysDim::D, ElemT>& hessq ) const
{
  evalHessian<RefCoordType, ElemT>(sRef, qfldElem, hessq);
}

template <class PhysDim >
template< class ElemT >
void
ElementXFieldLineBase<PhysDim>::evalHessian( const QuadraturePoint<TopoD1>& sRef,
                                             const Element< ElemT, TopoD1, Line >& qfldElem,
                                             DLA::MatrixSymS<PhysDim::D, ElemT>& hessq ) const
{
  evalHessian<QuadraturePoint<TopoD1>, ElemT>(sRef, qfldElem, hessq);
}

template <class PhysDim >
template<class RefCoord, class ElemT >
void
ElementXFieldLineBase<PhysDim>::evalHessian( const RefCoord& sRef,
                                             const Element< ElemT, TopoD1, Line >& qfldElem,
                                             DLA::MatrixSymS<PhysDim::D, ElemT>& hessq ) const
{
  SANS_ASSERT( PhysDim::D == 1 ); // TODO: currently only useful for in physically 1-D space; not for manifolds

  const int nBasis = qfldElem.nDOF();           // total solution DOFs in element
  std::vector<TensorSymX> hessphi(nBasis);      // Basis hessian

  evalBasisHessian( sRef, qfldElem, hessphi.data(), nBasis );
  qfldElem.evalFromBasis( hessphi.data(), nBasis, hessq );
}

//----------------------------------------------------------------------------//
// 2-D grid field element: line
//
// member functions:
//   .unitNormal                unit normal given reference line coordinate
//   .evalUnitTangentGradient   surface gradient of unit tangent vector, i.e. nablas(e_hat)
//----------------------------------------------------------------------------//

template<>
class ElementXField<PhysD2, TopoD1, Line> : public ElementXFieldLineBase< PhysD2 >
{
public:
  typedef ElementXFieldLineBase< PhysD2 > BaseType;

  typedef typename BaseType::VectorX VectorX;           // coordinates vector
  typedef DLA::VectorS<TopoD1::D, VectorX> LocalAxes;
  typedef typename DLA::VectorS<PhysD2::D, LocalAxes> VectorLocalAxes;
  typedef typename BaseType::RefCoordType RefCoordType;

  explicit ElementXField( const BasisFunctionLineBase* basis ) : BaseType(basis) {}
  ElementXField( int order, const BasisFunctionCategory& category ) : BaseType(order,category) {}
  ElementXField( const ElementXField& Elem ) : BaseType(Elem) {}
  ~ElementXField() {}

  ElementXField& operator=( const ElementXField& line ) { BaseType::operator=(line); return *this; }

  void unitNormal( const Real& sRef, Real& nx, Real& ny ) const;
  void unitNormal( const RefCoordType& sRef, VectorX& N ) const { unitNormal( sRef[0], N[0], N[1] ); }
  void unitNormal( const QuadraturePoint<TopoD1>& sRef, VectorX& N ) const;

  void evalUnitTangentGradient( const Real& sRef, VectorX& exgrad, VectorX& eygrad) const;
  void evalUnitTangentGradient( const Real& sRef, VectorLocalAxes& egrad) const
  {
    evalUnitTangentGradient(sRef, egrad[0][0], egrad[1][0]);
  }

  void evalUnitTangentGradient( const RefCoordType& sRef, VectorLocalAxes& egrad) const
  {
    evalUnitTangentGradient(sRef[0], egrad[0][0], egrad[1][0]);
  }
  void evalUnitTangentGradient( const QuadraturePoint<TopoD1>& ref, VectorLocalAxes& egrad) const
  {
    // TODO: Take advantage of quadrature cache
    evalUnitTangentGradient(ref.ref[0], egrad[0][0], egrad[1][0]);
  }
  void evalUnitTangentGradient( const QuadratureCellTracePoint<TopoD1>& ref, VectorLocalAxes& egrad) const
  {
    // TODO: Take advantage of quadrature cache
    evalUnitTangentGradient(ref.ref[0], egrad[0][0], egrad[1][0]);
  }

protected:
  void unitNormal( const Real phis[], Real& nx, Real& ny ) const;

  using BaseType::BaseType::nDOF_;
  using BaseType::BaseType::DOF_;
  using BaseType::BaseType::basis_;
  using BaseType::BaseType::phi_;
};


//----------------------------------------------------------------------------//
// 1-D/3-D/4-D grid field element: line
//----------------------------------------------------------------------------//
template<class PhysDim>
class ElementXField<PhysDim, TopoD1, Line> : public ElementXFieldLineBase< PhysDim >
{
public:
  typedef ElementXFieldLineBase< PhysDim > BaseType;

  typedef typename BaseType::VectorX VectorX;           // coordinates vector
  typedef typename BaseType::RefCoordType RefCoordType;

  explicit ElementXField( const BasisFunctionLineBase* basis ) : BaseType(basis) {}
  ElementXField( int order, const BasisFunctionCategory& category ) : BaseType(order,category) {}
  ElementXField( const ElementXField& Elem ) : BaseType(Elem) {}
  ~ElementXField() {}

  ElementXField& operator=( const ElementXField& line ) { BaseType::operator=(line); return *this; }
};

}

#endif //ELEMENTXFIELDLINE_H
