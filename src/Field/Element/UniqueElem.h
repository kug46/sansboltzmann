// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef UNIQUE_ELEM_H
#define UNIQUE_ELEM_H

#include <array>
#include <vector>
#include <ostream>

#include "Topology/ElementTopology.h"

#ifdef SANS_MPI
#include <boost/serialization/access.hpp>
#endif

//----------------------------------------------------------------------------//
// UniqueElem
//
// Provides a unique representation of an element based on the corner nodes.
// This is convenient to use as the key in for example std::map
//----------------------------------------------------------------------------//

namespace SANS
{

class UniqueElem
{
public:
  // Only uses the (corner) nodes of the element
  UniqueElem( const TopologyTypes topo, const std::vector<int>& nodes );
  // cppcheck-suppress noExplicitConstructor
  UniqueElem( const TopologyTypes topo, std::vector<int>&& nodes );

  UniqueElem( const int node0, const int node1 );

  UniqueElem( const TopologyTypes topo, const std::array<int,3>& nodes );

  UniqueElem();
  UniqueElem( const UniqueElem& elem );
  UniqueElem( UniqueElem&& elem );
  UniqueElem& operator=( const UniqueElem& elem );
  UniqueElem& operator=( UniqueElem&& elem );

  bool operator< ( const UniqueElem& elem ) const;
  bool operator==( const UniqueElem& elem ) const;

  friend std::ostream& operator<<( std::ostream& out, const UniqueElem& elem );

  const std::vector<int>& sortedNodes() const { return sortedNodes_; }

  const TopologyTypes topo;      // Topology of the element
protected:
  std::vector<int> sortedNodes_; // The sorted nodes of the trace

#ifdef SANS_MPI
  friend class boost::serialization::access;

  template<class Archive>
  void serialize(Archive & ar, const unsigned int version)
  {
    ar & const_cast<TopologyTypes&>(topo);
    ar & sortedNodes_;
  }
#endif
};

} // namespace SANS

#endif // UNIQUE_ELEM_H
