// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "ElementAssociativityLine.h"
#include "tools/SANSException.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// edge associativity constructor: global numberings for element nodes/edges

// NOTE: default ctor needed for 'new []'
ElementAssociativity<TopoD1,Line>::ElementAssociativity()
{
  rank_  = -1;
  order_ = -1;
  nNode_ = 0;
  nEdge_ = 0;

  nodeList_ = NULL;
  edgeList_ = NULL;
}


ElementAssociativity<TopoD1,Line>::ElementAssociativity( int order )
{
  const BasisFunctionLineBase* basis;
  switch (order)
  {
  case 1:
    basis = BasisFunctionLineBase::HierarchicalP1;
    break;
  case 2:
    basis = BasisFunctionLineBase::HierarchicalP2;
    break;
  case 3:
    basis = BasisFunctionLineBase::HierarchicalP3;
    break;
  case 4:
    basis = BasisFunctionLineBase::HierarchicalP4;
    break;
  default:
    SANS_DEVELOPER_EXCEPTION( "Unknown Hierarchical basis function order: %d", order );
    break;
  }

  rank_  = -1;
  order_ = order;
  nNode_ = basis->nBasisNode();
  nEdge_ = basis->nBasisEdge();

  if (nNode_ == 0)
    nodeList_ = NULL;
  else
    nodeList_ = new int[nNode_];

  if (nEdge_ == 0)
    edgeList_ = NULL;
  else
    edgeList_ = new int[nEdge_];
}


ElementAssociativity<TopoD1,Line>::ElementAssociativity( const BasisType* basis )
{
#if 0
  std::cout << "in ElementAssociativity<TopoD1,Line>(basis): basis =" << std::endl;
  basis.dump( 2 );
  std::cout << "  basis.nBasisNode() = " << basis.nBasisNode()
            << "  basis.nBasisEdge() = " << basis.nBasisEdge() << std::endl;
#endif
  rank_  = -1;
  order_ = basis->order();
  nNode_ = basis->nBasisNode();
  nEdge_ = basis->nBasisEdge();

  if (nNode_ == 0)
    nodeList_ = NULL;
  else
    nodeList_ = new int[nNode_];

  if (nEdge_ == 0)
    edgeList_ = NULL;
  else
    edgeList_ = new int[nEdge_];

#if 0
  std::cout << "  nodeList_ = " << nodeList_ << "  edgeList_ = " << edgeList_ << std::endl;
#endif
}


ElementAssociativity<TopoD1,Line>::ElementAssociativity( const ElementAssociativity<TopoD1,Line>& a )
{
  rank_  = a.rank_;
  order_ = a.order_;
  nNode_ = a.nNode_;
  nEdge_ = a.nEdge_;

  if (nNode_ == 0)
  {
    nodeList_ = NULL;
  }
  else
  {
    nodeList_ = new int[nNode_];
    const int nNode = nNode_;
    for (int n = 0; n < nNode; n++)
      nodeList_[n] = a.nodeList_[n];
  }

  if (nEdge_ == 0)
  {
    edgeList_ = NULL;
  }
  else
  {
    edgeList_ = new int[nEdge_];
    const int nEdge = nEdge_;
    for (int n = 0; n < nEdge; n++)
      edgeList_[n] = a.edgeList_[n];
  }
}


ElementAssociativity<TopoD1,Line>::~ElementAssociativity()
{
#if 0
  std::cout << "in ~ElementAssociativity<TopoD1,Line>()" << std::endl;
  std::cout << "  nodeList_ = " << nodeList_ << "  edgeList_ = " << edgeList_ << std::endl;
#endif
  delete [] nodeList_;
  delete [] edgeList_;
}


ElementAssociativity<TopoD1,Line>&
ElementAssociativity<TopoD1,Line>::operator=( const ElementAssociativity<TopoD1,Line>& a )
{
  if (this != &a)
  {
    rank_  = a.rank_;
    order_ = a.order_;
    nNode_ = a.nNode_;
    nEdge_ = a.nEdge_;

    if (nNode_ == 0)
    {
      nodeList_ = NULL;
    }
    else
    {
      delete [] nodeList_;
      nodeList_ = new int[nNode_];
      const int nNode = nNode_;
      for (int n = 0; n < nNode; n++)
        nodeList_[n] = a.nodeList_[n];
    }

    if (nEdge_ == 0)
    {
      edgeList_ = NULL;
    }
    else
    {
      delete [] edgeList_;
      edgeList_ = new int[nEdge_];
      const int nEdge = nEdge_;
      for (int n = 0; n < nEdge; n++)
        edgeList_[n] = a.edgeList_[n];
    }
  }

  return *this;
}


ElementAssociativity<TopoD1,Line>&
ElementAssociativity<TopoD1,Line>::operator=( const Constructor& a )
{

  rank_  = a.rank();
  order_ = a.order();
  nNode_ = a.nNode();
  nEdge_ = a.nEdge();

  if (nNode_ == 0)
  {
    delete [] nodeList_; nodeList_ = NULL;
  }
  else
  {
    delete [] nodeList_;
    nodeList_ = new int[nNode_];
    for (int n = 0; n < nNode_; n++)
      nodeList_[n] = a.nodeGlobal(n);
  }

  if (nEdge_ == 0)
  {
    delete [] edgeList_; edgeList_ = NULL;
  }
  else
  {
    delete [] edgeList_;
    edgeList_ = new int[nEdge_];
    for (int n = 0; n < nEdge_; n++)
      edgeList_[n] = a.edgeGlobal(n);
  }

  return *this;
}

void
ElementAssociativity<TopoD1,Line>::getNodeGlobalMapping( int node[], int nnode ) const
{
  if (nnode!=nNode_) printf("nnode = %d, nNode_ = %d\n",nnode,nNode_);
  SANS_ASSERT( nnode == nNode_ );

  for (int n = 0; n < nNode_; n++)
    node[n] = nodeList_[n];
}

void
ElementAssociativity<TopoD1,Line>::getNodeGlobalMapping( std::vector<int>& node ) const
{
  SANS_ASSERT( (int)node.size() == nNode_ );

  for (int n = 0; n < nNode_; n++)
    node[n] = nodeList_[n];
}

void
ElementAssociativity<TopoD1,Line>::getEdgeGlobalMapping( int edge[], int nedge ) const
{
  SANS_ASSERT_MSG( nedge == nEdge_, "nedge = %d, nEdge_ = %d\n", nedge, nEdge_ );

  for (int n = 0; n < nEdge_; n++)
    edge[n] = edgeList_[n];
}

void
ElementAssociativity<TopoD1,Line>::getEdgeGlobalMapping( std::vector<int>& edge ) const
{
  SANS_ASSERT_MSG( (int)edge.size() == nEdge_, "edge.size() = %d, nEdge_ = %d\n", edge.size(), nEdge_ );

  for (int n = 0; n < nEdge_; n++)
    edge[n] = edgeList_[n];
}

void
ElementAssociativity<TopoD1,Line>::getGlobalMapping( int map[], int ndof ) const
{
//  std::cout << " ndof: " << ndof << " nNode_: " << nNode_ << " nEdge_: " << nEdge_ << std::endl;
#if 0
  std::cout << "ElementAssociativity<TopoD1,Line>::getGlobalMapping  dumping..." << std::endl;
  this->dump(2);
#endif
  SANS_ASSERT( ndof == nNode_ + nEdge_ );

  int offset = 0;
  for (int n = 0; n < nNode_; n++)
    map[n + offset] = nodeList_[n];
  offset += nNode_;

  for (int n = 0; n < nEdge_; n++)
    map[n + offset] = edgeList_[n];
}


void
ElementAssociativity<TopoD1,Line>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "ElementAssociativity<TopoD1>: order_ = " << order_
      << "  nNode_ = " << nNode_
      << "  nEdge_ = " << nEdge_ << std::endl;
  if ( nNode_ > 0 )
  {
    out << indent << indent << "nodeList = ";
    for (int n = 0; n < nNode_; n++)
      out << nodeList_[n] << " ";
    out << std::endl;
  }
  if ( nEdge_ > 0 )
  {
    out << indent << indent << "edgeList = ";
    for (int n = 0; n < nEdge_; n++)
      out << edgeList_[n] << " ";
    out << std::endl;
  }
}

}
