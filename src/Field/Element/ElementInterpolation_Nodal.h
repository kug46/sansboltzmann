// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ELEMENTINTERPOLATION_NODAL_H
#define ELEMENTINTERPOLATION_NODAL_H

// Element solution interpolation: Nodal

#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "BasisFunction/BasisFunctionBase.h"
#include "BasisFunction/BasisFunctionLine_Lagrange.h"
#include "BasisFunction/BasisFunctionArea_Quad_Lagrange.h"
#include "BasisFunction/BasisFunctionArea_Triangle_Lagrange.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron_Lagrange.h"
#include "BasisFunction/BasisFunctionVolume_Hexahedron_Lagrange.h"

#include "Topology/ElementTopology.h"

#include "Field/Element/Element.h"

#include "LinearAlgebra/DenseLinAlg/InverseLU.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Element solution interpolation: Nodal
//
//  qexact(x,y) = q(x,y) at Lagrange nodes,  q(x,y) = sum q_k phi_k(x,y)

template<class TopoDim, class Topology>
class ElementInterpolation_Nodal
{
public:
  typedef typename BasisFunctionTopologyBase<TopoDim, Topology>::type BasisType;
  typedef typename Element<Real, TopoDim, Topology>::RefCoordType RefCoordType;

  // use max quadrature to integrate the solution as well as possible
  explicit ElementInterpolation_Nodal(const BasisType* basis):
        basis_(basis),
        nDOF_(basis_->nBasis()),
        phi_(nDOF_)
  {
    LagrangeNodes<Topology>::get(basis_->order(), sRef_);
  }

  // Functions
  template <class PhysDim, class T, class Functor>
  void
  interpolate( const ElementXField<PhysDim, TopoDim, Topology>& xfldElem,
               Element<T, TopoDim, Topology>& qfldElem,
               const Functor& solnExact )
  {
    typedef typename ElementXField<PhysDim, TopoDim, Topology>::VectorX VectorX;

    SANS_ASSERT( qfldElem.basis() == basis_ );
    const int nDOF = nDOF_;

    VectorX X;                  // physical coordinates
    T qexact;                   // exact solution

    DLA::VectorD<T> rhs( nDOF );

    DLA::MatrixD<Real> interpMat(nDOF, nDOF);

    rhs = 0;

    // loop over Lagrange points
    for (int i = 0; i < nDOF; i++)
    {
      // physical coordinates
      xfldElem.coordinates( sRef_[i], X );

      // Evaluate the solution function at the physical coordinate
      qexact = solnExact( X );

      rhs[i] = qexact;

      // evaluate basis function at Lagrange nodes (note Hierarchical basis might be different from element to element)
      qfldElem.evalBasis( sRef_[i], phi_.data(), phi_.size() );

      for (int j = 0; j < nDOF; j++)
        interpMat(i,j) = phi_[j];
    }

    qfldElem.vectorViewDOF() = SANS::DLA::InverseLU::Solve(interpMat, rhs);
  }

  // Constants
  template <class PhysDim, class T >
  void
  interpolate( const ElementXField<PhysDim, TopoDim, Topology>& xfldElem,
               Element<T, TopoDim, Topology>& qfldElem,
               const T& q0 )
  {
    typedef typename ElementXField<PhysDim, TopoDim, Topology>::VectorX VectorX;

    SANS_ASSERT( qfldElem.basis() == basis_ );
    const int nDOF = nDOF_;

    VectorX X;                  // physical coordinates
    T qexact;                   // exact solution

    DLA::VectorD<T> rhs( nDOF );

    DLA::MatrixD<Real> interpMat(nDOF, nDOF);

    rhs = 0;

    // loop over Lagrange points
    for (int i = 0; i < nDOF; i++)
    {
      // physical coordinates
      xfldElem.coordinates( sRef_[i], X );

      rhs[i] = q0;

      // evaluate basis function at Lagrange nodes (note Hierarchical basis might be different from element to element)
      qfldElem.evalBasis( sRef_[i], phi_.data(), phi_.size() );

      for (int j = 0; j < nDOF; j++)
        interpMat(i,j) = phi_[j];
    }

    qfldElem.vectorViewDOF() = SANS::DLA::InverseLU::Solve(interpMat, rhs);
  }

protected:
  const BasisType* basis_;
  const int nDOF_;
  std::vector<Real> phi_;
  std::vector<RefCoordType> sRef_;
};

}

#endif  // ELEMENTINTERPOLATION2D_NODAL_H
