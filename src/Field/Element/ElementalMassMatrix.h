// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ELEMENTAL_MASSMATRIX_H
#define ELEMENTAL_MASSMATRIX_H

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/BasisFunctionBase.h"

#include "Quadrature/Quadrature.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"

#include "Element.h"

namespace SANS
{

template<class TopoDim, class Topology>
class ElementalMassMatrix
{
public:
  typedef typename BasisFunctionTopologyBase<TopoDim, Topology>::type BasisType;
  typedef QuadraturePoint<TopoDim> QuadPointType;

  template<class PhysDim>
  ElementalMassMatrix( const ElementXField<PhysDim, TopoDim, Topology>& xfldElem,
                       const ElementBasis<TopoDim, Topology>& qfldElem ) :
    nBasis_( qfldElem.basis()->nBasis() ),
    // Note that the quadrature order is chosen to match the polynomial order of the integrand (including both solution q and grid jacobian)
    quadrature_( 2*qfldElem.basis()->order() + max(TopoDim::D*(xfldElem.basis()->order() - 1), 0) ),
    phi_( new Real[quadrature_.nQuadrature()*nBasis_] )
  {
    const int nBasis = nBasis_;

    // loop over quadrature points
    const int nquad = quadrature_.nQuadrature();
    for (int iquad = 0; iquad < nquad; iquad++)
    {
      // cached reference-element coordinates
      QuadPointType sRef = quadrature_.coordinates_cache( iquad );

      // save off the basis functions at the quadrature points
      qfldElem.evalBasis( sRef, phi_ + iquad*nBasis, nBasis );
    }
  }

  ~ElementalMassMatrix() { delete [] phi_; }

  ElementalMassMatrix(const ElementalMassMatrix&) = delete;
  ElementalMassMatrix& operator=(const ElementalMassMatrix&) = delete;

  template<class PhysDim>
  void operator()(const ElementXField<PhysDim, TopoDim, Topology>& xfldElem,
                  DLA::MatrixD<Real>& mtx) const
  {
    const int nBasis = nBasis_;
    Real weight, dJ;

    mtx = 0;
    // loop over quadrature points and compute the scalar mass matrix
    const int nquad = quadrature_.nQuadrature();
    for (int iquad = 0; iquad < nquad; iquad++)
    {
      quadrature_.weight( iquad, weight );
      QuadPointType sRef = quadrature_.coordinates_cache( iquad );

      dJ = weight * xfldElem.jacobianDeterminant( sRef );

      for (int i = 0; i < nBasis; i++)
        for (int j = 0; j < nBasis; j++)
          mtx(i,j) += dJ*phi_[iquad*nBasis+i]*phi_[iquad*nBasis+j];
    }
  }

  template<class PhysDim, int M>
  void operator()(const ElementXField<PhysDim, TopoDim, Topology>& xfldElem,
                  DLA::MatrixD< DLA::MatrixS<M,M,Real> >& mtx) const
  {
    const int nBasis = nBasis_;
    Real weight, dJ;

    mtx = 0;
    // loop over quadrature points and compute the diagonal mass matrix
    const int nquad = quadrature_.nQuadrature();
    for (int iquad = 0; iquad < nquad; iquad++)
    {
      quadrature_.weight( iquad, weight );
      QuadPointType sRef = quadrature_.coordinates_cache( iquad );

      dJ = weight * xfldElem.jacobianDeterminant( sRef );

      for (int i = 0; i < nBasis; i++)
        for (int j = 0; j < nBasis; j++)
        {
          Real tmp = dJ*phi_[iquad*nBasis+i]*phi_[iquad*nBasis+j];
          for (int m = 0; m < M; m++)
            mtx(i,j)(m,m) += tmp;
        }
    }
  }

  template<class PhysDim, int M>
  void operator()(const ElementXField<PhysDim, TopoDim, Topology>& xfldElem,
                  DLA::MatrixD< DLA::MatrixS<PhysDim::D,PhysDim::D,DLA::MatrixS<M,M,Real>> >& mtx) const
  {
    const int nBasis = nBasis_;
    Real weight, dJ;

    mtx = 0;
    // loop over quadrature points and compute the diagonal mass matrix
    const int nquad = quadrature_.nQuadrature();
    for (int iquad = 0; iquad < nquad; iquad++)
    {
      quadrature_.weight( iquad, weight );
      QuadPointType sRef = quadrature_.coordinates_cache( iquad );

      dJ = weight * xfldElem.jacobianDeterminant( sRef );

      for (int i = 0; i < nBasis; i++)
        for (int j = 0; j < nBasis; j++)
        {
          Real tmp = dJ*phi_[iquad*nBasis+i]*phi_[iquad*nBasis+j];
          for (int d = 0; d < PhysDim::D; d++)
            for (int m = 0; m < M; m++)
              mtx(i,j)(d,d)(m,m) += tmp;
        }
    }
  }

protected:
  const int nBasis_;
  Quadrature<TopoDim, Topology> quadrature_;
  Real *phi_;
};

}

#endif // ELEMENTAL_MASSMATRIX_H
