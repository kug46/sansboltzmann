// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef TRACEUNITNORMAL_H
#define TRACEUNITNORMAL_H

#include <cmath>  // sqrt
#include <limits> // std::numeric_limits

#include "BasisFunction/Quadrature_Cache.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/tools/cross.h"
#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "Field/Element/Element.h"

#include "Topology/Dimension.h"

#include "tools/SANSException.h"
#include "tools/minmax.h"

namespace SANS
{

// TODO: This function has not been unit tested

// A function for computing the unit normal on a trace element with specializations for 2D/3D manifolds

//=============================================================================
// Equal physical and topological dimensions
template<class PhysDim, class TopoDimCell, class TopologyL, class TopoDimTrace, class TopologyTrace>
void traceUnitNormal( const ElementXField<PhysDim, TopoDimCell , TopologyL    >& xfldElem,
                      const QuadratureCellTracePoint<TopoDimCell>& sRef,
                      const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
                      const QuadraturePoint<TopoDimTrace>& sRefTrace,
                      DLA::VectorS<PhysDim::D, Real>& N)
{
  // unit normal: points out of domain
  xfldElemTrace.unitNormal( sRefTrace, N );
}

//=============================================================================
// [3D manifold: area elements in physically 3D space] Shell trace unit normal
template<class TopologyL, class TopologyTrace>
void traceUnitNormal( const ElementXField<PhysD3, TopoD2, TopologyL    >& xfldElemL,
                      const QuadratureCellTracePoint<TopoD2>& sRef,
                      const ElementXField<PhysD3, TopoD1, TopologyTrace>& xfldElemTrace,
                      const QuadraturePoint<TopoD1>& sRefTrace,
                      DLA::VectorS<PhysD3::D, Real>& N)
{
  DLA::VectorS<PhysD3::D, Real> Xs, NL;

  // Get the normal vector on the left cell
  xfldElemL.unitNormal(sRef, NL);

  // Get the trace tangent
  xfldElemTrace.unitTangent( sRefTrace, Xs);

  // Compute the 'cell-to-cell' normal from the left element
  N = cross( Xs, NL );
}

// traceUnitNormal = orthogonal to bisector of tangent vectors of left and right cells & pointing from left to right
template<class TopologyTrace, class TopologyL, class TopologyR>
void traceUnitNormal( const ElementXField<PhysD3, TopoD1, TopologyTrace>& xfldElemTrace,
                      const QuadraturePoint<TopoD1>& sRefTrace,
                      const ElementXField<PhysD3, TopoD2, TopologyL    >& xfldElemL,
                      const QuadratureCellTracePoint<TopoD2>& sRefCellL,
                      const ElementXField<PhysD3, TopoD2, TopologyR    >& xfldElemR,
                      const QuadratureCellTracePoint<TopoD2>& sRefCellR,
                      DLA::VectorS<PhysD3::D, Real>& N)
{
  // NL:    outward-pointing trace normal vector of the left element
  // NR:    outward-pointing trace normal vector of the right element
  DLA::VectorS<PhysD3::D, Real> Xs, NL, NR;

  // Get the surface normal vector on the left/right cells (i.e. the tangent vector of the cell)
  xfldElemL.unitNormal(sRefCellL, NL);
  xfldElemR.unitNormal(sRefCellR, NR);

  // Get the trace tangent
  xfldElemTrace.unitTangent( sRefTrace, Xs);

  // Compute the 'cell-to-cell' normal from the left/right elements
  NL = cross( Xs, NL );
  NR = cross( Xs, NR );

  N  = 0.5*(NL - NR);    // orthogonal to bisector of NL and NR;
  N /= sqrt( dot(N,N) ); // normalize

  //TODO: this assertion is left here until unit test is implemented
  SANS_ASSERT_MSG( (dot(N,N) - 1) < 10*std::numeric_limits<Real>::epsilon(), "Error: unitTraceNormal is not normalized." );
}

//=============================================================================
// [2D manifold: line elements in physically 2D space] Line trace unit normal (i.e. the line tangent)

// traceUnitNormal = unit tangent vector of left cell
template<class TopologyL, class TopologyTrace>
void traceUnitNormal( const ElementXField<PhysD2, TopoD1, TopologyL    >& xfldElemL,
                      const QuadratureCellTracePoint<TopoD1>& sRef,
                      const ElementXField<PhysD2, TopoD0, TopologyTrace>& xfldElemTrace,
                      const QuadraturePoint<TopoD0>& sRefTrace,
                      DLA::VectorS<PhysD2::D, Real>& N)
{
  // Get the trace normal vector on the left cell (i.e. the tangent vector of the cell)
  xfldElemL.unitTangent(sRef, N);

  // Correct the sign of the tangent
  N *= xfldElemTrace.conormalSignL(TopologyTrace::centerRef);
}

// traceUnitNormal = orthogonal to bisector of tangent vectors of left and right cells & pointing from left to right
template<class TopologyTrace, class TopologyL, class TopologyR>
void traceUnitNormal( const ElementXField<PhysD2, TopoD0, TopologyTrace>& xfldElemTrace,
                      const QuadraturePoint<TopoD0>& sRefTrace,
                      const ElementXField<PhysD2, TopoD1, TopologyL    >& xfldElemL,
                      const QuadratureCellTracePoint<TopoD1>& sRefCellL,
                      const ElementXField<PhysD2, TopoD1, TopologyR    >& xfldElemR,
                      const QuadratureCellTracePoint<TopoD1>& sRefCellR,
                      DLA::VectorS<PhysD2::D, Real>& N)
{
  // NL:    outward-pointing trace normal vector of the left element
  // NR:    outward-pointing trace normal vector of the right element
  DLA::VectorS<PhysD2::D, Real> NL, NR;

  // Get the trace normal vector on the left/right cells (i.e. the tangent vector of the cell)
  xfldElemL.unitTangent(sRefCellL, NL);
  xfldElemR.unitTangent(sRefCellR, NR);

  // Correct the sign of the tangent
  NL *= xfldElemTrace.conormalSignL(TopologyTrace::centerRef);
  NR *= xfldElemTrace.conormalSignR(TopologyTrace::centerRef);

  N  = 0.5*(NL - NR);    // orthogonal to bisector of NL and NR;
  N /= sqrt( dot(N,N) ); // normalize
}

// traceUnitNormal = separately defined as tangents to left and right element respectively, with both pointing from left to right
template<class TopologyTrace, class TopologyL, class TopologyR>
void traceUnitNormal( const ElementXField<PhysD2, TopoD0, TopologyTrace>& xfldElemTrace,
                      const QuadraturePoint<TopoD0>& sRefTrace,
                      const ElementXField<PhysD2, TopoD1, TopologyL    >& xfldElemL,
                      const QuadratureCellTracePoint<TopoD1>& sRefCellL,
                      const ElementXField<PhysD2, TopoD1, TopologyR    >& xfldElemR,
                      const QuadratureCellTracePoint<TopoD1>& sRefCellR,
                      DLA::VectorS<PhysD2::D, Real>& NL, DLA::VectorS<PhysD2::D, Real>& NR)
{
  // NL:    outward-pointing trace normal vector of the left element
  // NR:     inward-pointing trace normal vector of the right element

  // Get the trace normal vector on the left/right cells (i.e. the tangent vector of the cell)
  xfldElemL.unitTangent(sRefCellL, NL);
  xfldElemR.unitTangent(sRefCellR, NR);

  // Correct the sign of the tangent
  NL *=  xfldElemTrace.conormalSignL(TopologyTrace::centerRef);
  NR *= -xfldElemTrace.conormalSignR(TopologyTrace::centerRef);
}

} // namespace SANS

#endif //TRACEUNITNORMAL_H
