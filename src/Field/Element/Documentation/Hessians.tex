\documentclass[12pt]{article}

\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage{times}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}

\parindent 0pt   % paragraph indentation
%\setlength\parindent{0pt} % Removes all indentation from paragraphs
\parskip 10pt     % paragraph spacing

\renewcommand{\d}{{\rm d}}
\newcommand{\lhl}{\mbox{---}} % long horizontal line
\newcommand{\lrp}[1]{\left( #1 \right)}

\begin{document}
\section{Introduction}
This document introduces the definition of Hessians and its implementation in \texttt{SANS}. Note that the formulation is restricted to the case of equal physical and topological dimensions, i.e. not applicable to manifolds in general.

\section{Hessian}
We start with the 2D formulation and then generalize it to higher dimensions.

\subsection{2D}
Let $\phi$ be a smooth function (e.g. a polynomial basis function). Denote partial differentiation with subscripts. For example, $\phi_x := \partial \phi/ \partial x$ and $\phi_{xx} := \partial^2 \phi/ \partial x^2$.
\begin{align}
\phi (x,y) &= \phi(s,t)\\
\phi_x &= \phi_s s_x + \phi_t t_x\\
\phi_y &= \phi_s s_y + \phi_t t_y\\	
\phi_{xx} &= \phi_{s} s_{xx}  + (\phi_{ss} s^2_x + \phi_{st} s_x t_x) + \phi_t t_{xx}  + (\phi_{tt} t^2_x + \phi_{st} s_x t_x)
\nonumber\\
		  &= \phi_{s} s_{xx}  + \phi_t t_{xx} + (\phi_{ss} s^2_x + 2\phi_{st} s_x t_x + \phi_{tt} t^2_x)\\
%
\phi_{xy} &= \phi_{s} s_{xy}  + (\phi_{ss} s_x s_y + \phi_{st} s_x t_y) + \phi_t t_{xy}  + (\phi_{tt} t_x t_y + \phi_{st} s_y t_x)
\nonumber\\
		  &= \phi_{s} s_{xy}  + \phi_t t_{xy} + (\phi_{ss} s_x s_y + \phi_{st}(s_x t_y+ s_y t_x) + \phi_{tt} t_x t_y) \\
%
\phi_{yx} &= \phi_{xy}
\label{eq:symSecondDerivative}
\\
%
\phi_{yy} &= \phi_{s} s_{yy}  + (\phi_{ss} s^2_y + \phi_{st} s_y t_y) + \phi_t t_{yy}  + (\phi_{tt} t^2_y + \phi_{st} s_y t_y)
\nonumber\\
		  &= \phi_{s} s_{yy}  + \phi_t t_{yy} + (\phi_{ss} s^2_y + 2\phi_{st} s_y t_y + \phi_{tt} t^2_y)
\end{align}
Note that Equation~(\ref{eq:symSecondDerivative}) uses the symmertry of second derivatives or Schwarz' theorem or Young's theorem or Clairaut's theorem.

Jacobians:
\begin{align}
J := J^{(x,y)}_{(s,t)}      &:= \begin{bmatrix} x_s & x_t \\ y_s & y_t \end{bmatrix} = \begin{bmatrix} J^x_{(s,t)} \\ J^y_{(s,t)}\end{bmatrix} \\
\Rightarrow J^{-1} = J^{(s,t)}_{(x,y)}   &:= \begin{bmatrix} s_x & s_y \\  t_x & t_y \end{bmatrix} = \begin{bmatrix} J^s_{(x,y)}  \\  J^t_{(x,y)} \end{bmatrix}
\end{align}
where $J$ denotes the Jacobians. For example, $J^x_{(s,t)} := [x_s, x_t]$.

Hessians:
\begin{align}
H^\phi_{(x,y)} &= \begin{bmatrix} \phi_{xx} & \phi_{xy} \\ \phi_{xy} & \phi_{yy} \end{bmatrix} \qquad
H^\phi_{(s,t)}  = \begin{bmatrix} \phi_{ss} & \phi_{st} \\ \phi_{st} & \phi_{tt} \end{bmatrix}\\
H^s_{(x,y)} &= \begin{bmatrix} s_{xx} & s_{xy} \\ s_{xy} & s_{yy} \end{bmatrix} \qquad
H^x_{(s,t)}  = \begin{bmatrix} x_{ss} & x_{st} \\ x_{st} & x_{tt} \end{bmatrix}\\
H^t_{(x,y)} &= \begin{bmatrix} t_{xx} & t_{xy} \\ t_{xy} & t_{yy} \end{bmatrix} \qquad
H^y_{(s,t)}  = \begin{bmatrix} y_{ss} & y_{st} \\ y_{st} & y_{tt} \end{bmatrix}
\end{align}
Note that the base functions are assumed to be at least second-order continuously differentiable so that the Hessians are symmetric.

Our goal is to obtain a formula for $H^\phi_{(x,y)}$. Notice that:
%
\begin{align}
&J^{-T} H^\phi_{(s,t)} J^{-1} \\
&= \begin{bmatrix} s_{x} & t_{x} \\ s_{y} & t_{y} \end{bmatrix} 
   \begin{bmatrix} \phi_{ss} & \phi_{st} \\ \phi_{st} & \phi_{tt} \end{bmatrix} 
   \begin{bmatrix} s_{x} & s_{y} \\ t_{x} & t_{y} \end{bmatrix} \\
&= \begin{bmatrix} 
	s_x \phi_{ss} + t_x \phi_{st} & s_x \phi_{st} + t_x \phi_{tt} \\
	s_y \phi_{ss} + t_y \phi_{st} & s_y \phi_{st} + t_y \phi_{tt} 
   \end{bmatrix} \begin{bmatrix} s_{x} & s_{y} \\ t_{x} & t_{y} \end{bmatrix}\\
   &= \begin{bmatrix}
   \phi_{ss} s^2_x + \phi_{st} 2 s_x t_x + \phi_{tt} t^2_x & \phi_{ss} s_x s_y + \phi_{st} (s_y t_x + s_x t_y) + \phi_{tt} t_x t_y  \\
   \phi_{ss} s_x s_y + \phi_{st} (s_x t_y + s_y t_x) + \phi_{tt} t_x t_y & \phi_{ss} s^2_y + \phi_{st} 2 s_y t_y + \phi_{tt} t^2_y 
   \end{bmatrix} 
\end{align}
%
and
%
\begin{align}
H^\phi_{(x,y)} - J^{-T}H^\phi_{(s,t)}J^{-1}
&= \begin{bmatrix} 
\phi_s s_{xx} + \phi_t t_{xx} & \phi_{s} s_{xy}  + \phi_t t_{xy}\\
\phi_{s} s_{xy}  + \phi_t t_{xy} & \phi_{s} s_{yy}  + \phi_t t_{yy}
\end{bmatrix}\\
&= \phi_s \begin{bmatrix} s_{xx} & s_{xy} \\ s_{xy} & s_{yy} \end{bmatrix} + \phi_t \begin{bmatrix} t_{xx} & t_{xy} \\ t_{xy} & t_{yy} \end{bmatrix}\\
\end{align}
%
Therefore,
\begin{align}
H^\phi_{(x,y)} &= \begin{bmatrix} s_{x} & t_{x} \\ s_{y} & t_{y} \end{bmatrix} 
		  \begin{bmatrix} \phi_{ss} & \phi_{st} \\ \phi_{ts} & \phi_{tt} \end{bmatrix} 
		  \begin{bmatrix} s_{x} & s_{y} \\ t_{x} & t_{y} \end{bmatrix}
		- \phi_s \begin{bmatrix} s_{xx} & s_{xy} \\ s_{xy} & s_{yy} \end{bmatrix} + \phi_t \begin{bmatrix} t_{xx} & t_{xy} \\ t_{xy} & t_{yy} \end{bmatrix}  \\
&= J^{-T}H^\phi_{(s,t)}J^{-1} + \phi_s H^s_{(x,y)} + \phi_t H^t_{(x,y)}
\end{align}

\subsection{Generalization}
Using index notation,  the formula for 2D Hessian generalizes to n-Dimensions:
\begin{equation}
H^\phi_{\vec{x}} = J^{-T}H^\phi_{\vec{\xi}}J^{-1} + \sum_i \phi_{\xi_i} H^{\xi_i}_{\vec{x}} \label{eq:Hess_Calc}
\end{equation}
%
where $\vec{x} := [x_1, x_2, \ldots]$ denotes the set of basis directions for the transformed coordinates (e.g. Cartesian coordinates $x,y,z$ in 3D) , and $\vec{\xi} := [\xi_1, \xi_2, \ldots]$ denotes the set of basis for the original coordinates (e.g. reference coordinates $s,t,u$ in 3D)

\section{Implementation}

\subsection{\texttt{evalReferenceCoordinateGradient}}
This function returns the partial derivatives of the master element coordinates (i.e. reference coordinates) with respect to the Cartesian coordinates. (The term \texttt{Gradient} refers to derivatives with respect to Cartesian coordinates.)

In 1D, the output is a scalar:
\begin{equation}
\nabla s := \frac{\d s}{\d x} = \lrp{ \frac{d x}{\d s} }^{-1} = \frac{1}{\frac{\partial x}{\partial s}}
\end{equation}

In 2D, \texttt{evalReferenceCoordinateGradient} returns two columnn vectors $\nabla s$ and $\nabla t$:
\begin{equation}
\begin{bmatrix} \lhl \nabla s \lhl \\  \lhl \nabla t \lhl \end{bmatrix} 
:= \begin{bmatrix} \frac{\partial s}{\partial x} & \frac{\partial s}{\partial y} \\  \frac{\partial t}{\partial x} & \frac{\partial t}{\partial y} \end{bmatrix} 
= \begin{bmatrix} \frac{\partial x}{\partial s} & \frac{\partial x}{\partial t} \\ \frac{\partial y}{\partial s} & \frac{\partial y}{\partial t} \end{bmatrix}^{-1} 
\end{equation}

\subsection{\texttt{evalReferenceCoordinateHessian}}
This function returns the tensor of the second partial derivatives of the master element coordinates with respect to the cartesian coordinates. To get this formula, evaluate Equation~(\ref{eq:Hess_Calc}) where $\phi=x_k, k =1,\ldots,D$, i.e. to take the second derivatives of the cartesian coordinates with respect to themselves, and then invert the resulting expression to obtain $H^i_j$. 
%
\begin{align}
H^{x_k}_{\vec{x}} = 0 &= J^{-T} H^{x_k}_{\vec{\xi}} J^{-1} + \sum_i (x_k)_{\xi_i} H^{\xi_i}_{\vec{x}}
\end{align}
%
This will then form a system of $D$ equations of rank 2 tensors.

In 1D:
\begin{align}
x_{xx} = 0 &= x_{ss} s^2_x + x_s s_{xx}\\
s_{xx} &= - \frac{x_{ss} s^2_x}{x_s} \\
&= -\frac{x_{ss}}{x^3_s}
\end{align}
The last line is exploiting $\frac{\partial s}{\partial x} = \frac{1}{\frac{\partial x}{\partial s}}$, which is implemented for 1D \texttt{ElementXField}.

In 2D:
\begin{align}
\begin{bmatrix} H^x_{(x,y)} \\ H^y_{(x,y)} \end{bmatrix} = \begin{bmatrix} 0 \\ 0 \end{bmatrix} &= \begin{bmatrix} J^{-T} H^x_{(s,t)} J^{-1} \\ J^{-T} H^y_{(s,t)} J^{-1} \end{bmatrix}  +  \begin{bmatrix} x_s & x_t \\ y_s & y_t \end{bmatrix}  \begin{bmatrix} H^s_{(x,y)} \\ H^t_{(x,y)} \end{bmatrix}
\\
\Rightarrow \begin{bmatrix} H^s_{(x,y)} \\ H^t_{(x,y)} \end{bmatrix} &= - \frac{1}{x_s y_t - x_t y_s}\begin{bmatrix} y_t & -x_t \\ -y_s & x_s \end{bmatrix} \begin{bmatrix} J^{-T} H^x_{(s,t)} J^{-1} \\ J^{-T} H^y_{(s,t)} J^{-1} \end{bmatrix}
\\
H^s_{(x,y)} 
&= \frac{1}{x_s y_t - x_t y_s} ( - y_t [J^{-T} H^x_{(s,t)} J^{-1}] + x_t [J^{-T} H^y_{(s,t)} J^{-1}] ) 
\nonumber\\
&= \frac{1}{x_s y_t - x_t y_s} J^{-T}\big[ - y_t  H^x_{(s,t)} + x_t H^y_{(s,t)} \big]J^{-1}
\\
%
H^t_{(x,y)} 
&= \frac{1}{x_s y_t - x_t y_s} (- x_s [J^{-T} H^y_{(s,t)} J^{-1}] + y_s [J^{-T} H^x_{(s,t)} J^{-1}] )
\nonumber\\
&= \frac{1}{x_s y_t - x_t y_s} J^{-T}\big[ y_s H^x_{(s,t)} - x_s H^y_{(s,t)}  \big]J^{-1}
\end{align}

The first derivatives can be brought inside the expression by virtue of being scalars, thus the number of matrix-matrix products can be reduced, this form is as coded in SANS.

\end{document}
