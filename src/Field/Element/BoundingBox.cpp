// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BoundingBox.h"

#include <limits>

#include "tools/minmax.h"
#include "Topology/Dimension.h"
#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

namespace SANS
{

//---------------------------------------------------------------------------//
template<class PhysDim>
BoundingBox<PhysDim>::BoundingBox()
{
  high_bounds_ = -std::numeric_limits<double>::max();
  low_bounds_  =  std::numeric_limits<double>::max();
}

//---------------------------------------------------------------------------//
template<class PhysDim>
BoundingBox<PhysDim>::BoundingBox(const BoundingBox& bbox)
{
  operator=(bbox);
}

//---------------------------------------------------------------------------//
template<class PhysDim>
BoundingBox<PhysDim>&
BoundingBox<PhysDim>::operator=(const BoundingBox& bbox)
{
  high_bounds_ = bbox.high_bounds_;
  low_bounds_  = bbox.low_bounds_;

  return *this;
}

//---------------------------------------------------------------------------//
template<class PhysDim>
bool
BoundingBox<PhysDim>::
intersects_with(const BoundingBox& bbox) const
{
  for (int d = 0; d < PhysDim::D; ++d)
  {
    if (bbox.high_bounds_[d] < low_bounds_[d] ||
        bbox.low_bounds_[d]  > high_bounds_[d] )
      return false;
  }
  return true;
}

//---------------------------------------------------------------------------//
template<class PhysDim>
bool
BoundingBox<PhysDim>::
encloses(const VectorX& X) const
{
  bool enclosed = true;

  for (int d = 0; d < PhysDim::D; ++d)
    enclosed &= (high_bounds_[d] >= X[d] && low_bounds_[d] <= X[d]);

  return enclosed;
}

//---------------------------------------------------------------------------//
template<class PhysDim>
Real
BoundingBox<PhysDim>::
distance_to(const VectorX& X) const
{
  VectorX dX;

  // the logic below encompases all the case statement to decide if the point is
  // closest to a corner or a plane
  for (int d = 0; d < PhysDim::D; ++d)
    dX[d] = MAX(MAX(low_bounds_[d] - X[d], 0), X[d] - high_bounds_[d]);

  return sqrt(dot(dX,dX));
}


//---------------------------------------------------------------------------//
template<class PhysDim>
void
BoundingBox<PhysDim>::
setBound(const VectorX& X)
{
  // Grow the box by 1% to give some slop in floating point comparisons
  for (int d = 0; d < PhysDim::D; ++d)
  {
    high_bounds_[d] = MAX(X[d], high_bounds_[d]);
    low_bounds_[d]  = MIN(X[d], low_bounds_[d] );
  }
}

//---------------------------------------------------------------------------//
template<class PhysDim>
void
BoundingBox<PhysDim>::
setBound(const BoundingBox& bbox)
{
  for (int d = 0; d < PhysDim::D; ++d)
  {
    high_bounds_[d] = MAX(bbox.high_bounds_[d], high_bounds_[d]);
    low_bounds_[d]  = MIN(bbox.low_bounds_[d] , low_bounds_[d] );
  }
}

// Explicit instantiations
template class BoundingBox<PhysD1>;
template class BoundingBox<PhysD2>;
template class BoundingBox<PhysD3>;
template class BoundingBox<PhysD4>;

} // namespace SANS
