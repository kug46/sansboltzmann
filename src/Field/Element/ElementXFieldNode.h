// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ELEMENTXFIELDNODE_H
#define ELEMENTXFIELDNODE_H

// node grid field element

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "ElementNode.h"
#include "BoundingBox.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Grid field element: node
//
// member functions:
//   .coordinates     physical coordinates
//----------------------------------------------------------------------------//

template< class PhysDim >
class ElementXFieldNodeBase : public Element< DLA::VectorS<PhysDim::D,Real>, TopoD0, Node >
{
public:
  static const int D = PhysDim::D;             // physical dimensions
  static const int TopoD = TopoD0::D;          // topological dimensions
  typedef Element< DLA::VectorS<D,Real>, TopoD0, Node > BaseType;
  typedef DLA::VectorS<D,Real> VectorX;
  typedef DLA::MatrixSymS<D,Real> TensorSymX;  // physical Hessian d(u)/d(x_i x_j) - e.g. implied metric
  typedef DLA::MatrixS<D,D,Real> Matrix;       // element Jacobian d(x_i)/d(s_j)
  typedef DLA::VectorS<D, DLA::MatrixSymS<TopoD,Real>> TensorSymHessian; // element Hessian d^2(x_i)/d(s_j s_k)

  typedef VectorX T;
  typedef DLA::VectorS<1,Real> RefCoordType;

  explicit ElementXFieldNodeBase( const BasisFunctionNodeBase* basis ) : BaseType(basis) {}
  ElementXFieldNodeBase( int order, const BasisFunctionCategory& category ) : BaseType(order,category) {}
  ElementXFieldNodeBase( const ElementXFieldNodeBase& Elem ) : BaseType(Elem) {}
  ~ElementXFieldNodeBase() {}

  ElementXFieldNodeBase& operator=( const ElementXFieldNodeBase& Elem ) { BaseType::operator=(Elem); return *this; }

  VectorX coordinates() const { return BaseType::eval(); }
  void coordinates( VectorX& X ) const { BaseType::eval( X ); }
  void coordinates( const RefCoordType& sRef, VectorX& X ) const { BaseType::eval( X ); }
  void coordinates( const QuadraturePoint<TopoD0>& sRef, VectorX& X ) const { BaseType::eval( X ); }
  Real jacobianDeterminant() const { return 1; }
  Real jacobianDeterminant( const RefCoordType& sRef) const {return 1; }
  Real jacobianDeterminant( const QuadraturePoint<TopoD0>& Ref ) const { return 1; }

  BoundingBox<PhysDim> boundingBox() const
  {
    BoundingBox<PhysDim> bbox;
    bbox.setBound(DOF_[0]);
    return bbox;
  }

protected:
  using BaseType::nDOF_;
  using BaseType::DOF_;
  using BaseType::basis_;
  using BaseType::phi_;
  using BaseType::normalSignL_;
  using BaseType::normalSignR_;
};

//----------------------------------------------------------------------------//
// 3-D grid field element: node
//----------------------------------------------------------------------------//

template<class PhysDim>
class ElementXField<PhysDim, TopoD0, Node> : public ElementXFieldNodeBase< PhysDim >
{
public:
  typedef ElementXFieldNodeBase< PhysDim > BaseType;

  using BaseType::D;                                            // physical dimensions

  typedef typename BaseType::VectorX VectorX;                   // coordinates vector

  explicit ElementXField( const BasisFunctionNodeBase* basis ) : BaseType(basis) {}
  ElementXField( int order, const BasisFunctionCategory& category ) : BaseType(order,category) {}
  ElementXField( const ElementXField& Elem ) : BaseType(Elem) {}
  ~ElementXField() {}

  ElementXField& operator=( const ElementXField& node ) { BaseType::operator=(node); return *this; }
};

//----------------------------------------------------------------------------//
// 2-D grid field element: node
//----------------------------------------------------------------------------//

template<>
class ElementXField<PhysD2, TopoD0, Node> : public ElementXFieldNodeBase< PhysD2 >
{
public:
  typedef ElementXFieldNodeBase< PhysD2 > BaseType;

  using BaseType::D;                                          // physical dimensions

  typedef typename BaseType::VectorX VectorX;                 // coordinates vector
  typedef typename BaseType::RefCoordType RefCoordType;

  explicit ElementXField( const BasisFunctionNodeBase* basis ) : BaseType(basis) {}
  ElementXField( int order, const BasisFunctionCategory& category ) : BaseType(order,category) {}
  ElementXField( const ElementXField& Elem ) : BaseType(Elem) {}
  ~ElementXField() {}

  ElementXField& operator=( const ElementXField& node ) { BaseType::operator=(node); return *this; }

  Real conormalSignL( const RefCoordType& ) const { return normalSignL_; }
  Real conormalSignR( const RefCoordType& ) const { return normalSignR_; }

  Real conormalSignL( const QuadraturePoint<TopoD0>& ) const { return normalSignL_; }
  Real conormalSignR( const QuadraturePoint<TopoD0>& ) const { return normalSignR_; }

protected:
  using BaseType::normalSignL_;
  using BaseType::normalSignR_;
};

//----------------------------------------------------------------------------//
// 1-D grid field element: node
//
// member functions:
//   .unitNormal      unit normal
//----------------------------------------------------------------------------//

template<>
class ElementXField<PhysD1, TopoD0, Node> : public ElementXFieldNodeBase< PhysD1 >
{
public:
  typedef ElementXFieldNodeBase< PhysD1 > BaseType;

  using BaseType::D;                                        // physical dimensions

  typedef typename BaseType::VectorX VectorX;               // coordinates vector
  typedef typename BaseType::RefCoordType RefCoordType;

  explicit ElementXField( const BasisFunctionNodeBase* basis ) : BaseType(basis) {}
  ElementXField( int order, const BasisFunctionCategory& category ) : BaseType(order,category) {}
  ElementXField( const ElementXField& Elem ) : BaseType(Elem) {}
  ~ElementXField() {}

  ElementXField& operator=( const ElementXField& node ) { BaseType::operator=(node); return *this; }

  void unitNormal( const RefCoordType& sRef, VectorX& N ) const { N=normalSignL_; } // TODO: have yet added normalSignR, but this still works
  void unitNormal( const QuadraturePoint<TopoD0>& sRef, VectorX& N ) const { N=normalSignL_; }

  using ElementXFieldNodeBase< PhysD1 >::jacobianDeterminant;

protected:
  using BaseType::normalSignL_;
};

}

#endif //ELEMENTXFIELDNODE_H
