// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BOUNDINGBOX_H
#define BOUNDINGBOX_H

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#ifdef SANS_MPI
namespace SANS
{
template<class PhysDim>
class BoundingBox;
}

namespace boost
{
namespace serialization
{
template<class Archive, class PhysDim>
void serialize(Archive & ar, SANS::BoundingBox<PhysDim>& bbox, const unsigned int version);
}
}
#endif //SANS_MPI

namespace SANS
{

template<class PhysDim>
class BoundingBox
{
public:
  typedef DLA::VectorS<PhysDim::D,Real> VectorX;

  BoundingBox();
  BoundingBox(const BoundingBox& bbox);
  BoundingBox& operator=(const BoundingBox& bbox);

  // checks if this bbox intersects that bbox
  bool intersects_with(const BoundingBox& bbox) const;

  // checks if this bbox encloses X
  bool encloses(const VectorX& X) const;

  // computes the distance between the bbox and X
  Real distance_to(const VectorX& X) const;

  void setBound(const VectorX& X);
  void setBound(const BoundingBox& bbox);

  const VectorX& low_bounds() const { return low_bounds_; }
  const VectorX& high_bounds() const { return high_bounds_; }
protected:
  VectorX low_bounds_, high_bounds_;

#ifdef SANS_MPI
  template<class Archive, class PhysD>
  friend void ::boost::serialization::serialize(Archive & ar, BoundingBox<PhysD>& bbox, const unsigned int version);
#endif
};

} // namespace SANS


#endif //BOUNDINGBOX_H
