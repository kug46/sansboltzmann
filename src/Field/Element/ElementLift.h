// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ELEMENTLIFT_H
#define ELEMENTLIFT_H

#include "Element.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "BasisFunction/BasisFunctionCategory.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// An element to represent an array of lifting operators on a cell element
//----------------------------------------------------------------------------//

template <class T_, class TopoDim, class Topology>
class ElementLift
{
public:
  typedef Element<T_,TopoDim,Topology> ElementType;
  typedef typename ElementType::BasisType BasisType;
  typedef Topology TopologyType;
  typedef DLA::VectorS<TopoDim::D,Real> RefCoordType;
  typedef T_ T;

  ElementLift( const int order, const BasisFunctionCategory& category )
    : basis_(BasisType::getBasisFunction(order, category))
  {
    for (int i = 0; i < Topology::NTrace; i++)
      elems_[i].setBasis(basis_);
  }

  explicit ElementLift( const BasisType* basis) : basis_(basis)
  {
    for (int i = 0; i < Topology::NTrace; i++)
      elems_[i].setBasis(basis);
  }

  ElementLift( const ElementLift& elems ) : basis_(elems.basis_)
  {
    for (int i = 0; i < Topology::NTrace; i++)
      elems_[i] = elems.elems_[i];
  }

  ElementLift& operator=( const ElementLift& elems ) = delete;

  int nElem() const { return Topology::NTrace; }

        ElementType& operator[](const int i)       { return elems_[i]; }
  const ElementType& operator[](const int i) const { return elems_[i]; }

  int order() const { return basis_->order(); }
  int nDOFElem() const { return basis_->nBasis(); }
  const BasisType* basis() const { return basis_; }

  template<class RefCoord>
  void evalBasis( const RefCoordType& sRef, Real phi[], int nphi ) const
  {
    elems_[0].evalBasis(sRef, phi, nphi);
  }

  template<class RefCoord>
  void evalBasis( const RefCoord& sRef, Real phi[], int nphi ) const
  {
    elems_[0].evalBasis(sRef, phi, nphi);
  }

  // project DOFs onto another polynomial order
  void projectTo( ElementLift& ElemTo ) const
  {
    for (int i = 0; i < Topology::NTrace; i++)
      elems_[i].projectTo(ElemTo.elems_[i]);
  }

protected:
  const BasisType* basis_;
  ElementType elems_[Topology::NTrace];
};


}


#endif //ELEMENTLIFT_H
