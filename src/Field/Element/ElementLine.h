// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ELEMENTLINE_H
#define ELEMENTLINE_H

// line element

#include <iostream>
#include <string>
#include <type_traits>

#include "Element.h"
#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine_Projection.h"
#include "BasisFunction/BasisPoint.h"
#include "BasisFunction/BasisPointDerivative.h"
#include "BasisFunction/Quadrature_Cache.h"
#include "Topology/ElementTopology.h"
#include "Topology/Dimension.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// field element basis: line
//
// basis function related methods
//
//----------------------------------------------------------------------------//


template <>
class ElementBasis< TopoD1, Line >
{
public:
  typedef TopoD1 TopoDim;
  typedef Line TopologyType;
  typedef DLA::VectorS<TopoD1::D,Real> RefCoordType;
  typedef BasisFunctionLineBase BasisType;

  typedef std::array<int,Line::NNode> IntNNode; // dummy sign arrays

  explicit ElementBasis( const BasisType* basis );
  ElementBasis( int order, const BasisFunctionCategory& category );
  ElementBasis( const ElementBasis& );
  ~ElementBasis();

  ElementBasis& operator=( const ElementBasis& );

  // basis function
  const BasisType* basis() const { return basis_; }

  int rank() const { return rank_; }
  void setRank( const int rank ) { rank_ = rank; }

  int order() const { return order_; }
  int nDOF() const { return nDOF_; }

  const IntNNode& traceOrientation() const { return sgn_; }
  void setTraceOrientation(const IntNNode&) {}

  void evalBasis( const Real& sRef, const Real **phi ) const;
  void evalBasis( const RefCoordType& sRef, const Real **phi ) const { evalBasis(sRef[0], phi); }

  template <int N>
  void evalBasis( const SurrealS<N>& sRef, SurrealS<N> phi[], const int nphi ) const;

  void evalBasis( const Real& sRef, Real phi[], int nphi ) const;
  void evalBasis( const RefCoordType& sRef, Real phi[], int nphi ) const { evalBasis(sRef[0], phi, nphi); }
  void evalBasis( const QuadraturePoint<TopoDim>& ref, Real phi[], int nphi ) const;
  void evalBasis( const QuadratureCellTracePoint<TopoDim>& ref, Real phi[], int nphi ) const;
  void evalBasis( const RefCoordType& Ref, BasisPoint<TopoD1::D>& phi ) const
  {
    phi.ref() = Ref;
    evalBasis(Ref[0], phi, phi.size());
  }

  void evalBasisDerivative( const Real& sRef, const Real **phis ) const;
  void evalBasisDerivative( const RefCoordType& sRef, const Real **phis ) const { evalBasisDerivative(sRef[0], phis); }
  void evalBasisDerivative( const QuadraturePoint<TopoDim>& ref, const Real **phis ) const;
  void evalBasisDerivative( const QuadratureCellTracePoint<TopoDim>& ref, const Real **phis ) const;

  template <int N>
  void evalBasisDerivative( const SurrealS<N>& sRef, SurrealS<N> phis[], const int nphi ) const;

  void evalBasisDerivative( const Real& sRef, Real phis[], int nphi ) const;
  void evalBasisDerivative( const RefCoordType& sRef, Real phis[], int nphi ) const { evalBasisDerivative(sRef[0], phis, nphi); }
  void evalBasisDerivative( const QuadraturePoint<TopoDim>& ref, Real phis[], int nphi ) const;
  void evalBasisDerivative( const QuadratureCellTracePoint<TopoDim>& ref, Real phis[], int nphi ) const;
  void evalBasisDerivative( const RefCoordType& Ref, BasisPointDerivative<TopoD1::D>& dphi ) const
  {
    dphi.ref() = Ref;
    evalBasisDerivative(Ref[0], dphi.deriv(0), dphi.size());
  }

  void evalBasisHessianDerivative( const Real& sRef, const Real **phiss ) const;
  void evalBasisHessianDerivative( const RefCoordType& sRef, const Real **phiss ) const { evalBasisHessianDerivative(sRef[0], phiss); }
  void evalBasisHessianDerivative( const QuadraturePoint<TopoDim>& ref, const Real **phiss ) const;

  void evalBasisHessianDerivative( const Real& sRef, Real phiss[], int nphi ) const;
  void evalBasisHessianDerivative( const RefCoordType& sRef, Real phiss[], int nphi ) const { evalBasisHessianDerivative(sRef[0], phiss, nphi); }
  void evalBasisHessianDerivative( const QuadraturePoint<TopoDim>& ref, Real phiss[], int nphi ) const;

protected:
  ElementBasis();             // only intended as a base class

  //Used for arrays of elements
  void setBasis( const BasisType* basis );

  int rank_;                  // processor rank that possesses this element

  int order_;                 // polynomial order (e.g. order=1 is linear)
  int nDOF_;                  // total DOFs in element

  const BasisType* basis_;
  Real *phi_;                 // temporary storage for basis function evaluation
  Real *phis_;
  Real *phiss_;

  // cached basis functions evaluated at quadrature points
  const QuadratureBasisPointStore<TopoDim::D>* pointStoreCell_;

  // cached basis functions evaluated at quadrature points on the trace of the element
  const std::vector<QuadratureBasisPointStore<TopoDim::D>>* pointStoreTrace_;

  IntNNode sgn_;

  void setCache();
};

// default ctor needed for 'new []'
inline
ElementBasis<TopoD1, Line>::ElementBasis()
{
  rank_  = -1;
  order_ = -1;
  nDOF_  = -1;
  basis_ = NULL;
  phi_ = NULL;
  phis_ = NULL;
  phiss_ = NULL;
  pointStoreCell_ = NULL;
  pointStoreTrace_ = NULL;
}

inline
ElementBasis<TopoD1, Line>::ElementBasis( int order, const BasisFunctionCategory& category )
{
  rank_  = -1;
  basis_ = BasisFunctionLineBase::getBasisFunction(order, category);

  order_ = order;
  nDOF_  = basis_->nBasis();
  phi_   = new Real[nDOF_];
  phis_  = new Real[nDOF_];
  phiss_ = new Real[nDOF_];
  pointStoreCell_ = NULL;
  pointStoreTrace_ = NULL;

  // must be called after the basis is set
  setCache();
}

inline
ElementBasis<TopoD1, Line>::ElementBasis( const BasisFunctionLineBase* basis )
{
  rank_  = -1;
  basis_ = basis;

  order_ = basis_->order();
  nDOF_  = basis_->nBasis();
  phi_   = new Real[nDOF_];
  phis_  = new Real[nDOF_];
  phiss_ = new Real[nDOF_];
  pointStoreCell_ = NULL;
  pointStoreTrace_ = NULL;

  // must be called after the basis is set
  setCache();
}

inline
ElementBasis<TopoD1, Line>::ElementBasis( const ElementBasis& a )
{
  rank_  = a.rank_;
  order_ = a.order_;
  nDOF_  = a.nDOF_;

  phi_   = new Real[nDOF_];
  phis_  = new Real[nDOF_];
  phiss_ = new Real[nDOF_];

  basis_ = a.basis_;
  pointStoreCell_ = a.pointStoreCell_;
  pointStoreTrace_ = a.pointStoreTrace_;
}

inline
ElementBasis<TopoD1, Line>::~ElementBasis()
{
  delete [] phi_;   phi_ = NULL;
  delete [] phis_;  phis_ = NULL;
  delete [] phiss_; phiss_ = NULL;
}

inline void
ElementBasis<TopoD1, Line>::setCache()
{
  // set the pointer to the specific cache based on the basis function
  BasisFunctionCategory cat = basis_->category();
  if ( cat == BasisFunctionCategory_Hierarchical )
  {
    pointStoreCell_ = &QuadratureCache<Line>::cache.hierarchicalCell[basis_->order()].begin()->second;
    pointStoreTrace_ = &QuadratureCache<Line>::cache.hierarchicalTrace[basis_->order()].begin()->second;
  }
  else
  {
    pointStoreCell_ = &QuadratureCache<Line>::cache.cell[cat][basis_->order()];
    pointStoreTrace_ = &QuadratureCache<Line>::cache.trace[cat][basis_->order()];
  }
}

inline ElementBasis<TopoD1, Line>& // cppcheck-suppress operatorEqVarError
ElementBasis<TopoD1, Line>::operator=( const ElementBasis& a )
{
  if (this != &a)
  {
    rank_  = a.rank_;
    order_ = a.order_;
    nDOF_  = a.nDOF_;

    delete [] phi_;   phi_ = NULL;
    delete [] phis_;  phis_ = NULL;
    delete [] phiss_; phiss_ = NULL;

    phi_   = new Real[nDOF_];
    phis_  = new Real[nDOF_];
    phiss_ = new Real[nDOF_];

    basis_ = a.basis_;
    pointStoreCell_ = a.pointStoreCell_;
  }

  return *this;
}

inline void
ElementBasis<TopoD1, Line>::setBasis( const BasisFunctionLineBase* basis )
{
  delete [] phi_;   phi_ = NULL;
  delete [] phis_;  phis_ = NULL;
  delete [] phiss_; phiss_ = NULL;

  basis_ = basis;
  order_ = basis_->order();
  nDOF_  = basis_->nBasis();
  phi_   = new Real[nDOF_];
  phis_  = new Real[nDOF_];
  phiss_ = new Real[nDOF_];

  // must be called after the basis is set
  setCache();
}

inline void
ElementBasis<TopoD1, Line>::evalBasis( const Real& sRef, const Real **phi ) const
{
  basis_->evalBasis(sRef, phi_, nDOF_);
  *phi = phi_;
}

template <int N>
inline void
ElementBasis<TopoD1, Line>::evalBasis( const SurrealS<N>& sRef, SurrealS<N> phi[], const int nphi ) const
{
  const Real sRefValue = sRef.value();

  std::vector<Real> phiValue(nDOF_,0.0);
  basis_->evalBasis(sRefValue, phiValue.data(), nphi);

  std::vector<Real> phisValue(nDOF_,0.0);
  basis_->evalBasisDerivative(sRefValue, phisValue.data(), nphi);

  for (int j = 0; j < nphi; ++j)
  {
    phi[j] = phiValue.at(j);

    for (int n = 0; n < N; ++n)
      phi[j].deriv(n) = phisValue.at(j)*sRef.deriv(n); // chain rule: d(phi)/dx = d(phi)/ds * ds/dx
  }
}

inline void
ElementBasis<TopoD1, Line>::evalBasis( const Real& sRef, Real phi[], int nphi ) const
{
  basis_->evalBasis(sRef, phi, nphi);
}

inline void
ElementBasis<TopoD1, Line>::evalBasis(
    const QuadraturePoint<TopoDim>& ref, Real phi[], int nphi ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalBasis(ref.ref, phi, nphi);
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoDim, Line>::get( pointStoreCell_, ref, sgn_ );
  SANS_ASSERT( nphi == static_cast<int>(cache.phi.size()) );
  for (int i = 0; i < nphi; i++)
    phi[i] = cache.phi[i];
}

inline void
ElementBasis<TopoD1, Line>::evalBasis(
    const QuadratureCellTracePoint<TopoDim>& ref, Real phi[], int nphi ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalBasis(ref.ref, phi, nphi);
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoDim, Line>::get(pointStoreTrace_, ref, sgn_);
  SANS_ASSERT( nphi == static_cast<int>(cache.phi.size()) );
  for (int i = 0; i < nphi; i++)
    phi[i] = cache.phi[i];
}

inline void
ElementBasis<TopoD1, Line>::evalBasisDerivative( const Real& sRef, const Real **phis ) const
{
  basis_->evalBasisDerivative(sRef, phis_, nDOF_);
  *phis = phis_;
}

inline void
ElementBasis<TopoD1, Line>::evalBasisDerivative(
    const QuadraturePoint<TopoDim>& ref, const Real **phis ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalBasisDerivative(ref.ref[0], phis_, nDOF_);
    *phis = phis_;
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoDim, Line>::get( pointStoreCell_, ref, sgn_ );
  const std::vector<Real>& phis_ = cache.dphi[0];
  *phis = phis_.data();
}


inline void
ElementBasis<TopoD1, Line>::evalBasisDerivative(
    const QuadratureCellTracePoint<TopoDim>& ref, const Real **phis ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalBasisDerivative(ref.ref[0], phis_, nDOF_);
    *phis = phis_;
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoDim, Line>::get(pointStoreTrace_, ref, sgn_);
  const std::vector<Real>& phis_ = cache.dphi[0];
  *phis = phis_.data();
}


template <int N>
inline void
ElementBasis<TopoD1, Line>::evalBasisDerivative( const SurrealS<N>& sRef, SurrealS<N> phis[], const int nphi ) const
{
  const Real sRefValue = sRef.value();

  std::vector<Real> phisValue(nDOF_,0.0);
  basis_->evalBasisDerivative(sRefValue, phisValue.data(), nphi);

  std::vector<Real> phissValue(nDOF_,0.0);
  basis_->evalBasisHessianDerivative(sRefValue, phissValue.data(), nphi);

  for (int j = 0; j < nphi; ++j)
  {
    phis[j] = phisValue.at(j);

    for (int n = 0; n < N; ++n)
      phis[j].deriv(n) = phissValue.at(j)*sRef.deriv(n); // chain rule: d(phis)/dx = d(phis)/ds * ds/dx = d^2(phi)/ds^2 * ds/dx
  }
}


inline void
ElementBasis<TopoD1, Line>::evalBasisDerivative( const Real& sRef, Real phis[], int nphi ) const
{
  basis_->evalBasisDerivative(sRef, phis, nphi);
}

inline void
ElementBasis<TopoD1, Line>::evalBasisDerivative(
    const QuadraturePoint<TopoDim>& ref, Real phis[], int nphi ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalBasisDerivative(ref.ref[0], phis, nphi);
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoDim, Line>::get( pointStoreCell_, ref, sgn_ );
  const std::vector<Real>& phis_ = cache.dphi[0];
  SANS_ASSERT( nphi == nDOF_ );
  for (int n = 0; n < nphi; n++)
  {
    phis[n] = phis_[n];
  }
}


inline void
ElementBasis<TopoD1, Line>::evalBasisDerivative(
    const QuadratureCellTracePoint<TopoDim>& ref, Real phis[], int nphi ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalBasisDerivative(ref.ref[0], phis, nphi);
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoDim, Line>::get(pointStoreTrace_, ref, sgn_);
  const std::vector<Real>& phis_ = cache.dphi[0];
  SANS_ASSERT( nphi == nDOF_ );
  for (int n = 0; n < nphi; n++)
  {
    phis[n] = phis_[n];
  }
}

inline void
ElementBasis<TopoD1, Line>::evalBasisHessianDerivative( const Real& sRef, const Real **phiss ) const
{
  basis_->evalBasisHessianDerivative(sRef, phiss_, nDOF_);
  *phiss = phiss_;
}

inline void
ElementBasis<TopoD1, Line>::evalBasisHessianDerivative(
    const QuadraturePoint<TopoDim>& ref, const Real **phiss ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalBasisHessianDerivative(ref.ref[0], phiss_, nDOF_);
    *phiss = phiss_;
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoD1, Line>::get(pointStoreCell_, ref, sgn_);
  const std::vector<Real>& phiss_c = cache.d2phi[0];
  *phiss = phiss_c.data();
}

inline void
ElementBasis<TopoD1, Line>::evalBasisHessianDerivative( const Real& sRef, Real phiss[], int nphi ) const
{
  basis_->evalBasisHessianDerivative(sRef, phiss, nphi);
}


inline void
ElementBasis<TopoD1, Line>::evalBasisHessianDerivative(
    const QuadraturePoint<TopoDim>& ref, Real phiss[], int nphi ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalBasisHessianDerivative(ref.ref[0], phiss, nphi);
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoD1, Line>::get(pointStoreCell_, ref, sgn_);
  const std::vector<Real>& phiss_c = cache.d2phi[0];
  SANS_ASSERT( nphi == nDOF_ );
  for (int n = 0; n < nphi; n++)
  {
    phiss[n] = phiss_c[n];
  }
}

//----------------------------------------------------------------------------//
// field element: line
//
// template parameters:
//   T            DOF data type
//----------------------------------------------------------------------------//

template <class Tin>
class Element< Tin, TopoD1, Line > : public ElementBasis<TopoD1, Line>
{
public:
  typedef ElementBasis<TopoD1, Line> BaseType;
  typedef Tin T;
  typedef DLA::VectorS<TopoD1::D,Tin> gradT;
  typedef TopoD1 TopoDim;
  typedef typename BaseType::TopologyType TopologyType;
  typedef typename BaseType::RefCoordType RefCoordType;
  typedef BasisFunctionLineBase BasisType;

  Element();   // needed for new []
  explicit Element( const BasisType* basis );
  Element( int order, const BasisFunctionCategory& category );
  Element( const Element& );
  ~Element();

  Element& operator=( const Element& );

  //Used for arrays of elements
  void setBasis( const BasisType* basis );

  // solution DOF accessors
        T& DOF( int n )       { return DOF_[n]; }
  const T& DOF( int n ) const { return DOF_[n]; }

  // provides a vector view of the DOFs
  DLA::VectorDView<T> vectorViewDOF() { return DLA::VectorDView<T>(DOF_, nDOF_); }

  // basis function
  using BaseType::basis;

  using BaseType::order;
  using BaseType::nDOF;

  using BaseType::evalBasis;
  using BaseType::evalBasisDerivative;
  using BaseType::evalBasisHessianDerivative;

  template <int N>
  void evalFromBasis( const SurrealS<N> phi[], int nphi, T& q ) const;
  template <int D, int N>
  void evalFromBasis( const DLA::VectorS<D,SurrealS<N>> gradphi[], int nphi, DLA::VectorS<D,T> & gradq ) const;

  void evalFromBasis( const Real phi[], int nphi, T& q ) const;
  template<int D>
  void evalFromBasis( const DLA::VectorS<D,Real> gradphi[], int nphi, DLA::VectorS<D,T> & gradq ) const;
  template<int D>
  void evalFromBasis( const DLA::MatrixSymS<D,Real> hessphi[], int nphi, DLA::MatrixSymS<D,T> & hessq ) const;

     T eval( const Real& ) const;
  template <int N>
  void eval( const SurrealS<N>& sRef, T& q ) const;
  void eval( const Real& sRef, T& q ) const;
     T eval( const RefCoordType& ref ) const { return eval(ref[0]); }
  void eval( const RefCoordType& ref, T& q ) const { eval(ref[0], q); }
  void eval( const QuadraturePoint<TopoDim>& ref, T& q ) const;
  void eval( const QuadratureCellTracePoint<TopoDim>& ref, T& q ) const;

  void evalDerivative( const Real& sRef, T& qs ) const;
  void evalDerivative( const RefCoordType& sRef, DLA::VectorS<TopoD1::D, T>& dq ) const { evalDerivative(sRef[0], dq[0]); }
  void evalDerivative( const QuadraturePoint<TopoDim>& ref, DLA::VectorS<TopoDim::D, T>& dq ) const;
  void evalDerivative( const QuadratureCellTracePoint<TopoDim>& ref, DLA::VectorS<TopoDim::D, T>& dq ) const;

  // project DOFs onto another polynomial order
  void projectTo( Element& ) const;

  void dump( int indentSize = 0, std::ostream& out = std::cout ) const;

protected:
  using BaseType::order_;     // polynomial order (e.g. order=1 is linear)
  using BaseType::nDOF_;      // total DOFs in element
  T* DOF_;                    // DOFs

  using BaseType::basis_;
  using BaseType::phi_;       // temporary storage for basis function evaluation
  using BaseType::phis_;

  using BaseType::pointStoreCell_;  // cached basis functions evaluated at quadrature points
  using BaseType::pointStoreTrace_; // cached basis functions evaluated at quadrature points on the trace of the element
};

// default ctor needed for 'new []'
template <class T>
Element<T, TopoD1, Line>::Element()
  : BaseType()
{
  DOF_  = NULL;
}

template <class T>
Element<T, TopoD1, Line>::Element( int order, const BasisFunctionCategory& category )
  : BaseType(order, category)
{
  DOF_  = new T[nDOF_];
}


template <class T>
Element<T, TopoD1, Line>::Element( const BasisFunctionLineBase* basis )
  : BaseType(basis)
{
  DOF_  = new T[nDOF_];
}


template <class T>
Element<T, TopoD1, Line>::Element( const Element& a )
  : BaseType(a)
{
  DOF_  = new T[nDOF_];

  for (int n = 0; n < nDOF_; n++)
    DOF_[n] = a.DOF_[n];
}


template <class T>
Element<T, TopoD1, Line>::~Element()
{
  delete [] DOF_; DOF_ = NULL;
}

template <class T>
Element<T, TopoD1, Line>& // cppcheck-suppress operatorEqVarError
Element<T, TopoD1, Line>::operator=( const Element& a )
{
  if (this != &a)
  {
    BaseType::operator=(a);

    delete [] DOF_; DOF_ = NULL;

    DOF_  = new T[nDOF_];

    for (int n = 0; n < nDOF_; n++)
      DOF_[n] = a.DOF_[n];
  }

  return *this;
}

template <class T>
inline void
Element<T, TopoD1, Line>::setBasis( const BasisFunctionLineBase* basis )
{
  BaseType::setBasis(basis);

  delete [] DOF_; DOF_ = NULL;

  DOF_  = new T[nDOF_];
}

template <class T>
template <int N>
inline void
Element<T, TopoD1, Line>::evalFromBasis( const SurrealS<N> phi[], int nphi, T& q ) const
{
  SANS_ASSERT((std::is_same<SurrealS<N>,typename Scalar<T>::type>::value));
  SANS_ASSERT(nphi == nDOF_);
  q = 0.0;
  for (int n = 0; n < nDOF_; n++)
    q += phi[n]*DOF_[n];
}

template <class T>
template <int D, int N>
inline void
Element<T, TopoD1, Line>::evalFromBasis(
    const DLA::VectorS<D,SurrealS<N>> gradphi[], int nphi, DLA::VectorS<D,T> & gradq ) const
{
  SANS_ASSERT((std::is_same<SurrealS<N>,typename Scalar<T>::type>::value));
  SANS_ASSERT_MSG(nphi == nDOF_, "nphi = %d, nDOF_ = %d", nphi, nDOF_);
  gradq = 0.0;
  for (int n = 0; n < nDOF_; n++)
    for (int i = 0; i < D; i++)
      gradq[i] += gradphi[n][i]*DOF_[n];
}

template <class T>
inline void
Element<T, TopoD1, Line>::evalFromBasis( const Real phi[], int nphi, T& q ) const
{
  SANS_ASSERT(nphi == nDOF_);
  q = 0;
  for (int n = 0; n < nDOF_; n++)
    q += phi[n]*DOF_[n];
}

template <class T>
template <int D>
inline void
Element<T, TopoD1, Line>::evalFromBasis(
    const DLA::VectorS<D,Real> gradphi[], int nphi, DLA::VectorS<D,T>& gradq ) const
{
  SANS_ASSERT_MSG(nphi == nDOF_, "nphi = %d, nDOF_ = %d", nphi, nDOF_);
  gradq = 0;
  for (int n = 0; n < nDOF_; n++)
    for (int i = 0; i < D; i++)
      gradq[i] += gradphi[n][i]*DOF_[n];
}

template <class T>
template <int D>
inline void
Element<T, TopoD1, Line>::evalFromBasis(
    const DLA::MatrixSymS<D,Real> hessphi[], int nphi, DLA::MatrixSymS<D,T>& hessq ) const
{
  SANS_ASSERT_MSG(nphi == nDOF_, "nphi = %d, nDOF_ = %d", nphi, nDOF_);
  hessq = 0;
  for (int n = 0; n < nDOF_; n++)
    for (int i = 0; i < D; i++)
      for (int j = 0; j <= i; j++)// because it is lower triangular
        hessq(i,j) += hessphi[n](i,j)*DOF_[n];
}

template <class T>
inline T
Element<T, TopoD1, Line>::eval( const Real& sRef ) const
{
  T q;
  eval(sRef, q);

  return q;
}

template <class T>
template <int N>
inline void
Element<T, TopoD1, Line>::eval( const SurrealS<N>& sRef, T& q ) const
{
  SANS_ASSERT((std::is_same<SurrealS<N>, typename Scalar<T>::type>::value));

  std::vector<SurrealS<N>> phi(nDOF_,0.0);
  evalBasis(sRef, phi.data(), nDOF_);

  q = 0;
  for (int n = 0; n < nDOF_; n++)
    q += phi.at(n)*DOF_[n];
}

template <class T>
inline void
Element<T, TopoD1, Line>::eval( const Real& sRef, T& q ) const
{
  basis_->evalBasis(sRef, phi_, nDOF_);

  q = 0;
  for (int n = 0; n < nDOF_; n++)
    q += phi_[n]*DOF_[n];
}

template <class T>
void
Element<T, TopoD1, Line>::eval(
    const QuadraturePoint<TopoDim>& ref, T& q ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    eval(ref.ref, q);
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoDim, Line>::get( pointStoreCell_, ref, sgn_ );
  q = 0;
  for (int n = 0; n < nDOF_; n++)
    q += cache.phi[n]*DOF_[n];
}

template <class T>
void
Element<T, TopoD1, Line>::eval(
    const QuadratureCellTracePoint<TopoDim>& ref, T& q ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    eval(ref.ref, q);
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoDim, Line>::get(pointStoreTrace_, ref, sgn_);
  q = 0;
  for (int n = 0; n < nDOF_; n++)
    q += cache.phi[n]*DOF_[n];
}

template <class T>
inline void
Element<T, TopoD1, Line>::evalDerivative( const Real& sRef, T& qs ) const
{
  evalBasisDerivative( sRef, phis_, nDOF_ );

  qs = 0;
  for (int n = 0; n < nDOF_; n++)
  {
    qs += phis_[n]*DOF_[n];
  }
}

template <class T>
void
Element<T, TopoD1, Line>::evalDerivative(
    const QuadraturePoint<TopoDim>& ref, DLA::VectorS<TopoDim::D, T>& dq ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalDerivative(ref.ref, dq);
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoDim, Line>::get( pointStoreCell_, ref, sgn_ );
  const std::vector<Real>& phis = cache.dphi[0];
  dq = 0;
  for (int n = 0; n < nDOF_; n++)
  {
    dq[0] += phis[n]*DOF_[n];
  }
}

template <class T>
void
Element<T, TopoD1, Line>::evalDerivative(
    const QuadratureCellTracePoint<TopoDim>& ref, DLA::VectorS<TopoDim::D, T>& dq ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalDerivative(ref.ref, dq);
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoDim, Line>::get(pointStoreTrace_, ref, sgn_);
  const std::vector<Real>& phis = cache.dphi[0];
  dq = 0;
  for (int n = 0; n < nDOF_; n++)
  {
    dq[0] += phis[n]*DOF_[n];
  }
}

// project DOFs onto another polynomial order
template <class T>
inline void
Element<T, TopoD1, Line>::projectTo( Element& ElemTo ) const
{
  BasisFunctionLine_projectTo( basis_, DOF_, nDOF_, ElemTo.basis_, ElemTo.DOF_, ElemTo.nDOF_ );
}

template <class T>
void
Element<T, TopoD1, Line>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "Element<T, TopoD1, Line>:"
      << "  order_ = " << order_ << "  nDOF_ = " << nDOF_ << std::endl;
  if (DOF_ != NULL)
  {
    out << indent << "  DOF_ = ";
    for (int n = 0; n < nDOF_; n++)
      out << "(" << DOF_[n] << ") ";
    out << std::endl;
  }
  if (basis_ != NULL)
  {
    out << indent << "  basis_:" << std::endl;
    basis_->dump( indentSize+2, out );
  }
}

}

#endif  // ELEMENTLINE_H
