% triangle
x0 = [0,0];
x1 = [1,0];
x2 = [1/2, sqrt(sym(3))/2];

J = [x1-x0;x2-x0]';
inv(J)
det(J)

% tetrahedron
x0 = [0,0,0];
x1 = [1,0,0];
x2 = [1/2,sqrt(sym(3))/2,0];
x3 = [1/2,sqrt(sym(3))/6,sqrt(sym(2)/sym(3))];
J = [x1-x0;x2-x0;x3-x0]';
inv(J)
det(J)

% pentatope: edge lengths are unit if we multiply be the following factor
fac = sqrt(sym(4)/sym(10)); % edge length between any two vertices below
x0 = [   1   0,                    0,                    0                  ]*fac;
x1 = [-1/4,  sqrt(sym(15))/4,      0,                    0                  ]*fac;
x2 = [-1/4, -sqrt(sym(5)/sym(48)), sqrt(sym(5)/sym(6)),  0                  ]*fac;
x3 = [-1/4,- sqrt(sym(5)/sym(48)),-sqrt(sym(5)/sym(24)), sqrt(sym(5)/sym(8))]*fac;
x4 = [-1/4,- sqrt(sym(5)/sym(48)),-sqrt(sym(5)/sym(24)),-sqrt(sym(5)/sym(8))]*fac;
J = [x1-x0;x2-x0;x3-x0;x4-x0]';
inv(J)
det(J)
ccode(inv(J))