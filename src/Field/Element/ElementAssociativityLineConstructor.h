// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ELEMENTASSOCIATIVITYLINECONSTRUCTOR_H
#define ELEMENTASSOCIATIVITYLINECONSTRUCTOR_H

// Constructor class for element line associativity (local to global mappings)

#include <ostream>
#include <vector>
#include <array>

#include "tools/SANSException.h"
#include "BasisFunction/BasisFunctionLine.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// edge associativity constructor: global numberings for single element nodes/edge
//
// member functions:
//   .order                     polynomial order
//   .nNode                     # node DOFs
//   .nEdge                     # edge interior DOFs
//   .node/edgeGlobal           global node/edge DOF accessors
//   .set/getNodeGlobalMapping  local-to-global node DOF mapping
//   .set/getEdgeGlobalMapping  local-to-global edge DOF mapping
//
//   .dump            debug dump of private data
//----------------------------------------------------------------------------//

template <class TopoDim, class Topology>
class ElementAssociativityConstructor;

template<>
class ElementAssociativityConstructor<TopoD1, Line>
{
public:
  typedef TopoD1 TopoDim;
  typedef Line TopologyType;             // element topology
  typedef BasisFunctionLineBase BasisType;
  typedef std::array<int,Line::NNode> IntNNode;

  ElementAssociativityConstructor();
  explicit ElementAssociativityConstructor( int order );
  explicit ElementAssociativityConstructor( const BasisType* basis ) : ElementAssociativityConstructor() { resize(basis); }
  ~ElementAssociativityConstructor();

  ElementAssociativityConstructor( const ElementAssociativityConstructor& a ) : ElementAssociativityConstructor() { *this = a; }
  ElementAssociativityConstructor& operator=( const ElementAssociativityConstructor& );

  void resize( const BasisType* basis );

  int rank() const  { SANS_ASSERT(isSetRank_); return rank_; }
  int order() const { return order_; }
  int nNode() const { return nNode_; }
  int nEdge() const { return nEdge_; }

  void setRank(const int rank) { isSetRank_ = true; rank_ = rank; }

  // node, edge maps
  int nodeGlobal( int n ) const { SANS_ASSERT(isSetNodeGlobalMapping_[n]); return (nodeList_[n]); }
  int edgeGlobal( int n ) const { SANS_ASSERT(isSetEdgeGlobalMapping_[n]); return (edgeList_[n]); }

  void setNodeGlobalMapping( const int node[], int nnode );
  void getNodeGlobalMapping(       int node[], int nnode ) const;

  void setNodeGlobalMapping( const std::vector<int>& node );

  void setEdgeGlobalMapping( const int edge[], int nedge );
  void getEdgeGlobalMapping(       int edge[], int nedge ) const;

  void setEdgeGlobalMapping( const std::vector<int>& edge );

  void setCellGlobalMapping( const int cell[], int ncell );
  void getCellGlobalMapping(       int cell[], int ncell ) const;

  void setCellGlobalMapping( const std::vector<int>& cell );

  void setGlobalMapping( const int map[], int ndof );
  void getGlobalMapping(       int map[], int ndof ) const;

  void setGlobalMapping( const std::vector<int>& map );

        IntNNode& nodeSign()       { return nodeSign_; }
  const IntNNode& nodeSign() const { return nodeSign_; }
        IntNNode& traceOrientation()       { return nodeSign_; }
  const IntNNode& traceOrientation() const { return nodeSign_; }

  // Dummy function here to provide an n-dimensional interface
  void setOrientation( int sgn, int canonicalNode ) {}

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  int rank_;                      // the processor rank that possesses this element
  int order_;                     // polynomial order for node/edge DOFs
  int nNode_;                     // # node DOFs
  int nEdge_;                     // # edge DOFs
  int* nodeList_;                 // global ordering of node DOFs
  int* edgeList_;                 // global ordering of edge DOFs

  bool isSetRank_;                 // flag: has the processor rank been set
  bool* isSetNodeGlobalMapping_;   // flag: is nodeList_ set yet?
  bool* isSetEdgeGlobalMapping_;   // flag: is edgeList_ set yet?

  IntNNode nodeSign_;
};

}
#endif  // ELEMENTASSOCIATIVITYLINECONSTRUCTOR_H
