// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ELEMENTAREA_H
#define ELEMENTAREA_H

// Area (triangle/quad) field element

#include <ostream>
#include <string>
#include <array>

#include "Element.h"
#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "BasisFunction/BasisFunctionArea.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionArea_Projection.h"
#include "BasisFunction/BasisPoint.h"
#include "BasisFunction/BasisPointDerivative.h"
#include "BasisFunction/Quadrature_Cache.h"
#include "Topology/Dimension.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// field element basis: area
//
// basis function related methods
//
// template parameters:
//   Topology       element/basis topology (e.g. Triangle, Quad)
//----------------------------------------------------------------------------//

template <class Topology>
class ElementBasis<TopoD2, Topology>
{
public:
  typedef TopoD2 TopoDim;
  typedef Topology TopologyType;
  typedef DLA::VectorS<TopoD2::D,Real> RefCoordType;
  typedef BasisFunctionAreaBase<Topology> BasisType;

  typedef std::array<int,Topology::NEdge> IntNEdge; // edge sign arrays

  explicit ElementBasis( const BasisFunctionAreaBase<Topology>* basis );
  ElementBasis( int order, const BasisFunctionCategory& category );
  ElementBasis( const ElementBasis& );
  ~ElementBasis();

  ElementBasis& operator=( const ElementBasis& );

  // basis function
  const BasisType* basis() const { return basis_; }

  int rank() const { return rank_; }
  void setRank( const int rank ) { rank_ = rank; }

  int order() const { return order_; }
  int nDOF() const { return nDOF_; }

  // edge sign accessors
  const IntNEdge& edgeSign() const { return edgeSign_; }
  const IntNEdge& traceOrientation() const { return edgeSign_; }

  void setEdgeSign( const IntNEdge& edgeSign );
  void setTraceOrientation(const IntNEdge& edgeSign) { setEdgeSign(edgeSign); }

  void evalBasis( const Real& sRef, const Real& tRef, const Real **phi ) const;
  void evalBasis( const RefCoordType& Ref, const Real **phi ) const { evalBasis(Ref[0], Ref[1], phi); }
  void evalBasis( const QuadraturePoint<TopoDim>& ref, const Real **phi ) const;
  void evalBasis( const QuadratureCellTracePoint<TopoDim>& ref, const Real **phi ) const;

  void evalBasis( const Real& sRef, const Real& tRef, Real phi[], int nphi ) const;
  void evalBasis( const RefCoordType& Ref, Real phi[], int nphi ) const { evalBasis(Ref[0], Ref[1], phi, nphi); }
  void evalBasis( const QuadraturePoint<TopoDim>& ref, Real phi[], int nphi ) const;
  void evalBasis( const QuadratureCellTracePoint<TopoDim>& ref, Real phi[], int nphi ) const;
  void evalBasis( const RefCoordType& Ref, BasisPoint<TopoD2::D>& phi ) const
  {
    phi.ref() = Ref;
    evalBasis(Ref[0], Ref[1], phi, phi.size());
  }

  void evalBasisDerivative( const Real& sRef, const Real& tRef, const Real **phis, const Real **phit ) const;
  void evalBasisDerivative( const RefCoordType& sRef, const Real **phis, const Real **phit ) const
  {
    evalBasisDerivative(sRef[0], sRef[1], phis, phit);
  }
  void evalBasisDerivative( const QuadraturePoint<TopoDim>& ref, const Real **phis, const Real **phit ) const;
  void evalBasisDerivative( const QuadratureCellTracePoint<TopoDim>& ref, const Real **phis, const Real **phit ) const;

  void evalBasisDerivative( const Real& sRef, const Real& tRef, Real phis[], Real phit[], int nphi ) const;
  void evalBasisDerivative( const RefCoordType& sRef, Real phis[], Real phit[], int nphi ) const
  {
    evalBasisDerivative(sRef[0], sRef[1], phis, phit, nphi);
  }
  void evalBasisDerivative( const RefCoordType& sRef, RefCoordType dphi[], int nphi ) const;
  void evalBasisDerivative( const QuadraturePoint<TopoDim>& sRef, RefCoordType dphi[], int nphi ) const;
  void evalBasisDerivative( const QuadratureCellTracePoint<TopoDim>& sRef, RefCoordType dphi[], int nphi ) const;
  void evalBasisDerivative( const QuadraturePoint<TopoDim>& ref, Real phis[], Real phit[], int nphi ) const;
  void evalBasisDerivative( const QuadratureCellTracePoint<TopoDim>& ref, Real phis[], Real phit[], int nphi ) const;
  void evalBasisDerivative( const RefCoordType& Ref, BasisPointDerivative<TopoDim::D>& dphi ) const
  {
    dphi.ref() = Ref;
    evalBasisDerivative(Ref[0], Ref[1], dphi.deriv(0), dphi.deriv(1), dphi.size());
  }

  void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Real **phiss, const Real **phist, const Real **phitt ) const;
  void evalBasisHessianDerivative( const QuadraturePoint<TopoDim>& Ref, const Real **phiss, const Real **phist, const Real **phitt ) const;

  void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, Real phiss[], Real phist[], Real phitt[], int nphi ) const;
  void evalBasisHessianDerivative( const QuadraturePoint<TopoDim>& Ref, Real phiss[], Real phist[], Real phitt[], int nphi ) const;

protected:
  ElementBasis();             // only intended as a base class

  //Used for arrays of elements
  void setBasis( const BasisType* basis );

  int rank_;                  // processor rank that possesses this element

  int order_;                 // polynomial order (e.g. order=1 is linear)
  int nDOF_;                  // total DOFs in element

  const BasisFunctionAreaBase<Topology>* basis_;
  Real *phi_;                 // temporary storage for basis function evaluation
  Real *phis_, *phit_;
  Real *phiss_, *phist_, *phitt_;

  // cached basis functions evaluated at quadrature points
  const QuadratureBasisPointStore<TopoDim::D>* pointStoreCell_;

  // cached basis functions evaluated at quadrature points on the trace of the element
  const std::vector<QuadratureBasisPointStore<TopoDim::D>>* pointStoreTrace_;

  IntNEdge edgeSign_;   // +/- sign for edge orientations (left/right element)

  void setCache();
};

template <class Topology>
ElementBasis<TopoD2, Topology>::ElementBasis()
{
  rank_  = -1;
  order_ = -1;
  nDOF_  = -1;
  basis_ = NULL;
  phi_   = NULL;
  phis_  = NULL;
  phit_  = NULL;
  phiss_ = NULL;
  phist_ = NULL;
  phitt_ = NULL;
  pointStoreCell_ = NULL;
  pointStoreTrace_ = NULL;

  for (int n = 0; n < Topology::NEdge; n++)
    edgeSign_[n] = +1;
}


template <class Topology>
ElementBasis<TopoD2, Topology>::ElementBasis(
    const BasisFunctionAreaBase<Topology>* basis )
{
  rank_  = -1;
  basis_ = basis;
  order_ = basis_->order();
  nDOF_  = basis_->nBasis();
  phi_   = new Real[nDOF_];
  phis_  = new Real[nDOF_];
  phit_  = new Real[nDOF_];
  phiss_ = new Real[nDOF_];
  phist_ = new Real[nDOF_];
  phitt_ = new Real[nDOF_];

  for (int n = 0; n < Topology::NEdge; n++)
    edgeSign_[n] = +1;

  // must be called after the edge sign and basis is set
  setCache();
}


template <class Topology>
ElementBasis<TopoD2, Topology>::ElementBasis( int order, const BasisFunctionCategory& category )
  : basis_( BasisFunctionAreaBase<Topology>::getBasisFunction(order, category) )
{
  rank_  = -1;
  order_ = order;
  nDOF_  = basis_->nBasis();
  phi_   = new Real[nDOF_];
  phis_  = new Real[nDOF_];
  phit_  = new Real[nDOF_];
  phiss_ = new Real[nDOF_];
  phist_ = new Real[nDOF_];
  phitt_ = new Real[nDOF_];

  for (int n = 0; n < Topology::NEdge; n++)
    edgeSign_[n] = +1;

  // must be called after the edge sign and basis is set
  setCache();
}


template <class Topology>
ElementBasis<TopoD2, Topology>::ElementBasis( const ElementBasis& a )
  : basis_( a.basis_ )
{
  rank_  = a.rank_;
  order_ = a.order_;
  nDOF_  = a.nDOF_;

  basis_ = a.basis_;
  pointStoreCell_ = a.pointStoreCell_;
  pointStoreTrace_ = a.pointStoreTrace_;

  phi_   = new Real[nDOF_];
  phis_  = new Real[nDOF_];
  phit_  = new Real[nDOF_];
  phiss_ = new Real[nDOF_];
  phist_ = new Real[nDOF_];
  phitt_ = new Real[nDOF_];

  for (int n = 0; n < Topology::NEdge; n++)
    edgeSign_[n] = a.edgeSign_[n];
}


template <class Topology>
ElementBasis<TopoD2, Topology>::~ElementBasis()
{
  delete [] phi_;
  delete [] phis_;
  delete [] phit_;
  delete [] phiss_;
  delete [] phist_;
  delete [] phitt_;
}

template <class Topology>
void
ElementBasis<TopoD2, Topology>::setCache()
{
  // set the pointer to the specific cache based on the basis function
  // note the dependence on the edgeSign
  BasisFunctionCategory cat = basis_->category();
  if ( cat == BasisFunctionCategory_Hierarchical )
  {
    pointStoreCell_  = &QuadratureCache<Topology>::cache.hierarchicalCell[basis_->order()].at(edgeSign_);
    pointStoreTrace_ = &QuadratureCache<Topology>::cache.hierarchicalTrace[basis_->order()].at(edgeSign_);
  }
  else
  {
    pointStoreCell_  = &QuadratureCache<Topology>::cache.cell[cat][basis_->order()];
    pointStoreTrace_ = &QuadratureCache<Topology>::cache.trace[cat][basis_->order()];
  }
}

template <class Topology>
ElementBasis<TopoD2, Topology>& // cppcheck-suppress operatorEqVarError
ElementBasis<TopoD2, Topology>::operator=( const ElementBasis& a )
{
  if (this != &a)
  {
    rank_  = a.rank_;
    order_ = a.order_;
    nDOF_ = a.nDOF_;

    delete [] phi_;   phi_   = NULL;
    delete [] phis_;  phis_  = NULL;
    delete [] phit_;  phit_  = NULL;
    delete [] phiss_; phiss_ = NULL;
    delete [] phist_; phist_ = NULL;
    delete [] phitt_; phitt_ = NULL;

    phi_   = new Real[nDOF_];
    phis_  = new Real[nDOF_];
    phit_  = new Real[nDOF_];
    phiss_ = new Real[nDOF_];
    phist_ = new Real[nDOF_];
    phitt_ = new Real[nDOF_];

    basis_ = a.basis_;
    pointStoreCell_  = a.pointStoreCell_;
    pointStoreTrace_  = a.pointStoreTrace_;

    for (int n = 0; n < Topology::NEdge; n++)
      edgeSign_[n] = a.edgeSign_[n];
  }

  return *this;
}

template <class Topology>
void
ElementBasis<TopoD2, Topology>::setBasis(
    const BasisFunctionAreaBase<Topology>* basis )
{
  delete [] phi_;   phi_   = NULL;
  delete [] phis_;  phis_  = NULL;
  delete [] phit_;  phit_  = NULL;
  delete [] phiss_; phiss_ = NULL;
  delete [] phist_; phist_ = NULL;
  delete [] phitt_; phitt_ = NULL;

  basis_ = basis;
  order_ = basis_->order();
  nDOF_  = basis_->nBasis();
  phi_   = new Real[nDOF_];
  phis_  = new Real[nDOF_];
  phit_  = new Real[nDOF_];
  phiss_ = new Real[nDOF_];
  phist_ = new Real[nDOF_];
  phitt_ = new Real[nDOF_];

  setCache();
}

template <class Topology>
void
ElementBasis<TopoD2, Topology>::setEdgeSign( const IntNEdge& edgeSign )
{
  edgeSign_ = edgeSign;

  if ( basis_->category() == BasisFunctionCategory_Hierarchical )
  {
    pointStoreCell_  = &QuadratureCache<Topology>::cache.hierarchicalCell[basis_->order()].at(edgeSign_);
    pointStoreTrace_ = &QuadratureCache<Topology>::cache.hierarchicalTrace[basis_->order()].at(edgeSign_);
  }
}

template <class Topology>
void
ElementBasis<TopoD2, Topology>::evalBasis(
    const Real& sRef, const Real& tRef, const Real **phi ) const
{
  basis_->evalBasis( sRef, tRef, edgeSign_, phi_, nDOF_ );
  *phi = phi_;
}

template <class Topology>
void
ElementBasis<TopoD2, Topology>::evalBasis(
    const QuadraturePoint<TopoDim>& ref, const Real **phi ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalBasis(ref.ref, phi_, nDOF_);
    *phi = phi_;
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoDim, Topology>::get(pointStoreCell_, ref, edgeSign_);
  *phi = cache.phi.data();
}

template <class Topology>
void
ElementBasis<TopoD2, Topology>::evalBasis(
    const QuadratureCellTracePoint<TopoDim>& ref, const Real **phi ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalBasis(ref.ref, phi_, nDOF_);
    *phi = phi_;
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoD2, Topology>::get(pointStoreTrace_, ref, edgeSign_);
  *phi = cache.phi.data();
}


template <class Topology>
void
ElementBasis<TopoD2, Topology>::evalBasis(
    const Real& sRef, const Real& tRef, Real phi[], int nphi ) const
{
  basis_->evalBasis( sRef, tRef, edgeSign_, phi, nphi );
}

template <class Topology>
void
ElementBasis<TopoD2, Topology>::evalBasis(
    const QuadraturePoint<TopoDim>& ref, Real phi[], int nphi ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalBasis(ref.ref, phi, nphi);
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoDim, Topology>::get(pointStoreCell_, ref, edgeSign_);
  SANS_ASSERT( nphi == static_cast<int>(cache.phi.size()) );
  for (int i = 0; i < nphi; i++)
    phi[i] = cache.phi[i];
}

template <class Topology>
void
ElementBasis<TopoD2, Topology>::evalBasis(
    const QuadratureCellTracePoint<TopoDim>& ref, Real phi[], int nphi ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalBasis(ref.ref, phi, nphi);
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoD2, Topology>::get(pointStoreTrace_, ref, edgeSign_);
  SANS_ASSERT( nphi == static_cast<int>(cache.phi.size()) );
  for (int i = 0; i < nphi; i++)
    phi[i] = cache.phi[i];
}


template <class Topology>
void
ElementBasis<TopoD2, Topology>::evalBasisDerivative(
    const Real& sRef, const Real& tRef, const Real **phis, const Real **phit ) const
{
  basis_->evalBasisDerivative( sRef, tRef, edgeSign_, phis_, phit_, nDOF_ );
  *phis = phis_;
  *phit = phit_;
}

template <class Topology>
void
ElementBasis<TopoD2, Topology>::evalBasisDerivative(
    const QuadraturePoint<TopoDim>& ref, const Real **phis, const Real **phit ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalBasisDerivative(ref.ref[0], ref.ref[1], phis_, phit_, nDOF_);
    *phis = phis_;
    *phit = phit_;
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoDim, Topology>::get(pointStoreCell_, ref, edgeSign_);
  const std::vector<Real>& phis_c = cache.dphi[0];
  const std::vector<Real>& phit_c = cache.dphi[1];
  *phis = phis_c.data();
  *phit = phit_c.data();
}

template <class Topology>
void
ElementBasis<TopoD2, Topology>::evalBasisDerivative(
    const QuadratureCellTracePoint<TopoDim>& ref, const Real **phis, const Real **phit ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalBasisDerivative(ref.ref[0], ref.ref[1], phis_, phit_, nDOF_);
    *phis = phis_;
    *phit = phit_;
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoD2, Topology>::get(pointStoreTrace_, ref, edgeSign_);
  const std::vector<Real>& phis_c = cache.dphi[0];
  const std::vector<Real>& phit_c = cache.dphi[1];
  *phis = phis_c.data();
  *phit = phit_c.data();
}


template <class Topology>
void
ElementBasis<TopoD2, Topology>::evalBasisDerivative(
    const Real& sRef, const Real& tRef, Real phis[], Real phit[], int nphi ) const
{
  basis_->evalBasisDerivative( sRef, tRef, edgeSign_, phis, phit, nphi );
}

template <class Topology>
void
ElementBasis<TopoD2, Topology>::evalBasisDerivative(
   const RefCoordType& sRef, RefCoordType dphi[], int nphi ) const
{
  basis_->evalBasisDerivative( sRef[0], sRef[1], edgeSign_, phis_, phit_, nphi );
  for (int n = 0; n < nphi; n++)
  {
    dphi[n][0] = phis_[n];
    dphi[n][1] = phit_[n];
  }
}

template <class Topology>
void
ElementBasis<TopoD2, Topology>::evalBasisDerivative(
   const QuadraturePoint<TopoDim>& ref, RefCoordType dphi[], int nphi ) const
{
  evalBasisDerivative( ref, phis_, phit_, nphi );
  for (int n = 0; n < nphi; n++)
  {
    dphi[n][0] = phis_[n];
    dphi[n][1] = phit_[n];
  }
}

template <class Topology>
void
ElementBasis<TopoD2, Topology>::evalBasisDerivative(
   const QuadratureCellTracePoint<TopoDim>& ref, RefCoordType dphi[], int nphi ) const
{
  evalBasisDerivative( ref, phis_, phit_, nphi );
  for (int n = 0; n < nphi; n++)
  {
    dphi[n][0] = phis_[n];
    dphi[n][1] = phit_[n];
  }
}

template <class Topology>
void
ElementBasis<TopoD2, Topology>::evalBasisDerivative(
    const QuadraturePoint<TopoDim>& ref, Real phis[], Real phit[], int nphi ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalBasisDerivative(ref.ref[0], ref.ref[1], phis, phit, nphi);
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoDim, Topology>::get(pointStoreCell_, ref, edgeSign_);
  const std::vector<Real>& phis_ = cache.dphi[0];
  const std::vector<Real>& phit_ = cache.dphi[1];
  SANS_ASSERT( nphi == nDOF_ );
  for (int n = 0; n < nphi; n++)
  {
    phis[n] = phis_[n];
    phit[n] = phit_[n];
  }
}

template <class Topology>
void
ElementBasis<TopoD2, Topology>::evalBasisDerivative(
    const QuadratureCellTracePoint<TopoDim>& ref, Real phis[], Real phit[], int nphi ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalBasisDerivative(ref.ref[0], ref.ref[1], phis, phit, nphi);
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoD2, Topology>::get(pointStoreTrace_, ref, edgeSign_);
  const std::vector<Real>& phis_ = cache.dphi[0];
  const std::vector<Real>& phit_ = cache.dphi[1];
  SANS_ASSERT( nphi == nDOF_ );
  for (int n = 0; n < nphi; n++)
  {
    phis[n] = phis_[n];
    phit[n] = phit_[n];
  }
}

template <class Topology>
void
ElementBasis<TopoD2, Topology>::evalBasisHessianDerivative(
    const Real& sRef, const Real& tRef, const Real **phiss, const Real **phist, const Real **phitt ) const
{
  basis_->evalBasisHessianDerivative( sRef, tRef, edgeSign_, phiss_, phist_, phitt_, nDOF_ );
  *phiss = phiss_;
  *phist = phist_;
  *phitt = phitt_;
}

template <class Topology>
void
ElementBasis<TopoD2, Topology>::evalBasisHessianDerivative(
    const QuadraturePoint<TopoDim>& ref, const Real **phiss, const Real **phist, const Real **phitt ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalBasisHessianDerivative(ref.ref[0], ref.ref[1], phiss_, phist_, phitt_, nDOF_);
    *phiss = phiss_;
    *phist = phist_;
    *phitt = phitt_;
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoDim, Topology>::get(pointStoreCell_, ref, edgeSign_);
  const std::vector<Real>& phiss_c = cache.d2phi[0];
  const std::vector<Real>& phist_c = cache.d2phi[1];
  const std::vector<Real>& phitt_c = cache.d2phi[2];
  *phiss = phiss_c.data();
  *phist = phist_c.data();
  *phitt = phitt_c.data();
}

template <class Topology>
void
ElementBasis<TopoD2, Topology>::evalBasisHessianDerivative(
    const Real& sRef, const Real& tRef, Real phiss[], Real phist[], Real phitt[], int nphi ) const
{
  basis_->evalBasisHessianDerivative( sRef, tRef, edgeSign_, phiss, phist, phitt, nphi );
}

template <class Topology>
void
ElementBasis<TopoD2, Topology>::evalBasisHessianDerivative(
    const QuadraturePoint<TopoDim>& ref, Real phiss[], Real phist[], Real phitt[], int nphi ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalBasisHessianDerivative(ref.ref[0], ref.ref[1], phiss, phist, phitt, nphi);
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoDim, Topology>::get(pointStoreCell_, ref, edgeSign_);
  const std::vector<Real>& phiss_c = cache.d2phi[0];
  const std::vector<Real>& phist_c = cache.d2phi[1];
  const std::vector<Real>& phitt_c = cache.d2phi[2];
  SANS_ASSERT( nphi == nDOF_ );
  for (int n = 0; n < nphi; n++)
  {
    phiss[n] = phiss_c[n];
    phist[n] = phist_c[n];
    phitt[n] = phitt_c[n];
  }
}

//----------------------------------------------------------------------------//
// field element: area
//
// template parameters:
//   Topology       element/basis topology (e.g. Triangle, Quad)
//   T              DOF type
//----------------------------------------------------------------------------//

template <class Tin, class Topology>
class Element<Tin, TopoD2, Topology> : public ElementBasis<TopoD2, Topology>
{
public:
  typedef ElementBasis<TopoD2, Topology> BaseType;
  typedef Tin T;
  typedef DLA::VectorS<TopoD2::D, T> gradT;
  typedef TopoD2 TopoDim;
  typedef typename BaseType::TopologyType TopologyType;
  typedef typename BaseType::RefCoordType RefCoordType;
  typedef BasisFunctionAreaBase<Topology> BasisType;

  Element();   // needed for new []
  explicit Element( const BasisFunctionAreaBase<Topology>* basis );
  Element( int order, const BasisFunctionCategory& category );
  Element( const Element& );
  ~Element();

  Element& operator=( const Element& );

  //Used for arrays of elements
  void setBasis( const BasisType* basis );

  // DOF accessors
        T& DOF( int n )       { return DOF_[n]; }
  const T& DOF( int n ) const { return DOF_[n]; }

  // provides a vector view of the DOFs
  DLA::VectorDView<T> vectorViewDOF() { return DLA::VectorDView<T>(DOF_, nDOF_); }

  // basis function
  using BaseType::basis;

  using BaseType::order;
  using BaseType::nDOF;

  // edge sign accessors
  using BaseType::edgeSign;
  using BaseType::traceOrientation;

  using BaseType::setEdgeSign;

  using BaseType::evalBasis;
  using BaseType::evalBasisDerivative;
  using BaseType::evalBasisHessianDerivative;

  void evalFromBasis( const Real phi[], int nphi, T& q ) const;
  template<int D>
  void evalFromBasis( const DLA::VectorS<D,Real> gradphi[], int nphi, DLA::VectorS<D,T> & gradq ) const;
  template<int D>
  void evalFromBasis( const DLA::MatrixSymS<D,Real> hessphi[], int nphi, DLA::MatrixSymS<D,T> & hessq ) const;

     T eval( const Real& sRef, const Real& tRef ) const;
  void eval( const Real& sRef, const Real& tRef, T& q ) const;
     T eval( const RefCoordType& ref ) const { return eval(ref[0], ref[1]); }
  void eval( const RefCoordType& ref, T& q ) const { eval(ref[0], ref[1], q); }
  void eval( const QuadraturePoint<TopoDim>& ref, T& q ) const;
  void eval( const QuadratureCellTracePoint<TopoDim>& ref, T& q ) const;

  void evalDerivative( const Real& sRef, const Real& tRef, T& qs, T& qt ) const;
  void evalDerivative( const RefCoordType& Ref, DLA::VectorS<TopoD2::D, T>& dq ) const { evalDerivative(Ref[0], Ref[1], dq[0], dq[1]); }
  void evalDerivative( const QuadraturePoint<TopoDim>& ref, DLA::VectorS<TopoDim::D, T>& dq ) const;
  void evalDerivative( const QuadratureCellTracePoint<TopoDim>& ref, DLA::VectorS<TopoDim::D, T>& dq ) const;

  // project DOFs onto another polynomial order
  void projectTo( Element& ) const;

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

protected:
  using BaseType::order_;     // polynomial order (e.g. order=1 is linear)
  using BaseType::nDOF_;      // total DOFs in element
  T* DOF_;                    // DOFs

  using BaseType::basis_;
  using BaseType::phi_;            // temporary storage for basis function evaluation
  using BaseType::phis_;
  using BaseType::phit_;
  using BaseType::phiss_;
  using BaseType::phist_;
  using BaseType::phitt_;

  using BaseType::pointStoreCell_;  // cached basis functions evaluated at quadrature points
  using BaseType::pointStoreTrace_; // cached basis functions evaluated at quadrature points on the trace of the element

  using BaseType::edgeSign_;       // +/- sign for edge orientations (left/right element)
};


// default ctor needed for 'new []'
template <class T, class Topology>
Element<T, TopoD2, Topology>::Element() : BaseType()
{
  DOF_  = NULL;
}


template <class T, class Topology>
Element<T, TopoD2, Topology>::Element(
    const BasisFunctionAreaBase<Topology>* basis ) : BaseType(basis)
{
  DOF_  = new T[nDOF_];
}


template <class T, class Topology>
Element<T, TopoD2, Topology>::Element( int order, const BasisFunctionCategory& category )
  : BaseType(order, category)
{
  DOF_  = new T[nDOF_];
}


template <class T, class Topology>
Element<T, TopoD2, Topology>::Element( const Element& a )
  : BaseType( a )
{
  DOF_  = new T[nDOF_];
  for (int n = 0; n < nDOF_; n++)
    DOF_[n] = a.DOF_[n];
}


template <class T, class Topology>
Element<T, TopoD2, Topology>::~Element()
{
  delete [] DOF_;
}

template <class T, class Topology>
Element<T, TopoD2, Topology>& // cppcheck-suppress operatorEqVarError
Element<T, TopoD2, Topology>::operator=( const Element& a )
{
  if (this != &a)
  {
    BaseType::operator=(a);

    delete [] DOF_; DOF_ = NULL;

    DOF_  = new T[nDOF_];
    for (int n = 0; n < nDOF_; n++)
      DOF_[n] = a.DOF_[n];
  }

  return *this;
}

template <class T, class Topology>
void
Element<T, TopoD2, Topology>::setBasis(
    const BasisFunctionAreaBase<Topology>* basis )
{
  BaseType::setBasis(basis);

  delete [] DOF_; DOF_ = NULL;

  DOF_  = new T[nDOF_];
}


template <class T, class Topology>
void
Element<T, TopoD2, Topology>::evalFromBasis(
    const Real phi[], int nphi, T& q ) const
{
  SANS_ASSERT_MSG(nphi == nDOF_, "nphi = %d, nDOF_ = %d", nphi, nDOF_);
  q = 0;
  for (int n = 0; n < nDOF_; n++)
    q += phi[n]*DOF_[n];
}

template <class T, class Topology>
template <int D>
void
Element<T, TopoD2, Topology>::evalFromBasis(
    const DLA::VectorS<D,Real> gradphi[], int nphi, DLA::VectorS<D,T>& gradq ) const
{
  SANS_ASSERT_MSG(nphi == nDOF_, "nphi = %d, nDOF_ = %d", nphi, nDOF_);
  gradq = 0;
  for (int n = 0; n < nDOF_; n++)
    for (int i = 0; i < D; i++)
      gradq[i] += gradphi[n][i]*DOF_[n];
}

template <class T, class Topology>
template <int D>
void
Element<T, TopoD2, Topology>::evalFromBasis(
    const DLA::MatrixSymS<D,Real> hessphi[], int nphi, DLA::MatrixSymS<D,T>& hessq ) const
{
  SANS_ASSERT_MSG(nphi == nDOF_, "nphi = %d, nDOF_ = %d", nphi, nDOF_);
  hessq = 0;
  for (int n = 0; n < nDOF_; n++)
    for (int i = 0; i < D; i++)
      for (int j = 0; j <=i; j++) // because it is lower triangular
        hessq(i,j) += hessphi[n](i,j)*DOF_[n];
}


template <class T, class Topology>
T
Element<T, TopoD2, Topology>::eval(
    const Real& sRef, const Real& tRef ) const
{
  basis_->evalBasis( sRef, tRef, edgeSign_, phi_, nDOF_ );
  T q;

  q = 0;
  for (int n = 0; n < nDOF_; n++)
    q += phi_[n]*DOF_[n];

  return q;
}

template <class T, class Topology>
void
Element<T, TopoD2, Topology>::eval(
    const Real& sRef, const Real& tRef, T& q ) const
{
  basis_->evalBasis( sRef, tRef, edgeSign_, phi_, nDOF_ );

  q = 0;
  for (int n = 0; n < nDOF_; n++)
    q += phi_[n]*DOF_[n];
}

template <class T, class Topology>
void
Element<T, TopoD2, Topology>::eval(
    const QuadraturePoint<TopoDim>& ref, T& q ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    eval(ref.ref, q);
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoDim, Topology>::get(pointStoreCell_, ref, edgeSign_);
  q = 0;
  for (int n = 0; n < nDOF_; n++)
    q += cache.phi[n]*DOF_[n];
}


template <class T, class Topology>
void
Element<T, TopoD2, Topology>::eval(
    const QuadratureCellTracePoint<TopoDim>& ref, T& q ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    eval(ref.ref, q);
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoD2, Topology>::get(pointStoreTrace_, ref, edgeSign_);
  q = 0;
  for (int n = 0; n < nDOF_; n++)
    q += cache.phi[n]*DOF_[n];
}


template <class T, class Topology>
void
Element<T, TopoD2, Topology>::evalDerivative(
    const Real& sRef, const Real& tRef, T& qs, T& qt ) const
{
  evalBasisDerivative( sRef, tRef, phis_, phit_, nDOF_ );

  qs = 0;
  qt = 0;
  for (int n = 0; n < nDOF_; n++)
  {
    qs += phis_[n]*DOF_[n];
    qt += phit_[n]*DOF_[n];
  }
}

template <class T, class Topology>
void
Element<T, TopoD2, Topology>::evalDerivative(
    const QuadraturePoint<TopoDim>& ref, DLA::VectorS<TopoDim::D, T>& dq ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalDerivative(ref.ref, dq);
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoDim, Topology>::get(pointStoreCell_, ref, edgeSign_);
  const std::vector<Real>& phis = cache.dphi[0];
  const std::vector<Real>& phit = cache.dphi[1];
  dq = 0;
  for (int n = 0; n < nDOF_; n++)
  {
    dq[0] += phis[n]*DOF_[n];
    dq[1] += phit[n]*DOF_[n];
  }
}

template <class T, class Topology>
void
Element<T, TopoD2, Topology>::evalDerivative(
    const QuadratureCellTracePoint<TopoDim>& ref, DLA::VectorS<TopoDim::D, T>& dq ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalDerivative(ref.ref, dq);
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoD2, Topology>::get(pointStoreTrace_, ref, edgeSign_);
  const std::vector<Real>& phis = cache.dphi[0];
  const std::vector<Real>& phit = cache.dphi[1];
  dq = 0;
  for (int n = 0; n < nDOF_; n++)
  {
    dq[0] += phis[n]*DOF_[n];
    dq[1] += phit[n]*DOF_[n];
  }
}

// project DOFs onto another polynomial order
template <class T, class Topology>
void
Element<T, TopoD2, Topology>::projectTo( Element& ElemTo ) const
{
  BasisFunctionArea_projectTo( basis_, DOF_, nDOF_, ElemTo.basis_, ElemTo.DOF_, ElemTo.nDOF_ );
  ElemTo.setEdgeSign(edgeSign_);
}


template <class T, class Topology>
void
Element<T, TopoD2, Topology>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "Element<T, TopoD2, Topology>:"
      << "  order_ = " << order_ << "  nDOF_ = " << nDOF_;
  out << "  edgeSign_ =";
  for (int n = 0; n < Topology::NEdge; n++)
    out << " " << edgeSign_[n];
  out << std::endl;
  if (DOF_ != NULL)
  {
    out << indent << indent << "DOF_ = ";
    for (int n = 0; n < nDOF_; n++)
      out << "(" << DOF_[n] << ") ";
    out << std::endl;
  }
  if ( basis_ != NULL )
  {
    out << indent << indent << "basis_:" << std::endl;
    basis_->dump( indentSize += 2, out );
  }
}

}

#endif  // ELEMENTAREA_H
