// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "ElementAssociativityVolume.h"
#include "tools/SANSException.h"

namespace SANS
{

// NOTE: default ctor needed for 'new []'
template <class Topology>
ElementAssociativity<TopoD3,Topology>::ElementAssociativity()
{
  rank_  = -1;
  order_ = -1;
  nNode_ = 0;
  nEdge_ = 0;
  nFace_ = 0;
  nCell_ = 0;

  nodeList_ = NULL;
  edgeList_ = NULL;
  faceList_ = NULL;
  cellList_ = NULL;

  for (int n = 0; n < Topology::NFace; n++)
    faceSign_[n] = +1;
}


template <class Topology>
ElementAssociativity<TopoD3,Topology>::ElementAssociativity( const BasisType* basis )
{
  rank_  = -1;
  order_ = basis->order();
  nNode_ = basis->nBasisNode();
  nEdge_ = basis->nBasisEdge();
  nFace_ = basis->nBasisFace();
  nCell_ = basis->nBasisCell();

  nodeList_ = NULL;
  edgeList_ = NULL;
  faceList_ = NULL;
  cellList_ = NULL;


  if (nNode_ != 0)
  {
    nodeList_ = new int[nNode_];
    for (int n = 0; n < nNode_; n++)
      nodeList_[n] = -1;
  }

  if (nEdge_ != 0)
  {
    edgeList_ = new int[nEdge_];
    for (int n = 0; n < nEdge_; n++)
      edgeList_[n] = -1;
  }

  if (nFace_ != 0)
  {
    faceList_ = new int[nFace_];
    for (int n = 0; n < nFace_; n++)
      faceList_[n] = -1;
  }

  if (nCell_ != 0)
  {
    cellList_ = new int[nCell_];
    for (int n = 0; n < nCell_; n++)
      cellList_[n] = -1;
  }

  for (int n = 0; n < Topology::NFace; n++)
    faceSign_[n] = +1;
}


template <class Topology>
ElementAssociativity<TopoD3,Topology>::ElementAssociativity( const ElementAssociativity<TopoD3,Topology>& a )
  : ElementAssociativity()
{
  operator=(a);
}


template <class Topology>
ElementAssociativity<TopoD3,Topology>::~ElementAssociativity()
{
  delete [] nodeList_;
  delete [] edgeList_;
  delete [] faceList_;
  delete [] cellList_;
}


template <class Topology>
ElementAssociativity<TopoD3,Topology>&
ElementAssociativity<TopoD3,Topology>::operator=( const ElementAssociativity<TopoD3,Topology>& a )
{
  if (this != &a)
  {
    rank_  = a.rank_;
    order_ = a.order_;
    nNode_ = a.nNode_;
    nEdge_ = a.nEdge_;
    nFace_ = a.nFace_;
    nCell_ = a.nCell_;

    delete [] nodeList_; nodeList_ = NULL;
    delete [] edgeList_; edgeList_ = NULL;
    delete [] faceList_; faceList_ = NULL;
    delete [] cellList_; cellList_ = NULL;

    if (nNode_ != 0)
    {
      nodeList_ = new int[nNode_];
      for (int n = 0; n < nNode_; n++)
        nodeList_[n] = a.nodeList_[n];
    }

    if (nEdge_ != 0)
    {
      edgeList_ = new int[nEdge_];
      for (int n = 0; n < nEdge_; n++)
        edgeList_[n] = a.edgeList_[n];
    }

    if (nFace_ != 0)
    {
      faceList_ = new int[nFace_];
      for (int n = 0; n < nFace_; n++)
        faceList_[n] = a.faceList_[n];
    }

    if (nCell_ != 0)
    {
      cellList_ = new int[nCell_];
      for (int n = 0; n < nCell_; n++)
        cellList_[n] = a.cellList_[n];
    }

    for (int n = 0; n < Topology::NFace; n++)
      faceSign_[n] = a.faceSign_[n];
  }

  return *this;
}

template <class Topology>
ElementAssociativity<TopoD3,Topology>&
ElementAssociativity<TopoD3,Topology>::operator=( const ElementAssociativityConstructor<TopoD3, Topology>& a )
{
  rank_  = a.rank();
  order_ = a.order();
  nNode_ = a.nNode();
  nEdge_ = a.nEdge();
  nFace_ = a.nFace();
  nCell_ = a.nCell();

  delete [] nodeList_; nodeList_ = NULL;
  delete [] edgeList_; edgeList_ = NULL;
  delete [] faceList_; faceList_ = NULL;
  delete [] cellList_; cellList_ = NULL;

  if (nNode_ != 0)
  {
    nodeList_ = new int[nNode_];
    for (int n = 0; n < nNode_; n++)
      nodeList_[n] = a.nodeGlobal(n);
  }

  if (nEdge_ != 0)
  {
    edgeList_ = new int[nEdge_];
    for (int n = 0; n < nEdge_; n++)
      edgeList_[n] = a.edgeGlobal(n);
  }

  if (nFace_ != 0)
  {
    faceList_ = new int[nFace_];
    for (int n = 0; n < nFace_; n++)
      faceList_[n] = a.faceGlobal(n);
  }

  if (nCell_ != 0)
  {
    cellList_ = new int[nCell_];
    for (int n = 0; n < nCell_; n++)
      cellList_[n] = a.cellGlobal(n);
  }

  faceSign_ = a.faceSign();

  return *this;
}


template <class Topology>
void
ElementAssociativity<TopoD3,Topology>::getNodeGlobalMapping( int node[], int nnode ) const
{
  SANS_ASSERT( nnode == nNode_ );

  for (int n = 0; n < nNode_; n++)
    node[n] = nodeList_[n];
}

template <class Topology>
void
ElementAssociativity<TopoD3,Topology>::getEdgeGlobalMapping( int edge[], int nedge ) const
{
  SANS_ASSERT_MSG( nedge == nEdge_, " nedge = %d  nEdge_ = %d", nedge, nEdge_ );

  for (int n = 0; n < nEdge_; n++)
    edge[n] = edgeList_[n];
}

template <class Topology>
void
ElementAssociativity<TopoD3,Topology>::getFaceGlobalMapping( int face[], int nface ) const
{
  SANS_ASSERT_MSG( nface == nFace_, " nface = %d  nFace_ = %d", nface, nFace_ );

  for (int n = 0; n < nFace_; n++)
    face[n] = faceList_[n];
}

template <class Topology>
void
ElementAssociativity<TopoD3,Topology>::getCellGlobalMapping( int cell[], int ncell ) const
{
  SANS_ASSERT( ncell == nCell_ );

  for (int n = 0; n < nCell_; n++)
    cell[n] = cellList_[n];
}

template <class Topology>
void
ElementAssociativity<TopoD3,Topology>::getGlobalMapping( int map[], int ndof ) const
{
#if 0
  std::cout << "ElementAssociativity<TopoD3,Topology>::getGlobalMapping  dumping..." << std::endl;
  this->dump(2);
#endif
  SANS_ASSERT( ndof == nNode_ + nEdge_ + nFace_ + nCell_ );

  int offset = 0;
  for (int n = 0; n < nNode_; n++)
    map[n + offset] = nodeList_[n];
  offset += nNode_;

  for (int n = 0; n < nEdge_; n++)
    map[n + offset] = edgeList_[n];
  offset += nEdge_;

  for (int n = 0; n < nFace_; n++)
    map[n + offset] = faceList_[n];
  offset += nFace_;

  for (int n = 0; n < nCell_; n++)
    map[n + offset] = cellList_[n];
}



template <class Topology>
void
ElementAssociativity<TopoD3,Topology>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "ElementAssociativity<TopoD3>: order_ = " << order_
      << "  nNode_ = " << nNode_
      << "  nEdge_ = " << nEdge_
      << "  nCell_ = " << nCell_;
  out << "  faceSign_ =";

  for (int n = 0; n < Topology::NFace; n++)
    out << " " << faceSign_[n];
  out << std::endl;

  if (nNode_ > 0)
  {
    out << indent << indent << "nodeList =";
    for (int n = 0; n < nNode_; n++)
      out << " " << nodeList_[n];
    out << std::endl;
  }

  if (nEdge_ > 0)
  {
    out << indent << indent << "edgeList =";
    for (int n = 0; n < nEdge_; n++)
      out << " " << edgeList_[n];
    out << std::endl;
  }

  if (nFace_ > 0)
  {
    out << indent << indent << "faceList =";
    for (int n = 0; n < nFace_; n++)
      out << " " << faceList_[n];
    out << std::endl;
  }

  if (nCell_ > 0)
  {
    out << indent << indent << "cellList =";
    for (int n = 0; n < nCell_; n++)
      out << " " << cellList_[n];
    out << std::endl;
  }
}

// Explicitly instantiate the classes
template class ElementAssociativity<TopoD3,Tet>;
template class ElementAssociativity<TopoD3,Hex>;

}
