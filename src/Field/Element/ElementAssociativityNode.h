// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ELEMENTASSOCIATIVITYNODE_H
#define ELEMENTASSOCIATIVITYNODE_H

// Element line associativity (local to global mappings)

#include <ostream>
#include <vector>
#include <array>

#include "BasisFunction/BasisFunctionNode.h"
#include "ElementAssociativityNodeConstructor.h"
#include "ElementNode.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// edge associativity: global numberings for single element nodes/edge
//
// member functions:
//   .order                     polynomial order
//   .nNode                     # node DOFs
//   .nEdge                     # edge interior DOFs
//   .node/edgeGlobal           global node/edge DOF accessors
//   .getNodeGlobalMapping      local-to-global node DOF mapping
//   .getEdgeGlobalMapping      local-to-global edge DOF mapping
//
//   .dump            debug dump of private data
//----------------------------------------------------------------------------//

template<class TopoDim, class Topology>
class ElementAssociativity;

template<>
class ElementAssociativity<TopoD0, Node>
{
public:
  static const int DElement = 0;                           // element dimensions

  typedef BasisFunctionNodeBase BasisType;

  typedef ElementAssociativityConstructor<TopoD0, Node> Constructor;

  ElementAssociativity();
  explicit ElementAssociativity( int order );
  explicit ElementAssociativity( const BasisType* basis );
  ElementAssociativity( const ElementAssociativity& a );
  explicit ElementAssociativity( const Constructor& a) : ElementAssociativity<TopoD0,Node>() { operator=(a); }
  ~ElementAssociativity();

  ElementAssociativity& operator=( const ElementAssociativity& );
  ElementAssociativity& operator=( const Constructor& );

  int rank() const  { return rank_; }
  int order() const { return 0; }
  int nNode() const { return 1; }

  // node, edge maps
  int nodeGlobal( int n ) const { return nodeList_; }

  void getNodeGlobalMapping( int node[], int nnode ) const;
  void getNodeGlobalMapping( std::vector<int>& node ) const;
  void getEdgeGlobalMapping( int edge[], int nedge ) const;
  void getEdgeGlobalMapping( std::vector<int>& edge ) const;
  void getGlobalMapping(     int map[] , int ndof ) const;

  template<class ElemT, class T>
  void getElement(       Element<ElemT, TopoD0, Node>& fldElem, const T* DOF, const int nDOF  ) const;
  template<class ElemT, class T>
  void setElement( const Element<ElemT, TopoD0, Node>& fldElem,       T* DOF, const int nDOF  ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

  int normalSignL() const { return normalSignL_; }
  int normalSignR() const { return normalSignR_; }

  // dummy function that really should never be called
  std::array<int,1> traceOrientation() const { return std::array<int,1>{{0}}; }

private:
  int rank_;                      // the processor rank that possesses this element
  int nodeList_;                 // global ordering of node DOFs
  int normalSignL_;              // trace normal sign with respect to the left element
  int normalSignR_;              // trace normal sign with respect to the right element
};


// extract DOFs for individual element
template<class ElemT, class T>
void
ElementAssociativity<TopoD0,Node>::getElement( Element<ElemT, TopoD0, Node>& fldElem, const T* DOF, const int nDOF ) const
{
  SANS_ASSERT( (DOF != NULL) );
  fldElem.setRank( rank_ );
  fldElem.DOF(0) = DOF[nodeList_];
  fldElem.normalSignL() = normalSignL_;
  fldElem.normalSignR() = normalSignR_;
}


// set DOFs for individual element
template<class ElemT, class T>
void
ElementAssociativity<TopoD0,Node>::setElement( const Element<ElemT, TopoD0, Node>& fldElem, T* DOF, const int nDOF  ) const
{
  SANS_ASSERT( (DOF != NULL) );
  DOF[nodeList_] = fldElem.DOF(0);
}

}
#endif  // ELEMENTASSOCIATIVITYNODE_H
