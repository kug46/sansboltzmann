// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef GALERKINWEIGHTEDINTEGRAL_H
#define GALERKINWEIGHTEDINTEGRAL_H

// volume/area/line element integrals: functor based where the integrands return an array of values, i.e. from weighting

#include <type_traits>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "Quadrature/Quadrature.h"
#include "BasisFunction/Quadrature_Cache.h"
#include "Element.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Galerkin weighted integral
template<class TopoDim, class Topology, class IntegrandType1, class IntegrandType2 = std::nullptr_t,
                                                              class IntegrandType3 = std::nullptr_t>
class GalerkinWeightedIntegral;

//----------------------------------------------------------------------------//
// Single element integral

template<class TopoDim, class Topology, class IntegrandType>
class GalerkinWeightedIntegral<TopoDim, Topology, IntegrandType, std::nullptr_t, std::nullptr_t>
{
public:
  typedef QuadraturePoint<TopoDim> QuadPoint;

  GalerkinWeightedIntegral( const GalerkinWeightedIntegral& ) = delete;
  GalerkinWeightedIntegral& operator=( const GalerkinWeightedIntegral& ) = delete;

  GalerkinWeightedIntegral(const int quadratureorder, const int nIntegrand) :
    quadrature_(quadratureorder), nIntegrand_(nIntegrand), integrands_(new IntegrandType[nIntegrand]) {}

  ~GalerkinWeightedIntegral() { delete [] integrands_; }

  template <class PhysDim, class IntegrandFunctor, class IntegralsT>
  void operator()( const IntegrandFunctor& fcn,
                   const ElementXField<PhysDim, TopoDim, Topology>& xfldElem,
                   IntegralsT* __restrict integrals, const int nIntegrals )
  {
    SANS_ASSERT( nIntegrals == nIntegrand_ );

    Real dJ;                          // incremental Jacobian determinant
    Real weight;                      // quadrature weight

    for (int k = 0; k < nIntegrals; k++)
      integrals[k] = 0;

    //if (! fcn.needsEvaluation())
    //  return;

    // loop over quadrature points
    const int nquad = quadrature_.nQuadrature();
    for (int iquad = 0; iquad < nquad; iquad++)
    {
      quadrature_.weight( iquad, weight );
      QuadPoint sRef = quadrature_.coordinates_cache( iquad );

      dJ = weight * xfldElem.jacobianDeterminant( sRef );

      fcn( sRef, integrands_, nIntegrand_ );

      for (int k = 0; k < nIntegrals; k++)
        integrals[k] += dJ*integrands_[k];
    }
  }

protected:
  const Quadrature<TopoDim, Topology> quadrature_;
  const int nIntegrand_;
  IntegrandType* integrands_;

};

//----------------------------------------------------------------------------//
// Integral for two elements (e.g. DG trace integral)

template<class TopoDim, class Topology, class IntegrandType1, class IntegrandType2>
class GalerkinWeightedIntegral<TopoDim, Topology, IntegrandType1, IntegrandType2, std::nullptr_t>
{
public:
  typedef QuadraturePoint<TopoDim> QuadPoint;

  GalerkinWeightedIntegral( const GalerkinWeightedIntegral& ) = delete;
  GalerkinWeightedIntegral& operator=( const GalerkinWeightedIntegral& ) = delete;

  GalerkinWeightedIntegral(const int quadratureorder, const int nIntegrand1, const int nIntegrand2) :
    quadrature_(quadratureorder), nIntegrand1_(nIntegrand1), nIntegrand2_(nIntegrand2),
    integrands1_(new IntegrandType1[nIntegrand1]), integrands2_(new IntegrandType2[nIntegrand2]) {}

  ~GalerkinWeightedIntegral() { delete [] integrands1_; delete [] integrands2_; }

  template <class PhysDim, class IntegrandFunctor>
  void operator()( const IntegrandFunctor& fcn,
                   const ElementXField<PhysDim, TopoDim, Topology>& xfldTrace,
                   IntegrandType1* __restrict integrals1, const int nIntegrals1,
                   IntegrandType2* __restrict integrals2, const int nIntegrals2 )
  {
    SANS_ASSERT( nIntegrals1 == nIntegrand1_ );
    SANS_ASSERT( nIntegrals2 == nIntegrand2_ );

    Real dJ;                          // incremental Jacobian determinant
    Real weight;                      // quadrature weight

    for (int k = 0; k < nIntegrals1; k++)
      integrals1[k] = 0;

    for (int k = 0; k < nIntegrals2; k++)
      integrals2[k] = 0;

    //if (! fcn.needsEvaluation())
    //  return;

    // loop over quadrature points
    const int nquad = quadrature_.nQuadrature();
    for (int iquad = 0; iquad < nquad; iquad++)
    {
      quadrature_.weight( iquad, weight );
      QuadPoint sRef = quadrature_.coordinates_cache( iquad );

      dJ = weight * xfldTrace.jacobianDeterminant( sRef );

      fcn( sRef, integrands1_, nIntegrand1_, integrands2_, nIntegrand2_ );

      for (int k = 0; k < nIntegrals1; k++)
        integrals1[k] += dJ*integrands1_[k];

      for (int k = 0; k < nIntegrals2; k++)
        integrals2[k] += dJ*integrands2_[k];
    }
  }

protected:
  const Quadrature<TopoDim, Topology> quadrature_;
  const int nIntegrand1_;
  const int nIntegrand2_;
  IntegrandType1* integrands1_;
  IntegrandType2* integrands2_;
};

//----------------------------------------------------------------------------//
// Integral for three elements (e.g. HDG trace integral)

template<class TopoDim, class Topology, class IntegrandType1, class IntegrandType2, class IntegrandType3>
class GalerkinWeightedIntegral
{
public:
  typedef QuadraturePoint<TopoDim> QuadPoint;

  GalerkinWeightedIntegral( const GalerkinWeightedIntegral& ) = delete;
  GalerkinWeightedIntegral& operator=( const GalerkinWeightedIntegral& ) = delete;

  GalerkinWeightedIntegral(const int quadratureorder, const int nIntegrand1, const int nIntegrand2, const int nIntegrand3) :
    quadrature_(quadratureorder), nIntegrand1_(nIntegrand1), nIntegrand2_(nIntegrand2), nIntegrand3_(nIntegrand3),
    integrands1_(new IntegrandType1[nIntegrand1]), integrands2_(new IntegrandType2[nIntegrand2]),
    integrands3_(new IntegrandType3[nIntegrand3]) {}

  ~GalerkinWeightedIntegral() { delete [] integrands1_; delete [] integrands2_; delete [] integrands3_; }

  template <class PhysDim, class IntegrandFunctor>
  void operator()( const IntegrandFunctor& fcn,
                   const ElementXField<PhysDim, TopoDim, Topology>& xfldTrace,
                   IntegrandType1* __restrict integrals1, const int nIntegrals1,
                   IntegrandType2* __restrict integrals2, const int nIntegrals2,
                   IntegrandType3* __restrict integrals3, const int nIntegrals3)
  {
    SANS_ASSERT( nIntegrals1 == nIntegrand1_ );
    SANS_ASSERT( nIntegrals2 == nIntegrand2_ );
    SANS_ASSERT( nIntegrals3 == nIntegrand3_ );

    Real dJ;                          // incremental Jacobian determinant
    Real weight;                      // quadrature weight

    for (int k = 0; k < nIntegrals1; k++)
      integrals1[k] = 0;

    for (int k = 0; k < nIntegrals2; k++)
      integrals2[k] = 0;

    for (int k = 0; k < nIntegrals3; k++)
      integrals3[k] = 0;

    //if (! fcn.needsEvaluation())
    //  return;

    // loop over quadrature points
    const int nquad = quadrature_.nQuadrature();
    for (int iquad = 0; iquad < nquad; iquad++)
    {
      quadrature_.weight( iquad, weight );
      QuadPoint sRef = quadrature_.coordinates_cache( iquad );

      dJ = weight * xfldTrace.jacobianDeterminant( sRef );

      fcn( sRef, integrands1_, nIntegrand1_, integrands2_, nIntegrand2_, integrands3_, nIntegrand3_ );

      for (int k = 0; k < nIntegrals1; k++)
        integrals1[k] += dJ*integrands1_[k];

      for (int k = 0; k < nIntegrals2; k++)
        integrals2[k] += dJ*integrands2_[k];

      for (int k = 0; k < nIntegrals3; k++)
        integrals3[k] += dJ*integrands3_[k];
    }
  }

protected:
  const Quadrature<TopoDim, Topology> quadrature_;
  const int nIntegrand1_;
  const int nIntegrand2_;
  const int nIntegrand3_;
  IntegrandType1* integrands1_;
  IntegrandType2* integrands2_;
  IntegrandType3* integrands3_;
};



}

#endif //GALERKINWEIGHTEDINTEGRAL_H
