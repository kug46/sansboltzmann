// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ELEMENTASSOCIATIVITYAREACONSTRUCTOR_H
#define ELEMENTASSOCIATIVITYAREACONSTRUCTOR_H

// Area associativity (local to global mappings)

#include <ostream>
#include <vector>
#include <array>

#include "tools/SANSException.h"
#include "BasisFunction/BasisFunctionArea.h"
#include "BasisFunction/CanonicalTraceToCell.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// area associativity: global numberings for element nodes/edges/cell
//
// template parameters:
//   Topology                   element topology (triangle/quad)
//                              Note: needed for basis function ctor
//
// member functions:
//   .order                     polynomial order
//   .nNode                     # node DOFs
//   .nEdge                     # edge DOFs
//   .nCell                     # cell interior DOFs
//   .node/edge/cellGlobal      global node/edge/cell DOF accessors
//   .set/getNodeGlobalMapping  local-to-global node DOF mapping
//   .set/getEdgeGlobalMapping  local-to-global edge DOF mapping
//   .set/getCellGlobalMapping  local-to-global cell DOF mapping
//
//   .dump            debug dump of private data
//----------------------------------------------------------------------------//

template <class TopoDim, class Topology>
class ElementAssociativityConstructor;

template <class Topology>
class ElementAssociativityConstructor<TopoD2, Topology>
{
public:
  typedef TopoD2 TopoDim;
  typedef Topology TopologyType;             // element topology (e.g. tri, quad)
  typedef BasisFunctionAreaBase<Topology> BasisType;
  typedef std::array<int,Topology::NEdge> IntNEdge;

  ElementAssociativityConstructor();
  explicit ElementAssociativityConstructor( const BasisType* basis ) : ElementAssociativityConstructor() { resize(basis); }
  ElementAssociativityConstructor( const ElementAssociativityConstructor& a ) : ElementAssociativityConstructor() { operator=(a); }
  ~ElementAssociativityConstructor();

  ElementAssociativityConstructor& operator=( const ElementAssociativityConstructor& );

  void resize( const BasisType* basis );

  int rank() const  { SANS_ASSERT(isSetRank_); return rank_; }
  int order() const { return order_; }
  int nNode() const { return nNode_; }
  int nEdge() const { return nEdge_; }
  int nCell() const { return nCell_; }
  int nDOF() const  { return nNode_ + nEdge_ + nCell_; }

  void setRank(const int rank) { isSetRank_ = true; rank_ = rank; }

  // node, edge, cell maps
  int nodeGlobal( int n ) const { SANS_ASSERT(isSetNodeGlobalMapping_[n]); return (nodeList_[n]); }
  int edgeGlobal( int n ) const { SANS_ASSERT(isSetEdgeGlobalMapping_[n]); return (edgeList_[n]); }
  int cellGlobal( int n ) const { SANS_ASSERT(isSetCellGlobalMapping_[n]); return (cellList_[n]); }

  void setNodeGlobalMapping( const int nodeMap[], int nnode );
  void getNodeGlobalMapping(       int nodeMap[], int nnode ) const;

  void setNodeGlobalMapping( const std::vector<int>& node );
  void getNodeGlobalMapping( std::vector<int>& nodeMap ) const { getNodeGlobalMapping( nodeMap.data(), nodeMap.size());}

  void setEdgeGlobalMapping( const int edgeMap[], int nedge );
  void getEdgeGlobalMapping(       int edgeMap[], int nedge ) const;
  void setEdgeGlobalMapping( const int edgeMap[], const int nsize, const CanonicalTraceToCell& canonicaledge );

  void setEdgeGlobalMapping( const std::vector<int>& edgeMap );


  void setCellGlobalMapping( const int cellMap[], int ncell );
  void getCellGlobalMapping(       int cellMap[], int ncell ) const;

  void setCellGlobalMapping( const std::vector<int>& cellMap );

  void setGlobalMapping( const int map[], int ndof );
  void getGlobalMapping(       int map[], int ndof ) const;

  void setGlobalMapping( const std::vector<int>& map );

  // edge sign accessors
        IntNEdge& edgeSign()       { return edgeSign_; }
  const IntNEdge& edgeSign() const { return edgeSign_; }
        IntNEdge& traceOrientation()       { return edgeSign_; }
  const IntNEdge& traceOrientation() const { return edgeSign_; }

  void setEdgeSign( int sgn, int canonicalEdge )
  {
    SANS_ASSERT( canonicalEdge >= 0 && canonicalEdge < Topology::NEdge );
    edgeSign_[canonicalEdge] = sgn;
  }

  void setOrientation( int sgn, int canonicalEdge ) { setEdgeSign(sgn, canonicalEdge); }

  void dump( int indentSize = 0, std::ostream& out = std::cout ) const;

private:
  int rank_;                      // the processor rank that possesses this element
  int order_;                     // polynomial order for node/edge DOFs
  int nNode_;                     // # node DOFs
  int nEdge_;                     // # edge DOFs
  int nCell_;                     // # cell DOFs
  int* nodeList_;                 // global ordering of node DOFs
  int* edgeList_;                 // global ordering of edge DOFs
  int* cellList_;                 // global ordering of cell DOFs

  bool isSetRank_;                 // flag: has the processor rank been set
  bool* isSetNodeGlobalMapping_;   // flag: is nodeList_ set yet?
  bool* isSetEdgeGlobalMapping_;   // flag: is edgeList_ set yet?
  bool* isSetCellGlobalMapping_;   // flag: is cellList_ set yet?

  IntNEdge edgeSign_;             // +/- sign for edge orientations (i.e. left/right element)
};

}

#endif  // ELEMENTASSOCIATIVITYAREACONSTRUCTOR_H
