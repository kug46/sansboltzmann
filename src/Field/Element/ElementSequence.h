// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ELEMENTSEQUENCE_H
#define ELEMENTSEQUENCE_H

#include <vector>

#include "BasisFunction/BasisFunctionCategory.h"
#include "Element.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Sequence of basis functions used to construct a sequence of elements
//----------------------------------------------------------------------------//

template <class BasisType>
class BasisFunctionSequence
{
public:
  BasisFunctionSequence(const int nbasis, const BasisType* basis) : nBasis(nbasis), basis(basis) {}

  const int nBasis;
  const BasisType* basis;
};

//----------------------------------------------------------------------------//
// A class for managing a sequence of elements
//----------------------------------------------------------------------------//

template <class T_, class TopoDim, class Topology>
class ElementSequence
{
public:
  typedef Element<T_,TopoDim,Topology> ElementType;
  typedef typename ElementType::BasisType BasisType;
  typedef Topology TopologyType;
  typedef DLA::VectorS<TopoDim::D,Real> RefCoordType;
  typedef T_ T;

  explicit ElementSequence( const BasisFunctionSequence<BasisType>& basis ) :
    ElementSequence(basis.basis, basis.nBasis) {}

  ElementSequence( const int order,
                   const BasisFunctionCategory& category,
                   const int nElem ):
    nElem_(nElem),
    basis_(BasisType::getBasisFunction(order, category))
  {
    ElementType elem(basis_);

    elems_.assign(nElem_, elem);
  }

  ElementSequence( const BasisType* basis, const int nElem ) : nElem_(nElem), basis_(basis)
  {
    ElementType elem(basis_);

    elems_.assign(nElem_, elem);
  }

  ElementSequence( const ElementSequence& elems ) : nElem_(elems.nElem_), basis_(elems.basis_), elems_(elems.elems_)
  {}
  ElementSequence& operator=( const ElementSequence& elems ) = delete;

  int nElem() const { return nElem_; }

        ElementType& operator[](const int i)       { return elems_[i]; }
  const ElementType& operator[](const int i) const { return elems_[i]; }

  int order() const { return basis_->order(); }
  int nDOFElem() const { return basis_->nDOF(); }
  const BasisType* basis() const { return basis_; }

protected:
  const int nElem_;
  const BasisType* basis_;
  std::vector < ElementType > elems_;
};


}

#endif //ELEMENTSEQUENCE_H
