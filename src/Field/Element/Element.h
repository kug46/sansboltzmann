// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ELEMENT_H
#define ELEMENT_H

#include <type_traits> // std::is_same

#include "Field/Tuple/FieldTuple_fwd.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Classes for identifying field classes
//----------------------------------------------------------------------------//

class ElementTypeBase {};

template< class Derived >
struct ElementType : ElementTypeBase
{
  //A convenient method for casting to the derived type
  const Derived& cast() const { return static_cast<const Derived&>(*this); }
};

//----------------------------------------------------------------------------//
// General element that is specialized based on the dimension
//----------------------------------------------------------------------------//

template <class TopoDim, class Topology>
class ElementBasis;

template <class T, class TopoDim, class Topology>
class Element;

//----------------------------------------------------------------------------//
// Grid element
//----------------------------------------------------------------------------//

template <class PhysDim, class TopoDim, class Topology>
class ElementXField;

//----------------------------------------------------------------------------//
// Element Lifting operators
//----------------------------------------------------------------------------//

template <class T, class TopoDim, class Topology>
class ElementLift;

//----------------------------------------------------------------------------//
// Element Sequence
//----------------------------------------------------------------------------//

template <class T, class TopoDim, class Topology>
class ElementSequence;

//----------------------------------------------------------------------------//
// Get functions needed so that XField elements may or may not be in a tuple
//----------------------------------------------------------------------------//

template <int k, class PhysDim, class TopoDim, class Topology>
const ElementXField<PhysDim, TopoDim, Topology>&
get(const ElementXField<PhysDim, TopoDim, Topology>& xfldElem)
{
  static_assert( k == 0 || k == -1, "k should be zero or -1 if the argument to get is ElementXField");
  return xfldElem;
}

template <class Type, class PhysDim, class TopoDim, class Topology>
const ElementXField<PhysDim, TopoDim, Topology>&
get(const ElementXField<PhysDim, TopoDim, Topology>& xfldElem)
{
  static_assert( ( std::is_same<Type, ElementXField<PhysDim, TopoDim, Topology> >::value // one is the typedef of the other
                   || std::is_base_of<ElementXField<PhysDim, TopoDim, Topology>, Type>::value // one is the base class of the other
                   || std::is_convertible<Type, ElementXField<PhysDim, TopoDim, Topology>>::value), // one can be converted to the other
                 "Type should be ElementXField or a derived class from it" );
  return xfldElem;
}

}

#endif //ELEMENT_H
