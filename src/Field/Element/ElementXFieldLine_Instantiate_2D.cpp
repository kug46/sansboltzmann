// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ELEMENTXFIELDLINE_INSTANTIATE
#include "ElementXFieldLine_impl.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Specialization: 2-D grid field line element
//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//
// compute unit normal vector of line element
//   rotating unit tangent clockwise by 90 degrees
void
ElementXField<PhysD2, TopoD1, Line>::unitNormal( const Real& sRef, Real& nx, Real& ny ) const
{
  evalBasisDerivative( sRef, phis_, nDOF_ );
  unitNormal( phis_, nx, ny );
}

void
ElementXField<PhysD2, TopoD1, Line>::unitNormal( const QuadraturePoint<TopoD1>& ref, VectorX& N ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    unitNormal(ref.ref[0], N[0], N[1]);
    return;
  }

  const QuadratureBasisPointValues<TopoD>& cache = QuadratureCacheValues<TopoD1, Line>::get( pointStoreCell_, ref, sgn_ );
  const std::vector<Real>& phis = cache.dphi[0];

  unitNormal( phis.data(), N[0], N[1] );
}

void
ElementXField<PhysD2, TopoD1, Line>::unitNormal( const Real phis[], Real& nx, Real& ny ) const
{
  Real xs, ys;
  Real ds;

  xs = 0;
  ys = 0;
  for (int i = 0; i < nDOF_; i++)
  {
    xs += phis[i]*DOF_[i](0);
    ys += phis[i]*DOF_[i](1);
  }

  ds = sqrt(xs*xs + ys*ys);
  nx = +ys/ds;
  ny = -xs/ds;
}

//----------------------------------------------------------------------------//
// compute surface gradient of unit tangent vector
//   i.e. nablas(ehat) = [ [nablas(ex)]
//                         [nablas(ey)] ]
//   for surface unit tangent ehat = [ex, ey]^T
// Documentation: pde/ShallowWater/Documentation/2D_Manifold_PDE.pdf --> Appendix
void
ElementXField<PhysD2, TopoD1, Line>::evalUnitTangentGradient(
    const Real& sRef, VectorX& ehat_x, VectorX& ehat_y) const
{
  //TODO: Take advantage of quadrature cache

  basis_->evalBasisDerivative( sRef, phis_, nDOF_ );
  basis_->evalBasisHessianDerivative( sRef, phiss_, nDOF_ );

  // J = [ dx/ds, dy/ds ]^T  Jacobian w.r.t. reference coordinate s
  // H = [ d^2x/ds^2, d^2y/ds^2 ]^T  Hessian w.r.t. reference coordinate s
  VectorX J = 0, H = 0;
  for (int n = 0; n < nDOF_; n++)
  {
    J += phis_[n]*DOF_[n];
    H += phiss_[n]*DOF_[n];
  }

  const Real x_s  = J[0], y_s  = J[1]; // dx/ds, dy/ds
  const Real x_ss = H[0], y_ss = H[1]; // d^2 x/ds^2, d^2 y/ds^2

  const Real absJ = sqrt( dot(J,J) ); // absolute value of J
  const Real tmp1 = 1/pow(absJ,3);
  const Real tmp2 = (x_s*x_ss + y_s*y_ss)/pow(absJ,5);

  ehat_x = x_s * (tmp1*H - tmp2*J); // d(ehat)/dx
  ehat_y = y_s * (tmp1*H - tmp2*J); // d(ehat)/dy

  // Note that the following gradients are different from d(ehat)/dx and d(ehat)/dy
  // grad(ex) = nablas(ex) = (x_ss*tmp1 - x_s*tmp2)*J; // gradient of x-componet of ehat
  // grad(ey) = nablas(ey) = (y_ss*tmp1 - y_s*tmp2)*J; // gradient of y-componet of ehat
}


//----------------------------------------------------------------------------//
// compute implied metric
template<class PhysDim>
void
ElementXFieldLineBase<PhysDim>::impliedMetric( const Real& s, TensorSymX& M ) const
{
  SANS_DEVELOPER_EXCEPTION("ElementXFieldLineBase<PhysD2>::impliedMetric - Not implemented yet.");

#if 0
  Matrix Jref; //jacobian from reference element to physical element
  jacobian( s, Jref );

  // transformation matrix from the unit equilateral element to the reference element (in 1D)
  const DLA::MatrixS<PhysD1::D,TopoD,Real>& invJeq = JacobianEquilateralTransform<PhysD1,TopoD1,Line>::invJeq;

  Matrix J = Jref*invJeq; //jacobian from equilateral element to physical element

  TensorSymX JJt = J*Transpose(J);

  // M = inv(JJt)
  Real det = JJt(0,0)*JJt(1,1) - JJt(0,1)*JJt(1,0);
  M(0,0) =  JJt(1,1)/det;
  M(1,0) = -JJt(1,0)/det;
  M(1,1) =  JJt(0,0)/det;
#endif
}

template <class PhysDim>
void
ElementXFieldLineBase<PhysDim>::evalReferenceCoordinateHessian( const Real& sRef, TensorSymX& shess ) const
{
  SANS_DEVELOPER_EXCEPTION("Not yet implemented for manifolds.");
#if 0
  SANS_ASSERT( PhysDim::D == 2 ); // specialized for 1-D

  SANS_ASSERT(nDOF_ > 0);  //Suppress clang analyzer warning

  // shess is d^2s/dx^2
  Real phiss[nDOF_];
  Real* phis = phi_;
  Real xs, xss;

  basis_->evalBasisDerivative( sRef, phis, nDOF_ );
  basis_->evalBasisHessianDerivative( sRef, phiss, nDOF_ );
  xs = 0; xss = 0;
  for (int n = 0; n < nDOF_; n++)
  {
    xs  += phis[n]*DOF_[n](0);
    xss += phiss[n]*DOF_[n](0);
  }

  shess(0,0) = -xss/(xs*xs*xs); // 1D shortcut
#endif
}


//----------------------------------------------------------------------------//
// Explicit instantiation
//----------------------------------------------------------------------------//
template class ElementXFieldLineBase<PhysD2>;

template
void
ElementXFieldLineBase<PhysD2>::evalBasisGradient<3>(const SurrealS<3>& sRef, const ElementBasis<TopoD1, Line>& fldElem,
                                                    DLA::VectorS<D,SurrealS<3>> gradphi[], int nphi ) const;

template
void
ElementXFieldLineBase<PhysD2>::evalBasisGradient<6>(const SurrealS<6>& sRef, const ElementBasis<TopoD1, Line>& fldElem,
                                                    DLA::VectorS<D,SurrealS<6>> gradphi[], int nphi ) const;
}
