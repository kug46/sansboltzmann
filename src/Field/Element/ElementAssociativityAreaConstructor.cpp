// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "ElementAssociativityAreaConstructor.h"
#include "Topology/ElementTopology.h"

namespace SANS
{

// NOTE: default ctor needed for 'new []'
template <class Topology>
ElementAssociativityConstructor<TopoD2, Topology>::ElementAssociativityConstructor()
{
  rank_  = -1;
  order_ = -1;
  nNode_ = 0;
  nEdge_ = 0;
  nCell_ = 0;

  nodeList_ = NULL;
  edgeList_ = NULL;
  cellList_ = NULL;

  isSetRank_ = false;
  isSetNodeGlobalMapping_ = NULL;
  isSetEdgeGlobalMapping_ = NULL;
  isSetCellGlobalMapping_ = NULL;

  for (int n = 0; n < Topology::NEdge; n++)
    edgeSign_[n] = +1;
}


template <class Topology>
void
ElementAssociativityConstructor<TopoD2, Topology>::resize( const BasisType* basis )
{
  rank_  = -1;
  order_ = basis->order();
  nNode_ = basis->nBasisNode();
  nEdge_ = basis->nBasisEdge();
  nCell_ = basis->nBasisCell();

  isSetRank_ = false;

  delete [] nodeList_; nodeList_ = NULL;
  delete [] isSetNodeGlobalMapping_; isSetNodeGlobalMapping_ = NULL;

  delete [] edgeList_; edgeList_ = NULL;
  delete [] isSetEdgeGlobalMapping_; isSetEdgeGlobalMapping_ = NULL;

  delete [] cellList_; cellList_ = NULL;
  delete [] isSetCellGlobalMapping_; isSetCellGlobalMapping_ = NULL;

  if (nNode_ != 0)
  {
    nodeList_ = new int[nNode_];
    isSetNodeGlobalMapping_ = new bool[nNode_];
    for (int n = 0; n < nNode_; n++)
    {
      nodeList_[n] = -1;
      isSetNodeGlobalMapping_[n] = false;
    }
  }

  if (nEdge_ != 0)
  {
    edgeList_ = new int[nEdge_];
    isSetEdgeGlobalMapping_ = new bool[nEdge_];
    for (int n = 0; n < nEdge_; n++)
    {
      edgeList_[n] = -1;
      isSetEdgeGlobalMapping_[n] = false;
    }
  }

  if (nCell_ != 0)
  {
    cellList_ = new int[nCell_];
    isSetCellGlobalMapping_ = new bool[nCell_];
    for (int n = 0; n < nCell_; n++)
    {
      cellList_[n] = -1;
      isSetCellGlobalMapping_[n] = false;
    }
  }

  for (int n = 0; n < Topology::NEdge; n++)
    edgeSign_[n] = +1;
}

template <class Topology>
ElementAssociativityConstructor<TopoD2, Topology>::~ElementAssociativityConstructor()
{
  delete [] nodeList_;
  delete [] edgeList_;
  delete [] cellList_;

  delete [] isSetNodeGlobalMapping_;
  delete [] isSetEdgeGlobalMapping_;
  delete [] isSetCellGlobalMapping_;
}

template <class Topology>
ElementAssociativityConstructor<TopoD2, Topology>&
ElementAssociativityConstructor<TopoD2, Topology>::operator=( const ElementAssociativityConstructor& a )
{
  if (this != &a)
  {
    rank_  = a.rank_;
    order_ = a.order_;
    nNode_ = a.nNode_;
    nEdge_ = a.nEdge_;
    nCell_ = a.nCell_;

    isSetRank_  = a.isSetRank_;

    delete [] nodeList_; nodeList_ = NULL;
    delete [] isSetNodeGlobalMapping_; isSetNodeGlobalMapping_ = NULL;

    delete [] edgeList_; edgeList_ = NULL;
    delete [] isSetEdgeGlobalMapping_; isSetEdgeGlobalMapping_ = NULL;

    delete [] cellList_; cellList_ = NULL;
    delete [] isSetCellGlobalMapping_; isSetCellGlobalMapping_ = NULL;

    if (nNode_ != 0)
    {
      nodeList_ = new int[nNode_];
      isSetNodeGlobalMapping_ = new bool[nNode_];
      for (int n = 0; n < nNode_; n++)
      {
        nodeList_[n] = a.nodeList_[n];
        isSetNodeGlobalMapping_[n] = a.isSetNodeGlobalMapping_[n];
      }
    }

    if (nEdge_ != 0)
    {
      edgeList_ = new int[nEdge_];
      isSetEdgeGlobalMapping_ = new bool[nEdge_];
      for (int n = 0; n < nEdge_; n++)
      {
        edgeList_[n] = a.edgeList_[n];
        isSetEdgeGlobalMapping_[n] = a.isSetEdgeGlobalMapping_[n];
      }
    }

    if (nCell_ != 0)
    {
      cellList_ = new int[nCell_];
      isSetCellGlobalMapping_ = new bool[nCell_];
      for (int n = 0; n < nCell_; n++)
      {
        cellList_[n] = a.cellList_[n];
        isSetCellGlobalMapping_[n] = a.isSetCellGlobalMapping_[n];
      }
    }

    edgeSign_ = a.edgeSign_;
  }

  return *this;
}


template <class Topology>
void
ElementAssociativityConstructor<TopoD2, Topology>::setNodeGlobalMapping( const int node[], int nnode )
{
  SANS_ASSERT_MSG( nnode == nNode_, "nnode = %d  nNode_ = %d", nnode, nNode_ );

  for (int n = 0; n < nNode_; n++)
  {
    nodeList_[n] = node[n];
    isSetNodeGlobalMapping_[n] = true;
  }
}


template <class Topology>
void
ElementAssociativityConstructor<TopoD2, Topology>::getNodeGlobalMapping( int node[], int nnode ) const
{
#if 0
  if ( isSetNodeGlobalMapping_ && (nnode == nNode_) )
  {}
  else
  { std::cout << "getNodeGlobalMapping: nnode = " << nnode << "  dumping..." << std::endl; dump(2); }
#endif
  SANS_ASSERT_MSG( nnode == nNode_, "nnode = %d  nNode_ = %d", nnode, nNode_ );

  for (int n = 0; n < nNode_; n++)
  {
    SANS_ASSERT_MSG( isSetNodeGlobalMapping_[n], "n = %d", n );
    node[n] = nodeList_[n];
  }
}

template <class Topology>
void
ElementAssociativityConstructor<TopoD2, Topology>::setNodeGlobalMapping( const std::vector<int>& node )
{
  SANS_ASSERT_MSG( (int)node.size() == nNode_, "node.size() = %d  nNode_ = %d", (int)node.size(), nNode_ );

  for (std::size_t n = 0; n < node.size(); n++)
  {
    nodeList_[n] = node[n];
    isSetNodeGlobalMapping_[n] = true;
  }
}


template <class Topology>
void
ElementAssociativityConstructor<TopoD2, Topology>::setEdgeGlobalMapping( const int edge[], int nedge )
{
  SANS_ASSERT_MSG( nedge == nEdge_, "nedge = %d  nEdge_ = %d", nedge, nEdge_ );

  for (int n = 0; n < nEdge_; n++)
  {
    edgeList_[n] = edge[n];
    isSetEdgeGlobalMapping_[n] = true;
  }
}


template <class Topology>
void
ElementAssociativityConstructor<TopoD2, Topology>::getEdgeGlobalMapping( int edge[], int nedge ) const
{
  SANS_ASSERT_MSG( nedge == nEdge_, " nedge = %d  nEdge_ = %d", nedge, nEdge_ );

  for (int n = 0; n < nEdge_; n++)
  {
    SANS_ASSERT_MSG( isSetEdgeGlobalMapping_[n], "n = %d", n );
    edge[n] = edgeList_[n];
  }
}


template <class Topology>
void
ElementAssociativityConstructor<TopoD2, Topology>::setEdgeGlobalMapping( const int edgeMap[], const int nsize,
                                                                     const CanonicalTraceToCell& canonicaledge )
{
  SANS_ASSERT_MSG( (canonicaledge.trace >= 0) && (canonicaledge.trace < Topology::NEdge), "[canonicaledge.trace = %d]", canonicaledge.trace );
  SANS_ASSERT_MSG( nsize*(Topology::NEdge) == nEdge_, "[nsize = %d  nEdge_ = %d]", nsize, nEdge_ );

  for (int n = 0; n < nsize; n++)
  {
    edgeList_[nsize*canonicaledge.trace + n] = edgeMap[n];
    isSetEdgeGlobalMapping_[nsize*canonicaledge.trace + n] = true;
  }
}


template <class Topology>
void
ElementAssociativityConstructor<TopoD2, Topology>::setEdgeGlobalMapping( const std::vector<int>& edge )
{
  SANS_ASSERT_MSG( (int)edge.size() == nEdge_, "edge.size() = %d, nEdge_ = %d\n", edge.size(), nEdge_ );

  for (int n = 0; n < nEdge_; n++)
  {
    edgeList_[n] = edge[n];
    isSetEdgeGlobalMapping_[n] = true;
  }
}

template <class Topology>
void
ElementAssociativityConstructor<TopoD2, Topology>::setCellGlobalMapping( const std::vector<int>& cell )
{
  SANS_ASSERT_MSG( (int)cell.size() == nCell_, "cell.size() = %d  nCell_ = %d", (int)cell.size(), nCell_ );

  for (int n = 0; n < nCell_; n++)
  {
    cellList_[n] = cell[n];
    isSetCellGlobalMapping_[n] = true;
  }
}

template <class Topology>
void
ElementAssociativityConstructor<TopoD2, Topology>::setCellGlobalMapping( const int cell[], int ncell )
{
  SANS_ASSERT_MSG( ncell == nCell_, "ncell = %d  nCell_ = %d", ncell, nCell_ );

  for (int n = 0; n < nCell_; n++)
  {
    cellList_[n] = cell[n];
    isSetCellGlobalMapping_[n] = true;
  }
}


template <class Topology>
void
ElementAssociativityConstructor<TopoD2, Topology>::getCellGlobalMapping( int cell[], int ncell ) const
{
  SANS_ASSERT_MSG( ncell == nCell_, "ncell = %d  nCell_ = %d", ncell, nCell_ );

  for (int n = 0; n < nCell_; n++)
  {
    SANS_ASSERT_MSG( isSetCellGlobalMapping_[n], "n = %d", n );
    cell[n] = cellList_[n];
  }
}


template <class Topology>
void
ElementAssociativityConstructor<TopoD2, Topology>::setGlobalMapping( const int map[], int ndof )
{
#if 0
  std::cout << "ElementAssociativityConstructor<TopoD2, Topology>::setGlobalMapping  dumping..." << std::endl;
  this->dump(2);
#endif
  SANS_ASSERT_MSG( ndof == nNode_ + nEdge_ + nCell_, "ndof = %d  nNode_ + nEdge_ + nCell_ = %d", ndof, nNode_ + nEdge_ + nCell_ );

  int offset = 0;
  for (int n = 0; n < nNode_; n++)
  {
    nodeList_[n] = map[n + offset];
    isSetNodeGlobalMapping_[n] = true;
  }
  offset += nNode_;

  for (int n = 0; n < nEdge_; n++)
  {
    edgeList_[n] = map[n + offset];
    isSetEdgeGlobalMapping_[n] = true;
  }
  offset += nEdge_;

  for (int n = 0; n < nCell_; n++)
  {
    cellList_[n] = map[n + offset];
    isSetCellGlobalMapping_[n] = true;
  }
}


template <class Topology>
void
ElementAssociativityConstructor<TopoD2, Topology>::setGlobalMapping( const std::vector<int>& map )
{
  setGlobalMapping(&map[0], static_cast<int>(map.size()));
}


template <class Topology>
void
ElementAssociativityConstructor<TopoD2, Topology>::getGlobalMapping( int map[], int ndof ) const
{
#if 0
  std::cout << "ElementAssociativityConstructor<TopoD2, Topology>::getGlobalMapping  dumping..." << std::endl;
  this->dump(2);
#endif
  SANS_ASSERT_MSG( ndof == nNode_ + nEdge_ + nCell_, "ndof = %d  nNode_ + nEdge_ + nCell_ = %d", ndof, nNode_ + nEdge_ + nCell_ );

  int offset = 0;
  for (int n = 0; n < nNode_; n++)
  {
    SANS_ASSERT_MSG( isSetNodeGlobalMapping_[n], "n = %d", n );
    map[n + offset] = nodeList_[n];
  }
  offset += nNode_;

  for (int n = 0; n < nEdge_; n++)
  {
    SANS_ASSERT_MSG( isSetEdgeGlobalMapping_[n], "n = %d", n );
    map[n + offset] = edgeList_[n];
  }
  offset += nEdge_;

  for (int n = 0; n < nCell_; n++)
  {
    SANS_ASSERT_MSG( isSetCellGlobalMapping_[n], "n = %d", n );
    map[n + offset] = cellList_[n];
  }
}



template <class Topology>
void
ElementAssociativityConstructor<TopoD2, Topology>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "ElementAssociativityArea<>: order_ = " << order_
      << "  nNode_ = " << nNode_
      << "  nEdge_ = " << nEdge_
      << "  nCell_ = " << nCell_;

  out << "  edgeSign_ =";
  for (int n = 0; n < Topology::NEdge; n++)
    out << " " << edgeSign_[n];
  out << std::endl;

  out << "  rank_  = " << rank_ << std::endl;
  if (nNode_ > 0)
  {
    out << indent << indent << "nodeList =";
    for (int n = 0; n < nNode_; n++)
      out << " " << nodeList_[n];
    out << std::endl;

    out << indent << indent << "isSetNodeGlobalMapping =";
    for (int n = 0; n < nNode_; n++)
      out << " " << isSetNodeGlobalMapping_[n];
    out << std::endl;
  }

  if (nEdge_ > 0)
  {
    out << indent << indent << "edgeList =";
    for (int n = 0; n < nEdge_; n++)
      out << " " << edgeList_[n];
    out << std::endl;

    out << indent << indent << "isSetEdgeGlobalMapping =";
    for (int n = 0; n < nEdge_; n++)
      out << " " << isSetEdgeGlobalMapping_[n];
    out << std::endl;
  }

  if (nCell_ > 0)
  {
    out << indent << indent << "cellList =";
    for (int n = 0; n < nCell_; n++)
      out << " " << cellList_[n];
    out << std::endl;

    out << indent << indent << "isSetCellGlobalMapping =";
    for (int n = 0; n < nCell_; n++)
      out << " " << isSetCellGlobalMapping_[n];
    out << std::endl;
  }
}

// Explicitly instantiate the classes
template class ElementAssociativityConstructor<TopoD2, Triangle>;
template class ElementAssociativityConstructor<TopoD2, Quad>;

}
