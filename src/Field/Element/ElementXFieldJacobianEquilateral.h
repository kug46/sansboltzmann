// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ELEMENTXFIELD_JACOBIANEQUILATERAL_IMPL
#define ELEMENTXFIELD_JACOBIANEQUILATERAL_IMPL

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "Topology/Dimension.h"
#include "Topology/ElementTopology.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"

namespace SANS
{

//----------------------------------------------------------------------------//
/* A struct that contains the inverse Jacobian of the unit equilateral element
 * invJeq is the transformation matrix from the unit equilateral element to the reference element
 * Jeq has dimension (PhysDim x TopoDim), so invJeq has dimension (TopoDim x PhysDim)
 */
//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim, class Topology>
struct JacobianEquilateralTransform
{
  static const DLA::MatrixS<TopoDim::D,PhysDim::D,Real> invJeq;
};

}

#endif  // ELEMENTXFIELD_JACOBIANEQUILATERAL_IMPL
