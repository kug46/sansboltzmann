// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "ElementAssociativitySpacetimeConstructor.h"
#include "Topology/ElementTopology.h"

#include "BasisFunction/TraceToCellRefCoord.h"

namespace SANS
{

// NOTE: default ctor needed for 'new []'
template <class Topology>
ElementAssociativityConstructor<TopoD4, Topology>::ElementAssociativityConstructor()
{
  rank_  = -1;
  order_ = -1;
  nNode_ = 0;
  nEdge_ = 0;
  nArea_ = 0;
  nFace_ = 0;
  nCell_ = 0;

  nodeList_ = NULL;
  edgeList_ = NULL;
  areaList_ = NULL;
  faceList_ = NULL;
  cellList_ = NULL;

  isSetRank_ = false;
  isSetNodeGlobalMapping_ = NULL;
  isSetEdgeGlobalMapping_ = NULL;
  isSetAreaGlobalMapping_ = NULL;
  isSetFaceGlobalMapping_ = NULL;
  isSetCellGlobalMapping_ = NULL;

  for (int n = 0; n < Topology::NFace; n++)
    faceSign_[n] = +1;
}


template <class Topology>
void
ElementAssociativityConstructor<TopoD4, Topology>::resize( const BasisType* basis )
{
  rank_  = -1;
  order_ = basis->order();
  nNode_ = basis->nBasisNode();
  nEdge_ = basis->nBasisEdge();
  nArea_ = basis->nBasisArea();
  nFace_ = basis->nBasisFace();
  nCell_ = basis->nBasisCell();

  isSetRank_ = false;

  delete [] nodeList_; nodeList_ = NULL;
  delete [] isSetNodeGlobalMapping_; isSetNodeGlobalMapping_ = NULL;

  delete [] edgeList_; edgeList_ = NULL;
  delete [] isSetEdgeGlobalMapping_; isSetEdgeGlobalMapping_ = NULL;

  delete [] areaList_; areaList_ = NULL;
  delete [] isSetAreaGlobalMapping_; isSetAreaGlobalMapping_ = NULL;

  delete [] faceList_; faceList_ = NULL;
  delete [] isSetFaceGlobalMapping_; isSetFaceGlobalMapping_ = NULL;

  delete [] cellList_; cellList_ = NULL;
  delete [] isSetCellGlobalMapping_; isSetCellGlobalMapping_ = NULL;

  if (nNode_ != 0)
  {
    nodeList_ = new int[nNode_];
    isSetNodeGlobalMapping_ = new bool[nNode_];
    for (int n = 0; n < nNode_; n++)
    {
      nodeList_[n] = -1;
      isSetNodeGlobalMapping_[n] = false;
    }
  }

  if (nEdge_ != 0)
  {
    edgeList_ = new int[nEdge_];
    isSetEdgeGlobalMapping_ = new bool[nEdge_];
    for (int n = 0; n < nEdge_; n++)
    {
      edgeList_[n] = -1;
      isSetEdgeGlobalMapping_[n] = false;
    }
  }

  if (nArea_ != 0)
  {
    areaList_ = new int[nArea_];
    isSetAreaGlobalMapping_ = new bool[nArea_];
    for (int n = 0; n < nArea_; n++)
    {
      areaList_[n] = -1;
      isSetAreaGlobalMapping_[n] = false;
    }
  }

  if (nFace_ != 0)
  {
    faceList_ = new int[nFace_];
    isSetFaceGlobalMapping_ = new bool[nFace_];
    for (int n = 0; n < nFace_; n++)
    {
      faceList_[n] = -1;
      isSetFaceGlobalMapping_[n] = false;
    }
  }

  if (nCell_ != 0)
  {
    cellList_ = new int[nCell_];
    isSetCellGlobalMapping_ = new bool[nCell_];
    for (int n = 0; n < nCell_; n++)
    {
      cellList_[n] = -1;
      isSetCellGlobalMapping_[n] = false;
    }
  }

  for (int n = 0; n < Topology::NFace; n++)
    faceSign_[n] = +1;
}

template <class Topology>
ElementAssociativityConstructor<TopoD4, Topology>::~ElementAssociativityConstructor()
{
  delete [] nodeList_;
  delete [] edgeList_;
  delete [] areaList_;
  delete [] faceList_;
  delete [] cellList_;

  delete [] isSetNodeGlobalMapping_;
  delete [] isSetEdgeGlobalMapping_;
  delete [] isSetAreaGlobalMapping_;
  delete [] isSetFaceGlobalMapping_;
  delete [] isSetCellGlobalMapping_;
}

template <class Topology>
ElementAssociativityConstructor<TopoD4, Topology>&
ElementAssociativityConstructor<TopoD4, Topology>::operator=( const ElementAssociativityConstructor& a )
{
  if (this != &a)
  {
    rank_  = a.rank_;
    order_ = a.order_;
    nNode_ = a.nNode_;
    nEdge_ = a.nEdge_;
    nArea_ = a.nArea_;
    nFace_ = a.nFace_;
    nCell_ = a.nCell_;

    isSetRank_ = a.isSetRank_;

    delete [] nodeList_; nodeList_ = NULL;
    delete [] isSetNodeGlobalMapping_; isSetNodeGlobalMapping_ = NULL;

    delete [] edgeList_; edgeList_ = NULL;
    delete [] isSetEdgeGlobalMapping_; isSetEdgeGlobalMapping_ = NULL;

    delete [] areaList_; areaList_ = NULL;
    delete [] isSetAreaGlobalMapping_; isSetAreaGlobalMapping_ = NULL;

    delete [] faceList_; faceList_ = NULL;
    delete [] isSetFaceGlobalMapping_; isSetFaceGlobalMapping_ = NULL;

    delete [] cellList_; cellList_ = NULL;
    delete [] isSetCellGlobalMapping_; isSetCellGlobalMapping_ = NULL;

    if (nNode_ != 0)
    {
      nodeList_ = new int[nNode_];
      isSetNodeGlobalMapping_ = new bool[nNode_];
      const int nNode = nNode_;
      for (int n = 0; n < nNode; n++)
      {
        nodeList_[n] = a.nodeList_[n];
        isSetNodeGlobalMapping_[n] = a.isSetNodeGlobalMapping_[n];
      }
    }

    if (nEdge_ != 0)
    {
      edgeList_ = new int[nEdge_];
      isSetEdgeGlobalMapping_ = new bool[nEdge_];
      const int nEdge = nEdge_;
      for (int n = 0; n < nEdge; n++)
      {
        edgeList_[n] = a.edgeList_[n];
        isSetEdgeGlobalMapping_[n] = a.isSetEdgeGlobalMapping_[n];
      }
    }

    if (nArea_ != 0)
    {
      areaList_ = new int[nArea_];
      isSetAreaGlobalMapping_ = new bool[nArea_];
      const int nArea = nArea_;
      for (int n = 0; n < nArea; n++)
      {
        areaList_[n] = a.areaList_[n];
        isSetAreaGlobalMapping_[n] = a.isSetAreaGlobalMapping_[n];
      }
    }

    if (nFace_ != 0)
    {
      faceList_ = new int[nFace_];
      isSetFaceGlobalMapping_ = new bool[nFace_];
      const int nFace = nFace_;
      for (int n = 0; n < nFace; n++)
      {
        faceList_[n] = a.faceList_[n];
        isSetFaceGlobalMapping_[n] = a.isSetFaceGlobalMapping_[n];
      }
    }

    if (nCell_ != 0)
    {
      cellList_ = new int[nCell_];
      isSetCellGlobalMapping_ = new bool[nCell_];
      const int nCell = nCell_;
      for (int n = 0; n < nCell; n++)
      {
        cellList_[n] = a.cellList_[n];
        isSetCellGlobalMapping_[n] = a.isSetCellGlobalMapping_[n];
      }
    }

    faceSign_ = a.faceSign_;
  }

  return *this;
}


template <class Topology>
void
ElementAssociativityConstructor<TopoD4, Topology>::setNodeGlobalMapping( const int node[], int nnode )
{
  SANS_ASSERT( nnode == nNode_ );

  for (int n = 0; n < nNode_; n++)
  {
    nodeList_[n] = node[n];
    isSetNodeGlobalMapping_[n] = true;
  }
}


template <class Topology>
void
ElementAssociativityConstructor<TopoD4, Topology>::getNodeGlobalMapping( int node[], int nnode ) const
{
#if 0
  if ( isSetNodeGlobalMapping_ && (nnode == nNode_) )
  {}
  else
  { std::cout << "getNodeGlobalMapping: nnode = " << nnode << "  dumping..." << std::endl; dump(2); }
#endif
  SANS_ASSERT( nnode == nNode_ );

  for (int n = 0; n < nNode_; n++)
  {
    SANS_ASSERT( isSetNodeGlobalMapping_[n] );
    node[n] = nodeList_[n];
  }
}

template <class Topology>
void
ElementAssociativityConstructor<TopoD4, Topology>::setNodeGlobalMapping( const std::vector<int>& node )
{
  SANS_ASSERT( (int)node.size() == nNode_ );

  for (std::size_t n = 0; n < node.size(); n++)
  {
    nodeList_[n] = node[n];
    isSetNodeGlobalMapping_[n] = true;
  }
}


template <class Topology>
void
ElementAssociativityConstructor<TopoD4, Topology>::setEdgeGlobalMapping( const int edge[], int nedge )
{
  SANS_ASSERT_MSG( nedge == nEdge_, "nedge = %d  nEdge_ = %d", nedge, nEdge_ );

  for (int n = 0; n < nEdge_; n++)
  {
    edgeList_[n] = edge[n];
    isSetEdgeGlobalMapping_[n] = true;
  }
}


template <class Topology>
void
ElementAssociativityConstructor<TopoD4, Topology>::getEdgeGlobalMapping( int edge[], int nedge ) const
{
  SANS_ASSERT_MSG( nedge == nEdge_, " nedge = %d  nEdge_ = %d", nedge, nEdge_ );

  for (int n = 0; n < nEdge_; n++)
  {
    SANS_ASSERT_MSG( isSetEdgeGlobalMapping_[n], "n = %", n );
    edge[n] = edgeList_[n];
  }
}

template <class Topology>
void
ElementAssociativityConstructor<TopoD4, Topology>::setEdgeGlobalMapping( const std::vector<int>& edge )
{
  SANS_ASSERT( (int)edge.size() == nEdge_ );

  for (int n = 0; n < nEdge_; n++)
  {
    edgeList_[n] = edge[n];
    isSetEdgeGlobalMapping_[n] = true;
  }
}

template <class Topology>
void
ElementAssociativityConstructor<TopoD4, Topology>::setEdgeGlobalMapping( const std::vector<int>& edgeMap,
                                                                       const CanonicalTraceToCell& canonicalface,
                                                                       const int face_edge)
{
  SANS_ASSERT_MSG( (canonicalface.trace >= 0) && (canonicalface.trace < Topology::NFace), "[canonicalface.trace = %d]", canonicalface.trace );

  // this function is untested
  SANS_DEVELOPER_EXCEPTION("implement");

  int nsize = (int)edgeMap.size();
  SANS_ASSERT_MSG( nsize*(Topology::NEdge) == nEdge_, "[nsize = %d  nEdge_ = %d]", nsize, nEdge_ );

  int cell_edge = TraceToCellRefCoord<Tet, TopoD4, Pentatope>::getCellEdge(canonicalface, face_edge);

  for (int n = 0; n < nsize; n++)
  {
    edgeList_[nsize*cell_edge + n] = edgeMap[n];
    isSetEdgeGlobalMapping_[nsize*cell_edge + n] = true;
  }

}

//// begin
template <class Topology>
void
ElementAssociativityConstructor<TopoD4, Topology>::setAreaGlobalMapping( const int area[], int narea )
{
  SANS_ASSERT_MSG( narea == nArea_, "narea = %d  nArea_ = %d", narea, nArea_ );

  for (int n = 0; n < nArea_; n++)
  {
    areaList_[n] = area[n];
    isSetAreaGlobalMapping_[n] = true;
  }
}


template <class Topology>
void
ElementAssociativityConstructor<TopoD4, Topology>::getAreaGlobalMapping( int area[], int narea ) const
{
  SANS_ASSERT_MSG( narea == nArea_, " narea = %d  nArea_ = %d", narea, nArea_ );

  for (int n = 0; n < nArea_; n++)
  {
    SANS_ASSERT_MSG( isSetAreaGlobalMapping_[n], "n = %", n );
    area[n] = areaList_[n];
  }
}

template <class Topology>
void
ElementAssociativityConstructor<TopoD4, Topology>::setAreaGlobalMapping( const std::vector<int>& area )
{
  SANS_ASSERT( (int)area.size() == nArea_ );

  for (int n = 0; n < nArea_; n++)
  {
    areaList_[n] = area[n];
    isSetAreaGlobalMapping_[n] = true;
  }
}

template <class Topology>
void
ElementAssociativityConstructor<TopoD4, Topology>::setAreaGlobalMapping( const std::vector<int>& areaMap,
                                                                       const CanonicalTraceToCell& canonicalface,
                                                                       const int face_area)
{
  SANS_DEVELOPER_EXCEPTION("implement");
  #if 0
  SANS_ASSERT_MSG( (canonicalface.trace >= 0) && (canonicalface.trace < Topology::NFace), "[canonicalface.trace = %d]", canonicalface.trace );

  int nsize = (int)areaMap.size();
  SANS_ASSERT_MSG( nsize*(Topology::NEdge) == nEdge_, "[nsize = %d  nEdge_ = %d]", nsize, nEdge_ );

  int cell_edge = TraceToCellRefCoord<Triangle, TopoD4, Tet>::getCellEdge(canonicalface, face_area);

  for (int n = 0; n < nsize; n++)
  {
    edgeList_[nsize*cell_edge + n] = edgeMap[n];
    isSetEdgeGlobalMapping_[nsize*cell_edge + n] = true;
  }
  #endif
}

//// end


template <class Topology>
void
ElementAssociativityConstructor<TopoD4, Topology>::setFaceGlobalMapping( const int face[], int nface )
{
  SANS_ASSERT_MSG( nface == nFace_, "nface = %d  nFace_ = %d", nface, nFace_ );

  for (int n = 0; n < nFace_; n++)
  {
    faceList_[n] = face[n];
    isSetFaceGlobalMapping_[n] = true;
  }
}


template <class Topology>
void
ElementAssociativityConstructor<TopoD4, Topology>::getFaceGlobalMapping( int face[], int nface ) const
{
  SANS_ASSERT_MSG( nface == nFace_, " nface = %d  nFace_ = %d", nface, nFace_ );

  for (int n = 0; n < nFace_; n++)
  {
    SANS_ASSERT_MSG( isSetFaceGlobalMapping_[n], "n = %", n );
    face[n] = faceList_[n];
  }
}


template <class Topology>
void
ElementAssociativityConstructor<TopoD4, Topology>::setFaceGlobalMapping( const int faceMap[], int nsize,
                                                                       const CanonicalTraceToCell& canonicalface )
{
  SANS_DEVELOPER_EXCEPTION("implement");

  SANS_ASSERT_MSG( (canonicalface.trace >= 0) && (canonicalface.trace < Topology::NFace), "[canonicalface.trace = %d]", canonicalface.trace );
  SANS_ASSERT_MSG( nsize*(Topology::NFace) == nFace_, "[nsize = %d  nFace_ = %d]", nsize, nFace_ );

  for (int n = 0; n < nsize; n++)
  {
    faceList_[nsize*canonicalface.trace + n] = faceMap[n];
    isSetFaceGlobalMapping_[nsize*canonicalface.trace + n] = true;
  }

}


template <class Topology>
void
ElementAssociativityConstructor<TopoD4, Topology>::setFaceGlobalMapping( const std::vector<int>& face )
{
  SANS_ASSERT( (int)face.size() == nFace_ );

  for (int n = 0; n < nFace_; n++)
  {
    faceList_[n] = face[n];
    isSetFaceGlobalMapping_[n] = true;
  }
}


template <class Topology>
void
ElementAssociativityConstructor<TopoD4, Topology>::setCellGlobalMapping( const std::vector<int>& cell )
{
  SANS_ASSERT( (int)cell.size() == nCell_ );

  for (int n = 0; n < nCell_; n++)
  {
    cellList_[n] = cell[n];
    isSetCellGlobalMapping_[n] = true;
  }
}

template <class Topology>
void
ElementAssociativityConstructor<TopoD4, Topology>::setCellGlobalMapping( const int cell[], int ncell )
{
  SANS_ASSERT( ncell == nCell_ );

  for (int n = 0; n < nCell_; n++)
  {
    cellList_[n] = cell[n];
    isSetCellGlobalMapping_[n] = true;
  }
}


template <class Topology>
void
ElementAssociativityConstructor<TopoD4, Topology>::getCellGlobalMapping( int cell[], int ncell ) const
{
  SANS_ASSERT( ncell == nCell_ );

  for (int n = 0; n < nCell_; n++)
  {
    SANS_ASSERT( isSetCellGlobalMapping_[n] );
    cell[n] = cellList_[n];
  }
}


template <class Topology>
void
ElementAssociativityConstructor<TopoD4, Topology>::setGlobalMapping( const int map[], int ndof )
{
#if 0
  std::cout << "ElementAssociativityConstructor<TopoD4, Topology>::setGlobalMapping  dumping..." << std::endl;
  this->dump(2);
#endif

  SANS_ASSERT( ndof == nNode_ + nEdge_ + nArea_ + nFace_ + nCell_ );

  int offset = 0;
  for (int n = 0; n < nNode_; n++)
  {
    nodeList_[n] = map[n + offset];
    isSetNodeGlobalMapping_[n] = true;
  }
  offset += nNode_;

  for (int n = 0; n < nEdge_; n++)
  {
    edgeList_[n] = map[n + offset];
    isSetEdgeGlobalMapping_[n] = true;
  }
  offset += nEdge_;

  for (int n = 0; n < nArea_; n++)
  {
    areaList_[n] = map[n + offset];
    isSetAreaGlobalMapping_[n] = true;
  }
  offset += nArea_;

  for (int n = 0; n < nFace_; n++)
  {
    faceList_[n] = map[n + offset];
    isSetFaceGlobalMapping_[n] = true;
  }
  offset += nFace_;

  for (int n = 0; n < nCell_; n++)
  {
    cellList_[n] = map[n + offset];
    isSetCellGlobalMapping_[n] = true;
  }
}


template <class Topology>
void
ElementAssociativityConstructor<TopoD4, Topology>::setGlobalMapping( const std::vector<int>& map )
{
  setGlobalMapping(&map[0], static_cast<int>(map.size()));
}


template <class Topology>
void
ElementAssociativityConstructor<TopoD4, Topology>::getGlobalMapping( int map[], int ndof ) const
{
#if 0
  std::cout << "ElementAssociativityConstructor<TopoD4, Topology>::getGlobalMapping  dumping..." << std::endl;
  this->dump(2);
#endif
  SANS_ASSERT( ndof == nNode_ + nEdge_ + nArea_ + nFace_ + nCell_ );

  int offset = 0;
  for (int n = 0; n < nNode_; n++)
  {
    SANS_ASSERT( isSetNodeGlobalMapping_[n] );
    map[n + offset] = nodeList_[n];
  }
  offset += nNode_;

  for (int n = 0; n < nEdge_; n++)
  {
    SANS_ASSERT( isSetEdgeGlobalMapping_[n] );
    map[n + offset] = edgeList_[n];
  }
  offset += nEdge_;

  for (int n = 0; n < nArea_; n++)
  {
    SANS_ASSERT( isSetAreaGlobalMapping_[n] );
    map[n + offset] = areaList_[n];
  }
  offset += nArea_;

  for (int n = 0; n < nFace_; n++)
  {
    SANS_ASSERT( isSetFaceGlobalMapping_[n] );
    map[n + offset] = faceList_[n];
  }
  offset += nFace_;

  for (int n = 0; n < nCell_; n++)
  {
    SANS_ASSERT( isSetCellGlobalMapping_[n] );
    map[n + offset] = cellList_[n];
  }
}



template <class Topology>
void
ElementAssociativityConstructor<TopoD4, Topology>::dump( int indentSize, std::ostream& out ) const
{

  std::string indent(indentSize, ' ');
  out << indent << "ElementAssociativitySpacetime<>: order_ = " << order_
      << "  nNode_ = " << nNode_
      << "  nEdge_ = " << nEdge_
      << "  nArea_ = " << nArea_
      << "  nFace_ = " << nFace_
      << "  nCell_ = " << nCell_;

  out << "  faceSign_ =";
  for (int n = 0; n < Topology::NFace; n++)
    out << " " << faceSign_[n];
  out << std::endl;

  out << " rank_ = " << rank_ << std::endl;
  if (nNode_ > 0)
  {
    out << indent << indent << "nodeList =";
    for (int n = 0; n < nNode_; n++)
      out << " " << nodeList_[n];
    out << std::endl;

    out << indent << indent << "isSetNodeGlobalMapping =";
    for (int n = 0; n < nNode_; n++)
      out << " " << isSetNodeGlobalMapping_[n];
    out << std::endl;
  }

  if (nEdge_ > 0)
  {
    out << indent << indent << "edgeList =";
    for (int n = 0; n < nEdge_; n++)
      out << " " << edgeList_[n];
    out << std::endl;

    out << indent << indent << "isSetEdgeGlobalMapping =";
    for (int n = 0; n < nEdge_; n++)
      out << " " << isSetEdgeGlobalMapping_[n];
    out << std::endl;
  }

  if (nArea_ > 0)
  {
    out << indent << indent << "areaList =";
    for (int n = 0; n < nArea_; n++)
      out << " " << areaList_[n];
    out << std::endl;

    out << indent << indent << "isSetAreaGlobalMapping =";
    for (int n = 0; n < nArea_; n++)
      out << " " << isSetAreaGlobalMapping_[n];
    out << std::endl;
  }

  if (nFace_ > 0)
  {
    out << indent << indent << "faceList =";
    for (int n = 0; n < nFace_; n++)
      out << " " << faceList_[n];
    out << std::endl;

    out << indent << indent << "isSetFaceGlobalMapping =";
    for (int n = 0; n < nFace_; n++)
      out << " " << isSetFaceGlobalMapping_[n];
    out << std::endl;
  }

  if (nCell_ > 0)
  {
    out << indent << indent << "cellList =";
    for (int n = 0; n < nCell_; n++)
      out << " " << cellList_[n];
    out << std::endl;

    out << indent << indent << "isSetCellGlobalMapping =";
    for (int n = 0; n < nCell_; n++)
      out << " " << isSetCellGlobalMapping_[n];
    out << std::endl;
  }
}

// Explicitly instantiate the classes
template class ElementAssociativityConstructor<TopoD4, Pentatope>;

}
