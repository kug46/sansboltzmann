// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "ElementAssociativityArea.h"
#include "tools/SANSException.h"

namespace SANS
{

//---------------------------------------------------------------------------//
// NOTE: default ctor needed for 'new []'
template <class Topology>
ElementAssociativity<TopoD2,Topology>::ElementAssociativity()
{
  rank_  = -1;
  order_ = -1;
  nNode_ = 0;
  nEdge_ = 0;
  nCell_ = 0;

  nodeList_ = NULL;
  edgeList_ = NULL;
  cellList_ = NULL;

  for (int n = 0; n < Topology::NEdge; n++)
    edgeSign_[n] = 0;
}

//---------------------------------------------------------------------------//
template <class Topology>
ElementAssociativity<TopoD2,Topology>::ElementAssociativity( const BasisType* basis )
{
  rank_  = -1;
  order_ = basis->order();
  nNode_ = basis->nBasisNode();
  nEdge_ = basis->nBasisEdge();
  nCell_ = basis->nBasisCell();

  nodeList_ = NULL;
  edgeList_ = NULL;
  cellList_ = NULL;

  if (nNode_ != 0)
  {
    nodeList_ = new int[nNode_];
    for (int n = 0; n < nNode_; n++)
      nodeList_[n] = -1;
  }

  if (nEdge_ != 0)
  {
    edgeList_ = new int[nEdge_];
    for (int n = 0; n < nEdge_; n++)
      edgeList_[n] = -1;
  }

  if (nCell_ != 0)
  {
    cellList_ = new int[nCell_];
    for (int n = 0; n < nCell_; n++)
      cellList_[n] = -1;
  }

  for (int n = 0; n < Topology::NEdge; n++)
    edgeSign_[n] = +1;
}

//---------------------------------------------------------------------------//
template <class Topology>
ElementAssociativity<TopoD2,Topology>::ElementAssociativity( const ElementAssociativity<TopoD2,Topology>& a )
  : ElementAssociativity<TopoD2,Topology>()
{
  operator=(a);
}

//---------------------------------------------------------------------------//
template <class Topology>
ElementAssociativity<TopoD2,Topology>::~ElementAssociativity()
{
  delete [] nodeList_;
  delete [] edgeList_;
  delete [] cellList_;
}

//---------------------------------------------------------------------------//
template <class Topology>
ElementAssociativity<TopoD2,Topology>&
ElementAssociativity<TopoD2,Topology>::operator=( const ElementAssociativity<TopoD2,Topology>& a )
{
  if (this != &a)
  {
    rank_  = a.rank_;
    order_ = a.order_;
    nNode_ = a.nNode_;
    nEdge_ = a.nEdge_;
    nCell_ = a.nCell_;

    delete [] nodeList_; nodeList_ = NULL;
    delete [] edgeList_; edgeList_ = NULL;
    delete [] cellList_; cellList_ = NULL;

    if (nNode_ != 0)
    {
      nodeList_ = new int[nNode_];
      for (int n = 0; n < nNode_; n++)
        nodeList_[n] = a.nodeList_[n];
    }

    if (nEdge_ != 0)
    {
      edgeList_ = new int[nEdge_];
      for (int n = 0; n < nEdge_; n++)
        edgeList_[n] = a.edgeList_[n];
    }

    if (nCell_ != 0)
    {
      cellList_ = new int[nCell_];
      for (int n = 0; n < nCell_; n++)
        cellList_[n] = a.cellList_[n];
    }

    for (int n = 0; n < Topology::NEdge; n++)
      edgeSign_[n] = a.edgeSign_[n];
  }

  return *this;
}

//---------------------------------------------------------------------------//
template <class Topology>
ElementAssociativity<TopoD2,Topology>&
ElementAssociativity<TopoD2,Topology>::operator=( const Constructor& a )
{
  rank_  = a.rank();
  order_ = a.order();
  nNode_ = a.nNode();
  nEdge_ = a.nEdge();
  nCell_ = a.nCell();

  delete [] nodeList_; nodeList_ = NULL;
  delete [] edgeList_; edgeList_ = NULL;
  delete [] cellList_; cellList_ = NULL;

  if (nNode_ != 0)
  {
    nodeList_ = new int[nNode_];
    for (int n = 0; n < nNode_; n++)
      nodeList_[n] = a.nodeGlobal(n);
  }

  if (nEdge_ != 0)
  {
    edgeList_ = new int[nEdge_];
    for (int n = 0; n < nEdge_; n++)
      edgeList_[n] = a.edgeGlobal(n);
  }

  if (nCell_ != 0)
  {
    cellList_ = new int[nCell_];
    for (int n = 0; n < nCell_; n++)
      cellList_[n] = a.cellGlobal(n);
  }

  edgeSign_ = a.edgeSign();


  return *this;
}

//---------------------------------------------------------------------------//
template <class Topology>
void
ElementAssociativity<TopoD2,Topology>::getNodeGlobalMapping( int node[], int nnode ) const
{
  SANS_ASSERT( nnode == nNode_ );

  for (int n = 0; n < nNode_; n++)
    node[n] = nodeList_[n];
}

//---------------------------------------------------------------------------//
template <class Topology>
void
ElementAssociativity<TopoD2,Topology>::getEdgeGlobalMapping( int edge[], int nedge ) const
{
  SANS_ASSERT_MSG( nedge == nEdge_, " nedge = %d  nEdge_ = %d", nedge, nEdge_ );

  for (int n = 0; n < nEdge_; n++)
    edge[n] = edgeList_[n];
}

//---------------------------------------------------------------------------//
template <class Topology>
void
ElementAssociativity<TopoD2,Topology>::getCellGlobalMapping( int cell[], int ncell ) const
{
  SANS_ASSERT_MSG( ncell == nCell_, " ncell = %d  nCell_ = %d", ncell, nCell_ );

  for (int n = 0; n < nCell_; n++)
    cell[n] = cellList_[n];
}

//---------------------------------------------------------------------------//
template <class Topology>
void
ElementAssociativity<TopoD2,Topology>::getGlobalMapping( int map[], int ndof ) const
{
  SANS_ASSERT( ndof == nNode_ + nEdge_ + nCell_ );

  int offset = 0;
  for (int n = 0; n < nNode_; n++)
    map[n + offset] = nodeList_[n];
  offset += nNode_;

  for (int n = 0; n < nEdge_; n++)
    map[n + offset] = edgeList_[n];
  offset += nEdge_;

  for (int n = 0; n < nCell_; n++)
    map[n + offset] = cellList_[n];
}

//---------------------------------------------------------------------------//
template <class Topology>
void
ElementAssociativity<TopoD2,Topology>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "ElementAssociativity<TopoD2>: order_ = " << order_
      << "  nNode_ = " << nNode_
      << "  nEdge_ = " << nEdge_
      << "  nCell_ = " << nCell_;
  out << "  edgeSign_ =";

  for (int n = 0; n < Topology::NEdge; n++)
    out << " " << edgeSign_[n];
  out << std::endl;

  if (nNode_ > 0)
  {
    out << indent << indent << "nodeList =";
    for (int n = 0; n < nNode_; n++)
      out << " " << nodeList_[n];
    out << std::endl;
  }

  if (nEdge_ > 0)
  {
    out << indent << indent << "edgeList =";
    for (int n = 0; n < nEdge_; n++)
      out << " " << edgeList_[n];
    out << std::endl;
  }

  if (nCell_ > 0)
  {
    out << indent << indent << "cellList =";
    for (int n = 0; n < nCell_; n++)
      out << " " << cellList_[n];
    out << std::endl;
  }
}

// Explicitly instantiate the classes
template class ElementAssociativity<TopoD2,Triangle>;
template class ElementAssociativity<TopoD2,Quad>;

}
