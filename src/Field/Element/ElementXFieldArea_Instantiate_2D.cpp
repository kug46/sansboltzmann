// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ELEMENTXFIELDAREA_INSTANTIATE

#include "ElementXFieldArea_impl.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/Eigen.h"
#include "ElementXFieldJacobianEquilateral.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// 2-D Cartesian
//----------------------------------------------------------------------------//

//---------------------------------------------------------------------------//
template <class PhysDim, class Topology>
Real
ElementXField<PhysDim, TopoD2, Topology>::area( const Matrix& J ) const
{
  Real det = J(0,0)*J(1,1) - J(0,1)*J(1,0);

  return Topology::areaRef*det;
}

//---------------------------------------------------------------------------//
template <class PhysDim, class Topology>
BoundingBox<PhysDim>
ElementXField<PhysDim, TopoD2, Topology>::boundingBox() const
{
  BoundingBox<PhysDim> bbox;

  // get the corners of the element
  for (int n = 0; n < Topology::NNode; n++)
    bbox.setBound(DOF_[n]);

  typedef QuadraturePoint<TopoD1> QuadPointTraceType;
  typedef QuadratureCellTracePoint<TopoD2> QuadPointCellType;

  // add points on the traces of the triangle
  if ( basis_->order()  > 1 )
  {
    QuadPointCellType sRefCell;
    CanonicalTraceToCell canonicalEdge(0,1);
    VectorX X;

    // use a 2P+1 quadrature rule
    Quadrature<TopoD1, Line> quadrature( 2*basis_->order() + 1 );
    const int nquad = quadrature.nQuadrature();

    for (int itrace = 0; itrace < Topology::NTrace; itrace++)
    {
      canonicalEdge.trace = itrace;
      for (int iquad = 0; iquad < nquad; iquad++)
      {
        QuadPointTraceType sRefTrace = quadrature.coordinates_cache(iquad);

        TraceToCellRefCoord<Line, TopoD2, Topology>::eval( canonicalEdge, sRefTrace, sRefCell );

        coordinates(sRefCell, X);

        bbox.setBound(X);
      }
    }
  }

  return bbox;
}

//---------------------------------------------------------------------------//
template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD2, Topology>::impliedMetric( const Real& s, const Real& t, TensorSymX& M ) const
{
  Matrix Jref; //jacobian from reference element to physical element
  jacobian( s, t, Jref );

  // transformation matrix from the unit equilateral element to the reference element (in 2D)
  const DLA::MatrixS<PhysD2::D,TopoD,Real>& invJeq = JacobianEquilateralTransform<PhysD2,TopoD2,Topology>::invJeq;

  Matrix J = Jref*invJeq; //jacobian from equilateral element to physical element

  TensorSymX JJt = J*Transpose(J);

  // The formula for inverting a 2-by-2 matrix is used:
  // For A = [ [a, b]
  //           [c, d] ],
  // its inverse A^(-1) is
  //   A^(-1) = 1/det(A) * [ [ d, -b]
  //                         [-c,  a] ]

  // Compute implied metric M = inv(JJt)
  Real det = JJt(0,0)*JJt(1,1) - JJt(0,1)*JJt(1,0);
  M(0,0) =  JJt(1,1)/det;
  M(1,0) = -JJt(1,0)/det;
  M(1,1) =  JJt(0,0)/det;
}


//---------------------------------------------------------------------------//
// For J = J^(x,y)_(s,t), i.e. Jacobian of (x,y) w.r.t. (s,t)
//     J = [ [dx/ds, dx/dt]
//           [dy/ds, dy/dt] ],
// we have that
//     J^(-1) = J^(s,t)_(x,y), i.e. Jacobian of (s,t) w.r.t. (x,y)
//     J^(-1) = [ [ds/dx, ds/dy]
//                [dt/dx, dt/dy] ]
//            = [ [grad(s)]^T
//                [grad(t)]^T ]

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD2, Topology>::evalReferenceCoordinateGradient(
    const Real phis[], const Real phit[], VectorX& sgrad, VectorX& tgrad ) const
{
  Real xs, xt, ys, yt;
  Real det;

  xs = 0;  xt = 0;
  ys = 0;  yt = 0;
  for (int n = 0; n < nDOF_; n++)
  {
    xs += phis[n]*DOF_[n](0);  xt += phit[n]*DOF_[n](0);
    ys += phis[n]*DOF_[n](1);  yt += phit[n]*DOF_[n](1);
  }

  det = xs*yt - xt*ys;
  sgrad[0]  =  yt/det;
  sgrad[1]  = -xt/det;
  tgrad[0]  = -ys/det;
  tgrad[1]  =  xs/det;

// The formula for inverting a 2-by-2 matrix is used:
// For A = [ [a, b]
//           [c, d] ],
// its inverse A^(-1) is
//   A^(-1) = 1/det(A) * [ [ d, -b]
//                         [-c,  a] ]

#if 0
  std::cout << "In evalReferenceCoordinateGradient:" << std::endl;
  std::cout << "det: "<< det << std::endl;
  std::cout << "xt,xs,yt,ys: " << xt << xs << yt << ys <<std::endl;
#endif
}


//---------------------------------------------------------------------------//
template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD2, Topology>::evalReferenceCoordinateHessian(
    const Real& s, const Real& t, TensorSymX& shess, TensorSymX& thess ) const
{
  SANS_ASSERT(nDOF_ > 0); //To please the clang compiler
  const int nDOF = nDOF_;

  Real xs, xt, ys, yt;
  Real xss, xst, xtt, yss, yst, ytt;
  Real det;

  basis_->evalBasisDerivative( s, t, edgeSign_, phis_, phit_, nDOF_ );
  basis_->evalBasisHessianDerivative( s, t, edgeSign_, phiss_, phist_, phitt_, nDOF_ );

  xs = 0; xt = 0; ys = 0; yt = 0;
  xss = 0;  xst = 0; xtt = 0;
  yss = 0;  yst = 0; ytt = 0;
  for (int n = 0; n < nDOF; n++)
  {
    // first derivatives
    xs +=phis_[n]*DOF_[n](0); xt +=phit_[n]*DOF_[n](0);
    ys +=phis_[n]*DOF_[n](1); yt +=phit_[n]*DOF_[n](1);

    // second derivatives
    xss += phiss_[n]*DOF_[n](0); xst += phist_[n]*DOF_[n](0); xtt += phitt_[n]*DOF_[n](0);
    yss += phiss_[n]*DOF_[n](1); yst += phist_[n]*DOF_[n](1); ytt += phitt_[n]*DOF_[n](1);
  }
  det = xs*yt - xt*ys;

  // Definition of Matrix in ElementXFieldArea.h
  Matrix invJ; // inverse Jacobian
  TensorSymX xHess, yHess, tempHess1, tempHess2;

  // Construct the inverse Jacobian using 2-by-2 matrix inversion formula
  invJ(0,0) =  yt/det; invJ(0,1) = -xt/det;
  invJ(1,0) = -ys/det; invJ(1,1) =  xs/det;

  // Populate the two Hessians of the Cartesian coordinates
  xHess(0,0) = xss;
  xHess(1,0) = xst; xHess(1,1) = xtt;
  yHess(0,0) = yss;
  yHess(1,0) = yst; yHess(1,1) = ytt;

  tempHess1 = (-yt*xHess + xt*yHess);
  tempHess2 = ( ys*xHess - xs*yHess);

#if 0
  std::cout << "invJ = " << std::endl;
  std::cout << "( " << invJ(0,0) << ",  " << invJ(0,1) << " )" << std::endl;
  std::cout << "( " << invJ(1,0) << ",  " << invJ(1,1) << " )" << std::endl;

  std::cout << "tempHess1 = " << std::endl;
  std::cout << "( " << tempHess1(0,0) << ",  " << tempHess1(0,1) << " )" << std::endl;
  std::cout << "( " << tempHess1(1,0) << ",  " << tempHess1(1,1) << " )" << std::endl;

  std::cout << "tempHess2 = " << std::endl;
  std::cout << "( " << tempHess2(0,0) << ",  " << tempHess2(0,1) << " )" << std::endl;
  std::cout << "( " << tempHess2(1,0) << ",  " << tempHess2(1,1) << " )" << std::endl;
#endif

  // Calculate the transformed hessian
  shess = Transpose(invJ)*tempHess1*invJ;
  thess = Transpose(invJ)*tempHess2*invJ;

  shess = shess / det;
  thess = thess / det;

#if 0
  std::cout << "In evalReferenceCoordinateHessian:" << std::endl;
  std::cout << "det: "<< det << std::endl;
  std::cout << "xt,xs,yt,ys: " << xt << xs << yt << ys <<std::endl;
  std::cout << "shess: " << shess(0,0) << shess(1,0) << shess(1,1) << std::endl;
  std::cout << "thess: " << thess(0,0) << thess(1,0) << thess(1,1) << std::endl;
  std::cout << "tempHess1" << tempHess1(0,0) << tempHess1(1,0) << tempHess1(1,1) <<std::endl;
  std::cout << "tempHess2" << tempHess2(0,0) << tempHess2(1,0) << tempHess2(1,1) <<std::endl;
#endif

}

//----------------------------------------------------------------------------//
// basis hessian
// Returns the hessian of basis functions in cartesian coords, transforms from the master element
template <class PhysDim, class Topology>
template <class RefCoord>
void
ElementXField<PhysDim, TopoD2, Topology>::evalBasisHessian( const RefCoord& sRef,
                                                            const ElementBasis<TopoD2, Topology>& fldElem,
                                                            TensorSymX hessphi[], int nphi ) const
{
  const int nBasis = fldElem.nDOF();   // total solution DOFs in element
  SANS_ASSERT(nphi == nBasis);

  VectorX sgrad, tgrad;               // gradients of reference coordinates (s,t)
  TensorSymX shess, thess;            // hessians of reference coordinates (s,t)
  const Real *phis, *phit;            // derivatives of solution basis wrt (s,t)
  const Real *phiss, *phist, *phitt;  // second derivatives of solution basis wrt (s,t)

  evalReferenceCoordinateGradient( sRef, sgrad, tgrad );
  evalReferenceCoordinateHessian( sRef, shess, thess );

  fldElem.evalBasisDerivative( sRef, &phis, &phit );
  fldElem.evalBasisHessianDerivative( sRef, &phiss, &phist, &phitt );

  Matrix invJ; //Inverse Jacobian
  TensorSymX refHess;
  invJ(0,0) = sgrad[0]; invJ(0,1) = sgrad[1];
  invJ(1,0) = tgrad[0]; invJ(1,1) = tgrad[1];

  for (int n = 0; n < nBasis; n++)
  {
    refHess(0,0) = phiss[n];
    refHess(1,0) = phist[n]; refHess(1,1) = phitt[n]; //copy values into hessian

    hessphi[n]  = Transpose(invJ)*refHess*invJ; // see Documentation/Hessians.pdf for formula
    hessphi[n] += phis[n]*shess + phit[n]*thess;
  }

#if 0 // for debug
  std::cout << "invJ = " << std::endl;
  std::cout << "( " << invJ(0,0) << ",  " << invJ(0,1) << " )" << std::endl;
  std::cout << "( " << invJ(1,0) << ",  " << invJ(1,1) << " )" << std::endl;

  std::cout << "refHess = " << std::endl;
  std::cout << "( " << refHess(0,0) << ",  " << refHess(1,0) << " )" << std::endl;
  std::cout << "( " << refHess(1,0) << ",  " << refHess(1,1) << " )" << std::endl;

  std::cout << "shess = " << std::endl;
  std::cout << "( " << shess(0,0) << ",  " << shess(1,0) << " )" << std::endl;
  std::cout << "( " << shess(1,0) << ",  " << shess(1,1) << " )" << std::endl;

  std::cout << "thess = " << std::endl;
  std::cout << "( " << thess(0,0) << ",  " << thess(1,0) << " )" << std::endl;
  std::cout << "( " << thess(1,0) << ",  " << thess(1,1) << " )" << std::endl;
#endif
}


template class ElementXField<PhysD2, TopoD2, Triangle>;
template class ElementXField<PhysD2, TopoD2, Quad>;

}
