// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ELEMENTASSOCIATIVITYVOLUME_H
#define ELEMENTASSOCIATIVITYVOLUME_H

// Volume associativity (local to global mappings)

#include <ostream>
#include <vector>
#include <array>

#include "BasisFunction/BasisFunctionVolume.h"
#include "ElementVolume.h"
#include "ElementAssociativityVolumeConstructor.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// volume associativity: global numberings for element nodes/edges/cell
//
// template parameters:
//   Topology                   element topology (triangle/quad)
//                              Note: needed for basis function ctor
//
// member functions:
//   .order                     polynomial order
//   .nNode                     # node DOFs
//   .nEdge                     # edge DOFs
//   .nFace                     # face DOFs
//   .nCell                     # cell interior DOFs
//   .node/edge/cellGlobal      global node/edge/cell DOF accessors
//   .getNodeGlobalMapping      local-to-global node DOF mapping
//   .getEdgeGlobalMapping      local-to-global edge DOF mapping
//   .getFaceGlobalMapping      local-to-global face DOF mapping
//   .getCellGlobalMapping      local-to-global cell DOF mapping
//
//   .dump            debug dump of private data
//----------------------------------------------------------------------------//

template <class TopoDim, class Topology>
class ElementAssociativity;

template <class Topology>
class ElementAssociativity<TopoD3,Topology>
{
public:
  static const int DElement = 3;                // element dimensions

  typedef Topology TopologyType;             // element topology (e.g. tet, hex)
  typedef BasisFunctionVolumeBase<Topology> BasisType;
  typedef std::array<int,Topology::NFace> IntNFace;

  typedef ElementAssociativityConstructor<TopoD3, Topology> Constructor;

//  explicit ElementAssociativityVolume( int order );
  ElementAssociativity();
  explicit ElementAssociativity( const BasisType* basis );
  ElementAssociativity( const ElementAssociativity& );
  explicit ElementAssociativity( const Constructor& a ) : ElementAssociativity<TopoD3,Topology>() { operator=(a); }
  ~ElementAssociativity();

  ElementAssociativity& operator=( const ElementAssociativity<TopoD3,Topology>& );
  ElementAssociativity& operator=( const Constructor& );

  int rank() const  { return rank_; }
  int order() const { return order_; }
  int nNode() const { return nNode_; }
  int nEdge() const { return nEdge_; }
  int nFace() const { return nFace_; }
  int nCell() const { return nCell_; }
  int nDOF() const { return nNode_ + nEdge_ + nFace_ + nCell_; }
  int nBubble() const { return nCell_; }

  // node, edge, cell maps
  int nodeGlobal( int n ) const { return (nodeList_[n]); }
  int edgeGlobal( int n ) const { return (edgeList_[n]); }
  int faceGlobal( int n ) const { return (faceList_[n]); }
  int cellGlobal( int n ) const { return (cellList_[n]); }

  void getNodeGlobalMapping( int nodeMap[], int nnode ) const;
  void getNodeGlobalMapping( std::vector<int>& nodeMap ) const { getNodeGlobalMapping( nodeMap.data(), nodeMap.size());}
  void getEdgeGlobalMapping( int edgeMap[], int nedge ) const;
  void getFaceGlobalMapping( int faceMap[], int nface ) const;
  void getCellGlobalMapping( int cellMap[], int ncell ) const;
  void getGlobalMapping(     int map[]    , int ndof ) const;
  void getBubbleMapping( int bubbleMap[], int nBubble ) const { getCellGlobalMapping( bubbleMap, nBubble); }

  // edge sign accessors
  const IntNFace& faceSign() const { return faceSign_; }
  const IntNFace& traceOrientation() const { return faceSign_; }

  template<class ElemT, class T>
  void getElement(       Element<ElemT, TopoD3, Topology>& fldElem, const T* DOF, const int nDOF ) const;
  template<class ElemT, class T>
  void setElement( const Element<ElemT, TopoD3, Topology>& fldElem,       T* DOF, const int nDOF ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  int rank_;                      // the processor rank that possesses this element
  int order_;                     // polynomial order for node/edge DOFs
  int nNode_;                     // # node DOFs
  int nEdge_;                     // # edge DOFs
  int nFace_;                     // # face DOFs
  int nCell_;                     // # cell DOFs
  int* nodeList_;                 // global ordering of node DOFs
  int* edgeList_;                 // global ordering of edge DOFs
  int* faceList_;                 // global ordering of face DOFs
  int* cellList_;                 // global ordering of cell DOFs

  IntNFace faceSign_;             // +/- sign for edge orientations (i.e. left/right element)
};

// extract DOFs for individual element
template <class Topology>
template<class ElemT, class T>
void
ElementAssociativity<TopoD3,Topology>::getElement( Element<ElemT, TopoD3, Topology>& fldElem, const T* DOF, const int nDOF ) const
{
  SANS_ASSERT( (DOF != NULL) );
  SANS_ASSERT( fldElem.nDOF() == nNode_ + nEdge_ + nFace_ + nCell_ );

  int k, n, offset;

  offset = 0;
  for (n = 0; n < nNode_; n++)
  {
    k = nodeGlobal(n);
    fldElem.DOF(n + offset) = DOF[k];
  }
  offset += nNode_;

  for (n = 0; n < nEdge_; n++)
  {
    k = edgeGlobal(n);
    fldElem.DOF(n + offset) = DOF[k];
  }
  offset += nEdge_;

  for (n = 0; n < nFace_; n++)
  {
    k = faceGlobal(n);
    fldElem.DOF(n + offset) = DOF[k];
  }
  offset += nFace_;

  for (n = 0; n < nCell_; n++)
  {
    k = cellGlobal(n);
    fldElem.DOF(n + offset) = DOF[k];
  }

  fldElem.setRank( rank_ );
  fldElem.setFaceSign(faceSign_);
}


// set DOFs for individual element
template <class Topology>
template<class ElemT, class T>
void
ElementAssociativity<TopoD3,Topology>::setElement( const Element<ElemT, TopoD3, Topology>& fldElem, T* DOF, const int nDOF ) const
{
  SANS_ASSERT( (DOF != NULL) );
  SANS_ASSERT( fldElem.nDOF() == nNode_ + nEdge_ + nFace_ + nCell_ );

  int k, n, offset;

  offset = 0;
  for (n = 0; n < nNode_; n++)
  {
    k = nodeGlobal(n);
    DOF[k] = fldElem.DOF(n + offset);
  }
  offset += nNode_;

  for (n = 0; n < nEdge_; n++)
  {
    k = edgeGlobal(n);
    DOF[k] = fldElem.DOF(n + offset);
  }
  offset += nEdge_;

  for (n = 0; n < nFace_; n++)
  {
    k = faceGlobal(n);
    DOF[k] = fldElem.DOF(n + offset);
  }
  offset += nFace_;

  for (n = 0; n < nCell_; n++)
  {
    k = cellGlobal(n);
    DOF[k] = fldElem.DOF(n + offset);
  }
}

}

#endif  // ELEMENTASSOCIATIVITYVOLUME_H
