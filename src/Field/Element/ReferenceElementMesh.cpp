// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "ReferenceElementMesh.h"

#include <map>

namespace SANS
{

//---------------------------------------------------------------------------//
ReferenceElementMesh<Line>::
ReferenceElementMesh(const int nRefine)
{
  int order = pow(2,nRefine);
  int nelem = order;
  int npts = nelem + 1;

  Real ds = 1./(Real)order;

  // resize the reference coordinates and associativity
  nodes.resize(npts);
  elems.resize(nelem);

  for ( int i = 0; i < npts; i++ )
    nodes[i] = i*ds;

  for ( int i = 0; i < nelem; i++ )
  {
    elems[i][0] = i;
    elems[i][1] = i+1;
  }
}

//---------------------------------------------------------------------------//
ReferenceElementMesh<Triangle>::
ReferenceElementMesh(const int nRefine)
{
  int nelem = pow(4,nRefine);

  int order = pow(2,nRefine);
  int npts = (order+1)*(order+2)/2;

  // resize the reference coordinates and associativity
  nodes.resize(npts);
  elems.resize(nelem);

  Real ds = 1./(Real)order;

  int n = 0;
  for (int j = 0; j < order+1; j++)
  {
    for (int i = 0; i < order+1-j; i++)
    {
      nodes[n][0] = i*ds;
      nodes[n][1] = j*ds;
      n++;
    }
  }

  int n0, n1, n2, n3;
  int elemL, elemR;
  int nodeoffset = 0;
  int elemoffset = 0;
  for (int j = 0; j < order; j++)
  {
    for (int i = 0; i < order-j; i++)
    {
      n0 = nodeoffset + i;         // n3--n2
      n1 = n0 + 1;                 //  |\R|
      n2 = n1 + (order+1-j);       //  |L\|
      n3 = n0 + (order+1-j);       // n0--n1

      elemL = elemoffset + 2*i;
      elemR = elemoffset + 2*i + 1;

      elems[elemL] = {{n0, n1, n3}};
      if ( i < order-j-1)
        elems[elemR] = {{n2, n3, n1}};
    }

    nodeoffset += order+1-j;
    elemoffset += 2*(order-j-1)+1;
  }
}

//---------------------------------------------------------------------------//
ReferenceElementMesh<Quad>::
ReferenceElementMesh(const int nRefine)
{
  int nelem = pow(4,nRefine);

  int order = pow(2,nRefine);
  int npts = (order+1)*(order+1);

  // resize the reference coordinates and associativity
  nodes.resize(npts);
  elems.resize(nelem);

  Real ds = 1./(Real)order;

  int n = 0;
  for (int j = 0; j < order+1; j++)
  {
    for (int i = 0; i < order+1; i++)
    {
      nodes[n][0] = i*ds;
      nodes[n][1] = j*ds;
      n++;
    }
  }

  int n0, n1, n2, n3;
  int elem;
  for (int j = 0; j < order; j++)
  {
    for (int i = 0; i < order; i++)
    {
      n0 =  i + j*(order+1);       // n3--n2
      n1 = n0 + 1;                 //  |  |
      n2 = n1 + (order+1);         //  |  |
      n3 = n0 + (order+1);         // n0--n1

      elem = j*order + i;

      elems[elem] = {{n0, n1, n2, n3}};
    }
  }
}


/*
// Node ordering of the original hex
//
//         y
//  2----------3
//  |\     ^   |\
//  | \    |   | \
//  |  \   |   |  \
//  |   6------+---7
//  |   |  +-- |-- | -> x
//  0---+---\--1   |
//   \  |    \  \  |
//    \ |     \  \ |
//     \|      z  \|
//      4----------5

// First split hex into two prism, then divide each prism into 3 tets
//
// Nodes that make up each of the 6 hexahedron:
//
// Left prism
// Tet[0]: {0, 1, 2, 4}
// Tet[1]: {1, 4, 3, 2}
// Tet[2]: {6, 2, 3, 4}
//
// Right prism
// Tet[3]: {1, 4, 5, 3}
// Tet[4]: {4, 6, 5, 3}
// Tet[5]: {7, 6, 3, 5}
//
// The specific ordering for each tetrahedron is chosen to give positive volumes,
// but without any other consideration
*/

//---------------------------------------------------------------------------//
ReferenceElementMesh<Tet>::
ReferenceElementMesh(const int nRefine)
{

  // Index table for each tet in the hex
  const int hextets[6][4] = { {0, 1, 2, 4},
                              {1, 4, 3, 2},
                              {6, 2, 3, 4},

                              {1, 4, 5, 3},
                              {4, 6, 5, 3},
                              {7, 6, 3, 5} };

  int nelem = pow(8,nRefine);

  int order = pow(2,nRefine);
  int npts = (order+1)*(order+2)*(order+3)/6;

  // resize the reference coordinates and associativity
  nodes.resize(npts);
  elems.resize(nelem);

  std::map<int,int> nodemap;
  Real ds = 1./(Real)order;

  int joffset = (order+1);
  int koffset = (order+1)*(order+1);

  int n = 0;
  for (int k = 0; k < order+1; k++)
    for (int j = 0; j < order+1-k; j++)
      for (int i = 0; i < order+1-j-k; i++)
      {
        nodes[n][0] = i*ds;
        nodes[n][1] = j*ds;
        nodes[n][2] = k*ds;
        nodemap[i + j*joffset + k*koffset] = n;
        n++;
      }

  int elem = 0;
  for (int k = 0; k < order; k++)
  {
    for (int j = 0; j < order-k; j++)
    {
      for (int i = 0; i < order-j-k; i++)
      {
        const int n0 = i + j*joffset + k*koffset;

        //All the nodes that make up an individual hex
        const int hexnodes[8] = { n0 + 0,
                                  n0 + 1,
                                  n0 + joffset + 0,
                                  n0 + joffset + 1,

                                  n0 + koffset + 0,
                                  n0 + koffset + 1,
                                  n0 + koffset + joffset + 0,
                                  n0 + koffset + joffset + 1 };

        //Loop over all tets that make up a hex
        for (int tet = 0; tet < 6; tet++)
        {
          //Get the nodes from the hex for each tet (if the nodes exist for the tet)
          try
          {
            for (int n = 0; n < 4; n++)
              elems[elem][n] = nodemap.at(hexnodes[hextets[tet][n]]);
            elem++;
            if (elem == nelem) break;
          }
          catch (std::out_of_range&)
          {
            // ignore elements without nodes
          }
        } // tet

      } // i
    } // j
  } // k

  SANS_ASSERT( elem == nelem );
}

//---------------------------------------------------------------------------//
ReferenceElementMesh<Hex>::
ReferenceElementMesh(const int nRefine)
{
  int ii = pow(2,nRefine);
  int jj = ii;
  int kk = ii;
  int nelem = ii*jj*kk;

  int order = pow(2,nRefine);
  int npts = (order+1)*(order+1)*(order+1);

  // resize the reference coordinates and associativity
  nodes.resize(npts);
  elems.resize(nelem);

  Real ds = 1./(Real)order;

  int n = 0;
  for (int k = 0; k < order+1; k++)
  {
    for (int j = 0; j < order+1; j++)
    {
      for (int i = 0; i < order+1; i++)
      {
        nodes[n][0] = i*ds;
        nodes[n][1] = j*ds;
        nodes[n][2] = k*ds;
        n++;
      }
    }
  }

  const int joffset = (ii + 1);
  const int koffset = (ii + 1) * (jj + 1);

  int elem;
  for (int k = 0; k < kk; k++)
  {
    for (int j = 0; j < jj; j++)
    {
      for (int i = 0; i < ii; i++)
      {
        const int n0 = k * koffset + j * joffset + i;

        elem = k*order*order + j*order + i;

        elems[elem] = {{ n0 + 0,
                         n0 + 1,
                         n0 + joffset + 1,
                         n0 + joffset + 0,

                         n0 + koffset + 0,
                         n0 + koffset + 1,
                         n0 + koffset + joffset + 1,
                         n0 + koffset + joffset + 0 }};
      }
    }
  }
}

//---------------------------------------------------------------------------//
ReferenceElementMesh<Pentatope>::
ReferenceElementMesh(const int nRefine)
{

  SANS_DEVELOPER_EXCEPTION("not unit tested.");

  const int nSimplexTesseract= 24;
  const int nNodesSimplex= 5;
  const int nNodeTesseract= 16;

  // index table for each pentatope in the tesseract
  const int tesseractpentatopes[nSimplexTesseract][nNodesSimplex]=
      {{0, 8, 10, 15, 14}, {0, 8, 10, 11, 15}, {0, 8, 9, 13, 15},
       {0, 8, 9, 15, 11}, {0, 2, 10, 14, 15}, {0, 2, 10, 15, 11},
       {0, 8, 12, 14, 15}, {0, 8, 12, 15, 13}, {0, 4, 12, 15, 14},
       {0, 4, 12, 13, 15}, {0, 4, 6, 14, 15}, {0, 4, 6, 15, 7},
       {0, 4, 5, 15, 13}, {0, 4, 5, 7, 15}, {0, 2, 6, 15, 14},
       {0, 2, 6, 7, 15}, {0, 2, 3, 11, 15}, {0, 2, 3, 15, 7},
       {0, 1, 9, 15, 13}, {0, 1, 9, 11, 15}, {0, 1, 5, 13, 15},
       {0, 1, 5, 15, 7}, {0, 1, 3, 15, 11}, {0, 1, 3, 7, 15}};

  int nelem= pow(nNodeTesseract, nRefine);

  int order= pow(2, nRefine);
  int npts= (order + 1)*(order + 2)*(order + 3)*(order + 4)/nSimplexTesseract;

  nodes.resize(npts);
  elems.resize(nelem);

  std::map<int, int> nodemap;
  Real ds= 1./((Real) order);

  int jOffset= (order + 1);
  int kOffset= (order + 1)*(order + 1);
  int mOffset= (order + 1)*(order + 1)*(order + 1);

  int n= 0;
  for (int m= 0; m < order + 1; m++)
    for (int k= 0; k < order + 1 - m; k++)
      for (int j= 0; j < order + 1 - m - k; j++)
        for (int i= 0; i < order + 1 - m - k - j; i++)
        {
          nodes[n][0]= i*ds;
          nodes[n][1]= j*ds;
          nodes[n][2]= k*ds;
          nodes[n][3]= m*ds;
          nodemap[i + j*jOffset + k*kOffset + m*mOffset]= n;
          printf("nodemap[%d]= %d\n", i + j*jOffset + k*kOffset + m*mOffset, n);
          n++;
        }

  int elem= 0;
  for (int m= 0; m < order; m++)
    for (int k= 0; k < order - m; k++)
      for (int j= 0; j < order - m - k; j++)
        for (int i= 0; i < order - m - k - j; i++)
        {
          printf("gets here\n");
          const int n0= i + j*jOffset + k*kOffset + m*mOffset;

          // all the nodes that make up an individual tesseract
          const int tesseractnodes[nNodeTesseract]= {n0 + 0,
                                                     n0 + 1,
                                                     n0 + jOffset + 0,
                                                     n0 + jOffset + 1,

                                                     n0 + kOffset + 0,
                                                     n0 + kOffset + 1,
                                                     n0 + jOffset + kOffset + 0,
                                                     n0 + jOffset + kOffset + 1,

                                                     n0 + mOffset + 0,
                                                     n0 + mOffset + 1,
                                                     n0 + jOffset + mOffset + 0,
                                                     n0 + jOffset + mOffset + 1,

                                                     n0 + kOffset + mOffset + 0,
                                                     n0 + kOffset + mOffset + 1,
                                                     n0 + kOffset + jOffset + mOffset + 0,
                                                     n0 + kOffset + jOffset + mOffset + 1};

          // loop over all pentatopes that make up a tesseract
          for (int ptope= 0; ptope < nSimplexTesseract; ptope++)
          {
            // get the nodes from the tesseract for each pentatope (if the nodes exist for the tesseract)
            try
            {
              for (int n= 0; n < nNodesSimplex; n++)
                elems[elem][n]= nodemap.at(tesseractnodes[tesseractpentatopes[ptope][n]]);
              elem++;
              if (elem == nelem) break;
            }
            catch (std::out_of_range&)
            {
              printf("ignoring ptope= %d, n= %d, tesspent= %d, tessnode= %d.\n", ptope, n,
                     tesseractpentatopes[ptope][n], tesseractnodes[tesseractpentatopes[ptope][n]]);
              // ignore elements without nodes
            }
          }

        }

  printf("elem= %d\tnelem= %d\n", elem, nelem);
  SANS_ASSERT(elem == nelem);

}

}
