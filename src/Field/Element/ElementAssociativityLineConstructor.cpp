// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "ElementAssociativityLineConstructor.h"
#include "tools/SANSException.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// edge associativity: global numberings for element nodes/edges

// NOTE: default ctor needed for 'new []'
ElementAssociativityConstructor<TopoD1, Line>::ElementAssociativityConstructor()
{
  rank_  = -1;
  order_ = -1;
  nNode_ = 0;
  nEdge_ = 0;

  isSetRank_ = false;
  nodeList_ = NULL;
  edgeList_ = NULL;

  isSetNodeGlobalMapping_ = NULL;
  isSetEdgeGlobalMapping_ = NULL;
}


ElementAssociativityConstructor<TopoD1, Line>::ElementAssociativityConstructor( int order )
{
  const BasisFunctionLineBase* basis;
  switch (order)
  {
  case 1:
    basis = BasisFunctionLineBase::HierarchicalP1;
    break;
  case 2:
    basis = BasisFunctionLineBase::HierarchicalP2;
    break;
  case 3:
    basis = BasisFunctionLineBase::HierarchicalP3;
    break;
  case 4:
    basis = BasisFunctionLineBase::HierarchicalP4;
    break;
  default:
    SANS_DEVELOPER_EXCEPTION( "Unknown Hierarchical basis function order: %d", order );
    break;
  }

  rank_  = -1;
  order_ = order;
  nNode_ = basis->nBasisNode();
  nEdge_ = basis->nBasisEdge();

  isSetRank_ = false;

  if (nNode_ == 0)
  {
    nodeList_ = NULL;
    isSetNodeGlobalMapping_ = NULL;
  }
  else
  {
    nodeList_ = new int[nNode_];
    isSetNodeGlobalMapping_ = new bool[nNode_];
    for (int n = 0; n < nNode_; n++)
    {
      nodeList_[n] = -1;
      isSetNodeGlobalMapping_[n] = false;
    }
  }

  if (nEdge_ == 0)
  {
    edgeList_ = NULL;
    isSetEdgeGlobalMapping_ = NULL;
  }
  else
  {
    edgeList_ = new int[nEdge_];
    isSetEdgeGlobalMapping_ = new bool[nEdge_];
    for (int n = 0; n < nEdge_; n++)
    {
      edgeList_[n] = -1;
      isSetEdgeGlobalMapping_[n] = false;
    }
  }
}


void
ElementAssociativityConstructor<TopoD1, Line>::resize( const BasisType* basis )
{
#if 0
  std::cout << "in ElementAssociativityConstructor(basis): basis =" << std::endl;
  basis.dump( 2 );
  std::cout << "  basis.nBasisNode() = " << basis.nBasisNode()
            << "  basis.nBasisEdge() = " << basis.nBasisEdge() << std::endl;
#endif
  rank_  = -1;
  order_ = basis->order();
  nNode_ = basis->nBasisNode();
  nEdge_ = basis->nBasisEdge();

  isSetRank_ = false;

  if (nNode_ == 0)
  {
    nodeList_ = NULL;
    isSetNodeGlobalMapping_ = NULL;
  }
  else
  {
    nodeList_ = new int[nNode_];
    isSetNodeGlobalMapping_ = new bool[nNode_];
    for (int n = 0; n < nNode_; n++)
    {
      nodeList_[n] = -1;
      isSetNodeGlobalMapping_[n] = false;
    }
  }

  if (nEdge_ == 0)
  {
    edgeList_ = NULL;
    isSetEdgeGlobalMapping_ = NULL;
  }
  else
  {
    edgeList_ = new int[nEdge_];
    isSetEdgeGlobalMapping_ = new bool[nEdge_];
    for (int n = 0; n < nEdge_; n++)
    {
      edgeList_[n] = -1;
      isSetEdgeGlobalMapping_[n] = false;
    }
  }
#if 0
  std::cout << "  nodeList_ = " << nodeList_ << "  edgeList_ = " << edgeList_ << std::endl;
#endif
}


ElementAssociativityConstructor<TopoD1, Line>&
ElementAssociativityConstructor<TopoD1, Line>::operator=( const ElementAssociativityConstructor& a )
{
  if (this != &a)
  {
    rank_  = a.rank_;
    order_ = a.order_;
    nNode_ = a.nNode_;
    nEdge_ = a.nEdge_;

    isSetRank_ = a.isSetRank_;

    delete [] nodeList_; nodeList_ = NULL;
    delete [] isSetNodeGlobalMapping_; isSetNodeGlobalMapping_ = NULL;

    delete [] edgeList_; edgeList_ = NULL;
    delete [] isSetEdgeGlobalMapping_; isSetEdgeGlobalMapping_ = NULL;

    if (nNode_ > 0)
    {
      nodeList_ = new int[nNode_];
      isSetNodeGlobalMapping_ = new bool[nNode_];
      for (int n = 0; n < nNode_; n++)
      {
        nodeList_[n] = a.nodeList_[n];
        isSetNodeGlobalMapping_[n] = a.isSetNodeGlobalMapping_[n];
      }
    }

    if (nEdge_ > 0)
    {
      edgeList_ = new int[nEdge_];
      isSetEdgeGlobalMapping_ = new bool[nEdge_];
      for (int n = 0; n < nEdge_; n++)
      {
        edgeList_[n] = a.edgeList_[n];
        isSetEdgeGlobalMapping_[n] = a.isSetEdgeGlobalMapping_[n];
      }
    }
  }

  return *this;
}

ElementAssociativityConstructor<TopoD1, Line>::~ElementAssociativityConstructor()
{
#if 0
  std::cout << "in ~ElementAssociativityConstructor()" << std::endl;
  std::cout << "  nodeList_ = " << nodeList_ << "  edgeList_ = " << edgeList_ << std::endl;
  std::cout << "  isSetNodeGlobalMapping_ = " << isSetNodeGlobalMapping_ << "  isSetEdgeGlobalMapping_ = " << isSetEdgeGlobalMapping_ << std::endl;
#endif
  delete [] nodeList_;
  delete [] edgeList_;

  delete [] isSetNodeGlobalMapping_;
  delete [] isSetEdgeGlobalMapping_;
}


void
ElementAssociativityConstructor<TopoD1, Line>::setNodeGlobalMapping( const int node[], int nnode )
{
  SANS_ASSERT( nnode == nNode_ );

  for (int n = 0; n < nNode_; n++)
  {
    nodeList_[n] = node[n];
    isSetNodeGlobalMapping_[n] = true;
  }
}

void
ElementAssociativityConstructor<TopoD1, Line>::setNodeGlobalMapping( const std::vector<int>& node )
{
  SANS_ASSERT( (int)node.size() == nNode_ );

  for (std::size_t n = 0; n < node.size(); n++)
  {
    nodeList_[n] = node[n];
    isSetNodeGlobalMapping_[n] = true;
  }
}

void
ElementAssociativityConstructor<TopoD1, Line>::getNodeGlobalMapping( int node[], int nnode ) const
{
  SANS_ASSERT( nnode == nNode_ );
  for (int n = 0; n < nNode_; n++)
  {
    SANS_ASSERT( isSetNodeGlobalMapping_[n] );
    node[n] = nodeList_[n];
  }
}


void
ElementAssociativityConstructor<TopoD1, Line>::setEdgeGlobalMapping( const int edge[], int nedge )
{
  SANS_ASSERT_MSG( nedge == nEdge_, "nedge = %d  nEdge_ = %d", nedge, nEdge_ );

  for (int n = 0; n < nEdge_; n++)
  {
    edgeList_[n] = edge[n];
    isSetEdgeGlobalMapping_[n] = true;
  }
}

void
ElementAssociativityConstructor<TopoD1, Line>::getEdgeGlobalMapping( int edge[], int nedge ) const
{
  SANS_ASSERT( nedge == nEdge_ );

  for (int n = 0; n < nEdge_; n++)
  {
    edge[n] = edgeList_[n];
    SANS_ASSERT( isSetEdgeGlobalMapping_[n] );
  }
}

void
ElementAssociativityConstructor<TopoD1, Line>::setEdgeGlobalMapping( const std::vector<int>& edge )
{
  SANS_ASSERT_MSG( (int)edge.size() == nEdge_, "edge.size() = %d, nEdge_ = %d\n", edge.size(), nEdge_ );

  for (int n = 0; n < nEdge_; n++)
  {
    edgeList_[n] = edge[n];
    isSetEdgeGlobalMapping_[n] = true;
  }
}

void
ElementAssociativityConstructor<TopoD1, Line>::setCellGlobalMapping( const int cell[], int ncell ) { setEdgeGlobalMapping(cell,ncell); }

void
ElementAssociativityConstructor<TopoD1, Line>::getCellGlobalMapping( int cell[], int ncell ) const { getEdgeGlobalMapping(cell,ncell); }

void
ElementAssociativityConstructor<TopoD1, Line>::setCellGlobalMapping( const std::vector<int>& cell ) { setEdgeGlobalMapping(cell); }


void
ElementAssociativityConstructor<TopoD1, Line>::setGlobalMapping( const int map[], int ndof )
{
#if 0
  std::cout << "ElementAssociativityConstructor<TopoD1, Line>::setGlobalMapping  dumping..." << std::endl;
  this->dump(2);
#endif
  SANS_ASSERT( ndof == nNode_ + nEdge_ );

  int offset = 0;
  for (int n = 0; n < nNode_; n++)
  {
    nodeList_[n] = map[n + offset];
    isSetNodeGlobalMapping_[n] = true;
  }
  offset += nNode_;

  for (int n = 0; n < nEdge_; n++)
  {
    edgeList_[n] = map[n + offset];
    isSetEdgeGlobalMapping_[n] = true;
  }
}


void
ElementAssociativityConstructor<TopoD1, Line>::setGlobalMapping( const std::vector<int>& map )
{
  setGlobalMapping(map.data(), static_cast<int>(map.size()));
}


void
ElementAssociativityConstructor<TopoD1, Line>::getGlobalMapping( int map[], int ndof ) const
{
#if 0
  std::cout << "ElementAssociativityConstructor<TopoD1, Line>::getGlobalMapping  dumping..." << std::endl;
  this->dump(2);
#endif
  SANS_ASSERT( ndof == nNode_ + nEdge_ );

  int offset = 0;
  for (int n = 0; n < nNode_; n++)
  {
    SANS_ASSERT_MSG( isSetNodeGlobalMapping_[n], "n = %d", n );
    map[n + offset] = nodeList_[n];
  }
  offset += nNode_;

  for (int n = 0; n < nEdge_; n++)
  {
    SANS_ASSERT_MSG( isSetEdgeGlobalMapping_[n], "n = %d", n );
    map[n + offset] = edgeList_[n];
  }
}


void
ElementAssociativityConstructor<TopoD1, Line>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "ElementAssociativityConstructor<TopoD1, Line>: order_ = " << order_
      << "  nNode_ = " << nNode_
      << "  nEdge_ = " << nEdge_
      << "  rank_  = " << rank_ << std::endl;
  if ((nNode_ > 0))
  {
    out << indent << indent << "nodeList = ";
    for (int n = 0; n < nNode_; n++)
      out << nodeList_[n] << " ";
    out << std::endl;

    out << indent << indent << "isSetNodeGlobalMapping = ";
    for (int n = 0; n < nNode_; n++)
      out << isSetNodeGlobalMapping_[n] << " ";
    out << std::endl;
  }
  if ((nEdge_ > 0))
  {
    out << indent << indent << "edgeList = ";
    for (int n = 0; n < nEdge_; n++)
      out << edgeList_[n] << " ";
    out << std::endl;

    out << indent << indent << "isSetEdgeGlobalMapping = ";
    for (int n = 0; n < nEdge_; n++)
      out << isSetEdgeGlobalMapping_[n] << " ";
    out << std::endl;
  }
}

}
