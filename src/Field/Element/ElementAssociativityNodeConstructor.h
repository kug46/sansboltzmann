// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ELEMENTASSOCIATIVITYNODECONSTRUCTOR_H
#define ELEMENTASSOCIATIVITYNODECONSTRUCTOR_H

// Constructor class for element node associativity (local to global mappings)

#include <iostream>
#include <vector>
#include <array>

#include "tools/SANSException.h"
#include "BasisFunction/BasisFunctionNode.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// edge associativity constructor: global numberings for single element nodes/edge
//
// member functions:
//   .order                     polynomial order
//   .nNode                     # node DOFs
//   .nodeGlobal                global node/edge DOF accessors
//   .set/getNodeGlobalMapping  local-to-global node DOF mapping
//
//   .dump                      debug dump of private data
//----------------------------------------------------------------------------//

template <class TopoDim, class Topology>
class ElementAssociativityConstructor;

template<>
class ElementAssociativityConstructor<TopoD0, Node>
{
public:
  typedef TopoD0 TopoDim;
  typedef Node TopologyType;             // element topology
  typedef BasisFunctionNodeBase BasisType;

  ElementAssociativityConstructor();
  explicit ElementAssociativityConstructor( int order );
  explicit ElementAssociativityConstructor( const BasisType* basis ) : ElementAssociativityConstructor() { resize(basis); }
  ~ElementAssociativityConstructor();

  ElementAssociativityConstructor( const ElementAssociativityConstructor& a ) : ElementAssociativityConstructor() { *this = a; }
  ElementAssociativityConstructor& operator=( const ElementAssociativityConstructor& );

  void resize( const BasisType* basis );

  int rank() const  { SANS_ASSERT(isSetRank_); return rank_; }
  int order() const { return 0; }
  int nNode() const { return 1; }

  void setRank(const int rank) { isSetRank_ = true; rank_ = rank; }

  // node map
  int nodeGlobal( int n ) const { SANS_ASSERT(isSetNodeGlobalMapping_); return nodeList_; }

  void setNodeGlobalMapping( const int node[], int nnode );
  void getNodeGlobalMapping(       int node[], int nnode ) const;

  void setNodeGlobalMapping( const std::vector<int>& node );

  void setGlobalMapping( const int map[], int ndof );
  void getGlobalMapping(       int map[], int ndof ) const;

  void setGlobalMapping( const std::vector<int>& map );

  void dump( int indentSize, std::ostream& out = std::cout ) const;

  int normalSignL() const { return normalSignL_; }
  int normalSignR() const { return normalSignR_; }
  void setNormalSignL(const int sgn) { SANS_ASSERT(abs(sgn)==1); normalSignL_ = sgn; }
  void setNormalSignR(const int sgn) { SANS_ASSERT(abs(sgn)==1); normalSignR_ = sgn; }

  void setEdgeSign( const int i0 , const int i1 ); // pcaplan dummy
//  void setOrientation( int i0, int i1 ); // hcarson dummy

private:
  int rank_;                      // the processor rank that possesses this element
  int nodeList_;                  // global ordering of node DOFs
  int normalSignL_;               // trace normal sign with respect to the left element
  int normalSignR_;               // trace normal sign with respect to the right element

  bool isSetRank_;                 // flag: has the processor rank been set
  bool isSetNodeGlobalMapping_;   // flag: is nodeList_ set yet?

};

}
#endif  // ELEMENTASSOCIATIVITYNODECONSTRUCTOR_H
