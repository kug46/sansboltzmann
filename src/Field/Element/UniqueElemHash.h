// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef UNIQUE_ELEM_HASH_H
#define UNIQUE_ELEM_HASH_H

#include "UniqueElem.h"

// both of these header files are "heavy" which is why UniqueElemHash.h is a
// separate file from UniqueElem.h
#include <functional> // std::hash
#include <boost/functional/hash.hpp>

//----------------------------------------------------------------------------//
// UniqueElem Hash Functions
//
// Provides hashing function so UniqueElem can be used with unordered containers
// e.g. uordered_set
//----------------------------------------------------------------------------//

namespace std
{

template<> struct hash<SANS::UniqueElem>
{
  typedef SANS::UniqueElem argument_type;
  typedef std::size_t result_type;
  result_type operator()(argument_type const& elem) const noexcept
  {
    std::size_t seed = 0;
    boost::hash_combine(seed, elem.topo);
    for (int n : elem.sortedNodes())
      boost::hash_combine(seed, n);

    return seed;
  }
};

} // namespace std

namespace SANS
{
  inline std::size_t hash_value(const UniqueElem& elem)
  {
    std::size_t seed = 0;
    boost::hash_combine(seed, elem.topo);
    for (int n : elem.sortedNodes())
      boost::hash_combine(seed, n);

    return seed;
  }
}

#endif // UNIQUE_ELEM_HASH_H
