// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(ELEMENTXFIELDAREA_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include <iomanip> //std::setprecision

#include "ElementXFieldArea.h"

#include "Quadrature/Quadrature.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "ReferenceElementMesh.h"
#include "Field/output_Tecplot/TecTopo.h"

namespace SANS
{

//---------------------------------------------------------------------------//
template <class PhysDim, class Topology>
Real
ElementXField<PhysDim, TopoD2, Topology>::area() const
{
  Real s = Topology::centerRef[0];
  Real t = Topology::centerRef[1];
  return area( s, t );
}

template <class PhysDim, class Topology>
Real
ElementXField<PhysDim, TopoD2, Topology>::area( const Real& sRef, const Real& tRef ) const
{
  Matrix J;
  jacobian( sRef, tRef, J );

  return area( J );
}

template <class PhysDim, class Topology>
Real
ElementXField<PhysDim, TopoD2, Topology>::area( const QuadraturePoint<TopoD2>& sRef ) const
{
  Matrix J;
  jacobian( sRef, J );

  return area( J );
}

//---------------------------------------------------------------------------//
template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD2, Topology>::impliedMetric( TensorSymX& M ) const
{
  Real s = Topology::centerRef[0];
  Real t = Topology::centerRef[1];
  impliedMetric( s, t, M );
}

//---------------------------------------------------------------------------//
template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD2, Topology>::evalReferenceCoordinateGradient(
    const Real& s, const Real& t, VectorX& sgrad, VectorX& tgrad ) const
{
  evalBasisDerivative( s, t, phis_, phit_, nDOF_ );
  evalReferenceCoordinateGradient( phis_, phit_, sgrad, tgrad );
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD2, Topology>::evalReferenceCoordinateGradient(
    const RefCoordType& Ref, VectorX& sgrad, VectorX& tgrad ) const
{
  evalBasisDerivative( Ref[0], Ref[1], phis_, phit_, nDOF_ );
  evalReferenceCoordinateGradient( phis_, phit_, sgrad, tgrad );
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD2, Topology>::evalReferenceCoordinateGradient(
    const QuadraturePoint<TopoD2>& ref, VectorX& sgrad, VectorX& tgrad ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalReferenceCoordinateGradient(ref.ref[0], ref.ref[1], sgrad, tgrad);
    return;
  }

  const QuadratureBasisPointValues<TopoD>& cache = QuadratureCacheValues<TopoD2, Topology>::get( pointStoreCell_, ref, edgeSign_ );
  const std::vector<Real>& phis = cache.dphi[0];
  const std::vector<Real>& phit = cache.dphi[1];

  evalReferenceCoordinateGradient( phis.data(), phit.data(), sgrad, tgrad );
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD2, Topology>::evalReferenceCoordinateGradient(
    const QuadratureCellTracePoint<TopoD2>& ref, VectorX& sgrad, VectorX& tgrad ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalReferenceCoordinateGradient(ref.ref[0], ref.ref[1], sgrad, tgrad);
    return;
  }

  const QuadratureBasisPointValues<TopoD>& cache = QuadratureCacheValues<TopoD2, Topology>::get(pointStoreTrace_, ref, edgeSign_);
  const std::vector<Real>& phis = cache.dphi[0];
  const std::vector<Real>& phit = cache.dphi[1];

  evalReferenceCoordinateGradient( phis.data(), phit.data(), sgrad, tgrad );
}

//---------------------------------------------------------------------------//
template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD2, Topology>::jacobian( Matrix& J ) const
{
  Real s = Topology::centerRef[0];
  Real t = Topology::centerRef[1];
  jacobian( s, t, J );
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD2, Topology>::jacobian( const Real& s, const Real& t, Matrix& J ) const
{
  evalBasisDerivative( s, t, phis_, phit_, nDOF_ );
  jacobian(phis_, phit_, J);
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD2, Topology>::jacobian( const QuadraturePoint<TopoD2>& ref, Matrix& J ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    jacobian(ref.ref[0], ref.ref[1], J);
    return;
  }

  const QuadratureBasisPointValues<TopoD>& cache = QuadratureCacheValues<TopoD2, Topology>::get( pointStoreCell_, ref, edgeSign_ );
  const std::vector<Real>& phis = cache.dphi[0];
  const std::vector<Real>& phit = cache.dphi[1];

  jacobian(phis.data(), phit.data(), J);
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD2, Topology>::jacobian(
    const Real* __restrict phis, const Real* __restrict phit, Matrix& J ) const
{
  J = 0;
  for (int n = 0; n < nDOF_; n++)
    for (int d = 0; d < D; d++)
    {
      J(d,0) += phis[n]*DOF_[n](d); //d(x_i)/ds
      J(d,1) += phit[n]*DOF_[n](d); //d(x_i)/dt
    }
}

//---------------------------------------------------------------------------//
template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD2, Topology>::hessian( const Real& s, const Real& t, TensorSymHessian& H ) const
{
  // second derivatives basis wrt (s,t)
  basis_->evalBasisHessianDerivative( s, t, edgeSign_, phiss_, phist_, phitt_, nDOF_ );

  H = 0;
  for (int n = 0; n < nDOF_; n++)
    for (int d = 0; d < D; d++)
    {
      H(d)(0,0) += phiss_[n]*DOF_[n](d); //d^2(x_i)/d(s^2)
      H(d)(1,0) += phist_[n]*DOF_[n](d); //d^2(x_i)/d(st)
      H(d)(1,1) += phitt_[n]*DOF_[n](d); //d^2(x_i)/d(t^2)
    }
}

//----------------------------------------------------------------------------//
// basis gradient
// Returns gradient of basis functions in Cartesian coords, transforms from the master element
template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD2, Topology>::evalBasisGradient( const RefCoordType& sRef,
                                                             const ElementBasis<TopoD2, Topology>& fldElem,
                                                             VectorX gradphi[], int nphi ) const
{
  evalBasisGradient<RefCoordType>(sRef, fldElem, gradphi, nphi);
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD2, Topology>::evalBasisGradient( const QuadraturePoint<TopoD2>& sRef,
                                                             const ElementBasis<TopoD2, Topology>& fldElem,
                                                             VectorX gradphi[], int nphi ) const
{
  evalBasisGradient<QuadraturePoint<TopoD2>>(sRef, fldElem, gradphi, nphi);
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD2, Topology>::evalBasisGradient( const QuadratureCellTracePoint<TopoD2>& sRef,
                                                             const ElementBasis<TopoD2, Topology>& fldElem,
                                                             VectorX gradphi[], int nphi ) const
{
  evalBasisGradient<QuadratureCellTracePoint<TopoD2>>(sRef, fldElem, gradphi, nphi);
}

template <class PhysDim, class Topology>
template <class RefCoord>
void
ElementXField<PhysDim, TopoD2, Topology>::evalBasisGradient( const RefCoord& sRef,
                                                             const ElementBasis<TopoD2, Topology>& fldElem,
                                                             VectorX gradphi[], int nphi ) const
{
  const int nBasis = fldElem.nDOF();   // total DOFs in element
  SANS_ASSERT(nphi == nBasis);

  VectorX sgrad, tgrad;        // gradients of reference coordinates (s,t)
  const Real *phis, *phit;     // derivatives of solution basis wrt (s,t)

  evalReferenceCoordinateGradient( sRef, sgrad, tgrad );

  fldElem.evalBasisDerivative( sRef, &phis, &phit );

  for (int n = 0; n < nphi; n++)
    gradphi[n] = phis[n]*sgrad + phit[n]*tgrad;
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD2, Topology>::evalBasisGradient( const BasisPointDerivative<TopoD2::D>& dphi,
                                                             VectorX gradphi[], int nphi ) const
{
  SANS_ASSERT(nphi == dphi.size());

  VectorX sgrad, tgrad;           // gradients of reference coordinates (s,t)
  const Real* phis = dphi.deriv(0);    // derivatives of solution basis wrt s
  const Real* phit = dphi.deriv(1);    // derivatives of solution basis wrt t

  evalReferenceCoordinateGradient( dphi.ref()[0], dphi.ref()[1], sgrad, tgrad );

  for (int n = 0; n < nphi; n++)
    gradphi[n] = phis[n]*sgrad + phit[n]*tgrad;
}

//----------------------------------------------------------------------------//
// basis hessian
// Returns the hessian of basis functions in cartesian coords, transforms from the master element
template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD2, Topology>::evalBasisHessian( const RefCoordType& sRef,
                                                            const ElementBasis<TopoD2, Topology>& fldElem,
                                                            TensorSymX hessphi[], int nphi ) const
{
  evalBasisHessian<RefCoordType>(sRef, fldElem, hessphi, nphi);
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD2, Topology>::evalBasisHessian( const QuadraturePoint<TopoD2>& sRef,
                                                            const ElementBasis<TopoD2, Topology>& fldElem,
                                                            TensorSymX hessphi[], int nphi ) const
{
  evalBasisHessian<QuadraturePoint<TopoD2>>(sRef, fldElem, hessphi, nphi);
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD2, Topology>::evalBasisHessian( const QuadratureCellTracePoint<TopoD2>& sRef,
                                                            const ElementBasis<TopoD2, Topology>& fldElem,
                                                            TensorSymX hessphi[], int nphi ) const
{
  evalBasisHessian<RefCoordType>(sRef.ref, fldElem, hessphi, nphi);
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD2, Topology>::dumpTecplot( std::ostream& out ) const
{
  int nRefine = this->order_ == 1 ? 0 : 4;
  ReferenceElementMesh<Topology> refMesh(nRefine);
  const int nRefnode = refMesh.nodes.size();
  const int nRefelem = refMesh.elems.size();

  std::vector<std::string> xyz = {"\"X\" ", "\"Y\" ", "\"Z\" "};

  out << std::endl;
  out << "VARIABLES = ";
  for (int d = 0; d < PhysDim::D; d++)
    out << xyz[d];
  out << std::endl;
  out << "ZONE"
      << " N=" << nRefnode
      << " E=" << nRefelem
      << " F=FEPOINT"
      << " ET=" << TecTopo<Topology>::name()
      << std::endl;

  VectorX X;
  RefCoordType sRefCell;

  for ( int i = 0; i < nRefnode; i++ )
  {
    sRefCell = refMesh.nodes[i];

    this->eval( sRefCell, X );

    for (int k = 0; k < PhysDim::D; k++)
      out << std::scientific << std::setprecision(16) << X[k] << " ";
    out << std::endl;
  }

  for (int relem = 0; relem < nRefelem; relem++)
  {
    for (int n = 0; n < Topology::NNode; n++ )
      out << refMesh.elems[relem][n] + 1 << " "; // +1 for one based indexing
    out << std::endl;
  }
}

}
