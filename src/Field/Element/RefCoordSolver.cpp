// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "RefCoordSolver.h"

#include <type_traits>
#include <sstream>
#include <iomanip> //std::setprecision

#include "LinearAlgebra/DenseLinAlg/InverseLDLT.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"
#include "LinearAlgebra/DenseLinAlg/tools/SingularException.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "BasisFunction/ReferenceElement.h"

#include "Field/Element/ElementXFieldNode.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "Field/Element/ElementXFieldVolume.h"
#include "Field/Element/ElementXFieldSpacetime.h"

namespace SANS
{

template<class PhysDim, class TopoDim, class Topology>
bool
RefCoordSolver<PhysDim,TopoDim,Topology>::solve( const VectorX& X0, RefCoordType& sRef_sol )
{
  //sRef_sol is the initial guess

  const int maxIter = 10;
  const Real ftol = 1e-12;
  const Real xtol = 1e-12;

  Real resNorm = 0;
  RefCoordType sRef_sol0;
  bool converged = false;

  // First try solving the unconstrained optimization problem
  for (int iter = 0; iter < maxIter; iter++)
  {
    RefCoordType res;
    residual(sRef_sol, X0, res);

    resNorm = sqrt(dot(res,res));
    if (resNorm <= ftol)
      return true;

    MatrixJac jac;
    jacobian(sRef_sol, X0, jac);

    RefCoordType dsRef;
    try
    {
      dsRef = -SANS::DLA::InverseLDLT::Solve( jac, res);
    }
    catch ( DLA::SingularMatrixException& e)
    {
      // add more useful information
      std::stringstream msg;
      msg << std::endl << std::endl;
      msg << "while trying to invert Jacobian matrix:" << std::endl << std::endl;
      msg << std::scientific << std::setprecision(16);
      msg << jac << std::endl;
      msg << "Determinant: " << DLA::Det(jac) << std::endl << std::endl;
      xfldElem_.dump(3, msg);
      xfldElem_.dumpTecplot(msg);
      e.add(msg.str());
      throw e;
    }

    sRef_sol0 = sRef_sol;
    linesearch(sRef_sol0, dsRef, sRef_sol); //limit the update if needed

    dsRef = sRef_sol - sRef_sol0;

    if ( sqrt(dot(dsRef,dsRef)) <= xtol ) //exit loop if solution doesn't change by much
    {
      if ( std::is_same<Topology, Node>::value ||
           std::is_same<Topology, Line>::value )
        return true; // No need to search bound constraints with Lines or Nodes
      else
      {
        converged = true;
        break;
      }
    }
  }

  Real resNorm_min = resNorm;
  RefCoordType sRef_min = sRef_sol;

  typename ReferenceElement<Topology>::MatrixConst jacLG;
  typename ReferenceElement<Topology>::VectorConst resLG, dsRefLG;

  // Solve Newton's method constrained to each trace of the element
  for (int itrace = 0; itrace < Topology::NTrace; itrace++)
  {
    jacLG = 0;
    resLG = 0;

    // add the trace constraint vi Lagrange multipliers to the system
    ReferenceElement<Topology>::constraintJacobian(itrace, jacLG);

    for (int iter = 0; iter < maxIter; iter++)
    {
      RefCoordType res;
      residual(sRef_sol, X0, res);

      resNorm = sqrt(dot(res,res));
      if (resNorm <= ftol)
        return true;

      // copy over the residual
      for (int m = 0; m < RefCoordType::M; m++)
        resLG[m] = res[m];

      ReferenceElement<Topology>::constraintResidual(itrace, sRef_sol, resLG);

      MatrixJac jac;
      jacobian(sRef_sol, X0, jac);

      // copy over the Hessian (Jacobian)
      for (int m = 0; m < RefCoordType::M; m++)
        for (int n = 0; n <= m; n++)
          jacLG(m,n) = jac(m,n);

      try
      {
        dsRefLG = -SANS::DLA::InverseLDLT::Solve( jacLG, resLG );
      }
      catch ( DLA::SingularMatrixException& e)
      {
        // add more useful information
        std::stringstream msg;
        msg << std::endl << std::endl;
        msg << "while trying to invert constrained Jacobian matrix:" << std::endl << std::endl;
        msg << std::scientific << std::setprecision(16);
        msg << jacLG << std::endl;
        //msg << "Determinant: " << DLA::Det(jacLG) << std::endl << std::endl;
        xfldElem_.dump(3, msg);
        xfldElem_.dumpTecplot(msg);
        e.add(msg.str());
        throw e;
      }

      RefCoordType dsRef;
      for (int m = 0; m < RefCoordType::M; m++)
        dsRef[m] = dsRefLG[m];

      sRef_sol0 = sRef_sol;
      linesearch(sRef_sol0, dsRef, sRef_sol); //limit the update if needed

      dsRef = sRef_sol - sRef_sol0;

      if ( sqrt(dot(dsRef,dsRef)) <= xtol ) //exit loop if solution doesn't change by much
      {
        converged = true;
        break;
      }
    }

    // check if the current solution norm is smaller
    if (resNorm < resNorm_min)
    {
      // New norm is smaller, save it as the best solution
      resNorm_min = resNorm;
      sRef_min = sRef_sol;
    }
    else
    {
      // Reset to the minimum norm found so far
      resNorm = resNorm_min;
      sRef_sol = sRef_min;
    }
  }

  return converged;
}

template<class PhysDim, class TopoDim, class Topology>
void
RefCoordSolver<PhysDim,TopoDim,Topology>::residual(const RefCoordType& sRef, const VectorX& X0, RefCoordType& res)
{
  /*
   * Let V be the reference coordinates [s,t,u]
   * Need to minimize squared distance
   *
   * Z(V) = (X(V) - X0)^T (X(V) - X0)
   *
   * At minimum, dZ/dV = 0 --> 2*(dX/dV)^T * (X(V) - X0) = 0
   *
   * Hence the residual is R(V) = (dX/dV)^T * (X(V) - X0)
  */

  VectorX X; // physical coordinates
  xfldElem_.coordinates( sRef, X );

  Matrix J; // element Jacobian
  xfldElem_.jacobian( sRef, J );

  res = Transpose(J)*(X - X0);
}

template<class PhysDim, class TopoDim, class Topology>
void
RefCoordSolver<PhysDim,TopoDim,Topology>::jacobian(const RefCoordType& sRef, const VectorX& X0, MatrixJac& jac)
{
  /*
   * The Jacobian of the above residual is given by:
   * dR/dV = (d^X/dV^2)^T * (X(V) - X0) + (dX/dV)^T * (dX/dV)
   */

  VectorX X, dX; // physical coordinates
  xfldElem_.coordinates( sRef, X );
  dX = X - X0;

  Matrix J; // element Jacobian
  xfldElem_.jacobian( sRef, J );

  Hessian H; // element Hessian
  xfldElem_.hessian( sRef, H );

  jac = dot(H, dX);
  jac += Transpose(J)*J;
}

template<class PhysDim, class TopoDim, class Topology>
bool
RefCoordSolver<PhysDim,TopoDim,Topology>::linesearch(const RefCoordType& sRef0, const RefCoordType& dsRef, RefCoordType& sRefnew)
{
  //Note that dsRef is already negated
  return ReferenceElement<Topology>::getInteriorSolution(sRef0, dsRef, sRefnew); //returns true if update was limited
}

//Special case for nodes
template<class PhysDim>
bool
RefCoordSolver<PhysDim,TopoD0, Node>::solve(const VectorX& X0, RefCoordType& sRef_sol)
{
  //The only possible solution is the node itself
  sRef_sol = Node::centerRef;

  // physical coordinates
  VectorX Xtmp, dX;
  xfldElem_.coordinates( sRef_sol, Xtmp );
  dX = Xtmp - X0;

  return (sqrt(dot(dX,dX)) < 1e-13);
}


//Explicit instantiations
template class RefCoordSolver<PhysD1, TopoD0, Node>;
template class RefCoordSolver<PhysD2, TopoD0, Node>;
template class RefCoordSolver<PhysD3, TopoD0, Node>;

template class RefCoordSolver<PhysD1, TopoD1, Line>;
template class RefCoordSolver<PhysD2, TopoD1, Line>;
template class RefCoordSolver<PhysD3, TopoD1, Line>;

template class RefCoordSolver<PhysD2, TopoD2, Triangle>;
template class RefCoordSolver<PhysD3, TopoD2, Triangle>;
template class RefCoordSolver<PhysD2, TopoD2, Quad>;
template class RefCoordSolver<PhysD3, TopoD2, Quad>;

template class RefCoordSolver<PhysD3, TopoD3, Tet>;
template class RefCoordSolver<PhysD3, TopoD3, Hex>;

template class RefCoordSolver<PhysD4, TopoD4, Pentatope>;

} //namespace SANS
