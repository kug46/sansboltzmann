// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ELEMENTXFIELDAREA_INSTANTIATE

#include "ElementXFieldArea_impl.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/InverseLU.h"
#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/tools/cross.h"
#include "ElementXFieldJacobianEquilateral.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// 3-D Cartesian
//----------------------------------------------------------------------------//

//---------------------------------------------------------------------------//
template <class PhysDim, class Topology>
inline Real
ElementXField<PhysDim, TopoD2, Topology>::area( const Matrix& J ) const
{
  Real xs, xt, ys, yt, zs, zt;
  Real det;

  xs = J(0,0);  xt = J(0,1);
  ys = J(1,0);  yt = J(1,1);
  zs = J(2,0);  zt = J(2,1);

  det = sqrt(pow(ys*zt-zs*yt,2) + pow(zs*xt-xs*zt,2) + pow(xs*yt-xt*ys,2));
  return Topology::areaRef*det;
}

//---------------------------------------------------------------------------//
void AddCurvedBBoxPoints(const ElementXField<PhysD3, TopoD2, Triangle>& xfldElem, BoundingBox<PhysD3>& bbox)
{
  typedef DLA::VectorS<PhysD3::D,Real> VectorX;
  typedef DLA::VectorS<TopoD2::D,Real> RefCoordCell;

  VectorX X;
  RefCoordCell sRefCell = 0;

  // Grab coordinates on the cell (including on edges) to add to the bounding box
  int kmax = 9;
  Real dRef = 1./(kmax-1);
  for (int ki = 0; ki < kmax; ki++)
  {
    sRefCell[1] = 0;
    for (int kj = ki; kj < kmax; kj++)
    {
      xfldElem.coordinates(sRefCell, X);

      bbox.setBound(X);

      sRefCell[1] += dRef;
    }
    sRefCell[0] += dRef;
  }
}

//---------------------------------------------------------------------------//
void AddCurvedBBoxPoints(const ElementXField<PhysD3, TopoD2, Quad>& xfldElem, BoundingBox<PhysD3>& bbox)
{
  typedef DLA::VectorS<PhysD3::D,Real> VectorX;
  typedef DLA::VectorS<TopoD2::D,Real> RefCoordCell;

  VectorX X;
  RefCoordCell sRefCell = 0;

  // Grab coordinates on the cell (including on edges) to add to the bounding box
  int kmax = 9;
  Real dRef = 1./(kmax-1);
  for (int ki = 0; ki < kmax; ki++)
  {
    for (int kj = 0; kj < kmax; kj++)
    {
      sRefCell[0] = dRef*ki;
      sRefCell[1] = dRef*kj;

      xfldElem.coordinates(sRefCell, X);

      bbox.setBound(X);
    }
  }
}

//---------------------------------------------------------------------------//
template <class PhysDim, class Topology>
BoundingBox<PhysDim>
ElementXField<PhysDim, TopoD2, Topology>::boundingBox() const
{
  BoundingBox<PhysDim> bbox;

  // get the corners of the element
  for (int n = 0; n < Topology::NNode; n++)
    bbox.setBound(DOF_[n]);

  if ( basis_->order()  > 1 )
    AddCurvedBBoxPoints(*this, bbox);

  return bbox;
}

//---------------------------------------------------------------------------//
template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD2, Topology>::impliedMetric(
    const Real& s, const Real& t, TensorSymX& M ) const
{
  SANS_DEVELOPER_EXCEPTION("ElementXField<PhysD3, TopoD2, Topology>::impliedMetric - Not implemented yet.");

#if 0
  Matrix Jref; //jacobian from reference element to physical element
  jacobian( s, t, Jref );

  // transformation matrix from the unit equilateral element to the reference element (in 2D)
  const DLA::MatrixS<PhysD2::D,TopoD,Real>& invJeq = JacobianEquilateralTransform<PhysD2,TopoD2,Topology>::invJeq;

  Matrix J = Jref*invJeq; //Jacobian from equilateral element to physical element

  TensorSymX JJt = J*Transpose(J);

  // Compute implied metric M = inv(JJt)
  Real det =   JJt(0,0)*( JJt(1,1)*JJt(2,2) - JJt(1,2)*JJt(2,1) )
             - JJt(0,1)*( JJt(1,0)*JJt(2,2) - JJt(1,2)*JJt(2,0) )
             + JJt(0,2)*( JJt(1,0)*JJt(2,1) - JJt(1,1)*JJt(2,0) );

  M(0,0) = (JJt(1,1)*JJt(2,2) - JJt(1,2)*JJt(2,1)) / det;
  M(1,0) = (JJt(1,2)*JJt(2,0) - JJt(1,0)*JJt(2,2)) / det;
  M(1,1) = (JJt(0,0)*JJt(2,2) - JJt(0,2)*JJt(2,0)) / det;
  M(2,0) = (JJt(1,0)*JJt(2,1) - JJt(1,1)*JJt(2,0)) / det;
  M(2,1) = (JJt(0,1)*JJt(2,0) - JJt(0,0)*JJt(2,1)) / det;
  M(2,2) = (JJt(0,0)*JJt(1,1) - JJt(0,1)*JJt(1,0)) / det;
#endif
}

template <class PhysDim, class Topology>
inline void
ElementXField<PhysDim, TopoD2, Topology>::evalReferenceCoordinateGradient(
    const Real* __restrict phis, const Real* __restrict phit, VectorX& sgrad, VectorX& tgrad ) const
{
#if 1
  // dX = [dx, dy, dz]^T
  // dS = [ds, dt]^T
  // J = [[dx/ds, dx/dt]
  //      [dy/ds, dy/dt]
  //      [dz/ds, dz/dt]]
  //
  // Solving the system
  // Transpose(J)*[dX] = Transpose(J)*J*[dS]
  // where J projects dX onto the tangent of the triangle
  //
  // For a slight performance gain, J^T is instead computed and the following is solved
  //
  // JT*[dX] = JT*Transpose(JT)*[dS]

  DLA::MatrixS<2,3,Real> JT = 0;
  DLA::MatrixS<2,2,Real> tmp;
  DLA::MatrixS<2,3,Real> stxyz;

  for (int n = 0; n < nDOF_; n++)
  {
    //xs                            ys                              zs
    //xt                            yt                              zt
    JT(0,0) += phis[n]*DOF_[n](0);  JT(0,1) += phis[n]*DOF_[n](1);  JT(0,2) += phis[n]*DOF_[n](2);
    JT(1,0) += phit[n]*DOF_[n](0);  JT(1,1) += phit[n]*DOF_[n](1);  JT(1,2) += phit[n]*DOF_[n](2);
  }


  tmp = JT*Transpose(JT);

  stxyz = DLA::InverseLU::Solve(tmp, JT);

  for (int n = 0; n < 3; n++)
  {
    sgrad[n] = stxyz(0,n);
    tgrad[n] = stxyz(1,n);
  }
#endif

#if 0
  // Solving the system
  // [ xs, ys, zs ] [dx] = [ds]
  // [ xt, yt, zt ] [dy] = [dt]
  // [ nx, ny, nz ] [dz] = [0 ]
  // gives the gradient with a zero normal component

  DLA::MatrixS<3,3,Real> J = 0, Jinv;
  DLA::VectorS<3,Real> Xs(0), Xt(0), N;

  // Get the gradient with respect to the local coordinates
  for (int n = 0; n < nDOF_; n++)
  {
    Xs += phis[n]*DOF_[n];
    Xt += phit[n]*DOF_[n];
  }

  N = cross(Xs, Xt);

  // Construct the Jacobian
  for (int n = 0; n < 3; n++ )
  {
    J(0,n) = Xs[n];
    J(1,n) = Xt[n];
    J(2,n) = N[n];
  }

  // Solve
  Jinv = DLA::InverseLUP::Inverse(J);

  // Set the gradients
  for (int n = 0; n < 3; n++)
  {
    sgrad[n] = Jinv(n,0);
    tgrad[n] = Jinv(n,1);
  }
#endif
}


template <class PhysDim, class Topology>
inline void
ElementXField<PhysDim, TopoD2, Topology>::evalReferenceCoordinateHessian(
    const Real& s, const Real& t, TensorSymX& shess, TensorSymX& thess ) const
{
  SANS_DEVELOPER_EXCEPTION("ElementXField<PhysD3, TopoD2, Topology>::evalReferenceCoordinateHessian - Not implemented yet.");

  /*
  Real phis[nDOF_], phit[nDOF_];
  Real phiss[nDOF_], phist[nDOF_], phitt[nDOF_];
  Real xs, xt, ys, yt;
  Real xss, xst, xtt, yss, yst, ytt;
  Real det;

  basis_->evalBasisDerivative( s, t, edgeSign_, phis, phit, nDOF_ );
  basis_->evalBasisHessianDerivative( s, t, edgeSign_, phiss, phist, phitt, nDOF_ );

  xs = 0; xt = 0; ys = 0; yt = 0;
  xss = 0;  xst = 0; xtt = 0;
  yss = 0;  yst = 0; ytt = 0;
  for (int n = 0; n < nDOF_; n++)
  {
    // first derivatives
    xs +=phis[n]*DOF_[n](0); xt +=phit[n]*DOF_[n](0);
    ys +=phis[n]*DOF_[n](1); yt +=phit[n]*DOF_[n](0);

    // second derivatives
    xss += phiss[n]*DOF_[n](0); xst += phist[n]*DOF_[n](0); xtt += phitt[n]*DOF_[n](0);
    yss += phiss[n]*DOF_[n](1); yst += phist[n]*DOF_[n](1); ytt += phitt[n]*DOF_[n](1);
  }
  det = xs*yt - xt*ys;

  // Definition of Matrix in ElementXFieldArea.h
  Matrix invJacobian;
  TensorSymX xHess, yHess;
  TensorSymX xJHessJT, yJHessJT;

  // Construct the inverse Jacobian
  invJacobian[0]= yt;invJacobian[1]=-ys;
  invJacobian[2]=-xt;invJacobian[3]= xs;
  invJacobian *= (1/det);

  // Populate the two hessians of the cartesian coords
  xHess[0] = xss; xHess[1] = xst; xHess[2] = xtt;
  yHess[0] = yss; yHess[1] = yst; yHess[2] = ytt;

  // Pre multiply the 'rhs'
  xJHessJT = invJacobian*xHess*Transpose(invJacobian);
  yJHessJT = invJacobian*yHess*Transpose(invJacobian);

  // Calculate the transformed hessian
  shess = -yt*xJHessJT + xt*yJHessJT;
  thess = -xs*xJHessJT + ys*yJHessJT;
  */

}

//----------------------------------------------------------------------------//
// basis hessian
// Returns the hessian of basis functions in cartesian coords, transforms from the master element
template <class PhysDim, class Topology>
template <class RefCoord>
void
ElementXField<PhysDim, TopoD2, Topology>::evalBasisHessian( const RefCoord& sRef,
                                                            const ElementBasis<TopoD2, Topology>& fldElem,
                                                            TensorSymX hessphi[], int nphi ) const
{
   SANS_DEVELOPER_EXCEPTION("ElementXField<PhysD3, TopoD2, Topology>::evalBasisHessian - Not implemented yet.");
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD2, Topology>::unitNormal(
    const RefCoordType& sRef, VectorX& N ) const
{
  evalBasisDerivative( sRef[0], sRef[1], phis_, phit_, nDOF_ );

  unitNormal(phis_, phit_, N);
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD2, Topology>::unitNormal(
    const QuadraturePoint<TopoD2>& ref, VectorX& N ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    unitNormal(ref.ref, N);
    return;
  }

  const QuadratureBasisPointValues<TopoD>& cache = QuadratureCacheValues<TopoD2, Topology>::get( pointStoreCell_, ref, edgeSign_ );
  const std::vector<Real>& phis = cache.dphi[0];
  const std::vector<Real>& phit = cache.dphi[1];

  unitNormal(phis.data(), phit.data(), N);
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD2, Topology>::unitNormal(
    const QuadratureCellTracePoint<TopoD2>& ref, VectorX& N ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    unitNormal(ref.ref, N);
    return;
  }

  const QuadratureBasisPointValues<TopoD>& cache = QuadratureCacheValues<TopoD2, Topology>::get(pointStoreTrace_, ref, edgeSign_);
  const std::vector<Real>& phis = cache.dphi[0];
  const std::vector<Real>& phit = cache.dphi[1];

  unitNormal(phis.data(), phit.data(), N);
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD2, Topology>::unitNormal(
    const Real phis[], const Real phit[], VectorX& N ) const
{
  Real xs, xt, ys, yt, zs, zt;

  xs = 0;  xt = 0;
  ys = 0;  yt = 0;
  zs = 0;  zt = 0;
  for (int n = 0; n < nDOF_; n++)
  {
    xs += phis[n]*DOF_[n](0);  xt += phit[n]*DOF_[n](0);
    ys += phis[n]*DOF_[n](1);  yt += phit[n]*DOF_[n](1);
    zs += phis[n]*DOF_[n](2);  zt += phit[n]*DOF_[n](2);
  }

  N[0] = ys*zt-zs*yt;
  N[1] = zs*xt-xs*zt;
  N[2] = xs*yt-xt*ys;

  N /= sqrt( dot(N,N) );
}

template class ElementXField<PhysD3, TopoD2, Triangle>;
template class ElementXField<PhysD3, TopoD2, Quad>;

}
