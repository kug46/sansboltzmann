// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ELEMENTPROJECTION_L2_H
#define ELEMENTPROJECTION_L2_H

// Element solution projection: L2

#include <ostream>
#include <vector>
#include <typeinfo>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "BasisFunction/BasisFunctionBase.h"
#include "Quadrature/Quadrature.h"

#include "Field/Element/Element.h"

#include "LinearAlgebra/DenseLinAlg/InverseLU.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "BasisFunction/BasisFunction_RefElement_Split.h"
#include "BasisFunction/BasisFunction_RefElement_Split_TraceToCell.h"
#include "BasisFunction/BasisFunction_TraceOrientation.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Base class for elemental projections. Computes and stores the reference element mass matrix
//   projection formulation: project f(var) onto q in L_2 norm
//   \int \phi q d\xi = \int \phi f(var) d\xi
//   [[M]] [q] = [f]  -->  [q] = [[M]]^{-1} [f]

template<class TopoDim, class Topology>
class ElementProjectionBase
{
public:
  typedef typename BasisFunctionTopologyBase<TopoDim, Topology>::type BasisType;
  typedef typename Element<Real, TopoDim, Topology>::RefCoordType RefCoordType;

  ElementProjectionBase(const ElementProjectionBase&) = delete;
  ElementProjectionBase& operator=(const ElementProjectionBase&) = delete;

  ElementProjectionBase(const BasisType* basis, const int quadOrder, bool constant = false) :
    basis_(basis),
    nDOF_(basis_->nBasis()),
    quadrature_(quadOrder),
    MassMatrix_(nDOF_, nDOF_),
    invMassMatrix_(nDOF_, nDOF_)
  {
    const int nquad = quadrature_.nQuadrature();

    phi_.resize(nquad, std::vector<Real>(nDOF_));

    Element<Real, TopoDim, Topology> qElem(basis);

    // Cubic or higher Hierarchical basis must recompute mass matrix, unless projecting a constant value
    solveMassMatrix_ = (basis_->category() == BasisFunctionCategory_Hierarchical && basis_->order() > 2) && !constant;

    if (solveMassMatrix_)
      invMassMatrix_ = 0;
    else
    {
      // compute the mass matrix
      computeMassMatrix(qElem);

      // store the matrix inverse
      invMassMatrix_ = DLA::InverseLU::Inverse(MassMatrix_);
    }
  }

  template<class T>
  void computeMassMatrix(const Element<T, TopoDim, Topology>& qElem)
  {
    const int nquad = quadrature_.nQuadrature();
    typedef QuadraturePoint<TopoDim> QuadPointType;

    Real weight;

    MassMatrix_ = 0;

    // loop over quadrature points and compute the scalar mass matrix
    for (int iquad = 0; iquad < nquad; iquad++)
    {
      quadrature_.weight( iquad, weight );
      QuadPointType sRef = quadrature_.coordinates_cache( iquad );

      std::vector<Real>& phi = phi_[iquad];

      // save off the basis functions at the quadrature points
      qElem.evalBasis( sRef, phi.data(), nDOF_ );

      for (int i = 0; i < nDOF_; i++)
        for (int j = 0; j < nDOF_; j++)
          MassMatrix_(i,j) += weight*phi[i]*phi[j];

      // Note that this MassMatrix does not include geometry jacobian (aka J = D(r)/D(sRef)).
      // This assumption is valid on condition that the projection (specifically, the evaluation
      // of left and right integrals) is carried on the same grid element.  In such a case,
      // the jacobian J can be dropped out in both sides.
    }
  }

  template<class T>
  void solve(const DLA::VectorD<T>& rhs, Element<T, TopoDim, Topology>& qElem)
  {
    DLA::VectorD<T> sln( nDOF_ );

    if (solveMassMatrix_)
    {
      // Mass matrix must be recomputed due to trace orientations
      sln = DLA::InverseLU::Solve(MassMatrix_, rhs);
    }
    else
    {
      // Inverse mass matrix is stored
      sln = invMassMatrix_*rhs;
    }

    // save the solution
    for (int i = 0; i < nDOF_; i++)
      qElem.DOF(i) = sln[i];
  }

protected:
  const BasisType* basis_;
  const int nDOF_;
  bool solveMassMatrix_;
  Quadrature<TopoDim, Topology> quadrature_;
  std::vector<std::vector<Real>> phi_; //basis function evaluations at each quadrature point (index: [quad][basis])

  DLA::MatrixD<Real> MassMatrix_;    //mass-matrix on reference element
  DLA::MatrixD<Real> invMassMatrix_; //mass-matrix inverse on reference element
};

//----------------------------------------------------------------------------//
// Element constant projection: L2
//
//  min  integral[ (q0 - q(s,t))^2 ],  q(s,t) = sum q_k phi_k(s,t)
//
// For projecting a constant q0 onto the basis

template<class TopoDim, class Topology>
class ElementProjectionConst_L2 : public ElementProjectionBase<TopoDim, Topology>
{
public:
  typedef ElementProjectionBase<TopoDim, Topology> BaseType;
  typedef typename BaseType::BasisType BasisType;
  typedef typename BaseType::RefCoordType RefCoordType;

  // use quadrature needed to integrate mass matrix exactly
  explicit ElementProjectionConst_L2(const BasisType* basis) : BaseType(basis, 2*basis->order(), true) {}

  template <class T>
  void
  project( Element<T, TopoDim, Topology>& qfldElem,
           const T& q0 )
  {
    SANS_ASSERT( qfldElem.basis() == basis_ );
    const int nDOF = nDOF_;

    Real weight;                // quadrature weight

    DLA::VectorD<T> rhs( nDOF );

    rhs = 0;

    // the mass matrix does not need to be recomputed for a constant value

    // loop over quadrature points
    const int nquad = quadrature_.nQuadrature();
    for (int iquad = 0; iquad < nquad; iquad++)
    {
      quadrature_.weight( iquad, weight );

      const std::vector<Real>& phi = phi_[iquad];

      for (int i = 0; i < nDOF; i++)
        rhs[i] += weight*phi[i]*q0;
    }

    // solve the mass matrix system for the projection
    this->solve(rhs, qfldElem);
  }

protected:
  using BaseType::basis_;
  using BaseType::nDOF_;
  using BaseType::quadrature_;
  using BaseType::phi_; //basis function evaluations at each quadrature point (index: [quad][basis])

  using BaseType::solveMassMatrix_; // does the mass matrix need to be recomputed?
};

//  specialization for a node
template<>
class ElementProjectionConst_L2<TopoD0, Node>
{
public:
  typedef typename BasisFunctionTopologyBase<TopoD0, Node>::type BasisType;

  explicit ElementProjectionConst_L2(const BasisType* basis)  {}

  template <class T>
  void
  project( Element<T, TopoD0, Node>& qfldElem,
                   const T& q0 )
  {
    qfldElem.DOF(0) = q0;
  }
};

//----------------------------------------------------------------------------//
// Element solution projection: L2
//
//  min  integral[ (qexact(x,y) - q(x,y))^2 ],  q(x,y) = sum q_k phi_k(x,y)
//
// For projecting an exact solution not represented with a basis

template<class TopoDim, class Topology>
class ElementProjectionSolution_L2 : public ElementProjectionBase<TopoDim, Topology>
{
public:
  typedef ElementProjectionBase<TopoDim, Topology> BaseType;
  typedef typename BaseType::BasisType BasisType;

  // use max quadrature to integrate the solution as well as possible
  explicit ElementProjectionSolution_L2(const BasisType* basis) : BaseType(basis, -2) {}

  template <class PhysDim, class T, class SolutionFunctor>
  void
  project( const ElementXField<PhysDim, TopoDim, Topology>& xfldElem,
           Element<T, TopoDim, Topology>& qfldElem,
           const SolutionFunctor& solnExact )
  {
    typedef typename ElementXField<PhysDim, TopoDim, Topology>::VectorX VectorX;
    typedef QuadraturePoint<TopoDim> QuadPointType;

    SANS_ASSERT( qfldElem.basis() == basis_ );
    const int nDOF = nDOF_;

    Real weight;                // quadrature weight
    VectorX X;                  // physical coordinates

    T qexact;                   // exact solution

    DLA::VectorD<T> rhs( nDOF );
    rhs = 0;

    // Mass matrix must be recomputed due to trace orientations
    // and phi_ is updated in the process
    if (solveMassMatrix_)
      this->computeMassMatrix(qfldElem);

    // loop over quadrature points
    const int nquad = quadrature_.nQuadrature();
    for (int iquad = 0; iquad < nquad; iquad++)
    {
      quadrature_.weight( iquad, weight );
      QuadPointType sRef = quadrature_.coordinates_cache( iquad );

      // physical coordinates
      xfldElem.coordinates( sRef, X );

      // Evaluate the solution function at the physical coordinate
      qexact = solnExact( X );

      const std::vector<Real>& phi = phi_[iquad];

      for (int i = 0; i < nDOF; i++)
        rhs[i] += weight*phi[i]*qexact;
    }

    // solve the mass matrix system for the projection
    this->solve(rhs, qfldElem);
  }

protected:
  using BaseType::basis_;
  using BaseType::nDOF_;
  using BaseType::quadrature_;
  using BaseType::phi_; //basis function evaluations at each quadrature point (index: [quad][basis])

  using BaseType::solveMassMatrix_; // does the mass matrix need to be recomputed?
};


//----------------------------------------------------------------------------//
// Element solution projection: L2 of the gradient
//
//  min  integral[ (qexact(x,y) - q(x,y))^2 ],  q(x,y) = sum q_k phi_k(x,y)
//
// For projecting the gradient of a scalar function onto a vector field
// where scalar function is not represented with a basis.

template<class TopoDim, class Topology>
class ElementProjectionSolutionGradient_L2 : public ElementProjectionBase<TopoDim, Topology>
{
public:
  typedef ElementProjectionBase<TopoDim, Topology> BaseType;
  typedef typename BaseType::BasisType BasisType;

  // use max quadrature to integrate the solution as well as possible
  explicit ElementProjectionSolutionGradient_L2(const BasisType* basis) : BaseType(basis, -2) {}

  template <class PhysDim, class T, class SolutionFunctor>
  void
  project( const ElementXField<PhysDim, TopoDim, Topology>& xfldElem,
           Element<DLA::VectorS<PhysDim::D,T>, TopoDim, Topology>& qfldElem,
           const SolutionFunctor& solnExact )
  {
    typedef typename ElementXField<PhysDim, TopoDim, Topology>::VectorX VectorX;
    typedef DLA::VectorS<PhysDim::D,T> VectorT;
    typedef QuadraturePoint<TopoDim> QuadPointType;

    SANS_ASSERT( qfldElem.basis() == basis_ );
    const int nDOF = nDOF_;

    Real weight;                // quadrature weight
    VectorX X;                  // physical coordinates

    T qexact;                   // exact solution
    VectorT gradqexact;         // exact solution gradient

    DLA::VectorD<VectorT> rhs( nDOF );

    rhs = 0;

    // Mass matrix must be recomputed due to trace orientations
    // and phi_ is updated in the process
    if (solveMassMatrix_)
      this->computeMassMatrix(qfldElem);

    // loop over quadrature points
    const int nquad = quadrature_.nQuadrature();
    for (int iquad = 0; iquad < nquad; iquad++)
    {
      quadrature_.weight( iquad, weight );
      QuadPointType sRef = quadrature_.coordinates_cache( iquad );

      // physical coordinates
      xfldElem.coordinates( sRef, X );

      // Evaluate the solution function and gradient at the physical coordinate
      solnExact.gradient( X, qexact, gradqexact );

      const std::vector<Real>& phi = phi_[iquad];

      for (int i = 0; i < nDOF; i++)
        rhs[i] += weight*phi[i]*gradqexact;
    }

    // solve the mass matrix system for the projection
    this->solve(rhs, qfldElem);
  }

protected:
  using BaseType::basis_;
  using BaseType::nDOF_;
  using BaseType::quadrature_;
  using BaseType::phi_; //basis function evaluations at each quadrature point (index: [quad][basis])

  using BaseType::solveMassMatrix_; // does the mass matrix need to be recomputed?
};


//----------------------------------------------------------------------------//
// boundary trace-element solution projection: L2
//
//  min  integral[ (qexact(x,y) - q(x,y))^2 ],  q(x,y) = sum q_k phi_k(x,y)
//
// NOTE: temporary; interface designed for Lagrange multiplier (uses 2nd version
// of SolutionFunction2D::operator())

template <class PhysDim, class TopoDim, class Topology, class ArrayQ, class SolutionFunctor>
void
ElementProjection_NormalGradient_L2(
    const ElementXField<PhysDim, TopoDim, Topology>& xfldElem,
          Element<ArrayQ, TopoDim, Topology>& qfldElem,
    const SolutionFunctor& solnExact )
{
  typedef typename ElementXField<PhysDim, TopoDim, Topology>::VectorX VectorX;
  //typedef typename Element<ArrayQ, TopoDim, Topology>::RefCoordType RefCoordType;

  const int nDOF = qfldElem.nDOF();                     // total solution DOFs in element
  SANS_ASSERT(nDOF == qfldElem.nDOF());
  SANS_ASSERT(nDOF > 0);      // Suppress clang analyzer warning
  typedef QuadraturePoint<TopoDim> QuadPointType;

  Real dJ;                    // incremental Jacobian determinant
  Real weight;                // quadrature weight
  //RefCoordType Ref;           // reference-line coordinate (s)
  VectorX X;                   // physical coordinates
  VectorX N;                   // unit normal (points out of domain)

  DLA::VectorD<Real> phi( nDOF );               // basis
  DLA::VectorS<PhysDim::D, ArrayQ> gradQExact;  // exact solution gradient
  ArrayQ qExact;
  ArrayQ qtmp;

  SANS::DLA::MatrixD<Real> mtx( nDOF, nDOF );
  SANS::DLA::VectorD<ArrayQ> rhs( nDOF );
  SANS::DLA::VectorD<ArrayQ> sln( nDOF );

  mtx = 0;
  rhs = 0;

  // use max quadrature rule
  Quadrature<TopoDim, Topology> quadrature( -2 );

  // loop over quadrature points
  const int nquad = quadrature.nQuadrature();
  for (int iquad = 0; iquad < nquad; iquad++)
  {
    quadrature.weight( iquad, weight );
    QuadPointType Ref = quadrature.coordinates_cache( iquad );

    dJ = weight * xfldElem.jacobianDeterminant( Ref );

    // physical coordinates
    xfldElem.coordinates( Ref, X );

    // physical coordinates
    xfldElem.unitNormal( Ref, N );

    // basis
    qfldElem.evalBasis( Ref, phi.data(), phi.size() );

    solnExact.gradient( X, qtmp, gradQExact );
    qExact = dot(N, gradQExact);

    for (int i = 0; i < nDOF; i++)
    {
      for (int j = 0; j < nDOF; j++)
        mtx(i,j) += dJ*phi[i]*phi[j];

       rhs[i] += dJ*phi[i]*qExact;
    }
  }

  sln = SANS::DLA::InverseLU::Solve( mtx, rhs );

  for (int i = 0; i < nDOF; i++)
    qfldElem.DOF(i) = sln[i];
}


//---------------------------------------------------------------------------------------//
// Element_Projector_L2:
// Performs an L2 projection of the field data in the main element into the same element on different Basis
// Must be done element-wise in order to account for hierarchical basis, and can only match or increase order
// so as to be exact with CG Fields
template<class TopoDim, class Topology>
class Element_Projector_L2 : public ElementProjectionBase<TopoDim, Topology>
{
public:
  typedef ElementProjectionBase<TopoDim, Topology> BaseType;
  typedef typename BasisFunctionTopologyBase<TopoDim, Topology>::type BasisType;
  typedef typename Element<Real, TopoDim, Topology>::RefCoordType RefCoordType;

  explicit Element_Projector_L2(const BasisType* basis) : BaseType(basis, 2*basis->order()) {}

  ~Element_Projector_L2() {}

  Element_Projector_L2(const Element_Projector_L2&) = delete;
  Element_Projector_L2& operator=(const Element_Projector_L2&) = delete;

  template <class T>
  void project(const Element<T, TopoDim, Topology>& fromElem,
               Element<T, TopoDim, Topology>& toElem )
  {
    // Doing element projection assumes the order of the projected to space is greater than or equal to
    // thus exact order can be recovered
    // For a CG field, element projection is not correct if the order decreases
    // a p2 solution is exactly recovered in a p3 field
    SANS_ASSERT( fromElem.basis()->order() <= toElem.basis()->order() );

    if (!solveMassMatrix_ &&
        (fromElem.basis()->category() == toElem.basis()->category()) )
    {
      // Simple projection where DOF's are copied
      fromElem.projectTo(toElem);
      return;
    }

    // Check that the inverse mass matrix was made for the correct basis
    SANS_ASSERT( basis_ == toElem.basis() );

    Real weight;                // quadrature weight
    typedef QuadraturePoint<TopoDim> QuadPointType;

    T eval;               // coordinate or solution evaluated on the 'from' element

    DLA::VectorD<T> rhs( nDOF_ );
    rhs = 0;

    // Mass matrix must be recomputed due to trace orientations
    // and phi_ is updated in the process
    if (solveMassMatrix_)
      this->computeMassMatrix(toElem);

    // loop over quadrature points
    const int nquad = quadrature_.nQuadrature();
    for (int iquad = 0; iquad < nquad; iquad++)
    {
      quadrature_.weight( iquad, weight );
      QuadPointType Ref_coord = quadrature_.coordinates_cache( iquad );

      fromElem.eval(Ref_coord, eval);

      const std::vector<Real>& phi = phi_[iquad];

      for (int i = 0; i < nDOF_; i++)
         rhs[i] += weight*phi[i]*eval;
    }

    // solve the mass matrix system for the projection
    this->solve(rhs, toElem);
  }

protected:
  using BaseType::basis_;
  using BaseType::nDOF_;
  using BaseType::quadrature_;
  using BaseType::phi_; //basis function evaluations at each quadrature point (index: [quad][basis])

  using BaseType::solveMassMatrix_; // does the mass matrix need to be recomputed?
};


//---------------------------------------------------------------------------------------//
// Element_Subdivision_Projector:
// Performs an L2 projection of the field data in the main element into the sub-element
template<class TopoDim, class Topology>
class Element_Subdivision_Projector : public ElementProjectionBase<TopoDim, Topology>
{
public:
  typedef ElementProjectionBase<TopoDim, Topology> BaseType;
  typedef typename BaseType::BasisType BasisType;
  typedef typename Element<Real, TopoDim, Topology>::RefCoordType RefCoordType;

  explicit Element_Subdivision_Projector(const BasisType* basis) : BaseType(basis, 2*basis->order()) {}

  template <class T>
  void project(const Element<T, TopoDim, Topology>& mainElem,
               Element<T, TopoDim, Topology>& subElem,
               const ElementSplitType split_type, const int split_edge_index, const int sub_cell_index)
  {
    //Check if the basis type of the element to be filled and the type of basis_ are the same
    SANS_ASSERT( basis_ == subElem.basis() );
    typedef QuadraturePoint<TopoDim> QuadPointType;

    // Doing element projection assumes the order is the same or greater
    // For a CG field, element projection is not correct if the order changes
    SANS_ASSERT( subElem.basis()->order() >= mainElem.basis()->order() );

    // incremental Jacobian determinant is not needed as it cancels between the two integrals
    Real weight;                // quadrature weight
    RefCoordType Ref_main;      // reference-element coordinates on main-element

    T Teval_main;               // coordinate or solution evaluated from main element

    DLA::VectorD<T> rhs( nDOF_ );

    rhs = 0;

    // Mass matrix must be recomputed due to trace orientations
    // and phi_ is updated in the process
    if (solveMassMatrix_)
      this->computeMassMatrix(subElem);

    // loop over quadrature points
    const int nquad = quadrature_.nQuadrature();
    for (int iquad = 0; iquad < nquad; iquad++)
    {
      quadrature_.weight( iquad, weight );
      QuadPointType Ref_sub = quadrature_.coordinates_cache( iquad );

      //Transform reference coordinates on sub-element to main-element
      BasisFunction_RefElement_Split<TopoDim, Topology>::transform(Ref_sub.ref, split_type, split_edge_index, sub_cell_index, Ref_main);
      mainElem.eval(Ref_main, Teval_main);

      const std::vector<Real>& phi = phi_[iquad];

      for (int i = 0; i < nDOF_; i++)
         rhs[i] += weight*phi[i]*Teval_main;
    }

    // solve the mass matrix system for the projection
    this->solve(rhs, subElem);
  }

protected:
  using BaseType::basis_;
  using BaseType::nDOF_;
  using BaseType::quadrature_;
  using BaseType::phi_; //basis function evaluations at each quadrature point (index: [quad][basis])

  using BaseType::solveMassMatrix_; // does the mass matrix need to be recomputed?
};


//---------------------------------------------------------------------------------------//
// Element_Subdivision_CellToTrace_Projector:
// Performs an L2 projection of the field data in the cell field to the trace field
template<class TopoDimTrace, class TopoTrace>
class Element_Subdivision_CellToTrace_Projector : public ElementProjectionBase<TopoDimTrace, TopoTrace>
{
public:
  typedef ElementProjectionBase<TopoDimTrace, TopoTrace> BaseType;
  typedef typename BasisFunctionTopologyBase<TopoDimTrace, TopoTrace>::type BasisType;
  typedef typename Element<Real, TopoDimTrace, TopoTrace>::RefCoordType RefCoordType;

  explicit Element_Subdivision_CellToTrace_Projector(const BasisType* basis) : BaseType(basis, 2*basis->order()) {}

  Element_Subdivision_CellToTrace_Projector(const Element_Subdivision_CellToTrace_Projector&) = delete;
  Element_Subdivision_CellToTrace_Projector& operator=(const Element_Subdivision_CellToTrace_Projector&) = delete;

  template <class TopoDim, class TopoCell, class T>
  void project(const Element<T, TopoDim, TopoCell>& cellElem,
               Element<T, TopoDimTrace, TopoTrace>& traceElem,
               const ElementSplitType split_type, const int split_edge_index, const int sub_trace_index)
  {
    typedef typename Element<Real, TopoDim, TopoCell>::RefCoordType RefCoordTypeCell;
    typedef QuadraturePoint<TopoDimTrace> QuadPointType;

    // Check if the basis type of the element to be filled and the type of basis_ are the same
    SANS_ASSERT( basis_ == traceElem.basis() );

    // Doing trace element projection assumes the order is the same
    // For a CG field, element projection is not correct if the order changes
    if (TopoDimTrace::D > 0)
      SANS_ASSERT( traceElem.basis()->order() >= cellElem.basis()->order() );

    Real weight;                // quadrature weight
    RefCoordTypeCell Ref_cell;  // reference-element coordinates on cell element

    T Teval_cell;               // coordinate or solution evaluated from cell element

    DLA::VectorD<T> rhs( nDOF_ );
    rhs = 0;

    // Mass matrix must be recomputed due to trace orientations
    // and phi_ is updated in the process
    if (solveMassMatrix_)
      this->computeMassMatrix(traceElem);

    // loop over quadrature points
    const int nquad = quadrature_.nQuadrature();
    for (int iquad = 0; iquad < nquad; iquad++)
    {
      quadrature_.weight( iquad, weight );
      QuadPointType Ref_trace = quadrature_.coordinates_cache( iquad );

      //Transform reference coordinates on trace-element to unsplit cell-element
      BasisFunction_RefElement_Split_TraceToCell<TopoDim, TopoCell>::transform(Ref_trace.ref, split_type, split_edge_index,
                                                                               sub_trace_index, Ref_cell);
      cellElem.eval(Ref_cell, Teval_cell);

      const std::vector<Real>& phi = phi_[iquad];

      for (int i = 0; i < nDOF_; i++)
         rhs[i] += weight*phi[i]*Teval_cell;
    }

    // solve the mass matrix system for the projection
    this->solve(rhs, traceElem);
  }

protected:
  using BaseType::basis_;
  using BaseType::nDOF_;
  using BaseType::quadrature_;
  using BaseType::phi_; //basis function evaluations at each quadrature point (index: [quad][basis])

  using BaseType::solveMassMatrix_; // does the mass matrix need to be recomputed?
};

//---------------------------------------------------------------------------------------//
// Element Trace_Projection:
// Performs an L2 projection of the field data in one trace element into another trace element,
// but taking into account the orientations of the two traces

template <class T, class TopoDim, class Topology>
void
Element_Trace_Projection_L2(
    const Element<T, TopoDim, Topology>& elem0, const int orientation0,
          Element<T, TopoDim, Topology>& elem1, const int orientation1)
{
  typedef typename Element<T, TopoDim, Topology>::RefCoordType RefCoordType;

  const int nDOF = elem1.nDOF();   // total solution DOFs in element
  SANS_ASSERT( nDOF > 0 );         // Suppress clang analyzer warning

  // Doing element projection assumes the order is the same.
  // For a CG field, element projection is not correct if the order changes
  SANS_ASSERT( elem0.basis()->order() == elem1.basis()->order() );

  // incremental Jacobian determinant is not needed as it cancels between the two integrals
  Real weight;                  // quadrature weight
  //RefCoordType Ref;             // reference-element coordinates on sub-element
  typedef QuadraturePoint<TopoDim> QuadPointType;
  RefCoordType Ref_elem0;       // reference-element coordinates on elem0
  RefCoordType Ref_elem1;       // reference-element coordinates on elem1

  DLA::VectorD<Real> phi(nDOF); // basis
  T Teval_elem0;                // coordinate or solution evaluated from elem0

  DLA::MatrixD<Real> mtx( nDOF, nDOF );
  DLA::VectorD<T> rhs( nDOF );
  DLA::VectorD<T> sln( nDOF );

  mtx = 0;
  rhs = 0;

  // use quadrature rule that matches the polynomial order: mass-matrix requires product of two polynomials
  Quadrature<TopoDim, Topology> quadrature( 2*elem0.basis()->order() );

  // loop over quadrature points
  const int nquad = quadrature.nQuadrature();
  for (int iquad = 0; iquad < nquad; iquad++)
  {
    quadrature.weight( iquad, weight );
    QuadPointType Ref = quadrature.coordinates_cache( iquad );

    //transform reference coordinates based on trace orientations
    BasisFunction_TraceOrientation<TopoDim, Topology>::transform(Ref.ref, orientation0, Ref_elem0);
    BasisFunction_TraceOrientation<TopoDim, Topology>::transform(Ref.ref, orientation1, Ref_elem1);

    // basis value
    elem1.evalBasis(Ref_elem1, phi.data(), phi.size());

    elem0.eval(Ref_elem0, Teval_elem0);

    for (int i = 0; i < nDOF; i++)
    {
      for (int j = 0; j < nDOF; j++)
        mtx(i,j) += weight*phi[i]*phi[j];

       rhs[i] += weight*phi[i]*Teval_elem0;
    }
  }

  sln = SANS::DLA::InverseLU::Solve( mtx, rhs);

  for (int i = 0; i < nDOF; i++)
    elem1.DOF(i) = sln[i];

//  std::cout << "sln = ";  dump( sln, 2 );
}

}

#endif  // ELEMENTPROJECTION2D_L2_H
