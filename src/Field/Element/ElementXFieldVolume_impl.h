// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(ELEMENTXFIELDVOLUME_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include <iomanip> // std::setprecision

#include "ElementXFieldVolume.h"
#include "ReferenceElementMesh.h"

#include "Field/output_Tecplot/TecTopo.h"

namespace SANS
{


//---------------------------------------------------------------------------//
template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD3, Topology>::evalReferenceCoordinateGradient(
    const Real& s, const Real& t, const Real& u, VectorX& sgrad, VectorX& tgrad, VectorX& ugrad ) const
{
  evalBasisDerivative( s, t, u, phis_, phit_, phiu_, nDOF_ );

  evalReferenceCoordinateGradient( phis_, phit_, phiu_, sgrad, tgrad, ugrad );
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD3, Topology>::evalReferenceCoordinateGradient(
    const RefCoordType& sRef, VectorX& sgrad, VectorX& tgrad, VectorX& ugrad ) const
{
  evalBasisDerivative( sRef[0], sRef[1], sRef[2], phis_, phit_, phiu_, nDOF_ );

  evalReferenceCoordinateGradient( phis_, phit_, phiu_, sgrad, tgrad, ugrad );
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD3, Topology>::evalReferenceCoordinateGradient(
    const QuadraturePoint<TopoD3>& ref, VectorX& sgrad, VectorX& tgrad, VectorX& ugrad ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalReferenceCoordinateGradient(ref.ref[0], ref.ref[1], ref.ref[2], sgrad, tgrad, ugrad);
    return;
  }

  const QuadratureBasisPointValues<TopoD>& cache = QuadratureCacheValues<TopoD3, Topology>::get( pointStoreCell_, ref, faceSign_ );
  const std::vector<Real>& phis = cache.dphi[0];
  const std::vector<Real>& phit = cache.dphi[1];
  const std::vector<Real>& phiu = cache.dphi[2];

  evalReferenceCoordinateGradient( phis.data(), phit.data(), phiu.data(), sgrad, tgrad, ugrad );
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD3, Topology>::evalReferenceCoordinateGradient(
    const QuadratureCellTracePoint<TopoD3>& ref, VectorX& sgrad, VectorX& tgrad, VectorX& ugrad ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalReferenceCoordinateGradient(ref.ref[0], ref.ref[1], ref.ref[2], sgrad, tgrad, ugrad);
    return;
  }

  const QuadratureBasisPointValues<TopoD>& cache = QuadratureCacheValues<TopoD3, Topology>::get(pointStoreTrace_, ref, faceSign_);
  const std::vector<Real>& phis = cache.dphi[0];
  const std::vector<Real>& phit = cache.dphi[1];
  const std::vector<Real>& phiu = cache.dphi[2];

  evalReferenceCoordinateGradient( phis.data(), phit.data(), phiu.data(), sgrad, tgrad, ugrad );
}

//---------------------------------------------------------------------------//
template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD3, Topology>::jacobian( Matrix& J ) const
{
  Real s = Topology::centerRef[0];
  Real t = Topology::centerRef[1];
  Real u = Topology::centerRef[2];
  jacobian( s, t, u, J );
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD3, Topology>::jacobian( const Real& s, const Real& t, const Real& u, Matrix& J ) const
{
  evalBasisDerivative( s, t, u, phis_, phit_, phiu_, nDOF_ );

  jacobian(phis_, phit_, phiu_, J);
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD3, Topology>::jacobian( const QuadraturePoint<TopoD3>& ref, Matrix& J ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    jacobian(ref.ref[0], ref.ref[1], ref.ref[2], J);
    return;
  }

  const QuadratureBasisPointValues<TopoD>& cache = QuadratureCacheValues<TopoD3, Topology>::get( pointStoreCell_, ref, faceSign_ );
  const std::vector<Real>& phis = cache.dphi[0];
  const std::vector<Real>& phit = cache.dphi[1];
  const std::vector<Real>& phiu = cache.dphi[2];

  jacobian(phis.data(), phit.data(), phiu.data(), J);
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD3, Topology>::jacobian(
    const Real* __restrict phis, const Real* __restrict phit, const Real* __restrict phiu, Matrix& J ) const
{
  J = 0;
  for (int n = 0; n < nDOF_; n++)
    for (int d = 0; d < D; d++)
    {
      J(d,0) += phis[n]*DOF_[n](d); //d(x_i)/ds
      J(d,1) += phit[n]*DOF_[n](d); //d(x_i)/dt
      J(d,2) += phiu[n]*DOF_[n](d); //d(x_i)/du
    }
}

//---------------------------------------------------------------------------//
template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD3, Topology>::hessian( const Real& s, const Real& t, const Real& u,
                                                   TensorSymHessian& H ) const
{
  SANS_ASSERT(nDOF_ > 0); //To please the clang compiler

  // second derivative of basis wrt (s,t,u)
  basis_->evalBasisHessianDerivative( s, t, u, faceSign_,
                                     phiss_, phist_, phitt_, phisu_, phitu_, phiuu_, nDOF_ );

  H = 0;
  for (int n = 0; n < nDOF_; n++)
    for (int d = 0; d < D; d++)
    {
      H(d)(0,0) += phiss_[n]*DOF_[n](d); //d^2(x_i)/d(s^2)
      H(d)(1,0) += phist_[n]*DOF_[n](d); //d^2(x_i)/d(st)
      H(d)(1,1) += phitt_[n]*DOF_[n](d); //d^2(x_i)/d(t^2)
      H(d)(2,0) += phisu_[n]*DOF_[n](d); //d^2(x_i)/d(su)
      H(d)(2,1) += phitu_[n]*DOF_[n](d); //d^2(x_i)/d(tu)
      H(d)(2,2) += phiuu_[n]*DOF_[n](d); //d^2(x_i)/d(u^2)
    }
}

//----------------------------------------------------------------------------//
// basis gradient
template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD3, Topology>::evalBasisGradient( const RefCoordType& sRef,
                                                             const ElementBasis<TopoD3, Topology>& fldElem,
                                                             VectorX gradphi[], const int nphi ) const
{
  evalBasisGradient<RefCoordType>(sRef, fldElem, gradphi, nphi);
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD3, Topology>::evalBasisGradient( const QuadraturePoint<TopoD3>& sRef,
                                                             const ElementBasis<TopoD3, Topology>& fldElem,
                                                             VectorX gradphi[], const int nphi ) const
{
  evalBasisGradient<QuadraturePoint<TopoD3>>(sRef, fldElem, gradphi, nphi);
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD3, Topology>::evalBasisGradient( const QuadratureCellTracePoint<TopoD3>& sRef,
                                                             const ElementBasis<TopoD3, Topology>& fldElem,
                                                             VectorX gradphi[], int nphi ) const
{
  evalBasisGradient<QuadratureCellTracePoint<TopoD3>>(sRef, fldElem, gradphi, nphi);
}

template <class PhysDim, class Topology>
template <class RefCoord>
void
ElementXField<PhysDim, TopoD3, Topology>::evalBasisGradient( const RefCoord& sRef,
                                                             const ElementBasis<TopoD3, Topology>& fldElem,
                                                             VectorX gradphi[], int nphi ) const
{
  const int nBasis = fldElem.nDOF();   // total solution DOFs in element
  SANS_ASSERT(nphi == nBasis);

  VectorX sgrad, tgrad, ugrad;     // gradients of reference coordinates (s,t,u)
  const Real *phis, *phit, *phiu;  // derivatives of solution basis wrt (s,t,u)

  evalReferenceCoordinateGradient( sRef, sgrad, tgrad, ugrad );
  fldElem.evalBasisDerivative( sRef, &phis, &phit, &phiu );

  for (int n = 0; n < nphi; n++)
    gradphi[n] = phis[n]*sgrad + phit[n]*tgrad + phiu[n]*ugrad;
}

//----------------------------------------------------------------------------//
// basis hessian

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD3, Topology>::evalBasisHessian( const RefCoordType& sRef,
                                                            const ElementBasis<TopoD3, Topology>& fldElem,
                                                            TensorSymX hessphi[], int nphi ) const
{
  evalBasisHessian<RefCoordType>(sRef, fldElem, hessphi, nphi);
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD3, Topology>::evalBasisHessian( const QuadraturePoint<TopoD3>& sRef,
                                                            const ElementBasis<TopoD3, Topology>& fldElem,
                                                            TensorSymX hessphi[], int nphi ) const
{
  evalBasisHessian<QuadraturePoint<TopoD3>>(sRef, fldElem, hessphi, nphi);
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD3, Topology>::dumpTecplot( std::ostream& out ) const
{

  out << std::endl;
  out << "VARIABLES = \"X\", \"Y\", \"Z\"" << std::endl;

  for (int dump = 0; dump < 3; dump++)
  {
    int nRefine = 0;

    if (this->order_ == 1 || dump == 0)
      nRefine = 0;
    else if (dump == 1)
      nRefine = std::max(this->order_-1, 0);
    else
      nRefine = 4;

    ReferenceElementMesh<Topology> refMesh(nRefine);
    const int nRefnode = refMesh.nodes.size();
    const int nRefelem = refMesh.elems.size();

    out << "ZONE"
        << " N=" << nRefnode
        << " E=" << nRefelem
        << " F=FEPOINT"
        << " ET=" << TecTopo<Topology>::name()
        << std::endl;

    VectorX X;
    RefCoordType sRefCell;

    for ( int i = 0; i < nRefnode; i++ )
    {
      sRefCell = refMesh.nodes[i];

      this->eval( sRefCell, X );

      for (int k = 0; k < PhysDim::D; k++)
        out << std::scientific << std::setprecision(16) << X[k] << " ";
      out << std::endl;
    }

    for (int relem = 0; relem < nRefelem; relem++)
    {
      for (int n = 0; n < Topology::NNode; n++ )
        out << refMesh.elems[relem][n] + 1 << " "; // +1 for one based indexing
      out << std::endl;
    }

    if (this->order_ == 1) break;
  }
}

}
