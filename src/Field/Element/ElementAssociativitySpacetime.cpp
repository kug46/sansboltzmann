// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "ElementAssociativitySpacetime.h"
#include "tools/SANSException.h"

namespace SANS
{

// NOTE: default ctor needed for 'new []'
template <class Topology>
ElementAssociativity<TopoD4,Topology>::ElementAssociativity()
{
  rank_  = -1;
  order_ = -1;
  nNode_ = 0;
  nEdge_ = 0;
  nArea_ = 0;
  nFace_ = 0;
  nCell_ = 0;

  nodeList_ = NULL;
  edgeList_ = NULL;
  areaList_ = NULL;
  faceList_ = NULL;
  cellList_ = NULL;

  for (int n = 0; n < Topology::NFace; n++)
    faceSign_[n] = +1;
}


template <class Topology>
ElementAssociativity<TopoD4,Topology>::ElementAssociativity( const BasisType* basis )
{
  rank_  = -1;
  order_ = basis->order();
  nNode_ = basis->nBasisNode();
  nEdge_ = basis->nBasisEdge();
  nArea_ = basis->nBasisArea();
  nFace_ = basis->nBasisFace();
  nCell_ = basis->nBasisCell();

  nodeList_ = NULL;
  edgeList_ = NULL;
  areaList_ = NULL;
  faceList_ = NULL;
  cellList_ = NULL;


  if (nNode_ != 0)
  {
    nodeList_ = new int[nNode_];
    const int nNode = nNode_;
    for (int n = 0; n < nNode; n++)
      nodeList_[n] = -1;
  }

  if (nEdge_ != 0)
  {
    edgeList_ = new int[nEdge_];
    const int nEdge = nEdge_;
    for (int n = 0; n < nEdge; n++)
      edgeList_[n] = -1;
  }

  if (nArea_ != 0)
  {
    areaList_ = new int[nArea_];
    const int nArea = nArea_;
    for (int n = 0; n < nArea; n++)
      areaList_[n] = -1;
  }

  if (nFace_ != 0)
  {
    faceList_ = new int[nFace_];
    const int nFace = nFace_;
    for (int n = 0; n < nFace; n++)
      faceList_[n] = -1;
  }

  if (nCell_ != 0)
  {
    cellList_ = new int[nCell_];
    const int nCell = nCell_;
    for (int n = 0; n < nCell; n++)
      cellList_[n] = -1;
  }

  for (int n = 0; n < Topology::NFace; n++)
    faceSign_[n] = +1;
}


template <class Topology>
ElementAssociativity<TopoD4,Topology>::ElementAssociativity( const ElementAssociativity<TopoD4,Topology>& a )
  : ElementAssociativity()
{
  operator=(a);
}


template <class Topology>
ElementAssociativity<TopoD4,Topology>::~ElementAssociativity()
{
  delete [] nodeList_;
  delete [] edgeList_;
  delete [] areaList_;
  delete [] faceList_;
  delete [] cellList_;
}


template <class Topology>
ElementAssociativity<TopoD4,Topology>&
ElementAssociativity<TopoD4,Topology>::operator=( const ElementAssociativity<TopoD4,Topology>& a )
{
  if (this != &a)
  {
    rank_  = a.rank_;
    order_ = a.order_;
    nNode_ = a.nNode_;
    nEdge_ = a.nEdge_;
    nArea_ = a.nArea_;
    nFace_ = a.nFace_;
    nCell_ = a.nCell_;

    delete [] nodeList_; nodeList_ = NULL;
    delete [] edgeList_; edgeList_ = NULL;
    delete [] areaList_; areaList_ = NULL;
    delete [] faceList_; faceList_ = NULL;
    delete [] cellList_; cellList_ = NULL;

    if (nNode_ != 0)
    {
      nodeList_ = new int[nNode_];
      const int nNode = nNode_;
      for (int n = 0; n < nNode; n++)
        nodeList_[n] = a.nodeList_[n];
    }

    if (nEdge_ != 0)
    {
      edgeList_ = new int[nEdge_];
      const int nEdge = nEdge_;
      for (int n = 0; n < nEdge; n++)
        edgeList_[n] = a.edgeList_[n];
    }

    if (nArea_ != 0)
    {
      areaList_ = new int[nArea_];
      const int nArea = nArea_;
      for (int n = 0; n < nArea; n++)
        areaList_[n] = a.areaList_[n];
    }

    if (nFace_ != 0)
    {
      faceList_ = new int[nFace_];
      const int nFace = nFace_;
      for (int n = 0; n < nFace; n++)
        faceList_[n] = a.faceList_[n];
    }

    if (nCell_ != 0)
    {
      cellList_ = new int[nCell_];
      const int nCell = nCell_;
      for (int n = 0; n < nCell; n++)
        cellList_[n] = a.cellList_[n];
    }

    for (int n = 0; n < Topology::NFace; n++)
      faceSign_[n] = a.faceSign_[n];
  }

  return *this;
}

template <class Topology>
ElementAssociativity<TopoD4,Topology>&
ElementAssociativity<TopoD4,Topology>::operator=( const ElementAssociativityConstructor<TopoD4, Topology>& a )
{
  rank_  = a.rank();
  order_ = a.order();
  nNode_ = a.nNode();
  nEdge_ = a.nEdge();
  nArea_ = a.nArea();
  nFace_ = a.nFace();
  nCell_ = a.nCell();

  delete [] nodeList_; nodeList_ = NULL;
  delete [] edgeList_; edgeList_ = NULL;
  delete [] areaList_; areaList_ = NULL;
  delete [] faceList_; faceList_ = NULL;
  delete [] cellList_; cellList_ = NULL;

  if (nNode_ != 0)
  {
    nodeList_ = new int[nNode_];
    for (int n = 0; n < nNode_; n++)
      nodeList_[n] = a.nodeGlobal(n);
  }

  if (nEdge_ != 0)
  {
    edgeList_ = new int[nEdge_];
    for (int n = 0; n < nEdge_; n++)
      edgeList_[n] = a.edgeGlobal(n);
  }

  if (nArea_ != 0)
  {
    areaList_ = new int[nArea_];
    for (int n = 0; n < nArea_; n++)
      areaList_[n] = a.areaGlobal(n);
  }

  if (nFace_ != 0)
  {
    faceList_ = new int[nFace_];
    for (int n = 0; n < nFace_; n++)
      faceList_[n] = a.faceGlobal(n);
  }

  if (nCell_ != 0)
  {
    cellList_ = new int[nCell_];
    for (int n = 0; n < nCell_; n++)
      cellList_[n] = a.cellGlobal(n);
  }

  faceSign_ = a.faceSign();

  return *this;
}


template <class Topology>
void
ElementAssociativity<TopoD4,Topology>::getNodeGlobalMapping( int node[], int nnode ) const
{
  SANS_ASSERT( nnode == nNode_ );

  for (int n = 0; n < nNode_; n++)
    node[n] = nodeList_[n];
}

template <class Topology>
void
ElementAssociativity<TopoD4,Topology>::getEdgeGlobalMapping( int edge[], int nedge ) const
{
  SANS_ASSERT_MSG( nedge == nEdge_, " nedge = %d  nEdge_ = %d", nedge, nEdge_ );

  for (int n = 0; n < nEdge_; n++)
    edge[n] = edgeList_[n];
}

template <class Topology>
void
ElementAssociativity<TopoD4,Topology>::getAreaGlobalMapping( int area[], int narea ) const
{
  SANS_ASSERT_MSG( narea == nArea_, " narea = %d  nArea_ = %d", narea, nArea_ );

  for (int n = 0; n < nArea_; n++)
    area[n] = areaList_[n];
}


template <class Topology>
void
ElementAssociativity<TopoD4,Topology>::getFaceGlobalMapping( int face[], int nface ) const
{
  SANS_ASSERT_MSG( nface == nFace_, " nface = %d  nFace_ = %d", nface, nFace_ );

  for (int n = 0; n < nFace_; n++)
    face[n] = faceList_[n];
}

template <class Topology>
void
ElementAssociativity<TopoD4,Topology>::getCellGlobalMapping( int cell[], int ncell ) const
{
  SANS_ASSERT( ncell == nCell_ );

  for (int n = 0; n < nCell_; n++)
    cell[n] = cellList_[n];
}

template <class Topology>
void
ElementAssociativity<TopoD4,Topology>::getGlobalMapping( int map[], int ndof ) const
{
  SANS_ASSERT( ndof == nNode_ + nEdge_ + nArea_ + nFace_ + nCell_ );

  int offset = 0;
  for (int n = 0; n < nNode_; n++)
    map[n + offset] = nodeList_[n];
  offset += nNode_;

  for (int n = 0; n < nEdge_; n++)
    map[n + offset] = edgeList_[n];
  offset += nEdge_;

  for (int n = 0; n < nArea_; n++)
    map[n + offset] = areaList_[n];
  offset += nArea_;

  for (int n = 0; n < nFace_; n++)
    map[n + offset] = faceList_[n];
  offset += nFace_;

  for (int n = 0; n < nCell_; n++)
    map[n + offset] = cellList_[n];
}



template <class Topology>
void
ElementAssociativity<TopoD4,Topology>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "ElementAssociativity<TopoD4>: order_ = " << order_
      << "  nNode_ = " << nNode_
      << "  nEdge_ = " << nEdge_
      << "  nArea_ = " << nArea_
      << "  nCell_ = " << nCell_;
  out << "  faceSign_ =";

  for (int n = 0; n < Topology::NFace; n++)
    out << " " << faceSign_[n];
  out << std::endl;

  if (nNode_ > 0)
  {
    out << indent << indent << "nodeList =";
    for (int n = 0; n < nNode_; n++)
      out << " " << nodeList_[n];
    out << std::endl;
  }

  if (nEdge_ > 0)
  {
    out << indent << indent << "edgeList =";
    for (int n = 0; n < nEdge_; n++)
      out << " " << edgeList_[n];
    out << std::endl;
  }

  if (nArea_ > 0)
  {
    out << indent << indent << "areaList =";
    for (int n = 0; n < nArea_; n++)
      out << " " << areaList_[n];
    out << std::endl;
  }

  if (nFace_ > 0)
  {
    out << indent << indent << "faceList =";
    for (int n = 0; n < nFace_; n++)
      out << " " << faceList_[n];
    out << std::endl;
  }

  if (nCell_ > 0)
  {
    out << indent << indent << "cellList =";
    for (int n = 0; n < nCell_; n++)
      out << " " << cellList_[n];
    out << std::endl;
  }
}

// Explicitly instantiate the classes
template class ElementAssociativity<TopoD4,Pentatope>;

}
