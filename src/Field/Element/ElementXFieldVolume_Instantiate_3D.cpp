// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ELEMENTXFIELDVOLUME_INSTANTIATE

#include "ElementXFieldVolume_impl.h"
#include "ElementXFieldJacobianEquilateral.h"
#include "Quadrature/Quadrature.h"
#include "BasisFunction/TraceToCellRefCoord.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// 3-D Cartesian
//----------------------------------------------------------------------------//


template <class PhysDim, class Topology>
inline Real
ElementXField<PhysDim, TopoD3, Topology>::volume() const
{
  Real s = Topology::centerRef[0];
  Real t = Topology::centerRef[1];
  Real u = Topology::centerRef[2];
  return volume( s, t, u );
}

template <class PhysDim, class Topology>
inline Real
ElementXField<PhysDim, TopoD3, Topology>::volume( const Real& sRef, const Real& tRef, const Real& uRef ) const
{
  Matrix J;
  jacobian( sRef, tRef, uRef, J );

  return volume( J );
}

template <class PhysDim, class Topology>
inline Real
ElementXField<PhysDim, TopoD3, Topology>::volume( const QuadraturePoint<TopoD3>& ref ) const
{
  Matrix J;
  jacobian( ref, J );

  return volume( J );
}

template <class PhysDim, class Topology>
inline Real
ElementXField<PhysDim, TopoD3, Topology>::volume( const Matrix& J ) const
{
  Real det =   J(0,0)*( J(1,1)*J(2,2) - J(1,2)*J(2,1) )
             - J(0,1)*( J(1,0)*J(2,2) - J(1,2)*J(2,0) )
             + J(0,2)*( J(1,0)*J(2,1) - J(1,1)*J(2,0) );

  return Topology::volumeRef*det;
}

//---------------------------------------------------------------------------//
template<class PhysDim>
void AddTracePoints(const ElementXField<PhysDim, TopoD3, Tet>& xfldElem, BoundingBox<PhysDim>& bbox)
{
  typedef DLA::VectorS<PhysDim::D,Real> VectorX;

  typedef DLA::VectorS<TopoD3::D,Real> RefCoordCell;
  typedef DLA::VectorS<TopoD2::D,Real> RefCoordTrace;

  CanonicalTraceToCell canonicalTrace(0,1);
  VectorX X;

  for (int itrace = 0; itrace < Tet::NTrace; itrace++)
  {
    canonicalTrace.trace = itrace;
    RefCoordCell sRefCell;
    RefCoordTrace sReTrace=0;

    // Grab coordinates on the trace (including on edges) to add to the bounding box
    int kmax = 9;
    Real dRef = 1./(kmax-1);
    for (int ki = 0; ki < kmax; ki++)
    {
      sReTrace[1] = 0;
      for (int kj = ki; kj < kmax; kj++)
      {
        TraceToCellRefCoord<Triangle, TopoD3, Tet>::eval( canonicalTrace, sReTrace, sRefCell );

        xfldElem.coordinates(sRefCell, X);

        bbox.setBound(X);

        sReTrace[1] += dRef;
      }
      sReTrace[0] += dRef;
    }
  }
}

//---------------------------------------------------------------------------//
template<class PhysDim>
void AddTracePoints(const ElementXField<PhysDim, TopoD3, Hex>& xfldElem, BoundingBox<PhysDim>& bbox)
{
  typedef DLA::VectorS<PhysDim::D,Real> VectorX;

  typedef DLA::VectorS<TopoD3::D,Real> RefCoordCell;
  typedef DLA::VectorS<TopoD2::D,Real> RefCoordTrace;

  CanonicalTraceToCell canonicalTrace(0,1);
  VectorX X;

  for (int itrace = 0; itrace < Tet::NTrace; itrace++)
  {
    canonicalTrace.trace = itrace;
    RefCoordCell sRefCell;
    RefCoordTrace sReTrace=0;

    // Grab coordinates on the trace (including on edges) to add to the bounding box
    int kmax = 9;
    Real dRef = 1./(kmax-1);
    for (int ki = 0; ki < kmax; ki++)
    {
      for (int kj = 0; kj < kmax; kj++)
      {
        sReTrace[0] = dRef*ki;
        sReTrace[1] = dRef*kj;

        TraceToCellRefCoord<Quad, TopoD3, Hex>::eval( canonicalTrace, sReTrace, sRefCell );

        xfldElem.coordinates(sRefCell, X);

        bbox.setBound(X);
      }
    }
  }
}

//---------------------------------------------------------------------------//
template <class PhysDim, class Topology>
BoundingBox<PhysDim>
ElementXField<PhysDim, TopoD3, Topology>::boundingBox() const
{
  BoundingBox<PhysDim> bbox;

  // get the corners of the element
  for (int n = 0; n < Topology::NNode; n++)
    bbox.setBound(DOF_[n]);

  if ( basis_->order()  > 1 )
    AddTracePoints(*this, bbox);

  return bbox;
}

//---------------------------------------------------------------------------//
template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD3, Topology>::impliedMetric( TensorSymX& M ) const
{
  Real s = Topology::centerRef[0];
  Real t = Topology::centerRef[1];
  Real u = Topology::centerRef[2];
  impliedMetric( s, t, u, M );
}

//---------------------------------------------------------------------------//
template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD3, Topology>::impliedMetric( const Real& s, const Real& t, const Real& u, TensorSymX& M ) const
{
  Matrix Jref; //jacobian from reference element to physical element
  jacobian( s, t, u, Jref );

  // transformation matrix from the unit equilateral element to the reference element
  const Matrix& invJeq = JacobianEquilateralTransform<PhysDim,TopoD3,Topology>::invJeq;

  Matrix J = Jref*invJeq; //jacobian from equilateral element to physical element

  TensorSymX JJt = J*Transpose(J);

  // Compute implied metric M = inv(JJt)
  Real det =   JJt(0,0)*( JJt(1,1)*JJt(2,2) - JJt(1,2)*JJt(2,1) )
             - JJt(0,1)*( JJt(1,0)*JJt(2,2) - JJt(1,2)*JJt(2,0) )
             + JJt(0,2)*( JJt(1,0)*JJt(2,1) - JJt(1,1)*JJt(2,0) );

  M(0,0) = (JJt(1,1)*JJt(2,2) - JJt(1,2)*JJt(2,1)) / det;
  M(1,0) = (JJt(1,2)*JJt(2,0) - JJt(1,0)*JJt(2,2)) / det;
  M(1,1) = (JJt(0,0)*JJt(2,2) - JJt(0,2)*JJt(2,0)) / det;
  M(2,0) = (JJt(1,0)*JJt(2,1) - JJt(1,1)*JJt(2,0)) / det;
  M(2,1) = (JJt(0,1)*JJt(2,0) - JJt(0,0)*JJt(2,1)) / det;
  M(2,2) = (JJt(0,0)*JJt(1,1) - JJt(0,1)*JJt(1,0)) / det;
}

template <class PhysDim, class Topology>
inline void
ElementXField<PhysDim, TopoD3, Topology>::evalReferenceCoordinateGradient(
    const Real* __restrict phis, const Real* __restrict phit, const Real* __restrict phiu,
    VectorX& sgrad, VectorX& tgrad, VectorX& ugrad ) const
{
  Real xs, xt, xu, ys, yt, yu, zs, zt, zu;
  Real det;

  xs = 0;  xt = 0;  xu = 0;
  ys = 0;  yt = 0;  yu = 0;
  zs = 0;  zt = 0;  zu = 0;
  for (int n = 0; n < nDOF_; n++)
  {
    xs += phis[n]*DOF_[n](0);  xt += phit[n]*DOF_[n](0);  xu += phiu[n]*DOF_[n](0);
    ys += phis[n]*DOF_[n](1);  yt += phit[n]*DOF_[n](1);  yu += phiu[n]*DOF_[n](1);
    zs += phis[n]*DOF_[n](2);  zt += phit[n]*DOF_[n](2);  zu += phiu[n]*DOF_[n](2);
  }

  det = xu*ys*zt - xs*yu*zt + xt*yu*zs - xu*yt*zs + xs*yt*zu - xt*ys*zu;

  sgrad[0]  = (yt*zu - yu*zt)/det;
  sgrad[1]  = (xu*zt - xt*zu)/det;
  sgrad[2]  = (xt*yu - xu*yt)/det;

  tgrad[0]  = (yu*zs - ys*zu)/det;
  tgrad[1]  = (xs*zu - xu*zs)/det;
  tgrad[2]  = (xu*ys - xs*yu)/det;

  ugrad[0]  = (ys*zt - yt*zs)/det;
  ugrad[1]  = (xt*zs - xs*zt)/det;
  ugrad[2]  = (xs*yt - xt*ys)/det;
}


template <class PhysDim, class Topology>
inline void
ElementXField<PhysDim, TopoD3, Topology>::evalReferenceCoordinateHessian(
    const Real& s, const Real& t, const Real& u, TensorSymX& shess, TensorSymX& thess, TensorSymX& uhess ) const
{
  SANS_ASSERT(nDOF_ > 0); //To please the clang compiler
  const int nDOF = nDOF_;

  Real xs=0, xt=0, xu=0;
  Real ys=0, yt=0, yu=0;
  Real zs=0, zt=0, zu=0;
  Real xss=0,
       xst=0, xtt=0,
       xsu=0, xtu=0, xuu=0;
  Real yss=0,
       yst=0, ytt=0,
       ysu=0, ytu=0, yuu=0;
  Real zss=0,
       zst=0, ztt=0,
       zsu=0, ztu=0, zuu=0;
  Real det;

  basis_->evalBasisDerivative( s, t, u, faceSign_, phis_, phit_, phiu_, nDOF_ );
  basis_->evalBasisHessianDerivative( s, t, u, faceSign_, phiss_,
                                                          phist_, phitt_,
                                                          phisu_, phitu_, phiuu_, nDOF_ );

  for (int n = 0; n < nDOF; n++)
  {
    // first derivatives
    xs += phis_[n]*DOF_[n](0);  xt += phit_[n]*DOF_[n](0);  xu += phiu_[n]*DOF_[n](0);
    ys += phis_[n]*DOF_[n](1);  yt += phit_[n]*DOF_[n](1);  yu += phiu_[n]*DOF_[n](1);
    zs += phis_[n]*DOF_[n](2);  zt += phit_[n]*DOF_[n](2);  zu += phiu_[n]*DOF_[n](2);

    // second derivatives
    xss += phiss_[n]*DOF_[n](0);
    xst += phist_[n]*DOF_[n](0); xtt += phitt_[n]*DOF_[n](0);
    xsu += phisu_[n]*DOF_[n](0); xtu += phitu_[n]*DOF_[n](0); xuu += phiuu_[n]*DOF_[n](0);

    yss += phiss_[n]*DOF_[n](1);
    yst += phist_[n]*DOF_[n](1); ytt += phitt_[n]*DOF_[n](1);
    ysu += phisu_[n]*DOF_[n](1); ytu += phitu_[n]*DOF_[n](1); yuu += phiuu_[n]*DOF_[n](1);

    zss += phiss_[n]*DOF_[n](2);
    zst += phist_[n]*DOF_[n](2); ztt += phitt_[n]*DOF_[n](2);
    zsu += phisu_[n]*DOF_[n](2); ztu += phitu_[n]*DOF_[n](2); zuu += phiuu_[n]*DOF_[n](2);
  }

  // Definition of Matrix in ElementXFieldArea.h
  Matrix invJ; // inverse Jacobian
  TensorSymX xHess, yHess, zHess, tempHess1, tempHess2, tempHess3;

  // Construct the inverse Jacobian using 3-by-3 matrix inversion formula
  det = xu*ys*zt - xs*yu*zt + xt*yu*zs - xu*yt*zs + xs*yt*zu - xt*ys*zu;

  // sgrad
  invJ(0,0)  = (yt*zu - yu*zt)/det;
  invJ(0,1)  = (xu*zt - xt*zu)/det;
  invJ(0,2)  = (xt*yu - xu*yt)/det;

  // tgrad
  invJ(1,0)  = (yu*zs - ys*zu)/det;
  invJ(1,1)  = (xs*zu - xu*zs)/det;
  invJ(1,2)  = (xu*ys - xs*yu)/det;

  // ugrad
  invJ(2,0)  = (ys*zt - yt*zs)/det;
  invJ(2,1)  = (xt*zs - xs*zt)/det;
  invJ(2,2)  = (xs*yt - xt*ys)/det;


  // Populate the Hessians of the Cartesian coordinates
  xHess(0,0) = xss;
  xHess(1,0) = xst; xHess(1,1) = xtt;
  xHess(2,0) = xsu; xHess(2,1) = xtu; xHess(2,2) = xuu;

  yHess(0,0) = yss;
  yHess(1,0) = yst; yHess(1,1) = ytt;
  yHess(2,0) = ysu; yHess(2,1) = ytu; yHess(2,2) = yuu;

  zHess(0,0) = zss;
  zHess(1,0) = zst; zHess(1,1) = ztt;
  zHess(2,0) = zsu; zHess(2,1) = ztu; zHess(2,2) = zuu;

  tempHess1 = -(invJ(0,0)*xHess + invJ(0,1)*yHess + invJ(0,2)*zHess);
  tempHess2 = -(invJ(1,0)*xHess + invJ(1,1)*yHess + invJ(1,2)*zHess);
  tempHess3 = -(invJ(2,0)*xHess + invJ(2,1)*yHess + invJ(2,2)*zHess);

#if 0
  std::cout << "invJ = " << std::endl;
  std::cout << "( " << invJ(0,0) << ",  " << invJ(0,1) << " )" << std::endl;
  std::cout << "( " << invJ(1,0) << ",  " << invJ(1,1) << " )" << std::endl;

  std::cout << "tempHess1 = " << std::endl;
  std::cout << "( " << tempHess1(0,0) << ",  " << tempHess1(0,1) << " )" << std::endl;
  std::cout << "( " << tempHess1(1,0) << ",  " << tempHess1(1,1) << " )" << std::endl;

  std::cout << "tempHess2 = " << std::endl;
  std::cout << "( " << tempHess2(0,0) << ",  " << tempHess2(0,1) << " )" << std::endl;
  std::cout << "( " << tempHess2(1,0) << ",  " << tempHess2(1,1) << " )" << std::endl;
#endif

  // Calculate the transformed hessian
  shess = Transpose(invJ)*tempHess1*invJ;
  thess = Transpose(invJ)*tempHess2*invJ;
  uhess = Transpose(invJ)*tempHess3*invJ;

#if 0
  std::cout << "In evalReferenceCoordinateHessian:" << std::endl;
  std::cout << "det: "<< det << std::endl;
  std::cout << "xt,xs,yt,ys: " << xt << xs << yt << ys <<std::endl;
  std::cout << "shess: " << shess(0,0) << shess(1,0) << shess(1,1) << std::endl;
  std::cout << "thess: " << thess(0,0) << thess(1,0) << thess(1,1) << std::endl;
  std::cout << "tempHess1" << tempHess1(0,0) << tempHess1(1,0) << tempHess1(1,1) <<std::endl;
  std::cout << "tempHess2" << tempHess2(0,0) << tempHess2(1,0) << tempHess2(1,1) <<std::endl;
#endif

}

template <class PhysDim, class Topology>
template <class RefCoord>
void
ElementXField<PhysDim, TopoD3, Topology>::evalBasisHessian( const RefCoord& sRef,
                                                            const ElementBasis<TopoD3, Topology>& fldElem,
                                                            TensorSymX hessphi[], const int nphi ) const
{
  SANS_ASSERT( PhysDim::D == 3 ); // TODO: currently only useful for in physically 3-D space; not for manifolds

  const int nBasis = fldElem.nDOF();   // total solution DOFs in element
  SANS_ASSERT(nphi == nBasis);

  VectorX sgrad, tgrad, ugrad;         // gradients of reference coordinates (s,t,u)
  TensorSymX shess, thess, uhess;      // hessians of reference coordinates (s,t,u)
  const Real *phis, *phit, *phiu;      // derivatives of solution basis wrt (s,t,u)
  const Real *phiss, *phist, *phisu,
                     *phitt, *phitu,
                             *phiuu;   // second derivatives of solution basis wrt (s,t,u)

  evalReferenceCoordinateGradient( sRef, sgrad, tgrad, ugrad );
  evalReferenceCoordinateHessian( sRef, shess, thess, uhess );
  fldElem.evalBasisDerivative( sRef, &phis, &phit, &phiu );
  fldElem.evalBasisHessianDerivative( sRef, &phiss, &phist, &phitt, &phisu, &phitu, &phiuu );

  Matrix invJ; // Inverse Jacobian
  TensorSymX refHess;
  invJ(0,0) = sgrad[0]; invJ(0,1) = sgrad[1]; invJ(0,2) = sgrad[2];
  invJ(1,0) = tgrad[0]; invJ(1,1) = tgrad[1]; invJ(1,2) = tgrad[2];
  invJ(2,0) = ugrad[0]; invJ(2,1) = ugrad[1]; invJ(2,2) = ugrad[2];

  for (int n = 0; n < nphi; n++)
  {
    refHess(0,0) = phiss[n];
    refHess(1,0) = phist[n]; refHess(1,1) = phitt[n];
    refHess(2,0) = phisu[n]; refHess(2,1) = phitu[n]; refHess(2,2) = phiuu[n];

    hessphi[n]  = Transpose(invJ)*refHess*invJ;
    hessphi[n] += phis[n]*shess + phit[n]*thess +phiu[n]*uhess;
  }
}

template class ElementXField<PhysD3, TopoD3, Tet>;
template class ElementXField<PhysD3, TopoD3, Hex>;
}
