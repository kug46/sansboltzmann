// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ELEMENTXFIELDSPACETIME_INSTANTIATE

#include "ElementXFieldSpacetime_impl.h"
#include "ElementXFieldJacobianEquilateral.h"
#include "Quadrature/Quadrature.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// 4-D Cartesian
//----------------------------------------------------------------------------//


template <class PhysDim, class Topology>
inline Real
ElementXField<PhysDim, TopoD4, Topology>::volume() const
{
  Real s = Topology::centerRef[0];
  Real t = Topology::centerRef[1];
  Real u = Topology::centerRef[2];
  Real v = Topology::centerRef[3];
  return volume( s, t, u, v );
}

template <class PhysDim, class Topology>
inline Real
ElementXField<PhysDim, TopoD4, Topology>::volume( const Real& sRef, const Real& tRef, const Real& uRef, const Real& vRef ) const
{
  Matrix J;
  jacobian( sRef, tRef, uRef, vRef,  J );

  return volume( J );
}

template <class PhysDim, class Topology>
inline Real
ElementXField<PhysDim, TopoD4, Topology>::volume( const QuadraturePoint<TopoD4>& ref ) const
{
  Matrix J;
  jacobian( ref, J );

  return volume( J );
}

template <class PhysDim, class Topology>
inline Real
ElementXField<PhysDim, TopoD4, Topology>::volume( const Matrix& J ) const
{
  Real det = Det(J);
  return Topology::volumeRef*det;
}

//---------------------------------------------------------------------------//
template<class PhysDim>
void AddTracePoints(const ElementXField<PhysDim, TopoD4, Pentatope>& xfldElem, BoundingBox<PhysDim>& bbox)
{
  SANS_DEVELOPER_EXCEPTION("implement");

  typedef DLA::VectorS<PhysDim::D,Real> VectorX;

  typedef DLA::VectorS<TopoD4::D,Real> RefCoordCell;
  typedef DLA::VectorS<TopoD3::D,Real> RefCoordTrace;

  CanonicalTraceToCell canonicalTrace(0,1);
  VectorX X;

  for (int itrace = 0; itrace < Pentatope::NTrace; itrace++)
  {
    canonicalTrace.trace = itrace;
    RefCoordCell sRefCell;
    RefCoordTrace sReTrace=0;

    // Grab coordinates on the trace (including on edges) to add to the bounding box
    int kmax = 9;
    Real dRef = 1./(kmax-1);
    for (int ki = 0; ki < kmax; ki++)
    {
      sReTrace[1] = 0;
      for (int kj = ki; kj < kmax; kj++)
      {
        TraceToCellRefCoord<Tet, TopoD4, Pentatope>::eval( canonicalTrace, sReTrace, sRefCell );

        xfldElem.coordinates(sRefCell, X);

        bbox.setBound(X);

        sReTrace[1] += dRef;
      }
      sReTrace[0] += dRef;
    }
  }
}

//---------------------------------------------------------------------------//
template <class PhysDim, class Topology>
BoundingBox<PhysDim>
ElementXField<PhysDim, TopoD4, Topology>::boundingBox() const
{

  if (BaseType::order() > 1)
    SANS_DEVELOPER_EXCEPTION("Bounding box hasn't been unit tested for order greater than one.");

  BoundingBox<PhysDim> bbox;

  // get the corners of the element
  for (int n = 0; n < Topology::NNode; n++)
    bbox.setBound(DOF_[n]);

  if ( basis_->order()  > 1 )
    AddTracePoints(*this, bbox);

  return bbox;
}

//---------------------------------------------------------------------------//
template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD4, Topology>::impliedMetric( TensorSymX& M ) const
{
  Real s = Topology::centerRef[0];
  Real t = Topology::centerRef[1];
  Real u = Topology::centerRef[2];
  Real v = Topology::centerRef[3];
  impliedMetric( s, t, u, v, M );
}

//---------------------------------------------------------------------------//
template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD4, Topology>::impliedMetric( const Real& s, const Real& t, const Real& u, const Real& v, TensorSymX& M ) const
{
  Matrix Jref; //jacobian from reference element to physical element
  jacobian( s, t, u, v, Jref );

  // transformation matrix from the unit equilateral element to the reference element
  const Matrix& invJeq = JacobianEquilateralTransform<PhysDim,TopoD4,Topology>::invJeq;

  Matrix J = Jref*invJeq; //jacobian from equilateral element to physical element

  TensorSymX JJt = J*Transpose(J);

  // Compute implied metric M = inv(JJt)
  Real det = DLA::Det(JJt);

  Real a = JJt(0,0), b = JJt(0,1), c = JJt(0,2), d = JJt(0,3);
  Real e = JJt(1,0), f = JJt(1,1), g = JJt(1,2), h = JJt(1,3);
  Real i = JJt(2,0), j = JJt(2,1), k = JJt(2,2), L = JJt(2,3);
  Real m = JJt(3,0), n = JJt(3,1), Q = JJt(3,2), p = JJt(3,3);

  M(0,0) = (-L*Q*f+L*g*n+Q*h*j+f*k*p-g*j*p-h*k*n )/det;
  M(0,1) = (L*Q*b-L*c*n-Q*d*j-b*k*p+c*j*p+d*k*n)/det;
  M(0,2) = (-Q*b*h+Q*d*f+b*g*p-c*f*p+c*h*n-d*g*n)/det;
  M(0,3) = (-L*b*g+L*c*f+b*h*k-c*h*j-d*f*k+d*g*j)/det;
  M(1,1) = (-L*Q*a+L*c*m+Q*d*i+a*k*p-c*i*p-d*k*m)/det;
  M(1,2) = (Q*a*h-Q*d*e-a*g*p+c*e*p-c*h*m+d*g*m)/det;
  M(1,3) = (L*a*g-L*c*e-a*h*k+c*h*i+d*e*k-d*g*i)/det;
  M(2,2) = (a*f*p-a*h*n-b*e*p+b*h*m+d*e*n-d*f*m)/det;
  M(2,3) = (-L*a*f+L*b*e+a*h*j-b*h*i-d*e*j+d*f*i)/det;
  M(3,3) = (a*f*k-a*g*j-b*e*k+b*g*i+c*e*j-c*f*i)/det;
}

template <class PhysDim, class Topology>
inline void
ElementXField<PhysDim, TopoD4, Topology>::evalReferenceCoordinateGradient(
    const Real* __restrict phis, const Real* __restrict phit,
    const Real* __restrict phiu, const Real* __restrict phiv,
    VectorX& sgrad, VectorX& tgrad, VectorX& ugrad , VectorX& vgrad) const
{

  Real xs, xt, xu, xv;
  Real ys, yt, yu, yv;
  Real zs, zt, zu, zv;
  Real ws, wt, wu, wv;
  Real det;

  xs= 0; xt= 0; xu= 0; xv= 0;
  ys= 0; yt= 0; yu= 0; yv= 0;
  zs= 0; zt= 0; zu= 0; zv= 0;
  ws= 0; wt= 0; wu= 0; wv= 0;

  for (int n = 0; n < nDOF_; n++)
  {
    xs += phis[n]*DOF_[n](0);  xt += phit[n]*DOF_[n](0);  xu += phiu[n]*DOF_[n](0); xv += phiv[n]*DOF_[n](0);
    ys += phis[n]*DOF_[n](1);  yt += phit[n]*DOF_[n](1);  yu += phiu[n]*DOF_[n](1); yv += phiv[n]*DOF_[n](1);
    zs += phis[n]*DOF_[n](2);  zt += phit[n]*DOF_[n](2);  zu += phiu[n]*DOF_[n](2); zv += phiv[n]*DOF_[n](2);
    ws += phis[n]*DOF_[n](3);  wt += phit[n]*DOF_[n](3);  wu += phiu[n]*DOF_[n](3); wv += phiv[n]*DOF_[n](3);
  }

  det= -wv*xu*yt*zs + wu*xv*yt*zs + wv*xt*yu*zs - wt*xv*yu*zs
        - wu*xt*yv*zs + wt*xu*yv*zs + wv*xu*ys*zt - wu*xv*ys*zt
        - wv*xs*yu*zt + ws*xv*yu*zt + wu*xs*yv*zt - ws*xu*yv*zt
        - wv*xt*ys*zu + wt*xv*ys*zu + wv*xs*yt*zu - ws*xv*yt*zu
        - wt*xs*yv*zu + ws*xt*yv*zu + wu*xt*ys*zv - wt*xu*ys*zv
        - wu*xs*yt*zv + ws*xu*yt*zv + wt*xs*yu*zv - ws*xt*yu*zv;

  sgrad[0]= (-wv*yu*zt + wu*yv*zt + wv*yt*zu - wt*yv*zu - wu*yt*zv + wt*yu*zv)/det;
  sgrad[1]= ( wv*xu*zt - wu*xv*zt - wv*xt*zu + wt*xv*zu + wu*xt*zv - wt*xu*zv)/det;
  sgrad[2]= (-wv*xu*yt + wu*xv*yt + wv*xt*yu - wt*xv*yu - wu*xt*yv + wt*xu*yv)/det;
  sgrad[3]= ( xv*yu*zt - xu*yv*zt - xv*yt*zu + xt*yv*zu + xu*yt*zv - xt*yu*zv)/det;

  tgrad[0]= ( wv*yu*zs - wu*yv*zs - wv*ys*zu + ws*yv*zu + wu*ys*zv - ws*yu*zv)/det;
  tgrad[1]= (-wv*xu*zs + wu*xv*zs + wv*xs*zu - ws*xv*zu - wu*xs*zv + ws*xu*zv)/det;
  tgrad[2]= ( wv*xu*ys - wu*xv*ys - wv*xs*yu + ws*xv*yu + wu*xs*yv - ws*xu*yv)/det;
  tgrad[3]= (-xv*yu*zs + xu*yv*zs + xv*ys*zu - xs*yv*zu - xu*ys*zv + xs*yu*zv)/det;

  ugrad[0]= (-wv*yt*zs + wt*yv*zs + wv*ys*zt - ws*yv*zt - wt*ys*zv + ws*yt*zv)/det;
  ugrad[1]= ( wv*xt*zs - wt*xv*zs - wv*xs*zt + ws*xv*zt + wt*xs*zv - ws*xt*zv)/det;
  ugrad[2]= (-wv*xt*ys + wt*xv*ys + wv*xs*yt - ws*xv*yt - wt*xs*yv + ws*xt*yv)/det;
  ugrad[3]= ( xv*yt*zs - xt*yv*zs - xv*ys*zt + xs*yv*zt + xt*ys*zv - xs*yt*zv)/det;

  vgrad[0]= ( wu*yt*zs - wt*yu*zs - wu*ys*zt + ws*yu*zt + wt*ys*zu - ws*yt*zu)/det;
  vgrad[1]= (-wu*xt*zs + wt*xu*zs + wu*xs*zt - ws*xu*zt - wt*xs*zu + ws*xt*zu)/det;
  vgrad[2]= ( wu*xt*ys - wt*xu*ys - wu*xs*yt + ws*xu*yt + wt*xs*yu - ws*xt*yu)/det;
  vgrad[3]= (-xu*yt*zs + xt*yu*zs + xu*ys*zt - xs*yu*zt - xt*ys*zu + xs*yt*zu)/det;

//  SANS_DEVELOPER_EXCEPTION("unit test.");

}


template <class PhysDim, class Topology>
inline void
ElementXField<PhysDim, TopoD4, Topology>::evalReferenceCoordinateHessian(
    const Real& s, const Real& t, const Real& u, const Real& v,
    TensorSymX& shess, TensorSymX& thess, TensorSymX& uhess, TensorSymX& vhess ) const
{

  if (BaseType::order() > 1)
    SANS_DEVELOPER_EXCEPTION("Ref coordinate Hessian hasn't been unit tested for order greater than one.");

  SANS_ASSERT(nDOF_ > 0); //To please the clang compiler
  const int nDOF = nDOF_;

  Real xs= 0, xt= 0, xu= 0, xv= 0;
  Real ys= 0, yt= 0, yu= 0, yv= 0;
  Real zs= 0, zt= 0, zu= 0, zv= 0;
  Real ws= 0, wt= 0, wu= 0, wv= 0;
  Real xss= 0,
       xst= 0, xtt= 0,
       xsu= 0, xtu= 0, xuu= 0,
       xsv= 0, xtv= 0, xuv= 0, xvv= 0;
  Real yss= 0,
       yst= 0, ytt= 0,
       ysu= 0, ytu= 0, yuu= 0,
       ysv= 0, ytv= 0, yuv= 0, yvv= 0;
  Real zss= 0,
       zst= 0, ztt= 0,
       zsu= 0, ztu= 0, zuu= 0,
       zsv= 0, ztv= 0, zuv= 0, zvv= 0;
  Real wss= 0,
       wst= 0, wtt= 0,
       wsu= 0, wtu= 0, wuu= 0,
       wsv= 0, wtv= 0, wuv= 0, wvv= 0;
  Real det;

  basis_->evalBasisDerivative( s, t, u, v, faceSign_, phis_, phit_, phiu_, phiv_, nDOF_ );
  basis_->evalBasisHessianDerivative( s, t, u, v, faceSign_, phiss_,
                                                             phist_, phitt_,
                                                             phisu_, phitu_, phiuu_,
                                                             phisv_, phitv_, phiuv_, phivv_, nDOF_ );

  for (int n = 0; n < nDOF; n++)
  {
    // first derivatives
    xs += phis_[n]*DOF_[n](0);  xt += phit_[n]*DOF_[n](0);  xu += phiu_[n]*DOF_[n](0);  xv += phiv_[n]*DOF_[n](0);
    ys += phis_[n]*DOF_[n](1);  yt += phit_[n]*DOF_[n](1);  yu += phiu_[n]*DOF_[n](1);  yv += phiv_[n]*DOF_[n](1);
    zs += phis_[n]*DOF_[n](2);  zt += phit_[n]*DOF_[n](2);  zu += phiu_[n]*DOF_[n](2);  zv += phiv_[n]*DOF_[n](2);
    ws += phis_[n]*DOF_[n](3);  wt += phit_[n]*DOF_[n](3);  wu += phiu_[n]*DOF_[n](3);  wv += phiv_[n]*DOF_[n](3);

    // second derivatives
    xss += phiss_[n]*DOF_[n](0);
    xst += phist_[n]*DOF_[n](0); xtt += phitt_[n]*DOF_[n](0);
    xsu += phisu_[n]*DOF_[n](0); xtu += phitu_[n]*DOF_[n](0); xuu += phiuu_[n]*DOF_[n](0);
    xsv += phisv_[n]*DOF_[n](0); xtv += phitv_[n]*DOF_[n](0); xuv += phiuv_[n]*DOF_[n](0); xvv += phivv_[n]*DOF_[n](0);

    yss += phiss_[n]*DOF_[n](1);
    yst += phist_[n]*DOF_[n](1); ytt += phitt_[n]*DOF_[n](1);
    ysu += phisu_[n]*DOF_[n](1); ytu += phitu_[n]*DOF_[n](1); yuu += phiuu_[n]*DOF_[n](1);
    ysv += phisv_[n]*DOF_[n](1); ytv += phitv_[n]*DOF_[n](1); yuv += phiuv_[n]*DOF_[n](1); yvv += phivv_[n]*DOF_[n](1);

    zss += phiss_[n]*DOF_[n](2);
    zst += phist_[n]*DOF_[n](2); ztt += phitt_[n]*DOF_[n](2);
    zsu += phisu_[n]*DOF_[n](2); ztu += phitu_[n]*DOF_[n](2); zuu += phiuu_[n]*DOF_[n](2);
    zsv += phisv_[n]*DOF_[n](2); ztv += phitv_[n]*DOF_[n](2); zuv += phiuv_[n]*DOF_[n](2); zvv += phivv_[n]*DOF_[n](2);

    wss += phiss_[n]*DOF_[n](3);
    wst += phist_[n]*DOF_[n](3); wtt += phitt_[n]*DOF_[n](3);
    wsu += phisu_[n]*DOF_[n](3); wtu += phitu_[n]*DOF_[n](3); wuu += phiuu_[n]*DOF_[n](3);
    wsv += phisv_[n]*DOF_[n](3); wtv += phitv_[n]*DOF_[n](3); wuv += phiuv_[n]*DOF_[n](3); wvv += phivv_[n]*DOF_[n](3);
  }

  // Definition of Matrix in ElementXFieldArea.h
  Matrix invJ; // inverse Jacobian
  TensorSymX xHess, yHess, zHess, wHess, tempHess1, tempHess2, tempHess3, tempHess4;

  // Construct the inverse Jacobian using 4-by-4 matrix inversion formula
  det= -wv*xu*yt*zs + wu*xv*yt*zs + wv*xt*yu*zs - wt*xv*yu*zs - wu*xt*yv*zs + wt*xu*yv*zs + wv*xu*ys*zt - wu*xv*ys*zt
        - wv*xs*yu*zt + ws*xv*yu*zt + wu*xs*yv*zt - ws*xu*yv*zt - wv*xt*ys*zu + wt*xv*ys*zu + wv*xs*yt*zu - ws*xv*yt*zu
        - wt*xs*yv*zu + ws*xt*yv*zu + wu*xt*ys*zv - wt*xu*ys*zv - wu*xs*yt*zv + ws*xu*yt*zv + wt*xs*yu*zv - ws*xt*yu*zv;

  invJ(0, 0)= (-wv*yu*zt + wu*yv*zt + wv*yt*zu - wt*yv*zu - wu*yt*zv + wt*yu*zv)/det;
  invJ(0, 1)= ( wv*xu*zt - wu*xv*zt - wv*xt*zu + wt*xv*zu + wu*xt*zv - wt*xu*zv)/det;
  invJ(0, 2)= (-wv*xu*yt + wu*xv*yt + wv*xt*yu - wt*xv*yu - wu*xt*yv + wt*xu*yv)/det;
  invJ(0, 3)= ( xv*yu*zt - xu*yv*zt - xv*yt*zu + xt*yv*zu + xu*yt*zv - xt*yu*zv)/det;

  invJ(1, 0)= ( wv*yu*zs - wu*yv*zs - wv*ys*zu + ws*yv*zu + wu*ys*zv - ws*yu*zv)/det;
  invJ(1, 1)= (-wv*xu*zs + wu*xv*zs + wv*xs*zu - ws*xv*zu - wu*xs*zv + ws*xu*zv)/det;
  invJ(1, 2)= ( wv*xu*ys - wu*xv*ys - wv*xs*yu + ws*xv*yu + wu*xs*yv - ws*xu*yv)/det;
  invJ(1, 3)= (-xv*yu*zs + xu*yv*zs + xv*ys*zu - xs*yv*zu - xu*ys*zv + xs*yu*zv)/det;

  invJ(2, 0)= (-wv*yt*zs + wt*yv*zs + wv*ys*zt - ws*yv*zt - wt*ys*zv + ws*yt*zv)/det;
  invJ(2, 1)= ( wv*xt*zs - wt*xv*zs - wv*xs*zt + ws*xv*zt + wt*xs*zv - ws*xt*zv)/det;
  invJ(2, 2)= (-wv*xt*ys + wt*xv*ys + wv*xs*yt - ws*xv*yt - wt*xs*yv + ws*xt*yv)/det;
  invJ(2, 3)= ( xv*yt*zs - xt*yv*zs - xv*ys*zt + xs*yv*zt + xt*ys*zv - xs*yt*zv)/det;

  invJ(3, 0)= ( wu*yt*zs - wt*yu*zs - wu*ys*zt + ws*yu*zt + wt*ys*zu - ws*yt*zu)/det;
  invJ(3, 1)= (-wu*xt*zs + wt*xu*zs + wu*xs*zt - ws*xu*zt - wt*xs*zu + ws*xt*zu)/det;
  invJ(3, 2)= ( wu*xt*ys - wt*xu*ys - wu*xs*yt + ws*xu*yt + wt*xs*yu - ws*xt*yu)/det;
  invJ(3, 3)= (-xu*yt*zs + xt*yu*zs + xu*ys*zt - xs*yu*zt - xt*ys*zu + xs*yt*zu)/det;

  // Populate the Hessians of the Cartesian coordinates
  xHess(0,0) = xss;
  xHess(1,0) = xst; xHess(1,1) = xtt;
  xHess(2,0) = xsu; xHess(2,1) = xtu; xHess(2,2) = xuu;
  xHess(3,0) = xsv; xHess(3,1) = xtv; xHess(3,2) = xuv; xHess(3, 3) = xvv;

  yHess(0,0) = yss;
  yHess(1,0) = yst; yHess(1,1) = ytt;
  yHess(2,0) = ysu; yHess(2,1) = ytu; yHess(2,2) = yuu;
  yHess(3,0) = ysv; yHess(3,1) = ytv; yHess(3,2) = yuv; yHess(3, 3)= yvv;

  zHess(0,0) = zss;
  zHess(1,0) = zst; zHess(1,1) = ztt;
  zHess(2,0) = zsu; zHess(2,1) = ztu; zHess(2,2) = zuu;
  zHess(3,0) = zsv; zHess(3,1) = ztv; zHess(3,2) = zuv; zHess(3, 3)= zvv;

  wHess(0,0) = wss;
  wHess(1,0) = wst; wHess(1,1) = wtt;
  wHess(2,0) = wsu; wHess(2,1) = wtu; wHess(2,2) = wuu;
  wHess(3,0) = wsv; wHess(3,1) = wtv; wHess(3,2) = wuv; wHess(3, 3)= wvv;

  tempHess1 = -(invJ(0,0)*xHess + invJ(0,1)*yHess + invJ(0,2)*zHess + invJ(0,3)*wHess);
  tempHess2 = -(invJ(1,0)*xHess + invJ(1,1)*yHess + invJ(1,2)*zHess + invJ(1,3)*wHess);
  tempHess3 = -(invJ(2,0)*xHess + invJ(2,1)*yHess + invJ(2,2)*zHess + invJ(2,3)*wHess);
  tempHess4 = -(invJ(3,0)*xHess + invJ(3,1)*yHess + invJ(3,2)*zHess + invJ(3,3)*wHess);

#if 0
  std::cout << "invJ = " << std::endl;
  std::cout << "( " << invJ(0,0) << ",  " << invJ(0,1) << " )" << std::endl;
  std::cout << "( " << invJ(1,0) << ",  " << invJ(1,1) << " )" << std::endl;

  std::cout << "tempHess1 = " << std::endl;
  std::cout << "( " << tempHess1(0,0) << ",  " << tempHess1(0,1) << " )" << std::endl;
  std::cout << "( " << tempHess1(1,0) << ",  " << tempHess1(1,1) << " )" << std::endl;

  std::cout << "tempHess2 = " << std::endl;
  std::cout << "( " << tempHess2(0,0) << ",  " << tempHess2(0,1) << " )" << std::endl;
  std::cout << "( " << tempHess2(1,0) << ",  " << tempHess2(1,1) << " )" << std::endl;
#endif

  // Calculate the transformed hessian
  shess = Transpose(invJ)*tempHess1*invJ;
  thess = Transpose(invJ)*tempHess2*invJ;
  uhess = Transpose(invJ)*tempHess3*invJ;
  vhess = Transpose(invJ)*tempHess4*invJ;

#if 0
  std::cout << "In evalReferenceCoordinateHessian:" << std::endl;
  std::cout << "det: "<< det << std::endl;
  std::cout << "xt,xs,yt,ys: " << xt << xs << yt << ys <<std::endl;
  std::cout << "shess: " << shess(0,0) << shess(1,0) << shess(1,1) << std::endl;
  std::cout << "thess: " << thess(0,0) << thess(1,0) << thess(1,1) << std::endl;
  std::cout << "tempHess1" << tempHess1(0,0) << tempHess1(1,0) << tempHess1(1,1) <<std::endl;
  std::cout << "tempHess2" << tempHess2(0,0) << tempHess2(1,0) << tempHess2(1,1) <<std::endl;
#endif

//  SANS_DEVELOPER_EXCEPTION("unit test.");

}

template class ElementXField<PhysD4, TopoD4, Pentatope>;

}
