// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ELEMENTXFIELDVOLUME_H
#define ELEMENTXFIELDVOLUME_H

// Volume (tetrahedron/hexahedron) grid field element

#include <iostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "BasisFunction/BasisFunctionVolume.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BoundingBox.h"
#include "ElementVolume.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Topologically 3-D grid field element: volume (tetrahedron/hexahedron)
//
// member functions:
//   .volume          tetrahedron volume
//   .coordinates     physical coordinates given reference triangle coordinates
//   .evalBasis       basis function
//   .evalBasisDerivative  basis function gradient wrt reference coordinates
//   .evalReferenceCoordinateGradient   reference coordinates gradient wrt (x,y,z)
//
//   .dump            debug dump of private data
//----------------------------------------------------------------------------//

template <class PhysDim, class Topology>
class ElementXField<PhysDim, TopoD3, Topology> : public Element< DLA::VectorS<PhysDim::D,Real>, TopoD3, Topology >
{
public:
  static const int D = PhysDim::D;             // physical dimensions
  static const int TopoD = TopoD3::D;          // topological dimensions
  typedef Element< DLA::VectorS<PhysDim::D,Real>, TopoD3, Topology > BaseType;
  typedef BasisFunctionVolumeBase<Topology> BasisType;
  typedef DLA::VectorS<TopoD3::D,Real> RefCoordType;

  typedef DLA::VectorS<D,Real> VectorX;        // coordinates vector
  typedef DLA::MatrixSymS<D,Real> TensorSymX;  // physical Hessian d(u)/d(x_i x_j) - e.g. implied metric
  typedef DLA::MatrixS<D,TopoD,Real> Matrix;   // element Jacobian d(x_i)/d(s_j)
  typedef DLA::VectorS<D, DLA::MatrixSymS<TopoD,Real>> TensorSymHessian; // element Hessian d^2(x_i)/d(s_j s_k)

  typedef VectorX T;

  explicit ElementXField( const BasisType* basis ) : BaseType(basis) {}
  ElementXField( int order, const BasisFunctionCategory& category ) : BaseType(order,category) {}
  ElementXField( const ElementXField& Elem ) : BaseType(Elem) {}
  ~ElementXField() {}

  ElementXField& operator=( const ElementXField& Elem ) { BaseType::operator=(Elem); return *this; }

  Real volume() const;
  Real volume( const Real& sRef, const Real& tRef, const Real& uRef ) const;
  Real volume( const QuadraturePoint<TopoD3>& sRef ) const;
  void coordinates( const Real& sRef, const Real& tRef, const Real& uRef, VectorX& X ) const { BaseType::eval(sRef,tRef,uRef,X); }
  void coordinates( const RefCoordType& Ref, VectorX& X ) const { BaseType::eval( Ref[0], Ref[1], Ref[2], X ); }
  void coordinates( const QuadraturePoint<TopoD3>& Ref, VectorX& X ) const { BaseType::eval( Ref, X ); }
  void coordinates( const QuadratureCellTracePoint<TopoD3>& Ref, VectorX& X ) const { BaseType::eval( Ref, X ); }

  BoundingBox<PhysDim> boundingBox() const;

  void jacobian( Matrix& J ) const;
  void jacobian( const RefCoordType& Ref, Matrix& J ) const { jacobian(Ref[0], Ref[1], Ref[2], J); }
  void jacobian( const QuadraturePoint<TopoD3>& Ref, Matrix& J ) const;
  void jacobian( const Real& s, const Real& t, const Real& u, Matrix& J ) const;

  void hessian( const RefCoordType& Ref, TensorSymHessian& H ) const { hessian(Ref[0], Ref[1], Ref[2], H); }
  void hessian( const Real& s, const Real& t, const Real& u, TensorSymHessian& H ) const;

  Real jacobianDeterminant() const { return jacobianDeterminant(Topology::centerRef); }
  Real jacobianDeterminant( const RefCoordType& Ref ) const { return volume(Ref[0], Ref[1], Ref[2]); }
  Real jacobianDeterminant( const QuadraturePoint<TopoD3>& Ref ) const { return volume(Ref); }

  void impliedMetric( TensorSymX& M ) const;
  void impliedMetric( const RefCoordType& Ref, TensorSymX& M ) const { impliedMetric(Ref[0], Ref[1], Ref[2], M); }
  void impliedMetric( const Real& s, const Real& t, const Real& u, TensorSymX& M ) const;

  void evalReferenceCoordinateGradient(const RefCoordType& Ref, VectorX& sgrad, VectorX& tgrad, VectorX& ugrad ) const;
  void evalReferenceCoordinateGradient(const Real& s, const Real& t, const Real& u, VectorX& sgrad, VectorX& tgrad, VectorX& ugrad ) const;
  void evalReferenceCoordinateGradient(const QuadraturePoint<TopoD3>& Ref, VectorX& sgrad, VectorX& tgrad, VectorX& ugrad ) const;
  void evalReferenceCoordinateGradient(const QuadratureCellTracePoint<TopoD3>& Ref, VectorX& sgrad, VectorX& tgrad, VectorX& ugrad ) const;

  void evalReferenceCoordinateHessian(const Real& s, const Real& t, const Real& u,
                                      TensorSymX& shess, TensorSymX& thess, TensorSymX& uhess ) const;
  void evalReferenceCoordinateHessian(const RefCoordType& Ref,
                                      TensorSymX& shess, TensorSymX& thess, TensorSymX& uhess ) const
  {
    evalReferenceCoordinateHessian(Ref[0], Ref[1], Ref[2], shess, thess, uhess);
  }
  void evalReferenceCoordinateHessian(const QuadraturePoint<TopoD3>& Ref,
                                      TensorSymX& shess, TensorSymX& thess, TensorSymX& uhess ) const
  {
    // TODO: Cache hessian
    evalReferenceCoordinateHessian(Ref.ref[0], Ref.ref[1],  Ref.ref[2],  shess, thess, uhess);
  }


  using BaseType::eval;
  using BaseType::evalBasisDerivative;

  void evalBasisGradient( const RefCoordType& sRef,
                          const ElementBasis<TopoD3, Topology>& fldElem,
                          VectorX gradphi[], const int nphi ) const;
  void evalBasisGradient( const QuadraturePoint<TopoD3>& sRef,
                          const ElementBasis<TopoD3, Topology>& fldElem,
                          VectorX gradphi[], int nphi ) const;
  void evalBasisGradient( const QuadratureCellTracePoint<TopoD3>& sRef,
                          const ElementBasis<TopoD3, Topology>& fldElem,
                          VectorX gradphi[], int nphi ) const;

  void evalBasisHessian( const RefCoordType& sRef,
                         const ElementBasis<TopoD3, Topology>& fldElem,
                         TensorSymX hessphi[], const int nphi ) const;
  void evalBasisHessian( const QuadraturePoint<TopoD3>& sRef,
                         const ElementBasis<TopoD3, Topology>& fldElem,
                         TensorSymX hessphi[], int nphi ) const;

  template< class ElemT >
  void evalGradient( const RefCoordType& sRef,
                     const Element< ElemT, TopoD3, Topology >& qfldElem,
                     DLA::VectorS<PhysDim::D, ElemT>& gradq ) const;
  template< class ElemT >
  void evalGradient( const QuadraturePoint<TopoD3>& sRef,
                     const Element< ElemT, TopoD3, Topology >& qfldElem,
                     DLA::VectorS<PhysDim::D, ElemT>& gradq ) const;
  template< class ElemT >
  void evalGradient( const QuadratureCellTracePoint<TopoD3>& sRef,
                     const Element< ElemT, TopoD3, Topology >& qfldElem,
                     DLA::VectorS<PhysDim::D, ElemT>& gradq ) const;

  template< class ElemT >
  void evalHessian( const RefCoordType& sRef,
                    const Element< ElemT, TopoD3, Topology >& qfldElem,
                    DLA::MatrixSymS<PhysDim::D, ElemT>& hessq ) const;

  template< class ElemT >
  void evalHessian( const QuadraturePoint<TopoD3>& sRef,
                    const Element< ElemT, TopoD3, Topology >& qfldElem,
                    DLA::MatrixSymS<PhysDim::D, ElemT>& hessq ) const;

  //----------------------------------------------------------------------------//
  // Specialized for 4-D grid field volume element
  //----------------------------------------------------------------------------//
  void unitNormal( const RefCoordType& sRef, VectorX& N ) const;
  void unitNormal( const QuadraturePoint<TopoD3>& sRef, VectorX& N ) const;
  void unitNormal( const QuadratureCellTracePoint<TopoD3>& sRef, VectorX& N ) const;

  void dumpTecplot( std::ostream& out = std::cout ) const;

protected:

  Real volume( const Matrix& J ) const;
  void jacobian( const Real* __restrict phis, const Real* __restrict phit, const Real* __restrict phiu, Matrix& J ) const;
  void evalReferenceCoordinateGradient( const Real* __restrict phis, const Real* __restrict phit, const Real* __restrict phiu,
                                        VectorX& sgrad, VectorX& tgrad, VectorX& ugrad ) const;

  template<class RefCoord>
  void evalBasisGradient( const RefCoord& sRef,
                          const ElementBasis<TopoD3, Topology>& fldElem,
                          VectorX gradphi[], int nphi ) const;

  template<class RefCoord>
  void evalBasisHessian( const RefCoord& sRef,
                         const ElementBasis<TopoD3, Topology>& fldElem,
                         TensorSymX hessphi[], int nphi ) const;

  template< class RefCoord, class ElemT >
  void evalGradient( const RefCoord& sRef,
                     const Element< ElemT, TopoD3, Topology >& qfldElem,
                     DLA::VectorS<PhysDim::D, ElemT>& gradq ) const;

  template <class RefCoord, class ElemT>
  void evalHessian( const RefCoord& sRef,
                    const Element<ElemT, TopoD3, Topology>& qfldElem,
                    DLA::MatrixSymS<PhysDim::D, ElemT>& hessq ) const;

  //----------------------------------------------------------------------------//
  // Specialized for 4-D grid field area element
  //----------------------------------------------------------------------------//
  void unitNormal( const Real phis[], const Real phit[], const Real phiu[], VectorX& N ) const;

  using BaseType::nDOF_;
  using BaseType::DOF_;
  using BaseType::phis_;
  using BaseType::phit_;
  using BaseType::phiu_;
  using BaseType::phiss_;
  using BaseType::phist_;
  using BaseType::phisu_;
  using BaseType::phitt_;
  using BaseType::phitu_;
  using BaseType::phiuu_;
  using BaseType::basis_;
  using BaseType::faceSign_;
  using BaseType::pointStoreCell_;
  using BaseType::pointStoreTrace_;
};

//----------------------------------------------------------------------------//
// computes the gradient of a solution
template <class PhysDim, class Topology>
template< class ElemT >
void
ElementXField<PhysDim, TopoD3, Topology>::evalGradient( const RefCoordType& sRef,
                                                        const Element< ElemT, TopoD3, Topology >& qfldElem,
                                                        DLA::VectorS<PhysDim::D, ElemT>& gradq ) const
{
  evalGradient<RefCoordType, ElemT>(sRef, qfldElem, gradq);
}

template <class PhysDim, class Topology>
template< class ElemT >
void
ElementXField<PhysDim, TopoD3, Topology>::evalGradient( const QuadraturePoint<TopoD3>& sRef,
                                                        const Element< ElemT, TopoD3, Topology >& qfldElem,
                                                        DLA::VectorS<PhysDim::D, ElemT>& gradq ) const
{
  evalGradient<QuadraturePoint<TopoD3>, ElemT>(sRef, qfldElem, gradq);
}

template <class PhysDim, class Topology>
template< class ElemT >
void
ElementXField<PhysDim, TopoD3, Topology>::evalGradient( const QuadratureCellTracePoint<TopoD3>& sRef,
                                                        const Element< ElemT, TopoD3, Topology >& qfldElem,
                                                        DLA::VectorS<PhysDim::D, ElemT>& gradq ) const
{
  evalGradient<QuadratureCellTracePoint<TopoD3>, ElemT>(sRef, qfldElem, gradq);
}

template <class PhysDim, class Topology>
template< class RefCoord, class ElemT >
void
ElementXField<PhysDim, TopoD3, Topology>::evalGradient( const RefCoord& sRef,
                                                        const Element< ElemT, TopoD3, Topology >& qfldElem,
                                                        DLA::VectorS<PhysDim::D, ElemT>& gradq ) const
{
  const int nBasis = qfldElem.nDOF();   // total solution DOFs in element
  std::vector<VectorX> gradphi(nBasis);  // Basis gradient

  evalBasisGradient( sRef, qfldElem, gradphi.data(), nBasis );
  qfldElem.evalFromBasis( gradphi.data(), nBasis, gradq );
}

//----------------------------------------------------------------------------//
// computes the Hessian of a solution

template <class PhysDim, class Topology>
template <class ElemT >
void
ElementXField<PhysDim, TopoD3, Topology>::evalHessian( const RefCoordType& sRef,
                                                       const Element< ElemT, TopoD3, Topology >& qfldElem,
                                                       DLA::MatrixSymS<PhysDim::D, ElemT>& hessq ) const
{
  evalHessian<RefCoordType, ElemT>(sRef, qfldElem, hessq);
}

template <class PhysDim, class Topology>
template <class ElemT >
void
ElementXField<PhysDim, TopoD3, Topology>::evalHessian( const QuadraturePoint<TopoD3>& sRef,
                                                       const Element< ElemT, TopoD3, Topology >& qfldElem,
                                                       DLA::MatrixSymS<PhysDim::D, ElemT>& hessq ) const
{
  evalHessian<QuadraturePoint<TopoD3>, ElemT>(sRef, qfldElem, hessq);
}

template <class PhysDim, class Topology>
template <class RefCoord, class ElemT >
void
ElementXField<PhysDim, TopoD3, Topology>::evalHessian( const RefCoord& sRef,
                                                       const Element< ElemT, TopoD3, Topology >& qfldElem,
                                                       DLA::MatrixSymS<PhysDim::D, ElemT>& hessq ) const
{
  SANS_ASSERT( PhysDim::D == 3 ); // TODO: currently only useful for in physically 3-D space; not for manifolds

//  const BasisType* basis = qfldElem.basis();
  const int nBasis = qfldElem.nDOF();           // total solution DOFs in element
  std::vector<TensorSymX> hessphi(nBasis);      // Basis hessian

  evalBasisHessian( sRef, qfldElem, &hessphi[0], nBasis );
  qfldElem.evalFromBasis( &hessphi[0], nBasis, hessq );
}


}

#endif  // ELEMENTXFIELDVOLUME_H
