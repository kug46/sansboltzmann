// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "ElementAssociativityNode.h"
#include "tools/SANSException.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// edge associativity constructor: global numberings for element nodes/edges

// NOTE: default ctor needed for 'new []'
ElementAssociativity<TopoD0,Node>::ElementAssociativity()
{
  rank_ = -1;
  nodeList_ = -1;
  normalSignL_ = 0;
  normalSignR_ = 0;
}


ElementAssociativity<TopoD0,Node>::ElementAssociativity( int order )
{
  SANS_ASSERT( order == 0 );
  rank_ = -1;
  nodeList_ = -1;
  normalSignL_ = 0;
  normalSignR_ = 0;
}


ElementAssociativity<TopoD0,Node>::ElementAssociativity( const BasisType* basis )
{
  rank_ = -1;
  nodeList_ = -1;
  normalSignL_ = 0;
  normalSignR_ = 0;
}


ElementAssociativity<TopoD0,Node>::ElementAssociativity( const ElementAssociativity<TopoD0,Node>& a )
{
  rank_ = a.rank_;
  nodeList_ = a.nodeList_;
  normalSignL_ = a.normalSignL_;
  normalSignR_ = a.normalSignR_;
}


ElementAssociativity<TopoD0,Node>::~ElementAssociativity<TopoD0,Node>()
{
}


ElementAssociativity<TopoD0,Node>&
ElementAssociativity<TopoD0,Node>::operator=( const ElementAssociativity<TopoD0,Node>& a )
{
  if (this != &a)
  {
    rank_ = a.rank_;
    nodeList_ = a.nodeList_;
    normalSignL_ = a.normalSignL_;
    normalSignR_ = a.normalSignR_;
  }

  return *this;
}


ElementAssociativity<TopoD0,Node>&
ElementAssociativity<TopoD0,Node>::operator=( const Constructor& a )
{
  rank_ = a.rank();
  nodeList_ = a.nodeGlobal(0);
  normalSignL_ = a.normalSignL();
  normalSignR_ = a.normalSignR();

  return *this;
}

void
ElementAssociativity<TopoD0,Node>::getNodeGlobalMapping( int node[], int nnode ) const
{
  SANS_ASSERT( nnode == 1 );

  node[0] = nodeList_;
}

void
ElementAssociativity<TopoD0,Node>::getNodeGlobalMapping( std::vector<int>& node) const
{
  SANS_ASSERT( (int)node.size() == 1 );

  node[0] = nodeList_;
}


void
ElementAssociativity<TopoD0,Node>::getGlobalMapping( int map[], int ndof ) const
{
  SANS_ASSERT( ndof == 1 );

  map[0] = nodeList_;
}


void
ElementAssociativity<TopoD0,Node>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "ElementAssociativity<TopoD0>: order_ = " << 0
      << "  nNode_ = " << 1 << std::endl;

  out << indent << indent << "nodeList = ";
  out << nodeList_ << " ";
  out << std::endl;
}

}
