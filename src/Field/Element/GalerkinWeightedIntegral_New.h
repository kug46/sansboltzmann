// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef GALERKINWEIGHTEDINTEGRAL_NEW_H
#define GALERKINWEIGHTEDINTEGRAL_NEW_H

// volume/area/line element integrals: functor based where the integrands return an array of values, i.e. from weighting

#include <algorithm>
#include <type_traits>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "Quadrature/Quadrature.h"
#include "BasisFunction/Quadrature_Cache.h"
#include "Element.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Galerkin weighted integral
template<class TopoDim, class Topology, class IntegrandType1, class IntegrandType2 = std::nullptr_t,
                                                              class IntegrandType3 = std::nullptr_t>
class GalerkinWeightedIntegral_New;

//----------------------------------------------------------------------------//
// Single element integral

template<class TopoDim, class Topology, class IntegrandType>
class GalerkinWeightedIntegral_New<TopoDim, Topology, IntegrandType, std::nullptr_t, std::nullptr_t>
{
public:
  typedef QuadraturePoint<TopoDim> QuadPoint;

  GalerkinWeightedIntegral_New( const GalerkinWeightedIntegral_New& ) = delete;
  GalerkinWeightedIntegral_New& operator=( const GalerkinWeightedIntegral_New& ) = delete;

  explicit GalerkinWeightedIntegral_New(const int quadratureorder) :
    quadrature_(quadratureorder) {}

  template <class PhysDim, class IntegrandFunctor>
  void operator()( const IntegrandFunctor& fcn,
                   const ElementXField<PhysDim, TopoDim, Topology>& xfldElem,
                   IntegrandType& integrals )
  {
    Real dJ;                          // incremental Jacobian determinant
    Real weight;                      // quadrature weight

    integrals = 0.0; // initialization

    //if (! fcn.needsEvaluation())
    //  return;

    // loop over quadrature points
    const int nquad = quadrature_.nQuadrature();
    for (int iquad = 0; iquad < nquad; iquad++)
    {
      quadrature_.weight( iquad, weight );
      QuadPoint sRef = quadrature_.coordinates_cache( iquad );

      dJ = weight * xfldElem.jacobianDeterminant( sRef );

      fcn( dJ, sRef, integrals );
    }
  }

protected:
  const Quadrature<TopoDim, Topology> quadrature_;
};

#if 1 // TODO: hack for cut-cell transition of IBL

//----------------------------------------------------------------------------//
template<class TopoDim, class Topology, class IntegrandType1, class IntegrandType2 = std::nullptr_t,
                                                              class IntegrandType3 = std::nullptr_t>
class GalerkinWeightedIntegral_New_TransitionIBL;

// Single element integral
template<class TopoDim, class Topology, class IntegrandType>
class GalerkinWeightedIntegral_New_TransitionIBL<TopoDim, Topology, IntegrandType, std::nullptr_t, std::nullptr_t>
{
public:
  typedef QuadraturePoint<TopoDim> QuadPoint;

  GalerkinWeightedIntegral_New_TransitionIBL( const GalerkinWeightedIntegral_New_TransitionIBL& ) = delete;
  GalerkinWeightedIntegral_New_TransitionIBL& operator=( const GalerkinWeightedIntegral_New_TransitionIBL& ) = delete;

  explicit GalerkinWeightedIntegral_New_TransitionIBL(const int quadratureorder) :
    quadrature_(quadratureorder)
  {
    // TODO: generalize this class and remove these asssertions
    static_assert(TopoDim::D == 1, "Only TopoD1 is implemented so far.");
    static_assert((std::is_same<Line, Topology>::value), "Assume line element");
  }

  // the following operators serve different cut-cell formulations
  template <class IntegrandFunctor>
  void operator()( const IntegrandFunctor& fcn,
                   const typename IntegrandFunctor::ElementXFieldType& xfldElem,
                   IntegrandType& integralsMATCH, IntegrandType& integralsPDE ) const;

  template <class IntegrandFunctor>
  void operator()( const IntegrandFunctor& fcn,
                   const typename IntegrandFunctor::ElementXFieldType& xfldElem,
                   IntegrandType& integralsPDE ) const;

protected:
  const Quadrature<TopoDim, Topology> quadrature_;
};

//----------------------------------------------------------------------------//
template<class TopoDim, class Topology, class IntegrandType>
template <class IntegrandFunctor>
void
GalerkinWeightedIntegral_New_TransitionIBL<TopoDim, Topology, IntegrandType, std::nullptr_t, std::nullptr_t>::
operator()( const IntegrandFunctor& fcn,
            const typename IntegrandFunctor::ElementXFieldType& xfldElem,
            IntegrandType& integralsMATCH, IntegrandType& integralsPDE ) const
{
  SANS_ASSERT_MSG(xfldElem.order() == 1, "Assume linear grid element!"); // TODO: generalize

  typedef typename IntegrandFunctor::VectorX VectorX;
  typedef typename IntegrandFunctor::ElementQFieldType::T ArrayQ;

  typedef typename Scalar<ArrayQ>::type Tq; // IBL solution scalar type: Surreal or Real
  typedef typename Scalar<typename IntegrandType::node_type>::type Ti; // residual integral scalar type

  const auto& pdeIBL = fcn.getpde();
  const auto& transitionModel = pdeIBL.getTransitionModel();
  const auto& varInterpret = pdeIBL.getVarInterpreter();
  const auto& qfldElem = fcn.getqfldElem();

  // check if cut-cell transition treatment is needed for the current element

  const int nnode = Topology::NNode; // number of vertices/nodes; 2 for a line element
  const std::vector<Real> sRefNode_list = {0.0, 1.0};
  SANS_ASSERT_MSG(nnode == sRefNode_list.size(), "Incorrect number of vertices are initialized!");
#if 0 //TODO: can switch to the following method for generalization
  // method 1
  std::vector<Real> sRefNode_list
  getLagrangeNodes_Line(1, getLagrangeNodes_Line(order, s););

  // method 2
  std::vector<DLA::VectorS<TopoDim::D,Real>> sRefNodes;
  LagrangeNodes<Topology>::get(1, sRefNodes);
#endif

  std::vector<VectorX> XNode_list(nnode);
  std::vector<Tq> ntNode_list(nnode);
  std::vector<bool> isTrNode_list(nnode); // if transition happens at a node

  bool isCutCellTransitionElem = false; // whether the current element contains the transition front (which requires a cell cut)
  if ( transitionModel.isTransitionActive() )
  {
    for (int n = 0; n < nnode; ++n)
    {
      Real sRef = sRefNode_list.at(n);
      XNode_list.at(n) = xfldElem.coordinates(sRef);
      ntNode_list.at(n) = varInterpret.getnt(qfldElem.eval(sRef));
      isTrNode_list.at(n) = transitionModel.isTransition(ntNode_list.at(n), XNode_list.at(n));
    }

    // whether the current element is fully laminar or turbulent
    const bool isLamiElem = std::all_of(isTrNode_list.begin(), isTrNode_list.end(), [](bool isTr){return !isTr;});
    const bool isTurbElem = std::all_of(isTrNode_list.begin(), isTrNode_list.end(), [](bool isTr){return isTr;});

    // transition happens on the current element if it is neither fully laminar nor fully turbulent
    isCutCellTransitionElem = !(isLamiElem || isTurbElem);
  }

  // --------------------------------------------------------- //
  // compute elemental residual depending on whether transition happens
  // --------------------------------------------------------- //
  Real weight; // quadrature weight

  const int nquad = quadrature_.nQuadrature(); // number of quadrature points

  if (isCutCellTransitionElem) // cut-cell treatment
  {
    // compute forced transition location in terms of element reference coordinate sRef
    const Real sRefTr_forced = ( transitionModel.xtr() - XNode_list.at(0)[0] ) / ( XNode_list.at(1)[0] - XNode_list.at(0)[0] );
    // TODO: this formula only works for linear line grid element
    // TODO: caution against degenerate element i.e. too small x1-x0

    // compute natural transition location in terms of element reference coordinate sRef
    const Tq sRefTr_natural = ( transitionModel.ntcrit() - ntNode_list.at(0) ) / ( ntNode_list.at(1) - ntNode_list.at(0) );

    Tq sRefTr; // transition location

    Tq sRefLami, sRefTurb;
    Real sRefRef;

    Real dJsRef_Lami, dJsRef_Turb;                          // incremental Jacobian determinant

    VectorX nrm_LtoT; // unit normal vector at the transition front, pointing from laminar to turbulent

    Ti source_turbAmp = 0.0;

    if ( !transitionModel.isTransition(ntNode_list.at(0), XNode_list.at(0)) &&
          transitionModel.isTransition(ntNode_list.at(1), XNode_list.at(1)) ) // sRef=0: laminar; sRef=1: turbulent
    {
      // the actual transition happens as soon as any transition mode is triggered
      if ( !(sRefTr_natural >= 0.0 && sRefTr_natural <= 1.0) )
        sRefTr = sRefTr_forced;
      else if ( (!(sRefTr_forced >= 0.0 && sRefTr_forced <= 1.0)) ||
          (sRefTr_natural < sRefTr_forced) )
        sRefTr = sRefTr_natural;
      else
        sRefTr = sRefTr_forced;

      SANS_ASSERT_MSG((0.0 <= sRefTr || sRefTr <= 1.0), "sRefTr should be in [0, 1.0]"); // TODO: redundant

      fcn.turbElemSourceAmp(sRefTr, 1.0, source_turbAmp);

      for (int iquad = 0; iquad < nquad; ++iquad)
      {
        quadrature_.weight( iquad, weight );
        sRefRef = quadrature_.coordinate( iquad ); // TODO: not caching here

        SANS_ASSERT_MSG( sRefRef>0.0 && sRefRef<1.0, "Line quadrature points should be strictly in (0,1)");

        // integrate over laminar element segment
        sRefLami = sRefRef*sRefTr;
        dJsRef_Lami = weight * xfldElem.jacobianDeterminant( fcn.getScalarValuePOD(sRefLami) ); // w * |dX/dsRef| //TODO: assuming linear grid

        fcn.lamiElem( dJsRef_Lami, sRefLami, sRefRef, sRefTr, integralsPDE );

        // integrate over turbulent element segment
        sRefTurb = sRefTr + sRefRef*(1.0-sRefTr);
        dJsRef_Turb = weight * xfldElem.jacobianDeterminant( fcn.getScalarValuePOD(sRefTurb) ); // w * |dX/dsRef| //TODO: assuming linear grid

        fcn.turbElem( dJsRef_Turb, sRefTurb, sRefRef, (1.0-sRefTr), integralsPDE );
        fcn.turbElemIntegralAmp( dJsRef_Turb, sRefTurb, (1.0-sRefTr), source_turbAmp, integralsPDE );
      }

      // integrate over transition front (i.e. an interface between element segments)
      xfldElem.unitTangent(fcn.getScalarValuePOD(sRefTr), nrm_LtoT); // TODO: independent of sRefTr if assuming linear grid element
      fcn.transitionFront(nrm_LtoT, sRefTr, 1.0, 0.0, integralsMATCH, integralsPDE);
    }
    else if ( transitionModel.isTransition(ntNode_list.at(0), XNode_list.at(0)) &&
             !transitionModel.isTransition(ntNode_list.at(1), XNode_list.at(1)) ) // sRef=0: turbulent; sRef=1: laminar
    {
      // the actual transition happens as soon as any transition mode is triggered
      if ( !(sRefTr_natural >= 0.0 && sRefTr_natural <= 1.0) )
        sRefTr = sRefTr_forced;
      else if ( (!(sRefTr_forced >= 0.0 && sRefTr_forced <= 1.0)) ||
          (sRefTr_natural > sRefTr_forced) )
        sRefTr = sRefTr_natural;
      else
        sRefTr = sRefTr_forced;

      SANS_ASSERT_MSG((0.0 <= sRefTr || sRefTr <= 1.0), "sRefTr should be in [0, 1.0]"); // TODO: redundant

      fcn.turbElemSourceAmp(sRefTr, 0.0, source_turbAmp);

      for (int iquad = 0; iquad < nquad; ++iquad)
      {
        quadrature_.weight( iquad, weight );
        sRefRef = quadrature_.coordinate( iquad ); // TODO: not caching here

        SANS_ASSERT_MSG( sRefRef>0.0 && sRefRef<1.0, "Line quadrature points should be strictly in (0,1)");

        // integrate over laminar element segment
        sRefLami = sRefTr + sRefRef*(1.0-sRefTr);
        dJsRef_Lami = weight * xfldElem.jacobianDeterminant( fcn.getScalarValuePOD(sRefLami) ); //TODO: assuming linear grid

        fcn.lamiElem( dJsRef_Lami, sRefLami, sRefRef, (1.0-sRefTr), integralsPDE );

        // integrate over turbulent element segment
        sRefTurb = sRefRef*sRefTr;
        dJsRef_Turb = weight * xfldElem.jacobianDeterminant( fcn.getScalarValuePOD(sRefTurb) ); //TODO: assuming linear grid

        fcn.turbElem( dJsRef_Turb, sRefTurb, sRefRef, sRefTr, integralsPDE );
        fcn.turbElemIntegralAmp( dJsRef_Turb, sRefTurb, sRefTr, source_turbAmp, integralsPDE );
      }

      // integrate over transition front (i.e. an interface between element segments)
      xfldElem.unitTangent(fcn.getScalarValuePOD(sRefTr), nrm_LtoT); // TODO: independent of sRefTr if assuming linear grid element
      nrm_LtoT = -nrm_LtoT;
      fcn.transitionFront(nrm_LtoT, sRefTr, 0.0, 1.0, integralsMATCH, integralsPDE);
    }
  }
  else // when transition is not in this element, reverts back to regular GalerkinWeightedIntegral
  {
    Real dJsRefRef; // incremental Jacobian determinant

    for (int iquad = 0; iquad < nquad; ++iquad)
    {
      quadrature_.weight( iquad, weight );

      //        QuadPoint sRef = quadrature_.coordinates_cache( iquad ); //TODO: not caching
      Real sRef = quadrature_.coordinate( iquad );

      dJsRefRef = weight * xfldElem.jacobianDeterminant( sRef );

      fcn( dJsRefRef, sRef, sRef, 1.0, integralsPDE );

#if 1 // TODO: hack to keep nt to grow
      const bool isTurbElem = std::all_of(isTrNode_list.begin(), isTrNode_list.end(), [](bool isTr){return isTr;});

      if (isTurbElem)
      {
//        Ti source_turbAmp = 0.0;
//        Ti source_turbAmp = -2.94;
//        Ti source_turbAmp = -2.94*20.0;
        Ti source_turbAmp = -2.94*100.0;
        fcn.turbElemIntegralAmp( dJsRefRef, sRef, 1.0, source_turbAmp, integralsPDE );
      }
#endif
    }

    fcn.nonTransitionDummyMatch(integralsMATCH);
  }
}

//----------------------------------------------------------------------------//
template<class TopoDim, class Topology, class IntegrandType>
template <class IntegrandFunctor>
void
GalerkinWeightedIntegral_New_TransitionIBL<TopoDim, Topology, IntegrandType, std::nullptr_t, std::nullptr_t>::
operator()( const IntegrandFunctor& fcn,
            const typename IntegrandFunctor::ElementXFieldType& xfldElem,
            IntegrandType& integralsPDE ) const
{
  SANS_ASSERT_MSG(xfldElem.order() == 1, "Assume linear grid element!"); // TODO: generalize

  typedef typename IntegrandFunctor::VectorX VectorX;
  typedef typename IntegrandFunctor::ElementQFieldType::T ArrayQ;

  typedef typename Scalar<ArrayQ>::type Tq; // IBL solution scalar type: Surreal or Real

  const auto& pdeIBL = fcn.getpde();
  const auto& transitionModel = pdeIBL.getTransitionModel();
  const auto& varInterpret = pdeIBL.getVarInterpreter();
  const auto& qfldElem = fcn.getqfldElem();

  // check if cut-cell transition treatment is needed for the current element

  const int nnode = Topology::NNode; // number of vertices/nodes; 2 for a line element
  const std::vector<Real> sRefNode_list = {0.0, 1.0};
  SANS_ASSERT_MSG(nnode == sRefNode_list.size(), "Incorrect number of vertices are initialized!");

  std::vector<VectorX> XNode_list(nnode);
  std::vector<Tq> ntNode_list(nnode);
  std::vector<bool> isTrNode_list(nnode); // if transition happens at a node

  bool isCutCellTransitionElem = false; // whether the current element contains the transition front (which requires a cell cut)
  if ( transitionModel.isTransitionActive() )
  {
    for (int n = 0; n < nnode; ++n)
    {
      Real sRef = sRefNode_list.at(n);
      XNode_list.at(n) = xfldElem.coordinates(sRef);
      ntNode_list.at(n) = pdeIBL.getnt(qfldElem.eval(sRef));
      isTrNode_list.at(n) = transitionModel.isTransition(ntNode_list.at(n), XNode_list.at(n));
    }

    // whether the current element is fully laminar or turbulent
    const bool isLamiElem = std::all_of(isTrNode_list.begin(), isTrNode_list.end(), [](bool isTr){return !isTr;});
    const bool isTurbElem = std::all_of(isTrNode_list.begin(), isTrNode_list.end(), [](bool isTr){return isTr;});

    // transition happens on the current element if it is neither fully laminar nor fully turbulent
    isCutCellTransitionElem = !(isLamiElem || isTurbElem);
  }

  // --------------------------------------------------------- //
  // compute elemental residual depending on whether transition happens
  // --------------------------------------------------------- //
  Real weight; // quadrature weight

  const int nquad = quadrature_.nQuadrature(); // number of quadrature points

  if (isCutCellTransitionElem) // cut-cell treatment
  {
    // compute forced transition location in terms of element reference coordinate sRef
    const Real sRefTr_forced = ( transitionModel.xtr() - XNode_list.at(0)[0] ) / ( XNode_list.at(1)[0] - XNode_list.at(0)[0] );
    // TODO: this formula only works for linear line grid element
    // TODO: caution against degenerate element i.e. too small x1-x0

    // compute natural transition location in terms of element reference coordinate sRef
    const Tq sRefTr_natural = ( transitionModel.ntcrit() - ntNode_list.at(0) ) / ( ntNode_list.at(1) - ntNode_list.at(0) );
    //TODO: to be fixed for floating point exception, i.e. division by zero
    
    Tq sRefTr; // transition location

    Tq sRefLami, sRefTurb;
    Real sRefRef;

    Real dJsRef_Lami, dJsRef_Turb;                          // incremental Jacobian determinant

    VectorX nrm_LtoT; // unit normal vector at the transition front, pointing from laminar to turbulent

    if ( !transitionModel.isTransition(ntNode_list.at(0), XNode_list.at(0)) &&
        transitionModel.isTransition(ntNode_list.at(1), XNode_list.at(1)) ) // sRef=0: laminar; sRef=1: turbulent
    {
      // the actual transition happens as soon as any transition mode is triggered
      if ( !(sRefTr_natural >= 0.0 && sRefTr_natural <= 1.0) )
        sRefTr = sRefTr_forced;
      else if ( (!(sRefTr_forced >= 0.0 && sRefTr_forced <= 1.0)) ||
          (sRefTr_natural < sRefTr_forced) )
        sRefTr = sRefTr_natural;
      else
        sRefTr = sRefTr_forced;

      SANS_ASSERT_MSG((0.0 <= sRefTr || sRefTr <= 1.0), "sRefTr should be in [0, 1.0]"); // TODO: redundant

      for (int iquad = 0; iquad < nquad; ++iquad)
      {
        quadrature_.weight( iquad, weight );
        sRefRef = quadrature_.coordinate( iquad ); // TODO: not caching here

        SANS_ASSERT_MSG( sRefRef>0.0 && sRefRef<1.0, "Line quadrature points should be strictly in (0,1)");

        // integrate over laminar element segment
        sRefLami = sRefRef*sRefTr;
        dJsRef_Lami = weight * xfldElem.jacobianDeterminant( fcn.getScalarValuePOD(sRefLami) ); // w * |dX/dsRef| //TODO: assuming linear grid

        fcn( dJsRef_Lami, sRefLami, sRefTr, integralsPDE );

        // integrate over turbulent element segment
        sRefTurb = sRefTr + sRefRef*(1.0-sRefTr);
        dJsRef_Turb = weight * xfldElem.jacobianDeterminant( fcn.getScalarValuePOD(sRefTurb) ); // w * |dX/dsRef| //TODO: assuming linear grid

        fcn( dJsRef_Turb, sRefTurb, (1.0-sRefTr), integralsPDE );
      }

#if 1 //todo: not needed for now
      // integrate over transition front (i.e. an interface between element segments)
      xfldElem.unitTangent(fcn.getScalarValuePOD(sRefTr), nrm_LtoT); // TODO: independent of sRefTr if assuming linear grid element
      fcn.transitionFront(nrm_LtoT, sRefTr, integralsPDE);
#endif
    }
    else if ( transitionModel.isTransition(ntNode_list.at(0), XNode_list.at(0)) &&
             !transitionModel.isTransition(ntNode_list.at(1), XNode_list.at(1)) ) // sRef=0: turbulent; sRef=1: laminar
    {
      // the actual transition happens as soon as any transition mode is triggered
      if ( !(sRefTr_natural >= 0.0 && sRefTr_natural <= 1.0) )
        sRefTr = sRefTr_forced;
      else if ( (!(sRefTr_forced >= 0.0 && sRefTr_forced <= 1.0)) ||
          (sRefTr_natural > sRefTr_forced) )
        sRefTr = sRefTr_natural;
      else
        sRefTr = sRefTr_forced;

      SANS_ASSERT_MSG((0.0 <= sRefTr || sRefTr <= 1.0), "sRefTr should be in [0, 1.0]"); // TODO: redundant

      for (int iquad = 0; iquad < nquad; ++iquad)
      {
        quadrature_.weight( iquad, weight );
        sRefRef = quadrature_.coordinate( iquad ); // TODO: not caching here

        SANS_ASSERT_MSG( sRefRef>0.0 && sRefRef<1.0, "Line quadrature points should be strictly in (0,1)");

        // integrate over laminar element segment
        sRefLami = sRefTr + sRefRef*(1.0-sRefTr);
        dJsRef_Lami = weight * xfldElem.jacobianDeterminant( fcn.getScalarValuePOD(sRefLami) ); //TODO: assuming linear grid

        fcn( dJsRef_Lami, sRefLami, (1.0-sRefTr), integralsPDE );

        // integrate over turbulent element segment
        sRefTurb = sRefRef*sRefTr;
        dJsRef_Turb = weight * xfldElem.jacobianDeterminant( fcn.getScalarValuePOD(sRefTurb) ); //TODO: assuming linear grid

        fcn( dJsRef_Turb, sRefTurb, sRefTr, integralsPDE );
      }

#if 1 //todo: not needed for now
      // integrate over transition front (i.e. an interface between element segments)
      xfldElem.unitTangent(fcn.getScalarValuePOD(sRefTr), nrm_LtoT); // TODO: independent of sRefTr if assuming linear grid element
      nrm_LtoT = -nrm_LtoT;
      fcn.transitionFront(nrm_LtoT, sRefTr, integralsPDE);
#endif
    }
  }
  else // when transition is not in this element, reverts back to regular GalerkinWeightedIntegral
  {
    Real dJsRefRef; // incremental Jacobian determinant

    for (int iquad = 0; iquad < nquad; ++iquad)
    {
      quadrature_.weight( iquad, weight );

      //        QuadPoint sRef = quadrature_.coordinates_cache( iquad ); //TODO: not caching
      Real sRef = quadrature_.coordinate( iquad );

      dJsRefRef = weight * xfldElem.jacobianDeterminant( sRef );

      fcn( dJsRefRef, sRef, 1.0, integralsPDE );
    }
  }
}

#endif

//----------------------------------------------------------------------------//
// Integral for two elements (e.g. DG trace integral)

template<class TopoDim, class Topology, class IntegrandType1, class IntegrandType2>
class GalerkinWeightedIntegral_New<TopoDim, Topology, IntegrandType1, IntegrandType2, std::nullptr_t>
{
public:
  typedef QuadraturePoint<TopoDim> QuadPoint;

  GalerkinWeightedIntegral_New( const GalerkinWeightedIntegral_New& ) = delete;
  GalerkinWeightedIntegral_New& operator=( const GalerkinWeightedIntegral_New& ) = delete;

  explicit GalerkinWeightedIntegral_New(const int quadratureorder) :
    quadrature_(quadratureorder) {}

  template <class PhysDim, class IntegrandFunctor>
  void operator()( const IntegrandFunctor& fcn,
                   const ElementXField<PhysDim, TopoDim, Topology>& xfldTrace,
                   IntegrandType1& integrals1,
                   IntegrandType2& integrals2 )
  {
    Real dJ;                          // incremental Jacobian determinant
    Real weight;                      // quadrature weight

    integrals1 = 0;
    integrals2 = 0;

    //if (! fcn.needsEvaluation())
    //  return;

    // loop over quadrature points
    const int nquad = quadrature_.nQuadrature();
    for (int iquad = 0; iquad < nquad; iquad++)
    {
      quadrature_.weight( iquad, weight );
      QuadPoint sRef = quadrature_.coordinates_cache( iquad );

      dJ = weight * xfldTrace.jacobianDeterminant( sRef );

      fcn( dJ, sRef, integrals1, integrals2 );
    }
  }

protected:
  const Quadrature<TopoDim, Topology> quadrature_;
};

//----------------------------------------------------------------------------//
// Integral for three elements (e.g. HDG trace integral)

template<class TopoDim, class Topology, class IntegrandType1, class IntegrandType2, class IntegrandType3>
class GalerkinWeightedIntegral_New
{
public:
  typedef QuadraturePoint<TopoDim> QuadPoint;

  GalerkinWeightedIntegral_New( const GalerkinWeightedIntegral_New& ) = delete;
  GalerkinWeightedIntegral_New& operator=( const GalerkinWeightedIntegral_New& ) = delete;

  explicit GalerkinWeightedIntegral_New(const int quadratureorder) :
    quadrature_(quadratureorder) {}

  template <class PhysDim, class IntegrandFunctor>
  void operator()( const IntegrandFunctor& fcn,
                   const ElementXField<PhysDim, TopoDim, Topology>& xfldTrace,
                   IntegrandType1& integrals1,
                   IntegrandType2& integrals2,
                   IntegrandType3& integrals3 )
  {
    Real dJ;                          // incremental Jacobian determinant
    Real weight;                      // quadrature weight

    integrals1 = 0.0;
    integrals2 = 0.0;
    integrals3 = 0.0;

    //if (! fcn.needsEvaluation())
    //  return;

    // loop over quadrature points
    const int nquad = quadrature_.nQuadrature();
    for (int iquad = 0; iquad < nquad; iquad++)
    {
      quadrature_.weight( iquad, weight );
      QuadPoint sRef = quadrature_.coordinates_cache( iquad );

      dJ = weight * xfldTrace.jacobianDeterminant( sRef );

      fcn( dJ, sRef, integrals1, integrals2, integrals3 );
    }
  }

protected:
  const Quadrature<TopoDim, Topology> quadrature_;
};

}

#endif //GALERKINWEIGHTEDINTEGRAL_NEW_H
