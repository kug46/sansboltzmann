// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ELEMENTXFIELDSPACETIME_H
#define ELEMENTXFIELDSPACETIME_H

// Spacetime (pentatope) grid field element

#include <iostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "BasisFunction/BasisFunctionSpacetime.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BoundingBox.h"
#include "ElementSpacetime.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Topologically 4-D grid field element: spaceime (pentatope)
//
// member functions:
//   .volume          pentatope volume
//   .coordinates     physical coordinates given reference triangle coordinates
//   .evalBasis       basis function
//   .evalBasisDerivative  basis function gradient wrt reference coordinates
//   .evalReferenceCoordinateGradient   reference coordinates gradient wrt (x,y,z)
//
//   .dump            debug dump of private data
//----------------------------------------------------------------------------//

template <class PhysDim, class Topology>
class ElementXField<PhysDim, TopoD4, Topology> : public Element< DLA::VectorS<PhysDim::D,Real>, TopoD4, Topology >
{
public:
  static const int D = PhysDim::D;             // physical dimensions
  static const int TopoD = TopoD4::D;          // topological dimensions
  typedef Element< DLA::VectorS<PhysDim::D,Real>, TopoD4, Topology > BaseType;
  typedef BasisFunctionSpacetimeBase<Topology> BasisType;
  typedef DLA::VectorS<TopoD4::D,Real> RefCoordType;

  typedef DLA::VectorS<D,Real> VectorX;        // coordinates vector
  typedef DLA::MatrixSymS<D,Real> TensorSymX;  // physical Hessian d(u)/d(x_i x_j) - e.g. implied metric
  typedef DLA::MatrixS<D,TopoD,Real> Matrix;   // element Jacobian d(x_i)/d(s_j)
  typedef DLA::VectorS<D, DLA::MatrixSymS<TopoD,Real>> TensorSymHessian; // element Hessian d^2(x_i)/d(s_j s_k)

  typedef VectorX T;

  explicit ElementXField( const BasisType* basis ) : BaseType(basis) {}
  ElementXField( int order, const BasisFunctionCategory& category ) : BaseType(order,category) {}
  ElementXField( const ElementXField& Elem ) : BaseType(Elem) {}
  ~ElementXField() {}

  ElementXField& operator=( const ElementXField& Elem ) { BaseType::operator=(Elem); return *this; }

  Real volume() const;
  Real volume( const Real& sRef, const Real& tRef, const Real& uRef, const Real& vRef) const;
  Real volume( const QuadraturePoint<TopoD4>& sRef ) const;
  void coordinates( const Real& sRef, const Real& tRef, const Real& uRef, const Real& vRef, VectorX& X ) const
    { BaseType::eval(sRef,tRef,uRef,vRef,X); }
  void coordinates( const RefCoordType& Ref, VectorX& X ) const { BaseType::eval( Ref[0], Ref[1], Ref[2], Ref[3], X ); }
  void coordinates( const QuadraturePoint<TopoD4>& Ref, VectorX& X ) const { BaseType::eval( Ref, X ); }
  void coordinates( const QuadratureCellTracePoint<TopoD4>& Ref, VectorX& X ) const { BaseType::eval( Ref, X ); }

  BoundingBox<PhysDim> boundingBox() const;

  void jacobian( Matrix& J ) const;
  void jacobian( const RefCoordType& Ref, Matrix& J ) const { jacobian(Ref[0], Ref[1], Ref[2], Ref[3], J); }
  void jacobian( const QuadraturePoint<TopoD4>& Ref, Matrix& J ) const;
  void jacobian( const Real& s, const Real& t, const Real& u, const Real& v, Matrix& J ) const;

  void hessian( const RefCoordType& Ref, TensorSymHessian& H ) const { hessian(Ref[0], Ref[1], Ref[2], Ref[3], H); }
  void hessian( const Real& s, const Real& t, const Real& u, const Real& v, TensorSymHessian& H ) const;

  Real jacobianDeterminant() const { return jacobianDeterminant(Topology::centerRef); }
  Real jacobianDeterminant( const RefCoordType& Ref ) const { return volume(Ref[0], Ref[1], Ref[2], Ref[3]); }
  Real jacobianDeterminant( const QuadraturePoint<TopoD4>& Ref ) const { return volume(Ref); }

  void impliedMetric( TensorSymX& M ) const;
  void impliedMetric( const RefCoordType& Ref, TensorSymX& M ) const { impliedMetric(Ref[0], Ref[1], Ref[2], Ref[3], M); }
  void impliedMetric( const Real& s, const Real& t, const Real& u, const Real& v, TensorSymX& M ) const;

  void evalReferenceCoordinateGradient(const RefCoordType& Ref, VectorX& sgrad, VectorX& tgrad, VectorX& ugrad, VectorX& vgrad ) const;
  void evalReferenceCoordinateGradient(const Real& s, const Real& t, const Real& u, const Real& v,
                                       VectorX& sgrad, VectorX& tgrad, VectorX& ugrad, VectorX& vgrad ) const;
  void evalReferenceCoordinateGradient(const QuadraturePoint<TopoD4>& Ref, VectorX& sgrad, VectorX& tgrad, VectorX& ugrad, VectorX& vgrad ) const;
  void evalReferenceCoordinateGradient(const QuadratureCellTracePoint<TopoD4>& Ref,
                                       VectorX& sgrad, VectorX& tgrad, VectorX& ugrad, VectorX& vgrad ) const;

  void evalReferenceCoordinateHessian(const Real& s, const Real& t, const Real& u, const Real& v,
                                      TensorSymX& shess, TensorSymX& thess, TensorSymX& uhess, TensorSymX& vhess ) const;
  void evalReferenceCoordinateHessian(const RefCoordType& Ref,
                                      TensorSymX& shess, TensorSymX& thess, TensorSymX& uhess, TensorSymX& vhess ) const
  {
    evalReferenceCoordinateHessian(Ref[0], Ref[1], Ref[2], Ref[3], shess, thess, uhess, vhess);
  }
  void evalReferenceCoordinateHessian(const QuadraturePoint<TopoD4>& Ref,
                                      TensorSymX& shess, TensorSymX& thess, TensorSymX& uhess, TensorSymX& vhess ) const
  {
    // TODO: Cache hessian
    evalReferenceCoordinateHessian(Ref.ref[0], Ref.ref[1],  Ref.ref[2], Ref.ref[3],  shess, thess, uhess, vhess);
  }


  using BaseType::eval;
  using BaseType::evalBasisDerivative;

  void evalBasisGradient( const RefCoordType& sRef,
                          const ElementBasis<TopoD4, Topology>& fldElem,
                          VectorX gradphi[], const int nphi ) const;
  void evalBasisGradient( const QuadraturePoint<TopoD4>& sRef,
                          const ElementBasis<TopoD4, Topology>& fldElem,
                          VectorX gradphi[], int nphi ) const;
  void evalBasisGradient( const QuadratureCellTracePoint<TopoD4>& sRef,
                          const ElementBasis<TopoD4, Topology>& fldElem,
                          VectorX gradphi[], int nphi ) const;

  void evalBasisHessian( const RefCoordType& sRef,
                         const ElementBasis<TopoD4, Topology>& fldElem,
                         TensorSymX hessphi[], const int nphi ) const;
  void evalBasisHessian( const QuadraturePoint<TopoD4>& sRef,
                         const ElementBasis<TopoD4, Topology>& fldElem,
                         TensorSymX hessphi[], int nphi ) const;

  template< class ElemT >
  void evalGradient( const RefCoordType& sRef,
                     const Element< ElemT, TopoD4, Topology >& qfldElem,
                     DLA::VectorS<PhysDim::D, ElemT>& gradq ) const;
  template< class ElemT >
  void evalGradient( const QuadraturePoint<TopoD4>& sRef,
                     const Element< ElemT, TopoD4, Topology >& qfldElem,
                     DLA::VectorS<PhysDim::D, ElemT>& gradq ) const;
  template< class ElemT >
  void evalGradient( const QuadratureCellTracePoint<TopoD4>& sRef,
                     const Element< ElemT, TopoD4, Topology >& qfldElem,
                     DLA::VectorS<PhysDim::D, ElemT>& gradq ) const;

  template< class ElemT >
  void evalHessian( const RefCoordType& sRef,
                    const Element< ElemT, TopoD4, Topology >& qfldElem,
                    DLA::MatrixSymS<PhysDim::D, ElemT>& hessq ) const;

  template< class ElemT >
  void evalHessian( const QuadraturePoint<TopoD4>& sRef,
                    const Element< ElemT, TopoD4, Topology >& qfldElem,
                    DLA::MatrixSymS<PhysDim::D, ElemT>& hessq ) const;

  void dumpTecplot( std::ostream& out = std::cout ) const;

protected:

  Real volume( const Matrix& J ) const;
  void jacobian( const Real* __restrict phis, const Real* __restrict phit,
                 const Real* __restrict phiu, const Real* __restrict phiv, Matrix& J ) const;
  void evalReferenceCoordinateGradient( const Real* __restrict phis, const Real* __restrict phit,
                                        const Real* __restrict phiu, const Real* __restrict phiv,
                                        VectorX& sgrad, VectorX& tgrad, VectorX& ugrad, VectorX& vgrad ) const;

  template<class RefCoord>
  void evalBasisGradient( const RefCoord& sRef,
                          const ElementBasis<TopoD4, Topology>& fldElem,
                          VectorX gradphi[], int nphi ) const;

  template<class RefCoord>
  void evalBasisHessian( const RefCoord& sRef,
                         const ElementBasis<TopoD4, Topology>& fldElem,
                         TensorSymX hessphi[], int nphi ) const;

  template< class RefCoord, class ElemT >
  void evalGradient( const RefCoord& sRef,
                     const Element< ElemT, TopoD4, Topology >& qfldElem,
                     DLA::VectorS<PhysDim::D, ElemT>& gradq ) const;

  template <class RefCoord, class ElemT>
  void evalHessian( const RefCoord& sRef,
                    const Element<ElemT, TopoD4, Topology>& qfldElem,
                    DLA::MatrixSymS<PhysDim::D, ElemT>& hessq ) const;

  using BaseType::nDOF_;
  using BaseType::DOF_;
  using BaseType::phis_;
  using BaseType::phit_;
  using BaseType::phiu_;
  using BaseType::phiv_;
  using BaseType::phiss_;
  using BaseType::phist_;
  using BaseType::phisu_;
  using BaseType::phitt_;
  using BaseType::phitu_;
  using BaseType::phiuu_;
  using BaseType::phisv_;
  using BaseType::phitv_;
  using BaseType::phiuv_;
  using BaseType::phivv_;
  using BaseType::basis_;
  using BaseType::faceSign_;
  using BaseType::pointStoreCell_;
  using BaseType::pointStoreTrace_;
};

//----------------------------------------------------------------------------//
// computes the gradient of a solution
template <class PhysDim, class Topology>
template< class ElemT >
void
ElementXField<PhysDim, TopoD4, Topology>::evalGradient( const RefCoordType& sRef,
                                                        const Element< ElemT, TopoD4, Topology >& qfldElem,
                                                        DLA::VectorS<PhysDim::D, ElemT>& gradq ) const
{
  evalGradient<RefCoordType, ElemT>(sRef, qfldElem, gradq);
}

template <class PhysDim, class Topology>
template< class ElemT >
void
ElementXField<PhysDim, TopoD4, Topology>::evalGradient( const QuadraturePoint<TopoD4>& sRef,
                                                        const Element< ElemT, TopoD4, Topology >& qfldElem,
                                                        DLA::VectorS<PhysDim::D, ElemT>& gradq ) const
{
  evalGradient<QuadraturePoint<TopoD4>, ElemT>(sRef, qfldElem, gradq);
}

template <class PhysDim, class Topology>
template< class ElemT >
void
ElementXField<PhysDim, TopoD4, Topology>::evalGradient( const QuadratureCellTracePoint<TopoD4>& sRef,
                                                        const Element< ElemT, TopoD4, Topology >& qfldElem,
                                                        DLA::VectorS<PhysDim::D, ElemT>& gradq ) const
{
  evalGradient<QuadratureCellTracePoint<TopoD4>, ElemT>(sRef, qfldElem, gradq);
}

template <class PhysDim, class Topology>
template< class RefCoord, class ElemT >
void
ElementXField<PhysDim, TopoD4, Topology>::evalGradient( const RefCoord& sRef,
                                                        const Element< ElemT, TopoD4, Topology >& qfldElem,
                                                        DLA::VectorS<PhysDim::D, ElemT>& gradq ) const
{
  const int nBasis = qfldElem.nDOF();   // total solution DOFs in element
  std::vector<VectorX> gradphi(nBasis);  // Basis gradient

  evalBasisGradient( sRef, qfldElem, gradphi.data(), nBasis );
  qfldElem.evalFromBasis( gradphi.data(), nBasis, gradq );
}

//----------------------------------------------------------------------------//
// computes the Hessian of a solution

template <class PhysDim, class Topology>
template <class ElemT >
void
ElementXField<PhysDim, TopoD4, Topology>::evalHessian( const RefCoordType& sRef,
                                                       const Element< ElemT, TopoD4, Topology >& qfldElem,
                                                       DLA::MatrixSymS<PhysDim::D, ElemT>& hessq ) const
{
  evalHessian<RefCoordType, ElemT>(sRef, qfldElem, hessq);
}

template <class PhysDim, class Topology>
template <class ElemT >
void
ElementXField<PhysDim, TopoD4, Topology>::evalHessian( const QuadraturePoint<TopoD4>& sRef,
                                                       const Element< ElemT, TopoD4, Topology >& qfldElem,
                                                       DLA::MatrixSymS<PhysDim::D, ElemT>& hessq ) const
{
  evalHessian<QuadraturePoint<TopoD4>, ElemT>(sRef, qfldElem, hessq);
}

template <class PhysDim, class Topology>
template <class RefCoord, class ElemT >
void
ElementXField<PhysDim, TopoD4, Topology>::evalHessian( const RefCoord& sRef,
                                                       const Element< ElemT, TopoD4, Topology >& qfldElem,
                                                       DLA::MatrixSymS<PhysDim::D, ElemT>& hessq ) const
{
  SANS_ASSERT( PhysDim::D == 4 ); // TODO: currently only useful for in physically 4-D space; not for manifolds

//  const BasisType* basis = qfldElem.basis();
  const int nBasis = qfldElem.nDOF();           // total solution DOFs in element
  std::vector<TensorSymX> hessphi(nBasis);      // Basis hessian

  evalBasisHessian( sRef, qfldElem, &hessphi[0], nBasis );
  qfldElem.evalFromBasis( &hessphi[0], nBasis, hessq );
}


}

#endif  // ELEMENTXFIELDSPACETIME_H
