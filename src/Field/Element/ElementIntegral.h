// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ELEMENTINTEGRAL_H
#define ELEMENTINTEGRAL_H

// volume/area/line element integrals: functor based where a single scalar is evaluated in the integral

#include <type_traits>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "Quadrature/Quadrature.h"
#include "BasisFunction/Quadrature_Cache.h"
#include "Element.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Element weighted integral
template<class TopoDim, class Topology, class IntegrandCellLType, class IntegrandCellRType = std::nullptr_t,
                                                                  class IntegrandTraceType = std::nullptr_t>
class ElementIntegral;

//----------------------------------------------------------------------------//
// Element integral with a single value

template<class TopoDim, class Topology, class IntegrandType>
class ElementIntegral<TopoDim, Topology, IntegrandType, std::nullptr_t, std::nullptr_t>
{
public:
  typedef QuadraturePoint<TopoDim> QuadPointType;

  ElementIntegral( const ElementIntegral& ) = delete;
  ElementIntegral& operator=( const ElementIntegral& ) = delete;

  explicit ElementIntegral(const int quadratureorder) :
    quadrature_(quadratureorder) {}

  template <class PhysDim, class IntegrandFunctor, class IntegralT>
  void operator()( const IntegrandFunctor& fcn,
                   const ElementXField<PhysDim, TopoDim, Topology>& xfldElem,
                   IntegralT& integral )
  {
    Real dJ;           // incremental Jacobian determinant
    Real weight;       // quadrature weight
    IntegrandType integrand;

    integral = 0;

    //if (! fcn.needsEvaluation())
    //  return;

    // loop over quadrature points
    const int nquad = quadrature_.nQuadrature();

    for (int iquad = 0; iquad < nquad; iquad++)
    {
      quadrature_.weight( iquad, weight );
      QuadPointType sRef = quadrature_.coordinates_cache( iquad );

      dJ = weight * xfldElem.jacobianDeterminant( sRef );

      fcn( sRef, integrand );

      integral += dJ*integrand;
    }

  }

protected:
  const Quadrature<TopoDim, Topology> quadrature_;
};

//----------------------------------------------------------------------------//
// Element integral with a two values, i.e. trace integral between two cells

template<class TopoDim, class Topology, class IntegrandCellLType, class IntegrandCellRType>
class ElementIntegral<TopoDim, Topology, IntegrandCellLType, IntegrandCellRType, std::nullptr_t>
{
public:
  typedef QuadraturePoint<TopoDim> QuadPointType;

  ElementIntegral( const ElementIntegral& ) = delete;
  ElementIntegral& operator=( const ElementIntegral& ) = delete;

  explicit ElementIntegral(const int quadratureorder) :
    quadrature_(quadratureorder) {}

  template <class PhysDim, class IntegrandFunctor, class IntegralTL, class IntegralTR>
  void operator()( const IntegrandFunctor& fcn,
                   const ElementXField<PhysDim, TopoDim, Topology>& xfldElem,
                   IntegralTL& integralL,
                   IntegralTR& integralR )
  {
    Real dJ;           // incremental Jacobian determinant
    Real weight;       // quadrature weight
    IntegrandCellLType integrandL;
    IntegrandCellRType integrandR;

    integralL = 0;
    integralR = 0;

    //if (! fcn.needsEvaluation())
    //  return;

    // loop over quadrature points
    const int nquad = quadrature_.nQuadrature();
    for (int iquad = 0; iquad < nquad; iquad++)
    {
      quadrature_.weight( iquad, weight );
      QuadPointType sRef = quadrature_.coordinates_cache( iquad );

      dJ = weight * xfldElem.jacobianDeterminant( sRef );

      fcn( sRef, integrandL, integrandR );

      integralL += dJ*integrandL;
      integralR += dJ*integrandR;
    }
  }

protected:
  const Quadrature<TopoDim, Topology> quadrature_;
};

//----------------------------------------------------------------------------//
// Element integral with a three values, i.e. trace integral along with two cells

template<class TopoDim, class Topology, class IntegrandCellLType, class IntegrandCellRType, class IntegrandTraceType>
class ElementIntegral
{
public:
  typedef QuadraturePoint<TopoDim> QuadPointType;

  ElementIntegral( const ElementIntegral& ) = delete;
  ElementIntegral& operator=( const ElementIntegral& ) = delete;

  explicit ElementIntegral(const int quadratureorder) :
    quadrature_(quadratureorder) {}

  template <class PhysDim, class IntegrandFunctor, class IntegralTL, class IntegralTR, class IntegralTTrace>
  void operator()( const IntegrandFunctor& fcn,
                   const ElementXField<PhysDim, TopoDim, Topology>& xfldElem,
                   IntegralTL& integralL,
                   IntegralTR& integralR,
                   IntegralTTrace& integralTrace )
  {
    Real dJ;           // incremental Jacobian determinant
    Real weight;       // quadrature weight
    IntegrandCellLType integrandL;
    IntegrandCellRType integrandR;
    IntegrandTraceType integrandTrace;

    integralL = 0;
    integralR = 0;
    integralTrace = 0;

    //if (! fcn.needsEvaluation())
    //  return;

    // loop over quadrature points
    const int nquad = quadrature_.nQuadrature();
    for (int iquad = 0; iquad < nquad; iquad++)
    {
      quadrature_.weight( iquad, weight );
      QuadPointType sRef = quadrature_.coordinates_cache( iquad );

      dJ = weight * xfldElem.jacobianDeterminant( sRef );

      fcn( sRef, integrandL, integrandR, integrandTrace );

      integralL     += dJ*integrandL;
      integralR     += dJ*integrandR;
      integralTrace += dJ*integrandTrace;
    }
  }

protected:
  const Quadrature<TopoDim, Topology> quadrature_;
};

}

#endif  // ELEMENTINTEGRAL_H
