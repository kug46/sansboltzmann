// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef REFCOORDSOLVER_H_
#define REFCOORDSOLVER_H_

#include "Topology/ElementTopology.h"
#include "Field/Element/Element.h"
#include "Field/Element/ElementXFieldArea.h"

namespace SANS
{

/*
 * A class for solving for the reference coordinates that map to a given physical coordinate
 */

template<class PhysDim, class TopoDim, class Topology>
class RefCoordSolver;

template<class PhysDim, class TopoDim, class Topology>
class RefCoordSolver
{
public:

  static const int D = PhysDim::D;
  static const int TopoD = TopoDim::D;

  typedef ElementXField<PhysDim, TopoDim, Topology> ElementXType;
  typedef typename ElementXType::VectorX VectorX;
  typedef typename ElementXType::TensorSymX TensorSymX;
  typedef typename ElementXType::Matrix Matrix;
  typedef typename ElementXType::TensorSymHessian Hessian;
  typedef typename ElementXType::RefCoordType RefCoordType;

  typedef DLA::MatrixSymS<TopoD,Real> MatrixJac;

  explicit RefCoordSolver(const ElementXType& xfldElem) : xfldElem_(xfldElem) {}

  ~RefCoordSolver(){};

  bool solve(const VectorX& X0, RefCoordType& sRef_sol);

protected:

  void residual(const RefCoordType& sRef, const VectorX& X0, RefCoordType& res );
  void jacobian(const RefCoordType& sRef, const VectorX& X0, MatrixJac& jac);

  bool linesearch(const RefCoordType& sRef0, const RefCoordType& dsRef, RefCoordType& sRefnew);

  const ElementXType& xfldElem_;
};

//Special case for nodes
template<class PhysDim>
class RefCoordSolver<PhysDim, TopoD0, Node>
{
public:

  static const int D = PhysDim::D;

  typedef ElementXField<PhysDim, TopoD0, Node> ElementXType;
  typedef typename ElementXType::VectorX VectorX;
  typedef typename ElementXType::RefCoordType RefCoordType;

  explicit RefCoordSolver(const ElementXType& xfldElem) : xfldElem_(xfldElem) {}

  bool solve(const VectorX& X0, RefCoordType& sRef_sol);

protected:
  const ElementXType& xfldElem_;
};

}

#endif /* REFCOORDSOLVER_H_ */
