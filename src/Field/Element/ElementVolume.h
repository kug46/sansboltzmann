// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ELEMENTVOLUME_H
#define ELEMENTVOLUME_H

// Volume (tetrahedron/hexahedron) field element

#include <ostream>
#include <string>
#include <array>

#include "Element.h"
#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "BasisFunction/BasisFunctionVolume.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionVolume_Projection.h"
#include "BasisFunction/BasisPoint.h"
#include "BasisFunction/BasisPointDerivative.h"
#include "BasisFunction/Quadrature_Cache.h"
#include "Topology/Dimension.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// field element basis: volume
//
// basis function related methods
//
// template parameters:
//   Topology       element/basis topology (e.g. Tet, Hex)
//----------------------------------------------------------------------------//


template <class Topology>
class ElementBasis<TopoD3, Topology>
{
public:
  typedef TopoD3 TopoDim;
  typedef Topology TopologyType;
  typedef DLA::VectorS<TopoD3::D,Real> RefCoordType;
  typedef BasisFunctionVolumeBase<Topology> BasisType;

  typedef std::array<int,Topology::NFace> IntNFace; // edge sign arrays

  explicit ElementBasis( const BasisType* basis );
  ElementBasis( int order, const BasisFunctionCategory& category );
  ElementBasis( const ElementBasis& );
  ~ElementBasis();

  ElementBasis& operator=( const ElementBasis& );

  // basis function
  const BasisType* basis() const { return basis_; }

  int rank() const { return rank_; }
  void setRank( const int rank ) { rank_ = rank; }

  int order() const { return order_; }
  int nDOF() const { return nDOF_; }

  // face sign accessors
  const IntNFace& faceSign() const { return faceSign_; }
  const IntNFace& traceOrientation() const { return faceSign_; }

  void setFaceSign( const IntNFace& faceSign );
  void setTraceOrientation(const IntNFace& faceSign) { setFaceSign(faceSign); }

  void evalBasis( const Real& sRef, const Real& tRef, const Real& uRef, const Real **phi ) const;
  void evalBasis( const RefCoordType& Ref, const Real **phi ) const { evalBasis(Ref[0], Ref[1], Ref[2], phi); }
  void evalBasis( const QuadraturePoint<TopoDim>& ref, const Real **phi ) const;
  void evalBasis( const QuadratureCellTracePoint<TopoDim>& ref, const Real **phi ) const;


  void evalBasis( const Real& sRef, const Real& tRef, const Real& uRef, Real phi[], int nphi ) const;
  void evalBasis( const RefCoordType& Ref, Real phi[], int nphi ) const { evalBasis(Ref[0], Ref[1], Ref[2], phi, nphi); }
  void evalBasis( const QuadraturePoint<TopoDim>& ref, Real phi[], int nphi ) const;
  void evalBasis( const QuadratureCellTracePoint<TopoDim>& ref, Real phi[], int nphi ) const;
  void evalBasis( const RefCoordType& Ref, BasisPoint<TopoD3::D>& phi ) const
  {
    phi.ref() = Ref;
    evalBasis(Ref[0], Ref[1], Ref[2], phi, phi.size());
  }


  void evalBasisDerivative( const Real& sRef, const Real& tRef, const Real& uRef,
                            const Real **phis, const Real **phit, const Real **phiu ) const;
  void evalBasisDerivative( const RefCoordType& Ref, const Real **phis, const Real **phit, const Real **phiu ) const
  {
    evalBasisDerivative(Ref[0], Ref[1], Ref[2], phis, phit, phiu);
  }
  void evalBasisDerivative( const QuadraturePoint<TopoDim>& ref, const Real **phis, const Real **phit, const Real **phiu ) const;
  void evalBasisDerivative( const QuadratureCellTracePoint<TopoDim>& ref, const Real **phis, const Real **phit, const Real **phiu ) const;


  void evalBasisDerivative( const Real& sRef, const Real& tRef, const Real& uRef,
                            Real phis[], Real phit[], Real phiu[], int nphi ) const;
  void evalBasisDerivative( const RefCoordType& Ref, Real phis[], Real phit[], Real phiu[], int nphi ) const
  {
    evalBasisDerivative(Ref[0], Ref[1], Ref[2], phis, phit, phiu, nphi);
  }
  void evalBasisDerivative( const QuadraturePoint<TopoDim>& ref, Real phis[], Real phit[], Real phiu[], int nphi ) const;
  void evalBasisDerivative( const QuadratureCellTracePoint<TopoDim>& ref, Real phis[], Real phit[], Real phiu[], int nphi ) const;
  void evalBasisDerivative( const RefCoordType& Ref, BasisPointDerivative<TopoD3::D>& dphi ) const
  {
    dphi.ref() = Ref;
    evalBasisDerivative(Ref[0], Ref[1], Ref[2], dphi.deriv(0), dphi.deriv(1), dphi.deriv(2), dphi.size());
  }


  void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Real& uRef,
                                   const Real **phiss,
                                   const Real **phist, const Real **phitt,
                                   const Real **phisu, const Real **phitu, const Real **phiuu ) const;
  void evalBasisHessianDerivative( const RefCoordType& Ref,
                                   const Real **phiss,
                                   const Real **phist, const Real **phitt,
                                   const Real **phisu, const Real **phitu, const Real **phiuu ) const
  {
    evalBasisHessianDerivative(Ref[0], Ref[1], Ref[2], phiss, phist, phitt, phisu, phitu, phiuu);
  }
  void evalBasisHessianDerivative( const QuadraturePoint<TopoDim>& Ref,
                                   const Real **phiss,
                                   const Real **phist, const Real **phitt,
                                   const Real **phisu, const Real **phitu, const Real **phiuu ) const;


  void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Real& uRef,
                                   Real phiss[],
                                   Real phist[], Real phitt[],
                                   Real phisu[], Real phitu[], Real phiuu[], int nphi ) const;
  void evalBasisHessianDerivative( const RefCoordType& Ref,
                                   Real phiss[],
                                   Real phist[], Real phitt[],
                                   Real phisu[], Real phitu[], Real phiuu[], int nphi ) const
  {
    evalBasisHessianDerivative(Ref[0], Ref[1], Ref[2], phiss, phist, phitt, phisu, phitu, phiuu, nphi);
  }
  void evalBasisHessianDerivative( const QuadraturePoint<TopoDim>& Ref,
                                   Real phiss[],
                                   Real phist[], Real phitt[],
                                   Real phisu[], Real phitu[], Real phiuu[], int nphi ) const;

protected:
  ElementBasis();             // only intended as a base class

  //Used for arrays of elements
  void setBasis( const BasisType* basis );

  int rank_;                  // processor rank that possesses this element

  int order_;                 // polynomial order (e.g. order=1 is linear)
  int nDOF_;                  // total DOFs in element

  const BasisType* basis_;
  Real *phi_;                 // temporary storage for bais function evaluation
  Real *phis_, *phit_, *phiu_;
  Real *phiss_, *phist_, *phisu_,
                *phitt_, *phitu_,
                         *phiuu_;

  // cached basis functions evaluated at quadrature points
  const QuadratureBasisPointStore<TopoDim::D>* pointStoreCell_;

  // cached basis functions evaluated at quadrature points on the trace of the element
  const std::vector<QuadratureBasisPointStore<TopoDim::D>>* pointStoreTrace_;

  IntNFace faceSign_;   // +/- sign for face orientations (left/right element)

  void setCache();
};


// default ctor needed for 'new []'
template <class Topology>
ElementBasis<TopoD3, Topology>::ElementBasis()
{
  rank_  = -1;
  order_ = -1;
  nDOF_  = -1;
  basis_ = NULL;
  phi_   = NULL;
  phis_  = NULL;
  phit_  = NULL;
  phiu_  = NULL;
  phiss_ = NULL;
  phist_ = NULL;
  phisu_ = NULL;
  phitt_ = NULL;
  phitu_ = NULL;
  phiuu_ = NULL;
  pointStoreCell_ = NULL;
  pointStoreTrace_ = NULL;

  for (int n = 0; n < Topology::NFace; n++)
    faceSign_[n] = +1;
}


template <class Topology>
ElementBasis<TopoD3, Topology>::ElementBasis(
    const BasisType* basis )
{
  rank_  = -1;
  basis_ = basis;
  order_ = basis_->order();
  nDOF_  = basis_->nBasis();

  phi_   = new Real[nDOF_];
  phis_  = new Real[nDOF_];
  phit_  = new Real[nDOF_];
  phiu_  = new Real[nDOF_];
  phiss_ = new Real[nDOF_];
  phist_ = new Real[nDOF_];
  phisu_ = new Real[nDOF_];
  phitt_ = new Real[nDOF_];
  phitu_ = new Real[nDOF_];
  phiuu_ = new Real[nDOF_];

  pointStoreCell_ = NULL;
  pointStoreTrace_ = NULL;

  for (int n = 0; n < Topology::NFace; n++)
    faceSign_[n] = +1;

  // must be called after the face sign and basis is set
  setCache();
}


template <class Topology>
ElementBasis<TopoD3, Topology>::ElementBasis( int order, const BasisFunctionCategory& category )
  : basis_( BasisFunctionVolumeBase<Topology>::getBasisFunction(order, category) )
{
  rank_  = -1;
  order_ = order;
  nDOF_  = basis_->nBasis();

  phi_   = new Real[nDOF_];
  phis_  = new Real[nDOF_];
  phit_  = new Real[nDOF_];
  phiu_  = new Real[nDOF_];
  phiss_ = new Real[nDOF_];
  phist_ = new Real[nDOF_];
  phisu_ = new Real[nDOF_];
  phitt_ = new Real[nDOF_];
  phitu_ = new Real[nDOF_];
  phiuu_ = new Real[nDOF_];

  pointStoreCell_ = NULL;
  pointStoreTrace_ = NULL;

  for (int n = 0; n < Topology::NFace; n++)
    faceSign_[n] = +1;

  // must be called after the face sign and basis is set
  setCache();
}


template <class Topology>
ElementBasis<TopoD3, Topology>::ElementBasis( const ElementBasis& a )
  : basis_( a.basis_ )
{
  rank_  = a.rank_;
  order_ = a.order_;
  nDOF_  = a.nDOF_;

  phi_   = new Real[nDOF_];
  phis_  = new Real[nDOF_];
  phit_  = new Real[nDOF_];
  phiu_  = new Real[nDOF_];
  phiss_ = new Real[nDOF_];
  phist_ = new Real[nDOF_];
  phisu_ = new Real[nDOF_];
  phitt_ = new Real[nDOF_];
  phitu_ = new Real[nDOF_];
  phiuu_ = new Real[nDOF_];

  for (int n = 0; n < Topology::NFace; n++)
    faceSign_[n] = a.faceSign_[n];

  pointStoreCell_ = a.pointStoreCell_;
  pointStoreTrace_ = a.pointStoreTrace_;
}


template <class Topology>
ElementBasis<TopoD3, Topology>::~ElementBasis()
{
  delete [] phi_;
  delete [] phis_;
  delete [] phit_;
  delete [] phiu_;
  delete [] phiss_;
  delete [] phist_;
  delete [] phisu_;
  delete [] phitt_;
  delete [] phitu_;
  delete [] phiuu_;
}


template <class Topology>
void
ElementBasis<TopoD3, Topology>::setCache()
{
  // set the pointer to the specific cache based on the basis function
  // note the dependence on the edgeSign
  BasisFunctionCategory cat = basis_->category();
  if ( cat == BasisFunctionCategory_Hierarchical )
  {
    // TODO: This is hacked for now until we get face signs sorted out
    SANS_ASSERT( basis_->order() <= 2 );

    // should be hierarchicalCell[basis_->order()].at(faceSign_) when faceSign is sorted out
    pointStoreCell_  = &QuadratureCache<Topology>::cache.hierarchicalCell[basis_->order()].begin()->second;
    pointStoreTrace_ = &QuadratureCache<Topology>::cache.hierarchicalTrace[basis_->order()].begin()->second;
  }
  else
  {
    pointStoreCell_  = &QuadratureCache<Topology>::cache.cell[cat][basis_->order()];
    pointStoreTrace_ = &QuadratureCache<Topology>::cache.trace[cat][basis_->order()];
  }
}


template <class Topology>
ElementBasis<TopoD3, Topology>& // cppcheck-suppress operatorEqVarError
ElementBasis<TopoD3, Topology>::operator=( const ElementBasis& a )
{
  if (this != &a)
  {
    rank_  = a.rank_;
    order_ = a.order_;
    nDOF_  = a.nDOF_;

    delete [] phi_;   phi_   = NULL;
    delete [] phis_;  phis_  = NULL;
    delete [] phit_;  phit_  = NULL;
    delete [] phiu_;  phiu_  = NULL;
    delete [] phiss_; phiss_ = NULL;
    delete [] phist_; phist_ = NULL;
    delete [] phisu_; phisu_ = NULL;
    delete [] phitt_; phitt_ = NULL;
    delete [] phitu_; phitu_ = NULL;
    delete [] phiuu_; phiuu_ = NULL;

    phi_   = new Real[nDOF_];
    phis_  = new Real[nDOF_];
    phit_  = new Real[nDOF_];
    phiu_  = new Real[nDOF_];
    phiss_ = new Real[nDOF_];
    phist_ = new Real[nDOF_];
    phisu_ = new Real[nDOF_];
    phitt_ = new Real[nDOF_];
    phitu_ = new Real[nDOF_];
    phiuu_ = new Real[nDOF_];

    basis_ = a.basis_;

    for (int n = 0; n < Topology::NFace; n++)
      faceSign_[n] = a.faceSign_[n];

    pointStoreCell_ = a.pointStoreCell_;
    pointStoreTrace_ = a.pointStoreTrace_;
  }

  return *this;
}

template <class Topology>
void
ElementBasis<TopoD3, Topology>::setBasis(
    const BasisType* basis )
{
  delete [] phi_;   phi_   = NULL;
  delete [] phis_;  phis_  = NULL;
  delete [] phit_;  phit_  = NULL;
  delete [] phiu_;  phiu_  = NULL;
  delete [] phiss_; phiss_ = NULL;
  delete [] phist_; phist_ = NULL;
  delete [] phisu_; phisu_ = NULL;
  delete [] phitt_; phitt_ = NULL;
  delete [] phitu_; phitu_ = NULL;
  delete [] phiuu_; phiuu_ = NULL;

  basis_ = basis;
  order_ = basis_->order();
  nDOF_  = basis_->nBasis();

  phi_   = new Real[nDOF_];
  phis_  = new Real[nDOF_];
  phit_  = new Real[nDOF_];
  phiu_  = new Real[nDOF_];
  phiss_ = new Real[nDOF_];
  phist_ = new Real[nDOF_];
  phisu_ = new Real[nDOF_];
  phitt_ = new Real[nDOF_];
  phitu_ = new Real[nDOF_];
  phiuu_ = new Real[nDOF_];

  // must be called after the face sign and basis is set
  setCache();
}

template <class Topology>
void
ElementBasis<TopoD3, Topology>::setFaceSign( const IntNFace& faceSign )
{
  faceSign_ = faceSign;

  // TODO: This is hacked for now until we get face signs sorted out
  if ( basis_->category() == BasisFunctionCategory_Hierarchical )
    SANS_ASSERT( basis_->order() <= 2 );

  //if ( basis_->category() == BasisFunctionCategory_Hierarchical )
  //  pointStoreCell_ = &QuadratureCache<Topology>::cache.hierarchicalCell[basis_->order()].at(edgeSign_);
}


template <class Topology>
void
ElementBasis<TopoD3, Topology>::evalBasis(
    const Real& sRef, const Real& tRef, const Real& uRef, const Real **phi ) const
{
  basis_->evalBasis( sRef, tRef, uRef, faceSign_, phi_, nDOF_ );
  *phi = phi_;
}

template <class Topology>
void
ElementBasis<TopoD3, Topology>::evalBasis(
    const QuadraturePoint<TopoDim>& ref, const Real **phi ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalBasis(ref.ref, phi_, nDOF_);
    *phi = phi_;
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoDim, Topology>::get( pointStoreCell_, ref, faceSign_ );
  *phi = cache.phi.data();
}

template <class Topology>
void
ElementBasis<TopoD3, Topology>::evalBasis(
    const QuadratureCellTracePoint<TopoDim>& ref, const Real **phi ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalBasis(ref.ref, phi_, nDOF_);
    *phi = phi_;
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoDim, Topology>::get(pointStoreTrace_, ref, faceSign_);
  *phi = cache.phi.data();
}


template <class Topology>
void
ElementBasis<TopoD3, Topology>::evalBasis(
    const Real& sRef, const Real& tRef, const Real& uRef, Real phi[], int nphi ) const
{
  basis_->evalBasis( sRef, tRef, uRef, faceSign_, phi, nphi );
}

template <class Topology>
void
ElementBasis<TopoD3, Topology>::evalBasis(
    const QuadraturePoint<TopoDim>& ref, Real phi[], int nphi ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalBasis(ref.ref, phi, nphi);
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoDim, Topology>::get( pointStoreCell_, ref, faceSign_ );
  SANS_ASSERT( nphi == static_cast<int>(cache.phi.size()) );
  for (int i = 0; i < nphi; i++)
    phi[i] = cache.phi[i];
}

template <class Topology>
void
ElementBasis<TopoD3, Topology>::evalBasis(
    const QuadratureCellTracePoint<TopoDim>& ref, Real phi[], int nphi ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalBasis(ref.ref, phi, nphi);
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoDim, Topology>::get(pointStoreTrace_, ref, faceSign_);
  SANS_ASSERT( nphi == static_cast<int>(cache.phi.size()) );
  for (int i = 0; i < nphi; i++)
    phi[i] = cache.phi[i];
}


template <class Topology>
void
ElementBasis<TopoD3, Topology>::evalBasisDerivative(
    const Real& sRef, const Real& tRef, const Real& uRef, const Real **phis, const Real **phit, const Real **phiu ) const
{
  basis_->evalBasisDerivative( sRef, tRef, uRef, faceSign_, phis_, phit_, phiu_, nDOF_ );
  *phis = phis_;
  *phit = phit_;
  *phiu = phiu_;
}

template <class Topology>
void
ElementBasis<TopoD3, Topology>::evalBasisDerivative(
    const QuadraturePoint<TopoDim>& ref, const Real **phis, const Real **phit, const Real **phiu ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalBasisDerivative(ref.ref[0], ref.ref[1], ref.ref[2], phis_, phit_, phiu_, nDOF_);
    *phis = phis_;
    *phit = phit_;
    *phiu = phiu_;
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoDim, Topology>::get( pointStoreCell_, ref, faceSign_ );
  const std::vector<Real>& phis_c = cache.dphi[0];
  const std::vector<Real>& phit_c = cache.dphi[1];
  const std::vector<Real>& phiu_c = cache.dphi[2];
  *phis = phis_c.data();
  *phit = phit_c.data();
  *phiu = phiu_c.data();
}


template <class Topology>
void
ElementBasis<TopoD3, Topology>::evalBasisDerivative(
    const QuadratureCellTracePoint<TopoDim>& ref, const Real **phis, const Real **phit, const Real **phiu ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalBasisDerivative(ref.ref[0], ref.ref[1], ref.ref[2], phis_, phit_, phiu_, nDOF_);
    *phis = phis_;
    *phit = phit_;
    *phiu = phiu_;
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoDim, Topology>::get(pointStoreTrace_, ref, faceSign_);
  const std::vector<Real>& phis_c = cache.dphi[0];
  const std::vector<Real>& phit_c = cache.dphi[1];
  const std::vector<Real>& phiu_c = cache.dphi[2];
  *phis = phis_c.data();
  *phit = phit_c.data();
  *phiu = phiu_c.data();
}


template <class Topology>
void
ElementBasis<TopoD3, Topology>::evalBasisDerivative(
    const Real& sRef, const Real& tRef, const Real& uRef, Real phis[], Real phit[], Real phiu[], int nphi ) const
{
  basis_->evalBasisDerivative( sRef, tRef, uRef, faceSign_, phis, phit, phiu, nphi );
}

template <class Topology>
void
ElementBasis<TopoD3, Topology>::evalBasisDerivative(
    const QuadraturePoint<TopoDim>& ref, Real phis[], Real phit[], Real phiu[], int nphi ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalBasisDerivative(ref.ref[0], ref.ref[1], ref.ref[2], phis, phit, phiu, nphi);
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoDim, Topology>::get( pointStoreCell_, ref, faceSign_ );
  const std::vector<Real>& phis_ = cache.dphi[0];
  const std::vector<Real>& phit_ = cache.dphi[1];
  const std::vector<Real>& phiu_ = cache.dphi[2];
  SANS_ASSERT( nphi == nDOF_ );
  for (int n = 0; n < nphi; n++)
  {
    phis[n] = phis_[n];
    phit[n] = phit_[n];
    phiu[n] = phiu_[n];
  }
}


template <class Topology>
void
ElementBasis<TopoD3, Topology>::evalBasisDerivative(
    const QuadratureCellTracePoint<TopoDim>& ref, Real phis[], Real phit[], Real phiu[], int nphi ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalBasisDerivative(ref.ref[0], ref.ref[1], ref.ref[2], phis, phit, phiu, nphi);
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoDim, Topology>::get(pointStoreTrace_, ref, faceSign_);
  const std::vector<Real>& phis_ = cache.dphi[0];
  const std::vector<Real>& phit_ = cache.dphi[1];
  const std::vector<Real>& phiu_ = cache.dphi[2];
  SANS_ASSERT( nphi == nDOF_ );
  for (int n = 0; n < nphi; n++)
  {
    phis[n] = phis_[n];
    phit[n] = phit_[n];
    phiu[n] = phiu_[n];
  }
}


template <class Topology>
void
ElementBasis<TopoD3, Topology>::evalBasisHessianDerivative(
    const Real& sRef, const Real& tRef, const Real& uRef,
    const Real **phiss,
    const Real **phist, const Real **phitt,
    const Real **phisu, const Real **phitu, const Real **phiuu ) const
{
  basis_->evalBasisHessianDerivative( sRef, tRef, uRef, faceSign_, phiss_, phist_, phitt_, phisu_, phitu_, phiuu_, nDOF_ );
  *phiss = phiss_;
  *phist = phist_;
  *phitt = phitt_;
  *phisu = phisu_;
  *phitu = phitu_;
  *phiuu = phiuu_;
}

template <class Topology>
void
ElementBasis<TopoD3, Topology>::evalBasisHessianDerivative(
    const QuadraturePoint<TopoDim>& ref,
    const Real **phiss,
    const Real **phist, const Real **phitt,
    const Real **phisu, const Real **phitu, const Real **phiuu ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalBasisHessianDerivative(ref.ref[0], ref.ref[1], ref.ref[2],
                               phiss_,
                               phist_, phitt_,
                               phisu_, phitu_, phiuu_, nDOF_);
    *phiss = phiss_;
    *phist = phist_;
    *phitt = phitt_;
    *phisu = phisu_;
    *phitu = phitu_;
    *phiuu = phiuu_;
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoDim, Topology>::get(pointStoreCell_, ref, faceSign_);
  const std::vector<Real>& phiss_c = cache.d2phi[0];
  const std::vector<Real>& phist_c = cache.d2phi[1];
  const std::vector<Real>& phitt_c = cache.d2phi[2];
  const std::vector<Real>& phisu_c = cache.d2phi[3];
  const std::vector<Real>& phitu_c = cache.d2phi[4];
  const std::vector<Real>& phiuu_c = cache.d2phi[5];
  *phiss = phiss_c.data();
  *phist = phist_c.data();
  *phitt = phitt_c.data();
  *phisu = phisu_c.data();
  *phitu = phitu_c.data();
  *phiuu = phiuu_c.data();
}

template <class Topology>
void
ElementBasis<TopoD3, Topology>::evalBasisHessianDerivative(
    const Real& sRef, const Real& tRef, const Real& uRef,
    Real phiss[],
    Real phist[], Real phitt[],
    Real phisu[], Real phitu[], Real phiuu[], int nphi ) const
{
  basis_->evalBasisHessianDerivative( sRef, tRef, uRef, faceSign_, phiss, phist, phitt, phisu, phitu, phiuu, nphi );
}

template <class Topology>
void
ElementBasis<TopoD3, Topology>::evalBasisHessianDerivative(
    const QuadraturePoint<TopoDim>& ref,
    Real phiss[],
    Real phist[], Real phitt[],
    Real phisu[], Real phitu[], Real phiuu[], int nphi ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalBasisHessianDerivative(ref.ref[0], ref.ref[1], ref.ref[2],
                               phiss,
                               phist, phitt,
                               phisu, phitu, phiuu, nphi);
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoDim, Topology>::get(pointStoreCell_, ref, faceSign_);
  const std::vector<Real>& phiss_c = cache.d2phi[0];
  const std::vector<Real>& phist_c = cache.d2phi[1];
  const std::vector<Real>& phitt_c = cache.d2phi[2];
  const std::vector<Real>& phisu_c = cache.d2phi[3];
  const std::vector<Real>& phitu_c = cache.d2phi[4];
  const std::vector<Real>& phiuu_c = cache.d2phi[5];
  SANS_ASSERT( nphi == nDOF_ );
  for (int n = 0; n < nphi; n++)
  {
    phiss[n] = phiss_c[n];
    phist[n] = phist_c[n];
    phitt[n] = phitt_c[n];
    phisu[n] = phisu_c[n];
    phitu[n] = phitu_c[n];
    phiuu[n] = phiuu_c[n];
  }
}

//----------------------------------------------------------------------------//
// field element: volume
//
// template parameters:
//   Topology       element/basis topology (e.g. Tet, Hex)
//   T              DOF type
//----------------------------------------------------------------------------//

template <class Tin, class Topology>
class Element<Tin, TopoD3, Topology> : public ElementBasis<TopoD3, Topology>
{
public:
  typedef ElementBasis<TopoD3, Topology> BaseType;
  typedef Tin T;
  typedef DLA::VectorS<TopoD3::D,Tin> gradT;
  typedef TopoD3 TopoDim;
  typedef typename BaseType::TopologyType TopologyType;
  typedef typename BaseType::RefCoordType RefCoordType;
  typedef BasisFunctionVolumeBase<Topology> BasisType;

  typedef std::array<int,Topology::NFace> IntNFace; // edge sign arrays

  Element();   // needed for new []
  explicit Element( const BasisType* basis );
  Element( int order, const BasisFunctionCategory& category );
  Element( const Element& );
  ~Element();

  Element& operator=( const Element& );

  //Used for arrays of elements
  void setBasis( const BasisType* basis );

  // DOF accessors
        T& DOF( int n )       { return DOF_[n]; }
  const T& DOF( int n ) const { return DOF_[n]; }

  // provides a vector view of the DOFs
  DLA::VectorDView<T> vectorViewDOF() { return DLA::VectorDView<T>(DOF_, nDOF_); }

  // basis function
  using BaseType::basis;

  using BaseType::order;
  using BaseType::nDOF;

  // face sign accessors
  using BaseType::faceSign;
  using BaseType::traceOrientation;

  using BaseType::setFaceSign;

  using BaseType::evalBasis;
  using BaseType::evalBasisDerivative;
  using BaseType::evalBasisHessianDerivative;

  void evalFromBasis( const Real phi[], int nphi, T& q ) const;
  template<int D>
  void evalFromBasis( const DLA::VectorS<D,Real> gradphi[], int nphi, DLA::VectorS<D,T> & gradq ) const;
  template<int D>
  void evalFromBasis( const DLA::MatrixSymS<D,Real> gradphi[], int nphi, DLA::MatrixSymS<D,T> & gradq ) const;

     T eval( const Real& sRef, const Real& tRef, const Real& uRef ) const;
  void eval( const Real& sRef, const Real& tRef, const Real& uRef, T& q ) const;
     T eval( const RefCoordType& ref ) const { return eval(ref[0], ref[1], ref[2]); }
  void eval( const RefCoordType& ref, T& q ) const { eval(ref[0], ref[1], ref[2], q); }
  void eval( const QuadraturePoint<TopoDim>& ref, T& q ) const;
  void eval( const QuadratureCellTracePoint<TopoDim>& ref, T& q ) const;

  void evalDerivative( const Real& sRef, const Real& tRef, const Real& uRef, T& qs, T& qt, T& qu ) const;
  void evalDerivative( const RefCoordType& Ref, DLA::VectorS<TopoD3::D, T>& dq ) const
  {
    evalDerivative(Ref[0], Ref[1], Ref[2], dq[0], dq[1], dq[2]);
  }
  void evalDerivative( const QuadraturePoint<TopoDim>& ref, DLA::VectorS<TopoDim::D, T>& dq ) const;
  void evalDerivative( const QuadratureCellTracePoint<TopoDim>& ref, DLA::VectorS<TopoDim::D, T>& dq ) const;

  // project DOFs onto another polynomial order
  void projectTo( Element& ) const;

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

protected:
  using BaseType::order_;     // polynomial order (e.g. order=1 is linear)
  using BaseType::nDOF_;      // total DOFs in element
  T* DOF_;                    // DOFs

  using BaseType::basis_;
  using BaseType::phi_;                 // temporary storage for bais function evaluation
  using BaseType::phis_;
  using BaseType::phit_;
  using BaseType::phiu_;

  using BaseType::pointStoreCell_; // cached basis functions evaluated at quadrature points
  using BaseType::pointStoreTrace_; // cached basis functions evaluated at quadrature points on the trace of the element

  using BaseType::faceSign_;   // +/- sign for face orientations (left/right element)
};


// default ctor needed for 'new []'
template <class T, class Topology>
Element<T, TopoD3, Topology>::Element()
  : BaseType()
{
  DOF_  = NULL;
}


template <class T, class Topology>
Element<T, TopoD3, Topology>::Element(
    const BasisType* basis ) : BaseType(basis)
{
  DOF_  = new T[nDOF_];
}


template <class T, class Topology>
Element<T, TopoD3, Topology>::Element( int order, const BasisFunctionCategory& category )
  : BaseType( order, category )
{
  DOF_  = new T[nDOF_];
}


template <class T, class Topology>
Element<T, TopoD3, Topology>::Element( const Element& a )
  : BaseType( a )
{
  DOF_  = new T[nDOF_];
  for (int n = 0; n < nDOF_; n++)
    DOF_[n] = a.DOF_[n];
}


template <class T, class Topology>
Element<T, TopoD3, Topology>::~Element()
{
  delete [] DOF_;
}


template <class T, class Topology>
Element<T, TopoD3, Topology>& // cppcheck-suppress operatorEqVarError
Element<T, TopoD3, Topology>::operator=( const Element& a )
{
  if (this != &a)
  {
    BaseType::operator=(a);

    delete [] DOF_; DOF_ = NULL;

    DOF_  = new T[nDOF_];
    for (int n = 0; n < nDOF_; n++)
      DOF_[n] = a.DOF_[n];
  }

  return *this;
}

template <class T, class Topology>
void
Element<T, TopoD3, Topology>::setBasis(
    const BasisType* basis )
{
  BaseType::setBasis(basis);

  delete [] DOF_; DOF_ = NULL;

  DOF_  = new T[nDOF_];
}


template <class T, class Topology>
void
Element<T, TopoD3, Topology>::evalFromBasis(
    const Real phi[], int nphi, T& q ) const
{
  SANS_ASSERT_MSG(nphi == nDOF_, "nphi = %d, nDOF_ = %d", nphi, nDOF_);
  q = 0;
  for (int n = 0; n < nDOF_; n++)
    q += phi[n]*DOF_[n];
}

template <class T, class Topology>
template <int D>
void
Element<T, TopoD3, Topology>::evalFromBasis(
    const DLA::VectorS<D,Real> gradphi[], int nphi, DLA::VectorS<D,T>& gradq ) const
{
  SANS_ASSERT_MSG(nphi == nDOF_, "nphi = %d, nDOF_ = %d", nphi, nDOF_);
  gradq = 0;
  for (int n = 0; n < nDOF_; n++)
    for (int i = 0; i < D; i++)
      gradq[i] += gradphi[n][i]*DOF_[n];
}

template <class T, class Topology>
template <int D>
void
Element<T, TopoD3, Topology>::evalFromBasis(
    const DLA::MatrixSymS<D,Real> hessphi[], int nphi, DLA::MatrixSymS<D,T>& hessq ) const
{
  SANS_ASSERT_MSG(nphi == nDOF_, "nphi = %d, nDOF_ = %d", nphi, nDOF_);
  hessq = 0;
  for (int n = 0; n < nDOF_; n++)
    for (int i = 0; i < D; i++)
      for (int j =0; j <=i; j++) // because it's lower triangular
      hessq(i,j) += hessphi[n](i,j)*DOF_[n];
}

template <class T, class Topology>
T
Element<T, TopoD3, Topology>::eval(
    const Real& sRef, const Real& tRef, const Real& uRef ) const
{
  basis_->evalBasis( sRef, tRef, uRef, faceSign_, phi_, nDOF_ );
  T q;

  q = 0;
  for (int n = 0; n < nDOF_; n++)
    q += phi_[n]*DOF_[n];

  return q;
}

template <class T, class Topology>
void
Element<T, TopoD3, Topology>::eval(
    const Real& sRef, const Real& tRef, const Real& uRef, T& q ) const
{
  basis_->evalBasis( sRef, tRef, uRef, faceSign_, phi_, nDOF_ );

  q = 0;
  for (int n = 0; n < nDOF_; n++)
    q += phi_[n]*DOF_[n];
}

template <class T, class Topology>
void
Element<T, TopoD3, Topology>::eval(
    const QuadraturePoint<TopoDim>& ref, T& q ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    eval(ref.ref, q);
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoDim, Topology>::get( pointStoreCell_, ref, faceSign_ );
  q = 0;
  for (int n = 0; n < nDOF_; n++)
    q += cache.phi[n]*DOF_[n];
}


template <class T, class Topology>
void
Element<T, TopoD3, Topology>::eval(
    const QuadratureCellTracePoint<TopoDim>& ref, T& q ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    eval(ref.ref, q);
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoDim, Topology>::get(pointStoreTrace_, ref, faceSign_);
  q = 0;
  for (int n = 0; n < nDOF_; n++)
    q += cache.phi[n]*DOF_[n];
}


template <class T, class Topology>
void
Element<T, TopoD3, Topology>::evalDerivative(
    const Real& sRef, const Real& tRef, const Real& uRef, T& qs, T& qt, T& qu ) const
{
  evalBasisDerivative( sRef, tRef, uRef, phis_, phit_, phiu_, nDOF_ );

  qs = 0;
  qt = 0;
  qu = 0;
  for (int n = 0; n < nDOF_; n++)
  {
    qs += phis_[n]*DOF_[n];
    qt += phit_[n]*DOF_[n];
    qu += phiu_[n]*DOF_[n];
  }
}


template <class T, class Topology>
void
Element<T, TopoD3, Topology>::evalDerivative(
    const QuadraturePoint<TopoDim>& ref, DLA::VectorS<TopoDim::D, T>& dq ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalDerivative(ref.ref, dq);
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoDim, Topology>::get( pointStoreCell_, ref, faceSign_ );
  const std::vector<Real>& phis = cache.dphi[0];
  const std::vector<Real>& phit = cache.dphi[1];
  const std::vector<Real>& phiu = cache.dphi[2];
  dq = 0;
  for (int n = 0; n < nDOF_; n++)
  {
    dq[0] += phis[n]*DOF_[n];
    dq[1] += phit[n]*DOF_[n];
    dq[2] += phiu[n]*DOF_[n];
  }
}


template <class T, class Topology>
void
Element<T, TopoD3, Topology>::evalDerivative(
    const QuadratureCellTracePoint<TopoDim>& ref, DLA::VectorS<TopoDim::D, T>& dq ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalDerivative(ref.ref, dq);
    return;
  }

  const QuadratureBasisPointValues<TopoDim::D>& cache = QuadratureCacheValues<TopoDim, Topology>::get(pointStoreTrace_, ref, faceSign_);
  const std::vector<Real>& phis = cache.dphi[0];
  const std::vector<Real>& phit = cache.dphi[1];
  const std::vector<Real>& phiu = cache.dphi[2];
  dq = 0;
  for (int n = 0; n < nDOF_; n++)
  {
    dq[0] += phis[n]*DOF_[n];
    dq[1] += phit[n]*DOF_[n];
    dq[2] += phiu[n]*DOF_[n];
  }
}


// project DOFs onto another polynomial order
template <class T, class Topology>
void
Element<T, TopoD3, Topology>::projectTo( Element& ElemTo ) const
{
  BasisFunctionVolume_projectTo( basis_, DOF_, nDOF_, ElemTo.basis_, ElemTo.DOF_, ElemTo.nDOF_ );
  ElemTo.setFaceSign(faceSign_);
}


template <class T, class Topology>
void
Element<T, TopoD3, Topology>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "Element<T, TopoD3, Topology>:"
      << "  order_ = " << order_ << "  nDOF_ = " << nDOF_;
  out << "  faceSign_ =";
  for (int n = 0; n < Topology::NFace; n++)
    out << " " << faceSign_[n];
  out << std::endl;
  if (DOF_ != NULL)
  {
    out << indent << indent << "DOF_ = ";
    for (int n = 0; n < nDOF_; n++)
      out << DOF_[n] << "; ";
    out << std::endl;
  }
  if ( basis_ != NULL )
  {
    out << indent << indent << "basis_:" << std::endl;
    basis_->dump( indentSize += 2, out );
  }
}

}

#endif  // ELEMENTVOLUME_H
