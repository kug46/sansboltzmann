// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ELEMENTXFIELDLINE_INSTANTIATE
#include "ElementXFieldLine_impl.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Specialization: 1-D grid field line element
//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//
// compute implied metric
template<class PhysDim>
void
ElementXFieldLineBase<PhysDim>::impliedMetric( const Real& s, TensorSymX& M ) const
{
  Matrix Jref; //jacobian from reference element to physical element
  jacobian( s, Jref );

  // transformation matrix from the unit equilateral element to the reference element (in 1D)
  const DLA::MatrixS<PhysD1::D,TopoD,Real>& invJeq = JacobianEquilateralTransform<PhysD1,TopoD1,Line>::invJeq;

  Matrix J = Jref*invJeq; //jacobian from equilateral element to physical element

  TensorSymX JJt = J*Transpose(J);

  // M = inv(JJt)
  M(0,0) = 1.0/JJt(0,0);
}

template <class PhysDim>
void
ElementXFieldLineBase<PhysDim>::evalReferenceCoordinateHessian( const Real& sRef, TensorSymX& shess ) const
{
  SANS_ASSERT( PhysDim::D == 1 ); // specialized for 1-D

  // shess is d^2s/dx^2
  Real xs, xss;

  basis_->evalBasisDerivative( sRef, phis_, nDOF_ );
  basis_->evalBasisHessianDerivative( sRef, phiss_, nDOF_ );
  xs = 0; xss = 0;
  for (int n = 0; n < nDOF_; n++)
  {
    xs  += phis_[n]*DOF_[n](0);
    xss += phiss_[n]*DOF_[n](0);
  }

  shess(0,0) = -xss/(xs*xs*xs); // 1D shortcut
}

//----------------------------------------------------------------------------//
// Explicit instantiation
//----------------------------------------------------------------------------//
template class ElementXFieldLineBase<PhysD1>;
}
