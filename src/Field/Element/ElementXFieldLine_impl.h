// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(ELEMENTXFIELDLINE_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include <iomanip> // std::setprecision

#include "ElementXFieldLine.h"
#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "ElementXFieldJacobianEquilateral.h"
#include "Quadrature/Quadrature.h"

#include "ReferenceElementMesh.h"
#include "Field/output_Tecplot/TecTopo.h"

#include "Surreal/SurrealS.h"

namespace SANS
{

//---------------------------------------------------------------------------//
// length of line element
template<class PhysDim>
Real
ElementXFieldLineBase<PhysDim>::length( const Real& sRef ) const
{
  evalBasisDerivative( sRef, phis_, nDOF_ );

  return length(phis_);
}

template<class PhysDim>
Real
ElementXFieldLineBase<PhysDim>::length( const QuadraturePoint<TopoD1>& ref ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    return length(ref.ref[0]);
  }

  const QuadratureBasisPointValues<TopoD>& cache = QuadratureCacheValues<TopoD1, Line>::get( pointStoreCell_, ref, sgn_ );
  const std::vector<Real>& phis = cache.dphi[0];

  return length(phis.data());
}

template<class PhysDim>
Real
ElementXFieldLineBase<PhysDim>::length( const Real phis[] ) const
{
  VectorX Xs;
  Real length;

  Xs = 0;
  for (int n = 0; n < nDOF_; n++)
    Xs += phis[n]*DOF_[n];

  length = sqrt(dot(Xs,Xs));
  return length;
}

// computed at mid reference coordinate point
template<class PhysDim>
Real
ElementXFieldLineBase<PhysDim>::length() const
{
  Real s = Line::centerRef[0];
  return length(s);
}

//---------------------------------------------------------------------------//
template <class PhysDim>
BoundingBox<PhysDim>
ElementXFieldLineBase<PhysDim>::boundingBox() const
{
  BoundingBox<PhysDim> bbox;

  // get the end points of the element
  for (int n = 0; n < Line::NNode; n++)
    bbox.setBound(DOF_[n]);

  typedef QuadraturePoint<TopoD1> QuadPointType;

  if ( basis_->order()  > 1 )
  {
    VectorX X;

    // use a 2P+1 quadrature rule
    Quadrature<TopoD1, Line> quadrature( 2*basis_->order() + 1 );
    const int nquad = quadrature.nQuadrature();

    // evaluate at select points on the edge
    for (int iquad = 0; iquad < nquad; iquad++)
    {
      QuadPointType sRef = quadrature.coordinates_cache(iquad);

      coordinates(sRef, X);

      bbox.setBound(X);
    }
  }


  return bbox;
}

//----------------------------------------------------------------------------//
template<class PhysDim>
void
ElementXFieldLineBase<PhysDim>::jacobian( Matrix& J ) const
{
  Real s = Line::centerRef[0];
  jacobian( s, J );
}

template<class PhysDim>
void
ElementXFieldLineBase<PhysDim>::jacobian( const Real& sRef, Matrix& J ) const
{
  evalBasisDerivative( sRef, phis_, nDOF_ );

  jacobian( phis_, J );
}

template<class PhysDim>
void
ElementXFieldLineBase<PhysDim>::jacobian( const Real* __restrict phis, Matrix& J ) const
{
  J = 0;
  for (int n = 0; n < nDOF_; n++)
    for (int d = 0; d < D; d++)
      J(d,0) += phis[n]*DOF_[n](d); //d(x_i)/ds
}

//----------------------------------------------------------------------------//
template<class PhysDim>
void
ElementXFieldLineBase<PhysDim>::hessian( const Real& sRef, TensorSymHessian& H ) const
{
  basis_->evalBasisHessianDerivative( sRef, phiss_, nDOF_ );

  H = 0;
  for (int n = 0; n < nDOF_; n++)
    for (int d = 0; d < D; d++)
      H(d)(0,0) += phiss_[n]*DOF_[n](d); //d^2(x_i)/d(s^2)
}

template<class PhysDim>
void
ElementXFieldLineBase<PhysDim>::impliedMetric( TensorSymX& M ) const
{
  Real s = Line::centerRef[0];
  impliedMetric( s, M );
}

//----------------------------------------------------------------------------//
// tangent vector: dX/ds where X = [x,y,z,...]^T
template <class PhysDim>
void
ElementXFieldLineBase<PhysDim>::tangent(const Real* __restrict phis, VectorX& Xs) const
{
  Xs = 0;
  for (int n = 0; n < nDOF_; n++)
    Xs += phis[n]*DOF_[n];
}

template <class PhysDim>
void
ElementXFieldLineBase<PhysDim>::tangent(const Real& sRef, VectorX& Xs) const
{
  evalBasisDerivative( sRef, phis_, nDOF_ );

  tangent(phis_, Xs);
}

template <class PhysDim>
void
ElementXFieldLineBase<PhysDim>::tangent(const QuadraturePoint<TopoD1>& ref, VectorX& Xs) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    tangent(ref.ref[0], Xs);
    return;
  }

  const QuadratureBasisPointValues<TopoD>& cache = QuadratureCacheValues<TopoD1, Line>::get( pointStoreCell_, ref, sgn_ );
  const std::vector<Real>& phis = cache.dphi[0];

  tangent(phis.data(), Xs);
}

template <class PhysDim>
void
ElementXFieldLineBase<PhysDim>::tangent(const QuadratureCellTracePoint<TopoD1>& ref, VectorX& Xs) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    tangent(ref.ref[0], Xs);
    return;
  }

  const QuadratureBasisPointValues<TopoD1::D>& cache = QuadratureCacheValues<TopoD1, Line>::get(pointStoreTrace_, ref, sgn_);
  const std::vector<Real>& phis = cache.dphi[0];

  tangent(phis.data(), Xs);
}

//----------------------------------------------------------------------------//
// normalized tangent vector: dX/ds / ||dX/ds||
template <class PhysDim>
void
ElementXFieldLineBase<PhysDim>::unitTangent(const Real& sRef, VectorX& Xs) const
{
  tangent(sRef, Xs);

  Xs /= sqrt(dot(Xs,Xs));
}

template <class PhysDim>
void
ElementXFieldLineBase<PhysDim>::unitTangent(const QuadraturePoint<TopoD1>& sRef, VectorX& Xs) const
{
  tangent(sRef, Xs);

  Xs /= sqrt(dot(Xs,Xs));
}

template <class PhysDim>
void
ElementXFieldLineBase<PhysDim>::unitTangent(const QuadratureCellTracePoint<TopoD1>& sRef, VectorX& Xs) const
{
  tangent(sRef, Xs);

  Xs /= sqrt(dot(Xs,Xs));
}

//----------------------------------------------------------------------------//
// gradients of reference coordinates: gradient(s)
template <class PhysDim>
template <int N>
void
ElementXFieldLineBase<PhysDim>::evalReferenceCoordinateGradient(
    const SurrealS<N>& s, DLA::VectorS<D,SurrealS<N>>& sgrad ) const
{
  std::vector<SurrealS<N>> phis(nDOF_,0.0);

  evalBasisDerivative( s, phis.data(), nDOF_ );

  evalReferenceCoordinateGradient( phis.data(), sgrad );
}

template <class PhysDim>
void
ElementXFieldLineBase<PhysDim>::evalReferenceCoordinateGradient( const Real& s, VectorX& sgrad ) const
{
  evalBasisDerivative( s, phis_, nDOF_ );

  evalReferenceCoordinateGradient( phis_, sgrad );
}

template <class PhysDim>
void
ElementXFieldLineBase<PhysDim>::evalReferenceCoordinateGradient( const RefCoordType& sRef, VectorX& sgrad ) const
{
  evalBasisDerivative( sRef[0], phis_, nDOF_ );

  evalReferenceCoordinateGradient( phis_, sgrad );
}

template <class PhysDim>
void
ElementXFieldLineBase<PhysDim>::evalReferenceCoordinateGradient( const QuadraturePoint<TopoD1>& ref, VectorX& sgrad ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalReferenceCoordinateGradient(ref.ref[0], sgrad);
    return;
  }

  const QuadratureBasisPointValues<TopoD>& cache = QuadratureCacheValues<TopoD1, Line>::get( pointStoreCell_, ref, sgn_ );
  const std::vector<Real>& phis = cache.dphi[0];

  evalReferenceCoordinateGradient( phis.data(), sgrad );
}

template <class PhysDim>
void
ElementXFieldLineBase<PhysDim>::evalReferenceCoordinateGradient( const QuadratureCellTracePoint<TopoD1>& ref, VectorX& sgrad ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalReferenceCoordinateGradient(ref.ref[0], sgrad);
    return;
  }

  const QuadratureBasisPointValues<TopoD1::D>& cache = QuadratureCacheValues<TopoD1, Line>::get(pointStoreTrace_, ref, sgn_);
  const std::vector<Real>& phis = cache.dphi[0];

  evalReferenceCoordinateGradient( phis.data(), sgrad );
}

//----------------------------------------------------------------------------//
// gradients of reference coordinates: gradient(s)
//   The following method should work for all physical dimensions
template <class PhysDim>
template <int N>
void
ElementXFieldLineBase<PhysDim>::evalReferenceCoordinateGradient(
    const SurrealS<N>* __restrict phis, DLA::VectorS<D, SurrealS<N>>& sgrad ) const
{
// dX = [dx, dy, dz]^T
// dS = [ds]
// J = [[dx/ds]
//      [dy/ds]
//      [dz/ds]]
//
// Solving the system
// Transpose(J)*[dX] = Transpose(J)*J*[dS]
// where Transpose(J) projects dX onto the tangent of the line, which is the normalized J
// --> [dS]/[dX][i] = J[i]/(Transpose(J)*J)


  // sgrad is ds/dX
  DLA::VectorS<D, SurrealS<N>> J = 0;

  for (int n = 0; n < nDOF_; n++)
    J += phis[n]*DOF_[n];

  SurrealS<N> tmp = dot(J,J);

  sgrad = J/tmp; // 1x1 matrix solve

// In 2D for example, the result is equivalent to the following method:
// Solving the system
//   [ [ dx/ds, dy/ds ]   [ [ds/dx]   = [ [ds/ds]   = [ [1]
//     [ nx,    ny    ] ]   [ds/dy] ]     [ 0   ] ]     [0] ]
// for grad(s) = [ds/dx, ds/dy]^T, i.e. the surface gradient of s with a zero normal component.
// Note that one can use the (non-normalized) normal vector n = [nx, ny]^T = [dy/ds, -dx/ds]^T.
}


template <class PhysDim>
void
ElementXFieldLineBase<PhysDim>::evalReferenceCoordinateGradient(
    const Real* __restrict phis, VectorX& sgrad ) const
{
// dX = [dx, dy, dz]^T
// dS = [ds]
// J = [[dx/ds]
//      [dy/ds]
//      [dz/ds]]
//
// Solving the system
// Transpose(J)*[dX] = Transpose(J)*J*[dS]
// where Transpose(J) projects dX onto the tangent of the line, which is the normalized J
// --> [dS]/[dX][i] = J[i]/(Transpose(J)*J)


  // sgrad is ds/dX
  VectorX J = 0;

  for (int n = 0; n < nDOF_; n++)
    J += phis[n]*DOF_[n];

  Real tmp = dot(J,J);

  sgrad = J/tmp; // 1x1 matrix solve

// In 2D for example, the result is equivalent to the following method:
// Solving the system
//   [ [ dx/ds, dy/ds ]   [ [ds/dx]   = [ [ds/ds]   = [ [1]
//     [ nx,    ny    ] ]   [ds/dy] ]     [ 0   ] ]     [0] ]
// for grad(s) = [ds/dx, ds/dy]^T, i.e. the surface gradient of s with a zero normal component.
// Note that one can use the (non-normalized) normal vector n = [nx, ny]^T = [dy/ds, -dx/ds]^T.
}


//----------------------------------------------------------------------------//
// basis gradient: gradient(phi)
template <class PhysDim>
template <int N>
void
ElementXFieldLineBase<PhysDim>::evalBasisGradient(
    const SurrealS<N>& sRef, const ElementBasis<TopoD1, Line>& fldElem,
    DLA::VectorS<D,SurrealS<N>> gradphi[], int nphi ) const
{
  const int nBasis = fldElem.nDOF();   // total DOFs in element
  SANS_ASSERT(nphi == nBasis);

  DLA::VectorS<D,SurrealS<N>> sgrad;    // gradients of reference coordinates (s)
  std::vector<SurrealS<N>> phis(nBasis,0.0); // derivatives of solution basis wrt (s)

  evalReferenceCoordinateGradient( sRef, sgrad );

  fldElem.evalBasisDerivative( sRef, phis.data(), nphi );

  for (int n = 0; n < nphi; n++)
    gradphi[n] = phis.at(n)*sgrad;
}

template <class PhysDim>
void
ElementXFieldLineBase<PhysDim>::evalBasisGradient( const Real& sRef,
                                                   const ElementBasis< TopoD1, Line >& fldElem,
                                                   VectorX gradphi[], int nphi ) const
{
  evalBasisGradient<Real>(sRef, fldElem, gradphi, nphi);
}

template <class PhysDim>
void
ElementXFieldLineBase<PhysDim>::evalBasisGradient( const RefCoordType& sRef,
                                                   const ElementBasis< TopoD1, Line >& fldElem,
                                                   VectorX gradphi[], int nphi ) const
{
  evalBasisGradient<RefCoordType>(sRef, fldElem, gradphi, nphi);
}

template <class PhysDim>
void
ElementXFieldLineBase<PhysDim>::evalBasisGradient( const QuadraturePoint<TopoD1>& sRef,
                                                   const ElementBasis< TopoD1, Line >& fldElem,
                                                   VectorX gradphi[], int nphi ) const
{
  evalBasisGradient<QuadraturePoint<TopoD1>>(sRef, fldElem, gradphi, nphi);
}

template <class PhysDim>
void
ElementXFieldLineBase<PhysDim>::evalBasisGradient( const QuadratureCellTracePoint<TopoD1>& sRef,
                                                   const ElementBasis< TopoD1, Line >& fldElem,
                                                   VectorX gradphi[], int nphi ) const
{
  evalBasisGradient<QuadratureCellTracePoint<TopoD1>>(sRef, fldElem, gradphi, nphi);
}

template <class PhysDim>
template <class RefCoord>
void
ElementXFieldLineBase<PhysDim>::evalBasisGradient( const RefCoord& sRef,
                                                   const ElementBasis<TopoD1, Line>& fldElem,
                                                   VectorX gradphi[], int nphi ) const
{
  const int nBasis = fldElem.nDOF();   // total DOFs in element
  SANS_ASSERT(nphi == nBasis);

  VectorX sgrad;    // gradients of reference coordinates (s)
  const Real *phis; // derivatives of solution basis wrt (s)

  evalReferenceCoordinateGradient( sRef, sgrad );

  fldElem.evalBasisDerivative( sRef, &phis );

  for (int n = 0; n < nphi; n++)
    gradphi[n] = phis[n]*sgrad;
}

template <class PhysDim>
void
ElementXFieldLineBase<PhysDim>::evalBasisGradient( const BasisPointDerivative<TopoD1::D>& dphi,
                                                   VectorX gradphi[], int nphi ) const
{
  SANS_ASSERT(nphi == dphi.size());

  VectorX sgrad;                        // gradients of reference coordinates (s)
  const Real* phis = dphi.deriv(0);     // derivatives of solution basis w.r.t. s

  evalReferenceCoordinateGradient( phis, sgrad );

  for (int n = 0; n < nphi; n++)
    gradphi[n] = phis[n]*sgrad;
}

//----------------------------------------------------------------------------//
// TODO: generalize
// basis hessian - See Documentation/Hessians.pdf
// This is the scalarized version of the formula
template <class PhysDim>
void
ElementXFieldLineBase<PhysDim>::evalBasisHessian( const RefCoordType& sRef,
                                                  const ElementBasis<TopoD1, Line>& fldElem,
                                                  TensorSymX hessphi[], int nphi ) const
{
  SANS_ASSERT( PhysDim::D == 1 ); //Only implemented in 1-D (non-manifold) for now

  const int nBasis = fldElem.nDOF();   // total solution DOFs in element
  SANS_ASSERT(nphi == nBasis);

  VectorX sgrad;
  TensorSymX shess;          // hessian of reference coordinates (s)
  const Real *phis, *phiss;  // derivatives of solution basis wrt (s)

  evalReferenceCoordinateGradient( sRef[0], sgrad );
  evalReferenceCoordinateHessian( sRef[0], shess );
  fldElem.evalBasisDerivative( sRef[0], &phis );
  fldElem.evalBasisHessianDerivative( sRef[0], &phiss );

  for (int n = 0; n < nphi; n++)
  {
    hessphi[n]  = sgrad[0]*phiss[n]*sgrad[0];
    hessphi[n] += phis[n]*shess;
  }
}

// TODO: Cache hessian at quadrature points
template <class PhysDim>
void
ElementXFieldLineBase<PhysDim>::evalBasisHessian( const QuadraturePoint<TopoD1>& sRef,
                                                  const ElementBasis<TopoD1, Line>& fldElem,
                                                  TensorSymX hessphi[], int nphi ) const
{
  evalBasisHessian(sRef.ref, fldElem, hessphi, nphi);
}

template <class PhysDim>
void
ElementXFieldLineBase<PhysDim>::evalBasisHessian( const QuadratureCellTracePoint<TopoD1>& sRef,
                                                  const ElementBasis<TopoD1, Line>& fldElem,
                                                  TensorSymX hessphi[], int nphi ) const
{
  evalBasisHessian(sRef.ref, fldElem, hessphi, nphi);
}


template <class PhysDim>
void
ElementXFieldLineBase<PhysDim>::dumpTecplot( std::ostream& out ) const
{
  int nRefine = this->order_ == 1 ? 0 : 4;
  ReferenceElementMesh<Line> refMesh(nRefine);
  const int nRefnode = refMesh.nodes.size();
  const int nRefelem = refMesh.elems.size();

  std::vector<std::string> xyzt = {"\"X\" ", "\"Y\" ", "\"Z\" ", "\"T\" "};

  out << std::endl;
  out << "VARIABLES = ";
  for (int d = 0; d < PhysDim::D; d++)
    out << xyzt[d];
  out << std::endl;
  out << "ZONE"
      << " N=" << nRefnode
      << " E=" << nRefelem
      << " F=FEPOINT"
      << " ET=" << TecTopo<Line>::name()
      << std::endl;

  VectorX X;
  RefCoordType sRefCell;

  for ( int i = 0; i < nRefnode; i++ )
  {
    sRefCell = refMesh.nodes[i];

    this->eval( sRefCell, X );

    for (int k = 0; k < PhysDim::D; k++)
      out << std::scientific << std::setprecision(16) << X[k] << " ";
    out << std::endl;
  }

  for (int relem = 0; relem < nRefelem; relem++)
  {
    for (int n = 0; n < Line::NNode; n++ )
      out << refMesh.elems[relem][n] + 1 << " "; // +1 for one based indexing
    out << std::endl;
  }
}

} //namespace SANS
