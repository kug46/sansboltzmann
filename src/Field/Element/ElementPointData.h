// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ELEMENTPOINTDATA_H
#define ELEMENTPOINTDATA_H

// an element to represent non-interpolatable point data

#include <iostream>
#include <limits>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "BasisFunction/BasisFunctionNode.h"
#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea.h"
#include "BasisFunction/BasisFunctionVolume.h"
#include "BasisFunction/BasisFunctionCategory.h"

#include "Topology/ElementTopology.h"
#include "Topology/Dimension.h"

#include "Element.h"

namespace SANS
{

template<class TopoDim, class Topology>
struct BasisFunctionTopoBase;

template<>
struct BasisFunctionTopoBase<TopoD0, Node>
{
  typedef BasisFunctionNodeBase type;
};

template<>
struct BasisFunctionTopoBase<TopoD1, Line>
{
  typedef BasisFunctionLineBase type;
};

template<class Topology>
struct BasisFunctionTopoBase<TopoD2, Topology>
{
  typedef BasisFunctionAreaBase<Topology> type;
};

template<class Topology>
struct BasisFunctionTopoBase<TopoD3, Topology>
{
  typedef BasisFunctionVolumeBase<Topology> type;
};

//----------------------------------------------------------------------------//
// point data element
//
// template parameters:
//   T            DOF data type
//----------------------------------------------------------------------------//

template <class T, class TopDim_, class Topology>
class ElementPointData
{
public:
  typedef TopDim_ TopoDim;
  typedef Topology TopologyType;
  typedef typename BasisFunctionTopoBase<TopoDim, Topology>::type BasisType;

  explicit ElementPointData( const BasisType* basis );
  ElementPointData( int order, const BasisFunctionCategory& category );
  ElementPointData( const ElementPointData& );
  ~ElementPointData();

  ElementPointData& operator=( const ElementPointData& );

  // basis function
  const BasisType* basis() const { return basis_; }

  int order() const { return order_; }
  int nDOF() const { return nDOF_; }

  // solution DOF accessors
        T& DOF( int n )       { return DOF_[n]; }
  const T& DOF( int n ) const { return DOF_[n]; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  int order_;                 // polynomial order (e.g. order=1 is linear)
  int nDOF_;                  // total DOFs in element
  T *DOF_;                    // DOFs

  const BasisType* basis_;
};


template <class T, class TopDim, class Topology>
ElementPointData<T, TopDim, Topology>::ElementPointData( int order, const BasisFunctionCategory& category )
{
  // Only works with point type basis functions
  if (order > 0)
    SANS_ASSERT( category == BasisFunctionCategory_Hierarchical );

  basis_ = BasisType::getBasisFunction(order, category);

  order_ = order;
  nDOF_  = basis_->nBasis();
  DOF_  = new T[nDOF_];
  for (int n = 0; n < nDOF_; n++)
    DOF_[n] = std::numeric_limits<Real>::max();
}


template <class T, class TopDim, class Topology>
ElementPointData<T, TopDim, Topology>::ElementPointData( const BasisType* basis )
{
  // Only works with point type basis functions
  if (basis->order() > 0)
    SANS_ASSERT( basis->category() == BasisFunctionCategory_Hierarchical );

  basis_ = basis;

  order_ = basis_->order();
  nDOF_  = basis_->nBasis();
  DOF_  = new T[nDOF_];
  for (int n = 0; n < nDOF_; n++)
    DOF_[n] = std::numeric_limits<Real>::max();

}


template <class T, class TopDim, class Topology>
ElementPointData<T, TopDim, Topology>::ElementPointData( const ElementPointData& a )
{
  order_ = a.order_;
  nDOF_  = a.nDOF_;
  DOF_  = new T[nDOF_];

  for (int n = 0; n < nDOF_; n++)
    DOF_[n] = a.DOF_[n];

  basis_ = a.basis_;
}


template <class T, class TopDim, class Topology>
ElementPointData<T, TopDim, Topology>::~ElementPointData()
{
  delete [] DOF_;
}


template <class T, class TopDim, class Topology>
ElementPointData<T, TopDim, Topology>&
ElementPointData<T, TopDim, Topology>::operator=( const ElementPointData& a )
{
  if (this != &a)
  {
    delete [] DOF_; DOF_ = NULL;

    order_ = a.order_;
    nDOF_  = a.nDOF_;
    DOF_  = new T[nDOF_];

    for (int n = 0; n < nDOF_; n++)
      DOF_[n] = a.DOF_[n];

    basis_ = a.basis_;
  }

  return *this;
}


template <class T, class TopDim, class Topology>
void
ElementPointData<T, TopDim, Topology>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "ElementNode:"
      << "  order_ = " << order_ << "  nDOF_ = " << nDOF_ << std::endl;

  out << indent << "  DOF_ = ";
  for (int n = 0; n < nDOF_; n++)
    out << "(" << DOF_[n] << ") ";
  out << std::endl;
  if (basis_ != NULL)
  {
    out << indent << "  basis_:" << std::endl;
    basis_->dump( indentSize+2, out );
  }
}

}

#endif  // ELEMENTPOINTDATA_H
