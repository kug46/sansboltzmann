// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ELEMENTNODE_H
#define ELEMENTNODE_H

// node element

#include <iostream>
#include <string>
#include <limits>

#include "Element.h"
#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "BasisFunction/BasisFunctionNode.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisPoint.h"
#include "BasisFunction/BasisPointDerivative.h"
#include "BasisFunction/Quadrature_Cache.h"
#include "Topology/ElementTopology.h"
#include "Topology/Dimension.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// field element: node
//
// template parameters:
//   T            DOF data type
//----------------------------------------------------------------------------//

template <class T>
class Element< T, TopoD0, Node >
{
public:
  typedef TopoD0 TopoDim;
  typedef Node TopologyType;
  typedef BasisFunctionNodeBase BasisType;
  typedef DLA::VectorS<1,Real> RefCoordType; //0-length vectors don't really work...

  explicit Element( const BasisType* basis );
  Element( int order, const BasisFunctionCategory& category );
  Element( const Element& );
  ~Element();

  Element& operator=( const Element& );

  // basis function
  const BasisType* basis() const { return basis_; }

  int rank() const { return rank_; }
  void setRank( const int rank ) { rank_ = rank; }

  int order() const { return order_; }
  int nDOF() const { return nDOF_; }

  // dummy function that really should never be called
  std::array<int,1> traceOrientation() const { return std::array<int,1>{{0}}; }
  void setTraceOrientation(const std::array<int,1>&) {}

  // solution DOF accessors
        T& DOF( int n )       { return DOF_[n]; }
  const T& DOF( int n ) const { return DOF_[n]; }

  void evalBasis( Real phi[], int nphi ) const;
  void evalBasis( const Real& sRef, Real phi[], int nphi ) const { evalBasis(phi, nphi); }
  void evalBasis( const RefCoordType& sRef, Real phi[], int nphi ) const { evalBasis(phi, nphi); }
  void evalBasis( const QuadraturePoint<TopoD0>& ref, Real phi[], int nphi ) const { evalBasis(phi, nphi); }
  void evalBasis( const RefCoordType& Ref, BasisPoint<1>& phi ) const
  {
    phi.ref() = Ref;
    evalBasis(Ref[0], phi, phi.size());
  }

  void evalBasisDerivative( Real phis[], int nphi ) const;
  void evalBasisDerivative( const Real& sRef, Real phis[], int nphi ) const { evalBasisDerivative(phis, nphi); }
  void evalBasisDerivative( const RefCoordType& sRef, Real phis[], int nphi ) const { evalBasisDerivative(phis, nphi); }
  void evalBasisDerivative( const QuadraturePoint<TopoD0>& ref, Real phis[], int nphi ) const { evalBasisDerivative(phis, nphi); }
  void evalBasisDerivative( const RefCoordType& Ref, BasisPointDerivative<TopoD0::D+1>& dphi ) const
  {
    dphi.ref() = Ref;
    evalBasisDerivative(Ref[0], dphi.deriv(0), dphi.size());
  }

  void evalBasisHessianDerivative( Real phiss[], int nphi ) const;
  void evalBasisHessianDerivative( const RefCoordType& sRef, Real phiss[], int nphi ) const { evalBasisHessianDerivative(phiss, nphi); }

  void evalFromBasis( const Real phi[], int nphi, T& q ) const;

  //Generic interface
     T eval() const;
  void eval( T& q ) const;
     T eval( const RefCoordType& ref ) const { return eval(); }
  void eval( const RefCoordType& ref, T& q ) const { eval(q); }
  void eval( const QuadraturePoint<TopoD0>& ref, T& q ) const { eval(q); }

  // project DOFs onto another polynomial order
  void projectTo( Element& ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

        int& normalSignL()       { return normalSignL_; }
  const int& normalSignL() const { return normalSignL_; }

        int& normalSignR()       { return normalSignR_; }
  const int& normalSignR() const { return normalSignR_; }

protected:
  int rank_;                  // processor rank that possesses this element

  int order_;                 // polynomial order (e.g. order=1 is linear)
  int nDOF_;                  // total DOFs in element
  T DOF_[1];                  // DOFs

  int normalSignL_;           // trace normal sign with respect to the left element
  int normalSignR_;           // trace normal sign with respect to the right element

  const BasisType* basis_;
  mutable Real phi_[1];                 // temporary storage for basis function evaluation
};


template <class T>
Element<T, TopoD0, Node>::Element( int order, const BasisFunctionCategory& category )
{
  basis_ = BasisFunctionNodeBase::getBasisFunction(order, category);

  order_ = order;
  nDOF_  = basis_->nBasis();
  SANS_ASSERT( nDOF_ == 1 );
  normalSignL_ = 0;
  normalSignR_ = 0;

  for (int n = 0; n < nDOF_; n++)
    DOF_[n] = std::numeric_limits<Real>::max();
}


template <class T>
Element<T, TopoD0, Node>::Element( const BasisFunctionNodeBase* basis )
{
  basis_ = basis;

  order_ = basis_->order();
  nDOF_  = basis_->nBasis();
  SANS_ASSERT( nDOF_ == 1 );
  normalSignL_ = 0;
  normalSignR_ = 0;

  for (int n = 0; n < nDOF_; n++)
    DOF_[n] = std::numeric_limits<Real>::max();
}


template <class T>
Element<T, TopoD0, Node>::Element( const Element& a )
{
  order_ = a.order_;
  nDOF_  = a.nDOF_;
  SANS_ASSERT( nDOF_ == 1 );
  normalSignL_ = a.normalSignL_;
  normalSignR_ = a.normalSignR_;

  for (int n = 0; n < nDOF_; n++)
    DOF_[n] = a.DOF_[n];

  basis_ = a.basis_;
}


template <class T>
Element<T, TopoD0, Node>::~Element()
{
}


template <class T>
Element<T, TopoD0, Node>&
Element<T, TopoD0, Node>::operator=( const Element& a )
{
  if (this != &a)
  {
    order_ = a.order_;
    nDOF_  = a.nDOF_;
    SANS_ASSERT( nDOF_ == 1 );
    normalSignL_ = a.normalSignL_;
    normalSignR_ = a.normalSignR_;

    for (int n = 0; n < nDOF_; n++)
      DOF_[n] = a.DOF_[n];

    basis_ = a.basis_;
  }

  return *this;
}


template <class T>
inline void
Element<T, TopoD0, Node>::evalBasis( Real phi[], int nphi ) const
{
  basis_->evalBasis(phi, nphi);
}


template <class T>
inline void
Element<T, TopoD0, Node>::evalBasisDerivative( Real phis[], int nphi ) const
{
  basis_->evalBasisDerivative(phis, nphi);
}

template <class T>
inline void
Element<T, TopoD0, Node>::evalBasisHessianDerivative( Real phiss[], int nphi ) const
{
  basis_->evalBasisHessianDerivative(phiss, nphi);
}


template <class T>
inline void
Element<T, TopoD0, Node>::evalFromBasis( const Real phi[], int nphi, T& q ) const
{
  SANS_ASSERT(nphi == nDOF_);
  q = 0;
  for (int n = 0; n < nDOF_; n++)
    q += phi[n]*DOF_[n];
}

template <class T>
inline T
Element<T, TopoD0, Node>::eval() const
{
  T q;
  basis_->evalBasis(phi_, nDOF_);

  q = 0;
  for (int n = 0; n < nDOF_; n++)
    q += phi_[n]*DOF_[n];

  return q;
}

template <class T>
inline void
Element<T, TopoD0, Node>::eval( T& q ) const
{

  basis_->evalBasis(phi_, nDOF_);

  q = 0;
  for (int n = 0; n < nDOF_; n++)
    q += phi_[n]*DOF_[n];
}

// project DOFs onto another polynomial order
template <class T>
inline void
Element<T, TopoD0, Node>::projectTo( Element& ElemTo ) const
{
  ElemTo.DOF_[0] = DOF_[0];
}

template <class T>
void
Element<T, TopoD0, Node>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "ElementNode:"
      << "  order_ = " << order_ << "  nDOF_ = " << nDOF_ << std::endl;

  out << indent << "  DOF_ = ";
  for (int n = 0; n < nDOF_; n++)
    out << "(" << DOF_[n] << ") ";
  out << std::endl;
  if (basis_ != NULL)
  {
    out << indent << "  basis_:" << std::endl;
    basis_->dump( indentSize+2, out );
  }
}

}

#endif  // ELEMENTNODE_H
