// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "ElementXFieldJacobianEquilateral.h"

#include <cmath> // sqrt

namespace SANS
{

//----------------------------------------------------------------------------//
/* A struct that contains the inverse Jacobian of the unit equilateral element
 * invJeq is the transformation matrix from the unit equilateral element to the reference element
 * Jeq has dimension (PhysDim x TopoDim), so invJeq has dimension (TopoDim x PhysDim)
 */
//----------------------------------------------------------------------------//

//Lines
template<>
const DLA::MatrixS<TopoD1::D,PhysD1::D,Real>
JacobianEquilateralTransform<PhysD1,TopoD1,Line>::invJeq = DLA::Identity();

//Triangles
template<>
const DLA::MatrixS<TopoD2::D,PhysD2::D,Real>
JacobianEquilateralTransform<PhysD2,TopoD2,Triangle>::invJeq = { {1.0, -1.0/sqrt(3.0)},
                                                                 {0.0,  2.0/sqrt(3.0)} };
//Quads
template<>
const DLA::MatrixS<TopoD2::D,PhysD2::D,Real>
JacobianEquilateralTransform<PhysD2,TopoD2,Quad>::invJeq = DLA::Identity();


//Tets
template<>
const DLA::MatrixS<TopoD3::D,PhysD3::D,Real>
JacobianEquilateralTransform<PhysD3,TopoD3,Tet>::invJeq = { {1.0, -1.0/sqrt(3.0), -1.0/sqrt(6.0)},
                                                            {0.0,  2.0/sqrt(3.0), -1.0/sqrt(6.0)},
                                                            {0.0,  0.0          ,  3.0/sqrt(6.0)} };
//Hexes
template<>
const DLA::MatrixS<TopoD3::D,PhysD3::D,Real>
JacobianEquilateralTransform<PhysD3,TopoD3,Hex>::invJeq = DLA::Identity();

// Pentatope: see accompanying matlab script
template<>
const DLA::MatrixS<TopoD4::D,PhysD4::D,Real>
JacobianEquilateralTransform<PhysD4,TopoD4,Pentatope>::invJeq =
{
 {
   (sqrt(2.0)*sqrt(3.0)*sqrt(5.0)*-2.0)/(sqrt(5.0)*sqrt(1.5E1)*3.0+sqrt(3.0)*5.0),
   (sqrt(2.0)*3.0E1)/(sqrt(5.0)*sqrt(1.5E1)*3.0+sqrt(3.0)*5.0),
   0,
   0
 },
 {
   (sqrt(2.0)*sqrt(1.5E1)*-2.0)/(sqrt(5.0)*sqrt(1.5E1)*3.0+sqrt(3.0)*5.0),
   (sqrt(2.0)*-1.0E1)/(sqrt(5.0)*sqrt(1.5E1)*3.0+sqrt(3.0)*5.0),
   sqrt(2.0)*sqrt(6.0)*(1.0/3.0),
   0,
 },
 {
   (sqrt(2.0)*sqrt(1.5E1)*-2.0)/(sqrt(5.0)*sqrt(1.5E1)*3.0+sqrt(3.0)*5.0),
   (sqrt(2.0)*-1.0E1)/(sqrt(5.0)*sqrt(1.5E1)*3.0+sqrt(3.0)*5.0),
   sqrt(2.0)*sqrt(6.0)*(-1.0/6.0),
   1.0
 },
 {
   (sqrt(2.0)*sqrt(1.5E1)*-2.0)/(sqrt(5.0)*sqrt(1.5E1)*3.0+sqrt(3.0)*5.0),
   (sqrt(2.0)*-1.0E1)/(sqrt(5.0)*sqrt(1.5E1)*3.0+sqrt(3.0)*5.0),
   sqrt(2.0)*sqrt(6.0)*(-1.0/6.0),
  -1.0
 }
};

} //namespace SANS
