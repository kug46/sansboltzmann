// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ELEMENTXFIELDVOLUME_INSTANTIATE

#include "ElementXFieldVolume_impl.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_SVD.h"
#include "LinearAlgebra/DenseLinAlg/InverseLU.h"
#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/tools/cross.h"
#include "ElementXFieldJacobianEquilateral.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// 4-D Cartesian
//----------------------------------------------------------------------------//

//---------------------------------------------------------------------------//
template <class PhysDim, class Topology>
inline Real
ElementXField<PhysDim, TopoD3, Topology>::volume() const
{
  Real s= Topology::centerRef[0];
  Real t= Topology::centerRef[1];
  Real u= Topology::centerRef[2];
  return volume(s, t, u);
}

template <class PhysDim, class Topology>
inline Real
ElementXField<PhysDim, TopoD3, Topology>::volume( const Matrix& J ) const
{
  Real xs, xt, xu;
  Real ys, yt, yu;
  Real zs, zt, zu;
  Real ws, wt, wu;

  Real det;

  xs= J(0, 0);  xt= J(0, 1);  xu= J(0, 2);
  ys= J(1, 0);  yt= J(1, 1);  yu= J(1, 2);
  zs= J(2, 0);  zt= J(2, 1);  zu= J(2, 2);
  ws= J(3, 0);  wt= J(3, 1);  wu= J(3, 2);

  Real s_dot_s= (xs*xs + ys*ys + zs*zs + ws*ws);
  Real s_dot_t= (xs*xt + ys*yt + zs*zt + ws*wt);
  Real s_dot_u= (xs*xu + ys*yu + zs*zu + ws*wu);
  Real t_dot_t= (xt*xt + yt*yt + zt*zt + wt*wt);
  Real t_dot_u= (xt*xu + yt*yu + zt*zu + wt*wu);
  Real u_dot_u= (xu*xu + yu*yu + zu*zu + wu*wu);

  Real underroot= ((u_dot_u)*((s_dot_s)*(t_dot_t) - pow(s_dot_t, 2))
      - (t_dot_u)*((s_dot_s)*(t_dot_u) - (s_dot_t)*(s_dot_u))
      + (s_dot_u)*((s_dot_t)*(t_dot_u) - (t_dot_t)*(s_dot_u)));

  det= sqrt(underroot);

//  SANS_DEVELOPER_EXCEPTION("ElementXField<PhysD4, TopoD3, Topology>::volume - Not implemented yet.");

  return Topology::volumeRef*det;
}

template <class PhysDim, class Topology>
inline Real
ElementXField<PhysDim, TopoD3, Topology>::volume( const Real& sRef, const Real& tRef, const Real& uRef ) const
{
  Matrix J;
  jacobian( sRef, tRef, uRef, J );
  return volume( J );
}

template <class PhysDim, class Topology>
inline Real
ElementXField<PhysDim, TopoD3, Topology>::volume( const QuadraturePoint<TopoD3>& ref ) const
{
  Matrix J;
  jacobian( ref, J );
  return volume( J );
}

//---------------------------------------------------------------------------//
void AddCurvedBBoxPoints(const ElementXField<PhysD4, TopoD3, Tet>& xfldElem, BoundingBox<PhysD4>& bbox)
{

  SANS_DEVELOPER_EXCEPTION("High order elements not yet unit tested in 4D.");

  typedef DLA::VectorS<PhysD4::D,Real> VectorX;
  typedef DLA::VectorS<TopoD3::D,Real> RefCoordCell;

  VectorX X;
  RefCoordCell sRefCell = 0;

  // Grab coordinates on the cell (including on edges) to add to the bounding box
  int kmax = 9;
  Real dRef = 1./(kmax-1);
  for (int ki = 0; ki < kmax; ki++)
  {
    sRefCell[1] = 0;
    for (int kj = ki; kj < kmax; kj++)
    {
      xfldElem.coordinates(sRefCell, X);

      bbox.setBound(X);

      sRefCell[1] += dRef;
    }
    sRefCell[0] += dRef;
  }

}

//---------------------------------------------------------------------------//
template <class PhysDim, class Topology>
BoundingBox<PhysDim>
ElementXField<PhysDim, TopoD3, Topology>::boundingBox() const
{
  BoundingBox<PhysDim> bbox;

  // get the corners of the element
  for (int n = 0; n < Topology::NNode; n++)
    bbox.setBound(DOF_[n]);

  if ( basis_->order()  > 1 )
    AddCurvedBBoxPoints(*this, bbox);

  return bbox;
}

//---------------------------------------------------------------------------//
template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD3, Topology>::impliedMetric(
    const Real& s, const Real& t, const Real& u, TensorSymX& M ) const
{
  SANS_DEVELOPER_EXCEPTION("ElementXField<PhysD4, TopoD3, Topology>::impliedMetric - Not implemented yet.");
}

template <class PhysDim, class Topology>
inline void
ElementXField<PhysDim, TopoD3, Topology>::evalReferenceCoordinateGradient(
    const Real* __restrict phis, const Real* __restrict phit, const Real* __restrict phiu,  VectorX& sgrad, VectorX& tgrad, VectorX& ugrad ) const
{
  SANS_DEVELOPER_EXCEPTION("ElementXField<PhysD4, TopoD3, Topology>::evalReferenceCoordinateGradient - Not implemented yet.");
}


template <class PhysDim, class Topology>
inline void
ElementXField<PhysDim, TopoD3, Topology>::evalReferenceCoordinateHessian(
    const Real& s, const Real& t, const Real& u, TensorSymX& shess, TensorSymX& thess, TensorSymX& uhess ) const
{
  SANS_DEVELOPER_EXCEPTION("ElementXField<PhysD4, TopoD3, Topology>::evalReferenceCoordinateHessian - Not implemented yet.");
}

//----------------------------------------------------------------------------//
// basis hessian
// Returns the hessian of basis functions in cartesian coords, transforms from the master element
template <class PhysDim, class Topology>
template <class RefCoord>
void
ElementXField<PhysDim, TopoD3, Topology>::evalBasisHessian( const RefCoord& sRef,
                                                            const ElementBasis<TopoD3, Topology>& fldElem,
                                                            TensorSymX hessphi[], int nphi ) const
{
   SANS_DEVELOPER_EXCEPTION("ElementXField<PhysD4, TopoD3, Topology>::evalBasisHessian - Not implemented yet.");
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD3, Topology>::unitNormal(
    const RefCoordType& sRef, VectorX& N ) const
{
  evalBasisDerivative( sRef[0], sRef[1], sRef[2], phis_, phit_, phiu_, nDOF_ );

  unitNormal(phis_, phit_, phiu_, N);
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD3, Topology>::unitNormal(
    const QuadraturePoint<TopoD3>& ref, VectorX& N ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    unitNormal(ref.ref, N);
    return;
  }

  const QuadratureBasisPointValues<TopoD>& cache = QuadratureCacheValues<TopoD3, Topology>::get( pointStoreCell_, ref, faceSign_ );
  const std::vector<Real>& phis = cache.dphi[0];
  const std::vector<Real>& phit = cache.dphi[1];
  const std::vector<Real>& phiu = cache.dphi[2];

  unitNormal(phis.data(), phit.data(), phiu.data(), N);
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD3, Topology>::unitNormal(
    const QuadratureCellTracePoint<TopoD3>& ref, VectorX& N ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    unitNormal(ref.ref, N);
    return;
  }

  const QuadratureBasisPointValues<TopoD>& cache = QuadratureCacheValues<TopoD3, Topology>::get(pointStoreTrace_, ref, faceSign_);
  const std::vector<Real>& phis = cache.dphi[0];
  const std::vector<Real>& phit = cache.dphi[1];
  const std::vector<Real>& phiu = cache.dphi[2];

  unitNormal(phis.data(), phit.data(), phiu.data(), N);
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD3, Topology>::unitNormal(
    const Real phis[], const Real phit[], const Real phiu[], VectorX& N ) const
{
  Real xs, xt, xu;
  Real ys, yt, yu;
  Real zs, zt, zu;
  Real ws, wt, wu;

  xs= 0;  xt= 0;  xu= 0;
  ys= 0;  yt= 0;  yu= 0;
  zs= 0;  zt= 0;  zu= 0;
  ws= 0;  wt= 0;  wu= 0;

  for (int n= 0; n < nDOF_; n++)
  {
    xs+= phis[n]*DOF_[n](0);
    ys+= phis[n]*DOF_[n](1);
    zs+= phis[n]*DOF_[n](2);
    ws+= phis[n]*DOF_[n](3);

    xt+= phit[n]*DOF_[n](0);
    yt+= phit[n]*DOF_[n](1);
    zt+= phit[n]*DOF_[n](2);
    wt+= phit[n]*DOF_[n](3);

    xu+= phiu[n]*DOF_[n](0);
    yu+= phiu[n]*DOF_[n](1);
    zu+= phiu[n]*DOF_[n](2);
    wu+= phiu[n]*DOF_[n](3);
  }

//  if (nDOF_!=4) SANS_DEVELOPER_EXCEPTION("cannot compute normal for high-order tetrahedra in 4d");
//  SANS_ASSERT( TopoD3::D==PhysDim::D-1 );
//
//  DLA::MatrixS<TopoD3::D,PhysDim::D,Real> A(0);
//  for (int k=0;k<TopoD3::D;k++)
//  for (int d=0;d<PhysDim::D;d++)
//    A(k,d) = DOF_[k+1](d) -DOF_[0](d);
//
//  Real xs3= A(0, 0);   SANS_ASSERT(xs3 == xs);
//  Real ys3= A(0, 1);   SANS_ASSERT(ys3 == ys);
//  Real zs3= A(0, 2);   SANS_ASSERT(zs3 == zs);
//  Real ws3= A(0, 3);   SANS_ASSERT(ws3 == ws);
//
//  Real xt3= A(1, 0);   SANS_ASSERT(xt3 == xt);
//  Real yt3= A(1, 1);   SANS_ASSERT(yt3 == yt);
//  Real zt3= A(1, 2);   SANS_ASSERT(zt3 == zt);
//  Real wt3= A(1, 3);   SANS_ASSERT(wt3 == wt);
//
//  Real xu3= A(2, 0);   SANS_ASSERT(xu3 == xu);
//  Real yu3= A(2, 1);   SANS_ASSERT(yu3 == yu);
//  Real zu3= A(2, 2);   SANS_ASSERT(zu3 == zu);
//  Real wu3= A(2, 3);   SANS_ASSERT(wu3 == wu);

  // determinants
  N[0]= -(ys*zt*wu - ys*wt*zu) + (yt*zs*wu - yt*ws*zu) - (yu*zs*wt - yu*ws*zt);
  N[1]=  (xs*zt*wu - xs*wt*zu) - (xt*zs*wu - xt*ws*zu) + (xu*zs*wt - xu*ws*zt);
  N[2]= -(xs*yt*wu - xs*wt*yu) + (xt*ys*wu - xt*ws*yu) - (xu*ys*wt - xu*ws*yt);
  N[3]=  (xs*yt*zu - xs*zt*yu) - (xt*ys*zu - xt*zs*yu) + (xu*ys*zt - xu*zs*yt);

  N /= sqrt( dot(N,N) );
}

template class ElementXField<PhysD4, TopoD3, Tet>;

}
