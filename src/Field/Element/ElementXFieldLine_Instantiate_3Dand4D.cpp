// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ELEMENTXFIELDLINE_INSTANTIATE
#include "ElementXFieldLine_impl.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Specialization: 3-D/4-D grid field line element
//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//
// compute implied metric
template<class PhysDim>
void
ElementXFieldLineBase<PhysDim>::impliedMetric( const Real& s, TensorSymX& M ) const
{
  SANS_DEVELOPER_EXCEPTION("ElementXFieldLineBase<PhysD3>::impliedMetric - Not implemented yet.");

#if 0
  Matrix Jref; //jacobian from reference element to physical element
  jacobian( s, Jref );

  // transformation matrix from the unit equilateral element to the reference element (in 1D)
  const DLA::MatrixS<PhysD1::D,TopoD,Real>& invJeq = JacobianEquilateralTransform<PhysD1,TopoD1,Line>::invJeq;

  Matrix J = Jref*invJeq; //jacobian from equilateral element to physical element

  TensorSymX JJt = J*Transpose(J);

  SANS_ASSERT( D == 3 ); // only implemented for 3D, not 4D

  // M = inv(JJt)
  Real det =   JJt(0,0)*( JJt(1,1)*JJt(2,2) - JJt(1,2)*JJt(2,1) )
             - JJt(0,1)*( JJt(1,0)*JJt(2,2) - JJt(1,2)*JJt(2,0) )
             + JJt(0,2)*( JJt(1,0)*JJt(2,1) - JJt(1,1)*JJt(2,0) );

  M(0,0) = (JJt(1,1)*JJt(2,2) - JJt(1,2)*JJt(2,1)) / det;
  M(1,0) = (JJt(1,2)*JJt(2,0) - JJt(1,0)*JJt(2,2)) / det;
  M(1,1) = (JJt(0,0)*JJt(2,2) - JJt(0,2)*JJt(2,0)) / det;
  M(2,0) = (JJt(1,0)*JJt(2,1) - JJt(1,1)*JJt(2,0)) / det;
  M(2,1) = (JJt(0,1)*JJt(2,0) - JJt(0,0)*JJt(2,1)) / det;
  M(2,2) = (JJt(0,0)*JJt(1,1) - JJt(0,1)*JJt(1,0)) / det;
#endif
}

template <class PhysDim>
void
ElementXFieldLineBase<PhysDim>::evalReferenceCoordinateHessian( const Real& sRef, TensorSymX& shess ) const
{
  SANS_DEVELOPER_EXCEPTION("Not yet implemented for manifolds.");
#if 0
  SANS_ASSERT( PhysDim::D == 2 ); // specialized for 1-D

  SANS_ASSERT(nDOF_ > 0);  //Suppress clang analyzer warning

  // shess is d^2s/dx^2
  Real phiss[nDOF_];
  Real* phis = phi_;
  Real xs, xss;

  basis_->evalBasisDerivative( sRef, phis, nDOF_ );
  basis_->evalBasisHessianDerivative( sRef, phiss, nDOF_ );
  xs = 0; xss = 0;
  for (int n = 0; n < nDOF_; n++)
  {
    xs  += phis[n]*DOF_[n](0);
    xss += phiss[n]*DOF_[n](0);
  }

  shess(0,0) = -xss/(xs*xs*xs); // 1D shortcut
#endif
}

//----------------------------------------------------------------------------//
// Explicit instantiation
//----------------------------------------------------------------------------//
template class ElementXFieldLineBase<PhysD3>;
template class ElementXFieldLineBase<PhysD4>;

}
