// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ELEMENTXFIELDAREA_H
#define ELEMENTXFIELDAREA_H

// Area (triangle/quad) grid field element

#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "BasisFunction/BasisFunctionArea.h"
#include "BasisFunction/BasisPointDerivative.h"
#include "BoundingBox.h"
#include "ElementArea.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Topologically 2-D grid field element: area (triangle/quad)
//
// member functions:
//   .area            triangle area
//   .coordinates     physical coordinates given reference triangle coordinates
//   .evalBasis       basis function
//   .evalBasisGradient                 basis function gradient wrt reference coordinates
//   .evalBasisHessian                  basis function hessian wrt reference coordinates
//   .evalReferenceCoordinateGradient   reference coordinates gradient wrt (x,y)
//   .evalReferenceCoordinateHessian    reference coordinates hessian wrt (x,y)
//
//   .dump            debug dump of private data
//----------------------------------------------------------------------------//

template <class PhysDim, class Topology>
class ElementXField<PhysDim, TopoD2, Topology> : public Element< DLA::VectorS<PhysDim::D,Real>, TopoD2, Topology >
{
public:
  static const int D = PhysDim::D;             // physical dimensions
  static const int TopoD = TopoD2::D;          // topological dimensions
  typedef Element< DLA::VectorS<D,Real>, TopoD2, Topology > BaseType;
  typedef BasisFunctionAreaBase<Topology> BasisType;
  typedef DLA::VectorS<TopoD2::D,Real> RefCoordType;

  typedef DLA::VectorS<D,Real> VectorX;        // coordinates vector
  typedef DLA::MatrixSymS<D,Real> TensorSymX;  // physical Hessian d(u)/d(x_i x_j) - e.g. implied metric
  typedef DLA::MatrixS<D,TopoD,Real> Matrix;   // element Jacobian d(x_i)/d(s_j)
  typedef DLA::VectorS<D, DLA::MatrixSymS<TopoD,Real>> TensorSymHessian; // element Hessian d^2(x_i)/d(s_j s_k)

  explicit ElementXField( const BasisType* basis ) : BaseType(basis) {}
  ElementXField( int order, const BasisFunctionCategory& category ) : BaseType(order,category) {}
  ElementXField( const ElementXField& Elem ) : BaseType(Elem) {}
  ~ElementXField() {}

  ElementXField& operator=( const ElementXField& Elem ) { BaseType::operator=(Elem); return *this; }

  Real area() const;
  Real area( const Real& sRef, const Real& tRef ) const;
  Real area( const QuadraturePoint<TopoD2>& sRef ) const;
  void coordinates( const Real& sRef, const Real& tRef, VectorX& X ) const { BaseType::eval(sRef,tRef,X); }
  void coordinates( const RefCoordType& Ref, VectorX& X ) const { BaseType::eval( Ref[0], Ref[1], X ); }
  void coordinates( const QuadraturePoint<TopoD2>& Ref, VectorX& X ) const { BaseType::eval( Ref, X ); }
  void coordinates( const QuadratureCellTracePoint<TopoD2>& Ref, VectorX& X ) const { BaseType::eval( Ref, X ); }

  BoundingBox<PhysDim> boundingBox() const;

  void jacobian( Matrix& J ) const;
  void jacobian( const RefCoordType& Ref, Matrix& J ) const { jacobian(Ref[0], Ref[1], J); }
  void jacobian( const QuadraturePoint<TopoD2>& Ref, Matrix& J ) const;
  void jacobian( const Real& s, const Real& t, Matrix& J ) const;

  void hessian( const RefCoordType& Ref, TensorSymHessian& H ) const { hessian(Ref[0], Ref[1], H); }
  void hessian( const Real& s, const Real& t, TensorSymHessian& H ) const;

  Real jacobianDeterminant() const { return jacobianDeterminant(Topology::centerRef); }
  Real jacobianDeterminant( const RefCoordType& Ref ) const { return area(Ref[0], Ref[1]); }
  Real jacobianDeterminant( const QuadraturePoint<TopoD2>& Ref ) const { return area(Ref); }

  void impliedMetric( TensorSymX& M ) const;
  void impliedMetric( const RefCoordType& Ref, TensorSymX& M ) const { impliedMetric(Ref[0], Ref[1], M); }
  void impliedMetric( const Real& s, const Real& t, TensorSymX& M ) const;

  void evalReferenceCoordinateGradient(const Real& s, const Real& t, VectorX& sgrad, VectorX& tgrad ) const;
  void evalReferenceCoordinateGradient(const RefCoordType& Ref, VectorX& sgrad, VectorX& tgrad ) const;
  void evalReferenceCoordinateGradient(const QuadraturePoint<TopoD2>& Ref, VectorX& sgrad, VectorX& tgrad ) const;
  void evalReferenceCoordinateGradient(const QuadratureCellTracePoint<TopoD2>& Ref, VectorX& sgrad, VectorX& tgrad ) const;

  void evalReferenceCoordinateHessian(const Real& s, const Real& t, TensorSymX& shess, TensorSymX& thess ) const;
  void evalReferenceCoordinateHessian(const RefCoordType& Ref, TensorSymX& shess, TensorSymX& thess ) const
  {
    evalReferenceCoordinateHessian(Ref[0], Ref[1], shess, thess);
  }
  void evalReferenceCoordinateHessian(const QuadraturePoint<TopoD2>& Ref, TensorSymX& shess, TensorSymX& thess ) const
  {
    // TODO: Cache hessian
    evalReferenceCoordinateHessian(Ref.ref[0], Ref.ref[1], shess, thess);
  }

  using BaseType::eval;
  using BaseType::evalBasisDerivative;

  void evalBasisGradient( const RefCoordType& sRef,
                          const ElementBasis<TopoD2, Topology>& fldElem,
                          VectorX gradphi[], int nphi ) const;
  void evalBasisGradient( const QuadraturePoint<TopoD2>& sRef,
                          const ElementBasis<TopoD2, Topology>& fldElem,
                          VectorX gradphi[], int nphi ) const;
  void evalBasisGradient( const QuadratureCellTracePoint<TopoD2>& sRef,
                          const ElementBasis<TopoD2, Topology>& fldElem,
                          VectorX gradphi[], int nphi ) const;
  void evalBasisGradient( const BasisPointDerivative<TopoD2::D>& dphi,
                          VectorX gradphi[], int nphi ) const;


  void evalBasisHessian( const RefCoordType& sRef,
                         const ElementBasis<TopoD2, Topology>& fldElem,
                         TensorSymX hessphi[], int nphi ) const;
  void evalBasisHessian( const QuadraturePoint<TopoD2>& sRef,
                         const ElementBasis<TopoD2, Topology>& fldElem,
                         TensorSymX hessphi[], int nphi ) const;
  void evalBasisHessian( const QuadratureCellTracePoint<TopoD2>& sRef,
                         const ElementBasis<TopoD2, Topology>& fldElem,
                         TensorSymX hessphi[], int nphi ) const;

  template <class ElemT>
  void evalGradient( const RefCoordType& sRef,
                     const Element<ElemT, TopoD2, Topology>& qfldElem,
                     DLA::VectorS<PhysDim::D, ElemT>& gradq ) const;
  template <class ElemT>
  void evalGradient( const QuadraturePoint<TopoD2>& sRef,
                     const Element<ElemT, TopoD2, Topology>& qfldElem,
                     DLA::VectorS<PhysDim::D, ElemT>& gradq ) const;
  template <class ElemT>
  void evalGradient( const QuadratureCellTracePoint<TopoD2>& sRef,
                     const Element<ElemT, TopoD2, Topology>& qfldElem,
                     DLA::VectorS<PhysDim::D, ElemT>& gradq ) const;

  template <class ElemT>
  void evalHessian( const RefCoordType& sRef,
                    const Element<ElemT, TopoD2, Topology>& qfldElem,
                    DLA::MatrixSymS<PhysDim::D, ElemT>& hessq ) const;
  template <class ElemT>
  void evalHessian( const QuadraturePoint<TopoD2>& sRef,
                    const Element<ElemT, TopoD2, Topology>& qfldElem,
                    DLA::MatrixSymS<PhysDim::D, ElemT>& hessq ) const;

  //----------------------------------------------------------------------------//
  // Specialized for 3-D grid field area element
  //----------------------------------------------------------------------------//
  void unitNormal( const RefCoordType& sRef, VectorX& N ) const;
  void unitNormal( const QuadraturePoint<TopoD2>& sRef, VectorX& N ) const;
  void unitNormal( const QuadratureCellTracePoint<TopoD2>& sRef, VectorX& N ) const;

  void dumpTecplot( std::ostream& out = std::cout ) const;

protected:

  Real area( const Matrix& J ) const;
  void jacobian( const Real* __restrict phis, const Real* __restrict phit, Matrix& J ) const;
  void evalReferenceCoordinateGradient(const Real* __restrict phis, const Real* __restrict phit, VectorX& sgrad, VectorX& tgrad ) const;

  template<class RefCoord>
  void evalBasisGradient( const RefCoord& sRef,
                          const ElementBasis<TopoD2, Topology>& fldElem,
                          VectorX gradphi[], int nphi ) const;

  template<class RefCoord>
  void evalBasisHessian( const RefCoord& sRef,
                         const ElementBasis<TopoD2, Topology>& fldElem,
                         TensorSymX hessphi[], int nphi ) const;

  template <class RefCoord, class ElemT>
  void evalGradient( const RefCoord& sRef,
                     const Element<ElemT, TopoD2, Topology>& qfldElem,
                     DLA::VectorS<PhysDim::D, ElemT>& gradq ) const;

  template <class RefCoord, class ElemT>
  void evalHessian( const RefCoord& sRef,
                    const Element<ElemT, TopoD2, Topology>& qfldElem,
                    DLA::MatrixSymS<PhysDim::D, ElemT>& hessq ) const;

  //----------------------------------------------------------------------------//
  // Specialized for 3-D grid field area element
  //----------------------------------------------------------------------------//
  void unitNormal( const Real phis[], const Real phit[], VectorX& N ) const;

  using BaseType::nDOF_;
  using BaseType::DOF_;
  using BaseType::phis_;
  using BaseType::phit_;
  using BaseType::phiss_;
  using BaseType::phist_;
  using BaseType::phitt_;
  using BaseType::basis_;
  using BaseType::edgeSign_;
  using BaseType::pointStoreCell_;
  using BaseType::pointStoreTrace_;
};


//----------------------------------------------------------------------------//
// computes the gradient of a solution
template <class PhysDim, class Topology>
template <class ElemT>
void
ElementXField<PhysDim, TopoD2, Topology>::evalGradient( const RefCoordType& sRef,
                                                        const Element<ElemT, TopoD2, Topology>& qfldElem,
                                                        DLA::VectorS<PhysDim::D, ElemT>& gradq ) const
{
  evalGradient<RefCoordType, ElemT>(sRef, qfldElem, gradq);
}

template <class PhysDim, class Topology>
template <class ElemT>
void
ElementXField<PhysDim, TopoD2, Topology>::evalGradient( const QuadraturePoint<TopoD2>& sRef,
                                                        const Element<ElemT, TopoD2, Topology>& qfldElem,
                                                        DLA::VectorS<PhysDim::D, ElemT>& gradq ) const
{
  evalGradient<QuadraturePoint<TopoD2>, ElemT>(sRef, qfldElem, gradq);
}

template <class PhysDim, class Topology>
template <class ElemT>
void
ElementXField<PhysDim, TopoD2, Topology>::evalGradient( const QuadratureCellTracePoint<TopoD2>& sRef,
                                                        const Element<ElemT, TopoD2, Topology>& qfldElem,
                                                        DLA::VectorS<PhysDim::D, ElemT>& gradq ) const
{
  evalGradient<QuadratureCellTracePoint<TopoD2>, ElemT>(sRef, qfldElem, gradq);
}

template <class PhysDim, class Topology>
template <class RefCoord, class ElemT>
void
ElementXField<PhysDim, TopoD2, Topology>::evalGradient( const RefCoord& sRef,
                                                        const Element<ElemT, TopoD2, Topology>& qfldElem,
                                                        DLA::VectorS<PhysDim::D, ElemT>& gradq ) const
{
  const int nBasis = qfldElem.nDOF();         // total solution DOFs in element
  std::vector<VectorX> gradphi(nBasis);       // Basis gradient

  evalBasisGradient( sRef, qfldElem, gradphi.data(), nBasis );
  qfldElem.evalFromBasis( gradphi.data(), nBasis, gradq );
}


//----------------------------------------------------------------------------//
// computes the Hessian of a solution
template <class PhysDim, class Topology>
template <class ElemT>
void
ElementXField<PhysDim, TopoD2, Topology>::evalHessian( const RefCoordType& sRef,
                                                       const Element<ElemT, TopoD2, Topology>& qfldElem,
                                                       DLA::MatrixSymS<PhysDim::D, ElemT>& hessq ) const
{
  evalHessian<RefCoordType, ElemT>(sRef, qfldElem, hessq);
}

template <class PhysDim, class Topology>
template <class ElemT>
void
ElementXField<PhysDim, TopoD2, Topology>::evalHessian( const QuadraturePoint<TopoD2>& sRef,
                                                       const Element<ElemT, TopoD2, Topology>& qfldElem,
                                                       DLA::MatrixSymS<PhysDim::D, ElemT>& hessq ) const
{
  evalHessian<QuadraturePoint<TopoD2>, ElemT>(sRef, qfldElem, hessq);
}

template <class PhysDim, class Topology>
template <class RefCoord, class ElemT>
void
ElementXField<PhysDim, TopoD2, Topology>::evalHessian( const RefCoord& sRef,
                                                       const Element<ElemT, TopoD2, Topology>& qfldElem,
                                                       DLA::MatrixSymS<PhysDim::D, ElemT>& hessq ) const
{
  SANS_ASSERT( PhysDim::D == 2 ); // TODO: currently only useful for in physically 2-D space; not for manifolds

  const int nBasis = qfldElem.nDOF();          // total solution DOFs in element
  std::vector<TensorSymX> hessphi(nBasis);     // Basis gradient

  evalBasisHessian( sRef, qfldElem, hessphi.data(), nBasis );
  qfldElem.evalFromBasis( hessphi.data(), nBasis, hessq );
}

}

#endif  // ELEMENTXFIELDAREA_H
