// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "UniqueElem.h"

#include "tools/SANSException.h"
#include "tools/output_std_vector.h"

#include <algorithm> // std::sort


namespace SANS
{

//---------------------------------------------------------------------------//
UniqueElem::UniqueElem( const TopologyTypes topo, const std::vector<int>& nodes )
  : topo(topo), sortedNodes_(nodes)
{
  // sort the nodes
  std::sort( sortedNodes_.begin(), sortedNodes_.end() );
}

//---------------------------------------------------------------------------//
UniqueElem::UniqueElem( const TopologyTypes topo, std::vector<int>&& nodes )
  : topo(topo), sortedNodes_(std::move(nodes))
{
  // sort the nodes
  std::sort( sortedNodes_.begin(), sortedNodes_.end() );
}

//---------------------------------------------------------------------------//
UniqueElem::UniqueElem( const int node0, const int node1 )
  : topo(eLine)
{
  SANS_ASSERT(node0 != node1);

  // sort the nodes
  if (node0 < node1) sortedNodes_ = {node0, node1};
  else               sortedNodes_ = {node1, node0};
}

//---------------------------------------------------------------------------//
UniqueElem::UniqueElem( const TopologyTypes topo, const std::array<int,3>& nodes )
  : topo(topo), sortedNodes_(nodes.begin(), nodes.end())
{
  // sort the nodes
  std::sort( sortedNodes_.begin(), sortedNodes_.end() );
}

//---------------------------------------------------------------------------//
UniqueElem::UniqueElem()
  : topo(eNode) {}

//---------------------------------------------------------------------------//
UniqueElem::UniqueElem( const UniqueElem& elem )
  : topo(elem.topo), sortedNodes_(elem.sortedNodes_) {}

//---------------------------------------------------------------------------//
UniqueElem::UniqueElem( UniqueElem&& elem )
  : topo(elem.topo), sortedNodes_(std::move(elem.sortedNodes_)) {}

//---------------------------------------------------------------------------//
UniqueElem&
UniqueElem::operator=( const UniqueElem& elem )
{
  if (&elem != this)
  {
    const_cast<TopologyTypes&>(topo) = elem.topo;
    sortedNodes_ = elem.sortedNodes_;
  }
  return *this;
}

//---------------------------------------------------------------------------//
UniqueElem&
UniqueElem::operator=( UniqueElem&& elem )
{
  if (&elem != this)
  {
    const_cast<TopologyTypes&>(topo) = elem.topo;
    sortedNodes_ = std::move(elem.sortedNodes_);
  }
  return *this;
}

//---------------------------------------------------------------------------//
bool
UniqueElem::operator<( const UniqueElem& elem ) const
{
  // Check if the topology is less
  if (topo < elem.topo)
    return true;

  // Leixgraphically compare the sorted nodes (google it)
  // This is also what it used for std::array operator<
  return std::lexicographical_compare(sortedNodes_.begin(), sortedNodes_.end(),
                                      elem.sortedNodes_.begin(), elem.sortedNodes_.end());
}

//---------------------------------------------------------------------------//
bool
UniqueElem::operator==( const UniqueElem& elem ) const
{
  // Check that the topology and nodes match
  bool equal = (topo == elem.topo);
  equal = equal && (sortedNodes_.size() == elem.sortedNodes_.size() );
  std::size_t n = sortedNodes_.size();

  // Check that all node numbers are equal
  for (std::size_t i = 0; (i < n) && equal; i++)
    equal = equal && (sortedNodes_[i] == elem.sortedNodes_[i]);

  return equal;
}

//---------------------------------------------------------------------------//
std::ostream&
operator<<( std::ostream& out, const UniqueElem& elem )
{
  out << "topo ";
  out << elem.topo;

  out << " sortedNodes_ ";
  out << elem.sortedNodes_;

  return out;
}

} // namespace SANS
