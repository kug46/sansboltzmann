// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(ELEMENTXFIELDSPACETIME_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include <iomanip> // std::setprecision

#include "ElementXFieldSpacetime.h"
#include "ReferenceElementMesh.h"

#include "Field/output_Tecplot/TecTopo.h"

namespace SANS
{


//---------------------------------------------------------------------------//
template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD4, Topology>::evalReferenceCoordinateGradient(
    const Real& s, const Real& t, const Real& u, const Real& v, VectorX& sgrad, VectorX& tgrad, VectorX& ugrad, VectorX& vgrad ) const
{
  evalBasisDerivative( s, t, u, v, phis_, phit_, phiu_, phiv_, nDOF_ );

  evalReferenceCoordinateGradient( phis_, phit_, phiu_, phiv_, sgrad, tgrad, ugrad, vgrad );
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD4, Topology>::evalReferenceCoordinateGradient(
    const RefCoordType& sRef, VectorX& sgrad, VectorX& tgrad, VectorX& ugrad, VectorX& vgrad ) const
{

  evalBasisDerivative( sRef[0], sRef[1], sRef[2], sRef[3], phis_, phit_, phiu_, phiv_, nDOF_ );

  evalReferenceCoordinateGradient( phis_, phit_, phiu_, phiv_, sgrad, tgrad, ugrad, vgrad );
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD4, Topology>::evalReferenceCoordinateGradient(
    const QuadraturePoint<TopoD4>& ref, VectorX& sgrad, VectorX& tgrad, VectorX& ugrad, VectorX& vgrad ) const
{

  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalReferenceCoordinateGradient(ref.ref[0], ref.ref[1], ref.ref[2], ref.ref[3], sgrad, tgrad, ugrad, vgrad);
    return;
  }

  const QuadratureBasisPointValues<TopoD>& cache = QuadratureCacheValues<TopoD4, Topology>::get( pointStoreCell_, ref, faceSign_ );
  const std::vector<Real>& phis = cache.dphi[0];
  const std::vector<Real>& phit = cache.dphi[1];
  const std::vector<Real>& phiu = cache.dphi[2];
  const std::vector<Real>& phiv = cache.dphi[3];

  evalReferenceCoordinateGradient( phis.data(), phit.data(), phiu.data(), phiv.data(), sgrad, tgrad, ugrad, vgrad );
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD4, Topology>::evalReferenceCoordinateGradient(
    const QuadratureCellTracePoint<TopoD4>& ref, VectorX& sgrad, VectorX& tgrad, VectorX& ugrad, VectorX& vgrad ) const
{
//  SANS_DEVELOPER_EXCEPTION("implement");
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    evalReferenceCoordinateGradient(ref.ref[0], ref.ref[1], ref.ref[2], ref.ref[3], sgrad, tgrad, ugrad, vgrad);
    return;
  }

  const QuadratureBasisPointValues<TopoD>& cache = QuadratureCacheValues<TopoD4, Topology>::get(pointStoreTrace_, ref, faceSign_);
  const std::vector<Real>& phis = cache.dphi[0];
  const std::vector<Real>& phit = cache.dphi[1];
  const std::vector<Real>& phiu = cache.dphi[2];
  const std::vector<Real>& phiv = cache.dphi[3];

  evalReferenceCoordinateGradient( phis.data(), phit.data(), phiu.data(), phiv.data(), sgrad, tgrad, ugrad, vgrad );
}

//---------------------------------------------------------------------------//
template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD4, Topology>::jacobian( Matrix& J ) const
{
  Real s = Topology::centerRef[0];
  Real t = Topology::centerRef[1];
  Real u = Topology::centerRef[2];
  Real v = Topology::centerRef[3];
  jacobian( s, t, u, v, J );
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD4, Topology>::jacobian( const Real& s, const Real& t, const Real& u, const Real& v, Matrix& J ) const
{
  evalBasisDerivative( s, t, u, v, phis_, phit_, phiu_, phiv_, nDOF_ );

  jacobian(phis_, phit_, phiu_, phiv_, J);
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD4, Topology>::jacobian( const QuadraturePoint<TopoD4>& ref, Matrix& J ) const
{
  // Revert to computing basis if the quadrature rule is not actually set. Happens with testing for example.
  if ( unlikely(ref.rule == QuadratureRule::eNone) )
  {
    jacobian(ref.ref[0], ref.ref[1], ref.ref[2], ref.ref[3], J);
    return;
  }

  const QuadratureBasisPointValues<TopoD>& cache = QuadratureCacheValues<TopoD4, Topology>::get( pointStoreCell_, ref, faceSign_ );
  const std::vector<Real>& phis = cache.dphi[0];
  const std::vector<Real>& phit = cache.dphi[1];
  const std::vector<Real>& phiu = cache.dphi[2];
  const std::vector<Real>& phiv = cache.dphi[3];

  jacobian(phis.data(), phit.data(), phiu.data(), phiv.data(), J);
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD4, Topology>::jacobian(
    const Real* __restrict phis, const Real* __restrict phit, const Real* __restrict phiu, const Real* __restrict phiv, Matrix& J ) const
{
  J = 0;
  for (int n = 0; n < nDOF_; n++)
    for (int d = 0; d < D; d++)
    {
      J(d,0) += phis[n]*DOF_[n](d); //d(x_i)/ds
      J(d,1) += phit[n]*DOF_[n](d); //d(x_i)/dt
      J(d,2) += phiu[n]*DOF_[n](d); //d(x_i)/du
      J(d,3) += phiv[n]*DOF_[n](d); //d(x_i)/dv
    }
}

//---------------------------------------------------------------------------//
template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD4, Topology>::hessian( const Real& s, const Real& t, const Real& u, const Real& v,
                                                   TensorSymHessian& H ) const
{

  SANS_ASSERT(nDOF_ > 0); //To please the clang compiler

  // second derivative of basis wrt (s,t,u)
  basis_->evalBasisHessianDerivative(s, t, u, v, faceSign_,
                                     phiss_,
                                     phist_, phitt_,
                                     phisu_, phitu_, phiuu_,
                                     phisv_, phitv_, phiuv_, phivv_, nDOF_);

  H= 0;
  for (int n= 0; n < nDOF_; n++)
    for (int d= 0; d < D; d++)
    {
      H(d)(0, 0) += phiss_[n]*DOF_[n](d); // d^2(x_i)/d(s^2)
      H(d)(1, 0) += phist_[n]*DOF_[n](d); // d^2(x_i)/d(st)
      H(d)(1, 1) += phitt_[n]*DOF_[n](d); // d^2(x_i)/d(t^2)
      H(d)(2, 0) += phisu_[n]*DOF_[n](d); // d^2(x_i)/d(su)
      H(d)(2, 1) += phitu_[n]*DOF_[n](d); // d^2(x_i)/d(tu)
      H(d)(2, 2) += phiuu_[n]*DOF_[n](d); // d^2(x_i)/d(u^2)
      H(d)(3, 0) += phisv_[n]*DOF_[n](d); // d^2(x_i)/d(sv)
      H(d)(3, 1) += phitv_[n]*DOF_[n](d); // d^2(x_i)/d(tv)
      H(d)(3, 2) += phiuv_[n]*DOF_[n](d); // d^2(x_i)/d(uv)
      H(d)(3, 3) += phivv_[n]*DOF_[n](d); // d^2(x_i)/d(vv)
    }

//  SANS_DEVELOPER_EXCEPTION("unit test me!");

}

//----------------------------------------------------------------------------//
// basis gradient
template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD4, Topology>::evalBasisGradient( const RefCoordType& sRef,
                                                             const ElementBasis<TopoD4, Topology>& fldElem,
                                                             VectorX gradphi[], const int nphi ) const
{
  evalBasisGradient<RefCoordType>(sRef, fldElem, gradphi, nphi);
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD4, Topology>::evalBasisGradient( const QuadraturePoint<TopoD4>& sRef,
                                                             const ElementBasis<TopoD4, Topology>& fldElem,
                                                             VectorX gradphi[], const int nphi ) const
{
  evalBasisGradient<QuadraturePoint<TopoD4>>(sRef, fldElem, gradphi, nphi);
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD4, Topology>::evalBasisGradient( const QuadratureCellTracePoint<TopoD4>& sRef,
                                                             const ElementBasis<TopoD4, Topology>& fldElem,
                                                             VectorX gradphi[], int nphi ) const
{
  evalBasisGradient<QuadratureCellTracePoint<TopoD4>>(sRef, fldElem, gradphi, nphi);
}

template <class PhysDim, class Topology>
template <class RefCoord>
void
ElementXField<PhysDim, TopoD4, Topology>::evalBasisGradient( const RefCoord& sRef,
                                                             const ElementBasis<TopoD4, Topology>& fldElem,
                                                             VectorX gradphi[], int nphi ) const
{
//  SANS_DEVELOPER_EXCEPTION("implement");
  const int nBasis = fldElem.nDOF();   // total solution DOFs in element
  SANS_ASSERT(nphi == nBasis);

  VectorX sgrad, tgrad, ugrad, vgrad;     // gradients of reference coordinates (s,t,u)
  const Real *phis, *phit, *phiu, *phiv;  // derivatives of solution basis wrt (s,t,u)

  evalReferenceCoordinateGradient( sRef, sgrad, tgrad, ugrad, vgrad );
  fldElem.evalBasisDerivative( sRef, &phis, &phit, &phiu, &phiv );

  for (int n = 0; n < nphi; n++)
    gradphi[n] = phis[n]*sgrad + phit[n]*tgrad + phiu[n]*ugrad +phiv[n]*vgrad;
}

//----------------------------------------------------------------------------//
// basis hessian

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD4, Topology>::evalBasisHessian( const RefCoordType& sRef,
                                                            const ElementBasis<TopoD4, Topology>& fldElem,
                                                            TensorSymX hessphi[], int nphi ) const
{
  evalBasisHessian<RefCoordType>(sRef, fldElem, hessphi, nphi);
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD4, Topology>::evalBasisHessian( const QuadraturePoint<TopoD4>& sRef,
                                                            const ElementBasis<TopoD4, Topology>& fldElem,
                                                            TensorSymX hessphi[], int nphi ) const
{
  evalBasisHessian<QuadraturePoint<TopoD4>>(sRef, fldElem, hessphi, nphi);
}


template <class PhysDim, class Topology>
template <class RefCoord>
void
ElementXField<PhysDim, TopoD4, Topology>::evalBasisHessian( const RefCoord& sRef,
                                                            const ElementBasis<TopoD4, Topology>& fldElem,
                                                            TensorSymX hessphi[], const int nphi ) const
{
  // SANS_DEVELOPER_EXCEPTION("implement");
  SANS_ASSERT( PhysDim::D == 4 ); // TODO: currently only useful for physically 4-D space; not for manifolds

  const int nBasis = fldElem.nDOF();   // total solution DOFs in element
  SANS_ASSERT(nphi == nBasis);

  VectorX sgrad, tgrad, ugrad, vgrad;         // gradients of reference coordinates (s,t,u)
  TensorSymX shess, thess, uhess, vhess;      // hessians of reference coordinates (s,t,u)
  const Real *phis, *phit, *phiu, *phiv;      // derivatives of solution basis wrt (s,t,u)
  const Real *phiss, *phist, *phisu, *phisv,
                     *phitt, *phitu, *phitv,
                             *phiuu, *phiuv,
                                     *phivv;   // second derivatives of solution basis wrt (s,t,u,v)

  evalReferenceCoordinateGradient( sRef, sgrad, tgrad, ugrad, vgrad );
  evalReferenceCoordinateHessian( sRef, shess, thess, uhess, vhess );
  fldElem.evalBasisDerivative( sRef, &phis, &phit, &phiu, &phiv );
  fldElem.evalBasisHessianDerivative( sRef, &phiss, &phist, &phitt, &phisu, &phitu, &phiuu , &phisv, &phitv, &phiuv, &phivv );

  Matrix invJ; // Inverse Jacobian
  TensorSymX refHess;
  invJ(0,0) = sgrad[0]; invJ(0,1) = sgrad[1]; invJ(0,2) = sgrad[2]; invJ(0,3) = sgrad[3];
  invJ(1,0) = tgrad[0]; invJ(1,1) = tgrad[1]; invJ(1,2) = tgrad[2]; invJ(1,3) = tgrad[3];
  invJ(2,0) = ugrad[0]; invJ(2,1) = ugrad[1]; invJ(2,2) = ugrad[2]; invJ(2,3) = ugrad[3];
  invJ(3,0) = vgrad[0]; invJ(3,1) = vgrad[1]; invJ(3,2) = vgrad[2]; invJ(3,3) = vgrad[3];

  // TODO philip
  for (int n = 0; n < nphi; n++)
  {
    refHess(0,0) = phiss[n];
    refHess(1,0) = phist[n]; refHess(1,1) = phitt[n];
    refHess(2,0) = phisu[n]; refHess(2,1) = phitu[n]; refHess(2,2) = phiuu[n];
    refHess(3,0) = phisv[n]; refHess(3,1) = phitv[n]; refHess(3,2) = phiuv[n]; refHess(3,3) = phivv[n];

    hessphi[n]  = Transpose(invJ)*refHess*invJ;
    hessphi[n] += phis[n]*shess + phit[n]*thess + phiu[n]*uhess + phiv[n]*vhess;
  }
}

template <class PhysDim, class Topology>
void
ElementXField<PhysDim, TopoD4, Topology>::dumpTecplot( std::ostream& out ) const
{
  out << "i don't think tecplot will understand..." << std::endl;
}

}
