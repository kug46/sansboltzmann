// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ELEMENTPROJECTION_LINEARSCALARLG_L2_H
#define ELEMENTPROJECTION_LINEARSCALARLG_L2_H

// Element solution projection: L2

#include <ostream>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "tools/SANSTraitsScalar.h" // For extracting the scalar type

#include "Quadrature/Quadrature.h"

#include "Field/Element/Element.h"

#include "LinearAlgebra/DenseLinAlg/InverseLU.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "BasisFunction/BasisFunction_RefElement_Split.h"


namespace SANS
{

//----------------------------------------------------------------------------//
// Element solution projection: L2 of the Diffusive Flux for Linear Scalar
//
//  min  integral[ (qexact(x,y) - q(x,y))^2 ],  q(x,y) = sum q_k phi_k(x,y)

template <class PhysDim, class TopoDim, class Topology, class ArrayQ, class PDE, class SolutionFunctor>
void
ElementProjectionDiffusiveFlux_L2(
    const ElementXField<PhysDim, TopoDim, Topology>& xfldElem,
          Element<SANS::DLA::VectorS<PhysDim::D,ArrayQ >, TopoDim, Topology>& qfldElem,
    const PDE& pde,
    const SolutionFunctor& solnExact )
{
  typedef typename Scalar<ArrayQ>::type T;

  typedef typename PDE::template MatrixQ<T> MatrixQ;    // solution/residual jacobians

  typedef typename ElementXField<PhysDim, TopoDim, Topology>::VectorX VectorX;
  typedef typename Element<ArrayQ, TopoDim, Topology>::RefCoordType RefCoordType;

  const int nDOF = qfldElem.nDOF();   // total solution DOFs in element
  SANS_ASSERT( nDOF > 0 );            // Suppress clang analyzer warning

  Real dJ;                    // incremental Jacobian determinant
  Real weight;                // quadrature weight
  RefCoordType Ref;           // reference-element coordinates
  VectorX X;                   // physical coordinates

  DLA::VectorD<Real> phi(nDOF);                         // basis
  ArrayQ qexact;
  SANS::DLA::VectorS<PhysDim::D,ArrayQ> gradqexact;     // exact solution

  SANS::DLA::MatrixS<PhysDim::D, PhysDim::D, MatrixQ > K;
  SANS::DLA::VectorS<PhysDim::D, ArrayQ> diffFlux; // diffusive flux

  DLA::MatrixD<Real> mtx( nDOF, nDOF );
  DLA::VectorD<ArrayQ> rhs( nDOF );
  DLA::VectorD<ArrayQ> sln( nDOF );

  // use max quadrature rule
  Quadrature<TopoDim, Topology> quadrature( -2 );

  const int nquad = quadrature.nQuadrature();
  // loop over dimensions
  for ( int k = 0; k < PhysDim::D ; k++ )
  {
    mtx = 0;
    rhs = 0;
    // loop over quadrature points
    for (int iquad = 0; iquad < nquad; iquad++)
    {
      quadrature.weight( iquad, weight );
      quadrature.coordinates( iquad, Ref );

      dJ = weight * xfldElem.jacobianDeterminant( Ref );

      // physical coordinates
      xfldElem.coordinates( Ref, X );

      // basis value, gradient
      qfldElem.evalBasis( Ref, phi.data(), phi.size() );

      qexact = 0;
      gradqexact = 0;
      solnExact.gradient( X, qexact, gradqexact );

      K=0;
      pde.diffusionViscous( X, qexact, gradqexact, K );
      diffFlux = -1*K*gradqexact;

      for (int i = 0; i < nDOF; i++)
      {
        for (int j = 0; j < nDOF; j++)
          mtx(i,j) += dJ*phi[i]*phi[j];

        rhs[i] += dJ*phi[i]*diffFlux[k];
      }
    }

#if 0
  std::cout << "mtx = ";  dump( mtx, 2 );
  std::cout << "rhs = ";  dump( rhs, 2 );
#endif

    sln = SANS::DLA::InverseLU::Solve( mtx, rhs);
    for (int i = 0; i < nDOF; i++)
      qfldElem.DOF(i)[k] = sln[i];
  }
}



//----------------------------------------------------------------------------//
// boundary trace-element solution projection: L2 fit for Linear Scalar Lagrange Multipliers
//
//  min  integral[ (qexact(x,y) - q(x,y))^2 ],  q(x,y) = sum q_k phi_k(x,y)
//

template <class PhysDim, class TopoDim, class Topology, class ArrayQ, class PDE, class BC, class SolutionFunctor>
void
ElementProjection_LinearScalarLG_L2(
    const ElementXField<PhysDim, TopoDim, Topology>& xfldElem,
          Element<ArrayQ, TopoDim, Topology>& qfldElem,
    const PDE& pde,
    const BC& bc,
    const SolutionFunctor& solnExact )
{
  typedef typename Scalar<ArrayQ>::type T;

  typedef typename PDE::template MatrixQ<T> MatrixQ;    // solution/residual jacobians

  typedef typename ElementXField<PhysDim, TopoDim, Topology>::VectorX VectorX;
  typedef typename Element<ArrayQ, TopoDim, Topology>::RefCoordType RefCoordType;

  const int nDOF = qfldElem.nDOF();                     // total solution DOFs in element
  SANS_ASSERT(nDOF == qfldElem.nDOF());
  SANS_ASSERT(nDOF > 0);      // Suppress clang analyzer warning

  Real dJ;                    // incremental Jacobian determinant
  Real weight;                // quadrature weight
  RefCoordType Ref;           // reference-line coordinate (s)
  VectorX X;                   // physical coordinates
  VectorX N;                   // unit normal (points out of domain)
  MatrixQ Amtx, Bmtx;
  Real A,B,an;
  DLA::VectorS< PhysDim::D, ArrayQ > KgradQExact;
  DLA::MatrixS< PhysDim::D, PhysDim::D, MatrixQ > K = 0;
  DLA::VectorS< PhysDim::D, MatrixQ > a = 0;

  DLA::VectorD<Real> phi( nDOF );               // basis

  ArrayQ qExact = 0;
  ArrayQ muExact = 0;
  DLA::VectorS<PhysDim::D, ArrayQ> gradQExact;  // exact solution gradient

  DLA::MatrixD<Real> mtx( nDOF, nDOF );
  DLA::VectorD<ArrayQ> rhs( nDOF );
  DLA::VectorD<ArrayQ> sln( nDOF );

  mtx = 0;
  rhs = 0;

  // use max quadrature rule
  Quadrature<TopoDim, Topology> quadrature( -2 );

  // loop over quadrature points
  const int nquad = quadrature.nQuadrature();
  for (int iquad = 0; iquad < nquad; iquad++)
  {
    quadrature.weight( iquad, weight );
    quadrature.coordinates( iquad, Ref );

    dJ = weight * xfldElem.jacobianDeterminant( Ref );

    // physical coordinates
    xfldElem.coordinates( Ref, X );

    // physical coordinates
    xfldElem.unitNormal( Ref, N );

    // bc data
    Amtx =0; Bmtx=0;
    bc.coefficients( X, N, Amtx, Bmtx );
    A = Amtx; B = Bmtx; // forces conversion to scalars, thus ensuring Linear Scalar

    // basis
    qfldElem.evalBasis( Ref, phi.data(), phi.size() );

    solnExact.gradient( X, qExact, gradQExact );

    a = 0; K = 0;
    pde.jacobianFluxAdvective( X, qExact, a );
    pde.diffusionViscous( X, qExact, gradQExact, K );

    an = dot(N,a);

    KgradQExact = K*gradQExact; // = Kn*dq/dn + Ks*dq/ds

    muExact = ((B - A*an)*qExact - A*dot(KgradQExact,N))/(A*A + B*B);

    for (int i = 0; i < nDOF; i++)
    {
      for (int j = 0; j < nDOF; j++)
        mtx(i,j) += dJ*phi[i]*phi[j];

       rhs[i] += dJ*phi[i]*muExact;
    }
  }

  sln = SANS::DLA::InverseLU::Solve( mtx, rhs );

  for (int i = 0; i < nDOF; i++)
    qfldElem.DOF(i) = sln[i];
}


}

#endif  // ELEMENTPROJECTION_LINEARSCALARLG_L2_H
