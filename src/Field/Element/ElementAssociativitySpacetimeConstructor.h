// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ELEMENTASSOCIATIVITYSPACETIMECONSTRUCTOR_H
#define ELEMENTASSOCIATIVITYSPACETIMECONSTRUCTOR_H

// Spacetime associativity (local to global mappings)

#include <ostream>
#include <vector>
#include <array>

#include "tools/SANSException.h"
#include "BasisFunction/BasisFunctionSpacetime.h"
#include "BasisFunction/CanonicalTraceToCell.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// spacetime associativity: global numberings for element nodes/faces/cell
//
// template parameters:
//   Topology                   element topology (triangle/quad)
//                              Note: needed for basis function ctor
//
// member functions:
//   .order                     polynomial order
//   .nNode                     # node DOFs
//   .nEdge                     # edge DOFs
//   .nArea                     # area DOFs
//   .nFace                     # face DOFs
//   .nCell                     # cell interior DOFs
//   .node/face/cellGlobal      global node/face/cell DOF accessors
//   .set/getNodeGlobalMapping  local-to-global node DOF mapping
//   .set/getEdgeGlobalMapping  local-to-global edge DOF mapping
//   .set/getAreaGlobalMapping  local-to-global area DOF mapping
//   .set/getFaceGlobalMapping  local-to-global face DOF mapping
//   .set/getCellGlobalMapping  local-to-global cell DOF mapping
//
//   .dump            debug dump of private data
//----------------------------------------------------------------------------//

template <class TopoDim, class Topology>
class ElementAssociativityConstructor;

template <class Topology>
class ElementAssociativityConstructor<TopoD4, Topology>
{
public:
  typedef TopoD4 TopoDim;
  typedef Topology TopologyType;             // element topology (e.g. Tet, Hex)
  typedef BasisFunctionSpacetimeBase<Topology> BasisType;
  typedef std::array<int,Topology::NFace> IntNFace;

//  explicit ElementAssociativitySpacetime( int order );
  ElementAssociativityConstructor();
  explicit ElementAssociativityConstructor( const BasisType* basis ) : ElementAssociativityConstructor() { resize(basis); }
  ElementAssociativityConstructor( const ElementAssociativityConstructor& a ) : ElementAssociativityConstructor() { operator=(a); }
  ~ElementAssociativityConstructor();

  ElementAssociativityConstructor& operator=( const ElementAssociativityConstructor& );

  void resize( const BasisType* basis );

  int rank() const  { SANS_ASSERT(isSetRank_); return rank_; }
  int order() const { return order_; }
  int nNode() const { return nNode_; }
  int nEdge() const { return nEdge_; }
  int nArea() const { return nArea_; }
  int nFace() const { return nFace_; }
  int nCell() const { return nCell_; }
  int nDOF() const  { return nNode_ + nEdge_ + +nArea_ + nCell_; }

  void setRank(const int rank) { isSetRank_ = true; rank_ = rank; }

  // node, face, cell maps
  int nodeGlobal( int n ) const { SANS_ASSERT_MSG(isSetNodeGlobalMapping_[n], "n = %d", n); return (nodeList_[n]); }
  int edgeGlobal( int n ) const { SANS_ASSERT_MSG(isSetEdgeGlobalMapping_[n], "n = %d", n); return (edgeList_[n]); }
  int areaGlobal( int n ) const { SANS_ASSERT_MSG(isSetAreaGlobalMapping_[n], "n = %d", n); return (areaList_[n]); }
  int faceGlobal( int n ) const { SANS_ASSERT_MSG(isSetFaceGlobalMapping_[n], "n = %d", n); return (faceList_[n]); }
  int cellGlobal( int n ) const { SANS_ASSERT_MSG(isSetCellGlobalMapping_[n], "n = %d", n); return (cellList_[n]); }

  void setNodeGlobalMapping( const int nodeMap[], int nnode );
  void getNodeGlobalMapping(       int nodeMap[], int nnode ) const;

  void setNodeGlobalMapping( const std::vector<int>& node );
  void getNodeGlobalMapping( std::vector<int>& nodeMap ) const { getNodeGlobalMapping( nodeMap.data(), nodeMap.size());}

  void setEdgeGlobalMapping( const int edgeMap[], int nedge );
  void getEdgeGlobalMapping(       int edgeMap[], int nedge ) const;

  void setEdgeGlobalMapping( const std::vector<int>& edgeMap );
  void setEdgeGlobalMapping( const std::vector<int>& edgeMap, const CanonicalTraceToCell& canonicalface,
                             const int edge);

  void setAreaGlobalMapping( const int areaMap[], int narea );
  void getAreaGlobalMapping(       int areaMap[], int narea ) const;

  void setAreaGlobalMapping( const std::vector<int>& areaMap );
  void setAreaGlobalMapping( const std::vector<int>& areaMap, const CanonicalTraceToCell& canonicalface,
                             const int area);

  void setFaceGlobalMapping( const int faceMap[], int nface );
  void getFaceGlobalMapping(       int faceMap[], int nface ) const;
  void setFaceGlobalMapping( const int faceMap[], int nsize, const CanonicalTraceToCell& canonicalface );

  void setFaceGlobalMapping( const std::vector<int>& faceMap );


  void setCellGlobalMapping( const int cellMap[], int ncell );
  void getCellGlobalMapping(       int cellMap[], int ncell ) const;

  void setCellGlobalMapping( const std::vector<int>& cellMap );


  void setGlobalMapping( const int map[], int ndof );
  void getGlobalMapping(       int map[], int ndof ) const;

  void setGlobalMapping( const std::vector<int>& map );

  // face sign accessors
        IntNFace& faceSign()       { return faceSign_; }
  const IntNFace& faceSign() const { return faceSign_; }
        IntNFace& traceOrientation()       { return faceSign_; }
  const IntNFace& traceOrientation() const { return faceSign_; }

  void setFaceSign( int sgn, const int canonicalFace )
  {
    SANS_ASSERT( canonicalFace >= 0 && canonicalFace < Topology::NFace );
    faceSign_[canonicalFace] = sgn;
  }

  void setOrientation( int sgn, int canonicalFace ) { setFaceSign(sgn, canonicalFace); }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  int rank_;                      // the processor rank that possesses this element
  int order_;                     // polynomial order for node/face DOFs
  int nNode_;                     // # node DOFs
  int nEdge_;                     // # edge DOFs
  int nArea_ ;                    // # area DOFs
  int nFace_;                     // # face DOFs
  int nCell_;                     // # cell DOFs
  int* nodeList_;                 // global ordering of node DOFs
  int* edgeList_;                 // global ordering of edge DOFs
  int* areaList_;                 // global ordering of area DOFs
  int* faceList_;                 // global ordering of face DOFs
  int* cellList_;                 // global ordering of cell DOFs

  bool isSetRank_;                 // flag: has the processor rank been set
  bool* isSetNodeGlobalMapping_;   // flag: is nodeList_ set yet?
  bool* isSetEdgeGlobalMapping_;   // flag: is edgeList_ set yet?
  bool* isSetAreaGlobalMapping_;   // flag: is areaList_ set yet?
  bool* isSetFaceGlobalMapping_;   // flag: is faceList_ set yet?
  bool* isSetCellGlobalMapping_;   // flag: is cellList_ set yet?

  IntNFace faceSign_;              // +/- sign for face orientations (i.e. left/right element)
};

}

#endif  // ELEMENTASSOCIATIVITYSPACETIMECONSTRUCTOR_H
