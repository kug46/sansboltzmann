// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "ElementAssociativityNodeConstructor.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// node associativity: global numberings for element nodes

// NOTE: default ctor needed for 'new []'
ElementAssociativityConstructor<TopoD0, Node>::ElementAssociativityConstructor()
{
  rank_ = -1;
  nodeList_ = -1;
  normalSignL_ = 0;
  normalSignR_ = 0;

  isSetRank_ = false;
  isSetNodeGlobalMapping_ = false;
}


ElementAssociativityConstructor<TopoD0, Node>::ElementAssociativityConstructor( int order )
{
  SANS_ASSERT( order == 0);
  rank_ = -1;
  nodeList_ = -1;
  normalSignL_ = 0;
  normalSignR_ = 0;

  isSetRank_ = false;
  isSetNodeGlobalMapping_ = false;
}


void
ElementAssociativityConstructor<TopoD0, Node>::resize( const BasisType* basis )
{
  rank_ = -1;
  nodeList_ = -1;
  normalSignL_ = 0;
  normalSignR_ = 0;

  isSetRank_ = false;
  isSetNodeGlobalMapping_ = false;
}


ElementAssociativityConstructor<TopoD0, Node>&
ElementAssociativityConstructor<TopoD0, Node>::operator=( const ElementAssociativityConstructor& a )
{
  if (this != &a)
  {
    rank_ = a.rank_;
    nodeList_ = a.nodeList_;
    normalSignL_ = a.normalSignL_;
    normalSignR_ = a.normalSignR_;

    isSetRank_ = a.isSetRank_;
    isSetNodeGlobalMapping_ = a.isSetNodeGlobalMapping_;
  }

  return *this;
}

ElementAssociativityConstructor<TopoD0, Node>::~ElementAssociativityConstructor()
{
}


void
ElementAssociativityConstructor<TopoD0, Node>::setNodeGlobalMapping( const int node[], int nnode )
{
  SANS_ASSERT( nnode == 1 );

  nodeList_ = node[0];
  isSetNodeGlobalMapping_ = true;
}

void
ElementAssociativityConstructor<TopoD0, Node>::setNodeGlobalMapping( const std::vector<int>& node )
{
  SANS_ASSERT( (int)node.size() == 1 );

  nodeList_ = node[0];
  isSetNodeGlobalMapping_ = true;
}

void
ElementAssociativityConstructor<TopoD0, Node>::getNodeGlobalMapping( int node[], int nnode ) const
{
  SANS_ASSERT( nnode == 1 );

  SANS_ASSERT( isSetNodeGlobalMapping_ );
  node[0] = nodeList_;
}

void
ElementAssociativityConstructor<TopoD0, Node>::setGlobalMapping( const int map[], int ndof )
{
  SANS_ASSERT( ndof == 1 );

  nodeList_ = map[0];
  isSetNodeGlobalMapping_ = true;
}


void
ElementAssociativityConstructor<TopoD0, Node>::setGlobalMapping( const std::vector<int>& map )
{
  setGlobalMapping(&map[0], static_cast<int>(map.size()));
}


void
ElementAssociativityConstructor<TopoD0, Node>::getGlobalMapping( int map[], int ndof ) const
{
  SANS_ASSERT( ndof == 1);
  SANS_ASSERT( isSetNodeGlobalMapping_ );
  map[0] = nodeList_;
}


void
ElementAssociativityConstructor<TopoD0, Node>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "ElementAssociativityConstructor<TopoD0, Node>: order = " << 0
      << "  nNode = " << 1 << std::endl;

  out << indent << indent << "rank = " << rank_ << std::endl;
  out << indent << indent << "nodeList = ";
  out << nodeList_ << " ";
  out << std::endl;

  out << indent << indent << "normalSign = "; //TODO: to clean up
  out << normalSignL_ << " ";
  out << std::endl;

  out << indent << indent << "isSetNodeGlobalMapping = ";
  out << isSetNodeGlobalMapping_ << " ";
  out << std::endl;
}

}
