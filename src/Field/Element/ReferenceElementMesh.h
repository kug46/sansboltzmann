// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef REFERENCE_ELEMENT_MESH_H
#define REFERENCE_ELEMENT_MESH_H

#include <array>
#include <vector>

#include "Topology/ElementTopology.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

namespace SANS
{

template<class Topology>
struct ReferenceElementMesh;

//---------------------------------------------------------------------------//
template<>
struct ReferenceElementMesh<Line>
{
  typedef DLA::VectorS<1,Real> RefCoordType;
  typedef std::array<int,Line::NNode> LinearElementNodesType;

  ReferenceElementMesh(const int nRefine);

  std::vector<RefCoordType> nodes;
  std::vector<LinearElementNodesType> elems;
};

//---------------------------------------------------------------------------//
template<>
struct ReferenceElementMesh<Triangle>
{
  typedef DLA::VectorS<2,Real> RefCoordType;
  typedef std::array<int,Triangle::NNode> LinearElementNodesType;

  ReferenceElementMesh(const int nRefine);

  std::vector<RefCoordType> nodes;
  std::vector<LinearElementNodesType> elems;
};

//---------------------------------------------------------------------------//
template<>
struct ReferenceElementMesh<Quad>
{
  typedef DLA::VectorS<2,Real> RefCoordType;
  typedef std::array<int,Quad::NNode> LinearElementNodesType;

  ReferenceElementMesh(const int nRefine);

  std::vector<RefCoordType> nodes;
  std::vector<LinearElementNodesType> elems;
};

//---------------------------------------------------------------------------//
template<>
struct ReferenceElementMesh<Tet>
{
  typedef DLA::VectorS<3,Real> RefCoordType;
  typedef std::array<int,Tet::NNode> LinearElementNodesType;

  ReferenceElementMesh(const int nRefine);

  std::vector<RefCoordType> nodes;
  std::vector<LinearElementNodesType> elems;
};


//---------------------------------------------------------------------------//
template<>
struct ReferenceElementMesh<Hex>
{
  typedef DLA::VectorS<3,Real> RefCoordType;
  typedef std::array<int,Hex::NNode> LinearElementNodesType;

  ReferenceElementMesh(const int nRefine);

  std::vector<RefCoordType> nodes;
  std::vector<LinearElementNodesType> elems;
};

//---------------------------------------------------------------------------//
template<>
struct ReferenceElementMesh<Pentatope>
{
  typedef DLA::VectorS<4,Real> RefCoordType;
  typedef std::array<int,Pentatope::NNode> LinearElementNodesType;

  ReferenceElementMesh(const int nRefine);

  std::vector<RefCoordType> nodes;
  std::vector<LinearElementNodesType> elems;
};

}

#endif // REFERENCE_ELEMENT_MESH_H
