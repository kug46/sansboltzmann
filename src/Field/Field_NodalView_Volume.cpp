// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BasisFunction/ElementEdges.h"
#include "Field_NodalView_impl.h"
#include "XFieldVolume.h"
#include "FieldVolume_CG_Cell.h"

#include <algorithm> //std::sort

namespace SANS
{

//=============================================================================
template <class FieldTraits>
void
Field_NodalView::init(const FieldBase<FieldTraits>& fld, const std::vector<int>& cellgroup_list)
{
  std::size_t nCellGroups = cellgroup_list.size();

  std::map<int, std::set<GroupElemIndex>> NodeDOF2CellMap;
  std::map<int, std::set<std::pair<int, int>>> NodeDOF2EdgeMap;

  //Create connectivity structure
  for (std::size_t group = 0; group < nCellGroups; group++ )
  {
    const int cellGroupGlobal = cellgroup_list[group];

    int nElem = fld.getCellGroupBase(cellGroupGlobal).nElem();

    if (fld.getCellGroupBase(cellGroupGlobal).topoTypeID() == typeid(Tet))
    {
      typedef typename FieldBase<FieldTraits>:: template FieldCellGroupType<Tet> FieldCellGroupType;
      const FieldCellGroupType& xfld_cellgrp = fld.template getCellGroup<Tet>(cellGroupGlobal);

      int nodeDOFMap[Tet::NNode];
      const int (*EdgeNodes)[ Line::NNode ] = ElementEdges<Tet>::EdgeNodes;

      for (int elem = 0; elem < nElem; elem++)
      {
        xfld_cellgrp.associativity(elem).getNodeGlobalMapping(nodeDOFMap, Tet::NNode);

        this->insertCellNodes(nodeDOFMap, Tet::NNode, cellGroupGlobal, elem, NodeDOF2CellMap);
        this->insertEdge(nodeDOFMap[EdgeNodes[0][0]], nodeDOFMap[EdgeNodes[0][1]], NodeDOF2EdgeMap); //Canonical edge 0
        this->insertEdge(nodeDOFMap[EdgeNodes[1][0]], nodeDOFMap[EdgeNodes[1][1]], NodeDOF2EdgeMap); //Canonical edge 1
        this->insertEdge(nodeDOFMap[EdgeNodes[2][0]], nodeDOFMap[EdgeNodes[2][1]], NodeDOF2EdgeMap); //Canonical edge 2
        this->insertEdge(nodeDOFMap[EdgeNodes[3][0]], nodeDOFMap[EdgeNodes[3][1]], NodeDOF2EdgeMap); //Canonical edge 3
        this->insertEdge(nodeDOFMap[EdgeNodes[4][0]], nodeDOFMap[EdgeNodes[4][1]], NodeDOF2EdgeMap); //Canonical edge 4
        this->insertEdge(nodeDOFMap[EdgeNodes[5][0]], nodeDOFMap[EdgeNodes[5][1]], NodeDOF2EdgeMap); //Canonical edge 5
      }
    }
    else if (fld.getCellGroupBase(cellGroupGlobal).topoTypeID() == typeid(Hex))
    {
      typedef typename FieldBase<FieldTraits>:: template FieldCellGroupType<Hex> FieldCellGroupType;
      const FieldCellGroupType& xfld_cellgrp = fld.template getCellGroup<Hex>(cellGroupGlobal);

      int nodeDOFMap[Hex::NNode];

      for (int elem = 0; elem < nElem; elem++)
      {
        xfld_cellgrp.associativity(elem).getNodeGlobalMapping(nodeDOFMap, Hex::NNode);

        this->insertCellNodes(nodeDOFMap, Hex::NNode, cellGroupGlobal, elem, NodeDOF2CellMap);
        //TODO: Need to insert edges
        SANS_DEVELOPER_EXCEPTION("Edge map not implemented for Hex...");
      }
    }
    else
      SANS_DEVELOPER_EXCEPTION("Field_NodalView<PhysDim, TopoD3> - Unknown Topology type for CellGroup.");
  }

  std::sort(NodeDOFList_.begin(), NodeDOFList_.end());

  this->setMaps( NodeDOF2CellMap, NodeDOF2EdgeMap );
  this->updateInverseNodeMap(fld.nDOF());
}

//=============================================================================
//Explicitly instantiate
template Field_NodalView::Field_NodalView(const XField<PhysD3, TopoD3>& xfld, const std::vector<int>& cellgroup_list);

template Field_NodalView::Field_NodalView(const Field_CG_Cell<PhysD3, TopoD3, Real>&);
template Field_NodalView::Field_NodalView(const Field_CG_Cell<PhysD3, TopoD3, DLA::MatrixSymS<3,Real>>&);
template Field_NodalView::Field_NodalView(const Field_CG_Cell<PhysD3, TopoD3, DLA::MatrixSymS<3,Real>>&, const std::vector<int>&);

template Field_NodalView::Field_NodalView(const Field_CG_Cell<PhysD3, TopoD3, DLA::VectorS<5,Real>>&);
template Field_NodalView::Field_NodalView(const Field_CG_Cell<PhysD3, TopoD3, DLA::VectorS<5,Real>>&, const std::vector<int>&);

}
