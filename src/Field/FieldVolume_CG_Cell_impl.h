// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(FIELDVOLUME_CG_CELL_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include <set>
#include <algorithm> // std::find
#include <numeric> // std::accumulate

#include "FieldVolume_CG_Cell.h"

#include "BasisFunction/BasisFunctionCategory.h"

#include "Field_CG/Field_CG_Topology.h"
#include "Field_CG/Field_CG_CellConstructor.h"

#include "tools/split_cat_std_vector.h"

namespace SANS
{

template <class PhysDim, class T>
Field_CG_Cell<PhysDim, TopoD3, T>::Field_CG_Cell( const XField<PhysDim, TopoD3>& xfld,
                                                  const int order, const BasisFunctionCategory& category,
                                                  const embeddedType eType )
                                                  : BaseType( xfld ), needsEmbeddedGhosts_(eType==EmbeddedCGField)
{
  // By default all boundary groups are used to construct lagrange multipliers
  init(order, category, {BaseType::createCellGroupIndex()} );
}

template <class PhysDim, class T>
Field_CG_Cell<PhysDim, TopoD3, T>::Field_CG_Cell( const XField<PhysDim, TopoD3>& xfld,
                                                  const int order, const BasisFunctionCategory& category,
                                                  const std::vector<int>& CellGroups,
                                                  const embeddedType eType )
                                                  : BaseType( xfld ), needsEmbeddedGhosts_(eType==EmbeddedCGField)
{
  // Check that the groups asked for are within the range of available groups
  BaseType::checkCellGroupIndex( CellGroups );
  init( order, category, {CellGroups});
}


template <class PhysDim, class T>
Field_CG_Cell<PhysDim, TopoD3, T>::Field_CG_Cell( const std::vector<std::vector<int>>& CellGroupSets,
                                                  const XField<PhysDim, TopoD3>& xfld,
                                                  const int order, const BasisFunctionCategory& category,
                                                  const embeddedType eType )
                                                  : BaseType( xfld ), needsEmbeddedGhosts_(eType==EmbeddedCGField)
{
//  SANS_DEVELOPER_EXCEPTION("Field_CG_Cell<PhysDim, TopoD3, T>::Field_CG_Cell -- Broken CG Fields not implemented for TopoD3");
  // Check that the groups asked for are within the range of available groups
  for (auto it = CellGroupSets.begin(); it != CellGroupSets.end(); ++it)
  {
    BaseType::checkCellGroupIndex( *it );
    // Check that no cell group occurs twice
    for (auto it2 = std::next(it,1); it2 != CellGroupSets.end(); ++it2) // search the upper triangle of macro set
      SANS_ASSERT_MSG( std::find_first_of(it->begin(),it->end(),it2->begin(),it2->end())== it->end() , "Cell groups can only be in one macro group");
  }
  init(order, category, CellGroupSets);
}

template <class PhysDim, class T>
Field_CG_Cell<PhysDim, TopoD3, T>::Field_CG_Cell( const Field_CG_Cell& fld, const FieldCopy&tag )
: BaseType(fld, tag), needsEmbeddedGhosts_(fld.needsEmbeddedGhosts()) {}

template <class PhysDim, class T>
Field_CG_Cell<PhysDim, TopoD3, T>&
Field_CG_Cell<PhysDim, TopoD3, T>::operator=( const ArrayQ& q ) { BaseType::operator=(q); return *this; }

//---------------------------------------------------------------------------//
template <class PhysDim, class T>
void
Field_CG_Cell<PhysDim, TopoD3, T>::init( const int order, const BasisFunctionCategory& category,
                                         const std::vector<std::vector<int>>& CellGroupSets )
{
  SANS_ASSERT_MSG( order >= 1, "CG volume requires order >= 1" );
  SANS_ASSERT_MSG( (category == BasisFunctionCategory_Hierarchical && order <= 2) || // Logic needed for higher order
                   category == BasisFunctionCategory_Lagrange,
                   "CG volume must use Hierarchical or Lagrange Basis" );

  Field_CG_CellConstructor<PhysDim, TopoD3> fldConstructor(xfld_, order, category, CellGroupSets, needsEmbeddedGhosts_);

  nElem_ = fldConstructor.nElem();

  // allocate the solution DOF array
  this->resizeDOF(fldConstructor.nDOF());
  for (int n = 0; n < nDOF_; n++)
    DOF_[n] = 0;

  // flatten the cell groups
  std::vector<int> ConcatCellGroups = cat(CellGroupSets);

  // allocate the volume cell groups; set node & cell DOF associativity; set edge signs from grid
  this->resizeCellGroups( ConcatCellGroups );

  for (std::size_t i = 0; i < CellGroupSets.size(); i++)
  {
    const std::vector<int>& CellGroups = CellGroupSets[i];
    for (std::size_t igroup = 0; igroup < CellGroups.size(); igroup++)
    {
      const int group = CellGroups[igroup];
      int localGroup = localCellGroups_.at(group);

      if ( xfld_.getCellGroupBase(group).topoTypeID() == typeid(Tet) )
      {
        typedef typename Field<PhysDim, TopoD3, T>::template FieldCellGroupType<Tet> FieldCellGroupClass;

        cellGroups_[localGroup] = fldConstructor.template createCellGroup<FieldCellGroupClass>( group, this->local2nativeDOFmap_);
        cellGroups_[localGroup]->setDOF( DOF_, nDOF_ );
      }
      else if ( xfld_.getCellGroupBase(group).topoTypeID() == typeid(Hex) )
      {
        typedef typename Field<PhysDim, TopoD3, T>::template FieldCellGroupType<Hex> FieldCellGroupClass;

        cellGroups_[localGroup] = fldConstructor.template createCellGroup<FieldCellGroupClass>( group, this->local2nativeDOFmap_);
        cellGroups_[localGroup]->setDOF( DOF_, nDOF_ );
      }
      else
      {
        SANS_DEVELOPER_EXCEPTION( "Unknown cell topology." );
      }
    }
  }

  // TODO: Decide if we really need trace groups in a Cell field. I don't think so...
  // Interior Traces are complicated by the internal boundaries introduced by CellGroupSets (some traces have multiple dofs).
  // As such, only adding in boundary trace information for the time being.
  // This is what will be necessary for setting essential bcs -- Hugh


  // figure out the number of boundary trace groups necessary
  std::vector<int> boundaryTraceGroups = {};
  for (int group = 0; group < xfld_.nBoundaryTraceGroups(); group++)
  {
    int cellGroupL = xfld_.getBoundaryTraceGroupBase(group).getGroupLeft();
    const bool internal_L ( std::find(ConcatCellGroups.begin(), ConcatCellGroups.end(), cellGroupL)
                            != ConcatCellGroups.end() );
    if (internal_L)
      boundaryTraceGroups.push_back(group);
  }

  this->resizeBoundaryTraceGroups( boundaryTraceGroups );

  // Need to loop macros separately, as all of fldAssocAreas must be built first
  for (std::size_t i = 0; i < CellGroupSets.size(); i++)
  {
    const std::vector<int>& CellGroups = CellGroupSets[i];

    for (int group = 0; group < this->nBoundaryTraceGroups(); group++)
    {
      int cellGroupL = xfld_.getBoundaryTraceGroupBase(group).getGroupLeft();

      // left Cell group is in current CellGroups
      const bool internal_L ( std::find(CellGroups.begin(), CellGroups.end(), cellGroupL) != CellGroups.end() );

      // skip any boundary trace group that is not connected to the requested cell groups
      if (internal_L)
      {
        int localGroup = localBoundaryTraceGroups_.at(group);
        if ( xfld_.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Triangle) )
        {
          typedef typename BaseType::template FieldTraceGroupType<Triangle> FieldTraceGroupClass;

          // allocate group
          boundaryTraceGroups_[localGroup] = fldConstructor.template createBoundaryTraceGroup<FieldTraceGroupClass>( group );
          boundaryTraceGroups_[localGroup]->setDOF( DOF_, nDOF_ );
        }
        else if ( xfld_.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Quad) )
        {
          typedef typename BaseType::template FieldTraceGroupType<Quad> FieldTraceGroupClass;

          // allocate group
          boundaryTraceGroups_[localGroup] = fldConstructor.template createBoundaryTraceGroup<FieldTraceGroupClass>( group );
          boundaryTraceGroups_[localGroup]->setDOF( DOF_, nDOF_ );
        }
        else
          SANS_DEVELOPER_EXCEPTION( "Unknown boundary-trace topology." );
      }
    }
  }


  // get the ghost/zombie DOF ranks
  // this must be last as it uses this->local2nativeDOFmap_
  this->nDOFpossessed_ = fldConstructor.nDOFpossessed();
  this->nDOFghost_     = fldConstructor.nDOFghost();

  this->resizeDOF_rank(fldConstructor.nDOF(), fldConstructor.nDOFpossessed());

  fldConstructor.getDOF_rank(this->DOFghost_rank_);
}

}
