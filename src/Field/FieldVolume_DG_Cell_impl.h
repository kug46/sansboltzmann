// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(FIELDVOLUME_DG_CELL_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "FieldVolume_DG_Cell.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// 3-D solution field: DG cell-field
//----------------------------------------------------------------------------//

template <class PhysDim, class T>
Field_DG_Cell<PhysDim, TopoD3, T>::Field_DG_Cell( const XField<PhysDim, TopoD3>& xfld,
                                                  const int order, const BasisFunctionCategory& category ) : BaseType(xfld)
{
  // By use default all cell groups
  init(order, category, BaseType::createCellGroupIndex() );
}

template <class PhysDim, class T>
Field_DG_Cell<PhysDim, TopoD3, T>::Field_DG_Cell( const XField<PhysDim, TopoD3>& xfld,
                                                  const int order, const BasisFunctionCategory& category, const std::vector<int>& CellGroups )
  : BaseType(xfld)
{
  // Check that the groups asked for are within the range of available groups
  BaseType::checkCellGroupIndex( CellGroups );
  init(order, category, CellGroups);
}

template <class PhysDim, class T>
Field_DG_Cell<PhysDim, TopoD3, T>::Field_DG_Cell( const Field_DG_Cell& fld, const FieldCopy& tag ) : BaseType(fld, tag) {}

template <class PhysDim, class T>
Field_DG_Cell<PhysDim, TopoD3, T>&
Field_DG_Cell<PhysDim, TopoD3, T>::operator=( const ArrayQ& q )
{
  Field< PhysDim, TopoD3, T >::operator=(q);
  return *this;
}

template <class PhysDim, class T>
void
Field_DG_Cell<PhysDim, TopoD3, T>::init( const int order, const BasisFunctionCategory& category, const std::vector<int>& CellGroups )
{
  std::vector<std::unique_ptr<FieldAssociativityConstructorBase>> fldAssocCells( CellGroups.size() );
  std::vector<int> nBasisGroups( CellGroups.size() );

  //allocate the cell groups
  this->resizeCellGroups( CellGroups );

  // Sort out which cells are zombie cells (all neighbors are ghosts)
  std::vector<std::vector<bool>> isZombie;
  {
    std::vector<std::vector<int>> nGhostNeigbor;

    // count the neighboring ghost elements (size Zombies)
    this->countGhostNeighbors(CellGroups, nGhostNeigbor, isZombie);

    // mark zombie cells surrounded by ghosts
    for (std::size_t igroup = 0; igroup < CellGroups.size(); igroup++)
    {
      const int groupGlobal = CellGroups[igroup];

      if (      xfld_.getCellGroupBase(groupGlobal).topoTypeID() == typeid(Tet) )
      {
        for (std::size_t elem = 0; elem < nGhostNeigbor[igroup].size(); elem++)
          isZombie[igroup][elem] = (nGhostNeigbor[igroup][elem] == Tet::NTrace);
      }
      else if ( xfld_.getCellGroupBase(groupGlobal).topoTypeID() == typeid(Hex) )
      {
        for (std::size_t elem = 0; elem < nGhostNeigbor[igroup].size(); elem++)
          isZombie[igroup][elem] = (nGhostNeigbor[igroup][elem] == Hex::NTrace);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Unknown element topology." );
    }
  }

  int nDOFpossessed = 0;

  for (std::size_t igroup = 0; igroup < CellGroups.size(); igroup++)
  {
    const int groupGlobal = CellGroups[igroup];

    if (      xfld_.getCellGroupBase(groupGlobal).topoTypeID() == typeid(Tet) )
    {
      fldAssocCells[igroup] = this->template createCellGroup<Tet>(
                                groupGlobal, order, category, nDOFpossessed, nBasisGroups[igroup]);
    }
    else if ( xfld_.getCellGroupBase(groupGlobal).topoTypeID() == typeid(Hex) )
    {
      fldAssocCells[igroup] = this->template createCellGroup<Hex>(
                                groupGlobal, order, category, nDOFpossessed, nBasisGroups[igroup]);
    }
    else
      SANS_DEVELOPER_EXCEPTION( "Unknown element topology." );
  }

  int nDOF = nDOFpossessed;

  bool setZombie = false;
  for (int z = 0; z < 2; z++)
  {
    for (std::size_t igroup = 0; igroup < CellGroups.size(); igroup++)
    {
      const int groupGlobal = CellGroups[igroup];

      if (      xfld_.getCellGroupBase(groupGlobal).topoTypeID() == typeid(Tet) )
      {
        this->template setGhostCellDOFs<Tet>(
            groupGlobal, isZombie, setZombie, nDOF, fldAssocCells[igroup]);
      }
      else if ( xfld_.getCellGroupBase(groupGlobal).topoTypeID() == typeid(Hex) )
      {
        this->template setGhostCellDOFs<Hex>(
            groupGlobal, isZombie, setZombie, nDOF, fldAssocCells[igroup]);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Unknown element topology." );
    }
    setZombie = true;
  }

  //Allocate the DOFs array
  this->createDOFs(nDOF, nDOFpossessed, isZombie, nBasisGroups);
}

}
