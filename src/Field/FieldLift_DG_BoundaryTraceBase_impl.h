// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(FIELDLIFT_DG_BOUNDARYTRACEBASE_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "FieldLift_DG_BoundaryTraceBase.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
FieldLift_DG_BoundaryTraceBase<PhysDim, TopoDim, T>::FieldLift_DG_BoundaryTraceBase( const XField<PhysDim, TopoDim>& xfld )
  : BaseType(xfld)
{
  //Derived classes call createBoundaryTraceGroup
}

template <class PhysDim, class TopoDim, class T>
FieldLift_DG_BoundaryTraceBase<PhysDim, TopoDim, T>::FieldLift_DG_BoundaryTraceBase(
    const FieldLift_DG_BoundaryTraceBase& fld, const FieldCopy& tag ) : BaseType(fld, tag) {}


//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
template<class TopologyCell, class TopologyTrace>
void
FieldLift_DG_BoundaryTraceBase<PhysDim, TopoDim, T>::
createBoundaryTraceCellGroup(const int bndGroupGlobal, const int order, const BasisFunctionCategory& category)
{
  typedef typename XField<PhysDim, TopoDim>:: template FieldTraceGroupType<TopologyTrace> XFieldTraceClass;
  typedef typename BaseType::template FieldCellGroupType<TopologyCell> FieldCellClass;
  typedef typename FieldCellClass::BasisType BasisType;

  const XFieldTraceClass& xfldGroup = xfld_.template getBoundaryTraceGroup<TopologyTrace>(bndGroupGlobal);

  const BasisType* basis = BasisType::getBasisFunction(order, category);

  // create field associativity constructor
  typename FieldCellClass::FieldAssociativityConstructorType fldAssocCell( basis, xfldGroup.nElem() );

  // DOF ordering: Fortran-array DOF(basis,elem)
  const int nElem = xfldGroup.nElem();
  const int nBasis = basis->nBasis();
  std::vector<int> map( nBasis );
  for (int elem = 0; elem < nElem; elem++)
  {
    for (int i = 0; i < nBasis; i++)
      map[i] = nDOF_ + nBasis*elem + i;

    fldAssocCell.setAssociativity( elem ).setRank( xfldGroup.associativity( elem ).rank() );
    fldAssocCell.setAssociativity( elem ).setGlobalMapping( map );
  }

  // allocate cell group using boundary trace indexing
  cellGroups_[localCellGroups_.at(bndGroupGlobal)] = new FieldCellClass( fldAssocCell );

  // accumulate DOF and Element count
  nDOF_  += nElem*nBasis;
  nElem_ += nElem;
}

//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
void
FieldLift_DG_BoundaryTraceBase<PhysDim, TopoDim, T>::createDOFs()
{
  // allocate the solution DOF array and assign it to groups

  this->resizeDOF(nDOF_);

  for (int group = 0; group < cellGroups_.size(); group++)
    if ( cellGroups_[group] != NULL )
      cellGroups_[group]->setDOF( DOF_, nDOF_ );
}

//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
int
FieldLift_DG_BoundaryTraceBase<PhysDim, TopoDim, T>::nDOFCellGroup(int cellgroup) const
{
  SANS_DEVELOPER_EXCEPTION("FieldLift_DG_BoundaryTraceBase::nDOFCellGroup - No cell groups!");
  return -1;
}

template <class PhysDim, class TopoDim, class T>
int
FieldLift_DG_BoundaryTraceBase<PhysDim, TopoDim, T>::nDOFInteriorTraceGroup(int tracegroup) const
{
  SANS_DEVELOPER_EXCEPTION("FieldLift_DG_BoundaryTraceBase::nDOFInteriorTraceGroup - No interior trace groups!");
  return -1;
}

template <class PhysDim, class TopoDim, class T>
int
FieldLift_DG_BoundaryTraceBase<PhysDim, TopoDim, T>::nDOFBoundaryTraceGroup(int tracegroup) const
{
  const int nElem = this->getBoundaryTraceGroupBase(tracegroup).nElem();
  const int nBasisTrace = this->getCellGroupBase(tracegroup).nBasis();
  return nElem*nBasisTrace;
}

}
