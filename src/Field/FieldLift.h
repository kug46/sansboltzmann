// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDLIFT_H
#define FIELDLIFT_H

#include "FieldSubGroup.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// General lifting operator variable field
//----------------------------------------------------------------------------//

template<class PhysDim, class TopoDim, class T>
class FieldLift : public FieldSubGroup< PhysDim, FieldLiftTraits<TopoDim, T> >, public FieldType< FieldLift<PhysDim,TopoDim,T> >
{
public:
  typedef FieldSubGroup< PhysDim, FieldLiftTraits<TopoDim, T> > BaseType;
  typedef typename BaseType::XFieldType XFieldType;

  static const int D = PhysDim::D;

  // cppcheck-suppress noExplicitConstructor
  FieldLift( const XField<PhysDim, TopoDim>& xfld ) : BaseType(xfld) {}
  ~FieldLift() {}

  FieldLift& operator=( const T& q ) { BaseType::operator=(q); return *this; }
  void cloneFrom( const FieldLift& fld ) { BaseType::cloneFrom(fld); }

  // Can only copy using the Tags
  FieldLift( const FieldLift& fld ) = delete;
  FieldLift& operator=( const FieldLift& fld ) = delete;

  FieldLift( const FieldLift& fld, const FieldCopy& tag ) : BaseType( fld,tag) {}
};

}

#endif //FIELDLIFT_H
