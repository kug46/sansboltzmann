// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define FIELDBASE_INSTANTIATE
#include "FieldBase_impl.h"

#define FIELDSUBGROUP_INSTANTIATE
#include "FieldSubGroup_impl.h"

#define FIELD_INSTANTIATE
#include "Field_impl.h"

#include "XFieldArea.h"
#include "FieldArea.h"

#include "Surreal/SurrealS.h"
#ifdef SANS_MPI
#include "MPI/serialize_SurrealS.h"
#endif

#include "UserVariables/BoltzmannNVar.h"
namespace SANS
{

// FieldSubGroup instantiation
template class FieldSubGroup<PhysD2, FieldTraits<TopoD2, Real> >;
template class FieldSubGroup<PhysD2, FieldTraits<TopoD2, SurrealS<1>> >;

template class FieldSubGroup<PhysD2, FieldTraits<TopoD2, DLA::VectorS<1, Real>> >;
template class FieldSubGroup<PhysD2, FieldTraits<TopoD2, DLA::VectorS<2, Real>> >;
template class FieldSubGroup<PhysD2, FieldTraits<TopoD2, DLA::VectorS<3, Real>> >;
template class FieldSubGroup<PhysD2, FieldTraits<TopoD2, DLA::VectorS<4, Real>> >;
template class FieldSubGroup<PhysD2, FieldTraits<TopoD2, DLA::VectorS<5, Real>> >;
template class FieldSubGroup<PhysD2, FieldTraits<TopoD2, DLA::VectorS<6, Real>> >;
// For Boltzmann Implementation
template class FieldSubGroup<PhysD2, FieldTraits<TopoD2, DLA::VectorS<9, Real>> >;
//template class FieldSubGroup<PhysD2, FieldTraits<TopoD2, DLA::VectorS<13, Real>> >;
//template class FieldSubGroup<PhysD2, FieldTraits<TopoD2, DLA::VectorS<16, Real>> >;
template class FieldSubGroup<PhysD2, FieldTraits<TopoD2, DLA::VectorS<NVar, Real>> >;

template class FieldSubGroup<PhysD2, FieldTraits<TopoD2, DLA::MatrixS<3, 3, Real>> >;
template class FieldSubGroup<PhysD2, FieldTraits<TopoD2, DLA::MatrixS<3, 3, SurrealS<1>> > >;

template class FieldSubGroup<PhysD2, FieldTraits<TopoD2, DLA::MatrixSymS<2, Real>> >;

template class FieldSubGroup<PhysD2, FieldTraits<TopoD2, DLA::VectorS<2, DLA::VectorS<2, Real>> > >;
template class FieldSubGroup<PhysD2, FieldTraits<TopoD2, DLA::VectorS<2, DLA::VectorS<3, Real>> > >;
template class FieldSubGroup<PhysD2, FieldTraits<TopoD2, DLA::VectorS<2, DLA::VectorS<4, Real>> > >;
template class FieldSubGroup<PhysD2, FieldTraits<TopoD2, DLA::VectorS<2, DLA::VectorS<5, Real>> > >;


// Field instantiation
template class Field<PhysD2, TopoD2, Real>;
template class Field<PhysD2, TopoD2, SurrealS<1> >;

template class Field<PhysD2, TopoD2, DLA::VectorS<1, Real> >;
template class Field<PhysD2, TopoD2, DLA::VectorS<2, Real> >;
template class Field<PhysD2, TopoD2, DLA::VectorS<3, Real> >;
template class Field<PhysD2, TopoD2, DLA::VectorS<4, Real> >;
template class Field<PhysD2, TopoD2, DLA::VectorS<5, Real> >;
template class Field<PhysD2, TopoD2, DLA::VectorS<6, Real> >;
// For Boltzmann Implementation
template class Field<PhysD2, TopoD2, DLA::VectorS<9, Real> >;
//template class Field<PhysD2, TopoD2, DLA::VectorS<13, Real> >;
//template class Field<PhysD2, TopoD2, DLA::VectorS<16, Real> >;
template class Field<PhysD2, TopoD2, DLA::VectorS<NVar, Real> >;

template class Field< PhysD2, TopoD2, DLA::MatrixS<3, 3, Real> >;
template class Field< PhysD2, TopoD2, DLA::MatrixS<3, 3, SurrealS<1> > >;

template class Field< PhysD2, TopoD2, DLA::MatrixSymS<2, Real> >;

template class Field< PhysD2, TopoD2, DLA::VectorS<2, DLA::VectorS<2, Real> > >;
template class Field< PhysD2, TopoD2, DLA::VectorS<2, DLA::VectorS<3, Real> > >;
template class Field< PhysD2, TopoD2, DLA::VectorS<2, DLA::VectorS<4, Real> > >;
template class Field< PhysD2, TopoD2, DLA::VectorS<2, DLA::VectorS<5, Real> > >;

}
