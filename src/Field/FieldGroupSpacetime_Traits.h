// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDGROUPSPACETIME_TRAITS_H
#define FIELDGROUPSPACETIME_TRAITS_H

#include "FieldTypes.h"
#include "FieldAssociativity.h"
#include "Field/Element/Element.h"
#include "Field/Element/ElementAssociativitySpacetime.h"

#include "tools/Surrealize.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Traits for a field group of spacetime topologies
//----------------------------------------------------------------------------//

template<class Tin>
struct FieldGroupSpacetimeTraitsBase
{
  typedef FieldAssociativityBase< Tin > FieldBase;

  typedef Tin T;                                     // DOF type
};


template<class Topology, class T>
struct FieldGroupSpacetimeTraits : public FieldGroupSpacetimeTraitsBase<T>
{
  typedef BasisFunctionSpacetimeBase<Topology> BasisType;
  typedef Topology TopologyType;
  typedef ElementAssociativity<TopoD4,Topology> ElementAssociativityType;
  typedef FieldAssociativityConstructor< typename ElementAssociativityType::Constructor > FieldAssociativityConstructorType;

  template<class ElemT = Real>
  using ElementType = Element< typename Surrealize<ElemT,T>::T, TopoD4, Topology >;
};

} //namespace SANS

#endif //FIELDGROUPSPACETIME_TRAITS_H
