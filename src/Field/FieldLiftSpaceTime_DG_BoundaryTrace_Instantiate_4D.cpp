// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define FIELDLIFT_DG_BOUNDARYTRACEBASE_INSTANTIATE
#include "FieldLift_DG_BoundaryTraceBase_impl.h"

#define FIELDLIFTSPACETIME_DG_BOUNDARYTRACE_INSTANTIATE
#include "FieldLiftSpaceTime_DG_BoundaryTrace_impl.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "XFieldSpacetime.h"

namespace SANS
{

template class FieldLift_DG_BoundaryTrace< PhysD4, TopoD4, DLA::VectorS<4, Real> >;

// template class FieldLift_DG_BoundaryTrace< PhysD4, TopoD4, DLA::VectorS<4, DLA::VectorS<6, Real> > >;
// template class FieldLift_DG_BoundaryTrace< PhysD4, TopoD4, DLA::VectorS<4, DLA::VectorS<7, Real> > >;

}
