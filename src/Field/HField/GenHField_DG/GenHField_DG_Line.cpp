// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "GenHField_DG_impl.h"

#include "Field/XFieldLine.h"
#include "Field/FieldLine_DG_Cell.h"

namespace SANS
{

// Explicit instantiations
template class GenHField_DG<PhysD1, TopoD1>;

} // namespace SANS
