// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "../GenHField_DG.h"

#include <vector>

#include "tools/SANSException.h"
#include "tools/linspace.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Pow.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Log.h"

#include "Field/Tuple/FieldTuple.h"
#include "Field/tools/for_each_CellGroup.h"

namespace SANS
{

template<class PhysDim, class TopoDim>
class GenHField_DG_Elemental_impl : public GroupFunctorCellType<GenHField_DG_Elemental_impl<PhysDim, TopoDim>>
{
public:
  typedef GenHField_DG<PhysDim, TopoDim> GenHClass;
  typedef typename GenHClass::MatrixSym MatrixSym;

  GenHField_DG_Elemental_impl( const std::vector<int>& cellgroup_list ) :
    cellgroup_list_(cellgroup_list) {}

  std::size_t nCellGroups() const          { return cellgroup_list_.size(); }
  std::size_t cellGroup(const int n) const { return cellgroup_list_[n];     }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology>
  void
  apply(const typename FieldTuple<XField<PhysDim, TopoDim>, GenHClass, TupleClass<>>::
        template FieldCellGroupType<Topology>& fldsCellGroup,
        const int cellGroupGlobal)
  {
    typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename                GenHClass::template FieldCellGroupType<Topology> HFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename HFieldCellGroupType::template ElementType<> ElementHFieldClass;

    const XFieldCellGroupType& xfldCellGroup = get<0>(fldsCellGroup);
    HFieldCellGroupType& HfldCellGroup = const_cast<HFieldCellGroupType&>(get<1>(fldsCellGroup));

    SANS_ASSERT_MSG( HfldCellGroup.basis()->order() == 0, "DG GenH field has to be P0!" );

    ElementXFieldClass xfldElem( xfldCellGroup.basis() );
    ElementHFieldClass HfldElem( HfldCellGroup.basis() );

    int nElem = xfldCellGroup.nElem(); //Number of elements in cell group
    SANS_ASSERT( nElem == HfldCellGroup.nElem() );

    MatrixSym metric; //storage for elemental metric

    for (int elem = 0; elem < nElem; elem++ )
    {
      xfldCellGroup.getElement(xfldElem,elem);

      //Get elemental implied metric
      xfldElem.impliedMetric(metric);

      HfldCellGroup.getElement( HfldElem, elem );
      HfldElem.DOF(0) = log(pow(metric, -0.5)); // compute the log(H) tensor
      HfldCellGroup.setElement( HfldElem, elem );
    }
  }

protected:
  const std::vector<int> cellgroup_list_;
};

//----------------------------------------------------------------------------//
// Class for computing a DG P0 log-GenHField
//----------------------------------------------------------------------------//

template<class PhysDim, class TopoDim>
GenHField_DG<PhysDim, TopoDim>::GenHField_DG( const XField<PhysDim, TopoDim>& xfld ) :
  GenHField_DG( xfld, linspace(0, xfld.nCellGroups()-1) ) {}

template<class PhysDim, class TopoDim>
GenHField_DG<PhysDim, TopoDim>::GenHField_DG( const XField<PhysDim, TopoDim>& xfld, const std::vector<int>& cellgroup_list ) :
  BaseType(xfld, 0, BasisFunctionCategory_Legendre, cellgroup_list)
{
  //Calculate elemental implied metrics on all processors
  for_each_CellGroup<TopoDim>::apply( GenHField_DG_Elemental_impl<PhysDim, TopoDim>(globalCellGroups_), (xfld_, *this) );

}

template<class PhysDim, class TopoDim>
GenHField_DG<PhysDim, TopoDim>::GenHField_DG( const XField_Local_Base<PhysDim, TopoDim>& xfld_local,
                                              const GenHField_DG<PhysDim, TopoDim>& hfld_global ) :
                                              GenHField_DG(xfld_local) {}

} // namespace SANS
