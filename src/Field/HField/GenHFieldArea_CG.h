// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef GENHFIELDAREA_CG_H
#define GENHFIELDAREA_CG_H

#include "Field/XFieldArea.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/HField/GenHField_CG.h"

#endif //GENHFIELDAREA_CG_H
