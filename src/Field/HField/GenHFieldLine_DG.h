// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef GENHFIELDLINE_DG_H
#define GENHFIELDLINE_DG_H

#include "Field/XFieldLine.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/HField/GenHField_DG.h"

#endif //GENHFIELDLINE_DG_H
