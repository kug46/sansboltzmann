// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "GenHField_CG_impl.h"

#include "Field/XFieldVolume.h"
#include "Field/FieldVolume_CG_Cell.h"

namespace SANS
{

// Explicit instantiations
template class GenHField_CG<PhysD3, TopoD3>;

} // namespace SANS
