// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "../GenHField_CG.h"

#include <vector>
#include <algorithm>
#include <set>

#include "tools/SANSException.h"
#include "tools/linspace.h"

#include "Meshing/Metric/MetricOps.h"

#include "Field/tools/for_each_CellGroup.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_MPI
#include "MPI/serialize_DenseLinAlg_MatrixS.h"
#include <boost/serialization/vector.hpp>
#endif // SANS_MPI

namespace SANS
{

struct GenHFieldLocalNodeInfo
{
  GenHFieldLocalNodeInfo() : global_node(-1), new_node(false) {}

  int global_node; //nodeDOF index of the global hfld to project average h-tensor data from
  bool new_node; // The node is new based on the split of the local xfield

  // lists of {cellgroup, cellelem} pairs referring to the elemental metrics that should be subtracted/added to the nodal average
  std::set<std::pair<int, int>> global_elem_subtractlist;
  std::set<std::pair<int, int>> local_elem_addlist;
};

template<class PhysDim, class TopoDim>
class GenHField_Elemental_impl : public GroupFunctorCellType<GenHField_Elemental_impl<PhysDim, TopoDim>>
{
public:
  typedef GenHField_CG<PhysDim, TopoDim> GenHClass;
  typedef typename GenHClass::ElementalMetricArrayType ElementalMetricArrayType;

  GenHField_Elemental_impl( const std::vector<int>& cellgroup_list, ElementalMetricArrayType& elementalMetrics ) :
    cellgroup_list_(cellgroup_list), elementalMetrics_(elementalMetrics) {}

  std::size_t nCellGroups() const          { return cellgroup_list_.size(); }
  std::size_t cellGroup(const int n) const { return cellgroup_list_[n];     }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology>
  void
  apply(const typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
        const int cellGroupGlobal)
  {
    //Cell Group Types
    typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

    ElementXFieldClass xfldElem(xfldCellGroup.basis() );

    int nElem = xfldCellGroup.nElem(); //Number of elements in cell group

    elementalMetrics_[cellGroupGlobal].resize(nElem,0.0);

    for (int elem = 0; elem < nElem; elem++ )
    {
      xfldCellGroup.getElement(xfldElem,elem);

      //Save off elemental implied metric
      xfldElem.impliedMetric(elementalMetrics_[cellGroupGlobal][elem]);
    }
  }

protected:
  const std::vector<int> cellgroup_list_;
  ElementalMetricArrayType& elementalMetrics_;
};

template<class PhysDim, class TopoDim>
class GenHField_Elemental_Local_impl : public GroupFunctorCellType<GenHField_Elemental_Local_impl<PhysDim, TopoDim>>
{
public:
  typedef GenHField_CG<PhysDim, TopoDim> GenHClass;
  typedef typename GenHClass::ElementalMetricArrayType ElementalMetricArrayType;

  GenHField_Elemental_Local_impl( const std::vector<int>& cellgroup_list,
                                  const XField_Local_Base<PhysDim, TopoDim>& xfld_local, const GenHClass& hfld_global,
                                  const GenHClass& hfld_local, std::vector<GenHFieldLocalNodeInfo>& nodalinfolist,
                                  ElementalMetricArrayType& elementalMetrics ) :
    cellgroup_list_(cellgroup_list), xfld_local_(xfld_local), hfld_global_(hfld_global), hfld_local_(hfld_local),
    nodalinfolist_(nodalinfolist), elementalMetrics_(elementalMetrics) {}

  std::size_t nCellGroups() const          { return cellgroup_list_.size(); }
  std::size_t cellGroup(const int n) const { return cellgroup_list_[n];     }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology >
  void
  apply(const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup_local,
        const int cellGroupGlobal)
  {
    //Cell Group Types
    typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

    typedef typename GenHClass::template FieldCellGroupType<Topology> HFieldCellGroupType;

    ElementXFieldClass xfldElem_local(xfldCellGroup_local.basis() );

    int nElem = xfldCellGroup_local.nElem(); //Number of elements in cell group

    elementalMetrics_[cellGroupGlobal].resize(nElem,0.0);

    const HFieldCellGroupType& hfld_local_cellgrp = hfld_local_.template getCellGroup<Topology>(cellGroupGlobal);

    // The list of all new nodes in the split local mesh
    std::vector<int> new_nodelist = xfld_local_.getNewNodeDOFs();

    //loop over cells in local mesh
    for (int elem = 0; elem < nElem; elem++ )
    {
      xfldCellGroup_local.getElement(xfldElem_local, elem);

      //Save off elemental implied metric on split local mesh
      xfldElem_local.impliedMetric(elementalMetrics_[cellGroupGlobal][elem]);

      // Get the xfld_local element node map to check if any node is new in the local mesh
      int nodeDOFMap_xfld_local[Topology::NNode];
      xfldCellGroup_local.associativity(elem).getNodeGlobalMapping(nodeDOFMap_xfld_local, Topology::NNode);

      // Node map for the GenHField_local
      int nodeDOFMap_local[Topology::NNode];
      hfld_local_cellgrp.associativity(elem).getNodeGlobalMapping(nodeDOFMap_local, Topology::NNode);

      //Find the corresponding unsplit cell in the global mesh
      std::pair<int,int> cellmap = xfld_local_.getGlobalCellMap({cellGroupGlobal, elem});
      int global_cellgrp_ind  = cellmap.first;
      int global_cellelem_ind = cellmap.second;

      const HFieldCellGroupType& hfld_global_cellgrp = hfld_global_.template getCellGroup<Topology>(global_cellgrp_ind);

      // Node map for the GenHField_global
      int nodeDOFMap_global[Topology::NNode];
      hfld_global_cellgrp.associativity(global_cellelem_ind).getNodeGlobalMapping(nodeDOFMap_global, Topology::NNode);

      for (int k = 0; k < Topology::NNode; k++)
      {
        const int node_local  = nodeDOFMap_local[k];
        const int node_global = nodeDOFMap_global[k];
        bool new_node = (std::find(new_nodelist.begin(), new_nodelist.end(), nodeDOFMap_xfld_local[k]) != new_nodelist.end());

        nodalinfolist_[node_local].global_node = node_global;
        nodalinfolist_[node_local].new_node = new_node;
        nodalinfolist_[node_local].global_elem_subtractlist.insert({global_cellgrp_ind, global_cellelem_ind});
        nodalinfolist_[node_local].local_elem_addlist.insert({cellGroupGlobal, elem});
      }
    } //loop over elements
  }

protected:
  const std::vector<int> cellgroup_list_;
  const XField_Local_Base<PhysDim, TopoDim>& xfld_local_;
  const GenHClass& hfld_global_;
  const GenHClass& hfld_local_;
  std::vector<GenHFieldLocalNodeInfo>& nodalinfolist_;
  ElementalMetricArrayType& elementalMetrics_;
};

//----------------------------------------------------------------------------//
// Class for computing a continuous P1 log-GenHField
//----------------------------------------------------------------------------//

template<class PhysDim, class TopoDim>
GenHField_CG<PhysDim, TopoDim>::GenHField_CG( const XField<PhysDim, TopoDim>& xfld ) :
  GenHField_CG( xfld, linspace(0, xfld.nCellGroups()-1) ) {}

template<class PhysDim, class TopoDim>
GenHField_CG<PhysDim, TopoDim>::GenHField_CG( const XField<PhysDim, TopoDim>& xfld, const std::vector<int>& cellgroup_list ) :
  BaseType(xfld, 1, BasisFunctionCategory_Hierarchical, cellgroup_list),
  nodalview_(*this)
{
  //Calculate elemental implied metrics on all processors
  elementalMetrics_.resize(xfld_.nCellGroups());
  for_each_CellGroup<TopoDim>::apply( GenHField_Elemental_impl<PhysDim, TopoDim>(globalCellGroups_, elementalMetrics_), xfld_);

  int nNode = this->nDOF();
  const std::vector<int>& node_list = nodalview_.getNodeDOFList(); //global node indices in xfld

  SANS_ASSERT( (int) node_list.size() == nNode );

  // Compute log-Euclidean averaged metrics as the initial guess for the implied metric
  for (int node = 0; node < nNode; node++)
  {
    int global_node = node_list[node];

    //Get the list of cells around this node (each returned pair contains cell_group and cell_elem)
    Field_NodalView::IndexVector cell_list = nodalview_.getCellList(global_node);

    std::size_t nCells = cell_list.size();
    std::vector<MatrixSym> metric_list(nCells);

    for (std::size_t i = 0; i < nCells; i++)
    {
      int cellgrp = cell_list[i].group;
      int elem = cell_list[i].elem;
      metric_list[i] = elementalMetrics_[cellgrp][elem];
    }

    //Average metric around node
    DLA::EigenSystemPair<D,Real> avgM = Metric::computeLogEuclideanAvg(metric_list);

    //Set log(H) = log(avgM^(-0.5)) tensor to CG field DOF
    this->DOF(global_node) = log(pow(avgM, -0.5));
  }

  // synchronize ghost/zombie DOFs
  this->syncDOFs_MPI_noCache();
}

template<class PhysDim, class TopoDim>
GenHField_CG<PhysDim, TopoDim>::GenHField_CG( const XField_Local_Base<PhysDim, TopoDim>& xfld_local,
                                              const GenHField_CG<PhysDim, TopoDim>& hfld_global ) :
  BaseType(xfld_local, 1, BasisFunctionCategory_Hierarchical), nodalview_(*this)
{
  elementalMetrics_.resize(this->nCellGroups());

  //Vector to hold information about how the h-tensor averages should be updated at each node in the local mesh
  //Using the length of nDOF() as the GenHField is hard coded as a linear CG field
  std::vector<GenHFieldLocalNodeInfo> nodalinfolist(this->nDOF());

  //Loop over each cell in the local mesh and track how the h-tensor average should be updated
  //i.e. which elemental metrics to subtract or add to the average
  for_each_CellGroup<TopoDim>::apply(
      GenHField_Elemental_Local_impl<PhysDim, TopoDim>(globalCellGroups_, xfld_local, hfld_global, *this, nodalinfolist, elementalMetrics_),
      xfld_local );

  const Field_NodalView& nodalview_global = hfld_global.nodalview_;
  const ElementalMetricArrayType& elementalMetrics_global = hfld_global.elementalMetrics_;

  for (std::size_t node = 0; node < nodalinfolist.size(); node++)
  {
    if ( nodalinfolist[node].new_node )
    {
      // New nodes in the local mesh are always averages of elements in the local mesh

      //Get the list of cells around this node (each returned pair contains cell_group and cell_elem)
      Field_NodalView::IndexVector cell_list = nodalview_.getCellList(node);

      std::size_t nCells = cell_list.size();
      std::vector<MatrixSym> metric_list(nCells);

      for (std::size_t i = 0; i < nCells; i++)
      {
        int cellgrp = cell_list[i].group;
        int elem = cell_list[i].elem;
        metric_list[i] = elementalMetrics_[cellgrp][elem];
      }

      //Average metric around node
      DLA::EigenSystemPair<D,Real> avgM = Metric::computeLogEuclideanAvg(metric_list);

      //Set log(H) = log(avgM^(-0.5)) tensor to CG field DOF
      this->DOF(node) = log(pow(avgM, -0.5));

    }
    else // nodalinfolist[node].new_node == false
    {
      // Nodes that existed in the global mesh are corrected by first removing the
      // cells that was split from the metric average in the global mesh, and then
      // adding in the metrics from the split cells

      const int global_node = nodalinfolist[node].global_node;

      //Get the number of cells around this node in the global mesh
      const std::size_t nCells = nodalview_global.getCellList(global_node).size();

      //Get the average h-tensor and metric at this global node
      DLA::EigenSystemPair<D,Real> avgH = hfld_global.DOF(global_node);
      MatrixSym avg_logM = log(pow(exp(avgH), -2.0));

      //Undo log-Euclidean averaging
      MatrixSym sumlogM = nCells*avg_logM;

      std::size_t nCells_new = nCells;

      //Subtract off elemental metrics of the unsplit elements in the global mesh, and update the cell count around this node
      std::set<std::pair<int, int>>::iterator it;
      for (it = nodalinfolist[node].global_elem_subtractlist.begin(); it != nodalinfolist[node].global_elem_subtractlist.end(); ++it)
      {
        nCells_new--;
        sumlogM -= log(elementalMetrics_global[it->first][it->second]);
      }

      //Add the elemental metrics of the split elements in the local mesh, and update the cell count around this node
      for (it = nodalinfolist[node].local_elem_addlist.begin(); it != nodalinfolist[node].local_elem_addlist.end(); ++it)
      {
        nCells_new++;
        sumlogM += log(elementalMetrics_[it->first][it->second]);
      }

      avg_logM = sumlogM / ((Real) nCells_new);
      DLA::EigenSystemPair<D,Real> avgM = exp(DLA::EigenSystemPair<D,Real>(avg_logM)); //new average nodal metric

      this->DOF(node) = log(pow(avgM, -0.5)); //updated average h-tensor
    }
  }
}

} // namespace SANS
