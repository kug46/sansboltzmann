// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "../HField_DG.h"

#include <vector>

#include "tools/SANSException.h"
#include "tools/linspace.h"

#include "Quadrature/Quadrature.h"
#include "BasisFunction/Quadrature_Cache.h"

#include "Field/tools/for_each_CellGroup.h"
#include "Field/tools/for_each_InteriorTraceGroup_Cell.h"
#include "Field/tools/for_each_BoundaryTraceGroup_Cell.h"
#include "Field/Tuple/FieldTuple.h"

namespace SANS
{


//----------------------------------------------------------------------------//
// HField class for computing a simple discontinuous HField
// with the cell volume divided by the total perimeter area
//----------------------------------------------------------------------------//

template<class PhysDim>
class HField_InteriorPerimeterSize:
    public GroupFunctorInteriorTraceType<HField_InteriorPerimeterSize<PhysDim>>
{
public:

  explicit HField_InteriorPerimeterSize( const std::vector<int>& interiorTraceGroups ) :
    interiorTraceGroups_(interiorTraceGroups) {}

  std::size_t nInteriorTraceGroups() const { return interiorTraceGroups_.size(); }
  std::size_t interiorTraceGroup(const int n) const { return interiorTraceGroups_[n]; }

  //----------------------------------------------------------------------------//
  // Distribution function that redistributes the error in each cell group
  template< class TopologyTrace, class TopologyL, class TopologyR>
  void
  apply( const typename Field<PhysDim, typename TopologyL::TopoDim, Real>::template FieldCellGroupType<TopologyL>& hfldCellL,
         const int cellGroupGlobalL,
         const typename Field<PhysDim, typename TopologyR::TopoDim, Real>::template FieldCellGroupType<TopologyR>& hfldCellR,
         const int cellGroupGlobalR,
         const typename XField<PhysDim, typename TopologyTrace::CellTopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
         const int traceGroupGlobal)
  {
    // Cell Group Types
    typedef typename Field<PhysDim, typename TopologyL::TopoDim, Real>::template FieldCellGroupType<TopologyL> HFieldCellGroupTypeL;
    typedef typename Field<PhysDim, typename TopologyR::TopoDim, Real>::template FieldCellGroupType<TopologyR> HFieldCellGroupTypeR;

    typedef typename XField<PhysDim, typename TopologyTrace::CellTopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;

    typedef typename HFieldCellGroupTypeL::template ElementType<> ElementHFieldCellClassL;
    typedef typename HFieldCellGroupTypeR::template ElementType<> ElementHFieldCellClassR;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;

    typedef typename TopologyTrace::TopoDim TopoDimTrace;

    typedef QuadraturePoint<TopoDimTrace> QuadPointType;

    // Construct the elements
    ElementHFieldCellClassL hfldElemL( hfldCellL.basis() );
    ElementHFieldCellClassR hfldElemR( hfldCellR.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    Quadrature<TopoDimTrace, TopologyTrace> quadrature( xfldTrace.basis()->order() );

    const int nquad = quadrature.nQuadrature();
    Real weight, J;

    // loop over elements
    const int nElem = xfldTrace.nElem();
    for (int elem = 0; elem < nElem; elem++)
    {
      int elemL = xfldTrace.getElementLeft( elem );
      int elemR = xfldTrace.getElementRight( elem );

      // copy global grid/solution DOFs to element
      xfldTrace.getElement( xfldElemTrace, elem );
      hfldCellL.getElement( hfldElemL, elemL );
      hfldCellR.getElement( hfldElemR, elemR );

      // compute the size of the trace element
      J = 0;
      for (int iquad = 0; iquad < nquad; iquad++)
      {
        quadrature.weight( iquad, weight );
        QuadPointType sRef = quadrature.coordinates_cache( iquad );

        J += weight * xfldElemTrace.jacobianDeterminant( sRef );
      }

      // add the perimeter contribution
      hfldElemL.DOF(0) += J;
      hfldElemR.DOF(0) += J;

      // save the elements
      const_cast<HFieldCellGroupTypeL&>(hfldCellL).setElement( hfldElemL, elemL );
      const_cast<HFieldCellGroupTypeR&>(hfldCellR).setElement( hfldElemR, elemR );
    }
  }
protected:
  const std::vector<int> interiorTraceGroups_;
};

//----------------------------------------------------------------------------//
template<class PhysDim>
class HField_BoundaryPerimeterSize:
    public GroupFunctorBoundaryCellType<HField_BoundaryPerimeterSize<PhysDim>>
{
public:

  explicit HField_BoundaryPerimeterSize( const std::vector<int>& boundaryTraceGroups ) :
    boundaryTraceGroups_(boundaryTraceGroups) {}

  std::size_t nBoundaryGroups() const { return boundaryTraceGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return boundaryTraceGroups_[n]; }

  //----------------------------------------------------------------------------//
  // Distribution function that redistributes the error in each cell group
  template< class TopologyTrace, class TopologyL>
  void
  apply( const typename Field<PhysDim, typename TopologyL::TopoDim, Real>::template FieldCellGroupType<TopologyL>& hfldCellL,
         const int cellGroupGlobalL,
         const typename XField<PhysDim, typename TopologyTrace::CellTopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
         const int traceGroupGlobal)
  {
    // Cell Group Types
    typedef typename Field<PhysDim, typename TopologyL::TopoDim, Real>::template FieldCellGroupType<TopologyL> HFieldCellGroupTypeL;

    typedef typename XField<PhysDim, typename TopologyTrace::CellTopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;

    typedef typename HFieldCellGroupTypeL::template ElementType<> ElementHFieldCellClassL;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;

    typedef typename TopologyTrace::TopoDim TopoDimTrace;

    typedef QuadraturePoint<TopoDimTrace> QuadPointType;

    // Construct the elements
    ElementHFieldCellClassL hfldElemL( hfldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    Quadrature<TopoDimTrace, TopologyTrace> quadrature( xfldTrace.basis()->order() );

    const int nquad = quadrature.nQuadrature();
    Real weight, J;

    // loop over elements
    const int nElem = xfldTrace.nElem();
    for (int elem = 0; elem < nElem; elem++)
    {
      int elemL = xfldTrace.getElementLeft( elem );

      // copy global grid/solution DOFs to element
      xfldTrace.getElement( xfldElemTrace, elem );
      hfldCellL.getElement( hfldElemL, elemL );

      // compute the size of the trace element
      J = 0;
      for (int iquad = 0; iquad < nquad; iquad++)
      {
        quadrature.weight( iquad, weight );
        QuadPointType sRef = quadrature.coordinates_cache( iquad );

        J += weight * xfldElemTrace.jacobianDeterminant( sRef );
      }

      // add the perimeter contribution
      hfldElemL.DOF(0) += J;

      // save the elements
      const_cast<HFieldCellGroupTypeL&>(hfldCellL).setElement( hfldElemL, elemL );
    }
  }
protected:
  const std::vector<int> boundaryTraceGroups_;
};

//----------------------------------------------------------------------------//
template<class PhysDim>
class HField_CellSize : public GroupFunctorCellType<HField_CellSize<PhysDim>>
{
public:

  explicit HField_CellSize( const std::vector<int>& cellGroups ) :
    cellGroups_(cellGroups)
  {
    // Nothing
  }

  std::size_t nCellGroups() const          { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n];     }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology >
  void
  apply( const typename FieldTuple<XField<PhysDim, typename Topology::TopoDim>, Field<PhysDim, typename Topology::TopoDim, Real>, TupleClass<>>::
                        template FieldCellGroupType<Topology>& fldsCell,
         const int cellGroupGlobal )
  {
    typedef typename XField<PhysDim, typename Topology::TopoDim>     ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field<PhysDim, typename Topology::TopoDim, Real>::template FieldCellGroupType<Topology> HFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename HFieldCellGroupType::template ElementType<> ElementHFieldClass;

    typedef QuadraturePoint<typename Topology::TopoDim> QuadPointType;

    const XFieldCellGroupType& xfldCell = get<0>(fldsCell);
          HFieldCellGroupType& hfldCell = const_cast<HFieldCellGroupType&>( get<1>(fldsCell) );

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementHFieldClass hfldElem( hfldCell.basis() );

    SANS_ASSERT_MSG( hfldCell.basis()->order() == 0, "Currently only assumes P0 h field" );

    Quadrature<typename Topology::TopoDim, Topology> quadrature( xfldCell.basis()->order() );

    const int nquad = quadrature.nQuadrature();
    Real weight, J;

    // loop over elements within group
    const int nelem = xfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid DOFs to element
      xfldCell.getElement( xfldElem, elem );
      hfldCell.getElement( hfldElem, elem );

      // compute the size of the cell element
      J = 0;
      for (int iquad = 0; iquad < nquad; iquad++)
      {
        quadrature.weight( iquad, weight );
        QuadPointType sRef = quadrature.coordinates_cache( iquad );

        J += weight * xfldElem.jacobianDeterminant( sRef );
      }

      // cell size divided by perimeter size
      hfldElem.DOF(0) = J/hfldElem.DOF(0);

      // save the h
      hfldCell.setElement( hfldElem, elem );
    }
  }

protected:
  const std::vector<int> cellGroups_;
};

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
HField_DG<PhysDim, TopoDim>::HField_DG( const XField<PhysDim, TopoDim>& xfld ) :
Field_DG_Cell<PhysDim, TopoDim, Real>(xfld, 0, BasisFunctionCategory_Legendre)
{
  // zero out the hfield so it can be filled with the perimeter size first
  for (int i = 0; i < this->nDOF_; i++)
    this->DOF_[i] = 0;

  if (xfld.nInteriorTraceGroups() > 0)
  {
    std::vector<int> globalInteriorTraceGroups = linspace(0, xfld.nInteriorTraceGroups()-1);
    for_each_InteriorTraceGroup_Cell<TopoDim>::apply( HField_InteriorPerimeterSize<PhysDim>(globalInteriorTraceGroups), *this);
  }

  //TODO: slightly concerned that periodic boundaries might mess this up...
  if (xfld.nBoundaryTraceGroups() > 0)
  {
    std::vector<int> globalBoundaryTraceGroups = linspace(0, xfld.nBoundaryTraceGroups()-1);
    for_each_BoundaryTraceGroup_Cell<TopoDim>::apply( HField_BoundaryPerimeterSize<PhysDim>(globalBoundaryTraceGroups), *this);
  }

  // Compute cell size over perimeter size
  for_each_CellGroup<TopoDim>::apply( HField_CellSize<PhysDim>(globalCellGroups_), (xfld_, *this));
}

} // namespace SANS
