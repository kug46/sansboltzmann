// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef HFIELDLINE_CG_H
#define HFIELDLINE_CG_H

#include "Field/XFieldLine.h"
#include "Field/FieldLine_CG_Cell.h"
#include "Field/HField/HField_CG.h"

#endif //HFIELDLINE_CG_H
