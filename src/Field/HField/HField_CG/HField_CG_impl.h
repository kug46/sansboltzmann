// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "../HField_CG.h"

#include <vector>

#include "tools/SANSException.h"

#include "BasisFunction/BasisFunctionCategory.h"

#include "Field/Field_NodalView.h"

#include "Field/HField/HField_DG.h"

#include "Field/tools/for_each_CellGroup.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// HField class for computing a continuous P1 HField
//----------------------------------------------------------------------------//

template<class PhysDim>
Real
HField_DG_Interface<PhysDim, TopoD1>::getCellSize(const Field_DG_Cell<PhysDim, TopoD1, Real>& hfld_DG,
                                                  const int cellgroup, const int elem)
{
  if (hfld_DG.getCellGroupBase(cellgroup).topoTypeID() == typeid(Line))
  {
    return getCellSizeCellGroup<Line>(hfld_DG, cellgroup, elem);
  }
  else
    SANS_DEVELOPER_EXCEPTION("getCellSize<PhysDim, TopoD1> - Unknown topology for cell group.");

  return -1;
}

template<class PhysDim>
Real
HField_DG_Interface<PhysDim, TopoD2>::getCellSize(const Field_DG_Cell<PhysDim, TopoD2, Real>& hfld_DG,
                                                  const int cellgroup, const int elem)
{
  if (hfld_DG.getCellGroupBase(cellgroup).topoTypeID() == typeid(Triangle))
  {
    return getCellSizeCellGroup<Triangle>(hfld_DG, cellgroup, elem);
  }
  else if (hfld_DG.getCellGroupBase(cellgroup).topoTypeID() == typeid(Quad))
  {
    return getCellSizeCellGroup<Quad>(hfld_DG, cellgroup, elem);
  }
  else
    SANS_DEVELOPER_EXCEPTION("getCellSize<PhysDim, TopoD2> - Unknown topology for cell group.");

  return -1;
}

template<class PhysDim>
Real
HField_DG_Interface<PhysDim, TopoD3>::getCellSize(const Field_DG_Cell<PhysDim, TopoD3, Real>& hfld_DG,
                                                  const int cellgroup, const int elem)
{
  if (hfld_DG.getCellGroupBase(cellgroup).topoTypeID() == typeid(Tet))
  {
    return getCellSizeCellGroup<Tet>(hfld_DG, cellgroup, elem);
  }
  else if (hfld_DG.getCellGroupBase(cellgroup).topoTypeID() == typeid(Hex))
  {
    return getCellSizeCellGroup<Hex>(hfld_DG, cellgroup, elem);
  }
  else
    SANS_DEVELOPER_EXCEPTION("getCellSize<PhysDim, TopoD3> - Unknown topology for cell group.");

  return -1;
}


//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
HField_CG<PhysDim, TopoDim>::HField_CG( const XField<PhysDim, TopoDim>& xfld ) :
Field_CG_Cell<PhysDim, TopoDim, Real>(xfld, 1, BasisFunctionCategory_Hierarchical)
{
  //The loop for setting CG field DOFs below assumes that the CG field DOF indices
  //are the same as the xfld ones, which is true for P1.
  for (int i = 0; i < xfld.nCellGroups(); i++)
    SANS_ASSERT( xfld.getCellGroupBase(i).order() == 1 );

  //Create a P0 DG HField to obtain cell sizes
  HField_DG<PhysDim, TopoDim> hfld_DG(xfld);

  std::vector<int> cellgroups;
  for (int i = 0; i < xfld.nCellGroups(); i++)
    cellgroups.push_back(i);

  //Create nodal view of xfld
  Field_NodalView nodalview(xfld,cellgroups);

  int nNode = this->nDOF();
  const std::vector<int>& node_list = nodalview.getNodeDOFList(); //global node indices in xfld

  SANS_ASSERT( (int) node_list.size() == nNode );

  for (int node = 0; node < nNode; node++)
  {
    //Obtain the list of cells around this node
    int global_node = node_list[node];
    Field_NodalView::IndexVector cell_list = nodalview.getCellList(global_node);

    int nCells = (int) cell_list.size();
    std::vector<Real> cell_sizes(nCells, 0.0);

    Real h_sum = 0.0;

    for (int i = 0; i < nCells; i++)
    {
      int cellgroup = cell_list[i].group;
      int cellelem = cell_list[i].elem;

      h_sum += HField_DG_Interface<PhysDim, TopoDim>::getCellSize(hfld_DG, cellgroup, cellelem);
    }

    //Set the CG field DOF at this node to the average h value
    this->DOF(global_node) = h_sum / (Real)nCells;
  }

}

} // namespace SANS
