// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "HField_CG_impl.h"

#include "Field/XFieldVolume.h"
#include "Field/FieldVolume_CG_Cell.h"

#include "Field/HField/HFieldVolume_DG.h"

namespace SANS
{

// Explicit instantiations
template class HField_CG<PhysD3, TopoD3>;

} // namespace SANS
