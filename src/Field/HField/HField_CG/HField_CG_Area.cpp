// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "HField_CG_impl.h"

#include "Field/XFieldArea.h"
#include "Field/FieldArea_CG_Cell.h"

#include "Field/HField/HFieldArea_DG.h"

namespace SANS
{

// Explicit instantiations
template class HField_CG<PhysD2, TopoD2>;

} // namespace SANS
