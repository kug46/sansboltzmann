// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef HFIELDAREA_DG_H
#define HFIELDAREA_DG_H

#include "Field/XFieldArea.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/HField/HField_DG.h"

#endif //HFIELDAREA_DG_H
