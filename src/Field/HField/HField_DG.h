// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef HFIELD_DG_H
#define HFIELD_DG_H

#include "tools/SANSnumerics.h"     // Real

#include "Field/FieldTypes.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// HField represent grid spacing with a DG polynomial
//----------------------------------------------------------------------------//

template<class PhysDim, class TopoDim>
class HField_DG : public Field_DG_Cell<PhysDim, TopoDim, Real>
{
public:
  typedef Field_DG_Cell<PhysDim, TopoDim, Real> BaseType;

  HField_DG( const HField_DG& ) = delete;
  HField_DG& operator=( const HField_DG& ) = delete;

  explicit HField_DG( const XField<PhysDim, TopoDim>& xfld );

protected:
  using BaseType::xfld_;
  using BaseType::globalCellGroups_;
  using BaseType::globalInteriorTraceGroups_;
  using BaseType::globalBoundaryTraceGroups_;

};  // class HField_DG


} // namespace SANS

#endif // HFIELD_DG_H
