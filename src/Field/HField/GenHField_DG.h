// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef GENHFIELD_DG_H
#define GENHFIELD_DG_H

#include "tools/SANSnumerics.h"     // Real

#include "Topology/ElementTopology.h"

#include "Field/FieldTypes.h"
#include "Field/tools/GroupFunctorType.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"

#include "Field/Local/XField_Local_Base.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// HField representing the log of the generalized h-tensor with a DG P0 field
//----------------------------------------------------------------------------//

template<class PhysDim, class TopoDim>
class GenHField_DG : public Field_DG_Cell<PhysDim, TopoDim, DLA::MatrixSymS<PhysDim::D,Real>>
{
public:
  typedef PhysDim PhysD;
  typedef TopoDim TopoD;

  static const int D = PhysDim::D;   // physical dimensions
  typedef DLA::MatrixSymS<D,Real> MatrixSym;

  typedef Field_DG_Cell<PhysDim, TopoDim, MatrixSym> BaseType;
  typedef GenHField_DG<PhysDim, TopoDim> GenHClass;

  GenHField_DG( const GenHField_DG& ) = delete;
  GenHField_DG& operator=( const GenHField_DG& ) = delete;

  explicit GenHField_DG( const XField<PhysDim, TopoDim>& xfld ); //generates HField for all cellgroups

  GenHField_DG( const XField<PhysDim, TopoDim>& xfld, const std::vector<int>& cellgroup_list );

  GenHField_DG( const XField_Local_Base<PhysDim, TopoDim>& xfld_local, const GenHClass& hfld_global );

protected:

  using BaseType::xfld_;
  using BaseType::globalCellGroups_;
};

} // namespace SANS

#endif // GENHFIELD_DG_H
