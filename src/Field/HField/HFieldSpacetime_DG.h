// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef HFIELDSPACETIME_DG_H
#define HFIELDSPACETIME_DG_H

#include "Field/XFieldSpacetime.h"
#include "Field/FieldSpacetime_DG_Cell.h"
#include "Field/HField/HField_DG.h"

#endif //HFIELDSPACETIME_DG_H
