// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef HFIELD_CG_H
#define HFIELD_CG_H

#include "tools/SANSException.h"
#include "tools/SANSnumerics.h"     // Real

#include "Topology/ElementTopology.h"
#include "Field/FieldTypes.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// HField representing grid spacing with a CG field
//----------------------------------------------------------------------------//

template<class PhysDim, class TopoDim>
class HField_CG : public Field_CG_Cell<PhysDim, TopoDim, Real>
{
public:
  typedef Field_CG_Cell<PhysDim, TopoDim, Real> BaseType;

  HField_CG( const HField_CG& ) = delete;
  HField_CG& operator=( const HField_CG& ) = delete;

  explicit HField_CG( const XField<PhysDim, TopoDim>& xfld );

protected:
  using BaseType::xfld_;
  using BaseType::globalCellGroups_;

};

template<class PhysDim, class TopoDim>
struct HField_DG_Interface;

template<class PhysDim>
struct HField_DG_Interface<PhysDim, TopoD1>
{
  static Real getCellSize(const Field_DG_Cell<PhysDim, TopoD1, Real>& hfld_DG,
                          const int cellgroup, const int elem);
};

template<class PhysDim>
struct HField_DG_Interface<PhysDim, TopoD2>
{
  static Real getCellSize(const Field_DG_Cell<PhysDim, TopoD2, Real>& hfld_DG,
                          const int cellgroup, const int elem);
};

template<class PhysDim>
struct HField_DG_Interface<PhysDim, TopoD3>
{
  static Real getCellSize(const Field_DG_Cell<PhysDim, TopoD3, Real>& hfld_DG,
                          const int cellgroup, const int elem);
};

template<class Topology, class PhysDim, class TopoDim>
Real getCellSizeCellGroup(const Field_DG_Cell<PhysDim, TopoDim, Real>& hfld_DG, const int cellgroup, const int elem)
{
  typedef typename Field_DG_Cell<PhysDim, TopoDim, Real>::template FieldCellGroupType<Topology> FieldCellGroupType;
  typedef typename FieldCellGroupType::template ElementType<> ElementFieldClass;

  const FieldCellGroupType& hfld_cellgrp = hfld_DG.template getCellGroup<Topology>(cellgroup);

  SANS_ASSERT_MSG( hfld_cellgrp.basis()->order() == 0, "DG HField needs to be P0" );

  ElementFieldClass hfldElem( hfld_cellgrp.basis() );
  hfld_cellgrp.getElement(hfldElem, elem);

  return hfldElem.DOF(0);
}

} // namespace SANS

#endif // HFIELD_CG_H
