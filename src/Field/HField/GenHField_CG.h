// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef GENHFIELD_CG_H
#define GENHFIELD_CG_H

#include <vector>

#include "tools/SANSnumerics.h"     // Real

#include "Topology/ElementTopology.h"

#include "Field/FieldTypes.h"
#include "Field/tools/GroupFunctorType.h"

#include "Field/Local/XField_Local_Base.h"
#include "Field/Field_NodalView.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// HField representing the log of the generalized h-tensor with a CG field
//----------------------------------------------------------------------------//

template<class PhysDim, class TopoDim>
class GenHField_Elemental_impl;

template<class PhysDim, class TopoDim>
class GenHField_Elemental_Local_impl;

struct GenHFieldLocalNodeInfo;

template<class PhysDim, class TopoDim>
class GenHField_CG : public Field_CG_Cell<PhysDim, TopoDim, DLA::MatrixSymS<PhysDim::D,Real>>
{
public:
  typedef PhysDim PhysD;
  typedef TopoDim TopoD;

  static const int D = PhysDim::D;   // physical dimensions
  typedef DLA::MatrixSymS<D,Real> MatrixSym;

  typedef Field_CG_Cell<PhysDim, TopoDim, MatrixSym> BaseType;
  typedef std::vector<std::vector<MatrixSym>> ElementalMetricArrayType;

  typedef GenHField_CG<PhysDim, TopoDim> GenHClass;
  friend class GenHField_Elemental_impl<PhysDim, TopoDim>;
  friend class GenHField_Elemental_Local_impl<PhysDim, TopoDim>;

  GenHField_CG( const GenHField_CG& ) = delete;
  GenHField_CG& operator=( const GenHField_CG& ) = delete;

  explicit GenHField_CG( const XField<PhysDim, TopoDim>& xfld ); //generates HField for all cellgroups

  GenHField_CG( const XField<PhysDim, TopoDim>& xfld, const std::vector<int>& cellgroup_list );

  GenHField_CG( const XField_Local_Base<PhysDim, TopoDim>& xfld_local, const GenHClass& hfld_global );

protected:

  using BaseType::xfld_;
  using BaseType::globalCellGroups_;

  const Field_NodalView nodalview_; //nodal view of xfld

  //Metrics on each element
  ElementalMetricArrayType elementalMetrics_; //indexing: [global cellgrp][elem]
};


} // namespace SANS

#endif // GENHFIELD_CG_H
