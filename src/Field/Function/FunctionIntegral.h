// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FUNCTION_INTEGRAL_H
#define FUNCTION_INTEGRAL_H

// Cell integral to project exact solution with discontinuous elements

#include "Topology/ElementTopology.h"

#include "Field/Field.h"
#include "Field/XField.h"

#include "Field/tools/GroupFunctorType.h"

#include "Quadrature/Quadrature.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  Projects a solution function onto a discontinuous field
//
template<class SOLN>
class FunctionIntegral_impl :
    public GroupFunctorCellType< FunctionIntegral_impl<SOLN> >
{
public:
  typedef typename SOLN::PhysDim PhysDim;
  typedef typename SOLN::template ArrayQ<Real> ArrayQ;

  // Save off the boundary trace integrand and the residual vectors
  FunctionIntegral_impl( const SOLN& soln, ArrayQ& integral, const int comm_rank, const std::vector<int>& cellGroups ) :
    soln_(soln), integral_(integral), comm_rank_(comm_rank), cellGroups_(cellGroups) {}

  std::size_t nCellGroups() const          { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n];     }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology>
  void
  apply( const typename XField<PhysDim, typename Topology::TopoDim>::template FieldCellGroupType<Topology>& xfldCell,
         const int cellGroupGlobal )
  {
    typedef typename XField<PhysDim, typename Topology::TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename ElementXFieldClass::RefCoordType RefCoordType;

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );

    Real dJ;           // incremental Jacobian determinant
    Real weight;       // quadrature weight
    RefCoordType sRef; // reference-element coordinates

    Quadrature<typename Topology::TopoDim, Topology> quadrature(-1);

    // loop over elements within group
    const int nelem = xfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid DOFs to element
      xfldCell.getElement( xfldElem, elem );

      // Only integrate elements possessed by the current processor
      if (xfldElem.rank() != comm_rank_) continue;

      const int nquad = quadrature.nQuadrature();
      for (int iquad = 0; iquad < nquad; iquad++)
      {
        quadrature.weight( iquad, weight );
        quadrature.coordinates( iquad, sRef );

        dJ = weight * xfldElem.jacobianDeterminant( sRef );

        integral_ += dJ*soln_(xfldElem.eval(sRef));
      }
    }
  }

protected:
  const SOLN& soln_;
  ArrayQ& integral_;
  const int comm_rank_;
  const std::vector<int> cellGroups_;
};


// Factory function

template<class SOLN, class ArrayQ>
FunctionIntegral_impl<SOLN>
FunctionIntegral( const SOLN& soln, ArrayQ& integral, const int comm_rank, const std::vector<int>& cellGroups)
{
  return FunctionIntegral_impl<SOLN>(soln, integral, comm_rank, cellGroups);
}


} //namespace SANS

#endif  // FUNCTION_INTEGRAL_H
