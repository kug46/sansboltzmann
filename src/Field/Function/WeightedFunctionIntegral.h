// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef WEIGHTED_FUNCTION_INTEGRAL_H
#define WEIGHTED_FUNCTION_INTEGRAL_H

// Cell integral to project exact solution with discontinuous elements

#include "Topology/ElementTopology.h"

#include "Field/Field.h"
#include "Field/XField.h"

#include "Field/tools/GroupFunctorType.h"

#include "Quadrature/Quadrature.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  Projects a solution function onto a discontinuous field
//
template<class WeightingFunction, class SOLN>
class WeightedFunctionIntegral_impl :
    public GroupFunctorCellType< WeightedFunctionIntegral_impl<WeightingFunction,SOLN> >
{
public:
  typedef typename SOLN::PhysDim PhysDim;
  typedef typename SOLN::template ArrayQ<Real> ArrayQ;

  // Save off the boundary trace integrand and the residual vectors
  WeightedFunctionIntegral_impl( const WeightingFunction& weightFcn, const SOLN& soln, ArrayQ& integral,
                                 const std::vector<int>& cellGroups ) :
    weightFcn_(weightFcn), soln_(soln), integral_(integral), cellGroups_(cellGroups) {}

  std::size_t nCellGroups() const          { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n];     }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology>
  void
  apply( const typename XField<PhysDim, typename Topology::TopoDim>::template FieldCellGroupType<Topology>& xfldCell,
         const int cellGroupGlobal )
  {
    typedef typename XField<PhysDim, typename Topology::TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename ElementXFieldClass::VectorX VectorX;
    typedef typename ElementXFieldClass::RefCoordType RefCoordType;

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );

    Real dJ;           // incremental Jacobian determinant
    Real weight;       // quadrature weight
    RefCoordType sRef; // reference-element coordinates

    Quadrature<typename Topology::TopoDim, Topology> quadrature(-1);

    // loop over elements within group
    const int nelem = xfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid DOFs to element
      xfldCell.getElement( xfldElem, elem );

      const int nquad = quadrature.nQuadrature();
      for (int iquad = 0; iquad < nquad; iquad++)
      {
        quadrature.weight( iquad, weight );
        quadrature.coordinates( iquad, sRef );

        dJ = weight * xfldElem.jacobianDeterminant( sRef );

        VectorX X = xfldElem.eval(sRef);

        integral_ += dJ*weightFcn_(X)*soln_(X);
      }
    }
  }

protected:
  const WeightingFunction& weightFcn_;
  const SOLN& soln_;
  ArrayQ& integral_;
  const std::vector<int> cellGroups_;
};


// Factory function

template<class WeightingFunction, class SOLN, class ArrayQ>
WeightedFunctionIntegral_impl<WeightingFunction, SOLN>
WeightedFunctionIntegral( const WeightingFunction& weightFcn, const SOLN& soln, ArrayQ& integral,
                          const std::vector<int>& cellGroups)
{
  return WeightedFunctionIntegral_impl<WeightingFunction, SOLN>(weightFcn, soln, integral, cellGroups);
}


} //namespace SANS

#endif  // WEIGHTED_FUNCTION_INTEGRAL_H
