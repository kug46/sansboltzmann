// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDLIFTVOLUME_DG_CELL_H
#define FIELDLIFTVOLUME_DG_CELL_H

#include <vector>

#include "FieldGroupArea_Traits.h"
#include "FieldLiftGroupVolume_Traits.h"
#include "FieldLift_DG_CellBase.h"

#include "Topology/Dimension.h"
#include "BasisFunction/BasisFunctionCategory.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Topologically 3D lifting operator field variables
//----------------------------------------------------------------------------//

template<class Tin>
struct FieldLiftTraits<TopoD3, Tin>
{
  typedef TopoD3 TopoDim;
  typedef Tin T;                                     // DOF type

  typedef typename FieldGroupAreaTraitsBase<T>::FieldBase FieldTraceGroupBase;
  typedef typename FieldLiftGroupVolumeTraitsBase<T>::FieldBase FieldCellGroupBase;

  template<class Topology>
  using FieldTraceGroupType = FieldAssociativity< FieldGroupAreaTraits<Topology, T> >;

  template<class Topology>
  using FieldCellGroupType = FieldLiftAssociativity< FieldLiftGroupVolumeTraits<Topology, T> >;
};


//----------------------------------------------------------------------------//
// 3-D solution field: DG cell-field
//----------------------------------------------------------------------------//

template <class PhysDim, class T>
class FieldLift_DG_Cell<PhysDim, TopoD3, T> : public FieldLift_DG_CellBase< PhysDim, TopoD3, T >
{
public:
  typedef FieldLift_DG_CellBase< PhysDim, TopoD3, T > BaseType;
  typedef T ArrayQ;

  FieldLift_DG_Cell( const XField<PhysDim, TopoD3>& xfld, const int order, const BasisFunctionCategory& category );

  FieldLift_DG_Cell( const XField<PhysDim, TopoD3>& xfld, const int order, const BasisFunctionCategory& category,
                     const std::vector<int>& CellGroups );

  FieldLift_DG_Cell( const FieldLift_DG_Cell& fld, const FieldCopy& tag );

  FieldLift_DG_Cell& operator=( const ArrayQ& q );

protected:
  void init( const int order, const BasisFunctionCategory& category, const std::vector<int>& CellGroups );

  using BaseType::nDOF_;
  using BaseType::DOF_;
  using BaseType::cellGroups_;
  using BaseType::xfld_;
  using BaseType::nElem_;
};

}

#endif  // FIELDLIFTVOLUME_DG_CELL_H
