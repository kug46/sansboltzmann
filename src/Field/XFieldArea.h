// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELDAREA_H
#define XFIELDAREA_H

#include "XField.h"
#include "XFieldLine_Traits.h"
#include "XFieldArea_Traits.h"
#include "FieldAssociativity.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Topologically 2D grid
//----------------------------------------------------------------------------//

template<class PhysDim>
struct XFieldTraits<PhysDim, TopoD2>
{
  typedef TopoD2 TopoDim;
  static const int D = PhysDim::D;                 // physical dimensions

  typedef DLA::VectorS<D,Real> VectorX;   // coordinates vector
  typedef VectorX T;

  typedef typename XFieldGroupLineTraits<PhysDim, TopoD2>::FieldBase FieldTraceGroupBase;
  typedef typename XFieldAreaTraits<PhysDim, TopoD2>::FieldBase FieldCellGroupBase;

  template<class Topology>
  using FieldTraceGroupType = ElementConnectivity< XFieldGroupLineTraits<PhysDim, TopoD2> >;

  template<class Topology>
  using FieldCellGroupType = FieldAssociativity< XFieldGroupAreaTraits<PhysDim, TopoD2, Topology> >;
};

} //namespace SANS

#endif //XFIELDAREA_H
