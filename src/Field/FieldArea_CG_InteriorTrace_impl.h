// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(FIELDAREA_CG_INTERIORTRACE_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "FieldArea_CG_InteriorTrace.h"

#include "BasisFunction/BasisFunctionCategory.h"

#include "Field_CG/Field_CG_TraceConstructor.h"

#include "tools/split_cat_std_vector.h"

namespace SANS
{

//===========================================================================//
template <class PhysDim, class T>
Field_CG_InteriorTrace<PhysDim, TopoD2, T>::Field_CG_InteriorTrace( const XField<PhysDim, TopoD2>& xfld, const int order,
                                                                    const BasisFunctionCategory category ) : BaseType( xfld )
{
  // By default all boundary groups are used to construct lagrange multipliers
  init(order, category, BaseType::createInteriorGroupIndex() );
}

template <class PhysDim, class T>
Field_CG_InteriorTrace<PhysDim, TopoD2, T>::Field_CG_InteriorTrace( const XField<PhysDim, TopoD2>& xfld,
                                                                    const int order, const BasisFunctionCategory category,
                                                                    const std::vector<int>& InteriorGroups ) : BaseType( xfld )
{
  // Check that the groups asked for are within the range of available groups
  BaseType::checkInteriorGroupIndex( InteriorGroups );
  init(order, category, InteriorGroups);
}

template <class PhysDim, class T>
Field_CG_InteriorTrace<PhysDim, TopoD2, T>::Field_CG_InteriorTrace( const Field_CG_InteriorTrace& fld, const FieldCopy&tag ) : BaseType(fld, tag) {}

template <class PhysDim, class T>
Field_CG_InteriorTrace<PhysDim, TopoD2, T>&
Field_CG_InteriorTrace<PhysDim, TopoD2, T>::operator=( const ArrayQ& q ) { BaseType::operator=(q); return *this; }

//----------------------------------------------------------------------------//
template <class PhysDim, class T>
void
Field_CG_InteriorTrace<PhysDim, TopoD2, T>::init( const int order, const BasisFunctionCategory& category,
                                                  const std::vector<int>& InteriorGroups )
{
  SANS_ASSERT_MSG( order >= 1, "CG for interior edge requires order >= 1" );
  SANS_ASSERT_MSG( category == BasisFunctionCategory_Hierarchical ||
                   category == BasisFunctionCategory_Lagrange,
                   "CG area must use Hierarchical or Lagrange Basis" );

  // convert to a set of interior trace groups for now (and empty boundary trace)
  std::vector<std::vector<int>> interiorGroupSets = {InteriorGroups};
  std::vector<std::vector<int>> boundaryGroupSets = {{}};

  Field_CG_TraceConstructor<PhysDim, TopoD2> fldConstructor(xfld_, order, category, interiorGroupSets, boundaryGroupSets);

  nElem_ = fldConstructor.nElem();

  // allocate the solution DOF array
  this->resizeDOF(fldConstructor.nDOF());

  // set solution DOF associativity for each interior group

  std::vector<int> ConcatInteriorGroups = cat(interiorGroupSets);

  this->resizeInteriorTraceGroups( ConcatInteriorGroups );

  for (int igroup = 0; igroup < this->nInteriorTraceGroups(); igroup++)
  {
    const int group = ConcatInteriorGroups[igroup];
    int localGroup = localInteriorTraceGroups_.at(group);

    if ( xfld_.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Line) )
    {
      typedef typename BaseType::template FieldTraceGroupType<Line> FieldTraceGroupClass;

      // allocate group
      interiorTraceGroups_[localGroup] = fldConstructor.template createInteriorTraceGroup<FieldTraceGroupClass>( group, this->local2nativeDOFmap_ );
      interiorTraceGroups_[localGroup]->setDOF( DOF_, nDOF_ );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "Unknown area element topology" );
  }

  // get the ghost/zombie DOF ranks
  // this must be last as it uses this->local2nativeDOFmap_
  this->nDOFpossessed_ = fldConstructor.nDOFpossessed();
  this->nDOFghost_     = fldConstructor.nDOFghost();

  this->resizeDOF_rank(fldConstructor.nDOF(), fldConstructor.nDOFpossessed());

  fldConstructor.getDOF_rank(this->DOFghost_rank_);
}

}
