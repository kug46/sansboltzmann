// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD_H
#define XFIELD_H

#include <vector>
#include <map>
#include <memory> //shared_ptr

#include "BasisFunction/TraceToCellRefCoord.h"

#include "MPI/communicator_fwd.h"

#include "FieldBase.h"
#include "FieldAssociativityConstructor.h"
#include "FieldAssociativity.h"

namespace SANS
{

// forward declaration of LagrangeElementGroup
// used for constructing cell/trace elements (defined in XField_Lagrange.h)
class LagrangeElementGroup;
class LagrangeConnectedGroup;
struct PeriodicBCNodeMap;
typedef std::shared_ptr<LagrangeElementGroup> LagrangeElementGroup_ptr;
template<class PhysDim>
class XField_Lagrange;

enum XFieldBalance
{
  Cell,
  CellPartitionCopy,
  CellPartitionRead,
  Serial
};

//=============================================================================
//Exception for XField errors
struct XFieldException : public SANSException
{
  // cppcheck-suppress noExplicitConstructor
  XFieldException( const std::string& message );
  XFieldException( const std::string& assertion, const std::string& message );
  XFieldException( const std::string& assertion, const char *fmt, ...);
  XFieldException( const XFieldException& e ) : SANSException(e) {}

  virtual ~XFieldException() noexcept;
};

#define XFIELD_CHECK( assertion, fmt... ) \
  if ( unlikely(!(assertion)) ) \
    BOOST_THROW_EXCEPTION( XFieldException( BOOST_PP_STRINGIZE( assertion ), fmt ) )

#define XFIELD_EXCEPTION( message ) \
  BOOST_THROW_EXCEPTION( XFieldException( message ) )


// Classes "private" to XField for dispatching topologies
// They need to be separated to generate partial specializations
namespace XField_
{
  //-----------------------------------------------------------------------------
  // Templated class for checkTraceNormal of line grids: specializations are made depending on whether PhysDim equals TopoDim
  template<class PhysDim, class TopoDim>
  struct checkTraceNormalStruct
  {
    typedef XField<PhysDim,TopoDim> XFieldType;

    template<class TopologyTrace, class TopologyL, class TopologyR>
    static void checkTraceNormal( const XFieldType& xfld,
                                  const typename XFieldType::template FieldTraceGroupType<TopologyTrace>& traceGroup, int elem,
                                  typename XFieldType::template FieldTraceGroupType<TopologyTrace>::template ElementType<>& elemTrace,
                                  const typename XFieldType::template FieldCellGroupType<TopologyL>::template ElementType<>& elemCellL,
                                  const typename XFieldType::template FieldCellGroupType<TopologyR>::template ElementType<>& elemCellR);
  };

  template<class PhysDim, class TopoDim, class TopologyTrace, class TopologyL>
  struct check_RightTopology
  {
    typedef XField<PhysDim, TopoDim> XFieldType;
    static void interiorTrace( const XFieldType& xfld, const int itraceGroup,
                               const typename XFieldType::template FieldTraceGroupType<TopologyTrace>& traceGroup,
                               std::vector< std::vector<int> >& cellCount );

    static void boundaryTrace( const XFieldType& xfld, const int itraceGroup,
                               const typename XFieldType::template FieldTraceGroupType<TopologyTrace>& traceGroup,
                               std::vector< std::vector<int> >& cellCount );
  };

  template<class PhysDim, class TopoDim, class TopologyTrace>
  struct check_LeftTopology
  {
    typedef XField<PhysDim, TopoDim> XFieldType;
    static void interiorTrace( const XFieldType& xfld, const int itraceGroup,
                               const typename XFieldType::template FieldTraceGroupType<TopologyTrace>& traceGroup,
                               std::vector< std::vector<int> >& cellCount );

    static void boundaryTrace( const XFieldType& xfld, const int itraceGroup,
                               const typename XFieldType::template FieldTraceGroupType<TopologyTrace>& traceGroup,
                               std::vector< std::vector<int> >& cellCount );
  };
} //namespace XField_


//----------------------------------------------------------------------------//
// XField represent a grid in any physical/topological dimension
//----------------------------------------------------------------------------//

template<class PhysDim_, class TopoDim_>
class XField : public FieldBase< XFieldTraits<PhysDim_, TopoDim_> >,
  public FieldType< XField<PhysDim_, TopoDim_> >
{
public:
  typedef PhysDim_ PhysDim;
  typedef TopoDim_ TopoDim;
  typedef FieldBase< XFieldTraits<PhysDim, TopoDim> > BaseType;
  typedef XField<PhysDim, TopoDim> XFieldType;
  static const int D = PhysDim::D;                         // physical dimensions

  typedef typename BaseType::FieldTraceGroupBase FieldTraceGroupBase;
  typedef typename BaseType::FieldCellGroupBase FieldCellGroupBase;

  template<class Topology>
  using FieldTraceGroupType = typename BaseType::template FieldTraceGroupType<Topology>;

  template<class Topology>
  using FieldCellGroupType = typename BaseType::template FieldCellGroupType<Topology>;

  typedef typename XFieldTraits<PhysDim, TopoDim>::T VectorX;   // coordinates vector

  XField( const XField& ) = delete;
  XField& operator=( const XField& ) = delete;

  XField();
  explicit XField( mpi::communicator& comm );
  explicit XField( std::shared_ptr<mpi::communicator> comm );
  XField( const XField& fld, const FieldCopy& tag );
  XField( const XField& xfld, XFieldBalance graph );
  XField( const XField& xfld, const int order,
          const BasisFunctionCategory category = BasisFunctionCategory_None );  //Constructor for creating higher order grids
  virtual ~XField() {}

  virtual const std::type_info& derivedTypeID() const { return typeid(XField); }

  void cloneFrom( const XField& fld );

  // This is needed for FieldTuple
  const XField& getXField() const { return *this; }

  // The XField needs these "Global" accessors to be consistent with Fields in Tuples
        FieldTraceGroupBase& getHubTraceGroupBaseGlobal( const int group )       { return BaseType::getHubTraceGroupBase(group); }
  const FieldTraceGroupBase& getHubTraceGroupBaseGlobal( const int group ) const { return BaseType::getHubTraceGroupBase(group); }

        FieldTraceGroupBase& getInteriorTraceGroupBaseGlobal( const int group )       { return BaseType::getInteriorTraceGroupBase(group); }
  const FieldTraceGroupBase& getInteriorTraceGroupBaseGlobal( const int group ) const { return BaseType::getInteriorTraceGroupBase(group); }

        FieldTraceGroupBase& getBoundaryTraceGroupBaseGlobal( const int group )       { return BaseType::getBoundaryTraceGroupBase(group); }
  const FieldTraceGroupBase& getBoundaryTraceGroupBaseGlobal( const int group ) const { return BaseType::getBoundaryTraceGroupBase(group); }

        FieldCellGroupBase& getCellGroupBaseGlobal( const int group )       { return BaseType::getCellGroupBase(group); }
  const FieldCellGroupBase& getCellGroupBaseGlobal( const int group ) const { return BaseType::getCellGroupBase(group); }


  template<class Topology>
        FieldTraceGroupType<Topology>& getHubTraceGroupGlobal( const int group )
  { return BaseType::template getHubTraceGroup<Topology>(group); }
  template<class Topology>
  const FieldTraceGroupType<Topology>& getHubTraceGroupGlobal( const int group ) const
  { return BaseType::template getHubTraceGroup<Topology>(group); }

  template<class Topology>
        FieldTraceGroupType<Topology>& getInteriorTraceGroupGlobal( const int group )
  { return BaseType::template getInteriorTraceGroup<Topology>(group); }
  template<class Topology>
  const FieldTraceGroupType<Topology>& getInteriorTraceGroupGlobal( const int group ) const
  { return BaseType::template getInteriorTraceGroup<Topology>(group); }

  template<class Topology>
        FieldTraceGroupType<Topology>& getBoundaryTraceGroupGlobal( const int group )
  { return BaseType::template getBoundaryTraceGroup<Topology>(group); }
  template<class Topology>
  const FieldTraceGroupType<Topology>& getBoundaryTraceGroupGlobal( const int group ) const
  { return BaseType::template getBoundaryTraceGroup<Topology>(group); }

  template<class Topology>
        FieldCellGroupType<Topology>& getCellGroupGlobal( const int group )
  { return BaseType::template getCellGroup<Topology>(group); }
  template<class Topology>
  const FieldCellGroupType<Topology>& getCellGroupGlobal( const int group ) const
  { return BaseType::template getCellGroup<Topology>(group); }

  virtual int nDOFCellGroup(int cellgroup) const override
  {
    SANS_DEVELOPER_EXCEPTION("XField::nDOFCellGroup - Not implemented yet.");
    return -1;
  }

  virtual int nDOFInteriorTraceGroup(int tracegroup) const override
  {
    SANS_DEVELOPER_EXCEPTION("XField::nDOFInteriorTraceGroup - Not implemented yet.");
    return -1;
  }

  virtual int nDOFBoundaryTraceGroup(int tracegroup) const override
  {
    SANS_DEVELOPER_EXCEPTION("XField::nDOFBoundaryTraceGroup - Not implemented yet.");
    return -1;
  }

  int nGhostBoundaryTraceGroups() const { return ghostBoundaryTraceGroups_.size(); }

  const FieldTraceGroupBase& getGhostBoundaryTraceGroupBase( const int group ) const;

  template<class Topology>
  const FieldTraceGroupType<Topology>& getGhostBoundaryTraceGroup( const int group ) const;

  // Returns the set of interior/periodic trace groups with the list of cell groups on either side.
  // Note that multiple interior/periodic trace groups may be bounded by a single cell group
  std::vector<int> getInteriorTraceGroupsGlobal( const std::vector<int>& cellGroupsGlobal ) const;
  std::vector<int> getPeriodicTraceGroupsGlobal( const std::vector<int>& cellGroupsGlobal ) const;

  // total number of DOF in a serial representation of the XField
  int nDOFnative() const;

  // communicator accessor for the XField
  std::shared_ptr<mpi::communicator> comm() const;

  // a list of unique native element IDs
  const std::vector<int>& cellIDs(const int group)          const { return cellIDs_[group];          }
  const std::vector<int>& boundaryTraceIDs(const int group) const { return boundaryTraceIDs_[group]; }

  const std::vector<std::vector<int>>& cellIDs()          const { return cellIDs_;          }
  const std::vector<std::vector<int>>& boundaryTraceIDs() const { return boundaryTraceIDs_; }

  // Used to check that the grid is correct
  void checkGrid();
  bool assertGrid()
  {
    try
    {
      checkGrid();
    }
    catch (...)
    {
      return false;
    }
    return true;
  }

protected:
  //-----------------------------------------------------------------------------
  // Friend struct

  template<class , class >
  friend struct XField_::checkTraceNormalStruct;

  template<class , class , class , class >
  friend struct XField_::check_RightTopology;

  template<class , class , class >
  friend struct XField_::check_LeftTopology;

  //-----------------------------------------------------------------------------
  // Methods
  void deallocate();

  template<class Topology>
  void checkPositiveJacobianDeterminant( const typename BaseType::template FieldCellGroupType<Topology>& cellGroup  ) const;

  //Dispatch functions for checking the interior trace
  template<class TopologyTrace, class TopologyL, class TopologyR>
  void checkInteriorTrace( const int itraceGroup,
                           const typename BaseType::template FieldTraceGroupType<TopologyTrace>& traceGroup,
                           std::vector< std::vector<int> >& cellCount ) const;

  //Dispatch functions for checking the boundary trace
  template<class TopologyTrace, class TopologyL>
  void checkBoundaryTrace( const int itraceGroup,
                           const typename BaseType::template FieldTraceGroupType<TopologyTrace>& traceGroup,
                           std::vector< std::vector<int> >& cellCount ) const;

  // Used when an boundary trace is connected to a right element, i.e. full potential wake and periodic boundaries
  template<class TopologyTrace, class TopologyL, class TopologyR>
  void checkBoundaryTraceConnected( const int itraceGroup,
                                    const typename BaseType::template FieldTraceGroupType<TopologyTrace>& traceGroup,
                                    std::vector< std::vector<int> >& cellCount ) const;

  template<class TopologyTrace, class TopologyL>
  void checkTraceNormal( const typename BaseType::template FieldTraceGroupType<TopologyTrace>& traceGroup, int elem,
                         ElementXField<PhysDim, typename TopologyTrace::TopoDim, TopologyTrace>& elemTrace,
                         const ElementXField<PhysDim, TopoDim, TopologyL>& elemCellL ) const;

  template<class TopologyTrace, class TopologyL>
  void checkTraceNormalL( const typename BaseType::template FieldTraceGroupType<TopologyTrace>& traceGroup, int elem,
                          ElementXField<PhysDim, typename TopologyTrace::TopoDim, TopologyTrace>& elemTrace,
                          const ElementXField<PhysDim, TopoDim, TopologyL>& elemCellL ) const;

  template<class TopologyTrace, class TopologyR>
  void checkTraceNormalR( const typename BaseType::template FieldTraceGroupType<TopologyTrace>& traceGroup, int elem,
                          ElementXField<PhysDim, typename TopologyTrace::TopoDim, TopologyTrace>& elemTrace,
                          const ElementXField<PhysDim, TopoDim, TopologyR>& elemCellR ) const;

  void buildFrom(const XField& xfld, const int order,
                 BasisFunctionCategory category = BasisFunctionCategory_None );


  //-------------------------------------------------------------------------//
  // LagrangeElementGroup functions
  // build an XField from XField_Lagrange
  void buildFrom( XField_Lagrange<PhysDim>& xfldin );

  // add a specific-type cell group
  template<typename Topology>
  void addCellGroup( const int group, const std::map<int,int>& global2localDOFmap,
                     const LagrangeElementGroup_ptr& cellGroup,
                     const std::vector<std::vector<int>>& orientations );

  // add specific-type boundary group
  template<class TopologyTrace>
  void addBoundaryGroup( const int group, const std::map<int,int>& global2localDOFmap,
                         const LagrangeConnectedGroup& traceGroup,
                         const std::vector<LagrangeElementGroup_ptr>& cellGroups,
                         PointerArray<FieldTraceGroupBase>& boundaryTraceGroups,
                         const std::vector<PeriodicBCNodeMap>& periodicity,
                         bool BCpossessed,
                         std::vector<std::vector<std::vector<int>>>& orientations );

  // add a specific-type interior group
  template<class TopologyTrace>
  void addInteriorGroup( const int group, const std::map<int,int>& global2localDOFmap,
                         const LagrangeConnectedGroup& traceGroup,
                         const std::vector<LagrangeElementGroup_ptr>& cellGroups,
                         std::vector<std::vector<std::vector<int>>>& orientations );

  // generic dispatch functions for adding cell, boundary and interior groups
  void addCellGroupType( int group_id, const std::map<int,int>& global2localDOFmap,
                         const LagrangeElementGroup_ptr& cellGroup,
                         const std::vector<std::vector<int>>& orientations );
  void addBoundaryGroupType( int group_id, const std::map<int,int>& global2localDOFmap,
                             const LagrangeConnectedGroup &traceGroup,
                             const std::vector<LagrangeElementGroup_ptr>& cellGroups,
                             PointerArray<FieldTraceGroupBase>& boundaryTraceGroups,
                             const std::vector<PeriodicBCNodeMap>& periodicity,
                             bool BCpossessed,
                             std::vector<std::vector<std::vector<int>>>& orientations );
  void addInteriorGroupType( int group_id, const std::map<int,int>& global2localDOFmap,
                             const LagrangeConnectedGroup &traceGroup,
                             const std::vector<LagrangeElementGroup_ptr>& cellGroups,
                             std::vector<std::vector<std::vector<int>>>& orientations );

  //-------------------------------------------------------------------------//
  // Functions to help construct the XField

  void createCellGroup( const int group_id , FieldCellGroupBase *grp )
  { this->cellGroups_[group_id] = grp; this->cellGroups_[group_id]->setDOF(this->DOF_ , this->nDOF_);  }

  void createBoundaryTraceGroup( const int group_id , FieldTraceGroupBase *grp )
  { this->boundaryTraceGroups_[group_id] = grp; this->boundaryTraceGroups_[group_id]->setDOF(this->DOF_ , this->nDOF_);  }

  void createInteriorTraceGroup( const int group_id , FieldTraceGroupBase *grp )
  { this->interiorTraceGroups_[group_id] = grp; this->interiorTraceGroups_[group_id]->setDOF(this->DOF_ , this->nDOF_);  }

  void resizeGhostBoundaryTraceGroups( const int nGhostBoundaryTraceGroups );

  PointerArray<FieldTraceGroupBase> ghostBoundaryTraceGroups_; // groups of ghost boundary-trace elements for checkGrid

  std::shared_ptr<mpi::communicator> comm_;

  void initDefaultElmentIDs();

  std::vector<std::vector<int>> cellIDs_;
  std::vector<std::vector<int>> boundaryTraceIDs_;
};  // class XField


template <int k, class PhysDim, class TopoDim>
const XField<PhysDim, TopoDim>&
get(const XField<PhysDim, TopoDim>& xfld)
{
  static_assert( k == 0 || k == -1, "k should be zero or -1 if the argument to get is XField");
  return xfld;
}

} // namespace SANS

#endif // XFIELD_H
