// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define FIELDBASE_INSTANTIATE
#include "FieldBase_impl.h"

#define FIELDSUBGROUP_INSTANTIATE
#include "FieldSubGroup_impl.h"

#define FIELDLIFT_DG_CELLBASE_INSTANTIATE
#include "FieldLift_DG_CellBase_impl.h"

#define FIELDLIFTAREA_DG_CELL_INSTANTIATE
#include "FieldLiftArea_DG_Cell_impl.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "XFieldArea.h"

namespace SANS
{

// Base class instantiation
template class FieldBase< FieldLiftTraits<TopoD2, Real > >;
template class FieldBase< FieldLiftTraits<TopoD2, DLA::VectorS<3, Real> > >;

// FieldSubGroup instantiation
template class FieldSubGroup<PhysD3, FieldLiftTraits<TopoD2, Real > >;
template class FieldSubGroup<PhysD3, FieldLiftTraits<TopoD2, DLA::VectorS<3, Real> > >;

// Field Lift instantiation
template class FieldLift_DG_Cell< PhysD3, TopoD2, Real >;
template class FieldLift_DG_Cell< PhysD3, TopoD2, DLA::VectorS<3, Real> >;

}
