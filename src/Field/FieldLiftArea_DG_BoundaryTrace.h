// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDLIFTAREA_DG_BOUNDARYTRACE_H
#define FIELDLIFTAREA_DG_BOUNDARYTRACE_H

#include "FieldLift_DG_BoundaryTraceBase.h"
#include "FieldArea.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// 2D boundary lifting operator field: DG boundary trace with cell basis functions
//----------------------------------------------------------------------------//

template <class PhysDim, class T>
class FieldLift_DG_BoundaryTrace< PhysDim, TopoD2, T > : public FieldLift_DG_BoundaryTraceBase< PhysDim, TopoD2, T >
{
public:
  typedef FieldLift_DG_BoundaryTraceBase< PhysDim, TopoD2, T > BaseType;
  typedef T ArrayQ;

  FieldLift_DG_BoundaryTrace( const XField<PhysDim, TopoD2>& xfld, const int order, const BasisFunctionCategory& category );

  FieldLift_DG_BoundaryTrace( const XField<PhysDim, TopoD2>& xfld, const int order,
                              const BasisFunctionCategory& category, const std::vector<int>& BoundaryGroups );

  // Groups are first to remove ambiguity with list initializers
  // DG Fields are already broken, can just cat the CellGroupSets and forward
  explicit FieldLift_DG_BoundaryTrace( const std::vector<std::vector<int>>& BoundaryGroupSets,
                                       const XField<PhysDim, TopoD2>& xfld, const int order, const BasisFunctionCategory& category )
  : FieldLift_DG_BoundaryTrace( xfld, order, category, SANS::cat(BoundaryGroupSets)) {}

  FieldLift_DG_BoundaryTrace( const FieldLift_DG_BoundaryTrace& fld, const FieldCopy& tag );

  FieldLift_DG_BoundaryTrace& operator=( const ArrayQ& q );

protected:
  using BaseType::nDOF_;
  using BaseType::DOF_;
  using BaseType::cellGroups_;
  using BaseType::boundaryTraceGroups_;

  void init(const XField<PhysDim, TopoD2>& xfld, const int order,
            const BasisFunctionCategory& category, const std::vector<int>& BoundaryGroups );
};

}

#endif  // FIELD_DG_BOUNDARYTRACE_H
