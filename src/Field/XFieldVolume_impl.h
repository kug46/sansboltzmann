// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(XFIELDVOLUME_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "XField.h"
#include "Partition/XField_Lagrange.h"

// This file contains XField functions for volume specializations

namespace SANS
{

template<class PhysDim,class TopoDim>
void
XField<PhysDim,TopoDim>::
addCellGroupType( int group_id, const std::map<int,int>& global2localDOFmap,
                  const LagrangeElementGroup_ptr& cellGroup,
                  const std::vector<std::vector<int>>& orientations )
{
  if (cellGroup->topo == eTet)
  {
    addCellGroup<Tet>(group_id, global2localDOFmap, cellGroup, orientations);
  }
  else if (cellGroup->topo == eHex)
  {
    addCellGroup<Hex>(group_id, global2localDOFmap, cellGroup, orientations);
  }
  else
    SANS_DEVELOPER_EXCEPTION("Unknown cell topology = %d", cellGroup->topo);

}

template<class PhysDim,class TopoDim>
void
XField<PhysDim,TopoDim>::
addBoundaryGroupType( int group_id, const std::map<int,int>& global2localDOFmap,
                      const LagrangeConnectedGroup& traceGroup,
                      const std::vector<LagrangeElementGroup_ptr>& cellGroups,
                      PointerArray<FieldTraceGroupBase>& boundaryTraceGroups,
                      const std::vector<PeriodicBCNodeMap>& periodicity,
                      bool BCpossessed,
                      std::vector<std::vector<std::vector<int>>>& orientations )
{
  if (traceGroup.topo == eTriangle)
  {
    addBoundaryGroup<Triangle>(group_id, global2localDOFmap, traceGroup, cellGroups,
                               boundaryTraceGroups, periodicity, BCpossessed, orientations);
  }
  else if (traceGroup.topo == eQuad)
  {
    addBoundaryGroup<Quad>(group_id, global2localDOFmap, traceGroup, cellGroups,
                           boundaryTraceGroups, periodicity, BCpossessed, orientations);
  }
  else
    SANS_DEVELOPER_EXCEPTION("Unknown trace topology = %d", traceGroup.topo);
}

template<class PhysDim,class TopoDim>
void
XField<PhysDim,TopoDim>::
addInteriorGroupType( int group_id, const std::map<int,int>& global2localDOFmap,
                      const LagrangeConnectedGroup& traceGroup,
                      const std::vector<LagrangeElementGroup_ptr>& cellGroups,
                      std::vector<std::vector<std::vector<int>>>& orientations )
{
  if (traceGroup.topo == eTriangle)
  {
    addInteriorGroup<Triangle>(group_id, global2localDOFmap, traceGroup, cellGroups, orientations);
  }
  else if (traceGroup.topo == eQuad)
  {
    addInteriorGroup<Quad>(group_id, global2localDOFmap, traceGroup, cellGroups, orientations);
  }
  else
    SANS_DEVELOPER_EXCEPTION("Unknown trace topology = %d", traceGroup.topo);
}

}
