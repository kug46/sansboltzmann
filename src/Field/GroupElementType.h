// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef GROUPELEMENTTYPE_H
#define GROUPELEMENTTYPE_H

namespace SANS
{

// Indicates if a group in a field is a cell group or hubtrace group
enum GroupElmentType
{
  eCellGroup,
  eHubTraceGroup
};

}

#endif //GROUPELEMENTTYPE_H
