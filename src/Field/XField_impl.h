// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(XFIELD_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include <sstream>
#include <algorithm> // std::transform
#include <iomanip>   // std::setprecision

#include "XField.h"
#include "GroupElementType.h"
#include "Partition/XField_Lagrange.h"
#include "Quadrature/Quadrature.h"

#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionBase.h"

#include "ElementConnectivityConstructor.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#endif

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Constructors
//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
XField<PhysDim,TopoDim>::XField()
{
  // default to a communicator local to each processor
  // this is a HACK while implementing MPI, not clear if it should stay...
  mpi::communicator world;
  comm_ = std::make_shared<mpi::communicator>( world.split(world.rank()) );
}

template<class PhysDim, class TopoDim>
XField<PhysDim,TopoDim>::XField(mpi::communicator& comm)
  : comm_( std::make_shared<mpi::communicator>( comm ) )
{}

template<class PhysDim, class TopoDim>
XField<PhysDim,TopoDim>::XField(std::shared_ptr<mpi::communicator> comm)
  : comm_( comm )
{}

template<class PhysDim, class TopoDim>
XField<PhysDim,TopoDim>::XField( const XField& xfld, const FieldCopy& tag ) : BaseType(), comm_(xfld.comm())
{
  cloneFrom(xfld);
}

//Constructor for creating higher order grids
template<class PhysDim, class TopoDim>
XField<PhysDim,TopoDim>::XField( const XField& xfld, const int order,
                                 const BasisFunctionCategory category ) : comm_(xfld.comm())
{
  buildFrom(xfld, order, category);
  this->checkGrid(); //Check that the grid is correct
}


//Construct a new grid with a different partitioning scheme
template<class PhysDim, class TopoDim>
XField<PhysDim,TopoDim>::XField( const XField& xfld, XFieldBalance graph ) : comm_(xfld.comm())
{
  XField_Lagrange<PhysDim> xfldin(xfld, graph);
  buildFrom(xfldin);

  // split the communicator into two groups if serializing
  // first group is just rank0, second group is all remaining processors
  if (graph == XFieldBalance::Serial)
  {
    const int comm_rank = xfldin.comm()->rank();
    mpi::communicator comm = xfldin.comm()->split( comm_rank == 0 );
    comm_ = std::make_shared<mpi::communicator>( comm );
  }
}

//----------------------------------------------------------------------------//
// Implements general protected XField functions
//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
void
XField<PhysDim,TopoDim>::deallocate()
{
  // deallcaote the base
  BaseType::deallocate();

  resizeGhostBoundaryTraceGroups( 0 ); // free ghost memory
}

//----------------------------------------------------------------------------//
// total number of DOF in a serial representation of the XField
template<class PhysDim, class TopoDim>
int
XField<PhysDim,TopoDim>::nDOFnative() const
{
#ifdef SANS_MPI
  int nDOF = 0;

  for (int i = 0; i < this->nDOF(); i++)
    nDOF = max(this->local2nativeDOFmap_[i], nDOF);

  nDOF++; // change to a size rather than an index

  return boost::mpi::all_reduce(*this->comm(), nDOF, boost::mpi::maximum<int>());
#else
  return this->nDOF_;
#endif
}

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
void
XField<PhysDim,TopoDim>::cloneFrom( const XField& xfld )
{
  BaseType::cloneFrom(xfld);

  //BaseType does not have GhostBoundaryTraceGroups, so clone them here
  if (this != &xfld)
  {
    // copy over the IDs
    cellIDs_ = xfld.cellIDs_;
    boundaryTraceIDs_ = xfld.boundaryTraceIDs_;

    resizeGhostBoundaryTraceGroups( 0 ); // free old memory
    resizeGhostBoundaryTraceGroups( xfld.nGhostBoundaryTraceGroups() );

    for (int n = 0; n < nGhostBoundaryTraceGroups(); n++)
    {
      if ( xfld.ghostBoundaryTraceGroups_[n] == nullptr ) continue;
      ghostBoundaryTraceGroups_[n] = xfld.ghostBoundaryTraceGroups_[n]->clone(); // NOTE: invokes new []
      ghostBoundaryTraceGroups_[n]->setDOF(this->DOF_, this->nDOF_);
    }
  }
}

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
template<class Topology>
void
XField<PhysDim,TopoDim>::checkPositiveJacobianDeterminant( const typename BaseType::template FieldCellGroupType<Topology>& cellGroup  ) const
{
  typedef typename BaseType::template FieldCellGroupType<Topology> XFieldCellGroupType;

  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( cellGroup.basis() );

  if ( xfldElem.order() == 1 )
  {
    typedef QuadraturePoint<TopoDim> QuadPointType;

    // use quadrature rule that matches the polynomial order
    Quadrature<TopoDim, Topology> quadrature( 0 );
    QuadPointType sRef = quadrature.coordinates_cache( 0 );

    // loop over elements within group
    const int nelem = cellGroup.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid DOFs to element
      cellGroup.getElement( xfldElem, elem );

      Real J = xfldElem.jacobianDeterminant( sRef );

      if (J < 0)
      {
        std::stringstream msg;
        msg << "Negative XField Jacobian = " << J << " in cell = " << elem <<std::endl;
        msg << "Element dump:" << std::endl << std::endl;
        xfldElem.dump(3, msg);
        xfldElem.dumpTecplot(msg);
        XFIELD_EXCEPTION( msg.str() );
      }
    }
  }
  else
  {
    //TODO: Need to expand this to support truly curved elements

    typedef QuadraturePoint<TopoDim> QuadPointType;

    // use quadrature rule that matches the polynomial order
    Quadrature<TopoDim, Topology> quadrature( -1 );
    const int nquad = quadrature.nQuadrature();

    // loop over elements within group
    const int nelem = cellGroup.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid DOFs to element
      cellGroup.getElement( xfldElem, elem );

      // loop over quadrature points
      for (int iquad = 0; iquad < nquad; iquad++)
      {
        QuadPointType sRef = quadrature.coordinates_cache( iquad );

        Real J = xfldElem.jacobianDeterminant( sRef );

        if (J < 0)
        {
          VectorX X;
          xfldElem.eval( sRef, X );
          std::stringstream msg;
          msg << "Negative XField Jacobian = " << J << " in cell = " << elem << std::endl;
          msg << std::scientific << std::setprecision(16) << "sRef = " << sRef.ref << std::endl;
          msg << std::scientific << std::setprecision(16) << "X = " << X << std::endl;
          msg << "Element dump:" << std::endl << std::endl;
          xfldElem.dump(3, msg);
          xfldElem.dumpTecplot(msg);
          XFIELD_EXCEPTION( msg.str() );
        }
      }
    }
  }
}

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
void
XField<PhysDim,TopoDim>::initDefaultElmentIDs()
{
  // All types of IDs must be set (or not)
  if (!cellIDs_.empty())
  {
    SANS_ASSERT( !boundaryTraceIDs_.empty() || this->nBoundaryTraceGroups() == 0 );
    return;
  }

  cellIDs_.resize(this->nCellGroups());

  int cellID = 0;
  for (int group = 0; group < this->nCellGroups(); group++)
  {
    cellIDs_[group].resize(this->getCellGroupBase(group).nElem());
    for (std::size_t i = 0; i < cellIDs_[group].size(); i++)
      cellIDs_[group][i] = cellID++;
  }

  boundaryTraceIDs_.resize(this->nBoundaryTraceGroups());

  int boundaryTraceID = 0;
  for (int group = 0; group < this->nBoundaryTraceGroups(); group++)
  {
    boundaryTraceIDs_[group].resize(this->getBoundaryTraceGroupBase(group).nElem());
    for (std::size_t i = 0; i < boundaryTraceIDs_[group].size(); i++)
      boundaryTraceIDs_[group][i] = boundaryTraceID++;
  }
}

template <class TopoDim, class Topology>
class ElementAssociativityConstructor;

// Helper functions to set the normal sign for 1D meshes
template<class TopoDim, class TopologyTrace>
void
setNormalSignL( ElementConnectivityConstructor<ElementAssociativityConstructor<TopoDim, TopologyTrace>>& fldAssoc,
                const int elem,
                const CanonicalTraceToCell& canonicalL ) {}

template<class TopoDim, class TopologyTrace>
void
setNormalSignR( ElementConnectivityConstructor<ElementAssociativityConstructor<TopoDim, TopologyTrace>>& fldAssoc,
                const int elem,
                const CanonicalTraceToCell& canonicalR ) {}

template<class TopoDim>
void
setNormalSignL( ElementConnectivityConstructor<ElementAssociativityConstructor<TopoDim, Node>>& fldAssoc,
                const int elem,
                const CanonicalTraceToCell& canonicalL )
{
  fldAssoc.setAssociativity( elem ).setNormalSignL( canonicalL.trace == 0 ? 1 : -1 );
}

template<class TopoDim>
void
setNormalSignR( ElementConnectivityConstructor<ElementAssociativityConstructor<TopoDim, Node>>& fldAssoc,
                const int elem,
                const CanonicalTraceToCell& canonicalR )
{
  fldAssoc.setAssociativity( elem ).setNormalSignR( canonicalR.trace == 0 ? -1 : 1 );
}

//-----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
template<class TopologyTrace>
void
XField<PhysDim,TopoDim>::
addInteriorGroup( int group_id, const std::map<int,int>& global2localDOFmap,
                  const LagrangeConnectedGroup &traceGroup,
                  const std::vector<LagrangeElementGroup_ptr> &cellGroups,
                  std::vector<std::vector<std::vector<int>>>& orientations  )
{
  typedef typename BasisFunctionTopologyBase<typename TopologyTrace::TopoDim, TopologyTrace>::type BasisFunctionBase;

  // create basis
  const BasisFunctionBase* basis = BasisFunctionBase::getBasisFunction(traceGroup.order(), BasisFunctionCategory_Lagrange);

  // associativity constructor types
  typedef typename BaseType::template FieldTraceGroupType<TopologyTrace> FieldTraceClass;
  typedef typename FieldTraceClass::FieldAssociativityConstructorType FieldAssociativityConstructorType;

  // DOF associativity for the trace elements
  FieldAssociativityConstructorType fldAssoc( basis, traceGroup.nElem() );

  // Set the left and right groups
  int groupL = traceGroup.groupL();
  int groupR = traceGroup.groupR();
  fldAssoc.setGroupLeft( groupL );
  fldAssoc.setGroupRight( groupR );
  std::vector<int> traceNodes;
  std::vector<int> cellNodesL;
  std::vector<int> cellNodesR;

  //printf("adding interior group %d with %d cells\n", group_id, traceGroup.nElem());
  for (int i = 0; i < traceGroup.nElem(); i++)
  {
    std::vector<int> elemMap = traceGroup.elem(i).elemMap;
    std::transform(elemMap.begin(), elemMap.end(), elemMap.begin(),
                   [&](const int idofGlobal) -> int { return global2localDOFmap.at(idofGlobal); });

    // set the rank and DOF mapping for the element
    fldAssoc.setAssociativity( i ).setRank(traceGroup.elem(i).rank);
    fldAssoc.setAssociativity( i ).setGlobalMapping( elemMap );

    // q1 representation of the trace
    traceNodes = traceGroup.elemNodes(i);

    // set left element
    int elemL = traceGroup.elemL(i);
    fldAssoc.setElementLeft( elemL , i );

    // determine and set the canonical trace of the left element
    cellNodesL = cellGroups[groupL]->elemNodes(elemL);
    CanonicalTraceToCell canonicalL = CellToTrace<TopologyTrace,TopoDim>::getCanonicalTrace( cellGroups[groupL]->topo, traceNodes, cellNodesL );
    fldAssoc.setCanonicalTraceLeft( canonicalL , i );
    setNormalSignL(fldAssoc, i, canonicalL);

    // set right element
    int elemR = traceGroup.elemR(i);
    fldAssoc.setElementRight( elemR , i );

    // determine and set the canonical trace of the right element
    cellNodesR = cellGroups[groupR]->elemNodes(elemR);
    CanonicalTraceToCell canonicalR = CellToTrace<TopologyTrace,TopoDim>::getCanonicalTrace( cellGroups[groupR]->topo, traceNodes, cellNodesR );
    fldAssoc.setCanonicalTraceRight( canonicalR , i );
    setNormalSignR(fldAssoc, i, canonicalR);

    // inform the associativity constructors of the correct trace orientations for the left and right elements
    orientations[groupL][elemL][canonicalL.trace] = canonicalL.orientation;
    orientations[groupR][elemR][canonicalR.trace] = canonicalR.orientation;
  }

  // finally construct the trace group
  this->createInteriorTraceGroup( group_id , new FieldTraceClass(fldAssoc) );
}

//-----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
template<class TopologyTrace>
void
XField<PhysDim,TopoDim>::
addBoundaryGroup( int group_id, const std::map<int,int>& global2localDOFmap,
                  const LagrangeConnectedGroup &traceGroup,
                  const std::vector<LagrangeElementGroup_ptr>& cellGroups,
                  PointerArray<FieldTraceGroupBase>& boundaryTraceGroups,
                  const std::vector<PeriodicBCNodeMap>& periodicity,
                  bool BCpossessed,
                  std::vector<std::vector<std::vector<int>>>& orientations )
{
  // create basis
  typedef typename BasisFunctionTopologyBase<typename TopologyTrace::TopoDim, TopologyTrace>::type BasisFunctionBase;

  const BasisFunctionBase* basis = BasisFunctionBase::getBasisFunction( traceGroup.order(), BasisFunctionCategory_Lagrange );

  // associativity constructor types
  typedef typename BaseType::template FieldTraceGroupType<TopologyTrace> FieldTraceClass;
  typedef typename FieldTraceClass::FieldAssociativityConstructorType FieldAssociativityConstructorType;

  // DOF associativity for the trace elements
  FieldAssociativityConstructorType fldAssoc( basis, traceGroup.nElem() );

  if (BCpossessed)
    boundaryTraceIDs_[group_id].resize( traceGroup.nElem(), -1 );

  // set the left group
  int groupL = traceGroup.groupL();
  int groupR = traceGroup.groupR();
  fldAssoc.setGroupLeft( groupL );
  if (groupR >= 0)
    fldAssoc.setGroupRight( groupR );

  //printf("adding boundary group %d with %d cells, order = %d\n",group_id,traceGroup.nElem(),traceGroup.order());
  for (int i = 0; i < traceGroup.nElem(); i++)
  {
    std::vector<int> elemMap = traceGroup.elem(i).elemMap;
    std::transform(elemMap.begin(), elemMap.end(), elemMap.begin(),
                   [&](const int idofGlobal) -> int { return global2localDOFmap.at(idofGlobal); });


    // save off the unique boundary trace ID
    if (BCpossessed)
      boundaryTraceIDs_[group_id][i] = traceGroup.elem(i).elemID;

    // set the rank and DOF mapping for the element
    fldAssoc.setAssociativity( i ).setRank(traceGroup.elem(i).rank);
    fldAssoc.setAssociativity( i ).setGlobalMapping( elemMap );

    // q1 representation of the trace
    std::vector<int> traceNodes = traceGroup.elemNodes(i);

    // set left element
    int elemL  = traceGroup.elemL(i);
    fldAssoc.setElementLeft( elemL, i );

    // determine and set the canonical trace of the left element
    std::vector<int> cellNodesL = cellGroups[groupL]->elemNodes(elemL);
    CanonicalTraceToCell canonicalL = CellToTrace<TopologyTrace,TopoDim>::getCanonicalTrace( cellGroups[groupL]->topo, traceNodes, cellNodesL );
    fldAssoc.setCanonicalTraceLeft( canonicalL, i );
    setNormalSignL(fldAssoc, i, canonicalL);

    // inform the associativity constructors of the correct trace orientations for the left element
    orientations[groupL][elemL][canonicalL.trace] = canonicalL.orientation;

    if (groupR >= 0 && BCpossessed)
    {
      // set right element
      int elemR  = traceGroup.elemR(i);
      fldAssoc.setElementRight( elemR, i );

      // determine and set the canonical trace of the right element
      std::vector<int> cellNodesR = cellGroups[groupR]->elemNodes(elemR);

      for ( const PeriodicBCNodeMap& period : periodicity )
      {
        if (period.bndGroupL != group_id) continue;

        // Update the mapping trace nodes from left to right
        for (std::size_t node = 0; node < traceNodes.size(); node++)
          traceNodes[node] = period.mapLtoR.at(traceNodes[node]);

        CanonicalTraceToCell canonicalR = CellToTrace<TopologyTrace,TopoDim>::getCanonicalTrace( cellGroups[groupR]->topo, traceNodes, cellNodesR );
        fldAssoc.setCanonicalTraceRight( canonicalR, i );
        setNormalSignR(fldAssoc, i, canonicalR);

        // inform the associativity constructors of the correct trace orientations for the left and right elements
        orientations[groupR][elemR][canonicalR.trace] = canonicalR.orientation;
      }
    }
  }

  // finally construct the trace group
  SANS_ASSERT(group_id < boundaryTraceGroups.size());
  SANS_ASSERT(boundaryTraceGroups[group_id] == nullptr);
  boundaryTraceGroups[group_id] = new FieldTraceClass(fldAssoc);
  boundaryTraceGroups[group_id]->setDOF(this->DOF_, this->nDOF_);
}

//-----------------------------------------------------------------------------//
template<class PhysDim,class TopoDim>
template <typename TopologyCell>
void
XField<PhysDim,TopoDim>::
addCellGroup( int group_id, const std::map<int,int>& global2localDOFmap,
              const LagrangeElementGroup_ptr& cellGroup, const std::vector<std::vector<int>>& orientations )
{
  // create basis
  typedef typename BasisFunctionTopologyBase<TopoDim, TopologyCell>::type BasisFunctionBase;

  const BasisFunctionBase* basis = BasisFunctionBase::getBasisFunction(cellGroup->order(), BasisFunctionCategory_Lagrange);

  // field associativity constructor
  typedef typename BaseType::template FieldCellGroupType<TopologyCell> FieldCellClass;
  typedef typename FieldCellClass::FieldAssociativityConstructorType FieldAssociativityConstructorType;

  // DOF associativity for the cell elements
  FieldAssociativityConstructorType fldAssocCell( basis, cellGroup->nElem() );
  cellIDs_[group_id].resize(cellGroup->nElem());

  for (int i = 0; i < cellGroup->nElem(); i++)
  {
    std::vector<int> elemMap = cellGroup->elem(i).elemMap;
    std::transform(elemMap.begin(), elemMap.end(), elemMap.begin(),
                   [&](const int idofGlobal) -> int { return global2localDOFmap.at(idofGlobal); });

    // save off the unique cell ID
    cellIDs_[group_id][i] = cellGroup->elem(i).elemID;

    // set the rank and DOF mapping for the element
    fldAssocCell.setAssociativity( i ).setRank(cellGroup->elem(i).rank);
    fldAssocCell.setAssociativity( i ).setGlobalMapping( elemMap );

    SANS_ASSERT( (int)orientations[i].size() == TopologyCell::NTrace );
    for (int j = 0; j < TopologyCell::NTrace; j++)
    {
      fldAssocCell.setAssociativity(i).setOrientation( orientations[i][j], j );
    }
  }

  this->createCellGroup( group_id, new FieldCellClass( fldAssocCell ) );
  this->nElem_ += cellGroup->nElem();
}

// Helper functions for building elements from LagrangeElement groups
void
checkElementGroupTypes( const std::vector<LagrangeElementGroup_ptr>& groups );


//-----------------------------------------------------------------------------//
// build an XField from simple cell descriptions
template<class PhysDim,class TopoDim>
void
XField<PhysDim,TopoDim>::
buildFrom( XField_Lagrange<PhysDim>& xfldin )
{
  // copy the communicator
  comm_ = xfldin.comm();

  // finalize the grid
  xfldin.balance();

  int nCellGroup   = xfldin.nCellGroups();
  int nBTraceGroup = xfldin.nBoundaryTraceGroups();

  cellIDs_.resize(nCellGroup);
  boundaryTraceIDs_.resize(nBTraceGroup);

  this->resizeDOF(xfldin.nDOF());
  for (int i = 0; i < xfldin.nDOF(); i++)
  {
    // copy over DOFs
    this->DOF_[i] = xfldin.DOF(i).X;

    // set the local to global DOF map
    // this maps from a continuous local DOF number on each processor
    // to the original global number of the DOFs across all processors
    this->local2nativeDOFmap_[i] = xfldin.DOF(i).idof;
  }

  const std::map<int,int>& global2localDOFmap = xfldin.global2localDOFmap();

  // first check that all cell groups are of the same geometric order and that the types are supported
  checkElementGroupTypes( xfldin.cellGroups() );

  // setup the storage for the cell trace orientations
  std::vector<std::vector<std::vector<int>>> orientations(nCellGroup);
  for (int igroup = 0; igroup < nCellGroup; igroup++)
  {
    orientations[igroup].resize(xfldin.cellGroup(igroup)->nElem());
    int nTrace = xfldin.cellGroup(igroup)->nTracePerElem();
    int nElem = xfldin.cellGroup(igroup)->nElem();
    for (int elem = 0; elem < nElem; elem++)
      orientations[igroup][elem].resize(nTrace, 0);
  }

  // we have separate interior face groups for the different cell groups since the orders might be different
  // but the underlying shape *needs* to be the same (because we don't support pyramids yet...think about it)
  std::vector<LagrangeConnectedGroup> interiorGroups;
  std::vector<LagrangeConnectedGroup> boundaryGroups;
  std::vector<LagrangeConnectedGroup> ghostBoundaryGroups;
  xfldin.getConnectedTraceGroups(interiorGroups, boundaryGroups, ghostBoundaryGroups);
  xfldin.mergePeriodicGroups( boundaryGroups );

  // add the interior trace groups
  this->resizeInteriorTraceGroups( interiorGroups.size() );
  for (std::size_t i = 0; i < interiorGroups.size(); i++)
  {
    // add the interior trace group to the XField
    this->addInteriorGroupType( i, global2localDOFmap, interiorGroups[i], xfldin.cellGroups(), orientations );
  }

  // add the boundary trace groups
  this->resizeBoundaryTraceGroups( nBTraceGroup );
  SANS_ASSERT( (int)boundaryGroups.size() == nBTraceGroup );
  for (std::size_t i = 0; i < boundaryGroups.size(); i++)
  {
    // add the boundary group to the XField
    this->addBoundaryGroupType( i, global2localDOFmap, boundaryGroups[i], xfldin.cellGroups(),
                                this->boundaryTraceGroups_, xfldin.periodicity(), true, orientations );
  }

  ghostBoundaryTraceGroups_.resize(ghostBoundaryGroups.size());
  for (std::size_t i = 0; i < ghostBoundaryGroups.size(); i++)
  {
    // add the ghost boundary group to the XField
    this->addBoundaryGroupType( i, global2localDOFmap, ghostBoundaryGroups[i], xfldin.cellGroups(),
                                ghostBoundaryTraceGroups_, xfldin.periodicity(), false, orientations );
  }

  // finally we can add the cell groups
  this->nElem_ = 0;
  this->resizeCellGroups( nCellGroup );
  for (int i = 0; i < nCellGroup; i++)
  {
    this->addCellGroupType( i, global2localDOFmap, xfldin.cellGroup(i), orientations[i] );
  }

  //Check that the grid is correct
  this->checkGrid();
}

//-----------------------------------------------------------------------------//
template<class PhysDim,class TopoDim>
const typename XField<PhysDim,TopoDim>::FieldTraceGroupBase&
XField<PhysDim,TopoDim>::
getGhostBoundaryTraceGroupBase( const int group ) const
{
  SANS_ASSERT_MSG(group >= 0 && group < ghostBoundaryTraceGroups_.size(),
                  "ghostBoundaryTraceGroups_.size() = %d, group = %d", ghostBoundaryTraceGroups_.size(), group);
  SANS_ASSERT( ghostBoundaryTraceGroups_[group] != NULL );
  return *ghostBoundaryTraceGroups_[group];
}

template<class PhysDim,class TopoDim>
template<class Topology>
const typename XField<PhysDim,TopoDim>::template FieldTraceGroupType<Topology>&
XField<PhysDim,TopoDim>::
getGhostBoundaryTraceGroup( const int group ) const
{
  typedef FieldTraceGroupType<Topology> FieldTraceGroupClass;
  SANS_ASSERT( group >=0 && group < ghostBoundaryTraceGroups_.size() );
  SANS_ASSERT( ghostBoundaryTraceGroups_[group] != NULL );
  SANS_ASSERT( ghostBoundaryTraceGroups_[group]->topoTypeID() == typeid(Topology) );
  return static_cast<const FieldTraceGroupClass&>( *ghostBoundaryTraceGroups_[group] );
}

//-----------------------------------------------------------------------------//
template<class PhysDim,class TopoDim>
void
XField<PhysDim,TopoDim>::
resizeGhostBoundaryTraceGroups( const int nGhostBoundaryTraceGroups )
{
  ghostBoundaryTraceGroups_.resize(nGhostBoundaryTraceGroups);
}

//-----------------------------------------------------------------------------//
template<class PhysDim,class TopoDim>
std::vector<int>
XField<PhysDim,TopoDim>::
getInteriorTraceGroupsGlobal( const std::vector<int>& cellGroupsGlobal ) const
{
  std::vector<int> interiorTraceGroups;

  for (int group = 0; group < this->nInteriorTraceGroups(); group++)
  {
    const FieldTraceGroupBase& xfldTrace = this->getInteriorTraceGroupBase(group);
    int cellGroupL = xfldTrace.getGroupLeft();
    int cellGroupR = xfldTrace.getGroupRight();

    // Add the interior trace group if both left and right cell groups are in the list
    if (std::find(cellGroupsGlobal.begin(), cellGroupsGlobal.end(), cellGroupL) != cellGroupsGlobal.end() &&
        std::find(cellGroupsGlobal.begin(), cellGroupsGlobal.end(), cellGroupR) != cellGroupsGlobal.end() )
    {
      interiorTraceGroups.push_back(group);
    }
  }

  return interiorTraceGroups;
}

//-----------------------------------------------------------------------------//
template<class PhysDim,class TopoDim>
std::vector<int>
XField<PhysDim,TopoDim>::
getPeriodicTraceGroupsGlobal( const std::vector<int>& cellGroupsGlobal ) const
{
  std::vector<int> boundaryTraceGroups;

  for (int group = 0; group < this->nBoundaryTraceGroups(); group++)
  {
    const FieldTraceGroupBase& xfldTrace = this->getBoundaryTraceGroupBase(group);
    int cellGroupL = xfldTrace.getGroupLeft();
    int cellGroupR = xfldTrace.getGroupRight();
    GroupElmentType groupType = xfldTrace.getGroupRightType();

    // Don't check if the boundary group is not connected or is a hubtrace
    if (cellGroupR < 0) continue;
    if (groupType == eHubTraceGroup) continue;

    // Add the interior trace group if both left and right cell groups are in the list
    if (std::find(cellGroupsGlobal.begin(), cellGroupsGlobal.end(), cellGroupL) != cellGroupsGlobal.end() &&
        std::find(cellGroupsGlobal.begin(), cellGroupsGlobal.end(), cellGroupR) != cellGroupsGlobal.end() )
    {
      boundaryTraceGroups.push_back(group);
    }
  }

  return boundaryTraceGroups;
}


//-----------------------------------------------------------------------------//
template<class PhysDim,class TopoDim>
std::shared_ptr<mpi::communicator>
XField<PhysDim,TopoDim>::comm() const
{
  return comm_;
}

}
