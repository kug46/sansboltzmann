// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDLINE_DG_HUBTRACE_H
#define FIELDLINE_DG_HUBTRACE_H

#include "Field_DG_HubTraceBase.h"
#include "FieldLine.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// 1D solution field: DG hub trace (e.g. Lagrange multiplier)
//----------------------------------------------------------------------------//

template <class PhysDim, class T>
class Field_DG_HubTrace< PhysDim, TopoD1, T > : public Field_DG_HubTraceBase< PhysDim, TopoD1, T >
{
public:
  typedef Field_DG_HubTraceBase< PhysDim, TopoD1, T > BaseType;
  typedef T ArrayQ;

  Field_DG_HubTrace( const XField<PhysDim, TopoD1>& xfld, const int order, const BasisFunctionCategory& category );

  Field_DG_HubTrace( const XField<PhysDim, TopoD1>& xfld, const int order,
                          const BasisFunctionCategory& category, const std::vector<int>& BoundaryGroups );

  Field_DG_HubTrace( const Field_DG_HubTrace& fld, const FieldCopy& tag );

  Field_DG_HubTrace& operator=( const ArrayQ& q );

protected:
  using BaseType::nDOF_;
  using BaseType::DOF_;
  using BaseType::hubTraceGroups_;

  void init(const XField<PhysDim, TopoD1>& xfld, const int order,
            const BasisFunctionCategory& category, const std::vector<int>& BoundaryGroups );
};

}

#endif  // FIELDLINE_DG_HUBTRACE_H
