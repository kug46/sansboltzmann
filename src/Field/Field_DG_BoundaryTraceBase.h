// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELD_DG_BOUNDARYTRACEBASE_H
#define FIELD_DG_BOUNDARYTRACEBASE_H

#include "Field.h"

#include "tools/split_cat_std_vector.h" // SANS::cat in derived constructors

namespace SANS
{

//----------------------------------------------------------------------------//
// solution field: DG boundary trace-field constructor class
//----------------------------------------------------------------------------//

template <class PhysDim, class TopoDim, class T>
class Field_DG_BoundaryTraceBase : public Field< PhysDim, TopoDim, T >
{
public:
  typedef Field< PhysDim, TopoDim, T > BaseType;
  typedef T ArrayQ;

  Field_DG_BoundaryTraceBase() = delete;
  virtual ~Field_DG_BoundaryTraceBase() {};

  virtual int nDOFCellGroup(int cellgroup) const override;
  virtual int nDOFInteriorTraceGroup(int tracegroup) const override;
  virtual int nDOFBoundaryTraceGroup(int tracegroup) const override;

  // Virtual class to denote the SpaceType of the Field
  virtual SpaceType spaceType() const override { return SpaceType::Discontinuous; }

protected:
  //Protected constructor. This is a helper class
  explicit Field_DG_BoundaryTraceBase( const XField<PhysDim, TopoDim>& xfld );

  Field_DG_BoundaryTraceBase( const Field_DG_BoundaryTraceBase& fld, const FieldCopy& tag );

  // Constructs a new cell group
  template<class Topology>
  void createBoundaryTraceGroup(const int groupGlobal, const int order, const BasisFunctionCategory& category);

  void createDOFs();

  using BaseType::nDOF_;
  using BaseType::DOF_;
  using BaseType::nElem_;
  using BaseType::boundaryTraceGroups_;
  using BaseType::xfld_;
  using BaseType::localBoundaryTraceGroups_;
};

}

#endif  // FIELD_DG_BOUNDARYTRACEBASE_H
