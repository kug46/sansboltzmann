// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "DistanceFunction.h"

#include <limits>

#include <kdtree++/kdtree.hpp>

#include "tools/timer.h"
#include "tools/minmax.h"

#include "Topology/ElementTopology.h"
#include "Quadrature/Quadrature.h"

#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/LagrangeDOFMap.h"
#include "BasisFunction/BasisFunctionNode.h"
#include "BasisFunction/BasisFunctionLine_Lagrange.h"
#include "BasisFunction/BasisFunctionArea_Quad_Lagrange.h"
#include "BasisFunction/BasisFunctionArea_Triangle_Lagrange.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron_Lagrange.h"
#include "BasisFunction/BasisFunctionVolume_Hexahedron_Lagrange.h"

#include "DistanceSolver.h"

#include "Field/XField_BoundaryTrace.h"

#include "Field/XFieldLine.h"
#include "Field/XFieldArea.h"
#include "Field/XFieldVolume.h"

#include "Field/XFieldLine_BoundaryTrace.h"
#include "Field/XFieldArea_BoundaryTrace.h"
#include "Field/XFieldVolume_BoundaryTrace.h"

#include "Field/FieldLine.h"
#include "Field/FieldArea.h"
#include "Field/FieldVolume.h"

#include "Field/tools/GroupFunctorType.h"
#include "Field/Tuple/FieldTuple.h"

#include "Field/tools/for_each_CellGroup.h"
#include "Field/tools/for_each_BoundaryTraceGroup.h"
#include "Field/tools/for_each_BoundaryTraceGroup_Cell.h"
#include "Field/tools/GroupElemIndex.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/InverseLUP.h"
#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

template<class PhysDim, int TopoTraceD>
class DistanceFunction3D_KDVertex
{
public:
  typedef std::vector<GroupElemIndex> IndexVector;

  static const int D = PhysDim::D;
  typedef Real value_type;

  typedef DLA::VectorS<D,Real> VectorX; // coordinates vector
  typedef DLA::VectorS<TopoTraceD,Real> RefCoordTraceType;

  DistanceFunction3D_KDVertex(const VectorX& X, const IndexVector& nodeElemMap) :
    X(X), nodeElemMap(nodeElemMap) {}

  inline value_type operator[](size_t const n) const { return X[n]; }

  VectorX X;
  IndexVector nodeElemMap;
};


//----------------------------------------------------------------------------//
//  Populates the tree with the physical coordinates of the quadrature points on the boundary of the given field
//
template<class PhysDim>
class PopulateTree_Distance3D : public GroupFunctorBoundaryTraceType< PopulateTree_Distance3D<PhysDim> >
{
public:
  typedef std::vector<GroupElemIndex> IndexVector;

  static const int D = PhysDim::D;

  PopulateTree_Distance3D( std::vector<IndexVector>& nodeElemMap, const int nLocalBTraceGroups ) :
    nodeElemMap_(nodeElemMap), nLocalBTraceGroups_(nLocalBTraceGroups) {}

  std::size_t nBoundaryTraceGroups() const          { return nLocalBTraceGroups_; }
  std::size_t boundaryTraceGroup(const int n) const { return n;                   }

  //----------------------------------------------------------------------------//
  // Integration function that integrates each element in the trace group
  template <class TopologyTrace>
  void
  apply( const typename XField_BoundaryTrace<PhysDim, typename TopologyTrace::CellTopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
         const int traceGroupGlobal )
  {
    int nodeDOFmap[TopologyTrace::NNode];

    // loop over elements within group
    const int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // get the node mapping for the element
      xfldTrace.associativity( elem ).getNodeGlobalMapping(nodeDOFmap, TopologyTrace::NNode);

      // add the list of elements
      for (int node = 0; node < TopologyTrace::NNode; node++)
        nodeElemMap_[nodeDOFmap[node]].emplace_back(traceGroupGlobal, elem);
    }
  }

protected:
  std::vector<IndexVector>& nodeElemMap_;
  const int nLocalBTraceGroups_;
};


//----------------------------------------------------------------------------//
// Explicitly sets Lagrange nodes on wall boundaries to zero distance
template<class PhysDim>
class SetWallBoundary_Distance3D :
    public GroupFunctorBoundaryCellType<SetWallBoundary_Distance3D<PhysDim>>
{
public:

  explicit SetWallBoundary_Distance3D( const std::vector<int>& boundaryTraceGroups ) :
    boundaryTraceGroups_(boundaryTraceGroups) {}

  std::size_t nBoundaryGroups() const { return boundaryTraceGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return boundaryTraceGroups_[n]; }

  //----------------------------------------------------------------------------//
  // Distribution function that redistributes the error in each cell group
  template< class TopologyTrace, class TopologyL>
  void
  apply( const typename Field<PhysDim, typename TopologyL::TopoDim, Real>::template FieldCellGroupType<TopologyL>& dfldCellL,
         const int cellGroupGlobalL,
         const typename XField<PhysDim, typename TopologyL::TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
         const int traceGroupGlobal)
  {
    // Cell Group Types
    typedef typename Field<PhysDim, typename TopologyL::TopoDim, Real>::template FieldCellGroupType<TopologyL> DFieldCellGroupTypeL;
    typedef typename DFieldCellGroupTypeL::template ElementType<> ElementDFieldCellClassL;

    // Construct the element
    ElementDFieldCellClassL dfldElemL( dfldCellL.basis() );
    int dorder = dfldElemL.order();

    // loop over elements
    const int nElem = xfldTrace.nElem();
    for (int elem = 0; elem < nElem; elem++)
    {
      int elemL = xfldTrace.getElementLeft( elem );
      CanonicalTraceToCell canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // copy global distance DOFs to element
      dfldCellL.getElement( dfldElemL, elemL );

      // set all DOFs on wall boundaries explicitly to zero
      std::vector<int> map = LagrangeDOFMap<TopologyL>::getCanonicalTraceMap( canonicalTraceL.trace, dorder );

      for (std::size_t i = 0; i < map.size(); i++)
        dfldElemL.DOF(map[i]) = 0;

      // save the element
      const_cast<DFieldCellGroupTypeL&>(dfldCellL).setElement( dfldElemL, elemL );
    }
  }
protected:
  const std::vector<int> boundaryTraceGroups_;
};


//----------------------------------------------------------------------------//
//  Populates the DOFs of the distance field
//
template<class PhysDim, class TopoDim>
class PopulateDistanceField3D : public GroupFunctorCellType< PopulateDistanceField3D<PhysDim, TopoDim> >
{
public:

  static const int D = PhysDim::D;

  typedef Real T; //solution data type: Real (distance)
  typedef DistanceFunction3D_KDVertex<PhysDim, MAX(TopoDim::D-1,1)> KDVertex;
  typedef KDTree::KDTree<D, KDVertex> KDTreeType;
  typedef typename KDVertex::IndexVector IndexVector;
  typedef typename KDVertex::VectorX VectorX;

  PopulateDistanceField3D( const KDTreeType& tree, const XField_BoundaryTrace<PhysDim, TopoDim>& xfld_bnd,
                           const int nCellGroups) :
    tree_(tree), solver_(xfld_bnd), xfld_bnd_(xfld_bnd), nCellGroups_(nCellGroups) {}

  std::size_t nCellGroups() const          { return nCellGroups_; }
  std::size_t cellGroup(const int n) const { return n;            }

//----------------------------------------------------------------------------//
  // Function that loops over each element in the cell group
  template <class Topology>
  void
  apply( const typename FieldTuple<XField<PhysDim, TopoDim>, Field<PhysDim, TopoDim, T>, TupleClass<>>::
                        template FieldCellGroupType<Topology>& fldsCell,
         const int cellGroupGlobal )
  {
    typedef typename XField<PhysDim, TopoDim   >::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field <PhysDim, TopoDim, T>::template FieldCellGroupType<Topology> QFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

    const XFieldCellGroupType& xfldCell    = get<0>(fldsCell);
          QFieldCellGroupType& distfldCell = const_cast<QFieldCellGroupType&>( get<1>(fldsCell) );

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass distfldElem( distfldCell.basis() );

    typedef typename ElementXField<PhysDim, TopoDim, Topology>::VectorX VectorX;
    typedef typename ElementXField<PhysDim, TopoDim, Topology>::RefCoordType RefCoordType;
    typedef DLA::VectorS<MAX(TopoDim::D-1,1), Real> RefCoordTraceType;

    RefCoordType Ref;  // reference-element coordinates
    VectorX X;         // physical coordinates
    RefCoordTraceType sRef0; // reference coordinate on the closest trace element

    // martix for computing Barycentric coordinates on a triangle
    DLA::MatrixS<3,3,Real> R, Rinv;

    R(0,0) = 0; R(0,1) = 1; R(0,2) = 0;
    R(1,0) = 0; R(1,1) = 0; R(1,2) = 1;
    R(2,0) = 1; R(2,1) = 1; R(2,2) = 1;

    Rinv = DLA::InverseLUP::Inverse(R);

    // loop over elements within group
    const int nElem = xfldCell.nElem();

    IndexVector dummyidx;

    std::vector<DLA::VectorS<TopoDim::D,Real>> sRef;
    LagrangeNodes<Topology>::get(distfldCell.order(), sRef);

    for (int elem = 0; elem < nElem; elem++)
    {
      // copy global grid DOFs to element
      xfldCell.getElement( xfldElem, elem );
      distfldCell.getElement( distfldElem, elem );

      for (std::size_t i = 0; i < sRef.size(); i++)
      {
        // dont recompute
        if ( distfldElem.DOF(i) != -1 ) continue;
        distfldElem.DOF(i) = std::numeric_limits<Real>::max();

        // physical coordinates at the Lagrange node
        xfldElem.coordinates( sRef[i], X );

#if 0
        // get the closest boundary point for this Lagrange node from KDtree
        KDVertex targetVertex(X, dummyidx); //dummy cellgroup, cellelem

        std::pair<typename KDTreeType::const_iterator,double> it = tree_.find_nearest(targetVertex);

        typedef std::vector<GroupElemIndex> IndexVector;

        const IndexVector& nodeElemMap = it.first->nodeElemMap;

        for (std::size_t n = 0; n < nodeElemMap.size(); n++)
        {
          int btracegroup_nearest = nodeElemMap[n].group;
          int elem_nearest = nodeElemMap[n].elem;
        }
#endif
        for (int btracegroup_nearest = 0; btracegroup_nearest < xfld_bnd_.nBoundaryTraceGroups(); btracegroup_nearest++)
        {
          int nElem_bnd = xfld_bnd_.getBoundaryTraceGroupBase(btracegroup_nearest).nElem();
          for (int elem_nearest = 0; elem_nearest < nElem_bnd; elem_nearest++)
          {
            typedef Triangle TopoTrace;
            typedef typename XField_BoundaryTrace<PhysDim, TopoDim>::template FieldTraceGroupType<TopoTrace> XFieldTraceGroupType;
            typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
            typedef typename ElementXFieldTraceClass::RefCoordType RefCoordType;

            const XFieldTraceGroupType& xfldTraceGroup = xfld_bnd_.template getBoundaryTraceGroupGlobal<TopoTrace>(btracegroup_nearest);

            ElementXFieldTraceClass xfldElem( xfldTraceGroup.basis() );

            // copy global grid DOFs to element
            xfldTraceGroup.getElement( xfldElem, elem_nearest );

            VectorX N, Xc, dX;

            xfldElem.unitNormal(TopoTrace::centerRef, N);
            xfldElem.eval(TopoTrace::centerRef, Xc);

            // get the point on the plane of the triangle
            VectorX Xt = X - dot(N, (X - Xc))*N;

            // Subtract off the first DOF to convert to reference coordinates
            VectorX Xp = Xt - xfldElem.DOF(0);

            // get the gradient to convert into reference coordinates
            VectorX sgrad, tgrad;
            xfldElem.evalReferenceCoordinateGradient(TopoTrace::centerRef, sgrad, tgrad );

            RefCoordType sRefXp = {dot(sgrad,Xp), dot(tgrad,Xp)};

            DLA::VectorS<3,Real> rhs = {sRefXp[0], sRefXp[1], 1};

            // compute the Barycentric coordinates of the point on the plane
            DLA::VectorS<3,Real> lam = Rinv*rhs;

            Real dist = std::numeric_limits<Real>::max();

            if (lam[1] <= 0 && lam[2] <= 0)      // closet to node 0
            {
              dX = X - xfldElem.DOF(0);
              dist = sqrt(dot(dX,dX));
            }
            else if (lam[0] <= 0 && lam[2] <= 0) // closet to node 1
            {
              dX = X - xfldElem.DOF(1);
              dist = sqrt(dot(dX,dX));
            }
            else if (lam[0] <= 0 && lam[1] <= 0) // closet to node 2
            {
              dX = X - xfldElem.DOF(2);
              dist = sqrt(dot(dX,dX));
            }
            else if (lam[0] < 0)      // Point is outside of edge 0
            {
              // normal vector on the edge
              RefCoordType Ne = {1/sqrt(2.), 1/sqrt(2.)};
              RefCoordType sRef0 = {1, 0};

              // get the reference coordinates on the edge
              RefCoordType sRefEdge = sRefXp - dot(Ne, (sRefXp - sRef0))*Ne;

              // edge coordinate
              Real s = sRefEdge[0];

              if (s > 1)
              {
                // node 1 is the closest
                dX = X - xfldElem.DOF(1);
                dist = sqrt(dot(dX,dX));
              }
              else if (s < 0)
              {
                // node 2 is the closest
                dX = X - xfldElem.DOF(2);
                dist = sqrt(dot(dX,dX));
              }
              else
              {
                // closest point is on the edge
                xfldElem.eval(sRefEdge, Xt);
                dX = X - Xt;
                dist = sqrt(dot(dX,dX));
              }
            }
            else if (lam[1] < 0) // Point is outside of edge 1
            {
              // normal vector on the edge
              RefCoordType Ne = {-1., 0.};
              RefCoordType sRef0 = {0, 1};

              // get the reference coordinates on the edge
              RefCoordType sRefEdge = sRefXp - dot(Ne, (sRefXp - sRef0))*Ne;

              // reference t coordinate
              Real t = sRefEdge[1];

              if (t > 1)
              {
                // node 2 is the closest
                dX = X - xfldElem.DOF(2);
                dist = sqrt(dot(dX,dX));
              }
              else if (t < 0)
              {
                // node 0 is the closest
                dX = X - xfldElem.DOF(0);
                dist = sqrt(dot(dX,dX));
              }
              else
              {
                // closest point is on the edge
                xfldElem.eval(sRefEdge, Xt);
                dX = X - Xt;
                dist = sqrt(dot(dX,dX));
              }
            }
            else if (lam[2] < 0) // Point is outside of edge 2
            {
              // normal vector on the edge
              RefCoordType Ne = {0., -1.};
              RefCoordType sRef0 = {0, 0};

              // get the reference coordinates on the edge
              RefCoordType sRefEdge = sRefXp - dot(Ne, (sRefXp - sRef0))*Ne;

              // reference s coordinate
              Real s = sRefEdge[0];

              if (s > 1)
              {
                // node 1 is the closest
                dX = X - xfldElem.DOF(1);
                dist = sqrt(dot(dX,dX));
              }
              else if (s < 0)
              {
                // node 0 is the closest
                dX = X - xfldElem.DOF(0);
                dist = sqrt(dot(dX,dX));
              }
              else
              {
                // closest point is on the edge
                xfldElem.eval(sRefEdge, Xt);
                dX = X - Xt;
                dist = sqrt(dot(dX,dX));
              }
            }
            else
            {
              // The point on the plane is inside the triangle
              dX = X - Xt;
              dist = sqrt(dot(dX,dX));
            }

            distfldElem.DOF(i) = std::min(dist,distfldElem.DOF(i));
          }
        }
      }

      // set the interpolated distance
      distfldCell.setElement( distfldElem, elem );
    }
  }

protected:
  const KDTreeType& tree_;
  const DistanceSolver<PhysDim, TopoDim> solver_;
  const XField_BoundaryTrace<PhysDim, TopoDim>& xfld_bnd_;
  const int nCellGroups_;
};

/*---------------------------------------------------------------------------------------*/
template<class PhysDim, class TopoDim>
void DistanceFunction( Field<PhysDim, TopoDim, Real>& distfld, const std::vector<int>& btraceGroups)
{
  static const int D = PhysDim::D;

  typedef DistanceFunction3D_KDVertex<PhysDim, MAX(TopoDim::D-1,1)> KDVertex;
  typedef KDTree::KDTree<D, KDVertex> KDTreeType;

  // Distance field is interpolated, so we can only use Lagrange basis functions
  for (int i = 0; i < distfld.nCellGroups(); i++)
    SANS_ASSERT( distfld.getCellGroupBase(i).basisCategory() == BasisFunctionCategory_Lagrange );

  const XField<PhysDim, TopoDim>& xfld = distfld.getXField();

  for (int i = 0; i < xfld.nBoundaryTraceGroups(); i++)
    SANS_ASSERT( xfld.getBoundaryTraceGroupBase(i).order() == 1 );

  // mark all distances as not set
  for (int n = 0; n < distfld.nDOF(); n++)
    distfld.DOF(n) = -1;

  if (xfld.comm()->rank() == 0)
    std::cout << "Constructing global boundary groups for distance function." << std::endl;

  XField_BoundaryTrace<PhysDim, TopoDim> xfld_bnd(xfld, btraceGroups);

  // Maps nodel DOF's to elements
  typedef std::vector<GroupElemIndex> IndexVector;
  std::vector<IndexVector> nodeElemMap(xfld_bnd.nDOF());

  timer disttime;

  if (xfld.comm()->rank() == 0)
    std::cout << "Computing distance function..." << std::endl;

  //Insert all boundary quadrature points in the KDtree
  //const int nLocalBTraceGroups = xfld_bnd.nBoundaryTraceGroups();
  //for_each_BoundaryTraceGroup<TopoDim>::apply( PopulateTree_Distance3D<PhysDim>(nodeElemMap, nLocalBTraceGroups), xfld_bnd );

  KDTreeType tree;

  // populate the tree with all node DOFs and the mappings
  //for (int n = 0; n < xfld_bnd.nDOF(); n++)
  //  tree.insert({xfld_bnd.DOF(n), nodeElemMap[n]});

  //tree.optimize();

  const int nCellGroups = distfld.nCellGroups();

  //Compute and distance solution on distfld
  for_each_CellGroup<TopoDim>::apply( PopulateDistanceField3D<PhysDim, TopoDim>(tree, xfld_bnd, nCellGroups), (xfld, distfld) );

  //Set all DOFs on wall boundaries explicitly to zero
  //for_each_BoundaryTraceGroup_Cell<TopoDim>::apply( SetWallBoundary_Distance3D<PhysDim>(btraceGroups), distfld );

  // wait for all processors to finish computing their distance function
  xfld.comm()->barrier();

  if (xfld.comm()->rank() == 0)
    std::cout << "Distance function time: " << disttime.elapsed() << " s" << std::endl;
}

//Explicit instantiations
//template void DistanceFunction<PhysD3, TopoD3>( Field<PhysD3, TopoD3, Real>& distfld, const std::vector<int>& btraceGroups);

} //namespace SANS
