// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef DISTANCEFUNCTION_KDVERTEX_H
#define DISTANCEFUNCTION_KDVERTEX_H

#include "tools/SANSnumerics.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

namespace SANS
{

template<class PhysDim, int TopoTraceD>
class DistanceFunction_KDVertex
{
public:
  static const int D = PhysDim::D;
  typedef Real value_type;

  typedef DLA::VectorS<D,Real> VectorX; // coordinates vector
  typedef DLA::VectorS<TopoTraceD,Real> RefCoordTraceType;

  DistanceFunction_KDVertex(const VectorX& X, const RefCoordTraceType& sRref0, int cellgroup, int cellelem) :
    X_(X), sRref0_(sRref0), cellgroup_(cellgroup), cellelem_(cellelem) {}

  DistanceFunction_KDVertex(const DistanceFunction_KDVertex& V) :
    X_(V.X_), sRref0_(V.sRref0_), cellgroup_(V.cellgroup_), cellelem_(V.cellelem_) {}

  DistanceFunction_KDVertex& operator=(const DistanceFunction_KDVertex& V)
  {
    X_ = V.X_;
    sRref0_ = V.sRref0_;
    cellgroup_ = V.cellgroup_;
    cellelem_ = V.cellelem_;
    return *this;
  }

  inline value_type operator[](size_t const n) const { return X_[n]; }

  const VectorX& getCoordinates() const { return X_; }
  const RefCoordTraceType& getRefCoordinate() const { return sRref0_; }

  void getInfo(int& cellgroup, int& cellelem) const
  {
    cellgroup = cellgroup_;
    cellelem  = cellelem_;
  }

protected:
  VectorX X_;
  RefCoordTraceType sRref0_;
  int cellgroup_;
  int cellelem_;
};

}

#endif //DISTANCEFUNCTION_KDVERTEX_H
