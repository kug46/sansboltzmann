// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef DISTANCEFUNCTIONGRADIENT_H_
#define DISTANCEFUNCTIONGRADIENT_H_

#include "Field/Field.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

namespace SANS
{

//----------------------------------------------------------------------------//

/*
 * A function for computing the distance function
 * Populates a given solution field with the distance to the nearest boundary point
 */
template<class PhysDim, class TopoDim>
void DistanceFunctionGradient( const Field<PhysDim, TopoDim, Real>& distfld,
                               Field<PhysDim, TopoDim, DLA::VectorS<PhysDim::D,Real>>& graddistfld );

}

#endif /* DISTANCEFUNCTIONGRADIENT_H_ */
