// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "DistanceFunction.h"

#include <limits>
#include <iomanip> // set::precision

#include <kdtree++/kdtree.hpp>

#include "tools/timer.h"
#include "tools/minmax.h"

#include "Topology/ElementTopology.h"
#include "Quadrature/Quadrature.h"

#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/LagrangeDOFMap.h"
#include "BasisFunction/BasisFunctionNode.h"
#include "BasisFunction/BasisFunctionLine_Lagrange.h"
#include "BasisFunction/BasisFunctionArea_Quad_Lagrange.h"
#include "BasisFunction/BasisFunctionArea_Triangle_Lagrange.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron_Lagrange.h"
#include "BasisFunction/BasisFunctionVolume_Hexahedron_Lagrange.h"

#include "DistanceSolver.h"
#include "DistanceFunction_KDVertex.h"

#include "Field/XField_BoundaryTrace.h"

#include "Field/XFieldLine.h"
#include "Field/XFieldArea.h"
#include "Field/XFieldVolume.h"

#include "Field/XFieldLine_BoundaryTrace.h"
#include "Field/XFieldArea_BoundaryTrace.h"
#include "Field/XFieldVolume_BoundaryTrace.h"

#include "Field/FieldLine.h"
#include "Field/FieldArea.h"
#include "Field/FieldVolume.h"

#include "Field/tools/GroupFunctorType.h"
#include "Field/Tuple/FieldTuple.h"

#include "Field/tools/for_each_CellGroup.h"
#include "Field/tools/for_each_BoundaryTraceGroup.h"
#include "Field/tools/for_each_BoundaryTraceGroup_Cell.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/InverseLU.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/reduce.hpp>
#endif

namespace SANS
{

//----------------------------------------------------------------------------//
//  Populates the tree with the physical coordinates of the quadrature points on the boundary of the given field
//
template<class PhysDim, class TopoDim>
class PopulateTree_Distance : public GroupFunctorBoundaryTraceType< PopulateTree_Distance<PhysDim, TopoDim> >
{
public:
  static const int D = PhysDim::D;

  typedef DistanceFunction_KDVertex<PhysDim, MAX(TopoDim::D-1,1)> KDVertex;
  typedef KDTree::KDTree<D, KDVertex> KDTreeType;
  typedef typename KDVertex::VectorX VectorX;

  PopulateTree_Distance( KDTreeType& tree, const int nLocalBTraceGroups ) :
    tree_(tree), nLocalBTraceGroups_(nLocalBTraceGroups) {}

  std::size_t nBoundaryTraceGroups() const          { return nLocalBTraceGroups_; }
  std::size_t boundaryTraceGroup(const int n) const { return n;                   }

  //----------------------------------------------------------------------------//
  // Integration function that integrates each element in the trace group
  template <class TopologyTrace>
  void
  apply( const typename XField_BoundaryTrace<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
         const int traceGroupGlobal )
  {
    typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldClass;
    typedef typename TopologyTrace::TopoDim TopoDimTrace;

    // element field variables
    ElementXFieldClass xfldElem( xfldTrace.basis() );

    typedef typename ElementXField<PhysDim, TopoDimTrace, TopologyTrace>::VectorX VectorX;
    typedef typename ElementXField<PhysDim, TopoDimTrace, TopologyTrace>::RefCoordType RefCoordType;

    RefCoordType Ref;  // reference-element coordinates
    VectorX X;         // physical coordinates

    Quadrature<TopoDimTrace, TopologyTrace> quadrature( 2*xfldTrace.order() );
    const int nquad = quadrature.nQuadrature();

    // loop over elements within group
    const int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid DOFs to element
      xfldTrace.getElement( xfldElem, elem );

      // loop over quadrature points
      for (int iquad = 0; iquad < nquad; iquad++)
      {
        quadrature.coordinates( iquad, Ref );

        // physical coordinates
        xfldElem.coordinates( Ref, X );

        tree_.insert( KDVertex(X, Ref, traceGroupGlobal, elem) );
      }
    }
  }

protected:
  KDTreeType& tree_;
  const int nLocalBTraceGroups_;
};


//----------------------------------------------------------------------------//
// Explicitly sets Lagrange nodes on wall boundaries to zero distance
template<class PhysDim>
class SetWallBoundary_Distance :
    public GroupFunctorBoundaryCellType<SetWallBoundary_Distance<PhysDim>>
{
public:

  explicit SetWallBoundary_Distance( const std::vector<int>& boundaryTraceGroups ) :
    boundaryTraceGroups_(boundaryTraceGroups) {}

  std::size_t nBoundaryGroups() const { return boundaryTraceGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return boundaryTraceGroups_[n]; }

  //----------------------------------------------------------------------------//
  // Distribution function that redistributes the error in each cell group
  template< class TopologyTrace, class TopologyL>
  void
  apply( const typename Field<PhysDim, typename TopologyL::TopoDim, Real>::template FieldCellGroupType<TopologyL>& dfldCellL,
         const int cellGroupGlobalL,
         const typename XField<PhysDim, typename TopologyL::TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
         const int traceGroupGlobal)
  {
    // Cell Group Types
    typedef typename Field<PhysDim, typename TopologyL::TopoDim, Real>::template FieldCellGroupType<TopologyL> DFieldCellGroupTypeL;
    typedef typename DFieldCellGroupTypeL::template ElementType<> ElementDFieldCellClassL;

    // Construct the element
    ElementDFieldCellClassL dfldElemL( dfldCellL.basis() );
    int dorder = dfldElemL.order();

    // loop over elements
    const int nElem = xfldTrace.nElem();
    for (int elem = 0; elem < nElem; elem++)
    {
      int elemL = xfldTrace.getElementLeft( elem );
      CanonicalTraceToCell canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // copy global distance DOFs to element
      dfldCellL.getElement( dfldElemL, elemL );

      // set all DOFs on wall boundaries explicitly to zero
      std::vector<int> map = LagrangeDOFMap<TopologyL>::getCanonicalTraceMap( canonicalTraceL.trace, dorder );

      for (std::size_t i = 0; i < map.size(); i++)
        dfldElemL.DOF(map[i]) = 0;

      // save the element
      const_cast<DFieldCellGroupTypeL&>(dfldCellL).setElement( dfldElemL, elemL );
    }
  }
protected:
  const std::vector<int> boundaryTraceGroups_;
};


//----------------------------------------------------------------------------//
//  Populates the DOFs of the distance field
//
template<class PhysDim, class TopoDim>
class PopulateDistanceField : public GroupFunctorCellType< PopulateDistanceField<PhysDim, TopoDim> >
{
public:

  static const int D = PhysDim::D;

  typedef Real T; //solution data type: Real (distance)
  typedef DistanceFunction_KDVertex<PhysDim, MAX(TopoDim::D-1,1)> KDVertex;
  typedef KDTree::KDTree<D, KDVertex> KDTreeType;
  typedef typename KDVertex::VectorX VectorX;

  PopulateDistanceField( const KDTreeType& tree, const XField_BoundaryTrace<PhysDim, TopoDim>& xfld_bnd,
                         const int nCellGroups, Real& solveTime) :
    tree_(tree), solver_(xfld_bnd), xfld_bnd_(xfld_bnd), nCellGroups_(nCellGroups), solveTime_(solveTime) {}

  std::size_t nCellGroups() const          { return nCellGroups_; }
  std::size_t cellGroup(const int n) const { return n;            }

//----------------------------------------------------------------------------//
  // Function that loops over each element in the cell group
  template <class Topology>
  void
  apply( const typename FieldTuple<XField<PhysDim, TopoDim>, Field<PhysDim, TopoDim, T>, TupleClass<>>::
                        template FieldCellGroupType<Topology>& fldsCell,
         const int cellGroupGlobal )
  {
    typedef typename XField<PhysDim, TopoDim   >::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field <PhysDim, TopoDim, T>::template FieldCellGroupType<Topology> QFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

    typedef typename ElementXField<PhysDim, TopoDim, Topology>::VectorX VectorX;

    const XFieldCellGroupType& xfldCell    = get<0>(fldsCell);
          QFieldCellGroupType& distfldCell = const_cast<QFieldCellGroupType&>( get<1>(fldsCell) );

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass distfldElem( distfldCell.basis() );

    typedef typename ElementXField<PhysDim, TopoDim, Topology>::VectorX VectorX;
    typedef typename ElementXField<PhysDim, TopoDim, Topology>::RefCoordType RefCoordType;
    typedef DLA::VectorS<MAX(TopoDim::D-1,1), Real> RefCoordTraceType;

    RefCoordType Ref;        // reference-element coordinates
    VectorX X;               // physical coordinates
    RefCoordTraceType sRef0; // reference coordinate on the closest trace element

    std::vector<DLA::VectorS<TopoDim::D,Real>> sRef;          // Lagrange point reference coordinates
    LagrangeNodes<Topology>::get(distfldCell.order(), sRef);

    const int nElem = xfldCell.nElem();

    // loop over elements within group
    for (int elem = 0; elem < nElem; elem++)
    {
      // copy global grid DOFs to element
      xfldCell.getElement( xfldElem, elem );
      distfldCell.getElement( distfldElem, elem );

      for (std::size_t i = 0; i < sRef.size(); i++)
      {
        // don't recompute
        if ( distfldElem.DOF(i) != -1 ) continue;
        distfldElem.DOF(i) = std::numeric_limits<Real>::max();

        // physical coordinates at the Lagrange node
        xfldElem.coordinates( sRef[i], X );

        // get the closest boundary point for this quadrature point from KDtree
        KDVertex targetVertex(X, 0, 0, 0); //dummy sRef, cellgroup, cellelem

        std::pair<typename KDTreeType::const_iterator,double> it = tree_.find_nearest(targetVertex);

        int btracegroup_nearest, elem_nearest;
        it.first->getInfo(btracegroup_nearest, elem_nearest);

        Real distmin = solver_.solve(X, btracegroup_nearest, elem_nearest, it.first->getRefCoordinate());

        for (int btracegroup = 0; btracegroup < xfld_bnd_.nBoundaryTraceGroups(); btracegroup++)
        {
          int nElem_bnd = xfld_bnd_.getBoundaryTraceGroupBase(btracegroup).nElem();
          for (int elem_bnd = 0; elem_bnd < nElem_bnd; elem_bnd++)
          {
            // only consider trace elements with bounding boxes closer than the current minimum distance
            // or encloses the point
            const BoundingBox<PhysDim>& bbox = xfld_bnd_.boundaryTraceElemBBox(btracegroup, elem_bnd);

            if (bbox.distance_to(X) <= distmin || bbox.encloses(X))
            {
              Real dist = solver_.solve(X, btracegroup, elem_bnd);

              distmin = std::min(distmin, dist);
            }
          }
        }

        distfldElem.DOF(i) = distmin;
      }

      // set the interpolated distance
      distfldCell.setElement( distfldElem, elem );
    }

    solveTime_ = solver_.solveTime();
  }

protected:
  const KDTreeType& tree_;
  const DistanceSolver<PhysDim, TopoDim> solver_;
  const XField_BoundaryTrace<PhysDim, TopoDim>& xfld_bnd_;
  const int nCellGroups_;
  Real& solveTime_;
};

/*---------------------------------------------------------------------------------------*/
template<class PhysDim, class TopoDim>
void DistanceFunction( Field<PhysDim, TopoDim, Real>& distfld, const std::vector<int>& btraceGroups, bool verbose)
{
  static const int D = PhysDim::D;

  typedef DistanceFunction_KDVertex<PhysDim, MAX(TopoDim::D-1,1)> KDVertex;
  typedef KDTree::KDTree<D, KDVertex> KDTreeType;

  // Distance field is interpolated, so we can only use Lagrange basis functions
  for (int i = 0; i < distfld.nCellGroups(); i++)
    SANS_ASSERT( distfld.getCellGroupBase(i).basisCategory() == BasisFunctionCategory_Lagrange );

  const XField<PhysDim, TopoDim>& xfld = distfld.getXField();

  // mark all distqnces as not set
  for (int n = 0; n < distfld.nDOF(); n++)
    distfld.DOF(n) = -1;

  if (xfld.comm()->rank() == 0 && verbose)
    std::cout << "Constructing global boundary groups for distance function." << std::endl;

  XField_BoundaryTrace<PhysDim, TopoDim> xfld_bnd(xfld, btraceGroups);

  KDTreeType tree;

  const int nLocalBTraceGroups = xfld_bnd.nBoundaryTraceGroups();

  timer disttime;

  if (xfld.comm()->rank() == 0 && verbose)
    std::cout << "Computing distance function..." << std::endl;

  //Insert all boundary quadrature points in the KDtree
  for_each_BoundaryTraceGroup<TopoDim>::apply( PopulateTree_Distance<PhysDim, TopoDim>(tree, nLocalBTraceGroups), xfld_bnd );
  tree.optimize();

  const int nCellGroups = distfld.nCellGroups();

  timer distanceTimer;

  //Compute and distance solution on distfld
  Real solveTime = 0;
  for_each_CellGroup<TopoDim>::apply(
      PopulateDistanceField<PhysDim, TopoDim>(tree, xfld_bnd, nCellGroups, solveTime), (xfld, distfld) );

  Real distanceTime = distanceTimer.elapsed();

  //Set all DOFs on wall boundaries explicitly to zero
  for_each_BoundaryTraceGroup_Cell<TopoDim>::apply( SetWallBoundary_Distance<PhysDim>(btraceGroups), distfld );

  // wait for all processors to finish computing their distance function
  xfld.comm()->barrier();

#ifdef SANS_MPI
  Real distanceTimeGlobal = 0;
  boost::mpi::reduce(*xfld.comm(), distanceTime, distanceTimeGlobal, std::plus<Real>(), 0);
  distanceTime = distanceTimeGlobal;

  Real solveTimeGlobal = 0;
  boost::mpi::reduce(*xfld.comm(), solveTime, solveTimeGlobal, std::plus<Real>(), 0);
  solveTime = solveTimeGlobal;
#endif

  if (xfld.comm()->rank() == 0 && verbose)
  {
    std::cout << "Distance function Wall time: " << std::setprecision(5) << disttime.elapsed() << " s" << std::endl;
    std::cout << "                   CPU time: " << std::setprecision(5) << distanceTime << " s" << std::endl;
    std::cout << "     KDtree search CPU time: " << std::setprecision(5) << distanceTime - solveTime << " s" << std::endl;
    std::cout << "   Nonlinear solve CPU time: " << std::setprecision(5) << solveTime << " s" << std::endl;
  }
}

//Explicit instantiations
template void DistanceFunction<PhysD1, TopoD1>( Field<PhysD1, TopoD1, Real>& distfld, const std::vector<int>& btraceGroups, bool verbose);
template void DistanceFunction<PhysD2, TopoD2>( Field<PhysD2, TopoD2, Real>& distfld, const std::vector<int>& btraceGroups, bool verbose);
template void DistanceFunction<PhysD3, TopoD3>( Field<PhysD3, TopoD3, Real>& distfld, const std::vector<int>& btraceGroups, bool verbose);

} //namespace SANS
