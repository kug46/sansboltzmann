// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "DistanceFunction.h"

#include <limits>
#include <iomanip> // set::precision

#include <kdtree++/kdtree.hpp>

#include "tools/timer.h"
#include "tools/minmax.h"

#include "Topology/ElementTopology.h"
#include "Quadrature/Quadrature.h"

#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/LagrangeDOFMap.h"
#include "BasisFunction/BasisFunctionNode.h"
#include "BasisFunction/BasisFunctionLine_Lagrange.h"
#include "BasisFunction/BasisFunctionArea_Quad_Lagrange.h"
#include "BasisFunction/BasisFunctionArea_Triangle_Lagrange.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron_Lagrange.h"
#include "BasisFunction/BasisFunctionVolume_Hexahedron_Lagrange.h"

#include "DistanceSolver.h"
#include "DistanceFunction_KDVertex.h"

#include "Field/XField_BoundaryTrace.h"

#include "Field/XFieldLine.h"
#include "Field/XFieldArea.h"
#include "Field/XFieldVolume.h"

#include "Field/XFieldLine_BoundaryTrace.h"
#include "Field/XFieldArea_BoundaryTrace.h"
#include "Field/XFieldVolume_BoundaryTrace.h"

#include "Field/FieldLine.h"
#include "Field/FieldArea.h"
#include "Field/FieldVolume.h"

#include "Field/tools/GroupFunctorType.h"
#include "Field/Tuple/FieldTuple.h"

#include "Field/tools/for_each_CellGroup.h"
#include "Field/tools/for_each_BoundaryTraceGroup.h"
#include "Field/tools/for_each_BoundaryTraceGroup_Cell.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/InverseLU.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/reduce.hpp>
#endif

namespace SANS
{

//----------------------------------------------------------------------------//
//  Populates the DOFs of the distance field
//
template<class PhysDim, class TopoD>
class PopulateDistanceGradientField : public GroupFunctorCellType< PopulateDistanceGradientField<PhysDim, TopoD> >
{
public:

  static const int D = PhysDim::D;
  typedef Real T; //solution data type: Real (distance)

  PopulateDistanceGradientField( const int nCellGroups ) :
    nCellGroups_(nCellGroups) {}

  std::size_t nCellGroups() const          { return nCellGroups_; }
  std::size_t cellGroup(const int n) const { return n;            }

//----------------------------------------------------------------------------//
  // Function that loops over each element in the cell group
  template <class Topology, class TopoDim>
  void
  apply( const typename FieldTuple< FieldTuple<XField<PhysDim, TopoDim>, Field<PhysDim, TopoDim, T>, TupleClass<0>>,
                                   Field<PhysDim, TopoDim, DLA::VectorS<D,T>>, TupleClass<0>>::template FieldCellGroupType<Topology>& fldsCell,
         const int cellGroupGlobal )
  {

    typedef typename DLA::VectorS<D,T> VectorX;

    typedef typename XField<PhysDim, TopoDim   >::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field <PhysDim, TopoDim, T>::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename Field<PhysDim, TopoDim, VectorX>::template FieldCellGroupType<Topology> GFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;
    typedef typename GFieldCellGroupType::template ElementType<> ElementGFieldClass;

    const XFieldCellGroupType& xfldCell    = get<0>(fldsCell);
    const QFieldCellGroupType& distfldCell = get<1>(fldsCell);
    GFieldCellGroupType& gdistfldCell = const_cast<GFieldCellGroupType&>( get<2>(fldsCell) );

    // element field variables
    ElementXFieldClass xfldElem(xfldCell.basis());
    ElementQFieldClass distfldElem( distfldCell.basis() );
    ElementGFieldClass gdistfldElem( gdistfldCell.basis() );

    const int nElem = distfldCell.nElem();

    typedef typename ElementXField<PhysDim, TopoDim, Topology>::RefCoordType RefCoordType;

    RefCoordType Ref;        // reference-element coordinates
    VectorX X;               // physical coordinates

    std::vector<DLA::VectorS<TopoDim::D,Real>> sRef;
    LagrangeNodes<Topology>::get(distfldCell.order(), sRef);

    int nDOF = sRef.size();
    VectorX graddist;
    std::vector< DLA::VectorS<TopoDim::D,Real> > gradphi(nDOF);  // Basis gradient

    // loop over elements within group
    for (int elem = 0; elem < nElem; elem++)
    {
      // copy global grid DOFs to element
      xfldCell.getElement( xfldElem, elem );
      distfldCell.getElement( distfldElem, elem );
      gdistfldCell.getElement( gdistfldElem, elem );

      for (std::size_t i = 0; i < sRef.size(); i++)
      {
        xfldElem.evalBasisGradient( sRef[i], distfldElem, gradphi.data(), nDOF );
        distfldElem.evalFromBasis( gradphi.data(), nDOF, graddist );
        gdistfldElem.DOF(i) = graddist;
      }

      // set the interpolated distance gradient
      gdistfldCell.setElement( gdistfldElem, elem );
    }
  }


protected:
  const int nCellGroups_;
};

/*---------------------------------------------------------------------------------------*/
template<class PhysDim, class TopoDim>
void DistanceFunctionGradient( const Field<PhysDim, TopoDim, Real>& distfld,
                               Field<PhysDim, TopoDim, DLA::VectorS<PhysDim::D, Real>>& graddistfld )
{
//  static const int D = PhysDim::D;

  SANS_ASSERT( &distfld.getXField() == &graddistfld.getXField() );

  const XField<PhysDim, TopoDim>& xfld = distfld.getXField();

  // Distance field is interpolated, so we can only use Lagrange basis functions
  for (int i = 0; i < distfld.nCellGroups(); i++)
  {
    SANS_ASSERT( distfld.getCellGroupBase(i).basisCategory() == BasisFunctionCategory_Lagrange );
    SANS_ASSERT( graddistfld.getCellGroupBase(i).basisCategory() == BasisFunctionCategory_Lagrange );
  }

  timer distanceTimer;

  const int nCellGroups = distfld.nCellGroups();

  //Compute and distance solution on distfld
  Real solveTime = 0;
  for_each_CellGroup<TopoDim>::apply(
      PopulateDistanceGradientField<PhysDim, TopoDim>( nCellGroups ), (xfld, distfld, graddistfld) );

  Real distanceTime = distanceTimer.elapsed();

  // wait for all processors to finish computing their distance function
  xfld.comm()->barrier();

#ifdef SANS_MPI
  Real distanceTimeGlobal = 0;
  boost::mpi::reduce(*xfld.comm(), distanceTime, distanceTimeGlobal, std::plus<Real>(), 0);
  distanceTime = distanceTimeGlobal;

  Real solveTimeGlobal = 0;
  boost::mpi::reduce(*xfld.comm(), solveTime, solveTimeGlobal, std::plus<Real>(), 0);
  solveTime = solveTimeGlobal;
#endif

}

//Explicit instantiations
template void DistanceFunctionGradient<PhysD1, TopoD1>( const Field<PhysD1, TopoD1, Real>& distfld,
                                                              Field<PhysD1, TopoD1, DLA::VectorS<1,Real>>& gdistfld);
template void DistanceFunctionGradient<PhysD2, TopoD2>( const Field<PhysD2, TopoD2, Real>& distfld,
                                                              Field<PhysD2, TopoD2, DLA::VectorS<2,Real>>& gdistfld);
template void DistanceFunctionGradient<PhysD3, TopoD3>( const Field<PhysD3, TopoD3, Real>& distfld,
                                                              Field<PhysD3, TopoD3, DLA::VectorS<3,Real>>& gdistfld);

} //namespace SANS
