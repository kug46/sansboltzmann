// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef DISTANCEFUNCTION_H_
#define DISTANCEFUNCTION_H_

#include "Field/Field.h"

namespace SANS
{

//----------------------------------------------------------------------------//

/*
 * A function for computing the distance function
 * Populates a given solution field with the distance to the nearest boundary point
 */
template<class PhysDim, class TopoDim>
void DistanceFunction( Field<PhysDim, TopoDim, Real>& distfld, const std::vector<int>& btraceGroups, bool verbose = true );

}

#endif /* DISTANCEFUNCTION_H_ */
