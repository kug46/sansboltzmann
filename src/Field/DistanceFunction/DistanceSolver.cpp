// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "DistanceSolver.h"

#include <limits>

#include "tools/timer.h"

#include "BasisFunction/BasisFunctionCategory.h"
#include "Quadrature/Quadrature.h"

#include "Field/Element/RefCoordSolver.h"

#include "Field/XFieldLine_BoundaryTrace.h"
#include "Field/XFieldArea_BoundaryTrace.h"
#include "Field/XFieldVolume_BoundaryTrace.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "BasisFunction/LagrangeDOFMap.h"
#include "BasisFunction/BasisFunctionNode.h"
#include "BasisFunction/BasisFunctionLine_Lagrange.h"
#include "BasisFunction/BasisFunctionArea_Quad_Lagrange.h"
#include "BasisFunction/BasisFunctionArea_Triangle_Lagrange.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron_Lagrange.h"
#include "BasisFunction/BasisFunctionVolume_Hexahedron_Lagrange.h"

namespace SANS
{

template<class PhysDim, class TopoDim>
template<class TopoTrace>
Real
DistanceSolverBase<PhysDim, TopoDim>::solve(const VectorX& X, const int btracegroup, const int elem, const RefCoordTraceType& sRef0 ) const
{
  typedef typename XField_BoundaryTrace<PhysDim, TopoDim>::template FieldTraceGroupType<TopoTrace> XFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
  typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

  timer solvetime;

  const XFieldTraceGroupType& xfldTraceGroup = xfld_.template getBoundaryTraceGroupGlobal<TopoTrace>(btracegroup);

  ElementXFieldTraceClass xfldElem( xfldTraceGroup.basis() );

  // copy global grid DOFs to element
  xfldTraceGroup.getElement( xfldElem, elem );

  RefCoordSolver<PhysDim, TopoDimTrace, TopoTrace> refcoord_solver(xfldElem); //nonlinear solver to find reference coords

  // reference-element coordinates (use sRef0 as the initial guess)
  RefCoordTraceType sRef = sRef0;

  // compute the distance to the quadrature point
  VectorX X0;
  xfldElem.coordinates( sRef0, X0 );
  VectorX dX0 = X - X0;
  Real dist0 = sqrt(dot(dX0, dX0));

  //Perform a nonlinear solve to find the reference coordinates of xfldElem
  //that are closest to the physical coordinate given by X
  refcoord_solver.solve(X, sRef);

  //Evaluate the coordinates at the reference coordinate found above
  VectorX Xsolved;
  xfldElem.coordinates( sRef, Xsolved );

  VectorX dX = X - Xsolved;
  Real distsolved = sqrt(dot(dX, dX));

  solveTime_ += solvetime.elapsed();

  //return the smallest distance just in case the solve did not go so well
  return std::min(dist0, distsolved);
}

template<class PhysDim, class TopoDim>
template<class TopoTrace>
Real
DistanceSolverBase<PhysDim, TopoDim>::solve(const VectorX& X, const int btracegroup, const int elem) const
{
  typedef typename XField_BoundaryTrace<PhysDim, TopoDim>::template FieldTraceGroupType<TopoTrace> XFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;

  timer solvetime;

  const XFieldTraceGroupType& xfldTraceGroup = xfld_.template getBoundaryTraceGroupGlobal<TopoTrace>(btracegroup);

  ElementXFieldTraceClass xfldElem( xfldTraceGroup.basis() );

  // copy global grid DOFs to element
  xfldTraceGroup.getElement( xfldElem, elem );

  Real dist = std::numeric_limits<Real>::max();
  Real distmin = std::numeric_limits<Real>::max();

  VectorX X0, dX;
  RefCoordTraceType sRef0;

  std::vector<DLA::VectorS<MAX(TopoTrace::TopoDim::D,1),Real>> sRef;
  LagrangeNodes<TopoTrace>::get(LagrangeNodes<TopoTrace>::PMax, sRef);

  // loop over Lagrange nodes and pick the closest as the initial guess
  for (std::size_t i = 0; i < sRef.size(); i++)
  {
    // physical coordinates at the Lagrange node
    xfldElem.coordinates( sRef[i], X0 );

    dX = X - X0;
    dist = sqrt(dot(dX,dX));

    if (dist < distmin)
    {
      sRef0 = sRef[i];
      distmin = dist;
    }
  }

  solveTime_ += solvetime.elapsed();

  // perform the non-linear solve
  return this->template solve<TopoTrace>(X, btracegroup, elem, sRef0);
}


template<class PhysDim>
Real
DistanceSolver<PhysDim, TopoD1>::solve(const VectorX& X, const int btracegroup, const int elem, const RefCoordTraceType& sRef0 ) const
{
  return BaseType::template solve<Node>(X, btracegroup, elem, sRef0);
}

template<class PhysDim>
Real
DistanceSolver<PhysDim, TopoD1>::solve(const VectorX& X, const int btracegroup, const int elem ) const
{
  return BaseType::template solve<Node>(X, btracegroup, elem);
}


template<class PhysDim>
Real
DistanceSolver<PhysDim, TopoD2>::solve(const VectorX& X, const int btracegroup, const int elem, const RefCoordTraceType& sRef0 ) const
{
  return BaseType::template solve<Line>(X, btracegroup, elem, sRef0);
}

template<class PhysDim>
Real
DistanceSolver<PhysDim, TopoD2>::solve(const VectorX& X, const int btracegroup, const int elem ) const
{
  return BaseType::template solve<Line>(X, btracegroup, elem);
}


template<class PhysDim>
Real
DistanceSolver<PhysDim, TopoD3>::solve(const VectorX& X, const int btracegroup, const int elem, const RefCoordTraceType& sRef0 ) const
{
  if ( this->xfld_.getBoundaryTraceGroupBase(btracegroup).topoTypeID() == typeid(Triangle) )
  {
    return BaseType::template solve<Triangle>(X, btracegroup, elem, sRef0);
  }
  else if ( this->xfld_.getBoundaryTraceGroupBase(btracegroup).topoTypeID() == typeid(Quad) )
  {
    return BaseType::template solve<Quad>(X, btracegroup, elem, sRef0);
  }
  else
    SANS_DEVELOPER_EXCEPTION( "DistanceSolver<PhysDim, TopoD3>::solve - Unknown trace topology." );

  return 0.0;
}

template<class PhysDim>
Real
DistanceSolver<PhysDim, TopoD3>::solve(const VectorX& X, const int btracegroup, const int elem ) const
{
  if ( this->xfld_.getBoundaryTraceGroupBase(btracegroup).topoTypeID() == typeid(Triangle) )
  {
    return BaseType::template solve<Triangle>(X, btracegroup, elem);
  }
  else if ( this->xfld_.getBoundaryTraceGroupBase(btracegroup).topoTypeID() == typeid(Quad) )
  {
    return BaseType::template solve<Quad>(X, btracegroup, elem);
  }
  else
    SANS_DEVELOPER_EXCEPTION( "DistanceSolver<PhysDim, TopoD3>::solve - Unknown trace topology." );

  return 0.0;
}


//Explicit instantiations
template class DistanceSolver<PhysD1, TopoD1>;
template class DistanceSolver<PhysD2, TopoD2>;
template class DistanceSolver<PhysD3, TopoD3>;

} //namespace SANS
