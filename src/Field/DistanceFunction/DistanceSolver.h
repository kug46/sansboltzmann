// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef DISTANCESOLVER_H_
#define DISTANCESOLVER_H_

#include "Topology/ElementTopology.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "Field/XField_BoundaryTrace.h"

namespace SANS
{

template<class PhysDim, class TopoDim>
class DistanceSolver;

template<class PhysDim, class TopoDim>
class DistanceSolverBase
{
public:

  static const int D = PhysDim::D;
  typedef DLA::VectorS<D,Real> VectorX; // coordinates vector
  typedef DLA::VectorS<MAX(TopoDim::D-1,1),Real> RefCoordTraceType; // trace referece coordinate

  explicit DistanceSolverBase( const XField_BoundaryTrace<PhysDim, TopoDim>& xfld ) : xfld_(xfld), solveTime_(0) {}

  template<class TopoTrace>
  Real solve( const VectorX& X, const int btracegroup, const int elem, const RefCoordTraceType& sRef0 ) const;

  template<class TopoTrace>
  Real solve( const VectorX& X, const int btracegroup, const int elem ) const;

  Real solveTime() const { return solveTime_; }

protected:
  const XField_BoundaryTrace<PhysDim, TopoDim>& xfld_;
  mutable Real solveTime_;
};

template<class PhysDim>
class DistanceSolver<PhysDim, TopoD1> : public DistanceSolverBase<PhysDim, TopoD1>
{
public:
  typedef DistanceSolverBase<PhysDim, TopoD1> BaseType;
  using BaseType::D;
  typedef DLA::VectorS<D,Real> VectorX; // coordinates vector
  typedef DLA::VectorS<1,Real> RefCoordTraceType; // trace referece coordinate

  explicit DistanceSolver( const XField_BoundaryTrace<PhysDim, TopoD1>& xfld ) : BaseType(xfld) {}

  Real solve( const VectorX& X, const int btracegroup, const int elem, const RefCoordTraceType& sRef0 ) const;
  Real solve( const VectorX& X, const int btracegroup, const int elem ) const;
};


template<class PhysDim>
class DistanceSolver<PhysDim, TopoD2> : public DistanceSolverBase<PhysDim, TopoD2>
{
public:
  typedef DistanceSolverBase<PhysDim, TopoD2> BaseType;
  using BaseType::D;
  typedef DLA::VectorS<D,Real> VectorX; // coordinates vector
  typedef DLA::VectorS<TopoD1::D,Real> RefCoordTraceType; // trace referece coordinate

  explicit DistanceSolver( const XField_BoundaryTrace<PhysDim, TopoD2>& xfld ) : BaseType(xfld) {}

  Real solve( const VectorX& X, const int btracegroup, const int elem, const RefCoordTraceType& sRef0 ) const;
  Real solve( const VectorX& X, const int btracegroup, const int elem ) const;
};

template<class PhysDim>
class DistanceSolver<PhysDim, TopoD3> : public DistanceSolverBase<PhysDim, TopoD3>
{
public:
  typedef DistanceSolverBase<PhysDim, TopoD3> BaseType;
  using BaseType::D;
  typedef DLA::VectorS<D,Real> VectorX; // coordinates vector
  typedef DLA::VectorS<TopoD2::D,Real> RefCoordTraceType; // trace referece coordinate

  explicit DistanceSolver( const XField_BoundaryTrace<PhysDim, TopoD3>& xfld ) : BaseType(xfld) {}

  Real solve( const VectorX& X, const int btracegroup, const int elem, const RefCoordTraceType& sRef0 ) const;
  Real solve( const VectorX& X, const int btracegroup, const int elem ) const;
};

}

#endif /* DISTANCESOLVER_H_ */
