// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define FIELD_DG_TRACEBASE_INSTANTIATE
#include "Field_DG_TraceBase_impl.h"

#define FIELDLINE_DG_TRACE_INSTANTIATE
#include "FieldLine_DG_Trace_impl.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "XFieldLine.h"

#include "UserVariables/BoltzmannNVar.h"
namespace SANS
{

template class Field_DG_Trace< PhysD2, TopoD1, Real >;

//template class Field_DG_Trace< PhysD2, TopoD1, DLA::VectorS<1, Real> >;
template class Field_DG_Trace< PhysD2, TopoD1, DLA::VectorS<2, Real> >;
template class Field_DG_Trace< PhysD2, TopoD1, DLA::VectorS<6, Real> >;
// For Boltzmann Implementation
template class Field_DG_Trace< PhysD2, TopoD1, DLA::VectorS<9, Real> >;
//template class Field_DG_Trace< PhysD2, TopoD1, DLA::VectorS<13, Real> >;
//template class Field_DG_Trace< PhysD2, TopoD1, DLA::VectorS<16, Real> >;
template class Field_DG_Trace< PhysD2, TopoD1, DLA::VectorS<NVar, Real> >;

}
