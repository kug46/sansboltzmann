// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELDLINE_H
#define XFIELDLINE_H

#include "XField.h"
#include "XFieldNode_Traits.h"
#include "XFieldLine_Traits.h"
#include "FieldAssociativity.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Topologically 1D grid
//----------------------------------------------------------------------------//

template<class PhysDim>
struct XFieldTraits<PhysDim, TopoD1>
{
  typedef TopoD1 TopoDim;
  static const int D = PhysDim::D;                 // physical dimensions

  typedef DLA::VectorS<D,Real> VectorX;   // coordinates vector
  typedef VectorX T;

  typedef typename XFieldGroupNodeTraits<PhysDim, TopoD1>::FieldBase FieldTraceGroupBase;
  typedef typename XFieldGroupLineTraits<PhysDim, TopoD1>::FieldBase FieldCellGroupBase;

  template<class Topology>
  using FieldTraceGroupType = ElementConnectivity< XFieldGroupNodeTraits<PhysDim, TopoD1> >;

  template<class Topology>
  using FieldCellGroupType = FieldAssociativity< XFieldGroupLineTraits<PhysDim, TopoD1> >;
};

} // namespace SANS

#endif //XFIELDLINE_H
