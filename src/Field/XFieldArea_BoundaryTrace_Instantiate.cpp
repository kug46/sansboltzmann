// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField_BoundaryTrace.h"
#include "XFieldArea_BoundaryTrace.h"

#define FIELDBASE_INSTANTIATE
#include "FieldBase_impl.h"

#define XFIELD_BOUNDARYTRACE_INSTANTIATE
#include "XField_BoundaryTrace_impl.h"

namespace SANS
{

template<class PhysDim, class TopoDim>
XField_BoundaryTrace<PhysDim, TopoDim>::
XField_BoundaryTrace( const XFieldType& xfld, const std::vector<int>& boundaryTraceGroups, bool global ) : xfld_(xfld)
{
  std::map<int,VectorX> bndDOF;
  std::map<int,int> localDOFmap;

  this->boundaryTraceBBox_.resize( boundaryTraceGroups.size() );

  for (std::size_t i = 0; i < boundaryTraceGroups.size(); i++)
    this->template getBoundaryTraceDOFs<Line>( bndDOF, boundaryTraceGroups[i] );

  createBoundaryTraceDOFs( bndDOF, localDOFmap, global );

  this->resizeBoundaryTraceGroups(boundaryTraceGroups.size());

  for (std::size_t i = 0; i < boundaryTraceGroups.size(); i++)
    this->template createBoundaryTraceElems<Line>(i, localDOFmap, boundaryTraceGroups[i], global );
}

// Explicit instantiations
template class FieldBase< XFieldTraits_BoundaryTrace<PhysD2, TopoD2> >;

template class XField_BoundaryTrace<PhysD2, TopoD2>;

} // namespace SANS
