// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDAREA_DG_BOUNDARYTRACE_H
#define FIELDAREA_DG_BOUNDARYTRACE_H

#include "Field_DG_BoundaryTraceBase.h"
#include "FieldArea.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// 2D solution field: DG boundary trace (e.g. Lagrange multiplier)
//----------------------------------------------------------------------------//

template <class PhysDim, class T>
class Field_DG_BoundaryTrace< PhysDim, TopoD2, T > : public Field_DG_BoundaryTraceBase< PhysDim, TopoD2, T >
{
public:
  typedef Field_DG_BoundaryTraceBase< PhysDim, TopoD2, T > BaseType;
  typedef T ArrayQ;

  Field_DG_BoundaryTrace( const XField<PhysDim, TopoD2>& xfld, const int order, const BasisFunctionCategory& category );

  Field_DG_BoundaryTrace( const XField<PhysDim, TopoD2>& xfld, const int order,
                          const BasisFunctionCategory& category, const std::vector<int>& BoundaryGroups );

  // Groups are first to remove ambiguity with list initializers
  // DG Fields are already broken, can just cat the CellGroupSets and forward
  explicit Field_DG_BoundaryTrace( const std::vector<std::vector<int>>& BoundaryGroupSets,
                                   const XField<PhysDim, TopoD2>& xfld, const int order, const BasisFunctionCategory& category )
  : Field_DG_BoundaryTrace( xfld, order, category, SANS::cat(BoundaryGroupSets)) {}

  Field_DG_BoundaryTrace( const Field_DG_BoundaryTrace& fld, const FieldCopy& tag );

  Field_DG_BoundaryTrace& operator=( const ArrayQ& q );

protected:
  using BaseType::nDOF_;
  using BaseType::DOF_;
  using BaseType::boundaryTraceGroups_;

  void init(const XField<PhysDim, TopoD2>& xfld, const int order,
            const BasisFunctionCategory& category, const std::vector<int>& BoundaryGroups );
};

}

#endif  // FIELD_DG_BOUNDARYTRACE_H
