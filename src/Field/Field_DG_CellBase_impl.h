// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(FIELD_DG_CELLBASE_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include <memory> // std::unique_ptr
#include <algorithm> // std::find

#include "Field_DG_CellBase.h"

#include "Field/tools/for_each_CellGroup.h"
#include "Field/tools/for_each_InteriorTraceGroup_Cell.h"
#include "Field/tools/for_each_PeriodicTraceGroup_Cell.h"
#include "Field/tools/for_each_BoundaryTraceGroup_Cell.h"
#include "Field/tools/for_each_GhostBoundaryTraceGroup_Cell.h"
#include "CountGhostNeighbors.h"
#include "tools/linspace.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_gather.hpp>
#include <boost/mpi/collectives/all_reduce.hpp>
#include <boost/serialization/access.hpp>
#endif

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

template <class PhysDim, class TopoDim, class T>
Field_DG_CellBase<PhysDim, TopoDim, T>::Field_DG_CellBase( const XField<PhysDim, TopoDim>& xfld ) : BaseType(xfld)
{
  //Derived classes call createCellGroup
}

template <class PhysDim, class TopoDim, class T>
Field_DG_CellBase<PhysDim, TopoDim, T>::Field_DG_CellBase( const Field_DG_CellBase& fld, const FieldCopy& tag ) : BaseType(fld, tag) {}

//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
void
Field_DG_CellBase<PhysDim, TopoDim, T>::
countGhostNeighbors(const std::vector<int>& CellGroups,
                    std::vector<std::vector<int>>& nGhostNeigbor,
                    std::vector<std::vector<bool>>& isZombie)
{
  isZombie.resize( CellGroups.size() );
  nGhostNeigbor.resize( CellGroups.size() );
  for (std::size_t igroup = 0; igroup < CellGroups.size(); igroup++)
  {
    const int groupGlobal = CellGroups[igroup];
    nGhostNeigbor[igroup].resize(xfld_.getCellGroupBase(groupGlobal).nElem(), 0);
    isZombie[igroup].resize(xfld_.getCellGroupBase(groupGlobal).nElem(), false);
  }

  // create a vector of all boundary trace groups
  std::vector<int> boundaryTraceGroups;
  if (xfld_.nBoundaryTraceGroups() > 0)
    boundaryTraceGroups = linspace(0,xfld_.nBoundaryTraceGroups()-1);

  // get the periodic boundary trace groups
  std::vector<int> periodicTraceGroups = xfld_.getPeriodicTraceGroupsGlobal(CellGroups);

  // remove the periodic groups from the list of boundary groups
  for (int bc : periodicTraceGroups)
    boundaryTraceGroups.erase(std::find(boundaryTraceGroups.begin(), boundaryTraceGroups.end(), bc));


  // count the number of neighbor ghost cells
  if (xfld_.nInteriorTraceGroups() > 0)
    for_each_InteriorTraceGroup_Cell<TopoDim>::apply(
        CountGhostNeighborsInterior<PhysDim>( linspace(0, xfld_.nInteriorTraceGroups()-1), xfld_.comm()->rank(), CellGroups, nGhostNeigbor ),
        xfld_ );

  if (periodicTraceGroups.size() > 0)
    for_each_PeriodicTraceGroup_Cell<TopoDim>::apply(
        CountGhostNeighborsInterior<PhysDim>( periodicTraceGroups, xfld_.comm()->rank(), CellGroups, nGhostNeigbor ),
        xfld_ );

  if (boundaryTraceGroups.size() > 0)
    for_each_BoundaryTraceGroup_Cell<TopoDim>::apply(
        CountGhostNeighborsBoundary<PhysDim>( boundaryTraceGroups, xfld_.comm()->rank(), CellGroups, nGhostNeigbor ),
        xfld_ );

  if (xfld_.nGhostBoundaryTraceGroups() > 0)
    for_each_GhostBoundaryTraceGroup_Cell<TopoDim>::apply(
        CountGhostNeighborsBoundary<PhysDim>( linspace(0,xfld_.nGhostBoundaryTraceGroups()-1), xfld_.comm()->rank(), CellGroups, nGhostNeigbor ),
        xfld_ );
}

//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
template<class Topology>
std::unique_ptr<FieldAssociativityConstructorBase>
Field_DG_CellBase<PhysDim, TopoDim, T>::
createCellGroup( const int groupGlobal, const int order, const BasisFunctionCategory& category, int& nDOFpossessed, int& nBasis )
{
  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupClass;
  typedef typename BaseType::template FieldCellGroupType<Topology> FieldCellGroupClass;
  typedef typename XFieldCellGroupClass::BasisType BasisType;

  const XFieldCellGroupClass& xfldGroup = xfld_.template getCellGroup<Topology>(groupGlobal);

  const BasisType* basis = BasisType::getBasisFunction(order, category);

  // create field associativity constructor
  typename FieldCellGroupClass::FieldAssociativityConstructorType* fldAssocCell = nullptr;
  fldAssocCell = new typename FieldCellGroupClass::FieldAssociativityConstructorType( basis, xfldGroup.nElem() );
  std::unique_ptr<FieldAssociativityConstructorBase> pfldAssocCell(fldAssocCell);

  const int nElem = xfldGroup.nElem();
  const int nbasis = nBasis = basis->nBasis();
  std::vector<int> map( nBasis );
  int comm_rank = xfld_.comm()->rank();

  for ( int elem = 0; elem < nElem; elem++ )
  {
    int rank = xfldGroup.associativity( elem ).rank();

    if (rank == comm_rank)
    {
      // increase the element count
      nElem_++;

      // create the local DOF map
      for ( int i = 0; i < nbasis; i++ )
        map[i] = nDOFpossessed++;

      fldAssocCell->setAssociativity( elem ).setRank( rank );
      fldAssocCell->setAssociativity( elem ).setGlobalMapping( map );

      // copy over the trace orientations from the cell
      fldAssocCell->setAssociativity( elem ).traceOrientation() = xfldGroup.associativity( elem ).traceOrientation();
    }
  }

  return pfldAssocCell;
}

//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
template<class Topology>
void
Field_DG_CellBase<PhysDim, TopoDim, T>::
setGhostCellDOFs(const int groupGlobal,
                 const std::vector<std::vector<bool>>& isZombie, const bool setZombie,
                 int& nDOF, std::unique_ptr<FieldAssociativityConstructorBase>& pfldAssocCell)
{
  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupClass;
  typedef typename BaseType::template FieldCellGroupType<Topology> FieldCellGroupClass;

  const XFieldCellGroupClass& xfldGroup = xfld_.template getCellGroup<Topology>(groupGlobal);

  typedef typename FieldCellGroupClass::FieldAssociativityConstructorType FieldAssociativityConstructorType;
  FieldAssociativityConstructorType& fldAssocCell = static_cast<FieldAssociativityConstructorType&>( *pfldAssocCell );

  int localGroup = localCellGroups_.at(groupGlobal);

  // DOF ordering: Fortran-array DOF(basis,cell)
  const int nElem = xfldGroup.nElem();
  const int nBasis = fldAssocCell.nBasis();
  std::vector<int> map( nBasis );
  int comm_rank = xfld_.comm()->rank();
  const std::vector<bool>& iszombie = isZombie[localGroup];

  for ( int elem = 0; elem < nElem; elem++ )
  {
    int rank = xfldGroup.associativity( elem ).rank();

    if (rank != comm_rank && iszombie[elem] == setZombie)
    {
      // set the local mapping for ghost cells

      // increase the element count
      nElem_++;

      // create the local DOF map for the ghost cells
      // this ensures that the ghost DOFs are last in the DOF array
      // and zombies are after the ghosts
      for ( int i = 0; i < nBasis; i++ )
        map[i] = nDOF++;

      fldAssocCell.setAssociativity( elem ).setRank( rank );
      fldAssocCell.setAssociativity( elem ).setGlobalMapping( map );

      // copy over the trace orientations from the cell
      fldAssocCell.setAssociativity( elem ).traceOrientation() = xfldGroup.associativity( elem ).traceOrientation();
    }
  }

  // allocate group
  if (setZombie)
    cellGroups_[localGroup] = new FieldCellGroupClass( fldAssocCell );
}


//----------------------------------------------------------------------------//
template <class PhysDim, class T>
class SetDOFrank_DG_Cell : public GroupFunctorCellType<SetDOFrank_DG_Cell<PhysDim, T>>
{
public:

  SetDOFrank_DG_Cell( const std::vector<int>& cellgroup_list,
                      const int comm_rank,
                      const int nDOFpossessed,
                      int *DOFghost_rank_ )
  : cellgroup_list_(cellgroup_list),
    comm_rank_(comm_rank), nDOFpossessed_(nDOFpossessed),
    DOFghost_rank_(DOFghost_rank_) {}

  std::size_t nCellGroups() const          { return cellgroup_list_.size(); }
  std::size_t cellGroup(const int n) const { return cellgroup_list_[n];     }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology>
  void
  apply(const typename Field<PhysDim,typename Topology::TopoDim,T>::template FieldCellGroupType<Topology>& fldCellGroup,
        const int cellGroupGlobal)
  {
    // DOF ordering: Fortran-array DOF(basis,cell)
    const int nElem = fldCellGroup.nElem();
    const int nBasis = fldCellGroup.nBasis();
    std::vector<int> map( nBasis );

    for ( int elem = 0; elem < nElem; elem++ )
    {
      int rank = fldCellGroup.associativity( elem ).rank();
      if (rank != comm_rank_)
      {
        fldCellGroup.associativity( elem ).getGlobalMapping( map.data(), map.size() );

        // assign the elemental rank to each DOF
        for ( int i = 0; i < nBasis; i++ )
          DOFghost_rank_[map[i] - nDOFpossessed_] = rank;
      }
    }
  }

protected:
  const std::vector<int> cellgroup_list_;
  const int comm_rank_;
  const int nDOFpossessed_;
  int *DOFghost_rank_;
};


//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
void
Field_DG_CellBase<PhysDim, TopoDim, T>::
createDOFs(const int nDOF, const int nDOFpossessed,
           const std::vector<std::vector<bool>>& isZombie, const std::vector<int>& nBasisGroups)
{
  // allocate the solution DOF array and assign it to groups

  this->resizeDOF(nDOF);

  this->nDOFpossessed_ = nDOFpossessed;

#ifdef SANS_MPI
  // compute the native DOF indexing based on the native cell numbering

  // TODO: The use of cellIDs in this->local2nativeDOFmap_ assumes
  // that all cell groups are used. Please fix!
  SANS_ASSERT(this->nCellGroups() == xfld_.nCellGroups());

  int iDOF = 0;
  int maxDOF = 0;
  bool setZombie = false;
  std::vector<int> offset_DOF(this->globalCellGroups_.size()+1, 0);
  for (int z = 0; z < 2; z++)
  {
    for (std::size_t igroup = 0; igroup < this->globalCellGroups_.size(); igroup++)
    {
      int groupGlobal = this->globalCellGroups_[igroup];

      const std::vector<int>& cellIDs = xfld_.cellIDs(groupGlobal);
      const int nBasis = nBasisGroups[igroup];

      for (std::size_t elem = 0; elem < cellIDs.size(); elem++)
        if (isZombie[igroup][elem] == setZombie)
          for (int n = 0; n < nBasis; n++)
          {
            this->local2nativeDOFmap_[iDOF] = nBasis*cellIDs[elem] + n + offset_DOF[igroup];
            maxDOF = std::max(maxDOF, this->local2nativeDOFmap_[iDOF]);
            iDOF++;
          }

      // compute the DOF offset based on the maximum DOF index
      if (z == 0)
        offset_DOF[igroup+1] = boost::mpi::all_reduce(*xfld_.comm(), maxDOF, boost::mpi::maximum<int>());
    }
    setZombie = true;
    if (z == 0)
      this->nDOFghost_ = iDOF - nDOFpossessed;
  }

#else
  // No ghost in serial
  this->nDOFghost_ = 0;
#endif

  for (int group = 0; group < cellGroups_.size(); group++)
    cellGroups_[group]->setDOF( DOF_, nDOF_ );

  this->resizeDOF_rank(nDOF, nDOFpossessed);

  // set the DOF ranks
  for_each_CellGroup<TopoDim>::apply(
      SetDOFrank_DG_Cell<PhysDim, T>( linspace(0, cellGroups_.size()-1), xfld_.comm()->rank(), nDOFpossessed_, this->DOFghost_rank_ ),
      *this );
}

//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
int
Field_DG_CellBase<PhysDim, TopoDim, T>::nDOFCellGroup(int cellgroup) const
{
  const int nElem = this->getCellGroupBase(cellgroup).nElem();
  const int nBasisCell = this->getCellGroupBase(cellgroup).nBasis();
  return nElem*nBasisCell;
}

template <class PhysDim, class TopoDim, class T>
int
Field_DG_CellBase<PhysDim, TopoDim, T>::nDOFInteriorTraceGroup(int tracegroup) const
{
  SANS_DEVELOPER_EXCEPTION("Field_DG_CellBase::nDOFInteriorTraceGroup - No interior trace groups!");
  return -1;
}

template <class PhysDim, class TopoDim, class T>
int
Field_DG_CellBase<PhysDim, TopoDim, T>::nDOFBoundaryTraceGroup(int tracegroup) const
{
  SANS_DEVELOPER_EXCEPTION("Field_DG_CellBase::nDOFBoundaryTraceGroup - No boundary trace groups!");
  return -1;
}

}
