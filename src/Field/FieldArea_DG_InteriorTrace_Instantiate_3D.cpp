// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define FIELD_DG_INTERIORTRACEBASE_INSTANTIATE
#include "Field_DG_InteriorTraceBase_impl.h"

#define FIELDAREA_DG_INTERIORTRACE_INSTANTIATE
#include "FieldArea_DG_InteriorTrace_impl.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "XFieldArea.h"

namespace SANS
{

template class Field_DG_InteriorTrace< PhysD3, TopoD2, Real >;

//template class Field_DG_InteriorTrace< PhysD3, TopoD2, DLA::VectorS<1, Real> >;

}
