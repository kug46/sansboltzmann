// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(FIELDVOLUME_CG_BOUNDARYTRACE_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "FieldVolume_CG_BoundaryTrace.h"

#include "BasisFunction/BasisFunctionCategory.h"

#include "Field_CG/Field_CG_TraceConstructor.h"

#include "tools/split_cat_std_vector.h"

namespace SANS
{

template <class PhysDim, class T>
Field_CG_BoundaryTrace<PhysDim, TopoD3, T>::Field_CG_BoundaryTrace( const XField<PhysDim, TopoD3>& xfld, const int order,
                                                                    const BasisFunctionCategory category ) : BaseType( xfld )
{
  // By default all boundary groups are used to construct lagrange multipliers
  init(order, category, {BaseType::createBoundaryGroupIndex()} );
}

template <class PhysDim, class T>
Field_CG_BoundaryTrace<PhysDim, TopoD3, T>::Field_CG_BoundaryTrace( const XField<PhysDim, TopoD3>& xfld,
                                                                    const int order, const BasisFunctionCategory category,
                                                                    const std::vector<int>& BoundaryGroups ) : BaseType( xfld )
{
  // Check that the groups asked for are within the range of available groups
  BaseType::checkBoundaryGroupIndex( BoundaryGroups );
  init(order, category, {BoundaryGroups});
}

template <class PhysDim, class T>
Field_CG_BoundaryTrace<PhysDim, TopoD3, T>::Field_CG_BoundaryTrace( const std::vector<std::vector<int>>& BoundaryGroupSets,
                                                                    const XField<PhysDim, TopoD3>& xfld, const int order,
                                                                    const BasisFunctionCategory category ) : BaseType( xfld )
{
  // Check that the groups asked for are within the range of available groups
  for (auto it = BoundaryGroupSets.begin(); it != BoundaryGroupSets.end(); ++it)
  {
    BaseType::checkBoundaryGroupIndex( *it );
    // Check that no boundary group occurs twice
    for (auto it2 = std::next(it,1); it2 != BoundaryGroupSets.end(); ++it2) // search the upper triangle of macro set
      SANS_ASSERT_MSG( std::find_first_of(it->begin(),it->end(),it2->begin(),it2->end())== it->end() ,
                       "Boundary groups can only be in one macro group");
  }

  init( order, category, BoundaryGroupSets );
}

template <class PhysDim, class T>
Field_CG_BoundaryTrace<PhysDim, TopoD3, T>::Field_CG_BoundaryTrace( const Field_CG_BoundaryTrace& fld, const FieldCopy&tag ) : BaseType(fld, tag) {}

template <class PhysDim, class T>
Field_CG_BoundaryTrace<PhysDim, TopoD3, T>&
Field_CG_BoundaryTrace<PhysDim, TopoD3, T>::operator=( const ArrayQ& q ) { BaseType::operator=(q); return *this; }

//----------------------------------------------------------------------------//
template <class PhysDim, class T>
void
Field_CG_BoundaryTrace<PhysDim, TopoD3, T>::init(const int order, const BasisFunctionCategory& category,
                                                 const std::vector<std::vector<int>>& boundaryGroupSets )
{
  SANS_ASSERT_MSG( order >= 1, "CG boundary face requires order >= 1" );
  SANS_ASSERT_MSG( (category == BasisFunctionCategory_Hierarchical && order <= 2) || // Logic needed for higher order
                   (category == BasisFunctionCategory_Lagrange),
                    "CG must use Hierarchical or Lagrange Basis, order = %d, category = %d", order, category);

  // create an empty set of interior trace groups
  std::vector<std::vector<int>> interiorGroupSets(boundaryGroupSets.size());

  Field_CG_TraceConstructor<PhysDim, TopoD3> fldConstructor(xfld_, order, category, interiorGroupSets, boundaryGroupSets);

  nElem_ = fldConstructor.nElem();

  // allocate the solution DOF array
  this->resizeDOF(fldConstructor.nDOF());

  // flatten the boundary sets
  std::vector<int> ConcatBoundaryGroups = cat(boundaryGroupSets);

  this->resizeBoundaryTraceGroups( ConcatBoundaryGroups );

  for ( std::size_t i = 0; i < boundaryGroupSets.size(); i++)
  {
    const std::vector<int>& boundaryGroups = boundaryGroupSets[i];
    for (std::size_t igroup = 0; igroup < boundaryGroups.size(); igroup++)
    {
      const int group = boundaryGroups[igroup];
      int localGroup = localBoundaryTraceGroups_.at(group);

      if ( xfld_.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Triangle) )
      {
        typedef typename BaseType:: template FieldTraceGroupType<Triangle> FieldTraceGroupClass;

        // allocate group
        boundaryTraceGroups_[localGroup] = fldConstructor.template createBoundaryTraceGroup<FieldTraceGroupClass>( group, this->local2nativeDOFmap_ );
        boundaryTraceGroups_[localGroup]->setDOF( DOF_, nDOF_ );
      }
      else if ( xfld_.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Quad) )
      {
        typedef typename BaseType:: template FieldTraceGroupType<Quad> FieldTraceGroupClass;

        // allocate group
        boundaryTraceGroups_[localGroup] = fldConstructor.template createBoundaryTraceGroup<FieldTraceGroupClass>( group, this->local2nativeDOFmap_ );
        boundaryTraceGroups_[localGroup]->setDOF( DOF_, nDOF_ );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Unknown boundary-trace topology." );
    }
  }

  // get the ghost/zombie DOF ranks
  // this must be last as it uses this->local2nativeDOFmap_
  this->nDOFpossessed_ = fldConstructor.nDOFpossessed();
  this->nDOFghost_     = fldConstructor.nDOFghost();

  this->resizeDOF_rank(fldConstructor.nDOF(), fldConstructor.nDOFpossessed());

  fldConstructor.getDOF_rank(this->DOFghost_rank_);
}

}
