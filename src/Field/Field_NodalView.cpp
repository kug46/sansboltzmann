// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "Field_NodalView.h"
#include <algorithm> // for std::set_intersection

namespace SANS
{

Field_NodalView::~Field_NodalView() {}

void
Field_NodalView::
insertCellNodes( int nodeDOFMap[], int nNode, int cell_group, int cell_elem, std::map<int, std::set<GroupElemIndex>>& NodeDOF2CellMap )
{
  std::map<int, std::set<GroupElemIndex>>::iterator it;

  GroupElemIndex cell(cell_group, cell_elem);

  for (int n = 0; n < nNode; n++)
  {
    int nodeDOF = nodeDOFMap[n];

    it = NodeDOF2CellMap.find(nodeDOF);

    if (it == NodeDOF2CellMap.end()) //this nodeDOF has no cell-set, so create one
    {
      NodeDOFList_.push_back(nodeDOF); //Add the newly encountered nodeDOF to list

      NodeDOF2CellMap[nodeDOF].insert(cell);
    }
    else // a cell-set already exists for this nodeDOF
    {
      it->second.insert(cell); //Add the new cellgroup-elem pair to the existing set, if not already added
    }
  }
  // convert the array of nodes into a std::vector and fill it
  // uses the nodeDOFMap as a pointer, then uses nodeDOFMap+nNode as the end of the pointer
  Cell2NodeDOFMap_.insert(std::pair<GroupElemIndex,std::vector<int>>(cell,std::vector<int>(nodeDOFMap,nodeDOFMap+nNode)));
}

void
Field_NodalView::insertEdge( int nodeDOF0, int nodeDOF1, std::map<int, std::set<std::pair<int, int>>>& NodeDOF2EdgeMap )
{
  SANS_ASSERT( nodeDOF0 != nodeDOF1 );

  std::map<int, std::set<std::pair<int, int>>>::iterator it;

  //2 nodes of edge sorted in ascending order => unique key to an edge
  std::pair<int, int> edge;
  if (nodeDOF0 < nodeDOF1)
    edge = {nodeDOF0, nodeDOF1};
  else
    edge = {nodeDOF1, nodeDOF0};

  //Add the current edge to the edge-set of nodeDOF0, if not added previously
  it = NodeDOF2EdgeMap.find(nodeDOF0);

  if (it == NodeDOF2EdgeMap.end()) //this nodeDOF has no edge-set, so create one
  {
    std::set<std::pair<int,int>> edgeset;
    edgeset.insert(edge);
    NodeDOF2EdgeMap[nodeDOF0] = edgeset;
  }
  else // an edge-set already exists for this nodeDOF
  {
    it->second.insert(edge); //Add edge to the existing set
  }

  //Add the current edge to the edge-set of nodeDOF1, if not added previously
  it = NodeDOF2EdgeMap.find(nodeDOF1);

  if (it == NodeDOF2EdgeMap.end()) //this nodeDOF has no edge-set, so create one
  {
    std::set<std::pair<int,int>> edgeset;
    edgeset.insert(edge);
    NodeDOF2EdgeMap[nodeDOF1] = edgeset;
  }
  else // an edge-set already exists for this nodeDOF
  {
    it->second.insert(edge); //Add edge to the existing set
  }

}

//----------------------------------------------------------------------------//
int
Field_NodalView::
getTopologicalShare( const std::vector<int>& nodeDOFs ) const
{
  /*
   * returns the number of cells sharing the object whose interior is defined by nodeDOFs
   *
   * should possibly assert that it's not empty, but this will be called a lot, so for speed don't
   */

  // 0th case
  if (nodeDOFs.empty() )
    return 0; // returned rather than asserted

  int result;

  // For storing the node attached cell sets
  std::vector<std::vector<GroupElemIndex>> cellsAttachedToNode(nodeDOFs.size());

  // collect them all
  for (std::size_t node_i = 0; node_i < nodeDOFs.size(); node_i++)
    cellsAttachedToNode[node_i] = getCellList( nodeDOFs[node_i] );

  // The options here are for speed reasons
  // This is expected to be called a lot, taking intersections can get costly
  // 1st case
  if (nodeDOFs.size() == 1)
  {
    result = static_cast<int>( cellsAttachedToNode[0].size() ); // just use the
  }
  else
  {
    // sort the base
    std::sort( cellsAttachedToNode[0].begin(), cellsAttachedToNode[0].end() );
    std::sort( cellsAttachedToNode[1].begin(), cellsAttachedToNode[1].end() );

    std::vector<GroupElemIndex> result_vec;

    std::set_intersection( cellsAttachedToNode[0].begin(), cellsAttachedToNode[0].end(),
                           cellsAttachedToNode[1].begin(), cellsAttachedToNode[1].end(),
                           std::back_inserter(result_vec) );

    if (nodeDOFs.size() > 2)
    {
      // outside loop so the memory is re-used
      // we're trying to avoid a cache-miss if possible
      std::vector<GroupElemIndex> buffer;

      for (std::size_t node_i = 2; node_i < nodeDOFs.size(); node_i++ )
      {
        // would be great if this were already sorted, but they'll likely be almost given how constructed
        std::sort( cellsAttachedToNode[node_i].begin(), cellsAttachedToNode[node_i].end() );
        buffer.clear();

        std::set_intersection( result_vec.begin(), result_vec.end(),
                               cellsAttachedToNode[node_i].begin(), cellsAttachedToNode[node_i].end(),
                               std::back_inserter( buffer ) );

        swap( result_vec, buffer);
      }
    }
    result = static_cast<int>( result_vec.size() );
  }

  return result;
}


//----------------------------------------------------------------------------//
void
Field_NodalView::
setMaps( std::map<int, std::set<GroupElemIndex>>& NodeDOF2CellMap,
         std::map<int, std::set<std::pair<int, int>>>& NodeDOF2EdgeMap )
{
  for (auto it = NodeDOF2CellMap.begin(); it != NodeDOF2CellMap.end(); ++it)
    NodeDOF2CellMap_[it->first] = std::vector<GroupElemIndex>( it->second.begin(), it->second.end() );

  for (auto it = NodeDOF2EdgeMap.begin(); it != NodeDOF2EdgeMap.end(); ++it)
    NodeDOF2EdgeMap_[it->first] = std::vector<std::pair<int,int>>( it->second.begin(), it->second.end() );
}

//----------------------------------------------------------------------------//
void
Field_NodalView::
updateInverseNodeMap(const int nDOF)
{
  invNodeDOFList_.resize(nDOF, -1);

  for (std::size_t n = 0; n < NodeDOFList_.size(); n++)
  {
    SANS_ASSERT(NodeDOFList_[n] < nDOF);
    invNodeDOFList_[NodeDOFList_[n]] = n;
  }
}

//----------------------------------------------------------------------------//
const Field_NodalView::IndexVector
Field_NodalView::getCellList(const int nodeDOF) const
{
  std::map<int, std::vector<GroupElemIndex>>::const_iterator it;

  it = NodeDOF2CellMap_.find(nodeDOF);

  if (it != NodeDOF2CellMap_.end()) //this nodeDOF doesn't exist in map
  {
    return it->second;
  }
  else
    SANS_DEVELOPER_EXCEPTION("Field_NodalView<PhysDim, TopoDim>::getCellList - nodeDOF %d not found in NodeDOF2CellMap!", nodeDOF);

  return std::vector<GroupElemIndex>(0);
}

//----------------------------------------------------------------------------//
const std::vector<std::pair<int,int>>
Field_NodalView::getEdgeList(const int nodeDOF) const
{
  std::map<int, std::vector<std::pair<int, int>>>::const_iterator it;

  it = NodeDOF2EdgeMap_.find(nodeDOF);

  if (it != NodeDOF2EdgeMap_.end()) //this node doesn't exist in map
  {
    return it->second;
  }
  else
    SANS_DEVELOPER_EXCEPTION("Field_NodalView<PhysDim, TopoDim>::getEdgeList - nodeDOF %d not found in NodeDOF2EdgeMap!", nodeDOF);

  return std::vector<std::pair<int,int>>(0);
}

//----------------------------------------------------------------------------//
const std::vector<int>
Field_NodalView::getNodeList( const GroupElemIndex cell ) const
{
  auto it = Cell2NodeDOFMap_.find(cell);

  if ( it != Cell2NodeDOFMap_.end())
  {
    return it->second;
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION("Field_NodalView<PhysDim, TopoDim>::getNodeList"
        "- group: %d, elem: %d not found in Cell2NodeDOFMap_!", cell.group,cell.elem);
  }
}

}
