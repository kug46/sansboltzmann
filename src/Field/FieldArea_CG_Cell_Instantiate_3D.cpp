// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define FIELDAREA_CG_CELL_INSTANTIATE
#include "FieldArea_CG_Cell_impl.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "XFieldArea.h"
#include "FieldArea.h"

namespace SANS
{

template class Field_CG_Cell< PhysD3, TopoD2, Real >;

}
