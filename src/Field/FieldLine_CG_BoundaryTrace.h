// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDLINE_CG_BOUNDARYTRACE_H
#define FIELDLINE_CG_BOUNDARYTRACE_H

#include <vector>

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "Field.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// solution field: CG boundary-edge (e.g. Lagrange multiplier)
//
// DOFs for all boundary regions determined together
//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
class Field_CG_BoundaryTrace;

template <class PhysDim, class T>
class Field_CG_BoundaryTrace<PhysDim, TopoD1, T> : public Field< PhysDim, TopoD1, T >
{
public:
  typedef Field< PhysDim, TopoD1, T > BaseType;
  typedef T ArrayQ;

  // dummy basis so the abstraction looks the same as for higher dimensions - it just gets ignored
  Field_CG_BoundaryTrace( const XField<PhysDim, TopoD1>& xfld, const int order,
                          const BasisFunctionCategory category );

  // dummy basis so the abstraction looks the same as for higher dimensions - it just gets ignored
  Field_CG_BoundaryTrace( const XField<PhysDim, TopoD1>& xfld, const int order,
                          const BasisFunctionCategory category, const std::vector<int>& BoundaryGroups );

  // Assumes that nodes are always broken groups.
  // Groups are first to remove ambiguity with list initializers
  // dummy basis so the abstraction looks the same as for higher dimensions - it just gets ignored
  explicit Field_CG_BoundaryTrace( const std::vector<std::vector<int>>& BoundaryGroupSets,
                                   const XField<PhysDim, TopoD1>& xfld, const int order,
                                   const BasisFunctionCategory category );

  Field_CG_BoundaryTrace( const Field_CG_BoundaryTrace& fld, const FieldCopy&tag );

  Field_CG_BoundaryTrace& operator=( const ArrayQ& q );

  // Virtual class to denote the SpaceType of the Field
  virtual SpaceType spaceType() const override { return SpaceType::Continuous; }

protected:
  using BaseType::nDOF_;
  using BaseType::DOF_;
  using BaseType::xfld_;
  using BaseType::boundaryTraceGroups_;
  using BaseType::nElem_;
  using BaseType::localBoundaryTraceGroups_;

  void init( const int order, const BasisFunctionCategory category, const std::vector<int>& BoundaryGroups );
};

}

#endif  // FIELDLINE_CG_BOUNDARYTRACE_H
