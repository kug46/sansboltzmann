// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(FIELDLIFT_DG_CELLBASE_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "FieldLift_DG_CellBase.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
FieldLift_DG_CellBase<PhysDim, TopoDim, T>&
FieldLift_DG_CellBase<PhysDim, TopoDim, T>::
operator=( const ArrayQ& q )
{
  BaseType::operator=(q);
  return *this;
}

//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
FieldLift_DG_CellBase<PhysDim, TopoDim, T>::
FieldLift_DG_CellBase( const XField<PhysDim, TopoDim>& xfld ) : BaseType(xfld)
{
    //allocate the cell groups
    this->resizeCellGroups( xfld.nCellGroups() );

    nDOF_ = 0;

    //Derived classes call createCellGroup
}

template <class PhysDim, class TopoDim, class T>
FieldLift_DG_CellBase<PhysDim, TopoDim, T>::FieldLift_DG_CellBase( const FieldLift_DG_CellBase& fld, const FieldCopy& tag ) : BaseType(fld, tag) {}


//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
template<class Topology>
void
FieldLift_DG_CellBase<PhysDim, TopoDim, T>::
createCellGroup(const int group, const int order, const BasisFunctionCategory& category)
{
  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupClass;
  typedef typename BaseType::template FieldCellGroupType<Topology> FieldCellGroupClass;
  typedef typename XFieldCellGroupClass::BasisType BasisType;

  static const int NTrace = Topology::NTrace;

  const XFieldCellGroupClass& xfldGroup = xfld_.template getCellGroup<Topology>(group);

  const BasisType* basis = BasisType::getBasisFunction(order, category);

  // create field associativity constructor
  typename FieldCellGroupClass::FieldLiftAssociativityConstructorType fldAssocCell( basis, xfldGroup.nElem() );

  // DOF ordering: Fortran-array DOF(basis,cell)
  const int nElem = xfldGroup.nElem();
  const int nBasis = basis->nBasis();
  std::vector<int> map( nBasis );
  for ( int elem = 0; elem < nElem; elem++ )
  {
    for ( int trace = 0; trace < NTrace; trace++ )
    {
      for ( int i = 0; i < nBasis; i++ )
        map[i] = nDOF_ +  NTrace*nBasis*elem + trace*nBasis + i;

      fldAssocCell.setAssociativity( elem, trace ).setRank( xfldGroup.associativity( elem ).rank() );
      fldAssocCell.setAssociativity( elem, trace ).setGlobalMapping( map );
    }
  }

  int localGroup = localCellGroups_.at(group);

  // allocate group
  cellGroups_[localGroup] = new FieldCellGroupClass( fldAssocCell );

  // accumulate DOF count
  nDOF_ += NTrace*nElem*nBasis;
  nElem_ += nElem;
}

//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
void
FieldLift_DG_CellBase<PhysDim, TopoDim, T>::createDOFs()
{
  // allocate the solution DOF array and assign it to groups

  this->resizeDOF(nDOF_);
  for (int n = 0; n < nDOF_; n++)
    DOF_[n] = 0;

  for (int group = 0; group < cellGroups_.size(); group++)
    cellGroups_[group]->setDOF( DOF_, nDOF_ );
}

//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
int
FieldLift_DG_CellBase<PhysDim, TopoDim, T>::nDOFCellGroup(int cellgroup) const
{
  SANS_DEVELOPER_EXCEPTION("FieldLift_DG_CellBase::nDOFCellGroup - Not implemented yet.");
  return -1;
}

template <class PhysDim, class TopoDim, class T>
int
FieldLift_DG_CellBase<PhysDim, TopoDim, T>::nDOFInteriorTraceGroup(int tracegroup) const
{
  SANS_DEVELOPER_EXCEPTION("FieldLift_DG_CellBase::nDOFInteriorTraceGroup - Not implemented yet.");
  return -1;
}

template <class PhysDim, class TopoDim, class T>
int
FieldLift_DG_CellBase<PhysDim, TopoDim, T>::nDOFBoundaryTraceGroup(int tracegroup) const
{
  SANS_DEVELOPER_EXCEPTION("FieldLift_DG_CellBase::nDOFBoundaryTraceGroup - Not implemented yet.");
  return -1;
}

}
