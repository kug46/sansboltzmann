// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDLIFTGROUPAREA_TRAITS_H
#define FIELDLIFTGROUPAREA_TRAITS_H

#include "BasisFunction/BasisFunctionArea.h"

#include "Field/Element/Element.h"
#include "Field/Element/ElementAssociativityArea.h"

#include "FieldLiftAssociativity.h"

#include "tools/Surrealize.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Traits for a field of lifting operators on area topologies
//----------------------------------------------------------------------------//

template<class Tin>
struct FieldLiftGroupAreaTraitsBase
{
  typedef FieldLiftAssociativityBase< Tin > FieldBase;

  typedef Tin T;                                     // DOF type
};


template<class Topology, class T>
struct FieldLiftGroupAreaTraits : public FieldLiftGroupAreaTraitsBase<T>
{
  typedef BasisFunctionAreaBase<Topology> BasisType;
  typedef Topology TopologyType;
  typedef ElementAssociativity<TopoD2,Topology> ElementAssociativityType;
  typedef FieldLiftAssociativityConstructor< typename ElementAssociativityType::Constructor > FieldLiftAssociativityConstructorType;

#if 0
  template<class ElemT = Real>
  struct ElementType : public Element< typename Surrealize<ElemT,T>::T, TopoD2, Topology >
  {
    typedef Element< typename Surrealize<ElemT,T>::T, TopoD2, Topology > BaseType;
    ElementType() : BaseType() {}
    explicit ElementType( const BasisType* basis ) : BaseType(basis) {}
    ElementType( int order, const BasisFunctionCategory& category ) : BaseType(order,category) {}
    ElementType& operator=( const ElementType& elem ) { BaseType::operator=(elem); return *this; }
  };
#else
  template<class ElemT = Real>
  using ElementType = Element< typename Surrealize<ElemT,T>::T, TopoD2, Topology >;
#endif
};

}

#endif //FIELDLIFTGROUPAREA_TRAITS_H
