// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(XFIELDVOLUME_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include <sstream>

#include "XFieldVolume.h"

namespace SANS
{

namespace XField_
{
//=============================================================================
template<class TopologyL>
struct check_RightTopology<PhysD3, TopoD3, Triangle, TopologyL>
{
  typedef XField<PhysD3, TopoD3> XFieldType;

  static void
  interiorTrace( const XFieldType& xfld, const int itraceGroup,
                 const typename XFieldType::template FieldTraceGroupType<Triangle>& traceGroup,
                 std::vector< std::vector<int> >& cellCount )
  {
    // determine topology for Right cell group
    const int groupR = traceGroup.getGroupRight();

    XFIELD_CHECK( groupR >= 0 && groupR < xfld.nCellGroups(), "groupR = %d, this->nCellGroups() = %d ", groupR, xfld.nCellGroups() );

    // dispatch check over elements of group
    if ( xfld.getCellGroupBase(groupR).topoTypeID() == typeid(Tet) )
    {
      xfld.template checkInteriorTrace<Triangle, TopologyL, Tet>( itraceGroup, traceGroup, cellCount );
    }
    else
    {
      char msg[] = "Error in XField< PhysD3, TopoD3 >::checkcheck_RightTopology: unknown cell topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  static void
  boundaryTrace( const XFieldType& xfld, const int itraceGroup,
                 const typename XFieldType::template FieldTraceGroupType<Triangle>& traceGroup,
                 std::vector< std::vector<int> >& cellCount )
  {
    // determine topology for Right cell group
    const int groupR = traceGroup.getGroupRight();

    XFIELD_CHECK( groupR >= 0 && groupR < xfld.nCellGroups(), "groupR = %d, this->nCellGroups() = %d ", groupR, xfld.nCellGroups() );

    // dispatch check over elements of group
    if ( xfld.getCellGroupBase(groupR).topoTypeID() == typeid(Tet) )
    {
      xfld.template checkBoundaryTraceConnected<Triangle, TopologyL, Tet>( itraceGroup, traceGroup, cellCount );
    }
    else
    {
      char msg[] = "Error in XField< PhysD3, TopoD3 >::checkcheck_RightTopology: unknown cell topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }
};

//=============================================================================
template<class TopologyL>
struct check_RightTopology<PhysD3, TopoD3, Quad, TopologyL>
{
  typedef XField<PhysD3, TopoD3> XFieldType;

  static void
  interiorTrace( const XFieldType& xfld, const int itraceGroup,
                 const typename XFieldType::template FieldTraceGroupType<Quad>& traceGroup,
                 std::vector< std::vector<int> >& cellCount )
  {
    // determine topology for Right cell group
    const int groupR = traceGroup.getGroupRight();

    XFIELD_CHECK( groupR >= 0 && groupR < xfld.nCellGroups(), "groupR = %d, this->nCellGroups() = %d ", groupR, xfld.nCellGroups() );

    // dispatch check over elements of group
    if ( xfld.getCellGroupBase(groupR).topoTypeID() == typeid(Hex) )
    {
      xfld.template checkInteriorTrace<Quad, TopologyL, Hex>( itraceGroup, traceGroup, cellCount );
    }
    else
    {
      char msg[] = "Error in XField< PhysD3, TopoD3 >::checkcheck_RightTopology: unknown cell topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  static void
  boundaryTrace( const XFieldType& xfld, const int itraceGroup,
                 const typename XFieldType::template FieldTraceGroupType<Quad>& traceGroup,
                 std::vector< std::vector<int> >& cellCount )
  {
    // determine topology for Right cell group
    const int groupR = traceGroup.getGroupRight();

    XFIELD_CHECK( groupR >= 0 && groupR < xfld.nCellGroups(), "groupR = %d, this->nCellGroups() = %d ", groupR, xfld.nCellGroups() );

    // dispatch check over elements of group
    if ( xfld.getCellGroupBase(groupR).topoTypeID() == typeid(Hex) )
    {
      xfld.template checkBoundaryTraceConnected<Quad, TopologyL, Hex>( itraceGroup, traceGroup, cellCount );
    }
    else
    {
      char msg[] = "Error in XField< PhysD3, TopoD3 >::checkcheck_RightTopology: unknown cell topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }
};

//=============================================================================
template<>
struct check_LeftTopology<PhysD3, TopoD3, Triangle>
{
  typedef XField<PhysD3, TopoD3> XFieldType;

  static void
  interiorTrace( const XFieldType& xfld, const int itraceGroup,
                 const typename XFieldType::template FieldTraceGroupType<Triangle>& traceGroup,
                 std::vector< std::vector<int> >& cellCount )
  {

    // determine topology for Left cell group
    const int groupL = traceGroup.getGroupLeft();

    XFIELD_CHECK( groupL >= 0 && groupL < xfld.nCellGroups(), "groupL = %d, this->nCellGroups() = %d ", groupL, xfld.nCellGroups() );

    if ( xfld.getCellGroupBase(groupL).topoTypeID() == typeid(Tet) )
    {
      // determine topology for R
      check_RightTopology<PhysD3, TopoD3, Triangle, Tet>::interiorTrace( xfld, itraceGroup, traceGroup, cellCount );
    }
    else
    {
      char msg[] = "Error in XField< PhysD3, TopoD3 >::checkInteriorTrace_LeftTopology: unknown cell topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  static void
  boundaryTrace( const XFieldType& xfld, const int itraceGroup,
                 const typename XFieldType::template FieldTraceGroupType<Triangle>& traceGroup,
                 std::vector< std::vector<int> >& cellCount )
  {
    // determine topology for Left cell group
    const int groupL = traceGroup.getGroupLeft();
    const int groupR = traceGroup.getGroupRight();

    XFIELD_CHECK( groupL >= 0 && groupL < xfld.nCellGroups(), "groupL = %d, this->nCellGroups() = %d ", groupL, xfld.nCellGroups() );

    if ( xfld.getCellGroupBase(groupL).topoTypeID() == typeid(Tet) )
    {
      if ( groupR < 0 )
        // dispatch check over elements of group
        xfld.checkBoundaryTrace<Triangle, Tet>( itraceGroup, traceGroup, cellCount );
      else
        check_RightTopology<PhysD3, TopoD3, Triangle, Tet>::boundaryTrace( xfld, itraceGroup, traceGroup, cellCount );
    }
    else
    {
      char msg[] = "Error in XField< PhysD3, TopoD3 >::checkBoundaryTrace_LeftTopology: unknown cell topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }
};


//=============================================================================
template<>
struct check_LeftTopology<PhysD3, TopoD3, Quad>
{
  typedef XField<PhysD3, TopoD3> XFieldType;

  static void
  interiorTrace( const XFieldType& xfld, const int itraceGroup,
                 const typename XFieldType::template FieldTraceGroupType<Quad>& traceGroup,
                 std::vector< std::vector<int> >& cellCount )
  {
    // determine topology for Left cell group
    const int groupL = traceGroup.getGroupLeft();

    XFIELD_CHECK( groupL >= 0 && groupL < xfld.nCellGroups(), "groupL = %d, this->nCellGroups() = %d ", groupL, xfld.nCellGroups() );

    if ( xfld.getCellGroupBase(groupL).topoTypeID() == typeid(Hex) )
    {
      // determine topology for R
      check_RightTopology<PhysD3, TopoD3, Quad, Hex>::interiorTrace( xfld, itraceGroup, traceGroup, cellCount );
    }
    else
    {
      char msg[] = "Error in XField< PhysD3, TopoD3 >::checkInteriorTrace_LeftTopology: unknown cell topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  static void
  boundaryTrace( const XFieldType& xfld, const int itraceGroup,
                 const typename XFieldType::template FieldTraceGroupType<Quad>& traceGroup,
                 std::vector< std::vector<int> >& cellCount )
  {
    // determine topology for Left cell group
    const int groupL = traceGroup.getGroupLeft();
    const int groupR = traceGroup.getGroupRight();

    XFIELD_CHECK( groupL >= 0 && groupL < xfld.nCellGroups(), "groupL = %d, this->nCellGroups() = %d ", groupL, xfld.nCellGroups() );

    if ( xfld.getCellGroupBase(groupL).topoTypeID() == typeid(Hex) )
    {
      if ( groupR < 0 )
        // dispatch check over elements of group
        xfld.checkBoundaryTrace<Quad, Hex>( itraceGroup, traceGroup, cellCount );
      else
        check_RightTopology<PhysD3, TopoD3, Quad, Hex>::boundaryTrace( xfld, itraceGroup, traceGroup, cellCount );
    }
    else
    {
      char msg[] = "Error in XField< PhysD3, TopoD3 >::checkBoundaryTrace_LeftTopology: unknown cell topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

};

} //namespace XField_


//=============================================================================
template<class PhysDim, class TopoDim>
void
XField< PhysDim, TopoDim >::checkGrid()
{
  initDefaultElmentIDs();

  std::vector< std::vector<int> > cellCount(this->nCellGroups());

  int nCellElem = 0;

  // loop over element groups
  for (int group = 0; group < this->nCellGroups(); group++)
  {
    nCellElem += this->getCellGroupBase(group).nElem();
    cellCount[group].resize( this->getCellGroupBase(group).nElem() );

    // dispatch integration over elements of group
    if ( this->getCellGroupBase(group).topoTypeID() == typeid(Tet) )
      checkPositiveJacobianDeterminant<Tet>( this->template getCellGroup<Tet>(group) );
    else if ( this->getCellGroupBase(group).topoTypeID() == typeid(Hex) )
      checkPositiveJacobianDeterminant<Hex>( this->template getCellGroup<Hex>(group) );
    else
    {
      const char msg[] = "Error in XField< PhysD3, TopoD3 >::check(): unknown cell topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  // loop over interior trace element groups
  for (int group = 0; group < this->nInteriorTraceGroups(); group++)
  {
    if ( this->getInteriorTraceGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      XField_::check_LeftTopology<PhysDim, TopoDim, Triangle>::
          interiorTrace( *this, group, this->template getInteriorTraceGroup<Triangle>(group), cellCount );
    }
    else if ( this->getInteriorTraceGroupBase(group).topoTypeID() == typeid(Quad) )
    {
      XField_::check_LeftTopology<PhysDim, TopoDim, Quad>::
          interiorTrace( *this, group, this->template getInteriorTraceGroup<Quad>(group), cellCount );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown trace topology" );
  }

  // loop over boundary trace element groups
  for (int group = 0; group < this->nBoundaryTraceGroups(); group++)
  {
    if ( this->getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      XField_::check_LeftTopology<PhysDim, TopoDim, Triangle>::
          boundaryTrace( *this, group, this->template getBoundaryTraceGroup<Triangle>(group), cellCount );
    }
    else if ( this->getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Quad) )
    {
      XField_::check_LeftTopology<PhysDim, TopoDim, Quad>::
          boundaryTrace( *this, group, this->template getBoundaryTraceGroup<Quad>(group), cellCount );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown boundary trace topology" );
  }

  // loop over ghost boundary trace element groups
  for (int group = 0; group < ghostBoundaryTraceGroups_.size(); group++)
  {
    SANS_ASSERT( ghostBoundaryTraceGroups_[group] != NULL );
    if ( ghostBoundaryTraceGroups_[group]->topoTypeID() == typeid(Triangle) )
    {
      XField_::check_LeftTopology<PhysDim, TopoDim, Triangle>::
          boundaryTrace( *this, group, this->template getGhostBoundaryTraceGroup<Triangle>(group), cellCount );
    }
    else if ( ghostBoundaryTraceGroups_[group]->topoTypeID() == typeid(Quad) )
    {
      XField_::check_LeftTopology<PhysDim, TopoDim, Quad>::
          boundaryTrace( *this, group, this->template getGhostBoundaryTraceGroup<Quad>(group), cellCount );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown trace topology" );
  }

  // loop over element groups and check the cell counts
  for (int group = 0; group < this->nCellGroups(); group++)
  {
    const int nelem = this->getCellGroupBase(group).nElem();

    if ( nelem == 0 ) continue; // Nothing to check

    if ( cellCount[group][0] == 0 )
    {
      for (int elem = 0; elem < nelem; elem++)
        XFIELD_CHECK( cellCount[group][elem] == 0,
          "Expecting zero cell count for all cells in cell group: cellCount[%d][%d] = %d", group, elem, cellCount[group][elem] );

      continue; //Move to the next group
    }

    // Check that each cell is touched the same number of times as they have edges
    if ( this->getCellGroupBase(group).topoTypeID() == typeid(Tet) )
    {
      for (int elem = 0; elem < nelem; elem++)
        XFIELD_CHECK( cellCount[group][elem] == Tet::NFace, "cellCount[%d][%d] = %d", group, elem, cellCount[group][elem] );
    }
    else if ( this->getCellGroupBase(group).topoTypeID() == typeid(Hex) )
    {
      for (int elem = 0; elem < nelem; elem++)
        XFIELD_CHECK( cellCount[group][elem] == Hex::NFace, "cellCount[%d][%d] = %d", group, elem, cellCount[group][elem] );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
  }

  // Check that the number of elements is set correctly
  XFIELD_CHECK( nCellElem == this->nElem_, "nCellElem = %d, nElem_ = %d", nCellElem, this->nElem_ );
}

//=============================================================================
template<class PhysDim, class TopoDim>
template <class TopologyTrace, class TopologyL, class TopologyR>
void
XField< PhysDim, TopoDim >::
checkInteriorTrace( const int itraceGroup,
                    const typename BaseType::template FieldTraceGroupType<TopologyTrace>& traceGroup,
                    std::vector< std::vector<int> >& cellCount ) const
{
  typedef typename BaseType::template FieldTraceGroupType<TopologyTrace> FieldTraceClass;
  typedef typename BaseType::template FieldCellGroupType<TopologyL> FieldCellLClass;
  typedef typename BaseType::template FieldCellGroupType<TopologyR> FieldCellRClass;

  int nodeMapT[ TopologyTrace::NNode ];
  int nodeMapL[ TopologyL::NNode ];
  int nodeMapR[ TopologyR::NNode ];

  const int groupL = traceGroup.getGroupLeft();
  const int groupR = traceGroup.getGroupRight();

  const int (*TraceNodesL)[ TopologyTrace::NTrace ] = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyL>::TraceNodes;
  const int (*TraceNodesR)[ TopologyTrace::NTrace ] = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyR>::TraceNodes;

  const int (*OrientPosR)[ TopologyTrace::NTrace ] = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyR>::OrientPos;
  const int (*OrientNegR)[ TopologyTrace::NTrace ] = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyR>::OrientNeg;

  const FieldCellLClass& cellGroupL = this->template getCellGroup<TopologyL>(groupL);
  const FieldCellRClass& cellGroupR = this->template getCellGroup<TopologyR>(groupR);

  typename FieldTraceClass::template ElementType<> elemTrace( traceGroup.basis() );
  typename FieldCellLClass::template ElementType<> elemCellL( cellGroupL.basis() );
  typename FieldCellRClass::template ElementType<> elemCellR( cellGroupR.basis() );

  VectorX N, cT, cL, dX; // Normal and centroids

  const int nelem = traceGroup.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    const int cellL                       = traceGroup.getElementLeft( elem );
    const CanonicalTraceToCell canonicalL = traceGroup.getCanonicalTraceLeft( elem );

    const int cellR                       = traceGroup.getElementRight( elem );
    const CanonicalTraceToCell canonicalR = traceGroup.getCanonicalTraceRight( elem );

    XFIELD_CHECK( cellL >= 0 && cellL < cellGroupL.nElem(), "cellL = %d, cellGroupL.nElem() = %d", cellL, cellGroupL.nElem() );
    XFIELD_CHECK( cellR >= 0 && cellR < cellGroupR.nElem(), "cellR = %d, cellGroupR.nElem() = %d", cellR, cellGroupR.nElem() );

    cellCount[groupL][cellL]++;
    cellCount[groupR][cellR]++;

    XFIELD_CHECK( canonicalL.trace >= 0 && canonicalL.trace < TopologyL::NTrace,
                  "canonicalL.trace = %d, TopologyL::NTrace = %d", canonicalL.trace, TopologyL::NTrace );
    XFIELD_CHECK( canonicalR.trace >= 0 && canonicalR.trace < TopologyR::NTrace,
                  "canonicalR.trace = %d, TopologyR::NTrace = %d", canonicalR.trace, TopologyR::NTrace );

    XFIELD_CHECK( canonicalL.orientation ==  1, "canonicalL.orientation = %d", canonicalL.orientation );
    XFIELD_CHECK( abs(canonicalR.orientation) >= 1 && abs(canonicalR.orientation) <= TopologyTrace::NTrace,
                  "canonicalR.orientation = %d", canonicalR.orientation );

    traceGroup.getElement( elemTrace, elem );
    cellGroupL.getElement( elemCellL, cellL );
    cellGroupR.getElement( elemCellR, cellR );

    traceGroup.associativity( elem ).getNodeGlobalMapping( nodeMapT, TopologyTrace::NNode );
    cellGroupL.associativity( cellL ).getNodeGlobalMapping( nodeMapL, TopologyL::NNode );
    cellGroupR.associativity( cellR ).getNodeGlobalMapping( nodeMapR, TopologyR::NNode );

    // Check the node mappings are consistent
    for (int i = 0; i < TopologyTrace::NNode; i++ )
    {
      int orient = canonicalR.orientation > 0 ? canonicalR.orientation-1 : -canonicalR.orientation-1;
      const int (*Orient)[ TopologyTrace::NTrace ] = canonicalR.orientation > 0 ? OrientPosR : OrientNegR;
      bool error = false;

      if (nodeMapL[TraceNodesL[canonicalL.trace][i]] != nodeMapT[i])
        error = true;

      if (nodeMapR[TraceNodesR[canonicalR.trace][Orient[orient][i]]] != nodeMapT[i])
        error = true;

      if (error)
      {
        // only construct std::stringstream if there is an error.
        // The constructor is very expensive time-wise for local solves
        std::stringstream msg;
        if (nodeMapL[TraceNodesL[canonicalL.trace][i]] != nodeMapT[i])
        {
          msg << std::endl;
          msg << "Interior trace nodes do not match left cell canonical trace nodes." << std::endl;
        }

        if (nodeMapR[TraceNodesR[canonicalR.trace][Orient[orient][i]]] != nodeMapT[i])
        {
          msg << std::endl;
          msg << "Interior trace nodes do not match right cell canonical trace nodes." << std::endl;
        }

        msg << std::endl;
        msg << "--- Trace ---" << std::endl;
        msg << "Trace Group = " << itraceGroup << std::endl;
        msg << "Group Element number = " << elem << std::endl;
        msg << "Nodes = { ";
        for (int n = 0; n < TopologyTrace::NNode-1; n++)
          msg << nodeMapT[n] << ", ";
        msg << nodeMapT[TopologyTrace::NNode-1] << " } " << std::endl;

        msg << std::endl;
        msg << "--- Left Cell ---" << std::endl;
        msg << "Canonical Trace = " << canonicalL.trace << std::endl;
        msg << "Canonical Orientation = " << canonicalL.orientation << std::endl;
        msg << "Canonical Trace nodes = { ";
        for (int n = 0; n < TopologyTrace::NNode-1; n++)
          msg << nodeMapL[TraceNodesL[canonicalL.trace][n]] << ", ";
        msg << nodeMapL[TraceNodesL[canonicalL.trace][TopologyTrace::NNode-1]] << " } " << std::endl;

        msg << "Element number = " << cellL << std::endl;
        msg << "Nodes = { ";
        for (int n = 0; n < TopologyL::NNode-1; n++)
          msg << nodeMapL[n] << ", ";
        msg << nodeMapL[TopologyL::NNode-1] << " } " << std::endl;

        msg << std::endl;
        msg << "--- Right Cell ---" << std::endl;
        msg << "Canonical Trace = " << canonicalR.trace << std::endl;
        msg << "Canonical Orientation = " << canonicalR.orientation << std::endl;
        msg << "Canonical Trace nodes = { ";
        for (int n = 0; n < TopologyTrace::NNode-1; n++)
          msg << nodeMapR[TraceNodesR[canonicalR.trace][Orient[orient][n]]] << ", ";
        msg << nodeMapR[TraceNodesR[canonicalR.trace][Orient[orient][TopologyTrace::NNode-1]]] << " } " << std::endl;

        msg << "Element number = " << cellR << std::endl;
        msg << "Nodes = { ";
        for (int n = 0; n < TopologyR::NNode-1; n++)
          msg << nodeMapR[n] << ", ";
        msg << nodeMapR[TopologyR::NNode-1] << " } " << std::endl;

        XFIELD_EXCEPTION( msg.str() );
      }
    }

    // check edge signs for area elements (L is +, R is -)
    XFIELD_CHECK( elemCellL.faceSign()[canonicalL.trace] == +1,
                  "groupL = %d, cellL = %d, groupR = %d, cellR = %d, canonicalR.trace = %d, elemCellL.faceSign()[%d] = %d",
                  groupL, cellL, groupR, cellR, canonicalR.trace, canonicalL.trace, elemCellL.faceSign()[canonicalL.trace] );
    XFIELD_CHECK( elemCellR.faceSign()[canonicalR.trace] == canonicalR.orientation,
                  "groupL = %d, cellL = %d, groupR = %d, cellR = %d, canonicalL.trace = %d, "
                  "elemCellR.faceSign()[%d] = %d, canonicalR.orientation = %d",
                  groupL, cellL, groupR, cellR, canonicalL.trace, canonicalR.trace, elemCellR.faceSign()[canonicalR.trace], canonicalR.orientation );

    // Check the normal vector
    checkTraceNormal<TopologyTrace, TopologyL>(traceGroup, elem, elemTrace, elemCellL);
  }
}


//=============================================================================
template<class PhysDim, class TopoDim>
template <class TopologyTrace, class TopologyL>
void
XField< PhysDim, TopoDim >::
checkBoundaryTrace( const int itraceGroup,
                    const typename BaseType::template FieldTraceGroupType<TopologyTrace>& traceGroup,
                    std::vector< std::vector<int> >& cellCount ) const
{
  typedef typename BaseType::template FieldTraceGroupType<TopologyTrace> FieldTraceClass;
  typedef typename BaseType::template FieldCellGroupType<TopologyL> FieldCellLClass;

  int nodeMapT[ TopologyTrace::NNode ];
  int nodeMapL[ TopologyL::NNode ];

  const int groupL = traceGroup.getGroupLeft();

  const int (*TraceNodesL)[ TopologyTrace::NTrace ] = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyL>::TraceNodes;

  const FieldCellLClass& cellGroupL = this->template getCellGroup<TopologyL>(groupL);

  typename FieldTraceClass::template ElementType<> elemTrace( traceGroup.basis() );
  typename FieldCellLClass::template ElementType<> elemCellL( cellGroupL.basis() );

  const int nelem = traceGroup.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    const int cellL                       = traceGroup.getElementLeft( elem );
    const CanonicalTraceToCell canonicalL = traceGroup.getCanonicalTraceLeft( elem );

    XFIELD_CHECK( cellL >= 0 && cellL < cellGroupL.nElem(), "cellL = %d, cellGroupL.nElem() = %d", cellL, cellGroupL.nElem() );

    cellCount[groupL][cellL]++;

    XFIELD_CHECK( canonicalL.trace >= 0 && canonicalL.trace < TopologyL::NTrace,
                  "canonicalL.trace = %d, TopologyL::NTrace = %d", canonicalL.trace, TopologyL::NTrace );

    XFIELD_CHECK( canonicalL.orientation ==  1, "canonicalL.orientation = %d", canonicalL.orientation );

    traceGroup.getElement( elemTrace, elem );
    cellGroupL.getElement( elemCellL, cellL );

    traceGroup.associativity( elem ).getNodeGlobalMapping( nodeMapT, TopologyTrace::NNode );
    cellGroupL.associativity( cellL ).getNodeGlobalMapping( nodeMapL, TopologyL::NNode );

    // Check the node mappings are consistent
    for (int i = 0; i < TopologyTrace::NNode; i++ )
    {
      if (nodeMapL[TraceNodesL[canonicalL.trace][i]] != nodeMapT[i])
      {
        std::stringstream msg;
        msg << std::endl;
        msg << "Boundary trace nodes do not match left cell canonical trace nodes." << std::endl;

        msg << std::endl;
        msg << "--- Trace ---" << std::endl;
        msg << "Trace Group = " << itraceGroup << std::endl;
        msg << "Group Element number = " << elem << std::endl;
        msg << "Nodes = { ";
        for (int n = 0; n < TopologyTrace::NNode-1; n++)
          msg << nodeMapT[n] << ", ";
        msg << nodeMapT[TopologyTrace::NNode-1] << " } " << std::endl;

        msg << std::endl;
        msg << "--- Left Cell ---" << std::endl;
        msg << "Canonical Trace = " << canonicalL.trace << std::endl;
        msg << "Canonical Trace nodes = { ";
        for (int n = 0; n < TopologyTrace::NNode-1; n++)
          msg << nodeMapL[TraceNodesL[canonicalL.trace][n]] << ", ";
        msg << nodeMapL[TraceNodesL[canonicalL.trace][TopologyTrace::NNode-1]] << " } " << std::endl;

        msg << "Element number = " << cellL << std::endl;
        msg << "Nodes = { ";
        for (int n = 0; n < TopologyL::NNode-1; n++)
          msg << nodeMapL[n] << ", ";
        msg << nodeMapL[TopologyL::NNode-1] << " } " << std::endl;

        XFIELD_EXCEPTION( msg.str() );
      }
    }

    // check edge signs for area elements (L is +, R is -)
    XFIELD_CHECK( elemCellL.faceSign()[canonicalL.trace] == +1,
                  "elemCellL.faceSign()[%d] = %e", canonicalL.trace, elemCellL.faceSign()[canonicalL.trace] );

    // Check the normal vector
    checkTraceNormal<TopologyTrace, TopologyL>(traceGroup, elem, elemTrace, elemCellL);
  }
}

//=============================================================================
template<class PhysDim, class TopoDim>
template <class TopologyTrace, class TopologyL, class TopologyR>
void
XField< PhysDim, TopoDim >::
checkBoundaryTraceConnected( const int itraceGroup,
                             const typename BaseType::template FieldTraceGroupType<TopologyTrace>& traceGroup,
                             std::vector< std::vector<int> >& cellCount ) const
{
  typedef typename BaseType::template FieldTraceGroupType<TopologyTrace> FieldTraceClass;
  typedef typename BaseType::template FieldCellGroupType<TopologyL> FieldCellLClass;
  typedef typename BaseType::template FieldCellGroupType<TopologyR> FieldCellRClass;

  int nodeMapT[ TopologyTrace::NNode ];
  int nodeMapL[ TopologyL::NNode ];

  const int groupL = traceGroup.getGroupLeft();
  const int groupR = traceGroup.getGroupRight();

  const int (*TraceNodesL)[ TopologyTrace::NTrace ] = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyL>::TraceNodes;

  const FieldCellLClass& cellGroupL = this->template getCellGroup<TopologyL>(groupL);
  const FieldCellRClass& cellGroupR = this->template getCellGroup<TopologyR>(groupR);

  typename FieldTraceClass::template ElementType<> elemTrace( traceGroup.basis() );
  typename FieldCellLClass::template ElementType<> elemCellL( cellGroupL.basis() );
  typename FieldCellRClass::template ElementType<> elemCellR( cellGroupR.basis() );


  const int nelem = traceGroup.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    const int cellL                       = traceGroup.getElementLeft( elem );
    const CanonicalTraceToCell canonicalL = traceGroup.getCanonicalTraceLeft( elem );

    const int cellR                       = traceGroup.getElementRight( elem );
    const CanonicalTraceToCell canonicalR = traceGroup.getCanonicalTraceRight( elem );

    XFIELD_CHECK( cellL >= 0 && cellL < cellGroupL.nElem(), "cellL = %d, cellGroupL.nElem() = %d", cellL, cellGroupL.nElem() );
    XFIELD_CHECK( cellR >= 0 && cellR < cellGroupR.nElem(), "cellR = %d, cellGroupR.nElem() = %d", cellR, cellGroupR.nElem() );

    cellCount[groupL][cellL]++;
    cellCount[groupR][cellR]++;

    XFIELD_CHECK( canonicalL.trace >= 0 && canonicalL.trace < TopologyL::NTrace,
                  "canonicalL.trace = %d, TopologyL::NTrace = %d", canonicalL.trace, TopologyL::NTrace );
    XFIELD_CHECK( canonicalR.trace >= 0 && canonicalR.trace < TopologyR::NTrace,
                  "canonicalR.trace = %d, TopologyR::NTrace = %d", canonicalR.trace, TopologyR::NTrace );

    XFIELD_CHECK( canonicalL.orientation ==  1, "canonicalL.orientation = %d", canonicalL.orientation );
    XFIELD_CHECK( abs(canonicalR.orientation) > 0 && abs(canonicalR.orientation) <= TopologyTrace::NTrace,
                  "canonicalR.orientation = %d", canonicalR.orientation );

    traceGroup.getElement( elemTrace, elem );
    cellGroupL.getElement( elemCellL, cellL );
    cellGroupR.getElement( elemCellR, cellR );

    traceGroup.associativity( elem ).getNodeGlobalMapping( nodeMapT, TopologyTrace::NNode );
    cellGroupL.associativity( cellL ).getNodeGlobalMapping( nodeMapL, TopologyL::NNode );

    // Check the node mappings are consistent
    for (int i = 0; i < TopologyTrace::NNode; i++ )
    {
      if (nodeMapL[TraceNodesL[canonicalL.trace][i]] != nodeMapT[i])
      {
        std::stringstream msg;
        msg << std::endl;
        msg << "Connected boundary trace nodes do not match left cell canonical trace nodes." << std::endl;

        msg << std::endl;
        msg << "--- Trace ---" << std::endl;
        msg << "Trace Group = " << itraceGroup << std::endl;
        msg << "Group Element number = " << elem << std::endl;
        msg << "Nodes = { ";
        for (int n = 0; n < TopologyTrace::NNode-1; n++)
          msg << nodeMapT[n] << ", ";
        msg << nodeMapT[TopologyTrace::NNode-1] << " } " << std::endl;

        msg << "Canonical Trace = " << canonicalL.trace << std::endl;
        msg << "Canonical Orientation = " << canonicalL.orientation << std::endl;
        msg << "Canonical Trace nodes = { ";
        for (int n = 0; n < TopologyTrace::NNode-1; n++)
          msg << nodeMapL[TraceNodesL[canonicalL.trace][n]] << ", ";
        msg << nodeMapL[TraceNodesL[canonicalL.trace][TopologyTrace::NNode-1]] << " } " << std::endl;

        msg << std::endl;
        msg << "--- Left Cell ---" << std::endl;
        msg << "Element number = " << cellL << std::endl;
        msg << "Nodes = { ";
        for (int n = 0; n < TopologyL::NNode-1; n++)
          msg << nodeMapL[n] << ", ";
        msg << nodeMapL[TopologyL::NNode-1] << " } " << std::endl;

        XFIELD_EXCEPTION( msg.str() );
      }
    }

    // check edge signs for area elements (L is +, R is -)
    XFIELD_CHECK( elemCellL.faceSign()[canonicalL.trace] == +1,
                  "cellL = %d, cellR = %d, canonicalR = %d, elemCellL.faceSign()[%d-1] = %d",
                  cellL, cellR, canonicalR.trace, canonicalL.trace, elemCellL.faceSign()[canonicalL.trace] );
    XFIELD_CHECK( elemCellR.faceSign()[canonicalR.trace] == canonicalR.orientation,
                  "cellL = %d, cellR = %d, canonicalL = %d, elemCellR.faceSign()[%d-1] = %d, canonicalR.orientation = %d",
                  cellL, cellR, canonicalL.trace, canonicalR.trace, elemCellR.faceSign()[canonicalR.trace], canonicalR.orientation );

    // Check the normal vector
    checkTraceNormal<TopologyTrace, TopologyL>(traceGroup, elem, elemTrace, elemCellL);
  }
}

} //namespace SANS
