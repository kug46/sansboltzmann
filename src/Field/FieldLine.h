// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDLINE_H
#define FIELDLINE_H

#include "FieldGroupNode_Traits.h"
#include "FieldGroupLine_Traits.h"

#include "Field.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Topologically 1D field variables
//----------------------------------------------------------------------------//

template<class Tin>
struct FieldTraits<TopoD1, Tin>
{
  typedef TopoD1 TopoDim;
  typedef Tin T;                                     // DOF type

  typedef typename FieldGroupNodeTraits<T>::FieldBase FieldTraceGroupBase;
  typedef typename FieldGroupLineTraits<T>::FieldBase FieldCellGroupBase;

  template<class Topology>
  using FieldTraceGroupType = FieldAssociativity< FieldGroupNodeTraits<T> >;

  template<class Topology>
  using FieldCellGroupType = FieldAssociativity< FieldGroupLineTraits<T> >;
};


// template<class Tin>
// struct FieldTraits<TopoD0,Tin>
// {
//   typedef TopoD0 TopoDim;
//   typedef Tin T;
//
//   typedef typename FieldGroupNodeTraits<T>::FieldBase FieldTraceGroupBase;
//   typedef typename FieldGroupLineTraits<T>::FieldBase FieldCellGroupBase;
//
//   template<class Topology>
//   using FieldTraceGroupType = FieldAssociativity< FieldGroupNodeTraits<T> >;
//
//   template<class Topology>
//   using FieldCellGroupType = FieldAssociativity< FieldGroupLineTraits<T> >;
// };

} //namespace SANS

#endif //FIELDLINE_H
