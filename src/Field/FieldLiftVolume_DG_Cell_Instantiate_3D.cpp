// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define FIELDBASE_INSTANTIATE
#include "FieldBase_impl.h"

#define FIELDSUBGROUP_INSTANTIATE
#include "FieldSubGroup_impl.h"

#define FIELDLIFT_DG_CELLBASE_INSTANTIATE
#include "FieldLift_DG_CellBase_impl.h"

#define FIELDLIFTVOLUME_DG_CELL_INSTANTIATE
#include "FieldLiftVolume_DG_Cell_impl.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "XFieldVolume.h"

namespace SANS
{
// Base class instantiation
template class FieldBase< FieldLiftTraits<TopoD3, Real > >;
//template class FieldBase< FieldLiftTraits<TopoD3, DLA::VectorS<2, Real> > >;
template class FieldBase< FieldLiftTraits<TopoD3, DLA::VectorS<3, Real> > >;
template class FieldBase< FieldLiftTraits<TopoD3, DLA::VectorS<3, DLA::VectorS<2, Real>> > >;
template class FieldBase< FieldLiftTraits<TopoD3, DLA::VectorS<3, DLA::VectorS<3, Real>> > >;
template class FieldBase< FieldLiftTraits<TopoD3, DLA::VectorS<3, DLA::VectorS<5, Real>> > >;
template class FieldBase< FieldLiftTraits<TopoD3, DLA::VectorS<3, DLA::VectorS<6, Real>> > >;
template class FieldBase< FieldLiftTraits<TopoD3, DLA::VectorS<3, DLA::VectorS<7, Real>> > >;

// FieldSubGroup instantiation
template class FieldSubGroup< PhysD3, FieldLiftTraits<TopoD3, Real > >;
//template class FieldSubGroup< PhysD3, FieldLiftTraits<TopoD3, DLA::VectorS<2, Real> > >;
template class FieldSubGroup< PhysD3, FieldLiftTraits<TopoD3, DLA::VectorS<3, Real> > >;
template class FieldSubGroup< PhysD3, FieldLiftTraits<TopoD3, DLA::VectorS<3, DLA::VectorS<2, Real>> > >;
template class FieldSubGroup< PhysD3, FieldLiftTraits<TopoD3, DLA::VectorS<3, DLA::VectorS<3, Real>> > >;
template class FieldSubGroup< PhysD3, FieldLiftTraits<TopoD3, DLA::VectorS<3, DLA::VectorS<5, Real>> > >;
template class FieldSubGroup< PhysD3, FieldLiftTraits<TopoD3, DLA::VectorS<3, DLA::VectorS<6, Real>> > >;
template class FieldSubGroup< PhysD3, FieldLiftTraits<TopoD3, DLA::VectorS<3, DLA::VectorS<7, Real>> > >;

// Field Lift instantiation
template class FieldLift_DG_Cell< PhysD3, TopoD3, Real >;
//template class FieldLift_DG_Cell< PhysD3, TopoD3, DLA::VectorS<2, Real> >;
template class FieldLift_DG_Cell< PhysD3, TopoD3, DLA::VectorS<3, Real> >;
template class FieldLift_DG_Cell< PhysD3, TopoD3, DLA::VectorS<3, DLA::VectorS<2, Real>> >;
template class FieldLift_DG_Cell< PhysD3, TopoD3, DLA::VectorS<3, DLA::VectorS<3, Real>> >;
template class FieldLift_DG_Cell< PhysD3, TopoD3, DLA::VectorS<3, DLA::VectorS<5, Real>> >;
template class FieldLift_DG_Cell< PhysD3, TopoD3, DLA::VectorS<3, DLA::VectorS<6, Real>> >;
template class FieldLift_DG_Cell< PhysD3, TopoD3, DLA::VectorS<3, DLA::VectorS<7, Real>> >;

}
