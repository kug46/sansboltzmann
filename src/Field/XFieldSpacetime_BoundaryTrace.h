// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELDSPACETIME_BOUNDARYTRACE_H
#define XFIELDSPACETIME_BOUNDARYTRACE_H

#include "XField_BoundaryTrace.h"
#include "XFieldSpacetime.h"
#include "FieldAssociativity.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Topologically 4D grid
//----------------------------------------------------------------------------//

template<class PhysDim>
struct XFieldTraits_BoundaryTrace<PhysDim, TopoD4>
{
  typedef TopoD4 TopoDim;
  static const int D = PhysDim::D;                 // physical dimensions

  typedef DLA::VectorS<D,Real> VectorX;   // coordinates vector
  typedef VectorX T;

  typedef typename XFieldVolumeTraits<PhysDim, TopoD4>::FieldBase FieldTraceGroupBase;
  typedef typename XFieldSpacetimeTraits<PhysDim, TopoD4>::FieldBase FieldCellGroupBase;

  template<class Topology>
  using FieldTraceGroupType = FieldAssociativity< XFieldGroupVolumeTraits<PhysDim, TopoD4, Topology> >;

  template<class Topology>
  using FieldCellGroupType = FieldAssociativity< XFieldGroupSpacetimeTraits<PhysDim, TopoD4, Topology> >;
};

} // namespace SANS

#endif //XFIELDSPACETIME_BOUNDARYTRACE_H
