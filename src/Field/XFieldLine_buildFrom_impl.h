// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(XFIELDLINE_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "BasisFunction/BasisFunctionCategory.h"

#include "XField.h"

#include "Field_CG/Field_CG_CellConstructor.h"

namespace SANS
{

//------------------------------------------------------------------------------//
// Implements the protected projectTo function for creating higher order meshes
//------------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
void
XField<PhysDim,TopoDim>::buildFrom(const XField& xfld, const int order,
                                   BasisFunctionCategory category)
{
  SANS_ASSERT_MSG( order >= 1, "CG area requires order >= 1" );
  SANS_ASSERT_MSG( category == BasisFunctionCategory_Hierarchical ||
                   category == BasisFunctionCategory_Lagrange ||
                   category == BasisFunctionCategory_None,
                   "XField must use Hierarchical or Lagrange Basis" );

  SANS_ASSERT(xfld.comm()->size() == 1); //TODO: Make this work in parallel. The normalSign is the issue..

  // make sure this grid is clean, as the resize functions do not deallocate memory
  this->deallocate();

  if (category == BasisFunctionCategory_None)
    category = xfld.getCellGroupBase(0).basisCategory();

  std::vector<int> CellGroups(xfld.nCellGroups());
  for ( std::size_t i = 0; i < CellGroups.size(); i++ )
    CellGroups[i] = i;

  Field_CG_CellConstructor<PhysDim, TopoDim> fldConstructor(xfld, order, category, {CellGroups});

  // create the DOFs
  this->resizeDOF(fldConstructor.nDOF());

  // allocate the area groups; set nodal & cell DOF associativity; set edge signs from grid
  this->resizeCellGroups( xfld.nCellGroups() );

  for (int group = 0; group < this->nCellGroups(); group++)
  {
    if ( xfld.getCellGroupBase(group).topoTypeID() == typeid(Line) )
    {
      typedef typename XField<PhysDim, TopoD1>::template FieldCellGroupType<Line> FieldCellGroupClass;

      this->cellGroups_[group] = fldConstructor.template createCellGroup<FieldCellGroupClass>( group, this->local2nativeDOFmap_);
      this->cellGroups_[group]->setDOF(this->DOF_,this->nDOF_);
    }
    else
    {
      SANS_DEVELOPER_EXCEPTION( "Unknown element topology." );
    }
  }
  // allocate the interior-node groups; set node DOF associativity
  this->resizeInteriorTraceGroups( xfld.nInteriorTraceGroups() );

  for (int group = 0; group < this->nInteriorTraceGroups(); group++)
  {
    if ( xfld.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Node) )
    {
      typedef typename XField<PhysDim, TopoD1>:: template FieldTraceGroupType<Node> FieldTraceGroupClass;

      // allocate group
      this->interiorTraceGroups_[group] = fldConstructor.template createXFieldInteriorTraceGroup<FieldTraceGroupClass>( group );
      this->interiorTraceGroups_[group]->setDOF( this->DOF_, this->nDOF_ );
    }
    else
    {
      SANS_DEVELOPER_EXCEPTION( "Unknown interior-trace topology." );
    }
  }

  // allocate the boundary-node groups; set node DOF associativity
  this->resizeBoundaryTraceGroups( xfld.nBoundaryTraceGroups() );

  for (int group = 0; group < this->nBoundaryTraceGroups(); group++)
  {
    if ( xfld.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Node) )
    {
      typedef typename XField<PhysDim, TopoD1>:: template FieldTraceGroupType<Node> FieldTraceGroupClass;

      // allocate group
      this->boundaryTraceGroups_[group] = fldConstructor.template createXFieldBoundaryTraceGroup<FieldTraceGroupClass>( group );
      this->boundaryTraceGroups_[group]->setDOF( this->DOF_, this->nDOF_ );
    }
    else
    {
      SANS_DEVELOPER_EXCEPTION( "Unknown boundary-trace topology." );
    }
  }

  // allocate the ghost boundary-node groups; set node DOF associativity
  this->resizeGhostBoundaryTraceGroups( xfld.nGhostBoundaryTraceGroups() );

  for (int group = 0; group < this->nGhostBoundaryTraceGroups(); group++)
  {
    if ( xfld.getGhostBoundaryTraceGroupBase(group).topoTypeID() == typeid(Node) )
    {
      typedef typename XField<PhysDim, TopoD1>:: template FieldTraceGroupType<Node> FieldTraceGroupClass;

      // allocate group
      this->ghostBoundaryTraceGroups_[group] = fldConstructor.template createXFieldGhostBoundaryTraceGroup<FieldTraceGroupClass>( group );
      this->ghostBoundaryTraceGroups_[group]->setDOF( this->DOF_, this->nDOF_ );
    }
    else
    {
      SANS_DEVELOPER_EXCEPTION( "Unknown ghost boundary-trace topology." );
    }
  }


  this->nElem_            = xfld.nElem();
  this->cellIDs_          = xfld.cellIDs_;
  this->boundaryTraceIDs_ = xfld.boundaryTraceIDs_;

  //Project DOFs from old mesh to new mesh
  xfld.projectTo(*this);
}

}
