// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD_CELLTOTRACE_H
#define XFIELD_CELLTOTRACE_H

#include <iostream>
#include <string>
#include <memory>
#include <typeinfo>     // typeid
#include <vector>

#include "CellToTraceAssociativity.h"
#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "XField.h"

namespace SANS
{

template <class PhysDim, class TopoDim>
class XField_CellToTrace_Base
{
public:

  // cppcheck-suppress noExplicitConstructor
  XField_CellToTrace_Base(const XField<PhysDim, TopoDim>& xfld) : xfld_(xfld), nCellGroups_(0){};

  XField_CellToTrace_Base(const XField_CellToTrace_Base&) = delete;
  XField_CellToTrace_Base& operator=(const XField_CellToTrace_Base&) = delete;

  const TraceInfo& getTrace(const int cellGroup, const int cellElem, const int canonicalTrace) const; //Cell to Trace connectivity

  const XField<PhysDim, TopoDim>& getXField() const { return xfld_; }

protected:

  template <class TopologyTrace>
  void loopInteriorTraces(int group);

  template <class TopologyTrace>
  void loopBoundaryTraces(int group);

  template <class TopologyTrace>
  void loopGhostBoundaryTraces(int group);

  const XField<PhysDim, TopoDim>& xfld_;
  int nCellGroups_;
  std::vector<std::shared_ptr<CellToTraceAssociativityBase>> CelltoTraceAssociativity_; //cell to trace mapping for elements
};


template <class PhysDim, class TopoDim>
class XField_CellToTrace;

template <class PhysDim>
class XField_CellToTrace<PhysDim, TopoD1> : public XField_CellToTrace_Base<PhysDim,TopoD1>
{
public:
  typedef XField_CellToTrace_Base<PhysDim, TopoD1> BaseType;

  // cppcheck-suppress noExplicitConstructor
  XField_CellToTrace(const XField<PhysDim, TopoD1>& xfld);

protected:
  using BaseType::xfld_;
  using BaseType::CelltoTraceAssociativity_;
  using BaseType::nCellGroups_;

  using BaseType::loopInteriorTraces;
  using BaseType::loopBoundaryTraces;
};


template <class PhysDim>
class XField_CellToTrace<PhysDim, TopoD2> : public XField_CellToTrace_Base<PhysDim,TopoD2>
{
public:
  typedef XField_CellToTrace_Base<PhysDim, TopoD2> BaseType;

  // cppcheck-suppress noExplicitConstructor
  XField_CellToTrace(const XField<PhysDim, TopoD2>& xfld);

protected:
  using BaseType::xfld_;
  using BaseType::CelltoTraceAssociativity_;
  using BaseType::nCellGroups_;

  using BaseType::loopInteriorTraces;
  using BaseType::loopBoundaryTraces;
};


template <class PhysDim>
class XField_CellToTrace<PhysDim, TopoD3> : public XField_CellToTrace_Base<PhysDim,TopoD3>
{
public:
  typedef XField_CellToTrace_Base<PhysDim, TopoD3> BaseType;

  // cppcheck-suppress noExplicitConstructor
  XField_CellToTrace(const XField<PhysDim, TopoD3>& xfld);

protected:
  using BaseType::xfld_;
  using BaseType::CelltoTraceAssociativity_;
  using BaseType::nCellGroups_;

  using BaseType::loopInteriorTraces;
  using BaseType::loopBoundaryTraces;
};

template <class PhysDim>
class XField_CellToTrace<PhysDim, TopoD4> : public XField_CellToTrace_Base<PhysDim,TopoD4>
{
public:
  typedef XField_CellToTrace_Base<PhysDim, TopoD4> BaseType;

  // cppcheck-suppress noExplicitConstructor
  XField_CellToTrace(const XField<PhysDim, TopoD4>& xfld);

protected:
  using BaseType::xfld_;
  using BaseType::CelltoTraceAssociativity_;
  using BaseType::nCellGroups_;

  using BaseType::loopInteriorTraces;
  using BaseType::loopBoundaryTraces;
};

}

#endif  // XFIELD_CELLTOTRACE_H
