// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDGROUPAREA_TRAITS_H
#define FIELDGROUPAREA_TRAITS_H

#include "FieldTypes.h"
#include "FieldAssociativity.h"
#include "Field/Element/Element.h"
#include "Field/Element/ElementAssociativityArea.h"

#include "tools/Surrealize.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Traits for a field group of area topologies
//----------------------------------------------------------------------------//

template<class Tin>
struct FieldGroupAreaTraitsBase
{
  typedef FieldAssociativityBase< Tin > FieldBase;

  typedef Tin T;                                     // DOF type
};

template<class Topology, class T>
struct FieldGroupAreaTraits : public FieldGroupAreaTraitsBase<T>
{
  typedef BasisFunctionAreaBase<Topology> BasisType;
  typedef Topology TopologyType;
  typedef ElementAssociativity<TopoD2,Topology> ElementAssociativityType;
  typedef FieldAssociativityConstructor< typename ElementAssociativityType::Constructor > FieldAssociativityConstructorType;

  template<class ElemT = Real>
  using ElementType = Element< typename Surrealize<ElemT,T>::T, TopoD2, Topology >;
};

} //namespace SANS

#endif //FIELDGROUPAREA_TRAITS_H
