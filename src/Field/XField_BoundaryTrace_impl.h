// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#if !defined(XFIELD_BOUNDARYTRACE_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "XField_BoundaryTrace.h"

#include <vector>
#include <map>

#define MPI_COMMUNICATOR_IN_CPP // this is ok because this an impl file only even included in cpp files for instantiations
#include "MPI/communicator.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/gather.hpp>
#include <boost/mpi/collectives/broadcast.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include "MPI/serialize_DenseLinAlg_MatrixS.h"
#endif

namespace SANS
{

//---------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
template<class TopologyTrace>
void
XField_BoundaryTrace<PhysDim, TopoDim>::
getBoundaryTraceDOFs(std::map<int,VectorX>& bndDOF, const int boundaryTraceGroup )
{
  typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;

  const XFieldTraceGroupType& xfldTraceGroup = xfld_.template getBoundaryTraceGroupGlobal<TopologyTrace>(boundaryTraceGroup);

  std::vector<int> elemMap(xfldTraceGroup.nBasis());

  const int nElem = xfldTraceGroup.nElem();
  for (int elem = 0; elem < nElem; elem++)
  {
    xfldTraceGroup.associativity(elem).getGlobalMapping(elemMap.data(), elemMap.size());

    for (std::size_t i = 0; i < elemMap.size(); i++)
    {
      // get the native DOF index
      int DOFNative = xfld_.local2nativeDOFmap(elemMap[i]);

      // get the unique set of DOFs that make up the boundary elements
      bndDOF[DOFNative] = xfld_.DOF(elemMap[i]);
    }
  }
}

//---------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
void
XField_BoundaryTrace<PhysDim, TopoDim>::
createBoundaryTraceDOFs( std::map<int,VectorX>& bndDOF, std::map<int,int>& localDOFmap, const bool global )
{

#ifdef SANS_MPI
  // reduce accross all processors if needed
  if (global)
  {
    // send all DOFs to rank 0
    std::vector< std::map<int,VectorX> > bndDOFRanks;
    boost::mpi::gather(*xfld_.comm(), bndDOF, bndDOFRanks, 0 );

    // collapse the map down across the ranks removing any possible duplicates
    for (std::size_t rank = 1; rank < bndDOFRanks.size(); rank++ )
      bndDOF.insert(bndDOFRanks[rank].begin(), bndDOFRanks[rank].end());

    // send the unique DOFs back to all other processors
    boost::mpi::broadcast(*xfld_.comm(), bndDOF, 0);
  }
#endif

  this->resizeDOF(bndDOF.size());

  // set all the DOFs for the boundary group
  // and map native indexing to local DOF array indexing
  int iDOF = 0;
  for ( const auto& DOFPair : bndDOF )
  {
    this->DOF(iDOF) = DOFPair.second;
    localDOFmap[DOFPair.first] = iDOF;
    iDOF++;
  }
}

//---------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
template<class TopologyTrace>
void
XField_BoundaryTrace<PhysDim, TopoDim>::
createBoundaryTraceElems( const int groupLocal, const std::map<int,int>& localDOFmap,
                          const int boundaryTraceGroup, const bool global )
{
  typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::FieldAssociativityConstructorType FieldAssociativityConstructorType;
  typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldClass;

  const XFieldTraceGroupType& xfldTraceGroup = xfld_.template getBoundaryTraceGroupGlobal<TopologyTrace>(boundaryTraceGroup);
  const std::vector<int>& boundaryTraceIDs = xfld_.boundaryTraceIDs(boundaryTraceGroup);

  std::vector<int> elemMap(xfldTraceGroup.nBasis());
  ElementXFieldClass xfldElem(xfldTraceGroup.basis());

  std::map<int,std::vector<int>> bndElemMaps;

  const int nElem = xfldTraceGroup.nElem();
  for (int elem = 0; elem < nElem; elem++)
  {
    xfldTraceGroup.associativity(elem).getGlobalMapping(elemMap.data(), elemMap.size());

    // transform to native DOF indexing
    for (std::size_t i = 0; i < elemMap.size(); i++)
      elemMap[i] = xfld_.local2nativeDOFmap(elemMap[i]);

    // save off the element map in native indexing
    bndElemMaps[boundaryTraceIDs[elem]] = elemMap;
  }

#ifdef SANS_MPI
  // reduce across all processors if desired
  if (global)
  {
    // send all element maps to rank 0
    std::vector< std::map<int,std::vector<int>> > bndElemMapsRanks;
    boost::mpi::gather(*xfld_.comm(), bndElemMaps, bndElemMapsRanks, 0 );

    // collapse the map down across the ranks removing any possible duplicates
    for (std::size_t rank = 1; rank < bndElemMapsRanks.size(); rank++ )
      bndElemMaps.insert(bndElemMapsRanks[rank].begin(), bndElemMapsRanks[rank].end());

    // send the maps back to all other processors
    boost::mpi::broadcast(*xfld_.comm(), bndElemMaps, 0);
  }
#endif

  int comm_rank = xfld_.comm()->rank();

  FieldAssociativityConstructorType fldAssocBtrace( xfldTraceGroup.basis(), bndElemMaps.size() );
  this->boundaryTraceBBox_[groupLocal].resize( bndElemMaps.size() );

  int elem = 0;
  for ( const auto& elemPair : bndElemMaps )
  {
    // boundary processor rank to the current processor
    fldAssocBtrace.setAssociativity( elem ).setRank( comm_rank );

    elemMap = elemPair.second;
    // map native to local DOF indexing
    for (std::size_t i = 0; i < elemMap.size(); i++)
      elemMap[i] = localDOFmap.at(elemMap[i]);

    // set the element associativity
    fldAssocBtrace.setAssociativity( elem ).setGlobalMapping( elemMap );
    elem++;
  }

  this->boundaryTraceGroups_[groupLocal] = new FieldTraceGroupType<TopologyTrace>( fldAssocBtrace );
  this->boundaryTraceGroups_[groupLocal]->setDOF(this->DOF_, this->nDOF_);


  typedef typename XField_BoundaryTrace<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupLocalType;

  // save off the bounding box for each element
  const XFieldTraceGroupLocalType& xfldTraceLocal = this->template getBoundaryTraceGroup<TopologyTrace>(groupLocal);

  const int nElemLocal = xfldTraceLocal.nElem();
  for (int elem = 0; elem < nElemLocal; elem++)
  {
    // save off the bounding box of this element
    xfldTraceLocal.getElement( xfldElem, elem );
    this->boundaryTraceBBox_[groupLocal][elem] = xfldElem.boundingBox();
  }
}


} // namespace SANS
