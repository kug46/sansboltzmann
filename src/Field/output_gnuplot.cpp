// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <iostream>
#include <fstream>
#include <iomanip>

#include "output_gnuplot.h"

#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"

#include "XFieldLine.h"
#include "XFieldArea.h"
#include "XFieldVolume.h"

#include "FieldLine.h"
#include "FieldArea.h"
#include "FieldVolume.h"

namespace SANS
{


//----------------------------------------------------------------------------//
// Higher order line element for PhysD1 TopoD1 specialization
//
template<class T>
void
output_gnuplot_CellGroup( const typename XField<PhysD1, TopoD1>::template FieldCellGroupType<Line>& xfld,
                          const typename Field< PhysD1, TopoD1, T >::template FieldCellGroupType<Line>& qfld,
                          std::ofstream &outputFile )
{
  typedef typename XField<PhysD1, TopoD1>::template FieldCellGroupType<Line> XFieldCellGroupType;
  typedef typename Field<PhysD1, TopoD1, T >::template FieldCellGroupType<Line> QFieldCellGroupType;

  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfld.basis() );
  ElementQFieldClass qfldElem( qfld.basis() );

  static const int N = DLA::VectorSize<T>::M;

  int npts = 11;
  int nelem = xfld.nElem();
  // grid coordinates and solution
  typename ElementQFieldClass::RefCoordType sRefCell;
  typename ElementXFieldClass::VectorX X;
  T q;
  // loop over elements within group
  for (int elem = 0; elem < nelem; elem++)
  {
    // copy global grid/solution DOFs to element
    xfld.getElement( xfldElem, elem );
    qfld.getElement( qfldElem, elem );

    for ( int i = 0; i < npts; i++ )
    {
      sRefCell = Real(i)/Real(npts-1);
      xfldElem.eval( sRefCell, X );
      qfldElem.eval( sRefCell, q );

      for (int k = 0; k < PhysD1::D; k++)
        outputFile << X[k];
      for (int k = 0; k < N; k++)
        outputFile << " " << DLA::index(q,k);
      outputFile << std::endl;
    }
  }
}

//----------------------------------------------------------------------------//
template<class T>
void
output_gnuplot( const Field< PhysD1, TopoD1, T >& qfld, const std::string& filename )
{
  std::cout << "output_gnuplot: filename = " << filename << std::endl;
  std::ofstream outputFile(filename, std::ios::out);
  outputFile << std::setprecision(16) << std::scientific;

  // output header
  outputFile << "# X";
  static const int N = DLA::VectorSize<T>::M;
  for (int k = 0; k < N; k++)
    outputFile << " q" << k+1;
  outputFile << std::endl;

  const XField<PhysD1,TopoD1>& xgrid = qfld.getXField();

  const int ngroup = qfld.nCellGroups();

  // loop over cell groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Line) )
    {
      output_gnuplot_CellGroup<T>( xgrid.template getCellGroup<Line>(group),
                                   qfld.template getCellGroup<Line>(group),
                                   outputFile );
    }
    else
    {
      const char msg[] = "Error in output_gnuplot(Field<PhysD1,TopoD1>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }
}

//Explicitly instantiate the function
template void
output_gnuplot( const Field< PhysD1, TopoD1, Real >& Q, const std::string& filename );
template void
output_gnuplot( const Field< PhysD1, TopoD1, DLA::VectorS<1,Real> >& Q, const std::string& filename );
template void
output_gnuplot( const Field< PhysD1, TopoD1, DLA::VectorS<3,Real> >& Q, const std::string& filename );
template void
output_gnuplot( const Field< PhysD1, TopoD1, DLA::VectorS<4,Real> >& Q, const std::string& filename );


} //namespace SANS
