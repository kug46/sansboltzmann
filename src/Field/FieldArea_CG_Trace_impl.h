// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(FIELDAREA_CG_TRACE_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "FieldArea_CG_Trace.h"

#include "Field_CG/Field_CG_TraceConstructor.h"

#include "tools/split_cat_std_vector.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// 2D solution field: CG trace
//----------------------------------------------------------------------------//

template <class PhysDim, class T>
Field_CG_Trace<PhysDim, TopoD2, T>::Field_CG_Trace( const XField<PhysDim, TopoD2>& xfld,
                                                    const int order, const BasisFunctionCategory& category )
  : BaseType( xfld )
{
  init(order ,category, BaseType::createInteriorGroupIndex(), BaseType::createBoundaryGroupIndex());
}

template <class PhysDim, class T>
Field_CG_Trace<PhysDim, TopoD2, T>::Field_CG_Trace( const XField<PhysDim, TopoD2>& xfld,
                                                    const int order,
                                                    const BasisFunctionCategory& category,
                                                    const std::vector<int>& InteriorGroups,
                                                    const std::vector<int>& BoundaryGroups )
  : BaseType( xfld )
{
  BaseType::checkInteriorGroupIndex(InteriorGroups);
  BaseType::checkBoundaryGroupIndex(BoundaryGroups);
  init(order, category, InteriorGroups,BoundaryGroups);
}

template <class PhysDim, class T>
Field_CG_Trace<PhysDim, TopoD2, T>::Field_CG_Trace( const Field_CG_Trace& fld, const FieldCopy& tag )
  : BaseType( fld, tag )
{
}

template <class PhysDim, class T>
Field_CG_Trace<PhysDim, TopoD2, T>&
Field_CG_Trace<PhysDim, TopoD2, T>::operator=( const ArrayQ& q )
{
  Field< PhysDim, TopoD2, T >::operator=(q);
  return *this;
}

//----------------------------------------------------------------------------//
template <class PhysDim, class T>
void
Field_CG_Trace<PhysDim, TopoD2, T>::init(
    const int order, const BasisFunctionCategory& category,
    const std::vector<int>& interiorGroups, const std::vector<int>& boundaryGroups )
{
  SANS_ASSERT_MSG( order >= 1, "CG for interior edge requires order >= 1" );
  SANS_ASSERT_MSG( category == BasisFunctionCategory_Hierarchical ||
                   category == BasisFunctionCategory_Lagrange,
                   "CG area must use Hierarchical or Lagrange Basis" );

  // Convert the groups into sets
  std::vector<std::vector<int>> interiorGroupSets({interiorGroups});
  std::vector<std::vector<int>> boundaryGroupSets({boundaryGroups});

  Field_CG_TraceConstructor<PhysDim, TopoD2> fldConstructor(xfld_, order, category, interiorGroupSets, boundaryGroupSets);

  nElem_ = fldConstructor.nElem();

  // allocate the solution DOF array
  this->resizeDOF(fldConstructor.nDOF());

  std::vector<int> concatInteriorGroups = cat(interiorGroupSets);

  this->resizeInteriorTraceGroups( concatInteriorGroups );

  for (int igroup = 0; igroup < this->nInteriorTraceGroups(); igroup++)
  {
    const int group = interiorGroups[igroup];
    int localGroup = localInteriorTraceGroups_.at(group);

    if ( xfld_.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Line) )
    {
      typedef typename BaseType::template FieldTraceGroupType<Line> FieldTraceGroupClass;

      // allocate group
      interiorTraceGroups_[localGroup] = fldConstructor.template createInteriorTraceGroup<FieldTraceGroupClass>( group, this->local2nativeDOFmap_ );
      interiorTraceGroups_[localGroup]->setDOF( DOF_, nDOF_ );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "Unknown trace element topology" );
  }


  std::vector<int> concatBoundaryGroups = cat(boundaryGroupSets);

  this->resizeBoundaryTraceGroups( concatBoundaryGroups );

  for (int igroup = 0; igroup < this->nBoundaryTraceGroups(); igroup++)
  {
    const int group = boundaryGroups[igroup];
    int localGroup = localBoundaryTraceGroups_.at(group);

    if ( xfld_.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Line) )
    {
      typedef typename BaseType::template FieldTraceGroupType<Line> FieldTraceGroupClass;

      // allocate group
      boundaryTraceGroups_[localGroup] = fldConstructor.template createBoundaryTraceGroup<FieldTraceGroupClass>( group, this->local2nativeDOFmap_ );
      boundaryTraceGroups_[localGroup]->setDOF( DOF_, nDOF_ );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "Unknown area element topology" );
  }
}

}
