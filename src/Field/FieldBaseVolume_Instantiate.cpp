// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define FIELDBASE_INSTANTIATE
#include "FieldBase_impl.h"

#include "FieldVolume.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"

namespace SANS
{

template class FieldBase< FieldTraits<TopoD3, Real> >;

template class FieldBase< FieldTraits<TopoD3, DLA::VectorS<1, Real>> >;
template class FieldBase< FieldTraits<TopoD3, DLA::VectorS<2, Real>> >;
template class FieldBase< FieldTraits<TopoD3, DLA::VectorS<3, Real>> >;
template class FieldBase< FieldTraits<TopoD3, DLA::VectorS<4, Real>> >;
template class FieldBase< FieldTraits<TopoD3, DLA::VectorS<5, Real>> >;
template class FieldBase< FieldTraits<TopoD3, DLA::VectorS<6, Real>> >;
template class FieldBase< FieldTraits<TopoD3, DLA::VectorS<7, Real>> >;

template class FieldBase< FieldTraits<TopoD3, DLA::VectorS<3, DLA::VectorS<4, Real>>> >;
template class FieldBase< FieldTraits<TopoD3, DLA::VectorS<3, DLA::VectorS<5, Real>>> >;
template class FieldBase< FieldTraits<TopoD3, DLA::VectorS<3, DLA::VectorS<6, Real>>> >;

template class FieldBase< FieldTraits<TopoD3, DLA::MatrixSymS<3, Real>> >;
}
