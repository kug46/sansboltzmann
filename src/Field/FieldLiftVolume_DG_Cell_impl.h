// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(FIELDLIFTVOLUME_DG_CELL_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include <vector>
#include <typeinfo>     // typeid

#include "FieldLiftVolume_DG_Cell.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template <class PhysDim, class T>
FieldLift_DG_Cell<PhysDim, TopoD3, T>::
FieldLift_DG_Cell( const XField<PhysDim, TopoD3>& xfld, const int order, const BasisFunctionCategory& category ) : BaseType(xfld)
{
  // By use default all cell groups
  init(order, category, BaseType::createCellGroupIndex() );
}

//----------------------------------------------------------------------------//
template <class PhysDim, class T>
FieldLift_DG_Cell<PhysDim, TopoD3, T>::
FieldLift_DG_Cell( const XField<PhysDim, TopoD3>& xfld, const int order, const BasisFunctionCategory& category, const std::vector<int>& CellGroups )
  : BaseType(xfld)
{
  // Check that the groups asked for are within the range of available groups
  BaseType::checkCellGroupIndex( CellGroups );
  init(order, category, CellGroups);
}

//----------------------------------------------------------------------------//
template <class PhysDim, class T>
FieldLift_DG_Cell<PhysDim, TopoD3, T>:: FieldLift_DG_Cell( const FieldLift_DG_Cell& fld, const FieldCopy& tag ) : BaseType(fld,tag) {}

//----------------------------------------------------------------------------//
template <class PhysDim, class T>
FieldLift_DG_Cell<PhysDim, TopoD3, T>&
FieldLift_DG_Cell<PhysDim, TopoD3, T>::
operator=( const ArrayQ& q )
{
  BaseType::operator=(q);
  return *this;
}

//----------------------------------------------------------------------------//
template <class PhysDim, class T>
void
FieldLift_DG_Cell<PhysDim, TopoD3, T>::init(
    const int order, const BasisFunctionCategory& category, const std::vector<int>& CellGroups )
{
  //allocate the cell groups
  this->resizeCellGroups( CellGroups );

  for (int group = 0; group < this->nCellGroups(); group++)
  {
    if ( xfld_.getCellGroupBase(group).topoTypeID() == typeid(Tet) )
      this->template createCellGroup<Tet>(group, order, category);
    else if ( xfld_.getCellGroupBase(group).topoTypeID() == typeid(Hex) )
      this->template createCellGroup<Hex>(group, order, category);
    else
      SANS_DEVELOPER_EXCEPTION( "Unknown element topology." );
  }

  //Allocate the DOFs array
  this->createDOFs();
}

}
