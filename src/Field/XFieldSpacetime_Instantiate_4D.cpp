// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define FIELDBASE_INSTANTIATE
#include "FieldBase_impl.h"

#define XFIELD_INSTANTIATE
#include "XField_impl.h"

#define XFIELDSPACETIME_INSTANTIATE
#include "XFieldSpacetime.h"
#include "XFieldSpacetime_impl.h"
#include "XFieldSpacetime_buildFrom_impl.h"
#include "XFieldSpacetime_checkGrid_impl.h"
#include "XField_checkTraceNormal_impl.h"

namespace SANS
{

//=============================================================================
template class FieldBase< XFieldTraits<PhysD4, TopoD4> >;

//=============================================================================
//Explicitly instantiate
template class XField< PhysD4, TopoD4 >;

}
