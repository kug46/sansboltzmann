// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define FIELDSPACETIME_CG_BOUNDARYTRACE_INSTANTIATE
#include "FieldSpacetime_CG_BoundaryTrace_impl.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "XFieldSpacetime.h"
#include "FieldSpacetime.h"

namespace SANS
{

template class Field_CG_BoundaryTrace< PhysD4, TopoD4, Real >;

template class Field_CG_BoundaryTrace< PhysD4, TopoD4, DLA::VectorS<2, Real> >;
//template class Field_CG_BoundaryTrace< PhysD4, TopoD4, DLA::VectorS<3, Real> >;
//template class Field_CG_BoundaryTrace< PhysD4, TopoD4, DLA::VectorS<4, Real> >;
//template class Field_CG_BoundaryTrace< PhysD4, TopoD4, DLA::VectorS<5, Real> >;
//template class Field_CG_BoundaryTrace< PhysD4, TopoD4, DLA::VectorS<6, Real> >;

}
