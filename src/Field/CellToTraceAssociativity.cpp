// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "CellToTraceAssociativity.h"
#include "Topology/ElementTopology.h"

namespace SANS
{

template<class Topology>
CellToTraceAssociativity<Topology>::CellToTraceAssociativity(const int nElem) :
  trace_map_(nElem, std::vector<TraceInfo>(Topology::NTrace)) {}

template<class Topology>
CellToTraceAssociativity<Topology>::~CellToTraceAssociativity() {}

template<class Topology>
void
CellToTraceAssociativity<Topology>::setTrace(int cellElem, int canonicalTrace,
                                             int traceGroup, int traceElem, TraceInfo::GroupType type)
{
  SANS_ASSERT( canonicalTrace >= 0 && canonicalTrace < Topology::NTrace );
  trace_map_[cellElem][canonicalTrace].group = traceGroup;
  trace_map_[cellElem][canonicalTrace].elem = traceElem;
  trace_map_[cellElem][canonicalTrace].type = type;
}

template<class Topology>
int
CellToTraceAssociativity<Topology>::nElem() const { return (int) trace_map_.size(); }

template<class Topology>
const TraceInfo&
CellToTraceAssociativity<Topology>::getTrace(const int cellElem, const int canonicalTrace) const
{
  SANS_ASSERT( canonicalTrace >= 0 && canonicalTrace < Topology::NTrace );
  return trace_map_[cellElem][canonicalTrace];
}

//=============================================================================
// Explicit instantiations

template class CellToTraceAssociativity<Line>;
template class CellToTraceAssociativity<Triangle>;
template class CellToTraceAssociativity<Quad>;
template class CellToTraceAssociativity<Tet>;
template class CellToTraceAssociativity<Hex>;
template class CellToTraceAssociativity<Pentatope>;

}
