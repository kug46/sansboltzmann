// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(FIELDAREA_DG_BOUNDARYTRACE_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "FieldArea_DG_BoundaryTrace.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// 2D solution field: DG boundary trace (e.g. Lagrange multiplier)
//----------------------------------------------------------------------------//

template <class PhysDim, class T>
Field_DG_BoundaryTrace<PhysDim, TopoD2, T>::Field_DG_BoundaryTrace( const XField<PhysDim, TopoD2>& xfld,
                                                                    const int order, const BasisFunctionCategory& category )
  : BaseType( xfld )
{
  init(xfld,order,category,BaseType::createBoundaryGroupIndex());
}

template <class PhysDim, class T>
Field_DG_BoundaryTrace<PhysDim, TopoD2, T>::Field_DG_BoundaryTrace( const XField<PhysDim, TopoD2>& xfld,
                                                                    const int order,
                                                                    const BasisFunctionCategory& category, const std::vector<int>& BoundaryGroups )
  : BaseType( xfld )
{
  BaseType::checkBoundaryGroupIndex(BoundaryGroups);
  init(xfld,order,category,BoundaryGroups);
}

template <class PhysDim, class T>
Field_DG_BoundaryTrace<PhysDim, TopoD2, T>::Field_DG_BoundaryTrace( const Field_DG_BoundaryTrace& fld, const FieldCopy& tag ) : BaseType(fld, tag) {}

template <class PhysDim, class T>
Field_DG_BoundaryTrace<PhysDim, TopoD2, T>&
Field_DG_BoundaryTrace<PhysDim, TopoD2, T>::operator=( const ArrayQ& q )
{
  Field< PhysDim, TopoD2, T >::operator=(q);
  return *this;
}

//----------------------------------------------------------------------------//
template <class PhysDim, class T>
void
Field_DG_BoundaryTrace<PhysDim, TopoD2, T>::init(
    const XField<PhysDim, TopoD2>& xfld, const int order,
    const BasisFunctionCategory& category, const std::vector<int>& BoundaryGroups )
{
  for (std::size_t igroup = 0; igroup < BoundaryGroups.size(); igroup++)
  {
    const int group = BoundaryGroups[igroup];

    if ( xfld.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Line) )
      this->template createBoundaryTraceGroup<Line>(group, order, category);
    else
      SANS_DEVELOPER_EXCEPTION( "Unknown element topology" );
  }

  // allocate the solution DOF array and assign it to groups
  this->createDOFs();
}

}
