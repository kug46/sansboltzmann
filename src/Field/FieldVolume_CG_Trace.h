// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDVOLUME_CG_TRACE_H
#define FIELDVOLUME_CG_TRACE_H

#include "FieldVolume.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// solution field: CG trace (e.g. interface solution)
//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//
// 3D solution field: CG trace
//----------------------------------------------------------------------------//

template <class PhysDim, class T>
class Field_CG_Trace< PhysDim, TopoD3, T > : public Field< PhysDim, TopoD3, T >
{
public:
  typedef Field< PhysDim, TopoD3, T > BaseType;
  typedef T ArrayQ;

  Field_CG_Trace( const XField<PhysDim, TopoD3>& xfld, const int order, const BasisFunctionCategory& category );

  Field_CG_Trace( const XField<PhysDim, TopoD3>& xfld, const int order,
                  const BasisFunctionCategory& category, const std::vector<int>& InteriorGroups,
                  const std::vector<int>& BoundaryGroups );

  Field_CG_Trace( const Field_CG_Trace& fld, const FieldCopy& tag );
  Field_CG_Trace& operator=( const ArrayQ& q );

  // Virtual class to denote the SpaceType of the Field
  virtual SpaceType spaceType() const override { return SpaceType::Continuous; }

protected:
  using BaseType::xfld_;
  using BaseType::nDOF_;
  using BaseType::DOF_;
  using BaseType::nElem_;
  using BaseType::interiorTraceGroups_;
  using BaseType::boundaryTraceGroups_;
  using BaseType::localBoundaryTraceGroups_;
  using BaseType::localInteriorTraceGroups_;

  void init(const int order, const BasisFunctionCategory& category,
            const std::vector<int>& InteriorGroups, const std::vector<int>& BoundaryGroups);
};

}

#endif  // FIELDVOLUME_CG_TRACE_H
