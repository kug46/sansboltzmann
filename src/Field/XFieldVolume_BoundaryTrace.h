// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELDVOLUME_BOUNDARYTRACE_H
#define XFIELDVOLUME_BOUNDARYTRACE_H

#include "XField_BoundaryTrace.h"
#include "XFieldVolume.h"
#include "FieldAssociativity.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Topologically 3D grid
//----------------------------------------------------------------------------//

template<class PhysDim>
struct XFieldTraits_BoundaryTrace<PhysDim, TopoD3>
{
  typedef TopoD3 TopoDim;
  static const int D = PhysDim::D;                 // physical dimensions

  typedef DLA::VectorS<D,Real> VectorX;   // coordinates vector
  typedef VectorX T;

  typedef typename XFieldAreaTraits<PhysDim, TopoD3>::FieldBase FieldTraceGroupBase;
  typedef typename XFieldVolumeTraits<PhysDim, TopoD3>::FieldBase FieldCellGroupBase;

  template<class Topology>
  using FieldTraceGroupType = FieldAssociativity< XFieldGroupAreaTraits<PhysDim, TopoD3, Topology> >;

  template<class Topology>
  using FieldCellGroupType = FieldAssociativity< XFieldGroupVolumeTraits<PhysDim, TopoD3, Topology> >;
};

} // namespace SANS

#endif //XFIELDVOLUME_BOUNDARYTRACE_H
