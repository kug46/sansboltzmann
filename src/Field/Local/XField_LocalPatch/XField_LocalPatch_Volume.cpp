// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XField_LocalPatch_impl.h"

#include "Field/XFieldVolume.h"

namespace SANS
{

template class XField_LocalPatch<PhysD3,Tet>;
INSTANTIATE_ALL_WITH_EDGE(PhysD3,Tet)

DISALLOW_LOCAL_PATCH(PhysD3,Hex)

}
