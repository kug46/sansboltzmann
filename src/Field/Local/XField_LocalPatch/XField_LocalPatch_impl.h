// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BasisFunction/ElementEdges.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/Element/UniqueElem.h"
#include "Field/Element/UniqueElemHash.h"
#include <unordered_set>

#include "Field/Field_NodalView.h"
#include "Field/XField.h"

#include "Field/Local/XField_LocalPatch.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

// helper function to retrieve the index of a particular value in a vector
// there's probably a standard library function for this...
inline int
indexof( const std::vector<int>& v, int p )
{
  for (std::size_t i=0;i<v.size();i++)
  {
    if (v[i]==p)
      return i;
  }
  return -1;
}

class TraceNoodle : public UniqueElem
{
public:
  TraceNoodle( const TopologyTypes& topo, const std::vector<int>& dof, int grp, int elm, bool bnd, int p ) :
    UniqueElem(topo,dof),
    group(grp),
    elem(elm),
    boundary(bnd),
    node(p)
  {}

  TraceNoodle( const TopologyTypes& topo, std::vector<int>&& dof, int grp, int elm, bool bnd, int p ) :
    UniqueElem(topo,std::move(dof)),
    group(grp),
    elem(elm),
    boundary(bnd),
    node(p)
  {}

  friend std::ostream& operator<<( std::ostream& out, const TraceNoodle& trace )
  {
    out << static_cast<UniqueElem>(trace) << " boundary = ";
    if (trace.boundary) out << "true";
    else out << "false";
    out << " elem = " << trace.elem << " group = " << trace.group;
    return out;
  }

  int hasEdge( int e0, int e1 ) const
  {
    if (std::find(sortedNodes_.begin(),sortedNodes_.end(),e0) == sortedNodes_.end()) return false;
    if (std::find(sortedNodes_.begin(),sortedNodes_.end(),e1) == sortedNodes_.end()) return false;
    return true;
  }

  // public members
  int group,elem; // trace element and trace group identifiers
  bool boundary;
  int node; // the node from the split edge which is on this split trace
};

template<class PhysDim,class Topology>
class TraceSoup
{
public:
  typedef typename Topology::TopologyTrace TraceTopology;
  typedef typename Topology::TopoDim TopoDim;
  typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TraceTopology> XFieldTraceGroupType;

  typedef std::unordered_set< TraceNoodle, boost::hash<UniqueElem> > TraceNoodleSet;

  TraceSoup() : e0_(-1), e1_(-1) {}

  TraceSoup( const XField_LocalPatchConstructor<PhysDim,Topology>& xfld, int e0, int e1, int es ) :
    e0_(e0), e1_(e1)
  {
    // loop through the interior trace groups
    for (int igroup = 0; igroup < xfld.nInteriorTraceGroups(); igroup++)
    {
      const XFieldTraceGroupType& group = xfld.template getInteriorTraceGroup<TraceTopology>(igroup);
      for (int ielem=0;ielem<group.nElem();ielem++)
      {
        std::vector<int> traceNodes( TraceTopology::NNode, -1 );
        group.associativity(ielem).getNodeGlobalMapping( traceNodes.data(), traceNodes.size() );
        unsplit_.emplace( TraceTopology::Topology, std::move(traceNodes), igroup, ielem, false, -1 );
      }
    }

    for (int igroup = 0; igroup < xfld.nBoundaryTraceGroups(); igroup++)
    {
      const XFieldTraceGroupType& group = xfld.template getBoundaryTraceGroup<TraceTopology>(igroup);
      for (int ielem=0;ielem<group.nElem();ielem++)
      {
        std::vector<int> traceNodes( TraceTopology::NNode, -1 );
        group.associativity(ielem).getNodeGlobalMapping( traceNodes.data(), traceNodes.size() );
        unsplit_.emplace( TraceTopology::Topology, std::move(traceNodes), igroup, ielem, true, -1 );
      }
    }

    int nGhost = xfld.nGhostBoundaryTraceGroups();
    SANS_ASSERT( nGhost==1 );
    const XFieldTraceGroupType& group = xfld.template getGhostBoundaryTraceGroup<TraceTopology>(0);
    for (int ielem=0;ielem<group.nElem();ielem++)
    {
      std::vector<int> traceNodes( TraceTopology::NNode, -1 );
      group.associativity(ielem).getNodeGlobalMapping( traceNodes.data(), traceNodes.size() );
      unsplit_.emplace( TraceTopology::Topology, std::move(traceNodes), -1, ielem, true, -1 );
    }

    // construct the split traces
    split(e0,e1,es);
  }

  // split all the traces according to the edges e0 and e1
  void split( int e0, int e1, int es )
  {
    std::vector<int> sortedNodes( TraceTopology::NNode, -1 );

    // go through all the unsplit traces and see if they are split
    for (const TraceNoodle& trace : unsplit_)
    {
      // check if this trace has the edge e0-e1
      if (trace.hasEdge(e0, e1))
      {
        // create the two split TraceNoodles
        std::copy(trace.sortedNodes().begin(), trace.sortedNodes().end(), sortedNodes.begin());

        // adjust the traceNodes
        int idx = indexof( sortedNodes, e0 );
        sortedNodes[idx] = es;
        split_.emplace( TraceTopology::Topology, sortedNodes, trace.group, trace.elem, trace.boundary, e1 );

        sortedNodes[idx] = e0;
        idx = indexof( sortedNodes, e1 );
        sortedNodes[idx] = es;
        split_.emplace( TraceTopology::Topology, sortedNodes, trace.group, trace.elem, trace.boundary, e0 );
      }
    }
  }

  ElementSplitFlag lookup( const std::vector<int>& traceNodes, int& group, int& elem, bool& bnd, int& vtx ) const
  {
    group = elem = vtx = -1;
    bnd = false;

    // create a temporary trace to hold the DOF for comparison
    TraceNoodle trace(TraceTopology::Topology, traceNodes, -1, -1, false, -1 );
    TraceNoodleSet::const_iterator it;

    // look for the trace in the unsplit set
    it = unsplit_.find( trace );
    if (it != unsplit_.end()) // Unsplit
    {
      group = it->group;
      elem  = it->elem;
      bnd   = it->boundary;
      vtx   = it->node;
      return ElementSplitFlag::Unsplit;
    }

    // look for the trace in the split set
    it = split_.find( trace );
    if (it != split_.end()) // Split
    {
      group = it->group;
      elem  = it->elem;
      bnd   = it->boundary;
      vtx   = it->node;
      return ElementSplitFlag::Split;
    }

    // neither the split or unsplit containers have this trace, so it's new
    return ElementSplitFlag::New;
  }

  void print() const
  {
    printf("there are %lu unsplit traces in soup\n", unsplit_.size());
    for (const TraceNoodle& noodle : unsplit_)
      std::cout << noodle << std::endl;

    printf("there are %lu split traces in soup\n", split_.size());
    for (const TraceNoodle& noodle : split_)
      std::cout << noodle << std::endl;
  }

  int e0() const { return e0_; }
  int e1() const { return e1_; }

private:
  TraceNoodleSet unsplit_;
  TraceNoodleSet split_;
  int e0_,e1_; // edge endpoints
};

namespace XField_LocalPatch_Utils
{

template<class PhysDim,class Topology>
void
allWithEdge( const XField<PhysDim,typename Topology::TopoDim>& xfld,
             const Field_NodalView* xfld_nodalView,
             int e0, int e1, std::set<std::pair<int,int>>& cavity )
{
  if (e1 < e0) std::swap(e0,e1);

  typedef typename Topology::TopoDim TopoDim;
  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename Field_NodalView::IndexVector GroupElemVector;

  const int (*edges)[Line::NNode] = ElementEdges<Topology>::EdgeNodes;

  std::vector<int> elemNodes( Topology::NNode );

  if (xfld_nodalView==NULL)
  {
    // loop through cell groups and cells
    for (int igroup=0;igroup<xfld.nCellGroups();igroup++)
    {
      XFieldCellGroupType cellGroup = xfld.template getCellGroup<Topology>(igroup);
      for (int ielem=0;ielem<cellGroup.nElem();ielem++)
      {
        cellGroup.associativity(ielem).getNodeGlobalMapping( elemNodes.data(), elemNodes.size() );
        for (int iedge=0;iedge<Topology::NEdge;iedge++)
        {
          int n0 = elemNodes[ edges[iedge][0] ];
          int n1 = elemNodes[ edges[iedge][1] ];

          if (n1 < n0) std::swap(n0,n1);

          if (n0==e0 && n1==e1)
          {
            cavity.insert( {igroup,ielem} );
            break;
          }
        }
      }
    }
  }
  else
  {
    // use the nodal view around vertices e0 and e1
    GroupElemVector cells0( xfld_nodalView->getCellList(e0) );
    GroupElemVector cells1( xfld_nodalView->getCellList(e1) );

    // compute the set intersection of cells0 and cells1
    GroupElemVector cells01;
    std::set_intersection( cells0.begin(), cells0.end(), cells1.begin(), cells1.end(), std::back_inserter(cells01) );

    for (GroupElemVector::const_iterator it = cells01.begin();it!=cells01.end();it++)
      cavity.insert( {it->group,it->elem} );

    SANS_DEVELOPER_EXCEPTION("not tested");
  }
}

} // XField_LocalPatch_Utils

// constructor from a global xfield -- Attached to an edge (global index numbering)
// This function is single step creating an constructor class internally
template<class PhysDim, class Topology>
XField_LocalPatch<PhysDim,Topology>::XField_LocalPatch( mpi::communicator& comm,
                                                        const XField_CellToTrace<PhysDim,TopoDim>& connectivity,
                                                        const std::array<int,2> edge, SpaceType type,
                                                        const Field_NodalView* nodalview  ) :
  XField_LocalPatchBase<PhysDim,Topology>( std::make_shared<mpi::communicator>( comm ), connectivity.getXField(), connectivity, nodalview )
{
  spaceType_ = type;

  // Constructor for an edge patch
  XField_LocalPatchConstructor<PhysDim,Topology> xfld_unsplit( comm, connectivity, edge, type, nodalview );

  this->global2localDOFmap_ = xfld_unsplit.global2localDOFmap();

  // Split the patch
  split( xfld_unsplit, edge[0], edge[1] );
  linearCommonNodes_ = xfld_unsplit.getLinearCommonNodes(); // extract the common nodes from the unsplit grid
  edgeMode_ = true;
}

// constructor for a particular split of an existing local patch -- using global edge dofs
template<class PhysDim,class Topology>
XField_LocalPatch<PhysDim,Topology>::XField_LocalPatch( const XField_LocalPatchConstructor<PhysDim,Topology>& xfld_local,
                                                        const int e0, const int e1 ) :
  XField_LocalPatchBase<PhysDim,Topology>( xfld_local.comm(), xfld_local.xfld_global(), xfld_local.connectivity(), xfld_local.xfld_nodalView() )
{
  SANS_ASSERT_MSG( xfld_local.mainCellNodes().size() == 0, "the wrong constructor was used for the extracted local patch");

  this->global2localDOFmap_ = xfld_local.global2localDOFmap();

  split( xfld_local, e0, e1 );
}

// constructor for a particular split of an existing local patch -- using global edge dofs
template<class PhysDim,class Topology>
XField_LocalPatch<PhysDim,Topology>::XField_LocalPatch( const XField_LocalPatchConstructor<PhysDim,Topology>& xfld_local,
                                                        const std::array<int,2>& edge ) :
  XField_LocalPatchBase<PhysDim,Topology>( xfld_local.comm(), xfld_local.xfld_global(), xfld_local.connectivity(), xfld_local.xfld_nodalView() )
{
  SANS_ASSERT_MSG( xfld_local.mainCellNodes().size() == 0, "the wrong constructor was used for the extracted local patch");

  this->global2localDOFmap_ = xfld_local.global2localDOFmap();

  split( xfld_local, edge[0], edge[1] );
}

// constructor for a particular split of an existing local patch
template<class PhysDim,class Topology>
XField_LocalPatch<PhysDim,Topology>::XField_LocalPatch( const XField_LocalPatchConstructor<PhysDim,Topology>& xfld_local,
                                                        ElementSplitType split_type, int split_edge_index ) :
  XField_LocalPatchBase<PhysDim,Topology>( xfld_local.comm(), xfld_local.xfld_global(), xfld_local.connectivity(), xfld_local.xfld_nodalView() )
{
  if ( typeid(Topology) != typeid(Line) )
  {
    SANS_ASSERT( split_type == ElementSplitType::Edge ); // only edge splits are currently supported
  }
  else
  {
    split_edge_index = 0;
  }

  const std::vector<int>& mainCell = xfld_local.mainCellNodes();
  SANS_ASSERT_MSG( mainCell.size() > 0, "the wrong constructor was used for the local patch" );

  this->global2localDOFmap_ = xfld_local.global2localDOFmap();

  const int (*edges)[Line::NNode] = ElementEdges<Topology>::EdgeNodes;

  int ge0 = mainCell[ edges[split_edge_index][0] ];
  int ge1 = mainCell[ edges[split_edge_index][1] ];

  split( xfld_local, ge0, ge1 );
}

// constructor that does not do any splitting. Used for unit testing
template<class PhysDim,class Topology>
XField_LocalPatch<PhysDim,Topology>::XField_LocalPatch( const XField_LocalPatchConstructor<PhysDim,Topology>& xfld_local ) :
  XField_LocalPatchBase<PhysDim,Topology>( xfld_local.comm(), xfld_local.xfld_global(), xfld_local.connectivity(), xfld_local.xfld_nodalView() )
{
  this->CellMapping_          = xfld_local.getGlobalCellMap();
  this->InteriorTraceMapping_ = xfld_local.getGlobalInteriorTraceMap();
  this->BoundaryTraceMapping_ = xfld_local.getGlobalBoundaryTraceMap();

  this->CellSplitInfo_          = xfld_local.getCellSplitInfo();
  this->InteriorTraceSplitInfo_ = xfld_local.getInteriorTraceSplitInfo();
  this->BoundaryTraceSplitInfo_ = xfld_local.getBoundaryTraceSplitInfo();

  // The local2global mappings
  local2globalBoundaryGroupMap_ = xfld_local.local2globalBoundaryGroupMap();
  boundaryGroupMap_             = xfld_local.boundaryGroupMap();
  global2localDOFmap_           = xfld_local.global2localDOFmap();

  spaceType_ = xfld_local.spaceType();

  edgeMode_ = xfld_local.isEdgeMode();
  if (edgeMode_)
    linearCommonNodes_ = xfld_local.getLinearCommonNodes();

  DOF_buffer_ = xfld_local.DOF_buffer();

  if (order_ == 1)
  {
    // linear grids can simply be cloned
    this->BaseType::cloneFrom(xfld_local);
  }
  else
  {
    // otherwise construct high-order and copy DOFs from global
    this->buildFrom(xfld_local, order_, xfld_global_.template getCellGroup<Topology>(0).basis()->category() );


    // copy high order DOF from the global xfield to the local
    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    for (int group = 0; group < this->nCellGroups(); group++)
    {
      XFieldCellGroupType& cellGroup_local = getCellGroup(group);
      ElementXFieldClass xfldElem_global( cellGroup_local.basis() );
      ElementXFieldClass xfldElem_local( cellGroup_local.basis() );

      Element_Projector_L2<TopoDim,Topology> projector(cellGroup_local.basis());

      for (int elem = 0; elem < cellGroup_local.nElem(); elem++)
      {
        // retrieve the split info
        ElementSplitInfo info = this->getCellSplitInfo({group,elem});
        SANS_ASSERT( info.split_flag == ElementSplitFlag::Unsplit );

        // retrieve the global element and assign it locally
        std::pair<int,int> globalElem = this->getGlobalCellMap( {group, elem} );
        xfld_global_.template getCellGroup<Topology>( globalElem.first ).getElement( xfldElem_global, globalElem.second );

        // get the local trace orientation
        xfldElem_local.setTraceOrientation(cellGroup_local.associativity(elem).traceOrientation());

        // project the solution
        projector.project(xfldElem_global, xfldElem_local);

        // set the projected element
        cellGroup_local.setElement( xfldElem_local, elem );
      }
    }
  }

  // check that the grid is valid
  this->checkGrid();

  this->setResolveGroups();
}

template<class PhysDim,class Topology>
void
XField_LocalPatch<PhysDim,Topology>::
classifyBoundaryTraces( const std::vector< UniqueTraceMap >& uniqueTraces,
                        const TraceSoup<PhysDim,Topology>& soup )
{
  // we need to figure out which of the leftover uniqueTraces are on actual
  // boundaries of the global xfld (placed in boundaryGroups)
  // and which are on boundaries of the local xfield (placed in ghost boundary groups)
  int bgroup;
  int dummy[2];
  bool boundary;
  for (std::size_t igroup = 0; igroup< uniqueTraces.size(); igroup++)
  {
    // construct a sorted set of unique traces to guarantee consistency due to hashing with unordered_map
    std::map< UniqueElem, UniqueTrace > sortedTraces(uniqueTraces[igroup].begin(), uniqueTraces[igroup].end());

    for (const std::pair<const UniqueElem, UniqueTrace>& trace : sortedTraces)
    {
      // lookup the trace soup element
      soup.lookup( trace.first.sortedNodes(), bgroup, dummy[0], boundary, dummy[1] );

      // single-counted traces *must* be on the boundary of the patch
      SANS_ASSERT( boundary );
      if (bgroup >= 0)
        this->addBoundaryTrace( igroup, trace.second, boundaryLagrangeGroups_[bgroup] );
      else
        this->addBoundaryTrace( igroup, trace.second, ghostBoundaryLagrangeGroups_[0] );
    }
  }

}

template<class Topology>
ElementSplitInfo
getSplitInfo( const std::vector<int>& dof, const std::vector<int>& dof_split, int e0, int e1 )
{
  // get the edge index in the original unsplit dof
  const int edge_index = ElementEdges<Topology>::getCanonicalEdge( std::array<int,2>{{e0,e1}} , dof );

  const int (*EdgeNodes)[Line::NNode] = ElementEdges<Topology>::EdgeNodes;
  const bool reversedEdge = ( dof[EdgeNodes[edge_index][0]] == e0 ) ? false : true;

  int subcell;
  if (reversedEdge)
  {
    // edge is in the reverse direction
    // if the split_dof has e0 (make sure it doesn't have e1)
    // then the subcell is 1
    if (indexof(dof_split,e0)>=0)
    {
      SANS_ASSERT(indexof(dof_split,e1)<0);
      subcell = 1;
    }
    else
    {
      // else it is subcell 0
      SANS_ASSERT(indexof(dof_split,e1)>=0);
      subcell = 0;
    }
  }
  else
  {
    // edge is aligned with the dof
    // if the split_dof has e0 (make sure it doesn't have e1)
    // then the subcell is 0
    if (indexof(dof_split,e0)>=0)
    {
      SANS_ASSERT(indexof(dof_split,e1)<0);
      subcell = 0;
    }
    else
    {
      // else it is subcell 1
      SANS_ASSERT(indexof(dof_split,e1)>=0);
      subcell = 1;
    }
  }
  //print_inline(dof,"\tdof");
  //print_inline(dof_split,"\tdof_split");
  //printf("\t\tedge index = %d, subcell = %d\n",edge_index,subcell);
  return ElementSplitInfo(ElementSplitFlag::Split,ElementSplitType::Edge,edge_index,subcell);
}

template<>
inline ElementSplitInfo
getSplitInfo<Node>( const std::vector<int>& dof, const std::vector<int>& dof_split, int e0, int e1 )
{
  return ElementSplitInfo(ElementSplitFlag::Split,ElementSplitType::Edge,0,0);
}

template<class PhysDim,class Topology>
void
XField_LocalPatch<PhysDim,Topology>::findTraceMappings_split( const XField_LocalPatchConstructor<PhysDim,Topology>& xfld_local,
                                                              const TraceSoup<PhysDim,Topology>& soup, int es )
{
  // loop through the interior trace groups
  std::vector<int> traceNodes( TraceTopology::NNode );
  std::vector<int> traceNodes_unsplit( TraceTopology::NNode );
  int group0,elem0;
  bool bnd0;
  int node;

  for (int igroup = 0; igroup < this->nInteriorTraceGroups(); igroup++)
  {
    const XFieldTraceGroupType& traceGroup = getInteriorTraceGroup(igroup);
    for (int ielem = 0; ielem < traceGroup.nElem(); ielem++)
    {
      traceGroup.associativity(ielem).getNodeGlobalMapping( traceNodes.data(), traceNodes.size() );

      ElementSplitFlag flag = soup.lookup( traceNodes, group0, elem0, bnd0, node );
      SANS_ASSERT( bnd0 == false );

      if (flag == ElementSplitFlag::Unsplit)
      {
        // the retrieved trace is unsplit
        std::pair<int,int> global_info = xfld_local.getGlobalInteriorTraceMap( {group0,elem0} );

        this->InteriorTraceMapping_[ {igroup,ielem} ]   = global_info;
        this->InteriorTraceSplitInfo_[ {igroup,ielem} ] = ElementSplitInfo( ElementSplitFlag::Unsplit );
      }
      else if (flag == ElementSplitFlag::Split)
      {
        // the retrieved trace is split
        std::pair<int,int> global_info = xfld_local.getGlobalInteriorTraceMap( {group0,elem0} );

        // get the unsplit trace nodes
        const XFieldTraceGroupType& group_local = xfld_local.template getInteriorTraceGroup<TraceTopology>(group0);
        group_local.associativity(elem0).getNodeGlobalMapping(traceNodes_unsplit.data(), traceNodes_unsplit.size());

        ElementSplitInfo info = getSplitInfo<TraceTopology>( traceNodes_unsplit, traceNodes, soup.e0(), soup.e1() );

        this->InteriorTraceMapping_[ {igroup,ielem} ]   = global_info;
        this->InteriorTraceSplitInfo_[ {igroup,ielem} ] = info;
      }
      else
      {
        // trace was not found...it's New and doesn't map to anything
        this->InteriorTraceMapping_[ {igroup,ielem} ]   = {-1,-1}; // probably not necessary
        this->InteriorTraceSplitInfo_[ {igroup,ielem} ] = ElementSplitInfo( ElementSplitFlag::New, 0 );
      }
    }
  }

  // loop through the boundary trace groups
  for (int igroup = 0; igroup < XField_Type::nBoundaryTraceGroups(); igroup++)
  {
    const XFieldTraceGroupType& traceGroup = XField_Type::template getBoundaryTraceGroup<TraceTopology>(igroup);
    for (int ielem = 0; ielem < traceGroup.nElem(); ielem++)
    {
      traceGroup.associativity(ielem).getNodeGlobalMapping( traceNodes.data(), traceNodes.size() );

      ElementSplitFlag flag = soup.lookup( traceNodes, group0, elem0, bnd0, node );
      SANS_ASSERT( bnd0 == true );

      if (flag == ElementSplitFlag::Unsplit)
      {
        // add the unsplit trace
        std::pair<int,int> global_info = xfld_local.getGlobalBoundaryTraceMap( {group0,elem0} );

        this->BoundaryTraceMapping_[ {igroup,ielem} ]   = global_info;
        this->BoundaryTraceSplitInfo_[ {igroup,ielem} ] = ElementSplitInfo( ElementSplitFlag::Unsplit );
      }
      else if (flag == ElementSplitFlag::Split)
      {
        std::pair<int,int> global_info = xfld_local.getGlobalBoundaryTraceMap( {group0,elem0} );

        // get the unsplit trace nodes
        const XFieldTraceGroupType& group_local = xfld_local.template getBoundaryTraceGroup<TraceTopology>(group0);
        group_local.associativity(elem0).getNodeGlobalMapping(traceNodes_unsplit.data(), traceNodes_unsplit.size());

        ElementSplitInfo info = getSplitInfo<TraceTopology>( traceNodes_unsplit, traceNodes, soup.e0(), soup.e1() );

        // retrieve the global trace dof
        this->BoundaryTraceMapping_[ {igroup,ielem} ]   = global_info;
        this->BoundaryTraceSplitInfo_[ {igroup,ielem} ] = info;
      }
      else
      {
        // trace was not found...it's New and doesn't map to anything
        this->BoundaryTraceMapping_[ {igroup,ielem} ]   = {-1,-1}; // probably not necessary
        this->BoundaryTraceSplitInfo_[ {igroup,ielem} ] = ElementSplitInfo( ElementSplitFlag::New, 0 ); // 0 because it is an edge split
      }
    }
  }
}


template<class PhysDim,class Topology>
void
XField_LocalPatch<PhysDim,Topology>::
split( const XField_LocalPatchConstructor<PhysDim,Topology>& xfld_local_, int ge0, int ge1 )
{
  // convert the global edge indexing to local index
  int e0 = this->global2localDOFmap(ge0);
  int e1 = this->global2localDOFmap(ge1);

  // copy over the space type so we know how to assign boundaries
  spaceType_ = xfld_local_.spaceType();

  // copy over the DOF from the local xfld
  // NOTE: they must be in the same order as the local xfld for the trace comparison to work
  DOF_buffer_ = xfld_local_.DOF_buffer();
  DOF_buffer_.resize(xfld_local_.nDOF()+1);

  // add the split node dof: TODO map split coordinates from reference space
  int dof_split = xfld_local_.nDOF();

  // get the split coordinates using the reference element
  DOF_buffer_.back() = GlobalDOFType( dof_split,
                                      0.5*( xfld_local_.DOF(e0) + xfld_local_.DOF(e1) ) );

  // find all elements in the local xfield with this edge
  std::set<std::pair<int,int>> cavity;
  XField_LocalPatch_Utils::allWithEdge<PhysDim,Topology>( xfld_local_, NULL, e0, e1, cavity );

  // create a local xfield patch of the cavity cells so we can extract the boundary (into uniqueTraces_cavity)
  std::vector<int> elemDOF;

  // add all resolve cells which are not in the split set to this patch
  int resolveCounter = 0;
  for (int i = 0; i < xfld_local_.nResolve(); i++)
  {
    if (cavity.find( {RESOLVE_CELL_GROUP,i} )!=cavity.end()) continue;

    xfld_local_.getElement( RESOLVE_CELL_GROUP, i, elemDOF );
    this->addElement( RESOLVE_CELL_GROUP, resolveCounter, elemDOF );

    std::pair<int,int> global_info = xfld_local_.getGlobalCellMap( {RESOLVE_CELL_GROUP,i} );
    this->CellMapping_.insert( { {RESOLVE_CELL_GROUP,resolveCounter}, global_info } );
    this->CellSplitInfo_[ {RESOLVE_CELL_GROUP,resolveCounter} ] = ElementSplitInfo( ElementSplitFlag::Unsplit );
    resolveCounter++;
  }

  // add all fixed cells which are not in the split set to this patch
  int fixedCounter = 0;
  for (int i = 0; i < xfld_local_.nFixed(); i++)
  {
    if (cavity.find( {FIXED_CELL_GROUP,i} )!=cavity.end()) continue;

    xfld_local_.getElement( FIXED_CELL_GROUP, i, elemDOF );
    this->addElement( FIXED_CELL_GROUP, fixedCounter, elemDOF );

    std::pair<int,int> global_info = xfld_local_.getGlobalCellMap( {FIXED_CELL_GROUP,i} );
    this->CellMapping_.insert( { {FIXED_CELL_GROUP,fixedCounter}, global_info } );
    this->CellSplitInfo_[ {FIXED_CELL_GROUP,fixedCounter} ] = ElementSplitInfo( ElementSplitFlag::Unsplit );
    fixedCounter++;
  }

  // add all new cells formed by the split
  // this is done by creating two new elements for each element in the cavity
  // the first is created by modifying the index of the elem dof which has e0 (the first edge node)
  // the second is created by modifying the index of the elem dof which has e1 (the second edge node)

  const int (*EdgeNodes)[Line::NNode] = ElementEdges<Topology>::EdgeNodes;
  for (const std::pair<int,int>& group_elem : cavity)
  {
    // retrieve the local cavity element and the map to the global xfield
    int group = group_elem.first;
    int elem = group_elem.second;
    std::pair<int,int> global_info = xfld_local_.getGlobalCellMap( group_elem );

    int elemID = (group==RESOLVE_CELL_GROUP) ? resolveCounter : fixedCounter;

    // retrieve the DOF of the local xfield
    xfld_local_.getElement( group, elem, elemDOF );
    std::vector<int> elemDOForiginal = elemDOF;

    const int split_edge = ElementEdges<Topology>::getCanonicalEdge( std::array<int,2>{{e0,e1}} , elemDOForiginal );

    const bool reversedEdge = ( elemDOForiginal[EdgeNodes[split_edge][0]] == e0 ) ? false : true;

    // create the first split element by modifying the index of the first edge node
    int idx = indexof( elemDOF, e0 );
    SANS_ASSERT( idx>=0 );

    // save the node (we need to restore the dof before creating the second split)
    int v0 = elemDOF[idx];

    // if edge is reversed, being attached to e1 is to be subcell 0
    int subcell_index = reversedEdge ? 0 : 1;

    // add the element
    elemDOF[idx] = dof_split;
    this->addElement( group, elemID, elemDOF );
    this->CellMapping_.insert( { {group,elemID}, global_info } );

    // create the elem split info
    this->CellSplitInfo_[ {group,elemID} ] = ElementSplitInfo( ElementSplitFlag::Split, ElementSplitType::Edge, split_edge, subcell_index );

    // restore the node
    elemDOF[idx] = v0;

    // create the second split element by modifying the index of the second edge node
    idx = indexof( elemDOF, e1 );
    SANS_ASSERT( idx>=0 );

    // if edge is reversed, being attached to e0 is to be subcell 1
    subcell_index = reversedEdge ? 1 : 0;

    elemDOF[idx] = dof_split;
    this->addElement( group, elemID+1, elemDOF ); // +1 because this is the second split element
    this->CellMapping_.insert( { {group,elemID+1}, global_info } );

    // create the elem split info
    this->CellSplitInfo_[ {group,elemID+1} ] = ElementSplitInfo( ElementSplitFlag::Split, ElementSplitType::Edge, split_edge, subcell_index );

    // increment the counter by 2 since we created two new elements
    if (group==RESOLVE_CELL_GROUP) resolveCounter += 2;
    else fixedCounter += 2;
  }

  this->newNodeDOFs_.push_back( dof_split ); // this gets changed to higher order node numbering (line 771)
  this->newLinearNodeDOFs_.push_back( dof_split ); // this does not get changed to higher order node numbering

  // connect the interior trace groups
  // if you look in XField_Lagrange.cpp you'll see that the traces
  // will be automatically oriented with elements in group0 to the left
  // for traces that border both group0 and group1
  std::vector< UniqueTraceMap > uniqueTraces(cellGroups_.size());
  connectInteriorGroups(cellGroups_, uniqueTraces, interiorLagrangeGroups_);

  // build a trace soup which will contain both unsplit and split traces
  TraceSoup<PhysDim,Topology> soup(xfld_local_, e0, e1, dof_split);

  // determine which leftover uniqueTraces are on boundaries using the soup
  // first create the boundary groups identical to those in the unsplit xfld
  for (int i = 0; i < xfld_local_.nBoundaryTraceGroups(); i++)
  {
    // get the left group and check the right group is void
    int groupL = xfld_local_.getBoundaryTraceGroupBase(i).getGroupLeft();
    SANS_ASSERT( xfld_local_.getBoundaryTraceGroupBase(i).getGroupRight()==-1 );
    const TraceGroupInfo info( TraceTopology::Topology, STRAIGHT_SIDED, groupL, -1 );
    boundaryLagrangeGroups_.emplace_back( info );
    local2globalBoundaryGroupMap_.insert( {i,xfld_local_.globalBoundaryGroup(i)} );
  }
  classifyBoundaryTraces( uniqueTraces, soup );

  // creates the DOF, builds the cell groups, interior/boundary/ghost trace groups and checks the grid
  this->finalize();

  // create the cell, boundary and interior trace mappings as well as the split info
  findTraceMappings_split(xfld_local_, soup, dof_split);

  // only need to check the grid if linear
  if (order_ == 1)
  {
    this->checkGrid();
    return;
  }

  // otherwise construct high-order and project DOFs
  XField<PhysDim,TopoDim> xfld(*this, FieldCopy());
  this->buildFrom(xfld, order_, xfld_global_.template getCellGroup<Topology>(0).basis()->category());

  // offset the new nodes based on the addtional high order DOFs added
  int newNodeOffset = this->nDOF() - xfld.nDOF();
  for (std::size_t n = 0; n < this->newNodeDOFs_.size(); n++)
    this->newNodeDOFs_[n] += newNodeOffset;

  // project the DOF from the global xfield to the local one
  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  for (int group = 0; group < this->nCellGroups(); group++)
  {
    // XFieldCellGroupType& cellGroup = this->template getCellGroup<Topology>(group);
    XFieldCellGroupType& cellGroup = getCellGroup(group);
    ElementXFieldClass xfldElemGlobal( cellGroup.basis() );
    ElementXFieldClass xfldElemLocal( cellGroup.basis() );
    Element_Subdivision_Projector<TopoDim,Topology> subElemProjector( cellGroup.basis() );
    Element_Projector_L2<TopoDim,Topology> elemProjector( cellGroup.basis() );
    for (int elem = 0; elem < cellGroup.nElem(); elem++)
    {
      // retrieve the split info
      ElementSplitInfo info = this->getCellSplitInfo( {group, elem} );
      SANS_ASSERT( info.split_flag != ElementSplitFlag::New );

      // retrieve the global element info from which we will do the projection
      std::pair<int,int> globalElem = this->getGlobalCellMap( {group, elem} );
      xfld_global_.template getCellGroup<Topology>( globalElem.first ).getElement( xfldElemGlobal, globalElem.second );

      if (info.split_flag == ElementSplitFlag::Unsplit)
      {
        // get the local trace orientation
        xfldElemLocal.setTraceOrientation(cellGroup.associativity(elem).traceOrientation());

        // project the global element onto the local one and set the DOF
        elemProjector.project(xfldElemGlobal, xfldElemLocal);
        cellGroup.setElement( xfldElemLocal, elem );
      }
      else
      {
        // get the local trace orientation
        xfldElemLocal.setTraceOrientation(cellGroup.associativity(elem).traceOrientation());

        // project the global element onto the local one and set the DOF
        subElemProjector.project(xfldElemGlobal, xfldElemLocal, info.split_type, info.edge_index, info.subcell_index);
        cellGroup.setElement( xfldElemLocal, elem );
      }
    }
  }

  try
  {
    this->checkGrid();
  }
  catch (const XFieldException& e)
  {
    // the projection might not create valid elements
    std::cout << e.what() << std::endl;
    std::cout << "Reverting to linear element for LocalPatch" << std::endl;

    this->buildFrom(xfld,1,BasisFunctionCategory_Lagrange);
    this->checkGrid();
  }
}

// disallow cubes (though these need to instantiated for the error model instantiation)
#define DISALLOW_LOCAL_PATCH(DIM,TOPOLOGY) \
  template<> XField_LocalPatch<DIM,TOPOLOGY>:: \
  XField_LocalPatch( const XField_LocalPatchConstructor<DIM,TOPOLOGY>& xfld_local, \
                     ElementSplitType split_type, int split_edge_index ) : \
    XField_LocalPatchBase<DIM,TOPOLOGY>( xfld_local.comm(), xfld_local.xfld_global(), xfld_local.connectivity(), xfld_local.xfld_nodalView() ) \
    { SANS_DEVELOPER_EXCEPTION("%s not supported",#TOPOLOGY); } \
    \
  template<> XField_LocalPatch<DIM,TOPOLOGY>:: \
  XField_LocalPatch( mpi::communicator& comm, \
                     const XField_CellToTrace<DIM,TOPOLOGY::TopoDim>& connectivity, \
                     const std::array<int,2> edge, SpaceType type, \
                     const Field_NodalView* nodalview ) : \
    XField_LocalPatchBase<DIM,TOPOLOGY>( std::make_shared<mpi::communicator>( comm ), connectivity.getXField(), connectivity, nodalview ) \
  { SANS_DEVELOPER_EXCEPTION("%s not supported",#TOPOLOGY); }

// macro for instantiations
#define INSTANTIATE_ALL_WITH_EDGE(DIM,TOPOLOGY) \
template void XField_LocalPatch_Utils::allWithEdge<DIM,TOPOLOGY>( const XField<DIM,typename TOPOLOGY::TopoDim>&, \
             const Field_NodalView*, int,  int, std::set<std::pair<int,int>>& );


} // SANS
