// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDLIFTBOUNDARY_LOCAL_H_
#define FIELDLIFTBOUNDARY_LOCAL_H_

#include <typeinfo>     // typeid

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"

#include "Field/XField.h"
#include "Field/XField_CellToTrace.h"

#include "Field/Field.h"
#include "Field/Local/XField_Local_Base.h"

#include "Field/Element/ElementProjection_L2.h"

namespace SANS
{

/*
 * A class for constructing/populating a local lifting operator field from a given local XField and a global Field.
 */

template <class TopoDim>
class FieldLiftBoundary_Local_Transfer;

template < class >
class FieldLiftBoundary_Local;

template <template<class,class,class> class FieldBase, class PhysDim, class TopoDim, class T>
class FieldLiftBoundary_Local< FieldBase<PhysDim, TopoDim, T> > : public FieldBase<PhysDim, TopoDim, T>
{
  friend FieldLiftBoundary_Local_Transfer<TopoDim>;

public:

  typedef FieldBase<PhysDim, TopoDim, T> FieldBaseType;

  FieldLiftBoundary_Local(const XField_Local_Base<PhysDim, TopoDim>& xfld, const FieldBase<PhysDim, TopoDim, T>& qfld,
              const int order, const BasisFunctionCategory& category) : FieldBaseType(xfld, order, category),
                                                                        local_xfld_(xfld)
  {
    FieldLiftBoundary_Local_Transfer<TopoDim>::transfer(*this, qfld);
  }

  ~FieldLiftBoundary_Local(){};

  FieldLiftBoundary_Local& operator=( const T& q ) { FieldBaseType::operator=(q); return *this; }

protected:

  template <class TopoCell>
  void transferCellGroup(const int local_group, const FieldBaseType& global_fld);

  const XField_Local_Base<PhysDim, TopoDim>& local_xfld_;
};

template <template<class,class,class> class FieldBase, class PhysDim, class TopoDim, class T>
template <class TopoCell>
void
FieldLiftBoundary_Local< FieldBase<PhysDim, TopoDim, T> >::transferCellGroup(const int local_group, const FieldBaseType& global_fld)
{
  typedef typename FieldBaseType::template FieldCellGroupType<TopoCell> FieldCellGroupType;
  typedef typename FieldCellGroupType::template ElementType<> ElementFieldClass;

  const FieldCellGroupType& local_cellgrp = this->template getCellGroup<TopoCell>(local_group);

  Element_Subdivision_Projector<TopoDim, TopoCell> elemProjector(local_cellgrp.basis());

  const int xfld_boundary_group = this->getGlobalCellGroupMap(local_group);
  const typename XField<PhysDim, TopoDim>::FieldTraceGroupBase& xfldTrace_local =
      local_xfld_.getBoundaryTraceGroupBaseGlobal(xfld_boundary_group);

  const int xfld_cell_group = xfldTrace_local.getGroupLeft();

  for (int elem = 0; elem < local_cellgrp.nElem(); elem++)
  {
    const int xfld_cell_elem = xfldTrace_local.getElementLeft(elem);

    ElementSplitInfo splitinfo = local_xfld_.getCellSplitInfo({xfld_cell_group,xfld_cell_elem});

    if (splitinfo.split_flag == ElementSplitFlag::New)
      SANS_DEVELOPER_EXCEPTION( "FieldLiftBoundary_Local<PhysDim, TopoDim, T>::transferCellGroup - cells cannot be of split-type 'New'!" );

    std::pair<int,int> tracemap = local_xfld_.getGlobalBoundaryTraceMap({xfld_boundary_group,elem});
    int global_tracegrp_ind = tracemap.first;
    int global_traceelem_ind  = tracemap.second;

    //Get global cell group
    const FieldCellGroupType& global_cellgrp = global_fld.template getCellGroup<TopoCell>(global_tracegrp_ind);

    ElementFieldClass fldElem_global( global_cellgrp.basis() );
    global_cellgrp.getElement(fldElem_global, global_traceelem_ind);

    if (splitinfo.split_flag == ElementSplitFlag::Split)
    {
      ElementFieldClass fldElem_sub( local_cellgrp.basis() );

      elemProjector.project(fldElem_global, fldElem_sub, splitinfo.split_type, splitinfo.edge_index, splitinfo.subcell_index);
      this->template getCellGroup<TopoCell>(local_group).setElement(fldElem_sub,elem);
    }
    else if (splitinfo.split_flag == ElementSplitFlag::Unsplit)
      this->template getCellGroup<TopoCell>(local_group).setElement(fldElem_global,elem);
    else
      SANS_DEVELOPER_EXCEPTION( "FieldLiftBoundary_Local<PhysDim, TopoDim, T>::transferCellGroup - Unknown ElementSplitFlag." );

  } //loop over elements
}


template <>
class FieldLiftBoundary_Local_Transfer<TopoD1>
{
public:
  //----------------------------------------------------------------------------//
  template <template<class,class,class> class FieldBase, class PhysDim, class T>
  static void
  transfer(FieldLiftBoundary_Local< FieldBase<PhysDim, TopoD1, T> >& fldLocal, const FieldBase<PhysDim, TopoD1, T>& fldGlobal )
  {
    //Zero out the DOF in the receiving field
    fldLocal = 0;

    for (int group=0; group<fldLocal.nCellGroups(); group++)
    {
      if ( fldLocal.getCellGroupBase(group).topoTypeID() == typeid(Line) )
      {
        fldLocal.template transferCellGroup<Line>(group, fldGlobal);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "FieldLiftBoundary_Local_Transfer<PhysDim, TopoD1, T>::transfer - Unknown cell topology." );
    } //loop over cell groups
  }
};

template <>
class FieldLiftBoundary_Local_Transfer<TopoD2>
{
public:
  //----------------------------------------------------------------------------//
  template <template<class,class,class> class FieldBase, class PhysDim, class T>
  static void
  transfer(FieldLiftBoundary_Local< FieldBase<PhysDim, TopoD2, T> >& fldLocal, const FieldBase<PhysDim, TopoD2, T>& fldGlobal )
  {
    //Zero out the DOF in the receiving field
    fldLocal = 0;

    for (int group=0; group<fldLocal.nCellGroups(); group++)
    {
      if ( fldLocal.getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
      {
        fldLocal.template transferCellGroup<Triangle>(group, fldGlobal);
      }
      else if ( fldLocal.getCellGroupBase(group).topoTypeID() == typeid(Quad) )
      {
        fldLocal.template transferCellGroup<Quad>(group, fldGlobal);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "FieldLiftBoundary_Local_Transfer<PhysDim, TopoD2, T>::transfer - Unknown cell topology." );
    } //loop over cell groups
  }
};

template <>
class FieldLiftBoundary_Local_Transfer<TopoD3>
{
public:
  //----------------------------------------------------------------------------//
  template <template<class,class,class> class FieldBase, class PhysDim, class T>
  static void
  transfer(FieldLiftBoundary_Local< FieldBase<PhysDim, TopoD3, T> >& fldLocal, const FieldBase<PhysDim, TopoD3, T>& fldGlobal )
  {
    //Zero out the DOF in the receiving field
    fldLocal = 0;

    for (int group=0; group<fldLocal.nCellGroups(); group++)
    {
      if ( fldLocal.getCellGroupBase(group).topoTypeID() == typeid(Tet) )
      {
        fldLocal.template transferCellGroup<Tet>(group, fldGlobal);
      }
      else if ( fldLocal.getCellGroupBase(group).topoTypeID() == typeid(Hex) )
      {
        fldLocal.template transferCellGroup<Hex>(group, fldGlobal);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "FieldLiftBoundary_Local_Transfer<PhysDim, TopoD3, T>::transfer - Unknown cell topology." );
    } //loop over cell groups
  }
};

template <>
class FieldLiftBoundary_Local_Transfer<TopoD4>
{
public:
  template <template<class, class, class> class FieldBase, class PhysDim, class T>
  static void
  transfer(FieldLiftBoundary_Local<FieldBase<PhysDim, TopoD4, T>> &fldLocal, const FieldBase<PhysDim, TopoD4, T> &fldGlobal)
  {

    // zero out DOFs in the receiving field
    fldLocal= 0;

    for (int group= 0; group < fldLocal.nCellGroups(); group++)
    {
      if (fldLocal.getCellGroupBase(group).topoTypeID() == typeid(Pentatope))
      {
        fldLocal.template transferCellGroup<Pentatope>(group, fldGlobal);
      }
      else
        SANS_DEVELOPER_EXCEPTION("FieldLiftBoundary_Local_Transfer<PhysDim, TopoD4, T>::transfer - Unknown cell topology.");
    }

  }
};

}

#endif /* FIELDLIFTBOUNDARY_LOCAL_H_ */
