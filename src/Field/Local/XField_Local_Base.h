// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD_LOCAL_BASE_H
#define XFIELD_LOCAL_BASE_H

#include <ostream>
#include <map>
#include <vector>
#include <utility> // std::pair

#include "BasisFunction/ElementEdges.h"
#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/TraceToCellRefCoord.h"
#include "BasisFunction/BasisFunction_RefElement_Split.h"

#include "Field/XField.h"

#include "MPI/communicator_fwd.h"

namespace SANS
{

template<class PhysDim, class TopoDim>
class XField_Local_Base : public XField<PhysDim, TopoDim>
{
public:
  typedef XField<PhysDim, TopoDim> BaseType;

  XField_Local_Base( const XField_Local_Base& xfld, const FieldCopy tag) : BaseType( xfld, tag) {};

  ~XField_Local_Base(){};

  std::pair<int,int> getGlobalCellMap(const std::pair<int,int>& localmap) const;
  std::pair<int,int> getGlobalInteriorTraceMap(const std::pair<int,int>& localmap) const;
  std::pair<int,int> getGlobalBoundaryTraceMap(const std::pair<int,int>& localmap) const;

  ElementSplitInfo getCellSplitInfo(const std::pair<int,int>& localmap) const;
  ElementSplitInfo getInteriorTraceSplitInfo(const std::pair<int,int>& localmap) const;
  ElementSplitInfo getBoundaryTraceSplitInfo(const std::pair<int,int>& localmap) const;

  const std::map<std::pair<int,int>,std::pair<int,int>>& getGlobalCellMap()          const { return CellMapping_; }
  const std::map<std::pair<int,int>,std::pair<int,int>>& getGlobalInteriorTraceMap() const { return InteriorTraceMapping_; }
  const std::map<std::pair<int,int>,std::pair<int,int>>& getGlobalBoundaryTraceMap() const { return BoundaryTraceMapping_; }

  const std::map<std::pair<int,int>,ElementSplitInfo>& getCellSplitInfo()          const { return CellSplitInfo_; }
  const std::map<std::pair<int,int>,ElementSplitInfo>& getInteriorTraceSplitInfo() const { return InteriorTraceSplitInfo_; }
  const std::map<std::pair<int,int>,ElementSplitInfo>& getBoundaryTraceSplitInfo() const { return BoundaryTraceSplitInfo_; }


  std::vector<int> getNewNodeDOFs() const { return newNodeDOFs_; }
  std::vector<int> getNewLinearNodeDOFs() const { return newLinearNodeDOFs_; }

  const std::vector<int>& getReSolveBoundaryTraceGroups() const
      { return reSolveBoundaryTraceGroups_; }
  void  getReSolveBoundaryTraceGroups( std::vector<int>& reSolveBoundaryTraceGroups ) const
      { reSolveBoundaryTraceGroups = reSolveBoundaryTraceGroups_; }
  const std::vector<int>& getReSolveInteriorTraceGroups() const
      { return reSolveInteriorTraceGroups_; }
  void  getReSolveInteriorTraceGroups( std::vector<int>& reSolveInteriorTraceGroups ) const
      { reSolveInteriorTraceGroups = reSolveInteriorTraceGroups_; }
  const std::vector<int>& getReSolveCellGroups() const
      { return reSolveCellGroups_; }
  void  getReSolveCellGroups( std::vector<int>& reSolveCellGroups ) const
      { reSolveCellGroups = reSolveCellGroups_; }

  virtual const std::type_info& derivedTypeID() const { return typeid(XField_Local_Base); }

  int globalBoundaryGroup( int local_group ) const { return local2globalBoundaryGroupMap_.at(local_group); }
protected:
  explicit XField_Local_Base(mpi::communicator& comm) : XField<PhysDim, TopoDim>(comm) {}
  explicit XField_Local_Base(std::shared_ptr<mpi::communicator> comm) : XField<PhysDim, TopoDim>(comm) {}

  std::vector<int> reSolveBoundaryTraceGroups_;
  std::vector<int> reSolveInteriorTraceGroups_;
  std::vector<int> reSolveCellGroups_ = {0}; // default to 0

  //Mappings from local mesh to global mesh
  std::map<std::pair<int,int>,std::pair<int,int>> CellMapping_;
  std::map<std::pair<int,int>,std::pair<int,int>> InteriorTraceMapping_;
  std::map<std::pair<int,int>,std::pair<int,int>> BoundaryTraceMapping_;

  std::map<std::pair<int,int>,ElementSplitInfo> CellSplitInfo_;
  std::map<std::pair<int,int>,ElementSplitInfo> InteriorTraceSplitInfo_;
  std::map<std::pair<int,int>,ElementSplitInfo> BoundaryTraceSplitInfo_;

  std::vector<int> newNodeDOFs_; //list of newly added node DOFs
  std::vector<int> newLinearNodeDOFs_; // list of newly added node DOFS for the LINEAR grid.

  std::map< int, int > local2globalBoundaryGroupMap_;
};

template <class PhysDim, class TopoDim>
std::pair<int,int>
XField_Local_Base<PhysDim, TopoDim>::getGlobalCellMap(const std::pair<int,int>& localmap) const
{
  std::map<std::pair<int,int>,std::pair<int,int>>::const_iterator it;

  it = CellMapping_.find(localmap);

  SANS_ASSERT_MSG( it != CellMapping_.end() , "Local cell group %d and element %d not found in cell map.", localmap.first, localmap.second);
  return it->second;
}

template <class PhysDim, class TopoDim>
std::pair<int,int>
XField_Local_Base<PhysDim, TopoDim>::getGlobalInteriorTraceMap(const std::pair<int,int>& localmap) const
{
  std::map<std::pair<int,int>,std::pair<int,int>>::const_iterator it;

  it = InteriorTraceMapping_.find(localmap);

  SANS_ASSERT_MSG( it != InteriorTraceMapping_.end() , "Local interior trace group %d and element %d not found in interior trace map.",
                  localmap.first, localmap.second);
  return it->second;
}

template <class PhysDim, class TopoDim>
std::pair<int,int>
XField_Local_Base<PhysDim, TopoDim>::getGlobalBoundaryTraceMap(const std::pair<int,int>& localmap) const
{
  std::map<std::pair<int,int>,std::pair<int,int>>::const_iterator it;

  it = BoundaryTraceMapping_.find(localmap);

  SANS_ASSERT_MSG( it != BoundaryTraceMapping_.end() , "Local boundary trace group %d and element %d not found in boundary trace map.",
                  localmap.first, localmap.second);
  return it->second;
}

template <class PhysDim, class TopoDim>
ElementSplitInfo
XField_Local_Base<PhysDim, TopoDim>::getCellSplitInfo(const std::pair<int,int>& localmap) const
{
  std::map<std::pair<int,int>,ElementSplitInfo>::const_iterator it;
  it = CellSplitInfo_.find(localmap);

  SANS_ASSERT_MSG( it != CellSplitInfo_.end(), "Local cell group %d and element %d not found in cell map.",
                   localmap.first, localmap.second);

  return it->second;
}

template <class PhysDim, class TopoDim>
ElementSplitInfo
XField_Local_Base<PhysDim, TopoDim>::getInteriorTraceSplitInfo(const std::pair<int,int>& localmap) const
{
  std::map<std::pair<int,int>,ElementSplitInfo>::const_iterator it;
  it = InteriorTraceSplitInfo_.find(localmap);

  SANS_ASSERT_MSG( it != InteriorTraceSplitInfo_.end(), "Local interior trace group %d and element %d not found in cell map.",
                   localmap.first, localmap.second);

  return it->second;
}

template <class PhysDim, class TopoDim>
ElementSplitInfo
XField_Local_Base<PhysDim, TopoDim>::getBoundaryTraceSplitInfo(const std::pair<int,int>& localmap) const
{
  std::map<std::pair<int,int>,ElementSplitInfo>::const_iterator it;
  it = BoundaryTraceSplitInfo_.find(localmap);

  SANS_ASSERT_MSG( it != BoundaryTraceSplitInfo_.end(), "Local boundary trace group %d and element %d not found in cell map.",
                   localmap.first, localmap.second);

  return it->second;
}

}

#endif // XFIELD_LOCAL_BASE_H
