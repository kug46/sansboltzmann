// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BasisFunction/ElementEdges.h"

#include "Field/Field_NodalView.h"
#include "Field/XField.h"

#include "Field/Local/XField_LocalPatchBase.h"

#include "Field/Partition/XField_Lagrange.h" // PeriodicBCNodeMap

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

// constructor from a global xfield
template<class PhysDim,class Topology>
XField_LocalPatchBase<PhysDim,Topology>::
XField_LocalPatchBase( std::shared_ptr<mpi::communicator> comm,
                       const XField<PhysDim,TopoDim>& xfld_global,
                       const XField_CellToTrace<PhysDim,TopoDim>& connectivity,
                       const Field_NodalView* xfld_nodalView ) :
  XField_Local_Base<PhysDim,TopoDim>(comm),
  xfld_global_(xfld_global),
  connectivity_(connectivity),
  xfld_nodalView_(xfld_nodalView)
{
  // start the element counter
  nElem_ = 0;

  // do some checks on the incoming mesh
  // retrieve the order from the global mesh in case we need to elevate the split meshes
  order_ = xfld_global.getCellGroupBase(0).order();
  for (int i = 0; i < xfld_global_.nCellGroups(); i++)
  {
    SANS_ASSERT( xfld_global_.getCellGroupBase(i).order() == order_ );
    SANS_ASSERT( xfld_global_.template getCellGroup<Topology>(i).basis()->category() == BasisFunctionCategory_Lagrange ||
                 xfld_global_.template getCellGroup<Topology>(i).basis()->category() == BasisFunctionCategory_Hierarchical);
  }

  // allocate the two cell groups that will be used to store cells
  // always work with a linear mesh, we will augment later if necessary
  cellGroups_.resize(2);
  cellGroups_[ RESOLVE_CELL_GROUP ] = LagrangeElementGroup_ptr( new LagrangeElementGroup( Topology::Topology, STRAIGHT_SIDED ) );
  cellGroups_[ FIXED_CELL_GROUP   ] = LagrangeElementGroup_ptr( new LagrangeElementGroup( Topology::Topology, STRAIGHT_SIDED ) );

  // allocate the ghost lagrange group for storing boundary traces of all other traces touching 1 element in the local xfield
  // If not in node mode, these are by definition attached to FIXED_CELL_GROUP
  const TraceGroupInfo info( TraceTopology::Topology, STRAIGHT_SIDED, FIXED_CELL_GROUP, -1 );
  ghostBoundaryLagrangeGroups_.emplace_back(info);
}

template<class PhysDim,class Topology>
void
XField_LocalPatchBase<PhysDim,Topology>::addElement( int local_group, int local_elem, const std::vector<int>& DOF )
{
  // add the cell to the local group
  int id = nElem_; // previously local_elem
  cellGroups_[local_group]->addElement( this->comm_->rank(), id, DOF );
  nElem_++;
}

template<class PhysDim,class Topology>
void
XField_LocalPatchBase<PhysDim,Topology>::addBoundaryTrace( int cellGroup, const UniqueTrace& trace, LagrangeConnectedGroup& bgroup )
{
  // get left properties
  int groupL = cellGroup;
  int elemL  = trace.cellElem;
  int traceL = trace.canonicalTrace;
  int orderL = cellGroups_[groupL]->order();
  int rankL  = cellGroups_[groupL]->elem(elemL).rank;

  // get the FULL trace with all the high-order nodes from the left element so that it is the canonicalTrace
  std::vector<int> traceDOFs = cellGroups_[groupL]->traceDOFs( elemL, traceL );

  // create the trace and add it to the requested group
  LagrangeTraceElement traceElement(rankL, bgroup.nElem(), Topology::Topology, traceDOFs,
                                    orderL, groupL, elemL, traceL, -1, -1, -1); // no right element
  bgroup.add_trace(traceElement);
}

#if 0
template<class Topology>
void setNormalSign(XField_LocalPatchBase<PhysDim,Topology>& xfld_local)
{

}

template<class PhysDim>
void setNormalSign<Line>(XField_LocalPatchBase<PhysDim,Line>& xfld_local)
{
  typedef Line Topology;
  typedef Node TraceTopology;
  typedef typename Topology::TopoDim TopoDim;

  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TraceTopology> XFieldTraceGroupType;

  const XField<PhysDim,TopoDim>& xfld_global = xfld_local.xfld_global();

  // Don't check interior trace equivalence as the normals might be reversed in the local grid

  // check DOF's for unsplit elements match global and local
  for (int group = 0; group < xfld_local.nBoundaryTraceGroups(); group++)
  {
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldClass;

    const XFieldTraceGroupType& traceGroup = xfld_local.getBoundaryTraceGroup(group);
    ElementXFieldClass xfldElemLocal( traceGroup.basis() );
    for (int elem = 0; elem < traceGroup.nElem(); elem++)
    {
      // retrieve the split info
      ElementSplitInfo info = xfld_local.getBoundaryTraceSplitInfo({group,elem});

      std::pair<int,int> globalElem = xfld_local.getGlobalBoundaryTraceMap( {group, elem} );

      ElementXFieldClass xfldElemGlobal( xfld_global.template getBoundaryTraceGroup<TraceTopology>( globalElem.first ).basis() );

      BOOST_CHECK( info.split_flag == ElementSplitFlag::Unsplit ||
                   info.split_flag == ElementSplitFlag::Split      );

      // retrieve the global and local elements
      xfld_global.template getBoundaryTraceGroup<TraceTopology>( globalElem.first ).getElement( xfldElemGlobal, globalElem.second );
      traceGroup.getElement( xfldElemLocal, elem );
    }
  }
}
#endif

template<class PhysDim,class Topology>
void
XField_LocalPatchBase<PhysDim,Topology>::finalize()
{
  // first create the dof
  this->resizeDOF(DOF_buffer_.size());
  std::map<int,int> g2l;
  for (std::size_t i = 0; i < DOF_buffer_.size(); i++)
  {
    // copy over DOFs from the buffer
    XField_Type::DOF_[i] = DOF_buffer_[i].X;

    // set the local to global DOF map
    // this maps from a continuous local DOF number on each processor
    // to the original global number of the DOFs across all processors
    this->local2nativeDOFmap_[i] = DOF_buffer_[i].idof;
    g2l.insert({i,i});
  }

  // setup the storage for the cell trace orientations
  int nCellGroup = cellGroups_.size();
  SANS_ASSERT( nCellGroup<=2 );
  std::vector<std::vector<std::vector<int>>> orientations(nCellGroup);
  for (int igroup = 0; igroup < nCellGroup; igroup++)
  {
    orientations[igroup].resize(this->cellGroups_[igroup]->nElem());
    int nTrace = this->cellGroups_[igroup]->nTracePerElem();
    int nElem = this->cellGroups_[igroup]->nElem();
    for (int elem = 0; elem < nElem; elem++)
      orientations[igroup][elem].resize(nTrace, 0);
  }

  // add the interior trace groups
  int nInteriorTraceGroup = interiorLagrangeGroups_.size();
  this->resizeInteriorTraceGroups( nInteriorTraceGroup );
  for (int i = 0; i < nInteriorTraceGroup; i++)
  {
    this->addInteriorGroupType( i, g2l, interiorLagrangeGroups_[i],
                                this->cellGroups_, orientations );
  }

  // TODO: No periodicity in local solves for now...
  std::vector<PeriodicBCNodeMap> periodicity;

  // add the boundary trace groups
  int nBTraceGroup = boundaryLagrangeGroups_.size();
  boundaryTraceIDs_.resize(nBTraceGroup);
  this->resizeBoundaryTraceGroups( nBTraceGroup );
  for (int i = 0; i < nBTraceGroup ; i++)
  {
    this->addBoundaryGroupType( i, g2l, boundaryLagrangeGroups_[i], this->cellGroups_,
                                this->boundaryTraceGroups_, periodicity, true, orientations );
  }


  int nGhostBTraceGroup = ghostBoundaryLagrangeGroups_.size();

  this->ghostBoundaryTraceGroups_.resize(nGhostBTraceGroup);
  for (int i = 0; i < nGhostBTraceGroup; i++)
  {
    this->addBoundaryGroupType( i, g2l, ghostBoundaryLagrangeGroups_[i], this->cellGroups_,
                                this->ghostBoundaryTraceGroups_, periodicity, false, orientations );
  }

  // finally we can add the cell groups
  cellIDs_.resize(nCellGroup);
  this->resizeCellGroups( nCellGroup );
  for (int igroup = 0; igroup < nCellGroup; igroup++)
    this->addCellGroupType( igroup, g2l, cellGroups_[igroup], orientations[igroup] );

  setResolveGroups();
}


template<class PhysDim,class Topology>
void
XField_LocalPatchBase<PhysDim,Topology>::setResolveGroups()
{

  // this is already set in XField_Local_Base but just to be explicit
  this->reSolveCellGroups_.clear();
  this->reSolveCellGroups_.push_back( RESOLVE_CELL_GROUP );

  if (spaceType_ == SpaceType::Continuous)
    this->reSolveCellGroups_.push_back( FIXED_CELL_GROUP );

  // identify the interior and boundary trace groups we will solve on
  for (int i = 0; i < this->nInteriorTraceGroups(); i++)
    this->reSolveInteriorTraceGroups_.push_back(i);

  for (int i = 0;i < this->nBoundaryTraceGroups(); i++)
  {
    // boundary groups are pre-allocated and may not have any elems to resolve on
    if (this->getBoundaryTraceGroupBase(i).nElem()==0) continue;
    this->reSolveBoundaryTraceGroups_.push_back(i);
  }
}

inline void
print_inline( const std::vector<int>& X, const std::string& title )
{
  printf("%s: (",title.c_str());
  for (std::size_t j=0;j<X.size();j++)
    printf(" %d ",X[j]);
  printf(")\n");
}

template<class PhysDim,class Topology>
void
XField_LocalPatchBase<PhysDim,Topology>::dump() const
{
  print_inline( mainCellNodes_, "main cell" );

  // print the dof
  for (std::size_t i=0;i<DOF_buffer_.size();i++)
    std::cout << "DOF " << i << ": " << DOF_buffer_[i].X << std::endl;

  // print the global2local map
  for (std::map<int,int>::const_iterator it=global2localDOFmap_.begin();it!=global2localDOFmap_.end();it++)
    printf("DOF global2local: %d -> %d\n",it->first,it->second);

  // print the resolve cell group
  for (int i=0;i<cellGroups_[RESOLVE_CELL_GROUP]->nElem();i++)
  {
    ElementSplitInfo info = this->getCellSplitInfo({RESOLVE_CELL_GROUP,i});
    std::pair<int,int> globalElem = this->getGlobalCellMap({RESOLVE_CELL_GROUP,i});
    printf("resolve elem %d with id %d, from elem (%d,%d) -> subcell = %d, edge_index = %d\n",
            i,cellGroups_[RESOLVE_CELL_GROUP]->elem(i).elemID,globalElem.first,globalElem.second,info.subcell_index,info.edge_index);
    for (std::size_t j=0;j<cellGroups_[RESOLVE_CELL_GROUP]->elem(i).elemMap.size();j++)
      printf(" %d",cellGroups_[RESOLVE_CELL_GROUP]->elem(i).elemMap[j]);
    printf("\n");
  }

  // print the fixed cell group
  for (int i=0;i<cellGroups_[FIXED_CELL_GROUP]->nElem();i++)
  {
    ElementSplitInfo info = this->getCellSplitInfo({FIXED_CELL_GROUP,i});
    std::pair<int,int> globalElem = this->getGlobalCellMap({FIXED_CELL_GROUP,i});
    printf("fixed elem %d with id %d, from elem (%d,%d) -> subcell = %d, edge_index = %d\n",
            i,cellGroups_[FIXED_CELL_GROUP]->elem(i).elemID,globalElem.first,globalElem.second,info.subcell_index,info.edge_index);
    for (std::size_t j=0;j<cellGroups_[FIXED_CELL_GROUP]->elem(i).elemMap.size();j++)
      printf(" %d",cellGroups_[FIXED_CELL_GROUP]->elem(i).elemMap[j]);
    printf("\n");
  }

  printf("there are %zu interior trace groups\n",interiorLagrangeGroups_.size());
  for (std::size_t i=0;i<interiorLagrangeGroups_.size();i++)
  {
    printf("\tinterior group %zu with %d elements\n",i,interiorLagrangeGroups_[i].nElem());
    for (int j=0;j<interiorLagrangeGroups_[i].nElem();j++)
    {
      std::string str = "\t\telem "+std::to_string(j);
      print_inline( interiorLagrangeGroups_[i].elem(j).elemMap, str );
    }
  }

  printf("there are %zu boundary trace groups\n",boundaryLagrangeGroups_.size());
  for (std::size_t i=0;i<boundaryLagrangeGroups_.size();i++)
  {
    printf("\tboundary group %zu with %d elements\n",i,boundaryLagrangeGroups_[i].nElem());
    for (int j=0;j<boundaryLagrangeGroups_[i].nElem();j++)
    {
      std::string str = "\t\telem "+std::to_string(j);
      print_inline( boundaryLagrangeGroups_[i].elem(j).elemMap, str );
    }
  }

  printf("there are %zu ghost boundary trace groups\n",ghostBoundaryLagrangeGroups_.size());
  for (std::size_t i=0;i<ghostBoundaryLagrangeGroups_.size();i++)
  {
    printf("\tghost boundary group %zu with %d elements\n",i,ghostBoundaryLagrangeGroups_[i].nElem());
    for (int j=0;j<ghostBoundaryLagrangeGroups_[i].nElem();j++)
    {
      std::string str = "\t\telem "+std::to_string(j);
      print_inline( ghostBoundaryLagrangeGroups_[i].elem(j).elemMap, str );
    }
  }

  for (auto it=this->CellMapping_.begin();it!=this->CellMapping_.end();++it)
    printf("local cell (%d,%d) maps to global cell (%d,%d)\n",
      it->first.first,it->first.second,it->second.first,it->second.second);
}

#define DISALLOW_LOCAL_PATCH(DIM,TOPOLOGY) \
template<> XField_LocalPatchBase<DIM,TOPOLOGY>:: \
XField_LocalPatchBase( std::shared_ptr<mpi::communicator> comm, \
                       const XField<DIM,TOPOLOGY::TopoDim>& xfld_global, \
                       const XField_CellToTrace<DIM,TOPOLOGY::TopoDim>& connectivity, \
                       const Field_NodalView* xfld_nodalView ) : \
    XField_Local_Base<DIM,TOPOLOGY::TopoDim>(comm), \
    xfld_global_(xfld_global), \
    connectivity_(connectivity), \
    xfld_nodalView_(xfld_nodalView) \
    { SANS_DEVELOPER_EXCEPTION("%s not supported",#TOPOLOGY); }


} // SANS
