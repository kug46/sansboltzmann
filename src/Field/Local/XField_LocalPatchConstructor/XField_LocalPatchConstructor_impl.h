// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BasisFunction/ElementEdges.h"

#include "Field/Element/ElementProjection_L2.h"
#include "Field/Element/UniqueElem.h"
#include "Field/Element/UniqueElemHash.h"
#include <unordered_set>

#include "Field/Field_NodalView.h"
#include "Field/XField.h"

#include "Field/Local/XField_LocalPatchConstructor.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

#define CG_ONE_TRACE_PER_BGROUP 0

// constructor from a global xfield
template<class PhysDim,class Topology>
XField_LocalPatchConstructor<PhysDim,Topology>::
XField_LocalPatchConstructor( mpi::communicator& comm,
                              const XField<PhysDim,TopoDim>& xfld_global,
                              const XField_CellToTrace<PhysDim,TopoDim>& connectivity,
                              const Field_NodalView* nodalview ) :
  XField_LocalPatchBase<PhysDim,Topology>( std::make_shared<mpi::communicator>( comm ), connectivity.getXField(), connectivity, nodalview )
{
}

// constructor from a global xfield
template<class PhysDim,class Topology>
XField_LocalPatchConstructor<PhysDim,Topology>::
XField_LocalPatchConstructor( mpi::communicator& comm,
                              const XField_CellToTrace<PhysDim,TopoDim>& connectivity,
                              int cellGroupGlobal, int elem,
                              SpaceType type, const Field_NodalView* nodalview ) :
  XField_LocalPatchBase<PhysDim,Topology>( std::make_shared<mpi::communicator>( comm ), connectivity.getXField(), connectivity, nodalview )
{
  // add the resolve element
  addResolveElement( cellGroupGlobal, elem );

  // swtich between patch type (support for DG or CG adaptation)
  if (type == SpaceType::Discontinuous)
    addSharedTraceNeighbours();
  else if (type == SpaceType::Continuous)
  {
    SANS_ASSERT_MSG( xfld_nodalView_!=NULL, "nodal view must be provided to extract a patch for CG");
    addSharedNodeNeighbours();
  }

  // perform the extraction
  extract();

  // save the main cell node DOF (for edge splitting later)
  // we only need node DOF to determine the edge splits
  const XFieldCellGroupType& group = xfld_global_.template getCellGroup<Topology>(cellGroupGlobal);
  mainCellNodes_.resize( Topology::NNode );
  group.associativity(elem).getNodeGlobalMapping( mainCellNodes_.data(), mainCellNodes_.size() );
}

// constructor from a global xfield -- Attached to an edge (global index numbering)
// This function is single step recursive, if splitting it will actually call itself once without splitting first
template<class PhysDim, class Topology>
XField_LocalPatchConstructor<PhysDim,Topology>::
XField_LocalPatchConstructor( mpi::communicator& comm,
                              const XField_CellToTrace<PhysDim,TopoDim>& connectivity,
                              const std::array<int,2> edge, SpaceType type,
                              const Field_NodalView* nodalview  ) :
  XField_LocalPatchBase<PhysDim,Topology>( std::make_shared<mpi::communicator>( comm ), connectivity.getXField(), connectivity, nodalview )
{
  spaceType_ = type;

  // check that the start and end nodes are not the same
  SANS_ASSERT_MSG( edge[0] != edge[1], "start and end node must not be the same" );

  // The first two common nodes are the edge
  createDOF(edge[0]); linearCommonNodes_.push_back(edge[0]);
  createDOF(edge[1]); linearCommonNodes_.push_back(edge[1]);

  SANS_ASSERT_MSG( nodalview != NULL, "nodal view must be provided to extract an edge local patch");
  Field_NodalView::IndexVector attachedCells0 = nodalview->getCellList(edge[0]);
  Field_NodalView::IndexVector attachedCells1 = nodalview->getCellList(edge[1]);

  // sort so that intersection and union
  std::sort(attachedCells0.begin(), attachedCells0.end());
  std::sort(attachedCells1.begin(), attachedCells1.end());

  Field_NodalView::IndexVector reSolveCellGroup;
  std::set_intersection( attachedCells0.begin(), attachedCells0.end(),
                         attachedCells1.begin(), attachedCells1.end(), std::back_inserter(reSolveCellGroup) );

  // Check the nodes have common attached cells
  SANS_ASSERT_MSG( !reSolveCellGroup.empty(), "nodes %d and %d do not share an edge", edge[0], edge[1]);

  std::set<int> connectedNodes;

  for (auto it = reSolveCellGroup.begin(); it != reSolveCellGroup.end(); ++it)
  {
    // std::cout << *it << std::endl;
    addResolveElement( it->group, it->elem );

    // add to the list of global commonNodes
    const std::vector<int>& cellNodes = nodalview->getNodeList(*it);
    for (auto const & node : cellNodes )
      if (node != edge[0] && node != edge[1] )
        connectedNodes.insert(node);
  }

  linearCommonNodes_.reserve(2+connectedNodes.size());
  for ( const int node : connectedNodes )
    linearCommonNodes_.push_back(node);

  addSharedNodeNeighbours(); // Add cell group 1

  // perform the extraction
  extract();

  // set edge mode
  this->edgeMode_ = true;
}

template<class PhysDim,class Topology>
int
XField_LocalPatchConstructor<PhysDim,Topology>::createDOF( int global_dof )
{
  // check if this dof already exists
  std::map<int,int>::const_iterator it = global2localDOFmap_.find(global_dof);
  if (it!=global2localDOFmap_.end())
    return it->second;

  int local_dof = global2localDOFmap_.size();

  // retrieve the dof from the global xfield
  const VectorX& X = xfld_global_.DOF(global_dof);

  // add it to this local field
  DOF_buffer_.push_back( GlobalDOFType(local_dof,X) );
  global2localDOFmap_.insert( std::pair<int,int>(global_dof,local_dof) );

  // return the index of this global_dof
  return local_dof;
}

template<class PhysDim,class Topology>
void
XField_LocalPatchConstructor<PhysDim,Topology>::getElement( int local_group, int local_elem, std::vector<int>& DOF ) const
{
  SANS_ASSERT( local_group==FIXED_CELL_GROUP || local_group==RESOLVE_CELL_GROUP );
  SANS_ASSERT_MSG( local_elem < cellGroups_[local_group]->nElem(),
                    "requested elem %d but there are %d elems in group %d",
                    local_elem, cellGroups_[local_group]->nElem(), local_group );

  // retrieve the cell dof from the Lagrange element in the cell group
  DOF.resize( Topology::NNode );
  for (std::size_t i=0;i<DOF.size();i++)
    DOF[i] = cellGroups_[local_group]->elem(local_elem).elemMap[i];
}

template<class PhysDim,class Topology>
void
XField_LocalPatchConstructor<PhysDim,Topology>::addResolveElement( int global_group, int global_elem )
{
  // retrieve all the dof of the element
  const XFieldCellGroupType& cellGroup = xfld_global_.template getCellGroup<Topology>( global_group );
  std::vector<int> elemDOF(Topology::NNode);
  cellGroup.associativity(global_elem).getNodeGlobalMapping( elemDOF.data(), elemDOF.size() );

  // loop through the element dof map to see if these dof have been added
  for (int idof = 0; idof < Topology::NNode; idof++)
  {
    // create the dof if it doesn't already exist and map it to the local element index
    int local_dof = createDOF(elemDOF[idof]);

    // map to the local dof
    elemDOF[idof] = local_dof;
  }

  // local group and element id
  const int local_group = RESOLVE_CELL_GROUP;
  const int local_elem = resolveElements_.size();

  // add the element to the appropriate group
  this->addElement( local_group, local_elem, elemDOF );

  // keep track of the map between the local element to the global xfield
  local2globalElemMap_.insert( { {local_group,local_elem}, {global_group,global_elem} } );

  // keep track of the fixed elements
  resolveElements_.insert( local_elem );
}

template<class PhysDim,class Topology>
void
XField_LocalPatchConstructor<PhysDim,Topology>::addFixedElement( int global_group, int global_elem )
{
  // retrieve all the dof of the element
  const XFieldCellGroupType& cellGroup = xfld_global_.template getCellGroup<Topology>( global_group );
  std::vector<int> elemDOF(Topology::NNode);
  cellGroup.associativity(global_elem).getNodeGlobalMapping( elemDOF.data(), elemDOF.size() );

  // loop through the element dof map to see if these dof have been added
  for (int idof = 0; idof < Topology::NNode; idof++)
  {
    // create the dof if it doesn't already exist and map it to the local element index
    int local_dof = createDOF(elemDOF[idof]);

    // map to the local dof
    elemDOF[idof] = local_dof;
  }

  // local group and element id
  const int local_group = FIXED_CELL_GROUP;
  const int local_elem = fixedElements_.size();

  // add the element to the appropriate group
  this->addElement( local_group, local_elem, elemDOF );

  // keep track of the map between the local element to the global xfield
  local2globalElemMap_.insert( { {local_group,local_elem}, {global_group,global_elem} } );

  // keep track of the fixed elements
  fixedElements_.insert( local_elem );
}

template<class PhysDim,class Topology>
void
XField_LocalPatchConstructor<PhysDim,Topology>::addSharedTraceNeighbours()
{
  // this is intended for DG local solves
  spaceType_ = SpaceType::Discontinuous;

  // add all the neighbours of resolve elements which are not resolve elements themselves
  SANS_ASSERT( nResolve()==1 ); // see TODO below
  for (int i = 0; i < nResolve(); i++)
  {
    for (int j = 0; j < Topology::NTrace; j++)
    {
      // retrive the trace element from the global mesh
      const std::pair<int,int>& global_map = local2globalElemMap_.find( {RESOLVE_CELL_GROUP,i} )->second;
      const TraceInfo& traceInfo = connectivity_.getTrace(global_map.first,global_map.second,j);

      if (traceInfo.type == TraceInfo::Boundary) continue; // no neighbor to add

      // The trance cannot be a ghost boundary. This means an element that is not possessed is extracted
      SANS_ASSERT (traceInfo.type != TraceInfo::GhostBoundary);

      // retrieve the opposite (right) element
      const XFieldTraceGroupType& traceGroup = xfld_global_.template getInteriorTraceGroup<TraceTopology>( traceInfo.group );

      int groupL = traceGroup.getGroupLeft();
      int elemL = traceGroup.getElementLeft(traceInfo.elem);

      int groupR = traceGroup.getGroupRight();
      int elemR = traceGroup.getElementRight(traceInfo.elem);

      int group = -1, elem = -1;
      if (groupL == global_map.first && elemL == global_map.second )
      {
        group = groupR;
        elem = elemR;
      }
      else if (groupR == global_map.first && elemR == global_map.second )
      {
        group = groupL;
        elem = elemL;
      }
      else
        SANS_DEVELOPER_EXCEPTION("should not be reached");

      // check if this element is a resolve element
      // TODO then we don't add
      addFixedElement( group, elem );
    }
  }
}

template<class PhysDim,class Topology>
void
XField_LocalPatchConstructor<PhysDim,Topology>::addSharedNodeNeighbours()
{
  // this is intended for CG local solves
  spaceType_ = SpaceType::Continuous;

  // add all the neighbours of resolve elements which are not resolve elements themselves
  SANS_ASSERT( xfld_nodalView_!=NULL );

  // build the set of vertices
  std::vector<int> vertices;
  std::vector<int> cellDOF( Topology::NNode ); // simplex for now
  std::set< std::pair<int,int> > neighbours;

  // add the current RESOLVE_CELL_GROUP to the neighbour list, so they don't get added later
  for (const int resolveElement : resolveElements_)
    neighbours.insert( local2globalElemMap_[{RESOLVE_CELL_GROUP,resolveElement}] );

  for (int i = 0; i < nResolve(); i++)
  {
    // retrieve the global element
    const std::pair<int,int>& global_map = local2globalElemMap_.find( {RESOLVE_CELL_GROUP,i} )->second;

    // retrieve the global dof
    const XFieldCellGroupType& cellGroup = xfld_global_.template getCellGroup<Topology>( global_map.first );
    cellGroup.associativity( global_map.second ).getNodeGlobalMapping( cellDOF.data(), cellDOF.size() );

    for (std::size_t j = 0; j < cellDOF.size(); j++)
    {
      // retrieve the cells attached to this node
      const Field_NodalView::IndexVector info = xfld_nodalView_->getCellList( cellDOF[j] );

      // add cells which have not yet been added
      for (std::size_t k = 0; k < info.size(); k++)
      {
        if (neighbours.find( {info[k].group,info[k].elem}) == neighbours.end() )
        {
          neighbours.insert( {info[k].group,info[k].elem} );

          addFixedElement(info[k].group,info[k].elem);
        }
      }
    }
  }
}

template<class PhysDim,class Topology>
LagrangeConnectedGroup&
XField_LocalPatchConstructor<PhysDim,Topology>::boundaryGroup( int global_bgroup, int cell_group )
{
#if CG_ONE_TRACE_PER_BGROUP
  int local_bgroup = boundaryLagrangeGroups_.size();
#else
  // check if this group already exists
  std::map< std::pair<int,int>, int >::const_iterator it;
  it = boundaryGroupMap_.find( {global_bgroup,cell_group} );
  if (it != boundaryGroupMap_.end())
    return boundaryLagrangeGroups_[it->second];

  // the {global_bgroup,cell_group} pair has not been referenced yet, so create the new boundary group
  int local_bgroup = boundaryGroupMap_.size();
  SANS_ASSERT( local_bgroup == (int) boundaryLagrangeGroups_.size() );
  SANS_ASSERT( local_bgroup == (int) boundaryGroupMap_.size() );
#endif

  const TraceGroupInfo info( TraceTopology::Topology, STRAIGHT_SIDED, cell_group, -1 );

  boundaryLagrangeGroups_.emplace_back( info );
  boundaryGroupMap_.insert( { {global_bgroup,cell_group}, local_bgroup } );
  local2globalBoundaryGroupMap_.insert( {local_bgroup,global_bgroup} );

  return boundaryLagrangeGroups_[local_bgroup];
}

template<class PhysDim,class Topology>
bool
XField_LocalPatchConstructor<PhysDim,Topology>::
isBoundaryTrace( const TraceInfo& info, int group, int elem ) const
{
  if (info.type != TraceInfo::Boundary) return false;

  if (spaceType_ == SpaceType::Discontinuous)
  {
    if (group == RESOLVE_CELL_GROUP && resolveElements_.find(elem) != resolveElements_.end())
      return true;
    return false;
  }

  if (spaceType_ == SpaceType::Continuous)
  {
    // include either boundary of the resolve cell group or the fixed cell group
    return true;
  }

  return false;
}

template<class PhysDim,class Topology>
void
XField_LocalPatchConstructor<PhysDim,Topology>::
classifyBoundaryTraces( const std::vector< UniqueTraceMap >& uniqueTraces )
{
  // we need to figure out which of the leftover uniqueTraces are on actual
  // boundaries of the global xfld (placed in boundaryGroups)
  // and which are on boundaries of the local xfield (placed in ghost boundary groups)
  for (std::size_t igroup = 0; igroup < uniqueTraces.size(); igroup++)
  {
    // construct a sorted set of unique traces to guarantee consistency due to hasing with unordered_map
    std::map< UniqueElem, UniqueTrace > sortedTraces(uniqueTraces[igroup].begin(), uniqueTraces[igroup].end());

    for (const std::pair<const UniqueElem, UniqueTrace>& trace : sortedTraces)
    {
      // retrieve the local trace info
      int groupL_local = igroup;
      int elemL_local  = trace.second.cellElem;
      int canonicalL   = trace.second.canonicalTrace;

      // retrieve the global trace info
      const std::pair<int,int>& global_map = local2globalElemMap_.find( {groupL_local,elemL_local} )->second;

      // remap to the global xfield, the canonical trace should be the same
      int groupL_global = global_map.first;
      int elemL_global  = global_map.second;

      const TraceInfo& traceInfo = connectivity_.getTrace(groupL_global,elemL_global,canonicalL);

      // determine if this should be a boundary or ghost trace
      if (isBoundaryTrace(traceInfo,groupL_local,elemL_local))
      {
        // retrieve the global boundary group
        int bgroup = traceInfo.group;

        // retrieve the group to which we will add the trace (creates if necessary)
        LagrangeConnectedGroup& group = boundaryGroup(bgroup,groupL_local);
        addBoundaryTrace( igroup, trace.second, group );
      }
      else
      {
        // the trace is actually an interior trace of the global mesh
        // or a boundary trace of a cell in group 1
        // so we'll add it to the list of ghost boundary traces
        // retrieve the node DOF (vertices) from the interior trace group

        SANS_ASSERT( groupL_local == FIXED_CELL_GROUP );

        addBoundaryTrace( igroup, trace.second, ghostBoundaryLagrangeGroups_[0] );
      }
    }
  }
}

template<class PhysDim,class Topology>
void
XField_LocalPatchConstructor<PhysDim,Topology>::findTraceMappings_unsplit()
{

  // loop through the interior trace groups
  for (std::size_t igroup = 0; igroup < interiorLagrangeGroups_.size(); igroup++)
  {
    const LagrangeConnectedGroup& group = interiorLagrangeGroups_[igroup];
    for (int ielem = 0; ielem < group.nElem(); ielem++)
    {
      // look at the left element to this local trace
      int cellL = group.elemL(ielem);
      int groupL = group.groupL();
      int itraceL = group.traceL(ielem);

      int cellR = group.elemR(ielem);
      int groupR = group.groupR();
      int itraceR = group.traceR(ielem);

      // find the global element info
      std::pair<int,int> global_infoL = local2globalElemMap_.at( {groupL,cellL} );
      std::pair<int,int> global_infoR = local2globalElemMap_.at( {groupR,cellR} );

      const TraceInfo& traceInfoL = connectivity_.getTrace(global_infoL.first,global_infoL.second,itraceL);
      const TraceInfo& traceInfoR = connectivity_.getTrace(global_infoR.first,global_infoR.second,itraceR);

      // the only reason we extract the right trace info was for a sanity check
      SANS_ASSERT( traceInfoR.group==traceInfoL.group && traceInfoR.elem==traceInfoL.elem );

      // trace elements that are interior in the local extract mesh need to be
      // interior trace elements in the global mesh
      SANS_ASSERT( traceInfoL.type==TraceInfo::Interior );
      SANS_ASSERT( traceInfoR.type==TraceInfo::Interior );

      // get the global trace element
      this->InteriorTraceMapping_.insert( { {igroup,ielem}, {traceInfoL.group,traceInfoL.elem} } );
      this->InteriorTraceSplitInfo_[ {igroup,ielem} ] = ElementSplitInfo( ElementSplitFlag::Unsplit );
    }
  }

  // loop through the boundary trace groups
  for (std::size_t igroup = 0; igroup < boundaryLagrangeGroups_.size(); igroup++)
  {
    const LagrangeConnectedGroup& group = boundaryLagrangeGroups_[igroup];
    for (int ielem = 0; ielem < group.nElem(); ielem++)
    {
      // look at the left element to this local trace
      int cellL = group.elemL(ielem);
      int groupL = group.groupL();
      int itraceL = group.traceL(ielem);

      // find the global element info
      std::map< std::pair<int,int>, std::pair<int,int> >::const_iterator it;
      it = local2globalElemMap_.find( {groupL,cellL} );
      SANS_ASSERT_MSG( it!=local2globalElemMap_.end(), "elem %d in group %d map does not exist",cellL,groupL );
      std::pair<int,int> global_info = local2globalElemMap_.at( {groupL,cellL} );

      const TraceInfo& traceInfo = connectivity_.getTrace(global_info.first,global_info.second,itraceL);

      // trace elements that are on the boundary of the local mesh need to
      // be boundary traces of the global mesh
      SANS_ASSERT( traceInfo.type==TraceInfo::Boundary );

      // get the global trace element
      this->BoundaryTraceMapping_.insert( { {igroup,ielem}, {traceInfo.group,traceInfo.elem} } );
      this->BoundaryTraceSplitInfo_[ {igroup,ielem} ] = ElementSplitInfo( ElementSplitFlag::Unsplit );

      // insert into the local2global group mappings
      local2globalBoundaryGroupMap_.insert( {igroup, traceInfo.group} );
      boundaryGroupMap_.insert( {{global_info.first,groupL}, igroup} );
    }
  }

  // assign the local to global mappings and cell split info
  this->CellMapping_ = local2globalElemMap_;
  for (auto it = this->CellMapping_.begin(); it != this->CellMapping_.end(); ++it)
    this->CellSplitInfo_[it->first] = ElementSplitInfo(ElementSplitFlag::Unsplit);
}

template<class PhysDim,class Topology>
void
XField_LocalPatchConstructor<PhysDim,Topology>::extract()
{
  // connect the interior trace groups
  // if you look in XField_Lagrange.cpp you'll see that the traces
  // will be automatically oriented with elements in group0 to the left
  // for traces that border both group0 and group1
  std::vector< UniqueTraceMap > uniqueTraces(cellGroups_.size());
  connectInteriorGroups(cellGroups_, uniqueTraces, interiorLagrangeGroups_);

  // the interior lagrange trace groups get filled and we can have a maximum of 3 groups: 0-0, 0-1 and 1-1
  SANS_ASSERT( interiorLagrangeGroups_.size() <= 3 );

  // process the trace groups using the leftover uniqueTraces
  classifyBoundaryTraces(uniqueTraces);

  // creates the DOF, builds the cell groups, interior/boundary/ghost trace groups and checks the grid
  this->finalize();

  // create the cell, boundary and interior trace mappings as well as the split info
  findTraceMappings_unsplit();

#if CG_ONE_TRACE_PER_BGROUP
  if (spaceType_==SpaceType::Continuous)
  {
    for (int i=0;i<XField_Type::nBoundaryTraceGroups();i++)
    {
      int nelem = XField_Type::getBoundaryTraceGroupBase(i).nElem();
      SANS_ASSERT_MSG( nelem==1, "nelem = %d", nelem );
    }
  }
#endif
}

// disallow cubes (though these need to instantiated for the error model instantiation)
#define DISALLOW_LOCAL_PATCH(DIM,TOPOLOGY) \
  template<> XField_LocalPatchConstructor<DIM,TOPOLOGY>:: \
  XField_LocalPatchConstructor( mpi::communicator& comm, \
                                const XField_CellToTrace<DIM,TOPOLOGY::TopoDim>& connectivity, \
                                int cellGroupGlobal, int elem, \
                                SpaceType type, const Field_NodalView* nodalview ) : \
    XField_LocalPatchBase<DIM,TOPOLOGY>( std::make_shared<mpi::communicator>( comm ), connectivity.getXField(), connectivity, nodalview ) \
    { SANS_DEVELOPER_EXCEPTION("%s not supported",#TOPOLOGY); }

} // SANS
