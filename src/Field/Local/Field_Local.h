// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELD_LOCAL_H_
#define FIELD_LOCAL_H_

#include <typeinfo>     // typeid

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"

#include "Field/XField.h"
#include "Field/XField_CellToTrace.h"

#include "Field/Field.h"
#include "Field/Local/XField_Local_Base.h"

#include "Field/Element/ElementProjection_L2.h"

namespace SANS
{

/*
 * A class for constructing and populating a local Field from a given local XField and a global solution Field.
 */

template <class TopoDim>
class Field_Local_Transfer;

template < class >
class Field_Local;

template <template<class,class,class> class FieldBase, class PhysDim, class TopoDim, class T>
class Field_Local< FieldBase<PhysDim, TopoDim, T> > : public FieldBase<PhysDim, TopoDim, T>
{
  friend Field_Local_Transfer<TopoDim>;

public:

  typedef FieldBase<PhysDim, TopoDim, T> FieldBaseType;

  Field_Local(const XField_Local_Base<PhysDim, TopoDim>& xfld, const FieldBaseType& qfld, const int order)
              : FieldBaseType(xfld, order), local_xfld_(xfld), global_xfld_(qfld.getXField()), global_qfld_(qfld)
  {
    Field_Local_Transfer<TopoDim>::transfer(qfld, *this);
  }

  //specialization for EG field
  template <class CGFieldType>
  Field_Local(const Field_Local< CGFieldType >& cgfld, const FieldBaseType& qfld, const int order,
      const BasisFunctionCategory& category)
              : FieldBaseType(cgfld, order, category), local_xfld_(cgfld.getXField()), global_xfld_(qfld.getXField()), global_qfld_(qfld)
  {
    Field_Local_Transfer<TopoDim>::transfer(qfld, *this);
  }

  Field_Local(const XField_Local_Base<PhysDim, TopoDim>& xfld, const FieldBaseType& qfld, const int order,
              const BasisFunctionCategory& category)
              : FieldBaseType(xfld, order, category), local_xfld_(xfld), global_xfld_(qfld.getXField()), global_qfld_(qfld)
  {
    Field_Local_Transfer<TopoDim>::transfer(qfld, *this);
  }

  Field_Local(const XField_Local_Base<PhysDim, TopoDim>& xfld, const FieldBaseType& qfld, const int order,
              const BasisFunctionCategory& category, const std::vector<int>& groups)
              : FieldBaseType(xfld, order, category, groups), local_xfld_(xfld), global_xfld_(qfld.getXField()), global_qfld_(qfld)
  {
    Field_Local_Transfer<TopoDim>::transfer(qfld, *this);
  }

  // Broken Field alternate constructor
  Field_Local( const std::vector<std::vector<int>>& groupSet,
               const XField_Local_Base<PhysDim, TopoDim>& xfld, const FieldBaseType& qfld, const int order,
               const BasisFunctionCategory& category)
              : FieldBaseType(groupSet, xfld, order, category), local_xfld_(xfld), global_xfld_(qfld.getXField()), global_qfld_(qfld)
  {
    Field_Local_Transfer<TopoDim>::transfer(qfld, *this);
  }

  Field_Local(const XField_Local_Base<PhysDim, TopoDim>& xfld, const FieldBaseType& qfld, const int order,
              const BasisFunctionCategory& category, const std::vector<int>& interiorGroups, const std::vector<int>& boundaryGroups )
              : FieldBaseType(xfld, order, category, interiorGroups, boundaryGroups),
                local_xfld_(xfld), global_xfld_(qfld.getXField()), global_qfld_(qfld)
  {
    Field_Local_Transfer<TopoDim>::transfer(qfld, *this);
  }

  ~Field_Local(){};

  Field_Local& operator=( const T& q ) { FieldBaseType::operator=(q); return *this; }

  const XField_Local_Base<PhysDim, TopoDim>& getXField() const { return local_xfld_; }
  const FieldBaseType& getQField() const { return global_qfld_; }

protected:

  template <class TopoCell>
  void transferCellGroup(const int local_group, const FieldBaseType& global_fld);

  template <class TopoTrace>
  void transferInteriorTraceGroup(const int local_group, const FieldBaseType& global_fld);

  template <class TopoTrace>
  void transferBoundaryTraceGroup(const int local_group, const FieldBaseType& global_fld);

  template <class FieldCellType, class TopoTrace>
  void transferCellToInteriorTrace(const int local_trace_group, const FieldCellType& globalCell_fld);

  template <class FieldCellType, class TopoDimTrace, class TopoTrace, class TopoCell>
  void projectCellToITrace(const FieldCellType& globalCell_fld,
                           const int global_cellgroup, const int global_cellelem,
                           const int local_tracegroup, const int local_traceelem,
                           const ElementSplitInfo& trace_splitinfo, const ElementSplitInfo& cell_splitinfo,
                           Element_Subdivision_CellToTrace_Projector<TopoDimTrace, TopoTrace>& elemProjector);

  const XField_Local_Base<PhysDim, TopoDim>& local_xfld_;
  const XField<PhysDim, TopoDim>& global_xfld_;
  const FieldBaseType& global_qfld_;
};

template <template<class,class,class> class FieldBase, class PhysDim, class TopoDim, class T>
template <class TopoCell>
void
Field_Local< FieldBase<PhysDim, TopoDim, T> >::transferCellGroup(const int local_group, const FieldBaseType& global_fld)
{

  typedef typename FieldBaseType::template FieldCellGroupType<TopoCell> FieldCellGroupType;
  typedef typename FieldCellGroupType::template ElementType<> ElementFieldClass;

  const FieldCellGroupType& local_cellgrp = this->template getCellGroup<TopoCell>(local_group);

  ElementFieldClass fldElem_sub( local_cellgrp.basis() );
  Element_Subdivision_Projector<TopoDim, TopoCell> elemSubProjector(local_cellgrp.basis());

  ElementFieldClass fldElem_local( local_cellgrp.basis() );

  for (int elem=0; elem<local_cellgrp.nElem(); elem++)
  {
    const int xfld_local_group = this->getGlobalCellGroupMap(local_group);
    ElementSplitInfo splitinfo = local_xfld_.getCellSplitInfo({xfld_local_group,elem});

    if (splitinfo.split_flag != ElementSplitFlag::New)
    {
      std::pair<int,int> cellmap = local_xfld_.getGlobalCellMap({xfld_local_group,elem});
      int global_cellgrp_ind = cellmap.first;
      int global_cellelem_ind  = cellmap.second;

      //Get global cell group
      const FieldCellGroupType& global_cellgrp = global_fld.template getCellGroupGlobal<TopoCell>(global_cellgrp_ind);
      ElementFieldClass fldElem_global( global_cellgrp.basis() );
      global_cellgrp.getElement(fldElem_global, global_cellelem_ind);

      if (splitinfo.split_flag == ElementSplitFlag::Split)
      {

        // get the local trace orientation
        if ( (local_cellgrp.basis()->category() == BasisFunctionCategory_Hierarchical) && (local_cellgrp.order() >= 3) )
          fldElem_sub.setTraceOrientation(local_cellgrp.associativity(elem).traceOrientation());

        elemSubProjector.project(fldElem_global, fldElem_sub, splitinfo.split_type, splitinfo.edge_index, splitinfo.subcell_index);
        this->template getCellGroup<TopoCell>(local_group).setElement(fldElem_sub,elem);
      }
      else if (splitinfo.split_flag == ElementSplitFlag::Unsplit)
      {
        if ( (local_cellgrp.basis()->category() == BasisFunctionCategory_Hierarchical) && (local_cellgrp.order() >= 3) ) // Need to transfer
        {
          Element_Projector_L2<TopoDim,TopoCell> elemProjector( local_cellgrp.basis() );

          // get the local trace orientation
          fldElem_local.setTraceOrientation(local_cellgrp.associativity(elem).traceOrientation());

          elemProjector.project(fldElem_global, fldElem_local);
          this->template getCellGroup<TopoCell>(local_group).setElement(fldElem_local,elem);
        }
        else
          this->template getCellGroup<TopoCell>(local_group).setElement(fldElem_global,elem);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Field_Local<PhysDim, TopoDim, T>::transferCellGroup - Unknown ElementSplitFlag." );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "Field_Local<PhysDim, TopoDim, T>::transferCellGroup - cells cannot be of split-type 'New'!" );
  } //loop over elements
}

template <template<class,class,class> class FieldBase, class PhysDim, class TopoDim, class T>
template <class TopoTrace>
void
Field_Local< FieldBase<PhysDim, TopoDim, T> >::transferInteriorTraceGroup(const int local_group, const FieldBaseType& global_fld)
{
  typedef typename FieldBaseType::template FieldTraceGroupType<TopoTrace> FieldTraceGroupType;
  typedef typename FieldTraceGroupType::template ElementType<> ElementFieldClass;
  typedef typename FieldTraceGroupType::TopologyType::TopoDim TopoDimTrace;

  typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopoTrace> XFieldTraceGroupType;

  const FieldTraceGroupType& local_tracegrp = this->template getInteriorTraceGroup<TopoTrace>(local_group);
  const XFieldTraceGroupType& xfld_local_tracegrp = local_xfld_.template getInteriorTraceGroup<TopoTrace>(local_group);

  Element_Subdivision_Projector<TopoDimTrace, TopoTrace> elemProjector(local_tracegrp.basis());

  for (int elem = 0; elem < local_tracegrp.nElem(); elem++)
  {
    const int xfld_local_group = this->getGlobalInteriorTraceGroupMap(local_group);
    ElementSplitInfo splitinfo = local_xfld_.getInteriorTraceSplitInfo({xfld_local_group, elem});

    if (splitinfo.split_flag != ElementSplitFlag::New)
    {
      int xfld_local_cellgroupL   = xfld_local_tracegrp.getGroupLeft();
      int xfld_local_cellelemL    = xfld_local_tracegrp.getElementLeft(elem);
      int trace_orientation_local = xfld_local_tracegrp.getCanonicalTraceLeft(elem).orientation; // Main cell group is always to the left

      std::pair<int,int> global_cell_pair = local_xfld_.getGlobalCellMap({xfld_local_cellgroupL, xfld_local_cellelemL});

      std::pair<int,int> tracemap = local_xfld_.getGlobalInteriorTraceMap({xfld_local_group, elem});
      int global_tracegrp_ind  = tracemap.first;
      int global_traceelem_ind = tracemap.second;

      //Get global interior trace field group
      const FieldTraceGroupType& global_tracegrp = global_fld.template getInteriorTraceGroupGlobal<TopoTrace>(global_tracegrp_ind);
      ElementFieldClass fldElem_global( global_tracegrp.basis() );

      global_tracegrp.getElement(fldElem_global, global_traceelem_ind);

      //Check if the DOFs obtained above need to be re-oriented due to orientation changes in interior traces
      //between the global and local meshes
      const XFieldTraceGroupType& xfld_global_tracegrp = global_xfld_.template getInteriorTraceGroupGlobal<TopoTrace>(global_tracegrp_ind);
      int xfld_global_cellgroupL = xfld_global_tracegrp.getGroupLeft();
      int xfld_global_cellelemL  = xfld_global_tracegrp.getElementLeft(global_traceelem_ind);

      int xfld_global_cellgroupR = xfld_global_tracegrp.getGroupRight();
      int xfld_global_cellelemR  = xfld_global_tracegrp.getElementRight(global_traceelem_ind);

      int trace_orientation_global = 0;

      // If the global cell is to the left of the local edge, the orientation has not changed
      // If the global cell is to the right of the local edge, the orientation has changed

      if ( xfld_global_cellgroupL == global_cell_pair.first &&
           xfld_global_cellelemL  == global_cell_pair.second )
      {
        //Local left cell is to the left of the global trace
        trace_orientation_global = xfld_global_tracegrp.getCanonicalTraceLeft(global_traceelem_ind).orientation;
      }
      else if ( xfld_global_cellgroupR == global_cell_pair.first &&
                xfld_global_cellelemR  == global_cell_pair.second )
      {
        //Local left cell is to the right of the global trace
        trace_orientation_global = xfld_global_tracegrp.getCanonicalTraceRight(global_traceelem_ind).orientation;
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Field_Local<PhysDim, TopoDim, T>::transferInteriorTraceGroup - "
                                  "Local mesh inconsistent with global mesh. Code should not get here!" );

      if (trace_orientation_global != trace_orientation_local) //Orientations are different, so need to do a projection
      {
        //Create temporary fldelem and copy global trace
        ElementFieldClass fldElem_tmp( fldElem_global );

        //Correct the orientation of global trace DOFs
        Element_Trace_Projection_L2(fldElem_tmp, trace_orientation_global, fldElem_global, trace_orientation_local);
      }

      if (splitinfo.split_flag == ElementSplitFlag::Split)
      {
        ElementFieldClass fldElem_sub( local_tracegrp.basis() );
        elemProjector.project(fldElem_global, fldElem_sub, splitinfo.split_type, splitinfo.edge_index, splitinfo.subcell_index);
        this->template getInteriorTraceGroup<TopoTrace>(local_group).setElement(fldElem_sub, elem);
      }
      else if (splitinfo.split_flag == ElementSplitFlag::Unsplit)
      {
        this->template getInteriorTraceGroup<TopoTrace>(local_group).setElement(fldElem_global, elem);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Field_Local<PhysDim, TopoDim, T>::transferInteriorTraceGroup - Unknown ElementSplitFlag." );
    }
  } //loop over elements
}

template <template<class,class,class> class FieldBase, class PhysDim, class TopoDim, class T>
template <class TopoTrace>
void
Field_Local< FieldBase<PhysDim, TopoDim, T> >::transferBoundaryTraceGroup(const int local_group, const FieldBaseType& global_fld)
{
  typedef typename FieldBaseType::template FieldTraceGroupType<TopoTrace> FieldTraceGroupType;
  typedef typename FieldTraceGroupType::template ElementType<> ElementFieldClass;
  typedef typename FieldTraceGroupType::TopologyType::TopoDim TopoDimTrace;

  const FieldTraceGroupType& local_tracegrp = this->template getBoundaryTraceGroup<TopoTrace>(local_group);

  ElementFieldClass fldElem_sub( local_tracegrp.basis() );
  Element_Subdivision_Projector<TopoDimTrace, TopoTrace> elemSubProjector(local_tracegrp.basis());

  for (int elem=0; elem<local_tracegrp.nElem(); elem++)
  {
    const int xfld_local_group = this->getGlobalBoundaryTraceGroupMap(local_group);
    ElementSplitInfo splitinfo = local_xfld_.getBoundaryTraceSplitInfo({xfld_local_group,elem});

    if (splitinfo.split_flag != ElementSplitFlag::New)
    {
      std::pair<int,int> tracemap = local_xfld_.getGlobalBoundaryTraceMap({xfld_local_group,elem});
      int global_tracegrp_ind = tracemap.first;
      int global_traceelem_ind  = tracemap.second;

      SANS_ASSERT(global_tracegrp_ind >= 0);

      //Get global boundary trace group
      const FieldTraceGroupType& global_tracegrp = global_fld.template getBoundaryTraceGroupGlobal<TopoTrace>(global_tracegrp_ind);
      ElementFieldClass fldElem_global( global_tracegrp.basis() );
      global_tracegrp.getElement(fldElem_global, global_traceelem_ind);

      if (splitinfo.split_flag == ElementSplitFlag::Split)
      {
        // get the local trace orientation
        if ( (global_tracegrp.basis()->category() == BasisFunctionCategory_Hierarchical) && (local_tracegrp.order() >= 3) )
          fldElem_sub.setTraceOrientation(global_tracegrp.associativity(elem).traceOrientation());

        elemSubProjector.project(fldElem_global, fldElem_sub, splitinfo.split_type, splitinfo.edge_index, splitinfo.subcell_index);
        this->template getBoundaryTraceGroup<TopoTrace>(local_group).setElement(fldElem_sub,elem);
      }
      else if (splitinfo.split_flag == ElementSplitFlag::Unsplit)
        this->template getBoundaryTraceGroup<TopoTrace>(local_group).setElement(fldElem_global,elem);
      else
        SANS_DEVELOPER_EXCEPTION( "Field_Local<PhysDim, TopoDim, T>::transferBoundaryTraceGroup - Unknown ElementSplitFlag." );

#if 0
      if (global_tracegrp_ind>=0)
      {
        if (global_fld.nInteriorTraceGroups() > 0) //Can copy from interior to boundary trace only if interior traces exist
        {
          //Get global interior trace group
          const FieldTraceGroupType& global_tracegrp = global_fld.template getInteriorTraceGroupGlobal<TopoTrace>(global_tracegrp_ind);
          ElementFieldClass fldElem_global( global_tracegrp.basis() );

          if (splitinfo.split_flag == ElementSplitFlag::Split)
          {
            ElementFieldClass fldElem_sub( local_tracegrp.basis() );
            elemProjector.project(fldElem_global, fldElem_sub, splitinfo.split_type, splitinfo.edge_index, splitinfo.subcell_index);
            this->template getBoundaryTraceGroup<TopoTrace>(local_group).setElement(fldElem_sub,elem);
          }
          else if (splitinfo.split_flag == ElementSplitFlag::Unsplit)
          {
            global_tracegrp.getElement(fldElem_global, global_traceelem_ind);
            this->template getBoundaryTraceGroup<TopoTrace>(local_group).setElement(fldElem_global,elem);
          }
          else
            SANS_DEVELOPER_EXCEPTION( "Field_Local<PhysDim, TopoDim, T>::transferBoundaryTraceGroup - Unknown ElementSplitFlag." );
        }
      }
      else
      {
        //Make tracegroup index positive, since tracegroup indices of BoundaryTraces are stored as negative numbers.
        const int Btrace_group = -(global_tracegrp_ind+1);

        //Get global boundary trace group
        const FieldTraceGroupType& global_tracegrp = global_fld.template getBoundaryTraceGroupGlobal<TopoTrace>(Btrace_group);
        ElementFieldClass fldElem_global( global_tracegrp.basis() );
        global_tracegrp.getElement(fldElem_global, global_traceelem_ind);

        if (splitinfo.split_flag == ElementSplitFlag::Split)
        {
          ElementFieldClass fldElem_sub( local_tracegrp.basis() );
          elemProjector.project(fldElem_global, fldElem_sub, splitinfo.split_type, splitinfo.edge_index, splitinfo.subcell_index);
          this->template getBoundaryTraceGroup<TopoTrace>(local_group).setElement(fldElem_sub,elem);
        }
        else if (splitinfo.split_flag == ElementSplitFlag::Unsplit)
          this->template getBoundaryTraceGroup<TopoTrace>(local_group).setElement(fldElem_global,elem);
        else
          SANS_DEVELOPER_EXCEPTION( "Field_Local<PhysDim, TopoDim, T>::transferBoundaryTraceGroup - Unknown ElementSplitFlag." );
      }
#endif
    }
    else
      SANS_DEVELOPER_EXCEPTION( "Field_Local<PhysDim, TopoDim, T>::transferBoundaryTraceGroup - boundary traces cannot be of split-type 'New'!" );

  } //loop over elements
}

template <template<class,class,class> class FieldBase, class PhysDim, class TopoDim, class T>
template <class FieldCellType, class TopoTrace>
void
Field_Local< FieldBase<PhysDim, TopoDim, T> >::transferCellToInteriorTrace(const int local_trace_group, const FieldCellType& globalCell_fld)
{
  typedef typename FieldBaseType::template FieldTraceGroupType<TopoTrace> FieldTraceGroupType;
  typedef typename FieldTraceGroupType::TopologyType::TopoDim TopoDimTrace;

  typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopoTrace> XFieldTraceGroupType;

  const FieldTraceGroupType& local_tracegrp = this->template getInteriorTraceGroup<TopoTrace>(local_trace_group);
  const XFieldTraceGroupType& xfld_local_tracegrp = local_xfld_.template getInteriorTraceGroup<TopoTrace>(local_trace_group);

  Element_Subdivision_CellToTrace_Projector<TopoDimTrace, TopoTrace> elemProjector(local_tracegrp.basis());

  for (int elem = 0; elem < local_tracegrp.nElem(); elem++)
  {
    const int xfld_local_group = this->getGlobalInteriorTraceGroupMap(local_trace_group);
    ElementSplitInfo trace_splitinfo = local_xfld_.getInteriorTraceSplitInfo({xfld_local_group, elem});

    if (trace_splitinfo.split_flag == ElementSplitFlag::New)
    {
      int xfld_local_cellgroupL = xfld_local_tracegrp.getGroupLeft();
      int xfld_local_cellelemL  = xfld_local_tracegrp.getElementLeft(elem);

      ElementSplitInfo cell_splitinfo = local_xfld_.getCellSplitInfo({xfld_local_cellgroupL, xfld_local_cellelemL});

      std::pair<int,int> global_cell_pair = local_xfld_.getGlobalCellMap({xfld_local_cellgroupL, xfld_local_cellelemL});

      Field_Local_Transfer<TopoDim>::template projectFromCellGroup<FieldCellType, FieldBase, PhysDim, T, TopoDimTrace, TopoTrace>(
                                     globalCell_fld, global_cell_pair.first, global_cell_pair.second, local_trace_group, elem,
                                     trace_splitinfo, cell_splitinfo, elemProjector, *this);
    }
  } //loop over elements
}

template <template<class,class,class> class FieldBase, class PhysDim, class TopoDim, class T>
template <class FieldCellType, class TopoDimTrace, class TopoTrace, class TopoCell>
void
Field_Local< FieldBase<PhysDim, TopoDim, T> >::projectCellToITrace(const FieldCellType& globalCell_fld,
                                                                   const int global_cellgroup, const int global_cellelem,
                                                                   const int local_tracegroup, const int local_traceelem,
                                                                   const ElementSplitInfo& trace_splitinfo,
                                                                   const ElementSplitInfo& cell_splitinfo,
                                                                   Element_Subdivision_CellToTrace_Projector<TopoDimTrace, TopoTrace>& elemProjector)
{
  typedef typename FieldBaseType::template FieldTraceGroupType<TopoTrace> FieldTraceGroupType;
  typedef typename FieldTraceGroupType::template ElementType<> ElementFieldClass_Trace;

  typedef typename FieldCellType::template FieldCellGroupType<TopoCell> FieldCellGroupType;
  typedef typename FieldCellGroupType::template ElementType<> ElementFieldClass_Cell;

  const FieldTraceGroupType& local_tracegrp = this->template getInteriorTraceGroup<TopoTrace>(local_tracegroup);

  const FieldCellGroupType& global_cellgrp = globalCell_fld.template getCellGroup<TopoCell>(global_cellgroup);
  ElementFieldClass_Cell fldCellElem( global_cellgrp.basis() );
  global_cellgrp.getElement(fldCellElem, global_cellelem);

  ElementFieldClass_Trace fldTraceElem( local_tracegrp.basis() );
  elemProjector.project(fldCellElem, fldTraceElem, cell_splitinfo.split_type, cell_splitinfo.edge_index, trace_splitinfo.subcell_index);
  this->template getInteriorTraceGroup<TopoTrace>(local_tracegroup).setElement(fldTraceElem, local_traceelem);
}

template <>
class Field_Local_Transfer<TopoD1>
{
public:
  //----------------------------------------------------------------------------//
  template <template<class,class,class> class FieldBase, class PhysDim, class T>
  static void
  transfer(const FieldBase<PhysDim, TopoD1, T>& fldGlobal, Field_Local< FieldBase<PhysDim, TopoD1, T> >& fldLocal)
  {
    //Zero out the DOF in the receiving field
    fldLocal = 0;

    for (int group = 0; group < fldLocal.nCellGroups(); group++)
    {
      if ( fldLocal.getCellGroupBase(group).topoTypeID() == typeid(Line) )
      {
        fldLocal.template transferCellGroup<Line>(group, fldGlobal);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Field_Local_Transfer<PhysDim, TopoD1, T>::transfer - Unknown cell topology." );
    } //loop over cell groups


    for (int group = 0; group < fldLocal.nInteriorTraceGroups(); group++)
    {
      if ( fldLocal.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Node) )
      {
        fldLocal.template transferInteriorTraceGroup<Node>(group, fldGlobal);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Field_Local_Transfer<PhysDim, TopoD1, T>::transfer - Unknown interior trace topology." );
    } //loop over interior trace groups

    for (int group = 0; group < fldLocal.nBoundaryTraceGroups(); group++)
    {
      if ( fldLocal.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Node) )
      {
        fldLocal.template transferBoundaryTraceGroup<Node>(group, fldGlobal);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Field_Local_Transfer<PhysDim, TopoD1, T>::transfer - Unknown boundary trace topology." );
    } //loop over boundary trace groups
  }

  //----------------------------------------------------------------------------//
  template <template<class,class,class> class FieldCellType, template<class,class,class> class FieldBase_Trace,
            class PhysDim, class T>
  static void
  transferFromFieldCell(const FieldCellType<PhysDim, TopoD1, T>& fldCell_global,
                        Field_Local< FieldBase_Trace<PhysDim, TopoD1, T> >& fldTrace_local)
  {
    for (int group = 0; group < fldTrace_local.nInteriorTraceGroups(); group++)
    {
      if ( fldTrace_local.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Node) )
      {
        fldTrace_local.template transferCellToInteriorTrace<FieldCellType<PhysDim, TopoD1, T>, Node>(group, fldCell_global);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Field_Local_Transfer<PhysDim, TopoD1, T>::transferFromFieldCell - Unknown interior trace topology." );
    } //loop over interior trace groups
  }

  //----------------------------------------------------------------------------//
  template <class FieldCellType, template<class,class,class> class FieldBase_Trace,
            class PhysDim, class T, class TopoDimTrace, class TopoTrace>
  static void
  projectFromCellGroup(const FieldCellType& fldCell_global,
                       const int global_cellgroup, const int global_cellelem,
                       const int local_tracegroup, const int local_traceelem,
                       const ElementSplitInfo& trace_splitinfo, const ElementSplitInfo& cell_splitinfo,
                       const Element_Subdivision_CellToTrace_Projector<TopoDimTrace, TopoTrace>& elemProjector,
                       Field_Local< FieldBase_Trace<PhysDim, TopoD1, T> >& fldTrace_local)
  {
    if ( fldCell_global.getCellGroupBase(global_cellgroup).topoTypeID() == typeid(Line) )
    {
      fldTrace_local.template projectCellToITrace<FieldCellType, TopoDimTrace, TopoTrace, Line>
                     (fldCell_global, global_cellgroup, global_cellelem,
                      local_tracegroup, local_traceelem, trace_splitinfo, cell_splitinfo, elemProjector);
    }
    else
      SANS_DEVELOPER_EXCEPTION( "Field_Local_Transfer<PhysDim, TopoD1, T>::projectFromCellGroup - Unknown cell topology." );
  }
};

template <>
class Field_Local_Transfer<TopoD2>
{
public:
  //----------------------------------------------------------------------------//
  template <template<class,class,class> class FieldBase, class PhysDim, class T>
  static void
  transfer(const FieldBase<PhysDim, TopoD2, T>& fldGlobal, Field_Local< FieldBase<PhysDim, TopoD2, T> >& fldLocal)
  {
#if 0
    std::cout << "local xfld" << std::endl;
    fldLocal.getXField().dump();

    std::cout << "//////////////" << std::endl << std::endl << std::endl << std::endl;
    std::cout << "//////////////" << std::endl;
    std::cout << "global xfld" << std::endl;
    fldGlobal.getXField().dump();

    std::cout << "//////////////" << std::endl << std::endl << std::endl << std::endl;
    std::cout << "//////////////" << std::endl;
#endif

    //Zero out the DOF in the receiving field
    fldLocal = 0;

    for (int group = 0; group < fldLocal.nCellGroups(); group++)
    {
      if ( fldLocal.getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
      {
        fldLocal.template transferCellGroup<Triangle>(group, fldGlobal);
      }
      else if ( fldLocal.getCellGroupBase(group).topoTypeID() == typeid(Quad) )
      {
        fldLocal.template transferCellGroup<Quad>(group, fldGlobal);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Field_Local_Transfer<PhysDim, TopoD2, T>::transfer - Unknown cell topology." );
    } //loop over cell groups

    for (int group = 0; group < fldLocal.nInteriorTraceGroups(); group++)
    {
      if ( fldLocal.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Line) )
      {
        fldLocal.template transferInteriorTraceGroup<Line>(group, fldGlobal);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Field_Local_Transfer<PhysDim, TopoD2, T>::transfer - Unknown interior trace topology." );
    } //loop over interior trace groups


    for (int group = 0; group < fldLocal.nBoundaryTraceGroups(); group++)
    {
      if ( fldLocal.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Line) )
      {
        fldLocal.template transferBoundaryTraceGroup<Line>(group, fldGlobal);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Field_Local_Transfer<PhysDim, TopoD2, T>::transfer - Unknown boundary trace topology." );
    } //loop over boundary trace groups

  }

  //----------------------------------------------------------------------------//
  template <template<class,class,class> class FieldCellType, template<class,class,class> class FieldBase_Trace,
            class PhysDim, class T>
  static void
  transferFromFieldCell(const FieldCellType<PhysDim, TopoD2, T>& fldCell_global,
                        Field_Local< FieldBase_Trace<PhysDim, TopoD2, T> >& fldTrace_local)
  {
    for (int group = 0; group < fldTrace_local.nInteriorTraceGroups(); group++)
    {
      if ( fldTrace_local.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Line) )
      {
        fldTrace_local.template transferCellToInteriorTrace<FieldCellType<PhysDim, TopoD2, T>, Line>(group, fldCell_global);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Field_Local_Transfer<PhysDim, TopoD2, T>::transferFromFieldCell - Unknown interior trace topology." );
    } //loop over interior trace groups
  }

  //----------------------------------------------------------------------------//
  template <class FieldCellType, template<class,class,class> class FieldBase_Trace,
            class PhysDim, class T, class TopoDimTrace, class TopoTrace>
  static void
  projectFromCellGroup(const FieldCellType& fldCell_global,
                       const int global_cellgroup, const int global_cellelem,
                       const int local_tracegroup, const int local_traceelem,
                       const ElementSplitInfo& trace_splitinfo, const ElementSplitInfo& cell_splitinfo,
                       Element_Subdivision_CellToTrace_Projector<TopoDimTrace, TopoTrace>& elemProjector,
                       Field_Local< FieldBase_Trace<PhysDim, TopoD2, T> >& fldTrace_local)
  {
    if ( fldCell_global.getCellGroupBase(global_cellgroup).topoTypeID() == typeid(Triangle) )
    {
      fldTrace_local.template projectCellToITrace<FieldCellType, TopoDimTrace, TopoTrace, Triangle>
                     (fldCell_global, global_cellgroup, global_cellelem,
                      local_tracegroup, local_traceelem, trace_splitinfo, cell_splitinfo, elemProjector);
    }
    else if ( fldCell_global.getCellGroupBase(global_cellgroup).topoTypeID() == typeid(Quad) )
    {
      fldTrace_local.template projectCellToITrace<FieldCellType, TopoDimTrace, TopoTrace, Quad>
                     (fldCell_global, global_cellgroup, global_cellelem,
                      local_tracegroup, local_traceelem, trace_splitinfo, cell_splitinfo, elemProjector);
    }
    else
      SANS_DEVELOPER_EXCEPTION( "Field_Local_Transfer<PhysDim, TopoD2, T>::projectFromCellGroup - Unknown cell topology." );
  }
};

template <>
class Field_Local_Transfer<TopoD3>
{
public:
  //----------------------------------------------------------------------------//
  template <template<class,class,class> class FieldBase, class PhysDim, class T>
  static void
  transfer(const FieldBase<PhysDim, TopoD3, T>& fldGlobal, Field_Local< FieldBase<PhysDim, TopoD3, T> >& fldLocal)
  {
    //Zero out the DOF in the receiving field
    fldLocal = 0;

    for (int group = 0; group < fldLocal.nCellGroups(); group++)
    {
      if ( fldLocal.getCellGroupBase(group).topoTypeID() == typeid(Tet) )
      {
        fldLocal.template transferCellGroup<Tet>(group, fldGlobal);
      }
      else if ( fldLocal.getCellGroupBase(group).topoTypeID() == typeid(Hex) )
      {
        fldLocal.template transferCellGroup<Hex>(group, fldGlobal);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Field_Local_Transfer<PhysDim, TopoD3, T>::transfer - Unknown cell topology." );
    } //loop over cell groups


    for (int group = 0; group < fldLocal.nInteriorTraceGroups(); group++)
    {
      if ( fldLocal.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Triangle) )
      {
        fldLocal.template transferInteriorTraceGroup<Triangle>(group, fldGlobal);
      }
      else if ( fldLocal.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Quad) )
      {
        fldLocal.template transferInteriorTraceGroup<Quad>(group, fldGlobal);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Field_Local_Transfer<PhysDim, TopoD3, T>::transfer - Unknown interior trace topology." );
    } //loop over interior trace groups


    for (int group = 0; group < fldLocal.nBoundaryTraceGroups(); group++)
    {
      if ( fldLocal.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Triangle) )
      {
        fldLocal.template transferBoundaryTraceGroup<Triangle>(group, fldGlobal);
      }
      else if ( fldLocal.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Quad) )
      {
        fldLocal.template transferBoundaryTraceGroup<Quad>(group, fldGlobal);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Field_Local_Transfer<PhysDim, TopoD3, T>::transfer - Unknown boundary trace topology." );
    } //loop over boundary trace groups

  }

  //----------------------------------------------------------------------------//
  template <template<class,class,class> class FieldCellType, template<class,class,class> class FieldBase_Trace,
            class PhysDim, class T>
  static void
  transferFromFieldCell(const FieldCellType<PhysDim, TopoD3, T>& fldCell_global,
                        Field_Local< FieldBase_Trace<PhysDim, TopoD3, T> >& fldTrace_local)
  {
    for (int group = 0; group < fldTrace_local.nInteriorTraceGroups(); group++)
    {
      if ( fldTrace_local.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Triangle) )
      {
        fldTrace_local.template transferCellToInteriorTrace<FieldCellType<PhysDim, TopoD3, T>, Triangle>(group, fldCell_global);
      }
      else if ( fldTrace_local.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Quad) )
      {
        fldTrace_local.template transferCellToInteriorTrace<FieldCellType<PhysDim, TopoD3, T>, Quad>(group, fldCell_global);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Field_Local_Transfer<PhysDim, TopoD3, T>::transferFromFieldCell - Unknown interior trace topology." );
    } //loop over interior trace groups
  }

  //----------------------------------------------------------------------------//
  template <class FieldCellType, template<class,class,class> class FieldBase_Trace,
            class PhysDim, class T, class TopoDimTrace, class TopoTrace>
  static void
  projectFromCellGroup(const FieldCellType& fldCell_global,
                       const int global_cellgroup, const int global_cellelem,
                       const int local_tracegroup, const int local_traceelem,
                       const ElementSplitInfo& trace_splitinfo, const ElementSplitInfo& cell_splitinfo,
                       const Element_Subdivision_CellToTrace_Projector<TopoDimTrace, TopoTrace>& elemProjector,
                       Field_Local< FieldBase_Trace<PhysDim, TopoD3, T> >& fldTrace_local)
  {
    if ( fldCell_global.getCellGroupBase(global_cellgroup).topoTypeID() == typeid(Tet) )
    {
      fldTrace_local.template projectCellToITrace<FieldCellType, TopoDimTrace, TopoTrace, Tet>
                     (fldCell_global, global_cellgroup, global_cellelem,
                      local_tracegroup, local_traceelem, trace_splitinfo, cell_splitinfo, elemProjector);
    }
    else if ( fldCell_global.getCellGroupBase(global_cellgroup).topoTypeID() == typeid(Hex) )
    {
      fldTrace_local.template projectCellToITrace<FieldCellType, TopoDimTrace, TopoTrace, Hex>
                     (fldCell_global, global_cellgroup, global_cellelem,
                      local_tracegroup, local_traceelem, trace_splitinfo, cell_splitinfo, elemProjector);
    }
    else
      SANS_DEVELOPER_EXCEPTION( "Field_Local_Transfer<PhysDim, TopoD3, T>::projectFromCellGroup - Unknown cell topology." );
  }
};

template <>
class Field_Local_Transfer<TopoD4>
{
public:
  //----------------------------------------------------------------------------//
  template <template<class,class,class> class FieldBase, class PhysDim, class T>
  static void
  transfer(const FieldBase<PhysDim, TopoD4, T>& fldGlobal, Field_Local< FieldBase<PhysDim, TopoD4, T> >& fldLocal)
  {
    //Zero out the DOF in the receiving field
    fldLocal = 0;

    for (int group = 0; group < fldLocal.nCellGroups(); group++)
    {
      if ( fldLocal.getCellGroupBase(group).topoTypeID() == typeid(Pentatope) )
      {
        fldLocal.template transferCellGroup<Pentatope>(group, fldGlobal);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Field_Local_Transfer<PhysDim, TopoD4, T>::transfer - Unknown cell topology." );
    } //loop over cell groups


    for (int group = 0; group < fldLocal.nInteriorTraceGroups(); group++)
    {
      if ( fldLocal.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Tet) )
      {
        fldLocal.template transferInteriorTraceGroup<Tet>(group, fldGlobal);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Field_Local_Transfer<PhysDim, TopoD3, T>::transfer - Unknown interior trace topology." );
    } //loop over interior trace groups


    for (int group = 0; group < fldLocal.nBoundaryTraceGroups(); group++)
    {
      if ( fldLocal.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Tet) )
      {
        fldLocal.template transferBoundaryTraceGroup<Tet>(group, fldGlobal);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Field_Local_Transfer<PhysDim, TopoD3, T>::transfer - Unknown boundary trace topology." );
    } //loop over boundary trace groups

  }

  //----------------------------------------------------------------------------//
  template <template<class,class,class> class FieldCellType, template<class,class,class> class FieldBase_Trace,
            class PhysDim, class T>
  static void
  transferFromFieldCell(const FieldCellType<PhysDim, TopoD4, T>& fldCell_global,
                        Field_Local< FieldBase_Trace<PhysDim, TopoD4, T> >& fldTrace_local)
  {
    for (int group = 0; group < fldTrace_local.nInteriorTraceGroups(); group++)
    {
      if ( fldTrace_local.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Tet) )
      {
        fldTrace_local.template transferCellToInteriorTrace<FieldCellType<PhysDim, TopoD4, T>, Tet>(group, fldCell_global);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Field_Local_Transfer<PhysDim, TopoD3, T>::transferFromFieldCell - Unknown interior trace topology." );
    } //loop over interior trace groups
  }

  //----------------------------------------------------------------------------//
  template <class FieldCellType, template<class,class,class> class FieldBase_Trace,
            class PhysDim, class T, class TopoDimTrace, class TopoTrace>
  static void
  projectFromCellGroup(const FieldCellType& fldCell_global,
                       const int global_cellgroup, const int global_cellelem,
                       const int local_tracegroup, const int local_traceelem,
                       const ElementSplitInfo& trace_splitinfo, const ElementSplitInfo& cell_splitinfo,
                       const Element_Subdivision_CellToTrace_Projector<TopoDimTrace, TopoTrace>& elemProjector,
                       Field_Local< FieldBase_Trace<PhysDim, TopoD3, T> >& fldTrace_local)
  {
    if ( fldCell_global.getCellGroupBase(global_cellgroup).topoTypeID() == typeid(Pentatope) )
    {
      fldTrace_local.template projectCellToITrace<FieldCellType, TopoDimTrace, TopoTrace, Pentatope>
                     (fldCell_global, global_cellgroup, global_cellelem,
                      local_tracegroup, local_traceelem, trace_splitinfo, cell_splitinfo, elemProjector);
    }
    else
      SANS_DEVELOPER_EXCEPTION( "Field_Local_Transfer<PhysDim, TopoD4, T>::projectFromCellGroup - Unknown cell topology." );
  }
};

}

#endif /* FIELD_LOCAL_H_ */
