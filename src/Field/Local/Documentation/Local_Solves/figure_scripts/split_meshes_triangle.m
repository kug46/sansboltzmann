clear
clc
% close all


nodes = [0.0, 0.0
         1.0, 0.0
         0.0, 1.0
         0.5, 0.5
         0.0, 0.5
         0.5, 0.0
         1.0, 1.0
        -1.0, 1.0
         1.0,-1.0];

%Reference element splits
tri_unsplit = [1,2,3];

tri_split_edge0 = [1,2,4
                   1,4,3];
               
tri_split_edge1 = [5,2,3
                   1,2,5];
               
tri_split_edge2 = [1,6,3
                   6,2,3];
               
tri_split_iso = [6,2,4
                 5,4,3
                 1,6,5
                 4,5,6];

%Case with neighboring elements
tri_neighbors_unsplit_edge1 = [7,3,2
                               3,8,1];  
                         
tri_neighbors_split_edge1 = [7,3,2
                             3,8,5
                             5,8,1];
                         
tri_neighbors_split_iso = [7,4,2
                           7,3,4
                           3,8,5
                           5,8,1]; 

%---------- PLOTTING -------------

hold off
Tneighbor = [];%tri_neighbors_split_edge1;
% trimesh(Tneighbor,nodes(:,1),nodes(:,2),'Color','b','LineWidth',2)
% hold on                       
                         
Tmain = [tri_split_iso];
trimesh(Tmain,nodes(:,1),nodes(:,2),'Color','k','LineWidth',2)
          
unique_nodes = unique([Tmain;Tneighbor]);
for i=1:length(unique_nodes)
   text(nodes(unique_nodes(i),1),nodes(unique_nodes(i),2),sprintf('%d',i-1),...
        'FontSize',18,'Interpreter','Latex','HorizontalAlignment','center'); 
end

for i=1:size(Tmain,1)
   tri_nodes = nodes(Tmain(i,:),:);
   centroid = mean(tri_nodes,1);
   text(centroid(1),centroid(2),sprintf('E%d',i-1),'FontSize',18,...
        'Interpreter','Latex','HorizontalAlignment','center'); 
end

for i=1:size(Tneighbor,1)
   tri_nodes = nodes(Tneighbor(i,:),:);
   centroid = mean(tri_nodes,1);
   text(centroid(1),centroid(2),sprintf('E%d',i-1),'FontSize',18,...
        'Interpreter','Latex','HorizontalAlignment','center','Color','b'); 
end

box on
set(gca,'FontSize',14)
xlim([-0.5,1.5]);
ylim([-0.5,1.5]);

% xlim([-1.25,1.25]);
% axis equal
% ylim([-1.25,1.25]);

grid on
%grid minor

xlabel('$x$','Interpreter','Latex')
ylabel('$y$','Interpreter','Latex')

set(gcf, 'Units', 'Inches', 'Position', [1, 1, 8, 8])
set(gcf,'PaperUnits','Inches','PaperSize',[8,8],'PaperPosition',[2 2 8 8])