clear
clc
% close all


nodes = [0.0, 1.0, 0.5, 2.0, -1.0];

%Reference element only
line_unsplit = [1,2];

line_split_iso = [1,3;
                  3,2];

line_group = [5,1;
              1,2;
              2,4];

%Case with neighboring elements
line_neighbors_unsplit = [2,4
                          5,1];  
                      
%---------- PLOTTING -------------

hold off
Tneighbor = [];%line_neighbors_unsplit;
% plot(nodes(Tneighbor'),zeros(size(Tneighbor')),'b-*','LineWidth',2)
% hold on                       
                         
Tmain = line_split_iso;
plot(nodes(Tmain'),zeros(size(Tmain')),'k-*','LineWidth',2)
          
unique_nodes = unique([Tmain;Tneighbor]);
for i=1:length(unique_nodes)
   text(nodes(unique_nodes(i)),0.25,sprintf('%d',i-1),...
        'FontSize',18,'Interpreter','Latex','HorizontalAlignment','center'); 
end

for i=1:size(Tmain,1)
   line_nodes = nodes(Tmain(i,:));
   centroid = mean(line_nodes);
   text(centroid,-0.25,sprintf('E%d',i-1),'FontSize',16,...
        'Interpreter','Latex','HorizontalAlignment','center'); 
end

for i=1:size(Tneighbor,1)
   line_nodes = nodes(Tneighbor(i,:));
   centroid = mean(line_nodes);
   text(centroid,-0.25,sprintf('E%d',i-1),'FontSize',16,...
        'Interpreter','Latex','HorizontalAlignment','center','Color','b'); 
end

box on
set(gca,'FontSize',14)

xlim([-0.5,1.5]);
% xlim([-1.5,2.5]);
% axis equal
% ylim([-1.25,1.25]);

% grid on
%grid minor

xlabel('$x$','Interpreter','Latex')

set(gcf, 'Units', 'Inches', 'Position', [1, 1, 8, 3])
set(gcf,'PaperUnits','Inches','PaperSize',[8,3],'PaperPosition',[2 2 8 3])