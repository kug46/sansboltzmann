clear
clc
% close all


nodes = [0.0, 0.0, 0.0
         1.0, 0.0, 0.0
         0.0, 1.0, 0.0
         0.0, 0.0, 1.0
         0.0, 0.5, 0.5
         0.5, 0.0, 0.5
         0.5, 0.5, 0.0
         0.0, 0.5, 0.0
         0.0, 0.0, 0.5
         0.5, 0.0, 0.0
        -1.0, 0.0, 0.0
         0.0, 0.0,-1.0];
    
nodesT(:,[2,3,1]) = nodes;

%Reference element splits
tet_unsplit = [1,2,3,4];

tet_split_edge0 = [1,2,3,5
                   1,2,5,4];
               
tet_split_edge1 = [1,6,3,4
                   1,2,3,6];
               
tet_split_edge2 = [1,2,7,4
                   1,7,3,4];
               
tet_split_edge3 = [8,2,3,4
                   1,2,8,4];
               
tet_split_edge4 = [1,2,3,9
                   9,2,3,4];

tet_split_edge5 = [1,10,3,4
                   10,2,3,4];
               
tri_split_iso = [8, 7, 3, 5
                 9, 6, 5, 4
                10, 2, 7, 6
                 1,10, 8, 9
                 8, 7, 5, 6
                 9, 5, 6, 8
                10, 6, 7, 8
                10, 8, 9, 6];
            
tet_split_isoface0 = [1,7,3,5
                      1,6,5,4
                      1,2,7,6
                      1,5,6,7];
                  
tet_split_isoface1 = [9,2,5,4
                      8,2,3,5
                      1,2,8,9
                      5,2,9,8]; 

tet_split_isoface2 = [10,2,3,6
                      9,6,3,4
                      1,10,3,9
                      6,9,3,10]; 

tet_split_isoface3 = [8,7,3,4
                      10,2,7,4
                      1,10,8,4
                      7,8,10,4]; 

%Cases with neighboring elements
tet_neighbors1_unsplit = [11, 1,3,4
                           1,12,3,2];
                                              
tet_neighbors1_split_edge2 = [11, 1,3,4
                               1,12,7,2
                               1,12,3,7];

tet_neighbors2_unsplit = [11,1,3,4]; 

tet_neighbors2_split_isotropic = [11,9,5,4
                                  11,8,3,5
                                  11,1,8,9
                                  11,5,8,9]; 

%---------- PLOTTING -------------                           
                              
hold off
Tneighbor = tet_neighbors1_unsplit;
tetramesh(Tneighbor,nodesT,1*ones(size(Tneighbor,1)),'LineWidth',1,'FaceAlpha',0.1)
hold on                       
                         
Tmain = [tet_split_edge1];
tetramesh(Tmain,nodesT,2*ones(size(Tmain,1)),'LineWidth',2,'FaceAlpha',0.1)
          
unique_nodes = unique([Tmain;Tneighbor]);
labelshift = zeros(length(unique_nodes),3);
% labelshift(1,2) = -0.05; %x:2, y:3, z:1
% labelshift(5,2) = -0.05;
% labelshift(6,2) = -0.05;
% labelshift(7,2) = -0.05;
% labelshift(8,2) = -0.05;
% labelshift(9,2) = -0.05;
% labelshift(10,1) = -0.07;

for i=1:length(unique_nodes)
   coord = nodesT(unique_nodes(i),:) + labelshift(i,:);
   text(coord(1),coord(2),coord(3),sprintf('%d',i-1),...
        'FontSize',20,'Interpreter','Latex','HorizontalAlignment','center'); 
end

labelshift = zeros(size(Tmain,1),3);

%Isotropic label shifts
% labelshift(1,:) = [0,0.04,0.1];
% labelshift(2,:) = [0,0,-0.15];
% labelshift(3,2) = 0.1;
% labelshift(3,3) = 0.05;
% labelshift(4,3) = -0.1;
% labelshift(5,2) = 0.05;
% labelshift(6,:) = [-0.1,-0.15,0.05];
% labelshift(7,:) = [0,0.1,0.15];
% labelshift(8,3) = -0.2;

% labelshift(1,:) = [0,0.0,-0.3];
% labelshift(2,:) = [0,-0.1,0];
% labelshift(3,:) = [0,0,0.2];
% labelshift(3,:) = [0,0,0];
% labelshift(4,:) = [0.1,0,0.05];

for i=1:size(Tmain,1)
   tri_nodes = nodesT(Tmain(i,:),:);
   centroid = mean(tri_nodes,1) + labelshift(i,:);
   text(centroid(1),centroid(2),centroid(3),sprintf('E%d',i-1),'FontSize',20,...
        'Interpreter','Latex','HorizontalAlignment','center','Color','k'); 
end

labelshift = zeros(size(Tneighbor,1),3);

% labelshift(1,:) = [0,-0.1,-0.1];
% labelshift(2,:) = [0,0.0,0.0];
% labelshift(3,:) = [0.05,0.3,0.05];
% labelshift(4,:) = [0,-0.1,-0.05];

for i=1:size(Tneighbor,1)
   tri_nodes = nodesT(Tneighbor(i,:),:);
   centroid = mean(tri_nodes,1) + labelshift(i,:);
   text(centroid(1),centroid(2),centroid(3),sprintf('E%d',i-1),'FontSize',20,...
        'Interpreter','Latex','HorizontalAlignment','center','Color','b'); 
end

% box on
set(gca,'FontSize',16)

% ylim([-0.5,1.5]);

% xlim([-1.25,1.25]);
% axis equal
% ylim([-1.25,1.25]);

grid on
%grid minor

% view([115,25])
view([125,25]) %Use for reference elements
% view([135,20])

%Limits for reference element only
ylim([-0.2,1.2]); %x
zlim([0,1]); %y
xlim([-0.2,1.2]); %z

%Limits for case with neighbors
ylim([-1.2,1.2]); %x
zlim([0,1]); %y
xlim([-1.2,1.2]); %z

xlabel('$z$','Interpreter','Latex','FontSize',20)
ylabel('$x$','Interpreter','Latex','FontSize',20)
zlabel('$y$','Interpreter','Latex','FontSize',20)


set(gcf, 'Units', 'Inches', 'Position', [2, 2, 12, 10]) %Reference element only
set(gcf, 'Units', 'Inches', 'Position', [2, 2, 12, 8]) %With neighbors
% set(gcf,'PaperUnits','Inches','PaperSize',[12,10],'PaperPosition',[2 2 12 10])