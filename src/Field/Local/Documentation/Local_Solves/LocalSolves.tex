% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

% This is a simple template for a LaTeX document using the "article" class.
% See "book", "report", "letter" for other types of document.

\documentclass[11pt]{article} % use larger type; default would be 10pt
\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)

%%% Examples of Article customizations
% These packages are optional, depending whether you want the features they provide.
% See the LaTeX Companion or other references for full information.

%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{a4paper} % or letterpaper (US) or a5paper or....
\geometry{margin=0.8in} % for example, change the margins to 2 inches all round
% \geometry{landscape} % set up the page for landscape
%   read geometry.pdf for detailed page layout information

\usepackage{graphicx} % support the \includegraphics command and options

% \usepackage[parfill]{parskip} % Activate to begin paragraphs with an empty line rather than an indent

%%% PACKAGES
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{paralist} % very flexible & customisable lists (eg. enumerate/itemize, etc.)
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
\usepackage{subfig} % make it possible to include more than one captioned figure/table in a single float
\usepackage{float}
% These packages are all incorporated in the memoir class to one degree or another...

%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customise the layout...
\lhead{}\chead{}\rhead{Savithru Jayasinghe}

\lfoot{}\cfoot{\thepage}\rfoot{}

%%% SECTION TITLE APPEARANCE
\usepackage{sectsty}
\allsectionsfont{\sffamily\mdseries\upshape} % (See the fntguide.pdf for font help)
% (This matches ConTeXt defaults)

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles,subfigure]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!

\renewcommand{\arraystretch}{1.2}


\usepackage{amsmath}
\usepackage{amssymb}	% Math symbols such as \mathbb
\usepackage{cancel}
\usepackage{listings}
\usepackage{caption}
\usepackage{mathtools}
\usepackage{array}
\usepackage{color}
\usepackage{bm}

\newcommand{\abs}[1]{\left| #1 \right|} % for absolute value
\newcommand{\avg}[1]{\left< #1 \right>} % for average
\let\underdot=\d % rename builtin command \d{} to \underdot{}
\renewcommand{\d}[2]{\frac{d #1}{d #2}} % for derivatives
\newcommand{\dd}[2]{\frac{d^2 #1}{d #2^2}} % for double derivatives
\newcommand{\ddd}[2]{\frac{d^3 #1}{d #2^3}} % for triple derivatives
\newcommand{\pd}[2]{\frac{\partial #1}{\partial #2}} % for partial derivatives
\newcommand{\pdd}[2]{\frac{\partial^2 #1}{\partial #2^2}} % for double partial derivatives
\newcommand{\pddd}[2]{\frac{\partial^3 #1}{\partial #2^3}} % for triple partial derivatives
\newcommand{\pdddd}[2]{\frac{\partial^4 #1}{\partial #2^4}} % for triple partial derivatives
\newcommand{\pdc}[3]{\left( \frac{\partial #1}{\partial #2} \right)_{#3}} % for thermodynamic partial derivatives
\newcommand{\grad}[1]{{\nabla} #1} % for gradient
\let\divsymb=\div % rename builtin command \div to \divsymb
\renewcommand{\div}[1]{{\nabla} \cdot #1} % for divergence
\newcommand{\curl}[1]{{\nabla} \times #1} % for curl
\newcommand{\lapl}[1]{{\nabla}^2 #1} % for laplacian
\newcommand{\half}{\frac{1}{2}} %half

\renewcommand{\vec}[1]{\mathbf{#1}} %bold vectors

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{frame=tb,
  language=matlab,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3
}

%%% END Article customizations

%%% The "real" document content comes below...

\title{SANS Documentation - Local Solves}
%\author{SANS}
%\date{} % Activate to display a given date or no date (if empty),
         % otherwise the current date is printed 


\begin{document}
\maketitle
\allowdisplaybreaks

\section{General}

This document describes the implementation of local solves in SANS, specifically for the DG (or possibly even HDG) discretization. Given a target cell in the global mesh and a splitting configuration, we first create a local mesh which contains only the target cell and its immediate neighbors, if any. The target cell (aka \emph{main cell}) in the local mesh is then split according to the specified splitting configuration. In 2D and 3D, this usually also requires splitting some of the neighboring cells in the local mesh, to avoid hanging nodes.

\subsection{Local mesh extraction}
The extraction of the local mesh from the global mesh is done through the \texttt{XFieldLine\_Local}, \texttt{XFieldArea\_Local} and \texttt{XFieldVolume\_Local} classes. All local meshes are constructed so that they satisfy the following properties:
%
\begin{itemize}
\item The target cell (or main-cell) is assigned to \texttt{CellGroup} 0, \texttt{elem} 0 of the local mesh.
\item All (immediate) neighboring cells of the main-cell, if any, are assigned to \texttt{CellGroup} 1.
\item Neighbor-cells are extracted by looping over the canonical traces of the main-cell, in order.
\item The interior-traces between the main-cell and neighbor-cells are oriented so that the main-cell is always to the left.
\item If the main-cell contains any boundaries of the global mesh, those traces are referred to as \emph{main-boundary-traces} and are assigned to a \texttt{BoundaryTraceGroup} of their own.
\item Traces of neighboring cells that are not shared with the main-cell are referred to as \emph{outer-boundary-traces}, and are all assigned to a single \texttt{BoundaryTraceGroup}.
\item This \emph{outer} \texttt{BoundaryTraceGroup} is added last, after all the \texttt{BoundaryTraceGroup}s of the main-boundary-traces.
\end{itemize}


\subsection{Local mesh splitting}
The extracted local mesh is then passed in to the \texttt{XFieldLine\_Local\_Split}, \texttt{XFieldArea\_Local\_Split} and \texttt{XFieldVolume\_Local\_Split} classes to split cells, interior traces and boundary traces as required. The splitting procedure is carried out so that the final split local meshes satisfy the following additional properties:
%
\begin{itemize}
\item The cells produced by splitting the main-cell (referred to as the \emph{main-subcells}) remain in \texttt{CellGroup} 0.
\item Similarly, the cells produced by splitting neighboring cells (referred to as the \emph{neighbor-subcells}) remain in \texttt{CellGroup} 1.
 \item Each split introduces new interior traces within the original main-cell, and also within the original neighbor cells. These new main-main interfaces and neighbor-neighbor interfaces are put into \texttt{InteriorTraceGroup}s of their own. All main-neighbor interfaces (unsplit or split) remain in the same \texttt{InteriorTraceGroup} as before. Neglecting mixed-element meshes, a final split local mesh may contain upto 3 different \texttt{InteriorTraceGroup}s, in the following order:
 	\begin{enumerate}
 		\item Main-Neighbor \texttt{InteriorTraceGroup} - interfaces between main cells/subcells and neighbor cells/subcells.
 		\item Main-Main \texttt{InteriorTraceGroup} - interfaces between main subcells.
 		\item Neighbor-Neighbor \texttt{InteriorTraceGroup} - interfaces between neighbor subcells.
 	\end{enumerate}

\item If any main-boundary-traces are split, then all sub-traces produced from that split remain in the same \texttt{BoundaryTraceGroup} of the original main-boundary-trace.
\item All outer-boundary-traces, split and unsplit, remain in the last \texttt{BoundaryTraceGroup}, as before.
\end{itemize}
%
The coordinate transformations between the reference cell and the sub-cells for each split type are performed by the \texttt{BasisFunction\_RefElement\_Split} class.

\section{1D - Line}

\subsection{Reference element splits}

The labels ``E0'' and ``E1'' show the ordering of the two sub-cells after the split.

\begin{figure}[h!]
\centering %L,B,R,T
\begin{minipage}{.45\textwidth}
  \centering
\includegraphics[width=\linewidth, trim = 15mm 0mm 15mm 0mm, clip]{./images/1D/line_ref_unsplit.eps}
\caption{Reference line element}
\end{minipage}%
\begin{minipage}{.45\textwidth}
  \centering
\includegraphics[width=\linewidth, trim = 15mm 0mm 15mm 0mm, clip]{./images/1D/line_ref_split_isotropic.eps}
\caption{SplitType: Isotropic}
\end{minipage}
\end{figure}

\noindent
Node connectivity:
\begin{itemize}
\item Reference: E0 = \{0, 1\}
\item Isotropic split: E0 = \{0, 2\}, E1 = \{2, 1\}
\end{itemize}


\subsection{Example including neighbors}

The figures below give an example of how a local mesh is extracted and split, from a global mesh with 3 line elements. The target cell (main-cell) is E1. The black triangles are in \texttt{CellGroup} 0 and the blue ones are in \texttt{CellGroup} 1.

\begin{figure}[h!]
\centering %L,B,R,T
\begin{minipage}{.45\textwidth}
  \centering
\includegraphics[width=\linewidth, trim = 15mm 0mm 15mm 0mm, clip]{./images/1D/line_group_unsplit.eps}
\caption{Global mesh}
\end{minipage}%
\begin{minipage}{.45\textwidth}
  \centering
\includegraphics[width=\linewidth, trim = 15mm 0mm 15mm 0mm, clip]{./images/1D/line_group_split_isotropic.eps}
\caption{SplitType: Isotropic}
\end{minipage}
\end{figure}

\clearpage
\section{2D - Triangle}

\subsection{Reference element splits}

The labels ``E0'', ``E1'' ... inside each triangle show the ordering of the sub-cells for each split type. The ordering follows the direction of the \emph{canonical} edge that is being split.

\begin{figure}[h!]
\centering %L,B,R,T
\includegraphics[width=0.37\linewidth, trim = 5mm 5mm 5mm 10mm, clip]{./images/2D/triangle_ref_unsplit.eps}
\caption{Reference triangle element}
\end{figure}

\begin{figure}[h!]
\centering %L,B,R,T
\begin{minipage}{.37\textwidth}
  \centering
\includegraphics[width=\linewidth, trim = 5mm 5mm 5mm 10mm, clip]{./images/2D/triangle_ref_split_edge0.eps}
\caption{SplitType: Edge-0}
\end{minipage}%
\begin{minipage}{.37\textwidth}
  \centering
\includegraphics[width=\linewidth, trim = 5mm 5mm 5mm 10mm, clip]{./images/2D/triangle_ref_split_edge1.eps}
\caption{SplitType: Edge-1}
\end{minipage}
\end{figure}

\begin{figure}[h!]
\centering %L,B,R,T
\begin{minipage}{.37\textwidth}
  \centering
\includegraphics[width=\linewidth, trim = 5mm 5mm 5mm 10mm, clip]{./images/2D/triangle_ref_split_edge2.eps}
\caption{SplitType: Edge-2}
\end{minipage}%
\begin{minipage}{.37\textwidth}
  \centering
\includegraphics[width=\linewidth, trim = 5mm 5mm 5mm 10mm, clip]{./images/2D/triangle_ref_split_isotropic.eps}
\caption{SplitType: Isotropic}
\end{minipage}
\end{figure}

\clearpage

\noindent
Node connectivity:
\begin{itemize}
\item Reference: E0 = \{0, 1, 2\}
\item Edge-0 split: E0 = \{0,1,3\}, E1 = \{0,3,2\}
\item Edge-1 split: E0 = \{3,1,2\}, E1 = \{0,1,3\}
\item Edge-2 split: E0 = \{0,3,2\}, E1 = \{3,1,2\}
\item Isotropic split: E0 = \{5, 1, 3\}, E1 = \{4, 3, 2\}, E2 = \{0, 5, 4\}, E3 = \{3, 4, 5\}
\end{itemize}

\subsection{Example including neighbors}

\begin{figure}[h!]
\centering %L,B,R,T
\includegraphics[width=0.37\linewidth, trim = 5mm 5mm 5mm 10mm, clip]{./images/2D/triangle_group_unsplit.eps}
\caption{Global mesh}
\end{figure}
%
\noindent
The figures below show the resulting split local meshes, for the case where the target cell (main-cell) is E0. The black triangles are in \texttt{CellGroup} 0 and the blue ones are in \texttt{CellGroup} 1.

\begin{figure}[h!]
\centering %L,B,R,T
\begin{minipage}{.37\textwidth}
  \centering
\includegraphics[width=\linewidth, trim = 5mm 5mm 5mm 10mm, clip]{./images/2D/triangle_group_split_edge1.eps}
\caption{SplitType: Edge-1}
\end{minipage}%
\begin{minipage}{.37\textwidth}
  \centering
\includegraphics[width=\linewidth, trim = 5mm 5mm 5mm 10mm, clip]{./images/2D/triangle_group_split_isotropic.eps}
\caption{SplitType: Isotropic}
\end{minipage}
\end{figure}

\clearpage
\section{3D - Tetrahedron}

\subsection{Reference element splits}

The labels ``E0'', ``E1'' ... inside each tetrahedron show the ordering of the sub-cells for each split type. The ordering follows the direction of the \emph{canonical} edge that is being split.

\begin{figure}[h!]
\centering %L,B,R,T
\begin{minipage}{.5\textwidth}
  \centering
\includegraphics[width=\linewidth, trim = 15mm 15mm 15mm 15mm, clip]{./images/3D/tet_ref_unsplit.png}
\caption{Reference element}
\end{minipage}%
\begin{minipage}{.5\textwidth}
  \centering
\includegraphics[width=\linewidth, trim = 15mm 15mm 15mm 15mm, clip]{./images/3D/tet_ref_split_edge0.png}
\caption{SplitType: Edge-0}
\end{minipage}
\end{figure}

\begin{figure}[h!]
\centering %L,B,R,T
\begin{minipage}{.5\textwidth}
  \centering
\includegraphics[width=\linewidth, trim = 15mm 15mm 15mm 15mm, clip]{./images/3D/tet_ref_split_edge1.png}
\caption{SplitType: Edge-1}
\end{minipage}%
\begin{minipage}{.5\textwidth}
  \centering
\includegraphics[width=\linewidth, trim = 15mm 15mm 15mm 15mm, clip]{./images/3D/tet_ref_split_edge2.png}
\caption{SplitType: Edge-2}
\end{minipage}
\end{figure}

\clearpage

\begin{figure}[h!]
\centering %L,B,R,T
\begin{minipage}{.5\textwidth}
  \centering
\includegraphics[width=\linewidth, trim = 15mm 15mm 15mm 15mm, clip]{./images/3D/tet_ref_split_edge3.png}
\caption{SplitType: Edge-3}
\end{minipage}%
\begin{minipage}{.5\textwidth}
  \centering
\includegraphics[width=\linewidth, trim = 15mm 15mm 15mm 15mm, clip]{./images/3D/tet_ref_split_edge4.png}
\caption{SplitType: Edge-4}
\end{minipage}
\end{figure}

\begin{figure}[h!]
\centering %L,B,R,T
\begin{minipage}{.5\textwidth}
  \centering
\includegraphics[width=\linewidth, trim = 15mm 15mm 15mm 15mm, clip]{./images/3D/tet_ref_split_edge5.png}
\caption{SplitType: Edge-5}
\end{minipage}%
\begin{minipage}{.5\textwidth}
  \centering
\includegraphics[width=\linewidth, trim = 15mm 15mm 15mm 15mm, clip]{./images/3D/tet_ref_split_isotropic.png}
\caption{SplitType: Isotropic}
\end{minipage}
\end{figure}

\begin{figure}[h!]
\centering %L,B,R,T
\begin{minipage}{.5\textwidth}
  \centering
\includegraphics[width=\linewidth, trim = 15mm 15mm 15mm 15mm, clip]{./images/3D/tet_ref_split_isotropicface0.png}
\caption{SplitType: IsotropicFace-0}
\end{minipage}%
\begin{minipage}{.5\textwidth}
  \centering
\includegraphics[width=\linewidth, trim = 15mm 15mm 15mm 15mm, clip]{./images/3D/tet_ref_split_isotropicface1.png}
\caption{SplitType: IsotropicFace-1}
\end{minipage}
\end{figure}

\clearpage

\begin{figure}[h!]
\centering %L,B,R,T
\begin{minipage}{.5\textwidth}
  \centering
\includegraphics[width=\linewidth, trim = 15mm 15mm 15mm 15mm, clip]{./images/3D/tet_ref_split_isotropicface2.png}
\caption{SplitType: IsotropicFace-2}
\end{minipage}%
\begin{minipage}{.5\textwidth}
  \centering
\includegraphics[width=\linewidth, trim = 15mm 15mm 15mm 15mm, clip]{./images/3D/tet_ref_split_isotropicface3.png}
\caption{SplitType: IsotropicFace-3}
\end{minipage}
\end{figure}

\vspace{2em}

\noindent
Node connectivity:
\begin{itemize}
\item Reference: E0 = \{0, 1, 2, 3\}
\item Edge-0 split: E0 = \{0, 1, 2, 4\}, E1 = \{0, 1, 4, 3\}
\item Edge-1 split: E0 = \{0, 4, 2, 3\}, E1 = \{0, 1, 2, 4\}
\item Edge-2 split: E0 = \{0, 1, 4, 3\}, E1 = \{0, 4, 2, 3\}
\item Edge-3 split: E0 = \{4, 1, 2, 3\}, E1 = \{0, 1, 4, 3\}
\item Edge-4 split: E0 = \{0, 1, 2, 4\}, E1 = \{4, 1, 2, 3\}
\item Edge-5 split: E0 = \{0, 4, 2, 3\}, E1 = \{4, 1, 2, 3\}
\item Isotropic split:
	\begin{itemize}
	\item E0 = \{7, 6, 2, 4\}, E1 = \{8, 5, 4, 3\}, E2 = \{9, 1, 6, 5\}, E3 = \{0, 9, 7, 8\},
	\item E4 = \{7, 6, 4, 5\}, E5 = \{8, 4, 5, 7\}, E6 = \{9, 5, 6, 7\}, E7 = \{9, 7, 8, 5\}
	\end{itemize}
\item IsotropicFace-0 split: E0 = \{0, 6, 2, 4\}, E1 = \{0, 5, 4, 3\}, E2 = \{0, 1, 6, 5\}, E3 = \{0, 4, 5, 6\}
\item IsotropicFace-1 split: E0 = \{6, 1, 4, 3\}, E1 = \{5, 1, 2, 4\}, E2 = \{0, 1, 5, 6\}, E3 = \{4, 1, 6, 5\}
\item IsotropicFace-2 split: E0 = \{6, 1, 2, 4\}, E1 = \{5, 4, 2, 3\}, E2 = \{0, 6, 2, 5\}, E3 = \{4, 5, 2, 6\}
\item IsotropicFace-3 split: E0 = \{5, 4, 2, 3\}, E1 = \{6, 1, 4, 3\}, E2 = \{0, 6, 5, 3\}, E3 = \{4, 5, 6, 3\}
\end{itemize}

\vspace{1em}
\noindent
Currently, the main cell can be split using any of the 6 \texttt{Edge} splits or the \texttt{Isotropic} split. If the main cell is split isotropically, then each of its neighbors need to be split using one of the 4 \texttt{IsotropicFace} configurations.

\clearpage
\subsection{Examples including neighbors}

\begin{figure}[h!]
\centering %L,B,R,T
\includegraphics[width=0.6\linewidth, trim = 15mm 15mm 15mm 15mm, clip]{./images/3D/tet_group1_unsplit.png}
\caption{Global mesh with 3 tets}
\end{figure}

\noindent
The figures below show the resulting split local meshes, for the case where the target cell (main-cell) in the global mesh is E0. The yellow tets are in \texttt{CellGroup} 0 and the purple tets are in \texttt{CellGroup} 1.

\begin{figure}[h!]
\centering %L,B,R,T
\includegraphics[width=0.6\linewidth, trim = 15mm 15mm 15mm 15mm, clip]{./images/3D/tet_group1_split_edge1.png}
\caption{SplitType: Edge-1}
\end{figure}

\begin{figure}[h!]
\centering %L,B,R,T
\includegraphics[width=0.6\linewidth, trim = 15mm 15mm 15mm 15mm, clip]{./images/3D/tet_group1_split_edge2.png}
\caption{SplitType: Edge-2}
\end{figure}

%\begin{figure}[h!]
%\centering %L,B,R,T
%\begin{minipage}{.5\textwidth}
%  \centering
%\includegraphics[width=\linewidth, trim = 15mm 15mm 15mm 15mm, clip]{./images/3D/tet_group1_split_edge1.png}
%\caption{SplitType: Edge-1}
%\end{minipage}%
%\begin{minipage}{.5\textwidth}
%  \centering
%\includegraphics[width=\linewidth, trim = 15mm 15mm 15mm 15mm, clip]{./images/3D/tet_group1_split_edge2.png}
%\caption{SplitType: Edge-2}
%\end{minipage}
%\end{figure}

\clearpage

\begin{figure}[h!]
\centering %L,B,R,T
\includegraphics[width=0.8\linewidth, trim = 5mm 5mm 5mm 10mm, clip]{./images/3D/tet_group2_unsplit.png}
\caption{Global mesh with 2 tets}
\end{figure}

\noindent
The target cell (main-cell) in the above global mesh is E0. The yellow tets in the local mesh are in \texttt{CellGroup} 0 and the purple tets are in \texttt{CellGroup} 1.

\begin{figure}[h!]
\centering %L,B,R,T
\includegraphics[width=0.8\linewidth, trim = 5mm 5mm 5mm 10mm, clip]{./images/3D/tet_group2_split_isotropic.png}
\caption{SplitType: Isotropic}
\end{figure}

\end{document}

