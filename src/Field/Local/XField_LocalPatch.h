// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SANS_FIELD_LOCAL_PATCH_H_
#define SANS_FIELD_LOCAL_PATCH_H_

#include <map>
#include <set>

#include "Field/Partition/LagrangeElementGroup.h"
#include "Field/XField_CellToTrace.h"
#include "Field/SpaceTypes.h"

#include "XField_LocalPatchBase.h"
#include "XField_LocalPatchConstructor.h"

namespace SANS
{

template<class PhysDim,class TopoDim> class XField_CellToTrace;
class Field_NodalView;

namespace XField_LocalPatch_Utils
{
  template<class PhysDim,class Topology>
  void allWithEdge( const XField<PhysDim,typename Topology::TopoDim>& xfld,
                    const Field_NodalView* xfld_nodalView, // possibly null
                    int e0, int e1, std::set<std::pair<int,int>>& cells );
}

template<class,class> class TraceSoup;

template<class PhysDim, class Topology>
class XField_LocalPatch : public XField_LocalPatchBase<PhysDim,Topology>
{
protected:
  typedef XField_LocalPatchBase<PhysDim,Topology> BaseType;
  typedef DLA::VectorS<PhysDim::D,Real> VectorX;
  typedef typename BaseType::GlobalDOFType GlobalDOFType;

  typedef typename Topology::TopologyTrace TraceTopology;
  typedef typename Topology::TopoDim TopoDim;

  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TraceTopology> XFieldTraceGroupType;

  typedef XField<PhysDim,TopoDim> XField_Type;

  // some labels for readability
  using BaseType::RESOLVE_CELL_GROUP;
  using BaseType::FIXED_CELL_GROUP;
  using BaseType::STRAIGHT_SIDED;

public:

   /*\
    * XField_LocalPatch constructor:
    *  construct a SPLIT local patch from an existing unsplit one
    *
    * input:
    *   xfld_local: local (already extracted) xfield which we will split
    *   edge0:      indices of the edge we will split
    *   global:     whether the edge0 indices are in the xfld_global map or the local one
    *
    * output:
    *   a fully split local grid, ready for a local solve
  \*/
  XField_LocalPatch( const XField_LocalPatchConstructor<PhysDim,Topology>& xfld_local,
                     ElementSplitType split_type, int split_edge_index );

   /*\
    * XField_LocalPatch constructor:
    *  construct a SPLIT local patch from an existing unsplit one
    *
    * input:
    *   xfld_local: local (already extracted) xfield which we will split
    *   edge0:      global indices of the edge we will split
    *
    * output:
    *   a fully split local grid, ready for a local solve
  \*/
  XField_LocalPatch( const XField_LocalPatchConstructor<PhysDim,Topology>& xfld_local,
                     const int e0, const int e1 );


  /*\
   * XField_LocalPatch constructor:
   *  construct a SPLIT local patch from an existing unsplit one
   *
   * input:
   *   xfld_local: local (already extracted) xfield which we will split
   *   edge:       global indices of the edge we will split
   *
   * output:
   *   a fully split local grid, ready for a local solve
  \*/
  XField_LocalPatch( const XField_LocalPatchConstructor<PhysDim,Topology>& xfld_local,
                     const std::array<int,2>& edge );

  /*\
   * XField_LocalPatch constructor:
   *   constructor for EXTRACTION and SPLIT
   *   Does a single step recursion if isSplit is true, calling itself with false to make an unsplit then splitting that
   *
   *   use addResolveElement and addFixedElement to extract the local patch attached to an edge
   *
   * input:
   *   connectivity:   xfield connectivity structure (for traversing cells to traces)
   *
   * output:
   *   a local xfield which is ready for cells to be added
   *   i.e. you need to call "addResolveElement", "addFixedElement" (or "addNeighbours")
   *        and finally call "extract" to do all the dirty work
   *
  \*/
  XField_LocalPatch( mpi::communicator& comm,
                     const XField_CellToTrace<PhysDim,TopoDim>& connectivity,
                     const std::array<int,2> edge, SpaceType type,
                     const Field_NodalView* xfld_nodalView );

  /*\
   * XField_LocalPatch constructor:
   *  construct a unsplit local patch for unit testing
   *
   * input:
   *   xfld_local: local (already extracted) xfield which we will split
   *
   * output:
   *   an unsplit local grid, ready for a local solve
  \*/
  XField_LocalPatch( const XField_LocalPatchConstructor<PhysDim,Topology>& xfld_local );

protected:
  /*\
   * split:
   *   performs the split of the input local xfield on edge (e0,e1).
   *   edge indices are already in local indexing of xfld_local.
   *
   * input:
   *   xfld_local: the local xfield extracted from xfld_global
   *   e0,e1: the indices of the edge to be split (in xfld_local indexing)
   *
   * output:
   *   the entire inherited XField structure is filled and checked
  \*/
  void split( const XField_LocalPatchConstructor<PhysDim,Topology>& xfld_local, int e0, int e1 );

public:
  // return the typeID, for run time checking
  virtual const std::type_info& derivedTypeID() const { return typeid(XField_LocalPatch); }

  // Resolving the diamond inheritence pattern, without requiring the Topology template

  using BaseType::nCellGroups;
  using BaseType::nInteriorTraceGroups;
  using BaseType::nBoundaryTraceGroups;
  using BaseType::nGhostBoundaryTraceGroups;

  // The default versions with the Topology Template still there
  using BaseType::getCellGroup;
  using BaseType::getInteriorTraceGroup;
  using BaseType::getBoundaryTraceGroup;
  using BaseType::getGhostBoundaryTraceGroup;

  // Some stripped away versions
        XFieldCellGroupType& getCellGroup( const int group )
        { return BaseType::template getCellGroup<Topology>(group); }
  const XFieldCellGroupType& getCellGroup( const int group ) const
        { return BaseType::template getCellGroup<Topology>(group); }

        XFieldTraceGroupType& getInteriorTraceGroup( const int group )
        { return BaseType::template getInteriorTraceGroup<typename Topology::TopologyTrace>(group); }
  const XFieldTraceGroupType& getInteriorTraceGroup( const int group ) const
        { return BaseType::template getInteriorTraceGroup<typename Topology::TopologyTrace>(group); }

        XFieldTraceGroupType& getBoundaryTraceGroup( const int group )
        { return BaseType::template getBoundaryTraceGroup<typename Topology::TopologyTrace>(group); }
  const XFieldTraceGroupType& getBoundaryTraceGroup( const int group ) const
        { return BaseType::template getBoundaryTraceGroup<typename Topology::TopologyTrace>(group); }

        // XFieldTraceGroupType& getGhostBoundaryTraceGroup( const int group )
        // { return BaseType::template getGhostBoundaryTraceGroup<typename Topology::TopologyTrace>(group); }
  const XFieldTraceGroupType& getGhostBoundaryTraceGroup( const int group ) const
        { return BaseType::template getGhostBoundaryTraceGroup<typename Topology::TopologyTrace>(group); }

private:

  using BaseType::spaceType_;

  using BaseType::order_;

  using BaseType::DOF_buffer_; // Buffer of DOFs mapped back to the global grid (per processor)
  using BaseType::cellGroups_;

  // we need this to finalize the mesh as in XField_impl.h
  using XField_Type::cellIDs_;
  using XField_Type::boundaryTraceIDs_;

  // map from global xfield to local dof
  using BaseType::global2localDOFmap_; // maps global DOF index to a local DOF index

  // things that will get stuffed upon cell and trace processing
  // we'll build the xfield groups from these
  using BaseType::interiorLagrangeGroups_;
  using BaseType::boundaryLagrangeGroups_;
  using BaseType::ghostBoundaryLagrangeGroups_;

  // the global xfield from which the local mesh is extracted and its connectivity structure
  using BaseType::xfld_global_;

  // classification of boundary traces when a local xfield is SPLIT
  // we need to unsplit local xfield (patch) to look up the boundary trace group id's
  void classifyBoundaryTraces( const std::vector< UniqueTraceMap >& uniqueTraces,
                               const TraceSoup<PhysDim,Topology>& soup );

  // functions for creating the ElemSplitInfo for either extracted or split xfields
  void findTraceMappings_split( const XField_LocalPatchConstructor<PhysDim,Topology>& xfld_local,
                                const TraceSoup<PhysDim,Topology>& soup, int es );

  using BaseType::connectivity_;
  using BaseType::xfld_nodalView_;

  // map from {global boundary groups and local cell groups} to local boundary group
  // we need the association with local cell groups specifically for CG local solves
  // where the same global boundary group might touch both resolve and fixed cell groups
  using BaseType::boundaryGroupMap_;
  using BaseType::local2globalBoundaryGroupMap_;

  // the main cell dof which will be used for splitting
  using BaseType::mainCellNodes_;

  using BaseType::nElem_;

  using BaseType::edgeMode_;
  using BaseType::linearCommonNodes_;
};

} // SANS

#endif
