// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XFieldVolume_Local_Split.h"

#include "XFieldVolume.h"

namespace SANS
{

template <class PhysDim>
XField_Local_Split<PhysDim, TopoD3>::XField_Local_Split(const XField_Local<PhysDim, TopoD3>& xfld_local,
                                                        XField_CellToTrace<PhysDim,TopoD3>& connectivity,
                                                        ElementSplitType split_type, int split_edge_index)
  : BaseType(xfld_local.comm()), xfld_local_unsplit_(xfld_local), connectivity_(connectivity)
{

  getOrder(); //Get the basis order for each cell group

  if (split_type==ElementSplitType::Edge)
  {

    XField_Local_Split_Linear_Edge<PhysDim,TopoD3>
    xfld_split_local_linear_edge(xfld_local, connectivity, split_type, split_edge_index);

    //Build a higher-order split local mesh if required
    if (orderVector_[0]==1)
      this->BaseType::cloneFrom(xfld_split_local_linear_edge);
    else
      this->buildFrom(xfld_split_local_linear_edge,orderVector_[0]);

    copyInfo(xfld_split_local_linear_edge); //Copying some variables from the linear split mesh, before it goes out of scope.
  }
  else if (split_type==ElementSplitType::Isotropic)
  {

    XField_Local_Split_Linear_Isotropic<PhysDim,TopoD3>
    xfld_split_local_linear_isotropic(xfld_local, connectivity, split_type, split_edge_index);

    //Build a higher-order split local mesh if required
    if (orderVector_[0]==1)
      this->BaseType::cloneFrom(xfld_split_local_linear_isotropic);
    else
      this->buildFrom(xfld_split_local_linear_isotropic,orderVector_[0]);

    copyInfo(xfld_split_local_linear_isotropic); //Copying some variables from the linear split mesh, before it goes out of scope.
  }

  projectMainCell(split_type, split_edge_index); //Project DOFs from unsplit local mesh to split mesh

  // This is checkGrid() but gets removed in release mode!
  this->checkGrid();

  for (int i = 0; i < this->nBoundaryTraceGroups(); i++)
    reSolveBoundaryTraceGroups_.push_back(i);

  for (int i = 0; i < this->nInteriorTraceGroups(); i++)
    reSolveInteriorTraceGroups_.push_back(i);
}


template <class PhysDim>
void
XField_Local_Split<PhysDim, TopoD3>::getOrder()
{

  orderVector_.clear();

  for (int group=0; group<xfld_local_unsplit_.nCellGroups(); group++)
  {

    if ( xfld_local_unsplit_.getCellGroupBase(group).topoTypeID() == typeid(Tet) )
    {
      typedef typename XField<PhysDim, TopoD3>::template FieldCellGroupType<Tet> XFieldCellGroupType;
      typedef typename XFieldCellGroupType::BasisType BasisType;

      const XFieldCellGroupType& cellgrp = xfld_local_unsplit_.template getCellGroup<Tet>(group);
      const BasisType* cell_basis = cellgrp.basis();
      orderVector_.push_back(cell_basis->order());
    }
    else if ( xfld_local_unsplit_.getCellGroupBase(group).topoTypeID() == typeid(Hex) )
    {
      typedef typename XField<PhysDim, TopoD3>::template FieldCellGroupType<Hex> XFieldCellGroupType;
      typedef typename XFieldCellGroupType::BasisType BasisType;

      const XFieldCellGroupType& cellgrp = xfld_local_unsplit_.template getCellGroup<Hex>(group);
      const BasisType* cell_basis = cellgrp.basis();
      orderVector_.push_back(cell_basis->order());
    }
    else
      SANS_DEVELOPER_EXCEPTION( "XField_Local_Split<PhysDim, TopoD3, T>::getOrder - Unknown cell topology." );

  }

}

template <class PhysDim>
void
XField_Local_Split<PhysDim, TopoD3>::copyInfo(XField_Local_Split_Linear_Edge<PhysDim,TopoD3>& xfld_split_local_linear)
{

  this->CellMapping_ = xfld_split_local_linear.CellMapping_;
  this->InteriorTraceMapping_ = xfld_split_local_linear.InteriorTraceMapping_;
  this->BoundaryTraceMapping_ = xfld_split_local_linear.BoundaryTraceMapping_;

  this->CellSplitInfo_ = xfld_split_local_linear.CellSplitInfo_;
  this->InteriorTraceSplitInfo_ = xfld_split_local_linear.InteriorTraceSplitInfo_;
  this->BoundaryTraceSplitInfo_ = xfld_split_local_linear.BoundaryTraceSplitInfo_;

  SplitEdgeIndex_ = xfld_split_local_linear.SplitEdgeIndex_;
  IsotropicSplitFlag_ = false;

  SplitEdgeNodeMap_[0] = xfld_split_local_linear.SplitEdgeNodeMap_[0];
  SplitEdgeNodeMap_[1] = xfld_split_local_linear.SplitEdgeNodeMap_[1];

  CellGroupTetNeighbors_ = xfld_split_local_linear.CellGroupTetNeighbors_;
  CellGroupHexNeighbors_ = xfld_split_local_linear.CellGroupHexNeighbors_;

  this->newNodeDOFs_ = xfld_split_local_linear.getNewNodeDOFs();
  //offset the nodeDOF indices to be the last (after cell, face and edge DOFs)
  for (std::size_t i = 0; i < this->newNodeDOFs_.size(); i++)
    this->newNodeDOFs_[i] += this->nDOF() - xfld_split_local_linear.nDOF();
}

template <class PhysDim>
void
XField_Local_Split<PhysDim, TopoD3>::copyInfo(XField_Local_Split_Linear_Isotropic<PhysDim,TopoD3>& xfld_split_local_linear)
{

  this->CellMapping_ = xfld_split_local_linear.CellMapping_;
  this->InteriorTraceMapping_ = xfld_split_local_linear.InteriorTraceMapping_;
  this->BoundaryTraceMapping_ = xfld_split_local_linear.BoundaryTraceMapping_;

  this->CellSplitInfo_ = xfld_split_local_linear.CellSplitInfo_;
  this->InteriorTraceSplitInfo_ = xfld_split_local_linear.InteriorTraceSplitInfo_;
  this->BoundaryTraceSplitInfo_ = xfld_split_local_linear.BoundaryTraceSplitInfo_;

  SplitEdgeIndex_ = -1;
  IsotropicSplitFlag_ = true;

  CellGroupTetNeighbors_ = xfld_split_local_linear.CellGroupTetNeighbors_;
  CellGroupHexNeighbors_ = xfld_split_local_linear.CellGroupHexNeighbors_;

  this->newNodeDOFs_ = xfld_split_local_linear.getNewNodeDOFs();
  //offset the nodeDOF indices to be the last (after cell, face and edge DOFs)
  for (std::size_t i = 0; i < this->newNodeDOFs_.size(); i++)
    this->newNodeDOFs_[i] += this->nDOF() - xfld_split_local_linear.nDOF();
}

template <class PhysDim>
void
XField_Local_Split<PhysDim, TopoD3>::projectMainCell(ElementSplitType split_type, int split_edge_index)
{
  int main_group = 0;
  int main_elem = 0;

  if ( xfld_local_unsplit_.getCellGroupBase(main_group).topoTypeID() == typeid(Tet) )
  {
    typedef typename BasisFunctionTopologyBase<TopoD3, Tet>::type BasisType;
    const BasisType* basis = xfld_local_unsplit_.template getCellGroup<Tet>(main_group).basis();
    Element_Subdivision_Projector<TopoD3, Tet> elemProjector(basis);

    if (IsotropicSplitFlag_)
    {
      //Loop across the sub-elements
      for (int sub_elem = 0; sub_elem < 8; sub_elem++)
        projectElem<Tet>(main_group, main_elem, main_group, sub_elem, elemProjector);
    }
    else
    {
      //Loop across the sub-elements
      for (int sub_elem = 0; sub_elem < 2; sub_elem++)
        projectElem<Tet>(main_group, main_elem, main_group, sub_elem, elemProjector);
    }

    projectNeighbors(Tet::NTrace, orderVector_[0], split_type, split_edge_index);
  }
  else if ( xfld_local_unsplit_.getCellGroupBase(main_group).topoTypeID() == typeid(Hex) )
  {
    SANS_DEVELOPER_EXCEPTION( "XField_Local_Split<PhysDim, TopoD3, T>::projectMainCell - Hex splitting not supported yet.");

#if 0
    typedef typename BasisFunctionTopologyBase<TopoD3, Hex>::type BasisType;
    const BasisType* basis = xfld_local_unsplit_.template getCellGroup<Hex>(main_group).basis();
    Element_Subdivision_Projector<TopoD3, Hex> elemProjector(basis);

    if (IsotropicSplitFlag_)
    {
      //Loop across the sub-elements
      for (int sub_elem = 0; sub_elem < 4; sub_elem++)
        projectElem<Hex>(main_group, main_elem, main_group, sub_elem, elemProjector);
    }
    else
    {
      //Loop across the sub-elements
      for (int sub_elem = 0; sub_elem < 2; sub_elem++)
        projectElem<Hex>(main_group, main_elem, main_group, sub_elem, elemProjector);
    }

    projectNeighbors(Hex::NTrace, orderVector_[0], split_type);
#endif
  }
  else
    SANS_DEVELOPER_EXCEPTION( "XField_Local_Split<PhysDim, TopoD3, T>::projectMainCell - Unknown cell topology." );
}

template <class PhysDim>
void
XField_Local_Split<PhysDim, TopoD3>::projectNeighbors(int Ntrace, int order, ElementSplitType split_type, int split_edge_index)
{

  int main_group = 0;
  int main_elem = 0;

  //counters to keep track of the neighbor cells which have been processed (includes splits)
    int cnt_neighbor_tet = 0;
  //  int cnt_neighbor_hex = 0;

  //-----------Fill in associativity for neighboring cells--------------------

  for (int itrace = 0; itrace < Ntrace; itrace++)
  {
    //Get indices of each neighboring trace (and its cellgroup)
    const TraceInfo& traceinfo = connectivity_.getTrace(main_group, main_elem, itrace);

    if (traceinfo.type == TraceInfo::Interior)
    {
      int neighbor_group = -1; //cellgroup of neighboring element in global mesh
      int neighbor_elem = -1; //element index of neighboring element in global mesh
      CanonicalTraceToCell neighbor_canonicalTrace; //canonicalTrace of the neighboring cell for the current trace
      CanonicalTraceToCell main_canonicalTrace; //canonicalTrace of the main cell for the current trace

      if (xfld_local_unsplit_.getInteriorTraceGroupBase(traceinfo.group).topoTypeID() == typeid(Triangle))
      {
        getNeighborInfoFromTrace<Triangle>(traceinfo.group, traceinfo.elem, main_elem,
                                           neighbor_group, neighbor_elem,
                                           neighbor_canonicalTrace, main_canonicalTrace);
      }
      else if (xfld_local_unsplit_.getInteriorTraceGroupBase(traceinfo.group).topoTypeID() == typeid(Quad))
      {
        getNeighborInfoFromTrace<Quad>(traceinfo.group, traceinfo.elem, main_elem,
                                       neighbor_group, neighbor_elem,
                                       neighbor_canonicalTrace, main_canonicalTrace);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "XField_Local<PhysDim, TopoD3, T>::projectNeighbors - Unknown trace topology for interior trace." );


      if ( xfld_local_unsplit_.getCellGroupBase(neighbor_group).topoTypeID() == typeid(Tet) )
      {
        typedef typename BasisFunctionTopologyBase<TopoD3, Tet>::type BasisType;
        const BasisType* basis = xfld_local_unsplit_.template getCellGroup<Tet>(main_group).basis();
        Element_Subdivision_Projector<TopoD3, Tet> elemProjector(basis);

        if (IsotropicSplitFlag_)
        { //Isotropic-split

          for (int i = 0; i < 4; i++)
            projectElem<Tet>(neighbor_group, neighbor_elem, CellGroupTetNeighbors_, cnt_neighbor_tet+i, elemProjector);

          cnt_neighbor_tet += 4; //Added four (split) tet neighbors
        }
        else
        { //Edge-split

          //Get the list of traces that are affected from this edge-split
          std::vector<int> split_trace_list = getSplitTracesFromSplitEdge<Tet>(SplitEdgeIndex_);

          bool split_neighbor_flag = false;

          for (int i = 0; i < (int) split_trace_list.size(); i++)
          {
            if (itrace==split_trace_list[i])
              split_neighbor_flag = true;
          }

          if (split_neighbor_flag)
          { //Split neighbor
            projectElem<Tet>(neighbor_group, neighbor_elem, CellGroupTetNeighbors_, cnt_neighbor_tet  , elemProjector);
            projectElem<Tet>(neighbor_group, neighbor_elem, CellGroupTetNeighbors_, cnt_neighbor_tet+1, elemProjector);
            cnt_neighbor_tet += 2; //Added two (split) tet neighbors
          }
          else
          { //Original neighbor (just copy from unsplit mesh)
            projectElem<Tet>(neighbor_group, neighbor_elem, CellGroupTetNeighbors_, cnt_neighbor_tet, elemProjector);
            cnt_neighbor_tet++; //Added only one (unsplit) tet neighbor
          }

        } //if-isotropic
      }
      else if ( xfld_local_unsplit_.getCellGroupBase(neighbor_group).topoTypeID() == typeid(Hex) )
      {
        SANS_DEVELOPER_EXCEPTION( "XField_Local_Split<PhysDim, TopoD3, T>::projectNeighbors - Hex splitting not supported yet.");
      }
      else
        SANS_DEVELOPER_EXCEPTION( "XField_Local_Split<PhysDim, TopoD3, T>::projectNeighbors - Unknown cell topology for neighbor." );
    }
  } //loop across traces of main element
}

template <class PhysDim>
template <class TopoCell>
void
XField_Local_Split<PhysDim, TopoD3>::projectElem(int cell_group, int cell_elem, int sub_group, int sub_elem,
                                                 Element_Subdivision_Projector<TopoD3, TopoCell>& projector)
{

  typedef typename XField<PhysDim, TopoD3>::template FieldCellGroupType<TopoCell> XFieldCellGroupType;
  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef typename XFieldCellGroupType::BasisType BasisType;

  const XFieldCellGroupType& cellgrp = xfld_local_unsplit_.template getCellGroup<TopoCell>(cell_group);

  const BasisType* cell_basis = cellgrp.basis();

  ElementXFieldClass xfldElem_main( cell_basis ); //Unsplit element
  ElementXFieldClass xfldElem_sub( cell_basis ); //One of the split elements

  cellgrp.getElement(xfldElem_main, cell_elem); //Get unsplit element from unsplit local mesh

  ElementSplitInfo splitinfo = this->getCellSplitInfo({sub_group,sub_elem});

  if (splitinfo.split_flag == ElementSplitFlag::Split)
  {
    projector.project(xfldElem_main, xfldElem_sub, splitinfo.split_type, splitinfo.edge_index, splitinfo.subcell_index);
    this->template getCellGroup<TopoCell>(sub_group).setElement(xfldElem_sub,sub_elem);
  }
  else if (splitinfo.split_flag == ElementSplitFlag::Unsplit)
  {
    this->template getCellGroup<TopoCell>(sub_group).setElement(xfldElem_main,sub_elem);
  }
  else
    SANS_DEVELOPER_EXCEPTION( "XField_Local_Split<PhysDim, TopoD3, T>::projectElem - ElementSplitFlag should be either 'Split' or 'Unsplit'." );
}

template<class PhysDim>
template <class TopoTrace>
void
XField_Local_Split< PhysDim, TopoD3>::
getNeighborInfoFromTrace(const int trace_group, const int trace_elem, const int main_elem,
                         int& neighbor_group, int& neighbor_elem,
                         CanonicalTraceToCell& neighbor_canonicalTrace, CanonicalTraceToCell& main_canonicalTrace)
{
  typedef typename XField<PhysDim, TopoD3>::template FieldTraceGroupType<TopoTrace> XFieldTraceGroupType;
  const XFieldTraceGroupType& tracegrp = xfld_local_unsplit_.template getInteriorTraceGroup<TopoTrace>(trace_group);

  if (tracegrp.getElementLeft(trace_elem) == main_elem)
  { //element to left of trace is the "main_elem" => getElementRight gives the neighboring cell
    neighbor_group          = tracegrp.getGroupRight();
    neighbor_elem           = tracegrp.getElementRight(trace_elem);
    neighbor_canonicalTrace = tracegrp.getCanonicalTraceRight(trace_elem);
    main_canonicalTrace     = tracegrp.getCanonicalTraceLeft(trace_elem);
  }
  else
    SANS_DEVELOPER_EXCEPTION("XField_Local_Split<PhysDim, TopoD3, T>::getNeighborInfoFromTrace - "
                             "The main-cell is not to the left of its neighbors in the unsplit local mesh.");
}

//Explicit instantiations
template class XField_Local_Split<PhysD3,TopoD3>;

}
