// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELDAREA_LOCAL_H
#define XFIELDAREA_LOCAL_H

#include <vector>

#include "Topology/Dimension.h"

#include "XField_CellToTrace.h"
#include "XField.h"
#include "XField_Local_Base.h"

#include "Field/Element/ElementAssociativityLineConstructor.h"
#include "Field/Element/ElementAssociativityAreaConstructor.h"

/*
 * This class constructs a 2D local mesh for a specified cell in the 2D global mesh.
 * The constructed local mesh will have the following properties:
 * - The target cell (or main-cell) will be assigned to cellgroup 0, element 0
 * - All (immediate) neighboring cells of the main-cell, if any, will be assigned to cellgroup 1
 * - The traces (edges) around the main-cell will be oriented so that the main-cell is always to the left.
 * - If the main-cell touches any boundaries of the global mesh, those traces (edges) will be assigned to a boundary-trace group of their own.
 * - Traces (edges) of neighboring cells that do not touch the main-cell (referred to as "outer-boundary-traces") will all be assigned
 *   to a single boundary-trace group. This boundary-trace group goes last.
 */

/* Refer to the local solve documentation in Field/Documentation/Local_Solves/LocalSolves.pdf for more details and figures. */

namespace SANS
{

template <class PhysDim, class TopoDim>
class XField_Local;


template<class PhysDim>
class XField_Local< PhysDim, TopoD2> : public XField_Local_Base<PhysDim, TopoD2>
{

public:
  typedef XField_Local_Base<PhysDim, TopoD2> BaseType;

  XField_Local(mpi::communicator& comm_local, const XField_CellToTrace<PhysDim, TopoD2>& connectivity, int group, int elem);
  ~XField_Local(){};

  const XField_CellToTrace<PhysDim, TopoD2>& getConnectivity() const { return connectivity_; }

protected:
  using BaseType::reSolveBoundaryTraceGroups_;
  using BaseType::reSolveInteriorTraceGroups_;
  using BaseType::reSolveCellGroups_;
  const XField_CellToTrace<PhysDim, TopoD2>& connectivity_;
  const XField<PhysDim, TopoD2>& global_xfld_;
  BasisFunctionCategory basisCategory_;

  //DOF maps
  std::map<int,int> NodeDOFMap_;
  std::map<int,int> EdgeDOFMap_;
  std::map<int,int> CellDOFMap_;

  int cntNodeDOF_, cntEdgeDOF_, cntCellDOF_; //Counters for different DOF types
  int offset_cellDOF_, offset_edgeDOF_, offset_nodeDOF_; //Offset for the different DOF indices in the global array

  int nTriNeighbors_, nQuadNeighbors_, nNeighbors_;
  int nMainBTraceGroups_; //No of boundary traces (and groups) on main cell
  int nOuterBTraceGroups_; //No of boundary groups for outer btraces

  int OuterBTraceGroup_; //Group index for the outer boundary traces

  std::vector<int> maincell_reversed_traces_;
  std::vector<int> neighborcell_reversed_traces_;

  void extractLocalGrid(int group, int elem);

  template <class TopoMainCell>
  void extractMainCell(int main_group, int main_elem);

  void extractNeighbors(int main_group, int main_elem, int Ntrace, int order);

  template <class Topology>
  void trackDOFs(int group, int elem);

  template <class TopoCell>
  void setCellAssociativity(int group, int elem, ElementAssociativityConstructor<TopoD2, TopoCell>& DOFAssoc);

  void setInteriorTraceAssociativity(int group, int elem, ElementAssociativityConstructor<TopoD1, Line>& DOFAssoc, bool reverse_edge);
  void setBoundaryTraceAssociativity(const typename XField<PhysDim, TopoD2>::template FieldTraceGroupType<Line>& tracegrp,
                                     int elem, ElementAssociativityConstructor<TopoD1, Line>& DOFAssoc);

  void
  process_OuterBTraces(const int neighbor_group, const int neighbor_elem, const int order, const int Ntrace,
                       const int local_neighbor_group, const int local_neighbor_elem,
                       const CanonicalTraceToCell& neighbor_canonicalTrace,
                       std::shared_ptr<typename BaseType::template FieldTraceGroupType<Line>
                                                        ::FieldAssociativityConstructorType> fldAssoc_Outer_BTrace,
                       const int OuterBTraceGroup, int& cnt_outer_Btraces);
};

}

#endif // XFIELDAREA_LOCAL_H
