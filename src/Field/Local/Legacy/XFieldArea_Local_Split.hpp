// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELDAREA_LOCAL_SPLIT_H_
#define XFIELDAREA_LOCAL_SPLIT_H_

#include <vector>

#include "Topology/Dimension.h"

#include "Field/Element/ElementAssociativityAreaConstructor.h"

#include "XFieldArea_Local.h"
#include "XFieldArea_Local_Split_Linear.h"

#include "Field/Element/ElementProjection_L2.h"

/*
 * This class splits the main cell (triangle or quad) in the local mesh produced by XField_Local according to the given split type.
 * - The two/four sub-cells (main-cells) are in cell group 0
 * - All neighboring cells remain in cell group 1
 * - Each split introduces new interior traces (lines) within the original main-cell, and also within the original neighbors.
 * - These new main-main interfaces and neighbor-neighbor interfaces are put into interior trace groups of their own.
 * - The process first constructs a *linear* (P1) split-mesh and then uses the XField->buildFrom to add higher-order nodes if needed.
 */

/* Refer to the local solve documentation in Field/Documentation/Local_Solves/LocalSolves.pdf for more details and figures. */

namespace SANS
{

template <class PhysDim, class TopoDim>
class XField_Local_Split;

template<class PhysDim>
class XField_Local_Split< PhysDim, TopoD2> : public XField_Local_Base<PhysDim, TopoD2>
{

public:
  typedef XField_Local_Base<PhysDim, TopoD2> BaseType;

  XField_Local_Split(const XField_Local<PhysDim, TopoD2>& xfld_local, XField_CellToTrace<PhysDim,TopoD2>& connectivity,
                     ElementSplitType split_type, int split_edge_index);

  ~XField_Local_Split() {}

  const XField_CellToTrace<PhysDim, TopoD2>& getConnectivity() const { return xfld_local_unsplit_.getConnectivity(); }

protected:
  using BaseType::reSolveBoundaryTraceGroups_;
  using BaseType::reSolveInteriorTraceGroups_;
  using BaseType::reSolveCellGroups_;
  const XField_Local<PhysDim, TopoD2>& xfld_local_unsplit_;
  const XField_CellToTrace<PhysDim, TopoD2>& connectivity_;

  std::vector<int> orderVector_; //Basis order for each cell group in split-mesh

  int SplitTraceIndex_; //Canonical trace of main-cell which needs to be split
  bool IsotropicSplitFlag_; //Flag which indicates if the split is a isotropic split

  int CellGroupTriNeighbors_, CellGroupQuadNeighbors_; //Group index for triangle/quad cell groups

  void getOrder();

  void copyInfo(XField_Local_Split_Linear<PhysDim,TopoD2>& xfld_split_local_linear);

  void projectMainCell(ElementSplitType split_type, int split_edge_index);
  void projectNeighbors(int Ntrace, int order, ElementSplitType split_type, int split_edge_index);

  template<class TopoCell>
  void projectElem(int main_group, int main_elem, int sub_group, int sub_elem,
                   Element_Subdivision_Projector<TopoD2, TopoCell>& projector);

  template <class TopoCell>
  int getSplitEdgeIndexFromTrace(int trace);
};

}

#endif // XFIELDAREA_LOCAL_SPLIT_H
