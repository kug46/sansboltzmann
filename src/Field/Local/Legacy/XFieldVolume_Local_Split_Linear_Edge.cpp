// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XFieldVolume.h"
#include "XFieldVolume_Local_Split_Linear_Edge.h"

namespace SANS
{

template <>
void updateNodeDOFMaps_EdgeSplit<Tet>(const int new_nodeDOF, const int split_edge_index,
                                       int Cell0_nodeDOF_map[], int Cell1_nodeDOF_map[], const int nNodeDOF_Cell,
                                       int ITraceMid_nodeDOF_map[], const int nNodeDOF_Trace)
{
  SANS_ASSERT_MSG( nNodeDOF_Cell == Tet::NNode, "nNodeDOF_Cell (=%d) should equal 4.", nNodeDOF_Cell);
  SANS_ASSERT_MSG( nNodeDOF_Trace == Triangle::NNode, "nNodeDOF_Trace (=%d) should equal 3.", nNodeDOF_Trace);

  std::vector<int> nodelist(Tet::NNode + 1);
  for (int i=0; i<Tet::NNode; i++) nodelist[i] = Cell0_nodeDOF_map[i]; //Add nodes of original unsplit tet
  nodelist[Tet::NNode] = new_nodeDOF; //Add the new node on the split edge

  //Node indexing for each of the sub-cells for each of the 6 edge split configurations
  int node_order_cell0[6][4] = {{0,1,2,4},{0,4,2,3},{0,1,4,3},{4,1,2,3},{0,1,2,4},{0,4,2,3}};
  int node_order_cell1[6][4] = {{0,1,4,3},{0,1,2,4},{0,4,2,3},{0,1,4,3},{4,1,2,3},{4,1,2,3}};
  int node_order_ITrace_mid[6][3] = {{0,1,4},{0,2,4},{0,3,4},{4,1,3},{1,2,4},{4,2,3}};

  //Updating cell nodeDOF mappings to include the new DOF
  for (int i=0; i<nNodeDOF_Cell; i++)
  {
    Cell0_nodeDOF_map[i] = nodelist[node_order_cell0[split_edge_index][i]];
    Cell1_nodeDOF_map[i] = nodelist[node_order_cell1[split_edge_index][i]];
  }

  //Setting interior trace nodeDOF mappings
  for (int i=0; i<nNodeDOF_Trace; i++)
  {
    ITraceMid_nodeDOF_map[i] = nodelist[node_order_ITrace_mid[split_edge_index][i]];
  }
}

template <>
void updateNodeDOFMaps_EdgeSplit<Hex>(const int new_nodeDOF, const int split_edge_index,
                                       int Cell0_nodeDOF_map[], int Cell1_nodeDOF_map[], const int nNodeDOF_Cell,
                                       int ITraceMid_nodeDOF_map[], const int nNodeDOF_Trace)
{
  SANS_DEVELOPER_EXCEPTION( "XField_Split_Local_Linear<PhysDim, TopoD3, T>::updateNodeDOFMaps_TraceSplit - Hex splitting not supported yet.");
}

template <>
std::vector<int>
getSplitTracesFromSplitEdge<Tet>(const int split_edge_index)
{
  //List of 2 canonical traces that need to be split for each edge-split configuration
  int faces[2][6] = {{0,0,0,1,1,2},
                     {1,2,3,3,2,3}};

  return {faces[0][split_edge_index], faces[1][split_edge_index]};
}

template <>
std::vector<int>
getSplitTracesFromSplitEdge<Hex>(const int split_edge_index)
{
  SANS_DEVELOPER_EXCEPTION( "XField_Split_Local_Linear<PhysDim, TopoD3, T>::getSplitFacesFromSplitEdge<Hex> - Hex splitting not supported yet.");
  return {-1};
}

template <>
std::vector<int>
getMainSubCellElements<Tet>(const int trace_index, const int split_edge_index)
{

  if (split_edge_index==0)
  {
    if (trace_index==0)
      return {0,1};
    else if (trace_index==1)
      return {0,1};
    else if (trace_index==2)
      return {1};
    else if (trace_index==3)
      return {0};
  }
  else if (split_edge_index==1)
  {
    if (trace_index==0)
      return {0,1};
    else if (trace_index==1)
      return {0};
    else if (trace_index==2)
      return {0,1};
    else if (trace_index==3)
      return {1};
  }
  else if (split_edge_index==2)
  {
    if (trace_index==0)
      return {0,1};
    else if (trace_index==1)
      return {1};
    else if (trace_index==2)
      return {0};
    else if (trace_index==3)
      return {0,1};
  }
  else if (split_edge_index==3)
  {
    if (trace_index==0)
      return {0};
    else if (trace_index==1)
      return {0,1};
    else if (trace_index==2)
      return {1};
    else if (trace_index==3)
      return {0,1};
  }
  else if (split_edge_index==4)
  {
    if (trace_index==0)
      return {1};
    else if (trace_index==1)
      return {0,1};
    else if (trace_index==2)
      return {0,1};
    else if (trace_index==3)
      return {0};
  }
  else if (split_edge_index==5)
  {
    if (trace_index==0)
      return {1};
    else if (trace_index==1)
      return {0};
    else if (trace_index==2)
      return {0,1};
    else if (trace_index==3)
      return {0,1};
  }

  SANS_DEVELOPER_EXCEPTION("XField_Local_Split_Linear<PhysDim, TopoD3, T>::getMainSubCellElements<Tet> - "
                           "Invalid trace_index (=%d) for split configuration!", trace_index);
  return {-1};
}

template <>
std::vector<int>
getMainSubCellElements<Hex>(const int trace_index, const int split_edge_index)
{
  SANS_DEVELOPER_EXCEPTION( "XField_Split_Local_Linear<PhysDim, TopoD3, T>::getMainSubCellElements<Hex> - Hex splitting not supported yet.");
  return {-1};
}

template <class PhysDim>
void
XField_Local_Split_Linear_Edge<PhysDim, TopoD3>::split(ElementSplitType split_type, int split_edge_index)
{
  //Clear current local grid
  this->deallocate();

  NodeDOFMap_.clear();
  this->newNodeDOFs_.clear();

  cntNodeDOF_ = 0;
  OuterBTraceGroup_Tri_ = -1;
  OuterBTraceGroup_Quad_ = -1;
  SplitEdgeNodeMap_[0] = -1;
  SplitEdgeNodeMap_[1] = -1;

  //Classify the split type
  if (split_type == ElementSplitType::Edge)
  {
    SplitEdgeIndex_ = split_edge_index;
  }
  else
    SANS_DEVELOPER_EXCEPTION("XField_Split_Local<PhysDim, TopoD3, T>::split - Invalid ElementSplitType. Expected an Edge split.");


  if ( xfld_local_unsplit_.getCellGroupBase(mainGroup_).topoTypeID() == typeid(Tet) )
  {
    //Checks
    if (SplitEdgeIndex_ >= 0 && SplitEdgeIndex_ < Tet::NEdge)
    {
      splitMainCell<Tet, Triangle>();
    }
    else
      SANS_DEVELOPER_EXCEPTION("XField_Split_Local<PhysDim, TopoD3, T>::split - "
                               "Invalid split_type and split_edge_index (=%d) combination.", split_edge_index);
  }
  else if ( xfld_local_unsplit_.getCellGroupBase(mainGroup_).topoTypeID() == typeid(Hex) )
  {
    //Checks
    if (SplitEdgeIndex_ >= 0 && SplitEdgeIndex_ < Hex::NEdge)
    {
      SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD3, T>::splitMainCell - Hex splitting not supported yet." );
//      splitMainCell<Hex, Quad>();
    }
    else
      SANS_DEVELOPER_EXCEPTION("XField_Split_Local<PhysDim, TopoD3, T>::split - "
                               "Invalid split_type and split_edge_index (=%d) combination.", split_edge_index);
  }
  else
    SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD3, T>::split - Unknown cell topology." );

  // This is checkGrid() but gets removed in release mode!
  this->checkGrid();
}


template <class PhysDim>
template <class TopoMainCell, class TopoMainTrace>
void
XField_Local_Split_Linear_Edge<PhysDim, TopoD3>::splitMainCell()
{

  typedef typename XField<PhysDim, TopoD3>::template FieldCellGroupType<TopoMainCell> XFieldCellGroupType;
  typedef typename XField<PhysDim, TopoD3>::template FieldTraceGroupType<TopoMainTrace> XFieldTraceGroupType;
  typedef typename XFieldCellGroupType::BasisType BasisType_Cell;
  typedef typename XFieldTraceGroupType::BasisType BasisType_Trace;

  std::pair<std::map<int,int>::iterator,bool> map_ret; //return value for map inserts

  const XFieldCellGroupType& cellgrp_main = xfld_local_unsplit_.template getCellGroup<TopoMainCell>(mainGroup_);

  //Get nodeDOF map from unsplit mesh
  int nodeDOF_map[TopoMainCell::NNode];
  cellgrp_main.associativity(mainElem_).getNodeGlobalMapping( nodeDOF_map, TopoMainCell::NNode );

  int order = 1; //We're constructing a linear split mesh
  const BasisType_Cell* cell_basis = BasisFunctionVolumeBase<TopoMainCell>::getBasisFunction(order, basisCategory_);
  const BasisType_Trace* trace_basis = BasisFunctionAreaBase<TopoMainTrace>::getBasisFunction(order, basisCategory_);

  nTetNeighbors_ = 0;
  nHexNeighbors_ = 0;
  nNeighbors_ = 0;

  nSplitTetNeighbors_ = 0;
  nSplitHexNeighbors_ = 0;

  //Count and map required DOFs from global to local mesh
  trackDOFs<TopoMainCell>(mainGroup_, mainElem_);

  if ( xfld_local_unsplit_.getCellGroupBase(mainGroup_).topoTypeID() == typeid(Tet) )
  {
    //Edge split
    //Add new nodeDOF to split the main cell
    map_ret = NodeDOFMap_.insert( std::pair<int,int>(-1,cntNodeDOF_) );
    if (map_ret.second==true)
    {
      this->newNodeDOFs_.push_back(cntNodeDOF_);
      cntNodeDOF_++; //increment counter if object was actually added
    }

    //Find the nodeDOFs of the split-edge and save for later use
    SplitEdgeNodeMap_[0] = nodeDOF_map[TetEdgeToNodeMap_[SplitEdgeIndex_][0]];
    SplitEdgeNodeMap_[1] = nodeDOF_map[TetEdgeToNodeMap_[SplitEdgeIndex_][1]];
  }
  else if ( xfld_local_unsplit_.getCellGroupBase(mainGroup_).topoTypeID() == typeid(Hex) )
  {
    SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD3, T>::splitMainCell - Hex splitting not supported yet." );
  }
  else
    SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD3, T>::splitMainCell - Unknown cell topology for neighbor." );


  //Initial loop across neighboring elements to find number of cells/traces/DOFs
  for (int i = 0; i < TopoMainCell::NTrace; i++)
  {
    //Get indices of each neighboring cell (and its cellgroup)
    const TraceInfo& traceinfo = connectivity_.getTrace(mainGroup_, mainElem_, i);

    if (traceinfo.type == TraceInfo::Interior)
    {
      const XFieldTraceGroupType& tracegrp = xfld_local_unsplit_.template getInteriorTraceGroup<TopoMainTrace>(traceinfo.group);
      int neighbor_group = -1; //cellgroup of neighboring element
      int neighbor_elem = -1; //element index of neighboring element

      if (tracegrp.getElementLeft(traceinfo.elem) == mainElem_)
      {
        neighbor_group = tracegrp.getGroupRight(); //getGroupRight gives the neighbor's cellgroup
        neighbor_elem = tracegrp.getElementRight(traceinfo.elem);
      }
      else
        SANS_DEVELOPER_EXCEPTION("XField_Split_Local<PhysDim, TopoD3, T>::splitMainCell - "
                                 "The main-cell is not to the left of its neighbors in the unsplit local mesh.");

      if ( xfld_local_unsplit_.getCellGroupBase(neighbor_group).topoTypeID() == typeid(Tet) )
      {
        //Count and map required DOFs from global to local mesh
        trackDOFs<Tet>(neighbor_group, neighbor_elem);
        nTetNeighbors_++; //Increment tet neighbor count

        for (int edge=0; edge < Triangle::NTrace; edge++)
        {
          if (TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceEdges[i][edge] == SplitEdgeIndex_)
          {
            nTetNeighbors_++; //The neighbor tet will be split into 2 tets (i.e. 1 new tet)
            nSplitTetNeighbors_++;
          }
        }
      }
      else if ( xfld_local_unsplit_.getCellGroupBase(neighbor_group).topoTypeID() == typeid(Hex) )
      {
        //Count and map required DOFs from global to local mesh
        trackDOFs<Hex>(neighbor_group, neighbor_elem);
        nHexNeighbors_++; //Increment hex neighbor count
      }
      else
        SANS_DEVELOPER_EXCEPTION( "XField_Local<PhysDim, TopoD3, T>::extractLocalGrid - Unknown cell topology for neighbor." );
    }

  } // initial loop across traces of main cell


  //Count DOFs in local mesh
  int nDOF_local = (int)NodeDOFMap_.size();

#if 0
  std::cout << "nDOF_local: " << nDOF_local << std::endl;
  std::cout << "mapNode: " << NodeDOFMap_.size() <<std::endl;
  std::cout << "nNTetNeighbors: " << nTetNeighbors_ <<std::endl;
  std::cout << "nNHexNeighbors: " << nHexNeighbors_ <<std::endl;
#endif

  //Create local XField
  this->resizeDOF(nDOF_local);

  //Add up neighbor cell counts
  nNeighbors_ = nTetNeighbors_ + nHexNeighbors_;

  if (nNeighbors_>0)
  {
    if (nTetNeighbors_>0 && nHexNeighbors_==0) //Only tet neighbors
    {
      this->resizeCellGroups(2); //2 CellGroups - one for main cell, and one for all neighboring tets

      if (nSplitTetNeighbors_ > 0)
        this->resizeInteriorTraceGroups(3); //three groups :(main-to-tet-neighbor, main-main and tet-neighbor-neighbor interfaces)
      else
        this->resizeInteriorTraceGroups(2); //two groups :(main-to-tet-neighbor, main-main)
    }
    else if (nTetNeighbors_==0 && nHexNeighbors_>0) //Only hex neighbors
    {
      this->resizeCellGroups(2); //2 CellGroups - one for main cell, and one for all neighboring hexes

      if (nSplitHexNeighbors_ > 0)
        this->resizeInteriorTraceGroups(3); //three groups :(main-to-hex-neighbor, main-main and hex-neighbor-neighbor interfaces)
      else
        this->resizeInteriorTraceGroups(2); //two groups :(main-to-hex-neighbor, main-main)
    }
    else //Both kinds of neighbors
    {
      SANS_DEVELOPER_EXCEPTION( "XField_Local<PhysDim, TopoD3, T>::extractLocalGrid - Mixed meshes not supported yet." );
    }
  }
  else //No neighboring cells
  {
    this->resizeCellGroups(1); // one group for main cell
    this->resizeInteriorTraceGroups(1); //one group for interfaces between main cells
  }

  //a group each for each BC of main cell + one group for outer boundaries of neighboring cells
  nMainBTraceGroups_ = TopoMainCell::NTrace - (nNeighbors_ - nSplitTetNeighbors_ - nSplitHexNeighbors_);
  nOuterBTraceGroups_ = 1;
  if (nTetNeighbors_ > 0 && nHexNeighbors_ > 0)
    nOuterBTraceGroups_ = 2; //If this is a mixed mesh, we need 2 outer Btrace groups (for triangles/quads)

  if (nNeighbors_ == 0) nOuterBTraceGroups_ = 0; //If there are no neighbors - there is no outer boundary of the neighboring cells
  this->resizeBoundaryTraceGroups(nMainBTraceGroups_);
  this->resizeGhostBoundaryTraceGroups(nOuterBTraceGroups_);

  if (nOuterBTraceGroups_ > 0)
  {
    OuterBTraceGroup_Tri_ = 0;
    OuterBTraceGroup_Quad_ = 1;

    if (nTetNeighbors_==0) //Promote quad btraces if there are no tet neighbors
      OuterBTraceGroup_Quad_--;
  }

  ITraceGroupInterMain_ = 1;

  //Zero out mesh DOFs
  for (int k = 0; k < this->nDOF_; k++)
    this->DOF_[k] = 0.0;

  //Offsets for node DOFs in the global array
  //(cell DOFs first, face DOFs second, edge DOFs third, node DOFs last)
  offset_nodeDOF_ = 0;

  //-------------------------------------------------------------------------------------------------------------
  //Copy and fill associativities of neighboring cells, interior traces and boundary traces
  splitNeighbors(TopoMainCell::NTrace, order);

  //------------------------------Fill in associativity for main cells--------------------------------------------

  int nMainCells = 2;  //number of main sub-cells
  int nInterMainTraces = 1; //number of interior traces between main sub-cells

  // create field associativity constructor for main cell
  typename XFieldCellGroupType::FieldAssociativityConstructorType fldAssocCell_Main( cell_basis, nMainCells );

  ElementAssociativityConstructor<TopoD3, TopoMainCell> DOFAssoc_MainCell0( cell_basis );
  ElementAssociativityConstructor<TopoD3, TopoMainCell> DOFAssoc_MainCell1( cell_basis );

  // DOF associativity for interior traces
  ElementAssociativityConstructor<TopoD2, TopoMainTrace> DOFAssoc_ITraceMid( trace_basis );

  CanonicalTraceToCell ITraceMid_canonicalL, ITraceMid_canonicalR;

  int new_nodeDOF = -1;

  setMainCellAssociativity_EdgeSplit<TopoMainCell>(new_nodeDOF, DOFAssoc_MainCell0, DOFAssoc_MainCell1, DOFAssoc_ITraceMid,
                                                    ITraceMid_canonicalL, ITraceMid_canonicalR);

  fldAssocCell_Main.setAssociativity( DOFAssoc_MainCell0, 0 );
  fldAssocCell_Main.setAssociativity( DOFAssoc_MainCell1, 1 );

  //Add cell group and copy DOFs for main cell
  this->cellGroups_[0] = new XFieldCellGroupType(fldAssocCell_Main);
  this->cellGroups_[0]->setDOF( this->DOF_, this->nDOF_ );
  this->nElem_ += fldAssocCell_Main.nElem();

  //Both left and right main cells in split mesh map to the same element in the global mesh
  this->CellMapping_[{0,0}] = xfld_local_unsplit_.getGlobalCellMap({0,0});
  this->CellMapping_[{0,1}] = xfld_local_unsplit_.getGlobalCellMap({0,0});

  //Save split configurations (needed for solution transfer)
  this->CellSplitInfo_[{0,0}] = ElementSplitInfo(ElementSplitFlag::Split, ElementSplitType::Edge, SplitEdgeIndex_, 0);
  this->CellSplitInfo_[{0,1}] = ElementSplitInfo(ElementSplitFlag::Split, ElementSplitType::Edge, SplitEdgeIndex_, 1);


  // create field associativity constructor for interior traces between main sub-cells
  std::shared_ptr<typename XFieldTraceGroupType::FieldAssociativityConstructorType> fldAssoc_ITrace_InterMain; //main-to-main

  fldAssoc_ITrace_InterMain = std::make_shared< typename XFieldTraceGroupType::FieldAssociativityConstructorType>
                                             ( trace_basis, nInterMainTraces);

  const int itrace_mid_index = 0;
  fldAssoc_ITrace_InterMain->setAssociativity( DOFAssoc_ITraceMid, itrace_mid_index );

  fldAssoc_ITrace_InterMain->setGroupLeft(mainGroup_);
  fldAssoc_ITrace_InterMain->setGroupRight(mainGroup_);
  fldAssoc_ITrace_InterMain->setElementLeft(0, itrace_mid_index);
  fldAssoc_ITrace_InterMain->setElementRight(1, itrace_mid_index);
  fldAssoc_ITrace_InterMain->setCanonicalTraceLeft(ITraceMid_canonicalL, itrace_mid_index);
  fldAssoc_ITrace_InterMain->setCanonicalTraceRight(ITraceMid_canonicalR, itrace_mid_index);

  this->InteriorTraceSplitInfo_[{ITraceGroupInterMain_,itrace_mid_index}] = ElementSplitInfo(ElementSplitFlag::New, itrace_mid_index);

  //Add trace group and set DOFs for interior traces
  this->interiorTraceGroups_[ITraceGroupInterMain_] = new XFieldTraceGroupType( *fldAssoc_ITrace_InterMain );
  this->interiorTraceGroups_[ITraceGroupInterMain_]->setDOF( this->DOF_, this->nDOF_ );

}


template <class PhysDim>
void
XField_Local_Split_Linear_Edge<PhysDim, TopoD3>::splitNeighbors(int Ntrace, int order)
{
  //Volumes
  typedef typename XField<PhysDim, TopoD3>::template FieldCellGroupType<Tet> XFieldCellGroupType_Tet;
  typedef typename XField<PhysDim, TopoD3>::template FieldCellGroupType<Hex> XFieldCellGroupType_Hex;
  typedef typename XFieldCellGroupType_Tet::BasisType CellBasisType_Tet;
  typedef typename XFieldCellGroupType_Hex::BasisType CellBasisType_Hex;

  //Faces
  typedef typename XField<PhysDim, TopoD3>::template FieldTraceGroupType<Triangle> XFieldTraceGroupType_Tri;
  typedef typename XField<PhysDim, TopoD3>::template FieldTraceGroupType<Quad> XFieldTraceGroupType_Quad;
  typedef typename XFieldTraceGroupType_Tri::BasisType TraceBasisType_Tri;
  typedef typename XFieldTraceGroupType_Quad::BasisType TraceBasisType_Quad;

  const CellBasisType_Tet* cell_basis_Tet = NULL;
  const CellBasisType_Hex* cell_basis_Hex = NULL;

  const TraceBasisType_Tri* trace_basis_Tri = NULL;
  const TraceBasisType_Quad* trace_basis_Quad = NULL;

  if (nTetNeighbors_>0)
  {
    cell_basis_Tet = BasisFunctionVolumeBase<Tet>::getBasisFunction(order, basisCategory_);
    trace_basis_Tri = BasisFunctionAreaBase<Triangle>::getBasisFunction(order, basisCategory_);
  }
  if (nHexNeighbors_>0)
  {
    cell_basis_Hex = BasisFunctionVolumeBase<Hex>::getBasisFunction(order, basisCategory_);
    trace_basis_Quad = BasisFunctionAreaBase<Quad>::getBasisFunction(order, basisCategory_);
  }

  // create field associativity constructor for neighboring cells
  std::shared_ptr<typename XFieldCellGroupType_Tet::FieldAssociativityConstructorType> fldAssocCell_Neighbors_Tet; //tet neighbors
  std::shared_ptr<typename XFieldCellGroupType_Hex::FieldAssociativityConstructorType> fldAssocCell_Neighbors_Hex; //hex neighbors

  // create field associativity constructor for interior traces
  std::shared_ptr<typename XFieldTraceGroupType_Tri::FieldAssociativityConstructorType> fldAssoc_ITrace_Tri;
  std::shared_ptr<typename XFieldTraceGroupType_Quad::FieldAssociativityConstructorType> fldAssoc_ITrace_Quad;
  std::shared_ptr<typename XFieldTraceGroupType_Tri::FieldAssociativityConstructorType> fldAssoc_ITrace_InterTetNeighbor; //tet neighbor-to-neighbor
  std::shared_ptr<typename XFieldTraceGroupType_Quad::FieldAssociativityConstructorType> fldAssoc_ITrace_InterHexNeighbor; //hex neighbor-neighbor


  // create field associativity constructor for outer boundary traces of neighboring cells (3 faces for each neighboring tet, 5 for each quad)
  std::shared_ptr<typename XFieldTraceGroupType_Tri::FieldAssociativityConstructorType> fldAssoc_Outer_BTrace_Tri;
  std::shared_ptr<typename XFieldTraceGroupType_Quad::FieldAssociativityConstructorType> fldAssoc_Outer_BTrace_Quad;


  //Assign cell group indices to neighbors (index 0 is for main cell) in local mesh
  CellGroupTetNeighbors_ = 1;
  CellGroupHexNeighbors_ = 2;

  int ITraceGroupTetNeighbors = 0;
  int ITraceGroupHexNeighbors = 1;
  ITraceGroupInterMain_ = 2; //interfaces between split main-cells
  int ITraceGroupInterTetNeighbor = 3; //interfaces between split tet neighbor-cells
  int ITraceGroupInterHexNeighbor = 4; //interfaces between split hex neighbor-cells

  if (nNeighbors_ > 0)
  {
    fldAssocCell_Neighbors_Tet  = std::make_shared< typename XFieldCellGroupType_Tet::FieldAssociativityConstructorType>
                                                 ( cell_basis_Tet, nTetNeighbors_ );

    fldAssocCell_Neighbors_Hex  = std::make_shared< typename XFieldCellGroupType_Hex::FieldAssociativityConstructorType>
                                                 ( cell_basis_Hex, nHexNeighbors_ );

    fldAssoc_ITrace_Tri         = std::make_shared< typename XFieldTraceGroupType_Tri::FieldAssociativityConstructorType>
                                                 ( trace_basis_Tri, nTetNeighbors_);

    fldAssoc_ITrace_Quad        = std::make_shared< typename XFieldTraceGroupType_Quad::FieldAssociativityConstructorType>
                                                 ( trace_basis_Quad, nHexNeighbors_);

    fldAssoc_ITrace_InterTetNeighbor  = std::make_shared< typename XFieldTraceGroupType_Tri::FieldAssociativityConstructorType>
                                                 ( trace_basis_Tri, nSplitTetNeighbors_);

    fldAssoc_ITrace_InterHexNeighbor = std::make_shared< typename XFieldTraceGroupType_Quad::FieldAssociativityConstructorType>
                                                 ( trace_basis_Quad, nSplitHexNeighbors_);

    fldAssoc_Outer_BTrace_Tri   = std::make_shared< typename XFieldTraceGroupType_Tri::FieldAssociativityConstructorType>
                                                 ( trace_basis_Tri, (Tet::NTrace - 1)*nTetNeighbors_ - 2*nSplitTetNeighbors_);

    fldAssoc_Outer_BTrace_Quad  = std::make_shared< typename XFieldTraceGroupType_Quad::FieldAssociativityConstructorType>
                                                 ( trace_basis_Quad, (Hex::NTrace - 1)*nHexNeighbors_ - 2*nSplitHexNeighbors_);

    //if no tet neighbors, then promote the hex neighbor cellgroup and the interior trace group indices
    if (nTetNeighbors_==0)
    {
      CellGroupTetNeighbors_ = -1;
      ITraceGroupTetNeighbors = -1;
      ITraceGroupInterTetNeighbor = -1;

      CellGroupHexNeighbors_--;
      ITraceGroupHexNeighbors--;
      ITraceGroupInterMain_--;
      ITraceGroupInterHexNeighbor -= 2;
    }

    //if no hex neighbors, then promote the interior trace group indices
    if (nHexNeighbors_==0)
    {
      CellGroupHexNeighbors_ = -1;
      ITraceGroupHexNeighbors = -1;
      ITraceGroupInterHexNeighbor = -1;

      ITraceGroupInterMain_--;
      ITraceGroupInterTetNeighbor--;
    }

  }
  else //no neighbors
  {
    CellGroupTetNeighbors_ = -1;
    ITraceGroupTetNeighbors = -1;
    ITraceGroupInterTetNeighbor = -1;
    CellGroupHexNeighbors_ = -1;
    ITraceGroupHexNeighbors = -1;
    ITraceGroupInterHexNeighbor = -1;

    ITraceGroupInterMain_ = 0;
  }

  //counters to keep track of the neighbor cells which have been processed (includes splits)
  int cnt_neighbor_tet = 0;
//  int cnt_neighbor_hex = 0;

  //counters to keep track of how many neighbor cells have been split
  int cnt_neighbor_tet_split = 0;
//  int cnt_neighbor_hex_split = 0;

  int cnt_main_Btraces = 0; //counter to keep track of the boundary traces of the main cell which have been processed

  //counters to keep track of the outer boundary traces which have been processed (includes splits)
  int cnt_outer_Btraces_tri = 0;
//  int cnt_outer_Btraces_quad = 0;

  //counters to keep track of how many outer boundary traces have been split
  int cnt_outer_Btraces_tri_split = 0;
//  int cnt_outer_Btraces_quad_split = 0;

  //-----------Fill in associativity for neighboring cells--------------------

  //Repeat loop across neighboring elements to fill associativity
  for (int itrace = 0; itrace < Ntrace; itrace++)
  {
    //Get indices of each neighboring trace (and its cellgroup)
    const TraceInfo& traceinfo = connectivity_.getTrace(mainGroup_, mainElem_, itrace);

    if (traceinfo.type == TraceInfo::Interior) //Ensure that neighboring cell exists: getTraceGroup returns negative if trace is a boundaryTrace
    {
      int neighbor_group = -1; //cellgroup of neighboring element in global mesh
      int neighbor_elem = -1; //element index of neighboring element in global mesh
      CanonicalTraceToCell neighbor_canonicalTrace; //canonicalTrace of the neighboring cell for the current trace
      CanonicalTraceToCell main_canonicalTrace; //canonicalTrace of the main cell for the current trace

      if (xfld_local_unsplit_.getInteriorTraceGroupBase(traceinfo.group).topoTypeID() == typeid(Triangle))
      {
        getNeighborInfoFromTrace<Triangle>(traceinfo.group, traceinfo.elem, mainElem_,
                                           neighbor_group, neighbor_elem,
                                           neighbor_canonicalTrace, main_canonicalTrace);
      }
      else if (xfld_local_unsplit_.getInteriorTraceGroupBase(traceinfo.group).topoTypeID() == typeid(Quad))
      {
        getNeighborInfoFromTrace<Quad>(traceinfo.group, traceinfo.elem, mainElem_,
                                       neighbor_group, neighbor_elem,
                                       neighbor_canonicalTrace, main_canonicalTrace);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "XField_Local<PhysDim, TopoD3, T>::extractLocalGrid - Unknown trace topology for interior trace." );


      if ( xfld_local_unsplit_.getCellGroupBase(neighbor_group).topoTypeID() == typeid(Tet) )
      {
        //Edge-split
        int new_nodeDOF = -1;

        //Get the list of traces that are affected from this edge-split
        std::vector<int> split_trace_list = getSplitTracesFromSplitEdge<Tet>(SplitEdgeIndex_);

        bool split_neighbor_flag = false;

        for (int i=0; i<(int) split_trace_list.size(); i++)
        {
          if (itrace==split_trace_list[i])
            split_neighbor_flag = true;
        }

        if (split_neighbor_flag)
        { //Split neighbor

          ElementAssociativityConstructor<TopoD3, Tet> DOFAssoc_NeighborSubCell0( cell_basis_Tet );
          ElementAssociativityConstructor<TopoD3, Tet> DOFAssoc_NeighborSubCell1( cell_basis_Tet );

          // DOF associativity for interior traces
          ElementAssociativityConstructor<TopoD2, Triangle> DOFAssoc_ITrace0( trace_basis_Tri );
          ElementAssociativityConstructor<TopoD2, Triangle> DOFAssoc_ITrace1( trace_basis_Tri );
          ElementAssociativityConstructor<TopoD2, Triangle> DOFAssoc_ITraceMid( trace_basis_Tri );
          CanonicalTraceToCell ITraceMid_canonicalL, ITraceMid_canonicalR;

          int Itrace_split_edge_index = -1, Itrace_split_subcell_index[2]; //Variables to store split information of interior traces

          setCellAssociativity_EdgeSplit<Tet,Triangle>(neighbor_group, neighbor_elem,
                                                       traceinfo.group, traceinfo.elem, new_nodeDOF,
                                                       DOFAssoc_NeighborSubCell0, DOFAssoc_NeighborSubCell1,
                                                       DOFAssoc_ITrace0, DOFAssoc_ITrace1, DOFAssoc_ITraceMid,
                                                       ITraceMid_canonicalL, ITraceMid_canonicalR,
                                                       Itrace_split_edge_index, Itrace_split_subcell_index);

          fldAssocCell_Neighbors_Tet->setAssociativity( DOFAssoc_NeighborSubCell0, cnt_neighbor_tet   );
          fldAssocCell_Neighbors_Tet->setAssociativity( DOFAssoc_NeighborSubCell1, cnt_neighbor_tet+1 );

          //Both left and right main cells in split mesh map to the same element in the global mesh
          this->CellMapping_[{CellGroupTetNeighbors_,cnt_neighbor_tet}] =
                xfld_local_unsplit_.getGlobalCellMap({CellGroupTetNeighbors_,(cnt_neighbor_tet - cnt_neighbor_tet_split)});

          this->CellMapping_[{CellGroupTetNeighbors_,cnt_neighbor_tet+1}] =
                xfld_local_unsplit_.getGlobalCellMap({CellGroupTetNeighbors_,(cnt_neighbor_tet - cnt_neighbor_tet_split)});

          //Find the split configurations for the neighbor sub-cells
          int neighbor_split_edge_index = getSplitEdgeIndexFromEdgeNodes<Tet>(SplitEdgeNodeMap_[0], SplitEdgeNodeMap_[1],
                                                                              neighbor_group, neighbor_elem);

          int sub_cell_index_0 = 0;
          int sub_cell_index_1 = 1;

          if (neighbor_split_edge_index < 0)
          {
            //Reverse sub-cell indices if the split-edge is oriented in the reverse direction.
            sub_cell_index_0 = 1;
            sub_cell_index_1 = 0;
            neighbor_split_edge_index = -neighbor_split_edge_index;
          }

          //Save split configurations (needed for solution transfer)
          this->CellSplitInfo_[{CellGroupTetNeighbors_,cnt_neighbor_tet}] =
              ElementSplitInfo(ElementSplitFlag::Split, ElementSplitType::Edge, neighbor_split_edge_index-1, sub_cell_index_0);

          this->CellSplitInfo_[{CellGroupTetNeighbors_,cnt_neighbor_tet+1}] =
              ElementSplitInfo(ElementSplitFlag::Split, ElementSplitType::Edge, neighbor_split_edge_index-1, sub_cell_index_1);


          //Add associativity for outer boundary traces of this neighbor cell
          process_OuterBTraces_EdgeSplit<Tet,Triangle>(neighbor_group, neighbor_elem, order, Tet::NTrace, new_nodeDOF,
                                                       CellGroupTetNeighbors_, cnt_neighbor_tet, neighbor_canonicalTrace,
                                                       fldAssoc_Outer_BTrace_Tri, OuterBTraceGroup_Tri_,
                                                       cnt_outer_Btraces_tri, cnt_outer_Btraces_tri_split);


          //Set DOF associativity for interior traces between main-cell and neighbor-cells

          int itrace_index0 = cnt_neighbor_tet; //The new index for this interior trace is simply the number of neighboring cells passed
          int itrace_index1 = cnt_neighbor_tet + 1;

          fldAssoc_ITrace_Tri->setAssociativity( DOFAssoc_ITrace0, itrace_index0 );
          fldAssoc_ITrace_Tri->setAssociativity( DOFAssoc_ITrace1, itrace_index1 );

          //Save this interior trace's mapping from local to global mesh
          this->InteriorTraceMapping_[{ITraceGroupTetNeighbors,itrace_index0}] =
               xfld_local_unsplit_.getGlobalInteriorTraceMap({ITraceGroupTetNeighbors,itrace_index0 - cnt_neighbor_tet_split});

          this->InteriorTraceMapping_[{ITraceGroupTetNeighbors,itrace_index1}] =
               xfld_local_unsplit_.getGlobalInteriorTraceMap({ITraceGroupTetNeighbors,itrace_index0 - cnt_neighbor_tet_split});

          this->InteriorTraceSplitInfo_[{ITraceGroupTetNeighbors,itrace_index0}] =
              ElementSplitInfo(ElementSplitFlag::Split, ElementSplitType::Edge, Itrace_split_edge_index, Itrace_split_subcell_index[0]);

          this->InteriorTraceSplitInfo_[{ITraceGroupTetNeighbors,itrace_index1}] =
              ElementSplitInfo(ElementSplitFlag::Split, ElementSplitType::Edge, Itrace_split_edge_index, Itrace_split_subcell_index[1]);


          //Get the indices of the main sub-cells on the left based on split-configuration and trace index
          std::vector<int> main_subelemlist = getMainSubCellElements<Tet>(itrace, SplitEdgeIndex_);

          fldAssoc_ITrace_Tri->setGroupLeft(mainGroup_); //Cell group on left is the main cell's group = 0
          fldAssoc_ITrace_Tri->setElementLeft(main_subelemlist[0], itrace_index0);
          fldAssoc_ITrace_Tri->setElementLeft(main_subelemlist[1], itrace_index1);
          fldAssoc_ITrace_Tri->setCanonicalTraceLeft(main_canonicalTrace, itrace_index0);
          fldAssoc_ITrace_Tri->setCanonicalTraceLeft(main_canonicalTrace, itrace_index1);

          fldAssoc_ITrace_Tri->setGroupRight(CellGroupTetNeighbors_); //Cell group on right is the triangle neighbors' cell group
          fldAssoc_ITrace_Tri->setElementRight(itrace_index0, itrace_index0); //Right element for interior trace i is equal to i
          fldAssoc_ITrace_Tri->setElementRight(itrace_index1, itrace_index1); //Right element for interior trace i is equal to i
          fldAssoc_ITrace_Tri->setCanonicalTraceRight(neighbor_canonicalTrace, itrace_index0);
          fldAssoc_ITrace_Tri->setCanonicalTraceRight(neighbor_canonicalTrace, itrace_index1);


          //Set DOF associativity for interior traces between neighbor-cells
          int itrace_mid_index = cnt_neighbor_tet_split; //itrace_mid_index is the number of neighbor tets split so far

          fldAssoc_ITrace_InterTetNeighbor->setAssociativity( DOFAssoc_ITraceMid, itrace_mid_index );

          fldAssoc_ITrace_InterTetNeighbor->setGroupLeft(CellGroupTetNeighbors_);
          fldAssoc_ITrace_InterTetNeighbor->setGroupRight(CellGroupTetNeighbors_);
          fldAssoc_ITrace_InterTetNeighbor->setElementLeft(itrace_index0, itrace_mid_index);
          fldAssoc_ITrace_InterTetNeighbor->setElementRight(itrace_index1, itrace_mid_index);
          fldAssoc_ITrace_InterTetNeighbor->setCanonicalTraceLeft(ITraceMid_canonicalL, itrace_mid_index);
          fldAssoc_ITrace_InterTetNeighbor->setCanonicalTraceRight(ITraceMid_canonicalR, itrace_mid_index);

          const int sub_trace_index = 0;
          this->InteriorTraceSplitInfo_[{ITraceGroupInterTetNeighbor,itrace_mid_index}] = ElementSplitInfo(ElementSplitFlag::New, sub_trace_index);

          cnt_neighbor_tet += 2; //Added two (split)-neighbors
          cnt_neighbor_tet_split ++; //increment tet-neighbor split count

        }
        else
        { //Original neighbor (just copy from unsplit mesh)

          // DOF associativity for neighbor cells
          ElementAssociativityConstructor<TopoD3, Tet> DOFAssoc_NeighborCell( cell_basis_Tet );

          setCellAssociativity(neighbor_group, neighbor_elem, DOFAssoc_NeighborCell);

          //Save this neighboring cell's mapping from local to global mesh
          this->CellMapping_[{CellGroupTetNeighbors_,cnt_neighbor_tet}] =
                xfld_local_unsplit_.getGlobalCellMap({CellGroupTetNeighbors_,(cnt_neighbor_tet - cnt_neighbor_tet_split)});

          this->CellSplitInfo_[{CellGroupTetNeighbors_,cnt_neighbor_tet}] = ElementSplitInfo(ElementSplitFlag::Unsplit);


          //Add associativity for outer boundary traces of this neighbor cell
          process_OuterBTraces<Tet,Triangle>(neighbor_group, neighbor_elem, order, Tet::NTrace, CellGroupTetNeighbors_, cnt_neighbor_tet,
                                             neighbor_canonicalTrace, fldAssoc_Outer_BTrace_Tri, OuterBTraceGroup_Tri_,
                                             cnt_outer_Btraces_tri, cnt_outer_Btraces_tri_split);

          fldAssocCell_Neighbors_Tet->setAssociativity( DOFAssoc_NeighborCell, cnt_neighbor_tet);

          // DOF associativity for interior trace
          ElementAssociativityConstructor<TopoD2, Triangle> DOFAssoc_ITrace( trace_basis_Tri );

          //Set DOF associativity
          setInteriorTraceAssociativity(traceinfo.group, traceinfo.elem, DOFAssoc_ITrace);


          int itrace_index = cnt_neighbor_tet; //The new index for this interior trace is simply the number of neighboring cells passed

          fldAssoc_ITrace_Tri->setAssociativity( DOFAssoc_ITrace, itrace_index );

          //Save this interior trace's mapping from local to global mesh
          this->InteriorTraceMapping_[{ITraceGroupTetNeighbors,itrace_index}] =
                xfld_local_unsplit_.getGlobalInteriorTraceMap({ITraceGroupTetNeighbors,itrace_index - cnt_neighbor_tet_split});

          this->InteriorTraceSplitInfo_[{ITraceGroupTetNeighbors,itrace_index}] = ElementSplitInfo(ElementSplitFlag::Unsplit);

          //Get the indices of the main sub-cell on the left based on split-configuration and trace index
          std::vector<int> main_subelemlist = getMainSubCellElements<Tet>(itrace, SplitEdgeIndex_);

          fldAssoc_ITrace_Tri->setGroupLeft(0); //Cell group on left is the main cell's group = 0
          fldAssoc_ITrace_Tri->setElementLeft(main_subelemlist[0], itrace_index);
          fldAssoc_ITrace_Tri->setCanonicalTraceLeft(main_canonicalTrace, itrace_index);

          fldAssoc_ITrace_Tri->setGroupRight(CellGroupTetNeighbors_); //Cell group on right is the triangle neighbors' cell group
          fldAssoc_ITrace_Tri->setElementRight(itrace_index, itrace_index); //Right element for interior trace i is equal to i
          fldAssoc_ITrace_Tri->setCanonicalTraceRight(neighbor_canonicalTrace, itrace_index);

          cnt_neighbor_tet++; //Added only one (unsplit) neighbor
        }

      }
      else if ( xfld_local_unsplit_.getCellGroupBase(neighbor_group).topoTypeID() == typeid(Hex) )
      {
        SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD3, T>::splitNeighbors - Hex splitting not supported yet.");
      }
      else
        SANS_DEVELOPER_EXCEPTION( "XField_Local<PhysDim, TopoD3, T>::extractLocalGrid - Unknown cell topology for neighbor." );

    }
    else if (traceinfo.type == TraceInfo::Boundary) //We are dealing with a boundary trace of the main cell
    {
      if (xfld_local_unsplit_.getBoundaryTraceGroupBase(traceinfo.group).topoTypeID() == typeid(Triangle))
      {
        //Edge-split
        int new_nodeDOF = -1;

        //Get the list of traces that are affected from this edge-split
        std::vector<int> split_trace_list = getSplitTracesFromSplitEdge<Tet>(SplitEdgeIndex_);

        bool split_boundary_flag = false;

        for (int i=0; i<(int) split_trace_list.size(); i++)
        {
          if (itrace==split_trace_list[i])
            split_boundary_flag = true;
        }

        if (split_boundary_flag)
        {
          process_MainBTrace_EdgeSplit<Tet,Triangle>(traceinfo.group, traceinfo.elem, order, itrace, new_nodeDOF, cnt_main_Btraces);
        }
        else
          process_MainBTrace<Tet,Triangle>(traceinfo.group, traceinfo.elem, order, itrace, cnt_main_Btraces);

      }
      else if (xfld_local_unsplit_.getBoundaryTraceGroupBase(traceinfo.group).topoTypeID() == typeid(Quad))
      {
        process_MainBTrace<Hex,Quad>(traceinfo.group, traceinfo.elem, order, itrace, cnt_main_Btraces);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "XField_Local<PhysDim, TopoD3, T>::extractLocalGrid - Unknown trace topology for boundary trace." );
    }
  } //repeated loop across traces of main element

  if (nNeighbors_>0)
  {
    if (nTetNeighbors_>0) //Tet neighbors
    {
      //Add cell group and copy DOFs for neighboring cells
      this->cellGroups_[CellGroupTetNeighbors_] = new XFieldCellGroupType_Tet( *fldAssocCell_Neighbors_Tet );
      this->cellGroups_[CellGroupTetNeighbors_]->setDOF( this->DOF_, this->nDOF_ );
      this->nElem_ += fldAssocCell_Neighbors_Tet->nElem();

      //Add trace group and set DOFs for interior traces
      this->interiorTraceGroups_[ITraceGroupTetNeighbors] = new XFieldTraceGroupType_Tri( *fldAssoc_ITrace_Tri );
      this->interiorTraceGroups_[ITraceGroupTetNeighbors]->setDOF( this->DOF_, this->nDOF_ );
    }

    if (nHexNeighbors_>0) //Hex neighbors
    {
      //Add cell group and copy DOFs for neighboring cells
      this->cellGroups_[CellGroupHexNeighbors_] = new XFieldCellGroupType_Hex( *fldAssocCell_Neighbors_Hex );
      this->cellGroups_[CellGroupHexNeighbors_]->setDOF( this->DOF_, this->nDOF_ );
      this->nElem_ += fldAssocCell_Neighbors_Hex->nElem();

      //Add trace group and set DOFs for interior traces
      this->interiorTraceGroups_[ITraceGroupHexNeighbors] = new XFieldTraceGroupType_Quad( *fldAssoc_ITrace_Quad );
      this->interiorTraceGroups_[ITraceGroupHexNeighbors]->setDOF( this->DOF_, this->nDOF_ );
    }

    if (nSplitTetNeighbors_ + nSplitHexNeighbors_>0)
    {
      if (nSplitTetNeighbors_>0)
      {
        this->interiorTraceGroups_[ITraceGroupInterTetNeighbor] = new XFieldTraceGroupType_Tri( *fldAssoc_ITrace_InterTetNeighbor );
        this->interiorTraceGroups_[ITraceGroupInterTetNeighbor]->setDOF( this->DOF_, this->nDOF_ );
      }
      if (nSplitHexNeighbors_>0)
      {
        this->interiorTraceGroups_[ITraceGroupInterHexNeighbor] = new XFieldTraceGroupType_Quad( *fldAssoc_ITrace_InterHexNeighbor );
        this->interiorTraceGroups_[ITraceGroupInterHexNeighbor]->setDOF( this->DOF_, this->nDOF_ );
      }
    }

    if (nOuterBTraceGroups_>0)
    {
      //Add trace group and copy DOFs for interior traces
      if (nTetNeighbors_>0) //Tet neighbors
      {
        this->ghostBoundaryTraceGroups_[OuterBTraceGroup_Tri_] = new XFieldTraceGroupType_Tri( *fldAssoc_Outer_BTrace_Tri );
        this->ghostBoundaryTraceGroups_[OuterBTraceGroup_Tri_]->setDOF( this->DOF_, this->nDOF_ );
      }
      if (nHexNeighbors_>0) //Hex neighbors
      {
        this->ghostBoundaryTraceGroups_[OuterBTraceGroup_Quad_] = new XFieldTraceGroupType_Quad( *fldAssoc_Outer_BTrace_Quad );
        this->ghostBoundaryTraceGroups_[OuterBTraceGroup_Quad_]->setDOF( this->DOF_, this->nDOF_ );
      }
    }
  } //if neighbors

}

template <class PhysDim>
template <class TopoCell, class TopoTrace>
void
XField_Local_Split_Linear_Edge<PhysDim, TopoD3>::
setMainCellAssociativity_EdgeSplit(
    const int new_nodeDOF,
    ElementAssociativityConstructor<TopoD3, TopoCell>& DOFAssocCell0,
    ElementAssociativityConstructor<TopoD3, TopoCell>& DOFAssocCell1,
    ElementAssociativityConstructor<TopoD2, TopoTrace>& DOFAssocTraceMid,
    CanonicalTraceToCell& TraceMid_canonicalL,
    CanonicalTraceToCell& TraceMid_canonicalR )
{

  typedef typename XField<PhysDim, TopoD3>::template FieldCellGroupType<TopoCell> XFieldCellGroupType;

  //Get cell/trace group from unsplit mesh
  const XFieldCellGroupType& cellgrp = xfld_local_unsplit_.template getCellGroup<TopoCell>(mainGroup_);

  const int nNodeDOFCell = TopoCell::NNode;
  const int nNodeDOFTrace = TopoTrace::NNode;

  //node DOF ordering for sub-cells
  int Cell0_nodeDOF_map[nNodeDOFCell], Cell1_nodeDOF_map[nNodeDOFCell];
  int ITraceMid_nodeDOF_map[nNodeDOFTrace]; //nodeDOF map for the trace between the sub-cells (main-main or neighbor-neighbor)

  int rank = cellgrp.associativity(mainElem_).rank();

  //Copying DOF mappings from original cell to both sub-cells
  cellgrp.associativity(mainElem_).getNodeGlobalMapping( Cell0_nodeDOF_map, nNodeDOFCell ); //Get nodeDOF map from unsplit mesh
  cellgrp.associativity(mainElem_).getNodeGlobalMapping( Cell1_nodeDOF_map, nNodeDOFCell ); //Get nodeDOF map from unsplit mesh

  //Updating nodeDOF mappings to include the new DOF
  updateNodeDOFMaps_EdgeSplit<TopoCell>(new_nodeDOF, SplitEdgeIndex_,
                                        Cell0_nodeDOF_map, Cell1_nodeDOF_map, nNodeDOFCell,
                                        ITraceMid_nodeDOF_map, nNodeDOFTrace);

  TraceMid_canonicalL = TraceToCellRefCoord<TopoTrace, TopoD3, TopoCell>::
                        getCanonicalTrace(ITraceMid_nodeDOF_map, TopoTrace::NNode, Cell0_nodeDOF_map, TopoCell::NNode);

  TraceMid_canonicalR = TraceToCellRefCoord<TopoTrace, TopoD3, TopoCell>::
                        getCanonicalTrace(ITraceMid_nodeDOF_map, TopoTrace::NNode, Cell1_nodeDOF_map, TopoCell::NNode);

  //Map subcell nodeDOFs from unsplit mesh to split mesh
  for (int k = 0; k < nNodeDOFCell; k++)
  {
    Cell0_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Cell0_nodeDOF_map[k]);
    Cell1_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Cell1_nodeDOF_map[k]);
  }

  DOFAssocCell0.setRank( rank );
  DOFAssocCell0.setNodeGlobalMapping( Cell0_nodeDOF_map, nNodeDOFCell );
  DOFAssocCell0.faceSign() = cellgrp.associativity(mainElem_).faceSign(); //Copy face signs from original unsplit cell
  DOFAssocCell0.setFaceSign(+1, TraceMid_canonicalL.trace); //Sub-cell 0 is to the left of the new split-interface

  DOFAssocCell1.setRank( rank );
  DOFAssocCell1.setNodeGlobalMapping( Cell1_nodeDOF_map, nNodeDOFCell );
  DOFAssocCell1.faceSign() = cellgrp.associativity(mainElem_).faceSign(); //Copy face signs from original unsplit cell
  DOFAssocCell1.setFaceSign(TraceMid_canonicalR.orientation, TraceMid_canonicalR.trace); //Sub-cell 1 is to the right of the new split-interface

  //Map trace nodeDOFs from unsplit mesh to split mesh
  for (int k = 0; k < nNodeDOFTrace; k++)
    ITraceMid_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(ITraceMid_nodeDOF_map[k]);

  DOFAssocTraceMid.setRank( rank );
  DOFAssocTraceMid.setNodeGlobalMapping( ITraceMid_nodeDOF_map, nNodeDOFTrace );

}

template <class PhysDim>
template <class TopoCell, class TopoTrace>
void
XField_Local_Split_Linear_Edge<PhysDim, TopoD3>::
setCellAssociativity_EdgeSplit(
    const int cell_group, const int cell_elem,
    const int trace_group, const int trace_elem, const int new_nodeDOF,
    ElementAssociativityConstructor<TopoD3, TopoCell>& DOFAssocCell0,
    ElementAssociativityConstructor<TopoD3, TopoCell>& DOFAssocCell1,
    ElementAssociativityConstructor<TopoD2, TopoTrace>& DOFAssocTrace0,
    ElementAssociativityConstructor<TopoD2, TopoTrace>& DOFAssocTrace1,
    ElementAssociativityConstructor<TopoD2, TopoTrace>& DOFAssocTraceMid,
    CanonicalTraceToCell& TraceMid_canonicalL,
    CanonicalTraceToCell& TraceMid_canonicalR,
    int& Itrace_split_edge_index, int Itrace_split_subcell_index[2] )
{

  typedef typename XField<PhysDim, TopoD3>::template FieldCellGroupType<TopoCell> XFieldCellGroupType;
  typedef typename XField<PhysDim, TopoD3>::template FieldTraceGroupType<TopoTrace> XFieldTraceGroupType;

  //Get cell/trace group from unsplit mesh
  const XFieldCellGroupType& cellgrp = xfld_local_unsplit_.template getCellGroup<TopoCell>(cell_group);
  const XFieldTraceGroupType& tracegrp = xfld_local_unsplit_.template getInteriorTraceGroup<TopoTrace>(trace_group);

  const int nNodeDOFCell = TopoCell::NNode;
  const int nNodeDOFTrace = TopoTrace::NNode;

  int trace_nodes[nNodeDOFTrace]; //Node mapping for interior trace between main cell and neighbor cell
  tracegrp.associativity(trace_elem).getNodeGlobalMapping(trace_nodes, nNodeDOFTrace);  //Get nodeDOFs of original trace (from unsplit mesh)

  //node DOF ordering for sub-cells
  int Cell0_nodeDOF_map[nNodeDOFCell], Cell1_nodeDOF_map[nNodeDOFCell];
  int ITrace0_nodeDOF_map[nNodeDOFTrace], ITrace1_nodeDOF_map[nNodeDOFTrace];
  int ITraceMid_nodeDOF_map[nNodeDOFTrace]; //nodeDOF map for the trace between the sub-cells (main-main or neighbor-neighbor)

  int rank = cellgrp.associativity(cell_elem).rank();

  //Copying DOF mappings from original cell to both sub-cells
  cellgrp.associativity(cell_elem).getNodeGlobalMapping( Cell0_nodeDOF_map, nNodeDOFCell ); //Get nodeDOF map from unsplit mesh
  cellgrp.associativity(cell_elem).getNodeGlobalMapping( Cell1_nodeDOF_map, nNodeDOFCell ); //Get nodeDOF map from unsplit mesh


  if ( xfld_local_unsplit_.getCellGroupBase(cell_group).topoTypeID() == typeid(Tet) )
  {

    int outer_node = -1; // index of nodeDOF which is on the outer boundary on this neighboring cell
    for (int k=0; k<nNodeDOFCell; k++)
    {
      if (Cell0_nodeDOF_map[k]!=trace_nodes[0] && Cell0_nodeDOF_map[k]!=trace_nodes[1] && Cell0_nodeDOF_map[k]!=trace_nodes[2])
      {
        outer_node = Cell0_nodeDOF_map[k];
        break;
      }
    }

    //Updating nodeDOF map for sub-cells with new node
    for (int k=0; k<nNodeDOFCell; k++)
    {
      if (Cell0_nodeDOF_map[k]==SplitEdgeNodeMap_[1])
        Cell0_nodeDOF_map[k] = new_nodeDOF; //first sub-cell should contain the first node of the split edge

      if (Cell1_nodeDOF_map[k]==SplitEdgeNodeMap_[0])
        Cell1_nodeDOF_map[k] = new_nodeDOF; //second sub-cell should contain the second node of the split edge
    }

    //Dividing the original interior trace to two sub-traces
    for (int k=0; k<nNodeDOFTrace; k++)
    {
      if (trace_nodes[k]==SplitEdgeNodeMap_[1])
        ITrace0_nodeDOF_map[k] = new_nodeDOF; //first sub-trace should contain the first node of the split edge
      else
        ITrace0_nodeDOF_map[k] = trace_nodes[k];

      if (trace_nodes[k]==SplitEdgeNodeMap_[0])
        ITrace1_nodeDOF_map[k] = new_nodeDOF; //second sub-trace should contain the second node of the split edge
      else
        ITrace1_nodeDOF_map[k] = trace_nodes[k];
    }

    //Find the canonical trace (edge) index of the interior trace (face), which corresponds to the split edge.
    CanonicalTraceToCell splitEdge_canonicalTrace = TraceToCellRefCoord<Line, TopoD2, Triangle>::
                                                        getCanonicalTrace(SplitEdgeNodeMap_, Line::NNode,
                                                                          trace_nodes, nNodeDOFTrace);

    //Save interior trace split information
    Itrace_split_edge_index = splitEdge_canonicalTrace.trace;

    if (splitEdge_canonicalTrace.orientation == 1)
    {
      Itrace_split_subcell_index[0] = 0; Itrace_split_subcell_index[1] = 1;
    }
    else //Edge is in reverse-direction (so swap subcell indices)
    {
      Itrace_split_subcell_index[0] = 1; Itrace_split_subcell_index[1] = 0;
    }

    //Get the node on the interior-trace between main-cell and neighbor, which connects to the new node on split edge.
    int base_node = trace_nodes[splitEdge_canonicalTrace.trace];

    //Temporarily fill nodeDOF map for interior trace between neighboring sub-cells - nodes not necessarily in the correct order.
    ITraceMid_nodeDOF_map[0] = base_node;
    ITraceMid_nodeDOF_map[1] = new_nodeDOF;
    ITraceMid_nodeDOF_map[2] = outer_node;

    //Find the canonical trace of sub-cell 0, for the temporary node-ordering above
    TraceMid_canonicalL = TraceToCellRefCoord<Triangle, TopoD3, Tet>::
                          getCanonicalTrace(ITraceMid_nodeDOF_map, nNodeDOFTrace, Cell0_nodeDOF_map, nNodeDOFCell);

    //Once the canonical trace is found, we can get the interior-trace nodes in the correct order (i.e. canonical orientation to sub-cell 0)
    for (int k=0; k < nNodeDOFTrace; k++)
      ITraceMid_nodeDOF_map[k] = Cell0_nodeDOF_map[TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[TraceMid_canonicalL.trace][k]];

    TraceMid_canonicalL.orientation = 1; //Canonical orientation for sub-cell 0 by design

    //Find the canonical trace for sub-cell 1, for the correctly ordered interior-trace nodes
    TraceMid_canonicalR = TraceToCellRefCoord<Triangle, TopoD3, Tet>::
                          getCanonicalTrace(ITraceMid_nodeDOF_map, nNodeDOFTrace, Cell1_nodeDOF_map, nNodeDOFCell);


  }
  else if ( xfld_local_unsplit_.getCellGroupBase(cell_group).topoTypeID() == typeid(Hex) )
  {
    SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD3, T>::setCellAssociativity_EdgeSplit - "
                              "Hex splitting not supported yet." );
  }
  else
    SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD3, T>::setCellAssociativity_EdgeSplit - "
                              "Unknown cell topology.");


  //Map subcell nodeDOFs from unsplit mesh to split mesh
  for (int k = 0; k < nNodeDOFCell; k++)
  {
    Cell0_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Cell0_nodeDOF_map[k]);
    Cell1_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Cell1_nodeDOF_map[k]);
  }

  DOFAssocCell0.setRank( rank );
  DOFAssocCell0.setNodeGlobalMapping( Cell0_nodeDOF_map, nNodeDOFCell );
  DOFAssocCell0.faceSign() = cellgrp.associativity(cell_elem).faceSign(); //Copy edge signs from original unsplit cell
  DOFAssocCell0.setFaceSign(+1, TraceMid_canonicalL.trace); //Sub-cell 0 is to the left of the new split-interface

  DOFAssocCell1.setRank( rank );
  DOFAssocCell1.setNodeGlobalMapping( Cell1_nodeDOF_map, nNodeDOFCell );
  DOFAssocCell1.faceSign() = cellgrp.associativity(cell_elem).faceSign(); //Copy edge signs from original unsplit cell
  DOFAssocCell1.setFaceSign(TraceMid_canonicalR.orientation, TraceMid_canonicalR.trace); //Sub-cell 1 is to the right of the new split-interface

  //Map trace nodeDOFs from unsplit mesh to split mesh
  for (int k = 0; k < nNodeDOFTrace; k++)
  {
    ITrace0_nodeDOF_map[k]   = offset_nodeDOF_ + NodeDOFMap_.at(ITrace0_nodeDOF_map[k]);
    ITrace1_nodeDOF_map[k]   = offset_nodeDOF_ + NodeDOFMap_.at(ITrace1_nodeDOF_map[k]);
    ITraceMid_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(ITraceMid_nodeDOF_map[k]);
  }

  DOFAssocTrace0.setRank( rank );
  DOFAssocTrace1.setRank( rank );
  DOFAssocTraceMid.setRank( rank );

  DOFAssocTrace0.setNodeGlobalMapping  ( ITrace0_nodeDOF_map  , nNodeDOFTrace );
  DOFAssocTrace1.setNodeGlobalMapping  ( ITrace1_nodeDOF_map  , nNodeDOFTrace );
  DOFAssocTraceMid.setNodeGlobalMapping( ITraceMid_nodeDOF_map, nNodeDOFTrace );

}

template <class PhysDim>
template <class TopoCell>
void
XField_Local_Split_Linear_Edge<PhysDim, TopoD3>::
setCellAssociativity(int group, int elem, ElementAssociativityConstructor<TopoD3, TopoCell>& DOFAssoc)
{

  typedef typename XField<PhysDim, TopoD3>::template FieldCellGroupType<TopoCell> XFieldCellGroupType;

  //Get global cell group
  const XFieldCellGroupType& cellgrp_main = xfld_local_unsplit_.template getCellGroup<TopoCell>(group);

  const int nNodeDOF = TopoCell::NNode;

  // node DOF, edge DOF, face DOF and cell DOF ordering for cell
  int Cell_nodeDOF_map[nNodeDOF];

  cellgrp_main.associativity(elem).getNodeGlobalMapping( Cell_nodeDOF_map, nNodeDOF ); //Get nodeDOF map from global field

  //Map DOFs from global to local mesh
  for (int k = 0; k < nNodeDOF; k++)
    Cell_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Cell_nodeDOF_map[k]);

  DOFAssoc.setRank( cellgrp_main.associativity(elem).rank() );
  DOFAssoc.setNodeGlobalMapping( Cell_nodeDOF_map, nNodeDOF );
  DOFAssoc.faceSign() = cellgrp_main.associativity(elem).faceSign();

}

template<class PhysDim>
template <class TopoTrace>
void
XField_Local_Split_Linear_Edge< PhysDim, TopoD3>::
setInteriorTraceAssociativity(int group, int elem,
                              ElementAssociativityConstructor<TopoD2, TopoTrace>& DOFAssoc)
{

  typedef typename XField<PhysDim, TopoD3>::template FieldTraceGroupType<TopoTrace> XFieldTraceGroupType;

  //Get global interior trace group
  const XFieldTraceGroupType& tracegrp = xfld_local_unsplit_.template getInteriorTraceGroup<TopoTrace>(group);

  const int nNodeDOF = TopoTrace::NNode;

  // node DOF, edge DOF and face DOF ordering for interior trace
  int Trace_nodeDOF_map[nNodeDOF];

  tracegrp.associativity(elem).getNodeGlobalMapping( Trace_nodeDOF_map, nNodeDOF ); //Get nodeDOF map from global field

  //Map DOFs from global to local mesh
  for (int k = 0; k < nNodeDOF; k++)
    Trace_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Trace_nodeDOF_map[k]);

  DOFAssoc.setRank( tracegrp.associativity(elem).rank() );
  DOFAssoc.setNodeGlobalMapping( Trace_nodeDOF_map, nNodeDOF );

}


template<class PhysDim>
template <class TopoTrace>
void
XField_Local_Split_Linear_Edge< PhysDim, TopoD3>::
setBoundaryTraceAssociativity(int group, int elem,
                              ElementAssociativityConstructor<TopoD2, TopoTrace>& DOFAssoc)
{

  typedef typename XField<PhysDim, TopoD3>::template FieldTraceGroupType<TopoTrace> XFieldTraceGroupType;

  //Get global boundary trace group
  const XFieldTraceGroupType& tracegrp = xfld_local_unsplit_.template getBoundaryTraceGroup<TopoTrace>(group);

  const int nNodeDOF = TopoTrace::NNode;

  // node DOF, edge DOF and face DOF ordering for boundary trace
  int Trace_nodeDOF_map[nNodeDOF];

  tracegrp.associativity(elem).getNodeGlobalMapping( Trace_nodeDOF_map, nNodeDOF ); //Get nodeDOF map from global field

  //Map DOFs from global to local mesh
  for (int k = 0; k < nNodeDOF; k++)
    Trace_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Trace_nodeDOF_map[k]);

  DOFAssoc.setRank( tracegrp.associativity(elem).rank() );
  DOFAssoc.setNodeGlobalMapping( Trace_nodeDOF_map, nNodeDOF );
}

template<class PhysDim>
template <class TopoTrace>
void
XField_Local_Split_Linear_Edge< PhysDim, TopoD3>::
setGhostBoundaryTraceAssociativity(int group, int elem,
                                   ElementAssociativityConstructor<TopoD2, TopoTrace>& DOFAssoc)
{

  typedef typename XField<PhysDim, TopoD3>::template FieldTraceGroupType<TopoTrace> XFieldTraceGroupType;

  //Get global boundary trace group
  const XFieldTraceGroupType& tracegrp = xfld_local_unsplit_.template getGhostBoundaryTraceGroup<TopoTrace>(group);

  const int nNodeDOF = TopoTrace::NNode;

  // node DOF, edge DOF and face DOF ordering for boundary trace
  int Trace_nodeDOF_map[nNodeDOF];

  tracegrp.associativity(elem).getNodeGlobalMapping( Trace_nodeDOF_map, nNodeDOF ); //Get nodeDOF map from global field

  //Map DOFs from global to local mesh
  for (int k = 0; k < nNodeDOF; k++)
    Trace_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Trace_nodeDOF_map[k]);

  DOFAssoc.setRank( tracegrp.associativity(elem).rank() );
  DOFAssoc.setNodeGlobalMapping( Trace_nodeDOF_map, nNodeDOF );
}


template<class PhysDim>
template <class TopoCell, class TopoTrace>
void
XField_Local_Split_Linear_Edge< PhysDim, TopoD3>::process_MainBTrace(const int trace_group, const int trace_elem, const int order,
                                                                const int main_cell_trace, int& cnt_main_btracegroups)
{

  typedef typename XField<PhysDim, TopoD3>::template FieldTraceGroupType<TopoTrace> XFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::BasisType TraceBasisType;

  const TraceBasisType* trace_basis = BasisFunctionAreaBase<TopoTrace>::getBasisFunction(order, basisCategory_);

  const XFieldTraceGroupType& tracegrp = xfld_local_unsplit_.template getBoundaryTraceGroup<TopoTrace>(trace_group);

  //canonicalTrace of the main cell for the current trace
  CanonicalTraceToCell main_canonicalTrace = tracegrp.getCanonicalTraceLeft(trace_elem);

  // DOF associativity for boundary trace
  ElementAssociativityConstructor<TopoD2, TopoTrace> DOFAssoc_BTrace( trace_basis );

  setBoundaryTraceAssociativity(trace_group, trace_elem, DOFAssoc_BTrace);

  // create field associativity constructor for each boundary trace of main cell
  // Note: each boundary trace of the main cell has its own boundaryTraceGroup!
  typename XFieldTraceGroupType::FieldAssociativityConstructorType fldAssoc_Main_BTrace( trace_basis, 1);

  fldAssoc_Main_BTrace.setAssociativity( DOFAssoc_BTrace, 0);

  //Save this boundary trace's mapping from local to global mesh
  this->BoundaryTraceMapping_[{cnt_main_btracegroups,0}] = xfld_local_unsplit_.getGlobalBoundaryTraceMap({cnt_main_btracegroups,0});

  this->BoundaryTraceSplitInfo_[{cnt_main_btracegroups,0}] = ElementSplitInfo(ElementSplitFlag::Unsplit);

  //Get the indices of the main sub-cell on the left based on split-configuration and trace index
  std::vector<int> main_subelemlist = getMainSubCellElements<TopoCell>(main_cell_trace, SplitEdgeIndex_);

  fldAssoc_Main_BTrace.setGroupLeft(0); //Cell group on left is the main cell's group = 0
  fldAssoc_Main_BTrace.setElementLeft(main_subelemlist[0], 0);
  fldAssoc_Main_BTrace.setCanonicalTraceLeft(main_canonicalTrace, 0);

  //Add trace group and copy DOFs for boundary traces of main cell
  this->boundaryTraceGroups_[cnt_main_btracegroups] = new XFieldTraceGroupType( fldAssoc_Main_BTrace );
  this->boundaryTraceGroups_[cnt_main_btracegroups]->setDOF( this->DOF_, this->nDOF_ );
  cnt_main_btracegroups++;
}

template<class PhysDim>
template <class TopoCell, class TopoTrace>
void
XField_Local_Split_Linear_Edge< PhysDim, TopoD3>::process_MainBTrace_EdgeSplit(const int trace_group, const int trace_elem, const int order,
                                                                               const int main_cell_trace, const int new_nodeDOF,
                                                                               int& cnt_main_btracegroups)
{

  typedef typename XField<PhysDim, TopoD3>::template FieldTraceGroupType<TopoTrace> XFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::BasisType TraceBasisType;

  const TraceBasisType* trace_basis = BasisFunctionAreaBase<TopoTrace>::getBasisFunction(order, basisCategory_);

  const XFieldTraceGroupType& tracegrp = xfld_local_unsplit_.template getBoundaryTraceGroup<TopoTrace>(trace_group);

  //node DOF ordering for sub-cells and boundary trace
  const int nNodeDOFTrace = TopoTrace::NNode;
  int BTrace0_nodeDOF_map[nNodeDOFTrace], BTrace1_nodeDOF_map[nNodeDOFTrace], BTrace_unsplit_nodeDOF_map[nNodeDOFTrace];

  //Copy nodeDOFs from original unsplit boundary trace to both sub-btraces
  tracegrp.associativity(trace_elem).getNodeGlobalMapping( BTrace0_nodeDOF_map, nNodeDOFTrace );
  tracegrp.associativity(trace_elem).getNodeGlobalMapping( BTrace1_nodeDOF_map, nNodeDOFTrace );
  tracegrp.associativity(trace_elem).getNodeGlobalMapping( BTrace_unsplit_nodeDOF_map, nNodeDOFTrace );

  if ( xfld_local_unsplit_.getCellGroupBase(mainGroup_).topoTypeID() == typeid(Tet) )
  {
    //Split original boundary trace to two sub-traces
    for (int k=0; k<nNodeDOFTrace; k++)
    {
      if (BTrace0_nodeDOF_map[k]==SplitEdgeNodeMap_[1])
        BTrace0_nodeDOF_map[k] = new_nodeDOF; //first sub-trace should contain the first node of the split edge

      if (BTrace1_nodeDOF_map[k]==SplitEdgeNodeMap_[0])
        BTrace1_nodeDOF_map[k] = new_nodeDOF; //second sub-trace should contain the second node of the split edge
    }
  }
  else if ( xfld_local_unsplit_.getCellGroupBase(mainGroup_).topoTypeID() == typeid(Hex) )
  {
    SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD3, T>::process_MainBTrace_EdgeSplit - "
                              "Hex splitting not supported yet." );
  }
  else
    SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD3, T>::process_MainBTrace_EdgeSplit - "
                              "Unknown cell topology for main-cell.");


  //canonicalTrace of the main cell for the current trace
  CanonicalTraceToCell main_canonicalTrace = tracegrp.getCanonicalTraceLeft(trace_elem);

  // DOF associativity for split boundary traces
  ElementAssociativityConstructor<TopoD2, TopoTrace> DOFAssoc_BTrace0( trace_basis );
  ElementAssociativityConstructor<TopoD2, TopoTrace> DOFAssoc_BTrace1( trace_basis );

  //Map trace nodeDOFs from unsplit mesh to split mesh
  for (int k = 0; k < nNodeDOFTrace; k++)
  {
    BTrace0_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(BTrace0_nodeDOF_map[k]);
    BTrace1_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(BTrace1_nodeDOF_map[k]);
  }

  DOFAssoc_BTrace0.setRank( tracegrp.associativity(trace_elem).rank() );
  DOFAssoc_BTrace1.setRank( tracegrp.associativity(trace_elem).rank() );

  DOFAssoc_BTrace0.setNodeGlobalMapping( BTrace0_nodeDOF_map, nNodeDOFTrace );
  DOFAssoc_BTrace1.setNodeGlobalMapping( BTrace1_nodeDOF_map, nNodeDOFTrace );

  // create field associativity constructor for each boundary trace of main cell
  // Note: each boundary trace of the main cell has its own boundaryTraceGroup!
  typename XFieldTraceGroupType::FieldAssociativityConstructorType fldAssoc_Main_BTrace( trace_basis, 2);

  fldAssoc_Main_BTrace.setAssociativity( DOFAssoc_BTrace0, 0);
  fldAssoc_Main_BTrace.setAssociativity( DOFAssoc_BTrace1, 1);

  //Save this boundary trace's mapping from local to global mesh
  this->BoundaryTraceMapping_[{cnt_main_btracegroups,0}] = xfld_local_unsplit_.getGlobalBoundaryTraceMap({cnt_main_btracegroups,0});
  this->BoundaryTraceMapping_[{cnt_main_btracegroups,1}] = xfld_local_unsplit_.getGlobalBoundaryTraceMap({cnt_main_btracegroups,0});

  //Get the canonical trace of outer btrace for the edge that is being split
  CanonicalTraceToCell splitEdge_canonicalTrace = TraceToCellRefCoord<Line, TopoD2, Triangle>::getCanonicalTrace(
                                                      SplitEdgeNodeMap_,Line::NNode,
                                                      BTrace_unsplit_nodeDOF_map, nNodeDOFTrace);

  if (splitEdge_canonicalTrace.orientation == 1)
  {
    this->BoundaryTraceSplitInfo_[{cnt_main_btracegroups,0}] =
        ElementSplitInfo(ElementSplitFlag::Split, ElementSplitType::Edge, splitEdge_canonicalTrace.trace, 0);

    this->BoundaryTraceSplitInfo_[{cnt_main_btracegroups,1}] =
        ElementSplitInfo(ElementSplitFlag::Split, ElementSplitType::Edge, splitEdge_canonicalTrace.trace, 1);
  }
  else //Edge is in reverse-direction (so swap subcell indices)
  {
    this->BoundaryTraceSplitInfo_[{cnt_main_btracegroups,0}] =
        ElementSplitInfo(ElementSplitFlag::Split, ElementSplitType::Edge, splitEdge_canonicalTrace.trace, 1);

    this->BoundaryTraceSplitInfo_[{cnt_main_btracegroups,1}] =
        ElementSplitInfo(ElementSplitFlag::Split, ElementSplitType::Edge, splitEdge_canonicalTrace.trace, 0);
  }

  //Get the indices of the main sub-cell on the left based on split-configuration and trace index
  std::vector<int> main_subelemlist = getMainSubCellElements<TopoCell>(main_cell_trace, SplitEdgeIndex_);

  fldAssoc_Main_BTrace.setGroupLeft(mainGroup_); //Cell group on left is the main cell's group = 0
  fldAssoc_Main_BTrace.setElementLeft(main_subelemlist[0], 0);
  fldAssoc_Main_BTrace.setElementLeft(main_subelemlist[1], 1);
  fldAssoc_Main_BTrace.setCanonicalTraceLeft(main_canonicalTrace, 0);
  fldAssoc_Main_BTrace.setCanonicalTraceLeft(main_canonicalTrace, 1);

  //Add trace group and copy DOFs for boundary traces of main cell
  this->boundaryTraceGroups_[cnt_main_btracegroups] = new XFieldTraceGroupType( fldAssoc_Main_BTrace );
  this->boundaryTraceGroups_[cnt_main_btracegroups]->setDOF( this->DOF_, this->nDOF_ );
  cnt_main_btracegroups++;

}

template<class PhysDim>
template <class TopoCell, class TopoTrace>
void
XField_Local_Split_Linear_Edge< PhysDim, TopoD3>::process_OuterBTraces(const int neighbor_group, const int neighbor_elem,
                                                     const int order, const int Ntrace,
                                                     const int local_neighbor_group, const int local_neighbor_elem,
                                                     const CanonicalTraceToCell& neighbor_canonicalTrace,
                                                     std::shared_ptr<typename BaseType::template FieldTraceGroupType<TopoTrace>
                                                                                      ::FieldAssociativityConstructorType> fldAssoc_Outer_BTrace,
                                                     const int OuterBTraceGroup, int& cnt_outer_Btraces, int& cnt_outer_Btraces_split)
{

  typedef typename XField<PhysDim, TopoD3>::template FieldTraceGroupType<TopoTrace> XFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::BasisType TraceBasisType;

  const TraceBasisType* trace_basis = BasisFunctionAreaBase<TopoTrace>::getBasisFunction(order, basisCategory_);

  // DOF associativity for boundary traces of neighbors
  ElementAssociativityConstructor<TopoD2, TopoTrace> DOFAssoc_Outer_BTrace( trace_basis );

  for (int j = 0; j < Ntrace; j++) //loop across traces of this neighboring element
  {
    if (j != neighbor_canonicalTrace.trace) //Outer boundary trace
    {
      //Get index of outer trace (and its cellgroup)
      const TraceInfo& outertraceinfo = connectivity_.getTrace(neighbor_group, neighbor_elem, j);

      //canonicalTrace of this neighboring cell for the outer (interior) trace
      CanonicalTraceToCell neighbor_outer_canonicalTrace;

      if (outertraceinfo.type == TraceInfo::GhostBoundary) //We are dealing with a boundaryTrace of this neighboring cell
      {
        //Set DOF associativity
        setGhostBoundaryTraceAssociativity(outertraceinfo.group, outertraceinfo.elem, DOFAssoc_Outer_BTrace);

        const XFieldTraceGroupType& outer_tracegrp = xfld_local_unsplit_.template getGhostBoundaryTraceGroup<TopoTrace>(outertraceinfo.group);

        //canonicalTrace of this neighboring cell for the outer boundary trace
        neighbor_outer_canonicalTrace = outer_tracegrp.getCanonicalTraceLeft(outertraceinfo.elem);
      }
      else
        SANS_DEVELOPER_EXCEPTION("XField_Split_Local<PhysDim, TopoD3, T>::processOuterBTraces - "
        "outer_trace_group (= %d) is not of type GhostBoundary!", outertraceinfo.group);

      //Save this boundary trace's mapping from local to global mesh
//      this->BoundaryTraceMapping_[{OuterBTraceGroup,cnt_outer_Btraces}] =
//            xfld_local_unsplit_.getGlobalBoundaryTraceMap({OuterBTraceGroup,(cnt_outer_Btraces-cnt_outer_Btraces_split)});
//
//      this->BoundaryTraceSplitInfo_[{OuterBTraceGroup,cnt_outer_Btraces}] = ElementSplitInfo(ElementSplitFlag::Unsplit);


      //Fill associativity of Outer BoundaryTraceGroup
      fldAssoc_Outer_BTrace->setAssociativity( DOFAssoc_Outer_BTrace, cnt_outer_Btraces);

      //Cell group on left is the neighbor cells' group
      fldAssoc_Outer_BTrace->setGroupLeft(local_neighbor_group);

      //Cell on left is the neighbor cell
      fldAssoc_Outer_BTrace->setElementLeft(local_neighbor_elem, cnt_outer_Btraces);
      fldAssoc_Outer_BTrace->setCanonicalTraceLeft(neighbor_outer_canonicalTrace, cnt_outer_Btraces);

      cnt_outer_Btraces++;
    }
  }

}

template<class PhysDim>
template <class TopoCell, class TopoTrace>
void
XField_Local_Split_Linear_Edge< PhysDim, TopoD3>::
process_OuterBTraces_EdgeSplit(
    const int neighbor_group, const int neighbor_elem,
    const int order, const int Ntrace, const int new_nodeDOF,
    const int local_neighbor_group, const int local_neighbor_elem,
    const CanonicalTraceToCell& neighbor_canonicalTrace,
    std::shared_ptr<typename BaseType::template FieldTraceGroupType<TopoTrace>
                      ::FieldAssociativityConstructorType> fldAssoc_Outer_BTrace,
    const int OuterBTraceGroup, int& cnt_outer_Btraces, int& cnt_outer_Btraces_split)
{

  typedef typename XField<PhysDim, TopoD3>::template FieldTraceGroupType<TopoTrace> XFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::BasisType TraceBasisType;

  const TraceBasisType* trace_basis = BasisFunctionAreaBase<TopoTrace>::getBasisFunction(order, basisCategory_);

  //node DOF ordering for traces
  const int nNodeDOFTrace = TopoTrace::NNode;
  int BTrace_unsplit_nodeDOF_map[nNodeDOFTrace];

  for (int j = 0; j < Ntrace; j++) //loop across traces of this neighboring element
  {
    if (j != neighbor_canonicalTrace.trace) //Outer boundary trace
    {
      //Get index of outer trace (and its cellgroup)
      const TraceInfo& outertraceinfo = connectivity_.getTrace(neighbor_group, neighbor_elem, j);

      CanonicalTraceToCell neighbor_outer_canonicalTrace; //canonicalTrace of this neighboring cell for the outer (interior) trace
//      int btrace_split_edge_index = -1, btrace_split_subcell_index[2];

      if (outertraceinfo.type == TraceInfo::GhostBoundary) //We are dealing with a boundaryTrace of this neighboring cell
      {
        const XFieldTraceGroupType& outer_tracegrp = xfld_local_unsplit_.template getGhostBoundaryTraceGroup<TopoTrace>(outertraceinfo.group);

        //Copy nodeDOFs from original unsplit boundary trace to both sub-btraces
        outer_tracegrp.associativity(outertraceinfo.elem).getNodeGlobalMapping( BTrace_unsplit_nodeDOF_map, nNodeDOFTrace );

        bool contains_SplitEdgeNode0 = false; //Flag to specify is this outer-btrace contains the first node of the split edge
        bool contains_SplitEdgeNode1 = false; //Flag to specify is this outer-btrace contains the second node of the split edge

        for (int k=0; k<nNodeDOFTrace; k++)
        {
          if (BTrace_unsplit_nodeDOF_map[k] == SplitEdgeNodeMap_[0]) contains_SplitEdgeNode0 = true;
          if (BTrace_unsplit_nodeDOF_map[k] == SplitEdgeNodeMap_[1]) contains_SplitEdgeNode1 = true;
        }

        //canonicalTrace of this neighboring cell for the outer boundary trace
        neighbor_outer_canonicalTrace = outer_tracegrp.getCanonicalTraceLeft(outertraceinfo.elem);

        if ( xfld_local_unsplit_.getCellGroupBase(mainGroup_).topoTypeID() == typeid(Tet) )
        {

          if (contains_SplitEdgeNode0 && contains_SplitEdgeNode1) //This outer btrace needs to be split
          {
            int rank = outer_tracegrp.associativity(outertraceinfo.elem).rank();

            int BTrace0_nodeDOF_map[nNodeDOFTrace], BTrace1_nodeDOF_map[nNodeDOFTrace];
            outer_tracegrp.associativity(outertraceinfo.elem).getNodeGlobalMapping( BTrace0_nodeDOF_map, nNodeDOFTrace );
            outer_tracegrp.associativity(outertraceinfo.elem).getNodeGlobalMapping( BTrace1_nodeDOF_map, nNodeDOFTrace );

            //Split original boundary trace to two sub-traces
            for (int k=0; k<nNodeDOFTrace; k++)
            {
              if (BTrace0_nodeDOF_map[k]==SplitEdgeNodeMap_[1])
                BTrace0_nodeDOF_map[k] = new_nodeDOF; //first sub-trace should contain the first node of the split edge

              if (BTrace1_nodeDOF_map[k]==SplitEdgeNodeMap_[0])
                BTrace1_nodeDOF_map[k] = new_nodeDOF; //second sub-trace should contain the second node of the split edge
            }

            //Get the canonical trace of outer btrace for the edge that is being split
//            CanonicalTraceToCell splitEdge_canonicalTrace = TraceToCellRefCoord<Line, TopoD2, Triangle>::getCanonicalTrace(
//                                                                SplitEdgeNodeMap_,Line::NNode,
//                                                                BTrace_unsplit_nodeDOF_map, nNodeDOFTrace);
//
//            btrace_split_edge_index = splitEdge_canonicalTrace.trace;
//
//            if (splitEdge_canonicalTrace.orientation == 1)
//            {
//              btrace_split_subcell_index[0] = 0; btrace_split_subcell_index[1] = 1;
//            }
//            else //Edge is in reverse-direction (so swap subcell indices)
//            {
//              btrace_split_subcell_index[0] = 1; btrace_split_subcell_index[1] = 0;
//            }

            // DOF associativity for boundary traces of neighbors
            ElementAssociativityConstructor<TopoD2, Triangle> DOFAssoc_Outer_BTrace0( trace_basis );
            ElementAssociativityConstructor<TopoD2, Triangle> DOFAssoc_Outer_BTrace1( trace_basis );

            //Map trace nodeDOFs from unsplit mesh to split mesh
            for (int k = 0; k < nNodeDOFTrace; k++)
            {
              BTrace0_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(BTrace0_nodeDOF_map[k]);
              BTrace1_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(BTrace1_nodeDOF_map[k]);
            }

            DOFAssoc_Outer_BTrace0.setRank( rank );
            DOFAssoc_Outer_BTrace1.setRank( rank );

            DOFAssoc_Outer_BTrace0.setNodeGlobalMapping( BTrace0_nodeDOF_map, nNodeDOFTrace );
            DOFAssoc_Outer_BTrace1.setNodeGlobalMapping( BTrace1_nodeDOF_map, nNodeDOFTrace );

            int btrace_index0 = cnt_outer_Btraces;
            int btrace_index1 = cnt_outer_Btraces + 1;

            //Fill associativity of Outer BoundaryTraceGroup
            fldAssoc_Outer_BTrace->setAssociativity( DOFAssoc_Outer_BTrace0, btrace_index0);
            fldAssoc_Outer_BTrace->setAssociativity( DOFAssoc_Outer_BTrace1, btrace_index1);

            //Cell group on left is the neighbor cells' group
            fldAssoc_Outer_BTrace->setGroupLeft(local_neighbor_group);

            //Cell on left is the neighbor cell
            fldAssoc_Outer_BTrace->setElementLeft(local_neighbor_elem  , btrace_index0);
            fldAssoc_Outer_BTrace->setElementLeft(local_neighbor_elem+1, btrace_index1);
            fldAssoc_Outer_BTrace->setCanonicalTraceLeft(neighbor_outer_canonicalTrace, btrace_index0);
            fldAssoc_Outer_BTrace->setCanonicalTraceLeft(neighbor_outer_canonicalTrace, btrace_index1);

            //Save this boundary trace's mapping from local to global mesh
//            this->BoundaryTraceMapping_[{OuterBTraceGroup,btrace_index0}] =
//                  xfld_local_unsplit_.getGlobalBoundaryTraceMap({OuterBTraceGroup,(btrace_index0-cnt_outer_Btraces_split)});
//
//            this->BoundaryTraceMapping_[{OuterBTraceGroup,btrace_index1}] =
//                  xfld_local_unsplit_.getGlobalBoundaryTraceMap({OuterBTraceGroup,(btrace_index0-cnt_outer_Btraces_split)});
//
//            this->BoundaryTraceSplitInfo_[{OuterBTraceGroup,btrace_index0}] =
//                ElementSplitInfo(ElementSplitFlag::Split, ElementSplitType::Edge, btrace_split_edge_index, btrace_split_subcell_index[0]);
//
//            this->BoundaryTraceSplitInfo_[{OuterBTraceGroup,btrace_index1}] =
//                ElementSplitInfo(ElementSplitFlag::Split, ElementSplitType::Edge, btrace_split_edge_index, btrace_split_subcell_index[1]);

            cnt_outer_Btraces_split++;
            cnt_outer_Btraces += 2;

          }
          else
          { //Unsplit outer btrace - just copy

            // DOF associativity for boundary traces of neighbors
            ElementAssociativityConstructor<TopoD2, Triangle> DOFAssoc_Outer_BTrace( trace_basis );

            //Map trace nodeDOFs from unsplit mesh to split mesh
            for (int k = 0; k < nNodeDOFTrace; k++)
              BTrace_unsplit_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(BTrace_unsplit_nodeDOF_map[k]);

            DOFAssoc_Outer_BTrace.setRank( outer_tracegrp.associativity(outertraceinfo.elem).rank() );
            DOFAssoc_Outer_BTrace.setNodeGlobalMapping( BTrace_unsplit_nodeDOF_map, nNodeDOFTrace );

            int btrace_index = cnt_outer_Btraces;

            //Fill associativity of Outer BoundaryTraceGroup
            fldAssoc_Outer_BTrace->setAssociativity( DOFAssoc_Outer_BTrace, btrace_index);

            //Cell group on left is the neighbor cells' group
            fldAssoc_Outer_BTrace->setGroupLeft(local_neighbor_group);

            //Cells on left are the neighbor subcells
            if (contains_SplitEdgeNode0 && !contains_SplitEdgeNode1)
              fldAssoc_Outer_BTrace->setElementLeft(local_neighbor_elem  , btrace_index); //Outer btrace is in subcell0
            else if (!contains_SplitEdgeNode0 && contains_SplitEdgeNode1)
              fldAssoc_Outer_BTrace->setElementLeft(local_neighbor_elem+1, btrace_index); //Outer btrace is in subcell1
            else
              SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD3, T>::process_OuterBTraces_EdgeSplit - Code should not reach here!" );

            fldAssoc_Outer_BTrace->setCanonicalTraceLeft(neighbor_outer_canonicalTrace, btrace_index);

            //Save this boundary trace's mapping from local to global mesh
//            this->BoundaryTraceMapping_[{OuterBTraceGroup,btrace_index}] =
//                  xfld_local_unsplit_.getGlobalBoundaryTraceMap({OuterBTraceGroup,(btrace_index-cnt_outer_Btraces_split)});
//
//            this->BoundaryTraceSplitInfo_[{OuterBTraceGroup,btrace_index}] = ElementSplitInfo(ElementSplitFlag::Unsplit);

            cnt_outer_Btraces++;
          }

        }
        else if ( xfld_local_unsplit_.getCellGroupBase(mainGroup_).topoTypeID() == typeid(Hex) )
        {
          SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD3, T>::process_OuterBTraces_EdgeSplit - "
                                    "Hex splitting not supported yet." );
        }
        else
          SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD3, T>::process_OuterBTraces_EdgeSplit - "
                                    "Unknown cell topology for main-cell.");
      }
      else
        SANS_DEVELOPER_EXCEPTION("XField_Split_Local<PhysDim, TopoD3, T>::processOuterBTraces_EdgeSplit - "
        "outer_trace_group (= %d) is not of type GhostBoundary!", outertraceinfo.group);

    } //if-outer-btrace
  } //trace loop

}


template <class PhysDim>
template <class TopoCell>
int
XField_Local_Split_Linear_Edge<PhysDim, TopoD3>::getSplitEdgeIndexFromEdgeNodes(int node0, int node1, int cell_group, int cell_elem)
{
  const int (*EdgeNodes)[Line::NNode] = ElementEdges<Tet>::EdgeNodes;

  typedef typename XField<PhysDim, TopoD3>::template FieldCellGroupType<TopoCell> XFieldCellGroupType;

  //Get cell/trace group from unsplit mesh
  const XFieldCellGroupType& cellgrp = xfld_local_unsplit_.template getCellGroup<TopoCell>(cell_group);

  //Get nodeDOF map from unsplit mesh
  const int nNodeDOFCell = TopoCell::NNode;
  int Cell_nodeDOF_map[nNodeDOFCell];
  cellgrp.associativity(cell_elem).getNodeGlobalMapping( Cell_nodeDOF_map, nNodeDOFCell );

  if ( xfld_local_unsplit_.getCellGroupBase(cell_group).topoTypeID() == typeid(Tet) )
  {
    for (int i = 0; i < Tet::NEdge; i++)
    {
      if ( node0 == Cell_nodeDOF_map[EdgeNodes[i][0]] && node1 == Cell_nodeDOF_map[EdgeNodes[i][1]])
        return (i+1);
      else if (node0 == Cell_nodeDOF_map[EdgeNodes[i][1]] && node1 == Cell_nodeDOF_map[EdgeNodes[i][0]])
        return -(i+1);  //split-edge in reverse direction
    }
  }
  else if ( xfld_local_unsplit_.getCellGroupBase(cell_group).topoTypeID() == typeid(Hex) )
  {
    SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD3, T>::getSplitEdgeIndexFromEdgeNodes - Hex splitting not supported yet.");
  }
  else
    SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD3, T>::getSplitEdgeIndexFromEdgeNodes - Unknown cell topology." );

  return -99;
}

template<class PhysDim>
template <class TopoTrace>
void
XField_Local_Split_Linear_Edge< PhysDim, TopoD3>::getNeighborInfoFromTrace(const int trace_group, const int trace_elem, const int main_elem,
                                                         int& neighbor_group, int& neighbor_elem,
                                                         CanonicalTraceToCell& neighbor_canonicalTrace, CanonicalTraceToCell& main_canonicalTrace)
{

  typedef typename XField<PhysDim, TopoD3>::template FieldTraceGroupType<TopoTrace> XFieldTraceGroupType;
  const XFieldTraceGroupType& tracegrp = xfld_local_unsplit_.template getInteriorTraceGroup<TopoTrace>(trace_group);

  if (tracegrp.getElementLeft(trace_elem)==main_elem)
  { //element to left of trace is the "main_elem" => getElementRight gives the neighboring cell
    neighbor_group          = tracegrp.getGroupRight();
    neighbor_elem           = tracegrp.getElementRight(trace_elem);
    neighbor_canonicalTrace = tracegrp.getCanonicalTraceRight(trace_elem);
    main_canonicalTrace     = tracegrp.getCanonicalTraceLeft(trace_elem);
  }
  else
    SANS_DEVELOPER_EXCEPTION("XField_Split_Local<PhysDim, TopoD3, T>::getNeighborInfoFromTrace - "
                             "The main-cell is not to the left of its neighbors in the unsplit local mesh.");
}

template <class PhysDim>
template <class TopoCell>
void
XField_Local_Split_Linear_Edge<PhysDim, TopoD3>::trackDOFs(int group, int elem)
{
  typedef typename XField<PhysDim, TopoD3>::template FieldCellGroupType<TopoCell> XFieldCellGroupType;

  std::pair<std::map<int,int>::iterator,bool> map_ret; //return value for map inserts

  const XFieldCellGroupType& cellgrp = xfld_local_unsplit_.template getCellGroup<TopoCell>(group);

  // node DOF map
  const int nNodeDOF = TopoCell::NNode;
  int nodeMap[nNodeDOF];

  //Save DOF mappings from global mesh to local mesh
  cellgrp.associativity(elem).getNodeGlobalMapping( nodeMap, nNodeDOF );
  for (int k = 0; k < nNodeDOF; k++)
  {
    map_ret = NodeDOFMap_.insert( std::pair<int,int>(nodeMap[k],cntNodeDOF_) );
    if (map_ret.second==true) cntNodeDOF_++; //increment counter if object was actually added
  }

}

//Explicit instantiations
template class XField_Local_Split_Linear_Edge<PhysD3,TopoD3>;

}
