// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

/*
 * XFieldVolume_Local_Split.h
 *
 *  Created on: Feb 29, 2016
 */

#ifndef XFIELDVOLUME_LOCAL_SPLIT_H_
#define XFIELDVOLUME_LOCAL_SPLIT_H_


#include <vector>

#include "Topology/Dimension.h"

#include "Field/Element/ElementAssociativityAreaConstructor.h"
#include "Field/Element/ElementAssociativityVolumeConstructor.h"

#include "Field/Element/ElementProjection_L2.h"
#include "XFieldVolume_Local.h"
#include "XFieldVolume_Local_Split_Linear_Isotropic.h"
#include "XFieldVolume_Local_Split_Linear_Edge.h"

/*
 * This class splits the main cell (tet or hex) in the local mesh produced by XField_Local according to the given split type.
 * - The sub-main-cells are in cell group 0
 * - All neighboring cells remain in cell group 1
 * - Each split introduces new interior traces (triangles/quads) within the original main-cell, and also within the original neighbors.
 * - These new main-main interfaces and neighbor-neighbor interfaces are put into interior trace groups of their own.
 * - The process first constructs a *linear* (P1) split-mesh and then uses the XField->buildFrom to add higher-order nodes if needed.
 */

/* Refer to the local solve documentation in Field/Documentation/Local_Solves/LocalSolves.pdf for more details and figures. */

namespace SANS
{

template <class PhysDim, class TopoDim>
class XField_Local_Split;

template<class PhysDim>
class XField_Local_Split< PhysDim, TopoD3> : public XField_Local_Base<PhysDim, TopoD3>
{

public:
  typedef XField_Local_Base<PhysDim, TopoD3> BaseType;

  XField_Local_Split(const XField_Local<PhysDim, TopoD3>& xfld_local, XField_CellToTrace<PhysDim,TopoD3>& connectivity,
                     ElementSplitType split_type, int split_edge_index);

  ~XField_Local_Split() {}

  const XField_CellToTrace<PhysDim, TopoD3>& getConnectivity() const { return xfld_local_unsplit_.getConnectivity(); }

protected:
  using BaseType::reSolveBoundaryTraceGroups_;
  using BaseType::reSolveInteriorTraceGroups_;
  using BaseType::reSolveCellGroups_;
  const XField_Local<PhysDim, TopoD3>& xfld_local_unsplit_;
  const XField_CellToTrace<PhysDim, TopoD3>& connectivity_;

  std::vector<int> orderVector_; //Basis order for each cell group in split-mesh

  int SplitEdgeIndex_; //Canonical edge of main-cell which needs to be split
  bool IsotropicSplitFlag_; //Flag which indicates if the split is a isotropic split
  int SplitEdgeNodeMap_[2]; //Node DOF mapping for the edge that needs to be split

  int CellGroupTetNeighbors_, CellGroupHexNeighbors_; //Group index for triangle/quad cell groups


  void getOrder();

  void copyInfo(XField_Local_Split_Linear_Isotropic<PhysDim,TopoD3>& xfld_split_local_linear);
  void copyInfo(XField_Local_Split_Linear_Edge<PhysDim,TopoD3>& xfld_split_local_linear);

  void projectMainCell(ElementSplitType split_type, int split_edge_index);
  void projectNeighbors(int Ntrace, int order, ElementSplitType split_type, int split_edge_index);

  template<class TopoCell>
  void projectElem(int cell_group, int cell_elem, int sub_group, int sub_elem,
                   Element_Subdivision_Projector<TopoD3, TopoCell>& projector);

  template <class TopoTrace>
  void getNeighborInfoFromTrace(const int trace_group, const int trace_elem, const int main_elem,
                                int& neighbor_group, int& neighbor_elem,
                                CanonicalTraceToCell& neighbor_canonicalTrace, CanonicalTraceToCell& main_canonicalTrace);
};

}
#endif /* XFIELDVOLUME_LOCAL_SPLIT_H_ */
