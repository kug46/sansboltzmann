// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELDLINE_LOCAL_H
#define XFIELDLINE_LOCAL_H

#include <map>

#include "Topology/Dimension.h"

#include "XField_CellToTrace.h"
#include "XField.h"
#include "XField_Local_Base.h"

#include "Field/Element/ElementAssociativityNodeConstructor.h"
#include "Field/Element/ElementAssociativityLineConstructor.h"

#include "MPI/communicator_fwd.h"

/*
 * This class constructs a 1D local mesh for a specified cell in the 1D global mesh.
 * The constructed local mesh will have the following properties:
 * - The target cell (or main-cell) will be assigned to cellgroup 0, element 0
 * - All (immediate) neighboring cells of the main-cell, if any, will be assigned to cellgroup 1
 * - The traces (nodes) around the main-cell will be oriented so that the main-cell is always to the left.
 * - If the main-cell touches any boundaries of the global mesh, those traces (nodes) will be assigned to a boundary-trace group of their own.
 * - Traces (nodes) of neighboring cells that do not touch the main-cell (referred to as "outer-boundary-traces") will all be assigned
 *   to a single boundary-trace group. This boundary-trace group goes last.
 */

/* Refer to the local solve documentation in Field/Documentation/Local_Solves/LocalSolves.pdf for more details and figures. */

namespace SANS
{

template <class PhysDim, class TopoDim>
class XField_Local;


template<class PhysDim>
class XField_Local< PhysDim, TopoD1> : public XField_Local_Base<PhysDim, TopoD1>
{

public:
  typedef XField_Local_Base<PhysDim, TopoD1> BaseType;

  XField_Local(mpi::communicator& comm_local, const XField_CellToTrace<PhysDim, TopoD1>& connectivity, int group, int elem) :
    BaseType(comm_local),
    connectivity_(connectivity),
    global_xfld_(connectivity_.getXField()) // needed for new []
  {
    extractLocalGrid(group, elem);

    // This is checkGrid() but gets removed in release mode!
    this->checkGrid();

    for (int i = 0; i < this->nBoundaryTraceGroups(); i++)
      reSolveBoundaryTraceGroups_.push_back(i);

    for (int i = 0; i < this->nInteriorTraceGroups(); i++)
      reSolveInteriorTraceGroups_.push_back(i);
  };

  ~XField_Local(){};

  const XField_CellToTrace<PhysDim, TopoD1>& getConnectivity() const { return connectivity_; }

protected:
  using BaseType::reSolveBoundaryTraceGroups_;
  using BaseType::reSolveInteriorTraceGroups_;
  using BaseType::reSolveCellGroups_;
  const XField_CellToTrace<PhysDim, TopoD1>& connectivity_; //connectivity of global mesh
  const XField<PhysDim, TopoD1>& global_xfld_; //global mesh

  //DOF maps
  std::map<int,int> NodeDOFMap_;
  std::map<int,int> EdgeDOFMap_;
  std::map<int,int> CellDOFMap_;

  int cntNodeDOF_, cntEdgeDOF_, cntCellDOF_; //Counters for different DOF types
  int offset_cellDOF_, offset_edgeDOF_, offset_nodeDOF_; //Offset for the different DOF indices in the global array

  int nNeighbors_;
  int nMainBTraceGroups_; //No of boundary traces (and groups) on main cell
  int nOuterBTraceGroups_; //No of boundary groups for outer btraces

  int OuterBTraceGroup_; //Group index for the outer boundary traces

  void extractLocalGrid(int group, int elem);

  void extractMainCell(int main_group, int main_elem);

  void extractNeighbors(int main_group, int main_elem, int Ntrace, int order);

  void trackDOFs(int group, int elem);

  void setCellAssociativity(int group, int elem, ElementAssociativityConstructor<TopoD1, Line>& DOFAssoc);

  void setInteriorTraceAssociativity(int group, int elem, ElementAssociativityConstructor<TopoD0, Node>& DOFAssoc);
  void setBoundaryTraceAssociativity(int group, int elem, ElementAssociativityConstructor<TopoD0, Node>& DOFAssoc);

};

}

#endif // XFIELDLINE_LOCAL_H
