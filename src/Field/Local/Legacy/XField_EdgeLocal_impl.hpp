// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#if !defined(XFIELD_EDGELOCAL_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

// #include "XFieldLine.h"
// #include "XFieldVolume.h"
// #include "XFieldSpacetime.h"
#include <iomanip>
#include <algorithm>
#include "XField_EdgeLocal.h"


namespace SANS
{


template <class PhysDim, class TopoDim>
template <class TopologyCell, class TopologyTrace>
void
XField_EdgeLocal<PhysDim, TopoDim>::extractEdgeLocalGrid(std::pair<int,int> nodes,
                                                         ConstructorVectorTopo<TopoDim,TopologyCell,TopologyTrace>& cV )
{
  //Offsets for node DOFs, =0 because only working on linear mesh

  // Make sure that target nodes are first and second in the map, makes splitting simpler
  NodeDOFMap_.insert( std::pair<int,int>(nodes.first,  cntNodeDOF_)); cntNodeDOF_++;
  NodeDOFMap_.insert( std::pair<int,int>(nodes.second, cntNodeDOF_)); cntNodeDOF_++;

  int nNodes = this->nodalView_.getNodeDOFList().size();
  SANS_ASSERT_MSG( nodes.first>=0  && nodes.first  < nNodes, "node.first = %d out of bounds", nodes.first);
  SANS_ASSERT_MSG( nodes.second>=0 && nodes.second < nNodes, "node.first = %d out of bounds", nodes.second);
  SANS_ASSERT_MSG( nodes.first != nodes.second, "nodes must not be the same" );

  // filling in the structs from the NodalView one
  GroupElemVector cellSet_1(this->nodalView_.getCellList(nodes.first));
  GroupElemVector cellSet_2(this->nodalView_.getCellList(nodes.second));

  // lexicographic sort the cell sets, so that the set operations on vectors below work
  std::sort(cellSet_1.begin(), cellSet_1.end());
  std::sort(cellSet_2.begin(), cellSet_2.end());

  cV.localCellGroup0.clear();
  cV.localCellGroup1.clear();

  // intersection of elements attached to two nodes
  std::set_intersection( cellSet_1.begin(), cellSet_1.end(),
                         cellSet_2.begin(), cellSet_2.end(), std::back_inserter(cV.localCellGroup0) );

  // Need to loop over attached cells and extract their nodes
  std::set<int> connectedNodes;
  for ( auto const& cell : cV.localCellGroup0 )
  {
    const std::vector<int>& cellNodes = this->nodalView_.getNodeList(cell);
    connectedNodes.insert( cellNodes.begin(), cellNodes.end() );
  }

  // Save off the common nodes
  // the first two nodes are the start and end of the edge
  linearCommonNodes_.reserve(connectedNodes.size());
  linearCommonNodes_.push_back(nodes.first);
  linearCommonNodes_.push_back(nodes.second);

  // collect the cell sets for each of these opposition nodes
  // for constructing cellgroup 1
  std::vector<GroupElemVector> cellSets;
  for ( int const& node : connectedNodes )
  {
    cellSets.push_back( this->nodalView_.getCellList(node) );
    if (node != nodes.first && node != nodes.second )
      linearCommonNodes_.push_back(node); // add in nodes other than the edge
  }


  // localCellGroup0 has to be the elements that get split later
  if ( this->neighborhood == Neighborhood::Dual )
  {
    // set to group unique group/elems in omega_K
    std::set<GroupElemIndex> superSet;
    // for (auto it = cellSets.begin(); it != cellSets.end(); ++it)
    //   for (auto itt = it->begin(); itt != it->end(); ++itt)
    //     superSet.insert( *itt );

    for ( auto const& set : cellSets ) // for sets in the set of sets
      for ( auto const& cell : set ) // for cells in the set
        superSet.insert( cell ); // uniquely insert

    // Those elements in superSet, but not localCellGroup0 go into localCellGroup1
    std::set_difference( superSet.begin(),         superSet.end(),
                         cV.localCellGroup0.begin(), cV.localCellGroup0.end(),
                         std::back_inserter(cV.localCellGroup1) );
  }
  else if ( this->neighborhood == Neighborhood::Primal )
  {
    std::set<GroupElemIndex> intersect;
    // \cup_i (omega_0 \cap \omega_i) \cup ( \omega_1 \cap \omega_i)
    for (auto it = cellSets.begin(); it != cellSets.end(); ++it)
    {
      std::set_intersection( it->begin(), it->end(),
                             cellSet_1.begin(), cellSet_1.end(),
                             std::inserter( intersect, intersect.begin() ) );

      std::set_intersection( it->begin(), it->end(),
                             cellSet_2.begin(), cellSet_2.end(),
                             std::inserter( intersect, intersect.begin() ) );
    }


    std::set_difference( intersect.begin(), intersect.end(),
                         cV.localCellGroup0.begin(), cV.localCellGroup0.end(),
                         std::back_inserter( cV.localCellGroup1 ) );
  }
  else
    SANS_DEVELOPER_EXCEPTION("Invalid Neighbourhood");

  // Check the nodes are attached
  SANS_ASSERT_MSG( !cV.localCellGroup0.empty(), "%d and %d do not share an edge", nodes.first, nodes.second);

  // Sort the local sets again so that local ordering matches the global ordering
  // comparators made using the local
  std::sort( cV.localCellGroup0.begin(), cV.localCellGroup0.end() );
  std::sort( cV.localCellGroup1.begin(), cV.localCellGroup1.end() );

  // Check that all groups in localCellGroup0 are of the same type (no mixed grids)
  // all in local group 0
  for (std::size_t i = 0; i < cV.localCellGroup0.size(); i++ )
  {
    GroupElemIndex tmpElem(0, (int) i);

    // Add element to forward and backward maps
    localToGlobalCellMapping_.insert( std::make_pair(tmpElem, (GroupElemIndex) cV.localCellGroup0[i]) ); // casting to plain (GroupElemIndex)
    globalToLocalCellMapping_.insert( std::make_pair((GroupElemIndex) cV.localCellGroup0[i], tmpElem) );

    SANS_ASSERT_MSG( this->global_xfld_.getCellGroupBase(cV.localCellGroup0.front().group).topoTypeID() ==
                     this->global_xfld_.getCellGroupBase(cV.localCellGroup0[i].group).topoTypeID(),
                     "Cell Group Topology must match for all attached elements");

    BaseType::template trackDOFs<TopologyCell>(cV.localCellGroup0[i].group, cV.localCellGroup0[i].elem);
  }

  // Check that all groups in localCellGroup1 are of the same
  // all in local group 1
  for (std::size_t i = 0; i < cV.localCellGroup1.size(); i++ )
  {
    GroupElemIndex tmpElem(1, (int) i);

    // Add element to forward and backward maps
    localToGlobalCellMapping_.insert( std::make_pair(tmpElem, (GroupElemIndex) cV.localCellGroup1[i]) ); // casting to plain
    globalToLocalCellMapping_.insert( std::make_pair((GroupElemIndex) cV.localCellGroup1[i], tmpElem) );

    SANS_ASSERT_MSG( this->global_xfld_.getCellGroupBase(cV.localCellGroup1.front().group).topoTypeID() ==
                     this->global_xfld_.getCellGroupBase(cV.localCellGroup1[i].group).topoTypeID(),
                     "Cell Group Topology must match for all attached elements");

    BaseType::template trackDOFs<TopologyCell>(cV.localCellGroup1[i].group, cV.localCellGroup1[i].elem);
  }

  BaseType::template extractGroups<TopologyCell, TopologyTrace>( cV );

}

}
