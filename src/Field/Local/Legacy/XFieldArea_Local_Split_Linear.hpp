// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELDAREA_LOCAL_SPLIT_LINEAR_H_
#define XFIELDAREA_LOCAL_SPLIT_LINEAR_H_

#include <ostream>
#include <utility> // std::pair

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Field/Element/ElementAssociativityLineConstructor.h"
#include "Field/Element/ElementAssociativityAreaConstructor.h"

#include "XField_Local_Base.h"
#include "XFieldArea_Local.h"

/*
 * This class splits the main cell (triangle or quad) in the local mesh produced by XField_Local according to the given split type.
 * - The two/four sub-cells (main-cells) are in cell group 0
 * - All neighboring cells remain in cell group 1
 * - Each split introduces new interior traces (lines) within the original main-cell, and also within the original neighbors.
 * - These new main-main interfaces and neighbor-neighbor interfaces are put into interior trace groups of their own.
 * - The process first constructs a *linear* (P1) split-mesh and then uses the XField->buildFrom to add higher-order nodes if needed.
 */

/* Refer to the local solve documentation in Field/Documentation/Local_Solves/LocalSolves.pdf for more details and figures. */

namespace SANS
{

template <class TopoCell>
void updateNodeDOFMaps_TraceSplit(const std::vector<int>& trace_nodes, const int new_nodeDOF,
                                  int nodeDOF_map0[], int nodeDOF_map1[], const int nNodeDOF);

template <class TopoCell>
int getSplitEdgeIndexFromTrace(int trace);

template <class PhysDim, class TopoDim>
class XField_Local_Split;

template <class PhysDim, class TopoDim>
class XField_Local_Split_Linear;

template<class PhysDim>
class XField_Local_Split_Linear< PhysDim, TopoD2> : public XField_Local_Base<PhysDim, TopoD2>
{
  friend XField_Local_Split< PhysDim, TopoD2>;

public:
  typedef XField_Local_Base<PhysDim, TopoD2> BaseType;

  XField_Local_Split_Linear(const XField_Local<PhysDim, TopoD2>& xfld_local, XField_CellToTrace<PhysDim,TopoD2>& connectivity,
                            ElementSplitType split_type, int split_edge_index)
                            : BaseType(xfld_local.comm()), xfld_local_unsplit_(xfld_local), connectivity_(connectivity) // needed for new []
  {
    basisCategory_ = xfld_local_unsplit_.getCellGroupBase(mainGroup_).basisCategory();
    split(split_type, split_edge_index);
  };

  ~XField_Local_Split_Linear(){};

  const XField_CellToTrace<PhysDim, TopoD2>& getConnectivity() const { return xfld_local_unsplit_.getConnectivity(); }

protected:
  const XField_Local<PhysDim, TopoD2>& xfld_local_unsplit_;
  const XField_CellToTrace<PhysDim, TopoD2>& connectivity_;

  const int mainGroup_ = 0; //cell-group of the target cell in the unsplit mesh
  const int mainElem_ = 0; //cell-elem of the target cell in the unsplit mesh
  BasisFunctionCategory basisCategory_;

  //DOF maps
  std::map<int,int> NodeDOFMap_;

  int cntNodeDOF_; //Counters for different DOF types
  int offset_nodeDOF_; //Offset for the different DOF indices in the global array

  int nTriNeighbors_, nQuadNeighbors_, nNeighbors_; //Number of neighbors of each type *after* splitting (i.e. includes newly added neighbors)
  int nSplitTriNeighbors_, nSplitQuadNeighbors_; //Number of *new* neighbors of each type added by splitting
  int nSplitMainBTraces_;
  int nMainBTraceGroups_; //No of boundary traces (and groups) on main cell
  int nOuterBTraceGroups_; //No of boundary groups for outer btraces

  int CellGroupTriNeighbors_, CellGroupQuadNeighbors_; //Group index for triangle/quad cell groups
  int ITraceGroupInterMain_; //Group index for the interior traces between main cells
  int OuterBTraceGroup_; //Group index for the outer boundary traces

  int SplitTraceIndex_; //Canonical trace of main-cell which needs to be split
  bool IsotropicSplitFlag_; //Flag which indicates if the split is an isotropic split

  void split(ElementSplitType split_type, int split_edge_index);

  template <class TopoMainCell>
  void splitMainCell();

  void splitNeighbors(int Ntrace, int order);

  template <class Topology>
  void trackDOFs(int group, int elem);

  template <class TopoCell>
  void setCellAssociativity(int group, int elem, ElementAssociativityConstructor<TopoD2, TopoCell>& DOFAssoc);


  template <class TopoCell>
  void setMainCellAssociativity_IsotropicSplit(const std::vector<int>& new_nodeDOFs,
                                               std::vector< ElementAssociativityConstructor<TopoD2, TopoCell> >& DOFAssocs_SubCell,
                                               std::vector< ElementAssociativityConstructor<TopoD1, Line> >& DOFAssocs_MidITrace,
                                               std::vector< CanonicalTraceToCell >& TracesMid_canonicalL,
                                               std::vector< CanonicalTraceToCell >& TracesMid_canonicalR);

  template <class TopoCell>
  void setMainCellAssociativity_TraceSplit(const TraceInfo& traceinfo, const int new_nodeDOF,
                                           ElementAssociativityConstructor<TopoD2, TopoCell>& DOFAssocCell0,
                                           ElementAssociativityConstructor<TopoD2, TopoCell>& DOFAssocCell1,
                                           ElementAssociativityConstructor<TopoD1, Line>& DOFAssocTraceMid,
                                           CanonicalTraceToCell& TraceMid_canonicalL,
                                           CanonicalTraceToCell& TraceMid_canonicalR);

  template <class TopoCell>
  void setCellAssociativity_TraceSplit(const int cell_group, const int cell_elem,
                                       const int trace_group, const int trace_elem, const int new_nodeDOF,
                                       ElementAssociativityConstructor<TopoD2, TopoCell>& DOFAssocCell0,
                                       ElementAssociativityConstructor<TopoD2, TopoCell>& DOFAssocCell1,
                                       ElementAssociativityConstructor<TopoD1, Line>& DOFAssocTrace0,
                                       ElementAssociativityConstructor<TopoD1, Line>& DOFAssocTrace1,
                                       ElementAssociativityConstructor<TopoD1, Line>& DOFAssocTraceMid,
                                       CanonicalTraceToCell& TraceMid_canonicalL,
                                       CanonicalTraceToCell& TraceMid_canonicalR);

  void setInteriorTraceAssociativity(int group, int elem, ElementAssociativityConstructor<TopoD1, Line>& DOFAssoc);
  void setBoundaryTraceAssociativity(int group, int elem, ElementAssociativityConstructor<TopoD1, Line>& DOFAssoc);
  void setGhostBoundaryTraceAssociativity(int group, int elem, ElementAssociativityConstructor<TopoD1, Line>& DOFAssoc);

  void
  process_OuterBTraces(const int neighbor_group, const int neighbor_elem, const int order, const int Ntrace,
                       const int local_neighbor_group, const int local_neighbor_elem,
                       const CanonicalTraceToCell& neighbor_canonicalTrace,
                       std::shared_ptr<typename BaseType::template FieldTraceGroupType<Line>
                                                        ::FieldAssociativityConstructorType> fldAssoc_Outer_BTrace,
                       const int OuterBTraceGroup, int& cnt_outer_Btraces);

  template <class TopoCell>
  void
  process_SplitOuterBTraces(const int neighbor_group, const int neighbor_elem, const int order, const int Ntrace,
                       const int local_neighbor_group, const int local_neighbor_elem,
                       const CanonicalTraceToCell& neighbor_canonicalTrace,
                       ElementAssociativityConstructor<TopoD2, TopoCell>& DOFAssocCell0,
                       ElementAssociativityConstructor<TopoD2, TopoCell>& DOFAssocCell1,
                       std::shared_ptr<typename BaseType::template FieldTraceGroupType<Line>
                                                        ::FieldAssociativityConstructorType> fldAssoc_Outer_BTrace,
                       const int OuterBTraceGroup, int& cnt_outer_Btraces);

  std::vector<int>
  getMainSubCellElements(const int trace_index);

};

}

#endif /* XFIELDAREA_LOCAL_SPLIT_LINEAR_H_ */
