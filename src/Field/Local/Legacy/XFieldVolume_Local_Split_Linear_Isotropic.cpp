// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XFieldVolume.h"
#include "XFieldVolume_Local_Split_Linear_Isotropic.h"

namespace SANS
{

template <>
std::vector<int>
getMainSubCellElements<Tet>(const int trace_index)
{

  if (trace_index==0)
    return {0,1,2,4};
  else if (trace_index==1)
    return {1,0,3,5};
  else if (trace_index==2)
    return {2,1,3,7};
  else if (trace_index==3)
    return {0,2,3,6};

  SANS_DEVELOPER_EXCEPTION("XField_Local_Split_Linear<PhysDim, TopoD3, T>::getMainSubCellElements<Tet> - "
                           "Invalid trace_index (=%d) for split configuration!", trace_index);
  return {-1};
}

template <>
std::vector<int>
getMainSubCellElements<Hex>(const int trace_index)
{
  SANS_DEVELOPER_EXCEPTION( "XField_Split_Local_Linear<PhysDim, TopoD3, T>::getMainSubCellElements<Hex> - Hex splitting not supported yet.");
  return {-1};
}

template <class PhysDim>
void
XField_Local_Split_Linear_Isotropic<PhysDim, TopoD3>::split(ElementSplitType split_type, int split_edge_index)
{
  //Clear current local grid
  this->deallocate();

  NodeDOFMap_.clear();
  this->newNodeDOFs_.clear();

  cntNodeDOF_ = 0;
  OuterBTraceGroup_Tri_ = -1;
  OuterBTraceGroup_Quad_ = -1;

  //Classify the split type
  if (split_type == ElementSplitType::Isotropic)
  {
    if (split_edge_index != -1)
      SANS_DEVELOPER_EXCEPTION("XField_Split_Local<PhysDim, TopoD3, T>::split - "
                               "Split edge index (=%d) should be -1 for an isotropic-split.", split_edge_index);
  }
  else
    SANS_DEVELOPER_EXCEPTION("XField_Split_Local<PhysDim, TopoD3, T>::split - Invalid ElementSplitType. Expected Isotropic split.");


  if ( xfld_local_unsplit_.getCellGroupBase(mainGroup_).topoTypeID() == typeid(Tet) )
  {
      splitMainCell<Tet, Triangle>();
  }
  else if ( xfld_local_unsplit_.getCellGroupBase(mainGroup_).topoTypeID() == typeid(Hex) )
  {
      SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD3, T>::splitMainCell - Hex splitting not supported yet." );
//      splitMainCell<Hex, Quad>();
  }
  else
    SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD3, T>::split - Unknown cell topology." );

  // This is checkGrid() but gets removed in release mode!
  this->checkGrid();
}


template <class PhysDim>
template <class TopoMainCell, class TopoMainTrace>
void
XField_Local_Split_Linear_Isotropic<PhysDim, TopoD3>::splitMainCell()
{

  typedef typename XField<PhysDim, TopoD3>::template FieldCellGroupType<TopoMainCell> XFieldCellGroupType;
  typedef typename XField<PhysDim, TopoD3>::template FieldTraceGroupType<TopoMainTrace> XFieldTraceGroupType;
  typedef typename XFieldCellGroupType::BasisType BasisType_Cell;
  typedef typename XFieldTraceGroupType::BasisType BasisType_Trace;

  std::pair<std::map<int,int>::iterator,bool> map_ret; //return value for map inserts

  const XFieldCellGroupType& cellgrp_main = xfld_local_unsplit_.template getCellGroup<TopoMainCell>(mainGroup_);

  //Get nodeDOF map from unsplit mesh
  int nodeDOF_map[TopoMainCell::NNode];
  cellgrp_main.associativity(mainElem_).getNodeGlobalMapping( nodeDOF_map, TopoMainCell::NNode );

  int order = 1; //We're constructing a linear split mesh
  const BasisType_Cell* cell_basis = BasisFunctionVolumeBase<TopoMainCell>::getBasisFunction(order, basisCategory_);
  const BasisType_Trace* trace_basis = BasisFunctionAreaBase<TopoMainTrace>::getBasisFunction(order, basisCategory_);

  nTetNeighbors_ = 0;
  nHexNeighbors_ = 0;
  nNeighbors_ = 0;

  nSplitTetNeighbors_ = 0;
  nSplitHexNeighbors_ = 0;

  //Count and map required DOFs from global to local mesh
  trackDOFs<TopoMainCell>(mainGroup_, mainElem_);

  if ( xfld_local_unsplit_.getCellGroupBase(mainGroup_).topoTypeID() == typeid(Tet) )
  {
      //Isotropic split
      //Add 6 new nodeDOFs to split the main cell
      for (int k = 0; k < Tet::NEdge; k++)
      {
        map_ret = NodeDOFMap_.insert( std::pair<int,int>(-1-k,cntNodeDOF_) );
        if (map_ret.second==true)
        {
          this->newNodeDOFs_.push_back(cntNodeDOF_);
          cntNodeDOF_++; //increment counter if object was actually added
        }
      }
  }
  else if ( xfld_local_unsplit_.getCellGroupBase(mainGroup_).topoTypeID() == typeid(Hex) )
  {
    SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD3, T>::splitMainCell - Hex splitting not supported yet." );
  }
  else
    SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD3, T>::splitMainCell - Unknown cell topology for neighbor." );


  //Initial loop across neighboring elements to find number of cells/traces/DOFs
  for (int i=0; i<TopoMainCell::NTrace; i++)
  {
    //Get indices of each neighboring cell (and its cellgroup)
    const TraceInfo& traceinfo = connectivity_.getTrace(mainGroup_, mainElem_, i);

    if (traceinfo.type == TraceInfo::Interior)
    {
      const XFieldTraceGroupType& tracegrp = xfld_local_unsplit_.template getInteriorTraceGroup<TopoMainTrace>(traceinfo.group);
      int neighbor_group = -1; //cellgroup of neighboring element
      int neighbor_elem = -1; //element index of neighboring element

      if (tracegrp.getElementLeft(traceinfo.elem) == mainElem_)
      {
        neighbor_group = tracegrp.getGroupRight(); //getGroupRight gives the neighbor's cellgroup
        neighbor_elem = tracegrp.getElementRight(traceinfo.elem);
      }
      else
        SANS_DEVELOPER_EXCEPTION("XField_Split_Local<PhysDim, TopoD3, T>::splitMainCell - "
                                 "The main-cell is not to the left of its neighbors in the unsplit local mesh.");

      if ( xfld_local_unsplit_.getCellGroupBase(neighbor_group).topoTypeID() == typeid(Tet) )
      {
        //Count and map required DOFs from global to local mesh
        trackDOFs<Tet>(neighbor_group, neighbor_elem);
        nTetNeighbors_++; //Increment tet neighbor count

        nTetNeighbors_ += 3; //The neighbor tet will be split into 4 tets (i.e. 3 new tets)
        nSplitTetNeighbors_ += 3;
      }
      else if ( xfld_local_unsplit_.getCellGroupBase(neighbor_group).topoTypeID() == typeid(Hex) )
      {
        //Count and map required DOFs from global to local mesh
        trackDOFs<Hex>(neighbor_group, neighbor_elem);
        nHexNeighbors_++; //Increment hex neighbor count
      }
      else
        SANS_DEVELOPER_EXCEPTION( "XField_Local<PhysDim, TopoD3, T>::extractLocalGrid - Unknown cell topology for neighbor." );
    }

  } // initial loop across traces of main cell


  //Count DOFs in local mesh
  int nDOF_local = (int)NodeDOFMap_.size();

#if 0
  std::cout << "nDOF_local: " << nDOF_local << std::endl;
  std::cout << "mapNode: " << NodeDOFMap_.size() <<std::endl;
  std::cout << "nNTetNeighbors: " << nTetNeighbors_ <<std::endl;
  std::cout << "nNHexNeighbors: " << nHexNeighbors_ <<std::endl;
#endif

  //Create local XField
  this->resizeDOF(nDOF_local);

  //Add up neighbor cell counts
  nNeighbors_ = nTetNeighbors_ + nHexNeighbors_;

  if (nNeighbors_>0)
  {
    if (nTetNeighbors_>0 && nHexNeighbors_==0) //Only tet neighbors
    {
      this->resizeCellGroups(2); //2 CellGroups - one for main cell, and one for all neighboring tets

      if (nSplitTetNeighbors_ > 0)
        this->resizeInteriorTraceGroups(3); //three groups :(main-to-tet-neighbor, main-main and tet-neighbor-neighbor interfaces)
      else
        this->resizeInteriorTraceGroups(2); //two groups :(main-to-tet-neighbor, main-main)
    }
    else if (nTetNeighbors_==0 && nHexNeighbors_>0) //Only hex neighbors
    {
      this->resizeCellGroups(2); //2 CellGroups - one for main cell, and one for all neighboring hexes

      if (nSplitHexNeighbors_ > 0)
        this->resizeInteriorTraceGroups(3); //three groups :(main-to-hex-neighbor, main-main and hex-neighbor-neighbor interfaces)
      else
        this->resizeInteriorTraceGroups(2); //two groups :(main-to-hex-neighbor, main-main)
    }
    else //Both kinds of neighbors
    {
      SANS_DEVELOPER_EXCEPTION( "XField_Local<PhysDim, TopoD3, T>::extractLocalGrid - Mixed meshes not supported yet." );
    }
  }
  else //No neighboring cells
  {
    this->resizeCellGroups(1); // one group for main cell
    this->resizeInteriorTraceGroups(1); //one group for interfaces between main cells
  }

  //a group each for each BC of main cell + one group for outer boundaries of neighboring cells
  nMainBTraceGroups_ = TopoMainCell::NTrace - (nNeighbors_ - nSplitTetNeighbors_ - nSplitHexNeighbors_);
  nOuterBTraceGroups_ = 1;
  if (nTetNeighbors_ > 0 && nHexNeighbors_ > 0)
    nOuterBTraceGroups_ = 2; //If this is a mixed mesh, we need 2 outer Btrace groups (for triangles/quads)

  if (nNeighbors_ == 0) nOuterBTraceGroups_ = 0; //If there are no neighbors - there is no outer boundary of the neighboring cells
  this->resizeBoundaryTraceGroups(nMainBTraceGroups_);
  this->resizeGhostBoundaryTraceGroups(nOuterBTraceGroups_);

  if (nOuterBTraceGroups_ > 0)
  {
    OuterBTraceGroup_Tri_ = 0;
    OuterBTraceGroup_Quad_ = 1;

    if (nTetNeighbors_ == 0) //Promote quad btraces if there are no tet neighbors
      OuterBTraceGroup_Quad_--;
  }

  ITraceGroupInterMain_ = 1;

  //Zero out mesh DOFs
  for (int k = 0; k < this->nDOF_; k++)
    this->DOF_[k] = 0.0;

  //Offsets for node DOFs in the global array
  //(cell DOFs first, face DOFs second, edge DOFs third, node DOFs last)
  offset_nodeDOF_ = 0;

  //-------------------------------------------------------------------------------------------------------------
  //Copy and fill associativities of neighboring cells, interior traces and boundary traces
  splitNeighbors(TopoMainCell::NTrace, order);

  //------------------------------Fill in associativity for main cells--------------------------------------------

  int nMainCells = 8;  //number of main sub-cells (for both tets and hexes)
  int nInterMainTraces = 2*TopoMainCell::NTrace; //number of interior traces between main sub-cells

  // create field associativity constructor for main cell
  typename XFieldCellGroupType::FieldAssociativityConstructorType fldAssocCell_Main( cell_basis, nMainCells );

  ElementAssociativityConstructor<TopoD3, TopoMainCell> DOFAssoc_MainCell( cell_basis );
  ElementAssociativityConstructor<TopoD2, TopoMainTrace> DOFAssoc_ITrace( trace_basis );

  std::vector< ElementAssociativityConstructor<TopoD3, TopoMainCell> > DOFAssocs_MainSubCell(nMainCells, DOFAssoc_MainCell);
  std::vector< ElementAssociativityConstructor<TopoD2, TopoMainTrace> > DOFAssocs_MidITrace(nInterMainTraces, DOFAssoc_ITrace);

  std::vector< CanonicalTraceToCell > TracesMid_canonicalL(nInterMainTraces);
  std::vector< CanonicalTraceToCell > TracesMid_canonicalR(nInterMainTraces);

  std::vector<int> new_nodeDOFs(TopoMainCell::NEdge);
  std::vector<int> left_elem_list(nInterMainTraces, -1);
  std::vector<int> right_elem_list(nInterMainTraces, -1);

  for (int i=0; i<TopoMainCell::NEdge; i++)
    new_nodeDOFs[i] = -i-1;

  setMainCellAssociativity_IsotropicSplit<TopoMainCell>(new_nodeDOFs,
                                                        DOFAssocs_MainSubCell, DOFAssocs_MidITrace,
                                                        TracesMid_canonicalL, TracesMid_canonicalR,
                                                        left_elem_list, right_elem_list);

  for (int i=0; i<nMainCells; i++)
  {
    fldAssocCell_Main.setAssociativity( DOFAssocs_MainSubCell[i], i );

    //All main sub-cells in split mesh map to the same main-cell in the unsplit mesh
    this->CellMapping_[{0,i}] = xfld_local_unsplit_.getGlobalCellMap({0,0});

    //Save split configurations (needed for solution transfer)
    ElementSplitInfo split_config(ElementSplitFlag::Split, ElementSplitType::Isotropic, -1, i);
    this->CellSplitInfo_[{0,i}] = split_config;
  }

  //Add cell group and copy DOFs for main cell
  this->cellGroups_[0] = new XFieldCellGroupType(fldAssocCell_Main);
  this->cellGroups_[0]->setDOF( this->DOF_, this->nDOF_ );
  this->nElem_ += fldAssocCell_Main.nElem();

  // create field associativity constructor for interior traces between main sub-cells
  std::shared_ptr<typename XFieldTraceGroupType::FieldAssociativityConstructorType> fldAssoc_ITrace_InterMain; //main-to-main

  fldAssoc_ITrace_InterMain = std::make_shared< typename XFieldTraceGroupType::FieldAssociativityConstructorType>
                                             ( trace_basis, nInterMainTraces);

  for (int itrace_mid = 0; itrace_mid < nInterMainTraces; itrace_mid++)
  {
    fldAssoc_ITrace_InterMain->setAssociativity( DOFAssocs_MidITrace[itrace_mid], itrace_mid );

    fldAssoc_ITrace_InterMain->setGroupLeft(mainGroup_);
    fldAssoc_ITrace_InterMain->setGroupRight(mainGroup_);
    fldAssoc_ITrace_InterMain->setElementLeft(left_elem_list[itrace_mid], itrace_mid);
    fldAssoc_ITrace_InterMain->setElementRight(right_elem_list[itrace_mid], itrace_mid);
    fldAssoc_ITrace_InterMain->setCanonicalTraceLeft (TracesMid_canonicalL[itrace_mid], itrace_mid);
    fldAssoc_ITrace_InterMain->setCanonicalTraceRight(TracesMid_canonicalR[itrace_mid], itrace_mid);

    this->InteriorTraceSplitInfo_[{ITraceGroupInterMain_,itrace_mid}] = ElementSplitInfo(ElementSplitFlag::New, itrace_mid);
  }

  //Add trace group and set DOFs for interior traces
  this->interiorTraceGroups_[ITraceGroupInterMain_] = new XFieldTraceGroupType( *fldAssoc_ITrace_InterMain );
  this->interiorTraceGroups_[ITraceGroupInterMain_]->setDOF( this->DOF_, this->nDOF_ );
}


template <class PhysDim>
void
XField_Local_Split_Linear_Isotropic<PhysDim, TopoD3>::splitNeighbors(int Ntrace, int order)
{
  //Volumes
  typedef typename XField<PhysDim, TopoD3>::template FieldCellGroupType<Tet> XFieldCellGroupType_Tet;
  typedef typename XField<PhysDim, TopoD3>::template FieldCellGroupType<Hex> XFieldCellGroupType_Hex;
  typedef typename XFieldCellGroupType_Tet::BasisType CellBasisType_Tet;
  typedef typename XFieldCellGroupType_Hex::BasisType CellBasisType_Hex;

  //Faces
  typedef typename XField<PhysDim, TopoD3>::template FieldTraceGroupType<Triangle> XFieldTraceGroupType_Tri;
  typedef typename XField<PhysDim, TopoD3>::template FieldTraceGroupType<Quad> XFieldTraceGroupType_Quad;
  typedef typename XFieldTraceGroupType_Tri::BasisType TraceBasisType_Tri;
  typedef typename XFieldTraceGroupType_Quad::BasisType TraceBasisType_Quad;

  const CellBasisType_Tet* cell_basis_Tet = NULL;
  const CellBasisType_Hex* cell_basis_Hex = NULL;

  const TraceBasisType_Tri* trace_basis_Tri = NULL;
  const TraceBasisType_Quad* trace_basis_Quad = NULL;

  if (nTetNeighbors_>0)
  {
    cell_basis_Tet = BasisFunctionVolumeBase<Tet>::getBasisFunction(order, basisCategory_);
    trace_basis_Tri = BasisFunctionAreaBase<Triangle>::getBasisFunction(order, basisCategory_);
  }
  if (nHexNeighbors_>0)
  {
    cell_basis_Hex = BasisFunctionVolumeBase<Hex>::getBasisFunction(order, basisCategory_);
    trace_basis_Quad = BasisFunctionAreaBase<Quad>::getBasisFunction(order, basisCategory_);
  }

  // create field associativity constructor for neighboring cells
  std::shared_ptr<typename XFieldCellGroupType_Tet::FieldAssociativityConstructorType> fldAssocCell_Neighbors_Tet; //tet neighbors
  std::shared_ptr<typename XFieldCellGroupType_Hex::FieldAssociativityConstructorType> fldAssocCell_Neighbors_Hex; //hex neighbors

  // create field associativity constructor for interior traces
  std::shared_ptr<typename XFieldTraceGroupType_Tri::FieldAssociativityConstructorType> fldAssoc_ITrace_Tri;
  std::shared_ptr<typename XFieldTraceGroupType_Quad::FieldAssociativityConstructorType> fldAssoc_ITrace_Quad;
  std::shared_ptr<typename XFieldTraceGroupType_Tri::FieldAssociativityConstructorType> fldAssoc_ITrace_InterTetNeighbor; //tet neighbor-to-neighbor
  std::shared_ptr<typename XFieldTraceGroupType_Quad::FieldAssociativityConstructorType> fldAssoc_ITrace_InterHexNeighbor; //hex neighbor-neighbor


  // create field associativity constructor for outer boundary traces of neighboring cells (3 faces for each neighboring tet, 5 for each quad)
  std::shared_ptr<typename XFieldTraceGroupType_Tri::FieldAssociativityConstructorType> fldAssoc_Outer_BTrace_Tri;
  std::shared_ptr<typename XFieldTraceGroupType_Quad::FieldAssociativityConstructorType> fldAssoc_Outer_BTrace_Quad;


  //Assign cell group indices to neighbors (index 0 is for main cell) in local mesh
  CellGroupTetNeighbors_ = 1;
  CellGroupHexNeighbors_ = 2;

  int ITraceGroupTetNeighbors = 0;
  int ITraceGroupHexNeighbors = 1;
  ITraceGroupInterMain_ = 2; //interfaces between split main-cells
  int ITraceGroupInterTetNeighbor = 3; //interfaces between split tet neighbor-cells
  int ITraceGroupInterHexNeighbor = 4; //interfaces between split hex neighbor-cells

  if (nNeighbors_ > 0)
  {
    fldAssocCell_Neighbors_Tet  = std::make_shared< typename XFieldCellGroupType_Tet::FieldAssociativityConstructorType>
                                                 ( cell_basis_Tet, nTetNeighbors_ );

    fldAssocCell_Neighbors_Hex  = std::make_shared< typename XFieldCellGroupType_Hex::FieldAssociativityConstructorType>
                                                 ( cell_basis_Hex, nHexNeighbors_ );

    fldAssoc_ITrace_Tri         = std::make_shared< typename XFieldTraceGroupType_Tri::FieldAssociativityConstructorType>
                                                 ( trace_basis_Tri, nTetNeighbors_);

    fldAssoc_ITrace_Quad        = std::make_shared< typename XFieldTraceGroupType_Quad::FieldAssociativityConstructorType>
                                                 ( trace_basis_Quad, nHexNeighbors_);

    fldAssoc_ITrace_InterTetNeighbor  = std::make_shared< typename XFieldTraceGroupType_Tri::FieldAssociativityConstructorType>
                                                 ( trace_basis_Tri, nSplitTetNeighbors_);

    fldAssoc_ITrace_InterHexNeighbor = std::make_shared< typename XFieldTraceGroupType_Quad::FieldAssociativityConstructorType>
                                                 ( trace_basis_Quad, nSplitHexNeighbors_);

    fldAssoc_Outer_BTrace_Tri   = std::make_shared< typename XFieldTraceGroupType_Tri::FieldAssociativityConstructorType>
                                                 ( trace_basis_Tri, (Tet::NTrace - 1)*nTetNeighbors_ - 2*nSplitTetNeighbors_);

    fldAssoc_Outer_BTrace_Quad  = std::make_shared< typename XFieldTraceGroupType_Quad::FieldAssociativityConstructorType>
                                                 ( trace_basis_Quad, (Hex::NTrace - 1)*nHexNeighbors_ - 2*nSplitHexNeighbors_);

    //if no tet neighbors, then promote the hex neighbor cellgroup and the interior trace group indices
    if (nTetNeighbors_==0)
    {
      CellGroupTetNeighbors_ = -1;
      ITraceGroupTetNeighbors = -1;
      ITraceGroupInterTetNeighbor = -1;

      CellGroupHexNeighbors_--;
      ITraceGroupHexNeighbors--;
      ITraceGroupInterMain_--;
      ITraceGroupInterHexNeighbor -= 2;
    }

    //if no hex neighbors, then promote the interior trace group indices
    if (nHexNeighbors_==0)
    {
      CellGroupHexNeighbors_ = -1;
      ITraceGroupHexNeighbors = -1;
      ITraceGroupInterHexNeighbor = -1;

      ITraceGroupInterMain_--;
      ITraceGroupInterTetNeighbor--;
    }

  }
  else //no neighbors
  {
    CellGroupTetNeighbors_ = -1;
    ITraceGroupTetNeighbors = -1;
    ITraceGroupInterTetNeighbor = -1;
    CellGroupHexNeighbors_ = -1;
    ITraceGroupHexNeighbors = -1;
    ITraceGroupInterHexNeighbor = -1;

    ITraceGroupInterMain_ = 0;
  }

  //counters to keep track of the neighbor cells which have been processed (includes splits)
  int cnt_neighbor_tet = 0;
//  int cnt_neighbor_hex = 0;

  //counters to keep track of how many neighbor cells have been split
  int cnt_neighbor_tet_split = 0;
//  int cnt_neighbor_hex_split = 0;

  int cnt_main_Btraces = 0; //counter to keep track of the boundary traces of the main cell which have been processed

  //counters to keep track of the outer boundary traces which have been processed (includes splits)
  int cnt_outer_Btraces_tri = 0;
//  int cnt_outer_Btraces_quad = 0;

  //counters to keep track of how many outer boundary traces have been split
  int cnt_outer_Btraces_tri_split = 0;
//  int cnt_outer_Btraces_quad_split = 0;

  //-----------Fill in associativity for neighboring cells--------------------

  //Repeat loop across neighboring elements to fill associativity
  for (int itrace=0; itrace< Ntrace; itrace++)
  {
    //Get indices of each neighboring trace (and its cellgroup)
    const TraceInfo& traceinfo = connectivity_.getTrace(mainGroup_, mainElem_, itrace);

    if (traceinfo.type == TraceInfo::Interior)
    {
      int neighbor_group = -1; //cellgroup of neighboring element in global mesh
      int neighbor_elem = -1; //element index of neighboring element in global mesh
      CanonicalTraceToCell neighbor_canonicalTrace; //canonicalTrace of the neighboring cell for the current trace
      CanonicalTraceToCell main_canonicalTrace; //canonicalTrace of the main cell for the current trace

      if (xfld_local_unsplit_.getInteriorTraceGroupBase(traceinfo.group).topoTypeID() == typeid(Triangle))
      {
        getNeighborInfoFromTrace<Triangle>(traceinfo.group, traceinfo.elem, mainElem_,
                                           neighbor_group, neighbor_elem,
                                           neighbor_canonicalTrace, main_canonicalTrace);
      }
      else if (xfld_local_unsplit_.getInteriorTraceGroupBase(traceinfo.group).topoTypeID() == typeid(Quad))
      {
        getNeighborInfoFromTrace<Quad>(traceinfo.group, traceinfo.elem, mainElem_,
                                       neighbor_group, neighbor_elem,
                                       neighbor_canonicalTrace, main_canonicalTrace);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "XField_Local<PhysDim, TopoD3, T>::extractLocalGrid - Unknown trace topology for interior trace." );


      if ( xfld_local_unsplit_.getCellGroupBase(neighbor_group).topoTypeID() == typeid(Tet) )
      {
        //Isotropic-split

        const int nSubCells = 4;  //number of sub-cells
        const int nSubTraces = 4; //number of interior traces between main and neighbor sub-cells
        const int nMidTraces = 3; //number of interior traces between neighbor sub-cells

        ElementAssociativityConstructor<TopoD3, Tet> DOFAssoc_SubCell( cell_basis_Tet );
        ElementAssociativityConstructor<TopoD2, Triangle> DOFAssoc_SubITrace( trace_basis_Tri );
        ElementAssociativityConstructor<TopoD2, Triangle> DOFAssoc_MidITrace( trace_basis_Tri );

        std::vector< ElementAssociativityConstructor<TopoD3, Tet> > DOFAssocs_SubCell(nSubCells, DOFAssoc_SubCell);
        std::vector< ElementAssociativityConstructor<TopoD2, Triangle> > DOFAssocs_SubITrace(nSubTraces, DOFAssoc_SubITrace);
        std::vector< ElementAssociativityConstructor<TopoD2, Triangle> > DOFAssocs_MidITrace(nMidTraces, DOFAssoc_MidITrace);

        std::vector< CanonicalTraceToCell > TracesSub_canonicalL(nSubTraces);
        std::vector< CanonicalTraceToCell > TracesSub_canonicalR(nSubTraces);
        std::vector< CanonicalTraceToCell > TracesMid_canonicalL(nMidTraces);
        std::vector< CanonicalTraceToCell > TracesMid_canonicalR(nMidTraces);

        setCellAssociativity_IsotropicSplit<Tet,Triangle>(neighbor_group, neighbor_elem,
                                                          traceinfo.group, traceinfo.elem, itrace,
                                                          DOFAssocs_SubCell, DOFAssocs_SubITrace, DOFAssocs_MidITrace,
                                                          TracesSub_canonicalL, TracesSub_canonicalR,
                                                          TracesMid_canonicalL, TracesMid_canonicalR);

        //Get the indices of the main sub-cells on the left based on split-configuration and trace index
        std::vector<int> main_subelemlist = getMainSubCellElements<Tet>(itrace);

        std::vector<int> subcell_indices = getIsotropicFaceSplitSubCellOrder<Tet,Triangle>(neighbor_group, neighbor_elem,
                                                                                           traceinfo.group, traceinfo.elem, itrace,
                                                                                           neighbor_canonicalTrace.trace);

        for (int i = 0; i < nSubCells; i++)
        {
          int sub_index = cnt_neighbor_tet + i;

          fldAssocCell_Neighbors_Tet->setAssociativity( DOFAssocs_SubCell[i], sub_index);

          //All sub-neighbor cells in split mesh map to the same element in the global mesh
          this->CellMapping_[{CellGroupTetNeighbors_, sub_index}] =
                xfld_local_unsplit_.getGlobalCellMap({CellGroupTetNeighbors_,(cnt_neighbor_tet - cnt_neighbor_tet_split)});

          this->CellSplitInfo_[{CellGroupTetNeighbors_,sub_index}] =
              ElementSplitInfo(ElementSplitFlag::Split, ElementSplitType::IsotropicFace, neighbor_canonicalTrace.trace, subcell_indices[i]);


          fldAssoc_ITrace_Tri->setAssociativity( DOFAssocs_SubITrace[i], sub_index );

          fldAssoc_ITrace_Tri->setElementLeft(main_subelemlist[i], sub_index);
          fldAssoc_ITrace_Tri->setElementRight(sub_index, sub_index);
          fldAssoc_ITrace_Tri->setCanonicalTraceLeft (TracesSub_canonicalL[i], sub_index);
          fldAssoc_ITrace_Tri->setCanonicalTraceRight(TracesSub_canonicalR[i], sub_index);

          //Save this interior trace's mapping from local to global mesh
          this->InteriorTraceMapping_[{ITraceGroupTetNeighbors, sub_index}] =
                xfld_local_unsplit_.getGlobalInteriorTraceMap({ITraceGroupTetNeighbors,(cnt_neighbor_tet - cnt_neighbor_tet_split)});

          this->InteriorTraceSplitInfo_[{ITraceGroupTetNeighbors,sub_index}] =
              ElementSplitInfo(ElementSplitFlag::Split, ElementSplitType::Isotropic, -1, i);
        }

        fldAssoc_ITrace_Tri->setGroupLeft(mainGroup_);
        fldAssoc_ITrace_Tri->setGroupRight(CellGroupTetNeighbors_);

        //Set DOF associativity for interior traces between neighbor-cells
        for (int itrace_mid = 0; itrace_mid < nMidTraces; itrace_mid++)
        {
          //itrace_mid_index is the number of neighbor tets split so far
          int itrace_mid_index = cnt_neighbor_tet_split + itrace_mid;
          int elemL_index = cnt_neighbor_tet + itrace_mid;
          int elemR_index = cnt_neighbor_tet + (nSubCells-1);

          fldAssoc_ITrace_InterTetNeighbor->setAssociativity( DOFAssocs_MidITrace[itrace_mid], itrace_mid_index );

          fldAssoc_ITrace_InterTetNeighbor->setElementLeft (elemL_index, itrace_mid_index);
          fldAssoc_ITrace_InterTetNeighbor->setElementRight(elemR_index, itrace_mid_index);
          fldAssoc_ITrace_InterTetNeighbor->setCanonicalTraceLeft (TracesMid_canonicalL[itrace_mid], itrace_mid_index);
          fldAssoc_ITrace_InterTetNeighbor->setCanonicalTraceRight(TracesMid_canonicalR[itrace_mid], itrace_mid_index);

          this->InteriorTraceSplitInfo_[{ITraceGroupInterTetNeighbor,itrace_mid_index}] = ElementSplitInfo(ElementSplitFlag::New, itrace_mid);
        }

        fldAssoc_ITrace_InterTetNeighbor->setGroupLeft(CellGroupTetNeighbors_);
        fldAssoc_ITrace_InterTetNeighbor->setGroupRight(CellGroupTetNeighbors_);

        //Add associativity for outer boundary traces of this neighbor cell
        process_OuterBTraces_IsotropicSplit<Tet,Triangle>(neighbor_group, neighbor_elem,
                                                          traceinfo.group, traceinfo.elem, itrace, order,
                                                          CellGroupTetNeighbors_, cnt_neighbor_tet, DOFAssocs_SubCell,
                                                          fldAssoc_Outer_BTrace_Tri, OuterBTraceGroup_Tri_,
                                                          cnt_outer_Btraces_tri, cnt_outer_Btraces_tri_split);

        cnt_neighbor_tet += nSubCells; //Added four (split)-neighbors
        cnt_neighbor_tet_split += (nSubCells-1); //increment tet-neighbor split count

      }
      else if ( xfld_local_unsplit_.getCellGroupBase(neighbor_group).topoTypeID() == typeid(Hex) )
      {
        SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD3, T>::splitNeighbors - Hex splitting not supported yet.");
      }
      else
        SANS_DEVELOPER_EXCEPTION( "XField_Local<PhysDim, TopoD3, T>::extractLocalGrid - Unknown cell topology for neighbor." );

    }
    else if (traceinfo.type == TraceInfo::Boundary) //We are dealing with a boundary trace of the main cell
    {
      if (xfld_local_unsplit_.getBoundaryTraceGroupBase(traceinfo.group).topoTypeID() == typeid(Triangle))
      {
        //Isotropic-split
        process_MainBTrace_IsotropicSplit<Tet,Triangle>(traceinfo.group, traceinfo.elem, order, itrace, cnt_main_Btraces);
      }
      else if (xfld_local_unsplit_.getBoundaryTraceGroupBase(traceinfo.group).topoTypeID() == typeid(Quad))
      {
        process_MainBTrace_IsotropicSplit<Hex,Quad>(traceinfo.group, traceinfo.elem, order, itrace, cnt_main_Btraces);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "XField_Local<PhysDim, TopoD3, T>::extractLocalGrid - Unknown trace topology for boundary trace." );
    }
  } //repeated loop across traces of main element

  if (nNeighbors_>0)
  {
    if (nTetNeighbors_>0) //Tet neighbors
    {
      //Add cell group and copy DOFs for neighboring cells
      this->cellGroups_[CellGroupTetNeighbors_] = new XFieldCellGroupType_Tet( *fldAssocCell_Neighbors_Tet );
      this->cellGroups_[CellGroupTetNeighbors_]->setDOF( this->DOF_, this->nDOF_ );
      this->nElem_ += fldAssocCell_Neighbors_Tet->nElem();

      //Add trace group and set DOFs for interior traces
      this->interiorTraceGroups_[ITraceGroupTetNeighbors] = new XFieldTraceGroupType_Tri( *fldAssoc_ITrace_Tri );
      this->interiorTraceGroups_[ITraceGroupTetNeighbors]->setDOF( this->DOF_, this->nDOF_ );
    }

    if (nHexNeighbors_>0) //Hex neighbors
    {
      //Add cell group and copy DOFs for neighboring cells
      this->cellGroups_[CellGroupHexNeighbors_] = new XFieldCellGroupType_Hex( *fldAssocCell_Neighbors_Hex );
      this->cellGroups_[CellGroupHexNeighbors_]->setDOF( this->DOF_, this->nDOF_ );
      this->nElem_ += fldAssocCell_Neighbors_Hex->nElem();

      //Add trace group and set DOFs for interior traces
      this->interiorTraceGroups_[ITraceGroupHexNeighbors] = new XFieldTraceGroupType_Quad( *fldAssoc_ITrace_Quad );
      this->interiorTraceGroups_[ITraceGroupHexNeighbors]->setDOF( this->DOF_, this->nDOF_ );
    }

    if (nSplitTetNeighbors_ + nSplitHexNeighbors_>0)
    {
      if (nSplitTetNeighbors_>0)
      {
        this->interiorTraceGroups_[ITraceGroupInterTetNeighbor] = new XFieldTraceGroupType_Tri( *fldAssoc_ITrace_InterTetNeighbor );
        this->interiorTraceGroups_[ITraceGroupInterTetNeighbor]->setDOF( this->DOF_, this->nDOF_ );
      }
      if (nSplitHexNeighbors_>0)
      {
        this->interiorTraceGroups_[ITraceGroupInterHexNeighbor] = new XFieldTraceGroupType_Quad( *fldAssoc_ITrace_InterHexNeighbor );
        this->interiorTraceGroups_[ITraceGroupInterHexNeighbor]->setDOF( this->DOF_, this->nDOF_ );
      }
    }

    if (nOuterBTraceGroups_>0)
    {
      //Add trace group and copy DOFs for interior traces
      if (nTetNeighbors_>0) //Tet neighbors
      {
        this->ghostBoundaryTraceGroups_[OuterBTraceGroup_Tri_] = new XFieldTraceGroupType_Tri( *fldAssoc_Outer_BTrace_Tri );
        this->ghostBoundaryTraceGroups_[OuterBTraceGroup_Tri_]->setDOF( this->DOF_, this->nDOF_ );
      }
      if (nHexNeighbors_>0) //Hex neighbors
      {
        this->ghostBoundaryTraceGroups_[OuterBTraceGroup_Quad_] = new XFieldTraceGroupType_Quad( *fldAssoc_Outer_BTrace_Quad );
        this->ghostBoundaryTraceGroups_[OuterBTraceGroup_Quad_]->setDOF( this->DOF_, this->nDOF_ );
      }
    }
  } //if neighbors

}

template <class PhysDim>
template <class TopoCell, class TopoTrace>
void
XField_Local_Split_Linear_Isotropic<PhysDim, TopoD3>::
setMainCellAssociativity_IsotropicSplit(
    const std::vector<int>& new_nodeDOFs,
    std::vector< ElementAssociativityConstructor<TopoD3, TopoCell> >& DOFAssocs_SubCell,
    std::vector< ElementAssociativityConstructor<TopoD2, TopoTrace> >& DOFAssocs_MidITrace,
    std::vector< CanonicalTraceToCell >& TracesMid_canonicalL,
    std::vector< CanonicalTraceToCell >& TracesMid_canonicalR,
    std::vector<int>& left_elem_list, std::vector<int>& right_elem_list )
{
  typedef typename XField<PhysDim, TopoD3>::template FieldCellGroupType<TopoCell> XFieldCellGroupType;
//  typedef typename XField<PhysDim, TopoD3>::template FieldTraceGroupType<TopoTrace> XFieldTraceGroupType;

  //Checks
  SANS_ASSERT_MSG((int)new_nodeDOFs.size() == TopoCell::NEdge,
                  "The number of new nodeDOFs (=%d) is not equal to the number of edges to be split of the main cell (=%d).",
                  (int)new_nodeDOFs.size(), TopoCell::NEdge);

  //Get cell/trace group from unsplit mesh
  const XFieldCellGroupType& cellgrp = xfld_local_unsplit_.template getCellGroup<TopoCell>(mainGroup_);

  const int nNodeDOFCell = TopoCell::NNode;
  const int nNodeDOFTrace = TopoTrace::NNode;

  int rank =  cellgrp.associativity(mainElem_).rank();

  int UnsplitCell_nodeDOF_map[nNodeDOFCell];
  cellgrp.associativity(mainElem_).getNodeGlobalMapping( UnsplitCell_nodeDOF_map, nNodeDOFCell ); //Get nodeDOF map from unsplit mesh

  if ( xfld_local_unsplit_.getCellGroupBase(mainGroup_).topoTypeID() == typeid(Tet) )
  {
    //Copy all nodeDOFs into one single vector
    int vlist[10];
    for (int k = 0; k < Tet::NNode; k++)
      vlist[k] = UnsplitCell_nodeDOF_map[k];

    for (int k = 0; k < Tet::NEdge; k++)
      vlist[k+Tet::NNode] = new_nodeDOFs[k];

    // Index table for each sub-tet
    const int subtets[8][4] = { {7, 6, 2, 4},
                                {8, 5, 4, 3},
                                {9, 1, 6, 5},
                                {0, 9, 7, 8},

                                {7, 6, 4, 5},
                                {8, 4, 5, 7},
                                {9, 5, 6, 7},
                                {9, 7, 8, 5} };

    //Node DOF ordering for sub-cells
    int subcell_nodeDOF_maps[8][nNodeDOFCell];

    //Map subcell nodeDOFs from unsplit mesh to split mesh
    for (int cell = 0; cell < 8; cell++)
    {
      for (int k = 0; k < nNodeDOFCell; k++)
      {
        subcell_nodeDOF_maps[cell][k] = vlist[subtets[cell][k]];
        subcell_nodeDOF_maps[cell][k] = offset_nodeDOF_ + NodeDOFMap_.at(subcell_nodeDOF_maps[cell][k]);
        DOFAssocs_SubCell[cell].setRank( rank );
        DOFAssocs_SubCell[cell].setNodeGlobalMapping( subcell_nodeDOF_maps[cell], nNodeDOFCell );
      }
    }

    for (int elem = 0; elem < 8; elem++)
      DOFAssocs_SubCell[elem].faceSign() = cellgrp.associativity(mainElem_).faceSign(); //Copy edge signs from original unsplit cell

    //Node DOF ordering for interior traces
    int ItraceMid_nodeDOF_maps[8][Triangle::NTrace];

    const int (*TraceNodes)[ Triangle::NTrace ] = TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes;

    int Itrace_elemLR_list[8][2] = {{0,4},{1,5},{2,6},{3,7},
                                    {4,5},{4,6},{5,7},{6,7}};

    int Itrace_faceLR_list[8][2] = {{2,3},{3,3},{1,3},{0,3},
                                    {1,0},{2,0},{1,0},{2,2}};

    for (int trace = 0; trace < 8; trace++)
    {
      int elemL = Itrace_elemLR_list[trace][0];
      int elemR = Itrace_elemLR_list[trace][1];

      int faceL = Itrace_faceLR_list[trace][0];
      int faceR = Itrace_faceLR_list[trace][1];

      for (int k = 0; k < nNodeDOFTrace; k++)
      {
        ItraceMid_nodeDOF_maps[trace][k] = vlist[subtets[elemL][TraceNodes[faceL][k]]];
        ItraceMid_nodeDOF_maps[trace][k] = offset_nodeDOF_ + NodeDOFMap_.at(ItraceMid_nodeDOF_maps[trace][k]);
      }

      DOFAssocs_MidITrace[trace].setRank( rank );
      DOFAssocs_MidITrace[trace].setNodeGlobalMapping( ItraceMid_nodeDOF_maps[trace], nNodeDOFTrace );

      left_elem_list[trace]  = elemL;
      right_elem_list[trace] = elemR;

      TracesMid_canonicalL[trace] = TraceToCellRefCoord<Triangle, TopoD3, Tet>
                                    ::getCanonicalTrace(ItraceMid_nodeDOF_maps[trace], 3, subcell_nodeDOF_maps[elemL], nNodeDOFCell);

      TracesMid_canonicalR[trace] = TraceToCellRefCoord<Triangle, TopoD3, Tet>
                                    ::getCanonicalTrace(ItraceMid_nodeDOF_maps[trace], 3, subcell_nodeDOF_maps[elemR], nNodeDOFCell);

      // face signs for elements (L is +, R is -)
      DOFAssocs_SubCell[elemL].setFaceSign( +1, faceL );
      DOFAssocs_SubCell[elemR].setFaceSign( TracesMid_canonicalR[trace].orientation, faceR );
    }
  }
  else if ( xfld_local_unsplit_.getCellGroupBase(mainGroup_).topoTypeID() == typeid(Hex) )
  {
    SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD3, T>::setMainCellAssociativity_IsotropicSplit - "
                              "Hex splitting not supported yet." );
  }
  else
    SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD3, T>::setMainCellAssociativity_IsotropicSplit - "
                              "Unknown cell topology for main-cell.");

}


template <class PhysDim>
template <class TopoCell, class TopoTrace>
void
XField_Local_Split_Linear_Isotropic<PhysDim, TopoD3>::
setCellAssociativity_IsotropicSplit(
    const int cell_group, const int cell_elem,
    const int trace_group, const int trace_elem, const int main_cell_trace,
    std::vector< ElementAssociativityConstructor<TopoD3, TopoCell> >& DOFAssocs_SubCell,
    std::vector< ElementAssociativityConstructor<TopoD2, TopoTrace> >& DOFAssocs_SubITrace,
    std::vector< ElementAssociativityConstructor<TopoD2, TopoTrace> >& DOFAssocs_MidITrace,
    std::vector< CanonicalTraceToCell >& TracesSub_canonicalL,
    std::vector< CanonicalTraceToCell >& TracesSub_canonicalR,
    std::vector< CanonicalTraceToCell >& TracesMid_canonicalL,
    std::vector< CanonicalTraceToCell >& TracesMid_canonicalR )
{
  typedef typename XField<PhysDim, TopoD3>::template FieldCellGroupType<TopoCell> XFieldCellGroupType;
  typedef typename XField<PhysDim, TopoD3>::template FieldTraceGroupType<TopoTrace> XFieldTraceGroupType;

  //Checks
//  SANS_ASSERT_MSG((int)new_nodeDOFs.size() == TopoCell::NEdge,
//                  "The number of new nodeDOFs (=%d) is not equal to the number of edges to be split of the main cell (=%d).",
//                  (int)new_nodeDOFs.size(), TopoCell::NEdge);

  //Get cell/trace group from unsplit mesh
  const XFieldCellGroupType& cellgrp = xfld_local_unsplit_.template getCellGroup<TopoCell>(cell_group);
  const XFieldTraceGroupType& tracegrp = xfld_local_unsplit_.template getInteriorTraceGroup<TopoTrace>(trace_group);

  const int nNodeDOFCell = TopoCell::NNode;
  const int nNodeDOFTrace = TopoTrace::NNode;

  int unsplit_trace_nodeDOF_map[nNodeDOFTrace]; //Node mapping for unsplit interior trace between main cell and neighbor cell
  tracegrp.associativity(trace_elem).getNodeGlobalMapping(unsplit_trace_nodeDOF_map, nNodeDOFTrace);

  int rank = cellgrp.associativity(cell_elem).rank();

  int unsplit_cell_nodeDOF_map[nNodeDOFCell];
  cellgrp.associativity(cell_elem).getNodeGlobalMapping(unsplit_cell_nodeDOF_map, nNodeDOFCell); //Get nodeDOF map from unsplit mesh

  int new_nodes[nNodeDOFTrace];
  for (int k=0; k<nNodeDOFTrace; k++)
    new_nodes[k] = -(TraceToCellRefCoord<TopoTrace, TopoD3, TopoCell>::TraceEdges[main_cell_trace][k] + 1);

  if ( xfld_local_unsplit_.getCellGroupBase(cell_group).topoTypeID() == typeid(Tet) )
  {
    //Copy all nodeDOFs into one single vector
    int vlist[2*Triangle::NNode];
    for (int k = 0; k< Triangle::NNode; k++)
    {
      vlist[k] = unsplit_trace_nodeDOF_map[k];
      vlist[Triangle::NNode + k] = new_nodes[k];
    }

    int outer_node = -1; // index of nodeDOF which is on the outer boundary on this neighboring cell
    for (int k = 0; k < nNodeDOFCell; k++)
    {
      if (unsplit_cell_nodeDOF_map[k]!=unsplit_trace_nodeDOF_map[0] &&
          unsplit_cell_nodeDOF_map[k]!=unsplit_trace_nodeDOF_map[1] &&
          unsplit_cell_nodeDOF_map[k]!=unsplit_trace_nodeDOF_map[2])
      {
        outer_node = unsplit_cell_nodeDOF_map[k];
        break;
      }
    }

    /* Subcell index within brackets, i.e (1) = subcell 1
     * Node index without brackets, i.e. 1 = node 1
     *
     *   2
     *   |\
     *   | \
     *   |  \
     *   |(1)\
     *   |    \
     *  4|-----\3
     *   |\    |\
     *   | \(3)| \
     *   |  \  |  \
     *   |(2)\ |(0)\
     *   |    \|    \
     *  0 -----------1
     *         5
     */

    //[subcell index][node index]
    int subtrace_corner_index_from[3][2] = {{2,0},{0,1},{1,2}}; //indices in vlist[] for unsplit trace nodes
    int subtrace_corner_index_to  [3][2] = {{3,5},{4,3},{5,4}}; //indices in vlist[] for new nodes

    //[trace of main-cell][node index]
    int subtrace_inner_index[4][3] = {{5,3,4},{5,3,4},{5,3,4},{4,5,3}}; //indices in vlist[] for new nodes

    //Node DOF ordering for sub-cells
    int subcell_nodeDOF_maps[4][nNodeDOFCell];
    int subtrace_nodeDOF_maps[4][nNodeDOFTrace];
    int midtrace_nodeDOF_maps[3][nNodeDOFTrace];

    //Map subcell nodeDOFs from unsplit mesh to split mesh for all sub-cells at the corners (i.e. indices 0,1,2)
    for (int subcell=0; subcell<3; subcell++)
    {
      //Get nodeDOF maps from unsplit mesh
      cellgrp.associativity(cell_elem).getNodeGlobalMapping( subcell_nodeDOF_maps[subcell], nNodeDOFCell );
      tracegrp.associativity(trace_elem).getNodeGlobalMapping(subtrace_nodeDOF_maps[subcell], nNodeDOFTrace);

      int from_node0 = subtrace_corner_index_from[subcell][0];
      int from_node1 = subtrace_corner_index_from[subcell][1];

      int to_node0 = subtrace_corner_index_to[subcell][0];
      int to_node1 = subtrace_corner_index_to[subcell][1];

      for (int k = 0; k < nNodeDOFCell; k++)
      {
        if (subcell_nodeDOF_maps[subcell][k] == vlist[from_node0])
          subcell_nodeDOF_maps[subcell][k] = vlist[to_node0];

        if (subcell_nodeDOF_maps[subcell][k] == vlist[from_node1])
          subcell_nodeDOF_maps[subcell][k] = vlist[to_node1];
      }

      subtrace_nodeDOF_maps[subcell][from_node0] = vlist[to_node0];
      subtrace_nodeDOF_maps[subcell][from_node1] = vlist[to_node1];

      //Temporarily fill nodeDOF map for interior traces between neighboring sub-cells - nodes not necessarily in the correct order.
      midtrace_nodeDOF_maps[subcell][0] = outer_node;
      midtrace_nodeDOF_maps[subcell][1] = vlist[to_node0];
      midtrace_nodeDOF_maps[subcell][2] = vlist[to_node1];
    }

    //Get nodeDOF map from unsplit mesh
    cellgrp.associativity(cell_elem).getNodeGlobalMapping( subcell_nodeDOF_maps[3], nNodeDOFCell );

    //Set nodeDOFs for the sub-cell at the center (index 3)
    for (int k = 0; k < nNodeDOFCell; k++)
    {
      if (subcell_nodeDOF_maps[3][k] == vlist[0])
        subcell_nodeDOF_maps[3][k] = vlist[3];

      if (subcell_nodeDOF_maps[3][k] == vlist[1])
        subcell_nodeDOF_maps[3][k] = vlist[4];

      if (subcell_nodeDOF_maps[3][k] == vlist[2])
        subcell_nodeDOF_maps[3][k] = vlist[5];
    }

    //Set the nodeDOFs for the inner sub-trace
    for (int k = 0; k < nNodeDOFTrace; k++)
      subtrace_nodeDOF_maps[3][k] = vlist[subtrace_inner_index[main_cell_trace][k]];


    //Map subcell and subtrace nodeDOFs from unsplit mesh to split mesh
    for (int subcell = 0; subcell < 4; subcell++)
    {
      for (int k = 0; k < nNodeDOFCell; k++)
      {
        subcell_nodeDOF_maps[subcell][k] = offset_nodeDOF_ + NodeDOFMap_.at(subcell_nodeDOF_maps[subcell][k]);
        DOFAssocs_SubCell[subcell].setRank( rank );
        DOFAssocs_SubCell[subcell].setNodeGlobalMapping( subcell_nodeDOF_maps[subcell], nNodeDOFCell );
      }

      for (int k = 0; k < nNodeDOFTrace; k++)
      {
        subtrace_nodeDOF_maps[subcell][k] = offset_nodeDOF_ + NodeDOFMap_.at(subtrace_nodeDOF_maps[subcell][k]);
        DOFAssocs_SubITrace[subcell].setRank( rank );
        DOFAssocs_SubITrace[subcell].setNodeGlobalMapping( subtrace_nodeDOF_maps[subcell], nNodeDOFTrace );
      }

      DOFAssocs_SubCell[subcell].faceSign() = cellgrp.associativity(cell_elem).faceSign(); //Copy edge signs from original unsplit cell

      //The canonicalL set here won't necessarily be correct for the inner sub-trace, so it is corrected below (after loop).
      TracesSub_canonicalL[subcell].set(main_cell_trace, +1);

      TracesSub_canonicalR[subcell] = TraceToCellRefCoord<Triangle, TopoD3, Tet>::
                                      getCanonicalTrace(subtrace_nodeDOF_maps[subcell], nNodeDOFTrace,
                                                        subcell_nodeDOF_maps[subcell], nNodeDOFCell);

      DOFAssocs_SubCell[subcell].setFaceSign(TracesSub_canonicalR[subcell].orientation, TracesSub_canonicalR[subcell].trace);
    }

    //The left canonical trace for the inner sub-traces between the main-cell and the neighbors, for each trace of the main-cell
    //CanonicalTraceL.trace for the inner sub-trace is not necessarily equal to (main_trace_index).
    int inner_subtrace_canonicalL[Tet::NTrace] = {0, 2, 1, 1};
    TracesSub_canonicalL[3].set(inner_subtrace_canonicalL[main_cell_trace], +1);

    for (int subcell = 0; subcell < 3; subcell++)
    {
      //Map subtrace nodeDOFs from unsplit mesh to split mesh
      for (int k = 0; k < nNodeDOFTrace; k++)
        midtrace_nodeDOF_maps[subcell][k] = offset_nodeDOF_ + NodeDOFMap_.at(midtrace_nodeDOF_maps[subcell][k]);

      //Find the canonical trace of the neighbor sub-cell, which matches the temporary node-ordering above
      TracesMid_canonicalL[subcell] = TraceToCellRefCoord<Triangle, TopoD3, Tet>::
                                      getCanonicalTrace(midtrace_nodeDOF_maps[subcell], nNodeDOFTrace,
                                                        subcell_nodeDOF_maps[subcell], nNodeDOFCell);

      //Once the canonicalL trace is found, we can get the interior-trace nodes in the correct order (i.e. canonical orientation to sub-cell)
      for (int k=0; k < nNodeDOFTrace; k++)
      {
        int ordered_index = TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes[TracesMid_canonicalL[subcell].trace][k];
        midtrace_nodeDOF_maps[subcell][k] = subcell_nodeDOF_maps[subcell][ordered_index];
      }

      TracesMid_canonicalL[subcell].orientation = 1; //CanonicalL orientation for outer neighbor sub-cell by design

      //Find the canonicalR trace for the inner sub-cell, for the correctly ordered interior-trace nodes
      TracesMid_canonicalR[subcell] = TraceToCellRefCoord<Triangle, TopoD3, Tet>::
                                      getCanonicalTrace(midtrace_nodeDOF_maps[subcell], nNodeDOFTrace,
                                                        subcell_nodeDOF_maps[3], nNodeDOFCell);

      DOFAssocs_MidITrace[subcell].setRank( rank );
      DOFAssocs_MidITrace[subcell].setNodeGlobalMapping( midtrace_nodeDOF_maps[subcell], nNodeDOFTrace );

      //Set the faceSign for the inner neighbor sub-cell (it's to the "right" of all other neighbor sub-cells)
      DOFAssocs_SubCell[3].setFaceSign(TracesMid_canonicalR[subcell].orientation, TracesMid_canonicalR[subcell].trace);
    }

  }
  else if ( xfld_local_unsplit_.getCellGroupBase(cell_group).topoTypeID() == typeid(Hex) )
  {
    SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD3, T>::setCellAssociativity_IsotropicSplit - "
                              "Hex splitting not supported yet." );
  }
  else
    SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD3, T>::setCellAssociativity_IsotropicSplit - "
                              "Unknown cell topology for main-cell.");
}

template <class PhysDim>
template <class TopoCell>
void
XField_Local_Split_Linear_Isotropic<PhysDim, TopoD3>::setCellAssociativity(int group, int elem,
                                                                           ElementAssociativityConstructor<TopoD3, TopoCell>& DOFAssoc)
{

  typedef typename XField<PhysDim, TopoD3>::template FieldCellGroupType<TopoCell> XFieldCellGroupType;

  //Get global cell group
  const XFieldCellGroupType& cellgrp_main = xfld_local_unsplit_.template getCellGroup<TopoCell>(group);

  int nNodeDOF = cellgrp_main.associativity(elem).nNode();

  // node DOF, edge DOF, face DOF and cell DOF ordering for cell
  std::vector<int> Cell_nodeDOF_map(nNodeDOF);

  cellgrp_main.associativity(elem).getNodeGlobalMapping( Cell_nodeDOF_map.data(), nNodeDOF ); //Get nodeDOF map from global field

  //Map DOFs from global to local mesh
  for (int k = 0; k < nNodeDOF; k++)
    Cell_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Cell_nodeDOF_map[k]);

  DOFAssoc.setRank( cellgrp_main.associativity(elem).rank() );
  DOFAssoc.setNodeGlobalMapping( Cell_nodeDOF_map );
  DOFAssoc.faceSign() = cellgrp_main.associativity(elem).faceSign();

}

template<class PhysDim>
template <class TopoTrace>
void
XField_Local_Split_Linear_Isotropic< PhysDim, TopoD3>::setInteriorTraceAssociativity(int group, int elem,
                                                                           ElementAssociativityConstructor<TopoD2, TopoTrace>& DOFAssoc)
{
  typedef typename XField<PhysDim, TopoD3>::template FieldTraceGroupType<TopoTrace> XFieldTraceGroupType;

  //Get global interior trace group
  const XFieldTraceGroupType& tracegrp = xfld_local_unsplit_.template getInteriorTraceGroup<TopoTrace>(group);

  int nNodeDOF = tracegrp.associativity(elem).nNode();

  // node DOF, edge DOF and face DOF ordering for interior trace
  std::vector<int> Trace_nodeDOF_map(nNodeDOF);

  tracegrp.associativity(elem).getNodeGlobalMapping( Trace_nodeDOF_map.data(), nNodeDOF ); //Get nodeDOF map from global field

  //Map DOFs from global to local mesh
  for (int k = 0; k < nNodeDOF; k++)
    Trace_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Trace_nodeDOF_map[k]);

  DOFAssoc.setRank( tracegrp.associativity(elem).rank() );
  DOFAssoc.setNodeGlobalMapping( Trace_nodeDOF_map );
}


template<class PhysDim>
template <class TopoTrace>
void
XField_Local_Split_Linear_Isotropic< PhysDim, TopoD3>::setBoundaryTraceAssociativity(int group, int elem,
                                                                           ElementAssociativityConstructor<TopoD2, TopoTrace>& DOFAssoc)
{
  typedef typename XField<PhysDim, TopoD3>::template FieldTraceGroupType<TopoTrace> XFieldTraceGroupType;

  //Get global boundary trace group
  const XFieldTraceGroupType& tracegrp = xfld_local_unsplit_.template getBoundaryTraceGroup<TopoTrace>(group);

  int nNodeDOF = tracegrp.associativity(elem).nNode();

  // node DOF, edge DOF and face DOF ordering for boundary trace
  std::vector<int> Trace_nodeDOF_map(nNodeDOF);

  tracegrp.associativity(elem).getNodeGlobalMapping( Trace_nodeDOF_map.data(), nNodeDOF ); //Get nodeDOF map from global field

  //Map DOFs from global to local mesh
  for (int k = 0; k < nNodeDOF; k++)
    Trace_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Trace_nodeDOF_map[k]);

  DOFAssoc.setRank( tracegrp.associativity(elem).rank() );
  DOFAssoc.setNodeGlobalMapping( Trace_nodeDOF_map );
}

template<class PhysDim>
template <class TopoCell, class TopoTrace>
void
XField_Local_Split_Linear_Isotropic< PhysDim, TopoD3>::process_MainBTrace_IsotropicSplit(const int trace_group, const int trace_elem, const int order,
                                                                                         const int main_cell_trace, int& cnt_main_btracegroups)
{
  typedef typename XField<PhysDim, TopoD3>::template FieldTraceGroupType<TopoTrace> XFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::BasisType TraceBasisType;

  const TraceBasisType* trace_basis = BasisFunctionAreaBase<TopoTrace>::getBasisFunction(order, basisCategory_);

  const XFieldTraceGroupType& tracegrp = xfld_local_unsplit_.template getBoundaryTraceGroup<TopoTrace>(trace_group);

  //node DOF ordering for sub-boundary traces
  const int nNodeDOFTrace = TopoTrace::NNode;
  int unsplit_trace_nodeDOF_map[nNodeDOFTrace];

  //Copy nodeDOFs from original unsplit boundary trace
  tracegrp.associativity(trace_elem).getNodeGlobalMapping( unsplit_trace_nodeDOF_map, nNodeDOFTrace );

  int nSubTraces = 4; //number of sub boundary traces (for both tets and hexes)

  ElementAssociativityConstructor<TopoD2, TopoTrace> DOFAssoc_SubBTrace( trace_basis );

  std::vector< ElementAssociativityConstructor<TopoD2, TopoTrace> > DOFAssocs_SubBTrace(nSubTraces, DOFAssoc_SubBTrace);
  std::vector< CanonicalTraceToCell > TracesSub_canonicalL(nSubTraces);

  //TODO: Fix templates in TraceToCellRefCoord to TopoTrace and TopoCell, when TraceEdges are available for Hexes.
  int new_nodes[nNodeDOFTrace];
  for (int k=0; k<nNodeDOFTrace; k++)
    new_nodes[k] = -(TraceToCellRefCoord<TopoTrace, TopoD3, TopoCell>::TraceEdges[main_cell_trace][k] + 1);

  if ( xfld_local_unsplit_.getCellGroupBase(mainGroup_).topoTypeID() == typeid(Tet) )
  {
    //Copy all nodeDOFs into one single vector
    int vlist[2*Triangle::NNode];
    for (int k=0; k<Triangle::NNode; k++)
    {
      vlist[k] = unsplit_trace_nodeDOF_map[k];
      vlist[Triangle::NNode + k] = new_nodes[k];
    }

    //[subcell index][node index]
    int subtrace_corner_index_from[3][2] = {{2,0},{0,1},{1,2}}; //indices in vlist[] for unsplit trace nodes
    int subtrace_corner_index_to  [3][2] = {{3,5},{4,3},{5,4}}; //indices in vlist[] for new nodes

    //[trace of main-cell][node index]
    int subtrace_inner_index[4][3] = {{5,3,4},{5,3,4},{5,3,4},{4,5,3}}; //indices in vlist[] for new nodes

    //Node DOF ordering for sub-cells
    int subtrace_nodeDOF_maps[4][nNodeDOFTrace];

    //Map subcell nodeDOFs from unsplit mesh to split mesh for all sub-cells at the corners (i.e. indices 0,1,2)
    for (int subcell = 0; subcell < 3; subcell++)
    {
      //Get nodeDOF maps from unsplit mesh
      tracegrp.associativity(trace_elem).getNodeGlobalMapping(subtrace_nodeDOF_maps[subcell], nNodeDOFTrace);

      int from_node0 = subtrace_corner_index_from[subcell][0];
      int from_node1 = subtrace_corner_index_from[subcell][1];

      int to_node0 = subtrace_corner_index_to[subcell][0];
      int to_node1 = subtrace_corner_index_to[subcell][1];

      subtrace_nodeDOF_maps[subcell][from_node0] = vlist[to_node0];
      subtrace_nodeDOF_maps[subcell][from_node1] = vlist[to_node1];
    }

    //Set the nodeDOFs for the inner sub-trace
    for (int k = 0; k < Triangle::NNode; k++)
      subtrace_nodeDOF_maps[3][k] = vlist[subtrace_inner_index[main_cell_trace][k]];

    //Map subcell and subtrace nodeDOFs from unsplit mesh to split mesh
    for (int subcell = 0; subcell < 4; subcell++)
    {
      for (int k = 0; k < nNodeDOFTrace; k++)
      {
        subtrace_nodeDOF_maps[subcell][k] = offset_nodeDOF_ + NodeDOFMap_.at(subtrace_nodeDOF_maps[subcell][k]);
        DOFAssocs_SubBTrace[subcell].setRank( tracegrp.associativity(trace_elem).rank() );
        DOFAssocs_SubBTrace[subcell].setNodeGlobalMapping( subtrace_nodeDOF_maps[subcell], nNodeDOFTrace );
      }

      //The canonicalL set here won't necessarily be correct for the inner sub-trace, so it is corrected below (after loop).
      TracesSub_canonicalL[subcell].set(main_cell_trace, +1);
    }

    //The left canonical trace for the inner sub-traces between the main-cell and the neighbors, for each trace of the main-cell
    //CanonicalTraceL.trace for the inner sub-trace is not necessarily equal to (main_cell_trace).
    int inner_subtrace_canonicalL[Tet::NTrace] = {0, 2, 1, 1};
    TracesSub_canonicalL[3].set(inner_subtrace_canonicalL[main_cell_trace], +1);

  }
  else if ( xfld_local_unsplit_.getCellGroupBase(mainGroup_).topoTypeID() == typeid(Hex) )
  {
    SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD3, T>::process_MainBTrace_IsotropicSplit - "
                              "Hex splitting not supported yet." );
  }
  else
    SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD3, T>::process_MainBTrace_IsotropicSplit - "
                              "Unknown cell topology for main-cell.");


  // create field associativity constructor for each boundary trace of main cell
  // Note: each boundary trace of the main cell has its own boundaryTraceGroup!
  typename XFieldTraceGroupType::FieldAssociativityConstructorType fldAssoc_Main_BTrace( trace_basis, nSubTraces);

  //Get the indices of the main sub-cell on the left based on split-configuration and trace index
  std::vector<int> main_subelemlist = getMainSubCellElements<TopoCell>(main_cell_trace);

  fldAssoc_Main_BTrace.setGroupLeft(mainGroup_); //Cell group on left is the main cell's group = 0

  //Set associativity
  for (int subtrace = 0; subtrace < nSubTraces; subtrace++)
  {
    fldAssoc_Main_BTrace.setAssociativity( DOFAssocs_SubBTrace[subtrace], subtrace);

    //Save this boundary trace's mapping from local to global mesh
    this->BoundaryTraceMapping_[{cnt_main_btracegroups,subtrace}] = xfld_local_unsplit_.getGlobalBoundaryTraceMap({cnt_main_btracegroups,0});

    this->BoundaryTraceSplitInfo_[{cnt_main_btracegroups,subtrace}] =
        ElementSplitInfo(ElementSplitFlag::Split, ElementSplitType::Isotropic, -1, subtrace);

    fldAssoc_Main_BTrace.setElementLeft(main_subelemlist[subtrace], subtrace);
    fldAssoc_Main_BTrace.setCanonicalTraceLeft(TracesSub_canonicalL[subtrace], subtrace);
  }

  //Add trace group and copy DOFs for boundary traces of main cell
  this->boundaryTraceGroups_[cnt_main_btracegroups] = new XFieldTraceGroupType( fldAssoc_Main_BTrace );
  this->boundaryTraceGroups_[cnt_main_btracegroups]->setDOF( this->DOF_, this->nDOF_ );
  cnt_main_btracegroups++;
}

template<class PhysDim>
template <class TopoCell, class TopoTrace>
void
XField_Local_Split_Linear_Isotropic< PhysDim, TopoD3>::
process_OuterBTraces_IsotropicSplit(
    const int neighbor_group, const int neighbor_elem,
    const int trace_group, const int trace_elem, const int main_cell_trace,
    const int order, const int local_neighbor_group, const int local_neighbor_elem,
    const std::vector< ElementAssociativityConstructor<TopoD3, TopoCell> >& DOFAssocs_SubCell,
    std::shared_ptr<typename BaseType::template FieldTraceGroupType<TopoTrace>
                      ::FieldAssociativityConstructorType> fldAssoc_Outer_BTrace,
    const int OuterBTraceGroup, int& cnt_outer_Btraces, int& cnt_outer_Btraces_split)
{
  typedef typename XField<PhysDim, TopoD3>::template FieldCellGroupType<TopoCell> XFieldCellGroupType;
  typedef typename XField<PhysDim, TopoD3>::template FieldTraceGroupType<TopoTrace> XFieldTraceGroupType;

  typedef typename XFieldTraceGroupType::BasisType TraceBasisType;

  const TraceBasisType* trace_basis = BasisFunctionAreaBase<TopoTrace>::getBasisFunction(order, basisCategory_);

  //Get cell/trace group from unsplit mesh
  const XFieldCellGroupType& cellgrp = xfld_local_unsplit_.template getCellGroup<TopoCell>(neighbor_group);
  const XFieldTraceGroupType& tracegrp = xfld_local_unsplit_.template getInteriorTraceGroup<TopoTrace>(trace_group);

  const int nNodeDOFCell = TopoCell::NNode;
  const int nNodeDOFTrace = TopoTrace::NNode;

  int rankTrace = tracegrp.associativity(trace_elem).rank();

  int unsplit_trace_nodeDOF_map[nNodeDOFTrace]; //Node mapping for unsplit interior trace between main cell and neighbor cell
  tracegrp.associativity(trace_elem).getNodeGlobalMapping(unsplit_trace_nodeDOF_map, nNodeDOFTrace);

  int unsplit_cell_nodeDOF_map[nNodeDOFCell];
  cellgrp.associativity(neighbor_elem).getNodeGlobalMapping( unsplit_cell_nodeDOF_map, nNodeDOFCell ); //Get nodeDOF map from unsplit mesh

  int new_nodes[nNodeDOFTrace];
  for (int k=0; k<nNodeDOFTrace; k++)
    new_nodes[k] = -(TraceToCellRefCoord<TopoTrace, TopoD3, TopoCell>::TraceEdges[main_cell_trace][k] + 1);

  if ( xfld_local_unsplit_.getCellGroupBase(neighbor_group).topoTypeID() == typeid(Tet) )
  {
    int outer_node = -1; // index of nodeDOF which is on the outer boundary on this neighboring cell
    for (int k = 0; k < nNodeDOFCell; k++)
    {
      if (unsplit_cell_nodeDOF_map[k]!=unsplit_trace_nodeDOF_map[0] &&
          unsplit_cell_nodeDOF_map[k]!=unsplit_trace_nodeDOF_map[1] &&
          unsplit_cell_nodeDOF_map[k]!=unsplit_trace_nodeDOF_map[2])
      {
        outer_node = unsplit_cell_nodeDOF_map[k];
        break;
      }
    }

    int subcell_index[3][2] = {{0,1},{1,2},{2,0}}; //[outer btrace of original neighbor cell][sub-cell index]

    for (int j=0; j<Triangle::NTrace; j++) //Loop across outer boundary traces of neighbor cell
    {
      int unsplit_edgeDOF_map[Line::NNode];
      int BTrace0_nodes_tmp[nNodeDOFTrace], BTrace1_nodes_tmp[nNodeDOFTrace], BTrace_unsplit_nodes_tmp[nNodeDOFTrace];
      int BTrace0_nodes_L[nNodeDOFTrace], BTrace1_nodes_L[nNodeDOFTrace], BTrace_unsplit_nodes_L[nNodeDOFTrace];

      unsplit_edgeDOF_map[0] = unsplit_trace_nodeDOF_map[TraceToCellRefCoord<Line, TopoD2, Triangle>::TraceNodes[j][0]];
      unsplit_edgeDOF_map[1] = unsplit_trace_nodeDOF_map[TraceToCellRefCoord<Line, TopoD2, Triangle>::TraceNodes[j][1]];

      //Temporarily fill nodeDOF map for split outer boundary traces - nodes not necessarily in the correct order.
      BTrace0_nodes_tmp[0] = outer_node;
      BTrace0_nodes_tmp[1] = unsplit_edgeDOF_map[0];
      BTrace0_nodes_tmp[2] = new_nodes[j];

      BTrace1_nodes_tmp[0] = outer_node;
      BTrace1_nodes_tmp[1] = new_nodes[j];
      BTrace1_nodes_tmp[2] = unsplit_edgeDOF_map[1];

      BTrace_unsplit_nodes_tmp[0] = outer_node;
      BTrace_unsplit_nodes_tmp[1] = unsplit_edgeDOF_map[0];
      BTrace_unsplit_nodes_tmp[2] = unsplit_edgeDOF_map[1];

      //Map trace nodeDOFs from unsplit mesh to split mesh
      for (int k=0; k < nNodeDOFTrace; k++)
      {
        BTrace0_nodes_tmp[k] = offset_nodeDOF_ + NodeDOFMap_.at(BTrace0_nodes_tmp[k]);
        BTrace1_nodes_tmp[k] = offset_nodeDOF_ + NodeDOFMap_.at(BTrace1_nodes_tmp[k]);
      }

      int subcell0_nodes[nNodeDOFCell], subcell1_nodes[nNodeDOFCell];
      DOFAssocs_SubCell[subcell_index[j][0]].getNodeGlobalMapping(subcell0_nodes, nNodeDOFCell);
      DOFAssocs_SubCell[subcell_index[j][1]].getNodeGlobalMapping(subcell1_nodes, nNodeDOFCell);

      CanonicalTraceToCell outer_canonicalTrace_0 = TraceToCellRefCoord<Triangle, TopoD3, Tet>::
                                                    getCanonicalTraceLeft(BTrace0_nodes_tmp, nNodeDOFTrace, subcell0_nodes, nNodeDOFCell,
                                                                          BTrace0_nodes_L, nNodeDOFTrace);

      CanonicalTraceToCell outer_canonicalTrace_1 = TraceToCellRefCoord<Triangle, TopoD3, Tet>::
                                                    getCanonicalTraceLeft(BTrace1_nodes_tmp, nNodeDOFTrace, subcell1_nodes, nNodeDOFCell,
                                                                          BTrace1_nodes_L, nNodeDOFTrace);

      //Get the nodes of the original unsplit outer btrace in the right order
      TraceToCellRefCoord<Triangle, TopoD3, Tet>::getCanonicalTraceLeft(BTrace_unsplit_nodes_tmp, nNodeDOFTrace,
                                                                        unsplit_cell_nodeDOF_map, nNodeDOFCell,
                                                                        BTrace_unsplit_nodes_L, nNodeDOFTrace);

      //Get the canonical trace of outer btrace for the edge that is being split
//      CanonicalTraceToCell splitEdge_canonicalTrace = TraceToCellRefCoord<Line, TopoD2, Triangle>::getCanonicalTrace(
//                                                           unsplit_edgeDOF_map,Line::NNode,
//                                                           BTrace_unsplit_nodes_L, nNodeDOFTrace);

      //DOF associativity for boundary traces of neighbors
      ElementAssociativityConstructor<TopoD2, Triangle> DOFAssoc_Outer_BTrace0( trace_basis );
      ElementAssociativityConstructor<TopoD2, Triangle> DOFAssoc_Outer_BTrace1( trace_basis );

      DOFAssoc_Outer_BTrace0.setRank( rankTrace );
      DOFAssoc_Outer_BTrace1.setRank( rankTrace );

      DOFAssoc_Outer_BTrace0.setNodeGlobalMapping( BTrace0_nodes_L, nNodeDOFTrace );
      DOFAssoc_Outer_BTrace1.setNodeGlobalMapping( BTrace1_nodes_L, nNodeDOFTrace );

      int index_subtrace0 = cnt_outer_Btraces;
      int index_subtrace1 = cnt_outer_Btraces + 1;

//      //Save this boundary trace's mapping from local to global mesh
//      this->BoundaryTraceMapping_[{OuterBTraceGroup,index_subtrace0}] =
//            xfld_local_unsplit_.getGlobalBoundaryTraceMap({OuterBTraceGroup,(cnt_outer_Btraces-cnt_outer_Btraces_split)});
//
//      this->BoundaryTraceMapping_[{OuterBTraceGroup,index_subtrace1}] =
//            xfld_local_unsplit_.getGlobalBoundaryTraceMap({OuterBTraceGroup,(cnt_outer_Btraces-cnt_outer_Btraces_split)});
//
//
//      if (splitEdge_canonicalTrace.orientation == 1)
//      {
//        this->BoundaryTraceSplitInfo_[{OuterBTraceGroup,index_subtrace0}] =
//            ElementSplitInfo(ElementSplitFlag::Split, ElementSplitType::Edge, splitEdge_canonicalTrace.trace, 0);
//
//        this->BoundaryTraceSplitInfo_[{OuterBTraceGroup,index_subtrace1}] =
//            ElementSplitInfo(ElementSplitFlag::Split, ElementSplitType::Edge, splitEdge_canonicalTrace.trace, 1);
//      }
//      else //Edge is in reverse-direction (so swap subcell indices)
//      {
//        this->BoundaryTraceSplitInfo_[{OuterBTraceGroup,index_subtrace0}] =
//            ElementSplitInfo(ElementSplitFlag::Split, ElementSplitType::Edge, splitEdge_canonicalTrace.trace, 1);
//
//        this->BoundaryTraceSplitInfo_[{OuterBTraceGroup,index_subtrace1}] =
//            ElementSplitInfo(ElementSplitFlag::Split, ElementSplitType::Edge, splitEdge_canonicalTrace.trace, 0);
//      }

      //Fill associativity of Outer BoundaryTraceGroup
      fldAssoc_Outer_BTrace->setAssociativity( DOFAssoc_Outer_BTrace0, index_subtrace0);
      fldAssoc_Outer_BTrace->setAssociativity( DOFAssoc_Outer_BTrace1, index_subtrace1);

      //Cell group on left is the neighbor cells' group
      fldAssoc_Outer_BTrace->setGroupLeft(local_neighbor_group);

      //Cell on left is a sub-cell of the split neighbor cell
      fldAssoc_Outer_BTrace->setElementLeft(local_neighbor_elem + subcell_index[j][0], index_subtrace0);
      fldAssoc_Outer_BTrace->setElementLeft(local_neighbor_elem + subcell_index[j][1], index_subtrace1);
      fldAssoc_Outer_BTrace->setCanonicalTraceLeft(outer_canonicalTrace_0, index_subtrace0);
      fldAssoc_Outer_BTrace->setCanonicalTraceLeft(outer_canonicalTrace_1, index_subtrace1);

      cnt_outer_Btraces += 2;
      cnt_outer_Btraces_split++; //increment split outer btrace count
    }
  }
  else if ( xfld_local_unsplit_.getCellGroupBase(neighbor_group).topoTypeID() == typeid(Hex) )
  {
    SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD3, T>::process_OuterBTraces_IsotropicSplit - "
                              "Hex splitting not supported yet." );
  }
  else
    SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD3, T>::process_OuterBTraces_IsotropicSplit - "
                              "Unknown cell topology for main-cell.");
}

template <class PhysDim>
template <class TopoCell, class TopoTrace>
std::vector<int>
XField_Local_Split_Linear_Isotropic<PhysDim, TopoD3>::getIsotropicFaceSplitSubCellOrder(const int cell_group, const int cell_elem,
                                                                              const int trace_group, const int trace_elem,
                                                                              const int main_cell_trace, const int neighbor_cell_trace)
{
  typedef typename XField<PhysDim, TopoD3>::template FieldCellGroupType<TopoCell> XFieldCellGroupType;
  typedef typename XField<PhysDim, TopoD3>::template FieldTraceGroupType<TopoTrace> XFieldTraceGroupType;

  //Get cell/trace group from unsplit mesh
  const XFieldCellGroupType& cellgrp = xfld_local_unsplit_.template getCellGroup<TopoCell>(cell_group);
  const XFieldTraceGroupType& tracegrp = xfld_local_unsplit_.template getInteriorTraceGroup<TopoTrace>(trace_group);

  const int nNodeDOFCell = TopoCell::NNode;
  const int nNodeDOFTrace = TopoTrace::NNode;

  int trace_nodeDOF_map[nNodeDOFTrace]; //Node mapping for unsplit interior trace between main cell and neighbor cell
  tracegrp.associativity(trace_elem).getNodeGlobalMapping(trace_nodeDOF_map, nNodeDOFTrace);

  int cell_nodeDOF_map[nNodeDOFCell];
  cellgrp.associativity(cell_elem).getNodeGlobalMapping(cell_nodeDOF_map, nNodeDOFCell); //Get nodeDOF map from unsplit mesh

  std::vector<int> subcell_order;

  if ( xfld_local_unsplit_.getCellGroupBase(cell_group).topoTypeID() == typeid(Tet) )
  {
    subcell_order.resize(4); //Neighbor cell is split into 4 sub-cells

    //Mapping from canonical edges of each triangle face in the tet, to the nodes of the tet
    const int edge_nodemap[Tet::NTrace][Triangle::NTrace][2] = { {{2,3},{3,1},{1,2}},
                                                                 {{3,2},{2,0},{0,3}},
                                                                 {{1,3},{3,0},{0,1}},
                                                                 {{2,1},{1,0},{0,2}} };

    //Order of the subcells in a triangle, by going in the reverse direction of each canonical edge
    //Going in reverse because the main-subcells are to the left of the interior trace, and neighbor-subcells are to the right.
    const int subcell_map[Triangle::NTrace][4] = {{1,0,2,3},{2,1,0,3},{0,2,1,3}};

    //Get the nodes of edge 0 of the main-cell's face (orientation = +1)
    int node0 = trace_nodeDOF_map[TraceToCellRefCoord<Line, TopoD2, Triangle>::TraceNodes[0][0]];
    int node1 = trace_nodeDOF_map[TraceToCellRefCoord<Line, TopoD2, Triangle>::TraceNodes[0][1]];

    for (int edge=0; edge<Triangle::NTrace; edge++) //Loop across edges of neighbor cell's face
    {
      //Check for reverse-ordering since main-cell face is left, and neighbor-cell face is right.
      if ( node1 == cell_nodeDOF_map[edge_nodemap[neighbor_cell_trace][edge][0]] &&
           node0 == cell_nodeDOF_map[edge_nodemap[neighbor_cell_trace][edge][1]])
      {
        for (int k=0; k<4; k++)
          subcell_order[k] = subcell_map[edge][k];

        return subcell_order;
      }
    }

    SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD3, T>::getIsotropicFaceSplitSubCellOrder - "
                               "No matching node-ordering for neighbor cell!" );
  }
  else if ( xfld_local_unsplit_.getCellGroupBase(cell_group).topoTypeID() == typeid(Hex) )
  {
    SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD3, T>::getIsotropicFaceSplitSubCellOrder - Hex splitting not supported yet.");
  }
  else
    SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD3, T>::getIsotropicFaceSplitSubCellOrder - Unknown cell topology." );

  return {-1};
}

template<class PhysDim>
template <class TopoTrace>
void
XField_Local_Split_Linear_Isotropic< PhysDim, TopoD3>::getNeighborInfoFromTrace(const int trace_group, const int trace_elem, const int main_elem,
                                                         int& neighbor_group, int& neighbor_elem,
                                                         CanonicalTraceToCell& neighbor_canonicalTrace, CanonicalTraceToCell& main_canonicalTrace)
{
  typedef typename XField<PhysDim, TopoD3>::template FieldTraceGroupType<TopoTrace> XFieldTraceGroupType;
  const XFieldTraceGroupType& tracegrp = xfld_local_unsplit_.template getInteriorTraceGroup<TopoTrace>(trace_group);

  if (tracegrp.getElementLeft(trace_elem) == main_elem)
  { //element to left of trace is the "main_elem" => getElementRight gives the neighboring cell
    neighbor_group          = tracegrp.getGroupRight();
    neighbor_elem           = tracegrp.getElementRight(trace_elem);
    neighbor_canonicalTrace = tracegrp.getCanonicalTraceRight(trace_elem);
    main_canonicalTrace     = tracegrp.getCanonicalTraceLeft(trace_elem);
  }
  else
    SANS_DEVELOPER_EXCEPTION("XField_Split_Local<PhysDim, TopoD3, T>::getNeighborInfoFromTrace - "
                             "The main-cell is not to the left of its neighbors in the unsplit local mesh.");
}

template <class PhysDim>
template <class TopoCell>
void
XField_Local_Split_Linear_Isotropic<PhysDim, TopoD3>::trackDOFs(int group, int elem)
{
  typedef typename XField<PhysDim, TopoD3>::template FieldCellGroupType<TopoCell> XFieldCellGroupType;

  std::pair<std::map<int,int>::iterator,bool> map_ret; //return value for map inserts

  const XFieldCellGroupType& cellgrp = xfld_local_unsplit_.template getCellGroup<TopoCell>(group);

  // node DOF map
  const int nNodeDOF = TopoCell::NNode;
  int nodeMap[nNodeDOF];

  //Save DOF mappings from global mesh to local mesh
  cellgrp.associativity(elem).getNodeGlobalMapping( nodeMap, nNodeDOF );
  for (int k = 0; k < nNodeDOF; k++)
  {
    map_ret = NodeDOFMap_.insert( std::pair<int,int>(nodeMap[k],cntNodeDOF_) );
    if (map_ret.second==true) cntNodeDOF_++; //increment counter if object was actually added
  }

}

//Explicit instantiations
template class XField_Local_Split_Linear_Isotropic<PhysD3,TopoD3>;

}
