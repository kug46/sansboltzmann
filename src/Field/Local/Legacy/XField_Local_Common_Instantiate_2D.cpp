// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define XFIELD_LOCAL_COMMON_INSTANTIATE

#include "XFieldArea.h"
#include "Field/Element/ElementAssociativityLine.h"
#include "Field/Element/ElementAssociativityArea.h"
#include "XField_Local_Common.h"
/*
  Helper functions for the construction of the local grids, reduces code duplication when constructing
*/

namespace SANS
{


// THIS IS TOPOD2 specific
template <class PhysDim, class TopoDim>
template <class TopologyCell, class TopologyTrace>
void
XField_Local_Common<PhysDim, TopoDim>::split( const int startNode, const int endNode, const int newNode,
                                              ConstructorVectorTopo<TopoDim,TopologyCell,TopologyTrace>& cV )
{

  typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::BasisType TraceBasisType;

  GroupTraceConstructorVector<TopoDim,TopologyTrace> newAdditionITraceToGroup0, newAdditionITraceToGroup1;

  std::array<int,Line::NNode> splitTraceNodeMap, ITraceNodeMap;
  splitTraceNodeMap.at(0) = startNode; splitTraceNodeMap.at(1) = endNode;

  const int nCell = static_cast<int>(cV.localCellGroup0.size()); // Should be 1
  const int nITrace0 = static_cast<int>(cV.localITraceGroup0.size()); // Should be 0

  // because will need to construct traces that aren't just copies
  const TraceBasisType* trace_basis = TraceBasisType::getBasisFunction(order_, this->basisCategory_);

  std::map<int,int> cellGroup0_initialElem_to_splitElem; // map from initial element number to its new split element
  std::map<int,int> cellGroup1_initialElem_to_splitElem; // map from initial element number to its new split element

  // extract information to figure out which edge is being split
  // const int TopologyCell::NNode = cV.localCellGroup0[0].localAssoc.nNode();

  std::array<int,TopologyCell::NNode>  cellNodeMap_0, cellNodeMap_1;
  std::array<int,TopologyTrace::NNode> traceNodeMap_0,traceNodeMap_1;

  // --------------------------------------------- //
  // Cell Group 0
  // --------------------------------------------- //
  // std::cout << "cellGroup 0" <<std::endl;
  std::size_t initialSize = cV.localCellGroup0.size(); // This is done so push_back can be used without the loop addressing new elements
  for (std::size_t i = 0; i < initialSize; i++)
  {
    ITraceNodeMap[1] = newNode; // the second node is always initially the new one

    // copy node map for element into array, Get nodeDOF map from current associativities
    cV.localCellGroup0[i].localAssoc.getNodeGlobalMapping( cellNodeMap_0.data(), cellNodeMap_0.size() );

    cellNodeMap_1 = cellNodeMap_0; // copy the node map

    // Extract canonical trace of edge to be split
    CanonicalTraceToCell ITrace_ToSplit = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyCell>::
        getCanonicalTrace(splitTraceNodeMap.data(), splitTraceNodeMap.size(), cellNodeMap_0.data(), cellNodeMap_0.size() );

    // Make the new node maps from the current one
    for (int k = 0; k < TopologyCell::NNode; k++)
    {
      if ( cellNodeMap_0[k] != startNode && cellNodeMap_0[k] != endNode ) // startNode and endNode make up the to be split edge
      {
        cellNodeMap_1[k] = cellNodeMap_0[k]; // the shared node in each map
        ITraceNodeMap[0] = cellNodeMap_1[k]; // the new internal edge start point
      }
      if ( cellNodeMap_0[k] == startNode) // the start node is replaced for the new element
        cellNodeMap_1[k] = newNode;
      if ( cellNodeMap_0[k] == endNode) // the end node is replaced for the original element
        cellNodeMap_0[k] = newNode;
    }

    // Set up the new trace connecting the original to the new split element
    CanonicalTraceToCell ITrace_canonicalL = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyCell>::
        getCanonicalTrace( ITraceNodeMap.data(), ITraceNodeMap.size(), cellNodeMap_0.data(), cellNodeMap_0.size() );
    if (ITrace_canonicalL.orientation < 1) // left orientation should be positive
    {
      ITraceNodeMap[1] = ITraceNodeMap[0];
      ITraceNodeMap[0] = newNode;
      ITrace_canonicalL = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyCell>::
          getCanonicalTrace( ITraceNodeMap.data(), ITraceNodeMap.size(), cellNodeMap_0.data(), cellNodeMap_0.size());
    }
    CanonicalTraceToCell ITrace_canonicalR = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyCell>::
        getCanonicalTrace( ITraceNodeMap.data(), ITraceNodeMap.size(), cellNodeMap_1.data(), cellNodeMap_1.size() );

    GroupElemConstructorIndex<TopoDim,TopologyCell> newElement(cV.localCellGroup0[i]); // make a new element based on the current

    const int rank = cV.localCellGroup0[i].localAssoc.rank();
    cV.localCellGroup0[i].localAssoc.setNodeGlobalMapping( cellNodeMap_0.data(), cellNodeMap_0.size() );
    cV.localCellGroup0[i].localAssoc.setEdgeSign(+1, ITrace_canonicalL.trace ); // initial cell is positive side, preserves trace ordering

    newElement.localAssoc.setRank( rank );
    newElement.localAssoc.setNodeGlobalMapping( cellNodeMap_1.data(), cellNodeMap_1.size() );
    newElement.localAssoc.setEdgeSign(-1, ITrace_canonicalR.trace ); // initial cell is positive side, preserves trace ordering

    // check if the left or the right hand of the trace being split, this is encoded in orientation
    if ( ITrace_ToSplit.orientation > 0 ) // LEFT
    {
      cV.localCellGroup0[i].elementSplitInfo = ElementSplitInfo( ElementSplitFlag::Split, ElementSplitType::Edge, ITrace_ToSplit.trace, 0 );
      newElement.elementSplitInfo = ElementSplitInfo(         ElementSplitFlag::Split, ElementSplitType::Edge, ITrace_ToSplit.trace, 1 );
    }
    else if ( ITrace_ToSplit.orientation < 0 ) // RIGHT
    {
      cV.localCellGroup0[i].elementSplitInfo = ElementSplitInfo( ElementSplitFlag::Split, ElementSplitType::Edge, ITrace_ToSplit.trace, 1 );
      newElement.elementSplitInfo = ElementSplitInfo(         ElementSplitFlag::Split, ElementSplitType::Edge, ITrace_ToSplit.trace, 0 );
    }

    cV.localCellGroup0.push_back(newElement); // add the new element at the end, thus giving it nCell + (int) i for an index

    cellGroup0_initialElem_to_splitElem.insert( std::make_pair<int,int>((int) i, nCell + (int) i ) );

    GroupTraceConstructorIndex<TopoDim,TopologyTrace> newTrace;
    newTrace.group = -1; // group didn't exist
    newTrace.trace = - ((int) i + 1); // new and indexed by the cell number starting at 1
    newTrace.localAssoc.resize( trace_basis ); // because there might not be any internal traces presently
    newTrace.localAssoc.setRank( rank );
    newTrace.localAssoc.setNodeGlobalMapping( ITraceNodeMap.data(), ITraceNodeMap.size() );
    newTrace.localLRElems.first = GroupElemIndex( 0, (int) i );
    newTrace.localLRElems.second = GroupElemIndex( 0, cellGroup0_initialElem_to_splitElem.at((int) i) );
    newTrace.elementSplitInfo = ElementSplitInfo(ElementSplitFlag::New, 2*nITrace0 + (int) i);

    newAdditionITraceToGroup0.push_back(newTrace);
  }

  // --------------------------------------------- //
  // Interior trace group interior to group 0
  // --------------------------------------------- //
  // std::cout << "ITraceGroup 0" <<std::endl;
  initialSize = cV.localITraceGroup0.size(); // Should be size 0 for Element view, but leave this loop here anyway
  for ( std::size_t i = 0; i < initialSize; i++)
  {
    cV.localITraceGroup0[i].localAssoc.getNodeGlobalMapping( traceNodeMap_0.data(), traceNodeMap_0.size() );
    traceNodeMap_1 = traceNodeMap_0;

    const int elemL = cV.localITraceGroup0[i].localLRElems.first.elem, elemR = cV.localITraceGroup0[i].localLRElems.second.elem;

    // reattach the trace node maps to the new node
    // must be done before comparing the maps because the cell maps were updated above
    for (int k = 0; k < TopologyTrace::NNode; k++)
    {
      if (traceNodeMap_0[k] == startNode) // it is the start node
        traceNodeMap_1[k] = newNode;
      if (traceNodeMap_0[k] == endNode) // it is the end node
        traceNodeMap_0[k] = newNode;
    }

    GroupTraceConstructorIndex<TopoDim,TopologyTrace> newTrace(cV.localITraceGroup0[i]); // copy of the current

    cV.localITraceGroup0[i].localAssoc.setNodeGlobalMapping( traceNodeMap_0.data(), traceNodeMap_0.size() );
    cV.localITraceGroup0[i].elementSplitInfo = ElementSplitInfo(ElementSplitFlag::Split, ElementSplitType::Isotropic, -1, 0);

    const int rank = cV.localITraceGroup0[i].localAssoc.rank();
    newTrace.localAssoc.setNodeGlobalMapping( traceNodeMap_1.data(), traceNodeMap_1.size() );
    newTrace.localAssoc.setRank( rank );
    newTrace.localLRElems = std::make_pair( GroupElemIndex(0, cellGroup0_initialElem_to_splitElem.at(elemL) ),
                                            GroupElemIndex(0, cellGroup0_initialElem_to_splitElem.at(elemR) ) );
    newTrace.elementSplitInfo = ElementSplitInfo(ElementSplitFlag::Split, ElementSplitType::Isotropic, -1, 1);

    cV.localITraceGroup0.push_back(newTrace); // add to the list
  }

  // Interior Traces for group 0 are all now known, can add the new ITraces after the split ones
  cV.localITraceGroup0.insert( cV.localITraceGroup0.end(), newAdditionITraceToGroup0.begin(), newAdditionITraceToGroup0.end() );

  // --------------------------------------------- //
  // Boundary Traces attached to Group 0
  // --------------------------------------------- //
  // std::cout << "BTraceGroup 0" <<std::endl;
  initialSize = cV.localBTraceGroup0.size();
  for (std::size_t i = 0; i < initialSize; i++)
  {
    cV.localBTraceGroup0[i].localAssoc.getNodeGlobalMapping( traceNodeMap_0.data(), traceNodeMap_0.size() );

    // Check if this trace contains node 0 and node 1 -- namely must be split
    const bool attachedToNode0 = std::find(std::begin(traceNodeMap_0),std::end(traceNodeMap_0), startNode) != std::end(traceNodeMap_0);
    const bool attachedToNode1 = std::find(std::begin(traceNodeMap_0),std::end(traceNodeMap_0),   endNode) != std::end(traceNodeMap_0);

    if ( attachedToNode0 && attachedToNode1 )
    {
      traceNodeMap_1 = traceNodeMap_0;

      // pair for detecting if the sub_cell_index is reversed
      // first bool is if it's been found, second bool is if it was reversed
      std::pair<bool,bool> found_reversed = std::make_pair(false,false);

      // attach the node maps to the new node
      // the original trace goes from startNOde to newNode, and the new trace goes from newNode to endNode
      for (int k = 0; k < TopologyTrace::NNode; k++)
      {
        if (traceNodeMap_0[k] == startNode) // it is the start node
        {
          if (found_reversed.first == false)
            found_reversed = std::make_pair(true,false);
          traceNodeMap_1[k] = newNode;
        }
        if (traceNodeMap_0[k] == endNode) // it is the end node
        {
          if (found_reversed.first == false)
            found_reversed = std::make_pair(true,true);
          traceNodeMap_0[k] = newNode;
        }
      }

      const int elemL = cV.localBTraceGroup0[i].localLRElems.first.elem;

      GroupTraceConstructorIndex<TopoDim,TopologyTrace> newTrace(cV.localBTraceGroup0[i]); // copy of the current
      cV.localBTraceGroup0[i].localAssoc.setNodeGlobalMapping( traceNodeMap_0.data(), traceNodeMap_0.size() );

      const int rank = cV.localBTraceGroup0[i].localAssoc.rank();

      newTrace.localAssoc.setNodeGlobalMapping( traceNodeMap_1.data(), traceNodeMap_1.size() );
      newTrace.localLRElems.first.elem = cellGroup0_initialElem_to_splitElem.at(elemL);
      newTrace.localAssoc.setRank( rank );

      if ( found_reversed.second == false )
      {
        cV.localBTraceGroup0[i].elementSplitInfo = ElementSplitInfo( ElementSplitFlag::Split, ElementSplitType::Isotropic, -1, 0 );
        newTrace.elementSplitInfo = ElementSplitInfo( ElementSplitFlag::Split, ElementSplitType::Isotropic, -1, 1 );
      }
      else if ( found_reversed.first == true )
      {
        cV.localBTraceGroup0[i].elementSplitInfo = ElementSplitInfo( ElementSplitFlag::Split, ElementSplitType::Isotropic, -1, 1 );
        newTrace.elementSplitInfo = ElementSplitInfo( ElementSplitFlag::Split, ElementSplitType::Isotropic, -1, 0 );
      }

      cV.localBTraceGroup0.push_back(newTrace); // add it on, the
    }
    else if ( !attachedToNode0 && attachedToNode1
      && cellGroup0_initialElem_to_splitElem.find(cV.localBTraceGroup0[i].localLRElems.first.elem) != cellGroup0_initialElem_to_splitElem.end() )
    {
      // is attached to node 1, but not 0, i.e. it's attached to a new cell
      cV.localBTraceGroup0[i].localLRElems.first.elem = cellGroup0_initialElem_to_splitElem.at(cV.localBTraceGroup0[i].localLRElems.first.elem);
    }
    else if ( attachedToNode0 && !attachedToNode1 )
    {
      // do nothing, the cell numbering is the same
    }
    else
    {
      SANS_DEVELOPER_EXCEPTION("XField_ElementLocal<PhysDim,TopoD2>::split() -- an element in boundary "
          "trace group 0 is attached to neither startNode nor endNode");
    }
  }

  // --------------------------------------------- //
  // Cell Group 1
  // --------------------------------------------- //
  // std::cout << "cellGroup 1" <<std::endl;

  initialSize = cV.localCellGroup1.size();
  const int nITrace1 = static_cast<int>(cV.localITraceGroup1.size()); // Should be 0 for primal grids
  int newGroup1Elems = 0; // number of new elements added to group 1, for building the maps
  for (std::size_t i = 0; i < initialSize; i++)
  {
    // Check if the cell has the startNode, endNode or both
    // copy node map for element into array, Get nodeDOF map from current associativities
    cV.localCellGroup1[i].localAssoc.getNodeGlobalMapping( cellNodeMap_0.data(), cellNodeMap_0.size() );

    // Check if this trace contains node 0 and node 1 -- namely must be split
    const bool attachedToNode0 = std::find(std::begin(cellNodeMap_0),std::end(cellNodeMap_0), startNode) != std::end(cellNodeMap_0);
    const bool attachedToNode1 = std::find(std::begin(cellNodeMap_0),std::end(cellNodeMap_0),   endNode) != std::end(cellNodeMap_0);

    if ( !( attachedToNode0 && attachedToNode1 ) )
      continue; // No splits necessary, not attached to both nodes

    // copy the cell node map
    cellNodeMap_1 = cellNodeMap_0; // copy the map

    ITraceNodeMap[1] = newNode; // the second node connects to the new trace

    // Extract canonical trace of edge to be split
    CanonicalTraceToCell ITrace_ToSplit = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyCell>::
        getCanonicalTrace(splitTraceNodeMap.data(), splitTraceNodeMap.size(), cellNodeMap_0.data(), cellNodeMap_0.size() );

    // Make the new node maps from the current one
    for (int k = 0; k < TopologyCell::NNode; k++)
    {
      if ( cellNodeMap_0[k] != startNode && cellNodeMap_0[k] != endNode ) // startNode and endNode make up the to be split edge
      {
        // cellNodeMap_1[k] = cellNodeMap_0[k]; // the shared node in each map
        ITraceNodeMap[0] = cellNodeMap_1[k]; // the new internal edge start point
      }
      if ( cellNodeMap_0[k] == startNode) // the start node is replaced for the new element
        cellNodeMap_1[k] = newNode;
      if ( cellNodeMap_0[k] == endNode) // the end node is replaced for the original element
        cellNodeMap_0[k] = newNode;
    }

    // Set up the new trace connecting the original to the new split element
    CanonicalTraceToCell ITrace_canonicalL = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyCell>::
        getCanonicalTrace( ITraceNodeMap.data(), ITraceNodeMap.size(), cellNodeMap_0.data(), cellNodeMap_0.size() );
    if (ITrace_canonicalL.orientation < 1) // left orientation should be positive
    {
      ITraceNodeMap[1] = ITraceNodeMap[0]; // flip the map
      ITraceNodeMap[0] = newNode;
      ITrace_canonicalL = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyCell>::
          getCanonicalTrace( ITraceNodeMap.data(), ITraceNodeMap.size(), cellNodeMap_0.data(), cellNodeMap_0.size());
    }
    CanonicalTraceToCell ITrace_canonicalR = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyCell>::
        getCanonicalTrace( ITraceNodeMap.data(), ITraceNodeMap.size(), cellNodeMap_1.data(), cellNodeMap_1.size() );

    GroupElemConstructorIndex<TopoDim,TopologyCell> newElement(cV.localCellGroup1[i]); // make a new element based on the current

    const int rank = cV.localCellGroup1[i].localAssoc.rank();
    cV.localCellGroup1[i].localAssoc.setNodeGlobalMapping( cellNodeMap_0.data(), cellNodeMap_0.size() );
    cV.localCellGroup1[i].localAssoc.setEdgeSign(+1, ITrace_canonicalL.trace ); // initial cell is positive side, preserves trace ordering

    newElement.localAssoc.setRank( rank );
    newElement.localAssoc.setNodeGlobalMapping( cellNodeMap_1.data(), cellNodeMap_1.size() );
    newElement.localAssoc.setEdgeSign(-1, ITrace_canonicalR.trace ); // initial cell is positive side, preserves trace ordering

    // check if the left or the right hand of the trace being split, this is encoded in orientation
    if ( ITrace_ToSplit.orientation > 0 ) // LEFT
    {
      cV.localCellGroup1[i].elementSplitInfo = ElementSplitInfo( ElementSplitFlag::Split, ElementSplitType::Edge, ITrace_ToSplit.trace, 0 );
      newElement.elementSplitInfo = ElementSplitInfo(            ElementSplitFlag::Split, ElementSplitType::Edge, ITrace_ToSplit.trace, 1 );
    }
    else if ( ITrace_ToSplit.orientation < 0 ) // RIGHT
    {
      cV.localCellGroup1[i].elementSplitInfo = ElementSplitInfo( ElementSplitFlag::Split, ElementSplitType::Edge, ITrace_ToSplit.trace, 1 );
      newElement.elementSplitInfo = ElementSplitInfo(            ElementSplitFlag::Split, ElementSplitType::Edge, ITrace_ToSplit.trace, 0 );
    }

    cV.localCellGroup1.push_back(newElement); // add the new element at the end, thus giving it nCell + (int) i for an index

    GroupTraceConstructorIndex<TopoDim,TopologyTrace> newTrace;
    newTrace.group = - 2; // group didn't exist, -2 is new internal to cell group 1
    newTrace.trace = - (newGroup1Elems + 1 ); // new and indexed by the cell number starting at 1
    newTrace.localAssoc.resize( trace_basis ); // because there might not be any internal traces presently
    newTrace.localAssoc.setRank( rank );
    newTrace.localAssoc.setNodeGlobalMapping( ITraceNodeMap.data(), ITraceNodeMap.size() );
    newTrace.localLRElems.first = GroupElemIndex( 1, (int) i );
    newTrace.localLRElems.second = GroupElemIndex( 1, (int)initialSize + newGroup1Elems ); // offset new elements by initial size of group 1
    newTrace.elementSplitInfo = ElementSplitInfo(ElementSplitFlag::New, nITrace1 + newGroup1Elems );

    newAdditionITraceToGroup1.push_back(newTrace);

    // add into the map so that it can be found for reconnection later
    cellGroup1_initialElem_to_splitElem.insert( std::pair<int,int>((int)i,(int)initialSize + newGroup1Elems ) );

    newGroup1Elems++; // Adding in a new element
  }

  // --------------------------------------------- //
  // Interior trace group interior to group 1
  // --------------------------------------------- //
  // std::cout << "ITraceGroup 1" <<std::endl;
  initialSize = cV.localITraceGroup1.size(); // Should be size 0 for Primal, can be non-zero for Dual
  // This is for splitting any existing traces that are connected to the split edge, really only kicks in in 3D
  for ( std::size_t i = 0; i < initialSize; i++)
  {
    cV.localITraceGroup1[i].localAssoc.getNodeGlobalMapping( traceNodeMap_0.data(), traceNodeMap_0.size() );

    // Check if this trace contains node 0 and node 1 -- namely must be split
    const bool attachedToNode0 = std::find(std::begin(traceNodeMap_0),std::end(traceNodeMap_0), startNode) != std::end(traceNodeMap_0);
    const bool attachedToNode1 = std::find(std::begin(traceNodeMap_0),std::end(traceNodeMap_0),   endNode) != std::end(traceNodeMap_0);

    if ( ( attachedToNode0 && attachedToNode1 ) ) // if this trace is attached to both nodes - can't happen in 2D
    {
      SANS_DEVELOPER_EXCEPTION("A trace interior to cell group 1 can not be connected to both startNode and endNode in 2D");
      // traceNodeMap_1 = traceNodeMap_0;
      //
      // const int elemL = cV.localITraceGroup1[i].localLRElems.first.elem, elemR = cV.localITraceGroup1[i].localLRElems.second.elem;
      //
      // // reattach the trace node maps to the new node
      // // must be done before comparing the maps because the cell maps were updated above
      // for (int k = 0; k < TopologyTrace::NNode; k++)
      // {
      //   if (traceNodeMap_0[k] == startNode) // it is the start node
      //     traceNodeMap_1[k] = newNode;
      //   if (traceNodeMap_0[k] == endNode) // it is the end node
      //     traceNodeMap_0[k] = newNode;
      // }
      //
      // GroupTraceConstructorIndex<TopoDim,TopologyTrace> newTrace(cV.localITraceGroup1[i]); // copy of the current
      //
      // cV.localITraceGroup1[i].localAssoc.setNodeGlobalMapping( traceNodeMap_0.data(), traceNodeMap_0.size() );
      // cV.localITraceGroup1[i].elementSplitInfo = ElementSplitInfo(ElementSplitFlag::Split, ElementSplitType::Isotropic, -1, 0);
      //
      // const int rank = cV.localITraceGroup1[i].localAssoc.rank();
      // newTrace.localAssoc.setNodeGlobalMapping( traceNodeMap_1.data(), traceNodeMap_1.size() );
      // newTrace.localAssoc.setRank( rank );
      // newTrace.localLRElems = std::make_pair( GroupElemIndex(1, cellGroup1_initialElem_to_splitElem.at(elemL) ),
      //                                         GroupElemIndex(1, cellGroup1_initialElem_to_splitElem.at(elemR) ) );
      // newTrace.elementSplitInfo = ElementSplitInfo(ElementSplitFlag::Split, ElementSplitType::Isotropic, -1, 1);
      //
      // cV.localITraceGroup1.push_back(newTrace); // add to the list
    }
    else if ( !attachedToNode0 && attachedToNode1 )// its attached to only endNode, and its left element was split
    {
      // left element has been split, reconnect
      if ( cellGroup1_initialElem_to_splitElem.find(cV.localITraceGroup1[i].localLRElems.first.elem) != cellGroup1_initialElem_to_splitElem.end() )
        cV.localITraceGroup1[i].localLRElems.first.elem = cellGroup1_initialElem_to_splitElem.at(cV.localITraceGroup1[i].localLRElems.first.elem);

      // right element has been split, reconnect
      if ( cellGroup1_initialElem_to_splitElem.find(cV.localITraceGroup1[i].localLRElems.second.elem) != cellGroup1_initialElem_to_splitElem.end() )
        cV.localITraceGroup1[i].localLRElems.second.elem = cellGroup1_initialElem_to_splitElem.at(cV.localITraceGroup1[i].localLRElems.second.elem);
    }
  }
  // Interior Traces are all known now, can add the new ITraces after the split ones
  cV.localITraceGroup1.insert( cV.localITraceGroup1.end(), newAdditionITraceToGroup1.begin(), newAdditionITraceToGroup1.end() );

  // --------------------------------------------- //
  // Interior Traces from group 0 to group 1
  // --------------------------------------------- //
  // std::cout << "ITraceGroup 0 to 1" <<std::endl;

  // Reconnect if attached only to endNode, create new traces if attached to both
  initialSize = cV.localITraceGroup0to1.size();
  for (std::size_t i = 0; i < initialSize; i++)
  {
    cV.localITraceGroup0to1[i].localAssoc.getNodeGlobalMapping( traceNodeMap_0.data(), traceNodeMap_0.size() );

    // Check if this trace contains node 0 and node 1 -- namely must be split
    const bool attachedToNode0 = std::find(std::begin(traceNodeMap_0),std::end(traceNodeMap_0), startNode) != std::end(traceNodeMap_0);
    const bool attachedToNode1 = std::find(std::begin(traceNodeMap_0),std::end(traceNodeMap_0),   endNode) != std::end(traceNodeMap_0);

    if (attachedToNode0 && attachedToNode1 ) // going to be split, need to make a new trace
    {
      traceNodeMap_1 = traceNodeMap_0;

      const int elemL = cV.localITraceGroup0to1[i].localLRElems.first.elem, elemR = cV.localITraceGroup0to1[i].localLRElems.second.elem;

      for (int k = 0; k < TopologyTrace::NNode; k++)
      {
        if (traceNodeMap_0[k] == startNode) // it is the start node
          traceNodeMap_1[k] = newNode;
        if (traceNodeMap_0[k] == endNode) // it is the end node
          traceNodeMap_0[k] = newNode;
      }

      GroupTraceConstructorIndex<TopoDim,TopologyTrace> newTrace(cV.localITraceGroup0to1[i]); // copy of the current

      cV.localITraceGroup0to1[i].localAssoc.setNodeGlobalMapping( traceNodeMap_0.data(), traceNodeMap_0.size() );
      cV.localITraceGroup0to1[i].elementSplitInfo = ElementSplitInfo(ElementSplitFlag::Split, ElementSplitType::Isotropic, -1, 0);

      const int rank = cV.localITraceGroup0to1[i].localAssoc.rank();
      newTrace.localAssoc.setNodeGlobalMapping( traceNodeMap_1.data(), traceNodeMap_1.size() );
      newTrace.localAssoc.setRank( rank );
      newTrace.localLRElems = std::make_pair( GroupElemIndex(0, cellGroup0_initialElem_to_splitElem.at(elemL) ),
                                              GroupElemIndex(1, cellGroup1_initialElem_to_splitElem.at(elemR) ) );
      newTrace.elementSplitInfo = ElementSplitInfo(ElementSplitFlag::Split, ElementSplitType::Isotropic, -1, 1);

      cV.localITraceGroup0to1.push_back(newTrace); // add to the list
    }
    else if ( !attachedToNode0 && attachedToNode1 ) // only attached to endNode, just need to go through maps to reconnect left element
    //&& cellGroup0_initialElem_to_splitElem.find(cV.localITraceGroup0to1[i].localLRElems.first.elem) != cellGroup0_initialElem_to_splitElem.end() )
    {
      // Don't need to check for key existence, the splitting of cell group 0 above should guarantee the existence
      cV.localITraceGroup0to1[i].localLRElems.first.elem = cellGroup0_initialElem_to_splitElem.at(cV.localITraceGroup0to1[i].localLRElems.first.elem);
    }
    else if ( attachedToNode0 && !attachedToNode1 )
    {
      // do nothing, the left element was unchanged
    }
    else
      SANS_DEVELOPER_EXCEPTION("XField_ElementLocal<PhysDim,TopoD2>::split() -- an element in interior "
               "trace group 0 to 1 is attached to neither node 0 nor node 1 or both");
  }

  // --------------------------------------------- //
  // Boundary Trace Group 1
  // --------------------------------------------- //
  // std::cout << "BTraceGroup 1" <<std::endl;
  initialSize = cV.localBTraceGroup1.size();
  // Just reconnecting to new elements if they're there
  for (std::size_t i = 0; i < initialSize; i++)
  {
    cV.localBTraceGroup1[i].localAssoc.getNodeGlobalMapping( traceNodeMap_0.data(), traceNodeMap_0.size() );

    // Check if this trace contains node 0 and node 1 -- namely must be split
    const bool attachedToNode0 = std::find(std::begin(traceNodeMap_0),std::end(traceNodeMap_0), startNode) != std::end(traceNodeMap_0);
    const bool attachedToNode1 = std::find(std::begin(traceNodeMap_0),std::end(traceNodeMap_0),   endNode) != std::end(traceNodeMap_0);

    if (attachedToNode0 && attachedToNode1 ) // its attached to split edge, it gets split
    {
      SANS_DEVELOPER_EXCEPTION("A trace in boundary group 1 can not be connected to both startNode and endNode in 2D");
      // traceNodeMap_1 = traceNodeMap_0;
      //
      // // pair for detecting if the sub_cell_index is reversed
      // // first bool is if it's been found, second bool is if it was reversed
      // std::pair<bool,bool> found_reversed = std::make_pair(false,false);
      //
      // // attach the node maps to the new node
      // // the original trace goes from startNOde to newNode, and the new trace goes from newNode to endNode
      // for (int k = 0; k < TopologyTrace::NNode; k++)
      // {
      //   if (traceNodeMap_0[k] == startNode) // it is the start node
      //   {
      //     if (found_reversed.first == false)
      //       found_reversed = std::make_pair(true,false);
      //     traceNodeMap_1[k] = newNode;
      //   }
      //   if (traceNodeMap_0[k] == endNode) // it is the end node
      //   {
      //     if (found_reversed.first == false)
      //       found_reversed = std::make_pair(true,true);
      //     traceNodeMap_0[k] = newNode;
      //   }
      // }
      //
      // const int elemL = cV.localBTraceGroup1[i].localLRElems.first.elem;
      //
      // GroupTraceConstructorIndex<TopoDim,TopologyTrace> newTrace(cV.localBTraceGroup1[i]); // copy of the current
      // cV.localBTraceGroup1[i].localAssoc.setNodeGlobalMapping( traceNodeMap_0.data(), traceNodeMap_0.size() );
      //
      // newTrace.localAssoc.setNodeGlobalMapping( traceNodeMap_1.data(), traceNodeMap_1.size() );
      // newTrace.localLRElems.first.elem = cellGroup1_initialElem_to_splitElem.at(elemL);
      //
      // if ( found_reversed.second == false )
      // {
      //   cV.localBTraceGroup1[i].elementSplitInfo = ElementSplitInfo( ElementSplitFlag::Split, ElementSplitType::Isotropic, -1, 0 );
      //   newTrace.elementSplitInfo = ElementSplitInfo( ElementSplitFlag::Split, ElementSplitType::Isotropic, -1, 1 );
      // }
      // else if ( found_reversed.first == true )
      // {
      //   cV.localBTraceGroup1[i].elementSplitInfo = ElementSplitInfo( ElementSplitFlag::Split, ElementSplitType::Isotropic, -1, 1 );
      //   newTrace.elementSplitInfo = ElementSplitInfo( ElementSplitFlag::Split, ElementSplitType::Isotropic, -1, 0 );
      // }
      //
      // cV.localBTraceGroup1.push_back(newTrace); // add it on
    }
    else if ( !attachedToNode0 && attachedToNode1 // its attached to only endNode, and its left element was split
      && cellGroup1_initialElem_to_splitElem.find(cV.localBTraceGroup1[i].localLRElems.first.elem) != cellGroup1_initialElem_to_splitElem.end() )
    {
      // is attached to node 1, but not 0, and its left element was split
      cV.localBTraceGroup1[i].localLRElems.first.elem = cellGroup1_initialElem_to_splitElem.at(cV.localBTraceGroup1[i].localLRElems.first.elem);
    }
  }

  // --------------------------------------------- //
  // Outer Boundary Trace Group
  // --------------------------------------------- //

  // std::cout << "OuterBTraceGroup" <<std::endl;
  std::map<int,int> *outerMap;
  outerMap = &cellGroup1_initialElem_to_splitElem;

  initialSize = cV.localBTraceOuterGroup1.size();
  // Just reconnecting to new elements if they're there.
  for (std::size_t i = 0; i < initialSize; i++)
  {
    cV.localBTraceOuterGroup1[i].localAssoc.getNodeGlobalMapping( traceNodeMap_0.data(), traceNodeMap_0.size() );

    // Check if this trace contains node 0 and node 1 -- namely must be split
    const bool attachedToNode0 = std::find(std::begin(traceNodeMap_0),std::end(traceNodeMap_0), startNode) != std::end(traceNodeMap_0);
    const bool attachedToNode1 = std::find(std::begin(traceNodeMap_0),std::end(traceNodeMap_0),   endNode) != std::end(traceNodeMap_0);

    if (attachedToNode0 && attachedToNode1 ) // its attached to split edge, it gets split
    {
      traceNodeMap_1 = traceNodeMap_0;

      // pair for detecting if the sub_cell_index is reversed
      // first bool is if it's been found, second bool is if it was reversed
      std::pair<bool,bool> found_reversed = std::make_pair(false,false);

      // attach the node maps to the new node
      // the original trace goes from startNOde to newNode, and the new trace goes from newNode to endNode
      for (int k = 0; k < TopologyTrace::NNode; k++)
      {
        if (traceNodeMap_0[k] == startNode) // it is the start node
        {
          if (found_reversed.first == false)
            found_reversed = std::make_pair(true,false);
          traceNodeMap_1[k] = newNode;
        }
        if (traceNodeMap_0[k] == endNode) // it is the end node
        {
          if (found_reversed.first == false)
            found_reversed = std::make_pair(true,true);
          traceNodeMap_0[k] = newNode;
        }
      }

      const int elemL = cV.localBTraceOuterGroup1[i].localLRElems.first.elem;

      GroupTraceConstructorIndex<TopoDim,TopologyTrace> newTrace(cV.localBTraceOuterGroup1[i]); // copy of the current
      cV.localBTraceOuterGroup1[i].localAssoc.setNodeGlobalMapping( traceNodeMap_0.data(), traceNodeMap_0.size() );

      const int rank = cV.localBTraceOuterGroup1[i].localAssoc.rank();

      newTrace.localAssoc.setNodeGlobalMapping( traceNodeMap_1.data(), traceNodeMap_1.size() );
      newTrace.localLRElems.first.elem = outerMap->at(elemL);
      newTrace.localAssoc.setRank( rank );

      if ( found_reversed.second == false )
      {
        cV.localBTraceOuterGroup1[i].elementSplitInfo = ElementSplitInfo( ElementSplitFlag::Split, ElementSplitType::Isotropic, -1, 0 );
        newTrace.elementSplitInfo = ElementSplitInfo( ElementSplitFlag::Split, ElementSplitType::Isotropic, -1, 1 );
      }
      else if ( found_reversed.first == true )
      {
        cV.localBTraceOuterGroup1[i].elementSplitInfo = ElementSplitInfo( ElementSplitFlag::Split, ElementSplitType::Isotropic, -1, 1 );
        newTrace.elementSplitInfo = ElementSplitInfo( ElementSplitFlag::Split, ElementSplitType::Isotropic, -1, 0 );
      }

      cV.localBTraceOuterGroup1.push_back(newTrace); // add it on
    }
    if ( !attachedToNode0 && attachedToNode1
      && outerMap->find(cV.localBTraceOuterGroup1[i].localLRElems.first.elem) != outerMap->end() )
    {
      // is attached to node 1, but not 0, i.e. it's attached to a new cell
      cV.localBTraceOuterGroup1[i].localLRElems.first.elem
        = outerMap->at(cV.localBTraceOuterGroup1[i].localLRElems.first.elem);
    }
  }

}

// Explicit instantiations
template class XField_Local_Common<PhysD2,TopoD2>;
template void  XField_Local_Common<PhysD2,TopoD2>::split<Triangle,Line>( const int, const int, const int,
            ConstructorVectorTopo<TopoD2,Triangle,Line>& );

}
