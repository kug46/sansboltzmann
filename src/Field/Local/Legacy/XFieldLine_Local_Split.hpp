// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELDLINE_LOCAL_SPLIT_H
#define XFIELDLINE_LOCAL_SPLIT_H

#include <map>

#include "Topology/Dimension.h"

#include "Field/Element/ElementAssociativityNodeConstructor.h"
#include "Field/Element/ElementAssociativityLineConstructor.h"

#include "XFieldLine_Local.h"

/*
 * This class uniformly splits the main cell in the local mesh produced by XField_Local.
 * - The two sub-cells (main-cells) are in cell group 0
 * - All neighboring cells remain in cell group 1
 * - The split introduces a new interior trace (node) inside the original main-cell, this trace is in a new interior trace group 1.
 */

/* Refer to the local solve documentation in Field/Documentation/Local_Solves/LocalSolves.pdf for more details and figures. */

namespace SANS
{

template <class PhysDim, class TopoDim>
class XField_Local_Split;


template<class PhysDim>
class XField_Local_Split< PhysDim, TopoD1> : public XField_Local_Base<PhysDim, TopoD1>
{

public:
  typedef XField_Local_Base<PhysDim, TopoD1> BaseType;

  XField_Local_Split(const XField_Local<PhysDim, TopoD1>& xfld_local, XField_CellToTrace<PhysDim,TopoD1>& connectivity);

  XField_Local_Split(const XField_Local<PhysDim, TopoD1>& xfld_local, XField_CellToTrace<PhysDim,TopoD1>& connectivity,
                     ElementSplitType split_type, int split_edge_index);

  ~XField_Local_Split() {}

  //Return connectivity of the global mesh which was used to construct the unsplit local mesh
  const XField_CellToTrace<PhysDim, TopoD1>& getConnectivity() const { return xfld_local_unsplit_.getConnectivity(); }

protected:
  using BaseType::reSolveBoundaryTraceGroups_;
  using BaseType::reSolveInteriorTraceGroups_;
  using BaseType::reSolveCellGroups_;
  const XField_Local<PhysDim, TopoD1>& xfld_local_unsplit_;
  const XField_CellToTrace<PhysDim, TopoD1>& connectivity_; //connectivity of local unsplit mesh

  //DOF maps
  std::map<int,int> NodeDOFMap_;
  std::map<int,int> EdgeDOFMap_;
  std::map<int,int> CellDOFMap_;

  int cntNodeDOF_, cntEdgeDOF_, cntCellDOF_; //Counters for different DOF types
  int offset_cellDOF_, offset_edgeDOF_, offset_nodeDOF_; //Offset for the different DOF indices in the global array

  int nNeighbors_;
  int nMainBTraceGroups_; //No of boundary traces (and groups) on main cell
  int nOuterBTraceGroups_; //No of boundary groups for outer btraces

  int OuterBTraceGroup_; //Group index for the outer boundary traces

  void splitMainCell();
  void extractNeighbors(int Ntrace, int order);

  void trackDOFs(int group, int elem);

  void setCellAssociativity(int group, int elem, ElementAssociativityConstructor<TopoD1, Line>& DOFAssoc);
  void setInteriorTraceAssociativity(int group, int elem, ElementAssociativityConstructor<TopoD0, Node>& DOFAssoc);
  void setBoundaryTraceAssociativity(int group, int elem, ElementAssociativityConstructor<TopoD0, Node>& DOFAssoc);
  void setGhostBoundaryTraceAssociativity(int group, int elem, ElementAssociativityConstructor<TopoD0, Node>& DOFAssoc);

};

}

#endif // XFIELDLINE_LOCAL_SPLIT_H
