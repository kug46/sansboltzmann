// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define XFIELD_ELEMENTLOCAL_INSTANTIATE

#include "XFieldVolume.h"
#include <iomanip>
#include <algorithm>
#include "XField_ElementLocal_impl.h"

namespace SANS
{

// Specialized Constructor for TopoD3
template< class PhysDim, class TopoDim >
XField_ElementLocal<PhysDim,TopoDim>::XField_ElementLocal( mpi::communicator& comm_local,
                                                           const XField_CellToTrace<PhysDim, TopoDim>& connectivity,
                                                           const Field_NodalView& nodalView, const int group, const int elem,
                                                           const Neighborhood neighborhood )
:
BaseType(comm_local, connectivity, nodalView, neighborhood)
{
  for (int group = 0; group < this->global_xfld_.nCellGroups(); group++)
  {
    if ( this->global_xfld_.getCellGroupBase(group).topoTypeID() == typeid(Tet) )
    {
      typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Tet> XFieldCellGroupType;
      typedef typename XFieldCellGroupType::BasisType BasisType;

      const XFieldCellGroupType& cellgrp = this->global_xfld_.template getCellGroup<Tet>(group);
      const BasisType* cell_basis = cellgrp.basis();
      orderVector_.push_back(cell_basis->order());
    }
    if ( this->global_xfld_.getCellGroupBase(group).topoTypeID() == typeid(Hex) )
    {
      SANS_DEVELOPER_EXCEPTION("Hexs unsupported");
    }
  }
  if ( std::adjacent_find( orderVector_.begin(), orderVector_.end(), std::not_equal_to<int>() ) != orderVector_.end() )
    SANS_DEVELOPER_EXCEPTION("Mixed order meshes are unsupported");


  // having now checked all groups are same topology and same order, proceed
  this->basisCategory_ = this->global_xfld_.getCellGroupBase(0).basisCategory();

  // Clear the maps
  NodeDOFMap_.clear();
  EdgeDOFMap_.clear();
  CellDOFMap_.clear();

  // Fill the vector structures and then use them to construct the grid
  if (  this->global_xfld_.getCellGroupBase(0).topoTypeID() == typeid(Tet) )
  {
    this->p_constructorVector_ = std::make_shared< ConstructorVectorTopo<TopoDim,Tet,Triangle> >();

    ConstructorVectorTopo<TopoDim,Tet,Triangle> *pconstructorVectors
    = static_cast<ConstructorVectorTopo<TopoDim,Tet,Triangle>*>(this->p_constructorVector_.get());

    extractLocalGrid<Tet,Triangle>( group, elem, *pconstructorVectors );

    BaseType::template allocateMesh<Tet,Triangle>(*pconstructorVectors);

    // build higher order mesh from the mesh if necessary
    if (orderVector_[0] > 1) // Higher order mesh required
    {
      // This copy constructor is doing all the hard work, can't modify *this in place
      XField_ElementLocal<PhysDim,TopoDim> xfld_local_linear( *this, FieldCopy() );
      this->buildFrom(xfld_local_linear,orderVector_[0]);
    }

    BaseType::template projectGroup( pconstructorVectors->localCellGroup0, 0 );
    BaseType::template projectGroup( pconstructorVectors->localCellGroup1, 1 );

  }
  else if (  this->global_xfld_.getCellGroupBase(0).topoTypeID() == typeid(Hex) )
  {
    SANS_DEVELOPER_EXCEPTION("Hexs Unsupported");
  }

  // This is checkGrid() but gets removed in release mode!
  this->checkGrid();

  this->computeReSolveGroups(); // compute the re-solve groups
}

template< class PhysDim, class TopoDim >
XField_ElementLocal<PhysDim,TopoDim>::XField_ElementLocal( const XField_ElementLocal<PhysDim,TopoDim>& xfld_unsplit,
                                                           const int edge )
: BaseType(xfld_unsplit.comm(),xfld_unsplit.connectivity_,xfld_unsplit.nodalView_,xfld_unsplit.neighborhood)
{
  this->basisCategory_ = xfld_unsplit.basisCategory_;

  this->NodeDOFMap_ = xfld_unsplit.NodeDOFMap_;
  this->EdgeDOFMap_ = xfld_unsplit.EdgeDOFMap_;
  this->CellDOFMap_ = xfld_unsplit.CellDOFMap_;

  this->orderVector_ = xfld_unsplit.orderVector_;

  this-> cntNodeDOF_ = xfld_unsplit.cntNodeDOF_;

  this->localToGlobalCellMapping_ = xfld_unsplit.localToGlobalCellMapping_;
  this->globalToLocalCellMapping_ = xfld_unsplit.globalToLocalCellMapping_;

  this->localToGlobalInteriorTraceMapping_ = xfld_unsplit.localToGlobalInteriorTraceMapping_;
  this->globalToLocalInteriorTraceMapping_ = xfld_unsplit.globalToLocalInteriorTraceMapping_;

  this->localToGlobalBoundaryTraceMapping_ = xfld_unsplit.localToGlobalBoundaryTraceMapping_;
  this->globalToLocalBoundaryTraceMapping_ = xfld_unsplit.globalToLocalBoundaryTraceMapping_;

  if ( xfld_unsplit.getCellGroupBase(0).topoTypeID() == typeid(Tet) )
  {
    typedef ConstructorVectorTopo<TopoDim,Tet,Triangle> ConstructorVectorTopoType;

    this->p_constructorVector_
      = std::make_shared< ConstructorVectorTopoType >( static_cast<ConstructorVectorTopoType&>(*xfld_unsplit.p_constructorVector_) );

    ConstructorVectorTopoType& newConstructorVector = static_cast<ConstructorVectorTopoType&>(*this->p_constructorVector_);

    // std::cout << "pointers manipulated" << std::endl;
    SANS_ASSERT( 0 <= edge && edge < Tet::NEdge );
    split<Tet,Triangle>( edge, newConstructorVector );

    // std::cout << "edge split" << std::endl;
    BaseType::template allocateMesh<Tet,Triangle>( newConstructorVector );

    // std::cout << "mesh allocated" << std::endl;
    // build higher order mesh from the mesh if necessary
    if (orderVector_[0] > 1) // Higher order mesh required
    {
      // This copy constructor is doing all the hard work, can't modify *this in place
      XField_ElementLocal<PhysDim,TopoDim> xfld_local_linear( *this, FieldCopy() );
      this->buildFrom(xfld_local_linear,orderVector_[0]);
    }

    BaseType::template projectGroup( newConstructorVector.localCellGroup0, 0 );
    BaseType::template projectGroup( newConstructorVector.localCellGroup1, 1 );
    // std::cout << "groups projected" << std::endl;
  }
  else
    SANS_DEVELOPER_EXCEPTION("Only works for Tets");

  // This is checkGrid() but gets removed in release mode!
  this->checkGrid();

  this->computeReSolveGroups(); // compute the re-solve groups

#if 0
  std::cout<< "constructing a split grid" << std::endl;
  std::cout<< "reSolveCellGroups_ = ";
  for (auto it = reSolveCellGroups_.begin(); it != reSolveCellGroups_.end(); ++it)
    std::cout << *it << ", ";
  std::cout << std::endl;

  std::cout<< "reSolveInteriorTraceGroups_ = ";
  for (auto it = reSolveInteriorTraceGroups_.begin(); it != reSolveInteriorTraceGroups_.end(); ++it)
    std::cout << *it << ", ";
  std::cout << std::endl;

  std::cout<< "reSolveBoundaryTraceGroups_ = ";
  for (auto it = reSolveBoundaryTraceGroups_.begin(); it != reSolveBoundaryTraceGroups_.end(); ++it)
    std::cout << *it << ", ";
  std::cout << std::endl;
#endif

}


// THIS IS TOPOD3 specific
template <class PhysDim, class TopoDim>
template <class TopologyCell, class TopologyTrace>
void
XField_ElementLocal<PhysDim, TopoDim>::split( const int edge, ConstructorVectorTopo<TopoDim,TopologyCell,TopologyTrace>& cV  )
{
  // SANS_DEVELOPER_EXCEPTION("Split not implemented yet for ElementLocal");
  /*
 * Can now do all the operations in here without ever having to reference the global mesh again, all the local Associativity constructors
 * will hold all the necessary information, and the allocate mesh interface largely speaking doesn't care about the splitting.
 */

  //Add new nodeDOF which will be added to the edge, currently the new node is -1, this is not good
  std::pair<std::map<int,int>::iterator,bool> map_ret =
       NodeDOFMap_.insert( std::pair<int,int>(-1,cntNodeDOF_) );
  SANS_ASSERT( map_ret.second==true );
  this->newNodeDOFs_.push_back(cntNodeDOF_);
  const int newNode = cntNodeDOF_ ;

  cntNodeDOF_++; //increment counter, this node will definitely be used

  // Figure out the nodes that make up the edge to be split
  std::array<int,TopologyCell::NNode> elemNodes;
  cV.localCellGroup0[0].localAssoc.getNodeGlobalMapping( elemNodes.data(), elemNodes.size() );

  const int (*EdgeNodes)[Line::NNode] = ElementEdges<TopologyCell>::EdgeNodes;

  // These nodes define the edge and are used to check which sets an element or trace is in later
  const int startNode = elemNodes[EdgeNodes[edge][0]], endNode = elemNodes[EdgeNodes[edge][1]];

  // SANS_ASSERT( newNode >= 0 ); SANS_ASSERT( startNode >= 0 ); SANS_ASSERT( endNode >= 0 ); // to silence unused variable warning

  // This actually does the splitting
  BaseType::split( startNode, endNode, newNode, cV );
  // SANS_DEVELOPER_EXCEPTION("Splitting not implemented in 3D");
}



//Explicit instantiations
template class XField_ElementLocal<PhysD3,TopoD3>;
//template class XField_ElementLocal<PhysD3,TopoD3>;

}
