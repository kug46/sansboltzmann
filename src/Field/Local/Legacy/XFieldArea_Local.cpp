// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XFieldArea.h"
#include "XFieldArea_Local.h"

#include <iomanip>
#include <algorithm>

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

//---------------------------------------------------------------------------//
template<class PhysDim>
XField_Local< PhysDim, TopoD2>::
XField_Local(mpi::communicator& comm_local, const XField_CellToTrace<PhysDim, TopoD2>& connectivity, int group, int elem) :
  BaseType(comm_local),
  connectivity_(connectivity), global_xfld_(connectivity_.getXField())
{
  // split the communicator so there is one per processor
  // this guarantess local grids are solved independently across processors
  this->comm_ = std::make_shared<mpi::communicator>(this->comm_->split(this->comm_->rank()));

  basisCategory_ = global_xfld_.getCellGroupBase(group).basisCategory();
  extractLocalGrid(group, elem);

  // This is checkGrid() but gets removed in release mode!
  this->checkGrid();

  for (int i = 0; i < this->nBoundaryTraceGroups(); i++)
    reSolveBoundaryTraceGroups_.push_back(i);

  for (int i = 0; i < this->nInteriorTraceGroups(); i++)
    reSolveInteriorTraceGroups_.push_back(i);
}

//---------------------------------------------------------------------------//
template <class PhysDim>
template <class Topology>
void
XField_Local<PhysDim, TopoD2>::trackDOFs(int group, int elem)
{
  typedef typename XField<PhysDim, TopoD2>::template FieldCellGroupType<Topology> XFieldCellGroupType;

  std::pair<std::map<int,int>::iterator,bool> map_ret; //return value for map inserts

  const XFieldCellGroupType& cellgrp = global_xfld_.template getCellGroup<Topology>(group);

  int nNodeDOF = cellgrp.associativity(elem).nNode();
  int nEdgeDOF = cellgrp.associativity(elem).nEdge();
  int nCellDOF = cellgrp.associativity(elem).nCell();

  // node DOF, edge DOF and cell DOF maps
  std::vector<int> nodeMap(nNodeDOF);
  std::vector<int> edgeMap(nEdgeDOF);
  std::vector<int> cellMap(nCellDOF);

  //Save DOF mappings from global mesh to local mesh
  cellgrp.associativity(elem).getNodeGlobalMapping( nodeMap.data(), nodeMap.size() );
  for (int k = 0; k < nNodeDOF; k++)
  {
    map_ret = NodeDOFMap_.insert( std::pair<int,int>(nodeMap[k],cntNodeDOF_) );
    if (map_ret.second==true) cntNodeDOF_++; //increment counter if object was actually added
  }

  cellgrp.associativity(elem).getEdgeGlobalMapping( edgeMap.data(), edgeMap.size() );
  for (int k = 0; k < nEdgeDOF; k++)
  {
    map_ret = EdgeDOFMap_.insert( std::pair<int,int>(edgeMap[k],cntEdgeDOF_) );
    if (map_ret.second==true) cntEdgeDOF_++; //increment counter if object was actually added
  }

  cellgrp.associativity(elem).getCellGlobalMapping( cellMap.data(), cellMap.size() );
  for (int k = 0; k < nCellDOF; k++)
  {
    map_ret = CellDOFMap_.insert( std::pair<int,int>(cellMap[k],cntCellDOF_) );
    if (map_ret.second==true) cntCellDOF_++; //increment counter if object was actually added
  }

}

//---------------------------------------------------------------------------//
template <class PhysDim>
void
XField_Local<PhysDim, TopoD2>::extractLocalGrid(int group, int elem)
{

  //Checks
  SANS_ASSERT_MSG( group>=0 && group < global_xfld_.nCellGroups(),
                  "group = %d, nCellGroups() = %d ", group, global_xfld_.nCellGroups());

  SANS_ASSERT_MSG( elem>=0 && elem < global_xfld_.getCellGroupBase(group).nElem(),
                  "cellElem=%d, getCellGroupBase(group).nElem()=%d ",elem,global_xfld_.getCellGroupBase(group).nElem());

  //Clear current local grid
  this->deallocate();

  NodeDOFMap_.clear();
  EdgeDOFMap_.clear();
  CellDOFMap_.clear();

  cntNodeDOF_ = 0;
  cntEdgeDOF_ = 0;
  cntCellDOF_ = 0;

  OuterBTraceGroup_ = -1;

  if ( global_xfld_.getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
  {
    extractMainCell<Triangle>(group, elem);
  }
  else if ( global_xfld_.getCellGroupBase(group).topoTypeID() == typeid(Quad) )
  {
    extractMainCell<Quad>(group, elem);
  }
  else
    SANS_DEVELOPER_EXCEPTION( "XField_Local<PhysDim, TopoD2, T>::extractLocalGrid - Unknown cell topology." );

}

//---------------------------------------------------------------------------//
template <class PhysDim>
template <class TopoMainCell>
void
XField_Local<PhysDim, TopoD2>::extractMainCell(int main_group, int main_elem)
{

  typedef typename XField<PhysDim, TopoD2>::template FieldCellGroupType<TopoMainCell> XFieldCellGroupType;
  typedef typename XField<PhysDim, TopoD2>::template FieldTraceGroupType<Line> XFieldTraceGroupType;
  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef typename XFieldCellGroupType::BasisType BasisType;

  const XFieldCellGroupType& cellgrp_main = global_xfld_.template getCellGroup<TopoMainCell>(main_group);

  const BasisType* cell_basis = cellgrp_main.basis();
  int order = cell_basis->order();

  nTriNeighbors_ = 0;
  nQuadNeighbors_ = 0;
  nNeighbors_ = 0;

  //Count and map required DOFs from global to local mesh
  trackDOFs<TopoMainCell>(main_group, main_elem);

  //Initial loop across neighboring elements to find number of cells/traces/DOFs
  for (int trace = 0; trace < TopoMainCell::NTrace; trace++)
  {
    //Get indices of each neighboring cell (and its cellgroup)
    const TraceInfo& traceinfo = connectivity_.getTrace(main_group, main_elem, trace);

    if (traceinfo.type == TraceInfo::Interior)
    {
      const XFieldTraceGroupType& tracegrp = global_xfld_.template getInteriorTraceGroup<Line>(traceinfo.group);
      int neighbor_group = -1; //cellgroup of neighboring element
      int neighbor_elem = -1; //element index of neighboring element

      if (tracegrp.getElementLeft(traceinfo.elem) != main_elem)
      {
        neighbor_group = tracegrp.getGroupLeft(); //getGroupLeft gives the neighbor's cellgroup
        neighbor_elem = tracegrp.getElementLeft(traceinfo.elem);
      }
      else
      {
        neighbor_group = tracegrp.getGroupRight(); //getGroupRight gives the neighbor's cellgroup
        neighbor_elem = tracegrp.getElementRight(traceinfo.elem);
      }

      if ( global_xfld_.getCellGroupBase(neighbor_group).topoTypeID() == typeid(Triangle) )
      {
        //Count and map required DOFs from global to local mesh
        trackDOFs<Triangle>(neighbor_group, neighbor_elem);
        nTriNeighbors_++; //Increment triangle neighbor count
      }
      else if ( global_xfld_.getCellGroupBase(neighbor_group).topoTypeID() == typeid(Quad) )
      {
        //Count and map required DOFs from global to local mesh
        trackDOFs<Quad>(neighbor_group, neighbor_elem);
        nQuadNeighbors_++; //Increment quad neighbor count
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Unknown cell topology for neighbor." );
    }
  } // initial loop across traces of main cell


  //Count DOFs in local mesh
  int nDOF_local = (int)CellDOFMap_.size() + (int)EdgeDOFMap_.size() + (int)NodeDOFMap_.size();

#if 0
  std::cout << "nDOF_local: " << nDOF_local << std::endl;
  std::cout << "mapNode: " << NodeDOFMap_.size() <<std::endl;
  std::cout << "mapEdge: " << EdgeDOFMap_.size() <<std::endl;
  std::cout << "mapCell: " << CellDOFMap_.size() <<std::endl;
#endif

  //Create local XField
  this->resizeDOF(nDOF_local);

  //Add up neighbor cell counts
  nNeighbors_ = nTriNeighbors_ + nQuadNeighbors_;

  if (nNeighbors_>0)
  {
    if (nTriNeighbors_>0 && nQuadNeighbors_==0) //Only triangle neighbors
    {
      this->resizeCellGroups(2); //2 CellGroups - one for main cell, and one for all neighboring triangles
      this->resizeInteriorTraceGroups(1); //one group for all interior traces
    }
    else if (nTriNeighbors_==0 && nQuadNeighbors_>0) //Only quad neighbors
    {
      this->resizeCellGroups(2); //2 CellGroups - one for main cell, and one for all neighboring quads
      this->resizeInteriorTraceGroups(1); //one group for all interior traces
    }
    else //Both kinds of neighbors
    {
      this->resizeCellGroups(3); //3 CellGroups - one for main cell, one for triangle neighbors and one for quad neighbors
      this->resizeInteriorTraceGroups(2); //one group for all interior traces between triangle neighbors, and one group for quad neighbors
    }
  }
  else //No neighboring cells
  {
    this->resizeCellGroups(1); // one group for main cell
    this->resizeInteriorTraceGroups(0);
  }

  //a group each for each BC of main cell + one group for outer boundaries of neighboring cells
  nMainBTraceGroups_ = TopoMainCell::NTrace - nNeighbors_;
  nOuterBTraceGroups_ = 1;
  if (nNeighbors_ == 0) nOuterBTraceGroups_ = 0; //If there are no neighbors - there is no outer boundary of the neighboring cells
  this->resizeBoundaryTraceGroups(nMainBTraceGroups_);
  this->resizeGhostBoundaryTraceGroups(nOuterBTraceGroups_);

  if (nOuterBTraceGroups_ > 0) OuterBTraceGroup_ = 0;

  //Zero out mesh DOFs
  for (int k = 0; k < this->nDOF_; k++)
    this->DOF_[k] = 0.0;

  //Offsets for cell, edge and node DOFs in the global array (cell DOFs first, edge DOFs second, node DOFs last)
  offset_cellDOF_ = 0;
  offset_edgeDOF_ = (int) CellDOFMap_.size();
  offset_nodeDOF_ = (int) CellDOFMap_.size() + (int) EdgeDOFMap_.size();

  //-------------------------------------------------------------------------------------------------------------
  //Copy and fill associativities of neighboring cells, interior traces and boundary traces
  extractNeighbors(main_group, main_elem, TopoMainCell::NTrace, order);


  //------------------------------Fill in associativity for main cell--------------------------------------------

  // create field associativity constructor for main cell
  typename XFieldCellGroupType::FieldAssociativityConstructorType fldAssocCell_Main( cell_basis, 1 ); //1 cell for main cell group

  ElementAssociativityConstructor<TopoD2, TopoMainCell> DOFAssoc_MainCell( cell_basis );

  setCellAssociativity(main_group, main_elem, DOFAssoc_MainCell);

  //The maincell_reversed_traces_ vector was filled in extractNeighbors()
  for (int k=0; k<(int)maincell_reversed_traces_.size(); k++)
    DOFAssoc_MainCell.setEdgeSign(+1, maincell_reversed_traces_[k]);

  fldAssocCell_Main.setAssociativity( DOFAssoc_MainCell, 0 );

  //Add cell group and copy DOFs for main cell
  this->cellGroups_[0] = new XFieldCellGroupType(fldAssocCell_Main);
  this->cellGroups_[0]->setDOF( this->DOF_, this->nDOF_ );
  this->nElem_ += fldAssocCell_Main.nElem();

  ElementXFieldClass xfldElem_main( cell_basis );
  cellgrp_main.getElement(xfldElem_main, main_elem); //Get current (main) element
  this->template getCellGroup<TopoMainCell>(0).setElement(xfldElem_main,0);

  //Save main cell's mapping to global mesh
  this->CellMapping_[{0,0}] = {main_group, main_elem};
  this->CellSplitInfo_[{0,0}] = ElementSplitInfo(ElementSplitFlag::Unsplit);
}

//---------------------------------------------------------------------------//
template <class PhysDim>
void
XField_Local<PhysDim, TopoD2>::extractNeighbors(int main_group, int main_elem, int Ntrace, int order)
{

  typedef typename XField<PhysDim, TopoD2>::template FieldCellGroupType<Triangle> XFieldCellGroupType_Triangle;
  typedef typename XField<PhysDim, TopoD2>::template FieldCellGroupType<Quad> XFieldCellGroupType_Quad;
  typedef typename XField<PhysDim, TopoD2>::template FieldTraceGroupType<Line> XFieldTraceGroupType;
  typedef typename XFieldCellGroupType_Triangle::template ElementType<> ElementXFieldClass_Triangle;
  typedef typename XFieldCellGroupType_Quad::template ElementType<> ElementXFieldClass_Quad;

  typedef typename XFieldCellGroupType_Triangle::BasisType BasisType_Triangle;
  typedef typename XFieldCellGroupType_Quad::BasisType BasisType_Quad;

  const BasisFunctionLineBase* trace_basis = BasisFunctionLineBase::getBasisFunction(order, basisCategory_);
  const BasisType_Triangle* cell_basis_Tri = NULL;
  const BasisType_Quad* cell_basis_Quad = NULL;

  if (nTriNeighbors_>0)
    cell_basis_Tri = BasisFunctionAreaBase<Triangle>::getBasisFunction(order, basisCategory_);

  if (nQuadNeighbors_>0)
    cell_basis_Quad = BasisFunctionAreaBase<Quad>::getBasisFunction(order, basisCategory_);


  // create field associativity constructor for neighboring cells
  std::shared_ptr<typename XFieldCellGroupType_Triangle::FieldAssociativityConstructorType> fldAssocCell_Neighbors_Tri; //triangles for neighbors
  std::shared_ptr<typename XFieldCellGroupType_Quad::FieldAssociativityConstructorType> fldAssocCell_Neighbors_Quad; //quads for neighbors

  // create field associativity constructor for interior traces
  std::shared_ptr<typename XFieldTraceGroupType::FieldAssociativityConstructorType> fldAssoc_ITrace_Tri;
  std::shared_ptr<typename XFieldTraceGroupType::FieldAssociativityConstructorType> fldAssoc_ITrace_Quad;

  // create field associativity constructor for boundary traces of neighboring cells (2 edges for each neighboring triangle, 3 for each quad)
  std::shared_ptr<typename XFieldTraceGroupType::FieldAssociativityConstructorType> fldAssoc_Outer_BTrace;

  //Assign cell group indices to triangle and quad neighbors (index 0 is for main cell)
  int CellgrpTriNeighbors = 1;
  int CellgrpQuadNeighbors = 2;

  int ITracegrpTriNeighbors = 0;
  int ITracegrpQuadNeighbors = 1;

  if (nNeighbors_ > 0)
  {
    fldAssocCell_Neighbors_Tri  = std::make_shared< typename XFieldCellGroupType_Triangle::FieldAssociativityConstructorType>
                                                 ( cell_basis_Tri, nTriNeighbors_ );

    fldAssocCell_Neighbors_Quad = std::make_shared< typename XFieldCellGroupType_Quad::FieldAssociativityConstructorType>
                                                 ( cell_basis_Quad, nQuadNeighbors_ );

    fldAssoc_ITrace_Tri         = std::make_shared< typename XFieldTraceGroupType::FieldAssociativityConstructorType>
                                                 ( trace_basis, nTriNeighbors_);

    fldAssoc_ITrace_Quad        = std::make_shared< typename XFieldTraceGroupType::FieldAssociativityConstructorType>
                                                 ( trace_basis, nQuadNeighbors_);

    fldAssoc_Outer_BTrace       = std::make_shared< typename XFieldTraceGroupType::FieldAssociativityConstructorType>
                                                 ( trace_basis, 2*nTriNeighbors_ + 3*nQuadNeighbors_);

    //if no triangle neighbors, then promote the quad neighbor cellgroup and the interior trace group
    if (nTriNeighbors_ == 0)
    {
      CellgrpQuadNeighbors--;
      ITracegrpQuadNeighbors--;
    }

  }


  int cnt_neighbor_tri = 0; //counter to keep track of triangles whose associativity has been processed
  int cnt_neighbor_quad = 0; //counter to keep track of quads whose associativity has been processed
  int cnt_main_Btraces = 0; //counter to keep track of the boundary traces of the main cell which have been processed

  int cnt_outer_Btraces = 0; //counter to keep track of the outer boundary traces which have been processed

  std::vector<ElementXFieldClass_Triangle> neighborTriangle_list;
  std::vector<ElementXFieldClass_Quad> neighborQuad_list;

  //-----------Fill in associativity for neighboring cells--------------------

  //Repeat loop across neighboring elements to fill associativity
  for (int itrace=0; itrace< Ntrace; itrace++)
  {
    //Get indices of each neighboring trace (and its cellgroup)
    const TraceInfo& traceinfo = connectivity_.getTrace(main_group, main_elem, itrace);

    if (traceinfo.type == TraceInfo::Interior)
    {
      const XFieldTraceGroupType& tracegrp = global_xfld_.template getInteriorTraceGroup<Line>(traceinfo.group);

      int neighbor_group = -1; //cellgroup of neighboring element in global mesh
      int neighbor_elem = -1; //element index of neighboring element in global mesh
      CanonicalTraceToCell neighbor_canonicalTrace; //canonicalTrace of the neighboring cell for the current trace
      CanonicalTraceToCell main_canonicalTrace; //canonicalTrace of the main cell for the current trace
      bool reverse_edge_flag = false;

      if (tracegrp.getElementLeft(traceinfo.elem) == main_elem)
      { //element to left of trace is the "main_elem" => getElementRight gives the neighboring cell
        neighbor_group          = tracegrp.getGroupRight();
        neighbor_elem           = tracegrp.getElementRight(traceinfo.elem);
        neighbor_canonicalTrace = tracegrp.getCanonicalTraceRight(traceinfo.elem);
        main_canonicalTrace     = tracegrp.getCanonicalTraceLeft(traceinfo.elem);
      }
      else
      { //element to left of trace is not the "main_elem" => getElementLeft gives the neighboring cell
        neighbor_group          = tracegrp.getGroupLeft();
        neighbor_elem           = tracegrp.getElementLeft(traceinfo.elem);
        neighbor_canonicalTrace = tracegrp.getCanonicalTraceLeft(traceinfo.elem);
        main_canonicalTrace     = tracegrp.getCanonicalTraceRight(traceinfo.elem);
        neighbor_canonicalTrace.orientation = -1;
        main_canonicalTrace.orientation = 1;
        reverse_edge_flag = true;
      }

      if ( global_xfld_.getCellGroupBase(neighbor_group).topoTypeID() == typeid(Triangle) )
      {

        const XFieldCellGroupType_Triangle& cellgrp_neighbor = global_xfld_.template getCellGroup<Triangle>(neighbor_group);
        ElementXFieldClass_Triangle xfldElem_neighbor(cell_basis_Tri);
        cellgrp_neighbor.getElement(xfldElem_neighbor,neighbor_elem); //Get neighboring cell
        neighborTriangle_list.push_back(xfldElem_neighbor);

        // DOF associativity for neighbor cells
        ElementAssociativityConstructor<TopoD2, Triangle> DOFAssoc_NeighborCell( cell_basis_Tri );

        setCellAssociativity(neighbor_group, neighbor_elem, DOFAssoc_NeighborCell);

        //Need to reverse edges to ensure that the main cell is always to the left of its neighbors
        if (reverse_edge_flag==true)
        {
          DOFAssoc_NeighborCell.setEdgeSign(-1,neighbor_canonicalTrace.trace);
          maincell_reversed_traces_.push_back(main_canonicalTrace.trace);
        }

        //Save this neighboring cell's mapping from local to global mesh
        this->CellMapping_[{CellgrpTriNeighbors,cnt_neighbor_tri}] = {neighbor_group,neighbor_elem};
        this->CellSplitInfo_[{CellgrpTriNeighbors,cnt_neighbor_tri}] = ElementSplitInfo(ElementSplitFlag::Unsplit);

        neighborcell_reversed_traces_.clear();

        //Add associativity for outer boundary traces of this neighbor cell
        process_OuterBTraces(neighbor_group, neighbor_elem, order, Triangle::NTrace, CellgrpTriNeighbors, cnt_neighbor_tri,
                             neighbor_canonicalTrace, fldAssoc_Outer_BTrace, OuterBTraceGroup_, cnt_outer_Btraces);

        for (int k=0; k<(int)neighborcell_reversed_traces_.size(); k++)
          DOFAssoc_NeighborCell.setEdgeSign(+1, neighborcell_reversed_traces_[k]);

        fldAssocCell_Neighbors_Tri->setAssociativity( DOFAssoc_NeighborCell, cnt_neighbor_tri);

        // DOF associativity for interior trace
        ElementAssociativityConstructor<TopoD1, Line> DOFAssoc_ITrace( trace_basis );

        //Set DOF associativity
        setInteriorTraceAssociativity(traceinfo.group, traceinfo.elem, DOFAssoc_ITrace, reverse_edge_flag);

        int itrace_index = cnt_neighbor_tri; //The new index for this interior trace is simply the number of neighboring cells passed

        fldAssoc_ITrace_Tri->setAssociativity( DOFAssoc_ITrace, itrace_index );

        //Save this interior trace's mapping from local to global mesh
        this->InteriorTraceMapping_[{ITracegrpTriNeighbors,itrace_index}] = {traceinfo.group,traceinfo.elem};
        this->InteriorTraceSplitInfo_[{ITracegrpTriNeighbors,itrace_index}] = ElementSplitInfo(ElementSplitFlag::Unsplit);

        fldAssoc_ITrace_Tri->setGroupLeft(0); //Cell group on left is the main cell's group = 0
        fldAssoc_ITrace_Tri->setElementLeft(0, itrace_index);
        fldAssoc_ITrace_Tri->setCanonicalTraceLeft(main_canonicalTrace, itrace_index);

        fldAssoc_ITrace_Tri->setGroupRight(CellgrpTriNeighbors); //Cell group on right is the triangle neighbors' cell group
        fldAssoc_ITrace_Tri->setElementRight(itrace_index, itrace_index); //Right element for interior trace i is equal to i
        fldAssoc_ITrace_Tri->setCanonicalTraceRight(neighbor_canonicalTrace, itrace_index);

        cnt_neighbor_tri++;
      }
      else if ( global_xfld_.getCellGroupBase(neighbor_group).topoTypeID() == typeid(Quad) )
      {
        const XFieldCellGroupType_Quad& cellgrp_neighbor = global_xfld_.template getCellGroup<Quad>(neighbor_group);
        ElementXFieldClass_Quad xfldElem_neighbor(cell_basis_Quad);
        cellgrp_neighbor.getElement(xfldElem_neighbor,neighbor_elem); //Get neighboring cell
        neighborQuad_list.push_back(xfldElem_neighbor);

        // DOF associativity for neighbor cells
        ElementAssociativityConstructor<TopoD2, Quad> DOFAssoc_NeighborCell( cell_basis_Quad );

        setCellAssociativity(neighbor_group, neighbor_elem, DOFAssoc_NeighborCell);

        if (reverse_edge_flag==true)
        {
          DOFAssoc_NeighborCell.setEdgeSign(-1,neighbor_canonicalTrace.trace);
          maincell_reversed_traces_.push_back(main_canonicalTrace.trace);
        }

        //Save this neighboring cell's mapping from local to global mesh
        this->CellMapping_[{CellgrpQuadNeighbors,cnt_neighbor_quad}] = {neighbor_group,neighbor_elem};
        this->CellSplitInfo_[{CellgrpQuadNeighbors,cnt_neighbor_quad}] = ElementSplitInfo(ElementSplitFlag::Unsplit);

        neighborcell_reversed_traces_.clear();

        //Add associativity for outer boundary traces of this neighbor cell
        process_OuterBTraces(neighbor_group, neighbor_elem, order, Quad::NTrace, CellgrpQuadNeighbors, cnt_neighbor_quad,
                             neighbor_canonicalTrace, fldAssoc_Outer_BTrace, OuterBTraceGroup_, cnt_outer_Btraces);

        for (int k=0; k<(int)neighborcell_reversed_traces_.size(); k++)
          DOFAssoc_NeighborCell.setEdgeSign(+1, neighborcell_reversed_traces_[k]);

        fldAssocCell_Neighbors_Quad->setAssociativity( DOFAssoc_NeighborCell, cnt_neighbor_quad);

        // DOF associativity for interior trace
        ElementAssociativityConstructor<TopoD1, Line> DOFAssoc_ITrace( trace_basis );

        //Set DOF associativity
        setInteriorTraceAssociativity(traceinfo.group, traceinfo.elem, DOFAssoc_ITrace, reverse_edge_flag);

        int itrace_index = cnt_neighbor_quad; //The new index for this interior trace is simply the number of neighboring cells passed

        fldAssoc_ITrace_Quad->setAssociativity( DOFAssoc_ITrace, itrace_index );

        //Save this interior trace's mapping from local to global mesh
        this->InteriorTraceMapping_[{ITracegrpQuadNeighbors,itrace_index}] = {traceinfo.group,traceinfo.elem};
        this->InteriorTraceSplitInfo_[{ITracegrpQuadNeighbors,itrace_index}] = ElementSplitInfo(ElementSplitFlag::Unsplit);

        fldAssoc_ITrace_Quad->setGroupLeft(0); //Cell group on left is the main cell's group = 0
        fldAssoc_ITrace_Quad->setElementLeft(0, itrace_index);
        fldAssoc_ITrace_Quad->setCanonicalTraceLeft(main_canonicalTrace, itrace_index);

        fldAssoc_ITrace_Quad->setGroupRight(CellgrpQuadNeighbors); //Cell group on right is the quad neighbors' cell group
        fldAssoc_ITrace_Quad->setElementRight(itrace_index, itrace_index); //Right element for interior trace i is equal to i
        fldAssoc_ITrace_Quad->setCanonicalTraceRight(neighbor_canonicalTrace, itrace_index);

        cnt_neighbor_quad++;
      }
      else
        SANS_DEVELOPER_EXCEPTION( "XField_Local<PhysDim, TopoD2, T>::extractLocalGrid - Unknown cell topology for neighbor." );

    }
    else if (traceinfo.type == TraceInfo::Boundary) //We are dealing with a boundary trace of the main cell
    {
      const XFieldTraceGroupType& tracegrp = global_xfld_.template getBoundaryTraceGroup<Line>(traceinfo.group);

      //canonicalTrace of the main cell for the current trace
      CanonicalTraceToCell main_canonicalTrace = tracegrp.getCanonicalTraceLeft(traceinfo.elem);

      // DOF associativity for boundary trace
      ElementAssociativityConstructor<TopoD1, Line> DOFAssoc_BTrace( trace_basis );

      setBoundaryTraceAssociativity(tracegrp, traceinfo.elem, DOFAssoc_BTrace);

      // create field associativity constructor for each boundary trace of main cell
      // Note: each boundary trace of the main cell has its own boundaryTraceGroup!
      typename XFieldTraceGroupType::FieldAssociativityConstructorType fldAssoc_Main_BTrace( trace_basis, 1);

      fldAssoc_Main_BTrace.setAssociativity( DOFAssoc_BTrace, 0);

      //Save this boundary trace's mapping from local to global mesh
      this->BoundaryTraceMapping_[{cnt_main_Btraces,0}] = {traceinfo.group,traceinfo.elem};
      this->BoundaryTraceSplitInfo_[{cnt_main_Btraces,0}] = ElementSplitInfo(ElementSplitFlag::Unsplit);


      fldAssoc_Main_BTrace.setGroupLeft(0); //Cell group on left is the main cell's group = 0
      fldAssoc_Main_BTrace.setElementLeft(0, 0);
      fldAssoc_Main_BTrace.setCanonicalTraceLeft(main_canonicalTrace, 0);

      //Add trace group and copy DOFs for boundary traces of main cell
      this->boundaryTraceGroups_[cnt_main_Btraces] = new XFieldTraceGroupType( fldAssoc_Main_BTrace );
      this->boundaryTraceGroups_[cnt_main_Btraces]->setDOF( this->DOF_, this->nDOF_ );
      cnt_main_Btraces++;
    }
  } //repeated loop across traces of main element

  if (nNeighbors_>0)
  {
    if (nTriNeighbors_>0) //Triangle neighbors
    {
      //Add cell group and copy DOFs for neighboring cells
      this->cellGroups_[CellgrpTriNeighbors] = new XFieldCellGroupType_Triangle( *fldAssocCell_Neighbors_Tri );
      this->cellGroups_[CellgrpTriNeighbors]->setDOF( this->DOF_, this->nDOF_ );
      this->nElem_ += fldAssocCell_Neighbors_Tri->nElem();
      for (int k=0; k<nTriNeighbors_; k++)
        this->template getCellGroup<Triangle>(CellgrpTriNeighbors).setElement(neighborTriangle_list[k],k);

      //Add trace group and set DOFs for interior traces
      this->interiorTraceGroups_[ITracegrpTriNeighbors] = new XFieldTraceGroupType( *fldAssoc_ITrace_Tri );
      this->interiorTraceGroups_[ITracegrpTriNeighbors]->setDOF( this->DOF_, this->nDOF_ );
    }

    if (nQuadNeighbors_>0) //Quad neighbors
    {
      //Add cell group and copy DOFs for neighboring cells
      this->cellGroups_[CellgrpQuadNeighbors] = new XFieldCellGroupType_Quad( *fldAssocCell_Neighbors_Quad );
      this->cellGroups_[CellgrpQuadNeighbors]->setDOF( this->DOF_, this->nDOF_ );
      this->nElem_ += fldAssocCell_Neighbors_Quad->nElem();
      for (int k=0; k<nQuadNeighbors_; k++)
        this->template getCellGroup<Quad>(CellgrpQuadNeighbors).setElement(neighborQuad_list[k],k);

      //Add trace group and set DOFs for interior traces
      this->interiorTraceGroups_[ITracegrpQuadNeighbors] = new XFieldTraceGroupType( *fldAssoc_ITrace_Quad );
      this->interiorTraceGroups_[ITracegrpQuadNeighbors]->setDOF( this->DOF_, this->nDOF_ );
    }

    if (nOuterBTraceGroups_>0)
    {
      //Add trace group and copy DOFs for interior traces
      this->ghostBoundaryTraceGroups_[OuterBTraceGroup_] = new XFieldTraceGroupType( *fldAssoc_Outer_BTrace );
      this->ghostBoundaryTraceGroups_[OuterBTraceGroup_]->setDOF( this->DOF_, this->nDOF_ );
    }
  } //if neighbors

}

//---------------------------------------------------------------------------//
template <class PhysDim>
template <class TopoCell>
void
XField_Local<PhysDim, TopoD2>::setCellAssociativity(int group, int elem, ElementAssociativityConstructor<TopoD2, TopoCell>& DOFAssoc)
{

  typedef typename XField<PhysDim, TopoD2>::template FieldCellGroupType<TopoCell> XFieldCellGroupType;

  //Get global cell group
  const XFieldCellGroupType& cellgrp_main = global_xfld_.template getCellGroup<TopoCell>(group);

  int nNodeDOF = cellgrp_main.associativity(elem).nNode();
  int nEdgeDOF = cellgrp_main.associativity(elem).nEdge();
  int nCellDOF = cellgrp_main.associativity(elem).nCell();

  // node DOF, edge DOF and cell DOF ordering for cell
  std::vector<int> Cell_nodeDOF_map(nNodeDOF);
  std::vector<int> Cell_edgeDOF_map(nEdgeDOF);
  std::vector<int> Cell_cellDOF_map(nCellDOF);

  cellgrp_main.associativity(elem).getNodeGlobalMapping( Cell_nodeDOF_map.data(), Cell_nodeDOF_map.size() ); //Get nodeDOF map from global field
  cellgrp_main.associativity(elem).getEdgeGlobalMapping( Cell_edgeDOF_map.data(), Cell_edgeDOF_map.size() ); //Get edgeDOF map from global field
  cellgrp_main.associativity(elem).getCellGlobalMapping( Cell_cellDOF_map.data(), Cell_cellDOF_map.size() ); //Get cellDOF map from global field

  //Map DOFs from global to local mesh
  for (int k = 0; k < nNodeDOF; k++)
    Cell_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Cell_nodeDOF_map[k]);

  for (int k = 0; k < nEdgeDOF; k++)
    Cell_edgeDOF_map[k] = offset_edgeDOF_ + EdgeDOFMap_.at(Cell_edgeDOF_map[k]);

  for (int k = 0; k < nCellDOF; k++)
    Cell_cellDOF_map[k] = offset_cellDOF_ + CellDOFMap_.at(Cell_cellDOF_map[k]);

  DOFAssoc.setRank( 0 ); // local to this processor (the comm is split)
  DOFAssoc.setNodeGlobalMapping( Cell_nodeDOF_map );
  DOFAssoc.setEdgeGlobalMapping( Cell_edgeDOF_map );
  DOFAssoc.setCellGlobalMapping( Cell_cellDOF_map );

  DOFAssoc.edgeSign() = cellgrp_main.associativity(elem).edgeSign();
}

//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Local<PhysDim, TopoD2>::
setInteriorTraceAssociativity(int group, int elem, ElementAssociativityConstructor<TopoD1, Line>& DOFAssoc, bool reverse_edge)
{

  typedef typename XField<PhysDim, TopoD2>::template FieldTraceGroupType<Line> XFieldTraceGroupType;

  //Get global interior trace group
  const XFieldTraceGroupType& tracegrp = global_xfld_.template getInteriorTraceGroup<Line>(group);

  int nNodeDOF = tracegrp.associativity(elem).nNode();
  int nEdgeDOF = tracegrp.associativity(elem).nEdge();

  // node DOF and edge DOF ordering for interior trace
  std::vector<int> Cell_nodeDOF_map(nNodeDOF);
  std::vector<int> Cell_edgeDOF_map(nEdgeDOF);

  tracegrp.associativity(elem).getNodeGlobalMapping( Cell_nodeDOF_map ); //Get nodeDOF map from global field
  tracegrp.associativity(elem).getEdgeGlobalMapping( Cell_edgeDOF_map ); //Get edgeDOF map from global field

  //Map DOFs from global to local mesh
  for (int k = 0; k < nNodeDOF; k++)
    Cell_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Cell_nodeDOF_map[k]);

  for (int k = 0; k < nEdgeDOF; k++)
    Cell_edgeDOF_map[k] = offset_edgeDOF_ + EdgeDOFMap_.at(Cell_edgeDOF_map[k]);

  if (reverse_edge) //Reverse ordering if the trace needs to be reversed
  {
    std::reverse(Cell_nodeDOF_map.begin(), Cell_nodeDOF_map.end());
//    std::reverse(Cell_edgeDOF_map.begin(), Cell_edgeDOF_map.end());
  }

  DOFAssoc.setRank( 0 ); // local to this processor (the comm is split)
  DOFAssoc.setNodeGlobalMapping( Cell_nodeDOF_map );
  DOFAssoc.setEdgeGlobalMapping( Cell_edgeDOF_map );
}

//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Local<PhysDim, TopoD2>::
setBoundaryTraceAssociativity(const typename XField<PhysDim, TopoD2>::template FieldTraceGroupType<Line>& tracegrp,
                              int elem, ElementAssociativityConstructor<TopoD1, Line>& DOFAssoc)
{
  int nNodeDOF = tracegrp.associativity(elem).nNode();
  int nEdgeDOF = tracegrp.associativity(elem).nEdge();

  // node DOF and edge DOF ordering for boundary trace
  std::vector<int> Cell_nodeDOF_map(nNodeDOF);
  std::vector<int> Cell_edgeDOF_map(nEdgeDOF);

  tracegrp.associativity(elem).getNodeGlobalMapping( Cell_nodeDOF_map.data(), Cell_nodeDOF_map.size() ); //Get nodeDOF map from global field
  tracegrp.associativity(elem).getEdgeGlobalMapping( Cell_edgeDOF_map.data(), Cell_edgeDOF_map.size() ); //Get edgeDOF map from global field

  //Map DOFs from global to local mesh
  for (int k = 0; k < nNodeDOF; k++)
    Cell_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Cell_nodeDOF_map[k]);

  for (int k = 0; k < nEdgeDOF; k++)
    Cell_edgeDOF_map[k] = offset_edgeDOF_ + EdgeDOFMap_.at(Cell_edgeDOF_map[k]);

  DOFAssoc.setRank( 0 ); // local to this processor (the comm is split)
  DOFAssoc.setNodeGlobalMapping( Cell_nodeDOF_map );
  DOFAssoc.setEdgeGlobalMapping( Cell_edgeDOF_map );
}

//---------------------------------------------------------------------------//
template<class PhysDim>
void
XField_Local<PhysDim, TopoD2>::process_OuterBTraces(const int neighbor_group, const int neighbor_elem, const int order, const int Ntrace,
                                                     const int local_neighbor_group, const int local_neighbor_elem,
                                                     const CanonicalTraceToCell& neighbor_canonicalTrace,
                                                     std::shared_ptr<typename BaseType::template FieldTraceGroupType<Line>
                                                                                      ::FieldAssociativityConstructorType> fldAssoc_Outer_BTrace,
                                                     const int OuterBTraceGroup, int& cnt_outer_Btraces)
{

  typedef typename XField<PhysDim, TopoD2>::template FieldTraceGroupType<Line> XFieldTraceGroupType;

  const BasisFunctionLineBase* trace_basis = BasisFunctionLineBase::getBasisFunction(order, basisCategory_);

  // DOF associativity for boundary traces of neighbors
  ElementAssociativityConstructor<TopoD1, Line> DOFAssoc_Outer_BTrace( trace_basis );

  for (int j = 0; j < Ntrace; j++) //loop across traces of this neighboring element
  {
    if (j != neighbor_canonicalTrace.trace) //Outer boundary trace
    {
      //Get index of outer trace (and its cellgroup)
      const TraceInfo& outertraceinfo = connectivity_.getTrace(neighbor_group, neighbor_elem, j);

      //canonicalTrace of this neighboring cell for the outer (interior) trace
      CanonicalTraceToCell neighbor_outer_canonicalTrace;

      if (outertraceinfo.type == TraceInfo::Interior)
      {
        bool reverse_edge_flag = false;

        const XFieldTraceGroupType& outer_tracegrp = global_xfld_.template getInteriorTraceGroup<Line>(outertraceinfo.group);

        if (outer_tracegrp.getElementLeft(outertraceinfo.elem) == neighbor_elem)
        { //element to left of outer trace is the current "neighbor_elem"
          neighbor_outer_canonicalTrace = outer_tracegrp.getCanonicalTraceLeft(outertraceinfo.elem);
        }
        else
        { //element to left of outer trace is not the current "neighbor_elem"
          neighbor_outer_canonicalTrace = outer_tracegrp.getCanonicalTraceRight(outertraceinfo.elem);
          neighbor_outer_canonicalTrace.orientation = 1;
          reverse_edge_flag = true;
          neighborcell_reversed_traces_.push_back(j);
        }

        //Set DOF associativity
        setInteriorTraceAssociativity(outertraceinfo.group, outertraceinfo.elem, DOFAssoc_Outer_BTrace, reverse_edge_flag);

      }
      else if ( outertraceinfo.type == TraceInfo::Boundary ) //We are dealing with a boundaryTrace of this neighboring cell
      {
        const XFieldTraceGroupType& outer_tracegrp = global_xfld_.template getBoundaryTraceGroup<Line>(outertraceinfo.group);

        //Set DOF associativity
        setBoundaryTraceAssociativity(outer_tracegrp, outertraceinfo.elem, DOFAssoc_Outer_BTrace);

        //canonicalTrace of this neighboring cell for the outer boundary trace
        neighbor_outer_canonicalTrace = outer_tracegrp.getCanonicalTraceLeft(outertraceinfo.elem);
      }
      else if ( outertraceinfo.type == TraceInfo::GhostBoundary ) //We are dealing with a ghost boundaryTrace of this neighboring cell
      {
        const XFieldTraceGroupType& outer_tracegrp = global_xfld_.template getGhostBoundaryTraceGroup<Line>(outertraceinfo.group);

        //Set DOF associativity
        setBoundaryTraceAssociativity(outer_tracegrp, outertraceinfo.elem, DOFAssoc_Outer_BTrace);

        //canonicalTrace of this neighboring cell for the outer boundary trace
        neighbor_outer_canonicalTrace = outer_tracegrp.getCanonicalTraceLeft(outertraceinfo.elem);
      }
      else
        SANS_DEVELOPER_EXCEPTION("Unknown outer trace info: %d", outertraceinfo.type);

      //Save this boundary trace's mapping from local to global mesh
//      this->BoundaryTraceMapping_[{OuterBTraceGroup,cnt_outer_Btraces}] = {outer_trace_group,outer_trace_elem};
//      this->BoundaryTraceSplitInfo_[{OuterBTraceGroup,cnt_outer_Btraces}] = ElementSplitInfo(ElementSplitFlag::Unsplit);

      //Fill associativity of Outer BoundaryTraceGroup
      fldAssoc_Outer_BTrace->setAssociativity( DOFAssoc_Outer_BTrace, cnt_outer_Btraces);

      fldAssoc_Outer_BTrace->setGroupLeft(local_neighbor_group); //Cell group on left is the triangle neighbor cells' group
      fldAssoc_Outer_BTrace->setElementLeft(local_neighbor_elem, cnt_outer_Btraces); //Cell on left is the neighbor cell
      fldAssoc_Outer_BTrace->setCanonicalTraceLeft(neighbor_outer_canonicalTrace, cnt_outer_Btraces);

      cnt_outer_Btraces++;
    }
  }

}

//Explicit instantiations
template class XField_Local<PhysD2,TopoD2>;
template class XField_Local<PhysD3,TopoD2>;

}
