// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELDSPACETIME_LOCAL_SPLIT_H_
#define XFIELDSPACETIME_LOCAL_SPLIT_H_

#include <vector>

#include "Topology/Dimension.h"

#include "Field/Element/ElementAssociativityVolumeConstructor.h"
#include "Field/Element/ElementAssociativitySpacetimeConstructor.h"

#include "XFieldSpacetime_Local.h"

/*\
 * this is a dummy class!
\*/
namespace SANS
{

template <class PhysDim, class TopoDim>
class XField_Local_Split;

template<class PhysDim>
class XField_Local_Split< PhysDim, TopoD4> : public XField_Local_Base<PhysDim, TopoD4>
{

public:
  typedef XField_Local_Base<PhysDim, TopoD4> BaseType;

  XField_Local_Split(const XField_Local<PhysDim, TopoD4>& xfld_local, XField_CellToTrace<PhysDim,TopoD4>& connectivity,
                     ElementSplitType split_type, int split_edge_index);

  ~XField_Local_Split() {}
};

}
#endif // XFIELDSPACETIME_LOCAL_SPLIT_H_
