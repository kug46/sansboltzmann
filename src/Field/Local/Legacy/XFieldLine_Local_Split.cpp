// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XFieldLine_Local_Split.h"

#include "XFieldLine.h"
#include "Field/Element/ElementProjection_L2.h"

namespace SANS
{

template <class PhysDim>
XField_Local_Split<PhysDim, TopoD1>::XField_Local_Split(const XField_Local<PhysDim, TopoD1>& xfld_local,
                                                        XField_CellToTrace<PhysDim,TopoD1>& connectivity)
  : BaseType(xfld_local.comm()),
    xfld_local_unsplit_(xfld_local), connectivity_(connectivity)
{
  splitMainCell();
  // This is checkGrid() but gets removed in release mode!
  this->checkGrid();
}

template <class PhysDim>
XField_Local_Split<PhysDim, TopoD1>::XField_Local_Split(const XField_Local<PhysDim, TopoD1>& xfld_local,
                                                        XField_CellToTrace<PhysDim,TopoD1>& connectivity,
                                                        ElementSplitType split_type, int split_edge_index)
  : BaseType(xfld_local.comm()),
    xfld_local_unsplit_(xfld_local), connectivity_(connectivity)
{
  SANS_ASSERT_MSG(split_type == ElementSplitType::Isotropic && split_edge_index == -1,
                  "XField_Local_Split<PhysDim, TopoD1>::constructor - split_type needs to be Isotropic.");

  splitMainCell();

  // This is checkGrid() but gets removed in release mode!
  this->checkGrid();

  for (int i = 0; i < this->nBoundaryTraceGroups(); i++)
    reSolveBoundaryTraceGroups_.push_back(i);

  for (int i = 0; i < this->nInteriorTraceGroups(); i++)
    reSolveInteriorTraceGroups_.push_back(i);
}

template <class PhysDim>
void
XField_Local_Split<PhysDim, TopoD1>::splitMainCell()
{
  typedef typename XField<PhysDim, TopoD1>::template FieldCellGroupType<Line> XFieldCellGroupType;
  typedef typename XField<PhysDim, TopoD1>::template FieldTraceGroupType<Node> XFieldTraceGroupType;
  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

  //Clear current local split grid
  this->deallocate();

  NodeDOFMap_.clear();
  EdgeDOFMap_.clear();
  CellDOFMap_.clear();
  this->newNodeDOFs_.clear();

  cntNodeDOF_ = 0;
  cntEdgeDOF_ = 0;
  cntCellDOF_ = 0;

  OuterBTraceGroup_ = -1;

  int main_group = 0;
  int main_elem = 0;

  std::pair<std::map<int,int>::iterator,bool> map_ret; //return value for map inserts

  const XFieldCellGroupType& cellgrp_main = xfld_local_unsplit_.template getCellGroup<Line>(main_group);

  const BasisFunctionLineBase* cell_basis = cellgrp_main.basis();
  int order = cell_basis->order();

  nNeighbors_ = 0;

  //Count and map required DOFs from global to local mesh
  trackDOFs(main_group, main_elem);

  //Add new nodeDOF to split the main cell
  map_ret = NodeDOFMap_.insert( std::pair<int,int>(-1,cntNodeDOF_) );
  if (map_ret.second==true)
  {
    this->newNodeDOFs_.push_back(cntNodeDOF_);
    cntNodeDOF_++; //increment counter if object was actually added
  }

  int nNodeDOF = cellgrp_main.associativity(main_elem).nNode();
  int nEdgeDOF = cellgrp_main.associativity(main_elem).nEdge();

  //Add edgeDOFs for the new cell
  for (int k = 0; k < nEdgeDOF; k++)
  {
    map_ret = EdgeDOFMap_.insert( std::pair<int,int>(-1-k, cntEdgeDOF_) );
    if (map_ret.second==true) cntEdgeDOF_++; //increment counter if object was actually added
  }


  //Initial loop across neighboring elements to find number of cells/traces/DOFs
  for (int i=0; i<Line::NTrace; i++)
  {
    //Get indices of each neighboring cell (and its cellgroup)
    const TraceInfo& traceinfo = connectivity_.getTrace(main_group, main_elem, i);

    if (traceinfo.type == TraceInfo::Interior)
    {
      const XFieldTraceGroupType& tracegrp = xfld_local_unsplit_.template getInteriorTraceGroup<Node>(traceinfo.group);
      int neighbor_group = -1; //cellgroup of neighboring element
      int neighbor_elem = -1; //element index of neighboring element

      if (tracegrp.getElementLeft(traceinfo.elem) == main_elem)
      {
        neighbor_group = tracegrp.getGroupRight(); //getGroupRight gives the neighbor's cellgroup
        neighbor_elem = tracegrp.getElementRight(traceinfo.elem);
      }
      else
        SANS_DEVELOPER_EXCEPTION(
            "XField_Split_Local<PhysDim, TopoD1, T>::splitMainCell - The main-cell is not to the left of its neighbors in the unsplit local mesh." );

      if ( xfld_local_unsplit_.getCellGroupBase(neighbor_group).topoTypeID() == typeid(Line) )
      {
        //Count and map required DOFs from global to local mesh
        trackDOFs(neighbor_group, neighbor_elem);
        nNeighbors_++; //Increment neighbor count
      }
      else
        SANS_DEVELOPER_EXCEPTION( "XField_Local<PhysDim, TopoD1, T>::extractLocalGrid - Unknown cell topology for neighbor." );
    }

  } // initial loop across traces of main cell


  //Count DOFs in local mesh
  int nDOF_local = (int)CellDOFMap_.size() + (int)EdgeDOFMap_.size() + (int)NodeDOFMap_.size();

#if 0
  std::cout << "nDOF_local: " << nDOF_local << std::endl;
  std::cout << "mapNode: " << NodeDOFMap_.size() <<std::endl;
  std::cout << "mapEdge: " << EdgeDOFMap_.size() <<std::endl;
  std::cout << "mapCell: " << CellDOFMap_.size() <<std::endl;
#endif

  //Create local XField
  this->resizeDOF(nDOF_local);

  if (nNeighbors_>0)
  {
    this->resizeCellGroups(2); //2 CellGroups - one for the two main cells, and one for all neighboring cells
    this->resizeInteriorTraceGroups(2); //1 group for interior traces between main and neighbors, and 1 for the interior trace between main cells
  }
  else //No neighboring cells
  {
    this->resizeCellGroups(1); // one group for the two main cells
    this->resizeInteriorTraceGroups(1); //1 group for the interior trace between the main cells
  }

  //a group each for each BC of the main cells + one group for outer boundaries of neighboring cells
  nMainBTraceGroups_ = Line::NTrace - nNeighbors_;
  nOuterBTraceGroups_ = 1;
  if (nNeighbors_ == 0) nOuterBTraceGroups_ = 0; //If there are no neighbors - there is no outer boundary of the neighboring cells
  this->resizeBoundaryTraceGroups(nMainBTraceGroups_);
  this->resizeGhostBoundaryTraceGroups(nOuterBTraceGroups_);

  if (nOuterBTraceGroups_ > 0) OuterBTraceGroup_ = 0;

  //Zero out mesh DOFs
  for (int k = 0; k < this->nDOF_; k++)
    this->DOF_[k] = 0.0;

  //Offsets for cell, edge and node DOFs in the global array (cell DOFs first, edge DOFs second, node DOFs last)
  offset_cellDOF_ = 0;
  offset_edgeDOF_ = (int) CellDOFMap_.size();
  offset_nodeDOF_ = (int) CellDOFMap_.size() + (int) EdgeDOFMap_.size();

  //Offset the newNodeDOF indices
  for (int k = 0; k < (int) this->newNodeDOFs_.size(); k++)
    this->newNodeDOFs_[k] += offset_nodeDOF_;


  //------------------------------Fill in associativity for main cells--------------------------------------------

  // create field associativity constructor for main cell
  typename XFieldCellGroupType::FieldAssociativityConstructorType fldAssocCell_Main( cell_basis, 2 ); //2 cells for main cell group

  ElementAssociativityConstructor<TopoD1, Line> DOFAssoc_MainCell_L( cell_basis );
  ElementAssociativityConstructor<TopoD1, Line> DOFAssoc_MainCell_R( cell_basis );


  //-----------------Modify DOF associativity for sub-cells----------------------

  int rank = cellgrp_main.associativity(main_elem).rank();

  // node DOF and cell DOF ordering for cell
  std::vector<int> CellL_nodeDOF_map(nNodeDOF), CellR_nodeDOF_map(nNodeDOF);
  std::vector<int> CellL_edgeDOF_map(nEdgeDOF), CellR_edgeDOF_map(nEdgeDOF);

  cellgrp_main.associativity(main_elem).getNodeGlobalMapping( CellL_nodeDOF_map.data(), nNodeDOF ); //Get nodeDOF map from unsplit mesh
  cellgrp_main.associativity(main_elem).getEdgeGlobalMapping( CellL_edgeDOF_map.data(), nEdgeDOF ); //Get edgeDOF map from unsplit mesh

  cellgrp_main.associativity(main_elem).getNodeGlobalMapping( CellR_nodeDOF_map.data(), nNodeDOF ); //Get nodeDOF map from unsplit mesh

  CellL_nodeDOF_map[1] = -1; //The right node of the left sub-cell is the newly added nodeDOF
  CellR_nodeDOF_map[0] = -1; //The left node of the right sub-cell is the newly added nodeDOF

  //Map nodeDOFs from unsplit mesh to split mesh
  for (int k = 0; k < nNodeDOF; k++)
  {
    CellL_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(CellL_nodeDOF_map[k]);
    CellR_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(CellR_nodeDOF_map[k]);
  }

  for (int k = 0; k < nEdgeDOF; k++)
  {
    CellL_edgeDOF_map[k] = offset_edgeDOF_ + EdgeDOFMap_.at(CellL_edgeDOF_map[k]);
    CellR_edgeDOF_map[k] = offset_edgeDOF_ + EdgeDOFMap_.at(-1-k); //The edgeDOFs of the right sub-cell are the newly added edgeDOFs
  }

  DOFAssoc_MainCell_L.setRank( rank );
  DOFAssoc_MainCell_R.setRank( rank );

  DOFAssoc_MainCell_L.setNodeGlobalMapping( CellL_nodeDOF_map );
  DOFAssoc_MainCell_L.setEdgeGlobalMapping( CellL_edgeDOF_map );

  DOFAssoc_MainCell_R.setNodeGlobalMapping( CellR_nodeDOF_map );
  DOFAssoc_MainCell_R.setEdgeGlobalMapping( CellR_edgeDOF_map );

  fldAssocCell_Main.setAssociativity( DOFAssoc_MainCell_L, 0 );
  fldAssocCell_Main.setAssociativity( DOFAssoc_MainCell_R, 1 );

  //Add cell group and copy DOFs for main cell
  this->cellGroups_[0] = new XFieldCellGroupType(fldAssocCell_Main);
  this->cellGroups_[0]->setDOF( this->DOF_, this->nDOF_ );
  this->nElem_ += fldAssocCell_Main.nElem();

  ElementXFieldClass xfldElem_main( cell_basis );
  ElementXFieldClass xfldElem_sub( cell_basis );

  cellgrp_main.getElement(xfldElem_main, main_elem); //Get main-cell element from unsplit local mesh

  ElementSplitInfo split_config0(ElementSplitFlag::Split, ElementSplitType::Isotropic, -1, 0);
  ElementSplitInfo split_config1(ElementSplitFlag::Split, ElementSplitType::Isotropic, -1, 1);

  Element_Subdivision_Projector<TopoD1, Line> elemProjector(cell_basis);

  elemProjector.project(xfldElem_main, xfldElem_sub, ElementSplitType::Isotropic, -1, 0);
  this->template getCellGroup<Line>(0).setElement(xfldElem_sub,0);

  elemProjector.project(xfldElem_main, xfldElem_sub, ElementSplitType::Isotropic, -1, 1);
  this->template getCellGroup<Line>(0).setElement(xfldElem_sub,1);

  //Both left and right main cells in split mesh map to the same element in the global mesh
  this->CellMapping_[{0,0}] = xfld_local_unsplit_.getGlobalCellMap({0,0});
  this->CellMapping_[{0,1}] = xfld_local_unsplit_.getGlobalCellMap({0,0});

  //Save split configurations (needed for solution transfer)
  this->CellSplitInfo_[{0,0}] = split_config0;
  this->CellSplitInfo_[{0,1}] = split_config1;

  //-------------------------------------------------------------------------------------------------------------
  //Copy and fill associativities of neighboring cells, interior traces and boundary traces
  extractNeighbors(Line::NTrace, order);
}

template <class PhysDim>
void
XField_Local_Split<PhysDim, TopoD1>::extractNeighbors(int Ntrace, int order)
{

  typedef typename XField<PhysDim, TopoD1>::template FieldCellGroupType<Line> XFieldCellGroupType;
  typedef typename XField<PhysDim, TopoD1>::template FieldTraceGroupType<Node> XFieldTraceGroupType;
  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

  const BasisFunctionLineBase* cell_basis = NULL;

  int main_group = 0;
  int main_elem = 0;

  const XFieldCellGroupType& cellgrp = xfld_local_unsplit_.template getCellGroup<Line>(main_group);

  const BasisFunctionNodeBase* trace_basis = BasisFunctionNodeBase::getBasisFunction(0, BasisFunctionCategory_Legendre);

  // create field associativity constructor for neighboring cells
  std::shared_ptr<typename XFieldCellGroupType::FieldAssociativityConstructorType> fldAssocCell_Neighbors;

  // create field associativity constructor for interior traces
  std::shared_ptr<typename XFieldTraceGroupType::FieldAssociativityConstructorType> fldAssoc_ITrace;

  // create field associativity constructor for boundary traces of neighboring cells (1 edge for each neighboring line)
  std::shared_ptr<typename XFieldTraceGroupType::FieldAssociativityConstructorType> fldAssoc_Outer_BTrace;

  //Assign cell group and trace group indices to neighbors (index 0 is for main cell's group)
  int CellgrpNeighbors = 1;
  int ITracegrpNeighbors = -1; //Trace group for interior traces between main cells and neighbors
  int ITracegrpSplitter = 0; //Trace group for interior trace between main cells

  if (nNeighbors_ > 0)
  {
    cell_basis = cellgrp.basis();

    fldAssocCell_Neighbors = std::make_shared< typename XFieldCellGroupType::FieldAssociativityConstructorType>
                                             ( cell_basis, nNeighbors_ );

    fldAssoc_ITrace        = std::make_shared< typename XFieldTraceGroupType::FieldAssociativityConstructorType>
                                             ( trace_basis, nNeighbors_);

    fldAssoc_Outer_BTrace  = std::make_shared< typename XFieldTraceGroupType::FieldAssociativityConstructorType>
                                             ( trace_basis, 1*nNeighbors_);

    //Promote trace group indices if there are neighbors
    ITracegrpNeighbors++;
    ITracegrpSplitter++;
  }


  int cnt_neighbor = 0; //counter to keep track of lines whose associativity has been processed
  int cnt_main_Btraces = 0; //counter to keep track of the boundary traces of the main cell which have been processed

  int cnt_outer_Btraces = 0; //counter to keep track of the outer boundary traces which have been processed

  std::vector<ElementXFieldClass> neighbor_list;

  //-----------Fill in associativity for neighboring cells--------------------

  //Repeat loop across neighboring elements to fill associativity
  for (int itrace = 0; itrace < Ntrace; itrace++)
  {
    //Get indices of each neighboring trace (and its cellgroup)
    const TraceInfo& traceinfo = connectivity_.getTrace(main_group, main_elem, itrace);

    if (traceinfo.type == TraceInfo::Interior)
    {
      const XFieldTraceGroupType& tracegrp = xfld_local_unsplit_.template getInteriorTraceGroup<Node>(traceinfo.group);

      int neighbor_group = -1; //cellgroup of neighboring element in global mesh
      int neighbor_elem = -1; //element index of neighboring element in global mesh
      CanonicalTraceToCell neighbor_canonicalTrace; //canonicalTrace of the neighboring cell for the current trace
      CanonicalTraceToCell main_canonicalTrace; //canonicalTrace of the main cell for the current trace

      if (tracegrp.getElementLeft(traceinfo.elem) == main_elem)
      { //element to left of trace is the "main_elem" => getElementRight gives the neighboring cell
        neighbor_group          = tracegrp.getGroupRight();
        neighbor_elem           = tracegrp.getElementRight(traceinfo.elem);
        neighbor_canonicalTrace = tracegrp.getCanonicalTraceRight(traceinfo.elem);
        main_canonicalTrace     = tracegrp.getCanonicalTraceLeft(traceinfo.elem);
      }
      else
        SANS_DEVELOPER_EXCEPTION("XField_Split_Local<PhysDim, TopoD1, T>::extractNeighbors - "
                                 "The main-cell is not to the left of its neighbors in the unsplit local mesh.");


      if ( xfld_local_unsplit_.getCellGroupBase(neighbor_group).topoTypeID() == typeid(Line) )
      {

        const XFieldCellGroupType& cellgrp_neighbor = xfld_local_unsplit_.template getCellGroup<Line>(neighbor_group);
        ElementXFieldClass xfldElem_neighbor(cell_basis);
        cellgrp_neighbor.getElement(xfldElem_neighbor,neighbor_elem); //Get neighboring cell
        neighbor_list.push_back(xfldElem_neighbor);

        // DOF associativity for neighbor cells
        ElementAssociativityConstructor<TopoD1, Line> DOFAssoc_NeighborCell( cell_basis );

        setCellAssociativity(neighbor_group, neighbor_elem, DOFAssoc_NeighborCell);
        fldAssocCell_Neighbors->setAssociativity( DOFAssoc_NeighborCell, cnt_neighbor);

        //Save this neighboring cell's mapping from local to global mesh
        this->CellMapping_[{CellgrpNeighbors,cnt_neighbor}] = xfld_local_unsplit_.getGlobalCellMap({CellgrpNeighbors,cnt_neighbor});
        this->CellSplitInfo_[{CellgrpNeighbors,cnt_neighbor}] = ElementSplitInfo(ElementSplitFlag::Unsplit);

        // DOF associativity for boundary traces of neighbors
        ElementAssociativityConstructor<TopoD0, Node> DOFAssoc_Outer_BTrace( trace_basis );

        for (int j = 0; j < Line::NTrace; j++) //loop across traces of this neighboring element
        {
          if (j != neighbor_canonicalTrace.trace) //Outer boundary trace
          {
            //Get index of outer trace (and its cellgroup)
            const TraceInfo& outertraceinfo = connectivity_.getTrace(neighbor_group, neighbor_elem, j);

            //canonicalTrace of this neighboring cell for the outer (interior) trace
            CanonicalTraceToCell neighbor_outer_canonicalTrace;

            if (outertraceinfo.type == TraceInfo::GhostBoundary) //We are dealing with an Outer BoundaryTrace of this neighboring cell
            {
              //Set DOF associativity
              setGhostBoundaryTraceAssociativity(outertraceinfo.group, outertraceinfo.elem, DOFAssoc_Outer_BTrace);

              const XFieldTraceGroupType& outer_tracegrp = xfld_local_unsplit_.template getGhostBoundaryTraceGroup<Node>(outertraceinfo.group);

              //canonicalTrace of this neighboring cell for the outer boundary trace
              neighbor_outer_canonicalTrace = outer_tracegrp.getCanonicalTraceLeft(outertraceinfo.elem);
            }
            else
              SANS_DEVELOPER_EXCEPTION("XField_Split_Local<PhysDim, TopoD1, T>::extractNeighbors - "
              "outer_trace_group (= %d) is not of type GhostBoundary!", outertraceinfo.group);


            //Save this boundary trace's mapping from local to global mesh
//            this->BoundaryTraceMapping_[{OuterBTraceGroup_,cnt_outer_Btraces}] =
//                  xfld_local_unsplit_.getGlobalBoundaryTraceMap({OuterBTraceGroup_,cnt_outer_Btraces});
//
//            this->BoundaryTraceSplitInfo_[{OuterBTraceGroup_,cnt_outer_Btraces}] = ElementSplitInfo(ElementSplitFlag::Unsplit);

            //Fill associativity of Outer BoundaryTraceGroup
            fldAssoc_Outer_BTrace->setAssociativity( DOFAssoc_Outer_BTrace, cnt_outer_Btraces);

            fldAssoc_Outer_BTrace->setGroupLeft(CellgrpNeighbors); //Cell group on left is the triangle neighbor cells' group
            fldAssoc_Outer_BTrace->setElementLeft(cnt_neighbor, cnt_outer_Btraces); //Cell on left is the neighbor cell
            fldAssoc_Outer_BTrace->setCanonicalTraceLeft(neighbor_outer_canonicalTrace, cnt_outer_Btraces);

            cnt_outer_Btraces++;
          }
        }

        // DOF associativity for interior trace
        ElementAssociativityConstructor<TopoD0, Node> DOFAssoc_ITrace( trace_basis );

        //Set DOF associativity
        setInteriorTraceAssociativity(traceinfo.group, traceinfo.elem, DOFAssoc_ITrace);

        int itrace_index = cnt_neighbor; //The new index for this interior trace is simply the number of neighboring cells passed

        fldAssoc_ITrace->setAssociativity( DOFAssoc_ITrace, itrace_index );

        //Save this interior trace's mapping from split to global mesh
        this->InteriorTraceMapping_[{ITracegrpNeighbors,itrace_index}] =
            xfld_local_unsplit_.getGlobalInteriorTraceMap({ITracegrpNeighbors,itrace_index});

        this->InteriorTraceSplitInfo_[{ITracegrpNeighbors,itrace_index}] = ElementSplitInfo(ElementSplitFlag::Unsplit);

        fldAssoc_ITrace->setGroupLeft(0); //Cell group on left is the main cell's group = 0

             if (DOFAssoc_ITrace.normalSignL() == -1)
          fldAssoc_ITrace->setElementLeft(0, itrace_index); //The left element is the left main cell
        else if (DOFAssoc_ITrace.normalSignL() == +1)
          fldAssoc_ITrace->setElementLeft(1, itrace_index); //The left element is the right main cell

        fldAssoc_ITrace->setCanonicalTraceLeft(main_canonicalTrace, itrace_index);

        fldAssoc_ITrace->setGroupRight(CellgrpNeighbors); //Cell group on right is the neighbors' cell group
        fldAssoc_ITrace->setElementRight(itrace_index, itrace_index); //Right element for interior trace i is equal to i
        fldAssoc_ITrace->setCanonicalTraceRight(neighbor_canonicalTrace, itrace_index);

        cnt_neighbor++;
      }
      else
        SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD1, T>::extractLocalGrid - Unknown cell topology for neighbor." );

    }
    else if (traceinfo.type == TraceInfo::Boundary) //We are dealing with a boundary trace of the main cell
    {
      const XFieldTraceGroupType& tracegrp = xfld_local_unsplit_.template getBoundaryTraceGroup<Node>(traceinfo.group);

      //canonicalTrace of the main cell for the current trace
      CanonicalTraceToCell main_canonicalTrace = tracegrp.getCanonicalTraceLeft(traceinfo.elem);

      // DOF associativity for boundary trace
      ElementAssociativityConstructor<TopoD0, Node> DOFAssoc_BTrace( trace_basis );

      setBoundaryTraceAssociativity(traceinfo.group, traceinfo.elem, DOFAssoc_BTrace);

      // create field associativity constructor for each boundary trace of main cell
      // Note: each boundary trace of the main cell has its own boundaryTraceGroup!
      typename XFieldTraceGroupType::FieldAssociativityConstructorType fldAssoc_Main_BTrace( trace_basis, 1);

      int trace_index = 0;
      fldAssoc_Main_BTrace.setAssociativity( DOFAssoc_BTrace, trace_index);

      //Save this boundary trace's mapping from local to global mesh
      this->BoundaryTraceMapping_[{cnt_main_Btraces,0}] = xfld_local_unsplit_.getGlobalBoundaryTraceMap({cnt_main_Btraces,0});
      this->BoundaryTraceSplitInfo_[{cnt_main_Btraces,0}] = ElementSplitInfo(ElementSplitFlag::Unsplit);

      fldAssoc_Main_BTrace.setGroupLeft(0); //Cell group on left is the main cell's group = 0

           if (DOFAssoc_BTrace.normalSignL() == -1)
        fldAssoc_Main_BTrace.setElementLeft(0, trace_index); //The left element is the left main cell
      else if (DOFAssoc_BTrace.normalSignL() == +1)
        fldAssoc_Main_BTrace.setElementLeft(1, trace_index); //The left element is the right main cell

      fldAssoc_Main_BTrace.setCanonicalTraceLeft(main_canonicalTrace, trace_index);

      //Add trace group and copy DOFs for boundary traces of main cell
      this->boundaryTraceGroups_[cnt_main_Btraces] = new XFieldTraceGroupType( fldAssoc_Main_BTrace );
      this->boundaryTraceGroups_[cnt_main_Btraces]->setDOF( this->DOF_, this->nDOF_ );
      cnt_main_Btraces++;
    }
  } //repeated loop across traces of main element

  if (nNeighbors_>0)
  {
    //Add cell group and copy DOFs for neighboring cells
    this->cellGroups_[CellgrpNeighbors] = new XFieldCellGroupType( *fldAssocCell_Neighbors );
    this->cellGroups_[CellgrpNeighbors]->setDOF( this->DOF_, this->nDOF_ );
    this->nElem_ += fldAssocCell_Neighbors->nElem();
    for (int k=0; k<nNeighbors_; k++)
      this->template getCellGroup<Line>(CellgrpNeighbors).setElement(neighbor_list[k],k);

    //Add trace group and set DOFs for interior traces
    this->interiorTraceGroups_[ITracegrpNeighbors] = new XFieldTraceGroupType( *fldAssoc_ITrace );
    this->interiorTraceGroups_[ITracegrpNeighbors]->setDOF( this->DOF_, this->nDOF_ );

    if (nOuterBTraceGroups_>0)
    {
      //Add trace group and copy DOFs for interior traces
      this->ghostBoundaryTraceGroups_[OuterBTraceGroup_] = new XFieldTraceGroupType( *fldAssoc_Outer_BTrace );
      this->ghostBoundaryTraceGroups_[OuterBTraceGroup_]->setDOF( this->DOF_, this->nDOF_ );
    }
  }

  /**********************************************************************************************************/
  // Add the interior trace that separates the two main-cells
  /**********************************************************************************************************/

  typename XFieldTraceGroupType::FieldAssociativityConstructorType fldAssoc_ITrace_Splitter( trace_basis, 1);

  ElementAssociativityConstructor<TopoD0, Node> DOFAssoc_ITrace_Splitter( trace_basis );

  DOFAssoc_ITrace_Splitter.setRank( cellgrp.associativity(main_elem).rank() );

  // node DOF ordering for interior trace
  int Trace_nodeDOF_map[1];

  //Map nodeDOFs from split mesh to unsplit mesh
  Trace_nodeDOF_map[0] = offset_nodeDOF_ + NodeDOFMap_.at(-1); // -1 gives the newly added node

  DOFAssoc_ITrace_Splitter.setNodeGlobalMapping( Trace_nodeDOF_map, 1 );
  DOFAssoc_ITrace_Splitter.setNormalSignL(+1);

  const int trace_index = 0;
  fldAssoc_ITrace_Splitter.setAssociativity( DOFAssoc_ITrace_Splitter, trace_index);

  //Save this interior trace's mapping from split to global mesh
  this->InteriorTraceSplitInfo_[{ITracegrpSplitter, trace_index}] = ElementSplitInfo(ElementSplitFlag::New, trace_index);

  fldAssoc_ITrace_Splitter.setGroupLeft(0); //Cell group on left is the main cell's group = 0
  fldAssoc_ITrace_Splitter.setElementLeft(0, trace_index); //Left element is the left main cell
  fldAssoc_ITrace_Splitter.setCanonicalTraceLeft(CanonicalTraceToCell(0,0), trace_index);

  fldAssoc_ITrace_Splitter.setGroupRight(0); //Cell group on right is the main cell's group = 0
  fldAssoc_ITrace_Splitter.setElementRight(1, trace_index); //Right element is the right main cell
  fldAssoc_ITrace_Splitter.setCanonicalTraceRight(CanonicalTraceToCell(1,0), trace_index);

  //Add trace group and set DOFs for interior trace
  this->interiorTraceGroups_[ITracegrpSplitter] = new XFieldTraceGroupType( fldAssoc_ITrace_Splitter );
  this->interiorTraceGroups_[ITracegrpSplitter]->setDOF( this->DOF_, this->nDOF_ );

}

template <class PhysDim>
void
XField_Local_Split<PhysDim, TopoD1>::trackDOFs(int group, int elem)
{
  typedef typename XField<PhysDim, TopoD1>::template FieldCellGroupType<Line> XFieldCellGroupType;

  std::pair<std::map<int,int>::iterator,bool> map_ret; //return value for map inserts

  const XFieldCellGroupType& cellgrp = xfld_local_unsplit_.template getCellGroup<Line>(group);

  int nNodeDOF = cellgrp.associativity(elem).nNode();
  int nEdgeDOF = cellgrp.associativity(elem).nEdge();

  // node DOF, edge DOF and cell DOF maps
  std::vector<int> nodeMap(nNodeDOF);
  std::vector<int> edgeMap(nEdgeDOF);

  //Save DOF mappings from unsplit mesh to split mesh
  cellgrp.associativity(elem).getNodeGlobalMapping( nodeMap.data(), nNodeDOF );
  for (int k = 0; k < nNodeDOF; k++)
  {
    map_ret = NodeDOFMap_.insert( std::pair<int,int>(nodeMap[k],cntNodeDOF_) );
    if (map_ret.second==true) cntNodeDOF_++; //increment counter if object was actually added
  }

  cellgrp.associativity(elem).getEdgeGlobalMapping( edgeMap.data(), nEdgeDOF );
  for (int k = 0; k < nEdgeDOF; k++)
  {
    map_ret = EdgeDOFMap_.insert( std::pair<int,int>(edgeMap[k],cntEdgeDOF_) );
    if (map_ret.second==true) cntEdgeDOF_++; //increment counter if object was actually added
  }

}

template <class PhysDim>
void
XField_Local_Split<PhysDim, TopoD1>::setCellAssociativity(int group, int elem, ElementAssociativityConstructor<TopoD1, Line>& DOFAssoc)
{

  typedef typename XField<PhysDim, TopoD1>::template FieldCellGroupType<Line> XFieldCellGroupType;

  //Get global cell group for main cell
  const XFieldCellGroupType& cellgrp = xfld_local_unsplit_.template getCellGroup<Line>(group);

  int nNodeDOF = cellgrp.associativity(elem).nNode();
  int nEdgeDOF = cellgrp.associativity(elem).nEdge();

  // node DOF and cell DOF ordering for cell
  std::vector<int> Cell_nodeDOF_map(nNodeDOF);
  std::vector<int> Cell_edgeDOF_map(nEdgeDOF);

  cellgrp.associativity(elem).getNodeGlobalMapping( Cell_nodeDOF_map.data(), nNodeDOF ); //Get nodeDOF map from global field
  cellgrp.associativity(elem).getEdgeGlobalMapping( Cell_edgeDOF_map.data(), nEdgeDOF ); //Get edgeDOF map from global field

  //Map nodeDOFs from global to local mesh
  for (int k = 0; k < nNodeDOF; k++)
    Cell_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Cell_nodeDOF_map[k]);

  for (int k = 0; k < nEdgeDOF; k++)
    Cell_edgeDOF_map[k] = offset_edgeDOF_ + EdgeDOFMap_.at(Cell_edgeDOF_map[k]);

  DOFAssoc.setRank( cellgrp.associativity(elem).rank() );
  DOFAssoc.setNodeGlobalMapping( Cell_nodeDOF_map );
  DOFAssoc.setEdgeGlobalMapping( Cell_edgeDOF_map );

}

template<class PhysDim>
void
XField_Local_Split< PhysDim, TopoD1>::setInteriorTraceAssociativity(int group, int elem, ElementAssociativityConstructor<TopoD0, Node>& DOFAssoc)
{

  typedef typename XField<PhysDim, TopoD1>::template FieldTraceGroupType<Node> XFieldTraceGroupType;

  //Get global cell group
  const XFieldTraceGroupType& tracegrp = xfld_local_unsplit_.template getInteriorTraceGroup<Node>(group);

  const int nNodeDOF = Node::NNode;

  // node DOF ordering for interior trace
  int Cell_nodeDOF_map[nNodeDOF];

  tracegrp.associativity(elem).getNodeGlobalMapping( Cell_nodeDOF_map, nNodeDOF ); //Get nodeDOF map from global field

  //Map nodeDOFs from global to local mesh
  for (int k = 0; k < nNodeDOF; k++)
    Cell_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Cell_nodeDOF_map[k]);

  DOFAssoc.setRank( tracegrp.associativity(elem).rank() );
  DOFAssoc.setNodeGlobalMapping( Cell_nodeDOF_map, nNodeDOF );

  DOFAssoc.setNormalSignL(tracegrp.associativity(elem).normalSignL());
}


template<class PhysDim>
void
XField_Local_Split< PhysDim, TopoD1>::setBoundaryTraceAssociativity(int group, int elem, ElementAssociativityConstructor<TopoD0, Node>& DOFAssoc)
{
  typedef typename XField<PhysDim, TopoD1>::template FieldTraceGroupType<Node> XFieldTraceGroupType;

  //Get global cell group
  const XFieldTraceGroupType& tracegrp = xfld_local_unsplit_.template getBoundaryTraceGroup<Node>(group);

  const int nNodeDOF = Node::NNode;

  // node DOF ordering for boundary trace
  int Cell_nodeDOF_map[nNodeDOF];

  tracegrp.associativity(elem).getNodeGlobalMapping( Cell_nodeDOF_map, nNodeDOF ); //Get nodeDOF map from global field

  //Map nodeDOFs from global to local mesh
  for (int k = 0; k < nNodeDOF; k++)
    Cell_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Cell_nodeDOF_map[k]);

  DOFAssoc.setRank( tracegrp.associativity(elem).rank() );
  DOFAssoc.setNodeGlobalMapping( Cell_nodeDOF_map, nNodeDOF );

  DOFAssoc.setNormalSignL(tracegrp.associativity(elem).normalSignL());
}

template<class PhysDim>
void
XField_Local_Split< PhysDim, TopoD1>::setGhostBoundaryTraceAssociativity(int group, int elem, ElementAssociativityConstructor<TopoD0, Node>& DOFAssoc)
{
  typedef typename XField<PhysDim, TopoD1>::template FieldTraceGroupType<Node> XFieldTraceGroupType;

  //Get global cell group
  const XFieldTraceGroupType& tracegrp = xfld_local_unsplit_.template getGhostBoundaryTraceGroup<Node>(group);

  const int nNodeDOF = Node::NNode;

  // node DOF ordering for boundary trace
  int Cell_nodeDOF_map[nNodeDOF];

  tracegrp.associativity(elem).getNodeGlobalMapping( Cell_nodeDOF_map, nNodeDOF ); //Get nodeDOF map from global field

  //Map nodeDOFs from global to local mesh
  for (int k = 0; k < nNodeDOF; k++)
    Cell_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Cell_nodeDOF_map[k]);

  DOFAssoc.setRank( tracegrp.associativity(elem).rank() );
  DOFAssoc.setNodeGlobalMapping( Cell_nodeDOF_map, nNodeDOF );

  DOFAssoc.setNormalSignL(tracegrp.associativity(elem).normalSignL());
}

//Explicit instantiations
template class XField_Local_Split<PhysD1,TopoD1>;
template class XField_Local_Split<PhysD2,TopoD1>;

}
