// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XFieldArea_Local_Split_Linear.h"

#include "XFieldArea.h"
#include "Field/Element/ElementProjection_L2.h"

namespace SANS
{

template <>
void updateNodeDOFMaps_TraceSplit<Triangle>(const std::vector<int>& trace_nodes, const int new_nodeDOF,
                                  int nodeDOF_map0[], int nodeDOF_map1[], const int nNodeDOF)
{
  //Updating nodeDOF mappings to include the new DOF
  for (int k=0; k<nNodeDOF; k++)
  {
    if (nodeDOF_map0[k]==trace_nodes[1])
      nodeDOF_map0[k] = new_nodeDOF;

    if (nodeDOF_map1[k]==trace_nodes[0])
      nodeDOF_map1[k] = new_nodeDOF;
  }
}

template <>
void updateNodeDOFMaps_TraceSplit<Quad>(const std::vector<int>& trace_nodes, const int new_nodeDOF,
                                  int nodeDOF_map0[], int nodeDOF_map1[], const int nNodeDOF)
{
  SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD2, T>::updateNodeDOFMaps_TraceSplit - Quad splitting not supported yet.");
}

template <class PhysDim>
void
XField_Local_Split_Linear<PhysDim, TopoD2>::split(ElementSplitType split_type, int split_edge_index)
{
  //Clear current local grid
  this->deallocate();

  NodeDOFMap_.clear();
  this->newNodeDOFs_.clear();

  cntNodeDOF_ = 0;

  OuterBTraceGroup_ = -1;

  //Classify the split type
  if (split_type == ElementSplitType::Edge)
  {
    IsotropicSplitFlag_ = false;
    SplitTraceIndex_ = split_edge_index;
  }
  else if (split_type == ElementSplitType::Isotropic)
  {
    if (split_edge_index == -1)
    {
      IsotropicSplitFlag_ = true;
      SplitTraceIndex_ = -1;
    }
    else
      SANS_DEVELOPER_EXCEPTION("XField_Split_Local<PhysDim, TopoD2, T>::split - "
                               "Split edge index (=%d) should be -1 for an isotropic-split.", split_edge_index);
  }
  else
    SANS_DEVELOPER_EXCEPTION("XField_Split_Local<PhysDim, TopoD2, T>::split - Invalid ElementSplitType.");


  if ( xfld_local_unsplit_.getCellGroupBase(mainGroup_).topoTypeID() == typeid(Triangle) )
  {
    //Checks
    if ((!IsotropicSplitFlag_ && SplitTraceIndex_ >= 0 && SplitTraceIndex_ < Triangle::NTrace) ||
        ( IsotropicSplitFlag_ && SplitTraceIndex_ == -1))
    {
      splitMainCell<Triangle>();
    }
    else
      SANS_DEVELOPER_EXCEPTION("XField_Split_Local<PhysDim, TopoD2, T>::split - "
                               "Invalid split_type and split_edge_index (=%d) combination.", split_edge_index);
  }
  else if ( xfld_local_unsplit_.getCellGroupBase(mainGroup_).topoTypeID() == typeid(Quad) )
  {
    //Checks
    if ((!IsotropicSplitFlag_ && SplitTraceIndex_ >= 0 && SplitTraceIndex_ < Quad::NTrace) ||
        ( IsotropicSplitFlag_ && SplitTraceIndex_ == -1))
    {
      SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD2, T>::splitMainCell - Quad splitting not supported yet." );
//      splitMainCell<Quad>();
    }
    else
      SANS_DEVELOPER_EXCEPTION("XField_Split_Local<PhysDim, TopoD2, T>::split - "
                               "Invalid split_type and split_edge_index (=%d) combination.", split_edge_index);
  }
  else
    SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD2, T>::split - Unknown cell topology." );

  // This is checkGrid() but gets removed in release mode!
  this->checkGrid();
}


template <class PhysDim>
template <class TopoMainCell>
void
XField_Local_Split_Linear<PhysDim, TopoD2>::splitMainCell()
{

  typedef typename XField<PhysDim, TopoD2>::template FieldCellGroupType<TopoMainCell> XFieldCellGroupType;
  typedef typename XField<PhysDim, TopoD2>::template FieldTraceGroupType<Line> XFieldTraceGroupType;
//  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef typename XFieldCellGroupType::BasisType BasisType;

  std::pair<std::map<int,int>::iterator,bool> map_ret; //return value for map inserts

//  const XFieldCellGroupType& cellgrp_main = xfld_local_unsplit_.template getCellGroup<TopoMainCell>(mainGroup_);

  int order = 1; //We're constructing a linear split mesh
  const BasisType* cell_basis = BasisFunctionAreaBase<TopoMainCell>::getBasisFunction(order, basisCategory_);
  const BasisFunctionLineBase* trace_basis = BasisFunctionLineBase::getBasisFunction(order, basisCategory_);

  nTriNeighbors_ = 0;
  nQuadNeighbors_ = 0;
  nNeighbors_ = 0;

  nSplitTriNeighbors_ = 0;
  nSplitQuadNeighbors_ = 0;
  nSplitMainBTraces_ = 0;

  //Count and map required DOFs from global to local mesh
  trackDOFs<TopoMainCell>(mainGroup_, mainElem_);

  if ( xfld_local_unsplit_.getCellGroupBase(mainGroup_).topoTypeID() == typeid(Triangle) )
  {
    if (IsotropicSplitFlag_) //Isotropic split
    {
      //Add 3 new nodeDOFs to split the main cell
      for (int k = 0; k < 3; k++)
      {
        map_ret = NodeDOFMap_.insert( std::pair<int,int>(-1-k,cntNodeDOF_) );
        if (map_ret.second==true)
        {
          this->newNodeDOFs_.push_back(cntNodeDOF_);
          cntNodeDOF_++; //increment counter if object was actually added
        }
      }
    }
    else //Trace split
    {
      //Add new nodeDOF to split the main cell
      map_ret = NodeDOFMap_.insert( std::pair<int,int>(-1,cntNodeDOF_) );
      if (map_ret.second==true)
      {
        this->newNodeDOFs_.push_back(cntNodeDOF_);
        cntNodeDOF_++; //increment counter if object was actually added
      }
    }
  }
  else if ( xfld_local_unsplit_.getCellGroupBase(mainGroup_).topoTypeID() == typeid(Quad) )
  {
    SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD2, T>::splitMainCell - Quad splitting not supported yet." );
  }
  else
    SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD2, T>::splitMainCell - Unknown cell topology for neighbor." );


  //Info about the trace to be split
  TraceInfo splitting_traceinfo;

  //Initial loop across neighboring elements to find number of cells/traces/DOFs
  for (int i = 0; i < TopoMainCell::NTrace; i++)
  {
    //Get indices of each neighboring cell (and its cellgroup)
    const TraceInfo& traceinfo = connectivity_.getTrace(mainGroup_, mainElem_, i);

    if (traceinfo.type == TraceInfo::Interior)
    {
      const XFieldTraceGroupType& tracegrp = xfld_local_unsplit_.template getInteriorTraceGroup<Line>(traceinfo.group);
      int neighbor_group = -1; //cellgroup of neighboring element
      int neighbor_elem = -1; //element index of neighboring element

      if (tracegrp.getElementLeft(traceinfo.elem) == mainElem_)
      {
        neighbor_group = tracegrp.getGroupRight(); //getGroupRight gives the neighbor's cellgroup
        neighbor_elem = tracegrp.getElementRight(traceinfo.elem);
      }
      else
        SANS_DEVELOPER_EXCEPTION("XField_Split_Local<PhysDim, TopoD2, T>::splitMainCell - "
                                 "The main-cell is not to the left of its neighbors in the unsplit local mesh.");

      if ( xfld_local_unsplit_.getCellGroupBase(neighbor_group).topoTypeID() == typeid(Triangle) )
      {
        //Count and map required DOFs from global to local mesh
        trackDOFs<Triangle>(neighbor_group, neighbor_elem);
        nTriNeighbors_++; //Increment triangle neighbor count

        if ((i==SplitTraceIndex_) || IsotropicSplitFlag_)
        {
          splitting_traceinfo = traceinfo;
          nTriNeighbors_++;
          nSplitTriNeighbors_++;
        }
      }
      else if ( xfld_local_unsplit_.getCellGroupBase(neighbor_group).topoTypeID() == typeid(Quad) )
      {
        SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD2, T>::splitMainCell - Quad splitting not supported yet." );
//        //Count and map required DOFs from global to local mesh
//        trackDOFs<Quad>(neighbor_group, neighbor_elem);
//        nQuadNeighbors_++; //Increment quad neighbor count
      }
      else
        SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD2, T>::splitMainCell - Unknown cell topology for neighbor." );
    }
    else if (traceinfo.type == TraceInfo::Boundary)
    { //MainBoundaryTrace
      if ((i==SplitTraceIndex_) || IsotropicSplitFlag_)
      {
        splitting_traceinfo = traceinfo;
        nSplitMainBTraces_++;
      }
    }
    else
      SANS_DEVELOPER_EXCEPTION("XField_Split_Local<PhysDim, TopoD2, T>::splitMainCell - Unknown trace type.");

  } // initial loop across traces of main cell


  //Count DOFs in local mesh
  int nDOF_local = (int)NodeDOFMap_.size();

#if 0
  std::cout << "nDOF_local: " << nDOF_local << std::endl;
  std::cout << "mapNode: " << NodeDOFMap_.size() <<std::endl;
  std::cout << "nNTriNeighbors: " << nTriNeighbors_ <<std::endl;
  std::cout << "nNQuadNeighbors: " << nQuadNeighbors_ <<std::endl;
#endif

  //Create local XField
  this->resizeDOF(nDOF_local);

  //Add up neighbor cell counts
  nNeighbors_ = nTriNeighbors_ + nQuadNeighbors_;

  if (nNeighbors_>0)
  {
    if (nTriNeighbors_>0 && nQuadNeighbors_==0) //Only triangle neighbors
    {
      this->resizeCellGroups(2); //2 CellGroups - one for main cells, and one for all neighboring triangles

      if (nSplitTriNeighbors_ > 0)
        this->resizeInteriorTraceGroups(3); //three groups :(main-to-tri-neighbor, main-main and tri-neighbor-neighbor interfaces)
      else
        this->resizeInteriorTraceGroups(2); //two groups :(main-to-tri-neighbor, main-main)
    }
    else if (nTriNeighbors_==0 && nQuadNeighbors_>0) //Only quad neighbors
    {
      this->resizeCellGroups(2); //2 CellGroups - one for main cells, and one for all neighboring quads

      if (nSplitQuadNeighbors_ > 0)
        this->resizeInteriorTraceGroups(3); //three groups :(main-to-quad-neighbor, main-main and quad-neighbor-neighbor interfaces)
      else
        this->resizeInteriorTraceGroups(2); //two groups :(main-to-quad-neighbor, main-main)
    }
    else //Both kinds of neighbors
    {
      this->resizeCellGroups(3); //3 CellGroups - one for main cell, one for triangle neighbors and one for quad neighbors

      if (nSplitTriNeighbors_ > 0 && nSplitQuadNeighbors_ == 0)
        this->resizeInteriorTraceGroups(4); //four groups :(main-to-tri-neighbor, main-to-quad-neighbor, main-main
                                            //              and tri-neighbor-neighbor interfaces)
      else if (nSplitTriNeighbors_ == 0 && nSplitQuadNeighbors_ > 0)
        this->resizeInteriorTraceGroups(4); //four groups :(main-to-tri-neighbor, main-to-quad-neighbor, main-main
                                            //              and quad-neighbor-neighbor interfaces)
      else
        this->resizeInteriorTraceGroups(5); //five groups :(main-to-tri-neighbor, main-to-quad-neighbor, main-main
                                            //              tri-neighbor-neighbor, and quad-neighbor-neighbor interfaces)
    }

  }
  else //No neighboring cells
  {
    this->resizeCellGroups(1); // one group for main cell
    this->resizeInteriorTraceGroups(1); //one group for interfaces between main cells
  }

  //a group each for each BC of main cell + one group for outer boundaries of neighboring cells
  nMainBTraceGroups_ = TopoMainCell::NTrace - (nNeighbors_ - nSplitTriNeighbors_ - nSplitQuadNeighbors_);
  nOuterBTraceGroups_ = 1;
  if (nNeighbors_ == 0) nOuterBTraceGroups_ = 0; //If there are no neighbors - there is no outer boundary of the neighboring cells
  this->resizeBoundaryTraceGroups(nMainBTraceGroups_);
  this->resizeGhostBoundaryTraceGroups(nOuterBTraceGroups_);

  if (nOuterBTraceGroups_ > 0) OuterBTraceGroup_ = 0;

  ITraceGroupInterMain_ = -1;

  //Zero out mesh DOFs
  for (int k = 0; k < this->nDOF_; k++)
    this->DOF_[k] = 0.0;

  //Offset for node DOFs in the global array (cell DOFs first, edge DOFs second, node DOFs last)
  offset_nodeDOF_ = 0;

  //-------------------------------------------------------------------------------------------------------------
  //Copy and fill associativities of neighboring cells, interior traces and boundary traces
  splitNeighbors(TopoMainCell::NTrace, order);


  //------------------------------Fill in associativity for main cells--------------------------------------------

  int nMainCells = -1;
  int nInterMainTraces = -1;
  if (IsotropicSplitFlag_)
  {
    nMainCells = 4;  //number of main sub-cells
    nInterMainTraces = TopoMainCell::NTrace;  //number of interior traces between main sub-cells

    // create field associativity constructor for main cell
    typename XFieldCellGroupType::FieldAssociativityConstructorType fldAssocCell_Main( cell_basis, nMainCells );

    ElementAssociativityConstructor<TopoD2, TopoMainCell> DOFAssoc_MainCell( cell_basis );
    ElementAssociativityConstructor<TopoD1, Line> DOFAssoc_ITrace( trace_basis );

    std::vector< ElementAssociativityConstructor<TopoD2, TopoMainCell> > DOFAssocs_MainSubCell(4, DOFAssoc_MainCell);
    std::vector< ElementAssociativityConstructor<TopoD1, Line> > DOFAssocs_MidITrace(nInterMainTraces, DOFAssoc_ITrace);

    std::vector< CanonicalTraceToCell > TracesMid_canonicalL(nInterMainTraces);
    std::vector< CanonicalTraceToCell > TracesMid_canonicalR(nInterMainTraces);

    std::vector<int> new_nodeDOFs(TopoMainCell::NTrace);
    for (int i=0; i<TopoMainCell::NTrace; i++)
      new_nodeDOFs[i] = -i-1;

    setMainCellAssociativity_IsotropicSplit<TopoMainCell>(new_nodeDOFs,
                                                          DOFAssocs_MainSubCell, DOFAssocs_MidITrace,
                                                          TracesMid_canonicalL, TracesMid_canonicalR);

    for (int i=0; i<nMainCells; i++)
    {
      fldAssocCell_Main.setAssociativity( DOFAssocs_MainSubCell[i], i );

      //All main sub-cells in split mesh map to the same main-cell in the unsplit mesh
      this->CellMapping_[{0,i}] = xfld_local_unsplit_.getGlobalCellMap({mainGroup_,mainElem_});

      //Save split configurations (needed for solution transfer)
      ElementSplitInfo split_config(ElementSplitFlag::Split, ElementSplitType::Isotropic, -1, i);
      this->CellSplitInfo_[{0,i}] = split_config;
    }

    //Add cell group and copy DOFs for main cell
    this->cellGroups_[0] = new XFieldCellGroupType(fldAssocCell_Main);
    this->cellGroups_[0]->setDOF( this->DOF_, this->nDOF_ );
    this->nElem_ += fldAssocCell_Main.nElem();

    // create field associativity constructor for interior traces between main sub-cells
    std::shared_ptr<typename XFieldTraceGroupType::FieldAssociativityConstructorType> fldAssoc_ITrace_InterMain; //main-to-main

    fldAssoc_ITrace_InterMain = std::make_shared< typename XFieldTraceGroupType::FieldAssociativityConstructorType>
                                               ( trace_basis, nInterMainTraces);

    for (int itrace_mid = 0; itrace_mid < nInterMainTraces; itrace_mid++)
    {
      fldAssoc_ITrace_InterMain->setAssociativity( DOFAssocs_MidITrace[itrace_mid], itrace_mid );

      fldAssoc_ITrace_InterMain->setGroupLeft(mainGroup_);
      fldAssoc_ITrace_InterMain->setGroupRight(mainGroup_);
      fldAssoc_ITrace_InterMain->setElementLeft(itrace_mid, itrace_mid);
      fldAssoc_ITrace_InterMain->setElementRight(3, itrace_mid);
      fldAssoc_ITrace_InterMain->setCanonicalTraceLeft (TracesMid_canonicalL[itrace_mid], itrace_mid);
      fldAssoc_ITrace_InterMain->setCanonicalTraceRight(TracesMid_canonicalR[itrace_mid], itrace_mid);

      this->InteriorTraceSplitInfo_[{ITraceGroupInterMain_,itrace_mid}] = ElementSplitInfo(ElementSplitFlag::New, itrace_mid);
    }

    //Add trace group and set DOFs for interior traces
    this->interiorTraceGroups_[ITraceGroupInterMain_] = new XFieldTraceGroupType( *fldAssoc_ITrace_InterMain );
    this->interiorTraceGroups_[ITraceGroupInterMain_]->setDOF( this->DOF_, this->nDOF_ );
  }
  else
  {
    nMainCells = 2;  //number of main sub-cells
    nInterMainTraces = 1; //number of interior traces between main sub-cells

    // create field associativity constructor for main cell
    typename XFieldCellGroupType::FieldAssociativityConstructorType fldAssocCell_Main( cell_basis, nMainCells );

    ElementAssociativityConstructor<TopoD2, TopoMainCell> DOFAssoc_MainCell0( cell_basis );
    ElementAssociativityConstructor<TopoD2, TopoMainCell> DOFAssoc_MainCell1( cell_basis );

    // DOF associativity for interior traces
    ElementAssociativityConstructor<TopoD1, Line> DOFAssoc_ITraceMid( trace_basis );

    CanonicalTraceToCell ITraceMid_canonicalL, ITraceMid_canonicalR;

    int new_nodeDOF = -1;

    setMainCellAssociativity_TraceSplit<TopoMainCell>(splitting_traceinfo, new_nodeDOF,
                                                      DOFAssoc_MainCell0, DOFAssoc_MainCell1, DOFAssoc_ITraceMid,
                                                      ITraceMid_canonicalL, ITraceMid_canonicalR);

    fldAssocCell_Main.setAssociativity( DOFAssoc_MainCell0, 0 );
    fldAssocCell_Main.setAssociativity( DOFAssoc_MainCell1, 1 );

    //Add cell group and copy DOFs for main cell
    this->cellGroups_[0] = new XFieldCellGroupType(fldAssocCell_Main);
    this->cellGroups_[0]->setDOF( this->DOF_, this->nDOF_ );
    this->nElem_ += fldAssocCell_Main.nElem();

    //Both left and right main cells in split mesh map to the same element in the global mesh
    this->CellMapping_[{0,0}] = xfld_local_unsplit_.getGlobalCellMap({0,0});
    this->CellMapping_[{0,1}] = xfld_local_unsplit_.getGlobalCellMap({0,0});

    //Save split configurations (needed for solution transfer)
    this->CellSplitInfo_[{0,0}] = ElementSplitInfo(ElementSplitFlag::Split, ElementSplitType::Edge, SplitTraceIndex_, 0);
    this->CellSplitInfo_[{0,1}] = ElementSplitInfo(ElementSplitFlag::Split, ElementSplitType::Edge, SplitTraceIndex_, 1);

    // create field associativity constructor for interior traces between main sub-cells
    std::shared_ptr<typename XFieldTraceGroupType::FieldAssociativityConstructorType> fldAssoc_ITrace_InterMain; //main-to-main

    fldAssoc_ITrace_InterMain = std::make_shared< typename XFieldTraceGroupType::FieldAssociativityConstructorType>
                                               ( trace_basis, nInterMainTraces);

    const int itrace_mid_index = 0;
    fldAssoc_ITrace_InterMain->setAssociativity( DOFAssoc_ITraceMid, itrace_mid_index );

    fldAssoc_ITrace_InterMain->setGroupLeft(mainGroup_);
    fldAssoc_ITrace_InterMain->setGroupRight(mainGroup_);
    fldAssoc_ITrace_InterMain->setElementLeft(0, itrace_mid_index);
    fldAssoc_ITrace_InterMain->setElementRight(1, itrace_mid_index);
    fldAssoc_ITrace_InterMain->setCanonicalTraceLeft(ITraceMid_canonicalL, itrace_mid_index);
    fldAssoc_ITrace_InterMain->setCanonicalTraceRight(ITraceMid_canonicalR, itrace_mid_index);

    this->InteriorTraceSplitInfo_[{ITraceGroupInterMain_,itrace_mid_index}] = ElementSplitInfo(ElementSplitFlag::New, itrace_mid_index);

    //Add trace group and set DOFs for interior traces
    this->interiorTraceGroups_[ITraceGroupInterMain_] = new XFieldTraceGroupType( *fldAssoc_ITrace_InterMain );
    this->interiorTraceGroups_[ITraceGroupInterMain_]->setDOF( this->DOF_, this->nDOF_ );
  }

}

template <class PhysDim>
void
XField_Local_Split_Linear<PhysDim, TopoD2>::splitNeighbors(int Ntrace, int order)
{

  typedef typename XField<PhysDim, TopoD2>::template FieldCellGroupType<Triangle> XFieldCellGroupType_Triangle;
  typedef typename XField<PhysDim, TopoD2>::template FieldCellGroupType<Quad> XFieldCellGroupType_Quad;
  typedef typename XField<PhysDim, TopoD2>::template FieldTraceGroupType<Line> XFieldTraceGroupType;
//  typedef typename XFieldCellGroupType_Triangle::template ElementType<> ElementXFieldClass_Triangle;
//  typedef typename XFieldCellGroupType_Quad::template ElementType<> ElementXFieldClass_Quad;

  typedef typename XFieldCellGroupType_Triangle::BasisType BasisType_Triangle;
  typedef typename XFieldCellGroupType_Quad::BasisType BasisType_Quad;

  const BasisFunctionLineBase* trace_basis = BasisFunctionLineBase::getBasisFunction(order, basisCategory_);
  const BasisType_Triangle* cell_basis_Tri = NULL;
  const BasisType_Quad* cell_basis_Quad = NULL;

  if (nTriNeighbors_>0)
    cell_basis_Tri = BasisFunctionAreaBase<Triangle>::getBasisFunction(order, basisCategory_);

  if (nQuadNeighbors_>0)
    cell_basis_Quad = BasisFunctionAreaBase<Quad>::getBasisFunction(order, basisCategory_);

  // create field associativity constructor for neighboring cells
  std::shared_ptr<typename XFieldCellGroupType_Triangle::FieldAssociativityConstructorType> fldAssocCell_Neighbors_Tri; //triangles for neighbors
  std::shared_ptr<typename XFieldCellGroupType_Quad::FieldAssociativityConstructorType> fldAssocCell_Neighbors_Quad; //quads for neighbors

  // create field associativity constructor for interior traces
  std::shared_ptr<typename XFieldTraceGroupType::FieldAssociativityConstructorType> fldAssoc_ITrace_Tri;
  std::shared_ptr<typename XFieldTraceGroupType::FieldAssociativityConstructorType> fldAssoc_ITrace_Quad;
  std::shared_ptr<typename XFieldTraceGroupType::FieldAssociativityConstructorType> fldAssoc_ITrace_InterTriNeighbor; //tri neighbor-to-neighbor
  std::shared_ptr<typename XFieldTraceGroupType::FieldAssociativityConstructorType> fldAssoc_ITrace_InterQuadNeighbor; //quad neighbor-to-neighbor

  // create field associativity constructor for boundary traces of neighboring cells (2 edges for each neighboring triangle, 3 for each quad)
  std::shared_ptr<typename XFieldTraceGroupType::FieldAssociativityConstructorType> fldAssoc_Outer_BTrace;

  //Assign cell group indices to triangle and quad neighbors (index 0 is for main cell)
  CellGroupTriNeighbors_ = 1;
  CellGroupQuadNeighbors_ = 2;

  int ITraceGroupTriNeighbors = 0;
  int ITraceGroupQuadNeighbors = 1;
  ITraceGroupInterMain_ = 2; //interfaces between split main-cells
  int ITraceGroupInterTriNeighbor = 3; //interfaces between split tri neighbor-cells
  int ITraceGroupInterQuadNeighbor = 4; //interfaces between split quad neighbor-cells

  if (nNeighbors_ > 0)
  {
    fldAssocCell_Neighbors_Tri  = std::make_shared< typename XFieldCellGroupType_Triangle::FieldAssociativityConstructorType>
                                                 ( cell_basis_Tri, nTriNeighbors_ );

    fldAssocCell_Neighbors_Quad = std::make_shared< typename XFieldCellGroupType_Quad::FieldAssociativityConstructorType>
                                                 ( cell_basis_Quad, nQuadNeighbors_ );

    fldAssoc_ITrace_Tri         = std::make_shared< typename XFieldTraceGroupType::FieldAssociativityConstructorType>
                                                 ( trace_basis, nTriNeighbors_);

    fldAssoc_ITrace_Quad        = std::make_shared< typename XFieldTraceGroupType::FieldAssociativityConstructorType>
                                                 ( trace_basis, nQuadNeighbors_);

    fldAssoc_ITrace_InterTriNeighbor  = std::make_shared< typename XFieldTraceGroupType::FieldAssociativityConstructorType>
                                                 ( trace_basis, nSplitTriNeighbors_);

    fldAssoc_ITrace_InterQuadNeighbor = std::make_shared< typename XFieldTraceGroupType::FieldAssociativityConstructorType>
                                                 ( trace_basis, nSplitQuadNeighbors_);

    fldAssoc_Outer_BTrace       = std::make_shared< typename XFieldTraceGroupType::FieldAssociativityConstructorType>
                                                 ( trace_basis,  2*(nTriNeighbors_ - nSplitTriNeighbors_)
                                                               + 3*nQuadNeighbors_ + nSplitQuadNeighbors_);

    //if no triangle neighbors, then promote the quad neighbor cellgroup and the interior trace group indices
    if (nTriNeighbors_==0)
    {
      CellGroupTriNeighbors_ = -1;
      ITraceGroupTriNeighbors = -1;
      ITraceGroupInterTriNeighbor = -1;

      CellGroupQuadNeighbors_--;
      ITraceGroupQuadNeighbors--;
      ITraceGroupInterMain_--;
      ITraceGroupInterQuadNeighbor -= 2;
    }

    //if no quad neighbors, then promote the interior trace group indices
    if (nQuadNeighbors_==0)
    {
      CellGroupQuadNeighbors_ = -1;
      ITraceGroupQuadNeighbors = -1;
      ITraceGroupInterQuadNeighbor = -1;

      ITraceGroupInterMain_--;
      ITraceGroupInterTriNeighbor--;
    }

  }
  else //no neighbors
  {
    CellGroupTriNeighbors_ = -1;
    ITraceGroupTriNeighbors = -1;
    ITraceGroupInterTriNeighbor = -1;
    CellGroupQuadNeighbors_ = -1;
    ITraceGroupQuadNeighbors = -1;
    ITraceGroupInterQuadNeighbor = -1;

    ITraceGroupInterMain_ = 0;
  }

  int cnt_neighbor_tri = 0; //counter to keep track of triangles whose associativity has been processed
//  int cnt_neighbor_quad = 0; //counter to keep track of quads whose associativity has been processed
  int cnt_main_btracegroups = 0; //counter to keep track of the boundary trace groups of the main cell which have been processed

  int cnt_neighbor_tri_splits = 0; //counter to keep track of how many neighboring triangles have been split

  int cnt_outer_Btraces = 0; //counter to keep track of the outer boundary traces which have been processed

  //-----------Fill in associativity for neighboring cells--------------------

  //Repeat loop across neighboring elements to fill associativity
  for (int itrace = 0; itrace < Ntrace; itrace++)
  {
    //Get indices of each neighboring trace (and its cellgroup)
    const TraceInfo& traceinfo = connectivity_.getTrace(mainGroup_, mainElem_, itrace);

    if (traceinfo.type == TraceInfo::Interior)
    {
      const XFieldTraceGroupType& tracegrp = xfld_local_unsplit_.template getInteriorTraceGroup<Line>(traceinfo.group);

      int neighbor_group = -1; //cellgroup of neighboring element in global mesh
      int neighbor_elem = -1; //element index of neighboring element in global mesh
      CanonicalTraceToCell neighbor_canonicalTrace; //canonicalTrace of the neighboring cell for the current trace
      CanonicalTraceToCell main_canonicalTrace; //canonicalTrace of the main cell for the current trace

      if (tracegrp.getElementLeft(traceinfo.elem) == mainElem_)
      { //element to left of trace is the "mainElem_" => getElementRight gives the neighboring cell
        neighbor_group          = tracegrp.getGroupRight();
        neighbor_elem           = tracegrp.getElementRight(traceinfo.elem);
        neighbor_canonicalTrace = tracegrp.getCanonicalTraceRight(traceinfo.elem);
        main_canonicalTrace     = tracegrp.getCanonicalTraceLeft(traceinfo.elem);
      }
      else
        SANS_DEVELOPER_EXCEPTION("XField_Split_Local<PhysDim, TopoD2, T>::splitNeighbors - "
                                 "The main-cell is not to the left of its neighbors in the unsplit local mesh.");


      if ( xfld_local_unsplit_.getCellGroupBase(neighbor_group).topoTypeID() == typeid(Triangle) )
      {

//        const XFieldCellGroupType_Triangle& cellgrp_neighbor = xfld_local_unsplit_.template getCellGroup<Triangle>(neighbor_group);

        if ((itrace==SplitTraceIndex_) || IsotropicSplitFlag_)
        { //Split neighbor

          int new_nodeDOF = -1;
          if (IsotropicSplitFlag_)
          {
            new_nodeDOF = -1 - itrace; //Many new nodes were added, since it was an isotropic split.
          }
          else
            new_nodeDOF = -1; //Only one new node was added, since it was a trace split.


          ElementAssociativityConstructor<TopoD2, Triangle> DOFAssoc_NeighborSubCell0( cell_basis_Tri );
          ElementAssociativityConstructor<TopoD2, Triangle> DOFAssoc_NeighborSubCell1( cell_basis_Tri );

          // DOF associativity for interior traces
          ElementAssociativityConstructor<TopoD1, Line> DOFAssoc_ITrace0( trace_basis );
          ElementAssociativityConstructor<TopoD1, Line> DOFAssoc_ITrace1( trace_basis );
          ElementAssociativityConstructor<TopoD1, Line> DOFAssoc_ITraceMid( trace_basis );
          CanonicalTraceToCell ITraceMid_canonicalL, ITraceMid_canonicalR;


          setCellAssociativity_TraceSplit<Triangle>(neighbor_group, neighbor_elem,
                                                    traceinfo.group, traceinfo.elem, new_nodeDOF,
                                                    DOFAssoc_NeighborSubCell0, DOFAssoc_NeighborSubCell1,
                                                    DOFAssoc_ITrace0, DOFAssoc_ITrace1, DOFAssoc_ITraceMid,
                                                    ITraceMid_canonicalL, ITraceMid_canonicalR);

          fldAssocCell_Neighbors_Tri->setAssociativity( DOFAssoc_NeighborSubCell0, cnt_neighbor_tri );
          fldAssocCell_Neighbors_Tri->setAssociativity( DOFAssoc_NeighborSubCell1, cnt_neighbor_tri+1 );

          //Both left and right main cells in split mesh map to the same element in the global mesh
          this->CellMapping_[{CellGroupTriNeighbors_,cnt_neighbor_tri  }] =
              xfld_local_unsplit_.getGlobalCellMap({CellGroupTriNeighbors_,(cnt_neighbor_tri - cnt_neighbor_tri_splits)});

          this->CellMapping_[{CellGroupTriNeighbors_,cnt_neighbor_tri+1}] =
              xfld_local_unsplit_.getGlobalCellMap({CellGroupTriNeighbors_,(cnt_neighbor_tri - cnt_neighbor_tri_splits)});

          //The sub_cell_index (0 or 1) order is reversed because neighbor_canonicalTrace has an orientation of -1.
          this->CellSplitInfo_[{CellGroupTriNeighbors_,cnt_neighbor_tri  }] =
              ElementSplitInfo(ElementSplitFlag::Split, ElementSplitType::Edge, neighbor_canonicalTrace.trace, 1);

          this->CellSplitInfo_[{CellGroupTriNeighbors_,cnt_neighbor_tri+1}] =
              ElementSplitInfo(ElementSplitFlag::Split, ElementSplitType::Edge, neighbor_canonicalTrace.trace, 0);

          //Add associativity for outer boundary traces of this neighbor cell
          process_SplitOuterBTraces(neighbor_group, neighbor_elem, order, Triangle::NTrace, CellGroupTriNeighbors_, cnt_neighbor_tri,
                                    neighbor_canonicalTrace, DOFAssoc_NeighborSubCell0, DOFAssoc_NeighborSubCell1,
                                    fldAssoc_Outer_BTrace, OuterBTraceGroup_, cnt_outer_Btraces);


          //Set DOF associativity for interior traces between main-cell and neighbor-cells

          int itrace_index0 = cnt_neighbor_tri; //The new index for this interior trace is simply the number of neighboring cells passed
          int itrace_index1 = cnt_neighbor_tri + 1;

          fldAssoc_ITrace_Tri->setAssociativity( DOFAssoc_ITrace0, itrace_index0 );
          fldAssoc_ITrace_Tri->setAssociativity( DOFAssoc_ITrace1, itrace_index1 );

          //Save this interior trace's mapping from local to global mesh
          this->InteriorTraceMapping_[{ITraceGroupTriNeighbors,itrace_index0}] =
              xfld_local_unsplit_.getGlobalInteriorTraceMap({ITraceGroupTriNeighbors,itrace_index0 - cnt_neighbor_tri_splits});

          this->InteriorTraceMapping_[{ITraceGroupTriNeighbors,itrace_index1}] =
              xfld_local_unsplit_.getGlobalInteriorTraceMap({ITraceGroupTriNeighbors,itrace_index0 - cnt_neighbor_tri_splits});

          //The original interior trace is split isotropically into 2 sub-traces
          this->InteriorTraceSplitInfo_[{ITraceGroupTriNeighbors,itrace_index0}] =
              ElementSplitInfo(ElementSplitFlag::Split, ElementSplitType::Isotropic, -1, 0);

          this->InteriorTraceSplitInfo_[{ITraceGroupTriNeighbors,itrace_index1}] =
              ElementSplitInfo(ElementSplitFlag::Split, ElementSplitType::Isotropic, -1, 1);


          //Get the indices of the main sub-cells on the left based on split-configuration and trace index
          std::vector<int> main_subelemlist = getMainSubCellElements(itrace);

          fldAssoc_ITrace_Tri->setGroupLeft(0); //Cell group on left is the main cell's group = 0
          fldAssoc_ITrace_Tri->setElementLeft(main_subelemlist[0], itrace_index0);
          fldAssoc_ITrace_Tri->setElementLeft(main_subelemlist[1], itrace_index1);
          fldAssoc_ITrace_Tri->setCanonicalTraceLeft(main_canonicalTrace, itrace_index0);
          fldAssoc_ITrace_Tri->setCanonicalTraceLeft(main_canonicalTrace, itrace_index1);

          fldAssoc_ITrace_Tri->setGroupRight(CellGroupTriNeighbors_); //Cell group on right is the triangle neighbors' cell group
          fldAssoc_ITrace_Tri->setElementRight(itrace_index0, itrace_index0); //Right element for interior trace i is equal to i
          fldAssoc_ITrace_Tri->setElementRight(itrace_index1, itrace_index1); //Right element for interior trace i is equal to i
          fldAssoc_ITrace_Tri->setCanonicalTraceRight(neighbor_canonicalTrace, itrace_index0);
          fldAssoc_ITrace_Tri->setCanonicalTraceRight(neighbor_canonicalTrace, itrace_index1);


          //Set DOF associativity for interior traces between neighbor-cells
          int itrace_mid_index = -1;

          if (IsotropicSplitFlag_)
            itrace_mid_index = cnt_neighbor_tri_splits;
          else
            itrace_mid_index = 0;

          fldAssoc_ITrace_InterTriNeighbor->setAssociativity( DOFAssoc_ITraceMid, itrace_mid_index );

          fldAssoc_ITrace_InterTriNeighbor->setGroupLeft(CellGroupTriNeighbors_);
          fldAssoc_ITrace_InterTriNeighbor->setGroupRight(CellGroupTriNeighbors_);
          fldAssoc_ITrace_InterTriNeighbor->setElementLeft(itrace_index0, itrace_mid_index);
          fldAssoc_ITrace_InterTriNeighbor->setElementRight(itrace_index1, itrace_mid_index);
          fldAssoc_ITrace_InterTriNeighbor->setCanonicalTraceLeft(ITraceMid_canonicalL, itrace_mid_index);
          fldAssoc_ITrace_InterTriNeighbor->setCanonicalTraceRight(ITraceMid_canonicalR, itrace_mid_index);

          const int sub_trace_index = 0;
          this->InteriorTraceSplitInfo_[{ITraceGroupInterTriNeighbor,itrace_mid_index}] = ElementSplitInfo(ElementSplitFlag::New, sub_trace_index);

          cnt_neighbor_tri += 2; //Added two (split)-neighbors
          cnt_neighbor_tri_splits ++; //increment tri-neighbor split count

        }
        else
        { //Unsplit neighbor

          // DOF associativity for neighbor cells
          ElementAssociativityConstructor<TopoD2, Triangle> DOFAssoc_NeighborCell( cell_basis_Tri );

          setCellAssociativity(neighbor_group, neighbor_elem, DOFAssoc_NeighborCell);


          //Save this neighboring cell's mapping from local to global mesh
          this->CellMapping_[{CellGroupTriNeighbors_,cnt_neighbor_tri}] =
              xfld_local_unsplit_.getGlobalCellMap({CellGroupTriNeighbors_,(cnt_neighbor_tri - cnt_neighbor_tri_splits)});

          this->CellSplitInfo_[{CellGroupTriNeighbors_,cnt_neighbor_tri}] = ElementSplitInfo(ElementSplitFlag::Unsplit);


          //Add associativity for outer boundary traces of this neighbor cell
          process_OuterBTraces(neighbor_group, neighbor_elem, order, Triangle::NTrace, CellGroupTriNeighbors_, cnt_neighbor_tri,
                               neighbor_canonicalTrace, fldAssoc_Outer_BTrace, OuterBTraceGroup_, cnt_outer_Btraces);


          fldAssocCell_Neighbors_Tri->setAssociativity( DOFAssoc_NeighborCell, cnt_neighbor_tri);

          // DOF associativity for interior trace
          ElementAssociativityConstructor<TopoD1, Line> DOFAssoc_ITrace( trace_basis );

          //Set DOF associativity
          setInteriorTraceAssociativity(traceinfo.group, traceinfo.elem, DOFAssoc_ITrace);

          int itrace_index = cnt_neighbor_tri; //The new index for this interior trace is simply the number of neighboring cells passed

          fldAssoc_ITrace_Tri->setAssociativity( DOFAssoc_ITrace, itrace_index );

          //Save this interior trace's mapping from local to global mesh
          this->InteriorTraceMapping_[{ITraceGroupTriNeighbors,itrace_index}] =
              xfld_local_unsplit_.getGlobalInteriorTraceMap({ITraceGroupTriNeighbors,itrace_index - cnt_neighbor_tri_splits});

          this->InteriorTraceSplitInfo_[{ITraceGroupTriNeighbors,itrace_index}] = ElementSplitInfo(ElementSplitFlag::Unsplit);


          //Get the indices of the main sub-cell on the left based on split-configuration and trace index
          std::vector<int> main_subelemlist = getMainSubCellElements(itrace);

          fldAssoc_ITrace_Tri->setGroupLeft(0); //Cell group on left is the main cell's group = 0
          fldAssoc_ITrace_Tri->setElementLeft(main_subelemlist[0], itrace_index);
          fldAssoc_ITrace_Tri->setCanonicalTraceLeft(main_canonicalTrace, itrace_index);

          fldAssoc_ITrace_Tri->setGroupRight(CellGroupTriNeighbors_); //Cell group on right is the triangle neighbors' cell group
          fldAssoc_ITrace_Tri->setElementRight(itrace_index, itrace_index); //Right element for interior trace i is equal to i
          fldAssoc_ITrace_Tri->setCanonicalTraceRight(neighbor_canonicalTrace, itrace_index);

          cnt_neighbor_tri++; //Added only one (unsplit) neighbor
        }
      }
      else if ( xfld_local_unsplit_.getCellGroupBase(neighbor_group).topoTypeID() == typeid(Quad) )
      {
        SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD2, T>::splitNeighbors - Quad splitting not supported yet.");
      }
      else
        SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD2, T>::splitNeighbors - Unknown cell topology for neighbor." );

    }
    else if (traceinfo.type == TraceInfo::Boundary) //We are dealing with a boundary trace of the main cell
    {
      const XFieldTraceGroupType& tracegrp = xfld_local_unsplit_.template getBoundaryTraceGroup<Line>(traceinfo.group);

      //canonicalTrace of the main cell for the current trace
      CanonicalTraceToCell main_canonicalTrace = tracegrp.getCanonicalTraceLeft(traceinfo.elem);

      if ((itrace==SplitTraceIndex_) || IsotropicSplitFlag_)
      { //Split main-boundary trace

        int new_nodeDOF = -1;
        if (IsotropicSplitFlag_)
        {
          new_nodeDOF = -1 - itrace; //Many new nodes were added, since it was an isotropic split.
        }
        else
          new_nodeDOF = -1; //Only one new node was added, since it was a trace split.

        // DOF associativity for each sub boundary trace
        ElementAssociativityConstructor<TopoD1, Line> DOFAssoc_BTrace0( trace_basis );
        ElementAssociativityConstructor<TopoD1, Line> DOFAssoc_BTrace1( trace_basis );

        const int nNodeDOFTrace = Line::NNode;
        std::vector<int> trace_nodes(nNodeDOFTrace);
        tracegrp.associativity(traceinfo.elem).getNodeGlobalMapping(trace_nodes);  //Get nodeDOFs of original Btrace (from unsplit mesh)

        DOFAssoc_BTrace0.setRank( tracegrp.associativity(traceinfo.elem).rank() );
        DOFAssoc_BTrace1.setRank( tracegrp.associativity(traceinfo.elem).rank() );

        //node DOF ordering for sub-traces
        int BTrace0_nodeDOF_map[nNodeDOFTrace], BTrace1_nodeDOF_map[nNodeDOFTrace];

        //Copying nodeDOF mapping from original trace to the two sub-traces (with the new DOF)
        BTrace0_nodeDOF_map[0] = trace_nodes[0];
        BTrace0_nodeDOF_map[1] = new_nodeDOF; //The end-point of sub-trace0 is the new node

        BTrace1_nodeDOF_map[0] = new_nodeDOF; //The starting-point of sub-trace1 is the new node
        BTrace1_nodeDOF_map[1] = trace_nodes[1];

        //Map trace nodeDOFs from unsplit mesh to split mesh
        for (int k = 0; k < nNodeDOFTrace; k++)
        {
          BTrace0_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(BTrace0_nodeDOF_map[k]);
          BTrace1_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(BTrace1_nodeDOF_map[k]);
        }

        DOFAssoc_BTrace0.setNodeGlobalMapping( BTrace0_nodeDOF_map, nNodeDOFTrace );
        DOFAssoc_BTrace1.setNodeGlobalMapping( BTrace1_nodeDOF_map, nNodeDOFTrace );


        // create field associativity constructor for each boundary trace of main cell
        // Note: each boundary trace of the main cell has its own boundaryTraceGroup!
        typename XFieldTraceGroupType::FieldAssociativityConstructorType fldAssoc_Main_BTrace( trace_basis, 2);

        fldAssoc_Main_BTrace.setAssociativity( DOFAssoc_BTrace0, 0);
        fldAssoc_Main_BTrace.setAssociativity( DOFAssoc_BTrace1, 1);

        //Save the sub-boundary traces' mappings from local to global mesh
        this->BoundaryTraceMapping_[{cnt_main_btracegroups,0}] = xfld_local_unsplit_.getGlobalBoundaryTraceMap({cnt_main_btracegroups,0});
        this->BoundaryTraceMapping_[{cnt_main_btracegroups,1}] = xfld_local_unsplit_.getGlobalBoundaryTraceMap({cnt_main_btracegroups,0});

        //The original boundary trace is split isotropically into 2 sub-traces
        this->BoundaryTraceSplitInfo_[{cnt_main_btracegroups,0}] =
            ElementSplitInfo(ElementSplitFlag::Split, ElementSplitType::Isotropic, -1, 0);

        this->BoundaryTraceSplitInfo_[{cnt_main_btracegroups,1}] =
            ElementSplitInfo(ElementSplitFlag::Split, ElementSplitType::Isotropic, -1, 1);

        //Get the indices of the main sub-cell on the left based on split-configuration and trace index
        std::vector<int> main_subelemlist = getMainSubCellElements(itrace);

        fldAssoc_Main_BTrace.setGroupLeft(0); //Cell group on left is the main cell's group = 0
        fldAssoc_Main_BTrace.setElementLeft(main_subelemlist[0], 0);
        fldAssoc_Main_BTrace.setElementLeft(main_subelemlist[1], 1);
        fldAssoc_Main_BTrace.setCanonicalTraceLeft(main_canonicalTrace, 0);
        fldAssoc_Main_BTrace.setCanonicalTraceLeft(main_canonicalTrace, 1);

        //Add trace group and copy DOFs for boundary traces of main cell
        this->boundaryTraceGroups_[cnt_main_btracegroups] = new XFieldTraceGroupType( fldAssoc_Main_BTrace );
        this->boundaryTraceGroups_[cnt_main_btracegroups]->setDOF( this->DOF_, this->nDOF_ );

      }
      else
      { //Unsplit main-boundary trace

        // DOF associativity for boundary trace
        ElementAssociativityConstructor<TopoD1, Line> DOFAssoc_BTrace( trace_basis );

        setBoundaryTraceAssociativity(traceinfo.group, traceinfo.elem, DOFAssoc_BTrace);

        // create field associativity constructor for each boundary trace of main cell
        // Note: each boundary trace of the main cell has its own boundaryTraceGroup!
        typename XFieldTraceGroupType::FieldAssociativityConstructorType fldAssoc_Main_BTrace( trace_basis, 1);

        fldAssoc_Main_BTrace.setAssociativity( DOFAssoc_BTrace, 0);

        //Save this boundary trace's mapping from local to global mesh
        this->BoundaryTraceMapping_[{cnt_main_btracegroups,0}] = xfld_local_unsplit_.getGlobalBoundaryTraceMap({cnt_main_btracegroups,0});

        this->BoundaryTraceSplitInfo_[{cnt_main_btracegroups,0}] = ElementSplitInfo(ElementSplitFlag::Unsplit);

        //Get the indices of the main sub-cell on left based on split-configuration and trace index
        std::vector<int> main_subelemlist = getMainSubCellElements(itrace);

        fldAssoc_Main_BTrace.setGroupLeft(0); //Cell group on left is the main cell's group = 0
        fldAssoc_Main_BTrace.setElementLeft(main_subelemlist[0], 0);
        fldAssoc_Main_BTrace.setCanonicalTraceLeft(main_canonicalTrace, 0);

        //Add trace group and copy DOFs for boundary traces of main cell
        this->boundaryTraceGroups_[cnt_main_btracegroups] = new XFieldTraceGroupType( fldAssoc_Main_BTrace );
        this->boundaryTraceGroups_[cnt_main_btracegroups]->setDOF( this->DOF_, this->nDOF_ );
      }

      cnt_main_btracegroups++;

    }//main-boundary-traces

  } //repeated loop across traces of main element

  if (nNeighbors_>0)
  {
    if (nTriNeighbors_>0) //Triangle neighbors
    {
      //Add cell group and copy DOFs for neighboring cells
      this->cellGroups_[CellGroupTriNeighbors_] = new XFieldCellGroupType_Triangle( *fldAssocCell_Neighbors_Tri );
      this->cellGroups_[CellGroupTriNeighbors_]->setDOF( this->DOF_, this->nDOF_ );
      this->nElem_ += fldAssocCell_Neighbors_Tri->nElem();

      //Add trace group and set DOFs for interior traces
      this->interiorTraceGroups_[ITraceGroupTriNeighbors] = new XFieldTraceGroupType( *fldAssoc_ITrace_Tri );
      this->interiorTraceGroups_[ITraceGroupTriNeighbors]->setDOF( this->DOF_, this->nDOF_ );
    }

    if (nQuadNeighbors_>0) //Quad neighbors
    {
      //Add cell group and copy DOFs for neighboring cells
      this->cellGroups_[CellGroupQuadNeighbors_] = new XFieldCellGroupType_Quad( *fldAssocCell_Neighbors_Quad );
      this->cellGroups_[CellGroupQuadNeighbors_]->setDOF( this->DOF_, this->nDOF_ );
      this->nElem_ += fldAssocCell_Neighbors_Quad->nElem();

      //Add trace group and set DOFs for interior traces
      this->interiorTraceGroups_[ITraceGroupQuadNeighbors] = new XFieldTraceGroupType( *fldAssoc_ITrace_Quad );
      this->interiorTraceGroups_[ITraceGroupQuadNeighbors]->setDOF( this->DOF_, this->nDOF_ );
    }

    if (nSplitTriNeighbors_ + nSplitQuadNeighbors_>0)
    {
      if (nSplitTriNeighbors_>0)
      {
        this->interiorTraceGroups_[ITraceGroupInterTriNeighbor] = new XFieldTraceGroupType( *fldAssoc_ITrace_InterTriNeighbor );
        this->interiorTraceGroups_[ITraceGroupInterTriNeighbor]->setDOF( this->DOF_, this->nDOF_ );
      }
      if (nSplitQuadNeighbors_>0)
      {
        this->interiorTraceGroups_[ITraceGroupInterQuadNeighbor] = new XFieldTraceGroupType( *fldAssoc_ITrace_InterQuadNeighbor );
        this->interiorTraceGroups_[ITraceGroupInterQuadNeighbor]->setDOF( this->DOF_, this->nDOF_ );
      }
    }

    if (nOuterBTraceGroups_>0)
    {
      //Add trace group and copy DOFs for interior traces
      this->ghostBoundaryTraceGroups_[OuterBTraceGroup_] = new XFieldTraceGroupType( *fldAssoc_Outer_BTrace );
      this->ghostBoundaryTraceGroups_[OuterBTraceGroup_]->setDOF( this->DOF_, this->nDOF_ );
    }
  } //if neighbors

}

template <class PhysDim>
template <class TopoCell>
void
XField_Local_Split_Linear<PhysDim, TopoD2>::
setMainCellAssociativity_IsotropicSplit(
    const std::vector<int>& new_nodeDOFs,
    std::vector< ElementAssociativityConstructor<TopoD2, TopoCell> >& DOFAssocs_SubCell,
    std::vector< ElementAssociativityConstructor<TopoD1, Line> >& DOFAssocs_MidITrace,
    std::vector< CanonicalTraceToCell >& TracesMid_canonicalL,
    std::vector< CanonicalTraceToCell >& TracesMid_canonicalR )
{

  typedef typename XField<PhysDim, TopoD2>::template FieldCellGroupType<TopoCell> XFieldCellGroupType;
//  typedef typename XField<PhysDim, TopoD2>::template FieldTraceGroupType<TopoTrace> XFieldTraceGroupType;

  //Main-cell info
  int cell_group = 0;
  int cell_elem = 0;

  //Checks
  SANS_ASSERT_MSG((int)new_nodeDOFs.size() == TopoCell::NTrace,
                  "The number of new nodeDOFs (=%d) is not equal to the number of traces to be split of the main cell (=%d).",
                  (int)new_nodeDOFs.size(), TopoCell::NTrace);

  //Get cell/trace group from unsplit mesh
  const XFieldCellGroupType& cellgrp = xfld_local_unsplit_.template getCellGroup<TopoCell>(cell_group);

  const int nNodeDOFCell = TopoCell::NNode;
  const int nNodeDOFTrace = Line::NNode;

  int rank = cellgrp.associativity(cell_elem).rank();

  int UnsplitCell_nodeDOF_map[nNodeDOFCell];
  cellgrp.associativity(cell_elem).getNodeGlobalMapping( UnsplitCell_nodeDOF_map, nNodeDOFCell ); //Get nodeDOF map from unsplit mesh

  if ( xfld_local_unsplit_.getCellGroupBase(cell_group).topoTypeID() == typeid(Triangle) )
  {
    //Node DOF ordering for sub-cells
    int Cell0_nodeDOF_map[nNodeDOFCell], Cell1_nodeDOF_map[nNodeDOFCell], Cell2_nodeDOF_map[nNodeDOFCell], Cell3_nodeDOF_map[nNodeDOFCell];

    //Sub-cell 0
    Cell0_nodeDOF_map[0] = new_nodeDOFs[2];
    Cell0_nodeDOF_map[1] = UnsplitCell_nodeDOF_map[1];
    Cell0_nodeDOF_map[2] = new_nodeDOFs[0];

    //Sub-cell 1
    Cell1_nodeDOF_map[0] = new_nodeDOFs[1];
    Cell1_nodeDOF_map[1] = new_nodeDOFs[0];
    Cell1_nodeDOF_map[2] = UnsplitCell_nodeDOF_map[2];

    //Sub-cell 2
    Cell2_nodeDOF_map[0] = UnsplitCell_nodeDOF_map[0];
    Cell2_nodeDOF_map[1] = new_nodeDOFs[2];
    Cell2_nodeDOF_map[2] = new_nodeDOFs[1];

    //Sub-cell 3 (inner sub-cell)
    Cell3_nodeDOF_map[0] = new_nodeDOFs[0];
    Cell3_nodeDOF_map[1] = new_nodeDOFs[1];
    Cell3_nodeDOF_map[2] = new_nodeDOFs[2];

    //Map subcell nodeDOFs from unsplit mesh to split mesh
    for (int k = 0; k < nNodeDOFCell; k++)
    {
      Cell0_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Cell0_nodeDOF_map[k]);
      Cell1_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Cell1_nodeDOF_map[k]);
      Cell2_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Cell2_nodeDOF_map[k]);
      Cell3_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Cell3_nodeDOF_map[k]);
    }

    DOFAssocs_SubCell[0].setRank( rank );
    DOFAssocs_SubCell[1].setRank( rank );
    DOFAssocs_SubCell[2].setRank( rank );
    DOFAssocs_SubCell[3].setRank( rank );

    DOFAssocs_SubCell[0].setNodeGlobalMapping( Cell0_nodeDOF_map, nNodeDOFCell );
    DOFAssocs_SubCell[1].setNodeGlobalMapping( Cell1_nodeDOF_map, nNodeDOFCell );
    DOFAssocs_SubCell[2].setNodeGlobalMapping( Cell2_nodeDOF_map, nNodeDOFCell );
    DOFAssocs_SubCell[3].setNodeGlobalMapping( Cell3_nodeDOF_map, nNodeDOFCell );

    for (int elem = 0; elem < 4; elem++)
      DOFAssocs_SubCell[elem].edgeSign() = cellgrp.associativity(cell_elem).edgeSign(); //Copy edge signs from original unsplit cell

    for (int trace = 0; trace < Triangle::NTrace; trace++)
      DOFAssocs_SubCell[3].setEdgeSign( -1, trace ); //The inner sub-cell is to the right of all the outer sub-cells.


    int ITraceMid0_nodeDOF_map[Line::NTrace] = {new_nodeDOFs[0], new_nodeDOFs[2]};
    int ITraceMid1_nodeDOF_map[Line::NTrace] = {new_nodeDOFs[1], new_nodeDOFs[0]};
    int ITraceMid2_nodeDOF_map[Line::NTrace] = {new_nodeDOFs[2], new_nodeDOFs[1]};

    //Map trace nodeDOFs from unsplit mesh to split mesh
    for (int k = 0; k < nNodeDOFTrace; k++)
    {
      ITraceMid0_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(ITraceMid0_nodeDOF_map[k]);
      ITraceMid1_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(ITraceMid1_nodeDOF_map[k]);
      ITraceMid2_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(ITraceMid2_nodeDOF_map[k]);
    }

    DOFAssocs_MidITrace[0].setRank( rank );
    DOFAssocs_MidITrace[1].setRank( rank );
    DOFAssocs_MidITrace[2].setRank( rank );

    DOFAssocs_MidITrace[0].setNodeGlobalMapping( ITraceMid0_nodeDOF_map, nNodeDOFTrace );
    DOFAssocs_MidITrace[1].setNodeGlobalMapping( ITraceMid1_nodeDOF_map, nNodeDOFTrace );
    DOFAssocs_MidITrace[2].setNodeGlobalMapping( ITraceMid2_nodeDOF_map, nNodeDOFTrace );

    TracesMid_canonicalL[0].set(1,1);
    TracesMid_canonicalL[1].set(2,1);
    TracesMid_canonicalL[2].set(0,1);

    TracesMid_canonicalR[0].set(1,-1);
    TracesMid_canonicalR[1].set(2,-1);
    TracesMid_canonicalR[2].set(0,-1);

  }
  else if ( xfld_local_unsplit_.getCellGroupBase(cell_group).topoTypeID() == typeid(Quad) )
  {
    SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD2, T>::setMainCellAssociativity_IsotropicSplit - "
                              "Quad splitting not supported yet." );
  }
  else
    SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD2, T>::setMainCellAssociativity_IsotropicSplit - "
                              "Unknown cell topology for main-cell.");

}

template <class PhysDim>
template <class TopoCell>
void
XField_Local_Split_Linear<PhysDim, TopoD2>::
setMainCellAssociativity_TraceSplit(
    const TraceInfo& traceinfo, const int new_nodeDOF,
    ElementAssociativityConstructor<TopoD2, TopoCell>& DOFAssocCell0,
    ElementAssociativityConstructor<TopoD2, TopoCell>& DOFAssocCell1,
    ElementAssociativityConstructor<TopoD1, Line>& DOFAssocTraceMid,
    CanonicalTraceToCell& TraceMid_canonicalL,
    CanonicalTraceToCell& TraceMid_canonicalR )
{

  typedef typename XField<PhysDim, TopoD2>::template FieldCellGroupType<TopoCell> XFieldCellGroupType;
  typedef typename XField<PhysDim, TopoD2>::template FieldTraceGroupType<Line> XFieldTraceGroupType;

  //Main-cell info
  int cell_group = 0;
  int cell_elem = 0;

  //Get cell/trace group from unsplit mesh
  const XFieldCellGroupType& cellgrp = xfld_local_unsplit_.template getCellGroup<TopoCell>(cell_group);

  //Get the relevant InteriorTraceGroup or BoundaryTraceGroup
  const XFieldTraceGroupType& tracegrp = (traceinfo.type == TraceInfo::Interior)
      ? xfld_local_unsplit_.template getInteriorTraceGroup<Line>(traceinfo.group)
      : xfld_local_unsplit_.template getBoundaryTraceGroup<Line>(traceinfo.group);

  const int nNodeDOFCell = TopoCell::NNode;
  const int nNodeDOFTrace = Line::NNode;

  std::vector<int> trace_nodes(nNodeDOFTrace);
  tracegrp.associativity(traceinfo.elem).getNodeGlobalMapping(trace_nodes);  //Get nodeDOFs of original trace (from unsplit mesh)

  //node DOF ordering for sub-cells
  int Cell0_nodeDOF_map[nNodeDOFCell], Cell1_nodeDOF_map[nNodeDOFCell];
  int ITraceMid_nodeDOF_map[nNodeDOFTrace]; //nodeDOF map for the trace between the sub-cells (main-main or neighbor-neighbor)

  int rank = cellgrp.associativity(cell_elem).rank();

  //Copying DOF mappings from original cell to both sub-cells
  cellgrp.associativity(cell_elem).getNodeGlobalMapping( Cell0_nodeDOF_map, nNodeDOFCell ); //Get nodeDOF map from unsplit mesh
  cellgrp.associativity(cell_elem).getNodeGlobalMapping( Cell1_nodeDOF_map, nNodeDOFCell ); //Get nodeDOF map from unsplit mesh

  if ( xfld_local_unsplit_.getCellGroupBase(cell_group).topoTypeID() == typeid(Triangle) )
  {
    int main_node = -1; // index of nodeDOF on the main-cell which connects to the new node
    for (int k=0; k<nNodeDOFCell; k++)
    {
      if (Cell0_nodeDOF_map[k]!=trace_nodes[0] && Cell0_nodeDOF_map[k]!=trace_nodes[1])
      {
        main_node = Cell0_nodeDOF_map[k];
        break;
      }
    }

    ITraceMid_nodeDOF_map[0] = new_nodeDOF;
    ITraceMid_nodeDOF_map[1] = main_node;
  }
  else if ( xfld_local_unsplit_.getCellGroupBase(cell_group).topoTypeID() == typeid(Quad) )
  {
    SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD2, T>::setMainCellAssociativity_TraceSplit - "
                              "Quad splitting not supported yet." );
  }
  else
    SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD2, T>::setMainCellAssociativity_TraceSplit - "
                              "Unknown cell topology for main-cell.");

  //Updating nodeDOF mappings to include the new DOF
  updateNodeDOFMaps_TraceSplit<TopoCell>(trace_nodes, new_nodeDOF, Cell0_nodeDOF_map, Cell1_nodeDOF_map, nNodeDOFCell);


  TraceMid_canonicalL = TraceToCellRefCoord<Line, TopoD2, TopoCell>::
                        getCanonicalTrace(ITraceMid_nodeDOF_map, Line::NNode, Cell0_nodeDOF_map, TopoCell::NNode);

  TraceMid_canonicalR = TraceToCellRefCoord<Line, TopoD2, TopoCell>::
                        getCanonicalTrace(ITraceMid_nodeDOF_map, Line::NNode, Cell1_nodeDOF_map, TopoCell::NNode);

  //Map subcell nodeDOFs from unsplit mesh to split mesh
  for (int k = 0; k < nNodeDOFCell; k++)
  {
    Cell0_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Cell0_nodeDOF_map[k]);
    Cell1_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Cell1_nodeDOF_map[k]);
  }

  DOFAssocCell0.setRank( rank );
  DOFAssocCell0.setNodeGlobalMapping( Cell0_nodeDOF_map, nNodeDOFCell );
  DOFAssocCell0.edgeSign() = cellgrp.associativity(cell_elem).edgeSign(); //Copy edge signs from original unsplit cell
  DOFAssocCell0.setEdgeSign(+1,TraceMid_canonicalL.trace); //Sub-cell 0 is to the left of the new split-interface

  DOFAssocCell1.setRank( rank );
  DOFAssocCell1.setNodeGlobalMapping( Cell1_nodeDOF_map, nNodeDOFCell );
  DOFAssocCell1.edgeSign() = cellgrp.associativity(cell_elem).edgeSign(); //Copy edge signs from original unsplit cell
  DOFAssocCell1.setEdgeSign(-1,TraceMid_canonicalR.trace); //Sub-cell 1 is to the right of the new split-interface

  //Map trace nodeDOFs from unsplit mesh to split mesh
  for (int k = 0; k < nNodeDOFTrace; k++)
    ITraceMid_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(ITraceMid_nodeDOF_map[k]);

  DOFAssocTraceMid.setRank( rank );
  DOFAssocTraceMid.setNodeGlobalMapping( ITraceMid_nodeDOF_map, nNodeDOFTrace );
}

template <class PhysDim>
template <class TopoCell>
void
XField_Local_Split_Linear<PhysDim, TopoD2>::setCellAssociativity_TraceSplit(const int cell_group, const int cell_elem,
                                                                        const int trace_group, const int trace_elem, const int new_nodeDOF,
                                                                        ElementAssociativityConstructor<TopoD2, TopoCell>& DOFAssocCell0,
                                                                        ElementAssociativityConstructor<TopoD2, TopoCell>& DOFAssocCell1,
                                                                        ElementAssociativityConstructor<TopoD1, Line>& DOFAssocTrace0,
                                                                        ElementAssociativityConstructor<TopoD1, Line>& DOFAssocTrace1,
                                                                        ElementAssociativityConstructor<TopoD1, Line>& DOFAssocTraceMid,
                                                                        CanonicalTraceToCell& TraceMid_canonicalL,
                                                                        CanonicalTraceToCell& TraceMid_canonicalR)
{
  typedef typename XField<PhysDim, TopoD2>::template FieldCellGroupType<TopoCell> XFieldCellGroupType;
  typedef typename XField<PhysDim, TopoD2>::template FieldTraceGroupType<Line> XFieldTraceGroupType;

  //Get cell/trace group from unsplit mesh
  const XFieldCellGroupType& cellgrp = xfld_local_unsplit_.template getCellGroup<TopoCell>(cell_group);
  const XFieldTraceGroupType& tracegrp = xfld_local_unsplit_.template getInteriorTraceGroup<Line>(trace_group);

  const int nNodeDOFCell = TopoCell::NNode;
  const int nNodeDOFTrace = Line::NNode;

  std::vector<int> trace_nodes(nNodeDOFTrace);
  tracegrp.associativity(trace_elem).getNodeGlobalMapping(trace_nodes);  //Get nodeDOFs of original trace (from unsplit mesh)

  //node DOF ordering for sub-cells
  int Cell0_nodeDOF_map[nNodeDOFCell], Cell1_nodeDOF_map[nNodeDOFCell];
  int ITrace0_nodeDOF_map[nNodeDOFTrace], ITrace1_nodeDOF_map[nNodeDOFTrace];
  int ITraceMid_nodeDOF_map[nNodeDOFTrace]; //nodeDOF map for the trace between the sub-cells (main-main or neighbor-neighbor)

  //Copying nodeDOF mapping from original trace to the two sub-traces (with the new DOF)
  ITrace0_nodeDOF_map[0] = trace_nodes[0];
  ITrace0_nodeDOF_map[1] = new_nodeDOF; //The end-point of sub-trace0 is the new node

  ITrace1_nodeDOF_map[0] = new_nodeDOF; //The starting-point of sub-trace1 is the new node
  ITrace1_nodeDOF_map[1] = trace_nodes[1];

  int rank = cellgrp.associativity(cell_elem).rank();

  //Copying DOF mappings from original cell to both sub-cells
  cellgrp.associativity(cell_elem).getNodeGlobalMapping( Cell0_nodeDOF_map, nNodeDOFCell ); //Get nodeDOF map from unsplit mesh
  cellgrp.associativity(cell_elem).getNodeGlobalMapping( Cell1_nodeDOF_map, nNodeDOFCell ); //Get nodeDOF map from unsplit mesh

  if ( xfld_local_unsplit_.getCellGroupBase(cell_group).topoTypeID() == typeid(Triangle) )
  {
    int outer_node = -1; // index of nodeDOF which is on the outer boundary on this neighboring cell
    for (int k=0; k<nNodeDOFCell; k++)
    {
      if (Cell0_nodeDOF_map[k]!=trace_nodes[0] && Cell0_nodeDOF_map[k]!=trace_nodes[1])
      {
        outer_node = Cell0_nodeDOF_map[k];
        break;
      }
    }

    ITraceMid_nodeDOF_map[0] = outer_node;
    ITraceMid_nodeDOF_map[1] = new_nodeDOF;
  }
  else if ( xfld_local_unsplit_.getCellGroupBase(cell_group).topoTypeID() == typeid(Quad) )
  {
    SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD2, T>::setCellAssociativity_TraceSplit - "
                              "Quad splitting not supported yet." );
  }
  else
    SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD2, T>::setCellAssociativity_TraceSplit - "
                              "Unknown cell topology for main-cell.");

  //Updating nodeDOF mappings to include the new DOF
  updateNodeDOFMaps_TraceSplit<TopoCell>(trace_nodes, new_nodeDOF, Cell0_nodeDOF_map, Cell1_nodeDOF_map, nNodeDOFCell);


  TraceMid_canonicalL = TraceToCellRefCoord<Line, TopoD2, TopoCell>::
                        getCanonicalTrace(ITraceMid_nodeDOF_map, Line::NNode, Cell0_nodeDOF_map, TopoCell::NNode);

  TraceMid_canonicalR = TraceToCellRefCoord<Line, TopoD2, TopoCell>::
                        getCanonicalTrace(ITraceMid_nodeDOF_map, Line::NNode, Cell1_nodeDOF_map, TopoCell::NNode);

  //Map subcell nodeDOFs from unsplit mesh to split mesh
  for (int k = 0; k < nNodeDOFCell; k++)
  {
    Cell0_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Cell0_nodeDOF_map[k]);
    Cell1_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Cell1_nodeDOF_map[k]);
  }

  DOFAssocCell0.setRank( rank );
  DOFAssocCell0.setNodeGlobalMapping( Cell0_nodeDOF_map, nNodeDOFCell );
  DOFAssocCell0.edgeSign() = cellgrp.associativity(cell_elem).edgeSign(); //Copy edge signs from original unsplit cell
  DOFAssocCell0.setEdgeSign(+1,TraceMid_canonicalL.trace); //Sub-cell 0 is to the left of the new split-interface

  DOFAssocCell1.setRank( rank );
  DOFAssocCell1.setNodeGlobalMapping( Cell1_nodeDOF_map, nNodeDOFCell );
  DOFAssocCell1.edgeSign() = cellgrp.associativity(cell_elem).edgeSign(); //Copy edge signs from original unsplit cell
  DOFAssocCell1.setEdgeSign(-1,TraceMid_canonicalR.trace); //Sub-cell 1 is to the right of the new split-interface

  //Map trace nodeDOFs from unsplit mesh to split mesh
  for (int k = 0; k < nNodeDOFTrace; k++)
  {
    ITrace0_nodeDOF_map[k]   = offset_nodeDOF_ + NodeDOFMap_.at(ITrace0_nodeDOF_map[k]);
    ITrace1_nodeDOF_map[k]   = offset_nodeDOF_ + NodeDOFMap_.at(ITrace1_nodeDOF_map[k]);
    ITraceMid_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(ITraceMid_nodeDOF_map[k]);
  }

  DOFAssocTrace0.setRank( rank );
  DOFAssocTrace1.setRank( rank );
  DOFAssocTraceMid.setRank( rank );

  DOFAssocTrace0.setNodeGlobalMapping  ( ITrace0_nodeDOF_map  , nNodeDOFTrace );
  DOFAssocTrace1.setNodeGlobalMapping  ( ITrace1_nodeDOF_map  , nNodeDOFTrace );
  DOFAssocTraceMid.setNodeGlobalMapping( ITraceMid_nodeDOF_map, nNodeDOFTrace );
}

template <class PhysDim>
template <class TopoCell>
void
XField_Local_Split_Linear<PhysDim, TopoD2>::setCellAssociativity(int group, int elem, ElementAssociativityConstructor<TopoD2, TopoCell>& DOFAssoc)
{
  typedef typename XField<PhysDim, TopoD2>::template FieldCellGroupType<TopoCell> XFieldCellGroupType;

  //Get global cell group
  const XFieldCellGroupType& cellgrp_main = xfld_local_unsplit_.template getCellGroup<TopoCell>(group);

  const int nNodeDOF = TopoCell::NNode;

  // node DOF, edge DOF and cell DOF ordering for cell
  int Cell_nodeDOF_map[nNodeDOF];

  cellgrp_main.associativity(elem).getNodeGlobalMapping( Cell_nodeDOF_map, nNodeDOF ); //Get nodeDOF map from global field

  //Map DOFs from global to local mesh
  for (int k = 0; k < nNodeDOF; k++)
    Cell_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Cell_nodeDOF_map[k]);

  DOFAssoc.setRank( cellgrp_main.associativity(elem).rank() );

  DOFAssoc.setNodeGlobalMapping( Cell_nodeDOF_map, nNodeDOF );

  DOFAssoc.edgeSign() = cellgrp_main.associativity(elem).edgeSign();

}

template<class PhysDim>
void
XField_Local_Split_Linear< PhysDim, TopoD2>::
setInteriorTraceAssociativity(int group, int elem, ElementAssociativityConstructor<TopoD1, Line>& DOFAssoc)
{
  typedef typename XField<PhysDim, TopoD2>::template FieldTraceGroupType<Line> XFieldTraceGroupType;

  //Get global interior trace group
  const XFieldTraceGroupType& tracegrp = xfld_local_unsplit_.template getInteriorTraceGroup<Line>(group);

  const int nNodeDOF = Line::NNode;

  // node DOF and edge DOF ordering for interior trace
  std::vector<int> Trace_nodeDOF_map(nNodeDOF);

  tracegrp.associativity(elem).getNodeGlobalMapping( Trace_nodeDOF_map ); //Get nodeDOF map from global field

  //Map DOFs from global to local mesh
  for (int k = 0; k < nNodeDOF; k++)
    Trace_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Trace_nodeDOF_map[k]);

  DOFAssoc.setRank( tracegrp.associativity(elem).rank() );

  DOFAssoc.setNodeGlobalMapping( Trace_nodeDOF_map );

}

template<class PhysDim>
void
XField_Local_Split_Linear< PhysDim, TopoD2>::
setBoundaryTraceAssociativity(int group, int elem, ElementAssociativityConstructor<TopoD1, Line>& DOFAssoc)
{
  typedef typename XField<PhysDim, TopoD2>::template FieldTraceGroupType<Line> XFieldTraceGroupType;

  //Get global boundary trace group
  const XFieldTraceGroupType& tracegrp = xfld_local_unsplit_.template getBoundaryTraceGroup<Line>(group);

  const int nNodeDOF = Line::NNode;

  // node DOF and edge DOF ordering for boundary trace
  int Cell_nodeDOF_map[nNodeDOF];

  tracegrp.associativity(elem).getNodeGlobalMapping( Cell_nodeDOF_map, nNodeDOF ); //Get nodeDOF map from global field

  //Map DOFs from global to local mesh
  for (int k = 0; k < nNodeDOF; k++)
    Cell_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Cell_nodeDOF_map[k]);

  DOFAssoc.setRank( tracegrp.associativity(elem).rank() );

  DOFAssoc.setNodeGlobalMapping( Cell_nodeDOF_map, nNodeDOF );
}

template<class PhysDim>
void
XField_Local_Split_Linear< PhysDim, TopoD2>::
setGhostBoundaryTraceAssociativity(int group, int elem, ElementAssociativityConstructor<TopoD1, Line>& DOFAssoc)
{
  typedef typename XField<PhysDim, TopoD2>::template FieldTraceGroupType<Line> XFieldTraceGroupType;

  //Get global boundary trace group
  const XFieldTraceGroupType& tracegrp = xfld_local_unsplit_.template getGhostBoundaryTraceGroup<Line>(group);

  const int nNodeDOF = Line::NNode;

  // node DOF and edge DOF ordering for boundary trace
  int Cell_nodeDOF_map[nNodeDOF];

  tracegrp.associativity(elem).getNodeGlobalMapping( Cell_nodeDOF_map, nNodeDOF ); //Get nodeDOF map from global field

  //Map DOFs from global to local mesh
  for (int k = 0; k < nNodeDOF; k++)
    Cell_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Cell_nodeDOF_map[k]);

  DOFAssoc.setRank( tracegrp.associativity(elem).rank() );

  DOFAssoc.setNodeGlobalMapping( Cell_nodeDOF_map, nNodeDOF );
}

template<class PhysDim>
void
XField_Local_Split_Linear< PhysDim, TopoD2>::process_OuterBTraces(const int neighbor_group, const int neighbor_elem,
                                                     const int order, const int Ntrace,
                                                     const int local_neighbor_group, const int local_neighbor_elem,
                                                     const CanonicalTraceToCell& neighbor_canonicalTrace,
                                                     std::shared_ptr<typename BaseType::template FieldTraceGroupType<Line>
                                                                                      ::FieldAssociativityConstructorType> fldAssoc_Outer_BTrace,
                                                     const int OuterBTraceGroup, int& cnt_outer_Btraces)
{

  typedef typename XField<PhysDim, TopoD2>::template FieldTraceGroupType<Line> XFieldTraceGroupType;

  const BasisFunctionLineBase* trace_basis = BasisFunctionLineBase::getBasisFunction(order, basisCategory_);

  // DOF associativity for boundary traces of neighbors
  ElementAssociativityConstructor<TopoD1, Line> DOFAssoc_Outer_BTrace( trace_basis );

  for (int j = 0; j < Ntrace; j++) //loop across traces of this neighboring element
  {
    if (j != neighbor_canonicalTrace.trace) //Outer boundary trace
    {
      //Get index of outer trace (and its cellgroup)
      const TraceInfo& outertraceinfo = connectivity_.getTrace(neighbor_group, neighbor_elem, j);

      //canonicalTrace of this neighboring cell for the outer (interior) trace
      CanonicalTraceToCell neighbor_outer_canonicalTrace;

      if (outertraceinfo.type == TraceInfo::GhostBoundary) //We are dealing with an Outer BoundaryTrace of this neighboring cell
      {
        //Set DOF associativity
        setGhostBoundaryTraceAssociativity(outertraceinfo.group, outertraceinfo.elem, DOFAssoc_Outer_BTrace);

        const XFieldTraceGroupType& outer_tracegrp = xfld_local_unsplit_.template getGhostBoundaryTraceGroup<Line>(outertraceinfo.group);

        //canonicalTrace of this neighboring cell for the outer boundary trace
        neighbor_outer_canonicalTrace = outer_tracegrp.getCanonicalTraceLeft(outertraceinfo.elem);
      }
      else
        SANS_DEVELOPER_EXCEPTION("XField_Split_Local<PhysDim, TopoD2, T>::processOuterBTraces - "
        "outer_trace_group (= %d) is not of type GhostBoundary!",outertraceinfo.group);

      //Save this boundary trace's mapping from local to global mesh
//      this->BoundaryTraceMapping_[{OuterBTraceGroup,cnt_outer_Btraces}] =
//            xfld_local_unsplit_.getGlobalBoundaryTraceMap({OuterBTraceGroup,cnt_outer_Btraces});
//
//      this->BoundaryTraceSplitInfo_[{OuterBTraceGroup,cnt_outer_Btraces}] = ElementSplitInfo(ElementSplitFlag::Unsplit);

      //Fill associativity of Outer BoundaryTraceGroup
      fldAssoc_Outer_BTrace->setAssociativity( DOFAssoc_Outer_BTrace, cnt_outer_Btraces);

      fldAssoc_Outer_BTrace->setGroupLeft(local_neighbor_group); //Cell group on left is the triangle neighbor cells' group
      fldAssoc_Outer_BTrace->setElementLeft(local_neighbor_elem, cnt_outer_Btraces); //Cell on left is the neighbor cell
      fldAssoc_Outer_BTrace->setCanonicalTraceLeft(neighbor_outer_canonicalTrace, cnt_outer_Btraces);

      cnt_outer_Btraces++;
    }
  }

}

template<class PhysDim>
template<class TopoCell>
void
XField_Local_Split_Linear< PhysDim, TopoD2>::process_SplitOuterBTraces(const int neighbor_group, const int neighbor_elem,
                                                     const int order, const int Ntrace,
                                                     const int local_neighbor_group, const int local_neighbor_elem,
                                                     const CanonicalTraceToCell& neighbor_canonicalTrace,
                                                     ElementAssociativityConstructor<TopoD2, TopoCell>& DOFAssocCell0,
                                                     ElementAssociativityConstructor<TopoD2, TopoCell>& DOFAssocCell1,
                                                     std::shared_ptr<typename BaseType::template FieldTraceGroupType<Line>
                                                                                      ::FieldAssociativityConstructorType> fldAssoc_Outer_BTrace,
                                                     const int OuterBTraceGroup, int& cnt_outer_Btraces)
{

  typedef typename XField<PhysDim, TopoD2>::template FieldTraceGroupType<Line> XFieldTraceGroupType;

  const BasisFunctionLineBase* trace_basis = BasisFunctionLineBase::getBasisFunction(order, basisCategory_);

  // DOF associativity for boundary traces of neighbors
  ElementAssociativityConstructor<TopoD1, Line> DOFAssoc_Outer_BTrace( trace_basis );

  for (int j = 0; j < Ntrace; j++) //loop across traces of this neighboring element
  {
    if (j != neighbor_canonicalTrace.trace) //Outer boundary trace
    {
      //Get index of outer trace (and its cellgroup)
      const TraceInfo& outertraceinfo = connectivity_.getTrace(neighbor_group, neighbor_elem, j);

      //canonicalTrace of this neighboring cell for the outer (interior) trace
      CanonicalTraceToCell neighbor_outer_canonicalTrace;

      // node DOF and edge DOF ordering for boundary trace
      int Trace_nodeDOF_map[Line::NTrace];

      if (outertraceinfo.type == TraceInfo::GhostBoundary) //We are dealing with an Outer BoundaryTrace of this neighboring cell
      {
        //Get global boundary trace group
        const XFieldTraceGroupType& tracegrp = xfld_local_unsplit_.template getGhostBoundaryTraceGroup<Line>(outertraceinfo.group);

        //Get nodeDOF map from unsplit local mesh
        tracegrp.associativity(outertraceinfo.elem).getNodeGlobalMapping( Trace_nodeDOF_map, Line::NTrace );

        //Map DOFs from global to local mesh
        for (int k = 0; k < Line::NTrace; k++)
          Trace_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Trace_nodeDOF_map[k]);

        DOFAssoc_Outer_BTrace.setRank( tracegrp.associativity(outertraceinfo.elem).rank() );
        DOFAssoc_Outer_BTrace.setNodeGlobalMapping( Trace_nodeDOF_map, Line::NTrace );

        const XFieldTraceGroupType& outer_tracegrp = xfld_local_unsplit_.template getGhostBoundaryTraceGroup<Line>(outertraceinfo.group);

        //canonicalTrace of this neighboring cell for the outer boundary trace
        neighbor_outer_canonicalTrace = outer_tracegrp.getCanonicalTraceLeft(outertraceinfo.elem);
      }
      else
        SANS_DEVELOPER_EXCEPTION("XField_Split_Local<PhysDim, TopoD2, T>::process_SplitOuterBTraces - "
        "outer_trace_group (= %d) is not of type GhostBoundary!",outertraceinfo.group);

      const int nNodeDOF_Cell = TopoCell::NNode;
      int SubCell0_nodeDOF_map[nNodeDOF_Cell];
      int SubCell1_nodeDOF_map[nNodeDOF_Cell];

      DOFAssocCell0.getNodeGlobalMapping( SubCell0_nodeDOF_map, nNodeDOF_Cell );
      DOFAssocCell1.getNodeGlobalMapping( SubCell1_nodeDOF_map, nNodeDOF_Cell );

      // Get the nodeDOFs on the boundary of the two sub-cells
      int btrace_nodes_cell0[Line::NTrace];
      int btrace_nodes_cell1[Line::NTrace];
      for ( int n = 0; n < Line::NTrace; n++)
      {
        btrace_nodes_cell0[n] = SubCell0_nodeDOF_map[TraceToCellRefCoord<Line, TopoD2, TopoCell>::
                                                     TraceNodes[neighbor_outer_canonicalTrace.trace][n]];
        btrace_nodes_cell1[n] = SubCell1_nodeDOF_map[TraceToCellRefCoord<Line, TopoD2, TopoCell>::
                                                     TraceNodes[neighbor_outer_canonicalTrace.trace][n]];
      }

      //Compare the DOFs from the cells to those from DOFAssoc_Outer_BTrace, to find out which of the two sub-cells
      //is the "left" element for this boundary trace

      int local_neighbor_subelem = -1;
      if (btrace_nodes_cell0[0]==Trace_nodeDOF_map[0] && btrace_nodes_cell0[1]==Trace_nodeDOF_map[1])
      {
        local_neighbor_subelem = local_neighbor_elem;
      }
      else if (btrace_nodes_cell1[0]==Trace_nodeDOF_map[0] && btrace_nodes_cell1[1]==Trace_nodeDOF_map[1])
      {
        local_neighbor_subelem = local_neighbor_elem + 1;
      }
      else
        SANS_DEVELOPER_EXCEPTION("XField_Split_Local<PhysDim, TopoD2, T>::process_SplitOuterBTraces - "
                                 "Inconsistent nodeDOF maps!");

      //Save this boundary trace's mapping from local to global mesh
//      this->BoundaryTraceMapping_[{OuterBTraceGroup,cnt_outer_Btraces}] =
//          xfld_local_unsplit_.getGlobalBoundaryTraceMap({OuterBTraceGroup,cnt_outer_Btraces});
//
//      this->BoundaryTraceSplitInfo_[{OuterBTraceGroup,cnt_outer_Btraces}] = ElementSplitInfo(ElementSplitFlag::Unsplit);

      //Fill associativity of Outer BoundaryTraceGroup
      fldAssoc_Outer_BTrace->setAssociativity( DOFAssoc_Outer_BTrace, cnt_outer_Btraces);

      fldAssoc_Outer_BTrace->setGroupLeft(local_neighbor_group); //Cell group on left is the neighbor cells' group
      fldAssoc_Outer_BTrace->setElementLeft(local_neighbor_subelem, cnt_outer_Btraces); //Cell on left is the neighbor cell
      fldAssoc_Outer_BTrace->setCanonicalTraceLeft(neighbor_outer_canonicalTrace, cnt_outer_Btraces);

      cnt_outer_Btraces++;
    }
  }

}

template <class PhysDim>
template <class Topology>
void
XField_Local_Split_Linear<PhysDim, TopoD2>::trackDOFs(int group, int elem)
{
  typedef typename XField<PhysDim, TopoD2>::template FieldCellGroupType<Topology> XFieldCellGroupType;

  std::pair<std::map<int,int>::iterator,bool> map_ret; //return value for map inserts

  const XFieldCellGroupType& cellgrp = xfld_local_unsplit_.template getCellGroup<Topology>(group);

  // node DOF map
  const int nNodeDOF = Topology::NNode;
  int nodeMap[nNodeDOF];

  //Save DOF mappings from unsplit mesh to split mesh
  cellgrp.associativity(elem).getNodeGlobalMapping( nodeMap, nNodeDOF );
  for (int k = 0; k < nNodeDOF; k++)
  {
    map_ret = NodeDOFMap_.insert( std::pair<int,int>(nodeMap[k],cntNodeDOF_) );
    if (map_ret.second==true) cntNodeDOF_++; //increment counter if object was actually added
  }

}

template <class PhysDim>
std::vector<int>
XField_Local_Split_Linear<PhysDim, TopoD2>::getMainSubCellElements(const int trace_index)
{

  if (IsotropicSplitFlag_)
  {
    if (trace_index==0)
      return {0,1};
    else if (trace_index==1)
      return {1,2};
    else if (trace_index==2)
      return {2,0};
  }
  else
  {
    if (SplitTraceIndex_==0)
    {
      if (trace_index==0)
        return {0,1};
      else if (trace_index==1)
        return {1};
      else if (trace_index==2)
        return {0};
    }
    else if (SplitTraceIndex_==1)
    {
      if (trace_index==0)
        return {0};
      else if (trace_index==1)
        return {0,1};
      else if (trace_index==2)
        return {1};
    }
    else if (SplitTraceIndex_==2)
    {
      if (trace_index==0)
        return {1};
      else if (trace_index==1)
        return {0};
      else if (trace_index==2)
        return {0,1};
    }
  }

  SANS_DEVELOPER_EXCEPTION("XField_Local_Split_Linear<PhysDim, TopoD2, T>::getMainSubCellElements - "
                           "Invalid trace_index (=%d) for split configuration!", trace_index);
  return {-1};
}

//Explicit instantiations
template class XField_Local_Split_Linear<PhysD2,TopoD2>;
template class XField_Local_Split_Linear<PhysD3,TopoD2>;

}
