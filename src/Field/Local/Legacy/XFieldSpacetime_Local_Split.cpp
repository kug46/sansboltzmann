// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "Local/XField_LocalPatch.h"

#include "XFieldSpacetime.h"
#include "XFieldSpacetime_Local_Split.h"

namespace SANS
{

template <class PhysDim>
XField_Local_Split<PhysDim, TopoD4>::XField_Local_Split(const XField_Local<PhysDim, TopoD4>& xfld_local,
                                                        XField_CellToTrace<PhysDim,TopoD4>& connectivity,
                                                        ElementSplitType split_type, int split_edge_index)
  : BaseType(xfld_local.comm())
{

  if (split_type==ElementSplitType::Edge)
  {
    // this is NOT how the mesh should be split but is simply here to run in compatibility mode
    // for the unit tests when LOCAL_SPLIT_PATCH is not defined
    // extra work is being done to reconstruct the local (unsplit) xfield every time
    // instead of doing it once and then constructing the split xfield from that
    printf("[warning] intended for unit tests only\n");
    XField_LocalPatchConstructor<PhysDim,Pentatope> patch(*xfld_local.comm(),
                                                          xfld_local.global_xfld(),
                                                          xfld_local.mainElem(),xfld_local.mainElemGroup());
    XField_LocalPatch<PhysDim,Pentatope> split_patch(patch,split_type,split_edge_index);
    this->cloneFrom(split_patch);
  }
  else
    SANS_DEVELOPER_EXCEPTION("unsupported split type for pentatopes");
}

//Explicit instantiations
template class XField_Local_Split<PhysD4,TopoD4>;

}
