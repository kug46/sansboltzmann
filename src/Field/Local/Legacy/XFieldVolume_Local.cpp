// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XFieldVolume.h"
#include "XFieldVolume_Local.h"

namespace SANS
{

template <class PhysDim>
template <class TopoCell>
void
XField_Local<PhysDim, TopoD3>::trackDOFs(int group, int elem)
{
  typedef typename XField<PhysDim, TopoD3>::template FieldCellGroupType<TopoCell> XFieldCellGroupType;

  std::pair<std::map<int,int>::iterator,bool> map_ret; //return value for map inserts

  const XFieldCellGroupType& cellgrp = global_xfld_.template getCellGroup<TopoCell>(group);

  int nNodeDOF = cellgrp.associativity(elem).nNode();
  int nEdgeDOF = cellgrp.associativity(elem).nEdge();
  int nFaceDOF = cellgrp.associativity(elem).nFace();
  int nCellDOF = cellgrp.associativity(elem).nCell();

  // node DOF, edge DOF and cell DOF maps
  std::vector<int> nodeMap(nNodeDOF);
  std::vector<int> edgeMap(nEdgeDOF);
  std::vector<int> faceMap(nFaceDOF);
  std::vector<int> cellMap(nCellDOF);

  //Save DOF mappings from global mesh to local mesh
  cellgrp.associativity(elem).getNodeGlobalMapping( nodeMap.data(), nNodeDOF );
  for (int k = 0; k < nNodeDOF; k++)
  {
    map_ret = NodeDOFMap_.insert( std::pair<int,int>(nodeMap[k],cntNodeDOF_) );
    if (map_ret.second==true) cntNodeDOF_++; //increment counter if object was actually added
  }

  cellgrp.associativity(elem).getEdgeGlobalMapping( edgeMap.data(), nEdgeDOF );
  for (int k = 0; k < nEdgeDOF; k++)
  {
    map_ret = EdgeDOFMap_.insert( std::pair<int,int>(edgeMap[k],cntEdgeDOF_) );
    if (map_ret.second==true) cntEdgeDOF_++; //increment counter if object was actually added
  }

  cellgrp.associativity(elem).getFaceGlobalMapping( faceMap.data(), nFaceDOF );
  for (int k = 0; k < nFaceDOF; k++)
  {
    map_ret = FaceDOFMap_.insert( std::pair<int,int>(faceMap[k],cntFaceDOF_) );
    if (map_ret.second==true) cntFaceDOF_++; //increment counter if object was actually added
  }

  cellgrp.associativity(elem).getCellGlobalMapping( cellMap.data(), nCellDOF );
  for (int k = 0; k < nCellDOF; k++)
  {
    map_ret = CellDOFMap_.insert( std::pair<int,int>(cellMap[k],cntCellDOF_) );
    if (map_ret.second==true) cntCellDOF_++; //increment counter if object was actually added
  }
}

template <class PhysDim>
void
XField_Local<PhysDim, TopoD3>::extractLocalGrid(int group, int elem)
{
  //Checks
  SANS_ASSERT_MSG( group>=0 && group < global_xfld_.nCellGroups(), "group = %d, nCellGroups() = %d ", group, global_xfld_.nCellGroups());

  SANS_ASSERT_MSG( elem>=0 && elem < global_xfld_.getCellGroupBase(group).nElem(),
                  "cellElem=%d, getCellGroupBase(group).nElem()=%d ",elem,global_xfld_.getCellGroupBase(group).nElem());

  //Clear current local grid
  this->deallocate();

  NodeDOFMap_.clear();
  EdgeDOFMap_.clear();
  FaceDOFMap_.clear();
  CellDOFMap_.clear();

  cntNodeDOF_ = 0;
  cntEdgeDOF_ = 0;
  cntFaceDOF_ = 0;
  cntCellDOF_ = 0;

  OuterBTraceGroup_Tri_ = -1;
  OuterBTraceGroup_Quad_ = -1;

  if ( global_xfld_.getCellGroupBase(group).topoTypeID() == typeid(Tet) )
  {
    extractMainCell<Tet, Triangle>(group, elem);
  }
  else if ( global_xfld_.getCellGroupBase(group).topoTypeID() == typeid(Hex) )
  {
    extractMainCell<Hex, Quad>(group, elem);
  }
  else
    SANS_DEVELOPER_EXCEPTION( "XField_Local<PhysDim, TopoD3, T>::extractLocalGrid - Unknown cell topology." );
}


template <class PhysDim>
template <class TopoMainCell, class TopoMainTrace>
void
XField_Local<PhysDim, TopoD3>::extractMainCell(int main_group, int main_elem)
{
  typedef typename XField<PhysDim, TopoD3>::template FieldCellGroupType<TopoMainCell> XFieldCellGroupType;
  typedef typename XField<PhysDim, TopoD3>::template FieldTraceGroupType<TopoMainTrace> XFieldTraceGroupType;
  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef typename XFieldCellGroupType::BasisType BasisType;

  const XFieldCellGroupType& cellgrp_main = global_xfld_.template getCellGroup<TopoMainCell>(main_group);

  const BasisType* cell_basis = cellgrp_main.basis();
  int order = cell_basis->order();

  nTetNeighbors_ = 0;
  nHexNeighbors_ = 0;
  nNeighbors_ = 0;

  //Count and map required DOFs from global to local mesh
  trackDOFs<TopoMainCell>(main_group, main_elem);

  //Initial loop across neighboring elements to find number of cells/traces/DOFs
  for (int i = 0; i < TopoMainCell::NTrace; i++)
  {
    //Get indices of each neighboring cell (and its cellgroup)
    const TraceInfo& traceinfo = connectivity_.getTrace(main_group, main_elem, i);

    if (traceinfo.type == TraceInfo::Interior)
    {
      const XFieldTraceGroupType& tracegrp = global_xfld_.template getInteriorTraceGroup<TopoMainTrace>(traceinfo.group);
      int neighbor_group = -1; //cellgroup of neighboring element
      int neighbor_elem = -1; //element index of neighboring element

      if (tracegrp.getElementLeft(traceinfo.elem) != main_elem)
      {
        neighbor_group = tracegrp.getGroupLeft(); //getGroupLeft gives the neighbor's cellgroup
        neighbor_elem = tracegrp.getElementLeft(traceinfo.elem);
      }
      else
      {
        neighbor_group = tracegrp.getGroupRight(); //getGroupRight gives the neighbor's cellgroup
        neighbor_elem = tracegrp.getElementRight(traceinfo.elem);
      }

      if ( global_xfld_.getCellGroupBase(neighbor_group).topoTypeID() == typeid(Tet) )
      {
        //Count and map required DOFs from global to local mesh
        trackDOFs<Tet>(neighbor_group, neighbor_elem);
        nTetNeighbors_++; //Increment tet neighbor count
      }
      else if ( global_xfld_.getCellGroupBase(neighbor_group).topoTypeID() == typeid(Hex) )
      {
        //Count and map required DOFs from global to local mesh
        trackDOFs<Hex>(neighbor_group, neighbor_elem);
        nHexNeighbors_++; //Increment hex neighbor count
      }
      else
        SANS_DEVELOPER_EXCEPTION( "XField_Local<PhysDim, TopoD3, T>::extractLocalGrid - Unknown cell topology for neighbor." );
    }

  } // initial loop across traces of main cell


  //Count DOFs in local mesh
  int nDOF_local = (int)CellDOFMap_.size() + (int)EdgeDOFMap_.size() + (int)NodeDOFMap_.size();

#if 0
  std::cout << "nDOF_local: " << nDOF_local << std::endl;
  std::cout << "mapNode: " << NodeDOFMap_.size() <<std::endl;
  std::cout << "mapEdge: " << EdgeDOFMap_.size() <<std::endl;
  std::cout << "mapFace: " << FaceDOFMap_.size() <<std::endl;
  std::cout << "mapCell: " << CellDOFMap_.size() <<std::endl;
#endif

  //Create local XField
  this->resizeDOF(nDOF_local);

  //Add up neighbor cell counts
  nNeighbors_ = nTetNeighbors_ + nHexNeighbors_;

  if (nNeighbors_>0)
  {
    if (nTetNeighbors_>0 && nHexNeighbors_==0) //Only tet neighbors
    {
      this->resizeCellGroups(2); //2 CellGroups - one for main cell, and one for all neighboring tets
      this->resizeInteriorTraceGroups(1); //one group for all interior traces
    }
    else if (nTetNeighbors_==0 && nHexNeighbors_>0) //Only hex neighbors
    {
      this->resizeCellGroups(2); //2 CellGroups - one for main cell, and one for all neighboring hexes
      this->resizeInteriorTraceGroups(1); //one group for all interior traces
    }
    else //Both kinds of neighbors
    {
      SANS_DEVELOPER_EXCEPTION( "XField_Local<PhysDim, TopoD3, T>::extractLocalGrid - Mixed meshes not supported yet." );
    }
  }
  else //No neighboring cells
  {
    this->resizeCellGroups(1); // one group for main cell
    this->resizeInteriorTraceGroups(0);
  }

  //a group each for each BC of main cell + one group for outer boundaries of neighboring cells
  nMainBTraceGroups_ = TopoMainCell::NTrace - nNeighbors_;
  nOuterBTraceGroups_ = 1;
  if (nTetNeighbors_>0 && nHexNeighbors_>0) nOuterBTraceGroups_ = 2; //If this is a mixed mesh, we need 2 outer Btrace groups (for triangles/quads)

  if (nNeighbors_==0) nOuterBTraceGroups_ = 0; //If there are no neighbors - there is no outer boundary of the neighboring cells
  this->resizeBoundaryTraceGroups(nMainBTraceGroups_);
  this->resizeGhostBoundaryTraceGroups(nOuterBTraceGroups_);

  if (nOuterBTraceGroups_ > 0)
  {
    OuterBTraceGroup_Tri_ = 0;
    OuterBTraceGroup_Quad_ = 1;

    if (nTetNeighbors_ == 0) //Promote quad btraces if there are no tet neighbors
      OuterBTraceGroup_Quad_--;
  }

  //Zero out mesh DOFs
  for (int k = 0; k < this->nDOF_; k++)
    this->DOF_[k] = 0.0;

  //Offsets for cell, face, edge and node DOFs in the global array
  //(cell DOFs first, face DOFs second, edge DOFs third, node DOFs last)
  offset_cellDOF_ = 0;
  offset_faceDOF_ = (int) CellDOFMap_.size();
  offset_edgeDOF_ = (int) CellDOFMap_.size() + (int) FaceDOFMap_.size();
  offset_nodeDOF_ = (int) CellDOFMap_.size() + (int) FaceDOFMap_.size() + (int) EdgeDOFMap_.size();


  //-------------------------------------------------------------------------------------------------------------
  //Copy and fill associativities of neighboring cells, interior traces and boundary traces
  extractNeighbors(main_group, main_elem, TopoMainCell::NTrace, order);


  //------------------------------Fill in associativity for main cell--------------------------------------------

  // create field associativity constructor for main cell
  typename XFieldCellGroupType::FieldAssociativityConstructorType fldAssocCell_Main( cell_basis, 1 ); //1 cell for main cell group

  ElementAssociativityConstructor<TopoD3, TopoMainCell> DOFAssoc_MainCell( cell_basis );

  setCellAssociativity(main_group, main_elem, DOFAssoc_MainCell);

  //The maincell_reversed_traces_ vector was filled in extractNeighbors()
  for (int k=0; k<(int)maincell_reversed_traces_.size(); k++)
    DOFAssoc_MainCell.setFaceSign(+1, maincell_reversed_traces_[k]);

  fldAssocCell_Main.setAssociativity( DOFAssoc_MainCell, 0 );

  //Add cell group and copy DOFs for main cell
  this->cellGroups_[0] = new XFieldCellGroupType(fldAssocCell_Main);
  this->cellGroups_[0]->setDOF( this->DOF_, this->nDOF_ );
  this->nElem_ += fldAssocCell_Main.nElem();

  ElementXFieldClass xfldElem_main( cell_basis );
  cellgrp_main.getElement(xfldElem_main, main_elem); //Get current (main) element
  this->template getCellGroup<TopoMainCell>(0).setElement(xfldElem_main,0);

  //Save main cell's mapping to global mesh
  this->CellMapping_[{0,0}] = {main_group,main_elem};
  this->CellSplitInfo_[{0,0}] = ElementSplitInfo(ElementSplitFlag::Unsplit);
}


template <class PhysDim>
void
XField_Local<PhysDim, TopoD3>::extractNeighbors(int main_group, int main_elem, int Ntrace, int order)
{
  //Volumes
  typedef typename XField<PhysDim, TopoD3>::template FieldCellGroupType<Tet> XFieldCellGroupType_Tet;
  typedef typename XField<PhysDim, TopoD3>::template FieldCellGroupType<Hex> XFieldCellGroupType_Hex;
  typedef typename XFieldCellGroupType_Tet::template ElementType<> ElementXFieldClass_Tet;
  typedef typename XFieldCellGroupType_Hex::template ElementType<> ElementXFieldClass_Hex;
  typedef typename XFieldCellGroupType_Tet::BasisType CellBasisType_Tet;
  typedef typename XFieldCellGroupType_Hex::BasisType CellBasisType_Hex;

  //Faces
  typedef typename XField<PhysDim, TopoD3>::template FieldTraceGroupType<Triangle> XFieldTraceGroupType_Tri;
  typedef typename XField<PhysDim, TopoD3>::template FieldTraceGroupType<Quad> XFieldTraceGroupType_Quad;
  typedef typename XFieldTraceGroupType_Tri::BasisType TraceBasisType_Tri;
  typedef typename XFieldTraceGroupType_Quad::BasisType TraceBasisType_Quad;

  const CellBasisType_Tet* cell_basis_Tet = NULL;
  const CellBasisType_Hex* cell_basis_Hex = NULL;

  const TraceBasisType_Tri* trace_basis_Tri = NULL;
  const TraceBasisType_Quad* trace_basis_Quad = NULL;

  if (nTetNeighbors_>0)
  {
    cell_basis_Tet = BasisFunctionVolumeBase<Tet>::getBasisFunction(order, basisCategory_);
    trace_basis_Tri = BasisFunctionAreaBase<Triangle>::getBasisFunction(order, basisCategory_);
  }
  if (nHexNeighbors_>0)
  {
    cell_basis_Hex = BasisFunctionVolumeBase<Hex>::getBasisFunction(order, basisCategory_);
    trace_basis_Quad = BasisFunctionAreaBase<Quad>::getBasisFunction(order, basisCategory_);
  }

  // create field associativity constructor for neighboring cells
  std::shared_ptr<typename XFieldCellGroupType_Tet::FieldAssociativityConstructorType> fldAssocCell_Neighbors_Tet; //tet neighbors
  std::shared_ptr<typename XFieldCellGroupType_Hex::FieldAssociativityConstructorType> fldAssocCell_Neighbors_Hex; //hex neighbors

  // create field associativity constructor for interior traces
  std::shared_ptr<typename XFieldTraceGroupType_Tri::FieldAssociativityConstructorType> fldAssoc_ITrace_Tri;
  std::shared_ptr<typename XFieldTraceGroupType_Quad::FieldAssociativityConstructorType> fldAssoc_ITrace_Quad;

  // create field associativity constructor for outer boundary traces of neighboring cells (3 faces for each neighboring tet, 5 for each quad)
  std::shared_ptr<typename XFieldTraceGroupType_Tri::FieldAssociativityConstructorType> fldAssoc_Outer_BTrace_Tri;
  std::shared_ptr<typename XFieldTraceGroupType_Quad::FieldAssociativityConstructorType> fldAssoc_Outer_BTrace_Quad;


  //Assign cell group indices to neighbors (index 0 is for main cell) in local mesh
  int CellgrpTetNeighbors = 1;
  int CellgrpHexNeighbors = 2;

  int ITracegrpTriNeighbors = 0;
  int ITracegrpQuadNeighbors = 1;

  if (nNeighbors_ > 0)
  {
    fldAssocCell_Neighbors_Tet  = std::make_shared< typename XFieldCellGroupType_Tet::FieldAssociativityConstructorType>
                                                 ( cell_basis_Tet, nTetNeighbors_ );

    fldAssocCell_Neighbors_Hex  = std::make_shared< typename XFieldCellGroupType_Hex::FieldAssociativityConstructorType>
                                                 ( cell_basis_Hex, nHexNeighbors_ );

    fldAssoc_ITrace_Tri         = std::make_shared< typename XFieldTraceGroupType_Tri::FieldAssociativityConstructorType>
                                                 ( trace_basis_Tri, nTetNeighbors_);

    fldAssoc_ITrace_Quad        = std::make_shared< typename XFieldTraceGroupType_Quad::FieldAssociativityConstructorType>
                                                 ( trace_basis_Quad, nHexNeighbors_);

    fldAssoc_Outer_BTrace_Tri   = std::make_shared< typename XFieldTraceGroupType_Tri::FieldAssociativityConstructorType>
                                                 ( trace_basis_Tri, (Tet::NTrace - 1)*nTetNeighbors_);

    fldAssoc_Outer_BTrace_Quad  = std::make_shared< typename XFieldTraceGroupType_Quad::FieldAssociativityConstructorType>
                                                 ( trace_basis_Quad, (Hex::NTrace - 1)*nHexNeighbors_);

    //if no tet neighbors, then promote the hex neighbor cellgroup and the interior trace group
    if (nTetNeighbors_==0)
    {
      CellgrpHexNeighbors--;
      ITracegrpQuadNeighbors--;
    }

  }

  int cnt_neighbor_tet = 0; //counter to keep track of tet neighbors whose associativity has been processed
  int cnt_neighbor_hex = 0; //counter to keep track of hex neighbors whose associativity has been processed
  int cnt_main_Btraces = 0; //counter to keep track of the boundary traces of the main cell which have been processed

  int cnt_outer_Btraces_tri = 0; //counter to keep track of the outer boundary traces which have been processed
  int cnt_outer_Btraces_quad = 0; //counter to keep track of the outer boundary traces which have been processed

  std::vector<ElementXFieldClass_Tet> neighborTet_list;
  std::vector<ElementXFieldClass_Hex> neighborHex_list;

  //-----------Fill in associativity for neighboring cells--------------------

  //Repeat loop across neighboring elements to fill associativity
  for (int itrace = 0; itrace < Ntrace; itrace++)
  {
    //Get indices of each neighboring trace (and its cellgroup)
    const TraceInfo& traceinfo = connectivity_.getTrace(main_group, main_elem, itrace);

    if (traceinfo.type == TraceInfo::Interior)
    {
      int neighbor_group = -1; //cellgroup of neighboring element in global mesh
      int neighbor_elem = -1; //element index of neighboring element in global mesh
      CanonicalTraceToCell neighbor_canonicalTrace; //canonicalTrace of the neighboring cell for the current trace
      CanonicalTraceToCell main_canonicalTrace; //canonicalTrace of the main cell for the current trace
      bool reverse_trace_flag = false; // (false) if main cell is to the left of interior trace, (true) otherwise.

      if (global_xfld_.getInteriorTraceGroupBase(traceinfo.group).topoTypeID() == typeid(Triangle))
      {
        getNeighborInfoFromTrace<Triangle>(traceinfo.group, traceinfo.elem, main_elem,
                                           neighbor_group, neighbor_elem, reverse_trace_flag,
                                           neighbor_canonicalTrace, main_canonicalTrace);
      }
      else if (global_xfld_.getInteriorTraceGroupBase(traceinfo.group).topoTypeID() == typeid(Quad))
      {
        getNeighborInfoFromTrace<Quad>(traceinfo.group, traceinfo.elem, main_elem,
                                       neighbor_group, neighbor_elem, reverse_trace_flag,
                                       neighbor_canonicalTrace, main_canonicalTrace);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "XField_Local<PhysDim, TopoD3, T>::extractLocalGrid - Unknown trace topology for interior trace." );


      if ( global_xfld_.getCellGroupBase(neighbor_group).topoTypeID() == typeid(Tet) )
      {
        const XFieldCellGroupType_Tet& cellgrp_neighbor = global_xfld_.template getCellGroup<Tet>(neighbor_group);
        ElementXFieldClass_Tet xfldElem_neighbor(cell_basis_Tet);
        cellgrp_neighbor.getElement(xfldElem_neighbor,neighbor_elem); //Get neighboring cell
        neighborTet_list.push_back(xfldElem_neighbor);

        // DOF associativity for neighbor cells
        ElementAssociativityConstructor<TopoD3, Tet> DOFAssoc_NeighborCell( cell_basis_Tet );

        setCellAssociativity(neighbor_group, neighbor_elem, DOFAssoc_NeighborCell);

        //Save this neighboring cell's mapping from local to global mesh
        this->CellMapping_[{CellgrpTetNeighbors,cnt_neighbor_tet}] = {neighbor_group,neighbor_elem};
        this->CellSplitInfo_[{CellgrpTetNeighbors,cnt_neighbor_tet}] = ElementSplitInfo(ElementSplitFlag::Unsplit);

        neighborcell_reversed_traces_.clear();

        //Add associativity for outer boundary traces of this neighbor cell
        process_OuterBTraces<Triangle,Tet>(neighbor_group, neighbor_elem, order, Tet::NTrace, CellgrpTetNeighbors, cnt_neighbor_tet,
                                           neighbor_canonicalTrace, fldAssoc_Outer_BTrace_Tri, OuterBTraceGroup_Tri_, cnt_outer_Btraces_tri);

        //Updating faceSigns for any outer-edges that were reversed
        for (int k=0; k<(int)neighborcell_reversed_traces_.size(); k++)
          DOFAssoc_NeighborCell.setFaceSign(+1, neighborcell_reversed_traces_[k]);

        // DOF associativity for interior trace
        ElementAssociativityConstructor<TopoD2, Triangle> DOFAssoc_ITrace( trace_basis_Tri );

        //Set DOF associativity
        setInteriorTraceAssociativity(traceinfo.group, traceinfo.elem, DOFAssoc_ITrace);

        //Need to reverse traces to ensure that the main cell is always to the left of its neighbors
        if (reverse_trace_flag==true)
        {
          //Change nodeDOF ordering of this interior trace to ensure that the main cell is to the left of it
          //and obtain the updated canonicalTrace for the neighbor cell
          neighbor_canonicalTrace = reverseTrace<Triangle,Tet>(traceinfo.group, traceinfo.elem, DOFAssoc_ITrace,
                                                               main_group, main_elem, neighbor_group, neighbor_elem);

          DOFAssoc_NeighborCell.setFaceSign(neighbor_canonicalTrace.orientation, neighbor_canonicalTrace.trace);

          main_canonicalTrace.orientation = 1;
          maincell_reversed_traces_.push_back(main_canonicalTrace.trace);
        }

        fldAssocCell_Neighbors_Tet->setAssociativity( DOFAssoc_NeighborCell, cnt_neighbor_tet);

        int itrace_index = cnt_neighbor_tet; //The new index for this interior trace is simply the number of neighboring cells passed

        fldAssoc_ITrace_Tri->setAssociativity( DOFAssoc_ITrace, itrace_index );

        //Save this interior trace's mapping from local to global mesh
        this->InteriorTraceMapping_[{ITracegrpTriNeighbors,itrace_index}] = {traceinfo.group,traceinfo.elem};
        this->InteriorTraceSplitInfo_[{ITracegrpTriNeighbors,itrace_index}] = ElementSplitInfo(ElementSplitFlag::Unsplit);

        fldAssoc_ITrace_Tri->setGroupLeft(0); //Cell group on left is the main cell's group = 0
        fldAssoc_ITrace_Tri->setElementLeft(0, itrace_index);
        fldAssoc_ITrace_Tri->setCanonicalTraceLeft(main_canonicalTrace, itrace_index);

        fldAssoc_ITrace_Tri->setGroupRight(CellgrpTetNeighbors); //Cell group on right is the triangle neighbors' cell group
        fldAssoc_ITrace_Tri->setElementRight(itrace_index, itrace_index); //Right element for interior trace i is equal to i
        fldAssoc_ITrace_Tri->setCanonicalTraceRight(neighbor_canonicalTrace, itrace_index);

        cnt_neighbor_tet++;
      }
      else if ( global_xfld_.getCellGroupBase(neighbor_group).topoTypeID() == typeid(Hex) )
      {

        const XFieldCellGroupType_Hex& cellgrp_neighbor = global_xfld_.template getCellGroup<Hex>(neighbor_group);
        ElementXFieldClass_Hex xfldElem_neighbor(cell_basis_Hex);
        cellgrp_neighbor.getElement(xfldElem_neighbor,neighbor_elem); //Get neighboring cell
        neighborHex_list.push_back(xfldElem_neighbor);

        // DOF associativity for neighbor cells
        ElementAssociativityConstructor<TopoD3, Hex> DOFAssoc_NeighborCell( cell_basis_Hex );

        setCellAssociativity(neighbor_group, neighbor_elem, DOFAssoc_NeighborCell);

        //Save this neighboring cell's mapping from local to global mesh
        this->CellMapping_[{CellgrpHexNeighbors,cnt_neighbor_hex}] = {neighbor_group,neighbor_elem};
        this->CellSplitInfo_[{CellgrpHexNeighbors,cnt_neighbor_hex}] = ElementSplitInfo(ElementSplitFlag::Unsplit);

        neighborcell_reversed_traces_.clear();

        //Add associativity for outer boundary traces of this neighbor cell
        process_OuterBTraces<Quad,Hex>(neighbor_group, neighbor_elem, order, Hex::NTrace, CellgrpHexNeighbors, cnt_neighbor_hex,
                                       neighbor_canonicalTrace, fldAssoc_Outer_BTrace_Quad, OuterBTraceGroup_Quad_, cnt_outer_Btraces_quad);

        //Updating faceSigns for any outer-edges that were reversed
        for (int k=0; k<(int)neighborcell_reversed_traces_.size(); k++)
          DOFAssoc_NeighborCell.setFaceSign(+1, neighborcell_reversed_traces_[k]);

        // DOF associativity for interior trace
        ElementAssociativityConstructor<TopoD2, Quad> DOFAssoc_ITrace( trace_basis_Quad );

        //Set DOF associativity
        setInteriorTraceAssociativity(traceinfo.group, traceinfo.elem, DOFAssoc_ITrace);

        //Need to reverse traces to ensure that the main cell is always to the left of its neighbors
        if (reverse_trace_flag==true)
        {
          //Change nodeDOF ordering of this interior trace to ensure that the main cell is to the left of it
          //and obtain the updated canonicalTrace for the neighbor cell
          neighbor_canonicalTrace = reverseTrace<Quad,Hex>(traceinfo.group, traceinfo.elem, DOFAssoc_ITrace,
                                                           main_group, main_elem, neighbor_group, neighbor_elem);

          DOFAssoc_NeighborCell.setFaceSign(neighbor_canonicalTrace.orientation, neighbor_canonicalTrace.trace);

          main_canonicalTrace.orientation = 1;
          maincell_reversed_traces_.push_back(main_canonicalTrace.trace);
        }

        fldAssocCell_Neighbors_Hex->setAssociativity( DOFAssoc_NeighborCell, cnt_neighbor_hex);

        int itrace_index = cnt_neighbor_hex; //The new index for this interior trace is simply the number of neighboring cells passed

        fldAssoc_ITrace_Quad->setAssociativity( DOFAssoc_ITrace, itrace_index );

        //Save this interior trace's mapping from local to global mesh
        this->InteriorTraceMapping_[{ITracegrpQuadNeighbors,itrace_index}] = {traceinfo.group,traceinfo.elem};
        this->InteriorTraceSplitInfo_[{ITracegrpQuadNeighbors,itrace_index}] = ElementSplitInfo(ElementSplitFlag::Unsplit);

        fldAssoc_ITrace_Quad->setGroupLeft(0); //Cell group on left is the main cell's group = 0
        fldAssoc_ITrace_Quad->setElementLeft(0, itrace_index);
        fldAssoc_ITrace_Quad->setCanonicalTraceLeft(main_canonicalTrace, itrace_index);

        fldAssoc_ITrace_Quad->setGroupRight(CellgrpHexNeighbors); //Cell group on right is the triangle neighbors' cell group
        fldAssoc_ITrace_Quad->setElementRight(itrace_index, itrace_index); //Right element for interior trace i is equal to i
        fldAssoc_ITrace_Quad->setCanonicalTraceRight(neighbor_canonicalTrace, itrace_index);

        cnt_neighbor_hex++;
      }
      else
        SANS_DEVELOPER_EXCEPTION( "XField_Local<PhysDim, TopoD3, T>::extractLocalGrid - Unknown cell topology for neighbor." );

    }
    else if (traceinfo.type == TraceInfo::Boundary) //We are dealing with a boundary trace of the main cell
    {
      if (global_xfld_.getBoundaryTraceGroupBase(traceinfo.group).topoTypeID() == typeid(Triangle))
      {
        process_MainBTrace<Triangle>(traceinfo.group, traceinfo.elem, order, cnt_main_Btraces);
      }
      else if (global_xfld_.getBoundaryTraceGroupBase(traceinfo.group).topoTypeID() == typeid(Quad))
      {
        process_MainBTrace<Quad>(traceinfo.group, traceinfo.elem, order, cnt_main_Btraces);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "XField_Local<PhysDim, TopoD3, T>::extractLocalGrid - Unknown trace topology for boundary trace." );
    }
  } //repeated loop across traces of main element

  if (nNeighbors_>0)
  {
    if (nTetNeighbors_>0) //Tet neighbors
    {
      //Add cell group and copy DOFs for neighboring cells
      this->cellGroups_[CellgrpTetNeighbors] = new XFieldCellGroupType_Tet( *fldAssocCell_Neighbors_Tet );
      this->cellGroups_[CellgrpTetNeighbors]->setDOF( this->DOF_, this->nDOF_ );
      this->nElem_ += fldAssocCell_Neighbors_Tet->nElem();
      for (int k=0; k<nTetNeighbors_; k++)
        this->template getCellGroup<Tet>(CellgrpTetNeighbors).setElement(neighborTet_list[k],k);

      //Add trace group and set DOFs for interior traces
      this->interiorTraceGroups_[ITracegrpTriNeighbors] = new XFieldTraceGroupType_Tri( *fldAssoc_ITrace_Tri );
      this->interiorTraceGroups_[ITracegrpTriNeighbors]->setDOF( this->DOF_, this->nDOF_ );
    }

    if (nHexNeighbors_>0) //Hex neighbors
    {
      //Add cell group and copy DOFs for neighboring cells
      this->cellGroups_[CellgrpHexNeighbors] = new XFieldCellGroupType_Hex( *fldAssocCell_Neighbors_Hex );
      this->cellGroups_[CellgrpHexNeighbors]->setDOF( this->DOF_, this->nDOF_ );
      this->nElem_ += fldAssocCell_Neighbors_Hex->nElem();
      for (int k=0; k<nHexNeighbors_; k++)
        this->template getCellGroup<Hex>(CellgrpHexNeighbors).setElement(neighborHex_list[k],k);

      //Add trace group and set DOFs for interior traces
      this->interiorTraceGroups_[ITracegrpQuadNeighbors] = new XFieldTraceGroupType_Quad( *fldAssoc_ITrace_Quad );
      this->interiorTraceGroups_[ITracegrpQuadNeighbors]->setDOF( this->DOF_, this->nDOF_ );
    }

    if (nOuterBTraceGroups_>0)
    {
      //Add trace group and copy DOFs for interior traces
      if (nTetNeighbors_>0) //Tet neighbors
      {
        this->ghostBoundaryTraceGroups_[OuterBTraceGroup_Tri_] = new XFieldTraceGroupType_Tri( *fldAssoc_Outer_BTrace_Tri );
        this->ghostBoundaryTraceGroups_[OuterBTraceGroup_Tri_]->setDOF( this->DOF_, this->nDOF_ );
      }
      if (nHexNeighbors_>0) //Hex neighbors
      {
        this->ghostBoundaryTraceGroups_[OuterBTraceGroup_Quad_] = new XFieldTraceGroupType_Quad( *fldAssoc_Outer_BTrace_Quad );
        this->ghostBoundaryTraceGroups_[OuterBTraceGroup_Quad_]->setDOF( this->DOF_, this->nDOF_ );
      }
    }
  } //if neighbors

}

template <class PhysDim>
template <class TopoCell>
void
XField_Local<PhysDim, TopoD3>::setCellAssociativity(int group, int elem, ElementAssociativityConstructor<TopoD3, TopoCell>& DOFAssoc)
{

  typedef typename XField<PhysDim, TopoD3>::template FieldCellGroupType<TopoCell> XFieldCellGroupType;

  //Get global cell group
  const XFieldCellGroupType& cellgrp_main = global_xfld_.template getCellGroup<TopoCell>(group);

  int nNodeDOF = cellgrp_main.associativity(elem).nNode();
  int nEdgeDOF = cellgrp_main.associativity(elem).nEdge();
  int nFaceDOF = cellgrp_main.associativity(elem).nFace();
  int nCellDOF = cellgrp_main.associativity(elem).nCell();

  // node DOF, edge DOF, face DOF and cell DOF ordering for cell
  std::vector<int> Cell_nodeDOF_map(nNodeDOF);
  std::vector<int> Cell_edgeDOF_map(nEdgeDOF);
  std::vector<int> Cell_faceDOF_map(nFaceDOF);
  std::vector<int> Cell_cellDOF_map(nCellDOF);

  cellgrp_main.associativity(elem).getNodeGlobalMapping( Cell_nodeDOF_map.data(), nNodeDOF ); //Get nodeDOF map from global field
  cellgrp_main.associativity(elem).getEdgeGlobalMapping( Cell_edgeDOF_map.data(), nEdgeDOF ); //Get edgeDOF map from global field
  cellgrp_main.associativity(elem).getFaceGlobalMapping( Cell_faceDOF_map.data(), nFaceDOF ); //Get faceDOF map from global field
  cellgrp_main.associativity(elem).getCellGlobalMapping( Cell_cellDOF_map.data(), nCellDOF ); //Get cellDOF map from global field

  //Map DOFs from global to local mesh
  for (int k = 0; k < nNodeDOF; k++)
    Cell_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Cell_nodeDOF_map[k]);

  for (int k = 0; k < nEdgeDOF; k++)
    Cell_edgeDOF_map[k] = offset_edgeDOF_ + EdgeDOFMap_.at(Cell_edgeDOF_map[k]);

  for (int k = 0; k < nFaceDOF; k++)
    Cell_faceDOF_map[k] = offset_faceDOF_ + FaceDOFMap_.at(Cell_faceDOF_map[k]);

  for (int k = 0; k < nCellDOF; k++)
    Cell_cellDOF_map[k] = offset_cellDOF_ + CellDOFMap_.at(Cell_cellDOF_map[k]);

  DOFAssoc.setRank( 0 ); // local to this processor (the comm is split)
  DOFAssoc.setNodeGlobalMapping( Cell_nodeDOF_map );
  DOFAssoc.setEdgeGlobalMapping( Cell_edgeDOF_map );
  DOFAssoc.setFaceGlobalMapping( Cell_faceDOF_map );
  DOFAssoc.setCellGlobalMapping( Cell_cellDOF_map );

  DOFAssoc.faceSign() = cellgrp_main.associativity(elem).faceSign();

}

template<class PhysDim>
template <class TopoTrace>
void
XField_Local<PhysDim, TopoD3>::setInteriorTraceAssociativity(int group, int elem, ElementAssociativityConstructor<TopoD2, TopoTrace>& DOFAssoc)
{

  typedef typename XField<PhysDim, TopoD3>::template FieldTraceGroupType<TopoTrace> XFieldTraceGroupType;

  //Get global interior trace group
  const XFieldTraceGroupType& tracegrp = global_xfld_.template getInteriorTraceGroup<TopoTrace>(group);

  int nNodeDOF = tracegrp.associativity(elem).nNode();
  int nEdgeDOF = tracegrp.associativity(elem).nEdge();
  int nFaceDOF = tracegrp.associativity(elem).nCell();

  // node DOF, edge DOF and face DOF ordering for interior trace
  std::vector<int> Trace_nodeDOF_map(nNodeDOF);
  std::vector<int> Trace_edgeDOF_map(nEdgeDOF);
  std::vector<int> Trace_faceDOF_map(nFaceDOF);

  tracegrp.associativity(elem).getNodeGlobalMapping( Trace_nodeDOF_map.data(), nNodeDOF ); //Get nodeDOF map from global field
  tracegrp.associativity(elem).getEdgeGlobalMapping( Trace_edgeDOF_map.data(), nEdgeDOF ); //Get edgeDOF map from global field
  tracegrp.associativity(elem).getCellGlobalMapping( Trace_faceDOF_map.data(), nFaceDOF ); //Get cellDOF map from global field

  //Map DOFs from global to local mesh
  for (int k = 0; k < nNodeDOF; k++)
    Trace_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Trace_nodeDOF_map[k]);

  for (int k = 0; k < nEdgeDOF; k++)
    Trace_edgeDOF_map[k] = offset_edgeDOF_ + EdgeDOFMap_.at(Trace_edgeDOF_map[k]);

  for (int k = 0; k < nFaceDOF; k++)
    Trace_faceDOF_map[k] = offset_faceDOF_ + FaceDOFMap_.at(Trace_faceDOF_map[k]);

  DOFAssoc.setRank( 0 ); // local to this processor (the comm is split)
  DOFAssoc.setNodeGlobalMapping( Trace_nodeDOF_map );
  DOFAssoc.setEdgeGlobalMapping( Trace_edgeDOF_map );
  DOFAssoc.setCellGlobalMapping( Trace_faceDOF_map );

}


template<class PhysDim>
template <class TopoTrace>
void
XField_Local<PhysDim, TopoD3>::
setBoundaryTraceAssociativity(typename XField<PhysDim, TopoD3>::template FieldTraceGroupType<TopoTrace> tracegrp,
                              int elem, ElementAssociativityConstructor<TopoD2, TopoTrace>& DOFAssoc)
{
  int nNodeDOF = tracegrp.associativity(elem).nNode();
  int nEdgeDOF = tracegrp.associativity(elem).nEdge();
  int nFaceDOF = tracegrp.associativity(elem).nCell();

  // node DOF, edge DOF and face DOF ordering for boundary trace
  std::vector<int> Trace_nodeDOF_map(nNodeDOF);
  std::vector<int> Trace_edgeDOF_map(nEdgeDOF);
  std::vector<int> Trace_faceDOF_map(nFaceDOF);

  tracegrp.associativity(elem).getNodeGlobalMapping( Trace_nodeDOF_map.data(), nNodeDOF ); //Get nodeDOF map from global field
  tracegrp.associativity(elem).getEdgeGlobalMapping( Trace_edgeDOF_map.data(), nEdgeDOF ); //Get edgeDOF map from global field
  tracegrp.associativity(elem).getCellGlobalMapping( Trace_faceDOF_map.data(), nFaceDOF ); //Get cellDOF map from global field

  //Map DOFs from global to local mesh
  for (int k = 0; k < nNodeDOF; k++)
    Trace_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Trace_nodeDOF_map[k]);

  for (int k = 0; k < nEdgeDOF; k++)
    Trace_edgeDOF_map[k] = offset_edgeDOF_ + EdgeDOFMap_.at(Trace_edgeDOF_map[k]);

  for (int k = 0; k < nFaceDOF; k++)
    Trace_faceDOF_map[k] = offset_faceDOF_ + FaceDOFMap_.at(Trace_faceDOF_map[k]);

  DOFAssoc.setRank( 0 ); // local to this processor (the comm is split)
  DOFAssoc.setNodeGlobalMapping( Trace_nodeDOF_map );
  DOFAssoc.setEdgeGlobalMapping( Trace_edgeDOF_map );
  DOFAssoc.setCellGlobalMapping( Trace_faceDOF_map );

}

template<class PhysDim>
template <class TopoTrace, class TopoCell>
CanonicalTraceToCell
XField_Local<PhysDim, TopoD3>::reverseTrace(int trace_group, int trace_elem, ElementAssociativityConstructor<TopoD2, TopoTrace>& DOFAssoc,
                                             int cellL_group, int cellL_elem, int cellR_group, int cellR_elem)
{

  typedef typename XField<PhysDim, TopoD3>::template FieldCellGroupType<TopoCell> XFieldCellGroupType;
  typedef typename XField<PhysDim, TopoD3>::template FieldTraceGroupType<TopoTrace> XFieldTraceGroupType;

  //Get global cell groups
  const XFieldCellGroupType& cellgrpL = global_xfld_.template getCellGroup<TopoCell>(cellL_group);
  const XFieldCellGroupType& cellgrpR = global_xfld_.template getCellGroup<TopoCell>(cellR_group);

  //Get global interior trace group
  const XFieldTraceGroupType& tracegrp = global_xfld_.template getInteriorTraceGroup<TopoTrace>(trace_group);

  const int nNodeDOF_trace = TopoTrace::NNode;
  const int nNodeDOF_cellL = TopoCell::NNode;
  const int nNodeDOF_cellR = TopoCell::NNode;

  //Node DOF ordering for interior trace
  int Trace_nodeDOF_map[nNodeDOF_trace];
  tracegrp.associativity(trace_elem).getNodeGlobalMapping( Trace_nodeDOF_map, nNodeDOF_trace ); //Get nodeDOF map from global field

  //Node DOF ordering for left and right cells
  int CellL_nodeDOF_map[nNodeDOF_cellL];
  int CellR_nodeDOF_map[nNodeDOF_cellR];

  cellgrpL.associativity(cellL_elem).getNodeGlobalMapping( CellL_nodeDOF_map, nNodeDOF_cellL ); //Get nodeDOF map from global field
  cellgrpR.associativity(cellR_elem).getNodeGlobalMapping( CellR_nodeDOF_map, nNodeDOF_cellR ); //Get nodeDOF map from global field

  //Get the node order which makes this trace a canonical trace (i.e. orientation = 1) of the left cell
  int canonicalTrace_nodeDOF_map[nNodeDOF_trace];
  TraceToCellRefCoord<TopoTrace, TopoD3, TopoCell>::getCanonicalTraceLeft(Trace_nodeDOF_map, nNodeDOF_trace,
                                                                          CellL_nodeDOF_map, nNodeDOF_cellL,
                                                                          canonicalTrace_nodeDOF_map, nNodeDOF_trace);

  //Get the new canonicalTrace info for the updated interior trace, as seen from the right cell.
  CanonicalTraceToCell canonical_traceR = TraceToCellRefCoord<TopoTrace, TopoD3, TopoCell>::
                                          getCanonicalTrace(canonicalTrace_nodeDOF_map, nNodeDOF_trace, CellR_nodeDOF_map, nNodeDOF_cellR);

  //Map DOFs from global to local mesh
  for (int k = 0; k < nNodeDOF_trace; k++)
    canonicalTrace_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(canonicalTrace_nodeDOF_map[k]);

  DOFAssoc.setNodeGlobalMapping( canonicalTrace_nodeDOF_map, nNodeDOF_trace );

  return canonical_traceR;
}

template<class PhysDim>
template <class TopoTrace>
void
XField_Local<PhysDim, TopoD3>::getNeighborInfoFromTrace(const int trace_group, const int trace_elem, const int main_elem,
                                                         int& neighbor_group, int& neighbor_elem, bool& reverse_trace_flag,
                                                         CanonicalTraceToCell& neighbor_canonicalTrace, CanonicalTraceToCell& main_canonicalTrace)
{

  typedef typename XField<PhysDim, TopoD3>::template FieldTraceGroupType<TopoTrace> XFieldTraceGroupType;
  const XFieldTraceGroupType& tracegrp = global_xfld_.template getInteriorTraceGroup<TopoTrace>(trace_group);

  if (tracegrp.getElementLeft(trace_elem)==main_elem)
  { //element to left of trace is the "main_elem" => getElementRight gives the neighboring cell
    neighbor_group          = tracegrp.getGroupRight();
    neighbor_elem           = tracegrp.getElementRight(trace_elem);
    neighbor_canonicalTrace = tracegrp.getCanonicalTraceRight(trace_elem);
    main_canonicalTrace     = tracegrp.getCanonicalTraceLeft(trace_elem);
    reverse_trace_flag = false;
  }
  else
  { //element to left of trace is not the "main_elem" => getElementLeft gives the neighboring cell
    neighbor_group          = tracegrp.getGroupLeft();
    neighbor_elem           = tracegrp.getElementLeft(trace_elem);
    neighbor_canonicalTrace = tracegrp.getCanonicalTraceLeft(trace_elem);
    main_canonicalTrace     = tracegrp.getCanonicalTraceRight(trace_elem);
    reverse_trace_flag = true;
  }
}

template<class PhysDim>
template <class TopoTrace>
void
XField_Local<PhysDim, TopoD3>::process_MainBTrace(const int trace_group, const int trace_elem, const int order, int& cnt_main_Btraces)
{

  typedef typename XField<PhysDim, TopoD3>::template FieldTraceGroupType<TopoTrace> XFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::BasisType TraceBasisType;

  const TraceBasisType* trace_basis = BasisFunctionAreaBase<TopoTrace>::getBasisFunction(order, basisCategory_);

  const XFieldTraceGroupType& tracegrp = global_xfld_.template getBoundaryTraceGroup<TopoTrace>(trace_group);

  //canonicalTrace of the main cell for the current trace
  CanonicalTraceToCell main_canonicalTrace = tracegrp.getCanonicalTraceLeft(trace_elem);

  // DOF associativity for boundary trace
  ElementAssociativityConstructor<TopoD2, TopoTrace> DOFAssoc_BTrace( trace_basis );

  setBoundaryTraceAssociativity(tracegrp, trace_elem, DOFAssoc_BTrace);

  // create field associativity constructor for each boundary trace of main cell
  // Note: each boundary trace of the main cell has its own boundaryTraceGroup!
  typename XFieldTraceGroupType::FieldAssociativityConstructorType fldAssoc_Main_BTrace( trace_basis, 1);

  fldAssoc_Main_BTrace.setAssociativity( DOFAssoc_BTrace, 0);

  //Save this boundary trace's mapping from local to global mesh
  this->BoundaryTraceMapping_[{cnt_main_Btraces,0}] = {trace_group,trace_elem};
  this->BoundaryTraceSplitInfo_[{cnt_main_Btraces,0}] = ElementSplitInfo(ElementSplitFlag::Unsplit);

  fldAssoc_Main_BTrace.setGroupLeft(0); //Cell group on left is the main cell's group = 0
  fldAssoc_Main_BTrace.setElementLeft(0, 0);
  fldAssoc_Main_BTrace.setCanonicalTraceLeft(main_canonicalTrace, 0);

  //Add trace group and copy DOFs for boundary traces of main cell
  this->boundaryTraceGroups_[cnt_main_Btraces] = new XFieldTraceGroupType( fldAssoc_Main_BTrace );
  this->boundaryTraceGroups_[cnt_main_Btraces]->setDOF( this->DOF_, this->nDOF_ );
  cnt_main_Btraces++;

}

template<class PhysDim>
template <class TopoTrace, class TopoCell>
void
XField_Local<PhysDim, TopoD3>::process_OuterBTraces(const int neighbor_group, const int neighbor_elem, const int order, const int Ntrace,
                                                     const int local_neighbor_group, const int local_neighbor_elem,
                                                     const CanonicalTraceToCell& neighbor_canonicalTrace,
                                                     std::shared_ptr<typename BaseType::template FieldTraceGroupType<TopoTrace>
                                                                                      ::FieldAssociativityConstructorType> fldAssoc_Outer_BTrace,
                                                     const int OuterBTraceGroup, int& cnt_outer_Btraces)
{

  typedef typename XField<PhysDim, TopoD3>::template FieldTraceGroupType<TopoTrace> XFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::BasisType TraceBasisType;

  const TraceBasisType* trace_basis = BasisFunctionAreaBase<TopoTrace>::getBasisFunction(order, basisCategory_);

  // DOF associativity for boundary traces of neighbors
  ElementAssociativityConstructor<TopoD2, TopoTrace> DOFAssoc_Outer_BTrace( trace_basis );

  for (int j = 0; j < Ntrace; j++) //loop across traces of this neighboring element
  {
    if (j != neighbor_canonicalTrace.trace) //Outer boundary trace
    {
      //Get index of outer trace (and its cellgroup)
      const TraceInfo& outertraceinfo = connectivity_.getTrace(neighbor_group, neighbor_elem, j);

      //canonicalTrace of this neighboring cell for the outer (interior) trace
      CanonicalTraceToCell neighbor_outer_canonicalTrace;

      if (outertraceinfo.type == TraceInfo::Interior)
      {
        bool reverse_trace_flag = false;
        int outer_neighbor_cellgroup = -1;
        int outer_neighbor_cellelem  = -1;

        const XFieldTraceGroupType& outer_tracegrp = global_xfld_.template getInteriorTraceGroup<TopoTrace>(outertraceinfo.group);

        if (outer_tracegrp.getElementLeft(outertraceinfo.elem) == neighbor_elem)
        { //element to left of outer trace is the current "neighbor_elem"
          neighbor_outer_canonicalTrace = outer_tracegrp.getCanonicalTraceLeft(outertraceinfo.elem);
        }
        else
        { //element to left of outer trace is not the current "neighbor_elem"
          neighbor_outer_canonicalTrace = outer_tracegrp.getCanonicalTraceRight(outertraceinfo.elem);

          //Get the cell that is *outside* the outer trace - need this info to reverse the outer trace
          outer_neighbor_cellgroup = outer_tracegrp.getGroupLeft();
          outer_neighbor_cellelem  = outer_tracegrp.getElementLeft(outertraceinfo.elem);

          neighbor_outer_canonicalTrace.orientation = 1;
          reverse_trace_flag = true;
          neighborcell_reversed_traces_.push_back(j);
        }

        //Set DOF associativity
        setInteriorTraceAssociativity(outertraceinfo.group, outertraceinfo.elem, DOFAssoc_Outer_BTrace);

        if (reverse_trace_flag==true)
        {
          //Change nodeDOF ordering of outer-trace to ensure that the neighbor cell is to the left of it
          reverseTrace<TopoTrace,TopoCell>(outertraceinfo.group, outertraceinfo.elem, DOFAssoc_Outer_BTrace,
                                           neighbor_group, neighbor_elem, outer_neighbor_cellgroup, outer_neighbor_cellelem);
        }

      }
      else if (outertraceinfo.type == TraceInfo::Boundary) //We are dealing with a boundaryTrace of this neighboring cell
      {
        const XFieldTraceGroupType& outer_tracegrp = global_xfld_.template getBoundaryTraceGroup<TopoTrace>(outertraceinfo.group);

        //Set DOF associativity
        setBoundaryTraceAssociativity(outer_tracegrp, outertraceinfo.elem, DOFAssoc_Outer_BTrace);

        //canonicalTrace of this neighboring cell for the outer boundary trace
        neighbor_outer_canonicalTrace = outer_tracegrp.getCanonicalTraceLeft(outertraceinfo.elem);
      }
      else if (outertraceinfo.type == TraceInfo::GhostBoundary) //We are dealing with a ghost boundaryTrace of this neighboring cell
      {
        const XFieldTraceGroupType& outer_tracegrp = global_xfld_.template getGhostBoundaryTraceGroup<TopoTrace>(outertraceinfo.group);

        //Set DOF associativity
        setBoundaryTraceAssociativity(outer_tracegrp, outertraceinfo.elem, DOFAssoc_Outer_BTrace);

        //canonicalTrace of this neighboring cell for the outer boundary trace
        neighbor_outer_canonicalTrace = outer_tracegrp.getCanonicalTraceLeft(outertraceinfo.elem);
      }
      else
        SANS_DEVELOPER_EXCEPTION("Unknown outer trace info: %d", outertraceinfo.type);

      //Save this boundary trace's mapping from local to global mesh
//      this->BoundaryTraceMapping_[{OuterBTraceGroup,cnt_outer_Btraces}] = {outer_trace_group,outertraceinfo.elem};
//      this->BoundaryTraceSplitInfo_[{OuterBTraceGroup,cnt_outer_Btraces}] = ElementSplitInfo(ElementSplitFlag::Unsplit);

      //Fill associativity of Outer BoundaryTraceGroup
      fldAssoc_Outer_BTrace->setAssociativity( DOFAssoc_Outer_BTrace, cnt_outer_Btraces);

      //Cell group on left is the neighbor cells' group
      fldAssoc_Outer_BTrace->setGroupLeft(local_neighbor_group);

      //Cell on left is the neighbor cell
      fldAssoc_Outer_BTrace->setElementLeft(local_neighbor_elem, cnt_outer_Btraces);
      fldAssoc_Outer_BTrace->setCanonicalTraceLeft(neighbor_outer_canonicalTrace, cnt_outer_Btraces);

      cnt_outer_Btraces++;
    }
  }

}

//Explicit instantiations
template class XField_Local<PhysD3,TopoD3>;

}
