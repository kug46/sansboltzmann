// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define XFIELD_EDGELOCAL_INSTANTIATE

#include "XFieldLine.h"
#include <iomanip>
#include <algorithm>
#include "XFieldLine.h"
#include "Field/Element/ElementAssociativityNode.h"
#include "Field/Element/ElementAssociativityLine.h"
#include "XField_EdgeLocal_impl.h"


namespace SANS
{

// Specialized Constructor for TopoD1

template< class PhysDim, class TopoDim >
XField_EdgeLocal<PhysDim,TopoDim>::XField_EdgeLocal(mpi::communicator& comm_local,
                                                    const XField_CellToTrace<PhysDim, TopoDim>& connectivity,
                                                    const Field_NodalView& nodalView, std::pair<int,int> nodes,
                                                    const Neighborhood neighborhood, const bool isSplit )
:
  BaseType(comm_local, connectivity, nodalView, neighborhood)
{

  for (int group = 0; group < this->global_xfld_.nCellGroups(); group++)
  {
    if ( this->global_xfld_.getCellGroupBase(group).topoTypeID() == typeid(Line) )
    {
      typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Line> XFieldCellGroupType;
      typedef typename XFieldCellGroupType::BasisType BasisType;

      const XFieldCellGroupType& cellgrp = this->global_xfld_.template getCellGroup<Line>(group);
      const BasisType* cell_basis = cellgrp.basis();
      orderVector_.push_back(cell_basis->order());
    }
  }
  if ( std::adjacent_find( orderVector_.begin(), orderVector_.end(), std::not_equal_to<int>() ) != orderVector_.end() )
    SANS_DEVELOPER_EXCEPTION("Mixed order meshes are unsupported");

  // having now checked all groups are same topology and same order, proceed
  this->basisCategory_ = this->global_xfld_.getCellGroupBase(0).basisCategory();

  // Clear the maps
  NodeDOFMap_.clear();
  EdgeDOFMap_.clear();
  CellDOFMap_.clear();

  // Fill the vector structures and then use them to construct the grid
  if (  this->global_xfld_.getCellGroupBase(0).topoTypeID() == typeid(Line) )
  {
    // GroupElemConstructorVector<Line> localCellGroup0, localCellGroup1;
    // GroupTraceConstructorVector<Node> localITraceGroup0, localITraceGroup0to1, localITraceGroup1;
    // GroupTraceConstructorVector<Node> localBTraceGroup0, localBTraceGroup1, localBTraceOuterGroup1;

    this->p_constructorVector_ = std::make_shared< ConstructorVectorTopo<TopoDim,Line,Node> >();
    ConstructorVectorTopo<TopoDim,Line,Node> *pconstructorVectors
    = static_cast<ConstructorVectorTopo<TopoDim,Line,Node>*>(this->p_constructorVector_.get());


    extractEdgeLocalGrid<Line,Node>( nodes,
                                     *pconstructorVectors );


    if (isSplit)
    {
      split<Line,Node>( *pconstructorVectors );
    }

    auto fixNormal = [](GroupElemConstructorVector<TopoDim,Line>& cGroup,
                        GroupTraceConstructorVector<TopoDim,Node>& tGroup)
    {
      std::array<int,Line::NNode> line;
      std::array<int,Node::NNode> node;
      for (auto it = tGroup.begin(); it != tGroup.end(); ++it)
      {
        int lElem = it->localLRElems.first.elem;

        cGroup[lElem].localAssoc.getNodeGlobalMapping( line.data(), line.size() );
        it->localAssoc.getNodeGlobalMapping( node.data(), node.size() );

        CanonicalTraceToCell canon =  TraceToCellRefCoord<Node,TopoD1,Line>
        ::getCanonicalTrace( node.data(), node.size(), line.data(), line.size() );

        it->localAssoc.setNormalSignL(canon.trace == 0 ? 1 : -1); // if canonical trace 0 +1 normalSign, else -1
      }
      return 1;
    };

    fixNormal( pconstructorVectors->localCellGroup0, pconstructorVectors->localITraceGroup0to1);
    fixNormal( pconstructorVectors->localCellGroup0, pconstructorVectors->localITraceGroup0);
    fixNormal( pconstructorVectors->localCellGroup0, pconstructorVectors->localBTraceGroup0);

    fixNormal( pconstructorVectors->localCellGroup1, pconstructorVectors->localITraceGroup1);
    fixNormal( pconstructorVectors->localCellGroup1, pconstructorVectors->localBTraceGroup1);
    fixNormal( pconstructorVectors->localCellGroup1, pconstructorVectors->localBTraceOuterGroup1);

    BaseType::template allocateMesh<Line,Node>(*pconstructorVectors);

    // build higher order mesh from the mesh if necessary
    if (orderVector_[0] > 1) // Higher order mesh required
    {
      // This copy constructor is doing all the hard work, can't modify *this in place
      XField_EdgeLocal<PhysDim,TopoDim> xfld_local_linear( *this, FieldCopy() );
      this->buildFrom(xfld_local_linear,orderVector_[0]);
    }

    BaseType::template projectGroup( pconstructorVectors->localCellGroup0, 0 );
    BaseType::template projectGroup( pconstructorVectors->localCellGroup1, 1 );

  }
  else
    SANS_DEVELOPER_EXCEPTION("XField_EdgeLocal<PhysDim,TopoD1> - Unsupported Topology");

  // This is checkGrid() but gets removed in release mode!
  this->checkGrid();

  this->computeReSolveGroups(); // compute the re-solve groups

  #if 0
    if (!isSplit)
      std::cout<< "constructing an unsplit grid" << std::endl;
    else
      std::cout<< "constructing a split grid" << std::endl;

    std::cout<< "reSolveCellGroups_ = ";
    for (auto it = reSolveCellGroups_.begin(); it != reSolveCellGroups_.end(); ++it)
      std::cout << *it << ", ";
    std::cout << std::endl;

    std::cout<<"nElem[0] = " << this->getCellGroupBase(0).nElem() <<std::endl;
    if ( reSolveCellGroups_.size() > 1 )
      std::cout<<"nElem[1] = " << this->getCellGroupBase(1).nElem() <<std::endl;

    std::cout<< "reSolveInteriorTraceGroups_ = ";
    for (auto it = reSolveInteriorTraceGroups_.begin(); it != reSolveInteriorTraceGroups_.end(); ++it)
      std::cout << *it << ", ";
    std::cout << std::endl;

    std::cout<< "idxITraceGroup0to1_,idxITraceGroup0_,idxITraceGroup1_ = "
    << idxITraceGroup0to1_ <<" "<< idxITraceGroup0_ <<" " << idxITraceGroup1_ <<std::endl;

    std::cout<< "reSolveBoundaryTraceGroups_ = ";
    for (auto it = reSolveBoundaryTraceGroups_.begin(); it != reSolveBoundaryTraceGroups_.end(); ++it)
      std::cout << *it << ", ";
    std::cout << std::endl;
  #endif
}


template <class PhysDim, class TopoDim>
template <class TopologyCell, class TopologyTrace>
void
XField_EdgeLocal<PhysDim, TopoDim>::split( ConstructorVectorTopo<TopoDim,TopologyCell,TopologyTrace>& cV )
{

  /*
   * Can now do all the operations in here without ever having to reference the global mesh again, all the local Associativity constructors
   * will hold all the necessary information, and the allocate mesh interface largely speaking doesn't care about the splitting.
   */
  typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::BasisType TraceBasisType;


  //Add new nodeDOF which will be added to the edge, currently the new node is -1, this is not good
  std::pair<std::map<int,int>::iterator,bool> map_ret =
      NodeDOFMap_.insert( std::pair<int,int>(-1,cntNodeDOF_) );
  SANS_ASSERT( map_ret.second==true );
  this->newNodeDOFs_.push_back(cntNodeDOF_);
  const int newNode = cntNodeDOF_;

  cntNodeDOF_++; //increment counter, this node will definitely be used

  const int startNode = 0, endNode = 1;

  GroupTraceConstructorVector<TopoDim,TopologyTrace> newAdditionITraceToGroup0;

  std::vector<int> ITraceNodeMap(Node::NNode);

  std::size_t initialSize = cV.localCellGroup0.size(); // This is done so push_back can be used without the loop addressing new elements
  const int nCell = static_cast<int>(cV.localCellGroup0.size());
//  int nITrace0 = static_cast<int>(cV.localITraceGroup0.size());

  // because will need to construct traces that aren't just copies
  const TraceBasisType* trace_basis = TraceBasisType::getBasisFunction(order_, this->basisCategory_);

  // --------------------------------------------- //
  // Cell Group 0
  // --------------------------------------------- //
  for (std::size_t i = 0; i < initialSize; i++)
  {
    ITraceNodeMap[0] = newNode; // the new trace is always the new node

    // extract information to figure out which edge is being split
    int nNodeDOFCell = cV.localCellGroup0[i].localAssoc.nNode();
    std::vector<int> cellNodeMap_0(nNodeDOFCell), cellNodeMap_1(nNodeDOFCell);

    // copy node map for element into array
    cV.localCellGroup0[i].localAssoc.getNodeGlobalMapping( cellNodeMap_0.data(), cellNodeMap_0.size() );

    for (int k = 0; k < nNodeDOFCell; k++)
      cellNodeMap_1[k] = cellNodeMap_0[k]; // copy the node map

    // Extract canonical trace of edge to be split
//    CanonicalTraceToCell ITrace_ToSplit = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyCell>::
//        getCanonicalTrace(splitTraceNodeMap.data(), splitTraceNodeMap.size(), cellNodeMap_0.data(), cellNodeMap_0.size() );

    // Make the new node maps from the current one
    for (int k = 0; k < nNodeDOFCell; k++)
    {
      if ( cellNodeMap_0[k] == startNode) // the 0th node is replaced for the new element
        cellNodeMap_1[k] = newNode;
      if ( cellNodeMap_0[k] == endNode) // the 1st node is replaced for the original element
        cellNodeMap_0[k] = newNode;
    }


    // Set up the new trace connecting the original to the new split element
//    CanonicalTraceToCell ITrace_canonicalL = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyCell>::
//        getCanonicalTrace( ITraceNodeMap.data(), ITraceNodeMap.size(), cellNodeMap_0.data(), cellNodeMap_0.size() );
//
//    CanonicalTraceToCell ITrace_canonicalR = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyCell>::
//        getCanonicalTrace( ITraceNodeMap.data(), ITraceNodeMap.size(), cellNodeMap_1.data(), cellNodeMap_1.size() );

    GroupElemConstructorIndex<TopoDim,TopologyCell> newElement(cV.localCellGroup0[i]); // make a new element based on the current

    const int rank = cV.localCellGroup0[i].localAssoc.rank();
    cV.localCellGroup0[i].localAssoc.setNodeGlobalMapping( cellNodeMap_0.data(), cellNodeMap_0.size() );

    newElement.localAssoc.setRank( rank );
    newElement.localAssoc.setNodeGlobalMapping( cellNodeMap_1.data(), cellNodeMap_1.size() );

    // check if the left or the right hand of the trace being split, this is encoded in orientation
    cV.localCellGroup0[i].elementSplitInfo = ElementSplitInfo( ElementSplitFlag::Split, ElementSplitType::Isotropic, -1, 0 );
    newElement.elementSplitInfo = ElementSplitInfo(         ElementSplitFlag::Split, ElementSplitType::Isotropic, -1, 1 );

    cV.localCellGroup0.push_back(newElement); // add the new element at the end, thus giving it nCell + (int) i for an index

    GroupTraceConstructorIndex<TopoDim,TopologyTrace> newTrace;
    newTrace.group = -1; // group didn't exist
    newTrace.trace = - ((int) i ); // new and indexed by the cell number
    newTrace.localAssoc.resize( trace_basis ); // because there might not be any internal traces presently
    newTrace.localAssoc.setRank( rank );
    newTrace.localAssoc.setNodeGlobalMapping( ITraceNodeMap.data(), ITraceNodeMap.size() );
    newTrace.localLRElems.first = GroupElemIndex( 0, (int) i );
    newTrace.localLRElems.second = GroupElemIndex( 0, nCell + (int) i );
    newTrace.elementSplitInfo = ElementSplitInfo(ElementSplitFlag::New, (int) i);

    newAdditionITraceToGroup0.push_back(newTrace);
  }

  // --------------------------------------------- //
  // Interior trace group interior to group 0
  // --------------------------------------------- //

  // Do nothing. There aren't any additional interior traces interior to an edge in 1D

  // Interior Traces are all known now, can add the new ITraces after the split ones
  cV.localITraceGroup0.insert( cV.localITraceGroup0.end(), newAdditionITraceToGroup0.begin(), newAdditionITraceToGroup0.end() );

  // --------------------------------------------- //
  // Boundary Traces attached to Group 0
  // --------------------------------------------- //

  // Just needs to reconnect. No splitting of traces in 1D

  initialSize = cV.localBTraceGroup0.size();
  for (std::size_t i = 0; i < initialSize; i++)
  {
    int nNodeDOFTrace = cV.localBTraceGroup0[i].localAssoc.nNode();
    std::vector<int> traceNodeMap_0(nNodeDOFTrace);
    cV.localBTraceGroup0[i].localAssoc.getNodeGlobalMapping( traceNodeMap_0.data(), traceNodeMap_0.size() );

    // Check if this trace contains node 0 and node 1 -- namely must be split
    bool attachedToNode0 = std::find(std::begin(traceNodeMap_0),std::end(traceNodeMap_0), startNode) != std::end(traceNodeMap_0);
    bool attachedToNode1 = std::find(std::begin(traceNodeMap_0),std::end(traceNodeMap_0),   endNode) != std::end(traceNodeMap_0);

    if ( !attachedToNode0 && attachedToNode1 )
      // is attached to node 1, but not 0, i.e. cell number requires relabeling
      cV.localBTraceGroup0[i].localLRElems.first.elem += nCell;
    else if ( attachedToNode0 && !attachedToNode1 )
    {
      // do nothing, the cell numbering is the same
    }
    else
      SANS_DEVELOPER_EXCEPTION("XField_EdgeLocal<PhysDim,TopoD2>::split() -- an element in boundary "
          "trace group 0 is attached to neither node 0 nor node 1");
  }


  // --------------------------------------------- //
  // Interior Traces from group 0 to group 1
  // --------------------------------------------- //

  // Need to loop over 0to1 group and reallocate their connections where necessary
  for (std::size_t i = 0; i < cV.localITraceGroup0to1.size(); i++)
  {
    int nNodeDOFTrace = cV.localITraceGroup0to1[i].localAssoc.nNode();
    std::vector<int> traceNodeMap_0(nNodeDOFTrace);
    cV.localITraceGroup0to1[i].localAssoc.getNodeGlobalMapping( traceNodeMap_0.data(), traceNodeMap_0.size() );

    // Check if this trace contains node 0 and node 1 -- namely must be split
    bool attachedToNode0 = std::find(std::begin(traceNodeMap_0),std::end(traceNodeMap_0), startNode) != std::end(traceNodeMap_0);
    bool attachedToNode1 = std::find(std::begin(traceNodeMap_0),std::end(traceNodeMap_0),   endNode) != std::end(traceNodeMap_0);

    if ( !attachedToNode0 && attachedToNode1 )
      cV.localITraceGroup0to1[i].localLRElems.first.elem += nCell;
    else if ( attachedToNode0 && !attachedToNode1 )
    {
      // do nothing
    }
  }

  // --------------------------------------------- //
  // Boundary Traces attached to Group 1
  // --------------------------------------------- //

  // Do nothing. There aren't any additional interior traces interior to an edge in 1D


  // --------------------------------------------- //
  // Outer Boundary Trace Group
  // --------------------------------------------- //

  if (this->neighborhood == Neighborhood::Primal) // Only need to reconnect outer groups if it's a primal mesh
  {
    // Need to loop over 0to1 group and reallocate their connections where necessary
    for (std::size_t i = 0; i < cV.localBTraceOuterGroup1.size(); i++)
    {
      int nNodeDOFTrace = cV.localBTraceOuterGroup1[i].localAssoc.nNode();
      std::vector<int> traceNodeMap_0(nNodeDOFTrace);
      cV.localBTraceOuterGroup1[i].localAssoc.getNodeGlobalMapping( traceNodeMap_0.data(), traceNodeMap_0.size() );

      // Check if this trace contains node 0 and node 1 -- namely must be split
      bool attachedToNode0 = std::find(std::begin(traceNodeMap_0),std::end(traceNodeMap_0), startNode) != std::end(traceNodeMap_0);
      bool attachedToNode1 = std::find(std::begin(traceNodeMap_0),std::end(traceNodeMap_0),   endNode) != std::end(traceNodeMap_0);

      if ( !attachedToNode0 && attachedToNode1 )
        cV.localBTraceOuterGroup1[i].localLRElems.first.elem += nCell;
      else if ( attachedToNode0 && !attachedToNode1 )
      {
        // do nothing
      }
    }

  }


}




//Explicit instantiations
template class XField_EdgeLocal<PhysD1,TopoD1>;
//template class XField_EdgeLocal<PhysD2,TopoD1>;

}
