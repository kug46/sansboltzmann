// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD_LOCAL_NEIGHBORHOODS_H
#define XFIELD_LOCAL_NEIGHBORHOODS_H

namespace SANS
{
  /* Shape of neighborhood
  Primal is elements of the grid
  Dual is elements attached to vertices of element
  Vertex is elements attached to a vertex
  */
  enum class Neighborhood { Dual, Primal, Vertex };

}

#endif // XFIELD_LOCAL_NEIGHBORHOODS_H
