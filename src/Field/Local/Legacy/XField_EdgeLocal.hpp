// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD_EDGELOCAL_H
#define XFIELD_EDGELOCAL_H

#include <vector>
#include <algorithm>

#include "Topology/Dimension.h"

#include "XField_CellToTrace.h"
#include "Field_NodalView.h"
#include "XField.h"
#include "XField_Local_Base.h"
#include "XField_Local_Common.h"
#include "XField_Local_Neighborhoods.h"

// #include "Field/Element/ElementAssociativityNode.h"
// #include "Field/Element/ElementAssociativityLine.h"
// #include "Field/Element/ElementAssociativityVolume.h"
// #include "Field/Element/ElementAssociativitySpacetime.h"

#include "Field/Element/ElementProjection_L2.h"

#include "MPI/communicator_fwd.h"

/*
 * This class constructs a 2D local mesh for a specified cell in the 2D global mesh.
 * The constructed local mesh will have the following properties:
 * - The target cell (or main-cell) will be assigned to cellgroup 0, element 0
 * - All (immediate) neighboring cells of the main-cell, if any, will be assigned to cellgroup 1
 * - The traces (edges) around the main-cell will be oriented so that the main-cell is always to the left.
 * - If the main-cell touches any boundaries of the global mesh, those traces (edges) will be assigned to a boundary-trace group of their own.
 * - Traces (edges) of neighboring cells that do not touch the main-cell (referred to as "outer-boundary-traces") will all be assigned
 *   to a single boundary-trace group. This boundary-trace group goes last.
 */

/*
 * Constructs a 2D mesh from a pair of nodes which are used to identify an edge.
 * The constructed local mesh has the following properties:
 * - The main edge runs from node 1 to node 2
 * - The cells attached to the edge are in group 0
 * - The first set of neighbours of group 0 are group 1
 * - The second set that connects the traces of group 1 is group 2
 */

/* Refer to the local solve documentation in Field/Documentation/Local_Solves/LocalSolves.pdf for more details and figures. */

namespace SANS
{

template <class PhysDim, class TopoDim>
class XField_EdgeLocal : public XField_Local_Common<PhysDim, TopoDim>
{

public:
  typedef XField_Local_Common<PhysDim, TopoDim> BaseType;
  typedef typename Field_NodalView::IndexVector GroupElemVector;

  XField_EdgeLocal(mpi::communicator& comm,
                   const XField_CellToTrace<PhysDim, TopoDim>& connectivity,
                   const Field_NodalView& nodalView, std::pair<int,int> nodes,
                   const Neighborhood neighbor, const bool isSplit = true );

  ~XField_EdgeLocal() {};

  XField_EdgeLocal( const XField_EdgeLocal& xfld, const FieldCopy tag) : BaseType(xfld,tag) {}

  const XField_CellToTrace<PhysDim, TopoDim>& getConnectivity() const { return this->connectivity_; }

  using BaseType::neighborhood;
//
//  const int& getIDX_ITraceGroup0() const { return idxITraceGroup0_; }
//  const int& getIDX_BTraceGroup0() const { return idxBTraceGroup0_; }

  virtual const std::type_info& derivedTypeID() const { return typeid(XField_EdgeLocal); }

  std::vector<int> getLinearCommonNodes() const { return linearCommonNodes_; }
  std::vector<int> getLocalLinearCommonNodes() const { return BaseType::getLocalNodes(linearCommonNodes_); }

protected:
  std::vector<int> linearCommonNodes_;

  using BaseType::reSolveBoundaryTraceGroups_;
  using BaseType::reSolveInteriorTraceGroups_;
  using BaseType::reSolveCellGroups_;

  using BaseType::order_;

  //DOF maps
  using BaseType::NodeDOFMap_;
  using BaseType::EdgeDOFMap_;
  using BaseType::CellDOFMap_;

  using BaseType::orderVector_; // basis order for each cell group

  using BaseType::nCellGroups_;
  using BaseType::nInteriorTraceGroups_;
  using BaseType::nBoundaryTraceGroups_;
  using BaseType::nGhostBoundaryTraceGroups_;

  using BaseType::cntNodeDOF_; //Counters for different DOF types
  using BaseType::offset_nodeDOF_; //Offset for the different DOF indices in the global array

  using BaseType::localToGlobalCellMapping_;
  using BaseType::globalToLocalCellMapping_;

  using BaseType::localToGlobalInteriorTraceMapping_;
  using BaseType::globalToLocalInteriorTraceMapping_;

  using BaseType::localToGlobalBoundaryTraceMapping_;
  using BaseType::globalToLocalBoundaryTraceMapping_;

  // indices for referencing cell and trace groups
  // in order
  using BaseType::idxITraceGroup0to1_;
  using BaseType::idxITraceGroup0_;
  using BaseType::idxITraceGroup1_;

  // first index of the boundary group, i.e. group0 is accessed with (int i = idxBTraceGroup0_; i < idxBTraceGroup1_; i++)
  using BaseType::idxBTraceGroup0_;
  using BaseType::idxBTraceGroup1_;

  using BaseType::p_constructorVector_;

  template <class TopologyCell, class TopologyTrace >
  void extractEdgeLocalGrid( std::pair<int,int> nodes,
                             ConstructorVectorTopo<TopoDim,TopologyCell,TopologyTrace>& constructorVectors );

  // Actually does the work in terms of splitting the grid
  template <class TopologyCell, class TopologyTrace >
  void split( ConstructorVectorTopo<TopoDim,TopologyCell,TopologyTrace>& constructorVectors );

};

}

#endif // XFIELDAREA_EDGELOCAL_H
