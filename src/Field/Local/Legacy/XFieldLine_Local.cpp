// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XFieldLine.h"
#include "XFieldLine_Local.h"

namespace SANS
{

template <class PhysDim>
void
XField_Local<PhysDim, TopoD1>::trackDOFs(int group, int elem)
{
  typedef typename XField<PhysDim, TopoD1>::template FieldCellGroupType<Line> XFieldCellGroupType;

  std::pair<std::map<int,int>::iterator,bool> map_ret; //return value for map inserts

  const XFieldCellGroupType& cellgrp = global_xfld_.template getCellGroup<Line>(group);

  int nNodeDOF = cellgrp.associativity(elem).nNode();
  int nEdgeDOF = cellgrp.associativity(elem).nEdge();

  // node DOF, edge DOF and cell DOF maps
  std::vector<int> nodeMap(nNodeDOF);
  std::vector<int> edgeMap(nEdgeDOF);

  //Save DOF mappings from global mesh to local mesh
  cellgrp.associativity(elem).getNodeGlobalMapping( nodeMap.data(), nodeMap.size() );
  for (int k = 0; k < nNodeDOF; k++)
  {
    map_ret = NodeDOFMap_.insert( std::pair<int,int>(nodeMap[k],cntNodeDOF_) );
    if (map_ret.second==true) cntNodeDOF_++; //increment counter if object was actually added
  }

  cellgrp.associativity(elem).getEdgeGlobalMapping( edgeMap.data(), edgeMap.size() );
  for (int k = 0; k < nEdgeDOF; k++)
  {
    map_ret = EdgeDOFMap_.insert( std::pair<int,int>(edgeMap[k],cntEdgeDOF_) );
    if (map_ret.second==true) cntEdgeDOF_++; //increment counter if object was actually added
  }

}


template <class PhysDim>
void
XField_Local<PhysDim, TopoD1>::extractLocalGrid(int group, int elem)
{

  //Checks
  SANS_ASSERT_MSG( group>=0 && group < global_xfld_.nCellGroups(), "group = %d, nCellGroups() = %d ", group, global_xfld_.nCellGroups());

  SANS_ASSERT_MSG( elem>=0 && elem < global_xfld_.getCellGroupBase(group).nElem(),
                  "cellElem=%d, getCellGroupBase(group).nElem()=%d ",elem,global_xfld_.getCellGroupBase(group).nElem());

  //Clear current local grid
  this->deallocate();

  NodeDOFMap_.clear();
  EdgeDOFMap_.clear();
  CellDOFMap_.clear();

  cntNodeDOF_ = 0;
  cntEdgeDOF_ = 0;
  cntCellDOF_ = 0;

  OuterBTraceGroup_ = -1;

  if ( global_xfld_.getCellGroupBase(group).topoTypeID() == typeid(Line) )
  {
    extractMainCell(group, elem);
  }
  else
    SANS_DEVELOPER_EXCEPTION( "XField_Local<PhysDim, TopoD1, T>::extractLocalGrid - Unknown cell topology." );

}


template <class PhysDim>
void
XField_Local<PhysDim, TopoD1>::extractMainCell(int main_group, int main_elem)
{

  typedef typename XField<PhysDim, TopoD1>::template FieldCellGroupType<Line> XFieldCellGroupType;
  typedef typename XField<PhysDim, TopoD1>::template FieldTraceGroupType<Node> XFieldTraceGroupType;
  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

  const XFieldCellGroupType& cellgrp_main = global_xfld_.template getCellGroup<Line>(main_group);

  const BasisFunctionLineBase* cell_basis = cellgrp_main.basis();
  int order = cell_basis->order();

  nNeighbors_ = 0;

  //Count and map required DOFs from global to local mesh
  trackDOFs(main_group, main_elem);

  //Initial loop across neighboring elements to find number of cells/traces/DOFs
  for (int i = 0; i < Line::NTrace; i++)
  {
    //Get indices of each neighboring cell (and its cellgroup)
    const TraceInfo& traceinfo = connectivity_.getTrace(main_group, main_elem, i);

    if (traceinfo.type == TraceInfo::Interior)
    {
      const XFieldTraceGroupType& tracegrp = global_xfld_.template getInteriorTraceGroup<Node>(traceinfo.group);
      int neighbor_group = -1; //cellgroup of neighboring element
      int neighbor_elem = -1; //element index of neighboring element

      if (tracegrp.getElementLeft(traceinfo.elem) != main_elem)
      {
        neighbor_group = tracegrp.getGroupLeft(); //getGroupLeft gives the neighbor's cellgroup
        neighbor_elem = tracegrp.getElementLeft(traceinfo.elem);
      }
      else
      {
        neighbor_group = tracegrp.getGroupRight(); //getGroupRight gives the neighbor's cellgroup
        neighbor_elem = tracegrp.getElementRight(traceinfo.elem);
      }

      if ( global_xfld_.getCellGroupBase(neighbor_group).topoTypeID() == typeid(Line) )
      {
        //Count and map required DOFs from global to local mesh
        trackDOFs(neighbor_group, neighbor_elem);
        nNeighbors_++; //Increment neighbor count
      }
      else
        SANS_DEVELOPER_EXCEPTION( "XField_Local<PhysDim, TopoD1, T>::extractLocalGrid - Unknown cell topology for neighbor." );
    }
  } // initial loop across traces of main cell


  //Count DOFs in local mesh
  int nDOF_local = (int)CellDOFMap_.size() + (int)EdgeDOFMap_.size() + (int)NodeDOFMap_.size();

#if 0
  std::cout << "nDOF_local: " << nDOF_local << std::endl;
  std::cout << "mapNode: " << NodeDOFMap_.size() <<std::endl;
  std::cout << "mapEdge: " << EdgeDOFMap_.size() <<std::endl;
  std::cout << "mapCell: " << CellDOFMap_.size() <<std::endl;
#endif

  //Create local XField
  this->resizeDOF(nDOF_local);

  if (nNeighbors_>0)
  {
    this->resizeCellGroups(2); //2 CellGroups - one for main cell, and one for all neighboring cells
    this->resizeInteriorTraceGroups(1); //one group for all interior traces
  }
  else //No neighboring cells
  {
    this->resizeCellGroups(1); // one group for main cell
    this->resizeInteriorTraceGroups(0);
  }

  //a group each for each BC of main cell + one group for outer boundaries of neighboring cells
  nMainBTraceGroups_ = Line::NTrace - nNeighbors_;
  nOuterBTraceGroups_ = 1;
  if (nNeighbors_ == 0) nOuterBTraceGroups_ = 0; //If there are no neighbors - there is no outer boundary of the neighboring cells
  this->resizeBoundaryTraceGroups(nMainBTraceGroups_);
  this->resizeGhostBoundaryTraceGroups(nOuterBTraceGroups_);

  if (nOuterBTraceGroups_ > 0) OuterBTraceGroup_ = 0;

  //Zero out mesh DOFs
  for (int k = 0; k < this->nDOF_; k++)
    this->DOF_[k] = 0.0;

  //Offsets for cell, edge and node DOFs in the global array (cell DOFs first, edge DOFs second, node DOFs last)
  offset_cellDOF_ = 0;
  offset_edgeDOF_ = (int) CellDOFMap_.size();
  offset_nodeDOF_ = (int) CellDOFMap_.size() + (int) EdgeDOFMap_.size();


  //------------------------------Fill in associativity for main cell--------------------------------------------

  // create field associativity constructor for main cell
  typename XFieldCellGroupType::FieldAssociativityConstructorType fldAssocCell_Main( cell_basis, 1 ); //1 cell for main cell group

  ElementAssociativityConstructor<TopoD1, Line> DOFAssoc_MainCell( cell_basis );

  setCellAssociativity(main_group, main_elem, DOFAssoc_MainCell);
  fldAssocCell_Main.setAssociativity( DOFAssoc_MainCell, 0 );

  //Add cell group and copy DOFs for main cell
  this->cellGroups_[0] = new XFieldCellGroupType(fldAssocCell_Main);
  this->cellGroups_[0]->setDOF( this->DOF_, this->nDOF_ );
  this->nElem_ += fldAssocCell_Main.nElem();

  ElementXFieldClass xfldElem_main( cell_basis );
  cellgrp_main.getElement(xfldElem_main, main_elem); //Get current (main) element
  this->template getCellGroup<Line>(0).setElement(xfldElem_main,0);

  //Save main cell's mapping to global mesh
  this->CellMapping_[{0,0}] = {main_group,main_elem};
  this->CellSplitInfo_[{0,0}] = ElementSplitInfo(ElementSplitFlag::Unsplit);

  //-------------------------------------------------------------------------------------------------------------

  //Copy and fill associativities of neighboring cells, interior traces and boundary traces
  extractNeighbors(main_group, main_elem, Line::NTrace, order);
}


template <class PhysDim>
void
XField_Local<PhysDim, TopoD1>::extractNeighbors(int main_group, int main_elem, int Ntrace, int order)
{

  typedef typename XField<PhysDim, TopoD1>::template FieldCellGroupType<Line> XFieldCellGroupType;
  typedef typename XField<PhysDim, TopoD1>::template FieldTraceGroupType<Node> XFieldTraceGroupType;
  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

  const BasisFunctionLineBase* cell_basis = NULL;

  if (nNeighbors_>0)
  {
    const XFieldCellGroupType& cellgrp = global_xfld_.template getCellGroup<Line>(main_group);
    cell_basis = cellgrp.basis();
  }

  const BasisFunctionNodeBase* trace_basis = BasisFunctionNodeBase::getBasisFunction(0, BasisFunctionCategory_Legendre);


  // create field associativity constructor for neighboring cells
  std::shared_ptr<typename XFieldCellGroupType::FieldAssociativityConstructorType> fldAssocCell_Neighbors;

  // create field associativity constructor for interior traces
  std::shared_ptr<typename XFieldTraceGroupType::FieldAssociativityConstructorType> fldAssoc_ITrace;

  // create field associativity constructor for boundary traces of neighboring cells (1 edge for each neighboring line)
  std::shared_ptr<typename XFieldTraceGroupType::FieldAssociativityConstructorType> fldAssoc_Outer_BTrace;

  //Assign cell group and trace group indices to neighbors (index 0 is for main cell's group)
  int CellgrpNeighbors = 1;
  int ITracegrpNeighbors = 0;

  if (nNeighbors_ > 0)
  {
    fldAssocCell_Neighbors = std::make_shared< typename XFieldCellGroupType::FieldAssociativityConstructorType>
                                             ( cell_basis, nNeighbors_ );

    fldAssoc_ITrace        = std::make_shared< typename XFieldTraceGroupType::FieldAssociativityConstructorType>
                                             ( trace_basis, nNeighbors_);

    fldAssoc_Outer_BTrace  = std::make_shared< typename XFieldTraceGroupType::FieldAssociativityConstructorType>
                                             ( trace_basis, 1*nNeighbors_);
  }


  int cnt_neighbor = 0; //counter to keep track of lines whose associativity has been processed
  int cnt_main_Btraces = 0; //counter to keep track of the boundary traces of the main cell which have been processed

  int cnt_outer_Btraces = 0; //counter to keep track of the outer boundary traces which have been processed

  std::vector<ElementXFieldClass> neighbor_list;

  //-----------Fill in associativity for neighboring cells--------------------

  //Repeat loop across neighboring elements to fill associativity
  for (int itrace = 0; itrace < Ntrace; itrace++)
  {
    //Get indices of each neighboring trace (and its cellgroup)
    const TraceInfo& traceinfo = connectivity_.getTrace(main_group, main_elem, itrace);

    if (traceinfo.type == TraceInfo::Interior)
    {
      const XFieldTraceGroupType& tracegrp = global_xfld_.template getInteriorTraceGroup<Node>(traceinfo.group);

      int neighbor_group = -1; //cellgroup of neighboring element in global mesh
      int neighbor_elem = -1; //element index of neighboring element in global mesh
      CanonicalTraceToCell neighbor_canonicalTrace; //canonicalTrace of the neighboring cell for the current trace
      CanonicalTraceToCell main_canonicalTrace; //canonicalTrace of the main cell for the current trace
      bool reverse_normal_flag = false; //flag to indicate if the trace normal should be reversed

      if (tracegrp.getElementLeft(traceinfo.elem) == main_elem)
      { //element to left of trace is the "main_elem" => getElementRight gives the neighboring cell
        neighbor_group          = tracegrp.getGroupRight();
        neighbor_elem           = tracegrp.getElementRight(traceinfo.elem);
        neighbor_canonicalTrace = tracegrp.getCanonicalTraceRight(traceinfo.elem);
        main_canonicalTrace     = tracegrp.getCanonicalTraceLeft(traceinfo.elem);
      }
      else
      { //element to left of trace is not the "main_elem" => getElementLeft gives the neighboring cell
        neighbor_group          = tracegrp.getGroupLeft();
        neighbor_elem           = tracegrp.getElementLeft(traceinfo.elem);
        neighbor_canonicalTrace = tracegrp.getCanonicalTraceLeft(traceinfo.elem);
        main_canonicalTrace     = tracegrp.getCanonicalTraceRight(traceinfo.elem);
        reverse_normal_flag     = true;
      }

      if ( global_xfld_.getCellGroupBase(neighbor_group).topoTypeID() == typeid(Line) )
      {

        const XFieldCellGroupType& cellgrp_neighbor = global_xfld_.template getCellGroup<Line>(neighbor_group);
        ElementXFieldClass xfldElem_neighbor(cell_basis);
        cellgrp_neighbor.getElement(xfldElem_neighbor,neighbor_elem); //Get neighboring cell
        neighbor_list.push_back(xfldElem_neighbor);

        // DOF associativity for neighbor cells
        ElementAssociativityConstructor<TopoD1, Line> DOFAssoc_NeighborCell( cell_basis );

        setCellAssociativity(neighbor_group, neighbor_elem, DOFAssoc_NeighborCell);
        fldAssocCell_Neighbors->setAssociativity( DOFAssoc_NeighborCell, cnt_neighbor);

        //Save this neighboring cell's mapping from local to global mesh
        this->CellMapping_[{CellgrpNeighbors,cnt_neighbor}] = {neighbor_group,neighbor_elem};
        this->CellSplitInfo_[{CellgrpNeighbors,cnt_neighbor}] = ElementSplitInfo(ElementSplitFlag::Unsplit);

        // DOF associativity for boundary traces of neighbors
        ElementAssociativityConstructor<TopoD0, Node> DOFAssoc_Outer_BTrace( trace_basis );

        for (int j = 0; j < Line::NTrace; j++) //loop across traces of this neighboring element
        {
          if (j != neighbor_canonicalTrace.trace) //Outer boundary trace
          {
            //Get index of outer trace (and its cellgroup)
            const TraceInfo& outertraceinfo = connectivity_.getTrace(neighbor_group, neighbor_elem, j);

            //canonicalTrace of this neighboring cell for the outer (interior) trace
            CanonicalTraceToCell neighbor_outer_canonicalTrace;

            if (outertraceinfo.type == TraceInfo::Interior)
            {
              //Set DOF associativity
              setInteriorTraceAssociativity(outertraceinfo.group, outertraceinfo.elem, DOFAssoc_Outer_BTrace);

              const XFieldTraceGroupType& outer_tracegrp = global_xfld_.template getInteriorTraceGroup<Node>(outertraceinfo.group);

              if (outer_tracegrp.getElementLeft(outertraceinfo.elem) == neighbor_elem)
              { //element to left of outer trace is the current "neighbor_elem"
                neighbor_outer_canonicalTrace = outer_tracegrp.getCanonicalTraceLeft(outertraceinfo.elem);
              }
              else
              { //element to left of outer trace is not the current "neighbor_elem"
                neighbor_outer_canonicalTrace = outer_tracegrp.getCanonicalTraceRight(outertraceinfo.elem);

                //need to reverse normal
                DOFAssoc_Outer_BTrace.setNormalSignL(-1);
              }
            }
            else if (outertraceinfo.type == TraceInfo::Boundary) //We are dealing with a boundaryTrace of this neighboring cell
            {
              //Set DOF associativity
              setBoundaryTraceAssociativity(outertraceinfo.group, outertraceinfo.elem, DOFAssoc_Outer_BTrace);

              const XFieldTraceGroupType& outer_tracegrp = global_xfld_.template getBoundaryTraceGroup<Node>(outertraceinfo.group);

              //canonicalTrace of this neighboring cell for the outer boundary trace
              neighbor_outer_canonicalTrace = outer_tracegrp.getCanonicalTraceLeft(outertraceinfo.elem);
            }

            //Save this boundary trace's mapping from local to global mesh
//            this->BoundaryTraceMapping_[{OuterBTraceGroup_,cnt_outer_Btraces}] = {outer_trace_group,outer_trace_elem};
//            this->BoundaryTraceSplitInfo_[{OuterBTraceGroup_,cnt_outer_Btraces}] = ElementSplitInfo(ElementSplitFlag::Unsplit);

            //Fill associativity of Outer BoundaryTraceGroup
            fldAssoc_Outer_BTrace->setAssociativity( DOFAssoc_Outer_BTrace, cnt_outer_Btraces);

            fldAssoc_Outer_BTrace->setGroupLeft(CellgrpNeighbors); //Cell group on left is the triangle neighbor cells' group
            fldAssoc_Outer_BTrace->setElementLeft(cnt_neighbor, cnt_outer_Btraces); //Cell on left is the neighbor cell
            fldAssoc_Outer_BTrace->setCanonicalTraceLeft(neighbor_outer_canonicalTrace, cnt_outer_Btraces);

            cnt_outer_Btraces++;
          }
        }

        // DOF associativity for interior trace
        ElementAssociativityConstructor<TopoD0, Node> DOFAssoc_ITrace( trace_basis );

        //Set DOF associativity
        setInteriorTraceAssociativity(traceinfo.group, traceinfo.elem, DOFAssoc_ITrace);

        //Need to reverse normals to ensure that the main cell is always to the left of its neighbors
        if (reverse_normal_flag==true)
        {
          DOFAssoc_ITrace.setNormalSignL(-1);
        }

        int itrace_index = cnt_neighbor; //The new index for this interior trace is simply the number of neighboring cells passed

        fldAssoc_ITrace->setAssociativity( DOFAssoc_ITrace, itrace_index );

        //Save this interior trace's mapping from local to global mesh
        this->InteriorTraceMapping_[{ITracegrpNeighbors,itrace_index}] = {traceinfo.group, traceinfo.elem};
        this->InteriorTraceSplitInfo_[{ITracegrpNeighbors,itrace_index}] = ElementSplitInfo(ElementSplitFlag::Unsplit);

        fldAssoc_ITrace->setGroupLeft(0); //Cell group on left is the main cell's group = 0
        fldAssoc_ITrace->setElementLeft(0, itrace_index);
        fldAssoc_ITrace->setCanonicalTraceLeft(main_canonicalTrace, itrace_index);

        fldAssoc_ITrace->setGroupRight(CellgrpNeighbors); //Cell group on right is the triangle neighbors' cell group
        fldAssoc_ITrace->setElementRight(itrace_index, itrace_index); //Right element for interior trace i is equal to i
        fldAssoc_ITrace->setCanonicalTraceRight(neighbor_canonicalTrace, itrace_index);

        cnt_neighbor++;
      }
      else
        SANS_DEVELOPER_EXCEPTION( "XField_Local<PhysDim, TopoD1, T>::extractLocalGrid - Unknown cell topology for neighbor." );

    }
    else if (traceinfo.type == TraceInfo::Boundary) //We are dealing with a boundary trace of the main cell
    {
      const XFieldTraceGroupType& tracegrp = global_xfld_.template getBoundaryTraceGroup<Node>(traceinfo.group);

      //canonicalTrace of the main cell for the current trace
      CanonicalTraceToCell main_canonicalTrace = tracegrp.getCanonicalTraceLeft(traceinfo.elem);

      // DOF associativity for interior trace
      ElementAssociativityConstructor<TopoD0, Node> DOFAssoc_BTrace( trace_basis );

      setBoundaryTraceAssociativity(traceinfo.group, traceinfo.elem, DOFAssoc_BTrace);

      // create field associativity constructor for each boundary trace of main cell
      // Note: each boundary trace of the main cell has its own boundaryTraceGroup!
      typename XFieldTraceGroupType::FieldAssociativityConstructorType fldAssoc_Main_BTrace( trace_basis, 1);

      fldAssoc_Main_BTrace.setAssociativity( DOFAssoc_BTrace, 0);

      //Save this boundary trace's mapping from local to global mesh
      this->BoundaryTraceMapping_[{cnt_main_Btraces,0}] = {traceinfo.group, traceinfo.elem};
      this->BoundaryTraceSplitInfo_[{cnt_main_Btraces,0}] = ElementSplitInfo(ElementSplitFlag::Unsplit);

      fldAssoc_Main_BTrace.setGroupLeft(0); //Cell group on left is the main cell's group = 0
      fldAssoc_Main_BTrace.setElementLeft(0, 0);
      fldAssoc_Main_BTrace.setCanonicalTraceLeft(main_canonicalTrace, 0);

      //Add trace group and copy DOFs for boundary traces of main cell
      this->boundaryTraceGroups_[cnt_main_Btraces] = new XFieldTraceGroupType( fldAssoc_Main_BTrace );
      this->boundaryTraceGroups_[cnt_main_Btraces]->setDOF( this->DOF_, this->nDOF_ );
      cnt_main_Btraces++;
    }
  } //repeated loop across traces of main element

  if (nNeighbors_>0)
  {
    //Add cell group and copy DOFs for neighboring cells
    this->cellGroups_[CellgrpNeighbors] = new XFieldCellGroupType( *fldAssocCell_Neighbors );
    this->cellGroups_[CellgrpNeighbors]->setDOF( this->DOF_, this->nDOF_ );
    this->nElem_ += fldAssocCell_Neighbors->nElem();
    for (int k=0; k<nNeighbors_; k++)
      this->template getCellGroup<Line>(CellgrpNeighbors).setElement(neighbor_list[k],k);

    //Add trace group and set DOFs for interior traces
    this->interiorTraceGroups_[ITracegrpNeighbors] = new XFieldTraceGroupType( *fldAssoc_ITrace );
    this->interiorTraceGroups_[ITracegrpNeighbors]->setDOF( this->DOF_, this->nDOF_ );

    if (nOuterBTraceGroups_>0)
    {
      //Add trace group and copy DOFs for interior traces
      this->ghostBoundaryTraceGroups_[OuterBTraceGroup_] = new XFieldTraceGroupType( *fldAssoc_Outer_BTrace );
      this->ghostBoundaryTraceGroups_[OuterBTraceGroup_]->setDOF( this->DOF_, this->nDOF_ );
    }
  }

}

template <class PhysDim>
void
XField_Local<PhysDim, TopoD1>::setCellAssociativity(int group, int elem, ElementAssociativityConstructor<TopoD1, Line>& DOFAssoc)
{

  typedef typename XField<PhysDim, TopoD1>::template FieldCellGroupType<Line> XFieldCellGroupType;

  //Get global cell group for main cell
  const XFieldCellGroupType& cellgrp = global_xfld_.template getCellGroup<Line>(group);

  int nNodeDOF = cellgrp.associativity(elem).nNode();
  int nEdgeDOF = cellgrp.associativity(elem).nEdge();

  // node DOF and cell DOF ordering for cell
  std::vector<int> Cell_nodeDOF_map(nNodeDOF);
  std::vector<int> Cell_edgeDOF_map(nEdgeDOF);

  cellgrp.associativity(elem).getNodeGlobalMapping( Cell_nodeDOF_map.data(), Cell_nodeDOF_map.size() ); //Get nodeDOF map from global field
  cellgrp.associativity(elem).getEdgeGlobalMapping( Cell_edgeDOF_map.data(), Cell_edgeDOF_map.size() ); //Get edgeDOF map from global field

  //Map nodeDOFs from global to local mesh
  for (int k = 0; k < nNodeDOF; k++)
    Cell_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Cell_nodeDOF_map[k]);

  for (int k = 0; k < nEdgeDOF; k++)
    Cell_edgeDOF_map[k] = offset_edgeDOF_ + EdgeDOFMap_.at(Cell_edgeDOF_map[k]);

  DOFAssoc.setRank( cellgrp.associativity(elem).rank() );
  DOFAssoc.setNodeGlobalMapping( Cell_nodeDOF_map );
  DOFAssoc.setEdgeGlobalMapping( Cell_edgeDOF_map );

}

template<class PhysDim>
void
XField_Local<PhysDim, TopoD1>::setInteriorTraceAssociativity(int group, int elem, ElementAssociativityConstructor<TopoD0, Node>& DOFAssoc)
{

  typedef typename XField<PhysDim, TopoD1>::template FieldTraceGroupType<Node> XFieldTraceGroupType;

  //Get global cell group
  const XFieldTraceGroupType& tracegrp = global_xfld_.template getInteriorTraceGroup<Node>(group);

  int nNodeDOF = tracegrp.associativity(elem).nNode();

  // node DOF ordering for interior trace
  std::vector<int> Cell_nodeDOF_map(nNodeDOF);

  tracegrp.associativity(elem).getNodeGlobalMapping( Cell_nodeDOF_map.data(), Cell_nodeDOF_map.size() ); //Get nodeDOF map from global field

  //Map nodeDOFs from global to local mesh
  for (int k = 0; k < nNodeDOF; k++)
    Cell_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Cell_nodeDOF_map[k]);

  DOFAssoc.setRank( tracegrp.associativity(elem).rank() );
  DOFAssoc.setNodeGlobalMapping( Cell_nodeDOF_map );

  DOFAssoc.setNormalSignL(tracegrp.associativity(elem).normalSignL());
}


template<class PhysDim>
void
XField_Local<PhysDim, TopoD1>::setBoundaryTraceAssociativity(int group, int elem, ElementAssociativityConstructor<TopoD0, Node>& DOFAssoc)
{

  typedef typename XField<PhysDim, TopoD1>::template FieldTraceGroupType<Node> XFieldTraceGroupType;

  //Get global cell group
  const XFieldTraceGroupType& tracegrp = global_xfld_.template getBoundaryTraceGroup<Node>(group);

  int nNodeDOF = tracegrp.associativity(elem).nNode();

  // node DOF ordering for boundary trace
  std::vector<int> Cell_nodeDOF_map(nNodeDOF);

  tracegrp.associativity(elem).getNodeGlobalMapping( Cell_nodeDOF_map.data(), Cell_nodeDOF_map.size() ); //Get nodeDOF map from global field

  //Map nodeDOFs from global to local mesh
  for (int k = 0; k < nNodeDOF; k++)
    Cell_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Cell_nodeDOF_map[k]);

  DOFAssoc.setRank( tracegrp.associativity(elem).rank() );
  DOFAssoc.setNodeGlobalMapping( Cell_nodeDOF_map );

  DOFAssoc.setNormalSignL(tracegrp.associativity(elem).normalSignL());
}

//Explicit instantiations
template class XField_Local<PhysD1,TopoD1>;
template class XField_Local<PhysD2,TopoD1>;

}
