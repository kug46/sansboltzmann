// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define XFIELD_EDGELOCAL_INSTANTIATE

#include "XFieldArea.h"
#include <iomanip>
#include <algorithm>
#include "XFieldArea.h"
#include "Field/Element/ElementAssociativityArea.h"
#include "Field/Element/ElementAssociativityLine.h"
#include "XField_EdgeLocal_impl.h"
// #include "XFieldLine.h"

namespace SANS
{

// Specialized Constructor for TopoD2
// Edge focused constructor - operates with neighbourhoods that are based directly on the edge, rather than an attached element
template< class PhysDim, class TopoDim >
XField_EdgeLocal<PhysDim,TopoDim>::XField_EdgeLocal( mpi::communicator& comm_local,
                                                     const XField_CellToTrace<PhysDim, TopoDim>& connectivity,
                                                     const Field_NodalView& nodalView, std::pair<int,int> nodes,
                                                     const Neighborhood neighborhood, const bool isSplit )
:
  BaseType(comm_local, connectivity, nodalView, neighborhood)
{
  for (int group = 0; group < this->global_xfld_.nCellGroups(); group++)
  {
    if ( this->global_xfld_.getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Triangle> XFieldCellGroupType;
      typedef typename XFieldCellGroupType::BasisType BasisType;

      const XFieldCellGroupType& cellgrp = this->global_xfld_.template getCellGroup<Triangle>(group);
      const BasisType* cell_basis = cellgrp.basis();
      orderVector_.push_back(cell_basis->order());
    }
    else if ( this->global_xfld_.getCellGroupBase(group).topoTypeID() == typeid(Quad) )
    SANS_DEVELOPER_EXCEPTION("Quads unsupported");
  }
  if ( std::adjacent_find( orderVector_.begin(), orderVector_.end(), std::not_equal_to<int>() ) != orderVector_.end() )
    SANS_DEVELOPER_EXCEPTION("Mixed order meshes are unsupported");


  // having now checked all groups are same topology and same order, proceed
  this->basisCategory_ = this->global_xfld_.getCellGroupBase(0).basisCategory();

  // Clear the maps
  NodeDOFMap_.clear();
  EdgeDOFMap_.clear();
  CellDOFMap_.clear();

  // Fill the vector structures and then use them to construct the grid
  if (  this->global_xfld_.getCellGroupBase(0).topoTypeID() == typeid(Triangle) )
  {
    this->p_constructorVector_ = std::make_shared< ConstructorVectorTopo<TopoDim,Triangle,Line> >();

    ConstructorVectorTopo<TopoDim,Triangle,Line> *pconstructorVectors
    = static_cast<ConstructorVectorTopo<TopoDim,Triangle,Line>*>(this->p_constructorVector_.get());

    extractEdgeLocalGrid<Triangle,Line>( nodes, *pconstructorVectors );

    if (isSplit)
      split<Triangle,Line>( *pconstructorVectors );

    BaseType::template allocateMesh<Triangle,Line>(*pconstructorVectors);

    // build higher order mesh from the mesh if necessary
    if (orderVector_[0] > 1) // Higher order mesh required
    {
      // This copy constructor is doing all the hard work, can't modify *this in place
      XField_EdgeLocal<PhysDim,TopoDim> xfld_local_linear( *this, FieldCopy() );
      this->buildFrom(xfld_local_linear,orderVector_[0]);
    }

    BaseType::template projectGroup( pconstructorVectors->localCellGroup0, 0 );
    BaseType::template projectGroup( pconstructorVectors->localCellGroup1, 1 );

  }
  else if (  this->global_xfld_.getCellGroupBase(0).topoTypeID() == typeid(Quad) )
  {
    SANS_DEVELOPER_EXCEPTION("Quads Unsupported");
  }

  // This is checkGrid() but gets removed in release mode!
  this->checkGrid();

  this->computeReSolveGroups(); // compute the re-solve groups

#if 0
  if (!isSplit)
    std::cout<< "constructing an unsplit grid" << std::endl;
  else
    std::cout<< "constructing a split grid" << std::endl;

  std::cout<< "reSolveCellGroups_ = ";
  for (auto it = reSolveCellGroups_.begin(); it != reSolveCellGroups_.end(); ++it)
    std::cout << *it << ", ";
  std::cout << std::endl;

  std::cout<< "reSolveInteriorTraceGroups_ = ";
  for (auto it = reSolveInteriorTraceGroups_.begin(); it != reSolveInteriorTraceGroups_.end(); ++it)
    std::cout << *it << ", ";
  std::cout << std::endl;

  std::cout<< "reSolveBoundaryTraceGroups_ = ";
  for (auto it = reSolveBoundaryTraceGroups_.begin(); it != reSolveBoundaryTraceGroups_.end(); ++it)
    std::cout << *it << ", ";
  std::cout << std::endl;
#endif

  // this->resizeGhostBoundaryTraceGroups(nGhostBoundaryTraceGroups_);
}


// THIS IS TOPOD2 specific
template <class PhysDim, class TopoDim>
template <class TopologyCell, class TopologyTrace>
void
XField_EdgeLocal<PhysDim, TopoDim>::split( ConstructorVectorTopo<TopoDim,TopologyCell,TopologyTrace>& cV )
{
  const int startNode = 0, endNode = 1;

  //Add new nodeDOF which will be added to the edge, currently the new node is -1, this is not good
  std::pair<std::map<int,int>::iterator,bool> map_ret =
       NodeDOFMap_.insert( std::pair<int,int>(-1,cntNodeDOF_) );
  SANS_ASSERT( map_ret.second==true );
  this->newNodeDOFs_.push_back(cntNodeDOF_);
  const int newNode = cntNodeDOF_ ;

  cntNodeDOF_++; //increment counter, this node will definitely be used

  // This actually does the splitting
  BaseType::split( startNode, endNode, newNode, cV );
}



//Explicit instantiations
template class XField_EdgeLocal<PhysD2,TopoD2>;
//template class XField_EdgeLocal<PhysD3,TopoD2>;

}
