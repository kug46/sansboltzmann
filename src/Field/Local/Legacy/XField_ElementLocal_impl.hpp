// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#if !defined(XFIELD_ELEMENTLOCAL_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "XFieldLine.h"
#include "XFieldArea.h"
#include "XFieldVolume.h"
#include <iomanip>
#include <algorithm>
#include "XField_ElementLocal.h"


namespace SANS
{

template <class PhysDim, class TopoDim>
template <class TopologyCell, class TopologyTrace>
void
XField_ElementLocal<PhysDim, TopoDim>::extractLocalGrid( const int group, const int elem,
                                                         ConstructorVectorTopo<TopoDim,TopologyCell,TopologyTrace>& cV )
{
  //Checks
  SANS_ASSERT_MSG( group>=0 && group < this->global_xfld_.nCellGroups(),
                  "group = %d, nCellGroups() = %d ", group, this->global_xfld_.nCellGroups());

  SANS_ASSERT_MSG( elem>=0 && elem < this->global_xfld_.getCellGroupBase(group).nElem(),
                  "cellElem=%d, getCellGroupBase(group).nElem()=%d ",elem, this->global_xfld_.getCellGroupBase(group).nElem());

  // Track dofs of the element to be split
  BaseType::template trackDOFs<TopologyCell>(group, elem);

  cV.localCellGroup0.clear();

  GroupElemConstructorIndex<TopoDim,TopologyCell> tmpElem(group,elem);
  tmpElem.pGlobalAssoc = &(this->global_xfld_.template getCellGroup<TopologyCell>(group).associativity(elem));

  cV.localCellGroup0.push_back( tmpElem );

  std::array<int,TopologyCell::NNode> elemNodes;
  tmpElem.pGlobalAssoc->getNodeGlobalMapping(elemNodes.data(),elemNodes.size());

  // Only one element in group 0

  cV.localCellGroup1.clear();

  if (  this->neighborhood == Neighborhood::Dual )
  {
    std::set<GroupElemIndex> superSet;
    for (std::size_t i = 0; i < TopologyCell::NNode; i++)
    {
      // cells attached to node i
      GroupElemVector cellSet(this->nodalView_.getCellList(elemNodes[i]));

      // copy into super set
      superSet.insert( cellSet.begin(), cellSet.end() );
    }

    // Those elements in superSet, but not localCellGroup0 go into localCellGroup1
    std::set_difference( superSet.begin(),         superSet.end(),
                         cV.localCellGroup0.begin(), cV.localCellGroup0.end(),
                         std::back_inserter(cV.localCellGroup1) );
  }
  else if ( this->neighborhood == Neighborhood::Primal )
  {
    // printf("[warning] primal neighbourhood under development...\n");

    std::set<GroupElemIndex> cellSet_1;
    for (int i = 0; i < TopologyCell::NNode; i++)
    {
      GroupElemVector cellSet(this->nodalView_.getCellList(elemNodes[i]) );
      for (int j = i+1; j < TopologyCell::NNode; j++)
      {
        GroupElemVector cellSet_2( this->nodalView_.getCellList(elemNodes[j]) );

        std::vector<GroupElemIndex> intersect; // from std::set<GroupElemIndex>
        std::set_intersection( cellSet.begin(), cellSet.end(),
                               cellSet_2.begin(), cellSet_2.end(),
                               std::back_inserter(intersect) ); // from std::inserter(intersect)

        for ( auto it = intersect.begin(); it != intersect.end(); ++it)
        {
          cellSet_1.insert( *it );
        }
      }
    }

     // Those elements in the intersection that aren't in group 0, i.e. all primal neighbours
     std::set_difference( cellSet_1.begin(),       cellSet_1.end(),
                          cV.localCellGroup0.begin(), cV.localCellGroup0.end(),
                          std::back_inserter(cV.localCellGroup1) );
    // for (auto it = cV.localCellGroup1.begin();it!=cV.localCellGroup1.end();++it)
    // {
    //   printf("cell group 1: group = %d, elem = %d\n",it->group,it->elem);
    //   this->global_xfld_.template getCellGroup<TopologyCell>(it->group).associativity(it->elem).dump(2);
    // }
  }
  else
    SANS_DEVELOPER_EXCEPTION("Invalid Neighbourhood");

  // Sort the local sets again so that local ordering matches the global ordering
  std::sort( cV.localCellGroup1.begin(), cV.localCellGroup1.end() );

  // Check that all groups in localCellGroup0 are of the same type (no mixed grids)
  // all in local group 0
  for (std::size_t i = 0; i < cV.localCellGroup0.size(); i++ )
  {
    GroupElemIndex tmpElem(0, (int) i);

    // Add element to forward and backward maps
    localToGlobalCellMapping_.insert( std::make_pair(tmpElem, (GroupElemIndex) cV.localCellGroup0[i]) ); // casting to plain (GroupElemIndex)
    globalToLocalCellMapping_.insert( std::make_pair((GroupElemIndex) cV.localCellGroup0[i], tmpElem) );

    SANS_ASSERT_MSG( this->global_xfld_.getCellGroupBase(cV.localCellGroup0.front().group).topoTypeID() ==
                     this->global_xfld_.getCellGroupBase(cV.localCellGroup0[i].group).topoTypeID(),
                     "Cell Group Topology must match for all attached elements");

    BaseType::template trackDOFs<TopologyCell>(cV.localCellGroup0[i].group, cV.localCellGroup0[i].elem);
  }

  // Check that all groups in localCellGroup1 are of the same
  // all in local group 1
  for (std::size_t i = 0; i < cV.localCellGroup1.size(); i++ )
  {
    GroupElemIndex tmpElem(1, (int) i);

    // Add element to forward and backward maps
    localToGlobalCellMapping_.insert( std::make_pair(tmpElem, (GroupElemIndex) cV.localCellGroup1[i]) ); // casting to plain
    globalToLocalCellMapping_.insert( std::make_pair((GroupElemIndex) cV.localCellGroup1[i], tmpElem) );

    SANS_ASSERT_MSG( this->global_xfld_.getCellGroupBase(cV.localCellGroup1.front().group).topoTypeID() ==
                     this->global_xfld_.getCellGroupBase(cV.localCellGroup1[i].group).topoTypeID(),
                     "Cell Group Topology must match for all attached elements");

    BaseType::template trackDOFs<TopologyCell>(cV.localCellGroup1[i].group, cV.localCellGroup1[i].elem);
  }

  // std::cout<< "extracting groups " << std::endl;
  BaseType::template extractGroups<TopologyCell, TopologyTrace>( cV );


}

}
