// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELDSPACETIME_LOCAL_H
#define XFIELDSPACETIME_LOCAL_H

#include <vector>
#include <map>

#include "Topology/Dimension.h"

#include "XField_CellToTrace.h"
#include "XField.h"
#include "XField_Local_Base.h"

#include "Field/Element/ElementAssociativityVolumeConstructor.h"
#include "Field/Element/ElementAssociativitySpacetimeConstructor.h"

#include "Field/Local/XField_LocalPatch.h"

#include "MPI/communicator_fwd.h"

/*
 * This class constructs a 3D local mesh for a specified cell in the 3D global mesh.
 * The constructed local mesh will have the following properties:
 * - The target cell (or main-cell) will be assigned to cellgroup 0, element 0
 * - All (immediate) neighboring cells of the main-cell, if any, will be assigned to cellgroup 1
 * - The traces (faces) around the main-cell will be oriented so that the main-cell is always to the left.
 * - If the main-cell touches any boundaries of the global mesh, those traces (faces) will be assigned to a boundary-trace group of their own.
 * - Traces (faces) of neighboring cells that do not touch the main-cell (referred to as "outer-boundary-traces") will all be assigned
 *   to a single boundary-trace group. This boundary-trace group goes last.
 */

/* Refer to the local solve documentation in Field/Documentation/Local_Solves/LocalSolves.pdf for more details and figures. */

namespace SANS
{

template <class PhysDim, class TopoDim>
class XField_Local;


template<class PhysDim>
class XField_Local< PhysDim, TopoD4> : public XField_Local_Base<PhysDim, TopoD4>
{

public:
  typedef XField_Local_Base<PhysDim, TopoD4> BaseType;

  XField_Local(mpi::communicator& comm_local, const XField_CellToTrace<PhysDim, TopoD4>& connectivity, int group, int elem):
    BaseType(comm_local),
    connectivity_(connectivity), global_xfld_(connectivity_.getXField()),
    mainElem_(elem),
    mainElemGroup_(group)
  {
    basisCategory_ = global_xfld_.getCellGroupBase(group).basisCategory();

    extractLocalGrid(group, elem);

    // This is checkGrid() but gets removed in release mode!
    this->checkGrid();

    for (int i = 0; i < this->nBoundaryTraceGroups(); i++)
      reSolveBoundaryTraceGroups_.push_back(i);

    for (int i = 0; i < this->nInteriorTraceGroups(); i++)
      reSolveInteriorTraceGroups_.push_back(i);
  };

  ~XField_Local(){};

  const XField_CellToTrace<PhysDim, TopoD4>& getConnectivity() const { return connectivity_; }

  int mainElem() const { return mainElem_; }
  int mainElemGroup() const { return mainElemGroup_; }
  const XField_LocalPatch<PhysDim,Pentatope>& patch() const { return *patch_; }
  const XField<PhysDim,TopoD4>& global_xfld() const { return global_xfld_; }

protected:
  using BaseType::reSolveBoundaryTraceGroups_;
  using BaseType::reSolveInteriorTraceGroups_;
  using BaseType::reSolveCellGroups_;
  const XField_CellToTrace<PhysDim, TopoD4>& connectivity_;
  const XField<PhysDim, TopoD4>& global_xfld_;
  BasisFunctionCategory basisCategory_;

  //DOF maps
  std::map<int,int> NodeDOFMap_;
  std::map<int,int> EdgeDOFMap_;
  std::map<int,int> AreaDOFMap_;
  std::map<int,int> FaceDOFMap_;
  std::map<int,int> CellDOFMap_;

  int mainElem_;
  int mainElemGroup_;
  std::shared_ptr<XField_LocalPatch<PhysDim,Pentatope>> patch_;

  int cntNodeDOF_, cntEdgeDOF_, cntAreaDOF_, cntFaceDOF_, cntCellDOF_; //Counters for different DOF types
  int offset_cellDOF_, offset_faceDOF_, offset_areaDOF_, offset_edgeDOF_, offset_nodeDOF_; //Offset for the different DOF indices in the global array

  int nPentatopeNeighbors_, nNeighbors_;
  int nMainBTraceGroups_; //No of boundary traces (and groups) on main cell
  int nOuterBTraceGroups_; //No of boundary groups for outer btraces

  int OuterBTraceGroup_Tet_; //Group indices for the outer boundary traces

  std::vector<int> maincell_reversed_traces_;
  std::vector<int> neighborcell_reversed_traces_;

  void extractLocalGrid(int group, int elem);

  template <class TopoMainCell, class TopoMainTrace>
  void extractMainCell(int main_group, int main_elem);

  void extractNeighbors(int main_group, int main_elem, int Ntrace, int order);

  template <class TopoCell>
  void trackDOFs(int group, int elem);

  template <class TopoCell>
  void setCellAssociativity(int group, int elem, ElementAssociativityConstructor<TopoD4, TopoCell>& DOFAssoc);

  template <class TopoTrace>
  void setInteriorTraceAssociativity(int group, int elem, ElementAssociativityConstructor<TopoD3, TopoTrace>& DOFAssoc);

  template <class TopoTrace>
  void setBoundaryTraceAssociativity(typename XField<PhysDim, TopoD4>::template FieldTraceGroupType<TopoTrace> tracegrp,
                                     int elem, ElementAssociativityConstructor<TopoD3, TopoTrace>& DOFAssoc);

  template <class TopoTrace, class TopoCell>
  CanonicalTraceToCell reverseTrace(int trace_group, int trace_elem, ElementAssociativityConstructor<TopoD3, TopoTrace>& DOFAssoc,
                                    int cellL_group, int cellL_elem, int cellR_group, int cellR_elem);

  template <class TopoTrace>
  void getNeighborInfoFromTrace(const int trace_group, const int trace_elem, const int main_elem,
                                int& neighbor_group, int& neighbor_elem, bool& reverse_trace_flag,
                                CanonicalTraceToCell& neighbor_canonicalTrace, CanonicalTraceToCell& main_canonicalTrace);

  template <class TopoTrace>
  void process_MainBTrace(const int trace_group, const int trace_elem, const int order, int& cnt_main_Btraces);

  template <class TopoTrace, class TopoCell>
  void
  process_OuterBTraces(const int neighbor_group, const int neighbor_elem, const int order, const int Ntrace,
                       const int local_neighbor_group, const int local_neighbor_elem,
                       const CanonicalTraceToCell& neighbor_canonicalTrace,
                       std::shared_ptr<typename BaseType::template FieldTraceGroupType<TopoTrace>
                                                        ::FieldAssociativityConstructorType> fldAssoc_Outer_BTrace,
                       const int OuterBTraceGroup, int& cnt_outer_Btraces);

};

}

#endif // XFIELDVOLUME_LOCAL_H
