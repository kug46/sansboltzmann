// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD_LOCAL_COMMON_H
#define XFIELD_LOCAL_COMMON_H

#include <vector>
#include <algorithm>

#include "Topology/Dimension.h"

#include "XField_CellToTrace.h"
#include "Field_NodalView.h"
#include "XField.h"
#include "XField_Local_Base.h"
#include "XField_Local_Neighborhoods.h"

#include "Field/Element/ElementAssociativityNode.h"
#include "Field/Element/ElementAssociativityLine.h"
#include "Field/Element/ElementAssociativityArea.h"
#include "Field/Element/ElementAssociativityVolume.h"
#include "Field/Element/ElementAssociativitySpacetime.h"

#include "Field/Element/ElementProjection_L2.h"
#include "MPI/communicator_fwd.h"
/*
  Helper functions for the construction of the local grids, reduces code duplication when constructing
*/

namespace SANS
{


struct GroupTraceIndex
{
  GroupTraceIndex() : group(-1), trace(-1) {}
  GroupTraceIndex( int group, int trace ) : group(group), trace(trace) {}

  int group, trace;

  // boilerplate stuff needed for the STL
  // Needed for std::set, follows std::pair logic
  bool operator<(const GroupTraceIndex& idx) const
  {
    if ( group < idx.group ) return true;
    if ( group == idx.group && trace  < idx.trace  ) return true;
    return false;
  }
  // syntactically consistent > operator
  bool operator>(const GroupTraceIndex& idx) const { return !operator<(idx); }

  bool operator==(const GroupTraceIndex& idx) const
  {
    if (group == idx.group && trace == idx.trace ) return true;
    return false;
  }
  // syntactically consistent != operator
  bool operator!=(const GroupTraceIndex& idx) const {return !operator==(idx); }

  // for debugging
  friend std::ostream &operator<<(std::ostream& os, const GroupTraceIndex& idx )
  {
    os << "(group, trace) = (" << idx.group << ", "  << idx.trace << ")";
    return os;
  }
};

template< class TopoDim, class TopologyTrace >
struct GroupTraceConstructorIndex : public GroupTraceIndex
{
  GroupTraceConstructorIndex() : GroupTraceIndex(),
      pGlobalAssoc(), localAssoc(), localLRElems(), elementSplitInfo(ElementSplitInfo(ElementSplitFlag::Unsplit)) {}
  GroupTraceConstructorIndex( int group, int trace ) : GroupTraceIndex(group,trace),
      pGlobalAssoc(), localAssoc(), localLRElems(), elementSplitInfo(ElementSplitInfo(ElementSplitFlag::Unsplit)) {}
  // cppcheck-suppress noExplicitConstructor
  GroupTraceConstructorIndex( const GroupTraceIndex& groupTrace) : GroupTraceIndex(groupTrace), // basic copy constructor
      pGlobalAssoc(), localAssoc(), localLRElems(), elementSplitInfo(ElementSplitInfo(ElementSplitFlag::Unsplit)) {}
  explicit GroupTraceConstructorIndex( const GroupTraceConstructorIndex& groupTrace) : GroupTraceIndex(groupTrace),  // richer copy constructor
      pGlobalAssoc(groupTrace.pGlobalAssoc), localAssoc(groupTrace.localAssoc),
      localLRElems(groupTrace.localLRElems), elementSplitInfo(groupTrace.elementSplitInfo) {}

  GroupTraceConstructorIndex& operator=( const GroupTraceConstructorIndex& groupTrace )
  {
    pGlobalAssoc = groupTrace.pGlobalAssoc;
    localAssoc = groupTrace.localAssoc;
    localLRElems = groupTrace.localLRElems;
    elementSplitInfo = groupTrace.elementSplitInfo;

    return *this;
  }

  const ElementAssociativity<typename TopoDim::TopoDTrace,TopologyTrace>* pGlobalAssoc;
  ElementAssociativityConstructor<typename TopoDim::TopoDTrace,TopologyTrace> localAssoc;

  std::pair<GroupElemIndex,GroupElemIndex> localLRElems; // left and right elements

  ElementSplitInfo elementSplitInfo; // default constructor is unsplit
};

// Inherits from the basic GroupElemIndex, but has some additional information about trace reversals
template< class TopoDim, class Topology>
struct GroupElemConstructorIndex : public GroupElemIndex
{
  GroupElemConstructorIndex(int group, int elem) : GroupElemIndex(group,elem),
      pGlobalAssoc(), localAssoc(), elementSplitInfo(ElementSplitInfo(ElementSplitFlag::Unsplit)) {}
  // cppcheck-suppress noExplicitConstructor
  GroupElemConstructorIndex(const GroupElemIndex& groupElem) : GroupElemIndex(groupElem), // base copy constructor
      pGlobalAssoc(), localAssoc(), elementSplitInfo(ElementSplitInfo(ElementSplitFlag::Unsplit)) {}
  GroupElemConstructorIndex(const GroupElemConstructorIndex& groupElem) : GroupElemIndex(groupElem), // richer copy constructor
      pGlobalAssoc(groupElem.pGlobalAssoc), localAssoc(groupElem.localAssoc),
      elementSplitInfo(groupElem.elementSplitInfo) {}

  GroupElemConstructorIndex& operator=( const GroupElemConstructorIndex& groupElem )
  {
    pGlobalAssoc = groupElem.pGlobalAssoc;
    localAssoc = groupElem.localAssoc;
    elementSplitInfo = groupElem.elementSplitInfo;

    return *this;
  }

  const ElementAssociativity<TopoDim,Topology>*  pGlobalAssoc;
  ElementAssociativityConstructor<TopoDim,Topology> localAssoc;

  ElementSplitInfo elementSplitInfo; // default constructor is unsplit
};

// // indices for the vector types
// template< class TopoDim, class TopologyCell>
// using GroupElemConstructorIndex = GroupElemConstructorIndex<TopoDim,TopologyCell>;
//
// template< class TopoDim, class TopologyTrace>
// using GroupTraceConstructorIndex = GroupTraceConstructorIndex<typename TopoDim::TopoDTrace,TopologyTrace>;

// vector types for holding the sets
template<class TopoDim, class TopologyCell>
using GroupElemConstructorVector = std::vector<GroupElemConstructorIndex<TopoDim,TopologyCell>>;

template<class TopoDim, class TopologyTrace>
using GroupTraceConstructorVector = std::vector<GroupTraceConstructorIndex<TopoDim,TopologyTrace>>;

struct ConstructorVector
{
  ConstructorVector() {}
  virtual ~ConstructorVector() {};
};

template<class TopoDim, class TopologyCell, class TopologyTrace>
struct ConstructorVectorTopo : public ConstructorVector
{
  GroupElemConstructorVector<TopoDim,TopologyCell> localCellGroup0, localCellGroup1;
  GroupTraceConstructorVector<TopoDim,TopologyTrace> localITraceGroup0, localITraceGroup0to1, localITraceGroup1;
  GroupTraceConstructorVector<TopoDim,TopologyTrace> localBTraceGroup0, localBTraceGroup1;
  GroupTraceConstructorVector<TopoDim,TopologyTrace> localBTraceOuterGroup0, localBTraceOuterGroup1;
};



template< class PhysDim, class TopoDim>
class XField_Local_Common : public XField_Local_Base<PhysDim,TopoDim>
{
public:
  typedef XField_Local_Base<PhysDim, TopoDim> BaseType;

  XField_Local_Common( const XField_Local_Common& xfld, const FieldCopy tag);

  XField_Local_Common( mpi::communicator& comm_local,
                       const XField_CellToTrace<PhysDim, TopoDim>& connectivity,
                       const Field_NodalView& nodalView,
                       const Neighborhood neighborhood )
  :
  BaseType(comm_local),
  neighborhood(neighborhood),
  connectivity_(connectivity), nodalView_(nodalView), global_xfld_(connectivity_.getXField()),
  nCellGroups_(-1), nInteriorTraceGroups_(-1), nBoundaryTraceGroups_(-1),
  cntNodeDOF_(0),
  idxITraceGroup0to1_(-1), idxITraceGroup0_(-1), idxITraceGroup1_(-1),
  idxBTraceGroup0_(-1), idxBTraceGroup1_(-1)
  {}

  XField_Local_Common( std::shared_ptr<mpi::communicator> comm_local,
                       const XField_CellToTrace<PhysDim, TopoDim>& connectivity,
                       const Field_NodalView& nodalView,
                       const Neighborhood neighborhood )
  :
  BaseType(comm_local),
  neighborhood(neighborhood),
  connectivity_(connectivity), nodalView_(nodalView), global_xfld_(connectivity_.getXField()),
  nCellGroups_(-1), nInteriorTraceGroups_(-1), nBoundaryTraceGroups_(-1),
  cntNodeDOF_(0),
  idxITraceGroup0to1_(-1), idxITraceGroup0_(-1), idxITraceGroup1_(-1),
  idxBTraceGroup0_(-1), idxBTraceGroup1_(-1)
  {}
  ~XField_Local_Common(){};

  const Neighborhood neighborhood;

  std::vector<int> getLocalNodes( const std::vector<int>& globalNodes) const
  {
    std::vector<int> localNodes;
    localNodes.reserve(globalNodes.size());
    for (auto const& node : globalNodes )
    {
      const int localNode = NodeDOFMap_.at(node);
      localNodes.push_back(localNode);
    }
    return localNodes;
  }
  //
  // virtual bool isEdgeMode() const { return false; };

protected:

  using BaseType::reSolveBoundaryTraceGroups_;
  using BaseType::reSolveInteriorTraceGroups_;
  using BaseType::reSolveCellGroups_;

  // XField_Local_Common(mpi::communicator& comm) : XField_Local_Base<PhysDim, TopoDim>(comm) {}
  // XField_Local_Common(std::shared_ptr<mpi::communicator> comm) : XField_Local_Base<PhysDim, TopoDim>(comm) {}

  const XField_CellToTrace<PhysDim, TopoDim>& connectivity_;
  const Field_NodalView& nodalView_;
  const XField<PhysDim, TopoDim>& global_xfld_;
  BasisFunctionCategory basisCategory_;

  //DOF maps
  std::map<int,int> NodeDOFMap_;
  std::map<int,int> EdgeDOFMap_;
  std::map<int,int> CellDOFMap_;

  std::vector<int> orderVector_; // basis order for each cell group

  int nCellGroups_, nInteriorTraceGroups_, nBoundaryTraceGroups_, nGhostBoundaryTraceGroups_;

  int cntNodeDOF_; //Counters for different DOF types
  const int offset_nodeDOF_ = 0; //Offset for the different DOF indices in the global array

  std::map<GroupElemIndex,GroupElemIndex> localToGlobalCellMapping_;
  std::map<GroupElemIndex,GroupElemIndex> globalToLocalCellMapping_;

  std::map<GroupTraceIndex,GroupTraceIndex> localToGlobalInteriorTraceMapping_;
  std::map<GroupTraceIndex,GroupTraceIndex> globalToLocalInteriorTraceMapping_;

  std::map<GroupTraceIndex,GroupTraceIndex> localToGlobalBoundaryTraceMapping_;
  std::map<GroupTraceIndex,GroupTraceIndex> globalToLocalBoundaryTraceMapping_;

  // indices for referencing cell and trace groups
  // in order
  int idxITraceGroup0to1_, idxITraceGroup0_, idxITraceGroup1_;

  // first index of the boundary group, i.e. group0 is accessed with (int i = idxBTraceGroup0_; i < idxBTraceGroup1_; i++)
  int idxBTraceGroup0_, idxBTraceGroup1_;

  const int order_ = 1;

  void computeReSolveGroups()
  {
    // Always start with cell group 0
    reSolveCellGroups_ = {0};

    // These are all less than or equal to because the idx are the last index of each group
    // i.e. idxITraceGroup0_ == 0 means that group 0 is in ITraceGroup0_

    // trace interior to group 0 and 0-1 are always included
    for (int i = 0; i < max(idxITraceGroup0to1_,idxITraceGroup0_); i++)
      reSolveInteriorTraceGroups_.push_back(i);

    for (int i = 0; i < idxBTraceGroup0_; i++)
      reSolveBoundaryTraceGroups_.push_back(i);

    if ( neighborhood == Neighborhood::Dual )
    {
      // if Dual grid, include cell group 1
      for (int i = 1; i < this->nCellGroups_; i++)
        reSolveCellGroups_.push_back(i);

      // if Dual grid, include trace groups internal to cell group 1 if there
      for (int i = max(0,max(idxITraceGroup0to1_,idxITraceGroup0_)); i < idxITraceGroup1_; i++)
        reSolveInteriorTraceGroups_.push_back(i);

      // if Dual grid, include boundary trace groups attached to cell group 1
      for (int i = max(0,idxBTraceGroup0_); i < idxBTraceGroup1_; i++)
        reSolveBoundaryTraceGroups_.push_back(i);
    }
  }

  // pointer for constructor vector types
  std::shared_ptr<ConstructorVector> p_constructorVector_;

  // fills in the vector structures
  template <class TopologyCell, class TopologyTrace>
  void extractGroups( ConstructorVectorTopo<TopoDim,TopologyCell,TopologyTrace>& constructorVectors );

  // uses the vector structures to create the grid
  template <class TopologyCell, class TopologyTrace>
  void allocateMesh(  const ConstructorVectorTopo<TopoDim,TopologyCell,TopologyTrace>& constructorVectors );

  template <class TopologyCell>
  void extractCellGroup( const GroupElemConstructorVector<TopoDim,TopologyCell>& localCellGroup, int& groupIndex );

  template <class TopologyCell, class TopologyTrace>
  void extractITraceGroup( const GroupTraceConstructorVector<TopoDim,TopologyTrace>& localTraceGroup,
                           const GroupElemConstructorVector<TopoDim,TopologyCell>& leftCellGroup,
                           const GroupElemConstructorVector<TopoDim,TopologyCell>& rightCellGroup,
                           int& groupIndex );

  template <class TopologyCell, class TopologyTrace>
  void extractBTraceGroup( const GroupTraceConstructorVector<TopoDim,TopologyTrace>& localTraceGroup,
                           const GroupElemConstructorVector<TopoDim,TopologyCell>& leftCellGroup,
                           int& groupIndex );

  template <class TopologyCell, class TopologyTrace>
  void extractOuterBTraceGroup( const GroupTraceConstructorVector<TopoDim,TopologyTrace>& localTraceGroup,
                                const GroupElemConstructorVector<TopoDim,TopologyCell>& leftCellGroup );

  // for constructing the initial local grid
  template <class Topology>
  void trackDOFs(int group, int elem);

  template <class TopoCell>
  void setCellAssociativity(const GroupElemConstructorIndex<TopoDim,TopoCell>& elem,
                            ElementAssociativityConstructor<TopoDim,TopoCell>& DOFAssoc);

  template < class TopologyTrace >
  void setInteriorTraceAssociativity(const GroupTraceConstructorIndex<TopoDim,TopologyTrace>& trace,
                                     ElementAssociativityConstructor<typename TopoDim::TopoDTrace,TopologyTrace>& DOFAssoc);

  template < class TopologyTrace >
  void setBoundaryTraceAssociativity(const GroupTraceConstructorIndex<TopoDim,TopologyTrace>& trace,
                                     ElementAssociativityConstructor<typename TopoDim::TopoDTrace,TopologyTrace>& DOFAssoc);

  // Actually does the work in terms of splitting the grid
  template <class TopologyCell, class TopologyTrace >
  void split( const int startNode, const int endNode, const int newNode,
              ConstructorVectorTopo<TopoDim,TopologyCell,TopologyTrace>& cV );

  template <class Topology>
  void projectGroup( GroupElemConstructorVector<TopoDim,Topology>& localCellGroup, const int cellGroup );

  template <class TopologyCell >
  void
  projectElem(const GroupElemIndex& main_elem, const int sub_group, const int sub_elem,
              Element_Subdivision_Projector<TopoDim, TopologyCell>& projector);

  template <class TopologyCell, class TopologyTrace>
  CanonicalTraceToCell
  orientTrace( const std::array<int,TopologyCell::NNode>& leftCellNodeMap,
               const std::array<int,TopologyCell::NNode>& rightCellNodeMap,
               typename ElementAssociativity<typename TopoDim::TopoDTrace,TopologyTrace>::Constructor& DOFAssoc);

  template <class TopologyCell, class TopologyTrace>
  void
  orientTrace( const std::array<int, TopologyCell::NNode>& leftCellNodeMap,
               typename ElementAssociativity<typename TopoDim::TopoDTrace,TopologyTrace>::Constructor& DOFAssoc);
};


template< class PhysDim, class TopoDim >
XField_Local_Common<PhysDim, TopoDim>::XField_Local_Common( const XField_Local_Common<PhysDim,TopoDim>& xfld, const FieldCopy tag)
: BaseType(  xfld, tag), neighborhood(xfld.neighborhood),
connectivity_(xfld.connectivity_), nodalView_(xfld.nodalView_), global_xfld_(xfld.global_xfld_)
 {
  // SANS_DEVELOPER_EXCEPTION("Copy Constructor is currently untested");
  this->reSolveBoundaryTraceGroups_ = xfld.reSolveBoundaryTraceGroups_;
  this->reSolveInteriorTraceGroups_ = xfld.reSolveInteriorTraceGroups_;
  this->reSolveCellGroups_ = xfld.reSolveCellGroups_;

  this->basisCategory_ = xfld.basisCategory_;

  this->NodeDOFMap_ = xfld.NodeDOFMap_;
  this->EdgeDOFMap_ = xfld.EdgeDOFMap_;
  this->CellDOFMap_ = xfld.CellDOFMap_;

  this->orderVector_ = xfld.orderVector_;

  this->nCellGroups_ = xfld.nCellGroups_;
  this->nInteriorTraceGroups_ = xfld.nInteriorTraceGroups_;
  this->nBoundaryTraceGroups_ = xfld.nBoundaryTraceGroups_;
  this->nGhostBoundaryTraceGroups_ = xfld.nGhostBoundaryTraceGroups_;

  this-> cntNodeDOF_ = xfld.cntNodeDOF_;

  this->localToGlobalCellMapping_ = xfld.localToGlobalCellMapping_;
  this->globalToLocalCellMapping_ = xfld.globalToLocalCellMapping_;

  this->localToGlobalInteriorTraceMapping_ = xfld.localToGlobalInteriorTraceMapping_;
  this->globalToLocalInteriorTraceMapping_ = xfld.globalToLocalInteriorTraceMapping_;

  this->localToGlobalBoundaryTraceMapping_ = xfld.localToGlobalBoundaryTraceMapping_;
  this->globalToLocalBoundaryTraceMapping_ = xfld.globalToLocalBoundaryTraceMapping_;

  this->idxITraceGroup0to1_ = xfld.idxITraceGroup0to1_;
  this->idxITraceGroup0_ = xfld.idxITraceGroup0_;
  this->idxITraceGroup1_ = xfld.idxITraceGroup1_;
  this->idxBTraceGroup0_ = xfld.idxBTraceGroup0_;
  this->idxBTraceGroup1_ = xfld.idxBTraceGroup1_;

  this->p_constructorVector_ = xfld.p_constructorVector_;

  this->CellMapping_          = xfld.CellMapping_;
  this->InteriorTraceMapping_ = xfld.InteriorTraceMapping_;
  this->BoundaryTraceMapping_ = xfld.BoundaryTraceMapping_;

  this->CellSplitInfo_          = xfld.CellSplitInfo_;
  this->InteriorTraceSplitInfo_ = xfld.InteriorTraceSplitInfo_;
  this->BoundaryTraceSplitInfo_ = xfld.InteriorTraceSplitInfo_;

  this->nCellGroups_ = xfld.nCellGroups_;
  this->nInteriorTraceGroups_ = xfld.nInteriorTraceGroups_;
  this->nBoundaryTraceGroups_ = xfld.nBoundaryTraceGroups_;
  this->nGhostBoundaryTraceGroups_ = xfld.nGhostBoundaryTraceGroups_;

 }

template <class PhysDim, class TopoDim>
template <class TopologyCell>
void
XField_Local_Common<PhysDim, TopoDim>::trackDOFs(int group, int elem)
{
  // Extract the linear mesh DOFs
  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyCell> XFieldCellGroupType;

  std::pair<std::map<int,int>::iterator,bool> map_ret; //return value for map inserts

  const XFieldCellGroupType& cellgrp = this->global_xfld_.template getCellGroup<TopologyCell>(group);

  int nNodeDOF = cellgrp.associativity(elem).nNode();

  // node DOF, edge DOF and cell DOF maps
  std::vector<int> nodeMap(nNodeDOF);

  //Save DOF mappings from global mesh to local mesh
  cellgrp.associativity(elem).getNodeGlobalMapping( nodeMap.data(), nodeMap.size() );
  for (int k = 0; k < nNodeDOF; k++)
  {
    map_ret = NodeDOFMap_.insert( std::pair<int,int>(nodeMap[k],cntNodeDOF_) );
    if (map_ret.second==true) cntNodeDOF_++; //increment counter if object was actually added
  }

}

template <class PhysDim, class TopoDim>
template <class TopologyCell, class TopologyTrace>
void
XField_Local_Common<PhysDim, TopoDim>::extractGroups( ConstructorVectorTopo<TopoDim,TopologyCell,TopologyTrace>& cV )
{
  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyCell> XFieldCellGroupType;
  typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename XFieldCellGroupType::BasisType CellBasisType;
  typedef typename XFieldTraceGroupType::BasisType TraceBasisType;
  const CellBasisType* cell_basis = CellBasisType::getBasisFunction(order_, basisCategory_);
  const TraceBasisType* trace_basis = TraceBasisType::getBasisFunction(order_, basisCategory_);

  //-------------------------------------------------------------------------------------------------------------
  // Looping over the main group to count interior traces, boundary traces and their groups
  // accumulating their information into GroupTraceVectors
  // Currently assumes that the topology is the same for group 0 and group 1

  // counter for the number of elems in the outer group
  // these are put in local group 2 in the localToGlobal maps
  int cnt_outerBTrace = 0;

  if (cV.localCellGroup0.empty()) // This best not be empty, else there'll be problems
      SANS_DEVELOPER_EXCEPTION("The 0 cell group is empty, something went very badly wrong");

  // loop through the cells of cell group 0
  for (std::size_t j = 0; j < cV.localCellGroup0.size(); j++)
  {
    // save off a copy of the relevant associativities
    const XFieldCellGroupType& cellgrp = global_xfld_.template getCellGroup<TopologyCell>(cV.localCellGroup0[j].group);
    cV.localCellGroup0[j].localAssoc.resize(cell_basis); // resize with the basis

    // copy global associativity pointer and construct local associativity
    cV.localCellGroup0[j].pGlobalAssoc = &(cellgrp.associativity(cV.localCellGroup0[j].elem));
    setCellAssociativity<TopologyCell>( cV.localCellGroup0[j], cV.localCellGroup0[j].localAssoc );

    // loop throug the traces of this element
    for (int i = 0; i < TopologyCell::NTrace; i++)
    {
      GroupElemIndex cellNeighbor;
      GroupTraceConstructorIndex<TopoDim,TopologyTrace> traceNeighbor;

      // Get indices of neighbouring cell (and the cell Group)
      const TraceInfo& traceinfo = connectivity_.getTrace( cV.localCellGroup0[j].group, cV.localCellGroup0[j].elem, i );
      traceNeighbor.group = traceinfo.group;
      traceNeighbor.trace = traceinfo.elem;

      // resize using the trace_basis
      traceNeighbor.localAssoc.resize(trace_basis);

      // BOUNDARY TRACES
      if (traceinfo.type == TraceInfo::Boundary) // The trace is a boundary
      {
        const XFieldTraceGroupType& tracegrp = global_xfld_.template getBoundaryTraceGroup<TopologyTrace>(traceNeighbor.group);
        traceNeighbor.localLRElems.first = globalToLocalCellMapping_.at((GroupElemIndex) cV.localCellGroup0[j]) ;

        // copy global and construct local associativity
        traceNeighbor.pGlobalAssoc = &(tracegrp.associativity(traceNeighbor.trace));
        setBoundaryTraceAssociativity(traceNeighbor, traceNeighbor.localAssoc);

        cV.localBTraceGroup0.push_back( traceNeighbor );
        continue; // skip the rest of the loop

      } // boundary trace

      if ( traceinfo.type == TraceInfo::GhostBoundary)
      {
        // Outer group and orientation is correct
        GroupElemIndex cellNeighbor(-1,-(1+cnt_outerBTrace)); // negative values as we don't have any information on it

        // // if this outer element isn't already in the maps, add to the maps
        // if ( globalToLocalCellMapping_.find(cellNeighbor) == globalToLocalCellMapping_.end() )
        // {
          // Add the element to the forward and backward maps, though its not strictly in the local mesh
        GroupElemIndex tmpElem( 2, cnt_outerBTrace );
        localToGlobalCellMapping_.insert( std::pair<GroupElemIndex,GroupElemIndex>(tmpElem, cellNeighbor) ); // cellNeighbor is plain GroupElemIndex
        globalToLocalCellMapping_.insert( std::pair<GroupElemIndex,GroupElemIndex>(cellNeighbor, tmpElem) );
        cnt_outerBTrace++;
        // }

        const XFieldTraceGroupType& tracegrp = global_xfld_.template getGhostBoundaryTraceGroup<TopologyTrace>(traceNeighbor.group);

        // copy global and construct local associativity
        traceNeighbor.pGlobalAssoc = &(tracegrp.associativity(traceNeighbor.trace));
        setBoundaryTraceAssociativity(traceNeighbor, traceNeighbor.localAssoc);

        traceNeighbor.localLRElems.first  = globalToLocalCellMapping_.at((GroupElemIndex) cV.localCellGroup0[j]);
        traceNeighbor.localLRElems.second = globalToLocalCellMapping_.at(cellNeighbor);

        // outer boundary trace groups are always oriented correctly so just add it in
        cV.localBTraceOuterGroup1.push_back( traceNeighbor );
        //std::array<int,TopologyCell::NTrace> traceSigns = cV.localCellGroup1[j].pGlobalAssoc->traceOrientation();
        //cV.localCellGroup1[j].localAssoc.setOrientation( traceSigns[i], i ); // copy the global Assoc trace sign over
        continue; // skip the rest of the loop
      }


      // Assert that not a ghost trace
      SANS_ASSERT_MSG( traceinfo.type == TraceInfo::Interior, "local cell group 0 can not be attached to a ghost boundary" );

      // INTERIOR TRACES
      const XFieldTraceGroupType& tracegrp = global_xfld_.template getInteriorTraceGroup<TopologyTrace>(traceNeighbor.group);
      traceNeighbor.pGlobalAssoc = &(tracegrp.associativity(traceNeighbor.trace));

      setInteriorTraceAssociativity( traceNeighbor, traceNeighbor.localAssoc );

      // Checking if there's an attached element also in the cellGroup
      if (   tracegrp.getElementLeft(traceNeighbor.trace) == cV.localCellGroup0[j].elem
          && tracegrp.getGroupLeft()                      == cV.localCellGroup0[j].group) // left elem is current
      {
        cellNeighbor.group  = tracegrp.getGroupRight();
        cellNeighbor.elem   = tracegrp.getElementRight( traceNeighbor.trace );

        // check if neighbour is in the maps. If it is in the map, check it is not an outerGroup member
        if (   globalToLocalCellMapping_.find(cellNeighbor) != globalToLocalCellMapping_.end()
            && globalToLocalCellMapping_.at(cellNeighbor).group != 2 )
        {
          // Left element comes before right in the local ordering
          if ( globalToLocalCellMapping_.at((GroupElemIndex)cV.localCellGroup0[j]) < globalToLocalCellMapping_.at(cellNeighbor) )
          {
            // Orientation was right construct without reversal
            traceNeighbor.localLRElems.first  = globalToLocalCellMapping_.at((GroupElemIndex) cV.localCellGroup0[j]);
            traceNeighbor.localLRElems.second = globalToLocalCellMapping_.at(cellNeighbor);
          }
          else if ( globalToLocalCellMapping_.at((GroupElemIndex)cV.localCellGroup0[j]) > globalToLocalCellMapping_.at(cellNeighbor) )
          {
            // Orientation was wrong, construct with reversal
            std::array<int, TopologyCell::NNode > leftCellNodeMap, rightCellNodeMap;

            const XFieldCellGroupType& neighborgrp = global_xfld_.template getCellGroup<TopologyCell>(cellNeighbor.group);
            neighborgrp.associativity(cellNeighbor.elem).getNodeGlobalMapping( leftCellNodeMap.data(), leftCellNodeMap.size() );
            cellgrp.associativity(cV.localCellGroup0[j].elem).getNodeGlobalMapping( rightCellNodeMap.data(), rightCellNodeMap.size() );

            // change node maps to the local nodes
            for (int k = 0; k < TopologyCell::NNode; k++)
            {
              leftCellNodeMap[k]  = offset_nodeDOF_ + NodeDOFMap_.at( leftCellNodeMap[k] );
              rightCellNodeMap[k] = offset_nodeDOF_ + NodeDOFMap_.at(rightCellNodeMap[k] );
            }

            CanonicalTraceToCell rightCanonical =
                orientTrace<TopologyCell,TopologyTrace>( leftCellNodeMap, rightCellNodeMap, traceNeighbor.localAssoc );

            // Update local cell associativity with the reversal
            SANS_ASSERT( rightCanonical.trace == i );
            cV.localCellGroup0[j].localAssoc.setOrientation( rightCanonical.orientation, rightCanonical.trace );

            traceNeighbor.localLRElems.second = globalToLocalCellMapping_.at((GroupElemIndex) cV.localCellGroup0[j]);
            traceNeighbor.localLRElems.first  = globalToLocalCellMapping_.at(cellNeighbor);
          }
          else // it's in the outer group
            SANS_DEVELOPER_EXCEPTION("A Cell Group 0 neighbour is not in the local mesh, this is wrong");
        }
        else // outer group and oriented correctly
        {
          // Orientation was correct construct without reversal

          // if this outer element isn't already in the maps, add to the maps
          if ( globalToLocalCellMapping_.find(cellNeighbor) == globalToLocalCellMapping_.end() )
          {
            // Add the element to the forward and backward maps, though its not strictly in the local mesh
            GroupElemIndex tmpElem( 2, cnt_outerBTrace );
            localToGlobalCellMapping_.insert( std::make_pair(tmpElem, cellNeighbor) ); // cellNeighbor is plain GroupElemIndex
            globalToLocalCellMapping_.insert( std::make_pair(cellNeighbor, tmpElem) );
            cnt_outerBTrace++;
          }

          traceNeighbor.localLRElems.first  = globalToLocalCellMapping_.at((GroupElemIndex) cV.localCellGroup0[j]);
          traceNeighbor.localLRElems.second = globalToLocalCellMapping_.at(cellNeighbor);
        }
      }
      else if ( tracegrp.getElementRight(traceNeighbor.trace) == cV.localCellGroup0[j].elem
             && tracegrp.getGroupRight()                      == cV.localCellGroup0[j].group) // right elem is current
      {
        cellNeighbor.group  = tracegrp.getGroupLeft();
        cellNeighbor.elem   = tracegrp.getElementLeft( traceNeighbor.trace );

        // check if neighbour is in the maps. If it is in the map, check it is not an outerGroup member
        if (   globalToLocalCellMapping_.find(cellNeighbor) != globalToLocalCellMapping_.end()
            && globalToLocalCellMapping_.at(cellNeighbor).group != 2)
        {
          if ( globalToLocalCellMapping_.at(cellNeighbor) < globalToLocalCellMapping_.at((GroupElemIndex)cV.localCellGroup0[j]) )
          {
            // Orientation was right construct without reversal
            traceNeighbor.localLRElems.first  = globalToLocalCellMapping_.at(cellNeighbor);
            traceNeighbor.localLRElems.second = globalToLocalCellMapping_.at((GroupElemIndex) cV.localCellGroup0[j]);

            //std::array<int,TopologyCell::NTrace> traceSigns = cV.localCellGroup0[j].pGlobalAssoc->traceOrientation();
            //cV.localCellGroup0[j].localAssoc.setOrientation( traceSigns[i], i ); // copy the global Assoc traceOrientation
          }
          else if ( globalToLocalCellMapping_.at(cellNeighbor) > globalToLocalCellMapping_.at((GroupElemIndex)cV.localCellGroup0[j]) )
          {
            // Orientation was wrong, construct with reversal
            std::array<int, TopologyCell::NNode > leftCellNodeMap;

            cellgrp.associativity(cV.localCellGroup0[j].elem).getNodeGlobalMapping( leftCellNodeMap.data(), leftCellNodeMap.size() );

            // change node maps to the local nodes
            for (int k = 0; k < TopologyCell::NNode; k++)
              leftCellNodeMap[k]  = offset_nodeDOF_ + NodeDOFMap_.at( leftCellNodeMap[k] );

            orientTrace<TopologyCell,TopologyTrace>( leftCellNodeMap, traceNeighbor.localAssoc );

            // Update local cell associativity with the reversal
            cV.localCellGroup0[j].localAssoc.setOrientation( +1, i );

            traceNeighbor.localLRElems.second = globalToLocalCellMapping_.at(cellNeighbor);
            traceNeighbor.localLRElems.first  = globalToLocalCellMapping_.at((GroupElemIndex) cV.localCellGroup0[j]);
          }
        }
        else // it's in the outer group and oriented incorrectly
        {
          // Orientation was wrong construct with reversal
          std::array<int, TopologyCell::NNode > leftCellNodeMap;

          cellgrp.associativity(cV.localCellGroup0[j].elem).getNodeGlobalMapping( leftCellNodeMap.data(), leftCellNodeMap.size() );

          // change node maps to the local nodes
          for (int k = 0; k < TopologyCell::NNode; k++)
            leftCellNodeMap[k]  = offset_nodeDOF_ + NodeDOFMap_.at( leftCellNodeMap[k] );

          orientTrace<TopologyCell,TopologyTrace>( leftCellNodeMap, traceNeighbor.localAssoc );

          // Update local cell associativity with the reversal
          cV.localCellGroup0[j].localAssoc.setOrientation( +1, i );

          if ( globalToLocalCellMapping_.find(cellNeighbor) == globalToLocalCellMapping_.end() )
          {
            // Add the element to the forward and backward maps, though not strictly in the local mesh
            GroupElemIndex tmpElem( 2, cnt_outerBTrace );
            localToGlobalCellMapping_.insert( std::make_pair(tmpElem, cellNeighbor) ); // cellNeighbor is plain GroupElemIndex
            globalToLocalCellMapping_.insert( std::make_pair(cellNeighbor, tmpElem) );
            cnt_outerBTrace++;
          }

          traceNeighbor.localLRElems.first  = globalToLocalCellMapping_.at((GroupElemIndex) cV.localCellGroup0[j]);
          traceNeighbor.localLRElems.second = globalToLocalCellMapping_.at(cellNeighbor);
        }
      }
      else
        SANS_DEVELOPER_EXCEPTION("Neither neighbour of the trace is the starting element, "
            "something went very wrong");

      bool inCellGroup0  = std::find(cV.localCellGroup0.begin(),   cV.localCellGroup0.end(),   cellNeighbor) != cV.localCellGroup0.end();
      bool inTraceGroup0 = std::find(cV.localITraceGroup0.begin(), cV.localITraceGroup0.end(), traceNeighbor)!= cV.localITraceGroup0.end();

      // checking if this neighbour is internal to cV.localCellGroup0 and trace has not already been added
      if ( inCellGroup0 && !inTraceGroup0 )
      {
        cV.localITraceGroup0.push_back( traceNeighbor ); // internal to group 0
        continue;
      }

      // checking if this neighbour is in cV.localCellGroup1, don't need to check uniqueness
      bool inCellGroup1 = std::find(cV.localCellGroup1.begin(), cV.localCellGroup1.end(), cellNeighbor)!=cV.localCellGroup1.end();
      if ( inCellGroup1 )
      {
        cV.localITraceGroup0to1.push_back( traceNeighbor ); // connecting to group 1
        continue;
      }

      if ( !inCellGroup0 && !inCellGroup1 ) // in outer group
      {
        cV.localBTraceOuterGroup1.push_back( traceNeighbor ); // connecting group 0 to rest of mesh
      }
    }
  }

  if (!cV.localCellGroup1.empty()) // neighbourhood 1 isn't empty
  {
    for ( std::size_t j = 0; j < cV.localCellGroup1.size(); j++)
    {
      // save off a copy of the relevant associativities
      const XFieldCellGroupType& cellgrp = global_xfld_.template getCellGroup<TopologyCell>(cV.localCellGroup1[j].group);
      cV.localCellGroup1[j].localAssoc.resize(cell_basis); // resize with the basis

      cV.localCellGroup1[j].pGlobalAssoc = &(cellgrp.associativity(cV.localCellGroup1[j].elem));
      setCellAssociativity<TopologyCell>( cV.localCellGroup1[j], cV.localCellGroup1[j].localAssoc );

      for (int i = 0; i < TopologyCell::NTrace; i++)
      {
        GroupElemIndex cellNeighbor;
        GroupTraceConstructorIndex<TopoDim,TopologyTrace> traceNeighbor;

        // Get indices of neighbouring cells (and the trace Group)
        const TraceInfo& traceinfo = connectivity_.getTrace( cV.localCellGroup1[j].group, cV.localCellGroup1[j].elem, i );
        traceNeighbor.group = traceinfo.group;
        traceNeighbor.trace = traceinfo.elem;

        // resize using the basis
        traceNeighbor.localAssoc.resize(trace_basis);

        // BOUNDARY TRACES
        if (traceinfo.type == TraceInfo::Boundary)
        {
          //printf("trace %d of elem %d in localCellGroup1 (group %d) is on boundary\n",i,cV.localCellGroup1[j].elem,cV.localCellGroup1[j].group);
          const XFieldTraceGroupType& tracegrp = global_xfld_.template getBoundaryTraceGroup<TopologyTrace>(traceNeighbor.group);

          // copy global and construct local associativity
          traceNeighbor.pGlobalAssoc = &(tracegrp.associativity(traceNeighbor.trace));
          setBoundaryTraceAssociativity(traceNeighbor, traceNeighbor.localAssoc);

          traceNeighbor.localLRElems.first = globalToLocalCellMapping_.at((GroupElemIndex) cV.localCellGroup1[j]) ;

          cV.localBTraceGroup1.push_back( traceNeighbor );
          continue; // skip the rest of the loop
        }

        if ( traceinfo.type == TraceInfo::GhostBoundary)
        {

          // Outer group and orientation is correct
          GroupElemIndex cellNeighbor(-1,-(1+cnt_outerBTrace)); // negative values as we don't have any information on it

          // // if this outer element isn't already in the maps, add to the maps
          // if ( globalToLocalCellMapping_.find(cellNeighbor) == globalToLocalCellMapping_.end() )
          // {
            // Add the element to the forward and backward maps, though its not strictly in the local mesh
          GroupElemIndex tmpElem( 2, cnt_outerBTrace );
          localToGlobalCellMapping_.insert( std::pair<GroupElemIndex,GroupElemIndex>(tmpElem, cellNeighbor) ); // cellNeighbor is plain GroupElemIndex
          globalToLocalCellMapping_.insert( std::pair<GroupElemIndex,GroupElemIndex>(cellNeighbor, tmpElem) );
          cnt_outerBTrace++;
          // }

          const XFieldTraceGroupType& tracegrp = global_xfld_.template getGhostBoundaryTraceGroup<TopologyTrace>(traceNeighbor.group);

          // copy global and construct local associativity
          traceNeighbor.pGlobalAssoc = &(tracegrp.associativity(traceNeighbor.trace));
          setBoundaryTraceAssociativity(traceNeighbor, traceNeighbor.localAssoc);

          traceNeighbor.localLRElems.first  = globalToLocalCellMapping_.at((GroupElemIndex) cV.localCellGroup1[j]);
          traceNeighbor.localLRElems.second = globalToLocalCellMapping_.at(cellNeighbor);

          // outer boundary trace groups are always oriented correctly so just add it in
          cV.localBTraceOuterGroup1.push_back( traceNeighbor );
          //std::array<int,TopologyCell::NTrace> traceSigns = cV.localCellGroup1[j].pGlobalAssoc->traceOrientation();
          //cV.localCellGroup1[j].localAssoc.setOrientation( traceSigns[i], i ); // copy the global Assoc trace sign over
          continue; // skip the rest of the loop
        }

        //printf("trace %d of elem %d in localCellGroup1 (group %d) is interior\n",i,cV.localCellGroup1[j].elem,cV.localCellGroup1[j].group);


        SANS_ASSERT( traceinfo.type == TraceInfo::Interior );

        // INTERIOR TRACES
        const XFieldTraceGroupType& tracegrp = global_xfld_.template getInteriorTraceGroup<TopologyTrace>(traceNeighbor.group);
        traceNeighbor.pGlobalAssoc = &(tracegrp.associativity(traceNeighbor.trace));

        setInteriorTraceAssociativity( traceNeighbor, traceNeighbor.localAssoc );

        // Checking if there's an attached element also in the local mesh
        if ( tracegrp.getElementLeft(traceNeighbor.trace) == cV.localCellGroup1[j].elem
          && tracegrp.getGroupLeft()                      == cV.localCellGroup1[j].group) // left elem is current
        {
          cellNeighbor.group  = tracegrp.getGroupRight();
          cellNeighbor.elem   = tracegrp.getElementRight( traceNeighbor.trace );

          // check if neighbour is in the maps, or if it is that is not an outerTrace member
          if ( globalToLocalCellMapping_.find(cellNeighbor) != globalToLocalCellMapping_.end()
            && globalToLocalCellMapping_.at(cellNeighbor).group != 2 )
          {
            // Left element comes before right in the local ordering
            if ( globalToLocalCellMapping_.at((GroupElemIndex)cV.localCellGroup1[j]) < globalToLocalCellMapping_.at(cellNeighbor) )
            {
              // Orientation was right construct without reversal
              traceNeighbor.localLRElems.first  = globalToLocalCellMapping_.at((GroupElemIndex) cV.localCellGroup1[j]);
              traceNeighbor.localLRElems.second = globalToLocalCellMapping_.at(cellNeighbor);

              //std::array<int,TopologyCell::NTrace> traceSigns = cV.localCellGroup1[j].pGlobalAssoc->traceOrientation();
              //cV.localCellGroup1[j].localAssoc.setOrientation( traceSigns[i], i ); // copy the global Assoc trace sign over
            }
            else if ( globalToLocalCellMapping_.at((GroupElemIndex)cV.localCellGroup1[j]) > globalToLocalCellMapping_.at(cellNeighbor) )
            {
              // Orientation was wrong construct with reversal
              std::array<int, TopologyCell::NNode > leftCellNodeMap, rightCellNodeMap;

              const XFieldCellGroupType& neighborgrp = global_xfld_.template getCellGroup<TopologyCell>(cellNeighbor.group);
              neighborgrp.associativity(cellNeighbor.elem).getNodeGlobalMapping( leftCellNodeMap.data(), leftCellNodeMap.size() );
              cellgrp.associativity(cV.localCellGroup1[j].elem).getNodeGlobalMapping( rightCellNodeMap.data(), rightCellNodeMap.size() );

              // change node maps to the local nodes
              for (int k = 0; k < TopologyCell::NNode; k++)
              {
                leftCellNodeMap[k]  = offset_nodeDOF_ + NodeDOFMap_.at( leftCellNodeMap[k] );
                rightCellNodeMap[k] = offset_nodeDOF_ + NodeDOFMap_.at(rightCellNodeMap[k] );
              }

              CanonicalTraceToCell rightCanonical =
                  orientTrace<TopologyCell,TopologyTrace>( leftCellNodeMap, rightCellNodeMap, traceNeighbor.localAssoc );

              // Update local cell associativity with the reversal
              cV.localCellGroup1[j].localAssoc.setOrientation( rightCanonical.orientation, rightCanonical.trace );

              traceNeighbor.localLRElems.second = globalToLocalCellMapping_.at((GroupElemIndex) cV.localCellGroup1[j]);
              traceNeighbor.localLRElems.first  = globalToLocalCellMapping_.at(cellNeighbor);
            }
          }
          else // outer group and oriented correctly
          {
            // Orientation was correct construct without reversal

            // if this outer element isn't already in the maps, add to the maps
            if ( globalToLocalCellMapping_.find(cellNeighbor) == globalToLocalCellMapping_.end() )
            {
              // Add the element to the forward and backward maps, though its not strictly in the local mesh
              GroupElemIndex tmpElem( 2, cnt_outerBTrace );
              localToGlobalCellMapping_.insert( std::make_pair(tmpElem, cellNeighbor) ); // cellNeighbor is plain GroupElemIndex
              globalToLocalCellMapping_.insert( std::make_pair(cellNeighbor, tmpElem) );
              cnt_outerBTrace++;
            }

            traceNeighbor.localLRElems.first  = globalToLocalCellMapping_.at((GroupElemIndex) cV.localCellGroup1[j]);
            traceNeighbor.localLRElems.second = globalToLocalCellMapping_.at(cellNeighbor);

            //std::array<int,TopologyCell::NTrace> traceSigns = cV.localCellGroup1[j].pGlobalAssoc->traceOrientation();
            //cV.localCellGroup1[j].localAssoc.setOrientation( traceSigns[i], i ); // copy the global Assoc trace sign over
          }
        }
        else if ( tracegrp.getElementRight(traceNeighbor.trace) == cV.localCellGroup1[j].elem
               && tracegrp.getGroupRight()                      == cV.localCellGroup1[j].group) // Current elem is to the right
        {
          cellNeighbor.group  = tracegrp.getGroupLeft();
          cellNeighbor.elem   = tracegrp.getElementLeft( traceNeighbor.trace );

          // check if neighbour is in the local mesh
          if ( globalToLocalCellMapping_.find(cellNeighbor) != globalToLocalCellMapping_.end()
            && globalToLocalCellMapping_.at(cellNeighbor).group != 2 )
          {
            // Left element comes before right in the local ordering
            if ( globalToLocalCellMapping_.at(cellNeighbor) < globalToLocalCellMapping_.at((GroupElemIndex)cV.localCellGroup1[j]) )
            {
              // Orientation was right construct without reversal
              traceNeighbor.localLRElems.first  = globalToLocalCellMapping_.at(cellNeighbor);
              traceNeighbor.localLRElems.second = globalToLocalCellMapping_.at((GroupElemIndex) cV.localCellGroup1[j]);

              //std::array<int,TopologyCell::NTrace> traceSigns = cV.localCellGroup1[j].pGlobalAssoc->traceOrientation();
              //cV.localCellGroup1[j].localAssoc.setOrientation( traceSigns[i], i ); // copy the global Assoc trace sign over
            }
            else if ( globalToLocalCellMapping_.at(cellNeighbor) > globalToLocalCellMapping_.at((GroupElemIndex)cV.localCellGroup1[j]) )
            {
              // Orientation was wrong construct with reversal
              std::array<int, TopologyCell::NNode > leftCellNodeMap;

              cellgrp.associativity(cV.localCellGroup1[j].elem).getNodeGlobalMapping( leftCellNodeMap.data(), leftCellNodeMap.size() );

              // change node maps to the local nodes
              for (int k = 0; k < TopologyCell::NNode; k++)
                leftCellNodeMap[k]  = offset_nodeDOF_ + NodeDOFMap_.at( leftCellNodeMap[k] );

              orientTrace<TopologyCell,TopologyTrace>( leftCellNodeMap, traceNeighbor.localAssoc );

              // Update local cell associativity with the reversal
              cV.localCellGroup1[j].localAssoc.setOrientation( +1, i );

              traceNeighbor.localLRElems.second = globalToLocalCellMapping_.at(cellNeighbor);
              traceNeighbor.localLRElems.first  = globalToLocalCellMapping_.at((GroupElemIndex) cV.localCellGroup1[j]);
            }
          }
          else // it's in outer group and oriented incorrectly
          {
            // Orientation was wrong construct with reversal
            std::array<int, TopologyCell::NNode > leftCellNodeMap;

            cellgrp.associativity(cV.localCellGroup1[j].elem).getNodeGlobalMapping( leftCellNodeMap.data(), leftCellNodeMap.size() );

            // change node maps to the local nodes
            for (int k = 0; k < TopologyCell::NNode; k++)
              leftCellNodeMap[k]  = offset_nodeDOF_ + NodeDOFMap_.at( leftCellNodeMap[k] );

            orientTrace<TopologyCell,TopologyTrace>( leftCellNodeMap, traceNeighbor.localAssoc );

            // Update local cell associativity with the reversal
            cV.localCellGroup1[j].localAssoc.setOrientation( +1, i );

            if ( globalToLocalCellMapping_.find(cellNeighbor) == globalToLocalCellMapping_.end() )
            {
              // Add the element to the forward and backward maps, though not strictly in the local mesh
              GroupElemIndex tmpElem( 2, cnt_outerBTrace );
              localToGlobalCellMapping_.insert( std::make_pair(tmpElem, cellNeighbor) ); // cellNeighbor is plain GroupElemIndex
              globalToLocalCellMapping_.insert( std::make_pair(cellNeighbor, tmpElem) );
              cnt_outerBTrace++;
            }

            traceNeighbor.localLRElems.first  = globalToLocalCellMapping_.at((GroupElemIndex) cV.localCellGroup1[j]);
            traceNeighbor.localLRElems.second = globalToLocalCellMapping_.at(cellNeighbor);
          }
        }
        else
          SANS_DEVELOPER_EXCEPTION("Neither neighbour of the trace is the starting element, "
                                   "something went wrong");

        bool inCellGroup0 = std::find(cV.localCellGroup0.begin(),    cV.localCellGroup0.end(),   cellNeighbor)  != cV.localCellGroup0.end();
        bool inCellGroup1 = std::find(cV.localCellGroup1.begin(),    cV.localCellGroup1.end(),   cellNeighbor)  != cV.localCellGroup1.end();
        bool inTraceGroup1 = std::find(cV.localITraceGroup1.begin(), cV.localITraceGroup1.end(), traceNeighbor) != cV.localITraceGroup1.end();

        // checking if this neighbour is in cV.localCellGroup1, and the trace is not already added
        if ( inCellGroup1 && !inTraceGroup1 )
        {
          cV.localITraceGroup1.push_back( traceNeighbor ); // internal to group 1
          continue;
        }

        // checking that also doesn't connect to anything in group 0 or group 1, if not must be in outer group
        if ( !inCellGroup0 && !inCellGroup1 )
          cV.localBTraceOuterGroup1.push_back( traceNeighbor ); // connecting group 1 to rest of mesh
      }
    }
  }
}


template< class PhysDim, class TopoDim >
template< class TopologyCell, class TopologyTrace >
void
XField_Local_Common<PhysDim, TopoDim>::allocateMesh( const ConstructorVectorTopo<TopoDim,TopologyCell,TopologyTrace>& cV )
{
  /*
   * The vector structures passed in now have all of the correctly constructed associativities to create the new mesh.
   * These were either just extracted from the global mesh (accompanied by some edge reversals), or they were first extracted
   * then modified by split which reorganizes the associativies in order to include the various new edges and
   */

  // ------------------------------------ RESIZE THE ACTUAL GRID ------------------------------------------------

  //Clear current local grid
  this->deallocate();

  //Count DOFs in local mesh, only working with linear mesh
  int nDOF_local = (int)NodeDOFMap_.size();

  //Create local XField
  this->resizeDOF(nDOF_local); // the additional DOF on the split mesh is added by split

  // Figure out how many cell, interior and boundary groups there are before the DOFs start getting assigned
  nCellGroups_=0;
  if (!cV.localCellGroup0.empty())
    nCellGroups_++;
  if (!cV.localCellGroup1.empty())
    nCellGroups_++;

  this->resizeCellGroups(nCellGroups_);

  nInteriorTraceGroups_ = 0;
  if (!cV.localITraceGroup0to1.empty())
  {
    // std::cout << "ITrace0to1 not empty" << std::endl;
    nInteriorTraceGroups_++;
  }
  if (!cV.localITraceGroup1.empty())
  {
    // std::cout << "ITrace1 not empty" << std::endl;
    nInteriorTraceGroups_++;
  }
  if (!cV.localITraceGroup0.empty())
  {
    // std::cout << "ITrace0 not empty" << std::endl;
    nInteriorTraceGroups_++;
  }

  this->resizeInteriorTraceGroups(nInteriorTraceGroups_);

  nBoundaryTraceGroups_ = 0;
  if (!cV.localBTraceGroup1.empty())
    nBoundaryTraceGroups_ += (int) cV.localBTraceGroup1.size(); // one group for each original boundary element
  if (!cV.localBTraceGroup0.empty())
    nBoundaryTraceGroups_ += (int) cV.localBTraceGroup0.size(); // one group for each original boundary element

  this->resizeBoundaryTraceGroups(nBoundaryTraceGroups_); // this is an upper bound

  // will resize down to the actual number of boundary groups afterwards

  //Add ghost-boundarytrace groups if there are outer traces
  nGhostBoundaryTraceGroups_ = 0;
  if (!cV.localBTraceOuterGroup1.empty())
    nGhostBoundaryTraceGroups_ ++; // one for all (and all for one)

  this->resizeGhostBoundaryTraceGroups(nGhostBoundaryTraceGroups_);

  //Zero out mesh DOFs
  for (int k = 0; k < this->nDOF_; k++)
    this->DOF_[k] = 0.0;

  // --------------------- CELL GROUPS ---------------------
  // nCellGroups is incremented in the function calls
  int cnt_cellGroups = 0;
  extractCellGroup<TopologyCell>(cV.localCellGroup0, cnt_cellGroups );

  if (!cV.localCellGroup1.empty() )
    extractCellGroup<TopologyCell>(cV.localCellGroup1, cnt_cellGroups );

  SANS_ASSERT( cnt_cellGroups == nCellGroups_ ); // check that all were accounted for

  // --------------------- INTERIOR TRACE GROUPS ---------------------
  // internal to group0 comes last because it is the one that gets modified, and if not there will need to be created
  // incremented in the extract function
  int cnt_ITraceGroups = 0;

  // interior trace from group 0 to group 1
  if ( !cV.localITraceGroup0to1.empty() )
  {
    extractITraceGroup<TopologyCell,TopologyTrace>( cV.localITraceGroup0to1, cV.localCellGroup0, cV.localCellGroup1, cnt_ITraceGroups );
    idxITraceGroup0to1_ = cnt_ITraceGroups; // the index used to end loops in computeReSolveGroups()
  }

  // internal interior trace group for cellGroup 0
  if ( !cV.localITraceGroup0.empty() )
  {
    extractITraceGroup<TopologyCell,TopologyTrace>( cV.localITraceGroup0, cV.localCellGroup0, cV.localCellGroup0, cnt_ITraceGroups );
    idxITraceGroup0_ = cnt_ITraceGroups; // the index used to end loops in computeReSolveGroups()
  }

  // internal interior trace group for cellGroup 1
  if ( !cV.localITraceGroup1.empty() )
  {
    extractITraceGroup<TopologyCell,TopologyTrace>( cV.localITraceGroup1, cV.localCellGroup1, cV.localCellGroup1, cnt_ITraceGroups );
    idxITraceGroup1_ = cnt_ITraceGroups; // the index used to end loops in computeReSolveGroups()
  }

  SANS_ASSERT( cnt_ITraceGroups == nInteriorTraceGroups_ ); // check that all were accounted for

  // --------------------- BOUNDARY TRACE GROUPS ---------------------

  // counter for boundary traces so far
  // gets incremented by the boundary group adding functions
  int cnt_BTraceGroups = 0;

  // main boundaries to group 0
  if (!cV.localBTraceGroup0.empty() )
  {
    extractBTraceGroup<TopologyCell,TopologyTrace>( cV.localBTraceGroup0, cV.localCellGroup0, cnt_BTraceGroups );
    idxBTraceGroup0_ = cnt_BTraceGroups; // the index used to end loops in computeReSolveGroups()
  }

  // main boundaries to group 1
  if (!cV.localBTraceGroup1.empty() )
  {
    extractBTraceGroup<TopologyCell,TopologyTrace>( cV.localBTraceGroup1, cV.localCellGroup1, cnt_BTraceGroups );
    idxBTraceGroup1_ = cnt_BTraceGroups; // the index used to end loops in computeReSolveGroups()
  }

  if (!cV.localBTraceOuterGroup1.empty() )
  {
    // There is an outer group, bundle them all in one group!
    if (neighborhood == Neighborhood::Vertex)
      extractOuterBTraceGroup<TopologyCell,TopologyTrace>( cV.localBTraceOuterGroup1,  cV.localCellGroup0 );
    else
      extractOuterBTraceGroup<TopologyCell,TopologyTrace>( cV.localBTraceOuterGroup1,  cV.localCellGroup1 );
  }

  // resize down to the actual number of boundary traces used if any could've been split
  this->resizeBoundaryTraceGroups(cnt_BTraceGroups);
  this->nBoundaryTraceGroups_ = cnt_BTraceGroups;
}

template< class PhysDim, class TopoDim >
template< class TopologyCell >
void
XField_Local_Common<PhysDim, TopoDim>::extractCellGroup( const GroupElemConstructorVector<TopoDim,TopologyCell>& localCellGroup, int& groupIndex )
{
  // This constructs the field Associativities from the Cell associativities, it does not populate the DOFs, that is done later
  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyCell> XFieldCellGroupType;
  typedef typename XFieldCellGroupType::BasisType BasisType;

  // all groups must have same cell group type as the local cell group type
  const XFieldCellGroupType& cellgrp_main = global_xfld_.template getCellGroup<TopologyCell>(localCellGroup.front().group);
  const BasisType* cell_basis = cellgrp_main.basis();

  typename XFieldCellGroupType ::FieldAssociativityConstructorType fldAssoc_CellGroup( cell_basis, localCellGroup.size() );

  for (std::size_t i = 0; i < localCellGroup.size(); i++)
  {
    // save the mapping into the int pair format used by XFieldLocalBase
    // groupIndex needed for cases where one of the cellGroups might've been empty
    this->CellMapping_[{groupIndex, (int) i}]
                       = {localCellGroup[i].group,localCellGroup[i].elem};
    // mark as an unsplit configuration
    this->CellSplitInfo_[{groupIndex, (int) i}]
                         = localCellGroup[i].elementSplitInfo;

    fldAssoc_CellGroup.setAssociativity( localCellGroup[i].localAssoc, (int) i );
  }
  // Add cell group and copy DOFs
  this->cellGroups_[groupIndex] = new XFieldCellGroupType( fldAssoc_CellGroup );
  this->cellGroups_[groupIndex]->setDOF( this->DOF_, this->nDOF_ );
  this->nElem_ += fldAssoc_CellGroup.nElem();

  groupIndex++; // increment so that next call starts at correct number
}

template< class PhysDim, class TopoDim >
template< class TopologyCell, class TopologyTrace >
void
XField_Local_Common<PhysDim, TopoDim>::extractITraceGroup(const GroupTraceConstructorVector<TopoDim,TopologyTrace>& localTraceGroup,
                                                       const GroupElemConstructorVector<TopoDim,TopologyCell>& leftCellGroup,
                                                       const GroupElemConstructorVector<TopoDim,TopologyCell>& rightCellGroup,
                                                       int& groupIndex )
{
  typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::BasisType TraceBasisType;

  const TraceBasisType* trace_basis = TraceBasisType::getBasisFunction(order_, basisCategory_);
  typename XFieldTraceGroupType::FieldAssociativityConstructorType fldAssoc_ITrace( trace_basis, localTraceGroup.size() );

  for ( std::size_t i = 0; i < localTraceGroup.size(); i++)
  {
    // save the mapping into the int pair format used by XFieldLocalBase, groupIndex needed in case a group was empty
    this->InteriorTraceMapping_[{groupIndex,(int) i }]    = { localTraceGroup[i].group, localTraceGroup[i].trace };
    // save the split config from the vector (so this all works for after splitting too)
    this->InteriorTraceSplitInfo_[{groupIndex,(int) i }]  = localTraceGroup[i].elementSplitInfo;

    // extract local grid connectivity using node maps
    CanonicalTraceToCell leftCanonical, rightCanonical;
    std::vector<int> leftCellNodeMap(TopologyCell::NNode), rightCellNodeMap(TopologyCell::NNode), traceNodeMap(TopologyTrace::NNode);
    const int leftElem = localTraceGroup[i].localLRElems.first.elem, rightElem = localTraceGroup[i].localLRElems.second.elem;

    leftCellGroup[leftElem]  .localAssoc.getNodeGlobalMapping( leftCellNodeMap.data(), leftCellNodeMap.size() );
    rightCellGroup[rightElem].localAssoc.getNodeGlobalMapping( rightCellNodeMap.data(), rightCellNodeMap.size() );
    localTraceGroup[i]       .localAssoc.getNodeGlobalMapping( traceNodeMap.data(), traceNodeMap.size() );

    leftCanonical = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyCell>::
        getCanonicalTrace( traceNodeMap.data(), traceNodeMap.size(),  leftCellNodeMap.data(),  leftCellNodeMap.size() );
    rightCanonical = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyCell>::
        getCanonicalTrace( traceNodeMap.data(), traceNodeMap.size(), rightCellNodeMap.data(), rightCellNodeMap.size() );

    // Fill the Field associativity
    fldAssoc_ITrace.setAssociativity( localTraceGroup[i].localAssoc, (int) i );
    fldAssoc_ITrace.setElementLeft( leftElem, (int) i );
    fldAssoc_ITrace.setElementRight( rightElem, (int) i );
    fldAssoc_ITrace.setCanonicalTraceLeft( leftCanonical, (int) i );
    fldAssoc_ITrace.setCanonicalTraceRight( rightCanonical, (int) i );
  }

  // Assumes that all traces have the same left and right group, which they should
  fldAssoc_ITrace.setGroupLeft (localTraceGroup.front().localLRElems.first.group);
  fldAssoc_ITrace.setGroupRight(localTraceGroup.front().localLRElems.second.group);

  this->interiorTraceGroups_[groupIndex] = new XFieldTraceGroupType( fldAssoc_ITrace );
  this->interiorTraceGroups_[groupIndex]->setDOF( this-> DOF_, this->nDOF_ );

  groupIndex++; // increment so that next call starts at correct number
}

template< class PhysDim, class TopoDim >
template< class TopologyCell, class TopologyTrace >
void
XField_Local_Common<PhysDim, TopoDim>::extractBTraceGroup( const GroupTraceConstructorVector<TopoDim,TopologyTrace>& localTraceGroup,
                                                        const GroupElemConstructorVector<TopoDim,TopologyCell>& leftCellGroup,
                                                        int& groupIndex )
{
  typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::BasisType TraceBasisType;

  const TraceBasisType* trace_basis = TraceBasisType::getBasisFunction(order_, basisCategory_);

  // shared_ptr trickery is used so that this class can be reused for the case of two members in a group, i.e. a split config
  // nGroups must be passed in so that we know the number of groups with 2 elements
  std::vector<std::shared_ptr<typename XFieldTraceGroupType::FieldAssociativityConstructorType>> fldAssoc_BTrace;

  // This relies on the fact that new elements have subcell_index 1 and come at the back of the list
  // THIS WON'T WORK -- Need something else robust to whether the edge gets added in reverse
  std::map<GroupTraceIndex,int> groupTraceMap; // for mapping from the global group trace combo, to the local group


  // Fill the vector of field Associativities
  for ( std::size_t i = 0; i < localTraceGroup.size(); i++)
  {
    if (localTraceGroup[i].elementSplitInfo.split_flag == ElementSplitFlag::Unsplit ) // This is a vanilla boundary element
    {
      // Unsplit element, behave normally
      // save the mapping into the int pair format used by XFieldLocalBase
      this->BoundaryTraceMapping_[{groupIndex + (int) i, 0}] = {localTraceGroup[i].group, localTraceGroup[i].trace};
      // save the split config from the vector (so this all works after splitting)
      this->BoundaryTraceSplitInfo_[{groupIndex + (int) i, 0 }] = localTraceGroup[i].elementSplitInfo;

      // extract local grid connectivity using node maps
      CanonicalTraceToCell leftCanonical;
      std::vector<int> leftCellNodeMap(TopologyCell::NNode), traceNodeMap(TopologyTrace::NNode);
      const int leftElem = localTraceGroup[i].localLRElems.first.elem;

      leftCellGroup[leftElem].localAssoc.getNodeGlobalMapping( leftCellNodeMap.data(), leftCellNodeMap.size() );
      localTraceGroup[i]     .localAssoc.getNodeGlobalMapping( traceNodeMap.data(), traceNodeMap.size() );

      leftCanonical = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyCell>::
              getCanonicalTrace(traceNodeMap.data(), traceNodeMap.size(), leftCellNodeMap.data(), leftCellNodeMap.size() );

      // add a new group
      fldAssoc_BTrace.push_back( std::make_shared<typename XFieldTraceGroupType::FieldAssociativityConstructorType> (trace_basis, 1 ) );
      // fill the associativity
      fldAssoc_BTrace.back()->setAssociativity( localTraceGroup[i].localAssoc, 0 );
      fldAssoc_BTrace.back()->setGroupLeft( localTraceGroup[i].localLRElems.first.group );
      fldAssoc_BTrace.back()->setElementLeft( leftElem, 0 );
      fldAssoc_BTrace.back()->setCanonicalTraceLeft( leftCanonical, 0 );
    }
    if ( localTraceGroup[i].elementSplitInfo.split_flag == ElementSplitFlag::Split )
    {
      // keep a local map to find this group again
      int j = static_cast<int>(i);
      std::pair<std::map<GroupTraceIndex,int>::iterator,bool> map_ret; //return value for map inserts
      map_ret = groupTraceMap.insert( std::make_pair( (GroupTraceIndex)localTraceGroup[i], j ) );

      if (map_ret.second==true) // only create a new trace if this is a new one
        fldAssoc_BTrace.push_back( std::make_shared<typename XFieldTraceGroupType::FieldAssociativityConstructorType> ( trace_basis, 2 ) );
      else // otherwise take the value from the map
        j = groupTraceMap.at( (GroupTraceIndex)localTraceGroup[i] );

      if ( localTraceGroup[i].elementSplitInfo.subcell_index == 0 )
      {
        // first half of a split trace
        // save the mapping into the int pair format used by XFieldLocalBase
        this->BoundaryTraceMapping_[{groupIndex + j, 0}] = {localTraceGroup[i].group, localTraceGroup[i].trace};
        // save the split config from the vector (so this all works after splitting)
        this->BoundaryTraceSplitInfo_[{groupIndex + j, 0 }] = localTraceGroup[i].elementSplitInfo;

        // extract local grid connectivity using node maps
        CanonicalTraceToCell leftCanonical;
        std::vector<int> leftCellNodeMap(TopologyCell::NNode), traceNodeMap(TopologyTrace::NNode);
        const int leftElem = localTraceGroup[i].localLRElems.first.elem;

        leftCellGroup[leftElem].localAssoc.getNodeGlobalMapping( leftCellNodeMap.data(), leftCellNodeMap.size() );
        localTraceGroup[i]     .localAssoc.getNodeGlobalMapping( traceNodeMap.data(), traceNodeMap.size() );

        leftCanonical = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyCell>::
                getCanonicalTrace(traceNodeMap.data(), traceNodeMap.size(), leftCellNodeMap.data(), leftCellNodeMap.size() );

        // fill the associativity
        fldAssoc_BTrace[j]->setAssociativity( localTraceGroup[i].localAssoc, 0 );
        fldAssoc_BTrace[j]->setGroupLeft( localTraceGroup[i].localLRElems.first.group );
        fldAssoc_BTrace[j]->setElementLeft( leftElem, 0 );
        fldAssoc_BTrace[j]->setCanonicalTraceLeft( leftCanonical, 0 );
      }
      else if ( localTraceGroup[i].elementSplitInfo.subcell_index == 1 )
      {
        /// second half of a split trace

        // second sub_cell index, the shared ptr must have been allocated by now, elem 0 come before 1 in the vector structs
        SANS_ASSERT_MSG( fldAssoc_BTrace[j], "fldAssoc_BTrace[%d] has not been assigned yet", j ); // nullptr gives false

        // save the mapping into the int pair format used by XFieldLocalBase
        this->BoundaryTraceMapping_  [{groupIndex + j, 1 }] = {localTraceGroup[j].group, localTraceGroup[j].trace}; // global trace info from before
        // save the split config from the vector (so this all works after splitting)
        this->BoundaryTraceSplitInfo_[{groupIndex + j, 1 }] = localTraceGroup[i].elementSplitInfo; // current split info

        // extract local grid connectivity using node maps
        CanonicalTraceToCell leftCanonical;
        std::vector<int> leftCellNodeMap(TopologyCell::NNode), traceNodeMap(TopologyTrace::NNode);
        const int leftElem = localTraceGroup[i].localLRElems.first.elem;

        leftCellGroup[leftElem].localAssoc.getNodeGlobalMapping( leftCellNodeMap.data(), leftCellNodeMap.size() );
        localTraceGroup[i]     .localAssoc.getNodeGlobalMapping( traceNodeMap.data(), traceNodeMap.size() );

        leftCanonical = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyCell>::
                getCanonicalTrace(traceNodeMap.data(), traceNodeMap.size(), leftCellNodeMap.data(), leftCellNodeMap.size() );

        // fill the associativity
        fldAssoc_BTrace[j]->setAssociativity( localTraceGroup[i].localAssoc, 1 );
        fldAssoc_BTrace[j]->setElementLeft( leftElem, 1 );
        fldAssoc_BTrace[j]->setCanonicalTraceLeft( leftCanonical, 1 );
      }
      else
        SANS_DEVELOPER_EXCEPTION("Neither a 0 nor 1 split configuration, something went wrong ");

    }
  }


  // populate the mesh using the field associativities
  for ( std::size_t i = 0; i < fldAssoc_BTrace.size(); i++ )
  {
    this->boundaryTraceGroups_[groupIndex + (int) i] = new XFieldTraceGroupType( *fldAssoc_BTrace[i] );
    this->boundaryTraceGroups_[groupIndex + (int) i]->setDOF( this->DOF_, this->nDOF_ );
  }

  groupIndex += (int) fldAssoc_BTrace.size(); // Increment the init value so that next call starts from the right place
}

template< class PhysDim, class TopoDim >
template< class TopologyCell, class TopologyTrace >
void
XField_Local_Common<PhysDim, TopoDim>::extractOuterBTraceGroup( const GroupTraceConstructorVector<TopoDim,TopologyTrace>& localTraceGroup,
                                                                const GroupElemConstructorVector<TopoDim,TopologyCell>& leftCellGroup)
{
  // These never get split by construction, so we can take short cuts compared to the other boundary traces,
  // to repeat the phrase, they're just interior traces in disguise

  typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::BasisType TraceBasisType;

  const TraceBasisType* trace_basis = TraceBasisType::getBasisFunction(order_, basisCategory_);
  typename XFieldTraceGroupType::FieldAssociativityConstructorType fldAssoc_BTrace( trace_basis, localTraceGroup.size() );

  for ( std::size_t i = 0; i < localTraceGroup.size(); i++)
  {
  #if 0 //We no longer need these mappings for outer btraces since they are now ghosts. Leaving this code ifdef'd out for now..
      // save the mapping into the int pair format used by the XField_Local_Base
      this->BoundaryTraceMapping_[{groupIndex, (int) i}]
            = std::make_pair( localTraceGroup[i].group, localTraceGroup[i].trace);
      // save the split config from the vector
      this->BoundaryTraceSplitInfo_[{groupIndex, (int) i}] = localTraceGroup[i].elementSplitInfo;
  #endif

    // extract local grid connectivity using node maps
    CanonicalTraceToCell leftCanonical;
    std::vector<int> leftCellNodeMap(TopologyCell::NNode), traceNodeMap(TopologyTrace::NNode);
    const int leftElem = localTraceGroup[i].localLRElems.first.elem;

    leftCellGroup[leftElem].localAssoc.getNodeGlobalMapping( leftCellNodeMap.data(), leftCellNodeMap.size() );
    localTraceGroup[i].localAssoc.getNodeGlobalMapping( traceNodeMap.data(), traceNodeMap.size() );

    leftCanonical = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyCell>::
            getCanonicalTrace(traceNodeMap.data(), traceNodeMap.size(), leftCellNodeMap.data(), leftCellNodeMap.size() );

    fldAssoc_BTrace.setAssociativity( localTraceGroup[i].localAssoc, (int) i );
    fldAssoc_BTrace.setElementLeft( leftElem, (int) i );
    fldAssoc_BTrace.setCanonicalTraceLeft( leftCanonical, (int) i );
  }

  // Assumes that all traces have the same left and right group, which they should
  fldAssoc_BTrace.setGroupLeft(localTraceGroup.front().localLRElems.first.group);

  // Only ever one ghost boundary trace group
  this->ghostBoundaryTraceGroups_[0] = new XFieldTraceGroupType( fldAssoc_BTrace );
  this->ghostBoundaryTraceGroups_[0]->setDOF( this->DOF_, this->nDOF_ );

}

template <class PhysDim, class TopoDim>
template <class TopologyCell>
void
XField_Local_Common<PhysDim, TopoDim>::projectGroup( GroupElemConstructorVector<TopoDim,TopologyCell>& localCellGroup, const int cellGroup  )
{

  typedef typename BasisFunctionTopologyBase<TopoDim, TopologyCell>::type BasisType;
  const BasisType* basis = this->template getCellGroup<TopologyCell>(0).basis();
  Element_Subdivision_Projector<TopoDim,TopologyCell> elemProjector(basis);

  for (std::size_t i = 0; i < localCellGroup.size(); i++)
    projectElem<TopologyCell>(localCellGroup[i], cellGroup, (int)i, elemProjector);

}

template <class PhysDim, class TopoDim>
template <class TopologyCell>
void
XField_Local_Common<PhysDim, TopoDim>::projectElem(const GroupElemIndex& main_elem, const int sub_group, const int sub_elem,
                                               Element_Subdivision_Projector<TopoDim, TopologyCell>& projector)
{
  // Performs the work of projecting split elements and higher order elements off of the original grid
  // main_elem is the group and elem for the main mesh, and sub_elem is the group and elem in the local mesh

  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyCell> XFieldCellGroupType;
  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef typename XFieldCellGroupType::BasisType BasisType;

  const XFieldCellGroupType& cellgrp_main = global_xfld_.template getCellGroup<TopologyCell>(main_elem.group);

  const BasisType* cell_basis = cellgrp_main.basis();

  ElementXFieldClass xfldElem_main( cell_basis ); // Unsplit element
  ElementXFieldClass xfldElem_sub ( cell_basis ); // One of the split elements

  cellgrp_main.getElement(xfldElem_main, main_elem.elem); // pulled from the original global xfld

  ElementSplitInfo splitinfo = this->getCellSplitInfo({sub_group,sub_elem}); // cell split info is indexed by local grid info

  if (splitinfo.split_flag == ElementSplitFlag::Split )
  {
    projector.project(xfldElem_main, xfldElem_sub, splitinfo.split_type, splitinfo.edge_index, splitinfo.subcell_index);
    this->template getCellGroup<TopologyCell>(sub_group).setElement(xfldElem_sub,sub_elem);
  }
  else if ( splitinfo.split_flag == ElementSplitFlag::Unsplit)
  {
    this->template getCellGroup<TopologyCell>(sub_group).setElement(xfldElem_main, sub_elem);
  }
  else
    SANS_DEVELOPER_EXCEPTION( "XField_Local_Common<PhysDim, TopoDim, T>::projectElem - ElementSplitFlag should be either 'Split' or 'Unsplit'." );

}

template <class PhysDim, class TopoDim>
template <class TopoCell>
void
XField_Local_Common<PhysDim, TopoDim>::setCellAssociativity(const GroupElemConstructorIndex<TopoDim,TopoCell>& elem,
                                                         ElementAssociativityConstructor<TopoDim,TopoCell>& DOFAssoc)
{
  int nNodeDOF = elem.pGlobalAssoc->nNode();

  // node DOF, edge DOF and cell DOF ordering for cell
  std::vector<int> Cell_nodeDOF_map(nNodeDOF);

  elem.pGlobalAssoc->getNodeGlobalMapping( Cell_nodeDOF_map ); //Get nodeDOF map from global associativity

  //Map DOFs from global to local mesh
  for (int k = 0; k < nNodeDOF; k++)
    Cell_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Cell_nodeDOF_map[k]);

  DOFAssoc.setRank( elem.pGlobalAssoc->rank() );
  DOFAssoc.setNodeGlobalMapping( Cell_nodeDOF_map );

  DOFAssoc.traceOrientation() = elem.pGlobalAssoc->traceOrientation();
}

template<class PhysDim, class TopoDim>
template< class TopologyTrace>
void
XField_Local_Common<PhysDim, TopoDim>::
setInteriorTraceAssociativity(const GroupTraceConstructorIndex<TopoDim,TopologyTrace>& trace,
                              ElementAssociativityConstructor<typename TopoDim::TopoDTrace,TopologyTrace>& DOFAssoc)
{
  int nNodeDOF = trace.pGlobalAssoc->nNode();

  // node DOF and edge DOF ordering for interior trace
  std::vector<int> Trace_nodeDOF_map(nNodeDOF);

  trace.pGlobalAssoc->getNodeGlobalMapping( Trace_nodeDOF_map ); //Get nodeDOF map from global field

  //Map DOFs from global to local mesh
  for (int k = 0; k < nNodeDOF; k++)
    Trace_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Trace_nodeDOF_map[k]);

  DOFAssoc.setRank( trace.pGlobalAssoc->rank() );
  DOFAssoc.setNodeGlobalMapping( Trace_nodeDOF_map );
}

template<class PhysDim, class TopoDim>
template<class TopologyTrace>
void
XField_Local_Common<PhysDim, TopoDim>::
setBoundaryTraceAssociativity(const GroupTraceConstructorIndex<TopoDim,TopologyTrace>& trace,
                              ElementAssociativityConstructor<typename TopoDim::TopoDTrace,TopologyTrace>& DOFAssoc)
{
  int nNodeDOF = trace.pGlobalAssoc->nNode();

  // node DOF and edge DOF ordering for boundary trace
  std::vector<int> Trace_nodeDOF_map(nNodeDOF);

  trace.pGlobalAssoc->getNodeGlobalMapping( Trace_nodeDOF_map ); //Get nodeDOF map from global field

  //Map DOFs from global to local mesh
  for (int k = 0; k < nNodeDOF; k++)
    Trace_nodeDOF_map[k] = offset_nodeDOF_ + NodeDOFMap_.at(Trace_nodeDOF_map[k]);

  DOFAssoc.setRank( trace.pGlobalAssoc->rank() );
  DOFAssoc.setNodeGlobalMapping( Trace_nodeDOF_map );
}

template< class PhysDim, class TopoDim >
template< class TopologyCell, class TopologyTrace >
CanonicalTraceToCell
XField_Local_Common<PhysDim, TopoDim>::orientTrace( const std::array<int,TopologyCell::NNode>& leftCellNodeMap,
                                                 const std::array<int,TopologyCell::NNode>& rightCellNodeMap,
                                                 typename ElementAssociativity<typename TopoDim::TopoDTrace, TopologyTrace>::Constructor& DOFAssoc)
{
  // Reverses a trace associativity, returns the CanonicalTraceToCell of the right element, the left is always +1 orientation
  // The node maps need to be using the same node scheme as DOFAssoc, namely the local

  std::array<int, TopologyTrace::NNode> traceNodeMap, newTraceNodeMap;
  CanonicalTraceToCell leftCanonical, rightCanonical;

  DOFAssoc.getNodeGlobalMapping( traceNodeMap.data(), traceNodeMap.size() );

  leftCanonical = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyCell>
                ::getCanonicalTraceLeft( traceNodeMap.data(), traceNodeMap.size(),
                                         leftCellNodeMap.data(), leftCellNodeMap.size(),
                                         newTraceNodeMap.data(), newTraceNodeMap.size() );

  rightCanonical = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyCell>
                 ::getCanonicalTrace( newTraceNodeMap.data(), newTraceNodeMap.size(),
                                      rightCellNodeMap.data(), rightCellNodeMap.size() );

  DOFAssoc.setNodeGlobalMapping( newTraceNodeMap.data(), newTraceNodeMap.size() );

  return rightCanonical;
}

template< class PhysDim, class TopoDim >
template< class TopologyCell, class TopologyTrace >
void
XField_Local_Common<PhysDim, TopoDim>::orientTrace( const std::array<int,TopologyCell::NNode>& leftCellNodeMap,
                                                 typename ElementAssociativity<typename TopoDim::TopoDTrace, TopologyTrace>::Constructor& DOFAssoc)
{
  // Reverses a trace associativity, returns the CanonicalTraceToCell of the right element, the left is always +1 orientation
  // The node maps need to be using the same node scheme as DOFAssoc, namely the local

  std::array<int, TopologyTrace::NNode> traceNodeMap, newTraceNodeMap;
  CanonicalTraceToCell leftCanonical;

  DOFAssoc.getNodeGlobalMapping( traceNodeMap.data(), traceNodeMap.size() );

  // gives the canonical trace info from leftCellNodeMap that corresponds to traceNodeMap
  leftCanonical = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyCell>
                ::getCanonicalTraceLeft( traceNodeMap.data(), traceNodeMap.size(),
                                         leftCellNodeMap.data(), leftCellNodeMap.size(),
                                         newTraceNodeMap.data(), newTraceNodeMap.size() );

  DOFAssoc.setNodeGlobalMapping( newTraceNodeMap.data(), newTraceNodeMap.size() );

}


}

#endif // XFIELD_LOCAL_COMMON_H
