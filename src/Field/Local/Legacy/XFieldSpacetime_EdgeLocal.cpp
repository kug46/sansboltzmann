// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define XFIELD_EDGELOCAL_INSTANTIATE

#include "XFieldSpacetime.h"
#include <iomanip>
#include <algorithm>
#include "XFieldSpacetime.h"
#include "Field/Element/ElementAssociativityVolume.h"
#include "Field/Element/ElementAssociativitySpacetime.h"
#include "XField_EdgeLocal_impl.h"


namespace SANS
{

// Specialized Constructor for TopoD4

template< class PhysDim, class TopoDim >
XField_EdgeLocal<PhysDim,TopoDim>::XField_EdgeLocal(mpi::communicator& comm_local,
                                                    const XField_CellToTrace<PhysDim, TopoDim>& connectivity,
                                                    const Field_NodalView& nodalView, std::pair<int,int> nodes,
                                                    const Neighborhood neighborhood, const bool isSplit )
:
  BaseType(comm_local, connectivity, nodalView, neighborhood)
{
  for (int group = 0; group < this->global_xfld_.nCellGroups(); group++)
  {
    if ( this->global_xfld_.getCellGroupBase(group).topoTypeID() == typeid(Pentatope) )
    {
      typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Pentatope> XFieldCellGroupType;
      typedef typename XFieldCellGroupType::BasisType BasisType;

      const XFieldCellGroupType& cellgrp = this->global_xfld_.template getCellGroup<Pentatope>(group);
      const BasisType* cell_basis = cellgrp.basis();
      orderVector_.push_back(cell_basis->order());
    }
  }
  if ( std::adjacent_find( orderVector_.begin(), orderVector_.end(), std::not_equal_to<int>() ) != orderVector_.end() )
    SANS_DEVELOPER_EXCEPTION("Mixed order meshes are unsupported");


  // having now checked all groups are same topology and same order, proceed
  this->basisCategory_ = this->global_xfld_.getCellGroupBase(0).basisCategory();

  // Clear the maps
  NodeDOFMap_.clear();
  EdgeDOFMap_.clear();
  CellDOFMap_.clear();

  // Fill the vector structures and then use them to construct the grid
  if (  this->global_xfld_.getCellGroupBase(0).topoTypeID() == typeid(Pentatope) )
  {
    // GroupElemConstructorVector<Tet> localCellGroup0, localCellGroup1;
    // GroupTraceConstructorVector<TopoDim,Triangle> localITraceGroup0, localITraceGroup0to1, localITraceGroup1;
    // GroupTraceConstructorVector<TopoDim,Triangle> localBTraceGroup0, localBTraceGroup1, localBTraceOuterGroup;
    this->p_constructorVector_ = std::make_shared< ConstructorVectorTopo<TopoDim,Pentatope,Tet> >();

    ConstructorVectorTopo<TopoDim,Pentatope,Tet> *pconstructorVectors
    = static_cast<ConstructorVectorTopo<TopoDim,Pentatope,Tet>*>(this->p_constructorVector_.get());


    extractEdgeLocalGrid<Pentatope,Tet>( nodes,
                                        *pconstructorVectors );

    if (isSplit)
    {
      split<Pentatope,Tet>(*pconstructorVectors);
    }

    // build higher order mesh from the mesh if necessary
    if (orderVector_[0] > 1) // Higher order mesh required
    {
      // This copy constructor is doing all the hard work, can't modify *this in place
      XField_EdgeLocal<PhysDim,TopoDim> xfld_local_linear( *this, FieldCopy() );
      this->buildFrom(xfld_local_linear,orderVector_[0]);
    }

    BaseType::template allocateMesh<Pentatope,Tet>(*pconstructorVectors);

    BaseType::template projectGroup( pconstructorVectors->localCellGroup0, 0 );
    BaseType::template projectGroup( pconstructorVectors->localCellGroup1, 1 );

  }

  // This is checkGrid() but gets removed in release mode!
  this->checkGrid();

  this->computeReSolveGroups(); // compute the re-solve groups
}

// THIS IS TOPOD3 specific
template <class PhysDim, class TopoDim>
template <class TopologyCell, class TopologyTrace>
void
XField_EdgeLocal<PhysDim, TopoDim>::split( ConstructorVectorTopo<TopoDim,TopologyCell,TopologyTrace>& cV )
{
  //Add new nodeDOF which will be added to the edge, currently the new node is -1, this is not good
  std::pair<std::map<int,int>::iterator,bool> map_ret =
       NodeDOFMap_.insert( std::pair<int,int>(-1,cntNodeDOF_) );
  SANS_ASSERT( map_ret.second==true );
  this->newNodeDOFs_.push_back(cntNodeDOF_);
  const int newNode = cntNodeDOF_ ;

  const int startNode = 0, endNode = 1;

  cntNodeDOF_++; //increment counter, this node will definitely be used

  // This actually does the splitting
  BaseType::split( startNode, endNode, newNode, cV );
}




//Explicit instantiations
template class XField_EdgeLocal<PhysD4,TopoD4>;

}
