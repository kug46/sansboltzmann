// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XFieldArea_Local_Split.h"

#include "XFieldArea.h"
#include "XFieldArea_Local_Split_Linear.h"

namespace SANS
{

template <class PhysDim>
XField_Local_Split<PhysDim, TopoD2>::XField_Local_Split(const XField_Local<PhysDim, TopoD2>& xfld_local,
                                                        XField_CellToTrace<PhysDim,TopoD2>& connectivity,
                                                        ElementSplitType split_type, int split_edge_index) :
  BaseType(xfld_local.comm()), xfld_local_unsplit_(xfld_local), connectivity_(connectivity)
{
  XField_Local_Split_Linear<PhysDim,TopoD2> xfld_split_local_linear(xfld_local, connectivity, split_type, split_edge_index);

  getOrder(); //Get the basis order for each cell group

  //Build a higher-order split local mesh if required
  if (orderVector_[0]==1)
    this->BaseType::cloneFrom(xfld_split_local_linear);
  else
    this->buildFrom(xfld_split_local_linear,orderVector_[0]);

  copyInfo(xfld_split_local_linear); //Copying some variables from the linear split mesh, before it goes out of scope.

  projectMainCell(split_type, split_edge_index); //Project DOFs from unsplit local mesh to split mesh

  // This is checkGrid() but gets removed in release mode!
  this->checkGrid();

  for (int i = 0; i < this->nBoundaryTraceGroups(); i++)
    reSolveBoundaryTraceGroups_.push_back(i);

  for (int i = 0; i < this->nInteriorTraceGroups(); i++)
    reSolveInteriorTraceGroups_.push_back(i);
}


template <class PhysDim>
void
XField_Local_Split<PhysDim, TopoD2>::getOrder()
{
  orderVector_.clear();

  for (int group=0; group<xfld_local_unsplit_.nCellGroups(); group++)
  {

    if ( xfld_local_unsplit_.getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      typedef typename XField<PhysDim, TopoD2>::template FieldCellGroupType<Triangle> XFieldCellGroupType;
      typedef typename XFieldCellGroupType::BasisType BasisType;

      const XFieldCellGroupType& cellgrp = xfld_local_unsplit_.template getCellGroup<Triangle>(group);
      const BasisType* cell_basis = cellgrp.basis();
      orderVector_.push_back(cell_basis->order());
    }
    else if ( xfld_local_unsplit_.getCellGroupBase(group).topoTypeID() == typeid(Quad) )
    {
      typedef typename XField<PhysDim, TopoD2>::template FieldCellGroupType<Quad> XFieldCellGroupType;
      typedef typename XFieldCellGroupType::BasisType BasisType;

      const XFieldCellGroupType& cellgrp = xfld_local_unsplit_.template getCellGroup<Quad>(group);
      const BasisType* cell_basis = cellgrp.basis();
      orderVector_.push_back(cell_basis->order());
    }
    else
      SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD2, T>::getOrder - Unknown cell topology." );
  }
}

template <class PhysDim>
void
XField_Local_Split<PhysDim, TopoD2>::copyInfo(XField_Local_Split_Linear<PhysDim,TopoD2>& xfld_split_local_linear)
{

  this->CellMapping_ = xfld_split_local_linear.CellMapping_;
  this->InteriorTraceMapping_ = xfld_split_local_linear.InteriorTraceMapping_;
  this->BoundaryTraceMapping_ = xfld_split_local_linear.BoundaryTraceMapping_;

  this->CellSplitInfo_ = xfld_split_local_linear.CellSplitInfo_;
  this->InteriorTraceSplitInfo_ = xfld_split_local_linear.InteriorTraceSplitInfo_;
  this->BoundaryTraceSplitInfo_ = xfld_split_local_linear.BoundaryTraceSplitInfo_;

  SplitTraceIndex_ = xfld_split_local_linear.SplitTraceIndex_;
  IsotropicSplitFlag_ = xfld_split_local_linear.IsotropicSplitFlag_;

  CellGroupTriNeighbors_ = xfld_split_local_linear.CellGroupTriNeighbors_;
  CellGroupQuadNeighbors_ = xfld_split_local_linear.CellGroupQuadNeighbors_;

  this->newNodeDOFs_ = xfld_split_local_linear.getNewNodeDOFs();
  //offset the nodeDOF indices to be the last (after cell and edge DOFs)
  for (std::size_t i = 0; i < this->newNodeDOFs_.size(); i++)
    this->newNodeDOFs_[i] += this->nDOF() - xfld_split_local_linear.nDOF();
}

template <class PhysDim>
void
XField_Local_Split<PhysDim, TopoD2>::projectMainCell(ElementSplitType split_type, int split_edge_index)
{

  int main_group = 0;
  int main_elem = 0;

  if ( xfld_local_unsplit_.getCellGroupBase(main_group).topoTypeID() == typeid(Triangle) )
  {
    typedef typename BasisFunctionTopologyBase<TopoD2, Triangle>::type BasisType;
    const BasisType* basis = xfld_local_unsplit_.template getCellGroup<Triangle>(main_group).basis();
    Element_Subdivision_Projector<TopoD2, Triangle> elemProjector(basis);

    if (IsotropicSplitFlag_)
    {
      //Loop across the sub-elements
      for (int sub_elem = 0; sub_elem < 4; sub_elem++)
        projectElem<Triangle>(main_group, main_elem, main_group, sub_elem, elemProjector);
    }
    else
    {
      //Loop across the sub-elements
      for (int sub_elem = 0; sub_elem < 2; sub_elem++)
        projectElem<Triangle>(main_group, main_elem, main_group, sub_elem, elemProjector);
    }

    projectNeighbors(Triangle::NTrace, orderVector_[0], split_type, split_edge_index);
  }
  else if ( xfld_local_unsplit_.getCellGroupBase(main_group).topoTypeID() == typeid(Quad) )
  {
    SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD2, T>::projectMainCell - Quad splitting not supported yet.");

#if 0
    typedef typename BasisFunctionTopologyBase<TopoD2, Quad>::type BasisType;
    const BasisType* basis = xfld_local_unsplit_.template getCellGroup<Quad>(main_group).basis();
    Element_Subdivision_Projector<TopoD2, Quad> elemProjector(basis);

    if (IsotropicSplitFlag_)
    {
      //Loop across the sub-elements
      for (int sub_elem = 0; sub_elem < 4; sub_elem++)
        projectElem<Quad>(main_group, main_elem, main_group, sub_elem, elemProjector);
    }
    else
    {
      //Loop across the sub-elements
      for (int sub_elem = 0; sub_elem < 2; sub_elem++)
        projectElem<Quad>(main_group, main_elem, main_group, sub_elem, elemProjector);
    }

    projectNeighbors(Quad::NTrace, orderVector_[0], split_type);
#endif
  }
  else
    SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD2, T>::projectMainCell - Unknown cell topology." );

}

template <class PhysDim>
void
XField_Local_Split<PhysDim, TopoD2>::projectNeighbors(int Ntrace, int order, ElementSplitType split_type, int split_edge_index)
{

  typedef typename XField<PhysDim, TopoD2>::template FieldTraceGroupType<Line> XFieldTraceGroupType;

  int main_group = 0;
  int main_elem = 0;

  int cnt_neighbor_tri = 0; //counter to keep track of triangles whose DOFs have been projected
  int cnt_neighbor_quad = 0; //counter to keep track of quads whose DOFs have been projected

  //-----------Fill in associativity for neighboring cells--------------------

  //Repeat loop across neighboring elements to fill associativity
  for (int itrace = 0; itrace < Ntrace; itrace++)
  {
    //Get indices of each neighboring trace (and its cellgroup)
    const TraceInfo& traceinfo = connectivity_.getTrace(main_group, main_elem, itrace);

    if (traceinfo.type == TraceInfo::Interior)
    {
      const XFieldTraceGroupType& tracegrp = xfld_local_unsplit_.template getInteriorTraceGroup<Line>(traceinfo.group);

      int neighbor_group = -1; //cellgroup of neighboring element in global mesh
      int neighbor_elem = -1; //element index of neighboring element in global mesh

      if (tracegrp.getElementLeft(traceinfo.elem) == main_elem)
      { //element to left of trace is the "main_elem" => getElementRight gives the neighboring cell
        neighbor_group          = tracegrp.getGroupRight();
        neighbor_elem           = tracegrp.getElementRight(traceinfo.elem);
      }
      else
        SANS_DEVELOPER_EXCEPTION("XField_Split_Local<PhysDim, TopoD2, T>::projectNeighbors - "
                                 "The main-cell is not to the left of its neighbors in the unsplit local mesh.");

      if ( xfld_local_unsplit_.getCellGroupBase(neighbor_group).topoTypeID() == typeid(Triangle) )
      {
        //Checks
        SANS_ASSERT_MSG( CellGroupTriNeighbors_ > 0 && CellGroupTriNeighbors_ < this->nCellGroups(),
                        "The cellgroup index for neighbors (=%d) should be greater than 0 and less than nCellGroups (=%d)",
                        CellGroupTriNeighbors_, this->nCellGroups());

        typedef typename BasisFunctionTopologyBase<TopoD2, Triangle>::type BasisType;
        const BasisType* basis = xfld_local_unsplit_.template getCellGroup<Triangle>(neighbor_group).basis();
        Element_Subdivision_Projector<TopoD2, Triangle> elemProjector(basis);

        if ((itrace==SplitTraceIndex_) || IsotropicSplitFlag_)
        { //Split neighbor
          projectElem<Triangle>(neighbor_group, neighbor_elem, CellGroupTriNeighbors_, cnt_neighbor_tri  , elemProjector);
          projectElem<Triangle>(neighbor_group, neighbor_elem, CellGroupTriNeighbors_, cnt_neighbor_tri+1, elemProjector);
          cnt_neighbor_tri += 2; //Added two (split) tri neighbors
        }
        else
        { //Unsplit neighbor
          projectElem<Triangle>(neighbor_group, neighbor_elem, CellGroupTriNeighbors_, cnt_neighbor_tri, elemProjector );
          cnt_neighbor_tri++; //Added only one (unsplit) tri neighbor
        }
      }
      else if ( xfld_local_unsplit_.getCellGroupBase(neighbor_group).topoTypeID() == typeid(Quad) )
      {
        //Checks
        SANS_ASSERT_MSG( CellGroupQuadNeighbors_ > 0 && CellGroupQuadNeighbors_ < this->nCellGroups(),
                        "The cellgroup index for neighbors (=%d) should be greater than 0 and less than nCellGroups (=%d)",
                        CellGroupQuadNeighbors_, this->nCellGroups());

        typedef typename BasisFunctionTopologyBase<TopoD2, Quad>::type BasisType;
        const BasisType* basis = xfld_local_unsplit_.template getCellGroup<Quad>(neighbor_group).basis();
        Element_Subdivision_Projector<TopoD2, Quad> elemProjector(basis);

        if ((itrace==SplitTraceIndex_) || IsotropicSplitFlag_)
        { //Split neighbor

          SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD2, T>::projectNeighbors - Quad splitting not supported yet.");

#if 0
          projectElem<Quad>(neighbor_group, neighbor_elem, CellGroupQuadNeighbors_, cnt_neighbor_quad  , elemProjector);
          projectElem<Quad>(neighbor_group, neighbor_elem, CellGroupQuadNeighbors_, cnt_neighbor_quad+1, elemProjector);
          cnt_neighbor_quad += 2; //Added two (split) quad neighbors
#endif
        }
        else
        { //Unsplit neighbor
          projectElem<Quad>(neighbor_group, neighbor_elem, CellGroupQuadNeighbors_, cnt_neighbor_quad, elemProjector );
          cnt_neighbor_quad++; //Added only one (unsplit) quad neighbor
        }
      }
      else
        SANS_DEVELOPER_EXCEPTION( "XField_Split_Local<PhysDim, TopoD2, T>::projectNeighbors - Unknown cell topology for neighbor." );
    }

  } //loop across traces of main element
}


template <class PhysDim>
template <class TopoCell>
void
XField_Local_Split<PhysDim, TopoD2>::projectElem(int main_group, int main_elem, int sub_group, int sub_elem,
                                                 Element_Subdivision_Projector<TopoD2, TopoCell>& projector)
{
  typedef typename XField<PhysDim, TopoD2>::template FieldCellGroupType<TopoCell> XFieldCellGroupType;
  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef typename XFieldCellGroupType::BasisType BasisType;

  const XFieldCellGroupType& cellgrp_main = xfld_local_unsplit_.template getCellGroup<TopoCell>(main_group);

  const BasisType* cell_basis = cellgrp_main.basis();

  ElementXFieldClass xfldElem_main( cell_basis ); //Unsplit element
  ElementXFieldClass xfldElem_sub( cell_basis ); //One of the split elements

  cellgrp_main.getElement(xfldElem_main, main_elem); //Get unsplit element from unsplit local mesh

  ElementSplitInfo splitinfo = this->getCellSplitInfo({sub_group,sub_elem});

  if (splitinfo.split_flag == ElementSplitFlag::Split)
  {
    projector.project(xfldElem_main, xfldElem_sub, splitinfo.split_type, splitinfo.edge_index, splitinfo.subcell_index);
    this->template getCellGroup<TopoCell>(sub_group).setElement(xfldElem_sub,sub_elem);
  }
  else if (splitinfo.split_flag == ElementSplitFlag::Unsplit)
  {
    this->template getCellGroup<TopoCell>(sub_group).setElement(xfldElem_main,sub_elem);
  }
  else
    SANS_DEVELOPER_EXCEPTION( "XField_Local_Split<PhysDim, TopoD2, T>::projectElem - ElementSplitFlag should be either 'Split' or 'Unsplit'." );
}


//Explicit instantiations
template class XField_Local_Split<PhysD2,TopoD2>;
template class XField_Local_Split<PhysD3,TopoD2>;
}
