// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELDVOLUME_LOCAL_SPLIT_LINEAR_ISOTROPIC_H_
#define XFIELDVOLUME_LOCAL_SPLIT_LINEAR_ISOTROPIC_H_


#include <ostream>
#include <string>
#include <memory>
#include <typeinfo> // typeid
#include <utility> // std::pair

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "XFieldArea_Traits.h"
#include "XFieldVolume_Traits.h"
#include "FieldAssociativity.h"

#include "Field/Element/ElementProjection_L2.h"
#include "XFieldVolume_Local.h"

/* Refer to the local solve documentation in Field/Documentation/Local_Solves/LocalSolves.pdf for more details and figures. */

namespace SANS
{

template <class TopoCell>
std::vector<int>
getMainSubCellElements(const int trace_index);

template <class PhysDim, class TopoDim>
class XField_Local_Split;

template <class PhysDim, class TopoDim>
class XField_Local_Split_Linear_Isotropic;

template<class PhysDim>
class XField_Local_Split_Linear_Isotropic< PhysDim, TopoD3> : public XField_Local_Base<PhysDim, TopoD3>
{
  friend XField_Local_Split< PhysDim, TopoD3>;

public:
  typedef XField_Local_Base<PhysDim, TopoD3> BaseType;

  XField_Local_Split_Linear_Isotropic(const XField_Local<PhysDim, TopoD3>& xfld_local, XField_CellToTrace<PhysDim,TopoD3>& connectivity,
                                      ElementSplitType split_type, int split_edge_index)
                                      : BaseType(xfld_local.comm()), xfld_local_unsplit_(xfld_local), connectivity_(connectivity)
  {
    basisCategory_ = xfld_local_unsplit_.getCellGroupBase(mainGroup_).basisCategory();
    split(split_type, split_edge_index);
  };

  ~XField_Local_Split_Linear_Isotropic(){};

  const XField_CellToTrace<PhysDim, TopoD3>& getConnectivity() const { return xfld_local_unsplit_.getConnectivity(); }

protected:
  const XField_Local<PhysDim, TopoD3>& xfld_local_unsplit_;
  const XField_CellToTrace<PhysDim, TopoD3>& connectivity_;

  const int mainGroup_ = 0; //cell-group of the target cell in the unsplit mesh
  const int mainElem_ = 0; //cell-elem of the target cell in the unsplit mesh
  BasisFunctionCategory basisCategory_;

  //DOF maps
  std::map<int,int> NodeDOFMap_;

  int cntNodeDOF_; //Counters for different DOF types
  int offset_nodeDOF_; //Offset for the different DOF indices in the global array

  int nTetNeighbors_, nHexNeighbors_, nNeighbors_;
  int nSplitTetNeighbors_, nSplitHexNeighbors_; //Number of *new* neighbors of each type added by splitting
  int nMainBTraceGroups_; //No of boundary traces (and groups) on main cell
  int nOuterBTraceGroups_; //No of boundary groups for outer btraces

  int CellGroupTetNeighbors_, CellGroupHexNeighbors_; //Group index for tet/hex cell groups
  int ITraceGroupInterMain_; //Group index for the interior traces between main cells
  int OuterBTraceGroup_Tri_, OuterBTraceGroup_Quad_; //Group indices for the outer boundary traces

  const int TetEdgeToNodeMap_[6][2] = {{2,3},{3,1},{1,2},{2,0},{0,3},{0,1}};

  void split(ElementSplitType split_type, int split_edge_index);

  template <class TopoMainCell, class TopoMainTrace>
  void splitMainCell();

  void splitNeighbors(int Ntrace, int order);

  void extractLocalGrid(int group, int elem);

  template <class TopoCell, class TopoTrace>
  void setMainCellAssociativity_IsotropicSplit(const std::vector<int>& new_nodeDOFs,
                                               std::vector< ElementAssociativityConstructor<TopoD3, TopoCell> >& DOFAssocs_SubCell,
                                               std::vector< ElementAssociativityConstructor<TopoD2, TopoTrace> >& DOFAssocs_MidITrace,
                                               std::vector< CanonicalTraceToCell >& TracesMid_canonicalL,
                                               std::vector< CanonicalTraceToCell >& TracesMid_canonicalR,
                                               std::vector<int>& left_elem_list, std::vector<int>& right_elem_list);

  template <class TopoCell, class TopoTrace>
  void
  setCellAssociativity_IsotropicSplit(const int cell_group, const int cell_elem,
                                      const int trace_group, const int trace_elem, const int main_cell_trace,
                                      std::vector< ElementAssociativityConstructor<TopoD3, TopoCell> >& DOFAssocs_SubCell,
                                      std::vector< ElementAssociativityConstructor<TopoD2, TopoTrace> >& DOFAssocs_SubITrace,
                                      std::vector< ElementAssociativityConstructor<TopoD2, TopoTrace> >& DOFAssocs_MidITrace,
                                      std::vector< CanonicalTraceToCell >& TracesSub_canonicalL,
                                      std::vector< CanonicalTraceToCell >& TracesSub_canonicalR,
                                      std::vector< CanonicalTraceToCell >& TracesMid_canonicalL,
                                      std::vector< CanonicalTraceToCell >& TracesMid_canonicalR);

  template <class TopoCell>
  void trackDOFs(int group, int elem);

  template <class TopoCell>
  void setCellAssociativity(int group, int elem, ElementAssociativityConstructor<TopoD3, TopoCell>& DOFAssoc);

  template <class TopoTrace>
  void setInteriorTraceAssociativity(int group, int elem, ElementAssociativityConstructor<TopoD2, TopoTrace>& DOFAssoc);

  template <class TopoTrace>
  void setBoundaryTraceAssociativity(int group, int elem, ElementAssociativityConstructor<TopoD2, TopoTrace>& DOFAssoc);

  template <class TopoCell, class TopoTrace>
  std::vector<int>
  getIsotropicFaceSplitSubCellOrder(const int cell_group, const int cell_elem,
                                    const int trace_group, const int trace_elem,
                                    const int main_cell_trace, const int neighbor_cell_trace);

  template <class TopoTrace>
  void getNeighborInfoFromTrace(const int trace_group, const int trace_elem, const int main_elem,
                                int& neighbor_group, int& neighbor_elem,
                                CanonicalTraceToCell& neighbor_canonicalTrace, CanonicalTraceToCell& main_canonicalTrace);

  template <class TopoCell, class TopoTrace>
  void process_MainBTrace_IsotropicSplit(const int trace_group, const int trace_elem, const int order,
                                         const int main_cell_trace, int& cnt_main_btracegroups);

  template <class TopoCell, class TopoTrace>
  void process_OuterBTraces_IsotropicSplit(const int neighbor_group, const int neighbor_elem,
                                           const int trace_group, const int trace_elem, const int main_cell_trace,
                                           const int order, const int local_neighbor_group, const int local_neighbor_elem,
                                           const std::vector< ElementAssociativityConstructor<TopoD3, TopoCell> >& DOFAssocs_SubCell,
                                           std::shared_ptr<typename BaseType::template FieldTraceGroupType<TopoTrace>
                                                                            ::FieldAssociativityConstructorType> fldAssoc_Outer_BTrace,
                                           const int OuterBTraceGroup, int& cnt_outer_Btraces, int& cnt_outer_Btraces_split);
};

}

#endif /* XFIELDVOLUME_LOCAL_SPLIT_LINEAR_ISOTROPIC_H_ */
