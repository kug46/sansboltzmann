// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define XFIELD_LOCAL_COMMON_INSTANTIATE

#include "XFieldVolume.h"
#include "Field/Element/ElementAssociativityArea.h"
#include "Field/Element/ElementAssociativityVolume.h"
#include "Field/Element/ElementAssociativitySpacetime.h"
#include "XFieldSpacetime.h"
#include "XField_Local_Common.h"
/*
  Helper functions for the construction of the local grids, reduces code duplication when constructing
*/

namespace SANS
{


// THIS IS TOPOD > 2 specific
template <class PhysDim, class TopoDim>
template <class TopologyCell, class TopologyTrace>
void
XField_Local_Common<PhysDim, TopoDim>::split( const int startNode, const int endNode, const int newNode,
                                              ConstructorVectorTopo<TopoDim,TopologyCell,TopologyTrace>& cV )
{
  /*
 * Can now do all the operations in here without ever having to reference the global mesh again, all the local Associativity constructors
 * will hold all the necessary information, and the allocate mesh interface largely speaking doesn't care about the splitting.
 */
  typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::BasisType TraceBasisType;

  GroupTraceConstructorVector<TopoDim,TopologyTrace> newAdditionITraceToGroup0, newAdditionITraceToGroup1;

  std::size_t initialSize = cV.localCellGroup0.size(); // This is done so push_back can be used without the loop addressing new elements
  const int nCell = static_cast<int>(cV.localCellGroup0.size());
  const int nITrace0 = static_cast<int>(cV.localITraceGroup0.size());

  // because will need to construct traces that aren't just copies
  const TraceBasisType* trace_basis = TraceBasisType::getBasisFunction(order_, this->basisCategory_);

  // --------------------------------------------- //
  // Cell Group 0
  // --------------------------------------------- //
  /*
   * 1. Loop over cells in group 0
   *  a. extract the cell dof map and duplicate it, have v0 and v1
   *  b. on v0, change node 1 to new node, on v1 change node 0 to new node
   *  c. Loop over traces of original cell,
   *  d. if trace contains node 0 and 1, duplicate, now have t0 and t1
   *  e. on t0 change node 1 to new node, on t1 change node 0 to new node,
   *  f. then associate t0 to v0 and t1 to v1
   *
   *  The two new elements and the trace defining their intersection is now defined.
   *  The traces between the group0 elements aren't correctly defined now, nor those connecting
   *  group0 to group1
   *
   * 2. Loop over initial traces interior to group 0, these all connect to node 0 and 1
   *  a. extract and duplicate trace DOF map
   *  b. map node 1 of t0 to newNode, map node 0 of t1 to newNode
   *  c. establish which canonicalEdge is being split and thus the sub-cell ordering
   *  d. add in the new trace
   *
   * 3. Now know all the new interior traces and cell groups, only remains to reconnect previously existing ones
   *  a. concatenate the temporary new list of traces from steps 1 and 2
   *
   * 4. Reconnect Boundary groups attached to 0
   *
   * 5. Reconnect InteriorTraceGroup0to1
   *
   */

  std::map<int,int> cellGroup0_initialElem_to_splitElem; // map from initial element number to its new split element
  std::map<int,int> cellGroup1_initialElem_to_splitElem; // map from initial element number to its new split element

  // All splitting and construction happens on linear meshes, curved grids are constructed after the fact
  std::array<int,TopologyCell::NNode>  cellNodeMap_0, cellNodeMap_1;
  std::array<int,TopologyTrace::NNode> traceNodeMap_0, traceNodeMap_1;

  // for loading edge dofs
  std::array<int,2> edgeNodes;

  // step 1

  // --------------------------------------------- //
  // Cell Group 0
  // --------------------------------------------- //
  for (std::size_t cell_i = 0; cell_i < initialSize; cell_i++)
  {
    // copy node map for element into vector
    //Get nodeDOF map from current associativities
    cV.localCellGroup0[cell_i].localAssoc.getNodeGlobalMapping( cellNodeMap_0.data(), cellNodeMap_0.size() );

    // Make the new cell node maps from the current ones
    for (int k = 0; k < TopologyCell::NNode; k++)
    {
      if ( cellNodeMap_0[k] != startNode && cellNodeMap_0[k] != endNode ) // node 0 and node 1 make up the to be split edge
        cellNodeMap_1[k] = cellNodeMap_0[k]; // the shared nodes in each map
      else if ( cellNodeMap_0[k] == startNode) // the start node is replaced for the new element
        cellNodeMap_1[k] = newNode;
      else if ( cellNodeMap_0[k] == endNode) // the end node is replaced for the original element
      {
        cellNodeMap_0[k] = newNode;
        cellNodeMap_1[k] = endNode;
      }
      else
        SANS_DEVELOPER_EXCEPTION("what is this node!?");
    }

    // Reconnect the original elements cell node mapping
    cV.localCellGroup0[cell_i].localAssoc.setNodeGlobalMapping( cellNodeMap_0.data(), cellNodeMap_0.size() );

    // New Element based on the new cell node mapping
    GroupElemConstructorIndex<TopoDim,TopologyCell> newElement(cV.localCellGroup0[cell_i]); // make a new element based on the current
    const int rank = cV.localCellGroup0[cell_i].localAssoc.rank();
    newElement.localAssoc.setRank( rank );
    newElement.localAssoc.setNodeGlobalMapping( cellNodeMap_1.data(), cellNodeMap_1.size() );

    for (int trace = 0; trace < TopologyCell::NTrace; trace++) // loop over the traces of the original cell
    {
      // trace nodes based on the update cell node map
      // canonical nodes that make up the trace
      for (std::size_t i = 0; i < traceNodeMap_0.size(); i++)
        traceNodeMap_0[i] = cellNodeMap_0[TraceToCellRefCoord<TopologyTrace,TopoDim,TopologyCell>::TraceNodes[trace][i]];

      // Check if the trace contains startNode and newNode
      const bool contains_node0   = std::find( traceNodeMap_0.begin(), traceNodeMap_0.end(), startNode ) != traceNodeMap_0.end();
      const bool contains_newNode = std::find( traceNodeMap_0.begin(), traceNodeMap_0.end(), newNode   ) != traceNodeMap_0.end();

      // trace is between the original element and the new
      if ( !contains_node0 && contains_newNode  ) // the trace dof map contains newNode but not node 0
      {
        // to have the newNode and not the startNode, it has to be the trace that is the split

        // The original element already has a +1 orientation for this new trace
        // so don't need to re-orient it. This is because the orientation was set in the original vector gather stage
        // do it anyway for safety right now

        // fills traceNodeMap_1 with the correct ordering to give a +1 orientation to the left
        CanonicalTraceToCell leftCanonical;
        leftCanonical = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyCell>
          ::getCanonicalTraceLeft( traceNodeMap_0.data(), traceNodeMap_0.size(),
                                   cellNodeMap_0.data(),  cellNodeMap_0.size(),
                                   traceNodeMap_1.data(), traceNodeMap_1.size() );
        SANS_ASSERT_MSG( leftCanonical.orientation == +1, "The left orientation needs to be +1" );

        // add into the map for unsplit to split elem number
        cellGroup0_initialElem_to_splitElem.insert( std::make_pair<int,int>((int) cell_i, nCell + (int) cell_i ) );

        // make a new Trace object that connects the original to the new element
        GroupTraceConstructorIndex<TopoDim,TopologyTrace> newTrace;

        newTrace.group = -1; // group didn't exist
        newTrace.trace = - ((int) cell_i + 1); // new and indexed by the cell number starting at 1
        newTrace.localAssoc.resize( trace_basis ); // because there might not be any internal traces presently
        newTrace.localAssoc.setRank( rank );
        newTrace.localAssoc.setNodeGlobalMapping( traceNodeMap_1.data(), traceNodeMap_1.size() );
        newTrace.localLRElems.first = GroupElemIndex( 0, (int) cell_i );
        newTrace.localLRElems.second = GroupElemIndex( 0, cellGroup0_initialElem_to_splitElem.at((int) cell_i) );
        newTrace.elementSplitInfo = ElementSplitInfo(ElementSplitFlag::New, 2*nITrace0 + (int) cell_i);

        // find the canonical trace on the new element and orientation corresponding to the new trace ordering
        CanonicalTraceToCell newTraceToNewElem = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyCell>::
            getCanonicalTrace( traceNodeMap_1.data(), traceNodeMap_1.size(), cellNodeMap_1.data(), cellNodeMap_1.size() );

        SANS_ASSERT_MSG( newTraceToNewElem.orientation < 0, "The right orientation needs to be < 0");

        newElement.localAssoc.setOrientation( newTraceToNewElem.orientation, newTraceToNewElem.trace );

        newAdditionITraceToGroup0.push_back(newTrace); // add the newTrace to the list of Traces that will get added

        break; // exit the loop over traces of the original element, found the new trace
      }
    }

    // find which canonical Edge of the cell that is being split, and whether it's sub cell ordering is reverse
    for (int edge = 0; edge < TopologyCell::NEdge; edge++)
    {
      edgeNodes[0] = cellNodeMap_0[ElementEdges<TopologyCell>::EdgeNodes[edge][0]];
      edgeNodes[1] = cellNodeMap_0[ElementEdges<TopologyCell>::EdgeNodes[edge][1]];

      if ( (edgeNodes[0] == startNode) && (edgeNodes[1] == newNode) ) // found the edge, and its oriented forward
      {
        cV.localCellGroup0[cell_i].elementSplitInfo = ElementSplitInfo( ElementSplitFlag::Split, ElementSplitType::Edge, edge, 0 );
        newElement.elementSplitInfo = ElementSplitInfo(                 ElementSplitFlag::Split, ElementSplitType::Edge, edge, 1 );
        break;
      }
      if ( (edgeNodes[0] == newNode)  && (edgeNodes[1] == startNode)  ) // found the edge, and its oriented reversed
      {
        cV.localCellGroup0[cell_i].elementSplitInfo = ElementSplitInfo( ElementSplitFlag::Split, ElementSplitType::Edge, edge, 1 );
        newElement.elementSplitInfo = ElementSplitInfo(                 ElementSplitFlag::Split, ElementSplitType::Edge, edge, 0 );
        break;
      }
    }

    cV.localCellGroup0.push_back(newElement); // add the new element at the end, thus giving it nCell + (int) i for an index
  }

  // All the new cells have been added to cellGroup0
  // The traces between the original cell and the new cell have been added to an interim group
  // newAdditionITraceToGroup0 for later adding to the traces

  // --------------------------------------------- //
  // Interior trace group interior to group 0
  // --------------------------------------------- //

  initialSize = cV.localITraceGroup0.size();
  for ( std::size_t trace_i = 0; trace_i < initialSize; trace_i++)
  {
    cV.localITraceGroup0[trace_i].localAssoc.getNodeGlobalMapping( traceNodeMap_0.data(), traceNodeMap_0.size() );

    // copy the trace
    GroupTraceConstructorIndex<TopoDim,TopologyTrace> newTrace(cV.localITraceGroup0[trace_i]); // copy of the current

    traceNodeMap_1 = traceNodeMap_0;

    const int elemL = cV.localITraceGroup0[trace_i].localLRElems.first.elem, elemR = cV.localITraceGroup0[trace_i].localLRElems.second.elem;

    // change the dof maps to use the new dof
    // must be done before comparing the maps because the cell maps were updated above
    for (int k = 0; k < TopologyTrace::NNode; k++)
    {
      if (traceNodeMap_0[k] == startNode) // remap the 0-th node for the new trace
        traceNodeMap_1[k] = newNode;
      if (traceNodeMap_0[k] == endNode) // remap the 1-st node for the original trace
        traceNodeMap_0[k] = newNode;
    }

    // loop over edges of the trace, and figure out canonicalEdge and sub-cell ordering
    for (int edge = 0; edge < TopologyTrace::NEdge; edge++)
    {
      edgeNodes[0] = traceNodeMap_0[ElementEdges<TopologyTrace>::EdgeNodes[edge][0]];
      edgeNodes[1] = traceNodeMap_0[ElementEdges<TopologyTrace>::EdgeNodes[edge][1]];

      if ( (edgeNodes[0] == startNode) && (edgeNodes[1] == newNode)   ) // found the edge, and its oriented forward
      {
        cV.localITraceGroup0[trace_i].elementSplitInfo = ElementSplitInfo( ElementSplitFlag::Split, ElementSplitType::Edge, edge, 0 );
        newTrace.elementSplitInfo = ElementSplitInfo(                      ElementSplitFlag::Split, ElementSplitType::Edge, edge, 1 );
        break;
      }
      if ( (edgeNodes[0] == newNode)   && (edgeNodes[1] == startNode) ) // found the edge, and its oriented reversed
      {
        cV.localITraceGroup0[trace_i].elementSplitInfo = ElementSplitInfo( ElementSplitFlag::Split, ElementSplitType::Edge, edge, 1 );
        newTrace.elementSplitInfo = ElementSplitInfo(                      ElementSplitFlag::Split, ElementSplitType::Edge, edge, 0 );
        break;
      }
    }

    // set the trace node map. Don't need to change trace orientations as they inherit the previous orientation from the unsplit Assoc
    cV.localITraceGroup0[trace_i].localAssoc.setNodeGlobalMapping( traceNodeMap_0.data(), traceNodeMap_0.size() );

    newTrace.localAssoc.setNodeGlobalMapping( traceNodeMap_1.data(), traceNodeMap_1.size() );
    newTrace.localLRElems = std::make_pair( GroupElemIndex(0, cellGroup0_initialElem_to_splitElem.at(elemL) ),
                                            GroupElemIndex(0, cellGroup0_initialElem_to_splitElem.at(elemR) ) );

    cV.localITraceGroup0.push_back(newTrace); // add to the list
  }

  // Interior Traces are all known now, can add the new ITraces from the creation of the new cells after the split ones
  cV.localITraceGroup0.insert( cV.localITraceGroup0.end(), newAdditionITraceToGroup0.begin(), newAdditionITraceToGroup0.end() );

  // All new cells and new interior traces are now known and in place. It remains to reconnect the boundary trace groups
  // and interior traces from group 0 to group 1

  // --------------------------------------------- //
  // Boundary Traces attached to Group 0
  // --------------------------------------------- //
  initialSize = cV.localBTraceGroup0.size();
  for (std::size_t trace_i = 0; trace_i < initialSize; trace_i++)
  {
    cV.localBTraceGroup0[trace_i].localAssoc.getNodeGlobalMapping( traceNodeMap_0.data(), traceNodeMap_0.size() );

    // Check if this trace contains node 0 and node 1 -- namely must be split
    const bool attachedToNode0 = std::find(std::begin(traceNodeMap_0),std::end(traceNodeMap_0), startNode) != std::end(traceNodeMap_0);
    const bool attachedToNode1 = std::find(std::begin(traceNodeMap_0),std::end(traceNodeMap_0),   endNode) != std::end(traceNodeMap_0);

    if ( attachedToNode0 && attachedToNode1 )
    {
      traceNodeMap_1 = traceNodeMap_0; // duplicate the map

      GroupTraceConstructorIndex<TopoDim,TopologyTrace> newTrace(cV.localBTraceGroup0[trace_i]); // copy of the current

      // change the dof maps to use the new dof
      // must be done before comparing the maps because the cell maps were updated above
      for (int k = 0; k < TopologyTrace::NNode; k++)
      {
        if (traceNodeMap_0[k] == startNode) // remap the start node for the new trace
          traceNodeMap_1[k] = newNode;
        if (traceNodeMap_0[k] == endNode) // remap the end node for the original trace
          traceNodeMap_0[k] = newNode;
      }

      // loop over edges of the trace, and figure out canonicalEdge and sub-cell ordering
      for (int edge = 0; edge < TopologyTrace::NEdge; edge++)
      {
        edgeNodes[0] = traceNodeMap_0[ElementEdges<TopologyTrace>::EdgeNodes[edge][0]];
        edgeNodes[1] = traceNodeMap_0[ElementEdges<TopologyTrace>::EdgeNodes[edge][1]];
//        std::cout << "edgeNodes = " << edgeNodes[0] << ", " << edgeNodes[1] << std::endl;

        if ( (edgeNodes[0] == startNode) && (edgeNodes[1] == newNode) ) // found the edge, and its oriented forward
        {
          cV.localBTraceGroup0[trace_i].elementSplitInfo = ElementSplitInfo( ElementSplitFlag::Split, ElementSplitType::Edge, edge, 0 );
          newTrace.elementSplitInfo = ElementSplitInfo(                      ElementSplitFlag::Split, ElementSplitType::Edge, edge, 1 );
//          std::cout<< "oriented forward" << std::endl;
          break;
        }
        if ( (edgeNodes[0] == newNode)   && (edgeNodes[1] == startNode) ) // found the edge, and its oriented reversed
        {
          cV.localBTraceGroup0[trace_i].elementSplitInfo = ElementSplitInfo( ElementSplitFlag::Split, ElementSplitType::Edge, edge, 1 );
          newTrace.elementSplitInfo = ElementSplitInfo(                      ElementSplitFlag::Split, ElementSplitType::Edge, edge, 0 );
//          std::cout<< "oriented reversed" << std::endl;
          break;
        }
      }

      const int elemL = cV.localBTraceGroup0[trace_i].localLRElems.first.elem;

      cV.localBTraceGroup0[trace_i].localAssoc.setNodeGlobalMapping( traceNodeMap_0.data(), traceNodeMap_0.size() );

      newTrace.localAssoc.setNodeGlobalMapping( traceNodeMap_1.data(), traceNodeMap_1.size() );
      newTrace.localLRElems.first.elem = cellGroup0_initialElem_to_splitElem.at(elemL); // attached to the new cell from the split

      cV.localBTraceGroup0.push_back(newTrace); // add it on to the group
    }
    else if ( !attachedToNode0 && attachedToNode1
      && cellGroup0_initialElem_to_splitElem.find(cV.localBTraceGroup0[trace_i].localLRElems.first.elem)
        != cellGroup0_initialElem_to_splitElem.end() )
      // is attached to node 1, but not 0, i.e. cell number requires relabeling
      cV.localBTraceGroup0[trace_i].localLRElems.first.elem
        = cellGroup0_initialElem_to_splitElem.at(cV.localBTraceGroup0[trace_i].localLRElems.first.elem);
    else if ( attachedToNode0 && !attachedToNode1 )
    {
      // do nothing, the cell numbering is the same
    }
    else
      SANS_DEVELOPER_EXCEPTION("XField_EdgeLocal<PhysDim,TopoDim>::split() -- an element in boundary "
          "trace group 0 is attached to neither node 0 nor node 1");
  }


    // --------------------------------------------- //
    // Cell Group 1
    // --------------------------------------------- //
    initialSize = cV.localCellGroup1.size();
    const int nITrace1 = static_cast<int>(cV.localITraceGroup1.size()); // Should be 0 for primal grids
    int newGroup1Elems = 0; // number of new elements added to group 1, for building the maps
    for (std::size_t cell_i = 0; cell_i < initialSize; cell_i++)
    {
      // copy node map for element into vector
      //Get nodeDOF map from current associativities
      cV.localCellGroup1[cell_i].localAssoc.getNodeGlobalMapping( cellNodeMap_0.data(), cellNodeMap_0.size() );

      // Check if this trace contains node 0 and node 1 -- namely must be split
      const bool attachedToNode0 = std::find(std::begin(cellNodeMap_0),std::end(cellNodeMap_0), startNode) != std::end(cellNodeMap_0);
      const bool attachedToNode1 = std::find(std::begin(cellNodeMap_0),std::end(cellNodeMap_0),   endNode) != std::end(cellNodeMap_0);

      if ( !( attachedToNode0 && attachedToNode1 ) )
        continue; // No splits necessary, not attached to both nodes

      // Make the new cell node maps from the current ones
      for (int k = 0; k < TopologyCell::NNode; k++)
      {
        if ( cellNodeMap_0[k] != startNode && cellNodeMap_0[k] != endNode ) // node 0 and node 1 make up the to be split edge
          cellNodeMap_1[k] = cellNodeMap_0[k]; // the shared nodes in each map
        else if ( cellNodeMap_0[k] == startNode) // the start node is replaced for the new element
          cellNodeMap_1[k] = newNode;
        else if ( cellNodeMap_0[k] == endNode) // the end node is replaced for the original element
        {
          cellNodeMap_0[k] = newNode;
          cellNodeMap_1[k] = endNode;
        }
        else
          SANS_DEVELOPER_EXCEPTION("what is this node!?");
      }

      // Reconnect the original elements cell node mapping
      cV.localCellGroup1[cell_i].localAssoc.setNodeGlobalMapping( cellNodeMap_0.data(), cellNodeMap_0.size() );

      // New Element based on the new cell node mapping
      GroupElemConstructorIndex<TopoDim,TopologyCell> newElement(cV.localCellGroup1[cell_i]); // make a new element based on the current
      const int rank = cV.localCellGroup1[cell_i].localAssoc.rank();
      newElement.localAssoc.setRank( rank );
      newElement.localAssoc.setNodeGlobalMapping( cellNodeMap_1.data(), cellNodeMap_1.size() );

      for (int trace = 0; trace < TopologyCell::NTrace; trace++) // loop over the traces of the original cell to find the split one
      {
        // trace nodes based on the update cell node map
        // canonical nodes that make up the trace
        for (std::size_t i = 0; i < traceNodeMap_0.size(); i++)
          traceNodeMap_0[i] = cellNodeMap_0[ TraceToCellRefCoord<TopologyTrace,TopoDim,TopologyCell>::TraceNodes[trace][i] ];

        // Check if the trace contains startNode and newNode
        const bool contains_node0   = std::find( traceNodeMap_0.begin(), traceNodeMap_0.end(), startNode ) != traceNodeMap_0.end();
        const bool contains_newNode = std::find( traceNodeMap_0.begin(), traceNodeMap_0.end(), newNode   ) != traceNodeMap_0.end();

        // trace is between the original element and the new
        if ( !contains_node0 && contains_newNode  ) // the trace dof map contains newNode but not node 0
        {
          // The original element already has a +1 orientation for this new trace
          // so don't need to re-orient it. This is because the orientation was set in the original vector gather stage
          // do it anyway for safety right now

          // fills traceNodeMap_1 with the correct ordering to give a +1 orientation to the left
          CanonicalTraceToCell leftCanonical;
          leftCanonical = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyCell>
            ::getCanonicalTraceLeft( traceNodeMap_0.data(), traceNodeMap_0.size(),
                                     cellNodeMap_0.data(),  cellNodeMap_0.size(),
                                     traceNodeMap_1.data(), traceNodeMap_1.size() );

          // SANS_ASSERT_MSG( leftCanonical.orientation == +1, "The left orientation needs to be +1" );
          cV.localCellGroup1[cell_i].localAssoc.setOrientation( leftCanonical.orientation, leftCanonical.trace );

          // add into the map for unsplit to split elem number
          cellGroup1_initialElem_to_splitElem.insert( std::make_pair<int,int>((int) cell_i, (int)initialSize + newGroup1Elems ) );

          // make a new Trace object that connects the original to the new element
          GroupTraceConstructorIndex<TopoDim,TopologyTrace> newTrace;

          newTrace.group = -1; // group didn't exist
          newTrace.trace = - ((int) cell_i + 1 + (cV.localCellGroup0.size()/2) ); // indexed by cell number starting at 1 offset by cells in group 0
          newTrace.localAssoc.resize( trace_basis ); // because there might not be any internal traces presently
          newTrace.localAssoc.setRank( rank );
          newTrace.localAssoc.setNodeGlobalMapping( traceNodeMap_1.data(), traceNodeMap_1.size() );
          newTrace.localLRElems.first = GroupElemIndex( 1, (int) cell_i );
          newTrace.localLRElems.second = GroupElemIndex( 1, cellGroup1_initialElem_to_splitElem.at((int) cell_i) );
          newTrace.elementSplitInfo = ElementSplitInfo(ElementSplitFlag::New, 2*nITrace1 + (int) cell_i);

          // find the canonical trace on the new element and orientation corresponding to the new trace ordering
          CanonicalTraceToCell newTraceToNewElem = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyCell>::
              getCanonicalTrace( traceNodeMap_1.data(), traceNodeMap_1.size(), cellNodeMap_1.data(), cellNodeMap_1.size() );

          SANS_ASSERT_MSG( newTraceToNewElem.orientation < 0, "The right orientation needs to be < 0");

          newElement.localAssoc.setOrientation( newTraceToNewElem.orientation, newTraceToNewElem.trace );

          newAdditionITraceToGroup1.push_back(newTrace); // add the newTrace to the list of Traces that will get added

          break; // exit the loop over traces of the original element, found the new trace
        }
      }

      // find which canonical Edge of the cell that is being split, and whether it's sub cell ordering is reverse
      for (int edge = 0; edge < TopologyCell::NEdge; edge++)
      {
        edgeNodes[0] = cellNodeMap_0[ElementEdges<TopologyCell>::EdgeNodes[edge][0]];
        edgeNodes[1] = cellNodeMap_0[ElementEdges<TopologyCell>::EdgeNodes[edge][1]];

        if ( (edgeNodes[0] == startNode) && (edgeNodes[1] == newNode) ) // found the edge, and its oriented forward
        {
          cV.localCellGroup1[cell_i].elementSplitInfo = ElementSplitInfo( ElementSplitFlag::Split, ElementSplitType::Edge, edge, 0 );
          newElement.elementSplitInfo = ElementSplitInfo(                 ElementSplitFlag::Split, ElementSplitType::Edge, edge, 1 );
          break;
        }
        if ( (edgeNodes[0] == newNode)  && (edgeNodes[1] == startNode)  ) // found the edge, and its oriented reversed
        {
          cV.localCellGroup1[cell_i].elementSplitInfo = ElementSplitInfo( ElementSplitFlag::Split, ElementSplitType::Edge, edge, 1 );
          newElement.elementSplitInfo = ElementSplitInfo(                 ElementSplitFlag::Split, ElementSplitType::Edge, edge, 0 );
          break;
        }
      }

      cV.localCellGroup1.push_back(newElement); // add the new element at the end, thus giving it nCell + (int) i for an index
      newGroup1Elems++; // added in a new element
    }

  // --------------------------------------------- //
  // Interior trace group interior to group 1
  // --------------------------------------------- //

  initialSize = cV.localITraceGroup1.size();
  // This is for splitting any existing traces that are connected to the split edge, really only kicks in in 3D
  for ( std::size_t trace_i = 0; trace_i < initialSize; trace_i++)
  {
    cV.localITraceGroup1[trace_i].localAssoc.getNodeGlobalMapping( traceNodeMap_0.data(), traceNodeMap_0.size() );

    // Check if this trace contains node 0 and node 1 -- namely must be split
    const bool attachedToNode0 = std::find(std::begin(traceNodeMap_0),std::end(traceNodeMap_0), startNode) != std::end(traceNodeMap_0);
    const bool attachedToNode1 = std::find(std::begin(traceNodeMap_0),std::end(traceNodeMap_0),   endNode) != std::end(traceNodeMap_0);

    if ( ( attachedToNode0 && attachedToNode1 ) ) // It's been split
    {
      // copy the trace
      GroupTraceConstructorIndex<TopoDim,TopologyTrace> newTrace(cV.localITraceGroup1[trace_i]); // copy of the current

      traceNodeMap_1 = traceNodeMap_0; // copy the map

      const int elemL = cV.localITraceGroup1[trace_i].localLRElems.first.elem, elemR = cV.localITraceGroup1[trace_i].localLRElems.second.elem;

      // change the dof maps to use the new dof
      // must be done before comparing the maps because the cell maps were updated above
      for (int k = 0; k < TopologyTrace::NNode; k++)
      {
        if (traceNodeMap_0[k] == startNode) // remap the start node for the new trace
          traceNodeMap_1[k] = newNode;
        if (traceNodeMap_0[k] == endNode) // remap the end node for the original trace
          traceNodeMap_0[k] = newNode;
      }

      // loop over edges of the trace, and figure out canonicalEdge and sub-cell ordering
      for (int edge = 0; edge < TopologyTrace::NEdge; edge++)
      {
        edgeNodes[0] = traceNodeMap_0[ElementEdges<TopologyTrace>::EdgeNodes[edge][0]];
        edgeNodes[1] = traceNodeMap_0[ElementEdges<TopologyTrace>::EdgeNodes[edge][1]];

        if ( (edgeNodes[0] == startNode) && (edgeNodes[1] == newNode)   ) // found the edge, and its oriented forward
        {
          cV.localITraceGroup1[trace_i].elementSplitInfo = ElementSplitInfo( ElementSplitFlag::Split, ElementSplitType::Edge, edge, 0 );
          newTrace.elementSplitInfo = ElementSplitInfo(                      ElementSplitFlag::Split, ElementSplitType::Edge, edge, 1 );
          break;
        }
        if ( (edgeNodes[0] == newNode)   && (edgeNodes[1] == startNode) ) // found the edge, and its oriented reversed
        {
          cV.localITraceGroup1[trace_i].elementSplitInfo = ElementSplitInfo( ElementSplitFlag::Split, ElementSplitType::Edge, edge, 1 );
          newTrace.elementSplitInfo = ElementSplitInfo(                      ElementSplitFlag::Split, ElementSplitType::Edge, edge, 0 );
          break;
        }
      }

      // set the trace node map. Don't need to change trace orientations as they inherit the previous orientation from the unsplit Assoc
      cV.localITraceGroup1[trace_i].localAssoc.setNodeGlobalMapping( traceNodeMap_0.data(), traceNodeMap_0.size() );

      newTrace.localAssoc.setNodeGlobalMapping( traceNodeMap_1.data(), traceNodeMap_1.size() );
      newTrace.localLRElems = std::make_pair( GroupElemIndex(1, cellGroup1_initialElem_to_splitElem.at(elemL) ),
                                              GroupElemIndex(1, cellGroup1_initialElem_to_splitElem.at(elemR) ) );


      // if ( newTrace.localLRElems.first.elem == 1
      //   && newTrace.localLRElems.second.elem == 5 )
      //   std::cout<< "Found YA 2" << std::endl;

      cV.localITraceGroup1.push_back(newTrace); // add to the list
    }
    else if ( !attachedToNode0 && attachedToNode1 )// its attached to only endNode, and its left element was split
    {
      // left element has been split, reconnect
      if ( cellGroup1_initialElem_to_splitElem.find(cV.localITraceGroup1[trace_i].localLRElems.first.elem)
        != cellGroup1_initialElem_to_splitElem.end() )
        cV.localITraceGroup1[trace_i].localLRElems.first.elem
        = cellGroup1_initialElem_to_splitElem.at(cV.localITraceGroup1[trace_i].localLRElems.first.elem);

      // right element has been split, reconnect
      if ( cellGroup1_initialElem_to_splitElem.find(cV.localITraceGroup1[trace_i].localLRElems.second.elem)
        != cellGroup1_initialElem_to_splitElem.end() )
        cV.localITraceGroup1[trace_i].localLRElems.second.elem
        = cellGroup1_initialElem_to_splitElem.at(cV.localITraceGroup1[trace_i].localLRElems.second.elem);
    }

    // if ( cV.localITraceGroup1[trace_i].localLRElems.first.elem == 1
    //   && cV.localITraceGroup1[trace_i].localLRElems.second.elem == 5 )
    //   std::cout<< "Found YA 3" << std::endl;

  }
  // Interior Traces are all known now, can add the new ITraces after the split ones
  cV.localITraceGroup1.insert( cV.localITraceGroup1.end(), newAdditionITraceToGroup1.begin(), newAdditionITraceToGroup1.end() );


  // --------------------------------------------- //
  // Interior Traces from group 0 to group 1
  // --------------------------------------------- //

  // Need to loop over 0to1 group and reallocate their connections where necessary
  initialSize = cV.localITraceGroup0to1.size();
  for (std::size_t trace_i = 0; trace_i < initialSize; trace_i++)
  {
    cV.localITraceGroup0to1[trace_i].localAssoc.getNodeGlobalMapping( traceNodeMap_0.data(), traceNodeMap_0.size() );

    // Check if this trace contains node 0 and node 1 -- namely must be split
    bool attachedToNode0 = std::find(std::begin(traceNodeMap_0),std::end(traceNodeMap_0), startNode) != std::end(traceNodeMap_0);
    bool attachedToNode1 = std::find(std::begin(traceNodeMap_0),std::end(traceNodeMap_0),   endNode) != std::end(traceNodeMap_0);

    if (attachedToNode0 && attachedToNode1 ) // going to be split, need to make a new trace
    {
      // copy the trace
      GroupTraceConstructorIndex<TopoDim,TopologyTrace> newTrace(cV.localITraceGroup0to1[trace_i]); // copy of the current

      traceNodeMap_1 = traceNodeMap_0;

      const int elemL = cV.localITraceGroup0to1[trace_i].localLRElems.first.elem, elemR = cV.localITraceGroup0to1[trace_i].localLRElems.second.elem;

      // change the dof maps to use the new dof
      // must be done before comparing the maps because the cell maps were updated above
      for (int k = 0; k < TopologyTrace::NNode; k++)
      {
        if (traceNodeMap_0[k] == startNode) // remap the 0-th node for the new trace
          traceNodeMap_1[k] = newNode;
        if (traceNodeMap_0[k] == endNode) // remap the 1-st node for the original trace
          traceNodeMap_0[k] = newNode;
      }

      // loop over edges of the trace, and figure out canonicalEdge and sub-cell ordering
      for (int edge = 0; edge < TopologyTrace::NEdge; edge++)
      {
        edgeNodes[0] = traceNodeMap_0[ElementEdges<TopologyTrace>::EdgeNodes[edge][0]];
        edgeNodes[1] = traceNodeMap_0[ElementEdges<TopologyTrace>::EdgeNodes[edge][1]];

        if ( (edgeNodes[0] == startNode) && (edgeNodes[1] == newNode)   ) // found the edge, and its oriented forward
        {
          cV.localITraceGroup0to1[trace_i].elementSplitInfo = ElementSplitInfo( ElementSplitFlag::Split, ElementSplitType::Edge, edge, 0 );
          newTrace.elementSplitInfo = ElementSplitInfo(                         ElementSplitFlag::Split, ElementSplitType::Edge, edge, 1 );
          break;
        }
        if ( (edgeNodes[0] == newNode)   && (edgeNodes[1] == startNode) ) // found the edge, and its oriented reversed
        {
          cV.localITraceGroup0to1[trace_i].elementSplitInfo = ElementSplitInfo( ElementSplitFlag::Split, ElementSplitType::Edge, edge, 1 );
          newTrace.elementSplitInfo = ElementSplitInfo(                         ElementSplitFlag::Split, ElementSplitType::Edge, edge, 0 );
          break;
        }
      }

      // set the trace node map. Don't need to change trace orientations as they inherit the previous orientation from the unsplit Assoc
      cV.localITraceGroup0to1[trace_i].localAssoc.setNodeGlobalMapping( traceNodeMap_0.data(), traceNodeMap_0.size() );

      newTrace.localAssoc.setNodeGlobalMapping( traceNodeMap_1.data(), traceNodeMap_1.size() );
      newTrace.localLRElems = std::make_pair( GroupElemIndex(0, cellGroup0_initialElem_to_splitElem.at(elemL) ),
                                              GroupElemIndex(1, cellGroup1_initialElem_to_splitElem.at(elemR) ) );

      cV.localITraceGroup0to1.push_back(newTrace); // add to the list

    }
    else if ( !attachedToNode0 && attachedToNode1 ) // only attached to endNode, just need to go through maps to reconnect left element
    {
      // don't need to check key existence, as for 0->1 group, it must be there
      cV.localITraceGroup0to1[trace_i].localLRElems.first.elem
        = cellGroup0_initialElem_to_splitElem.at(cV.localITraceGroup0to1[trace_i].localLRElems.first.elem);
    }
    else if ( attachedToNode0 && !attachedToNode1 )
    {
      // do nothing
    }
    else
      SANS_DEVELOPER_EXCEPTION("XField_ElementLocal<PhysDim,TopoD3>::split() -- an element in interior "
               "trace group 0 to 1 is attached to neither node 0 nor node 1 or both");
  }


  // --------------------------------------------- //
  // Boundary Trace Group 1
  // --------------------------------------------- //

  initialSize = cV.localBTraceGroup1.size();
  // reconnecting if only attached to one trace, splitting if they also share the split edge, i.e. if the split edge is on a boundary
  for (std::size_t trace_i = 0; trace_i < initialSize; trace_i++)
  {
    cV.localBTraceGroup1[trace_i].localAssoc.getNodeGlobalMapping( traceNodeMap_0.data(), traceNodeMap_0.size() );

    // Check if this trace contains node 0 and node 1 -- namely must be split
    bool attachedToNode0 = std::find(std::begin(traceNodeMap_0),std::end(traceNodeMap_0), startNode) != std::end(traceNodeMap_0);
    bool attachedToNode1 = std::find(std::begin(traceNodeMap_0),std::end(traceNodeMap_0),   endNode) != std::end(traceNodeMap_0);

    if (attachedToNode0 && attachedToNode1 ) // going to be split, need to make a new trace
    {
      // copy the trace
      GroupTraceConstructorIndex<TopoDim,TopologyTrace> newTrace(cV.localBTraceGroup1[trace_i]); // copy of the current

      traceNodeMap_1 = traceNodeMap_0;

      const int elemL = cV.localBTraceGroup1[trace_i].localLRElems.first.elem;

      // change the dof maps to use the new dof
      // must be done before comparing the maps because the cell maps were updated above
      for (int k = 0; k < TopologyTrace::NNode; k++)
      {
        if (traceNodeMap_0[k] == startNode) // remap the 0-th node for the new trace
          traceNodeMap_1[k] = newNode;
        if (traceNodeMap_0[k] == endNode) // remap the 1-st node for the original trace
          traceNodeMap_0[k] = newNode;
      }

      // loop over edges of the trace, and figure out canonicalEdge and sub-cell ordering
      for (int edge = 0; edge < TopologyTrace::NEdge; edge++)
      {
        edgeNodes[0] = traceNodeMap_0[ElementEdges<TopologyTrace>::EdgeNodes[edge][0]];
        edgeNodes[1] = traceNodeMap_0[ElementEdges<TopologyTrace>::EdgeNodes[edge][1]];

        if ( (edgeNodes[0] == startNode) && (edgeNodes[1] == newNode)   ) // found the edge, and its oriented forward
        {
          cV.localBTraceGroup1[trace_i].elementSplitInfo = ElementSplitInfo( ElementSplitFlag::Split, ElementSplitType::Edge, edge, 0 );
          newTrace.elementSplitInfo = ElementSplitInfo(                      ElementSplitFlag::Split, ElementSplitType::Edge, edge, 1 );
          break;
        }
        if ( (edgeNodes[0] == newNode)   && (edgeNodes[1] == startNode) ) // found the edge, and its oriented reversed
        {
          cV.localBTraceGroup1[trace_i].elementSplitInfo = ElementSplitInfo( ElementSplitFlag::Split, ElementSplitType::Edge, edge, 1 );
          newTrace.elementSplitInfo = ElementSplitInfo(                      ElementSplitFlag::Split, ElementSplitType::Edge, edge, 0 );
          break;
        }
      }

      // set the trace node map. Don't need to change trace orientations as they inherit the previous orientation from the unsplit Assoc
      cV.localBTraceGroup1[trace_i].localAssoc.setNodeGlobalMapping( traceNodeMap_0.data(), traceNodeMap_0.size() );

      newTrace.localAssoc.setNodeGlobalMapping( traceNodeMap_1.data(), traceNodeMap_1.size() );
      newTrace.localLRElems.first.elem = cellGroup1_initialElem_to_splitElem.at(elemL);

      cV.localBTraceGroup1.push_back(newTrace); // add to the list

    }
    else if ( !attachedToNode0 && attachedToNode1  // only attached to endNode, if its left element was split, re-attach
      && cellGroup1_initialElem_to_splitElem.find(cV.localBTraceGroup1[trace_i].localLRElems.first.elem)
        != cellGroup1_initialElem_to_splitElem.end() )
    {
      cV.localBTraceGroup1[trace_i].localLRElems.first.elem
        = cellGroup1_initialElem_to_splitElem.at(cV.localBTraceGroup1[trace_i].localLRElems.first.elem);
    }
    else if ( attachedToNode0 && !attachedToNode1 )
    {
      // do nothing
    }
  }

  // --------------------------------------------- //
  // Outer Boundary Trace Group
  // --------------------------------------------- //

  std::map<int,int> *outerMap;
  // if (this->neighborhood == Neighborhood::Primal)
  //   outerMap = &cellGroup0_initialElem_to_splitElem;
  // else
  outerMap = &cellGroup1_initialElem_to_splitElem;

  // Need to loop over outer group and reallocate their connections where necessary
  for (std::size_t trace_i = 0; trace_i < cV.localBTraceOuterGroup1.size(); trace_i++)
  {
    cV.localBTraceOuterGroup1[trace_i].localAssoc.getNodeGlobalMapping( traceNodeMap_0.data(), traceNodeMap_0.size() );

    // Check if this trace contains node 0 and node 1 -- namely must be split
    bool attachedToNode0 = std::find(std::begin(traceNodeMap_0),std::end(traceNodeMap_0), startNode) != std::end(traceNodeMap_0);
    bool attachedToNode1 = std::find(std::begin(traceNodeMap_0),std::end(traceNodeMap_0),   endNode) != std::end(traceNodeMap_0);

    if (attachedToNode0 && attachedToNode1 ) // going to be split, need to make a new trace
    {
      // copy the trace
      GroupTraceConstructorIndex<TopoDim,TopologyTrace> newTrace(cV.localBTraceOuterGroup1[trace_i]); // copy of the current

      traceNodeMap_1 = traceNodeMap_0;

      const int elemL = cV.localBTraceOuterGroup1[trace_i].localLRElems.first.elem;

      // change the dof maps to use the new dof
      // must be done before comparing the maps because the cell maps were updated above
      for (int k = 0; k < TopologyTrace::NNode; k++)
      {
        if (traceNodeMap_0[k] == startNode) // remap the 0-th node for the new trace
          traceNodeMap_1[k] = newNode;
        if (traceNodeMap_0[k] == endNode) // remap the 1-st node for the original trace
          traceNodeMap_0[k] = newNode;
      }

      // loop over edges of the trace, and figure out canonicalEdge and sub-cell ordering
      for (int edge = 0; edge < TopologyTrace::NEdge; edge++)
      {
        edgeNodes[0] = traceNodeMap_0[ElementEdges<TopologyTrace>::EdgeNodes[edge][0]];
        edgeNodes[1] = traceNodeMap_0[ElementEdges<TopologyTrace>::EdgeNodes[edge][1]];

        if ( (edgeNodes[0] == startNode) && (edgeNodes[1] == newNode)   ) // found the edge, and its oriented forward
        {
          cV.localBTraceOuterGroup1[trace_i].elementSplitInfo = ElementSplitInfo( ElementSplitFlag::Split, ElementSplitType::Edge, edge, 0 );
          newTrace.elementSplitInfo = ElementSplitInfo(                          ElementSplitFlag::Split, ElementSplitType::Edge, edge, 1 );
          break;
        }
        if ( (edgeNodes[0] == newNode)   && (edgeNodes[1] == startNode) ) // found the edge, and its oriented reversed
        {
          cV.localBTraceOuterGroup1[trace_i].elementSplitInfo = ElementSplitInfo( ElementSplitFlag::Split, ElementSplitType::Edge, edge, 1 );
          newTrace.elementSplitInfo = ElementSplitInfo(                          ElementSplitFlag::Split, ElementSplitType::Edge, edge, 0 );
          break;
        }
      }

      // set the trace node map. Don't need to change trace orientations as they inherit the previous orientation from the unsplit Assoc
      cV.localBTraceOuterGroup1[trace_i].localAssoc.setNodeGlobalMapping( traceNodeMap_0.data(), traceNodeMap_0.size() );

      newTrace.localAssoc.setNodeGlobalMapping( traceNodeMap_1.data(), traceNodeMap_1.size() );
      newTrace.localLRElems.first.elem = outerMap->at(elemL);

      cV.localBTraceOuterGroup1.push_back(newTrace); // add to the list

    }
    else if ( !attachedToNode0 && attachedToNode1 // its attached to only endNode, and its left element was split
      && outerMap->find(cV.localBTraceOuterGroup1[trace_i].localLRElems.first.elem) != outerMap->end() )
    {
      cV.localBTraceOuterGroup1[trace_i].localLRElems.first.elem = outerMap->at(cV.localBTraceOuterGroup1[trace_i].localLRElems.first.elem);
    }
    else if ( attachedToNode0 && !attachedToNode1 )
    {
      // do nothing
    }
  }

}

// Explicit instantiations
template class XField_Local_Common<PhysD3,TopoD3>;
template void  XField_Local_Common<PhysD3,TopoD3>::split<Tet,Triangle>( const int, const int, const int,
            ConstructorVectorTopo<TopoD3,Tet,Triangle>& );

// Explicit instantiations
template class XField_Local_Common<PhysD4,TopoD4>;
template void  XField_Local_Common<PhysD4,TopoD4>::split<Pentatope,Tet>( const int, const int, const int,
            ConstructorVectorTopo<TopoD4,Pentatope,Tet>& );
}
