// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SANS_FIELD_LOCAL_PATCH_CONSTRUCTOR_H_
#define SANS_FIELD_LOCAL_PATCH_CONSTRUCTOR_H_

#include <map>

#include "Field/Partition/LagrangeElementGroup.h"
#include "Field/XField_CellToTrace.h"
#include "Field/SpaceTypes.h"

#include "XField_LocalPatchBase.h"

namespace SANS
{

template<class PhysDim,class TopoDim> class XField_CellToTrace;
class Field_NodalView;

template<class,class> class TraceSoup;

template<class PhysDim, class Topology>
class XField_LocalPatchConstructor : public XField_LocalPatchBase<PhysDim,Topology>
{
protected:
  typedef XField_LocalPatchBase<PhysDim,Topology> BaseType;
  typedef DLA::VectorS<PhysDim::D,Real> VectorX;
  typedef typename BaseType::GlobalDOFType GlobalDOFType;

  typedef typename Topology::TopologyTrace TraceTopology;
  typedef typename Topology::TopoDim TopoDim;

  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TraceTopology> XFieldTraceGroupType;

  typedef XField<PhysDim,TopoDim> XField_Type;

  // some labels for readability
  using BaseType::RESOLVE_CELL_GROUP;
  using BaseType::FIXED_CELL_GROUP;
  using BaseType::STRAIGHT_SIDED;

public:
  /*\
   * XField_LocalPatch constructor:
   *   constructor for EXTRACTION,
   *   use addResolveElement and addFixedElement to extract the local patch
   *   the last two arguments are required to extract a patch to support CG adaptation
   *
   * input:
   *   connectivity:   xfield connectivity structure (for traversing cells to traces)
   *
   * output:
   *   a local xfield which is ready for cells to be added
   *   i.e. you need to call "addResolveElement", "addFixedElement" (or "addNeighbours")
   *        and finally call "extract" to do all the dirty work
   *
  \*/
  XField_LocalPatchConstructor( mpi::communicator& comm,
                                const XField_CellToTrace<PhysDim,TopoDim>& connectivity,
                                int cellGroupGlobal, int elem,
                                SpaceType type = SpaceType::Discontinuous, const Field_NodalView* xfld_nodalView=NULL );

  /*\
   * XField_LocalPatch constructor:
   *   constructor for EXTRACTION,
   *   use addResolveElement and addFixedElement to extract the local patch
   *
   * input:
   *   xfld_global:    global xfield from which we will extract elements
   *   connectivity:   xfield connectivity structure (for traversing cells to traces)
   *   xfld_nodalView: optional nodalView structure for quick node-to-cell lookup
   *
   * output:
   *   a local xfield which is ready for cells to be added
   *   i.e. you need to call "addResolveElement", "addFixedElement" (or "addNeighbours")
   *        and finally call "extract" to do all the dirty work
   *
  \*/
  XField_LocalPatchConstructor( mpi::communicator& comm,
                                const XField<PhysDim,TopoDim>& xfld_global,
                                const XField_CellToTrace<PhysDim,TopoDim>& connectivity,
                                const Field_NodalView* xfld_nodalView=NULL );

  /*\
   * XField_LocalPatch constructor:
   *   constructor for EXTRACTION,
   *   use addResolveElement and addFixedElement to extract the local patch attached to an edge
   *
   * input:
   *   connectivity:   xfield connectivity structure (for traversing cells to traces)
   *
   * output:
   *   a local xfield which is ready for cells to be added
   *   i.e. you need to call "addResolveElement", "addFixedElement" (or "addNeighbours")
   *        and finally call "extract" to do all the dirty work
   *
  \*/
  XField_LocalPatchConstructor( mpi::communicator& comm,
                                const XField_CellToTrace<PhysDim,TopoDim>& connectivity,
                                const std::array<int,2> edge, const Field_NodalView* xfld_nodalView );

  /*\
   * XField_LocalPatch constructor:
   *   constructor for EXTRACTION and SPLIT
   *   Does a single step recursion if isSplit is true, calling itself with false to make an unsplit then splitting that
   *
   *   use addResolveElement and addFixedElement to extract the local patch attached to an edge
   *
   * input:
   *   connectivity:   xfield connectivity structure (for traversing cells to traces)
   *
   * output:
   *   a local xfield which is ready for cells to be added
   *   i.e. you need to call "addResolveElement", "addFixedElement" (or "addNeighbours")
   *        and finally call "extract" to do all the dirty work
   *
  \*/
  XField_LocalPatchConstructor( mpi::communicator& comm,
                                const XField_CellToTrace<PhysDim,TopoDim>& connectivity,
                                const std::array<int,2> edge, SpaceType type,
                                const Field_NodalView* xfld_nodalView );

  /*\
   *  addResolveElement:
   *    add an element on which we want to resolve the DOF
   *
   *  input:
   *       group: cell group identifier of the global xfield
   *       elem: element within the specified cell group of the global xfield
   *
  \*/
  void addResolveElement( int group, int elem );

  /*\
   *  addFixedElement:
   *    add an element on which we want to fix the DOF
   *
   *  input:
   *       group: cell group identifier of the global xfield
   *       elem: element within the specified cell group of the global xfield
   *
  \*/
  void addFixedElement( int group, int elem );

  /*\
   *  addSharedTraceNeighbours:
   *    add all direct (trace) neighbours of the elem in specified group
   *    this is simply a helper function for DG local xfield extraction
   *
   *  input: []
   *
   *  output:
   *       the direct neighbours of the element are added to FIXED_CELL_GROUP
   *       the xfield structure is NOT filled, you still need to call "extract"
  \*/
  void addSharedTraceNeighbours();

  /*\
   *  addSharedNodeNeighbours:
   *    add all elements attached to the vertices of the resolve cell group
   *    this is simply a helper function for CG local xfield extraction
   *
   *  input: []
   *
   *  output:
   *       the elements touching the vertices of are added to FIXED_CELL_GROUP
   *       the xfield structure is NOT filled, you still need to call "extract"
  \*/
  void addSharedNodeNeighbours();

  /*\
   *  extract:
   *    performs the full extraction of the local patch and sets up
   *    the local xfield by constructing the interior and boundary
   *    trace groups. Trace groups will subsequently be ordered
   *    as defined in the documentation (Documents/LocalSolves.pdf)
   *
   *  input:
   *    there is no input because the resolve and fixed elements
   *    should already have been added
   *
   *  output:
   *    the entire XField structure is filled after creating
   *    temporary LagrangeElementGroup's, checkGrid is then
   *    used to check the validity of the produced xfield.
   *
  \*/
  void extract();

  /*\
   * boundaryGroup:
   *  returns the boundary group associated with the pair {global_bgroup,cell_group}
   *  (creates the group if necessary).
   *  It is important to associate the {global_bgroup,cell_group} pair to a local
   *  boundary group (not just global_bgroup to local_bgroup) so that different
   *  local cell groups (resolve or fixed) with the same global_bgroup touch
   *  unique local boundary groups (arises for CG)...this is a consequence of
   *  using XField_Lagrange to build the the boundary traces.
   *
   * input:
   *  global_bgroup: the bgroup id of the global xfield
   *  cell_group:    the local cell group (either RESOVLE or FIXED)
   *
   * output:
   *  reference to the local LagrangeConnectedGroup (either existing or newly created)
   *  associated with the input pair.
   *
  \*/
  LagrangeConnectedGroup& boundaryGroup( int global_bgroup, int cell_group );

  using BaseType::addBoundaryTrace;

  // dof functions
  int createDOF( int global_dof );

  int nFixed() const { return fixedElements_.size(); }
  int nResolve() const { return resolveElements_.size(); }

  // element functions
  void getElement( int local_group, int local_elem, std::vector<int>& DOF) const;

  bool isBoundaryTrace( const TraceInfo& info, int group, int elem ) const;

private:

  using BaseType::spaceType_;

  using BaseType::DOF_buffer_; // Buffer of DOFs mapped back to the global grid (per processor)
  using BaseType::cellGroups_;

  // we need this to finalize the mesh as in XField_impl.h
  using XField_Type::cellIDs_;
  using XField_Type::boundaryTraceIDs_;

  // map from global xfield to local dof
  using BaseType::global2localDOFmap_; // maps global DOF index to a local DOF index

  // things that will get stuffed upon cell and trace processing
  // we'll build the xfield groups from these
  using BaseType::interiorLagrangeGroups_;
  using BaseType::boundaryLagrangeGroups_;
  using BaseType::ghostBoundaryLagrangeGroups_;

  // full list of local elements in the RESOLVE_CELL_GROUP
  std::set<int> resolveElements_;

  // full list of local elements in the FIXED_CELL_GROUP
  std::set<int> fixedElements_;

  // the global xfield from which the local mesh is extracted and its connectivity structure
  using BaseType::xfld_global_;

  // classification of boundary traces when a local xfield is EXTRACTED
  void classifyBoundaryTraces( const std::vector< UniqueTraceMap >& uniqueTraces );

  // functions for creating the ElemSplitInfo for either extracted or split xfields
  void findTraceMappings_unsplit();

  using BaseType::connectivity_;
  using BaseType::xfld_nodalView_;

  // map from local pairs of (group,elem) to global pairs of (group,elem)
  std::map< std::pair<int,int>, std::pair<int,int> > local2globalElemMap_;

  // map from {global boundary groups and local cell groups} to local boundary group
  // we need the association with local cell groups specifically for CG local solves
  // where the same global boundary group might touch both resolve and fixed cell groups
  using BaseType::boundaryGroupMap_;
  using BaseType::local2globalBoundaryGroupMap_;

  // the main cell dof which will be used for splitting
  using BaseType::mainCellNodes_;

  using BaseType::nElem_;

  using BaseType::edgeMode_;
  using BaseType::linearCommonNodes_;
};

} // SANS

#endif // SANS_FIELD_LOCAL_PATCH_CONSTRUCTOR_H_
