// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SANS_FIELD_LOCAL_PATCH_BASE_H_
#define SANS_FIELD_LOCAL_PATCH_BASE_H_

#include <map>
#include <set>

#include "Field/Partition/LagrangeElementGroup.h"
#include "Field/XField_CellToTrace.h"
#include "Field/SpaceTypes.h"

#include "XField_Local_Base.h"

namespace SANS
{

template<class PhysDim,class TopoDim> class XField_CellToTrace;
class Field_NodalView;

template<class,class> class TraceSoup;

template<class PhysDim, class Topology>
class XField_LocalPatchBase : public XField_Local_Base<PhysDim,typename Topology::TopoDim>
{
protected:
  typedef XField_Local_Base<PhysDim,typename Topology::TopoDim> BaseType;
  typedef DLA::VectorS<PhysDim::D,Real> VectorX;

  struct GlobalDOFType
  {
    GlobalDOFType() : idof(-1) {}
    GlobalDOFType(const int idof, const VectorX& X) : idof(idof), X(X) {}

    int idof;  // global DOF index
    VectorX X; // grid coordinate
  };

  typedef typename Topology::TopologyTrace TraceTopology;
  typedef typename Topology::TopoDim TopoDim;

  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TraceTopology> XFieldTraceGroupType;

  typedef XField<PhysDim,TopoDim> XField_Type;

  // some labels for readability
  const int RESOLVE_CELL_GROUP = 0;
  const int FIXED_CELL_GROUP = 1;
  const int STRAIGHT_SIDED = 1;

  /*\
   * XField_LocalPatchBase constructor:
   *   constructor for EXTRACTION,
   *   use addResolveElement and addFixedElement to extract the local patch
   *
   * input:
   *   xfld_global:    global xfield from which we will extract elements
   *   connectivity:   xfield connectivity structure (for traversing cells to traces)
   *   xfld_nodalView: optional nodalView structure for quick node-to-cell lookup
   *
   * output:
   *   a local xfield which is ready for cells to be added
   *   i.e. you need to call "addResolveElement", "addFixedElement" (or "addNeighbours")
   *        and finally call "extract" to do all the dirty work
   *
  \*/
  XField_LocalPatchBase( std::shared_ptr<mpi::communicator> comm,
                         const XField<PhysDim,TopoDim>& xfld_global,
                         const XField_CellToTrace<PhysDim,TopoDim>& connectivity,
                         const Field_NodalView* xfld_nodalView );

  /*\
   * finalize:
   *   finalizes the xfield structure but stuffing the LagrangeElementGroup's
   *   into the appropriate cell, boundary and interior trace groups,
   *   the DOF are also officially added to the inherited XField structure
   *
   * input:
   *   the LagrangeElementGroups need to be filled
   *
   * output:
   *   the entire XField structure is built and checkGrid is called
  \*/
  void finalize();

  /*\
   * addBoundaryTrace:
   *  adds a boundary trace that was leftover from a particular cellGroup
   *  into the appropriate local boundary group.
   *  this is used in both EXTRACT and SPLIT modes where the bgroup is
   *  determined by the caller by either examining the connectivity info (EXTRACT mode)
   *  or some associativity saved from the local xfield extraction (SPLIT mode)
   *
  \*/
  void addBoundaryTrace( int cellGroup, const UniqueTrace& trace, LagrangeConnectedGroup& bgroup );

  void setResolveGroups();

public:
  const std::vector<GlobalDOFType>& DOF_buffer() const { return DOF_buffer_; }

  // element functions
  void addElement( int local_group, int local_elem, const std::vector<int>& DOF );

  // local structure retrieval
  const XField<PhysDim,TopoDim>& xfld_global() const { return xfld_global_; }
  const XField_CellToTrace<PhysDim,TopoDim>& connectivity() const { return connectivity_; }
  const Field_NodalView* xfld_nodalView() const { return xfld_nodalView_; }

  // retrieves the global cell dof (so we can identify which edge to split)
  const std::vector<int>& mainCellNodes() const { return mainCellNodes_; }

  const std::map<int,int>& global2localDOFmap() const { return global2localDOFmap_; }
  int global2localDOFmap(const int iglobal) const { return global2localDOFmap_.at(iglobal); }

  const SpaceType& spaceType() const { return spaceType_; }

  // now in XField_Local_Base
  // int globalBoundaryGroup( int local_group ) const { return local2globalBoundaryGroupMap_.at(local_group); }

  // only used for unit testing
  std::vector<int> globalBoundaryGroups() const
  {
    std::set<int> groups;
    for (std::map<std::pair<int,int>,int>::const_iterator it=boundaryGroupMap_.begin();it!=boundaryGroupMap_.end();it++)
      groups.insert( it->first.first );
    return std::vector<int>(groups.begin(),groups.end());
  }

  const std::map< int, int >& local2globalBoundaryGroupMap() const { return local2globalBoundaryGroupMap_; }
  const std::map< std::pair<int,int>, int >& boundaryGroupMap() const { return boundaryGroupMap_; }


  // prints things for debugging
  void dump() const;

  // return the typeID, for run time checking
  virtual const std::type_info& derivedTypeID() const { return typeid(XField_LocalPatchBase); }

  // Resolving the diamond inheritence pattern, without requiring the Topology template

  using BaseType::nCellGroups;
  using BaseType::nInteriorTraceGroups;
  using BaseType::nBoundaryTraceGroups;
  using BaseType::nGhostBoundaryTraceGroups;

  // The default versions with the Topology Template still there
  using BaseType::getCellGroup;
  using BaseType::getInteriorTraceGroup;
  using BaseType::getBoundaryTraceGroup;
  using BaseType::getGhostBoundaryTraceGroup;

  // Some stripped away versions
        XFieldCellGroupType& getCellGroup( const int group )
        { return BaseType::template getCellGroup<Topology>(group); }
  const XFieldCellGroupType& getCellGroup( const int group ) const
        { return BaseType::template getCellGroup<Topology>(group); }

        XFieldTraceGroupType& getInteriorTraceGroup( const int group )
        { return BaseType::template getInteriorTraceGroup<typename Topology::TopologyTrace>(group); }
  const XFieldTraceGroupType& getInteriorTraceGroup( const int group ) const
        { return BaseType::template getInteriorTraceGroup<typename Topology::TopologyTrace>(group); }

        XFieldTraceGroupType& getBoundaryTraceGroup( const int group )
        { return BaseType::template getBoundaryTraceGroup<typename Topology::TopologyTrace>(group); }
  const XFieldTraceGroupType& getBoundaryTraceGroup( const int group ) const
        { return BaseType::template getBoundaryTraceGroup<typename Topology::TopologyTrace>(group); }

        // XFieldTraceGroupType& getGhostBoundaryTraceGroup( const int group )
        // { return BaseType::template getGhostBoundaryTraceGroup<typename Topology::TopologyTrace>(group); }
  const XFieldTraceGroupType& getGhostBoundaryTraceGroup( const int group ) const
        { return BaseType::template getGhostBoundaryTraceGroup<typename Topology::TopologyTrace>(group); }

  /*
   * START OF EDGE AND NODE PATCH ASSOCIATED FUNCTIONS
   */

  bool isEdgeMode() const { return edgeMode_; };

  std::vector<int> getLinearCommonNodes() const
    { SANS_ASSERT_MSG( edgeMode_, "Should only be called in edge mode" );  return linearCommonNodes_; }
  std::vector<int> getLocalLinearCommonNodes() const
    { SANS_ASSERT_MSG( edgeMode_, "Should only be called in edge mode" ); return getLocalNodes(linearCommonNodes_); }

  std::vector<int> getLocalNodes( const std::vector<int>& globalNodes) const
  {
    std::vector<int> localNodes;
    localNodes.reserve(globalNodes.size());
    for (auto const& node : globalNodes )
    {
      const int localNode = global2localDOFmap_.at(node);
      localNodes.push_back(localNode);
    }
    return localNodes;
  }

  // const std::map<int,int>& global2localDOFmap() const { return global2localDOFmap_; }

  /*
   * END OF EDGE AND NODE PATCH ASSOCIATED FUNCTIONS
   */

  int order() const { return order_; }

protected:

  SpaceType spaceType_ = SpaceType::Discontinuous; // default to DG

  // the order of the cell groups in the global xfield which is transferred to the local one
  int order_;

  // stuff need from XField_Lagrange
  std::vector<GlobalDOFType> DOF_buffer_; // Buffer of DOFs mapped back to the global grid (per processor)
  std::vector<LagrangeElementGroup_ptr> cellGroups_;

  // we need this to finalize the mesh as in XField_impl.h
  using XField_Type::cellIDs_;
  using XField_Type::boundaryTraceIDs_;

  // map from global xfield to local dof
  std::map<int,int> global2localDOFmap_; // maps global DOF index to a local DOF index

  // things that will get stuffed upon cell and trace processing
  // we'll build the xfield groups from these
  std::vector<LagrangeConnectedGroup> interiorLagrangeGroups_;
  std::vector<LagrangeConnectedGroup> boundaryLagrangeGroups_;
  std::vector<LagrangeConnectedGroup> ghostBoundaryLagrangeGroups_;

  // the global xfield from which the local mesh is extracted and its connectivity structure
  const XField<PhysDim,TopoDim>& xfld_global_;

  const XField_CellToTrace<PhysDim,TopoDim>& connectivity_;
  const Field_NodalView* xfld_nodalView_;

  // map from local pairs of (group,elem) to global pairs of (group,elem)
  std::map< std::pair<int,int>, std::pair<int,int> > local2globalElemMap_;

  // map from {global boundary groups and local cell groups} to local boundary group
  // we need the association with local cell groups specifically for CG local solves
  // where the same global boundary group might touch both resolve and fixed cell groups
  std::map< std::pair<int,int>, int > boundaryGroupMap_;
  using BaseType::local2globalBoundaryGroupMap_;

  // the main cell dof which will be used for splitting
  std::vector<int> mainCellNodes_;

  int nElem_;


  /*
   * START OF EDGE AND NODE PATCH ASSOCIATED FUNCTIONS
   */

  // Is this the edge mode? Namely is cell group 0 all attached to an edge
  bool edgeMode_ = false; // default to element mode
  std::vector<int> linearCommonNodes_; // the nodes common to cell group 0, in global coordinates

  /*
   * END OF EDGE AND NODE PATCH ASSOCIATED FUNCTIONS
   */
};

} // SANS

#endif
