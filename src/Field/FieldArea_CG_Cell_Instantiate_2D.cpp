// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define FIELDAREA_CG_CELL_INSTANTIATE
#include "FieldArea_CG_Cell_impl.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"

#include "XFieldArea.h"
#include "FieldArea.h"

#include "Surreal/SurrealS.h"

namespace SANS
{

template class Field_CG_Cell< PhysD2, TopoD2, Real >;
template class Field_CG_Cell< PhysD2, TopoD2, SurrealS<1> >;

template class Field_CG_Cell< PhysD2, TopoD2, DLA::VectorS<2, Real> >;
template class Field_CG_Cell< PhysD2, TopoD2, DLA::VectorS<3, Real> >;
template class Field_CG_Cell< PhysD2, TopoD2, DLA::VectorS<4 ,Real> >;
template class Field_CG_Cell< PhysD2, TopoD2, DLA::VectorS<5, Real> >;
template class Field_CG_Cell< PhysD2, TopoD2, DLA::VectorS<6, Real> >;
template class Field_CG_Cell< PhysD2, TopoD2, DLA::VectorS<9, Real> >;

template class Field_CG_Cell< PhysD2, TopoD2, DLA::MatrixS<3, 3, Real> >;
template class Field_CG_Cell< PhysD2, TopoD2, DLA::MatrixS<3, 3, SurrealS<1> > >;

template class Field_CG_Cell< PhysD2, TopoD2, DLA::MatrixSymS<2, Real> >;

template class Field_CG_Cell< PhysD2, TopoD2, DLA::VectorS<2, DLA::VectorS<2, Real> > >;
}
