// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELDAREA_TRAITS_H
#define XFIELDAREA_TRAITS_H

#include "tools/SANSnumerics.h"     // Real

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "Field/Element/ElementAssociativityArea.h"
#include "Field/Element/ElementXFieldArea.h"

#include "XFieldTypes.h"
#include "ElementConnectivity.h"
#include "FieldAssociativityConstructor.h"


namespace SANS
{
//----------------------------------------------------------------------------//
// Area grid field traits in a topologically 2D grid
//----------------------------------------------------------------------------//

template<class PhysDim>
struct XFieldAreaTraits<PhysDim, TopoD2>
{
  static const int D = PhysDim::D;                       // physical dimensions

  typedef DLA::VectorS<D,Real> VectorX;        // coordinates vector

  typedef FieldAssociativityBase< VectorX > FieldBase;

  typedef VectorX T;                                  // DOF type
};

template<class PhysDim, class Topology>
struct XFieldGroupAreaTraits<PhysDim, TopoD2, Topology> : public XFieldAreaTraits<PhysDim, TopoD2>
{
  typedef typename XFieldAreaTraits<PhysDim, TopoD2>::VectorX VectorX;        // coordinates vector

  typedef Topology TopologyType;

  typedef ElementAssociativity<TopoD2,Topology> ElementAssociativityType;

  typedef BasisFunctionAreaBase<Topology> BasisType;

  typedef FieldAssociativityConstructor< typename ElementAssociativityType::Constructor > FieldAssociativityConstructorType;

  template<class ElemT = Real>
  struct ElementType : public ElementXField<PhysDim, TopoD2, Topology>
  {
    typedef ElementXField<PhysDim, TopoD2, Topology> BaseType;

    explicit ElementType( const BasisType* basis ) : BaseType(basis) {}
    ElementType( int order, const BasisFunctionCategory& category ) : BaseType(order,category) {}
    ElementType& operator=( const ElementType& elem ) { BaseType::operator=(elem); return *this; }
  };
};


//----------------------------------------------------------------------------//
// Area grid field traits in a topologically 3D grid
//----------------------------------------------------------------------------//

template<class PhysDim>
struct XFieldAreaTraits<PhysDim, TopoD3>
{
  static const int D = PhysDim::D;                       // physical dimensions

  typedef DLA::VectorS<D,Real> VectorX;        // coordinates vector

  typedef ElementConnectivityBase< VectorX > FieldBase;

  typedef VectorX T;                                  // DOF type
};

template<class PhysDim, class Topology>
struct XFieldGroupAreaTraits<PhysDim, TopoD3, Topology> : public XFieldAreaTraits<PhysDim, TopoD3>
{
  typedef typename XFieldAreaTraits<PhysDim, TopoD3>::VectorX VectorX;        // coordinates vector

  typedef Topology TopologyType;

  typedef ElementAssociativity<TopoD2,Topology> ElementAssociativityType;

  typedef BasisFunctionAreaBase<Topology> BasisType;

  typedef ElementConnectivityConstructor< typename ElementAssociativityType::Constructor > FieldAssociativityConstructorType;

  template<class ElemT = Real>
  struct ElementType : public ElementXField<PhysDim, TopoD2, Topology>
  {
    typedef ElementXField<PhysDim, TopoD2, Topology> BaseType;

    ElementType( const ElementType& elem ) : BaseType(elem) {}
    explicit ElementType( const BasisType* basis ) : BaseType(basis) {}
    ElementType( int order, const BasisFunctionCategory& category ) : BaseType(order,category) {}
    ElementType& operator=( const ElementType& elem ) { BaseType::operator=(elem); return *this; }
  };
};

}

#endif  // XFIELDAREA_TRAITS_H
