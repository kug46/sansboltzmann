// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELD_DG_CELLBASE_H
#define FIELD_DG_CELLBASE_H

#include "BasisFunction/BasisFunctionCategory.h"
#include "Field.h"

#include <memory> // std::unique_ptr

#include "tools/split_cat_std_vector.h" // SANS::cat (in derived constructors)

namespace SANS
{

struct MPIElement;

//----------------------------------------------------------------------------//
// solution field: DG cell-field constructor class
//----------------------------------------------------------------------------//

template <class PhysDim, class TopoDim, class T>
class Field_DG_CellBase : public Field< PhysDim, TopoDim, T >
{
public:
  typedef Field< PhysDim, TopoDim, T > BaseType;
  typedef T ArrayQ;

  Field_DG_CellBase() = delete;
  virtual ~Field_DG_CellBase() {};

  Field_DG_CellBase& operator=( const Field_DG_CellBase& ) = delete;

  virtual int nDOFCellGroup(int cellgroup) const override;
  virtual int nDOFInteriorTraceGroup(int tracegroup) const override;
  virtual int nDOFBoundaryTraceGroup(int tracegroup) const override;

  // Virtual class to denote the SpaceType of the Field
  virtual SpaceType spaceType() const override { return SpaceType::Discontinuous; }

protected:
  //Protected constructor. This is a helper class
  explicit Field_DG_CellBase( const XField<PhysDim, TopoDim>& xfld );

  Field_DG_CellBase( const Field_DG_CellBase& fld, const FieldCopy& tag );

  // counts the number of ghost cells around each cell
  void
  countGhostNeighbors(const std::vector<int>& CellGroups,
                      std::vector<std::vector<int>>& nGhostNeigbor,
                      std::vector<std::vector<bool>>& isZombie);

  // Initializes the construction of a new cell group
  template<class Topology>
  std::unique_ptr<FieldAssociativityConstructorBase>
  createCellGroup( const int groupGlobal, const int order, const BasisFunctionCategory& category,
                   int& nDOFpossessed, int& nBasis );

  // Adds ghost DOF indexing and finalizes cell groups
  template<class Topology>
  void
  setGhostCellDOFs(const int groupGlobal,
                   const std::vector<std::vector<bool>>& isZombie, const bool setZombie,
                   int& nDOF, std::unique_ptr<FieldAssociativityConstructorBase>& pfldAssocCell);

  // allocate the DOF array
  void createDOFs(const int nDOF, const int nDOFpossessed,
                  const std::vector<std::vector<bool>>& isZombie, const std::vector<int>& nBasisGroups);

  using BaseType::nDOF_;
  using BaseType::DOF_;
  using BaseType::nDOFpossessed_;
  using BaseType::nDOFghost_;
  using BaseType::nElem_;
  using BaseType::cellGroups_;
  using BaseType::localCellGroups_;
  using BaseType::xfld_;
};

}

#endif  // FIELD_DG_CELLBASE_H
