// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDAREA_H
#define FIELDAREA_H

#include "Field.h"
#include "FieldGroupLine_Traits.h"
#include "FieldGroupArea_Traits.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Topologically 2D field variables
//----------------------------------------------------------------------------//

template<class T_>
struct FieldTraits<TopoD2, T_>
{
  typedef TopoD2 TopoDim;
  typedef T_ T;                                     // DOF type

  typedef typename FieldGroupLineTraits<T>::FieldBase FieldTraceGroupBase;
  typedef typename FieldGroupAreaTraitsBase<T>::FieldBase FieldCellGroupBase;

  template<class Topology>
  using FieldTraceGroupType = FieldAssociativity< FieldGroupLineTraits<T> >;

  template<class Topology>
  using FieldCellGroupType = FieldAssociativity< FieldGroupAreaTraits<Topology, T> >;
};

} //namespace SANS

#endif //FIELDAREA_H
