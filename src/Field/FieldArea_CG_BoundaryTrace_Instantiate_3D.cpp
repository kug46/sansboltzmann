// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define FIELDAREA_CG_BOUNDARYTRACE_INSTANTIATE
#include "FieldArea_CG_BoundaryTrace_impl.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "XFieldArea.h"
#include "FieldArea.h"

namespace SANS
{

template class Field_CG_BoundaryTrace< PhysD3, TopoD2, Real >;
#if 0
template class Field_CG_BoundaryTrace< PhysD2, TopoD2, DLA::VectorS<2, Real> >;
template class Field_CG_BoundaryTrace< PhysD2, TopoD2, DLA::VectorS<3, Real> >;
template class Field_CG_BoundaryTrace< PhysD2, TopoD2, DLA::VectorS<4 ,Real> >;
template class Field_CG_BoundaryTrace< PhysD2, TopoD2, DLA::VectorS<5, Real> >;
template class Field_CG_BoundaryTrace< PhysD2, TopoD2, DLA::VectorS<6, Real> >;
#endif

}
