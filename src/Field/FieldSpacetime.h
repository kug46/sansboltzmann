// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDSPACETIME_H
#define FIELDSPACETIME_H

#include "FieldGroupVolume_Traits.h"
#include "FieldGroupSpacetime_Traits.h"

#include "Field.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Topologically 4D field variables
//----------------------------------------------------------------------------//

template<class Tin>
struct FieldTraits<TopoD4, Tin>
{
  typedef TopoD4 TopoDim;
  typedef Tin T;                                     // DOF type

  typedef typename FieldGroupVolumeTraitsBase<T>::FieldBase FieldTraceGroupBase;
  typedef typename FieldGroupSpacetimeTraitsBase<T>::FieldBase FieldCellGroupBase;

  template<class Topology>
  using FieldTraceGroupType = FieldAssociativity< FieldGroupVolumeTraits<Topology, T> >;

  template<class Topology>
  using FieldCellGroupType = FieldAssociativity< FieldGroupSpacetimeTraits<Topology, T> >;
};

} //namespace SANS

#endif //FIELDVOLUME_H
