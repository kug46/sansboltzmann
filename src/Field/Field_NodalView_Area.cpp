// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "Field_NodalView_impl.h"
#include "XFieldArea.h"
#include "FieldArea_CG_Cell.h"

#include <algorithm> //std::sort

namespace SANS
{

//=============================================================================
template <class FieldTraits>
void
Field_NodalView::init(const FieldBase<FieldTraits>& fld, const std::vector<int>& cellgroup_list)
{
  std::size_t nCellGroups = cellgroup_list.size();

  std::map<int, std::set<GroupElemIndex>> NodeDOF2CellMap;
  std::map<int, std::set<std::pair<int, int>>> NodeDOF2EdgeMap;

  //Create connectivity structure
  for (std::size_t group = 0; group < nCellGroups; group++ )
  {
    const int cellGroupGlobal = cellgroup_list[group];

    int nElem = fld.getCellGroupBase(cellGroupGlobal).nElem();

    if (fld.getCellGroupBase(cellGroupGlobal).topoTypeID() == typeid(Triangle))
    {
      typedef typename FieldBase<FieldTraits>:: template FieldCellGroupType<Triangle> FieldCellGroupType;
      const FieldCellGroupType& fld_cellgrp = fld.template getCellGroup<Triangle>(cellGroupGlobal);

      int nodeDOFMap[Triangle::NNode];
      const int (*TraceNodes)[ Line::NNode ] = TraceToCellRefCoord<Line, TopoD2, Triangle>::TraceNodes;

      for (int elem = 0; elem < nElem; elem++)
      {
        fld_cellgrp.associativity(elem).getNodeGlobalMapping(nodeDOFMap, Triangle::NNode);

        this->insertCellNodes(nodeDOFMap, Triangle::NNode, cellGroupGlobal, elem, NodeDOF2CellMap);
        this->insertEdge(nodeDOFMap[TraceNodes[0][0]], nodeDOFMap[TraceNodes[0][1]], NodeDOF2EdgeMap); //Canonical trace 0
        this->insertEdge(nodeDOFMap[TraceNodes[1][0]], nodeDOFMap[TraceNodes[1][1]], NodeDOF2EdgeMap); //Canonical trace 1
        this->insertEdge(nodeDOFMap[TraceNodes[2][0]], nodeDOFMap[TraceNodes[2][1]], NodeDOF2EdgeMap); //Canonical trace 2
      }
    }
    else if (fld.getCellGroupBase(cellGroupGlobal).topoTypeID() == typeid(Quad))
    {
      typedef typename FieldBase<FieldTraits>:: template FieldCellGroupType<Quad> FieldCellGroupType;
      const FieldCellGroupType& fld_cellgrp = fld.template getCellGroup<Quad>(cellGroupGlobal);

      int nodeDOFMap[Quad::NNode];
      const int (*TraceNodes)[ Line::NNode ] = TraceToCellRefCoord<Line, TopoD2, Quad>::TraceNodes;

      for (int elem = 0; elem < nElem; elem++)
      {
        fld_cellgrp.associativity(elem).getNodeGlobalMapping(nodeDOFMap, Quad::NNode);

        this->insertCellNodes(nodeDOFMap, Quad::NNode, cellGroupGlobal, elem, NodeDOF2CellMap);
        this->insertEdge(nodeDOFMap[TraceNodes[0][0]], nodeDOFMap[TraceNodes[0][1]], NodeDOF2EdgeMap); //Canonical trace 0
        this->insertEdge(nodeDOFMap[TraceNodes[1][0]], nodeDOFMap[TraceNodes[1][1]], NodeDOF2EdgeMap); //Canonical trace 1
        this->insertEdge(nodeDOFMap[TraceNodes[2][0]], nodeDOFMap[TraceNodes[2][1]], NodeDOF2EdgeMap); //Canonical trace 2
        this->insertEdge(nodeDOFMap[TraceNodes[3][0]], nodeDOFMap[TraceNodes[3][1]], NodeDOF2EdgeMap); //Canonical trace 3
      }
    }
    else
      SANS_DEVELOPER_EXCEPTION("Field_NodalView<PhysDim, TopoD2> - Unknown Topology type for CellGroup.");
  }

  std::sort(NodeDOFList_.begin(), NodeDOFList_.end());

  this->setMaps( NodeDOF2CellMap, NodeDOF2EdgeMap );
  this->updateInverseNodeMap(fld.nDOF());
}


//=============================================================================
//Explicitly instantiate
template Field_NodalView::Field_NodalView(const XField<PhysD2, TopoD2>& xfld, const std::vector<int>& cellgroup_list);

template Field_NodalView::Field_NodalView(const Field_CG_Cell<PhysD2, TopoD2, Real>& fld);
template Field_NodalView::Field_NodalView(const Field_CG_Cell<PhysD2, TopoD2, DLA::MatrixSymS<2,Real>>& fld);
template Field_NodalView::Field_NodalView(const Field_CG_Cell<PhysD2, TopoD2, DLA::MatrixSymS<2,Real>>& fld,
                                          const std::vector<int>& cellgroup_list);

}
