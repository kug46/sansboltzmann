// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDLIFTLINE_DG_CELL_H
#define FIELDLIFTLINE_DG_CELL_H

#include <vector>

#include "FieldGroupNode_Traits.h"
#include "FieldLiftGroupLine_Traits.h"
#include "FieldLift_DG_CellBase.h"

#include "Topology/Dimension.h"
#include "BasisFunction/BasisFunctionCategory.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Topological 1D lifting operator field traits
//----------------------------------------------------------------------------//

template<class Tin>
struct FieldLiftTraits<TopoD1, Tin>
{
  typedef TopoD1 TopoDim;
  typedef Tin T;                                     // DOF type

  typedef typename FieldGroupNodeTraits<T>::FieldBase FieldTraceGroupBase;
  typedef typename FieldLiftGroupLineTraits<T>::FieldBase FieldCellGroupBase;

  template<class Topology>
  using FieldTraceGroupType = FieldAssociativity< FieldGroupNodeTraits<T> >;

  template<class Topology>
  using FieldCellGroupType = FieldLiftAssociativity< FieldLiftGroupLineTraits<T> >;
};


//----------------------------------------------------------------------------//
// 1-D solution field: DG cell-field
//----------------------------------------------------------------------------//

template <class PhysDim, class T>
class FieldLift_DG_Cell<PhysDim, TopoD1, T> : public FieldLift_DG_CellBase< PhysDim, TopoD1, T >
{
public:
  typedef FieldLift_DG_CellBase< PhysDim, TopoD1, T > BaseType;
  typedef T ArrayQ;

  FieldLift_DG_Cell( const XField<PhysDim, TopoD1>& xfld, const int order, const BasisFunctionCategory& category );

  FieldLift_DG_Cell( const XField<PhysDim, TopoD1>& xfld, const int order, const BasisFunctionCategory& category,
                     const std::vector<int>& CellGroups );

  FieldLift_DG_Cell( const FieldLift_DG_Cell& fld, const FieldCopy& tag );

  FieldLift_DG_Cell& operator=( const ArrayQ& q );

protected:
  void init( const int order, const BasisFunctionCategory& category, const std::vector<int>& CellGroups );

  using BaseType::nDOF_;
  using BaseType::DOF_;
  using BaseType::cellGroups_;
  using BaseType::xfld_;
  using BaseType::nElem_;
};

}

#endif  // FIELDLIFTLINE_DG_CELL_H
