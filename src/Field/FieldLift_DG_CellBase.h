// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDLIFT_DG_CELLBASE_H
#define FIELDLIFT_DG_CELLBASE_H

#include "BasisFunction/BasisFunctionCategory.h"
#include "FieldLift.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// lifting operator field: DG cell-field constructor class
//----------------------------------------------------------------------------//

template <class PhysDim, class TopoDim, class T>
class FieldLift_DG_CellBase : public FieldLift< PhysDim, TopoDim, T >
{
public:
  typedef FieldLift< PhysDim, TopoDim, T > BaseType;
  typedef T ArrayQ;

  FieldLift_DG_CellBase() = delete;
  virtual ~FieldLift_DG_CellBase() {};

  FieldLift_DG_CellBase& operator=( const FieldLift_DG_CellBase& ) = delete;

  FieldLift_DG_CellBase& operator=( const ArrayQ& q );

  virtual int nDOFCellGroup(int cellgroup) const override;
  virtual int nDOFInteriorTraceGroup(int tracegroup) const override;
  virtual int nDOFBoundaryTraceGroup(int tracegroup) const override;

protected:
  //Protected constructor. This is a helper class
  explicit FieldLift_DG_CellBase( const XField<PhysDim, TopoDim>& xfld );

  FieldLift_DG_CellBase( const FieldLift_DG_CellBase& fld, const FieldCopy& tag );

  // Constructs a new cell group
  template<class Topology>
  void createCellGroup(const int group, const int order, const BasisFunctionCategory& category);

  void createDOFs();

  using BaseType::nDOF_;
  using BaseType::DOF_;
  using BaseType::cellGroups_;
  using BaseType::localCellGroups_;
  using BaseType::xfld_;
  using BaseType::nElem_;
};

}

#endif  // FIELDLIFT_DG_CELLBASE_H
