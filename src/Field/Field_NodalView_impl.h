// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "Field_NodalView.h"

namespace SANS
{

template <class PhysDim, class TopoDim>
Field_NodalView::Field_NodalView(const XField<PhysDim, TopoDim>& xfld, const std::vector<int>& cellgroup_list)
{
  init(xfld, cellgroup_list);
}

template <class PhysDim, class TopoDim, class T>
Field_NodalView::Field_NodalView(const Field_CG_Cell<PhysDim, TopoDim, T>& fld )
{
  init(fld, fld.getGlobalCellGroups());
}

template <class PhysDim, class TopoDim, class T>
Field_NodalView::Field_NodalView(const Field_CG_Cell<PhysDim, TopoDim, T>& fld, const std::vector<int>& cellgroup_list )
{
  init(fld, cellgroup_list);
}

}
