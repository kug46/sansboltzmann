// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define FIELD_DG_CELLBASE_INSTANTIATE
#include "Field_DG_CellBase_impl.h"

#define FIELDSPACETIME_DG_CELL_INSTANTIATE
#include "FieldSpacetime_DG_Cell_impl.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"

#include "XFieldSpacetime.h"

namespace SANS
{

template class Field_DG_Cell< PhysD4, TopoD4, Real >;

template class Field_DG_Cell< PhysD4, TopoD4, DLA::VectorS<2, Real> >;
//template class Field_DG_Cell< PhysD4, TopoD4, DLA::VectorS<3, Real> >;
template class Field_DG_Cell< PhysD4, TopoD4, DLA::VectorS<4, Real> >;
//template class Field_DG_Cell< PhysD4, TopoD4, DLA::VectorS<5, Real> >;
//template class Field_DG_Cell< PhysD4, TopoD4, DLA::VectorS<6, Real> >;
//template class Field_DG_Cell< PhysD4, TopoD4, DLA::VectorS<7, Real> >;
template class Field_DG_Cell< PhysD4, TopoD4, DLA::VectorS<10, Real> >;

//template class Field_DG_Cell< PhysD4, TopoD4, DLA::VectorS< 4, DLA::VectorS<5, Real> > >;

template class Field_DG_Cell< PhysD4, TopoD4, DLA::MatrixSymS<4, Real> >;

}
