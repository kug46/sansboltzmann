// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(FIELDVOLUME_DG_INTERIORTRACE_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "FieldVolume_DG_InteriorTrace.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// 3D solution field: DG interior trace
//----------------------------------------------------------------------------//

template <class PhysDim, class T>
Field_DG_InteriorTrace<PhysDim, TopoD3, T>::Field_DG_InteriorTrace( const XField<PhysDim, TopoD3>& xfld,
                                                                    const int order, const BasisFunctionCategory& category )
  : BaseType( xfld )
{
  init(xfld,order,category,BaseType::createInteriorGroupIndex());
}

template <class PhysDim, class T>
Field_DG_InteriorTrace<PhysDim, TopoD3, T>::Field_DG_InteriorTrace( const XField<PhysDim, TopoD3>& xfld,
                                                                    const int order,
                                                                    const BasisFunctionCategory& category, const std::vector<int>& InteriorGroups )
  : BaseType( xfld )
{
  BaseType::checkInteriorGroupIndex(InteriorGroups);
  init(xfld,order,category,InteriorGroups);
}


template <class PhysDim, class T>
Field_DG_InteriorTrace<PhysDim, TopoD3, T>::Field_DG_InteriorTrace( const Field_DG_InteriorTrace& fld, const FieldCopy& tag )
  : BaseType( fld, tag )
{
}

template <class PhysDim, class T>
Field_DG_InteriorTrace<PhysDim, TopoD3, T>&
Field_DG_InteriorTrace<PhysDim, TopoD3, T>::operator=( const ArrayQ& q )
{
  Field< PhysDim, TopoD3, T >::operator=(q);
  return *this;
}

template <class PhysDim, class T>
void
Field_DG_InteriorTrace<PhysDim, TopoD3, T>::init(
    const XField<PhysDim, TopoD3>& xfld, const int order,
    const BasisFunctionCategory& category, const std::vector<int>& InteriorGroups )
{
  for (std::size_t igroup = 0; igroup < InteriorGroups.size(); igroup++)
  {
    const int group = InteriorGroups[igroup];

    if ( xfld.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Triangle) )
      this->template createInteriorTraceGroup<Triangle>(group, order, category);
    else if ( xfld.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Quad) )
      this->template createInteriorTraceGroup<Quad>(group, order, category);
    else
      SANS_DEVELOPER_EXCEPTION( "Field_DG_InteriorTrace<PhysDim, TopoDim, T>::Field_DG_InteriorTrace: Unknown element topology" );
  }

  // allocate the solution DOF array and assign it to groups
  this->createDOFs();
}

}
