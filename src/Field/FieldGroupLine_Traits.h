// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDGROUPLINE_TRAITS_H
#define FIELDGROUPLINE_TRAITS_H

#include "FieldTypes.h"
#include "FieldAssociativity.h"
#include "Field/Element/Element.h"
#include "Field/Element/ElementAssociativityLine.h"

#include "tools/Surrealize.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Traits for a field group of lines
//----------------------------------------------------------------------------//

template<class Tin>
struct FieldGroupLineTraits
{
  typedef FieldAssociativityBase<Tin> FieldBase;

  typedef Tin T;                                    // DOF type
  typedef BasisFunctionLineBase BasisType;
  typedef Line TopologyType;
  typedef ElementAssociativity<TopoD1,Line> ElementAssociativityType;
  typedef FieldAssociativityConstructor<ElementAssociativityType::Constructor> FieldAssociativityConstructorType;

  template<class ElemT = Real>
  using ElementType = Element< typename Surrealize<ElemT,Tin>::T, TopoD1, Line >;
};

} //namespace SANS

#endif //FIELDGROUPLINE_TRAITS_H
