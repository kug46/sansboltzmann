// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PROJECTSOLNTRACE_DISCONTINUOUS_H
#define PROJECTSOLNTRACE_DISCONTINUOUS_H

// Trace integral to project exact solution with discontinuous elements

#include "Topology/ElementTopology.h"

#include "Field/Field.h"
#include "Field/XField.h"

#include "Field/Element/ElementProjection_L2.h"

#include "Field/tools/GroupFunctorType.h"
#include "Field/Tuple/FieldTuple.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  Projects a solution function onto a discontinuous field
//

template<class SOLN>
class ProjectSolnTrace_Discontinuous_impl :
    public GroupFunctorInteriorTraceType< ProjectSolnTrace_Discontinuous_impl<SOLN> >
{
public:
  typedef typename SOLN::PhysDim PhysDim;
  typedef typename SOLN::template ArrayQ<Real> ArrayQ;

  // Save off the boundary trace integrand and the residual vectors
  ProjectSolnTrace_Discontinuous_impl( const SOLN& soln, const std::vector<int>& traceGroups ) :
    soln_(soln), traceGroups_(traceGroups) {}

  std::size_t nInteriorTraceGroups() const { return traceGroups_.size(); }
  std::size_t interiorTraceGroup(const int n) const { return traceGroups_[n]; }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the Trace group
  template <class TopologyTrace>
  void
  apply( const typename FieldTuple<XField<PhysDim, typename TopologyTrace::CellTopoDim>,
                                    Field<PhysDim, typename TopologyTrace::CellTopoDim, ArrayQ>, TupleClass<>>::
             template FieldTraceGroupType<TopologyTrace>& fldsTrace,
         const int TraceGroupGlobal)
  {
    typedef typename XField<PhysDim, typename TopologyTrace::CellTopoDim>       ::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename Field<PhysDim, typename TopologyTrace::CellTopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;

    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldTraceGroupType::template ElementType<> ElementQFieldClass;
    typedef typename TopologyTrace::TopoDim TopoDimTrace;

    const XFieldTraceGroupType& xfldTrace = get<0>(fldsTrace);
          QFieldTraceGroupType& qfldTrace = const_cast<QFieldTraceGroupType&>( get<1>(fldsTrace) );

    // element field variables
    ElementXFieldClass xfldElem( xfldTrace.basis() );
    ElementQFieldClass qfldElem( qfldTrace.basis() );

    // class for computing the projection
    ElementProjectionSolution_L2<TopoDimTrace, TopologyTrace> projector(qfldElem.basis());

    // just to make sure things are consistent
    SANS_ASSERT( xfldTrace.nElem() == qfldTrace.nElem() );

    // loop over elements within group
    const int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid/solution DOFs to element
      xfldTrace.getElement( xfldElem, elem );
      qfldTrace.getElement( qfldElem, elem );

      // project the solution on the element
      projector.project(xfldElem, qfldElem, soln_);

      // set the projected solution
      qfldTrace.setElement( qfldElem, elem );
    }
  }

protected:
  const SOLN& soln_;
  const std::vector<int> traceGroups_;
};


// Factory function

template<class SOLN>
ProjectSolnTrace_Discontinuous_impl<SOLN>
ProjectSolnTrace_Discontinuous( const SOLN& soln, const std::vector<int>& traceGroups)
{
  return ProjectSolnTrace_Discontinuous_impl<SOLN>(soln, traceGroups);
}


} //namespace SANS

#endif  // PROJECTSOLNTRACE_DISCONTINUOUS_H
