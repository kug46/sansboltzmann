// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(PROJECTGLOBALFIELD_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include <limits>

#include "../ProjectGlobalField.h"

#include <kdtree++/kdtree.hpp>

#include "../ProjectGlobalField_KDVertex.h"
#include "../PointwiseSolutionStorage.h"

#include "Quadrature/Quadrature.h"

#include "Field/Element/RefCoordSolver.h"

#include "Field/XField.h"

#include "Field/Field.h"

#include "Field/tools/for_each_CellGroup.h"
#include "Field/tools/GroupFunctorType.h"
#include "Field/Tuple/FieldTuple.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/InverseLU.h"
#include "LinearAlgebra/DenseLinAlg/InverseLUP.h"
#include "LinearAlgebra/DenseLinAlg/tools/norm.h"

#include "BasisFunction/LagrangeDOFMap.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_gather.hpp>
#include <boost/mpi/collectives/all_to_all.hpp>
#include <boost/mpi/collectives/reduce.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/vector.hpp>
#include "MPI/serialize_BoundingBox.h"
#endif

namespace SANS
{

template<class Topology>
struct LagrangeNodes;

/*---------------------------------------------------------------------------------------*/

template<class PhysDim, class TopoDim, class T>
class UnallocatedPredicate
{
public:

  typedef ProjectGlobalField_KDVertex<PhysDim> KDVertex;

  explicit UnallocatedPredicate( const PointwiseSolutionStorage<T>& solutionStorage ) :
    solutionStorage_(solutionStorage) {}

   bool operator()( const KDVertex& node ) const
   {
     int cellgroup, cellelem, quadpoint;
     node.getInfo(cellgroup, cellelem, quadpoint);

     Real dist = solutionStorage_.getDistance(cellgroup, cellelem, quadpoint);

     return (dist != -1);  //true if this node was successfully found in the old mesh
   }

protected:
  const PointwiseSolutionStorage<T>& solutionStorage_;
};


//----------------------------------------------------------------------------//
//  Populates the tree with the physical coordinates of the quadrature points of the given field
//
template<class PhysDim, class T, class TreeType>
class PopulateTree : public GroupFunctorCellType< PopulateTree<PhysDim, T, TreeType> >
{
public:

  typedef ProjectGlobalField_KDVertex<PhysDim> KDVertex;

  PopulateTree( TreeType& tree, const std::vector<int>& cellGroups,
                PointwiseSolutionStorage<T>& solutionStorage) :
    tree_(tree), cellGroups_(cellGroups), solutionStorage_(solutionStorage) {}

  std::size_t nCellGroups() const          { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n];     }

//----------------------------------------------------------------------------//
  // Function that loops over each element in the cell group
  template <class Topology>
  void
  apply( const typename FieldTuple<XField<PhysDim, typename Topology::TopoDim>, Field<PhysDim, typename Topology::TopoDim, T>, TupleClass<>>::
                        template FieldCellGroupType<Topology>& fldsCell,
         const int cellGroupGlobal )
  {
    typedef typename XField<PhysDim, typename Topology::TopoDim   >::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field <PhysDim, typename Topology::TopoDim, T>::template FieldCellGroupType<Topology> QFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename ElementXField<PhysDim, typename Topology::TopoDim, Topology>::VectorX VectorX;

    const XFieldCellGroupType& xfldCell = get<0>(fldsCell);
    const QFieldCellGroupType& qfldCell = get<1>(fldsCell);

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );

    VectorX X;         // physical coordinates

    //Get solution order
    const int order = qfldCell.basis()->order();

#if 0
    typedef QuadraturePoint<typename Topology::TopoDim> QuadPointType;

    // use a 2P+1 quadrature rule
    Quadrature<TopoDim, Topology> quadrature( 2*order + 1 );
    const int nquad = quadrature.nQuadrature();

    // loop over elements within group
    const int nelem = xfldCell.nElem();
    solutionStorage_.resizeCellElems(cellGroupGlobal, nelem, nquad);

    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid DOFs to element
      xfldCell.getElement( xfldElem, elem );

      // loop over quadrature points
      for (int iquad = 0; iquad < nquad; iquad++)
      {
        // reference-element coordinates
        QuadPointType Ref = quadrature.coordinates_cache( iquad );

        // physical coordinates
        xfldElem.coordinates( Ref, X );

        tree_.insert( KDVertex(X, cellGroupGlobal, elem, iquad) );
      }
    }
#else
    std::vector<DLA::VectorS<Topology::TopoDim::D,Real>> sRef;
    LagrangeNodes<Topology>::get(order, sRef);

    // loop over elements within group
    const int nelem = xfldCell.nElem();
    solutionStorage_.resizeCellElems(cellGroupGlobal, nelem, sRef.size());

    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid DOFs to element
      xfldCell.getElement( xfldElem, elem );

      // loop over quadrature points
      for (std::size_t inode = 0; inode < sRef.size(); inode++)
      {
        // physical coordinates
        xfldElem.coordinates( sRef[inode], X );

        tree_.insert( KDVertex(X, cellGroupGlobal, elem, inode) );
      }
    }
#endif
  }

protected:
  TreeType& tree_;
  const std::vector<int> cellGroups_;
  PointwiseSolutionStorage<T>& solutionStorage_; //reference to the global solution storage
};


//----------------------------------------------------------------------------//
//  Populates the solution storage object with solutions evaluated from qfldFrom
//
template<class PhysDim, class T>
class PopulateSolutionVector : public GroupFunctorCellType< PopulateSolutionVector<PhysDim, T> >
{
public:

  static const int D = PhysDim::D;

  typedef ProjectGlobalField_KDVertex<PhysDim> KDVertex;
  typedef KDTree::KDTree<D, KDVertex> KDTreeType;
  typedef typename KDTreeType::_Region_ KDTreeRegionType;

  PopulateSolutionVector( KDTreeType& tree, const std::vector<int>& cellGroups,
                          PointwiseSolutionStorage<T>& solutionStorage ) :
    tree_(tree), cellGroups_(cellGroups), solutionStorage_(solutionStorage) {}

  std::size_t nCellGroups() const          { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n];     }

//----------------------------------------------------------------------------//
  // Function that loops over each element in the cell group
  template <class Topology>
  void
  apply( const typename FieldTuple<XField<PhysDim, typename Topology::TopoDim>, Field<PhysDim, typename Topology::TopoDim, T>, TupleClass<>>::
                        template FieldCellGroupType<Topology>& fldsCell,
         const int cellGroupGlobal )
  {
    typedef typename XField<PhysDim, typename Topology::TopoDim   >::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field <PhysDim, typename Topology::TopoDim, T>::template FieldCellGroupType<Topology> QFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

    const XFieldCellGroupType& xfldCell = get<0>(fldsCell);
    const QFieldCellGroupType& qfldCell = get<1>(fldsCell);

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );

    typedef typename ElementXField<PhysDim, typename Topology::TopoDim, Topology>::VectorX VectorX;
    typedef typename ElementXField<PhysDim, typename Topology::TopoDim, Topology>::RefCoordType RefCoordType;

    RefCoordSolver<PhysDim, typename Topology::TopoDim, Topology> refcoord_solver(xfldElem); //nonlinear solver to find reference coords

    // loop over elements within group
    const int nelem = xfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      //Copy global DOFs to element
      xfldCell.getElement( xfldElem, elem );
      qfldCell.getElement( qfldElem, elem );

      //Compute the bounding box of the current element (in qfldFrom)
      BoundingBox<PhysDim> bbox = xfldElem.boundingBox();

      KDTreeRegionType region;
      KDVertex low_bound(bbox.low_bounds(),0,0,0);
      KDVertex high_bound(bbox.high_bounds(),0,0,0);

      for (int d = 0; d < D; d++)
      {
        region.set_low_bound (low_bound, d);
        region.set_high_bound(high_bound, d);
      }

      //Use the KDtree to find all quadrature points from qfldTo that are inside the bounding box
      std::vector<KDVertex> retVertices;
      tree_.find_within_range( region, std::back_inserter(retVertices) );

      for (std::size_t k = 0; k < retVertices.size(); k++)
      {
        VectorX XcoordTo = retVertices[k].getCoordinates();
        RefCoordType sRefFrom = Topology::centerRef;  // reference-element coordinates of xfldElem

        //Perform a nonlinear solve to find the reference coordinates of xfldElem
        //that are closest to the physical coordinate given by XcoordTo
        refcoord_solver.solve(XcoordTo, sRefFrom);

        //Evaluate the solution from qfldFrom, at the reference coordinate found above
        T q = qfldElem.eval(sRefFrom);
        VectorX XcoordFrom = xfldElem.eval(sRefFrom);

        VectorX dX = XcoordFrom - XcoordTo;
        Real distance = norm(dX, 2.0);

        int cellgroupTo, cellelemTo, quadindexTo;
        retVertices[k].getInfo(cellgroupTo, cellelemTo, quadindexTo);

        //Store the evaluated solution in the global storage
        solutionStorage_.setSolution(cellgroupTo, cellelemTo, quadindexTo, q, distance);
      }
    } //loop over elements
  }

protected:
  KDTreeType& tree_;
  const std::vector<int> cellGroups_;
  PointwiseSolutionStorage<T>& solutionStorage_; //reference to the global solution storage
};


//----------------------------------------------------------------------------//
//  Populates a vector with the physical coordinates of unallocated quadrature points of the receiving field
//
template<class PhysDim, class T>
class PopulateUnallocatedPoints : public GroupFunctorCellType< PopulateUnallocatedPoints<PhysDim, T> >
{
public:

  typedef DLA::VectorS<PhysDim::D,Real> VectorX; // coordinates vector

  PopulateUnallocatedPoints( const std::vector<int>& cellGroups,
                             const std::vector<std::array<std::size_t,3>>& pointList,
                             std::vector<VectorX>& coordList) :
    cellGroups_(cellGroups), pointList_(pointList), coordList_(coordList) {}

  std::size_t nCellGroups() const          { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n];     }

//----------------------------------------------------------------------------//
  // Function that loops over each element in the cell group
  template <class Topology>
  void
  apply( const typename FieldTuple<XField<PhysDim, typename Topology::TopoDim>, Field<PhysDim, typename Topology::TopoDim, T>, TupleClass<>>::
                        template FieldCellGroupType<Topology>& fldsCell,
         const int cellGroupGlobal )
  {
    typedef typename XField<PhysDim, typename Topology::TopoDim   >::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field <PhysDim, typename Topology::TopoDim, T>::template FieldCellGroupType<Topology> QFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

    typedef typename ElementXField<PhysDim, typename Topology::TopoDim, Topology>::VectorX VectorX;
    typedef typename ElementXField<PhysDim, typename Topology::TopoDim, Topology>::RefCoordType RefCoordType;

    const XFieldCellGroupType& xfldCell = get<0>(fldsCell);
    const QFieldCellGroupType& qfldCell = get<1>(fldsCell);

    RefCoordType Ref;  // reference-element coordinates
    VectorX X;         // physical coordinates

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );

    //Get solution order
    const int order = qfldCell.basis()->order();

#if 0
    // use a 2P+1 quadrature rule
    Quadrature<typename Topology::TopoDim, Topology> quadrature( 2*order + 1 );

    for (std::size_t i = 0; i < pointList_.size(); i++)
    {
      if (cellGroupGlobal == (int) pointList_[i][0] )
      {
        int cellelem = pointList_[i][1];
        int quadpoint = pointList_[i][2];

        // copy global grid DOFs to element
        xfldCell.getElement( xfldElem, cellelem );

        quadrature.coordinates( quadpoint, Ref );

        // get physical coordinates
        xfldElem.coordinates( Ref, coordList_[i] );
      }
    }
#else
    std::vector<DLA::VectorS<Topology::TopoDim::D,Real>> sRef;
    LagrangeNodes<Topology>::get(order, sRef);

    for (std::size_t i = 0; i < pointList_.size(); i++)
    {
      if (cellGroupGlobal == (int) pointList_[i][0] )
      {
        int cellelem = pointList_[i][1];
        int inode    = pointList_[i][2];

        // copy global grid DOFs to element
        xfldCell.getElement( xfldElem, cellelem );

        // get physical coordinates
        xfldElem.coordinates( sRef[inode], coordList_[i] );
      }
    }
#endif
  }

protected:
  const std::vector<int> cellGroups_;
  const std::vector<std::array<std::size_t,3>>& pointList_; //reference to list of unallocated points [cellgroup][cellelem][quadpoint]
  std::vector<VectorX>& coordList_;
};



//----------------------------------------------------------------------------//
//  Populates the DOFs of the qfldTo field with the stored solutions
//
template<class PhysDim, class T>
class PopulateField : public GroupFunctorCellType< PopulateField<PhysDim, T> >
{
public:

  PopulateField( const std::vector<int>& cellGroups,
                 const PointwiseSolutionStorage<T>& solutionStorage) :
    cellGroups_(cellGroups), solutionStorage_(solutionStorage) {}

  std::size_t nCellGroups() const          { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n];     }

//----------------------------------------------------------------------------//
  // Function that loops over each element in the cell group
  template <class Topology>
  void
  apply( const typename FieldTuple<XField<PhysDim, typename Topology::TopoDim>, Field<PhysDim, typename Topology::TopoDim, T>, TupleClass<>>::
                        template FieldCellGroupType<Topology>& fldsCell,
         const int cellGroupGlobal )
  {
    typedef typename XField<PhysDim, typename Topology::TopoDim   >::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field <PhysDim, typename Topology::TopoDim, T>::template FieldCellGroupType<Topology> QFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

    const XFieldCellGroupType& xfldCell = get<0>(fldsCell);
          QFieldCellGroupType& qfldCell = const_cast<QFieldCellGroupType&>( get<1>(fldsCell) );

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );

    //Get solution order
    const int order = qfldCell.basis()->order();

    // loop over elements within group
    const int nelem = xfldCell.nElem();

#if 0
    typedef QuadraturePoint<typename Topology::TopoDim> QuadPointType;

    // use a 2P+1 quadrature rule
    Quadrature<typename Topology::TopoDim, Topology> quadrature( 2*order + 1 );
    const int nquad = quadrature.nQuadrature();

    const int nDOF = qfldElem.nDOF();   // total solution DOFs in element
    SANS_ASSERT( nDOF > 0 );            // Suppress clang analyzer warning
    std::vector<Real> phi(nDOF);        // basis
    DLA::MatrixD<Real> MM(nDOF, nDOF);  // mass-matrix (computed once per cellgroup)
    DLA::VectorD<T> rhs(nDOF);

    MM = 0.0;

    // loop over quadrature points
    for (int iquad = 0; iquad < nquad; iquad++)
    {
      Real weight;
      quadrature.weight( iquad, weight );
      QuadPointType sRef = quadrature.coordinates_cache( iquad );

      qfldElem.evalBasis( sRef, phi.data(), phi.size() );

      for (int i = 0; i < nDOF; i++)
        for (int j = 0; j < nDOF; j++)
          MM(i,j) += weight*phi[i]*phi[j];
    }

    // Only these basis functions work with a single mass matrix per element
    BasisFunctionCategory cat = qfldCell.basis()->category();
    SANS_ASSERT( cat == BasisFunctionCategory_Legendre ||
                 cat == BasisFunctionCategory_Lagrange ||
                (cat == BasisFunctionCategory_Hierarchical && order < 3) );

    DLA::MatrixD<Real> invMM = DLA::InverseLU::Inverse(MM);  // inverse of mass-matrix (computed once per cellgroup)

    for (int elem = 0; elem < nelem; elem++)
    {
      rhs = 0;

      // loop over quadrature points
      for (int iquad = 0; iquad < nquad; iquad++)
      {
        Real weight;
        quadrature.weight( iquad, weight );
        QuadPointType sRef = quadrature.coordinates_cache( iquad );

        qfldElem.evalBasis( sRef, phi.data(), phi.size() );

        // get the solution for this quadrature point from storage
        T qFrom = solutionStorage_.getSolution(cellGroupGlobal, elem, iquad);

        for (int i = 0; i < nDOF; i++)
          rhs[i] += weight*phi[i]*qFrom;
      }

      //Compute L2 projection
      qfldElem.vectorViewDOF() = invMM*rhs;

      // set the projected solution
      qfldCell.setElement( qfldElem, elem );
    }
#else
    const int nDOF = qfldElem.nDOF();   // total solution DOFs in element
    SANS_ASSERT( nDOF > 0 );            // Suppress clang analyzer warning
    std::vector<Real> phi(nDOF);        // basis
    DLA::MatrixD<Real> V(nDOF, nDOF);   // Vandermode (computed once per cellgroup)
    DLA::VectorD<T> qFrom(nDOF);

    V = 0.0;

    std::vector<DLA::VectorS<Topology::TopoDim::D,Real>> sRef;
    LagrangeNodes<Topology>::get(order, sRef);

    // evaluate the bassi at each lagrange node
    for (std::size_t inode = 0; inode < sRef.size(); inode++)
    {
      qfldElem.evalBasis( sRef[inode], phi.data(), phi.size() );

      for (int j = 0; j < nDOF; j++)
         V(inode,j) = phi[j];
    }

    DLA::MatrixD<Real> invV = DLA::InverseLUP::Inverse(V);  // inverse of Vandermode matrix

    for (int elem = 0; elem < nelem; elem++)
    {
      // loop over quadrature nodes
      for (std::size_t inode = 0; inode < sRef.size(); inode++)
      {
         // get the solution for this Lagrange point from storage
        qFrom[inode] = solutionStorage_.getSolution(cellGroupGlobal, elem, inode);
      }

      //Compute the fit
      qfldElem.vectorViewDOF() = invV*qFrom;

      // set the projected solution
      qfldCell.setElement( qfldElem, elem );
    }

#endif
  }

protected:
  const std::vector<int> cellGroups_;
  const PointwiseSolutionStorage<T>& solutionStorage_; //reference to the global solution storage
};


#ifdef SANS_MPI

//---------------------------------------------------------------------------//
template<class PhysDim>
class FieldBoundingBox : public GroupFunctorCellType< FieldBoundingBox<PhysDim> >
{
public:

  FieldBoundingBox( const int nCellGroups, BoundingBox<PhysDim>& fldbbox ):
    nCellGroups_(nCellGroups), fldbbox_(fldbbox) {}

  std::size_t nCellGroups() const          { return nCellGroups_; }
  std::size_t cellGroup(const int n) const { return n;            }

  //----------------------------------------------------------------------------//
  // Function that loops over each element in the cell group
  template <class Topology>
  void
  apply( const typename XField<PhysDim, typename Topology::TopoDim>::template FieldCellGroupType<Topology>& xfldCell,
         const int cellGroupGlobal )
  {
    typedef typename XField<PhysDim, typename Topology::TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );

    // loop over elements within group
    const int nelem = xfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      //Copy global DOFs to element
      xfldCell.getElement( xfldElem, elem );

      // add the element bounding box to the total field bounding box
      fldbbox_.setBound(xfldElem.boundingBox());
    }
  }

protected:
  const int nCellGroups_;
  BoundingBox<PhysDim>& fldbbox_;
};

//---------------------------------------------------------------------------//
struct MPIElement
{
  MPIElement() : group(-1) {}
  MPIElement(const int group, const std::vector<int>& elemMap) :
    group(group), elemMap(elemMap) {}

  int group;
  std::vector<int> elemMap;

protected:
  friend class boost::serialization::access;

  template<class Archive>
  void serialize(Archive & ar, const unsigned int version)
  {
    ar & group;
    ar & elemMap;
  }
};

//---------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
class XFieldElementSend : public GroupFunctorCellType< XFieldElementSend<PhysDim, TopoDim> >
{
public:
  typedef DLA::VectorS<PhysDim::D,Real> VectorX;

  XFieldElementSend( const XField<PhysDim, TopoDim>& xfldFrom,
                     const std::vector<BoundingBox<PhysDim>>& fldbboxRank,
                     std::vector<std::vector<std::vector<int>>>& rankElem,
                     std::vector<std::map<int,VectorX>>& sendDOF,
                     std::vector<std::map<int,MPIElement>>& sendElem ):
                       xfldFrom_(xfldFrom), fldbboxRank_(fldbboxRank),
                       rankElem_(rankElem), sendDOF_(sendDOF), sendElem_(sendElem) {}

  std::size_t nCellGroups() const          { return xfldFrom_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return n;                       }

  //----------------------------------------------------------------------------//
  // Function that loops over each element in the cell group
  template <class Topology>
  void
  apply( const typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology>& xfldCell,
         const int cellGroupGlobal )
  {
    typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

    const std::vector<int>& cellIDs = xfldFrom_.cellIDs(cellGroupGlobal);

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );

    std::vector<int> elemMap(xfldCell.nBasis());
    std::vector<int> elemMapNative(xfldCell.nBasis());

    // loop over elements within group
    const int nelem = xfldCell.nElem();

    rankElem_[cellGroupGlobal].resize(nelem);

    for (int elem = 0; elem < nelem; elem++)
    {
      //Copy global DOFs to element
      xfldCell.getElement( xfldElem, elem );

      // get the element bounding box
      BoundingBox<PhysDim> bbox = xfldElem.boundingBox();

      xfldCell.associativity(elem).getGlobalMapping(elemMap.data(), elemMap.size());

      // get the native DOF indexing
      for (std::size_t i = 0; i < elemMap.size(); i++)
        elemMapNative[i] = xfldFrom_.local2nativeDOFmap(elemMap[i]);

      // send this element to all processors ranks with overlapping bounding boxes
      for (std::size_t rank = 0; rank < fldbboxRank_.size(); rank++)
        if ( fldbboxRank_[rank].intersects_with(bbox) )
        {
          // save off which ranks this element is sent to
          rankElem_[cellGroupGlobal][elem].push_back(rank);

          // save off the DOF to be sent
          for (std::size_t i = 0; i < elemMap.size(); i++)
            sendDOF_[rank][elemMapNative[i]] = xfldFrom_.DOF(elemMap[i]);

          // save off the element map to send to other processors
          sendElem_[rank][cellIDs[elem]] = MPIElement(cellGroupGlobal, elemMapNative);
        }
    }
  }

protected:
  const XField<PhysDim, TopoDim>& xfldFrom_;
  const std::vector<BoundingBox<PhysDim>>& fldbboxRank_;
  std::vector<std::vector<std::vector<int>>>& rankElem_;
  std::vector<std::map<int,VectorX>>& sendDOF_;
  std::vector<std::map<int,MPIElement>>& sendElem_;
};

template<class PhysDim, class TopoDim, class T>
class Field_Overlap;

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim>
class XField_Overlap : public XField<PhysDim,TopoDim>
{
public:
  typedef XField<PhysDim,TopoDim> BaseType;
  typedef typename BaseType::VectorX VectorX;

  XField_Overlap(const XField<PhysDim,TopoDim>& xfldFrom,
                 const XField<PhysDim,TopoDim>& xfldTo ) : BaseType(xfldFrom.comm())
  {
    BoundingBox<PhysDim> fldbbox;

    //Compute the bounding box of the field
    for_each_CellGroup<TopoDim>::apply( FieldBoundingBox<PhysDim>(xfldTo.nCellGroups(), fldbbox), xfldTo );

    // Send the bounding box to all other processors
    std::vector<BoundingBox<PhysDim>> fldbboxRank;
    boost::mpi::all_gather(*xfldFrom.comm(), fldbbox, fldbboxRank);

    // initialize the map to indicate which processor each element needs to be sent to
    rankElem_.resize(xfldFrom.nCellGroups());
    std::vector<std::map<int,VectorX>> sendDOF(fldbboxRank.size());
    std::vector<std::map<int,MPIElement>> sendElem(fldbboxRank.size());

    for_each_CellGroup<TopoDim>::apply(
        XFieldElementSend<PhysDim,TopoDim>(xfldFrom, fldbboxRank, rankElem_, sendDOF, sendElem),
        xfldFrom );

    // send DOFs and elements to the other ranks
    std::vector<std::map<int,VectorX>> recvDOF;
    std::vector<std::map<int,MPIElement>> recvElem;
    boost::mpi::all_to_all(*xfldFrom.comm(), sendDOF, recvDOF);
    boost::mpi::all_to_all(*xfldFrom.comm(), sendElem, recvElem);

    // collapse the map down across the ranks removing any possible duplicates
    std::map<int,VectorX> DOF;
    std::map<int,MPIElement> Elems;
    for (std::size_t rank = 0; rank < recvDOF.size(); rank++ )
    {
      DOF.insert(recvDOF[rank].begin(), recvDOF[rank].end());
      Elems.insert(recvElem[rank].begin(), recvElem[rank].end());
    }

    this->resizeDOF(DOF.size());
    std::map<int,int> localDOFmap;

    // set all the DOFs for the cell groups
    // and map native indexing to local DOF array indexing
    int iDOF = 0;
    for ( const auto& DOFPair : DOF)
    {
      this->DOF(iDOF) = DOFPair.second;
      localDOFmap[DOFPair.first] = iDOF;
      iDOF++;
    }

    // resize the number of cell groups
    this->resizeCellGroups(xfldFrom.nCellGroups());

    // create the cell groups
    for_each_CellGroup<TopoDim>::apply( CreateCellGroups(*this, localDOFmap, Elems),
                                        xfldFrom );

    // DO NOT CALL CheckGrid. This is not a valid grid, and is only usefull for transfering solutions.
  }
protected:
  //---------------------------------------------------------------------------//
  class CreateCellGroups : public GroupFunctorCellType< CreateCellGroups >
  {
  public:

    CreateCellGroups( XField_Overlap<PhysDim, TopoDim>& xfld,
                      const std::map<int,int>& localDOFmap,
                      const std::map<int,MPIElement>& Elems ):
                        xfld_(xfld), localDOFmap_(localDOFmap), Elems_(Elems) {}

    std::size_t nCellGroups() const          { return xfld_.nCellGroups(); }
    std::size_t cellGroup(const int n) const { return n;                   }

    //----------------------------------------------------------------------------//
    // Function that loops over each element in the cell group
    template <class Topology>
    void
    apply( const typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology>& xfldCell,
           const int cellGroupGlobal )
    {
      xfld_.template createCells<Topology>( localDOFmap_, Elems_, xfldCell, cellGroupGlobal );
    }

  protected:
    XField_Overlap<PhysDim, TopoDim>& xfld_;
    const std::map<int,int>& localDOFmap_;
    const std::map<int,MPIElement>& Elems_;
  };

  friend class CreateCellGroups;

  template<class Topology>
  void createCells(const std::map<int,int>& localDOFmap,
                   const std::map<int,MPIElement>& Elems,
                   const typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology>& xfldCell,
                   const int cellGroupGlobal )
  {
    typedef typename BaseType::
        template FieldCellGroupType<Topology>::FieldAssociativityConstructorType FieldAssociativityConstructorType;

    int nElem = 0;
    for ( const auto& elemPair : Elems)
    {
      // count the total number of elements in this group
      if (elemPair.second.group == cellGroupGlobal)
        nElem++;
    }

    FieldAssociativityConstructorType fldAssocCell( xfldCell.basis(), nElem );

    int comm_rank = this->comm_->rank();

    std::vector<int> elemMap(fldAssocCell.nBasis());

    int elem = 0;
    for ( const auto& elemPair : Elems)
    {
      // skip any element not in this group
      if (elemPair.second.group != cellGroupGlobal) continue;

      // boundary processor rank to the current processor
      fldAssocCell.setAssociativity( elem ).setRank( comm_rank );

      // map native to local DOF indexing
      elemMap = elemPair.second.elemMap;
      for (std::size_t i = 0; i < elemMap.size(); i++)
        elemMap[i] = localDOFmap.at(elemMap[i]);

      // set the element associativity
      fldAssocCell.setAssociativity( elem ).setGlobalMapping( elemMap );
      elem++;
    }

    this->cellGroups_[cellGroupGlobal] = new typename BaseType::template FieldCellGroupType<Topology>( fldAssocCell );
    this->cellGroups_[cellGroupGlobal]->setDOF(this->DOF_, this->nDOF_);
  }

  // give Field_Overlap access to rankElem_
  template<class, class, class >
  friend class Field_Overlap;

  std::vector<std::vector<std::vector<int>>> rankElem_;
};


//---------------------------------------------------------------------------//
template<class PhysDim, class TopoDim, class T>
class FieldElementSend : public GroupFunctorCellType< FieldElementSend<PhysDim, TopoDim, T> >
{
public:
  FieldElementSend( const Field<PhysDim, TopoDim, T>& qfldFrom,
                    const std::vector<std::vector<std::vector<int>>>& rankElem,
                    std::vector<std::map<int,T>>& sendDOF,
                    std::vector<std::map<int,MPIElement>>& sendElem ):
                       qfldFrom_(qfldFrom), rankElem_(rankElem),
                       sendDOF_(sendDOF), sendElem_(sendElem) {}

  std::size_t nCellGroups() const          { return qfldFrom_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return n;                       }

  //----------------------------------------------------------------------------//
  // Function that loops over each element in the cell group
  template <class Topology>
  void
  apply( const typename Field<PhysDim, TopoDim, T>::template FieldCellGroupType<Topology>& qfldCell,
         const int cellGroupGlobal )
  {
    const std::vector<int>& cellIDs = qfldFrom_.getXField().cellIDs(cellGroupGlobal);

    std::vector<int> elemMap(qfldCell.nBasis());
    std::vector<int> elemMapNative(qfldCell.nBasis());

    // loop over elements within group
    const int nelem = qfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      qfldCell.associativity(elem).getGlobalMapping(elemMap.data(), elemMap.size());

      // get the native DOF indexing
      for (std::size_t i = 0; i < elemMap.size(); i++)
        elemMapNative[i] = qfldFrom_.local2nativeDOFmap(elemMap[i]);

      // send this element to all processors ranks that need it
      for ( std::size_t rank : rankElem_[cellGroupGlobal][elem] )
      {
        // save off the DOFs to send to other processors
        for (std::size_t i = 0; i < elemMap.size(); i++)
          sendDOF_[rank][elemMapNative[i]] = qfldFrom_.DOF(elemMap[i]);

        // save off the element map to send to other processors
        sendElem_[rank][cellIDs[elem]] = MPIElement(cellGroupGlobal, elemMapNative);
      }
    }
  }

protected:
  const Field<PhysDim, TopoDim, T>& qfldFrom_;
  const std::vector<std::vector<std::vector<int>>>& rankElem_;
  std::vector<std::map<int,T>>& sendDOF_;
  std::vector<std::map<int,MPIElement>>& sendElem_;
};

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim, class T>
class Field_Overlap : public Field<PhysDim,TopoDim,T>
{
public:
  typedef Field<PhysDim,TopoDim,T> BaseType;

  Field_Overlap(const Field<PhysDim,TopoDim,T>& qfldFrom,
                const XField_Overlap<PhysDim,TopoDim>& xfldFrom )
    : BaseType(xfldFrom)
  {
    // copy over sub-group mappings
    this->globalCellGroups_ = qfldFrom.getGlobalCellGroups();
    this->localCellGroups_ = qfldFrom.getLocalCellGroups();

    int comm_size = qfldFrom.comm()->size();

    // initialize the map to indicate which processor each element needs to be sent to
    std::vector<std::map<int,T>> sendDOF(comm_size);
    std::vector<std::map<int,MPIElement>> sendElem(comm_size);

    for_each_CellGroup<TopoDim>::apply(
        FieldElementSend<PhysDim,TopoDim,T>(qfldFrom, xfldFrom.rankElem_, sendDOF, sendElem),
        qfldFrom );

    // send DOFs and elements to the other ranks
    std::vector<std::map<int,T>> recvDOF;
    std::vector<std::map<int,MPIElement>> recvElem;
    boost::mpi::all_to_all(*xfldFrom.comm(), sendDOF, recvDOF);
    boost::mpi::all_to_all(*xfldFrom.comm(), sendElem, recvElem);

    // collapse the map down across the ranks removing any possible duplicates
    std::map<int,T> DOF;
    std::map<int,MPIElement> Elems;
    for (std::size_t rank = 0; rank < recvDOF.size(); rank++ )
    {
      DOF.insert(recvDOF[rank].begin(), recvDOF[rank].end());
      Elems.insert(recvElem[rank].begin(), recvElem[rank].end());
    }

    this->resizeDOF(DOF.size());
    std::map<int,int> localDOFmap;

    // set all the DOFs for the cell groups
    // and map native indexing to local DOF array indexing
    int iDOF = 0;
    for ( const auto& DOFPair : DOF)
    {
      this->DOF(iDOF) = DOFPair.second;
      localDOFmap[DOFPair.first] = iDOF;
      iDOF++;
    }

    // resize the number of cell groups
    this->resizeCellGroups(qfldFrom.nCellGroups());

    // create the cell groups
    for_each_CellGroup<TopoDim>::apply( CreateCellGroups(*this, localDOFmap, Elems),
                                        qfldFrom );
  }
protected:
  //---------------------------------------------------------------------------//
  class CreateCellGroups : public GroupFunctorCellType< CreateCellGroups >
  {
  public:
    CreateCellGroups( Field_Overlap<PhysDim, TopoDim, T>& qfld,
                      const std::map<int,int>& localDOFmap,
                      const std::map<int,MPIElement>& Elems ):
                        qfld_(qfld), localDOFmap_(localDOFmap), Elems_(Elems) {}

    std::size_t nCellGroups() const          { return qfld_.nCellGroups();            }
    std::size_t cellGroup(const int n) const { return qfld_.getGlobalCellGroupMap(n); }

    //----------------------------------------------------------------------------//
    // Function that loops over each element in the cell group
    template <class Topology>
    void
    apply( const typename Field<PhysDim, TopoDim, T>::template FieldCellGroupType<Topology>& qfldCell,
           const int cellGroupGlobal )
    {
      qfld_.template createCells<Topology>( localDOFmap_, Elems_, qfldCell, cellGroupGlobal );
    }

  protected:
    Field_Overlap<PhysDim, TopoDim, T>& qfld_;
    const std::map<int,int>& localDOFmap_;
    const std::map<int,MPIElement>& Elems_;
  };

  friend class CreateCellGroups;

  template<class Topology>
  void createCells(const std::map<int,int>& localDOFmap,
                   const std::map<int,MPIElement>& Elems,
                   const typename Field<PhysDim, TopoDim, T>::template FieldCellGroupType<Topology>& qfldCell,
                   const int cellGroupGlobal )
  {
    typedef typename BaseType::
        template FieldCellGroupType<Topology>::FieldAssociativityConstructorType FieldAssociativityConstructorType;

    int nElem = 0;
    for ( const auto& elemPair : Elems)
    {
      // count the total number of elements in this group
      if (elemPair.second.group == cellGroupGlobal)
        nElem++;
    }

    FieldAssociativityConstructorType fldAssocCell( qfldCell.basis(), nElem );

    int comm_rank = this->comm()->rank();

    std::vector<int> elemMap(fldAssocCell.nBasis());

    int elem = 0;
    for ( const auto& elemPair : Elems)
    {
      // skip any element not in this group
      if (elemPair.second.group != cellGroupGlobal) continue;

      // boundary processor rank to the current processor
      fldAssocCell.setAssociativity( elem ).setRank( comm_rank );

      // map native to local DOF indexing
      elemMap = elemPair.second.elemMap;
      for (std::size_t i = 0; i < elemMap.size(); i++)
        elemMap[i] = localDOFmap.at(elemMap[i]);

      // set the element associativity
      fldAssocCell.setAssociativity( elem ).setGlobalMapping( elemMap );
      elem++;
    }

    int cellGroupLocal = this->getLocalCellGroupMap(cellGroupGlobal);
    this->cellGroups_[cellGroupLocal] = new typename BaseType::template FieldCellGroupType<Topology>( fldAssocCell );
    this->cellGroups_[cellGroupLocal]->setDOF(this->DOF_, this->nDOF_);
  }
};
#endif

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim, class T>
void ProjectGlobalField(const Field<PhysDim, TopoDim, T>& qfldFrom,
                              Field<PhysDim, TopoDim, T>& qfldTo)
{
  static const int D = PhysDim::D;

  typedef ProjectGlobalField_KDVertex<PhysDim> KDVertex;
  typedef KDTree::KDTree<D, KDVertex> KDTreeType;

  const XField<PhysDim, TopoDim>& xfldFrom = qfldFrom.getXField();
  const XField<PhysDim, TopoDim>& xfldTo = qfldTo.getXField();

#ifdef SANS_MPI
  XField_Overlap<PhysDim, TopoDim> xfldFrom_Overlap(xfldFrom, xfldTo);
  Field_Overlap<PhysDim, TopoDim, T> qfldFrom_Overlap(qfldFrom, xfldFrom_Overlap);
#else
  const XField<PhysDim, TopoDim>& xfldFrom_Overlap(xfldFrom);
  const Field<PhysDim, TopoDim, T>& qfldFrom_Overlap(qfldFrom);
#endif

  KDTreeType tree;
  PointwiseSolutionStorage<T> solutionStorage;

  const int nCellGroupsFrom = qfldFrom_Overlap.nCellGroups();
  std::vector<int> cellGroupsFrom(nCellGroupsFrom);
  for (int i = 0; i < nCellGroupsFrom; i++)
    cellGroupsFrom[i] = i;

  const int nCellGroupsTo = qfldTo.nCellGroups();
  std::vector<int> cellGroupsTo(nCellGroupsTo);
  for (int i = 0; i < nCellGroupsTo; i++)
    cellGroupsTo[i] = i;

  solutionStorage.resizeCellGroups(nCellGroupsTo);

  //Insert all quadrature points in qfldTo_ to KDtree, and resize solution vector arrays
  for_each_CellGroup<TopoDim>::apply( PopulateTree<PhysDim, T, KDTreeType>(tree, cellGroupsTo, solutionStorage),
                                      (xfldTo, qfldTo) );
  tree.optimize();

  int kdnodesGlobal = tree.size();
#ifdef SANS_MPI
  int kdnodes = kdnodesGlobal;
  boost::mpi::reduce(*qfldTo.comm(), kdnodes, kdnodesGlobal, std::plus<int>(), 0);
#endif
  if (qfldTo.comm()->rank() == 0)
    std::cout<<"Created KDtree with " << kdnodesGlobal << " nodes."<<std::endl;

  //Fill the solution vector array from qfldFrom_
  for_each_CellGroup<TopoDim>::apply( PopulateSolutionVector<PhysDim, T>(tree, cellGroupsFrom, solutionStorage),
                                      (xfldFrom_Overlap, qfldFrom_Overlap) );

  std::vector<std::array<std::size_t,3>> pList = solutionStorage.getUnallocatedPoints();
  std::size_t nMissedPoints = pList.size();
  std::size_t nMissedPointsGlobal = nMissedPoints;

#ifdef SANS_MPI
  boost::mpi::reduce(*qfldTo.comm(), nMissedPoints, nMissedPointsGlobal, std::plus<std::size_t >(), 0);
#endif

  if (nMissedPoints == tree.size())
    SANS_RUNTIME_EXCEPTION("No solution points were transferred. The donor and receiver meshes do not overlap!");

  if (nMissedPoints != 0)
  {
    std::vector<DLA::VectorS<PhysDim::D,Real>> pList_coords(pList.size());

    //Get the coordinates of the unallocated points in qfldTo_
    for_each_CellGroup<TopoDim>::apply( PopulateUnallocatedPoints<PhysDim, T>(cellGroupsTo, pList, pList_coords),
                                        (xfldTo, qfldTo) );

    //Class to decide if a given point in the KDtree was successfully matched or not
    UnallocatedPredicate<PhysDim, TopoDim, T> predicate(solutionStorage);

    for (std::size_t i = 0; i < pList.size(); i++)
    {
      KDVertex unallocatedVertex(pList_coords[i], pList[i][0], pList[i][1], pList[i][2]);

      //Use KDtree to find nearest point that was successfully matched
      std::pair<typename KDTreeType::const_iterator,double> it =
         tree.find_nearest_if(unallocatedVertex, std::numeric_limits<Real>::max(), predicate);

      int cellgroup_nearest, cellelem_nearest, quadpoint_nearest;
      it.first->getInfo(cellgroup_nearest, cellelem_nearest, quadpoint_nearest);

      //Set the solution at this unallocated point to the solution of the nearest valid point
      Real distance = it.second;
      T q_nearest = solutionStorage.getSolution(cellgroup_nearest, cellelem_nearest, quadpoint_nearest);
      solutionStorage.setSolution(pList[i][0], pList[i][1], pList[i][2], q_nearest, distance);
    }
  }

  if (qfldTo.comm()->rank() == 0)
  {
    if (nMissedPointsGlobal == 0)
      std::cout<<"Field projection completed with no mismatched points."<<std::endl;
    else
      std::cout<<"Field projection completed with "<< nMissedPointsGlobal <<" mismatched points."<<std::endl;
  }

  //Project solution
  for_each_CellGroup<TopoDim>::apply( PopulateField<PhysDim, T>(cellGroupsFrom, solutionStorage),
                                      (xfldTo, qfldTo) );
}

} //namespace SANS
