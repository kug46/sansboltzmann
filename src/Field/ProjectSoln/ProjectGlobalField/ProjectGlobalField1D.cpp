// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define PROJECTGLOBALFIELD_INSTANTIATE
#include "ProjectGlobalField_impl.h"

#include "Field/XFieldLine.h"

#include "Field/FieldLine.h"

#include "BasisFunction/LagrangeDOFMap.h"
#include "BasisFunction/BasisFunctionNode.h"
#include "BasisFunction/BasisFunctionLine_Lagrange.h"

namespace SANS
{

typedef DLA::VectorS<2,Real> Vector2;
typedef DLA::VectorS<3,Real> Vector3;
typedef DLA::VectorS<4,Real> Vector4;

//Explicit instantiations
template void ProjectGlobalField<PhysD1, TopoD1, Real>(const Field<PhysD1, TopoD1, Real>& qfldFrom, Field<PhysD1, TopoD1, Real>& qfldTo);
template void ProjectGlobalField<PhysD1, TopoD1, Vector2>(const Field<PhysD1, TopoD1, Vector2>& qfldFrom, Field<PhysD1, TopoD1, Vector2>& qfldTo);
template void ProjectGlobalField<PhysD1, TopoD1, Vector3>(const Field<PhysD1, TopoD1, Vector3>& qfldFrom, Field<PhysD1, TopoD1, Vector3>& qfldTo);
template void ProjectGlobalField<PhysD1, TopoD1, Vector4>(const Field<PhysD1, TopoD1, Vector4>& qfldFrom, Field<PhysD1, TopoD1, Vector4>& qfldTo);

} //namespace SANS
