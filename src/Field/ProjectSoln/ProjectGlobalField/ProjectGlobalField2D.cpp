// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define PROJECTGLOBALFIELD_INSTANTIATE
#include "ProjectGlobalField_impl.h"

#include "Field/XFieldArea.h"

#include "Field/FieldArea.h"

#include "BasisFunction/BasisFunctionArea_Quad_Lagrange.h"
#include "BasisFunction/BasisFunctionArea_Triangle_Lagrange.h"

namespace SANS
{

typedef DLA::VectorS<2,Real> Vector2;
typedef DLA::VectorS<3,Real> Vector3;
typedef DLA::VectorS<4,Real> Vector4;
typedef DLA::VectorS<5,Real> Vector5;
typedef DLA::VectorS<6,Real> Vector6;

//Explicit instantiations
template void ProjectGlobalField<PhysD2, TopoD2, Real>(const Field<PhysD2, TopoD2, Real>& qfldFrom, Field<PhysD2, TopoD2, Real>& qfldTo);
template void ProjectGlobalField<PhysD2, TopoD2, Vector2>(const Field<PhysD2, TopoD2, Vector2>& qfldFrom, Field<PhysD2, TopoD2, Vector2>& qfldTo);
template void ProjectGlobalField<PhysD2, TopoD2, Vector3>(const Field<PhysD2, TopoD2, Vector3>& qfldFrom, Field<PhysD2, TopoD2, Vector3>& qfldTo);
template void ProjectGlobalField<PhysD2, TopoD2, Vector4>(const Field<PhysD2, TopoD2, Vector4>& qfldFrom, Field<PhysD2, TopoD2, Vector4>& qfldTo);
template void ProjectGlobalField<PhysD2, TopoD2, Vector5>(const Field<PhysD2, TopoD2, Vector5>& qfldFrom, Field<PhysD2, TopoD2, Vector5>& qfldTo);
template void ProjectGlobalField<PhysD2, TopoD2, Vector6>(const Field<PhysD2, TopoD2, Vector6>& qfldFrom, Field<PhysD2, TopoD2, Vector6>& qfldTo);

} //namespace SANS
