// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "PointwiseSolutionStorage.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

namespace SANS
{

template<class T>
void
PointwiseSolutionStorage<T>::resizeCellGroups(int n)
{
  solutionData_.resize(n);
  distanceData_.resize(n);
}

template<class T>
void
PointwiseSolutionStorage<T>::resizeCellElems(int cellgroup, int nelem, int nquad_per_elem)
{
  solutionData_[cellgroup].resize(nelem, std::vector<T>(nquad_per_elem));
  distanceData_[cellgroup].resize(nelem, std::vector<Real>(nquad_per_elem, -1));
}

template<class T>
void
PointwiseSolutionStorage<T>::setSolution(int cellgroup, int elem, int quad_index, T q, Real distance)
{
  //Save only if solution was either not set before, or if the point found now is closer than the previously stored point
  if (distanceData_[cellgroup][elem][quad_index] == -1 || distance < distanceData_[cellgroup][elem][quad_index])
  {
    solutionData_[cellgroup][elem][quad_index] = q;

    //Save the distance between the quadpoint in the new mesh and the closest point found in old mesh
    distanceData_[cellgroup][elem][quad_index] = distance;
  }
}

template<class T>
const T&
PointwiseSolutionStorage<T>::getSolution(int cellgroup, int elem, int quad_index) const
{
  return solutionData_[cellgroup][elem][quad_index];
}

template<class T>
Real
PointwiseSolutionStorage<T>::getDistance(int cellgroup, int elem, int quad_index) const
{
  return distanceData_[cellgroup][elem][quad_index];
}

template<class T>
std::vector<std::array<std::size_t,3>>
PointwiseSolutionStorage<T>::getUnallocatedPoints() const
{
  std::vector<std::array<std::size_t,3>> vec;

  for (std::size_t i = 0; i < distanceData_.size(); i++)
    for (std::size_t j = 0; j < distanceData_[i].size(); j++)
      for (std::size_t k = 0; k < distanceData_[i][j].size(); k++)
        if (distanceData_[i][j][k] == -1)
        {
          std::array<std::size_t,3> info = {{i,j,k}};
          vec.push_back(info);
        }

  vec.shrink_to_fit();
  return vec;
}

//Explicit instantiations
template class PointwiseSolutionStorage<Real>;
template class PointwiseSolutionStorage<DLA::VectorS<2,Real>>;
template class PointwiseSolutionStorage<DLA::VectorS<3,Real>>;
template class PointwiseSolutionStorage<DLA::VectorS<4,Real>>;
template class PointwiseSolutionStorage<DLA::VectorS<5,Real>>;
template class PointwiseSolutionStorage<DLA::VectorS<6,Real>>;
template class PointwiseSolutionStorage<DLA::VectorS<7,Real>>;

} //namespace SANS
