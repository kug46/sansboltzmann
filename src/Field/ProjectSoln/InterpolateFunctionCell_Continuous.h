// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTERPOLATEFUNCTIONCELL_CONTINUOUS_H
#define INTERPOLATEFUNCTIONCELL_CONTINUOUS_H

// Cell integral to project exact solution with continuous elements

#include "Field/Element/ElementInterpolation_Nodal.h"
#include "Topology/ElementTopology.h"

#include "Field/Field.h"
#include "Field/XField.h"

#include "Field/tools/GroupFunctorType.h"
#include "Field/Tuple/FieldTuple.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  Projects a solution function onto a continuous field
//

template<class SOLN>
class InterpolateFunctionCell_Continuous_impl :
    public GroupFunctorCellType< InterpolateFunctionCell_Continuous_impl<SOLN> >
{
public:
  typedef typename SOLN::PhysDim PhysDim;
  typedef typename SOLN::template ArrayQ<Real> ArrayQ;

  // Save off the boundary trace integrand and the residual vectors
  InterpolateFunctionCell_Continuous_impl( const SOLN& soln, const std::vector<int>& cellGroups ) :
    soln_(soln), cellGroups_(cellGroups) {}

  std::size_t nCellGroups() const          { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n];     }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology>
  void
  apply( const typename FieldTuple<XField<PhysDim, typename Topology::TopoDim>, Field<PhysDim, typename Topology::TopoDim, ArrayQ>, TupleClass<>>::
                        template FieldCellGroupType<Topology>& fldsCell,
         const int cellGroupGlobal )
  {
    typedef typename XField<PhysDim, typename Topology::TopoDim>       ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field<PhysDim, typename Topology::TopoDim, ArrayQ>::template FieldCellGroupType<Topology> QFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

    const XFieldCellGroupType& xfldCell = get<0>(fldsCell);
          QFieldCellGroupType& qfldCell = const_cast<QFieldCellGroupType&>( get<1>(fldsCell) );

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );

    // class for computing the projection
    ElementInterpolation_Nodal<typename Topology::TopoDim, Topology> interpolater(qfldElem.basis());

    // loop over elements within group
    const int nelem = xfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid DOFs to element
      xfldCell.getElement( xfldElem, elem );

      // trace orientation must be correct for Herarchical
      if (qfldCell.basis()->category() == BasisFunctionCategory_Hierarchical &&
          qfldCell.basis()->order() >= 3)
        qfldElem.setTraceOrientation(qfldCell.associativity(elem).traceOrientation());

      // project the solution on the element
      interpolater.interpolate(xfldElem, qfldElem, soln_);

      // set the projected solution
      qfldCell.setElement( qfldElem, elem );
    }
  }

protected:
  const SOLN& soln_;
  const std::vector<int> cellGroups_;
};


// Factory function

template<class SOLN>
InterpolateFunctionCell_Continuous_impl<SOLN>
InterpolateFunctionCell_Continuous( const SOLN& soln, const std::vector<int>& cellGroups)
{
  return InterpolateFunctionCell_Continuous_impl<SOLN>(soln, cellGroups);
}


} //namespace SANS

#endif  // INTERPOLATEFUNCTIONCELL_CONTINUOUS_H
