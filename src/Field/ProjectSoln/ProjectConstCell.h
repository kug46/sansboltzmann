// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PROJECTCONSTCELL_H
#define PROJECTCONSTCELL_H

// Projection of a constant onto cell elements

#include <vector>

#include "Topology/ElementTopology.h"

#include "Field/Element/ElementProjection_L2.h"

#include "Field/tools/GroupFunctorType.h"
#include "Field/Field.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  Projects a solution function onto a discontinuous field
//

template<class PhysDim, class ArrayQ>
class ProjectConstCell_impl :
    public GroupFunctorCellType< ProjectConstCell_impl<PhysDim, ArrayQ> >
{
public:

  // Save off the boundary trace integrand and the residual vectors
  ProjectConstCell_impl( const ArrayQ& q0, const std::vector<int>& cellGroups ) :
    q0_(q0), cellGroups_(cellGroups) {}

  std::size_t nCellGroups() const          { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n];     }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology>
  void
  apply( const typename Field<PhysDim, typename Topology::TopoDim, ArrayQ>::template FieldCellGroupType<Topology>& fldsCell,
         const int cellGroupGlobal )
  {
    typedef typename Field<PhysDim, typename Topology::TopoDim, ArrayQ>::template FieldCellGroupType<Topology> QFieldCellGroupType;

    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

    QFieldCellGroupType& qfldCell = const_cast<QFieldCellGroupType&>( fldsCell );

    // element field variables
    ElementQFieldClass qfldElem( qfldCell.basis() );

    // class for computing the projection
    ElementProjectionConst_L2<typename Topology::TopoDim, Topology> projector(qfldElem.basis());

    // project the constant on the element
    projector.project(qfldElem, q0_);

    // loop over elements within group
    const int nelem = qfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // trace orientation must be correct, but does not matter for a constant
      if (qfldCell.basis()->category() == BasisFunctionCategory_Hierarchical &&
          qfldCell.basis()->order() >= 3)
        qfldElem.setTraceOrientation(qfldCell.associativity(elem).traceOrientation());

      // set the projected constant
      qfldCell.setElement( qfldElem, elem );
    }
  }

protected:
  const ArrayQ& q0_;
  const std::vector<int> cellGroups_;
};


// Factory function

template<class PhysDim, class ArrayQ>
ProjectConstCell_impl<PhysDim, ArrayQ>
ProjectConstCell( const ArrayQ& q0, const std::vector<int>& cellGroups)
{
  return ProjectConstCell_impl<PhysDim, ArrayQ>(q0, cellGroups);
}


} //namespace SANS

#endif  // PROJECTCONSTCELL_H
