// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PROJECTGLOBALFIELD_H_
#define PROJECTGLOBALFIELD_H_

#include "Field/Field.h"

namespace SANS
{

//----------------------------------------------------------------------------//

/*
 * A function for projecting solutions between two global Fields.
 */
template<class PhysDim, class TopoDim, class T>
void ProjectGlobalField(const Field<PhysDim, TopoDim, T>& qfldFrom,
                              Field<PhysDim, TopoDim, T>& qfldTo);

}

#endif /* PROJECTGLOBALFIELD_H_ */
