// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef POINTWISESOLUTIONSTORAGE_H_
#define POINTWISESOLUTIONSTORAGE_H_

#include <vector>
#include <array>

#include "tools/SANSnumerics.h"

namespace SANS
{

template<class T>
class PointwiseSolutionStorage
{
public:

  PointwiseSolutionStorage() {};
  ~PointwiseSolutionStorage() {};

  void resizeCellGroups(int n);
  void resizeCellElems(int cellgroup, int nelem, int nquad_per_elem);
  void setSolution(int cellgroup, int elem, int quad_index, T q, Real distance);

  const T& getSolution(int cellgroup, int elem, int quad_index) const;
  Real getDistance(int cellgroup, int elem, int quad_index) const;
  std::vector<std::array<std::size_t,3>> getUnallocatedPoints() const;

protected:
  std::vector<std::vector<std::vector<T>>> solutionData_; //indexing: [cellgroup][cellelem][quadpoint]
  std::vector<std::vector<std::vector<Real>>> distanceData_; //indexing: [cellgroup][cellelem][quadpoint]
};

}

#endif /* POINTWISESOLUTIONSTORAGE_H_ */
