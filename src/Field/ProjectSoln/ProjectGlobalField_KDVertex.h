// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PROJECTGLOBALFIELD_KDVERTEX_H
#define PROJECTGLOBALFIELD_KDVERTEX_H

#include "tools/SANSnumerics.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

namespace SANS
{

template<class PhysDim>
class ProjectGlobalField_KDVertex
{
public:
  static const int D = PhysDim::D;
  typedef Real value_type;

  typedef DLA::VectorS<D,Real> VectorX; // coordinates vector

  ProjectGlobalField_KDVertex(const VectorX& X, int cellgroup, int cellelem, int quad_index) :
    X_(X), cellgroup_(cellgroup), cellelem_(cellelem), quad_index_(quad_index) {}

  ProjectGlobalField_KDVertex(const ProjectGlobalField_KDVertex& V) :
    X_(V.X_), cellgroup_(V.cellgroup_), cellelem_(V.cellelem_), quad_index_(V.quad_index_) {}

  ProjectGlobalField_KDVertex& operator=(const ProjectGlobalField_KDVertex& V)
  {
    X_  = V.X_;
    cellgroup_ = V.cellgroup_;
    cellelem_ = V.cellelem_;
    quad_index_ = V.quad_index_;
    return *this;
  }

  inline value_type operator[](size_t const n) const { return X_[n]; }

  const VectorX& getCoordinates() const { return X_; }

  void getInfo(int& cellgroup, int& cellelem, int& quad_index) const
  {
    cellgroup = cellgroup_;
    cellelem  = cellelem_;
    quad_index = quad_index_;
  }

protected:
  VectorX X_;
  int cellgroup_;
  int cellelem_;
  int quad_index_;
};

}

#endif //PROJECTGLOBALFIELD_KDVERTEX_H
