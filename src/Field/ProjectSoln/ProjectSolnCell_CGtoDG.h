// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PROJECTSOLNCELL_CGTODG_H
#define PROJECTSOLNCELL_CGTODG_H

// Evaluate solution at Lagrange points on cell elements

#include "Topology/ElementTopology.h"

#include "Field/Field.h"
#include "Field/XField.h"

#include "Field/tools/GroupFunctorType.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/ElementProjection_L2.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Transfers the solution between two fields of the same order and basis
// If the basis is hierarchical such that it is element dependent, it will also resort to elemental L2 transfer.

template<class PhysDim, class ArrayQ>
class ProjectSolnCell_CGtoDG_impl :
    public GroupFunctorCellType< ProjectSolnCell_CGtoDG_impl<PhysDim, ArrayQ> >
{
public:

  ProjectSolnCell_CGtoDG_impl( const std::vector<int>& cellGroups ) :
    cellGroups_(cellGroups) {}

  std::size_t nCellGroups() const          { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n];     }


  // Integration function that integrates each element in the cell group
  template <class Topology>
  void
  apply( const typename FieldTuple<Field<PhysDim, typename Topology::TopoDim, ArrayQ>,
                                   Field<PhysDim, typename Topology::TopoDim, ArrayQ>, TupleClass<>>::
                        template FieldCellGroupType<Topology>& fldsCell,
         const int cellGroupGlobal )
  {
    typedef typename Field<PhysDim, typename Topology::TopoDim, ArrayQ>::template FieldCellGroupType<Topology> QFieldCellGroupType;

    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

    const QFieldCellGroupType& qfldFromCell = get<0>(fldsCell);
          QFieldCellGroupType& qfldToCell = const_cast<QFieldCellGroupType&>( get<1>(fldsCell) );

    // element field variables
    ElementQFieldClass qfldElem( qfldFromCell.basis() ), qfldElem_To( qfldToCell.basis() );

    SANS_ASSERT(qfldFromCell.basis()->order() == qfldToCell.basis()->order() );
    SANS_ASSERT(qfldFromCell.basis()->category() == qfldToCell.basis()->category() );

    // loop over elements within group
    SANS_ASSERT(qfldFromCell.nElem() == qfldToCell.nElem() );
    const int nelem = qfldFromCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid DOFs to element
      qfldFromCell.getElement( qfldElem, elem );

      // set the solution
      qfldToCell.setElement( qfldElem, elem );

    }
  }

protected:
  const std::vector<int> cellGroups_;
};


// Factory function

template<class PhysDim, class ArrayQ>
ProjectSolnCell_CGtoDG_impl<PhysDim, ArrayQ>
ProjectSolnCell_CGtoDG( const std::vector<int>& cellGroups)
{
  return {cellGroups};
}


} //namespace SANS

#endif  // PROJECTSOLNCELL_CGTODG_H
