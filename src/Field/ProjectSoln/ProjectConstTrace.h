// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PROJECTCONSTTRACE_H
#define PROJECTCONSTTRACE_H

// Projection of a constant onto interior trace elements

#include <vector>

#include "Topology/ElementTopology.h"

#include "Field/Element/ElementProjection_L2.h"

#include "Field/tools/GroupFunctorType.h"
#include "Field/Field.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  Projects a solution function onto a discontinuous field
//

template<class PhysDim, class ArrayQ>
class ProjectConstTrace_impl :
    public GroupFunctorHubTraceType     < ProjectConstTrace_impl<PhysDim, ArrayQ> >,
    public GroupFunctorInteriorTraceType< ProjectConstTrace_impl<PhysDim, ArrayQ> >,
    public GroupFunctorBoundaryTraceType< ProjectConstTrace_impl<PhysDim, ArrayQ> >
{
public:

  // Save off the boundary trace integrand and the residual vectors
  ProjectConstTrace_impl( const ArrayQ& q0, const std::vector<int>& traceGroups ) :
    q0_(q0), traceGroups_(traceGroups) {}

  std::size_t nHubTraceGroups() const               { return traceGroups_.size(); }
  std::size_t hubTraceGroup(const int n) const      { return traceGroups_[n];     }

  std::size_t nInteriorTraceGroups() const          { return traceGroups_.size(); }
  std::size_t interiorTraceGroup(const int n) const { return traceGroups_[n];     }

  std::size_t nBoundaryTraceGroups() const          { return traceGroups_.size(); }
  std::size_t boundaryTraceGroup(const int n) const { return traceGroups_[n];     }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the trace group
  template <class TopologyTrace>
  void
  apply( const typename Field<PhysDim, typename TopologyTrace::CellTopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& fldsTrace,
         const int traceGroupGlobal )
  {
    typedef typename Field<PhysDim, typename TopologyTrace::CellTopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;

    typedef typename QFieldTraceGroupType::template ElementType<> ElementQFieldClass;
    // typedef typename ElementQFieldClass::TopoDim TopoDimTrace;

    QFieldTraceGroupType& qfldTrace = const_cast<QFieldTraceGroupType&>( fldsTrace );

    // element field variables
    ElementQFieldClass qfldElem( qfldTrace.basis() );

    // class for computing the projection
    ElementProjectionConst_L2<typename TopologyTrace::TopoDim, TopologyTrace> projector(qfldElem.basis());

    // project the constant on the element
    projector.project(qfldElem, q0_);

    // loop over elements within group
    const int nelem = qfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // set the projected constant
      qfldTrace.setElement( qfldElem, elem );
    }
  }

protected:
  const ArrayQ& q0_;
  const std::vector<int> traceGroups_;
};


// Factory function

template<class PhysDim, class ArrayQ>
ProjectConstTrace_impl<PhysDim, ArrayQ>
ProjectConstTrace( const ArrayQ& q0, const std::vector<int>& traceGroups)
{
  return ProjectConstTrace_impl<PhysDim, ArrayQ>(q0, traceGroups);
}


} //namespace SANS

#endif  // PROJECTCONSTTRACE_H
