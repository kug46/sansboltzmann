// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PROJECTSOLNGRADIENTCELL_DISCONTINUOUS_H
#define PROJECTSOLNGRADIENTCELL_DISCONTINUOUS_H

// Cell integral to project exact solution with discontinuous elements

#include "Topology/ElementTopology.h"

#include "Field/Field.h"
#include "Field/XField.h"

#include "Field/Element/ElementProjection_L2.h"

#include "Field/tools/GroupFunctorType.h"
#include "Field/Tuple/FieldTuple.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  Projects a solution function onto a discontinuous field
//

template<class SOLN>
class ProjectSolnGradientCell_Discontinuous_impl :
    public GroupFunctorCellType< ProjectSolnGradientCell_Discontinuous_impl<SOLN> >
{
public:
  typedef typename SOLN::PhysDim PhysDim;
  typedef typename SOLN::template ArrayQ<Real> ArrayQ;

  static const int D = PhysDim::D;
  typedef DLA::VectorS<D, ArrayQ > VectorArrayQ;       // vector of solution arrays, i.e. solution gradients/flux vector

  // Save off the boundary trace integrand and the residual vectors
  ProjectSolnGradientCell_Discontinuous_impl( const SOLN& soln, const std::vector<int>& cellGroups ) :
    soln_(soln), cellGroups_(cellGroups) {}

  std::size_t nCellGroups() const          { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n];     }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology>
  void
  apply( const typename FieldTuple<XField<PhysDim, typename Topology::TopoDim>,
                                    Field<PhysDim, typename Topology::TopoDim, VectorArrayQ>, TupleClass<>>::
                        template FieldCellGroupType<Topology>& fldsCell,
         const int cellGroupGlobal )
  {
    typedef typename XField<PhysDim, typename Topology::TopoDim>       ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field<PhysDim, typename Topology::TopoDim, VectorArrayQ>::template FieldCellGroupType<Topology> QFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

    const XFieldCellGroupType& xfldCell = get<0>(fldsCell);
          QFieldCellGroupType& qfldCell = const_cast<QFieldCellGroupType&>( get<1>(fldsCell) );

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );

    ElementProjectionSolutionGradient_L2<typename Topology::TopoDim, Topology> projector( qfldElem.basis() );

    // loop over elements within group
    const int nelem = xfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid/solution DOFs to element
      xfldCell.getElement( xfldElem, elem );

      // project the solution gradient on the element
      projector.project(xfldElem, qfldElem, soln_);

      // set the projected solution
      qfldCell.setElement( qfldElem, elem );
    }
  }

protected:
  const SOLN& soln_;
  const std::vector<int> cellGroups_;
};


// Factory function

template<class SOLN>
ProjectSolnGradientCell_Discontinuous_impl<SOLN>
ProjectSolnGradientCell_Discontinuous( const SOLN& soln, const std::vector<int>& cellGroups)
{
  return ProjectSolnGradientCell_Discontinuous_impl<SOLN>(soln, cellGroups);
}


} //namespace SANS

#endif  // PROJECTSOLNGRADIENTCELL_DISCONTINUOUS_H
