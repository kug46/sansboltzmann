// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Writes a point cloud of a field given sets of reference coordinates

#include "Field/output_Points.h"

#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"

#include "XFieldLine.h"
#include "XFieldArea.h"
#include "XFieldVolume.h"

#include "FieldLine.h"
#include "FieldArea.h"
#include "FieldVolume.h"

#include "FieldLiftLine_DG_Cell.h"
#include "FieldLiftArea_DG_Cell.h"
#include "FieldLiftVolume_DG_Cell.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"
#include "MPI/continuousElementMap.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template<class T, class PhysDim, class TopoDim, class Topology>
void
output_Point_CellGroup( mpi::communicator& comm, const std::vector<int>& cellIDs, const bool partitioned,
                          const typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology>& xfldGroup,
                          const typename  Field<PhysDim, TopoDim, T>::template FieldCellGroupType<Topology>& qfldGroup,
                          const std::vector<DLA::VectorS<TopoD2::D,Real>>& refPoints,
                          std::vector<FILE*> fps )
{
  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename  Field<PhysDim, TopoDim, T>::template FieldCellGroupType<Topology> QFieldCellGroupType;

  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfldGroup.basis() );
  ElementQFieldClass qfldElem( qfldGroup.basis() );

  static const int N = DLA::VectorSize<T>::M;

  const std::size_t nRefPoint = refPoints.size();

  int nElem_global;
  std::map<int,int> globalElemMap;
  continuousElementMap( comm, cellIDs, xfldGroup, nElem_global, globalElemMap );

  int nElemLocal = xfldGroup.nElem();

  // Dont't bother with empty groups
  if ( nElem_global == 0 ) return;
  if ( nElemLocal == 0 && partitioned ) return;

  if (partitioned) nElem_global = nElemLocal;

  int comm_rank = comm.rank();

  if (comm_rank == 0 || partitioned)
    for (int k = 0; k < N; k++)
      fprintf( fps[k], "%d %d\n", nElem_global, (int)nRefPoint );

  // grid coordinates and solution

  typename ElementQFieldClass::RefCoordType sRefCell;
  typename ElementXFieldClass::VectorX X;
  T q;

#ifdef SANS_MPI
  if (!partitioned)
  {
    int comm_size = comm.size();

    // maximum element chunk size that rank 0 will write at any given time
    int Elemchunk = nElem_global / comm_size + nElem_global % comm_size;

    // send one chunk of elements at a time to rank 0
    for (int rank = 0; rank < comm_size; rank++)
    {
      int elemLow  =  rank   *Elemchunk;
      int elemHigh = (rank+1)*Elemchunk;

      std::map<int,std::vector<DLA::VectorS<PhysDim::D+N,double>>> buffer;

      // loop over elements within group
      for (int elem = 0; elem < nElemLocal; elem++)
      {
        int elemID = globalElemMap[cellIDs[elem]];

        if (elemID >= elemLow && elemID < elemHigh &&
            xfldGroup.associativity(elem).rank() == comm_rank)
        {
          // copy global grid/solution DOFs to element
          xfldGroup.getElement( xfldElem, elem );
          qfldGroup.getElement( qfldElem, elem );

          std::vector<DLA::VectorS<PhysDim::D+N,double>> nodeValues(nRefPoint);

          for ( std::size_t i = 0; i < nRefPoint; i++ )
          {
            sRefCell = refPoints[i];

            qfldElem.eval( sRefCell, q );

            int n = 0;
            for (int k = 0; k < N; k++)
              nodeValues[i][n++] = DLA::index(q,k);
          }

          buffer[elemID] = std::move(nodeValues);
        }
      }


      if (comm_rank == 0)
      {
        std::vector<std::map<int,std::vector<DLA::VectorS<PhysDim::D+N,double>>>> bufferOnRank;
        boost::mpi::gather(comm, buffer, bufferOnRank, 0 );

        // collapse down the buffer from all other ranks
        for (std::size_t i = 1; i < bufferOnRank.size(); i++)
          buffer.insert(bufferOnRank[i].begin(), bufferOnRank[i].end());

        //Write the node data
        for ( const auto& nodesPair : buffer)
        {
          for ( std::size_t i = 0; i < nRefPoint; i++ )
          {
            for (int n = 0; n < N; n++)
              fprintf( fps[n], "%22.15e ", nodesPair.second[i][n] );
          }
          for (int n = 0; n < N; n++)
            fprintf( fps[n], "\n" );
        }
      }
#ifndef __clang_analyzer__
      else // send the buffer to rank 0
        boost::mpi::gather(comm, buffer, 0 );
#endif

    } // for rank

  }
  else // if partitioned
  {
#endif

    // loop over elements within group
    for (int elem = 0; elem < nElemLocal; elem++)
    {
      // copy global grid/solution DOFs to element
      xfldGroup.getElement( xfldElem, elem );
      qfldGroup.getElement( qfldElem, elem );

      for ( std::size_t i = 0; i < nRefPoint; i++ )
      {
        sRefCell = refPoints[i];

        qfldElem.eval( sRefCell, q );

        for (int n = 0; n < N; n++)
          fprintf( fps[n], "%22.15e ", DLA::index(q,n) );
      }

      for (int n = 0; n < N; n++)
        fprintf( fps[n], "\n" );
    }

#ifdef SANS_MPI
  } // if !partitioned
#endif
}

#if 0
//----------------------------------------------------------------------------//
template<class PhysDim, class T>
void
output_Points( const Field< PhysDim, TopoD1, T >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names,
                const bool partitioned )
{
  FILE* fp = nullptr;

  if (qfld.comm()->rank() == 0 || partitioned)
  {
    std::cout << "output_Point: filename = " << filename << std::endl;
    fp = fopen( filename.c_str(), "w" );
    if (fp == NULL)
      SANS_DEVELOPER_EXCEPTION("output_Point<PhysDim,TopoD1> - Error opening file: %s", filename.c_str());

    fprintf( fp, "\"\"\n" );
    fprintf( fp, "VARIABLES = \"X\"" );

    const char XYZ[] = "XYZ";
    for (int k = 1; k < PhysDim::D; k++)
      fprintf( fp, ", \"%c\"", XYZ[k] );

    static const int N = DLA::VectorSize<T>::M;
    if (state_names.empty())
    {
      for (int k = 0; k < N; k++)
        fprintf( fp, ", \"q%d\"", k+1 );
    }
    else
    {
      SANS_ASSERT((int) state_names.size() == N);
      for (int k = 0; k < N; k++)
        fprintf( fp, ", \"%s\"", state_names[k].c_str());
    }
    fprintf( fp, "\n" );
  }

  const XField<PhysDim,TopoD1>& xgrid = qfld.getXField();

  const int ngroup = qfld.nCellGroups();

  // loop over cell groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Line) )
    {
      output_Point_CellGroup<T, PhysDim, TopoD1, Line>( *xgrid.comm(), xgrid.cellIDs(group), partitioned,
                                                          xgrid.template getCellGroup<Line>(group),
                                                           qfld.template getCellGroup<Line>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Points(XField<PhysD1,TopoD1>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

#if 0
  // loop over trace boundary groups
  for (int group = 0; group < qfld.nBoundaryTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      output_Point_TraceGroup<N>( group, xgrid.template getBoundaryTraceGroup<Triangle>(group),
                                            qfld.template getBoundaryTraceGroup<Triangle>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Points(XField<PhysD3,TopoD3>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }
#endif

  if (qfld.comm()->rank() == 0 || partitioned)
    fclose( fp );
}
#endif

//----------------------------------------------------------------------------//
template<class PhysDim, class T>
void
output_Points( const Field< PhysDim, TopoD2, T >& qfld, const std::string& filename,
               const std::vector<std::vector<DLA::VectorS<TopoD2::D,Real>>>& refPoints,
               const std::vector<std::string>& state_names,
               const bool partitioned )
{

  const XField<PhysDim,TopoD2>& xgrid = qfld.getXField();

  const int ngroup = qfld.nCellGroups();
  SANS_ASSERT_MSG( ngroup == (int)refPoints.size(), "ngroup = %d, refPoints.size() == %d", ngroup, refPoints.size());

  std::vector<FILE*> fps(state_names.size(), nullptr);

  if (qfld.comm()->rank() == 0 || partitioned)
  {
    SANS_ASSERT((int)state_names.size() == DLA::VectorSize<T>::M);
    for ( std::size_t i = 0; i < state_names.size(); i++ )
    {
      std::string filename_var = filename + "_" + state_names[i] + ".dat";
      std::cout << "output_Point: filename = " << filename_var << std::endl;
      fps[i] = fopen( filename_var.c_str(), "w" );
      if (fps[i] == NULL)
        SANS_DEVELOPER_EXCEPTION("Error opening file: %s", filename_var.c_str());

      fprintf( fps[i], "%d\n", ngroup );
    }
  }

  // loop over cell groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      output_Point_CellGroup<T, PhysDim, TopoD2, Triangle>( *xgrid.comm(), xgrid.cellIDs(group), partitioned,
                                                              xgrid.template getCellGroup<Triangle>(group),
                                                               qfld.template getCellGroup<Triangle>(group), refPoints[group], fps );
    }
    else if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Quad) )
    {
      output_Point_CellGroup<T, PhysDim, TopoD2, Quad>( *xgrid.comm(), xgrid.cellIDs(group), partitioned,
                                                          xgrid.template getCellGroup<Quad>(group),
                                                           qfld.template getCellGroup<Quad>(group), refPoints[group], fps );
    }
    else
    {
      SANS_DEVELOPER_EXCEPTION( "unknown topology" );
    }
  }

  if (qfld.comm()->rank() == 0 || partitioned)
    for ( std::size_t i = 0; i < state_names.size(); i++ )
      fclose( fps[i] );
}

#if 0
//----------------------------------------------------------------------------//
template<class T>
void
output_Points( const Field< PhysD3, TopoD3, T >& qfld, const std::string& filename,
                const std::vector<std::string>& state_names,
                const bool partitioned )
{
  FILE* fp = nullptr;

  if (qfld.comm()->rank() == 0 || partitioned)
  {
    std::cout << "output_Point: filename = " << filename << std::endl;
    fp = fopen( filename.c_str(), "w" );
    if (fp == NULL)
      SANS_DEVELOPER_EXCEPTION("output_Point<PhysD3,TopoD3> - Error opening file: %s", filename.c_str());

    fprintf( fp, "\"\"\n" );
    fprintf( fp, "VARIABLES = \"X\", \"Y\", \"Z\"" );

    static const int N = DLA::VectorSize<T>::M;
    if (state_names.empty())
    {
      for (int k = 0; k < N; k++)
        fprintf( fp, ", \"q%d\"", k+1 );
    }
    else
    {
      SANS_ASSERT((int) state_names.size() == N);
      for (int k = 0; k < N; k++)
        fprintf( fp, ", \"%s\"", state_names[k].c_str());
    }
    fprintf( fp, "\n" );
  }

  const XField<PhysD3,TopoD3>& xgrid = qfld.getXField();

  const int ngroup = qfld.nCellGroups();

  // loop over cell groups
  for (int group = 0; group < ngroup; group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Tet) )
    {
      output_Point_CellGroup<T, PhysD3, TopoD3, Tet>( *xgrid.comm(), xgrid.cellIDs(group), partitioned,
                                                        xgrid.template getCellGroup<Tet>(group),
                                                        qfld.template getCellGroup<Tet>(group), fp );
    }
    else if ( xgrid.getCellGroupBase(group).topoTypeID() == typeid(Hex) )
    {
      output_Point_CellGroup<T, PhysD3, TopoD3, Hex>( *xgrid.comm(), xgrid.cellIDs(group), partitioned,
                                                        xgrid.template getCellGroup<Hex>(group),
                                                        qfld.template getCellGroup<Hex>(group), fp );
    }
    else
      SANS_DEVELOPER_EXCEPTION("Unknown topology");
  }

  // loop over trace boundary groups
  for (int groupTrace = 0; groupTrace < xgrid.nBoundaryTraceGroups(); groupTrace++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(groupTrace).topoTypeID() == typeid(Triangle) )
    {
      // determine topology for Left cell group
      int groupL = xgrid.template getBoundaryTraceGroup<Triangle>(groupTrace).getGroupLeft();

      if ( xgrid.getCellGroupBase(groupL).topoTypeID() == typeid(Tet) )
        output_Point_TraceCellGroup<T, PhysD3, TopoD3, Tet, Triangle>( groupTrace,
                                                                         *xgrid.comm(), xgrid.boundaryTraceIDs(groupTrace), partitioned,
                                                                         xgrid.template getBoundaryTraceGroup<Triangle>(groupTrace),
                                                                          qfld.template getCellGroup<Tet>(groupL), fp );
      else
        SANS_DEVELOPER_EXCEPTION("Unknown topology");
    }
    else if ( xgrid.getBoundaryTraceGroupBase(groupTrace).topoTypeID() == typeid(Quad) )
    {
      // determine topology for Left cell group
      int groupL = xgrid.template getBoundaryTraceGroup<Quad>(groupTrace).getGroupLeft();

      if ( xgrid.getCellGroupBase(groupL).topoTypeID() == typeid(Hex) )
        output_Point_TraceCellGroup<T, PhysD3, TopoD3, Hex, Quad>( groupTrace,
                                                                     *xgrid.comm(), xgrid.boundaryTraceIDs(groupTrace), partitioned,
                                                                     xgrid.template getBoundaryTraceGroup<Quad>(groupTrace),
                                                                      qfld.template getCellGroup<Hex>(groupL), fp );
      else
        SANS_DEVELOPER_EXCEPTION("Unknown topology");
    }
    else
      SANS_DEVELOPER_EXCEPTION("Unknown topology");
  }

  // loop over trace boundary groups
  for (int group = 0; group < qfld.nBoundaryTraceGroups(); group++)
  {
    // dispatch integration over cell of group
    if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Triangle) )
    {
      output_Point_TraceGroup<T, PhysD3, TopoD3, Triangle>( group, xgrid.template getBoundaryTraceGroup<Triangle>(group),
                                                              qfld.template getBoundaryTraceGroup<Triangle>(group), fp );
    }
    else if ( xgrid.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Quad) )
    {
      output_Point_TraceGroup<T, PhysD3, TopoD3, Quad>( group, xgrid.template getBoundaryTraceGroup<Quad>(group),
                                                          qfld.template getBoundaryTraceGroup<Quad>(group), fp );
    }
    else
    {
      const char msg[] = "Error in output_Points(XField<PhysD3,TopoD3>): unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  if (qfld.comm()->rank() == 0 || partitioned)
    fclose( fp );
}
#endif

//Explicitly instantiate the function
#if 0
template void
output_Points( const Field< PhysD1, TopoD1, Real >& Q, const std::string& filename,
               const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Points( const Field< PhysD2, TopoD1, Real >& Q, const std::string& filename,
               const std::vector<std::string>& state_names, const bool partitioned );
#endif
template void
output_Points( const Field< PhysD2, TopoD2, Real >& Q, const std::string& filename,
               const std::vector<std::vector<DLA::VectorS<TopoD2::D,Real>>>& refPoints,
               const std::vector<std::string>& state_names, const bool partitioned );
#if 0
template void
output_Points( const Field< PhysD3, TopoD2, Real >& Q, const std::string& filename,
               const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Points( const Field< PhysD3, TopoD3, Real >& Q, const std::string& filename,
               const std::vector<std::string>& state_names, const bool partitioned );


template void
output_Points( const Field< PhysD1, TopoD1, DLA::VectorS<1,Real> >& Q, const std::string& filename,
               const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Points( const Field< PhysD1, TopoD1, DLA::VectorS<2,Real> >& Q, const std::string& filename,
               const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Points( const Field< PhysD1, TopoD1, DLA::VectorS<3,Real> >& Q, const std::string& filename,
               const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Points( const Field< PhysD1, TopoD1, DLA::VectorS<4,Real> >& Q, const std::string& filename,
               const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Points( const Field< PhysD1, TopoD1, DLA::VectorS<5,Real> >& Q, const std::string& filename,
               const std::vector<std::string>& state_names, const bool partitioned );

template void
output_Points( const Field< PhysD2, TopoD1, DLA::VectorS<1,Real> >& Q, const std::string& filename,
               const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Points( const Field< PhysD2, TopoD1, DLA::VectorS<2,Real> >& Q, const std::string& filename,
               const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Points( const Field< PhysD2, TopoD1, DLA::VectorS<3,Real> >& Q, const std::string& filename,
               const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Points( const Field< PhysD2, TopoD1, DLA::VectorS<8,Real> >& Q, const std::string& filename,
               const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Points( const Field< PhysD2, TopoD1, DLA::VectorS<19,Real> >& Q, const std::string& filename,
               const std::vector<std::string>& state_names, const bool partitioned );
#endif
template void
output_Points( const Field< PhysD2, TopoD2, DLA::VectorS<1,Real> >& Q, const std::string& filename,
               const std::vector<std::vector<DLA::VectorS<TopoD2::D,Real>>>& refPoints,
               const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Points( const Field< PhysD2, TopoD2, DLA::VectorS<2,Real> >& Q, const std::string& filename,
               const std::vector<std::vector<DLA::VectorS<TopoD2::D,Real>>>& refPoints,
               const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Points( const Field< PhysD2, TopoD2, DLA::VectorS<3,Real> >& Q, const std::string& filename,
               const std::vector<std::vector<DLA::VectorS<TopoD2::D,Real>>>& refPoints,
               const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Points( const Field< PhysD2, TopoD2, DLA::VectorS<4,Real> >& Q, const std::string& filename,
               const std::vector<std::vector<DLA::VectorS<TopoD2::D,Real>>>& refPoints,
               const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Points( const Field< PhysD2, TopoD2, DLA::VectorS<5,Real> >& Q, const std::string& filename,
               const std::vector<std::vector<DLA::VectorS<TopoD2::D,Real>>>& refPoints,
               const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Points( const Field< PhysD2, TopoD2, DLA::VectorS<6,Real> >& Q, const std::string& filename,
               const std::vector<std::vector<DLA::VectorS<TopoD2::D,Real>>>& refPoints,
               const std::vector<std::string>& state_names, const bool partitioned );

#if 0
template void
output_Points( const Field< PhysD3, TopoD2, DLA::VectorS<1,Real> >& Q, const std::string& filename,
               const std::vector<std::string>& state_names, const bool partitioned );

template void
output_Points( const Field< PhysD3, TopoD3, DLA::VectorS<1,Real> >& Q, const std::string& filename,
               const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Points( const Field< PhysD3, TopoD3, DLA::VectorS<2,Real> >& Q, const std::string& filename,
               const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Points( const Field< PhysD3, TopoD3, DLA::VectorS<3,Real> >& Q, const std::string& filename,
               const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Points( const Field< PhysD3, TopoD3, DLA::VectorS<4,Real> >& Q, const std::string& filename,
               const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Points( const Field< PhysD3, TopoD3, DLA::VectorS<5,Real> >& Q, const std::string& filename,
               const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Points( const Field< PhysD3, TopoD3, DLA::VectorS<6,Real> >& Q, const std::string& filename,
               const std::vector<std::string>& state_names, const bool partitioned );
template void
output_Points( const Field< PhysD3, TopoD3, DLA::VectorS<7,Real> >& Q, const std::string& filename,
               const std::vector<std::string>& state_names, const bool partitioned );
#endif

} //namespace SANS
