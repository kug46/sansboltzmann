// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ELEMENTTUPLE_H
#define ELEMENTTUPLE_H

#include <type_traits>

#include "tools/Tuple.h"

#include "Field/Element/Element.h"

#include "FieldTuple_Traits.h"
#include "BasisTuple.h"
#include "Field/Tuple/ParamTuple.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// A class for storing any number of element variables that are created from basis functions
//
// template parameters:
//   Element types: ElementTypeL, ElementTypeR
//----------------------------------------------------------------------------//
template<class ElementTypeL, class ElementTypeR, class >
class ElementTuple;

template<class ElementTypeL_, class ElementTypeR_, int Level>
class ElementTuple<ElementTypeL_, ElementTypeR_, TupleClass<Level> >
{
public:
  typedef ElementTypeL_ ElementTypeL;
  typedef ElementTypeR_ ElementTypeR;

  static_assert( std::is_same<typename ElementTypeL::TopologyType,
                              typename ElementTypeR::TopologyType>::value,
                              "ElementTypeL and ElementTypeR must have the same topology" );

  typedef typename ElementTypeL::RefCoordType RefCoordType;
  typedef typename ElementTypeL::TopologyType TopologyType;

  typedef BasisTuple< typename ElementTypeL::BasisType,
                      typename ElementTypeR::BasisType, TupleClass<Level>> BasisType;

  typedef typename BasisType::BasisPointType BasisPointType;
  typedef typename BasisType::BasisPointDerivativeType BasisPointDerivativeType;

  typedef ParamTuple< typename ElementTypeL::T,
                      typename ElementTypeR::T, TupleClass<Level> > T;

  typedef ParamTuple< typename ElementTypeL::gradT,
                      typename ElementTypeR::gradT, TupleClass<Level> > gradT;

  ElementTuple( const ElementTypeL& elemL, const ElementTypeR& elemR ) :
    fldBasis_(elemL.basis(), elemR.basis()), elemL_(elemL), elemR_(elemR) {}

  ElementTuple( const ElementTuple& fldElem ) :
    fldBasis_(fldElem.fldBasis_), elemL_(fldElem.elemL_), elemR_(fldElem.elemR_) {}

  ElementTuple& operator=( const ElementTuple& fldElem ) = delete;

  explicit ElementTuple( const BasisType& fldBasis ) :
    fldBasis_(fldBasis), elemL_(fldBasis.basisL()), elemR_(fldBasis.basisR()) {}


  const BasisType& basis() const { return fldBasis_; }

  // DOF accessor

  T DOF( const int i ) const
  {
    return T(elemL_.DOF(i), elemR_.DOF(i));
  }

  // evaluation routines

  void eval( const RefCoordType& ref, T& params ) const
  {
    elemL_.eval( ref, params.paramL() );
    elemR_.eval( ref, params.paramR() );
  }

  template<class QuadPoint>
  void eval( const QuadPoint& ref, T& params ) const
  {
    elemL_.eval( ref, params.paramL() );
    elemR_.eval( ref, params.paramR() );
  }


  void evalDerivative( const RefCoordType& ref, gradT& params ) const
  {
    elemL_.evalDerivative( ref, params.paramL() );
    elemR_.evalDerivative( ref, params.paramR() );
  }

  template<class QuadPoint>
  void evalDerivative( const QuadPoint& ref, gradT& params ) const
  {
    elemL_.evalDerivative( ref, params.paramL() );
    elemR_.evalDerivative( ref, params.paramR() );
  }

  void evalBasis( const RefCoordType& Ref, BasisPointType& phi ) const
  {
    elemL_.evalBasis( Ref, phi.phiL() );
    elemR_.evalBasis( Ref, phi.phiR() );
  }
  void evalBasisDerivative( const RefCoordType& Ref, BasisPointDerivativeType& dphi ) const
  {
    elemL_.evalBasisDerivative( Ref, dphi.dphiL() );
    elemR_.evalBasisDerivative( Ref, dphi.dphiR() );
  }

  void evalFromBasis( const BasisPointType& phi, int nphi, T& params ) const
  {
    elemL_.evalFromBasis( phi.phiL(), phi.phiL().size(), params.paramL() );
    elemR_.evalFromBasis( phi.phiR(), phi.phiR().size(), params.paramR() );
  }

  // accessors

  ElementTypeL& elemL() { return elemL_; }
  ElementTypeR& elemR() { return elemR_; }

  const ElementTypeL& elemL() const { return elemL_; }
  const ElementTypeR& elemR() const { return elemR_; }

  // Generic accessors for Tuple get function
  ElementTypeL& left() { return elemL_; }
  ElementTypeR& right() { return elemR_; }

  const ElementTypeL& left() const { return elemL_; }
  const ElementTypeR& right() const { return elemR_; }

  // appending routines

  // appending an element (R) to "this" tuple (L)
  //    e.g. ((elem0, elem1), elem2)
  template<class TR, class TopoDimR, class TopologyR>
  ElementTuple< ElementTuple<ElementTypeL,ElementTypeR,TupleClass<Level>>,
                Element<TR,TopoDimR,TopologyR>                           , TupleClass<Level>>
  operator,(const Element<TR,TopoDimR,TopologyR>& fldElem)
  {
    return ElementTuple< ElementTuple<ElementTypeL,ElementTypeR,TupleClass<Level>>,
                         Element<TR,TopoDimR,TopologyR>                           , TupleClass<Level> >(*this, fldElem);
  }

  template<class PhysDimR, class TopoDimR, class TopologyR>
  ElementTuple< ElementTuple<ElementTypeL,ElementTypeR,TupleClass<Level>>,
                ElementXField<PhysDimR, TopoDimR,TopologyR>              , TupleClass<Level>>
  operator,(const ElementXField<PhysDimR, TopoDimR,TopologyR>& xfldElem)
  {
    return ElementTuple< ElementTuple<ElementTypeL,ElementTypeR,TupleClass<Level>>,
                         ElementXField<PhysDimR, TopoDimR,TopologyR>              , TupleClass<Level>>(*this, xfldElem);
  }


  // appending a tuple to "this" tuple, i.e ((elem0, elem1), (elem2, elem3))
  // this increments the level to indicate a tuple of tuples
  template<class RElementTypeL, class RElementTypeR>
  ElementTuple< ElementTuple<ElementTypeL ,ElementTypeR ,TupleClass<Level>>,
                ElementTuple<RElementTypeL,RElementTypeR,TupleClass<Level>>, TupleClass<Level+1> >
  operator,(const ElementTuple<RElementTypeL,RElementTypeR,TupleClass<Level>>& fld)
  {
    return ElementTuple< ElementTuple<ElementTypeL ,ElementTypeR ,TupleClass<Level>>,
                         ElementTuple<RElementTypeL,RElementTypeR,TupleClass<Level>>, TupleClass<Level+1>>(*this,fld);
  }


  // appending a tuple of a lower level to "this" tuple, i.e (((elem0, elem1), (elem2, elem3)), (elem4, elem5))
  template<class RElementTypeL, class RElementTypeR, int RLevel>
  typename std::enable_if< (RLevel < Level),
                           ElementTuple< ElementTuple<ElementTypeL ,ElementTypeR ,TupleClass<Level>>,
                                         ElementTuple<RElementTypeL,RElementTypeR,TupleClass<RLevel>>, TupleClass<Level> >
                         >::type
  operator,(const ElementTuple<RElementTypeL,RElementTypeR,TupleClass<RLevel>>& fld)
  {
    return ElementTuple< ElementTuple<ElementTypeL ,ElementTypeR ,TupleClass<Level>>,
                         ElementTuple<RElementTypeL,RElementTypeR,TupleClass<RLevel>>, TupleClass<Level> >(*this,fld);
  }


protected:
  const BasisType fldBasis_;
  ElementTypeL elemL_;
  ElementTypeR elemR_;
};


//----------------------------------------------------------------------------//
// Functions for creating tuples of elements
//----------------------------------------------------------------------------//

template<class PhysDim, class T, class TopoDim, class Topology>
ElementTuple< ElementXField<PhysDim, TopoDim, Topology>, Element<T, TopoDim, Topology>, TupleClass<> >
operator,(const ElementXField<PhysDim, TopoDim, Topology>& xfldElem, const Element<T, TopoDim, Topology>& fldElem)
{
  return ElementTuple< ElementXField<PhysDim, TopoDim, Topology>, Element<T, TopoDim, Topology>, TupleClass<> >(xfldElem,fldElem);
}

template<class PhysDim, class T, class TopoDim, class Topology>
ElementTuple< Element<T, TopoDim, Topology>, ElementXField<PhysDim, TopoDim, Topology>, TupleClass<> >
operator,(const Element<T, TopoDim, Topology>& fldElem, const ElementXField<PhysDim, TopoDim, Topology>& xfldElem)
{
  return ElementTuple< Element<T, TopoDim, Topology>, ElementXField<PhysDim, TopoDim, Topology>, TupleClass<> >(fldElem,xfldElem);
}

template<class T0, class T1, class TopoDim, class Topology>
ElementTuple< Element<T0, TopoDim, Topology>, Element<T1, TopoDim, Topology>, TupleClass<> >
operator,(const Element<T0, TopoDim, Topology>& fldElem0, const Element<T1, TopoDim, Topology>& fldElem1)
{
  return ElementTuple< Element<T0, TopoDim, Topology>, Element<T1, TopoDim, Topology>, TupleClass<> >(fldElem0,fldElem1);
}

template<class PhysDim, class TopoDim, class Topology>
ElementTuple< ElementXField<PhysDim, TopoDim, Topology>, ElementXField<PhysDim, TopoDim, Topology>, TupleClass<> >
operator,(const ElementXField<PhysDim, TopoDim, Topology>& xfldElemL, const ElementXField<PhysDim, TopoDim, Topology>& xfldElemR)
{
  return ElementTuple< ElementXField<PhysDim, TopoDim, Topology>, ElementXField<PhysDim, TopoDim, Topology>, TupleClass<> >(xfldElemL,xfldElemR);
}

template<class PhysDim, class T, class TopoDim, class Topology>
ElementTuple< ElementXField<PhysDim, TopoDim, Topology>, ElementSequence<T, TopoDim, Topology>, TupleClass<> >
operator,(const ElementXField<PhysDim, TopoDim, Topology>& xfldElem, const ElementSequence<T, TopoDim, Topology>& fldElem)
{
  return ElementTuple< ElementXField<PhysDim, TopoDim, Topology>, ElementSequence<T, TopoDim, Topology>, TupleClass<> >(xfldElem,fldElem);
}

template<class T0, class T1, class TopoDim, class Topology>
ElementTuple< ElementSequence<T0, TopoDim, Topology>, ElementSequence<T1, TopoDim, Topology>, TupleClass<> >
operator,(const ElementSequence<T0, TopoDim, Topology>& fldElems1, const ElementSequence<T1, TopoDim, Topology>& fldElems2)
{
  return ElementTuple< ElementSequence<T0, TopoDim, Topology>, ElementSequence<T1, TopoDim, Topology>, TupleClass<> >(fldElems1,fldElems2);
}

}

#endif //ELEMENTTUPLE_H
