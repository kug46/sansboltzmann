// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SURREALIZED_ELEMENT_TUPLE_H
#define SURREALIZED_ELEMENT_TUPLE_H

#include "tools/SANSnumerics.h"
#include "tools/Tuple.h"

namespace SANS
{

// forward declaration
template<class ElementTypeL, class ElementTypeR, class >
class ElementTuple;

template<class FieldGroupTypeL, class FieldGroupTypeR, class >
class FieldAssociativityTuple;


// Used to change only specific element types
namespace detail
{
template<class T, int size, int i>
struct ElemTypeT { typedef Real type; };

template<class T, int i>
struct ElemTypeT<T,i,i> { typedef T type; };
}

template<class FieldGroupType, class T, int i>
struct SurrealizedElementTuple
{
  typedef typename detail::ElemTypeT<T,0,i>::type ElemT;
  typedef typename FieldGroupType::template ElementType<ElemT> type;
};

template<class FieldGroupTypeL, class FieldGroupTypeR, int Level, class T, int i>
struct SurrealizedElementTuple<FieldAssociativityTuple<FieldGroupTypeL, FieldGroupTypeR, TupleClass<Level>>, T, i>
{
  static const int size = TupleSize< FieldGroupTypeL, Level >::size;
  typedef typename detail::ElemTypeT<T,size,i>::type ElemT;

  typedef ElementTuple< typename SurrealizedElementTuple<FieldGroupTypeL, T, i>::type,
                        typename FieldGroupTypeR::template ElementType<ElemT>, TupleClass<Level> > type;
};


}

#endif //SURREALIZED_ELEMENT_TUPLE_H
