// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDTUPLE_H
#define FIELDTUPLE_H

#include <type_traits>
#include <memory> // std::shared_ptr

#include "MPI/communicator_fwd.h"

#include "Field/FieldTypes.h"

#include "FieldAssociativityBaseTuple.h"
#include "FieldAssociativityTuple.h"
#include "tools/SANSException.h"
#include "tools/Tuple.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// A class for storing any number of fields
//
// template parameters:
//   FieldType(L/R) Two field types stored through a recursive pattern.
//                  FieldTypeL might be a FieldTuple.
//----------------------------------------------------------------------------//
template<class FieldTypeL, class FieldTypeR, class >
class FieldTuple;

template<class FieldTypeL, class FieldTypeR, int Level>
class FieldTuple< FieldTypeL, FieldTypeR, TupleClass<Level>> : public FieldTupleType,
  public FieldType< FieldTuple< FieldTypeL, FieldTypeR, TupleClass<Level> > >
{
public:
  static_assert( std::is_same<typename FieldTypeL::XFieldType, typename FieldTypeR::XFieldType>::value,
                 "XFields types do not match");
  typedef typename FieldTypeL::XFieldType XFieldType;
  typedef typename XFieldType::TopoDim TopoDim;

  //Use const FieldTypeL& if FieldTypeL not a FieldTupleType, otherwise use const FieldTypeL
  typedef typename std::conditional< std::is_base_of<FieldTupleType, FieldTypeL>::value,
                                     const FieldTypeL,
                                     typename std::add_lvalue_reference<const FieldTypeL>::type
                                   >::type FieldTypeL_ref;

  typedef typename std::conditional< std::is_base_of<FieldTupleType, FieldTypeR>::value,
                                     const FieldTypeR,
                                     typename std::add_lvalue_reference<const FieldTypeR>::type
                                   >::type FieldTypeR_ref;

  FieldTuple( const FieldTuple& flds ) : fldL_(flds.fldL_), fldR_(flds.fldR_) {}
  FieldTuple(const FieldTypeL& fldL, const FieldTypeR& fldR) : fldL_(fldL), fldR_(fldR) {}
  FieldTuple& operator=(const FieldTuple&) = delete;

  typedef FieldAssociativityBaseTuple< typename FieldTypeL::FieldTraceGroupBase,
                                       typename FieldTypeR::FieldTraceGroupBase, TupleClass<Level> > FieldTraceGroupBase;

  typedef FieldAssociativityBaseTuple< typename FieldTypeL::FieldCellGroupBase,
                                       typename FieldTypeR::FieldCellGroupBase, TupleClass<Level> > FieldCellGroupBase;


  template<class Topology>
  using FieldTraceGroupType = FieldAssociativityTuple< typename FieldTypeL::template FieldTraceGroupType<Topology>,
                                                       typename FieldTypeR::template FieldTraceGroupType<Topology>, TupleClass<Level> >;

  template<class Topology>
  using FieldCellGroupType = FieldAssociativityTuple< typename FieldTypeL::template FieldCellGroupType<Topology>,
                                                      typename FieldTypeR::template FieldCellGroupType<Topology>, TupleClass<Level> >;

  std::shared_ptr<mpi::communicator> comm() const
  {
    SANS_ASSERT( fldL_.comm() == fldR_.comm() );
    return fldL_.comm();
  }

  const XFieldType& getXField() const
  {
    SANS_ASSERT( &fldL_.getXField() == &fldR_.getXField() );
    return fldL_.getXField();
  }

  int nInteriorTraceGroups() const
  {
    SANS_ASSERT( fldL_.nInteriorTraceGroups() == fldR_.nInteriorTraceGroups() );
    return fldL_.nInteriorTraceGroups();
  }
  int nBoundaryTraceGroups() const
  {
    SANS_ASSERT( fldL_.nBoundaryTraceGroups() == fldR_.nBoundaryTraceGroups() );
    return fldL_.nBoundaryTraceGroups();
  }
  int nCellGroups() const
  {
    SANS_ASSERT( fldL_.nCellGroups() == fldR_.nCellGroups() );
    return fldL_.nCellGroups();
  }

  const FieldTraceGroupBase getInteriorTraceGroupBase(const int group) const
  {
    return FieldTraceGroupBase( fldL_.getInteriorTraceGroupBase(group),
                                fldR_.getInteriorTraceGroupBase(group) );
  }

  const FieldTraceGroupBase getBoundaryTraceGroupBase(const int group) const
  {
    return FieldTraceGroupBase( fldL_.getBoundaryTraceGroupBase(group),
                                fldR_.getBoundaryTraceGroupBase(group) );
  }

  const FieldCellGroupBase getCellGroupBase(const int group) const
  {
    return FieldCellGroupBase( fldL_.getCellGroupBase(group),
                               fldR_.getCellGroupBase(group) );
  }

  const FieldTraceGroupBase getInteriorTraceGroupBaseGlobal(const int group) const
  {
    return FieldTraceGroupBase( fldL_.getInteriorTraceGroupBaseGlobal(group),
                                fldR_.getInteriorTraceGroupBaseGlobal(group) );
  }

  const FieldTraceGroupBase getBoundaryTraceGroupBaseGlobal(const int group) const
  {
    return FieldTraceGroupBase( fldL_.getBoundaryTraceGroupBaseGlobal(group),
                                fldR_.getBoundaryTraceGroupBaseGlobal(group) );
  }

  const FieldCellGroupBase getCellGroupBaseGlobal(const int group) const
  {
    return FieldCellGroupBase( fldL_.getCellGroupBaseGlobal(group),
                               fldR_.getCellGroupBaseGlobal(group) );
  }


  template<class Topology>
  const FieldTraceGroupType<Topology> getInteriorTraceGroup(const int group) const
  {
    return FieldTraceGroupType<Topology>( fldL_.template getInteriorTraceGroup<Topology>(group),
                                          fldR_.template getInteriorTraceGroup<Topology>(group) );
  }

  template<class Topology>
  const FieldTraceGroupType<Topology> getBoundaryTraceGroup(const int group) const
  {
    return FieldTraceGroupType<Topology>( fldL_.template getBoundaryTraceGroup<Topology>(group),
                                          fldR_.template getBoundaryTraceGroup<Topology>(group) );
  }

  template<class Topology>
  const FieldCellGroupType<Topology> getCellGroup(const int group) const
  {
    return FieldCellGroupType<Topology>( fldL_.template getCellGroup<Topology>(group),
                                         fldR_.template getCellGroup<Topology>(group) );
  }

  template<class Topology>
  const FieldTraceGroupType<Topology> getInteriorTraceGroupGlobal(const int group) const
  {
    return FieldTraceGroupType<Topology>( fldL_.template getInteriorTraceGroupGlobal<Topology>(group),
                                          fldR_.template getInteriorTraceGroupGlobal<Topology>(group) );
  }

  template<class Topology>
  const FieldTraceGroupType<Topology> getBoundaryTraceGroupGlobal(const int group) const
  {
    return FieldTraceGroupType<Topology>( fldL_.template getBoundaryTraceGroupGlobal<Topology>(group),
                                          fldR_.template getBoundaryTraceGroupGlobal<Topology>(group) );
  }

  template<class Topology>
  const FieldCellGroupType<Topology> getCellGroupGlobal(const int group) const
  {
    return FieldCellGroupType<Topology>( fldL_.template getCellGroupGlobal<Topology>(group),
                                         fldR_.template getCellGroupGlobal<Topology>(group) );
  }

  // Generic accessors for Tuple get function
  const FieldTypeL& left() const { return fldL_; }
  const FieldTypeR& right() const { return fldR_; }

protected:
  FieldTypeL_ref fldL_;
  FieldTypeR_ref fldR_;
};


//----------------------------------------------------------------------------//
// Function for creating tuples of fields (but not tuples of tuples)
//----------------------------------------------------------------------------//

template<class FieldL, class FieldR>
typename std::enable_if< not (std::is_base_of<FieldTupleType, FieldL>::value || std::is_base_of<FieldTupleType, FieldR>::value),
                         FieldTuple< FieldL, FieldR, TupleClass<> > >::type
operator,(const FieldType<FieldL>& fldL, const FieldType<FieldR>& fldR)
{
  return FieldTuple< FieldL, FieldR, TupleClass<> >(fldL.cast(),fldR.cast());
}

//----------------------------------------------------------------------------//
// operator, used to append a field to the tuple. Appending a FieldTuple is treated separately
//----------------------------------------------------------------------------//

template<class LFieldTypeL, class LFieldTypeR, int Level, class R>
typename std::enable_if< not std::is_base_of<FieldTupleType, R>::value,
                         FieldTuple< FieldTuple<LFieldTypeL,LFieldTypeR,TupleClass<Level>>, R, TupleClass<Level> > >::type
operator,(const FieldTuple<LFieldTypeL,LFieldTypeR,TupleClass<Level>>& fldL, const FieldType<R>& fldR)
{
  return FieldTuple< FieldTuple<LFieldTypeL,LFieldTypeR,TupleClass<Level> >, R, TupleClass<Level> >(fldL,fldR.cast());
}

//----------------------------------------------------------------------------//
// appending a tuple to this tuple, i.e ((fld0, fld1), (fld2, fld3))
// this increments the level to indicate a tuple of tuples
//----------------------------------------------------------------------------//

template<class LFieldTypeL, class LFieldTypeR, class RFieldTypeL, class RFieldTypeR, int Level>
FieldTuple< FieldTuple<LFieldTypeL,LFieldTypeR,TupleClass<Level>>,
            FieldTuple<RFieldTypeL,RFieldTypeR,TupleClass<Level>>, TupleClass<Level+1> >
operator,(const FieldTuple<LFieldTypeL,LFieldTypeR,TupleClass<Level>>& fldL, const FieldTuple<RFieldTypeL,RFieldTypeR,TupleClass<Level>>& fldR)
{
  return FieldTuple< FieldTuple<LFieldTypeL,LFieldTypeR,TupleClass<Level>>,
                     FieldTuple<RFieldTypeL,RFieldTypeR,TupleClass<Level>>, TupleClass<Level+1>>(fldL,fldR);
}

//----------------------------------------------------------------------------//
// appending a tuple of a lower level to this tuple, i.e ((fld0, fld1), (fld2, fld3), (fld4, fld5))
// the left tuple level must be greater than the right tuple level
//----------------------------------------------------------------------------//

template<class LFieldTypeL, class LFieldTypeR, int LLevel, class RFieldTypeL, class RFieldTypeR, int RLevel>
typename std::enable_if< (RLevel < LLevel),
                         FieldTuple< FieldTuple<LFieldTypeL,LFieldTypeR,TupleClass<LLevel>>,
                                     FieldTuple<RFieldTypeL,RFieldTypeR,TupleClass<RLevel>>, TupleClass<LLevel> >
                       >::type
operator,(const FieldTuple<LFieldTypeL,LFieldTypeR,TupleClass<LLevel>>& fldL,
          const FieldTuple<RFieldTypeL,RFieldTypeR,TupleClass<RLevel>>& fldR )
{
  return FieldTuple< FieldTuple<LFieldTypeL,LFieldTypeR,TupleClass<LLevel> >,
                     FieldTuple<RFieldTypeL,RFieldTypeR,TupleClass<RLevel>>, TupleClass<LLevel> >(fldL,fldR);
}

//----------------------------------------------------------------------------//
// Function for creating creating tupes of tuples from a field and a tuple
//----------------------------------------------------------------------------//

template<class FieldL, class RFieldTypeL, class RFieldTypeR, int RLevel>
typename std::enable_if< not std::is_base_of<FieldTupleType, FieldL>::value,
                         FieldTuple< FieldL, FieldTuple<RFieldTypeL,RFieldTypeR,TupleClass<RLevel>>, TupleClass<RLevel+1> > >::type
operator,(const FieldType<FieldL>& fldL, const FieldTuple<RFieldTypeL,RFieldTypeR,TupleClass<RLevel>>& fldR)
{
  return FieldTuple< FieldL, FieldTuple<RFieldTypeL,RFieldTypeR,TupleClass<RLevel>>, TupleClass<RLevel+1> >(fldL.cast(),fldR);
}

}

#endif //FIELDTUPLE_H
