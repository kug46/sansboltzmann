// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDASSOCIATIVITYTUPLE_H
#define FIELDASSOCIATIVITYTUPLE_H

#include <type_traits>

#include "tools/SANSException.h"
#include "tools/Tuple.h"

#include "FieldTuple_Traits.h"
#include "ElementTuple.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// A class for recursively storing any number of field associativity variables
//----------------------------------------------------------------------------//
template<class FieldGroupTypeL, class FieldGroupTypeR, class >
class FieldAssociativityTuple;

template<class FieldGroupTypeL_, class FieldGroupTypeR_, int Level>
class FieldAssociativityTuple<FieldGroupTypeL_, FieldGroupTypeR_, TupleClass<Level>> : public FieldAssociativityTupleType
{
public:
  typedef FieldGroupTypeL_ FieldGroupTypeL;
  typedef FieldGroupTypeR_ FieldGroupTypeR;

  //Only add a reference to FieldGroupTypeL if it is not a FieldElementTuple
  typedef typename std::conditional< std::is_base_of<FieldAssociativityTupleType, FieldGroupTypeL>::value,
                                     const FieldGroupTypeL,
                                     typename std::add_lvalue_reference<const FieldGroupTypeL>::type
                                   >::type FieldGroupTypeL_ref;

  typedef typename std::conditional< std::is_base_of<FieldAssociativityTupleType, FieldGroupTypeR>::value,
                                     const FieldGroupTypeR,
                                     typename std::add_lvalue_reference<const FieldGroupTypeR>::type
                                   >::type FieldGroupTypeR_ref;

  static_assert( std::is_same< typename FieldGroupTypeL::TopologyType,
                               typename FieldGroupTypeR::TopologyType >::value,
                               "FieldElemTypeL and FieldElemTypeR must have the same topology" );

  typedef typename FieldGroupTypeL::TopologyType TopologyType;
  typedef typename TopologyType::TopoDim TopoDim;

  template<class T = Real>
  using ElementType = ElementTuple< typename FieldGroupTypeL::template ElementType<T>,
                                    typename FieldGroupTypeR::template ElementType<T>, TupleClass<Level> >;


  FieldAssociativityTuple(const FieldGroupTypeL& fldGroupL, const FieldGroupTypeR& fldGroupR) : fldGroupL_(fldGroupL), fldGroupR_(fldGroupR) {}
  FieldAssociativityTuple(const FieldAssociativityTuple& fldGroups) : fldGroupL_(fldGroups.fldGroupL_), fldGroupR_(fldGroups.fldGroupR_) {}
  FieldAssociativityTuple& operator=(const FieldAssociativityTuple&) = delete;

  template<class ElementTypeL, class ElementTypeR>
  void getElement( ElementTuple< ElementTypeL,
                                 ElementTypeR, TupleClass<Level> >& fldElem, const int elem ) const
  {
    fldGroupL_.getElement( fldElem.elemL(), elem );
    fldGroupR_.getElement( fldElem.elemR(), elem );
  }

  typedef BasisTuple< typename FieldGroupTypeL::BasisType,
                      typename FieldGroupTypeR::BasisType, TupleClass<Level> > BasisType;
  const BasisType basis() const
  {
    return BasisType( fldGroupL_.basis(), fldGroupR_.basis() );
  }

  int nElem() const
  {
    const int nElemL = fldGroupL_.nElem();
    const int nElemR = fldGroupR_.nElem();
    SANS_ASSERT( nElemL == nElemR );
    return nElemL;
  }

  const FieldGroupTypeL& fldL() const { return fldGroupL_; }
  const FieldGroupTypeR& fldR() const { return fldGroupR_; }

  // Generic accessors for Tuple get function
  const FieldGroupTypeL& left() const { return fldGroupL_; }
  const FieldGroupTypeR& right() const { return fldGroupR_; }

protected:
  FieldGroupTypeL_ref fldGroupL_;
  FieldGroupTypeR_ref fldGroupR_;
};

}

#endif //FIELDASSOCIATIVITYTUPLE_H
