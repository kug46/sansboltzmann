// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PARAMTYPE_H
#define PARAMTYPE_H

#include "tools/SANSnumerics.h"
#include "tools/Tuple.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// A class for storing any number of parameter types
//
// template parameters:
//   ParamTypeL, ParamTypeR are simple data types, i.e. Real, VectorS, MatrixS
//   ParamTypeL might be a ParamTyple
//----------------------------------------------------------------------------//
template<class ParamTypeL, class ParamTypeR, class >
class ParamTuple;

template<class ParamTypeL, class ParamTypeR, int Level>
class ParamTuple<ParamTypeL, ParamTypeR, TupleClass<Level> >
{
public:

  ParamTuple() {}
  ParamTuple(const ParamTypeL& paramL, const ParamTypeR& paramR) : paramL_(paramL), paramR_(paramR) {}

  template<class... Args> // cppcheck-suppress noExplicitConstructor
  ParamTuple(const Args&... args)
  {
    set_impl<0>(args...);
  }

  Real operator=(const Real v)
  {
    paramL_ = v;
    paramR_ = v;
    return v;
  }

  ParamTypeL& paramL() { return paramL_; }
  ParamTypeR& paramR() { return paramR_; }

  const ParamTypeL& paramL() const { return paramL_; }
  const ParamTypeR& paramR() const { return paramR_; }

  //Generic interfaces
  ParamTypeL& left() { return paramL_; }
  ParamTypeR& right() { return paramR_; }

  const ParamTypeL& left() const { return paramL_; }
  const ParamTypeR& right() const { return paramR_; }

  template<class... Args>
  void set(const Args&... args)
  {
    set_impl<0>(args...);
  }

protected:
  ParamTypeL paramL_;
  ParamTypeR paramR_;

  template<int k, class T, class... Args>
  void set_impl(const T& L, const Args&... args)
  {
    SANS::set<k>(*this) = L;
    set_impl<k+1>(args...);
  }

  template<int k, class T>
  void set_impl(const T& R)
  {
    SANS::set<k>(*this) = R;
  }
};

// forward declare
template<class... Args>
struct promote_Surreal;

// Extract the types from the param tuple to get the surreal
template<class ParamTypeL, class ParamTypeR, int Level, class T2>
struct promote_Surreal< ParamTuple<ParamTypeL, ParamTypeR, TupleClass<Level> >, T2>
{
  typedef typename promote_Surreal< ParamTypeL, T2 >::type typeL;
  typedef typename promote_Surreal< ParamTypeR, T2 >::type typeR;

  typedef typename promote_Surreal< typeL, typeR >::type type;
};

}

#endif //DOFSTYPE_H
