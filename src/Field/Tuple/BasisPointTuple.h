// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BASISPOINTTUPLE_H
#define BASISPOINTTUPLE_H

#include <type_traits>

#include "tools/SANSnumerics.h"
#include "tools/Tuple.h"

#include "BasisFunction/BasisPoint.h"

#include "FieldTuple_Traits.h"

namespace SANS
{

//Forward declare
template<class BasisTypeL, class BasisTypeR, class >
class BasisTuple;

template<class BasisTypeL, class BasisTypeR, class >
class BasisPointTuple;


//A helper class to extract the class needed to represent a basis function evaluated at a point
template<class BasisType>
struct getBasisPointType
{
  typedef BasisPoint<BasisType::D> BasisPointType;
};

template<class BasisTypeL, class BasisTypeR, int Level>
struct getBasisPointType< BasisTuple<BasisTypeL, BasisTypeR, TupleClass<Level> > >
{
  typedef BasisPointTuple<BasisTypeL, BasisTypeR, TupleClass<Level> > BasisPointType;
};

//----------------------------------------------------------------------------//
// A class for storing muliple basis functions evaluated at point to be used with evalBasis and evalFromBasis
//
// template parameters:
//   BasisTypeL, BasisTypeR   Basis types, where BasisTypeL might be a BasisPointTuple
//----------------------------------------------------------------------------//
template<class BasisTypeL, class BasisTypeR, class >
class BasisPointTuple;

template<class BasisTypeL, class BasisTypeR, int Level>
class BasisPointTuple<BasisTypeL, BasisTypeR, TupleClass<Level> >
{
public:
  typedef typename getBasisPointType<BasisTypeL>::BasisPointType BasisPointTypeL;
  typedef BasisPoint<BasisTypeR::D> BasisPointTypeR;

  explicit BasisPointTuple( const BasisTuple<BasisTypeL, BasisTypeR, TupleClass<Level> >& basis ) : phiL_(basis.basisL()), phiR_(basis.basisR()) {}
  BasisPointTuple& operator=(const BasisPointTuple&) = delete;

  BasisPointTypeL& phiL() { return phiL_; }
  BasisPointTypeR& phiR() { return phiR_; }

  const BasisPointTypeL& phiL() const { return phiL_; }
  const BasisPointTypeR& phiR() const { return phiR_; }

  // This is a bit of a hack... but the BasisFunction evalBasis and evalFromBasis methods require a size argument...
  int size() const { return -1; }

protected:
  BasisPointTypeL phiL_;
  BasisPointTypeR phiR_;
};

}

#endif //BASISPOINTTUPLE_H
