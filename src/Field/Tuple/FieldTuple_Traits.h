// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDTUPLE_TRAITS_H
#define FIELDTUPLE_TRAITS_H

namespace SANS
{

//A base class that can be used to identify FieldTuple classes
class FieldTupleType {};

//A base class that can be used to identify FieldAssociativityBaseTuple classes
class FieldAssociativityBaseTupleType {};

//A base class that can be used to identify FieldAssociativityTuple classes
class FieldAssociativityTupleType {};

//A base class that can be used to identify BasisTuple classes
class BasisTupleType {};

}

#endif //FIELDTUPLE_TRAITS_H
