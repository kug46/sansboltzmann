// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BASISTUPLE_H
#define BASISTUPLE_H

#include <type_traits>

#include "tools/Tuple.h"

#include "FieldTuple_Traits.h"
#include "BasisPointTuple.h"
#include "BasisPointDerivativeTuple.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// A class for storing muliple basis functions used for the constructor of ElementTuple
//
// template parameters:
//   BasisTypeL, BasisTypeR   Basis types, where BasisTypeL might be a BasisTuple
//----------------------------------------------------------------------------//
template<class BasisTypeL, class BasisTypeR, class >
class BasisTuple;

template<class BasisTypeL, class BasisTypeR, int Level>
class BasisTuple<BasisTypeL, BasisTypeR, TupleClass<Level> > : public BasisTupleType
{
public:
  // Data types to represent basis functions evaluated at a point
  typedef BasisPointTuple<BasisTypeL, BasisTypeR, TupleClass<Level> > BasisPointType;
  typedef BasisPointDerivativeTuple<BasisTypeL, BasisTypeR, TupleClass<Level> > BasisPointDerivativeType;

  //Make the const pointers
  typedef const BasisTypeL* BasisTypeL_ptr;
  typedef const BasisTypeR* BasisTypeR_ptr;

  //Use BasisTypeL if BasisTypeL not a BasisTupleType, otherwise use BasisTypeL_ptr
  typedef typename std::conditional< std::is_base_of<BasisTupleType, BasisTypeL>::value,
                                     typename std::add_const< BasisTypeL >::type,
                                     BasisTypeL_ptr
                                   >::type BasisTypeL_const_ptr;

  typedef typename std::conditional< std::is_base_of<BasisTupleType, BasisTypeR>::value,
                                     typename std::add_const< BasisTypeR >::type,
                                     BasisTypeR_ptr
                                   >::type BasisTypeR_const_ptr;


  //Only add a reference to BasisTypeL if it is not a BasisTupleType
  typedef typename std::conditional< std::is_base_of<BasisTupleType, BasisTypeL>::value,
                                     typename std::add_lvalue_reference<const BasisTypeL>::type,
                                     BasisTypeL_ptr
                                   >::type BasisTypeL_ref;

  typedef typename std::conditional< std::is_base_of<BasisTupleType, BasisTypeR>::value,
                                     typename std::add_lvalue_reference<const BasisTypeR>::type,
                                     BasisTypeR_ptr
                                   >::type BasisTypeR_ref;


  BasisTuple( const BasisTuple& fldBasis) : basisL_(fldBasis.basisL_), basisR_(fldBasis.basisR_) {}
  BasisTuple( BasisTypeL_ref basisL, const BasisTypeR_ref basisR ) : basisL_(basisL), basisR_(basisR) {}
  BasisTuple& operator=(const BasisTuple&) = delete;

  BasisTypeL_ref basisL() const { return basisL_; }
  BasisTypeR_ref basisR() const { return basisR_; }

protected:
  BasisTypeL_const_ptr basisL_;
  BasisTypeR_const_ptr basisR_;
};

}

#endif //BASISTUPLE_H
