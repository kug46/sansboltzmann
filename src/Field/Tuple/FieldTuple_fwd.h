// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FUELDTUPLE_FWD_H
#define FUELDTUPLE_FWD_H

namespace SANS
{
// Forward declare typle classes

template<class FieldTypeL, class FieldTypeR, class >
class FieldTuple;

template<class FieldGroupTypeL, class FieldGroupTypeR, class >
class FieldAssociativityTuple;

template<class ElementTypeL, class ElementTypeR, class >
class ElementTuple;

template<class ElementTypeL, class ElementTypeR, class >
class ElementArray;

template<class DofTypeL, class DofTypeR, class >
class ParamTuple;

template<class BasisTypeL, class BasisTypeR, class >
class BasisTuple;

}

#endif //FUELDTUPLE_FWD_H
