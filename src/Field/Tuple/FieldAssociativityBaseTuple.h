// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDASSOCIATIVITYBASETUPLE_H
#define FIELDASSOCIATIVITYBASETUPLE_H

#include <type_traits>
#include <typeinfo>

#include "tools/SANSException.h"
#include "tools/Tuple.h"

#include "FieldTuple_Traits.h"
#include "ElementTuple.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// A class for recursively storing any number of field associativity variables
//----------------------------------------------------------------------------//
template<class FieldGroupBaseTypeL, class FieldGroupBaseTypeR, class >
class FieldAssociativityBaseTuple;

template<class FieldGroupBaseTypeL, class FieldGroupBaseTypeR, int Level>
class FieldAssociativityBaseTuple<FieldGroupBaseTypeL, FieldGroupBaseTypeR, TupleClass<Level>> : public FieldAssociativityBaseTupleType
{
public:

  //Only add a reference to FieldGroupBaseTypeL if it is not a FieldAssociativityBaseTuple
  typedef typename std::conditional< std::is_base_of<FieldAssociativityBaseTupleType, FieldGroupBaseTypeL>::value,
                                     const FieldGroupBaseTypeL,
                                     typename std::add_lvalue_reference<const FieldGroupBaseTypeL>::type
                                   >::type FieldGroupBaseTypeL_ref;

  typedef typename std::conditional< std::is_base_of<FieldAssociativityBaseTupleType, FieldGroupBaseTypeR>::value,
                                     const FieldGroupBaseTypeR,
                                     typename std::add_lvalue_reference<const FieldGroupBaseTypeR>::type
                                   >::type FieldGroupBaseTypeR_ref;

  FieldAssociativityBaseTuple(const FieldGroupBaseTypeL& fldGroupL, const FieldGroupBaseTypeR& fldGroupR)
      : fldGroupBaseL_(fldGroupL), fldGroupBaseR_(fldGroupR) {}
  FieldAssociativityBaseTuple(const FieldAssociativityBaseTuple& fldGroups)
      : fldGroupBaseL_(fldGroups.fldGroupBaseL_), fldGroupBaseR_(fldGroups.fldGroupBaseR_) {}
  FieldAssociativityBaseTuple& operator=(const FieldAssociativityBaseTuple&) = delete;


  const std::type_info& topoTypeID() const
  {
    const std::type_info& IDL = fldGroupBaseL_.topoTypeID();
    const std::type_info& IDR = fldGroupBaseR_.topoTypeID();
    SANS_ASSERT( IDL == IDR );
    return IDL;
  }

  int nElem() const
  {
    const int nElemL = fldGroupBaseL_.nElem();
    const int nElemR = fldGroupBaseR_.nElem();
    SANS_ASSERT( nElemL == nElemR );
    return nElemL;
  }

#if 0 // Mixed Basis functions between fields gives different DOF's, so a single DOF function is not sensical. It would need to be a tuple
  int nDOF() const
  {
    const int nDOFL = fldGroupBaseL_.nDOF();
    const int nDOFR = fldGroupBaseR_.nDOF();
    SANS_ASSERT_MSG( nDOFL == nDOFR, "nDOFL = %d, nDOFR = %d", nDOFL, nDOFR );
    return nDOFL;
  }
#endif

  const FieldGroupBaseTypeL& fldGroupBaseL() const { return fldGroupBaseL_; }
  const FieldGroupBaseTypeR& fldGroupBaseR() const { return fldGroupBaseR_; }

  // Generic accessors for Tuple get function
  const FieldGroupBaseTypeL& left() const { return fldGroupBaseL_; }
  const FieldGroupBaseTypeR& right() const { return fldGroupBaseR_; }

protected:
  FieldGroupBaseTypeL_ref fldGroupBaseL_;
  FieldGroupBaseTypeR_ref fldGroupBaseR_;
};

}

#endif //FIELDASSOCIATIVITYBASETUPLE_H
