// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BASISPOINTDERIVATIVETUPLE_H
#define BASISPOINTDERIVATIVETUPLE_H

#include <type_traits>

#include "tools/SANSnumerics.h"
#include "tools/Tuple.h"

#include "BasisFunction/BasisPointDerivative.h"

#include "FieldTuple_Traits.h"

namespace SANS
{

//Forward declare
template<class BasisTypeL, class BasisTypeR, class >
class BasisTuple;

template<class BasisTypeL, class BasisTypeR, class >
class BasisPointDerivativeTuple;


//A helper class to extract the class needed to represent the derivative of a basis function evaluated at a point
template<class BasisType>
struct getBasisPointDerivativeType
{
  typedef BasisPointDerivative<BasisType::D> BasisPointDerivativeType;
};

template<class BasisTypeL, class BasisTypeR, int Level>
struct getBasisPointDerivativeType< BasisTuple<BasisTypeL, BasisTypeR, TupleClass<Level> > >
{
  typedef BasisPointDerivativeTuple<BasisTypeL, BasisTypeR, TupleClass<Level> > BasisPointDerivativeType;
};

//----------------------------------------------------------------------------//
// A class for storing muliple derivatives of basis functions evaluated at point to be used with evalBasisDerivative and evalFromBasis
//
// template parameters:
//   BasisTypeL, BasisTypeR   Basis types, where BasisTypeL might be a BasisPointDerivativeTuple
//----------------------------------------------------------------------------//

template<class BasisTypeL, class BasisTypeR, int Level>
class BasisPointDerivativeTuple<BasisTypeL, BasisTypeR, TupleClass<Level> >
{
public:
  typedef typename getBasisPointDerivativeType<BasisTypeL>::BasisPointDerivativeType BasisPointDerivativeTypeL;
  typedef BasisPointDerivative<BasisTypeR::D> BasisPointDerivativeTypeR;

  explicit BasisPointDerivativeTuple( const BasisTuple<BasisTypeL, BasisTypeR, TupleClass<Level> >& basis )
    : dphiL_(basis.basisL()), dphiR_(basis.basisR()) {}
  BasisPointDerivativeTuple& operator=(const BasisPointDerivativeTuple&) = delete;

  BasisPointDerivativeTypeL& dphiL() { return dphiL_; }
  BasisPointDerivativeTypeR& dphiR() { return dphiR_; }

  const BasisPointDerivativeTypeL& dphiL() const { return dphiL_; }
  const BasisPointDerivativeTypeR& dphiR() const { return dphiR_; }

  // This is a bit of a hack... but the BasisFunction evalBasis and evalFromBasis methods require a size argument...
  int size() const { return -1; }

protected:
  BasisPointDerivativeTypeL dphiL_;
  BasisPointDerivativeTypeR dphiR_;
};

}

#endif //BASISPOINTDERIVATIVETUPLE_H
