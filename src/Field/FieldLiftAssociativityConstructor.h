// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDTRACEASSOCIATIVITYCONSTRUCTOR_H
#define FIELDTRACEASSOCIATIVITYCONSTRUCTOR_H


#include <iostream>
#include <string>
#include <memory>
#include <typeinfo>     // typeid

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Constructor class for FieldLiftAssociativity
//
// An instances of FieldLiftAssociativityConstructor must be constructed and
// populated in order to construct or resize a FieldLiftAssociativity
//
//----------------------------------------------------------------------------//
class FieldLiftAssociativityConstructorBase
{
public:
  virtual ~FieldLiftAssociativityConstructorBase() {}
};

template<class ElementAssociativityConstructor>
class FieldLiftAssociativityConstructor : public FieldLiftAssociativityConstructorBase
{
public:
  static const int NTrace = ElementAssociativityConstructor::TopologyType::NTrace;
  typedef typename ElementAssociativityConstructor::BasisType BasisType;
  typedef ElementAssociativityConstructor ElementAssociativityConstructorType;

  // needed for new []
  FieldLiftAssociativityConstructor() : nElem_(0), basis_(NULL), assoc_(NULL), isSetassoc_(NULL) {}

  // Constructor for the line associativity
  FieldLiftAssociativityConstructor( const BasisType* basis, const int nelem ) : FieldLiftAssociativityConstructor() { resize(basis, nelem); }
  virtual ~FieldLiftAssociativityConstructor();

  int nElem() const { return nElem_; }

  // basis function
  const BasisType* basis() const { return basis_; }

  //Used to resize the in case an array is allocated
  void resize( const BasisType* basis, const int nelem );

  //No copy constructors
  FieldLiftAssociativityConstructor( const FieldLiftAssociativityConstructor& ) = delete;
  FieldLiftAssociativityConstructor& operator=( const FieldLiftAssociativityConstructor& ) = delete;

  void setAssociativity( const ElementAssociativityConstructor& elemassoc, const int elem, const int canonicalTrace );
  ElementAssociativityConstructor& setAssociativity( const int elem, const int canonicalTrace );
  const ElementAssociativityConstructor& getAssociativity( const int elem, const int canonicalTrace ) const
  {
    SANS_ASSERT_MSG( isSetassoc_[elem*NTrace + canonicalTrace], "elem = %d cononicalTrace = %d", elem, canonicalTrace );
    return assoc_[elem*NTrace + canonicalTrace];
  }

protected:
  int nElem_;                         // number of elements

  const BasisType* basis_;

  ElementAssociativityConstructor* assoc_;       // element DOF associations (local to global)
  bool* isSetassoc_;                  // flag: is DOF associations set for each element?
};


template<class ElementAssociativityConstructor>
void
FieldLiftAssociativityConstructor<ElementAssociativityConstructor>::resize( const BasisType* basis, const int nelem )
{
  SANS_ASSERT( nelem >= 0 );

  nElem_ = nelem;

  basis_ = basis;

  delete [] assoc_; assoc_ = NULL;
  delete [] isSetassoc_; isSetassoc_ = NULL;

  // element DOF associativity
  assoc_ = new ElementAssociativityConstructor[nelem*NTrace];
  isSetassoc_ = new bool[nelem*NTrace];

  for (int n = 0; n < nelem*NTrace; n++)
  {
    assoc_[n].resize(basis);
    isSetassoc_[n] = false;
  }
}

template<class ElementAssociativityConstructor>
FieldLiftAssociativityConstructor<ElementAssociativityConstructor>::~FieldLiftAssociativityConstructor()
{
  // edge-element DOF associativity
  delete [] assoc_;
  delete [] isSetassoc_;
}


template<class ElementAssociativityConstructor>
void
FieldLiftAssociativityConstructor<ElementAssociativityConstructor>::
setAssociativity( const ElementAssociativityConstructor& elemassoc, const int elem, const int canonicalTrace )
{
  SANS_ASSERT_MSG( elem >= 0 && elem < nElem_ && canonicalTrace >= 0 && canonicalTrace < NTrace && !isSetassoc_[elem*NTrace + canonicalTrace],
                   "with elem = %d, nElem_ = %d, canonicalTrace = %d, NTrace = %d", elem, nElem_, canonicalTrace, NTrace );
  assoc_[elem*NTrace + canonicalTrace] = elemassoc;
  isSetassoc_[elem*NTrace + canonicalTrace] = true;
}

template<class ElementAssociativityConstructor>
ElementAssociativityConstructor&
FieldLiftAssociativityConstructor<ElementAssociativityConstructor>::setAssociativity( const int elem, const int canonicalTrace )
{
  SANS_ASSERT_MSG( elem >= 0 && elem < nElem_ && canonicalTrace >= 0 && canonicalTrace < NTrace,
                   "with elem = %d, nElem_ = %d, canonicalTrace = %d, NTrace = %d", elem, nElem_, canonicalTrace, NTrace );
  isSetassoc_[elem*NTrace + canonicalTrace] = true;
  return assoc_[elem*NTrace + canonicalTrace];
}

}

#endif //FIELDTRACEASSOCIATIVITYCONSTRUCTOR_H
