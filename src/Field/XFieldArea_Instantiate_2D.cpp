// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define FIELDBASE_INSTANTIATE
#include "FieldBase_impl.h"

#define XFIELD_INSTANTIATE
#include "XField_impl.h"

#define XFIELDAREA_INSTANTIATE
#include "XFieldArea.h"
#include "XFieldArea_impl.h"
#include "XFieldArea_buildFrom_impl.h"
#include "XFieldArea_checkGrid_impl.h"
#include "XField_checkTraceNormal_impl.h"

namespace SANS
{

//=============================================================================
template class FieldBase< XFieldTraits<PhysD2, TopoD2> >;

//=============================================================================
//Explicitly instantiate
template class XField< PhysD2, TopoD2 >;

}
