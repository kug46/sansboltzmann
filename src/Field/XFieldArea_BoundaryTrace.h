// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELDAREA_BOUNDARYTRACE_H
#define XFIELDAREA_BOUNDARYTRACE_H

#include "XField_BoundaryTrace.h"
#include "XFieldArea.h"
#include "FieldAssociativity.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Represents boundary trace groups from a topologically 2D grid
//----------------------------------------------------------------------------//

template<class PhysDim>
struct XFieldTraits_BoundaryTrace<PhysDim, TopoD2>
{
  typedef TopoD2 TopoDim;
  static const int D = PhysDim::D;                 // physical dimensions

  typedef DLA::VectorS<D,Real> VectorX;   // coordinates vector
  typedef VectorX T;

  typedef typename XFieldGroupLineTraits<PhysDim, TopoD2>::FieldBase FieldTraceGroupBase;
  typedef typename XFieldAreaTraits<PhysDim, TopoD2>::FieldBase FieldCellGroupBase;

  template<class Topology>
  using FieldTraceGroupType = FieldAssociativity< XFieldGroupLineTraits<PhysDim, TopoD2> >;

  template<class Topology>
  using FieldCellGroupType = FieldAssociativity< XFieldGroupAreaTraits<PhysDim, TopoD2, Topology> >;
};

} //namespace SANS

#endif //XFIELDAREA_BOUNDARYTRACE_H
