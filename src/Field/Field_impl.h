// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(FIELD_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "Field.h"

#include "tools/for_each_CellGroup.h"
#include "tools/for_each_HubTraceGroup.h"
#include "tools/for_each_InteriorFieldTraceGroup.h"
#include "tools/for_each_BoundaryTraceGroup.h"
#include "ProjectSoln/ProjectConstCell.h"
#include "ProjectSoln/ProjectConstTrace.h"

#ifdef SANS_MPI
#include <boost/serialization/vector.hpp>
#include <boost/mpi/collectives/all_to_all.hpp>
#include <boost/mpi/collectives/all_gather.hpp>
#include <boost/mpi/collectives/all_reduce.hpp>
#include "MPI/serialize_DenseLinAlg_MatrixS.h"
#endif

#include <numeric> //std::accumulate

namespace SANS
{

template<class PhysDim, class TopoDim, class T>
Field<PhysDim, TopoDim, T>::Field( const XField<PhysDim, TopoDim>& xfld ) :
  BaseType(xfld), nDOFpossessed_(0), nDOFghost_(0), DOFghost_rank_(nullptr) {}

template<class PhysDim, class TopoDim, class T>
Field<PhysDim, TopoDim, T>::Field( const Field& fld, const FieldCopy& tag ) :
  BaseType(fld, tag), nDOFpossessed_(fld.nDOFpossessed_), nDOFghost_(fld.nDOFghost_), DOFghost_rank_(nullptr)
{
  resizeDOF_rank(fld.nDOF_, fld.nDOFpossessed_);

  for (int i = 0; i < nDOF_ - nDOFpossessed_; i++)
    DOFghost_rank_[i] = fld.DOFghost_rank_[i];

  sendDOF_ = fld.sendDOF_;
}

template<class PhysDim, class TopoDim, class T>
Field<PhysDim, TopoDim, T>::~Field()
{
  delete [] DOFghost_rank_;
}

//----------------------------------------------------------------------------//
// total number of DOF in a serial representation of the XField
template<class PhysDim, class TopoDim, class T>
int Field<PhysDim, TopoDim, T>::nDOFnative() const
{
#ifdef SANS_MPI
  return boost::mpi::all_reduce(*this->comm(), nDOFpossessed_, std::plus<int>());
#else
  return nDOFpossessed_;
#endif
}

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim, class T>
void Field<PhysDim, TopoDim, T>::deallocate()
{
  BaseType::deallocate();

  delete [] DOFghost_rank_; DOFghost_rank_ = NULL;

  nDOFpossessed_ = 0;
  nDOFghost_ = 0;
}

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim, class T>
void Field<PhysDim, TopoDim, T>::resizeDOF( const int nDOF )
{
  BaseType::resizeDOF( nDOF );
  nDOFpossessed_ = nDOF;
}

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim, class T>
void Field<PhysDim, TopoDim, T>::cloneFrom( const Field& fld )
{
  BaseType::cloneFrom(fld);

  nDOFpossessed_ = fld.nDOFpossessed_;
  nDOFghost_     = fld.nDOFghost_;

  resizeDOF_rank(fld.nDOF_, fld.nDOFpossessed_);

  for (int i = 0; i < nDOF_ - nDOFpossessed_; i++)
    DOFghost_rank_[i] = fld.DOFghost_rank_[i];

  sendDOF_ = fld.sendDOF_;
}

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim, class T>
Field<PhysDim, TopoDim, T>&
Field<PhysDim, TopoDim, T>::operator=( const T& q )
{
  // Set the field to a constant value
  for_each_HubTraceGroup<TopoDim>::apply( ProjectConstTrace<PhysDim>(q, globalHubTraceGroups_), *this );

  for_each_InteriorFieldTraceGroup<TopoDim>::apply( ProjectConstTrace<PhysDim>(q, globalInteriorTraceGroups_), *this );

  for_each_BoundaryTraceGroup<TopoDim>::apply( ProjectConstTrace<PhysDim>(q, globalBoundaryTraceGroups_), *this );

  for_each_CellGroup<TopoDim>::apply( ProjectConstCell<PhysDim>(q, globalCellGroups_), *this );

  return *this;
}

//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
void
Field<PhysDim, TopoDim, T>::
resizeDOF_rank( const int nDOF, const int nDOFpossessed )
{
  delete [] DOFghost_rank_; DOFghost_rank_ = nullptr;

  SANS_ASSERT( nDOF == nDOF_ );
  SANS_ASSERT( nDOFpossessed == nDOFpossessed_ );
  SANS_ASSERT( nDOF - nDOFpossessed >= 0 );

  DOFghost_rank_ = new int[nDOF - nDOFpossessed];

  // initialize to an invalid rank
  for (int i = 0; i < nDOF - nDOFpossessed; i++)
    DOFghost_rank_[i] = -1;
}
//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
void
Field<PhysDim, TopoDim, T>::
syncDOFs_MPI_noCache()
{
  if (!sendDOF_.empty())
  {
    // use the cache if it exists
    syncDOFs_MPI_Cached();
    return;
  }

  std::vector<std::vector<int>> sendDOF;
  setupDOFsync(sendDOF);
  syncDOFs(sendDOF);
}

//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
void
Field<PhysDim, TopoDim, T>::
syncDOFs_MPI_Cached()
{
  // create the cache if it is empty
  if (sendDOF_.empty())
    setupDOFsync(sendDOF_);

  syncDOFs(sendDOF_);
}
//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
void
Field<PhysDim, TopoDim, T>::
setupDOFsync(std::vector<std::vector<int>>& sendDOF)
{
#ifdef SANS_MPI
  std::map<int,int> native2localDOFmap;
  std::vector<std::vector<int>> needDOF(this->xfld_.comm()->size());

  // save off the mapping to get the local DOF based on native DOF number
  for (int idof = 0; idof < nDOFpossessed_; idof++)
    native2localDOFmap[local2nativeDOFmap_[idof]] = idof;

  // add to the required DOFs from other ranks
  for (int idof = 0; idof < nDOF_ - nDOFpossessed_; idof++)
    needDOF[DOFghost_rank_[idof]].push_back(local2nativeDOFmap_[idof + nDOFpossessed_]);

  for ( std::size_t rank = 0; rank < needDOF.size(); rank++ )
    needDOF[rank].shrink_to_fit();

  // send the requested native DOF indexing to the other ranks
  boost::mpi::all_to_all(*this->xfld_.comm(), needDOF, sendDOF);
  needDOF.clear();

  // translate the native DOF index to a local index
  for ( std::size_t rank = 0; rank < sendDOF.size(); rank++ )
  {
    std::vector<int>& sendDOF_rank = sendDOF[rank];

    // Map which local DOF's need to be send to the respective ranks
    for ( std::size_t i = 0; i < sendDOF_rank.size(); i++ )
      sendDOF_rank[i] = native2localDOFmap.at(sendDOF_rank[i]);
  }
#endif
}

//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
void
Field<PhysDim, TopoDim, T>::
syncDOFs(const std::vector<std::vector<int>>& sendDOF)
{
#ifdef SANS_MPI
  std::vector<std::vector<T>> sendDOFvals(sendDOF.size());
  std::vector<std::vector<T>> recvDOFvals(sendDOF.size());

  // translate the native DOF index to a local index
  for ( std::size_t rank = 0; rank < sendDOF.size(); rank++ )
  {
    const std::vector<int>& sendDOF_rank = sendDOF[rank];
    sendDOFvals[rank].resize(sendDOF_rank.size());

    // copy values to be sent
    for ( std::size_t i = 0; i < sendDOF_rank.size(); i++ )
      sendDOFvals[rank][i] = DOF_[sendDOF_rank[i]];
  }

  // send the DOF values to the other ranks
  boost::mpi::all_to_all(*this->xfld_.comm(), sendDOFvals, recvDOFvals);
  sendDOFvals.clear();

  std::vector<int> iRemote(recvDOFvals.size(), 0);
  for (int idof = 0; idof < nDOF_ - nDOFpossessed_; idof++)
  {
    int rank = DOFghost_rank_[idof];

    // assign DOFs from other ranks
    DOF_[idof + nDOFpossessed_] = recvDOFvals[rank][iRemote[rank]++];
  }
#endif
}

//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
GlobalContinuousMap
Field<PhysDim, TopoDim, T>::
continuousGlobalMap(const int nDOFpre, const int nDOFpost) const
{
  // The total number of possessed DOFs on the processor (including from other fields) should be
  // nDOFpre + nDOFpossessed_ + nDOFpost
  // nDOFpre are DOF's preceding this field, and nDOFpost those afte this field

  GlobalContinuousMap continuousMap(this->xfld_.comm());

  // save off how many DOFs are possessed by this rank
  continuousMap.nDOFpossessed = nDOFpossessed_;
  continuousMap.nDOFghost = nDOFghost_;

#ifdef SANS_MPI
  std::vector<int> nDOFOnRank;
  boost::mpi::all_gather(*this->xfld_.comm(), nDOFpre + nDOFpossessed_ + nDOFpost, nDOFOnRank);

  int comm_rank = this->xfld_.comm()->rank();

  // compute the DOF offset from lower ranks
  continuousMap.nDOF_rank_offset = std::accumulate(nDOFOnRank.begin(), nDOFOnRank.begin()+comm_rank, 0);

  // size the ghost indexing (do not include zombies)
  continuousMap.remoteGhostIndex.resize(nDOFghost_);

  std::map<int,int> native2continuousDOFmap;
  std::vector<std::vector<int>> needDOF(this->xfld_.comm()->size());
  std::vector<std::vector<int>> sendDOF(this->xfld_.comm()->size());
  std::vector<std::vector<int>> ghostDOF(this->xfld_.comm()->size());

  // save off the mapping to get the continuous DOF based on native DOF number
  for (int idof = 0; idof < nDOFpossessed_; idof++)
    native2continuousDOFmap[local2nativeDOFmap_[idof]] = continuousMap.nDOF_rank_offset + nDOFpre + idof;

  // add to the required DOFs from other ranks
  // use the native indexing as a unique identifier
  for (int idof = 0; idof < nDOFghost_; idof++)
    needDOF[DOFghost_rank_[idof]].push_back(local2nativeDOFmap_[idof + nDOFpossessed_]);

  for ( std::size_t rank = 0; rank < needDOF.size(); rank++ )
    needDOF[rank].shrink_to_fit();

  // send the requested native DOF indexing to the other ranks
  boost::mpi::all_to_all(*this->xfld_.comm(), needDOF, sendDOF);
  needDOF.clear();

  // translate the native DOF index to a local index
  for ( std::size_t rank = 0; rank < sendDOF.size(); rank++ )
  {
    // Map which local DOF's need to be send to the respective ranks with a continuous index value
    for ( std::size_t i = 0; i < sendDOF[rank].size(); i++ )
      sendDOF[rank][i] = native2continuousDOFmap.at(sendDOF[rank][i]);
  }

  // send the requested native DOF indexing to the other ranks
  boost::mpi::all_to_all(*this->xfld_.comm(), sendDOF, ghostDOF);
  sendDOF.clear();

  // copy over the remote index
  std::vector<int> ighost(ghostDOF.size(), 0);
  for (int idof = 0; idof < nDOFghost_; idof++)
  {
    int rank = DOFghost_rank_[idof];

    continuousMap.remoteGhostIndex[idof] = ghostDOF[rank][ighost[rank]++];
  }
#else
  continuousMap.nDOF_rank_offset = 0;
#endif

  return continuousMap;
}

//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
int
Field<PhysDim, TopoDim, T>::nDOFCellGroup(int cellgroup) const
{
  SANS_DEVELOPER_EXCEPTION("Field<PhysDim, TopoDim, T>::nDOFCellGroup - Function not implemented in derived class.");
  return -1;
}

template <class PhysDim, class TopoDim, class T>
int
Field<PhysDim, TopoDim, T>::nDOFInteriorTraceGroup(int tracegroup) const
{
  SANS_DEVELOPER_EXCEPTION("Field<PhysDim, TopoDim, T>::nDOFInteriorTraceGroup - Function not implemented in derived class.");
  return -1;
}

template <class PhysDim, class TopoDim, class T>
int
Field<PhysDim, TopoDim, T>::nDOFBoundaryTraceGroup(int tracegroup) const
{
  SANS_DEVELOPER_EXCEPTION("Field<PhysDim, TopoDim, T>::nDOFBoundaryTraceGroup - Function not implemented in derived class.");
  return -1;
}

}
