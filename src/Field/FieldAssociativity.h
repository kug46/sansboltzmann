// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDASSOCIATIVITY_H
#define FIELDASSOCIATIVITY_H

#include <iostream>
#include <string>
#include <typeinfo>     // type_info
#include <type_traits> // std::conditional

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "BasisFunction/BasisFunctionCategory.h"

#include "Field/Element/Element.h"

#include "FieldAssociativityConstructor.h"


namespace SANS
{
//----------------------------------------------------------------------------//
// Field of elements associativities
//
// implemented as abstract base class with templated derived classes
//
// template parameters:
//   FieldTraits    Traits to specialize the field
//
//----------------------------------------------------------------------------//

struct NullBase;

template<class T, class CloneBase = NullBase >
class FieldAssociativityBase
{
public:

  virtual ~FieldAssociativityBase() {}

  //Choose the appropriate return type from the clone function
  typedef typename std::conditional< std::is_same< CloneBase, NullBase >::value,
                                     FieldAssociativityBase,
                                     CloneBase
                                   >::type CloneType;

  virtual CloneType* clone() const = 0;

  int nElem() const { return nElem_; }
  int nDOF()  const { return nDOF_; }

  virtual const std::type_info& topoTypeID() const = 0;
  virtual int nElemTrace() const = 0; // Number of trace elements of a cell element
  virtual int nBasis() const = 0;
  virtual int order() const = 0;
  virtual BasisFunctionCategory basisCategory() const = 0;

  // DOF accessors
        T& DOF( int n )       { return DOF_[n]; }
  const T& DOF( int n ) const { return DOF_[n]; }

  // set DOFs shared amongst the groups
  void setDOF( T* DOF, const int nDOF )
  {
    DOF_  = DOF;
    nDOF_ = nDOF;
  }

  virtual void dump( int indentSize=0, std::ostream& out = std::cout ) const = 0;

protected:
  int nElem_;                         // number of elements in this group
  int nDOF_;                          // total DOFs in the field (not local to this group)

  T* DOF_;                            // DOFs in the field

  FieldAssociativityBase() : nElem_(0), nDOF_(0), DOF_(nullptr) {}     // abstract base class
};


template <class FieldTraits >
class FieldAssociativity;

//This is needed so the correct clone operator is defined for different FieldBase classes
template<class FieldTraits, class FieldBase>
struct FieldAssociativityClone : public FieldTraits::FieldBase
{
  virtual FieldBase* clone() const override { return NULL; }
  virtual ~FieldAssociativityClone() {}
};

//Specialization for FieldAssociativityBase<T> as the base class
template<class FieldTraits, class T>
struct FieldAssociativityClone<FieldTraits, FieldAssociativityBase<T> > : public FieldTraits::FieldBase
{
  typedef typename FieldTraits::FieldBase FieldBase;
  virtual FieldBase* clone() const override
  {
    return new FieldAssociativity<FieldTraits>(*reinterpret_cast<const FieldAssociativity<FieldTraits>*>(this));
  }
  virtual ~FieldAssociativityClone() {}
};


template <class FieldTraits>
class FieldAssociativity : public FieldAssociativityClone<FieldTraits, typename FieldTraits::FieldBase >
{
public:
  typedef FieldAssociativityClone<FieldTraits, typename FieldTraits::FieldBase > BaseType;
  typedef typename FieldTraits::T T;              // DOF type
  typedef typename FieldTraits::FieldBase FieldBase;
  typedef typename FieldTraits::BasisType BasisType;
  typedef typename FieldTraits::TopologyType TopologyType;
  typedef typename FieldTraits::ElementAssociativityType ElementAssociativityType;
  typedef typename FieldTraits::FieldAssociativityConstructorType FieldAssociativityConstructorType;

  template<class ElemT = Real>
  using ElementType = typename FieldTraits::template ElementType<ElemT>;

  FieldAssociativity();   // needed for new []
  FieldAssociativity( const FieldAssociativity& a ) : FieldAssociativity() { operator=(a); }
  explicit FieldAssociativity( const FieldAssociativityConstructorType& assoc ) : FieldAssociativity() { resize(assoc); }
  virtual ~FieldAssociativity();

  // needed to resize arrays of FieldAssociativity
  void resize( const FieldAssociativityConstructorType& assoc );

  FieldAssociativity& operator=( const FieldAssociativity& );

  using BaseType::nElem;
  using BaseType::nDOF;
  using BaseType::setDOF;

  virtual int nElemTrace()    const override { return TopologyType::NTrace; }
  virtual int nBasis()        const override { return basis_->nBasis(); }
  virtual int order()         const override { return basis_->order(); }
  virtual BasisFunctionCategory basisCategory() const override { return basis_->category(); }

  // gets the toplogical type ID
  virtual const std::type_info& topoTypeID() const override { return typeid(TopologyType); }

  // basis function
  const BasisType* basis() const { return basis_; }

  // element DOF associations
  const ElementAssociativityType& associativity( const int elem ) const { return assoc_[elem]; }

  // extract/set individual element
  template <class ElemT>
  void getElement(       Element<ElemT, typename TopologyType::TopoDim, TopologyType>& fldElem, const int elem ) const;
  template <class ElemT>
  void setElement( const Element<ElemT, typename TopologyType::TopoDim, TopologyType>& fldElem, const int elem );

  // projects dofs onto FieldAssociativity with possibly different basis functions
  void projectTo( FieldAssociativity& ) const;

  virtual void dump( int indentSize=0, std::ostream& out = std::cout ) const override;

protected:
  using BaseType::nElem_;             // total elements in this field group
  using BaseType::nDOF_;              // total DOFs in the field (not local to this group)
  using BaseType::DOF_;               // DOFs in the field

  const BasisType* basis_;

  ElementAssociativityType* assoc_;   // element DOF associations (local to global)
};


// default ctor needed for 'new []'
template <class FieldTraits>
FieldAssociativity<FieldTraits>::FieldAssociativity()
{
  basis_ = NULL;

  DOF_ = NULL;
  nDOF_ = 0;

  assoc_ = NULL;
}

template <class FieldTraits>
void
FieldAssociativity<FieldTraits>::resize( const FieldAssociativityConstructorType& assoc )
{
  nElem_ = assoc.nElem();

  basis_ = assoc.basis();

  DOF_ = NULL;
  nDOF_ = 0;

  delete [] assoc_; assoc_ = NULL;

  // element to DOF associativity
  assoc_ = new ElementAssociativityType[nElem_];

  for (int n = 0; n < nElem_; n++)
    assoc_[n] = assoc.getAssociativity(n);
}


template <class FieldTraits>
FieldAssociativity<FieldTraits>::~FieldAssociativity()
{
  // element to DOF associativity
  delete [] assoc_;
}

template <class FieldTraits>
FieldAssociativity<FieldTraits>&
FieldAssociativity<FieldTraits>::operator=( const FieldAssociativity& a )
{
  if (this != &a)
  {
    nElem_ = a.nElem_;
    nDOF_  = a.nDOF_;

    basis_ = a.basis_;

    DOF_ = a.DOF_;

    delete [] assoc_; assoc_ = NULL;

    assoc_ = new ElementAssociativityType[nElem_];

    for (int n = 0; n < nElem_; n++)
      assoc_[n] = a.assoc_[n];
  }

  return *this;
}

// extract DOFs for individual element
template <class FieldTraits>
template <class ElemT>
void
FieldAssociativity<FieldTraits>::getElement( Element<ElemT, typename TopologyType::TopoDim, TopologyType>& fldElem, const int elem ) const
{
  SANS_ASSERT( (elem >= 0) && (elem < nElem_) );
  SANS_ASSERT( (DOF_ != NULL) );
  SANS_ASSERT( basis_ == fldElem.basis() );

  assoc_[elem].getElement(fldElem, DOF_, nDOF_);
}

// set DOFs for individual element
template <class FieldTraits>
template <class ElemT>
void
FieldAssociativity<FieldTraits>::setElement( const Element<ElemT, typename TopologyType::TopoDim, TopologyType>& fldElem, const int elem )
{
  SANS_ASSERT( (elem >= 0) && (elem < nElem_) );
  SANS_ASSERT( (DOF_ != NULL) );
  SANS_ASSERT( basis_ == fldElem.basis() );

  if (basis_->category() == BasisFunctionCategory_Hierarchical &&
      basis_->order() > 2 &&
      TopologyType::TopoDim::D > 1)
  {
    for (int n = 0; n < TopologyType::NTrace; n++)
    {
      SANS_ASSERT_MSG( assoc_[elem].traceOrientation()[n] == fldElem.traceOrientation()[n],
                       "%d != %d", assoc_[elem].traceOrientation()[n], fldElem.traceOrientation()[n] );
    }
  }

  assoc_[elem].setElement(fldElem, DOF_, nDOF_);
}

// project DOF onto another polynomial order
template <class FieldTraits>
void
FieldAssociativity<FieldTraits>::projectTo( FieldAssociativity& fldTo ) const
{
  SANS_ASSERT( nElem_ == fldTo.nElem_ );

  ElementType<> ElemFrom( basis() );
  typename FieldAssociativity::template ElementType<> ElemTo( fldTo.basis() );

  for ( int elem = 0; elem < nElem_; elem++ )
  {
    getElement(ElemFrom, elem);
    ElemFrom.projectTo(ElemTo);
    fldTo.setElement(ElemTo, elem);
  }
}


template <class FieldTraits>
void
FieldAssociativity<FieldTraits>::dump( int indentSize, std::ostream& out ) const
{
  int n;
  std::string indent(indentSize, ' ');
  out << indent << "FieldAssociativity:"
      << "  nElem_ = " << nElem_
      << "  nDOF_ = " << nDOF_ << std::endl;
  out << indent << "  DOF_[] =";
  for (n = 0; n < nDOF_; n++)
    out << "(" << DOF_[n] << ") ";
  out << std::endl;
  for (n = 0; n < nElem_; n++)
  {
    out << indent << "  assoc_[" << n << "]:" << std::endl;
    assoc_[n].dump( indentSize+4, out );
  }
}

template <int k, class FieldTraits>
const FieldAssociativity<FieldTraits>&
get(const FieldAssociativity<FieldTraits>& xfld)
{
  static_assert( k == 0 || k == -1, "k should be zero or -1 if the argument to get is FieldAssociativity");
  return xfld;
}

template <class Type, class FieldTraits>
const FieldAssociativity<FieldTraits>&
get(const FieldAssociativity<FieldTraits>& xfld)
{
  static_assert( ( std::is_same<Type, FieldAssociativity<FieldTraits>>::value // one is the typedef of the other
                   || std::is_base_of<FieldAssociativity<FieldTraits>, Type>::value // one is the base class of the other
                   || std::is_convertible<Type, FieldAssociativity<FieldTraits>>::value), // one can be converted to the other
                 "Type should be FieldAssociativity or a derived class from it" );
  return xfld;
}

}

#endif  // FIELDASSOCIATIVITY_H
