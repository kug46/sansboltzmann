// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define FIELDLIFT_DG_BOUNDARYTRACEBASE_INSTANTIATE
#include "FieldLift_DG_BoundaryTraceBase_impl.h"

#define FIELDLIFTAREA_DG_BOUNDARYTRACE_INSTANTIATE
#include "FieldLiftArea_DG_BoundaryTrace_impl.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "XFieldArea.h"


namespace SANS
{

template class FieldLift_DG_BoundaryTrace< PhysD2, TopoD2, DLA::VectorS<2, Real> >;

//template class FieldLift_DG_BoundaryTrace< PhysD2, TopoD2, DLA::VectorS<2, DLA::VectorS<2, Real> > >;
//template class FieldLift_DG_BoundaryTrace< PhysD2, TopoD2, DLA::VectorS<2, DLA::VectorS<3, Real> > >;
template class FieldLift_DG_BoundaryTrace< PhysD2, TopoD2, DLA::VectorS<2, DLA::VectorS<4, Real> > >;
template class FieldLift_DG_BoundaryTrace< PhysD2, TopoD2, DLA::VectorS<2, DLA::VectorS<5, Real> > >;
//template class FieldLift_DG_BoundaryTrace< PhysD2, TopoD2, DLA::VectorS<2, DLA::VectorS<6, Real> > >;

}
