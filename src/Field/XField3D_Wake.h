// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SANS_XFIELD3D_WAKE_H
#define SANS_XFIELD3D_WAKE_H

//Represents a 3D grid with wakes for full potential

#include "XFieldLine.h"
#include "XFieldVolume.h"

namespace SANS
{

struct NodeToBCElemMap
{
  int KuttaPoint;
  std::vector<int> elems;
};


class XField3D_Wake : public XField<PhysD3, TopoD3>
{
public:
  static const char* WAKESHEET;     //Used to mark that an EGADS face is a wake for full-potential
  static const char* TREFFTZ;
  static const char* BCNAME;

  static const int Dim = PhysD3::D;
  typedef XField<PhysD3, TopoD3> BaseType;
  typedef BaseType::VectorX CartCoord;
  typedef DLA::MatrixSymS<Dim,Real> MatrixSym;

  XField3D_Wake();
  XField3D_Wake( mpi::communicator& comm );
  XField3D_Wake( const std::shared_ptr<mpi::communicator>& comm );

  XField3D_Wake( const XField3D_Wake& ) = delete;
  XField3D_Wake& operator=( const XField3D_Wake& ) = delete;
  ~XField3D_Wake();

  int nVertex() { return nDOF_; }
  CartCoord& X(const int n) { return DOF_[n]; }

  void cloneFrom( const XField3D_Wake& xfld );

  typedef ElementConnectivity< XFieldGroupLineTraits<PhysD3, TopoD2> > FieldFrameGroupType;

  int nBoundaryFrameGroups() const { return boundaryFrameGroups_.size(); }
  const FieldFrameGroupType& getBoundaryFrameGroup( const int group ) const
  {
    SANS_ASSERT_MSG(group >= 0, "group =%d", group);
    SANS_ASSERT_MSG(group < boundaryFrameGroups_.size(), "group =%d, nBoundaryFrameGroups_ = %d", group, boundaryFrameGroups_.size());
    return *boundaryFrameGroups_[group];
  }

  std::vector< int > modelBodyIndex_[2];

  int dupPointOffset_;
  std::vector<int> invPointMap_;
  std::vector<int> KuttaPoints_;

  std::map<int, std::map<int,NodeToBCElemMap>> KuttaBCTraceElemLeft_;
  std::map<int, std::map<int,NodeToBCElemMap>> KuttaBCTraceElemRight_;

  std::map<int, std::map<int,NodeToBCElemMap>> KuttaCellElemLeft_;
  std::map<int, std::map<int,NodeToBCElemMap>> KuttaCellElemRight_;

protected:
  void resizeBoundaryFrameGroups( const int nBoundaryFrameGroups );

  PointerArray<FieldFrameGroupType> boundaryFrameGroups_;   // groups of boundary-frame elements
};

//---------------------------------------------------------------------------//
template<int k>
const XField3D_Wake&
get(const XField3D_Wake& xfld)
{
  static_assert( k == 0 || k == -1, "k should be zero or -1 if the argument to get is XField3D_Wake");
  return xfld;
}

}

#endif //SANS_XFIELD3D_WAKE_H
