// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ELEMENTCONNECTIVITYCONSTRUCTOR_H
#define ELEMENTCONNECTIVITYCONSTRUCTOR_H

#include <iostream>
#include <string>

#include "GroupElementType.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "Topology/Dimension.h"
#include "FieldAssociativityConstructor.h"
#include "BasisFunction/TraceToCellRefCoord.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Element connectivity between the canonical traces of two elements
//
// implemented as abstract base class with templated derived classes
//----------------------------------------------------------------------------//

template <class ElementAssociativityConstructor>
class ElementConnectivityConstructor : public FieldAssociativityConstructor< ElementAssociativityConstructor >
{
public:
  typedef FieldAssociativityConstructor< ElementAssociativityConstructor > BaseType;
  typedef typename ElementAssociativityConstructor::BasisType BasisType;

  ElementConnectivityConstructor() { init(); }  // needed for new []
  ElementConnectivityConstructor( const BasisType* basis, const int nelem )
   : BaseType() //resize will resize the basetype as well (so no nelem is passed to base)
  {
    init();
    resize(basis,nelem);
  }

  ~ElementConnectivityConstructor();

  // no copy constructor
  ElementConnectivityConstructor( const ElementConnectivityConstructor& ) = delete;
  ElementConnectivityConstructor& operator=( const ElementConnectivityConstructor& ) = delete;

  //Used to resize the in case an array is allocated
  void resize( const BasisType* basis, const int nelem );

  // adjacent area-element group/element connectivity
  void setGroupLeft( const int cellGroupL );
  int getGroupLeft() const;
  bool isGroupLeftSet() const { return isSetGroupL_; }
  void setElementLeft( const int cellElemL, const int elem );
  int getElementLeft( const int elem ) const;
  bool isElementLeftSet( const int elem ) const { return isSetElemL_[elem]; }
  void setCanonicalTraceLeft( const CanonicalTraceToCell& CanonicalL, const int elem );
  CanonicalTraceToCell& getCanonicalTraceLeft( const int elem ) const;

  void setGroupRight( const int cellGroupR );
  int getGroupRight() const;
  bool isGroupRightSet() const { return isSetGroupR_; }
  void setElementRight( const int cellElemR, const int elem );
  int getElementRight( const int elem ) const;
  bool isElementRightSet( const int elem ) const { return isSetElemR_[elem]; }
  void setCanonicalTraceRight( const CanonicalTraceToCell& CanonicalR, const int elem );
  CanonicalTraceToCell& getCanonicalTraceRight( const int elem ) const;

  void setGroupRightType(const GroupElmentType& GroupRType) { GroupRType_ = GroupRType; }
  GroupElmentType getGroupRightType() const { return GroupRType_; }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  void init();

  using BaseType::nElem_;

  // Indicates if the right element group is a cell group or a hub trace group
  GroupElmentType GroupRType_;

  // left adjacent element connectivity
  int GroupL_;
  int* ElemL_;                         // list of elements in groupL
  CanonicalTraceToCell* CanonicalL_;   // list of canonical element boundary (1-3 for triangle)
  bool  isSetGroupL_;
  bool* isSetElemL_;
  bool* isSetCanonicalL_;

  // right adjacent element connectivity
  int GroupR_;
  int* ElemR_;                        // list of elements in groupR
  CanonicalTraceToCell* CanonicalR_;  // list of canonical element boundary (1-3 for triangle)
  bool  isSetGroupR_;
  bool* isSetElemR_;
  bool* isSetCanonicalR_;
};


// default ctor needed for 'new []'
template <class ElementAssociativityConstructor>
void
ElementConnectivityConstructor<ElementAssociativityConstructor>::init()
{
  // Hub traces are only used for very special cases, defaulting it CellGroup is pretty safe
  GroupRType_ = eCellGroup;

  GroupL_ = -1;
  GroupR_ = -1;
  ElemL_ = nullptr;
  ElemR_ = nullptr;
  CanonicalL_ = nullptr;
  CanonicalR_ = nullptr;

  isSetGroupL_ = false;
  isSetGroupR_ = false;
  isSetElemL_ = nullptr;
  isSetElemR_ = nullptr;
  isSetCanonicalL_ = nullptr;
  isSetCanonicalR_ = nullptr;
}


template <class ElementAssociativityConstructor>
void
ElementConnectivityConstructor<ElementAssociativityConstructor>::resize( const BasisType* basis, const int nelem )
{
  SANS_ASSERT_MSG( nelem >= 0, "nelem = %d", nelem );

  BaseType::resize(basis, nelem);

  // adjacent area-element connectivity
  ElemL_ = new int[nElem_];
  isSetElemL_ = new bool[nElem_];
  CanonicalL_ = new CanonicalTraceToCell[nElem_];
  isSetCanonicalL_ = new bool[nElem_];

  ElemR_ = new int[nElem_];
  isSetElemR_ = new bool[nElem_];
  if (GroupRType_ == eCellGroup)
  {
    CanonicalR_ = new CanonicalTraceToCell[nElem_];
    isSetCanonicalR_ = new bool[nElem_];
  }

  GroupL_ = -1;
  GroupR_ = -1;
  for (int n = 0; n < nElem_; n++)
  {
    ElemL_[n] = -1;
    ElemR_[n] = -1;
  }

  isSetGroupL_ = false;
  isSetGroupR_ = false;
  const int nElem = nElem_;
  for (int n = 0; n < nElem; n++)
  {
    isSetElemL_[n] = false;
    isSetElemR_[n] = false;

    isSetCanonicalL_[n] = false;
    if (GroupRType_ == eCellGroup)
      isSetCanonicalR_[n] = false;
  }
}


template <class ElementAssociativityConstructor>
ElementConnectivityConstructor<ElementAssociativityConstructor>::~ElementConnectivityConstructor()
{
  // adjacent area-element connectivity
  delete [] ElemL_;
  delete [] ElemR_;
  delete [] CanonicalL_;
  delete [] CanonicalR_;

  delete [] isSetElemL_;
  delete [] isSetElemR_;
  delete [] isSetCanonicalL_;
  delete [] isSetCanonicalR_;
}

template <class ElementAssociativityConstructor>
void
ElementConnectivityConstructor<ElementAssociativityConstructor>::setGroupLeft( const int areaGroupL )
{
  if (isSetGroupL_) SANS_ASSERT( GroupL_ == areaGroupL );
  GroupL_ = areaGroupL;
  isSetGroupL_ = true;
}


template <class ElementAssociativityConstructor>
int
ElementConnectivityConstructor<ElementAssociativityConstructor>::getGroupLeft() const
{
  SANS_ASSERT( isSetGroupL_ );

  return GroupL_;
}


template <class ElementAssociativityConstructor>
void
ElementConnectivityConstructor<ElementAssociativityConstructor>::setElementLeft( const int areaElemL, const int elem )
{
  SANS_ASSERT_MSG( (elem >= 0) && (elem < nElem_) && !isSetElemL_[elem], "elem = %d, nElem_ = %d", elem, nElem_ );

  ElemL_[elem] = areaElemL;
  isSetElemL_[elem] = true;
}


template <class ElementAssociativityConstructor>
int
ElementConnectivityConstructor<ElementAssociativityConstructor>::getElementLeft( const int elem ) const
{
  SANS_ASSERT_MSG( (elem >= 0) && (elem < nElem_) && isSetElemL_[elem],
               "elem=%d, nElem_=%d, isSetElemL_[elem]=%s", elem, nElem_, isSetElemL_[elem] ? "true" : "false" );

  return ElemL_[elem];
}


template <class ElementAssociativityConstructor>
void
ElementConnectivityConstructor<ElementAssociativityConstructor>::setCanonicalTraceLeft( const CanonicalTraceToCell& CanonicalL, const int elem )
{
  SANS_ASSERT( (elem >= 0) && (elem < nElem_) && !isSetCanonicalL_[elem] );

  CanonicalL_[elem] = CanonicalL;
  isSetCanonicalL_[elem] = true;
}


template <class ElementAssociativityConstructor>
CanonicalTraceToCell&
ElementConnectivityConstructor<ElementAssociativityConstructor>::getCanonicalTraceLeft( const int elem ) const
{
  SANS_ASSERT( (elem >= 0) && (elem < nElem_) && isSetCanonicalL_[elem] );

  return CanonicalL_[elem];
}


template <class ElementAssociativityConstructor>
void
ElementConnectivityConstructor<ElementAssociativityConstructor>::setGroupRight( int areaGroupR )
{
  if (isSetGroupR_) SANS_ASSERT( GroupR_ == areaGroupR );
  GroupR_ = areaGroupR;
  isSetGroupR_ = true;
}


template <class ElementAssociativityConstructor>
int
ElementConnectivityConstructor<ElementAssociativityConstructor>::getGroupRight() const
{
  SANS_ASSERT( isSetGroupR_ );

  return GroupR_;
}


template <class ElementAssociativityConstructor>
void
ElementConnectivityConstructor<ElementAssociativityConstructor>::setElementRight( const int areaElemR, const int elem )
{
  SANS_ASSERT( (elem >= 0) && (elem < nElem_) && !isSetElemR_[elem] );

  ElemR_[elem] = areaElemR;
  isSetElemR_[elem] = true;
}


template <class ElementAssociativityConstructor>
int
ElementConnectivityConstructor<ElementAssociativityConstructor>::getElementRight( const int elem ) const
{
  SANS_ASSERT( (elem >= 0) && (elem < nElem_) && isSetElemR_[elem] );

  return ElemR_[elem];
}


template <class ElementAssociativityConstructor>
void
ElementConnectivityConstructor<ElementAssociativityConstructor>::setCanonicalTraceRight( const CanonicalTraceToCell& CanonicalR, const int elem )
{
  SANS_ASSERT( (GroupRType_ == eCellGroup) && (elem >= 0) && (elem < nElem_) && !isSetCanonicalR_[elem] );

  CanonicalR_[elem] = CanonicalR;
  isSetCanonicalR_[elem] = true;
}


template <class ElementAssociativityConstructor>
CanonicalTraceToCell&
ElementConnectivityConstructor<ElementAssociativityConstructor>::getCanonicalTraceRight( const int elem ) const
{
  SANS_ASSERT( (GroupRType_ == eCellGroup) && (elem >= 0) && (elem < nElem_) && isSetCanonicalR_[elem] );

  return CanonicalR_[elem];
}


template <class ElementAssociativityConstructor>
void
ElementConnectivityConstructor<ElementAssociativityConstructor>::dump( int indentSize, std::ostream& out ) const
{
  BaseType::dump(indentSize, out);

  int n;
  std::string indent(indentSize, ' ');
  out << indent << "ElementConnectivityConstructor:";
  out << std::endl;

  out << indent << "  L: isSetareaGroupL_ = " << isSetGroupL_ << "  isSetareaElemL_[] =";
  for (n = 0; n < nElem_; n++)
    out << " " << isSetElemL_[n];
  out << "  isSetareaCanonicalL_[] =";
  for (n = 0; n < nElem_; n++)
    out << " " << isSetCanonicalL_[n];
  out << std::endl;
  out << indent << "  L: areaGroupL_ = " << GroupL_ << "  areaElemL_[] =";
  for (n = 0; n < nElem_; n++)
    if (isSetElemL_[n])
      out << " " << ElemL_[n];
    else
      out << " -";
  out << "  areaCanonicalL_[] =";
  for (n = 0; n < nElem_; n++)
    if (isSetElemL_[n])
      out << " " << CanonicalL_[n];
    else
      out << " -";
  out << std::endl;

  out << indent << "  R: isSetareaGroupR_ = " << isSetGroupR_ << "  isSetareaElemR_[] =";
  for (n = 0; n < nElem_; n++)
    out << " " << isSetElemR_[n];
  out << "  isSetareaCanonicalR_[] =";
  for (n = 0; n < nElem_; n++)
    out << " " << isSetCanonicalR_[n];
  out << std::endl;
  out << indent << "  R: areaGroupR_ = " << GroupR_ << "  areaElemR_[] =";
  for (n = 0; n < nElem_; n++)
    if (isSetElemR_[n])
      out << " " << ElemR_[n];
    else
      out << " -";
  out << "  areaCanonicalR_[] =";
  for (n = 0; n < nElem_; n++)
    if (isSetElemR_[n])
      out << " " << CanonicalR_[n];
    else
      out << " -";
  out << std::endl;
}

}

#endif  // ELEMENTCONNECTIVITY_H
