
INCLUDE( ${CMAKE_SOURCE_DIR}/CMakeInclude/ForceOutOfSource.cmake ) #This must be the first thing included

SET( FIELD_SRC
     Element/BoundingBox.cpp
     Element/ElementAssociativityNode.cpp
     Element/ElementAssociativityNodeConstructor.cpp
     Element/ElementAssociativityLine.cpp
     Element/ElementAssociativityLineConstructor.cpp
     Element/ElementAssociativityArea.cpp
     Element/ElementAssociativityAreaConstructor.cpp
     Element/ElementAssociativityVolume.cpp
     Element/ElementAssociativityVolumeConstructor.cpp
     Element/ElementAssociativitySpacetime.cpp
     Element/ElementAssociativitySpacetimeConstructor.cpp
     Element/ElementXFieldLine_Instantiate_1D.cpp
     Element/ElementXFieldLine_Instantiate_2D.cpp
     Element/ElementXFieldLine_Instantiate_3Dand4D.cpp
     Element/ElementXFieldArea_Instantiate_2D.cpp
     Element/ElementXFieldArea_Instantiate_3D.cpp
     Element/ElementXFieldVolume_Instantiate_3D.cpp
     Element/ElementXFieldVolume_Instantiate_4D.cpp
     Element/ElementXFieldSpacetime_Instantiate_4D.cpp
     Element/ElementXFieldJacobianEquilateral.cpp
     Element/ReferenceElementMesh.cpp
     Element/RefCoordSolver.cpp
     Element/UniqueElem.cpp

     CellToTraceAssociativity.cpp

     FieldBaseLine_Instantiate.cpp
     FieldBaseArea_Instantiate.cpp
     FieldBaseVolume_Instantiate.cpp
     FieldBaseSpacetime_Instantiate.cpp

     FieldLine_Instantiate_1D.cpp
     FieldLine_Instantiate_2D.cpp
     FieldArea_Instantiate_2D.cpp
     FieldArea_Instantiate_3D.cpp
     FieldVolume_Instantiate_3D.cpp
     FieldVolume_Instantiate_4D.cpp
     FieldSpacetime_Instantiate_4D.cpp

     FieldLine_DG_Cell_Instantiate_1D.cpp
     FieldLine_DG_Cell_Instantiate_2D.cpp
     FieldArea_DG_Cell_Instantiate_2D.cpp
     FieldArea_DG_Cell_Instantiate_3D.cpp
     FieldVolume_DG_Cell_Instantiate_3D.cpp
     FieldVolume_DG_Cell_Instantiate_4D.cpp
     FieldSpacetime_DG_Cell_Instantiate_4D.cpp

     FieldLine_DG_Trace_Instantiate_1D.cpp
     FieldLine_DG_Trace_Instantiate_2D.cpp
     FieldArea_DG_Trace_Instantiate_2D.cpp
     FieldArea_DG_Trace_Instantiate_3D.cpp
     FieldVolume_DG_Trace_Instantiate_3D.cpp
     FieldVolume_DG_Trace_Instantiate_4D.cpp
     FieldSpacetime_DG_Trace_Instantiate_4D.cpp

     FieldLine_DG_HubTrace_Instantiate_1D.cpp
     FieldLine_DG_HubTrace_Instantiate_2D.cpp

     FieldLine_DG_BoundaryTrace_Instantiate_1D.cpp
     FieldLine_DG_BoundaryTrace_Instantiate_2D.cpp
     FieldArea_DG_BoundaryTrace_Instantiate_2D.cpp
     FieldArea_DG_BoundaryTrace_Instantiate_3D.cpp
     FieldVolume_DG_BoundaryTrace_Instantiate_3D.cpp
     FieldVolume_DG_BoundaryTrace_Instantiate_4D.cpp
     FieldSpacetime_DG_BoundaryTrace_Instantiate_4D.cpp

     FieldLine_DG_InteriorTrace_Instantiate_1D.cpp
     FieldLine_DG_InteriorTrace_Instantiate_2D.cpp
     FieldArea_DG_InteriorTrace_Instantiate_2D.cpp
     FieldArea_DG_InteriorTrace_Instantiate_3D.cpp
     FieldVolume_DG_InteriorTrace_Instantiate_3D.cpp
     FieldVolume_DG_InteriorTrace_Instantiate_4D.cpp
     FieldSpacetime_DG_InteriorTrace_Instantiate_4D.cpp

     FieldLiftLine_DG_Cell_Instantiate_1D.cpp
     FieldLiftLine_DG_Cell_Instantiate_2D.cpp
     FieldLiftArea_DG_Cell_Instantiate_2D.cpp
     FieldLiftArea_DG_Cell_Instantiate_3D.cpp
     FieldLiftVolume_DG_Cell_Instantiate_3D.cpp
     FieldLiftSpaceTime_DG_Cell_Instantiate_4D.cpp

     FieldLiftArea_DG_BoundaryTrace_Instantiate_2D.cpp
     FieldLiftVolume_DG_BoundaryTrace_Instantiate_3D.cpp
     FieldLiftSpaceTime_DG_BoundaryTrace_Instantiate_4D.cpp

     Field_CG/Field_CG_Topology.cpp
     Field_CG/Field_CG_CellConstructorLine_1D.cpp
     Field_CG/Field_CG_CellConstructorLine_2D.cpp
     Field_CG/Field_CG_CellConstructorArea_2D.cpp
     Field_CG/Field_CG_CellConstructorArea_3D.cpp
     Field_CG/Field_CG_CellConstructorVolume.cpp
     Field_CG/Field_CG_CellConstructorSpacetime.cpp
     Field_CG/Field_CG_TraceConstructorLine_1D.cpp
     Field_CG/Field_CG_TraceConstructorLine_2D.cpp
     Field_CG/Field_CG_TraceConstructorArea_2D.cpp
     Field_CG/Field_CG_TraceConstructorArea_3D.cpp
     Field_CG/Field_CG_TraceConstructorVolume.cpp
     Field_CG/Field_CG_TraceConstructorSpacetime.cpp

     FieldLine_CG_Cell_Instantiate_1D.cpp
     FieldLine_CG_BoundaryTrace_Instantiate_1D.cpp

     FieldLine_CG_Cell_Instantiate_2D.cpp
     FieldLine_CG_BoundaryTrace_Instantiate_2D.cpp

     FieldArea_CG_Cell_Instantiate_2D.cpp
     FieldArea_CG_Trace_Instantiate_2D.cpp
     FieldArea_CG_InteriorTrace_Instantiate_2D.cpp
     FieldArea_CG_BoundaryTrace_Instantiate_2D.cpp

     FieldArea_CG_Cell_Instantiate_3D.cpp
     FieldArea_CG_BoundaryTrace_Instantiate_3D.cpp

     FieldVolume_CG_Cell_Instantiate_3D.cpp
     FieldVolume_CG_Trace_Instantiate_3D.cpp
     FieldVolume_CG_InteriorTrace_Instantiate_3D.cpp
     FieldVolume_CG_BoundaryTrace_Instantiate_3D.cpp

     FieldSpacetime_CG_Cell_Instantiate_4D.cpp
     FieldSpacetime_CG_Trace_Instantiate_4D.cpp
     FieldSpacetime_CG_InteriorTrace_Instantiate_4D.cpp
     FieldSpacetime_CG_BoundaryTrace_Instantiate_4D.cpp

     FieldLine_EG_Cell_Instantiate_1D.cpp
     FieldArea_EG_Cell_Instantiate_2D.cpp
     FieldVolume_EG_Cell_Instantiate_3D.cpp
     FieldSpacetime_EG_Cell_Instantiate_4D.cpp

     FieldData/FieldDataInvMassMatrix_Cell.cpp

     FieldLinesearch/FieldLinesearch_Line_Instantiate.cpp
     FieldLinesearch/FieldLinesearch_Area_Instantiate.cpp
     FieldLinesearch/FieldLinesearch_Volume_Instantiate.cpp
     FieldLinesearch/FieldLinesearch_SpaceTime_Instantiate.cpp

     HField/HField_DG/HField_DG_Line.cpp
     HField/HField_DG/HField_DG_Area.cpp
     HField/HField_DG/HField_DG_Volume.cpp
     HField/HField_DG/HField_DG_Spacetime.cpp

     HField/HField_CG/HField_CG_Line.cpp
     HField/HField_CG/HField_CG_Area.cpp
     HField/HField_CG/HField_CG_Volume.cpp

     HField/GenHField_DG/GenHField_DG_Line.cpp
     HField/GenHField_DG/GenHField_DG_Area.cpp
     HField/GenHField_DG/GenHField_DG_Volume.cpp
     HField/GenHField_DG/GenHField_DG_Spacetime.cpp

     HField/GenHField_CG/GenHField_CG_Line.cpp
     HField/GenHField_CG/GenHField_CG_Area.cpp
     HField/GenHField_CG/GenHField_CG_Volume.cpp

     DistanceFunction/DistanceFunction.cpp
     DistanceFunction/DistanceFunction3D.cpp
     DistanceFunction/DistanceSolver.cpp

     ProjectSoln/PointwiseSolutionStorage.cpp
     ProjectSoln/ProjectGlobalField/ProjectGlobalField1D.cpp
     ProjectSoln/ProjectGlobalField/ProjectGlobalField2D.cpp
     ProjectSoln/ProjectGlobalField/ProjectGlobalField3D.cpp
     ProjectSoln/ProjectGlobalField/ProjectGlobalField4D.cpp

     Field_NodalView.cpp
     Field_NodalView_Line.cpp
     Field_NodalView_Area.cpp
     Field_NodalView_Volume.cpp
     Field_NodalView_Spacetime.cpp

     XField_CellToTrace/XField_CellToTrace_Line_1D.cpp
     XField_CellToTrace/XField_CellToTrace_Line_2D.cpp
     XField_CellToTrace/XField_CellToTrace_Area_2D.cpp
     XField_CellToTrace/XField_CellToTrace_Area_3D.cpp
     XField_CellToTrace/XField_CellToTrace_Volume.cpp
     XField_CellToTrace/XField_CellToTrace_Spacetime.cpp

     Partition/LagrangeElementGroup.cpp
     Partition/XField_Lagrange_Line_1D.cpp
     Partition/XField_Lagrange_Line_2D.cpp
     Partition/XField_Lagrange_Area_2D.cpp
     Partition/XField_Lagrange_Area_3D.cpp
     Partition/XField_Lagrange_Volume_3D.cpp
     Partition/XField_Lagrange_Spacetime_4D.cpp

     XFieldLine_Instantiate_1D.cpp
     XFieldLine_Instantiate_2D.cpp
     XFieldArea_Instantiate_2D.cpp
     XFieldArea_Instantiate_3D.cpp
     XFieldVolume_Instantiate_3D.cpp
     XFieldVolume_Instantiate_4D.cpp
     XFieldSpacetime_Instantiate_4D.cpp

     XFieldLine_BoundaryTrace_Instantiate.cpp
     XFieldArea_BoundaryTrace_Instantiate.cpp
     XFieldVolume_BoundaryTrace_Instantiate.cpp
     XFieldSpacetime_BoundaryTrace_Instantiate.cpp

     XFieldException.cpp

     XField3D_Wake.cpp

     Local/XField_LocalPatchBase/XField_LocalPatchBase_Line.cpp
     Local/XField_LocalPatchBase/XField_LocalPatchBase_Area.cpp
     Local/XField_LocalPatchBase/XField_LocalPatchBase_Volume.cpp
     Local/XField_LocalPatchBase/XField_LocalPatchBase_Spacetime.cpp
     Local/XField_LocalPatchConstructor/XField_LocalPatchConstructor_Line.cpp
     Local/XField_LocalPatchConstructor/XField_LocalPatchConstructor_Area.cpp
     Local/XField_LocalPatchConstructor/XField_LocalPatchConstructor_Volume.cpp
     Local/XField_LocalPatchConstructor/XField_LocalPatchConstructor_Spacetime.cpp
     Local/XField_LocalPatch/XField_LocalPatch_Line.cpp
     Local/XField_LocalPatch/XField_LocalPatch_Area.cpp
     Local/XField_LocalPatch/XField_LocalPatch_Volume.cpp
     Local/XField_LocalPatch/XField_LocalPatch_Spacetime.cpp

     tools/GroupElemIndex.cpp

     output_Tecplot/output_Tecplot_FieldLine_1D.cpp
     output_Tecplot/output_Tecplot_FieldLine_2D.cpp
     output_Tecplot/output_Tecplot_FieldArea.cpp
     output_Tecplot/output_Tecplot_FieldVolume.cpp
     output_Tecplot/output_Tecplot_FieldVolume4.cpp

     output_Tecplot/output_Tecplot_LiftLine.cpp
     output_Tecplot/output_Tecplot_LiftArea.cpp
     output_Tecplot/output_Tecplot_LiftVolume.cpp
     #output_Tecplot/output_Tecplot_LiftVolume4.cpp

     output_Tecplot/output_Tecplot_MetricLine.cpp
     output_Tecplot/output_Tecplot_MetricArea.cpp
     output_Tecplot/output_Tecplot_MetricVolume.cpp
     output_Tecplot/output_Tecplot_MetricVolume4.cpp

     output_Tecplot/output_Tecplot_LIP.cpp
     output_Tecplot/output_Tecplot_FP.cpp
     output_Tecplot/output_Tecplot_Area_3D.cpp
     output_Tecplot/output_Tecplot_SA.cpp
     output_Tecplot/output_Tecplot_Error.cpp

     output_grm.cpp
     output_fluent.cpp

     output_gnuplot.cpp
     output_Points.cpp
  )

ADD_LIBRARY( FieldLib STATIC ${FIELD_SRC} )

#Create the vera targets for this library
ADD_VERA_CHECKS_RECURSE( FieldLib *.h *.cpp )
ADD_HEADER_COMPILE_CHECK_RECURSE( FieldLib *.h )
ADD_CPPCHECK( FieldLib ${FIELD_SRC} )

IF( DEFINED PARMETIS_DEPENDS )
  ADD_DEPENDENCIES( FieldLib ${PARMETIS_DEPENDS} )
  IF( USE_HEADERCHECK )
    ADD_DEPENDENCIES( FieldLib_headercheck ${PARMETIS_DEPENDS} )
  ENDIF()
ENDIF()
