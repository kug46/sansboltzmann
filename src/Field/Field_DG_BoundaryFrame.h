// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELD_DG_BOUNDARYFRAME_H
#define FIELD_DG_BOUNDARYFRAME_H

#include <ostream>
#include <string>
#include <memory>
#include <typeinfo>     // typeid

#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionCategory.h"
#include "FieldGroupLine_Traits.h"
#include "Field.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// solution field: DG boundary trace-field constructor class
//----------------------------------------------------------------------------//

template <class PhysDim, class TopoDim, class T>
class Field_DG_BoundaryFrame_GroupConstructor : public Field< PhysDim, TopoDim, T >
{
public:
  typedef Field< PhysDim, TopoDim, T > BaseType;
  typedef T ArrayQ;

  typedef FieldAssociativity< FieldGroupLineTraits<ArrayQ> > FieldFrameGroupType;

  int nBoundaryFrameGroups() const { return nBoundaryFrameGroups_; }
  const FieldFrameGroupType& getBoundaryFrameGroup( const int group ) const { return *boundaryFrameGroups_[group]; }
  const FieldFrameGroupType& getBoundaryFrameGroupGlobal( const int groupGlobal ) const;

  ~Field_DG_BoundaryFrame_GroupConstructor();
protected:
  //Protected constructor. This is a helper class
  explicit Field_DG_BoundaryFrame_GroupConstructor( const XField<PhysDim, TopoDim>& xfld )
     : BaseType(xfld), nBoundaryFrameGroups_(0), boundaryFrameGroups_(NULL)
  {
    //Derived classes call createBoundaryFrameGroup
  }

  // Constructs a new cell group
  template<class XFieldType, class Topology>
  void createBoundaryFrameGroup(const XFieldType& xfld, const int groupGlobal, const int order, const BasisFunctionCategory& category);

  void createDOFs();

  using BaseType::nDOF_;
  using BaseType::DOF_;
//  using BaseType::nBoundaryFrameGroups_;
//  using BaseType::boundaryFrameGroups_;
  using BaseType::xfld_;
//  using BaseType::localBoundaryFrameGroups_;


  template<class XFieldType>
  void resizeBoundaryFrameGroups( const XFieldType& xfld, const std::vector<int>& BoundaryFrameGroups );

  void resizeBoundaryFrameGroups( const int nBoundaryFrameGroups );

  std::vector<int> localBoundaryFrameGroups_;

  int nBoundaryFrameGroups_;
  FieldFrameGroupType** boundaryFrameGroups_;   // groups of boundary-frame elements
};

//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
template<class XFieldType, class Topology>
void
Field_DG_BoundaryFrame_GroupConstructor<PhysDim, TopoDim, T>::
createBoundaryFrameGroup(const XFieldType& xfld, const int groupGlobal, const int order, const BasisFunctionCategory& category)
{
  typedef typename XFieldType::FieldFrameGroupType XFieldFrameClass;
  typedef FieldFrameGroupType FieldFrameClass;
  typedef typename FieldFrameClass::BasisType BasisType;

  const XFieldFrameClass& xfldGroup = xfld.getBoundaryFrameGroup(groupGlobal);

  const BasisType* basis = BasisType::getBasisFunction(order, category);

  // create field associativity constructor
  typename FieldFrameClass::FieldAssociativityConstructorType fldAssocFrame( basis, xfldGroup.nElem() );

  // DOF ordering: Fortran-array DOF(basis,elem)
  const int nElem = xfldGroup.nElem();
  const int nBasis = basis->nBasis();
  std::vector<int> map( nBasis );
  for (int elem = 0; elem < nElem; elem++)
  {
    for (int i = 0; i < nBasis; i++)
      map[i] = nDOF_ + nBasis*elem + i;

    fldAssocFrame.setAssociativity( elem ).setGlobalMapping( map );
  }

  // allocate group
  boundaryFrameGroups_[localBoundaryFrameGroups_.at(groupGlobal)] = new FieldFrameClass( fldAssocFrame );

  // accumulate DOF count
  nDOF_ += nElem*nBasis;
}

//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
void
Field_DG_BoundaryFrame_GroupConstructor<PhysDim, TopoDim, T>::createDOFs()
{
  // allocate the solution DOF array and assign it to groups

  this->resizeDOF(nDOF_);
  for (int n = 0; n < nDOF_; n++)
    DOF_[n] = 0;

  for (int group = 0; group < nBoundaryFrameGroups_; group++)
    if ( boundaryFrameGroups_[group] != NULL )
      boundaryFrameGroups_[group]->setDOF( DOF_, nDOF_ );
}


//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim, class T>
template<class XFieldType>
void
Field_DG_BoundaryFrame_GroupConstructor<PhysDim, TopoDim, T>::
resizeBoundaryFrameGroups( const XFieldType& xfld, const std::vector<int>& BoundaryFrameGroups )
{
  this->resizeBoundaryFrameGroups(BoundaryFrameGroups.size());
  localBoundaryFrameGroups_.clear();
  localBoundaryFrameGroups_.resize(xfld.nBoundaryFrameGroups(), -1);
  for (std::size_t group = 0; group < BoundaryFrameGroups.size(); group++)
    localBoundaryFrameGroups_[BoundaryFrameGroups[group]] = group;
}


//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
void
Field_DG_BoundaryFrame_GroupConstructor<PhysDim, TopoDim, T>::resizeBoundaryFrameGroups( const int nBoundaryFrameGroups )
{
  SANS_ASSERT( nBoundaryFrameGroups >= 0 );

  for (int i = 0; i < nBoundaryFrameGroups_; i++ )
    delete boundaryFrameGroups_[i];

  delete [] boundaryFrameGroups_;
  boundaryFrameGroups_= NULL;

  nBoundaryFrameGroups_ = nBoundaryFrameGroups;

  boundaryFrameGroups_ = new FieldFrameGroupType*[nBoundaryFrameGroups];

  for (int i = 0; i < nBoundaryFrameGroups_; i++ )
    boundaryFrameGroups_[i] = NULL;
}

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim, class T>
const typename Field_DG_BoundaryFrame_GroupConstructor<PhysDim, TopoDim, T>::FieldFrameGroupType&
Field_DG_BoundaryFrame_GroupConstructor<PhysDim, TopoDim, T>::getBoundaryFrameGroupGlobal( const int groupGlobal ) const
{
  int groupLocal = localBoundaryFrameGroups_.at(groupGlobal);
  SANS_ASSERT( groupLocal >= 0 );
  return getBoundaryFrameGroup(groupLocal);
}

//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
Field_DG_BoundaryFrame_GroupConstructor<PhysDim, TopoDim, T>::~Field_DG_BoundaryFrame_GroupConstructor()
{
  for (int i = 0; i < nBoundaryFrameGroups_; i++ )
    delete boundaryFrameGroups_[i];

  delete [] boundaryFrameGroups_;
}


template <class PhysDim, class TopoDim, class T>
class Field_DG_BoundaryFrame;

//----------------------------------------------------------------------------//
// 1D solution field: DG boundary trace (e.g. Lagrange multiplier)
//----------------------------------------------------------------------------//
#if 0
template <class PhysDim, class T>
class Field_DG_BoundaryFrame< PhysDim, TopoD1, T > : public Field_DG_BoundaryFrame_GroupConstructor< PhysDim, TopoD1, T >
{
public:
  typedef Field_DG_BoundaryFrame_GroupConstructor< PhysDim, TopoD1, T > BaseType;
  typedef T ArrayQ;

  Field_DG_BoundaryFrame( const XField<PhysDim, TopoD1>& xfld, const int order, const BasisFunctionCategory& category )
    : BaseType( xfld )
  {
    init(xfld,order,category,BaseType::createBoundaryGroupIndex());
  }

  Field_DG_BoundaryFrame( const XField<PhysDim, TopoD1>& xfld, const int order,
                          const BasisFunctionCategory& category, const std::vector<int>& FrameGroups )
    : BaseType( xfld )
  {
    BaseType::checkBoundaryGroupIndex(FrameGroups);
    init(xfld,order,category,FrameGroups);
  }

  Field_DG_BoundaryFrame& operator=( const ArrayQ& q ) { Field< PhysDim, TopoD1, T >::operator=(q); return *this; }

protected:
  using BaseType::nDOF_;
  using BaseType::DOF_;
  using BaseType::nBoundaryFrameGroups_;
  using BaseType::boundaryFrameGroups_;

  void init(const XField<PhysDim, TopoD1>& xfld, const int order,
            const BasisFunctionCategory& category, const std::vector<int>& FrameGroups );
};


//----------------------------------------------------------------------------//
template <class PhysDim, class T>
void
Field_DG_BoundaryFrame<PhysDim, TopoD1, T>::init(
    const XField<PhysDim, TopoD1>& xfld, const int order,
    const BasisFunctionCategory& category, const std::vector<int>& FrameGroups )
{
  for (std::size_t igroup = 0; igroup < FrameGroups.size(); igroup++)
  {
    const int group = FrameGroups[igroup];

    if ( xfld.getBoundaryFrameGroupBase(group).topoTypeID() == typeid(Node) )
      this->template createBoundaryFrameGroup<Node>(group, 0, BasisFunctionCategory_Legendre);
    else
      SANS_DEVELOPER_EXCEPTION( "Field_DG_BoundaryFrame<PhysDim, TopoD1, T>::Field_DG_BoundaryFrame: Unknown element topology" );
  }

  // allocate the solution DOF array and assign it to groups
  this->createDOFs();
}

//----------------------------------------------------------------------------//
// 2D solution field: DG boundary trace (e.g. Lagrange multiplier)
//----------------------------------------------------------------------------//

template <class PhysDim, class T>
class Field_DG_BoundaryFrame< PhysDim, TopoD2, T > : public Field_DG_BoundaryFrame_GroupConstructor< PhysDim, TopoD2, T >
{
public:
  typedef Field_DG_BoundaryFrame_GroupConstructor< PhysDim, TopoD2, T > BaseType;
  typedef T ArrayQ;

  Field_DG_BoundaryFrame( const XField<PhysDim, TopoD2>& xfld, const int order, const BasisFunctionCategory& category )
    : BaseType( xfld )
  {
    init(xfld,order,category,BaseType::createBoundaryGroupIndex());
  }

  Field_DG_BoundaryFrame( const XField<PhysDim, TopoD2>& xfld, const int order,
                          const BasisFunctionCategory& category, const std::vector<int>& FrameGroups )
    : BaseType( xfld )
  {
    BaseType::checkBoundaryGroupIndex(FrameGroups);
    init(xfld,order,category,FrameGroups);
  }

  Field_DG_BoundaryFrame& operator=( const ArrayQ& q ) { Field< PhysDim, TopoD2, T >::operator=(q); return *this; }

protected:
  using BaseType::nDOF_;
  using BaseType::DOF_;
  using BaseType::nBoundaryFrameGroups_;
  using BaseType::boundaryFrameGroups_;

  void init(const XField<PhysDim, TopoD2>& xfld, const int order,
            const BasisFunctionCategory& category, const std::vector<int>& FrameGroups );
};


//----------------------------------------------------------------------------//
template <class PhysDim, class T>
void
Field_DG_BoundaryFrame<PhysDim, TopoD2, T>::init(
    const XField<PhysDim, TopoD2>& xfld, const int order,
    const BasisFunctionCategory& category, const std::vector<int>& FrameGroups )
{
  for (std::size_t igroup = 0; igroup < FrameGroups.size(); igroup++)
  {
    const int group = FrameGroups[igroup];

    if ( xfld.getBoundaryFrameGroupBase(group).topoTypeID() == typeid(Line) )
      this->template createBoundaryFrameGroup<Line>(group, order, category);
    else
      SANS_DEVELOPER_EXCEPTION( "Field_DG_BoundaryFrame<PhysDim, TopoDim, T>::Field_DG_BoundaryFrame: Unknown element topology" );
  }

  // allocate the solution DOF array and assign it to groups
  this->createDOFs();
}
#endif

//----------------------------------------------------------------------------//
// 3D solution field: DG boundary frame
//----------------------------------------------------------------------------//

template <class PhysDim, class T>
class Field_DG_BoundaryFrame< PhysDim, TopoD3, T > : public Field_DG_BoundaryFrame_GroupConstructor< PhysDim, TopoD3, T >
{
public:
  typedef Field_DG_BoundaryFrame_GroupConstructor< PhysDim, TopoD3, T > BaseType;
  typedef T ArrayQ;
#if 0
  Field_DG_BoundaryFrame( const XField<PhysDim, TopoD3>& xfld, const int order, const BasisFunctionCategory& category )
    : BaseType( xfld )
  {
    init(xfld,order,category,BaseType::createBoundaryGroupIndex());
  }
#endif
  template<class XFieldType>
  Field_DG_BoundaryFrame( const XFieldType& xfld, const int order,
                          const BasisFunctionCategory& category, const std::vector<int>& FrameGroups )
    : BaseType( xfld )
  {
    //BaseType::checkBoundaryGroupIndex(FrameGroups);
    init(xfld,order,category,FrameGroups);
  }

  Field_DG_BoundaryFrame& operator=( const ArrayQ& q ) { Field< PhysDim, TopoD3, T >::operator=(q); return *this; }

protected:
  using BaseType::nDOF_;
  using BaseType::DOF_;
  using BaseType::nBoundaryFrameGroups_;
  using BaseType::boundaryFrameGroups_;

  template<class XFieldType>
  void init(const XFieldType& xfld, const int order,
            const BasisFunctionCategory& category, const std::vector<int>& FrameGroups );
};


//----------------------------------------------------------------------------//
template <class PhysDim, class T>
template<class XFieldType>
void
Field_DG_BoundaryFrame<PhysDim, TopoD3, T>::init(
    const XFieldType& xfld, const int order,
    const BasisFunctionCategory& category, const std::vector<int>& FrameGroups )
{
  this->resizeBoundaryFrameGroups(xfld, FrameGroups);

  for (std::size_t igroup = 0; igroup < FrameGroups.size(); igroup++)
  {
    const int group = FrameGroups[igroup];

    if ( xfld.getBoundaryFrameGroup(group).topoTypeID() == typeid(Line) )
      this->template createBoundaryFrameGroup<XFieldType,Line>(xfld, group, order, category);
    else
      SANS_DEVELOPER_EXCEPTION( "Field_DG_BoundaryFrame<PhysDim, TopoDim, T>::Field_DG_BoundaryFrame: Unknown element topology" );
  }

  // allocate the solution DOF array and assign it to groups
  this->createDOFs();
}

}

#endif  // FIELD_DG_BOUNDARYFRAME_H
