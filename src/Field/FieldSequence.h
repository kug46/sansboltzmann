// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDSEQUENCE_H
#define FIELDSEQUENCE_H

#include <algorithm> // rotate

#include "Field.h"
#include "XField.h"

#include "FieldAssociatvitySequence.h"

namespace SANS
{

template<template<class,class,class> class FieldBuilder>
class FieldConstructor {};

template<class TopoDim>
struct FieldSequenceGroupConstructor;


//----------------------------------------------------------------------------//
// A class for managing a sequence of fields
//----------------------------------------------------------------------------//

template<class PhysDim, class TopoDim, class T, template<class,class,class> class BaseField >
class FieldSequence : public FieldType< FieldSequence<PhysDim,TopoDim,T,BaseField> >
{
public:
  typedef BaseField<PhysDim, TopoDim, T> FieldType;
  typedef XField<PhysDim, TopoDim> XFieldType;

  static const int D = PhysDim::D;

  friend struct FieldSequenceGroupConstructor<TopoDim>;
protected:
  // Group types for a single field
  typedef typename FieldType::FieldTraceGroupBase SingleFieldTraceGroupBase;
  typedef typename FieldType::FieldCellGroupBase SingleFieldCellGroupBase;

  template<class Topology>
  using SingleFieldTraceGroupType = typename FieldType::template FieldTraceGroupType<Topology>;

  template<class Topology>
  using SingleFieldCellGroupType = typename FieldType::template FieldCellGroupType<Topology>;

public:

  typedef FieldAssociativitySequenceBase FieldTraceGroupBase;
  typedef FieldAssociativitySequenceBase FieldCellGroupBase;

  template<class Topology>
  using FieldTraceGroupType = FieldAssociativitySequence< SingleFieldTraceGroupType<Topology> >;

  template<class Topology>
  using FieldCellGroupType = FieldAssociativitySequence< SingleFieldCellGroupType<Topology> >;

  // Constructs the series of fields based on the FieldConstructor, i.e. Field_DG*, Field_CG*
  template<template<class,class,class> class FieldBuilder, class... Args>
  FieldSequence(const int nflds, const FieldConstructor<FieldBuilder>, const XFieldType& xfld, Args&&... args);
  FieldSequence(const int nflds, const FieldType& fld);
  FieldSequence(const FieldSequence& fldseq);

  ~FieldSequence();

  int nFields() const { return nFields_; }

        FieldType& operator[](const int i)       { return *flds_[i]; }
  const FieldType& operator[](const int i) const { return *flds_[i]; }

  T operator=(const T v)
  {
    for (int i = 0; i < nFields_; i++)
      *flds_[i] = v;
    return v;
  }

  // These should be the same for all fields, so just return the first one
  int nElem()         const { return flds_[0]->nElem();         }
  int nDOF()          const { return flds_[0]->nDOF();          }
  int nDOFpossessed() const { return flds_[0]->nDOFpossessed(); }
  int nDOFghost()     const { return flds_[0]->nDOFghost();     }

  int nInteriorTraceGroups() const { return flds_[0]->nInteriorTraceGroups(); }
  int nBoundaryTraceGroups() const { return flds_[0]->nBoundaryTraceGroups(); }
  int nCellGroups() const { return flds_[0]->nCellGroups(); }

  const XField<PhysDim, TopoDim>& getXField() const { return xfld_; }

  // Rotates the last field so it's first. i.e. {fld0, fld1, fld2, fld3} -> {fld3, fld0, fld1, fld2}
  void rotate();

  // group accessors
  FieldTraceGroupBase& getInteriorTraceGroupBase( const int group )
  {
    SANS_ASSERT_MSG(group >= 0 && group < nInteriorTraceGroups_, "nInteriorTraceGroups_ = %d, group = %d", nInteriorTraceGroups_, group);
    SANS_ASSERT( interiorTraceGroups_[group] != NULL );
    return *interiorTraceGroups_[group];
  }
  const FieldTraceGroupBase& getInteriorTraceGroupBase( const int group ) const
  {
    SANS_ASSERT_MSG(group >= 0 && group < nInteriorTraceGroups_, "nInteriorTraceGroups_ = %d, group = %d", nInteriorTraceGroups_, group);
    SANS_ASSERT( interiorTraceGroups_[group] != NULL );
    return *interiorTraceGroups_[group];
  }

  FieldTraceGroupBase& getBoundaryTraceGroupBase( const int group )
  {
    SANS_ASSERT_MSG(group >= 0 && group < nBoundaryTraceGroups_, "nBoundaryTraceGroups_ = %d, group = %d", nBoundaryTraceGroups_, group);
    SANS_ASSERT( boundaryTraceGroups_[group] != NULL );
    return *boundaryTraceGroups_[group];
  }
  const FieldTraceGroupBase& getBoundaryTraceGroupBase( const int group ) const
  {
    SANS_ASSERT_MSG(group >= 0 && group < nBoundaryTraceGroups_, "nBoundaryTraceGroups_ = %d, group = %d", nBoundaryTraceGroups_, group);
    SANS_ASSERT( boundaryTraceGroups_[group] != NULL );
    return *boundaryTraceGroups_[group];
  }

  FieldCellGroupBase& getCellGroupBase( const int group )
  {
    SANS_ASSERT_MSG(group >= 0 && group < nCellGroups_, "nCellGroups_ = %d, group = %d", nCellGroups_, group);
    SANS_ASSERT( cellGroups_[group] != NULL );
    return *cellGroups_[group];
  }
  const FieldCellGroupBase& getCellGroupBase( const int group ) const
  {
    SANS_ASSERT_MSG(group >= 0 && group < nCellGroups_, "nCellGroups_ = %d, group = %d", nCellGroups_, group);
    SANS_ASSERT( cellGroups_[group] != NULL );
    return *cellGroups_[group];
  }

  template<class Topology>
        FieldTraceGroupType<Topology>& getInteriorTraceGroup( const int group );
  template<class Topology>
  const FieldTraceGroupType<Topology>& getInteriorTraceGroup( const int group ) const;

  template<class Topology>
        FieldTraceGroupType<Topology>& getBoundaryTraceGroup( const int group );
  template<class Topology>
  const FieldTraceGroupType<Topology>& getBoundaryTraceGroup( const int group ) const;

  template<class Topology>
        FieldCellGroupType<Topology>& getCellGroup( const int group );
  template<class Topology>
  const FieldCellGroupType<Topology>& getCellGroup( const int group ) const;

  // global index group accessors
  FieldTraceGroupBase& getInteriorTraceGroupBaseGlobal( const int groupGlobal );
  const FieldTraceGroupBase& getInteriorTraceGroupBaseGlobal( const int groupGlobal ) const;

  FieldTraceGroupBase& getBoundaryTraceGroupBaseGlobal( const int groupGlobal );
  const FieldTraceGroupBase& getBoundaryTraceGroupBaseGlobal( const int groupGlobal ) const;

  FieldCellGroupBase& getCellGroupBaseGlobal( const int groupGlobal );
  const FieldCellGroupBase& getCellGroupBaseGlobal( const int groupGlobal ) const;

  template<class Topology>
        FieldTraceGroupType<Topology>& getInteriorTraceGroupGlobal( const int groupGlobal );
  template<class Topology>
  const FieldTraceGroupType<Topology>& getInteriorTraceGroupGlobal( const int groupGlobal ) const;

  template<class Topology>
        FieldTraceGroupType<Topology>& getBoundaryTraceGroupGlobal( const int groupGlobal );
  template<class Topology>
  const FieldTraceGroupType<Topology>& getBoundaryTraceGroupGlobal( const int groupGlobal ) const;

  template<class Topology>
        FieldCellGroupType<Topology>& getCellGroupGlobal( const int groupGlobal );
  template<class Topology>
  const FieldCellGroupType<Topology>& getCellGroupGlobal( const int groupGlobal ) const;

protected:
  void createGroups();
  template<class Topology> void createInteriorTraceGroup(const int group);
  template<class Topology> void createBoundaryTraceGroup(const int group);
  template<class Topology> void createCellGroup(const int group);

  void deallocate();

  const XFieldType& xfld_; //grid field used to construct the solution fields

  int nFields_;            // number of fields
  FieldType** flds_;       // vector of fields

  int nInteriorTraceGroups_;
  FieldTraceGroupBase** interiorTraceGroups_;   // groups of interior-trace elements

  int nBoundaryTraceGroups_;
  FieldTraceGroupBase** boundaryTraceGroups_;   // groups of boundary-trace elements

  int nCellGroups_;
  FieldCellGroupBase** cellGroups_;             // groups of cell elements
};

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim, class T, template<class,class,class> class BaseField>
template<template<class,class,class> class FieldBuilder, class... Args>
FieldSequence<PhysDim, TopoDim, T, BaseField>::
FieldSequence(const int nflds, const FieldConstructor<FieldBuilder>,
              const XFieldType& xfld, Args&&... args) :
  xfld_(xfld),
  nFields_(nflds), flds_(NULL),
  nInteriorTraceGroups_(0), interiorTraceGroups_(NULL),
  nBoundaryTraceGroups_(0), boundaryTraceGroups_(NULL),
  nCellGroups_(0), cellGroups_(NULL)
{
  // There needs to be at least one field
  SANS_ASSERT( nFields_ > 0 );

  flds_ = new FieldType*[nFields_];

  for (int i = 0; i < nFields_; i++ )
    flds_[i] = new FieldBuilder<PhysDim, TopoDim, T>(xfld, std::forward<Args>(args)...);

  createGroups();
}

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim, class T, template<class,class,class> class BaseField>
FieldSequence<PhysDim, TopoDim, T, BaseField>::
FieldSequence(const int nflds, const FieldType& fld) :
  xfld_(fld.getXField()),
  nFields_(nflds), flds_(NULL),
  nInteriorTraceGroups_(0), interiorTraceGroups_(NULL),
  nBoundaryTraceGroups_(0), boundaryTraceGroups_(NULL),
  nCellGroups_(0), cellGroups_(NULL)
{
  // There needs to be at least one field
  SANS_ASSERT( nFields_ > 0 );

  flds_ = new FieldType*[nFields_];

  for (int i = 0; i < nFields_; i++ )
    flds_[i] = new Field<PhysDim, TopoDim, T>(fld, FieldCopy());

  createGroups();
}


//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim, class T, template<class,class,class> class BaseField>
FieldSequence<PhysDim, TopoDim, T, BaseField>::FieldSequence(const FieldSequence& fldseq) :
  xfld_(fldseq.xfld_),
  nFields_(fldseq.nFields_), flds_(NULL),
  nInteriorTraceGroups_(0), interiorTraceGroups_(NULL),
  nBoundaryTraceGroups_(0), boundaryTraceGroups_(NULL),
  nCellGroups_(0), cellGroups_(NULL)
{
  // There needs to be at least one field
  SANS_ASSERT( nFields_ > 0 );

  flds_ = new FieldType*[nFields_];

  for (int i = 0; i < nFields_; i++ )
    flds_[i] = new FieldType(*fldseq.flds_[i], FieldCopy());

  createGroups();
}

//----------------------------------------------------------------------------//
template<>
struct FieldSequenceGroupConstructor<TopoD1>
{
  template<class PhysDim, class T, template<class,class,class> class BaseField>
  static void createGroups(FieldSequence<PhysDim, TopoD1, T, BaseField>& flds)
  {

    // loop interior trace groups
    for (int group = 0; group < flds.nInteriorTraceGroups_; group++)
    {
      if ( flds.flds_[0]->getInteriorTraceGroupBase(group).topoTypeID() == typeid(Node) )
      {
        flds.template createInteriorTraceGroup<Node>(group);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown topology");
    }

    // loop boundary trace groups
    for (int group = 0; group < flds.nBoundaryTraceGroups_; group++)
    {
      if ( flds.flds_[0]->getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Node) )
      {
        flds.template createBoundaryTraceGroup<Node>(group);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown topology");
    }

    // loop over cell groups
    for (int group = 0; group < flds.nCellGroups_; group++)
    {
      if ( flds.flds_[0]->getCellGroupBase(group).topoTypeID() == typeid(Line) )
      {
        flds.template createCellGroup<Line>(group);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown topology");
    }
  }
};

//----------------------------------------------------------------------------//
template<>
struct FieldSequenceGroupConstructor<TopoD2>
{
  template<class PhysDim, class T, template<class,class,class> class BaseField>
  static void createGroups(FieldSequence<PhysDim, TopoD2, T, BaseField>& flds)
  {

    // loop interior trace groups
    for (int group = 0; group < flds.nInteriorTraceGroups_; group++)
    {
      if ( flds.flds_[0]->getInteriorTraceGroupBase(group).topoTypeID() == typeid(Line) )
      {
        flds.template createInteriorTraceGroup<Line>(group);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown topology");
    }

    // loop boundary trace groups
    for (int group = 0; group < flds.nBoundaryTraceGroups_; group++)
    {
      if ( flds.flds_[0]->getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Line) )
      {
        flds.template createBoundaryTraceGroup<Line>(group);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown topology");
    }

    // loop over cell groups
    for (int group = 0; group < flds.nCellGroups_; group++)
    {
      if (      flds.flds_[0]->getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
      {
        flds.template createCellGroup<Triangle>(group);
      }
      else if ( flds.flds_[0]->getCellGroupBase(group).topoTypeID() == typeid(Quad) )
      {
        flds.template createCellGroup<Quad>(group);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown topology");
    }
  }
};

//----------------------------------------------------------------------------//
template<>
struct FieldSequenceGroupConstructor<TopoD3>
{
  template<class PhysDim, class T, template<class,class,class> class BaseField>
  static void createGroups(FieldSequence<PhysDim, TopoD3, T, BaseField>& flds)
  {

    // loop interior trace groups
    for (int group = 0; group < flds.nInteriorTraceGroups_; group++)
    {
      if (      flds.flds_[0]->getInteriorTraceGroupBase(group).topoTypeID() == typeid(Triangle) )
      {
        flds.template createInteriorTraceGroup<Triangle>(group);
      }
      else if ( flds.flds_[0]->getInteriorTraceGroupBase(group).topoTypeID() == typeid(Quad) )
      {
        flds.template createInteriorTraceGroup<Quad>(group);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown topology");
    }

    // loop boundary trace groups
    for (int group = 0; group < flds.nBoundaryTraceGroups_; group++)
    {
      if (      flds.flds_[0]->getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Triangle) )
      {
        flds.template createBoundaryTraceGroup<Triangle>(group);
      }
      else if ( flds.flds_[0]->getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Quad) )
      {
        flds.template createBoundaryTraceGroup<Quad>(group);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown topology");
    }

    // loop over cell groups
    for (int group = 0; group < flds.nCellGroups_; group++)
    {
      if (      flds.flds_[0]->getCellGroupBase(group).topoTypeID() == typeid(Tet) )
      {
        flds.template createCellGroup<Tet>(group);
      }
      else if ( flds.flds_[0]->getCellGroupBase(group).topoTypeID() == typeid(Hex) )
      {
        flds.template createCellGroup<Hex>(group);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown topology");
    }
  }
};


//----------------------------------------------------------------------------//
template<>
struct FieldSequenceGroupConstructor<TopoD4>
{
  template<class PhysDim, class T, template<class,class,class> class BaseField>
  static void createGroups(FieldSequence<PhysDim, TopoD4, T, BaseField>& flds)
  {

    // loop interior trace groups
    for (int group = 0; group < flds.nInteriorTraceGroups_; group++)
    {
      if ( flds.flds_[0]->getInteriorTraceGroupBase(group).topoTypeID() == typeid(Tet) )
      {
        flds.template createInteriorTraceGroup<Tet>(group);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown topology");
    }

    // loop boundary trace groups
    for (int group = 0; group < flds.nBoundaryTraceGroups_; group++)
    {
      if ( flds.flds_[0]->getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Tet) )
      {
        flds.template createBoundaryTraceGroup<Tet>(group);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown topology");
    }

    // loop over cell groups
    for (int group = 0; group < flds.nCellGroups_; group++)
    {
      if ( flds.flds_[0]->getCellGroupBase(group).topoTypeID() == typeid(Pentatope) )
      {
        flds.template createCellGroup<Pentatope>(group);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown topology");
    }
  }
};

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim, class T, template<class,class,class> class BaseField>
void
FieldSequence<PhysDim, TopoDim, T, BaseField>::rotate()
{
  // Rotate all the pointers
  std::rotate(flds_, flds_ +nFields_-1, flds_+nFields_);

  for (int n = 0; n < nInteriorTraceGroups_; n++)
    interiorTraceGroups_[n]->rotate();
  for (int n = 0; n < nBoundaryTraceGroups_; n++)
    boundaryTraceGroups_[n]->rotate();
  for (int n = 0; n < nCellGroups_; n++)
    cellGroups_[n]->rotate();
}

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim, class T, template<class,class,class> class BaseField>
void
FieldSequence<PhysDim, TopoDim, T, BaseField>::createGroups()
{
  // Save of sizes and initialize arrays of pointers
  nInteriorTraceGroups_ = flds_[0]->nInteriorTraceGroups();
  nBoundaryTraceGroups_ = flds_[0]->nBoundaryTraceGroups();
  nCellGroups_          = flds_[0]->nCellGroups();

  interiorTraceGroups_ = new FieldTraceGroupBase*[nInteriorTraceGroups_];
  boundaryTraceGroups_ = new FieldTraceGroupBase*[nBoundaryTraceGroups_];
  cellGroups_          = new FieldCellGroupBase*[nCellGroups_];

  FieldSequenceGroupConstructor<TopoDim>::createGroups(*this);
}

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim, class T, template<class,class,class> class BaseField>
template<class Topology>
void
FieldSequence<PhysDim, TopoDim, T, BaseField>::createInteriorTraceGroup(const int group)
{
  typedef SingleFieldTraceGroupType<Topology> SingleFieldTraceGroupClass;
  typedef FieldTraceGroupType<Topology> FieldTraceGroupClass;

  std::vector<SingleFieldTraceGroupClass*> interiorTraceGroups(nFields_);

  for (int i = 0; i < nFields_; i++ )
    interiorTraceGroups[i] = &flds_[i]->template getInteriorTraceGroup<Topology>(group);

  interiorTraceGroups_[group] = new FieldTraceGroupClass(flds_[0]->template getInteriorTraceGroup<Topology>(group).basis(),
                                                         interiorTraceGroups);
}

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim, class T, template<class,class,class> class BaseField>
template<class Topology>
void
FieldSequence<PhysDim, TopoDim, T, BaseField>::createBoundaryTraceGroup(const int group)
{
  typedef SingleFieldTraceGroupType<Topology> SingleFieldTraceGroupClass;
  typedef FieldTraceGroupType<Topology> FieldTraceGroupClass;

  std::vector<SingleFieldTraceGroupClass*> boundaryTraceGroups(nFields_);

  for (int i = 0; i < nFields_; i++ )
    boundaryTraceGroups[i] = &flds_[i]->template getBoundaryTraceGroup<Topology>(group);

  boundaryTraceGroups_[group] = new FieldTraceGroupClass(flds_[0]->template getBoundaryTraceGroup<Topology>(group).basis(),
                                                         boundaryTraceGroups);
}

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim, class T, template<class,class,class> class BaseField>
template<class Topology>
void
FieldSequence<PhysDim, TopoDim, T, BaseField>::createCellGroup(const int group)
{
  typedef SingleFieldCellGroupType<Topology> SingleFieldCellGroupClass;
  typedef FieldCellGroupType<Topology> FieldCellGroupClass;

  std::vector<SingleFieldCellGroupClass*> cellGroups(nFields_);

  for (int i = 0; i < nFields_; i++ )
    cellGroups[i] = &flds_[i]->template getCellGroup<Topology>(group);

  cellGroups_[group] = new FieldCellGroupClass(flds_[0]->template getCellGroup<Topology>(group).basis(),
                                               cellGroups);
}

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim, class T, template<class,class,class> class BaseField>
FieldSequence<PhysDim, TopoDim, T, BaseField>::~FieldSequence()
{
  deallocate();
}

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim, class T, template<class,class,class> class BaseField>
void
FieldSequence<PhysDim, TopoDim, T, BaseField>::deallocate()
{
  for (int i = 0; i < nFields_; i++ )
    delete flds_[i];

  for (int i = 0; i < nInteriorTraceGroups_; i++ )
    delete interiorTraceGroups_[i];

  for (int i = 0; i < nBoundaryTraceGroups_; i++ )
    delete boundaryTraceGroups_[i];

  for (int i = 0; i < nCellGroups_; i++ )
    delete cellGroups_[i];

  delete [] flds_;                flds_ = NULL;
  delete [] interiorTraceGroups_; interiorTraceGroups_ = NULL;
  delete [] boundaryTraceGroups_; boundaryTraceGroups_ = NULL;
  delete [] cellGroups_;          cellGroups_ = NULL;
}

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim, class T, template<class,class,class> class BaseField>
template<class Topology>
typename FieldSequence<PhysDim, TopoDim, T, BaseField>::template FieldTraceGroupType<Topology>&
FieldSequence<PhysDim, TopoDim, T, BaseField>::getInteriorTraceGroup( const int group )
{
  typedef FieldTraceGroupType<Topology> FieldTraceGroupClass;
  SANS_ASSERT( group >=0 && group < nInteriorTraceGroups_ );
  SANS_ASSERT( interiorTraceGroups_[group] != NULL );
  SANS_ASSERT( interiorTraceGroups_[group]->topoTypeID() == typeid(Topology) );
  return static_cast<FieldTraceGroupClass&>( *interiorTraceGroups_[group] );
}

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim, class T, template<class,class,class> class BaseField>
template<class Topology>
const typename FieldSequence<PhysDim, TopoDim, T, BaseField>::template FieldTraceGroupType<Topology>&
FieldSequence<PhysDim, TopoDim, T, BaseField>::getInteriorTraceGroup( const int group ) const
{
  typedef FieldTraceGroupType<Topology> FieldTraceGroupClass;
  SANS_ASSERT( group >=0 && group < nInteriorTraceGroups_ );
  SANS_ASSERT( interiorTraceGroups_[group] != NULL );
  SANS_ASSERT( interiorTraceGroups_[group]->topoTypeID() == typeid(Topology) );
  return static_cast<const FieldTraceGroupClass&>( *interiorTraceGroups_[group] );
}


//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim, class T, template<class,class,class> class BaseField>
template<class Topology>
typename FieldSequence<PhysDim, TopoDim, T, BaseField>::template FieldTraceGroupType<Topology>&
FieldSequence<PhysDim, TopoDim, T, BaseField>::getBoundaryTraceGroup( const int group )
{
  typedef FieldTraceGroupType<Topology> FieldTraceGroupClass;
  SANS_ASSERT( group >=0 && group < nBoundaryTraceGroups_ );
  SANS_ASSERT( boundaryTraceGroups_[group] != NULL );
  SANS_ASSERT( boundaryTraceGroups_[group]->topoTypeID() == typeid(Topology) );
  return static_cast<FieldTraceGroupClass&>( *boundaryTraceGroups_[group] );
}

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim, class T, template<class,class,class> class BaseField>
template<class Topology>
const typename FieldSequence<PhysDim, TopoDim, T, BaseField>::template FieldTraceGroupType<Topology>&
FieldSequence<PhysDim, TopoDim, T, BaseField>::getBoundaryTraceGroup( const int group ) const
{
  typedef FieldTraceGroupType<Topology> FieldTraceGroupClass;
  SANS_ASSERT( group >=0 && group < nBoundaryTraceGroups_ );
  SANS_ASSERT( boundaryTraceGroups_[group] != NULL );
  SANS_ASSERT( boundaryTraceGroups_[group]->topoTypeID() == typeid(Topology) );
  return static_cast<const FieldTraceGroupClass&>( *boundaryTraceGroups_[group] );
}


//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim, class T, template<class,class,class> class BaseField>
template<class Topology>
typename FieldSequence<PhysDim, TopoDim, T, BaseField>::template FieldCellGroupType<Topology>&
FieldSequence<PhysDim, TopoDim, T, BaseField>::getCellGroup( const int group )
{
  typedef FieldCellGroupType<Topology> FieldCellGroupClass;
  SANS_ASSERT( group >=0 && group < nCellGroups_ );
  SANS_ASSERT( cellGroups_[group] != NULL );
  SANS_ASSERT( cellGroups_[group]->topoTypeID() == typeid(Topology) );
  return static_cast<FieldCellGroupClass&>( *cellGroups_[group] );
}

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim, class T, template<class,class,class> class BaseField>
template<class Topology>
const typename FieldSequence<PhysDim, TopoDim, T, BaseField>::template FieldCellGroupType<Topology>&
FieldSequence<PhysDim, TopoDim, T, BaseField>::getCellGroup( const int group ) const
{
  typedef FieldCellGroupType<Topology> FieldCellGroupClass;
  SANS_ASSERT_MSG( group >=0 && group < nCellGroups_, "group = %d, nCellGroups_ = %d", group, nCellGroups_ );
  SANS_ASSERT( cellGroups_[group] != NULL );
  SANS_ASSERT( cellGroups_[group]->topoTypeID() == typeid(Topology) );
  return static_cast<const FieldCellGroupClass&>( *cellGroups_[group] );
}



//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim, class T, template<class,class,class> class BaseField>
typename FieldSequence<PhysDim, TopoDim, T, BaseField>::FieldTraceGroupBase&
FieldSequence<PhysDim, TopoDim, T, BaseField>::getInteriorTraceGroupBaseGlobal( const int groupGlobal )
{
  int groupLocal = flds_[0]->getLocalInteriorTraceGroupMap(groupGlobal);
  SANS_ASSERT( groupLocal >= 0 );
  return getInteriorTraceGroupBase(groupLocal);
}
template<class PhysDim, class TopoDim, class T, template<class,class,class> class BaseField>
const typename FieldSequence<PhysDim, TopoDim, T, BaseField>::FieldTraceGroupBase&
FieldSequence<PhysDim, TopoDim, T, BaseField>::getInteriorTraceGroupBaseGlobal( const int groupGlobal ) const
{
  int groupLocal = flds_[0]->getLocalInteriorTraceGroupMap(groupGlobal);
  SANS_ASSERT( groupLocal >= 0 );
  return getInteriorTraceGroupBase(groupLocal);
}

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim, class T, template<class,class,class> class BaseField>
typename FieldSequence<PhysDim, TopoDim, T, BaseField>::FieldTraceGroupBase&
FieldSequence<PhysDim, TopoDim, T, BaseField>::getBoundaryTraceGroupBaseGlobal( const int groupGlobal )
{
  int groupLocal = flds_[0]->getLocalBoundaryTraceGroupMap(groupGlobal);
  SANS_ASSERT( groupLocal >= 0 );
  return getBoundaryTraceGroupBase(groupLocal);
}
template<class PhysDim, class TopoDim, class T, template<class,class,class> class BaseField>
const typename FieldSequence<PhysDim, TopoDim, T, BaseField>::FieldTraceGroupBase&
FieldSequence<PhysDim, TopoDim, T, BaseField>::getBoundaryTraceGroupBaseGlobal( const int groupGlobal ) const
{
  int groupLocal = flds_[0]->getLocalBoundaryTraceGroupMap(groupGlobal);
  SANS_ASSERT( groupLocal >= 0 );
  return getBoundaryTraceGroupBase(groupLocal);
}

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim, class T, template<class,class,class> class BaseField>
typename FieldSequence<PhysDim, TopoDim, T, BaseField>::FieldCellGroupBase&
FieldSequence<PhysDim, TopoDim, T, BaseField>::getCellGroupBaseGlobal( const int groupGlobal )
{
  int groupLocal = flds_[0]->getLocalCellGroupMap(groupGlobal);
  SANS_ASSERT( groupLocal >= 0 );
  return getCellGroupBase(groupLocal);
}
template<class PhysDim, class TopoDim, class T, template<class,class,class> class BaseField>
const typename FieldSequence<PhysDim, TopoDim, T, BaseField>::FieldCellGroupBase&
FieldSequence<PhysDim, TopoDim, T, BaseField>::getCellGroupBaseGlobal( const int groupGlobal ) const
{
  int groupLocal = flds_[0]->getLocalCellGroupMap(groupGlobal);
  SANS_ASSERT( groupLocal >= 0 );
  return getCellGroupBase(groupLocal);
}



//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim, class T, template<class,class,class> class BaseField>
template<class Topology>
typename FieldSequence<PhysDim, TopoDim, T, BaseField>::template FieldTraceGroupType<Topology>&
FieldSequence<PhysDim, TopoDim, T, BaseField>::getInteriorTraceGroupGlobal( const int groupGlobal )
{
  int groupLocal = flds_[0]->getLocalInteriorTraceGroupMap(groupGlobal);
  SANS_ASSERT( groupLocal >= 0 );
  return this->template getInteriorTraceGroup<Topology>(groupLocal);
}
template<class PhysDim, class TopoDim, class T, template<class,class,class> class BaseField>
template<class Topology>
const typename FieldSequence<PhysDim, TopoDim, T, BaseField>::template FieldTraceGroupType<Topology>&
FieldSequence<PhysDim, TopoDim, T, BaseField>::getInteriorTraceGroupGlobal( const int groupGlobal ) const
{
  int groupLocal = flds_[0]->getLocalInteriorTraceGroupMap(groupGlobal);
  SANS_ASSERT( groupLocal >= 0 );
  return this->template getInteriorTraceGroup<Topology>(groupLocal);
}

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim, class T, template<class,class,class> class BaseField>
template<class Topology>
typename FieldSequence<PhysDim, TopoDim, T, BaseField>::template FieldTraceGroupType<Topology>&
FieldSequence<PhysDim, TopoDim, T, BaseField>::getBoundaryTraceGroupGlobal( const int groupGlobal )
{
  int groupLocal = flds_[0]->getLocalBoundaryTraceGroupMap(groupGlobal);
  SANS_ASSERT( groupLocal >= 0 );
  return this->template getBoundaryTraceGroup<Topology>(groupLocal);
}
template<class PhysDim, class TopoDim, class T, template<class,class,class> class BaseField>
template<class Topology>
const typename FieldSequence<PhysDim, TopoDim, T, BaseField>::template FieldTraceGroupType<Topology>&
FieldSequence<PhysDim, TopoDim, T, BaseField>::getBoundaryTraceGroupGlobal( const int groupGlobal ) const
{
  int groupLocal = flds_[0]->getLocalBoundaryTraceGroupMap(groupGlobal);
  SANS_ASSERT( groupLocal >= 0 );
  return this->template getBoundaryTraceGroup<Topology>(groupLocal);
}

//----------------------------------------------------------------------------//
template<class PhysDim, class TopoDim, class T, template<class,class,class> class BaseField>
template<class Topology>
typename FieldSequence<PhysDim, TopoDim, T, BaseField>::template FieldCellGroupType<Topology>&
FieldSequence<PhysDim, TopoDim, T, BaseField>::getCellGroupGlobal( const int groupGlobal )
{
  int groupLocal = flds_[0]->getLocalCellGroupMap(groupGlobal);
  SANS_ASSERT( groupLocal >= 0 );
  return this->template getCellGroup<Topology>(groupLocal);
}
template<class PhysDim, class TopoDim, class T, template<class,class,class> class BaseField>
template<class Topology>
const typename FieldSequence<PhysDim, TopoDim, T, BaseField>::template FieldCellGroupType<Topology>&
FieldSequence<PhysDim, TopoDim, T, BaseField>::getCellGroupGlobal( const int groupGlobal ) const
{
  int groupLocal = flds_[0]->getLocalCellGroupMap(groupGlobal);
  SANS_ASSERT( groupLocal >= 0 );
  return this->template getCellGroup<Topology>(groupLocal);
}

}

#endif //FIELDSEQUENCE_H
