// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(XFIELDLINE_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include <sstream>

#include "XFieldLine.h"

namespace SANS
{

//=============================================================================
namespace XField_
{
//-----------------------------------------------------------------------------
// Specialization for 2D grid field line element
template<>
template<class TopologyTrace, class TopologyL, class TopologyR>
void
checkTraceNormalStruct<PhysD2,TopoD1>::
checkTraceNormal( const XFieldType& xfld,
                  const typename XFieldType::template FieldTraceGroupType<TopologyTrace>& traceGroup, int elem,
                  typename XFieldType::template FieldTraceGroupType<TopologyTrace>::template ElementType<>& elemTrace,
                  const typename XFieldType::template FieldCellGroupType<TopologyL>::template ElementType<>& elemCellL,
                  const typename XFieldType::template FieldCellGroupType<TopologyR>::template ElementType<>& elemCellR)
{
  // Always check trace normal of the left element
  xfld.checkTraceNormalL<TopologyTrace, TopologyL>(traceGroup, elem, elemTrace, elemCellL);

  // Only check trace normal of the right element if it exists
  if ( elemTrace.normalSignR() != 0 )
  {
    xfld.checkTraceNormalR<TopologyTrace, TopologyR>(traceGroup, elem, elemTrace, elemCellR);
  }
}

//-----------------------------------------------------------------------------
// Specialization for 1D grid field line element
template<>
template<class TopologyTrace, class TopologyL, class TopologyR>
void
checkTraceNormalStruct<PhysD1,TopoD1>::
checkTraceNormal( const XFieldType& xfld,
                  const typename XFieldType::template FieldTraceGroupType<TopologyTrace>& traceGroup, int elem,
                  typename XFieldType::template FieldTraceGroupType<TopologyTrace>::template ElementType<>& elemTrace,
                  const typename XFieldType::template FieldCellGroupType<TopologyL>::template ElementType<>& elemCellL,
                  const typename XFieldType::template FieldCellGroupType<TopologyR>::template ElementType<>& elemCellR)
{
  xfld.checkTraceNormal<TopologyTrace, TopologyL>(traceGroup, elem, elemTrace, elemCellL);
}

//-----------------------------------------------------------------------------
template<class PhysDim, class TopoDim, class TopologyTrace, class TopologyL>
void
check_RightTopology<PhysDim, TopoDim, TopologyTrace, TopologyL>::
interiorTrace( const XFieldType& xfld, const int itraceGroup,
               const typename XFieldType::template FieldTraceGroupType<TopologyTrace>& traceGroup,
               std::vector< std::vector<int> >& cellCount )
{
  // determine topology for Right cell group
  const int groupR = traceGroup.getGroupRight();

  XFIELD_CHECK( groupR >= 0 && groupR < xfld.nCellGroups(), "groupR = %d, this->nCellGroups() = %d ", groupR, xfld.nCellGroups() );

  // dispatch check over elements of group
  if ( xfld.getCellGroupBase(groupR).topoTypeID() == typeid(Line) )
  {
    xfld.template checkInteriorTrace<TopologyTrace, TopologyL, Line>( itraceGroup, traceGroup, cellCount );
  }
  else
    SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
}

//-----------------------------------------------------------------------------
template<class PhysDim, class TopoDim, class TopologyTrace, class TopologyL>
void
check_RightTopology<PhysDim, TopoDim, TopologyTrace, TopologyL>::
boundaryTrace( const XFieldType& xfld, const int itraceGroup,
               const typename XFieldType::template FieldTraceGroupType<TopologyTrace>& traceGroup,
               std::vector< std::vector<int> >& cellCount )
{
  // determine topology for Right cell group
  const int groupR = traceGroup.getGroupRight();

  XFIELD_CHECK( groupR >= 0 && groupR < xfld.nCellGroups(), "groupR = %d, this->nCellGroups() = %d ", groupR, xfld.nCellGroups() );

  // dispatch check over elements of group
  if ( xfld.getCellGroupBase(groupR).topoTypeID() == typeid(Line) )
  {
    xfld.template checkBoundaryTraceConnected<TopologyTrace, TopologyL, Line>( itraceGroup, traceGroup, cellCount );
  }
  else
    SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
}

//-----------------------------------------------------------------------------
template<class PhysDim, class TopoDim, class TopologyTrace>
void
check_LeftTopology<PhysDim, TopoDim, TopologyTrace>::
interiorTrace( const XFieldType& xfld, const int itraceGroup,
               const typename XFieldType::template FieldTraceGroupType<TopologyTrace>& traceGroup,
               std::vector< std::vector<int> >& cellCount )
{
  // determine topology for Left cell group
  const int groupL = traceGroup.getGroupLeft();

  XFIELD_CHECK( groupL >= 0 && groupL < xfld.nCellGroups(), "groupL = %d, this->nCellGroups() = %d ", groupL, xfld.nCellGroups() );

  // determine topology for R
  if ( xfld.getCellGroupBase(groupL).topoTypeID() == typeid(Line) )
  {
    check_RightTopology<PhysDim, TopoDim, TopologyTrace, Line>::interiorTrace( xfld, itraceGroup, traceGroup, cellCount );
  }
  else
    SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
}

//-----------------------------------------------------------------------------
template<class PhysDim, class TopoDim, class TopologyTrace>
void
check_LeftTopology<PhysDim, TopoDim, TopologyTrace>::
boundaryTrace( const XFieldType& xfld, const int itraceGroup,
               const typename XFieldType::template FieldTraceGroupType<TopologyTrace>& traceGroup,
               std::vector< std::vector<int> >& cellCount )
{
  // determine topology for Left cell group
  const int groupL = traceGroup.getGroupLeft();
  const int groupR = traceGroup.getGroupRight();

  XFIELD_CHECK( groupL >= 0 && groupL < xfld.nCellGroups(), "groupL = %d, this->nCellGroups() = %d ", groupL, xfld.nCellGroups() );

  if ( xfld.getCellGroupBase(groupL).topoTypeID() == typeid(Line) )
  {
    if ( groupR < 0 || traceGroup.getGroupRightType() == eHubTraceGroup )  // when there is NO cell group to the right
      // dispatch check over elements of group
      xfld.template checkBoundaryTrace<TopologyTrace, Line>( itraceGroup, traceGroup, cellCount );
    else               // when there is a cell group to the right
      check_RightTopology<PhysDim, TopoDim, TopologyTrace, Line>::boundaryTrace( xfld, itraceGroup, traceGroup, cellCount );
  }
  else
    SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
}

} //namespace XField_


//=============================================================================
template<class PhysDim, class TopoDim>
void
XField< PhysDim, TopoDim >::checkGrid()
{
  initDefaultElmentIDs();

  std::vector< std::vector<int> > cellCount(this->nCellGroups());

  int nCellElem = 0;

  // loop over element groups
  for (int group = 0; group < this->nCellGroups(); group++)
  {
    nCellElem += this->getCellGroupBase(group).nElem();
    cellCount[group].resize( this->getCellGroupBase(group).nElem() );

    // dispatch integration over elements of group
    if ( this->getCellGroupBase(group).topoTypeID() == typeid(Line) )
    {
      checkPositiveJacobianDeterminant<Line>( this->template getCellGroup<Line>(group) );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
  }

  // loop over interior trace element groups
  for (int group = 0; group < this->nInteriorTraceGroups(); group++)
  {
    if ( this->getInteriorTraceGroupBase(group).topoTypeID() == typeid(Node) )
    {
      XField_::check_LeftTopology<PhysDim, TopoDim, Node>::
          interiorTrace( *this, group, this->template getInteriorTraceGroup<Node>(group), cellCount );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown trace topology" );
  }

  // loop over boundary trace element groups
  for (int group = 0; group < this->nBoundaryTraceGroups(); group++)
  {
    if ( this->getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Node) )
    {
      XField_::check_LeftTopology<PhysDim, TopoDim, Node>::
          boundaryTrace( *this, group, this->template getBoundaryTraceGroup<Node>(group), cellCount );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown trace topology" );
  }

  // loop over ghost boundary trace element groups
  for (int group = 0; group < ghostBoundaryTraceGroups_.size(); group++)
  {
    SANS_ASSERT( ghostBoundaryTraceGroups_[group] != NULL );
    if ( ghostBoundaryTraceGroups_[group]->topoTypeID() == typeid(Node) )
    {
      XField_::check_LeftTopology<PhysDim, TopoDim, Node>::
          boundaryTrace( *this, group, this->template getGhostBoundaryTraceGroup<Node>(group), cellCount );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown trace topology" );
  }

  // loop over element groups and check the cell counts
  for (int group = 0; group < this->nCellGroups(); group++)
  {
    const int nelem = this->getCellGroupBase(group).nElem();

    // Check that each cell is touched the same number of times as they have edges
    if ( this->getCellGroupBase(group).topoTypeID() == typeid(Line) )
    {
      for (int elem = 0; elem < nelem; elem++)
        XFIELD_CHECK( cellCount[group][elem] == Line::NNode, "cellCount[%d][%d] = %d", group, elem, cellCount[group][elem] );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
  }

  // Check that the number of elements is set correctly
  XFIELD_CHECK( nCellElem == this->nElem_, "nCellElem = %d, nElem_ = %d", nCellElem, this->nElem_ );
}


//=============================================================================
template<class PhysDim, class TopoDim>
template <class TopologyTrace, class TopologyL, class TopologyR>
void
XField< PhysDim, TopoDim >::
checkInteriorTrace( const int itraceGroup,
                    const typename BaseType::template FieldTraceGroupType<TopologyTrace>& traceGroup,
                    std::vector< std::vector<int> >& cellCount ) const
{
  typedef typename BaseType::template FieldTraceGroupType<TopologyTrace> FieldTraceClass;
  typedef typename BaseType::template FieldCellGroupType<TopologyL> FieldCellLClass;
  typedef typename BaseType::template FieldCellGroupType<TopologyR> FieldCellRClass;

  int nodeMapT[ TopologyTrace::NNode ];
  int nodeMapL[ TopologyL::NNode ];
  int nodeMapR[ TopologyR::NNode ];

  const int groupL = traceGroup.getGroupLeft();
  const int groupR = traceGroup.getGroupRight();

  const int (*TraceNodesL)[ TopologyTrace::NTrace ] = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyL>::TraceNodes;
  const int (*TraceNodesR)[ TopologyTrace::NTrace ] = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyR>::TraceNodes;

  const FieldCellLClass& cellGroupL = this->template getCellGroup<TopologyL>(groupL);
  const FieldCellRClass& cellGroupR = this->template getCellGroup<TopologyR>(groupR);

  typename FieldTraceClass::template ElementType<> elemTrace( traceGroup.basis() );
  typename FieldCellLClass::template ElementType<> elemCellL( cellGroupL.basis() );
  typename FieldCellRClass::template ElementType<> elemCellR( cellGroupR.basis() );

  const int nelem = traceGroup.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    const int cellL                       = traceGroup.getElementLeft( elem );
    const CanonicalTraceToCell canonicalL = traceGroup.getCanonicalTraceLeft( elem );

    const int cellR                       = traceGroup.getElementRight( elem );
    const CanonicalTraceToCell canonicalR = traceGroup.getCanonicalTraceRight( elem );

    XFIELD_CHECK( cellL >= 0 && cellL < cellGroupL.nElem(), "cellL = %d, cellGroupL.nElem() = %d", cellL, cellGroupL.nElem() );
    XFIELD_CHECK( cellR >= 0 && cellR < cellGroupR.nElem(), "cellR = %d, cellGroupR.nElem() = %d", cellR, cellGroupR.nElem() );

    cellCount[groupL][cellL]++;
    cellCount[groupR][cellR]++;

    XFIELD_CHECK( canonicalL.trace >= 0 && canonicalL.trace < TopologyL::NTrace,
                  "canonicalL.trace = %d, TopologyL::NTrace = %d", canonicalL.trace, TopologyL::NTrace );
    XFIELD_CHECK( canonicalR.trace >= 0 && canonicalR.trace < TopologyR::NTrace,
                  "canonicalR.trace = %d, TopologyR::NTrace = %d", canonicalR.trace, TopologyR::NTrace );

    XFIELD_CHECK( canonicalL.orientation == 0, "canonicalL.orientation = %d", canonicalL.orientation );
    XFIELD_CHECK( canonicalR.orientation == 0, "canonicalR.orientation = %d", canonicalR.orientation );

    traceGroup.getElement( elemTrace, elem );
    cellGroupL.getElement( elemCellL, cellL );
    cellGroupR.getElement( elemCellR, cellR );

    traceGroup.associativity( elem ).getNodeGlobalMapping( nodeMapT, TopologyTrace::NNode );
    cellGroupL.associativity( cellL ).getNodeGlobalMapping( nodeMapL, TopologyL::NNode );
    cellGroupR.associativity( cellR ).getNodeGlobalMapping( nodeMapR, TopologyR::NNode );

    // Check the node mappings are consistent
    for (int i = 0; i < TopologyTrace::NNode; i++ )
    {
      bool error = false;

      if (nodeMapL[TraceNodesL[canonicalL.trace][i]] != nodeMapT[i])
        error = true;

      if (nodeMapR[TraceNodesR[canonicalR.trace][0]] != nodeMapT[i])
        error = true;

      if (error)
      {
        // only construct std::stringstream if there is an error.
        // The constructor is very expensive time-wise for local solves
        std::stringstream msg;
        if (nodeMapL[TraceNodesL[canonicalL.trace][i]] != nodeMapT[i])
        {
          msg << std::endl;
          msg << "Interior trace nodes do not match left cell canonical trace nodes." << std::endl;
        }

        if (nodeMapR[TraceNodesR[canonicalR.trace][0]] != nodeMapT[i])
        {
          msg << std::endl;
          msg << "Interior trace nodes do not match right cell canonical trace nodes." << std::endl;
        }

        msg << std::endl;
        msg << "--- Trace ---" << std::endl;
        msg << "Trace Group = " << itraceGroup << std::endl;
        msg << "Group Trace Element number = " << elem << std::endl;
        msg << "Trace nodes = { ";
        for (int n = 0; n < TopologyTrace::NNode-1; n++)
          msg << nodeMapT[n] << ", ";
        msg << nodeMapT[TopologyTrace::NNode-1] << " } " << std::endl;

        msg << std::endl;
        msg << "--- Left Cell ---" << std::endl;
        msg << "Canonical Trace = " << canonicalL.trace << std::endl;
        msg << "Canonical Trace nodes = { ";
        for (int n = 0; n < TopologyTrace::NNode-1; n++)
          msg << nodeMapL[TraceNodesL[canonicalL.trace][n]] << ", ";
        msg << nodeMapL[TraceNodesL[canonicalL.trace][TopologyTrace::NNode-1]] << " } " << std::endl;

        msg << "Element number = " << cellL << std::endl;
        msg << "Nodes = { ";
        for (int n = 0; n < TopologyL::NNode-1; n++)
          msg << nodeMapL[n] << ", ";
        msg << nodeMapL[TopologyL::NNode-1] << " } " << std::endl;

        msg << std::endl;
        msg << "--- Right Cell ---" << std::endl;
        msg << "Canonical Trace = " << canonicalR.trace << std::endl;
        msg << "Canonical Trace nodes = { ";
        msg << nodeMapR[TraceNodesR[canonicalR.trace][0]] << " } " << std::endl;

        msg << "Element number = " << cellR << std::endl;
        msg << "Nodes = { ";
        for (int n = 0; n < TopologyR::NNode-1; n++)
          msg << nodeMapR[n] << ", ";
        msg << nodeMapR[TopologyR::NNode-1] << " } " << std::endl;

        XFIELD_EXCEPTION( msg.str() );
      }
    }

    // Check trace normal vectors
    XField_::template checkTraceNormalStruct<PhysDim,TopoDim>::
    template checkTraceNormal<TopologyTrace, TopologyL, TopologyR>(*this, traceGroup, elem, elemTrace, elemCellL, elemCellR);
  }
}

//=============================================================================
template<class PhysDim, class TopoDim>
template <class TopologyTrace, class TopologyL>
void
XField< PhysDim, TopoDim >::
checkBoundaryTrace( const int itraceGroup,
                    const typename BaseType::template FieldTraceGroupType<TopologyTrace>& traceGroup,
                    std::vector< std::vector<int> >& cellCount ) const
{
  typedef typename BaseType::template FieldTraceGroupType<TopologyTrace> FieldTraceClass;
  typedef typename BaseType::template FieldCellGroupType<TopologyL> FieldCellLClass;

  int nodeMapT[ TopologyTrace::NNode ];
  int nodeMapL[ TopologyL::NNode ];

  const int groupL = traceGroup.getGroupLeft();

  const int (*TraceNodesL)[ TopologyTrace::NTrace ] = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyL>::TraceNodes;

  const FieldCellLClass& cellGroupL = this->template getCellGroup<TopologyL>(groupL);

  typename FieldTraceClass::template ElementType<> elemTrace( traceGroup.basis() );
  typename FieldCellLClass::template ElementType<> elemCellL( cellGroupL.basis() );

  const int nelem = traceGroup.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    const int cellL                       = traceGroup.getElementLeft( elem );
    const CanonicalTraceToCell canonicalL = traceGroup.getCanonicalTraceLeft( elem );

    XFIELD_CHECK( cellL >= 0 && cellL < cellGroupL.nElem(), "cellL = %d, cellGroupL.nElem() = %d", cellL, cellGroupL.nElem() );

    cellCount[groupL][cellL]++;

    XFIELD_CHECK( canonicalL.trace >= 0 && canonicalL.trace < TopologyL::NTrace,
                  "canonicalL.trace = %d, TopologyL::NTrace = %d", canonicalL.trace, TopologyL::NTrace );

    XFIELD_CHECK( canonicalL.orientation == 0,
                  "canonicalL.orientation = %d", canonicalL.orientation );

    traceGroup.getElement( elemTrace, elem );
    cellGroupL.getElement( elemCellL, cellL );

    traceGroup.associativity( elem ).getNodeGlobalMapping( nodeMapT, TopologyTrace::NNode );
    cellGroupL.associativity( cellL ).getNodeGlobalMapping( nodeMapL, TopologyL::NNode );

    // Check the node mappings are consistent
    for (int i = 0; i < TopologyTrace::NNode; i++ )
      if (nodeMapL[TraceNodesL[canonicalL.trace][i]] != nodeMapT[i])
      {
        std::stringstream msg;
        msg << std::endl;
        msg << "Boundary trace nodes do not match left cell canonical trace nodes." << std::endl;

        msg << std::endl;
        msg << "--- Trace ---" << std::endl;
        msg << "Trace Group = " << itraceGroup << std::endl;
        msg << "Group Element number = " << elem << std::endl;
        msg << "Nodes = { ";
        for (int n = 0; n < TopologyTrace::NNode-1; n++)
          msg << nodeMapT[n] << ", ";
        msg << nodeMapT[TopologyTrace::NNode-1] << " } " << std::endl;

        msg << std::endl;
        msg << "--- Left Cell ---" << std::endl;
        msg << "Canonical Trace = " << canonicalL.trace << std::endl;
        msg << "Canonical Trace nodes = { ";
        for (int n = 0; n < TopologyTrace::NNode-1; n++)
          msg << nodeMapL[TraceNodesL[canonicalL.trace][n]] << ", ";
        msg << nodeMapL[TraceNodesL[canonicalL.trace][TopologyTrace::NNode-1]] << " } " << std::endl;

        msg << "Element number = " << cellL << std::endl;
        msg << "Nodes = { ";
        for (int n = 0; n < TopologyL::NNode-1; n++)
          msg << nodeMapL[n] << ", ";
        msg << nodeMapL[TopologyL::NNode-1] << " } " << std::endl;

        XFIELD_EXCEPTION( msg.str() );
      }

    // Check the trace normal vector
    typedef TopologyL TopologyRdummy;
    typename FieldCellLClass::template ElementType<> elemCellRdummy( cellGroupL.basis() );

    XField_::template checkTraceNormalStruct<PhysDim,TopoDim>::
    template checkTraceNormal<TopologyTrace, TopologyL, TopologyRdummy>(*this, traceGroup, elem, elemTrace, elemCellL, elemCellRdummy);
  }
}


//=============================================================================
template<class PhysDim, class TopoDim>
template <class TopologyTrace, class TopologyL, class TopologyR>
void
XField< PhysDim, TopoDim >::
checkBoundaryTraceConnected( const int itraceGroup,
                             const typename BaseType::template FieldTraceGroupType<TopologyTrace>& traceGroup,
                             std::vector< std::vector<int> >& cellCount ) const
{
  typedef typename BaseType::template FieldTraceGroupType<TopologyTrace> FieldTraceClass;
  typedef typename BaseType::template FieldCellGroupType<TopologyL> FieldCellLClass;
  typedef typename BaseType::template FieldCellGroupType<TopologyR> FieldCellRClass;

  int nodeMapT[ TopologyTrace::NNode ];
  int nodeMapL[ TopologyL::NNode ];

  const int groupL = traceGroup.getGroupLeft();
  const int groupR = traceGroup.getGroupRight();

  const int (*TraceNodesL)[ TopologyTrace::NTrace ] = TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyL>::TraceNodes;

  const FieldCellLClass& cellGroupL = this->template getCellGroup<TopologyL>(groupL);
  const FieldCellRClass& cellGroupR = this->template getCellGroup<TopologyR>(groupR);

  typename FieldTraceClass::template ElementType<> elemTrace( traceGroup.basis() );
  typename FieldCellLClass::template ElementType<> elemCellL( cellGroupL.basis() );
  typename FieldCellRClass::template ElementType<> elemCellR( cellGroupR.basis() );

  const int nelem = traceGroup.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    const int cellL                       = traceGroup.getElementLeft( elem );
    const CanonicalTraceToCell canonicalL = traceGroup.getCanonicalTraceLeft( elem );

    const int cellR                       = traceGroup.getElementRight( elem );
    const CanonicalTraceToCell canonicalR = traceGroup.getCanonicalTraceRight( elem );

    XFIELD_CHECK( cellL >= 0 && cellL < cellGroupL.nElem(), "cellL = %d, cellGroupL.nElem() = %d", cellL, cellGroupL.nElem() );
    XFIELD_CHECK( cellR >= 0 && cellR < cellGroupR.nElem(), "cellR = %d, cellGroupR.nElem() = %d", cellR, cellGroupR.nElem() );

    cellCount[groupL][cellL]++;
    cellCount[groupR][cellR]++;

    XFIELD_CHECK( canonicalL.trace >= 0 && canonicalL.trace < TopologyL::NTrace,
                  "canonicalL.trace = %d, TopologyL::NTrace = %d", canonicalL.trace, TopologyL::NTrace );
    XFIELD_CHECK( canonicalR.trace >= 0 && canonicalR.trace < TopologyR::NTrace,
                  "canonicalR.trace = %d, TopologyR::NTrace = %d", canonicalR.trace, TopologyR::NTrace );

    XFIELD_CHECK( canonicalL.orientation == 0, "canonicalL.orientation = %d", canonicalL.orientation );
    XFIELD_CHECK( canonicalR.orientation == 0, "canonicalR.orientation = %d", canonicalR.orientation );

    traceGroup.getElement( elemTrace, elem );
    cellGroupL.getElement( elemCellL, cellL );
    cellGroupR.getElement( elemCellR, cellR );

    traceGroup.associativity( elem ).getNodeGlobalMapping( nodeMapT, TopologyTrace::NNode );
    cellGroupL.associativity( cellL ).getNodeGlobalMapping( nodeMapL, TopologyL::NNode );

    // Check the node mappings are consistent
    for (int i = 0; i < TopologyTrace::NNode; i++ )
    {
      if (nodeMapL[TraceNodesL[canonicalL.trace][i]] != nodeMapT[i])
      {
        std::stringstream msg;
        msg << std::endl;
        msg << "Connected boundary trace nodes do not match left cell canonical trace nodes." << std::endl;

        msg << std::endl;
        msg << "--- Trace ---" << std::endl;
        msg << "Trace Group = " << itraceGroup << std::endl;
        msg << "Group Element number = " << elem << std::endl;
        msg << "Nodes = { ";
        for (int n = 0; n < TopologyTrace::NNode-1; n++)
          msg << nodeMapT[n] << ", ";
        msg << nodeMapT[TopologyTrace::NNode-1] << " } " << std::endl;

        msg << "Canonical Trace = " << canonicalL.trace << std::endl;
        msg << "Canonical Trace nodes = { ";
        for (int n = 0; n < TopologyTrace::NNode-1; n++)
          msg << nodeMapL[TraceNodesL[canonicalL.trace][n]] << ", ";
        msg << nodeMapL[TraceNodesL[canonicalL.trace][TopologyTrace::NNode-1]] << " } " << std::endl;

        msg << std::endl;
        msg << "--- Left Cell ---" << std::endl;
        msg << "Element number = " << cellL << std::endl;
        msg << "Nodes = { ";
        for (int n = 0; n < TopologyL::NNode-1; n++)
          msg << nodeMapL[n] << ", ";
        msg << nodeMapL[TopologyL::NNode-1] << " } " << std::endl;

        XFIELD_EXCEPTION( msg.str() );
      }
    }

    // Check trace normal vectors
    XField_::template checkTraceNormalStruct<PhysDim,TopoDim>::
    template checkTraceNormal<TopologyTrace, TopologyL, TopologyR>(*this, traceGroup, elem, elemTrace, elemCellL, elemCellR);
  }
}

} //namespace SANS
