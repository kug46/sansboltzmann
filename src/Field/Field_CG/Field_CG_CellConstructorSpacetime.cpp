// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "Field_CG_CellConstructor.h"
#include "Field_CG_Topology.h"

#include "BasisFunction/ElementFrame.h"

#include "Topology/ElementTopology.h"

#include "Field/Field.h"
#include "Field/XFieldSpacetime.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#define CORY_VERBOSE 0

namespace SANS
{

//===========================================================================//
template <class PhysDim>
Field_CG_CellConstructorBase<PhysDim, TopoD4>::
Field_CG_CellConstructorBase(const XField<PhysDim, TopoD4>& xfld, const int order, const BasisFunctionCategory category,
                             const std::vector<std::vector<int>>& CellGroupSets, const bool needsEmbeddedGhosts ) :
  xfld_(xfld),
  CellGroupSets_(CellGroupSets),
  needsEmbeddedGhosts_(needsEmbeddedGhosts),
  order_(order), category_(category), nElem_(0), nDOF_(0), nDOFpossessed_(0), nDOFghost_(0)
{
  // populate DOF vectors
  for (std::size_t i = 0; i < CellGroupSets.size(); i++)
  {
    nativeCellDOFs_.emplace_back( xfld_, order );
    nativeFaceDOFs_.emplace_back( xfld_, order );
    nativeAreaDOFs_.emplace_back( xfld_, order );
    nativeEdgeDOFs_.emplace_back( xfld_, order );
    nativeNodeDOFs_.emplace_back( xfld_ );
  }

  // DOF we're working on...
  int DOFidx = 0;

  // loop over sets - nodes on interfaces get counted twice
  for (std::size_t i = 0; i < CellGroupSets.size(); i++)
  {
    // break the cell groups out of the sets
    const std::vector<int>& CellGroups = CellGroupSets[i];
    // loop over the cell groups
    for (std::size_t igroup = 0; igroup < CellGroups.size(); igroup++)
    {
      // group number
      const int group = CellGroups[igroup];
      // increment the number of elements based on this cell group from the xfld
      nElem_ += xfld_.getCellGroupBase(group).nElem();

      // if we're looking at pentatopes...
      if ( xfld_.getCellGroupBase(group).topoTypeID() == typeid(Pentatope) )
      {
        // add the cells, faces, areas, edges, and nodes that are in the xfield
        nativeCellDOFs_[i].insertCells( xfld_.template getCellGroup<Pentatope>(group), group );
        nativeFaceDOFs_[i].insertFaces( xfld_.template getCellGroup<Pentatope>(group) );
        nativeAreaDOFs_[i].insertAreas( xfld_.template getCellGroup<Pentatope>(group) );
        nativeEdgeDOFs_[i].insertEdges( xfld_.template getCellGroup<Pentatope>(group) );
        nativeNodeDOFs_[i].insertNodes( xfld_.template getCellGroup<Pentatope>(group) );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Unknown element topology." );
    }
  }

#if CORY_VERBOSE
  printf("DOFidx: %d (before anything, should be zero).\n", DOFidx);
#endif
  // Construct the Native DOF indexing. Cells first, then edges, then nodes
  for (std::size_t i = 0; i < CellGroupSets.size(); i++)
    nativeCellDOFs_[i].createNativeIndex(DOFidx);
#if CORY_VERBOSE
  printf("DOFidx: %d (after cellDOFs).\n", DOFidx);
#endif
  for (std::size_t i = 0; i < CellGroupSets.size(); i++)
    nativeFaceDOFs_[i].createNativeIndex(DOFidx);
#if CORY_VERBOSE
  printf("DOFidx: %d (after faceDOFs).\n", DOFidx);
#endif

  for (std::size_t i = 0; i < CellGroupSets.size(); i++)
    nativeAreaDOFs_[i].createNativeIndex(DOFidx);
#if CORY_VERBOSE
  printf("DOFidx: %d (after areaDOFs).\n", DOFidx);
#endif

  for (std::size_t i = 0; i < CellGroupSets.size(); i++)
    nativeEdgeDOFs_[i].createNativeIndex(DOFidx);
#if CORY_VERBOSE
  printf("DOFidx: %d (after faceDOFs).\n", DOFidx);
#endif

  for (std::size_t i = 0; i < CellGroupSets.size(); i++)
    nativeNodeDOFs_[i].createNativeIndex(DOFidx);
#if CORY_VERBOSE
  printf("DOFidx: %d (after nodeDOFs).\n", DOFidx);
#endif

  for (std::size_t i = 0; i < CellGroupSets.size(); i++)
  {
    // Total DOF count in the field on each processor
    nDOF_ += nativeCellDOFs_[i].nDOF()
           + nativeFaceDOFs_[i].nDOF()
           + nativeAreaDOFs_[i].nDOF()
           + nativeEdgeDOFs_[i].nDOF()
           + nativeNodeDOFs_[i].nDOF();
  }

#ifdef SANS_MPI

  // every DOF is possessed for one rank
  if (xfld.comm()->size() > 1)
  {
    // Mark all ghost generating element (remainder are zombies)
    for (std::size_t i = 0; i < CellGroupSets.size(); i++)
    {
      const std::vector<int>& CellGroups = CellGroupSets[i];
      for (std::size_t igroup = 0; igroup < CellGroups.size(); igroup++)
      {
        const int group = CellGroups[igroup];

        if ( xfld_.getCellGroupBase(group).topoTypeID() == typeid(Pentatope) )
        {
          this->template
          setCellGroupDOFPossession( xfld_.template getCellGroup<Pentatope>(group), group,
                                     nativeCellDOFs_[i],
                                     nativeFaceDOFs_[i],
                                     nativeAreaDOFs_[i],
                                     nativeEdgeDOFs_[i],
                                     nativeNodeDOFs_[i] );
        }
        else
          SANS_DEVELOPER_EXCEPTION( "Unknown element topology." );
      }

      // count the number of possessed and ghost DOFs
      GhoulDOF nDOFcell = nativeCellDOFs_[i].nDOFghoul();
      GhoulDOF nDOFface = nativeFaceDOFs_[i].nDOFghoul();
      GhoulDOF nDOFarea = nativeAreaDOFs_[i].nDOFghoul();
      GhoulDOF nDOFedge = nativeEdgeDOFs_[i].nDOFghoul();
      GhoulDOF nDOFnode = nativeNodeDOFs_[i].nDOFghoul();

      nDOFpossessed_ += nDOFcell.possessed + nDOFface.possessed + nDOFarea.possessed + nDOFedge.possessed + nDOFnode.possessed;
      nDOFghost_     += nDOFcell.ghost     + nDOFface.ghost     + nDOFarea.ghost     + nDOFedge.ghost     + nDOFnode.ghost;
    }
  }
  else // comm size
  {
    nDOFpossessed_ = nDOF_;
    nDOFghost_ = 0;
  }


  // construct the local indexing on each processor
  // possessed - [[Cells],
  //              [Faces],
  //              [Edges],
  //              [Nodes],
  // ghost ------ [Cells],
  //              [Faces],
  //              [Edges],
  //              [Nodes],
  // zombies ---- [Cells],
  //              [Faces],
  //              [Edges],
  //              [Nodes]]

  DOFidx = 0;

  std::array<Ghoul,3> spirits = {{possessed, ghost, zombie}};

  // don't bother with ghost and zombie for one processor (improves local solves)
  const int nGhoul = xfld.comm()->size() == 1 ? 1 : 3;

  for (int n = 0; n < nGhoul; n++)
  {
    for (std::size_t i = 0; i < CellGroupSets.size(); i++)
      nativeCellDOFs_[i].createLocalIndex(spirits[n], DOFidx);

    for (std::size_t i = 0; i < CellGroupSets.size(); i++)
      nativeFaceDOFs_[i].createLocalIndex(spirits[n], DOFidx);

    for (std::size_t i = 0; i < CellGroupSets.size(); i++)
      nativeAreaDOFs_[i].createLocalIndex(spirits[n], DOFidx);

    for (std::size_t i = 0; i < CellGroupSets.size(); i++)
      nativeEdgeDOFs_[i].createLocalIndex(spirits[n], DOFidx);

    for (std::size_t i = 0; i < CellGroupSets.size(); i++)
      nativeNodeDOFs_[i].createLocalIndex(spirits[n], DOFidx);
  }

#else
  // in serial all DOFs are possessed
  // possessed - [[Cells],
  //              [Faces],
  //              [Areas],
  //              [Edges],
  //              [Nodes]]

  nDOFpossessed_ = nDOF_;

  DOFidx = 0;

  for (std::size_t i = 0; i < CellGroupSets.size(); i++)
    nativeCellDOFs_[i].createLocalIndex(possessed, DOFidx);

  for (std::size_t i = 0; i < CellGroupSets.size(); i++)
    nativeFaceDOFs_[i].createLocalIndex(possessed, DOFidx);

  for (std::size_t i = 0; i < CellGroupSets.size(); i++)
    nativeAreaDOFs_[i].createLocalIndex(possessed, DOFidx);

  for (std::size_t i = 0; i < CellGroupSets.size(); i++)
    nativeEdgeDOFs_[i].createLocalIndex(possessed, DOFidx);

  for (std::size_t i = 0; i < CellGroupSets.size(); i++)
    nativeNodeDOFs_[i].createLocalIndex(possessed, DOFidx);

#endif

}

//----------------------------------------------------------------------------//
template <class PhysDim>
template <class Topology>
void
Field_CG_CellConstructorBase<PhysDim, TopoD4>::
setCellGroupDOFPossession(const typename XField<PhysDim, TopoD4>::template FieldCellGroupType<Topology>& xfldCellGroup,
                          const int cellGroup,
                          Field_CG_CellDOF& nativeCellDOFs,
                          Field_CG_FaceDOF& nativeFaceDOFs,
                          Field_CG_AreaDOF& nativeAreaDOFs,
                          Field_CG_EdgeDOF& nativeEdgeDOFs,
                          Field_CG_NodeDOF& nativeNodeDOFs)
{
  //SANS_DEVELOPER_EXCEPTION("implement");
  typedef typename Topology::TopologyTrace TopologyTrace;
  typedef typename TopologyTrace::TopologyTrace TopologyFrame;

  const int nDOFperCell = TopologyDOF_CG<Topology>::count(order_);
  const int nDOFperFace = TopologyDOF_CG<TopologyTrace>::count(order_);
  const int nDOFperArea = TopologyDOF_CG<TopologyFrame>::count(order_);
  const int nDOFperEdge = TopologyDOF_CG<Line>::count(order_);

  int nodeMap[Topology::NNode];
  std::vector<int> faceMap(TopologyTrace::NNode);
  std::vector<int> areaMap(TopologyFrame::NNode);
  const int (*EdgeNodes)[ Line::NNode ] = ElementEdges<Topology>::EdgeNodes;
  const int (*AreaNodes)[ TopologyFrame::NNode ] = ElementFrame<Topology>::FrameNodes;
  const int (*FaceNodes)[ TopologyTrace::NNode ] = TraceToCellRefCoord<TopologyTrace, TopoD4, Topology>::TraceNodes;

  // if any component of a cell element is possessed, then the non-possessed components make ghosts DOFs
  for (int icell = 0; icell < xfldCellGroup.nElem(); icell++)
  {
    bool isPossessed = false;
    xfldCellGroup.associativity( icell ).getNodeGlobalMapping( nodeMap, Topology::NNode );

    // check the cell
    if (nDOFperCell > 0)
    {
      Field_CG_CellDOF::CellDOFType& cell = nativeCellDOFs.getNativeCell(icell, cellGroup);
      if ( cell.spirit == possessed ) isPossessed = true;
    }

    //extra ghost information needed if embedded field
    if ( needsEmbeddedGhosts_ && xfldCellGroup.associativity( icell ).rank() == xfld_.comm()->rank() ) isPossessed = true;

    // check all traaces
    if (nDOFperFace > 0)
      for (int iface = 0; iface < Topology::NFace && !isPossessed; iface++)
      {
        for (int n = 0; n < TopologyTrace::NNode; n++)
          faceMap[n] = nodeMap[FaceNodes[iface][n]];

        Field_CG_FaceDOF::FaceDOFType& face = nativeFaceDOFs.getNativeFace(TopologyTrace::Topology, faceMap);
        if ( face.spirit == possessed ) isPossessed = true;
      }

    // check all areas
    if (nDOFperArea > 0)
      for (int iarea = 0; iarea < Topology::NFrame && !isPossessed; iarea++)
      {
        for (int n = 0; n < TopologyFrame::NNode; n++)
          areaMap[n] = nodeMap[AreaNodes[iarea][n]];

        Field_CG_AreaDOF::AreaDOFType& area = nativeAreaDOFs.getNativeArea(TopologyFrame::Topology, areaMap);
        if ( area.spirit == possessed ) isPossessed = true;
      }

    // check all edges
    if (nDOFperEdge > 0)
      for (int iedge = 0; iedge < Topology::NEdge && !isPossessed; iedge++)
      {
        int node0 = nodeMap[EdgeNodes[iedge][0]];
        int node1 = nodeMap[EdgeNodes[iedge][1]];

        Field_CG_EdgeDOF::EdgeDOFType& edge = nativeEdgeDOFs.getNativeEdge(node0, node1);

        //add the canonical edge DOFs if they are not already in the map
        if ( edge.spirit == possessed ) isPossessed = true;
      }

    // check all nodes
    for (int n = 0; n < Topology::NNode && !isPossessed; n++)
      if ( nativeNodeDOFs.getNativeNode(nodeMap[n]).spirit == possessed ) isPossessed = true;


    if (isPossessed)
    {
      // convert zombies to ghosts
      if (nDOFperCell > 0)
      {
        Field_CG_CellDOF::CellDOFType& cell = nativeCellDOFs.getNativeCell(icell, cellGroup);
        if ( cell.spirit == zombie )
          cell.spirit = ghost;
      }

      if (nDOFperFace > 0)
        for (int iface = 0; iface < Topology::NTrace; iface++)
        {
          for (int n = 0; n < TopologyTrace::NNode; n++)
            faceMap[n] = nodeMap[FaceNodes[iface][n]];

          Field_CG_FaceDOF::FaceDOFType& face = nativeFaceDOFs.getNativeFace(TopologyTrace::Topology, faceMap);
          if ( face.spirit == zombie )
            face.spirit = ghost;
        }

      if (nDOFperArea > 0)
        for (int iarea = 0; iarea < Topology::NFrame; iarea++)
        {
          for (int n = 0; n < TopologyFrame::NNode; n++)
            areaMap[n] = nodeMap[AreaNodes[iarea][n]];

          Field_CG_AreaDOF::AreaDOFType& area = nativeAreaDOFs.getNativeArea(TopologyFrame::Topology, areaMap);
          if ( area.spirit == zombie )
            area.spirit = ghost;
        }

      if (nDOFperEdge > 0)
        for (int iedge = 0; iedge < Topology::NEdge; iedge++)
        {
          int node0 = nodeMap[EdgeNodes[iedge][0]];
          int node1 = nodeMap[EdgeNodes[iedge][1]];

          Field_CG_EdgeDOF::EdgeDOFType& edge = nativeEdgeDOFs.getNativeEdge(node0, node1);

          if ( edge.spirit == zombie )
            edge.spirit = ghost;
        }

      for (int n = 0; n < Topology::NNode; n++)
      {
        Field_CG_NodeDOF::NodeDOFType& node = nativeNodeDOFs.getNativeNode(nodeMap[n]);
        if ( node.spirit == zombie )
          node.spirit = ghost;
      }
    }
  }
}

//----------------------------------------------------------------------------//
template <class PhysDim>
void
Field_CG_CellConstructorBase<PhysDim, TopoD4>::
getDOF_rank(int* DOF_rank)
{
  //SANS_DEVELOPER_EXCEPTION("implement");
  for (std::size_t i = 0; i < CellGroupSets_.size(); i++)
    nativeCellDOFs_[i].getDOF_rank(nDOFpossessed_, DOF_rank);

  for (std::size_t i = 0; i < CellGroupSets_.size(); i++)
    nativeFaceDOFs_[i].getDOF_rank(nDOFpossessed_, DOF_rank);

  for (std::size_t i= 0; i < CellGroupSets_.size(); i++)
    nativeAreaDOFs_[i].getDOF_rank(nDOFpossessed_, DOF_rank);

  for (std::size_t i = 0; i < CellGroupSets_.size(); i++)
    nativeEdgeDOFs_[i].getDOF_rank(nDOFpossessed_, DOF_rank);

  for (std::size_t i = 0; i < CellGroupSets_.size(); i++)
    nativeNodeDOFs_[i].getDOF_rank(nDOFpossessed_, DOF_rank);
}

//----------------------------------------------------------------------------//
template<>
void orientVolumeDOFs<Tet>( const int order, const BasisFunctionCategory category, const int orientation,
                              std::vector<int>& faceDOF, std::vector<int>& nativeFaceDOFs )
{
  if (category == BasisFunctionCategory_Lagrange)
  {
    if ( order == 4 )
    {
      SANS_DEVELOPER_EXCEPTION("implement. -CVF");

      // rotate the DOFs by the orientation
      for (int i = 1; i < abs(orientation); i++)
      {
        std::rotate(       faceDOF.begin(),        faceDOF.begin() + 1,        faceDOF.end());
        std::rotate(nativeFaceDOFs.begin(), nativeFaceDOFs.begin() + 1, nativeFaceDOFs.end());
      }

      // swap the last two if the orientation is negated
      if (orientation < 0)
      {
        std::swap(faceDOF[1], faceDOF[2]);
        std::swap(nativeFaceDOFs[1], nativeFaceDOFs[2]);
      }
    }
    else if ( order > 4)
      SANS_DEVELOPER_EXCEPTION("Face map not correct for Triangle Lagrange P > 4, order = %d", order);
  }
}

// Explicit instantiations
template struct Field_CG_CellConstructorBase<PhysD4, TopoD4>;

} // namespace SANS
