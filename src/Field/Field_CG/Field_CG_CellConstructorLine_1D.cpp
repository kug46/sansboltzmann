// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define FIELD_CG_CELLCONSTRUCTORLINE_INSTANTIATE
#include "Field_CG_CellConstructorLine_impl.h"

namespace SANS
{

// Explicit instantiations
template struct Field_CG_CellConstructorBase<PhysD1, TopoD1>;

} // namespace SANS
