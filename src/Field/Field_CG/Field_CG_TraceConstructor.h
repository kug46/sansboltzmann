// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELD_CG_TRACECONSTRUCTOR_H
#define FIELD_CG_TRACECONSTRUCTOR_H

#include <vector>
#include <memory> // std::unique_ptr

#include "Topology/ElementTopology.h"

#include "BasisFunction/LagrangeDOFMap.h"

#include "Field/Field.h"
#include "Field_CG_Topology.h"

#include "Field/ElementConnectivityConstructor.h"
#include "Field/Element/ElementAssociativityAreaConstructor.h"
#include "Field/Element/ElementAssociativityVolumeConstructor.h"
#include "Field/Element/ElementAssociativitySpacetimeConstructor.h"


namespace SANS
{
//============================================================================//
template <class TopologyTrace>
void orientFaceDOFs( const int order, const BasisFunctionCategory category, const int orientation,
                    std::vector<int>& faceDOF, std::vector<int>& nativeFaceDOFs );

template <class TopologyTrace>
void orientVolumeDOFs( const int order, const BasisFunctionCategory category, const int orientation,
                    std::vector<int>& volumeDOF, std::vector<int>& nativeVolumeDOFs );


//============================================================================//
template <class PhysDim, class TopoDim>
struct Field_CG_TraceConstructor;

//============================================================================//
// Class for constructing indexing for a continuous field
//============================================================================//
template <class PhysDim>
struct Field_CG_TraceConstructor<PhysDim, TopoD1>
{
  Field_CG_TraceConstructor(const XField<PhysDim, TopoD1>& xfld, const int order, const BasisFunctionCategory category,
                            const std::vector<std::vector<int>>& interiorGroupSets,
                            const std::vector<std::vector<int>>& boundaryGroupSets );

  int nElem()         const { return nElem_;         }
  int nDOF()          const { return nDOF_;          }
  int nDOFpossessed() const { return nDOFpossessed_; }
  int nDOFghost()     const { return nDOFghost_;     }

  template <class FieldTraceGroupClass>
  FieldTraceGroupClass*
  createInteriorTraceGroup( const int iTraceGroup, int* local2nativeDOFmap );

  template <class FieldTraceGroupClass>
  FieldTraceGroupClass*
  createBoundaryTraceGroup( const int bTraceGroup, int* local2nativeDOFmap );

  template <class FieldTraceGroupClass>
  FieldTraceGroupClass*
  createGhostBoundaryTraceGroup( const int bTraceGroup, int* local2nativeDOFmap );

  void getDOF_rank(int* DOF_rank);

protected:
  const XField<PhysDim, TopoD1>& xfld_;

  //Used to count unique node DOFs in a native indexing
  std::vector<Field_CG_NodeDOF> nativeNodeDOFs_;

  const std::vector<std::vector<int>>& interiorGroupSets_;
  const std::vector<std::vector<int>>& boundaryGroupSets_;

  const int order_;
  const BasisFunctionCategory category_;

  // Total number of cell elements
  int nElem_;

  // Total DOF count
  int nDOF_;
  int nDOFpossessed_;
  int nDOFghost_;

  template <class Topology>
  void
  setTraceGroupDOFPossession( const typename XField<PhysDim, TopoD1>::template FieldTraceGroupType<Topology>& xfldTraceGroup,
                              Field_CG_NodeDOF& nativeNodeDOFs );

  template <class FieldTraceGroupClass, class Topology>
  FieldTraceGroupClass*
  createTraceGroup( const typename XField<PhysDim, TopoD1>::template FieldTraceGroupType<Topology>& xfldTraceGroup,
                    Field_CG_NodeDOF& nativeNodeDOFs,
                    int* local2nativeDOFmap );

};

//---------------------------------------------------------------------------//
template <class PhysDim>
template <class FieldTraceGroupClass>
FieldTraceGroupClass*
Field_CG_TraceConstructor<PhysDim, TopoD1>::
createInteriorTraceGroup( const int iTraceGroup, int* local2nativeDOFmap )
{
  typedef typename FieldTraceGroupClass::TopologyType Topology;

  for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
  {
    const std::vector<int>& interiorGroups = interiorGroupSets_[i];

    // check if the current set of cell groups is to the left if the iTrace group
    if (std::find(interiorGroups.begin(), interiorGroups.end(), iTraceGroup) != interiorGroups.end())
      return this->template createTraceGroup<FieldTraceGroupClass, Topology>(
          xfld_.template getInteriorTraceGroup<Topology>(iTraceGroup),
          nativeNodeDOFs_[i],
          local2nativeDOFmap );
  }

  SANS_DEVELOPER_EXCEPTION("Count not find left cell group!!");
  return nullptr;
}

//---------------------------------------------------------------------------//
template <class PhysDim>
template <class FieldTraceGroupClass>
FieldTraceGroupClass*
Field_CG_TraceConstructor<PhysDim, TopoD1>::
createBoundaryTraceGroup( const int bTraceGroup, int* local2nativeDOFmap )
{
  typedef typename FieldTraceGroupClass::TopologyType Topology;

  for (std::size_t i = 0; i < boundaryGroupSets_.size(); i++)
  {
    const std::vector<int>& boundaryGroups = boundaryGroupSets_[i];

    // check if the current set of cell groups is to the left of the bTrace group
    if (std::find(boundaryGroups.begin(), boundaryGroups.end(), bTraceGroup) != boundaryGroups.end())
      return this->template createTraceGroup<FieldTraceGroupClass, Topology>(
          xfld_.template getBoundaryTraceGroup<Topology>(bTraceGroup),
          nativeNodeDOFs_[i],
          local2nativeDOFmap );
  }

  SANS_DEVELOPER_EXCEPTION("Count not find left cell group!!");
  return nullptr;
}

#if 0
//---------------------------------------------------------------------------//
template <class PhysDim>
template <class FieldTraceGroupClass>
FieldTraceGroupClass*
Field_CG_TraceConstructor<PhysDim, TopoD1>::
createGhostBoundaryTraceGroup( const int bTraceGroup, int* local2nativeDOFmap )
{
  typedef typename FieldTraceGroupClass::TopologyType Topology;

  for (std::size_t i = 0; i < CellGroupSets_.size(); i++)
  {
    const std::vector<int>& CellGroups = CellGroupSets_[i];

    // check if the current set of cell groups is to the left of the bTrace group
    if (std::find(CellGroups.begin(), CellGroups.end(), cellGroupL) != CellGroups.end())
      return this->template createTraceGroup<FieldTraceGroupClass, Topology>(
          xfld_.template getGhostBoundaryTraceGroup<Topology>(bTraceGroup),
          nativeNodeDOFs_[i],
          local2nativeDOFmap );
  }

  SANS_DEVELOPER_EXCEPTION("Count not find left cell group!!");
  return nullptr;
}
#endif

//---------------------------------------------------------------------------//
template <class PhysDim>
template <class FieldTraceGroupClass, class Topology>
FieldTraceGroupClass*
Field_CG_TraceConstructor<PhysDim, TopoD1>::
createTraceGroup( const typename XField<PhysDim, TopoD1>::template FieldTraceGroupType<Topology>& xfldTraceGroup,
                  Field_CG_NodeDOF& nativeNodeDOFs,
                  int* local2nativeDOFmap )
{
  typedef typename FieldTraceGroupClass::BasisType BasisType;
  typedef typename FieldTraceGroupClass::FieldAssociativityConstructorType FieldTraceConstructorType;

  const int order = order_;
  const BasisFunctionCategory category = category_;

  const BasisType* basis = BasisType::getBasisFunction(order, category);

  // create field associativity constructor
  FieldTraceConstructorType fldAssocTrace( basis, xfldTraceGroup.nElem() );

  int nodeMap[Topology::NNode];

  for (int itrace = 0; itrace < xfldTraceGroup.nElem(); itrace++)
  {
    // set the processor rank
    fldAssocTrace.setAssociativity(itrace).setRank( xfldTraceGroup.associativity( itrace ).rank() );

    // get the node mapping of the grid
    xfldTraceGroup.associativity( itrace ).getNodeGlobalMapping( nodeMap, Topology::NNode );

    // update the node numbering and set node DOFs
    for (int k = 0; k < Topology::NNode; k++)
    {
      Field_CG_NodeDOF::NodeDOFType& node    = nativeNodeDOFs.getNativeNode(nodeMap[k]);
      const int                      nodeDOF = nativeNodeDOFs.getLocalNode(nodeMap[k]);

      nodeMap[k] = nodeDOF;
      local2nativeDOFmap[nodeDOF] = node.nativeNodeDOF;
    }

    fldAssocTrace.setAssociativity(itrace).setNodeGlobalMapping( nodeMap, Topology::NNode );
  }

  return new FieldTraceGroupClass(fldAssocTrace);
}

//============================================================================//
// Class for constructing indexing for a continuous field
//============================================================================//
template <class PhysDim>
struct Field_CG_TraceConstructor<PhysDim, TopoD2>
{
  Field_CG_TraceConstructor(const XField<PhysDim, TopoD2>& xfld, const int order, const BasisFunctionCategory category,
                            const std::vector<std::vector<int>>& interiorGroupSets,
                            const std::vector<std::vector<int>>& boundaryGroupSets );

  int nElem()         const { return nElem_;         }
  int nDOF()          const { return nDOF_;          }
  int nDOFpossessed() const { return nDOFpossessed_; }
  int nDOFghost()     const { return nDOFghost_;     }

  template <class FieldTraceGroupClass>
  FieldTraceGroupClass*
  createInteriorTraceGroup( const int iTraceGroup, int* local2nativeDOFmap );

  template <class FieldTraceGroupClass>
  FieldTraceGroupClass*
  createBoundaryTraceGroup( const int bTraceGroup, int* local2nativeDOFmap );

  template <class FieldTraceGroupClass>
  FieldTraceGroupClass*
  createGhostBoundaryTraceGroup( const int bTraceGroup, int* local2nativeDOFmap );

  void getDOF_rank(int* DOF_rank);

protected:
  const XField<PhysDim, TopoD2>& xfld_;

  //A map of all the edge DOFs in a native indexing
  std::vector<Field_CG_EdgeDOF> nativeEdgeDOFs_;

  //Used to count unique node DOFs in a native indexing
  std::vector<Field_CG_NodeDOF> nativeNodeDOFs_;

  const std::vector<std::vector<int>>& interiorGroupSets_;
  const std::vector<std::vector<int>>& boundaryGroupSets_;

  const int order_;
  const BasisFunctionCategory category_;

  // Total number of cell elements
  int nElem_;

  // Total DOF count
  int nDOF_;
  int nDOFpossessed_;
  int nDOFghost_;

  template <class Topology>
  void
  setTraceGroupDOFPossession( const typename XField<PhysDim, TopoD2>::template FieldTraceGroupType<Topology>& xfldTraceGroup,
                              Field_CG_EdgeDOF& nativeEdgeDOFs,
                              Field_CG_NodeDOF& nativeNodeDOFs );

  template <class FieldTraceGroupClass, class Topology>
  FieldTraceGroupClass*
  createTraceGroup( const typename XField<PhysDim, TopoD2>::template FieldTraceGroupType<Topology>& xfldTraceGroup,
                    Field_CG_EdgeDOF& nativeEdgeDOFs,
                    Field_CG_NodeDOF& nativeNodeDOFs,
                    int* local2nativeDOFmap );

};

//---------------------------------------------------------------------------//
template <class PhysDim>
template <class FieldTraceGroupClass>
FieldTraceGroupClass*
Field_CG_TraceConstructor<PhysDim, TopoD2>::
createInteriorTraceGroup( const int iTraceGroup, int* local2nativeDOFmap )
{
  typedef typename FieldTraceGroupClass::TopologyType Topology;

  for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
  {
    const std::vector<int>& interiorGroups = interiorGroupSets_[i];

    // check if the current set of cell groups is to the left if the iTrace group
    if (std::find(interiorGroups.begin(), interiorGroups.end(), iTraceGroup) != interiorGroups.end())
      return this->template createTraceGroup<FieldTraceGroupClass, Topology>(
          xfld_.template getInteriorTraceGroup<Topology>(iTraceGroup),
          nativeEdgeDOFs_[i],
          nativeNodeDOFs_[i],
          local2nativeDOFmap );
  }

  SANS_DEVELOPER_EXCEPTION("Count not find left cell group!!");
  return nullptr;
}

//---------------------------------------------------------------------------//
template <class PhysDim>
template <class FieldTraceGroupClass>
FieldTraceGroupClass*
Field_CG_TraceConstructor<PhysDim, TopoD2>::
createBoundaryTraceGroup( const int bTraceGroup, int* local2nativeDOFmap )
{
  typedef typename FieldTraceGroupClass::TopologyType Topology;

  for (std::size_t i = 0; i < boundaryGroupSets_.size(); i++)
  {
    const std::vector<int>& boundaryGroups = boundaryGroupSets_[i];

    // check if the current set of cell groups is to the left of the bTrace group
    if (std::find(boundaryGroups.begin(), boundaryGroups.end(), bTraceGroup) != boundaryGroups.end())
      return this->template createTraceGroup<FieldTraceGroupClass, Topology>(
          xfld_.template getBoundaryTraceGroup<Topology>(bTraceGroup),
          nativeEdgeDOFs_[i],
          nativeNodeDOFs_[i],
          local2nativeDOFmap );
  }

  SANS_DEVELOPER_EXCEPTION("Count not find left cell group!!");
  return nullptr;
}

#if 0
//---------------------------------------------------------------------------//
template <class PhysDim>
template <class FieldTraceGroupClass>
FieldTraceGroupClass*
Field_CG_TraceConstructor<PhysDim, TopoD2>::
createGhostBoundaryTraceGroup( const int bTraceGroup, int* local2nativeDOFmap )
{
  typedef typename FieldTraceGroupClass::TopologyType Topology;

  for (std::size_t i = 0; i < CellGroupSets_.size(); i++)
  {
    const std::vector<int>& CellGroups = CellGroupSets_[i];

    // check if the current set of cell groups is to the left of the bTrace group
    if (std::find(CellGroups.begin(), CellGroups.end(), cellGroupL) != CellGroups.end())
      return this->template createTraceGroup<FieldTraceGroupClass, Topology>(
          xfld_.template getGhostBoundaryTraceGroup<Topology>(bTraceGroup),
          nativeEdgeDOFs_[i],
          nativeNodeDOFs_[i],
          local2nativeDOFmap );
  }

  SANS_DEVELOPER_EXCEPTION("Count not find left cell group!!");
  return nullptr;
}
#endif

//---------------------------------------------------------------------------//
template <class PhysDim>
template <class FieldTraceGroupClass, class Topology>
FieldTraceGroupClass*
Field_CG_TraceConstructor<PhysDim, TopoD2>::
createTraceGroup( const typename XField<PhysDim, TopoD2>::template FieldTraceGroupType<Topology>& xfldTraceGroup,
                  Field_CG_EdgeDOF& nativeEdgeDOFs,
                  Field_CG_NodeDOF& nativeNodeDOFs,
                  int* local2nativeDOFmap )
{
  typedef typename FieldTraceGroupClass::BasisType BasisType;
  typedef typename FieldTraceGroupClass::FieldAssociativityConstructorType FieldTraceConstructorType;

  const int order = order_;
  const BasisFunctionCategory category = category_;

  const int nDOFperEdge = TopologyDOF_CG<Line>::count(order);

  const BasisType* basis = BasisType::getBasisFunction(order, category);

  // create field associativity constructor
  FieldTraceConstructorType fldAssocTrace( basis, xfldTraceGroup.nElem() );

  int nodeMap[Topology::NNode];

  for (int itrace = 0; itrace < xfldTraceGroup.nElem(); itrace++)
  {
    // set the processor rank
    fldAssocTrace.setAssociativity(itrace).setRank( xfldTraceGroup.associativity( itrace ).rank() );

    // get the node mapping of the grid
    xfldTraceGroup.associativity( itrace ).getNodeGlobalMapping( nodeMap, Topology::NNode );

    if (nDOFperEdge > 0)
    {
      // Get the nodes for the canonical edge
      int node0 = nodeMap[0];
      int node1 = nodeMap[1];

      Field_CG_EdgeDOF::EdgeDOFType& edge    = nativeEdgeDOFs.getNativeEdge(node0, node1);
      std::vector<int>               edgeDOF = nativeEdgeDOFs.getLocalEdge(node0, node1);

      std::vector<int> nativeEdgeDOFs = edge.nativeEdgeDOFs;

      //Reverse the DOF ordering if the basis is Lagrange and the edge orientation is reversed
      if (category_ == BasisFunctionCategory_Lagrange && edge.nativeNode0 != xfld_.local2nativeDOFmap(node0))
      {
        std::reverse(edgeDOF.begin(), edgeDOF.end());
        std::reverse(nativeEdgeDOFs.begin(), nativeEdgeDOFs.end());
      }

      for (int i = 0; i < nDOFperEdge; i++)
        local2nativeDOFmap[edgeDOF[i]] = nativeEdgeDOFs[i];

      fldAssocTrace.setAssociativity(itrace).setEdgeGlobalMapping(edgeDOF);
    }

    // update the node numbering and set node DOFs
    for (int k = 0; k < Topology::NNode; k++)
    {
      Field_CG_NodeDOF::NodeDOFType& node    = nativeNodeDOFs.getNativeNode(nodeMap[k]);
      const int                      nodeDOF = nativeNodeDOFs.getLocalNode(nodeMap[k]);

      nodeMap[k] = nodeDOF;
      local2nativeDOFmap[nodeDOF] = node.nativeNodeDOF;
    }

    fldAssocTrace.setAssociativity(itrace).setNodeGlobalMapping( nodeMap, Topology::NNode );
  }

  return new FieldTraceGroupClass(fldAssocTrace);
}


//============================================================================//
// Class for constructing indexing for a continuous field
//============================================================================//
template <class PhysDim>
struct Field_CG_TraceConstructor<PhysDim, TopoD3>
{
  Field_CG_TraceConstructor(const XField<PhysDim, TopoD3>& xfld, const int order, const BasisFunctionCategory category,
                            const std::vector<std::vector<int>>& interiorGroupSets,
                            const std::vector<std::vector<int>>& boundaryGroupSets );

  // return the number of elements
  int nElem()         const { return nElem_;         }
  // return the number of degrees of freedom
  int nDOF()          const { return nDOF_;          }
  // return the number of degrees of freedom possessed by this processor ("global")
  int nDOFpossessed() const { return nDOFpossessed_; }
  // return the number of ghost degrees of freedom here (data not possessed by this processor)
  int nDOFghost()     const { return nDOFghost_;     }

  template <class FieldTraceGroupClass>
  FieldTraceGroupClass*
  createInteriorTraceGroup( const int iTraceGroup, int* local2nativeDOFmap );

  template <class FieldTraceGroupClass>
  FieldTraceGroupClass*
  createBoundaryTraceGroup( const int bTraceGroup, int* local2nativeDOFmap );

  template <class FieldTraceGroupClass>
  FieldTraceGroupClass*
  createGhostBoundaryTraceGroup( const int bTraceGroup, int* local2nativeDOFmap );

  void getDOF_rank(int* DOF_rank);

protected:
  // the XField object that holds the grid
  const XField<PhysDim, TopoD3>& xfld_;

  // a map of all the face DOFs in a native indexing
  // each member is a container, the container holds the native nodes that make up
  // the face in a vector, as well as the processor that possesses the face and a map
  // to the local DOFs that make up the face (also in a vector)
  std::vector<Field_CG_FaceDOF> nativeFaceDOFs_;

  // each member is a container, the container holds the native nodes that make up
  // the face in a vector, as well as the processor that possesses the face and a map
  // to the local DOFs that make up the face (also in a vector)
  std::vector<Field_CG_EdgeDOF> nativeEdgeDOFs_;

  // used to count unique node DOFs in a native indexing
  // each member is a container, the container holds the native node index, as well
  // as a map to the processor ranks that hold the elements that involve the node!
  std::vector<Field_CG_NodeDOF> nativeNodeDOFs_;

  // sets of trace groups corresponding to the interior traces and the boundary traces
  const std::vector<std::vector<int>>& interiorGroupSets_;
  const std::vector<std::vector<int>>& boundaryGroupSets_;

  // order and category of the basis functions
  const int order_;
  const BasisFunctionCategory category_;

  // total number of cell elements
  int nElem_;

  // total DOF counts
  int nDOF_;
  int nDOFpossessed_;
  int nDOFghost_;

  template <class Topology>
  void
  setTraceGroupDOFPossession(const typename XField<PhysDim, TopoD3>::template FieldTraceGroupType<Topology>& xfldTraceGroup,
                             Field_CG_FaceDOF& nativeFaceDOFs,
                             Field_CG_EdgeDOF& nativeEdgeDOFs,
                             Field_CG_NodeDOF& nativeNodeDOFs );

  template <class FieldTraceGroupClass, class Topology>
  FieldTraceGroupClass*
  createTraceGroup( const typename XField<PhysDim, TopoD3>::template FieldTraceGroupType<Topology>& xfldTraceGroup,
                    Field_CG_FaceDOF& nativeFaceDOFs,
                    Field_CG_EdgeDOF& nativeEdgeDOFs,
                    Field_CG_NodeDOF& nativeNodeDOFs,
                    int* local2nativeDOFmap );
};

//---------------------------------------------------------------------------//
template <class PhysDim>
template <class FieldTraceGroupClass>
FieldTraceGroupClass*
Field_CG_TraceConstructor<PhysDim, TopoD3>::
createInteriorTraceGroup( const int iTraceGroup, int* local2nativeDOFmap )
{
  typedef typename FieldTraceGroupClass::TopologyType Topology;

  for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
  {
    const std::vector<int>& interiorGroups = interiorGroupSets_[i];

    // check if the current set of cell groups is to the left if the iTrace group
    if (std::find(interiorGroups.begin(), interiorGroups.end(), iTraceGroup) != interiorGroups.end())
      return this->template createTraceGroup<FieldTraceGroupClass, Topology>(
          xfld_.template getInteriorTraceGroup<Topology>(iTraceGroup),
          nativeFaceDOFs_[i],
          nativeEdgeDOFs_[i],
          nativeNodeDOFs_[i],
          local2nativeDOFmap );
  }

  SANS_DEVELOPER_EXCEPTION("Count not find left cell group!!");
  return nullptr;
}

//---------------------------------------------------------------------------//
template <class PhysDim>
template <class FieldTraceGroupClass>
FieldTraceGroupClass*
Field_CG_TraceConstructor<PhysDim, TopoD3>::
createBoundaryTraceGroup( const int bTraceGroup, int* local2nativeDOFmap )
{
  typedef typename FieldTraceGroupClass::TopologyType Topology;

  for (std::size_t i = 0; i < boundaryGroupSets_.size(); i++)
  {
    const std::vector<int>& boundaryGroups = boundaryGroupSets_[i];

    // if this bTraceGroup doesn't already exist
    if (std::find(boundaryGroups.begin(), boundaryGroups.end(), bTraceGroup) != boundaryGroups.end())
      return this->template createTraceGroup<FieldTraceGroupClass, Topology>(
          xfld_.template getBoundaryTraceGroup<Topology>(bTraceGroup),
          nativeFaceDOFs_[i],
          nativeEdgeDOFs_[i],
          nativeNodeDOFs_[i],
          local2nativeDOFmap );
  }

  SANS_DEVELOPER_EXCEPTION("Count not find left cell group!!");
  return nullptr;
}

#if 0
//---------------------------------------------------------------------------//
template <class PhysDim>
template <class FieldTraceGroupClass>
FieldTraceGroupClass*
Field_CG_TraceConstructor<PhysDim, TopoD3>::
createGhostBoundaryTraceGroup( const int bTraceGroup, int* local2nativeDOFmap )
{
  typedef typename FieldTraceConstructorType::TopologyType Topology;

  int cellGroupL = xfld_.getGhostBoundaryTraceGroupBase(bTraceGroup).getGroupLeft();

  for (std::size_t i = 0; i < CellGroupSets_.size(); i++)
  {
    const std::vector<int>& CellGroups = CellGroupSets_[i];

    // if this bTraceGroup doesn't already exist
    if (std::find(CellGroups.begin(), CellGroups.end(), cellGroupL) != CellGroups.end())
      return this->template createTraceGroup<FieldTraceGroupClass, Topology>(
          xfld_.template getGhostBoundaryTraceGroup<Topology>(bTraceGroup),
          nativeFaceDOFs_[i],
          nativeEdgeDOFs_[i],
          nativeNodeDOFs_[i] );
  }

  SANS_DEVELOPER_EXCEPTION("Count not find left cell group!!");
  return nullptr;
}
#endif

//---------------------------------------------------------------------------//
/** createTraceGroup creates a new trace group
 *
 */
template <class PhysDim>
template <class FieldTraceGroupClass, class Topology>
FieldTraceGroupClass*
Field_CG_TraceConstructor<PhysDim, TopoD3>::
createTraceGroup( const typename XField<PhysDim, TopoD3>::template FieldTraceGroupType<Topology>& xfldTraceGroup,
                  Field_CG_FaceDOF& nativeFaceDOFs,
                  Field_CG_EdgeDOF& nativeEdgeDOFs,
                  Field_CG_NodeDOF& nativeNodeDOFs,
                  int* local2nativeDOFmap )
{
  typedef typename FieldTraceGroupClass::BasisType BasisType;
  typedef typename FieldTraceGroupClass::FieldAssociativityConstructorType FieldTraceConstructorType;

  const int (*EdgeNodes)[ Line::NNode ] = ElementEdges<Topology>::EdgeNodes;

  const int order = order_;
  const BasisFunctionCategory category = category_;

  const int nDOFperFace = TopologyDOF_CG<Topology>::count(order);
  const int nDOFperEdge = TopologyDOF_CG<Line>::count(order);

  const BasisType* basis = BasisType::getBasisFunction(order, category);

  // create field associativity constructor
  FieldTraceConstructorType fldAssocTrace( basis, xfldTraceGroup.nElem() );

  std::vector<int> nodeMap(Topology::NNode);
  std::vector<int> edgeMap(Topology::NEdge*nDOFperEdge);

  for (int itrace = 0; itrace < xfldTraceGroup.nElem(); itrace++)
  {
    // set the processor rank
    fldAssocTrace.setAssociativity(itrace).setRank( xfldTraceGroup.associativity( itrace ).rank() );

    // get the node mapping of the grid
    xfldTraceGroup.associativity( itrace ).getNodeGlobalMapping( nodeMap.data(), nodeMap.size() );

    if (nDOFperFace > 0)
    {
      // Retrieve face DOFs and set them
      Field_CG_FaceDOF::FaceDOFType& face    = nativeFaceDOFs.getNativeFace(Topology::Topology, nodeMap);
      std::vector<int>               faceDOF = nativeFaceDOFs.getLocalFace(Topology::Topology, nodeMap);

      std::vector<int> nativeNodes(nodeMap.size());
      for (std::size_t i = 0; i < nodeMap.size(); i++)
        nativeNodes[i] = xfld_.local2nativeDOFmap(nodeMap[i]);

      {
        std::vector<int> nativeFaceDOFs = face.nativeFaceDOFs;

        // Get the orientation
        int orientation = CanonicalOrientation<Topology, TopoD3>::get(nativeNodes, face.nativeNodes);

        orientFaceDOFs<Topology>( order, category, orientation, faceDOF, nativeFaceDOFs );

        // set the native mapping
        for (int i = 0; i < nDOFperFace; i++)
          local2nativeDOFmap[faceDOF[i]] = nativeFaceDOFs[i];
      }

      fldAssocTrace.setAssociativity(itrace).setCellGlobalMapping(faceDOF.data(), faceDOF.size());
    }


    if (nDOFperEdge > 0)
    {
      // Retrieve edge DOFs and set them
      int iedgeDOF = 0;
      for (int iedge = 0; iedge < Topology::NEdge; iedge++)
      {
        // Get the nodes for the canonical edge
        int node0 = nodeMap[EdgeNodes[iedge][0]];
        int node1 = nodeMap[EdgeNodes[iedge][1]];

        Field_CG_EdgeDOF::EdgeDOFType& edge    = nativeEdgeDOFs.getNativeEdge(node0, node1);
        std::vector<int>               edgeDOF = nativeEdgeDOFs.getLocalEdge(node0, node1);

        std::vector<int> nativeEdgeDOFs = edge.nativeEdgeDOFs;

        //Reverse the DOF ordering if the basis is Lagrange and the edge orientation is reversed
        if (category == BasisFunctionCategory_Lagrange && edge.nativeNode0 != xfld_.local2nativeDOFmap(node0))
        {
          std::reverse(edgeDOF.begin(), edgeDOF.end());
          std::reverse(nativeEdgeDOFs.begin(), nativeEdgeDOFs.end());
        }

        // copy over the edge map
        for (int i = 0; i < nDOFperEdge; i++)
        {
          edgeMap[iedgeDOF] = edgeDOF[i];
          local2nativeDOFmap[edgeDOF[i]] = nativeEdgeDOFs[i];
          iedgeDOF++;
        }
      }
      fldAssocTrace.setAssociativity(itrace).setEdgeGlobalMapping(edgeMap);
    }

    // update the node numbering and set node DOFs
    for (int k = 0; k < Topology::NNode; k++)
    {
      Field_CG_NodeDOF::NodeDOFType& node    = nativeNodeDOFs.getNativeNode(nodeMap[k]);
      const int                      nodeDOF = nativeNodeDOFs.getLocalNode(nodeMap[k]);

      nodeMap[k] = nodeDOF;
      local2nativeDOFmap[nodeDOF] = node.nativeNodeDOF;
    }

    fldAssocTrace.setAssociativity(itrace).setNodeGlobalMapping( nodeMap );
  }

  return new FieldTraceGroupClass(fldAssocTrace);
}

//============================================================================//
// Class for constructing indexing for a continuous field
//============================================================================//
template <class PhysDim>
struct Field_CG_TraceConstructor<PhysDim, TopoD4>
{
  Field_CG_TraceConstructor(const XField<PhysDim, TopoD4>& xfld, const int order, const BasisFunctionCategory category,
                            const std::vector<std::vector<int>>& interiorGroupSets,
                            const std::vector<std::vector<int>>& boundaryGroupSets );

  // return the number of elements
  int nElem()         const { return nElem_;         }
  // return the number of degrees of freedom
  int nDOF()          const { return nDOF_;          }
  // return the number of degrees of freedom possessed by this processor ("global")
  int nDOFpossessed() const { return nDOFpossessed_; }
  // return the number of ghost degrees of freedom here (data not possessed by this processor)
  int nDOFghost()     const { return nDOFghost_;     }

  template <class FieldTraceGroupClass>
  FieldTraceGroupClass*
  createInteriorTraceGroup( const int iTraceGroup, int* local2nativeDOFmap );

  template <class FieldTraceGroupClass>
  FieldTraceGroupClass*
  createBoundaryTraceGroup( const int bTraceGroup, int* local2nativeDOFmap );

  template <class FieldTraceGroupClass>
  FieldTraceGroupClass*
  createGhostBoundaryTraceGroup( const int bTraceGroup, int* local2nativeDOFmap );

  void getDOF_rank(int* DOF_rank);

protected:
  // the XField object which holds the grid
  const XField<PhysDim, TopoD4>& xfld_;

  // a map of all the face DOFs in a native indexing
  // each member is a container, the container holds the native nodes that make up
  // the face in a vector, as well as the processor that possesses the face and a map
  // to the local DOFs that make up the face (also in a vector)
  std::vector<Field_CG_FaceDOF> nativeFaceDOFs_;

  // a map of all the area DOFs in a native indexing
  // each member is a container, the container holds the native nodes that make up
  // the areas in a vector, as well as the processor that possesses the face and a map
  // to the local DOFs that make up the area (also in a vector)
  std::vector<Field_CG_AreaDOF> nativeAreaDOFs_;

  // used to count unique area DOFs in a native indexing
  // each member is a container, the container holds the native node index, as well
  // as a map to the processor ranks that hold the elements that involve the node!
  std::vector<Field_CG_EdgeDOF> nativeEdgeDOFs_;

  // used to count unique node DOFs in a native indexing
  // each member is a container, the container holds the native node index, as well
  // as a map to the processor ranks that hold the elements that involve the node!
  std::vector<Field_CG_NodeDOF> nativeNodeDOFs_;

  // sets of trace groups holding the interior traces and the boundary traces, respectively
  const std::vector<std::vector<int>>& interiorGroupSets_;
  const std::vector<std::vector<int>>& boundaryGroupSets_;

  // order and category of the basis functions
  const int order_;
  const BasisFunctionCategory category_;

  // total number of cell elements
  int nElem_;

  // total DOF counts
  int nDOF_;
  int nDOFpossessed_;
  int nDOFghost_;

  template <class Topology>
  void
  setTraceGroupDOFPossession(const typename XField<PhysDim, TopoD4>::template FieldTraceGroupType<Topology>& xfldTraceGroup,
                             Field_CG_FaceDOF& nativeFaceDOFs,
                             Field_CG_AreaDOF& nativeAreaDOFs,
                             Field_CG_EdgeDOF& nativeEdgeDOFs,
                             Field_CG_NodeDOF& nativeNodeDOFs );

  template <class FieldTraceGroupClass, class Topology>
  FieldTraceGroupClass*
  createTraceGroup( const typename XField<PhysDim, TopoD4>::template FieldTraceGroupType<Topology>& xfldTraceGroup,
                    Field_CG_FaceDOF& nativeFaceDOFs,
                    Field_CG_AreaDOF& nativeAreaDOFs,
                    Field_CG_EdgeDOF& nativeEdgeDOFs,
                    Field_CG_NodeDOF& nativeNodeDOFs,
                    int* local2nativeDOFmap );
};

//---------------------------------------------------------------------------//
template <class PhysDim>
template <class FieldTraceGroupClass>
FieldTraceGroupClass*
Field_CG_TraceConstructor<PhysDim, TopoD4>::
createInteriorTraceGroup( const int iTraceGroup, int *local2nativeDOFmap)
{
  // get the current topology
  typedef typename FieldTraceGroupClass::TopologyType Topology;

  // loop over all the interior group sets
  for (std::size_t i= 0; i < interiorGroupSets_.size(); i++)
  {
    // grab this set of interior groups
    const std::vector<int>& interiorGroups= interiorGroupSets_[i];

    // check if the current set of cell groups is to the left if the iTrace group
    if (std::find(interiorGroups.begin(), interiorGroups.end(), iTraceGroup) != interiorGroups.end())
      return this->template createTraceGroup<FieldTraceGroupClass, Topology>(
          xfld_.template getInteriorTraceGroup<Topology>(iTraceGroup),
          nativeFaceDOFs_[i],
          nativeAreaDOFs_[i],
          nativeEdgeDOFs_[i],
          nativeNodeDOFs_[i],
          local2nativeDOFmap );
  }

  SANS_DEVELOPER_EXCEPTION("Count not find left cell group!!");
  return nullptr;
}

//---------------------------------------------------------------------------//
template <class PhysDim>
template <class FieldTraceGroupClass>
FieldTraceGroupClass*
Field_CG_TraceConstructor<PhysDim, TopoD4>::
createBoundaryTraceGroup( const int bTraceGroup, int* local2nativeDOFmap )
{
  // grab the current topology
  typedef typename FieldTraceGroupClass::TopologyType Topology;

  // loop over all the boundary group sets
  for (std::size_t i= 0; i < boundaryGroupSets_.size(); i++)
  {
    // grab this set of boundary groups
    const std::vector<int>& boundaryGroups = boundaryGroupSets_[i];

    // if this bTraceGroup doesn't already exist
    if (std::find(boundaryGroups.begin(), boundaryGroups.end(), bTraceGroup) != boundaryGroups.end())
      return this->template createTraceGroup<FieldTraceGroupClass, Topology>(
          xfld_.template getBoundaryTraceGroup<Topology>(bTraceGroup),
          nativeFaceDOFs_[i],
          nativeAreaDOFs_[i],
          nativeEdgeDOFs_[i],
          nativeNodeDOFs_[i],
          local2nativeDOFmap );
  }

  SANS_DEVELOPER_EXCEPTION("Count not find left cell group!!");
  return nullptr;
}


//---------------------------------------------------------------------------//
template <class PhysDim>
template <class FieldTraceGroupClass, class Topology>
FieldTraceGroupClass*
Field_CG_TraceConstructor<PhysDim, TopoD4>::
createTraceGroup( const typename XField<PhysDim, TopoD4>::template FieldTraceGroupType<Topology>& xfldTraceGroup,
                  Field_CG_FaceDOF& nativeFaceDOFs,
                  Field_CG_AreaDOF& nativeAreaDOFs,
                  Field_CG_EdgeDOF& nativeEdgeDOFs,
                  Field_CG_NodeDOF& nativeNodeDOFs,
                  int* local2nativeDOFmap )
{
  typedef typename Topology::TopologyTrace TopologyTrace;

  typedef typename FieldTraceGroupClass::BasisType BasisType;
  typedef typename FieldTraceGroupClass::FieldAssociativityConstructorType FieldTraceConstructorType;

  // i've assumed simplices all the way down from here, so assert
  static_assert(Topology::Topology == eTet, "specialized for the pentatope, sorry!");

  // the nodes that define the edges of an "element": definition probably deprecated for 4D objects
  const int (*EdgeNodes)[ Line::NNode ]= ElementEdges<Topology>::EdgeNodes;
  // something probably has to go here for 4D...

  // basis function basics
  const int order= order_;
  const BasisFunctionCategory category= category_;

  // number of DOFs per that first appear on lowest degree entity
  const int nDOFperFace= TopologyDOF_CG<Topology>::count(order);
  const int nDOFperArea= TopologyDOF_CG<TopologyTrace>::count(order);
  const int nDOFperEdge= TopologyDOF_CG<Line>::count(order);

  // get the basis type
  const BasisType* basis= BasisType::getBasisFunction(order, category);

  // create field associativity constructor
  FieldTraceConstructorType fldAssocTrace( basis, xfldTraceGroup.nElem() );

  // maps!
  std::vector<int> nodeMap(Topology::NNode);
  std::vector<int> edgeMap(Topology::NEdge*nDOFperEdge);
  std::vector<int> areaMap(Topology::NFace*nDOFperArea);

  // loop over all traces
  for (int itrace= 0; itrace < xfldTraceGroup.nElem(); itrace++)
  {
    // set the processor rank
    fldAssocTrace.setAssociativity(itrace).setRank( xfldTraceGroup.associativity( itrace ).rank() );

    // get the node mapping of the grid
    xfldTraceGroup.associativity( itrace ).getNodeGlobalMapping( nodeMap.data(), nodeMap.size() );

    if (nDOFperFace > 0)
    {
      SANS_DEVELOPER_EXCEPTION("check implementation on 4D grids when DOFs appear on traces.");

      // retrieve face DOFs and set them
      Field_CG_FaceDOF::FaceDOFType& face= nativeFaceDOFs.getNativeFace(Topology::Topology, nodeMap);
      std::vector<int> faceDOF= nativeFaceDOFs.getLocalFace(Topology::Topology, nodeMap);

      // get the native nodes for the face DOFs
      std::vector<int> nativeNodes(nodeMap.size());
      for (std::size_t i= 0; i < nodeMap.size(); i++)
        nativeNodes[i]= xfld_.local2nativeDOFmap(nodeMap[i]);

      {
        // the native face DOFs
        std::vector<int> nativeFaceDOFs= face.nativeFaceDOFs;

        // get the orientation of the tetrahedral faces to the pentatope
        int orientation= CanonicalOrientation<Topology, TopoD4>::get(nativeNodes, face.nativeNodes);

        // get the corresponding orientations
        orientVolumeDOFs<Topology>( order, category, orientation, faceDOF, nativeFaceDOFs );

        // set the native mapping
        for (int i= 0; i < nDOFperFace; i++)
          local2nativeDOFmap[faceDOF[i]]= nativeFaceDOFs[i];
      }

      fldAssocTrace.setAssociativity(itrace).setCellGlobalMapping(faceDOF.data(), faceDOF.size());
    }

    if (nDOFperArea > 0)
    {
      SANS_DEVELOPER_EXCEPTION("check implementation on 4D grids when DOFs appear on frames/areas.");

      // retrieve area DOFs and set them
      Field_CG_AreaDOF::AreaDOFType& area= nativeAreaDOFs.getNativeArea(Topology::TopologyTrace::Topology, areaMap);
      std::vector<int> areaDOF= nativeAreaDOFs.getLocalArea(Topology::TopologyTrace::Topology, areaMap);

      // get the native nodes for the area DOFs
      std::vector<int> nativeXNodes(areaMap.size());
      for (std::size_t i= 0; i < nodeMap.size(); i++)
        nativeXNodes[i]= xfld_.local2nativeDOFmap(nodeMap[i]);

      {
        // the native area DOFs
        std::vector<int> nativeAreaDOFs= area.nativeAreaDOFs;

        // get the orientation of the triangular areas to the pentatope
        int orientation= CanonicalOrientation<TopologyTrace, TopoD3>::get(nativeXNodes, area.nativeNodes);

        // get the corresponding orientations
        // THIS IS CERTAINLY WRONG ... should be orientFrameDOFs ...
        orientFaceDOFs<TopologyTrace>( order, category, orientation, areaDOF, nativeAreaDOFs );

        // set the native mapping
        for (int i= 0; i < nDOFperFace; i++)
          local2nativeDOFmap[areaDOF[i]]= nativeAreaDOFs[i];
      }

      // set the mapping
      fldAssocTrace.setAssociativity(itrace).setFaceGlobalMapping(areaDOF.data(), areaDOF.size());
    }

    if (nDOFperEdge > 0)
    {
      // Retrieve edge DOFs and set them
      int iedgeDOF = 0;
      for (int iedge = 0; iedge < Topology::NEdge; iedge++)
      {
        // Get the nodes for the canonical edge
        int node0 = nodeMap[EdgeNodes[iedge][0]];
        int node1 = nodeMap[EdgeNodes[iedge][1]];

        Field_CG_EdgeDOF::EdgeDOFType& edge    = nativeEdgeDOFs.getNativeEdge(node0, node1);
        std::vector<int>               edgeDOF = nativeEdgeDOFs.getLocalEdge(node0, node1);

        std::vector<int> nativeEdgeDOFs = edge.nativeEdgeDOFs;

        //Reverse the DOF ordering if the basis is Lagrange and the edge orientation is reversed
        if (category == BasisFunctionCategory_Lagrange && edge.nativeNode0 != xfld_.local2nativeDOFmap(node0))
        {
          std::reverse(edgeDOF.begin(), edgeDOF.end());
          std::reverse(nativeEdgeDOFs.begin(), nativeEdgeDOFs.end());
        }

        // copy over the edge map
        for (int i = 0; i < nDOFperEdge; i++)
        {
          edgeMap[iedgeDOF] = edgeDOF[i];
          local2nativeDOFmap[edgeDOF[i]] = nativeEdgeDOFs[i];
          iedgeDOF++;
        }
      }
      fldAssocTrace.setAssociativity(itrace).setEdgeGlobalMapping(edgeMap);
    }

    // update the node numbering and set node DOFs
    for (int k = 0; k < Topology::NNode; k++)
    {
      Field_CG_NodeDOF::NodeDOFType& node    = nativeNodeDOFs.getNativeNode(nodeMap[k]);
      const int                      nodeDOF = nativeNodeDOFs.getLocalNode(nodeMap[k]);

      nodeMap[k] = nodeDOF;
      local2nativeDOFmap[nodeDOF] = node.nativeNodeDOF;
    }

    fldAssocTrace.setAssociativity(itrace).setNodeGlobalMapping( nodeMap );
  }

  return new FieldTraceGroupClass(fldAssocTrace);
}

} // namespace SANS

#endif  // FIELD_CG_TRACECONSTRUCTOR_H
