// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define FIELD_CG_TRACECONSTRUCTORAREA_INSTANTIATE
#include "Field_CG_TraceConstructorArea_impl.h"

namespace SANS
{

// Explicit instantiations
template struct Field_CG_TraceConstructor<PhysD2, TopoD2>;

} // namespace SANS
