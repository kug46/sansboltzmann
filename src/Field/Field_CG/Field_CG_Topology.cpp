// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "Field_CG_Topology.h"

#include <algorithm> // std::sort
#include <numeric> // std::accumulate

#include "tools/minmax.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#include <boost/mpi/collectives/all_gather.hpp>
#include <boost/mpi/collectives/all_to_all.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/vector.hpp>
#endif

namespace SANS
{

#ifdef SANS_MPI

#define CORY_VERBOSE 0

template<class Archive>
void serialize(Archive & ar, Field_CG_NodeDOF::NodeDOFType& node, const unsigned int version)
{
  ar & node.node;
  ar & node.ranks;
  ar & node.spirit;
  ar & node.nativeNodeDOF;
}

template<class Archive>
void serialize(Archive & ar, Field_CG_EdgeDOF::EdgeDOFType& edge, const unsigned int version)
{
  ar & edge.nativeNode0;
  ar & edge.ranks;
  ar & edge.spirit;
  ar & edge.nativeEdgeDOFs;
}

template<class Archive>
void serialize(Archive & ar, Field_CG_AreaDOF::AreaDOFType& area, const unsigned int version)
{
  ar & area.nativeNodes;
  ar & area.rank;
  ar & area.spirit;
  ar & area.nativeAreaDOFs;
}

template<class Archive>
void serialize(Archive & ar, Field_CG_FaceDOF::FaceDOFType& face, const unsigned int version)
{
  ar & face.nativeNodes;
  ar & face.rank;
  ar & face.spirit;
  ar & face.nativeFaceDOFs;
}

template<class Archive>
void serialize(Archive & ar, Field_CG_CellDOF::CellDOFType& cell, const unsigned int version)
{
  ar & cell.topo;
  ar & cell.rank;
  ar & cell.spirit;
  ar & cell.nativeCellDOFs;
}

#endif


//===========================================================================//
int Topology_DOF_CG(const TopologyTypes topo, const int order)
{
  switch (topo)
  {
  case eLine:
    return TopologyDOF_CG<Line>::count(order);
  case eTriangle:
    return TopologyDOF_CG<Triangle>::count(order);
  case eQuad:
    return TopologyDOF_CG<Quad>::count(order);
  case eTet:
    return TopologyDOF_CG<Tet>::count(order);
  case eHex:
    return TopologyDOF_CG<Hex>::count(order);
  default:
    break;
  }

  SANS_DEVELOPER_EXCEPTION("Unknown topology : %d", topo);
  return eNode;
}

//===========================================================================//
/** insert_node inserts a grid node into the local DOF map if it didn't already exist
 *
 * param    nodeX     the integer number of the grid node of interest
 * param    rank      the integer number of the rank of the processor we're working
 *                        on behalf of
 *
 */
void
Field_CG_NodeDOF::insert_node(const int nodeX, const int rank)
{
  // get the map iterator
  std::map<int,NodeDOFType>::iterator it;

  // the native grid node number for the node of interest
  const int nativeNodeX = xfld_local2nativeDOFmap_[nodeX];

  // check to see if the node  is already in the map
  it = nativeNodeMap_.find(nativeNodeX);

  // if this node wasn't already added,
  if (it == nativeNodeMap_.end())
  {
    // create a new node object corresponding to this node
    NodeDOFType nodeDOF;
    nodeDOF.node = nodeX; // why the xfield node??? it makes sense i guess

    // increase the occurrence of this node owned by this rank (to one)
    ++nodeDOF.ranks[ rank ];

    // possessiveness of the node defaults to zombie
    nodeDOF.spirit = zombie;

    // set invalid native node DOF for now
    nodeDOF.nativeNodeDOF = -1;

    // save off the new node
    nativeNodeMap_[nativeNodeX] = nodeDOF;
  }
  // otherwise, it already existed in the map
  else
  {
    // so we increase the occurrence of this node owned by this rank
    ++it->second.ranks[ rank ];
  }
}

//---------------------------------------------------------------------------//
/** createNativeIndex makes the native mappings for all the DOF nodes handed in
 *
 * createNativeIndex makes the native mappings for the DOF nodes, either in
 * a parallel sense or in a serial one, depending on MPI settings
 *
 * param    nodeDOF_index     pointer to the DOF node indices
 */
void
Field_CG_NodeDOF::
createNativeIndex(int& nodeDOF_index)
{
  // use comm size logic to speed up local solve construction
  if (comm_->size() > 1)
  {

#ifdef SANS_MPI

    // MPI typicals
    const int comm_size = comm_->size();
    const int comm_rank = comm_->rank();

    // vector of processor-wise maps from local DOF node index to DOF objects
    std::vector<std::map<int,NodeDOFType>> localDOF(comm_size);

    // size of native node map across all processors
    int nativeNodeSize = nativeNodeMap_.size();
    nativeNodeSize = boost::mpi::all_reduce(*comm_, nativeNodeSize, std::plus<int>());

    // compute the number of nodes on each processor (approximately at least)
    int NodesPerProc = MAX(1, nativeNodeSize / comm_size);

    // for each native node
    for ( std::pair<const int,NodeDOFType>& key_node : nativeNodeMap_ )
    {
      // native node index
      int nativeNode = key_node.first;

      // compute the rank that should receive the DOF for the Native indexing
      int rank = MIN(comm_size-1, nativeNode / NodesPerProc);

      // populate the map appropriately
      localDOF[rank][nativeNode] = key_node.second;
    }

    // vector of processor-wise maps from native DOF node index to DOF objects
    std::vector<std::map<int,NodeDOFType>> nativeDOF;

    // send this processors local DOFs to everyone!
    boost::mpi::all_to_all(*comm_, localDOF, nativeDOF);

    // partition-wise nodemap
    std::map<int,NodeDOFType> partNodeMap;
    std::map<int,NodeDOFType>::iterator it;

    // loop over the processors
    for (std::size_t rank = 0; rank < nativeDOF.size(); rank++)
    {
      // loop over the DOFs on a given rank
      for (std::pair<const int,NodeDOFType>& key_node : nativeDOF[rank])
      {
        // look to see if the node is already in the map
        it = partNodeMap.find(key_node.first);

        // add the node if it's not in there
        if (it == partNodeMap.end())
        {
          partNodeMap[key_node.first] = key_node.second;
        }
        else
        {
          // use the ranks from the node with the most elements attached
          int noccur0 = 0;
          for (std::pair<const int, int>& occurance : it->second.ranks)
            noccur0 += occurance.second;

          int noccur1 = 0;
          for (std::pair<const int, int>& occurance : key_node.second.ranks)
            noccur1 += occurance.second;

          if (noccur1 > noccur0)
            it->second.ranks = key_node.second.ranks;
        }
      }
    }

    // get the number of nodes on this processor to compute the offset
    int nNode = partNodeMap.size();
    std::vector<int> nNodeOnRank;
    boost::mpi::all_gather(*comm_, nNode, nNodeOnRank);

    // compute the node offset from lower ranks
    int node_offset = std::accumulate(nNodeOnRank.begin(), nNodeOnRank.begin()+comm_rank, 0);

    int nodeIdx = node_offset;
    for ( std::pair<const int,NodeDOFType>& key_node : partNodeMap )
    {
      NodeDOFType& node = key_node.second;

      // create the native indexes for the node DOFs
      node.nativeNodeDOF = nodeDOF_index + nodeIdx++;

      int maxoccur = 0;
      int node_rank = -1;
      for (std::pair<const int, int>& occurance : node.ranks)
        if (occurance.second > maxoccur)
        {
          node_rank = occurance.first;
          maxoccur = occurance.second;
        }

      // only keep the maximum occurrence
      node.ranks.clear();
      node.ranks[node_rank] = maxoccur;

      // update the maps to send back to the processor where they originated
      for (std::size_t rank = 0; rank < nativeDOF.size(); rank++)
      {
        it = nativeDOF[rank].find(key_node.first);

        if (it != nativeDOF[rank].end())
          it->second = node;
      }
    }

    // send the native indexing back to the originating processors
    boost::mpi::all_to_all(*comm_, nativeDOF, localDOF);

    // save off the DOFs with the native indexing
    for (std::size_t rank = 0; rank < localDOF.size(); rank++)
    {
      for (std::pair<const int,NodeDOFType>& key_node : localDOF[rank])
      {
        NodeDOFType& node = key_node.second;

        // set that the node is possessed now that the minimization is complete
        if (node.ranks.begin()->first == comm_rank)
          node.spirit = possessed;

        nativeNodeMap_.at(key_node.first) = node;
      }
    }

    // increment the total number of node DOFs so far
    nodeDOF_index += std::accumulate(nNodeOnRank.begin(), nNodeOnRank.end(), 0);

#endif
  }
  else
  {

    //go through all nodes and construct the DOF indexing
    for ( std::pair<const int,NodeDOFType>& key_node : nativeNodeMap_ )
    {
      NodeDOFType& node = key_node.second;

      // create the indexes for the node DOFs
      node.nativeNodeDOF = nodeDOF_index++;
#if CORY_VERBOSE
      printf("nodeDOF %d added! on node %d.\n", nodeDOF_index - 1, key_node.first);
#endif
      // all nodes are possessed in serial
      node.spirit = possessed;
    }

  }

  // total DOF count on this processor
  nDOF_ = nativeNodeMap_.size();
}

//---------------------------------------------------------------------------//
/** createLocalIndex adds a local index for spirits of a given type
 *
 * createLocalIndex runs through the native node map and when the spirit is of the
 * type specified, it adds the DOF to the localNodeMap
 *
 * param    spirit          spirit (possessitivity) requested
 * param    nodeDOF_index   vector address of the big dataset of grid node indices
 */
void
Field_CG_NodeDOF::
createLocalIndex(const Ghoul spirit, int& nodeDOF_index)
{
  // loop over all the native node map as pairs
  for ( std::pair<const int,NodeDOFType>& key_node : nativeNodeMap_ )
  {
    // the native node index
    const int nativeNode = key_node.first;
    // the DOF object associated
    NodeDOFType& node = key_node.second;

    // if the local index is of the spirit type we're asking for, add it to the local map
    if ( spirit == node.spirit )
      localNodeMap_[nativeNode] = nodeDOF_index++;
  }
}

//---------------------------------------------------------------------------//
/** getDOF_rank get the DOFs possessed by this particular rank
 *
 * param    nDOFpossessed   the number of DOFs possessed locally
 * param    DOF_rank        pointer to the data that _is_ possessed by this process
 */
void
Field_CG_NodeDOF::
getDOF_rank(const int nDOFpossessed, int* DOF_rank)
{
  if ( comm_->size() == 1 ) return; // nothing to do for 1 rank (everything is local)

  // loop over the nativeNodeMap entries
  for ( std::pair<const int,NodeDOFType>& key_node : nativeNodeMap_ )
  {
    // unpack key-value pairs
    const int nativeNodeDOF = key_node.first;
    NodeDOFType& nodeDOF = key_node.second;

    // if the node is NOT possessed by this process ...
    if ( nodeDOF.spirit != possessed )
    {
      // ... set DOF_rank to the process that does possess it
      const int localDOF = localNodeMap_[nativeNodeDOF];
      DOF_rank[localDOF - nDOFpossessed] = nodeDOF.ranks.begin()->first;
    }
  }
}

//---------------------------------------------------------------------------//
/** nDOFghoul counts the numbers of DOFs possessed and ghosted by this process
 */
GhoulDOF
Field_CG_NodeDOF::
nDOFghoul() const
{
  GhoulDOF nDOF;

  for ( const std::pair<const int,NodeDOFType>& key_node : nativeNodeMap_ )
  {
    const NodeDOFType& node = key_node.second;

    // count possessed and ghost DOFs
    if (node.spirit == possessed) nDOF.possessed++;
    if (node.spirit == ghost    ) nDOF.ghost++;
  }

  return nDOF;
}

//---------------------------------------------------------------------------//
/** getNativeNode returns the native node object for a given grid native node index
 *
 * param    nodeX   native node index from the grid/XField
 * return           node dof object
 */
Field_CG_NodeDOF::NodeDOFType&
Field_CG_NodeDOF::
getNativeNode(const int nodeX)
{
  // get the native grid DOF node associated with local node nodeX from the XField
  const int nativeNode = xfld_local2nativeDOFmap_[nodeX];

  // return the native DOF node corresponding to that point!
  return nativeNodeMap_.at(nativeNode);
}

//---------------------------------------------------------------------------//
/** getLocalNode returns the local node map index for a given native node index?
 *
 * param    node    native node index from the grid/XField
 * return           local node index for the DOF object
 */
int
Field_CG_NodeDOF::
getLocalNode(const int nodeX) const
{
  // get the native node associated with local grid node nodeX
  const int nativeNode= xfld_local2nativeDOFmap_[nodeX];

  // return the local DOF node associated with the native grid node
  return localNodeMap_.at(nativeNode);
}

//===========================================================================//
/** insert_edge inserts an edge into the native edge DOF map if it didn't already exist
 *
 * param    node0     the local integer index of the first node of interest
 * param    node1     the local integer index of the second node of interest
 * param    rank      the integer number of the rank of the processor we're working
 *                        on behalf of
 *
 */
void
Field_CG_EdgeDOF::
insert_edge(const int nodeX0, const int nodeX1, const int rank)
{

  // get the map iterator
  std::map<UniqueElem,EdgeDOFType>::iterator it;

  // get the native grid DOFs corresponding to the two local grid DOF nodes handed in
  const int nativeNode0 = xfld_local2nativeDOFmap_[nodeX0];
  const int nativeNode1 = xfld_local2nativeDOFmap_[nodeX1];

  // construct a unique edge based on the bounding grid node DOFs
  UniqueElem key(nativeNode0, nativeNode1);

  // check to see if the edge is already in the map
  it = nativeEdgeMap_.find(key);

  // if this edge wasn't already added
  if (it == nativeEdgeMap_.end())
  {
    EdgeDOFType edge;
    edge.nativeNode0 = std::min(nativeNode0, nativeNode1);

    // increase the occurrence of the rank
    ++edge.ranks[ rank ];

    edge.spirit = zombie;

    // save off the new edge
    nativeEdgeMap_[key] = edge;
  }
  else
  {
    // otherwise increase the occurrence of the rank
    ++it->second.ranks[ rank ];
  }
}

//---------------------------------------------------------------------------//
/** createNativeIndex makes the native mappings for all the DOF nodes on the edges
 *
 * createNativeIndex make the native mappings for all the DOF nodes on the edges,
 * either in a parallel sense or a serial one, depending on the MPI settings
 *
 * param    nodeDOF_index   pointer to the DOF index of interest
 */
void
Field_CG_EdgeDOF::
createNativeIndex(int& edgeDOF_index)
{

  // count the number of interior DOFs on a given edge
  const int nDOFperEdge= TopologyDOF_CG<Line>::count(order_); //(order - 1) for lines

  // Use comm size logic to speed up local solve construction
  if (comm_->size() > 1)
  {
#ifdef SANS_MPI

    // MPI standards
    const int comm_size= comm_->size();
    const int comm_rank= comm_->rank();

    // map from a processor to a local edge DOF object
    std::vector<std::map<UniqueElem,EdgeDOFType>> localDOF(comm_size);

    // number of local edges on this processor
    int nativeEdgeSize= nativeEdgeMap_.size();

    // sum across all processors
    nativeEdgeSize= boost::mpi::all_reduce(*comm_, nativeEdgeSize, std::plus<int>());

    // compute the mean number of edges on each processor (approximately at least)
    int EdgesPerProc= MAX(1, nativeEdgeSize/comm_size);

    // iterate over the unique entities/native edge objects
    for (std::pair<const UniqueElem,EdgeDOFType>& key_edge : nativeEdgeMap_)
    {
      // unpack tuple
      const UniqueElem& key= key_edge.first;

      // compute the rank that should receive the DOF for the native indexing
      int rank= MIN(comm_size - 1, key.sortedNodes()[0]/EdgesPerProc);

      // on that processor, the edge DOF object should be stored by its unique entity
      localDOF[rank][key]= key_edge.second;
    }

    // vector of all the (unique edge->native edge object) maps -> gathered via MPI
    std::vector<std::map<UniqueElem, EdgeDOFType>> nativeDOF;
    boost::mpi::all_to_all(*comm_, localDOF, nativeDOF);

    // mapping from unique entities to edges on this partition
    std::map<UniqueElem, EdgeDOFType> partEdgeMap;

    // map iterator
    std::map<UniqueElem, EdgeDOFType>::iterator it;

    // loop over the processes
    for (std::size_t rank= 0; rank < nativeDOF.size(); rank++)
    {
      // loop over the unique edge-edge object tuples
      for (std::pair<const UniqueElem, EdgeDOFType>& key_edge : nativeDOF[rank])
      {
        // look to see if the edge is already in the map
        it= partEdgeMap.find(key_edge.first);

        // add the edge to the partition map if it's not in there
        if (it == partEdgeMap.end())
        {
          partEdgeMap[key_edge.first]= key_edge.second;
        }
        else
        {
          // otherwise it's already there
          // assign ownership to the ranks from the edge with the most elements attached
          int noccur0= 0;
          for (std::pair<const int, int>& occurance : it->second.ranks)
            noccur0 += occurance.second;

          int noccur1= 0;
          for (std::pair<const int, int>& occurance : key_edge.second.ranks)
            noccur1 += occurance.second;

          if (noccur1 > noccur0)
            it->second.ranks= key_edge.second.ranks;
        }
      }
    }

    // get the number of nodes on this processor to compute the offset
    int nEdge= partEdgeMap.size();
    std::vector<int> nEdgeOnRank;
    boost::mpi::all_gather(*comm_, nEdge, nEdgeOnRank);

    // compute the node offset from lower ranks
    int edge_offset= nDOFperEdge*std::accumulate(nEdgeOnRank.begin(), nEdgeOnRank.begin() + comm_rank, 0);

    // start indexing of native edge DOFs here
    int edgeIdx= edge_offset;

    // loop over partition edge map
    for (std::pair<const UniqueElem,EdgeDOFType>& key_edge : partEdgeMap)
    {
      // grab the edge we're on
      EdgeDOFType& edge= key_edge.second;

      // create the native indexes for the node DOFs by accounting for the offsets
      edge.nativeEdgeDOFs.resize(nDOFperEdge);
      for (int i= 0; i < nDOFperEdge; i++)
      {
        edge.nativeEdgeDOFs[i]= edgeDOF_index + edgeIdx++;
#if CORY_VERBOSE
        if (comm_rank == 0)
          printf("edgeDOF %d added! on edge: {%d, %d}.\n", edgeDOF_index + edgeIdx - 1,
              key_edge.first.sortedNodes()[0], key_edge.first.sortedNodes()[1]);
#endif
      }

      // maximum occurences and rank of interest
      int maxoccur= 0;
      int node_rank= -1;

      // loop over the edge ranks
      for (std::pair<const int, int>& occurance : edge.ranks)
        if (occurance.second > maxoccur)
        {
          // add newly added occurrences to the list
          node_rank= occurance.first;
          maxoccur= occurance.second;
        }

      // only keep the maximum occurrence
      edge.ranks.clear();
      edge.ranks[node_rank]= maxoccur;

      // update the maps to send back to the processor where they originated
      for (std::size_t rank= 0; rank < nativeDOF.size(); rank++)
      {
        it= nativeDOF[rank].find(key_edge.first);

        if (it != nativeDOF[rank].end())
          it->second= edge;
      }
    }

    // send the native indexing back to the originating processors
    boost::mpi::all_to_all(*comm_, nativeDOF, localDOF);

    // save off the DOFs with the native indexing
    // loop over processes
    for (std::size_t rank= 0; rank < localDOF.size(); rank++)
    {
      // loop over (unique entity, edge object) pairs
      for (std::pair<const UniqueElem,EdgeDOFType>& key_edge : localDOF[rank])
      {
        // the edge
        EdgeDOFType& edge= key_edge.second;

        // set that the node is possessed now that the minimization is complete!
        if (edge.ranks.begin()->first == comm_rank)
          edge.spirit= possessed;

        // set the edge into the map
        nativeEdgeMap_.at(key_edge.first)= edge;
      }
    }

    // increment the total number of edge DOFs so far
    edgeDOF_index += nDOFperEdge*std::accumulate(nEdgeOnRank.begin(), nEdgeOnRank.end(), 0);

#endif
  }
  else
  {

    //go through all edges and construct the DOF indexing
    for ( std::pair<const UniqueElem,EdgeDOFType>& key_edge : nativeEdgeMap_ )
    {
      EdgeDOFType& edge = key_edge.second;

      // all edges are possessed in serial
      edge.spirit = possessed;

      // create the indexes for the edge DOFs
      edge.nativeEdgeDOFs.resize(nDOFperEdge);
      for (int i = 0; i < nDOFperEdge; i++)
      {
        edge.nativeEdgeDOFs[i] = edgeDOF_index++;
#if CORY_VERBOSE
        printf("edgeDOF %d added! on edge: {%d, %d}.\n", edgeDOF_index - 1,
            key_edge.first.sortedNodes()[0], key_edge.first.sortedNodes()[1]);
#endif
      }
    }

  }

  // total DOF count on this processor
  nDOF_ = nativeEdgeMap_.size()*nDOFperEdge;
}

//---------------------------------------------------------------------------//
/** createLocalIndex adds a local index for spirits of a given type
 *
 * createLocalIndex runs through the native node map and when the spirit is
 * of the type specified, it adds the edge to the localEdgeMap
 *
 * param    spirit          spirit (possessivity requested)
 * param    edgeDOF_index   vector address of the big dataset of edge DOF indices
 */
void
Field_CG_EdgeDOF::
createLocalIndex(const Ghoul spirit, int& edgeDOF_index)
{
  // loop over all the (unique edge, native edge DOF) maps
  for ( std::pair<const UniqueElem,EdgeDOFType>& key_edge : nativeEdgeMap_ )
  {
    // unpack tuple
    const UniqueElem& key = key_edge.first;
    EdgeDOFType& edge = key_edge.second;

    // if this edge is of the correct type
    if ( spirit == edge.spirit )
    {
      // grab the local native node DOFs for the edge
      std::vector<int>& localDOF = localEdgeMap_[key];

      // add it to the front and increase the edge DOF count
      localDOF.resize(edge.nativeEdgeDOFs.size());
      for (std::size_t i = 0; i < localDOF.size(); i++)
        localDOF[i] = edgeDOF_index++;
    }
  }
}

//---------------------------------------------------------------------------//
/** getDOF_rank get the edges possessed by this particular rank
 *
 * param  nDOFpossessed   the number of DOFs possessed locally
 * param  DOF_rank        pointer to the data that __is__ possessed by this process
 */
void
Field_CG_EdgeDOF::
getDOF_rank(const int nDOFpossessed, int* DOF_rank)
{
  if ( comm_->size() == 1 ) return; // nothing to do for 1 rank

  // loop over the pairs of unique elements and their corresponding DOF objects
  for ( std::pair<const UniqueElem,EdgeDOFType>& key_edge : nativeEdgeMap_ )
  {
    // depair key-values
    const UniqueElem& key = key_edge.first;
    EdgeDOFType& edge = key_edge.second;

    // if it's possessed...
    if ( edge.spirit != possessed )
    {
      // ... get the local DOFs for the edge
      std::vector<int>& localDOF = localEdgeMap_[key];

      for (std::size_t i = 0; i < localDOF.size(); i++)
        DOF_rank[localDOF[i] - nDOFpossessed] = edge.ranks.begin()->first;
    }
  }
}

//---------------------------------------------------------------------------//
/** nDOFghoul counts the numbers of edge DOFs possessed and ghosted by this process
 */
GhoulDOF
Field_CG_EdgeDOF::
nDOFghoul() const
{
  GhoulDOF nDOF;

  for ( const std::pair<const UniqueElem,EdgeDOFType>& key_edge : nativeEdgeMap_ )
  {
    const EdgeDOFType& edge = key_edge.second;

    // count possessed and ghost DOFs
    if (edge.spirit == possessed) nDOF.possessed += edge.nativeEdgeDOFs.size();
    if (edge.spirit == ghost    ) nDOF.ghost     += edge.nativeEdgeDOFs.size();
  }

  return nDOF;
}

//---------------------------------------------------------------------------//
/** getNativeEdge returns the native node object for a given pair of grid native node indices
 *
 * param nodeX0   the first native node index fron the grid/XField
 * param nodeX1   the second native node index fron the grid/XField
 */
Field_CG_EdgeDOF::EdgeDOFType&
Field_CG_EdgeDOF::
getNativeEdge(const int nodeX0, const int nodeX1)
{
  // the native grid indices
  const int nativeNode0 = xfld_local2nativeDOFmap_[nodeX0];
  const int nativeNode1 = xfld_local2nativeDOFmap_[nodeX1];

  // construct a unique element based on the grid nodes
  UniqueElem key(nativeNode0, nativeNode1);

  // return the edge object corresponding to the unique edge
  return nativeEdgeMap_.at(key);
}

//---------------------------------------------------------------------------//
//---------------------------------------------------------------------------//
/** getLocalEdge returns the local edge indices for a given pair of grid native node indices
 *
 * param nodeX0   the first native node index fron the grid/XField
 * param nodeX1   the second native node index fron the grid/XField
 */
const std::vector<int>&
Field_CG_EdgeDOF::
getLocalEdge(const int nodeX0, const int nodeX1) const
{
  // the native grid indices
  const int nativeNode0 = xfld_local2nativeDOFmap_[nodeX0];
  const int nativeNode1 = xfld_local2nativeDOFmap_[nodeX1];

  // construct a unique element based on the nodes
  UniqueElem key(nativeNode0, nativeNode1);

  // return the edge DOF node indices
  return localEdgeMap_.at(key);
}

//===========================================================================//
void
Field_CG_FaceDOF::
insert_face(const TopologyTypes& topo, const std::vector<int>& nodes, const int rank)
{
  std::map<UniqueElem,FaceDOFType>::iterator it;

  std::vector<int> nativeNodes(nodes.size());
  for (std::size_t i = 0; i < nodes.size(); i++)
    nativeNodes[i] = xfld_local2nativeDOFmap_[nodes[i]];

  // construct a unique element based on the nodes
  UniqueElem key(topo, nativeNodes);

  // check to see if the edge is already in the map
  it = nativeFaceMap_.find(key);

  //if this edge wasn't already added
  if (it == nativeFaceMap_.end())
  {
    if (topo == eTriangle)
    {
      // simply sort the nodes for a triangle to define a unique orientation
      std::sort(nativeNodes.begin(), nativeNodes.end());
    }
    else if (topo == eQuad)
    {
      // rotate the nodes so the minimum nodes is first
      int minNode = *std::min_element(nativeNodes.begin(), nativeNodes.end());
      while ( nativeNodes.front() != minNode )
        std::rotate(nativeNodes.begin(), nativeNodes.begin()+1, nativeNodes.end());

      // finally swap two nodes to give a unique orientation
      if (nativeNodes[1] > nativeNodes[3])
        std::swap(nativeNodes[1], nativeNodes[3]);
    }

    FaceDOFType face;
    face.nativeNodes = std::move(nativeNodes);

    face.rank = rank;

    face.spirit = zombie;

    // save off the new edge
    nativeFaceMap_[key] = std::move(face);
  }
  else
  {
    // otherwise update the minimum rank
    it->second.rank = std::min(it->second.rank, rank);
  }
}


//---------------------------------------------------------------------------//
void
Field_CG_FaceDOF::
createNativeIndex(int& faceDOF_index)
{
  nDOF_ = 0;
  int nDOFperFace = 0;

  // Use comm size logic to speed up local solve construction
  if (comm_->size() > 1)
  {

#ifdef SANS_MPI

    const int comm_size = comm_->size();
    const int comm_rank = comm_->rank();
    std::vector<std::map<UniqueElem,FaceDOFType>> localDOF(comm_size);

    int nativeFaceSize = nativeFaceMap_.size();
    nativeFaceSize = boost::mpi::all_reduce(*comm_, nativeFaceSize, std::plus<int>());

    // Compute the number of Faces on each processor (approximately at least)
    int FacesPerProc = MAX(1, nativeFaceSize / comm_size);

    for ( std::pair<const UniqueElem,FaceDOFType>& key_face : nativeFaceMap_ )
    {
      const UniqueElem& key = key_face.first;

      // Compute the rank that should receive the DOF for the Native indexing
      int rank = MIN(comm_size-1, key.sortedNodes()[0] / FacesPerProc);

      localDOF[rank][key] = key_face.second;
    }

    std::vector<std::map<UniqueElem,FaceDOFType>> nativeDOF;
    boost::mpi::all_to_all(*comm_, localDOF, nativeDOF);

    std::map<UniqueElem,FaceDOFType> partFaceMap;
    std::map<UniqueElem,FaceDOFType>::iterator it;

    int nFaceDOF = 0;
    for (std::size_t rank = 0; rank < nativeDOF.size(); rank++)
    {
      for (std::pair<const UniqueElem,FaceDOFType>& key_face : nativeDOF[rank])
      {
        //Look to see if the node is already in the map
        it = partFaceMap.find(key_face.first);

        // add the node if it's not in there
        if (it == partFaceMap.end())
        {
          partFaceMap[key_face.first] = key_face.second;

          nDOFperFace = Topology_DOF_CG(key_face.first.topo, order_);

          nFaceDOF += nDOFperFace;
        }
        else
        {
          // make sure the rank is minimized (needed for ghost/zombies)
          it->second.rank = std::min(it->second.rank, key_face.second.rank);
        }
      }
    }

    // get the number of face DOFs on this processor to compute the offset
    std::vector<int> nFaceDOFOnRank;
    boost::mpi::all_gather(*comm_, nFaceDOF, nFaceDOFOnRank);

    // compute the node offset from lower ranks
    int face_offset = std::accumulate(nFaceDOFOnRank.begin(), nFaceDOFOnRank.begin()+comm_rank, 0);

    int faceIdx = face_offset;
    for ( std::pair<const UniqueElem,FaceDOFType>& key_face : partFaceMap )
    {
      FaceDOFType& face = key_face.second;

      nDOFperFace = Topology_DOF_CG(key_face.first.topo, order_);

      // create the native indexes for the node DOFs
      face.nativeFaceDOFs.resize(nDOFperFace);
      for (int i = 0; i < nDOFperFace; i++)
        face.nativeFaceDOFs[i] = faceDOF_index + faceIdx++;

      // update the maps to send back to the processor where they originated
      for (std::size_t rank = 0; rank < nativeDOF.size(); rank++)
      {
        it = nativeDOF[rank].find(key_face.first);

        if (it != nativeDOF[rank].end())
          it->second = face;
      }
    }

    // send the native indexing back to the originating processors
    boost::mpi::all_to_all(*comm_, nativeDOF, localDOF);

    // save off the DOFs with the native indexing
    for (std::size_t rank = 0; rank < localDOF.size(); rank++)
    {
      for (std::pair<const UniqueElem,FaceDOFType>& key_face : localDOF[rank])
      {
        FaceDOFType& face = key_face.second;

        // set that the node is possessed now that the minimization is complete
        if (face.rank == comm_rank)
          face.spirit = possessed;

        nativeFaceMap_.at(key_face.first) = face;

        // total DOF count on this processor
        nDOF_ += face.nativeFaceDOFs.size();
      }
    }

    // increment the total number of edge DOFs so far
    faceDOF_index += std::accumulate(nFaceDOFOnRank.begin(), nFaceDOFOnRank.end(), 0);
#endif
  }
  else
  {

    //go through all edges and construct the DOF indexing
    for ( std::pair<const UniqueElem,FaceDOFType>& key_face : nativeFaceMap_ )
    {
      FaceDOFType& face = key_face.second;

      nDOFperFace = Topology_DOF_CG(key_face.first.topo, order_);

      // all face are possessed in serial
      face.spirit = possessed;

      // create the indexes for the edge DOFs
      face.nativeFaceDOFs.resize(nDOFperFace);
      for (int i = 0; i < nDOFperFace; i++)
      {
        face.nativeFaceDOFs[i] = faceDOF_index++;
#if CORY_VERBOSE
        if (key_face.first.topo == ePentatope)
          printf("faceDOF %d added! on face {%d, %d, %d, %d}.\n", faceDOF_index - 1,
              key_face.first.sortedNodes()[0], key_face.first.sortedNodes()[1],
              key_face.first.sortedNodes()[2], key_face.first.sortedNodes()[3]);
#endif
      }

      // total DOF count
      nDOF_ += nDOFperFace;
    }

  }

}

//---------------------------------------------------------------------------//
void
Field_CG_FaceDOF::
createLocalIndex(const Ghoul spirit, int& faceDOF_index)
{
  for ( const std::pair<const UniqueElem,FaceDOFType>& key_face : nativeFaceMap_ )
  {
    const UniqueElem& key = key_face.first;
    const FaceDOFType& face = key_face.second;

    if ( spirit == face.spirit )
    {
      std::vector<int>& localDOF = localFaceMap_[key];

      localDOF.resize(face.nativeFaceDOFs.size());
      for (std::size_t i = 0; i < localDOF.size(); i++)
        localDOF[i] = faceDOF_index++;
    }
  }
}

//---------------------------------------------------------------------------//
void
Field_CG_FaceDOF::
getDOF_rank(const int nDOFpossessed, int* DOF_rank)
{
  if ( comm_->size() == 1 ) return; // nothing to do for 1 rank

  for ( std::pair<const UniqueElem,FaceDOFType>& key_face : nativeFaceMap_ )
  {
    const UniqueElem& key = key_face.first;
    FaceDOFType& face = key_face.second;

    if ( face.spirit != possessed )
    {
      std::vector<int>& localDOF = localFaceMap_[key];

      for (std::size_t i = 0; i < localDOF.size(); i++)
        DOF_rank[localDOF[i] - nDOFpossessed] = face.rank;
    }
  }
}

//---------------------------------------------------------------------------//
GhoulDOF
Field_CG_FaceDOF::
nDOFghoul() const
{
  GhoulDOF nDOF;

  for ( const std::pair<const UniqueElem,FaceDOFType>& key_face : nativeFaceMap_ )
  {
    const FaceDOFType& face = key_face.second;

    // count possessed and ghost DOFs
    if (face.spirit == possessed) nDOF.possessed += face.nativeFaceDOFs.size();
    if (face.spirit == ghost    ) nDOF.ghost     += face.nativeFaceDOFs.size();
  }

  return nDOF;
}

//---------------------------------------------------------------------------//
Field_CG_FaceDOF::FaceDOFType&
Field_CG_FaceDOF::
getNativeFace(const TopologyTypes& topo, const std::vector<int>& nodes)
{
  std::vector<int> nativeNodes(nodes.size());
  for (std::size_t i = 0; i < nodes.size(); i++)
    nativeNodes[i] = xfld_local2nativeDOFmap_[nodes[i]];

  // construct a unique element based on the nodes
  UniqueElem key(topo, std::move(nativeNodes));

  return nativeFaceMap_.at(key);
}

//---------------------------------------------------------------------------//
const std::vector<int>&
Field_CG_FaceDOF::
getLocalFace(const TopologyTypes& topo, const std::vector<int>& nodes) const
{
  std::vector<int> nativeNodes(nodes.size());
  for (std::size_t i = 0; i < nodes.size(); i++)
    nativeNodes[i] = xfld_local2nativeDOFmap_[nodes[i]];

  // construct a unique element based on the nodes
  UniqueElem key(topo, std::move(nativeNodes));

  return localFaceMap_.at(key);
}

/// AREA DOF

//===========================================================================//
void
Field_CG_AreaDOF::
insert_area(const TopologyTypes& topo, const std::vector<int>& nodes, const int rank)
{

  // // i have no reason to think this is broken ... it should work exactly the
  // // same way as the face version...
  // SANS_DEVELOPER_EXCEPTION("check implementation. -CVF");

  std::map<UniqueElem,AreaDOFType>::iterator it;

  std::vector<int> nativeNodes(nodes.size());
  for (std::size_t i = 0; i < nodes.size(); i++)
    nativeNodes[i]= xfld_local2nativeDOFmap_[nodes[i]];

  // construct a unique element based on the corner native grid nodes
  UniqueElem key(topo, nativeNodes);

  // check to see if the area is already in the map
  it= nativeAreaMap_.find(key);

  // if this area wasn't already added
  if (it == nativeAreaMap_.end())
  {
    if (topo == eTriangle)
    {
      // simply sort the nodes for a triangle to define a unique orientation
      std::sort(nativeNodes.begin(), nativeNodes.end());
    }
    else
      SANS_DEVELOPER_EXCEPTION("unsupported frame element");

    AreaDOFType area;
    area.nativeNodes= std::move(nativeNodes);

    area.rank= rank;

    area.spirit= zombie;

    // save off the new edge
    nativeAreaMap_[key] = std::move(area);
  }
  else
  {
    // otherwise update the minimum rank
    it->second.rank = std::min(it->second.rank, rank);
  }
}

//---------------------------------------------------------------------------//
void
Field_CG_AreaDOF::
createNativeIndex(int& areaDOF_index)
{

  // // this exception should actually get thrown below, under certain cases
  // SANS_DEVELOPER_EXCEPTION("check implementation. -CVF");

  nDOF_= 0;
  int nDOFperArea= 0;

  // Use comm size logic to speed up local solve construction
  if (comm_->size() > 1)
  {

#ifdef SANS_MPI

    const int comm_size= comm_->size();
    const int comm_rank= comm_->rank();
    std::vector<std::map<UniqueElem, AreaDOFType>> localDOF(comm_size);

    int nativeAreaSize= nativeAreaMap_.size();
    nativeAreaSize= boost::mpi::all_reduce(*comm_, nativeAreaSize, std::plus<int>());

    // Compute the number of A=aareas on each processor (approximately at least)
    int AreasPerProc= MAX(1, nativeAreaSize/comm_size);

    for (std::pair<const UniqueElem, AreaDOFType>& key_area : nativeAreaMap_)
    {
      const UniqueElem& key= key_area.first;

      // compute the rank that should receive the DOF for the Native indexing
      int rank= MIN(comm_size - 1, key.sortedNodes()[0]/AreasPerProc);

      // on this processor, the area dof object should be stored by its unique entity
      localDOF[rank][key]= key_area.second;
    }

    // vector of all the (unique area-> native area object) maps -> gathered via MPI
    std::vector<std::map<UniqueElem, AreaDOFType>> nativeDOF;
    boost::mpi::all_to_all(*comm_, localDOF, nativeDOF);

    // mapping from unique entities to edges on this partition & iterator
    std::map<UniqueElem, AreaDOFType> partAreaMap;
    std::map<UniqueElem, AreaDOFType>::iterator it;

    int nAreaDOF= 0;

    // loop over all the processes
    for (std::size_t rank= 0; rank < nativeDOF.size(); rank++)
    {
      // loop over the unique area-area object tuples
      for (std::pair<const UniqueElem,AreaDOFType>& key_area : nativeDOF[rank])
      {
        // look to see if the area is already in the map
        it= partAreaMap.find(key_area.first);

        // add the node if it's not in there
        if (it == partAreaMap.end())
        {
          partAreaMap[key_area.first]= key_area.second;

          nDOFperArea= Topology_DOF_CG(key_area.first.topo, order_);

          nAreaDOF += nDOFperArea;
        }
        else
        {
          // make sure the rank is minimized (needed for ghost/zombies)
          it->second.rank= std::min(it->second.rank, key_area.second.rank);
        }
      }
    }

    // get the number of area DOFs on this processor to compute the offset
    std::vector<int> nAreaDOFOnRank;
    boost::mpi::all_gather(*comm_, nAreaDOF, nAreaDOFOnRank);

    // for (unsigned int i= 0; i < nAreaDOFOnRank.size(); i++)
    //   if (nAreaDOFOnRank[i] != 0)
    //     SANS_DEVELOPER_EXCEPTION("check implementation when areadofs appear. -CVF");

    // compute the node offset from lower ranks
    int area_offset= std::accumulate(nAreaDOFOnRank.begin(), nAreaDOFOnRank.begin() + comm_rank, 0);

    int areaIdx= area_offset;
    for (std::pair<const UniqueElem,AreaDOFType>& key_area : partAreaMap)
    {
      AreaDOFType& area= key_area.second;

      nDOFperArea= Topology_DOF_CG(key_area.first.topo, order_);

      // create the native indexes for the node DOFs
      area.nativeAreaDOFs.resize(nDOFperArea);
      for (int i= 0; i < nDOFperArea; i++)
      {
        area.nativeAreaDOFs[i]= areaDOF_index + areaIdx++;
#if CORY_VERBOSE
        if (key_area.first.topo == ePentatope)
          printf("areaDOF %d added! on area {%d, %d, %d}.\n", areaDOF_index + areaIdx - 1,
              key_area.first.sortedNodes()[0], key_area.first.sortedNodes()[1],
              key_area.first.sortedNodes()[2]);
#endif
      }

      // update the maps to send back to the processor where they originated
      for (std::size_t rank= 0; rank < nativeDOF.size(); rank++)
      {
        it= nativeDOF[rank].find(key_area.first);

        if (it != nativeDOF[rank].end())
          it->second= area;
      }
    }

    // send the native indexing back to the originating processors
    boost::mpi::all_to_all(*comm_, nativeDOF, localDOF);

    // save off the DOFs with the native indexing
    for (std::size_t rank= 0; rank < localDOF.size(); rank++)
    {
      for (std::pair<const UniqueElem, AreaDOFType>& key_area : localDOF[rank])
      {
        AreaDOFType& area= key_area.second;

        // set that the node is possessed now that the minimization is complete
        if (area.rank == comm_rank)
          area.spirit= possessed;

        nativeAreaMap_.at(key_area.first)= area;

        // total DOF count on this processor
        nDOF_ += area.nativeAreaDOFs.size();
      }
    }

    // increment the total number of area DOFs so far
    areaDOF_index += std::accumulate(nAreaDOFOnRank.begin(), nAreaDOFOnRank.end(), 0);
#endif
  }
  else
  {

    // go through all areas and construct the DOF indexing
    for (std::pair<const UniqueElem,AreaDOFType>& key_area : nativeAreaMap_)
    {
      AreaDOFType& area= key_area.second;

      nDOFperArea= Topology_DOF_CG(key_area.first.topo, order_);

      // if (nDOFperArea != 0)
      //   SANS_DEVELOPER_EXCEPTION("check implementation before adding areadofs! -CVF");

      // all areas are possessed in serial
      area.spirit = possessed;

      // create the indexes for the area DOFs
      area.nativeAreaDOFs.resize(nDOFperArea);
      for (int i = 0; i < nDOFperArea; i++)
      {
        area.nativeAreaDOFs[i]= areaDOF_index++;
#if CORY_VERBOSE
        if (key_area.first.topo == ePentatope)
          printf("areaDOF %d added! on area {%d, %d, %d}.\n", areaDOF_index - 1,
              key_area.first.sortedNodes()[0], key_area.first.sortedNodes()[1],
              key_area.first.sortedNodes()[2]);
#endif
      }

      // total DOF count
      nDOF_ += nDOFperArea;
    }

  }

}

//---------------------------------------------------------------------------//
void
Field_CG_AreaDOF::
createLocalIndex(const Ghoul spirit, int& areaDOF_index)
{
  for (const std::pair<const UniqueElem, AreaDOFType>& key_area : nativeAreaMap_)
  {
    const UniqueElem& key= key_area.first;
    const AreaDOFType& area= key_area.second;

    if (spirit == area.spirit)
    {
      std::vector<int>& localDOF= localAreaMap_[key];

      localDOF.resize(area.nativeAreaDOFs.size());
      for (std::size_t i= 0; i < localDOF.size(); i++)
        localDOF[i]= areaDOF_index++;
    }
  }
}

//---------------------------------------------------------------------------//
void
Field_CG_AreaDOF::
getDOF_rank(const int nDOFpossessed, int* DOF_rank)
{
  if (comm_->size() == 1) return; // nothing to do for 1 rank

  for (std::pair<const UniqueElem, AreaDOFType>& key_area : nativeAreaMap_)
  {
    const UniqueElem& key= key_area.first;
    AreaDOFType& area= key_area.second;

    if (area.spirit != possessed)
    {
      std::vector<int>& localDOF= localAreaMap_[key];

      for (std::size_t i= 0; i < localDOF.size(); i++)
        DOF_rank[localDOF[i] - nDOFpossessed]= area.rank;
    }
  }
}

//---------------------------------------------------------------------------//
GhoulDOF
Field_CG_AreaDOF::
nDOFghoul() const
{
  GhoulDOF nDOF;

  for (const std::pair<const UniqueElem, AreaDOFType>& key_area : nativeAreaMap_)
  {
    const AreaDOFType& area= key_area.second;

    // count possessed and ghost DOFs
    if (area.spirit == possessed) nDOF.possessed += area.nativeAreaDOFs.size();
    if (area.spirit == ghost    ) nDOF.ghost     += area.nativeAreaDOFs.size();
  }

  return nDOF;
}

//---------------------------------------------------------------------------//
Field_CG_AreaDOF::AreaDOFType&
Field_CG_AreaDOF::
getNativeArea(const TopologyTypes& topo, const std::vector<int>& nodes)
{
  // SANS_DEVELOPER_EXCEPTION("check implementation. -CVF");
  // // it'd be hard to get this one wrong... not impossible though

  // get the native grid DOF indices for the outside of the area
  std::vector<int> nativeNodes(nodes.size());
  for (std::size_t i = 0; i < nodes.size(); i++)
    nativeNodes[i]= xfld_local2nativeDOFmap_[nodes[i]];

  // construct a unique element based on these nodes
  UniqueElem key(topo, std::move(nativeNodes));

  return nativeAreaMap_.at(key);
}

//---------------------------------------------------------------------------//
const std::vector<int>&
Field_CG_AreaDOF::
getLocalArea(const TopologyTypes& topo, const std::vector<int>& nodes) const
{
  // SANS_DEVELOPER_EXCEPTION("check implementation. -CVF");
  // // it'd be hard to get this one wrong... not impossible though

  // get the native grid DOF indices for the outside of the area
  std::vector<int> nativeNodes(nodes.size());
  for (std::size_t i = 0; i < nodes.size(); i++)
    nativeNodes[i] = xfld_local2nativeDOFmap_[nodes[i]];

  // construct a unique element based on the nodes
  UniqueElem key(topo, std::move(nativeNodes));

  return localAreaMap_.at(key);
}

/// END AREA DOF


//===========================================================================//
void
Field_CG_CellDOF::
insert_cell(const TopologyTypes& topo, const int cellID, const int rank)
{
  CellDOFType cell;

  cell.topo = topo;

  cell.rank = rank;

  cell.spirit = zombie;

  // save off the new cell
  nativeCellMap_[cellID] = cell;
}

//---------------------------------------------------------------------------//
void
Field_CG_CellDOF::
createNativeIndex(int& cellDOF_index)
{
  nDOF_ = 0;
  int nDOFperCell = 0;

  // Use comm size logic to speed up local solve construction
  if (comm_->size() > 1)
  {

#ifdef SANS_MPI

    const int comm_size = comm_->size();
    const int comm_rank = comm_->rank();
    std::vector<std::map<int,CellDOFType>> localDOF(comm_size);

    int nativeCellSize = nativeCellMap_.size();
    nativeCellSize = boost::mpi::all_reduce(*comm_, nativeCellSize, std::plus<int>());

    // Compute the number of Faces on each processor (approximately at least)
    int CellsPerProc = MAX(1, nativeCellSize / comm_size);

    for ( std::pair<const int,CellDOFType>& key_cell : nativeCellMap_ )
    {
      const int cellID = key_cell.first;

      // Compute the rank that should receive the DOF for the Native indexing
      int rank = MIN(comm_size-1, cellID / CellsPerProc);

      localDOF[rank][cellID] = key_cell.second;
    }

    std::vector<std::map<int,CellDOFType>> nativeDOF;
    boost::mpi::all_to_all(*comm_, localDOF, nativeDOF);

    std::map<int,CellDOFType> partFaceMap;
    std::map<int,CellDOFType>::iterator it;

    int nCellDOF = 0;
    for (std::size_t rank = 0; rank < nativeDOF.size(); rank++)
    {
      for (std::pair<const int,CellDOFType>& key_cell : nativeDOF[rank])
      {
        //Look to see if the node is already in the map
        it = partFaceMap.find(key_cell.first);

        // add the node if it's not in there
        // do not minimize ranks for cells
        if (it == partFaceMap.end())
        {
          partFaceMap[key_cell.first] = key_cell.second;

          nDOFperCell = Topology_DOF_CG(key_cell.second.topo, order_);

          nCellDOF += nDOFperCell;
        }
      }
    }

    // get the number of face DOFs on this processor to compute the offset
    std::vector<int> nCellDOFOnRank;
    boost::mpi::all_gather(*comm_, nCellDOF, nCellDOFOnRank);

    // compute the node offset from lower ranks
    int cell_offset = std::accumulate(nCellDOFOnRank.begin(), nCellDOFOnRank.begin()+comm_rank, 0);

    int cellIdx = cell_offset;
    for ( std::pair<const int,CellDOFType>& key_cell : partFaceMap )
    {
      CellDOFType& cell = key_cell.second;

      nDOFperCell = Topology_DOF_CG(cell.topo, order_);

      // create the native indexes for the node DOFs
      cell.nativeCellDOFs.resize(nDOFperCell);
      for (int i = 0; i < nDOFperCell; i++)
        cell.nativeCellDOFs[i] = cellDOF_index + cellIdx++;

      // update the maps to send back to the processor where they originated
      for (std::size_t rank = 0; rank < nativeDOF.size(); rank++)
      {
        it = nativeDOF[rank].find(key_cell.first);

        if (it != nativeDOF[rank].end())
          it->second.nativeCellDOFs = cell.nativeCellDOFs;
      }
    }

    // send the native indexing back to the originating processors
    boost::mpi::all_to_all(*comm_, nativeDOF, localDOF);

    // save off the DOFs with the native indexing
    for (std::size_t rank = 0; rank < localDOF.size(); rank++)
    {
      for (std::pair<const int,CellDOFType>& key_cell : localDOF[rank])
      {
        CellDOFType& cell = key_cell.second;

        // set that the node is possessed now that the minimization is complete
        if (cell.rank == comm_rank)
          cell.spirit = possessed;

        nativeCellMap_.at(key_cell.first) = cell;

        // total DOF count on this processor
        nDOF_ += cell.nativeCellDOFs.size();
      }
    }

    // increment the total number of cell DOFs so far
    cellDOF_index += std::accumulate(nCellDOFOnRank.begin(), nCellDOFOnRank.end(), 0);

#endif
  }
  else
  {

    //go through all cells and construct the DOF indexing
    for ( std::pair<const int,CellDOFType>& key_cell : nativeCellMap_ )
    {
      CellDOFType& cell = key_cell.second;

      nDOFperCell = Topology_DOF_CG(cell.topo, order_);

      // all cells are possessed in serial
      cell.spirit = possessed;

      // create the indexes for the cell DOFs
      cell.nativeCellDOFs.resize(nDOFperCell);
      for (int i = 0; i < nDOFperCell; i++)
      {
        cell.nativeCellDOFs[i] = cellDOF_index++;
#if CORY_VERBOSE
        printf("cellDOF %d added! on cell %d.\n", cellDOF_index - 1,
            key_cell.first);
#endif
      }

      // total DOF count
      nDOF_ += nDOFperCell;
    }
  }

}

//---------------------------------------------------------------------------//
void
Field_CG_CellDOF::
createLocalIndex(const Ghoul spirit, int& cellDOF_index)
{
  for ( std::pair<const int,CellDOFType>& key_cell : nativeCellMap_ )
  {
    const int cellID = key_cell.first;
    CellDOFType& cell = key_cell.second;

    if ( spirit == cell.spirit )
    {
      std::vector<int>& localDOF = localCellMap_[cellID];

      localDOF.resize(cell.nativeCellDOFs.size());
      for (std::size_t i = 0; i < localDOF.size(); i++)
        localDOF[i] = cellDOF_index++;
    }
  }
}

//---------------------------------------------------------------------------//
void
Field_CG_CellDOF::
getDOF_rank(const int nDOFpossessed, int* DOF_rank)
{
  if ( comm_->size() == 1 ) return; // nothing to do for 1 rank

  for ( std::pair<const int,CellDOFType>& key_cell : nativeCellMap_ )
  {
    const int cellID = key_cell.first;
    CellDOFType& cell = key_cell.second;

    if ( cell.spirit != possessed )
    {
      std::vector<int>& localDOF = localCellMap_[cellID];

      for (std::size_t i = 0; i < localDOF.size(); i++)
        DOF_rank[localDOF[i] - nDOFpossessed] = cell.rank;
    }
  }
}

//---------------------------------------------------------------------------//
GhoulDOF
Field_CG_CellDOF::
nDOFghoul() const
{
  GhoulDOF nDOF;

  for ( const std::pair<const int,CellDOFType>& key_cell : nativeCellMap_ )
  {
    const CellDOFType& cell = key_cell.second;

    // count possessed and ghost DOFs
    if (cell.spirit == possessed) nDOF.possessed += cell.nativeCellDOFs.size();
    if (cell.spirit == ghost    ) nDOF.ghost     += cell.nativeCellDOFs.size();
  }

  return nDOF;
}

//---------------------------------------------------------------------------//
Field_CG_CellDOF::CellDOFType&
Field_CG_CellDOF::
getNativeCell(const int elem, const int group)
{
  const std::vector<int>& cellID = cellIDs_[group];

  return nativeCellMap_.at(cellID[elem]);
}

//---------------------------------------------------------------------------//
const std::vector<int>&
Field_CG_CellDOF::
getLocalCell(const int elem, const int group) const
{
  const std::vector<int>& cellID = cellIDs_[group];

  return localCellMap_.at(cellID[elem]);
}

}
