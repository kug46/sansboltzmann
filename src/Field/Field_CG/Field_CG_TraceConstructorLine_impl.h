// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(FIELD_CG_TRACECONSTRUCTORLINE_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "Field_CG_TraceConstructor.h"
#include "Field_CG_Topology.h"

#include "Topology/ElementTopology.h"

#include "Field/Field.h"
#include "Field/XFieldLine.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

//===========================================================================//
template <class PhysDim>
Field_CG_TraceConstructor<PhysDim, TopoD1>::
Field_CG_TraceConstructor(const XField<PhysDim, TopoD1>& xfld, const int order, const BasisFunctionCategory category,
                          const std::vector<std::vector<int>>& interiorGroupSets,
                          const std::vector<std::vector<int>>& boundaryGroupSets ) :
  xfld_(xfld),
  interiorGroupSets_(interiorGroupSets),
  boundaryGroupSets_(boundaryGroupSets),
  order_(order), category_(category), nElem_(0), nDOF_(0), nDOFpossessed_(0), nDOFghost_(0)
{
  SANS_ASSERT(interiorGroupSets_.size() == boundaryGroupSets_.size());

  for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
  {
    nativeNodeDOFs_.emplace_back( xfld_ );
  }

  for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
  {
    const std::vector<int>& interiorGroups = interiorGroupSets_[i];
    for (std::size_t igroup = 0; igroup < interiorGroups.size(); igroup++)
    {
      const int group = interiorGroups[igroup];
      nElem_ += xfld_.getInteriorTraceGroupBase(group).nElem();

      if ( xfld_.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Node) )
      {
        nativeNodeDOFs_[i].insertNodes( xfld_.template getInteriorTraceGroup<Node>(group) );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Unknown element topology." );
    }
  }

  for (std::size_t i = 0; i < boundaryGroupSets_.size(); i++)
  {
    const std::vector<int>& boundaryGroups = boundaryGroupSets_[i];
    for (std::size_t igroup = 0; igroup < boundaryGroups.size(); igroup++)
    {
      const int group = boundaryGroups[igroup];
      nElem_ += xfld_.getBoundaryTraceGroupBase(group).nElem();

      if ( xfld_.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Node) )
      {
        nativeNodeDOFs_[i].insertNodes( xfld_.template getBoundaryTraceGroup<Node>(group) );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Unknown element topology." );
    }
  }


  int DOFidx = 0;

  // Construct the Native DOF indexing. Cells first, then nodes
  for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
    nativeNodeDOFs_[i].createNativeIndex(DOFidx);


  for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
  {
    // Total DOF count in the field on each processor
    nDOF_ += nativeNodeDOFs_[i].nDOF();
  }

#ifdef SANS_MPI
  // every DOF is possessed for one rank
  if (xfld.comm()->size() > 1)
  {
    // Mark all ghost generating element (remainder are zombies)
    for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
    {
      const std::vector<int>& interiorGroups = interiorGroupSets_[i];
      for (std::size_t igroup = 0; igroup < interiorGroups.size(); igroup++)
      {
        const int group = interiorGroups[igroup];

        if ( xfld_.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Node) )
        {
          this->template
          setTraceGroupDOFPossession<Node>( xfld_.template getInteriorTraceGroup<Node>(group),
                                            nativeNodeDOFs_[i] );
        }
        else
          SANS_DEVELOPER_EXCEPTION( "Unknown element topology." );
      }
    }

    for (std::size_t i = 0; i < boundaryGroupSets_.size(); i++)
    {
      const std::vector<int>& boundaryGroups = boundaryGroupSets_[i];
      for (std::size_t igroup = 0; igroup < boundaryGroups.size(); igroup++)
      {
        const int group = boundaryGroups[igroup];

        if ( xfld_.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Node) )
        {
          this->template
          setTraceGroupDOFPossession<Node>( xfld_.template getBoundaryTraceGroup<Node>(group),
                                            nativeNodeDOFs_[i] );
        }
        else
          SANS_DEVELOPER_EXCEPTION( "Unknown element topology." );
      }
    }

    for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
    {
      // count the number of possessed and ghost DOFs
      GhoulDOF nDOFnode = nativeNodeDOFs_[i].nDOFghoul();

      nDOFpossessed_ += nDOFnode.possessed;
      nDOFghost_     += nDOFnode.ghost;
    }
  }
  else // comm size
  {
    nDOFpossessed_ = nDOF_;
    nDOFghost_ = 0;
  }

  // construct the local indexing on each processor
  // possessed - [[Cells],
  //              [Nodes],
  // ghost -      [Cells],
  //              [Nodes],
  // zombies -    [Cells],
  //              [Nodes]]

  DOFidx = 0;

  std::array<Ghoul,3> spirits = {{possessed, ghost, zombie}};

  // don't bother with ghost and zombie for one processor (improves local solves)
  const int nGhoul = xfld.comm()->size() == 1 ? 1 : 3;

  for (int n = 0; n < nGhoul; n++)
  {
    for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
      nativeNodeDOFs_[i].createLocalIndex(spirits[n], DOFidx);
  }

#else
  // in serial all DOFs are possessed
  nDOFpossessed_ = nDOF_;

  DOFidx = 0;

  for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
    nativeNodeDOFs_[i].createLocalIndex(possessed, DOFidx);
#endif

}

//----------------------------------------------------------------------------//
template <class PhysDim>
template <class Topology>
void
Field_CG_TraceConstructor<PhysDim, TopoD1>::
setTraceGroupDOFPossession(const typename XField<PhysDim, TopoD1>::template FieldTraceGroupType<Topology>& xfldTraceGroup,
                          Field_CG_NodeDOF& nativeNodeDOFs)
{
  int nodeMap[Topology::NNode];

  // if any component of a cell element is possessed, then the non-possessed components make ghosts DOFs
  for (int itrace = 0; itrace < xfldTraceGroup.nElem(); itrace++)
  {
    bool isPossessed = false;
    xfldTraceGroup.associativity( itrace ).getNodeGlobalMapping( nodeMap, Topology::NNode );

    {
      // check all nodes
      for (int n = 0; n < Topology::NNode && !isPossessed; n++)
        if ( nativeNodeDOFs.getNativeNode(nodeMap[n]).spirit == possessed ) isPossessed = true;
    }

    if (isPossessed)
    {
      for (int n = 0; n < Topology::NNode; n++)
      {
        Field_CG_NodeDOF::NodeDOFType& node = nativeNodeDOFs.getNativeNode(nodeMap[n]);
        if ( node.spirit == zombie )
          node.spirit = ghost;
      }
    }
  }
}

//----------------------------------------------------------------------------//
template <class PhysDim>
void
Field_CG_TraceConstructor<PhysDim, TopoD1>::
getDOF_rank(int* DOF_rank)
{
  for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
    nativeNodeDOFs_[i].getDOF_rank(nDOFpossessed_, DOF_rank);
}

} // namespace SANS
