// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(FIELD_CG_TRACECONSTRUCTORAREA_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "Field_CG_TraceConstructor.h"
#include "Field_CG_Topology.h"

#include "Topology/ElementTopology.h"

#include "Field/Field.h"
#include "Field/XFieldArea.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

//===========================================================================//
template <class PhysDim>
Field_CG_TraceConstructor<PhysDim, TopoD2>::
Field_CG_TraceConstructor(const XField<PhysDim, TopoD2>& xfld, const int order, const BasisFunctionCategory category,
                          const std::vector<std::vector<int>>& interiorGroupSets,
                          const std::vector<std::vector<int>>& boundaryGroupSets ) :
  xfld_(xfld),
  interiorGroupSets_(interiorGroupSets),
  boundaryGroupSets_(boundaryGroupSets),
  order_(order), category_(category), nElem_(0), nDOF_(0), nDOFpossessed_(0), nDOFghost_(0)
{
  SANS_ASSERT(interiorGroupSets_.size() == boundaryGroupSets_.size());

  for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
  {
    nativeEdgeDOFs_.emplace_back( xfld_, order );
    nativeNodeDOFs_.emplace_back( xfld_ );
  }

  for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
  {
    const std::vector<int>& interiorGroups = interiorGroupSets_[i];
    for (std::size_t igroup = 0; igroup < interiorGroups.size(); igroup++)
    {
      const int group = interiorGroups[igroup];
      nElem_ += xfld_.getInteriorTraceGroupBase(group).nElem();

      if ( xfld_.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Line) )
      {
        nativeEdgeDOFs_[i].insertEdges( xfld_.template getInteriorTraceGroup<Line>(group) );
        nativeNodeDOFs_[i].insertNodes( xfld_.template getInteriorTraceGroup<Line>(group) );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Unknown element topology." );
    }
  }

  for (std::size_t i = 0; i < boundaryGroupSets_.size(); i++)
  {
    const std::vector<int>& boundaryGroups = boundaryGroupSets_[i];
    for (std::size_t igroup = 0; igroup < boundaryGroups.size(); igroup++)
    {
      const int group = boundaryGroups[igroup];
      nElem_ += xfld_.getBoundaryTraceGroupBase(group).nElem();

      if ( xfld_.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Line) )
      {
        nativeEdgeDOFs_[i].insertEdges( xfld_.template getBoundaryTraceGroup<Line>(group) );
        nativeNodeDOFs_[i].insertNodes( xfld_.template getBoundaryTraceGroup<Line>(group) );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Unknown element topology." );
    }
  }


  int DOFidx = 0;

  // Construct the Native DOF indexing. Cells first, then edges, then nodes
  for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
    nativeEdgeDOFs_[i].createNativeIndex(DOFidx);

  for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
    nativeNodeDOFs_[i].createNativeIndex(DOFidx);


  for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
  {
    // Total DOF count in the field on each processor
    nDOF_ += nativeEdgeDOFs_[i].nDOF()
           + nativeNodeDOFs_[i].nDOF();
  }

#ifdef SANS_MPI

  // every DOF is possessed for one rank
  if (xfld.comm()->size() > 1)
  {
    // Mark all ghost generating element (remainder are zombies)
    for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
    {
      const std::vector<int>& interiorGroups = interiorGroupSets_[i];
      for (std::size_t igroup = 0; igroup < interiorGroups.size(); igroup++)
      {
        const int group = interiorGroups[igroup];

        if ( xfld_.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Line) )
        {
          this->template
          setTraceGroupDOFPossession<Line>( xfld_.template getInteriorTraceGroup<Line>(group),
                                            nativeEdgeDOFs_[i],
                                            nativeNodeDOFs_[i] );
        }
        else
          SANS_DEVELOPER_EXCEPTION( "Unknown element topology." );
      }
    }

    for (std::size_t i = 0; i < boundaryGroupSets_.size(); i++)
    {
      const std::vector<int>& boundaryGroups = boundaryGroupSets_[i];
      for (std::size_t igroup = 0; igroup < boundaryGroups.size(); igroup++)
      {
        const int group = boundaryGroups[igroup];

        if ( xfld_.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Line) )
        {
          this->template
          setTraceGroupDOFPossession<Line>( xfld_.template getBoundaryTraceGroup<Line>(group),
                                            nativeEdgeDOFs_[i],
                                            nativeNodeDOFs_[i] );
        }
        else
          SANS_DEVELOPER_EXCEPTION( "Unknown element topology." );
      }
    }

    for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
    {
      // count the number of possessed and ghost DOFs
      GhoulDOF nDOFedge = nativeEdgeDOFs_[i].nDOFghoul();
      GhoulDOF nDOFnode = nativeNodeDOFs_[i].nDOFghoul();

      nDOFpossessed_ += nDOFedge.possessed + nDOFnode.possessed;
      nDOFghost_     += nDOFedge.ghost     + nDOFnode.ghost;
    }
  }
  else // comm size
  {
    nDOFpossessed_ = nDOF_;
    nDOFghost_ = 0;
  }


  // construct the local indexing on each processor
  // possessed - [[Cells],
  //              [Edges],
  //              [Nodes],
  // ghost -      [Cells],
  //              [Edges],
  //              [Nodes],
  // zombies -    [Cells],
  //              [Edges],
  //              [Nodes]]

  DOFidx = 0;

  std::array<Ghoul,3> spirits = {{possessed, ghost, zombie}};

  for (int n = 0; n < 3; n++)
  {
    for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
      nativeEdgeDOFs_[i].createLocalIndex(spirits[n], DOFidx);

    for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
      nativeNodeDOFs_[i].createLocalIndex(spirits[n], DOFidx);
  }

#else
  // in serial all DOFs are possessed
  nDOFpossessed_ = nDOF_;

  DOFidx = 0;

  for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
    nativeEdgeDOFs_[i].createLocalIndex(possessed, DOFidx);

  for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
    nativeNodeDOFs_[i].createLocalIndex(possessed, DOFidx);
#endif

}

//----------------------------------------------------------------------------//
template <class PhysDim>
template <class Topology>
void
Field_CG_TraceConstructor<PhysDim, TopoD2>::
setTraceGroupDOFPossession(const typename XField<PhysDim, TopoD2>::template FieldTraceGroupType<Topology>& xfldTraceGroup,
                          Field_CG_EdgeDOF& nativeEdgeDOFs,
                          Field_CG_NodeDOF& nativeNodeDOFs)
{
  int nodeMap[Topology::NNode];

  const int nDOFperEdge = TopologyDOF_CG<Line>::count(order_);

  // if any component of a cell element is possessed, then the non-possessed components make ghosts DOFs
  for (int itrace = 0; itrace < xfldTraceGroup.nElem(); itrace++)
  {
    bool isPossessed = false;
    xfldTraceGroup.associativity( itrace ).getNodeGlobalMapping( nodeMap, Topology::NNode );

    if (nDOFperEdge > 0)
    {
      // check the edge
      int node0 = nodeMap[0];
      int node1 = nodeMap[1];

      Field_CG_EdgeDOF::EdgeDOFType& edge = nativeEdgeDOFs.getNativeEdge(node0, node1);

      //add the canonical edge DOFs if they are not already in the map
      if ( edge.spirit == possessed && edge.nativeEdgeDOFs.size() > 0 ) isPossessed = true;
    }

    // check all nodes
    for (int n = 0; n < Topology::NNode && !isPossessed; n++)
      if ( nativeNodeDOFs.getNativeNode(nodeMap[n]).spirit == possessed ) isPossessed = true;

    if (isPossessed)
    {
      // convert zombies to ghosts
      if (nDOFperEdge > 0)
      {
        int node0 = nodeMap[0];
        int node1 = nodeMap[1];

        Field_CG_EdgeDOF::EdgeDOFType& edge = nativeEdgeDOFs.getNativeEdge(node0, node1);

        if ( edge.spirit == zombie )
          edge.spirit = ghost;
      }

      for (int n = 0; n < Topology::NNode; n++)
      {
        Field_CG_NodeDOF::NodeDOFType& node = nativeNodeDOFs.getNativeNode(nodeMap[n]);
        if ( node.spirit == zombie )
          node.spirit = ghost;
      }
    }
  }
}

//----------------------------------------------------------------------------//
template <class PhysDim>
void
Field_CG_TraceConstructor<PhysDim, TopoD2>::
getDOF_rank(int* DOF_rank)
{
  for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
    nativeEdgeDOFs_[i].getDOF_rank(nDOFpossessed_, DOF_rank);

  for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
    nativeNodeDOFs_[i].getDOF_rank(nDOFpossessed_, DOF_rank);
}

} // namespace SANS
