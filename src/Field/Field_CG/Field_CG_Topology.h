// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELD_CG_TOPOLOGY_H_
#define FIELD_CG_TOPOLOGY_H_

#include <set>
#include <vector>
#include <map>
#include <memory> // std::shared_ptr


#include "Topology/ElementTopology.h"

#include "BasisFunction/ElementEdges.h"
#include "BasisFunction/ElementFrame.h"
#include "BasisFunction/TraceToCellRefCoord.h"
#include "BasisFunction/BasisFunctionCategory.h"

#include "Field/Element/UniqueElem.h"

#include "Field/FieldTypes.h"
#include "Field/FieldAssociativity.h"

#include "MPI/communicator_fwd.h"

namespace SANS
{

template <class Topology>
struct TopologyDOF_CG;

template <>
struct TopologyDOF_CG<Line>
{
  /** count returns the number of DOFs purely interior to a line */
  static int count(const int order) { return (order-1); }
};

template <>
struct TopologyDOF_CG<Triangle>
{
  /** count returns the number of DOFs purely interior to a triangle */
  static int count(const int order) { return (order-1)*(order-2)/2; }
};

template <>
struct TopologyDOF_CG<Quad>
{
  /** count returns the number of DOFs purely interior to a quadrilateral */
  static int count(const int order) { return (order-1)*(order-1); }
};

template <>
struct TopologyDOF_CG<Tet>
{
  /** count returns the number of DOFs purely interior to a tetrahedron */
  static int count(const int order) { return (order-1)*(order-2)*(order-3)/6; }
};

template <>
struct TopologyDOF_CG<Hex>
{
  /** count returns the number of DOFs purely interior to a hexahedron */
  static int count(const int order) { return (order-1)*(order-1)*(order-1); }
};

template <>
struct TopologyDOF_CG<Pentatope>
{
  /** count returns the number of DOFs purely interior to a pentatope */
  static int count(const int order) { return (order-1)*(order-2)*(order-3)*(order-4)/24; }
};

/** Topology_DOF_CG returns the number of DOFs interior to a given topology & order
 *
 * Topology_DOF_CG returns the number of DOFs that are interior to a given topology at a
 * given order; in other words this returns the count of DOFs that are found on the
 * topologies at this order that _are not_ already counted as being on any lower entity
 */
int Topology_DOF_CG(const TopologyTypes topo, const int order);

// indicates the possessiveness of the CG element ranking
enum Ghoul
{
  possessed,
  ghost,
  zombie
};

/** GhoulDOF is a container for possessivity counts
 *
 */
struct GhoulDOF
{
  GhoulDOF() : possessed(0), ghost(0) {}

  int possessed;
  int ghost;
};

//===========================================================================//

/** Field_CG_NodeDOF is a structure to hold the set of node objects in a CG field.
 *
 * Field_CG_NodeDOF holds node objects in a CG field, including metadata about
 * which outside processors (by rank) need to interact with each node, what the
 * possessivity on this processor rank is for each node, as well as a handful
 * of useful index maps of the associated DOFs and fundamental operations for
 * the nodes.
 *
 * tparam PhysDim   the physical dimension of the incoming grid
 * tparam TopoDim   the topological dimension of the incoming grid
 * param  xfld      an XField grid that you're constructing a CG field out of
 *
 */
struct Field_CG_NodeDOF
{
  /** NodeDOFType is a struct to hold a single Node and its degree of freedom
   */
  struct NodeDOFType
  {
    // local grid node index
    int node;

    // map from a processor rank that exists to the number of times this node
    // appears on that rank
    std::map<int, int> ranks;

    // does this process own the data for this DOF?
    Ghoul spirit;

    // native DOFs associated with this node
    int nativeNodeDOF;
  };

  /** Field_CG_NodeDOF constructs an empty Field_CG_NodeDOF object
   *
   * Field_CG_NodeDOF constructs an empty Field_CG_NodeDOF object, and loads in the
   * local->native DOF map from the XField object it's constructed with.
   *
   * tparam PhysDim   the physical dimension of the incoming grid
   * tparam TopoDim   the topological dimension of the incoming grid
   * param  xfld      an XField grid that you're constructing a CG field out of
   *
   */
  template<class PhysDim, class TopoDim>
  explicit Field_CG_NodeDOF(const XField<PhysDim, TopoDim>& xfld) :
    comm_(xfld.comm()), xfld_local2nativeDOFmap_(xfld.local2nativeDOFmap()), nDOF_(0) {}

  /** insertNodes makes sure all the nodes in a grid cell group are in the nativeNodeMap_
   *
   * insertNodes loops over all of the elements in a grid cell group, then loops over all
   * of the nodes in the elements, and adds each node to the nativeNodeMap_ iff it
   * hasn't already been added!
   *
   * param    xfldGroup   an xFldGroup object holding a cell group
   * tparam   Traits      ???
   */
  template<class Traits>
  void insertNodes(const FieldAssociativity< Traits >& xfldGroup);

  //===========================================================================//
  /** insert_node inserts a node into the local DOF map if it didn't already exist
   *
   * param    node      the integer number of the node of interest
   * param    rank      the integer number of the rank of the processor we're working
   *                        on behalf of
   *
   */
  void insert_node(const int node, const int rank);

  /** createNativeIndex makes the native mappings and possession data for all the nodes
   *
   * createNativeIndex makes the native mappings for the field DOFs, either in
   * a parallel sense or in a serial one, depending on MPI settings, and saves off the
   * parallel possessivity data for each processor
   *
   * param    nodeDOF_index     pointer to the node DOF indices
   */
  void createNativeIndex(int& nodeDOF_index);

  /** createLocalIndex adds a local index for spirits of a given type
   *
   * createLocalIndex runs through the native node map and when the spirit is of the
   * type specified, it adds the DOF to the localNodeMap!
   *
   * param    spirit          spirit (possessitivity) requested
   * param    nodeDOF_index   vector address of the nodeDOFs of interest
   */
  void createLocalIndex(const Ghoul spirit, int& nodeDOF_index);

  //---------------------------------------------------------------------------//
  /** getDOF_rank gets the possessor process rank for each of the degrees of freedom
   *
   * param    nDOFpossessed   the number of DOFs possessed locally
   * param    DOF_rank        the data address at which the DOF-rank associations are stored
   */
  void getDOF_rank(const int nDOFpossessed, int* DOF_rank);

  //---------------------------------------------------------------------------//
  /** getNativeNode returns the native DOF node object for a given local grid node index
   *
   * param    node    local grid node index
   * return           native DOF node object
   */
  NodeDOFType& getNativeNode(const int node);

  /** getLocalNode returns the local DOF node index for a given local grid node index
   *
   *  param   node    local grid node index
   *  return          local DOF node index
   */
  int getLocalNode(const int node) const;

  /** nDOF gets the total number of degrees of freedom on a process
   */
  int nDOF() const { return nDOF_; }
  /** nDOFghoul gets a struct containing the DOF counts of each posessivity type on a process
   */
  GhoulDOF nDOFghoul() const;

protected:

  // MPI communicator
  std::shared_ptr<mpi::communicator> comm_;

  // map from local DOFs to native DOFs __on the grid__
  const int *xfld_local2nativeDOFmap_;

  // map from native grid node index to native DOF objects
  std::map<int,NodeDOFType> nativeNodeMap_;

  // map from native grid node index to local DOF node index
  std::map<int,int> localNodeMap_;

  // number of degrees of freedom
  int nDOF_;

};

/** insertNodes makes sure all the nodes in a grid cell group are in the nativeNodeMap_
 *
 * insertNodes loops over all of the elements in a grid cell group, then loops over all
 * of the nodes in the elements, and adds each DDF node to the nativeNodeMap_ iff it
 * hasn't already been added!
 *
 * param    xfldGroup   an xFldGroup object holding a cell group from a grid
 * tparam   Traits      ???
 */
template<class Traits>
void
Field_CG_NodeDOF::insertNodes(const FieldAssociativity< Traits >& xfldGroup)
{
  // the topo type
  typedef typename Traits::TopologyType Topology;
  // create a map to fill
  int nodeMap[Topology::NNode];

  // loop over each of the elements in the xfldGroup!
  for (int elem = 0; elem < xfldGroup.nElem(); elem++)
  {

    // get the mapping of the local grid nodes to the global DOF nodes
    xfldGroup.associativity( elem ).getNodeGlobalMapping( nodeMap, Topology::NNode );
    // get the rank that this xfldGroup is meant to be associated with
    const int rank = xfldGroup.associativity( elem ).rank();

    // loop over the nodes in the current topology
    for (int n = 0; n < Topology::NNode; n++)
    {
      // add the node DOFs to the Field_CG_NodeDOF maps if they are not already in there
      insert_node(nodeMap[n], rank);
    }
  }
}

//===========================================================================//

/** Field_CG_EdgeDOF is a structure to hold the set of edge objects in a CG field.
 *
 * Field_CG_NodeDOF holds a node object in a CG field, including metadata about
 * which outside processors (by rank) need to interact with it, what the
 * possessivity on this processor rank is, as well as a handful of useful index
 * maps of the associated DOFs and fundamental operations for the edges
 *
 * tparam PhysDim   the physical dimension of the incoming grid
 * tparam TopoDim   the topological dimension of the incoming grid
 * param  xfld      an XField grid that you're constructing a CG field out of
 *
 */
class Field_CG_EdgeDOF
{
public:

  /** EdgeDOFType is a structure to hold an individual edge object and map it to
   * its degrees of freedom
   */
  struct EdgeDOFType
  {
    // native DOF node index of originating node to identify edge orientation
    int nativeNode0;

    // the occurrence of element ranks surrounding/possessing the edge
    std::map<int, int> ranks;

    // the possessiveness of the edge element
    Ghoul spirit;

    // native DOF indices associated with edge
    std::vector<int> nativeEdgeDOFs;
  };

  /** Field_CG_EdgeDOF constructs an empty Field_CG_EdgeDOF object
   *
   * Field_CG_EdgeDOF constructs an empty Field_CG_EdgeDOF object and loads in the
   * local to native DOF map from the XField object with which it is constructed.
   *
   * tparam PhysDim   the physical dimension of the incoming grid
   * tparam TopoDim   the topological dimension of the incoming grid
   * param  xfld      an XField grid that you're constructing a CG field out of
   */
  template<class PhysDim, class TopoDim>
  explicit Field_CG_EdgeDOF(const XField<PhysDim, TopoDim>& xfld, const int order) :
    comm_(xfld.comm()), xfld_local2nativeDOFmap_(xfld.local2nativeDOFmap()), order_(order), nDOF_(0) {}

  /** insertEdges makes sure all the edges in a grid cell group are in the nativeEdgeMap_
   *
   * insertEdges loops over all of the elements in a cell group, then loops over all
   * of the edges in the elements, and adds each node in the grid edge to the
   * nativeEdgeMap_ iff it hasn't already been added!
   *
   * param    xfldGroup   an xFldGroup object holding a cell group
   * tparam   Traits      ???
   */
  template<class Traits>
  void insertEdges(const FieldAssociativity< Traits >& xfldGroup);

  /** insert_edge inserts an edge into the local DOF edge map if it didn't already exist
   *
   * param    node0     the local integer index of the first node of interest
   * param    node1     the local integer index of the second node of interest
   * param    rank      the integer number of the rank of the processor we're working
   *                        on behalf of
   */
  void insert_edge(const int node0, const int node1, const int rank);

  void createNativeIndex(int& edgeDOF_index);
  void createLocalIndex(const Ghoul spirit, int& edgeDOF_index);

  void getDOF_rank(const int nDOFpossessed, int* DOF_rank);

  EdgeDOFType& getNativeEdge(const int node0, const int node1);
  const std::vector<int>& getLocalEdge(const int node0, const int node1) const;

  int nDOF() const { return nDOF_; }
  GhoulDOF nDOFghoul() const;

protected:

  // MPI communicator
  std::shared_ptr<mpi::communicator> comm_;

  // map from local grid DOFs to native grid DOFs
  const int *xfld_local2nativeDOFmap_;

  // map from a given unique grid entity to the native edge field DOF object
  std::map<UniqueElem,EdgeDOFType> nativeEdgeMap_;

  // map from a given unique grid entity to the ordered local DOF indices of this entity
  std::map<UniqueElem,std::vector<int>> localEdgeMap_;

  // polynomial order of representation
  const int order_;

  // number of degrees of freedom in the edges ...?
  int nDOF_;
};

/** insertEdges makes sure all the edges in a grid cell group are in the nativeEdgeMap_
 *
 * insertEdges loops over all of the elements in a cell group, then loops over all
 * of the edges in the elements, and adds each node in the grid edge to the
 * nativeEdgeMap_ iff it hasn't already been added!
 *
 * param    xfldGroup   an xFldGroup object holding a cell group
 * tparam   Traits      ???
 */
template<class Traits>
void
Field_CG_EdgeDOF::insertEdges(const FieldAssociativity< Traits >& xfldGroup)
{
  // the topology
  typedef typename Traits::TopologyType Topology;

  // we don't add any DOFs from the edges if the order isn't high enough
  // if the order isn't high, then there are not any DOFs along the edge that
  // are not also already on one of the lower-dimensional objects!
  if (TopologyDOF_CG<Line>::count(order_) == 0) return;

  // a map from the local grid node indices to their global grid indices on a given element
  int nodeMap[Topology::NNode];

  // the canonical edges of this dimension's element topology
  const int (*EdgeNodes)[ Line::NNode ] = ElementEdges<Topology>::EdgeNodes;

  // loop over all elements in the cell group
  for (int elem = 0; elem < xfldGroup.nElem(); elem++)
  {

    // get the mapping of the local DOF nodes to their global indices
    xfldGroup.associativity( elem ).getNodeGlobalMapping( nodeMap, Topology::NNode );

    // get the processor rank associated to the cell
    const int rank = xfldGroup.associativity( elem ).rank();

    // loop over all edges of the cell
    for (int iedge = 0; iedge < Topology::NEdge; iedge++)
    {
      // get either canonical grid node's global index along an edge
      int nodeX0 = nodeMap[EdgeNodes[iedge][0]];
      int nodeX1 = nodeMap[EdgeNodes[iedge][1]];

      // add the edge if they are not already in the map
      insert_edge(nodeX0, nodeX1, rank);
    }
  }
}

class Field_CG_AreaDOF
{
public:

  struct AreaDOFType
  {
    // Native grid nodes that make up the area
    std::vector<int> nativeNodes;

    // The processor rank of the area
    int rank;

    // The possessiveness of the area element
    Ghoul spirit;

    // local DOFs associated with area
    std::vector<int> nativeAreaDOFs;
  };

  template<class PhysDim, class TopoDim>
  explicit Field_CG_AreaDOF(const XField<PhysDim, TopoDim>& xfld, const int order) :
    comm_(xfld.comm()), xfld_local2nativeDOFmap_(xfld.local2nativeDOFmap()), order_(order), nDOF_(0) {}

  template<class Traits>
  void insertAreas(const FieldAssociativity< Traits >& xfldGroup);

  void insert_area(const TopologyTypes& topo, const std::vector<int>& nodes, const int rank);

  void createNativeIndex(int& areaDOF_index);
  void createLocalIndex(const Ghoul spirit, int& edgeDOF_index);

  void getDOF_rank(const int nDOFpossessed, int* DOF_rank);

  AreaDOFType& getNativeArea(const TopologyTypes& topo, const std::vector<int>& nodes);
  const std::vector<int>& getLocalArea(const TopologyTypes& topo, const std::vector<int>& nodes) const;

  int nDOF() const { return nDOF_; }
  GhoulDOF nDOFghoul() const;

protected:
  std::shared_ptr<mpi::communicator> comm_;
  const int *xfld_local2nativeDOFmap_;
  std::map<UniqueElem,AreaDOFType> nativeAreaMap_;
  std::map<UniqueElem,std::vector<int>> localAreaMap_;
  const int order_;
  int nDOF_;

  template<class TopoDim>
  struct AreaInserter;
};

template<>
struct Field_CG_AreaDOF::AreaInserter<TopoD4>
{
  template<class Traits>
  static void insertAreas(const FieldAssociativity< Traits >& xfldGroup, const int order, Field_CG_AreaDOF& areaDOFs)
  {
    typedef typename Traits::TopologyType Topology;
//    typedef typename Topology::TopoDim TopoDim;
//    typedef typename Topology::TopologyTrace TopologyTrace;
    typedef typename Topology::TopologyFrame TopologyFrame;

    // don't add any elements if the order isn't high enough
    if (TopologyDOF_CG<TopologyFrame>::count(order) == 0) return;

//    SANS_DEVELOPER_EXCEPTION("check implementation");

    int nodeMap[Topology::NNode];
    const int (*AreaNodes)[ TopologyFrame::NNode ] = ElementFrame<Topology>::FrameNodes;
    std::vector<int> nodes(TopologyFrame::NNode);

    for (int elem = 0; elem < xfldGroup.nElem(); elem++)
    {
      xfldGroup.associativity( elem ).getNodeGlobalMapping( nodeMap, Topology::NNode );
      const int rank = xfldGroup.associativity( elem ).rank();

      //loop over all edges of the cell
      for (int iframe = 0; iframe < Topology::NFrame; iframe++)
      {
        for (int n = 0; n < TopologyFrame::NNode; n++ )
          nodes[n] = nodeMap[AreaNodes[iframe][n]];

        //add the canonical face nodes if they are not already in the map
        areaDOFs.insert_area(TopologyFrame::Topology, nodes, rank);
      }
    }
  }
};

template<>
struct Field_CG_AreaDOF::AreaInserter<TopoD3>
{
  template<class Traits>
  static void insertAreas(const FieldAssociativity< Traits >& xfldGroup, const int order, Field_CG_AreaDOF& areaDOFs)
  {
    typedef typename Traits::TopologyType Topology;
    typedef typename Topology::TopoDim TopoDim;
    typedef typename Topology::TopologyTrace TopologyTrace;

    // don't add any elements if the order isn't high enough
    if (TopologyDOF_CG<TopologyTrace>::count(order) == 0) return;

    // don't think i should ever get here?
    SANS_DEVELOPER_EXCEPTION("check implementation");

    int nodeMap[Topology::NNode];
    const int (*AreaNodes)[ TopologyTrace::NNode ] = TraceToCellRefCoord<TopologyTrace, TopoDim, Topology>::TraceNodes;
    std::vector<int> nodes(TopologyTrace::NNode);

    for (int elem = 0; elem < xfldGroup.nElem(); elem++)
    {
      xfldGroup.associativity( elem ).getNodeGlobalMapping( nodeMap, Topology::NNode );
      const int rank = xfldGroup.associativity( elem ).rank();

      //loop over all edges of the cell
      for (int iface = 0; iface < Topology::NFace; iface++)
      {
        for (int n = 0; n < TopologyTrace::NNode; n++ )
          nodes[n] = nodeMap[AreaNodes[iface][n]];

        //add the canonical face nodes if they are not already in the map
        areaDOFs.insert_area(TopologyTrace::Topology, nodes, rank);
      }
    }
  }
};

template<class Traits>
void
Field_CG_AreaDOF::insertAreas(const FieldAssociativity< Traits >& xfldGroup)
{
  typedef typename Traits::TopologyType Topology;
  typedef typename Topology::TopoDim TopoDim;

  AreaInserter<TopoDim>::insertAreas(xfldGroup, order_, *this);
}

//===========================================================================//
class Field_CG_FaceDOF
{
public:

  struct FaceDOFType
  {
    // Native grid nodes that make up the face
    std::vector<int> nativeNodes;

    // The processor rank of the face
    int rank;

    // The possessiveness of the face element
    Ghoul spirit;

    // local DOFs associated with face
    std::vector<int> nativeFaceDOFs;
  };

  template<class PhysDim, class TopoDim>
  explicit Field_CG_FaceDOF(const XField<PhysDim, TopoDim>& xfld, const int order) :
    comm_(xfld.comm()), xfld_local2nativeDOFmap_(xfld.local2nativeDOFmap()), order_(order), nDOF_(0) {}

  template<class Traits>
  void insertFaces(const FieldAssociativity< Traits >& xfldGroup);

  void insert_face(const TopologyTypes& topo, const std::vector<int>& nodes, const int rank);

  void createNativeIndex(int& faceDOF_index);
  void createLocalIndex(const Ghoul spirit, int& edgeDOF_index);

  void getDOF_rank(const int nDOFpossessed, int* DOF_rank);

  FaceDOFType& getNativeFace(const TopologyTypes& topo, const std::vector<int>& nodes);
  const std::vector<int>& getLocalFace(const TopologyTypes& topo, const std::vector<int>& nodes) const;

  int nDOF() const { return nDOF_; }
  GhoulDOF nDOFghoul() const;

protected:
  std::shared_ptr<mpi::communicator> comm_;
  const int *xfld_local2nativeDOFmap_;
  std::map<UniqueElem,FaceDOFType> nativeFaceMap_;
  std::map<UniqueElem,std::vector<int>> localFaceMap_;
  const int order_;
  int nDOF_;

  template<class TopoDim>
  struct FaceInserter;
};

template<>
struct Field_CG_FaceDOF::FaceInserter<TopoD2>
{
  template<class Traits>
  static void insertFaces(const FieldAssociativity< Traits >& xfldGroup, const int order, Field_CG_FaceDOF& faceDOFs)
  {
    typedef typename Traits::TopologyType Topology;

    // don't add any elements if the order isn't high enough
    if (TopologyDOF_CG<Topology>::count(order) == 0) return;

    std::vector<int> nodes(Topology::NNode);

    for (int elem = 0; elem < xfldGroup.nElem(); elem++)
    {
      xfldGroup.associativity( elem ).getNodeGlobalMapping( nodes.data(), nodes.size() );
      const int rank = xfldGroup.associativity( elem ).rank();

      //add the canonical face nodes if they are not already in the map
      faceDOFs.insert_face(Topology::Topology, nodes, rank);
    }
  }
};

template<>
struct Field_CG_FaceDOF::FaceInserter<TopoD3>
{
  template<class Traits>
  static void insertFaces(const FieldAssociativity< Traits >& xfldGroup, const int order, Field_CG_FaceDOF& faceDOFs)
  {
    typedef typename Traits::TopologyType Topology;
    typedef typename Topology::TopoDim TopoDim;
    typedef typename Topology::TopologyTrace TopologyTrace;

    // don't add any elements if the order isn't high enough
    if (TopologyDOF_CG<TopologyTrace>::count(order) == 0) return;

    int nodeMap[Topology::NNode];
    const int (*FaceNodes)[ TopologyTrace::NNode ] = TraceToCellRefCoord<TopologyTrace, TopoDim, Topology>::TraceNodes;
    std::vector<int> nodes(TopologyTrace::NNode);

    for (int elem = 0; elem < xfldGroup.nElem(); elem++)
    {
      xfldGroup.associativity( elem ).getNodeGlobalMapping( nodeMap, Topology::NNode );
      const int rank = xfldGroup.associativity( elem ).rank();

      //loop over all edges of the cell
      for (int iface = 0; iface < Topology::NFace; iface++)
      {
        for (int n = 0; n < TopologyTrace::NNode; n++ )
          nodes[n] = nodeMap[FaceNodes[iface][n]];

        //add the canonical face nodes if they are not already in the map
        faceDOFs.insert_face(TopologyTrace::Topology, nodes, rank);
      }
    }
  }
};

template<>
struct Field_CG_FaceDOF::FaceInserter<TopoD4>
{
  template<class Traits>
  static void insertFaces(const FieldAssociativity< Traits >& xfldGroup, const int order, Field_CG_FaceDOF& faceDOFs)
  {
    typedef typename Traits::TopologyType Topology;
    typedef typename Topology::TopoDim TopoDim;
    typedef typename Topology::TopologyTrace TopologyTrace;

    // don't add any elements if the order isn't high enough
    if (TopologyDOF_CG<TopologyTrace>::count(order) == 0) return;

    SANS_DEVELOPER_EXCEPTION("check implementation");

    int nodeMap[Topology::NNode];
    const int (*FaceNodes)[ TopologyTrace::NNode ] = TraceToCellRefCoord<TopologyTrace, TopoDim, Topology>::TraceNodes;
    std::vector<int> nodes(TopologyTrace::NNode);

    for (int elem = 0; elem < xfldGroup.nElem(); elem++)
    {
      xfldGroup.associativity( elem ).getNodeGlobalMapping( nodeMap, Topology::NNode );
      const int rank = xfldGroup.associativity( elem ).rank();

      //loop over all edges of the cell
      for (int iface = 0; iface < Topology::NFace; iface++)
      {
        for (int n = 0; n < TopologyTrace::NNode; n++ )
          nodes[n] = nodeMap[FaceNodes[iface][n]];

        //add the canonical face nodes if they are not already in the map
        faceDOFs.insert_face(TopologyTrace::Topology, nodes, rank);
      }
    }
  }
};


template<class Traits>
void
Field_CG_FaceDOF::insertFaces(const FieldAssociativity< Traits >& xfldGroup)
{
  typedef typename Traits::TopologyType Topology;
  typedef typename Topology::TopoDim TopoDim;

  FaceInserter<TopoDim>::insertFaces(xfldGroup, order_, *this);
}


//===========================================================================//
class Field_CG_CellDOF
{
public:

  struct CellDOFType
  {
    // Topology of the cell
    TopologyTypes topo;

    // The processor rank of the cell
    int rank;

    // The possessiveness of the cell element
    Ghoul spirit;

    // native DOF indexes associated with face
    std::vector<int> nativeCellDOFs;
  };

  template<class PhysDim, class TopoDim>
  explicit Field_CG_CellDOF(const XField<PhysDim, TopoDim>& xfld, const int order) :
    comm_(xfld.comm()), cellIDs_(xfld.cellIDs()), order_(order), nDOF_(0) {}

  template<class Traits>
  void insertCells(const FieldAssociativity< Traits >& xfldGroup, const int group);

  void insert_cell(const TopologyTypes& topo, const int cellID, const int rank);

  void createNativeIndex(int& cellDOF_index);
  void createLocalIndex(const Ghoul spirit, int& edgeDOF_index);

  void getDOF_rank(const int nDOFpossessed, int* DOF_rank);

  CellDOFType& getNativeCell(const int elem, const int group);
  const std::vector<int>& getLocalCell(const int elem, const int group) const;

  int nDOF() const { return nDOF_; }
  GhoulDOF nDOFghoul() const;

protected:
  std::shared_ptr<mpi::communicator> comm_;
  const std::vector<std::vector<int>>& cellIDs_;
  std::map<int,CellDOFType> nativeCellMap_;
  std::map<int,std::vector<int>> localCellMap_;
  const int order_;
  int nDOF_;
};

template<class Traits>
void
Field_CG_CellDOF::insertCells(const FieldAssociativity< Traits >& xfldGroup, const int group)
{
  typedef typename Traits::TopologyType Topology;

  // don't add any elements if the order isn't high enough
  if (TopologyDOF_CG<Topology>::count(order_) == 0) return;

  const std::vector<int>& cellID = cellIDs_[group];

  for (int elem = 0; elem < xfldGroup.nElem(); elem++)
  {
    const int rank = xfldGroup.associativity( elem ).rank();
    insert_cell(Topology::Topology, cellID[elem], rank);
  }
}

}

#endif /* FIELD_CG_TOPOLOGY_H_ */
