// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(FIELD_CG_CELLCONSTRUCTORAREA_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "Field_CG_CellConstructor.h"
#include "Field_CG_Topology.h"

#include "BasisFunction/ElementFrame.h"

#include "Topology/ElementTopology.h"

#include "Field/Field.h"
#include "Field/XFieldArea.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

//============================================================================//
template <class PhysDim>
Field_CG_CellConstructorBase<PhysDim, TopoD2>::
Field_CG_CellConstructorBase(const XField<PhysDim, TopoD2>& xfld, const int order, const BasisFunctionCategory category,
                             const std::vector<std::vector<int>>& CellGroupSets,
                             const bool needsEmbeddedGhosts ) :
  xfld_(xfld),
  CellGroupSets_(CellGroupSets),
  needsEmbeddedGhosts_(needsEmbeddedGhosts),
  order_(order), category_(category), nElem_(0), nDOF_(0), nDOFpossessed_(0), nDOFghost_(0)
{

  for (std::size_t i = 0; i < CellGroupSets.size(); i++)
  {
    nativeCellDOFs_.emplace_back( xfld_, order );
    nativeEdgeDOFs_.emplace_back( xfld_, order );
    nativeNodeDOFs_.emplace_back( xfld_ );
  }

  int DOFidx = 0;

  // Loop over sets - Nodes on interfaces get counted twice
  for (std::size_t i = 0; i < CellGroupSets.size(); i++)
  {
    const std::vector<int>& CellGroups = CellGroupSets[i];
    for (std::size_t igroup = 0; igroup < CellGroups.size(); igroup++)
    {
      const int group = CellGroups[igroup];
      nElem_ += xfld_.getCellGroupBase(group).nElem();

      if ( xfld_.getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
      {
        nativeCellDOFs_[i].insertCells( xfld_.template getCellGroup<Triangle>(group), group );
        nativeEdgeDOFs_[i].insertEdges( xfld_.template getCellGroup<Triangle>(group) );
        nativeNodeDOFs_[i].insertNodes( xfld_.template getCellGroup<Triangle>(group) );
      }
      else if ( xfld_.getCellGroupBase(group).topoTypeID() == typeid(Quad) )
      {
        nativeCellDOFs_[i].insertCells( xfld_.template getCellGroup<Quad>(group), group );
        nativeEdgeDOFs_[i].insertEdges( xfld_.template getCellGroup<Quad>(group) );
        nativeNodeDOFs_[i].insertNodes( xfld_.template getCellGroup<Quad>(group) );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Unknown element topology." );
    }
  }

  // Construct the Native DOF indexing. Cells first, then edges, then nodes
  for (std::size_t i = 0; i < CellGroupSets.size(); i++)
    nativeCellDOFs_[i].createNativeIndex(DOFidx);

  for (std::size_t i = 0; i < CellGroupSets.size(); i++)
    nativeEdgeDOFs_[i].createNativeIndex(DOFidx);

  for (std::size_t i = 0; i < CellGroupSets.size(); i++)
    nativeNodeDOFs_[i].createNativeIndex(DOFidx);


  for (std::size_t i = 0; i < CellGroupSets.size(); i++)
  {
    // Total DOF count in the field on each processor
    nDOF_ += nativeCellDOFs_[i].nDOF()
           + nativeEdgeDOFs_[i].nDOF()
           + nativeNodeDOFs_[i].nDOF();
  }

#ifdef SANS_MPI

  // every DOF is possessed for one rank
  if (xfld.comm()->size() > 1)
  {
    // Mark all ghost generating element (remainder are zombies)
    for (std::size_t i = 0; i < CellGroupSets.size(); i++)
    {
      const std::vector<int>& CellGroups = CellGroupSets[i];
      for (std::size_t igroup = 0; igroup < CellGroups.size(); igroup++)
      {
        const int group = CellGroups[igroup];

        if ( xfld_.getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
        {
          this->template
          setCellGroupDOFPossession( xfld_.template getCellGroup<Triangle>(group), group,
                                     nativeCellDOFs_[i],
                                     nativeEdgeDOFs_[i],
                                     nativeNodeDOFs_[i] );
        }
        else if ( xfld_.getCellGroupBase(group).topoTypeID() == typeid(Quad) )
        {
          this->template
          setCellGroupDOFPossession( xfld_.template getCellGroup<Quad>(group), group,
                                     nativeCellDOFs_[i],
                                     nativeEdgeDOFs_[i],
                                     nativeNodeDOFs_[i] );
        }
        else
          SANS_DEVELOPER_EXCEPTION( "Unknown element topology." );
      }

      // count the number of possessed and ghost DOFs
      GhoulDOF nDOFcell = nativeCellDOFs_[i].nDOFghoul();
      GhoulDOF nDOFedge = nativeEdgeDOFs_[i].nDOFghoul();
      GhoulDOF nDOFnode = nativeNodeDOFs_[i].nDOFghoul();

      nDOFpossessed_ += nDOFcell.possessed + nDOFedge.possessed + nDOFnode.possessed;
      nDOFghost_     += nDOFcell.ghost     + nDOFedge.ghost     + nDOFnode.ghost;
    }
  }
  else // comm size
  {
    nDOFpossessed_ = nDOF_;
    nDOFghost_ = 0;
  }


  // construct the local indexing on each processor
  // possessed - [[Cells],
  //              [Edges],
  //              [Nodes],
  // ghost ------ [Cells],
  //              [Edges],
  //              [Nodes],
  // zombies ---- [Cells],
  //              [Edges],
  //              [Nodes]]

  DOFidx = 0;

  std::array<Ghoul,3> spirits = {{possessed, ghost, zombie}};

  // don't bother with ghost and zombie for one processor (improves local solves)
  const int nGhoul = xfld.comm()->size() == 1 ? 1 : 3;

  for (int n = 0; n < nGhoul; n++)
  {
    for (std::size_t i = 0; i < CellGroupSets.size(); i++)
      nativeCellDOFs_[i].createLocalIndex(spirits[n], DOFidx);

    for (std::size_t i = 0; i < CellGroupSets.size(); i++)
      nativeEdgeDOFs_[i].createLocalIndex(spirits[n], DOFidx);

    for (std::size_t i = 0; i < CellGroupSets.size(); i++)
      nativeNodeDOFs_[i].createLocalIndex(spirits[n], DOFidx);
  }

#else
  // in serial all DOFs are possessed
  // possessed - [[Cells],
  //              [Edges],
  //              [Nodes]]

  nDOFpossessed_ = nDOF_;

  DOFidx = 0;

  for (std::size_t i = 0; i < CellGroupSets.size(); i++)
    nativeCellDOFs_[i].createLocalIndex(possessed, DOFidx);

  for (std::size_t i = 0; i < CellGroupSets.size(); i++)
    nativeEdgeDOFs_[i].createLocalIndex(possessed, DOFidx);

  for (std::size_t i = 0; i < CellGroupSets.size(); i++)
    nativeNodeDOFs_[i].createLocalIndex(possessed, DOFidx);
#endif

}


//----------------------------------------------------------------------------//
template <class PhysDim>
template <class Topology>
void
Field_CG_CellConstructorBase<PhysDim, TopoD2>::
setCellGroupDOFPossession(const typename XField<PhysDim, TopoD2>::template FieldCellGroupType<Topology>& xfldCellGroup,
                          const int cellGroup,
                          Field_CG_CellDOF& nativeCellDOFs,
                          Field_CG_EdgeDOF& nativeEdgeDOFs,
                          Field_CG_NodeDOF& nativeNodeDOFs)
{
  int nodeMap[Topology::NNode];
  const int (*EdgeNodes)[ Line::NNode ] = ElementEdges<Topology>::EdgeNodes;

  const int nDOFperCell = TopologyDOF_CG<Topology>::count(order_);
  const int nDOFperEdge = TopologyDOF_CG<Line>::count(order_);

  // if any component of a cell element is possessed, then the non-possessed components make ghosts DOFs
  for (int icell = 0; icell < xfldCellGroup.nElem(); icell++)
  {
    bool isPossessed = false;

    xfldCellGroup.associativity( icell ).getNodeGlobalMapping( nodeMap, Topology::NNode );

    // check the cell
    if (nDOFperCell > 0)
    {
      Field_CG_CellDOF::CellDOFType& cell = nativeCellDOFs.getNativeCell(icell, cellGroup);
      if ( cell.spirit == possessed && cell.nativeCellDOFs.size() > 0 ) isPossessed = true;
    }

    //extra ghost information needed if embedded field
    if ( needsEmbeddedGhosts_ && xfldCellGroup.associativity( icell ).rank() == xfld_.comm()->rank() ) isPossessed = true;

    // check all edges
    if (nDOFperEdge > 0)
      for (int iedge = 0; iedge < Topology::NEdge && !isPossessed; iedge++)
      {
        int node0 = nodeMap[EdgeNodes[iedge][0]];
        int node1 = nodeMap[EdgeNodes[iedge][1]];

        Field_CG_EdgeDOF::EdgeDOFType& edge = nativeEdgeDOFs.getNativeEdge(node0, node1);

        //add the canonical edge DOFs if they are not already in the map
        if ( edge.spirit == possessed && edge.nativeEdgeDOFs.size() > 0 ) isPossessed = true;
      }

    // check all nodes
    for (int n = 0; n < Topology::NNode && !isPossessed; n++)
      if ( nativeNodeDOFs.getNativeNode(nodeMap[n]).spirit == possessed ) isPossessed = true;

    if (isPossessed)
    {
      // convert zombies to ghosts
      if (nDOFperCell > 0)
      {
        Field_CG_CellDOF::CellDOFType& cell = nativeCellDOFs.getNativeCell(icell, cellGroup);
        if ( cell.spirit == zombie )
          cell.spirit = ghost;
      }

      if (nDOFperEdge > 0)
        for (int iedge = 0; iedge < Topology::NEdge; iedge++)
        {
          int node0 = nodeMap[EdgeNodes[iedge][0]];
          int node1 = nodeMap[EdgeNodes[iedge][1]];

          Field_CG_EdgeDOF::EdgeDOFType& edge = nativeEdgeDOFs.getNativeEdge(node0, node1);

          //add the canonical edge DOFs if they are not already in the map
          if ( edge.spirit == zombie )
            edge.spirit = ghost;
        }

      for (int n = 0; n < Topology::NNode; n++)
      {
        Field_CG_NodeDOF::NodeDOFType& node = nativeNodeDOFs.getNativeNode(nodeMap[n]);
        if ( node.spirit == zombie )
          node.spirit = ghost;
      }
    }
  }
}

//----------------------------------------------------------------------------//
template <class PhysDim>
void
Field_CG_CellConstructorBase<PhysDim, TopoD2>::
getDOF_rank(int* DOF_rank)
{
  for (std::size_t i = 0; i < CellGroupSets_.size(); i++)
    nativeCellDOFs_[i].getDOF_rank(nDOFpossessed_, DOF_rank);

  for (std::size_t i = 0; i < CellGroupSets_.size(); i++)
    nativeEdgeDOFs_[i].getDOF_rank(nDOFpossessed_, DOF_rank);

  for (std::size_t i = 0; i < CellGroupSets_.size(); i++)
    nativeNodeDOFs_[i].getDOF_rank(nDOFpossessed_, DOF_rank);
}

} // namespace SANS
