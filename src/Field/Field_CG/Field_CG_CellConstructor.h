// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELD_CG_CELLCONSTRUCTOR_H
#define FIELD_CG_CELLCONSTRUCTOR_H

#include <vector>
#include <memory> // std::unique_ptr

#include "Topology/ElementTopology.h"

#include "BasisFunction/LagrangeDOFMap.h"

#include "Field_CG_Topology.h"

#include "Field/Field.h"

#include "Field/ElementConnectivityConstructor.h"
#include "Field/Element/ElementAssociativityAreaConstructor.h"
#include "Field/Element/ElementAssociativityVolumeConstructor.h"
#include "Field/Element/ElementAssociativitySpacetimeConstructor.h"


namespace SANS
{
//============================================================================//
template <class TopologyTrace>
void orientFaceDOFs( const int order, const BasisFunctionCategory category, const int orientation,
                    std::vector<int>& faceDOF, std::vector<int>& nativeFaceDOFs );

template <class TopologyTrace>
void orientVolumeDOFs( const int order, const BasisFunctionCategory category, const int orientation,
                    std::vector<int>& faceDOF, std::vector<int>& nativeFaceDOFs );


//============================================================================//
template <class PhysDim, class TopoDim>
struct Field_CG_CellConstructorBase;

//============================================================================//
// Class for constructing indexing for a continuous field
//============================================================================//

/** Field_CG_CellConstructorBase is the base class for a cell constructor object!
 *
 * Field_CG_CellConstructorBase is the fundamental constructor class for the Field_CG_Cell.
 * It carries the fundamentals of the crucial information for the generation of a CG field;
 * the key thing here is that it carries the important underlying stuff that is distributed
 * far beyond the region of a given cell or processor
 */
template <class PhysDim>
struct Field_CG_CellConstructorBase<PhysDim, TopoD1>
{
  // constructor
  Field_CG_CellConstructorBase(const XField<PhysDim, TopoD1>& xfld, const int order, const BasisFunctionCategory category,
                               const std::vector<std::vector<int>>& CellGroupSets, const bool needsEmbeddedGhosts );

  // public functions to return protected variables
  int nElem()         const { return nElem_;         }      // return number of elements
  int nDOF()          const { return nDOF_;          }      // return number of degrees of freedom
  int nDOFpossessed() const { return nDOFpossessed_; }      // return number of DOFs possessed
  int nDOFghost()     const { return nDOFghost_;     }      // return number of ghosted DOFs

  // function to create cell group
  template<class FieldCellGroupClass>
  FieldCellGroupClass*
  createCellGroup( const int cellGroup, int* local2nativeDOFmap );

  // ???
  void getDOF_rank(int* DOF_rank);

protected:

  // the underlying field mesh that is used to represent the solution
  const XField<PhysDim, TopoD1>& xfld_;

  // a map of all the cell DOFs in a native indexing
  std::vector<Field_CG_CellDOF> nativeCellDOFs_;

  // used to count unique node DOFs in a native indexing
  std::vector<Field_CG_NodeDOF> nativeNodeDOFs_;

  // a vector of vectors, inner vectors are the cellgroups, outer vectors are the cellgroup sets
  const std::vector<std::vector<int>> CellGroupSets_;

  const bool needsEmbeddedGhosts_;

  // the basis function order and category that the solution is represented with!
  const int order_;
  const BasisFunctionCategory category_;

  // total number of cell elements
  int nElem_;

  // total DOF counts
  int nDOF_;                  // total
  int nDOFpossessed_;         // DOFs that are possessed locally
  int nDOFghost_;             // DOFs that are possessed elsewhere!

  // ???
  template <class Topology>
  void
  setCellGroupDOFPossession(const typename XField<PhysDim, TopoD1>::template FieldCellGroupType<Topology>& xfldCellGroup,
                            const int cellGroup,
                            Field_CG_CellDOF& nativeCellDOFs,
                            Field_CG_NodeDOF& nativeNodeDOFs);

  // ???
  template<class FieldCellGroupClass, class Topology>
  FieldCellGroupClass*
  createCellGroup( const int cellGroup,
                   const typename XField<PhysDim, TopoD1>::template FieldCellGroupType<Topology>& xfldCellGroup,
                   Field_CG_CellDOF& nativeCellDOFs,
                   Field_CG_NodeDOF& nativeNodeDOFs,
                   int* local2nativeDOFmap );

  // ???
  template <class FieldTraceConstructorType>
  std::unique_ptr<FieldTraceConstructorType>
  createInteriorTraceGroup( const int iTraceGroup );

  // ???
  template <class FieldTraceConstructorType>
  std::unique_ptr<FieldTraceConstructorType>
  createBoundaryTraceGroup( const int bTraceGroup );

  // ???
  template <class FieldTraceConstructorType>
  std::unique_ptr<FieldTraceConstructorType>
  createGhostBoundaryTraceGroup( const int bTraceGroup );

  // ???
  template <class FieldTraceConstructorType, class Topology>
  std::unique_ptr<FieldTraceConstructorType>
  createTraceGroup( const typename XField<PhysDim, TopoD1>::template FieldTraceGroupType<Topology>& xfldTraceGroup,
                    Field_CG_NodeDOF& nativeNodeDOFs );

};


//---------------------------------------------------------------------------//
/** createCellGroup creates a cell group using the DOF map input
 */
template <class PhysDim>
template <class FieldCellGroupClass>
FieldCellGroupClass*
Field_CG_CellConstructorBase<PhysDim, TopoD1>::
createCellGroup( const int cellGroup, int* local2nativeDOFmap )
{
  // typedef the details away
  typedef typename FieldCellGroupClass::TopologyType Topology;

  // loop over the cellgroupsets
  for (std::size_t i = 0; i < CellGroupSets_.size(); i++)
  {
    // the current cellgroupset
    const std::vector<int>& CellGroups = CellGroupSets_[i];

    // if the cellgroup of interest is in this cellgroupset ...
    if (std::find(CellGroups.begin(), CellGroups.end(), cellGroup) != CellGroups.end())
      // then create the cellgroup and return its result (a pointer to the created cellgroup)
      return this->template createCellGroup<FieldCellGroupClass, Topology>(
          cellGroup,
          xfld_.template getCellGroup<Topology>(cellGroup),
          nativeCellDOFs_[i],
          nativeNodeDOFs_[i],
          local2nativeDOFmap );
  }

  SANS_DEVELOPER_EXCEPTION("Count not find cell group!!");
  return nullptr;
}

//---------------------------------------------------------------------------//
/** createCellGroup creates a cell group and populates its subordinates!
 *
 * createCellGroup will create a cell group, make sure that the metadata is copied
 * in, then builds up all the DOFs that are interior to the objects, and makes sure
 * all of the associativities and orderings are correctly implemented!
 */
template <class PhysDim>
template <class FieldCellGroupClass, class Topology>
FieldCellGroupClass*
Field_CG_CellConstructorBase<PhysDim, TopoD1>::
createCellGroup( const int cellGroup,
                 const typename XField<PhysDim, TopoD1>::template FieldCellGroupType<Topology>& xfldCellGroup,
                 Field_CG_CellDOF& nativeCellDOFs,
                 Field_CG_NodeDOF& nativeNodeDOFs,
                 int* local2nativeDOFmap )
{
  // typedef away base class
  typedef typename FieldCellGroupClass::BasisType BasisType;

  // basis function order and category used to represent solution
  const int order = order_;
  const BasisFunctionCategory category = category_;

  // number of interior DOFs per entity
  const int nDOFperCell = TopologyDOF_CG<Topology>::count(order);
  const int nDOFperEdge = TopologyDOF_CG<Line>::count(order);

  // grab the specified basis function
  const BasisType* basis = BasisType::getBasisFunction(order, category);

  // create field associativity constructor -> this will eventually be what ties all the entities together
  typename FieldCellGroupClass::FieldAssociativityConstructorType fldAssocCell( basis, xfldCellGroup.nElem() );

  // create nodal DOF ordering map
  int nodeMap[Topology::NNode];
  // create cell DOF ordering map!
  std::vector<int> edgeMap(Topology::NEdge*nDOFperEdge);

  for (int icell = 0; icell < xfldCellGroup.nElem(); icell++)
  {
    // set the processor rank
    fldAssocCell.setAssociativity(icell).setRank( xfldCellGroup.associativity( icell ).rank() );

    // retrieve interior-to-cell DOFs and set them
    if (nDOFperCell > 0)
    {
      // grab the native cell DOF map
      Field_CG_CellDOF::CellDOFType& cell= nativeCellDOFs.getNativeCell(icell, cellGroup);
      // grab the local cell DOF index vectors
      const std::vector<int>& cellDOF= nativeCellDOFs.getLocalCell(icell, cellGroup);

      // for each entry in cell DOF
      for (std::size_t k = 0; k < cellDOF.size(); k++)
        // add to the local2native map the nativeCellDOFs indexed on the local DOF no.
        local2nativeDOFmap[cellDOF[k]]= cell.nativeCellDOFs[k];

      // set the associativity list of cell DOFs!
      fldAssocCell.setAssociativity(icell).setCellGlobalMapping( cellDOF );
    }

    // get the node mapping of the grid
    xfldCellGroup.associativity( icell ).getNodeGlobalMapping( nodeMap, Topology::NNode );

    // update the node numbering and set node DOFs

    // loop over the nodes on a given cell
    for (int k = 0; k < Topology::NNode; k++)
    {
      // grab the native node DOF map for this node
      Field_CG_NodeDOF::NodeDOFType& node= nativeNodeDOFs.getNativeNode(nodeMap[k]);
      // grab the local node DOF index for this node
      const int nodeDOF = nativeNodeDOFs.getLocalNode(nodeMap[k]);

      // set in this node into the node map
      nodeMap[k] = nodeDOF;
      // add to the local2native map the nativeNodeDOFs indexed on the local DOF no.
      local2nativeDOFmap[nodeDOF] = node.nativeNodeDOF;
    }

    // set the associativity list of node DOFs!
    fldAssocCell.setAssociativity(icell).setNodeGlobalMapping( nodeMap, Topology::NNode );

    // copy over the trace orientations from the cell
    fldAssocCell.setAssociativity(icell).traceOrientation() = xfldCellGroup.associativity( icell ).traceOrientation();
  }

  // create and return pointer to a new FieldCellGroup associativity object
  // using new keyword to protect it from function-local scoping!
  return new FieldCellGroupClass(fldAssocCell);
}

//---------------------------------------------------------------------------//
/** createInteriorTraceGroup creates an interior trace group
 *
 * createInteriorTraceGroup creates an interior trace group for a constructor by searching
 * for the appropriate cell group and creating and returning the associated interior trace
 * group using data from the grid's equivalent interior trace groups
 */
template <class PhysDim>
template <class FieldTraceConstructorType>
std::unique_ptr<FieldTraceConstructorType>
Field_CG_CellConstructorBase<PhysDim, TopoD1>::
createInteriorTraceGroup( const int iTraceGroup )
{
  typedef typename FieldTraceConstructorType::TopologyType Topology;

  // get the left cell group for this trace
  int cellGroupL = xfld_.getInteriorTraceGroupBase(iTraceGroup).getGroupLeft();

  // loop over the cell group sets stored by this constructor
  for (std::size_t i = 0; i < CellGroupSets_.size(); i++)
  {
    // grab the cellgroups in this cellgroup set
    const std::vector<int>& CellGroups = CellGroupSets_[i];

    // check if the current set of cell groups is to the left if the iTrace group
    if (std::find(CellGroups.begin(), CellGroups.end(), cellGroupL) != CellGroups.end())
      // if so, create the correct tracegroup based on the grid's interior trace groups
      return this->template createTraceGroup<FieldTraceConstructorType, Topology>(
          xfld_.template getInteriorTraceGroup<Topology>(iTraceGroup),
          nativeNodeDOFs_[i] );
  }

  // if we get here, we've had an issue
  SANS_DEVELOPER_EXCEPTION("Count not find left cell group!!");
  return nullptr;
}

//---------------------------------------------------------------------------//
/** createBoundaryTraceGroup creates an boundary trace group
 *
 * createBoundaryTraceGroup creates an boundary trace group for a constructor by searching
 * for the appropriate cell group and creating and returning the associated boundary trace
 * group using data from the grid's equivalent boundary trace groups
 */
template <class PhysDim>
template <class FieldTraceConstructorType>
std::unique_ptr<FieldTraceConstructorType>
Field_CG_CellConstructorBase<PhysDim, TopoD1>::
createBoundaryTraceGroup( const int bTraceGroup )
{
  typedef typename FieldTraceConstructorType::TopologyType Topology;

  // get the left cell group for this trace
  int cellGroupL = xfld_.getBoundaryTraceGroupBase(bTraceGroup).getGroupLeft();

  // loop over all the cell group sets stored by this container
  for (std::size_t i = 0; i < CellGroupSets_.size(); i++)
  {
    // grab the cellgroups in this cellgroupset
    const std::vector<int>& CellGroups = CellGroupSets_[i];

    // check if the current set of cell groups is to the left of the bTrace group
    if (std::find(CellGroups.begin(), CellGroups.end(), cellGroupL) != CellGroups.end())
      // if so, create the correct tracegroup based on the grid's boundary trace group
      return this->template createTraceGroup<FieldTraceConstructorType, Topology>(
          xfld_.template getBoundaryTraceGroup<Topology>(bTraceGroup),
          nativeNodeDOFs_[i] );
  }

  // if we get here, we've had an issue
  SANS_DEVELOPER_EXCEPTION("Count not find left cell group!!");
  return nullptr;
}

//---------------------------------------------------------------------------//
/** createGhostBoundaryTraceGroup creates a ghost boundary trace group
 *
 * createGhostBoundaryTraceGroup creates a ghost boundary trace group for a constructor
 * by searching for the appropriate cell group and creating and returning the associated
 * ghost boundary trace group using data from the grid's equivalent ghost boundary trace
 * groups
 */
template <class PhysDim>
template <class FieldTraceConstructorType>
std::unique_ptr<FieldTraceConstructorType>
Field_CG_CellConstructorBase<PhysDim, TopoD1>::
createGhostBoundaryTraceGroup( const int bTraceGroup )
{
  typedef typename FieldTraceConstructorType::TopologyType Topology;

  // get the left cell gorup for this trace
  int cellGroupL = xfld_.getGhostBoundaryTraceGroupBase(bTraceGroup).getGroupLeft();

  // loop over all the cell group sets stored by this container
  for (std::size_t i = 0; i < CellGroupSets_.size(); i++)
  {
    // grab the cellgroups in this cellgroupset
    const std::vector<int>& CellGroups = CellGroupSets_[i];

    // check if the current set of cell groups is to the left of the bTrace group
    if (std::find(CellGroups.begin(), CellGroups.end(), cellGroupL) != CellGroups.end())
      // if so, create the correct tracegroup based on the grid's ghost boundary trace group
      return this->template createTraceGroup<FieldTraceConstructorType, Topology>(
          xfld_.template getGhostBoundaryTraceGroup<Topology>(bTraceGroup),
          nativeNodeDOFs_[i] );
  }

  // if we get here, we've had an issue
  SANS_DEVELOPER_EXCEPTION("Count not find left cell group!!");
  return nullptr;
}

//---------------------------------------------------------------------------//
/** createTraceGroup creates a trace group!
 *
 * createTraceGroup creates a trace group and sets it up using the grid XField
 * associativities that are loaded in.
 */
template <class PhysDim>
template <class FieldTraceConstructorType, class Topology>
std::unique_ptr<FieldTraceConstructorType>
Field_CG_CellConstructorBase<PhysDim, TopoD1>::
createTraceGroup( const typename XField<PhysDim, TopoD1>::template FieldTraceGroupType<Topology>& xfldTraceGroup,
                  Field_CG_NodeDOF& nativeNodeDOFs )
{
  typedef typename FieldTraceConstructorType::BasisType BasisType;

  // set up the trace: in this case, it's a node so order one default
  const int order = 1;
  const BasisFunctionCategory category = category_;

  // grab the basis
  const BasisType* basis = BasisType::getBasisFunction(order, category);

  // create the field associativity constructor
  std::unique_ptr<FieldTraceConstructorType> pfldAssocTrace( new FieldTraceConstructorType( basis, xfldTraceGroup.nElem() ) );

  // make an int array to store the native node indices of the traces
  int nodeMap[Topology::NNode];

  // loop over the traces in the grid tracegroup
  for (int itrace = 0; itrace < xfldTraceGroup.nElem(); itrace++)
  {

    // set the processor rank for the solution trace based on the associated grid trace
    pfldAssocTrace->setAssociativity(itrace).setRank( xfldTraceGroup.associativity( itrace ).rank() );

    // set normalSign[L|R] for nodes based on their grid counterparts
    const int normalSignL = xfldTraceGroup.associativity( itrace ).normalSignL();
    const int normalSignR = xfldTraceGroup.associativity( itrace ).normalSignR();
    if (normalSignL != 0 ) pfldAssocTrace->setAssociativity(itrace).setNormalSignL( normalSignL );
    if (normalSignR != 0 ) pfldAssocTrace->setAssociativity(itrace).setNormalSignR( normalSignR );

    // get the node mapping from the grid
    xfldTraceGroup.associativity( itrace ).getNodeGlobalMapping( nodeMap, Topology::NNode );

    // update the node numbering and set node DOFs

    // loop over the nodes
    for (int k = 0; k < Topology::NNode; k++)
      // for node, take the global node map and flip it for the local node numbering for the native node!
      nodeMap[k] = nativeNodeDOFs.getLocalNode(nodeMap[k]);

    // save the global node ordering list
    pfldAssocTrace->setAssociativity(itrace).setNodeGlobalMapping( nodeMap, Topology::NNode );
  }

  return pfldAssocTrace;
}


//============================================================================//
// Class for constructing indexing for a continuous field
//============================================================================//
template <class PhysDim>
struct Field_CG_CellConstructorBase<PhysDim, TopoD2>
{
  Field_CG_CellConstructorBase(const XField<PhysDim, TopoD2>& xfld, const int order, const BasisFunctionCategory category,
                               const std::vector<std::vector<int>>& CellGroupSets, const bool needsEmbeddedGhosts );

  int nElem()         const { return nElem_;         }
  int nDOF()          const { return nDOF_;          }
  int nDOFpossessed() const { return nDOFpossessed_; }
  int nDOFghost()     const { return nDOFghost_;     }

  template<class FieldCellGroupClass>
  FieldCellGroupClass*
  createCellGroup( const int cellGroup, int* local2nativeDOFmap );

  void getDOF_rank(int* DOF_rank);

protected:
  const XField<PhysDim, TopoD2>& xfld_;

  //A map of all the Cell DOFs in a native indexing
  std::vector<Field_CG_CellDOF> nativeCellDOFs_;

  //A map of all the edge DOFs in a native indexing
  std::vector<Field_CG_EdgeDOF> nativeEdgeDOFs_;

  //Used to count unique node DOFs in a native indexing
  std::vector<Field_CG_NodeDOF> nativeNodeDOFs_;

  const std::vector<std::vector<int>> CellGroupSets_;

  const bool needsEmbeddedGhosts_;

  const int order_;
  const BasisFunctionCategory category_;

  // Total number of cell elements
  int nElem_;

  // Total DOF count
  int nDOF_;
  int nDOFpossessed_;
  int nDOFghost_;

  template <class Topology>
  void
  setCellGroupDOFPossession(const typename XField<PhysDim, TopoD2>::template FieldCellGroupType<Topology>& xfldCellGroup,
                            const int cellGroup,
                            Field_CG_CellDOF& nativeCellDOFs,
                            Field_CG_EdgeDOF& nativeEdgeDOFs,
                            Field_CG_NodeDOF& nativeNodeDOFs);

  template<class FieldCellGroupClass, class Topology>
  FieldCellGroupClass*
  createCellGroup( const int cellGroup,
                   const typename XField<PhysDim, TopoD2>::template FieldCellGroupType<Topology>& xfldCellGroup,
                   Field_CG_CellDOF& nativeCellDOFs,
                   Field_CG_EdgeDOF& nativeEdgeDOFs,
                   Field_CG_NodeDOF& nativeNodeDOFs,
                   int* local2nativeDOFmap );

  template <class FieldTraceConstructorType>
  std::unique_ptr<FieldTraceConstructorType>
  createInteriorTraceGroup( const int iTraceGroup );

  template <class FieldTraceConstructorType>
  std::unique_ptr<FieldTraceConstructorType>
  createBoundaryTraceGroup( const int bTraceGroup );

  template <class FieldTraceConstructorType>
  std::unique_ptr<FieldTraceConstructorType>
  createGhostBoundaryTraceGroup( const int bTraceGroup );

  template <class FieldTraceConstructorType, class Topology>
  std::unique_ptr<FieldTraceConstructorType>
  createTraceGroup( const typename XField<PhysDim, TopoD2>::template FieldTraceGroupType<Topology>& xfldTraceGroup,
                    Field_CG_EdgeDOF& nativeEdgeDOFs,
                    Field_CG_NodeDOF& nativeNodeDOFs );

};


//---------------------------------------------------------------------------//
template <class PhysDim>
template <class FieldCellGroupClass>
FieldCellGroupClass*
Field_CG_CellConstructorBase<PhysDim, TopoD2>::
createCellGroup( const int cellGroup, int* local2nativeDOFmap )
{
  typedef typename FieldCellGroupClass::TopologyType Topology;

  for (std::size_t i = 0; i < CellGroupSets_.size(); i++)
  {
    const std::vector<int>& CellGroups = CellGroupSets_[i];

    if (std::find(CellGroups.begin(), CellGroups.end(), cellGroup) != CellGroups.end())
      return this->template createCellGroup<FieldCellGroupClass, Topology>(
          cellGroup,
          xfld_.template getCellGroup<Topology>(cellGroup),
          nativeCellDOFs_[i],
          nativeEdgeDOFs_[i],
          nativeNodeDOFs_[i],
          local2nativeDOFmap );
  }

  SANS_DEVELOPER_EXCEPTION("Count not find cell group!!");
  return nullptr;
}

//---------------------------------------------------------------------------//
template <class PhysDim>
template <class FieldCellGroupClass, class Topology>
FieldCellGroupClass*
Field_CG_CellConstructorBase<PhysDim, TopoD2>::
createCellGroup( const int cellGroup,
                 const typename XField<PhysDim, TopoD2>::template FieldCellGroupType<Topology>& xfldCellGroup,
                 Field_CG_CellDOF& nativeCellDOFs,
                 Field_CG_EdgeDOF& nativeEdgeDOFs,
                 Field_CG_NodeDOF& nativeNodeDOFs,
                 int* local2nativeDOFmap )
{
  typedef typename FieldCellGroupClass::BasisType BasisType;

  const int (*EdgeNodes)[ Line::NNode ] = ElementEdges<Topology>::EdgeNodes;

  const int order = order_;
  const BasisFunctionCategory category = category_;

  const int nDOFperCell = TopologyDOF_CG<Topology>::count(order);
  const int nDOFperEdge = TopologyDOF_CG<Line>::count(order);

  const BasisType* basis = BasisType::getBasisFunction(order, category);

  // create field associativity constructor
  typename FieldCellGroupClass::FieldAssociativityConstructorType fldAssocCell( basis, xfldCellGroup.nElem() );

  // nodal, cell DOF ordering
  int nodeMap[Topology::NNode];
  std::vector<int> edgeMap(Topology::NEdge*nDOFperEdge);

  for (int icell = 0; icell < xfldCellGroup.nElem(); icell++)
  {
    // set the processor rank
    fldAssocCell.setAssociativity(icell).setRank( xfldCellGroup.associativity( icell ).rank() );

    // Retrieve cells DOFs and set them
    if (nDOFperCell > 0)
    {
      Field_CG_CellDOF::CellDOFType& cell    = nativeCellDOFs.getNativeCell(icell, cellGroup);
      const std::vector<int>&        cellDOF = nativeCellDOFs.getLocalCell(icell, cellGroup);

      //keep increasing the cellDOF index as we add new cell DOFs
      for (std::size_t k = 0; k < cellDOF.size(); k++)
        local2nativeDOFmap[cellDOF[k]] = cell.nativeCellDOFs[k];

      fldAssocCell.setAssociativity(icell).setCellGlobalMapping( cellDOF );
    }

    // get the node mapping of the grid
    xfldCellGroup.associativity( icell ).getNodeGlobalMapping( nodeMap, Topology::NNode );

    if (nDOFperEdge > 0)
    {
      // Retrieve edge DOFs and set them
      int iedgeDOF = 0;
      for (int iedge = 0; iedge < Topology::NEdge; iedge++)
      {
        // Get the nodes for the canonical edge
        int node0 = nodeMap[EdgeNodes[iedge][0]];
        int node1 = nodeMap[EdgeNodes[iedge][1]];

        Field_CG_EdgeDOF::EdgeDOFType& edge    = nativeEdgeDOFs.getNativeEdge(node0, node1);
        std::vector<int>               edgeDOF = nativeEdgeDOFs.getLocalEdge(node0, node1);

        std::vector<int> nativeEdgeDOFs = edge.nativeEdgeDOFs;

        //Reverse the DOF ordering if the basis is Lagrange and the edge orientation is reversed
        if (category_ == BasisFunctionCategory_Lagrange && edge.nativeNode0 != xfld_.local2nativeDOFmap(node0))
        {
          std::reverse(edgeDOF.begin(), edgeDOF.end());
          std::reverse(nativeEdgeDOFs.begin(), nativeEdgeDOFs.end());
        }

        // copy over the edge map
        for (int i = 0; i < nDOFperEdge; i++)
        {
          edgeMap[iedgeDOF] = edgeDOF[i];
          local2nativeDOFmap[edgeDOF[i]] = nativeEdgeDOFs[i];
          iedgeDOF++;
        }
      }
      fldAssocCell.setAssociativity(icell).setEdgeGlobalMapping(edgeMap);
    }

    // update the node numbering and set node DOFs
    for (int k = 0; k < Topology::NNode; k++)
    {
      Field_CG_NodeDOF::NodeDOFType& node    = nativeNodeDOFs.getNativeNode(nodeMap[k]);
      const int                      nodeDOF = nativeNodeDOFs.getLocalNode(nodeMap[k]);

      nodeMap[k] = nodeDOF;
      local2nativeDOFmap[nodeDOF] = node.nativeNodeDOF;
    }
    fldAssocCell.setAssociativity(icell).setNodeGlobalMapping( nodeMap, Topology::NNode );

    // copy over the trace orientations
    fldAssocCell.setAssociativity(icell).traceOrientation() = xfldCellGroup.associativity( icell ).traceOrientation();
  }

  // build and pass the complete associativity object
  return new FieldCellGroupClass(fldAssocCell);
}

//---------------------------------------------------------------------------//
template <class PhysDim>
template <class FieldTraceConstructorType>
std::unique_ptr<FieldTraceConstructorType>
Field_CG_CellConstructorBase<PhysDim, TopoD2>::
createInteriorTraceGroup( const int iTraceGroup )
{
  typedef typename FieldTraceConstructorType::TopologyType Topology;

  int cellGroupL = xfld_.getInteriorTraceGroupBase(iTraceGroup).getGroupLeft();

  for (std::size_t i = 0; i < CellGroupSets_.size(); i++)
  {
    const std::vector<int>& CellGroups = CellGroupSets_[i];

    // check if the current set of cell groups is to the left if the iTrace group
    if (std::find(CellGroups.begin(), CellGroups.end(), cellGroupL) != CellGroups.end())
      return this->template createTraceGroup<FieldTraceConstructorType, Topology>(
          xfld_.template getInteriorTraceGroup<Topology>(iTraceGroup),
          nativeEdgeDOFs_[i],
          nativeNodeDOFs_[i] );
  }

  SANS_DEVELOPER_EXCEPTION("Count not find left cell group!!");
  return nullptr;
}

//---------------------------------------------------------------------------//
template <class PhysDim>
template <class FieldTraceConstructorType>
std::unique_ptr<FieldTraceConstructorType>
Field_CG_CellConstructorBase<PhysDim, TopoD2>::
createBoundaryTraceGroup( const int bTraceGroup )
{
  typedef typename FieldTraceConstructorType::TopologyType Topology;

  int cellGroupL = xfld_.getBoundaryTraceGroupBase(bTraceGroup).getGroupLeft();

  for (std::size_t i = 0; i < CellGroupSets_.size(); i++)
  {
    const std::vector<int>& CellGroups = CellGroupSets_[i];

    // check if the current set of cell groups is to the left of the bTrace group
    if (std::find(CellGroups.begin(), CellGroups.end(), cellGroupL) != CellGroups.end())
      return this->template createTraceGroup<FieldTraceConstructorType, Topology>(
          xfld_.template getBoundaryTraceGroup<Topology>(bTraceGroup),
          nativeEdgeDOFs_[i],
          nativeNodeDOFs_[i] );
  }

  SANS_DEVELOPER_EXCEPTION("Count not find left cell group!!");
  return nullptr;
}

//---------------------------------------------------------------------------//
template <class PhysDim>
template <class FieldTraceConstructorType>
std::unique_ptr<FieldTraceConstructorType>
Field_CG_CellConstructorBase<PhysDim, TopoD2>::
createGhostBoundaryTraceGroup( const int bTraceGroup )
{
  typedef typename FieldTraceConstructorType::TopologyType Topology;

  int cellGroupL = xfld_.getGhostBoundaryTraceGroupBase(bTraceGroup).getGroupLeft();

  for (std::size_t i = 0; i < CellGroupSets_.size(); i++)
  {
    const std::vector<int>& CellGroups = CellGroupSets_[i];

    // check if the current set of cell groups is to the left of the bTrace group
    if (std::find(CellGroups.begin(), CellGroups.end(), cellGroupL) != CellGroups.end())
      return this->template createTraceGroup<FieldTraceConstructorType, Topology>(
          xfld_.template getGhostBoundaryTraceGroup<Topology>(bTraceGroup),
          nativeEdgeDOFs_[i],
          nativeNodeDOFs_[i] );
  }

  SANS_DEVELOPER_EXCEPTION("Count not find left cell group!!");
  return nullptr;
}

//---------------------------------------------------------------------------//
template <class PhysDim>
template <class FieldTraceConstructorType, class Topology>
std::unique_ptr<FieldTraceConstructorType>
Field_CG_CellConstructorBase<PhysDim, TopoD2>::
createTraceGroup( const typename XField<PhysDim, TopoD2>::template FieldTraceGroupType<Topology>& xfldTraceGroup,
                  Field_CG_EdgeDOF& nativeEdgeDOFs,
                  Field_CG_NodeDOF& nativeNodeDOFs )
{
  typedef typename FieldTraceConstructorType::BasisType BasisType;

  const int order = order_;
  const BasisFunctionCategory category = category_;

  const int nDOFperEdge = TopologyDOF_CG<Line>::count(order);

  const BasisType* basis = BasisType::getBasisFunction(order, category);

  // create field associativity constructor
  std::unique_ptr<FieldTraceConstructorType> pfldAssocTrace( new FieldTraceConstructorType( basis, xfldTraceGroup.nElem() ) );

  int nodeMap[Topology::NNode];

  for (int itrace = 0; itrace < xfldTraceGroup.nElem(); itrace++)
  {
    // set the processor rank
    pfldAssocTrace->setAssociativity(itrace).setRank( xfldTraceGroup.associativity( itrace ).rank() );

    // get the node mapping of the grid
    xfldTraceGroup.associativity( itrace ).getNodeGlobalMapping( nodeMap, Topology::NNode );

    if (nDOFperEdge > 0)
    {
      // Get the nodes for the canonical edge
      int node0 = nodeMap[0];
      int node1 = nodeMap[1];

      Field_CG_EdgeDOF::EdgeDOFType& edge    = nativeEdgeDOFs.getNativeEdge(node0, node1);
      std::vector<int>               edgeDOF = nativeEdgeDOFs.getLocalEdge(node0, node1);

      //Reverse the DOF ordering if the basis is Lagrange and the edge orientation is reversed
      if (category_ == BasisFunctionCategory_Lagrange && edge.nativeNode0 != xfld_.local2nativeDOFmap(node0))
        std::reverse(edgeDOF.begin(), edgeDOF.end());

      pfldAssocTrace->setAssociativity(itrace).setEdgeGlobalMapping(edgeDOF);
    }

    // update the node numbering and set node DOFs
    for (int k = 0; k < Topology::NNode; k++)
      nodeMap[k] = nativeNodeDOFs.getLocalNode(nodeMap[k]);

    pfldAssocTrace->setAssociativity(itrace).setNodeGlobalMapping( nodeMap, Topology::NNode );
  }

  return pfldAssocTrace;
}


//============================================================================//
// Class for constructing indexing for a continuous field
//============================================================================//
template <class PhysDim>
struct Field_CG_CellConstructorBase<PhysDim, TopoD3>
{
  Field_CG_CellConstructorBase(const XField<PhysDim, TopoD3>& xfld, const int order, const BasisFunctionCategory category,
                               const std::vector<std::vector<int>>& CellGroupSets, const bool needsEmbeddedGhosts );

  int nElem()         const { return nElem_;         }
  int nDOF()          const { return nDOF_;          }
  int nDOFpossessed() const { return nDOFpossessed_; }
  int nDOFghost()     const { return nDOFghost_;     }

  template<class FieldCellGroupClass>
  FieldCellGroupClass*
  createCellGroup( const int cellGroup, int* local2nativeDOFmap );

  void getDOF_rank(int* DOF_rank);

protected:
  const XField<PhysDim, TopoD3>& xfld_;

  //A map of all the Cell DOFs in a native indexing
  std::vector<Field_CG_CellDOF> nativeCellDOFs_;

  //A map of all the face DOFs in a native indexing
  std::vector<Field_CG_FaceDOF> nativeFaceDOFs_;

  //A map of all the edge DOFs in a native indexing
  std::vector<Field_CG_EdgeDOF> nativeEdgeDOFs_;

  //Used to count unique node DOFs in a native indexing
  std::vector<Field_CG_NodeDOF> nativeNodeDOFs_;

  const std::vector<std::vector<int>> CellGroupSets_;

  const bool needsEmbeddedGhosts_;

  const int order_;
  const BasisFunctionCategory category_;

  // Total number of cell elements
  int nElem_;

  // Total DOF count
  int nDOF_;
  int nDOFpossessed_;
  int nDOFghost_;

  template <class Topology>
  void
  setCellGroupDOFPossession(const typename XField<PhysDim, TopoD3>::template FieldCellGroupType<Topology>& xfldCellGroup,
                            const int cellGroup,
                            Field_CG_CellDOF& nativeCellDOFs,
                            Field_CG_FaceDOF& nativeFaceDOFs,
                            Field_CG_EdgeDOF& nativeEdgeDOFs,
                            Field_CG_NodeDOF& nativeNodeDOFs);

  template<class FieldCellGroupClass, class Topology>
  FieldCellGroupClass*
  createCellGroup( const int cellGroup,
                   const typename XField<PhysDim, TopoD3>::template FieldCellGroupType<Topology>& xfldCellGroup,
                   Field_CG_CellDOF& nativeCellDOFs,
                   Field_CG_FaceDOF& nativeFaceDOFs,
                   Field_CG_EdgeDOF& nativeEdgeDOFs,
                   Field_CG_NodeDOF& nativeNodeDOFs,
                   int* local2nativeDOFmap );

  template <class FieldTraceConstructorType>
  std::unique_ptr<FieldTraceConstructorType>
  createInteriorTraceGroup( const int iTraceGroup );

  template <class FieldTraceConstructorType>
  std::unique_ptr<FieldTraceConstructorType>
  createBoundaryTraceGroup( const int bTraceGroup );

  template <class FieldTraceConstructorType>
  std::unique_ptr<FieldTraceConstructorType>
  createGhostBoundaryTraceGroup( const int bTraceGroup );

  template <class FieldTraceConstructorType, class Topology>
  std::unique_ptr<FieldTraceConstructorType>
  createTraceGroup( const typename XField<PhysDim, TopoD3>::template FieldTraceGroupType<Topology>& xfldTraceGroup,
                    Field_CG_FaceDOF& nativeFaceDOFs,
                    Field_CG_EdgeDOF& nativeEdgeDOFs,
                    Field_CG_NodeDOF& nativeNodeDOFs );
};

//---------------------------------------------------------------------------//
template <class PhysDim>
template <class FieldCellGroupClass>
FieldCellGroupClass*
Field_CG_CellConstructorBase<PhysDim, TopoD3>::
createCellGroup( const int cellGroup, int* local2nativeDOFmap )
{
  typedef typename FieldCellGroupClass::TopologyType Topology;

  for (std::size_t i = 0; i < CellGroupSets_.size(); i++)
  {
    const std::vector<int>& CellGroups = CellGroupSets_[i];

    if (std::find(CellGroups.begin(), CellGroups.end(), cellGroup) != CellGroups.end())
      return this->template createCellGroup<FieldCellGroupClass, Topology>(
          cellGroup,
          xfld_.template getCellGroup<Topology>(cellGroup),
          nativeCellDOFs_[i],
          nativeFaceDOFs_[i],
          nativeEdgeDOFs_[i],
          nativeNodeDOFs_[i],
          local2nativeDOFmap );
  }

  SANS_DEVELOPER_EXCEPTION("Count not find cell group!!");
  return nullptr;
}

//---------------------------------------------------------------------------//
template <class PhysDim>
template <class FieldCellGroupClass, class Topology>
FieldCellGroupClass*
Field_CG_CellConstructorBase<PhysDim, TopoD3>::
createCellGroup( const int cellGroup,
                 const typename XField<PhysDim, TopoD3>::template FieldCellGroupType<Topology>& xfldCellGroup,
                 Field_CG_CellDOF& nativeCellDOFs,
                 Field_CG_FaceDOF& nativeFaceDOFs,
                 Field_CG_EdgeDOF& nativeEdgeDOFs,
                 Field_CG_NodeDOF& nativeNodeDOFs,
                 int* local2nativeDOFmap )
{
  typedef typename Topology::TopologyTrace TopologyTrace;
  typedef typename FieldCellGroupClass::BasisType BasisType;

  const int (*EdgeNodes)[ Line::NNode ] = ElementEdges<Topology>::EdgeNodes;
  const int (*FaceNodes)[ TopologyTrace::NNode ] = TraceToCellRefCoord<TopologyTrace, TopoD3, Topology>::TraceNodes;

  const int order = order_;
  const BasisFunctionCategory category = category_;

  const int nDOFperCell = TopologyDOF_CG<Topology>::count(order);
  const int nDOFperFace = TopologyDOF_CG<TopologyTrace>::count(order);
  const int nDOFperEdge = TopologyDOF_CG<Line>::count(order);

  const BasisType* basis = BasisType::getBasisFunction(order, category);

  // create field associativity constructor
  typename FieldCellGroupClass::FieldAssociativityConstructorType fldAssocCell( basis, xfldCellGroup.nElem() );

  std::vector<int> nodeMap(Topology::NNode);
  std::vector<int> faceMap(TopologyTrace::NNode);
  std::vector<int> edgeMap(Topology::NEdge*nDOFperEdge);
  std::vector<int> nativeNodes(faceMap.size());
  CanonicalTraceToCell canonicalFace;

  for (int icell = 0; icell < xfldCellGroup.nElem(); icell++)
  {
    // set the processor rank
    fldAssocCell.setAssociativity(icell).setRank( xfldCellGroup.associativity( icell ).rank() );

    // Retrieve cells DOFs and set them
    if (nDOFperCell > 0)
    {
      Field_CG_CellDOF::CellDOFType& cell    = nativeCellDOFs.getNativeCell(icell, cellGroup);
      const std::vector<int>&        cellMap = nativeCellDOFs.getLocalCell(icell, cellGroup);

      //keep increasing the cellDOF index as we add new cell DOFs
      for (std::size_t k = 0; k < cellMap.size(); k++)
        local2nativeDOFmap[cellMap[k]] = cell.nativeCellDOFs[k];

      fldAssocCell.setAssociativity(icell).setCellGlobalMapping( cellMap );
    }

    // get the node mapping of the grid
    xfldCellGroup.associativity( icell ).getNodeGlobalMapping( nodeMap.data(), nodeMap.size() );

    if (nDOFperFace > 0)
    {
      // Retrieve face DOFs and set them
      for (int iface = 0; iface < Topology::NFace; iface++)
      {
        for (int n = 0; n < TopologyTrace::NNode; n++)
          faceMap[n] = nodeMap[FaceNodes[iface][n]];

        Field_CG_FaceDOF::FaceDOFType& face    = nativeFaceDOFs.getNativeFace(TopologyTrace::Topology, faceMap);
        std::vector<int>               faceDOF = nativeFaceDOFs.getLocalFace(TopologyTrace::Topology, faceMap);

        for (std::size_t i = 0; i < faceMap.size(); i++)
          nativeNodes[i] = xfld_.local2nativeDOFmap(faceMap[i]);

        {
          // Get the orientation
          int orientation = CanonicalOrientation<TopologyTrace, TopoD3>::get(nativeNodes, face.nativeNodes);

          std::vector<int> nativeFaceDOFs = face.nativeFaceDOFs;
          orientFaceDOFs<TopologyTrace>( order, category, orientation, faceDOF, nativeFaceDOFs );

          canonicalFace.orientation = orientation;
          canonicalFace.trace = iface;

          // set the native mapping
          for (int i = 0; i < nDOFperFace; i++)
            local2nativeDOFmap[faceDOF[i]] = nativeFaceDOFs[i];
        }

        fldAssocCell.setAssociativity(icell).setFaceGlobalMapping(faceDOF.data(), faceDOF.size(), canonicalFace);
      }
    }

    if (nDOFperEdge > 0)
    {
      // Retrieve edge DOFs and set them
      int iedgeDOF = 0;
      for (int iedge = 0; iedge < Topology::NEdge; iedge++)
      {
        // Get the nodes for the canonical edge
        int node0 = nodeMap[EdgeNodes[iedge][0]];
        int node1 = nodeMap[EdgeNodes[iedge][1]];

        Field_CG_EdgeDOF::EdgeDOFType& edge    = nativeEdgeDOFs.getNativeEdge(node0, node1);
        std::vector<int>               edgeDOF = nativeEdgeDOFs.getLocalEdge(node0, node1);

        std::vector<int> nativeEdgeDOFs = edge.nativeEdgeDOFs;

        //Reverse the DOF ordering if the basis is Lagrange and the edge orientation is reversed
        if (category == BasisFunctionCategory_Lagrange && edge.nativeNode0 != xfld_.local2nativeDOFmap(node0))
        {
          std::reverse(edgeDOF.begin(), edgeDOF.end());
          std::reverse(nativeEdgeDOFs.begin(), nativeEdgeDOFs.end());
        }

        // copy over the edge map
        for (int i = 0; i < nDOFperEdge; i++)
        {
          edgeMap[iedgeDOF] = edgeDOF[i];
          local2nativeDOFmap[edgeDOF[i]] = nativeEdgeDOFs[i];
          iedgeDOF++;
        }
      }
      fldAssocCell.setAssociativity(icell).setEdgeGlobalMapping(edgeMap);
    }

    // update the node numbering and set node DOFs
    for (int k = 0; k < Topology::NNode; k++)
    {
      Field_CG_NodeDOF::NodeDOFType& node    = nativeNodeDOFs.getNativeNode(nodeMap[k]);
      const int                      nodeDOF = nativeNodeDOFs.getLocalNode(nodeMap[k]);

      nodeMap[k] = nodeDOF;
      local2nativeDOFmap[nodeDOF] = node.nativeNodeDOF;
    }
    fldAssocCell.setAssociativity(icell).setNodeGlobalMapping( nodeMap );

    // copy over the trace orientations
    fldAssocCell.setAssociativity(icell).traceOrientation() = xfldCellGroup.associativity( icell ).traceOrientation();
  }

  return new FieldCellGroupClass(fldAssocCell);
}

//---------------------------------------------------------------------------//
template <class PhysDim>
template <class FieldTraceConstructorType>
std::unique_ptr<FieldTraceConstructorType>
Field_CG_CellConstructorBase<PhysDim, TopoD3>::
createInteriorTraceGroup( const int iTraceGroup )
{
  typedef typename FieldTraceConstructorType::TopologyType Topology;

  int cellGroupL = xfld_.getInteriorTraceGroupBase(iTraceGroup).getGroupLeft();

  for (std::size_t i = 0; i < CellGroupSets_.size(); i++)
  {
    const std::vector<int>& CellGroups = CellGroupSets_[i];

    // check if the current set of cell groups is to the left if the iTrace group
    if (std::find(CellGroups.begin(), CellGroups.end(), cellGroupL) != CellGroups.end())
      return this->template createTraceGroup<FieldTraceConstructorType, Topology>(
          xfld_.template getInteriorTraceGroup<Topology>(iTraceGroup),
          nativeFaceDOFs_[i],
          nativeEdgeDOFs_[i],
          nativeNodeDOFs_[i] );
  }

  SANS_DEVELOPER_EXCEPTION("Count not find left cell group!!");
  return nullptr;
}

//---------------------------------------------------------------------------//
template <class PhysDim>
template <class FieldTraceConstructorType>
std::unique_ptr<FieldTraceConstructorType>
Field_CG_CellConstructorBase<PhysDim, TopoD3>::
createBoundaryTraceGroup( const int bTraceGroup )
{
  typedef typename FieldTraceConstructorType::TopologyType Topology;

  int cellGroupL = xfld_.getBoundaryTraceGroupBase(bTraceGroup).getGroupLeft();

  for (std::size_t i = 0; i < CellGroupSets_.size(); i++)
  {
    const std::vector<int>& CellGroups = CellGroupSets_[i];

    // check if the current set of cell groups is to the left othe bTrace group
    if (std::find(CellGroups.begin(), CellGroups.end(), cellGroupL) != CellGroups.end())
      return this->template createTraceGroup<FieldTraceConstructorType, Topology>(
          xfld_.template getBoundaryTraceGroup<Topology>(bTraceGroup),
          nativeFaceDOFs_[i],
          nativeEdgeDOFs_[i],
          nativeNodeDOFs_[i] );
  }

  SANS_DEVELOPER_EXCEPTION("Count not find left cell group!!");
  return nullptr;
}

//---------------------------------------------------------------------------//
template <class PhysDim>
template <class FieldTraceConstructorType>
std::unique_ptr<FieldTraceConstructorType>
Field_CG_CellConstructorBase<PhysDim, TopoD3>::
createGhostBoundaryTraceGroup( const int bTraceGroup )
{
  typedef typename FieldTraceConstructorType::TopologyType Topology;

  int cellGroupL = xfld_.getGhostBoundaryTraceGroupBase(bTraceGroup).getGroupLeft();

  for (std::size_t i = 0; i < CellGroupSets_.size(); i++)
  {
    const std::vector<int>& CellGroups = CellGroupSets_[i];

    // check if the current set of cell groups is to the left othe bTrace group
    if (std::find(CellGroups.begin(), CellGroups.end(), cellGroupL) != CellGroups.end())
      return this->template createTraceGroup<FieldTraceConstructorType, Topology>(
          xfld_.template getGhostBoundaryTraceGroup<Topology>(bTraceGroup),
          nativeFaceDOFs_[i],
          nativeEdgeDOFs_[i],
          nativeNodeDOFs_[i] );
  }

  SANS_DEVELOPER_EXCEPTION("Count not find left cell group!!");
  return nullptr;
}

//---------------------------------------------------------------------------//
template <class PhysDim>
template <class FieldTraceConstructorType, class Topology>
std::unique_ptr<FieldTraceConstructorType>
Field_CG_CellConstructorBase<PhysDim, TopoD3>::
createTraceGroup( const typename XField<PhysDim, TopoD3>::template FieldTraceGroupType<Topology>& xfldTraceGroup,
                  Field_CG_FaceDOF& nativeFaceDOFs,
                  Field_CG_EdgeDOF& nativeEdgeDOFs,
                  Field_CG_NodeDOF& nativeNodeDOFs )
{
  typedef typename FieldTraceConstructorType::BasisType BasisType;

  const int (*EdgeNodes)[ Line::NNode ] = ElementEdges<Topology>::EdgeNodes;

  const int order = order_;
  const BasisFunctionCategory category = category_;

  const int nDOFperFace = TopologyDOF_CG<Topology>::count(order);
  const int nDOFperEdge = TopologyDOF_CG<Line>::count(order);

  const BasisType* basis = BasisType::getBasisFunction(order, category);

  // create field associativity constructor
  std::unique_ptr<FieldTraceConstructorType> pfldAssocTrace( new FieldTraceConstructorType( basis, xfldTraceGroup.nElem() ) );

  std::vector<int> nodeMap(Topology::NNode);
  std::vector<int> edgeMap(Topology::NEdge*nDOFperEdge);

  std::vector<int> nativefaceDOF(nDOFperFace, 0);
  std::vector<int> nativeNodes(nodeMap.size());

  for (int itrace = 0; itrace < xfldTraceGroup.nElem(); itrace++)
  {
    // set the processor rank
    pfldAssocTrace->setAssociativity(itrace).setRank( xfldTraceGroup.associativity( itrace ).rank() );

    // get the node mapping of the grid
    xfldTraceGroup.associativity( itrace ).getNodeGlobalMapping( nodeMap.data(), nodeMap.size() );

    if (nDOFperFace > 0)
    {
      // Retrieve face DOFs and set them
      Field_CG_FaceDOF::FaceDOFType& face    = nativeFaceDOFs.getNativeFace(Topology::Topology, nodeMap);
      std::vector<int>               faceDOF = nativeFaceDOFs.getLocalFace(Topology::Topology, nodeMap);

      for (std::size_t i = 0; i < nodeMap.size(); i++)
        nativeNodes[i] = xfld_.local2nativeDOFmap(nodeMap[i]);

      // Get the orientation
      int orientation = CanonicalOrientation<Topology, TopoD3>::get(nativeNodes, face.nativeNodes);

      orientFaceDOFs<Topology>( order, category, orientation, faceDOF, nativefaceDOF );

      pfldAssocTrace->setAssociativity(itrace).setCellGlobalMapping(faceDOF.data(), faceDOF.size());
    }


    if (nDOFperEdge > 0)
    {
      // Retrieve edge DOFs and set them
      int iedgeDOF = 0;
      for (int iedge = 0; iedge < Topology::NEdge; iedge++)
      {
        // Get the nodes for the canonical edge
        int node0 = nodeMap[EdgeNodes[iedge][0]];
        int node1 = nodeMap[EdgeNodes[iedge][1]];

        Field_CG_EdgeDOF::EdgeDOFType& edge    = nativeEdgeDOFs.getNativeEdge(node0, node1);
        std::vector<int>               edgeDOF = nativeEdgeDOFs.getLocalEdge(node0, node1);

        //Reverse the DOF ordering if the basis is Lagrange and the edge orientation is reversed
        if (category == BasisFunctionCategory_Lagrange && edge.nativeNode0 != xfld_.local2nativeDOFmap(node0))
          std::reverse(edgeDOF.begin(), edgeDOF.end());

        // copy over the edge map
        for (int i = 0; i < nDOFperEdge; i++)
        {
          edgeMap[iedgeDOF] = edgeDOF[i];
          iedgeDOF++;
        }
      }
      pfldAssocTrace->setAssociativity(itrace).setEdgeGlobalMapping(edgeMap);
    }

    // update the node numbering and set node DOFs
    for (int k = 0; k < Topology::NNode; k++)
      nodeMap[k] = nativeNodeDOFs.getLocalNode(nodeMap[k]);

    pfldAssocTrace->setAssociativity(itrace).setNodeGlobalMapping( nodeMap );
  }

  return pfldAssocTrace;
}

//============================================================================//
// Class for constructing indexing for a continuous field
//============================================================================//
/** Field_CG_CellConstructorBase is the base class for a cell constructor
 *
 * Field_CG_CellConstructorBase is the fundamental constructor class for the
 * Field_CG_Cell. It carries the fundamentals of the crucial information for
 * the generation of a CG field; the key thing here is that it carries the
 * important underlying stuff that is distributed far beyond the region of a
 * given cell or processor
 */
template <class PhysDim>
struct Field_CG_CellConstructorBase<PhysDim, TopoD4>
{
  // constructor
  Field_CG_CellConstructorBase(const XField<PhysDim, TopoD4>& xfld, const int order, const BasisFunctionCategory category,
                               const std::vector<std::vector<int>>& CellGroupSets, const bool needsEmbeddedGhosts );

  // public functions to return protected variables
  int nElem()         const { return nElem_;         }
  int nDOF()          const { return nDOF_;          }
  int nDOFpossessed() const { return nDOFpossessed_; }
  int nDOFghost()     const { return nDOFghost_;     }

  // function to create cell group
  template<class FieldCellGroupClass>
  FieldCellGroupClass*
  createCellGroup( const int cellGroup, int* local2nativeDOFmap );

  // ???
  void getDOF_rank(int* DOF_rank);

protected:

  // the underlying field mesh that is used to represent the solution
  const XField<PhysDim, TopoD4>& xfld_;

  // a map of all the cell-unique DOFs in a native indexing
  std::vector<Field_CG_CellDOF> nativeCellDOFs_;

  // a map of all the face-unique DOFs in a native indexing
  std::vector<Field_CG_FaceDOF> nativeFaceDOFs_;

  // a map of all the area-unique DOFs in a native indexing
  std::vector<Field_CG_AreaDOF> nativeAreaDOFs_;

  // a map of all the edge-unique DOFs in a native indexing
  std::vector<Field_CG_EdgeDOF> nativeEdgeDOFs_;

  // used to count unique node DOFs in a native indexing
  std::vector<Field_CG_NodeDOF> nativeNodeDOFs_;

  // a vector of sets of cellgroups
  // above maps are all vectored away by set
  const std::vector<std::vector<int>> CellGroupSets_;

  const bool needsEmbeddedGhosts_;

  // the basis function specification
  const int order_;
  const BasisFunctionCategory category_;

  // total number of cell elements
  int nElem_;

  // total DOF count
  int nDOF_;                    // total
  int nDOFpossessed_;           // DOFs that are possessed locally
  int nDOFghost_;               // DOFs that are possessed elsewhere!

  template <class Topology>
  void
  setCellGroupDOFPossession(const typename XField<PhysDim, TopoD4>::template FieldCellGroupType<Topology>& xfldCellGroup,
                            const int cellGroup,
                            Field_CG_CellDOF& nativeCellDOFs,
                            Field_CG_FaceDOF& nativeFaceDOFs,
                            Field_CG_AreaDOF& nativeAreaDOFs,
                            Field_CG_EdgeDOF& nativeEdgeDOFs,
                            Field_CG_NodeDOF& nativeNodeDOFs);

  template<class FieldCellGroupClass, class Topology>
  FieldCellGroupClass*
  createCellGroup( const int cellGroup,
                   const typename XField<PhysDim, TopoD4>::template FieldCellGroupType<Topology>& xfldCellGroup,
                   Field_CG_CellDOF& nativeCellDOFs,
                   Field_CG_FaceDOF& nativeFaceDOFs,
                   Field_CG_AreaDOF& nativeAreaDOFs,
                   Field_CG_EdgeDOF& nativeEdgeDOFs,
                   Field_CG_NodeDOF& nativeNodeDOFs,
                   int* local2nativeDOFmap );

  template <class FieldTraceConstructorType>
  std::unique_ptr<FieldTraceConstructorType>
  createInteriorTraceGroup( const int iTraceGroup );

  template <class FieldTraceConstructorType>
  std::unique_ptr<FieldTraceConstructorType>
  createBoundaryTraceGroup( const int bTraceGroup );

  template <class FieldTraceConstructorType>
  std::unique_ptr<FieldTraceConstructorType>
  createGhostBoundaryTraceGroup( const int bTraceGroup );

  template <class FieldTraceConstructorType, class Topology>
  std::unique_ptr<FieldTraceConstructorType>
  createTraceGroup( const typename XField<PhysDim, TopoD4>::template FieldTraceGroupType<Topology>& xfldTraceGroup,
                    Field_CG_FaceDOF& nativeFaceDOFs,
                    Field_CG_AreaDOF& nativeAreaDOFs,
                    Field_CG_EdgeDOF& nativeEdgeDOFs,
                    Field_CG_NodeDOF& nativeNodeDOFs );
};

//---------------------------------------------------------------------------//
template <class PhysDim>
template <class FieldCellGroupClass>
FieldCellGroupClass*
Field_CG_CellConstructorBase<PhysDim, TopoD4>::
createCellGroup( const int cellGroup, int* local2nativeDOFmap )
{
  //SANS_DEVELOPER_EXCEPTION("implement");
  typedef typename FieldCellGroupClass::TopologyType Topology;

  for (std::size_t i = 0; i < CellGroupSets_.size(); i++)
  {
    const std::vector<int>& CellGroups = CellGroupSets_[i];

    if (std::find(CellGroups.begin(), CellGroups.end(), cellGroup) != CellGroups.end())
      return this->template createCellGroup<FieldCellGroupClass, Topology>(
          cellGroup,
          xfld_.template getCellGroup<Topology>(cellGroup),
          nativeCellDOFs_[i],
          nativeFaceDOFs_[i],
          nativeAreaDOFs_[i],
          nativeEdgeDOFs_[i],
          nativeNodeDOFs_[i],
          local2nativeDOFmap );
  }

  SANS_DEVELOPER_EXCEPTION("Count not find cell group!!");
  return nullptr;
}

//---------------------------------------------------------------------------//

template <class PhysDim>
template <class FieldCellGroupClass, class Topology>
FieldCellGroupClass*
Field_CG_CellConstructorBase<PhysDim, TopoD4>::
createCellGroup( const int cellGroup,
                 const typename XField<PhysDim, TopoD4>::template FieldCellGroupType<Topology>& xfldCellGroup,
                 Field_CG_CellDOF& nativeCellDOFs,
                 Field_CG_FaceDOF& nativeFaceDOFs,
                 Field_CG_AreaDOF& nativeAreaDOFs,
                 Field_CG_EdgeDOF& nativeEdgeDOFs,
                 Field_CG_NodeDOF& nativeNodeDOFs,
                 int* local2nativeDOFmap )
{
  // typedef away the details
  typedef typename Topology::TopologyTrace TopologyTrace;
  typedef typename Topology::TopologyTrace::TopologyTrace TopologyFrame;
  typedef typename FieldCellGroupClass::BasisType BasisType;

  // grab the canonical edges, areas, and faces
  const int (*EdgeNodes)[ Line::NNode ] = ElementEdges<Topology>::EdgeNodes;
  const int (*AreaNodes)[ TopologyFrame::NNode ]= ElementFrame<Topology>::FrameNodes;
  const int (*FaceNodes)[ TopologyTrace::NNode ] = TraceToCellRefCoord<TopologyTrace, TopoD4, Topology>::TraceNodes;

  // basis function order and category used to represent the solution
  const int order = order_;
  const BasisFunctionCategory category = category_;

  // number of purely interior DOFs per entity
  const int nDOFperCell = TopologyDOF_CG<Topology>::count(order);
  const int nDOFperFace = TopologyDOF_CG<TopologyTrace>::count(order);
  const int nDOFperArea = TopologyDOF_CG<TopologyFrame>::count(order);
  const int nDOFperEdge = TopologyDOF_CG<Line>::count(order);

  // grab the specified basis function
  const BasisType* basis = BasisType::getBasisFunction(order, category);

  // create field associativity constructor -> this will eventually be what ties all the entities together
  typename FieldCellGroupClass::FieldAssociativityConstructorType fldAssocCell( basis, xfldCellGroup.nElem() );

  // create ordered node DOF maps
  std::vector<int> nodeMap(Topology::NNode);                  // maps local DOF node index to native DOF node index
  std::vector<int> faceMap(TopologyTrace::NNode);             // maps from trace DOF indices to native DOF node indices
  std::vector<int> areaMap(Topology::NFrame*nDOFperArea);     // maps from the list of DOFs exclusively on the areas to the native DOF node indices
  std::vector<int> edgeMap(Topology::NEdge*nDOFperEdge);      // maps from the list of DOFs exclusively on the edges to the native DOF node indices
  std::vector<int> nativeFaceNodes(faceMap.size());           // maps from the ???
  std::vector<int> nativeAreaNodes(TopologyFrame::NNode);
  CanonicalTraceToCell canonicalFace;

  // loop over the cells in the grid cell group
  for (int icell = 0; icell < xfldCellGroup.nElem(); icell++)
  {

    // set the processor rank based on that used for the grid
    fldAssocCell.setAssociativity(icell).setRank( xfldCellGroup.associativity( icell ).rank() );

    // retrieve cell DOFs and set them
    if (nDOFperCell > 0)
    {
      SANS_DEVELOPER_EXCEPTION("check implementation. -CVF");
      // grab the cell DOF object
      Field_CG_CellDOF::CellDOFType& cell= nativeCellDOFs.getNativeCell(icell, cellGroup);
      // grab the local indices for the DOFs
      const std::vector<int>& cellMap= nativeCellDOFs.getLocalCell(icell, cellGroup);

      // keep increasing the cellDOF index as we add new cell DOFs
      for (std::size_t k = 0; k < cellMap.size(); k++)
        local2nativeDOFmap[cellMap[k]] = cell.nativeCellDOFs[k];

      fldAssocCell.setAssociativity(icell).setCellGlobalMapping( cellMap );
    }

    // get the node mapping of the grid
    xfldCellGroup.associativity( icell ).getNodeGlobalMapping( nodeMap.data(), nodeMap.size() );

    if (nDOFperFace > 0)
    {
      SANS_DEVELOPER_EXCEPTION("check implementation. -CVF");
      // Retrieve face DOFs and set them
      for (int iface = 0; iface < Topology::NFace; iface++)
      {
        for (int n = 0; n < TopologyTrace::NNode; n++)
          faceMap[n] = nodeMap[FaceNodes[iface][n]];

        Field_CG_FaceDOF::FaceDOFType& face    = nativeFaceDOFs.getNativeFace(TopologyTrace::Topology, faceMap);
        std::vector<int>               faceDOF = nativeFaceDOFs.getLocalFace(TopologyTrace::Topology, faceMap);

        for (std::size_t i = 0; i < faceMap.size(); i++)
          nativeFaceNodes[i] = xfld_.local2nativeDOFmap(faceMap[i]);

        {
          // Get the orientation
          int orientation = CanonicalOrientation<TopologyTrace, TopoD4>::get(nativeFaceNodes, face.nativeNodes);

          std::vector<int> nativeFaceDOFs = face.nativeFaceDOFs;
          orientVolumeDOFs<TopologyTrace>( order, category, orientation, faceDOF, nativeFaceDOFs );

          canonicalFace.orientation = orientation;
          canonicalFace.trace = iface;

          // set the native mapping
          for (int i = 0; i < nDOFperFace; i++)
            local2nativeDOFmap[faceDOF[i]] = nativeFaceDOFs[i];
        }

        fldAssocCell.setAssociativity(icell).setFaceGlobalMapping(faceDOF.data(), faceDOF.size(), canonicalFace);
      }
    }

    if (nDOFperArea > 0)
    {
      // SANS_DEVELOPER_EXCEPTION("check implementation. -CVF");

      // counter for area DOFs
      int iareaDOF= 0;

      // loop over the areas that exist
      for (int iarea= 0; iarea < Topology::NFrame; iarea++)
      {

        // create an areaMap for this guy
        std::vector<int> areaMapLocal(TopologyFrame::NNode);

        // populate the areaMap with the grid nodes on the corners of the tri
        for (int n= 0; n < TopologyFrame::NNode; n++)
          areaMapLocal[n]= nodeMap[AreaNodes[iarea][n]];

        // create an object to hold the area and grab its local DOF numbering
        Field_CG_AreaDOF::AreaDOFType &area= nativeAreaDOFs.getNativeArea(TopologyFrame::Topology, areaMapLocal);
        std::vector<int> areaDOF= nativeAreaDOFs.getLocalArea(TopologyFrame::Topology, areaMapLocal);

        // grab the native DOF numberings for the grid cells
        for (std::size_t i= 0; i < areaMapLocal.size(); i++)
          nativeAreaNodes[i]= xfld_.local2nativeDOFmap(areaMapLocal[i]);

        {
          // grab the orientation of the triangle from the (d - 1) canonical
          int orientation= CanonicalOrientation<TopologyFrame, TopoD3>::get(nativeAreaNodes, area.nativeNodes);
          // this might be a problematic line... should work same as before but...

          // a vector for the DOFs associated with the area
          std::vector<int> nativeAreaDOFs= area.nativeAreaDOFs;
          // orient them correctly according to the orientation
          orientFaceDOFs<TopologyFrame>( order, category, orientation, areaDOF, nativeAreaDOFs);

          // grab the orientation and trace data
          canonicalFace.orientation= orientation;
          canonicalFace.trace= iarea;

          // set the native mapping into the DOFs
          for (int i= 0; i < nDOFperArea; i++)
          {
            areaMap[iareaDOF]= areaDOF[i];
            local2nativeDOFmap[areaDOF[i]]= nativeAreaDOFs[i];
            iareaDOF++;
          }
        }
      }
      // set in the associaitivities
      fldAssocCell.setAssociativity(icell).setAreaGlobalMapping(areaMap);
    }

    if (nDOFperEdge > 0)
    {
      // Retrieve edge DOFs and set them
      int iedgeDOF = 0;
      for (int iedge = 0; iedge < Topology::NEdge; iedge++)
      {
        // Get the nodes for the canonical edge
        int node0 = nodeMap[EdgeNodes[iedge][0]];
        int node1 = nodeMap[EdgeNodes[iedge][1]];

        Field_CG_EdgeDOF::EdgeDOFType& edge    = nativeEdgeDOFs.getNativeEdge(node0, node1);
        std::vector<int>               edgeDOF = nativeEdgeDOFs.getLocalEdge(node0, node1);

        std::vector<int> nativeEdgeDOFs = edge.nativeEdgeDOFs;

        //Reverse the DOF ordering if the basis is Lagrange and the edge orientation is reversed
        if (category == BasisFunctionCategory_Lagrange && edge.nativeNode0 != xfld_.local2nativeDOFmap(node0))
        {
          std::reverse(edgeDOF.begin(), edgeDOF.end());
          std::reverse(nativeEdgeDOFs.begin(), nativeEdgeDOFs.end());
        }

        // copy over the edge map
        for (int i = 0; i < nDOFperEdge; i++)
        {
          edgeMap[iedgeDOF] = edgeDOF[i];
          local2nativeDOFmap[edgeDOF[i]] = nativeEdgeDOFs[i];
          iedgeDOF++;
        }

      }
      fldAssocCell.setAssociativity(icell).setEdgeGlobalMapping(edgeMap);

    }

    // update the node numbering and set node DOFs
    for (int k = 0; k < Topology::NNode; k++)
    {
      Field_CG_NodeDOF::NodeDOFType& node    = nativeNodeDOFs.getNativeNode(nodeMap[k]);
      const int                      nodeDOF = nativeNodeDOFs.getLocalNode(nodeMap[k]);

      nodeMap[k] = nodeDOF;
      local2nativeDOFmap[nodeDOF] = node.nativeNodeDOF;
    }
    fldAssocCell.setAssociativity(icell).setNodeGlobalMapping( nodeMap );

    // copy over the trace orientations
    fldAssocCell.setAssociativity(icell).traceOrientation() = xfldCellGroup.associativity( icell ).traceOrientation();

  }

  return new FieldCellGroupClass(fldAssocCell);
}


//---------------------------------------------------------------------------//
template <class PhysDim>
template <class FieldTraceConstructorType>
std::unique_ptr<FieldTraceConstructorType>
Field_CG_CellConstructorBase<PhysDim, TopoD4>::
createInteriorTraceGroup( const int iTraceGroup )
{
  // SANS_DEVELOPER_EXCEPTION("implement");
  typedef typename FieldTraceConstructorType::TopologyType Topology;

  int cellGroupL = xfld_.getInteriorTraceGroupBase(iTraceGroup).getGroupLeft();

  for (std::size_t i = 0; i < CellGroupSets_.size(); i++)
  {
    const std::vector<int>& CellGroups = CellGroupSets_[i];

    // check if the current set of cell groups is to the left if the iTrace group
    if (std::find(CellGroups.begin(), CellGroups.end(), cellGroupL) != CellGroups.end())
      return this->template createTraceGroup<FieldTraceConstructorType, Topology>(
          xfld_.template getInteriorTraceGroup<Topology>(iTraceGroup),
          nativeFaceDOFs_[i],
          nativeAreaDOFs_[i],
          nativeEdgeDOFs_[i],
          nativeNodeDOFs_[i] );
  }

  SANS_DEVELOPER_EXCEPTION("Count not find left cell group!!");
  return nullptr;
}

//---------------------------------------------------------------------------//
template <class PhysDim>
template <class FieldTraceConstructorType>
std::unique_ptr<FieldTraceConstructorType>
Field_CG_CellConstructorBase<PhysDim, TopoD4>::
createBoundaryTraceGroup( const int bTraceGroup )
{
  //SANS_DEVELOPER_EXCEPTION("implement");
  typedef typename FieldTraceConstructorType::TopologyType Topology;

  int cellGroupL = xfld_.getBoundaryTraceGroupBase(bTraceGroup).getGroupLeft();

  for (std::size_t i = 0; i < CellGroupSets_.size(); i++)
  {
    const std::vector<int>& CellGroups = CellGroupSets_[i];

    // check if the current set of cell groups is to the left othe bTrace group
    if (std::find(CellGroups.begin(), CellGroups.end(), cellGroupL) != CellGroups.end())
      return this->template createTraceGroup<FieldTraceConstructorType, Topology>(
          xfld_.template getBoundaryTraceGroup<Topology>(bTraceGroup),
          nativeFaceDOFs_[i],
          nativeAreaDOFs_[i],
          nativeEdgeDOFs_[i],
          nativeNodeDOFs_[i] );
  }

  SANS_DEVELOPER_EXCEPTION("Count not find left cell group!!");
  return nullptr;
}

//---------------------------------------------------------------------------//
template <class PhysDim>
template <class FieldTraceConstructorType>
std::unique_ptr<FieldTraceConstructorType>
Field_CG_CellConstructorBase<PhysDim, TopoD4>::
createGhostBoundaryTraceGroup( const int bTraceGroup )
{
  //SANS_DEVELOPER_EXCEPTION("implement");

  typedef typename FieldTraceConstructorType::TopologyType Topology;

  int cellGroupL = xfld_.getGhostBoundaryTraceGroupBase(bTraceGroup).getGroupLeft();

  for (std::size_t i = 0; i < CellGroupSets_.size(); i++)
  {
    const std::vector<int>& CellGroups = CellGroupSets_[i];

    // check if the current set of cell groups is to the left othe bTrace group
    if (std::find(CellGroups.begin(), CellGroups.end(), cellGroupL) != CellGroups.end())
      return this->template createTraceGroup<FieldTraceConstructorType, Topology>(
          xfld_.template getGhostBoundaryTraceGroup<Topology>(bTraceGroup),
          nativeFaceDOFs_[i],
          nativeAreaDOFs_[i],
          nativeEdgeDOFs_[i],
          nativeNodeDOFs_[i] );
  }

  SANS_DEVELOPER_EXCEPTION("Count not find left cell group!!");
  return nullptr;
}

//---------------------------------------------------------------------------//
template <class PhysDim>
template <class FieldTraceConstructorType, class Topology>
std::unique_ptr<FieldTraceConstructorType>
Field_CG_CellConstructorBase<PhysDim, TopoD4>::
createTraceGroup( const typename XField<PhysDim, TopoD4>::template FieldTraceGroupType<Topology>& xfldTraceGroup,
                  Field_CG_FaceDOF& nativeFaceDOFs,
                  Field_CG_AreaDOF& nativeAreaDOFs,
                  Field_CG_EdgeDOF& nativeEdgeDOFs,
                  Field_CG_NodeDOF& nativeNodeDOFs )
{
  typedef typename FieldTraceConstructorType::BasisType BasisType;

  //SANS_DEVELOPER_EXCEPTION("implement");

  typedef typename Topology::TopologyTrace TraceTopology;

  const int (*EdgeNodes)[ Line::NNode ] = ElementEdges<Topology>::EdgeNodes;
  const int (*AreaNodes)[ TraceTopology::NNode]= TraceToCellRefCoord<TraceTopology, TopoD3, Topology>::TraceNodes;
  // const int (*AreaNodes)[ TraceTopology::NNode]= ElementFrame<Topology>::FrameNodes;
  //const int (*TraceNodes)[ TraceTopology::NNode ] = TraceToCellRefCoord<TraceTopology,TopoD4,Topology>::TraceNodes;

  const int order = order_;
  const BasisFunctionCategory category = category_;

  const int nDOFperFace = TopologyDOF_CG<Topology>::count(order);
  const int nDOFperArea = TopologyDOF_CG<TraceTopology>::count(order);
  const int nDOFperEdge = TopologyDOF_CG<Line>::count(order);

  const BasisType* basis = BasisType::getBasisFunction(order, category);

  // create field associativity constructor
  std::unique_ptr<FieldTraceConstructorType> pfldAssocTrace( new FieldTraceConstructorType( basis, xfldTraceGroup.nElem() ) );

  std::vector<int> nodeMap(Topology::NNode);
  std::vector<int> edgeMap(Topology::NEdge*nDOFperEdge);
  std::vector<int> areaMap(Topology::NTrace*nDOFperArea);

  std::vector<int> nativefaceDOF(nDOFperFace, 0);
  std::vector<int> nativeareaDOF(nDOFperArea, 0);
  std::vector<int> nativeNodes(nodeMap.size());
  std::vector<int> nativeAreaNodes(TraceTopology::NNode);

  // loop over the traces
  for (int itrace = 0; itrace < xfldTraceGroup.nElem(); itrace++)
  {
    // set the processor rank
    pfldAssocTrace->setAssociativity(itrace).setRank( xfldTraceGroup.associativity( itrace ).rank() );

    // get the node mapping of the grid
    xfldTraceGroup.associativity( itrace ).getNodeGlobalMapping( nodeMap.data(), nodeMap.size() );

    if (nDOFperFace > 0)
    {
      // Retrieve face DOFs and set them
      Field_CG_FaceDOF::FaceDOFType& face    = nativeFaceDOFs.getNativeFace(Topology::Topology, nodeMap);
      std::vector<int>               faceDOF = nativeFaceDOFs.getLocalFace(Topology::Topology, nodeMap);

      for (std::size_t i = 0; i < nodeMap.size(); i++)
        nativeNodes[i] = xfld_.local2nativeDOFmap(nodeMap[i]);

      // Get the orientation
      int orientation = CanonicalOrientation<Topology, TopoD4>::get(nativeNodes, face.nativeNodes);

      orientVolumeDOFs<Topology>( order, category, orientation, faceDOF, nativefaceDOF );

      pfldAssocTrace->setAssociativity(itrace).setCellGlobalMapping(faceDOF.data(), faceDOF.size());
    }

    if (nDOFperArea > 0)
    {
      int iareaDOF= 0;

      // loop over the area (e.g.: triangles w.r.t. pentatopes)
      for (int iarea= 0; iarea < Topology::NTrace; iarea++)
      {

        std::vector<int> areaMapLocal(TraceTopology::NNode);
        SANS_ASSERT(TraceTopology::NNode == 3); // here we should be looking at the trace topo of the faces aka triangles

        for (int n= 0; n < TraceTopology::NNode; n++)
          areaMapLocal[n]= nodeMap[AreaNodes[iarea][n]];

        // retrieve face DOFs and set them
        Field_CG_AreaDOF::AreaDOFType &area= nativeAreaDOFs.getNativeArea(TraceTopology::Topology, areaMapLocal);
        std::vector<int> areaDOF= nativeAreaDOFs.getLocalArea(TraceTopology::Topology, areaMapLocal);

        for (std::size_t i= 0; i < areaMapLocal.size(); i++)
          nativeAreaNodes[i]= xfld_.local2nativeDOFmap(areaMapLocal[i]);

        {
          // get the orientation
          int orientation= CanonicalOrientation<TraceTopology, TopoD3>::get(nativeAreaNodes, area.nativeNodes);

          std::vector<int> nativeAreaDOFs= area.nativeAreaDOFs;

          orientFaceDOFs<TraceTopology>( order, category, orientation, areaDOF, nativeareaDOF );

          for (int i= 0; i < nDOFperArea; i++)
          {
            areaMap[iareaDOF]= areaDOF[i];
            iareaDOF++;
          }
        }
      }

      pfldAssocTrace->setAssociativity(itrace).setFaceGlobalMapping(areaMap);
    }

    if (nDOFperEdge > 0)
    {
      // Retrieve edge DOFs and set them
      int iedgeDOF = 0;
      for (int iedge = 0; iedge < Topology::NEdge; iedge++)
      {
        // Get the nodes for the canonical edge
        int node0 = nodeMap[EdgeNodes[iedge][0]];
        int node1 = nodeMap[EdgeNodes[iedge][1]];

        Field_CG_EdgeDOF::EdgeDOFType& edge    = nativeEdgeDOFs.getNativeEdge(node0, node1);
        std::vector<int>               edgeDOF = nativeEdgeDOFs.getLocalEdge(node0, node1);

        //Reverse the DOF ordering if the basis is Lagrange and the edge orientation is reversed
        if (category == BasisFunctionCategory_Lagrange && edge.nativeNode0 != xfld_.local2nativeDOFmap(node0))
          std::reverse(edgeDOF.begin(), edgeDOF.end());

        // copy over the edge map
        for (int i = 0; i < nDOFperEdge; i++)
        {
          edgeMap[iedgeDOF] = edgeDOF[i];
          iedgeDOF++;
        }
      }
      pfldAssocTrace->setAssociativity(itrace).setEdgeGlobalMapping(edgeMap);
    }

    // update the node numbering and set node DOFs
    for (int k = 0; k < Topology::NNode; k++)
      nodeMap[k] = nativeNodeDOFs.getLocalNode(nodeMap[k]);

    pfldAssocTrace->setAssociativity(itrace).setNodeGlobalMapping( nodeMap );
  }

  return pfldAssocTrace;
}

// end TopoD4 ============== //

/** Field_CG_CellConstructor is a constructor class to generate a Field_CG_Cell
 *
 * Field_CG_CellConstructor is a constructor class to generate a Field_CG_Cell correctly, its
 * purpose is to carry the critical information necessary for building a Field_CG_Cell, but not
 * necessarily needed by the Field_CG_Cell after it is created, typically information and tools
 * that involve data across multiple processors.
 */
template <class PhysDim, class TopoDim>
struct Field_CG_CellConstructor : public Field_CG_CellConstructorBase<PhysDim, TopoDim>
{
  typedef Field_CG_CellConstructorBase<PhysDim, TopoDim> BaseType;


  Field_CG_CellConstructor(const XField<PhysDim, TopoDim>& xfld, const int order, const BasisFunctionCategory category,
                           const std::vector<std::vector<int>>& CellGroupSets ) :
      BaseType(xfld, order, category, CellGroupSets, false) {}

  // constructor: calls the base type
  Field_CG_CellConstructor(const XField<PhysDim, TopoDim>& xfld, const int order, const BasisFunctionCategory category,
                           const std::vector<std::vector<int>>& CellGroupSets, const bool needsEmbeddedGhosts ) :
      BaseType(xfld, order, category, CellGroupSets, needsEmbeddedGhosts) {}

  /** createBoundaryTraceGroup creates a boundary trace group
   */
  template <class FieldTraceGroupClass>
  FieldTraceGroupClass*
  createBoundaryTraceGroup( const int bTraceGroup );

  /** createXFieldInteriorTraceGroup creates an XField interior trace group
   */
  template <class FieldTraceGroupClass>
  FieldTraceGroupClass*
  createXFieldInteriorTraceGroup( const int iTraceGroup );

  /** createXFieldBoundaryTraceGroup creates an XField boundary trace group
   */
  template <class FieldTraceGroupClass>
  FieldTraceGroupClass*
  createXFieldBoundaryTraceGroup( const int bTraceGroup );

  /** createXFieldGhostBoundaryTraceGroup creates an XField ghost boundary trace group
   */
  template <class FieldTraceGroupClass>
  FieldTraceGroupClass*
  createXFieldGhostBoundaryTraceGroup( const int bTraceGroup );

protected:
  // promote the underlying mesh to be a public member
  using BaseType::xfld_;

  /** copyTraceConnectivity copies the trace connectivity ???
   */
  template <class Topology, class ElementAssociativityConstructor>
  void
  copyTraceConnectivity( const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Topology>& xfldTraceGroup,
                         std::unique_ptr<ElementConnectivityConstructor<ElementAssociativityConstructor>>& pxfldAssocTrace );
};

//---------------------------------------------------------------------------//
/** createBoundaryTraceGroup creates a FieldtraceGroup for the boundary
 */
template <class PhysDim, class TopoDim>
template <class FieldTraceGroupClass>
FieldTraceGroupClass*
Field_CG_CellConstructor<PhysDim, TopoDim>::
createBoundaryTraceGroup( const int bTraceGroup )
{
  typedef typename FieldTraceGroupClass::FieldAssociativityConstructorType FieldTraceConstructorType;

  // create the associativities for the boundary tracegroup
  std::unique_ptr<FieldTraceConstructorType> xfldAssocTrace =
      this->BaseType::template createBoundaryTraceGroup<FieldTraceConstructorType>(bTraceGroup);

  // return a new of the tracegroup
  return new FieldTraceGroupClass(*xfldAssocTrace);
}

//---------------------------------------------------------------------------//
/** createXFieldInteriorTraceGroup creates a FieldTraceGroup for the interior boundaries
 */
template <class PhysDim, class TopoDim>
template <class FieldTraceGroupClass>
FieldTraceGroupClass*
Field_CG_CellConstructor<PhysDim, TopoDim>::
createXFieldInteriorTraceGroup( const int iTraceGroup )
{
  // typedef away the details ...
  typedef typename FieldTraceGroupClass::TopologyType Topology;
  typedef typename FieldTraceGroupClass::FieldAssociativityConstructorType FieldTraceConstructorType;
  typedef typename XField<PhysDim, TopoDim>:: template FieldTraceGroupType<Topology> XFieldTraceClass;

  // get the particular interior trace group of interest
  const XFieldTraceClass& xfldTraceGroup = xfld_.template getInteriorTraceGroup<Topology>(iTraceGroup);

  // create the associativities for the boundary trace group
  std::unique_ptr<FieldTraceConstructorType> xfldAssocTrace =
      this->BaseType::template createInteriorTraceGroup<FieldTraceConstructorType>(iTraceGroup);

  // copy the associativities into the mesh!
  this->template copyTraceConnectivity<Topology>(xfldTraceGroup, xfldAssocTrace);

  // return a copy of the trace group
  return new FieldTraceGroupClass(*xfldAssocTrace);
}

//---------------------------------------------------------------------------//
/** createXFieldBoundaryTraceGroup creates a FieldTraceGroup for the boundary
 */
template <class PhysDim, class TopoDim>
template <class FieldTraceGroupClass>
FieldTraceGroupClass*
Field_CG_CellConstructor<PhysDim, TopoDim>::
createXFieldBoundaryTraceGroup( const int bTraceGroup )
{
  // typedef away the details ...
  typedef typename FieldTraceGroupClass::TopologyType Topology;
  typedef typename FieldTraceGroupClass::FieldAssociativityConstructorType FieldTraceConstructorType;
  typedef typename XField<PhysDim, TopoDim>:: template FieldTraceGroupType<Topology> XFieldTraceClass;

  // get the particular boundary tracegroup of interest from this xfield
  const XFieldTraceClass& xfldTraceGroup = xfld_.template getBoundaryTraceGroup<Topology>(bTraceGroup);

  // create the associativities for the boundary tracegroup
  std::unique_ptr<FieldTraceConstructorType> xfldAssocTrace =
      this->BaseType::template createBoundaryTraceGroup<FieldTraceConstructorType>(bTraceGroup);

  // copy the associativities into the new boundary tracegroup constructor!
  this->template copyTraceConnectivity<Topology>(xfldTraceGroup, xfldAssocTrace);

  // return a new trace group
  return new FieldTraceGroupClass(*xfldAssocTrace);
}


//---------------------------------------------------------------------------//
/** createXFieldGhostBoundaryTraceGroup creates a FieldTraceGroup for the ghost boundary
 */
template <class PhysDim, class TopoDim>
template <class FieldTraceGroupClass>
FieldTraceGroupClass*
Field_CG_CellConstructor<PhysDim, TopoDim>::
createXFieldGhostBoundaryTraceGroup( const int bTraceGroup )
{
  // typedef off the details ...
  typedef typename FieldTraceGroupClass::TopologyType Topology;
  typedef typename FieldTraceGroupClass::FieldAssociativityConstructorType FieldTraceConstructorType;
  typedef typename XField<PhysDim, TopoDim>:: template FieldTraceGroupType<Topology> XFieldTraceClass;

  // get the particular ghost boundary trace group of interest
  const XFieldTraceClass& xfldTraceGroup = xfld_.template getGhostBoundaryTraceGroup<Topology>(bTraceGroup);

  // create the associativities for the ghost boundary trace group
  std::unique_ptr<FieldTraceConstructorType> xfldAssocTrace =
      this->BaseType::template createGhostBoundaryTraceGroup<FieldTraceConstructorType>(bTraceGroup);

  // copy the associativities into the mesh!
  this->template copyTraceConnectivity<Topology>(xfldTraceGroup, xfldAssocTrace);

  // return a copy of the trace group
  return new FieldTraceGroupClass(*xfldAssocTrace);
}


//---------------------------------------------------------------------------//
/** copyTraceConnectivity copies the trace connectivity from a mesh to a new field connectivity constructor
 */
template <class PhysDim, class TopoDim>
template <class Topology, class ElementAssociativityConstructor>
void
Field_CG_CellConstructor<PhysDim, TopoDim>::
copyTraceConnectivity( const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Topology>& xfldTraceGroup,
                       std::unique_ptr<ElementConnectivityConstructor<ElementAssociativityConstructor>>& pxfldAssocTrace )
{
  // get the cell groups from the XField on the left and the right
  int groupL = xfldTraceGroup.getGroupLeft();
  int groupR = xfldTraceGroup.getGroupRight();

  // set the cell groups in the new field accordingly!
  pxfldAssocTrace->setGroupLeft(groupL);
  pxfldAssocTrace->setGroupRight(groupR);

  // set the right group type from the mesh field
  pxfldAssocTrace->setGroupRightType(xfldTraceGroup.getGroupRightType());

  int elemL, elemR;
  CanonicalTraceToCell canonicalL, canonicalR;

  // loop over old grid trace elements and set associativity of the new grid trace elements
  for (int elemT = 0; elemT < xfldTraceGroup.nElem(); elemT++)
  {
    // left element connectivity
    elemL      = xfldTraceGroup.getElementLeft( elemT );
    canonicalL = xfldTraceGroup.getCanonicalTraceLeft( elemT );

    // right element connectivity
    elemR      = xfldTraceGroup.getElementRight( elemT );
    canonicalR = xfldTraceGroup.getCanonicalTraceRight( elemT );

    // set connectivity information
    pxfldAssocTrace->setElementLeft(elemL, elemT);
    pxfldAssocTrace->setCanonicalTraceLeft(canonicalL, elemT);

    pxfldAssocTrace->setElementRight(elemR, elemT);
    pxfldAssocTrace->setCanonicalTraceRight(canonicalR, elemT);
  }
}

} // namespace SANS

#endif  // FIELD_CG_CELLCONSTRUCTOR_H
