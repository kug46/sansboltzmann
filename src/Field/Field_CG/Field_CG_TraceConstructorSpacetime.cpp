// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "Field_CG_TraceConstructor.h"
#include "Field_CG_Topology.h"

#include "Topology/ElementTopology.h"

#include "Field/Field.h"
#include "Field/XFieldSpacetime.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

//===========================================================================//
template <class PhysDim>
Field_CG_TraceConstructor<PhysDim, TopoD4>::
Field_CG_TraceConstructor(const XField<PhysDim, TopoD4>& xfld, const int order, const BasisFunctionCategory category,
                          const std::vector<std::vector<int>>& interiorGroupSets,
                          const std::vector<std::vector<int>>& boundaryGroupSets ) :
  xfld_(xfld),
  interiorGroupSets_(interiorGroupSets),
  boundaryGroupSets_(boundaryGroupSets),
  order_(order), category_(category), nElem_(0), nDOF_(0), nDOFpossessed_(0), nDOFghost_(0)
{
  // SANS_DEVELOPER_EXCEPTION("check implementation. -CVF");

  SANS_ASSERT(interiorGroupSets_.size() == boundaryGroupSets_.size());

  // for each cell group set
  for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
  {
    // create a new map for each type of subordinate entity at the end of the vector
    nativeFaceDOFs_.emplace_back( xfld_, order );
    nativeAreaDOFs_.emplace_back( xfld_, order );
    nativeEdgeDOFs_.emplace_back( xfld_, order );
    nativeNodeDOFs_.emplace_back( xfld_ );
  }

  int DOFidx= 0;

  // loop over the interior cellgroup sets
  for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
  {
    // get then loop over the current cellgroup
    const std::vector<int>& interiorGroups = interiorGroupSets[i];
    for (std::size_t igroup = 0; igroup < interiorGroups.size(); igroup++)
    {
      // current cellgroup
      const int group = interiorGroups[igroup];
      // add current cellgroup to the total number of elements
      nElem_ += xfld_.getInteriorTraceGroupBase(group).nElem();

      // if the traces are tets
      if ( xfld_.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Tet) )
      {
        // add all the DOFs
        nativeFaceDOFs_[i].insertFaces( xfld_.template getInteriorTraceGroup<Tet>(group) );
        nativeAreaDOFs_[i].insertAreas( xfld_.template getInteriorTraceGroup<Tet>(group) );
        nativeEdgeDOFs_[i].insertEdges( xfld_.template getInteriorTraceGroup<Tet>(group) );
        nativeNodeDOFs_[i].insertNodes( xfld_.template getInteriorTraceGroup<Tet>(group) );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Unknown element topology." );
    }
  }

  // loop over the boundary cellgroup sets
  for (std::size_t i = 0; i < boundaryGroupSets_.size(); i++)
  {
    // get then loop over the current cellgroup
    const std::vector<int>& boundaryGroups = boundaryGroupSets[i];
    for (std::size_t igroup = 0; igroup < boundaryGroups.size(); igroup++)
    {
      // current cellgroup
      const int group = boundaryGroups[igroup];
      // add current cellgroup elements to the total number of elements
      nElem_ += xfld_.getBoundaryTraceGroupBase(group).nElem();

      // if the boundaries are tets
      if ( xfld_.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Tet) )
      {
        // add all the DOFs
        nativeFaceDOFs_[i].insertFaces( xfld_.template getBoundaryTraceGroup<Tet>(group) );
        nativeAreaDOFs_[i].insertAreas( xfld_.template getBoundaryTraceGroup<Tet>(group) );
        nativeEdgeDOFs_[i].insertEdges( xfld_.template getBoundaryTraceGroup<Tet>(group) );
        nativeNodeDOFs_[i].insertNodes( xfld_.template getBoundaryTraceGroup<Tet>(group) );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Unknown element topology." );
    }
  }

  // construct the native DOF indexing; cells first, then areas, then edges, then nodes
  for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
    nativeFaceDOFs_[i].createNativeIndex(DOFidx);

  for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
    nativeAreaDOFs_[i].createNativeIndex(DOFidx);

  for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
    nativeEdgeDOFs_[i].createNativeIndex(DOFidx);

  for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
    nativeNodeDOFs_[i].createNativeIndex(DOFidx);

  for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
  {
    // Total DOF count in the field on each processor
    nDOF_ += nativeFaceDOFs_[i].nDOF()
        + nativeAreaDOFs_[i].nDOF()
        + nativeEdgeDOFs_[i].nDOF()
        + nativeNodeDOFs_[i].nDOF();
  }

#ifdef SANS_MPI

  // every DOF is possessed for one rank
  if (xfld.comm()->size() > 1)
  {
    // Mark all ghost generating element (remainder are zombies)
    for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
    {
      const std::vector<int>& interiorGroups = interiorGroupSets[i];
      for (std::size_t igroup = 0; igroup < interiorGroups.size(); igroup++)
      {
        const int group = interiorGroups[igroup];

        if ( xfld_.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Tet) )
        {
          this->template
          setTraceGroupDOFPossession<Tet>( xfld_.template getInteriorTraceGroup<Tet>(group),
                                                nativeFaceDOFs_[i],
                                                nativeAreaDOFs_[i],
                                                nativeEdgeDOFs_[i],
                                                nativeNodeDOFs_[i] );
        }
        else
          SANS_DEVELOPER_EXCEPTION( "Unknown element topology." );
      }
    }

    for (std::size_t i = 0; i < boundaryGroupSets.size(); i++)
    {
      const std::vector<int>& boundaryGroups = boundaryGroupSets[i];
      for (std::size_t igroup = 0; igroup < boundaryGroups.size(); igroup++)
      {
        const int group = boundaryGroups[igroup];

        if ( xfld_.getBoundaryTraceGroupBase(group).topoTypeID() == typeid(Tet) )
        {
          this->template
          setTraceGroupDOFPossession<Tet>( xfld_.template getBoundaryTraceGroup<Tet>(group),
                                                nativeFaceDOFs_[i],
                                                nativeAreaDOFs_[i],
                                                nativeEdgeDOFs_[i],
                                                nativeNodeDOFs_[i] );
        }
        else
          SANS_DEVELOPER_EXCEPTION( "Unknown element topology." );
      }
    }

    for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
    {
      // count the number of possessed and ghost DOFs
      GhoulDOF nDOFface = nativeFaceDOFs_[i].nDOFghoul();
      GhoulDOF nDOFarea = nativeAreaDOFs_[i].nDOFghoul();
      GhoulDOF nDOFedge = nativeEdgeDOFs_[i].nDOFghoul();
      GhoulDOF nDOFnode = nativeNodeDOFs_[i].nDOFghoul();

      nDOFpossessed_ += nDOFface.possessed + nDOFarea.possessed + nDOFedge.possessed + nDOFnode.possessed;
      nDOFghost_     += nDOFface.ghost     + nDOFarea.ghost     + nDOFedge.ghost     + nDOFnode.ghost;
    }
  }
  else // comm size
  {
    nDOFpossessed_ = nDOF_;
    nDOFghost_ = 0;
  }


  // construct the local indexing on each processor
  // possessed - [[Cells],
  //              [Faces],
  //              [Areas],
  //              [Edges],
  //              [Nodes],
  // ghost -      [Cells],
  //              [Faces],
  //              [Areas],
  //              [Edges],
  //              [Nodes],
  // zombies -    [Cells],
  //              [Faces],
  //              [Areas],
  //              [Edges],
  //              [Nodes]]

  DOFidx = 0;

  std::array<Ghoul,3> spirits = {{possessed, ghost, zombie}};

  for (int n = 0; n < 3; n++)
  {
    for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
      nativeFaceDOFs_[i].createLocalIndex(spirits[n], DOFidx);

    for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
      nativeAreaDOFs_[i].createLocalIndex(spirits[n], DOFidx);

    for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
      nativeEdgeDOFs_[i].createLocalIndex(spirits[n], DOFidx);

    for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
      nativeNodeDOFs_[i].createLocalIndex(spirits[n], DOFidx);
  }

#else
  // in serial all DOFs are possessed
  nDOFpossessed_ = nDOF_;

  DOFidx = 0;

  for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
    nativeFaceDOFs_[i].createLocalIndex(possessed, DOFidx);

  for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
    nativeAreaDOFs_[i].createLocalIndex(possessed, DOFidx);

  for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
    nativeEdgeDOFs_[i].createLocalIndex(possessed, DOFidx);

  for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
    nativeNodeDOFs_[i].createLocalIndex(possessed, DOFidx);

#endif

}

//----------------------------------------------------------------------------//
template <class PhysDim>
template <class Topology>
void
Field_CG_TraceConstructor<PhysDim, TopoD4>::
setTraceGroupDOFPossession(const typename XField<PhysDim, TopoD4>::template FieldTraceGroupType<Topology>& xfldTraceGroup,
                           Field_CG_FaceDOF& nativeFaceDOFs,
                           Field_CG_AreaDOF& nativeAreaDOFs,
                           Field_CG_EdgeDOF& nativeEdgeDOFs,
                           Field_CG_NodeDOF& nativeNodeDOFs )
{
  SANS_DEVELOPER_EXCEPTION("check implementation. -CVF");
  const int nDOFperEdge = TopologyDOF_CG<Line>::count(order_);
  const int nDOFperFace = TopologyDOF_CG<Topology>::count(order_);

  std::vector<int> nodeMap(Topology::NNode);
  const int (*EdgeNodes)[ Line::NNode ] = ElementEdges<Topology>::EdgeNodes;

  // if any component of a cell element is possessed, then the non-possessed components make ghosts DOFs
  for (int itrace = 0; itrace < xfldTraceGroup.nElem(); itrace++)
  {
    bool isPossessed = false;
    xfldTraceGroup.associativity( itrace ).getNodeGlobalMapping( nodeMap.data(), nodeMap.size() );

    // check the face
    if (nDOFperFace > 0)
    {
      Field_CG_FaceDOF::FaceDOFType& face = nativeFaceDOFs.getNativeFace(Topology::Topology, nodeMap);
      if ( face.spirit == possessed ) isPossessed = true;
    }

    // check all edges
    if (nDOFperEdge > 0)
    {
      for (int iedge = 0; iedge < Topology::NEdge && !isPossessed; iedge++)
      {
        int node0 = nodeMap[EdgeNodes[iedge][0]];
        int node1 = nodeMap[EdgeNodes[iedge][1]];

        Field_CG_EdgeDOF::EdgeDOFType& edge = nativeEdgeDOFs.getNativeEdge(node0, node1);

        //add the canonical edge DOFs if they are not already in the map
        if ( edge.spirit == possessed ) isPossessed = true;
      }
    }

    // check all nodes
    for (int n = 0; n < Topology::NNode && !isPossessed; n++)
      if ( nativeNodeDOFs.getNativeNode(nodeMap[n]).spirit == possessed ) isPossessed = true;

    if (isPossessed)
    {
      // convert zombies to ghosts
      if (nDOFperFace > 0)
      {
        Field_CG_FaceDOF::FaceDOFType& face = nativeFaceDOFs.getNativeFace(Topology::Topology, nodeMap);
        if ( face.spirit == zombie )
          face.spirit = ghost;
      }

      if (nDOFperEdge > 0)
      {
        for (int iedge = 0; iedge < Topology::NEdge; iedge++)
        {
          int node0 = nodeMap[EdgeNodes[iedge][0]];
          int node1 = nodeMap[EdgeNodes[iedge][1]];

          Field_CG_EdgeDOF::EdgeDOFType& edge = nativeEdgeDOFs.getNativeEdge(node0, node1);

          //add the canonical edge DOFs if they are not already in the map
          if ( edge.spirit == zombie )
            edge.spirit = ghost;
        }
      }

      for (int n = 0; n < Topology::NNode; n++)
      {
        Field_CG_NodeDOF::NodeDOFType& node = nativeNodeDOFs.getNativeNode(nodeMap[n]);
        if ( node.spirit == zombie )
          node.spirit = ghost;
      }
    }
  }
}

//----------------------------------------------------------------------------//
template <class PhysDim>
void
Field_CG_TraceConstructor<PhysDim, TopoD4>::
getDOF_rank(int* DOF_rank)
{
  for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
    nativeFaceDOFs_[i].getDOF_rank(nDOFpossessed_, DOF_rank);

  for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
    nativeAreaDOFs_[i].getDOF_rank(nDOFpossessed_, DOF_rank);

  for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
    nativeEdgeDOFs_[i].getDOF_rank(nDOFpossessed_, DOF_rank);

  for (std::size_t i = 0; i < interiorGroupSets_.size(); i++)
    nativeNodeDOFs_[i].getDOF_rank(nDOFpossessed_, DOF_rank);
}


// Explicit instantiations
template struct Field_CG_TraceConstructor<PhysD4, TopoD4>;

} // namespace SANS
