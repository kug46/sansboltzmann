// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELDSPACETIME_H
#define XFIELDSPACETIME_H

#include "XField.h"
#include "XFieldVolume_Traits.h"
#include "XFieldSpacetime_Traits.h"
#include "FieldAssociativity.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Topologically 4D grid
//----------------------------------------------------------------------------//

template<class PhysDim>
struct XFieldTraits<PhysDim, TopoD4>
{
  typedef TopoD4 TopoDim;
  static const int D = PhysDim::D;                 // physical dimensions

  typedef DLA::VectorS<D,Real> VectorX;   // coordinates vector
  typedef VectorX T;

  typedef typename XFieldVolumeTraits<PhysDim, TopoD4>::FieldBase FieldTraceGroupBase;
  typedef typename XFieldSpacetimeTraits<PhysDim, TopoD4>::FieldBase FieldCellGroupBase;

  template<class Topology>
  using FieldTraceGroupType = ElementConnectivity< XFieldGroupVolumeTraits<PhysDim, TopoD4, Topology> >;

  template<class Topology>
  using FieldCellGroupType = FieldAssociativity< XFieldGroupSpacetimeTraits<PhysDim, TopoD4, Topology> >;
};

} // namespace SANS

#endif //XFIELDVOLUME_H
