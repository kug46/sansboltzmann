// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDAREA_CG_CELL_H
#define FIELDAREA_CG_CELL_H

#include <vector>

#include "Topology/ElementTopology.h"
#include "FieldArea.h"
#include "EmbeddedCGType.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// solution field: 2D CG element-field
//
// basis functions set to hierarchical
//----------------------------------------------------------------------------//
template <class PhysDim, class TopoDim, class T>
class Field_CG_Cell;


template <class PhysDim, class T>
class Field_CG_Cell< PhysDim, TopoD2, T > : public Field< PhysDim, TopoD2, T >
{
public:
  typedef Field< PhysDim, TopoD2, T > BaseType;
  typedef T ArrayQ;

  Field_CG_Cell( const XField<PhysDim, TopoD2>& xfld_, const int order, const BasisFunctionCategory& category,
      const embeddedType eType = RegularCGField );

  explicit Field_CG_Cell( const XField<PhysDim, TopoD2>& xfld_, const int order, const BasisFunctionCategory& category,
                 const std::vector<int>& CellGroups,
                 const embeddedType eType = RegularCGField );

  // Groups are first to remove ambiguity with list initializers
  explicit Field_CG_Cell( const std::vector<std::vector<int>>& CellGroups,
                          const XField<PhysDim, TopoD2>& xfld_, const int order, const BasisFunctionCategory& category,
                          const embeddedType eType = RegularCGField );

  Field_CG_Cell( const Field_CG_Cell& fld, const FieldCopy&tag );

  Field_CG_Cell& operator=( const ArrayQ& q );

  // Virtual class to denote the SpaceType of the Field
  virtual SpaceType spaceType() const override { return SpaceType::Continuous; }

  bool needsEmbeddedGhosts() const { return needsEmbeddedGhosts_; }

protected:
  void init( const int order, const BasisFunctionCategory& category,
             const std::vector<std::vector<int>>& CellGroups );

  const bool needsEmbeddedGhosts_;

  using BaseType::nElem_;
  using BaseType::nDOF_;
  using BaseType::DOF_;
  using BaseType::xfld_;
  using BaseType::cellGroups_;
  using BaseType::localCellGroups_;
  using BaseType::interiorTraceGroups_;
  using BaseType::boundaryTraceGroups_;
  using BaseType::localBoundaryTraceGroups_;
};

}

#endif  // FIELDAREA_CG_CELL_H
