// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PANELMETHOD_ALGEBRAICEQUATIONSET_PROJECTTOQAUXI_H_
#define SRC_PANELMETHOD_ALGEBRAICEQUATIONSET_PROJECTTOQAUXI_H_

#include <boost/ptr_container/ptr_vector.hpp>

#include "Discretization/IntegrateInteriorTraceGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

#include "Discretization/Galerkin/AlgebraicEquationSet_Project.h"

#include "Discretization/Galerkin/IntegrandCell_ProjectFunction.h"
#include "Discretization/Galerkin/IntegrandInteriorTrace_Galerkin_manifold.h"

#include "Discretization/Galerkin/ResidualInteriorTrace_Galerkin.h"
#include "Discretization/Galerkin/ResidualBoundaryTrace_Galerkin.h"

#include "Discretization/Galerkin/JacobianInteriorTrace_Galerkin_Param.h"
#include "Discretization/Galerkin/JacobianBoundaryTrace_Galerkin_Param.h"

#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/NDConvert/FunctionNDConvertSpace2D.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"


#include "IntegrandBoundaryTrace_ProjectToQauxi.h"
#include "ProjectionToQauxi_IBL.h"
#include "XfoilPanel.h"
#include "XfoilPanelEquationSet.h"

namespace SANS
{

// AlgebraicEquationSet_ProjectToQauxi specific details
namespace AESProjToQauxi_detail
{
typedef PhysD2 PhysDim;
typedef SolnNDConvertSpace<PhysDim, ScalarFunction2D_Const> NullForcingAuxiND;
typedef FunctionNDConvertSpace<PhysDim, Real> FunctionAuxiND;
typedef IntegrandCell_ProjectFunction<FunctionAuxiND,IntegrandCell_ProjFcn_detail::FcnX> IntegrandCellProjToQauxiClass;

const NullForcingAuxiND forcingFcnProjNullToAuxi(0.0);
const FunctionAuxiND forcingNDFcnProjNullToAuxi(forcingFcnProjNullToAuxi);

// Integrand cell class of projection from zero
class IntegrandCellProjNull : public IntegrandCellProjToQauxiClass
{
public:
  explicit IntegrandCellProjNull(const std::vector<int>& cellGroup) :
    IntegrandCellProjToQauxiClass(forcingNDFcnProjNullToAuxi, cellGroup) {}
};

} // namespace AESProjToQauxi_detail

//----------------------------------------------------------------------------//
// Algebraic equation set specialized for auxiliary inviscid equations
// i.e. L^2 projection onto auxiliary inviscid variables of the panel method in coupling with IBL2D
// \int phi (q - f) = 0, where phi is weighting function (basis), q is auxiliary inviscid variable, and f(Qauxv,QIBL) is forcing function
//
template<class ParamFieldType, class IntegrandCellClass, class TopoDim, class Traits,
         class AESIBLClass, class AESAUXVClass>
class AlgebraicEquationSet_ProjectToQauxi :
    public AlgebraicEquationSet_Project<ParamFieldType,IntegrandCellClass,TopoDim,Traits>
{
public:
  typedef AlgebraicEquationSet_Project<ParamFieldType,IntegrandCellClass,TopoDim,Traits> BaseClassType;

  typedef Traits TraitsTag;

  using typename BaseClassType::PhysDim;
  using typename BaseClassType::ArrayQ;
  using typename BaseClassType::SystemMatrix;
  using typename BaseClassType::SystemVector;
  using typename BaseClassType::SystemVectorView;

  typedef XfoilPanelEquationSet<XfoilPanel,Traits> PanelEquationType; // hard coded
  typedef PDENDConvertSpace<PhysDim, ProjectionToQauxi<PanelEquationType, typename AESIBLClass::NDPDEClass> > NDProjectionToQauxiType;

  static const int N = BaseClassType::N;
  static const int NQIBL = NDProjectionToQauxiType::NQIBL;
  static const int NQauxv = NDProjectionToQauxiType::NQauxv;

  static const int iParamQIBL = NDProjectionToQauxiType::iParamQIBL;
  static const int iParamQauxv = NDProjectionToQauxiType::iParamQauxv;

  using MatrixParamQIBL = DLA::MatrixS<N,NQIBL,Real>;
  using MatrixParamQauxv = DLA::MatrixS<N,NQauxv,Real>;

  typedef SurrealS<NQIBL> SurrealTypeQIBL;
  typedef SurrealS<NQauxv> SurrealTypeQauxv;

  using BaseClassType::iProj;
  using BaseClassType::iq;

  typedef IntegrandInteriorTrace_Galerkin_manifold<NDProjectionToQauxiType> IntegrandInteriorTraceType;
  typedef IntegrandBoundaryTrace_ProjectToQauxi<NDProjectionToQauxiType> IntegrandBoundaryTraceType;


  AlgebraicEquationSet_ProjectToQauxi(const ParamFieldType& paramfld,
                                      Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                      const IntegrandCellClass& fcnCell, // not used in this class; is here only to conform to BaseClass ctor
                                      const QuadratureOrder& quadratureOrder,
                                      const std::array<Real,1> tol,
                                      const std::vector<std::shared_ptr<IntegrandInteriorTraceType> >& integrandInteriorTraceArray,
                                      const std::vector<std::shared_ptr<IntegrandBoundaryTraceType> >& integrandBoundaryTraceArray) :
    BaseClassType(paramfld,qfld,fcnCell,quadratureOrder,tol),
    integrandInteriorTraceArray_(integrandInteriorTraceArray),
    integrandBoundaryTraceArray_(integrandBoundaryTraceArray),
    nintegrand_(integrandInteriorTraceArray_.size())
  {
    static_assert( std::is_same<IntegrandCellClass, AESProjToQauxi_detail::IntegrandCellProjNull>::value,
                   "The default integrand cell class has to be projection from zero!" );

    SANS_ASSERT( integrandInteriorTraceArray_.size() == integrandBoundaryTraceArray_.size() );

    for (int group = 0; group < qfld.nCellGroups(); ++group)
      SANS_ASSERT_MSG(PanelEquationType::order_lam == (qfld.template getCellGroup<Line>(group)).order(),
                      "qfld (Qauxi) should have order order_lam");
  }


  void residual(SystemVectorView& rsd) override;

  // compute jacobian of the auxiliary inviscid equation w.r.t. its parameters
  // i.e. auxiliary viscous variables Qauxv, IBL variables QIBL
  template<class SparseMatrixType>
  void jacobianParamQIBL(SparseMatrixType& jac) const { jacobianParam<SparseMatrixType,SurrealTypeQIBL,iParamQIBL>(jac,AESIBLClass::iq); }

  template<class SparseMatrixType>
  void jacobianParamQauxv(SparseMatrixType& jac) const { jacobianParam<SparseMatrixType,SurrealTypeQauxv,iParamQauxv>(jac,AESAUXVClass::iq); }

protected:
  // compute jacobian w.r.t. parameters
  template<class SparseMatrixType, class SurrealParamType, int iParam>
  void jacobianParam(SparseMatrixType& jac, const int ip) const;

  using BaseClassType::fcnCell_;
  using BaseClassType::paramfld_;
  using BaseClassType::qfld_; // auxiliary inviscid variable Qauxi field
  using BaseClassType::quadratureOrder_;

  const std::vector<std::shared_ptr<IntegrandInteriorTraceType> >& integrandInteriorTraceArray_;
  const std::vector<std::shared_ptr<IntegrandBoundaryTraceType> >& integrandBoundaryTraceArray_;
  const size_t nintegrand_; // number of different trace integrand classes
};

template<class ParamFieldType, class IntegrandCellClass, class TopoDim, class Traits, class AESIBLClass, class AESAUXVClass>
void
AlgebraicEquationSet_ProjectToQauxi<ParamFieldType, IntegrandCellClass, TopoDim, Traits, AESIBLClass, AESAUXVClass>::
residual(SystemVectorView& rsd)
{
  // Cell integrals: \int phi ( q )
  IntegrateCellGroups<TopoDim>::integrate( ResidualCell_Galerkin(fcnCell_, rsd(iProj)),
                                           paramfld_, qfld_,
                                           quadratureOrder_.cellOrders.data(),
                                           quadratureOrder_.cellOrders.size() );

  // Note that cell integrals for advective fluxes are dropped since it is assumed that the weighting function is assumed to be p=0
  // Trace integrals: \int phi ( -f )
  for (std::size_t j = 0; j < nintegrand_; ++j)
  {
    // Interior trace integrals
    IntegrateInteriorTraceGroups<TopoDim>::integrate(
        ResidualInteriorTrace_Galerkin(
            *integrandInteriorTraceArray_[j], rsd(iProj)),
            paramfld_, qfld_,
            quadratureOrder_.interiorTraceOrders.data(),
            quadratureOrder_.interiorTraceOrders.size() );

    // Boundary trace integrals
    IntegrateBoundaryTraceGroups<TopoDim>::integrate(
        ResidualBoundaryTrace_Galerkin(
            *integrandBoundaryTraceArray_[j], rsd(iProj)),
            paramfld_, qfld_,
            quadratureOrder_.boundaryTraceOrders.data(),
            quadratureOrder_.boundaryTraceOrders.size() );
  }
}

template<class ParamFieldType, class IntegrandCellClass, class TopoDim, class Traits, class AESIBLClass, class AESAUXVClass>
template<class SparseMatrixType, class SurrealParamType, int iParam>
void
AlgebraicEquationSet_ProjectToQauxi<ParamFieldType, IntegrandCellClass, TopoDim, Traits, AESIBLClass, AESAUXVClass>::
jacobianParam(SparseMatrixType& jac, const int ip) const
{
  typedef typename std::result_of<SparseMatrixType(const int, const int)>::type Matrix;
  Matrix jacProj_p = jac(iProj,ip);

  // Only trace integrals depend on parameters: \int phi ( -f(Qauxv,QIBL) )
  for (std::size_t j = 0; j < nintegrand_; ++j)
  {
    // Interior trace integrals
    IntegrateInteriorTraceGroups<TopoDim>::integrate(
        JacobianInteriorTrace_Galerkin_Param<SurrealParamType,iParam>(
            *integrandInteriorTraceArray_[j], jacProj_p ),
            paramfld_, qfld_,
            quadratureOrder_.interiorTraceOrders.data(),
            quadratureOrder_.interiorTraceOrders.size() );

    // Boundary trace integrals
    IntegrateBoundaryTraceGroups<TopoDim>::integrate(
        JacobianBoundaryTrace_Galerkin_Param<SurrealParamType,iParam>(
            *integrandBoundaryTraceArray_[j], jacProj_p ),
            paramfld_, qfld_,
            quadratureOrder_.boundaryTraceOrders.data(),
            quadratureOrder_.boundaryTraceOrders.size() );
  }
}

} // namespace SANS

#endif /* SRC_PANELMETHOD_ALGEBRAICEQUATIONSET_PROJECTTOQAUXI_H_ */
