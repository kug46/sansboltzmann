// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PANELMETHOD_FUNCTIONEVALQAUXV_IBL_H_
#define SRC_PANELMETHOD_FUNCTIONEVALQAUXV_IBL_H_

#include <vector>

#include "Field/Element/ElementXFieldLine.h"

#include "pde/IBL/QauxvInterpret2D.h"

#include "Surreal/PromoteSurreal.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Class for evaluating forcing function for projection onto auxiliary viscous variables
// in coupling with the panel method with IBL equations.
//
template<class PanelMethodType, class Tgam, class Tlam>
class FunctionEvalQauxv
{
public:
  typedef Line TopologyType; // assume line grid
  typedef typename promote_Surreal<Tgam,Tlam>::type Tauxv;

  typedef typename PanelMethodType::PhysDim PhysDim;
  typedef typename PanelMethodType::TopoDim TopoDim;

  template <class T>
  using GamFieldType = typename PanelMethodType::template GamFieldType<T>;

  template <class T>
  using LamFieldType = typename PanelMethodType::template LamFieldType<T>;

  static const int NQauxv = IBLParamIndexer<PhysDim>::Nparam;

  template <class T>
  using ArrayQauxv = DLA::VectorS<NQauxv,T>;

  typedef ArrayQauxv<Real> ArrayQ;

  using MatrixQ = DLA::MatrixS<NQauxv,NQauxv,Real>;

  template<class T>
  using VectorX = DLA::VectorS<PhysDim::D,T>;

  using ElementXFieldType = ElementXField<PhysDim,TopoDim,TopologyType>;

  template <class T>
  using ElementVelFieldType = Element<VectorX<T>, TopoDim, TopologyType>;

  // Here, it is assumed Qauxv is continuously piecewise linear, in order to consistent with the velocity field representation of the panel method
  static const int order_qauxv = 1;


  FunctionEvalQauxv(const PanelMethodType& panel,
                    const ElementXFieldType& xfldElem,
                    const GamFieldType<Tgam>& gamfld,
                    const LamFieldType<Tlam>& lamfld,
                    const int cellGroupGlobal, const int elem) :
    sRefList_({0., 1.}),
    panel_(panel), xfldElem_(xfldElem),
    gamfld_(gamfld), lamfld_(lamfld),
    cellGroupGlobal_(cellGroupGlobal), elem_(elem),
    qfldElem_(order_qauxv, BasisFunctionCategory_Hierarchical),
    varInterpret_()
  {
    std::vector<int> nodeList;
    if (cellGroupGlobal == PanelMethodType::domainName::airfoil)
    {
      nodeList = {elem_, elem_+1};
    }
    else if (cellGroupGlobal == PanelMethodType::domainName::wake)
    {
      const int nnode_a = panel_.nNodeAirfoil();
      nodeList = {nnode_a + elem_, nnode_a + elem_+1};
    }
    else
      SANS_DEVELOPER_EXCEPTION("Invalid cell group index");

    // compute velocity field DOFs
    SANS_ASSERT(qfldElem_.nDOF() == (int)nodeList.size());
    SANS_ASSERT(qfldElem_.basis() == xfldElem_.basis());

    for (int i = 0; i < qfldElem_.nDOF(); ++i)
    {
      Tauxv Ue = 0; // edge velocity as defined in panel method
      const VectorX<Real>& X = gamfld.getXField().DOF(nodeList[i]);

      panel_.Uecalc(gamfld_, lamfld_, X, nodeList[i], Ue);

      VectorX<Real> t = 0;
      xfldElem_.unitTangent(sRefList_[i], t);

      qfldElem_.DOF(i) = Ue * t;
    }
  }

  //Evaluate Qauxv at element reference coordinate sRef
  ArrayQauxv<Tauxv> operator()(const QuadraturePoint<TopoDim>& sRef) const
  {
    VectorX<Tauxv> q = 0.; // velocity
    qfldElem_.eval(sRef, q);

    VectorX< VectorX<Tauxv> > gradq = 0.0; // velocity gradient
    xfldElem_.evalGradient(sRef, qfldElem_, gradq);

    const Tauxv p0 = panel_.getp0();
    const Tauxv T0 = panel_.getT0();

    return varInterpret_.setDOFFrom(q, gradq, p0, T0);
  }

  const std::vector<Real> sRefList_; // reference element coordinates

protected:
  const PanelMethodType& panel_;
  const ElementXFieldType& xfldElem_;
  const GamFieldType<Tgam>& gamfld_; // vortex strength (gamma) field
  const LamFieldType<Tlam>& lamfld_; // source strength (sigma) field
  const int cellGroupGlobal_;
  const int elem_;
  ElementVelFieldType<Tauxv> qfldElem_;
  const typename QauxvInterpret<PhysDim>::type varInterpret_;
};

} // namespace SANS

#endif /* SRC_PANELMETHOD_FUNCTIONEVALQAUXV_IBL_H_ */
