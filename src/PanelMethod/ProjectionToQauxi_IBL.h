// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PANELMETHOD_PROJECTIONTOQAUXI_IBL_H_
#define SRC_PANELMETHOD_PROJECTIONTOQAUXI_IBL_H_

#define IS_NOUPWIND_PROJECTIONTOQAUXI 0
#define IS_AVGFLUX_PROJECTIONTOQAUXI 0

#include "pde/NDConvert/PDENDConvertSpace2D.h"

namespace SANS
{
template<class InviscidEqnType, class PDEIBLClass>
class ProjectionToQauxi;

//----------------------------------------------------------------------------//
// The auxiliary inviscid equation is governed by q - div . F = 0, where q refers to the auxiliary inviscid variable Qauxi, and
// F is a flux (e.g. mass defect flux) computed based on the IBL solution and auxiliary visous variables Qauxv.
// Hence, the discrete PDE is essentially project onto q.
//
// The following class accounts for the ( - div . F ) term of the auxiliary inviscid equation.
//
template<class InviscidEqnType, class PDEIBLClass>
class ProjectionToQauxi
{
public:
  typedef typename PDEIBLClass::PhysDim PhysDim;
  static_assert(PhysDim::D == 2, "currently assumes 2D space"); //TODO: to be extended

  static const int N = InviscidEqnType::Nauxi;
  static const int NQIBL = PDEIBLClass::N;
  static const int NQauxv = PDEIBLClass::Nparam;

  static_assert(N==1, "The size of ArrayQ and MatrixQ assumes N=1");

  template <class T>
  using ArrayQ = T; // solution/residual array type: Qauxi

  template <class T>
  using VectorArrayQ = DLA::VectorS<PhysDim::D, ArrayQ<T> >; // solution/residual array type

  template <class T>
  using MatrixQ = T;

  template <class T>
  using ArrayQIBL = typename PDEIBLClass::template ArrayQ<T>; // solution/residual array type

  template <class T>
  using ArrayQauxv = typename PDEIBLClass::template ArrayParam<T>; // solution/residual array type

  template <class T>
  using VectorX = DLA::template VectorS<PhysDim::D,T>;

  // TODO: need to ensure the following indexing is consistent with the parameter fields that are constructed
  static const int iParamQIBL = 0; // tuple indexing
  static const int iParamQauxv = 1;
  template<class Tibl, class Tauxv>
  using ParamType = ParamTuple<ArrayQIBL<Tibl>, ArrayQauxv<Tauxv>, TupleClass<0> >; // tuple < QIBL, Qauxv >

  typedef typename PDEIBLClass::ProfileCategory ProfileCategory;

  ProjectionToQauxi(const PDEIBLClass& pdeIBL) :
    pdeIBL_(pdeIBL),
    varInterpret_(pdeIBL_.getVarInterpreter() ),
    paramInterpret_(pdeIBL_.getParamInterpreter() ),
    gasModel_(pdeIBL_.getGasModel() ) {}

  // flux components
  bool hasFluxAdvectiveTime() const { return false; }
  bool hasFluxAdvective() const { return true; }
  bool hasFluxViscous() const { return false; }
  bool hasSource() const { return false; }
  bool hasSourceTrace() const { return false; }
  bool hasForcingFunction() const { return false; }

  bool needsSolutionGradientforSource() const { return false; }

  template<class Tibl, class Tauxv>
  void fluxAdvectiveBoundary(
      const ParamType<Tibl,Tauxv>& params, const VectorX<Real>& e1,
      const Real& x, const Real& z, const Real& time,
      const ArrayQ<Real>& var, // Qauxi assumed to be not surrealized
      ArrayQ<typename promote_Surreal<Tibl,Tauxv>::type>& fx,
      ArrayQ<typename promote_Surreal<Tibl,Tauxv>::type>& fz ) const
  {
    typedef typename promote_Surreal<Tibl,Tauxv>::type Tout;

    const ArrayQIBL<Tibl> QIBL = get<iParamQIBL>(params);
    const ArrayQauxv<Tauxv> Qauxv = get<iParamQauxv>(params);

    const auto gamma = pdeIBL_.calcIntermittency(Qauxv,e1,x,z,QIBL);

    const ProfileCategory profileCat = pdeIBL_.getProfileCategory(QIBL,x);

    if (0.0 < gamma &&  gamma < 1.0) // mixture of laminar and turbulent
    {
      ArrayQ<Tout> fxLami = 0.0, fxTurb = 0.0;
      ArrayQ<Tout> fzLami = 0.0, fzTurb = 0.0;

      if (profileCat == ProfileCategory::LaminarBL ||
          profileCat == ProfileCategory::TurbulentBL)
      {
        fluxAdvectiveBoundary(ProfileCategory::LaminarBL, QIBL, Qauxv, e1, x, z, time, var, fxLami, fzLami);
        fluxAdvectiveBoundary(ProfileCategory::TurbulentBL, QIBL, Qauxv, e1, x, z, time, var, fxTurb, fzTurb);
      }
      else
        SANS_DEVELOPER_EXCEPTION("Blending should be used for BL, not wake");

      fx += (1.0-gamma)*fxLami + gamma*fxTurb;
      fz += (1.0-gamma)*fzLami + gamma*fzTurb;
    }
    else // either laminar or turbulent
      fluxAdvectiveBoundary(profileCat, QIBL, Qauxv, e1, x, z, time, var, fx, fz);
  }

  template<class Tibl, class Tauxv>
  void fluxAdvectiveUpwind(
      const ParamType<Tibl,Tauxv>& paramsL, const ParamType<Tibl,Tauxv>& paramsR,
      const VectorX<Real>& e1L, const VectorX<Real>& e1R,
      const Real& x, const Real& z, const Real& time,
      const ArrayQ<Real>& varL, const ArrayQ<Real>& varR,
      const Real& nxL, const Real& nzL, const Real& nxR, const Real& nzR,
      ArrayQ<typename promote_Surreal<Tibl,Tauxv>::type>& fL,
      ArrayQ<typename promote_Surreal<Tibl,Tauxv>::type>& fR ) const
  {
    typedef typename promote_Surreal<Tibl,Tauxv>::type Tout;

    ArrayQ<Tout> fxL = 0.0, fzL = 0.0;
    fluxAdvectiveBoundary(paramsL, e1L, x, z, time, varL, fxL, fzL);

    ArrayQ<Tout> fxR = 0.0, fzR = 0.0;
    fluxAdvectiveBoundary(paramsR, e1R, x, z, time, varR, fxR, fzR);

    // assemble final numerical flux
#if IS_NOUPWIND_PROJECTIONTOQAUXI // no upwinding at all
    fL += fxL*nxL + fzL*nzL;
    fR += fxR*nxR + fzR*nzR;
#elif IS_AVGFLUX_PROJECTIONTOQAUXI // average flux
    const ArrayQ<Tout> f_unique = 0.5*(fxL*nxL + fzL*nzL + fxR*nxR + fzR*nzR); // averaging

    fL += f_unique;
    fR += f_unique;

#else // fully upwinded flux
    const VectorX<Real> nrmL = {nxL, nzL}; // trace normal vector
    const VectorX<Tauxv> q1L = paramInterpret_.getq1(get<iParamQauxv>(paramsL));
    const Tauxv qnL = dot(q1L, nrmL);

    const ArrayQ<Tout> f_unique = (qnL > 0) ? fxL*nxL + fzL*nzL : fxR*nxR + fzR*nzR;

    fL += f_unique;
    fR += f_unique;
#endif

  }

#if 1 // dummy functions requested from PDENDConvert
  template <class Tp, class T>
  void forcingFunction( const ArrayQauxv<Tp>& params, const Real& x, const Real& y, const Real& time, ArrayQ<T>& forcing ) const {}

  bool isValidState( const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out=std::cout ) const { std::cout << "ProjectionToQauxi: Dummy dump" << std::endl; }

  template<class T>
  void setDOFFrom( ArrayQ<T>& var ) const {}
#endif

protected:
  const PDEIBLClass& pdeIBL_;
  const typename PDEIBLClass::VarInterpType& varInterpret_;
  const typename PDEIBLClass::ParamInterpType& paramInterpret_;
  const typename PDEIBLClass::GasModelType& gasModel_;

private:
  template<class Tibl, class Tauxv>
  void fluxAdvectiveBoundary(
      const typename PDEIBLClass::ProfileCategory profileCat,
      const ArrayQIBL<Tibl>& QIBL, const ArrayQauxv<Tauxv>& Qauxv,
      const VectorX<Real>& e1, const Real& x, const Real& z, const Real& time,
      const ArrayQ<Real>& var, // Qauxi assumed to be not surrealized
      ArrayQ<typename promote_Surreal<Tibl,Tauxv>::type>& fx,
      ArrayQ<typename promote_Surreal<Tibl,Tauxv>::type>& fz ) const
  {
    typedef typename promote_Surreal<Tibl,Tauxv>::type Tout;

    const VectorX<Tauxv> q1 = paramInterpret_.getq1(Qauxv);
    const Tauxv qe = paramInterpret_.getqe(Qauxv);
    Tauxv rhoe = gasModel_.density(qe, paramInterpret_.getp0(Qauxv), paramInterpret_.getT0(Qauxv));
    const Tauxv nue = (pdeIBL_.getViscosityModel()).dynamicViscosity() / rhoe;

    const auto& thicknessesCoefficientsFunctor
      = (pdeIBL_.getSecondaryVarObj()).getCalculatorThicknessesCoefficients(profileCat, Qauxv, QIBL, nue);

    const Tout delta1s = thicknessesCoefficientsFunctor.getdelta1s();

    fx += -delta1s*q1[0];
    fz += -delta1s*q1[1];
  }

  template<class Tibl, class Tauxv>
  void masterState(const ProfileCategory profileCat,
                   const ArrayQIBL<Tibl>& QIBL, const ArrayQauxv<Tauxv>& Qauxv, const VectorX<Real>& e1,
                   const Real& x, const Real& z, const ArrayQ<Real>& var,
                   ArrayQ<typename promote_Surreal<Tibl,Tauxv>::type>& uCons) const
  {
    const Tauxv qe = paramInterpret_.getqe(Qauxv);
    const Tauxv rhoe = gasModel_.density(qe, paramInterpret_.getp0(Qauxv), paramInterpret_.getT0(Qauxv));
    const Tauxv nue = (pdeIBL_.getViscosityModel()).dynamicViscosity() / rhoe;

    const auto& thicknessesCoefficientsFunctor
      = (pdeIBL_.getSecondaryVarObj()).getCalculatorThicknessesCoefficients(profileCat, Qauxv, QIBL, nue);

    uCons = -thicknessesCoefficientsFunctor.getdelta1s();
  }
};

} // namespace SANS

#endif /* SRC_PANELMETHOD_PROJECTIONTOQAUXI_IBL_H_ */
