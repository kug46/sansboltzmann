// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "XfoilPanelEquationSet.h"
#include "XfoilPanel.h"

#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"

namespace SANS
{
template class XfoilPanelEquationSet<XfoilPanel,AlgEqSetTraits_Sparse>;
}
