// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PANELMETHOD_JACOBIANPARAM_PANELEQN_QAUXI_H_
#define SRC_PANELMETHOD_JACOBIANPARAM_PANELEQN_QAUXI_H_

#include "Discretization/JacobianParamBase.h"
#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// A class for constructing jacobian of the panel method equations w.r.t parameters (i.e. auxiliary inviscid variables Qauxi)
// This is used for coupling with IBL equations.
//
template<class PanelEqnClass>
class JacobianParam_PanelEqn_Qauxi
  : public JacobianParamBase<
      typename AlgebraicEquationSetTraits<typename PanelEqnClass::template MatrixParam<Real>,
                                          typename PanelEqnClass::template ArrayQ<Real>,
                                          typename PanelEqnClass::TraitsTag
                                         >::SystemMatrix
                            >
{
public:
  typedef typename PanelEqnClass::PhysDim PhysDim;
  typedef typename PanelEqnClass::template ArrayQ<Real> ArrayQ;
  typedef typename PanelEqnClass::template MatrixParam<Real> MatrixParam;

  typedef typename PanelEqnClass::TraitsTag TraitsTag;
  typedef AlgebraicEquationSetTraits<MatrixParam, ArrayQ, TraitsTag> TraitsType;

  typedef typename TraitsType::MatrixSizeClass MatrixSizeClass;

  typedef typename TraitsType::SystemMatrix SystemMatrix;
  typedef typename TraitsType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename TraitsType::SystemMatrixView SystemMatrixView;
  typedef typename TraitsType::SystemNonZeroPatternView SystemNonZeroPatternView;

  typedef JacobianParamBase<SystemMatrix> BaseType;

  typedef typename BaseType::SystemMatrixTranspose SystemMatrixTranspose;
  typedef typename BaseType::SystemNonZeroPatternTranspose SystemNonZeroPatternTranspose;

  explicit JacobianParam_PanelEqn_Qauxi(const PanelEqnClass& PanelEqn) :
    PanelEqn_(PanelEqn) {}

  virtual ~JacobianParam_PanelEqn_Qauxi() {}

  //Fills jacobian or the non-zero pattern of a jacobian
  virtual void jacobian(SystemMatrixView& mtx       ) override { jacobianPanelEqn_Qauxi<SystemMatrixView&>(mtx); }
  virtual void jacobian(SystemNonZeroPatternView& nz) override { jacobianPanelEqn_Qauxi<SystemNonZeroPatternView&>(nz); }

  //Fills jacobian transpose or the non-zero pattern transpose of a jacobian transpose
  virtual void jacobian(SystemMatrixTranspose mtxT       ) override { jacobianPanelEqn_Qauxi(mtxT); }
  virtual void jacobian(SystemNonZeroPatternTranspose nzT) override { jacobianPanelEqn_Qauxi(nzT); }

protected:
  template<class SparseMatrixType>
  void jacobianPanelEqn_Qauxi(SparseMatrixType jac) const { PanelEqn_.template jacobianParam<SparseMatrixType&>(jac); }

  const PanelEqnClass& PanelEqn_;
};

} // namespace SANS

#endif /* SRC_PANELMETHOD_JACOBIANPARAM_PANELEQN_QAUXI_H_ */
