// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PANELMETHOD_INTEGRANDBOUNDARYTRACE_PROJECTTOQAUXI_H_
#define SRC_PANELMETHOD_INTEGRANDBOUNDARYTRACE_PROJECTTOQAUXI_H_

#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/Galerkin/Integrand_Galerkin_fwd.h"

#include "Field/Element/TraceUnitNormal.h"

namespace SANS
{
//----------------------------------------------------------------------------//
template<class NDProjectionClass>
class IntegrandBoundaryTrace_ProjectToQauxi:
  public IntegrandBoundaryTraceType<IntegrandBoundaryTrace_ProjectToQauxi<NDProjectionClass> >
{
public:
#if 1 // dummy types required for using generic BC machinery
  typedef BCCategory::None Category;
  typedef Galerkin_manifold DiscTag;
#endif

  typedef typename NDProjectionClass::PhysDim PhysDim;

  template <class T>
  using ArrayQ = typename NDProjectionClass::template ArrayQ<T>;

  template <class T>
  using VectorArrayQ = typename NDProjectionClass::template VectorArrayQ<T>; // solution gradient arrays

  template <class T>
  using MatrixQ = typename NDProjectionClass::template MatrixQ<T>;           // matrices

  static const int N = NDProjectionClass::N;

  IntegrandBoundaryTrace_ProjectToQauxi(const NDProjectionClass& projectorToQauxi,
                                        const std::vector<int>& BoundaryGroups) :
    projectorToQauxi_(projectorToQauxi),
    BoundaryGroups_(BoundaryGroups) {}

  virtual ~IntegrandBoundaryTrace_ProjectToQauxi() {}

  std::size_t nBoundaryGroups() const { return BoundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return BoundaryGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return BoundaryGroups_; }


  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class Topology, class ElementParam>
  class BasisWeighted
  {
  public:
    typedef Element<ArrayQ<T>, TopoDimCell, Topology> ElementQFieldL;

    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology     > ElementXFieldL;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    BasisWeighted(const NDProjectionClass& projectorToQauxi,
                  const ElementXFieldTrace& xfldElemTrace,
                  const CanonicalTraceToCell& canonicalTrace,
                  const ElementParam&  paramfldElem,
                  const ElementQFieldL& qfldElem) :
      projectorToQauxi_(projectorToQauxi),
      xfldElemTrace_(xfldElemTrace),
      canonicalTrace_(canonicalTrace),
      xfldElem_(get<-1>(paramfldElem)),  // XField must be the last parameter in tuple
      qfldElem_(qfldElem),
      paramfldElem_(paramfldElem),
      nDOF_(qfldElem.nDOF()),
      phi_( new Real[nDOF_]),
      gradphi_( new VectorX[nDOF_]) {}

    ~BasisWeighted()
    {
      delete [] phi_;
      delete [] gradphi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // boundary element trace integrand
    template<class Ti>
    void operator()(const QuadPointTraceType& RefTrace, ArrayQ<Ti> integrand[], int neqn ) const;

  protected:
    const NDProjectionClass& projectorToQauxi_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldL& xfldElem_;
    const ElementQFieldL& qfldElem_;
    const ElementParam&  paramfldElem_;

    const int nDOF_;
    mutable Real *phi_;
    mutable VectorX *gradphi_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class Topology, class ElementParam>
  BasisWeighted<T, TopoDimTrace, TopologyTrace,
                   TopoDimCell,  Topology, ElementParam>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const CanonicalTraceToCell&                                canonicalTrace,
            const ElementParam&                                        paramfldElem,
            const Element<ArrayQ<T>    , TopoDimCell , Topology     >& qfldElem) const
  {
    return BasisWeighted<T, TopoDimTrace, TopologyTrace,
                            TopoDimCell,  Topology, ElementParam>(
            projectorToQauxi_,
            xfldElemTrace, canonicalTrace,
            paramfldElem, qfldElem);
  }

protected:
  const NDProjectionClass& projectorToQauxi_;
  const std::vector<int>& BoundaryGroups_;
};


template <class NDProjectionClass>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class Topology, class ElementParam>
template<class Ti>
void
IntegrandBoundaryTrace_ProjectToQauxi<NDProjectionClass>::
BasisWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  Topology, ElementParam>::
operator()(const QuadPointTraceType& sRefTrace, ArrayQ<Ti> integrand[], int neqn) const
{
  SANS_ASSERT(neqn == nDOF_);

  typedef DLA::VectorS<TopoDimCell::D, VectorX> LocalAxes;  // manifold local axes type

  ParamT param;             // Elemental parameters (such as grid coordinates and distance functions)

  VectorX N;                // unit normal (points out of domain)
  VectorX e01L;             // basis direction vector
  LocalAxes e0L;            // basis direction vector

  ArrayQ<T> q;              // solution
  VectorArrayQ<T> gradq;    // solution gradient

  QuadPointCellType sRefL;  // reference coordinates (s,t) wrt left element

  const bool needsSolutionGradient = projectorToQauxi_.hasFluxViscous();

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval(canonicalTrace_, sRefTrace, sRefL);

  static_assert(TopoDimCell::D == 1, "Only TopoD1 element is implemented so far."); // TODO: generalize

  // Elemental parameters (includes X)
  paramfldElem_.eval(sRefL, param);

  // physical coordinates
  xfldElem_.unitTangent(sRefL, e01L);
  e0L[0] = e01L;

  // unit normal: points out of domain
  traceUnitNormal(xfldElem_, sRefL, xfldElemTrace_, sRefTrace, N);

  // basis value, gradient
  qfldElem_.evalBasis(sRefL, phi_, nDOF_);
  xfldElem_.evalBasisGradient(sRefL, qfldElem_, gradphi_, nDOF_);

  // solution value, gradient, lifting operators, viscous eta parameter
  qfldElem_.evalFromBasis(phi_, nDOF_, q);
  if (needsSolutionGradient)
    qfldElem_.evalFromBasis(gradphi_, nDOF_, gradq);
  else
    gradq = 0;

  // PDE residual: integrand of weak form boundary integral
  for (int k = 0; k < neqn; k++)
    integrand[k] = 0;

  VectorArrayQ<Ti> F = 0;     // PDE flux

  // advective flux
  if ( projectorToQauxi_.hasFluxAdvective() )
    projectorToQauxi_.fluxAdvectiveBoundary(param, e0L, q, F);

  ArrayQ<Ti> Fn = dot(N,F);
  for (int k = 0; k < neqn; k++)
    integrand[k] += phi_[k]*Fn;
}

} // namespace SANS

#endif /* SRC_PANELMETHOD_INTEGRANDBOUNDARYTRACE_PROJECTTOQAUXI_H_ */
