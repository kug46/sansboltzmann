
INCLUDE( ${CMAKE_SOURCE_DIR}/CMakeInclude/ForceOutOfSource.cmake ) #This must be the first thing included

SET( PANELMETHOD_SRC
  XfoilPanelEquationSet.cpp
   )
   
ADD_LIBRARY( PanelMethodLib STATIC ${PANELMETHOD_SRC} )

#ADD_CUSTOM_TARGET( PanelMethodLib )

SET_TARGET_PROPERTIES( PanelMethodLib PROPERTIES LINKER_LANGUAGE CXX )

#Create the vera targest for this library
ADD_VERA_CHECKS_RECURSE( PanelMethodLib *.h *.cpp )
ADD_HEADER_COMPILE_CHECK_RECURSE( PanelMethodLib *.h )
ADD_CPPCHECK( PanelMethodLib ${PANELMETHOD_SRC} )