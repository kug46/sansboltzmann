// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PANELMETHOD_XFOILPANEL_H_
#define SRC_PANELMETHOD_XFOILPANEL_H_

#include <limits>

#include "Field/FieldLine_CG_Cell.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/XFieldLine.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Class for handling data and calculation of panel method

class XfoilPanel
{
public:

  enum domainName
  {
    // indexed by assumed cell group number
    airfoil = 0,
    wake    = 1
  };

  typedef PhysD2 PhysDim;
  typedef TopoD1 TopoDim;

  static const int D = PhysDim::D;
  static const int N = 1; // size of solution ArrayQ

  const Real float_tol_ = 100*std::numeric_limits<Real>::epsilon(); // tolerance for floating point comparison

  // Order of various fields
  static const int order_xfld = 1;
  static const int order_gam = 1;
  static const int order_lam = 0;

  template<class T>
  using ArrayQ = T;

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template<class T>
  using MatrixQ = T;

  typedef DLA::VectorS<D,Real> VectorX;

  typedef XField<PhysDim,TopoDim> XFieldType;

  template <class Tg>
  using GamFieldType = Field_CG_Cell<PhysDim,TopoDim,Tg>;

  template <class Ts>
  using LamFieldType = Field_DG_Cell<PhysDim,TopoDim,Ts>;

  typedef typename XFieldType::template FieldCellGroupType<Line> XFieldCellGroupType;
  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldType;
  typedef typename ElementXFieldType::RefCoordType RefCoordType;

  XfoilPanel(const VectorX& Vinf, const Real& p0, const Real& T0,
             const XFieldType& xfld, const std::vector<int>& CellGroups) :
    Vinf_(Vinf),
    p0_(p0),
    T0_(T0),
    xfld_(xfld),
    panelCellGroups_(CellGroups)
  {
    SANS_ASSERT_MSG(order_xfld == (xfld_.getCellGroup<Line>(0)).order(), "Assumption that xfld is linear CG fails!");
  }

  // get panel counts
  int countPanel(const int cellGroupGlobal) const
  {
    SANS_ASSERT_MSG(cellGroupGlobal < xfld_.nCellGroups(), "Cell group index goes out of bound!");
    return (xfld_.getCellGroup<Line>(cellGroupGlobal)).nElem();
  }

  int nPanelAirfoil() const { return countPanel(domainName::airfoil); }
  int nPanelWake() const
  {
    if ((int)panelCellGroups_.size() == 1)
    {
      return 0;
    }
    else
    {
      return countPanel(domainName::wake);
    }
  }

  // get node counts
  int nNodeAirfoil() const { return nPanelAirfoil() + 1; }
  int nNodeWake() const
  {
    if ((int)panelCellGroups_.size() == 1)
    {
      return 0;
    }
    else
    {
      return nPanelWake() + 1;
    }
  }

  // access protected members
  const XFieldType& getXfield() const { return xfld_; }

  std::vector<int> getPanelCellGroups() const { return panelCellGroups_; }

  VectorX getVinf() const { return Vinf_; }
  Real getp0() const { return p0_; }
  Real getT0() const { return T0_; }

  // check TE gap: finite or sharp
  bool isFiniteTE() const
  {
    VectorX XTE1 = xfld_.DOF(nNodeAirfoil()-1), XTE2 = xfld_.DOF(0); // left/right TE node coordinates
    return (sqrt(dot(XTE1-XTE2,XTE1-XTE2)) >= float_tol_);
  }

  // bisector of upper & lower TE panels
  VectorX bisectorTE() const
  {
    const int nnode_a = nNodeAirfoil();

    VectorX tmp1 = (xfld_.DOF(0) - xfld_.DOF(1));
    tmp1 = tmp1 / sqrt(dot(tmp1,tmp1));

    VectorX tmp2 = (xfld_.DOF(nnode_a-1) - xfld_.DOF(nnode_a-2));
    tmp2 = tmp2 / sqrt(dot(tmp2,tmp2));

    VectorX bisecTE = 0.5*(tmp1+tmp2);
    return bisecTE/sqrt(dot(bisecTE,bisecTE));
  }

  // Compute streamfunction Psi and its directional derivative Psi_nep
  // at evaluation point Xep of node index inode and directional vector nep
  template<class Tg, class Ts>
  void Psicalc(const GamFieldType<Tg>& gamfld, const LamFieldType<Ts>& lamfld,
               const VectorX& Xep, const int inode, const VectorX& nep,
               const std::vector<int>& integrationCellGroups,
               typename promote_Surreal<Tg,Ts>::type& Psi,
               typename promote_Surreal<Tg,Ts>::type& Psi_nep) const;

  // this version assumes Xep is a panel node on airfoil or wake, such that nep is defined implicitly
  template<class Tg, class Ts>
  void Psicalc(const GamFieldType<Tg>& gamfld, const LamFieldType<Ts>& lamfld,
               const VectorX& Xep, const int inode,
               typename promote_Surreal<Tg,Ts>::type& Psi,
               typename promote_Surreal<Tg,Ts>::type& Psi_nep) const;

  // compute edge velocity Ue (scalar) at a node on airfoil or wake
  template<class Tg, class Ts>
  void Uecalc(const GamFieldType<Tg>& gamfld, const LamFieldType<Ts>& lamfld,
              const VectorX& Xep, const int inode,
              typename promote_Surreal<Tg,Ts>::type& Ue) const;

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
  {
    SANS_ASSERT(rsdPDEOut.m() == 1);
    rsdPDEOut[0] = rsdPDEIn;
  }

#if 1 // these are redundant, but needed to conform to AES_Debug pattern
  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const
  {
    SANS_ASSERT(rsdAuxOut.m() == 1);
    rsdAuxOut[0] = rsdAuxIn;
  }

  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const
  {
    SANS_ASSERT(rsdBCOut.m() == 1);
    rsdBCOut[0] = rsdBCIn;
  }
#endif

  // how many residual equations are we dealing with
  int nMonitor() const { return 1; }

protected:
  const VectorX Vinf_; // freestream velocity [m/s]
  const Real p0_; // stagnation pressure [Pa]
  const Real T0_; // stagnation temperature [K]
  const XFieldType& xfld_; // grid field
  const std::vector<int> panelCellGroups_; // panel cell groups
};


// Computes streamfunction Psi and its directional derivative along outward point normal
template<class Tg, class Ts>
void
XfoilPanel::
Psicalc(const GamFieldType<Tg>& gamfld, const LamFieldType<Ts>& lamfld,
        const VectorX& Xep, const int inode, const VectorX& nep,
        const std::vector<int>& integrationCellGroups,
        typename promote_Surreal<Tg,Ts>::type& Psi,
        typename promote_Surreal<Tg,Ts>::type& Psi_nep) const
{
  // Xep location
  // on airfoil: 0       <= inode < nnode_a
  // on wake:    nnode_a <= inode < nnode_a+nnode_w
  // off grid:   inode = -1

  // Ensures all the fields use a consistent xfld
  SANS_ASSERT(&gamfld.getXField() == &xfld_);
  SANS_ASSERT(&lamfld.getXField() == &xfld_);

  const int nelem_a = nPanelAirfoil();
  const int nnode_a = nNodeAirfoil();

  // Initialize output
  Psi = 0;
  Psi_nep = 0;

  //-------------------------------------------//
  // Sum up constributions of airfoil and wake panels to overall streamfunction Psi
  //-------------------------------------------//
  const int nCellGroups = integrationCellGroups.size();

  SANS_ASSERT_MSG(order_gam == (gamfld.template getCellGroup<Line>(domainName::airfoil)).order(),
                  "Assumption that gamfld has order 1 fails!");

  // looping over cell groups (i.e. domains: airfoil or wake)
  for (int group = 0; group < nCellGroups; ++group)
  {
    SANS_ASSERT_MSG(order_lam == (lamfld.template getCellGroup<Line>(group)).order(),
                    "Assumption that lamfld has order 0 fails!");

    const int cellGroupGlobal = integrationCellGroups.at(group);
    const XFieldCellGroupType& xfldCell = xfld_.getCellGroup<Line>(cellGroupGlobal);
    const int nelem = xfldCell.nElem();

    ElementXFieldType xfldElem(xfldCell.basis());
    const RefCoordType& sRefmid = Line::centerRef; // reference coordinate of element mid point

    // loop over all integration panels
    for (int elem = 0; elem < nelem; ++elem)
    {
      xfldCell.getElement(xfldElem, elem);

      // panel local directional vectors: {s,n} forms a right-handed system
      VectorX n = 0, s = 0;
      xfldElem.unitTangent(sRefmid, s);
      n = {-s[1],s[0]};

      // left/right node coordinates
      VectorX X1 = xfldElem.DOF(0);
      VectorX X2 = xfldElem.DOF(1);

      // relative position vector Xep-X
      VectorX dX1 = Xep-X1;
      VectorX dX2 = Xep-X2;

      // distance |Xep-X|
      Real r1 = sqrt(dot(dX1,dX1));
      Real r2 = sqrt(dot(dX2,dX2));

      // local {s,n} coordinates
      Real s1 = dot(dX1,s);
      Real s2 = dot(dX2,s);
      Real nn = dot(dX1,n);

      Real s1_nep = dot(s,nep);
      Real s2_nep = dot(s,nep);
      Real nn_nep = dot(n,nep);

      // panel size
      Real dl = xfldElem.length();

      // panel node indexing
      int inode1=-1, inode2=-1; // initialize
      if (cellGroupGlobal == domainName::airfoil)
      {
        inode1 = elem;
        inode2 = elem+1;
      }
      else if (cellGroupGlobal == domainName::wake)
      {
        inode1 = nnode_a + elem;
        inode2 = nnode_a + elem+1;
      }

      // log(r) and arctan(s/n)
      Real G1 = 0, beta1 = 0;
      if ( (inode != inode1) && (r1 > float_tol_) )
      {
        beta1 = atan2(s1,nn);
        G1 = log(r1);
      }

      Real G2 = 0, beta2 = 0;
      if ( (inode != inode2) && (r2 > float_tol_) )
      {
        beta2 = atan2(s2,nn);
        G2 = log(r2);
      }

      // vortex contributions
      if (cellGroupGlobal == domainName::airfoil)
      {
        Real PsiSum = s1*G1 - s2*G2+ s2-s1 + nn*(beta1-beta2);
        Real PsiDif = ((s1+s2)*PsiSum + r2*r2*G2 - r1*r1*G1 + 0.5*(s1*s1 - s2*s2)) / dl;

        Real PsiSum_s1 = G1;
        Real PsiSum_s2 = -G2;
        Real PsiSum_nn = beta1-beta2;

        Real PsiDif_s1 = ((s1+s2)*PsiSum_s1 + PsiSum - 2*s1*G1 - PsiDif) / dl;
        Real PsiDif_s2 = ((s1+s2)*PsiSum_s2 + PsiSum + 2*s2*G2 + PsiDif) / dl;
        Real PsiDif_nn = ((s1+s2)*PsiSum_nn - 2*nn*(G1-G2)                   ) / dl;

        Real PsiSum_nep = PsiSum_s1*s1_nep + PsiSum_s2*s2_nep + PsiSum_nn*nn_nep;
        Real PsiDif_nep = PsiDif_s1*s1_nep + PsiDif_s2*s2_nep + PsiDif_nn*nn_nep;

        Tg GamSum = gamfld.DOF(inode2) + gamfld.DOF(inode1);
        Tg GamDif = gamfld.DOF(inode2) - gamfld.DOF(inode1);

        Psi += (PsiSum*GamSum + PsiDif*GamDif) / (4.0*PI);
        Psi_nep += (PsiSum_nep*GamSum + PsiDif_nep*GamDif) / (4.0*PI);
      }

      // source contributions
      {
        // current panel midpoint quantities
        Real s0 = 0.5*(s1+s2);
        Real r0 = sqrt(s0*s0+nn*nn);
        Real beta0 = atan2(s0,nn);
        Real G0 = log(r0);

        Real s0_nep = 0.5*(s1_nep+s2_nep);

        // neighboring panel quantities
        const int elemM = std::max(0,elem-1);
        const int elemQ = std::min(nelem-1,elem+1);

        xfldCell.getElement(xfldElem,elemM);
        VectorX XM = xfldElem.DOF(0);

        xfldCell.getElement(xfldElem,elemQ);
        VectorX XQ = xfldElem.DOF(1);

        Real dlm = sqrt(dot(X1-XM,X1-XM));
        Real dlq = sqrt(dot(XQ-X2,XQ-X2));
        Real DSM = sqrt(dot(X2-XM,X2-XM));
        Real DSQ = sqrt(dot(X1-XQ,X1-XQ));

        // panel indexing
        int ipanel = -1, ipanelM = -1, ipanelQ = -1;
        if (cellGroupGlobal == domainName::airfoil)
        {
          ipanel = elem;
          ipanelM = std::max(0,elem-1);
          ipanelQ = std::min(nelem-1,elem+1);
        }
        else if (cellGroupGlobal == domainName::wake)
        {
          ipanel = nelem_a + elem;
          ipanelM = nelem_a + std::max(0,elem-1);
          ipanelQ = nelem_a + std::min(nelem-1,elem+1);
        }

        Ts LamM = (lamfld.DOF(ipanelM)*dlm + lamfld.DOF(ipanel)*dl)/DSM;
        Ts Lam = lamfld.DOF(ipanel);
        Ts LamQ = (lamfld.DOF(ipanel)*dl + lamfld.DOF(ipanelQ)*dlq)/DSQ;

        // unit streamfunctions
        Real PsiSum10 = s0*beta0 - s1*beta1 + nn*(G1-G0);
        Real PsiDif10 = ((s1+s0)*PsiSum10 + r1*r1*beta1 - r0*r0*beta0 + nn*(s0-s1)) / (s1-s0);

        Real PsiSum10_s1 = -beta1;
        Real PsiSum10_s0 = beta0;
        Real PsiSum10_nn = G1-G0;

        Real PsiDif10_s1 = ((s1+s0)*PsiSum10_s1 + PsiSum10 + 2*s1*beta1 - PsiDif10) / (s1-s0);
        Real PsiDif10_s0 = ((s1+s0)*PsiSum10_s0 + PsiSum10 - 2*s0*beta0 + PsiDif10) / (s1-s0);
        Real PsiDif10_nn = ((s1+s0)*PsiSum10_nn + 2*(s0-s1 + nn*(beta1-beta0))    ) / (s1-s0);

        Real PsiSum10_nep = PsiSum10_s1*s1_nep + PsiSum10_s0*s0_nep + PsiSum10_nn*nn_nep;
        Real PsiDif10_nep = PsiDif10_s1*s1_nep + PsiDif10_s0*s0_nep + PsiDif10_nn*nn_nep;

        Real PsiSum02 = s2*beta2 - s0*beta0 + nn*(G0-G2);
        Real PsiDif02 = ((s0+s2)*PsiSum02 + r0*r0*beta0 - r2*r2*beta2 + nn*(s2-s0)) / (s0-s2);

        Real PsiSum02_s0 = -beta0;
        Real PsiSum02_s2 = beta2;
        Real PsiSum02_nn = G0-G2;

        Real PsiDif02_s0 = ((s0+s2)*PsiSum02_s0 + PsiSum02 + 2*s0*beta0 - PsiDif02)/(s0-s2);
        Real PsiDif02_s2 = ((s0+s2)*PsiSum02_s2 + PsiSum02 - 2*s2*beta2 + PsiDif02)/(s0-s2);
        Real PsiDif02_nn = ((s0+s2)*PsiSum02_nn + 2*(s2-s0 + nn*(beta0-beta2))    )/(s0-s2);

        Real PsiSum02_nep = PsiSum02_s0*s0_nep + PsiSum02_s2*s2_nep + PsiSum02_nn*nn_nep;
        Real PsiDif02_nep = PsiDif02_s0*s0_nep + PsiDif02_s2*s2_nep + PsiDif02_nn*nn_nep;

        Ts LamSum10 = Lam + LamM;
        Ts LamDif10 = Lam - LamM;

        Ts LamSum02 = LamQ + Lam;
        Ts LamDif02 = LamQ - Lam;

        // Add to overall streamfunction
        Psi += (PsiSum10*LamSum10 + PsiDif10*LamDif10) / (4.0*PI);
        Psi_nep += (PsiSum10_nep*LamSum10 + PsiDif10_nep*LamDif10) / (4.0*PI);

        Psi += (PsiSum02*LamSum02 + PsiDif02*LamDif02) / (4.0*PI);
        Psi_nep += (PsiSum02_nep*LamSum02 + PsiDif02_nep*LamDif02) / (4.0*PI);
      }
    }
  }

  //-------------------------------------------//
  // Add contribution of finite-thickness TE panel
  //-------------------------------------------//
  if (isFiniteTE()) // TE has finite thickness
  {
    VectorX XTE1 = xfld_.DOF(nnode_a-1), XTE2 = xfld_.DOF(0); // left/right TE node coordinates

    // panel local directional vectors: {s,n} forms a right-handed system
    VectorX s = XTE2-XTE1;
    s = s/sqrt(dot(s,s));
    VectorX n = {-s[1], s[0]};

//    SANS_ASSERT( fabs(sqrt(dot(s,s)) - 1) < float_tol_ );
//    SANS_ASSERT( fabs(sqrt(dot(n,n)) - 1) < float_tol_ );

    // bisector of upper & lower TE panels
    VectorX bisecTE = bisectorTE();

    // relative position vector Xep-X
    VectorX dX1 = Xep-XTE1;
    VectorX dX2 = Xep-XTE2;

    // distance |Xep-X|
    Real r1 = sqrt(dot(dX1,dX1));
    Real r2 = sqrt(dot(dX2,dX2));

    // local {s,n} coordinates
    Real s1 = dot(dX1,s);
    Real s2 = dot(dX2,s);
    Real nn = dot(dX1,n);

    Real s1_nep = dot(s,nep);
    Real s2_nep = dot(s,nep);
    Real nn_nep = dot(n,nep);

    // log(r) and arctan(n/s)
    const int inode1 = nnode_a-1, inode2 = 0;

    Real G1 = 0, theta1 = 0;
    if ( (inode != inode1) && (r1 > float_tol_) )
    {
      theta1 = atan2(nn,s1);
      G1 = log(r1);
    }

    Real G2 = 0, theta2 = 0;
    if ( (inode != inode2) && (r2 > float_tol_) )
    {
      theta2 = atan2(nn,s2);
      G2 = log(r2);
    }

    // finite-thickness TE source strength
    const Tg lambdaTE = 0.5*(gamfld.DOF(0) - gamfld.DOF(nnode_a-1))
                           *fabs(bisecTE[0]*s[1]-bisecTE[1]*s[0]);

    Real PsiLam = s1*theta1 -s2*theta2 + nn*(G1-G2);

    Real PsiLam_s1 = theta1;
    Real PsiLam_s2 = -theta2;
    Real PsiLam_nn = G1-G2;

    Real PsiLam_nep = PsiLam_s1*s1_nep + PsiLam_s2*s2_nep + PsiLam_nn*nn_nep;

    Psi += lambdaTE*PsiLam/(2.0*PI);
    Psi_nep += lambdaTE*PsiLam_nep/(2.0*PI);
  }

  //-------------------------------------------//
  // Add contribution of freestream
  //-------------------------------------------//
  Psi += Vinf_[0]*Xep[1] - Vinf_[1]*Xep[0];
  Psi_nep += -Vinf_[1]*nep[0] + Vinf_[0]*nep[1];
}


template<class Tg, class Ts>
void
XfoilPanel::
Psicalc(const GamFieldType<Tg>& gamfld, const LamFieldType<Ts>& lamfld,
        const VectorX& Xep, const int inode,
        typename promote_Surreal<Tg,Ts>::type& Psi,
        typename promote_Surreal<Tg,Ts>::type& Psi_nep) const
{
  const int nnode_a = nNodeAirfoil();

  SANS_ASSERT_MSG(((inode>=0)&&(inode<nnode_a+nNodeWake())),"Panel node index goes out of bound!");

  // normal vector at evaluation point, pointing outward
  VectorX nep = 0;
  {
    int itrace = -1, cellGroupGlobal = -1;
    if ((inode>=0) && (inode<nnode_a))
    {
      itrace = inode;
      cellGroupGlobal = domainName::airfoil;
    }
    else if ((inode>=nnode_a) && (inode<nnode_a+nNodeWake()))
    {
      itrace = inode-nnode_a;
      cellGroupGlobal = domainName::wake;
    }

    const XFieldCellGroupType& xfldCell = xfld_.getCellGroup<Line>(cellGroupGlobal);
    const int nelem = xfldCell.nElem();

    // indices of left/right elements in the current cell group
    const int elemL = std::max(0,itrace-1);
    const int elemR = std::min(nelem-1,itrace);

    // get outward-pointing normals of left/right elements
    VectorX normalL = 0, normalR = 0;

    ElementXFieldType xfldElem(xfldCell.basis());
    const RefCoordType& sRefmid = Line::centerRef; // reference coordinate of element mid point

    xfldCell.getElement(xfldElem, elemL);
    xfldElem.unitNormal(sRefmid, normalL);

    xfldCell.getElement(xfldElem, elemR);
    xfldElem.unitNormal(sRefmid, normalR);

    // take bisector
    if (cellGroupGlobal == domainName::airfoil)
    {
      nep = normalL+normalR;
    }
    else if (cellGroupGlobal == domainName::wake)
    {
      nep = -(normalL+normalR);
    }
    nep = nep/sqrt(dot(nep,nep));
  }

  Psicalc(gamfld,lamfld,Xep,inode,nep,panelCellGroups_,Psi,Psi_nep);
}


// computes edge velocity component Ue
template<class Tg, class Ts>
void
XfoilPanel::
Uecalc(const GamFieldType<Tg>& gamfld, const LamFieldType<Ts>& lamfld,
       const VectorX& Xep, const int inode,
       typename promote_Surreal<Tg,Ts>::type& Ue) const
{
  typedef typename promote_Surreal<Tg,Ts>::type Tmix;

  // Note that the sign of Ue is chosen such that Ue is negative on the suction side (aka upper surface) or airfoil
  // and positive on the pressure side (aka lower surface) of airfoil. When multiplied with unit directional vectors of
  // CCW airfoil grids, the resulting velocity should be in the correct streamwise direction.
  //
  // Xep location
  // on airfoil: 0       <= inode < nnode_a
  // on wake:    nnode_a <= inode < nnode_a+nnode_w
  // off grid:   inode = -1

  // Ensures all the fields use a consistent xfld
  SANS_ASSERT(&gamfld.getXField() == &xfld_);
  SANS_ASSERT(&lamfld.getXField() == &xfld_);

  const int nnode_a = nNodeAirfoil();

  // Initialize output
  Ue = 0;
  //-------------------------------------------//
  // if evaluation point is on airfoil surface
  //-------------------------------------------//
  if ( (inode>=0)&&(inode<nnode_a) )
  {
    Ue = -gamfld.DOF(inode);
  }
  //-------------------------------------------//
  // if evaluation point is on wake or other place
  //-------------------------------------------//
  else if ( inode==nnode_a )
  {
    // ensures edge velocity at the first wake node is equal to that at TE
    Ue = 0.5*(gamfld.DOF(0) - gamfld.DOF(nnode_a-1));
//    Ue = -gamfld.DOF(nnode_a-1);
  }
  else if ( (inode>=nnode_a) && (inode<(nnode_a+nNodeWake())) )
  {
    Tmix Psi_dummy;
    Psicalc(gamfld,lamfld,Xep,inode,Psi_dummy,Ue);
  }
}

} // SANS namespace

#endif /* SRC_PANELMETHOD_XFOILPANEL_H_ */
