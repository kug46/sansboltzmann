// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Algebraic equation set class for
// linear-vorticity panel method with constant source strength

#ifndef SRC_PANELMETHOD_XFOILPANELEQUATIONSET_H_
#define SRC_PANELMETHOD_XFOILPANELEQUATIONSET_H_

#define PanelSharpTETreatment_isXfoilPaper 0

#include "BasisFunction/BasisFunctionCategory.h"

#include "Discretization/AlgebraicEquationSet_Debug.h"

#include "Field/FieldLine_DG_Cell.h"

#include "Surreal/SurrealS.h"

#include "Topology/ElementTopology.h"

namespace SANS
{
template <class PanelType_, class Traits>
class XfoilPanelEquationSet;

// instantiation
template <class PanelType_, class Traits>
class XfoilPanelEquationSet : public AlgebraicEquationSet_Debug<PanelType_, Traits>
{
public:
  typedef PanelType_ PanelClass;

  typedef typename PanelClass::PhysDim PhysDim;
  typedef typename PanelClass::TopoDim TopoDim;

  static const int D = PanelClass::D;
  static const int N = PanelClass::N;
  static const int Nauxi = 1; // number of inviscid auxiliary variables

  template<class T>
  using ArrayQ = typename PanelClass::template ArrayQ<T>;

  template<class T>
  using MatrixQ = typename PanelClass::template MatrixQ<T>;

  template<class T>
  using MatrixParam = T;

  using VectorX = typename PanelClass::VectorX;

  using XFieldType = typename PanelClass::XFieldType;

  template <class Tg>
  using GamFieldType = typename PanelClass::template GamFieldType<Tg>;

  template <class Ts>
  using LamFieldType = typename PanelClass::template LamFieldType<Ts>;

  static const int order_xfld = PanelClass::order_xfld;
  static const int order_gam = PanelClass::order_gam;
  static const int order_lam = PanelClass::order_lam;

  using XFieldCellGroupType = typename PanelClass::XFieldCellGroupType;
  using ElementXFieldType = typename PanelClass::ElementXFieldType;
  using RefCoordType = typename PanelClass::RefCoordType;

  typedef Traits TraitsTag;
  typedef AlgebraicEquationSetTraits<MatrixQ<Real>,ArrayQ<Real>,TraitsTag> TraitsType;

  typedef AlgebraicEquationSet_Debug<PanelType_, TraitsTag> DebugBaseType;
  typedef typename TraitsType::AlgebraicEquationSetBaseClass BaseType;

  typedef typename TraitsType::VectorSizeClass VectorSizeClass;
  typedef typename TraitsType::MatrixSizeClass MatrixSizeClass;

  typedef typename TraitsType::SystemMatrix SystemMatrix;
  typedef typename TraitsType::SystemVector SystemVector;
  typedef typename TraitsType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename TraitsType::SystemMatrixView SystemMatrixView;
  typedef typename TraitsType::SystemVectorView SystemVectorView;
  typedef typename TraitsType::SystemNonZeroPatternView SystemNonZeroPatternView;

  typedef typename BaseType::LinesearchData LinesearchData;

  typedef SurrealS<N> SurrealType;
  typedef ArrayQ<SurrealType> ArrayQSurreal;
  typedef DLA::VectorD<SLA::SparseVector<ArrayQSurreal>> SystemVectorSurreal;

  // number of subsets of equations/variable, which are indexed as follows
  static const int nsubeqn = 3;

  // Equation indexes
  static const int iProj  = 0; // Projection equation to compute lambda
  static const int iPanel = 1; // Panel method equations
  static const int iKutta = 2; // Kutta equation

  // State variable indexes
  static const int ilam   = 0; // Lambda state variable
  static const int igam   = 1; // Gamma  state variable
  static const int iPsi0  = 2; // Variable for kutta equation

  // Parameter index in JacobianParam (i.e. jacobian of panel method equation w.r.t. its parameters)
  static const int iParam  = 0; // Parameters = Auxiliary inviscid variable

  XfoilPanelEquationSet(const PanelClass& panel,
                        GamFieldType<Real>& gamfld,
                        Real& Psi0,
                        LamFieldType<Real>& lamfld,
                        const LamFieldType<Real>& qauxifld,
                        const Real& tol);

  virtual ~XfoilPanelEquationSet() {}

  using BaseType::residual;
  using BaseType::jacobian;
  using BaseType::jacobianTranspose;

  //Computes the residual
  virtual void residual(SystemVectorView& rsd) override;

  //Fills jacobian or the non-zero pattern of a jacobian
  virtual void jacobian(SystemMatrixView& mtx       ) override;
  virtual void jacobian(SystemNonZeroPatternView& nz) override;

  SLA::SparseMatrixSize jacobian_Qauxi_size() const;
  void jacobian_Qauxi_NonZero(SLA::SparseNonZeroPattern<Real>& nz) const;
  void jacobian_Qauxi(SLA::SparseMatrix_CRS<Real>& mtx) const;

  template<class SparseMatrixType>
  void jacobianParam(SparseMatrixType& jac) const;

  //Fills jacobian transpose or the non-zero pattern transpose of a jacobian transpose
  virtual void jacobianTranspose(SystemMatrixView& mtxT       ) override { SANS_DEVELOPER_EXCEPTION("Not implemented yet."); }
  virtual void jacobianTranspose(SystemNonZeroPatternView& nzT) override { SANS_DEVELOPER_EXCEPTION("Not implemented yet."); }

  //Evaluate Residual Norm
  virtual std::vector<std::vector<Real>> residualNorm( const SystemVectorView& rsd ) const override;

  //provides info about the residuals
  virtual void residualInfo(std::vector<std::string>& titles, std::vector<int>& idx) const override
  {
    titles = {"Source Projection : ",
              "Panel equation    : ",
              "Kutta condition   : "};
    idx = {iProj, iPanel, iKutta};
  }

  //Translates the system vector into a solution field
  virtual void setSolutionField(const SystemVectorView& q) override;

  //Translates the solution field into a system vector
  virtual void fillSystemVector(SystemVectorView& q) const override;

  // Returns the vector and matrix sizes needed for the linear algebra system
  virtual VectorSizeClass vectorEqSize() const override;    // vector for equations (rows in matrixSize)
  virtual VectorSizeClass vectorStateSize() const override; // vector for state DOFs (columns in matrixSize)
  virtual MatrixSizeClass matrixSize() const override;

  // Gives the PDE and solution indices in the system //TODO: are these actually necessary???
  virtual int indexPDE() const override { SANS_DEVELOPER_EXCEPTION("Not implemented yet."); return iPanel; }
  virtual int indexQ() const override { SANS_DEVELOPER_EXCEPTION("Not implemented yet."); return igam; }

  // update fraction needed for physically valid state
  virtual Real updateFraction(const SystemVectorView& q, const SystemVectorView& dq, const Real maxChangeFraction) const override
  {
    return 1.0; // full Newton update is used by default since the panel equations form a linear system
  }

  // Checks to see if proposed solution is physical
  virtual bool isValidStateSystemVector(SystemVectorView& q) override { return true; } // assume the state is always valid

  // Returns the size of the residual norm outer vector
  virtual int nResidNorm() const override { return nsubeqn; }

  // MPI communicator for this algebraic equation set
  virtual std::shared_ptr<mpi::communicator> comm() const override { return get<-1>(xfld_).comm(); }

  virtual void syncDOFs_MPI() override {}

  // Dump the localized linesearch parameter info (for debugging purposes)
  virtual void dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                                       const LinesearchData& pStepData) const override
  {
    SANS_DEVELOPER_EXCEPTION("Not implemented yet.");
  }

  // get DOF counts
  int nDOFgamma() const { return panel_.nNodeAirfoil(); }
  int nDOFlambda() const { return lamfld_.nDOF(); }

  // get references protected fields
  const PanelClass& getpanel() const { return panel_; }
  const XFieldType& getxfld() const { return xfld_; }
  const GamFieldType<Real>& getgamfld() const { return gamfld_; }
  const Real& getPsi0() const { return Psi0_; }
  const LamFieldType<Real>& getlamfld() const { return lamfld_; }
  const LamFieldType<Real>& getqauxifld() const { return qauxifld_; }
  VectorX getVinf() const { return Vinf_; }

  // specialized implementation to compute residual
  template<class Tg, class Tp, class Ts, class Vector>
  void residualPanel(const GamFieldType<Tg>& gamfld, const Tp& Psi0, const LamFieldType<Ts>& lamfld,
                     Vector& rsd) const;
protected:
  const PanelClass& panel_; // panel
  const XFieldType& xfld_; // grid field
  GamFieldType<Real>& gamfld_; // vortex strength (gamma) field
  Real& Psi0_; // constant streamfunction Psi_0
  LamFieldType<Real>& lamfld_; // source strength (sigma) field
  const LamFieldType<Real>& qauxifld_; // param field: delta^* mass defect thickness
  const VectorX Vinf_; // freestream velocity
  const Real tol_;

  // These influence matices are cached here because they are completely defined by the panel geometry, i.e. independent of solution
  DLA::MatrixD<Real> mtxPanel_lam_;  // Panel equation Jacobian wrt lam
  DLA::MatrixD<Real> mtxPanel_gam_;  // Panel equation Jacobian wrt gam
  DLA::MatrixD<Real> mtxPanel_Psi0_; // Panel equation Jacobian wrt Psi0

  // specialized implementation to compute jacobian
  template<class SystemMatrixType>
  void jacobian_impl(SystemMatrixType& mtx);
};


template <class PanelType_, class Traits>
XfoilPanelEquationSet<PanelType_, Traits>::
XfoilPanelEquationSet(const PanelClass& panel,
                       GamFieldType<Real>& gamfld,
                       Real& Psi0,
                       LamFieldType<Real>& lamfld,
                       const LamFieldType<Real>& qauxifld,
                       const Real& tol) :
   DebugBaseType(panel, std::vector<Real>({tol, tol, tol})),
   panel_(panel),
   xfld_(panel_.getXfield()),
   gamfld_(gamfld),
   Psi0_(Psi0),
   lamfld_(lamfld),
   qauxifld_(qauxifld),
   Vinf_(panel_.getVinf()),
   tol_(tol),
   mtxPanel_lam_(panel_.nNodeAirfoil(), lamfld.nDOF()),
   mtxPanel_gam_(panel_.nNodeAirfoil(), panel_.nNodeAirfoil()),
   mtxPanel_Psi0_(panel_.nNodeAirfoil(), 1)
{
  // check orders of fields
  SANS_ASSERT_MSG(order_xfld == (xfld_.template getCellGroup<Line>(0)).order(),
                  "xfld should have order order_xfld");
  SANS_ASSERT_MSG(order_gam == (gamfld_.template getCellGroup<Line>(0)).order(),
                  "gamfld should have order order_gam");
  SANS_ASSERT_MSG(order_lam == (lamfld_.template getCellGroup<Line>(0)).order(),
                  "lamfld should have order order_lam");

  // reset influence matrices (jacobian) in cache
  mtxPanel_lam_ = 0;
  mtxPanel_gam_ = 0;
  mtxPanel_Psi0_ = 0;

  // create surrealized solution fields
  GamFieldType<SurrealType> gamfldSurreal(xfld_, order_gam, BasisFunctionCategory_Hierarchical, {0});
  SurrealType Psi0Surreal;
  LamFieldType<SurrealType> lamfldSurreal(xfld_, order_lam, BasisFunctionCategory_Legendre);

  // initialize the jacobian caches for the scatter add
  mtxPanel_lam_ = 0;
  mtxPanel_gam_ = 0;
  mtxPanel_Psi0_ = 0;

  // set surrealized field DOF and derivatives
  const int nDOFgam = nDOFgamma();
  const int nDOFlam = nDOFlambda();

  for (int j = 0; j < nDOFgam; ++j)
    gamfldSurreal.DOF(j) = gamfld_.DOF(j);

  Psi0Surreal = Psi0_;

  for (int j = 0; j < nDOFlam; ++j)
    lamfldSurreal.DOF(j) = lamfld_.DOF(j);

  // compute cached jacobians using auto differentiation
  std::vector<int> MapRowPanel(nDOFgam);
  std::vector<int> MapCol(1);

  for (int i = 0; i < nDOFgam; ++i)
    MapRowPanel[i] = i;

  DLA::VectorD<Real> JacCol( nDOFgam );
  DLA::VectorD<SurrealType> rsdSurreal(nDOFgam);
  rsdSurreal = 0;

  // Projection equation
  for (int j = 0; j < nDOFlam; ++j)
  {
    MapCol[0] = j;

    lamfldSurreal.DOF(j).deriv() = 1; // shorthand for .deriv(0) since Surreal<1> has only one derivative
    residualPanel(gamfldSurreal, Psi0Surreal, lamfldSurreal, rsdSurreal);
    lamfldSurreal.DOF(j).deriv() = 0;

    for (int i = 0; i < nDOFgam; ++i)
      JacCol[i] = rsdSurreal[i].deriv();

    mtxPanel_lam_.scatterAdd(JacCol, MapRowPanel.data(), MapRowPanel.size(), MapCol.data(), MapCol.size());
  }

  // Panel equation
  for (int j = 0; j < nDOFgam; ++j)
  {
    MapCol[0] = j;

    gamfldSurreal.DOF(j).deriv() = 1; // shorthand for .deriv(0) since Surreal<1> has only one derivative
    residualPanel(gamfldSurreal, Psi0Surreal, lamfldSurreal, rsdSurreal);
    gamfldSurreal.DOF(j).deriv() = 0;

    for (int i = 0; i < nDOFgam; ++i)
      JacCol[i] = rsdSurreal[i].deriv();

    mtxPanel_gam_.scatterAdd(JacCol, MapRowPanel.data(), MapRowPanel.size(), MapCol.data(), MapCol.size());
  }

  // Panel equation
  for (int j = 0; j < 1; ++j)
  {
    MapCol[0] = j;

    Psi0Surreal.deriv() = 1;
    residualPanel(gamfldSurreal, Psi0Surreal, lamfldSurreal, rsdSurreal);
    Psi0Surreal.deriv() = 0;

    for (int i = 0; i < nDOFgam; ++i)
      JacCol[i] = rsdSurreal[i].deriv();

    mtxPanel_Psi0_.scatterAdd(JacCol, MapRowPanel.data(), MapRowPanel.size(), MapCol.data(), MapCol.size());
  }
}

template <class PanelType_, class Traits>
void
XfoilPanelEquationSet<PanelType_, Traits>::
jacobian(SystemMatrixView& mtx)
{
  this->template jacobian_impl<SystemMatrixView>(mtx);
}

template <class PanelType_, class Traits>
void
XfoilPanelEquationSet<PanelType_, Traits>::
jacobian(SystemNonZeroPatternView& nz)
{
  this->template jacobian_impl<SystemNonZeroPatternView>(nz);
}

template <class PanelType_, class Traits>
std::vector<std::vector<Real>>
XfoilPanelEquationSet<PanelType_, Traits>::
residualNorm(const SystemVectorView& rsd) const
{
  const int nDOFProj = rsd[iProj].m();
  const int nDOFPanel = rsd[iPanel].m();
  const int nDOFKutta = rsd[iKutta].m();

  const int nMon = panel_.nMonitor();

  DLA::VectorD<Real> rsdtmp(nMon);

  SANS_ASSERT(nMon == 1); // each state variable is scalar

  rsdtmp = 0;

  std::vector<std::vector<Real>> rsdNorm(nResidNorm(), std::vector<Real>(nMon, 0.0));

  //compute residual norm

  // Projection residuals
  for (int n = 0; n < nDOFProj; n++)
  {
    panel_.interpResidVariable(rsd[iProj][n], rsdtmp);

    for (int j = 0; j < nMon; j++)
      rsdNorm[iProj][j] += pow(rsdtmp[j],2);
  }

  for (int j = 0; j < nMon; j++)
    rsdNorm[iProj][j] = sqrt(rsdNorm[iProj][j]);

  // Panel method residuals
  for (int n = 0; n < nDOFPanel; n++)
  {
    panel_.interpResidVariable(rsd[iPanel][n], rsdtmp);

    for (int j = 0; j < nMon; j++)
      rsdNorm[iPanel][j] += pow(rsdtmp[j],2);
  }

  for (int j = 0; j < nMon; j++)
    rsdNorm[iPanel][j] = sqrt(rsdNorm[iPanel][j]);

  // Kutta condition residual
  for (int n = 0; n < nDOFKutta; n++)
  {
    panel_.interpResidVariable(rsd[iKutta][n], rsdtmp);

    for (int j = 0; j < nMon; j++)
      rsdNorm[iKutta][j] += pow(rsdtmp[j],2);
  }

  for (int j = 0; j < nMon; j++)
    rsdNorm[iKutta][j] = sqrt(rsdNorm[iKutta][j]);

  return rsdNorm;
}

//Translates the system vector into a solution field
template <class PanelType_, class Traits>
void
XfoilPanelEquationSet<PanelType_, Traits>::
setSolutionField(const SystemVectorView& q)
{
  for (int i = 0; i < nDOFlambda(); ++i)
    lamfld_.DOF(i) = q[ilam][i];

  for (int i = 0; i < nDOFgamma(); ++i)
    gamfld_.DOF(i) = q[igam][i];

  Psi0_ = q[iPsi0][0];
}

//Translates the solution field into a system vector
template <class PanelType_, class Traits>
void
XfoilPanelEquationSet<PanelType_, Traits>::
fillSystemVector(SystemVectorView& q) const
{
  for (int i = 0; i < nDOFlambda(); ++i)
    q[ilam][i] = lamfld_.DOF(i);

  for (int i = 0; i < nDOFgamma(); ++i)
    q[igam][i] = gamfld_.DOF(i);

  q[iPsi0][0] = Psi0_;
}

//Returns the vector and matrix sizes needed for the linear algebra system
template <class PanelType_, class Traits>
typename XfoilPanelEquationSet<PanelType_, Traits>::VectorSizeClass
XfoilPanelEquationSet<PanelType_, Traits>::
vectorEqSize() const
{
  VectorSizeClass size(nsubeqn);

  // Create the size that represents the size of a linear algebra vector
  size[iProj] = nDOFlambda();
  size[iPanel] = nDOFgamma();
  size[iKutta] = 1;

  return size;
}

template <class PanelType_, class Traits>
typename XfoilPanelEquationSet<PanelType_, Traits>::VectorSizeClass
XfoilPanelEquationSet<PanelType_, Traits>::
vectorStateSize() const
{
  return vectorEqSize();
}


template <class PanelType_, class Traits>
typename XfoilPanelEquationSet<PanelType_, Traits>::MatrixSizeClass
XfoilPanelEquationSet<PanelType_, Traits>::
matrixSize() const
{
  const int nDOFlam = nDOFlambda();
  const int nDOFgam = nDOFgamma();

  MatrixSizeClass size(nsubeqn,nsubeqn);

  // Create the size that represents the size of a linear algebra matrix
  size(iProj,ilam)  = {nDOFlam, nDOFlam};
  size(iPanel,ilam) = {nDOFgam, nDOFlam};
  size(iKutta,ilam) = {      1, nDOFlam};

  size(iProj,igam)  = {nDOFlam, nDOFgam};
  size(iPanel,igam) = {nDOFgam, nDOFgam};
  size(iKutta,igam) = {      1, nDOFgam};

  size(iProj,iPsi0)  = {nDOFlam, 1};
  size(iPanel,iPsi0) = {nDOFgam, 1};
  size(iKutta,iPsi0) = {      1, 1};

  return size;
}


template <class PanelType_, class Traits>
void
XfoilPanelEquationSet<PanelType_, Traits>::
residual(SystemVectorView& rsd)
{
  const int nnode_a = panel_.nNodeAirfoil();
  const int nDOFlam = lamfld_.nDOF();

#if 0 // [Function evaluation] Compute panel equation residuals by calling re-evaluating the residuals
  residualPanel(gamfld_, Psi0_, lamfld_, rsd[iPanel]);
#else // [Matrix arithmetic] Compute panel equation residuals by reusing cached influence matrices since these residuals are linear
  for (int i=0; i<nnode_a; ++i)
  {
    rsd[iPanel][i] = 0; // reset this before accumulation starts

    for (int j=0; j<nDOFgamma(); ++j) // vorticity contribution
    {
      rsd[iPanel][i] += mtxPanel_gam_(i,j)*gamfld_.DOF(j);
    }

    for (int j=0; j<nDOFlam; ++j) // source contribution
    {
      rsd[iPanel][i] += mtxPanel_lam_(i,j)*lamfld_.DOF(j);
    }

    rsd[iPanel][i] += mtxPanel_Psi0_(i,0)*Psi0_; // Psi_0 contribution

    // const freestream contribution
    const VectorX Xcp = xfld_.DOF(i); // control point coordinates
    if ( !(panel_.isFiniteTE()) && (i==(nnode_a-1)) ) // modification to sharpTE
    {
#if PanelSharpTETreatment_isXfoilPaper // 1989 XFOIL paper
      // no need to add anything here
#else // XFOIL 6.99
      VectorX bisecTE = panel_.bisectorTE();
      VectorX nep = {-bisecTE[1], bisecTE[0]};
      rsd[iPanel][i] += -Vinf_[1]*nep[0] + Vinf_[0]*nep[1];
#endif
    }
    else
    {
      rsd[iPanel][i] += Vinf_[0]*Xcp[1] - Vinf_[1]*Xcp[0];
    }
  }
#endif

  // Kutta condition
  rsd[iKutta][0] = gamfld_.DOF(0) + gamfld_.DOF(nnode_a-1);

  // source strength projection
  for (int i = 0; i < nDOFlam; ++i)
  {
    Real lambdaInput = qauxifld_.DOF(i);
    rsd[iProj][i] = lamfld_.DOF(i) - lambdaInput;
  }
}

// specialized implementation to compute jacobian
template <class PanelType_, class Traits>
template<class SystemMatrixType>
void
XfoilPanelEquationSet<PanelType_, Traits>::
jacobian_impl(SystemMatrixType& mtx)
{
  SANS_ASSERT(mtx.m() == nsubeqn);
  SANS_ASSERT(mtx.n() == nsubeqn);

  // Get the matrix type of components of SparseMatrixType (aka jac). This could be a reference or temporary variable depending on SparseMatrixType
  typedef typename std::result_of<SystemMatrixType(const int, const int)>::type Matrix;

  Matrix jacProj_lam  = mtx(iProj , ilam);
  Matrix jacPanel_lam = mtx(iPanel, ilam);

  Matrix jacPanel_gam = mtx(iPanel, igam);
  Matrix jacKutta_gam = mtx(iKutta, igam);

  Matrix jacPanel_Psi0 = mtx(iPanel, iPsi0);

  // set DOF and derivatives
  const int nDOFgam = nDOFgamma();
  const int nDOFlam = nDOFlambda();

  std::vector<int> Map_lam(nDOFlam);
  std::vector<int> Map_gam(nDOFgam);
  std::vector<int> Map_Psi(1);
  std::vector<int> MapRowKutta(1), MapColKutta(2);
  std::vector<int> MapProj(1);

  for (int i = 0; i < nDOFlam; ++i)
    Map_lam[i] = i;

  for (int i = 0; i < nDOFgam; ++i)
    Map_gam[i] = i;

  Map_Psi[0] = 0;

  // Jacobian of panel equations based on cached influence matrices
  jacPanel_lam.scatterAdd(mtxPanel_lam_  , Map_gam.data(), Map_gam.size(), Map_lam.data(), Map_lam.size());
  jacPanel_gam.scatterAdd(mtxPanel_gam_  , Map_gam.data(), Map_gam.size(), Map_gam.data(), Map_gam.size());
  jacPanel_Psi0.scatterAdd(mtxPanel_Psi0_, Map_gam.data(), Map_gam.size(), Map_Psi.data(), Map_Psi.size());

  // Jacobian of projection equation is a simple diagonal matrix
  DLA::MatrixD<Real> jacProj(1,1); jacProj = 1;

  for (int j = 0; j < nDOFlam; ++j)
  {
    MapProj[0] = j;
    jacProj_lam.scatterAdd(jacProj, MapProj.data(), MapProj.size(), MapProj.data(), MapProj.size());
  }

  // Jacobian of Kutta condition is a single row array and only involves two gamma values
  MapRowKutta[0] = 0;
  DLA::MatrixD<Real> jacKutta(1,2);
  jacKutta(0,0) = 1;
  jacKutta(0,1) = 1;

  MapColKutta = {0, panel_.nNodeAirfoil()-1};
  jacKutta_gam.scatterAdd(jacKutta, MapRowKutta.data(), MapRowKutta.size(), MapColKutta.data(), MapColKutta.size());
}

// specialized implementation to compute residual for the panel equation
template <class PanelType_, class Traits>
template<class Tg, class Tp, class Ts, class Vector>
void
XfoilPanelEquationSet<PanelType_, Traits>::
residualPanel(const GamFieldType<Tg>& gamfld, const Tp& Psi0, const LamFieldType<Ts>& lamfld,
              Vector& rsd) const
{
  typedef typename promote_Surreal<Tg,Tp,Ts>::type Tmix;

  const int nnode_a = panel_.nNodeAirfoil();

  // Construct residuals

  // Panel equation: Psi - Psi_0 = 0 at all control points (i.e. airfoil panel nodes)
  for ( int node = 0; node < nnode_a; node++ )
  {
    const VectorX Xcp = xfld_.DOF(node);

    // total streamfunction at control point (only using datatypes that are needed)
    Tmix Psi = 0;
    Tmix Psi_nep_dummy = 0;
    panel_.Psicalc(gamfld, lamfld, Xcp, node, Psi, Psi_nep_dummy);

    rsd[node] = Psi - Psi0;
  }

  // sharp trailing edge treatment
  if (!panel_.isFiniteTE())
  {
#if PanelSharpTETreatment_isXfoilPaper // 1989 XFOIL paper
    rsd[iPDE][nnode_a-1] = (gamfld.DOF(2) - 2*gamfld.DOF(1) + gamfld.DOF(0))
                         - (gamfld.DOF(nnode_a-3) - 2*gamfld.DOF(nnode_a-2) + gamfld.DOF(nnode_a-1));
#else // XFOIL 6.99
    VectorX bisecTE = panel_.bisectorTE();
    VectorX nep = {-bisecTE[1], bisecTE[0]};

    VectorX dX1 = xfld_.DOF(0) - xfld_.DOF(1);
    VectorX dX2 = xfld_.DOF(nnode_a-1) - xfld_.DOF(nnode_a-2);

    const Real dl1 = sqrt(dot(dX1,dX1));
    const Real dl2 = sqrt(dot(dX2,dX2));
    const Real ds = std::min(dl1,dl2);

    VectorX Xep = xfld_.DOF(0) - 0.1*ds*bisecTE;
    const int inode = -1;
    Tmix Psi_dummy = 0, Psi_nep = 0;
    const std::vector<int> integrationCellGroups = {PanelClass::domainName::airfoil};
    panel_.Psicalc(gamfld, lamfld, Xep, inode, nep, integrationCellGroups, Psi_dummy, Psi_nep);

    rsd[nnode_a-1] = Psi_nep;
#endif
  }
}

// compute jacobian wrt qauxi parameters
// TODO: this function is not unit tested in PanelMethod directory, but appears in 4x4 coupled IBL jacobian ping test
template<class PanelType_, class Traits>
template<class SparseMatrixType>
void
XfoilPanelEquationSet<PanelType_, Traits>::
jacobianParam(SparseMatrixType& jac) const
{
  SANS_ASSERT(jac.m() >= 3);
  SANS_ASSERT(jac.n() == 1); // assume there is only one column block of sparse matrix corresponding to Qauxi

  // Get the matrix type, this could be a reference or temporary variable depending on SparseMatrixType
  typedef typename std::result_of<SparseMatrixType(const int, const int)>::type Matrix;

  Matrix jacPanel_Qauxi = jac(iProj, iParam);

  std::vector<int> GlobalRowMap(1);
  std::vector<int> GlobalColMap(1);

  // source strength projection
  const int nDOFlam = lamfld_.nDOF();

  DLA::MatrixD<Real> jac_unit(1,1); // smallest jacobian unit for fill-in

  for (int i = 0; i < nDOFlam; ++i)
  {
    // Fill jacobian for the following residual
    // Tmix lambdaInput = qauxifld.DOF(i);
    // rsd[iProj][i] = lamfld.DOF(i) - lambdaInput;

    GlobalRowMap[0] = i;
    GlobalColMap[0] = i;

    jac_unit(0,0) = -1;

    // add the local Jacobian to the matrix
    jacPanel_Qauxi.scatterAdd(jac_unit, GlobalRowMap.data(), GlobalRowMap.size(), GlobalColMap.data(), GlobalColMap.size());
  }
}

}

#endif /* SRC_PANELMETHOD_XFOILPANELEQUATIONSET_H_ */
