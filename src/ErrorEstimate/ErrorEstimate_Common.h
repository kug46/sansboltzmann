// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ERRORESTIMATE_COMMON_H
#define ERRORESTIMATE_COMMON_H

#include <cmath>
#include <vector> //vector
#include <set> //set

#include "tools/KahanSum.h"
#include "tools/Tuple.h"

#include "Field/XField.h"
#include "Field/Field.h"

#include "Field/tools/GroupFunctorType.h"
#include "Quadrature/Quadrature.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"


namespace SANS
{

// Some Common Functors for use in all the Error Estimate classes

// Functor for finding out which global dofs apply to a macro element in the local solve
// For instance, Splitting one triangle to give two, it will collect the 3 corner dofs, and the new dof introduced
template<class PhysDim>
class ConstructDOFSet : public GroupFunctorCellType<ConstructDOFSet<PhysDim>>
{
public:
  ConstructDOFSet( std::vector<std::set<int>>& dofArray, const std::vector<int>& cellGroups ) :
  dofArray_(dofArray), cellGroups_(cellGroups)
  {
    SANS_ASSERT(cellGroups_.size() == 1); // assumes local solve origin
  }

  std::size_t nCellGroups()          const { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n];     }

  //----------------------------------------------------------------------------//
  // Distribution function that redistributes the error in each cell group
  template< class Topology>
  void
  apply( const typename Field_CG_Cell<PhysDim, typename Topology::TopoDim, Real>::
                     template FieldCellGroupType<Topology>& efldCell,
         const int cellGroupGlobal)
  {
    // Cell Group Types
    typedef typename Field_CG_Cell< PhysDim,typename Topology::TopoDim, Real >::template FieldCellGroupType<Topology> EFieldCellGroupType;
    typedef typename EFieldCellGroupType::template ElementType<>  ElementEFieldClass;

    // loop over groups
    ElementEFieldClass efldElem(efldCell.basis() );

    std::vector<int> efldCellGlobalMap( efldCell.nBasis() );

    SANS_ASSERT( efldCell.nElem()%2 == 0 ); // came from a grid split in two
    dofArray_.resize( efldCell.nElem()/2 );

    // loop over elements
    for (int elem = 0; elem < efldCell.nElem(); elem++)
    {
      efldCell.associativity(elem).getGlobalMapping( efldCellGlobalMap.data(), efldCellGlobalMap.size() );

      // insert the dofs for the element into the relevant set
      // uniqueness of sets means that double counting is avoided later
      // elem%(efldCell.nElem()/2) means it goes 0, 1, 2, 0, 1, 2 if there are 6 elements
      dofArray_[elem%(efldCell.nElem()/2)].insert( efldCellGlobalMap.begin(), efldCellGlobalMap.end() );

    }
  }
protected:
  std::vector<std::set<int>>& dofArray_;
  const std::vector<int> cellGroups_;
};


template < class PhysDim_>
class DistributeBoundaryTraceField : public GroupFunctorBoundaryTraceType<DistributeBoundaryTraceField<PhysDim_>>
{
public:
  typedef PhysDim_ PhysDim;
  explicit DistributeBoundaryTraceField( const std::vector<int>& boundaryTraceGroups, const bool absFlag = false ) :
  boundaryTraceGroups_(boundaryTraceGroups), absFlag_(absFlag) {}

  std::size_t nBoundaryTraceGroups() const { return boundaryTraceGroups_.size(); }
  std::size_t boundaryTraceGroup(const int n) const { return boundaryTraceGroups_[n]; }

  //----------------------------------------------------------------------------//
  // Distribution function that redistributes the error in each cell group
  template< class TopologyTrace, class Topology>
  void
  apply( const typename Field<PhysDim, typename Topology::TopoDim, Real>::template FieldCellGroupType<Topology>& efld,
         const typename Field<PhysDim, typename Topology::TopoDim, Real>::template FieldTraceGroupType<TopologyTrace>& eBfld,
         const typename XField<PhysDim, typename Topology::TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
         const int traceGroupGlobal)
  {
    typedef typename Field<PhysDim, typename Topology::TopoDim, Real>::template FieldCellGroupType<Topology>      EFieldCellGroupType;
    typedef typename Field<PhysDim, typename Topology::TopoDim, Real>::template FieldTraceGroupType<TopologyTrace> EBFieldTraceGroupType;

    typedef typename EFieldCellGroupType::template ElementType<> ElementEFieldClass;
    typedef typename EBFieldTraceGroupType::template ElementType<> ElementEBFieldTraceClass;

    // Elements
    ElementEFieldClass efldElem( efld.basis() );
    ElementEBFieldTraceClass eBfldElem( eBfld.basis() );
    if (absFlag_ == false) // logic performed before loop for speed
    { // DEFAULT BRANCH
      SANS_ASSERT_MSG( xfldTrace.nElem() == eBfld.nElem(), "xfldTrace has %d elements, eBfld has %d elements", xfldTrace.nElem(), eBfld.nElem() );

      // loop over elements in trace group
      for (int elem = 0; elem< xfldTrace.nElem(); elem++)
      {
        int elemL = xfldTrace.getElementLeft( elem );
//        std::cout << "group, elemL, elem = " << traceGroupGlobal << "," << elemL << "," << elem <<std::endl;
        efld.getElement( efldElem, elemL);
        eBfld.getElement( eBfldElem, elem );
        efldElem.DOF(0) += eBfldElem.DOF(0);
        const_cast<EFieldCellGroupType&>(efld).setElement( efldElem, elemL);
      }
    }
    else
    { // OPTIONAL BRANCH
      // loop over elements in trace group
      SANS_ASSERT_MSG( xfldTrace.nElem() == eBfld.nElem(), "xfldTrace has %d elements, eBfld has %d elements", xfldTrace.nElem(), eBfld.nElem() );

      for (int elem = 0; elem< xfldTrace.nElem(); elem++)
      {
        int elemL = xfldTrace.getElementLeft( elem );
//        std::cout << "group, elemL, elem = " << traceGroupGlobal << "," << elemL << "," << elem <<std::endl;
        efld.getElement( efldElem, elemL);
        eBfld.getElement( eBfldElem, elem );
        efldElem.DOF(0) += fabs(eBfldElem.DOF(0));
        const_cast<EFieldCellGroupType&>(efld).setElement( efldElem, elemL);
      }
    }
  }
protected:
  const std::vector<int>& boundaryTraceGroups_;
  const bool absFlag_;
};

template < class PhysDim_ >
class AccumulateVector : public GroupFunctorCellType<AccumulateVector<PhysDim_>>
{
public:
  typedef PhysDim_ PhysDim;
  AccumulateVector( std::vector<std::vector<Real>>& errorArray, const std::vector<int>& cellGroups ) :
  errorArray_(errorArray), cellGroups_(cellGroups) {}

  std::size_t nCellGroups()          const { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n];     }

  //----------------------------------------------------------------------------//
  // Distribution function that redistributes the error in each cell group
  template< class Topology>
  void
  apply( const typename Field<PhysDim, typename Topology::TopoDim, Real>::
                     template FieldCellGroupType<Topology>& efldCell,
         const int cellGroupGlobal)
  {
    // Cell Group Types
    typedef typename Field< PhysDim,typename Topology::TopoDim, Real >::template FieldCellGroupType<Topology> EFieldCellGroupType;
    typedef typename EFieldCellGroupType::template ElementType<>  ElementEFieldClass;

    // loop over groups
    ElementEFieldClass efldElem(efldCell.basis() );
    // loop over elements
    for (int elem = 0; elem < efldCell.nElem(); elem++)
    {
//      std::cout<< "elem = " << elem << std::endl;
      efldCell.getElement(efldElem,elem);

      // if efld is p0 this will only assign the value
      // if efld is p1 this will assign the first then loop to collect the remainders

      errorArray_[cellGroupGlobal][elem] = efldElem.DOF(0); // assign
      for (int n = 1; n < efldElem.nDOF(); n++) // loop starts at 1 here because of the above assign
        errorArray_[cellGroupGlobal][elem] += efldElem.DOF(n); // accumulate
    }
  }

protected:
  std::vector<std::vector<Real>>& errorArray_;
  const std::vector<int> cellGroups_;
};

template < class PhysDim_>
class FieldSum : public GroupFunctorCellType<FieldSum<PhysDim_>>
{
public:
  typedef PhysDim_ PhysDim;
  FieldSum( KahanSum<Real>& Sum, const std::vector<int>& cellGroups, const int comm_rank, const bool absFlag = false ) :
    Sum_(Sum), cellGroups_(cellGroups), comm_rank_(comm_rank), absFlag_(absFlag)  {}

  std::size_t nCellGroups() const          { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n];     }

  //----------------------------------------------------------------------------//
  // Summation Function that loops over cell Groups to return global estimate
  template< class Topology >
  void
  apply(const typename Field_DG_Cell<PhysDim,typename Topology::TopoDim,Real>
            ::template FieldCellGroupType<Topology>& efldCell, const int cellGroupGlobal)
  {
    // Cell Group Types
    typedef typename Field< PhysDim, typename Topology::TopoDim, Real >::template FieldCellGroupType<Topology> EFieldCellGroupType;
    typedef typename EFieldCellGroupType::template ElementType<>  ElementEFieldClass;

    ElementEFieldClass efldElem(efldCell.basis() );

    if (absFlag_ == false)
    {
      for (int elem = 0; elem < efldCell.nElem(); elem++ )
      {
        efldCell.getElement(efldElem,elem);

        // want to only count each element once
        if (efldElem.rank() != comm_rank_) continue;

        for (int dof = 0; dof < efldElem.nDOF(); dof++)
          Sum_ += efldElem.DOF(dof);
      }
    }
    else
    {
      for (int elem = 0; elem < efldCell.nElem(); elem++ )
      {
        efldCell.getElement(efldElem,elem);

        // want to only count each element once
        if (efldElem.rank() != comm_rank_) continue;

        Real tmp = 0.0;
        for (int dof = 0; dof < efldElem.nDOF(); dof++)
          tmp += efldElem.DOF(dof);

        Sum_ += fabs(tmp);
      }
    }
  }
protected:
  KahanSum<Real>& Sum_;
  const std::vector<int> cellGroups_;
  const int comm_rank_;
  const bool absFlag_;
};


// class for collecting nodal estimates up into P0 elements
template<class PhysDim>
class CollectNodalEstimates:
    public GroupFunctorCellType<CollectNodalEstimates<PhysDim>>
{
public:
  explicit CollectNodalEstimates( const std::vector<int>& cellGroups, const bool absFlag = false ) :
  cellGroups_(cellGroups), absFlag_(absFlag) {}

  std::size_t nCellGroups()          const { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n];     }

  //----------------------------------------------------------------------------//
  // Distribution function that redistributes the error in each cell group
  template< class Topology >
  void
  apply( const typename MakeTuple< FieldTuple,Field_CG_Cell<PhysDim, typename Topology::TopoDim, Real>, // efld
                                              Field_DG_Cell<PhysDim, typename Topology::TopoDim, Real>, // enodal
                                              Field_DG_Cell<PhysDim, typename Topology::TopoDim, Real>, // Kfld
                                              Field_DG_Cell<PhysDim, typename Topology::TopoDim, Real>>::type:: //Sfld
                     template FieldCellGroupType<Topology>& fldsCell,
      const int cellGroupGlobal)
  {
    // Cell Group Types
    typedef typename Field< PhysDim,typename Topology::TopoDim,Real>::template FieldCellGroupType<Topology> EFieldCellGroupType;

    typedef typename EFieldCellGroupType::template ElementType<>  ElementEFieldClass;

    // Apportioning the eLfld to efld
    // loop over groups
          EFieldCellGroupType& efldCell  = const_cast<EFieldCellGroupType&>(get<0>(fldsCell)); // the P0 field accumulated to
    const EFieldCellGroupType& efldNodal = get<1>(fldsCell); // The P1 field to be collected from
    const EFieldCellGroupType& Kfld      = get<2>(fldsCell); // The element volume field
    const EFieldCellGroupType& Sfld      = get<3>(fldsCell); // The star volume field

    ElementEFieldClass efldCellElem(efldCell.basis() );
    ElementEFieldClass efldNodalElem(efldNodal.basis() );
    ElementEFieldClass KfldElem(Kfld.basis() );
    ElementEFieldClass SfldElem(Sfld.basis() );

    SANS_ASSERT( efldCell.basis()->order() == 0 );
    SANS_ASSERT( efldNodal.basis()->order() == 1 );
    SANS_ASSERT( Kfld.basis()->order() == 0 );
    SANS_ASSERT( Sfld.basis()->order() == 1 );

    if (absFlag_ == false) // logic performed before loop for speed
    { // DEFAULT BRANCH
      // add the components of the eLfld into the efld
      for (int elem = 0; elem < efldCell.nElem(); elem++)
      {
        efldCell.getElement(efldCellElem,elem);
        efldNodal.getElement(efldNodalElem,elem);
        Kfld.getElement( KfldElem,elem);
        Sfld.getElement( SfldElem,elem);

        efldCellElem.DOF(0) = 0.0;

        for (int n = 0; n < efldNodalElem.nDOF(); n++) // d + 1 nodes to a simplex
          efldCellElem.DOF(0) += efldNodalElem.DOF(n)*KfldElem.DOF(0) / SfldElem.DOF(n);

        efldCell.setElement(efldCellElem,elem);
      }
    }
    else
    { // OPTIONAL BRANCH
      // adds the absolution
      for (int elem = 0; elem < efldCell.nElem(); elem++)
      {
        efldCell.getElement(efldCellElem,elem);
        efldNodal.getElement(efldNodalElem,elem);
        Kfld.getElement( KfldElem,elem);
        Sfld.getElement( SfldElem,elem);

        efldCellElem.DOF(0) = 0.0;

        for (int n = 0; n < efldNodalElem.nDOF(); n++) // d + 1 nodes to a simplex
          efldCellElem.DOF(0) += efldNodalElem.DOF(n)*KfldElem.DOF(0) / SfldElem.DOF(n);

        efldCellElem.DOF(0) = fabs(efldCellElem.DOF(0));///efldNodalElem.nDOF(); // collect from nodes then absolute

        efldCell.setElement(efldCellElem,elem);
      }
    }
  }

protected:
  const std::vector<int> cellGroups_;
  const bool absFlag_;
};


//----------------------------------------------------------------------------//
// fills a P0 field with the cell volume information
template<class PhysDim>
class CellVolumeField : public GroupFunctorCellType<CellVolumeField<PhysDim>>
{
public:

  explicit CellVolumeField( const std::vector<int>& cellGroups ) :
    cellGroups_(cellGroups) { }

  std::size_t nCellGroups() const          { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n];     }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology >
  void
  apply( const typename FieldTuple<XField<PhysDim, typename Topology::TopoDim>, Field<PhysDim, typename Topology::TopoDim, Real>, TupleClass<>>::
                        template FieldCellGroupType<Topology>& fldsCell,
         const int cellGroupGlobal )
  {
    typedef typename XField<PhysDim, typename Topology::TopoDim>     ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field<PhysDim, typename Topology::TopoDim, Real>::template FieldCellGroupType<Topology> KFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename KFieldCellGroupType::template ElementType<> ElementKFieldClass;

    typedef QuadraturePoint<typename Topology::TopoDim> QuadPointType;

    const XFieldCellGroupType& xfldCell = get<0>(fldsCell);
          KFieldCellGroupType& kfldCell = const_cast<KFieldCellGroupType&>( get<1>(fldsCell) );

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementKFieldClass kfldElem( kfldCell.basis() );

    SANS_ASSERT_MSG( kfldCell.basis()->order() == 0, "K field must be P0, it is the volume of the element" );

    Quadrature<typename Topology::TopoDim, Topology> quadrature( xfldCell.basis()->order() );

    const int nquad = quadrature.nQuadrature();
    Real weight, J;

    // loop over elements within group
    const int nelem = xfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid DOFs to element
      xfldCell.getElement( xfldElem, elem );
      kfldCell.getElement( kfldElem, elem );

      // compute the size of the cell element
      J = 0;
      for (int iquad = 0; iquad < nquad; iquad++)
      {
        quadrature.weight( iquad, weight );
        QuadPointType sRef = quadrature.coordinates_cache( iquad );

        J += weight * xfldElem.jacobianDeterminant( sRef );
      }

      // cell size divided by perimeter size
      kfldElem.DOF(0) = J;

      // save the h
      kfldCell.setElement( kfldElem, elem );
    }
  }

protected:
  const std::vector<int> cellGroups_;
};


//----------------------------------------------------------------------------//
// fills a P1 field with the volumes of the cells attached to a node
template<class PhysDim>
class StarVolumeField : public GroupFunctorCellType<StarVolumeField<PhysDim>>
{
public:

  explicit StarVolumeField( const std::vector<int>& cellGroups ) :
    cellGroups_(cellGroups) { }

  std::size_t nCellGroups() const          { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n];     }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology >
  void
  apply( const typename FieldTuple< Field<PhysDim, typename Topology::TopoDim, Real>,
                                    Field<PhysDim, typename Topology::TopoDim, Real>, TupleClass<>>::
                        template FieldCellGroupType<Topology>& fldsCell,
         const int cellGroupGlobal )
  {
    typedef typename Field<PhysDim, typename Topology::TopoDim, Real>::template FieldCellGroupType<Topology> KFieldCellGroupType;

    typedef typename KFieldCellGroupType::template ElementType<> ElementKFieldClass;

    KFieldCellGroupType& kfldCell = const_cast<KFieldCellGroupType&>( get<0>(fldsCell) );
    KFieldCellGroupType& sfldCell = const_cast<KFieldCellGroupType&>( get<1>(fldsCell) );

    // element field variables
    ElementKFieldClass kfldElem( kfldCell.basis() ), sfldElem( sfldCell.basis() );

    SANS_ASSERT_MSG( kfldCell.basis()->order() == 0, "K field must be P0, it is the volume of the element" );
    SANS_ASSERT_MSG( sfldCell.basis()->order() == 1, "S field must be P1, it stores the volume of the star" );

    // loop over elements within group
    const int nelem = kfldCell.nElem();
    SANS_ASSERT_MSG( nelem == sfldCell.nElem(), "number of elements must match" );
    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid DOFs to element
      kfldCell.getElement( kfldElem, elem );
      sfldCell.getElement( sfldElem, elem );

      for (int n = 0; n < sfldCell.nBasis(); n++)
        sfldElem.DOF(n) += kfldElem.DOF(0);

      // save the volume
      sfldCell.setElement( sfldElem, elem );
    }
  }

protected:
  const std::vector<int> cellGroups_;
};


} //namespace SANS

#endif  // ERRORESTIMATE_COMMON_H
