// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ERRORESTIMATE_FWD_H
#define ERRORESTIMATE_FWD_H


namespace SANS
{
// Forward declares so that the Algebraic Eq Sets don't just recompile

template< class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class ParamFieldType >
class ErrorEstimate_StrongForm_Galerkin;

template< class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class ParamFieldType >
class ErrorEstimateNodal_Galerkin;

template< class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class ParamFieldType >
class ErrorEstimateNodal_Galerkin_LiftedBoundary;

template< class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class ParamFieldType >
class ErrorEstimate_VMSD;

template< class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class ParamFieldType >
class ErrorEstimate_VMSD_Linear;

template< class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class ParamFieldType >
class ErrorEstimate_VMSD_BR2;

template< class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class ParamFieldType >
class ErrorEstimate_Galerkin_Stabilized;

template< class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class ParamFieldType >
class OutputCorrection_Galerkin_Stabilized;

template< class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector, class ParamFieldType >
class ErrorEstimate_DGBR2;

template< class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class ParamFieldType >
class ErrorEstimate_DGAdvective;

template< class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class XFieldType >
class ErrorEstimate_HDG;

} //namespace SANS

#endif  // ERRORESTIMATE_COMMON_H
