// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Cell integral residual functions

#include <vector> //vector
#include <set> // set

#include "ErrorEstimateNodalBase_Galerkin.h"

#include "tools/KahanSum.h"
#include "tools/abs.h"


#include "Field/Local/XField_LocalPatch.h"

#include "Field/XFieldLine.h"
#include "Field/XFieldArea.h"
#include "Field/XFieldVolume.h"
#include "Field/XFieldSpacetime.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldSpacetime_DG_Cell.h"

#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/FieldSpacetime_DG_BoundaryTrace.h"

#include "Field/FieldLine_CG_Cell.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldVolume_CG_Cell.h"
#include "Field/FieldSpacetime_CG_Cell.h"

#include "Field/FieldLine_CG_BoundaryTrace.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldVolume_CG_BoundaryTrace.h"
#include "Field/FieldSpacetime_CG_BoundaryTrace.h"

#include "Field/tools/for_each_CellGroup.h"

#include "Field/Tuple/FieldTuple.h"
#include "Field/output_Tecplot.h"

#include "ErrorEstimate/ErrorEstimate_Common.h"

#include "Quadrature/Quadrature.h"

#include "tools/make_unique.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#endif

#define FULL_NODAL

namespace SANS
{

//----------------------------------------------------------------------------//
// Check that the primal and adjoint fields are all reasonable

template< class PhysDim, class TopoDim, class ArrayQ >
void
ErrorEstimateNodalBase_Galerkin<PhysDim, TopoDim, ArrayQ>::
init()
{
  ifldCalc_ = false; eSfldCalc_ = false;

  // check the passed in pointer is valid
  if ( up_efld_DG_nodal_ ) // the external field has some data. This means we are doing a local solve/can skip some re-compute!
  {
    SANS_ASSERT_MSG( &up_efld_DG_nodal_->getXField() == &qfld_.getXField(), "The externally created field must be made from the same XField" );

    // The efld is not zeroed out - it has important data that was transfered in from outside
    // We are ASSUMING that whoever is doing this has filled it with useful data.
    // Did you fill it with useful data? Did you REALLY?

    // // Additionally we are going to assert that we are only going to actually calculate the estimate for cell group 0
    // SANS_ASSERT_MSG( cellGroups_.size() == 1, "Should only be calculating the estimate on one cell group");
    // SANS_ASSERT_MSG( cellGroups_[0] == 0, "Should only be calculating the estimate for cell group 0" );

    SANS_ASSERT_MSG( active_BGroup_list_.empty(), "The local estimate constructor can only be used if there's no Lagrange field" );
  }

  // zero the internal fields
  efld_nodal_ = 0;
  if ( !active_BGroup_list_.empty() )
  {
    up_eBfld_nodal_ = SANS::make_unique<Field_CG_BoundaryTrace<PhysDim, TopoDim, Real>>
        (xfld_, 1, BasisFunctionCategory_Lagrange, active_BGroup_list_);

    *up_eBfld_nodal_ = 0;
  }

  check(); // checks that all the fields come from the same grid

}

template< class PhysDim, class TopoDim, class ArrayQ >
void
ErrorEstimateNodalBase_Galerkin<PhysDim, TopoDim, ArrayQ>::
check( )
{
  SANS_ASSERT( &xfld_.getXField() == &qfld_.getXField() ); // check that xfld matches that from constructor
  SANS_ASSERT( &qfld_.getXField() == &lgfld_.getXField() ); // check both were made off the same grid
  SANS_ASSERT( &qfld_.getXField() == &wfld_.getXField() ); // check both were made off the same grid
  SANS_ASSERT( &qfld_.getXField() == &mufld_.getXField() ); // check both were made off the same grid
}

template< class PhysDim, class TopoDim, class ArrayQ >
void
ErrorEstimateNodalBase_Galerkin<PhysDim, TopoDim, ArrayQ>::
getErrorEstimate( Real& estimate, const std::vector<int>& cellGroups )
{
  if (eSfldCalc_ == false)
    calculateSumEstimates();

  KahanSum<Real> Sum=0;
  for (int node = 0; node < up_eSfld_nodal_->nDOFpossessed(); node++)
    Sum += up_eSfld_nodal_->DOF(node);

  estimate = static_cast<Real>(Sum);

#ifdef SANS_MPI
  estimate = boost::mpi::all_reduce(*qfld_.comm(), estimate, std::plus<Real>());
#endif
}

template< class PhysDim, class TopoDim, class ArrayQ >
void ErrorEstimateNodalBase_Galerkin<PhysDim, TopoDim, ArrayQ>::
getErrorEstimate( Real& estimate )
{
  getErrorEstimate( estimate, cellGroups_ );
}

template< class PhysDim, class TopoDim, class ArrayQ >
void ErrorEstimateNodalBase_Galerkin<PhysDim, TopoDim, ArrayQ>::
getLocalSolveErrorEstimate( std::vector<Real>& estimate ){ getLocalSolveErrorIndicator(estimate); } // sums before absolution so just forward it

template< class PhysDim, class TopoDim, class ArrayQ >
void ErrorEstimateNodalBase_Galerkin<PhysDim, TopoDim, ArrayQ>::
getErrorIndicator( Real& estimate, const std::vector<int>& cellGroups )
{
  if (ifldCalc_ == false)
    calculateIndicators();

  KahanSum<Real> Sum=0;
#ifdef FULL_NODAL // the flag of whether to run in purely nodal mode
  for (int dof = 0; dof < up_ifld_nodal_->nDOFpossessed(); dof++)
    Sum += up_ifld_nodal_->DOF(dof);
#else
  int comm_rank = efld_nodal_.comm()->rank();
  for_each_CellGroup<TopoDim>::apply( FieldSum<PhysDim>(Sum,cellGroups,comm_rank,true),efld_nodal_);
#endif
  estimate = static_cast<Real>(Sum);

#ifdef SANS_MPI
  estimate = boost::mpi::all_reduce(*qfld_.comm(), estimate, std::plus<Real>());
#endif
}

template< class PhysDim, class TopoDim, class ArrayQ >
void ErrorEstimateNodalBase_Galerkin<PhysDim, TopoDim, ArrayQ>::
getErrorIndicator( Real& estimate )
{
  getErrorIndicator( estimate, cellGroups_ );
}

template< class PhysDim, class TopoDim, class ArrayQ >
void ErrorEstimateNodalBase_Galerkin<PhysDim, TopoDim, ArrayQ>::
getLocalSolveErrorIndicator( std::vector<Real>& estimate )
{

  // // The sum estimates are used in the local indicator
  // if (eSfldCalc_ == false)
  //   calculateSumEstimates();

  // This is really dirty. Will need to remove this ultimately, but right now people need to run CG in parallel
  // and that will only work with the elemental model that we don't like

#ifdef FULL_NODAL
    // check that this is only getting called from an actual Edge Local Grid
    typedef typename Simplex<TopoDim>::type Topology; // Local Split Patch assumes purely simplicial mesh

    // EDGE MODE ESTIMATION
    std::vector<int> localAttachedNodes, newLinearNodeDOFs;
    // check you're using the right patch
    SANS_ASSERT_MSG( xfld_.derivedTypeID() == typeid(XField_LocalPatch<PhysDim,Topology>), "Not an Edge Local Patch" );

    // get the pointer
    const XField_LocalPatch<PhysDim,Topology>* plocal_xfld = static_cast<const XField_LocalPatch<PhysDim,Topology>*>(&xfld_);

    // check that it is being run in edge mode
    SANS_ASSERT( plocal_xfld->isEdgeMode() );

    localAttachedNodes = plocal_xfld->getLocalLinearCommonNodes();
    newLinearNodeDOFs = plocal_xfld->getNewLinearNodeDOFs();

    // the set of nodes making up the edge or opposite to it
    // these are from the local grid - thus they correspond to dofs in the xfield used to generate pefld.
    SANS_ASSERT_MSG( localAttachedNodes.size() >= 2, "There should be at least two nodes!" );
    SANS_ASSERT_MSG( localAttachedNodes[0] == 0, "The first attached node is the start of the edge" );
    SANS_ASSERT_MSG( localAttachedNodes[1] == 1, "The second attached node is the end of the edge" );

    // This is a local dof, in the indexing of pefld.
    SANS_ASSERT_MSG( newLinearNodeDOFs.size() <= 1,"There should be one new node or no new node!");

    estimate.assign(localAttachedNodes.size(),0);
    // the localNode is the node on the xfld_local
    // there is also a globalNode list that correspond to xfld_linear_
    for (std::size_t node = 0; node < localAttachedNodes.size(); node++)
    {
      const int localNode = localAttachedNodes[node]; // the local node in the grid

      estimate[node] = efld_nodal_.DOF(localNode); // indexed relative to localCommonNodes;

      // The first two nodes make up the edge. Each gets half of the error associated with the new vertex.
      if (node < 2 && newLinearNodeDOFs.size() > 0)
        estimate[node] += 0.5*efld_nodal_.DOF(newLinearNodeDOFs[0]);

      estimate[node] = fabs(estimate[node]);
    }
#else
    // the old elemental version of things. Keeping it for now.... sigh -- Hugh
    // Construct the map of which dofs are included in the `elemental estimate'
    std::vector<std::set<int>> elementDOFSupportMap;
    for_each_CellGroup<TopoDim>::apply( ConstructDOFSet<PhysDim>(elementDOFSupportMap,{0}), &up_eSfld_nodal_ );

    estimate.resize( elementDOFSupportMap.size() );
    for (std::size_t i = 0; i < elementDOFSupportMap.size(); i++ )
    {
      estimate[i] = 0;
      for (auto it = elementDOFSupportMap[i].begin(); it != elementDOFSupportMap[i].end(); ++it) // loop over dofs in macro set
        estimate[i] += efld_nodal_.DOF(*it);
      // std::cout<< "including dof: " << *it << " with value " << up_eSfld_nodal_->DOF(*it) << std::endl;
      estimate[i] = fabs(estimate[i]);
    }
#endif

}

template< class PhysDim, class TopoDim, class ArrayQ >
void
ErrorEstimateNodalBase_Galerkin<PhysDim, TopoDim, ArrayQ>::
fillEArray(std::vector<std::vector<Real>>& estimates ) // fragile interface for Savithru
{

  if (eSfldCalc_ == false)
    calculateSumEstimates();

#ifdef FULL_NODAL
  // All nodal estimates are placed in one large vector
  estimates.resize(1);
  estimates[0].reserve(up_eSfld_nodal_->nDOFpossessed());
  for (int dof = 0; dof < up_eSfld_nodal_->nDOFpossessed(); dof++)
    estimates[0].push_back(fabs(up_eSfld_nodal_->DOF(dof)));
#else
  estimates.resize(xfld_.nCellGroups());
  for (int i = 0; i < xfld_.nCellGroups(); i++ )
    estimates[i].resize(xfld_.getCellGroupBaseGlobal(i).nElem());

   // Populate the vector of vectors from the relevant eSfld_
  for_each_CellGroup<TopoDim>::apply( AccumulateVector<PhysDim>(estimates,cellGroups_),*up_eSfld_nodal_);

  // Do the absolution at the element level, everything having been now accumulated
  for (auto it = estimates.begin(); it != estimates.end(); ++it)
    for (auto itt = it->begin(); itt != it->end(); ++itt)
    {
      *itt = fabs(*itt);
    }
#endif
}

template< class PhysDim, class TopoDim, class ArrayQ >
void
ErrorEstimateNodalBase_Galerkin<PhysDim, TopoDim, ArrayQ>::
outputFields( const std::string& filename ) const
{
  Field_CG_Cell<PhysDim,TopoDim,DLA::VectorS<3,Real>> ofld(xfld_, 1, BasisFunctionCategory_Lagrange);
  SANS_ASSERT( up_eSfld_nodal_->nDOFpossessed() == ofld.nDOFpossessed());
  SANS_ASSERT( up_ifld_nodal_->nDOFpossessed() == ofld.nDOFpossessed());
  SANS_ASSERT( efld_nodal_.nDOFpossessed() == ofld.nDOFpossessed());


  for (int n = 0; n < ofld.nDOFpossessed(); n++)
  {
    ofld.DOF(n)[0] = up_eSfld_nodal_->DOF(n);
    ofld.DOF(n)[1] = up_ifld_nodal_->DOF(n);
    ofld.DOF(n)[2] = efld_nodal_.DOF(n);
  }

  // synchronize the ghost/zombie DOFs for the tecplot dump
  ofld.syncDOFs_MPI_noCache();

  output_Tecplot( ofld, filename, {"eSfld", "ifld", "efld"} );
}


//----------------------------------------------------------------------------//
// Function for calculating the sum estimates
template< class PhysDim, class TopoDim, class ArrayQ >
void
ErrorEstimateNodalBase_Galerkin<PhysDim, TopoDim, ArrayQ>::
calculateSumEstimates()
{
  // for (int dof = 0; dof < efld_nodal_.nDOFpossessed(); dof++)
  //   up_eSfld_nodal_->DOF(dof) = efld_nodal_.DOF(dof);

  up_eSfld_nodal_ = SANS::make_unique<Field_CG_Cell<PhysDim,TopoDim,Real>>(efld_nodal_,FieldCopy());

  // LGFLD UNSUPPORTED RIGHT NOW - Not too important given they're not supported anywhere
  // Need a functor for figuring out which cg field dofs in the volume field correspond to nodes in the lagrange multiplier field.

  // // Distributing the boundary estimate field
  // for_each_BoundaryFieldTraceGroup_Cell<TopoDim>::apply(
  //     DistributeBoundaryTraceField<PhysDim>(active_BGroup_list_), eSfld_nodal_, eBfld_nodal_ ); // Lagrange Fields

  eSfldCalc_ = true; // only place this is changed
}

//----------------------------------------------------------------------------//
// Function for calculating the indicator estimates(
template< class PhysDim, class TopoDim, class ArrayQ >
void
ErrorEstimateNodalBase_Galerkin<PhysDim, TopoDim, ArrayQ>::
calculateIndicators()
{
  up_ifld_nodal_ = SANS::make_unique<Field_CG_Cell<PhysDim,TopoDim,Real>>(efld_nodal_,FieldCopy());

  for (int dof = 0; dof < up_ifld_nodal_->nDOF(); dof++)
    up_ifld_nodal_->DOF(dof) = fabs( up_ifld_nodal_->DOF(dof));

  ifldCalc_ = true; // only place this is changed
}

template class ErrorEstimateNodalBase_Galerkin<PhysD1, TopoD1, Real>;
template class ErrorEstimateNodalBase_Galerkin<PhysD1, TopoD1, DLA::VectorS<3,Real>>;

template class ErrorEstimateNodalBase_Galerkin<PhysD2, TopoD2, Real>;
template class ErrorEstimateNodalBase_Galerkin<PhysD2, TopoD2, DLA::VectorS<4,Real>>;
template class ErrorEstimateNodalBase_Galerkin<PhysD2, TopoD2, DLA::VectorS<5,Real>>;
template class ErrorEstimateNodalBase_Galerkin<PhysD2, TopoD2, DLA::VectorS<6,Real>>;

template class ErrorEstimateNodalBase_Galerkin<PhysD3, TopoD3, Real>;
template class ErrorEstimateNodalBase_Galerkin<PhysD3, TopoD3, DLA::VectorS<5,Real>>;
template class ErrorEstimateNodalBase_Galerkin<PhysD3, TopoD3, DLA::VectorS<6,Real>>;
template class ErrorEstimateNodalBase_Galerkin<PhysD3, TopoD3, DLA::VectorS<7,Real>>;

template class ErrorEstimateNodalBase_Galerkin<PhysD4, TopoD4, Real>;
//template class ErrorEstimateNodalBase_Galerkin<PhysD4, TopoD4, DLA::VectorS<5,Real>>;
//template class ErrorEstimateNodalBase_Galerkin<PhysD4, TopoD4, DLA::VectorS<6,Real>>;
//template class ErrorEstimateNodalBase_Galerkin<PhysD4, TopoD4, DLA::VectorS<7,Real>>;
//template class ErrorEstimateNodalBase_Galerkin<PhysD4, TopoD4, DLA::VectorS<8,Real>>;
} //namespace SANS
