// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ERRORESTIMATECELL_PRIMALSTRONGFORM_H
#define ERRORESTIMATECELL_PRIMALSTRONGFORM_H

// Cell weighted integrals

#include <memory> //unique_ptr

//#include "Topology/ElementTopology.h"
#include "Field/Field.h"
// #include "Field/Element/ElementIntegral.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"
#include "Field/Tuple/FieldTuple.h"

#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Field Cell group weighted residual for Galerkin
//
// topology specific group weighted residual
//
// template parameters:
//   Topology                          element topology (e.g. Triangle)
//   Integrand                         integrand functor
//   XFieldType                        Grid data type, possibly a tuple with parameters
//   QFieldCellGroupTypeTuple          Inputs required by Field Weighted Integrands and efld
//
//   Efld gets removed from the Tuple and the QW Tuple gets passed on to the integrand

//----------------------------------------------------------------------------//
//  Error Estimate cell group weighted integral
//

template<class IntegrandCell >
class ErrorEstimateCell_PrimalStrongForm_Galerkin_impl :
    public GroupIntegralCellType< ErrorEstimateCell_PrimalStrongForm_Galerkin_impl<IntegrandCell> >
{
public:
  typedef typename IntegrandCell::PhysDim PhysDim;
  typedef typename IntegrandCell::template ArrayQ<Real> ArrayQ;

  // Save off the cell integrand
  explicit ErrorEstimateCell_PrimalStrongForm_Galerkin_impl( const IntegrandCell& fcn) :
    fcn_(fcn) {}

  std::size_t nCellGroups() const { return fcn_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcn_.cellGroup(n); }

//----------------------------------------------------------------------------//
// Check that q, w and e flds have corresponding dof
  template <class TopoDim>
  void check( const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>,
                                                   Field<PhysDim,TopoDim,Real>>::type& flds ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);
    const Field<PhysDim, TopoDim, Real>& efld   = get<1>(flds);

    SANS_ASSERT( qfld.nElem() == efld.nElem() ); // same number of elems
    SANS_ASSERT( &qfld.getXField() == &efld.getXField() ); // check both were made off the same grid
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobal,
             const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
             const typename MakeTuple< FieldTuple,
                                       Field<PhysDim,TopoDim,ArrayQ>,
                                       Field<PhysDim,TopoDim,Real>>::type
                              ::template FieldCellGroupType<Topology>& flds,
             const int quadratureorder )
  {
    typedef typename XFieldType::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field<PhysDim,TopoDim,ArrayQ>::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename Field<PhysDim,TopoDim,Real>::template FieldCellGroupType<Topology> EFieldCellGroupType;

    // separating off Q, W and casting away the const on Efield
    const QFieldCellGroupType& qfldCell = get<0>(flds);
    EFieldCellGroupType& efldCell = const_cast<EFieldCellGroupType&>(get<1>(flds));

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;
    typedef typename EFieldCellGroupType::template ElementType<> ElementEFieldClass;

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );
    ElementQFieldClass wfldElem( 0, BasisFunctionCategory_Legendre ); // This is going to be set to 1
    ElementEFieldClass efldElem( efldCell.basis() );

    //Checking it's a P0 field
    SANS_ASSERT_MSG( efldElem.order() == 0, "Primal StrongForm estimates should only be done elementwise" );

    // element integral
    GalerkinWeightedIntegral<TopoDim, Topology, Real> integral(quadratureorder,1);

    // just to make sure things are consistent
    SANS_ASSERT( xfldCell.nElem() == efldCell.nElem() );

    wfldElem.DOF(0) = 0; // set the constant mode to 0

    Real rsdElem[1] = {0}; // know the length is 1

    // loop over elements within group
    const int nelem = xfldCell.nElem();
    const int nEqn = IntegrandCell::N;
    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid/solution DOFs to element
      xfldCell.getElement( xfldElem, elem );
      qfldCell.getElement( qfldElem, elem );
      efldCell.getElement( efldElem, elem );

      ArrayQ rsd = 0;

      // loop over pde equations
      for (int n = 0; n < nEqn; n++)
      {

        DLA::index(wfldElem.DOF(0),n) += 1;
        rsdElem[0] = 0;

        // cell integration for canonical element
        integral( fcn_.integrand( xfldElem, qfldElem, wfldElem, efldElem ), get<-1>(xfldElem), rsdElem, 1 );
        DLA::index(rsd,n) = rsdElem[0];

        DLA::index(wfldElem.DOF(0),n) -= 1;
      }

      // PDE
      efldElem.DOF(0) += dot(rsd,rsd); // so as not to overwrite
      efldCell.setElement( efldElem, elem );

    }
  }

protected:
  const IntegrandCell& fcn_;
};

// Factory function

template<class IntegrandCell>
ErrorEstimateCell_PrimalStrongForm_Galerkin_impl<IntegrandCell>
ErrorEstimateCell_PrimalStrongForm_Galerkin( const IntegrandCellType<IntegrandCell>& fcn )
{
  return ErrorEstimateCell_PrimalStrongForm_Galerkin_impl<IntegrandCell>( fcn.cast() );
}


} //namespace SANS

#endif  // ErrorEstimateCell_PrimalStrongForm_Galerkin_H
