// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef OUTPUTCORRECTION_GALERKIN_STABILIZED_H
#define OUTPUTCORRECTION_GALERKIN_STABILIZED_H

// Cell integral residual functions

#include <memory> // shared_ptr
#include <vector>

#include "Discretization/Galerkin/IntegrandCell_Galerkin_Stabilized_Corrected.h"
#include "tools/Tuple.h"

#include "pde/BCParameters.h"

#include "Field/XField.h"
#include "Field/Field.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

#include "Discretization/QuadratureOrder.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"

#include "Discretization/Galerkin/IntegrateBoundaryTrace_Dispatch_Galerkin.h"

// Estimation classes
#include "ErrorEstimateNodalBase_Galerkin.h"
#include "ErrorEstimateCell_Galerkin.h"
#include "ErrorEstimateBoundaryTrace_Dispatch_Galerkin.h"

#include "ErrorEstimateBoundaryTrace_Dirichlet_mitLG_Galerkin.h"
#include "ErrorEstimateBoundaryTrace_FieldTrace_Galerkin.h"
#include "ErrorEstimateBoundaryTrace_Galerkin.h"

// Main Integrands
#include "Discretization/Galerkin/IntegrandBoundaryTrace_None_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Dirichlet_sansLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_mitLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_sansLG_Galerkin.h"

#include "Discretization/Galerkin/FieldBundle_Galerkin.h"
#include "Discretization/Galerkin/FieldBundle_Galerkin_LiftedBoundary.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Output Correction for Stabilized Galerkin
//----------------------------------------------------------------------------//

template< class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class ParamFieldType >
class OutputCorrection_Galerkin_Stabilized :
    public ErrorEstimateNodalBase_Galerkin<typename NDPDEClass::PhysDim, typename ParamFieldType::TopoDim, typename NDPDEClass::template ArrayQ<Real>>
{
public:
  typedef typename NDPDEClass::PhysDim PhysDim;
  typedef typename ParamFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef ErrorEstimateNodalBase_Galerkin<PhysDim, TopoDim, ArrayQ> BaseType;

  typedef BCParameters<BCVector> BCParams;
  typedef IntegrandCell_Galerkin_Stabilized_Corrected<NDPDEClass> IntegrandCellClass;
  // typedef IntegrandInteriorTrace_Galerkin<NDPDEClass> IntegrandTraceClass;
  typedef IntegrateBoundaryTrace_Dispatch_Galerkin<NDPDEClass, BCNDConvert, BCVector, Galerkin> IntegrateBoundaryTrace_Dispatch;
  //TODO: disc tag to be updated

  typedef FieldBundleBase_Galerkin<PhysDim,TopoDim,ArrayQ> FieldBundle;
  typedef FieldBundleBase_Galerkin_LiftedBoundary<PhysDim,TopoDim,ArrayQ> FieldBundleLifted;

  std::vector<int> BCAccumulate(const std::map< std::string, std::vector<int> >& BCBoundaryGroups )
  {
    std::vector<int> BoundaryGroups;
    for (auto it = BCBoundaryGroups.begin(); it != BCBoundaryGroups.end(); ++it) // loop over map
      for (auto itt = (it->second).begin(); itt != (it->second).end(); ++itt) // loop over vector at current key
        BoundaryGroups.push_back(*itt);
    return BoundaryGroups;
  }

  // Constructor
  template<class... BCArgs>
  OutputCorrection_Galerkin_Stabilized( const ParamFieldType& paramfld,
                          const Field<PhysDim, TopoDim, ArrayQ>& qfld, // q
                          const Field<PhysDim, TopoDim, ArrayQ>& lgfld, // lg
                          const Field<PhysDim, TopoDim, ArrayQ>& wfld, // w (p adjoint!)
                          const Field<PhysDim, TopoDim, ArrayQ>& mufld, // mu
                          std::shared_ptr<Field<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                          const NDPDEClass& pde,
                          const StabilizationMatrix& stab,
                          const QuadratureOrder& quadratureOrder,
                          const std::vector<int>& CellGroups,
                          const std::vector<int>& InteriorTraceGroups,
                          PyDict& BCList,
                          const std::map< std::string, std::vector<int> >& BCBoundaryGroups, BCArgs&... args)
                            : BaseType(get<-1>(paramfld), qfld, lgfld, wfld, mufld, pLiftedQuantityfld, CellGroups,
                                       BCParams::getLGBoundaryGroups(BCList, BCBoundaryGroups)),
                              paramfld_(paramfld),
                              pde_(pde), stab_(stab),
                              quadratureOrder_(quadratureOrder),
                              fcnCell_(pde, CellGroups, stab)
  {
    if ( pde_.hasSourceLiftedQuantity() ) SANS_ASSERT_MSG(this->pLiftedQuantityfld_, "The lifted quantity field has not been initialized!");

    estimate();
  }

  template<class... BCArgs> //NO LIFTED QUANTITY FIELD
  OutputCorrection_Galerkin_Stabilized( const ParamFieldType& paramfld,
                          const Field<PhysDim, TopoDim, ArrayQ>& qfld, // q
                          const Field<PhysDim, TopoDim, ArrayQ>& lgfld, // lg
                          const Field<PhysDim, TopoDim, ArrayQ>& wfld, // w
                          const Field<PhysDim, TopoDim, ArrayQ>& mufld, // mu
                          const NDPDEClass& pde,
                          const StabilizationMatrix& stab,
                          const QuadratureOrder& quadratureOrder,
                          const std::vector<int>& CellGroups,
                          const std::vector<int>& InteriorTraceGroups,
                          PyDict& BCList,
                          const std::map< std::string, std::vector<int> >& BCBoundaryGroups, BCArgs&... args)
: OutputCorrection_Galerkin_Stabilized( paramfld, qfld, lgfld, wfld, mufld, {},
                          pde, stab, quadratureOrder, CellGroups, InteriorTraceGroups, BCList, BCBoundaryGroups, args...) {}

  // Alternative Agnostic Constructor
  // Hardwired to the unstabilized CG for the time being TODO: Get a strong and stable integrand
  // then can turn into OutputCorrection_Galerkin_Stabilized
  template<class... BCArgs>
  OutputCorrection_Galerkin_Stabilized( const ParamFieldType& paramfld,
                          const FieldBundle& primal,   // (q, lg)
                          const FieldBundle& adjoint,  // (w, mu)
                          std::shared_ptr<Field<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                          const NDPDEClass& pde,
                          const StabilizationMatrix& stab,
                          const QuadratureOrder& quadratureOrder,
                          const std::vector<int>& CellGroups,
                          const std::vector<int>& InteriorTraceGroups,
                          PyDict& BCList,
                          const std::map< std::string, std::vector<int> >& BCBoundaryGroups, BCArgs&... args)
: OutputCorrection_Galerkin_Stabilized( paramfld, primal.qfld, primal.lgfld, adjoint.qfld, adjoint.lgfld, pLiftedQuantityfld,
                          pde, stab, quadratureOrder, CellGroups, InteriorTraceGroups, BCList, BCBoundaryGroups, args...)
  {}

  // Alternative Agnostic Constructor
  // Hardwired to the unstabilized CG for the time being TODO: Get a strong and stable integrand
  // then can turn into OutputCorrection_Galerkin_Stabilized
  template<class... BCArgs>
  OutputCorrection_Galerkin_Stabilized( const ParamFieldType& paramfld,
                          const FieldBundleLifted& primal,   // (q, lg)
                          const FieldBundleLifted& adjoint,  // (w, mu)
                          std::shared_ptr<Field<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                          const NDPDEClass& pde,
                          const StabilizationMatrix& stab,
                          const QuadratureOrder& quadratureOrder,
                          const std::vector<int>& CellGroups,
                          const std::vector<int>& InteriorTraceGroups,
                          PyDict& BCList,
                          const std::map< std::string, std::vector<int> >& BCBoundaryGroups, BCArgs&... args)
: OutputCorrection_Galerkin_Stabilized( paramfld, primal.qfld, primal.lgfld, adjoint.qfld, adjoint.lgfld, pLiftedQuantityfld,
                          pde, stab, quadratureOrder, CellGroups, InteriorTraceGroups, BCList, BCBoundaryGroups, args...)
  {}

  std::size_t nCellGroups() const { return fcnCell_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcnCell_.cellGroup(n); }

protected:
  using BaseType::xfld_;

  using BaseType::efld_nodal_;    // q field estimates
  // using BaseType::up_ifld_nodal_;    // indicator field
  // using BaseType::up_eSfld_nodal_;   // estimate sum field
  // using BaseType::up_eBfld_nodal_;

  using BaseType::qfld_; // q
  // using BaseType::lgfld_; // lg
  using BaseType::wfld_; // w
  // using BaseType::mufld_; // mu
  // using BaseType::pLiftedQuantityfld_; //pointer to lifted quantity field

  using BaseType::cellGroups_;

  const ParamFieldType& paramfld_;

  const NDPDEClass& pde_;
  const StabilizationMatrix& stab_;
  const QuadratureOrder quadratureOrder_;

  IntegrandCellClass fcnCell_;

  //----------------------------------------------------------------------------//
  // Estimate function that populates the efld_ and eBfld_, called in constructor
  void
  estimate( )
  {
    efld_nodal_ = 0;

    // Cell Estimates
    IntegrateCellGroups<TopoDim>::integrate(ErrorEstimateCell_Galerkin(fcnCell_),
                                            paramfld_, (qfld_, wfld_, efld_nodal_),
                                            quadratureOrder_.cellOrders.data(),
                                            quadratureOrder_.cellOrders.size()  );

  }
};

} //namespace SANS

#endif  // ERRORESTIMATENODAL_GALERKIN_STABILIZED_H
