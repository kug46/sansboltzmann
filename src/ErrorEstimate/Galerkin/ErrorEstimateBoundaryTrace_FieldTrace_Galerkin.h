// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ERRORESTIMATEBOUNDARYTRACE_MITLG_GALERKIN_H
#define ERRORESTIMATEBOUNDARYTRACE_MITLG_GALERKIN_H

// boundary-trace integral residual functions

#include <memory>     // std::unique_ptr

#include "Topology/ElementTopology.h"
#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
// #include "Field/Element/ElementIntegral.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  Galerkin boundary-trace integral
//

template<class IntegrandBoundaryTrace>
class ErrorEstimateBoundaryTrace_FieldTrace_Galerkin_impl :
    public GroupIntegralBoundaryTraceType< ErrorEstimateBoundaryTrace_FieldTrace_Galerkin_impl<IntegrandBoundaryTrace> >
{
public:
  typedef typename IntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename IntegrandBoundaryTrace::template ArrayQ<Real> ArrayQ;

  // Save off the boundary trace integrand and the residual vectors
  explicit ErrorEstimateBoundaryTrace_FieldTrace_Galerkin_impl( const IntegrandBoundaryTrace& fcn, const int accSign = 1) :
    fcn_(fcn), accSign_(accSign) {}

  std::size_t nBoundaryGroups() const { return fcn_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcn_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
  // A dummy function so IntegrateGroups doesn't get upset
  // Could do with some sort of useful test in here maybe
  template < class TopoDim >
  void check( const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>,
                                                   Field<PhysDim,TopoDim,ArrayQ>,
                                                   Field<PhysDim,TopoDim,Real>>::type& flds,
              const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>,
                                                   Field<PhysDim,TopoDim,ArrayQ>,
                                                   Field<PhysDim,TopoDim,Real>>::type& fldsTrace ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);
    const Field<PhysDim, TopoDim, ArrayQ>& wfld = get<1>(flds);
    const Field<PhysDim, TopoDim, Real>& efld = get<2>(flds);

    SANS_ASSERT( qfld.nElem() == wfld.nElem() ); // same number of elems
    SANS_ASSERT( qfld.nElem() == efld.nElem() ); // same number of elems
    SANS_ASSERT( &qfld.getXField() == &wfld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &efld.getXField() ); // check both were made off the same grid

    const Field<PhysDim, TopoDim, ArrayQ>& lgfld = get<0>(fldsTrace);
    const Field<PhysDim, TopoDim, ArrayQ>& mufld = get<1>(fldsTrace);
    const Field<PhysDim, TopoDim, Real>& eBfld = get<2>(fldsTrace);

    SANS_ASSERT( lgfld.nElem() == mufld.nElem() ); // same number of elems
    SANS_ASSERT( lgfld.nElem() == eBfld.nElem() ); // same number of elems
    SANS_ASSERT( &lgfld.getXField() == &mufld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &lgfld.getXField() == &eBfld.getXField() ); // check both were made off the same grid

    SANS_ASSERT( &qfld.getXField() == &lgfld.getXField() );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class Topology, class TopoDim, class XFieldType>
  void
  integrate(
            const int cellGroupGlobalL,const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
            const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>,
                                                 Field<PhysDim,TopoDim,ArrayQ>,
                                                 Field<PhysDim,TopoDim,Real>>::type
                                      ::template FieldCellGroupType<Topology>& fldsCell,
            const int traceGroupGlobal,
            const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
            const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>,
                                                 Field<PhysDim,TopoDim,ArrayQ>,
                                                 Field<PhysDim,TopoDim,Real>>::type
                                                 ::template FieldTraceGroupType<TopologyTrace>& fldsTrace,
            int quadratureorder )
  {
    typedef typename XFieldType                     ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename Field<PhysDim, TopoDim, Real  >::template FieldCellGroupType<Topology> EFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldCellClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldCellClass;
    typedef typename EFieldCellGroupType::template ElementType<> ElementEFieldCellClass;

    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;
    typedef typename Field<PhysDim, TopoDim, Real  >::template FieldTraceGroupType<TopologyTrace> EFieldTraceGroupType;

    typedef typename QFieldTraceGroupType::template ElementType<> ElementQFieldTraceClass;
    typedef typename EFieldTraceGroupType::template ElementType<> ElementEFieldTraceClass;

    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    // separating off Q, W and casting away the const on Efield
    const QFieldCellGroupType& qfldCell = get<0>(fldsCell);
    const QFieldCellGroupType& wfldCell = get<1>(fldsCell);
    EFieldCellGroupType& efldCell = const_cast<EFieldCellGroupType&>(get<2>(fldsCell));

    // separating off QW and casting away the const on Efield
    const QFieldTraceGroupType& lgfldTrace = get<0>(fldsTrace);
    const QFieldTraceGroupType& mufldTrace = get<1>(fldsTrace);
    EFieldTraceGroupType& efldTrace = const_cast<EFieldTraceGroupType&>(get<2>(fldsTrace));

    // element field variables
    ElementXFieldCellClass xfldElem( xfldCell.basis() );
    ElementQFieldCellClass qfldElem( qfldCell.basis() );
    ElementQFieldCellClass wfldElem( wfldCell.basis() );
    ElementEFieldCellClass efldElem( efldCell.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );
    ElementQFieldTraceClass lgfldElemTrace( lgfldTrace.basis() );
    ElementQFieldTraceClass mufldElemTrace( mufldTrace.basis() );
    ElementEFieldTraceClass efldElemTrace( efldTrace.basis() );

    //Checking it's a p0 or p1 field, assumed for now given they're the only partitions of unity
    SANS_ASSERT( efldElem.order() == 0 || efldElem.order() == 1 );
    SANS_ASSERT( efldElemTrace.order() == 0 || efldElemTrace.order() == 1 );

    const int nIntegrandCell = efldElem.nDOF(), nIntegrandTrace = efldElemTrace.nDOF();

    // trace element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, Real, Real> integral(quadratureorder,nIntegrandCell,nIntegrandTrace);

    std::vector<Real> estPDEElem(nIntegrandCell), estBCTrace(nIntegrandTrace);

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      const int elemCell = xfldTrace.getElementLeft( elem );
      CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // copy global grid/solution DOFs to element
      xfldCell.getElement( xfldElem, elemCell );
      qfldCell.getElement( qfldElem, elemCell );
      wfldCell.getElement( wfldElem, elemCell );
      efldCell.getElement( efldElem, elemCell );

      xfldTrace.getElement(   xfldElemTrace, elem );
      lgfldTrace.getElement( lgfldElemTrace, elem );
      mufldTrace.getElement( mufldElemTrace, elem );
      efldTrace.getElement(   efldElemTrace, elem );

      for (int k = 0; k < nIntegrandCell; k++)
        estPDEElem[k] = 0;

      for (int k = 0; k < nIntegrandTrace; k++)
        estBCTrace[k] = 0;

      integral( fcn_.integrand(xfldElemTrace, canonicalTraceL,
                               xfldElem,
                               qfldElem, wfldElem, efldElem,
                               lgfldElemTrace, mufldElemTrace, efldElemTrace),
                get<-1>(xfldElemTrace), estPDEElem.data(), nIntegrandCell,
                                        estBCTrace.data(), nIntegrandTrace );

      for (int k = 0; k < nIntegrandCell; k++)
        efldElem.DOF(k) += accSign_*estPDEElem[k]; // so as not to overwrite

      for (int k = 0; k < nIntegrandTrace; k++)
        efldElemTrace.DOF(k) += accSign_*estBCTrace[k]; // so as not to overwrite

      efldCell.setElement( efldElem, elemCell ); // the Error associated with the cell weighting
      efldTrace.setElement( efldElemTrace, elem ); // the Error associated with the lagrange weighting
    }
  }

protected:
  const IntegrandBoundaryTrace& fcn_;
  const int accSign_;
};

// Factory function

template<class IntegrandBoundaryTrace>
ErrorEstimateBoundaryTrace_FieldTrace_Galerkin_impl<IntegrandBoundaryTrace>
ErrorEstimateBoundaryTrace_FieldTrace_Galerkin( const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn, const int accSign = 1)
{
  return ErrorEstimateBoundaryTrace_FieldTrace_Galerkin_impl<IntegrandBoundaryTrace>(fcn.cast(), accSign);
}


}

#endif  // ERRORESTIMATEBOUNDARYTRACE_MITLG_GALERKIN_H
