// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ERRORESTIMATEBOUNDARYTRACE_DISPATCH_GALERKIN_H
#define ERRORESTIMATEBOUNDARYTRACE_DISPATCH_GALERKIN_H

// boundary-trace integral residual functions

#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"
#include "ErrorEstimateBoundaryTrace_FieldTrace_Galerkin.h"
#include "ErrorEstimateBoundaryTrace_Galerkin.h"
#include "ErrorEstimateBoundaryTrace_Galerkin_LiftedBoundary.h"

namespace SANS
{

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's with Lagrange multipliers
//
//---------------------------------------------------------------------------//
template< class XFieldType, class PhysDim, class TopoDim, class ArrayQ >
class ErrorEstimateBoundaryTrace_FieldTrace_Dispatch_Galerkin_impl
{
public:
  // accSign_ is used in the accumulation est += accSign*estElem
  ErrorEstimateBoundaryTrace_FieldTrace_Dispatch_Galerkin_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& wfld,
      Field<PhysDim, TopoDim, Real>& efld,
      const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
      const Field<PhysDim, TopoDim, ArrayQ>& mufld,
      std::unique_ptr<Field<PhysDim, TopoDim, Real>>& up_eBfld,
      const int* quadratureorder, int ngroup,
      const int accSign)
    : xfld_(xfld),
      qfld_(qfld), wfld_(wfld), efld_(efld),
      lgfld_(lgfld), mufld_(mufld), up_eBfld_(up_eBfld),
      quadratureorder_(quadratureorder), ngroup_(ngroup),
      accSign_(accSign)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    if (up_eBfld_) // don't need to bother if its a null
    {
      IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
          ErrorEstimateBoundaryTrace_FieldTrace_Galerkin(fcn, accSign_),
          xfld_,
          (qfld_,wfld_,efld_),
          (lgfld_,mufld_,*(up_eBfld_.get())), quadratureorder_, ngroup_ );
    }
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& wfld_;
  Field<PhysDim, TopoDim, Real>& efld_;
  const Field<PhysDim, TopoDim, ArrayQ>& lgfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& mufld_;
  std::unique_ptr<Field<PhysDim, TopoDim, Real>>& up_eBfld_;
  const int* quadratureorder_;
  const int ngroup_;
  const int accSign_;
};

// Factory function

template< class XFieldType, class PhysDim, class TopoDim, class ArrayQ >
ErrorEstimateBoundaryTrace_FieldTrace_Dispatch_Galerkin_impl<XFieldType, PhysDim, TopoDim, ArrayQ>
ErrorEstimateBoundaryTrace_FieldTrace_Dispatch_Galerkin( const XFieldType& xfld,
                                                         const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                                         const Field<PhysDim, TopoDim, ArrayQ>& wfld,
                                                         Field<PhysDim, TopoDim, Real>& efld,
                                                         const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                                                         const Field<PhysDim, TopoDim, ArrayQ>& mufld,
                                                         std::unique_ptr<Field<PhysDim, TopoDim, Real>>& up_eBfld,
                                                         const int* quadratureorder, int ngroup,
                                                         const int accSign = 1)
{
  return ErrorEstimateBoundaryTrace_FieldTrace_Dispatch_Galerkin_impl<XFieldType, PhysDim, TopoDim,ArrayQ>(
      xfld,
      qfld,wfld,efld,
      lgfld,mufld,up_eBfld,
      quadratureorder, ngroup,
      accSign );
}

#if 1
//---------------------------------------------------------------------------//
//
// Dispatch class for BC's without Lagrange multipliers
//
//---------------------------------------------------------------------------//
template< class XFieldType, class PhysDim, class TopoDim, class ArrayQ>
class ErrorEstimateBoundaryTrace_Dispatch_Galerkin_impl
{
public:
  ErrorEstimateBoundaryTrace_Dispatch_Galerkin_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& wfld,
      Field<PhysDim, TopoDim, Real>& efld,
      const int* quadratureorder, int ngroup,
      const int accSign)
    : xfld_(xfld),
      qfld_(qfld), wfld_(wfld), efld_(efld),
      quadratureorder_(quadratureorder), ngroup_(ngroup),
      accSign_(accSign)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups<TopoDim>::integrate(
        ErrorEstimateBoundaryTrace_Galerkin(fcn, accSign_),
        xfld_,
        (qfld_,wfld_,efld_),
        quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& wfld_;
  Field<PhysDim, TopoDim, Real>& efld_;
  const int* quadratureorder_;
  const int ngroup_;
  const int accSign_;
};

// Factory function

template< class XFieldType, class PhysDim, class TopoDim, class ArrayQ >
ErrorEstimateBoundaryTrace_Dispatch_Galerkin_impl<XFieldType, PhysDim, TopoDim, ArrayQ>
ErrorEstimateBoundaryTrace_Dispatch_Galerkin( const XFieldType& xfld,
                                              const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                              const Field<PhysDim, TopoDim, ArrayQ>& wfld,
                                              Field<PhysDim, TopoDim, Real>& efld,
                                              const int* quadratureorder, int ngroup,
                                              const int accSign = 1)
{
  return ErrorEstimateBoundaryTrace_Dispatch_Galerkin_impl<XFieldType, PhysDim, TopoDim,ArrayQ>(
      xfld,
      qfld,wfld,efld,
      quadratureorder, ngroup, accSign);
}

#endif


#if 1
//---------------------------------------------------------------------------//
//
// Dispatch class for BC's without Lagrange multipliers
//
//---------------------------------------------------------------------------//
template< class XFieldType, class PhysDim, class TopoDim, class ArrayQ>
class ErrorEstimateBoundaryTrace_Dispatch_Galerkin_LiftedBoundary_impl
{
public:
  ErrorEstimateBoundaryTrace_Dispatch_Galerkin_LiftedBoundary_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const FieldLift<PhysDim, TopoDim, DLA::VectorS<PhysDim::D,ArrayQ>>& rfld,
      const Field<PhysDim, TopoDim, ArrayQ>& wfld,
      const FieldLift<PhysDim, TopoDim, DLA::VectorS<PhysDim::D,ArrayQ>>& sfld,
      Field<PhysDim, TopoDim, Real>& efld,
      const int* quadratureorder, int ngroup,
      const int accSign)
    : xfld_(xfld),
      qfld_(qfld), rfld_(rfld),  wfld_(wfld), sfld_(sfld), efld_(efld),
      quadratureorder_(quadratureorder), ngroup_(ngroup),
      accSign_(accSign)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups<TopoDim>::integrate(
        ErrorEstimateBoundaryTrace_Galerkin_LiftedBoundary(fcn, accSign_),
        xfld_,
        (qfld_,rfld_,wfld_,sfld_,efld_),
        quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const FieldLift<PhysDim, TopoDim, DLA::VectorS<PhysDim::D,ArrayQ>>& rfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& wfld_;
  const FieldLift<PhysDim, TopoDim, DLA::VectorS<PhysDim::D,ArrayQ>>& sfld_;
  Field<PhysDim, TopoDim, Real>& efld_;
  const int* quadratureorder_;
  const int ngroup_;
  const int accSign_;
};

// Factory function

template< class XFieldType, class PhysDim, class TopoDim, class ArrayQ >
ErrorEstimateBoundaryTrace_Dispatch_Galerkin_LiftedBoundary_impl<XFieldType, PhysDim, TopoDim, ArrayQ>
ErrorEstimateBoundaryTrace_Dispatch_Galerkin_LiftedBoundary( const XFieldType& xfld,
                                              const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                              const FieldLift<PhysDim, TopoDim, DLA::VectorS<PhysDim::D,ArrayQ>>& rfld,
                                              const Field<PhysDim, TopoDim, ArrayQ>& wfld,
                                              const FieldLift<PhysDim, TopoDim, DLA::VectorS<PhysDim::D,ArrayQ>>& sfld,
                                              Field<PhysDim, TopoDim, Real>& efld,
                                              const int* quadratureorder, int ngroup,
                                              const int accSign = 1)
{
  return ErrorEstimateBoundaryTrace_Dispatch_Galerkin_LiftedBoundary_impl<XFieldType, PhysDim, TopoDim,ArrayQ>(
      xfld,
      qfld, rfld,  wfld, sfld, efld,
      quadratureorder, ngroup, accSign);
}

#endif


#if 1
//---------------------------------------------------------------------------//
//
// Dispatch class for ghost boundary group. Assumes only one ghost boundary group
//
//---------------------------------------------------------------------------//
template< class XFieldType, class PhysDim, class TopoDim, class ArrayQ>
class ErrorEstimateBoundaryTrace_Ghost_Dispatch_Galerkin_impl
{
public:
  ErrorEstimateBoundaryTrace_Ghost_Dispatch_Galerkin_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& wfld,
      Field<PhysDim, TopoDim, Real>& efld,
      const int quadratureorder,
      const int accSign)
    : xfld_(xfld),
      qfld_(qfld), wfld_(wfld), efld_(efld),
      quadratureorder_(quadratureorder),
      accSign_(accSign)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups<TopoDim>::integrate_ghost(
        ErrorEstimateBoundaryTrace_Galerkin(fcn, accSign_),
        xfld_,
        (qfld_,wfld_,efld_),
        quadratureorder_);
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& wfld_;
  Field<PhysDim, TopoDim, Real>& efld_;
  const int quadratureorder_;
  const int ngroup_;
  const int accSign_;
};

// Factory function

template< class XFieldType, class PhysDim, class TopoDim, class ArrayQ >
ErrorEstimateBoundaryTrace_Ghost_Dispatch_Galerkin_impl<XFieldType, PhysDim, TopoDim, ArrayQ>
ErrorEstimateBoundaryTrace_Ghost_Dispatch_Galerkin( const XFieldType& xfld,
                                              const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                              const Field<PhysDim, TopoDim, ArrayQ>& wfld,
                                              Field<PhysDim, TopoDim, Real>& efld,
                                              const int quadratureorder,
                                              const int accSign = 1)
{
  return ErrorEstimateBoundaryTrace_Ghost_Dispatch_Galerkin_impl<XFieldType, PhysDim, TopoDim,ArrayQ>(
      xfld,
      qfld,wfld,efld,
      quadratureorder, accSign);
}

#endif

}
#endif //ERRORESTIMATEBOUNDARYTRACE_GALERKIN_H
