// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ERRORESTIMATENODAL_GALERKIN_LIFTEDBOUNDARY_H
#define ERRORESTIMATENODAL_GALERKIN_LIFTEDBOUNDARY_H

// Cell integral residual functions

#include <memory> // shared_ptr
#include <vector>

#include "tools/Tuple.h"

#include "pde/BCParameters.h"

#include "Field/XField.h"
#include "Field/Field.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

#include "Discretization/QuadratureOrder.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"

#include "Discretization/Galerkin/IntegrateBoundaryTrace_Dispatch_Galerkin.h"

#include "Discretization/DG/IntegrateBoundaryTrace_Dispatch_DGBR2.h"
#include "Discretization/DG/DiscretizationDGBR2.h"

// Estimation classes
#include "ErrorEstimateNodalBase_Galerkin.h"
#include "ErrorEstimateCell_Galerkin.h"
#include "ErrorEstimateCell_LO.h"
// #include "ErrorEstimateInteriorTrace_Galerkin.h"
#include "ErrorEstimateBoundaryTrace_Dispatch_Galerkin.h"

#include "ErrorEstimateBoundaryTrace_Dirichlet_mitLG_Galerkin.h"
#include "ErrorEstimateBoundaryTrace_FieldTrace_Galerkin.h"
#include "ErrorEstimateBoundaryTrace_Galerkin.h"

// Main Integrands
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Stabilized.h"
// #include "Discretization/Galerkin/IntegrandInteriorTrace_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_None_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Dirichlet_sansLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_mitLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_sansLG_Galerkin.h"

#include "Discretization/DG/IntegrandCell_DGBR2.h"

#include "Discretization/Galerkin/FieldBundle_Galerkin_LiftedBoundary.h"

#include "tools/make_unique.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Field weighted Estimates for Galerkin
// Combines Cell, interior trace and boundary trace integrals together
//----------------------------------------------------------------------------//

template< class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class ParamFieldType >
class ErrorEstimateNodal_Galerkin_LiftedBoundary :
    public ErrorEstimateNodalBase_Galerkin<typename NDPDEClass::PhysDim, typename ParamFieldType::TopoDim, typename NDPDEClass::template ArrayQ<Real>>
{
public:
  typedef typename NDPDEClass::PhysDim PhysDim;
  typedef typename ParamFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef DLA::VectorS<PhysDim::D, ArrayQ> VectorArrayQ;

  typedef ErrorEstimateNodalBase_Galerkin<PhysDim, TopoDim, ArrayQ> BaseType;

  typedef BCParameters<BCVector> BCParams;
  typedef IntegrandCell_Galerkin_Stabilized<NDPDEClass> IntegrandCellClass;
  typedef IntegrandCell_DGBR2<NDPDEClass> IntegrandCellDGClass;

  typedef IntegrateBoundaryTrace_Dispatch_DGBR2<NDPDEClass, BCNDConvert, BCVector, DGBR2> IntegrateBoundaryTrace_Dispatch;
//  typedef IntegrateBoundaryTrace_Dispatch_Galerkin<NDPDEClass, BCNDConvert, BCVector, Galerkin> IntegrateBoundaryTrace_Dispatch;

  //TODO: disc tag to be updated

  typedef FieldBundleBase_Galerkin_LiftedBoundary<PhysDim,TopoDim,ArrayQ> FieldBundle;

  std::vector<int> BCAccumulate(const std::map< std::string, std::vector<int> >& BCBoundaryGroups )
  {
    std::vector<int> BoundaryGroups;
    for (auto it = BCBoundaryGroups.begin(); it != BCBoundaryGroups.end(); ++it) // loop over map
      for (auto itt = (it->second).begin(); itt != (it->second).end(); ++itt) // loop over vector at current key
        BoundaryGroups.push_back(*itt);
    return BoundaryGroups;
  }

  // // For viewing a forcing function as a solution function. Can then project onto a field.
  // template< class NDPDEClass_ >
  // class ForcingWrapper
  // {
  // public:
  //   typedef typename NDPDEClass_::PhysDim PhysDim;
  //
  //   template <class Z>
  //   using ArrayQ = typename NDPDEClass_::template ArrayQ<Z>;
  //
  //   ForcingWrapper( const NDPDEClass_& pde ) : pde_(pde) {}
  //
  //   void operator() ( const typename NDPDEClass_::VectorX& x, ArrayQ<Real>& forcing) const
  //   {
  //     pde_.forcingFunction( x, forcing );
  //   }
  //
  //   ArrayQ<Real> operator() ( const typename NDPDEClass_::VectorX& x ) const
  //   {
  //     ArrayQ<Real> forcing=0;
  //
  //     pde_.forcingFunction( x, forcing );
  //
  //     return forcing;
  //   }
  //
  //   const NDPDEClass_& pde_;
  // };

  // Constructor
  template<class... BCArgs>
  ErrorEstimateNodal_Galerkin_LiftedBoundary( const ParamFieldType& paramfld,
                          const Field<PhysDim, TopoDim, ArrayQ>& qfld, // q
                          const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld, // r
                          const Field<PhysDim, TopoDim, ArrayQ>& lgfld, // lg
                          const Field<PhysDim, TopoDim, ArrayQ>& wfld, // w
                          const FieldLift<PhysDim, TopoDim, VectorArrayQ>& sfld, // r
                          const Field<PhysDim, TopoDim, ArrayQ>& mufld, // mu
                          std::shared_ptr<Field<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                          const NDPDEClass& pde,
                          const StabilizationMatrix& stab,
                          const QuadratureOrder& quadratureOrder,
                          const std::vector<int>& CellGroups,
                          const std::vector<int>& InteriorTraceGroups,
                          PyDict& BCList,
                          const std::map< std::string, std::vector<int> >& BCBoundaryGroups, BCArgs&... args)
                            : BaseType(get<-1>(paramfld), qfld, lgfld, wfld, mufld, pLiftedQuantityfld, CellGroups,
                                       BCParams::getLGBoundaryGroups(BCList, BCBoundaryGroups)),
                              rfld_(rfld), sfld_(sfld),
                              paramfld_(paramfld),
                              pde_(pde), stab_(stab),
                              quadratureOrder_(quadratureOrder),
                              fcnCell_(pde, CellGroups, stab),
                              BCs_(BCParams::template createBCs<BCNDConvert>(pde, BCList, args...)),
                              disc_(0, stab.getNitscheConstant(1)),
                              fcnCellDG_(pde, disc_, CellGroups),
                              dispatchBC_(pde, BCList, BCs_, BCBoundaryGroups, disc_)
  {
    if ( pde_.hasSourceLiftedQuantity() ) SANS_ASSERT_MSG(this->pLiftedQuantityfld_, "The lifted quantity field has not been initialized!");

    estimate();
  }

  template<class... BCArgs>
  ErrorEstimateNodal_Galerkin_LiftedBoundary( const ParamFieldType& paramfld,
                          const Field<PhysDim, TopoDim, ArrayQ>& qfld, // q
                          const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld, // r
                          const Field<PhysDim, TopoDim, ArrayQ>& lgfld, // lg
                          const Field<PhysDim, TopoDim, ArrayQ>& wfld, // w
                          const FieldLift<PhysDim, TopoDim, VectorArrayQ>& sfld, // s
                          const Field<PhysDim, TopoDim, ArrayQ>& mufld, // mu
                          const NDPDEClass& pde,
                          const StabilizationMatrix& stab,
                          const QuadratureOrder& quadratureOrder,
                          const std::vector<int>& CellGroups,
                          const std::vector<int>& InteriorTraceGroups,
                          PyDict& BCList,
                          const std::map< std::string, std::vector<int> >& BCBoundaryGroups, BCArgs&... args)
: ErrorEstimateNodal_Galerkin_LiftedBoundary( paramfld, qfld,  rfld, lgfld, wfld, sfld, mufld, {},
                          pde, stab, quadratureOrder, CellGroups, InteriorTraceGroups, BCList, BCBoundaryGroups, args...) {}

  // Alternative Agnostic Constructor
  template<class... BCArgs>
  ErrorEstimateNodal_Galerkin_LiftedBoundary( const ParamFieldType& paramfld,
                          const FieldBundle& primal,   // (q, lg)
                          const FieldBundle& adjoint,  // (w, mu)
                          std::shared_ptr<Field<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                          const NDPDEClass& pde,
                          const StabilizationMatrix& stab,
                          const QuadratureOrder& quadratureOrder,
                          const std::vector<int>& CellGroups,
                          const std::vector<int>& InteriorTraceGroups,
                          PyDict& BCList,
                          const std::map< std::string, std::vector<int> >& BCBoundaryGroups, BCArgs&... args)
: ErrorEstimateNodal_Galerkin_LiftedBoundary( paramfld,
                                              primal.qfld, primal.rfld, primal.lgfld,
                                              adjoint.qfld, adjoint.rfld, adjoint.lgfld, pLiftedQuantityfld,
                          pde, stab, quadratureOrder, CellGroups, InteriorTraceGroups, BCList, BCBoundaryGroups, args...)
  {}

  std::size_t nCellGroups() const { return fcnCell_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcnCell_.cellGroup(n); }

protected:
  using BaseType::xfld_;

  using BaseType::active_BGroup_list_;

  using BaseType::efld_nodal_;    // q field estimates
  using BaseType::up_ifld_nodal_;    // indicator field
  using BaseType::up_eSfld_nodal_;   // estimate sum field
  using BaseType::up_eBfld_nodal_;

  using BaseType::qfld_; // q
  const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld_; // r
  using BaseType::lgfld_; // lg
  using BaseType::wfld_; // w
  const FieldLift<PhysDim, TopoDim, VectorArrayQ>& sfld_; // r
  using BaseType::mufld_; // mu
  using BaseType::pLiftedQuantityfld_; //pointer to lifted quantity field

#ifdef VOLUME_WEIGHTED
  using BaseType::Kfld_; // element volume field
  using BaseType::Sfld_; // star volume field
  using BaseType::efld_; // q field cell estimate
  using BaseType::ifld_; // indicator cell field
  using BaseType::eSfld_; // estimate sum cell field
#endif

  using BaseType::cellGroups_;

  const ParamFieldType& paramfld_;

  const NDPDEClass& pde_;
  const StabilizationMatrix& stab_;
  const QuadratureOrder quadratureOrder_;

  IntegrandCellClass fcnCell_;

  std::map< std::string, std::shared_ptr<BCBase> > BCs_;
  const DiscretizationDGBR2 disc_;
  IntegrandCellDGClass fcnCellDG_;

  IntegrateBoundaryTrace_Dispatch dispatchBC_;

  //----------------------------------------------------------------------------//
  // Estimate function that populates the efld_ and eBfld_, called in constructor
  void
  estimate( )
  {
    /*
     * Compute the Estimates using the adjoint field
     */

     // zero the internal fields
    efld_nodal_ = 0;

    if ( !active_BGroup_list_.empty() )
    {
      up_eBfld_nodal_ = SANS::make_unique<Field_CG_BoundaryTrace<PhysDim, TopoDim, Real>>
          (xfld_, 1, BasisFunctionCategory_Lagrange, active_BGroup_list_);

      *up_eBfld_nodal_ = 0;
    }

    // Cell Estimates
    IntegrateCellGroups<TopoDim>::integrate(ErrorEstimateCell_Galerkin(fcnCell_),
                                            paramfld_, (qfld_, wfld_, efld_nodal_),
                                            quadratureOrder_.cellOrders.data(),
                                            quadratureOrder_.cellOrders.size()  );


    // Cell Estimates
    IntegrateCellGroups<TopoDim>::integrate( ErrorEstimateCell_LO(fcnCellDG_),
                                             paramfld_, (qfld_, rfld_, wfld_, sfld_, efld_nodal_),
                                             quadratureOrder_.cellOrders.data(),
                                             quadratureOrder_.cellOrders.size()  );


    // Boundary Trace Estimates
    dispatchBC_.dispatch(
        ErrorEstimateBoundaryTrace_FieldTrace_Dispatch_Galerkin(  paramfld_,
                                                                  qfld_, wfld_, efld_nodal_,
                                                                  lgfld_,mufld_,up_eBfld_nodal_,
                                                                  quadratureOrder_.boundaryTraceOrders.data(),
                                                                  quadratureOrder_.boundaryTraceOrders.size() ),
        ErrorEstimateBoundaryTrace_Dispatch_Galerkin_LiftedBoundary( paramfld_,
                                                            qfld_, rfld_, wfld_, sfld_, efld_nodal_,
                                                            quadratureOrder_.boundaryTraceOrders.data(),
                                                            quadratureOrder_.boundaryTraceOrders.size()),
        ErrorEstimateBoundaryTrace_Dispatch_Galerkin( paramfld_,
                                                      qfld_, wfld_, efld_nodal_,
                                                      quadratureOrder_.boundaryTraceOrders.data(),
                                                      quadratureOrder_.boundaryTraceOrders.size() )
    );

  // #ifdef VOLUME_WEIGHTED
  //   this->collectNodalEstimates();
  // #endif
  }
};

} //namespace SANS

#endif  // ERRORESTIMATENODAL_GALERKIN_LIFTEDBOUNDARY_H
