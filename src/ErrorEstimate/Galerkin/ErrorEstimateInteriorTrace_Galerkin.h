// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ERRORESTIMATEINTERIORTRACE_GALERKIN_H
#define ERRORESTIMATEINTERIORTRACE_GALERKIN_H

// interior-trace integral residual functions

#include <memory>     // std::unique_ptr

#include "Topology/ElementTopology.h"
#include "Quadrature/Quadrature.h"

#include "Field/XField.h"
#include "Field/Field.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"
#include "Field/Tuple/FieldTuple.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Field Interior Trace group weighted residual - CG
//
// topology specific group weighted residual
//
// template parameters:
//   TopologyTrace                     trace topology (e.g. Line)
//   TopologyL/R                       element topology Left/Right
//   Integrand                         integrand functor
//   XFieldType                        Grid data type, possibly a tuple with parameters
//   QFieldCellGroupTypeTupleL/R       Inputs required by Field Weighted Integrands and efld for L/R
//
//   Efld gets removed from the Tuple and the QW Tuple gets passed on to the integrand


//----------------------------------------------------------------------------//
//  CG interior-trace integral
//

template<class IntegrandInteriorTrace >
class ErrorEstimateInteriorTrace_Galerkin_impl :
    public GroupIntegralInteriorTraceType< ErrorEstimateInteriorTrace_Galerkin_impl<IntegrandInteriorTrace> >
{
public:
  typedef typename IntegrandInteriorTrace::PhysDim PhysDim;
  typedef typename IntegrandInteriorTrace::template ArrayQ<Real> ArrayQ;

  // Save off the boundary trace integrand
  explicit ErrorEstimateInteriorTrace_Galerkin_impl( const IntegrandInteriorTrace& fcn) :
    fcn_(fcn) {}

  std::size_t nInteriorTraceGroups() const { return fcn_.nInteriorTraceGroups(); }
  std::size_t interiorTraceGroup(const int n) const { return fcn_.interiorTraceGroup(n); }

  //----------------------------------------------------------------------------//
  // Checking that all the fields are from the same grid
  template <class TopoDim>
  void check( const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>,
                                                   Field<PhysDim,TopoDim,ArrayQ>,
                                                   Field<PhysDim,TopoDim,Real>>::type& flds) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);
    const Field<PhysDim, TopoDim, ArrayQ>& wfld = get<1>(flds);
    const Field<PhysDim, TopoDim, Real>& efld = get<2>(flds);

    SANS_ASSERT( qfld.nElem() == wfld.nElem() ); // same number of elems
    SANS_ASSERT( qfld.nElem() == efld.nElem() ); // same number of elems
    SANS_ASSERT( &qfld.getXField() == &wfld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &efld.getXField() ); // check both were made off the same grid
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the interior trace group
  template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename MakeTuple< FieldTuple,
                                Field<PhysDim,TopoDim,ArrayQ>,
                                Field<PhysDim,TopoDim,ArrayQ>,
                                Field<PhysDim,TopoDim,Real>>::type
                                ::template FieldCellGroupType<TopologyL>& fldsCellL,
      const int cellGroupGlobalR,
      const typename XFieldType::template FieldCellGroupType<TopologyR>& xfldCellR,
      const typename MakeTuple< FieldTuple,
                                Field<PhysDim,TopoDim,ArrayQ>,
                                Field<PhysDim,TopoDim,ArrayQ>,
                                Field<PhysDim,TopoDim,Real>>::type
                                ::template FieldCellGroupType<TopologyR>& fldsCellR,
      const int traceGroupGlobal,
      const typename XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      int quadratureorder )
  {
    typedef typename XFieldType                     ::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, Real  >::template FieldCellGroupType<TopologyL> EFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;
    typedef typename EFieldCellGroupTypeL::template ElementType<> ElementEFieldClassL;

    typedef typename XFieldType                     ::template FieldCellGroupType<TopologyR> XFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, Real  >::template FieldCellGroupType<TopologyR> EFieldCellGroupTypeR;

    typedef typename XFieldCellGroupTypeR::template ElementType<> ElementXFieldClassR;
    typedef typename QFieldCellGroupTypeR::template ElementType<> ElementQFieldClassR;
    typedef typename EFieldCellGroupTypeR::template ElementType<> ElementEFieldClassR;

    typedef typename XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    // separating off Q, W and casting away the const on Efield
    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const QFieldCellGroupTypeL& wfldCellL = get<1>(fldsCellL);
    EFieldCellGroupTypeL& efldCellL = const_cast<EFieldCellGroupTypeL&>(get<2>(fldsCellL));

    // separating off Q, W and casting away the const on Efield
    const QFieldCellGroupTypeR& qfldCellR = get<0>(fldsCellR);
    const QFieldCellGroupTypeR& wfldCellR = get<1>(fldsCellR);
    EFieldCellGroupTypeR& efldCellR = const_cast<EFieldCellGroupTypeR&>(get<2>(fldsCellR));

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementQFieldClassL wfldElemL( wfldCellL.basis() );
    ElementEFieldClassL efldElemL( efldCellL.basis() );

    ElementXFieldClassR xfldElemR( xfldCellR.basis() );
    ElementQFieldClassR qfldElemR( qfldCellR.basis() );
    ElementQFieldClassR wfldElemR( wfldCellR.basis() );
    ElementEFieldClassR efldElemR( efldCellR.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    //Checking they're a P0 fields
    SANS_ASSERT( efldElemL.order() == 0 || efldElemL.order() == 1);
    SANS_ASSERT( efldElemR.order() == 0 || efldElemR.order() == 1);

    const int nIntegrandL = efldElemL.nDOF(), nIntegrandR = efldElemR.nDOF();

    // estimate arrays
    std::vector<Real> estPDEElemL( nIntegrandL ), estPDEElemR( nIntegrandR );

    // trace element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, Real, Real> integral(quadratureorder,nIntegrandL,nIntegrandR);

    // loop over trace elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      const int elemL = xfldTrace.getElementLeft( elem );
      const int elemR = xfldTrace.getElementRight( elem );
      CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
      CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );
      wfldCellL.getElement( wfldElemL, elemL );
      efldCellL.getElement( efldElemL, elemL );

      xfldCellR.getElement( xfldElemR, elemR );
      qfldCellR.getElement( qfldElemR, elemR );
      wfldCellR.getElement( wfldElemR, elemR );
      efldCellR.getElement( efldElemR, elemR );

      xfldTrace.getElement( xfldElemTrace, elem );

      for (int k = 0; k < nIntegrandL; k++)
        estPDEElemL[k] = 0;

      for (int k = 0; k < nIntegrandR; k++)
        estPDEElemR[k] = 0;

      // integral needs to return two Real that get allocated to elements afterwards
      integral(
          fcn_.integrand( xfldElemTrace, canonicalTraceL, canonicalTraceR,
                          xfldElemL,
                          qfldElemL, wfldElemL, efldElemL,
                          xfldElemR,
                          qfldElemR, wfldElemR, efldElemR),
                          xfldElemTrace, estPDEElemL.data(), nIntegrandL,
                                         estPDEElemR.data(), nIntegrandR );

      for (int k = 0; k < nIntegrandL; k++)
        efldElemL.DOF(k) += estPDEElemL[k];

      for (int k = 0; k < nIntegrandR; k++)
        efldElemR.DOF(k) += estPDEElemR[k];

      efldCellL.setElement( efldElemL, elemL );
      efldCellR.setElement( efldElemR, elemR );
    }
  }

protected:
  const IntegrandInteriorTrace& fcn_;
};

// Factory function

template<class IntegrandInteriorTrace>
ErrorEstimateInteriorTrace_Galerkin_impl<IntegrandInteriorTrace>
ErrorEstimateInteriorTrace_Galerkin( const IntegrandInteriorTraceType<IntegrandInteriorTrace>& fcn)
{
  return ErrorEstimateInteriorTrace_Galerkin_impl<IntegrandInteriorTrace>(fcn.cast());
}

} //namespace SANS

#endif  // ERRORESTIMATEINTERIORTRACE_H
