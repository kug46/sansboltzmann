// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ERRORESTIMATEBASE_GALERKIN_H
#define ERRORESTIMATEBASE_GALERKIN_H

// Cell integral residual functions

#include <vector> //vector

#include "tools/Tuple.h"
#include "tools/KahanSum.h"

#include "Field/XField.h"
#include "Field/Field.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/tools/for_each_BoundaryFieldTraceGroup_Cell.h"

#include "ErrorEstimate/ErrorEstimate_Common.h"

#include "tools/abs.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Field weighted Estimates for Galerkin
// Combines Cell, interior trace and boundary trace integrals together
//
//----------------------------------------------------------------------------//
//

// Forward declare of XField_Local_Base
template<class PhysDim, class TopoDim>
class XField_Local_Base;


template< class PhysDim, class TopoDim, class ArrayQ >
class ErrorEstimateBase_Galerkin
{
public:

  // Constructor
  template<class... BCArgs>
  ErrorEstimateBase_Galerkin( const XField<PhysDim, TopoDim>& xfld,
                              const Field<PhysDim, TopoDim, ArrayQ>& qfld, // q
                              const Field<PhysDim, TopoDim, ArrayQ>& lgfld, // lg
                              const Field<PhysDim, TopoDim, ArrayQ>& wfld, // w
                              const Field<PhysDim, TopoDim, ArrayQ>& mufld, // mu
                              std::shared_ptr<Field<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                              const std::vector<int>& CellGroups,
                              const std::vector<int>& active_BGroup_list)
                              : xfld_(xfld),
                                active_BGroup_list_(active_BGroup_list),
                                efld_ (xfld_, 0, BasisFunctionCategory_Legendre),
                                ifld_ (xfld_, 0, BasisFunctionCategory_Legendre),
                                eSfld_ (xfld_, 0, BasisFunctionCategory_Legendre),
//                                oscfld_ (xfld_, 0, BasisFunctionCategory_Legendre),
                                // eBfld_(xfld_, 0, BasisFunctionCategory_Legendre, active_BGroup_list_),
                                qfld_(qfld), lgfld_(lgfld), wfld_(wfld), mufld_(mufld),
                                pLiftedQuantityfld_(pLiftedQuantityfld),
                                cellGroups_(CellGroups)
  {
    ifldCalc_ = false; eSfldCalc_ = false; ifld_ = 0.0; eSfld_ = 0.0;

    check(); // checks that all the fields come from the same grid
  }

  //----------------------------------------------------------------------------//
  // Check that the primal and adjoint fields are all reasonable
  void check( )
  {
    SANS_ASSERT( &xfld_.getXField() == &qfld_.getXField() ); // check that xfld matches that from constructor
    SANS_ASSERT( &qfld_.getXField() == &lgfld_.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld_.getXField() == &wfld_.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld_.getXField() == &mufld_.getXField() ); // check both were made off the same grid
  }

  void
  getErrorEstimate( Real& estimate, const std::vector<int>& cellGroups )
  {
    if (eSfldCalc_ == false)
      calculateSumEstimates();

    int comm_rank = eSfld_.comm()->rank();

    KahanSum<Real> Sum=0;
    for_each_CellGroup<TopoDim>::apply( FieldSum<PhysDim>(Sum,cellGroups,comm_rank),eSfld_ );
    estimate = Sum;
  }

  void
  getErrorEstimate( Real& estimate ) { getErrorEstimate( estimate, cellGroups_ ); }

  void
  getLocalSolveErrorEstimate( std::vector<Real>& estimate )
  {
    if (eSfldCalc_ == false)
      calculateSumEstimates();

    std::vector<std::vector<Real>> dummyVector;
    fillVector(dummyVector, eSfld_, {0});

    SANS_ASSERT( dummyVector[0].size() % 2 == 0);

    int originalSize = dummyVector[0].size()/2;
    estimate.resize( originalSize );
    for (int i = 0; i < originalSize; i++ )
      estimate[i] = dummyVector[0][i] + dummyVector[0][i+originalSize];
  }

  void
  getErrorIndicator( Real& estimate, const std::vector<int>& cellGroups )
  {
    if (ifldCalc_ == false)
      calculateIndicators();

    int comm_rank = eSfld_.comm()->rank();

    // Sum up the total
    KahanSum<Real> Sum=0;
    for_each_CellGroup<TopoDim>::apply( FieldSum<PhysDim>(Sum,cellGroups,comm_rank),ifld_ );
    estimate = (Real)Sum;
  }

  void
  getErrorIndicator( Real& estimate ) { getErrorIndicator( estimate, cellGroups_ );  }

  void
  getLocalSolveErrorIndicator( std::vector<Real>& estimate )
  {

#if 1
    // Need to use the sum estimates, because the absolute value needs to occur after summation
    // This is because interior traces need to be allowed to cancel
    // This is important for CG estimation -- Hugh
    std::vector<std::vector<Real>> dummyVector;
    if ( true )
    {
      if (eSfldCalc_ == false)
        calculateSumEstimates();

      fillVector(dummyVector, eSfld_, {0}); // Assumes all the re-solve error/effected metrics are in group 0
    }
    else
    {
      if (ifldCalc_ == false)
        calculateIndicators();

      fillVector(dummyVector, ifld_, {0}); // Assumes all the re-solve error/effected metrics are in group 0
    }

    SANS_ASSERT( dummyVector[0].size() % 2 == 0); // Assumes being used on a mesh that was split in 2

    int originalSize = dummyVector[0].size()/2;
    estimate.resize( originalSize );
    for (int i = 0; i < originalSize; i++ )
      estimate[i] = ABS(dummyVector[0][i] + dummyVector[0][i+originalSize]);
#else

  // check that this is only getting called from an actual Local XField
  SANS_ASSERT( xfld_.derivedTypeID() == typeid(XField_Local_Base<PhysDim,TopoDim>) );

  std::vector<int> reSolveCellGroups;
  (static_cast<const XField_Local_Base<PhysDim,TopoDim>*>(&xfld_))
  ->getReSolveCellGroups(reSolveCellGroups);

  Real err = 0.0;
  estimate.resize(1); // This only works for elemental modeling strategies
  getErrorEstimate( err, reSolveCellGroups ); // Just calculate the `global' error on the patch
  estimate[0] = ABS(err);
#endif

  }

  // accessor functions
  const XField<PhysDim, TopoDim>& getXField() const { return xfld_; }

  const Field_DG_Cell<PhysDim,TopoDim,Real>& getESField()
  {
    if (eSfldCalc_ == false)
      calculateSumEstimates();

    return eSfld_;
  }
  const Field_DG_Cell<PhysDim,TopoDim,Real>& getIField()
  {
    if (ifldCalc_ == false)
      calculateIndicators();

    return ifld_;
  }

  const Field_DG_Cell<PhysDim,TopoDim,Real>& getEField()             const { return efld_; }
  const Field_DG_BoundaryTrace<PhysDim, TopoDim, Real>& getEBField() const { return *(up_eBfld_.get()); }

  // fragile interface for edge solve - accumulates only group 0
  void fillVector(std::vector<std::vector<Real>>& accVector,
                  const Field_DG_Cell<PhysDim,TopoDim,Real>& accFld,
                  const std::vector<int>& cellGroups )
  {
    // Accumulates a field into the less safe vector of vectors format, useful for communicating with external classes

    accVector.resize(cellGroups.size());
    for (std::size_t i = 0; i < accVector.size(); i++ )
      accVector[i].resize(xfld_.getCellGroupBaseGlobal(cellGroups[i]).nElem());

    // Populate the vector of vectors from ifld_
    for_each_CellGroup<TopoDim>::apply( AccumulateVector<PhysDim>(accVector,cellGroups), accFld );
  }

  void fillEArray(std::vector<std::vector<Real>>& estimates ) // fragile interface for Savithru
  {
    if (ifldCalc_ == false)
      calculateIndicators();

    estimates.resize(xfld_.nCellGroups());
    for (int i = 0; i < xfld_.nCellGroups(); i++ )
      estimates[i].resize(xfld_.getCellGroupBaseGlobal(i).nElem());

    // Populate the vector of vectors from efld_
    for_each_CellGroup<TopoDim>::apply( AccumulateVector<PhysDim>(estimates,cellGroups_),ifld_);
  };

  void outputFields( const std::string& filename ) const
  {
    Field_DG_Cell<PhysDim,TopoDim,DLA::VectorS<3,Real>> ofld_(xfld_, 0, BasisFunctionCategory_Legendre);
    for (int n = 0; n < ofld_.nDOF(); n++)
    {
      ofld_.DOF(n)[0] = eSfld_.DOF(n);
      ofld_.DOF(n)[1] = ifld_.DOF(n);
      ofld_.DOF(n)[2] = efld_.DOF(n);
    }
    output_Tecplot( ofld_, filename, {"eSfld", "ifld", "efld"} );
  }

protected:
  const XField<PhysDim, TopoDim>& xfld_;
  std::vector<int> active_BGroup_list_; //list of BoundaryTraceGroups which have Lagrange multiplier DOFs (mitLG)

  Field_DG_Cell<PhysDim,TopoDim,Real> efld_;    // q field estimates
  Field_DG_Cell<PhysDim,TopoDim,Real> ifld_;    // indicator field
  Field_DG_Cell<PhysDim,TopoDim,Real> eSfld_;   // estimate sum field
//  Field_DG_Cell<PhysDim,TopoDim,Real> oscfld_;  // data oscillation field - the average of the forcing function
  std::unique_ptr<Field<PhysDim, TopoDim, Real>> up_eBfld_;

  const Field<PhysDim, TopoDim, ArrayQ>& qfld_; // q
  const Field<PhysDim, TopoDim, ArrayQ>& lgfld_; // lg
  const Field<PhysDim, TopoDim, ArrayQ>& wfld_; // w
  const Field<PhysDim, TopoDim, ArrayQ>& mufld_; // mu

  std::shared_ptr<Field<PhysDim, TopoDim, Real>> pLiftedQuantityfld_; //pointer to the lifted-quantity field (shock-capturing)

  std::vector<int> cellGroups_;

  bool ifldCalc_;
  bool eSfldCalc_;

  //----------------------------------------------------------------------------//
  // Function for calculating the sum estimates
  void
  calculateSumEstimates()
  {
    // in lieu of a copy constructor
    for (int i = 0; i < efld_.nDOF(); i++)
      eSfld_.DOF(i) = efld_.DOF(i);

    // Distributing the boundary estimate field
    if (up_eBfld_)
    {
      for_each_BoundaryFieldTraceGroup_Cell<TopoDim>::apply(
          DistributeBoundaryTraceField<PhysDim>(active_BGroup_list_), eSfld_, *(up_eBfld_.get()) ); // Lagrange Fields
    }

    eSfldCalc_ = true; // only place this is changed
  }

  //----------------------------------------------------------------------------//
  // Function for calculating the indicator estimates
  void
  calculateIndicators()
  {
    // Flag for using absolutes in the indicators
    // false -> eta = abs(eta_w + eta_lg)
    // true  -> eta = abs(eta_w) + abs(eta_lg)
    const bool absFlag = true;
    ifld_ = 0;

    if (absFlag == true)
      for (int i = 0; i < efld_.nDOF(); i++)
        ifld_.DOF(i) += fabs(efld_.DOF(i));

    if (up_eBfld_)
    {
      for_each_BoundaryFieldTraceGroup_Cell<TopoDim>::apply(
          DistributeBoundaryTraceField<PhysDim>(active_BGroup_list_, absFlag), ifld_, *(up_eBfld_.get()) ); // Lagrange Fields
    }

    if (absFlag == false)
      for (int i = 0; i < efld_.nDOF(); i++)
        ifld_.DOF(i) = fabs(ifld_.DOF(i) + efld_.DOF(i));

    ifldCalc_ = true; // only place this is changed
  }
};

} //namespace SANS

#endif  // ERRORESTIMATEBASE_GALERKIN_H
