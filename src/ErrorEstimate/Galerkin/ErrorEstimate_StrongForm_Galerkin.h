// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ERRORESTIMATE_STRONGFORM_GALERKIN_H
#define ERRORESTIMATE_STRONGFORM_GALERKIN_H

// Cell integral residual functions

#include <memory> // shared_ptr
#include <vector>

#include "tools/Tuple.h"

#include "pde/BCParameters.h"

#include "Field/XField.h"
#include "Field/Field.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

#include "Discretization/QuadratureOrder.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"

#include "Discretization/Galerkin/IntegrateBoundaryTrace_Dispatch_Galerkin.h"

// Estimation classes
#include "ErrorEstimateBase_Galerkin.h"
#include "ErrorEstimateCell_Galerkin.h"
#include "ErrorEstimateInteriorTrace_Galerkin.h"
#include "ErrorEstimateBoundaryTrace_Dispatch_Galerkin.h"

#include "ErrorEstimateBoundaryTrace_Dirichlet_mitLG_Galerkin.h"
#include "ErrorEstimateBoundaryTrace_FieldTrace_Galerkin.h"
#include "ErrorEstimateBoundaryTrace_Galerkin.h"

// Main Integrands
#include "Discretization/Galerkin/IntegrandCell_Galerkin_StrongForm.h"
#include "Discretization/Galerkin/IntegrandInteriorTrace_Galerkin_StrongForm.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_None_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_sansLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_mitLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Dirichlet_sansLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Dirichlet_mitLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Flux_mitState_Nitsche.h"

#include "Discretization/Galerkin/FieldBundle_Galerkin.h"

#include "tools/make_unique.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Field weighted Estimates for Galerkin
// Combines Cell, interior trace and boundary trace integrals together
//----------------------------------------------------------------------------//

template< class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class ParamFieldType >
class ErrorEstimate_StrongForm_Galerkin :
    public ErrorEstimateBase_Galerkin<typename NDPDEClass::PhysDim, typename ParamFieldType::TopoDim, typename NDPDEClass::template ArrayQ<Real>>
{
public:
  typedef typename NDPDEClass::PhysDim PhysDim;
  typedef typename ParamFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef ErrorEstimateBase_Galerkin<PhysDim, TopoDim, ArrayQ> BaseType;

  typedef BCParameters<BCVector> BCParams;
  typedef IntegrandCell_Galerkin_StrongForm<NDPDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_Galerkin_StrongForm<NDPDEClass> IntegrandTraceClass;
  typedef IntegrateBoundaryTrace_Dispatch_Galerkin<NDPDEClass, BCNDConvert, BCVector, Galerkin> IntegrateBoundaryTrace_Dispatch;
  //TODO: disc tag to be updated

  typedef FieldBundleBase_Galerkin<PhysDim,TopoDim,ArrayQ> FieldBundle;

  std::vector<int> BCAccumulate(const std::map< std::string, std::vector<int> >& BCBoundaryGroups )
  {
    std::vector<int> BoundaryGroups;
    for (auto it = BCBoundaryGroups.begin(); it != BCBoundaryGroups.end(); ++it) // loop over map
      for (auto itt = (it->second).begin(); itt != (it->second).end(); ++itt) // loop over vector at current key
        BoundaryGroups.push_back(*itt);
    return BoundaryGroups;
  }

  // For viewing a forcing function as a solution function. Can then project onto a field.
  template< class NDPDEClass_ >
  class ForcingWrapper
  {
  public:
    typedef typename NDPDEClass_::PhysDim PhysDim;

    template <class Z>
    using ArrayQ = typename NDPDEClass_::template ArrayQ<Z>;

    ForcingWrapper( const NDPDEClass_& pde ) : pde_(pde) {}

    void operator() ( const typename NDPDEClass_::VectorX& x, ArrayQ<Real>& forcing) const
    {
      pde_.forcingFunction( x, forcing );
    }

    ArrayQ<Real> operator() ( const typename NDPDEClass_::VectorX& x ) const
    {
      ArrayQ<Real> forcing=0;

      pde_.forcingFunction( x, forcing );

      return forcing;
    }

    const NDPDEClass_& pde_;
  };

  // Constructor
  template<class... BCArgs>
  ErrorEstimate_StrongForm_Galerkin( const ParamFieldType& paramfld,
                          const Field<PhysDim, TopoDim, ArrayQ>& qfld, // q
                          const Field<PhysDim, TopoDim, ArrayQ>& lgfld, // lg
                          const Field<PhysDim, TopoDim, ArrayQ>& wfld, // w
                          const Field<PhysDim, TopoDim, ArrayQ>& mufld, // mu
                          std::shared_ptr<Field<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                          const NDPDEClass& pde,
                          const StabilizationMatrix& stab,
                          const QuadratureOrder& quadratureOrder,
                          const std::vector<int>& CellGroups,
                          const std::vector<int>& InteriorTraceGroups,
                          PyDict& BCList,
                          const std::map< std::string, std::vector<int> >& BCBoundaryGroups, BCArgs&... args)
                            : BaseType(get<-1>(paramfld), qfld, lgfld, wfld, mufld, pLiftedQuantityfld, CellGroups,
                                       BCParams::getLGBoundaryGroups(BCList, BCBoundaryGroups)),
                              paramfld_(paramfld),
                              pde_(pde), stab_(stab),
                              quadratureOrder_(quadratureOrder),
//                              quadratureOrder_(quadratureOrder, {3,0,0} ), // increase the quadrature of the cell integrands to catch oscillation
//                              quadratureOrder_(get<-1>(paramfld), -1),
                              fcnCell_(pde, CellGroups, stab), fcnIntTrace_( pde, InteriorTraceGroups ),
                              BCs_(BCParams::template createBCs<BCNDConvert>(pde, BCList, args...)),
                              dispatchBC_(pde, BCList, BCs_, BCBoundaryGroups, stab)
  {
    if ( pde_.hasSourceLiftedQuantity() ) SANS_ASSERT_MSG(this->pLiftedQuantityfld_, "The lifted quantity field has not been initialized!");

    // Python dict for BC and list of single BC
    PyDict BCNoneList, BCNoneUndo;
    BCNoneUndo[BCParams::params.BC.BCType] = BCParams::params.BC.None;
    BCNoneList["NoneBCUndo"] = BCNoneUndo;

    // Map to all groups
    std::map<std::string, std::vector<int>> BCNoneBoundaryGroups = { {"NoneBCUndo",BCAccumulate(BCBoundaryGroups)}  };

    // BCs
    std::map< std::string, std::shared_ptr<BCBase> > NoneBCs(BCParams::template createBCs<BCNDConvert>(pde,BCNoneList));

    // Dispatcher, where everything is a none bc
    // has to be a pointer because it gets made so late in the constructor, and it doesn't have an empty constructor
    dispatchNoneBC_ = std::make_shared<IntegrateBoundaryTrace_Dispatch>(pde, BCNoneList, NoneBCs, BCNoneBoundaryGroups, stab);


//    // Calculate data oscillation
//    if ( pde_.hasForcingFunction() )
//    {
//      ForcingWrapper wrap(pde_);
//
//      for_each_CellGroup<TopoDim>::apply( ProjectSolnCell_Discontinuous(wrap, CellGroups), (paramfld, oscfld_) );
//    }


    estimate();
  }

  template<class... BCArgs>
  ErrorEstimate_StrongForm_Galerkin( const ParamFieldType& paramfld,
                          const Field<PhysDim, TopoDim, ArrayQ>& qfld, // q
                          const Field<PhysDim, TopoDim, ArrayQ>& lgfld, // lg
                          const Field<PhysDim, TopoDim, ArrayQ>& wfld, // w
                          const Field<PhysDim, TopoDim, ArrayQ>& mufld, // mu
                          const NDPDEClass& pde,
                          const StabilizationMatrix& stab,
                          const QuadratureOrder& quadratureOrder,
                          const std::vector<int>& CellGroups,
                          const std::vector<int>& InteriorTraceGroups,
                          PyDict& BCList,
                          const std::map< std::string, std::vector<int> >& BCBoundaryGroups, BCArgs&... args)
: ErrorEstimate_StrongForm_Galerkin( paramfld, qfld, lgfld, wfld, mufld, {},
                          pde, stab, quadratureOrder, CellGroups, InteriorTraceGroups, BCList, BCBoundaryGroups, args...) {}

  // Alternative Agnostic Constructor
  // Hardwired to the unstabilized CG for the time being TODO: Get a strong and stable integrand
  // then can turn into ErrorEstimate_StrongForm_Galerkin_Stabilized
  template<class... BCArgs>
  ErrorEstimate_StrongForm_Galerkin( const ParamFieldType& paramfld,
                          const FieldBundle& primal,   // (q, lg)
                          const FieldBundle& adjoint,  // (w, mu)
                          std::shared_ptr<Field<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                          const NDPDEClass& pde,
                          const StabilizationMatrix& stab,
                          const QuadratureOrder& quadratureOrder,
                          const std::vector<int>& CellGroups,
                          const std::vector<int>& InteriorTraceGroups,
                          PyDict& BCList,
                          const std::map< std::string, std::vector<int> >& BCBoundaryGroups, BCArgs&... args)
: ErrorEstimate_StrongForm_Galerkin( paramfld, primal.qfld, primal.lgfld, adjoint.qfld, adjoint.lgfld, pLiftedQuantityfld,
                          pde, stab, quadratureOrder, CellGroups, InteriorTraceGroups, BCList, BCBoundaryGroups, args...)
  {}

  std::size_t nCellGroups() const { return fcnCell_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcnCell_.cellGroup(n); }

protected:
  using BaseType::xfld_;

  using BaseType::active_BGroup_list_;

  using BaseType::efld_;    // q field estimates
  // using BaseType::ifld_;    // indicator field
  // using BaseType::eSfld_;   // estimate sum field
//  using BaseType::oscfld_;  // data oscillation field
  using BaseType::up_eBfld_;

  using BaseType::qfld_; // q
  using BaseType::lgfld_; // lg
  using BaseType::wfld_; // w
  using BaseType::mufld_; // mu
  using BaseType::pLiftedQuantityfld_; //pointer to lifted quantity field

  const ParamFieldType& paramfld_;

  const NDPDEClass& pde_;
  const StabilizationMatrix& stab_;
  const QuadratureOrder quadratureOrder_;

  IntegrandCellClass fcnCell_;
  IntegrandTraceClass fcnIntTrace_;

  std::map< std::string, std::shared_ptr<BCBase> > BCs_;
  IntegrateBoundaryTrace_Dispatch dispatchBC_;
  std::shared_ptr<IntegrateBoundaryTrace_Dispatch> dispatchNoneBC_;

  //----------------------------------------------------------------------------//
  // Estimate function that populates the efld_ and eBfld_, called in constructor
  void
  estimate( )
  {
    /*
     * Compute the Estimates using the adjoint field
     */
// zero the internal fields
    efld_ = 0;

    if ( !active_BGroup_list_.empty() )
    {
      up_eBfld_ = SANS::make_unique<Field_DG_BoundaryTrace<PhysDim, TopoDim, Real>>
          (xfld_, 1, BasisFunctionCategory_Lagrange, active_BGroup_list_);

      *(up_eBfld_.get()) = 0;
    }

    // Cell Estimates
    IntegrateCellGroups<TopoDim>::integrate(ErrorEstimateCell_Galerkin(fcnCell_),
                                            paramfld_, (qfld_, wfld_, efld_),
                                            quadratureOrder_.cellOrders.data(),
                                            quadratureOrder_.cellOrders.size()  );
    // Interior Trace Estimates
    IntegrateInteriorTraceGroups<TopoDim>::integrate(ErrorEstimateInteriorTrace_Galerkin(fcnIntTrace_),
                                                     paramfld_,
                                                     (qfld_, wfld_, efld_),
                                                     quadratureOrder_.interiorTraceOrders.data(),
                                                     quadratureOrder_.interiorTraceOrders.size());
//    std::cout << "dispatching the estimate" << std::endl;
    // Boundary Trace Estimates
    dispatchBC_.dispatch(
        ErrorEstimateBoundaryTrace_FieldTrace_Dispatch_Galerkin(  paramfld_,
                                                                  qfld_, wfld_, efld_,
                                                                  lgfld_,mufld_,up_eBfld_,
                                                                  quadratureOrder_.boundaryTraceOrders.data(),
                                                                  quadratureOrder_.boundaryTraceOrders.size()),
        ErrorEstimateBoundaryTrace_Dispatch_Galerkin( paramfld_,
                                                      qfld_, wfld_, efld_,
                                                      quadratureOrder_.boundaryTraceOrders.data(),
                                                      quadratureOrder_.boundaryTraceOrders.size())
    );

    // Need to apply boundary integrand to all groups to remove off the weak form
    // Create a new PyDict with all None bc, and then change the accumulator sign

    // uses pointers because of constructor difficulties

    const int accSign = -1; // so that the noneBC are subtracted off
    dispatchNoneBC_->dispatch(
        ErrorEstimateBoundaryTrace_FieldTrace_Dispatch_Galerkin(  paramfld_,
                                                                  qfld_, wfld_, efld_,
                                                                  lgfld_,mufld_,up_eBfld_,
                                                                  quadratureOrder_.boundaryTraceOrders.data(),
                                                                  quadratureOrder_.boundaryTraceOrders.size(), accSign),
        ErrorEstimateBoundaryTrace_Dispatch_Galerkin( paramfld_,
                                                      qfld_, wfld_, efld_,
                                                      quadratureOrder_.boundaryTraceOrders.data(),
                                                      quadratureOrder_.boundaryTraceOrders.size(), accSign)
    );


//    eBfld_ = 0;
//    std::cout << "finished dispatching the estimate" << std::endl;
  }
};

} //namespace SANS

#endif  // ERRORESTIMATE_STRONGFORM_GALERKIN_H
