// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ERRORESTIMATE_GALERKIN_STABILIZED_H
#define ERRORESTIMATE_GALERKIN_STABILIZED_H

// Cell integral residual functions

#include <memory> // shared_ptr
#include <vector>

#include "tools/Tuple.h"

#include "pde/BCParameters.h"
#include "pde/BCNone.h"

#include "Field/XField.h"
#include "Field/Field.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

#include "Discretization/QuadratureOrder.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"

#include "Discretization/Galerkin/IntegrateBoundaryTrace_Dispatch_Galerkin.h"

// Estimation classes
#include "ErrorEstimateBase_Galerkin.h"
#include "ErrorEstimateCell_Galerkin.h"
#include "ErrorEstimateInteriorTrace_Galerkin.h"
#include "ErrorEstimateBoundaryTrace_Dispatch_Galerkin.h"

#include "ErrorEstimateBoundaryTrace_Dirichlet_mitLG_Galerkin.h"
#include "ErrorEstimateBoundaryTrace_FieldTrace_Galerkin.h"
#include "ErrorEstimateBoundaryTrace_Galerkin.h"

// Main Integrands
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/IntegrandInteriorTrace_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_None_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Dirichlet_sansLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_mitLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_sansLG_Galerkin.h"

#include "Discretization/Galerkin/FieldBundle_Galerkin.h"

#include "tools/make_unique.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Field weighted Estimates for Galerkin
// Combines Cell, interior trace and boundary trace integrals together
//----------------------------------------------------------------------------//

template< class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class ParamFieldType >
class ErrorEstimate_Galerkin_Stabilized :
    public ErrorEstimateBase_Galerkin<typename NDPDEClass::PhysDim, typename ParamFieldType::TopoDim, typename NDPDEClass::template ArrayQ<Real>>
{
public:
  typedef typename NDPDEClass::PhysDim PhysDim;
  typedef typename ParamFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef ErrorEstimateBase_Galerkin<PhysDim, TopoDim, ArrayQ> BaseType;

  typedef BCParameters<BCVector> BCParams;
  typedef IntegrandCell_Galerkin_Stabilized<NDPDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_Galerkin<NDPDEClass> IntegrandTraceClass;
  typedef IntegrateBoundaryTrace_Dispatch_Galerkin<NDPDEClass, BCNDConvert, BCVector, Galerkin> IntegrateBoundaryTrace_Dispatch;
  //TODO: disc tag to be updated

  typedef FieldBundleBase_Galerkin<PhysDim,TopoDim,ArrayQ> FieldBundle;

  std::vector<int> BCAccumulate(const std::map< std::string, std::vector<int> >& BCBoundaryGroups )
  {
    std::vector<int> BoundaryGroups;
    for (auto it = BCBoundaryGroups.begin(); it != BCBoundaryGroups.end(); ++it) // loop over map
      for (auto itt = (it->second).begin(); itt != (it->second).end(); ++itt) // loop over vector at current key
        BoundaryGroups.push_back(*itt);
    return BoundaryGroups;
  }

  // // For viewing a forcing function as a solution function. Can then project onto a field.
  // template< class NDPDEClass_ >
  // class ForcingWrapper
  // {
  // public:
  //   typedef typename NDPDEClass_::PhysDim PhysDim;
  //
  //   template <class Z>
  //   using ArrayQ = typename NDPDEClass_::template ArrayQ<Z>;
  //
  //   ForcingWrapper( const NDPDEClass_& pde ) : pde_(pde) {}
  //
  //   void operator() ( const typename NDPDEClass_::VectorX& x, ArrayQ<Real>& forcing) const { pde_.forcingFunction( x, forcing ); }
  //
  //   ArrayQ<Real> operator() ( const typename NDPDEClass_::VectorX& x ) const
  //   {
  //     ArrayQ<Real> forcing=0;
  //     pde_.forcingFunction( x, forcing );
  //     return forcing;
  //   }
  //
  //   const NDPDEClass_& pde_;
  // };

  // Constructor
  template<class... BCArgs>
  ErrorEstimate_Galerkin_Stabilized( const ParamFieldType& paramfld,
                          const Field<PhysDim, TopoDim, ArrayQ>& qfld, // q
                          const Field<PhysDim, TopoDim, ArrayQ>& lgfld, // lg
                          const Field<PhysDim, TopoDim, ArrayQ>& wfld, // w
                          const Field<PhysDim, TopoDim, ArrayQ>& mufld, // mu
                          std::shared_ptr<Field<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                          const NDPDEClass& pde,
                          const StabilizationMatrix& stab,
                          const QuadratureOrder& quadratureOrder,
                          const std::vector<int>& CellGroups,
                          const std::vector<int>& InteriorTraceGroups,
                          PyDict& BCList,
                          const std::map< std::string, std::vector<int> >& BCBoundaryGroups, BCArgs&... args)
                            : BaseType(get<-1>(paramfld), qfld, lgfld, wfld, mufld, pLiftedQuantityfld, CellGroups,
                                       BCParams::getLGBoundaryGroups(BCList, BCBoundaryGroups)),
                              paramfld_(paramfld),
                              pde_(pde), stab_(stab),
                              quadratureOrder_(quadratureOrder),
                              fcnCell_(pde, CellGroups, stab), fcnIntTrace_( pde, InteriorTraceGroups ),
                              BCs_(BCParams::template createBCs<BCNDConvert>(pde, BCList, args...)),
                              dispatchBC_(pde, BCList, BCs_, BCBoundaryGroups, stab)
  {
    if ( pde_.hasSourceLiftedQuantity() ) SANS_ASSERT_MSG(this->pLiftedQuantityfld_, "The lifted quantity field has not been initialized!");

    estimate();
  }

  template<class... BCArgs>
  ErrorEstimate_Galerkin_Stabilized( const ParamFieldType& paramfld,
                          const Field<PhysDim, TopoDim, ArrayQ>& qfld, // q
                          const Field<PhysDim, TopoDim, ArrayQ>& lgfld, // lg
                          const Field<PhysDim, TopoDim, ArrayQ>& wfld, // w
                          const Field<PhysDim, TopoDim, ArrayQ>& mufld, // mu
                          const NDPDEClass& pde,
                          const StabilizationMatrix& stab,
                          const QuadratureOrder& quadratureOrder,
                          const std::vector<int>& CellGroups,
                          const std::vector<int>& InteriorTraceGroups,
                          PyDict& BCList,
                          const std::map< std::string, std::vector<int> >& BCBoundaryGroups, BCArgs&... args)
: ErrorEstimate_Galerkin_Stabilized( paramfld, qfld, lgfld, wfld, mufld, {},
                          pde, stab, quadratureOrder, CellGroups, InteriorTraceGroups, BCList, BCBoundaryGroups, args...) {}

  // Alternative Agnostic Constructor
  template<class... BCArgs>
  ErrorEstimate_Galerkin_Stabilized( const ParamFieldType& paramfld,
                          const FieldBundle& primal,   // (q, lg)
                          const FieldBundle& adjoint,  // (w, mu)
                          std::shared_ptr<Field<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                          const NDPDEClass& pde,
                          const StabilizationMatrix& stab,
                          const QuadratureOrder& quadratureOrder,
                          const std::vector<int>& CellGroups,
                          const std::vector<int>& InteriorTraceGroups,
                          PyDict& BCList,
                          const std::map< std::string, std::vector<int> >& BCBoundaryGroups, BCArgs&... args)
: ErrorEstimate_Galerkin_Stabilized( paramfld, primal.qfld, primal.lgfld, adjoint.qfld, adjoint.lgfld, pLiftedQuantityfld,
                          pde, stab, quadratureOrder, CellGroups, InteriorTraceGroups, BCList, BCBoundaryGroups, args...)
  {}

  std::size_t nCellGroups() const { return fcnCell_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcnCell_.cellGroup(n); }


  // This is a REDEFINE of the function in the BaseClass. This is quite dangerous, as it avoids vtable lookup
  // This is necessary as vtable lookup would be too expensive in this
  void
  getLocalSolveErrorIndicator( std::vector<Real>& estimate )
  {


    // Do integrations on ghost boundaries to complete the IBP

    /*
    1. Get the NoneBC for the particular PDE
    2. Wrap a BCNone in NDConvert
    3. Construct IntegrandNone with the BC
    3. Call IntegrateBoundaryTraceGroups::integrate_ghost( ... );
    */

    // if ( xfld_.nGhostBoundaryTraceGroups() == 1 ) // there's a ghost boundary
    // {
    //   typedef BCNone<PhysDim,NDPDEClass::N> BCNoneClass;
    //   typedef BCNDConvert<PhysDim,BCNoneClass> NDBCNoneClass;
    //   typedef NDVectorCategory<boost::mpl::vector1<NDBCNoneClass>, typename NDBCNoneClass::Category> NDBCVecCat;
    //   typedef IntegrandBoundaryTrace<NDPDEClass,NDBCVecCat,Galerkin> IntegrandClass;
    //
    //   // none bc for applying on ghost boundaries, completing the IBP
    //   NDBCNoneClass bcNone(true); // include the boundary terms
    //
    //   // Create integrand
    //   IntegrandClass fcn( pde_, bcNone, {0} );
    //
    //   // Loop over ghost boundary trace group !
    //   IntegrateBoundaryTraceGroups<TopoDim>::integrate_ghost( ErrorEstimateBoundaryTrace_Galerkin(fcn),
    //                                                     xfld_, (qfld_,wfld_,efld_),
    //                                                     quadratureOrder_.cellOrders[0] );
    //
    // }


    // check that this is only getting called from an actual Local XField
    SANS_ASSERT( xfld_.derivedTypeID() == typeid(XField_Local_Base<PhysDim,TopoDim>) );

    std::vector<int> reSolveCellGroups;
    (static_cast<const XField_Local_Base<PhysDim,TopoDim>*>(&xfld_))
    ->getReSolveCellGroups(reSolveCellGroups);


    Real err = 0.0;
    estimate.resize(1); // This only works for elemental modeling strategies

    // calculate the sum estimates from efld
    this->calculateSumEstimates();
    this->getErrorEstimate( err, reSolveCellGroups ); // calculate the `global' error on the patch

    estimate[0] = ABS(err);
  }

protected:
  using BaseType::xfld_;

  using BaseType::active_BGroup_list_;

  using BaseType::efld_;    // q field estimates
  using BaseType::ifld_;    // indicator field
  using BaseType::eSfld_;   // estimate sum field
  using BaseType::up_eBfld_;

  using BaseType::qfld_; // q
  using BaseType::lgfld_; // lg
  using BaseType::wfld_; // w
  using BaseType::mufld_; // mu
  using BaseType::pLiftedQuantityfld_; //pointer to lifted quantity field

  using BaseType::cellGroups_;

  using BaseType::eSfldCalc_;
  using BaseType::ifldCalc_;

  const ParamFieldType& paramfld_;

  const NDPDEClass& pde_;
  const StabilizationMatrix& stab_;
  const QuadratureOrder quadratureOrder_;

  IntegrandCellClass fcnCell_;
  IntegrandTraceClass fcnIntTrace_;

  std::map< std::string, std::shared_ptr<BCBase> > BCs_;
  IntegrateBoundaryTrace_Dispatch dispatchBC_;

  //----------------------------------------------------------------------------//
  // Estimate function that populates the efld_ and eBfld_, called in constructor
  void
  estimate( )
  {
    /*
     * Compute the Estimates using the adjoint field
     */

     // zero the internal fields
     efld_ = 0;

     if ( !active_BGroup_list_.empty() )
     {
       up_eBfld_ = SANS::make_unique<Field_CG_BoundaryTrace<PhysDim, TopoDim, Real>>
           (xfld_, 0, BasisFunctionCategory_Legendre, active_BGroup_list_);

       *(up_eBfld_.get()) = 0;
     }


    // Cell Estimates
    IntegrateCellGroups<TopoDim>::integrate(ErrorEstimateCell_Galerkin(fcnCell_),
                                            paramfld_, (qfld_, wfld_, efld_),
                                            quadratureOrder_.cellOrders.data(),
                                            quadratureOrder_.cellOrders.size()  );

    // Interior Trace Estimates
    // This is key for ensuring the localization works! -- Hugh
    IntegrateInteriorTraceGroups<TopoDim>::integrate(ErrorEstimateInteriorTrace_Galerkin(fcnIntTrace_),
                                                     paramfld_,
                                                     (qfld_, wfld_, efld_),
                                                     quadratureOrder_.interiorTraceOrders.data(),
                                                     quadratureOrder_.interiorTraceOrders.size());

//    std::cout << "dispatching the estimate" << std::endl;
    // Boundary Trace Estimates
    dispatchBC_.dispatch(
        ErrorEstimateBoundaryTrace_FieldTrace_Dispatch_Galerkin(  paramfld_,
                                                                  qfld_, wfld_,  efld_,
                                                                  lgfld_,mufld_, up_eBfld_,
                                                                  quadratureOrder_.boundaryTraceOrders.data(),
                                                                  quadratureOrder_.boundaryTraceOrders.size()),
        ErrorEstimateBoundaryTrace_Dispatch_Galerkin( paramfld_,
                                                      qfld_, wfld_, efld_,
                                                      quadratureOrder_.boundaryTraceOrders.data(),
                                                      quadratureOrder_.boundaryTraceOrders.size())
    );

    // get everyone to agree
    efld_.syncDOFs_MPI_noCache();
    if (up_eBfld_) // check not a null ptr
      up_eBfld_->syncDOFs_MPI_noCache();

  }
};

} //namespace SANS

#endif  // ERRORESTIMATE_GALERKIN_STABILIZED_H
