// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ERRORESTIMATECELL_GALERKIN_H
#define ERRORESTIMATECELL_GALERKIN_H

// Cell weighted integrals

#include <memory> //unique_ptr

//#include "Topology/ElementTopology.h"
#include "Field/Field.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"
#include "Field/Tuple/FieldTuple.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

// #include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

namespace SANS
{

/*

//----------------------------------------------------------------------------//
Field Cell group weighted residual for Galerkin

topology specific group weighted residual

template parameters:
  Topology                          element topology (e.g. Triangle)
  Integrand                         integrand functor
  XFieldType                        Grid data type, possibly a tuple with parameters
  QFieldCellGroupTypeTuple          Inputs required by Field Weighted Integrands and efld

  Tuple of QWE gets passed down to integrands which specify efldElem.
  It will then weight by phi w, where phi is the basis used in the efld, and w
  is the adjoint weighting.

//----------------------------------------------------------------------------//
 Error Estimate cell group weighted integral

*/

template<class IntegrandCell >
class ErrorEstimateCell_Galerkin_impl :
    public GroupIntegralCellType< ErrorEstimateCell_Galerkin_impl<IntegrandCell> >
{
public:
  typedef typename IntegrandCell::PhysDim PhysDim;
  typedef typename IntegrandCell::template ArrayQ<Real> ArrayQ;

  // Save off the cell integrand
  explicit ErrorEstimateCell_Galerkin_impl( const IntegrandCell& fcn) :
    fcn_(fcn) {}

  std::size_t nCellGroups() const { return fcn_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcn_.cellGroup(n); }

//----------------------------------------------------------------------------//
// Check that q, w and e flds have corresponding dof
  template <class TopoDim>
  void check( const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>,
                                                   Field<PhysDim,TopoDim,ArrayQ>,
                                                   Field<PhysDim,TopoDim,Real>>::type& flds ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);
    const Field<PhysDim, TopoDim, ArrayQ>& wfld = get<1>(flds);
    const Field<PhysDim, TopoDim, Real>& efld = get<2>(flds);

    SANS_ASSERT( qfld.nElem() == wfld.nElem() ); // same number of elems
    SANS_ASSERT( qfld.nElem() == efld.nElem() ); // same number of elems
    SANS_ASSERT( &qfld.getXField() == &wfld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &efld.getXField() ); // check both were made off the same grid
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobal,
             const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
             const typename MakeTuple< FieldTuple,
                                       Field<PhysDim,TopoDim,ArrayQ>,
                                       Field<PhysDim,TopoDim,ArrayQ>,
                                       Field<PhysDim,TopoDim,Real>>::type
                              ::template FieldCellGroupType<Topology>& flds,
             const int quadratureorder )
  {
    typedef typename XFieldType::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field<PhysDim,TopoDim,ArrayQ>::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename Field<PhysDim,TopoDim,Real>::template FieldCellGroupType<Topology> EFieldCellGroupType;

    // separating off Q, W and casting away the const on Efield
    const QFieldCellGroupType& qfldCell = get<0>(flds);
    const QFieldCellGroupType& wfldCell = get<1>(flds);
    EFieldCellGroupType& efldCell = const_cast<EFieldCellGroupType&>(get<2>(flds));

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;
    typedef typename EFieldCellGroupType::template ElementType<> ElementEFieldClass;

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );
    ElementQFieldClass wfldElem( wfldCell.basis() );
    ElementEFieldClass efldElem( efldCell.basis() );

    //Checking it's a p0 or p1 field, assumed for now given they're the only partitions of unity
    SANS_ASSERT_MSG( efldElem.order() == 0
    || (efldElem.order() == 1  && (efldElem.basis()->category() == BasisFunctionCategory_Hierarchical
                                || efldElem.basis()->category() == BasisFunctionCategory_Lagrange) ),
    "order = %i, category = %i", efldElem.order(), (int)efldElem.basis()->category() );

    const int nIntegrand = efldElem.nDOF();

    // estimate array
    std::vector<Real> estPDEElem( nIntegrand );

    // element integral
    GalerkinWeightedIntegral<TopoDim, Topology, Real> integral(quadratureorder, nIntegrand);

    // just to make sure things are consistent
    SANS_ASSERT( xfldCell.nElem() == efldCell.nElem() );

    auto integrand = fcn_.integrand( xfldElem, qfldElem, wfldElem, efldElem );

    // loop over elements within group
    const int nelem = xfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid/solution DOFs to element
      xfldCell.getElement( xfldElem, elem );
      qfldCell.getElement( qfldElem, elem );
      wfldCell.getElement( wfldElem, elem );
      efldCell.getElement( efldElem, elem );

      for (int n = 0; n < nIntegrand; n++)
        estPDEElem[n] = 0;

      // cell integration for canonical element
      integral( integrand, get<-1>(xfldElem),
          estPDEElem.data(), nIntegrand );

      // PDE
      for (int n = 0; n < nIntegrand; n++)
        efldElem.DOF(n) += estPDEElem[n]; // so as not to overwrite
      efldCell.setElement( efldElem, elem );

    }
  }

protected:
  const IntegrandCell& fcn_;
};

// Factory function

template<class IntegrandCell>
ErrorEstimateCell_Galerkin_impl<IntegrandCell>
ErrorEstimateCell_Galerkin( const IntegrandCellType<IntegrandCell>& fcn )
{
  return ErrorEstimateCell_Galerkin_impl<IntegrandCell>( fcn.cast() );
}


} //namespace SANS

#endif  // ErrorEstimateCell_Galerkin_H
