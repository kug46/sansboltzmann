// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ERRORESTIMATENODAL_GALERKIN_H
#define ERRORESTIMATENODAL_GALERKIN_H

// Cell integral residual functions

#include <memory> // shared_ptr
#include <vector>


#include "pde/BCParameters.h"

#include "Field/XField.h"
#include "Field/Field.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"
#include "Field/Tuple/FieldTuple.h"

#include "Field/tools/for_each_CellGroup.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

#include "Discretization/QuadratureOrder.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"

#include "Discretization/Galerkin/IntegrateBoundaryTrace_Dispatch_Galerkin.h"

// Estimation classes
#include "ErrorEstimateNodalBase_Galerkin.h"
#include "ErrorEstimateCell_Galerkin.h"
#include "ErrorEstimateInteriorTrace_SIP_Galerkin.h"
#include "ErrorEstimateBoundaryTrace_Dispatch_Galerkin.h"

#include "ErrorEstimateBoundaryTrace_Dirichlet_mitLG_Galerkin.h"
#include "ErrorEstimateBoundaryTrace_FieldTrace_Galerkin.h"
#include "ErrorEstimateBoundaryTrace_Galerkin.h"

// Main Integrands
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/IntegrandInteriorTrace_SIP_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_None_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Dirichlet_sansLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_mitLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_sansLG_Galerkin.h"

#include "Discretization/Galerkin/FieldBundle_Galerkin.h"
#include "Discretization/Galerkin/ExtractCGLocalBoundaries.h"

#include "tools/Tuple.h"
#include "tools/make_unique.h"
#include "tools/linspace.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Field weighted Estimates for Galerkin
// Combines Cell, interior trace and boundary trace integrals together
//----------------------------------------------------------------------------//

template< class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class ParamFieldType >
class ErrorEstimateNodal_Galerkin :
    public ErrorEstimateNodalBase_Galerkin<typename NDPDEClass::PhysDim, typename ParamFieldType::TopoDim, typename NDPDEClass::template ArrayQ<Real>>
{
public:
  typedef typename NDPDEClass::PhysDim PhysDim;
  typedef typename ParamFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef ErrorEstimateNodalBase_Galerkin<PhysDim, TopoDim, ArrayQ> BaseType;

  typedef BCParameters<BCVector> BCParams;
  typedef IntegrandCell_Galerkin_Stabilized<NDPDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_SIP_Galerkin<NDPDEClass> IntegrandTraceSIPClass;
  typedef IntegrateBoundaryTrace_Dispatch_Galerkin<NDPDEClass, BCNDConvert, BCVector, Galerkin> IntegrateBoundaryTrace_Dispatch;
  //TODO: disc tag to be updated

  typedef FieldBundleBase_Galerkin<PhysDim,TopoDim,ArrayQ> FieldBundle;

  std::vector<int> BCAccumulate(const std::map< std::string, std::vector<int> >& BCBoundaryGroups )
  {
    std::vector<int> BoundaryGroups;
    for (auto it = BCBoundaryGroups.begin(); it != BCBoundaryGroups.end(); ++it) // loop over map
      for (auto itt = (it->second).begin(); itt != (it->second).end(); ++itt) // loop over vector at current key
        BoundaryGroups.push_back(*itt);
    return BoundaryGroups;
  }

  // Constructor
  template<class... BCArgs>
  ErrorEstimateNodal_Galerkin(const ParamFieldType& paramfld,
                              const Field<PhysDim, TopoDim, ArrayQ>& qfld, // q
                              const Field<PhysDim, TopoDim, ArrayQ>& lgfld, // lg
                              const Field<PhysDim, TopoDim, ArrayQ>& wfld, // w
                              const Field<PhysDim, TopoDim, ArrayQ>& mufld, // mu
                              std::unique_ptr<Field_DG_Cell<PhysDim,TopoDim,Real>>& up_efld_DG_nodal, // reference so base can take control
                              std::shared_ptr<Field<PhysDim,TopoDim,Real>> pLiftedQuantityfld,
                              const NDPDEClass& pde,
                              const StabilizationMatrix& stab,
                              const QuadratureOrder& quadratureOrder,
                              const std::vector<int>& CellGroups,
                              const std::vector<int>& InteriorTraceGroups,
                              PyDict& BCList,
                              const std::map< std::string, std::vector<int> >& BCBoundaryGroups, BCArgs&... args)
  : BaseType(get<-1>(paramfld), qfld, lgfld, wfld, mufld, up_efld_DG_nodal, pLiftedQuantityfld, CellGroups,
             BCParams::getLGBoundaryGroups(BCList, BCBoundaryGroups)),
    paramfld_(paramfld),
    pde_(pde), stab_(stab),
    quadratureOrder_(quadratureOrder),
    fcnCell_(pde, CellGroups, stab),
    fcnSIP_(pde, stab, InteriorTraceGroups ), // find the broken trace! (helper function in ExtractCGLocalBoundaries.h)
    BCs_(BCParams::template createBCs<BCNDConvert>(pde, BCList, args...)),
    dispatchBC_(pde, BCList, BCs_, BCBoundaryGroups, stab)
  {
    if ( pde_.hasSourceLiftedQuantity() ) SANS_ASSERT_MSG(this->pLiftedQuantityfld_, "The lifted quantity field has not been initialized!");

    estimate();
  }

  // Constructor - without pre allocated field, sends to the base class without the up_efld_DG_nodal
  template<class... BCArgs>
  ErrorEstimateNodal_Galerkin(const ParamFieldType& paramfld,
                              const Field<PhysDim, TopoDim, ArrayQ>& qfld, // q
                              const Field<PhysDim, TopoDim, ArrayQ>& lgfld, // lg
                              const Field<PhysDim, TopoDim, ArrayQ>& wfld, // w
                              const Field<PhysDim, TopoDim, ArrayQ>& mufld, // mu
                              const NDPDEClass& pde,
                              const StabilizationMatrix& stab,
                              const QuadratureOrder& quadratureOrder,
                              const std::vector<int>& CellGroups,
                              const std::vector<int>& InteriorTraceGroups,
                              PyDict& BCList,
                              const std::map< std::string, std::vector<int> >& BCBoundaryGroups, BCArgs&... args)
  : BaseType(get<-1>(paramfld), qfld, lgfld, wfld, mufld, nullptr, CellGroups,
             BCParams::getLGBoundaryGroups(BCList, BCBoundaryGroups)),
    paramfld_(paramfld),
    pde_(pde), stab_(stab),
    quadratureOrder_(quadratureOrder),
    fcnCell_(pde, CellGroups, stab),
    fcnSIP_(pde, stab, InteriorTraceGroups ), // find the broken trace! (helper function in ExtractCGLocalBoundaries.h)
    BCs_(BCParams::template createBCs<BCNDConvert>(pde, BCList, args...)),
    dispatchBC_(pde, BCList, BCs_, BCBoundaryGroups, stab)
  {
    SANS_ASSERT_MSG( !pde_.hasSourceLiftedQuantity(), "There is no lifted quantity field!");

    estimate();
  }

  // Alternative Agnostic Constructor
  template<class... BCArgs>
  ErrorEstimateNodal_Galerkin(const ParamFieldType& paramfld,
                              const FieldBundle& primal,   // (q, lg)
                              const FieldBundle& adjoint,  // (w, mu)
                              std::shared_ptr<Field<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                              const NDPDEClass& pde,
                              const StabilizationMatrix& stab,
                              const QuadratureOrder& quadratureOrder,
                              const std::vector<int>& CellGroups,
                              const std::vector<int>& InteriorTraceGroups,
                              PyDict& BCList,
                              const std::map< std::string, std::vector<int> >& BCBoundaryGroups, BCArgs&... args)
  : BaseType(get<-1>(paramfld), primal.qfld, primal.lgfld, adjoint.qfld, adjoint.lgfld, pLiftedQuantityfld, CellGroups,
             BCParams::getLGBoundaryGroups(BCList, BCBoundaryGroups)),
    paramfld_(paramfld),
    pde_(pde), stab_(stab),
    quadratureOrder_(quadratureOrder),
    fcnCell_(pde, CellGroups, stab),
    fcnSIP_(pde, stab, InteriorTraceGroups ), // find the broken trace! (helper function in ExtractCGLocalBoundaries.h)
    BCs_(BCParams::template createBCs<BCNDConvert>(pde, BCList, args...)),
    dispatchBC_(pde, BCList, BCs_, BCBoundaryGroups, stab)
  {
    if ( pde_.hasSourceLiftedQuantity() ) SANS_ASSERT_MSG(this->pLiftedQuantityfld_, "The lifted quantity field has not been initialized!");

    estimate();
  }

// : ErrorEstimateNodal_Galerkin( paramfld, primal, adjoint,
//                                nullptr, pLiftedQuantityfld, // no est field
//                                pde, stab, quadratureOrder, CellGroups, InteriorTraceGroups, BCList, BCBoundaryGroups, args...)
//   {}


  // Alternative Agnostic Constructor -- That allows for pre-computed estimate field
  // up_efld_DG_nodal is passed by reference because in the base class it is going to steal it
  template<class... BCArgs>
  ErrorEstimateNodal_Galerkin(const ParamFieldType& paramfld,
                              const FieldBundle& primal,   // (q, lg)
                              const FieldBundle& adjoint,  // (w, mu)
                              std::unique_ptr<Field_DG_Cell<PhysDim,TopoDim,Real>>& up_efld_DG_nodal, // reference so base can take control
                              std::shared_ptr<Field<PhysDim,TopoDim,Real>> pLiftedQuantityfld,
                              const NDPDEClass& pde,
                              const StabilizationMatrix& stab,
                              const QuadratureOrder& quadratureOrder,
                              const std::vector<int>& CellGroups,
                              const std::vector<int>& InteriorTraceGroups,
                              PyDict& BCList,
                              const std::map< std::string, std::vector<int> >& BCBoundaryGroups, BCArgs&... args)
  : BaseType(get<-1>(paramfld), primal.qfld, primal.lgfld, adjoint.qfld, adjoint.lgfld, up_efld_DG_nodal, pLiftedQuantityfld, CellGroups,
           BCParams::getLGBoundaryGroups(BCList, BCBoundaryGroups)),
    paramfld_(paramfld),
    pde_(pde), stab_(stab),
    quadratureOrder_(quadratureOrder),
    fcnCell_(pde, CellGroups, stab), // only going to evalute on cell group 0!
    fcnSIP_(pde, stab, InteriorTraceGroups ), // find the broken trace! (helper function in ExtractCGLocalBoundaries.h)
    BCs_(BCParams::template createBCs<BCNDConvert>(pde, BCList, args...)),
    dispatchBC_(pde, BCList, BCs_, BCBoundaryGroups, stab)
  {
    if ( pde_.hasSourceLiftedQuantity() ) SANS_ASSERT_MSG(this->pLiftedQuantityfld_, "The lifted quantity field has not been initialized!");

    estimate();
  }

  std::size_t nCellGroups() const { return fcnCell_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcnCell_.cellGroup(n); }

  void getLocalSolveErrorIndicator( std::vector<Real>& estimate )
  {
#if !defined(WHOLEPATCH) && !defined(INNERPATCH)
    estimateLocal();
#endif
    BaseType::getLocalSolveErrorIndicator(estimate);
  }

  void getLocalSolveErrorEstimate( std::vector<Real>& estimate ) { getLocalSolveErrorIndicator(estimate); }

protected:
  using BaseType::xfld_;

  using BaseType::efld_nodal_;    // q field estimates
  using BaseType::up_efld_DG_nodal_; // DG nodal field representation -- can come with precomputed data
  using BaseType::up_ifld_nodal_;    // indicator field
  using BaseType::up_eSfld_nodal_;   // estimate sum field
  using BaseType::up_eBfld_nodal_;

  using BaseType::qfld_; // q
  using BaseType::lgfld_; // lg
  using BaseType::wfld_; // w
  using BaseType::mufld_; // mu
  using BaseType::pLiftedQuantityfld_; //pointer to lifted quantity field

  using BaseType::cellGroups_;

  const ParamFieldType& paramfld_;

  const NDPDEClass& pde_;
  const StabilizationMatrix& stab_;
  const QuadratureOrder quadratureOrder_;

  IntegrandCellClass fcnCell_;
  IntegrandTraceSIPClass fcnSIP_;

  std::map< std::string, std::shared_ptr<BCBase> > BCs_;
  IntegrateBoundaryTrace_Dispatch dispatchBC_;

  //----------------------------------------------------------------------------//
  // Estimate function that populates the efld_ and eBfld_, called in constructor
  void
  estimate()
  {

    if ( up_efld_DG_nodal_ == nullptr ) // if the field wasn't declared then we need to declare it
    {
      // std::cout << "creating group" << std::endl;
      // This is what will happen for general
      up_efld_DG_nodal_ = SANS::make_unique<Field_DG_Cell<PhysDim,TopoDim,Real>>(xfld_, 1, BasisFunctionCategory_Lagrange );
      *up_efld_DG_nodal_ = 0; // zero the data
    }
    else // if the field was declared - that means there was precomputed data.
    {
      // std::cout << "zeroing group" << std::endl;
      // zero out the data in cell group 0 so that it can be filled below
      for_each_CellGroup<TopoDim>::apply( ZeroField<PhysDim,Real>(fcnCell_.cellGroups()), *up_efld_DG_nodal_ );
    }

    // Cell Estimates
    IntegrateCellGroups<TopoDim>::integrate(ErrorEstimateCell_Galerkin(fcnCell_),
                                            paramfld_, (qfld_, wfld_, *up_efld_DG_nodal_ ),
                                            quadratureOrder_.cellOrders.data(),
                                            quadratureOrder_.cellOrders.size()  );


    // Boundary Trace Estimates
    dispatchBC_.dispatch(
        ErrorEstimateBoundaryTrace_FieldTrace_Dispatch_Galerkin(  paramfld_,
                                                                  qfld_, wfld_, *up_efld_DG_nodal_ ,
                                                                  lgfld_,mufld_,up_eBfld_nodal_,
                                                                  quadratureOrder_.boundaryTraceOrders.data(),
                                                                  quadratureOrder_.boundaryTraceOrders.size()),
        ErrorEstimateBoundaryTrace_Dispatch_Galerkin( paramfld_,
                                                      qfld_, wfld_, *up_efld_DG_nodal_ ,
                                                      quadratureOrder_.boundaryTraceOrders.data(),
                                                      quadratureOrder_.boundaryTraceOrders.size() )
    );

    // need to sync here because the efld will then be populated below by accumulating
    up_efld_DG_nodal_->syncDOFs_MPI_noCache();

    // map from the DG dofs to CG dofs
    std::map<int,int> DG_to_CG_DOFMap;

    // fill the map for all dofs that are in the DG field - transfer for all cell groups!
    for_each_CellGroup<TopoDim>::apply(
      PairFieldDOFs<PhysDim,Real>(DG_to_CG_DOFMap,linspace(0,xfld_.nCellGroups()-1)), (*up_efld_DG_nodal_,efld_nodal_) );

    // loop over the DG dofs and accumulate the data into the CG field
    for (const auto& keyVal : DG_to_CG_DOFMap )
      efld_nodal_.DOF(keyVal.second) += up_efld_DG_nodal_->DOF(keyVal.first);

    // We don't need to sync up the efld_nodal, as the DG sync was already done.
  }


  inline void estimateLocal()
  {
    // estimate error associated with added SIP type stabilization
    IntegrateInteriorTraceGroups<TopoDim>::integrate(ErrorEstimateInteriorTrace_SIP_Galerkin(fcnSIP_),
                                                     paramfld_, (qfld_, wfld_, efld_nodal_),
                                                     quadratureOrder_.interiorTraceOrders.data(),
                                                     quadratureOrder_.interiorTraceOrders.size() );

  }


};

} //namespace SANS

#endif  // ERRORESTIMATENODAL_GALERKIN_H
