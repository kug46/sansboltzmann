// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ERRORESTIMATENODALBASE_GALERKIN_H
#define ERRORESTIMATENODALBASE_GALERKIN_H

// Cell integral residual functions

#include <vector> //vector
#include <set> // set

#include "Field/XField.h"
#include "Field/Field.h"
#include "Field/FieldTypes.h"

// #include "Field/tools/for_each_CellGroup.h" // so the constructDOFMap works

// #define VOLUME_WEIGHTED // Turn this on to use the volume weighting in the local error calculation

namespace SANS
{
//----------------------------------------------------------------------------//
// Field weighted Estimates for Galerkin
// Combines Cell, interior trace and boundary trace integrals together
//
//----------------------------------------------------------------------------//
//


template< class PhysDim, class TopoDim, class ArrayQ >
class ErrorEstimateNodalBase_Galerkin
{
public:

  // Constructor where the efield ptr is created internally -- Standard when using this in global solves
  template<class... BCArgs>
  ErrorEstimateNodalBase_Galerkin(const XField<PhysDim, TopoDim>& xfld,
                                  const Field<PhysDim, TopoDim, ArrayQ>& qfld, // q
                                  const Field<PhysDim, TopoDim, ArrayQ>& lgfld, // lg
                                  const Field<PhysDim, TopoDim, ArrayQ>& wfld, // w
                                  const Field<PhysDim, TopoDim, ArrayQ>& mufld, // mu
                                  std::shared_ptr<Field<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                                  const std::vector<int>& CellGroups,
                                  const std::vector<int>& active_BGroup_list)
                                  : xfld_(xfld),
                                    active_BGroup_list_(active_BGroup_list),
                                    efld_nodal_(xfld_, 1, BasisFunctionCategory_Lagrange),
                                    qfld_(qfld), lgfld_(lgfld), wfld_(wfld), mufld_(mufld),
                                    pLiftedQuantityfld_(pLiftedQuantityfld),
                                    cellGroups_(CellGroups)
  {
    init();
  }


  // Alternative constructor where the efield ptr is transfered in from outside -- used for local solves
  template<class... BCArgs>
  ErrorEstimateNodalBase_Galerkin(const XField<PhysDim, TopoDim>& xfld,
                                  const Field<PhysDim, TopoDim, ArrayQ>& qfld, // q
                                  const Field<PhysDim, TopoDim, ArrayQ>& lgfld, // lg
                                  const Field<PhysDim, TopoDim, ArrayQ>& wfld, // w
                                  const Field<PhysDim, TopoDim, ArrayQ>& mufld, // mu
                                  std::unique_ptr<Field_DG_Cell<PhysDim,TopoDim,Real>>& up_efld_DG_nodal, // efld EXTERNALLY created
                                  std::shared_ptr<Field<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                                  const std::vector<int>& CellGroups,
                                  const std::vector<int>& active_BGroup_list)
                                  : xfld_(xfld),
                                    active_BGroup_list_(active_BGroup_list),
                                    efld_nodal_(xfld_, 1, BasisFunctionCategory_Lagrange),
                                    up_efld_DG_nodal_(std::move(up_efld_DG_nodal)), // Take control of the pointer. The heap memory is now owned
                                    qfld_(qfld), lgfld_(lgfld), wfld_(wfld), mufld_(mufld),
                                    pLiftedQuantityfld_(pLiftedQuantityfld),
                                    cellGroups_(CellGroups)
  {
    init();
  }

  //----------------------------------------------------------------------------//
  // Check that the primal and adjoint fields are all reasonable
  void init();
  void check( );

  void getErrorEstimate( Real& estimate, const std::vector<int>& cellGroups );
  void getErrorEstimate( Real& estimate );

  void getLocalSolveErrorEstimate( std::vector<Real>& estimate );

  void getErrorIndicator( Real& estimate, const std::vector<int>& cellGroups );
  void getErrorIndicator( Real& estimate );

  void getLocalSolveErrorIndicator( std::vector<Real>& estimate );

  // accessor functions
  const XField<PhysDim, TopoDim>& getXField() const { return xfld_; }

  const Field<PhysDim,TopoDim,Real>& getESField()
  {
    if (eSfldCalc_ == false)
      calculateSumEstimates();

    return *up_eSfld_nodal_;
  }
  const Field<PhysDim,TopoDim,Real>& getIField()
  {
    if (ifldCalc_ == false)
      calculateIndicators();

    return *up_ifld_nodal_;
  }
  const Field<PhysDim,TopoDim,Real>& getEField() const { return efld_nodal_; }

  // fragile interface for Savithru
  void fillEArray(std::vector<std::vector<Real>>& estimates );

  void outputFields( const std::string& filename ) const;

  // A reference to the data in the internally controlled unique_ptr
  const Field_DG_Cell<PhysDim,TopoDim,Real>& efld_DG_nodal() const
  {
    return *up_efld_DG_nodal_;
  }


protected:
  const XField<PhysDim, TopoDim>& xfld_;
  std::vector<int> active_BGroup_list_; //list of BoundaryTraceGroups which have Lagrange multiplier DOFs (mitLG)

  Field_CG_Cell<PhysDim,TopoDim,Real> efld_nodal_; // q field estimates

  // defining a unique_ptr here so we can optionally take control of memory declared externally
  // this field is used to pass in precomputed error estimates in cell group 1 - which we are assuming are locked
  std::unique_ptr<Field_DG_Cell<PhysDim,TopoDim,Real>> up_efld_DG_nodal_; // q field estimates - can be computed externally

  // use unique_ptr so that they only get created if they're actually needed. Avoids some memory allocation and deallocation
  std::unique_ptr<Field<PhysDim,TopoDim,Real>> up_ifld_nodal_;    // indicator field
  std::unique_ptr<Field<PhysDim,TopoDim,Real>> up_eSfld_nodal_;   // estimate sum field
  std::unique_ptr<Field<PhysDim,TopoDim,Real>> up_eBfld_nodal_;

  const Field<PhysDim, TopoDim, ArrayQ>& qfld_; // q
  const Field<PhysDim, TopoDim, ArrayQ>& lgfld_; // lg
  const Field<PhysDim, TopoDim, ArrayQ>& wfld_; // w
  const Field<PhysDim, TopoDim, ArrayQ>& mufld_; // mu

  std::shared_ptr<Field<PhysDim, TopoDim, Real>> pLiftedQuantityfld_; //pointer to the lifted-quantity field (shock-capturing)

  std::vector<int> cellGroups_;

  bool ifldCalc_;
  bool eSfldCalc_;

  //----------------------------------------------------------------------------//
  // Function for calculating the sum estimates
  void calculateSumEstimates();

  //----------------------------------------------------------------------------//
  // Function for calculating the indicator estimates(
  void calculateIndicators();

};

} //namespace SANS

#endif  // ERRORESTIMATENODALBASE_GALERKIN_H
