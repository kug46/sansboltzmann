// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ERRORESTIMATE_HDG_H
#define ERRORESTIMATE_HDG_H

// Cell integral residual functions

#include <memory> //unique_ptr
#include <vector> //vector

#include "tools/Tuple.h"
#include "tools/KahanSum.h"

#include "pde/BCParameters.h"

#include "Field/XField.h"
#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/tools/for_each_InteriorFieldTraceGroup_Cell.h"
#include "Field/tools/for_each_BoundaryFieldTraceGroup_Cell.h"
#include "Field/XField_CellToTrace.h"

#include "Discretization/QuadratureOrder.h"

#include "Discretization/HDG/DiscretizationHDG.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateInteriorTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"

#include "Discretization/HDG/IntegrateBoundaryTrace_Dispatch_HDG.h"

// Estimation classes
#include "ErrorEstimate/HDG/ErrorEstimateCell_HDG.h"
#include "ErrorEstimate/HDG/ErrorEstimateInteriorTrace_HDG.h"
#include "ErrorEstimate/HDG/ErrorEstimateBoundaryTrace_Dispatch_HDG.h"

#include "ErrorEstimate/HDG/ErrorEstimateBoundaryTrace_HDG.h"
#include "ErrorEstimate/HDG/ErrorEstimateBoundaryTrace_FieldTrace_HDG.h"

// Main Integrands
#include "Discretization/HDG/IntegrandCell_HDG.h"
#include "Discretization/HDG/IntegrandInteriorTrace_HDG.h"

// Boundary Integrands
#include "Discretization/HDG/IntegrandBoundaryTrace_None_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_Dirichlet_sansLG_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_LinearScalar_mitLG_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_LinearScalar_sansLG_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_Flux_mitState_HDG.h"

#include "ErrorEstimate/ErrorEstimate_Common.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Field weighted Estimates for HDG
// Combines Cell, interior trace and boundary trace integrals together
//
// topology specific group weighted residual
//
// template parameters:
//   Topology                          element topology (e.g. Triangle)
//   XFieldType                        Grid data type, possibly a tuple with parameters
//   QFieldCellGroupTypeTuple          Inputs required by Field Weighted Integrands and efld
//
//----------------------------------------------------------------------------//
//

template< class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class XFieldType >
class ErrorEstimate_HDG
{
public:
  typedef typename NDPDEClass::PhysDim PhysDim;
  typedef typename XFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCParameters<BCVector> BCParams;
  typedef IntegrandCell_HDG<NDPDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_HDG<NDPDEClass> IntegrandTraceClass;
  typedef IntegrateBoundaryTrace_Dispatch_HDG<NDPDEClass, BCNDConvert, BCVector, HDG> IntegrateBoundaryTrace_Dispatch;

  // Constructor
  template<class... BCArgs>
  ErrorEstimate_HDG( const XFieldType& xfld,
                     const Field<PhysDim, TopoDim, ArrayQ>& qfld, // q
                     const Field<PhysDim, TopoDim, VectorArrayQ>& afld, // a
                     const Field<PhysDim, TopoDim, ArrayQ>& qIfld, // qI
                     const Field<PhysDim, TopoDim, ArrayQ>& lgfld, // lg
                     const Field<PhysDim, TopoDim, ArrayQ>& wfld, // w
                     const Field<PhysDim, TopoDim, VectorArrayQ>& bfld, // b
                     const Field<PhysDim, TopoDim, ArrayQ>& wIfld, // wI
                     const Field<PhysDim, TopoDim, ArrayQ>& mufld, // mu
                     const NDPDEClass& pde,
                     DiscretizationHDG<NDPDEClass>& disc,
                     const QuadratureOrder& quadratureOrder,
                     const std::vector<int>& CellGroups,
                     const std::vector<int>& InteriorTraceGroups,
                     PyDict& BCList,
                     const std::map< std::string, std::vector<int> >& BCBoundaryGroups, BCArgs&... args)
                     : xfld_(xfld), xfldCellToTrace_(xfld_),
                       active_BGroup_list_(BCParams::getLGBoundaryGroups(BCList, BCBoundaryGroups)),
                       efld_ (xfld_, 0, BasisFunctionCategory_Legendre),
                       ifld_ (xfld_, 0, BasisFunctionCategory_Legendre),
                       eSfld_(xfld_, 0, BasisFunctionCategory_Legendre),
                       eAfld_(xfld_, 0, BasisFunctionCategory_Legendre),
                       eIfld_(xfld_, 0, BasisFunctionCategory_Legendre),
                       eBfld_(xfld_, 0, BasisFunctionCategory_Legendre, active_BGroup_list_),
                       qfld_(qfld), afld_(afld), qIfld_(qIfld), lgfld_(lgfld),
                       wfld_(wfld), bfld_(bfld), wIfld_(wIfld), mufld_(mufld),
                       pde_(pde), quadratureOrder_(quadratureOrder),
                       fcnCell_(pde, disc, CellGroups), fcnIntTrace_( pde, disc, InteriorTraceGroups ),
                       BCs_(BCParams::template createBCs<BCNDConvert>(pde, BCList, args...)), dispatchBC_(pde, BCList, BCs_, BCBoundaryGroups, disc),
                       cellGroups_(CellGroups), interiorTraceGroups_(InteriorTraceGroups)
  {
    // collecting up all boundary trace groups to loop over with eIfld
    for (auto it = BCBoundaryGroups.begin(); it != BCBoundaryGroups.end(); ++it)
      for (auto it2 = it->second.begin(); it2 != it->second.end(); ++it2)
        boundaryTraceGroups_.push_back(*it2);

    ifldCalc_ = false; eSfldCalc_ = false; ifld_ = 0.0; eSfld_ = 0.0;
    check(); // checks that all the fields come from the same grid
    estimate();
  }

  std::size_t nCellGroups() const { return fcnCell_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcnCell_.cellGroup(n); }

  //----------------------------------------------------------------------------//
  // Check that the primal and adjoint fields are all reasonable
  void check( )
  {
    SANS_ASSERT( &xfld_.getXField() == &qfld_.getXField() ); // check that xfld matches that from constructor
    SANS_ASSERT( &qfld_.getXField() == &afld_.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld_.getXField() == &lgfld_.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld_.getXField() == &wfld_.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld_.getXField() == &bfld_.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld_.getXField() == &mufld_.getXField() ); // check both were made off the same grid
  }

  void
  getErrorEstimate( Real& estimate, const std::vector<int>& cellGroups )
  {
    if (eSfldCalc_ == false)
      calculateSumEstimates();

    int comm_rank = eSfld_.comm()->rank();

    // Sum up the total
    KahanSum<Real> Sum=0;
    for_each_CellGroup<TopoDim>::apply( FieldSum<PhysDim>(Sum,cellGroups,comm_rank),eSfld_ );
    estimate = Sum;
  }

  void
  getErrorEstimate( Real& estimate ) { getErrorEstimate( estimate, cellGroups_ ); }

  void
  getLocalSolveErrorEstimate( std::vector<Real>& estimate )
  {
    if (eSfldCalc_ == false)
      calculateSumEstimates();

    std::vector<std::vector<Real>> dummyVector;
    fillVector(dummyVector, eSfld_, {0});

    SANS_ASSERT( dummyVector[0].size() % 2 == 0);

    int originalSize = dummyVector[0].size()/2;
    estimate.resize( originalSize );
    for (int i = 0; i < originalSize; i++ )
      estimate[i] = dummyVector[0][i] + dummyVector[0][i+originalSize];
  }

  void
  getErrorIndicator( Real& estimate, const std::vector<int>& cellGroups )
  {
    if (ifldCalc_ == false)
      calculateIndicators();

    int comm_rank = ifld_.comm()->rank();

    // Sum up the total
    KahanSum<Real> Sum=0;
    for_each_CellGroup<TopoDim>::apply( FieldSum<PhysDim>(Sum,cellGroups,comm_rank),ifld_ );
    estimate = (Real)Sum;
  }

  void
  getErrorIndicator( Real& estimate ) { getErrorIndicator( estimate, cellGroups_ );  }

  void
  getLocalSolveErrorIndicator( std::vector<Real>& estimate )
  {
    if (ifldCalc_ == false)
      calculateIndicators();

    std::vector<std::vector<Real>> dummyVector;
    fillVector(dummyVector, ifld_, {0});

    SANS_ASSERT( dummyVector[0].size() % 2 == 0);

    int originalSize = dummyVector[0].size()/2;
    estimate.resize( originalSize );
    for (int i = 0; i < originalSize; i++ )
      estimate[i] = dummyVector[0][i] + dummyVector[0][i+originalSize];
  }


  const XField<PhysDim, TopoDim>& getXField() const { return xfld_; }

  const Field_DG_Cell<PhysDim,TopoDim,Real>& getESField()
  {
    if (eSfldCalc_ == false)
      calculateSumEstimates();

    return eSfld_;
  }
  const Field_DG_Cell<PhysDim,TopoDim,Real>& getIField()
  {
    if (ifldCalc_ == false)
      calculateIndicators();

    return ifld_;
  }

  const Field_DG_Cell<PhysDim,TopoDim,Real>& getEField()             const { return efld_; }
  const Field_DG_Cell<PhysDim, TopoDim, Real>& getEAField()          const { return eAfld_; }
  const Field_DG_Trace<PhysDim, TopoDim, Real>& getEIField()         const { return eIfld_; }
  const Field_DG_BoundaryTrace<PhysDim, TopoDim, Real>& getEBField() const { return eBfld_; }

  // fragile interface for edge solve - accumulates only group 0
  void fillVector(std::vector<std::vector<Real>>& accVector,
                  const Field_DG_Cell<PhysDim,TopoDim,Real>& accFld,
                  const std::vector<int>& cellGroups )
  {
    // Accumulates a field into the less safe vector of vectors format, useful for communicating with external classes

    accVector.resize(cellGroups.size());
    for (std::size_t i = 0; i < accVector.size(); i++ )
      accVector[i].resize(xfld_.getCellGroupBaseGlobal(cellGroups[i]).nElem());

    // Populate the vector of vectors from ifld_
    for_each_CellGroup<TopoDim>::apply( AccumulateVector<PhysDim>(accVector,cellGroups), accFld );
  }

  void fillEArray(std::vector<std::vector<Real>>& estimates ) // fragile interface for Savithru
  {
    if (ifldCalc_ == false)
      calculateIndicators();

    estimates.resize(xfld_.nCellGroups());
    for (int i = 0; i < xfld_.nCellGroups(); i++ )
      estimates[i].resize(xfld_.getCellGroupBaseGlobal(i).nElem());

    // Populate the vector of vectors from efld_
    for_each_CellGroup<TopoDim>::apply( AccumulateVector<PhysDim>(estimates,cellGroups_),ifld_);
  };

  void outputFields( const std::string& filename )
  {
    Field_DG_Cell<PhysDim,TopoDim,DLA::VectorS<5,Real>> ofld_(xfld_, 0, BasisFunctionCategory_Legendre);

    for (int n = 0; n < ofld_.nDOF(); n++)
    {
      ofld_.DOF(n)[0] = eSfld_.DOF(n);
      ofld_.DOF(n)[1] = ifld_.DOF(n);
      ofld_.DOF(n)[2] = efld_.DOF(n);
      ofld_.DOF(n)[3] = eAfld_.DOF(n);
      ofld_.DOF(n)[4] = eSfld_.DOF(n) - efld_.DOF(n) - eAfld_.DOF(n);
    }

    output_Tecplot( ofld_, filename, {"eSfld","ifld","efld","eAfld","eIfld"} );
  }

protected:
  const XFieldType& xfld_;
  XField_CellToTrace<PhysDim, TopoDim> xfldCellToTrace_;
  std::vector<int> active_BGroup_list_; //list of BoundaryTraceGroups which have Lagrange multiplier DOFs (mitLG)

  Field_DG_Cell<PhysDim,TopoDim,Real>            efld_; // q field estimates
  Field_DG_Cell<PhysDim,TopoDim,Real>            ifld_; // indicator field
  Field_DG_Cell<PhysDim,TopoDim,Real>            eSfld_; // estimate sum field
  Field_DG_Cell<PhysDim, TopoDim, Real>          eAfld_;
  Field_DG_Trace<PhysDim, TopoDim, Real>         eIfld_;
  Field_DG_BoundaryTrace<PhysDim, TopoDim, Real> eBfld_;

  const Field<PhysDim, TopoDim, ArrayQ>& qfld_; // q
  const Field<PhysDim, TopoDim, VectorArrayQ>& afld_; // a
  const Field<PhysDim, TopoDim, ArrayQ>& qIfld_; // qI
  const Field<PhysDim, TopoDim, ArrayQ>& lgfld_; // lg
  const Field<PhysDim, TopoDim, ArrayQ>& wfld_; // w
  const Field<PhysDim, TopoDim, VectorArrayQ>& bfld_; // b
  const Field<PhysDim, TopoDim, ArrayQ>& wIfld_; // wI
  const Field<PhysDim, TopoDim, ArrayQ>& mufld_; // mu

  const NDPDEClass& pde_;
  const QuadratureOrder quadratureOrder_;

  IntegrandCellClass fcnCell_;
  IntegrandTraceClass fcnIntTrace_;

  std::map< std::string, std::shared_ptr<BCBase> > BCs_;
  IntegrateBoundaryTrace_Dispatch dispatchBC_;

  std::vector<int> cellGroups_;
  std::vector<int> interiorTraceGroups_;
  std::vector<int> boundaryTraceGroups_;

  bool ifldCalc_;
  bool eSfldCalc_;

  //----------------------------------------------------------------------------//
  // Function for calculating the sum estimates
  void
  calculateSumEstimates()
  {
    // in lieu of a copy constructor
    for (int i = 0; i < efld_.nDOF(); i++)
      eSfld_.DOF(i) = efld_.DOF(i);

    // Distributing the Auxilliary estimate field
    for_each_CellGroup<TopoDim>::apply(
        DistributeAuxilliaryField(cellGroups_), (eSfld_, eAfld_) );

    // Distributing the Interior component of Interface estimate field
    for_each_InteriorFieldTraceGroup_Cell<TopoDim>::apply(
        DistributeInteriorTraceField(interiorTraceGroups_), eSfld_, eIfld_ );

    // Distributing the Boundary component of Interface estimate field
    for_each_BoundaryFieldTraceGroup_Cell<TopoDim>::apply(
        DistributeBoundaryTraceField<PhysDim>(boundaryTraceGroups_), eSfld_, eIfld_ );

    // Distributing the boundary estimate field
    for_each_BoundaryFieldTraceGroup_Cell<TopoDim>::apply(
        DistributeBoundaryTraceField<PhysDim>(active_BGroup_list_), eSfld_, eBfld_ ); // Lagrange Fields

    eSfldCalc_ = true; // only place this is changed
  }

  //----------------------------------------------------------------------------//
  // Function for calculating the indicator estimates
  void
  calculateIndicators()
  {
    // Flag for using absolutes in the indicators
    // false -> eta = abs(eta_w + eta_b + eta_wI + eta_lg)
    // true  -> eta = abs(eta_w) + abs(eta_b) + abs(eta_wI) + abs(eta_lg)
    const bool absFlag = true;
    ifld_ = 0;

    if (absFlag == true)
      for (int i = 0; i < efld_.nDOF(); i++)
        ifld_.DOF(i) += fabs(efld_.DOF(i));

    // Distributing the Lifting Operator estimate field
    for_each_CellGroup<TopoDim>::apply(
        DistributeAuxilliaryField(cellGroups_, absFlag), (ifld_, eAfld_ ));

    // Distributing the Interior component of Interface estimate field
    for_each_InteriorFieldTraceGroup_Cell<TopoDim>::apply(
        DistributeInteriorTraceField(interiorTraceGroups_, absFlag), ifld_, eIfld_ );

    // Distributing the Boundary component of Interface estimate field
    for_each_BoundaryFieldTraceGroup_Cell<TopoDim>::apply(
        DistributeBoundaryTraceField<PhysDim>(boundaryTraceGroups_, absFlag), ifld_, eIfld_ );

    // Distributing the boundary estimate field
    for_each_BoundaryFieldTraceGroup_Cell<TopoDim>::apply(
        DistributeBoundaryTraceField<PhysDim>(active_BGroup_list_, absFlag), ifld_, eBfld_ ); // Lagrange Fields

    if (absFlag == false)
      for (int i = 0; i < efld_.nDOF(); i++)
        ifld_.DOF(i) = fabs(ifld_.DOF(i) + efld_.DOF(i));

    ifldCalc_ = true; // only place this is changed
  }


  //----------------------------------------------------------------------------//
  // Estimate function that populates the efld_, eLFld_ and eBfld_, called in constructor
  void
  estimate( )
  {
    efld_ = 0; eAfld_ = 0; eIfld_ = 0; eBfld_ = 0; // zero the internal fields

    // Cell and Interior Estimates
    IntegrateCellGroups<TopoDim>::integrate( ErrorEstimateCell_HDG( fcnCell_, fcnIntTrace_, xfldCellToTrace_,
                                                                   xfld_, qfld_, afld_, qIfld_, wfld_, bfld_, wIfld_,
                                                                   efld_, eAfld_, eIfld_,
                                                                   quadratureOrder_.interiorTraceOrders.data(),
                                                                   quadratureOrder_.interiorTraceOrders.size() ),
                                            xfld_, (qfld_, afld_, wfld_, bfld_, efld_, eAfld_),
                                            quadratureOrder_.cellOrders.data(), quadratureOrder_.cellOrders.size());

    // Boundary Trace Estimates
    dispatchBC_.dispatch(
        ErrorEstimateBoundaryTrace_FieldTrace_Dispatch_HDG( xfld_, qfld_, afld_, qIfld_, lgfld_,
                                                            wfld_, bfld_, wIfld_, mufld_,
                                                            efld_, eAfld_, eIfld_, eBfld_,
                                                            quadratureOrder_.boundaryTraceOrders.data(),
                                                            quadratureOrder_.boundaryTraceOrders.size()),
        ErrorEstimateBoundaryTrace_Dispatch_HDG( xfld_, qfld_, afld_, qIfld_,
                                                 wfld_, bfld_, wIfld_,
                                                 efld_, eAfld_, eIfld_,
                                                 quadratureOrder_.boundaryTraceOrders.data(),
                                                 quadratureOrder_.boundaryTraceOrders.size())
    );
  }

  class DistributeAuxilliaryField:
      public GroupFunctorCellType<DistributeAuxilliaryField>
  {
  public:

    explicit DistributeAuxilliaryField( const std::vector<int>& cellGroups, const bool absFlag = false ) :
    cellGroups_(cellGroups), absFlag_(absFlag) {}

    std::size_t nCellGroups()          const { return cellGroups_.size(); }
    std::size_t cellGroup(const int n) const { return cellGroups_[n];     }

    //----------------------------------------------------------------------------//
    // Distribution function that redistributes the error in each cell group
    template< class Topology>
    void
    apply(  const typename FieldTuple<Field<PhysDim, typename Topology::TopoDim, Real>,
                                      Field<PhysDim, typename Topology::TopoDim, Real>,TupleClass<>>::
                                      template FieldCellGroupType<Topology>& fldsCell,
            const int cellGroupGlobal)
    {
      // Cell Group Types
      typedef typename Field< PhysDim,typename Topology::TopoDim, Real >::template FieldCellGroupType<Topology> EFieldCellGroupType;
      typedef typename Field< PhysDim,typename Topology::TopoDim, Real >::template FieldCellGroupType<Topology> EAFieldCellGroupType;

      typedef typename EFieldCellGroupType::template ElementType<>  ElementEFieldClass;
      typedef typename EAFieldCellGroupType::template ElementType<> ElementEAFieldClass;

      EFieldCellGroupType& efld = const_cast<EFieldCellGroupType&> (get<0>(fldsCell));
      const EAFieldCellGroupType& eAfld = get<1>(fldsCell);

      // Elements
      ElementEFieldClass efldElem(efld.basis() );
      ElementEAFieldClass eAfldElem(eAfld.basis() );

      if (absFlag_ == false )
      {
        // DEFAULT BRANCH

        // loop over elements
        for (int elem = 0; elem<efld.nElem();elem++)
        {
          efld.getElement(efldElem,elem);
          eAfld.getElement(eAfldElem,elem);
          efldElem.DOF(0) += eAfldElem.DOF(0);
          efld.setElement(efldElem,elem);
        }
      }
      else
      {
        // loop over elements
        for (int elem = 0; elem<efld.nElem();elem++)
        {
          efld.getElement(efldElem,elem);
          eAfld.getElement(eAfldElem,elem);
          efldElem.DOF(0) += fabs(eAfldElem.DOF(0));
          efld.setElement(efldElem,elem);
        }
      }
    }
  protected:
    const std::vector<int> cellGroups_;
    const bool absFlag_;
  };

  class DistributeInteriorTraceField:
      public GroupFunctorInteriorTraceType<DistributeInteriorTraceField>
  {
  public:
    typedef typename NDPDEClass::PhysDim PhysDim;

    explicit DistributeInteriorTraceField( const std::vector<int>& interiorTraceGroups, const bool absFlag = false ) :
    interiorTraceGroups_(interiorTraceGroups), absFlag_(absFlag) {}

    std::size_t nInteriorTraceGroups() const { return interiorTraceGroups_.size(); }
    std::size_t interiorTraceGroup(const int n) const { return interiorTraceGroups_[n]; }

    //----------------------------------------------------------------------------//
    // Distribution function that redistributes the error in each cell group
    template< class TopologyTrace, class TopologyL, class TopologyR>
    void
    apply( const typename Field<PhysDim, typename TopologyL::TopoDim, Real>::template FieldCellGroupType<TopologyL>& efldL,
           const typename Field<PhysDim, typename TopologyR::TopoDim, Real>::template FieldCellGroupType<TopologyR>& efldR,
           const typename Field<PhysDim, typename TopologyTrace::CellTopoDim, Real>::template FieldTraceGroupType<TopologyTrace>& eIfld,
           const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
           const int traceGroupGlobal)
    {
      typedef typename TopologyL::TopoDim TopoDim;
      // Cell Group Types
      typedef typename Field< PhysDim,TopoDim, Real>::template FieldCellGroupType<TopologyL> ELFieldCellGroupType;
      typedef typename Field< PhysDim,TopoDim, Real>::template FieldCellGroupType<TopologyR> ERFieldCellGroupType;
      typedef typename Field< PhysDim,TopoDim, Real>::template FieldTraceGroupType<TopologyTrace> EIFieldCellGroupType;

      typedef typename ELFieldCellGroupType::template ElementType<> ElementELFieldClass;
      typedef typename ERFieldCellGroupType::template ElementType<> ElementERFieldClass;
      typedef typename EIFieldCellGroupType::template ElementType<> ElementEIFieldClass;

      // Apportioning the eIfld to efld
      ElementELFieldClass efldElemL(efldL.basis() );
      ElementERFieldClass efldElemR(efldR.basis() );

      ElementEIFieldClass eIfldElem(eIfld.basis() );

      if (absFlag_ == false)
        // loop over elements
        for (int elem = 0; elem<eIfld.nElem();elem++)
        {
          int elemL = xfldTrace.getElementLeft( elem );
          int elemR = xfldTrace.getElementRight( elem );

          eIfld.getElement(eIfldElem, elem );

          efldL.getElement( efldElemL, elemL );
          efldElemL.DOF(0) += 0.5*eIfldElem.DOF(0);
          const_cast<ELFieldCellGroupType&>(efldL).setElement( efldElemL, elemL );

          efldR.getElement( efldElemR, elemR );
          efldElemR.DOF(0) += 0.5*eIfldElem.DOF(0);
          const_cast<ERFieldCellGroupType&>(efldR).setElement( efldElemR, elemR );
        }
      else
        for (int elem = 0; elem<eIfld.nElem();elem++)
        {
          int elemL = xfldTrace.getElementLeft( elem );
          int elemR = xfldTrace.getElementRight( elem );

          eIfld.getElement(eIfldElem, elem );

          efldL.getElement( efldElemL, elemL );
          efldElemL.DOF(0) += fabs(0.5*eIfldElem.DOF(0));
          const_cast<ELFieldCellGroupType&>(efldL).setElement( efldElemL, elemL );

          efldR.getElement( efldElemR, elemR );
          efldElemR.DOF(0) += fabs(0.5*eIfldElem.DOF(0));
          const_cast<ERFieldCellGroupType&>(efldR).setElement( efldElemR, elemR );
        }
    }
  protected:
    const std::vector<int> interiorTraceGroups_;
    const bool absFlag_;
  };

};

} //namespace SANS

#endif  // ERRORESTIMATE_HDG_H
