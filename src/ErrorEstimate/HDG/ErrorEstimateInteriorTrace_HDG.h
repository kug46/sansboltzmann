// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ERRORESTIMATEINTERIORTRACE_HDG_H
#define ERRORESTIMATEINTERIORTRACE_HDG_H

#include "tools/Tuple.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/ElementIntegral.h"

#include "Discretization/HDG/DiscretizationHDG.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  DG interior-trace integral
//

template<class IntegrandITrace>
class ErrorEstimateInteriorTrace_HDG_impl :
    public GroupIntegralInteriorTraceType< ErrorEstimateInteriorTrace_HDG_impl<IntegrandITrace> >
{
public:
  typedef typename IntegrandITrace::PhysDim PhysDim;
  typedef typename IntegrandITrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandITrace::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename IntegrandITrace::PDE PDE;

  // Save off the boundary trace integrand
  explicit ErrorEstimateInteriorTrace_HDG_impl( const IntegrandITrace& fcnITrace,
                                                const std::map<int,std::vector<int>>& cellITraceGroups,
                                                const int& cellgroup, const int& cellelem ) :
                                                    fcnITrace_(fcnITrace),
                                                    cellITraceGroups_(cellITraceGroups),
                                                    cellgroup_(cellgroup), cellelem_(cellelem)
  {
    for (std::map<int,std::vector<int>>::const_iterator it = cellITraceGroups_.begin(); it != cellITraceGroups_.end(); ++it)
      traceGroupIndices_.push_back( it->first );
  }

  std::size_t nInteriorTraceGroups() const { return traceGroupIndices_.size(); }
  std::size_t interiorTraceGroup(const int n) const { return traceGroupIndices_[n]; }

//----------------------------------------------------------------------------//
  template <class TopoDim>
  void check( const typename MakeTuple<FieldTuple,
              Field<PhysDim, TopoDim, ArrayQ>, // q
              Field<PhysDim, TopoDim, VectorArrayQ>, // a
              Field<PhysDim, TopoDim, ArrayQ>, // w
              Field<PhysDim, TopoDim, VectorArrayQ>, // b
              Field<PhysDim, TopoDim, Real>, // e
              Field<PhysDim, TopoDim, Real>>::type& flds, // eA
              const typename MakeTuple<FieldTuple,
              Field<PhysDim, TopoDim, ArrayQ>,       // qI
              Field<PhysDim, TopoDim, ArrayQ>,       // wI
              Field<PhysDim, TopoDim, Real>>::type& fldsTrace) const // eI
  {
    const Field<PhysDim, TopoDim, ArrayQ>&       qfld  = get<0>(flds);
    const Field<PhysDim, TopoDim, VectorArrayQ>& afld  = get<1>(flds);
    const Field<PhysDim, TopoDim, ArrayQ>&       wfld  = get<2>(flds);
    const Field<PhysDim, TopoDim, VectorArrayQ>& bfld  = get<3>(flds);
    const Field<PhysDim, TopoDim, Real>&         efld  = get<4>(flds);
    const Field<PhysDim, TopoDim, Real>&         eAfld = get<5>(flds);

    const Field<PhysDim, TopoDim, ArrayQ>& qIfld = get<0>(fldsTrace);
    const Field<PhysDim, TopoDim, ArrayQ>& wIfld = get<1>(fldsTrace);
    const Field<PhysDim, TopoDim, Real>& eIfld = get<2>(fldsTrace);

    SANS_ASSERT( &qfld.getXField() == &afld.getXField() );  // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &wfld.getXField() );  // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &bfld.getXField() );  // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &efld.getXField() );  // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &eAfld.getXField() ); // check both were made off the same grid

    SANS_ASSERT( &qfld.getXField() == &qIfld.getXField() );
    SANS_ASSERT( &qfld.getXField() == &wIfld.getXField() );
    SANS_ASSERT( &qfld.getXField() == &eIfld.getXField() );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the interior trace group
  template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, ArrayQ>,         // q
                                           Field<PhysDim, TopoDim, VectorArrayQ>,   // a
                                           Field<PhysDim, TopoDim, ArrayQ>,         // w
                                           Field<PhysDim, TopoDim, VectorArrayQ>,   // b
                                           Field<PhysDim, TopoDim, Real>,           // e
                                           Field<PhysDim, TopoDim, Real>>::type  // eA
                              ::template FieldCellGroupType<TopologyL>& fldsCellL,
      const int cellGroupGlobalR,
      const typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyR>& xfldCellR,
      const typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, ArrayQ>,         // q
                                           Field<PhysDim, TopoDim, VectorArrayQ>,   // a
                                           Field<PhysDim, TopoDim, ArrayQ>,         // w
                                           Field<PhysDim, TopoDim, VectorArrayQ>,   // b
                                           Field<PhysDim, TopoDim, Real>,           // e
                                           Field<PhysDim, TopoDim, Real>>::type  // eA
                              ::template FieldCellGroupType<TopologyR>& fldsCellR,
      const int traceGroupGlobal,
      const typename XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, ArrayQ>,       // qI
                                           Field<PhysDim, TopoDim, ArrayQ>,       // wI
                                           Field<PhysDim, TopoDim, Real>>::type   // eI
                              ::template FieldTraceGroupType<TopologyTrace>& fldsTrace,
      int quadratureorder )
  {
    // Left types
    typedef typename XFieldType                           ::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ >     ::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> AFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, Real >       ::template FieldCellGroupType<TopologyL> EFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, Real >       ::template FieldCellGroupType<TopologyL> EAFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL ::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL ::template ElementType<> ElementQFieldClassL;
    typedef typename AFieldCellGroupTypeL ::template ElementType<> ElementAFieldClassL;
    typedef typename EFieldCellGroupTypeL ::template ElementType<> ElementEFieldClassL;
    typedef typename EAFieldCellGroupTypeL::template ElementType<> ElementEAFieldClassL;

    // Right types
    typedef typename XFieldType                           ::template FieldCellGroupType<TopologyR> XFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, ArrayQ >     ::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyR> AFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, Real >       ::template FieldCellGroupType<TopologyR> EFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, Real >       ::template FieldCellGroupType<TopologyR> EAFieldCellGroupTypeR;

    typedef typename XFieldCellGroupTypeR ::template ElementType<> ElementXFieldClassR;
    typedef typename QFieldCellGroupTypeR ::template ElementType<> ElementQFieldClassR;
    typedef typename AFieldCellGroupTypeR ::template ElementType<> ElementAFieldClassR;
    typedef typename EFieldCellGroupTypeR ::template ElementType<> ElementEFieldClassR;
    typedef typename EAFieldCellGroupTypeR::template ElementType<> ElementEAFieldClassR;

    //Trace types
    typedef typename XFieldType                       ::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ > ::template FieldTraceGroupType<TopologyTrace> QIFieldTraceGroupType;
    typedef typename Field<PhysDim, TopoDim, Real >   ::template FieldTraceGroupType<TopologyTrace> EIFieldTraceGroupType;

    typedef typename XFieldTraceGroupType ::template ElementType<> ElementXFieldTraceClass;
    typedef typename QIFieldTraceGroupType::template ElementType<> ElementQIFieldTraceClass;
    typedef typename EIFieldTraceGroupType::template ElementType<> ElementEIFieldTraceClass;

    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    // separating tuple and casting away const
    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const AFieldCellGroupTypeL& afldCellL = get<1>(fldsCellL);
    const QFieldCellGroupTypeL& wfldCellL = get<2>(fldsCellL);
    const AFieldCellGroupTypeL& bfldCellL = get<3>(fldsCellL);
    EFieldCellGroupTypeL& efldCellL = const_cast<EFieldCellGroupTypeL&>(get<4>(fldsCellL));
    EAFieldCellGroupTypeL& eAfldCellL = const_cast<EAFieldCellGroupTypeL&>(get<5>(fldsCellL));

    const QFieldCellGroupTypeR& qfldCellR = get<0>(fldsCellR);
    const AFieldCellGroupTypeR& afldCellR = get<1>(fldsCellR);
    const QFieldCellGroupTypeR& wfldCellR = get<2>(fldsCellR);
    const AFieldCellGroupTypeR& bfldCellR = get<3>(fldsCellR);
    EFieldCellGroupTypeR& efldCellR = const_cast<EFieldCellGroupTypeR&>(get<4>(fldsCellR));
    EAFieldCellGroupTypeR& eAfldCellR = const_cast<EAFieldCellGroupTypeR&>(get<5>(fldsCellR));

    const QIFieldTraceGroupType qIfldTrace = get<0>(fldsTrace);
    const QIFieldTraceGroupType wIfldTrace = get<1>(fldsTrace);
    EIFieldTraceGroupType eIfldTrace = const_cast<EIFieldTraceGroupType&>(get<2>(fldsTrace));

    // LEFT element field variables
    ElementXFieldClassL  xfldElemL(  xfldCellL.basis() );
    ElementQFieldClassL  qfldElemL(  qfldCellL.basis() );
    ElementAFieldClassL  afldElemL(  afldCellL.basis() );
    ElementQFieldClassL  wfldElemL(  wfldCellL.basis() );
    ElementAFieldClassL  bfldElemL(  bfldCellL.basis() );
    ElementEFieldClassL  efldElemL(  efldCellL.basis() );
    ElementEAFieldClassL eAfldElemL( eAfldCellL.basis() );

    // RIGHT element field variables
    ElementXFieldClassR  xfldElemR(  xfldCellR.basis() );
    ElementQFieldClassR  qfldElemR(  qfldCellR.basis() );
    ElementAFieldClassR  afldElemR(  afldCellR.basis() );
    ElementQFieldClassR  wfldElemR(  wfldCellR.basis() );
    ElementAFieldClassR  bfldElemR(  bfldCellR.basis() );
    ElementEFieldClassR  efldElemR(  efldCellR.basis() );
    ElementEAFieldClassR eAfldElemR( eAfldCellR.basis() );

    // TRACE element field variables
    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );
    ElementQIFieldTraceClass qIfldElemTrace( qIfldTrace.basis() );
    ElementQIFieldTraceClass wIfldElemTrace( wIfldTrace.basis() );
    ElementEIFieldTraceClass eIfldElemTrace( eIfldTrace.basis() );

    //Check if they are P0 fields
    SANS_ASSERT( efldElemL.order() == 0 );
    SANS_ASSERT( efldElemR.order() == 0 );
    SANS_ASSERT( eIfldElemTrace.order() == 0 );

    typedef IntegrandHDG< Real, Real > IntegrandTypeL;
    typedef IntegrandHDG< Real, Real > IntegrandTypeR;
    typedef Real IntegrandTraceType;

    ElementIntegral<TopoDimTrace, TopologyTrace, IntegrandTypeL, IntegrandTraceType> integralL(quadratureorder);
    ElementIntegral<TopoDimTrace, TopologyTrace, IntegrandTypeR, IntegrandTraceType> integralR(quadratureorder);

    // just to make sure things are consistent
    SANS_ASSERT( xfldCellL.nElem() == efldCellL.nElem() );
    SANS_ASSERT( xfldCellL.nElem() == eAfldCellL.nElem() );
    SANS_ASSERT( xfldCellR.nElem() == efldCellR.nElem() );
    SANS_ASSERT( xfldCellR.nElem() == eAfldCellR.nElem() );
    SANS_ASSERT( xfldTrace.nElem() == eIfldTrace.nElem() );

    IntegrandTypeL rsdElemCellL;
    IntegrandTypeR rsdElemCellR;
    IntegrandTraceType rsdElemTrace;

    const std::vector<int>& traceElemList = cellITraceGroups_.at(traceGroupGlobal);

    // loop over trace elements in list
    for (int i = 0; i < (int) traceElemList.size(); i++)
    {
      const int elem = traceElemList[i];

      const int elemL = xfldTrace.getElementLeft( elem );
      const int elemR = xfldTrace.getElementRight( elem );

      xfldTrace.getElement( xfldElemTrace, elem );
      qIfldTrace.getElement( qIfldElemTrace, elem );
      wIfldTrace.getElement( wIfldElemTrace, elem );

      if (xfldTrace.getGroupLeft() == cellgroup_ && elemL == cellelem_)
      {
        //The cell that called this function is to the left of the trace
        const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

        // copy global grid/solution DOFs to element
        xfldCellL.getElement( xfldElemL, elemL );
        qfldCellL.getElement( qfldElemL, elemL );
        afldCellL.getElement( afldElemL, elemL );
        wfldCellL.getElement( wfldElemL, elemL );
        bfldCellL.getElement( bfldElemL, elemL );

        rsdElemCellL = 0;
        rsdElemTrace = 0;

        integralL( fcnITrace_.integrand( xfldElemTrace, qIfldElemTrace, wIfldElemTrace,
                                         canonicalTraceL, +1, xfldElemL,
                                         qfldElemL, afldElemL, wfldElemL, bfldElemL ),
                   xfldElemTrace, rsdElemCellL, rsdElemTrace );

        // PDE
        efldCellL.getElement( efldElemL,elemL );
        efldElemL.DOF(0) += rsdElemCellL.PDE;
        efldCellL.setElement( efldElemL, elemL );

        // Auxiliary
        eAfldCellL.getElement( eAfldElemL, elemL );
        eAfldElemL.DOF(0) += rsdElemCellL.Au;
        eAfldCellL.setElement( eAfldElemL, elemL );

        // Trace
        eIfldTrace.getElement( eIfldElemTrace, elem );
        eIfldElemTrace.DOF(0) += rsdElemTrace;
        eIfldTrace.setElement( eIfldElemTrace, elem );
      }
      else if (xfldTrace.getGroupRight() == cellgroup_ && elemR == cellelem_)
      {
        //The cell that called this function is to the right of the trace
        const CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

        // copy global grid/solution DOFs to element
        xfldCellR.getElement( xfldElemR, elemR );
        qfldCellR.getElement( qfldElemR, elemR );
        afldCellR.getElement( afldElemR, elemR );
        wfldCellR.getElement( wfldElemR, elemR );
        bfldCellR.getElement( bfldElemR, elemR );

        rsdElemCellR = 0;
        rsdElemTrace = 0;

        integralR( fcnITrace_.integrand( xfldElemTrace, qIfldElemTrace, wIfldElemTrace,
                                         canonicalTraceR, -1, xfldElemR,
                                         qfldElemR, afldElemR, wfldElemR, bfldElemR ),
                   xfldElemTrace, rsdElemCellR, rsdElemTrace );

        // PDE
        efldCellR.getElement( efldElemR,elemR );
        efldElemR.DOF(0) += rsdElemCellR.PDE;
        efldCellR.setElement( efldElemR, elemR );

        // Auxiliary
        eAfldCellR.getElement( eAfldElemR, elemR );
        eAfldElemR.DOF(0) += rsdElemCellR.Au;
        eAfldCellR.setElement( eAfldElemR, elemR );

        // Trace
        eIfldTrace.getElement( eIfldElemTrace, elem );
        eIfldElemTrace.DOF(0) += rsdElemTrace;
        eIfldTrace.setElement( eIfldElemTrace, elem );
      }
      else
        SANS_DEVELOPER_EXCEPTION("ErrorEstimateInteriorTrace_HDG_impl::integrate - Invalid configuration!");
    }
  }

protected:
  const IntegrandITrace& fcnITrace_;
  const std::map<int,std::vector<int>>& cellITraceGroups_;
  const int& cellgroup_;
  const int& cellelem_;
  std::vector<int> traceGroupIndices_;
};


// Factory function

template<class IntegrandITrace>
ErrorEstimateInteriorTrace_HDG_impl<IntegrandITrace>
ErrorEstimateInteriorTrace_HDG( const IntegrandInteriorTraceType<IntegrandITrace>& fcnITrace,
                                const std::map<int,std::vector<int>>& cellITraceGroups,
                                const int& cellgroup, const int& cellelem )
{
  return ErrorEstimateInteriorTrace_HDG_impl<IntegrandITrace>(
      fcnITrace.cast(), cellITraceGroups, cellgroup, cellelem);
}


}

#endif  // ERRORESTIMATEINTERIORTRACE_HDG_H
