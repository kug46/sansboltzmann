// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ERRORESTIMATEBOUNDARYTRACE_DISPATCH_HDG_H
#define ERRORESTIMATEBOUNDARYTRACE_DISPATCH_HDG_H

// boundary-trace integral residual functions

#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"
#include "ErrorEstimateBoundaryTrace_FieldTrace_HDG.h"
#include "ErrorEstimateBoundaryTrace_HDG.h"

namespace SANS
{

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's with Lagrange multipliers
//
//---------------------------------------------------------------------------//
template< class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ >
class ErrorEstimateBoundaryTrace_FieldTrace_Dispatch_HDG_impl
{
public:
  ErrorEstimateBoundaryTrace_FieldTrace_Dispatch_HDG_impl( const XFieldType& xfld,
                                                           const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                                           const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
                                                           const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                                                           const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                                                           const Field<PhysDim, TopoDim, ArrayQ>& wfld,
                                                           const Field<PhysDim, TopoDim, VectorArrayQ>& bfld,
                                                           const Field<PhysDim, TopoDim, ArrayQ>& wIfld,
                                                           const Field<PhysDim, TopoDim, ArrayQ>& mufld,
                                                           Field<PhysDim, TopoDim, Real>& efld,
                                                           Field<PhysDim, TopoDim, Real>& eAfld,
                                                           Field<PhysDim, TopoDim, Real>& eIfld,
                                                           Field<PhysDim, TopoDim, Real>& eBfld,
                                                           const int* quadratureorder, int ngroup)
    : xfld_(xfld),
      qfld_(qfld), afld_(afld), qIfld_(qIfld), lgfld_(lgfld),
      wfld_(wfld), bfld_(bfld), wIfld_(wIfld), mufld_(mufld),
      efld_(efld), eAfld_(eAfld), eIfld_(eIfld), eBfld_(eBfld),
      quadratureorder_(quadratureorder), ngroup_(ngroup) {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
        ErrorEstimateBoundaryTrace_FieldTrace_HDG(fcn),
        xfld_,
        (qfld_,afld_,wfld_,bfld_,efld_,eAfld_),
        (qIfld_, lgfld_, wIfld_, mufld_, eIfld_, eBfld_), quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;

  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, VectorArrayQ>& afld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qIfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& lgfld_;

  const Field<PhysDim, TopoDim, ArrayQ>& wfld_;
  const Field<PhysDim, TopoDim, VectorArrayQ>& bfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& wIfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& mufld_;

  Field<PhysDim, TopoDim, Real>& efld_;
  Field<PhysDim, TopoDim, Real>& eAfld_;
  Field<PhysDim, TopoDim, Real>& eIfld_;
  Field<PhysDim, TopoDim, Real>& eBfld_;

  const int* quadratureorder_;
  const int ngroup_;
};

// Factory function

template< class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ >
ErrorEstimateBoundaryTrace_FieldTrace_Dispatch_HDG_impl<XFieldType, PhysDim, TopoDim, ArrayQ, VectorArrayQ>
ErrorEstimateBoundaryTrace_FieldTrace_Dispatch_HDG( const XFieldType& xfld,
                                                    const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                                    const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
                                                    const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                                                    const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                                                    const Field<PhysDim, TopoDim, ArrayQ>& wfld,
                                                    const Field<PhysDim, TopoDim, VectorArrayQ>& bfld,
                                                    const Field<PhysDim, TopoDim, ArrayQ>& wIfld,
                                                    const Field<PhysDim, TopoDim, ArrayQ>& mufld,
                                                    Field<PhysDim, TopoDim, Real>& efld,
                                                    Field<PhysDim, TopoDim, Real>& eAfld,
                                                    Field<PhysDim, TopoDim, Real>& eIfld,
                                                    Field<PhysDim, TopoDim, Real>& eBfld,
                                                    const int* quadratureorder, int ngroup)
{
  return { xfld,
           qfld, afld, qIfld, lgfld,
           wfld, bfld, wIfld, mufld,
           efld, eAfld, eIfld, eBfld,
           quadratureorder, ngroup };
}

#if 1
//---------------------------------------------------------------------------//
//
// Dispatch class for BC's without Lagrange multipliers
//
//---------------------------------------------------------------------------//
template< class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ>
class ErrorEstimateBoundaryTrace_Dispatch_HDG_impl
{
public:
  ErrorEstimateBoundaryTrace_Dispatch_HDG_impl( const XFieldType& xfld,
                                                const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                                const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
                                                const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                                                const Field<PhysDim, TopoDim, ArrayQ>& wfld,
                                                const Field<PhysDim, TopoDim, VectorArrayQ>& bfld,
                                                const Field<PhysDim, TopoDim, ArrayQ>& wIfld,
                                                Field<PhysDim, TopoDim, Real>& efld,
                                                Field<PhysDim, TopoDim, Real>& eAfld,
                                                Field<PhysDim, TopoDim, Real>& eIfld,
                                                const int* quadratureorder, int ngroup ) :
                                                  xfld_(xfld),
                                                  qfld_(qfld), afld_(afld), qIfld_(qIfld),
                                                  wfld_(wfld), bfld_(bfld), wIfld_(wIfld),
                                                  efld_(efld), eAfld_(eAfld), eIfld_(eIfld),
                                                  quadratureorder_(quadratureorder), ngroup_(ngroup) {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
        ErrorEstimateBoundaryTrace_HDG(fcn),
        xfld_, (qfld_,afld_,wfld_,bfld_,efld_,eAfld_), (qIfld_, wIfld_, eIfld_),
        quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;

  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, VectorArrayQ>& afld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qIfld_;

  const Field<PhysDim, TopoDim, ArrayQ>& wfld_;
  const Field<PhysDim, TopoDim, VectorArrayQ>& bfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& wIfld_;

  Field<PhysDim, TopoDim, Real>& efld_;
  Field<PhysDim, TopoDim, Real>& eAfld_;
  Field<PhysDim, TopoDim, Real>& eIfld_;

  const int* quadratureorder_;
  const int ngroup_;
};

// Factory function

template< class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ >
ErrorEstimateBoundaryTrace_Dispatch_HDG_impl<XFieldType, PhysDim, TopoDim, ArrayQ, VectorArrayQ>
ErrorEstimateBoundaryTrace_Dispatch_HDG( const XFieldType& xfld,
                                         const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                         const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
                                         const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                                         const Field<PhysDim, TopoDim, ArrayQ>& wfld,
                                         const Field<PhysDim, TopoDim, VectorArrayQ>& bfld,
                                         const Field<PhysDim, TopoDim, ArrayQ>& wIfld,
                                         Field<PhysDim, TopoDim, Real>& efld,
                                         Field<PhysDim, TopoDim, Real>& eAfld,
                                         Field<PhysDim, TopoDim, Real>& eIfld,
                                         const int* quadratureorder, int ngroup)
{
  return { xfld, qfld, afld, qIfld,
           wfld, bfld, wIfld,
           efld, eAfld, eIfld,
           quadratureorder, ngroup };
}

#endif

}
#endif //ERRORESTIMATEBOUNDARYTRACE_HDG_H
