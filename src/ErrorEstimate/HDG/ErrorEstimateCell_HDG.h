// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ERRORESTIMATECELL_HDG_H
#define ERRORESTIMATECELL_HDG_H

// Cell integral residual functions

#include "tools/Tuple.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/ElementIntegral.h"
#include "Field/XField_CellToTrace.h"

#include "Discretization/HDG/DiscretizationHDG.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"
#include "Discretization/IntegrateInteriorTraceGroups_FieldTrace.h"

#include "ErrorEstimate/HDG/ErrorEstimateInteriorTrace_HDG.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Field Cell group weighted residual for HDG
//
// topology specific group weighted residual
//
// template parameters:
//   Topology                          element topology (e.g. Triangle)
//   Integrand                         integrand functor
//   XFieldType                        Grid data type, possibly a tuple with parameters
//
//   Eflds get removed from the Tuple and the QW fields get retupled and passed on to the integrand
//----------------------------------------------------------------------------//
//
template< class IntegrandCell, class IntegrandITrace, class XFieldType_, class TopoDim_ >
class ErrorEstimateCell_HDG_impl :
    public GroupIntegralCellType< ErrorEstimateCell_HDG_impl<IntegrandCell, IntegrandITrace, XFieldType_, TopoDim_ > >
{
public:
  typedef typename IntegrandCell::PhysDim PhysDim;
  typedef typename IntegrandCell::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandCell::template VectorArrayQ<Real> VectorArrayQ;

  static_assert( std::is_same< PhysDim, typename IntegrandITrace::PhysDim >::value, "PhysDim should be the same.");

  // Save off the cell integrand
  explicit ErrorEstimateCell_HDG_impl( const IntegrandCell& fcnCell, const IntegrandITrace& fcnITrace,
                                       const XField_CellToTrace<PhysDim, TopoDim_>& xfldCellToTrace,
                                       const XFieldType_& xfld,
                                       const Field<PhysDim, TopoDim_, ArrayQ>& qfld,
                                       const Field<PhysDim, TopoDim_, VectorArrayQ>& afld,
                                       const Field<PhysDim, TopoDim_, ArrayQ>& qIfld,
                                       const Field<PhysDim, TopoDim_, ArrayQ>& wfld,
                                       const Field<PhysDim, TopoDim_, VectorArrayQ>& bfld,
                                       const Field<PhysDim, TopoDim_, ArrayQ>& wIfld,
                                       Field<PhysDim, TopoDim_, Real>& efld,
                                       Field<PhysDim, TopoDim_, Real>& eAfld,
                                       Field<PhysDim, TopoDim_, Real>& eIfld,
                                       const int quadOrderITrace[], const int nITraceGroup ) :
                                            fcnCell_(fcnCell), fcnITrace_(fcnITrace),
                                            xfldCellToTrace_(xfldCellToTrace),
                                            xfld_(xfld), qfld_(qfld), afld_(afld), qIfld_(qIfld),
                                            wfld_(wfld), bfld_(bfld), wIfld_(wIfld),
                                            efld_(efld), eAfld_(eAfld), eIfld_(eIfld),
                                            quadOrderITrace_(quadOrderITrace), nITraceGroup_(nITraceGroup) {}

  std::size_t nCellGroups() const { return fcnCell_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcnCell_.cellGroup(n); }

//----------------------------------------------------------------------------//
  // A dummy function so IntegrateCellGroups doesn't get upset
  // Could do with some sort of useful test in here maybe
  template <class TopoDim>
  void check( const typename MakeTuple<FieldTuple,
              Field<PhysDim, TopoDim, ArrayQ>, // q
              Field<PhysDim, TopoDim, VectorArrayQ>, // a
              Field<PhysDim, TopoDim, ArrayQ>, // w
              Field<PhysDim, TopoDim, VectorArrayQ>, // b
              Field<PhysDim, TopoDim, Real>, // e
              Field<PhysDim, TopoDim, Real>>::type& flds ) const // eA
  {
    const Field<PhysDim, TopoDim, ArrayQ>&       qfld  = get<0>(flds);
    const Field<PhysDim, TopoDim, VectorArrayQ>& afld  = get<1>(flds);
    const Field<PhysDim, TopoDim, ArrayQ>&       wfld  = get<2>(flds);
    const Field<PhysDim, TopoDim, VectorArrayQ>& bfld  = get<3>(flds);
    const Field<PhysDim, TopoDim, Real>&         efld  = get<4>(flds);
    const Field<PhysDim, TopoDim, Real>&         eAfld = get<5>(flds);

    SANS_ASSERT( &qfld.getXField() == &afld.getXField() );  // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &wfld.getXField() );  // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &bfld.getXField() );  // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &efld.getXField() );  // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &eAfld.getXField() ); // check both were made off the same grid
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template < class Topology, class TopoDim, class XFieldType >
  void
  integrate( const int cellGroupGlobal,
             const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
             const typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, ArrayQ>,       // q
                                                  Field<PhysDim, TopoDim, VectorArrayQ>, // a
                                                  Field<PhysDim, TopoDim, ArrayQ>,       // w
                                                  Field<PhysDim, TopoDim, VectorArrayQ>, // b
                                                  Field<PhysDim, TopoDim, Real>,         // e
                                                  Field<PhysDim, TopoDim, Real>>::type   // eA
                                                  ::template FieldCellGroupType<Topology>& fldsCell,
             const int quadratureorder )
  {
    // XField
    typedef typename XFieldType                          ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field< PhysDim,TopoDim,ArrayQ      >::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename Field< PhysDim,TopoDim,VectorArrayQ>::template FieldCellGroupType<Topology> AFieldCellGroupType;
    typedef typename Field< PhysDim, TopoDim, Real      >::template FieldCellGroupType<Topology> EFieldCellGroupType;
    typedef typename Field< PhysDim, TopoDim, Real      >::template FieldCellGroupType<Topology> EAFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<>  ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<>  ElementQFieldClass;
    typedef typename AFieldCellGroupType::template ElementType<>  ElementAFieldClass;
    typedef typename EFieldCellGroupType::template ElementType<>  ElementEFieldClass;
    typedef typename EAFieldCellGroupType::template ElementType<> ElementEAFieldClass;

    const QFieldCellGroupType& qfldCell = get<0>(fldsCell);
    const AFieldCellGroupType& afldCell = get<1>(fldsCell);
    const QFieldCellGroupType& wfldCell = get<2>(fldsCell);
    const AFieldCellGroupType& bfldCell = get<3>(fldsCell);
    EFieldCellGroupType& efldCell = const_cast<EFieldCellGroupType&>(get<4>(fldsCell));
    EAFieldCellGroupType& eAfldCell = const_cast<EAFieldCellGroupType&>(get<5>(fldsCell));

    // element field variables
    ElementXFieldClass  xfldElem(  xfldCell.basis() );
    ElementQFieldClass  qfldElem(  qfldCell.basis() );
    ElementAFieldClass  afldElem(  afldCell.basis() );
    ElementQFieldClass  wfldElem(  wfldCell.basis() );
    ElementAFieldClass  bfldElem(  bfldCell.basis() );
    ElementEFieldClass  efldElem(  efldCell.basis() );
    ElementEAFieldClass eAfldElem( eAfldCell.basis() );

    //Check if they are P0 fields
    SANS_ASSERT( 0 == efldElem.basis()->order() );
    SANS_ASSERT( 0 == eAfldElem.basis()->order());

    typedef IntegrandHDG< Real, Real > IntegrandType;

    // element integral
    ElementIntegral<TopoDim, Topology, IntegrandType > integral(quadratureorder);

    // just to make sure things are consistent
    SANS_ASSERT( xfldCell.nElem() == efldCell.nElem() );
    SANS_ASSERT( xfldCell.nElem() == eAfldCell.nElem() );

    IntegrandType rsdElem;

    // loop over elements within group
    const int nelem = xfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid/solution DOFs to element
      xfldCell.getElement( xfldElem, elem );
      qfldCell.getElement( qfldElem, elem );
      afldCell.getElement( afldElem, elem );
      wfldCell.getElement( wfldElem, elem );
      bfldCell.getElement( bfldElem, elem );

      std::map<int,std::vector<int>> cellITraceGroups;

      for (int trace = 0; trace < Topology::NTrace; trace++)
      {
        const TraceInfo& traceinfo = xfldCellToTrace_.getTrace(cellGroupGlobal, elem, trace);

        if (traceinfo.type == TraceInfo::Interior)
        {
          //add this traceGroup and traceElem to the set of interior traces attached to this cell,
          //but only if the qIfld actually has this traceGroup
          if (qIfld_.getLocalInteriorTraceGroups()[traceinfo.group] >= 0)
            cellITraceGroups[traceinfo.group].push_back(traceinfo.elem);
        }
      }

      rsdElem = 0;
      // cell integration for canonical element
      integral( fcnCell_.integrand(xfldElem, qfldElem, afldElem, wfldElem, bfldElem), xfldElem, rsdElem );

      //Collect the contributions to the PDE residual from the traces
      IntegrateInteriorTraceGroups_FieldTrace<TopoDim>::integrate(
          ErrorEstimateInteriorTrace_HDG(fcnITrace_, cellITraceGroups, cellGroupGlobal, elem),
          xfld_, (qfld_, afld_, wfld_, bfld_, efld_, eAfld_), (qIfld_, wIfld_, eIfld_),
          quadOrderITrace_, nITraceGroup_ );

      // PDE
      efldCell.getElement( efldElem, elem );
      efldElem.DOF(0) += rsdElem.PDE; // so as not to overwrite
      efldCell.setElement( efldElem, elem );

      // AUX
      eAfldCell.getElement( eAfldElem, elem );
      eAfldElem.DOF(0) += rsdElem.Au;
      eAfldCell.setElement( eAfldElem, elem );
    }
  }

protected:
  const IntegrandCell& fcnCell_;
  const IntegrandITrace& fcnITrace_;
  const XField_CellToTrace<PhysDim, TopoDim_>& xfldCellToTrace_;

  const XFieldType_& xfld_;
  const Field<PhysDim, TopoDim_, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim_, VectorArrayQ>& afld_;
  const Field<PhysDim, TopoDim_, ArrayQ>& qIfld_;

  const Field<PhysDim, TopoDim_, ArrayQ>& wfld_;
  const Field<PhysDim, TopoDim_, VectorArrayQ>& bfld_;
  const Field<PhysDim, TopoDim_, ArrayQ>& wIfld_;

  Field<PhysDim, TopoDim_, Real>& efld_;
  Field<PhysDim, TopoDim_, Real>& eAfld_;
  Field<PhysDim, TopoDim_, Real>& eIfld_;

  const int *quadOrderITrace_;
  const int nITraceGroup_;
};

// Factory function

template<class IntegrandCell, class IntegrandITrace,
         class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ>
ErrorEstimateCell_HDG_impl<IntegrandCell, IntegrandITrace, XFieldType, TopoDim>
ErrorEstimateCell_HDG( const IntegrandCellType<IntegrandCell>& fcnCell,
                       const IntegrandInteriorTraceType<IntegrandITrace>& fcnITrace,
                       const XField_CellToTrace<PhysDim, TopoDim>& xfldCellToTrace,
                       const XFieldType& xfld,
                       const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                       const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
                       const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                       const Field<PhysDim, TopoDim, ArrayQ>& wfld,
                       const Field<PhysDim, TopoDim, VectorArrayQ>& bfld,
                       const Field<PhysDim, TopoDim, ArrayQ>& wIfld,
                       Field<PhysDim, TopoDim, Real>& efld,
                       Field<PhysDim, TopoDim, Real>& eAfld,
                       Field<PhysDim, TopoDim, Real>& eIfld,
                       const int quadOrderITrace[], const int nITraceGroup )
{
  return ErrorEstimateCell_HDG_impl<IntegrandCell, IntegrandITrace, XFieldType, TopoDim>(
          fcnCell.cast(), fcnITrace.cast(),
          xfldCellToTrace, xfld, qfld, afld, qIfld, wfld, bfld, wIfld,
          efld, eAfld, eIfld,
          quadOrderITrace, nITraceGroup );
}


} //namespace SANS

#endif  // ERRORESTIMATECELL_HDG_H
