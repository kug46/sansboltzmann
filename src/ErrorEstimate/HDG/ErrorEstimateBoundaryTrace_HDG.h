// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ERRORESTIMATEBOUNDARYTRACE_HDG_H
#define ERRORESTIMATEBOUNDARYTRACE_HDG_H

// boundary-trace integral residual functions

#include "Field/Field.h"
#include "Field/Element/ElementIntegral.h"
#include "Field/Tuple/FieldTuple.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"
#include "Discretization/HDG/DiscretizationHDG.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  HDG boundary-trace integral
//

template<class IntegrandBoundaryTrace>
class ErrorEstimateBoundaryTrace_HDG_impl :
    public GroupIntegralBoundaryTraceType< ErrorEstimateBoundaryTrace_HDG_impl<IntegrandBoundaryTrace> >
{
public:
  typedef typename IntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename IntegrandBoundaryTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandBoundaryTrace::template VectorArrayQ<Real> VectorArrayQ;

  // Save off the boundary trace integrand and the residual vectors
  explicit ErrorEstimateBoundaryTrace_HDG_impl( const IntegrandBoundaryTrace& fcnBTrace ) :
    fcnBTrace_(fcnBTrace) {}

  std::size_t nBoundaryGroups() const { return fcnBTrace_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcnBTrace_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
  // A dummy function so IntegrateGroups doesn't get upset
  // Could do with some sort of useful test in here maybe
  template < class TopoDim >
  void check( const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>,                // q
                                                   Field<PhysDim,TopoDim,VectorArrayQ>,          // a
                                                   Field<PhysDim,TopoDim,ArrayQ>,                // w
                                                   Field<PhysDim,TopoDim,VectorArrayQ>,          // b
                                                   Field<PhysDim,TopoDim,Real>,                  // e
                                                   Field<PhysDim,TopoDim,Real>>::type& fldsCell, // eA
              const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>,                        // qI
                                                   Field<PhysDim,TopoDim,ArrayQ>,                        // wI
                                                   Field<PhysDim,TopoDim,Real>>::type& fldsTrace ) const // eI
  {
    const Field<PhysDim, TopoDim, ArrayQ>&        qfld = get<0>(fldsCell);
    const Field<PhysDim, TopoDim, VectorArrayQ>&  afld = get<1>(fldsCell);
    const Field<PhysDim, TopoDim, ArrayQ>&        wfld = get<2>(fldsCell);
    const Field<PhysDim, TopoDim, VectorArrayQ>&  bfld = get<3>(fldsCell);
    const Field<PhysDim, TopoDim, Real>&          efld = get<4>(fldsCell);
    const Field<PhysDim, TopoDim, Real>&          eAfld = get<5>(fldsCell);
    const Field<PhysDim, TopoDim, ArrayQ>&        qIfld = get<0>(fldsTrace);
    const Field<PhysDim, TopoDim, ArrayQ>&        wIfld = get<1>(fldsTrace);
    const Field<PhysDim, TopoDim, Real>&          eIfld = get<2>(fldsTrace);

    SANS_ASSERT( &qfld.getXField() == &afld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &wfld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &bfld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &efld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &eAfld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &qIfld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &wIfld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &eIfld.getXField() ); // check both were made off the same grid
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class Topology, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobal,
             const typename XFieldType::template  FieldCellGroupType<Topology>& xfldCell,
             const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>,        // q
                                                  Field<PhysDim,TopoDim,VectorArrayQ>,  // a
                                                  Field<PhysDim,TopoDim,ArrayQ>,        // w
                                                  Field<PhysDim,TopoDim,VectorArrayQ>,  // b
                                                  Field<PhysDim,TopoDim,Real>,          // e
                                                  Field<PhysDim,TopoDim,Real>>::type    // eA
                                                  ::template FieldCellGroupType<Topology>& fldsCell,
             const int traceGroupGlobal,
             const typename XFieldType::template  FieldTraceGroupType<TopologyTrace>& xfldTrace,
             const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>,        // qI
                                                  Field<PhysDim,TopoDim,ArrayQ>,        // wI
                                                  Field<PhysDim,TopoDim,Real>>::type    // eI
                                                  ::template FieldTraceGroupType<TopologyTrace>& fldsTrace,
             int quadratureorder )
  {
    typedef typename XFieldType                           ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>      ::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename Field<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<Topology> AFieldCellGroupType;
    typedef typename Field<PhysDim, TopoDim, Real  >      ::template FieldCellGroupType<Topology> EFieldCellGroupType;
    typedef typename Field<PhysDim, TopoDim, Real  >      ::template FieldCellGroupType<Topology> EAFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldCellClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldCellClass;
    typedef typename AFieldCellGroupType::template ElementType<> ElementAFieldCellClass;
    typedef typename EFieldCellGroupType::template ElementType<> ElementEFieldCellClass;
    typedef typename EAFieldCellGroupType::template ElementType<> ElementEAFieldCellClass;

    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;
    typedef typename Field<PhysDim, TopoDim, Real  >::template FieldTraceGroupType<TopologyTrace> EFieldTraceGroupType;

    typedef typename QFieldTraceGroupType::template ElementType<> ElementQFieldTraceClass;
    typedef typename EFieldTraceGroupType::template ElementType<> ElementEFieldTraceClass;

    typedef typename XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    // separating off Q, W and casting away the const on E fields
    const QFieldCellGroupType& qfldCell = get<0>(fldsCell);
    const AFieldCellGroupType& afldCell = get<1>(fldsCell);
    const QFieldCellGroupType& wfldCell = get<2>(fldsCell);
    const AFieldCellGroupType& bfldCell = get<3>(fldsCell);
    EFieldCellGroupType& efldCell = const_cast<EFieldCellGroupType&>(get<4>(fldsCell));
    EAFieldCellGroupType& eAfldCell = const_cast<EAFieldCellGroupType&>(get<5>(fldsCell));

    const QFieldTraceGroupType& qIfldTrace = get<0>(fldsTrace);
    const QFieldTraceGroupType& wIfldTrace = get<1>(fldsTrace);
    EFieldTraceGroupType& eIfldTrace = const_cast<EFieldTraceGroupType&>(get<2>(fldsTrace));

    // element field variables
    ElementXFieldCellClass xfldElem( xfldCell.basis() );
    ElementQFieldCellClass qfldElem( qfldCell.basis() );
    ElementAFieldCellClass afldElem( afldCell.basis() );
    ElementQFieldCellClass wfldElem( wfldCell.basis() );
    ElementAFieldCellClass bfldElem( bfldCell.basis() );
    ElementEFieldCellClass efldElem( efldCell.basis() );
    ElementEAFieldCellClass eAfldElem( eAfldCell.basis() );

    ElementQFieldTraceClass qIfldElemTrace( qIfldTrace.basis() );
    ElementQFieldTraceClass wIfldElemTrace( wIfldTrace.basis() );
    ElementEFieldTraceClass eIfldElemTrace( eIfldTrace.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    //Checking they're a P0 fields
    SANS_ASSERT( efldElem.order() == 0 );
    SANS_ASSERT( eAfldElem.order() == 0 );
    SANS_ASSERT( eIfldElemTrace.order() == 0 );

    // trace element integral
    typedef IntegrandHDG< Real, Real > IntegrandCellType;
    typedef Real IntegrandTraceType;

    ElementIntegral<TopoDimTrace, TopologyTrace, IntegrandCellType, IntegrandTraceType > integral(quadratureorder);

    IntegrandCellType rsdElemCell;
    IntegrandTraceType rsdElemTrace;

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      const int elemCell = xfldTrace.getElementLeft( elem );
      CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // copy global grid/solution DOFs to element
      xfldCell.getElement( xfldElem, elemCell );
      qfldCell.getElement( qfldElem, elemCell );
      afldCell.getElement( afldElem, elemCell );
      wfldCell.getElement( wfldElem, elemCell );
      bfldCell.getElement( bfldElem, elemCell );

      xfldTrace.getElement(  xfldElemTrace,  elem );
      qIfldTrace.getElement(  qIfldElemTrace,  elem );
      wIfldTrace.getElement(  wIfldElemTrace,  elem );

      rsdElemCell = 0;
      rsdElemTrace = 0;

      integral( fcnBTrace_.integrand( xfldElemTrace, canonicalTraceL,
                                      xfldElem,
                                      qfldElem, afldElem,
                                      wfldElem, bfldElem,
                                      qIfldElemTrace, wIfldElemTrace ),
                   get<-1>(xfldElemTrace), rsdElemCell, rsdElemTrace );

      //PDE
      efldCell.getElement( efldElem, elemCell );
      efldElem.DOF(0) += rsdElemCell.PDE;
      efldCell.setElement( efldElem, elemCell );

      //AUX
      eAfldCell.getElement( eAfldElem, elemCell );
      eAfldElem.DOF(0) += rsdElemCell.Au;
      eAfldCell.setElement( eAfldElem, elemCell );

      //INT
      eIfldTrace.getElement( eIfldElemTrace, elem );
      eIfldElemTrace.DOF(0) += rsdElemTrace;
      eIfldTrace.setElement( eIfldElemTrace, elem );
    }
  }

protected:
  const IntegrandBoundaryTrace& fcnBTrace_;
};

// Factory function

template<class IntegrandBTrace>
ErrorEstimateBoundaryTrace_HDG_impl<IntegrandBTrace>
ErrorEstimateBoundaryTrace_HDG( const IntegrandBoundaryTraceType<IntegrandBTrace>& fcnBTrace )
{
  return ErrorEstimateBoundaryTrace_HDG_impl<IntegrandBTrace>(fcnBTrace.cast());
}

}

#endif  // ERRORESTIMATEBOUNDARYTRACE_HDG_H
