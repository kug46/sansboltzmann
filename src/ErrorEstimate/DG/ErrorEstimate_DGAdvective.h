// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ERRORESTIMATE_DGADVECTIVE_H
#define ERRORESTIMATE_DGADVECTIVE_H

// Cell integral residual functions

#include <memory> // shared_ptr
#include <vector>

#include "tools/Tuple.h"

#include "pde/BCParameters.h"

#include "Field/XField.h"
#include "Field/Field.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

#include "Discretization/QuadratureOrder.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"

#include "Discretization/Galerkin/IntegrateBoundaryTrace_Dispatch_Galerkin.h"

// Estimation classes
#include "ErrorEstimate/Galerkin/ErrorEstimateBase_Galerkin.h"
#include "ErrorEstimate/Galerkin/ErrorEstimateCell_Galerkin.h"
#include "ErrorEstimate/Galerkin/ErrorEstimateInteriorTrace_Galerkin.h"
#include "ErrorEstimate/Galerkin/ErrorEstimateBoundaryTrace_Dispatch_Galerkin.h"

#include "ErrorEstimate/Galerkin/ErrorEstimateBoundaryTrace_FieldTrace_Galerkin.h"
#include "ErrorEstimate/Galerkin/ErrorEstimateBoundaryTrace_Galerkin.h"

// Main Integrands
#include "Discretization/Galerkin/IntegrandCell_Galerkin.h"
#include "Discretization/Galerkin/IntegrandInteriorTrace_Galerkin.h"

// Boundary Integrands
#include "Discretization/Galerkin/IntegrandBoundaryTrace_None_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Dirichlet_sansLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Flux_mitState_Nitsche.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_mitLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_sansLG_Galerkin.h"

#include "Discretization/DG/FieldBundle_DGAdvective.h"
#include "Discretization/Galerkin/Stabilization_Nitsche.h"

#include "tools/make_unique.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Field weighted Estimates for Galerkin
// Combines Cell, interior trace and boundary trace integrals together
//----------------------------------------------------------------------------//
//

// Discretization (specifically, integrands) identification tag
class DGAdv;

template< class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class ParamFieldType >
class ErrorEstimate_DGAdvective :
    public ErrorEstimateBase_Galerkin<typename NDPDEClass::PhysDim, typename ParamFieldType::TopoDim, typename NDPDEClass::template ArrayQ<Real>>
{
public:
  typedef typename NDPDEClass::PhysDim PhysDim;
  typedef typename ParamFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef ErrorEstimateBase_Galerkin<PhysDim, TopoDim, ArrayQ> BaseType;

  typedef BCParameters<BCVector> BCParams;
  typedef IntegrandCell_Galerkin<NDPDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_Galerkin<NDPDEClass> IntegrandTraceClass;
  typedef IntegrateBoundaryTrace_Dispatch_Galerkin<NDPDEClass, BCNDConvert, BCVector, Galerkin> IntegrateBoundaryTrace_Dispatch;
  //TODO: disc tag to be updated

  typedef FieldBundleBase_DGAdvective<PhysDim,TopoDim,ArrayQ> FieldBundle;

  // Constructor
  template<class... BCArgs>
  ErrorEstimate_DGAdvective( const ParamFieldType& paramfld,
                             const Field<PhysDim, TopoDim, ArrayQ>& qfld, // q
                             const Field<PhysDim, TopoDim, ArrayQ>& lgfld, // lg
                             const Field<PhysDim, TopoDim, ArrayQ>& wfld, // w
                             const Field<PhysDim, TopoDim, ArrayQ>& mufld, // mu
                             std::shared_ptr<Field<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                             const NDPDEClass& pde,
                             const QuadratureOrder& quadratureOrder,
                             const std::vector<int>& CellGroups,
                             const std::vector<int>& InteriorTraceGroups,
                             PyDict& BCList,
                             const std::map< std::string, std::vector<int> >& BCBoundaryGroups, BCArgs&... args)
                             : BaseType(get<-1>(paramfld), qfld, lgfld, wfld, mufld, pLiftedQuantityfld, CellGroups,
                               BCParams::getLGBoundaryGroups(BCList, BCBoundaryGroups)),
                               paramfld_(paramfld),
                               pde_(pde), quadratureOrder_(quadratureOrder),
                               fcnCell_(pde, CellGroups), fcnIntTrace_( pde, InteriorTraceGroups ),
                               BCs_(BCParams::template createBCs<BCNDConvert>(pde, BCList, args...)),
                               stab_(1),
                               dispatchBC_(pde, BCList, BCs_, BCBoundaryGroups, stab_)
  {
    if ( pde_.hasSourceLiftedQuantity() )
      SANS_ASSERT_MSG(this->pLiftedQuantityfld_, "ErrorEstimate_DGAdvective - The lifted quantity field has not been initialized!");

    estimate();
  }

  template<class... BCArgs>
  ErrorEstimate_DGAdvective( const ParamFieldType& paramfld,
                             const Field<PhysDim, TopoDim, ArrayQ>& qfld, // q
                             const Field<PhysDim, TopoDim, ArrayQ>& lgfld, // lg
                             const Field<PhysDim, TopoDim, ArrayQ>& wfld, // w
                             const Field<PhysDim, TopoDim, ArrayQ>& mufld, // mu
                             const NDPDEClass& pde,
                             const QuadratureOrder& quadratureOrder,
                             const std::vector<int>& CellGroups,
                             const std::vector<int>& InteriorTraceGroups,
                             PyDict& BCList,
                             const std::map< std::string, std::vector<int> >& BCBoundaryGroups, BCArgs&... args)
                             : ErrorEstimate_DGAdvective(paramfld, qfld, lgfld, wfld, mufld, {}, pde, quadratureOrder,
                                                         CellGroups, InteriorTraceGroups, BCList, BCBoundaryGroups, args...) {}

  // Alternative Agnostic Constructor
  template<class... BCArgs>
  ErrorEstimate_DGAdvective( const ParamFieldType& paramfld,
                             const FieldBundle& primal,   // (q, lg)
                             const FieldBundle& adjoint,  // (w, mu)
                             std::shared_ptr<Field<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                             const NDPDEClass& pde,
                             const DiscretizationObject& stab,
                             const QuadratureOrder& quadratureOrder,
                             const std::vector<int>& CellGroups,
                             const std::vector<int>& InteriorTraceGroups,
                             PyDict& BCList,
                             const std::map< std::string, std::vector<int> >& BCBoundaryGroups, BCArgs&... args)
                             : ErrorEstimate_DGAdvective(paramfld, primal.qfld, primal.lgfld, adjoint.qfld, adjoint.lgfld, {},
                                                         pde, quadratureOrder, CellGroups, InteriorTraceGroups,
                                                         BCList, BCBoundaryGroups, args...) {}

  std::size_t nCellGroups() const { return fcnCell_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcnCell_.cellGroup(n); }

protected:
  using BaseType::xfld_;
  using BaseType::active_BGroup_list_;

  using BaseType::efld_;    // q field estimates
  using BaseType::ifld_;    // indicator field
  using BaseType::eSfld_;   // estimate sum field
  using BaseType::up_eBfld_;

  using BaseType::qfld_; // q
  using BaseType::lgfld_; // lg
  using BaseType::wfld_; // w
  using BaseType::mufld_; // mu
  using BaseType::pLiftedQuantityfld_; //pointer to lifted quantity field

  const ParamFieldType& paramfld_;

  const NDPDEClass& pde_;
  const QuadratureOrder quadratureOrder_;

  IntegrandCellClass fcnCell_;
  IntegrandTraceClass fcnIntTrace_;

  std::map< std::string, std::shared_ptr<BCBase> > BCs_;

  StabilizationNitsche stab_;
  IntegrateBoundaryTrace_Dispatch dispatchBC_;

  //----------------------------------------------------------------------------//
  // Estimate function that populates the efld_ and eBfld_, called in constructor
  void
  estimate( )
  {
    efld_ = 0; // zero the internal fields

    if ( !active_BGroup_list_.empty() )
    {
      up_eBfld_ = SANS::make_unique<Field_DG_BoundaryTrace<PhysDim, TopoDim, Real>>
          (xfld_, 0, BasisFunctionCategory_Legendre, active_BGroup_list_);

      *(up_eBfld_.get()) = 0;
    }

    // Cell Estimates
    IntegrateCellGroups<TopoDim>::integrate(ErrorEstimateCell_Galerkin(fcnCell_),
                                            paramfld_, (qfld_, wfld_, efld_),
                                            quadratureOrder_.cellOrders.data(),
                                            quadratureOrder_.cellOrders.size()  );
    // Interior Trace Estimates
    IntegrateInteriorTraceGroups<TopoDim>::integrate(ErrorEstimateInteriorTrace_Galerkin(fcnIntTrace_),
                                                     paramfld_,
                                                     (qfld_, wfld_, efld_),
                                                     quadratureOrder_.interiorTraceOrders.data(),
                                                     quadratureOrder_.interiorTraceOrders.size());
    // Boundary Trace Estimates
    dispatchBC_.dispatch(
        ErrorEstimateBoundaryTrace_FieldTrace_Dispatch_Galerkin(  paramfld_,
                                                                  qfld_, wfld_, efld_,
                                                                  lgfld_,mufld_, up_eBfld_,
                                                                  quadratureOrder_.boundaryTraceOrders.data(),
                                                                  quadratureOrder_.boundaryTraceOrders.size()),
        ErrorEstimateBoundaryTrace_Dispatch_Galerkin( paramfld_,
                                                      qfld_, wfld_, efld_,
                                                      quadratureOrder_.boundaryTraceOrders.data(),
                                                      quadratureOrder_.boundaryTraceOrders.size())
    );
  }
};

} //namespace SANS

#endif  // ERRORESTIMATE_DGADVECTIVE_H
