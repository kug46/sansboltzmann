// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#if !defined(ERRORESTIMATE_DGBR2_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "ErrorEstimate_DGBR2.h"

#include "tools/KahanSum.h"
#include "tools/Tuple.h"

#include "Field/tools/for_each_CellGroup.h"
#include "Field/tools/for_each_BoundaryFieldTraceGroup_Cell.h"
#include "Field/Tuple/FieldTuple.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h" //DiscBCTag

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"
#include "Discretization/IntegratePeriodicTraceGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"

// Estimation classes
#include "ErrorEstimate/DG/ErrorEstimateCell_DGBR2.h"
#include "ErrorEstimate/DG/ErrorEstimateInteriorTrace_DGBR2.h"
#include "ErrorEstimate/DG/ErrorEstimateBoundaryTrace_Dispatch_DGBR2.h"
#include "ErrorEstimate/Galerkin/ErrorEstimateBoundaryTrace_Dispatch_Galerkin.h"

#include "ErrorEstimate/DG/ErrorEstimateBoundaryTrace_DGBR2.h"
#include "ErrorEstimate/Galerkin/ErrorEstimateBoundaryTrace_Dirichlet_mitLG_Galerkin.h"
#include "ErrorEstimate/Galerkin/ErrorEstimateBoundaryTrace_FieldTrace_Galerkin.h"
#include "ErrorEstimate/Galerkin/ErrorEstimateBoundaryTrace_Galerkin.h"

// Interior Integrands
#include "Discretization/DG/IntegrandCell_DGBR2.h"
#include "Discretization/DG/IntegrandInteriorTrace_DGBR2.h"

// Boundary Integrands
#include "Discretization/Galerkin/IntegrandBoundaryTrace_None_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Dirichlet_mitLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Dirichlet_sansLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_mitLG_Galerkin.h"
#include "Discretization/DG/IntegrandBoundaryTrace_LinearScalar_sansLG_DGBR2.h"
#include "Discretization/DG/IntegrandBoundaryTrace_Flux_mitState_DGBR2.h"

#include "ErrorEstimate/DistributeBoundaryTraceGroups.h"

#include "tools/make_unique.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#endif

namespace SANS
{

//---------------------------------------------------------------------------//
// Constructor
template< class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class ParamFieldType >
template<class... BCArgs>
ErrorEstimate_DGBR2<NDPDEClass, BCNDConvert, BCVector, ParamFieldType>::
ErrorEstimate_DGBR2( const ParamFieldType& paramfld,
                     const Field<PhysDim, TopoDim, ArrayQ>& qfld, // q
                     const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld, // r
                     const Field<PhysDim, TopoDim, ArrayQ>& lgfld, // lg
                     const Field<PhysDim, TopoDim, ArrayQ>& wfld, // w
                     const FieldLift<PhysDim, TopoDim, VectorArrayQ>& sfld, // s
                     const Field<PhysDim, TopoDim, ArrayQ>& mufld, // mu
                     std::shared_ptr<Field_DG_Cell<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                     const NDPDEClass& pde,
                     const DiscretizationDGBR2& disc,
                     const QuadratureOrder& quadratureOrder,
                     const std::vector<int>& CellGroups,
                     const std::vector<int>& InteriorTraceGroups,
                     PyDict& BCList,
                     const std::map< std::string, std::vector<int> >& BCBoundaryGroups, BCArgs&... args)
                     : paramfld_(paramfld),
                       xfld_(paramfld_.getXField()),
                       active_BGroup_list_(BCParams::getLGBoundaryGroups(BCList, BCBoundaryGroups)),
                       efld_ (xfld_, 0, BasisFunctionCategory_Legendre),
                       eLfld_(xfld_, 0, BasisFunctionCategory_Legendre),
                       qfld_(qfld), rfld_(rfld), lgfld_(lgfld), wfld_(wfld), sfld_(sfld), mufld_(mufld),
                       pLiftedQuantityfld_(pLiftedQuantityfld),
                       pde_(pde), quadratureOrder_(quadratureOrder),
                       fcnCell_(pde, disc, CellGroups),
                       fcnIntTrace_( pde, disc, InteriorTraceGroups, paramfld.getXField().getPeriodicTraceGroupsGlobal(CellGroups) ),
                       BCs_(BCParams::template createBCs<BCNDConvert>(pde, BCList, args...)),
                       dispatchBC_(pde, BCList, BCs_, BCBoundaryGroups, disc),
                       cellGroups_(CellGroups)
{
  ifldCalc_ = false; eSfldCalc_ = false;
  check(); // checks that all the fields come from the same grid
  estimate(); // calculates the estimates
}

template< class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class ParamFieldType >
template<class... BCArgs>
ErrorEstimate_DGBR2<NDPDEClass, BCNDConvert, BCVector, ParamFieldType>::
ErrorEstimate_DGBR2( const ParamFieldType& paramfld,
                     const Field<PhysDim, TopoDim, ArrayQ>& qfld, // q
                     const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld, // r
                     const Field<PhysDim, TopoDim, ArrayQ>& lgfld, // lg
                     const Field<PhysDim, TopoDim, ArrayQ>& wfld, // w
                     const FieldLift<PhysDim, TopoDim, VectorArrayQ>& sfld, // s
                     const Field<PhysDim, TopoDim, ArrayQ>& mufld, // mu
                     const NDPDEClass& pde,
                     const DiscretizationDGBR2& disc,
                     const QuadratureOrder& quadratureOrder,
                     const std::vector<int>& CellGroups,
                     const std::vector<int>& InteriorTraceGroups,
                     PyDict& BCList,
                     const std::map< std::string, std::vector<int> >& BCBoundaryGroups, BCArgs&... args)
                     : ErrorEstimate_DGBR2(paramfld, qfld, rfld, lgfld, wfld, sfld, mufld, {},
                                           pde, disc, quadratureOrder, CellGroups, InteriorTraceGroups,
                                           BCList, BCBoundaryGroups, args...) {}

//---------------------------------------------------------------------------//
// Alternative Agnostic Constructor
template< class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class ParamFieldType >
template<class... BCArgs>
ErrorEstimate_DGBR2<NDPDEClass, BCNDConvert, BCVector, ParamFieldType>::
ErrorEstimate_DGBR2( const ParamFieldType& paramfld,
                     const FieldBundle& primal,   // (q, r, lg)
                     const FieldBundle& adjoint,  // (w, s, mu)
                     std::shared_ptr<Field_DG_Cell<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                     const NDPDEClass& pde,
                     const DiscretizationDGBR2& disc,
                     const QuadratureOrder& quadratureOrder,
                     const std::vector<int>& CellGroups,
                     const std::vector<int>& InteriorTraceGroups,
                     PyDict& BCList,
                     const std::map< std::string, std::vector<int> >& BCBoundaryGroups, BCArgs&... args)
                     : ErrorEstimate_DGBR2(paramfld, primal.qfld, primal.rfld, primal.lgfld,
                                           adjoint.qfld, adjoint.rfld, adjoint.lgfld, pLiftedQuantityfld,
                                           pde, disc, quadratureOrder, CellGroups, InteriorTraceGroups,
                                           BCList, BCBoundaryGroups, args...)
{}

template<class PhysDim>
class DistributeLiftingOperator:
    public GroupFunctorCellType<DistributeLiftingOperator<PhysDim>>
{
public:

  explicit DistributeLiftingOperator( const std::vector<int>& cellGroups, const bool absFlag = false ) :
  cellGroups_(cellGroups), absFlag_(absFlag) {}

  std::size_t nCellGroups()          const { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n];     }

  //----------------------------------------------------------------------------//
  // Distribution function that redistributes the error in each cell group
  template< class Topology>
  void
  apply(
      const typename FieldTuple< Field<PhysDim, typename Topology::TopoDim, Real>,
                                 FieldLift_DG_Cell<PhysDim, typename Topology::TopoDim, Real>,TupleClass<>>::
                     template FieldCellGroupType<Topology>& fldsCell,
      const int cellGroupGlobal)
  {
    // Cell Group Types
    typedef typename Field< PhysDim,typename Topology::TopoDim, Real   >::template FieldCellGroupType<Topology> EFieldCellGroupType;
    typedef typename FieldLift< PhysDim,typename Topology::TopoDim,Real>::template FieldCellGroupType<Topology> ELFieldCellGroupType;

    typedef typename EFieldCellGroupType::template ElementType<>  ElementEFieldClass;
    typedef ElementLift<Real,typename Topology::TopoDim,Topology>                    ElementELFieldClass;

    // Apportioning the eLfld to efld
    // loop over groups
    EFieldCellGroupType& efldCell = const_cast<EFieldCellGroupType&>(get<0>(fldsCell));
    const ELFieldCellGroupType& eLfldCell = get<1>(fldsCell);

    ElementEFieldClass efldElem(efldCell.basis() );
    ElementELFieldClass eLfldElem(efldCell.basis() );

    if (absFlag_ == false) // logic performed before loop for speed
    { // DEFAULT BRANCH
      // add the components of the eLfld into the efld
      for (int elem = 0; elem < efldCell.nElem(); elem++)
      {
        efldCell.getElement(efldElem,elem);
        eLfldCell.getElement(eLfldElem,elem);

        for (int trace = 0; trace < Topology::NTrace; trace++)
          efldElem.DOF(0) += eLfldElem[trace].DOF(0);

        efldCell.setElement(efldElem,elem);
      }
    }
    else
    { // OPTIONAL BRANCH
      // adds the squared version
      for (int elem = 0; elem < efldCell.nElem(); elem++)
      {
        efldCell.getElement(efldElem,elem);
        eLfldCell.getElement(eLfldElem,elem);

        for (int trace = 0; trace < Topology::NTrace; trace++)
          efldElem.DOF(0) += fabs(eLfldElem[trace].DOF(0));

        efldCell.setElement(efldElem,elem);
      }
    }
  }

protected:
  const std::vector<int> cellGroups_;
  const bool absFlag_;
};

//---------------------------------------------------------------------------//
template< class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class ParamFieldType >
void
ErrorEstimate_DGBR2<NDPDEClass, BCNDConvert, BCVector, ParamFieldType>::
calculateSumEstimates()
{
  up_eSfld_ = SANS::make_unique<Field_DG_Cell<PhysDim,TopoDim,Real>>(efld_,FieldCopy());

  // Distributing the Lifting Operator estimate field
  for_each_CellGroup<TopoDim>::apply( DistributeLiftingOperator<PhysDim>(cellGroups_),(*up_eSfld_,eLfld_));

  // Distributing the boundary estimate field
  if (up_eBfld_)
  {
    for_each_BoundaryFieldTraceGroup_Cell<TopoDim>::apply(
        DistributeBoundaryTraceField<PhysDim>(active_BGroup_list_), *up_eSfld_, *up_eBfld_ ); // Lagrange Fields
  }

  eSfldCalc_ = true; // only place this is changed
}

//---------------------------------------------------------------------------//
template< class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class ParamFieldType >
void
ErrorEstimate_DGBR2<NDPDEClass, BCNDConvert, BCVector, ParamFieldType>::
getErrorEstimate( Real& estimate, const std::vector<int>& cellGroups )
{
  if (eSfldCalc_ == false)
    calculateSumEstimates();

  int comm_rank = up_eSfld_->comm()->rank();

  // Sum up the total
  KahanSum<Real> Sum=0;
  for_each_CellGroup<TopoDim>::apply( FieldSum<PhysDim>(Sum,cellGroups,comm_rank),*up_eSfld_ );
  estimate = Sum;

#ifdef SANS_MPI
  estimate = boost::mpi::all_reduce(*qfld_.comm(), estimate, std::plus<Real>());
#endif
}


//---------------------------------------------------------------------------//
template< class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class ParamFieldType >
void
ErrorEstimate_DGBR2<NDPDEClass, BCNDConvert, BCVector, ParamFieldType>::
getLocalSolveErrorEstimate( std::vector<Real>& estimate )
{
  if (eSfldCalc_ == false)
    calculateSumEstimates();

  std::vector<std::vector<Real>> dummyVector;
  fillVector(dummyVector, *up_eSfld_, {0});

  SANS_ASSERT( dummyVector[0].size() % 2 == 0);

  int originalSize = dummyVector[0].size()/2;
  estimate.resize( originalSize );
  for (int i = 0; i < originalSize; i++ )
    estimate[i] = dummyVector[0][i] + dummyVector[0][i+originalSize];
}

//---------------------------------------------------------------------------//
template< class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class ParamFieldType >
void
ErrorEstimate_DGBR2<NDPDEClass, BCNDConvert, BCVector, ParamFieldType>::
calculateIndicators()
{
  // eta = abs(eta_w) + abs(eta_r) + abs(eta_b)

  up_ifld_ = SANS::make_unique<Field_DG_Cell<PhysDim,TopoDim,Real>>(efld_,FieldCopy());
  for (int i = 0; i < up_ifld_->nDOF(); i++)
    up_ifld_->DOF(i) = fabs(up_ifld_->DOF(i));

  // Distributing the Lifting Operator estimate field
  for_each_CellGroup<TopoDim>::apply( DistributeLiftingOperator<PhysDim>(cellGroups_,true),(*up_ifld_,eLfld_));

  if (up_eBfld_)
  {
    for_each_BoundaryFieldTraceGroup_Cell<TopoDim>::apply(
        DistributeBoundaryTraceField<PhysDim>(active_BGroup_list_,true), *up_ifld_, *up_eBfld_ ); // Lagrange Fields
  }

  ifldCalc_ = true; // only place this is changed
}

//---------------------------------------------------------------------------//
template< class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class ParamFieldType >
void
ErrorEstimate_DGBR2<NDPDEClass, BCNDConvert, BCVector, ParamFieldType>::
getErrorIndicator( Real& estimate, const std::vector<int>& cellGroups )
{
  if (ifldCalc_ == false)
    calculateIndicators();

  int comm_rank = up_ifld_->comm()->rank();

  // Sum up the total
  KahanSum<Real> Sum=0;
  for_each_CellGroup<TopoDim>::apply( FieldSum<PhysDim>(Sum,cellGroups,comm_rank), *up_ifld_ );
  estimate = (Real)Sum;

#ifdef SANS_MPI
  estimate = boost::mpi::all_reduce(*qfld_.comm(), estimate, std::plus<Real>());
#endif
}


//---------------------------------------------------------------------------//
template< class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class ParamFieldType >
void
ErrorEstimate_DGBR2<NDPDEClass, BCNDConvert, BCVector, ParamFieldType>::
getLocalSolveErrorIndicator( std::vector<Real>& estimate )
{
  if (ifldCalc_ == false)
    calculateIndicators();

  std::vector<std::vector<Real>> dummyVector;
  fillVector(dummyVector, *up_ifld_, {0});

  SANS_ASSERT( dummyVector[0].size() % 2 == 0);

  int originalSize = dummyVector[0].size()/2;
  estimate.resize( originalSize );
  for (int i = 0; i < originalSize; i++ )
    estimate[i] = fabs(dummyVector[0][i]) + fabs(dummyVector[0][i+originalSize]);
}

//---------------------------------------------------------------------------//
template< class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class ParamFieldType >
void
ErrorEstimate_DGBR2<NDPDEClass, BCNDConvert, BCVector, ParamFieldType>::
fillEArray(std::vector<std::vector<Real>>& estimates)
{
  // Accumulates a field into the less safe vector of vectors format, useful for communicating with external classes
  if (ifldCalc_ == false)
    calculateIndicators();

  estimates.resize(cellGroups_.size());
  for (std::size_t i = 0; i < estimates.size(); i++ )
    estimates[i].resize(xfld_.getCellGroupBaseGlobal(cellGroups_[i]).nElem());

  // Populate the vector of vectors from ifld_
  for_each_CellGroup<TopoDim>::apply( AccumulateVector<PhysDim>(estimates,cellGroups_), *up_ifld_ );
}


//---------------------------------------------------------------------------//
template< class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class ParamFieldType >
void
ErrorEstimate_DGBR2<NDPDEClass, BCNDConvert, BCVector, ParamFieldType>::
fillVector(std::vector<std::vector<Real>>& accVector,
           const Field_DG_Cell<PhysDim,TopoDim,Real>& accFld,
           const std::vector<int>& cellGroups)
{
  // Accumulates a field into the less safe vector of vectors format, useful for communicating with external classes

  accVector.resize(cellGroups.size());
  for (std::size_t i = 0; i < accVector.size(); i++ )
    accVector[i].resize(xfld_.getCellGroupBaseGlobal(cellGroups[i]).nElem());

  // Populate the vector of vectors from ifld_
  for_each_CellGroup<TopoDim>::apply( AccumulateVector<PhysDim>(accVector,cellGroups), accFld );
}

//---------------------------------------------------------------------------//
template< class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class ParamFieldType >
void
ErrorEstimate_DGBR2<NDPDEClass, BCNDConvert, BCVector, ParamFieldType>::
estimate( )
{
  efld_ = 0; eLfld_ = 0;// zero the internal fields

  if ( !active_BGroup_list_.empty() )
  {
    up_eBfld_ = SANS::make_unique<Field_DG_BoundaryTrace<PhysDim, TopoDim, Real>>
        (xfld_, 0, BasisFunctionCategory_Legendre, active_BGroup_list_);

    *up_eBfld_ = 0; // set the data at the end of the pointer to 0
  }

  // Cell Estimates
  if (!pde_.hasSourceLiftedQuantity())
  {
    IntegrateCellGroups<TopoDim>::integrate( ErrorEstimateCell_DGBR2(fcnCell_),
                                             paramfld_,
                                             (qfld_, rfld_, wfld_, sfld_, efld_, eLfld_),
                                             quadratureOrder_.cellOrders.data(),
                                             quadratureOrder_.cellOrders.size() );
  }
  else
  {
    IntegrateCellGroups<TopoDim>::integrate( ErrorEstimateCell_DGBR2(fcnCell_),
                                             paramfld_,
                                             (qfld_, rfld_, wfld_, sfld_, *pLiftedQuantityfld_, efld_, eLfld_),
                                             quadratureOrder_.cellOrders.data(),
                                             quadratureOrder_.cellOrders.size() );
  }

  // Interior Trace Estimates
  IntegrateInteriorTraceGroups<TopoDim>::integrate( ErrorEstimateInteriorTrace_DGBR2(fcnIntTrace_),
                                                    paramfld_,
                                                    (qfld_, rfld_, wfld_, sfld_, efld_, eLfld_),
                                                    quadratureOrder_.interiorTraceOrders.data(),
                                                    quadratureOrder_.interiorTraceOrders.size() );

  // Periodic Trace Estimates
  IntegratePeriodicTraceGroups<TopoDim>::integrate( ErrorEstimateInteriorTrace_DGBR2(fcnIntTrace_),
                                                    paramfld_,
                                                    (qfld_, rfld_, wfld_, sfld_, efld_, eLfld_),
                                                    quadratureOrder_.boundaryTraceOrders.data(),
                                                    quadratureOrder_.boundaryTraceOrders.size() );

  // Boundary Trace Estimates
  dispatchBC_.dispatch(
      ErrorEstimateBoundaryTrace_FieldTrace_Dispatch_Galerkin(  paramfld_,
                                                                qfld_, wfld_, efld_,
                                                                lgfld_,mufld_,up_eBfld_,
                                                                quadratureOrder_.boundaryTraceOrders.data(),
                                                                quadratureOrder_.boundaryTraceOrders.size() ),
      ErrorEstimateBoundaryTrace_mitState_Dispatch_DGBR2( paramfld_,
                                                          qfld_, rfld_, wfld_, sfld_, efld_, eLfld_,
                                                          quadratureOrder_.boundaryTraceOrders.data(),
                                                          quadratureOrder_.boundaryTraceOrders.size()),
      ErrorEstimateBoundaryTrace_Dispatch_Galerkin( paramfld_,
                                                    qfld_, wfld_, efld_,
                                                    quadratureOrder_.boundaryTraceOrders.data(),
                                                    quadratureOrder_.boundaryTraceOrders.size() )
  );

  // get everyone to agree
  // efld_.syncDOFs_MPI_noCache();
  // eLfld_.syncDOFs_MPI_noCache();
  // eBfld_.syncDOFs_MPI_noCache();

}

//---------------------------------------------------------------------------//
template< class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class ParamFieldType >
void
ErrorEstimate_DGBR2<NDPDEClass, BCNDConvert, BCVector, ParamFieldType>::
outputFields( const std::string& filename )
{
  if (eSfldCalc_ == false)
    calculateSumEstimates();

  if (ifldCalc_ == false)
    calculateIndicators();

  Field_DG_Cell<PhysDim,TopoDim,DLA::VectorS<4,Real>> ofld_(xfld_, 0, BasisFunctionCategory_Legendre);

  for (int n = 0; n < ofld_.nDOF(); n++)
  {
    ofld_.DOF(n)[0] = up_eSfld_->DOF(n);
    ofld_.DOF(n)[1] = up_ifld_->DOF(n);
    ofld_.DOF(n)[2] = efld_.DOF(n);
    ofld_.DOF(n)[3] = up_eSfld_->DOF(n) - efld_.DOF(n);
  }
  ofld_.syncDOFs_MPI_noCache();
  output_Tecplot( ofld_, filename, {"eSfld","ifld","efld","eLfld"} );
}


#include <boost/preprocessor/cat.hpp>

//---------------------------------------------------------------------------//
// Helper macros to reduce the amount of work needed to instantiate ErrorEstimate_DGBR2
#define ERRORESTIMATE_DGBR2_INSTANTIATE_TRAITS(PDEND, BCNDCONVERT, BCVECTOR, TOPODIM, PARAMFIELDTUPLE ) \
template class ErrorEstimate_DGBR2< PDEND, BCNDCONVERT, BCVECTOR, PARAMFIELDTUPLE >; \
\
template ErrorEstimate_DGBR2< PDEND, BCNDCONVERT, BCVECTOR, PARAMFIELDTUPLE >:: \
         ErrorEstimate_DGBR2( const PARAMFIELDTUPLE& xfld, \
                              const Field    <PDEND::PhysDim, TOPODIM, PDEND::ArrayQ<Real>       >& qfld, \
                              const FieldLift<PDEND::PhysDim, TOPODIM, PDEND::VectorArrayQ<Real> >& rfld, \
                              const Field    <PDEND::PhysDim, TOPODIM, PDEND::ArrayQ<Real>       >& lgfld, \
                              const Field    <PDEND::PhysDim, TOPODIM, PDEND::ArrayQ<Real>       >& wfld, \
                              const FieldLift<PDEND::PhysDim, TOPODIM, PDEND::VectorArrayQ<Real> >& sfld, \
                              const Field    <PDEND::PhysDim, TOPODIM, PDEND::ArrayQ<Real>       >& mufld, \
                              const PDEND& pde, \
                              const DiscretizationDGBR2& disc, \
                              const QuadratureOrder& quadratureOrder, \
                              const std::vector<int>& CellGroups, \
                              const std::vector<int>& InteriorTraceGroups, \
                              PyDict& BCList, \
                              const std::map< std::string, std::vector<int> >& BCBoundaryGroups );\
template ErrorEstimate_DGBR2< PDEND, BCNDCONVERT, BCVECTOR, PARAMFIELDTUPLE >:: \
         ErrorEstimate_DGBR2( const PARAMFIELDTUPLE& xfld, \
                              const FieldBundle& primal, \
                              const FieldBundle& adjoint, \
                              std::shared_ptr<Field_DG_Cell<PhysDim, TopoDim, Real>> pLiftedQuantityfld, \
                              const PDEND& pde, \
                              const DiscretizationDGBR2& disc, \
                              const QuadratureOrder& quadratureOrder, \
                              const std::vector<int>& CellGroups, \
                              const std::vector<int>& InteriorTraceGroups, \
                              PyDict& BCList, \
                              const std::map< std::string, std::vector<int> >& BCBoundaryGroups );


#define ERRORESTIMATE_DGBR2_INSTANTIATE_UNSTEADY_TRAITS(PDEND, BCVECTOR, TOPODIM, PARAMFIELDTUPLE ) \
\
template ErrorEstimate_DGBR2< PDEND, BCNDConvertSpace, BCVECTOR, PARAMFIELDTUPLE >::  \
         ErrorEstimate_DGBR2( const PARAMFIELDTUPLE& xfld, \
                              const Field    <PDEND::PhysDim, TOPODIM, PDEND::ArrayQ<Real>       >& qfld,  \
                              const FieldLift<PDEND::PhysDim, TOPODIM, PDEND::VectorArrayQ<Real> >& rfld,  \
                              const Field    <PDEND::PhysDim, TOPODIM, PDEND::ArrayQ<Real>       >& lgfld, \
                              const Field    <PDEND::PhysDim, TOPODIM, PDEND::ArrayQ<Real>       >& wfld,  \
                              const FieldLift<PDEND::PhysDim, TOPODIM, PDEND::VectorArrayQ<Real> >& sfld,  \
                              const Field    <PDEND::PhysDim, TOPODIM, PDEND::ArrayQ<Real>       >& mufld, \
                              const PDEND& pde, \
                              const DiscretizationDGBR2& disc, \
                              const QuadratureOrder& quadratureOrder, \
                              const std::vector<int>& CellGroups, \
                              const std::vector<int>& InteriorTraceGroups, \
                              PyDict& BCList, \
                              const std::map< std::string, std::vector<int> >& BCBoundaryGroups, GlobalTime& );\
template ErrorEstimate_DGBR2< PDEND, BCNDConvertSpace, BCVECTOR, PARAMFIELDTUPLE >::  \
         ErrorEstimate_DGBR2( const PARAMFIELDTUPLE& xfld, \
                              const FieldBundle& primal, \
                              const FieldBundle& adjoint, \
                              std::shared_ptr<Field_DG_Cell<PhysDim, TopoDim, Real>> pLiftedQuantityfld, \
                              const PDEND& pde, \
                              const DiscretizationDGBR2& disc, \
                              const QuadratureOrder& quadratureOrder, \
                              const std::vector<int>& CellGroups, \
                              const std::vector<int>& InteriorTraceGroups, \
                              PyDict& BCList, \
                              const std::map< std::string, std::vector<int> >& BCBoundaryGroups, GlobalTime& );

//---------------------------------------------------------------------------//
// This is the main macro used for steady instantiation
#define ERRORESTIMATE_DGBR2_INSTANTIATE_SPACE( PDE, BCVECTOR, TOPODIM, PARAMFIELDTUPLE ) \
\
typedef PDENDConvertSpace<PDE::PhysDim, PDE> BOOST_PP_CAT(PDE, NDSpace); \
\
ERRORESTIMATE_DGBR2_INSTANTIATE_TRAITS( BOOST_PP_CAT(PDE, NDSpace), BCNDConvertSpace, BCVECTOR, TOPODIM, PARAMFIELDTUPLE ) \
\
ERRORESTIMATE_DGBR2_INSTANTIATE_UNSTEADY_TRAITS( BOOST_PP_CAT(PDE, NDSpace), BCVECTOR, TOPODIM, PARAMFIELDTUPLE )

//---------------------------------------------------------------------------//
// This is the main macro used for space-time instantiation
#define ERRORESTIMATE_DGBR2_INSTANTIATE_SPACETIME( PDE, BCVECTOR, TOPODIM, PARAMFIELDTUPLE ) \
\
typedef PDENDConvertSpaceTime<PDE::PhysDim, PDE> BOOST_PP_CAT(PDE, NDSpaceTime); \
\
ERRORESTIMATE_DGBR2_INSTANTIATE_TRAITS( BOOST_PP_CAT(PDE, NDSpaceTime), BCNDConvertSpaceTime, BCVECTOR, TOPODIM, PARAMFIELDTUPLE )

} // namespace SANS
