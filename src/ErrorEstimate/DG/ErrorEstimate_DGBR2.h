// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ERRORESTIMATE_DGBR2_H
#define ERRORESTIMATE_DGBR2_H

#include <vector> //vector

#include "pde/BCParameters.h"

#include "Field/XField.h"
#include "Field/Field.h"

#include "Discretization/DG/IntegrateBoundaryTrace_Dispatch_DGBR2.h"
#include "Discretization/DG/DiscretizationDGBR2.h"
#include "Discretization/QuadratureOrder.h"

#include "ErrorEstimate/ErrorEstimate_Common.h"

#include "Discretization/DG/FieldBundle_DGBR2.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Field weighted Estimates for DGBR2
// Combines Cell, interior trace and boundary trace integrals together
//
// topology specific group weighted residual
//
// template parameters:
//   Topology                          element topology (e.g. Triangle)
//   ParamFieldType                        Grid data type, possibly a tuple with parameters
//   QFieldCellGroupTypeTuple          Inputs required by Field Weighted Integrands and efld
//
//----------------------------------------------------------------------------//
//

template< class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector, class ParamFieldType >
class ErrorEstimate_DGBR2
{
public:
  typedef NDPDEClass_ NDPDEClass;
  typedef typename NDPDEClass::PhysDim PhysDim;
  typedef typename ParamFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef BCParameters<BCVector> BCParams;
  typedef IntegrandCell_DGBR2<NDPDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_DGBR2<NDPDEClass> IntegrandTraceClass;
  typedef IntegrateBoundaryTrace_Dispatch_DGBR2<NDPDEClass, BCNDConvert, BCVector, DGBR2> IntegrateBoundaryTrace_Dispatch;

  // Vector of Reals for Lifting Operator eLfld
  typedef typename DLA::VectorS<PhysDim::D, Real> VecReal;

  typedef FieldBundleBase_DGBR2<PhysDim,TopoDim,ArrayQ> FieldBundle;


  // Constructor
  template<class... BCArgs>
  ErrorEstimate_DGBR2( const ParamFieldType& paramfld,
                       const Field<PhysDim, TopoDim, ArrayQ>& qfld, // q
                       const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld, // r
                       const Field<PhysDim, TopoDim, ArrayQ>& lgfld, // lg
                       const Field<PhysDim, TopoDim, ArrayQ>& wfld, // w
                       const FieldLift<PhysDim, TopoDim, VectorArrayQ>& sfld, // s
                       const Field<PhysDim, TopoDim, ArrayQ>& mufld, // mu
                       const NDPDEClass& pde,
                       const DiscretizationDGBR2& disc,
                       const QuadratureOrder& quadratureOrder,
                       const std::vector<int>& CellGroups,
                       const std::vector<int>& InteriorTraceGroups,
                       PyDict& BCList,
                       const std::map< std::string, std::vector<int> >& BCBoundaryGroups, BCArgs&... args);

  //Constructor for passing in a lifted quantity field (i.e. for shock-capturing)
  template<class... BCArgs>
  ErrorEstimate_DGBR2( const ParamFieldType& paramfld,
                       const Field<PhysDim, TopoDim, ArrayQ>& qfld, // q
                       const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld, // r
                       const Field<PhysDim, TopoDim, ArrayQ>& lgfld, // lg
                       const Field<PhysDim, TopoDim, ArrayQ>& wfld, // w
                       const FieldLift<PhysDim, TopoDim, VectorArrayQ>& sfld, // s
                       const Field<PhysDim, TopoDim, ArrayQ>& mufld, // mu
                       std::shared_ptr<Field_DG_Cell<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                       const NDPDEClass& pde,
                       const DiscretizationDGBR2& disc,
                       const QuadratureOrder& quadratureOrder,
                       const std::vector<int>& CellGroups,
                       const std::vector<int>& InteriorTraceGroups,
                       PyDict& BCList,
                       const std::map< std::string, std::vector<int> >& BCBoundaryGroups, BCArgs&... args);

  // Alternate Agnostic Constructor
  template<class... BCArgs>
  ErrorEstimate_DGBR2( const ParamFieldType& paramfld,
                       const FieldBundle& primal,   // (q, r, lg)
                       const FieldBundle& adjoint,  // (w, s, mu)
                       std::shared_ptr<Field_DG_Cell<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                       const NDPDEClass& pde,
                       const DiscretizationDGBR2& disc,
                       const QuadratureOrder& quadratureOrder,
                       const std::vector<int>& CellGroups,
                       const std::vector<int>& InteriorTraceGroups,
                       PyDict& BCList,
                       const std::map< std::string, std::vector<int> >& BCBoundaryGroups, BCArgs&... args);

  std::size_t nCellGroups() const { return fcnCell_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcnCell_.cellGroup(n); }

  //----------------------------------------------------------------------------//
  // Check that the primal and adjoint fields are all reasonable
  void check( )
  {
    SANS_ASSERT( &xfld_.getXField() == &qfld_.getXField() );  // check that xfld matches that from constructor
    SANS_ASSERT( &xfld_.getXField() == &rfld_.getXField() );
    SANS_ASSERT( &xfld_.getXField() == &lgfld_.getXField() );
    SANS_ASSERT( &xfld_.getXField() == &wfld_.getXField() );
    SANS_ASSERT( &xfld_.getXField() == &sfld_.getXField() );
    SANS_ASSERT( &xfld_.getXField() == &mufld_.getXField() );

    if ( pde_.hasSourceLiftedQuantity() )
      SANS_ASSERT_MSG(pLiftedQuantityfld_, "ErrorEstimate_DGBR2 - The lifted quantity field has not been initialized!");
  }

  void getErrorEstimate( Real& estimate, const std::vector<int>& cellGroups );
  void getErrorEstimate( Real& estimate ) { getErrorEstimate( estimate, cellGroups_ ); }
  void getLocalSolveErrorEstimate( std::vector<Real>& estimate );

  void getErrorIndicator( Real& estimate, const std::vector<int>& cellGroups );
  void getErrorIndicator( Real& estimate ) { getErrorIndicator( estimate, cellGroups_ ); }
  void getLocalSolveErrorIndicator( std::vector<Real>& estimate );

  const XField<PhysDim, TopoDim>& getXField() const { return xfld_; }

  const Field<PhysDim,TopoDim,Real>& getESField()
  {
    if (eSfldCalc_ == false)
      calculateSumEstimates();

    return *up_eSfld_;
  }
  const Field<PhysDim,TopoDim,Real>& getIField()
  {
    if (ifldCalc_ == false)
      calculateIndicators();

    return *up_ifld_;
  }
  const Field<PhysDim,TopoDim,Real>& getEField() const { return efld_; }
  const FieldLift_DG_Cell<PhysDim, TopoDim, Real>& getELField()   const { return eLfld_; }
  const Field<PhysDim, TopoDim, Real>& getEBField() const { return *up_eBfld_; }

  const FieldLift<PhysDim, TopoDim, VectorArrayQ>& getRField() const { return rfld_; }
  const FieldLift<PhysDim, TopoDim, VectorArrayQ>& getSField() const { return sfld_; }

  void fillEArray( std::vector<std::vector<Real>>& estimates );

  // fragile interface for edge solve - accumulates only group 0
  void fillVector(std::vector<std::vector<Real>>& accVector,
                  const Field_DG_Cell<PhysDim,TopoDim,Real>& accFld,
                  const std::vector<int>& cellGroups );

  void outputFields( const std::string& filename_base );

protected:
  void calculateSumEstimates();
  void calculateIndicators();

  const ParamFieldType& paramfld_;
  const XField<PhysDim, TopoDim>& xfld_;
  std::vector<int> active_BGroup_list_; //list of BoundaryTraceGroups which have Lagrange multiplier DOFs (mitLG)

  Field_DG_Cell<PhysDim,TopoDim,Real> efld_; // estimate field
  std::unique_ptr<Field_DG_Cell<PhysDim,TopoDim,Real>> up_ifld_; // indicator field
  std::unique_ptr<Field_DG_Cell<PhysDim,TopoDim,Real>> up_eSfld_; // estimate sum field
  FieldLift_DG_Cell<PhysDim, TopoDim, Real> eLfld_;
  std::unique_ptr<Field<PhysDim, TopoDim, Real>> up_eBfld_;

  const Field<PhysDim, TopoDim, ArrayQ>& qfld_; // q
  const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld_; // r
  const Field<PhysDim, TopoDim, ArrayQ>& lgfld_; // lg
  const Field<PhysDim, TopoDim, ArrayQ>& wfld_; // w
  const FieldLift<PhysDim, TopoDim, VectorArrayQ>& sfld_; // s
  const Field<PhysDim, TopoDim, ArrayQ>& mufld_; // mu

  std::shared_ptr<Field_DG_Cell<PhysDim, TopoDim, Real>> pLiftedQuantityfld_; //pointer to the lifted-quantity field (shock-capturing)

  const NDPDEClass& pde_;
  const QuadratureOrder& quadratureOrder_;

  IntegrandCellClass fcnCell_;
  IntegrandTraceClass fcnIntTrace_;

  std::map< std::string, std::shared_ptr<BCBase> > BCs_;
  IntegrateBoundaryTrace_Dispatch dispatchBC_;

  std::vector<int> cellGroups_;

  bool ifldCalc_;
  bool eSfldCalc_;

  //----------------------------------------------------------------------------//
  // Estimate function that populates the efld_, eLFld_ and eBfld_, called in constructor
  void estimate( );

};

} //namespace SANS

#endif  // ERRORESTIMATE_DGBR2_H
