// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ERRORESTIMATEINTERIORTRACE_DGBR2_H
#define ERRORESTIMATEINTERIORTRACE_DGBR2_H

#include <memory>     // std::unique_ptr

#include "tools/Tuple.h"

#include "Field/Field.h"
#include "Field/FieldLift.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/ElementLift.h"
#include "Field/Element/ElementIntegral.h"

#include "Discretization/DG/DiscretizationDGBR2.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  DG interior-trace integral
//

template<class IntegrandInteriorTrace>
class ErrorEstimateInteriorTrace_DGBR2_impl :
    public GroupIntegralInteriorTraceType< ErrorEstimateInteriorTrace_DGBR2_impl<IntegrandInteriorTrace> >
{
public:
  typedef typename IntegrandInteriorTrace::PhysDim PhysDim;
  typedef typename IntegrandInteriorTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandInteriorTrace::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename IntegrandInteriorTrace::PDE PDE;

  // Save off the boundary trace integrand
  explicit ErrorEstimateInteriorTrace_DGBR2_impl( const IntegrandInteriorTrace& fcn) :
     fcn_(fcn), comm_rank_(0) {}

  std::size_t nInteriorTraceGroups() const { return fcn_.nInteriorTraceGroups(); }
  std::size_t interiorTraceGroup(const int n) const { return fcn_.interiorTraceGroup(n); }

  std::size_t nPeriodicTraceGroups() const { return fcn_.nPeriodicTraceGroups(); }
  std::size_t periodicTraceGroup(const int n) const { return fcn_.periodicTraceGroup(n); }

//----------------------------------------------------------------------------//
  // A dummy function so IntegrateGroups doesn't get upset
  // Could do with some sort of useful test in here maybe
  // slightly cheating just templating it
  template <class TopoDim >
  void check( const typename MakeTuple<FieldTuple,
              Field<PhysDim, TopoDim, ArrayQ>,
              FieldLift<PhysDim, TopoDim, VectorArrayQ>,
              Field<PhysDim, TopoDim, ArrayQ>,
              FieldLift<PhysDim, TopoDim, VectorArrayQ>,
              Field<PhysDim, TopoDim, Real>,
              FieldLift<PhysDim, TopoDim, Real>>::type& flds ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>&           qfld  = get<0>(flds);
    const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld  = get<1>(flds);
    const Field<PhysDim, TopoDim, ArrayQ>&           wfld  = get<2>(flds);
    const FieldLift<PhysDim, TopoDim, VectorArrayQ>& sfld  = get<3>(flds);
    const Field<PhysDim, TopoDim, Real>&             efld  = get<4>(flds);
    const FieldLift<PhysDim, TopoDim, Real>&         eLfld = get<5>(flds);

    SANS_ASSERT( &qfld.getXField() == &wfld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &rfld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &wfld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &sfld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &efld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &eLfld.getXField() ); // check both were made off the same grid

    comm_rank_ = qfld.comm()->rank();
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the interior trace group
  template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, ArrayQ>,
                                                 FieldLift<PhysDim, TopoDim, VectorArrayQ>,
                                                 Field<PhysDim, TopoDim, ArrayQ>,
                                                 FieldLift<PhysDim, TopoDim, VectorArrayQ>,
                                                 Field<PhysDim, TopoDim, Real>,
                                                 FieldLift<PhysDim, TopoDim, Real>>::type
                                     ::template FieldCellGroupType<TopologyL>& fldsCellL,
      const int cellGroupGlobalR,
      const typename XFieldType::template FieldCellGroupType<TopologyR>& xfldCellR,
      const typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, ArrayQ>,
                                                 FieldLift<PhysDim, TopoDim, VectorArrayQ>,
                                                 Field<PhysDim, TopoDim, ArrayQ>,
                                                 FieldLift<PhysDim, TopoDim, VectorArrayQ>,
                                                 Field<PhysDim, TopoDim, Real>,
                                                 FieldLift<PhysDim, TopoDim, Real>>::type
                                     ::template FieldCellGroupType<TopologyR>& fldsCellR,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      int quadratureorder )
  {
    // Left types
    typedef typename XFieldType                               ::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>          ::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename FieldLift<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> RFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, Real        >    ::template FieldCellGroupType<TopologyL> EFieldCellGroupTypeL;
    typedef typename FieldLift<PhysDim, TopoDim, Real >       ::template FieldCellGroupType<TopologyL> ELFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL ::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL ::template ElementType<> ElementQFieldClassL;
    typedef typename RFieldCellGroupTypeL ::template ElementType<> ElementRFieldClassL;
    typedef typename EFieldCellGroupTypeL ::template ElementType<> ElementEFieldClassL;
    typedef typename ELFieldCellGroupTypeL::template ElementType<> ElementELFieldClassL;

    // Right types
    typedef typename XFieldType                               ::template FieldCellGroupType<TopologyR> XFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>          ::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;
    typedef typename FieldLift<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyR> RFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, Real        >    ::template FieldCellGroupType<TopologyR> EFieldCellGroupTypeR;
    typedef typename FieldLift<PhysDim, TopoDim, Real >       ::template FieldCellGroupType<TopologyR> ELFieldCellGroupTypeR;

    typedef typename XFieldCellGroupTypeR ::template ElementType<> ElementXFieldClassR;
    typedef typename QFieldCellGroupTypeR ::template ElementType<> ElementQFieldClassR;
    typedef typename RFieldCellGroupTypeR ::template ElementType<> ElementRFieldClassR;
    typedef typename EFieldCellGroupTypeR ::template ElementType<> ElementEFieldClassR;
    typedef typename ELFieldCellGroupTypeR::template ElementType<> ElementELFieldClassR;

    //Trace types
    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;

    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const RFieldCellGroupTypeL& rfldCellL = get<1>(fldsCellL);
    const QFieldCellGroupTypeL& wfldCellL = get<2>(fldsCellL);
    const RFieldCellGroupTypeL& sfldCellL = get<3>(fldsCellL);
    EFieldCellGroupTypeL& efldCellL = const_cast<EFieldCellGroupTypeL&>(get<4>(fldsCellL));
    ELFieldCellGroupTypeL& eLfldCellL = const_cast<ELFieldCellGroupTypeL&>(get<5>(fldsCellL));

    const QFieldCellGroupTypeR& qfldCellR = get<0>(fldsCellR);
    const RFieldCellGroupTypeR& rfldCellR = get<1>(fldsCellR);
    const QFieldCellGroupTypeR& wfldCellR = get<2>(fldsCellR);
    const RFieldCellGroupTypeR& sfldCellR = get<3>(fldsCellR);
    EFieldCellGroupTypeR& efldCellR = const_cast<EFieldCellGroupTypeR&>(get<4>(fldsCellR));
    ELFieldCellGroupTypeR& eLfldCellR = const_cast<ELFieldCellGroupTypeR&>(get<5>(fldsCellR));

    // LEFT element field variables
    ElementXFieldClassL  xfldElemL(  xfldCellL.basis() );
    ElementQFieldClassL  qfldElemL(  qfldCellL.basis() );
    ElementRFieldClassL  rfldElemL(  rfldCellL.basis() );
    ElementQFieldClassL  wfldElemL(  wfldCellL.basis() );
    ElementRFieldClassL  sfldElemL(  sfldCellL.basis() );
    ElementEFieldClassL  efldElemL(  efldCellL.basis() );
    ElementELFieldClassL eLfldElemL( eLfldCellL.basis() );

    // RIGHT element field variables
    ElementXFieldClassR  xfldElemR(  xfldCellR.basis() );
    ElementQFieldClassR  qfldElemR(  qfldCellR.basis() );
    ElementRFieldClassR  rfldElemR(  rfldCellR.basis() );
    ElementQFieldClassR  wfldElemR(  wfldCellR.basis() );
    ElementRFieldClassR  sfldElemR(  sfldCellR.basis() );
    ElementEFieldClassR  efldElemR(  efldCellR.basis() );
    ElementELFieldClassR eLfldElemR( eLfldCellR.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    //Checking they're a P0 fields
    SANS_ASSERT( efldElemL.order() == 0 );
    SANS_ASSERT( efldElemR.order() == 0 );

    typedef IntegrandDGBR2<Real,Real> IntegrandTypeL;
    typedef IntegrandDGBR2<Real,Real> IntegrandTypeR;

    ElementIntegral<TopoDimTrace, TopologyTrace, IntegrandTypeL, IntegrandTypeR> integral(quadratureorder);

    // just to make sure things are consistent
    SANS_ASSERT( xfldCellL.nElem() == efldCellL.nElem() );
    SANS_ASSERT( xfldCellL.nElem() == eLfldCellL.nElem() );
    SANS_ASSERT( xfldCellR.nElem() == efldCellR.nElem() );
    SANS_ASSERT( xfldCellR.nElem() == eLfldCellR.nElem() );

    IntegrandTypeL rsdElemL;
    IntegrandTypeR rsdElemR;

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      const int elemL = xfldTrace.getElementLeft( elem );
      const int elemR = xfldTrace.getElementRight( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
      const CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );
      rfldCellL.getElement( rfldElemL, elemL, canonicalTraceL.trace );
      wfldCellL.getElement( wfldElemL, elemL );
      sfldCellL.getElement( sfldElemL, elemL, canonicalTraceL.trace );

      xfldCellR.getElement( xfldElemR, elemR );
      qfldCellR.getElement( qfldElemR, elemR );
      rfldCellR.getElement( rfldElemR, elemR, canonicalTraceR.trace );
      wfldCellR.getElement( wfldElemR, elemR );
      sfldCellR.getElement( sfldElemR, elemR, canonicalTraceR.trace );

      xfldTrace.getElement( xfldElemTrace, elem );

      int rankL = qfldElemL.rank();
      int rankR = qfldElemR.rank();

      // nothing to do if both elements are ghosted
      if (rankL != comm_rank_ && rankR != comm_rank_) continue;

      rsdElemL = 0;
      rsdElemR = 0;
      // TODO: Move this integrand construction outside of the loop for speed
      integral( fcn_.integrand(xfldElemTrace, canonicalTraceL, canonicalTraceR,
                                     xfldElemL,
                                     qfldElemL, rfldElemL,
                                     wfldElemL, sfldElemL,
                                     xfldElemR,
                                     qfldElemR, rfldElemR,
                                     wfldElemR, sfldElemR),
                   xfldElemTrace, rsdElemL, rsdElemR);

      // PDE
      if (rankL == comm_rank_)
      {
        efldCellL.getElement( efldElemL, elemL );
        efldElemL.DOF(0) += rsdElemL.PDE;
        efldCellL.setElement( efldElemL, elemL );
      }

      if (rankR == comm_rank_)
      {
        efldCellR.getElement( efldElemR, elemR );
        efldElemR.DOF(0) += rsdElemR.PDE;
        efldCellR.setElement( efldElemR, elemR );
      }

      // LO
      if (rankL == comm_rank_)
      {
        eLfldCellL.getElement( eLfldElemL, elemL, canonicalTraceL.trace );
        eLfldElemL.DOF(0) += rsdElemL.Lift; // fill in the correct canonical trace
        eLfldCellL.setElement( eLfldElemL, elemL, canonicalTraceL.trace );
      }

      if (rankR == comm_rank_)
      {
        eLfldCellR.getElement( eLfldElemR, elemR, canonicalTraceR.trace );
        eLfldElemR.DOF(0) += rsdElemR.Lift; // fill in the correct canonical trace
        eLfldCellR.setElement( eLfldElemR, elemR, canonicalTraceR.trace );
      }
    }
  }

protected:
  const IntegrandInteriorTrace& fcn_;
  mutable int comm_rank_;
};


// Factory function

template<class IntegrandInteriorTrace>
ErrorEstimateInteriorTrace_DGBR2_impl<IntegrandInteriorTrace>
ErrorEstimateInteriorTrace_DGBR2( const IntegrandInteriorTraceType<IntegrandInteriorTrace>& fcn)
{
  return ErrorEstimateInteriorTrace_DGBR2_impl<IntegrandInteriorTrace>(fcn.cast());
}


}

#endif  // ERRORESTIMATEINTERIORTRACE_DGBR2_H
