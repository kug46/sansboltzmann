// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ERRORESTIMATECELL_DGBR2_H
#define ERRORESTIMATECELL_DGBR2_H

// Cell integral residual functions

#include <memory> //unique_ptr

#include "tools/Tuple.h"

#include "Field/Field.h"
#include "Field/FieldLift.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/ElementLift.h"
#include "Field/Element/ElementIntegral.h"

#include "Discretization/DG/DiscretizationDGBR2.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Field Cell group weighted residual for DGBR2
//
// topology specific group weighted residual
//
// template parameters:
//   Topology                          element topology (e.g. Triangle)
//   Integrand                         integrand functor
//   XFieldType                        Grid data type, possibly a tuple with parameters
//   QFieldCellGroupTypeTuple          Inputs required by Field Weighted Integrands and efld
//
//   Efld gets removed from the Tuple and the QW Tuple gets passed on to the integrand
//----------------------------------------------------------------------------//
//
template< class IntegrandCell >
class ErrorEstimateCell_DGBR2_impl :
    public GroupIntegralCellType< ErrorEstimateCell_DGBR2_impl<IntegrandCell> >
{
public:
  typedef typename IntegrandCell::PhysDim PhysDim;
  typedef typename IntegrandCell::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandCell::template VectorArrayQ<Real> VectorArrayQ;

  typedef Real LiftedT; //data-type for lifted quantity

  // Save off the cell integrand
  explicit ErrorEstimateCell_DGBR2_impl( const IntegrandCell& fcn) :
    fcn_(fcn), comm_rank_(0) {}

  std::size_t nCellGroups() const { return fcn_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcn_.cellGroup(n); }

//----------------------------------------------------------------------------//
  // A dummy function so IntegrateCellGroups doesn't get upset
  // Could do with some sort of useful test in here maybe
  template <class TopoDim>
  void check( const typename MakeTuple<FieldTuple,
              Field<PhysDim, TopoDim, ArrayQ>,
              FieldLift<PhysDim, TopoDim, VectorArrayQ>,
              Field<PhysDim, TopoDim, ArrayQ>,
              FieldLift<PhysDim, TopoDim, VectorArrayQ>,
              Field<PhysDim, TopoDim, Real>,
              FieldLift<PhysDim, TopoDim, Real>>::type& flds ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>&           qfld  = get<0>(flds);
    const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld  = get<1>(flds);
    const Field<PhysDim, TopoDim, ArrayQ>&           wfld  = get<2>(flds);
    const FieldLift<PhysDim, TopoDim, VectorArrayQ>& sfld  = get<3>(flds);
    const Field<PhysDim, TopoDim, Real>&             efld  = get<4>(flds);
    const FieldLift<PhysDim, TopoDim, Real>&         eLfld = get<5>(flds);

    SANS_ASSERT( &qfld.getXField() == &wfld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &rfld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &wfld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &sfld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &efld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &eLfld.getXField() ); // check both were made off the same grid

    comm_rank_ = qfld.comm()->rank();
  }

  template <class TopoDim>
  void check( const typename MakeTuple<FieldTuple,
              Field<PhysDim, TopoDim, ArrayQ>,
              FieldLift<PhysDim, TopoDim, VectorArrayQ>,
              Field<PhysDim, TopoDim, ArrayQ>,
              FieldLift<PhysDim, TopoDim, VectorArrayQ>,
              Field<PhysDim, TopoDim, LiftedT>,
              Field<PhysDim, TopoDim, Real>,
              FieldLift<PhysDim, TopoDim, Real>>::type& flds ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>&           qfld  = get<0>(flds);
    const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld  = get<1>(flds);
    const Field<PhysDim, TopoDim, ArrayQ>&           wfld  = get<2>(flds);
    const FieldLift<PhysDim, TopoDim, VectorArrayQ>& sfld  = get<3>(flds);
    const Field<PhysDim, TopoDim, LiftedT>&          lqfld = get<4>(flds);
    const Field<PhysDim, TopoDim, Real>&             efld  = get<5>(flds);
    const FieldLift<PhysDim, TopoDim, Real>&         eLfld = get<6>(flds);

    SANS_ASSERT( &qfld.getXField() == &wfld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &rfld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &wfld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &sfld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &lqfld.getXField() ); // check both were made off the same grid

    SANS_ASSERT( &qfld.getXField() == &efld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &eLfld.getXField() ); // check both were made off the same grid

    comm_rank_ = qfld.comm()->rank();
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template < class Topology, class TopoDim, class XFieldType >
  void
  integrate( const int cellGroupGlobal,
             const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
             const typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, ArrayQ>,
                                                  FieldLift<PhysDim, TopoDim, VectorArrayQ>,
                                                  Field<PhysDim, TopoDim, ArrayQ>,
                                                  FieldLift<PhysDim, TopoDim, VectorArrayQ>,
                                                  Field<PhysDim, TopoDim, Real>,
                                                  FieldLift<PhysDim, TopoDim, Real>>::type
                                                  ::template FieldCellGroupType<Topology>& fldsCell,
             const int quadratureorder )
  {
    // XField
    typedef typename XFieldType                              ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field< PhysDim,TopoDim,ArrayQ        >  ::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename FieldLift< PhysDim,TopoDim,VectorArrayQ>::template FieldCellGroupType<Topology> RFieldCellGroupType;
    typedef typename Field< PhysDim, TopoDim, Real        >  ::template FieldCellGroupType<Topology> EFieldCellGroupType;
    typedef typename FieldLift< PhysDim, TopoDim, Real    >  ::template FieldCellGroupType<Topology> ELFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;
    typedef ElementLift<VectorArrayQ, TopoDim, Topology>         ElementRFieldClass;
    typedef typename EFieldCellGroupType::template ElementType<> ElementEFieldClass;
    typedef ElementLift<Real, TopoDim, Topology>                 ElementELFieldClass;

    const int NTrace = Topology::NTrace;    // total traces in element

    const QFieldCellGroupType& qfldCell = get<0>(fldsCell);
    const RFieldCellGroupType& rfldCell = get<1>(fldsCell);
    const QFieldCellGroupType& wfldCell = get<2>(fldsCell);
    const RFieldCellGroupType& sfldCell = get<3>(fldsCell);
    EFieldCellGroupType& efldCell = const_cast<EFieldCellGroupType&>(get<4>(fldsCell));
    ELFieldCellGroupType& eLfldCell = const_cast<ELFieldCellGroupType&>(get<5>(fldsCell));

    // element field variables
    ElementXFieldClass  xfldElem(   xfldCell.basis() );
    ElementQFieldClass  qfldElem(   qfldCell.basis() );
    ElementRFieldClass  rfldElems(  rfldCell.basis() );
    ElementQFieldClass  wfldElem(   wfldCell.basis() );
    ElementRFieldClass  sfldElems(  sfldCell.basis() );
    ElementEFieldClass  efldElem(   efldCell.basis() );
    ElementELFieldClass eLfldElems( eLfldCell.basis() );

    //Checking it's a p0 field
    SANS_ASSERT( 0 == efldElem.basis()->order() );
    SANS_ASSERT( 0 == eLfldElems.basis()->order());

    typedef IntegrandDGBR2<Real,Real,Topology> IntegrandType;

    // element integral
    ElementIntegral<TopoDim, Topology, IntegrandType > integral(quadratureorder);

    // just to make sure things are consistent
    SANS_ASSERT( xfldCell.nElem() == efldCell.nElem() );
    SANS_ASSERT( xfldCell.nElem() == eLfldCell.nElem() );

    IntegrandType rsdElem;

    auto integrand = fcn_.integrand(xfldElem, qfldElem, rfldElems, wfldElem, sfldElems);

    // loop over elements within group
    const int nelem = xfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid/solution DOFs to element
      qfldCell.getElement( qfldElem, elem );

      // only consider elements possessed by this processor
      if (qfldElem.rank() != comm_rank_) continue;

      xfldCell.getElement( xfldElem, elem );
      rfldCell.getElement( rfldElems, elem );
      wfldCell.getElement( wfldElem, elem );
      sfldCell.getElement( sfldElems, elem );

      rsdElem = 0;
      // cell integration for canonical element
      integral( integrand,
                get<-1>(xfldElem), rsdElem );

      // PDE
      efldCell.getElement( efldElem, elem );
      efldElem.DOF(0) += rsdElem.PDE; // so as not to overwrite
      efldCell.setElement( efldElem, elem );

      // LO
      eLfldCell.getElement( eLfldElems, elem );
      for (int trace = 0; trace < NTrace; trace++)
        eLfldElems[trace].DOF(0) += rsdElem.Lift[trace];

      eLfldCell.setElement( eLfldElems, elem );
    }
  }

  //For case with lifted quantity field for source terms
  template < class Topology, class TopoDim, class XFieldType >
  void
  integrate( const int cellGroupGlobal,
             const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
             const typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, ArrayQ>,
                                                  FieldLift<PhysDim, TopoDim, VectorArrayQ>,
                                                  Field<PhysDim, TopoDim, ArrayQ>,
                                                  FieldLift<PhysDim, TopoDim, VectorArrayQ>,
                                                  Field<PhysDim, TopoDim, LiftedT>,
                                                  Field<PhysDim, TopoDim, Real>,
                                                  FieldLift<PhysDim, TopoDim, Real>>::type
                                                  ::template FieldCellGroupType<Topology>& fldsCell,
             const int quadratureorder )
  {
    // XField
    typedef typename XFieldType                              ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field< PhysDim,TopoDim,ArrayQ        >  ::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename FieldLift< PhysDim,TopoDim,VectorArrayQ>::template FieldCellGroupType<Topology> RFieldCellGroupType;
    typedef typename Field< PhysDim,TopoDim,LiftedT       >  ::template FieldCellGroupType<Topology> SFieldCellGroupType;

    typedef typename Field< PhysDim, TopoDim, Real        >  ::template FieldCellGroupType<Topology> EFieldCellGroupType;
    typedef typename FieldLift< PhysDim, TopoDim, Real    >  ::template FieldCellGroupType<Topology> ELFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;
    typedef ElementLift<VectorArrayQ, TopoDim, Topology>         ElementRFieldClass;
    typedef typename SFieldCellGroupType::template ElementType<> ElementSFieldClass;

    typedef typename EFieldCellGroupType::template ElementType<> ElementEFieldClass;
    typedef ElementLift<Real, TopoDim, Topology>                 ElementELFieldClass;

    const int NTrace = Topology::NTrace;    // total traces in element

    const QFieldCellGroupType& qfldCell = get<0>(fldsCell);
    const RFieldCellGroupType& rfldCell = get<1>(fldsCell);
    const QFieldCellGroupType& wfldCell = get<2>(fldsCell);
    const RFieldCellGroupType& sfldCell = get<3>(fldsCell);
    const SFieldCellGroupType& liftedQuantityfldCell = get<4>(fldsCell);

    EFieldCellGroupType& efldCell = const_cast<EFieldCellGroupType&>(get<5>(fldsCell));
    ELFieldCellGroupType& eLfldCell = const_cast<ELFieldCellGroupType&>(get<6>(fldsCell));

    // element field variables
    ElementXFieldClass  xfldElem(   xfldCell.basis() );
    ElementQFieldClass  qfldElem(   qfldCell.basis() );
    ElementRFieldClass  rfldElems(  rfldCell.basis() );
    ElementQFieldClass  wfldElem(   wfldCell.basis() );
    ElementRFieldClass  sfldElems(  sfldCell.basis() );
    std::shared_ptr<ElementSFieldClass> psfldElem = std::make_shared<ElementSFieldClass>( liftedQuantityfldCell.basis() );

    ElementEFieldClass  efldElem(   efldCell.basis() );
    ElementELFieldClass eLfldElems( eLfldCell.basis() );

    //Checking it's a p0 field
    SANS_ASSERT( 0 == efldElem.basis()->order() );
    SANS_ASSERT( 0 == eLfldElems.basis()->order());

    typedef IntegrandDGBR2<Real,Real,Topology> IntegrandType;

    // element integral
    ElementIntegral<TopoDim, Topology, IntegrandType > integral(quadratureorder);

    // just to make sure things are consistent
    SANS_ASSERT( xfldCell.nElem() == efldCell.nElem() );
    SANS_ASSERT( xfldCell.nElem() == eLfldCell.nElem() );

    IntegrandType rsdElem;

    // loop over elements within group
    const int nelem = xfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid/solution DOFs to element
      qfldCell.getElement( qfldElem, elem );

      // only consider elements possessed by this processor
      if (qfldElem.rank() != comm_rank_) continue;

      xfldCell.getElement( xfldElem, elem );
      rfldCell.getElement( rfldElems, elem );
      wfldCell.getElement( wfldElem, elem );
      sfldCell.getElement( sfldElems, elem );
      liftedQuantityfldCell.getElement( *psfldElem, elem );

      rsdElem = 0;
      // cell integration for canonical element
      integral( fcn_.integrand(xfldElem, qfldElem, rfldElems, wfldElem, sfldElems, psfldElem),
                get<-1>(xfldElem), rsdElem );

      // PDE
      efldCell.getElement( efldElem, elem );
      efldElem.DOF(0) += rsdElem.PDE; // so as not to overwrite
      efldCell.setElement( efldElem, elem );

      // LO
      eLfldCell.getElement( eLfldElems, elem );
      for (int trace = 0; trace < NTrace; trace++)
        eLfldElems[trace].DOF(0) += rsdElem.Lift[trace];

      eLfldCell.setElement( eLfldElems, elem );
    }
  }

protected:
  const IntegrandCell& fcn_;
  mutable int comm_rank_;
};

// Factory function

template<class IntegrandCell>
ErrorEstimateCell_DGBR2_impl<IntegrandCell>
ErrorEstimateCell_DGBR2( const IntegrandCellType<IntegrandCell>& fcn)
{
  return ErrorEstimateCell_DGBR2_impl<IntegrandCell>(fcn.cast());
}


} //namespace SANS

#endif  // ERRORESTIMATECELL_DGBR2_H
