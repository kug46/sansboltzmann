// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ERRORESTIMATEBOUNDARYTRACE_DISPATCH_DGBR2_H
#define ERRORESTIMATEBOUNDARYTRACE_DISPATCH_DGBR2_H

// boundary-trace integral residual functions

#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"
#include "ErrorEstimateBoundaryTrace_DGBR2.h"

namespace SANS
{
//---------------------------------------------------------------------------//
//
// Dispatch class for BC's with internal state
//
//---------------------------------------------------------------------------//
template<class XFieldType, class PhysDim, class TopoDim, class ArrayQ >
class ErrorEstimateBoundaryTrace_mitState_Dispatch_DGBR2_impl
{
public:
  ErrorEstimateBoundaryTrace_mitState_Dispatch_DGBR2_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const FieldLift<PhysDim, TopoDim, DLA::VectorS<PhysDim::D,ArrayQ>>& rfld,
      const Field<PhysDim, TopoDim, ArrayQ>& wfld,
      const FieldLift<PhysDim, TopoDim, DLA::VectorS<PhysDim::D,ArrayQ>>& sfld,
      Field<PhysDim, TopoDim, Real>& efld,
      FieldLift<PhysDim, TopoDim, Real>& eLfld,
      const int* quadratureorder, int ngroup )
    : xfld_(xfld),
      qfld_(qfld), rfld_(rfld), wfld_(wfld), sfld_(sfld), efld_(efld), eLfld_(eLfld),
      quadratureorder_(quadratureorder), ngroup_(ngroup)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups<TopoDim>::integrate(
        ErrorEstimateBoundaryTrace_DGBR2(fcn),
        xfld_,
        (qfld_,rfld_,wfld_,sfld_,efld_,eLfld_),
        quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const FieldLift<PhysDim, TopoDim, DLA::VectorS<PhysDim::D,ArrayQ>>& rfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& wfld_;
  const FieldLift<PhysDim, TopoDim, DLA::VectorS<PhysDim::D,ArrayQ>>& sfld_;
  Field<PhysDim, TopoDim, Real>& efld_;
  FieldLift<PhysDim, TopoDim, Real>& eLfld_;
  const int* quadratureorder_;
  const int ngroup_;
};

// Factory function

template<class XFieldType, class PhysDim, class TopoDim, class ArrayQ>
ErrorEstimateBoundaryTrace_mitState_Dispatch_DGBR2_impl<XFieldType, PhysDim, TopoDim, ArrayQ>
ErrorEstimateBoundaryTrace_mitState_Dispatch_DGBR2( const XFieldType& xfld,
                                                    const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                                    const FieldLift<PhysDim, TopoDim, DLA::VectorS<PhysDim::D,ArrayQ>>& rfld,
                                                    const Field<PhysDim, TopoDim, ArrayQ>& wfld,
                                                    const FieldLift<PhysDim, TopoDim, DLA::VectorS<PhysDim::D,ArrayQ>>& sfld,
                                                    Field<PhysDim, TopoDim, Real>& efld,
                                                    FieldLift<PhysDim, TopoDim, Real>& eLfld,
                                                    const int* quadratureorder, int ngroup)
{
  return ErrorEstimateBoundaryTrace_mitState_Dispatch_DGBR2_impl<XFieldType, PhysDim, TopoDim, ArrayQ>(
      xfld,
      qfld, rfld, wfld, sfld, efld, eLfld,
      quadratureorder, ngroup);
}


}
#endif //ERRORESTIMATEBOUNDARYTRACE_DGBR2_H
