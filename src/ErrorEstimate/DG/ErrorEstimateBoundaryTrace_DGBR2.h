// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ERRORESTIMATEBOUNDARYTRACE_DGBR2_H
#define ERRORESTIMATEBOUNDARYTRACE_DGBR2_H

// boundary-trace integral error estimation functions

#include <memory>     // std::unique_ptr

#include "Topology/ElementTopology.h"
#include "Field/Field.h"
#include "Field/FieldLift.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/ElementIntegral.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"
#include "Discretization/DG/DiscretizationDGBR2.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  DGBR2 error estimate boundary-trace integral
//

template<class IntegrandBoundaryTrace>
class ErrorEstimateBoundaryTrace_DGBR2_impl :
    public GroupIntegralBoundaryTraceType< ErrorEstimateBoundaryTrace_DGBR2_impl<IntegrandBoundaryTrace> >
{
public:
  typedef typename IntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename IntegrandBoundaryTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandBoundaryTrace::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename DLA::VectorS<PhysDim::D, Real> VecReal;

  // Save off the boundary trace integrand and the residual vectors
  explicit ErrorEstimateBoundaryTrace_DGBR2_impl( const IntegrandBoundaryTrace& fcn) :
    fcn_(fcn), comm_rank_(0) {}

  std::size_t nBoundaryGroups() const { return fcn_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcn_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
// does some checks on the inputs to make sure they all make sense
  template < class TopoDim >
  void check( const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>, // q
                                                   FieldLift<PhysDim,TopoDim,VectorArrayQ>, // r
                                                   Field<PhysDim,TopoDim,ArrayQ>, // w
                                                   FieldLift<PhysDim,TopoDim,VectorArrayQ>, // s
                                                   Field<PhysDim,TopoDim,Real>, // e
                                                   FieldLift<PhysDim,TopoDim,Real> >::type& flds ) // eL
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);
    const Field<PhysDim, TopoDim, ArrayQ>& wfld = get<2>(flds);
    const Field<PhysDim, TopoDim, Real>& efld = get<4>(flds);

    SANS_ASSERT( qfld.nElem() == wfld.nElem() ); // same number of elems
    SANS_ASSERT( qfld.nElem() == efld.nElem() ); // same number of elems
    SANS_ASSERT( &qfld.getXField() == &wfld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &efld.getXField() ); // check both were made off the same grid

    const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld = get<1>(flds);
    const FieldLift<PhysDim, TopoDim, VectorArrayQ>& sfld = get<3>(flds);
    const FieldLift<PhysDim, TopoDim, Real>&         eLfld = get<5>(flds);

    SANS_ASSERT( rfld.nElem() == sfld.nElem() ); // same number of elems
    SANS_ASSERT( rfld.nElem() == eLfld.nElem() ); // same number of elems
    SANS_ASSERT( &rfld.getXField() == &sfld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &rfld.getXField() == &eLfld.getXField() ); // check both were made off the same grid

    SANS_ASSERT( &qfld.getXField() == &rfld.getXField() ); // checking both were made off the same grid

    comm_rank_ = qfld.comm()->rank();
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class Topology, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobalL,
             const typename XFieldType::template FieldCellGroupType<Topology>& xfldCellL,
             const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>, // q
                                                  FieldLift<PhysDim,TopoDim,VectorArrayQ>, // r
                                                  Field<PhysDim,TopoDim,ArrayQ>, // w
                                                  FieldLift<PhysDim,TopoDim,VectorArrayQ>, // s
                                                  Field<PhysDim,TopoDim,Real>, // e
                                                  FieldLift<PhysDim,TopoDim,Real>>::type // eL
                              ::template FieldCellGroupType<Topology>& fldsCellL,
             const int traceGroupGlobal,
             const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
             int quadratureorder )
  {
    typedef typename XFieldType                             ::template FieldCellGroupType<Topology> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>        ::template FieldCellGroupType<Topology> QFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, Real  >        ::template FieldCellGroupType<Topology> EFieldCellGroupTypeL;
    typedef typename FieldLift<PhysDim,TopoDim,VectorArrayQ>::template FieldCellGroupType<Topology> RFieldCellGroupTypeL;
    typedef typename FieldLift<PhysDim,TopoDim,Real>     ::template FieldCellGroupType<Topology> ELFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL ::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL ::template ElementType<> ElementQFieldClassL;
    typedef typename RFieldCellGroupTypeL ::template ElementType<> ElementRFieldClassL;
    typedef typename EFieldCellGroupTypeL ::template ElementType<> ElementEFieldClassL;
    typedef typename ELFieldCellGroupTypeL::template ElementType<> ElementELFieldClassL;

    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>  XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<>             ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    // separating off Q, W and casting away the const on Efield
    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const QFieldCellGroupTypeL& wfldCellL = get<2>(fldsCellL);
    EFieldCellGroupTypeL& efldCellL = const_cast<EFieldCellGroupTypeL&>(get<4>(fldsCellL));

    const RFieldCellGroupTypeL& rfldCellL = get<1>(fldsCellL);
    const RFieldCellGroupTypeL& sfldCellL = get<3>(fldsCellL);
    ELFieldCellGroupTypeL& eLfldCellL = const_cast<ELFieldCellGroupTypeL&>(get<5>(fldsCellL));

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementQFieldClassL wfldElemL( wfldCellL.basis() );
    ElementEFieldClassL efldElemL( efldCellL.basis() );

    ElementRFieldClassL rfldElemL( rfldCellL.basis() );
    ElementRFieldClassL sfldElemL( sfldCellL.basis() );
    ElementELFieldClassL eLfldElemL( eLfldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    //Checking they're a P0 fields
    SANS_ASSERT( efldElemL.order() == 0 );
    SANS_ASSERT( 0 == eLfldElemL.basis()->order());

    // trace element integral
    typedef IntegrandDGBR2<Real,Real> IntegrandType;
    ElementIntegral<TopoDimTrace, TopologyTrace, IntegrandType > integral(quadratureorder);

    IntegrandType rsdElemL;

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      const int elemL = xfldTrace.getElementLeft( elem );
      CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );
      wfldCellL.getElement( wfldElemL, elemL );
      rfldCellL.getElement( rfldElemL, elemL, canonicalTraceL.trace );
      sfldCellL.getElement( sfldElemL, elemL, canonicalTraceL.trace );

      xfldTrace.getElement( xfldElemTrace, elem );

      int rankL = qfldElemL.rank();

      // nothing to do if elements is ghosted
      if (rankL != comm_rank_) continue;

      rsdElemL = 0;

      integral( fcn_.integrand(xfldElemTrace, canonicalTraceL,
                               xfldElemL,
                               qfldElemL, rfldElemL,
                               wfldElemL, sfldElemL),
                get<-1>(xfldElemTrace), rsdElemL );

      // PDE
      efldCellL.getElement( efldElemL, elemL );
      efldElemL.DOF(0) += rsdElemL.PDE; // so as not to overwrite
      efldCellL.setElement( efldElemL, elemL );

      // LO
      eLfldCellL.getElement( eLfldElemL, elemL, canonicalTraceL.trace );
      eLfldElemL.DOF(0) += rsdElemL.Lift; // fill in the correct canonical trace
      eLfldCellL.setElement( eLfldElemL, elemL, canonicalTraceL.trace );

    }
  }

protected:
  const IntegrandBoundaryTrace& fcn_;
  mutable int comm_rank_;
};

// Factory function

template<class IntegrandBoundaryTrace>
ErrorEstimateBoundaryTrace_DGBR2_impl<IntegrandBoundaryTrace>
ErrorEstimateBoundaryTrace_DGBR2( const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
{
  return ErrorEstimateBoundaryTrace_DGBR2_impl<IntegrandBoundaryTrace>(fcn.cast());
}


}

#endif  // ERRORESTIMATEBOUNDARYTRACE_DGBR2_H
