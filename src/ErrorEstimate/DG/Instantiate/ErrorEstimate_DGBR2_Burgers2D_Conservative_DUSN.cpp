// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ERRORESTIMATE_DGBR2_INSTANTIATE
#include "ErrorEstimate/DG/ErrorEstimate_DGBR2_impl.h"

#include "pde/Burgers/PDEBurgers2D.h"
#include "pde/Burgers/BCBurgers2D.h"
#include "pde/Burgers/BurgersConservative2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime2D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime2D.h"

#include "Field/XFieldArea.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

namespace SANS
{
typedef BurgersConservative2D QType;
typedef ViscousFlux2D_Uniform DiffusionModel;
typedef Source2D_None SourceModel;

typedef PDEBurgers<PhysD2,
                   QType,
                   DiffusionModel,
                   SourceModel > PDEClass;
typedef XField<PhysD2, TopoD2> ParamFieldTupleType;

typedef BCBurgers2DVector<BurgersConservative2D,ViscousFlux2D_Uniform> BCVector;

ERRORESTIMATE_DGBR2_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD2, ParamFieldTupleType )

}
