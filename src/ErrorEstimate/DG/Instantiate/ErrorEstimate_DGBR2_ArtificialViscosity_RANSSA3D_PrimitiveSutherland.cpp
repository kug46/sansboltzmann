// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ERRORESTIMATE_DGBR2_INSTANTIATE
#include "ErrorEstimate/DG/ErrorEstimate_DGBR2_impl.h"

#include "pde/NS/Fluids3D_Sensor.h"
#include "pde/NS/TraitsRANSSAArtificialViscosity.h"
#include "pde/NS/Q3DPrimitiveRhoPressure.h"
#include "pde/NS/QRANSSA3D.h"
#include "pde/NS/PDEEulermitAVDiffusion3D.h"
#include "pde/NS/BCEulermitAVSensor3D.h"

#include "pde/ArtificialViscosity/AVVariable.h"
#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux3D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux3D.h"
#include "pde/ArtificialViscosity/AVSensor_Source3D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor3D.h"

#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"

#include "Field/XFieldVolume.h"
#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/FieldLiftVolume_DG_Cell.h"


namespace SANS
{
typedef QTypePrimitiveRhoPressure QType;
typedef ViscosityModel_Sutherland ViscosityModelType;
typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
typedef PDEEulermitAVDiffusion3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClass, PDERANSSA3D> PDEBaseClass;

typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorAdvectiveFlux;
typedef AVSensor_ViscousFlux3D_GenHScale<TraitsSizeRANSSAArtificialViscosity> SensorViscousFlux;
typedef Fluids_Sensor<PhysD3, PDEBaseClass> Sensor;
//typedef AVSensor_Source3D_PressureGrad<TraitsSizeRANSSAArtificialViscosity, Sensor> SensorSource;
typedef AVSensor_Source3D_Jump<TraitsSizeRANSSAArtificialViscosity, Sensor> SensorSource;

typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
typedef PDEmitAVSensor3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelAV> PDEClass;

typedef BCRANSSAmitAVDiffusion3DVector<TraitsSizeRANSSAArtificialViscosity, TraitsModelAV> BCVector;

typedef typename DLA::MatrixSymS<PhysD3::D,Real> HType;

typedef typename MakeTuple<FieldTuple, Field<PhysD3, TopoD3, HType>,
                                       Field<PhysD3, TopoD3, Real>,
                                       XField<PhysD3, TopoD3>>::type ParamFieldTupleType;

ERRORESTIMATE_DGBR2_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD3, ParamFieldTupleType )
}
