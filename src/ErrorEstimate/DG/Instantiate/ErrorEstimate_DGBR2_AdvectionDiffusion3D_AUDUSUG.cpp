// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ERRORESTIMATE_DGBR2_INSTANTIATE
#include "ErrorEstimate/DG/ErrorEstimate_DGBR2_impl.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion3D.h"

#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime3D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime3D.h"

#include "Field/XFieldVolume.h"
#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/FieldLiftVolume_DG_Cell.h"

#include "Field/XFieldSpacetime.h"
#include "Field/FieldSpacetime_DG_Cell.h"
#include "Field/FieldSpacetime_DG_BoundaryTrace.h"
#include "Field/FieldLiftSpaceTime_DG_Cell.h"

namespace SANS
{
typedef PDEAdvectionDiffusion<PhysD3,
                              AdvectiveFlux3D_Uniform,
                              ViscousFlux3D_Uniform,
                              Source3D_UniformGrad > PDEClass;
typedef XField<PhysD3, TopoD3> ParamFieldTupleType;

// Add the solution BC to the BCVector
typedef BCAdvectionDiffusion3DVector<AdvectiveFlux3D_Uniform, ViscousFlux3D_Uniform> BCVector;


typedef XField<PhysD3, TopoD3> ParamFieldTupleType;

ERRORESTIMATE_DGBR2_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD3, ParamFieldTupleType )

typedef XField<PhysD4, TopoD4> ParamFieldTupleSpaceTime;

typedef PDENDConvertSpaceTime<PhysD4, PDEClass> PDENDSpaceTime;

ERRORESTIMATE_DGBR2_INSTANTIATE_SPACETIME(PDEClass, BCVector, TopoD4, ParamFieldTupleSpaceTime)


// philip
typedef PDEAdvectionDiffusion<PhysD3,
                              AdvectiveFlux3D_Radial,
                              ViscousFlux3D_Uniform,
                              Source3D_UniformGrad > PDEClassR;
typedef XField<PhysD3, TopoD3> ParamFieldTupleType;

// Add the solution BC to the BCVector
typedef BCAdvectionDiffusion3DVector<AdvectiveFlux3D_Radial, ViscousFlux3D_Uniform> BCVectorR;


typedef XField<PhysD3, TopoD3> ParamFieldTupleType;

ERRORESTIMATE_DGBR2_INSTANTIATE_SPACE( PDEClassR, BCVectorR, TopoD3, ParamFieldTupleType )

typedef XField<PhysD4, TopoD4> ParamFieldTupleSpaceTime;

typedef PDENDConvertSpaceTime<PhysD4, PDEClassR> PDENDSpaceTimeR;

ERRORESTIMATE_DGBR2_INSTANTIATE_SPACETIME(PDEClassR, BCVectorR, TopoD4, ParamFieldTupleSpaceTime)


}
