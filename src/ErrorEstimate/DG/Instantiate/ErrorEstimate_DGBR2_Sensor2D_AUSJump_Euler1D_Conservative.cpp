// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ERRORESTIMATE_DGBR2_INSTANTIATE
#include "ErrorEstimate/DG/ErrorEstimate_DGBR2_impl.h"

#include "pde/NS/Fluids1D_Sensor.h"
#include "pde/NS/TraitsEuler.h"
#include "pde/NS/Q1DConservative.h"
#include "pde/NS/PDEEuler1D.h"

#include "pde/Sensor/Source2D_JumpSensor.h"
#include "pde/Sensor/PDESensorParameter2D.h"
#include "pde/Sensor/BCSensorParameter2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime2D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime2D.h"

#include "Field/XFieldArea.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "Field/XFieldVolume.h"
#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/FieldLiftVolume_DG_Cell.h"

namespace SANS
{

typedef QTypeConservative QType;
typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;

typedef Sensor_AdvectiveFlux2D_Uniform Sensor_Advection;
typedef Sensor_ViscousFlux2D_GenHScale Sensor_Diffusion;
typedef Fluids_Sensor<PhysD1, PDEClass> Sensor;
typedef Source2D_JumpSensor<Sensor> Source_JumpSensor;

typedef PDESensorParameter<PhysD2,
                           TraitsSizeEuler<PhysD1>,
                           Sensor_Advection,
                           Sensor_Diffusion,
                           Source_JumpSensor > PDEClass_Sensor;

typedef typename DLA::MatrixSymS<PhysD2::D,Real> HType;
typedef typename DLA::MatrixSymS<PhysD3::D,Real> HType_ST;

typedef BCSensorParameter2DVector<Sensor_Advection, Sensor_Diffusion> BCVector;


typedef DLA::VectorS<3,Real> ArrayQ0;

typedef typename MakeTuple<FieldTuple, Field<PhysD2, TopoD2, HType>,
                                       Field<PhysD2, TopoD2, ArrayQ0>,
                                       XField<PhysD2, TopoD2>>::type VecS3ParamFieldTupleType;

typedef typename MakeTuple<FieldTuple, Field<PhysD3, TopoD3, HType_ST>,
                                       Field<PhysD3, TopoD3, ArrayQ0>,
                                       XField<PhysD3, TopoD3>>::type VecS3ParamFieldTupleType_ST;

ERRORESTIMATE_DGBR2_INSTANTIATE_SPACE( PDEClass_Sensor, BCVector, TopoD2, VecS3ParamFieldTupleType )

#if 0
ERRORESTIMATE_DGBR2_INSTANTIATE_SPACE( PDEClass_Sensor, BCVector, TopoD2, VecS3ParamFieldTupleType_ST )
#endif

}
