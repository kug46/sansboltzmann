// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ERRORESTIMATE_DGBR2_INSTANTIATE
#include "ErrorEstimate/DG/ErrorEstimate_DGBR2_impl.h"

#include "pde/Burgers/BurgersConservative1D.h"
#include "pde/Burgers/PDEBurgers_ArtificialViscosity1D.h"
#include "pde/Burgers/BCmitAVSensorBurgers1D.h"

#include "pde/Sensor/SensorParameter_Traits.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"

#include "Field/XFieldArea.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

namespace SANS
{

typedef BurgersConservative1D QType;
typedef ViscousFlux1D_Uniform DiffusionModel;
typedef Source1D_UniformGrad SourceModel;

typedef PDEBurgers_ArtificialViscosity<PhysD1, QType, DiffusionModel, SourceModel > PDEClass_Primal;

typedef BCmitAVSensorBurgers1DVector<QType, DiffusionModel, SourceModel> BCVector_Primal;

typedef typename DLA::MatrixSymS<PhysD2::D,Real> HType;

typedef typename MakeTuple<FieldTuple, Field<PhysD2, TopoD2, HType>,
                                       Field<PhysD2, TopoD2, SensorParameterTraits<PhysD2>::ArrayQ<Real>>,
                                       XField<PhysD2, TopoD2>>::type ParamFieldTupleType_Primal;

ERRORESTIMATE_DGBR2_INSTANTIATE_SPACETIME( PDEClass_Primal, BCVector_Primal, TopoD2, ParamFieldTupleType_Primal )

}
