// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ERRORESTIMATE_DGBR2_INSTANTIATE
#include "ErrorEstimate/DG/ErrorEstimate_DGBR2_impl.h"

#include "pde/AdvectionDiffusion/AdvectionDiffusion_Sensor.h"

#include "pde/Sensor/Source1D_JumpSensor.h"
#include "pde/Sensor/PDESensorParameter1D.h"
#include "pde/Sensor/BCSensorParameter1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"

#include "Field/XFieldArea.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

namespace SANS
{
typedef AdvectionDiffusion_Sensor Sensor;
typedef Source1D_JumpSensor<Sensor> Source_JumpSensor;
typedef Sensor_AdvectiveFlux1D_Uniform Sensor_Advection;
typedef Sensor_ViscousFlux1D_GenHScale Sensor_Diffusion;

typedef PDESensorParameter<PhysD1,
                           SensorParameterTraits<PhysD1>,
                           Sensor_Advection,
                           Sensor_Diffusion,
                           Source_JumpSensor > PDEClass_Sensor;

typedef typename DLA::MatrixSymS<PhysD2::D,Real> HType;

typedef typename MakeTuple<FieldTuple, Field<PhysD2, TopoD2, HType>,
                                       Field<PhysD2, TopoD2, Real>,
                                       XField<PhysD2, TopoD2>>::type ParamFieldTupleType_Sensor;

typedef BCSensorParameter1DVector<Sensor_Advection, Sensor_Diffusion> BCVector_Sensor;

ERRORESTIMATE_DGBR2_INSTANTIATE_SPACETIME( PDEClass_Sensor, BCVector_Sensor, TopoD2, ParamFieldTupleType_Sensor )

}
