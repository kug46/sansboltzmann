// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ERRORESTIMATE_DGBR2_INSTANTIATE
#include "ErrorEstimate/DG/ErrorEstimate_DGBR2_impl.h"

#include "pde/PorousMedia/Q2DPrimitive_pnSw.h"
#include "pde/PorousMedia/DensityModel.h"
#include "pde/PorousMedia/PorosityModel.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/ViscosityModel_Constant.h"
#include "pde/PorousMedia/PermeabilityModel2D.h"
#include "pde/PorousMedia/CapillaryModel.h"

#include "pde/PorousMedia/PDETwoPhase2D.h"
#include "pde/PorousMedia/BCTwoPhase2D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime2D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"

#include "Field/XFieldArea.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "Field/XFieldVolume.h"
#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/FieldLiftVolume_DG_Cell.h"


namespace SANS
{
typedef DensityModel_Comp DensityModel;
typedef PorosityModel_Comp PorosityModel;
typedef RelPermModel_PowerLaw RelPermModel;
typedef ViscosityModel_Constant ViscModel;
typedef PermeabilityModel2D_QuadBlock RockPermModel;
typedef CapillaryModel_Linear CapillaryModel;

typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                            RelPermModel, RelPermModel, ViscModel, ViscModel, RockPermModel, CapillaryModel> TraitsModelClass;

typedef PDETwoPhase2D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

typedef BCTwoPhase2DVector<PDEClass>::type BCVector;

typedef XField<PhysD2, TopoD2> ParamFieldTupleType;

ERRORESTIMATE_DGBR2_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD2, ParamFieldTupleType )

typedef XField<PhysD3, TopoD3> ParamFieldTupleSpaceTime;

ERRORESTIMATE_DGBR2_INSTANTIATE_SPACETIME( PDEClass, BCVector, TopoD3, ParamFieldTupleSpaceTime )

}
