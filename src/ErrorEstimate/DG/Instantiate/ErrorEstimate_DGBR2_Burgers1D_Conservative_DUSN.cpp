// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ERRORESTIMATE_DGBR2_INSTANTIATE
#include "ErrorEstimate/DG/ErrorEstimate_DGBR2_impl.h"

#include "pde/Burgers/PDEBurgers1D.h"
#include "pde/Burgers/BCBurgers1D.h"
#include "pde/Burgers/BurgersConservative1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"

#include "Field/XFieldLine.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLiftLine_DG_Cell.h"

#include "Field/XFieldArea.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

namespace SANS
{
typedef PDEBurgers< PhysD1,
                    BurgersConservative1D,
                    ViscousFlux1D_Uniform,
                    Source1D_None > PDEClass;

typedef BCBurgers1DVector<BurgersConservative1D,ViscousFlux1D_Uniform,Source1D_None> BCVector;


typedef XField<PhysD1, TopoD1> ParamFieldTupleType;

ERRORESTIMATE_DGBR2_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD1, ParamFieldTupleType )

typedef XField<PhysD2, TopoD2> ParamFieldTupleSpaceTime;

ERRORESTIMATE_DGBR2_INSTANTIATE_SPACETIME( PDEClass, BCVector, TopoD2, ParamFieldTupleSpaceTime )

}
