// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ERRORESTIMATE_DGBR2_INSTANTIATE
#include "ErrorEstimate/DG/ErrorEstimate_DGBR2_impl.h"

#include "pde/NS/TraitsRANSSA.h"
#include "pde/NS/PDERANSSA2D.h"
#include "pde/NS/PDERANSSA_Brenner2D.h"
#include "pde/NS/BCRANSSA2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/QRANSSA2D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime2D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"

#include "Field/XFieldArea.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

namespace SANS
{
typedef QTypePrimitiveRhoPressure QType;
typedef ViscosityModel_Sutherland ViscosityModelType;
typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
typedef PDERANSSA2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
typedef BCRANSSA2DVector<TraitsSizeRANSSA, TraitsModelRANSSAClass>::type BCVector;

typedef FieldTuple<Field<PhysD2, TopoD2, Real>, XField<PhysD2, TopoD2>, TupleClass<>> ParamFieldTupleType;

ERRORESTIMATE_DGBR2_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD2, ParamFieldTupleType )

typedef PDERANSSA_Brenner2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEBClass;
ERRORESTIMATE_DGBR2_INSTANTIATE_SPACE( PDEBClass, BCVector, TopoD2, ParamFieldTupleType )

}
