// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ERRORESTIMATE_DGBR2_INSTANTIATE
#include "ErrorEstimate/DG/ErrorEstimate_DGBR2_impl.h"

#include "pde/PorousMedia/Q1DPrimitive_pnSw.h"
#include "pde/PorousMedia/DensityModel.h"
#include "pde/PorousMedia/PorosityModel.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/ViscosityModel_Constant.h"
#include "pde/PorousMedia/CapillaryModel.h"

#include "pde/PorousMedia/PDETwoPhase1D.h"
#include "pde/PorousMedia/BCTwoPhase1D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"

#include "Field/XFieldLine.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLiftLine_DG_Cell.h"

#include "Field/XFieldArea.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"


namespace SANS
{
typedef DensityModel_Comp DensityModel;
typedef PorosityModel_Comp PorosityModel;
typedef RelPermModel_PowerLaw RelPermModel;
typedef ViscosityModel_Constant ViscModel;
typedef CapillaryModel_Linear CapillaryModel;

typedef TraitsModelTwoPhase<QTypePrimitive_pnSw, DensityModel, DensityModel, PorosityModel,
                            RelPermModel, RelPermModel, ViscModel, ViscModel, Real, CapillaryModel> TraitsModelClass;

typedef PDETwoPhase1D<TraitsSizeTwoPhase, TraitsModelClass> PDEClass;

typedef BCTwoPhase1DVector<PDEClass>::type BCVector;


typedef XField<PhysD2, TopoD2> ParamFieldTupleSpaceTime;

ERRORESTIMATE_DGBR2_INSTANTIATE_SPACETIME( PDEClass, BCVector, TopoD2, ParamFieldTupleSpaceTime )

}
