// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ERRORESTIMATE_DGBR2_INSTANTIATE
#include "ErrorEstimate/DG/ErrorEstimate_DGBR2_impl.h"

#include "pde/PorousMedia/Q1DPrimitive_Sw.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/TraitsBuckleyLeverettArtificialViscosity.h"
#include "pde/PorousMedia/PDEBuckleyLeverett_ArtificialViscosity1D.h"
#include "pde/PorousMedia/BCBuckleyLeverett_ArtificialViscosity1D.h"

#include "pde/ArtificialViscosity/AVVariable.h"
#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_Source1D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor1D.h"

//#include "pde/NDConvert/PDENDConvertSpace1D.h"
//#include "pde/NDConvert/BCNDConvertSpace1D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"

//#include "Field/XFieldLine.h"
//#include "Field/FieldLine_DG_Cell.h"
//#include "Field/FieldLine_DG_BoundaryTrace.h"
//#include "Field/FieldLiftLine_DG_Cell.h"

#include "Field/XFieldArea.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"


namespace SANS
{
typedef RelPermModel_PowerLaw RelPermModel;
typedef CapillaryModel_Linear CapillaryModel;
typedef QTypePrimitive_Sw QType;

typedef TraitsModelBuckleyLeverett<QType, RelPermModel, CapillaryModel> TraitsModelClass;

typedef PDEBuckleyLeverett_ArtificialViscosity1D<TraitsSizeBuckleyLeverettArtificialViscosity,
                                                 TraitsModelClass> PDEBaseClass;

typedef AVSensor_AdvectiveFlux1D_Uniform<TraitsSizeBuckleyLeverettArtificialViscosity> SensorAdvectiveFlux;
typedef AVSensor_ViscousFlux1D_GenHScale<TraitsSizeBuckleyLeverettArtificialViscosity> SensorViscousFlux;
typedef AVSensor_Source1D_TwoPhase<PDEBaseClass> SensorSource;

typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
typedef PDEmitAVSensor1D<TraitsSizeBuckleyLeverettArtificialViscosity, TraitsModelAV> PDEClass;

typedef BCBuckleyLeverett_ArtificialViscosity1DVector<TraitsSizeBuckleyLeverettArtificialViscosity,
                                                      TraitsModelClass> BCVector;

//typedef typename DLA::MatrixSymS<PhysD1::D,Real> HType;
//
//typedef FieldTuple<Field<PhysD1, TopoD1, HType>, XField<PhysD1, TopoD1>, TupleClass<>> ParamFieldTupleType;
//
//ERRORESTIMATE_DGBR2_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD1, ParamFieldTupleType )

typedef typename DLA::MatrixSymS<PhysD2::D,Real> HTypeSpaceTime;

typedef FieldTuple<Field<PhysD2, TopoD2, HTypeSpaceTime>, XField<PhysD2, TopoD2>, TupleClass<>> ParamFieldTupleSpaceTime;

ERRORESTIMATE_DGBR2_INSTANTIATE_SPACETIME( PDEClass, BCVector, TopoD2, ParamFieldTupleSpaceTime )

}
