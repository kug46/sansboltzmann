// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ERRORESTIMATE_DGBR2_INSTANTIATE
#include "ErrorEstimate/DG/ErrorEstimate_DGBR2_impl.h"

#include "pde/PorousMedia/Q1DPrimitive_Sw.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/TraitsBuckleyLeverett.h"
#include "pde/PorousMedia/PDEBuckleyLeverett1D.h"
#include "pde/PorousMedia/BCBuckleyLeverett1D.h"

#include "pde/Sensor/PDESensorParameter1D.h"
#include "pde/Sensor/BCSensorParameter1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"

#include "Field/XFieldLine.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLiftLine_DG_Cell.h"

#include "Field/XFieldArea.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"


namespace SANS
{
typedef RelPermModel_PowerLaw RelPermModel;
typedef CapillaryModel_Linear CapillaryModel;
typedef TraitsModelBuckleyLeverett<QTypePrimitive_Sw, RelPermModel, CapillaryModel> TraitsModelClass;
typedef PDEBuckleyLeverett1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEClass;

typedef BCBuckleyLeverett1DVector<PDEClass> BCVector;

typedef XField<PhysD1, TopoD1> ParamFieldTupleType;

ERRORESTIMATE_DGBR2_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD1, ParamFieldTupleType )

typedef XField<PhysD2, TopoD2> ParamFieldTupleSpaceTime;

typedef PDENDConvertSpaceTime<PhysD1, PDEClass> PDENDSpaceTime;

ERRORESTIMATE_DGBR2_INSTANTIATE_SPACETIME( PDEClass, BCVector, TopoD2, ParamFieldTupleSpaceTime )

}
