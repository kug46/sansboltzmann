// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ERRORESTIMATE_DGBR2_INSTANTIATE
#include "ErrorEstimate/DG/ErrorEstimate_DGBR2_impl.h"

#include "pde/PorousMedia/Q1DPrimitive_Sw.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/TraitsBuckleyLeverett.h"
#include "pde/PorousMedia/PDEBuckleyLeverett_ArtificialViscosity1D.h"
#include "pde/PorousMedia/BCBuckleyLeverett_ArtificialViscosity1D.h"
#include "pde/PorousMedia/Sensor_BuckleyLeverett.h"

#include "pde/Sensor/PDESensorParameter1D.h"
#include "pde/Sensor/BCSensorParameter1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"

#include "Field/XFieldLine.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLiftLine_DG_Cell.h"

//#include "pde/Sensor/PDESensorParameter2D.h"
//#include "pde/Sensor/BCSensorParameter2D.h"

//#include "pde/NDConvert/PDENDConvertSpace2D.h"
//#include "pde/NDConvert/BCNDConvertSpace2D.h"

#include "Field/XFieldArea.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"


namespace SANS
{
typedef QTypePrimitive_Sw QType;
typedef RelPermModel_PowerLaw RelPermModel;
typedef CapillaryModel_Linear CapillaryModel;
typedef TraitsModelBuckleyLeverett<QType, RelPermModel, CapillaryModel> TraitsModelClass;
typedef PDEBuckleyLeverett1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEClass;
typedef PDEBuckleyLeverett_ArtificialViscosity1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEAVClass;

typedef BCBuckleyLeverett_ArtificialViscosity1DVector<TraitsSizeBuckleyLeverett, TraitsModelClass> BCVector_Primal;

typedef typename DLA::MatrixSymS<PhysD1::D,Real> HType;
typedef typename DLA::MatrixSymS<PhysD2::D,Real> HType_ST;

typedef typename MakeTuple<FieldTuple, Field<PhysD2, TopoD2, HType_ST>,
                                       Field<PhysD2, TopoD2, SensorParameterTraits<PhysD2>::ArrayQ<Real>>,
                                       XField<PhysD2, TopoD2>>::type ParamFieldTuple_ST_Primal;

typedef Sensor_BuckleyLeverett<PDEClass> Sensor;

typedef Sensor_AdvectiveFlux1D_Uniform Sensor_Advection1D;
typedef Sensor_ViscousFlux1D_GenHScale Sensor_Diffusion1D;
typedef Source1D_JumpSensor_BuckleyLeverett<Sensor> Source_JumpSensor1D;

typedef PDESensorParameter<PhysD1,
                           SensorParameterTraits<PhysD1>,
                           Sensor_Advection1D,
                           Sensor_Diffusion1D,
                           Source_JumpSensor1D> PDEClass_Sensor1D;

typedef typename MakeTuple<FieldTuple, Field<PhysD1, TopoD1, HType>,
                                       Field<PhysD1, TopoD1, PDEClass::ArrayQ<Real>>,
                                       XField<PhysD1, TopoD1>>::type ParamFieldTuple_Sensor;

typedef typename MakeTuple<FieldTuple, Field<PhysD2, TopoD2, HType_ST>,
                                       Field<PhysD2, TopoD2, PDEClass::ArrayQ<Real>>,
                                       XField<PhysD2, TopoD2>>::type ParamFieldTuple_ST_Sensor;

typedef BCSensorParameter1DVector<Sensor_Advection1D, Sensor_Diffusion1D> BCVector_Sensor1D;

//ERRORESTIMATE_DGBR2_INSTANTIATE_SPACE( PDEClass_Primal, BCVector_Primal, TopoD1, ParamFieldTuple_Primal )
//ERRORESTIMATE_DGBR2_INSTANTIATE_SPACE( PDEClass_Sensor, BCVector_Sensor, TopoD1, ParamFieldTuple_Sensor )

ERRORESTIMATE_DGBR2_INSTANTIATE_SPACETIME( PDEAVClass, BCVector_Primal, TopoD2, ParamFieldTuple_ST_Primal )
ERRORESTIMATE_DGBR2_INSTANTIATE_SPACETIME( PDEClass_Sensor1D, BCVector_Sensor1D, TopoD2, ParamFieldTuple_ST_Sensor )

#if 0
// 2D spatial sensor

typedef Sensor_AdvectiveFlux2D_Uniform Sensor_Advection2D;
typedef Sensor_ViscousFlux2D_GenHScale Sensor_Diffusion2D;
typedef Source2D_JumpSensor_BuckleyLeverett<Sensor> Source_JumpSensor2D;

typedef PDESensorParameter<PhysD2,
                           SensorParameterTraits<PhysD2>,
                           Sensor_Advection2D,
                           Sensor_Diffusion2D,
                           Source_JumpSensor2D> PDEClass_Sensor2D;

typedef typename MakeTuple<FieldTuple, Field<PhysD2, TopoD2, HType_ST>,
                                       Field<PhysD2, TopoD2, PDEClass::ArrayQ<Real>>,
                                       XField<PhysD2, TopoD2>>::type ParamFieldTupleType_Sensor2D;

typedef BCSensorParameter2DVector<Sensor_Advection2D, Sensor_Diffusion2D> BCVector_Sensor2D;

ERRORESTIMATE_DGBR2_INSTANTIATE_SPACE( PDEClass_Sensor2D, BCVector_Sensor2D, TopoD2, ParamFieldTupleType_Sensor2D )
#endif
}
