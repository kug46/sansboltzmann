// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef DISTRIBUTEBOUNDARYTRACEGROUPS_H
#define DISTRIBUTEBOUNDARYTRACEGROUPS_H

// boundary-trace estimate field allocation functions

#include "Topology/ElementTopology.h"
#include "Field/Field.h"

namespace SANS
{

template< class TopologyTrace, class TopologyL, class TopoDim, class PhysDim>
void
DistributeBoundaryTraceGroups_Distribution(
    typename Field<PhysDim,TopoDim,Real>::template FieldCellGroupType<TopologyL>& efldCell,
    const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
    const typename Field_DG_BoundaryTrace<PhysDim, TopoDim, Real>::template
    FieldTraceGroupType<TopologyTrace>& eBfldTrace)
{
  typedef typename Field<PhysDim,TopoDim,Real>::template FieldCellGroupType<TopologyL> EFieldCellGroupType;
  typedef typename Field_DG_BoundaryTrace<PhysDim, TopoDim, Real>::template FieldTraceGroupType<TopologyTrace> EBFieldTraceGroupType;
  typedef typename EFieldCellGroupType::template ElementType<> ElementEFieldClass;
  typedef typename EBFieldTraceGroupType::template ElementType<> ElementEBFieldTraceClass;

  // Elements
  ElementEFieldClass efldElem( efldCell.basis() );
  ElementEBFieldTraceClass eBfldElem(eBfldTrace.basis() );

  // loop over elements in trace group
  for (int elem = 0; elem< xfldTrace.nElem(); elem++)
  {
    int elemL = xfldTrace.getElementLeft( elem );
    efldCell.getElement( efldElem, elemL);
    eBfldTrace.getElement( eBfldElem, elem );
    efldElem.DOF(0) += eBfldElem.DOF(0);
    efldCell.setElement( efldElem, elemL);
  }
}

//----------------------------------------------------------------------------//
// Internal function for allocating boundary Trace Groups
template< class TopDim >
class DistributeBoundaryTraceGroups;

// base class interface
template<>
class DistributeBoundaryTraceGroups<TopoD1>
{
public:
  typedef TopoD1 TopoDim;

  template< class PhysDim, class TopologyTrace >
  static void
  LeftTopology(Field<PhysDim, TopoDim, Real>& efld,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename Field_DG_BoundaryTrace<PhysDim, TopoDim, Real>::template
              FieldTraceGroupType<TopologyTrace>& eBfldTraceType)
  {
    // extract left cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( efld.getCellGroupBase(groupL).topoTypeID() == typeid(Line) )
    {
      DistributeBoundaryTraceGroups_Distribution<TopologyTrace, Line, TopoDim, PhysDim>(
          efld.template getCellGroup<Line>(groupL),xfldTrace,eBfldTraceType);
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
  }

  template < class PhysDim>
  static void
  distribute(
      Field<PhysDim, TopoDim, Real>& efld,
      const Field_DG_BoundaryTrace<PhysDim, TopoDim, Real>& eBfld)
  {
    const XField<PhysDim, TopoDim>& xfld = efld.getXField();

    // loop over boundary trace groups
    for (int group = 0; group < eBfld.nBoundaryTraceGroups(); group++)
    {
      const int groupGlobal = eBfld.getGlobalBoundaryTraceGroupMap(group);

      // check Boundary Trace Topology type
      if ( xfld.getBoundaryTraceGroupBase(groupGlobal).topoTypeID() == typeid(Node) )
      {
        LeftTopology<PhysDim, Node>(
            efld,
            xfld.template getBoundaryTraceGroup<Node>(groupGlobal),
            eBfld.template getBoundaryTraceGroup<Node>(group));
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown trace topology" );
    }
  }
};

// base class interface
template<>
class DistributeBoundaryTraceGroups<TopoD2>
{
public:
  typedef TopoD2 TopoDim;

  template< class PhysDim, class TopologyTrace >
  static void
  LeftTopology(Field<PhysDim, TopoDim, Real>& efld,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename Field_DG_BoundaryTrace<PhysDim, TopoDim, Real>::template
              FieldTraceGroupType<TopologyTrace>& eBfldTraceType)
  {
    // extract left cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( efld.getCellGroupBase(groupL).topoTypeID() == typeid(Triangle) )
    {
      DistributeBoundaryTraceGroups_Distribution<TopologyTrace, Triangle, TopoDim, PhysDim>(
          efld.template getCellGroup<Triangle>(groupL),xfldTrace,eBfldTraceType);
    }
    else if ( efld.getCellGroupBase(groupL).topoTypeID() == typeid(Quad) )
    {
      DistributeBoundaryTraceGroups_Distribution<TopologyTrace, Quad, TopoDim, PhysDim>(
          efld.template getCellGroup<Quad>(groupL),xfldTrace,eBfldTraceType);
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
  }

  template < class PhysDim >
  static void
  distribute(
      Field<PhysDim, TopoDim, Real>& efld,
      const Field_DG_BoundaryTrace<PhysDim, TopoDim, Real>& eBfld)
  {
    const XField<PhysDim, TopoDim>& xfld = efld.getXField();
    // loop over local boundary trace groups
    for (int group = 0; group < eBfld.nBoundaryTraceGroups(); group++)
    {
      const int groupGlobal = eBfld.getGlobalBoundaryTraceGroupMap(group);

      // check Boundary Trace Topology type
      if ( xfld.getBoundaryTraceGroupBase(groupGlobal).topoTypeID() == typeid(Line) )
      {
        LeftTopology<PhysDim, Line>(
            efld,
            xfld.template getBoundaryTraceGroup<Line>(groupGlobal),
            eBfld.template getBoundaryTraceGroup<Line>(group));
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown trace topology" );
    }
  }
};

// base class interface
template<>
class DistributeBoundaryTraceGroups<TopoD3>
{
public:
  typedef TopoD3 TopoDim;

  template< class PhysDim >
  static void
  LeftTopology_Triangle(
      Field<PhysDim, TopoDim, Real>& efld,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Triangle>& xfldTrace,
      const typename Field_DG_BoundaryTrace<PhysDim, TopoDim, Real>::template
              FieldTraceGroupType<Triangle>& eBfldTraceType)
  {
    // extract left cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( efld.getCellGroupBase(groupL).topoTypeID() == typeid(Tet) )
    {
      DistributeBoundaryTraceGroups_Distribution<Triangle, Tet, TopoDim, PhysDim>(
          efld.template getCellGroup<Tet>(groupL),xfldTrace,eBfldTraceType);
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
  }
  template< class PhysDim >
  static void
  LeftTopology_Quad(
      Field<PhysDim, TopoDim, Real>& efld,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Quad>& xfldTrace,
      const typename Field_DG_BoundaryTrace<PhysDim, TopoDim, Real>::template
              FieldTraceGroupType<Quad>& eBfldTraceType)
  {
    // extract left cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( efld.getCellGroupBase(groupL).topoTypeID() == typeid(Tet) )
    {
      DistributeBoundaryTraceGroups_Distribution<Quad, Hex, TopoDim, PhysDim>(
          efld.template getCellGroup<Hex>(groupL),xfldTrace,eBfldTraceType);
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
  }

  template < class PhysDim >
  static void
  distribute(
      Field<PhysDim, TopoDim, Real>& efld,
      const Field_DG_BoundaryTrace<PhysDim, TopoDim, Real>& eBfld)
  {
    const XField<PhysDim, TopoDim>& xfld = efld.getXField();
    // loop over boundary trace groups
    for (int group = 0; group < eBfld.nBoundaryTraceGroups(); group++)
    {
      const int groupGlobal = eBfld.getGlobalBoundaryTraceGroupMap(group);

      // check Boundary Trace Topology type
      if ( xfld.getBoundaryTraceGroupBase(groupGlobal).topoTypeID() == typeid(Triangle) )
      {
        LeftTopology_Triangle<PhysDim>(
            efld,
            xfld.template getBoundaryTraceGroup<Triangle>(groupGlobal),
            eBfld.template getBoundaryTraceGroup<Triangle>(group));
      }
      else if ( xfld.getBoundaryTraceGroupBase(groupGlobal).topoTypeID() == typeid(Quad) )
      {
        LeftTopology_Quad<PhysDim>(
            efld,
            xfld.template getBoundaryTraceGroup<Quad>(groupGlobal),
            eBfld.template getBoundaryTraceGroup<Quad>(group));
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown trace topology" );
    }
  }
};

}

#endif  // DISTRIBUTEBOUNDARYTRACEGROUPS_H
