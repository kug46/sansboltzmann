// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ERRORESTIMATEINTERIORTRACE_VMSD_BR2_H
#define ERRORESTIMATEINTERIORTRACE_VMSD_BR2_H

#include "tools/Tuple.h"


#include "Topology/ElementTopology.h"
#include "Quadrature/Quadrature.h"

#include "Field/Field.h"
#include "Field/FieldData/FieldDataReal_Cell.h"

#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/ElementIntegral.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

 #include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  DG interior-trace integral
//

template<class IntegrandITrace>
class ErrorEstimateInteriorTrace_VMSD_BR2_impl :
    public GroupIntegralInteriorTraceType< ErrorEstimateInteriorTrace_VMSD_BR2_impl<IntegrandITrace> >
{
public:
  typedef typename IntegrandITrace::PhysDim PhysDim;
  typedef typename IntegrandITrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandITrace::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename IntegrandITrace::PDE PDE;

  // Save off the boundary trace integrand
  explicit ErrorEstimateInteriorTrace_VMSD_BR2_impl( const IntegrandITrace& fcnITrace,
                                                const std::map<int,std::vector<int>>& cellITraceGroups,
                                                const int& cellgroup, const int& cellelem,
                                                DLA::VectorD<Real>& estPDEElemCell,
                                                DLA::VectorD<Real>& estPDEpElemCell) :
                                                    fcnITrace_(fcnITrace),
                                                    cellITraceGroups_(cellITraceGroups),
                                                    cellgroup_(cellgroup), cellelem_(cellelem),
                                                    estPDEElemCell_(estPDEElemCell),
                                                    estPDEpElemCell_(estPDEpElemCell)
  {
    for (std::map<int,std::vector<int>>::const_iterator it = cellITraceGroups_.begin(); it != cellITraceGroups_.end(); ++it)
      traceGroupIndices_.push_back( it->first );
  }

  std::size_t nInteriorTraceGroups() const { return traceGroupIndices_.size(); }
  std::size_t interiorTraceGroup(const int n) const { return traceGroupIndices_[n]; }

//----------------------------------------------------------------------------//
  template <class TopoDim>
  void check( const typename MakeTuple<FieldTuple,
              Field<PhysDim, TopoDim, ArrayQ>, // q
              Field<PhysDim, TopoDim, ArrayQ>, // qp
              FieldLift<PhysDim,TopoDim,VectorArrayQ>,
              Field<PhysDim, TopoDim, ArrayQ>, // w
              Field<PhysDim, TopoDim, ArrayQ>, // wp
              FieldLift<PhysDim,TopoDim,VectorArrayQ>,
              Field<PhysDim, TopoDim, Real>, // e
              Field<PhysDim, TopoDim, Real>>::type& flds // ep
              ) const // eI
  {
    const Field<PhysDim, TopoDim, ArrayQ>&       qfld  = get<0>(flds);
    const Field<PhysDim, TopoDim, ArrayQ>&       qpfld  = get<1>(flds);
    //r
    const Field<PhysDim, TopoDim, ArrayQ>&       wfld  = get<3>(flds);
    const Field<PhysDim, TopoDim, ArrayQ>&       wpfld  = get<4>(flds);
    //s
    const Field<PhysDim, TopoDim, Real>&         efld  = get<6>(flds);
    const Field<PhysDim, TopoDim, Real>&         epfld = get<7>(flds);

    SANS_ASSERT( &qfld.getXField() == &qpfld.getXField() );  // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &wfld.getXField() );  // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &wpfld.getXField() );  // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &efld.getXField() );  // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &epfld.getXField() ); // check both were made off the same grid
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the interior trace group
  template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename MakeTuple< FieldTuple,
                                Field<PhysDim,TopoDim,ArrayQ>,
                                Field<PhysDim,TopoDim,ArrayQ>,
                                FieldLift<PhysDim,TopoDim,VectorArrayQ>,
                                Field<PhysDim,TopoDim,ArrayQ>,
                                Field<PhysDim,TopoDim,ArrayQ>,
                                FieldLift<PhysDim,TopoDim,VectorArrayQ>,
                                Field<PhysDim,TopoDim,Real>,
                                Field<PhysDim,TopoDim,Real>>::type::template FieldCellGroupType<TopologyL>& fldsCellL,
      const int cellGroupGlobalR,
      const typename XFieldType::template FieldCellGroupType<TopologyR>& xfldCellR,
      const typename MakeTuple< FieldTuple,
                                Field<PhysDim,TopoDim,ArrayQ>,
                                Field<PhysDim,TopoDim,ArrayQ>,
                                FieldLift<PhysDim,TopoDim,VectorArrayQ>,
                                Field<PhysDim,TopoDim,ArrayQ>,
                                Field<PhysDim,TopoDim,ArrayQ>,
                                FieldLift<PhysDim,TopoDim,VectorArrayQ>,
                                Field<PhysDim,TopoDim,Real>,
                                Field<PhysDim,TopoDim,Real>>::type::template FieldCellGroupType<TopologyR>& fldsCellR,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      int quadratureorder )
  {
    // Left types
    typedef typename XFieldType                           ::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ >     ::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename FieldLift<PhysDim, TopoDim, VectorArrayQ >     ::template FieldCellGroupType<TopologyL> RFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, Real >       ::template FieldCellGroupType<TopologyL> EFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL ::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL ::template ElementType<> ElementQFieldClassL;
    typedef typename RFieldCellGroupTypeL ::template ElementType<> ElementRFieldClassL;
    typedef typename EFieldCellGroupTypeL ::template ElementType<> ElementEFieldClassL;

    // Right types
    typedef typename XFieldType                           ::template FieldCellGroupType<TopologyR> XFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, ArrayQ >     ::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;
    typedef typename FieldLift<PhysDim, TopoDim, VectorArrayQ >     ::template FieldCellGroupType<TopologyR> RFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, Real >       ::template FieldCellGroupType<TopologyR> EFieldCellGroupTypeR;

    typedef typename XFieldCellGroupTypeR ::template ElementType<> ElementXFieldClassR;
    typedef typename QFieldCellGroupTypeR ::template ElementType<> ElementQFieldClassR;
    typedef typename RFieldCellGroupTypeR ::template ElementType<> ElementRFieldClassR;
    typedef typename EFieldCellGroupTypeR ::template ElementType<> ElementEFieldClassR;

    //Trace types
    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType ::template ElementType<> ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    // separating tuple and casting away const
    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const QFieldCellGroupTypeL& qpfldCellL = get<1>(fldsCellL);
    const RFieldCellGroupTypeL& rfldCellL = get<2>(fldsCellL);

    const QFieldCellGroupTypeL& wfldCellL = get<3>(fldsCellL);
    const QFieldCellGroupTypeL& wpfldCellL = get<4>(fldsCellL);
    const RFieldCellGroupTypeL& sfldCellL = get<5>(fldsCellL);

    EFieldCellGroupTypeL& efldCellL = const_cast<EFieldCellGroupTypeL&>(get<6>(fldsCellL));
    EFieldCellGroupTypeL& epfldCellL = const_cast<EFieldCellGroupTypeL&>(get<7>(fldsCellL));

    const QFieldCellGroupTypeR& qfldCellR = get<0>(fldsCellR);
    const QFieldCellGroupTypeR& qpfldCellR = get<1>(fldsCellR);
    const RFieldCellGroupTypeR& rfldCellR = get<2>(fldsCellR);

    const QFieldCellGroupTypeR& wfldCellR = get<3>(fldsCellR);
    const QFieldCellGroupTypeR& wpfldCellR = get<4>(fldsCellR);
    const RFieldCellGroupTypeR& sfldCellR = get<5>(fldsCellR);

    EFieldCellGroupTypeR& efldCellR = const_cast<EFieldCellGroupTypeR&>(get<6>(fldsCellR));
    EFieldCellGroupTypeR& epfldCellR = const_cast<EFieldCellGroupTypeR&>(get<7>(fldsCellR));

    // LEFT element field variables
    ElementXFieldClassL  xfldElemL(  xfldCellL.basis() );
    ElementQFieldClassL  qfldElemL(  qfldCellL.basis() );
    ElementQFieldClassL  qpfldElemL(  qpfldCellL.basis() );
    ElementRFieldClassL  rfldElemL(  rfldCellL.basis() );

    ElementQFieldClassL  wfldElemL(  wfldCellL.basis() );
    ElementQFieldClassL  wpfldElemL(  wpfldCellL.basis() );
    ElementRFieldClassL  sfldElemL(  sfldCellL.basis() );

    ElementEFieldClassL  efldElemL(  efldCellL.basis() );
    ElementEFieldClassL  epfldElemL( epfldCellL.basis() );

    // RIGHT element field variables
    ElementXFieldClassR  xfldElemR(  xfldCellR.basis() );
    ElementQFieldClassR  qfldElemR(  qfldCellR.basis() );
    ElementQFieldClassR  qpfldElemR(  qpfldCellR.basis() );
    ElementRFieldClassR  rfldElemR(  rfldCellR.basis() );

    ElementQFieldClassR  wfldElemR(  wfldCellR.basis() );
    ElementQFieldClassR  wpfldElemR(  wpfldCellR.basis() );
    ElementRFieldClassR  sfldElemR(  sfldCellR.basis() );

    ElementEFieldClassR  efldElemR(  efldCellR.basis() );
    ElementEFieldClassR epfldElemR( epfldCellR.basis() );

    // TRACE element field variables
    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // estimate arrays
    const int nIntegrandL = efldElemL.nDOF(), nIntegrandR = efldElemR.nDOF();
    const int npIntegrandL = epfldElemL.nDOF(), npIntegrandR = epfldElemR.nDOF();

//    SANS_ASSERT( nIntegrandL == estPDEElemCell_.size() );
//    SANS_ASSERT( npIntegrandL == estPDEpElemCell_.size() );
//    SANS_ASSERT( nIntegrandR == estPDEElemCell_.size() );
//    SANS_ASSERT( npIntegrandR == estPDEpElemCell_.size() );

    DLA::VectorD<Real> estPDEElemL( nIntegrandL ), estPDEElemR( nIntegrandR );
    DLA::VectorD<Real> estPDEpElemL( npIntegrandL ), estPDEpElemR( npIntegrandR );

    // trace element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, Real> integralL(quadratureorder,nIntegrandL);
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, Real> integralR(quadratureorder,nIntegrandR);

    // just to make sure things are consistent
    SANS_ASSERT( xfldCellL.nElem() == efldCellL.nElem() );
    SANS_ASSERT( xfldCellL.nElem() == epfldCellL.nElem() );
    SANS_ASSERT( xfldCellR.nElem() == efldCellR.nElem() );
    SANS_ASSERT( xfldCellR.nElem() == epfldCellR.nElem() );

    const std::vector<int>& traceElemList = cellITraceGroups_.at(traceGroupGlobal);

    // loop over trace elements in list
    for (int i = 0; i < (int) traceElemList.size(); i++)
    {
      const int elem = traceElemList[i];

      const int elemL = xfldTrace.getElementLeft( elem );
      const int elemR = xfldTrace.getElementRight( elem );

      xfldTrace.getElement( xfldElemTrace, elem );

      if (xfldTrace.getGroupLeft() == cellgroup_ && elemL == cellelem_)
      {
        //The cell that called this function is to the left of the trace
        const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

        // copy global grid/solution DOFs to element
        xfldCellL.getElement( xfldElemL, elemL );
        qfldCellL.getElement( qfldElemL, elemL );
        qpfldCellL.getElement( qpfldElemL, elemL );
        rfldCellL.getElement( rfldElemL, elemL, canonicalTraceL.trace );

        wfldCellL.getElement( wfldElemL, elemL );
        wpfldCellL.getElement( wpfldElemL, elemL );
        sfldCellL.getElement( sfldElemL, elemL, canonicalTraceL.trace );

        for (int k = 0; k < nIntegrandL; k++) estPDEElemL[k] = 0;
        for (int k = 0; k < npIntegrandL; k++) estPDEpElemL[k] = 0;

        integralL( fcnITrace_.integrand( xfldElemTrace, canonicalTraceL, +1, xfldElemL,
                                         qfldElemL, qpfldElemL, rfldElemL,
                                         wfldElemL, wpfldElemL, sfldElemL, efldElemL ),
                                         xfldElemTrace, estPDEElemL.data(), nIntegrandL );

        // PDE
        estPDEElemCell_ += estPDEElemL;
//        estPDEpElemCell_  += estPDEpElemL;

      }
      else if (xfldTrace.getGroupRight() == cellgroup_ && elemR == cellelem_)
      {
        //The cell that called this function is to the right of the trace
        const CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

        // copy global grid/solution DOFs to element
        xfldCellR.getElement( xfldElemR, elemR );
        qfldCellR.getElement( qfldElemR, elemR );
        qpfldCellR.getElement( qpfldElemR, elemR );
        rfldCellR.getElement( rfldElemR, elemR, canonicalTraceR.trace );

        wfldCellR.getElement( wfldElemR, elemR );
        wpfldCellR.getElement( wpfldElemR, elemR );
        sfldCellR.getElement( sfldElemR, elemR, canonicalTraceR.trace );

        for (int k = 0; k < nIntegrandR; k++) estPDEElemR[k] = 0;
        for (int k = 0; k < npIntegrandR; k++) estPDEpElemR[k] = 0;

        integralR( fcnITrace_.integrand( xfldElemTrace, canonicalTraceR, -1, xfldElemR,
                                         qfldElemR, qpfldElemR, rfldElemR,
                                         wfldElemR, wpfldElemR, sfldElemR, efldElemR ),
                                         xfldElemTrace, estPDEElemR.data(), nIntegrandR );


        // PDE
        estPDEElemCell_ += estPDEElemR;
//        estPDEpElemCell_ += estPDEpElemR;
      }
      else
        SANS_DEVELOPER_EXCEPTION("ErrorEstimateInteriorTrace_VMSD_BR2_impl::integrate - Invalid configuration!");
    }
  }

protected:
  const IntegrandITrace& fcnITrace_;
  const std::map<int,std::vector<int>>& cellITraceGroups_;
  const int& cellgroup_;
  const int& cellelem_;

  DLA::VectorD<Real>& estPDEElemCell_;
  DLA::VectorD<Real>& estPDEpElemCell_;

  std::vector<int> traceGroupIndices_;
};


// Factory function

template<class IntegrandITrace>
ErrorEstimateInteriorTrace_VMSD_BR2_impl<IntegrandITrace>
ErrorEstimateInteriorTrace_VMSD_BR2( const IntegrandInteriorTraceType<IntegrandITrace>& fcnITrace,
                                const std::map<int,std::vector<int>>& cellITraceGroups,
                                const int& cellgroup, const int& cellelem,
                                DLA::VectorD<Real>& estPDEElemCell,
                                DLA::VectorD<Real>& estPDEpElemCell )
{
  return ErrorEstimateInteriorTrace_VMSD_BR2_impl<IntegrandITrace>(
      fcnITrace.cast(), cellITraceGroups, cellgroup, cellelem, estPDEElemCell, estPDEpElemCell);
}


}

#endif  // ERRORESTIMATEINTERIORTRACE_VMSD_BR2_H
