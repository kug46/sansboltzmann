// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ERRORESTIMATEBOUNDARYTRACE_DISPATCH_VMSD_H
#define ERRORESTIMATEBOUNDARYTRACE_DISPATCH_VMSD_H

// boundary-trace integral residual functions

#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateBoundaryTraceGroups_BoundaryCell.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"
#include "ErrorEstimateBoundaryTrace_VMSD.h"
#include "ErrorEstimateBoundaryTrace_VMSD_BR2.h"

namespace SANS
{

#if 1
//---------------------------------------------------------------------------//
//
// Dispatch class for BC's without Lagrange multipliers
//
//---------------------------------------------------------------------------//
template< class XFieldType, class PhysDim, class TopoDim, class ArrayQ>
class ErrorEstimateBoundaryTrace_Dispatch_VMSD_impl
{
public:
  ErrorEstimateBoundaryTrace_Dispatch_VMSD_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qpfld,
      const Field<PhysDim, TopoDim, ArrayQ>& wfld,
      Field<PhysDim, TopoDim, Real>& efld,
      const int* quadratureorder, int ngroup,
      const int accSign)
    : xfld_(xfld),
      qfld_(qfld), qpfld_(qpfld), wfld_(wfld), efld_(efld),
      quadratureorder_(quadratureorder), ngroup_(ngroup),
      accSign_(accSign)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups<TopoDim>::integrate(
        ErrorEstimateBoundaryTrace_VMSD(fcn, accSign_),
        xfld_,
        (qfld_, qpfld_, wfld_, efld_),
        quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qpfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& wfld_;
  Field<PhysDim, TopoDim, Real>& efld_;
  const int* quadratureorder_;
  const int ngroup_;
  const int accSign_;
};

// Factory function

template< class XFieldType, class PhysDim, class TopoDim, class ArrayQ >
ErrorEstimateBoundaryTrace_Dispatch_VMSD_impl<XFieldType, PhysDim, TopoDim, ArrayQ>
ErrorEstimateBoundaryTrace_Dispatch_VMSD( const XFieldType& xfld,
                                              const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                              const Field<PhysDim, TopoDim, ArrayQ>& qpfld,
                                              const Field<PhysDim, TopoDim, ArrayQ>& wfld,
                                              Field<PhysDim, TopoDim, Real>& efld,
                                              const int* quadratureorder, int ngroup,
                                              const int accSign = 1)
{
  return ErrorEstimateBoundaryTrace_Dispatch_VMSD_impl<XFieldType, PhysDim, TopoDim,ArrayQ>(
      xfld,
      qfld, qpfld, wfld, efld, quadratureorder, ngroup, accSign);
}

#endif


#if 1
//---------------------------------------------------------------------------//
//
// Dispatch class for BC's without Lagrange multipliers
//
//---------------------------------------------------------------------------//
template< class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ>
class ErrorEstimateBoundaryTrace_Dispatch_VMSD_BR2_impl
{
public:
  ErrorEstimateBoundaryTrace_Dispatch_VMSD_BR2_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qpfld,
      const FieldLift_DG_BoundaryTrace<PhysDim, TopoDim, VectorArrayQ>& rbfld,
      const Field<PhysDim, TopoDim, ArrayQ>& wfld,
      const Field<PhysDim, TopoDim, ArrayQ>& wpfld,
      const FieldLift_DG_BoundaryTrace<PhysDim, TopoDim, VectorArrayQ>& sbfld,
      Field<PhysDim, TopoDim, Real>& efld,
      const int* quadratureorder, int ngroup,
      const int accSign)
    : xfld_(xfld),
      qfld_(qfld), qpfld_(qpfld), rbfld_(rbfld),
      wfld_(wfld), wpfld_(wpfld), sbfld_(sbfld), efld_(efld),
      quadratureorder_(quadratureorder), ngroup_(ngroup),
      accSign_(accSign)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups_BoundaryCell<TopoDim>::integrate(
        ErrorEstimateBoundaryTrace_VMSD_BR2(fcn, accSign_),
        xfld_,
        (qfld_, qpfld_, wfld_, wpfld_, efld_), (rbfld_, sbfld_),
        quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qpfld_;
  const FieldLift_DG_BoundaryTrace<PhysDim, TopoDim, VectorArrayQ>& rbfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& wfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& wpfld_;
  const FieldLift_DG_BoundaryTrace<PhysDim, TopoDim, VectorArrayQ>& sbfld_;
  Field<PhysDim, TopoDim, Real>& efld_;
  const int* quadratureorder_;
  const int ngroup_;
  const int accSign_;
};

// Factory function

template< class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ >
ErrorEstimateBoundaryTrace_Dispatch_VMSD_BR2_impl<XFieldType, PhysDim, TopoDim, ArrayQ, VectorArrayQ>
ErrorEstimateBoundaryTrace_Dispatch_VMSD_BR2( const XFieldType& xfld,
                                              const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                              const Field<PhysDim, TopoDim, ArrayQ>& qpfld,
                                              const FieldLift_DG_BoundaryTrace<PhysDim, TopoDim, VectorArrayQ>& rbfld,
                                              const Field<PhysDim, TopoDim, ArrayQ>& wfld,
                                              const Field<PhysDim, TopoDim, ArrayQ>& wpfld,
                                              const FieldLift_DG_BoundaryTrace<PhysDim, TopoDim, VectorArrayQ>& sbfld,
                                              Field<PhysDim, TopoDim, Real>& efld,
                                              const int* quadratureorder, int ngroup,
                                              const int accSign = 1)
{
  return ErrorEstimateBoundaryTrace_Dispatch_VMSD_BR2_impl
      <XFieldType, PhysDim, TopoDim,ArrayQ, VectorArrayQ>(xfld,
                                                          qfld, qpfld, rbfld,
                                                          wfld, wpfld, sbfld, efld,
                                                          quadratureorder, ngroup, accSign);
}

#endif


#if 1
//---------------------------------------------------------------------------//
//
// Dispatch class for ghost boundary group. Assumes only one ghost boundary group
//
//---------------------------------------------------------------------------//
template< class XFieldType, class PhysDim, class TopoDim, class ArrayQ>
class ErrorEstimateBoundaryTrace_Ghost_Dispatch_VMSD_impl
{
public:
  ErrorEstimateBoundaryTrace_Ghost_Dispatch_VMSD_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qpfld,
      const Field<PhysDim, TopoDim, ArrayQ>& wfld,
      Field<PhysDim, TopoDim, Real>& efld,
      const int quadratureorder,
      const int accSign)
    : xfld_(xfld),
      qfld_(qfld), qpfld_(qpfld),
      wfld_(wfld), efld_(efld),
      quadratureorder_(quadratureorder),
      accSign_(accSign)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups<TopoDim>::integrate_ghost(
        ErrorEstimateBoundaryTrace_VMSD(fcn, accSign_),
        xfld_,
        (qfld_,qpfld_, wfld_,efld_),
        quadratureorder_);
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qpfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& wfld_;
  Field<PhysDim, TopoDim, Real>& efld_;
  const int quadratureorder_;
  const int ngroup_;
  const int accSign_;
};

// Factory function

template< class XFieldType, class PhysDim, class TopoDim, class ArrayQ >
ErrorEstimateBoundaryTrace_Ghost_Dispatch_VMSD_impl<XFieldType, PhysDim, TopoDim, ArrayQ>
ErrorEstimateBoundaryTrace_Ghost_Dispatch_VMSD( const XFieldType& xfld,
                                              const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                              const Field<PhysDim, TopoDim, ArrayQ>& qpfld,
                                              const Field<PhysDim, TopoDim, ArrayQ>& wfld,
                                              Field<PhysDim, TopoDim, Real>& efld,
                                              const int quadratureorder,
                                              const int accSign = 1)
{
  return ErrorEstimateBoundaryTrace_Ghost_Dispatch_VMSD_impl<XFieldType, PhysDim, TopoDim,ArrayQ>(
      xfld,
      qfld, qpfld, wfld,efld,
      quadratureorder, accSign);
}

#endif

}
#endif //ERRORESTIMATEBOUNDARYTRACE_GALERKIN_H
