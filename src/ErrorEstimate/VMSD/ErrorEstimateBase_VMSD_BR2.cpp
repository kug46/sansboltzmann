// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// Cell integral residual functions

#include "ErrorEstimateBase_VMSD_BR2.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldVolume_DG_Cell.h"

#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"

#include "Field/FieldLine_CG_Cell.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldVolume_CG_Cell.h"

#include "Field/FieldLine_CG_BoundaryTrace.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/FieldVolume_CG_BoundaryTrace.h"

#include "Field/FieldLiftLine_DG_Cell.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/FieldLiftVolume_DG_Cell.h"

namespace SANS
{

template class ErrorEstimateBase_VMSD_BR2<PhysD1, TopoD1, Real>;

template class ErrorEstimateBase_VMSD_BR2<PhysD2, TopoD2, Real>;
template class ErrorEstimateBase_VMSD_BR2<PhysD2, TopoD2, DLA::VectorS<4,Real>>;
template class ErrorEstimateBase_VMSD_BR2<PhysD2, TopoD2, DLA::VectorS<5,Real>>;
template class ErrorEstimateBase_VMSD_BR2<PhysD2, TopoD2, DLA::VectorS<6,Real>>;

template class ErrorEstimateBase_VMSD_BR2<PhysD3, TopoD3, Real>;
template class ErrorEstimateBase_VMSD_BR2<PhysD3, TopoD3, DLA::VectorS<5,Real>>;
template class ErrorEstimateBase_VMSD_BR2<PhysD3, TopoD3, DLA::VectorS<6,Real>>;
template class ErrorEstimateBase_VMSD_BR2<PhysD3, TopoD3, DLA::VectorS<7,Real>>;

} //namespace SANS
