// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ERRORESTIMATEBASE_VMSD_H
#define ERRORESTIMATEBASE_VMSD_H

// Cell integral residual functions

#include <vector> //vector
#include <set> // set

#include "Field/XField.h"
#include "Field/Field.h"
#include "Field/XField_CellToTrace.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Field weighted Estimates for Galerkin
// Combines Cell, interior trace and boundary trace integrals together
//
//----------------------------------------------------------------------------//
//

template< class PhysDim, class TopoDim, class ArrayQ >
class ErrorEstimateBase_VMSD
{
public:

  // Constructor
  template<class... BCArgs>
  ErrorEstimateBase_VMSD( const XField<PhysDim, TopoDim>& xfld,
                              const Field<PhysDim, TopoDim, ArrayQ>& qfld, // q
                              const Field<PhysDim, TopoDim, ArrayQ>& qpfld, // q
                              const Field<PhysDim, TopoDim, ArrayQ>& lgfld, // lg
                              const Field<PhysDim, TopoDim, ArrayQ>& wfld, // w
                              const Field<PhysDim, TopoDim, ArrayQ>& wpfld, // w
                              const Field<PhysDim, TopoDim, ArrayQ>& mufld, // mu
                              std::shared_ptr<Field<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                              const std::vector<int>& CellGroups,
                              const std::vector<int>& active_BGroup_list)
                              : xfld_(xfld),
                                xfldCellToTrace_(xfld_),
                                active_BGroup_list_(active_BGroup_list),
                                efld_nodal_ (xfld_, 1, BasisFunctionCategory_Lagrange),
                                epfld_(xfld_, 0, BasisFunctionCategory_Legendre),
                                qfld_(qfld), qpfld_(qpfld), lgfld_(lgfld), wfld_(wfld), wpfld_(wpfld), mufld_(mufld),
                                pLiftedQuantityfld_(pLiftedQuantityfld),
                                cellGroups_(CellGroups)
  {
    init();
  }


  // Constructor
  template<class... BCArgs>
  ErrorEstimateBase_VMSD( const XField<PhysDim, TopoDim>& xfld,
                              const Field<PhysDim, TopoDim, ArrayQ>& qfld, // q
                              const Field<PhysDim, TopoDim, ArrayQ>& qpfld, // q
                              const Field<PhysDim, TopoDim, ArrayQ>& lgfld, // lg
                              const Field<PhysDim, TopoDim, ArrayQ>& wfld, // w
                              const Field<PhysDim, TopoDim, ArrayQ>& wpfld, // w
                              const Field<PhysDim, TopoDim, ArrayQ>& mufld, // mu
                              std::unique_ptr<Field_DG_Cell<PhysDim,TopoDim,Real>>& up_efld_DG_nodal, // efld EXTERNALLY created
                              std::shared_ptr<Field<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                              const std::vector<int>& CellGroups,
                              const std::vector<int>& active_BGroup_list)
                              : xfld_(xfld),
                                xfldCellToTrace_(xfld_),
                                active_BGroup_list_(active_BGroup_list),
                                efld_nodal_ (xfld_, 1, BasisFunctionCategory_Lagrange),
                                epfld_(xfld_, 0, BasisFunctionCategory_Legendre),
                                up_efld_DG_nodal_(std::move(up_efld_DG_nodal)), // Take control of the pointer. The heap memory is now owned
                                qfld_(qfld), qpfld_(qpfld), lgfld_(lgfld), wfld_(wfld), wpfld_(wpfld), mufld_(mufld),
                                pLiftedQuantityfld_(pLiftedQuantityfld),
                                cellGroups_(CellGroups)
  {
    init();
  }

  //----------------------------------------------------------------------------//
  // Check that the primal and adjoint fields are all reasonable
  void init();
  void check( );

  void getErrorEstimate( Real& estimate, const std::vector<int>& cellGroups );
  void getErrorEstimate( Real& estimate );

  void getLocalSolveErrorEstimate( std::vector<Real>& estimate );

  void getErrorIndicator( Real& estimate, const std::vector<int>& cellGroups );
  void getErrorIndicator( Real& estimate );

  void getLocalSolveErrorIndicator( std::vector<Real>& estimate );

  // accessor functions
  const XField<PhysDim, TopoDim>& getXField() const { return xfld_; }

  const Field<PhysDim,TopoDim,Real>& getESField()
  {
    if (eSfldCalc_ == false)
      calculateSumEstimates();

    return *up_eSfld_nodal_;
  }
  const Field<PhysDim,TopoDim,Real>& getIField()
  {
    if (!ifldCalc_)
      calculateIndicators();
    return *up_ifld_nodal_;
  }
  const Field<PhysDim,TopoDim,Real>& getEField() const { return efld_nodal_; }

  // fragile interface for edge solve - accumulates only group 0
  void fillVector(std::vector<std::vector<Real>>& accVector,
                  const Field<PhysDim,TopoDim,Real>& accFld, // DG SPECIFIC
                  const std::vector<int>& cellGroups );

  // fragile interface for Savithru
  void fillEArray(std::vector<std::vector<Real>>& estimates );

  void outputFields( const std::string& filename ) const;

  // A reference to the data in the internally controlled unique_ptr
  const Field_DG_Cell<PhysDim,TopoDim,Real>& efld_DG_nodal() const
  {
    return *up_efld_DG_nodal_;
  }

protected:
  const XField<PhysDim, TopoDim>& xfld_;
  const XField_CellToTrace<PhysDim, TopoDim> xfldCellToTrace_;

  std::vector<int> active_BGroup_list_; //list of BoundaryTraceGroups which have Lagrange multiplier DOFs (mitLG)

  // use unique_ptr so that they only get created if they're actually needed. Avoids some memory allocation and deallocation

  Field_CG_Cell<PhysDim,TopoDim,Real> efld_nodal_;    // q field estimates

  // defining a unique_ptr here so we can optionally take control of memory declared externally
  // this field is used to pass in precomputed error estimates in cell group 1 - which we are assuming are locked
  std::unique_ptr<Field<PhysDim,TopoDim,Real>> up_ifld_nodal_;    // indicator field for q
  std::unique_ptr<Field<PhysDim,TopoDim,Real>> up_eSfld_nodal_;   // estimate sum field for q

  Field_DG_Cell<PhysDim,TopoDim,Real> epfld_;    // qp field estimates
  std::unique_ptr<Field_DG_Cell<PhysDim,TopoDim,Real>> up_efld_DG_nodal_; // q field estimates - can be computed externally
  std::unique_ptr<Field<PhysDim,TopoDim,Real>> up_ipfld_;    // indicator field for qp
  std::unique_ptr<Field<PhysDim,TopoDim,Real>> up_epSfld_;   // estimate sum field for qp

  std::unique_ptr<Field<PhysDim,TopoDim,Real>> up_eBfld_nodal_;


  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;  // q
  const Field<PhysDim, TopoDim, ArrayQ>& qpfld_; // qp
  const Field<PhysDim, TopoDim, ArrayQ>& lgfld_; // lg
  const Field<PhysDim, TopoDim, ArrayQ>& wfld_;  // w
  const Field<PhysDim, TopoDim, ArrayQ>& wpfld_; // wp
  const Field<PhysDim, TopoDim, ArrayQ>& mufld_; // mu

  std::shared_ptr<Field<PhysDim, TopoDim, Real>> pLiftedQuantityfld_; //pointer to the lifted-quantity field (shock-capturing)

  std::vector<int> cellGroups_;

  bool ifldCalc_;
  bool eSfldCalc_;

  //----------------------------------------------------------------------------//
  // Function for calculating the sum estimates
  void calculateSumEstimates();

  //----------------------------------------------------------------------------//
  // Function for calculating the indicator estimates(
  void calculateIndicators();
};

} //namespace SANS

#endif  // ERRORESTIMATE_VMSD_H
