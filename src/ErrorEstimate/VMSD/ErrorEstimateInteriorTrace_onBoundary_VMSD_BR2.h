// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ERRORESTIMATE_INTERIORTRACE_ONBOUNDARY_VMSD_H
#define ERRORESTIMATE_INTERIORTRACE_ONBOUNDARY_VMSD_H

// boundary-trace integral residual functions

#include <memory>     // std::unique_ptr

#include "Quadrature/Quadrature.h"
#include "Topology/ElementTopology.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"

#include "Field/FieldData/FieldDataReal_Cell.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"
#include "Discretization/DG/DiscretizationDGBR2.h"

 #include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  Galerkin boundary-trace integral
//

template<class IntegrandBoundaryTrace>
class ErrorEstimate_InteriorTrace_onBoundary_VMSD_BR2_impl :
    public GroupIntegralBoundaryTraceType< ErrorEstimate_InteriorTrace_onBoundary_VMSD_BR2_impl<IntegrandBoundaryTrace> >
{
public:
  typedef typename IntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename IntegrandBoundaryTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandBoundaryTrace::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename DLA::VectorS<PhysDim::D, Real> VecReal;

  // Save off the boundary trace integrand and the residual vectors
  // accSign is the accumulation sign for the residual, this is needed to allow removal of weak form boundary fluxes after the fact
  explicit ErrorEstimate_InteriorTrace_onBoundary_VMSD_BR2_impl(
      const IntegrandBoundaryTrace& fcn,
      const std::map<int,std::vector<int>>& cellBTraceGroups,
      const int& cellgroup, const int& cellelem,
      DLA::VectorD<Real>& estPDEElemCell,
      DLA::VectorD<Real>& estPDEpElemCell) :
          fcn_(fcn),
          cellBTraceGroups_(cellBTraceGroups),
          cellgroup_(cellgroup), cellelem_(cellelem),
          estPDEElemCell_(estPDEElemCell),
          estPDEpElemCell_(estPDEpElemCell)
{
    for (std::map<int,std::vector<int>>::const_iterator it = cellBTraceGroups_.begin(); it != cellBTraceGroups_.end(); ++it)
      traceGroupIndices_.push_back( it->first );
}

  std::size_t nBoundaryGroups() const { return traceGroupIndices_.size(); }
  std::size_t boundaryGroup(const int n) const { return traceGroupIndices_[n]; }

//----------------------------------------------------------------------------//
// does some checks on the inputs to make sure they all make sense
  template < class TopoDim >
  void check( const typename MakeTuple<FieldTuple,
              Field<PhysDim, TopoDim, ArrayQ>, // q
              Field<PhysDim, TopoDim, ArrayQ>, // qp
              FieldLift<PhysDim,TopoDim,VectorArrayQ>,
              Field<PhysDim, TopoDim, ArrayQ>, // w
              Field<PhysDim, TopoDim, ArrayQ>, // wp
              FieldLift<PhysDim,TopoDim,VectorArrayQ>,
              Field<PhysDim, TopoDim, Real>, // e
              Field<PhysDim, TopoDim, Real>>::type& flds // ep
              ) // e
  {
    const Field<PhysDim, TopoDim, ArrayQ>&       qfld  = get<0>(flds);
    const Field<PhysDim, TopoDim, ArrayQ>&       qpfld  = get<1>(flds);
    //r
    const Field<PhysDim, TopoDim, ArrayQ>&       wfld  = get<3>(flds);
    const Field<PhysDim, TopoDim, ArrayQ>&       wpfld  = get<4>(flds);
    //s
    const Field<PhysDim, TopoDim, Real>&         efld  = get<6>(flds);
    const Field<PhysDim, TopoDim, Real>&         epfld = get<7>(flds);

    SANS_ASSERT( qfld.nElem() == qpfld.nElem() ); // same number of elems
    SANS_ASSERT( qfld.nElem() == wfld.nElem() ); // same number of elems
    SANS_ASSERT( qfld.nElem() == efld.nElem() ); // same number of elems
    SANS_ASSERT( qfld.nElem() == wpfld.nElem() ); // same number of elems
    SANS_ASSERT( qfld.nElem() == epfld.nElem() ); // same number of elems
    SANS_ASSERT( &qfld.getXField() == &wfld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &efld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &wpfld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &epfld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &qpfld.getXField() ); // check both were made off the same grid
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class Topology, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobalL,
             const typename XFieldType::template  FieldCellGroupType<Topology>& xfldCell,
             const typename MakeTuple< FieldTuple,
                                       Field<PhysDim,TopoDim,ArrayQ>,
                                       Field<PhysDim,TopoDim,ArrayQ>,
                                       FieldLift<PhysDim,TopoDim,VectorArrayQ>,
                                       Field<PhysDim,TopoDim,ArrayQ>,
                                       Field<PhysDim,TopoDim,ArrayQ>,
                                       FieldLift<PhysDim,TopoDim,VectorArrayQ>,
                                       Field<PhysDim,TopoDim,Real>,
                                       Field<PhysDim,TopoDim,Real>>::type::template FieldCellGroupType<Topology>& fldsCell,
             const int traceGroupGlobal,
             const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
             int quadratureorder )
  {
    typedef typename XFieldType                             ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>        ::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename FieldLift<PhysDim, TopoDim, VectorArrayQ >     ::template FieldCellGroupType<Topology> RFieldCellGroupType;
    typedef typename Field<PhysDim, TopoDim, Real  >        ::template FieldCellGroupType<Topology> EFieldCellGroupType;

    typedef typename XFieldCellGroupType ::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType ::template ElementType<> ElementQFieldClass;
    typedef typename RFieldCellGroupType ::template ElementType<> ElementRFieldClass;
    typedef typename EFieldCellGroupType ::template ElementType<> ElementEFieldClass;

    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>  XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<>             ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    // separating off Q, W and casting away the const on Efield
    const QFieldCellGroupType& qfldCell = get<0>(fldsCell);
    const QFieldCellGroupType& qpfldCell = get<1>(fldsCell);
    const RFieldCellGroupType& rfldCell = get<2>(fldsCell);

    const QFieldCellGroupType& wfldCell = get<3>(fldsCell);
    const QFieldCellGroupType& wpfldCell = get<4>(fldsCell);
    const RFieldCellGroupType& sfldCell = get<5>(fldsCell);

    EFieldCellGroupType& efldCell = const_cast<EFieldCellGroupType&>(get<6>(fldsCell));
    EFieldCellGroupType& epfldCell = const_cast<EFieldCellGroupType&>(get<7>(fldsCell));

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );
    ElementQFieldClass qpfldElem( qpfldCell.basis() );
    ElementRFieldClass rfldElem( rfldCell.basis() );

    ElementQFieldClass wfldElem( wfldCell.basis() );
    ElementQFieldClass wpfldElem( wpfldCell.basis() );
    ElementRFieldClass sfldElem( sfldCell.basis() );

    ElementEFieldClass efldElem( efldCell.basis() );
    ElementEFieldClass epfldElem( epfldCell.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    //Checking it's a p0 or p1 field, assumed for now given they're the only partitions of unity
    SANS_ASSERT( efldElem.order() == 0 || efldElem.order() == 1 );

    const int nIntegrand = efldElem.nDOF();
//    const int npIntegrand = epfldElem.nDOF();

    // trace element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, Real > integral(quadratureorder, nIntegrand);
//    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, Real > integralp(quadratureorder, npIntegrand);

    DLA::VectorD<Real> estPDEElem(nIntegrand);
//    DLA::VectorD<Real> estPDEpElem(npIntegrand);

    const std::vector<int>& traceElemList = cellBTraceGroups_.at(traceGroupGlobal);

    // loop over elements within group
    for (int i = 0; i < (int) traceElemList.size(); i++)
    {
      const int elem = traceElemList[i];

      const int elemL = xfldTrace.getElementLeft( elem );

      if (xfldTrace.getGroupLeft() == cellgroup_ && elemL == cellelem_)
      {
        const int elemCell = xfldTrace.getElementLeft( elem );
        CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

        // copy global grid/solution DOFs to element
        xfldCell.getElement( xfldElem, elemCell );
        qfldCell.getElement( qfldElem, elemCell );
        qpfldCell.getElement( qpfldElem, elemCell );
        rfldCell.getElement( rfldElem, elemCell, canonicalTraceL.trace );

        wfldCell.getElement( wfldElem, elemCell );
        wpfldCell.getElement( wpfldElem, elemCell );
        sfldCell.getElement( sfldElem, elemCell, canonicalTraceL.trace );

        efldCell.getElement( efldElem, elemCell );
        epfldCell.getElement( epfldElem, elemCell );

        xfldTrace.getElement( xfldElemTrace, elem );

        for (int k = 0; k < nIntegrand; k++) estPDEElem[k] = 0;
//        for (int k = 0; k < npIntegrand; k++) estPDEpElem[k] = 0;

        integral( fcn_.integrand(xfldElemTrace, canonicalTraceL, +1, xfldElem,
                                 qfldElem, qpfldElem, rfldElem,
                                 wfldElem, wpfldElem, sfldElem, efldElem),
            get<-1>(xfldElemTrace), estPDEElem.data(), nIntegrand );

        // PDE
        estPDEElemCell_ += estPDEElem; // so as not to overwrite
//        estPDEpElemCell_ += estPDEpElem; // so as not to overwrite

      }
    }
  }

protected:
  const IntegrandBoundaryTrace& fcn_;

  const std::map<int,std::vector<int>>& cellBTraceGroups_;
  const int& cellgroup_;
  const int& cellelem_;
  DLA::VectorD<Real>& estPDEElemCell_;
  DLA::VectorD<Real>& estPDEpElemCell_;

  std::vector<int> traceGroupIndices_;
};

// Factory function

template<class IntegrandBoundaryTrace>
ErrorEstimate_InteriorTrace_onBoundary_VMSD_BR2_impl<IntegrandBoundaryTrace>
ErrorEstimate_InteriorTrace_onBoundary_VMSD_BR2( const IntegrandInteriorTraceType<IntegrandBoundaryTrace>& fcn,
    const std::map<int,std::vector<int>>& cellBTraceGroups,
    const int& cellgroup, const int& cellelem,
    DLA::VectorD<Real>& estPDEElemCell,
    DLA::VectorD<Real>& estPDEpElemCell)
{
  return ErrorEstimate_InteriorTrace_onBoundary_VMSD_BR2_impl<IntegrandBoundaryTrace>(
      fcn.cast(), cellBTraceGroups, cellgroup, cellelem, estPDEElemCell, estPDEpElemCell);
}


}

#endif  // ERRORESTIMATE_INTERIORTRACE_ONBOUNDARY_VMSD_H
