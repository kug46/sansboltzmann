// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ERRORESTIMATE_VMSD_LINEAR_H
#define ERRORESTIMATE_VMSD_LINEAR_H

// Cell integral residual functions

#include <memory> // shared_ptr
#include <vector>

#include "tools/Tuple.h"

#include "pde/BCParameters.h"

#include "Field/XField.h"
#include "Field/Field.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"
#include "Field/Tuple/FieldTuple.h"

#include "Field/tools/for_each_CellGroup.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

#include "Discretization/QuadratureOrder.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"

#include "Discretization/VMSD/IntegrateBoundaryTrace_Dispatch_VMSD.h"

#include "Discretization/VMSD/IntegrandBoundaryTrace_Flux_mitState_VMSD.h"
#include "Discretization/VMSD/IntegrandBoundaryTrace_Dirichlet_mitLG_VMSD.h"
#include "Discretization/VMSD/IntegrandBoundaryTrace_LinearScalar_sansLG_VMSD.h"
#include "Discretization/VMSD/IntegrandBoundaryTrace_LinearScalar_mitLG_VMSD.h"
#include "Discretization/VMSD/IntegrandBoundaryTrace_None_VMSD.h"
#include "Discretization/VMSD/Discretization_VMSD.h"
#include "Discretization/VMSD/FieldBundle_VMSD.h"
#include "Discretization/VMSD/IntegrandCell_VMSD.h"
#include "Discretization/VMSD/IntegrandTrace_VMSD.h"
#include "Discretization/VMSD/IntegrandCell_VMSD_NL.h"
#include "Discretization/VMSD/IntegrandTrace_VMSD_NL.h"
#include "Discretization/VMSD/ComputePerimeter.h"

// Estimation classes
#include "ErrorEstimate/Galerkin/ErrorEstimateInteriorTrace_SIP_Galerkin.h"
#include "Discretization/Galerkin/IntegrandInteriorTrace_SIP_Galerkin.h"

// Main Integrands
#include "Discretization/Galerkin/Stabilization_Nitsche.h"
#include "Discretization/Galerkin/ExtractCGLocalBoundaries.h"

#include "ErrorEstimate/VMSD/ErrorEstimateBoundaryTrace_Dispatch_VMSD.h"
#include "ErrorEstimate/VMSD/ErrorEstimateBoundaryTrace_VMSD.h"
#include "ErrorEstimateBase_VMSD.h"
#include "ErrorEstimateCell_VMSD.h"

#include "tools/Tuple.h"
#include "tools/make_unique.h"
#include "tools/linspace.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Field weighted Estimates for Galerkin
// Combines Cell, interior trace and boundary trace integrals together
//----------------------------------------------------------------------------//

template< class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class ParamFieldType >
class ErrorEstimate_VMSD_Linear :
    public ErrorEstimateBase_VMSD<typename NDPDEClass::PhysDim, typename ParamFieldType::TopoDim, typename NDPDEClass::template ArrayQ<Real>>
{
public:
  typedef typename NDPDEClass::PhysDim PhysDim;
  typedef typename ParamFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef ErrorEstimateBase_VMSD<PhysDim, TopoDim, ArrayQ> BaseType;

  typedef BCParameters<BCVector> BCParams;
  typedef IntegrandInteriorTrace_SIP_Galerkin<NDPDEClass> IntegrandTraceSIPClass;

  typedef IntegrandCell_VMSD<NDPDEClass> IntegrandCellClass;
  typedef IntegrandTrace_VMSD<NDPDEClass> IntegrandTraceClass;

  typedef IntegrateBoundaryTrace_Dispatch_VMSD<NDPDEClass, BCNDConvert, BCVector, VMSD> IntegrateBoundaryTrace_Dispatch;
  //TODO: disc tag to be updated

  typedef FieldBundleBase_VMSD<PhysDim,TopoDim,ArrayQ> FieldBundle;

  std::vector<int> BCAccumulate(const std::map< std::string, std::vector<int> >& BCBoundaryGroups )
  {
    std::vector<int> BoundaryGroups;
    for (auto it = BCBoundaryGroups.begin(); it != BCBoundaryGroups.end(); ++it) // loop over map
      for (auto itt = (it->second).begin(); itt != (it->second).end(); ++itt) // loop over vector at current key
        BoundaryGroups.push_back(*itt);
    return BoundaryGroups;
  }

  // For viewing a forcing function as a solution function. Can then project onto a field.
  template< class NDPDEClass_ >
  class ForcingWrapper
  {
  public:
    typedef typename NDPDEClass_::PhysDim PhysDim;

    template <class Z>
    using ArrayQ = typename NDPDEClass_::template ArrayQ<Z>;

    ForcingWrapper( const NDPDEClass_& pde ) : pde_(pde) {}

    void operator() ( const typename NDPDEClass_::VectorX& x, ArrayQ<Real>& forcing) const
    {
      pde_.forcingFunction( x, forcing );
    }

    ArrayQ<Real> operator() ( const typename NDPDEClass_::VectorX& x ) const
    {
      ArrayQ<Real> forcing=0;

      pde_.forcingFunction( x, forcing );

      return forcing;
    }

    const NDPDEClass_& pde_;
  };

  // Constructor
  template<class... BCArgs>
  ErrorEstimate_VMSD_Linear( const ParamFieldType& paramfld,
                          const Field<PhysDim, TopoDim, ArrayQ>& qfld, // q
                          const Field<PhysDim, TopoDim, ArrayQ>& qpfld, // q
                          const Field<PhysDim, TopoDim, ArrayQ>& lgfld, // lg
                          const Field<PhysDim, TopoDim, ArrayQ>& wfld, // w
                          const Field<PhysDim, TopoDim, ArrayQ>& wpfld, // w
                          const Field<PhysDim, TopoDim, ArrayQ>& mufld, // mu
                          std::unique_ptr<Field_DG_Cell<PhysDim,TopoDim,Real>>& up_efld_DG_nodal, // reference so base can take control
                          std::shared_ptr<Field<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                          const NDPDEClass& pde,
                          const DiscretizationVMSD& stab,
                          const QuadratureOrder& quadratureOrder,
                          const std::vector<int>& CellGroups,
                          const std::vector<int>& InteriorTraceGroups,
                          PyDict& BCList,
                          const std::map< std::string, std::vector<int> >& BCBoundaryGroups, BCArgs&... args)
                            : BaseType(get<-1>(paramfld), qfld, qpfld, lgfld, wfld, wpfld, mufld, up_efld_DG_nodal,
                                       pLiftedQuantityfld, CellGroups, BCParams::getLGBoundaryGroups(BCList, BCBoundaryGroups)),
                              paramfld_(paramfld),
                              pde_(pde),
                              quadratureOrder_(quadratureOrder),
                              fcnCell_(pde, CellGroups),
                              fcnITrace_(pde, stab, InteriorTraceGroups),
                              interiorTraceGroups0to1_(findInteriorTraceGroup(paramfld.getXField(),0,1)),
                              fcnSIP_(pde, stab, interiorTraceGroups0to1_),
                              boundaryTraceGroups_(0),
                              fcnBTrace_(pde, stab, boundaryTraceGroups_),
                              BCs_(BCParams::template createBCs<BCNDConvert>(pde, BCList, args...)),
                              dispatchBC_(pde, BCList, BCs_, BCBoundaryGroups, stab),
                              cellPerimeter_(qfld)
  {
    if ( pde_.hasSourceLiftedQuantity() ) SANS_ASSERT_MSG(this->pLiftedQuantityfld_, "The lifted quantity field has not been initialized!");

    for (int i =0; i<xfld_.getXField().nBoundaryTraceGroups(); i++)
      boundaryTraceGroups_.push_back(i);

    //COMPUTE CELL PERIMETERS FOR SIP
    IntegrateInteriorTraceGroups<TopoDim>::integrate(
        ComputePerimeter_InteriorTrace<PhysDim,ArrayQ>( cellPerimeter_, InteriorTraceGroups),
                                                     xfld_, qfld_,
                                                     quadratureOrder_.interiorTraceOrders.data(),
                                                     quadratureOrder_.interiorTraceOrders.size() );

    //COMPUTE CELL PERIMETERS FOR SIP
    IntegrateBoundaryTraceGroups<TopoDim>::integrate(
        ComputePerimeter_BoundaryTrace<PhysDim, ArrayQ>( cellPerimeter_, boundaryTraceGroups_),
                                                     xfld_, qfld_,
                                                     quadratureOrder_.boundaryTraceOrders.data(),
                                                     quadratureOrder_.boundaryTraceOrders.size() );

    estimate();
  }


  // Constructor - without pre allocated field, sends to the base class without the up_efld_DG_nodal
  template<class... BCArgs>
  ErrorEstimate_VMSD_Linear( const ParamFieldType& paramfld,
                          const Field<PhysDim, TopoDim, ArrayQ>& qfld, // q
                          const Field<PhysDim, TopoDim, ArrayQ>& qpfld, // q
                          const Field<PhysDim, TopoDim, ArrayQ>& lgfld, // lg
                          const Field<PhysDim, TopoDim, ArrayQ>& wfld, // w
                          const Field<PhysDim, TopoDim, ArrayQ>& wpfld, // w
                          const Field<PhysDim, TopoDim, ArrayQ>& mufld, // mu
                          std::shared_ptr<Field<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                          const NDPDEClass& pde,
                          const DiscretizationVMSD& stab,
                          const QuadratureOrder& quadratureOrder,
                          const std::vector<int>& CellGroups,
                          const std::vector<int>& InteriorTraceGroups,
                          PyDict& BCList,
                          const std::map< std::string, std::vector<int> >& BCBoundaryGroups, BCArgs&... args)
                            : BaseType(get<-1>(paramfld), qfld, qpfld, lgfld, wfld, wpfld, mufld,
                                       pLiftedQuantityfld, CellGroups, BCParams::getLGBoundaryGroups(BCList, BCBoundaryGroups)),
                              paramfld_(paramfld),
                              pde_(pde),
                              quadratureOrder_(quadratureOrder),
                              fcnCell_(pde, CellGroups),
                              fcnITrace_(pde, stab, InteriorTraceGroups),
                              fcnSIP_(pde, stab, InteriorTraceGroups),
                              boundaryTraceGroups_(0),
                              fcnBTrace_(pde, stab, boundaryTraceGroups_),
                              BCs_(BCParams::template createBCs<BCNDConvert>(pde, BCList, args...)),
                              dispatchBC_(pde, BCList, BCs_, BCBoundaryGroups, stab),
                              cellPerimeter_(qfld)
  {
    if ( pde_.hasSourceLiftedQuantity() ) SANS_ASSERT_MSG(this->pLiftedQuantityfld_, "The lifted quantity field has not been initialized!");

    for (int i =0; i<xfld_.getXField().nBoundaryTraceGroups(); i++)
      boundaryTraceGroups_.push_back(i);

    //COMPUTE CELL PERIMETERS FOR SIP
    IntegrateInteriorTraceGroups<TopoDim>::integrate(
        ComputePerimeter_InteriorTrace<PhysDim,ArrayQ>( cellPerimeter_, InteriorTraceGroups),
                                                     xfld_, qfld_,
                                                     quadratureOrder_.interiorTraceOrders.data(),
                                                     quadratureOrder_.interiorTraceOrders.size() );

    //COMPUTE CELL PERIMETERS FOR SIP
    IntegrateBoundaryTraceGroups<TopoDim>::integrate(
        ComputePerimeter_BoundaryTrace<PhysDim, ArrayQ>( cellPerimeter_, boundaryTraceGroups_),
                                                     xfld_, qfld_,
                                                     quadratureOrder_.boundaryTraceOrders.data(),
                                                     quadratureOrder_.boundaryTraceOrders.size() );

    estimate();
  }

  template<class... BCArgs>
  ErrorEstimate_VMSD_Linear( const ParamFieldType& paramfld,
                          const Field<PhysDim, TopoDim, ArrayQ>& qfld, // q
                          const Field<PhysDim, TopoDim, ArrayQ>& qpfld, // q
                          const Field<PhysDim, TopoDim, ArrayQ>& lgfld, // lg
                          const Field<PhysDim, TopoDim, ArrayQ>& wfld, // w
                          const Field<PhysDim, TopoDim, ArrayQ>& wpfld, // w
                          const Field<PhysDim, TopoDim, ArrayQ>& mufld, // mu
                          const NDPDEClass& pde,
                          const DiscretizationVMSD& stab,
                          const QuadratureOrder& quadratureOrder,
                          const std::vector<int>& CellGroups,
                          const std::vector<int>& InteriorTraceGroups,
                          PyDict& BCList,
                          const std::map< std::string, std::vector<int> >& BCBoundaryGroups, BCArgs&... args)
: ErrorEstimate_VMSD_Linear( paramfld, qfld, lgfld, wfld, mufld, {},
                          pde, stab, quadratureOrder, CellGroups, InteriorTraceGroups, BCList, BCBoundaryGroups, args...) {}

  // Alternative Agnostic Constructor
  template<class... BCArgs>
  ErrorEstimate_VMSD_Linear( const ParamFieldType& paramfld,
                          const FieldBundle& primal,   // (q, lg)
                          const FieldBundle& adjoint,  // (w, mu)
                          std::shared_ptr<Field<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                          const NDPDEClass& pde,
                          const DiscretizationVMSD& stab,
                          const QuadratureOrder& quadratureOrder,
                          const std::vector<int>& CellGroups,
                          const std::vector<int>& InteriorTraceGroups,
                          PyDict& BCList,
                          const std::map< std::string, std::vector<int> >& BCBoundaryGroups, BCArgs&... args)
: ErrorEstimate_VMSD_Linear( paramfld, primal.qfld, primal.qpfld, primal.lgfld, adjoint.qfld, adjoint.qpfld, adjoint.lgfld, pLiftedQuantityfld,
                      pde, stab, quadratureOrder, CellGroups, InteriorTraceGroups, BCList, BCBoundaryGroups, args...)
  {}


  // Alternative Agnostic Constructor
  template<class... BCArgs>
  ErrorEstimate_VMSD_Linear( const ParamFieldType& paramfld,
                          const FieldBundle& primal,   // (q, lg)
                          const FieldBundle& adjoint,  // (w, mu)
                          std::unique_ptr<Field_DG_Cell<PhysDim,TopoDim,Real>>& up_efld_DG_nodal, // reference so base can take control
                          std::shared_ptr<Field<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                          const NDPDEClass& pde,
                          const DiscretizationVMSD& stab,
                          const QuadratureOrder& quadratureOrder,
                          const std::vector<int>& CellGroups,
                          const std::vector<int>& InteriorTraceGroups,
                          PyDict& BCList,
                          const std::map< std::string, std::vector<int> >& BCBoundaryGroups, BCArgs&... args)
: ErrorEstimate_VMSD_Linear( paramfld, primal.qfld, primal.qpfld, primal.lgfld, adjoint.qfld, adjoint.qpfld, adjoint.lgfld,
                      up_efld_DG_nodal, pLiftedQuantityfld, pde, stab, quadratureOrder, CellGroups, InteriorTraceGroups,
                      BCList, BCBoundaryGroups, args...)
  {}

  std::size_t nCellGroups() const { return fcnCell_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcnCell_.cellGroup(n); }


  void getLocalSolveErrorIndicator( std::vector<Real>& estimate )
  {
#if !defined(WHOLEPATCH) && !defined(INNERPATCH)
    estimateLocal();
#endif
    BaseType::getLocalSolveErrorIndicator(estimate);
  }

  void getLocalSolveErrorEstimate( std::vector<Real>& estimate ) { getLocalSolveErrorIndicator(estimate); }

protected:
  using BaseType::xfld_;
  using BaseType::active_BGroup_list_;

  using BaseType::xfldCellToTrace_;

  using BaseType::efld_nodal_;    // q field estimates
  using BaseType::up_efld_DG_nodal_; // DG nodal field representation -- can come with precomputed data
  using BaseType::up_ifld_nodal_;    // indicator field
  using BaseType::up_eSfld_nodal_;   // estimate sum field

  using BaseType::epfld_;    // qp field estimates
  using BaseType::up_ipfld_;    // indicator field
  using BaseType::up_epSfld_;   // estimate sum field
  using BaseType::up_eBfld_nodal_;

  using BaseType::qfld_; // q
  using BaseType::qpfld_; // q
  using BaseType::lgfld_; // lg
  using BaseType::wfld_; // w
  using BaseType::wpfld_; // w
  using BaseType::mufld_; // mu
  using BaseType::pLiftedQuantityfld_; //pointer to lifted quantity field

  using BaseType::cellGroups_;

  const ParamFieldType& paramfld_;

  const NDPDEClass& pde_;
  const QuadratureOrder quadratureOrder_;

  IntegrandCellClass fcnCell_;
  const IntegrandTraceClass fcnITrace_;
  std::vector<int> interiorTraceGroups0to1_;
  IntegrandTraceSIPClass fcnSIP_;

  std::vector<int> boundaryTraceGroups_;
  const IntegrandTraceClass fcnBTrace_;

  std::map< std::string, std::shared_ptr<BCBase> > BCs_;
  IntegrateBoundaryTrace_Dispatch dispatchBC_;
  FieldDataReal_Cell cellPerimeter_;

  //----------------------------------------------------------------------------//
  // Estimate function that populates the efld_ and eBfld_, called in constructor
  void
  estimate( )
  {

    if ( up_efld_DG_nodal_ == nullptr ) // if the field wasn't declared then we need to declare it
    {
      // std::cout << "creating group" << std::endl;
      // This is what will happen for general
      up_efld_DG_nodal_ = SANS::make_unique<Field_DG_Cell<PhysDim,TopoDim,Real>>(xfld_, 1, BasisFunctionCategory_Lagrange );
      *up_efld_DG_nodal_ = 0; // zero the data
    }
    else // if the field was declared - that means there was precomputed data.
    {
      // std::cout << "zeroing group" << std::endl;
      // zero out the data in cell group 0 so that it can be filled below
      for_each_CellGroup<TopoDim>::apply( ZeroField<PhysDim,Real>(fcnCell_.cellGroups()), *up_efld_DG_nodal_ );
    }

    // Cell and Interior Estimates
    IntegrateCellGroups<TopoDim>::integrate( ErrorEstimateCell_VMSD( fcnCell_, fcnITrace_, fcnBTrace_, xfldCellToTrace_,
                                                                    paramfld_, qfld_, qpfld_, wfld_, wpfld_,
                                                                    cellPerimeter_,
                                                                    quadratureOrder_,
                                                                    *up_efld_DG_nodal_, epfld_),
                                            paramfld_, (qfld_, qpfld_, wfld_, wpfld_, *up_efld_DG_nodal_, epfld_),
                                            quadratureOrder_.cellOrders.data(), quadratureOrder_.cellOrders.size());


    // Boundary Trace Estimates

    dispatchBC_.dispatch_VMSD(
        ErrorEstimateBoundaryTrace_Dispatch_VMSD( paramfld_,
                                                      qfld_, qpfld_, wfld_, *up_efld_DG_nodal_, cellPerimeter_,
                                                      quadratureOrder_.boundaryTraceOrders.data(),
                                                      quadratureOrder_.boundaryTraceOrders.size() )
                         );
    // need to sync here because the efld will then be populated below by accumulating
    up_efld_DG_nodal_->syncDOFs_MPI_noCache();

    // map from the DG dofs to CG dofs
    std::map<int,int> DG_to_CG_DOFMap;

    // fill the map for all dofs that are in the DG field - transfer for all cell groups!
    for_each_CellGroup<TopoDim>::apply(
      PairFieldDOFs<PhysDim,Real>(DG_to_CG_DOFMap,linspace(0,xfld_.nCellGroups()-1)), (*up_efld_DG_nodal_, efld_nodal_) );

    // loop over the DG dofs and accumulate the data into the CG field
    for (const auto& keyVal : DG_to_CG_DOFMap )
      efld_nodal_.DOF(keyVal.second) += up_efld_DG_nodal_->DOF(keyVal.first);

    // We don't need to sync up the efld_nodal, as the DG sync was already done.

  }


  void estimateLocal()
  {
    //add SIP type stabilization
    IntegrateInteriorTraceGroups<TopoDim>::integrate(ErrorEstimateInteriorTrace_SIP_Galerkin(fcnSIP_),
                                                     paramfld_, (qfld_, wfld_, efld_nodal_),
                                                     quadratureOrder_.interiorTraceOrders.data(),
                                                     quadratureOrder_.interiorTraceOrders.size() );

  }

};

} //namespace SANS

#endif  // ERRORESTIMATE_VMSD_LINEAR_H
