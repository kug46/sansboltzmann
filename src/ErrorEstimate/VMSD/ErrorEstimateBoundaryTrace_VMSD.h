// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ERRORESTIMATEBOUNDARYTRACE_VMSD_H
#define ERRORESTIMATEBOUNDARYTRACE_VMSD_H

// boundary-trace integral residual functions

#include <memory>     // std::unique_ptr

#include "Topology/ElementTopology.h"
#include "Field/Field.h"
#include "Field/FieldData/FieldDataReal_Cell.h"

#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"
#include "Discretization/DG/DiscretizationDGBR2.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  Galerkin boundary-trace integral
//

template<class IntegrandBoundaryTrace>
class ErrorEstimateBoundaryTrace_VMSD_impl :
    public GroupIntegralBoundaryTraceType< ErrorEstimateBoundaryTrace_VMSD_impl<IntegrandBoundaryTrace> >
{
public:
  typedef typename IntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename IntegrandBoundaryTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandBoundaryTrace::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename DLA::VectorS<PhysDim::D, Real> VecReal;

  // Save off the boundary trace integrand and the residual vectors
  // accSign is the accumulation sign for the residual, this is needed to allow removal of weak form boundary fluxes after the fact
  explicit ErrorEstimateBoundaryTrace_VMSD_impl( const IntegrandBoundaryTrace& fcn, const int accSign ) :
    fcn_(fcn), accSign_(accSign) {}

  std::size_t nBoundaryGroups() const { return fcn_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcn_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
// does some checks on the inputs to make sure they all make sense
  template < class TopoDim >
  void check( const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>, // q
                                                   Field<PhysDim,TopoDim,ArrayQ>, // qp
                                                   Field<PhysDim,TopoDim,ArrayQ>, // w
                                                   Field<PhysDim,TopoDim,Real> >::type& flds ) // e
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);
    const Field<PhysDim, TopoDim, ArrayQ>& qpfld = get<1>(flds);
    const Field<PhysDim, TopoDim, ArrayQ>& wfld = get<2>(flds);
    const Field<PhysDim, TopoDim, Real>& efld = get<3>(flds);

    SANS_ASSERT( qfld.nElem() == wfld.nElem() ); // same number of elems
    SANS_ASSERT( qfld.nElem() == qpfld.nElem() ); // same number of elems
    SANS_ASSERT( qfld.nElem() == efld.nElem() ); // same number of elems
    SANS_ASSERT( &qfld.getXField() == &qpfld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &wfld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &efld.getXField() ); // check both were made off the same grid
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class Topology, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobalL,
             const typename XFieldType::template  FieldCellGroupType<Topology>& xfldCell,
             const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>, // q
                                                  Field<PhysDim,TopoDim,ArrayQ>, // qp
                                                  Field<PhysDim,TopoDim,ArrayQ>, // w
                                                  Field<PhysDim,TopoDim,Real> >::type
                                                  ::template FieldCellGroupType<Topology>& fldsCell,
             const int traceGroupGlobal,
             const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
             int quadratureorder )
  {

    typedef typename XFieldType                             ::template FieldCellGroupType<Topology> XFieldCelLGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>        ::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename Field<PhysDim, TopoDim, Real  >        ::template FieldCellGroupType<Topology> EFieldCellGroupType;

    typedef typename XFieldCelLGroupType ::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupType ::template ElementType<> ElementQFieldClassL;
    typedef typename EFieldCellGroupType ::template ElementType<> ElementEFieldClassL;

    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>  XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<>             ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    // separating off Q, W and casting away the const on Efield
    const QFieldCellGroupType& qfldCell = get<0>(fldsCell);
    const QFieldCellGroupType& qpfldCell = get<1>(fldsCell);
    const QFieldCellGroupType& wfldCell = get<2>(fldsCell);
    EFieldCellGroupType& efldCell = const_cast<EFieldCellGroupType&>(get<3>(fldsCell));

    // element field variables
    ElementXFieldClassL xfldElem( xfldCell.basis() );
    ElementQFieldClassL qfldElem( qfldCell.basis() );
    ElementQFieldClassL qpfldElem( qpfldCell.basis() );
    ElementQFieldClassL wfldElem( wfldCell.basis() );
    ElementEFieldClassL efldElem( efldCell.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    //Checking it's a p0 or p1 field, assumed for now given they're the only partitions of unity
    SANS_ASSERT( efldElem.order() == 0 || efldElem.order() == 1 );

    const int nIntegrand = efldElem.nDOF();

    // trace element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, Real > integral(quadratureorder, nIntegrand);

    std::vector<Real> estPDEElem(nIntegrand);

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      const int elemCell = xfldTrace.getElementLeft( elem );
      CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // copy global grid/solution DOFs to element
      xfldCell.getElement( xfldElem, elemCell );
      qfldCell.getElement( qfldElem, elemCell );
      qpfldCell.getElement( qpfldElem, elemCell );
      wfldCell.getElement( wfldElem, elemCell );
      efldCell.getElement( efldElem, elemCell );

      xfldTrace.getElement( xfldElemTrace, elem );

      for (int k = 0; k < nIntegrand; k++)
        estPDEElem[k] = 0;

      integral( fcn_.integrand(xfldElemTrace, canonicalTraceL,
                               xfldElem, qfldElem, qpfldElem, wfldElem, efldElem),
                               get<-1>(xfldElemTrace), estPDEElem.data(), nIntegrand );

      // PDE
      for (int k = 0; k < nIntegrand; k++)
        efldElem.DOF(k) += accSign_*estPDEElem[k]; // so as not to overwrite

      efldCell.setElement( efldElem, elemCell );
    }
  }

protected:
  const IntegrandBoundaryTrace& fcn_;
  const int accSign_;
};

// Factory function

template<class IntegrandBoundaryTrace>
ErrorEstimateBoundaryTrace_VMSD_impl<IntegrandBoundaryTrace>
ErrorEstimateBoundaryTrace_VMSD( const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn,
                                     const int accSign = 1)
{
  return ErrorEstimateBoundaryTrace_VMSD_impl<IntegrandBoundaryTrace>(fcn.cast(), accSign);
}


}

#endif  // ERRORESTIMATEBOUNDARYTRACE_VMSD_H
