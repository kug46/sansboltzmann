// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ERRORESTIMATEBASE_VMSD_BR2_H
#define ERRORESTIMATEBASE_VMSD_BR2_H

// Cell integral residual functions

#include <vector> //vector
#include <set> // set

#include "Field/XField.h"
#include "Field/Field.h"
#include "Field/FieldLift.h"
#include "Field/XField_CellToTrace.h"
#include "ErrorEstimateBase_VMSD.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Field weighted Estimates for Galerkin
// Combines Cell, interior trace and boundary trace integrals together
//
//----------------------------------------------------------------------------//
//

template< class PhysDim, class TopoDim, class ArrayQ >
class ErrorEstimateBase_VMSD_BR2 : ErrorEstimateBase_VMSD<PhysDim, TopoDim, ArrayQ>
{
public:

  typedef ErrorEstimateBase_VMSD<PhysDim, TopoDim, ArrayQ> BaseType;
  typedef DLA::VectorS<PhysDim::D, ArrayQ> VectorArrayQ;

  // Constructor
  template<class... BCArgs>
  ErrorEstimateBase_VMSD_BR2( const XField<PhysDim, TopoDim>& xfld,
                              const Field<PhysDim, TopoDim, ArrayQ>& qfld, // q
                              const Field<PhysDim, TopoDim, ArrayQ>& qpfld, // q
                              const FieldLift_DG_Cell<PhysDim, TopoDim, VectorArrayQ>& rfld, // q
                              const FieldLift_DG_BoundaryTrace<PhysDim, TopoDim, VectorArrayQ>& rbfld, // q
                              const Field<PhysDim, TopoDim, ArrayQ>& lgfld, // lg
                              const Field<PhysDim, TopoDim, ArrayQ>& wfld, // w
                              const Field<PhysDim, TopoDim, ArrayQ>& wpfld, // w
                              const FieldLift_DG_Cell<PhysDim, TopoDim, VectorArrayQ>& sfld, // q
                              const FieldLift_DG_BoundaryTrace<PhysDim, TopoDim, VectorArrayQ>& sbfld, // q
                              const Field<PhysDim, TopoDim, ArrayQ>& mufld, // mu
                              std::shared_ptr<Field<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                              const std::vector<int>& CellGroups,
                              const std::vector<int>& active_BGroup_list)
                              :  BaseType(xfld, qfld, qpfld, lgfld, wfld, wpfld, mufld,
                                                        pLiftedQuantityfld, CellGroups, active_BGroup_list),
                                                        rfld_(rfld), rbfld_(rbfld), sfld_(sfld), sbfld_(sbfld)
  {}


  // Constructor
  template<class... BCArgs>
  ErrorEstimateBase_VMSD_BR2( const XField<PhysDim, TopoDim>& xfld,
                              const Field<PhysDim, TopoDim, ArrayQ>& qfld, // q
                              const Field<PhysDim, TopoDim, ArrayQ>& qpfld, // q
                              const FieldLift_DG_Cell<PhysDim, TopoDim, VectorArrayQ>& rfld, // q
                              const FieldLift_DG_BoundaryTrace<PhysDim, TopoDim, VectorArrayQ>& rbfld, // q
                              const Field<PhysDim, TopoDim, ArrayQ>& lgfld, // lg
                              const Field<PhysDim, TopoDim, ArrayQ>& wfld, // w
                              const Field<PhysDim, TopoDim, ArrayQ>& wpfld, // w
                              const FieldLift_DG_Cell<PhysDim, TopoDim, VectorArrayQ>& sfld, // q
                              const FieldLift_DG_BoundaryTrace<PhysDim, TopoDim, VectorArrayQ>& sbfld, // q
                              const Field<PhysDim, TopoDim, ArrayQ>& mufld, // mu
                              std::unique_ptr<Field_DG_Cell<PhysDim,TopoDim,Real>>& up_efld_DG_nodal, // efld EXTERNALLY created
                              std::shared_ptr<Field<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                              const std::vector<int>& CellGroups,
                              const std::vector<int>& active_BGroup_list)
                              : BaseType(xfld, qfld, qpfld, lgfld, wfld, wpfld, mufld, up_efld_DG_nodal,
                                                       pLiftedQuantityfld, CellGroups, active_BGroup_list),
                                                       rfld_(rfld), rbfld_(rbfld), sfld_(sfld), sbfld_(sbfld)
  {}

  using BaseType::check;
  using BaseType::getErrorEstimate;
  using BaseType::getLocalSolveErrorEstimate;
  using BaseType::getErrorIndicator;
  using BaseType::getLocalSolveErrorIndicator;
  using BaseType::getXField;
  using BaseType::getESField;
  using BaseType::getIField;
  using BaseType::getEField;
  using BaseType::fillVector;
  using BaseType::fillEArray;
  using BaseType::outputFields;
  using BaseType::efld_DG_nodal;

protected:
  using BaseType::xfld_;
  using BaseType::active_BGroup_list_;

  using BaseType::xfldCellToTrace_;

  using BaseType::efld_nodal_;    // q field estimates
  using BaseType::up_efld_DG_nodal_; // DG nodal field representation -- can come with precomputed data
  using BaseType::up_ifld_nodal_;    // indicator field
  using BaseType::up_eSfld_nodal_;   // estimate sum field

  using BaseType::epfld_;    // qp field estimates
  using BaseType::up_ipfld_;    // indicator field
  using BaseType::up_epSfld_;   // estimate sum field
  using BaseType::up_eBfld_nodal_;

  using BaseType::qfld_; // q
  using BaseType::qpfld_; // q'
  using BaseType::lgfld_; // lg
  using BaseType::wfld_; // w
  using BaseType::wpfld_; // w'
  using BaseType::mufld_; // mu
  using BaseType::pLiftedQuantityfld_; //pointer to lifted quantity field

  using BaseType::cellGroups_;

  const FieldLift_DG_Cell<PhysDim,TopoDim,VectorArrayQ>& rfld_;    // qp field estimates
  const FieldLift_DG_BoundaryTrace<PhysDim,TopoDim,VectorArrayQ>& rbfld_;    // qp boundary field estimates
  const FieldLift_DG_Cell<PhysDim,TopoDim,VectorArrayQ>& sfld_;    // qp field estimates
  const FieldLift_DG_BoundaryTrace<PhysDim,TopoDim,VectorArrayQ>& sbfld_;    // qp field estimates
};

} //namespace SANS

#endif  // ERRORESTIMATEBASE_VMSD_BR2_H
