// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ERRORESTIMATEBOUNDARYTRACE_VMSD_BR2_H
#define ERRORESTIMATEBOUNDARYTRACE_VMSD_BR2_H

// boundary-trace integral error estimation functions

#include <memory>     // std::unique_ptr

#include "Topology/ElementTopology.h"
#include "Field/Field.h"
#include "Field/FieldLift.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/ElementIntegral.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "Field/Element/GalerkinWeightedIntegral.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"
#include "Discretization/DG/DiscretizationDGBR2.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  DGBR2 error estimate boundary-trace integral
//

template<class IntegrandBoundaryTrace>
class ErrorEstimateBoundaryTrace_VMSD_BR2_impl :
    public GroupIntegralBoundaryTraceType< ErrorEstimateBoundaryTrace_VMSD_BR2_impl<IntegrandBoundaryTrace> >
{
public:
  typedef typename IntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename IntegrandBoundaryTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandBoundaryTrace::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename DLA::VectorS<PhysDim::D, Real> VecReal;

  // Save off the boundary trace integrand and the residual vectors
  explicit ErrorEstimateBoundaryTrace_VMSD_BR2_impl( const IntegrandBoundaryTrace& fcn,
                                                     const int accSign) :
    fcn_(fcn), accSign_(accSign), comm_rank_(0) {}

  std::size_t nBoundaryGroups() const { return fcn_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcn_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
// does some checks on the inputs to make sure they all make sense
  template < class TopoDim >
  void check( const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>, // q
                                                   Field<PhysDim,TopoDim,ArrayQ>, // qp
                                                   Field<PhysDim,TopoDim,ArrayQ>, // w
                                                   Field<PhysDim,TopoDim,ArrayQ>, // wp
                                                   Field<PhysDim,TopoDim,Real> >::type& flds,
              const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,VectorArrayQ>,
                                                   Field<PhysDim,TopoDim,VectorArrayQ>>::type & fldsLiftCellL )
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);
    const Field<PhysDim, TopoDim, ArrayQ>& wfld = get<2>(flds);
    const Field<PhysDim, TopoDim, Real>& efld = get<4>(flds);

    SANS_ASSERT( qfld.nElem() == wfld.nElem() ); // same number of elems
    SANS_ASSERT( qfld.nElem() == efld.nElem() ); // same number of elems
    SANS_ASSERT( &qfld.getXField() == &wfld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &efld.getXField() ); // check both were made off the same grid

    comm_rank_ = qfld.comm()->rank();
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class Topology, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobalL,
             const typename XFieldType::template FieldCellGroupType<Topology>& xfldCellL,
             const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>, // q
                                                  Field<PhysDim,TopoDim,ArrayQ>, // qp
                                                  Field<PhysDim,TopoDim,ArrayQ>, // w
                                                  Field<PhysDim,TopoDim,ArrayQ>, // wp
                                                  Field<PhysDim,TopoDim,Real>>::type // e
                              ::template FieldCellGroupType<Topology>& fldsCellL,
             const int traceGroupGlobal,
             const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
             const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,VectorArrayQ>,  //rb
                                                  Field<PhysDim,TopoDim,VectorArrayQ>>::type // sb
                              ::template FieldCellGroupType<Topology>& fldsLiftCellL,
             int quadratureorder )
  {
    typedef typename XFieldType                             ::template FieldCellGroupType<Topology> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>        ::template FieldCellGroupType<Topology> QFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, Real  >        ::template FieldCellGroupType<Topology> EFieldCellGroupTypeL;
    typedef typename Field<PhysDim,TopoDim,VectorArrayQ>::template FieldCellGroupType<Topology> RFieldCellGroupTypeL;
//    typedef typename FieldLift<PhysDim,TopoDim,Real>     ::template FieldCellGroupType<Topology> ELFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL ::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL ::template ElementType<> ElementQFieldClassL;
    typedef typename RFieldCellGroupTypeL ::template ElementType<> ElementRFieldClassL;
    typedef typename EFieldCellGroupTypeL ::template ElementType<> ElementEFieldClassL;
//    typedef typename ELFieldCellGroupTypeL::template ElementType<> ElementELFieldClassL;

    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>  XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<>             ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    // separating off Q, W and casting away the const on Efield
    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const QFieldCellGroupTypeL& qpfldCellL = get<1>(fldsCellL);
    const QFieldCellGroupTypeL& wfldCellL = get<2>(fldsCellL);
    const QFieldCellGroupTypeL& wpfldCellL = get<3>(fldsCellL);
    EFieldCellGroupTypeL& efldCellL = const_cast<EFieldCellGroupTypeL&>(get<4>(fldsCellL));

    const RFieldCellGroupTypeL& rbfldCellL = get<0>(fldsLiftCellL);
    const RFieldCellGroupTypeL& sbfldCellL = get<1>(fldsLiftCellL);

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );

    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementQFieldClassL qpfldElemL( qpfldCellL.basis() );
    ElementRFieldClassL rbfldElemL( rbfldCellL.basis() );

    ElementQFieldClassL wfldElemL( wfldCellL.basis() );
    ElementQFieldClassL wpfldElemL( wpfldCellL.basis() );
    ElementEFieldClassL efldElemL( efldCellL.basis() );
    ElementRFieldClassL sbfldElemL( sbfldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );
    //Checking it's a p0 or p1 field, assumed for now given they're the only partitions of unity
    SANS_ASSERT( efldElemL.order() == 0 || efldElemL.order() == 1 );

    const int nIntegrand = efldElemL.nDOF();

    // trace element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, Real > integral(quadratureorder, nIntegrand);

    GalerkinWeightedIntegral<TopoDim, Topology, Real > integralCell(quadratureorder, nIntegrand);

    std::vector<Real> estPDEElem(nIntegrand);

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      const int elemL = xfldTrace.getElementLeft( elem );
      CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );
      qpfldCellL.getElement( qpfldElemL, elemL );

      wfldCellL.getElement( wfldElemL, elemL );
      wpfldCellL.getElement( wpfldElemL, elemL );
      efldCellL.getElement( efldElemL, elemL );

      //trace element numbering
      rbfldCellL.getElement( rbfldElemL, elem );
      sbfldCellL.getElement( sbfldElemL, elem );
      xfldTrace.getElement( xfldElemTrace, elem );

      int rankL = qfldElemL.rank();

      // nothing to do if elements is ghosted
      if (rankL != comm_rank_) continue;

      for (int k = 0; k < nIntegrand; k++)
        estPDEElem[k] = 0;

      integral( fcn_.integrand(xfldElemTrace, canonicalTraceL,
                               xfldElemL,
                               qfldElemL, qpfldElemL, rbfldElemL,
                               wfldElemL, wpfldElemL, sbfldElemL, efldElemL),
                get<-1>(xfldElemTrace), estPDEElem.data(), nIntegrand );

      // PDE
      for (int k = 0; k < nIntegrand; k++)
        efldElemL.DOF(k) += accSign_*estPDEElem[k]; // so as not to overwrite


      for (int k = 0; k < nIntegrand; k++)
        estPDEElem[k] = 0;

      integralCell( fcn_.integrandCellLO_FW( xfldElemL, rbfldElemL, sbfldElemL, efldElemL),
                get<-1>(xfldElemL), estPDEElem.data(), nIntegrand );

      for (int k = 0; k < nIntegrand; k++)
        efldElemL.DOF(k) += accSign_*estPDEElem[k]; // so as not to overwrite

      efldCellL.setElement( efldElemL, elemL );

    }
  }

protected:
  const IntegrandBoundaryTrace& fcn_;
  const int accSign_;
  mutable int comm_rank_;
};

// Factory function

template<class IntegrandBoundaryTrace>
ErrorEstimateBoundaryTrace_VMSD_BR2_impl<IntegrandBoundaryTrace>
ErrorEstimateBoundaryTrace_VMSD_BR2( const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn,
                                     const int accSign = 1)
{
  return ErrorEstimateBoundaryTrace_VMSD_BR2_impl<IntegrandBoundaryTrace>(fcn.cast(), accSign);
}


}

#endif  // ERRORESTIMATEBOUNDARYTRACE_VMSD_BR2_H
