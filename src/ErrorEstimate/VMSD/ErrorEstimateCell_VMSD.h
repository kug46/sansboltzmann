// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ERRORESTIMATECELL_VMSD_H
#define ERRORESTIMATECELL_VMSD_H

// Cell weighted integrals

#include <memory> //unique_ptr

#include "Field/Field.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/XField_CellToTrace.h"

#include "Discretization/QuadratureOrder.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"
#include "Discretization/IntegrateGhostBoundaryTraceGroups.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"
#include "ErrorEstimate_InnerBoundaryTrace_VMSD.h"
#include "ErrorEstimateInteriorTrace_VMSD.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

namespace SANS
{

/*

//----------------------------------------------------------------------------//
Field Cell group weighted residual for Galerkin

topology specific group weighted residual

template parameters:
  Topology                          element topology (e.g. Triangle)
  Integrand                         integrand functor
  XFieldType                        Grid data type, possibly a tuple with parameters
  QFieldCellGroupTypeTuple          Inputs required by Field Weighted Integrands and efld

  Tuple of QWE gets passed down to integrands which specify efldElem.
  It will then weight by phi w, where phi is the basis used in the efld, and w
  is the adjoint weighting.

//----------------------------------------------------------------------------//
 Error Estimate cell group weighted integral

*/

template< class IntegrandCell, class IntegrandITrace, class XFieldType_, class TopoDim_ >
class ErrorEstimateCell_VMSD_impl :
    public GroupIntegralCellType< ErrorEstimateCell_VMSD_impl<IntegrandCell, IntegrandITrace, XFieldType_, TopoDim_ > >
{
public:
  typedef typename IntegrandCell::PhysDim PhysDim;
  typedef typename IntegrandCell::template ArrayQ<Real> ArrayQ;

  // Save off the cell integrand
  ErrorEstimateCell_VMSD_impl( const IntegrandCell& fcnCell,
                               const IntegrandITrace& fcnITrace, const IntegrandITrace& fcnBTrace,
                               const XField_CellToTrace<PhysDim, TopoDim_>& xfldCellToTrace,
                               const XFieldType_& xfld,
                               const Field<PhysDim, TopoDim_, ArrayQ>& qfld,
                               const Field<PhysDim, TopoDim_, ArrayQ>& qpfld,
                               const Field<PhysDim, TopoDim_, ArrayQ>& wfld,
                               const Field<PhysDim, TopoDim_, ArrayQ>& wpfld,
                               const QuadratureOrder& quadOrder,
                               Field<PhysDim, TopoDim_, Real>& efld,
                               Field<PhysDim, TopoDim_, Real>& epfld) :
                                 fcnCell_(fcnCell), fcnITrace_(fcnITrace), fcnBTrace_(fcnBTrace),
                                 xfldCellToTrace_(xfldCellToTrace),
                                 xfld_(xfld), qfld_(qfld), qpfld_(qpfld),
                                 wfld_(wfld), wpfld_(wpfld), quadOrder_(quadOrder),
                                 nITraceGroup_( xfld.getXField().nInteriorTraceGroups() ),
                                 nBTraceGroup_( xfld.getXField().nBoundaryTraceGroups() ),
                                 nGTraceGroup_( xfld.getXField().nGhostBoundaryTraceGroups() ),
                                 efld_(efld), epfld_(epfld) {}


  std::size_t nCellGroups() const { return fcnCell_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcnCell_.cellGroup(n); }

//----------------------------------------------------------------------------//
// Check that q, w and e flds have corresponding dof
  template <class TopoDim>
  void check( const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>,
                                                   Field<PhysDim,TopoDim,ArrayQ>,
                                                   Field<PhysDim,TopoDim,ArrayQ>,
                                                   Field<PhysDim,TopoDim,ArrayQ>,
                                                   Field<PhysDim,TopoDim,Real>,
                                                   Field<PhysDim,TopoDim,Real>>::type& flds ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);
    const Field<PhysDim, TopoDim, ArrayQ>& qpfld = get<1>(flds);
    const Field<PhysDim, TopoDim, ArrayQ>& wfld = get<2>(flds);
    const Field<PhysDim, TopoDim, ArrayQ>& wpfld = get<3>(flds);
    const Field<PhysDim, TopoDim, Real>& efld = get<4>(flds);
    const Field<PhysDim, TopoDim, Real>& epfld = get<5>(flds);

    SANS_ASSERT( qfld.nElem() == wfld.nElem() ); // same number of elems
    SANS_ASSERT( qfld.nElem() == efld.nElem() ); // same number of elems
    SANS_ASSERT( qpfld.nElem() == wpfld.nElem() ); // same number of elems
    SANS_ASSERT( qpfld.nElem() == efld.nElem() ); // same number of elems
    SANS_ASSERT( qpfld.nElem() == epfld.nElem() ); // same number of elems
    SANS_ASSERT( &qfld.getXField() == &wfld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qfld.getXField() == &efld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qpfld.getXField() == &wpfld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qpfld.getXField() == &efld.getXField() ); // check both were made off the same grid
    SANS_ASSERT( &qpfld.getXField() == &epfld.getXField() ); // check both were made off the same grid
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobal,
             const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
             const typename MakeTuple< FieldTuple,
                                       Field<PhysDim,TopoDim,ArrayQ>,
                                       Field<PhysDim,TopoDim,ArrayQ>,
                                       Field<PhysDim,TopoDim,ArrayQ>,
                                       Field<PhysDim,TopoDim,ArrayQ>,
                                       Field<PhysDim,TopoDim,Real>,
                                       Field<PhysDim,TopoDim,Real>>::type
                              ::template FieldCellGroupType<Topology>& flds,
             const int quadratureorder )
  {

    typedef typename XFieldType::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field<PhysDim,TopoDim,ArrayQ>::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename Field<PhysDim,TopoDim,Real>::template FieldCellGroupType<Topology> EFieldCellGroupType;

    // separating off Q, W and casting away the const on Efield
    const QFieldCellGroupType& qfldCell = get<0>(flds);
    const QFieldCellGroupType& qpfldCell = get<1>(flds);
    const QFieldCellGroupType& wfldCell = get<2>(flds);
    const QFieldCellGroupType& wpfldCell = get<3>(flds);
    EFieldCellGroupType& efldCell = const_cast<EFieldCellGroupType&>(get<4>(flds));
    EFieldCellGroupType& epfldCell = const_cast<EFieldCellGroupType&>(get<5>(flds));

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;
    typedef typename EFieldCellGroupType::template ElementType<> ElementEFieldClass;

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );
    ElementQFieldClass qpfldElem( qpfldCell.basis() );
    ElementQFieldClass wfldElem( wfldCell.basis() );
    ElementQFieldClass wpfldElem( wpfldCell.basis() );
    ElementEFieldClass efldElem( efldCell.basis() );
    ElementEFieldClass epfldElem( epfldCell.basis() );

    //Checking it's a p0 or p1 field, assumed for now given they're the only partitions of unity
    SANS_ASSERT_MSG( efldElem.order() == 0
    || (efldElem.order() == 1  && (efldElem.basis()->category() == BasisFunctionCategory_Hierarchical
                                || efldElem.basis()->category() == BasisFunctionCategory_Lagrange) ),
    "order = %i, category = %i", efldElem.order(), (int)efldElem.basis()->category() );
    SANS_ASSERT_MSG( epfldElem.order() == 0
    || (epfldElem.order() == 1  && (epfldElem.basis()->category() == BasisFunctionCategory_Hierarchical
                                || epfldElem.basis()->category() == BasisFunctionCategory_Lagrange) ),
    "order = %i, category = %i", epfldElem.order(), (int)epfldElem.basis()->category() );

    const int nIntegrand = efldElem.nDOF();
    const int npIntegrand = epfldElem.nDOF();

    // estimate array
    DLA::VectorD<Real> estPDEElem( nIntegrand );
    DLA::VectorD<Real> estPDEpElem( npIntegrand );

    // element integral
    GalerkinWeightedIntegral<TopoDim, Topology, Real> integral(quadratureorder, nIntegrand);
//    GalerkinWeightedIntegral<TopoDim, Topology, Real> integralp(quadratureorder, npIntegrand);

    // just to make sure things are consistent
    SANS_ASSERT( xfldCell.nElem() == efldCell.nElem() );
    SANS_ASSERT( xfldCell.nElem() == epfldCell.nElem() );

    auto integrandCoarse = fcnCell_.integrand( xfldElem, qfldElem, qpfldElem, wfldElem, wpfldElem, efldElem );

    // loop over elements within group
    const int nelem = xfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid/solution DOFs to element
      xfldCell.getElement( xfldElem, elem );
      qfldCell.getElement( qfldElem, elem );
      qpfldCell.getElement( qpfldElem, elem );
      wfldCell.getElement( wfldElem, elem );
      wpfldCell.getElement( wpfldElem, elem );
      efldCell.getElement( efldElem, elem );
      epfldCell.getElement( epfldElem, elem );

      for (int n = 0; n < nIntegrand; n++)
        estPDEElem[n] = 0;

      for (int n = 0; n < npIntegrand; n++)
        estPDEpElem[n] = 0;

      // cell integration for canonical element
      integral( integrandCoarse, get<-1>(xfldElem), estPDEElem.data(), nIntegrand );

      //DO THE TRACES!
      std::map<int,std::vector<int>> cellITraceGroups;
      std::map<int,std::vector<int>> cellBTraceGroups;
      std::map<int,std::vector<int>> cellGTraceGroups;

      //ELEMENT-WISE TRACE INTEGRAND
      for (int trace = 0; trace < Topology::NTrace; trace++)
      {
        const TraceInfo& traceinfo = xfldCellToTrace_.getTrace(cellGroupGlobal, elem, trace);

        if (traceinfo.type == TraceInfo::Interior)
        {
          cellITraceGroups[traceinfo.group].push_back(traceinfo.elem);
        }
        else if (traceinfo.type == TraceInfo::Boundary) //boundary trace
        {
          cellBTraceGroups[traceinfo.group].push_back(traceinfo.elem);
        }
        else if (traceinfo.type == TraceInfo::GhostBoundary) //boundary trace
        {
          cellGTraceGroups[traceinfo.group].push_back(traceinfo.elem);
        }
      }


      //Collect the contributions to the PDE residual from the traces
      if (nITraceGroup_>0)
      {
        std::vector<int> quadOrderITrace;

        for (int i=0; i< nITraceGroup_; i++)
        {
          quadOrderITrace.push_back(quadOrder_.interiorTraceOrders[0]);
        }

        IntegrateInteriorTraceGroups<TopoDim>::integrate(ErrorEstimateInteriorTrace_VMSD(fcnITrace_, cellITraceGroups,
                                                                           cellGroupGlobal, elem, estPDEElem, estPDEpElem),
                                                                    xfld_, (qfld_, qpfld_, wfld_, wpfld_, efld_, epfld_),
                                                                    quadOrderITrace.data(), quadOrderITrace.size() );
      }


      if (nBTraceGroup_ > 0)
      {
        std::vector<int> quadOrderBTrace;

        for (int i=0; i< nBTraceGroup_; i++)
          quadOrderBTrace.push_back(quadOrder_.interiorTraceOrders[0]);


        IntegrateBoundaryTraceGroups<TopoDim>::integrate(
            ErrorEstimate_InnerBoundaryTrace_VMSD(fcnBTrace_, cellBTraceGroups,
                                                  cellGroupGlobal, elem, estPDEElem, estPDEpElem),
                                                         xfld_, (qfld_, qpfld_, wfld_, wpfld_, efld_, epfld_),
                                                         quadOrderBTrace.data(), quadOrderBTrace.size() );
      }

      if (nGTraceGroup_ > 0)
      {
        std::vector<int> quadOrderGTrace;

        for (int i=0; i< nGTraceGroup_; i++)
          quadOrderGTrace.push_back(quadOrder_.interiorTraceOrders[0]);


        IntegrateGhostBoundaryTraceGroups<TopoDim>::integrate(
            ErrorEstimate_InnerBoundaryTrace_VMSD(fcnBTrace_, cellGTraceGroups,
                                                  cellGroupGlobal, elem, estPDEElem, estPDEpElem),
                                                              xfld_, (qfld_, qpfld_, wfld_, wpfld_, efld_, epfld_),
                                                              quadOrderGTrace.data(), quadOrderGTrace.size() );
      }

      // PDE
      for (int n = 0; n < nIntegrand; n++)
        efldElem.DOF(n) += estPDEElem[n]; // so as not to overwrite

      for (int n=0; n < npIntegrand; n++)
        epfldElem.DOF(n) += estPDEpElem[n];

      efldCell.setElement( efldElem, elem );
      epfldCell.setElement( epfldElem, elem );

    }
  }

protected:
  const IntegrandCell& fcnCell_;
  const IntegrandITrace& fcnITrace_;
  const IntegrandITrace& fcnBTrace_;
  const XField_CellToTrace<PhysDim, TopoDim_>& xfldCellToTrace_;

  const XFieldType_& xfld_;
  const Field<PhysDim, TopoDim_, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim_, ArrayQ>& qpfld_;

  const Field<PhysDim, TopoDim_, ArrayQ>& wfld_;
  const Field<PhysDim, TopoDim_, ArrayQ>& wpfld_;

  const QuadratureOrder& quadOrder_;
  const int nITraceGroup_;
  const int nBTraceGroup_;
  const int nGTraceGroup_;

  Field<PhysDim, TopoDim_, Real>& efld_;
  Field<PhysDim, TopoDim_, Real>& epfld_;
};

//Factory Function
template<class IntegrandCell, class IntegrandITrace,
         class XFieldType, class PhysDim, class TopoDim, class ArrayQ>
ErrorEstimateCell_VMSD_impl<IntegrandCell, IntegrandITrace, XFieldType, TopoDim>
ErrorEstimateCell_VMSD( const IntegrandCellType<IntegrandCell>& fcnCell,
                       const IntegrandInteriorTraceType<IntegrandITrace>& fcnITrace,
                       const IntegrandInteriorTraceType<IntegrandITrace>& fcnBTrace,
                       const XField_CellToTrace<PhysDim, TopoDim>& xfldCellToTrace,
                       const XFieldType& xfld,
                       const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                       const Field<PhysDim, TopoDim, ArrayQ>& qpfld,
                       const Field<PhysDim, TopoDim, ArrayQ>& wfld,
                       const Field<PhysDim, TopoDim, ArrayQ>& wpfld,
                       const QuadratureOrder& quadOrder,
                       Field<PhysDim, TopoDim, Real>& efld,
                       Field<PhysDim, TopoDim, Real>& epfld)
{
  return { fcnCell.cast(), fcnITrace.cast(), fcnBTrace.cast(),
           xfldCellToTrace, xfld, qfld, qpfld, wfld, wpfld, quadOrder,
           efld, epfld };
}



} //namespace SANS

#endif  // ErrorEstimateCell_VMSD_H
