// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ERRORESTIMATE_L2_H
#define ERRORESTIMATE_L2_H

// Cell integral residual functions

#include <vector> //vector

#include "tools/Tuple.h"
#include "tools/KahanSum.h"

#include "Field/XField.h"
#include "Field/Field.h"

#include "Field/tools/findGroups.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/tools/for_each_BoundaryFieldTraceGroup_Cell.h"
#include "pde/NDConvert/FunctionNDConvertSpace1D.h"
#include "pde/NDConvert/FunctionNDConvertSpace2D.h"
#include "pde/NDConvert/FunctionNDConvertSpace3D.h"
#include "pde/NDConvert/FunctionNDConvertSpace4D.h"
#include "pde/NDConvert/FunctionNDConvertSpaceTime3D.h"

#include "ErrorEstimate/ErrorEstimate_Common.h"
#include "Discretization/Galerkin/IntegrandCell_ProjectFunction.h"
#include "Discretization/Galerkin/IntegrandInteriorTrace_JumpPenalty.h"

// For the cell integral class
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"
#include "Field/Tuple/FieldTuple.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

// For the actual cell integral
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"
#include "Discretization/QuadratureOrder.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#endif

#include "Field/Local/XField_LocalPatch.h"
#include "Discretization/Galerkin/ExtractCGLocalBoundaries.h"

#include "tools/abs.h"
#include "tools/make_unique.h" // This is in namespace std so that it looks just like make_shared
#include "tools/linspace.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Field weighted Estimates for Galerkin
// Combines Cell, interior trace and boundary trace integrals together
//
//----------------------------------------------------------------------------//
//

// Forward declare of XField_Local_Base
template<class PhysDim, class TopoDim>
class XField_Local_Base;


template< class SolnExact, class PhysDim_, class TopoDim_, class ArrayQ_ >
class ErrorEstimate_L2
{
public:
  typedef PhysDim_ PhysDim;
  typedef TopoDim_ TopoDim;
  typedef ArrayQ_ ArrayQ;

  typedef IntegrandCell_ProjectFunction<FunctionNDConvertSpace<PhysDim, ArrayQ>,IntegrandCell_ProjFcn_detail::FcnX> IntegrandCellClass;
  typedef IntegrandInteriorTrace_JumpPenalty<PhysDim> IntegrandInteriorTraceClass;

  // Constructor
  ErrorEstimate_L2( const XField<PhysDim, TopoDim>& xfld,
                    const Field<PhysDim, TopoDim, ArrayQ>& qfld, // q
                    const SolnExact& solnExact,
                    const QuadratureOrder& quadratureOrder,
                    const int estimateOrder, // is it a nodal or elemental field?
                    const std::vector<int>& cellGroups)
                    : xfld_(xfld),
                      qfld_(qfld),
                      solnExact_(solnExact),
                      quadratureOrder_(quadratureOrder),
                      estimateOrder_(estimateOrder),
                      cellGroups_(cellGroups),
                      fcnCell_(solnExact,cellGroups_),
                      fcnTrace_(findInteriorTraceGroup(xfld,0,1))
  {
    check(); // check the inputs work together
    if (estimateOrder_ == 0)
      pefld_ = make_unique<Field_DG_Cell<PhysDim,TopoDim,Real>>(xfld_,0,BasisFunctionCategory_Legendre);
    else if (estimateOrder_ == 1)
      pefld_ = make_unique<Field_CG_Cell<PhysDim,TopoDim,Real>>(xfld_,1,BasisFunctionCategory_Lagrange);
    else
      SANS_DEVELOPER_EXCEPTION("Only elemental and nodal estimate fields supported");

    estimate();
  }


  // Constructor
  ErrorEstimate_L2( const XField<PhysDim, TopoDim>& xfld,
                    const Field<PhysDim, TopoDim, ArrayQ>& qfld, // q
                    std::unique_ptr<Field_DG_Cell<PhysDim,TopoDim,Real>>& up_efld_DG_nodal, // reference so base can take control
                    const SolnExact& solnExact,
                    const QuadratureOrder& quadratureOrder,
                    const int estimateOrder, // is it a nodal or elemental field?
                    const std::vector<int>& cellGroups)
                    : xfld_(xfld),
                      qfld_(qfld),
                      up_efld_DG_nodal_(std::move(up_efld_DG_nodal)), // Take control of the pointer. The heap memory is now owned
                      solnExact_(solnExact),
                      quadratureOrder_(quadratureOrder),
                      estimateOrder_(estimateOrder),
                      cellGroups_(cellGroups),
                      fcnCell_(solnExact,cellGroups_),
                      fcnTrace_(findInteriorTraceGroup(xfld,0,1))
  {
    check(); // check the inputs work together
    if (estimateOrder_ == 0)
      pefld_ = make_unique<Field_DG_Cell<PhysDim,TopoDim,Real>>(xfld_,0,BasisFunctionCategory_Legendre);
    else if (estimateOrder_ == 1)
      pefld_ = make_unique<Field_CG_Cell<PhysDim,TopoDim,Real>>(xfld_,1,BasisFunctionCategory_Lagrange);
    else
      SANS_DEVELOPER_EXCEPTION("Only elemental and nodal estimate fields supported");

    estimate();
  }

  void
  getErrorEstimate( Real& estimate, const std::vector<int>& cellGroups )
  {
    KahanSum<Real> Sum=0;
    for (int dof = 0; dof < pefld_->nDOFpossessed(); dof++)
      Sum += pefld_->DOF(dof);

    estimate = static_cast<Real>(Sum);
#ifdef SANS_MPI
    estimate = boost::mpi::all_reduce( *qfld_.comm(), estimate, std::plus<Real>() );
#endif

  }

  void getErrorEstimate( Real& estimate ) { getErrorEstimate( estimate, cellGroups_ ); }
  void getErrorIndicator( Real& estimate, const std::vector<int>& cellGroups ) { getErrorEstimate(estimate, cellGroups); }
  void getErrorIndicator( Real& estimate ) { getErrorEstimate( estimate, cellGroups_ );  }

  void
  getLocalSolveErrorIndicator( std::vector<Real>& estimate )
  {
    if (estimateOrder_ == 0)
    {
      // the dofs to be included in the new elemental estimate
      std::vector<std::set<int>> elementDOFSupportMap;

      // Collect the set of dofs that need to be collected into the local estimate
      // For a DG field this will just give the dofs for cell group 0.
      // For a CG field this will avoid double counting dofs that are shared by multiple elements in cell group 0
      for_each_CellGroup<TopoDim>::apply( ConstructDOFSet<PhysDim>(elementDOFSupportMap,{0}), *pefld_ );

      estimate.resize( elementDOFSupportMap.size() );

      for (std::size_t i = 0; i < elementDOFSupportMap.size(); i++ )
      {
        estimate[i] = 0;
        for (auto it = elementDOFSupportMap[i].begin(); it != elementDOFSupportMap[i].end(); ++it) // loop over dofs in macro set
          estimate[i] += pefld_->DOF(*it);

        estimate[i] = fabs(estimate[i]);
      }
    }
    else
    {
#if (!defined(WHOLEPATCH)) && (!defined(INNERPATCH))
      estimateLocal();
#endif

      typedef typename Simplex<TopoDim>::type Topology; // Local Split Patch assumes purely simplicial mesh

      // EDGE MODE ESTIMATION
      std::vector<int> localAttachedNodes, newLinearNodeDOFs;
      // check you're using the right patch
      SANS_ASSERT_MSG( xfld_.derivedTypeID() == typeid(XField_LocalPatch<PhysDim,Topology>), "Not an Edge Local Patch" );

      // get the pointer
      const XField_LocalPatch<PhysDim,Topology>* plocal_xfld = static_cast<const XField_LocalPatch<PhysDim,Topology>*>(&xfld_);

      // check that it is being run in edge mode
      SANS_ASSERT( plocal_xfld->isEdgeMode() );

      localAttachedNodes = plocal_xfld->getLocalLinearCommonNodes();
      newLinearNodeDOFs = plocal_xfld->getNewLinearNodeDOFs();

      // the set of nodes making up the edge or opposite to it
      // these are from the local grid - thus they correspond to dofs in the xfield used to generate pefld.
      SANS_ASSERT_MSG( localAttachedNodes.size() >= 2, "There should be at least two nodes!" );
      SANS_ASSERT_MSG( localAttachedNodes[0] == 0, "The first attached node is the start of the edge" );
      SANS_ASSERT_MSG( localAttachedNodes[1] == 1, "The second attached node is the end of the edge" );

      // This is a local dof, in the indexing of pefld.
      // Adding in no new allows this to be called with unsplit patches
      SANS_ASSERT_MSG( newLinearNodeDOFs.size() <= 1,"There should be one new node or no new node!");

      estimate.assign(localAttachedNodes.size(),0);
      // the localNode is the node on the xfld_local
      // there is also a globalNode list that correspond to xfld_linear_
      for (std::size_t node = 0; node < localAttachedNodes.size(); node++)
      {
        const int localNode = localAttachedNodes[node]; // the local node in the grid

        estimate[node] += pefld_->DOF(localNode); // indexed relative to localCommonNodes;

        // The first two nodes make up the edge. Each gets half of the error associated with the new vertex.
        if (node < 2 && newLinearNodeDOFs.size() > 0)
          estimate[node] += 0.5*pefld_->DOF(newLinearNodeDOFs[0]);

        estimate[node] = fabs(estimate[node]);
      }
    }
  }

  void
  fillEArray(std::vector<std::vector<Real>>& estimates ) // fragile interface for Savithru
  {
    if (estimateOrder_ == 0)
    {
      estimates.resize(xfld_.nCellGroups());
      for (int i = 0; i < xfld_.nCellGroups(); i++ )
        estimates[i].resize(xfld_.getCellGroupBaseGlobal(i).nElem());

      // Populate the vector of vectors from the relevant eSfld_
      for_each_CellGroup<TopoDim>::apply( AccumulateVector<PhysDim>(estimates,cellGroups_),*pefld_);

      // Do the absolution at the element level, everything having been now accumulated
      for (auto it = estimates.begin(); it != estimates.end(); ++it)
        for (auto itt = it->begin(); itt != it->end(); ++itt)
        {
          *itt = fabs(*itt);
        }
    }
    else
    {
      // All nodal estimates are placed in one large vector
      estimates.resize(1);
      estimates[0].reserve(pefld_->nDOF());
      for (int dof = 0; dof < pefld_->nDOF(); dof++)
        estimates[0].push_back(fabs(pefld_->DOF(dof)));
    }

  }

  // accessor functions
  const XField<PhysDim, TopoDim>& getXField() const { return xfld_; }
  const Field<PhysDim,TopoDim,Real>& getEField() const { return *pefld_; }
  const Field<PhysDim,TopoDim,Real>& getIField() const { return *pefld_; }

  void
  outputFields( const std::string& filename ) const
  {
    pefld_->syncDOFs_MPI_noCache();
    output_Tecplot( *pefld_, filename, {"efld"} );
  }

  // A reference to the data in the internally controlled unique_ptr
  // Binding a reference to the end of a unique_ptr is slightly sketchy. HOWEVER, this is ok here because
  // in estimate() it is created if it isn't passed in, and estimate() is called in the constructor.
  // Thus there is no way that this pointer can be a nullptr by time this gets called.
  const Field_DG_Cell<PhysDim,TopoDim,Real>& efld_DG_nodal() const { return *up_efld_DG_nodal_; }

protected:

  //----------------------------------------------------------------------------//
  // Check that the primal and adjoint fields are all reasonable
  void check( )
  {
    SANS_ASSERT( &xfld_.getXField() == &qfld_.getXField() ); // check that xfld matches that from constructor

    // check the passed in pointer is valid
    if ( up_efld_DG_nodal_ != nullptr ) // the external field has some data. This means we are doing a local solve/can skip some re-compute!
    {
      SANS_ASSERT_MSG( fcnCell_.nCellGroups() == 1, "Should only be one cell group in the functional" );
      SANS_ASSERT_MSG( fcnCell_.cellGroup(0) == 0, "Functional should only be applied to cell group 0" );
      SANS_ASSERT_MSG( &up_efld_DG_nodal_->getXField() == &qfld_.getXField(), "The externally created field must be made from the same XField" );
    }
    else if (estimateOrder_ == 1) // first order estimates and no pre-computation -> need to estimate using all groups
    {
      SANS_ASSERT_MSG( fcnCell_.nCellGroups() == (std::size_t)xfld_.nCellGroups(), "Functional should be applied to all cell groups" );
      // SANS_ASSERT_MSG( fcnCell_.cellGroup(0) == 0, "Functional should be applied to cell group 0" );
      // SANS_ASSERT_MSG( fcnCell_.cellGroup(1) == 1, "Functional should be applied to cell group 1" );
    }
  }

  const XField<PhysDim, TopoDim>& xfld_;
  std::unique_ptr<Field<PhysDim,TopoDim,Real>> pefld_; // pointer to the estimate field

  const Field<PhysDim, TopoDim, ArrayQ>& qfld_; //
  std::unique_ptr<Field_DG_Cell<PhysDim,TopoDim,Real>> up_efld_DG_nodal_; // q field estimates - can be computed externally

  const SolnExact& solnExact_;

  const QuadratureOrder quadratureOrder_;
  const int estimateOrder_;
  std::vector<int> cellGroups_;

  IntegrandCellClass fcnCell_;
  IntegrandInteriorTraceClass fcnTrace_;

  void estimate()
  {
    *pefld_ = 0.0; // initialize error estimate field to zero

    if (estimateOrder_ == 0) // DG style, just call as usual
    {
      // Cell Estimates -- only computed for cell group 0
      IntegrateCellGroups<TopoDim>::integrate(L2Error_Cell(fcnCell_),
                                              xfld_, (qfld_, *pefld_ ),
                                              quadratureOrder_.cellOrders.data(),
                                              quadratureOrder_.cellOrders.size()  );
    }
    else // CG style
    {
#ifndef WHOLEPATCH
      if ( up_efld_DG_nodal_ == nullptr ) // if the field wasn't declared then we need to declare it
      {
        // This is what will happen for general
        up_efld_DG_nodal_ = SANS::make_unique<Field_DG_Cell<PhysDim,TopoDim,Real>>(xfld_, 1, BasisFunctionCategory_Lagrange );
        *up_efld_DG_nodal_ = 0; // zero the data
      }
      else // if the field was declared - that means there was precomputed data.
      {
        // zero out the data in the cell groups that the estimate is going to be re-computed for
        for_each_CellGroup<TopoDim>::apply( ZeroField<PhysDim,Real>(fcnCell_.cellGroups()), *up_efld_DG_nodal_ );
      }

      // Cell Estimates -- only computed for cell group 0
      IntegrateCellGroups<TopoDim>::integrate(L2Error_Cell(fcnCell_),
                                              xfld_, (qfld_, *up_efld_DG_nodal_ ),
                                              quadratureOrder_.cellOrders.data(),
                                              quadratureOrder_.cellOrders.size()  );

      // need to sync here because the efld will then be populated below by accumulating
      up_efld_DG_nodal_->syncDOFs_MPI_noCache();

      // map from the DG dofs to CG dofs
      std::map<int,int> DG_to_CG_DOFMap;

      // fill the map for all dofs that are in the DG field - transfer for all cell groups!
      for_each_CellGroup<TopoDim>::apply(
        PairFieldDOFs<PhysDim,Real>(DG_to_CG_DOFMap,linspace(0,xfld_.nCellGroups()-1)), (*up_efld_DG_nodal_,*pefld_) );

      // loop over the DG dofs and accumulate the data into the CG field
      for (const auto& keyVal : DG_to_CG_DOFMap )
        pefld_->DOF(keyVal.second) += up_efld_DG_nodal_->DOF(keyVal.first);
#else
      // Cell Estimates -- just the same as for DG
      IntegrateCellGroups<TopoDim>::integrate(L2Error_Cell(fcnCell_),
                                              xfld_, (qfld_, *pefld_ ),
                                              quadratureOrder_.cellOrders.data(),
                                              quadratureOrder_.cellOrders.size()  );
#endif
    } // CG style
  }

  // Additional piece for evaluating local error estimates
  inline void estimateLocal()
  {
    // estimate error associated with added SIP type stabilization
    IntegrateInteriorTraceGroups<TopoDim>
      ::integrate( JumpPenaltyError_InteriorTrace(fcnTrace_),
                   xfld_, (qfld_, *pefld_),
                   quadratureOrder_.interiorTraceOrders.data(),
                   quadratureOrder_.interiorTraceOrders.size() );
  }

  // An interior trace class to estimate the pseudo error from the introduction of non-conformity (in CG)
  template< class IntegrandInteriorTrace>
  class JumpPenaltyError_InteriorTrace_impl :
    public GroupIntegralInteriorTraceType<JumpPenaltyError_InteriorTrace_impl<IntegrandInteriorTrace>>
  {
  public:
    typedef typename IntegrandInteriorTrace::PhysDim PhysDim;
    typedef typename IntegrandInteriorTrace::template ArrayQ<Real> ArrayQ;

    // save off the integrand
    explicit JumpPenaltyError_InteriorTrace_impl( const IntegrandInteriorTrace& fcn ) :
    fcn_(fcn) {}

      std::size_t nInteriorTraceGroups() const { return fcn_.nInteriorTraceGroups(); }
      std::size_t interiorTraceGroup(const int n) const { return fcn_.interiorTraceGroup(n); }

    //----------------------------------------------------------------------------//
    // Checking that all the fields are from the same grid
    template <class TopoDim>
    void check( const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>,
                                                     Field<PhysDim,TopoDim,Real>>::type& flds) const
    {
      const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);
      const Field<PhysDim, TopoDim, ArrayQ>& efld = get<1>(flds);

      SANS_ASSERT( qfld.nElem() == efld.nElem() ); // same number of elems
      SANS_ASSERT( &qfld.getXField() == &efld.getXField() ); // check both were made off the same grid
    }

    //----------------------------------------------------------------------------//
    // Integration function that integrates each element in the interior trace group
    // THIS TOPODIM IS REALLY NOT NECESSARY
    template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class XFieldType>
    void
    integrate(
        const int cellGroupGlobalL,
        const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
        const typename MakeTuple< FieldTuple,
                                  Field<PhysDim,TopoDim,ArrayQ>,
                                  Field<PhysDim,TopoDim,Real>>::type
                                  ::template FieldCellGroupType<TopologyL>& fldsCellL,
        const int cellGroupGlobalR,
        const typename XFieldType::template FieldCellGroupType<TopologyR>& xfldCellR,
        const typename MakeTuple< FieldTuple,
                                  Field<PhysDim,TopoDim,ArrayQ>,
                                  Field<PhysDim,TopoDim,Real>>::type
                                  ::template FieldCellGroupType<TopologyR>& fldsCellR,
        const int traceGroupGlobal,
        const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
        int quadratureorder )
    {
      // // This shouldn't be necessary, but lets leave it in anyway
      // if ((cellGroupGlobalL != 0) && (cellGroupGlobalR != 0) ) return;
      // if ((cellGroupGlobalL == 0) && (cellGroupGlobalR == 0) ) return;
      SANS_ASSERT_MSG( (cellGroupGlobalL == 0) && (cellGroupGlobalR == 1), "should only be used on the broken trace" );

      typedef typename XFieldType                     ::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
      typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
      typedef typename Field<PhysDim, TopoDim, Real  >::template FieldCellGroupType<TopologyL> EFieldCellGroupTypeL;

      typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
      typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;
      typedef typename EFieldCellGroupTypeL::template ElementType<> ElementEFieldClassL;

      typedef typename XFieldType                     ::template FieldCellGroupType<TopologyR> XFieldCellGroupTypeR;
      typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;
      typedef typename Field<PhysDim, TopoDim, Real  >::template FieldCellGroupType<TopologyR> EFieldCellGroupTypeR;

      typedef typename XFieldCellGroupTypeR::template ElementType<> ElementXFieldClassR;
      typedef typename QFieldCellGroupTypeR::template ElementType<> ElementQFieldClassR;
      typedef typename EFieldCellGroupTypeR::template ElementType<> ElementEFieldClassR;

      typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
      typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
      typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

      // separating off Q, W and casting away the const on Efield
      const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
      EFieldCellGroupTypeL& efldCellL = const_cast<EFieldCellGroupTypeL&>(get<1>(fldsCellL));

      // separating off Q, W and casting away the const on Efield
      const QFieldCellGroupTypeR& qfldCellR = get<0>(fldsCellR);
      EFieldCellGroupTypeR& efldCellR = const_cast<EFieldCellGroupTypeR&>(get<1>(fldsCellR));

      // element field variables
      ElementXFieldClassL xfldElemL( xfldCellL.basis() );
      ElementQFieldClassL qfldElemL( qfldCellL.basis() );
      ElementEFieldClassL efldElemL( efldCellL.basis() );

      ElementXFieldClassR xfldElemR( xfldCellR.basis() );
      ElementQFieldClassR qfldElemR( qfldCellR.basis() );
      ElementEFieldClassR efldElemR( efldCellR.basis() );

      ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

      //Checking they're a P0 fields
      SANS_ASSERT( efldElemL.order() == 0 || efldElemL.order() == 1);
      SANS_ASSERT( efldElemR.order() == 0 || efldElemR.order() == 1);

      const int nIntegrandL = efldElemL.nDOF(), nIntegrandR = efldElemR.nDOF();

      // estimate arrays
      std::vector<Real> estPDEElemL( nIntegrandL ), estPDEElemR( nIntegrandR );

      // trace element integral
      GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, Real> integralL(quadratureorder,nIntegrandL);
      GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, Real> integralR(quadratureorder,nIntegrandR);

      // loop over trace elements within group
      int nelem = xfldTrace.nElem();
      for (int elem = 0; elem < nelem; elem++)
      {
        const int elemL = xfldTrace.getElementLeft( elem );
        const int elemR = xfldTrace.getElementRight( elem );
        CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
        CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

        // copy global grid/solution DOFs to element
        xfldCellL.getElement( xfldElemL, elemL );
        qfldCellL.getElement( qfldElemL, elemL );
        efldCellL.getElement( efldElemL, elemL );

        xfldCellR.getElement( xfldElemR, elemR );
        qfldCellR.getElement( qfldElemR, elemR );
        efldCellR.getElement( efldElemR, elemR );

        xfldTrace.getElement( xfldElemTrace, elem );

        // if ((cellGroupGlobalL == 0) && (cellGroupGlobalR == 1) )
        // {


          for (int k = 0; k < nIntegrandL; k++)
            estPDEElemL[k] = 0;

          // integral needs to return two Real that get allocated to elements afterwards
          integralL(
              fcn_.integrand( xfldElemTrace, canonicalTraceL, canonicalTraceR,
                              xfldElemL,
                              qfldElemL, efldElemL,
                              xfldElemR,
                              qfldElemR, efldElemR),
                              xfldElemTrace, estPDEElemL.data(), nIntegrandL );

          for (int k = 0; k < nIntegrandL; k++)
            efldElemL.DOF(k) += estPDEElemL[k];

          efldCellL.setElement( efldElemL, elemL );
        // }
        // else if ((cellGroupGlobalL == 1) && (cellGroupGlobalR == 0) )
        // {
          // SANS_DEVELOPER_EXCEPTION("Shouldn't get here, cell group 0 should always be left");
          // for (int k = 0; k < nIntegrandR; k++)
          //   estPDEElemR[k] = 0;
          //
          // // integral needs to return two Real that get allocated to elements afterwards
          // integralR(
          //     fcn_.integrand( xfldElemTrace, canonicalTraceR, canonicalTraceL,
          //                     xfldElemR,
          //                     qfldElemR, efldElemR,
          //                     xfldElemL,
          //                     qfldElemL, efldElemL),
          //                     xfldElemTrace, estPDEElemR.data(), nIntegrandR );
          //
          // for (int k = 0; k < nIntegrandR; k++)
          //   efldElemR.DOF(k) += estPDEElemR[k];
          //
          // efldCellR.setElement( efldElemR, elemR );
        // }
      }
    }
  protected:
    const IntegrandInteriorTrace& fcn_;
  };


  // Factory function

  template<class IntegrandInteriorTrace>
  JumpPenaltyError_InteriorTrace_impl<IntegrandInteriorTrace>
  JumpPenaltyError_InteriorTrace( const IntegrandInteriorTraceType<IntegrandInteriorTrace>& fcn )
  {
    return JumpPenaltyError_InteriorTrace_impl<IntegrandInteriorTrace>( fcn.cast() );
  }


  // A cell integrand class to apply the integrand
  // protected inside of ErrorEstimate_L2 because that's the only place it is needed
  template<class IntegrandCell >
  class L2Error_Cell_impl :
      public GroupIntegralCellType< L2Error_Cell_impl<IntegrandCell> >
  {
  public:
    typedef typename IntegrandCell::PhysDim PhysDim;
    typedef typename IntegrandCell::template ArrayQ<Real> ArrayQ;

    // Save off the cell integrand
    explicit L2Error_Cell_impl( const IntegrandCell& fcn) :
      fcn_(fcn) {}

    std::size_t nCellGroups() const { return fcn_.nCellGroups(); }
    std::size_t cellGroup(const int n) const { return fcn_.cellGroup(n); }

  //----------------------------------------------------------------------------//
  // Check that q, w and e flds have corresponding dof
    template <class TopoDim>
    void check( const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>,
                                                     Field<PhysDim,TopoDim,Real>>::type& flds ) const
    {
      const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);
      const Field<PhysDim, TopoDim, Real>& efld = get<1>(flds);

      SANS_ASSERT( qfld.nElem() == efld.nElem() ); // same number of elems
      SANS_ASSERT( &qfld.getXField() == &efld.getXField() ); // check both were made off the same grid
    }

  //----------------------------------------------------------------------------//
    // Integration function that integrates each element in the cell group
    // THE TOPODIM TEMPLATE IS NOT NECESSARY HERE!!!!
    template <class Topology, class TopoDim, class XFieldType>
    void
    integrate( const int cellGroupGlobal,
               const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
               const typename MakeTuple< FieldTuple,
                                         Field<PhysDim,typename Topology::TopoDim,ArrayQ>,
                                         Field<PhysDim,typename Topology::TopoDim,Real>>::type
                                ::template FieldCellGroupType<Topology>& flds,
               const int quadratureorder )
    {
      typedef typename XFieldType::template FieldCellGroupType<Topology> XFieldCellGroupType;
      typedef typename Field<PhysDim,typename Topology::TopoDim,ArrayQ>::template FieldCellGroupType<Topology> QFieldCellGroupType;
      typedef typename Field<PhysDim,typename Topology::TopoDim,Real>::template FieldCellGroupType<Topology> EFieldCellGroupType;

      // separating off Q, W and casting away the const on Efield
      const QFieldCellGroupType& qfldCell = get<0>(flds);
      EFieldCellGroupType& efldCell = const_cast<EFieldCellGroupType&>(get<1>(flds));

      typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
      typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;
      typedef typename EFieldCellGroupType::template ElementType<> ElementEFieldClass;

      // element field variables
      ElementXFieldClass xfldElem( xfldCell.basis() );
      ElementQFieldClass qfldElem( qfldCell.basis() );
      ElementEFieldClass efldElem( efldCell.basis() );

      //Checking it's a p0 or p1 field, assumed for now given they're the only partitions of unity
      SANS_ASSERT_MSG( efldElem.order() == 0
      || (efldElem.order() == 1  && (efldElem.basis()->category() == BasisFunctionCategory_Hierarchical
                                  || efldElem.basis()->category() == BasisFunctionCategory_Lagrange) ),
      "order = %i, category = %i", efldElem.order(), (int)efldElem.basis()->category() );

      const int nIntegrand = efldElem.nDOF();

      // estimate array
      std::vector<Real> estPDEElem( nIntegrand );

      // element integral -- We shouldn't be templating on TopoDim, just take it off the Topology
      GalerkinWeightedIntegral<typename Topology::TopoDim, Topology, Real> integral(quadratureorder, nIntegrand);

      // just to make sure things are consistent
      SANS_ASSERT( xfldCell.nElem() == efldCell.nElem() );

      auto integrand = fcn_.integrand(xfldElem, qfldElem, efldElem );

      // loop over elements within group
      const int nelem = xfldCell.nElem();
      for (int elem = 0; elem < nelem; elem++)
      {
        // copy global grid/solution DOFs to element
        xfldCell.getElement( xfldElem, elem );
        qfldCell.getElement( qfldElem, elem );
        efldCell.getElement( efldElem, elem );

        for (int n = 0; n < nIntegrand; n++)
          estPDEElem[n] = 0;

        // cell integration for canonical element
        integral( integrand, xfldElem, estPDEElem.data(), nIntegrand );

        // PDE
        for (int n = 0; n < nIntegrand; n++)
          efldElem.DOF(n) += estPDEElem[n]; // so as not to overwrite
        efldCell.setElement( efldElem, elem );

      }
    }

  protected:
    const IntegrandCell& fcn_;
  };

  // Factory function

  template<class IntegrandCell>
  L2Error_Cell_impl<IntegrandCell>
  L2Error_Cell( const IntegrandCellType<IntegrandCell>& fcn )
  {
    return L2Error_Cell_impl<IntegrandCell>( fcn.cast() );
  }


};




} //namespace SANS

#endif  // ERRORESTIMATE_L2_H
