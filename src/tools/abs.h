// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SANS_ABS_H
#define SANS_ABS_H

#ifndef ABS
#define ABS(a) ((a) > 0 ? (a) : -(a))
#endif


#endif //SANS_ABS_H
