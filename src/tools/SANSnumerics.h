// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SANSNUMERICS_H
#define SANSNUMERICS_H

typedef double Real;

typedef long LongInt;

// math constants
static const Real PI = 3.14159265358979323846;

#endif  // SANSNUMERICS_H
