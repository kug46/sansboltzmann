
INCLUDE( ${CMAKE_SOURCE_DIR}/CMakeInclude/ForceOutOfSource.cmake ) #This must be the first thing included

SET( OUTPUTFUNCTIONAL_SRC
  
  )

#ADD_LIBRARY( OutputFunctionalLib STATIC ${OUTPUTFUNCTIONAL_SRC} )

# This should be deleted and replaced with the library if cpp files are added
ADD_CUSTOM_TARGET( OutputFunctionalLib ) 

#Create the vera targest for this library
ADD_VERA_CHECKS_RECURSE( OutputFunctionalLib *.h *.cpp )
ADD_HEADER_COMPILE_CHECK_RECURSE( OutputFunctionalLib *.h )
#ADD_CPPCHECK( OutputFunctionalLib ${OUTPUTFUNCTIONAL_SRC} )