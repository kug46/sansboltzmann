// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_OUTPUTFUNCTIONAL_QTOOUTPUT_SHALLOWWATER2D_MANIFOLD_H_
#define SRC_OUTPUTFUNCTIONAL_QTOOUTPUT_SHALLOWWATER2D_MANIFOLD_H_

#include "pde/ShallowWater/PDEShallowWater2D_manifold.h"
#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Subfunctions to compute single output from shallow water solution variables

template <class QType, class PDENDConvert>
class QtoOutput_ShallowWater2D_manifold_H
{
public:
  typedef VarInterpret2D_manifold<QType> QInterpret;            // solution variable interpreter type

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  explicit QtoOutput_ShallowWater2D_manifold_H( const PDENDConvert& pde ) :
      pde_(pde), qInterpret_(pde_.getVarInterpreter()) {}

  template<class T>
  T operator()( ArrayQ<T>& q ) const
  {
    T H;
    qInterpret_.getH( q, H );

    return H;
  }

private:
  const PDENDConvert& pde_;
  const QInterpret& qInterpret_;
};

} // namespace SANS

#endif /* SRC_OUTPUTFUNCTIONAL_QTOOUTPUT_SHALLOWWATER2D_MANIFOLD_H_ */
