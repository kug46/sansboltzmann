// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_OUTPUTFUNCTIONAL_QTOOUTPUT_SHALLOWWATER2D_H_
#define SRC_OUTPUTFUNCTIONAL_QTOOUTPUT_SHALLOWWATER2D_H_

#include "pde/ShallowWater/PDEShallowWater2D.h"
#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Subfunctions to compute single output from shallow water solution variables

template <class QType, class PDENDConvert>
class QtoOutput_ShallowWater2D_H
{
public:
  typedef PhysD2 PhysDim;

  typedef VarInterpret2D<QType> QInterpret;            // solution variable interpreter type

  template<class T>
  using ArrayQ = ShallowWaterTraits<PhysD2>::ArrayQ<T>;

  explicit QtoOutput_ShallowWater2D_H( const PDENDConvert& pde ) :
      pde_(pde), qInterpret_(pde_.getVarInterpreter()) {}

  template<class T>
  T operator()( ArrayQ<T>& q ) const
  {
    T H;
    qInterpret_.getH( q, H );

    return H;
  }

private:
  const PDENDConvert& pde_;
  const QInterpret& qInterpret_;
};


template <class QType, class PDENDConvert>
class QtoOutput_ShallowWater2D_V
{
public:
  typedef PhysD2 PhysDim;

  typedef VarInterpret2D<QType> QInterpret;            // solution variable interpreter type

  template<class T>
  using ArrayQ = ShallowWaterTraits<PhysD2>::ArrayQ<T>;

  explicit QtoOutput_ShallowWater2D_V( const PDENDConvert& pde ) :
      pde_(pde), qInterpret_(pde_.getVarInterpreter()) {}

  template<class T>
  T operator()( ArrayQ<T>& q ) const
  {
    DLA::VectorS<2,T> Velocity; // velocity vector
    T V; // velocity magnitude
    qInterpret_.getVelocity( q, Velocity );

    V = sqrt( dot(Velocity,Velocity) );

    return V;
  }

private:
  const PDENDConvert& pde_;
  const QInterpret& qInterpret_;
};


template <class QType, class PDENDConvert>
class QtoOutput_ShallowWater2D_Vx
{
public:
  typedef PhysD2 PhysDim;

  typedef VarInterpret2D<QType> QInterpret;            // solution variable interpreter type

  template<class T>
  using ArrayQ = ShallowWaterTraits<PhysD2>::ArrayQ<T>;

  explicit QtoOutput_ShallowWater2D_Vx( const PDENDConvert& pde ) :
      pde_(pde), qInterpret_(pde_.getVarInterpreter()) {}

  template<class T>
  T operator()( ArrayQ<T>& q ) const
  {
    DLA::VectorS<2,T> Velocity; // velocity vector
    T Vx; // velocity x component

    qInterpret_.getVelocity( q, Velocity );

    Vx = Velocity(0);

    return Vx;
  }

private:
  const PDENDConvert& pde_;
  const QInterpret& qInterpret_;
};


template <class QType, class PDENDConvert>
class QtoOutput_ShallowWater2D_Vy
{
public:
  typedef PhysD2 PhysDim;

  typedef VarInterpret2D<QType> QInterpret;            // solution variable interpreter type

  template<class T>
  using ArrayQ = ShallowWaterTraits<PhysD2>::ArrayQ<T>;

  explicit QtoOutput_ShallowWater2D_Vy( const PDENDConvert& pde ) :
      pde_(pde), qInterpret_(pde_.getVarInterpreter()) {}

  template<class T>
  T operator()( ArrayQ<T>& q ) const
  {
    DLA::VectorS<2,T> Velocity; // velocity vector
    T Vy; // velocity x component

    qInterpret_.getVelocity( q, Velocity );

    Vy = Velocity(1);

    return Vy;
  }

private:
  const PDENDConvert& pde_;
  const QInterpret& qInterpret_;
};

} // namespace SANS



#endif /* SRC_OUTPUTFUNCTIONAL_QTOOUTPUT_SHALLOWWATER2D_H_ */
