// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_OUTPUTFUNCTIONAL_QTOOUTPUT_SHALLOWWATER1D_H_
#define SRC_OUTPUTFUNCTIONAL_QTOOUTPUT_SHALLOWWATER1D_H_


#include "pde/ShallowWater/PDEShallowWater1D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Subfunctions to compute single output from shallow water solution variables

template <class QType, class PDENDConvert>
class QtoOutput_ShallowWater1D_H
{
public:
  typedef PhysD1 PhysDim;

  typedef VarInterpret1D<QType> QInterpret;            // solution variable interpreter type

  template<class T>
  using ArrayQ = ShallowWaterTraits<PhysDim>::ArrayQ<T>;

  explicit QtoOutput_ShallowWater1D_H( const PDENDConvert& pde ) :
      pde_(pde), qInterpret_(pde_.getVarInterpreter()) {}

  template<class T>
  T operator()( ArrayQ<T>& q ) const
  {
      T H;

      qInterpret_.getH( q, H );

      return H;
  }

private:
  const PDENDConvert& pde_;
  const QInterpret& qInterpret_;
};


template <class QType, class PDENDConvert>
class QtoOutput_ShallowWater1D_V
{
public:
  typedef PhysD1 PhysDim;

  typedef VarInterpret1D<QType> QInterpret;            // solution variable interpreter type

  template<class T>
  using ArrayQ = ShallowWaterTraits<PhysDim>::ArrayQ<T>;

  explicit QtoOutput_ShallowWater1D_V( const PDENDConvert& pde ) :
      pde_(pde), qInterpret_(pde_.getVarInterpreter()) {}

  template<class T>
  T operator()( ArrayQ<T>& q ) const
  {
      DLA::VectorS<1,T> V;

      qInterpret_.getVelocity( q, V );

      return V[0];
  }

private:
  const PDENDConvert& pde_;
  const QInterpret& qInterpret_;
};

} // namespace SANS


#endif /* SRC_OUTPUTFUNCTIONAL_QTOOUTPUT_SHALLOWWATER1D_H_ */
