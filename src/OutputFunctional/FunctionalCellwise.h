// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FUNCTIONALCELLWISE_H
#define FUNCTIONALCELLWISE_H

// Cell integral output functions
#include <vector>

#include <memory> //unique_ptr

#include "Field/Field.h"
#include "Field/XField.h"
#include "Field/Element/ElementIntegral.h"

namespace SANS
{

// This class computes the integral of the specified integrand functor over each cell
// and populates an array with the cell integrals (index: [cellgroup][elem])

//----------------------------------------------------------------------------//
// Field Cell group residual
//
// topology specific group residual
//
// template parameters:
//   Topology                          element topology (e.g. Triangle)
//   IntegrandCellFunctor              integrand functor

template <class Topology, class PhysDim, class TopoDim, class ArrayQ,
          class IntegrandCellFunctor,
          class FunctionalType>
void
FunctionalCellwise_Group_Integral(
    const IntegrandCellFunctor& fcn,
    const typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology>& xfld,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology>& qfld,
    const int quadratureorder,
    std::vector<FunctionalType>& integrals )
{
  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology> QFieldCellGroupType;

  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfld.basis() );
  ElementQFieldClass qfldElem( qfld.basis() );

  // element integral
  ElementIntegral<TopoDim, Topology, FunctionalType> integral(quadratureorder);

  // loop over elements within group
  const int nelem = xfld.nElem();

  integrals.resize(nelem);

  for (int elem = 0; elem < nelem; elem++)
  {
    // copy global grid/solution DOFs to element
    xfld.getElement( xfldElem, elem );
    qfld.getElement( qfldElem, elem );

    FunctionalType result = 0;

    // cell integration for canonical element
    integral( fcn.integrand(xfldElem, qfldElem), xfldElem, result );

    integrals[elem] = result;
  }
}


// base class interface

//----------------------------------------------------------------------------//
template<class TopDim>
class FunctionalCellwise;

// base class interface
template<>
class FunctionalCellwise<TopoD1>
{
public:
  typedef TopoD1 TopoDim;

  template <class IntegrandCellFunctor,
            class PhysDim, class ArrayQ, class FunctionalType>
  static void
  integrate( const IntegrandCellFunctor& fcn,
             const Field<PhysDim, TopoDim, ArrayQ>& qfld,
             int quadratureorder[], int ngroup,
             std::vector<std::vector<FunctionalType>>& integrals )
  {
    SANS_ASSERT( ngroup == qfld.nCellGroups() );
    integrals.resize(ngroup);

    const XField<PhysDim, TopoDim>& xfld = qfld.getXField();

    // loop over element groups
    for (int group = 0; group < ngroup; group++)
    {
      // dispatch integration over elements of group
      if ( xfld.getCellGroupBase(group).topoTypeID() == typeid(Line) )
      {
        FunctionalCellwise_Group_Integral<Line, PhysDim, TopoDim, ArrayQ, IntegrandCellFunctor, FunctionalType>(
                                      fcn,
                                      xfld.template getCellGroup<Line>(group),
                                      qfld.template getCellGroup<Line>(group),
                                      quadratureorder[group],
                                      integrals[group] );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "FunctionalCellwise<TopoD1> - Unknown cell topology." );
    }
  }
};


template<>
class FunctionalCellwise<TopoD2>
{
public:
  typedef TopoD2 TopoDim;

  template <class IntegrandCellFunctor,
            class PhysDim, class ArrayQ, class FunctionalType>
  static void
  integrate( const IntegrandCellFunctor& fcn,
             const Field<PhysDim, TopoDim, ArrayQ>& qfld,
             int quadratureorder[], int ngroup,
             std::vector<std::vector<FunctionalType>>& integrals )
  {
    SANS_ASSERT( ngroup == qfld.nCellGroups() );
    integrals.resize(ngroup);

    const XField<PhysDim, TopoDim>& xfld = qfld.getXField();

    // loop over element groups
    for (int group = 0; group < ngroup; group++)
    {
      // dispatch integration over elements of group
      if ( xfld.getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
      {
        FunctionalCellwise_Group_Integral<Triangle, PhysDim, TopoDim, ArrayQ, IntegrandCellFunctor, FunctionalType>(
                                      fcn,
                                      xfld.template getCellGroup<Triangle>(group),
                                      qfld.template getCellGroup<Triangle>(group),
                                      quadratureorder[group],
                                      integrals[group] );
      }
      else if ( xfld.getCellGroupBase(group).topoTypeID() == typeid(Quad) )
      {
        FunctionalCellwise_Group_Integral<Quad, PhysDim, TopoDim, ArrayQ, IntegrandCellFunctor, FunctionalType>(
                                      fcn,
                                      xfld.template getCellGroup<Quad>(group),
                                      qfld.template getCellGroup<Quad>(group),
                                      quadratureorder[group],
                                      integrals[group] );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "FunctionalCellwise<TopoD2> - Unknown cell topology." );
    }
  }
};



template<>
class FunctionalCellwise<TopoD3>
{
public:
  typedef TopoD3 TopoDim;

  template <class IntegrandCellFunctor,
            class PhysDim, class ArrayQ, class FunctionalType>
  static void
  integrate( const IntegrandCellFunctor& fcn,
             const Field<PhysDim, TopoDim, ArrayQ>& qfld,
             int quadratureorder[], int ngroup,
             std::vector<std::vector<FunctionalType>>& integrals )
  {
    SANS_ASSERT( ngroup == qfld.nCellGroups() );
    integrals.resize(ngroup);

    const XField<PhysDim, TopoDim>& xfld = qfld.getXField();

    // loop over element groups
    for (int group = 0; group < ngroup; group++)
    {
      // dispatch integration over elements of group
      if ( xfld.getCellGroupBase(group).topoTypeID() == typeid(Tet) )
      {
        FunctionalCellwise_Group_Integral<Tet, PhysDim, TopoDim, ArrayQ, IntegrandCellFunctor, FunctionalType>(
                                      fcn,
                                      xfld.template getCellGroup<Tet>(group),
                                      qfld.template getCellGroup<Tet>(group),
                                      quadratureorder[group],
                                      integrals[group]);
      }
      else if ( xfld.getCellGroupBase(group).topoTypeID() == typeid(Hex) )
      {
        FunctionalCellwise_Group_Integral<Hex, PhysDim, TopoDim, ArrayQ, IntegrandCellFunctor, FunctionalType>(
                                      fcn,
                                      xfld.template getCellGroup<Hex>(group),
                                      qfld.template getCellGroup<Hex>(group),
                                      quadratureorder[group],
                                      integrals[group]);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "FunctionalCellwise<TopoD3> - Unknown cell topology." );
    }
  }
};

}

#endif  // FUNCTIONALCELLWISE_H
