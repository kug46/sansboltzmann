// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDBOUNDARYTRACE_LAGRANGEMULTIPLIER_H
#define INTEGRANDBOUNDARYTRACE_LAGRANGEMULTIPLIER_H

// boundary output functional: average lagrange multiplier

#include <vector>

#include "tools/SANSnumerics.h"     // Real

#include "Field/Element/Element.h"

#include "BasisFunction/TraceToCellRefCoord.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// element boundary integrand: lagrange multiplier

template <class PhysDim>
class IntegrandBoundaryTrace_LagrangeMultiplier
{
public:

  explicit IntegrandBoundaryTrace_LagrangeMultiplier( const std::vector<int>& BoundaryGroups)
    : BoundaryGroups_(BoundaryGroups) {}

  std::size_t nBoundaryGroups() const { return BoundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return BoundaryGroups_[n]; }

  template<class ArrayQ, class TopoDimTrace, class TopologyTrace,
                         class TopoDimCell,  class TopologyL>
  class Functor
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldL;

    typedef Element<ArrayQ, TopoDimCell, TopologyL> ElementQFieldL;
    typedef Element<ArrayQ, TopoDimTrace, TopologyTrace> ElementQFieldTrace;

    typedef typename ElementXFieldTrace::RefCoordType RefCoordTraceType;

    Functor( const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
             const ElementXFieldL& xfldElem,
             const ElementQFieldL& qfldElem,
             const ElementQFieldTrace& lgfldTrace ) :
             xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
             xfldElem_(xfldElem), qfldElem_(qfldElem),
             lgfldTrace_(lgfldTrace) {}

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOFElem() const { return qfldElem_.nDOF(); }
    int nDOFTrace() const { return lgfldTrace_.nDOF(); }

    // element trace integrand
    template<class T>
    void operator()( const RefCoordTraceType& sRefTrace, T& integrand ) const
    {
      ArrayQ lg;

      lgfldTrace_.eval( sRefTrace, lg );
      integrand = lg(0);
    }

  protected:
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldL& xfldElem_;
    const ElementQFieldL& qfldElem_;
    const ElementQFieldTrace& lgfldTrace_;
  };

  template<class ArrayQ, class TopoDimTrace, class TopologyTrace,
                         class TopoDimCell, class TopologyCell>
  Functor<ArrayQ, TopoDimTrace, TopologyTrace,TopoDimCell, TopologyCell>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementXField<PhysDim, TopoDimCell , TopologyCell >& xfldElem,
            const Element<ArrayQ       , TopoDimCell , TopologyCell >& qfldElem,
            const Element<ArrayQ       , TopoDimTrace, TopologyTrace>& lgfldTrace) const
  {
    return Functor<ArrayQ, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyCell>(xfldElemTrace, canonicalTrace,
                                                                                   xfldElem, qfldElem,
                                                                                   lgfldTrace);
  }


private:
  const std::vector<int> BoundaryGroups_;
};

}

#endif  // INTEGRANDBOUNDARYTRACE_LAGRANGEMULTIPLIER_H
