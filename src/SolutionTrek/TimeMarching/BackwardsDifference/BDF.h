// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SANS_BDF_H
#define SANS_BDF_H

#include "Python/PyDict.h" // Python must be included first
#include "Python/Parameter.h"

#include "tools/SANSException.h"
#include "tools/PointerSequence.h"

#include "Field/Field.h"
#include "Field/XField.h"
#include "Field/FieldSequence.h"

#include "pde/NDConvert/PDENDConvert_fwd.h"
#include "pde/NDConvert/GlobalTime.h"

#include "NonLinearSolver/NonLinearSolver.h"

#include "SolutionTrek/TimeMarching/AlgebraicEquationSet_TimeMarch.h"

namespace SANS
{

//=============================================================================
struct BDFParam : noncopyable
{
  //const ParameterDict NonLinearSolver{"NonLinearSolver", NO_DEFAULT, NewtonSolverParam::checkInputs,
  //                                    "The non-linear solver for each Backwards Difference time step"};

  const ParameterNumeric<int> order{"order", NO_DEFAULT, 1, 4, "Backwards Difference Order"};

  static void checkInputs(PyDict d);
  static BDFParam params;
};


//----------------------------------------------------------------------------//
// Backwards Difference Time Marching Scheme
//
// template parameters:
//----------------------------------------------------------------------------//
template<class NDPDEClass, class Traits, class XFieldType>
class BDF;

template<class PhysDim, class PDEClass, class Traits, class XFieldType>
class BDF< PDENDConvertSpace<PhysDim, PDEClass>, Traits, XFieldType >
{
public:
  typedef PDENDConvertSpace<PhysDim, PDEClass> NDPDEClass;
  typedef typename XFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef AlgebraicEquationSet_TimeMarch<NDPDEClass, Traits, XFieldType> AlgebraicEquationSet_TimeClass;

  typedef typename AlgebraicEquationSet_TimeClass::BaseType AlgebraicEquationSetBaseClass;
  typedef typename AlgebraicEquationSet_TimeClass::SystemMatrix SystemMatrix;
  typedef typename AlgebraicEquationSet_TimeClass::SystemVector SystemVector;
  typedef typename AlgebraicEquationSet_TimeClass::SystemNonZeroPattern SystemNonZeroPattern;

  typedef NonLinearSolver<SystemMatrix> NonLinearSolverType;

  BDF(const int BDForder,
      const Real dt,
      GlobalTime& time,
      const XFieldType& xfld,
      Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const PyDict& nonLinearSolver_dict,
      const NDPDEClass& pde,
      const QuadratureOrder& quadratureOrder,
      const std::vector<int>& CellGroups,
      AlgebraicEquationSetBaseClass& spatial)
   : BDForder_(BDForder), dt_(dt), time_(time), xfld_(xfld), qfld_(qfld),
     uIntpast_(BDForder, spatial.vectorEqSize()),
     qfldpast_(BDForder, qfld),
     force_(spatial.vectorEqSize()),
     algEqSetTime_(xfld, qfld, pde, quadratureOrder, CellGroups, weightn_, force_, spatial ),
     nonLinearSolver_(algEqSetTime_, nonLinearSolver_dict)
  {
    init();
  }

  // Use to set past solutions for BDF
  void setqfldPast(const int i, const Field<PhysDim, TopoDim, ArrayQ>& qfldPast);

  // March in time
  bool march(const int nTimeSteps);

  int nFieldPast() const { return qfldpast_.nFields(); }
        typename FieldSequence<PhysDim, TopoDim, ArrayQ>::FieldType& qfldpast(const int k)       { return qfldpast_[k]; }
  const typename FieldSequence<PhysDim, TopoDim, ArrayQ>::FieldType& qfldpast(const int k) const { return qfldpast_[k]; }

  AlgebraicEquationSet_TimeClass& getUnsteadyAlgEqnSet() { return algEqSetTime_; }

protected:
  void init();

  int BDForder_;                                      // order of the BDF
  Real dt_;                                           // time step
  Real weightn_;                                      // weight applied to the integral of the conservative variables at the current time
  std::vector<Real> weights_;                         // temporal weights

  GlobalTime& time_;                                  // the current time
  const XFieldType& xfld_;                            // grid
  Field<PhysDim, TopoDim, ArrayQ>& qfld_;             // solution field at current time
  PointerSequence<SystemVector> uIntpast_;            // previous integrals of the conservative vector
  FieldSequence<PhysDim, TopoDim, ArrayQ> qfldpast_;  // previous solutions fields. Often needed for adjoint calculations
  SystemVector force_;                                // residual forcing vector. Weighted some of past conservative state integrals

  AlgebraicEquationSet_TimeClass algEqSetTime_;       // temporal algebraic equation set

  NonLinearSolverType nonLinearSolver_;               // nonlinear solver for each time step
};

template<class PhysDim, class PDEClass, class Traits, class XFieldType>
void
BDF< PDENDConvertSpace<PhysDim, PDEClass>, Traits, XFieldType >::
setqfldPast(const int i, const Field<PhysDim, TopoDim, ArrayQ>& qfldPast)
{
  // Make sure that previous solutions are being set
  SANS_ASSERT_MSG( i >= 0 && i < BDForder_, "i=%d, BDForder_-1=%d", i, BDForder_);

  SANS_ASSERT( qfldPast.nDOF() == qfldpast_[i].nDOF() );

  for (int k = 0; k < qfldPast.nDOF(); k++)
    qfldpast_[i].DOF(k) = qfldPast.DOF(k);

  // Set the past conservative state integral
  weightn_ = 1;
  uIntpast_[i] = 0;
  algEqSetTime_.conservative_integral(qfldPast, uIntpast_[i]);
}

template<class PhysDim, class PDEClass, class Traits, class XFieldType>
void
BDF< PDENDConvertSpace<PhysDim, PDEClass>, Traits, XFieldType >::init()
{
  if (BDForder_ == 1)
  {
    weights_ = {1, -1}; //BDF1
  }
  else if (BDForder_ == 2)
  {
    weights_ = {1.5, -2, 0.5}; //BDF2
  }
  else if (BDForder_ == 3)
  {
    weights_ = {11./6., -3., 9./6., -1./3.}; //BDF3
  }
  else if (BDForder_ == 4)
  {
    weights_ = {25./12., -4., 3., -4./3., 1./4.}; //BDF4
  }
  else
    SANS_DEVELOPER_EXCEPTION( "Unknown requested BDF order = %d", BDForder_ );

  // Divide out the time step from the weights
  for (std::size_t i = 0; i < weights_.size(); i++)
    weights_[i] /= dt_;

  // Set the weight for the conservative integrand to 1
  // and initialize past integrands
  weightn_ = 1;
  for (int i = 0; i < uIntpast_.size(); i++)
  {
    uIntpast_[i] = 0;
    algEqSetTime_.conservative_integral(uIntpast_[i]);
  }
}

//---------------------------------------------------------------------------//
template<class PhysDim, class PDEClass, class Traits, class XFieldType>
bool
BDF< PDENDConvertSpace<PhysDim, PDEClass>, Traits, XFieldType >::march(const int nTimeSteps)
{
  SolveStatus status;
  SystemVector qold(algEqSetTime_.vectorStateSize());
  SystemVector q(qold.size());

  algEqSetTime_.fillSystemVector(q);

  for (int step = 0; step < nTimeSteps; step++)
  {
    qold = q;

    // compute the forcing function which is the weighted sum of previous conservative state integrals
    force_ = uIntpast_[0]*weights_[1];
    for (int i = 1; i < uIntpast_.size(); i++)
      force_ += uIntpast_[i]*weights_[i+1];

    // set the weight for the current conservative state integral
    weightn_ = weights_[0];

    // Advance the current time as the spatial residual is evaluated at n+1
    // This matters for any residual (think forcing function) that depends explicitly on time
    time_ += dt_;

    // Solve the current time step
    status = nonLinearSolver_.solve(qold, q);
    if ( status.converged == false )
      return false;

    // Update the solution field
    algEqSetTime_.setSolutionField(q);

    //rotate past integrals {u0, u1, u2} -> {u2, u0, u1}
    uIntpast_.rotate();
    qfldpast_.rotate();

    // compute the conservative state integral of the current solution
    weightn_ = 1;
    uIntpast_[0] = 0;
    algEqSetTime_.conservative_integral(uIntpast_[0]);

    // save off the current solution
    for (int k = 0; k < qfld_.nDOF(); k++)
      qfldpast_[0].DOF(k) = qfld_.DOF(k);
  }

  return true;
}

}

#endif //SANS_BDF_H
