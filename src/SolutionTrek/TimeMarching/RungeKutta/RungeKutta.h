// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SANS_RK_H
#define SANS_RK_H

#include "Python/PyDict.h" // Python must be included first
#include "Python/Parameter.h"

#include "tools/SANSException.h"
#include "tools/PointerSequence.h"

#include "Field/Field.h"
#include "Field/XField.h"

#include "pde/NDConvert/PDENDConvert_fwd.h"
#include "pde/NDConvert/GlobalTime.h"

#include "NonLinearSolver/NonLinearSolver.h"

#include "SolutionTrek/TimeMarching/AlgebraicEquationSet_TimeMarch.h"
#include "SolutionTrek/TimeMarching/AlgebraicEquationSet_TimeStep.h"

namespace SANS
{

//=============================================================================
struct RKParam : noncopyable
{
  const ParameterNumeric<int> order{"order", NO_DEFAULT, 1, 4, "Runge Kutta Order"};

  static void checkInputs(PyDict d);
  static RKParam params;
};


//----------------------------------------------------------------------------//
// Runge Kutta Time Marching Scheme
//
// template parameters:
//----------------------------------------------------------------------------//
template<class NDPDEClass, class Traits, class XFieldType>
class RK;

template<class PhysDim, class PDEClass, class Traits, class XFieldType>
class RK< PDENDConvertSpace<PhysDim, PDEClass>, Traits, XFieldType >
{
public:
  typedef PDENDConvertSpace<PhysDim, PDEClass> NDPDEClass;
  typedef typename XFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef AlgebraicEquationSet_TimeMarch<NDPDEClass, Traits, XFieldType> AlgebraicEquationSet_TimeMarchClass;
  typedef AlgebraicEquationSet_TimeStep<NDPDEClass, Traits, XFieldType> AlgebraicEquationSet_TimeStepClass;

  typedef typename AlgebraicEquationSet_TimeMarchClass::BaseType AlgebraicEquationSetBaseClass;
  typedef typename AlgebraicEquationSet_TimeMarchClass::SystemMatrix SystemMatrix;
  typedef typename AlgebraicEquationSet_TimeMarchClass::SystemVector SystemVector;
  typedef typename AlgebraicEquationSet_TimeMarchClass::SystemNonZeroPattern SystemNonZeroPattern;

  typedef NonLinearSolver<SystemMatrix> NonLinearSolverType;

  RK( const int RKorder,
      const int nRKstages,
      const int RKtype,
      const Real dt,
      GlobalTime& time,
      const XFieldType& xfld,
      Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const PyDict& nonLinearSolver_dict,
      const NDPDEClass& pde,
      const QuadratureOrder& quadratureOrder,
      const std::vector<Real>& tol,
      const std::vector<int>& CellGroups,
      AlgebraicEquationSetBaseClass& spatial)
   : RKorder_(RKorder), nRKstages_(nRKstages), RKtype_(RKtype), dt_(dt), weightn_(1), weightU_(1),
     time_(time), xfld_(xfld), qfld_(qfld),
     uIntStages_(nRKstages, spatial.vectorStateSize()),
     fStages_(nRKstages, spatial.vectorEqSize()),
     qStages_(nRKstages, spatial.vectorStateSize()),
     uIntOld_(spatial.vectorEqSize()),
     algEqSetTimeMarch_(xfld, qfld, pde, quadratureOrder, CellGroups, weightn_, force_, spatial ),
     algEqSetTimeStep_(xfld, qfld, pde, quadratureOrder, tol, CellGroups, weightU_, forceU_ ),
     spatial_(spatial),
     force_(spatial.vectorEqSize()),
     forceU_(algEqSetTimeStep_.vectorEqSize()),
     nonLinearSolverf_(algEqSetTimeMarch_, nonLinearSolver_dict),
     nonLinearSolverU_(algEqSetTimeStep_, nonLinearSolver_dict)
  {
    SANS_ASSERT_MSG( spatial.indexPDE() == iPDE_, "Assuming the PDE index is 0!" );
    init();
  }

  // March in time
  bool march(const int nTimeSteps);

protected:
  void init();

  int RKorder_;                                       // order of the RK
  int nRKstages_;                                     // number of stages
  int RKtype_;                                        // algebraically stable = 0, L-stable and stiffly accurate = 1

  Real dt_;                                           // time step
  Real weightn_;                                      // weight applied to the integral of the conservative variables at the current time
  Real weightU_;                                      // weight applied to time term when advancing the solution

  std::vector<std::vector<Real>> weightsA_;            // stage weights a_ij from Butcher table
  std::vector<Real> weightsB_;                         // stage weights b_i from Butcher table
  std::vector<Real> weightsC_;                         // stage weights c_i from Butcher table

  GlobalTime& time_;                                   // the current time
  const XFieldType& xfld_;                             // grid
  Field<PhysDim, TopoDim, ArrayQ>& qfld_;              // solution field at current time
  PointerSequence<SystemVector> uIntStages_;           // integrals of the conservative vector at each stage
  PointerSequence<SystemVector> fStages_;              // spatial residual at each stage
  PointerSequence<SystemVector> qStages_;              // solutions fields at each stage
  SystemVector                  uIntOld_;              // previous integral of the conservative vector

  AlgebraicEquationSet_TimeMarchClass algEqSetTimeMarch_; // temporal algebraic equation set with spatial residual
  AlgebraicEquationSet_TimeStepClass algEqSetTimeStep_;   // temporal algebraic equation set for advancing the solution
  AlgebraicEquationSetBaseClass& spatial_;                // spatial algebraic equation set

  SystemVector force_;                                 // residual forcing vector. Weighted some of past conservative state integrals
  SystemVector forceU_;                                // residual forcing vector for computing the updated primary solution

  NonLinearSolverType nonLinearSolverf_;                // nonlinear solver for each time step
  NonLinearSolverType nonLinearSolverU_;               // nonlinear solver the final time step advancement

  const int iPDE_=0;                                   // index to for PDE equations in the residual vector
};


template<class PhysDim, class PDEClass, class Traits, class XFieldType>
void
RK< PDENDConvertSpace<PhysDim, PDEClass>, Traits, XFieldType >::init()
{
  Real clambda;

  weightsA_.resize(10, std::vector<Real>(10, 0.));
  weightsB_.resize(10, 0.);
  weightsC_.resize(10, 0.);

  if (RKtype_ <= 0) //Algebraically stable
  {
    if ((RKorder_ == 1) && (nRKstages_ == 1))
    {
      //Algebraically Stable RK1
      clambda = 1.;

      weightsA_[0][0] = clambda;
      weightsB_[0]    = clambda;
      weightsC_[0]    = clambda;
    }
    else if ((RKorder_ == 2) && (nRKstages_ == 2))
    {
      //Algebraically Stable RK2
      clambda = (2.-sqrt(2.))/2.;

      weightsA_[0][0] = clambda;
      weightsA_[0][1] = 0.;
      weightsA_[1][0] = 1.-2.*clambda;
      weightsA_[1][1] = clambda;

      weightsB_[0] = 0.5;
      weightsB_[1] = 0.5;

      weightsC_[0] = clambda;
      weightsC_[1] = 1.-clambda;
    }
    else if ((RKorder_ == 3) && (nRKstages_ == 3))
    {
      //Algebraically Stable RK3
      clambda = 0.435866521508459;

      Real d1, d2;
      Real c2, c3;
      Real b1, b2, b3;
      Real a21, a31, a32;

      d1 = 2.; //Must be greater than 1.77429424707279

      b1 = (d1*(1.-2.*clambda) + (1./6.))/((2.*clambda - 1.)*(2.*clambda - 1. -2.*d1));
      b2 = (clambda*clambda - clambda + (1./6.))/((clambda-0.5)*(clambda-0.5)-d1*d1);
      b3 = (d1*(2.*clambda-1.) + (1./6.))/((2.*clambda - 1.)*(2.*clambda - 1. +2.*d1));

      d2 = (0.5-clambda-d1)*(b2/b3);

      a21 = 0.5-clambda+d1;
      a31 = 1. - 2.*clambda -d2;
      a32 = d2;

      c2 = 0.5+d1;
      c3 = 1.-clambda;

      weightsA_[0][0] = clambda;
      weightsA_[0][1] = 0.;
      weightsA_[0][2] = 0.;

      weightsA_[1][0] = a21;
      weightsA_[1][1] = clambda;
      weightsA_[1][2] = 0.;

      weightsA_[2][0] = a31;
      weightsA_[2][1] = a32;
      weightsA_[2][2] = clambda;

      weightsB_[0] = b1;
      weightsB_[1] = b2;
      weightsB_[2] = b3;

      weightsC_[0] = clambda;
      weightsC_[1] = c2;
      weightsC_[2] = c3;
    }
    else if ((RKorder_ == 4) && (nRKstages_ == 4))
    {
      //Algebraically Stable RK3
      clambda = 0.5728160624821349;

      Real d1, d2, d3;
      Real a21, a31, a32;
      Real a41, a42, a43;
      Real b2, c2, c3;

      d2 = -(clambda*(clambda - (1./3.)))/(2.*clambda*clambda - clambda + (1./6.));

      d1 = clambda*clambda - (clambda/2.) + (1./12.);
      d3 = clambda*clambda*clambda - (3./2.)*clambda*clambda + (1./2.)*clambda - (1./24.);

      b2 = (d1*d1)/(2.*clambda*(clambda - (1./3.))*(clambda - (1./4.)));

      a21 = (clambda*((1./3.)-clambda))/(2.*d1);

      a31 = (((1./2.)-clambda-(d3/d1))*(clambda*(clambda-(1./3.))) + 8.*d3*(clambda - (1./4.)))/(clambda*(clambda - (1./3.)));
      a32 = -(8.*d3*(clambda - (1./4.)))/(clambda*(clambda-(1./3.)));

      a42 = ((clambda*clambda - clambda + (1./6.))*(1./2.) + (d3/d2))/(d2*((1./2.)-b2));
      a41 = 1.-2.*clambda-a42-(d1/((4.*clambda-1.)*(b2-(1./2.))));
      a43 = d1/((4.*clambda -1.)*(b2-(1./2.)));

      c2 = (clambda*(clambda-(1./2.))*(clambda-(1./2.)))/d1;
      c3 = (1./2.)-(d3/d1);

      weightsA_[0][0] = clambda;
      weightsA_[0][1] = 0.;
      weightsA_[0][2] = 0.;
      weightsA_[0][3] = 0.;

      weightsA_[1][0] = a21;
      weightsA_[1][1] = clambda;
      weightsA_[1][2] = 0.;
      weightsA_[1][3] = 0.;

      weightsA_[2][0] = a31;
      weightsA_[2][1] = a32;
      weightsA_[2][2] = clambda;
      weightsA_[2][3] = 0.;

      weightsA_[3][0] = a41;
      weightsA_[3][1] = a42;
      weightsA_[3][2] = a43;
      weightsA_[3][3] = clambda;

      weightsB_[0] = 0.5-b2;
      weightsB_[1] = b2;
      weightsB_[2] = b2;
      weightsB_[3] = 0.5-b2;

      weightsC_[0] = clambda;
      weightsC_[1] = c2;
      weightsC_[2] = c3;
      weightsC_[3] = 1.-clambda;
    }
    else
    {
      SANS_DEVELOPER_EXCEPTION( "Unknown requested RK order or number of stages" );
    }
  }
  else if (RKtype_ == 1) //L-stable and stiffly accurate
  {
    if ((RKorder_ == 1) && (nRKstages_ == 1))
    {
      //L-stable and stiffly accurate RK1
      clambda = 1.;

      weightsA_[0][0] = clambda;
      weightsB_[0]    = clambda;
      weightsC_[0]    = clambda;
    }
    else if ((RKorder_ == 2) && (nRKstages_ == 2))
    {
      //L-stable and stiffly accurate RK2
      clambda = (2.-sqrt(2.))/2.;

      weightsA_[0][0] = clambda;
      weightsA_[0][1] = 0.;
      weightsA_[1][0] = 1.-clambda;
      weightsA_[1][1] = clambda;

      weightsB_[0] = 1.-clambda;
      weightsB_[1] = clambda;

      weightsC_[0] = clambda;
      weightsC_[1] = 1.;
    }
    else if ((RKorder_ == 3) && (nRKstages_ == 3))
    {
      //L-stable and stiffly accurate RK3
      clambda = 0.435866521508459;

      Real calpha, cbeta;
      Real b2, c2;

      calpha = 1. -4.*clambda + 2.*clambda*clambda;
      cbeta  = -1. + 6.*clambda -9.*clambda*clambda + 3.*clambda*clambda*clambda;

      b2 = (-3.*calpha*calpha)/(4.*cbeta);
      c2 = (2. - 9.*clambda + 6.*clambda*clambda)/(3.*calpha);

      weightsA_[0][0] = clambda;
      weightsA_[0][1] = 0.;
      weightsA_[0][2] = 0.;

      weightsA_[1][0] = c2-clambda;
      weightsA_[1][1] = clambda;
      weightsA_[1][2] = 0.;

      weightsA_[2][0] = 1.-b2-clambda;
      weightsA_[2][1] = b2;
      weightsA_[2][2] = clambda;

      weightsB_[0] = 1.-b2-clambda;
      weightsB_[1] = b2;
      weightsB_[2] = clambda;

      weightsC_[0] = clambda;
      weightsC_[1] = c2;
      weightsC_[2] = 1.;
    }
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION( "Unknown requested RK type" );
  }


  // Set the weight for the conservative integrand to 1
  // and initialize past integrands
  weightn_ = 1;
}

//---------------------------------------------------------------------------//
template<class PhysDim, class PDEClass, class Traits, class XFieldType>
bool
RK< PDENDConvertSpace<PhysDim, PDEClass>, Traits, XFieldType >::march(const int nTimeSteps)
{
  Real told;
  Real weightDiag;

  // solution vector for total system
  SystemVector q(algEqSetTimeMarch_.vectorStateSize());

  // solution vector for only updating the temporal term
  SystemVector qt(algEqSetTimeStep_.vectorStateSize());

  SolveStatus status;

  for (int step = 0; step < nTimeSteps; step++)
  {
    algEqSetTimeMarch_.fillSystemVector(q);

    told = time_;

    // Initialize the conservative integral u_n
    weightn_ = 1;
    uIntOld_ = 0;
    algEqSetTimeMarch_.conservative_integral(uIntOld_);

    for (int i = 0; i < nRKstages_; i++)
    {
      weightDiag = weightsA_[i][i];
      weightn_ = 1./(weightDiag*dt_);

      time_ = told + weightsC_[i]*dt_;

      force_ = -uIntOld_*weightn_;
      for (int j = 0; j < i; j++)
      {
         force_ += ((weightsA_[i][j])/weightDiag)*fStages_[j];
      }

      //q_n and q_i
      status = nonLinearSolverf_.solve(q, qStages_[i]);
      if ( status.converged == false )
      {
        return false;
      }

      //Spatial residual
      fStages_[i] = 0;
      //q_i and f_i
      spatial_.residual(qStages_[i], fStages_[i]);
    }

    // the solution contains the last qStage
    algEqSetTimeStep_.fillSystemVector(qt);

    // Advance the current time and update the solution field
    time_ = told + dt_;

    // trigger that only the time term is solved
    forceU_[0] = -uIntOld_[iPDE_];
    for (int i=0; i < nRKstages_; i++)
    {
      forceU_[0] += weightsB_[i]*dt_*fStages_[i][iPDE_];
    }

    // solve the for the solution time at q_n+1
    status = nonLinearSolverU_.solve(qt, qt);
    if ( status.converged == false )
      return false;

    // Update solution
    algEqSetTimeStep_.setSolutionField(qt);

    //debugging
    //std::cout << "time  " << time_ << " qfld " << qfld_.DOF(0) << std::endl;
  }


  return true;
}

}

#endif //SANS_RK_H
