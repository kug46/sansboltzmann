// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ALGEBRAICEQUATIONSET_TIMEMARCH_H
#define ALGEBRAICEQUATIONSET_TIMEMARCH_H

#include <type_traits>

#include "tools/SANSnumerics.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/tools/norm.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/tools/norm.h"

#include "Field/Field.h"
#include "Field/XField.h"

#include "LinearAlgebra/AlgebraicEquationSetBase.h"
#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/QuadratureOrder.h"

#include "Discretization/Galerkin/ResidualCell_Galerkin.h"
#include "Discretization/Galerkin/JacobianCell_Galerkin.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Conservative.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template<class NDPDEClass, class Traits, class XFieldType>
class AlgebraicEquationSet_TimeMarch
    : public AlgebraicEquationSetTraits<typename NDPDEClass::template MatrixQ<Real>,
                                        typename NDPDEClass::template ArrayQ<Real>,
                                        Traits>::AlgebraicEquationSetBaseClass
{
public:
  typedef IntegrandCell_Galerkin_Conservative<NDPDEClass> IntegrandCell;

  typedef AlgebraicEquationSetTraits<typename NDPDEClass::template MatrixQ<Real>,
                                     typename NDPDEClass::template ArrayQ<Real>,
                                     Traits> TraitsType;

  typedef typename TraitsType::AlgebraicEquationSetBaseClass BaseType;

  typedef typename NDPDEClass::PhysDim PhysDim;
  typedef typename XFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDPDEClass::template MatrixQ<Real> MatrixQ;

  typedef typename TraitsType::VectorSizeClass VectorSizeClass;
  typedef typename TraitsType::MatrixSizeClass MatrixSizeClass;

  typedef typename TraitsType::SystemMatrix SystemMatrix;
  typedef typename TraitsType::SystemVector SystemVector;
  typedef typename TraitsType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename TraitsType::SystemMatrixView SystemMatrixView;
  typedef typename TraitsType::SystemVectorView SystemVectorView;
  typedef typename TraitsType::SystemNonZeroPatternView SystemNonZeroPatternView;

  typedef typename BaseType::LinesearchData LinesearchData;

  AlgebraicEquationSet_TimeMarch(const XFieldType& xfld,
                           Field<PhysDim, TopoDim, ArrayQ>& qfld,
                           const NDPDEClass& pde,
                           const QuadratureOrder& quadratureOrder,
                           const std::vector<int>& CellGroups,
                           const Real& weight,
                           const SystemVectorView& force,
                           BaseType& spatial)
   : xfld_(xfld), qfld_(qfld),
     fcnCell_( pde, CellGroups, weight ), spatial_(spatial), force_(force),
     iPDE_(spatial_.indexPDE()), iq_(spatial_.indexQ()),
     quadratureOrder_(quadratureOrder),
     quadratureOrderMin_(get<-1>(xfld), 0)
  {
    isStaticCondensed_ = spatial.isStaticCondensed();
  }

  virtual ~AlgebraicEquationSet_TimeMarch() {}

  using BaseType::residual;
  using BaseType::jacobian;
  using BaseType::jacobianTranspose;

  //Computes the weighted integral of the conservative vector
  void conservative_integral(SystemVectorView& rsd);
  void conservative_integral(const Field<PhysDim, TopoDim, ArrayQ>& qfld, SystemVectorView& rsd);

  //Computes the residual
  virtual void residual(SystemVectorView& rsd) override;

  //Fills jacobian or the non-zero pattern of a jacobian
  virtual void jacobian(SystemMatrixView& mtx       ) override
  {
    spatial_.jacobian(mtx);
    this->template jacobian<SystemMatrixView&>(mtx, quadratureOrder_ );
  }
  virtual void jacobian(SystemNonZeroPatternView& nz) override
  {
    spatial_.jacobian(nz);
    this->template jacobian<SystemNonZeroPatternView&>(nz, quadratureOrderMin_ );
  }

  //Fills jacobian transpose or the non-zero pattern transpose of a jacobian transpose
  virtual void jacobianTranspose(SystemMatrixView& mtxT       ) override
  {
    spatial_.jacobian(mtxT);
    jacobian(Transpose(mtxT), quadratureOrder_ );
  }
  virtual void jacobianTranspose(SystemNonZeroPatternView& nzT) override
  {
    spatial_.jacobian(nzT);
    jacobian(Transpose( nzT), quadratureOrderMin_ );
  }

  // Compute Residual Norm
  virtual std::vector<std::vector<Real>> residualNorm( const SystemVectorView& rsd) const override
  {
    return spatial_.residualNorm(rsd);
  }

  //Convergence check of the residual
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm) const override
  {
    return spatial_.convergedResidual(rsdNorm);
  }

  //Convergence check of the residual
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm, int iEq, int iMon) const override
  {
    return spatial_.convergedResidual(rsdNorm, iEq, iMon);
  }

  // check if residual decreased
  virtual bool decreasedResidual( const std::vector<std::vector<Real>>& rsdNormOld,
                                  const std::vector<std::vector<Real>>& rsdNormNew) const override
  {
    return spatial_.decreasedResidual(rsdNormOld, rsdNormNew);
  }

  virtual void printDecreaseResidualFailure(const std::vector<std::vector<Real>>& rsdNorm, std::ostream& os = std::cout) const override
  {
    spatial_.printDecreaseResidualFailure(rsdNorm, os);
  }

  virtual void dumpSolution(const std::string& filename) const override
  {
    spatial_.dumpSolution(filename);
  }

  //Translates the system vector into a solution field
  virtual void setSolutionField(const SystemVectorView& q) override { spatial_.setSolutionField(q); }

  //Translates the solution field into a system vector
  virtual void fillSystemVector(SystemVectorView& q) const override { spatial_.fillSystemVector(q); }

  // Returns the vector an matrix sizes needed for the linear algebra system
  virtual VectorSizeClass vectorEqSize() const override    { return spatial_.vectorEqSize();    } // vector for equations (rows in matrixSize)
  virtual VectorSizeClass vectorStateSize() const override { return spatial_.vectorStateSize(); } // vector for state DOFs (columns in matrixSize)
  virtual MatrixSizeClass matrixSize() const override      { return spatial_.matrixSize();      }

  // Gives the PDE and solution indices in the system
  virtual int indexPDE() const override { SANS_ASSERT(iPDE_ == spatial_.indexPDE()); return iPDE_; }
  virtual int indexQ() const override { SANS_ASSERT(iq_ == spatial_.indexQ()); return iq_; }

  // Checks to see if proposed solution is physical
  virtual bool isValidStateSystemVector(SystemVectorView& q) override;

  // Returns the side of the residual norm outer vector
  virtual int nResidNorm() const override;

  // Converts processor local indexing to a processor continuous indexing
  virtual std::vector<GlobalContinuousMap> continuousGlobalMap() const override { return spatial_.continuousGlobalMap(); }

  // MPI communicator for this algebraic equation set
  virtual std::shared_ptr<mpi::communicator> comm() const override { return spatial_.comm(); }

  virtual void syncDOFs_MPI() override { spatial_.syncDOFs_MPI(); }

  // Dump the localized linesearch parameter info (for debugging purposes)
  virtual bool updateLinesearchDebugInfo(const Real& s, const SystemVectorView& rsd,
                                         LinesearchData& pResData,
                                         LinesearchData& pStepData) const override;

  virtual void dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                                       const LinesearchData& pStepData) const override;


  // Are we having machine precision issues
  virtual bool atMachinePrecision(const SystemVectorView& q, const std::vector<std::vector<Real>>& R0norm) override
  {
    return spatial_.atMachinePrecision(q, R0norm);
  }

protected:
  template<class SparseMatrixType>
  void jacobian(SparseMatrixType mtx, const QuadratureOrder& quadratureOrder );

  const XFieldType& xfld_;
  Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const IntegrandCell fcnCell_;
  BaseType& spatial_;
  const SystemVectorView& force_;

  const int iPDE_;
  const int iq_;

  const QuadratureOrder quadratureOrder_;
  const QuadratureOrder quadratureOrderMin_;

  using BaseType::isStaticCondensed_;
};

template<class NDPDEClass, class Traits, class XFieldType>
void
AlgebraicEquationSet_TimeMarch<NDPDEClass, Traits, XFieldType>::
conservative_integral(SystemVectorView& rsd)
{
  conservative_integral(qfld_, rsd);
}

template<class NDPDEClass, class Traits, class XFieldType>
void
AlgebraicEquationSet_TimeMarch<NDPDEClass, Traits, XFieldType>::
conservative_integral(const Field<PhysDim, TopoDim, ArrayQ>& qfld, SystemVectorView& rsd)
{
  IntegrateCellGroups<TopoDim>::integrate( ResidualCell_Galerkin(fcnCell_, rsd(iq_)),
                                           xfld_, qfld, quadratureOrder_.cellOrders.data(), quadratureOrder_.cellOrders.size() );
}

template<class NDPDEClass, class Traits, class XFieldType>
void
AlgebraicEquationSet_TimeMarch<NDPDEClass, Traits, XFieldType>::
residual(SystemVectorView& rsd)
{
  // Compute the spatial residual
  spatial_.residual(rsd);

  // Add the integral of the conservative state vector
  conservative_integral(rsd);

  // Add the forcing terms to the residual
  rsd += force_;
}


template<class NDPDEClass, class Traits, class XFieldType>
template<class SparseMatrixType>
void
AlgebraicEquationSet_TimeMarch<NDPDEClass, Traits, XFieldType>::
jacobian(SparseMatrixType jac, const QuadratureOrder& quadratureOrder)
{
  // Get the matrix type, this could be a reference or temporary variable depending on SparseMatrixType
  typedef typename std::result_of<SparseMatrixType(const int, const int)>::type Matrix;

  Matrix jacPDE_q  = jac(iPDE_,iq_);

  IntegrateCellGroups<TopoDim>::integrate( JacobianCell_Galerkin(fcnCell_, jacPDE_q),
                                           xfld_, qfld_, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

}

template<class NDPDEClass, class Traits, class XFieldType>
bool
AlgebraicEquationSet_TimeMarch<NDPDEClass, Traits, XFieldType>::
isValidStateSystemVector(SystemVectorView& q)
{
  return spatial_.isValidStateSystemVector(q);
}

template<class NDPDEClass, class Traits, class XFieldType>
int
AlgebraicEquationSet_TimeMarch<NDPDEClass, Traits, XFieldType>::
nResidNorm() const
{
  return spatial_.nResidNorm();
}

template<class NDPDEClass, class Traits, class XFieldType>
bool
AlgebraicEquationSet_TimeMarch<NDPDEClass, Traits, XFieldType>::
updateLinesearchDebugInfo(const Real& s, const SystemVectorView& rsd,
                          LinesearchData& pResData,
                          LinesearchData& pStepData) const
{
  return spatial_.updateLinesearchDebugInfo(s, rsd, pResData, pStepData);
}

template<class NDPDEClass, class Traits, class XFieldType>
void
AlgebraicEquationSet_TimeMarch<NDPDEClass, Traits, XFieldType>::
dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                        const LinesearchData& pStepData) const
{
  spatial_.dumpLinesearchDebugInfo(filenamebase, nonlinear_iter, pStepData);
}


} //namespace SANS

#endif //ALGEBRAICEQUATIONSET_TIMEMARCH_H
