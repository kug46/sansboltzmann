// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ALGEBRAICEQUATIONSET_TIMESTEP_H
#define ALGEBRAICEQUATIONSET_TIMESTEP_H

#include <type_traits>

#include "tools/SANSnumerics.h"
#include "tools/KahanSum.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Field/XField.h"
#include "Field/Field.h"
#include "Field/output_Tecplot.h"

#include "Discretization/AlgebraicEquationSet_Debug.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/QuadratureOrder.h"

#include "Discretization/Galerkin/ResidualCell_Galerkin.h"
#include "Discretization/Galerkin/JacobianCell_Galerkin.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Conservative.h"

// Parameter jacobians
//#include "Discretization/Galerkin/JacobianCell_Galerkin_Param.h"

#include "Discretization/isValidState/isValidStateCell.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#include <boost/serialization/vector.hpp>
#include "tools/plus_std_vector.h"
#endif

namespace SANS
{

//----------------------------------------------------------------------------//
template<class NDPDEClass, class Traits, class XFieldType>
class AlgebraicEquationSet_TimeStep : public AlgebraicEquationSet_Debug<NDPDEClass, Traits>
{
public:
  typedef typename NDPDEClass::PhysDim PhysDim;
  typedef typename XFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDPDEClass::template MatrixQ<Real> MatrixQ;

  typedef AlgebraicEquationSetTraits<MatrixQ, ArrayQ, Traits> TraitsType;

  typedef AlgebraicEquationSet_Debug<NDPDEClass, Traits> DebugBaseType;
  typedef typename TraitsType::AlgebraicEquationSetBaseClass BaseType;

  typedef typename TraitsType::VectorSizeClass VectorSizeClass;
  typedef typename TraitsType::MatrixSizeClass MatrixSizeClass;

  typedef typename TraitsType::SystemMatrix SystemMatrix;
  typedef typename TraitsType::SystemVector SystemVector;
  typedef typename TraitsType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename TraitsType::SystemMatrixView SystemMatrixView;
  typedef typename TraitsType::SystemVectorView SystemVectorView;
  typedef typename TraitsType::SystemNonZeroPatternView SystemNonZeroPatternView;

  typedef typename BaseType::LinesearchData LinesearchData;

  template<class ArrayQT>
  using SystemVectorTemplate = typename TraitsType::template SystemVectorTemplate<ArrayQT>;

  typedef IntegrandCell_Galerkin_Conservative<NDPDEClass> IntegrandCellClass;

  AlgebraicEquationSet_TimeStep(const XFieldType& xfld,
                                Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                const NDPDEClass& pde,
                                const QuadratureOrder& quadratureOrder,
                                const std::vector<Real>& tol,
                                const std::vector<int>& CellGroups,
                                const Real& weight,
                                const SystemVectorView& force ) :
    DebugBaseType(pde, tol),
    fcnCell_(pde, CellGroups, weight),
    xfld_(xfld),
    qfld_(qfld),
    pde_(pde),
    quadratureOrder_(quadratureOrder),
    quadratureOrderMin_(get<-1>(xfld), 0),
    force_(force)
  {
  }

  virtual ~AlgebraicEquationSet_TimeStep() {}

  using BaseType::residual;
  using BaseType::jacobian;
  using BaseType::jacobianTranspose;

  //Computes the residual
  virtual void residual(SystemVectorView& rsd) override;

  //Fills jacobian or the non-zero pattern of a jacobian
  virtual void jacobian(SystemMatrixView& mtx       ) override { this->template jacobian<        SystemMatrixView&>(mtx, quadratureOrder_ );    }
  virtual void jacobian(SystemNonZeroPatternView& nz) override { this->template jacobian<SystemNonZeroPatternView&>( nz, quadratureOrderMin_ ); }

  //Fills jacobian transpose or the non-zero pattern transpose of a jacobian transpose
  virtual void jacobianTranspose(SystemMatrixView& mtxT       ) override { jacobian(Transpose(mtxT), quadratureOrder_ );    }
  virtual void jacobianTranspose(SystemNonZeroPatternView& nzT) override { jacobian(Transpose( nzT), quadratureOrderMin_ ); }

  // Used to compute jacobians wrt. parameters
  template<int iParam, class SparseMatrixType>
  void jacobianParam(SparseMatrixType& jac, const QuadratureOrder& quadratureOrder, int ip ) const;

  //Evaluate Residual Norm
  virtual std::vector<std::vector<Real>> residualNorm( const SystemVectorView& rsd ) const override
  {
    const int nRSDPDE = rsd[iPDE].m();
    const int nMon = pde_.nMonitor();

    DLA::VectorD<Real> rsdPDEtmp(nMon);

    rsdPDEtmp = 0;

    std::vector<std::vector<Real>> rsdNorm(1, std::vector<Real>(nMon, 0));
    std::vector<KahanSum<Real>> rsdNormKahan(nMon, 0);

    //compute residual norm
    //HACKED Simple L2 norm
    //TODO: Allow for non-L2 norms
    for (int n = 0; n < nRSDPDE; n++)
    {
      pde_.interpResidVariable(rsd[iPDE][n], rsdPDEtmp);

      for (int j = 0; j < nMon; j++)
        rsdNormKahan[j] += pow(rsdPDEtmp[j],2);
    }

    for (int j = 0; j < nMon; j++)
      rsdNorm[iPDE][j] = (Real)rsdNormKahan[j];

#ifdef SANS_MPI
    rsdNorm[iPDE] = boost::mpi::all_reduce(*qfld_.comm(), rsdNorm[iPDE], std::plus<std::vector<Real>>());
#endif

    for (int j = 0; j < nMon; j++)
      rsdNorm[iPDE][j] = sqrt(rsdNorm[iPDE][j]);

    return rsdNorm;
  }

  //provides info about the residuals
  virtual void residualInfo(std::vector<std::string>& titles, std::vector<int>& idx) const override
  {
    titles = {"PDE : "};
    idx = {iPDE};
  }

  //Translates the system vector into a solution field
  virtual void setSolutionField(const SystemVectorView& q) override;

  //Translates the solution field into a system vector
  virtual void fillSystemVector(SystemVectorView& q) const override;

  // Returns the vector an matrix sizes needed for the linear algebra system
  virtual VectorSizeClass vectorEqSize() const override;    // vector for equations (rows in matrixSize)
  virtual VectorSizeClass vectorStateSize() const override; // vector for state DOFs (columns in matrixSize)
  virtual MatrixSizeClass matrixSize() const override;

  // Gives the PDE and solution indices in the system
  virtual int indexPDE() const override { return iPDE; }
  virtual int indexQ() const override { return iq; }

  // Checks to see if proposed solution is physical
  virtual bool isValidStateSystemVector(SystemVectorView& q) override;

  // Returns the side of the residual norm outer vector
  virtual int nResidNorm() const override;

  // Converts processor local indexing to a processor continuous indexing
  virtual std::vector<GlobalContinuousMap> continuousGlobalMap() const override { return {qfld_.continuousGlobalMap()}; }

  // MPI communicator for this algebraic equation set
  virtual std::shared_ptr<mpi::communicator> comm() const override { return get<-1>(xfld_).comm(); }

  virtual void syncDOFs_MPI() override { qfld_.syncDOFs_MPI_Cached(); }

  // Dump the localized linesearch parameter info (for debugging purposes)
  virtual void dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                                       const LinesearchData& pStepData) const override;

  // Indexes to order the equations and the solution vectors
  static const int iPDE = 0;
  static const int iq = 0;

protected:
  template<class SparseMatrixType>
  void jacobian( SparseMatrixType mtx, const QuadratureOrder& quadratureOrder );

  IntegrandCellClass fcnCell_;

  const XFieldType& xfld_;
  Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const NDPDEClass& pde_;
  const QuadratureOrder quadratureOrder_;
  const QuadratureOrder quadratureOrderMin_;

  const SystemVectorView& force_;
};

template<class NDPDEClass, class Traits, class XFieldType>
void
AlgebraicEquationSet_TimeStep<NDPDEClass, Traits, XFieldType>::
residual(SystemVectorView& rsd)
{
  IntegrateCellGroups<TopoDim>::integrate( ResidualCell_Galerkin(fcnCell_, rsd(iPDE)),
                                           xfld_, qfld_, quadratureOrder_.cellOrders.data(), quadratureOrder_.cellOrders.size() );

  // Add the forcing terms to the residual
  rsd += force_;
}


template<class NDPDEClass, class Traits, class XFieldType>
template<class SparseMatrixType>
void
AlgebraicEquationSet_TimeStep<NDPDEClass, Traits, XFieldType>::
jacobian(SparseMatrixType jac, const QuadratureOrder& quadratureOrder)
{
  SANS_ASSERT(jac.m() > iPDE);
  SANS_ASSERT(jac.n() > iq);

  // Get the matrix type, this could be a reference or temporary variable depending on SparseMatrixType
  typedef typename std::result_of<SparseMatrixType(const int, const int)>::type Matrix;

  Matrix jacPDE_q  = jac(iPDE,iq);

  IntegrateCellGroups<TopoDim>::integrate( JacobianCell_Galerkin(fcnCell_, jacPDE_q),
                                           xfld_, qfld_, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
}

template<class NDPDEClass, class Traits, class XFieldType>
bool
AlgebraicEquationSet_TimeStep<NDPDEClass, Traits, XFieldType>::
isValidStateSystemVector(SystemVectorView& q)
{
  bool isValidState = true;

  // Update the solution field
  setSolutionField(q);

  IntegrateCellGroups<TopoDim>::integrate( isValidStateCell(pde_, fcnCell_.cellGroups(), isValidState),
                                             xfld_, qfld_, quadratureOrder_.cellOrders.data(), quadratureOrder_.cellOrders.size() );

  return isValidState;
}

template<class NDPDEClass, class Traits, class XFieldType>
template<int iParam, class SparseMatrixType>
void
AlgebraicEquationSet_TimeStep<NDPDEClass, Traits, XFieldType>::
jacobianParam(SparseMatrixType& jac, const QuadratureOrder& quadratureOrder, int ip ) const
{
  SANS_DEVELOPER_EXCEPTION("Parameter Jacobian for time steps is not implemented");
#if 0
  SANS_ASSERT(jac.m() >= 2);
  SANS_ASSERT(jac.n() > ip);

  // Get the matrix type, this could be a reference or temporary variable depending on SparseMatrixType
  typedef typename std::result_of<SparseMatrixType(const int, const int)>::type Matrix;

  Matrix jacPDE_p  = jac(iPDE,ip);

  typedef SurrealS<NDPDEClass::N> SurrealClass;

  IntegrateCellGroups<TopoDim>::integrate(
      JacobianCell_Galerkin_Param<SurrealClass, iParam>(fcnCell_, jacPDE_p),
      xfld_, qfld_,
      quadratureOrder_.cellOrders.data(),
      quadratureOrder_.cellOrders.size() );
#endif
}


template<class NDPDEClass, class Traits, class XFieldType>
void
AlgebraicEquationSet_TimeStep<NDPDEClass, Traits, XFieldType>::setSolutionField(const SystemVectorView& q)
{
  // Copy the solution from the linear algebra vector to the field variables
  const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();
  SANS_ASSERT( nDOFPDE == q[iq].m() );
  for (int k = 0; k < nDOFPDE; k++)
    qfld_.DOF(k) = q[iq][k];
}

template<class NDPDEClass, class Traits, class XFieldType>
void
AlgebraicEquationSet_TimeStep<NDPDEClass, Traits, XFieldType>::fillSystemVector(SystemVectorView& q) const
{
  // Copy the solution from the field variables to the linear algebra vector
  const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();
  SANS_ASSERT( nDOFPDE == q[iq].m() );
  for (int k = 0; k < nDOFPDE; k++)
    q[iq][k] = qfld_.DOF(k);
}

template<class NDPDEClass, class Traits, class XFieldType>
typename AlgebraicEquationSet_TimeStep<NDPDEClass, Traits, XFieldType>::VectorSizeClass
AlgebraicEquationSet_TimeStep<NDPDEClass, Traits, XFieldType>::
vectorEqSize() const
{
  static_assert(iPDE == 0,"");

  // Create the size that represents the equations linear algebra vector
  const int nDOFPDEpos = qfld_.nDOFpossessed();

  return {nDOFPDEpos};
}

template<class NDPDEClass, class Traits, class XFieldType>
typename AlgebraicEquationSet_TimeStep<NDPDEClass, Traits, XFieldType>::VectorSizeClass
AlgebraicEquationSet_TimeStep<NDPDEClass, Traits, XFieldType>::
vectorStateSize() const
{
  static_assert(iPDE == 0,"");

  // Create the size that represents the number of unknowns (possessed + ghost) linear algebra vector
  const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();

  return {nDOFPDE};
}

template<class NDPDEClass, class Traits, class XFieldType>
typename AlgebraicEquationSet_TimeStep<NDPDEClass, Traits, XFieldType>::MatrixSizeClass
AlgebraicEquationSet_TimeStep<NDPDEClass, Traits, XFieldType>::matrixSize() const
{
  static_assert(iPDE == 0,"");
  static_assert(iq == 0,"");

  // Create the size that represents the size of a sparse linear algebra matrix
  const int nDOFPDEpos = qfld_.nDOFpossessed();

  const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();

  return {{ {nDOFPDEpos, nDOFPDE} }};
}

template<class NDPDEClass, class Traits, class XFieldType>
int
AlgebraicEquationSet_TimeStep<NDPDEClass, Traits, XFieldType>::
nResidNorm() const
{
  return 1;
}

template<class NDPDEClass, class Traits, class XFieldType>
void
AlgebraicEquationSet_TimeStep<NDPDEClass, Traits, XFieldType>::
dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                        const LinesearchData& pStepData) const
{
  std::string filename_iPDE = filenamebase + "_iPDE_iter" + std::to_string(nonlinear_iter) + ".plt";
  this->dumpLinesearchField(qfld_, *pStepData, iPDE, filename_iPDE);
}

} //namespace SANS

#endif //ALGEBRAICEQUATIONSET_TIMESTEP_H
