// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef AlgebraicEquationSet_HarmonicBalance_H
#define AlgebraicEquationSet_HarmonicBalance_H

#include <type_traits>
#include <memory> //unique_ptr
#include <vector>

#include "tools/SANSnumerics.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/tools/norm.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/tools/norm.h"

#include "Field/Field.h"
#include "Field/XField.h"
#include "Field/FieldSequence.h"

#include "LinearAlgebra/AlgebraicEquationSetBase.h"
#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/QuadratureOrder.h"

#include "Discretization/Galerkin/ResidualCell_Galerkin.h"
#include "Discretization/Galerkin/JacobianCell_Galerkin.h"

#include "Discretization/Galerkin/ResidualCell_Galerkin_HB.h"
#include "Discretization/Galerkin/JacobianCell_Galerkin_HB.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Conservative.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_HB.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template<class NDPDEClass, class Traits, class XFieldType>
class AlgebraicEquationSet_HarmonicBalance
    : public AlgebraicEquationSet_Debug<NDPDEClass, Traits>
{
public:
  typedef IntegrandCell_Galerkin_HB<NDPDEClass> IntegrandCell;

  typedef AlgebraicEquationSetTraits<typename NDPDEClass::template MatrixQ<Real>,
                                     typename NDPDEClass::template ArrayQ<Real>,
                                     Traits> TraitsType;

  typedef typename TraitsType::AlgebraicEquationSetBaseClass BaseType;

  typedef typename NDPDEClass::PhysDim PhysDim;
  typedef typename XFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDPDEClass::template MatrixQ<Real> MatrixQ;

  typedef typename TraitsType::VectorSizeClass VectorSizeClass;
  typedef typename TraitsType::MatrixSizeClass MatrixSizeClass;

  typedef typename TraitsType::SystemMatrix SystemMatrix;
  typedef typename TraitsType::SystemVector SystemVector;
  typedef typename TraitsType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename TraitsType::SystemMatrixView SystemMatrixView;
  typedef typename TraitsType::SystemVectorView SystemVectorView;
  typedef typename TraitsType::SystemNonZeroPatternView SystemNonZeroPatternView;

  typedef AlgebraicEquationSet_Debug<NDPDEClass, Traits> DebugBaseType;

  typedef typename BaseType::LinesearchData LinesearchData;

  typedef std::unique_ptr<BaseType> spatialptr;

  AlgebraicEquationSet_HarmonicBalance(const XFieldType& xfld,
                                       FieldSequence<PhysDim, TopoDim, ArrayQ>& qfldvec,
                                       const NDPDEClass& pde,
                                       const QuadratureOrder& quadratureOrder,
                                       const std::vector<int>& CellGroups,
                                       const Real& period,
                                       const int& ntimes,
                                       GlobalTime& time,
                                       const std::vector<Real>& tol,
                                       std::vector<spatialptr>& spatial)
   : DebugBaseType(pde, tol),
     xfld_(xfld), qfldvec_(qfldvec), pde_(pde),
     fcnCell_( pde, CellGroups, period, ntimes ),
     spatial_(spatial), period_(period), ntimes_(ntimes), time_(time), tvec_(ntimes_,0),
     iPDE_(spatial_[0]->indexPDE()), iq_(spatial_[0]->indexQ()),
     quadratureOrder_(quadratureOrder),
     quadratureOrderMin_(get<-1>(xfld), 0)
  {
    if (ntimes!= 1)
    {
      for (int i=0; i<ntimes_; i++)
        tvec_[i] = (Real)(i)*period_/(Real)(ntimes_);
    }
    else
      tvec_[0] = 0;
  }

  virtual ~AlgebraicEquationSet_HarmonicBalance() {}

  using BaseType::residual;
  using BaseType::jacobian;
  using BaseType::jacobianTranspose;

  //Computes the weighted integral of the conservative vector
  void harmonic_integral(SystemVectorView& rsd);
  void harmonic_integral(const FieldSequence<PhysDim, TopoDim, ArrayQ>& qflds, SystemVectorView& rsd);

  //Computes the residual
  virtual void residual(SystemVectorView& rsd) override;

  //Fills jacobian or the non-zero pattern of a jacobian
  virtual void jacobian(SystemMatrixView& mtx       ) override
  {
    for (int i=0; i<ntimes_; i++)
    {
      time_ = tvec_[i];
      int ii = nResidNormSpatial()*i;
      SystemMatrixView submtx = mtx.sub(ii,ii,nResidNormSpatial(),nResidNormSpatial());
      spatial_[i]->jacobian( submtx );
    }

    //Add HB integrals
    this->template jacobian<SystemMatrixView&>(mtx, quadratureOrder_ );
  }
  virtual void jacobian(SystemNonZeroPatternView& nz) override
  {
    for (int i=0; i<ntimes_; i++)
    {
      time_ = tvec_[i];
      int ii = nResidNormSpatial()*i;
      SystemNonZeroPatternView subnz = nz.sub(ii,ii,nResidNormSpatial(),nResidNormSpatial());
      spatial_[i]->jacobian( subnz );
    }

    //Add HB integrals
    this->template jacobian<SystemNonZeroPatternView&>(nz, quadratureOrderMin_ );
  }

  //Fills jacobian transpose or the non-zero pattern transpose of a jacobian transpose
  virtual void jacobianTranspose(SystemMatrixView& mtxT       ) override
  {

    for (int i=0; i<ntimes_; i++)
    {
      time_ = tvec_[i];
      int ii = nResidNormSpatial()*i;
      SystemMatrixView submtxT = mtxT.sub(ii,ii,nResidNormSpatial(),nResidNormSpatial());
      spatial_[i]->jacobianTranspose( submtxT );
    }

    //Add HB integrals
//    jacobian<SystemMatrixView&>(Transpose(mtxT), quadratureOrder_ );
  }

  virtual void jacobianTranspose(SystemNonZeroPatternView& nzT) override
  {

    for (int i=0; i<ntimes_; i++)
    {
      time_ = tvec_[i];
      int ii = nResidNormSpatial()*i;
      SystemNonZeroPatternView subnzT = nzT.sub(ii,ii,nResidNormSpatial(),nResidNormSpatial());
      spatial_[i]->jacobianTranspose( subnzT );
    }

    //Add HB integrals
//    jacobian<SystemNonZeroPatternView&>(Transpose( nzT), quadratureOrderMin_ );
  }

  // Compute Residual Norm
  virtual std::vector<std::vector<Real>> residualNorm( const SystemVectorView& rsd) const override
  {
    //concatenated residual norm vector [ [rsdNorm_t1], [rsdNorm_t2], [rsdNorm t3]...]
    const int nMon = pde_.nMonitor();

    std::vector<std::vector<Real> > rsdNorm(nResidNorm(), std::vector<Real>(nMon,0));

    for (int i=0; i< ntimes_; i++)
    {
      int ii = nResidNormSpatial()*i;

      std::vector<std::vector<Real> > rsdNormtmp = (spatial_[i])->residualNorm( rsd.sub(ii,nResidNorm()) );

      for (int j=0; j<nResidNormSpatial(); j++)
        rsdNorm[ii+j] = rsdNormtmp[j];

    }

    return rsdNorm;
  }

  //provides info about the residuals
  virtual void residualInfo(std::vector<std::string>& titles, std::vector<int>& idx) const override
  {
//    std::vector<std::string> titlestmp;
//    std::vector<int> idxtmp;
//
//    int count = 0;
//
//    for (int i=0; i< ntimes_; i++)
//    {
//      spatial_[i]->residualInfo(titlestmp,idxtmp);
//
//      for(int j=0; j<titlestmp.size(); j++ )
//      {
//        titles.push_back( titlestmp[j] );
//        idx.push_back(count);
//        count++;
//      }
//
//    }

  }

  virtual void printDecreaseResidualFailure(const std::vector<std::vector<Real>>& rsdNorm, std::ostream& os = std::cout) const override
  {
//    spatial_.printDecreaseResidualFailure(rsdNorm, os);
  }

  virtual void dumpSolution(const std::string& filename) const override
  {
    for (int i=0; i<ntimes_; i++)
    {
      std::string filename2 = filename + "_time" + std::to_string(i) + ".plt";
      (spatial_[i])->dumpSolution(filename2);
    }
  }

  //Translates the system vector into a solution field
  virtual void setSolutionField(const SystemVectorView& q) override
  {
    for (int i=0; i<ntimes_; i++)
    {
      int ii = nResidNormSpatial()*i;
      SystemVectorView qsub = q.sub(ii,nResidNormSpatial());

      (spatial_[i])->setSolutionField(qsub);
    }
  }

  //Translates the solution field into a system vector
  virtual void fillSystemVector(SystemVectorView& q) const override
  {
    for (int i=0; i<ntimes_;i++)
    {
      int ii = nResidNormSpatial()*i;
      SystemVectorView qsub = q.sub(ii,nResidNormSpatial());
      (spatial_[i])->fillSystemVector(qsub);
    }
  }

  // Returns the vector an matrix sizes needed for the linear algebra system
  virtual VectorSizeClass vectorEqSize() const override
  {

    //THIS IS NOT RIGHT
//    SANS_DEVELOPER_EXCEPTION("HB vectorEqSize() NOT IMPLEMENTED YET");
    VectorSizeClass tmpVector(nResidNorm());

    for (int i=0; i<ntimes_; i++)
    {
      VectorSizeClass tmp = spatial_[i]->vectorEqSize();
      for (int j=0; j< nResidNormSpatial(); j++ )
        tmpVector[i*nResidNormSpatial()+j] = tmp[j].m();
    }

    return tmpVector;
  } // vector for equations (rows in matrixSize)

  virtual VectorSizeClass vectorStateSize() const override
  {

//    SANS_DEVELOPER_EXCEPTION("HB vectorStateSize() NOT IMPLEMENTED YET");
    VectorSizeClass tmpVector(nResidNorm());
    for (int i=0; i<ntimes_; i++)
    {
      VectorSizeClass tmp = spatial_[i]->vectorStateSize();
      for (int j=0; j< nResidNormSpatial(); j++ )
        tmpVector[i*nResidNormSpatial()+j] = tmp[j].m();
    }

    return tmpVector;
  } // vector for state DOFs (columns in matrixSize)

  virtual MatrixSizeClass matrixSize() const override
  {
    int nSp = nResidNormSpatial();

//    SANS_DEVELOPER_EXCEPTION("HB matrixSize() NOT IMPLEMENTED YET");
    MatrixSizeClass tmpMatrix(nResidNorm(), nResidNorm());
    for (int i=0; i<ntimes_; i++)
      for (int j=0; j<ntimes_; j++)
      {
        MatrixSizeClass tmp(spatial_[i]->matrixSize());
        for (int ii=0; ii< nSp; ii++ )
          for (int jj=0; jj< nSp; jj++ )
            tmpMatrix(i*nSp+ii, j*nSp+jj).resize(tmp(ii,jj).m(), tmp(ii,jj).n());
      }

    return tmpMatrix;
  }

  // Gives the PDE and solution indices in the system
  virtual int indexPDE() const override { SANS_ASSERT(iPDE_ == spatial_[0]->indexPDE()); return iPDE_; }
  virtual int indexQ() const override { SANS_ASSERT(iq_ == spatial_[0]->indexQ()); return iq_; }

  // Checks to see if proposed solution is physical
  virtual bool isValidStateSystemVector(SystemVectorView& q) override;

  // Returns the side of the residual norm outer vector
  virtual int nResidNorm() const override;

  // Converts processor local indexing to a processor continuous indexing
  virtual std::vector<GlobalContinuousMap> continuousGlobalMap() const override { return spatial_[0]->continuousGlobalMap(); }

  // MPI communicator for this algebraic equation set
  virtual std::shared_ptr<mpi::communicator> comm() const override { return spatial_[0]->comm(); }

  virtual void syncDOFs_MPI() override
  {
    for (int i = 0; i < ntimes_; i++)
      spatial_[i]->syncDOFs_MPI();
  }

  virtual void dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                                       const LinesearchData& pStepData) const override;

protected:
  template<class SparseMatrixType>
  void jacobian(SparseMatrixType mtx, const QuadratureOrder& quadratureOrder );

  int nResidNormSpatial() const { return spatial_[0]->nResidNorm(); }

  const XFieldType& xfld_;
  FieldSequence<PhysDim, TopoDim, ArrayQ>& qfldvec_;
  const NDPDEClass& pde_;
  const IntegrandCell fcnCell_;
  std::vector<spatialptr>& spatial_;
  const Real period_;
  const int ntimes_;
  GlobalTime& time_;

  mutable std::vector<Real> tvec_;

  const int iPDE_;
  const int iq_;

  const QuadratureOrder quadratureOrder_;
  const QuadratureOrder quadratureOrderMin_;
};

template<class NDPDEClass, class Traits, class XFieldType>
void
AlgebraicEquationSet_HarmonicBalance<NDPDEClass, Traits, XFieldType>::
harmonic_integral(SystemVectorView& rsd)
{
  harmonic_integral(qfldvec_, rsd);
}

template<class NDPDEClass, class Traits, class XFieldType>
void
AlgebraicEquationSet_HarmonicBalance<NDPDEClass, Traits, XFieldType>::
harmonic_integral(const FieldSequence<PhysDim, TopoDim, ArrayQ>& qflds, SystemVectorView& rsd)
{
  int iq = spatial_[0]->indexQ();

  for (int i=0; i<ntimes_; i++)
  {
//    time_ = tvec_[i];
    int ii = i*nResidNormSpatial() + iq;

    IntegrateCellGroups<TopoDim>::integrate( ResidualCell_Galerkin_HB(fcnCell_, rsd(ii), i),
                                             xfld_, qfldvec_, quadratureOrder_.cellOrders.data(), quadratureOrder_.cellOrders.size() );
  }
}

template<class NDPDEClass, class Traits, class XFieldType>
void
AlgebraicEquationSet_HarmonicBalance<NDPDEClass, Traits, XFieldType>::
residual(SystemVectorView& rsd)
{
  // Compute the spatial residual
  for (int i=0; i<ntimes_; i++)
  {
    time_ = tvec_[i];
    int ii = i*nResidNormSpatial();
    SystemVectorView subrsd = rsd.sub(ii,nResidNorm());
    spatial_[i]->residual( subrsd );
  }

  // Add the integral of the conservative state vector
  harmonic_integral(rsd);
}


template<class NDPDEClass, class Traits, class XFieldType>
template<class SparseMatrixType>
void
AlgebraicEquationSet_HarmonicBalance<NDPDEClass, Traits, XFieldType>::
jacobian(SparseMatrixType jac, const QuadratureOrder& quadratureOrder)
{
  // Get the matrix type, this could be a reference or temporary variable depending on SparseMatrixType
  typedef typename std::result_of<SparseMatrixType(const int, const int)>::type Matrix;

  int iq = spatial_[0]->indexQ();
  int iPDE = spatial_[0]->indexPDE();

  for (int i=0; i<ntimes_; i++)
  {

//    time_ = tvec_[i];
    int ii = i*nResidNormSpatial() + iq;
    for (int j=0; j<ntimes_; j++)
    {
      if (i != j)
      {
        int jj = j*nResidNormSpatial() + iPDE;
        Matrix jacPDE_q  = jac(ii,jj);

        IntegrateCellGroups<TopoDim>::integrate( JacobianCell_Galerkin_HB(fcnCell_, jacPDE_q, i, j),
                                                 xfld_, qfldvec_, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
      }
    }
  }

}

template<class NDPDEClass, class Traits, class XFieldType>
bool
AlgebraicEquationSet_HarmonicBalance<NDPDEClass, Traits, XFieldType>::
isValidStateSystemVector(SystemVectorView& q)
{

  bool tmp = true;
  for (int i=0; i<ntimes_; i++)
  {

    int ii = nResidNormSpatial()*i;
    SystemVectorView qsub = q.sub(ii,nResidNormSpatial());

    bool tmp2 = spatial_[i]->isValidStateSystemVector(qsub);
    tmp = (tmp && tmp2);
  }

  return tmp;
}

template<class NDPDEClass, class Traits, class XFieldType>
int
AlgebraicEquationSet_HarmonicBalance<NDPDEClass, Traits, XFieldType>::
nResidNorm() const
{
  return ntimes_*(spatial_[0]->nResidNorm());
}

template<class NDPDEClass, class Traits, class XFieldType>
void
AlgebraicEquationSet_HarmonicBalance<NDPDEClass, Traits, XFieldType>::
dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                        const LinesearchData& pStepData) const
{
//  spatial_.dumpLinesearchDebugInfo(filenamebase, nonlinear_iter, pStepData);
  SANS_DEVELOPER_EXCEPTION("HB dumpLinesearchDebugInfo NOT IMPLEMENTED YET");
}


} //namespace SANS

#endif //AlgebraicEquationSet_HarmonicBalance_H
