// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ALGEBRAICEQUATIONSET_HTC2_H
#define ALGEBRAICEQUATIONSET_HTC2_H

#include <type_traits>

#include "tools/SANSnumerics.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/tools/norm.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/tools/norm.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "LinearAlgebra/BlockLinAlg/MatrixBlock_2x2.h"
#include "LinearAlgebra/BlockLinAlg/VectorBlock_2.h"

#include "Field/Field.h"
#include "Field/FieldSequence.h"
#include "Field/XField.h"

#include "LinearAlgebra/AlgebraicEquationSetBase.h"
#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"
#include "Discretization/IntegrateCellGroups.h"

#include "PDEHomotopy.h"

namespace SANS
{

template <class NDPDEClass, class Traits, class XFieldType>
class AlgebraicEquationSet_HTC2
    : public AlgebraicEquationSetBase
      <
        BLA::MatrixBlock_2x2
        <
          typename AlgebraicEquationSetTraits
          <
            typename NDPDEClass::template MatrixQ<Real>,
            typename NDPDEClass::template ArrayQ<Real>,
            Traits
          >::SystemMatrix,
          typename AlgebraicEquationSetTraits
          <
//            DLA::MatrixS<NDPDEClass::N, 1, Real>,
            typename std::conditional<NDPDEClass::N == 1, Real, DLA::MatrixS<NDPDEClass::N, 1, Real>>::type,
            Real,
            Traits
          >::SystemMatrix,
          typename AlgebraicEquationSetTraits
          <
//            DLA::MatrixS<1, NDPDEClass::N, Real>,
            typename std::conditional<NDPDEClass::N == 1, Real, DLA::MatrixS<1, NDPDEClass::N, Real>>::type,
//            DLA::VectorS<NDPDEClass::N, Real>,
            typename std::conditional<NDPDEClass::N == 1, Real, DLA::VectorS<NDPDEClass::N, Real>>::type,
            Traits
          >::SystemMatrix,
          typename AlgebraicEquationSetTraits
          <
            Real,
            Real,
            Traits
          >::SystemMatrix
        >
      >
    // the inheritance mess here allows a homotopy continuation algebraic
    // equation set scheme to inherit the block 2x2 framework, but in a way
    // that doesn't need to pull in a new coupling scheme
{
public:

  typedef typename NDPDEClass::PhysDim PhysDim;
  typedef typename XFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDPDEClass::template MatrixQ<Real> MatrixQ;

  typedef AlgebraicEquationSetTraits<MatrixQ, ArrayQ, Traits> TraitsType;

  typedef typename TraitsType::AlgebraicEquationSetBaseClass AlgEqSetBaseType;

  typedef typename TraitsType::SystemMatrix SystemMatrix00;
  typedef typename TraitsType::SystemVector SystemVector0;
  typedef typename TraitsType::SystemVectorView SystemVectorView0;
  typedef typename TraitsType::SystemNonZeroPatternView SystemNonZeroPattern00View;

//  typedef typename AlgebraicEquationSetTraits
//  <
//    DLA::MatrixS<NDPDEClass::N, 1, Real>,
//    DLA::VectorS<1, Real>,
//    Traits
//  >::SystemMatrix SystemMatrix01;

  typedef typename AlgebraicEquationSetTraits
  <
//  DLA::MatrixS<NDPDEClass::N, 1, Real>,
    typename std::conditional<NDPDEClass::N == 1, Real, DLA::MatrixS<NDPDEClass::N, 1, Real>>::type,
    Real,
    Traits
  >::SystemMatrix SystemMatrix01;

//  typedef typename AlgebraicEquationSetTraits
//  <
//    DLA::MatrixS<1, NDPDEClass::N, Real>,
//    DLA::VectorS<NDPDEClass::N, Real>,
//    Traits
//  >::SystemMatrix SystemMatrix10;
  typedef typename AlgebraicEquationSetTraits
  <
//  DLA::MatrixS<1, NDPDEClass::N, Real>,
    typename std::conditional<NDPDEClass::N == 1, Real, DLA::MatrixS<1, NDPDEClass::N, Real>>::type,
//  DLA::VectorS<NDPDEClass::N, Real>,
    typename std::conditional<NDPDEClass::N == 1, Real, DLA::VectorS<NDPDEClass::N, Real>>::type,
    Traits
  >::SystemMatrix SystemMatrix10;

  typedef typename AlgebraicEquationSetTraits<Real, Real, Traits>::SystemMatrix SystemMatrix11;
  typedef typename AlgebraicEquationSetTraits<Real, Real, Traits>::SystemVector SystemVector1;
  // each above uses the AES traits to guarantee sparsity/density matching

  typedef AlgebraicEquationSetBase<BLA::MatrixBlock_2x2<SystemMatrix00,
      SystemMatrix01, SystemMatrix10, SystemMatrix11>> AlgEqSetBlockType;

  typedef typename AlgEqSetBlockType::SystemMatrix SystemMatrix;
  typedef typename AlgEqSetBlockType::SystemVector SystemVector;
  typedef typename AlgEqSetBlockType::SystemMatrixView SystemMatrixView;
  typedef typename AlgEqSetBlockType::SystemVectorView SystemVectorView;
  typedef typename AlgEqSetBlockType::SystemNonZeroPattern SystemNonZeroPattern;
  typedef typename AlgEqSetBlockType::SystemNonZeroPatternView SystemNonZeroPatternView;

  typedef typename AlgEqSetBlockType::LinesearchData LinesearchData;

  typedef typename AlgEqSetBlockType::MatrixSizeClass MatrixSizeClass;
  typedef typename AlgEqSetBlockType::VectorSizeClass VectorSizeClass;

  typedef typename std::conditional<NDPDEClass::N == 1, Real, DLA::MatrixS<NDPDEClass::N, 1, Real>>::type ArrayQT;
  typedef typename std::conditional<NDPDEClass::N == 1, Real, DLA::MatrixS<1, NDPDEClass::N, Real>>::type ArrayQM;

  AlgebraicEquationSet_HTC2(const XFieldType &xfld, Field<PhysDim,
                            TopoDim, ArrayQ> &qfld,
                            NDPDEClass &pde,
                            const std::vector<int> &cellGroups,
                            AlgEqSetBaseType &baseAES) :
    xfld_(xfld),
    qfld_(qfld),
    pde_(pde),
    baseAES_(baseAES),
    dWds_(vectorStateSize()),
    iPDE_(baseAES_.indexPDE()),
    iq_(baseAES_.indexQ())
  {
    quadratureOrder_.resize(xfld_.nCellGroups(), -1);
    quadratureOrderMin_.resize(quadratureOrder_.size(), 0);

    pde_.setMergeMode(ResidualMode);

    isStaticCondensed_= baseAES_.isStaticCondensed();
  }

  virtual ~AlgebraicEquationSet_HTC2() {}

  // special function to update dWds
  void updateTangent(SystemVectorView &dWds)
  {
    dWds_= dWds;
  }

  using AlgEqSetBlockType::residual;
  using AlgEqSetBlockType::jacobian;
  using AlgEqSetBlockType::jacobianTranspose;
//  using AlgEqSetBlockType::completeUpdate;

  virtual bool isStaticCondensed() const override
  {
    return baseAES_.isStaticCondensed();
  }

  // computes the residual
  virtual void residual(SystemVectorView& rsd) override;

  // fills jacobian or the non-zero pattern of a jacobian
  virtual void jacobian(SystemMatrixView& mtx) override
  {
    // we MUST be in ResidualMode here
    pde_.setMergeMode(ResidualMode);
    baseAES_.jacobian(mtx.m00);
    this->template jacobian<SystemMatrixView&>(mtx, quadratureOrder_ );
#if 0
    std::string filename = "tmp/jac_NoSC.mtx";
    std::fstream fout(filename, std::fstream::out);
    WriteMatrixMarketFile(mtx, fout);
    SANS_DEVELOPER_EXCEPTION("STAHP!", false);
#endif
  }
  virtual void jacobian(SystemNonZeroPatternView& nz) override
  {
    // we MUST be in ResidualMode here
    pde_.setMergeMode(ResidualMode);
    baseAES_.jacobian(nz.m00);
    this->template jacobian<SystemNonZeroPatternView&>(nz, quadratureOrderMin_ );
  }

  void jacobian(SystemVectorView& b, SystemNonZeroPatternView& nz, bool transposed) override
  {
    pde_.setMergeMode(ResidualMode);
    baseAES_.jacobian( b.v0, nz.m00, transposed );
    this->template jacobian<SystemNonZeroPatternView&>(b, nz, quadratureOrderMin_ );
  }

  void jacobian(SystemVectorView& b, SystemMatrixView& mtx, bool transposed) override
  {
    pde_.setMergeMode(ResidualMode);
    baseAES_.jacobian( b.v0, mtx.m00, transposed );
    this->template jacobian<SystemMatrixView&>(b, mtx, quadratureOrder_ );
#if 0
    std::string filename = "tmp/jac_SC.mtx";
    std::fstream fout(filename, std::fstream::out);
    WriteMatrixMarketFile(mtx, fout);
    SANS_DEVELOPER_EXCEPTION("STAHP!", false);
#endif
  }

  // fills jacobian transpose or the non-zero pattern transpose of a jacobian transpose
  virtual void jacobianTranspose(SystemMatrixView& mtxT) override
  {
    SANS_DEVELOPER_EXCEPTION("implement. -cfrontin");
    // // we MUST be in ResidualMode here
    // pde_.setMergeMode(ResidualMode);
    // baseAES_.jacobian(mtxT.m00);
    // jacobian(Transpose(mtxT), quadratureOrder_ );
  }
  virtual void jacobianTranspose(SystemNonZeroPatternView& nzT) override
  {
    SANS_DEVELOPER_EXCEPTION("implement. -cfrontin");
    // // we MUST be in ResidualMode here
    // pde_.setMergeMode(ResidualMode);
    // baseAES_.jacobian(nzT.m00);
    // jacobian(Transpose( nzT), quadratureOrderMin_ );
  }

  // compute Residual Norm
  virtual std::vector<std::vector<Real>> residualNorm( const SystemVectorView& rsd) const override
  {
    return baseAES_.residualNorm(rsd.v0);
  }

  // convergence check of the residual
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm) const override
  {
    return baseAES_.convergedResidual(rsdNorm);
  }

  // convergence check of the residual
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm, int iEq, int iMon) const override
  {
    return baseAES_.convergedResidual(rsdNorm, iEq, iMon);
  }

  // check if residual decreased
  virtual bool decreasedResidual( const std::vector<std::vector<Real>>& rsdNormOld,
                                  const std::vector<std::vector<Real>>& rsdNormNew) const override
  {
    return baseAES_.decreasedResidual(rsdNormOld, rsdNormNew);
  }

  // prints out a residual that could not be decreased and the convergence tolerances
  virtual void printDecreaseResidualFailure(const std::vector<std::vector<Real>>& rsdNorm, std::ostream& os = std::cout) const override
  {
    baseAES_.printDecreaseResidualFailure(rsdNorm, os);
  }

  // translates the system vector into a solution field
  virtual void setSolutionField(const SystemVectorView& q) override
  {
    pde_.setLambda(q.v1[0][0]); // block 1x1, only entry of 1x1 matrix, only entry of a 1-length vector
    baseAES_.setSolutionField(q.v0);
  }

  // translates the solution field into a system vector
  virtual void fillSystemVector(SystemVectorView& q) const override
  {
    baseAES_.fillSystemVector(q.v0);
  }

  // translates the solution field into a system vector
  virtual void completeUpdate(const SystemVectorView& rsd, SystemVectorView& xcondensed, SystemVectorView& x) const override
  {
    baseAES_.completeUpdate(rsd.v0, xcondensed.v0, x.v0);
    x.v1 = xcondensed.v1;
  }

  // returns the vector an matrix sizes needed for the linear algebra system
  virtual VectorSizeClass vectorEqSize() const override
  {
    // the physical system vector is sized by the base algebraic equation set
    typename VectorSizeType<SystemVector0>::type size0(baseAES_.vectorEqSize());

    // the auxiliary system is a 1-length block of 1
    typename VectorSizeType<SystemVector1>::type size1(1);
    size1(0)= {1};

    VectorSizeClass size(size0, size1);

    return size;
  }

  virtual VectorSizeClass vectorStateSize() const override
  {
    // the physical system vector is sized by the base algebraic equation set
    typename VectorSizeType<SystemVector0>::type size0(baseAES_.vectorStateSize());

    // // the auxiliary system is a 1-length block of 1
    typename VectorSizeType<SystemVector1>::type size1(1);
    size1(0)= {1};

    VectorSizeClass size(size0, size1);

    return size;
  }

  virtual MatrixSizeClass matrixSize() const override
  {
    // the physical system matrix is sized by the base algebraic equation set
    typename MatrixSizeType<SystemMatrix00>::type size00(baseAES_.matrixSize());

    // the auxiliary system is a 1x1 block of 1x1
    typename MatrixSizeType<SystemMatrix11>::type size11(1, 1);
    size11(0, 0)= {1, 1};

    // the off-diagonal matrixes come from the above
    typename MatrixSizeType<SystemMatrix01>::type size01(size00.m(), size11.n());
    typename MatrixSizeType<SystemMatrix10>::type size10(size11.m(), size00.n());

    // then set the size of the interior SparseMatrix
    for (int i= 0; i < size01.m(); i++)
      for (int j= 0; j < size01.n(); j++)
        size01(i, j).resize(size00(i, 0).m(), size11(0, j).n());

    for (int i= 0; i < size10.m(); i++)
      for (int j= 0; j < size10.n(); j++)
        size10(i, j).resize(size11(i, 0).m(), size00(0, j).n());

    MatrixSizeClass size(size00, size01, size10, size11);

    return size;
  }

  // gives the PDE and solution indices in the system
  virtual int indexPDE() const override
  {
    SANS_ASSERT(iPDE_ == baseAES_.indexPDE());
    return iPDE_;
  }
  virtual int indexQ() const override
  {
    SANS_ASSERT(iq_ == baseAES_.indexQ());
    return iq_;
  }

  // checks to see if proposed solution is physical
  virtual bool isValidStateSystemVector(SystemVectorView& q) override
  {
    return baseAES_.isValidStateSystemVector(q.v0);
  }

  // returns the side of the residual norm outer vector
  virtual int nResidNorm() const override
  {
    return baseAES_.nResidNorm();
  }

  // MPI communicator for this algebraic equation set
  virtual std::shared_ptr<mpi::communicator> comm() const override
  {
    return get<-1>(xfld_).comm();
  }

  virtual void syncDOFs_MPI() override
  {
    baseAES_.syncDOFs_MPI();
  }

  // Dump the localized linesearch parameter info (for debugging purposes)
  virtual bool updateLinesearchDebugInfo(const Real& s, const SystemVectorView& rsd,
                                         LinesearchData& pResData,
                                         LinesearchData& pStepData) const override
  {
    // not sure how to convert to base linesearchdata
    SANS_DEVELOPER_EXCEPTION("implement. -cfrontin");
    return false;
    // return baseAES_.updateLinesearchDebugInfo(s, rsd.v0, pResData, pStepData);
  }

  virtual void dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                                       const LinesearchData& pStepData) const override
  {
    // not sure how to convert to base linesearchdata
    SANS_DEVELOPER_EXCEPTION("implement. -cfrontin");
    // baseAES_.dumpLinesearchDebugInfo(filenamebase, nonlinear_iter, pStepData);
  }

  // are we having machine precision issues - assume no issues
  virtual bool atMachinePrecision(const SystemVectorView& q, const std::vector<std::vector<Real>>& R0norm) override
  {
    SANS_DEVELOPER_EXCEPTION("AlgebraicEquationSet_HTC<>::atMachinePrecision() Not Implemented!");
    return false;
  }

protected:

  template <class SparseMatrixType>
  void jacobian(SparseMatrixType mtx, const std::vector<int>& quadratureOrder);

  template <class SparseMatrixType>
  void jacobian(SystemVectorView& b, SparseMatrixType mtx, const std::vector<int>& quadratureOrder);

  const XFieldType &xfld_;
  Field<PhysDim, TopoDim, ArrayQ> &qfld_;
  NDPDEClass &pde_;
  AlgEqSetBaseType &baseAES_;

  // Tangent vector
  SystemVector dWds_;

  const int iPDE_;
  const int iq_;

  std::vector<int> quadratureOrder_;
  std::vector<int> quadratureOrderMin_;

  bool isStaticCondensed_ = false;
};

template <class NDPDEClass, class Traits, class XFieldType>
void
AlgebraicEquationSet_HTC2<NDPDEClass, Traits, XFieldType>::
residual(SystemVectorView& rsd)
{
  // computer the baseAES residual, which also updates the solution field
  // we must be in residualmode here
  pde_.setMergeMode(ResidualMode);
  // pass the relevant block
  baseAES_.residual(rsd.v0);
}

template <class NDPDEClass, class Traits, class XFieldType>
template <class SparseMatrixType>
void
AlgebraicEquationSet_HTC2<NDPDEClass, Traits, XFieldType>::
jacobian(SparseMatrixType jac, const std::vector<int>& quadratureOrder)
{
  // HTC section

  // copy the tangent vector into the bottom row
  int rowMap[]= {0};
  int colMap[]= {0};

  // std::cout << "adding the bottom row" << std::endl;

  for (int i= 0; i < jac.m10.n(); i++) // loop over block-cols of jac(1, 0)
  {
    for (int k= 0; k < jac.m10(0, i).n(); k++)
    {
      rowMap[0]= 0;
      colMap[0]= k;

      ArrayQ val= dWds_.v0[i][k];

      ArrayQM valT[]= {Transpose(val)};

      // std::cout << dWds_.v0[i][k] << std::endl;
      // std::cout << valT[0] << std::endl;

      DLA::MatrixDView<ArrayQM> M(valT, 1, 1);
      jac.m10(0, i).scatterAdd(M, rowMap, 1, colMap, 1);
    }
  }

  {
    rowMap[0]= 0;
    colMap[0]= 0;

    Real val[]= {1.0};

    DLA::MatrixDView<Real> M(val, 1, 1);
    jac.m11(0, 0).scatterAdd(M, rowMap, 1, colMap, 1);
  }

  // the normal vector is zero in the auxiliary variable, so don't do anything

  // get the residual we need
  pde_.setMergeMode(JacobianMode);
  SystemVector0 rsd(baseAES_.vectorEqSize());
  rsd= 0.0;
  baseAES_.residual(rsd);
  pde_.setMergeMode(ResidualMode);

  // copy the residual vector into the right column
  rowMap[0]= 0;
  colMap[0]= 0;

  // std::cout << "adding the right column" << std::endl;

  for (int i= 0; i < jac.m01.m(); i++)
  {
    for (int k= 0; k < jac.m01(i, 0).m(); k++)
    {
      rowMap[0]= k;
      colMap[0]= 0;

      ArrayQ val= rsd[i][k];

      ArrayQT valM= {0.0};

      for (int idx= 0; idx < NDPDEClass::N; idx++)
        DLA::index(valM, idx, 0) = DLA::index(val, idx);

      ArrayQT valMp[]= {valM};

      // std::cout << rsd[i][k] << std::endl;
      // std::cout << valMp[0] << std::endl;

      DLA::MatrixDView<ArrayQT> M(valMp, 1, 1);
      jac.m01(i, 0).scatterAdd(M, rowMap, 1, colMap, 1);
    }
  }
}

template <class NDPDEClass, class Traits, class XFieldType>
template <class SparseMatrixType>
void
AlgebraicEquationSet_HTC2<NDPDEClass, Traits, XFieldType>::
jacobian(SystemVectorView& b, SparseMatrixType jac, const std::vector<int>& quadratureOrder)
{
  // HTC section

  // copy the tangent vector into the bottom row
  int rowMap[]= {0};
  int colMap[]= {0};

  // This "might" be pretty much hardcoded for VMSD...
  int ij = 0; //AlgEqSetBaseType::iPDESC;
  int ir = 1; //AlgEqSetBaseType::iPDE;
  for (int k= 0; k < jac.m10(0, ij).n(); k++)
  {
    rowMap[0]= 0;
    colMap[0]= k;

    ArrayQ val= dWds_.v0[ir][k];

    ArrayQM valT[]= {Transpose(val)};

    // std::cout << dWds_.v0[i][k] << std::endl;
    // std::cout << valT[0] << std::endl;

    DLA::MatrixDView<ArrayQM> M(valT, 1, 1);
    jac.m10(0, ij).scatterAdd(M, rowMap, 1, colMap, 1);
  }
  ij = 1; //AlgEqSetBaseType::iBCSC;
  ir = 2; //AlgEqSetBaseType::iBC;
  for (int k= 0; k < jac.m10(0, ij).n(); k++)
  {
    rowMap[0]= 0;
    colMap[0]= k;

    ArrayQ val= dWds_.v0[ir][k];

    ArrayQM valT[]= {Transpose(val)};

    // std::cout << dWds_.v0[i][k] << std::endl;
    // std::cout << valT[0] << std::endl;

    DLA::MatrixDView<ArrayQM> M(valT, 1, 1);
    jac.m10(0, ij).scatterAdd(M, rowMap, 1, colMap, 1);
  }

  {
    rowMap[0]= 0;
    colMap[0]= 0;

    Real val[]= {1.0};

    DLA::MatrixDView<Real> M(val, 1, 1);
    jac.m11(0, 0).scatterAdd(M, rowMap, 1, colMap, 1);
  }

  // the normal vector is zero in the auxiliary variable, so don't do anything

  // get the residual we need
  pde_.setMergeMode(JacobianMode);
  SystemVector0 rsd(baseAES_.vectorEqSize());
  rsd= 0.0;
  baseAES_.residual(rsd);
  pde_.setMergeMode(ResidualMode);

  // copy the residual vector into the right column
  rowMap[0]= 0;
  colMap[0]= 0;

  // This might be pretty much hardcoded for VMSD...
  ij = 0; //AlgEqSetBaseType::iPDESC;
  ir = 1; //AlgEqSetBaseType::iPDE;
  for (int k= 0; k < jac.m01(ij, 0).m(); k++)
  {
    rowMap[0]= k;
    colMap[0]= 0;

    ArrayQ val= rsd[ir][k];

    ArrayQT valM= {0.0};

    for (int idx= 0; idx < NDPDEClass::N; idx++)
      DLA::index(valM, idx, 0) = DLA::index(val, idx);

    ArrayQT valMp[]= {valM};

    // std::cout << rsd[i][k] << std::endl;
    // std::cout << valMp[0] << std::endl;

    DLA::MatrixDView<ArrayQT> M(valMp, 1, 1);
    jac.m01(ij, 0).scatterAdd(M, rowMap, 1, colMap, 1);
  }
  ij = 1; //AlgEqSetBaseType::iBCSC;
  ir = 2; //AlgEqSetBaseType::iBC;
  for (int k= 0; k < jac.m01(ij, 0).m(); k++)
  {
    rowMap[0]= k;
    colMap[0]= 0;

    ArrayQ val= rsd[ir][k];

    ArrayQT valM= {0.0};

    for (int idx= 0; idx < NDPDEClass::N; idx++)
      DLA::index(valM, idx, 0) = DLA::index(val, idx);

    ArrayQT valMp[]= {valM};

    // std::cout << rsd[i][k] << std::endl;
    // std::cout << valMp[0] << std::endl;

    DLA::MatrixDView<ArrayQT> M(valMp, 1, 1);
    jac.m01(ij, 0).scatterAdd(M, rowMap, 1, colMap, 1);
  }
}

} //namespace SANS

#endif //ALGEBRAICEQUATIONSET_HTC2_H
