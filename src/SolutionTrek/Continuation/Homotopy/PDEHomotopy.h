// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDEHOMOTOPY_H
#define PDEHOMOTOPY_H

// Merges two PDEs at an ND level to be used in the Homotopy framework

#include <iostream>
#include <string>

#include "tools/SANSnumerics.h"     // Real
#include "tools/smoothmath.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"

#include "Topology/ElementTopology.h"
#include "Topology/Dimension.h"

#include "Field/Tuple/ParamTuple.h"

namespace SANS
{

enum MergeType
{
  ResidualMode = 0,
  JacobianMode = 1,
  NotSet = -1 // This is the default which forces it to be set
};

//----------------------------------------------------------------------------//
// PDE class: Homotopy Merging
//
// template parameters:
//   T                        solution DOF data type (e.g. double)
//
// member functions:
//   .hasFluxAdvectiveTime     T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective        T/F: PDE has advective/inviscid flux term
//   .hasFluxViscous          T/F: PDE has viscous flux term
//
//   .masterState        unsteady conservative fluxes: U(Q)
//   .fluxAdvective           advective/inviscid fluxes: F(Q)
//   .fluxViscous             viscous fluxes: Fv(Q, QX)
//   .diffusionViscous        viscous diffusion coefficient: d(Fv)/d(UX)
//   .isValidState            T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom              set from primitive variable array
//----------------------------------------------------------------------------//

template<class PrimaryPDE, class AuxiliaryPDE>
class PDEHomotopy
{
public:
  static_assert( std::is_same<typename PrimaryPDE::PhysDim, typename AuxiliaryPDE::PhysDim>::value, "PDE dimensions must be the same");

  typedef typename PrimaryPDE::PhysDim PhysDim;

  static const int D = PrimaryPDE::D;   // physical dimensions
  static const int N = PrimaryPDE::N;   // total solution variables

  typedef DLA::VectorS<D,Real> VectorX;                                        // physical coordinate vector, i.e. cartesian or cylindrical

  template<class Z> using ArrayQ          = typename PrimaryPDE::template ArrayQ<Z>;  // solution/flux arrays
  template<class Z> using MatrixQ         = typename PrimaryPDE::template MatrixQ<Z>; // diffusion matrix/flux jacobians
  template<class Z> using VectorArrayQ    = DLA::VectorS<D, ArrayQ<Z> >;       // vector of solution arrays, i.e. solution gradients/flux vector
  template<class Z> using VectorMatrixQ   = DLA::VectorS<D, MatrixQ<Z> >;      // vector of jacobians, i.e. flux jacobians d(F)/d(U)
  template<class Z> using TensorSymArrayQ = DLA::MatrixSymS<D, ArrayQ<Z> >;    // hessian of solution, i.e QXX
  template<class Z> using TensorMatrixQ   = DLA::MatrixS<D, D, MatrixQ<Z> >;   // diffusion, i.e. viscous flux jacobian d(Fv)/d(UX)
  template<class Z> using VectorD         = DLA::VectorD<Z>;

  PDEHomotopy(Real& lambda,
              PrimaryPDE& F,
              AuxiliaryPDE& G) :
    lambda_(lambda),
    F_(F),
    G_(G),
    mergeMode(NotSet)
  {
    // Nothing - Should do some checks...
  }

  ~PDEHomotopy() {}

  PDEHomotopy& operator=( const PDEHomotopy& ) = delete;

  // Set lambda
  void setLambda(Real lambda) {lambda_ = lambda; }
  PrimaryPDE& getPrimaryPDE() const { return this->F_; }
  AuxiliaryPDE& getAuxiliaryPDE() const { return this->G_; }

  template <class T, class Ts>
  void mergeProperties(T& F, T& G, Ts& result) const;
  void setMergeMode( MergeType Type );
  MergeType getMergeMode();
  PrimaryPDE& getPrimaryPDE()
  {
    return F_;
  }

  // flux components
  bool hasFluxAdvectiveTime() const { return (F_.hasFluxAdvectiveTime() && G_.hasFluxAdvectiveTime()); }
  bool hasFluxAdvective() const { return (F_.hasFluxAdvective() || G_.hasFluxAdvective()); }
  bool hasFluxViscous() const { return (F_.hasFluxViscous() || G_.hasFluxViscous()); }
  bool hasSource() const { return (F_.hasSource() || G_.hasSource()); }
  bool hasSourceTrace() const { return (F_.hasSourceTrace() || G_.hasSourceTrace()); }
  bool hasSourceLiftedQuantity() const { return (F_.hasSourceLiftedQuantity() || G_.hasSourceLiftedQuantity()); }
  bool hasForcingFunction() const { return (F_.hasForcingFunction() || G_.hasForcingFunction()); }

  bool fluxViscousLinearInGradient() const
  {
    // both PDE's must be of the form K(q).gradu if the PDE is linear in the gradient
    return (F_.fluxViscousLinearInGradient() && G_.fluxViscousLinearInGradient());
  }
  bool needsSolutionGradientforSource() const
  {
    return (F_.needsSolutionGradientforSource() || G_.needsSolutionGradientforSource());
  }

  // unsteady conservative flux: U(Q)
  template <class Tp, class T>
  void masterState( const Tp& X,
                         const ArrayQ<T>& q, ArrayQ<T>& uCons ) const;

  template <class Tp, class T>
  void jacobianMasterState( const Tp& X,
                                 const ArrayQ<T>& q, MatrixQ<T>& dudq ) const;

  // unsteady conservative flux: d(U)/d(t)
  template <class Tp, class T>
  void strongFluxAdvectiveTime( const Tp& X,
                               const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& uCons ) const;

  // advective flux: F(Q)
  template <class Tp, class T, class Ts>
  void fluxAdvective( const Tp& X,
                      const ArrayQ<T>& q, VectorArrayQ<Ts>& F ) const;

  template <class Tp, class Tq, class Tf>
  void fluxAdvectiveUpwind( const Tp& X,
                            const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, const VectorX& N, ArrayQ<Tf>& f, const Real& scaleFactor = 1 ) const;

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class Tp, class T>
  void jacobianFluxAdvective( const Tp& X,
                              const ArrayQ<T>& q, VectorMatrixQ<T>& a ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tp, class T>
  void jacobianFluxAdvectiveAbsoluteValue( const Tp& X,
                                           const ArrayQ<T>& q, const VectorX& N, MatrixQ<T>& a ) const;

  // strong form advective fluxes
  template <class Tp, class T>
  void strongFluxAdvective( const Tp& X,
                            const ArrayQ<T>& q, const VectorArrayQ<T>& gradq, ArrayQ<T>& strongPDE ) const;

  // viscous flux: Fv(Q, QX)
  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscous( const Tp& X,
                    const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, VectorArrayQ<Tf>& F ) const;

  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscous( const Tp& XL,
                    const Tp& XR,
                    const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqL,
                    const ArrayQ<Tq>& qR, const VectorArrayQ<Tg>& gradqR,
                    const VectorX& N, ArrayQ<Tf>& f ) const;

  template <class Tp, class Tq, class Tg, class Tgu, class Tf>
  void perturbedGradFluxViscous( const Tp& X,
                                 const ArrayQ<Tq>& q,
                                 const VectorArrayQ<Tg>& gradq, const VectorArrayQ<Tgu>& graddq,
                                 VectorArrayQ<Tf>& dF ) const;


  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tp, class Tq, class Tg, class Tk>
  void diffusionViscous( const Tp& X,
                         const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq,
                         TensorMatrixQ<Tk>& K ) const;

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tp, class T>
  void jacobianFluxViscous( const Tp& X,
                            const ArrayQ<T>& q, VectorMatrixQ<T>& a ) const;

  // strong form viscous fluxes
  template <class Tp, class T>
  void strongFluxViscous( const Tp& X,
                          const ArrayQ<T>& q, const VectorArrayQ<T>& gradq,
                          const TensorSymArrayQ<T>& hessq, ArrayQ<T>& strongPDE ) const;

  // solution-dependent source: S(X, Q, QX)
  template<class Tp, class Tq, class Tg, class Ts>
  void source( const Tp& X,
               const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq,
               ArrayQ<Ts>& source ) const;

  // solution-dependent source: S(X, Q, QX)
  template<class Tp, class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void source( const Tp& X,
               const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
               const VectorArrayQ<Tg>& gradq, const VectorArrayQ<Tgp>& gradqp,
               ArrayQ<Ts>& source ) const;

  // solution-dependent source: S(X, Q, QX)
  template<class Tp, class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void sourceCoarse( const Tp& X,
                     const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
                     const VectorArrayQ<Tg>& gradq, const VectorArrayQ<Tgp>& gradqp,
                     ArrayQ<Ts>& source ) const;

  // solution-dependent source: S(X, Q, QX)
  template<class Tp, class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void sourceFine( const Tp& X,
                   const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
                   const VectorArrayQ<Tg>& gradq, const VectorArrayQ<Tgp>& gradqp,
                   ArrayQ<Ts>& source ) const;

  template<class Tp, class Tq, class Tlq, class Tg, class Ts>
  void source( const Tp& X,
               const ArrayQ<Tq>& q, const Tlq& lifted_quantity, const VectorArrayQ<Tg>& gradq,
               ArrayQ<Ts>& source ) const;

  template<class Tp, class Tq, class Tqp, class Tlq, class Tg, class Tgp, class Ts>
  void source( const Tp& X,
               const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
               const Tlq& lifted_quantity,
               const VectorArrayQ<Tg>& gradq, const VectorArrayQ<Tgp>& gradqp,
               ArrayQ<Ts>& source ) const;

  // dual-consistent source
  template <class Tp, class Tq, class Tg, class Ts>
  void sourceTrace( const Tp& XL, const Tp& XR,
                    const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqL,
                    const ArrayQ<Tq>& qR, const VectorArrayQ<Tg>& gradqR,
                    ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const;

  // trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
  template <class Tp, class Tq, class Ts>
  void sourceLiftedQuantity(
      const Tp& XL, const Tp& XR,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, Ts& s ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tp, class T>
  void jacobianSource( const Tp& X,
                       const ArrayQ<T>& q, const VectorArrayQ<T>& gradq, MatrixQ<T>& dsdu ) const;

  // right-hand-side forcing function
  template <class Tp, class T>
  void forcingFunction( const Tp& X,
                        ArrayQ<T>& forcing ) const;

  // characteristic speed (needed for timestep)
  template <class Tp, class T>
  void speedCharacteristic( const Tp& X,
                            const VectorX& DX, const ArrayQ<T>& q, Real& speed ) const;

  // update fraction needed for physically valid state
  template <class ParamT>
  void updateFraction( const ParamT& param,
                       const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
                       const Real maxChangeFraction, Real& updateFraction ) const;

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const;

  // set from primitive variable array
  template <class T>
  void setDOFFrom(
      ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const;

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const;

  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const;

  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const;

  // how many residual equations are we dealing with
  int nMonitor() const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

  std::vector<std::string> derivedQuantityNames() const
  {
    return F_.derivedQuantityNames();
  }

  template<class Tq, class Tg, class Tf>
  void derivedQuantity(
      const int& index, const VectorX& X,
      const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, Tf& J ) const
  {
    F_.derivedQuantity(index, X, q, gradq, J);
  }

  template<class L, class Tq, class Tg, class Tf>
  void derivedQuantity(
      const int& index, const ParamTuple<L, VectorX, TupleClass<>>& param,
      const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq, Tf& J ) const
  {
    const VectorX& X = param.right();
    F_.derivedQuantity(index ,param.left(), X, q, gradq, J);
  }

private:
  Real& lambda_;
  PrimaryPDE& F_;
  AuxiliaryPDE& G_;
  MergeType mergeMode;
};

template<class PrimaryPDE, class AuxiliaryPDE>
template <class T, class Ts>
inline void
PDEHomotopy<PrimaryPDE, AuxiliaryPDE>::mergeProperties(T& F, T& G, Ts& result) const
{
  // Merge properties based on the mergeMode
  if (mergeMode == ResidualMode)
  {
    //
    result += lambda_ * F + (1.0 - lambda_) * G;
  }
  else if (mergeMode == JacobianMode)
  {
    //
    result += F - G;
  }
  else
  {
    // We should not get here... ever
    SANS_DEVELOPER_EXCEPTION( "Unknown mergeMode used in PDEHomotopy" );
  }
}

template<class PrimaryPDE, class AuxiliaryPDE>
inline void
PDEHomotopy<PrimaryPDE, AuxiliaryPDE>::setMergeMode( MergeType Type )
{
  mergeMode = Type;
}

template<class PrimaryPDE, class AuxiliaryPDE>
inline MergeType
PDEHomotopy<PrimaryPDE, AuxiliaryPDE>::getMergeMode()
{
  return mergeMode;
}

template<class PrimaryPDE, class AuxiliaryPDE>
template <class Tp, class T>
inline void
PDEHomotopy<PrimaryPDE, AuxiliaryPDE>::masterState(
    const Tp& X,
    const ArrayQ<T>& q, ArrayQ<T>& uCons ) const
{
  ArrayQ<T> uConsF = 0.0;
//  ArrayQ<T> uConsG = 0.0;

  F_.masterState(X, q, uConsF);
//  G_.masterState(q, uConsG);

//  mergeProperties(uConsF, uConsG, uCons);
  uCons = uConsF;
}

template<class PrimaryPDE, class AuxiliaryPDE>
template <class Tp, class T>
inline void
PDEHomotopy<PrimaryPDE, AuxiliaryPDE>::jacobianMasterState(
    const Tp& X,
    const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
{
  F_.jacobianMasterState(X, q, dudq);
}

template<class PrimaryPDE, class AuxiliaryPDE>
template <class Tp, class T>
inline void
PDEHomotopy<PrimaryPDE, AuxiliaryPDE>::strongFluxAdvectiveTime(
    const Tp& X,
    const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& uCons ) const
{
  ArrayQ<T> uConsF = 0.0;
  ArrayQ<T> uConsG = 0.0;

  F_.masterState(X, qt, uConsF);
  G_.masterState(X, qt, uConsG);

  mergeProperties(uConsF, uConsG, uCons);
}

template<class PrimaryPDE, class AuxiliaryPDE>
template <class Tp, class T, class Ts>
inline void
PDEHomotopy<PrimaryPDE, AuxiliaryPDE>::fluxAdvective( const Tp& X,
                                                      const ArrayQ<T>& q, VectorArrayQ<Ts>& F )const
{
  VectorArrayQ<T> FF = 0.0;
  VectorArrayQ<T> FG = 0.0;

  F_.fluxAdvective( X, q, FF );
  G_.fluxAdvective( X, q, FG );

  mergeProperties(FF, FG, F);
}

template<class PrimaryPDE, class AuxiliaryPDE>
template <class Tp, class Tq, class Tf>
inline void
PDEHomotopy<PrimaryPDE, AuxiliaryPDE>::fluxAdvectiveUpwind( const Tp& X,
                                                            const ArrayQ<Tq>& qL,
                                                            const ArrayQ<Tq>& qR,
                                                            const VectorX& N, ArrayQ<Tf>& f, const Real& scaleFactor ) const
{
  ArrayQ<Tf> fF = 0.0;
  ArrayQ<Tf> fG = 0.0;

  F_.fluxAdvectiveUpwind(X, qL, qR, N, fF, scaleFactor);
  G_.fluxAdvectiveUpwind(X, qL, qR, N, fG, scaleFactor);

  mergeProperties(fF, fG, f);
}


// advective flux jacobian: d(F)/d(U)
template<class PrimaryPDE, class AuxiliaryPDE>
template <class Tp, class T>
inline void
PDEHomotopy<PrimaryPDE, AuxiliaryPDE>::jacobianFluxAdvective( const Tp& X,
                                                              const ArrayQ<T>& q, VectorMatrixQ<T>& a ) const
{
  VectorMatrixQ<T> aF = 0.0;
  VectorMatrixQ<T> aG = 0.0;

  F_.jacobianFluxAdvective( X, q, aF );
  G_.jacobianFluxAdvective( X, q, aG );

  mergeProperties(aF, aG, a);
}

// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template<class PrimaryPDE, class AuxiliaryPDE>
template <class Tp, class T>
inline void
PDEHomotopy<PrimaryPDE, AuxiliaryPDE>::jacobianFluxAdvectiveAbsoluteValue( const Tp& X,
                                                                           const ArrayQ<T>& q,
                                                                           const VectorX& N, MatrixQ<T>& a ) const
{
  MatrixQ<T> aF = 0.0;
  MatrixQ<T> aG = 0.0;

  F_.jacobianFluxAdvectiveAbsoluteValue( X, q, N, aF );
  G_.jacobianFluxAdvectiveAbsoluteValue( X, q, N, aG );

  mergeProperties(aF, aG, a);
}

// strong form of advective flux: div.F
template<class PrimaryPDE, class AuxiliaryPDE>
template <class Tp, class T>
inline void
PDEHomotopy<PrimaryPDE, AuxiliaryPDE>::strongFluxAdvective( const Tp& X,
                                                            const ArrayQ<T>& q,
                                                            const VectorArrayQ<T>& gradq,
                                                            ArrayQ<T>& strongPDE ) const
{
  ArrayQ<T> strongPDEF = 0.0;
  ArrayQ<T> strongPDEG = 0.0;

  F_.strongFlux( X, q, gradq, strongPDEF );
  G_.strongFlux( X, q, gradq, strongPDEG );

  mergeProperties(strongPDEF, strongPDEG, strongPDE);
}

// viscous flux
template<class PrimaryPDE, class AuxiliaryPDE>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDEHomotopy<PrimaryPDE, AuxiliaryPDE>::fluxViscous( const Tp& X,
                                                    const ArrayQ<Tq>& q,
                                                    const VectorArrayQ<Tg>& gradq,
                                                    VectorArrayQ<Tf>& F ) const
{
  VectorArrayQ<Tf> fF = 0.0;
  VectorArrayQ<Tf> fG = 0.0;

  F_.fluxViscous( X, q, gradq, fF );
  G_.fluxViscous( X, q, gradq, fG );

  mergeProperties(fF, fG, F);
}

template<class PrimaryPDE, class AuxiliaryPDE>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDEHomotopy<PrimaryPDE, AuxiliaryPDE>::fluxViscous( const Tp& XL,
                                                    const Tp& XR,
                                                    const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqL,
                                                    const ArrayQ<Tq>& qR, const VectorArrayQ<Tg>& gradqR,
                                                    const VectorX& N, ArrayQ<Tf>& f ) const
{
  ArrayQ<Tf> fF = 0.0;
  ArrayQ<Tf> fG = 0.0;

  F_.fluxViscous( XL, XR, qL, gradqL, qR, gradqR, N, fF );
  G_.fluxViscous( XL, XR, qL, gradqL, qR, gradqR, N, fG );

  mergeProperties(fF, fG, f);
}

template<class PrimaryPDE, class AuxiliaryPDE>
template <class Tp, class Tq, class Tg, class Tgu, class Tf>
inline void
PDEHomotopy<PrimaryPDE, AuxiliaryPDE>::
perturbedGradFluxViscous( const Tp& X,
                          const ArrayQ<Tq>& q,
                          const VectorArrayQ<Tg>& gradq, const VectorArrayQ<Tgu>& graddq,
                          VectorArrayQ<Tf>& dF ) const
{
  VectorArrayQ<Tf> fF = 0.0;
  VectorArrayQ<Tf> fG = 0.0;

  F_.perturbedGradFluxViscous( X, q, gradq, graddq, fF );
  G_.perturbedGradFluxViscous( X, q, gradq, graddq, fG );

  mergeProperties(fF, fG, dF);
}

// viscous diffusion matrix: d(Fv)/d(UX)
template<class PrimaryPDE, class AuxiliaryPDE>
template <class Tp, class Tq, class Tg, class Tk>
inline void
PDEHomotopy<PrimaryPDE, AuxiliaryPDE>::
diffusionViscous( const Tp& X,
                  const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq,
                  TensorMatrixQ<Tk>& K ) const
{
  TensorMatrixQ<Tk> kF = 0.0;
  TensorMatrixQ<Tk> kG = 0.0;

  F_.diffusionViscous( X, q, gradq, kF );
  G_.diffusionViscous( X, q, gradq, kG );

  mergeProperties(kF, kG, K);
}


template<class PrimaryPDE, class AuxiliaryPDE>
template <class Tp, class T>
inline void
PDEHomotopy<PrimaryPDE, AuxiliaryPDE>::jacobianFluxViscous( const Tp& X,
                                                            const ArrayQ<T>& q, VectorMatrixQ<T>& a ) const
{
  VectorMatrixQ<T> aF = 0.0;
  VectorMatrixQ<T> aG = 0.0;

  F_.jacobianFluxViscous( X, q, aF );
  G_.jacobianFluxViscous( X, q, aG );

  mergeProperties(aF, aG, a);
}

// strong form of viscous flux: d(Fv)/d(X)
template<class PrimaryPDE, class AuxiliaryPDE>
template <class Tp, class T>
inline void
PDEHomotopy<PrimaryPDE, AuxiliaryPDE>::strongFluxViscous( const Tp& X,
                                                          const ArrayQ<T>& q, const VectorArrayQ<T>& gradq,
                                                          const TensorSymArrayQ<T>& hessq, ArrayQ<T>& strongPDE ) const
{
  ArrayQ<T> strongPDEF = 0.0;
  ArrayQ<T> strongPDEG = 0.0;

  F_.strongFluxViscous( X, q, gradq, hessq, strongPDEF );
  G_.strongFluxViscous( X, q, gradq, hessq, strongPDEG );

  mergeProperties(strongPDEF, strongPDEG, strongPDE);
}

// solution-dependent source: S(X, Q, QX)
template<class PrimaryPDE, class AuxiliaryPDE>
template<class Tp, class Tq, class Tg, class Ts>
inline void
PDEHomotopy<PrimaryPDE, AuxiliaryPDE>::source( const Tp& X,
                                               const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq,
                                               ArrayQ<Ts>& source ) const
{
  ArrayQ<Ts> sourceF = 0.0;
  ArrayQ<Ts> sourceG = 0.0;

  F_.source(X, q, gradq, sourceF );
  G_.source(X, q, gradq, sourceG );

  mergeProperties(sourceF, sourceG, source);
}

// solution-dependent source: S(X, Q, QX)
template<class PrimaryPDE, class AuxiliaryPDE>
template<class Tp, class Tq, class Tqp, class Tg, class Tgp, class Ts>
inline void
PDEHomotopy<PrimaryPDE, AuxiliaryPDE>::source( const Tp& X,
                                               const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
                                               const VectorArrayQ<Tg>& gradq, const VectorArrayQ<Tgp>& gradqp,
                                               ArrayQ<Ts>& source ) const
{
  ArrayQ<Ts> sourceF = 0.0;
  ArrayQ<Ts> sourceG = 0.0;

  F_.source(X, q, qp, gradq, gradqp, sourceF );
  G_.source(X, q, qp, gradq, gradqp, sourceG );

  mergeProperties(sourceF, sourceG, source);
}

// solution-dependent source: S(X, Q, QX)
template<class PrimaryPDE, class AuxiliaryPDE>
template<class Tp, class Tq, class Tqp, class Tg, class Tgp, class Ts>
inline void
PDEHomotopy<PrimaryPDE, AuxiliaryPDE>::sourceCoarse( const Tp& X,
                                                     const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
                                                     const VectorArrayQ<Tg>& gradq, const VectorArrayQ<Tgp>& gradqp,
                                                     ArrayQ<Ts>& source ) const
{
  ArrayQ<Ts> sourceF = 0.0;
  ArrayQ<Ts> sourceG = 0.0;

  F_.sourceCoarse(X, q, qp, gradq, gradqp, sourceF );
  G_.sourceCoarse(X, q, qp, gradq, gradqp, sourceG );

  mergeProperties(sourceF, sourceG, source);
}

// solution-dependent source: S(X, Q, QX)
template<class PrimaryPDE, class AuxiliaryPDE>
template<class Tp, class Tq, class Tqp, class Tg, class Tgp, class Ts>
inline void
PDEHomotopy<PrimaryPDE, AuxiliaryPDE>::sourceFine( const Tp& X,
                                                   const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
                                                   const VectorArrayQ<Tg>& gradq, const VectorArrayQ<Tgp>& gradqp,
                                                   ArrayQ<Ts>& source ) const
{
  ArrayQ<Ts> sourceF = 0.0;
  ArrayQ<Ts> sourceG = 0.0;

  F_.sourceFine(X, q, qp, gradq, gradqp, sourceF );
  G_.sourceFine(X, q, qp, gradq, gradqp, sourceG );

  mergeProperties(sourceF, sourceG, source);
}

// solution-dependent source with lifted quantity: S(X, Q, QX)
template<class PrimaryPDE, class AuxiliaryPDE>
template<class Tp, class Tq, class Tlq, class Tg, class Ts>
inline void
PDEHomotopy<PrimaryPDE, AuxiliaryPDE>::source( const Tp& X,
                                               const ArrayQ<Tq>& q, const Tlq& lifted_quantity, const VectorArrayQ<Tg>& gradq,
                                               ArrayQ<Ts>& source ) const
{
  ArrayQ<Ts> sourceF = 0.0;
  ArrayQ<Ts> sourceG = 0.0;

  F_.source(X, lifted_quantity, q, gradq, sourceF );
  G_.source(X, lifted_quantity, q, gradq, sourceG );

  mergeProperties(sourceF, sourceG, source);
}

// solution-dependent source with lifted quantity: S(X, Q, QX)
template<class PrimaryPDE, class AuxiliaryPDE>
template<class Tp, class Tq, class Tqp, class Tlq, class Tg, class Tgp, class Ts>
inline void
PDEHomotopy<PrimaryPDE, AuxiliaryPDE>::source( const Tp& X,
                                               const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
                                               const Tlq& lifted_quantity,
                                               const VectorArrayQ<Tg>& gradq, const VectorArrayQ<Tgp>& gradqp,
                                               ArrayQ<Ts>& source ) const
{
  ArrayQ<Ts> sourceF = 0.0;
  ArrayQ<Ts> sourceG = 0.0;

  F_.source(X, q, qp, lifted_quantity, gradq, gradqp, sourceF );
  G_.source(X, q, qp, lifted_quantity, gradq, gradqp, sourceG );

  mergeProperties(sourceF, sourceG, source);
}

// dual-consistent source
template<class PrimaryPDE, class AuxiliaryPDE>
template <class Tp, class Tq, class Tg, class Ts>
inline void
PDEHomotopy<PrimaryPDE, AuxiliaryPDE>::sourceTrace( const Tp& XL, const Tp& XR,
                                                    const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqL,
                                                    const ArrayQ<Tq>& qR, const VectorArrayQ<Tg>& gradqR,
                                                    ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
{
  ArrayQ<Ts> sourceLF = 0.0;
  ArrayQ<Ts> sourceRF = 0.0;
  ArrayQ<Ts> sourceLG = 0.0;
  ArrayQ<Ts> sourceRG = 0.0;

  F_.sourceTrace(XL, XR,
                 qL, gradqL,
                 qR, gradqR,
                 sourceLF, sourceRF);
  G_.sourceTrace(XL, XR,
                 qL, gradqL,
                 qR, gradqR,
                 sourceLG, sourceRG);

  mergeProperties(sourceLF, sourceLG, sourceL);
  mergeProperties(sourceRF, sourceRG, sourceR);
}

// jacobian of source wrt conservation variables: d(S)/d(U)
template<class PrimaryPDE, class AuxiliaryPDE>
template <class Tp, class T>
inline void
PDEHomotopy<PrimaryPDE, AuxiliaryPDE>::jacobianSource( const Tp& X,
                                                       const ArrayQ<T>& q, const VectorArrayQ<T>& gradq, MatrixQ<T>& dsdu ) const
{
  MatrixQ<T> dsduF = 0.0;
  MatrixQ<T> dsduG = 0.0;

  F_.jacobianSource(X, q, gradq, dsduF);
  G_.jacobianSource(X, q, gradq, dsduG);

  mergeProperties(dsduF, dsduG, dsdu);
}

// right-hand-side forcing function
template<class PrimaryPDE, class AuxiliaryPDE>
template <class Tp, class T>
inline void
PDEHomotopy<PrimaryPDE, AuxiliaryPDE>::forcingFunction( const Tp& X,
                                                        ArrayQ<T>& forcing ) const
{
  ArrayQ<T> forcingF = 0.0;
  ArrayQ<T> forcingG = 0.0;

  if ( F_.hasForcingFunction() )
    F_.forcingFunction(X, forcingF );
  if ( G_.hasForcingFunction() )
    G_.forcingFunction(X, forcingG );

  mergeProperties(forcingF, forcingG, forcing);
}

// characteristic speed (needed for timestep)
template<class PrimaryPDE, class AuxiliaryPDE>
template <class Tp, class T>
inline void
PDEHomotopy<PrimaryPDE, AuxiliaryPDE>::speedCharacteristic( const Tp& X,
                                                            const VectorX& DX, const ArrayQ<T>& q, Real& speed ) const
{
  Real speedF = 0.0;
  Real speedG = 0.0;

  F_.speedCharacteristic(X, DX, q, speedF );
  G_.speedCharacteristic(X, DX, q, speedG );

  mergeProperties(speedF, speedG, speed);
}


// update fraction needed for physically valid state
template<class PrimaryPDE, class AuxiliaryPDE>
template<class ParamT>
inline void
PDEHomotopy<PrimaryPDE, AuxiliaryPDE>::
updateFraction( const ParamT& param,
                const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
                const Real maxChangeFraction, Real& updateFraction ) const
{
  Real updateFraction_F;
  Real updateFraction_G;

  F_.updateFraction(param, q, dq, maxChangeFraction, updateFraction_F);
  G_.updateFraction(param, q, dq, maxChangeFraction, updateFraction_G);

  updateFraction = MIN(updateFraction_F, updateFraction_G);
}


// is state physically valid
template<class PrimaryPDE, class AuxiliaryPDE>
inline bool
PDEHomotopy<PrimaryPDE, AuxiliaryPDE>::isValidState( const ArrayQ<Real>& q ) const
{
  return (F_.isValidState(q) && G_.isValidState(q));
}


// set from primitive variable array
template<class PrimaryPDE, class AuxiliaryPDE>
template <class T>
inline void
PDEHomotopy<PrimaryPDE, AuxiliaryPDE>::setDOFFrom(
    ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const
{
  F_.setDOFFrom(q, data, name, nn);
  G_.setDOFFrom(q, data, name, nn);
}

// interpret residuals of the solution variable
template<class PrimaryPDE, class AuxiliaryPDE>
template <class T>
inline void
PDEHomotopy<PrimaryPDE, AuxiliaryPDE>::interpResidVariable(
    const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
{
  F_.interpResidVariable(rsdPDEIn, rsdPDEOut);
}

// interpret residuals of the gradients in the solution variable
template<class PrimaryPDE, class AuxiliaryPDE>
template <class T>
inline void
PDEHomotopy<PrimaryPDE, AuxiliaryPDE>::interpResidGradient(
    const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const
{
  F_.interpResidGradient(rsdAuxIn, rsdAuxOut);
}

// interpret residuals at the boundary (should forward to BCs)
template<class PrimaryPDE, class AuxiliaryPDE>
template <class T>
inline void
PDEHomotopy<PrimaryPDE, AuxiliaryPDE>::interpResidBC(
    const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const
{
  F_.interpResidBC(rsdBCIn, rsdBCOut);
}

// how many residual equations are we dealing with
template<class PrimaryPDE, class AuxiliaryPDE>
inline int
PDEHomotopy<PrimaryPDE, AuxiliaryPDE>::nMonitor() const
{
  return F_.nMonitor();
}

template<class PrimaryPDE, class AuxiliaryPDE>
void
PDEHomotopy<PrimaryPDE, AuxiliaryPDE>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PDEHomotopy: F_ =" << std::endl;
  F_.dump(indentSize+2, out);
  out << indent << "PDEHomotopy3D: G_ =" << std::endl;
  G_.dump(indentSize+2, out);
}

} //namespace SANS

#endif  // PDEHOMOTOPY_H
