// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCHOMOTOPY_H
#define BCHOMOTOPY_H

// Merges between two BCs for Homotopy purposes
// NOTE: Only forwards to primary BC currently

#include "Python/PyDict.h" //Python must be included first

#include "tools/SANSnumerics.h"     // Real
#include "tools/add_decorator.h"

#include "Topology/Dimension.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"

#include "pde/BCCategory.h"
#include "pde/NDConvert/GlobalTime.h"

#include "PDEHomotopy.h"

#include <iostream>
#include <string>

namespace SANS
{

template <class PhysDim, class BC>
class BCNDConvertHomotopy;

// Specialized add decorator to NOT add any decoration for homotopy
template<class arg>
struct add_ND_decorator<BCNDConvertHomotopy, arg>
{
  template<class arg1>
  struct apply
  {
    // Don't add a decorator
    typedef arg1 type;
  };
};

template<class NDPrimaryPDE, class NDAuxiliaryPDE, class NDPrimaryBC, class NDAuxiliaryBC>
class BCHomotopy : public BCType< BCHomotopy<NDPrimaryPDE, NDAuxiliaryPDE, NDPrimaryBC, NDAuxiliaryBC> >
{
public:
  static_assert( std::is_same<typename NDPrimaryBC::PhysDim, typename NDAuxiliaryBC::PhysDim>::value, "BC dimensions must be the same");
//  static_assert( std::is_same<typename NDPrimaryBC::Category, typename NDAuxiliaryBC::Category>::value,
//                 "BC categories must be the same type for Homotopy");

  typedef typename NDPrimaryBC::Category Category;
  typedef typename NDPrimaryBC::ParamsType ParamsType;

  typedef typename NDPrimaryBC::PhysDim PhysDim;

  static const int D = NDPrimaryBC::D;   // physical dimensions
  static const int N = NDPrimaryBC::N;   // total solution variables

  static const int NBC = NDPrimaryBC::NBC;                   // total BCs

  typedef DLA::VectorS<D,Real> VectorX;

  template <class T>
  using ArrayQ = typename NDPrimaryBC::template ArrayQ<T>;
  template <class T>
  using MatrixQ = typename NDPrimaryBC::template MatrixQ<T>;        // matrices
  template <class T>
  using VectorArrayQ = typename NDPrimaryBC::template VectorArrayQ<T>;        // matrices

  BCHomotopy(const Real& lambda, const PDEHomotopy< NDPrimaryPDE, NDAuxiliaryPDE >& pde, PyDict& d) :
    lambda_(lambda),
    pde_(pde),
    F_(pde.getPrimaryPDE(), d),
    G_(pde.getAuxiliaryPDE(), d)
  {
    // Nothing
  }


//  BCHomotopy(const Real& lambda,
//             const NDPrimaryBC& F,
//             const NDAuxiliaryBC& G) :
//    lambda_(lambda),
//    F_(F),
//    G_(G)
//  {
//    // Nothing
//  }

  virtual ~BCHomotopy() {}

  BCHomotopy& operator=( const BCHomotopy& ) = delete;

  virtual const std::type_info& derivedTypeID() const override { return typeid(*this); }

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const
  {
//    return (F_.hasFluxViscous() || G_.hasFluxViscous());
    return F_.hasFluxViscous();
  }

  // BC coefficients:  A u + B (kn un + ks us)
  template <class T>
  void coefficients(
      const VectorX& X, const VectorX& N,
      MatrixQ<T>& A, MatrixQ<T>& B ) const
  {
    F_.coefficients(X, N, A, B);
  }

  template <class T>
  void data( const VectorX& X, const VectorX& N, ArrayQ<T>& bcdata ) const
  {
    F_.data(X, N, bcdata);
  }

  // BC state vector
  template <class Tp, class T>
  void state( const Tp& X, const VectorX& N,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    ArrayQ<T> qBF = 0;
//    ArrayQ<T> qBG = 0;
    F_.state(X, N, qI, qBF);
//    G_.state(X, N, qI, qBG);

    qB = qBF;
//    pde_.mergeProperties(qBF, qBG, qB);
  }

  // normal BC flux
  template <class Tp, class T>
  void fluxNormal( const Tp& X, const VectorX& N,
                   const ArrayQ<T>& qI,
                   const VectorArrayQ<T>& gradq,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const
  {
    ArrayQ<T> FnF = 0, FnG = 0;
    F_.fluxNormal(X, N, qI, gradq, qB, FnF);
//    G_.fluxNormal(X, N, qI, gradq, qB, FnG);

    pde_.mergeProperties(FnF, FnG, Fn);
  }

  // is the boundary state valid
  bool isValidState(const VectorX& N, const ArrayQ<Real> q) const
  {
    return (F_.isValidState(N, q) && G_.isValidState(N, q));
  }

private:
  const Real& lambda_;
  const PDEHomotopy< NDPrimaryPDE, NDAuxiliaryPDE >& pde_;
  const NDPrimaryBC F_;
  const NDAuxiliaryBC G_;
};

} //namespace SANS

#endif  // BCHOMOTOPY_H
