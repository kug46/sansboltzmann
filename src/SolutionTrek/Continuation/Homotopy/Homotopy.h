// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SANS_HOMOTOPY_H
#define SANS_HOMOTOPY_H

#include "Python/PyDict.h" // Python must be included first
#include "Python/Parameter.h"

#include "tools/SANSException.h"

#include "Field/Field.h"
#include "Field/XField.h"

#include "pde/NDConvert/PDENDConvert_fwd.h"

#include "NonLinearSolver/NonLinearSolver.h"

#include "AlgebraicEquationSet_HTC.h"
#include "AlgebraicEquationSet_HTC2.h"
#include "PDEHomotopy.h"
#include "BCHomotopy.h"

#include "Field/output_gnuplot.h"

#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
// #include "LinearAlgebra/SparseLinAlg/tools/dot.h"
//
// #include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "LinearAlgebra/SparseLinAlg/tools/dot.h"

namespace SANS
{

Real chi(const Real& s, const Real& alpha, const Real& beta)
{
  if (s <= alpha)
    return alpha;
  else if (s > beta)
    return beta;
  else
    return s;
}

//=============================================================================
struct HomotopyParam : noncopyable
{
  //const ParameterDict NonLinearSolver{"NonLinearSolver", NO_DEFAULT, NewtonSolverParam::checkInputs,
  //                                    "The non-linear solver for each Backwards Difference time iter"};

  //const ParameterNumeric<int> order{"order", NO_DEFAULT, 1, 4, "Backwards Difference Order"};

  static void checkInputs(PyDict d);
  static HomotopyParam params;
};


//----------------------------------------------------------------------------//
// Homotopy Scheme, traverses homotopy path between two PDE solutions
//
// template parameters:
//----------------------------------------------------------------------------//
//template<class PhysDim, class PrimaryPDE, class AuxiliaryPDE, class Traits, class XFieldType>
//class Homotopy;

template<class PhysDim, class NDPDEClass, class Traits, class XFieldType>
class Homotopy
{
public:
  typedef typename XFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;

  // typedef AlgebraicEquationSet_HTC<NDPDEClass, Traits, XFieldType> AlgebraicEquationSet_HTCClass;
  typedef AlgebraicEquationSet_HTC2<NDPDEClass, Traits, XFieldType> AlgebraicEquationSet_HTCClass;

  typedef typename AlgebraicEquationSet_HTCClass::AlgEqSetBaseType AlgebraicEquationSetBaseClass;
  typedef typename AlgebraicEquationSet_HTCClass::SystemMatrix SystemMatrix;
  typedef typename AlgebraicEquationSet_HTCClass::SystemVector SystemVector;
  typedef typename AlgebraicEquationSet_HTCClass::SystemNonZeroPattern SystemNonZeroPattern;

  typedef NonLinearSolver<SystemMatrix> NonLinearSolverType;

  // TODO: This is a hack, we should be getting this from the pyDict
  typedef SLA::UMFPACK<SystemMatrix> LinearSolver;
  typedef LinearSolver LinearSolverType;

  Homotopy(const XFieldType& xfld,
           Field<PhysDim, TopoDim, ArrayQ>& qfld,
           const PyDict& nonLinearSolver_dict,
           NDPDEClass& pde,
           const std::vector<int>& CellGroups,
           AlgebraicEquationSetBaseClass& AES,
           Real& lambda,
           int MaxIterations = 1000,
           bool verbose = false) :
    xfld_(xfld),
    qfld_(qfld),
    pde_(pde),
    AES_(AES),
    algEqSetHTC_(xfld, qfld, pde_, CellGroups, AES_ ),
    nonLinearSolver_(algEqSetHTC_, nonLinearSolver_dict),
    linearSolver_(algEqSetHTC_),
    lambda_(lambda),
  MaxIterations_(MaxIterations),
  verbose_(verbose)
  {
    // Nothing
  }

  bool iterate(const int niter);
  bool solve();

protected:
  const XFieldType& xfld_;                            // grid
  Field<PhysDim, TopoDim, ArrayQ>& qfld_;             // solution field at current iter

  NDPDEClass& pde_;
  AlgebraicEquationSetBaseClass& AES_;                // spatial algebraic equation set
  AlgebraicEquationSet_HTCClass algEqSetHTC_;         // HTC algebraic equation set

  NonLinearSolverType nonLinearSolver_;               // nonlinear solver for each iter
  LinearSolverType linearSolver_;                     // linear solver for each iter

  std::string filedir_dump= "";

  Real& lambda_;                                      // Homotopy parameter

  int MaxIterations_;                                 // maximum number of iterations
  bool verbose_;                                      // output more information to console
};

//---------------------------------------------------------------------------//
template<class PhysDim, class NDPDEClass, class Traits, class XFieldType>
bool
Homotopy<PhysDim, NDPDEClass, Traits, XFieldType >::
solve()
{
  return iterate(MaxIterations_);
}

//---------------------------------------------------------------------------//
template<class PhysDim, class NDPDEClass, class Traits, class XFieldType>
bool
Homotopy<PhysDim, NDPDEClass, Traits, XFieldType >::iterate(const int niter)
{
  SystemVector qold(algEqSetHTC_.vectorStateSize());
  SystemVector qpred(algEqSetHTC_.vectorStateSize());
  SystemVector q(algEqSetHTC_.vectorStateSize());
  SystemVector rsd(algEqSetHTC_.vectorEqSize());

  SolveStatus status;

  // Make a unit vector and tangent vectors
  SystemVector N(algEqSetHTC_.vectorStateSize());
  SystemVector v(algEqSetHTC_.vectorStateSize());
  SystemVector dWds(algEqSetHTC_.vectorStateSize());

  // Assign initial values
  N.v0 = 0.0;
  N.v1 = 1.0;
  v.v0 = 0.0;
  v.v1 = 1.0;
  dWds.v0 = 0.0;
  dWds.v1 = 1.0;

  // parameters needed for predictor
  Real s = 0.0;
  Real ds = 0.3;
  int iter = 0;
  Real sigma = 0;
  bool finalSolve = false;

  Real lastLambda;
  Real dlambdads;

  // parameters needed for adaptive iter size
  Real delta_corrector = 0;
  const Real kappa = 3.0;
  const Real alpha_min = 0.05;
  const Real h_max = 1000.0;
  const Real h_min = 0.001;
//  const Real h_min = 0.0;
  Real alpha_i;
  Real omega_i;
  Real Lambda_next;
  Real h_next;
  Real sigma_min;
  Real sigma_max;
  Real sigma_next;

  qold = 0;
  q = 0;

  // Lambda
  lambda_ = 0.0;
  lastLambda = lambda_;
  dlambdads = 1e10;

  // Fill the solution vector from the field
  algEqSetHTC_.fillSystemVector(q);

  // Compute any possible auxiliary variables
  algEqSetHTC_.setSolutionField(q);

  q.v1 = lambda_;

  // Set the tangent
  algEqSetHTC_.updateTangent(dWds);

  while (((lambda_ <= 1.0) && (++iter <= niter)) || finalSolve)
  {
    // Fill the solution vector from the field
    algEqSetHTC_.fillSystemVector(q);

    // Compute any possible auxiliary variables
    algEqSetHTC_.setSolutionField(q);

    // Output current lambda
    if (verbose_)
    {
      std::cout << "Step: " << iter << " lambda: " << lambda_  << " (1 - lambda: " << 1.0 - lambda_
                << ") s: " << s << " d(lambda)/ds: " << dlambdads << " (lastLambda: " << lastLambda << ")" << std::endl;
    }
    lastLambda = lambda_;

    if (verbose_)
    {
      // check that the spatial residual is converged
      rsd = 0;
      lambda_ = 1.0;
      q.v1[0][0] = lambda_;
      algEqSetHTC_.residual(q, rsd);
      const std::vector<std::vector<Real>> nrmRsd = algEqSetHTC_.residualNorm(rsd);
      std::cout << "          Residual Norm: " << std::setprecision(5) << std::scientific << nrmRsd << std::endl;
      lambda_ = lastLambda;
      q.v1[0][0] = lambda_;
    }

    // Move the current solution to the previous one
    qold = q;

#if 0
    std::string filename_base = "tmp/jac";
    filename_base += std::to_string(iter);

    typename AlgebraicEquationSet_HTCClass::SystemNonZeroPattern nnz(algEqSetHTC_.matrixSize());
    algEqSetHTC_.jacobian(nnz);
    typename AlgebraicEquationSet_HTCClass::SystemMatrix jac(nnz);
    algEqSetHTC_.jacobian(jac);
    {
      std::string filename= filename_base + ".mtx";
      std::fstream fout(filename, std::fstream::out);
      WriteMatrixMarketFile(jac, fout);
    }

#endif

    // Solve for predictor step
    if (iter > 1) linearSolver_.solve(N, v);

    // Adjust direction
    if ((dot(v.v0, dWds.v0) + dot(v.v1, dWds.v1)) > 0.0)
//    if ((dot(v.v1, dWds.v1)) > 0.0)
      sigma = 1.0;
    else
      sigma = -1.0;

    // Normalise
    dWds = sigma*v/sqrt(dot(v.v0, v.v0) + dot(v.v1, v.v1));

    // We're going to try to solve with Newton, so no movement along lambda
    if (finalSolve) dWds = N;

    // Update the tangent
    algEqSetHTC_.updateTangent(dWds);

    if (finalSolve)
    {
      qpred = qold;
      if (verbose_)
      {
       std::cout << "Step: " << iter << " trying final solve ... ";
      }
      status = nonLinearSolver_.solve(qpred, q);
      if (verbose_)
      {
        std::cout << (status.converged ? "passed" : "failed") << std::endl;
        std::cout << "Converged at step: " << iter
                  << " with lambda: " << lambda_
                  << " (1 - lambda: " << 1.0 - lambda_
                  << ") and s: " << s << std::endl;

        // check that the spatial residual is converged
        rsd = 0;
        algEqSetHTC_.residual(q, rsd);
        const std::vector<std::vector<Real>> nrmRsd = algEqSetHTC_.residualNorm(rsd);
        std::cout << "          Residual Norm: " << std::setprecision(5) << std::scientific << nrmRsd << std::endl;
      }
      break;
    }

    // predict!
    bool worked = false;
    while (!worked)
    {
      qpred = qold + dWds*ds;
      bool isValidState = algEqSetHTC_.isValidStateSystemVector(qpred);
      if (isValidState)
      {
        worked = true;
      }
      else
      {
        ds = 0.5*ds;
      }
      if (ds < 1e-7)
      {
        if (verbose_) std::cout << "Failed to find valid step at minimum ds" << std::endl;
        return false;
      }
    }
    if (verbose_)
    {
     std::cout << "Step: " << iter << " validity check pass with ds: " << ds << std::endl;
    }

    // correct!
    worked= false;
    while (!worked)
    {
      qpred= qold + dWds*ds;
      if (verbose_)
      {
       std::cout << "Step: " << iter << " trying solve with ds: " << ds << " ... ";
      }
      try
      {
        // correct the current iter to correct
        status = nonLinearSolver_.solve(qpred, q);
      }
      catch (...)
      {
        // Nothing
      }
//      if (status.converged == false)
//      {
//       std::cout << " Warning convergence failure in sub-iter " << iter << std::endl;
//      }
      if (verbose_)
      {
       std::cout << (status.converged ? "passed" : "failed") << std::endl;
      }

      if (!status.converged)
      {
        ds= 0.5*ds;
      }

      if (ds < 1e-7)
      {
        if (verbose_) std::cout << "Failed to converge Newton solve at minimum ds" << std::endl;
        return false;
      }

      worked= status.converged;
    }
    algEqSetHTC_.fillSystemVector(q);

    s += ds;

    lambda_ = q.v1[0][0];
    dlambdads = (lambda_ - lastLambda)/ds; // FD of d(lambda)/ds

    // length of correction
    delta_corrector = sqrt(dot(q.v0 - qpred.v0, q.v0 - qpred.v0)
                           + dot(q.v1 - qpred.v1, q.v1 - qpred.v1));

    alpha_i = acos(chi(1.0/ds*(dot(dWds.v0, q.v0 - qold.v0) + dot(dWds.v1, q.v1 - qold.v1)), -1.0, 1.0));
    omega_i = 2.0*std::abs(sin(0.5*chi(alpha_i, alpha_min, 0.5*PI)));
    sigma_min = sqrt(2.0)/kappa/kappa;
    sigma_max = 2.0*kappa*kappa*sin(0.5*alpha_min);

    if ((std::abs(sin(0.5*alpha_i)) <= sin(0.5*alpha_min)) || (delta_corrector >= sigma_max*ds))
    {
      sigma_next = sigma_max;
    }
    else if ((std::abs(sin(0.5*alpha_i)) >= sqrt(2.0)) || (delta_corrector <= sigma_min*ds))
    {
      sigma_next = sigma_min;
    }
    else
    {
      sigma_next = delta_corrector/ds;
    }

    Lambda_next = chi(sqrt(sigma_next/omega_i), 1.0/kappa, kappa);
    h_next = chi(Lambda_next * ds, h_min, h_max);

//    std::cout << "          delta_i: " << delta_corrector << std::endl;
//    std::cout << "          sigma_next: " << sigma_next << std::endl;
//    std::cout << "          omega_i: " << omega_i << std::endl;
//    std::cout << "          alpha_i: " << alpha_i << std::endl;
//    std::cout << "          alpha_min: " << alpha_min << std::endl;
//    std::cout << "          h_u: " << Lambda_next * ds << std::endl;

    if (!std::isnan(h_next))
    {
      // No divide by zero errors
      ds = h_next;
    }
    else
    {
      // There was a division by zero, so max ds out i.e. ds - h_max
      //  If d(lambda)/ds > 0, then we dont want to step past that, so limit ds
      //  If d(lambda)/ds < 0, then we only want to have lambda -> lambda / 2;
      ds = min(h_max,  max((1.0 - lambda_) / dlambdads, 0.5 * (lambda_ - 0.0) / dlambdads));
    }

//    // check that the spatial residual is converged
//    rsd= 0;
//    algEqSetHTC_.residual(q, rsd);
//
//    if (dot(rsd.v0, rsd.v0) + dot(rsd.v1, rsd.v1) >= 1e-10)
//      SANS_DEVELOPER_EXCEPTION("not converged!");

    // Did we shoot past our goal
    if (lambda_ >= 1.0)
    {
      // Lets limit how far we move based on a linear interpolation
      SystemVector dq(algEqSetHTC_.vectorStateSize());
      dq.v0 = (q.v0 - qold.v0);
      dq.v1 = (q.v1 - qold.v1);
      q = qold + (1.0 - lastLambda) / (lambda_ - lastLambda) * dq;
      // Update any auxiliary variables
      algEqSetHTC_.setSolutionField(q);
      // Linear interpolation to get step size
      ds = ds * (1.0 - lastLambda) / (lambda_ - lastLambda);
      // Force lambda to 1
      lambda_ = 1.0;
      q.v1 = lambda_;
      // We're going to try and solve this w/ Newton now...
      finalSolve = true;
    }

    if (verbose_)
    {
      std::cout << "Step: " << iter << " predicting next ds: " << ds /*<< " delta_i: " << delta_corrector*/ << std::endl;
      std::cout << "          Extrapolated lambda: " << lambda_ + dlambdads * ds << std::endl;
      std::cout << "          Ideal ds: " << (1.0 - lambda_) / dlambdads << std::endl;
    }

    // if (dlambdads < 1e-4) // "near a bifurcation point, needs a kick"
    // {
    //   std::cout << "stand back!!! perturbation ho!" << std::endl;
    //   q= q + 2*dWds*ds;
    // }

    if (lambda_ < 0.0)
    {
      lambda_ = 0.0;
      q.v1 = lambda_;
    }

#if 0
    {
      std::string filename= "tmp/HTC_" + stringify(iter) + ".dat";
      output_Tecplot(pde_.getPrimaryPDE(), xfld_, qfld_, filename);
    }
#endif

    // break; // DEBUG!!!!!
  }

  // update solution field
  algEqSetHTC_.setSolutionField(q);

  return (status.converged and (lambda_ == 1));
}

}

#endif //SANS_HOMOTOPY_H
