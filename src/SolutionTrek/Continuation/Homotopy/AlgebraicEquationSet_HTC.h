// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ALGEBRAICEQUATIONSET_HTC_H
#define ALGEBRAICEQUATIONSET_HTC_H

#include <type_traits>

#include "tools/SANSnumerics.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/tools/norm.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/tools/norm.h"

#include "Field/Field.h"
#include "Field/FieldSequence.h"
#include "Field/XField.h"

#include "LinearAlgebra/AlgebraicEquationSetBase.h"
#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"
#include "Discretization/IntegrateCellGroups.h"

#include "PDEHomotopy.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template<class NDPDEClass, class Traits, class XFieldType>
class AlgebraicEquationSet_HTC
    : public AlgebraicEquationSetTraits<typename NDPDEClass::template MatrixQ<Real>,
                                        typename NDPDEClass::template ArrayQ<Real>,
                                        Traits>::AlgebraicEquationSetBaseClass
{
public:

  typedef AlgebraicEquationSetTraits<typename NDPDEClass::template MatrixQ<Real>,
                                     typename NDPDEClass::template ArrayQ<Real>,
                                     Traits> TraitsType;

  typedef typename TraitsType::AlgebraicEquationSetBaseClass BaseType;

  typedef typename NDPDEClass::PhysDim PhysDim;
  typedef typename XFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDPDEClass::template MatrixQ<Real> MatrixQ;

  typedef typename TraitsType::VectorSizeClass VectorSizeClass;
  typedef typename TraitsType::MatrixSizeClass MatrixSizeClass;

  typedef typename TraitsType::SystemMatrix SystemMatrix;
  typedef typename TraitsType::SystemVector SystemVector;
  typedef typename TraitsType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename TraitsType::SystemMatrixView SystemMatrixView;
  typedef typename TraitsType::SystemVectorView SystemVectorView;
  typedef typename TraitsType::SystemNonZeroPatternView SystemNonZeroPatternView;

  typedef typename BaseType::LinesearchData LinesearchData;

  // Field type to represent the inverse time step
  typedef Field_DG_Cell<PhysDim, TopoDim, Real> TField_DG_CellType;

  AlgebraicEquationSet_HTC(const XFieldType& xfld,
                           Field<PhysDim, TopoDim, ArrayQ>& qfld,
                           NDPDEClass& pde,
                           const std::vector<int>& CellGroups,
                           BaseType& baseAES) :
    xfld_(xfld),
    qfld_(qfld),
    pde_(pde),
    baseAES_(baseAES),
    dWds_(vectorStateSize()),
    iPDE_(baseAES_.indexPDE()),
    iq_(baseAES_.indexQ())
  {
    quadratureOrder_.resize(xfld.nCellGroups(), -1);
    quadratureOrderMin_.resize(quadratureOrder_.size(), 0);

    pde_.setMergeMode(ResidualMode);

    isStaticCondensed_ = baseAES.isStaticCondensed();
  }

  virtual ~AlgebraicEquationSet_HTC() {}

  // Special function to update dWds
  void updateTangent(SystemVectorView& dWds)
  {
    dWds_ = dWds;
  }

  using BaseType::residual;
  using BaseType::jacobian;
  using BaseType::jacobianTranspose;

  //Computes the residual
  virtual void residual(SystemVectorView& rsd) override;

  //Fills jacobian or the non-zero pattern of a jacobian
  virtual void jacobian(SystemMatrixView& mtx       ) override
  {
    // We MUST be in ResidualMode here
    pde_.setMergeMode(ResidualMode);
    baseAES_.jacobian(mtx);
    this->template jacobian<SystemMatrixView&>(mtx, quadratureOrder_ );
  }
  virtual void jacobian(SystemNonZeroPatternView& nz) override
  {
    // We MUST be in ResidualMode here
    pde_.setMergeMode(ResidualMode);
    baseAES_.jacobian(nz);
    this->template jacobian<SystemNonZeroPatternView&>(nz, quadratureOrderMin_ );
  }

  void jacobian(SystemVectorView& b, SystemNonZeroPatternView& nz, bool transposed) override
  {
    pde_.setMergeMode(ResidualMode);
    baseAES_.jacobian( b, nz, transposed );
    this->template jacobian<SystemNonZeroPatternView&>(nz, quadratureOrderMin_ );
  }

  void jacobian(SystemVectorView& b, SystemMatrixView& mtx, bool transposed) override
  {
    pde_.setMergeMode(ResidualMode);
    baseAES_.jacobian( b, mtx, transposed );
    this->template jacobian<SystemMatrixView&>(mtx, quadratureOrder_ );
  }

  //Fills jacobian transpose or the non-zero pattern transpose of a jacobian transpose
  virtual void jacobianTranspose(SystemMatrixView& mtxT       ) override
  {
    // We MUST be in ResidualMode here
    pde_.setMergeMode(ResidualMode);
    baseAES_.jacobian(mtxT);
    jacobian(Transpose(mtxT), quadratureOrder_ );
  }
  virtual void jacobianTranspose(SystemNonZeroPatternView& nzT) override
  {
    // We MUST be in ResidualMode here
    pde_.setMergeMode(ResidualMode);
    baseAES_.jacobian(nzT);
    jacobian(Transpose( nzT), quadratureOrderMin_ );
  }

  // Compute Residual Norm
  virtual std::vector<std::vector<Real>> residualNorm( const SystemVectorView& rsd) const override
  {
    return baseAES_.residualNorm(rsd);
  }

  //Convergence check of the residual
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm) const override
  {
    return baseAES_.convergedResidual(rsdNorm);
  }

  //Convergence check of the residual
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm, int iEq, int iMon) const override
  {
    return baseAES_.convergedResidual(rsdNorm, iEq, iMon);
  }

  // check if residual decreased
  virtual bool decreasedResidual( const std::vector<std::vector<Real>>& rsdNormOld,
                                  const std::vector<std::vector<Real>>& rsdNormNew) const override
  {
    return baseAES_.decreasedResidual(rsdNormOld, rsdNormNew);
  }

  //prints out a residual that could not be decreased and the convergence tolerances
  virtual void printDecreaseResidualFailure(const std::vector<std::vector<Real>>& rsdNorm, std::ostream& os = std::cout) const override
  {
    baseAES_.printDecreaseResidualFailure(rsdNorm, os);
  }

  //Translates the system vector into a solution field
  virtual void setSolutionField(const SystemVectorView& q) override
  {
    int il = q.m() - 1;
    pde_.setLambda(q[il][0]);
    baseAES_.setSolutionField(q);
  }

  //Translates the solution field into a system vector
  virtual void fillSystemVector(SystemVectorView& q) const override { baseAES_.fillSystemVector(q); }

  // Returns the vector an matrix sizes needed for the linear algebra system
  virtual VectorSizeClass vectorEqSize() const override    // vector for equations (rows in matrixSize)
  {
    VectorSizeClass base = baseAES_.vectorEqSize();

    int m = base.m();
    VectorSizeClass aug(m+1);

    for (int i = 0; i < m; i++)
      aug(i) = base(i);

    aug(m) = {1};

    return aug;
  }

  virtual VectorSizeClass vectorStateSize() const override // vector for state DOFs (columns in matrixSize)
  {
    VectorSizeClass base = baseAES_.vectorStateSize();

    int m = base.m();
    VectorSizeClass aug(m+1);

    for (int i = 0; i < m; i++)
      aug(i) = base(i);

    aug(m) = {1};

    return aug;
  }

  virtual MatrixSizeClass matrixSize() const override
  {
    MatrixSizeClass base = baseAES_.matrixSize();

    int m = base.m();
    int n = base.n();
    MatrixSizeClass aug(m+1, n+1);

    for (int i = 0; i < m; i++)
      for (int j = 0; j < n; j++)
        aug(i,j) = base(i,j);

    for (int i = 0; i < m; i++)
    {
      int t = base(i,i).m();
      aug(i,n) = {t,1};
    }

    for (int j = 0; j < n; j++)
    {
      int t = base(j,j).n();
      aug(m,j) = {1,t};
    }

    aug(m,n) = {1,1};

    return aug;
  }

  // Gives the PDE and solution indices in the system
  virtual int indexPDE() const override
  {
    SANS_ASSERT(iPDE_ == baseAES_.indexPDE());
    return iPDE_;
  }
  virtual int indexQ() const override
  {
    SANS_ASSERT(iq_ == baseAES_.indexQ());
    return iq_;
  }

  // Checks to see if proposed solution is physical
  virtual bool isValidStateSystemVector(SystemVectorView& q) override;

  // Returns the side of the residual norm outer vector
  virtual int nResidNorm() const override;

  // MPI communicator for this algebraic equation set
  virtual std::shared_ptr<mpi::communicator> comm() const override { return get<-1>(xfld_).comm(); }

  virtual void syncDOFs_MPI() override { baseAES_.syncDOFs_MPI(); }

  // Dump the localized linesearch parameter info (for debugging purposes)
  virtual bool updateLinesearchDebugInfo(const Real& s, const SystemVectorView& rsd,
                                         LinesearchData& pResData,
                                         LinesearchData& pStepData) const override;

  virtual void dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                                       const LinesearchData& pStepData) const override;


  // Are we having machine precision issues - assume no issues
  virtual bool atMachinePrecision(const SystemVectorView& q, const std::vector<std::vector<Real>>& R0norm) override
  {
    SANS_DEVELOPER_EXCEPTION("AlgebraicEquationSet_HTC<>::atMachinePrecision() Not Implemented!");
    return false;
  }

protected:
  template<class SparseMatrixType>
  void jacobian(SparseMatrixType mtx, const std::vector<int>& quadratureOrder );

  const XFieldType& xfld_;
  Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  NDPDEClass& pde_;
  BaseType& baseAES_;

  // Tangent vector
  SystemVector dWds_;

  const int iPDE_;
  const int iq_;

  std::vector<int> quadratureOrder_;
  std::vector<int> quadratureOrderMin_;

  using BaseType::isStaticCondensed_;
};

template<class NDPDEClass, class Traits, class XFieldType>
void
AlgebraicEquationSet_HTC<NDPDEClass, Traits, XFieldType>::
residual(SystemVectorView& rsd)
{
  // Compute the baseAES residual, which also updates the solution field
  // We MUST be in ResidualMode here
  pde_.setMergeMode(ResidualMode);
  baseAES_.residual(rsd);
}


template<class NDPDEClass, class Traits, class XFieldType>
template<class SparseMatrixType>
void
AlgebraicEquationSet_HTC<NDPDEClass, Traits, XFieldType>::
jacobian(SparseMatrixType jac, const std::vector<int>& quadratureOrder)
{
  // HTC SECTION

  // What is our lambda index?
  int il = jac.m() - 1;
//  pde_.lambda_ = q[il][0];

  // Copy the tangent vector into the bottom row
  int rowMap[] = {0};
  int colMap[] = {0};
  for (int i = 0; i < jac.m(); i++)
  {
    for (int k = 0; k < jac(i,i).m(); k++)
    {
      colMap[0] = k;
      Real val[] = {dWds_[i][k]};
      SANS::DLA::MatrixDView<Real> M(val, 1, 1);
      jac(il,i).scatterAdd(M, rowMap, 1, colMap, 1);
    }
  }

  // Get the residual we need
  pde_.setMergeMode(JacobianMode);
  SystemVector rsd(baseAES_.vectorEqSize());
  rsd = 0.0;
  baseAES_.residual(rsd);
  pde_.setMergeMode(ResidualMode);

  // Copy the residual vector into the right column
  rowMap[0] = 0;
  colMap[0] = 0;
  for (int i = 0; i < il; i++)
  {
    for (int k = 0; k < jac(i,i).m(); k++)
    {
      rowMap[0] = k;
      Real val[] = {rsd[i][k]};
      SANS::DLA::MatrixDView<Real> M(val, 1, 1);
      jac(i,il).scatterAdd(M, rowMap, 1, colMap, 1);
    }
  }

}

template<class NDPDEClass, class Traits, class XFieldType>
bool
AlgebraicEquationSet_HTC<NDPDEClass, Traits, XFieldType>::
isValidStateSystemVector(SystemVectorView& q)
{
  return baseAES_.isValidStateSystemVector(q);
}

template<class NDPDEClass, class Traits, class XFieldType>
int
AlgebraicEquationSet_HTC<NDPDEClass, Traits, XFieldType>::
nResidNorm() const
{
  return baseAES_.nResidNorm();
}

template<class NDPDEClass, class Traits, class XFieldType>
bool
AlgebraicEquationSet_HTC<NDPDEClass, Traits, XFieldType>::
updateLinesearchDebugInfo(const Real& s, const SystemVectorView& rsd,
                          LinesearchData& pResData,
                          LinesearchData& pStepData) const
{
  return baseAES_.updateLinesearchDebugInfo(s, rsd, pResData, pStepData);
}

template<class NDPDEClass, class Traits, class XFieldType>
void
AlgebraicEquationSet_HTC<NDPDEClass, Traits, XFieldType>::
dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                        const LinesearchData& pStepData) const
{
  baseAES_.dumpLinesearchDebugInfo(filenamebase, nonlinear_iter, pStepData);
}

} //namespace SANS

#endif //ALGEBRAICEQUATIONSET_HTC_H
