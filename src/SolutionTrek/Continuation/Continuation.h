// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef CONTINUATION_H
#define CONTINUATION_H

#include "Python/PyDict.h" // Python must be included first
#include "Python/Parameter.h"

#include "pde/NDConvert/PDENDConvert_fwd.h"

#include "NonLinearSolver/NonLinearSolver.h"
#include "SolutionTrek/Continuation/PseudoTime/PseudoTime.h"
#include "SolutionTrek/Continuation/ImplicitResidualSmoothing/ImplicitResidualSmoothing.h"

namespace SANS
{

template<class Temporal>
struct SolverContinuationParams;

//=============================================================================
template<>
struct SolverContinuationParams<TemporalMarch> : noncopyable
{
  struct ContinuationOptions
  {
    typedef DictKeyPair ExtractType;
    const ParameterString Type{"Type", "None", "Continuation type" };
    const ParameterString& key = Type;

    const DictOption None{"None", NonLinearSolverParam::checkInputs};
    const DictOption PseudoTime{"PseudoTime", PseudoTimeParam::checkInputs};
    const DictOption ImplicitResidualSmoothing{"ImplicitResidualSmoothing", ImplicitResidualSmoothingParam::checkInputs};

    const std::vector<DictOption> options{None, PseudoTime, ImplicitResidualSmoothing};
  };
  const ParameterOption<ContinuationOptions> Continuation{"Continuation", NO_DEFAULT, "Solver continuation type"};

  const ParameterBool Verbose{"Verbose", true, "Provide timing information for primal and adjoint solves and estimation"};

  static void checkInputs(PyDict d);
  static SolverContinuationParams params;
};

//=============================================================================
template<>
struct SolverContinuationParams<TemporalSpaceTime> : noncopyable
{
  struct ContinuationOptions
  {
    typedef DictKeyPair ExtractType;
    const ParameterString Type{"Type", "None", "Continuation type" };
    const ParameterString& key = Type;

    const DictOption None{"None", NonLinearSolverParam::checkInputs};
    const DictOption PseudoTime{"PseudoTime", PseudoTimeParam::checkInputs};

    const std::vector<DictOption> options{None, PseudoTime};
  };
  const ParameterOption<ContinuationOptions> Continuation{"Continuation", NO_DEFAULT, "Solver continuation type"};

  const ParameterBool Verbose{"Verbose", true, "Provide timing information for primal and adjoint solves and estimation"};

  static void checkInputs(PyDict d);
  static SolverContinuationParams params;
};


}

#endif //CONTINUATION_H
