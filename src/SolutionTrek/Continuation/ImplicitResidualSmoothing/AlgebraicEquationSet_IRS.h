// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ALGEBRAICEQUATIONSET_IRS_H
#define ALGEBRAICEQUATIONSET_IRS_H

#include <type_traits>

#include "tools/SANSnumerics.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Field/Field.h"
#include "Field/FieldSequence.h"
#include "Field/XField.h"
#include "Field/HField/HField_DG.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/tools/for_each_CellGroup.h"

#include "AlgebraicEquationSetBase_IRS.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/QuadratureOrder.h"

#include "Discretization/Galerkin/ResidualCell_Galerkin_BDF.h"
#include "Discretization/Galerkin/JacobianCell_Galerkin_BDF.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_PTC.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_IRS.h"

#include "SolutionTrek/Continuation/PseudoTime/SetLocalTimeStep.h"

#include "pde/NDConvert/PDENDConvert_fwd.h"

namespace SANS
{

// Integrand identification tag
class IRS;
class IRS_manifold;

template<class DiscTag>
struct IRSIntegrands; // for resolving integrand types

template<>
struct IRSIntegrands<IRS>
{
  template <class PDE>
  using IntegrandCellMass = IntegrandCell_Galerkin_PTC<PDE>;
  template <class PDE>
  using IntegrandCellIRS  = IntegrandCell_Galerkin_IRS<PDE>;
};

//----------------------------------------------------------------------------//
namespace AES_IRS_detail
{
// worker object to do some partially specialized labor
template<class PhysDim, class PDEClass, class Traits, class IntegrandTag, class ParamFieldType>
struct worker;
} // namespace AES_IRS_detail

//----------------------------------------------------------------------------//
// Algebraic equation set class that wraps pseudo-time continuation (PTC) around an existing equation set of spatial discretization
// Note: Assume that PTC uses backward Euler time discretization
//
template<class NDPDEClass, class Traits, class IntegrandTag, class ParamFieldType>
class AlgebraicEquationSet_IRS;

//----------------------------------------------------------------------------//
template<class PhysDim, class PDEClass, class Traits, class IntegrandTag, class ParamFieldType>
class AlgebraicEquationSet_IRS<PDENDConvertSpace<PhysDim, PDEClass>, Traits, IntegrandTag, ParamFieldType>
    : public AlgebraicEquationSetTraits_IRS<typename PDEClass::template MatrixQ<Real>,
                                            typename PDEClass::template ArrayQ<Real>,
                                            Traits>::AlgebraicEquationSetBase_IRSClass
{
  friend struct AES_IRS_detail::worker<PhysDim, PDEClass, Traits, IntegrandTag, ParamFieldType>; // make a friend struct to do some labor

public:
  typedef PDENDConvertSpace<PhysDim, PDEClass> NDPDEClass;
  typedef typename IRSIntegrands<IntegrandTag>::template IntegrandCellMass<NDPDEClass> IntegrandCellMassClass;
  typedef typename IRSIntegrands<IntegrandTag>::template IntegrandCellIRS <NDPDEClass> IntegrandCellIRSClass;

  typedef typename ParamFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDPDEClass::template MatrixQ<Real> MatrixQ;

  typedef AlgebraicEquationSetTraits_IRS<MatrixQ, ArrayQ, Traits> PTCTraitsType;
  typedef typename PTCTraitsType::TraitsType TraitsType;

  typedef typename TraitsType::VectorSizeClass VectorSizeClass;
  typedef typename TraitsType::MatrixSizeClass MatrixSizeClass;

  typedef typename TraitsType::SystemMatrix SystemMatrix;
  typedef typename TraitsType::SystemVector SystemVector;
  typedef typename TraitsType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename TraitsType::SystemMatrixView SystemMatrixView;
  typedef typename TraitsType::SystemVectorView SystemVectorView;
  typedef typename TraitsType::SystemNonZeroPatternView SystemNonZeroPatternView;

  typedef AlgebraicEquationSetBase_IRS<SystemMatrix> PTCAESBaseType;
  typedef typename TraitsType::AlgebraicEquationSetBaseClass SpatialAESBaseType;

  typedef typename SpatialAESBaseType::LinesearchData LinesearchData;

  // Field type to represent the inverse time step (i.e. 1/dt)
  typedef Field_DG_Cell<PhysDim, TopoDim, Real> TField_DG_CellType;

  AlgebraicEquationSet_IRS(const ParamFieldType& paramfld,
                           Field<PhysDim, TopoDim, ArrayQ>& qfld,
                           const NDPDEClass& pde,
                           const QuadratureOrder& quadratureOrder,
                           const std::vector<int>& CellGroups,
                           SpatialAESBaseType& spatialAES) :
    paramfld_(paramfld),
    paramfldpast_(paramfld),
    hfld_(paramfld.getXField()),
    qfld_(qfld),
    qfldpast_(1, qfld), // a sequence of only one item due to backward Euler discretization
    pde_(pde),
    fcnCellMass_(pde, CellGroups),
    fcnCellIRS_(pde, CellGroups),
    dtifld_(paramfld.getXField(), 0, BasisFunctionCategory_Legendre),
    spatial_(spatialAES),
    iPDE_(spatial_.indexPDE()),
    iq_(spatial_.indexQ()),
    cellGroups_(CellGroups),
    quadratureOrder_(quadratureOrder),
    quadratureOrderMin_(quadratureOrder.cellOrders.size(), 0)
  {
    isStaticCondensed_ = spatialAES.isStaticCondensed();
  }

  virtual ~AlgebraicEquationSet_IRS() {}

  using PTCAESBaseType::residual;
  using PTCAESBaseType::jacobian;
  using PTCAESBaseType::jacobianTranspose;

  //Computes the residual
  virtual void residual(SystemVectorView& rsd) override;

  //Fills jacobian or the non-zero pattern of a jacobian
  virtual void jacobian(SystemMatrixView& mtx) override
  {
    spatial_.jacobian(mtx);
    this->template jacobian<SystemMatrixView&>(mtx, quadratureOrder_.cellOrders );
  }
  virtual void jacobian(SystemNonZeroPatternView& nz) override
  {
    spatial_.jacobian(nz);
    this->template jacobian<SystemNonZeroPatternView&>(nz, quadratureOrderMin_ );
  }

  //Fills jacobian transpose or the non-zero pattern transpose of a jacobian transpose
  virtual void jacobianTranspose(SystemMatrixView& mtxT) override
  {
    spatial_.jacobian(mtxT);
    jacobian(Transpose(mtxT), quadratureOrder_.cellOrders );
  }
  virtual void jacobianTranspose(SystemNonZeroPatternView& nzT) override
  {
    spatial_.jacobian(nzT);
    jacobian(Transpose(nzT), quadratureOrderMin_ );
  }


  void jacobian(SystemVectorView& b, SystemNonZeroPatternView& nz, bool transposed) override
  {
    spatial_.jacobian( b, nz, transposed );
    this->template jacobian<SystemNonZeroPatternView&>(nz, quadratureOrderMin_ );
  }

  void jacobian(SystemVectorView& b, SystemMatrixView& mtx, bool transposed) override
  {
    spatial_.jacobian( b, mtx, transposed );
    this->template jacobian<SystemMatrixView&>(mtx, quadratureOrder_.cellOrders );
  }


  // Compute Residual Norm
  virtual std::vector<std::vector<Real>> residualNorm( const SystemVectorView& rsd) const override
  {
    return spatial_.residualNorm(rsd);
  }

  //Convergence check of the residual
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm) const override
  {
    return spatial_.convergedResidual(rsdNorm);
  }

  //Convergence check of the residual
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm, int iEq, int iMon) const override
  {
    return spatial_.convergedResidual(rsdNorm, iEq, iMon);
  }

  // check if residual decreased
  virtual bool decreasedResidual( const std::vector<std::vector<Real>>& rsdNormOld,
                                  const std::vector<std::vector<Real>>& rsdNormNew) const override
  {
    return spatial_.decreasedResidual(rsdNormOld, rsdNormNew);
  }

  //prints out a residual that could not be decreased and the convergence tolerances
  virtual void printDecreaseResidualFailure(const std::vector<std::vector<Real>>& rsdNorm, std::ostream& os = std::cout) const override
  {
     spatial_.printDecreaseResidualFailure(rsdNorm, os);
  }

  //Translates the system vector into a solution field
  virtual void setSolutionField(const SystemVectorView& q) override { spatial_.setSolutionField(q); }

  //Translates the solution field into a system vector
  virtual void fillSystemVector(SystemVectorView& q) const override { spatial_.fillSystemVector(q); }

  // Returns the vector an matrix sizes needed for the linear algebra system
  virtual VectorSizeClass vectorEqSize() const override    { return spatial_.vectorEqSize();    } // vector for equations (rows in matrixSize)
  virtual VectorSizeClass vectorStateSize() const override { return spatial_.vectorStateSize(); } // vector for state DOFs (columns in matrixSize)
  virtual MatrixSizeClass matrixSize() const override      { return spatial_.matrixSize();      }

  // Gives the PDE and solution indices in the system
  virtual int indexPDE() const override { SANS_ASSERT(iPDE_ == spatial_.indexPDE()); return iPDE_; }
  virtual int indexQ() const override { SANS_ASSERT(iq_ == spatial_.indexQ()); return iq_; }

  // update fraction needed for physically valid state
  virtual Real updateFraction(const SystemVectorView& q, const SystemVectorView& dq, const Real maxChangeFraction) const override
  {
    return spatial_.updateFraction(q, dq, maxChangeFraction);
  }

  // Checks to see if proposed solution is physical
  virtual bool isValidStateSystemVector(SystemVectorView& q) override;

  // Returns the side of the residual norm outer vector
  virtual int nResidNorm() const override;

  // Converts processor local indexing to a processor continuous indexing
  virtual std::vector<GlobalContinuousMap> continuousGlobalMap() const override
  {
    return spatial_.continuousGlobalMap();
  }

  // MPI communicator for this algebraic equation set
  virtual std::shared_ptr<mpi::communicator> comm() const override { return spatial_.comm(); }

  virtual void syncDOFs_MPI() override { spatial_.syncDOFs_MPI(); }

  // Dump the localized linesearch parameter info (for debugging purposes)
  virtual bool updateLinesearchDebugInfo(const Real& s, const SystemVectorView& rsd,
                                         LinesearchData& pResData,
                                         LinesearchData& pStepData) const override;

  virtual void dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                                       const LinesearchData& pStepData) const override;

  SpatialAESBaseType& getSpatialAlgebraicEquationSet() override { return spatial_; }

  virtual void setLocalTimeStepField(Real invCFL) override
  {
    // Clear the inverse time field efficiently rather than using operator= (which is an L2 projection)
    for (int n = 0; n < dtifld_.nDOF(); n++)
      dtifld_.DOF(n) = 0.0;

    for_each_CellGroup<TopoDim>::apply(
        SetLocalTimeStep<NDPDEClass, ParamFieldType>(pde_, quadratureOrder_.cellOrders, cellGroups_, invCFL),
        (paramfld_, (qfld_, hfld_, dtifld_)));
    // Note that only DOFs from the cellGroups are used in this AES class, which is generally not the same as all the cell groups of the field

    // synchronize so time steps are consistent accross processors
    dtifld_.syncDOFs_MPI_Cached();
  }

  virtual void dumpSolution(const std::string& filename) const override
  {
    spatial_.dumpSolution(filename);
  }

  virtual void dumpLocalTimeStepField(const std::string& filename) const override
  {
    output_Tecplot(dtifld_, filename + ".dat", {"dti"});
  }

  virtual void storeCurrentSolution() override
  {
    // Copy the current field to the past field for the dU/dt term
    for (int i = 0; i < qfld_.nDOF(); i++)
      qfldpast_[0].DOF(i) = qfld_.DOF(i);
  }


  // Are we having machine precision issues - assume no issues
  virtual bool atMachinePrecision(const SystemVectorView& q, const std::vector<std::vector<Real>>& R0norm) override
  {
    return spatial_.atMachinePrecision(q, R0norm);
  }

protected:
  template<class SparseMatrixType>
  void jacobian(SparseMatrixType mtx, const std::vector<int>& quadratureOrder );

  const ParamFieldType& paramfld_;
  const ParamFieldType& paramfldpast_; // assume paramfld remains the same during time integration
  HField_DG<PhysDim, TopoDim> hfld_;   // grid size h field
  Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  FieldSequence<PhysDim, TopoDim, ArrayQ> qfldpast_;
  const NDPDEClass& pde_; // pde
  const IntegrandCellMassClass fcnCellMass_;
  const IntegrandCellIRSClass fcnCellIRS_;
  TField_DG_CellType dtifld_; // 1/dt field
  SpatialAESBaseType& spatial_;

  const int iPDE_;
  const int iq_;

  const std::vector<int> cellGroups_;                 // cell groups the pseudo time is applied to

  QuadratureOrder quadratureOrder_;
  std::vector<int> quadratureOrderMin_;

  using PTCAESBaseType::isStaticCondensed_;
};


template<class PhysDim, class PDEClass, class Traits, class IntegrandTag, class ParamFieldType>
void
AlgebraicEquationSet_IRS<PDENDConvertSpace<PhysDim, PDEClass>, Traits, IntegrandTag, ParamFieldType>::
residual(SystemVectorView& rsd)
{
  // Compute the spatial residual
  spatial_.residual(rsd);

  // Compute contribution of PTC time-dependent terms to the residual
  AES_IRS_detail::worker<PhysDim, PDEClass, Traits, IntegrandTag, ParamFieldType>::template residual_IRS(this, rsd);
}


template<class PhysDim, class PDEClass, class Traits, class IntegrandTag, class ParamFieldType>
template<class SparseMatrixType>
void
AlgebraicEquationSet_IRS<PDENDConvertSpace<PhysDim, PDEClass>, Traits, IntegrandTag, ParamFieldType>::
jacobian(SparseMatrixType jac, const std::vector<int>& quadratureOrder)
{
  AES_IRS_detail::worker<PhysDim, PDEClass, Traits, IntegrandTag, ParamFieldType>::
    template jacobian_IRS(this, jac, quadratureOrder);
}

template<class PhysDim, class PDEClass, class Traits, class IntegrandTag, class ParamFieldType>
bool
AlgebraicEquationSet_IRS<PDENDConvertSpace<PhysDim, PDEClass>, Traits, IntegrandTag, ParamFieldType>::
isValidStateSystemVector(SystemVectorView& q)
{
  return spatial_.isValidStateSystemVector(q);
}

template<class PhysDim, class PDEClass, class Traits, class IntegrandTag, class ParamFieldType>
int
AlgebraicEquationSet_IRS<PDENDConvertSpace<PhysDim, PDEClass>, Traits, IntegrandTag, ParamFieldType>::
nResidNorm() const
{
  return spatial_.nResidNorm();
}

template<class PhysDim, class PDEClass, class Traits, class IntegrandTag, class ParamFieldType>
bool
AlgebraicEquationSet_IRS<PDENDConvertSpace<PhysDim, PDEClass>, Traits, IntegrandTag, ParamFieldType>::
updateLinesearchDebugInfo(const Real& s, const SystemVectorView& rsd,
                          LinesearchData& pResData,
                          LinesearchData& pStepData) const
{
  return spatial_.updateLinesearchDebugInfo(s, rsd, pResData, pStepData);
}

template<class PhysDim, class PDEClass, class Traits, class IntegrandTag, class ParamFieldType>
void
AlgebraicEquationSet_IRS<PDENDConvertSpace<PhysDim, PDEClass>, Traits, IntegrandTag, ParamFieldType>::
dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                        const LinesearchData& pStepData) const
{
  spatial_.dumpLinesearchDebugInfo(filenamebase, nonlinear_iter, pStepData);
}

//----------------------------------------------------------------------------//
namespace AES_IRS_detail
{

// partial template specialization: IntegrandTag = PTC, i.e. non-manifold discretization
template<class PhysDim, class PDEClass, class Traits, class ParamFieldType>
struct worker<PhysDim, PDEClass, Traits, IRS, ParamFieldType>
{
  typedef AlgebraicEquationSet_IRS<PDENDConvertSpace<PhysDim, PDEClass>, Traits, IRS, ParamFieldType> AES_IRS_Type;
  typedef typename AES_IRS_Type::TopoDim TopoDim;

  template<class SystemVector>
  static void residual_IRS(AES_IRS_Type* AES_IRS, SystemVector& rsd)
  {
    IntegrateCellGroups<TopoDim>::integrate( ResidualCell_Galerkin_BDF(AES_IRS->fcnCellMass_, rsd(AES_IRS->iq_)),
                                             (AES_IRS->paramfld_, AES_IRS->dtifld_),
                                             (AES_IRS->qfld_, AES_IRS->qfldpast_),
                                             AES_IRS->quadratureOrder_.cellOrders.data(),
                                             AES_IRS->cellGroups_.size() );
    IntegrateCellGroups<TopoDim>::integrate( ResidualCell_Galerkin_BDF(AES_IRS->fcnCellIRS_, rsd(AES_IRS->iq_)),
                                             (AES_IRS->paramfld_, AES_IRS->hfld_, AES_IRS->dtifld_),
                                             (AES_IRS->qfld_, AES_IRS->qfldpast_),
                                             AES_IRS->quadratureOrder_.cellOrders.data(),
                                             AES_IRS->cellGroups_.size() );
  }

  template<class SparseMatrixType>
  static void jacobian_IRS(AES_IRS_Type* AES_IRS, SparseMatrixType& jac, const std::vector<int>& quadratureOrder)
  {
    // Get the matrix type, this could be a reference or temporary variable depending on SparseMatrixType
    typedef typename std::result_of<SparseMatrixType(const int, const int)>::type Matrix;

    Matrix jacPDE_q  = jac(AES_IRS->iPDE_,AES_IRS->iq_);

    IntegrateCellGroups<TopoDim>::integrate( JacobianCell_Galerkin_BDF(AES_IRS->fcnCellMass_, jacPDE_q),
                                             (AES_IRS->paramfld_, AES_IRS->dtifld_),
                                             (AES_IRS->qfld_, AES_IRS->qfldpast_),
                                             quadratureOrder.data(),
                                             AES_IRS->cellGroups_.size() );

    IntegrateCellGroups<TopoDim>::integrate( JacobianCell_Galerkin_BDF(AES_IRS->fcnCellIRS_, jacPDE_q),
                                             (AES_IRS->paramfld_, AES_IRS->hfld_, AES_IRS->dtifld_),
                                             (AES_IRS->qfld_, AES_IRS->qfldpast_),
                                             quadratureOrder.data(),
                                             AES_IRS->cellGroups_.size() );
  }
};

// partial template specialization: IntegrandTag = PTC_manifold, i.e. manifold discretization
template<class PhysDim, class PDEClass, class Traits, class ParamFieldType>
struct worker<PhysDim, PDEClass, Traits, IRS_manifold, ParamFieldType>
{
  typedef AlgebraicEquationSet_IRS<PDENDConvertSpace<PhysDim, PDEClass>, Traits, IRS_manifold, ParamFieldType> AES_IRS_Type;
  typedef typename AES_IRS_Type::TopoDim TopoDim;

  template<class SystemVector>
  static void residual_IRS(AES_IRS_Type* AES_IRS, SystemVector& rsd)
  {
    IntegrateCellGroups<TopoDim>::integrate( ResidualCell_Galerkin_BDF(AES_IRS->fcnCellMass_, rsd(AES_IRS->iq_)),
                                             (AES_IRS->paramfldpast_, AES_IRS->paramfld_, AES_IRS->dtifld_),
                                             (AES_IRS->qfld_, AES_IRS->qfldpast_),
                                             AES_IRS->quadratureOrder_.cellOrders.data(),
                                             AES_IRS->cellGroups_.size() );
    IntegrateCellGroups<TopoDim>::integrate( ResidualCell_Galerkin_BDF(AES_IRS->fcnCellIRS_, rsd(AES_IRS->iq_)),
                                             (AES_IRS->paramfldpast_, AES_IRS->paramfld_, AES_IRS->hfld_, AES_IRS->dtifld_),
                                             (AES_IRS->qfld_, AES_IRS->qfldpast_),
                                             AES_IRS->quadratureOrder_.cellOrders.data(),
                                             AES_IRS->cellGroups_.size() );
  }

  template<class SparseMatrixType>
  static void jacobian_IRS(AES_IRS_Type* AES_IRS, SparseMatrixType& jac, const std::vector<int>& quadratureOrder)
  {
    // Get the matrix type, this could be a reference or temporary variable depending on SparseMatrixType
    typedef typename std::result_of<SparseMatrixType(const int, const int)>::type Matrix;

    Matrix jacPDE_q  = jac(AES_IRS->iPDE_,AES_IRS->iq_);

    IntegrateCellGroups<TopoDim>::integrate( JacobianCell_Galerkin_BDF(AES_IRS->fcnCellMass_, jacPDE_q),
                                             (AES_IRS->paramfldpast_, AES_IRS->paramfld_, AES_IRS->dtifld_),
                                             (AES_IRS->qfld_, AES_IRS->qfldpast_),
                                             quadratureOrder.data(),
                                             AES_IRS->cellGroups_.size() );

    IntegrateCellGroups<TopoDim>::integrate( JacobianCell_Galerkin_BDF(AES_IRS->fcnCellIRS_, jacPDE_q),
                                             (AES_IRS->paramfldpast_, AES_IRS->paramfld_, AES_IRS->hfld_, AES_IRS->dtifld_),
                                             (AES_IRS->qfld_, AES_IRS->qfldpast_),
                                             quadratureOrder.data(),
                                             AES_IRS->cellGroups_.size() );
  }
};

} // namespace AES_IRS_detail

} // namespace SANS

#endif //ALGEBRAICEQUATIONSET_IRS_H
