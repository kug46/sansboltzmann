// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ALGEBRAICEQUATIONSETBASE_IRS_H
#define ALGEBRAICEQUATIONSETBASE_IRS_H

#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"

namespace SANS
{

// Base class that adds methods needed for PseudTime Continuation
template<class SystemMatrix>
class AlgebraicEquationSetBase_IRS;

template<class MatrixQ_, class ArrayQ_, class Traits>
struct AlgebraicEquationSetTraits_IRS
{
  typedef ArrayQ_ ArrayQ;
  typedef MatrixQ_ MatrixQ;

  typedef AlgebraicEquationSetTraits<MatrixQ, ArrayQ, Traits> TraitsType;
  typedef AlgebraicEquationSetBase_IRS<typename TraitsType::SystemMatrix> AlgebraicEquationSetBase_IRSClass;
};


//----------------------------------------------------------------------------//
template<class SystemMatrix>
class AlgebraicEquationSetBase_IRS
    : public AlgebraicEquationSetBase<SystemMatrix>
{
public:
  typedef AlgebraicEquationSetBase<SystemMatrix> SpatialAESBaseType;

  virtual ~AlgebraicEquationSetBase_IRS() {}

  virtual SpatialAESBaseType& getSpatialAlgebraicEquationSet() = 0;

  virtual void setLocalTimeStepField(Real invCFL) = 0;

  virtual void dumpLocalTimeStepField(const std::string& filename) const = 0;

  virtual void storeCurrentSolution() = 0;
};

} //namespace SANS

#endif //ALGEBRAICEQUATIONSETBASE_IRS_H
