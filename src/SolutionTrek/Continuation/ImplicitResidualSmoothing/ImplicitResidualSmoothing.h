// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef IMPLICITRESIDUALSMOOTHING_H
#define IMPLICITRESIDUALSMOOTHING_H

#include <limits>
#include <memory> // std::unique_ptr
#include <limits> // std::numeric_limits

#include "Python/PyDict.h" // Python must be included first
#include "Python/Parameter.h"

#include "tools/SANSException.h"

#include "NonLinearSolver/NonLinearSolver.h"

#include "AlgebraicEquationSetBase_IRS.h"

namespace SANS
{

//=============================================================================
struct ImplicitResidualSmoothingParam : noncopyable
{
  //const ParameterDict NonLinearSolver{"NonLinearSolver", NO_DEFAULT, NonLinearSolverParam::checkInputs,
  //                                    "The non-linear solver for each pseudo time step"
  //                                   };
  const ParameterOption<NonLinearSolverParam::SolverOptions>& NonLinearSolver;

  const ParameterNumeric<int > MaxIterations{"MaxIterations", 100, 0, NO_LIMIT, "Maximum Number of PTC Iterations"};

  const ParameterNumeric<Real> invCFL{"invCFL", 1, 0, NO_LIMIT, "Initial inverse CFL number"};

  const ParameterNumeric<Real> invCFL_max{"invCFL_max", 1000, 0, NO_LIMIT, "Maximum inverse CFL number"};
  const ParameterNumeric<Real> invCFL_min{"invCFL_min",    0, 0, NO_LIMIT, "Minimum inverse CFL number"};

  const ParameterNumeric<Real> CFLDecreaseFactor{"CFLDecreaseFactor", 0.1, 0,        1, "Factor for deceasing the CFL number"};
  const ParameterNumeric<Real> CFLIncreaseFactor{"CFLIncreaseFactor",   2, 1, NO_LIMIT, "Factor for increasing the CFL number"};

  const ParameterNumeric<Real> CFLPartialStepDecreaseFactor
  {
    "CFLPartialStepDecreaseFactor", 0.9, 0, 1,
    "Factor for deceasing the CFL number on linesearch partial step"
  };

  const ParameterBool Verbose{"Verbose", false, "Verbose Pseudo Time output"};

  const ParameterFileName ResidualHistoryFile{"ResidualHistoryFile", std::ios_base::out, "", "Dumps spatial residual history to give file name"};
  const ParameterFileName LineSearchHistoryFile
  {
    "LineSearchHistoryFile",
    std::ios_base::out,
    "",
    "Dumps spatial linesearch history to give file name"
  };
  ImplicitResidualSmoothingParam();

  static void checkInputs(PyDict d);
  static ImplicitResidualSmoothingParam params;
};


//----------------------------------------------------------------------------//
// Pseudo time continuation (PTC) solution scheme of nonlinear system
//----------------------------------------------------------------------------//
template<class SystemMatrix>
class ImplicitResidualSmoothing
{
public:
  typedef AlgebraicEquationSetBase_IRS<SystemMatrix> PTCAESBaseType;

  typedef typename PTCAESBaseType::SpatialAESBaseType SpatialAESBaseType;

  typedef NonLinearSolver<SystemMatrix> NonLinearSolverType;

  typedef typename VectorType<SystemMatrix>::type SystemVector;
  typedef typename NonZeroPatternType<SystemMatrix>::type SystemNonZeroPattern;

  ImplicitResidualSmoothing(const Real invCFL0, const Real invCFL_max, const Real invCFL_min,
                            const Real CFLDecreaseFactor, const Real CFLIncreaseFactor,
                            const Real CFLPartialStepDecreaseFactor,
                            const PyDict& nonLinearSolver_dict,
                            PTCAESBaseType& algEqSetPTC,
                            bool verbose = false,
                            int MaxIterations = 1000,
                            std::string residualfilename = "",
                            std::string linesearchfilename = "" ) :
   MaxIterations_(MaxIterations),
   invCFL_max_(invCFL_max),
   invCFL_(invCFL0),
   invCFL_min_(invCFL_min),
   CFLDecreaseFactor_(CFLDecreaseFactor),
   CFLIncreaseFactor_(CFLIncreaseFactor),
   CFLPartialStepDecreaseFactor_(CFLPartialStepDecreaseFactor),
   algEqSetIRS_(algEqSetPTC),
   spatial_(algEqSetIRS_.getSpatialAlgebraicEquationSet()),
   nonLinearSolver_(algEqSetIRS_, nonLinearSolver_dict),
   verbose_(verbose)
 {
   // Make sure the parameters given are in the correct range
   SANS_ASSERT_MSG(CFLDecreaseFactor_ > 0, "CFLDecreaseFactor must be positive");
   SANS_ASSERT_MSG(CFLDecreaseFactor_ <= 1, "CFLDecreaseFactor must be less than or equal to 1");
   SANS_ASSERT_MSG(CFLIncreaseFactor_ >= 1, "CFLIncreaseFactor must be less than or equal to 1");
   SANS_ASSERT_MSG(CFLPartialStepDecreaseFactor_ > 0, "CFLPartialStepDecreaseFactor must be positive");
   SANS_ASSERT_MSG(CFLPartialStepDecreaseFactor_ <= 1, "CFLPartialStepDecreaseFactor must be less than or equal to 1");

   if (!residualfilename.empty() && spatial_.comm()->rank() == 0)
   { // TODO: this block has not been unit tested ...
     fhist_.open(residualfilename, std::ios_base::app);
     // add a new zone to distinguish between restarts
     fhist_ << "ZONE" << std::endl;
   }

   if (!linesearchfilename.empty() && spatial_.comm()->rank() == 0)
   { // TODO: this block has not been unit tested ...
     flinesearchhist_.open(linesearchfilename, std::ios_base::app);
     // add a new zone to distinguish between restarts
     flinesearchhist_ << "ZONE" << std::endl;
   }
 }

  ImplicitResidualSmoothing(const PyDict& d, PTCAESBaseType& algEqSetPTC) :
    ImplicitResidualSmoothing( d.get(ImplicitResidualSmoothingParam::params.invCFL),
                               d.get(ImplicitResidualSmoothingParam::params.invCFL_max),
                               d.get(ImplicitResidualSmoothingParam::params.invCFL_min),
                               d.get(ImplicitResidualSmoothingParam::params.CFLDecreaseFactor),
                               d.get(ImplicitResidualSmoothingParam::params.CFLIncreaseFactor),
                               d.get(ImplicitResidualSmoothingParam::params.CFLPartialStepDecreaseFactor),
                               d,
                               algEqSetPTC,
                               d.get(ImplicitResidualSmoothingParam::params.Verbose),
                               d.get(ImplicitResidualSmoothingParam::params.MaxIterations),
                               d.get(ImplicitResidualSmoothingParam::params.ResidualHistoryFile),
                               d.get(ImplicitResidualSmoothingParam::params.LineSearchHistoryFile) ) {}

  bool solve();
  bool iterate(const int nSteps);

protected:
  void updateCFL(SolveStatus status);

  int MaxIterations_;                                 // maximum number of interations of PTC
  Real invCFL_max_;                                   // maximum inverse Courant–Friedrichs–Lewy condition number
  Real invCFL_;                                       // current inverse Courant–Friedrichs–Lewy condition number
  Real invCFL_min_;                                   // minimum inverse Courant–Friedrichs–Lewy condition number
  Real CFLDecreaseFactor_;                            // CFL shrink factor
  Real CFLIncreaseFactor_;                            // CFL growth factor
  Real CFLPartialStepDecreaseFactor_;                 // CFL shrink factor for partial step on linesearch

  PTCAESBaseType& algEqSetIRS_;                       // PTC algebraic equation set
  SpatialAESBaseType& spatial_;                       // spatial algebraic equation set

  NonLinearSolverType nonLinearSolver_;               // nonlinear solver for each step

  bool verbose_;                                      // output CFL to the console

  std::ofstream fhist_;                               // residual history file
  std::ofstream flinesearchhist_;                     // linesearch step size history file

  Real alpha_target_ = 0.7;
  Real power_ = 0.5;
};

#define NEWLOGIC 0
#if NEWLOGIC
//---------------------------------------------------------------------------//
template<class SystemMatrix>
void
ImplicitResidualSmoothing< SystemMatrix >::
updateCFL(SolveStatus status)
{
  int comm_rank = spatial_.comm()->rank();

  // Increase the CFL --> getting closer to steady state
  // Decrease the CFL --> refining transient state

  // NB: Logic here to pick the largest \alpha^{*} of all from residuals
  Real alpha_star = 0.0;

  // U_{0} + \mu \Delta U valid \oftaal \mu \in [0,1]
  if (abs(status.linesearch.validStepSize - 1.0) < 1e-5)
  {
    if (comm_rank == 0)
    {
      std::cout << "status.linesearch.validStepSize: " << status.linesearch.validStepSize << "\n";
      std::cout << "Always Valid: " << std::endl;
    }
    for (std::size_t i = 0; i < status.linesearch.initialResidual.size(); i++)
    {
      for (std::size_t j = 0; j < status.linesearch.initialResidual[i].size(); j++)
      {
        if (!algEqSetPTC_.convergedResidual(status.linesearch.initialResidual, i, j))
        {
          // \alpha^{*} = \alpha(1)
          Real alpha_candidate = status.linesearch.validResidual[i][j] / status.linesearch.initialResidual[i][j];

          if (comm_rank == 0)
            std::cout << " " << i << " " << j << ": " << alpha_candidate << std::endl;

          alpha_star = std::max(alpha_star, alpha_candidate);
        }
      }
    }
  }
  else
  {
    if (comm_rank == 0)
      std::cout << "Not always Valid: " << status.linesearch.validStepSize << std::endl;

    for (std::size_t i = 0; i < status.linesearch.initialResidual.size(); i++)
    {
      for (std::size_t j = 0; j < status.linesearch.initialResidual[i].size(); j++)
      {
        if (!algEqSetPTC_.convergedResidual(status.linesearch.initialResidual, i, j))
        {
          // \alpha^{*} = \alpha(\mu_{f}) / \mu_{v}
          Real alpha_candidate = status.linesearch.minimizedResidual[i][j] / status.linesearch.initialResidual[i][j];
          alpha_candidate /= status.linesearch.validStepSize;

          if (comm_rank == 0)
            std::cout << " " << i << " " << j << ": " << alpha_candidate << std::endl;

          alpha_star = std::max(alpha_star, alpha_candidate);
        }
      }
    }
  }

  // keep invCFL in specified bounds
  invCFL_ = pow(alpha_star / alpha_target_, power_) * invCFL_;
  invCFL_ = min(invCFL_, invCFL_max_);
  invCFL_ = max(invCFL_, invCFL_min_);
  if (comm_rank == 0)
    std::cout << alpha_star << " " << invCFL_ << std::endl;
}
#else
//---------------------------------------------------------------------------//
template<class SystemMatrix>
void
ImplicitResidualSmoothing< SystemMatrix >::
updateCFL(SolveStatus status)
{
  // Increase the CFL --> getting closer to steady state
  // Decrease the CFL --> refining transient state

  if (status.linesearch.status == LineSearchStatus::Failure)
  {
    // Decrease the CFL
    if ( invCFL_ > 0 )
      invCFL_ = min(invCFL_/CFLDecreaseFactor_, invCFL_max_);
    else // in case the solver gets trapped when invCFL_==0
      invCFL_ = min(sqrt(std::numeric_limits<Real>::epsilon())/CFLDecreaseFactor_, invCFL_max_);
  }
  else if (status.linesearch.status == LineSearchStatus::PartialStep || !status.linearSolveStatus.success)
  {
    // Decrease the CFL a little
    invCFL_ = min(invCFL_/CFLPartialStepDecreaseFactor_, invCFL_max_);
  }
  else if (status.linesearch.status == LineSearchStatus::LimitedStep)
  {
    // Decrease the CFL a little
    //invCFL_ = min(invCFL_/0.9, invCFL_max_);
  }
  else if (status.linesearch.status == LineSearchStatus::FullStep)
  {
    // Increase the CFL
    invCFL_ = max(invCFL_/CFLIncreaseFactor_, invCFL_min_);
  }

}
#endif
#undef NEWLOGIC

//---------------------------------------------------------------------------//
template< class SystemMatrix >
bool
ImplicitResidualSmoothing< SystemMatrix >::solve()
{
  return iterate(MaxIterations_);
}

//---------------------------------------------------------------------------//
template< class SystemMatrix >
bool
ImplicitResidualSmoothing< SystemMatrix >::iterate(const int nSteps)
{
  SystemVector qold(algEqSetIRS_.vectorStateSize());
  SystemVector q(algEqSetIRS_.vectorStateSize());
  SystemVector rsd_spatial(spatial_.vectorEqSize());

  int comm_rank = spatial_.comm()->rank();

  // default initialization
  bool convergedSpatial = false;
  SolveStatus statusNonlinearSolvePTC;

  // Compute potentially any auxiliary variables
  algEqSetIRS_.fillSystemVector(q);

  // Pseudo time continuation iterations
  for (int step = 0; step < nSteps; step++)
  {
    // Move the current solution to the previous one
    qold = q;

    // check that the spatial residual is converged
    rsd_spatial = 0; // resetting residual to zero is required!
    spatial_.residual(q, rsd_spatial);
    std::vector<std::vector<Real>> nrmRsd = spatial_.residualNorm(rsd_spatial);

    // dump residual history to file
    if (verbose_ || fhist_.is_open() || flinesearchhist_.is_open())
    {
      std::vector<std::vector<Real>> nrmRsd = spatial_.residualNorm(rsd_spatial);
      if (comm_rank == 0)
      {
        // print if requested
        if (verbose_)
        {
          std::cout << "---------------------------------" << std::endl;
          std::cout << "Step: " << step << ", Inverse CFL: " << invCFL_ << std::endl;
          std::cout << "  Spatial Res_L2norm = " << std::setprecision(5) << std::scientific << nrmRsd << std::endl;
        }

        // save if requested
        if (fhist_.is_open())
          fhist_ << step << ", " << invCFL_ << ", " << std::setprecision(16) << std::scientific << nrmRsd << std::endl;

        // save if requested
        if (flinesearchhist_.is_open() && (step > 0))
          flinesearchhist_ << step
                           << ", "
                           << std::setprecision(16)
                           << std::scientific
                           << nonLinearSolver_.getLineSearchHistory() << std::endl;
      }
    }

    // check convergence of the spatial equation set
    if (spatial_.convergedResidual(nrmRsd))
    {
      convergedSpatial = true;
      break;
    }

    // Copy the current field to the past field for the dU/dt term
    algEqSetIRS_.storeCurrentSolution();

    // Set the local inverse time step field
    algEqSetIRS_.setLocalTimeStepField(invCFL_);

#if 0 // Debugging
    algEqSetIRS_.dumpLocalTimeStepField("tmp/dtifld.dat");
#endif

    // Solve the current step
    statusNonlinearSolvePTC = nonLinearSolver_.solve(qold, q);

    if ((statusNonlinearSolvePTC.atMachinePrecision) && (step > 0))
    {
      std::cout << "Detected machine precision issues" << std::endl;
      // We think we're at out machine precision floor...
      spatial_.setSolutionField(qold);
      // We probably converged?
      return true;
    }

    if ((statusNonlinearSolvePTC.linesearch.status == LineSearchStatus::Failure) && (invCFL_ >= invCFL_max_))
    {
      // Well, line search hits the minimum CFL (i.e. maximum invCFL = 1/CFL) and still couldn't make progress...
      spatial_.setSolutionField(qold);
      return convergedSpatial;
    }

    // Update the inverse CFL
    updateCFL(statusNonlinearSolvePTC);
  }

  spatial_.setSolutionField(q);
  return convergedSpatial;
}

}

#endif //IMPLICITRESIDUALSMOOTHING_H
