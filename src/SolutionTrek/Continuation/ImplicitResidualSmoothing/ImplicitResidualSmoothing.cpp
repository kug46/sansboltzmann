// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "ImplicitResidualSmoothing.h"

#define PYDICT_INSTANTIATE
#include "Python/PyDict_impl.h"

namespace SANS
{

ImplicitResidualSmoothingParam::ImplicitResidualSmoothingParam()
  : NonLinearSolver(NonLinearSolverParam::params.NonLinearSolver)
{}

// cppcheck-suppress passedByValue
void ImplicitResidualSmoothingParam::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.NonLinearSolver));
  allParams.push_back(d.checkInputs(params.MaxIterations));
  allParams.push_back(d.checkInputs(params.invCFL));
  allParams.push_back(d.checkInputs(params.invCFL_max));
  allParams.push_back(d.checkInputs(params.invCFL_min));
  allParams.push_back(d.checkInputs(params.CFLDecreaseFactor));
  allParams.push_back(d.checkInputs(params.CFLIncreaseFactor));
  allParams.push_back(d.checkInputs(params.CFLPartialStepDecreaseFactor));
  allParams.push_back(d.checkInputs(params.Verbose));
  allParams.push_back(d.checkInputs(params.ResidualHistoryFile));
  allParams.push_back(d.checkInputs(params.LineSearchHistoryFile));
  d.checkUnknownInputs(allParams);
}

ImplicitResidualSmoothingParam ImplicitResidualSmoothingParam::params;

}
