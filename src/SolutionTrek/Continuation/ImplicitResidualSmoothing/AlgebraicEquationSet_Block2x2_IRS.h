// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ALGEBRAICEQUATIONSET_BLOCK2X2_IRS_H
#define ALGEBRAICEQUATIONSET_BLOCK2X2_IRS_H

#include <type_traits>

#include "tools/SANSnumerics.h"
#include "tools/output_std_vector.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "LinearAlgebra/BlockLinAlg/MatrixBlock_2x2.h"
#include "LinearAlgebra/BlockLinAlg/VectorBlock_2.h"

#include "Discretization/Block/AlgebraicEquationSet_Block2x2.h"

#include "AlgebraicEquationSetBase_IRS.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Abbreviations:
// SM = system matrix
//
template<class SM00, class SM01,
         class SM10, class SM11>
class AlgebraicEquationSet_Block2x2_IRS
    : public AlgebraicEquationSet_Block2x2< SM00, SM01,
                                            SM10, SM11,
                                            AlgebraicEquationSetBase_IRS >
{
public:
  typedef AlgebraicEquationSet_Block2x2<SM00, SM01,
                                        SM10, SM11,
                                        AlgebraicEquationSetBase_IRS > BaseType;

  typedef typename BaseType::SystemMatrix SystemMatrix;
  typedef typename BaseType::SystemNonZeroPattern SystemNonZeroPattern;
  typedef typename BaseType::SystemVector SystemVector;

  typedef AlgebraicEquationSet_Block2x2<SM00, SM01,
                                        SM10, SM11 > SpatialAESType;

  typedef AlgebraicEquationSetBase_IRS< SM00 > BaseType00;
  typedef JacobianParamBase           < SM01 > BaseType01;
  typedef JacobianParamBase           < SM10 > BaseType10;
  typedef AlgebraicEquationSetBase    < SM11 > BaseType11;

  typedef typename MatrixSizeType<SystemMatrix>::type MatrixSizeClass;
  typedef typename VectorSizeType<SystemVector>::type VectorSizeClass;

  typedef typename BaseType::LinesearchData LinesearchData;

  AlgebraicEquationSet_Block2x2_IRS(BaseType00& AES00,
                                    BaseType01& JP01,
                                    BaseType10& JP10,
                                    BaseType11& AES11) :
    BaseType(AES00,JP01,JP10,AES11),
    AES00_(AES00),
    spatial_(AES00.getSpatialAlgebraicEquationSet(),JP01,JP10,AES11)
  {
    // Nothing
  }

  virtual ~AlgebraicEquationSet_Block2x2_IRS() {}

  virtual typename SpatialAESType::BaseType&
  getSpatialAlgebraicEquationSet() override
  {
    return spatial_;
  }

  virtual void setLocalTimeStepField(Real invCFL) override
  {
    AES00_.setLocalTimeStepField(invCFL);
  }

  virtual void dumpLocalTimeStepField(const std::string& filename) const override
  {
    AES00_.dumpLocalTimeStepField(filename);
  }

  virtual void storeCurrentSolution() override
  {
    AES00_.storeCurrentSolution();
  }

  using BaseType::residual;
  using BaseType::jacobian;
  using BaseType::jacobianTranspose;
  using BaseType::residualNorm;
  using BaseType::convergedResidual;
  using BaseType::decreasedResidual;
  using BaseType::printDecreaseResidualFailure;
  using BaseType::setSolutionField;
  using BaseType::fillSystemVector;
  using BaseType::vectorEqSize;
  using BaseType::vectorStateSize;
  using BaseType::matrixSize;
  using BaseType::indexPDE;
  using BaseType::indexQ;
  using BaseType::isValidStateSystemVector;
  using BaseType::nResidNorm;
  using BaseType::updateLinesearchDebugInfo;
  using BaseType::dumpLinesearchDebugInfo;

protected:
  BaseType00& AES00_;
  using BaseType::JP01_;
  using BaseType::JP10_;
  using BaseType::AES11_;
  SpatialAESType spatial_;
};

} //namespace SANS

#endif /* ALGEBRAICEQUATIONSET_BLOCK2X2_IRS_H */
