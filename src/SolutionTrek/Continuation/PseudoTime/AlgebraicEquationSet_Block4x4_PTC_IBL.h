// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_SOLUTIONTREK_CONTINUATION_PSEUDOTIME_ALGEBRAICEQUATIONSET_BLOCK4X4_PTC_IBL_H_
#define SRC_SOLUTIONTREK_CONTINUATION_PSEUDOTIME_ALGEBRAICEQUATIONSET_BLOCK4X4_PTC_IBL_H_

#include <type_traits>

#include "tools/SANSnumerics.h"
#include "tools/output_std_vector.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "LinearAlgebra/BlockLinAlg/MatrixBlock_4x4.h"
#include "LinearAlgebra/BlockLinAlg/VectorBlock_4.h"

#include "Discretization/Block/AlgebraicEquationSet_Block4x4.h"

#include "AlgebraicEquationSetBase_PTC.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Abbreviations:
// SM = system matrix
//
// Currently assumes that PTC is only applied to the IBL equations.
//
template<class SM00, class SM01, class SM02, class SM03,
         class SM10, class SM11, class SM12, class SM13,
         class SM20, class SM21, class SM22, class SM23,
         class SM30, class SM31, class SM32, class SM33>
class AlgebraicEquationSet_Block4x4_PTC_IBL
    : public AlgebraicEquationSet_Block4x4<SM00, SM01, SM02, SM03,
                                           SM10, SM11, SM12, SM13,
                                           SM20, SM21, SM22, SM23,
                                           SM30, SM31, SM32, SM33,
                                           AlgebraicEquationSetBase_PTC>
{
public:
  typedef AlgebraicEquationSet_Block4x4<SM00, SM01, SM02, SM03,
                                        SM10, SM11, SM12, SM13,
                                        SM20, SM21, SM22, SM23,
                                        SM30, SM31, SM32, SM33,
                                        AlgebraicEquationSetBase_PTC> BaseType;

  typedef typename BaseType::SystemMatrix SystemMatrix;
  typedef typename BaseType::SystemNonZeroPattern SystemNonZeroPattern;
  typedef typename BaseType::SystemVector SystemVector;

  typedef AlgebraicEquationSet_Block4x4<SM00, SM01, SM02, SM03,
                                        SM10, SM11, SM12, SM13,
                                        SM20, SM21, SM22, SM23,
                                        SM30, SM31, SM32, SM33> SpatialAESType;

  typedef AlgebraicEquationSetBase<SM00> BaseType00;
  typedef JacobianParamBase       <SM01> BaseType01;
  typedef JacobianParamBase       <SM02> BaseType02;
  typedef JacobianParamBase       <SM03> BaseType03;

  typedef JacobianParamBase       <SM10> BaseType10;
  typedef AlgebraicEquationSetBase<SM11> BaseType11;
  typedef JacobianParamBase       <SM12> BaseType12;
  typedef JacobianParamBase       <SM13> BaseType13;

  typedef JacobianParamBase           <SM20> BaseType20;
  typedef JacobianParamBase           <SM21> BaseType21;
  typedef AlgebraicEquationSetBase_PTC<SM22> BaseType22;
  typedef JacobianParamBase           <SM23> BaseType23;

  typedef JacobianParamBase       <SM30> BaseType30;
  typedef JacobianParamBase       <SM31> BaseType31;
  typedef JacobianParamBase       <SM32> BaseType32;
  typedef AlgebraicEquationSetBase<SM33> BaseType33;

  typedef typename MatrixSizeType<SystemMatrix>::type MatrixSizeClass;
  typedef typename VectorSizeType<SystemVector>::type VectorSizeClass;

  typedef typename BaseType::LinesearchData LinesearchData;

  AlgebraicEquationSet_Block4x4_PTC_IBL(
      BaseType00& AE00, BaseType01& JP01, BaseType02& JP02, BaseType03& JP03,
      BaseType10& JP10, BaseType11& AE11, BaseType12& JP12, BaseType13& JP13,
      BaseType20& JP20, BaseType21& JP21, BaseType22& AE22, BaseType23& JP23,
      BaseType30& JP30, BaseType31& JP31, BaseType32& JP32, BaseType33& AE33) :
    BaseType(AE00,JP01,JP02,JP03,
             JP10,AE11,JP12,JP13,
             JP20,JP21,AE22,JP23,
             JP30,JP31,JP32,AE33),
    AE22_(AE22),
    spatial_(AE00,JP01,JP02,JP03,
             JP10,AE11,JP12,JP13,
             JP20,JP21,AE22_.getSpatialAlgebraicEquationSet(),JP23,
             JP30,JP31,JP32,AE33) {}

  virtual ~AlgebraicEquationSet_Block4x4_PTC_IBL() {}

  virtual void residualPseudoTemporal(SystemVector& rsd) override
  {
    AE22_.residualPseudoTemporal(rsd.v2);
  };

  virtual void jacobianPseudoTemporal(SystemMatrix& mtx) override
  {
    AE22_.jacobianPseudoTemporal(mtx.m22);
  };

  virtual void jacobianPseudoTemporal(SystemNonZeroPattern& nz) override
  {
    AE22_.jacobianPseudoTemporal(nz.m22);
  };

  virtual void jacobianPseudoTemporalTranspose(SystemMatrix& mtx) override
  {
    AE22_.jacobianPseudoTemporalTranspose(mtx.m22);
  };

  virtual void jacobianPseudoTemporalTranspose(SystemNonZeroPattern& nz) override
  {
    AE22_.jacobianPseudoTemporalTranspose(nz.m22);
  };

  virtual typename SpatialAESType::BaseType& getSpatialAlgebraicEquationSet() override
  {
    return spatial_;
  }

  virtual void setLocalTimeStepField(Real invCFL) override
  {
    AE22_.setLocalTimeStepField(invCFL);
  }

  virtual void dumpLocalTimeStepField(const std::string& filename) const override
  {
    AE22_.dumpLocalTimeStepField(filename);
  }

  virtual void storeCurrentSolution() override
  {
    AE22_.storeCurrentSolution();
  }

  //TODO: these methods are already public in the base class. Do they need to be introduced here again?
//  using BaseType::residual;
//  using BaseType::jacobian;
//  using BaseType::jacobianTranspose;
//  using BaseType::residualNorm;
//  using BaseType::convergedResidual;
//  using BaseType::decreasedResidual;
//  using BaseType::printDecreaseResidualFailure;
//  using BaseType::setSolutionField;
//  using BaseType::fillSystemVector;
//  using BaseType::vectorEqSize;
//  using BaseType::vectorStateSize;
//  using BaseType::matrixSize;
//  using BaseType::indexPDE;
//  using BaseType::indexQ;
//  using BaseType::isValidStateSystemVector;
//  using BaseType::nResidNorm;
//  using BaseType::updateLinesearchDebugInfo;
//  using BaseType::dumpLinesearchDebugInfo;

protected:
  BaseType22& AE22_;
  SpatialAESType spatial_;
};

} //namespace SANS

#endif /* SRC_SOLUTIONTREK_CONTINUATION_PSEUDOTIME_ALGEBRAICEQUATIONSET_BLOCK4X4_PTC_IBL_H_ */
