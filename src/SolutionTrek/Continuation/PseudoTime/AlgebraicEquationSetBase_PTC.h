// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ALGEBRAICEQUATIONSETBASE_PTC_H
#define ALGEBRAICEQUATIONSETBASE_PTC_H

#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"

namespace SANS
{

// Base class that adds methods needed for PseudTime Continuation
template<class SystemMatrix>
class AlgebraicEquationSetBase_PTC;

template<class MatrixQ_, class ArrayQ_, class Traits>
struct AlgebraicEquationSetTraits_PTC
{
  typedef ArrayQ_ ArrayQ;
  typedef MatrixQ_ MatrixQ;

  typedef AlgebraicEquationSetTraits<MatrixQ, ArrayQ, Traits> TraitsType;
  typedef AlgebraicEquationSetBase_PTC<typename TraitsType::SystemMatrix> AlgebraicEquationSetBase_PTCClass;
};


//----------------------------------------------------------------------------//
template<class SystemMatrix>
class AlgebraicEquationSetBase_PTC
    : public AlgebraicEquationSetBase<SystemMatrix>
{
public:
  typedef AlgebraicEquationSetBase<SystemMatrix> SpatialAESBaseType;

  using typename SpatialAESBaseType::SystemVectorView;
  using typename SpatialAESBaseType::SystemMatrixView;
  using typename SpatialAESBaseType::SystemNonZeroPatternView;

  virtual ~AlgebraicEquationSetBase_PTC() {}

  virtual void residualPseudoTemporal(SystemVectorView& rsd) = 0;

  virtual void jacobianPseudoTemporal(SystemMatrixView& mtx) = 0;
  virtual void jacobianPseudoTemporal(SystemNonZeroPatternView& nz) = 0;

  virtual void jacobianPseudoTemporalTranspose(SystemMatrixView& mtx) = 0;
  virtual void jacobianPseudoTemporalTranspose(SystemNonZeroPatternView& nz) = 0;

  virtual SpatialAESBaseType& getSpatialAlgebraicEquationSet() = 0;

  virtual void setLocalTimeStepField(Real invCFL) = 0;

  virtual void dumpLocalTimeStepField(const std::string& filename) const = 0;

  virtual void storeCurrentSolution() = 0;
};

} //namespace SANS

#endif //ALGEBRAICEQUATIONSETBASE_PTC_H
