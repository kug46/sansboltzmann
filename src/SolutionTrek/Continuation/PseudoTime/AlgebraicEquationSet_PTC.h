// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ALGEBRAICEQUATIONSET_PTC_H
#define ALGEBRAICEQUATIONSET_PTC_H

#include <type_traits>

#include "tools/SANSnumerics.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Field/Field.h"
#include "Field/FieldSequence.h"
#include "Field/XField.h"
#include "Field/HField/HField_DG.h"
#include "Field/FieldLine_CG_Cell.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldVolume_CG_Cell.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/tools/for_each_CellGroup.h"

#include "AlgebraicEquationSetBase_PTC.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/QuadratureOrder.h"

#include "Discretization/Galerkin/ResidualCell_Galerkin_BDF.h"
#include "Discretization/Galerkin/JacobianCell_Galerkin_BDF.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_PTC.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_PTC_manifold.h"

#include "SetLocalTimeStep.h"

#include "pde/NDConvert/PDENDConvert_fwd.h"

//#define CG_TIME_FIELD

namespace SANS
{

// Integrand identification tag
class PTC;
class PTC_manifold;

template<class DiscTag>
struct PTCIntegrands; // for resolving integrand types

template<>
struct PTCIntegrands<PTC>
{
  template <class PDE>
  using IntegrandCell = IntegrandCell_Galerkin_PTC<PDE>;
};

template<>
struct PTCIntegrands<PTC_manifold>
{
  template <class PDE>
  using IntegrandCell = IntegrandCell_Galerkin_PTC_manifold<PDE>;
};


//----------------------------------------------------------------------------//
namespace AES_PTC_detail
{
// worker object to do some partially specialized labor
template<class NDPDEClass, class Traits, class IntegrandTag, class ParamFieldType>
struct worker;
} // namespace AES_PTC_detail

//----------------------------------------------------------------------------//
// Algebraic equation set class that wraps pseudo-time continuation (PTC) around an existing equation set of spatial discretization
// Note: Assume that PTC uses backward Euler time discretization
//
template<class NDPDEClass, class Traits, class IntegrandTag, class ParamFieldType>
class AlgebraicEquationSet_PTC;

//----------------------------------------------------------------------------//
template<class NDPDEClass, class Traits, class IntegrandTag, class ParamFieldType>
class AlgebraicEquationSet_PTC
    : public AlgebraicEquationSetTraits_PTC<typename NDPDEClass::template MatrixQ<Real>,
                                            typename NDPDEClass::template ArrayQ<Real>,
                                            Traits>::AlgebraicEquationSetBase_PTCClass
{
  friend struct AES_PTC_detail::worker<NDPDEClass, Traits, IntegrandTag, ParamFieldType>; // make a friend struct to do some labor

public:
  typedef typename NDPDEClass::PhysDim PhysDim;
  typedef typename PTCIntegrands<IntegrandTag>::template IntegrandCell<NDPDEClass> IntegrandCellClass;

  typedef typename ParamFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDPDEClass::template MatrixQ<Real> MatrixQ;

  typedef AlgebraicEquationSetTraits_PTC<MatrixQ, ArrayQ, Traits> PTCTraitsType;
  typedef typename PTCTraitsType::TraitsType TraitsType;

  typedef typename TraitsType::VectorSizeClass VectorSizeClass;
  typedef typename TraitsType::MatrixSizeClass MatrixSizeClass;

  typedef typename TraitsType::SystemMatrix SystemMatrix;
  typedef typename TraitsType::SystemVector SystemVector;
  typedef typename TraitsType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename TraitsType::SystemMatrixView SystemMatrixView;
  typedef typename TraitsType::SystemVectorView SystemVectorView;
  typedef typename TraitsType::SystemNonZeroPatternView SystemNonZeroPatternView;

  typedef AlgebraicEquationSetBase_PTC<SystemMatrix> PTCAESBaseType;
  typedef typename TraitsType::AlgebraicEquationSetBaseClass SpatialAESBaseType;

  typedef typename SpatialAESBaseType::LinesearchData LinesearchData;

  // Field type to represent the inverse time step (i.e. 1/dt)
#ifdef CG_TIME_FIELD
  typedef Field_CG_Cell<PhysDim, TopoDim, Real> TField_CellType;
#else
  typedef Field_DG_Cell<PhysDim, TopoDim, Real> TField_CellType;
#endif

  AlgebraicEquationSet_PTC(const ParamFieldType& paramfld,
                           Field<PhysDim, TopoDim, ArrayQ>& qfld,
                           const NDPDEClass& pde,
                           const QuadratureOrder& quadratureOrder,
                           const std::vector<int>& CellGroups,
                           SpatialAESBaseType& spatialAES) :
    paramfld_(paramfld),
    paramfldpast_(paramfld),
    hfld_(paramfld.getXField()),
    qfld_(qfld),
    qfldpast_(1, qfld), // a sequence of only one item due to backward Euler discretization
    pde_(pde),
    fcnCell_(pde, CellGroups),
#ifdef CG_TIME_FIELD
    dtifld_(paramfld.getXField(), 1, BasisFunctionCategory_Lagrange),
#else
    dtifld_(paramfld.getXField(), 0, BasisFunctionCategory_Legendre),
#endif
    spatial_(spatialAES),
    iPDE_(spatial_.indexPDE()),
    iPDESC_(spatial_.indexPDESC()),
    iq_(spatial_.indexQ()),
    iqSC_(spatial_.indexQSC()),
    cellGroups_(CellGroups),
    quadratureOrder_(quadratureOrder),
    quadratureOrderMin_(paramfld.getXField(), 0),
    AES_PTC_(nullptr)
  {
    isStaticCondensed_ = spatialAES.isStaticCondensed();

    // Clear the inverse time field efficiently rather than using operator= (which is an L2 projection)
    for (int n = 0; n < dtifld_.nDOF(); n++)
      dtifld_.DOF(n) = 0.0;
  }

  virtual ~AlgebraicEquationSet_PTC() {}

  using PTCAESBaseType::residual;
  using PTCAESBaseType::jacobian;
  using PTCAESBaseType::jacobianTranspose;

  //Computes the residual
  virtual void residual(SystemVectorView& rsd) override
  {
    spatial_.residual(rsd); // compute spatial residual
    residualPseudoTemporal(rsd); // compute pseudo temporal residual
  }

  virtual void residualPseudoTemporal(SystemVectorView& rsd) override
  {
    if (AES_PTC_ != nullptr) AES_PTC_->residualPseudoTemporal(rsd);

    AES_PTC_detail::worker<NDPDEClass, Traits, IntegrandTag, ParamFieldType>::template residual_PTC(this, rsd);
  }

  //Fills jacobian or the non-zero pattern of a jacobian
  virtual void jacobian(SystemMatrixView& mtx) override
  {
    spatial_.jacobian(mtx);
    jacobianPseudoTemporal(mtx);
  }

  virtual void jacobian(SystemNonZeroPatternView& nz) override
  {
    spatial_.jacobian(nz);
    jacobianPseudoTemporal(nz);
  }

  //Fills jacobian transpose or the non-zero pattern transpose of a jacobian transpose
  virtual void jacobianTranspose(SystemMatrixView& mtxT) override
  {
    spatial_.jacobian(mtxT);
    jacobianPseudoTemporalTranspose(mtxT);
  }

  virtual void jacobianTranspose(SystemNonZeroPatternView& nzT) override
  {
    spatial_.jacobian(nzT);
    jacobianPseudoTemporalTranspose(nzT);
  }

  void jacobian(SystemVectorView& b, SystemNonZeroPatternView& nz, bool transposed) override
  {
    spatial_.jacobian( b, nz, transposed );
    jacobianPseudoTemporal(nz);
  }

  void jacobian(SystemVectorView& b, SystemMatrixView& mtx, bool transposed) override
  {
    spatial_.jacobian( b, mtx, transposed );
    jacobianPseudoTemporal(mtx);
  }


  void jacobianPseudoTemporal(SystemMatrixView& jac) override
  {
    if (AES_PTC_ != nullptr) AES_PTC_->jacobianPseudoTemporal(jac);

    jacobianPseudoTemporal(jac, quadratureOrder_);
  }

  virtual void jacobianPseudoTemporal(SystemNonZeroPatternView& nz) override
  {
    if (AES_PTC_ != nullptr) AES_PTC_->jacobianPseudoTemporal(nz);

    jacobianPseudoTemporal(nz, quadratureOrderMin_);
  }

  void jacobianPseudoTemporalTranspose(SystemMatrixView& jacT) override
  {
    if (AES_PTC_ != nullptr) AES_PTC_->jacobianPseudoTemporalTranspose(jacT);

    jacobianPseudoTemporal(Transpose(jacT), quadratureOrder_);
  }

  virtual void jacobianPseudoTemporalTranspose(SystemNonZeroPatternView& nzT) override
  {
    if (AES_PTC_ != nullptr) AES_PTC_->jacobianPseudoTemporalTranspose(nzT);

    jacobianPseudoTemporal(Transpose(nzT), quadratureOrderMin_);
  }


  virtual void completeUpdate(const SystemVectorView& rsd, SystemVectorView& xcondensed, SystemVectorView& x) const override
  {
    spatial_.completeUpdate(rsd, xcondensed, x);
  }


  // Compute Residual Norm
  virtual std::vector<std::vector<Real>> residualNorm( const SystemVectorView& rsd) const override
  {
    return spatial_.residualNorm(rsd);
  }

  //Convergence check of the residual
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm) const override
  {
    return spatial_.convergedResidual(rsdNorm);
  }

  //Convergence check of the residual
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm, int iEq, int iMon) const override
  {
    return spatial_.convergedResidual(rsdNorm, iEq, iMon);
  }

  // check if residual decreased
  virtual bool decreasedResidual( const std::vector<std::vector<Real>>& rsdNormOld,
                                  const std::vector<std::vector<Real>>& rsdNormNew) const override
  {
    return spatial_.decreasedResidual(rsdNormOld, rsdNormNew);
  }

  //prints out a residual that could not be decreased and the convergence tolerances
  virtual void printDecreaseResidualFailure(const std::vector<std::vector<Real>>& rsdNorm, std::ostream& os = std::cout) const override
  {
     spatial_.printDecreaseResidualFailure(rsdNorm, os);
  }

  // Are we having machine precision issues
  virtual bool atMachinePrecision(const SystemVectorView& q, const std::vector<std::vector<Real>>& R0norm) override
  {
    return spatial_.atMachinePrecision(q, R0norm);
  }

  //Translates the system vector into a solution field
  virtual void setSolutionField(const SystemVectorView& q) override { spatial_.setSolutionField(q); }

  //Translates the solution field into a system vector
  virtual void fillSystemVector(SystemVectorView& q) const override { spatial_.fillSystemVector(q); }

  // Returns the vector an matrix sizes needed for the linear algebra system
  virtual VectorSizeClass vectorEqSize() const override    { return spatial_.vectorEqSize();    } // vector for equations (rows in matrixSize)
  virtual VectorSizeClass vectorStateSize() const override { return spatial_.vectorStateSize(); } // vector for state DOFs (columns in matrixSize)
  virtual MatrixSizeClass matrixSize() const override      { return spatial_.matrixSize();      }

  // Gives the PDE and solution indices in the system
  virtual int indexPDE() const override { SANS_ASSERT(iPDE_ == spatial_.indexPDE()); return iPDE_; }
  virtual int indexQ() const override { SANS_ASSERT(iq_ == spatial_.indexQ()); return iq_; }

  // update fraction needed for physically valid state
  virtual Real updateFraction(const SystemVectorView& q, const SystemVectorView& dq, const Real maxChangeFraction) const override
  {
    return spatial_.updateFraction(q, dq, maxChangeFraction);
  }

  // Checks to see if proposed solution is physical
  virtual bool isValidStateSystemVector(SystemVectorView& q) override;

  // Returns the side of the residual norm outer vector
  virtual int nResidNorm() const override;

  // Converts processor local indexing to a processor continuous indexing
  virtual std::vector<GlobalContinuousMap> continuousGlobalMap() const override
  {
    return spatial_.continuousGlobalMap();
  }

  // MPI communicator for this algebraic equation set
  virtual std::shared_ptr<mpi::communicator> comm() const override { return spatial_.comm(); }

  virtual void syncDOFs_MPI() override { spatial_.syncDOFs_MPI(); }

  // Dump the localized linesearch parameter info (for debugging purposes)
  virtual bool updateLinesearchDebugInfo(const Real& s, const SystemVectorView& rsd,
                                         LinesearchData& pResData,
                                         LinesearchData& pStepData) const override;

  virtual void dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                                       const LinesearchData& pStepData) const override;

  SpatialAESBaseType& getSpatialAlgebraicEquationSet() override { return spatial_; }

  virtual void setLocalTimeStepField(Real invCFL) override
  {
    if (AES_PTC_ != nullptr) AES_PTC_->setLocalTimeStepField(invCFL);

    // Clear the inverse time field efficiently rather than using operator= (which is an L2 projection)
    for (int n = 0; n < dtifld_.nDOF(); n++)
      dtifld_.DOF(n) = 0.0;

    for_each_CellGroup<TopoDim>::apply(
        SetLocalTimeStep<NDPDEClass, ParamFieldType>(pde_, quadratureOrder_.cellOrders, cellGroups_, invCFL),
        (paramfld_, (qfld_, hfld_, dtifld_)));
    // Note that only DOFs from the cellGroups are used in this AES class, which is generally not the same as all the cell groups of the field

    // synchronize so time steps are consistent accross processors
    dtifld_.syncDOFs_MPI_Cached();
  }

  virtual void dumpSolution(const std::string& filename) const override
  {
    spatial_.dumpSolution(filename);
  }

  virtual void dumpLocalTimeStepField(const std::string& filename) const override
  {
    if (AES_PTC_ != nullptr) AES_PTC_->dumpLocalTimeStepField(filename + "_daisyChain");

    output_Tecplot(dtifld_, filename + ".dat", {"dti"});
  }

  virtual void storeCurrentSolution() override
  {
    if (AES_PTC_ != nullptr) AES_PTC_->storeCurrentSolution();

    // Copy the current field to the past field for the dU/dt term
    SANS_ASSERT( qfldpast_[0].nDOF() == qfld_.nDOF() );
    for (int i = 0; i < qfld_.nDOF(); i++)
    {
      qfldpast_[0].DOF(i) = qfld_.DOF(i);
    }
  }

  // Add an AES_PTC to this one to daisy-chain
  virtual void addAlgebraicEquationSetPTC(std::shared_ptr<PTCAESBaseType> AES_PTC)
  {
    AES_PTC_ = AES_PTC;
  }

protected:
  template<class SparseMatrixType>
  void jacobianPseudoTemporal(SparseMatrixType mtx, const QuadratureOrder& quadratureOrder );

  const ParamFieldType& paramfld_;
  const ParamFieldType& paramfldpast_; // assume paramfld remains the same during time integration
  HField_DG<PhysDim, TopoDim> hfld_;   // grid size h field
  Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  FieldSequence<PhysDim, TopoDim, ArrayQ> qfldpast_;
  const NDPDEClass& pde_; // pde
  const IntegrandCellClass fcnCell_;
  TField_CellType dtifld_; // 1/dt field
  SpatialAESBaseType& spatial_;

  const int iPDE_;
  const int iPDESC_;
  const int iq_;
  const int iqSC_;

  const std::vector<int> cellGroups_;                 // cell groups the pseudo time is applied to

  QuadratureOrder quadratureOrder_;
  QuadratureOrder quadratureOrderMin_;

  std::shared_ptr<PTCAESBaseType> AES_PTC_;

  using PTCAESBaseType::isStaticCondensed_;
};


template<class NDPDEClass, class Traits, class IntegrandTag, class ParamFieldType>
template<class SparseMatrixType>
void
AlgebraicEquationSet_PTC<NDPDEClass, Traits, IntegrandTag, ParamFieldType>::
jacobianPseudoTemporal(SparseMatrixType jac, const QuadratureOrder& quadratureOrder)
{
  AES_PTC_detail::worker<NDPDEClass, Traits, IntegrandTag, ParamFieldType>::
    template jacobian_PTC(this, jac, quadratureOrder);
}

template<class NDPDEClass, class Traits, class IntegrandTag, class ParamFieldType>
bool
AlgebraicEquationSet_PTC<NDPDEClass, Traits, IntegrandTag, ParamFieldType>::
isValidStateSystemVector(SystemVectorView& q)
{
  return spatial_.isValidStateSystemVector(q);
}

template<class NDPDEClass, class Traits, class IntegrandTag, class ParamFieldType>
int
AlgebraicEquationSet_PTC<NDPDEClass, Traits, IntegrandTag, ParamFieldType>::
nResidNorm() const
{
  return spatial_.nResidNorm();
}

template<class NDPDEClass, class Traits, class IntegrandTag, class ParamFieldType>
bool
AlgebraicEquationSet_PTC<NDPDEClass, Traits, IntegrandTag, ParamFieldType>::
updateLinesearchDebugInfo(const Real& s, const SystemVectorView& rsd,
                          LinesearchData& pResData,
                          LinesearchData& pStepData) const
{
  return spatial_.updateLinesearchDebugInfo(s, rsd, pResData, pStepData);
}

template<class NDPDEClass, class Traits, class IntegrandTag, class ParamFieldType>
void
AlgebraicEquationSet_PTC<NDPDEClass, Traits, IntegrandTag, ParamFieldType>::
dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                        const LinesearchData& pStepData) const
{
  spatial_.dumpLinesearchDebugInfo(filenamebase, nonlinear_iter, pStepData);
}

//----------------------------------------------------------------------------//
namespace AES_PTC_detail
{

// partial template specialization: IntegrandTag = PTC, i.e. non-manifold discretization
template<class NDPDEClass, class Traits, class ParamFieldType>
struct worker<NDPDEClass, Traits, PTC, ParamFieldType>
{
  typedef AlgebraicEquationSet_PTC<NDPDEClass, Traits, PTC, ParamFieldType> AES_PTC_Type;
  typedef typename AES_PTC_Type::TopoDim TopoDim;

  // add contribution of PTC time-dependent terms to the residual
  template<class SystemVector>
  static void residual_PTC(AES_PTC_Type* AES_PTC, SystemVector& rsd)
  {
    IntegrateCellGroups<TopoDim>::integrate( ResidualCell_Galerkin_BDF(AES_PTC->fcnCell_, rsd(AES_PTC->iPDE_)),
                                             (AES_PTC->paramfld_, AES_PTC->dtifld_),
                                             (AES_PTC->qfld_, AES_PTC->qfldpast_),
                                             AES_PTC->quadratureOrder_.cellOrders.data(),
                                             AES_PTC->quadratureOrder_.cellOrders.size() );
  }

  // add contribution of PTC time-dependent terms to the jacobian
  template<class SparseMatrixType>
  static void jacobian_PTC(AES_PTC_Type* AES_PTC, SparseMatrixType& jac, const QuadratureOrder& quadratureOrder)
  {
    // Get the matrix type, this could be a reference or temporary variable depending on SparseMatrixType
    typedef typename std::result_of<SparseMatrixType(const int, const int)>::type Matrix;

    int ipde = 0, iq = 0;
    if (AES_PTC->isStaticCondensed_)
    {
      ipde = AES_PTC->iPDESC_;
      iq = AES_PTC->iqSC_;
    }
    else
    {
      ipde = AES_PTC->iPDE_;
      iq = AES_PTC->iq_;
    }

    Matrix jacPDE_q  = jac(ipde, iq);

    IntegrateCellGroups<TopoDim>::integrate( JacobianCell_Galerkin_BDF(AES_PTC->fcnCell_, jacPDE_q),
                                             (AES_PTC->paramfld_, AES_PTC->dtifld_),
                                             (AES_PTC->qfld_, AES_PTC->qfldpast_),
                                             quadratureOrder.cellOrders.data(),
                                             quadratureOrder.cellOrders.size() );
  }
};

// partial template specialization: IntegrandTag = PTC_manifold, i.e. manifold discretization
template<class NDPDEClass, class Traits, class ParamFieldType>
struct worker<NDPDEClass, Traits, PTC_manifold, ParamFieldType>
{
  typedef AlgebraicEquationSet_PTC<NDPDEClass, Traits, PTC_manifold, ParamFieldType> AES_PTC_Type;
  typedef typename AES_PTC_Type::TopoDim TopoDim;

  // add contribution of PTC time-dependent terms to the residual
  template<class SystemVector>
  static void residual_PTC(AES_PTC_Type* AES_PTC, SystemVector& rsd)
  {
    IntegrateCellGroups<TopoDim>::integrate( ResidualCell_Galerkin_BDF(AES_PTC->fcnCell_, rsd(AES_PTC->iq_)),
                                             (AES_PTC->paramfldpast_, AES_PTC->paramfld_, AES_PTC->dtifld_),
                                             (AES_PTC->qfld_, AES_PTC->qfldpast_),
                                             AES_PTC->quadratureOrder_.cellOrders.data(),
                                             AES_PTC->quadratureOrder_.cellOrders.size() );
  }

  // add contribution of PTC time-dependent terms to the jacobian
  template<class SparseMatrixType>
  static void jacobian_PTC(AES_PTC_Type* AES_PTC, SparseMatrixType& jac, const QuadratureOrder& quadratureOrder)
  {
    // Get the matrix type, this could be a reference or temporary variable depending on SparseMatrixType
    typedef typename std::result_of<SparseMatrixType(const int, const int)>::type Matrix;

    int ipde = 0, iq = 0;
    if (AES_PTC->isStaticCondensed_)
    {
      ipde = AES_PTC->iPDESC_;
      iq = AES_PTC->iqSC_;
    }
    else
    {
      ipde = AES_PTC->iPDE_;
      iq = AES_PTC->iq_;
    }

    Matrix jacPDE_q  = jac(ipde, iq);

    IntegrateCellGroups<TopoDim>::integrate( JacobianCell_Galerkin_BDF(AES_PTC->fcnCell_, jacPDE_q),
                                             (AES_PTC->paramfldpast_, AES_PTC->paramfld_, AES_PTC->dtifld_),
                                             (AES_PTC->qfld_, AES_PTC->qfldpast_),
                                             quadratureOrder.cellOrders.data(),
                                             quadratureOrder.cellOrders.size() );
  }
};

} // namespace AES_PTC_detail

} // namespace SANS

#endif //ALGEBRAICEQUATIONSET_PTC_H
