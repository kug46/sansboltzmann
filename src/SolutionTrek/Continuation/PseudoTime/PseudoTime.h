// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PSEUDOTIME_H
#define PSEUDOTIME_H

//#define PTCSOLVER_DEBUG_DUMPVECTOR // turned on just for debugging

#include <limits>
#include <memory> // std::unique_ptr
#include <limits> // std::numeric_limits

#include "Python/PyDict.h" // Python must be included first
#include "Python/Parameter.h"

#include "tools/SANSException.h"

#include "NonLinearSolver/NonLinearSolver.h"

#include "AlgebraicEquationSetBase_PTC.h"

namespace SANS
{

//=============================================================================
struct PseudoTimeParam : noncopyable
{
  //const ParameterDict NonLinearSolver{"NonLinearSolver", NO_DEFAULT, NonLinearSolverParam::checkInputs,
  //                                    "The non-linear solver for each pseudo time step"
  //                                   };
  const ParameterOption<NonLinearSolverParam::SolverOptions>& NonLinearSolver;

  const ParameterNumeric<int > MaxIterations{"MaxIterations", 100, 0, NO_LIMIT, "Maximum Number of PTC Iterations"};

  const ParameterNumeric<Real> invCFL{"invCFL", 1, 0, NO_LIMIT, "Initial inverse CFL number"};

  const ParameterNumeric<Real> invCFL_max{"invCFL_max", 1000, 0, NO_LIMIT, "Maximum inverse CFL number"};
  const ParameterNumeric<Real> invCFL_min{"invCFL_min",    0, 0, NO_LIMIT, "Minimum inverse CFL number"};

  const ParameterNumeric<Real> CFLDecreaseFactor{"CFLDecreaseFactor", 0.1, 0,        1, "Factor for deceasing the CFL number"};
  const ParameterNumeric<Real> CFLIncreaseFactor{"CFLIncreaseFactor",   2, 1, NO_LIMIT, "Factor for increasing the CFL number"};

  const ParameterNumeric<Real> CFLPartialStepDecreaseFactor
  {
    "CFLPartialStepDecreaseFactor", 0.9, 0, 1,
    "Factor for decreasing the CFL number on linesearch partial step"
  };

  const ParameterBool Verbose{"Verbose", false, "Verbose Pseudo Time output"};
  const ParameterBool SteveLogic{"SteveLogic", false, "Steve's Pseudo Time logic"};
  const ParameterBool RequireConvergedNewton{"RequireConvergedNewton", false, "Require Newton Solver Convergence to take PTC step"};

  const ParameterFileName ResidualHistoryFile{"ResidualHistoryFile", std::ios_base::out, "", "Dumps spatial residual history to give file name"};
  const ParameterFileName LineSearchHistoryFile
  {
    "LineSearchHistoryFile",
    std::ios_base::out,
    "",
    "Dumps spatial linesearch history to give file name"
  };

  PseudoTimeParam();

  static void checkInputs(PyDict d);
  static PseudoTimeParam params;
};


//----------------------------------------------------------------------------//
// Pseudo time continuation (PTC) solution scheme of nonlinear system
//----------------------------------------------------------------------------//
template<class SystemMatrix>
class PseudoTime
{
public:
  typedef AlgebraicEquationSetBase_PTC<SystemMatrix> PTCAESBaseType;

  typedef typename PTCAESBaseType::SpatialAESBaseType SpatialAESBaseType;

  typedef NonLinearSolver<SystemMatrix> NonLinearSolverType;

  typedef typename VectorType<SystemMatrix>::type SystemVector;
  typedef typename NonZeroPatternType<SystemMatrix>::type SystemNonZeroPattern;

  PseudoTime(const Real invCFL0, const Real invCFL_max, const Real invCFL_min,
             const Real CFLDecreaseFactor, const Real CFLIncreaseFactor,
             const Real CFLPartialStepDecreaseFactor,
             const PyDict& nonLinearSolver_dict,
             PTCAESBaseType& algEqSetPTC,
             bool verbose = false,
             int MaxIterations = 1000,
             std::string residualfilename = "",
             std::string linesearchfilename = "",
             const bool steveLogic = false,
             const bool requireConvergedNewton = false) :
    MaxIterations_(MaxIterations),
    invCFL_max_(invCFL_max),
    invCFL_(invCFL0),
    invCFL_min_(invCFL_min),
    CFLDecreaseFactor_(CFLDecreaseFactor),
    CFLIncreaseFactor_(CFLIncreaseFactor),
    CFLPartialStepDecreaseFactor_(CFLPartialStepDecreaseFactor),
    algEqSetPTC_(algEqSetPTC),
    spatial_(algEqSetPTC_.getSpatialAlgebraicEquationSet()),
    nonLinearSolver_(algEqSetPTC_, nonLinearSolver_dict),
    verbose_(verbose),
    steveLogic_(steveLogic),
    requireConvergedNewton_(requireConvergedNewton)
  {
    // Make sure the parameters given are in the correct range
    SANS_ASSERT_MSG(CFLDecreaseFactor_ > 0, "CFLDecreaseFactor must be positive");
    SANS_ASSERT_MSG(CFLDecreaseFactor_ <= 1, "CFLDecreaseFactor must be less than or equal to 1");
    SANS_ASSERT_MSG(CFLIncreaseFactor_ >= 1, "CFLIncreaseFactor must be less than or equal to 1");
    SANS_ASSERT_MSG(CFLPartialStepDecreaseFactor_ > 0, "CFLPartialStepDecreaseFactor must be positive");
    SANS_ASSERT_MSG(CFLPartialStepDecreaseFactor_ <= 1, "CFLPartialStepDecreaseFactor must be less than or equal to 1");

    if (!residualfilename.empty() && spatial_.comm()->rank() == 0)
    { // TODO: this block has not been unit tested ...
      fhist_.open(residualfilename, std::ios_base::app);
      // add a new zone to distinguish between restarts
      fhist_ << "ZONE" << std::endl;
    }

    if (!linesearchfilename.empty() && spatial_.comm()->rank() == 0)
    { // TODO: this block has not been unit tested ...
      flinesearchhist_.open(linesearchfilename, std::ios_base::app);
      // add a new zone to distinguish between restarts
      flinesearchhist_ << "ZONE" << std::endl;
    }
  }

  PseudoTime(const PyDict& d, PTCAESBaseType& algEqSetPTC) :
    PseudoTime( d.get(PseudoTimeParam::params.invCFL),
                d.get(PseudoTimeParam::params.invCFL_max),
                d.get(PseudoTimeParam::params.invCFL_min),
                d.get(PseudoTimeParam::params.CFLDecreaseFactor),
                d.get(PseudoTimeParam::params.CFLIncreaseFactor),
                d.get(PseudoTimeParam::params.CFLPartialStepDecreaseFactor),
                d,
                algEqSetPTC,
                d.get(PseudoTimeParam::params.Verbose),
                d.get(PseudoTimeParam::params.MaxIterations),
                d.get(PseudoTimeParam::params.ResidualHistoryFile),
                d.get(PseudoTimeParam::params.LineSearchHistoryFile),
                d.get(PseudoTimeParam::params.SteveLogic),
                d.get(PseudoTimeParam::params.RequireConvergedNewton) ) {}

  bool solve();
  bool iterate(const int nSteps);

protected:
  void updateCFL(SolveStatus status);

  int MaxIterations_;                                 // maximum number of interations of PTC
  Real invCFL_max_;                                   // maximum inverse Courant–Friedrichs–Lewy condition number
  Real invCFL_;                                       // current inverse Courant–Friedrichs–Lewy condition number
  Real invCFL_min_;                                   // minimum inverse Courant–Friedrichs–Lewy condition number
  Real CFLDecreaseFactor_;                            // CFL shrink factor
  Real CFLIncreaseFactor_;                            // CFL growth factor
  Real CFLPartialStepDecreaseFactor_;                 // CFL shrink factor for partial step on linesearch

  PTCAESBaseType& algEqSetPTC_;                       // PTC algebraic equation set
  SpatialAESBaseType& spatial_;                       // spatial algebraic equation set

  NonLinearSolverType nonLinearSolver_;               // nonlinear solver for each step

  bool verbose_;                                      // output CFL to the console

  std::ofstream fhist_;                               // residual history file
  std::ofstream flinesearchhist_;                     // linesearch step size history file

  Real alpha_target_ = 0.7;
  Real power_ = 0.5;
  const bool steveLogic_;
  const bool requireConvergedNewton_;
};

//---------------------------------------------------------------------------//
template<class SystemMatrix>
void
PseudoTime< SystemMatrix >::
updateCFL(SolveStatus status)
{
  if (steveLogic_) //Steve's New Special Sauce
  {
    int comm_rank = spatial_.comm()->rank();

    // Increase the CFL --> getting closer to steady state
    // Decrease the CFL --> refining transient state

    // NB: Logic here to pick the largest \alpha^{*} of all from residuals
    Real alpha_star = 0.0;

    // U_{0} + \mu \Delta U valid \oftaal \mu \in [0,1]
    if ( status.linesearch.validStepSize > (1. - 1e-5) )
    {
      if (comm_rank == 0)
      {
        std::cout << "status.linesearch.validStepSize: " << status.linesearch.validStepSize << "\n";
        std::cout << "Always Valid: " << std::endl;
      }
      for (std::size_t i = 0; i < status.linesearch.initialResidual.size(); i++)
      {
        for (std::size_t j = 0; j < status.linesearch.initialResidual[i].size(); j++)
        {
          if (!algEqSetPTC_.convergedResidual(status.linesearch.initialResidual, i, j))
          {
            // \alpha^{*} = \alpha(1)
            Real alpha_candidate = status.linesearch.validResidual[i][j] / status.linesearch.initialResidual[i][j];

            if (comm_rank == 0)
              std::cout << " " << i << " " << j << ": " << alpha_candidate << std::endl;

            alpha_star = std::max(alpha_star, alpha_candidate);
          }
        }
      }
    }
    else
    {
      if (comm_rank == 0)
        std::cout << "Not always Valid: " << status.linesearch.validStepSize << std::endl;

      for (std::size_t i = 0; i < status.linesearch.initialResidual.size(); i++)
      {
        for (std::size_t j = 0; j < status.linesearch.initialResidual[i].size(); j++)
        {
          if (!algEqSetPTC_.convergedResidual(status.linesearch.initialResidual, i, j))
          {
            // \alpha^{*} = \alpha(\mu_{f}) / \mu_{v}
            Real alpha_candidate = status.linesearch.minimizedResidual[i][j] / status.linesearch.initialResidual[i][j];
            alpha_candidate /= status.linesearch.validStepSize;

            if (comm_rank == 0)
              std::cout << " " << i << " " << j << ": " << alpha_candidate << std::endl;

            alpha_star = std::max(alpha_star, alpha_candidate);
          }
        }
      }
    }

    if ( (requireConvergedNewton_ && !status.converged) || status.linesearch.status == LineSearchStatus::Failure )
    {
      // ARTHUR LIKES TO DO FULL NEWTON SOLVES, SO DON'T DECREASE CFL UNLESS YOU DO
      invCFL_ = max( pow(alpha_star / alpha_target_, power_), 1./CFLDecreaseFactor_ )*invCFL_;
    }
    else
    {
      invCFL_ = pow(alpha_star / alpha_target_, power_) * invCFL_;
    }

    // keep invCFL in specified bounds
    invCFL_ = min(invCFL_, invCFL_max_);
    invCFL_ = max(invCFL_, invCFL_min_);
    if (comm_rank == 0)
      std::cout << alpha_star << " " << invCFL_ << std::endl;

  }
  else //old Pseudotime Logic
  {
    // Increase the CFL --> getting closer to steady state
    // Decrease the CFL --> refining transient state

    if (status.linesearch.status == LineSearchStatus::Failure || (!status.converged && requireConvergedNewton_))
    {
      // Decrease the CFL
      if ( invCFL_ > 0 )
        invCFL_ = min(invCFL_/CFLDecreaseFactor_, invCFL_max_);
      else // in case the solver gets trapped when invCFL_==0
        invCFL_ = min(sqrt(std::numeric_limits<Real>::epsilon())/CFLDecreaseFactor_, invCFL_max_);
    }
    else if (status.linesearch.status == LineSearchStatus::PartialStep || !status.linearSolveStatus.success)
    {
      // Decrease the CFL a little
      invCFL_ = min(invCFL_/CFLPartialStepDecreaseFactor_, invCFL_max_);
    }
    else if (status.linesearch.status == LineSearchStatus::LimitedStep)
    {
      // Decrease the CFL a little
      //invCFL_ = min(invCFL_/0.9, invCFL_max_);
    }
    else if (status.linesearch.status == LineSearchStatus::FullStep)
    {
      // Increase the CFL
      invCFL_ = max(invCFL_/CFLIncreaseFactor_, invCFL_min_);
    }

  }

}

//---------------------------------------------------------------------------//
template< class SystemMatrix >
bool
PseudoTime< SystemMatrix >::solve()
{
  return iterate(MaxIterations_);
}

//---------------------------------------------------------------------------//
template< class SystemMatrix >
bool
PseudoTime< SystemMatrix >::iterate(const int nSteps)
{
  SystemVector qold(algEqSetPTC_.vectorStateSize());
  SystemVector q(algEqSetPTC_.vectorStateSize());
  SystemVector rsd_spatial(spatial_.vectorEqSize());

  int comm_rank = spatial_.comm()->rank();

  // default initialization
  bool convergedSpatial = false;
  SolveStatus statusNonlinearSolvePTC;

  // Compute potentially any auxiliary variables
  algEqSetPTC_.fillSystemVector(q);

  // Pseudo time continuation iterations
  for (int step = 0; step < nSteps; step++)
  {
    // Move the current solution to the previous one
    qold = q;

    // check that the spatial residual is converged
    rsd_spatial = 0.0; // resetting residual to zero is required!
    spatial_.residual(q, rsd_spatial);
    std::vector<std::vector<Real>> nrmRsd = spatial_.residualNorm(rsd_spatial);

//#if 1 // Debugging
//    algEqSetPTC_.dumpSolution("tmp/qfldI_step" + std::to_string(step));
//#endif

    // dump residual history to file
    if (verbose_ || fhist_.is_open() || flinesearchhist_.is_open())
    {
      if (comm_rank == 0)
      {
        // print if requested
        if (verbose_)
        {
          std::cout << "---------------------------------" << std::endl;
          std::cout << "Step: " << step << ", Inverse CFL: " << invCFL_ << std::endl;
          std::cout << "  Spatial Res_L2norm = " << std::setprecision(5) << std::scientific << nrmRsd << std::endl;
        }

        // save if requested
        if (fhist_.is_open())
          fhist_ << step << ", " << invCFL_ << ", " << std::setprecision(16) << std::scientific << nrmRsd << std::endl;

        // save if requested
        if (flinesearchhist_.is_open() && (step > 0))
          flinesearchhist_ << step
                           << ", " << std::setprecision(16) << std::scientific
                           << nonLinearSolver_.getLineSearchHistory() << std::endl;

#if defined(PTCSOLVER_DEBUG_DUMPVECTOR)
        // TODO: This printout is intended for debugging, and can be generalized to be a function
        { // dump formatted vector
          std::cout << "...Writing nonlinear system vectors to files (including residuals, solution, and update) ..." << std::endl;

          //dump vectors: require overloaded insertion operator "<<" for printing out vectors
          std::string res_filename = "tmp/rsd_iter" + std::to_string(step) + "_ptc.dat";
          std::fstream fout_res( res_filename, std::fstream::out );
          fout_res << rsd_spatial << std::endl;

          std::string sol_filename = "tmp/sln_iter" + std::to_string(step) + "_ptc.dat";
          std::fstream fout_sol( sol_filename, std::fstream::out );
          fout_sol << q << std::endl;
        }
#endif
      }
    }

    // check convergence of the spatial equation set
    if (spatial_.convergedResidual(nrmRsd))
    {
      convergedSpatial = true;
      break;
    }

    // Copy the current field to the past field for the dU/dt term
    algEqSetPTC_.storeCurrentSolution();

    // Set the local inverse time step field
    algEqSetPTC_.setLocalTimeStepField(invCFL_);

#if 0 // Debugging
    algEqSetPTC_.dumpLocalTimeStepField("tmp/dtifld_step" + std::to_string(step));
#endif

    // Solve the current step
    statusNonlinearSolvePTC = nonLinearSolver_.solve(qold, q);

    if ((statusNonlinearSolvePTC.atMachinePrecision) && (step > 0))
    {
      std::cout << "Detected machine precision issues" << std::endl;
      // We think we're at out machine precision floor...
      spatial_.setSolutionField(qold);
      // We probably converged?
      return true;
    }

    if ((statusNonlinearSolvePTC.linesearch.status == LineSearchStatus::Failure) && (invCFL_ >= invCFL_max_))
    {
      // Well, line search hits the minimum CFL (i.e. maximum invCFL = 1/CFL) and still couldn't make progress...
      spatial_.setSolutionField(qold);

      if (comm_rank == 0 && verbose_)
      {
        std::cout << "...Printing spatial residual..." << std::endl;
        spatial_.printDecreaseResidualFailure(nrmRsd);
      }

      return convergedSpatial;
    }

    if ( (statusNonlinearSolvePTC.linesearch.status == LineSearchStatus::Failure) ||
         (!statusNonlinearSolvePTC.converged && requireConvergedNewton_) )
    {
      if (comm_rank == 0)
        std::cout << "PTC step failed, not taking the update...\n";

      //don't take the step if we didn't converge or the linesearch failed
      q = qold;
    }

    // Update the inverse CFL
    updateCFL(statusNonlinearSolvePTC);
  }

  spatial_.setSolutionField(q);

  if (comm_rank == 0 && verbose_ && !convergedSpatial)
  {
    std::cout << "...Printing spatial residual when solution does not converge..." << std::endl;
    std::vector<std::vector<Real>> nrmRsd = spatial_.residualNorm(rsd_spatial);
    spatial_.printDecreaseResidualFailure(nrmRsd);
  }

  return convergedSpatial;
}

}

#endif //PSEUDOTIME_H
