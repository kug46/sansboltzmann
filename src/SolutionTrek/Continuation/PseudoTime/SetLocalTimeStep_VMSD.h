// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SETLOCALTIMESTEP_VMSD_H
#define SETLOCALTIMESTEP_VMSD_H

#include "Python/PyDict.h" // Python must be included first
#include "Python/Parameter.h"

#include "tools/SANSnumerics.h"
#include "tools/SANSException.h"

#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Field.h"
#include "Field/XField.h"
#include "Field/HField/HField_DG.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/tools/for_each_CellGroup.h"

#include "pde/NDConvert/PDENDConvert_fwd.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "Quadrature/Quadrature.h"

namespace SANS
{

//=============================================================================

template<class NDPDE, class ParamFieldType>
class SetLocalTimeStep_VMSD;


//Partial specialization for spatial PDEs
template<class PhysDim, class PDEClass, class ParamFieldType>
class SetLocalTimeStep_VMSD<PDENDConvertSpace<PhysDim, PDEClass>, ParamFieldType> :
  public GroupFunctorCellType<SetLocalTimeStep_VMSD<PDENDConvertSpace<PhysDim, PDEClass>, ParamFieldType>>
{
public:
  typedef PDENDConvertSpace<PhysDim, PDEClass> PDE;
  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::VectorX VectorX;

  explicit SetLocalTimeStep_VMSD( const PDE& pde,
                             const std::vector<int>& quadratureOrders,
                             const std::vector<int>& cellGroups,
                             Real& invCFL ) :
    pde_(pde),
    quadratureOrders_(quadratureOrders),
    cellGroups_(cellGroups),
    invCFL_(invCFL)
  {
    // Nothing
  }

  std::size_t nCellGroups() const          { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n];     }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology>
  void
  apply( const typename FieldTuple< ParamFieldType,
                                    typename MakeTuple<FieldTuple,
                                                       Field<PhysDim, typename Topology::TopoDim, ArrayQ>,      // qfld
                                                       Field<PhysDim, typename Topology::TopoDim, ArrayQ>,      // qpfld
                                                       Field<PhysDim, typename Topology::TopoDim, Real>,        // hfld
                                                       Field<PhysDim, typename Topology::TopoDim, Real>>::type, // dtifld
                                    TupleClass<1>>::template FieldCellGroupType<Topology>& fldsCell,
         const int cellGroupGlobal )
  {
    typedef typename ParamFieldType                 ::template FieldCellGroupType<Topology> ParamFieldCellGroupType;
    typedef typename Field<PhysDim, typename Topology::TopoDim, ArrayQ>::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename HField_DG<PhysDim, typename Topology::TopoDim>    ::template FieldCellGroupType<Topology> HFieldCellGroupType;
    typedef typename Field<PhysDim, typename Topology::TopoDim, Real>  ::template FieldCellGroupType<Topology> TFieldCellGroupType;

    typedef typename ParamFieldCellGroupType::template ElementType<> ElementParamFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;
    typedef typename HFieldCellGroupType::template ElementType<> ElementHFieldClass;
    typedef typename TFieldCellGroupType::template ElementType<> ElementTFieldClass;

    typedef ElementXField<PhysDim, typename Topology::TopoDim, Topology> ElementXFieldClass;
    typedef typename ElementParamFieldClass::T ParamT;

    typedef QuadraturePoint<typename Topology::TopoDim> QuadPoint;

    ParamT param;               // Elemental parameters (such as grid coordinates and distance functions)

    Real dti, dtisource = 0;    // inverse time steps
    ArrayQ q, qp;                   // solution
    VectorArrayQ gradq, gradqp;         // gradient
    MatrixQ dsdu;               // source jacobian wrt conservative variables
    VectorX X;                  // cartesian coordinate
    Real deltaX;                // element length scale
    Real lam_max;               // maximum characteristic speed

    // determines if we need gradients for the source Jacobian
    const bool needsSolutionGradient = (pde_.hasSource() && pde_.needsSolutionGradientforSource());

    // use quadrature rule that matches the polynomial order
    Quadrature<typename Topology::TopoDim, Topology> quadrature( quadratureOrders_[cellGroupGlobal] );

    const ParamFieldCellGroupType& paramfldCell = get<0>(fldsCell);

    const QFieldCellGroupType& qfldCell         = get<0>(get<1>(fldsCell));
    const QFieldCellGroupType& qpfldCell         = get<1>(get<1>(fldsCell));
    const HFieldCellGroupType& hfldCell         = get<2>(get<1>(fldsCell));
          TFieldCellGroupType& dtifldCell       = const_cast<TFieldCellGroupType&>(get<3>(get<1>(fldsCell)));

    // element field variables
    ElementParamFieldClass paramfldElem( paramfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );
    ElementQFieldClass qpfldElem( qpfldCell.basis() );
    ElementHFieldClass hfldElem( hfldCell.basis() );
    ElementTFieldClass dtifldElem( dtifldCell.basis() );

    // Assert the assumption that basis function DOFs represent values
    SANS_ASSERT(  dtifldCell.basis()->category() == BasisFunctionCategory_Lagrange ||
                 (dtifldCell.basis()->category() == BasisFunctionCategory_Legendre &&
                  dtifldCell.basis()->order() == 0) );
    SANS_ASSERT_MSG( hfldCell.basis()->order() == 0, "Currently only assumes P0 h field" );

    const int nDOFElem = dtifldElem.nDOF();

    // loop over elements within group
    const int nelem = paramfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      dti = 0.0;

      // copy global grid/solution DOFs to element
      paramfldCell.getElement( paramfldElem, elem );
      qfldCell.getElement( qfldElem, elem );
      qpfldCell.getElement( qpfldElem, elem );
      hfldCell.getElement( hfldElem, elem );

      // get the inverse time step
      dtifldCell.getElement( dtifldElem, elem );

      const ElementXFieldClass& xfldElem( get<-1>(paramfldElem) );

      // loop over quadrature points
      const int nquad = quadrature.nQuadrature();
      for (int iquad = 0; iquad < nquad; iquad++)
      {
        QuadPoint Ref = quadrature.coordinates_cache( iquad );

        // parameters, solution, and h-field
        paramfldElem.eval( Ref, param );
        qfldElem.eval( Ref, q );
        qpfldElem.eval( Ref, qp );
        hfldElem.eval( Ref, deltaX );

        q += qp; // summed solution

        //Real J = xfldElem.jacobianDeterminant( Ref );
        //deltaX = pow(J, 1./typename Topology::TopoDim::D);

        // solution gradient
        if (needsSolutionGradient)
        {
          xfldElem.evalGradient( Ref, qfldElem, gradq );
          xfldElem.evalGradient( Ref, qpfldElem, gradqp );

          gradq += gradqp;
        }


        if (pde_.hasSource())
        {
          dsdu = 0;
          pde_.jacobianSource( param, q, gradq, dsdu );

          // Use the maximum diagonal value (or 0) to add to the inverse time step
          dtisource = 0;
          for (int n = 0; n < pde_.N; n++)
            dtisource = max(dtisource, DLA::index(dsdu,n,n));
        }

        lam_max = 0;
        pde_.speedCharacteristic(param, q, lam_max);

        dti = max(dti, invCFL_ * (lam_max / deltaX + dtisource));
      }

      // Set the maximum inverse time for each DOF
      for (int n = 0; n < nDOFElem; n++)
        dtifldElem.DOF(n) = max(dtifldElem.DOF(n), dti);

      // set the inverse time step
      dtifldCell.setElement( dtifldElem, elem );
    }
  }

protected:
  const PDE& pde_;
  const std::vector<int> quadratureOrders_;
  const std::vector<int> cellGroups_;
  Real invCFL_;
};


//Partial specialization for space-time PDEs
template<class PhysDim, class PDEClass, class ParamFieldType>
class SetLocalTimeStep_VMSD<PDENDConvertSpaceTime<PhysDim, PDEClass>, ParamFieldType> :
  public GroupFunctorCellType<SetLocalTimeStep_VMSD<PDENDConvertSpaceTime<PhysDim, PDEClass>, ParamFieldType>>
{
public:
  typedef PDENDConvertSpaceTime<PhysDim, PDEClass> PDE;
  typedef typename PDE::PhysDim PhysDimST;
  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::VectorXT VectorXT;

  explicit SetLocalTimeStep_VMSD( const PDE& pde,
                             const std::vector<int>& quadratureOrders,
                             const std::vector<int>& cellGroups,
                             Real& invCFL ) :
    pde_(pde),
    quadratureOrders_(quadratureOrders),
    cellGroups_(cellGroups),
    invCFL_(invCFL)
  {
    // Nothing
  }

  std::size_t nCellGroups() const          { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n];     }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology>
  void
  apply( const typename FieldTuple< ParamFieldType,
                                    typename MakeTuple<FieldTuple,
                                                       Field<PhysDimST, typename Topology::TopoDim, ArrayQ>,      // qfld
                                                       Field<PhysDimST, typename Topology::TopoDim, Real>,        // hfld
                                                       Field<PhysDimST, typename Topology::TopoDim, Real>>::type, // dtifld
                                    TupleClass<1>>::template FieldCellGroupType<Topology>& fldsCell,
         const int cellGroupGlobal )
  {
    typedef typename ParamFieldType                 ::template FieldCellGroupType<Topology> ParamFieldCellGroupType;
//    typedef typename Field<PhysDimST, typename Topology::TopoDim, ArrayQ>::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename HField_DG<PhysDimST, typename Topology::TopoDim>    ::template FieldCellGroupType<Topology> HFieldCellGroupType;
    typedef typename Field<PhysDimST, typename Topology::TopoDim, Real>  ::template FieldCellGroupType<Topology> TFieldCellGroupType;

//    typedef typename ParamFieldCellGroupType::template ElementType<> ElementParamFieldClass;
//    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;
    typedef typename HFieldCellGroupType::template ElementType<> ElementHFieldClass;
    typedef typename TFieldCellGroupType::template ElementType<> ElementTFieldClass;

//    typedef ElementXField<PhysDim, typename Topology::TopoDim, Topology> ElementXFieldClass;
//    typedef typename ElementParamFieldClass::T ParamT;

    const ParamFieldCellGroupType& paramfldCell = get<0>(fldsCell);

//    const QFieldCellGroupType& qfldCell         = get<0>(get<1>(fldsCell));
    const HFieldCellGroupType& hfldCell         = get<1>(get<1>(fldsCell));
          TFieldCellGroupType& dtifldCell       = const_cast<TFieldCellGroupType&>(get<2>(get<1>(fldsCell)));

    // Assert the assumption that basis function DOFs represent values
    SANS_ASSERT(  dtifldCell.basis()->category() == BasisFunctionCategory_Lagrange ||
                  (dtifldCell.basis()->category() == BasisFunctionCategory_Legendre &&
                      dtifldCell.basis()->order() == 0) );
    SANS_ASSERT_MSG( hfldCell.basis()->order() == 0, "Currently only assumes P0 h field" );

    // element field variables
    ElementHFieldClass hfldElem( hfldCell.basis() );
    ElementTFieldClass dtifldElem( dtifldCell.basis() );

    SANS_ASSERT( paramfldCell.nElem() == hfldCell.nElem() );
    SANS_ASSERT( paramfldCell.nElem() == dtifldCell.nElem() );

    const int nDOFElem = dtifldElem.nDOF();

    // loop over elements within group
    const int nelem = paramfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      hfldCell.getElement( hfldElem, elem );

      // get the inverse time step
      dtifldCell.getElement( dtifldElem, elem );

      //This is somewhat of a hack, since we don't know how to compute a characteristic "space-time" speed
      // Set the maximum inverse time for each DOF
      for (int n = 0; n < nDOFElem; n++)
        dtifldElem.DOF(n) = max(dtifldElem.DOF(n), invCFL_ / hfldElem.DOF(0));

      // set the inverse time step
      dtifldCell.setElement( dtifldElem, elem );
    }
  }

protected:
  const PDE& pde_;
  const std::vector<int> quadratureOrders_;
  const std::vector<int> cellGroups_;
  Real invCFL_;
};

}

#endif //SETLOCALTIMESTEP_VMSD_H
