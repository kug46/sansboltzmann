// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "Continuation.h"

#define PYDICT_INSTANTIATE
#include "Python/PyDict_impl.h"

namespace SANS
{

PYDICT_PARAMETER_OPTION_INSTANTIATE(SolverContinuationParams<TemporalMarch>::ContinuationOptions)
PYDICT_PARAMETER_OPTION_INSTANTIATE(SolverContinuationParams<TemporalSpaceTime>::ContinuationOptions)

// cppcheck-suppress passedByValue
void SolverContinuationParams<TemporalMarch>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Continuation));
  d.checkUnknownInputs(allParams);
}
SolverContinuationParams<TemporalMarch> SolverContinuationParams<TemporalMarch>::params;


// cppcheck-suppress passedByValue
void SolverContinuationParams<TemporalSpaceTime>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Continuation));
  d.checkUnknownInputs(allParams);
}
SolverContinuationParams<TemporalSpaceTime> SolverContinuationParams<TemporalSpaceTime>::params;

}
