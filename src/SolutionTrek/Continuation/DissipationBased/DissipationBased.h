// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SANS_DISSIPATIONBASED_H
#define SANS_DISSIPATIONBASED_H

#include "Python/PyDict.h" // Python must be included first
#include "Python/Parameter.h"

#include "tools/SANSException.h"

#include "Field/Field.h"
#include "Field/XField.h"

#include "pde/NDConvert/PDENDConvert_fwd.h"

#include "NonLinearSolver/NonLinearSolver.h"

#include "AlgebraicEquationSet_DBC.h"
#include "SolutionTrek/Continuation/Homotopy/PDEHomotopy.h"
#include "SolutionTrek/Continuation/Homotopy/BCHomotopy.h"

namespace SANS
{

//=============================================================================
struct DissipationBasedParam : noncopyable
{
  //const ParameterDict NonLinearSolver{"NonLinearSolver", NO_DEFAULT, NewtonSolverParam::checkInputs,
  //                                    "The non-linear solver for each Backwards Difference time step"};

  //const ParameterNumeric<int> order{"order", NO_DEFAULT, 1, 4, "Backwards Difference Order"};

  static void checkInputs(PyDict d);
  static DissipationBasedParam params;
};


//----------------------------------------------------------------------------//
// Reduced dissipation of the PDE in a predetermined fashion
//
// template parameters:
//----------------------------------------------------------------------------//
//template<class PhysDim, class PrimaryPDE, class AuxiliaryPDE, class Traits, class XFieldType>
//class Homotopy;

template<class PhysDim, class NDPDEClass, class Traits, class XFieldType>
class DissipationBased
{
public:
  typedef typename XFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef AlgebraicEquationSet_DBC<NDPDEClass, Traits, XFieldType> AlgebraicEquationSet_DBCClass;

  typedef typename AlgebraicEquationSet_DBCClass::BaseType AlgebraicEquationSetBaseClass;
  typedef typename AlgebraicEquationSet_DBCClass::SystemMatrix SystemMatrix;
  typedef typename AlgebraicEquationSet_DBCClass::SystemVector SystemVector;
  typedef typename AlgebraicEquationSet_DBCClass::SystemNonZeroPattern SystemNonZeroPattern;

  typedef NonLinearSolver<SystemMatrix> NonLinearSolverType;

  DissipationBased(const XFieldType& xfld,
                   Field<PhysDim, TopoDim, ArrayQ>& qfld,
                   const PyDict& nonLinearSolver_dict,
                   NDPDEClass& pde,
                   const std::vector<int>& CellGroups,
                   AlgebraicEquationSetBaseClass& AES,
                   Real& lambda) :
    xfld_(xfld),
    qfld_(qfld),
    pde_(pde),
    AES_(AES),
    algEqSetDBC_(xfld, qfld, pde_, CellGroups, AES_ ),
    nonLinearSolver_(algEqSetDBC_, nonLinearSolver_dict),
    lambda_(lambda)
  {
    // Nothing
  }

  bool iterate(const int nSteps);

protected:
  const XFieldType& xfld_;                            // grid
  Field<PhysDim, TopoDim, ArrayQ>& qfld_;             // solution field at current step

  NDPDEClass& pde_;
  AlgebraicEquationSetBaseClass& AES_;                // spatial algebraic equation set
  AlgebraicEquationSet_DBCClass algEqSetDBC_;         // HTC algebraic equation set

  NonLinearSolverType nonLinearSolver_;               // nonlinear solver for each step

  Real& lambda_;                                      // Homotopy parameter
};

//---------------------------------------------------------------------------//
template<class PhysDim, class NDPDEClass, class Traits, class XFieldType>
bool
DissipationBased<PhysDim, NDPDEClass, Traits, XFieldType >::iterate(const int nSteps)
{
  SystemVector qold(algEqSetDBC_.vectorStateSize());
  SystemVector q(algEqSetDBC_.vectorStateSize());
  SystemVector rsd(algEqSetDBC_.vectorEqSize());

  SolveStatus status;

  Real nu = 1.0;

  // Fill the solution vector from the field
  algEqSetDBC_.fillSystemVector(q);

  for (int step = 0; step < nSteps; step++)
  {
    // Move the current solution to the previous one
    qold = q;

    // Update the merging parameter
    if ( step == nSteps - 1 ) nu = 0.0;
    lambda_ = 1. - nu;
//    std::cout << " nu " << nu << " lambda_ " << lambda_ << std::endl;
    nu /= 10.0;
    // Solve the current step
    status = nonLinearSolver_.solve(qold, q);

    // Update the solution field with the current solution vector
    algEqSetDBC_.setSolutionField(q);
  }

  return status.converged;
}

}

#endif //SANS_DISSIPATIONBASED_H
