// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ALGEBRAICEQUATIONSET_DBC_H
#define ALGEBRAICEQUATIONSET_DBC_H

#include <type_traits>

#include "tools/SANSnumerics.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/tools/norm.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/tools/norm.h"

#include "Field/Field.h"
#include "Field/XField.h"

#include "LinearAlgebra/AlgebraicEquationSetBase.h"
#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"
#include "Discretization/IntegrateCellGroups.h"

#include "SolutionTrek/Continuation/Homotopy/PDEHomotopy.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template<class NDPDEClass, class Traits, class XFieldType>
class AlgebraicEquationSet_DBC
    : public AlgebraicEquationSetTraits<typename NDPDEClass::template MatrixQ<Real>,
                                        typename NDPDEClass::template ArrayQ<Real>,
                                        Traits>::AlgebraicEquationSetBaseClass
{
public:

  typedef AlgebraicEquationSetTraits<typename NDPDEClass::template MatrixQ<Real>,
                                     typename NDPDEClass::template ArrayQ<Real>,
                                     Traits> TraitsType;

  typedef typename TraitsType::AlgebraicEquationSetBaseClass BaseType;

  typedef typename NDPDEClass::PhysDim PhysDim;
  typedef typename XFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDPDEClass::template MatrixQ<Real> MatrixQ;

  typedef typename TraitsType::VectorSizeClass VectorSizeClass;
  typedef typename TraitsType::MatrixSizeClass MatrixSizeClass;

  typedef typename TraitsType::SystemMatrix SystemMatrix;
  typedef typename TraitsType::SystemVector SystemVector;
  typedef typename TraitsType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename TraitsType::SystemMatrixView SystemMatrixView;
  typedef typename TraitsType::SystemVectorView SystemVectorView;
  typedef typename TraitsType::SystemNonZeroPatternView SystemNonZeroPatternView;

  typedef typename BaseType::LinesearchData LinesearchData;

  // Field type to represent the inverse time step
  typedef Field_DG_Cell<PhysDim, TopoDim, Real> TField_DG_CellType;

  AlgebraicEquationSet_DBC(const XFieldType& xfld,
                           Field<PhysDim, TopoDim, ArrayQ>& qfld,
                           NDPDEClass& pde,
                           const std::vector<int>& CellGroups,
                           BaseType& baseAES) :
    xfld_(xfld),
    qfld_(qfld),
    pde_(pde),
    baseAES_(baseAES),
    iPDE_(baseAES_.indexPDE()),
    iq_(baseAES_.indexQ())
  {
    quadratureOrder_.resize(xfld.nCellGroups(), -1);
    quadratureOrderMin_.resize(quadratureOrder_.size(), 0);

    pde_.setMergeMode(ResidualMode);

    isStaticCondensed_ = baseAES_.isStaticCondensed();
  }

  virtual ~AlgebraicEquationSet_DBC() {}

//  // Sets the merging parameter lambda
//  void setLambda(const Real lambda)
//  {
//    pde_.setLambda(lambda);
//  }

  //Computes the residual
  virtual void residual(SystemVectorView& rsd) override;

  //Fills jacobian or the non-zero pattern of a jacobian
  virtual void jacobian(SystemMatrixView& mtx       ) override
  {
    baseAES_.jacobian(mtx);
    jacobian<SystemMatrixView&>(mtx, quadratureOrder_ );
  }
  virtual void jacobian(SystemNonZeroPatternView& nz) override
  {
    baseAES_.jacobian(nz);
    jacobian<SystemNonZeroPatternView&>(nz, quadratureOrderMin_ );
  }

  void jacobian(SystemVectorView& b, SystemNonZeroPatternView& nz, bool transposed) override
  {
    baseAES_.jacobian( b, nz, transposed );
    jacobian<SystemNonZeroPatternView&>(nz, quadratureOrderMin_ );
  }

  void jacobian(SystemVectorView& b, SystemMatrixView& mtx, bool transposed) override
  {
    baseAES_.jacobian( b, mtx, transposed );
    jacobian<SystemMatrixView&>(mtx, quadratureOrder_ );
  }

  //Fills jacobian transpose or the non-zero pattern transpose of a jacobian transpose
  virtual void jacobianTranspose(SystemMatrixView& mtxT       ) override
  {
    baseAES_.jacobian(mtxT);
    jacobian(Transpose(mtxT), quadratureOrder_ );
  }
  virtual void jacobianTranspose(SystemNonZeroPatternView& nzT) override
  {
    baseAES_.jacobian(nzT);
    jacobian(Transpose( nzT), quadratureOrderMin_ );
  }

  // Compute Residual Norm
  virtual std::vector<std::vector<Real>> residualNorm( const SystemVectorView& rsd) const override
  {
     return baseAES_.residualNorm(rsd);
  }

  //Convergence check of the residual
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm) const override
  {
    return baseAES_.convergedResidual(rsdNorm);
  }

  //Convergence check of the residual
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm, int iEq, int iMon) const override
  {
    return baseAES_.convergedResidual(rsdNorm, iEq, iMon);
  }

  // check if residual decreased
  virtual bool decreasedResidual( const std::vector<std::vector<Real>>& rsdNormOld,
                                  const std::vector<std::vector<Real>>& rsdNormNew) const override
  {
     return baseAES_.decreasedResidual(rsdNormOld, rsdNormNew);
  }

  //prints out a residual that could not be decreased and the convergence tolerances
  virtual void printDecreaseResidualFailure(const std::vector<std::vector<Real>>& rsdNorm, std::ostream& os = std::cout) const override
  {
    baseAES_.printDecreaseResidualFailure(rsdNorm, os);
  }

  //Translates the system vector into a solution field
  virtual void setSolutionField(const SystemVectorView& q) override { baseAES_.setSolutionField(q); }

  //Translates the solution field into a system vector
  virtual void fillSystemVector(SystemVectorView& q) const override { baseAES_.fillSystemVector(q); }

  // Returns the vector an matrix sizes needed for the linear algebra system
  virtual VectorSizeClass vectorEqSize() const override    // vector for equations (rows in matrixSize)
  {
    return baseAES_.vectorEqSize();
  }

  virtual VectorSizeClass vectorStateSize() const override // vector for state DOFs (columns in matrixSize)
  {
    return baseAES_.vectorStateSize();
  }

  virtual MatrixSizeClass matrixSize() const override
  {
    return baseAES_.matrixSize();
  }

  // Gives the PDE and solution indices in the system
  virtual int indexPDE() const override { SANS_ASSERT(iPDE_ == baseAES_.indexPDE()); return iPDE_; }
  virtual int indexQ() const override { SANS_ASSERT(iq_ == baseAES_.indexQ()); return iq_; }

  //Checks to see if proposed solution is physical
  virtual bool isValidStateSystemVector(SystemVectorView& q) override;

  // Returns the side of the residual norm outer vector
  virtual int nResidNorm() const override;

  // MPI communicator for this algebraic equation set
  virtual std::shared_ptr<mpi::communicator> comm() const override { return get<-1>(xfld_).comm(); }

  virtual void syncDOFs_MPI() override { baseAES_.syncDOFs_MPI(); }

  // Dump the localized linesearch parameter info (for debugging purposes)
  virtual bool updateLinesearchDebugInfo(const Real& s, const SystemVectorView& rsd,
                                         LinesearchData& pResData,
                                         LinesearchData& pStepData) const override;

  virtual void dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                                       const LinesearchData& pStepData) const override;


  // Are we having machine precision issues - assume no issues
  virtual bool atMachinePrecision(const SystemVectorView& q, const std::vector<std::vector<Real>>& R0norm) override
  {
    return baseAES_.atMachinePrecision(q, R0norm);
  }

protected:
  template<class SparseMatrixType>
  void jacobian(SparseMatrixType mtx, const std::vector<int>& quadratureOrder );

  const XFieldType& xfld_;
  Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  NDPDEClass& pde_;
  BaseType& baseAES_;

  const int iPDE_;
  const int iq_;

  std::vector<int> quadratureOrder_;
  std::vector<int> quadratureOrderMin_;

  using BaseType::isStaticCondensed_;
};

template<class NDPDEClass, class Traits, class XFieldType>
void
AlgebraicEquationSet_DBC<NDPDEClass, Traits, XFieldType>::
residual(SystemVectorView& rsd)
{
  // Compute the baseAES residual, which also updates the solution field
  baseAES_.residual(rsd);
}


template<class NDPDEClass, class Traits, class XFieldType>
template<class SparseMatrixType>
void
AlgebraicEquationSet_DBC<NDPDEClass, Traits, XFieldType>::
jacobian(SparseMatrixType jac, const std::vector<int>& quadratureOrder)
{
  // Nothing
}

template<class NDPDEClass, class Traits, class XFieldType>
bool
AlgebraicEquationSet_DBC<NDPDEClass, Traits, XFieldType>::
isValidStateSystemVector(SystemVectorView& q)
{
  return baseAES_.isValidStateSystemVector(q);
}

template<class NDPDEClass, class Traits, class XFieldType>
int
AlgebraicEquationSet_DBC<NDPDEClass, Traits, XFieldType>::
nResidNorm() const
{
  return baseAES_.nResidNorm();
}

template<class NDPDEClass, class Traits, class XFieldType>
bool
AlgebraicEquationSet_DBC<NDPDEClass, Traits, XFieldType>::
updateLinesearchDebugInfo(const Real& s, const SystemVectorView& rsd,
                          LinesearchData& pResData,
                          LinesearchData& pStepData) const
{
  return baseAES_.updateLinesearchDebugInfo(s, rsd, pResData, pStepData);
}

template<class NDPDEClass, class Traits, class XFieldType>
void
AlgebraicEquationSet_DBC<NDPDEClass, Traits, XFieldType>::
dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                        const LinesearchData& pStepData) const
{
  baseAES_.dumpLinesearchDebugInfo(filenamebase, nonlinear_iter, pStepData);
}

} //namespace SANS

#endif //ALGEBRAICEQUATIONSET_DBC_H
