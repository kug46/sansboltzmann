// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ADAPTATION_METRICCONFORMITY_H_
#define ADAPTATION_METRICCONFORMITY_H_

#include <vector>
#include <set>
#include <cmath>
#include <algorithm> // std::sort

#include "BasisFunction/ElementEdges.h"

#include "Field/XFieldLine.h"
#include "Field/XFieldArea.h"
#include "Field/XFieldVolume.h"
#include "Field/XFieldSpacetime.h"

#include "Field/FieldLine_CG_Cell.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldVolume_CG_Cell.h"
#include "Field/FieldSpacetime_CG_Cell.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "MOESS/ReferenceElementCost.h"

#include <set>

namespace SANS
{

template<class PhysDim>
Real
calculateEdgeLength( const DLA::MatrixSymS<PhysDim::D,Real>& mi ,
                     const DLA::MatrixSymS<PhysDim::D,Real>& mj ,
                     const DLA::VectorS<PhysDim::D,Real>& eij )
{
  // Edge length calculation consistent with feflo.a
  Real lmi = Transpose(eij)*mi*eij;
  Real lmj = Transpose(eij)*mj*eij;

  lmi = sqrt(lmi);
  lmj = sqrt(lmj);

  Real edgeLen = 0;

  if ( fabs(lmi-lmj) <= 1.0e-6*lmi )
    edgeLen = lmi;
  else
    edgeLen = (lmi-lmj)/(log(lmi/lmj));

  return edgeLen;
}

template<class PhysDim,class TopoDim, class Topology>
void
evaluateMetricConformity(
  const XField<PhysDim,TopoDim>& xfld ,
  const Field_CG_Cell<PhysDim,TopoDim,DLA::MatrixSymS<PhysDim::D,Real>>& metric,
  std::vector<Real>& lengths , std::vector<Real>& qualities )
{
  typedef typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology> FieldCellGroupType;
  typedef typename FieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;
  typedef DLA::VectorS<PhysDim::D,Real> VectorX;

  //The loop for setting CG field DOFs below assumes that the CG field DOF indices
  //are the same as the xfld_linear_ ones, which is true for P1.
  for (int i = 0; i < xfld.nCellGroups(); i++)
    SANS_ASSERT( xfld.getCellGroupBase(i).order() == 1 );

  // the set of unique edges we will fill
  std::set< std::pair<int,int> > edges;

  const int (*EdgeNodes)[Line::NNode] = ElementEdges<Topology>::EdgeNodes;

  // normalization factor
  Real vRef = 1./ReferenceElementCost<Topology>::getCost(1); // reciprocal of 1 element cost = volume of unit elem
  vRef = std::pow( vRef , 2./TopoDim::D );
  Real c_d = vRef/Topology::NEdge;

// just in case
  qualities.clear();
  lengths.clear();

  int cellNodes[Topology::NNode];

  // loop over cell groups
  Real total_vol = 0.0;
  for (int group=0;group<xfld.nCellGroups();group++)
  {
    // retrieve cell group
    const FieldCellGroupType& xfldGroup = xfld.template getCellGroup<Topology>(group);

    ElementXFieldClass xfldElem( xfldGroup.basis() );

    // loop over elements in cell group
    for (int elem=0;elem<xfldGroup.nElem();elem++)
    {

      // get the global mapping of this element
      xfldGroup.associativity(elem).getNodeGlobalMapping(cellNodes,Topology::NNode);
      xfldGroup.getElement( xfldElem , elem );

      // get the vertex metrics
      int imax = -1;
      Real maxdet = -1;
      for (int i=0;i<Topology::NNode;i++)
      {
        const MatrixSym& metric_i = metric.DOF(cellNodes[i]);
        if (DLA::Det(metric_i)>maxdet)
        {
          imax = i;
          maxdet = DLA::Det(metric_i);
        }
      }

      // reference the one with the maximum determinant
      const MatrixSym& Mmax = metric.DOF(cellNodes[imax]);

      // compute the volume of the element
      Real vol = xfldElem.jacobianDeterminant();
      //if (TopoDim::D==2) vol /= 2.0;
      //if (TopoDim::D==3) vol /= 6.0;
      //if (TopoDim::D==4) vol /= 24.0;
      total_vol += vol;

      vol *= std::sqrt(maxdet);
      vol = std::pow( fabs(vol) , 2./TopoDim::D );

      // compute all the edge lengths under this metric
      Real sumEdgeLengths = 0.0;
      for (int edge=0;edge<Topology::NEdge;edge++)
      {
        int node_i = cellNodes[ EdgeNodes[edge][0] ];
        int node_j = cellNodes[ EdgeNodes[edge][1] ];

        // physical vector from node_i to node_j
        VectorX eij = xfld.DOF(node_j) - xfld.DOF(node_i);

        // add the contribution to the denominator of the quality
        sumEdgeLengths += Transpose(eij)*Mmax*eij;

        // check if this edge exists in the map
        if (node_i > node_j) std::swap(node_i,node_j);
        std::pair<int,int> E = {node_i,node_j};
        if (edges.find(E)==edges.end())
        {
          // insert the edge in the list
          edges.insert(E);

          // add the length
          lengths.push_back( calculateEdgeLength<PhysDim>( metric.DOF(node_i) , metric.DOF(node_j) , eij ) );
        }
      }

      // compute the quality
      Real q = (1./c_d)*vol/sumEdgeLengths;
      qualities.push_back(q);
    }
  }
  printf("total volume = %g, nb_elem = %lu, nb_edges = %lu\n",total_vol,qualities.size(),lengths.size());
  std::sort(lengths.begin(),lengths.end());
  std::sort(qualities.begin(),qualities.end());
}

template<class PhysDim,class TopoDim>
inline void
getMetricConformity( const XField<PhysDim,TopoDim>& xfld ,
const Field_CG_Cell<PhysDim,TopoDim,DLA::MatrixSymS<PhysDim::D,Real>>& metric,
std::vector<Real>& lengths , std::vector<Real>& qualities );

template<>
inline void
getMetricConformity( const XField<PhysD1,TopoD1>& xfld ,
const Field_CG_Cell<PhysD1,TopoD1,DLA::MatrixSymS<PhysD1::D,Real>>& metric,
std::vector<Real>& lengths , std::vector<Real>& qualities )
{
  return evaluateMetricConformity<PhysD1,TopoD1,Line>(xfld,metric,lengths,qualities);
}

template<>
inline void
getMetricConformity( const XField<PhysD2,TopoD2>& xfld ,
const Field_CG_Cell<PhysD2,TopoD2,DLA::MatrixSymS<PhysD2::D,Real>>& metric,
std::vector<Real>& lengths , std::vector<Real>& qualities )
{
  return evaluateMetricConformity<PhysD2,TopoD2,Triangle>(xfld,metric,lengths,qualities);
}

template<>
inline void
getMetricConformity( const XField<PhysD3,TopoD3>& xfld ,
const Field_CG_Cell<PhysD3,TopoD3,DLA::MatrixSymS<PhysD3::D,Real>>& metric,
std::vector<Real>& lengths , std::vector<Real>& qualities )
{
  return evaluateMetricConformity<PhysD3,TopoD3,Tet>(xfld,metric,lengths,qualities);
}

template<>
inline void
getMetricConformity( const XField<PhysD4,TopoD4>& xfld ,
const Field_CG_Cell<PhysD4,TopoD4,DLA::MatrixSymS<PhysD4::D,Real>>& metric,
std::vector<Real>& lengths , std::vector<Real>& qualities )
{
  return evaluateMetricConformity<PhysD4,TopoD4,Pentatope>(xfld,metric,lengths,qualities);
}

} // SANS

#endif
