// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef MESHADAPTER_H_
#define MESHADAPTER_H_

#include <memory>

#include "Adaptation/TargetMetric.h"

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "Meshing/avro/MesherInterface_avro.h"
#include "Meshing/EPIC/MesherInterface_Epic.h"
#include "Meshing/FeFloa/MesherInterface_FeFloa.h"
#include "Meshing/Embedding/MesherInterface_Embedding.h"
#include "Meshing/AFLR/AFLR2.h"
#include "Meshing/AFLR/AFLR3.h"
#include "Meshing/refine/MesherInterface_refine.h"

#include "MOESS/MOESS.h"
#include "MOESS/SolverInterfaceBase.h"

#include "Field/XField.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Type.h"

namespace SANS
{

template <class PhysDim, class TopoDim>
class MeshAdapter;

//=============================================================================
template <class PhysDim, class TopoDim>
struct MesherOptions;

template <>
struct MesherOptions<PhysD1, TopoD1>
{
  typedef DictKeyPair ExtractType;
  const ParameterString Name{"Name", NO_DEFAULT, "Mesher name" };
  const ParameterString& key = Name;

  const DictOption embedding{"embedding", EmbeddingMesherParams::checkInputs};

  const std::vector<DictOption> options{embedding};
};

template <>
struct MesherOptions<PhysD2, TopoD2>
{
  typedef DictKeyPair ExtractType;
  const ParameterString Name{"Name", NO_DEFAULT, "Mesher name" };
  const ParameterString& key = Name;

  const DictOption FeFloa{"FeFloa", FeFloaParams::checkInputs};
  const DictOption Epic{"Epic", EpicParams::checkInputs};
  const DictOption refine{"refine", refineParams::checkInputs};
#ifdef SANS_AVRO
  const DictOption avro{"avro", avroParams::checkInputs};
#endif
#ifdef SANS_AFLR
  const DictOption AFLR{"AFLR", AFLRParams::checkInputs};
#endif

  const std::vector<DictOption> options{FeFloa, Epic, refine
#ifdef SANS_AVRO
                                       , avro
#endif
#ifdef SANS_AFLR
                                       , AFLR
#endif
                                       };
};

template<>
struct MesherOptions<PhysD3, TopoD3>
{
  typedef DictKeyPair ExtractType;
  const ParameterString Name{"Name", NO_DEFAULT, "Mesher name" };
  const ParameterString& key = Name;

  const DictOption FeFloa{"FeFloa", FeFloaParams::checkInputs};
  const DictOption Epic{"Epic", EpicParams::checkInputs};
  const DictOption refine{"refine", refineParams::checkInputs};
#ifdef SANS_AVRO
  const DictOption avro{"avro", avroParams::checkInputs};
#endif
#ifdef SANS_AFLR
  const DictOption AFLR{"AFLR", AFLRParams::checkInputs};
#endif

  const std::vector<DictOption> options{FeFloa, Epic, refine
#ifdef SANS_AVRO
                                       , avro
#endif
#ifdef SANS_AFLR
                                       , AFLR
#endif
                                       };
};

template <>
struct MesherOptions<PhysD4, TopoD4>
{
  typedef DictKeyPair ExtractType;
  const ParameterString Name{"Name", NO_DEFAULT, "Mesher name" };
  const ParameterString& key = Name;

#ifdef SANS_AVRO
  const DictOption avro{"avro", avroParams::checkInputs};
#endif

  const std::vector<DictOption> options{
#ifdef SANS_AVRO
                                        avro
#endif
                                       };
};

namespace MeshAdapter_detail
{
  struct AlgorithmOptions
  {
    typedef DictKeyPair ExtractType;
    const ParameterString Name{"Name", "MOESS", "Algorithm name" };
    const ParameterString& key = Name;

    const DictOption MOESS{"MOESS", MOESSParams::checkInputs};

    const std::vector<DictOption> options{MOESS};
  };
}

template <class PhysDim, class TopoDim>
struct MeshAdapterParams : noncopyable
{
  const ParameterNumeric<Real> TargetCost{"TargetCost", NO_DEFAULT, 0.0, NO_LIMIT, "Target cost for adaptation"};

  const ParameterOption<MeshAdapter_detail::AlgorithmOptions> Algorithm{"Algorithm", NO_DEFAULT, "Adaptation algorithm"};

  const ParameterString FilenameBase{"FilenameBase", "tmp/", "Default filepath prefix for generated files"};

  const ParameterBool dumpMetric        {"dumpMetric"       , false, "Dump requested nodal metric field?"};
  const ParameterBool dumpImpliedMetric {"dumpImpliedMetric", false, "Dump implied nodal metric field?"};
  const ParameterBool dumpStepMatrix    {"dumpStepMatrix"   , false, "Dump nodal step matrix field?"};
  const ParameterBool dumpRateMatrix    {"dumpRateMatrix"   , false, "Dump rate matrix field?"};
  const ParameterBool dumpEdgeLength    {"dumpEdgeLength"   , false, "Dump edge length field?"};
  const ParameterBool dumpError         {"dumpError"        , false, "Dump elemental error field?"};
  const ParameterBool dumpOptimizationInfo{"dumpOptimizationInfo", false, "Dump optimization info?"};

  const ParameterBool hasTrueOutput{"hasTrueOutput", false, "Is the true output value known?"};
  const ParameterNumeric<Real> TrueOutput{"TrueOutput", 0.0, NO_RANGE, "True output value (if known)"};

  const ParameterOption<MesherOptions<PhysDim,TopoDim>> Mesher{"Mesher", NO_DEFAULT, "Mesh generator"};

  static void checkInputs(PyDict d);
  static MeshAdapterParams params;
};

//=============================================================================
template <class PhysDim, class TopoDim>
class MeshAdapter
{
public:
  static const int D = PhysDim::D;
  typedef DLA::MatrixSymS<D,Real> MatrixSym;

  typedef XField<PhysDim, TopoDim> XFieldType;
  typedef std::shared_ptr<XFieldType> MeshPtr;
  typedef std::pair<MeshPtr,MeshPtr> MeshPtrPair; //{linear mesh, curved mesh}

  MeshAdapter(const PyDict& d, std::fstream& fadapthist);

  //interface for linear meshes
  MeshPtr adapt(const XFieldType& xfld,
                const std::vector<int>& cellGroups, const SolverInterfaceBase<PhysDim,TopoDim>& interface, const int adapt_iter);

  //interface for curved meshes
  MeshPtrPair adapt(const XFieldType& xfld_linear, const XFieldType& xfld_curved,
                    const std::vector<int>& cellGroups, const SolverInterfaceBase<PhysDim,TopoDim>& interface, const int adapt_iter);

  std::shared_ptr< XField<PhysDim,TopoDim> > adapt_metric(
    const XField<PhysDim,TopoDim>& xfld ,
    const std::vector<int>& cellGroups,
    Field_CG_Cell<PhysDim,TopoDim,MatrixSym>& metric , const int adapt_iter);

  // interface for linear meshes
  MeshPtr adapt_implied_metric(
    const XFieldType& xfld,
    const std::vector<int>& cellGroups, const int adapt_iter);

  // interface for curved meshes
  MeshPtrPair adapt_implied_metric(
    const XFieldType& xfld_linear, const XFieldType& xfld_curved,
    const std::vector<int>& cellGroups, const int adapt_iter);

  void adjustTargetCost( const Real targetCost ) { SANS_ASSERT(targetCost>0); targetCost_ = targetCost; }

protected:

  Real targetCost_;
  const PyDict MOESSDict_;
  PyDict MesherDict_;
  std::string filename_base_;
  bool dumpMetric_;
  bool dumpImpliedMetric_;
  bool dumpStepMatrix_;
  bool dumpRateMatrix_;
  bool dumpEdgeLength_;
  bool dumpError_;
  bool dumpOptimizationInfo_;
  bool hasTrueOutput_;
  const Real trueOutput_;
  std::fstream& fadapthist_;
};

}

#endif /* MESHADAPTER_H_ */
