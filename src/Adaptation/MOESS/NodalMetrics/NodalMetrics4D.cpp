// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define NODALMETRICS_INSTANTIATE
#include "NodalMetrics_impl.h"

#include "Field/FieldSpacetime_CG_Cell.h"

#include "Field/XFieldSpacetime.h"

namespace SANS
{

//Explicit instantiations
template class NodalMetrics<PhysD4,TopoD4>;

} // namespace SANS
