// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(NODALMETRICS_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "../NodalMetrics.h"

#include "tools/timer.h"
#include "tools/smoothmath.h"

#include "Surreal/SurrealS.h"

#include "Field/Field_NodalView.h"
#include "Field/tools/for_each_CellGroup.h"

// Needed for edge length calculations
#include "Field/Element/ElementXFieldLine.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_FrobNorm.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS_InnerProduct.h"

#include "Meshing/Metric/MetricOps.h"

#include "Quadrature/Quadrature.h"
#include "Quadrature/QuadratureLine.h"

#include "Adaptation/MOESS/ReferenceElementCost.h"
#include "Adaptation/MOESS/CostModel_LogEuclidean.h"

#include "Adaptation/MOESS/NLOPT/ImpliedMetric_Optimizer_NLOPT.h"

#ifdef SANS_PETSC
#include "Adaptation/MOESS/TAO/ImpliedMetric_Optimizer_TAO.h"
#endif

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#include <boost/mpi.hpp>
#include <boost/serialization/vector.hpp>
#include "MPI/serialize_DenseLinAlg_MatrixS.h"
#endif

// Use the max determinant attached metric (this is what avro does...)
#define METRIC_MAX

// #define LOGEUCLIDEAN_1D

namespace SANS
{

//===========================================================================//
template <class PhysDim, class TopoDim>
NodalMetrics<PhysDim, TopoDim>::
NodalMetrics( const XField<PhysDim,TopoDim>& xfld_linear,
              const std::vector<int>& cellgroup_list,
              const SolverInterfaceBase<PhysDim,TopoDim>& problem,
              const PyDict& paramsDict )
  : xfld_linear_(xfld_linear),
    cellgroup_list_(cellgroup_list),
    implied_metric_(xfld_linear_, 1, BasisFunctionCategory_Lagrange, cellgroup_list),
    nodalStepMatrices_(implied_metric_.nDOF(), 0),
    costModel_(implied_metric_, cellgroup_list_),
    problem_(problem),
    ImpliedMetric_(paramsDict.get(ParamsType::params.ImpliedMetric)),
    verbosity_(paramsDict.get(ParamsType::params.Verbosity)),
    DOFIncreaseFactor_(paramsDict.get(ParamsType::params.DOFIncreaseFactor)),
    paramsDict_(paramsDict)
{
  if (paramsDict.get(ParamsType::params.MetricNorm) == ParamsType::params.MetricNorm.L2)
    metricNorm_ = L2;
  else if (paramsDict.get(ParamsType::params.MetricNorm) == ParamsType::params.MetricNorm.Linf)
    metricNorm_ = Linf;
  else
    SANS_DEVELOPER_EXCEPTION("Missing metric norm option");

  //xfld_linear_ should be linear (i.e. Q1), and is used for the implied metric
  for (int i = 0; i < xfld_linear_.nCellGroups(); i++)
    SANS_ASSERT( xfld_linear_.getCellGroupBase(i).order() == 1 );

  // compute the true initial cost and complexity
  computeInitialCostTrue();

  // extract all the edges from the mesh
  extractEdges();

  // initialize the implied metrics
  computeImpliedMetrics();
}

//===========================================================================//
template <class PhysDim, class TopoDim>
NodalMetrics<PhysDim, TopoDim>::
NodalMetrics(const XField<PhysDim,TopoDim>& xfld_linear,
             const std::vector<int>& cellgroup_list,
             const SolverInterfaceBase<PhysDim,TopoDim>& problem,
             const PyDict& paramsDict,
             const NodalMetrics<PhysDim,TopoDim>& nodalMetrics_rank0)
  : xfld_linear_(xfld_linear),
    cellgroup_list_(cellgroup_list),
    implied_metric_(xfld_linear_, 1, BasisFunctionCategory_Lagrange, cellgroup_list),
    nodalStepMatrices_(implied_metric_.nDOF(), 0),
    costModel_(implied_metric_, cellgroup_list_),
    problem_(problem),
    ImpliedMetric_(paramsDict.get(ParamsType::params.ImpliedMetric)),
    verbosity_(paramsDict.get(ParamsType::params.Verbosity)),
    DOFIncreaseFactor_(paramsDict.get(ParamsType::params.DOFIncreaseFactor)),
    paramsDict_(paramsDict)
{
  if (paramsDict.get(ParamsType::params.MetricNorm) == ParamsType::params.MetricNorm.L2)
    metricNorm_ = L2;
  else if (paramsDict.get(ParamsType::params.MetricNorm) == ParamsType::params.MetricNorm.Linf)
    metricNorm_ = Linf;
  else
    SANS_DEVELOPER_EXCEPTION("Missing metric norm option");

  //xfld_linear_ should be linear (i.e. Q1), and is used for the implied metric
  for (int i = 0; i < xfld_linear_.nCellGroups(); i++)
    SANS_ASSERT( xfld_linear_.getCellGroupBase(i).order() == 1 );

#define NODE_TAG 0
#define METRICS_TAG 1
#define STEP_TAG 2

  if (xfld_linear_.comm()->rank() == 0)
  {
    const int nNode = implied_metric_.nDOF();

    // copy over arrays on rank 0
    nodalMetricSqrt_.resize(nNode);
    nodalMetricLamMax_.resize(nNode);
    nodalStepMatrices_.resize(nNode);

    for (int local_node = 0; local_node < nNode; local_node++)
    {
      int native_node = implied_metric_.local2nativeDOFmap(local_node);

      implied_metric_.DOF(local_node)  = nodalMetrics_rank0.implied_metric_.DOF(native_node);
      nodalMetricSqrt_[local_node]     = nodalMetrics_rank0.nodalMetricSqrt_[native_node];
      nodalMetricLamMax_[local_node]   = nodalMetrics_rank0.nodalMetricLamMax_[native_node];
      nodalStepMatrices_[local_node]   = nodalMetrics_rank0.nodalStepMatrices_[native_node];
    }

    // send metrics and step matricies to all other ranks
    int comm_size = xfld_linear_.comm()->size();

    for (int rank = 1; rank < comm_size; rank++)
    {
      // recieve the array of node indexes that need to be sent
      std::vector<int> remoteNodes;
      xfld_linear_.comm()->recv(rank, NODE_TAG, remoteNodes);

      std::vector<MatrixSym> tmpMetrics(remoteNodes.size());
      std::vector<MatrixSym> tmpStepMatrices(remoteNodes.size());

      for (std::size_t node = 0; node < remoteNodes.size(); node++)
      {
        tmpMetrics[node]      = nodalMetrics_rank0.implied_metric_.DOF(remoteNodes[node]);
        tmpStepMatrices[node] = nodalMetrics_rank0.nodalStepMatrices_[remoteNodes[node]];
      }

      // send the nodal metrics and step matricies
      xfld_linear_.comm()->send(rank, METRICS_TAG, tmpMetrics);
      xfld_linear_.comm()->send(rank, STEP_TAG, tmpStepMatrices);
    }
  }
  else
  {
    std::vector<int> sendNodes(implied_metric_.nDOF());
    for (std::size_t i = 0; i < sendNodes.size(); i++)
      sendNodes[i] = implied_metric_.local2nativeDOFmap(i);

    // send the requested nodes to rank 0
    xfld_linear_.comm()->send(0, NODE_TAG, sendNodes);

    std::vector<MatrixSym> tmpMetrics(sendNodes.size());
    nodalStepMatrices_.resize(sendNodes.size());

    // recv the nodal metrics and step matricies
    xfld_linear_.comm()->recv(0, METRICS_TAG, tmpMetrics);
    xfld_linear_.comm()->recv(0, STEP_TAG, nodalStepMatrices_);

    nodalMetricSqrt_.resize(tmpMetrics.size());
    nodalMetricLamMax_.resize(tmpMetrics.size());

    for (int node = 0; node < implied_metric_.nDOF(); node++)
    {
      implied_metric_.DOF(node) = tmpMetrics[node];

      nodalMetricSqrt_[node] = sqrt(implied_metric_.DOF(node));
      DLA::VectorS<D,Real> lam;
      DLA::EigenValues(implied_metric_.DOF(node), lam);
      Real lammax = lam[0];
      for (int i = 0; i < D; i++)
        lammax = max(lam[i], lammax);
      nodalMetricLamMax_[node] = lammax;
    }
  }

#undef NODE_TAG
#undef METRICS_TAG
#undef STEP_TAG
}

//===========================================================================//
template <class PhysDim, class TopoDim>
NodalMetrics<PhysDim, TopoDim>::
NodalMetrics(const XField<PhysDim,TopoDim>& xfld_linear,
             const std::vector<int>& cellgroup_list,
             const MatrixSymFieldType& target_metric,
             const SolverInterfaceBase<PhysDim,TopoDim>& problem,
             const PyDict& paramsDict )
  : xfld_linear_(xfld_linear),
    cellgroup_list_(cellgroup_list),
    implied_metric_(xfld_linear_, 1, BasisFunctionCategory_Lagrange, cellgroup_list),
    nodalStepMatrices_(implied_metric_.nDOF(), 0),
    costModel_(implied_metric_, cellgroup_list_),
    problem_(problem),
    ImpliedMetric_(paramsDict.get(ParamsType::params.ImpliedMetric)),
    verbosity_(paramsDict.get(ParamsType::params.Verbosity)),
    DOFIncreaseFactor_(paramsDict.get(ParamsType::params.DOFIncreaseFactor)),
    paramsDict_(paramsDict)
{
  if (paramsDict.get(ParamsType::params.MetricNorm) == ParamsType::params.MetricNorm.L2)
    metricNorm_ = L2;
  else if (paramsDict.get(ParamsType::params.MetricNorm) == ParamsType::params.MetricNorm.Linf)
    metricNorm_ = Linf;
  else
    SANS_DEVELOPER_EXCEPTION("Missing metric norm option");

  //xfld_linear_ should be linear (i.e. Q1), and is used for the implied metric
  for (int i = 0; i < xfld_linear_.nCellGroups(); i++)
    SANS_ASSERT( xfld_linear_.getCellGroupBase(i).order() == 1 );

  // compute the true initial cost and complexity
  computeInitialCostTrue();

  // initialize the implied metrics
  computeImpliedMetrics();

  // compute the limited step matrix to the target metric
  matchTargetMetric(target_metric);
}

//===========================================================================//
template <class PhysDim, class TopoDim>
class ElementalMetric : public GroupFunctorCellType<ElementalMetric<PhysDim, TopoDim>>
{
public:
  typedef typename NodalMetrics<PhysDim, TopoDim>::ElementalMetricVectorType ElementalMetricVectorType;

  ElementalMetric( const std::vector<int>& cellgroup_list,
                   ElementalMetricVectorType& elementalMetrics,
                   std::vector<std::vector<Real>>& cellJacobianDeterminant )
  : cellgroup_list_(cellgroup_list),
    elementalMetrics_(elementalMetrics),
    cellJacobianDeterminant_(cellJacobianDeterminant) {}

  std::size_t nCellGroups() const          { return cellgroup_list_.size(); }
  std::size_t cellGroup(const int n) const { return cellgroup_list_[n];     }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology>
  void
  apply(const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
        const int cellGroupGlobal)
  {
    //Cell Group Types
    typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

    ElementXFieldClass xfldElem(xfldCellGroup.basis() );

    int nElem = xfldCellGroup.nElem(); //Number of elements in cell group

    elementalMetrics_[cellGroupGlobal].resize(nElem,0.0);
    cellJacobianDeterminant_[cellGroupGlobal].resize(nElem,0.0);

    for (int elem = 0; elem < nElem; elem++ )
    {
      xfldCellGroup.getElement(xfldElem, elem);

      //Save off elemental implied metric and jacobian determinant
      xfldElem.impliedMetric(elementalMetrics_[cellGroupGlobal][elem]);
      cellJacobianDeterminant_[cellGroupGlobal][elem] = xfldElem.jacobianDeterminant();
    }
  }

protected:
  const std::vector<int> cellgroup_list_;

  ElementalMetricVectorType& elementalMetrics_;              //indexing: [global cellgrp][elem]
  std::vector<std::vector<Real>>& cellJacobianDeterminant_;  //indexing: [global cellgrp][elem]
};

//===========================================================================//
template <class PhysDim, class TopoDim>
void
NodalMetrics<PhysDim, TopoDim>::
computeImpliedMetrics()
{
  timer impliedMetricTime;

  int nNode = implied_metric_.nDOF();

  Field_NodalView metric_nodalview(implied_metric_, cellgroup_list_);

  //Metrics and jacobian determinant on each element (indexing: [global cellgrp][elem])
  ElementalMetricVectorType elementalMetrics;
  std::vector<std::vector<Real>> cellJacobianDeterminant;

  elementalMetrics.resize(xfld_linear_.nCellGroups());
  cellJacobianDeterminant.resize(xfld_linear_.nCellGroups());

  // compute the elemental metric and jacobian determinant
  for_each_CellGroup<TopoDim>::apply( ElementalMetric<PhysDim, TopoDim>(cellgroup_list_, elementalMetrics, cellJacobianDeterminant),
                                      xfld_linear_);

  nodalMetricSqrt_.resize(nNode);
  nodalMetricLamMax_.resize(nNode);

  std::vector<MatrixSym> metric_list;
  std::vector<Real> weight_list;

  if ( (ImpliedMetric_ == MOESSParams::params.ImpliedMetric.VolumeWeighted) ||
       (ImpliedMetric_ == MOESSParams::params.ImpliedMetric.UniformWeighted)||
       (ImpliedMetric_ == MOESSParams::params.ImpliedMetric.Optimized) )
  {
    for (int node = 0; node < nNode; node++)
    {
      //Get the list of cells around this node (each returned pair contains cell_group and cell_elem)
      Field_NodalView::IndexVector cell_list = metric_nodalview.getCellList(node);

      if (cell_list.size() == 1) //Only one cell at this node -> nothing to average
      {
        int cellgrp = cell_list[0].group;
        int elem = cell_list[0].elem;
        implied_metric_.DOF(node) = elementalMetrics[cellgrp][elem];
      }
      else //more than one cell around this node
      {
        int nCells = (int) cell_list.size();
        metric_list.resize(nCells);
        weight_list.resize(nCells,1);
        Real weight_sum = 0.0;

        for (int i = 0; i < nCells; i++)
        {
          int cellgrp = cell_list[i].group;
          int elem = cell_list[i].elem;
          metric_list[i] = elementalMetrics[cellgrp][elem];

          //Compute error weights
          if ( ImpliedMetric_ == MOESSParams::params.ImpliedMetric.VolumeWeighted )
            weight_list[i] = cellJacobianDeterminant[cellgrp][elem]; //Get the size of the element

          weight_sum += weight_list[i];
        }

        //Avoid dividing by zero
        if (weight_sum == 0.0)
        {
          for (int i = 0; i < nCells; i++)
            weight_list[i] = 1.0/((Real)nCells); //Use equal weights
        }
        else
        {
          for (int i = 0; i < nCells; i++)
            weight_list[i] /= weight_sum; //Normalize weights
        }

        // Compute the weighted metric average
        implied_metric_.DOF(node) = Metric::computeLogEuclideanAvg(metric_list, weight_list);
      }
    } //loop over nodes

    // synchronize ghost/zombie DOFs
    implied_metric_.syncDOFs_MPI_noCache();

    // Volume weighted log-Euclidean averaged metrics is used as the initial guess for the implied metric
    if (ImpliedMetric_ == MOESSParams::params.ImpliedMetric.Optimized)
    {
      // Save off sqrt of the nodal metrics to be used in an exponential map
      for (int node = 0; node < nNode; node++)
        nodalMetricSqrt_[node] = sqrt(implied_metric_.DOF(node));

      //Optimize the implied metric so the mesher is stationary with the implied metric
#ifdef SANS_PETSC
      std::string Optimization = paramsDict_.get(ParamsType::params.MetricOptimization);
      if (Optimization == ParamsType::params.MetricOptimization.SANSparallel)
        ImpliedMetric_Optimizer<PhysDim, TopoDim, TAO> optimizer(*this);
      else
#endif
      {
        //implied_metric_ is updated by the optimizer
        ImpliedMetric_Optimizer<PhysDim, TopoDim, NLOPT> optimizer(*this);
      }
    }
  }
  else if ( (ImpliedMetric_ == MOESSParams::params.ImpliedMetric.AffineInvariant)  )
  {
    unsigned int cnt_unconv = 0;
    Real maxNorm = 0.0;
    for (int node = 0; node < nNode; node++)
    {
      //Get the list of cells around this node (each returned pair contains cell_group and cell_elem)
      Field_NodalView::IndexVector cell_list = metric_nodalview.getCellList(node);

      std::pair<MatrixSym,OptResult> Mpair;

      if (cell_list.size() == 1) //Only one cell at this node -> nothing to average
      {
        int cellgrp = cell_list[0].group;
        int elem = cell_list[0].elem;
        implied_metric_.DOF(node) = elementalMetrics[cellgrp][elem];
      }
      else //more than one cell around this node
      {
        int nCells = (int) cell_list.size();
        metric_list.resize(nCells);

        for (int i = 0; i < nCells; i++)
        {
          int cellgrp = cell_list[i].group;
          int elem = cell_list[i].elem;
          metric_list[i] = elementalMetrics[cellgrp][elem];
        }
        // Compute the affine invariant metric average
        // std::cout << "node = " << node << ", nMetric = " << metric_list.size() << ", ";
        Mpair = Metric::computeAffineInvAvgResult(metric_list);
        implied_metric_.DOF(node) = Mpair.first;
        maxNorm = std::max(maxNorm,Mpair.second.resid);
        if (Mpair.second.converged == false)
          cnt_unconv++;
      }
    } //loop over nodes

    unsigned int cnt_unconv_global;
    Real maxNorm_global;
#ifdef SANS_MPI
    // send all to processor 0
    boost::mpi::reduce( *implied_metric_.comm(), cnt_unconv, cnt_unconv_global, std::plus<unsigned int>(), 0 );
    boost::mpi::reduce( *implied_metric_.comm(), maxNorm, maxNorm_global, std::plus<Real>(), 0 );
#endif
    cnt_unconv_global = cnt_unconv;
    maxNorm_global = maxNorm;

    if (implied_metric_.comm()->rank() == 0 && cnt_unconv_global > 0)
      std::cout<< "warning: unconverged affine invariant metric calulations = "
               << cnt_unconv_global << " / " << nNode << ", max resid = " << std::scientific << maxNorm_global << std::fixed << std::endl;

    // synchronize ghost/zombie DOFs
    implied_metric_.syncDOFs_MPI_noCache();

  }
  else if ( (ImpliedMetric_ == MOESSParams::params.ImpliedMetric.InverseLengthTensor) )
  {
    // compute the inverse of the edge length tensor at each vertex

    // need to construct list of edges attached to each vertex
    std::vector<std::pair<int,int>> edgeNodes;
    std::vector<VectorX> edgeVectors;
    for (int node = 0; node < nNode; node++)
    {
      edgeNodes.clear();
      edgeVectors.clear();

      // This is assuming that the xfld_linear and implied metric field dofs match up
      edgeNodes = metric_nodalview.getEdgeList(node);

      for (const auto& edge: edgeNodes )
        edgeVectors.push_back( xfld_linear_.DOF(edge.first) - xfld_linear_.DOF(edge.second) );

      implied_metric_.DOF(node) = Metric::computeInverseLengthDistributionTensor(edgeVectors);
    }
    // synchronize ghost/zombie DOFs
    implied_metric_.syncDOFs_MPI_noCache();

  }
  else
    SANS_DEVELOPER_EXCEPTION( "Unknown Implied Metric calculation option: %s", ImpliedMetric_.c_str() );

  MatrixSymFieldType Sfld(xfld_linear_, 1, BasisFunctionCategory_Lagrange, cellgroup_list_);
  Sfld = 0;
  Real Cost = 0;
  std::vector<MatrixSym> dCost_dS;
  computeCost(Sfld, Cost, dCost_dS);
  if (comm()->rank() == 0 )
  {
    std::cout << "Implied Metric Calculation" << std::endl;
    std::cout << " Time           : " << impliedMetricTime.elapsed() << " s" << std::endl;
    std::cout << " Implied Cost   : " << Cost << std::endl;
    std::cout << " Actual Cost    : " << cost_initial_true_ << std::endl;
  }

#if 0
  // Compute diagonal value of step matrix that will adjust estimated cost to the true cost
  Real s_k = 2.0*log(cost_initial_true_/Cost) / ((Real) D);

  MatrixSym expS = 0;
  for (int d = 0; d < D; d++)
    expS(d,d) = exp(s_k);

  for (int node = 0; node < nNode; node++)
  {
    MatrixSym sqrt_M0 = sqrt(implied_metric_.DOF(node));
    implied_metric_.DOF(node) = sqrt_M0*expS*sqrt_M0;
  }

  Cost = 0;
  computeCost(Sfld, Cost, dCost_dS);
  if (comm()->rank() == 0 )
    std::cout << " Corrected Cost : " << Cost << std::endl;
#endif

  if ( (ImpliedMetric_ != MOESSParams::params.ImpliedMetric.Optimized) )
  {
    Real Qmean,Qmin,Qmax;
    computeQualityStatistics(implied_metric_,Qmean,Qmin,Qmax);
    if (comm()->rank() == 0 )
    {
      std::cout << " Mean, Min, Max Quality   : " << Qmean << ", " << Qmin << ", " << Qmax << std::endl;
    }
  }


  // Save off sqrt of the nodal metrics to be used in an exponential map
  for (int node = 0; node < nNode; node++)
  {
    DLA::EigenSystemPair<D,Real> LE(implied_metric_.DOF(node));

    nodalMetricSqrt_[node] = sqrt(LE);
    Real lammax = LE.L[0];
    for (int i = 0; i < D; i++)
      lammax = max(LE.L[i], lammax);
    nodalMetricLamMax_[node] = lammax;
  }
}

//===========================================================================//
template <class PhysDim, class TopoDim>
void
NodalMetrics<PhysDim, TopoDim>::
matchTargetMetric( const MatrixSymFieldType& target )
{
  int nNode = implied_metric_.nDOF();

  const Real h_ref = 2.0;
  int nlimited = 0;

  nodalStepMatrices_.resize(nNode);
  for (int node=0; node < nNode; node++)
  {
    // compute the step from the implied metric to the target
    const MatrixSym& mi = implied_metric_.DOF( node );

    const MatrixSym& mt = target.DOF( node );
    MatrixSym s = Metric::computeStepMatrix(mi,mt);

    // limit the step
    bool limited = false;
    for (int i = 0; i < PhysDim::D; i++)
      for (int j = 0; j <= i; j++)
      {
        if (s(i,j) > 2*log(h_ref))
        {
          s(i,j) = 2*log(h_ref);
          if (!limited) nlimited++;
          limited = true;
        }
        else if (s(i,j) < -2*log(h_ref))
        {
          s(i,j) = -2*log(h_ref);
          if (!limited) nlimited++;
          limited = true;
        }
      }

    // save the nodal the possibly limited step
    nodalStepMatrices_[node] = s;
  }

  printf("limited %d step matrices out of %d.\n", nlimited, nNode);
}

//===========================================================================//
template <class PhysDim, class TopoDim>
class InitialCost : public GroupFunctorCellType<InitialCost<PhysDim, TopoDim>>
{
public:
  InitialCost( const int comm_rank, const std::vector<int>& cellgroup_list,
               Real& cost_initial_true,
               Real& complexity_initial_true,
               const SolverInterfaceBase<PhysDim,TopoDim>& problem )
    : comm_rank_(comm_rank), cellgroup_list_(cellgroup_list),
      cost_initial_true_(cost_initial_true),
      complexity_initial_true_(complexity_initial_true), problem_(problem) {}

  std::size_t nCellGroups() const          { return cellgroup_list_.size(); }
  std::size_t cellGroup(const int n) const { return cellgroup_list_[n];     }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology>
  void
  apply(const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
        const int cellGroupGlobal)
  {
    // Number of DOFs in each cell
    Real nDOFperCell = problem_.getnDOFperCell(cellGroupGlobal);

    // Equilateral reference element size
    Real size = 1./ReferenceElementCost<Topology>::getCost(1);

    int nElem = xfldCellGroup.nElem();

    for (int elem = 0; elem < nElem; elem++)
    {
      if (xfldCellGroup.associativity(elem).rank() != comm_rank_) continue;

      cost_initial_true_ += nDOFperCell;
      complexity_initial_true_ += size;
    }
  }

protected:
  const int comm_rank_;
  const std::vector<int> cellgroup_list_;
  Real& cost_initial_true_;
  Real& complexity_initial_true_;
  const SolverInterfaceBase<PhysDim,TopoDim>& problem_;
};

//===========================================================================//
template <class PhysDim, class TopoDim>
void
NodalMetrics<PhysDim, TopoDim>::
computeInitialCostTrue()
{
  cost_initial_true_ = 0.0;
  complexity_initial_true_ = 0.0;

  // compute the initial cost and complexity of the grid
  for_each_CellGroup<TopoDim>::apply(
      InitialCost<PhysDim, TopoDim>(xfld_linear_.comm()->rank(), cellgroup_list_, cost_initial_true_, complexity_initial_true_, problem_),
      xfld_linear_);

  //computeNodalError0();

#ifdef SANS_MPI
  // sum across all processors
  cost_initial_true_ = boost::mpi::all_reduce(*xfld_linear_.comm(), cost_initial_true_, std::plus<Real>());
  complexity_initial_true_ = boost::mpi::all_reduce(*xfld_linear_.comm(), complexity_initial_true_, std::plus<Real>());
#endif
}

//===========================================================================//
template <class PhysDim, class TopoDim>
class ExtractEdges : public GroupFunctorCellType<ExtractEdges<PhysDim, TopoDim>>
{
public:
  typedef typename NodalMetrics<PhysDim, TopoDim>::ElementalMetricVectorType ElementalMetricVectorType;
  typedef std::pair<int,int> NodePair;

  ExtractEdges( const std::vector<int>& cellgroup_list,
                NodalMetrics<PhysDim, TopoDim>& nodalMetrics,
                std::map<NodePair, NodePair>& edges,
                std::map<NodePair, NodePair>& edgesGhost)
  : cellgroup_list_(cellgroup_list),
    nodalMetrics_(nodalMetrics), edges_(edges), edgesGhost_(edgesGhost){}

  std::size_t nCellGroups() const          { return cellgroup_list_.size(); }
  std::size_t cellGroup(const int n) const { return cellgroup_list_[n];     }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology>
  void
  apply(const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
        const int cellGroupGlobal)
  {
    nodalMetrics_.insertEdges(xfldCellGroup,
                              nodalMetrics_.impliedMetrics().template getCellGroupGlobal<Topology>(cellGroupGlobal),
                              edges_, edgesGhost_);
  }

protected:
  const std::vector<int> cellgroup_list_;
  NodalMetrics<PhysDim, TopoDim>& nodalMetrics_;
  std::map<NodePair, NodePair>& edges_;
  std::map<NodePair, NodePair>& edgesGhost_;
};

//===========================================================================//
template <class PhysDim, class TopoDim>
void
NodalMetrics<PhysDim, TopoDim>::extractEdges()
{
  std::map<NodePair, NodePair> edges;
  std::map<NodePair, NodePair> edgesGhost;

  // extract all the edges from the grid
  for_each_CellGroup<TopoDim>::apply(
      ExtractEdges<PhysDim, TopoDim>(cellgroup_list_, *this, edges, edgesGhost), xfld_linear_);

  // convert the set to a vector for faster access
  edges_.resize(edges.size());
  std::size_t i = 0;
  for (const std::pair<NodePair, NodePair>& edge : edges)
  {
    edges_[i].ifld = edge.first;
    edges_[i].xfld = edge.second;
    i++;
  }

  edgesGhost_.resize(edgesGhost.size());
  i = 0;
  for (const std::pair<NodePair, NodePair>& edge : edgesGhost)
  {
    edgesGhost_[i].ifld = edge.first;
    edgesGhost_[i].xfld = edge.second;
    i++;
  }

#if 0
  for (int rank = 0; rank < xfld_linear_.comm()->size(); rank++)
  {
    xfld_linear_.comm()->barrier();
    if (rank != xfld_linear_.comm()->rank()) continue;
    std::cout << implied_metric_.nDOFpossessed() << std::endl;
    for (const Edge& edge : edges_)
      std::cout << rank << " " << implied_metric_.local2nativeDOFmap(edge.ifld.first)
                        << " " << implied_metric_.local2nativeDOFmap(edge.ifld.second)
                        << " : " << edge.ifld.first << " " << edge.ifld.second << std::endl;

    for (const Edge& edge : edgesGhost_)
      std::cout << rank << " g " << implied_metric_.local2nativeDOFmap(edge.ifld.first)
                        << " " << implied_metric_.local2nativeDOFmap(edge.ifld.second)
                        << " : " << edge.ifld.first << " " << edge.ifld.second << std::endl;
  }
#endif
}

//===========================================================================//
template <class PhysDim, class TopoDim>
template<class XTraits, class ITraits>
void
NodalMetrics<PhysDim, TopoDim>::
insertEdges( const FieldAssociativity< XTraits >& xfldGroup,
             const FieldAssociativity< ITraits >& ifldGroup,
             std::map<NodePair, NodePair>& edges,
             std::map<NodePair, NodePair>& edgesGhost )
{
  typedef typename XTraits::TopologyType Topology;

  int xnodeMap[Topology::NNode];
  int inodeMap[Topology::NNode];
  const int (*EdgeNodes)[ Line::NNode ] = ElementEdges<Topology>::EdgeNodes;

  for (int elem = 0; elem < xfldGroup.nElem(); elem++)
  {
    xfldGroup.associativity( elem ).getNodeGlobalMapping( xnodeMap, Topology::NNode );
    ifldGroup.associativity( elem ).getNodeGlobalMapping( inodeMap, Topology::NNode );

    //loop over all edges of the cell
    for (int iedge = 0; iedge < Topology::NEdge; iedge++)
    {
      int xnode0 = xnodeMap[EdgeNodes[iedge][0]];
      int xnode1 = xnodeMap[EdgeNodes[iedge][1]];

      int inode0 = inodeMap[EdgeNodes[iedge][0]];
      int inode1 = inodeMap[EdgeNodes[iedge][1]];

      // sort the nodes based on the native index
      // this guarantees uniqueness across all processors
      if (implied_metric_.local2nativeDOFmap(inode0) > implied_metric_.local2nativeDOFmap(inode1))
      {
        std::swap(xnode0, xnode1);
        std::swap(inode0, inode1);
      }

      // only consider edges where the primary node is possessed
      if (inode0 < implied_metric_.nDOFpossessed())
      {
        // add the edge to the list
        // note that the grid node numbers may not be the same as the fld node numbers
        edges[{inode0,inode1}] = {xnode0, xnode1};
      }
      else if (inode1 < implied_metric_.nDOFpossessed())
      {
        // ghost edge needed to complete gradients
        edgesGhost[{inode0,inode1}] = {xnode0, xnode1};
      }
    }
  }
}

//===========================================================================//
template <class PhysDim, class TopoDim>
void
NodalMetrics<PhysDim, TopoDim>::
computeCost(const MatrixSymFieldType& Sfld,
            Real& Cost, std::vector<MatrixSym>& dCost_dS) const
{
  //Check if the node count is consistent
  SANS_ASSERT( (Sfld.nDOF() == (int)dCost_dS.size()) || (dCost_dS.size() == 0) );
  SANS_ASSERT( Sfld.nDOF() == implied_metric_.nDOF() );

  //TODO: Assumes that all cellgroups have the same no. of DOF per cell (so no mixed meshes)
  // Is a 'Real' because CG has fractional nDOF0
  Real nDOF0 = problem_.getnDOFperCell(cellgroup_list_[0]);
  SANS_ASSERT( nDOF0 > 0 );

  //Test assumption
  for (int i = 0; i < (int) cellgroup_list_.size(); i++)
    SANS_ASSERT( nDOF0 == problem_.getnDOFperCell(cellgroup_list_[i]));

  // Negate to indicate Linf norm for ReferenceElementCost calculation (i.e. right angle cells)
  if (metricNorm_ == Linf) nDOF0 = -nDOF0;

  costModel_.computeCost_S(nDOF0, Sfld, Cost, dCost_dS);
}

//===========================================================================//
template <class PhysDim, class TopoDim>
Real
NodalMetrics<PhysDim, TopoDim>::
computeRequestedElements(const MatrixSymFieldType& Sfld) const
{
  //Check if the node count is consistent
  SANS_ASSERT( Sfld.nDOF() == implied_metric_.nDOF() );

  Real nElem = 0;

  std::vector<MatrixSym> dCost_dS; // Not used

  //Use nDOF0 = 1 to compute the number of elements
  Real nDOF0 = 1;

  costModel_.computeCost_S(nDOF0, Sfld, nElem, dCost_dS);

  return nElem;
}

//===========================================================================//
template <class PhysDim, class TopoDim>
void
NodalMetrics<PhysDim, TopoDim>::
computeComplexity(const MatrixSymFieldType& logMfld,
                  Real& Complexity, std::vector<MatrixSym>& dComplexity_dlogM) const
{
  //Check if the node count is consistent
  SANS_ASSERT( (logMfld.nDOF() == (int)dComplexity_dlogM.size()) || (dComplexity_dlogM.size() == 0) );
  SANS_ASSERT( logMfld.nDOF() == implied_metric_.nDOF() );

  //Setting nDOF0 to zero computes the complexity rather than cost
  Real nDOF0 = 0;

  costModel_.computeCost_logM(nDOF0, logMfld, Complexity, dComplexity_dlogM);
}

//===========================================================================//
template <class PhysDim, class TopoDim>
void
NodalMetrics<PhysDim, TopoDim>::
initializeStepMatrices(const Real h_domain_max, const Real hRefineFactorMax,
                       const Real targetCost, Real& cost_initial_estimate,
                       MatrixSymFieldType& Sfld, std::vector<Real>& h_coarsen_factors) const
{
  /* This function sets bounds for the values of the step matrices at each node.
   * The bounds are computed so that a given edge does not get refined or coarsened by more than
   * a factor of h_refine_factor_max.
   *
   * If coarsening an edge by a factor of h_refine_factor_max causes that edge to be larger
   * than the domain size, then the bounds are computed using a "limited" factor: h_refine_factor_limited.
   */

  SANS_ASSERT_MSG( Sfld.nDOF() == implied_metric_.nDOF(),
                   "Sfld.nDOF() = %d, implied_metric_.nDOF() = %d",
                   Sfld.nDOF(), implied_metric_.nDOF() );
  SANS_ASSERT_MSG( (int)h_coarsen_factors.size() == implied_metric_.nDOF(),
                   "h_coarsen_factors.size() = %d, implied_metric_.nDOF() = %d",
                   h_coarsen_factors.size(), implied_metric_.nDOF() );

  const int nNode = implied_metric_.nDOF();

  //Maximum factor by which the length of an edge is refined
  Real h_refine_factor_max = hRefineFactorMax;

  for (int node = 0; node < nNode; node++)
  {
    Real h_coarsen_factor_limited = h_refine_factor_max; //Refinement factor after imposing max edge length constraints

    //Get initial metric at node
    const MatrixSym& M0 = implied_metric_.DOF(node);

    //Obtain eigenvalues of M0
    SANS::DLA::VectorS<D,Real> L;
    DLA::EigenValues( M0, L );

    for (int d = 0; d < D; d++)
    {
      SANS_ASSERT_MSG( L[d] > 0, "Non-positive eigenvalue for metric tensor!");
      Real h_principal = pow(L[d], -1.0/D); //Edge length in principal direction d

      //If coarsening an edge by a factor of h_refine_factor_max causes it to grow larger than h_domain_max,
      //then limit the refinement factor
      if ( h_principal*h_refine_factor_max > h_domain_max )
        h_coarsen_factor_limited = min(h_coarsen_factor_limited, max(1.01, h_domain_max/h_principal));
    }

    h_coarsen_factors[node] = h_coarsen_factor_limited;
  }

  // initialize the step matrix to zero
  Sfld = 0;

  std::vector<MatrixSym> dCost_dS;

  //Compute an initial cost estimate for current mesh (i.e. Svec = 0)
  cost_initial_estimate = 0.0;
  computeCost(Sfld, cost_initial_estimate, dCost_dS);

  std::cout << "Initial cost estimate = " << std::scientific << std::setprecision(3) << cost_initial_estimate <<std::endl;

  //If the initial cost is larger than the target, then try to initialize x such that we are under the target
  if (cost_initial_estimate > targetCost)
  {
    std::cout << "Current cost estimate (" << cost_initial_estimate << ") is larger than the target cost (" << targetCost << ")." << std::endl;
    std::cout << "Computing a suitable initial value for the nodal step matrices.." << std::endl;

    // Compute diagonal value of step matrix that will reduce initialCost to targetCost_
    Real s_k = 2.0*log(targetCost/cost_initial_estimate) / ((Real) D);

    for (int node = 0; node < nNode; node++)
    {
      for (int d = 0; d < D; d++)
      {
        Real tmp = s_k;

        // use the lower bound if the calculated trace is too small
        if (tmp < -2.0*log(h_coarsen_factors[node]))
          tmp = -2.0*log(h_coarsen_factors[node]);

        Sfld.DOF(node)(d,d) = tmp;
      }
    } //loop over nodes

    Real newCost = 0.0;

    //Compute the new cost estimate
    computeCost(Sfld, newCost, dCost_dS);

    std::cout << "Updated initial cost: " << newCost << std::endl;
  }
}


//===========================================================================//
template <class PhysDim, class TopoDim>
void
NodalMetrics<PhysDim, TopoDim>::
initializeCostStepMatrices(const Real h_domain_max, const Real hRefineFactorMax,
                           Real& targetCost, Real& cost_initial_estimate,
                           MatrixSymFieldType& Sfld, std::vector<Real>& h_coarsen_factors) const
{
  /* This function sets bounds for the values of the step matrices at each node.
   * The bounds are computed so that a given edge does not get refined or coarsened by more than
   * a factor of h_refine_factor_max.
   *
   * If coarsening an edge by a factor of h_refine_factor_max causes that edge to be larger
   * than the domain size, then the bounds are computed using a "limited" factor: h_refine_factor_limited.
   */

  SANS_ASSERT_MSG( Sfld.nDOF() == implied_metric_.nDOF(),
                   "Sfld.nDOF() = %d, implied_metric_.nDOF() = %d",
                   Sfld.nDOF(), implied_metric_.nDOF() );
  SANS_ASSERT_MSG( (int)h_coarsen_factors.size() == implied_metric_.nDOF(),
                   "h_coarsen_factors.size() = %d, implied_metric_.nDOF() = %d",
                   h_coarsen_factors.size(), implied_metric_.nDOF() );

  const int nNode = implied_metric_.nDOF();

  for (int node = 0; node < nNode; node++)
  {
    Real h_coarsen_factor_limited = hRefineFactorMax; //Refinement factor after imposing max edge length constraints

    //Get initial metric at node
    const MatrixSym& M0 = implied_metric_.DOF(node);

    //Obtain eigenvalues of M0
    SANS::DLA::VectorS<D,Real> L;
    DLA::EigenValues( M0, L );

    for (int d = 0; d < D; d++)
    {
      SANS_ASSERT_MSG( L[d] > 0, "Non-positive eigenvalue for metric tensor!");
      Real h_principal = pow(L[d], -1.0/D); //Edge length in principal direction d

      //If coarsening an edge by a factor of h_refine_factor_max causes it to grow larger than h_domain_max,
      //then limit the refinement factor
      if ( h_principal*hRefineFactorMax > h_domain_max )
        h_coarsen_factor_limited = min(h_coarsen_factor_limited, max(1.01, h_domain_max/h_principal));
    }

    h_coarsen_factors[node] = h_coarsen_factor_limited;
  }

  std::vector<MatrixSym> dCost_dS;

  // initialize the step matrix to zero
  Sfld = 0;

  //Compute an initial cost estimate for current mesh (i.e. Svec = 0)
  cost_initial_estimate = 0.0;
  computeCost(Sfld, cost_initial_estimate, dCost_dS);

  for (int node = 0; node < nNode; node++)
  {
    // Compute diagonal value of step matrix that give the maximum cost
    Real s_k = 2.0*log(hRefineFactorMax) / ((Real) D);

    for (int d = 0; d < D; d++)
      Sfld.DOF(node)(d,d) = s_k;
  }

  //Compute the maximum cost estimate
  Real maxCost = 0.0;
  computeCost(Sfld, maxCost, dCost_dS);

  for (int node = 0; node < nNode; node++)
  {
    // Compute diagonal value of step matrix that give the minimum cost
    Real s_k = -2.0*log(h_coarsen_factors[node]) / ((Real) D);

    for (int d = 0; d < D; d++)
      Sfld.DOF(node)(d,d) = s_k;
  }

  //Compute the minimum cost estimate
  Real minCost = 0.0;
  computeCost(Sfld, minCost, dCost_dS);

  Real fracMax = DOFIncreaseFactor_;
  //Real fracMin = 0.9;

  // save off the target cost request
  Real targetCostReq = targetCost;
  Real dmaxCost = maxCost - cost_initial_estimate;
  maxCost = fracMax*dmaxCost + cost_initial_estimate;
  if ( targetCost > maxCost )
    targetCost = maxCost;

  // If the target cost is below the range, limit it to close to the minimum
  //Real dminCost = minCost - cost_initial_estimate;
  //minCost = fracMin*dminCost + cost_initial_estimate;
  //if ( targetCost < minCost )
  //  targetCost = minCost;

  Sfld = 0;

  if (targetCost < cost_initial_estimate)
  {
    // Compute diagonal value of step matrix that will adjust initialCost to targetCost
    Real s_k = 2.0*log(targetCost/cost_initial_estimate) / ((Real) D);

    for (int node = 0; node < nNode; node++)
    {
      for (int d = 0; d < D; d++)
      {
        Real tmp = s_k;

        // use the lower bound if the calculated trace is too small
        if (tmp < -2.0*log(h_coarsen_factors[node]) / ((Real) D))
          tmp = -2.0*log(h_coarsen_factors[node]) / ((Real) D);

        Sfld.DOF(node)(d,d) = tmp;
      }
    } //loop over nodes
  }

  //Compute the new cost estimate
  Real newCost = 0.0;
  computeCost(Sfld, newCost, dCost_dS);

  if (xfld_linear_.comm()->rank() == 0)
  {
    std::cout << std::endl;
    std::cout << "------------------------------------------" << std::endl;
    std::cout << "Initial cost estimate : " << std::scientific << std::setprecision(3) << cost_initial_estimate <<std::endl;
    std::cout << "Available cost range  : " << minCost << " - " << maxCost << std::endl;
    std::cout << "Requested target cost : " << targetCostReq << std::endl;
    std::cout << "Adjusted target cost  : " << targetCost << std::endl;
    std::cout << "Updated initial cost  : " << newCost << std::endl;
    std::cout << "------------------------------------------" << std::endl;
    std::cout << std::endl;
  }
}

//===========================================================================//
template <class PhysDim, class TopoDim>
template <class T>
void
NodalMetrics<PhysDim, TopoDim>::
edgeLengthDeviation(const QuadratureLine& quadrature,
                    const Element<DLA::MatrixSymS<D, T>, TopoD1, Line>& mfldElem,
                    const VectorX& eij,
                    T& delUnity)
{
  T edgeLen = 0;

  // compute the edge length
  edgeLength(quadrature,mfldElem,eij,edgeLen);

  // Compute how far the edge length is out of bounds 1/sqrt(2) and sqrt(2)
  // del == 0 if the edge length is within the bounds
  Real bound = TopoDim::D == 1 ? 1. : sqrt(2.);

  T del;
  if ( edgeLen > 1 )
    del = edgeLen - bound;
  else
    del = 1/bound - edgeLen;

  // Raise the deviation to the power of the topological dimension so
  // the units are consistent with complexity (area of the Riemannian surface)
  //static const int D = TopoDim::D;
  if ( TopoDim::D == 1)
    delUnity = pow(del,2);
  else
    delUnity = pow( smoothmaxC2(T(0.0), del, 1e-2), 2 );
}


//===========================================================================//
template <class PhysDim, class TopoDim>
template <class T>
void
NodalMetrics<PhysDim, TopoDim>::
edgeLength( const QuadratureLine& quadrature,
            const Element<DLA::MatrixSymS<D, T>, TopoD1, Line>& mfldElem,
            const VectorX& eij,
            T& edgeLength)
{
#ifdef LOGEUCLIDEAN_1D
  // Edge length calculation from
  //
  // Loseille Lohner IMR 18 (2009) pg 613
  // Alauzet Finite Elements in Analysis and Design 46 (2010) pg 185
  // Alauzet_2010_Size_gradation_control_of_anisotropic_meshes.pdf
  //
  // This is an analytic integral of log-Euclidean interpolation

  // Edge length based on the metric at each node
  // SurrealClass lni = Transpose(eij)*exp(mfldElem.DOF(0))*eij;
  // SurrealClass lnj = Transpose(eij)*exp(mfldElem.DOF(1))*eij;

  T lni = DLA::InnerProduct(eij,exp(mfldElem.DOF(0)));
  T lnj = DLA::InnerProduct(eij,exp(mfldElem.DOF(1)));

  lni = sqrt(lni);
  lnj = sqrt(lnj);

  if ( fabs(lni-lnj) <= 1.0e-6*lni )
    edgeLength = lni;
  else
    edgeLength = (lni-lnj)/(log(lni/lnj));
#else
  // Use 3rd order quadrature consistent with EPIC
  const int nquad = quadrature.nQuadrature();
  Real weight;       // quadrature weight

  DLA::MatrixSymS<D, T> logM;
  for (int iquad = 0; iquad < nquad; iquad++)
  {
    quadrature.weight( iquad, weight );
    QuadraturePoint<TopoD1> sRef = quadrature.coordinates_cache( iquad );

    mfldElem.eval( sRef, logM );

    // T h2 = Transpose(eij)*exp(logM)*eij;
    T h2 = DLA::InnerProduct(eij,exp(logM));
    edgeLength += weight*sqrt(h2);
  }
#endif
}

//===========================================================================//
template <class PhysDim, class TopoDim>
void
NodalMetrics<PhysDim, TopoDim>::
computeEdgeLengthDeviation(const MatrixSymFieldType& logMfld,
                           Real& EdgeLenDev, std::vector<MatrixSym>& dEdgeLenDev_dlogM)
{
  SANS_ASSERT(logMfld.nDOF() == implied_metric_.nDOF());

  // Use 3rd order quadrature consistent with EPIC
  Quadrature<TopoD1, Line> quadrature(3);

  if (dEdgeLenDev_dlogM.size() == 0)
  {
    typedef Element<MatrixSym, TopoD1, Line> ElementMFieldClass;

    ElementMFieldClass mfldElem( 1, BasisFunctionCategory_Lagrange );

    Real delUnity;

    // Loop over the edge and compute the edge length deviation
    for (const Edge& edge : edges_)
    {
      // Get the two nodes in the logM field
      const int node_i = edge.ifld.first;
      const int node_j = edge.ifld.second;

      // Extract the nodal metrics
      const MatrixSym& logMi = logMfld.DOF(node_i);
      const MatrixSym& logMj = logMfld.DOF(node_j);

      // Set the element DOF
      mfldElem.DOF(0) = logMi;
      mfldElem.DOF(1) = logMj;

      // Compute the length of the edge (this assumes a linear mesh!)
      VectorX eij = xfld_linear_.DOF(edge.xfld.second) - xfld_linear_.DOF(edge.xfld.first);

      // compute the edge length deviation
      edgeLengthDeviation(quadrature, mfldElem, eij, delUnity);

      EdgeLenDev += delUnity;
    } // loop over edges
  }
  else
  {
    SANS_ASSERT(logMfld.nDOF() == (int)dEdgeLenDev_dlogM.size());

    typedef SurrealS<MatrixSym::SIZE*2> SurrealClass;
    typedef DLA::MatrixSymS<D, SurrealClass> MatrixSymSurreal;
    typedef Element<MatrixSymSurreal, TopoD1, Line> ElementMFieldClass;

    ElementMFieldClass mfldElem( 1, BasisFunctionCategory_Lagrange );

    SurrealClass delUnity;

    // reference element DOFs to reduce copying
    MatrixSymSurreal& logMi = mfldElem.DOF(0);
    MatrixSymSurreal& logMj = mfldElem.DOF(1);

    // initialize the derivatives
    logMi = 0;
    logMj = 0;
    for (int j = 0; j < MatrixSym::SIZE; j++)
    {
      logMi.value(j).deriv(                j) = 1.0;
      logMj.value(j).deriv(MatrixSym::SIZE+j) = 1.0;
    }

    // Loop over the nodes and compute the edge length deviation
    const int nEdges = 1;//edges_.size(); // pcaplan: set nEdges = 1 to recover marshall's old method
    for (const Edge& edge : edges_)
    {
      // Get the two nodes
      const int node_i = edge.ifld.first;
      const int node_j = edge.ifld.second;

      // Extract the nodal metrics and setup the automatic differentiation
      const MatrixSym& logMvi = logMfld.DOF(node_i);
      const MatrixSym& logMvj = logMfld.DOF(node_j);

      // set the values in the element DOF
      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      {
        logMi.value(ind).value() = logMvi.value(ind);
        logMj.value(ind).value() = logMvj.value(ind);
      }

      // Compute the length of the edge (this assumes a linear mesh!)
      VectorX eij = xfld_linear_.DOF(edge.xfld.second) - xfld_linear_.DOF(edge.xfld.first);

      // compute the edge length deviation
      edgeLengthDeviation(quadrature, mfldElem, eij, delUnity);

      EdgeLenDev += delUnity.value()/nEdges;

      for (int j = 0; j < MatrixSym::SIZE; j++)
      {
        dEdgeLenDev_dlogM[node_i].value(j) += delUnity.deriv(                j)/nEdges;
        dEdgeLenDev_dlogM[node_j].value(j) += delUnity.deriv(MatrixSym::SIZE+j)/nEdges;
      }
    }

    // complete the gradients from ghost edges
    for (const Edge& edge : edgesGhost_)
    {
      // Get the two nodes
      const int node_i = edge.ifld.first;
      const int node_j = edge.ifld.second;

      // Extract the nodal metrics and setup the automatic differentiation
      const MatrixSym& logMvi = logMfld.DOF(node_i);
      const MatrixSym& logMvj = logMfld.DOF(node_j);

      // set the values in the element DOF
      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      {
        logMi.value(ind).value() = logMvi.value(ind);
        logMj.value(ind).value() = logMvj.value(ind);
      }

      // Compute the length of the edge (this assumes a linear mesh!)
      VectorX eij = xfld_linear_.DOF(edge.xfld.second) - xfld_linear_.DOF(edge.xfld.first);

      // compute the edge length deviation
      edgeLengthDeviation(quadrature, mfldElem, eij, delUnity);

      for (int j = 0; j < MatrixSym::SIZE; j++)
      {
        dEdgeLenDev_dlogM[node_i].value(j) += delUnity.deriv(                j);
        dEdgeLenDev_dlogM[node_j].value(j) += delUnity.deriv(MatrixSym::SIZE+j);
      }
    } // loop over edges
  }

  // sum across processors
#ifdef SANS_MPI
  EdgeLenDev = boost::mpi::all_reduce( *xfld_linear_.comm(), EdgeLenDev, std::plus<Real>() );
#endif
}


//===========================================================================//
template <class PhysDim, class TopoDim>
void
NodalMetrics<PhysDim, TopoDim>::
computeQualityAverage( const MatrixSymFieldType& logMfld,
                       Real& QAvg, std::vector<MatrixSym>& dQAvg_dlogM,
                       const bool inverted )
{
  // inverted is whether to compute 1/N sum_k 1/Q_k or 1/N sum_k Q_k

  // The entire adaptation folder is littered with the simplex assumption.
  // Putting in the for_each_CellGroup style here is a waste of time
  typedef typename Simplex<TopoDim>::type Topology;

  typedef typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename MatrixSymFieldType::template FieldCellGroupType<Topology> IFieldCellGroupType;
  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

  SANS_ASSERT(logMfld.nDOF() == implied_metric_.nDOF());

  const int (*EdgeNodes)[Line::NNode] = ElementEdges<Topology>::EdgeNodes;

  // typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;
  // typedef DLA::VectorS<PhysDim::D,Real> VectorX;

  const bool computingDerivatives = (dQAvg_dlogM.size() != 0);

  std::array<int,Topology::NNode> xCellNodes, iCellNodes;

  // normalization factor
  Real vRef = 1./ReferenceElementCost<Topology>::getCost(1); // reciprocal of 1 element cost = volume of unit elem
  vRef = std::pow( vRef , 2./TopoDim::D );
  Real c_d = vRef/Topology::NEdge;

  int nElem = 0;
  // calculate the total number of elements up front
  for (int group = 0; group < xfld_linear_.nCellGroups(); group++)
  {
    nElem += xfld_linear_.getCellGroupBaseGlobal(group).nElem();
  }

  if ( !computingDerivatives )
  {
    for (int group = 0; group < xfld_linear_.nCellGroups(); group++)
    {
      const XFieldCellGroupType& xfldGroup = xfld_linear_.template getCellGroup<Topology>(group);
      const IFieldCellGroupType& ifldGroup = logMfld.template getCellGroup<Topology>(group);

      ElementXFieldClass xfldElem( xfldGroup.basis() );

      // loop over elements in cell groups
      for (int elem = 0; elem < xfldGroup.nElem(); elem++)
      {
        // skip elements not on processor
        if ( xfldGroup.associativity(elem).rank() != xfld_linear_.comm()->rank() ) continue;

        // get the global mapping of this element
        xfldGroup.getElement( xfldElem , elem );
        xfldGroup.associativity(elem).getNodeGlobalMapping(xCellNodes.data(),Topology::NNode);

        // get the ifield nodes
        ifldGroup.associativity(elem).getNodeGlobalMapping(iCellNodes.data(),Topology::NNode);

        Real vol = xfldElem.jacobianDeterminant();
        MatrixSym Melem = 0;

#if defined(METRIC_MAX)
        // Use the maximum of the attached metrics
        // get the vertex metrics
        int imax = -1;
        Real maxdet = -1;
        for (int i=0;i<Topology::NNode;i++)
        {
          const MatrixSym metric_i = exp(logMfld.DOF(iCellNodes[i]));
          if (DLA::Det(metric_i)>maxdet)
          {
            imax = i;
            maxdet = DLA::Det(metric_i);
          }
        } // loop over nodes

        Melem = exp(logMfld.DOF(iCellNodes[imax]));
#else
        // Use the log-euclidean mean

        for (int i = 0; i < Topology::NNode; i++)
          Melem += logMfld.DOF(iCellNodes[i]);

        Melem /= Topology::NNode;
        Melem = exp(Melem);
#endif
        Real Mdet = DLA::Det(Melem);
        vol *= std::sqrt(Mdet);

        vol = std::pow( fabs(vol) , 2./TopoDim::D );

        // compute all the edge lengths under this metric
        Real sumSquareEdgeLengths = 0.0;
        for (int edge=0;edge<Topology::NEdge;edge++)
        {
          int node_i = xCellNodes[ EdgeNodes[edge][0] ];
          int node_j = xCellNodes[ EdgeNodes[edge][1] ];

          // physical vector from node_i to node_j
          VectorX eij = xfld_linear_.DOF(node_j) - xfld_linear_.DOF(node_i);

          // add the contribution to the denominator of the quality
          // sumSquareEdgeLengths += Transpose(eij)*Melem*eij;
          sumSquareEdgeLengths += DLA::InnerProduct(eij,Melem);
        } // loop over edges

        // add to the accumulation
        if (inverted)
          QAvg += (c_d*sumSquareEdgeLengths)/vol;
        else
          QAvg += 1 - vol/(c_d*sumSquareEdgeLengths);

      } // loop over elements
    } // loop over cell groups
    QAvg /= nElem;
  } // not computing derivatives
  else
  {
    SANS_ASSERT(logMfld.nDOF() == (int)dQAvg_dlogM.size());

#if defined(METRIC_MAX)
    const int SurrealSize = MatrixSym::SIZE;
#else
    const int SurrealSize = MatrixSym::SIZE * Topology::NNode;
#endif

    typedef SurrealS<SurrealSize> SurrealClass;
    typedef DLA::MatrixSymS<D, SurrealClass> MatrixSymSurreal;

    SurrealClass Q = 0;

#if defined(METRIC_MAX)
    // Only taking the max rather than averages. So can use one surreal set
    MatrixSymSurreal logM = 0;
    for (int ind = 0; ind < SurrealSize; ind++)
      logM.value(ind).deriv(ind) = 1.0;
#else
    MatrixSymSurreal logM_nodes[Topology::NNode];
    for (int n = 0; n < Topology::NNode; n++)
    {
      logM_nodes[n] = 0; // initialize to zero

      //Set up Surreal matrices so that all derivatives can be computed in one shot
      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
        logM_nodes[n].value(ind).deriv(n*MatrixSym::SIZE + ind) = 1.0;
    }
#endif

    for (int group = 0; group < xfld_linear_.nCellGroups(); group++)
    {
      const XFieldCellGroupType& xfldGroup = xfld_linear_.template getCellGroup<Topology>(group);
      const IFieldCellGroupType& ifldGroup = logMfld.template getCellGroup<Topology>(group);

      ElementXFieldClass xfldElem( xfldGroup.basis() );

      // loop over elements in cell groups
      for (int elem = 0; elem < xfldGroup.nElem(); elem++)
      {
        // skip elements not on processor
        if ( xfldGroup.associativity(elem).rank() != xfld_linear_.comm()->rank() ) continue;

        // get the global mapping of this element
        xfldGroup.getElement( xfldElem , elem );
        xfldGroup.associativity(elem).getNodeGlobalMapping(xCellNodes.data(),Topology::NNode);

        // get the ifield nodes
        ifldGroup.associativity(elem).getNodeGlobalMapping(iCellNodes.data(),Topology::NNode);

        MatrixSymSurreal Melem = 0.0; // will be zeroed once per element
#if defined(METRIC_MAX)
        // get the vertex metrics
        int imax = -1;
        { // scoped so that maxdet can be surreal later
          Real maxdet = -1;
          for (int i=0;i<Topology::NNode;i++)
          {
            const MatrixSym metric_i = exp(logMfld.DOF(iCellNodes[i]));
            if (DLA::Det(metric_i)>maxdet)
            {
              imax = i;
              maxdet = DLA::Det(metric_i);
            }
          } // loop over nodes
        }
        // have calculate which has the largest determinant, set the value
        for (int ind = 0; ind < MatrixSym::SIZE; ind++)
          logM.value(ind).value() = logMfld.DOF(iCellNodes[imax]).value(ind);

        Melem = exp(logM);
#else
        // Take the log-euclidean mean of the attached nodes
        // exp ( 1/N sum_n log M )
        for (int n = 0; n < Topology::NNode; n++)
        {
          const int node = iCellNodes[n];
          // load in the values
          for (int ind = 0; ind < MatrixSym::SIZE; ind++)
            logM_nodes[n].value(ind).value() = logMfld.DOF(node).value(ind);

          // add in to running sum
          Melem += logM_nodes[n];
        }
        Melem /= Topology::NNode; // arithmetic mean

        Melem = exp(Melem);
#endif

        // compute the determinant from the surreal version
        SurrealClass Mdet = DLA::Det(Melem);

        SurrealClass vol = xfldElem.jacobianDeterminant();
        vol *= sqrt(Mdet);
        vol = pow(vol, 2./TopoDim::D );

        SurrealClass sumSquareEdgeLengths = 0.0;
        for (int edge = 0; edge < Topology::NEdge; edge++)
        {
          int node_i = xCellNodes[ EdgeNodes[edge][0] ];
          int node_j = xCellNodes[ EdgeNodes[edge][1] ];

          // physical vector from node_i to node_j
          VectorX eij = xfld_linear_.DOF(node_j) - xfld_linear_.DOF(node_i);

          sumSquareEdgeLengths += DLA::InnerProduct(eij,Melem);
        } // loop over edges

        // add to the accumulation
        if (inverted)
          Q = (c_d*sumSquareEdgeLengths)/vol;
        else
          Q = 1 - vol/(c_d*sumSquareEdgeLengths);


        QAvg += Q.value()/nElem;

        // NOTE: The derivatives have the division by nElem done as they are placed in the vector

#if defined(METRIC_MAX)
        // dispatch the derivatives
        for (int ind = 0; ind < SurrealSize; ind++)
          dQAvg_dlogM[iCellNodes[imax]].value(ind) += Q.deriv(ind)/nElem;
#else
        // dispatch the derivatives
        for (int n = 0; n < Topology::NNode; n++)
        {
          const int node = iCellNodes[n];
          // unload the derivatives from the correct Surreal slot
          for (int ind = 0; ind < MatrixSym::SIZE; ind++)
            dQAvg_dlogM[node].value(ind) += Q.deriv(n*MatrixSym::SIZE + ind)/nElem;
        }
#endif
      } // loop over elems
    } // loop over groups
  } // computing derivatives

  // sum across processors
#ifdef SANS_MPI
  QAvg = boost::mpi::all_reduce( *xfld_linear_.comm(), QAvg, std::plus<Real>() );
#endif
}


//===========================================================================//
template <class PhysDim, class TopoDim>
void
NodalMetrics<PhysDim, TopoDim>::
computeQualityStatistics( const MatrixSymFieldType& Mfld,
                          Real& Qmean, Real& Qmin, Real& Qmax )
{
  // Compute the qualities as in line with the UGAWG methodology

  // initialize the qualities
  Qmean = 0.0; Qmin = 1.0; Qmax = 0.0;

  // The entire adaptation folder is littered with the simplex assumption.
  // Putting in the for_each_CellGroup style here is a waste of time
  typedef typename Simplex<TopoDim>::type Topology;

  typedef typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename MatrixSymFieldType::template FieldCellGroupType<Topology> IFieldCellGroupType;
  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

  SANS_ASSERT(Mfld.nDOF() == implied_metric_.nDOF());

  const int (*EdgeNodes)[Line::NNode] = ElementEdges<Topology>::EdgeNodes;

  std::array<int,Topology::NNode> xCellNodes, iCellNodes;

  // normalization factor
  Real vRef = 1./ReferenceElementCost<Topology>::getCost(1); // reciprocal of 1 element cost = volume of unit elem
  vRef = std::pow( vRef , 2./TopoDim::D );
  Real c_d = vRef/Topology::NEdge;

  int nElem = 0;

  for (int group = 0; group < xfld_linear_.nCellGroups(); group++)
  {
    const XFieldCellGroupType& xfldGroup = xfld_linear_.template getCellGroup<Topology>(group);
    const IFieldCellGroupType& ifldGroup = Mfld.template getCellGroup<Topology>(group);

    ElementXFieldClass xfldElem( xfldGroup.basis() );

    // loop over elements in cell groups
    for (int elem = 0; elem < xfldGroup.nElem(); elem++)
    {
      // skip elements not on processor
      if ( xfldGroup.associativity(elem).rank() != xfld_linear_.comm()->rank() ) continue;

      nElem++; // increment the element count

      // get the global mapping of this element
      xfldGroup.getElement( xfldElem , elem );
      xfldGroup.associativity(elem).getNodeGlobalMapping(xCellNodes.data(),Topology::NNode);

      // get the ifield nodes
      ifldGroup.associativity(elem).getNodeGlobalMapping(iCellNodes.data(),Topology::NNode);

      Real vol = xfldElem.jacobianDeterminant();
      // Use the maximum of the attached metrics
      // get the vertex metrics
      int imax = -1;
      Real maxdet = -1;
      for (int i=0;i<Topology::NNode;i++)
      {
        Real det = DLA::Det(Mfld.DOF(iCellNodes[i]));
        if (det>maxdet)
        {
          imax = i;
          maxdet = det;
        }
      } // loop over nodes

      const MatrixSym& Melem = Mfld.DOF(iCellNodes[imax]);

      vol *= std::sqrt(maxdet);

      vol = std::pow( fabs(vol) , 2./TopoDim::D );

      // compute all the edge lengths under this metric
      Real sumSquareEdgeLengths = 0.0;
      for (int edge=0;edge<Topology::NEdge;edge++)
      {
        int node_i = xCellNodes[ EdgeNodes[edge][0] ];
        int node_j = xCellNodes[ EdgeNodes[edge][1] ];

        // physical vector from node_i to node_j
        VectorX eij = xfld_linear_.DOF(node_j) - xfld_linear_.DOF(node_i);

        // add the contribution to the denominator of the quality
        sumSquareEdgeLengths += DLA::InnerProduct(eij,Melem);
      } // loop over edges

      // add to the accumulation of qualities
      Qmean += vol/(c_d*sumSquareEdgeLengths);

      Qmin = min(Qmin,vol/(c_d*sumSquareEdgeLengths));
      Qmax = max(Qmax,vol/(c_d*sumSquareEdgeLengths));
    } // loop over elements
  } // loop over cell groups

  // sum across processors
#ifdef SANS_MPI
  nElem = boost::mpi::all_reduce( *xfld_linear_.comm(), nElem, std::plus<int>() );
  Qmean = boost::mpi::all_reduce( *xfld_linear_.comm(), Qmean, std::plus<Real>() );
  Qmin  = boost::mpi::all_reduce( *xfld_linear_.comm(), Qmin, boost::mpi::minimum<Real>() );
  Qmax  = boost::mpi::all_reduce( *xfld_linear_.comm(), Qmax, boost::mpi::maximum<Real>() );
#endif

  if (nElem > 0) Qmean /= nElem;
}



//===========================================================================//
template <class PhysDim, class TopoDim>
void
NodalMetrics<PhysDim, TopoDim>::
computeQualityStatistics_logM( const MatrixSymFieldType& logMfld,
                               Real& Qmean, Real& Qmin, Real& Qmax )
{
  // initialize the qualities
  Qmean = 0.0; Qmin = 1.0; Qmax = 0.0;

  // The entire adaptation folder is littered with the simplex assumption.
  // Putting in the for_each_CellGroup style here is a waste of time
  typedef typename Simplex<TopoDim>::type Topology;

  typedef typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename MatrixSymFieldType::template FieldCellGroupType<Topology> IFieldCellGroupType;
  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

  SANS_ASSERT(logMfld.nDOF() == implied_metric_.nDOF());

  const int (*EdgeNodes)[Line::NNode] = ElementEdges<Topology>::EdgeNodes;

  std::array<int,Topology::NNode> xCellNodes, iCellNodes;

  // normalization factor
  Real vRef = 1./ReferenceElementCost<Topology>::getCost(1); // reciprocal of 1 element cost = volume of unit elem
  vRef = std::pow( vRef , 2./TopoDim::D );
  Real c_d = vRef/Topology::NEdge;

  int nElem = 0;

  for (int group = 0; group < xfld_linear_.nCellGroups(); group++)
  {
    const XFieldCellGroupType& xfldGroup = xfld_linear_.template getCellGroup<Topology>(group);
    const IFieldCellGroupType& ifldGroup = logMfld.template getCellGroup<Topology>(group);

    ElementXFieldClass xfldElem( xfldGroup.basis() );

    // loop over elements in cell groups
    for (int elem = 0; elem < xfldGroup.nElem(); elem++)
    {
      // skip elements not on processor
      if ( xfldGroup.associativity(elem).rank() != xfld_linear_.comm()->rank() ) continue;

      nElem++; // increment the element count

      // get the global mapping of this element
      xfldGroup.getElement( xfldElem , elem );
      xfldGroup.associativity(elem).getNodeGlobalMapping(xCellNodes.data(),Topology::NNode);

      // get the ifield nodes
      ifldGroup.associativity(elem).getNodeGlobalMapping(iCellNodes.data(),Topology::NNode);

      Real vol = xfldElem.jacobianDeterminant();
      MatrixSym Melem = 0;
#if defined(METRIC_MAX)
      // Use the maximum of the attached metrics
      // get the vertex metrics
      int imax = -1;
      Real maxdet = -1;
      for (int i=0;i<Topology::NNode;i++)
      {
        const MatrixSym metric_i = exp(logMfld.DOF(iCellNodes[i]));
        if (DLA::Det(metric_i)>maxdet)
        {
          imax = i;
          maxdet = DLA::Det(metric_i);
        }
      } // loop over nodes

      Melem = exp(logMfld.DOF(iCellNodes[imax]));
#else
      // Use the log-euclidean mean

      for (int i = 0; i < Topology::NNode; i++)
        Melem += logMfld.DOF(iCellNodes[i]);

      Melem /= Topology::NNode;
      Melem = exp(Melem);
#endif
      Real Mdet = DLA::Det(Melem);
      vol *= std::sqrt(Mdet);

      vol = std::pow( fabs(vol) , 2./TopoDim::D );

      // compute all the edge lengths under this metric
      Real sumSquareEdgeLengths = 0.0;
      for (int edge=0;edge<Topology::NEdge;edge++)
      {
        int node_i = xCellNodes[ EdgeNodes[edge][0] ];
        int node_j = xCellNodes[ EdgeNodes[edge][1] ];

        // physical vector from node_i to node_j
        VectorX eij = xfld_linear_.DOF(node_j) - xfld_linear_.DOF(node_i);

        // add the contribution to the denominator of the quality
        // sumSquareEdgeLengths += Transpose(eij)*Melem*eij;
        sumSquareEdgeLengths += DLA::InnerProduct(eij,Melem);
      } // loop over edges

      // add to the accumulation of qualities
      Qmean += vol/(c_d*sumSquareEdgeLengths);

      Qmin = min(Qmin,vol/(c_d*sumSquareEdgeLengths));
      Qmax = max(Qmax,vol/(c_d*sumSquareEdgeLengths));
    } // loop over elements
  } // loop over cell groups

  // sum across processors
#ifdef SANS_MPI
  nElem = boost::mpi::all_reduce( *xfld_linear_.comm(), nElem, std::plus<int>() );
  Qmean = boost::mpi::all_reduce( *xfld_linear_.comm(), Qmean, std::plus<Real>() );
  Qmin  = boost::mpi::all_reduce( *xfld_linear_.comm(), Qmin, boost::mpi::minimum<Real>() );
  Qmax  = boost::mpi::all_reduce( *xfld_linear_.comm(), Qmax, boost::mpi::maximum<Real>() );
#endif

  if (nElem > 0) Qmean /= nElem;
}



#if 0
//===========================================================================//
template <class PhysDim, class TopoDim>
void
NodalMetrics<PhysDim, TopoDim>::computeNodalError0()
{
  const int nNode = implied_metric_.nDOF();

  nodalError0_.resize(nNode, 0.0);

  Field_NodalView metric_nodalview(implied_metric_, cellgroup_list_);

  for (int node = 0; node < nNode; node++)
  {
    //Get the list of cells around this node (each returned pair contains cell_group and cell_elem)
    Field_NodalView::IndexVector cell_list = metric_nodalview.getCellList(node);

    for (std::size_t i = 0; i < cell_list.size(); i++)
    {
      int cellGroupGlobal = cell_list[i].group;
      int elem = cell_list[i].elem;

      //Get error estimate on original element
      Real error0 = problem_.getElementalErrorEstimate(cellGroupGlobal, elem);
      error0 = fabs(error0);

      nodalError0_[node] = max(nodalError0_[node], error0);
    }
  } //loop over nodes
}
#endif

//===========================================================================//
template <class PhysDim, class TopoDim>
template <class T>
void
NodalMetrics<PhysDim, TopoDim>::
gradationPenalty(const DLA::MatrixSymS<D,T> M[], const VectorX& eij, const Real beta, T& node_penalty)
{
  typedef DLA::MatrixSymS<D,T> MatrixSym;

  Real dij = sqrt(dot(eij,eij));

  int i = 0, j = 1;

  // visit the edge in both directions
  for (int side : {1, -1})
  {
    if (side == -1)
    {
      i = 1;
      j = 0;
    }

//#define ISOTROPIC_GRADATION
#ifdef ISOTROPIC_GRADATION
    // SurrealClass dij2 = Transpose(eij)*Mi*eij;
    T dij2 = DLA::InnerProduct(eij,M[i]);

    // Isotropic growth along eij
    T eta = pow( 1 + sqrt(dij2)*log(beta), -2.0 );

    // Scaled metric growing along eij to node j
    MatrixSym etaMi = eta*M[i];
#else
    //Compute the eigensystem with normalized eigenvectors
    DLA::EigenSystemPair<D,T> etaMi(M[i]);

    // Anisotropic growth along each characteristic direction of the metric
    for (int d = 0; d < D; d++ )
      etaMi.L[d] *= pow( 1 + sqrt(etaMi.L[d])*dij*log(beta), -2.0 );
#endif

    // Compute the intersection between the scaled metric and metric j
    MatrixSym MI = Metric::smoothintersection(etaMi, M[j], 20);

    // If etaMi is larger than Mj, then MI == Mj and dMIj is zero
    MatrixSym dMIj = (MI - M[j]);

    // Normalize by Mj to remove dimensionality
    //node_penalty += DLA::FrobNormSq(dMIj)/DLA::FrobNormSq(M[j]);
    node_penalty += DLA::InnerProduct(eij,dMIj);
  }
}

//===========================================================================//
template <class PhysDim, class TopoDim>
void
NodalMetrics<PhysDim, TopoDim>::
computeGradationPenalty_S(const MatrixSymFieldType& Sfld,
                          Real& penalty, std::vector<MatrixSym>& dpenalty_dS)
{
  const int SurrealSize = MatrixSym::SIZE*2;
  typedef SurrealS<SurrealSize> SurrealClass;
  typedef DLA::MatrixSymS<D,SurrealClass> MatrixSymSurreal;

  Real beta = paramsDict_.get(MOESSParams::params.GradationFactor);

  MatrixSymSurreal M[2];

  MatrixSymSurreal Si = 0;
  MatrixSymSurreal Sj = 0;

  // setup automatic differentiaiton once, and then only extract values
  for (int ind = 0; ind < MatrixSym::SIZE; ind++)
  {
    Si.value(ind).deriv(                ind) = 1.0;
    Sj.value(ind).deriv(MatrixSym::SIZE+ind) = 1.0;
  }

  for (const Edge& edge : edges_)
  {
    // Get the nodes for the edge
    int node_s_i = edge.ifld.first;
    int node_s_j = edge.ifld.second;

    int node_x_i = edge.xfld.first;
    int node_x_j = edge.xfld.second;

    // Extract the step matrix and set automatic differentiation
    const MatrixSym& Siv = Sfld.DOF(node_s_i);
    const MatrixSym& Sjv = Sfld.DOF(node_s_j);

    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
    {
      Si.value(ind).value() = Siv.value(ind);
      Sj.value(ind).value() = Sjv.value(ind);
    }

    const MatrixSym& sqrt_Mni = nodalMetricSqrt_[node_s_i];
    const MatrixSym& sqrt_Mnj = nodalMetricSqrt_[node_s_j];

    // Compute the metric at the two nodes
    M[0] = sqrt_Mni*exp(Si)*sqrt_Mni;
    M[1] = sqrt_Mnj*exp(Sj)*sqrt_Mnj;

    VectorX eij = xfld_linear_.DOF(node_x_j) - xfld_linear_.DOF(node_x_i);

    SurrealClass node_penalty = 0;
    gradationPenalty(M, eij, beta, node_penalty);

    penalty += node_penalty.value();

    if (!dpenalty_dS.empty())
      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      {
        dpenalty_dS[node_s_i].value(ind) += node_penalty.deriv(                ind);
        dpenalty_dS[node_s_j].value(ind) += node_penalty.deriv(MatrixSym::SIZE+ind);
      }

  } // loop over edges

  // sum across processors
#ifdef SANS_MPI
  SANS_ASSERT(xfld_linear_.comm()->size() == 1);
  //penalty = boost::mpi::all_reduce( *xfld_linear_.comm(), penalty, std::plus<Real>() );
#endif
}


//===========================================================================//
template <class PhysDim, class TopoDim>
void
NodalMetrics<PhysDim, TopoDim>::
computeGradationPenalty_logM(const MatrixSymFieldType& logMfld,
                             Real& penalty, std::vector<MatrixSym>& dpenalty_dlogM)
{
  const int SurrealSize = MatrixSym::SIZE*2;
  typedef SurrealS<SurrealSize> SurrealClass;
  typedef DLA::MatrixSymS<D,SurrealClass> MatrixSymSurreal;

  Real beta = paramsDict_.get(MOESSParams::params.GradationFactor);

  MatrixSymSurreal M[2];

  MatrixSymSurreal logMi = 0;
  MatrixSymSurreal logMj = 0;

  // setup automatic differentiaiton once, and then only extract values
  for (int ind = 0; ind < MatrixSym::SIZE; ind++)
  {
    logMi.value(ind).deriv(                ind) = 1.0;
    logMj.value(ind).deriv(MatrixSym::SIZE+ind) = 1.0;
  }

  for (const Edge& edge : edges_)
  {
    // Get the nodes for the edge
    int node_m_i = edge.ifld.first;
    int node_m_j = edge.ifld.second;

    int node_x_i = edge.xfld.first;
    int node_x_j = edge.xfld.second;

    // Extract the step matrix and set automatic differentiation
    const MatrixSym& logMiv = logMfld.DOF(node_m_i);
    const MatrixSym& logMjv = logMfld.DOF(node_m_j);

    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
    {
      logMi.value(ind).value() = logMiv.value(ind);
      logMj.value(ind).value() = logMjv.value(ind);
    }

    // Compute the metric at the two nodes
    M[0] = exp(logMi);
    M[1] = exp(logMj);

    VectorX eij = xfld_linear_.DOF(node_x_j) - xfld_linear_.DOF(node_x_i);

    SurrealClass node_penalty = 0;
    gradationPenalty(M, eij, beta, node_penalty);

    penalty += node_penalty.value();

    if (!dpenalty_dlogM.empty())
      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      {
        dpenalty_dlogM[node_m_i].value(ind) += node_penalty.deriv(                ind);
        dpenalty_dlogM[node_m_j].value(ind) += node_penalty.deriv(MatrixSym::SIZE+ind);
      }

  } // loop over edges

  // sum across processors
#ifdef SANS_MPI
  SANS_ASSERT(xfld_linear_.comm()->size() == 1);
  //penalty = boost::mpi::all_reduce( *xfld_linear_.comm(), penalty, std::plus<Real>() );
#endif
}

//===========================================================================//
template <class PhysDim, class TopoDim>
void
NodalMetrics<PhysDim, TopoDim>::
computeStepDeviation_logM(const MatrixSymFieldType& logMfld,
                          Real& penalty, std::vector<MatrixSym>& dpenalty_dlogM)
{
  // The entire adaptation folder is littered with the simplex assumption.
  // Putting in the for_each_CellGroup style here is a waste of time
  typedef typename Simplex<TopoDim>::type Topology;

  const int SurrealSize = MatrixSym::SIZE;
  typedef SurrealS<SurrealSize> SurrealClass;
  typedef DLA::MatrixSymS<D,SurrealClass> MatrixSymSurreal;

  typedef typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename MatrixSymFieldType::template FieldCellGroupType<Topology> IFieldCellGroupType;
  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

  SANS_ASSERT(logMfld.nDOF() == implied_metric_.nDOF());

  std::array<int,Topology::NNode> mCellNodes;

  for (int group = 0; group < xfld_linear_.nCellGroups(); group++)
  {
    const XFieldCellGroupType& xfldGroup = xfld_linear_.template getCellGroup<Topology>(group);
    const IFieldCellGroupType& mfldGroup = logMfld.template getCellGroup<Topology>(group);

    ElementXFieldClass xfldElem( xfldGroup.basis() );

    // loop over elements in cell groups
    for (int elem = 0; elem < xfldGroup.nElem(); elem++)
    {
      // skip elements not on processor
      if ( xfldGroup.associativity(elem).rank() != xfld_linear_.comm()->rank() ) continue;

      // get the global mapping of this element
      xfldGroup.getElement( xfldElem , elem );

      //get elemental implied metric
      MatrixSym MI;
      xfldElem.impliedMetric(MI);
      MatrixSym invsqrt_MI = pow(MI, -0.5);

      // get the logM field nodes
      mfldGroup.associativity(elem).getNodeGlobalMapping(mCellNodes.data(),Topology::NNode);

      // Use the log-euclidean mean
      MatrixSymSurreal M = 0;
      for (int i = 0; i < Topology::NNode; i++)
        M += logMfld.DOF(mCellNodes[i]);

      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
        M.value(ind).deriv(ind) = 1.0;

      M /= Topology::NNode;
      M = exp(M);

      MatrixSymSurreal S = invsqrt_MI*M*invsqrt_MI;
      S = log(S);

      SurrealClass elem_penalty = DLA::FrobNormSq(S);

      penalty += elem_penalty.value();

      if (!dpenalty_dlogM.empty())
        for (int i = 0; i < Topology::NNode; i++)
          for (int ind = 0; ind < MatrixSym::SIZE; ind++)
          {
            dpenalty_dlogM[mCellNodes[i]].value(ind) += elem_penalty.deriv(ind);
          }
    } // loop over elements
  } // loop over cell groups

  // sum across processors
#ifdef SANS_MPI
  penalty = boost::mpi::all_reduce( *xfld_linear_.comm(), penalty, std::plus<Real>() );
#endif
}

//===========================================================================//
template <class PhysDim, class TopoDim>
void
NodalMetrics<PhysDim, TopoDim>::
setNodalStepMatrix(const MatrixSymFieldType& Sfld)
{
  SANS_ASSERT( Sfld.nDOF() == implied_metric_.nDOF() );

  const int nNode = implied_metric_.nDOF();
  nodalStepMatrices_.resize(nNode);

  for (int node = 0; node < nNode; node++)
  {
    // save the step matrix
    nodalStepMatrices_[node] = Sfld.DOF(node);
  }
}

//===========================================================================//
template <class PhysDim, class TopoDim>
void
NodalMetrics<PhysDim, TopoDim>::
getStepMatrixField(MatrixSymFieldType_Nodal& step_fld)
{
  SANS_ASSERT( &step_fld.getXField() == &xfld_linear_ );

  for (int i = 0; i < step_fld.nCellGroups(); i++)
    SANS_ASSERT( step_fld.getCellGroupBase(i).order() == 1 ); //Has to be a P1 field

  //The loop for setting CG field DOFs below assumes that the CG field DOF indices
  //are the same as the xfld_linear_ ones, which is true for P1.
  for (int i = 0; i < xfld_linear_.nCellGroups(); i++)
    SANS_ASSERT( xfld_linear_.getCellGroupBase(i).order() == 1 );

  step_fld = 0;

  const int nNode = implied_metric_.nDOF();

  for (int node = 0; node < nNode; node++)
     for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      step_fld.DOF(node)[ind] = nodalStepMatrices_[node].value(ind);
}

//===========================================================================//
template <class PhysDim, class TopoDim>
void
NodalMetrics<PhysDim, TopoDim>::
getStepMatrixEigField(VectorFieldType_Nodal& eig_fld)
{
  SANS_ASSERT( &eig_fld.getXField() == &xfld_linear_ );

  for (int i = 0; i < eig_fld.nCellGroups(); i++)
    SANS_ASSERT( eig_fld.getCellGroupBase(i).order() == 1 ); //Has to be a P1 field

  //The loop for setting CG field DOFs below assumes that the CG field DOF indices
  //are the same as the xfld_linear_ ones, which is true for P1.
  for (int i = 0; i < xfld_linear_.nCellGroups(); i++)
    SANS_ASSERT( xfld_linear_.getCellGroupBase(i).order() == 1 );

  eig_fld = 0;

  const int nNode = implied_metric_.nDOF();

  for (int node = 0; node < nNode; node++)
  {
    //Obtain eigenvalues of S
    SANS::DLA::VectorS<D,Real> L;
    DLA::EigenValues( nodalStepMatrices_[node], L );

    for (int d = 0; d < D; d++)
      eig_fld.DOF(node)[d] = L[d];
  }
}

//===========================================================================//
template <class PhysDim, class TopoDim>
void
NodalMetrics<PhysDim, TopoDim>::
getEdgeLengthField(ScalarFieldType_Nodal& edgeLen_fld)
{
  SANS_ASSERT( &edgeLen_fld.getXField() == &xfld_linear_ );

  for (int i = 0; i < edgeLen_fld.nCellGroups(); i++)
    SANS_ASSERT( edgeLen_fld.getCellGroupBase(i).order() == 1 ); //Has to be a P1 field

  //The loop for setting CG field DOFs below assumes that the CG field DOF indices
  //are the same as the xfld_linear_ ones, which is true for P1.
  for (int i = 0; i < xfld_linear_.nCellGroups(); i++)
    SANS_ASSERT( xfld_linear_.getCellGroupBase(i).order() == 1 );

  edgeLen_fld = 0;

  MatrixSymFieldType metric_request(xfld_linear_, 1, BasisFunctionCategory_Lagrange, cellgroup_list_);
  getNodalMetricRequestField(metric_request);

  const int nNode = implied_metric_.nDOF();
  Field_NodalView metric_nodalview(implied_metric_, cellgroup_list_);

  for (int node = 0; node < nNode; node++)
  {
    //Get the list of edges around this node (each returned pair contains edge nodes)
    const std::vector<std::pair<int,int>> edge_list = metric_nodalview.getEdgeList(node);

    // Loop over all edges attached to this node
    for (auto edge = edge_list.begin(); edge != edge_list.end(); ++edge)
    {
      //Get the local index for the global node index using inverse map
      const int node_i = edge->first;
      const int node_j = edge->second;

      // Extract the metrics at the two nodes
      const MatrixSym& Mi = metric_request.DOF(node_i);
      const MatrixSym& Mj = metric_request.DOF(node_j);

      // Get the edge vector
      VectorX eij = xfld_linear_.DOF(edge->second) - xfld_linear_.DOF(edge->first);

      // Edge length calculation consistent with feflo.a
      // Real lmi = Transpose(eij)*Mi*eij;
      // Real lmj = Transpose(eij)*Mj*eij;
      Real lmi = DLA::InnerProduct(eij,Mi);
      Real lmj = DLA::InnerProduct(eij,Mj);

      lmi = sqrt(lmi);
      lmj = sqrt(lmj);

      Real edgeLen = 0;

      if ( fabs(lmi-lmj) <= 1.0e-6*lmi )
        edgeLen = lmi;
      else
        edgeLen = (lmi-lmj)/(log(lmi/lmj));

      // Compute the average edge length (not sure how else of visualize edge length...)
      //edgeLen_fld.DOF(node) += edgeLen/edge_list.size();

      // Compute the deviation from the bounds of the edge length
      Real bound = TopoDim::D == 1 ? 1.0 : sqrt(2.);

      Real del = 0;
      if ( edgeLen > 1 )
        del = edgeLen - bound;
      else
        del = 1/bound - edgeLen;

      // Compute the maximum deviation from the acceptable edge length
      edgeLen_fld.DOF(node) = max(edgeLen_fld.DOF(node), del);
    } // loop over edges
  } //loop over nodes
}

//===========================================================================//
template <class PhysDim, class TopoDim>
void
NodalMetrics<PhysDim, TopoDim>::
getEdgeLengths(std::vector<Real>& lengths )
{
  //The loop for setting CG field DOFs below assumes that the CG field DOF indices
  //are the same as the xfld_linear_ ones, which is true for P1.
  for (int i = 0; i < xfld_linear_.nCellGroups(); i++)
    SANS_ASSERT( xfld_linear_.getCellGroupBase(i).order() == 1 );

  MatrixSymFieldType metric_request(xfld_linear_, 1, BasisFunctionCategory_Lagrange, cellgroup_list_);
  getNodalMetricRequestField(metric_request);

  // Loop over all edges
  for (const Edge& edge : edges_)
  {
    //Get the nodes of this edge
    const int node_i = edge.ifld.first;
    const int node_j = edge.ifld.second;

    // Extract the metrics at the two nodes
    const MatrixSym& Mi = metric_request.DOF(node_i);
    const MatrixSym& Mj = metric_request.DOF(node_j);

    // Get the edge vector
    VectorX eij = xfld_linear_.DOF(edge.xfld.second) - xfld_linear_.DOF(edge.xfld.first);

    // Edge length calculation consistent with feflo.a
    // Real lmi = Transpose(eij)*Mi*eij;
    // Real lmj = Transpose(eij)*Mj*eij;
    Real lmi = DLA::InnerProduct(eij,Mi);
    Real lmj = DLA::InnerProduct(eij,Mj);

    lmi = sqrt(lmi);
    lmj = sqrt(lmj);

    Real edgeLen = 0;

    if ( fabs(lmi-lmj) <= 1.0e-6*lmi )
      edgeLen = lmi;
    else
      edgeLen = (lmi-lmj)/(log(lmi/lmj));

    lengths.push_back( edgeLen );

  } // loop over edges around node

  std::sort( lengths.begin() , lengths.end() );
}

//===========================================================================//
template <class PhysDim, class TopoDim>
void
NodalMetrics<PhysDim, TopoDim>::
getNodalMetricRequestField( MatrixSymFieldType& metric_fld )
{
  SANS_ASSERT( &metric_fld.getXField() == &xfld_linear_ );
  SANS_ASSERT( metric_fld.nDOF() == implied_metric_.nDOF() );

  for (int i = 0; i < metric_fld.nCellGroups(); i++)
    SANS_ASSERT( metric_fld.getCellGroupBase(i).order() == 1 ); //Has to be a P1 field

  int nNode = implied_metric_.nDOF();

  for (int node = 0; node < nNode; node++)
    metric_fld.DOF(node) = Metric::computeExponentialMap(implied_metric_.DOF(node), nodalStepMatrices_[node]);
}

//===========================================================================//
template <class PhysDim, class TopoDim>
void
NodalMetrics<PhysDim, TopoDim>::
getNodalImpliedMetricField( MatrixSymFieldType& metric_fld )
{
  SANS_ASSERT( &metric_fld.getXField() == &xfld_linear_ );

  for (int i = 0; i < metric_fld.nCellGroups(); i++)
    SANS_ASSERT( metric_fld.getCellGroupBase(i).order() == 1 ); //Has to be a P1 field

  //The loop for setting CG field DOFs below assumes that the CG field DOF indices
  //are the same as the xfld_linear_ ones, which is true for P1.
  for (int i = 0; i < xfld_linear_.nCellGroups(); i++)
    SANS_ASSERT( xfld_linear_.getCellGroupBase(i).order() == 1 );

  metric_fld = 0;

  const int nNode = implied_metric_.nDOF();

  for (int node = 0; node < nNode; node++)
    metric_fld.DOF(node) = implied_metric_.DOF(node);
}

} // namespace SANS
