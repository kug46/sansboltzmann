// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define NODALMETRICS_INSTANTIATE
#include "NodalMetrics_impl.h"

#include "Field/FieldVolume_CG_Cell.h"

#include "Field/XFieldVolume.h"

namespace SANS
{

//Explicit instantiations
template class NodalMetrics<PhysD3,TopoD3>;

} // namespace SANS
