// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOLVERINTERFACEBASE_H_
#define SOLVERINTERFACEBASE_H_

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "Topology/Dimension.h"
#include "Topology/ElementTopology.h"

#include "Field/Local/XField_Local_Base.h"
#include "Field/SpaceTypes.h"
#include "Field/tools/GroupFunctorType.h"

#include "Discretization/QuadratureOrder.h"

namespace SANS
{

struct LocalSolveStatus
{
  LocalSolveStatus() : converged(false), iter(0),
  time_residual(0), time_jacobian(0), time_solve(0), time_errest(0),
  time_algeqset(0), time_constructor(0), time_diagnose(0) {}
  LocalSolveStatus(bool converged, int iter) :
    converged(converged), iter(iter),
    time_residual(0), time_jacobian(0), time_solve(0), time_errest(0),
    time_algeqset(0), time_constructor(0), time_diagnose(0) {}

  bool converged = false;
  int iter = 0;
  Real time_residual = 0;
  Real time_jacobian = 0;
  Real time_solve = 0;
  Real time_errest = 0;
  Real time_algeqset = 0;
  Real time_constructor = 0;
  Real time_diagnose = 0;
  std::vector<std::vector<Real>> rsdNorm0_unconverged;
  std::vector<std::vector<Real>> rsdNorm_unconverged;
};

template <class PhysDim, class TopoDim>
class SolverInterfaceBase
{

public:

  SolverInterfaceBase() {}

  virtual ~SolverInterfaceBase() {}

  virtual void solveGlobalPrimalProblem() = 0;
  virtual void solveGlobalAdjointProblem() = 0;

  virtual void computeErrorEstimates() = 0;
  virtual Real getElementalErrorEstimate(int cellgroup, int elem) const = 0;
  virtual Real getGlobalErrorIndicator() const = 0;
  virtual Real getGlobalErrorEstimate() const = 0;

  // For elemental view, local_error is size 1, for edge view, local error is larger
  virtual LocalSolveStatus solveLocalProblem(const XField_Local_Base<PhysDim, TopoDim>& local_xfld, std::vector<Real>& local_error) const = 0;
  virtual Real computeInitialError(const XField_Local_Base<PhysDim, TopoDim>& local_xfld ) const
  {
    std::cout<< "warning: computeInitialError is assuming that local_xfld is unsplit" <<std::endl;
    std::vector<Real> tmp{0.0};
    LocalSolveStatus status = solveLocalProblem( local_xfld, tmp );
    return tmp[0];
  }

  // TODO: getnDOFperCell should instead be nDOFpossessed
  virtual Real getnDOFperCell(int cellgroup) const = 0;
  virtual int getSolutionOrder(int cellgroup) const = 0;

  virtual Real getOutput() const = 0;

  virtual void output_EField(const std::string& filename) const = 0;

  virtual const Field<PhysDim,TopoDim,Real>* pifld() const { return nullptr; };

  virtual int getCost() const = 0;

  virtual SpaceType spaceType() const { return SpaceType::Unspecified; }

protected:

};


template <class SolverInterfaceClass>
class SolverInterface_impl : public GroupFunctorCellType<SolverInterface_impl<SolverInterfaceClass>>
{
public:
  typedef typename SolverInterfaceClass::PhysDim PhysDim;

  SolverInterface_impl( const std::vector<int>& cellgroup_list, SolverInterfaceClass& base )
  : cellgroup_list_(cellgroup_list), interface_base_(base) {}

  std::size_t nCellGroups() const          { return cellgroup_list_.size(); }
  std::size_t cellGroup(const int n) const { return cellgroup_list_[n];     }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology >
  void
  apply(const typename XField<PhysDim,typename Topology::TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
        const int cellGroupGlobal)
  {
    interface_base_.template getSolutionInfo_cellgroup<Topology>(xfldCellGroup, cellGroupGlobal);
  }

protected:
  const std::vector<int> cellgroup_list_;

  //Reference to an instance of the problem statement class so that we can access its members
  SolverInterfaceClass& interface_base_;
};

}

#endif /* SOLVERINTERFACEBASE_H_ */
