// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOLVERINTERFACE_H_
#define SOLVERINTERFACE_H_

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/timer.h"
#include "Topology/Dimension.h"
#include "Topology/ElementTopology.h"

#include "SolverInterfaceBase.h"

#include "Discretization/ResidualNormType.h"
#include "Discretization/AlgebraicEquationSet_Type.h" // Meta class for getting local EqSet from a global EqSet

#include "Field/Local/XField_Local_Base.h"
#include "Field/output_Tecplot.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/tools/findGroups.h"
#include "tools/split_cat_std_vector.h"

#include "SolutionTrek/Continuation/Continuation.h"
#include "SolutionTrek/Continuation/PseudoTime/AlgebraicEquationSet_PTC.h"
#include "SolutionTrek/Continuation/ImplicitResidualSmoothing/AlgebraicEquationSet_IRS.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_LinearSolver.h"
#include "LinearAlgebra/DenseLinAlg/InverseLUP.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Meshing/EPIC/XField_PX.h"
#include "Meshing/EPIC/SolutionField_PX.h"

#include "Adaptation/MOESS/ParamFieldBuilder.h"

#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#include "tools/plus_std_vector.h"
#endif

namespace SANS
{


template<class SolutionClass, class AlgebraicEqSet, class OutputIntegrandType>
class SolverInterface : public SolverInterfaceBase<typename AlgebraicEqSet::PhysDim, typename AlgebraicEqSet::TopoDim>
{
public:
  typedef typename AlgebraicEqSet::PhysDim PhysDim;
  typedef typename AlgebraicEqSet::TopoDim TopoDim;

  const int D = PhysDim::D;
  typedef XField<PhysDim, TopoDim> XFieldType;

  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef typename SolutionClass::ParamFieldBuilderType ParamFieldBuilderType;
  typedef typename SolutionClass::ParamFieldBuilderLocalType ParamFieldBuilderLocalType;
  typedef typename SolutionClass::LiftedQuantityFieldType LiftedQuantityFieldType;

  typedef typename AlgebraicEqSet::NDPDEClass NDPDEClass;
  typedef typename NDPDEClass::template ArrayQ<Real> PDEArrayQ;

  typedef AlgebraicEqSet AlgEqnSet_Global;

  typedef typename LocalEquationSet<AlgEqnSet_Global>::type AlgEqnSet_Local;
  typedef typename AlgEqnSet_Global::FieldBundle FieldBundle;
  typedef typename AlgEqnSet_Global::ErrorEstimateClass ErrorEstimateClass;
  typedef typename AlgEqnSet_Local::FieldBundle_Local FieldBundle_Local;

  typedef SolverInterface SolverInterfaceClass;

  typedef typename OutputIntegrandType::template ArrayJ<Real> ArrayJ;

  typedef typename AlgEqnSet_Global::ArrayQ ArrayQ;
  typedef typename AlgEqnSet_Global::VectorArrayQ VectorArrayQ;
  typedef typename AlgEqnSet_Global::MatrixQ MatrixQ;

  template< class... BCArgs >
  SolverInterface(SolutionClass& sol, const ResidualNormType& resNormType, std::vector<Real>& tol, const int quadOrder,
                  const std::vector<int>& cellgroups, const std::vector<int>& interiortracegroups,
                  PyDict& BCList, const std::map< std::string, std::vector<int> >& BCBoundaryGroups,
                  const PyDict& NonlinearSolverDict, const PyDict& AdjLinearSolverDict,
                  const OutputIntegrandType& fcnOutput, BCArgs&... args )
    : sol_(sol),
      xfld_(sol_.xfld),
      resNormType_(resNormType),
      tol_(tol),
      quadOrder_(quadOrder),
      cellgroup_list_(cellgroups),
      interiortracegroup_list_(interiortracegroups),
      BCList_(BCList),
      BCBoundaryGroups_(BCBoundaryGroups),
      NonlinearSolverDict_(NonlinearSolverDict),
      AdjLinearSolverDict_(AdjLinearSolverDict),
      verbosity_(NonlinearSolverDict_.get(SolverContinuationParams<typename NDPDEClass::Temporal>::params.Verbose)),
      quadrule_(xfld_, quadOrder),
      AlgEqnSet_(sol_.paramfld, sol_.primal, sol_.pliftedQuantityfld,
                 sol_.pde, sol_.disc, quadrule_, resNormType_, tol_,
                 cellgroup_list_, interiortracegroup_list_, BCList_, BCBoundaryGroups_, args...),

      fcnOutput_(fcnOutput),
      estimatesEvaluated_(false)
  {
    errorIndicator_ = 0.0;
    errorEstimate_ = 0.0;

    //Loop over each cellgroup and find the no. of DOF per cell and solution order for that cellgroup
    nDOFperCell_.resize((int) xfld_.nCellGroups(), -1);
    orderVec_.resize((int) xfld_.nCellGroups(), -1);
    for_each_CellGroup<TopoDim>::apply( CellwiseOperations(cellgroup_list_, *this), xfld_);

    //Compute the total number of qfld DOFs
    nDOFGlobal_ = sol_.primal.qfld.nDOFpossessed();
#if defined(SANS_MPI) && !defined(__clang_analyzer__)
    nDOFGlobal_ = boost::mpi::all_reduce(*sol_.primal.qfld.comm(), nDOFGlobal_, std::plus<int>());
#endif
  }

  virtual ~SolverInterface() {}

  virtual void computeErrorEstimates() override;
  virtual Real getElementalErrorEstimate(int cellgroup, int elem) const override { return errorArray_[cellgroup][elem]; }
  virtual Real getGlobalErrorIndicator() const override { return errorIndicator_; }
  virtual Real getGlobalErrorEstimate()  const override { return errorEstimate_; }

  virtual void output_EField(const std::string& filename ) const override;

  virtual LocalSolveStatus solveLocalProblem(const XField_Local_Base<PhysDim, TopoDim>& local_xfld, std::vector<Real>& local_error) const override;
  virtual Real computeInitialError(const XField_Local_Base<PhysDim, TopoDim>& local_xfld ) const override;

  virtual Real getnDOFperCell(const int cellgroup) const override;
  virtual int getSolutionOrder(const int cellgroup) const override;

  virtual Real getOutput() const override { return outputJ_; }

  virtual int getCost() const override { return nDOFGlobal_; }
  const AlgEqnSet_Global& AlgEqnSet() { return AlgEqnSet_; }

  virtual SpaceType spaceType() const override { return sol_.spaceType; }

  const QuadratureOrder& getQuadOrder() const { return quadrule_; }
  void getQuadOrder( QuadratureOrder& quadrule ) const { quadrule = quadrule_; }

  virtual const Field<PhysDim,TopoDim,Real>* pifld() const override { return &pErrorEstimate_->getIField(); };

  virtual void writeRestart(const std::string xfld_file, const std::string qfld_file) const
  {
    WriteMeshGrm<PhysDim, TopoDim>(xfld_, xfld_file);
    WriteSolution_PX<PhysDim, TopoDim, ArrayQ>(sol_.primal.qfld, qfld_file);
  }

  ErrorEstimateClass& getErrorEstimateClass() const { return *pErrorEstimate_; }

  SolverInterface_impl<SolverInterfaceClass>
  CellwiseOperations(const std::vector<int>& cellgroup_list, SolverInterfaceClass& base )
  {
    return SolverInterface_impl<SolverInterfaceClass>(cellgroup_list, base);
  }

  template<class Topology>
  void getSolutionInfo_cellgroup( const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
                                  const int cellGroupGlobal);

  void setBaseFilePath(const std::string& path) { filename_base_ = path; }

  // A Sub-class for the Solution of Local Problems - Used because it helps encapsulates a bunch of functions
  // that can now be inspected externally, for instance, solveLocalPrimalProblem
  class LocalProblem
  {
  public:

    LocalProblem( const SolverInterfaceClass& interface, const XField_Local_Base<PhysDim,TopoDim>& xfld_local, LocalSolveStatus& status ) :
      construct_timer_(),
      interface_(interface), xfld_local_(xfld_local), status_(status), quadrule_(xfld_local, interface_.quadOrder_), maxIter(status.iter),
      BCList_local_(), BCBoundaryGroups_local_(),
#ifdef WHOLEPATCH
      local_cellGroups_{linspace(0,xfld_local_.nCellGroups()-1)}, // not using the whole patch then only have inner cell groups
#else
      local_cellGroups_{0}, // not using the whole patch then only have inner cell groups
#endif
      local_interiorTraceGroups_(findInteriorTraceGroup(xfld_local,0,1)),
      primal_local_(xfld_local_,  interface_.sol_.primal,       interface_.sol_.primal.order,  active_local_boundaries_ ),
      adjoint_local_(xfld_local_, interface_.sol_.adjoint,      interface_.sol_.adjoint.order, active_local_boundaries_ ),
      parambuilder_(xfld_local_, interface_.sol_.parambuilder, primal_local_.qfld),
      resNormType_local_(interface_.resNormType_)
    {
      // This fills BCList_local_ and BCBoundaryGroups_local_
      setActiveLocalBoundaries( xfld_local_, BCList_local_, BCBoundaryGroups_local_ );

      // DG fields need the additional interior trace group from 0-0
      // VMSD too but we add it in the LocalProblem_VMSD
      if (interface_.sol_.primal.spaceType == SpaceType::Discontinuous)
      {
        std::vector<int> tmp = findInteriorTraceGroup(xfld_local,0,0);

        if (!tmp.empty())
        {
          local_interiorTraceGroups_.push_back(tmp[0]);
          std::sort(local_interiorTraceGroups_.begin(),local_interiorTraceGroups_.end());
        }
      }

      if (interface_.sol_.pliftedQuantityfld) //global solution has a lifted quantity field
      {
        //Create local lifted quantity field
        const int order = interface_.sol_.orderLiftedQuantity;
        BasisFunctionCategory category = interface_.sol_.basisCategoryLiftedQuantity;
        pliftedQuantityfld_local_ = std::make_shared<LiftedQuantityFieldType>(xfld_local_, order, category);
      }
      status.time_constructor += construct_timer_.elapsed();

    }

    virtual ~LocalProblem() {}

    // All of these functions have to be called via this->
    // because we need to get the virtual function in the derived classes+
    virtual std::vector<Real> getLocalError( )
    {
      // Solve
      this->solveLocalPrimalProblem(xfld_local_, primal_local_);

      // Estimate
      this->estimateLocalError( xfld_local_, primal_local_, adjoint_local_ );

      // check that all the errors returned by a local solve are non nan
      for (auto const& error1 : local_error_)
        if ( std::isnan(error1) )
        {
          this->dumpLocalFields();
          SANS_DEVELOPER_EXCEPTION("Local solve returned a nan, check the local solution and adjoint!");
        }


      return local_error_;
    };

    Real getInitialLocalError( )
    {
      // Calculate eta_kappa_0 directly on the local solve patch.
      // For DG this makes no difference, the estimate recompute region is cell group 0.
      // For CG this makes a difference because the recompute region includes cell group 1.
      return estimateInitialError( xfld_local_, primal_local_, adjoint_local_ );
    };

  protected:
    timer construct_timer_;

    void setActiveLocalBoundaries( const XField_Local_Base<PhysDim,TopoDim>& local_xfld,
                                   PyDict& BCList_local, std::map<std::string, std::vector<int>>& BCBoundaryGroups_local );

    virtual void solveLocalPrimalProblem( const XField_Local_Base<PhysDim,TopoDim>& local_xfld, FieldBundle_Local& primal );

    virtual void estimateLocalError( const XField_Local_Base<PhysDim,TopoDim>& local_xfld,
                                     FieldBundle_Local& primal, FieldBundle_Local& adjoint );
    Real estimateInitialError( const XField_Local_Base<PhysDim,TopoDim>& local_xfld, FieldBundle_Local& primal, FieldBundle_Local& adjoint );

    // This can be re-defined for a different discretization, for instance if additional fields need to be dumped
    // Have a look at VMSD, it does this in order to dump the qp and ap fields.
    virtual void dumpLocalFields()
    {
      output_Tecplot( primal_local_.qfld, interface_.filename_base_ + "qfld_local_unconverged.plt", std::vector<std::string>(), true );
      output_Tecplot( adjoint_local_.qfld, interface_.filename_base_ + "adjfld_local_unconverged.plt", std::vector<std::string>(), true );
    };

    const SolverInterfaceClass& interface_; // SolverInterface that owns this LocalProblem

    const XField_Local_Base<PhysDim,TopoDim>& xfld_local_;
    LocalSolveStatus& status_;
    QuadratureOrder quadrule_;
    const int maxIter; // max number of iterations in local solve

    // vectors of groups operated on
    PyDict BCList_local_;
    std::map<std::string, std::vector<int>> BCBoundaryGroups_local_;
    std::vector<int> local_cellGroups_, local_interiorTraceGroups_;
    std::vector<std::vector<int>> active_local_boundaries_;

    FieldBundle_Local primal_local_, adjoint_local_;
    std::shared_ptr<LiftedQuantityFieldType> pliftedQuantityfld_local_;
    ParamFieldBuilderLocalType parambuilder_;
    const ResidualNormType& resNormType_local_;

    const Real stol = 1e-10; // line search tolerance in local solve

    std::vector<Real> local_error_;
  };


protected:

  template<class Temporal>
  typename std::enable_if< std::is_same<Temporal,TemporalMarch>::value, void >::type
  solveGlobalPrimalContinuation();

  template<class Temporal>
  typename std::enable_if< std::is_same<Temporal,TemporalSpaceTime>::value, void >::type
  solveGlobalPrimalContinuation();

  SolutionClass& sol_; //class containing all the solution data
  const XFieldType& xfld_; // mesh
  const ResidualNormType resNormType_;
  std::vector<Real>& tol_;
  const int quadOrder_;
  const std::vector<int>& cellgroup_list_;
  const std::vector<int>& interiortracegroup_list_;
  PyDict& BCList_;
  const std::map< std::string, std::vector<int> >& BCBoundaryGroups_;
  const PyDict& NonlinearSolverDict_;
  const PyDict& AdjLinearSolverDict_;
  const bool verbosity_;

  QuadratureOrder quadrule_;

  AlgEqnSet_Global AlgEqnSet_; // AlgebraicEquationSet

  // pointers to the estimate
  std::shared_ptr<ErrorEstimateClass> pErrorEstimate_;

  std::vector<std::vector<Real>> errorArray_; //Array of elemental error estimates (index:[cellgroup][elem])
  const OutputIntegrandType& fcnOutput_;
  Real errorIndicator_, errorEstimate_;
  ArrayJ outputJ_; //output functional evaluated at current solution

  // has to be Real rather than int, because continuous schemes end up with effectively fractional dof
  std::vector<Real> nDOFperCell_; //indexing: [global cellgrp] - number of degrees of freedom per cell
  std::vector<int> orderVec_; //indexing: [global cellgrp] - solution order of each cellgroup

  Real nDOFGlobal_; //total number of qfld DOFs

  std::string filename_base_ = "tmp/";
  bool estimatesEvaluated_;
};

template<class SolutionClass, class AlgebraicEqSet, class OutputIntegrandType>
template<class Temporal>
typename std::enable_if< std::is_same<Temporal,TemporalMarch>::value, void >::type
SolverInterface<SolutionClass, AlgebraicEqSet, OutputIntegrandType>::
solveGlobalPrimalContinuation()
{
  typedef typename AlgEqnSet_Global::SystemMatrix SystemMatrixClass;
  typedef typename AlgEqnSet_Global::SystemVector SystemVectorClass;

  // TODO: note that currently it defaults to non-manifold integrand in PTC
  typedef AlgebraicEquationSet_PTC<NDPDEClass, AlgEqSetTraits_Sparse, PTC, ParamFieldType> AlgebraicEquationSet_PTCClass;
  typedef AlgebraicEquationSet_IRS<NDPDEClass, AlgEqSetTraits_Sparse, IRS, ParamFieldType> AlgebraicEquationSet_IRSClass;

  typedef SolverContinuationParams<typename NDPDEClass::Temporal> ContinuationParams;

  //Get the requested continuation
  DictKeyPair continuation = NonlinearSolverDict_.get(ContinuationParams::params.Continuation);

  if (continuation == ContinuationParams::params.Continuation.None)
  {
    // Create the nonlinear solver object
    NonLinearSolver<SystemMatrixClass> nonlinear_solver( AlgEqnSet_, continuation );

    // set initial condition from current solution in solution fields
    SystemVectorClass sln0(AlgEqnSet_.vectorStateSize());
    AlgEqnSet_.fillSystemVector(sln0);

    // nonlinear solve
    SystemVectorClass sln(AlgEqnSet_.vectorStateSize());
    SolveStatus status = nonlinear_solver.solve(sln0, sln);

    if (status.converged == false)
    {
      sol_.dumpPrimalSolution( filename_base_, "_unconverged.plt" );

      if (sol_.pliftedQuantityfld)
        output_Tecplot( *sol_.pliftedQuantityfld, filename_base_ + "liftedQuantityfld_unconverged.plt" );

      SANS_DEVELOPER_EXCEPTION("Nonlinear solver failed to converge.");
    }
  }
  else if (continuation == ContinuationParams::params.Continuation.PseudoTime)
  {
    AlgebraicEquationSet_PTCClass AlgEqSetPTC(sol_.paramfld,
                                              sol_.primal.qfld, sol_.pde,
                                              QuadratureOrder(xfld_, quadOrder_),
                                              cellgroup_list_, AlgEqnSet_);
    // Create the pseudo time continuation class
    PseudoTime<SystemMatrixClass> PTC(continuation, AlgEqSetPTC);

    if (!PTC.solve())
    {
      sol_.dumpPrimalSolution( filename_base_, "_unconverged.plt" );

      if (sol_.pliftedQuantityfld)
        output_Tecplot( *sol_.pliftedQuantityfld, filename_base_ + "liftedQuantityfld_unconverged.plt" );

      SANS_DEVELOPER_EXCEPTION("Nonlinear solver with PTC failed to converge.");
    }
  }
  else if (continuation == ContinuationParams::params.Continuation.ImplicitResidualSmoothing)
  {
    AlgebraicEquationSet_IRSClass AlgEqSetPTC(sol_.paramfld,
                                              sol_.primal.qfld, sol_.pde,
                                              QuadratureOrder(xfld_, quadOrder_),
                                              cellgroup_list_, AlgEqnSet_);
    // Create the pseudo time continuation class
    ImplicitResidualSmoothing<SystemMatrixClass> IRS(continuation, AlgEqSetPTC);

    if (!IRS.solve())
    {
      output_Tecplot( sol_.primal.qfld, filename_base_ + "qfld_unconverged.plt" );

      if (sol_.pliftedQuantityfld)
        output_Tecplot( *sol_.pliftedQuantityfld, filename_base_ + "liftedQuantityfld_unconverged.plt" );

      SANS_DEVELOPER_EXCEPTION("Nonlinear solver with IRS failed to converge.");
    }
  }
  else
    SANS_DEVELOPER_EXCEPTION("SolverInterface::solveGlobalPrimalProblem - Unknown nonlinear solver continuation type.");
}

template<class SolutionClass, class AlgebraicEqSet, class OutputIntegrandType>
template<class Temporal>
typename std::enable_if< std::is_same<Temporal,TemporalSpaceTime>::value, void >::type
SolverInterface<SolutionClass, AlgebraicEqSet, OutputIntegrandType>::
solveGlobalPrimalContinuation()
{
  typedef typename AlgEqnSet_Global::SystemMatrix SystemMatrixClass;
  typedef typename AlgEqnSet_Global::SystemVector SystemVectorClass;

  // TODO: note that currently it defaults to non-manifold integrand in PTC
  typedef AlgebraicEquationSet_PTC<NDPDEClass, AlgEqSetTraits_Sparse, PTC, ParamFieldType> AlgebraicEquationSet_PTCClass;

  typedef SolverContinuationParams<typename NDPDEClass::Temporal> ContinuationParams;

  //Get the requested continuation
  DictKeyPair continuation = NonlinearSolverDict_.get(ContinuationParams::params.Continuation);

  if (continuation == ContinuationParams::params.Continuation.None)
  {
    // Create the nonlinear solver object
    NonLinearSolver<SystemMatrixClass> nonlinear_solver( AlgEqnSet_, continuation );

    // set initial condition from current solution in solution fields
    SystemVectorClass sln0(AlgEqnSet_.vectorStateSize());
    AlgEqnSet_.fillSystemVector(sln0);

    // nonlinear solve
    SystemVectorClass sln(AlgEqnSet_.vectorStateSize());
    SolveStatus status = nonlinear_solver.solve(sln0, sln);

    if (status.converged == false)
    {
      output_Tecplot( sol_.primal.qfld, filename_base_ + "qfld_unconverged.plt" );
#if 0 //TODO: only works for DG + some PDEs for now
      output_Tecplot( sol_.pde, sol_.paramfld, sol_.primal.qfld, sol_.primal.rfld, filename_base_ + "sol_unconverged.plt" );
#endif

      if (sol_.pliftedQuantityfld)
        output_Tecplot( *sol_.pliftedQuantityfld, filename_base_ + "liftedQuantityfld_unconverged.plt" );

      SANS_DEVELOPER_EXCEPTION("Nonlinear solver failed to converge.");
    }
  }
  else if (continuation == ContinuationParams::params.Continuation.PseudoTime)
  {
    AlgebraicEquationSet_PTCClass AlgEqSetPTC(sol_.paramfld,
                                              sol_.primal.qfld, sol_.pde,
                                              QuadratureOrder(xfld_, quadOrder_),
                                              cellgroup_list_, AlgEqnSet_);
    // Create the pseudo time continuation class
    PseudoTime<SystemMatrixClass> PTC(continuation, AlgEqSetPTC);

    if (!PTC.solve())
    {
      output_Tecplot( sol_.primal.qfld, filename_base_ + "qfld_unconverged.plt" );

      if (sol_.pliftedQuantityfld)
        output_Tecplot( *sol_.pliftedQuantityfld, filename_base_ + "liftedQuantityfld_unconverged.plt" );

      SANS_DEVELOPER_EXCEPTION("Nonlinear solver with PTC failed to converge.");
    }
  }
  else
    SANS_DEVELOPER_EXCEPTION("SolverInterface::solveGlobalPrimalProblem - Unknown nonlinear solver continuation type.");
}


template<class SolutionClass, class AlgebraicEqSet, class OutputIntegrandType>
void
SolverInterface<SolutionClass, AlgebraicEqSet, OutputIntegrandType>::
computeErrorEstimates()
{
  timer clock;

  QuadratureOrder quadrule(xfld_, quadOrder_);

  pErrorEstimate_ = std::make_shared<ErrorEstimateClass>( sol_.paramfld, sol_.primal, sol_.adjoint, sol_.pliftedQuantityfld,
                           sol_.pde, sol_.disc, quadrule, cellgroup_list_, interiortracegroup_list_,
                           BCList_, BCBoundaryGroups_ );

  pErrorEstimate_->fillEArray(errorArray_);
  errorIndicator_ = 0.0, errorEstimate_ = 0.0;

  pErrorEstimate_->getErrorIndicator(errorIndicator_);

  pErrorEstimate_->getErrorEstimate(errorEstimate_);

  estimatesEvaluated_ = true;

  if (get<-1>(xfld_).comm()->rank() == 0 && verbosity_ )
    std::cout << "Error estimation time: " << std::fixed << std::setprecision(3) << clock.elapsed() << "s" << std::endl;

}

template<class SolutionClass, class AlgebraicEqSet, class OutputIntegrandType>
void
SolverInterface<SolutionClass, AlgebraicEqSet, OutputIntegrandType>::
output_EField(const std::string& filename ) const
{
  SANS_ASSERT_MSG( estimatesEvaluated_, "computeErrorEstimates() must be called before estimates can be output");

  pErrorEstimate_->outputFields(filename);
}

template<class SolutionClass, class AlgebraicEqSet, class OutputIntegrandType>
void
SolverInterface<SolutionClass, AlgebraicEqSet, OutputIntegrandType>::LocalProblem::
setActiveLocalBoundaries( const XField_Local_Base<PhysDim,TopoDim>& xfld_local,
                          PyDict& BCList_local, std::map<std::string, std::vector<int>>& BCBoundaryGroups_local )
{
  /*
    Loop over the local xfld boundary groups. If they are attached to cell group 0,
    then find their entry in the global BCList and and create an entry in the local BCList.
    Then add this local boundary group to the set associated with that local boundary condition

    This is the list of local boundaries which residuals, jacobians and estimates will be computed
  */
  for (int local_group = 0; local_group < xfld_local.nBoundaryTraceGroups(); local_group++)
  {
    // if not using the whole patch, then only need BCs attached to inner region
#ifndef WHOLEPATCH
    if ( xfld_local.getBoundaryTraceGroupBase(local_group).getGroupLeft() != 0 )
      continue; // skip local boundary groups not connected to cell group 0
#endif

    const int globalBGroup = xfld_local.globalBoundaryGroup(local_group); // the global boundary group
    bool found = false; // have you found the local boundary group yet?
    for ( auto const& keyVal : interface_.BCBoundaryGroups_ ) // loop over BCName - BC groups pairs from the global problem
    {
      for (auto const& group : keyVal.second ) // loop over the groups for this BCName
        if ( globalBGroup == group ) // found the corresponding global boundary group
        {
          BCBoundaryGroups_local[keyVal.first].push_back(local_group);
          BCList_local[keyVal.first] = interface_.BCList_[keyVal.first];
          found = true; break;
        }
      if (found) break; // found this local group - stop checking the rest of the boundary groups
    }
  }

  // set of boundary groups of lgfld on xfld_local that are active in the local solve.
  // Need to decide which ones must share dofs
  std::vector<int> active_local_boundaries = AlgEqnSet_Local::BCParams::getLGBoundaryGroups(BCList_local, BCBoundaryGroups_local);

  // This is a problem for another day. If the lagrange multipler boundary conditions ever get fixed.
  // Then something like the group bundling below will need to be done, to ensure the fields share dofs appropriately based on the global solve
  SANS_ASSERT_MSG( active_local_boundaries.empty(), "Lagrange multiplier boundary conditions are unsupported in local patches");
}

// separating off the primal re-solving from the estimation
template<class SolutionClass, class AlgebraicEqSet, class OutputIntegrandType>
void
SolverInterface<SolutionClass, AlgebraicEqSet, OutputIntegrandType>::LocalProblem::
solveLocalPrimalProblem( const XField_Local_Base<PhysDim, TopoDim>& xfld_local, FieldBundle_Local& primal_local )
{
  typedef typename AlgEqnSet_Local::SystemVector SystemVectorClass;
  typedef typename AlgEqnSet_Local::SystemMatrix SystemMatrixClass;
  typedef typename AlgEqnSet_Local::SystemNonZeroPattern SystemNonZeroPattern;

  //The residual norm tolerances for the local problems should account for any nDOF scalings
  std::vector<Real> local_tol = interface_.tol_;
  const Real DOF_ratio = (Real) primal_local.qfld.nDOF() / (Real) interface_.nDOFGlobal_;

  if (interface_.resNormType_ == ResidualNorm_L2) //L2 norm scales as sqrt(nDOFGlobal)
  {
    local_tol[AlgEqnSet_Local::iPDE] = interface_.tol_[AlgEqnSet_Global::iPDE] * sqrt(DOF_ratio);
  }
  else if (interface_.resNormType_ == ResidualNorm_L2_DOFWeighted) // independent of nDOFGlobal
  {
    local_tol[AlgEqnSet_Local::iPDE] = interface_.tol_[AlgEqnSet_Global::iPDE];
  }
  else if (interface_.resNormType_ == ResidualNorm_InvJac_DOF_Weighted) // independent of nDOFGlobal
  {
    local_tol[AlgEqnSet_Local::iPDE] = interface_.tol_[AlgEqnSet_Global::iPDE];
  }
  else if (interface_.resNormType_ == ResidualNorm_L2_DOFAvg) // L2 norm scales as sqrt(nDOFGlobal)
  {
    local_tol[AlgEqnSet_Local::iPDE] = interface_.tol_[AlgEqnSet_Global::iPDE]*sqrt(DOF_ratio);
  }
  else if (interface_.resNormType_ == ResidualNorm_InvJac_RelWeighted) // this norm scales as 1/sqrt(nDOFGlobal)
  {
    local_tol[AlgEqnSet_Local::iPDE] = interface_.tol_[AlgEqnSet_Global::iPDE] / sqrt(DOF_ratio);
  }
  else
    SANS_DEVELOPER_EXCEPTION("SolverInterface::solveLocalPrimalProblem - Unknown residual norm type!");

  timer algeqset;
  AlgEqnSet_Local algEqnSet_local(parambuilder_.fld, primal_local, pliftedQuantityfld_local_,
                                  interface_.sol_.pde, interface_.sol_.disc, quadrule_, resNormType_local_, local_tol,
                                  local_cellGroups_, local_interiorTraceGroups_,
                                  BCList_local_, BCBoundaryGroups_local_);
  status_.time_algeqset += algeqset.elapsed();

  // Local vectors
  SystemVectorClass q_local   (algEqnSet_local.vectorStateSize()); // current solution
  SystemVectorClass q_orig   (algEqnSet_local.vectorStateSize()); // current solution
  SystemVectorClass q0_local  (algEqnSet_local.vectorStateSize()); // previous solution
  SystemVectorClass dsln_local(algEqnSet_local.vectorStateSize()); // solution update
  SystemVectorClass rsd_local (algEqnSet_local.vectorEqSize()); // residual

  // Convert the projected qfld to a vector
  algEqnSet_local.fillSystemVector(q_local);

  // // check if the solution transfered to the split grid is physical
  // bool initialStateValid = algEqnSet_local.isValidStateSystemVector(q_local);

  if ( algEqnSet_local.isValidStateSystemVector(q_local) == false )
  {
    // project the global solution down to a P1 basis
    std::vector<int> collapsed_active_local_boundaries = SANS::cat(active_local_boundaries_);
    FieldBundle primal_local_P1(xfld_local, 1, primal_local, collapsed_active_local_boundaries );

    // project the solution onto the P1 basis
    primal_local.projectTo(primal_local_P1);

    // prolongate the P1 solution transfer into the local fields
    primal_local_P1.projectTo(primal_local);

    // Convert the projected qfld to a vector
    algEqnSet_local.fillSystemVector(q_local);
  }

  q_orig = q_local;

  // Compute the lifting operators (rfld)
  algEqnSet_local.setSolutionField(q_local);

  // Local Jacobian
  SystemNonZeroPattern nz_local(algEqnSet_local.matrixSize());
  SystemMatrixClass jac_local(nz_local);

//  int maxIter = 10;
//  Real stol = 1e-10;
  Real s = stol + 1.0;

  timer clock_residual;
  rsd_local = 0;
  algEqnSet_local.residual(rsd_local);

  status_.time_residual += clock_residual.elapsed();

  // save off the current residual with the projected solution
  std::vector<std::vector<Real>> rsdNorm0( algEqnSet_local.residualNorm(rsd_local) );
  std::vector<std::vector<Real>> rsdNorm = rsdNorm0;

  for (int iter = 0; iter < maxIter && s > stol; iter++)
  {
//    std::vector<std::vector<Real>> nrmRsd = algEqnSet_local.residualNorm(rsd_local);
//    std::cout << "Local Newton iteration " << iter  << ": Res_L2norm = " << std::setprecision(10) << nrmRsd << std::endl;

    // save the current solution and residual norm
    q0_local = q_local;

    //Jacobian
    timer clock_jacobian;
    jac_local = 0;

    if (algEqnSet_local.isStaticCondensed())
      algEqnSet_local.jacobian(rsd_local, jac_local);
    else
      algEqnSet_local.jacobian(jac_local);

//    std::cout << "jac_local = "<< std::endl;
//    jac_local.dump();

    status_.time_jacobian += clock_jacobian.elapsed();

    //Perform local solve
    timer clock_solve;
//    std::cout << "rsd_local 1 = "<< std::endl;
//    std::cout << "rsd_local 1 = "<< std::endl;
//    std::cout << "rsd_local 1 = "<< std::endl;
//    std::cout << "rsd_local 1 m: " << rsd_local(0).m();
//    rsd_local(0).dump();
//    std::cout << "rsd_local 2 = "<< std::endl;
//    std::cout << "rsd_local 2 = "<< std::endl;
//    std::cout << "rsd_local 2 = "<< std::endl;
//    std::cout << "rsd_local 2 m: " << rsd_local(1).m();
//    rsd_local(1).dump();
//    std::cout << std::endl;

#if 1
    // If there are lagrange multipliers, flatten and pivot
    // This is gonna slow things down a bit on boundaries, but whatever -- Hugh
    // TODO: Block InverseLUP
    if (algEqnSet_local.isStaticCondensed())
    {
      SystemVectorClass dsln_condensed = dsln_local.sub(1,2);
      SystemVectorClass rsd_condensed = rsd_local.sub(1,2);
      try
      {
//        SLA::WriteMatrixMarketFile( jac_local(0,0), "tmp/jac_local.mtx");
        dsln_condensed = DLA::InverseLUP::Solve(jac_local, rsd_condensed);
      }
      catch ( ... )
      {
        SLA::WriteMatrixMarketFile( jac_local(0,0), "tmp/jac_local.mtx");
        fprintf(stdout,"Singular Local Jacobian written to file");
        SANS_DEVELOPER_EXCEPTION("Local solve failed");
      }

      algEqnSet_local.completeUpdate(rsd_local, dsln_condensed, dsln_local);

    }
    else if (jac_local.m() > 1 && jac_local(1,1).m() > 0 )
    {

      // Copy, flatten and pivot inverse
      int m0 = jac_local(0,0).m(), m1 = jac_local(1,0).m();
      int n0 = jac_local(0,0).n(), n1 = jac_local(0,1).n();
      int qL = DLA::VectorSize<ArrayQ>::M;

      SANS_ASSERT_MSG( m0 + m1 == n0 + n1, "system should be square");
      DLA::MatrixD<Real> jac_local_flat(qL*(m0+m1),qL*(n0+n1));
      DLA::VectorD<Real> rsd_local_flat(qL*(n0+n1)), dsln_local_flat(qL*(n0+n1));

      const int M = DLA::VectorSize<ArrayQ>::M;

      // 0 Row
      for ( int j = 0; j < n0; j++) //j columns
        for (int qJ = 0; qJ < M; qJ++)
        {
          rsd_local_flat(j*qL + qJ) = DLA::index((rsd_local(0))(j),qJ);
          for (int qI = 0; qI < M; qI++)
          {
            // (0,0) block
            for (int i = 0; i < m0; i++)
              jac_local_flat(i*qL + qI,j*qL + qJ) = DLA::index((jac_local(0,0))(i,j),qI,qJ);
            // (0,1) block
            for (int i = 0; i < m1; i++)
              jac_local_flat( (m0 + i)*qL + qI,j*qL + qJ) = DLA::index((jac_local(1,0))(i,j),qI,qJ);
          }
        }


      // 1 Row
      for ( int j = 0; j < n1; j++)
        for (int qJ = 0; qJ < M; qJ++)
        {
          rsd_local_flat( (n0 + j)*qL + qJ) = DLA::index((rsd_local(1))(j),qJ);
          for (int qI = 0; qI < M; qI++)
          {
            // (1,0) block
            for (int i = 0; i < m0; i++)
              jac_local_flat(i*qL + qI, (n0 + j)*qL + qJ) = DLA::index((jac_local(0,1))(i,j),qI,qJ);
            // (1,1) block
            for (int i = 0; i < m1; i++)
              jac_local_flat( (m0 + i)*qL + qI, (n0 + j)*qL + qJ) = DLA::index((jac_local(1,1))(i,j),qI,qJ);
          }
        }

//      std::cout<< "flat solve" << std::endl;
//      SLA::WriteMatrixMarketFile( jac_local_flat, "tmp/jac_local_flat.mtx");
      try
      {
//        SLA::WriteMatrixMarketFile( jac_local_flat, "tmp/jac_local_nonSC.mtx");
        dsln_local_flat = DLA::InverseLUP::Solve(jac_local_flat, rsd_local_flat);
      }
      catch ( ... )
      {
        SLA::WriteMatrixMarketFile( jac_local_flat, "tmp/jac_local.mtx");
        fprintf(stdout,"Singular Local Jacobian written to file");
        SANS_DEVELOPER_EXCEPTION("Local solve failed");
      }

      // fold back up into the solution vector
      for (int j = 0; j < n0; j++)
        for (int qJ = 0; qJ < M; qJ++)
          DLA::index(dsln_local(0)(j),qJ) = dsln_local_flat(j*qL + qJ);

      for (int j = 0; j < n1; j++)
        for (int qJ = 0; qJ < M; qJ++)
          DLA::index(dsln_local(1)(j),qJ) = dsln_local_flat( (n0 + j)*qL + qJ);

    }
    else
    {
      //      std::cout<< "block solve" << std::endl;
      //      SLA::WriteMatrixMarketFile( jac_local(0,0), "tmp/jac_local.mtx");
      try
      {
        dsln_local = DLA::InverseLUP::Solve(jac_local, rsd_local);
      }
      catch ( ... )
      {
        SLA::WriteMatrixMarketFile( jac_local(0,0), "tmp/jac_local.mtx");
        fprintf(stdout,"Singular Local Jacobian written to file");
        output_Tecplot( primal_local.qfld, "tmp/qfld_local_unsolved.plt");
        SANS_DEVELOPER_EXCEPTION("Local solve failed");
      }


    }
#else
    dsln_local = DLA::InverseLUP::Solve(jac_local, rsd_local);
#endif


//    std::cout << "dsln_local = ";
//    dsln_local(0).dump();
//    dsln_local(1).dump();
    status_.time_solve += clock_solve.elapsed();

    // use a halving line search to update the solution
    Real s = 2.0;
    bool isValidState = false;
    while (s > stol)
    {
      s = s/2.0;

      //Update local solution vector
      q_local = q0_local - s*dsln_local;

      // check that the solution is valid (this sets the qfld and also computes the rfld)
      isValidState = algEqnSet_local.isValidStateSystemVector(q_local);
      if ( !isValidState )
      {

//        std::cout << "Local Line Search s: " << s  << ": invalid state "  << std::endl;
        continue;
      }

      // Compute the new residual
      timer clock_residual;
      rsd_local = 0;
      algEqnSet_local.residual(rsd_local);
      // save the residual for this iteration
      rsdNorm = algEqnSet_local.residualNorm(rsd_local);
      status_.time_residual += clock_residual.elapsed();

//      std::cout << "Local Line Search s: " << s  << ": Res_L2norm = " << std::setprecision(10) << rsdNorm
//          << "isValidState: " << isValidState <<
//          ", decreasedresid: " << algEqnSet_local.decreasedResidual(rsdNorm0, rsdNorm) << std::endl;

      // break out if the residual decreased
      if ( algEqnSet_local.decreasedResidual(rsdNorm0, rsdNorm) ) break;
    }

    if (!isValidState)
    {
      // Here we've cut back the linesearch all the way but the solution is still not physical
      // so revert to the initial solution.
      q_local = q0_local;
      // This call sets the solution field using the q0 vector
      algEqnSet_local.setSolutionField(q_local);
      status_.converged = false;
      status_.iter = iter+1;
      break;
    }

    // save the residual for this iteration
    rsdNorm0 = algEqnSet_local.residualNorm(rsd_local);


//    output_Tecplot(primal_local_.qfld, "tmp/q_local.plt");
//    output_Tecplot(primal_local_.qpfld, "tmp/qp_local.plt");
//
//    SANS_DEVELOPER_EXCEPTION("STOPHERE\n");

    // checking the convergence at the end of the loop ensures at least one Newton iteration
    // this helps the error model as the tol used in algEqnSet_local is not necessarily precise
    if ( algEqnSet_local.convergedResidual(rsdNorm) )
    {
      status_.converged = true;
      status_.iter = iter+1;
      break;
    }
  }


  //Gather some information about the residual norms if the solve didn't converge
  if (status_.converged == false)
  {
    status_.rsdNorm0_unconverged = rsdNorm0;
    status_.rsdNorm_unconverged = rsdNorm;
//
  }

}

// separating off the estimation part
template<class SolutionClass, class AlgebraicEqSet, class OutputIntegrandType>
void
SolverInterface<SolutionClass, AlgebraicEqSet, OutputIntegrandType>::LocalProblem::
estimateLocalError( const XField_Local_Base<PhysDim, TopoDim>& xfld_local, FieldBundle_Local& primal_local, FieldBundle_Local& adjoint_local )
{
  //Compute local error estimates
  timer clock_errest;
  // Only estimate error for cellgroup 0! this is the split group
  ErrorEstimateClass ErrorEstimate(parambuilder_.fld, primal_local, adjoint_local, pliftedQuantityfld_local_,
                                   interface_.sol_.pde, interface_.sol_.disc, quadrule_,
                                   local_cellGroups_, local_interiorTraceGroups_,
                                   BCList_local_, BCBoundaryGroups_local_);


  ErrorEstimate.getLocalSolveErrorIndicator(local_error_); //Accumulate errors only from cellgroup 0 - (main cells)

  status_.time_errest += clock_errest.elapsed();
}


// For calculating the initial estimate from the local patch
template<class SolutionClass, class AlgebraicEqSet, class OutputIntegrandType>
Real
SolverInterface<SolutionClass, AlgebraicEqSet, OutputIntegrandType>::LocalProblem::
estimateInitialError( const XField_Local_Base<PhysDim, TopoDim>& xfld_local, FieldBundle_Local& primal_local, FieldBundle_Local& adjoint_local )
{
  //Compute initial local error estimate
  timer clock_errest;

  // Only estimate error for cellgroup 0! this is the split group
  ErrorEstimateClass ErrorEstimate(parambuilder_.fld, primal_local, adjoint_local, pliftedQuantityfld_local_,
                                   interface_.sol_.pde, interface_.sol_.disc, quadrule_,
                                   local_cellGroups_, local_interiorTraceGroups_,
                                   BCList_local_, BCBoundaryGroups_local_);

  Real error0 = 0.0;
  ErrorEstimate.getErrorEstimate( error0, local_cellGroups_ );
  status_.time_errest += clock_errest.elapsed();
  return error0;
}

template<class SolutionClass, class AlgebraicEqSet, class OutputIntegrandType>
LocalSolveStatus
SolverInterface<SolutionClass, AlgebraicEqSet, OutputIntegrandType>::
solveLocalProblem(const XField_Local_Base<PhysDim, TopoDim>& xfld_local, std::vector<Real>& local_error) const
{
  // std::cout<< "Using the standard local solve call " << std::endl;
  const int maxIter = 10;
  LocalSolveStatus status(false, maxIter);

  LocalProblem localProblem( *this, xfld_local, status );
  local_error = localProblem.getLocalError();
  return status;
}

template<class SolutionClass, class AlgebraicEqSet, class OutputIntegrandType>
Real
SolverInterface<SolutionClass, AlgebraicEqSet, OutputIntegrandType>::
computeInitialError(const XField_Local_Base<PhysDim, TopoDim>& xfld_local) const
{
  const int maxIter = 10;
  LocalSolveStatus status(false, maxIter);

  LocalProblem localProblem( *this, xfld_local, status );
  return localProblem.getInitialLocalError();
}

template<class SolutionClass, class AlgebraicEqSet, class OutputIntegrandType>
Real
SolverInterface<SolutionClass, AlgebraicEqSet, OutputIntegrandType>::
getnDOFperCell(const int cellgroup) const
{
  SANS_ASSERT(cellgroup >= 0 && cellgroup < (int) nDOFperCell_.size());
  return nDOFperCell_[cellgroup];
}

template<class SolutionClass, class AlgebraicEqSet, class OutputIntegrandType>
int
SolverInterface<SolutionClass, AlgebraicEqSet, OutputIntegrandType>::
getSolutionOrder(const int cellgroup) const
{
  SANS_ASSERT(cellgroup >= 0 && cellgroup < (int) orderVec_.size());
  return orderVec_[cellgroup];
}

template<class SolutionClass, class AlgebraicEqSet, class OutputIntegrandType>
template<class Topology>
void
SolverInterface<SolutionClass, AlgebraicEqSet, OutputIntegrandType>::
getSolutionInfo_cellgroup(const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
                          const int cellGroupGlobal)
{
  typedef typename SolutionClass::FieldBundle::QFieldType::template FieldCellGroupType<Topology> QFieldCellGroupType;
  const QFieldCellGroupType& cellgrp = sol_.primal.qfld.template getCellGroup<Topology>(cellGroupGlobal);

  nDOFperCell_[cellGroupGlobal] = cellgrp.basis()->nBasis();
  orderVec_[cellGroupGlobal]    = cellgrp.basis()->order();

  const int order = cellgrp.basis()->order();

  if ( spaceType() == SpaceType::Continuous)
  {
    switch ( AlgebraicEqSet::TopoDim::D )
    {
    case TopoD1::D:
      // 2*1 node dof -> 2*1*1/2 node dof = 1 dof;
      nDOFperCell_[cellGroupGlobal] -= 1; // the P1 dofs are shared
      break;
    case TopoD2::D:
      /*
        6 regular triangles can share a point, so node dofs are shared by 6
        2 regular triangles can share an edge, so edge dofs are shared by 2
        cell dofs are solely owned

        3*1 node dofs -> 3*1/6 node dof = 1/2 dof
        3*(order-1) edge dofs -> 3*(order-1)*1/2 dof =
      */
      nDOFperCell_[cellGroupGlobal] -= (3 - 1./2); // the node dofs are shared by 6

      nDOFperCell_[cellGroupGlobal] -= (3 - 3./2)* max(0,(order - 1 )); // if there are edge dofs they are shared
      break;
    case TopoD3::D:

      /*
        20 regular tetrahedra can share a point, so P1 dofs are shared by 20
        6 regular tetrahedra can share an edge, so edge dofs are shared by 6
        2 regular tetrahedra can share a face, so face dofs are shared by 2
        cell dofs are solely owned

        4*1 node dof -> 4 *1/20 node dof = 1/5 dof
        6*(order-1) edge dofs -> 6*(order-1)*1/6 edge dof = (order-1) dof
        4*(order-2)*(order-1)/2 face dofs -> 4*(order-2)*(order-1)/2 *1/2 face dofs = 2(order-2)(order-1) dof

      */

      nDOFperCell_[cellGroupGlobal] -= (4 - 1./5); // the node dofs are shared by 20

      nDOFperCell_[cellGroupGlobal] -= (6 - 1)*max(0,(order-1)); // if there are edge dofs they are shared by 6

      nDOFperCell_[cellGroupGlobal] -= (4 - 2)*max(0,(order-1)*(order-2)/2); // if there are face dofs they are shared by 2
      break;

    case TopoD4::D:
       // (d + 1)!/((m + 1)!(d - m)!) is the number of m-faces on a d-dimensional simplex

      /*
       * 120 regular pentatopes can share a point so vertex dofs are shared by 120
       * 20 regular pentatopes can share an edge so edge DOFs are shared by 20
       * 6 regular pentatopes can share a triangle so area DOFs are shared by 6
       * 2 regular pentatopes can share a tetrahedron, so face DOFs are shared by 2
       * cell dofs are solely owned
       *
       * 5 nodes,  1/120 dof shares per node, 1/24 dof share per elem,  1 dof per node
       * 10 edges, 1/20 edge shares per node, 1/2 edge share per elem,  (order - 1) dof per edge (interior)
       * 10 areas, 1/6 area share per node,   5/3 area share per elem,  (order - 2)*(order - 1)/2 dofs per area (interior)
       * 5 traces, 1/2 trace share per node,  5/2 trace share per elem, (order - 3)*(order - 2)*(order - 1)/6 dofs per trace (interior)
       */

      nDOFperCell_[cellGroupGlobal] -= ( 5 - 1./24);
      nDOFperCell_[cellGroupGlobal] -= (10 - 1./2)*max(0, (order - 1));
      nDOFperCell_[cellGroupGlobal] -= (10 - 5./3)*max(0, (order - 1)*(order - 2)/2);
      nDOFperCell_[cellGroupGlobal] -= ( 5 - 5./2)*max(0, (order - 1)*(order - 2)*(order - 3)/6);

      break;
    }
  }
}

}

#endif /* SOLVERINTERFACE_H_ */
