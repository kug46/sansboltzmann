// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOLVERINTERFACE_DGBR2_H_
#define SOLVERINTERFACE_DGBR2_H_

#include <ostream>
#include <vector>

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/DG/AlgebraicEquationSet_Local_DG.h"

// Output Functional Stuff that needs to be removed eventually
#include "Discretization/DG/FunctionalCell_DGBR2.h"
#include "Discretization/DG/JacobianFunctionalCell_DGBR2.h"
#include "Discretization/DG/FunctionalBoundaryTrace_Dispatch_DGBR2.h"
#include "Discretization/DG/JacobianFunctionalBoundaryTrace_Dispatch_DGBR2.h"

#include "Discretization/Galerkin/FunctionalBoundaryTrace_Dispatch_Galerkin.h"
#include "Discretization/Galerkin/JacobianFunctionalBoundaryTrace_Dispatch_Galerkin.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#include "MPI/communicator_fwd.h"
#endif

// What will eventually be the true Solver Interface. This file is a stop gap solution until output functionals work
#include "Adaptation/MOESS/SolverInterface.h"

#include "ErrorEstimate/DG/ErrorEstimate_DGBR2.h"

#ifdef BOUNDARYOUTPUT
// HACK: These boundary integrands are needed for the boundary output at the moment.
//       They should be removed in the future.
#include "Discretization/DG/IntegrandBoundaryTrace_LinearScalar_sansLG_DGBR2.h"
#include "Discretization/DG/IntegrandBoundaryTrace_Flux_mitState_DGBR2.h"

#include "Discretization/Galerkin/IntegrandBoundaryTrace_None_Galerkin.h"
#endif

namespace SANS
{

template<class SolutionClass, class AlgebraicEqSet, class OutputIntegrandType>
class SolverInterface_DGBR2 : public SolverInterface<SolutionClass, AlgebraicEqSet, OutputIntegrandType>
{
public:
  typedef SolverInterface<SolutionClass, AlgebraicEqSet, OutputIntegrandType> BaseType;

  typedef typename AlgebraicEqSet::PhysDim PhysDim;
  typedef typename AlgebraicEqSet::TopoDim TopoDim;

  const int D = PhysDim::D;
  typedef XField<PhysDim, TopoDim> XFieldType;

  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef typename SolutionClass::ParamFieldBuilderType ParamFieldBuilderType;
  typedef typename SolutionClass::ParamFieldBuilderLocalType ParamFieldBuilderLocalType;

  typedef typename AlgebraicEqSet::NDPDEClass NDPDEClass;
  typedef typename NDPDEClass::template ArrayQ<Real> PDEArrayQ;

  typedef AlgebraicEqSet AlgEqnSet_Global;

  typedef typename LocalEquationSet<AlgEqnSet_Global>::type AlgEqnSet_Local;
  typedef typename AlgEqnSet_Global::FieldBundle FieldBundle;
  typedef typename AlgEqnSet_Global::ErrorEstimateClass ErrorEstimateClass;
  typedef typename AlgEqnSet_Local::FieldBundle FieldBundle_Local;

  typedef SolverInterface_DGBR2 SolverInterfaceClass;

  typedef typename OutputIntegrandType::template ArrayJ<Real> ArrayJ;

  typedef typename AlgEqnSet_Global::ArrayQ ArrayQ;

  template< class... BCArgs >
  SolverInterface_DGBR2(SolutionClass& sol, const ResidualNormType& resNormType, std::vector<Real>& tol, const int quadOrder,
                        const std::vector<int>& cellgroups, const std::vector<int>& interiortracegroups,
                        PyDict& BCList, const std::map< std::string, std::vector<int> >& BCBoundaryGroups,
                        const PyDict& NonlinearSolverDict, const PyDict& AdjLinearSolverDict,
                        const OutputIntegrandType& fcnOutput, BCArgs&... args )
    : BaseType( sol, resNormType, tol, quadOrder, cellgroups, interiortracegroups,
                BCList, BCBoundaryGroups, NonlinearSolverDict, AdjLinearSolverDict, fcnOutput, args... )
  {
/*
    errorIndicator_ = 0.0;
    errorEstimate_ = 0.0;

    //Loop over each cellgroup and find the no. of DOF per cell and solution order for that cellgroup
    nDOFperCell_.resize((int) xfld_.nCellGroups(), -1);
    orderVec_.resize((int) xfld_.nCellGroups(), -1);
    for_each_CellGroup<TopoDim>::apply( CellwiseOperations(cellgroup_list_, *this), xfld_);
*/
  }

  virtual ~SolverInterface_DGBR2() {}

  virtual void solveGlobalPrimalProblem() override;
  virtual void solveGlobalAdjointProblem() override;

protected:

  using BaseType::sol_; //class containing all the solution data
  using BaseType::xfld_; // mesh
  using BaseType::resNormType_;
  using BaseType::tol_;
  using BaseType::quadOrder_;
  using BaseType::cellgroup_list_;
  using BaseType::interiortracegroup_list_;
  using BaseType::BCList_;
  using BaseType::BCBoundaryGroups_;
  using BaseType::NonlinearSolverDict_;
  using BaseType::AdjLinearSolverDict_;
  using BaseType::verbosity_;

  using BaseType::AlgEqnSet_; // AlgebraicEquationSet

  using BaseType::errorArray_; //Array of elemental error estimates (index:[cellgroup][elem])
  using BaseType::fcnOutput_;
  using BaseType::errorIndicator_;
  using BaseType::errorEstimate_;
  using BaseType::outputJ_; //output functional evaluated at current solution

  using BaseType::nDOFperCell_; //indexing: [global cellgrp] - number of degrees of freedom per cell
  using BaseType::orderVec_; //indexing: [global cellgrp] - solution order of each cellgroup
};

template<class SolutionClass, class AlgebraicEqSet, class OutputIntegrandType>
void
SolverInterface_DGBR2<SolutionClass, AlgebraicEqSet, OutputIntegrandType>::
solveGlobalPrimalProblem()
{
  timer clock;

  this->template solveGlobalPrimalContinuation<typename NDPDEClass::Temporal>();

  if (get<-1>(xfld_).comm()->rank() == 0 && verbosity_ )
    std::cout << "Primal solve time: " << std::fixed << std::setprecision(3) << clock.elapsed() << "s" << std::endl;

  // evaluate output function
  outputJ_ = 0.0;

  QuadratureOrder quadrule(xfld_, quadOrder_);

#ifndef BOUNDARYOUTPUT
  IntegrateCellGroups<TopoDim>::integrate( FunctionalCell_DGBR2( fcnOutput_, outputJ_ ),
                                           sol_.paramfld, (sol_.primal.qfld, sol_.primal.rfld),
                                           quadrule.cellOrders.data(), quadrule.cellOrders.size() );
#else
  AlgEqnSet_.dispatchBC().dispatch_DGBR2(
    FunctionalBoundaryTrace_Dispatch_DGBR2(fcnOutput_, sol_.paramfld, sol_.primal.qfld, sol_.primal.rfld,
                                           quadrule.boundaryTraceOrders.data(), quadrule.boundaryTraceOrders.size(), outputJ_ ),
    FunctionalBoundaryTrace_Dispatch_Galerkin(fcnOutput_, sol_.paramfld, sol_.primal.qfld,
                                              quadrule.boundaryTraceOrders.data(),
                                              quadrule.boundaryTraceOrders.size(), outputJ_ ) );
#endif

#ifdef SANS_MPI
  outputJ_ = boost::mpi::all_reduce( *get<-1>(xfld_).comm(), outputJ_, std::plus<Real>() );
#endif
}

template<class SolutionClass, class AlgebraicEqSet, class OutputIntegrandType>
void
SolverInterface_DGBR2<SolutionClass, AlgebraicEqSet, OutputIntegrandType>::
solveGlobalAdjointProblem()
{
  typedef typename AlgEqnSet_Global::SystemVector SystemVectorClass;
  typedef typename AlgEqnSet_Global::SystemMatrix SystemMatrixClass;

  typedef SurrealS<NDPDEClass::N> SurrealClass;

  const int iPDE = AlgEqnSet_.iPDE;

  //Create primal solutions in richer solution space
  FieldBundle primal_pro(xfld_, sol_.adjoint.order,
                         sol_.primal.basis_cell, sol_.primal.basis_trace, sol_.mitlg_boundarygroups);

  // Prolongate the primal solution to the richer solution space
  sol_.primal.projectTo(primal_pro);

  // Create param fields with prolongated primal solution
  ParamFieldBuilderType parambuilder(sol_.paramfld, primal_pro.qfld, sol_.parambuilder.dict);

  QuadratureOrder quadrule(xfld_, quadOrder_);

  //Create AlgebraicEquationSets for solutions in richer space
  AlgEqnSet_Global AlgEqnSet_Pro(parambuilder.fld, primal_pro, sol_.pliftedQuantityfld,
                                 sol_.pde, sol_.disc, quadrule, resNormType_, tol_,
                                 cellgroup_list_, interiortracegroup_list_, BCList_, BCBoundaryGroups_);

  SystemVectorClass adj(AlgEqnSet_Pro.vectorStateSize());
  adj = 0; // set initial guess


#if defined(SANS_PETSC) && 1
  DictKeyPair SolverParam = AdjLinearSolverDict_.get(SLA::LinearSolverParam::params.LinearSolver);

  if ( SolverParam == SLA::LinearSolverParam::params.LinearSolver.PETSc )
  {
    // Because the estimate used in Galerkin methods is strong form, thus need to subtract off a P solution
    timer clock_p;

    FieldBundle adjoint_p( xfld_,sol_.primal.order, sol_.adjoint,
                           sol_.mitlg_boundarygroups );

    if (get<-1>(xfld_).comm()->rank() == 0 && verbosity_ )
      std::cout<< "Computing P adjoint..." << std::endl;

    SystemVectorClass rhs_p( AlgEqnSet_.vectorEqSize() );
    rhs_p = 0;

#ifndef BOUNDARYOUTPUT
  IntegrateCellGroups<TopoDim>::integrate( JacobianFunctionalCell_DGBR2<SurrealClass>( fcnOutput_, rhs_p(iPDE) ),
                                           sol_.paramfld, (sol_.primal.qfld, sol_.primal.rfld),
                                           quadrule.cellOrders.data(), quadrule.cellOrders.size() );
#else
  AlgEqnSet_.dispatchBC().dispatch_DGBR2(
      JacobianFunctionalBoundaryTrace_Dispatch_DGBR2<SurrealClass>(fcnOutput_, sol_.paramfld,
                                                                   sol_.primal.qfld, sol_.primal.rfld, adjoint_p.rfld,
                                                                   quadrule.boundaryTraceOrders.data(),
                                                                   quadrule.boundaryTraceOrders.size(), rhs_p(iPDE) ),
      JacobianFunctionalBoundaryTrace_Dispatch_Galerkin<SurrealClass>(fcnOutput_, sol_.paramfld,
                                                                      sol_.primal.qfld,
                                                                      quadrule.boundaryTraceOrders.data(),
                                                                      quadrule.boundaryTraceOrders.size(), rhs_p(iPDE) ) );
#endif

    // Adjoint solver
    SLA::LinearSolver< SystemMatrixClass > solver_p( AdjLinearSolverDict_, AlgEqnSet_, SLA::TransposeSolve );

    SystemVectorClass adj_p( AlgEqnSet_.vectorStateSize() );
    adj_p = 0; // set initial guess

    solver_p.solve( rhs_p, adj_p );

    // update solution
    AlgEqnSet_.setAdjointField( adj_p, adjoint_p.qfld, adjoint_p.rfld, adjoint_p.lgfld );

    adjoint_p.qfld.syncDOFs_MPI_noCache();
    adjoint_p.lgfld.syncDOFs_MPI_noCache();

    // Prolongate the P-adjoint solution to the richer space
    adjoint_p.qfld.projectTo(sol_.adjoint.qfld);
    adjoint_p.lgfld.projectTo(sol_.adjoint.lgfld);

    // set the initial guess as the prolongated adjoint solution
    AlgEqnSet_Pro.fillSystemVector(adj, sol_.adjoint.qfld, sol_.adjoint.lgfld );

    if (get<-1>(xfld_).comm()->rank() == 0 && verbosity_ )
      std::cout << "Adjoint order=" << sol_.primal.order << " solve time: "
                << std::fixed << std::setprecision(3) << clock_p.elapsed() << "s" << std::endl;
  }
#endif


  timer clock;

  // Functional integral
  SystemVectorClass rhs(AlgEqnSet_Pro.vectorEqSize());
  rhs = 0;

#ifndef BOUNDARYOUTPUT
  IntegrateCellGroups<TopoDim>::integrate( JacobianFunctionalCell_DGBR2<SurrealClass>( fcnOutput_, rhs(iPDE) ),
                                           parambuilder.fld, (primal_pro.qfld, primal_pro.rfld),
                                           quadrule.cellOrders.data(), quadrule.cellOrders.size() );
#else
  AlgEqnSet_Pro.dispatchBC().dispatch_DGBR2(
      JacobianFunctionalBoundaryTrace_Dispatch_DGBR2<SurrealClass>(fcnOutput_, parambuilder.fld,
                                                                   primal_pro.qfld, primal_pro.rfld, sol_.adjoint.rfld,
                                                                   quadrule.boundaryTraceOrders.data(),
                                                                   quadrule.boundaryTraceOrders.size(), rhs(iPDE) ),
      JacobianFunctionalBoundaryTrace_Dispatch_Galerkin<SurrealClass>(fcnOutput_, parambuilder.fld,
                                                                      primal_pro.qfld,
                                                                      quadrule.boundaryTraceOrders.data(),
                                                                      quadrule.boundaryTraceOrders.size(), rhs(iPDE) ) );
#endif

  // adjoint solver
  SLA::LinearSolver< SystemMatrixClass > solver(AdjLinearSolverDict_, AlgEqnSet_Pro, SLA::TransposeSolve);

  if (get<-1>(xfld_).comm()->rank() == 0 && verbosity_ )
    std::cout<< "Computing P+"<< sol_.adjoint.order - sol_.primal.order << " adjoint..." << std::endl;

  solver.solve(rhs, adj);

  // update solution
  AlgEqnSet_Pro.setAdjointField(adj, sol_.adjoint.qfld, sol_.adjoint.rfld, sol_.adjoint.lgfld);

  sol_.adjoint.qfld.syncDOFs_MPI_noCache();
  sol_.adjoint.lgfld.syncDOFs_MPI_noCache();

  if (get<-1>(xfld_).comm()->rank() == 0 && verbosity_ )
    std::cout << "Adjoint order=" << sol_.adjoint.order << " solve time: "
              << std::fixed << std::setprecision(3) << clock.elapsed() << "s" << std::endl;
}


} // namespace SANS

#endif /* SOLVERINTERFACE_DGBR2_H_ */
