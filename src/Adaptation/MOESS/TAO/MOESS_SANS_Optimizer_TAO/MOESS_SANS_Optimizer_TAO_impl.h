// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(MOESS_SANS_OPTIMIZER_TAO_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "../MOESS_SANS_Optimizer_TAO.h"
#include "Adaptation/MOESS/TAO/TAOException.h"

#include "Adaptation/MOESS/MOESS.h"

#include <vector>
#include <limits>

#include <petscvec.h>

#include "Field/XField.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_FrobNorm.h"
#include "Surreal/SurrealS.h"
#include "tools/smoothmath.h"
#include "tools/linspace.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#endif

//#define MMA_DEBUG

namespace SANS
{

// augmemted lagrangian stolen shamelesslu from auglag.c in NLopt

//---------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
MOESS_SANS_Optimizer<PhysDim,TopoDim,TAO>::
MOESS_SANS_Optimizer( const Real targetCost, const Real h_domain_max,
                      std::shared_ptr<ErrorModelType>& errorModel,
                      const XField<PhysDim,TopoDim>& xfld_linear,
                      const std::vector<int>& cellgroup_list,
                      const SolverInterfaceBase<PhysDim,TopoDim>& problem,
                      const PyDict& paramsDict )
  : BaseType(targetCost, errorModel),
    Sfld_(xfld_linear, 1, BasisFunctionCategory_Lagrange, cellgroup_list),
    lambda_(0), rho_(10)
{
  if (xfld_linear.comm()->rank() == 0)
  {
    std::cout << std::endl;
    std::cout << "TAO SANS optimizing metrics" << std::endl;
    std::cout << std::endl;
  }

  // construct the nodal implied metric field
  nodalMetrics_ = std::make_shared<NodalMetricsType>(xfld_linear, cellgroup_list,
                                                     problem, paramsDict);

  const int nNode = Sfld_.nDOF();

  h_coarsen_factors_.resize(nNode);
  h_refine_factor_ = sqrt(2); //paramsDict.get(ParamsType::params.hRefineFactorMax);
  Real beta;
  if (problem.spaceType() == SpaceType::Continuous)
    beta = 1.1; // Must be consistent with ErrorModel::computeNodalError
  else
    beta = 1.5; // Must be consistent with ErrorModel::computeError_cellgroup
  h_refine_factor_ = pow(h_refine_factor_, sqrt(D)*beta);

  nodalMetrics_->initializeCostStepMatrices(h_domain_max, h_refine_factor_,
                                            targetCost_, cost_initial_estimate_,
                                            Sfld_, h_coarsen_factors_);

  std::vector<MatrixSym> d_dSvec;

  //Compute the global error estimate as the scaling factor
  errorModel_->computeError(Sfld_, error0_, d_dSvec);

  // just so we don't divide by zero
  if (error0_ == 0) error0_ = std::numeric_limits<Real>::epsilon();

  Tao tao; // the tao optimization object

  TAO_STATUS( TaoCreate(*nodalMetrics_->comm(), &tao) );

  // TAOBQNLS will replace TAOBLMVM in newer version of PETSc
  // Nethier of these enforce constraints...
#if PETSC_VERSION_LT(3,10,0)
  TAO_STATUS( TaoSetType(tao, TAOBLMVM) );
#else
  TAO_STATUS( TaoSetType(tao, TAOBQNLS) );
#endif

  TAO_STATUS( TaoSetObjectiveAndGradientRoutine(tao, FormFunctionGradientAugLag, reinterpret_cast<void*>(this)) );

#if 0 // Non-linear constraints are not yet supported
  TAO_STATUS( TaoSetObjectiveAndGradientRoutine(tao, FormFunctionGradient, reinterpret_cast<void*>(this)) );

  PetscInt nCE = 1;
  Vec CE;
  TAO_STATUS( VecCreateSeq(PETSC_COMM_SELF, nCE, &CE) );

  Mat JE;
  TAO_STATUS( MatCreateDense(*nodalMetrics_->comm(), PETSC_DECIDE, nLocal, 1, PETSC_DECIDE, NULL, &JE) );

  TAO_STATUS( TaoSetEqualityConstraintsRoutine(tao, CE, FormEqualityConstraints, reinterpret_cast<void*>(this)) );
  TAO_STATUS( TaoSetJacobianRoutine(tao, JE, JE, FormEqualityJacobian, reinterpret_cast<void*>(this)) );
#endif

  //Termination conditions
  //Real xtol_rel = MOESS_base_.paramsDict_.get(ParamsType::params.Optimizer_XTol_Rel);
  Real ftol_rel = paramsDict.get(ParamsType::params.Optimizer_FTol_Rel);
  int max_eval  = paramsDict.get(ParamsType::params.Optimizer_MaxEval);

  TAO_STATUS( TaoSetTolerances(tao,1e-8,1e-8,ftol_rel) );

  //stop when the maximum number of function evaluations is reached
  TAO_STATUS( TaoSetMaximumFunctionEvaluations(tao, max_eval) );
  TAO_STATUS( TaoSetMaximumIterations(tao, 10*max_eval) );

#if 0
  //stop when every parameter changes by less than the tolerance multiplied by the absolute value of the parameter.
  opt_.set_xtol_rel(xtol_rel);

  //stop when the objective function value changes by less than the tolerance multiplied by the absolute value of the function value
  opt_.set_ftol_rel(ftol_rel);
#endif

  //const int nNode = Sfld_.nDOF();
  const int nNodePossessed = Sfld_.nDOFpossessed();
  const int nLocal = nNodePossessed*MatrixSym::SIZE;

  {
    Vec Lower_bound_vector, Upper_bound_vector;
    TAO_STATUS( VecCreateMPI(*nodalMetrics_->comm(), nLocal, PETSC_DETERMINE, &Lower_bound_vector) );
    TAO_STATUS( VecCreateMPI(*nodalMetrics_->comm(), nLocal, PETSC_DETERMINE, &Upper_bound_vector) );

    PetscScalar *lower_bound_vector;
    TAO_STATUS( VecGetArray(Lower_bound_vector,&lower_bound_vector) );
    PetscScalar *upper_bound_vector;
    TAO_STATUS( VecGetArray(Upper_bound_vector,&upper_bound_vector) );

    for (int node = 0; node < nNodePossessed; node++)
    {
      //Set bounds on the step matrix at this node
      Real lower_bound = -2.0*log(h_coarsen_factors_[node]); //max coarsening factor = h_refine_factor_limited
      Real upper_bound = +2.0*log(h_refine_factor_);         //max refinement factor = h_refine_factor_limited

      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      {
        lower_bound_vector[node*MatrixSym::SIZE + ind] = lower_bound;
        upper_bound_vector[node*MatrixSym::SIZE + ind] = upper_bound;
      }
    } //loop over nodes

    TAO_STATUS(TaoSetVariableBounds(tao, Lower_bound_vector, Upper_bound_vector));
    TAO_STATUS( VecDestroy(&Lower_bound_vector) );
    TAO_STATUS( VecDestroy(&Upper_bound_vector) );
  }

  Vec X;
  TAO_STATUS( VecCreateMPI(*nodalMetrics_->comm(), nLocal, PETSC_DETERMINE, &X) );
  PetscInt nDim;
  TAO_STATUS( VecGetSize(X, &nDim) );

  //Initialize solution vector
  {
    PetscScalar *x;
    TAO_STATUS( VecGetArray(X,&x) );

    for (int node = 0; node < nNodePossessed; node++)
      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
        x[node*MatrixSym::SIZE + ind] = Sfld_.DOF(node).value(ind);

    TAO_STATUS( VecRestoreArray(X,&x) );
    TAO_STATUS( TaoSetInitialVector(tao, X) );
  }
  //TAO_STATUS( TaoSetFromOptions(tao) );

#if 0 // Show progress
  PetscViewer monviewer;
  TAO_STATUS( PetscViewerASCIIOpen(PETSC_COMM_SELF,"stdout",&monviewer) );
  TAO_STATUS( TaoSetMonitor(tao,TaoMonitorDefault,monviewer,(PetscErrorCode (*)(void**))PetscViewerDestroy) );
#endif

  // get the initial value with the penalty terms possibly active
  PetscReal f_init;
  Vec G;
  TAO_STATUS( VecDuplicate(X, &G) );
  computeObjectiveAugLag(X, &f_init, G);
  TAO_STATUS( VecDestroy(&G) );
  PetscReal f_opt = 0.0;

  if (nodalMetrics_->comm()->rank() == 0)
  {
    std::cout << std::endl;
    std::cout << "------------------------------------------" << std::endl;
    std::cout << "Starting TAO Optimization"   << std::endl;
    std::cout << "------------------------------------------" << std::endl;
    std::cout << "    Variables         : " << nDim        << std::endl;
    std::cout << "------------------------------------------" << std::endl;
    //std::cout << std::endl;
  }

  int total_eval_count = 0;
  int outer_iter = 0;
  Real ICM = 0;
  Real constraint = 0;

  /* magic parameters from Birgin & Martinez */
  const Real tau = 0.5, gam = 10;
  const Real lam_min = -1e20, lam_max = 1e20;

  do //Outer loop - until consrraint is satisfiec
  {
    this->eval_count_ = 0;

    TAO_STATUS( TaoSolve(tao) );

    PetscReal gnorm, cnorm, xdiff;
    PetscInt its;
    TaoConvergedReason reason;

    TAO_STATUS( TaoGetSolutionStatus(tao, &its, &f_opt, &gnorm, &cnorm, &xdiff, &reason) );

    const PetscScalar *x;
    TAO_STATUS( VecGetArrayRead(X,&x) );
    for (int node = 0; node < nNodePossessed; node++)
    {
      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
        Sfld_.DOF(node).value(ind) = x[node*MatrixSym::SIZE + ind];
    }
    TAO_STATUS( VecRestoreArrayRead(X,&x) );

    // synchronize ghost/zombie DOFs
    Sfld_.syncDOFs_MPI_Cached();

    Real SnormSq = 0.;
    for (int node = 0; node < nNodePossessed; node++)
    {
      Real SnormSq_temp = DLA::FrobNormSq(Sfld_.DOF(node));
      if ( SnormSq_temp > SnormSq )
        SnormSq = SnormSq_temp;
    }

#ifdef SANS_MPI
    SnormSq = boost::mpi::all_reduce(*nodalMetrics_->comm(), SnormSq, boost::mpi::maximum<Real>());
#endif

    // calculate the penalty for the max norm node
    Real maxSpenalty;
    {
      Real frobNormSqTargetOuter = pow(sqrt(D)*2.0*log(h_refine_factor_),2.0);
      Real frobNormSqTargetInner = pow(        2.0*log(h_refine_factor_),2.0);
      Real frobNormSqTargetDel = D == 1 ? 1e-2 : frobNormSqTargetOuter/frobNormSqTargetInner - 1;

      Real tmp = SnormSq/frobNormSqTargetInner - 1 - frobNormSqTargetDel/2;
      Real tmp_clipped = smoothmaxC2(0.0, tmp, frobNormSqTargetDel );
      maxSpenalty = tmp_clipped*tmp_clipped;
    }

    //Compute the global error estimate
    Real errEst = 0;
    errorModel_->computeError(Sfld_, errEst, d_dSvec);

    //Compute the cost estimate
    nodalMetrics_->computeCost(Sfld_, cost_final_estimate_, d_dSvec);

    int eigenvalue_violation_count = this->countStepMatrixViolations(Sfld_);

    // Compute the average frobenius norm of the change
    Real avgStepNorm = this->computeAverageFrobeniusNorm(Sfld_);

    Real elemRequest = nodalMetrics_->computeRequestedElements(Sfld_);

    if (nodalMetrics_->comm()->rank() == 0)
    {
      //std::cout << "------------------------------------------" << std::endl;
      //std::cout << "MOESS TAO Summary" << std::endl;
      //std::cout << "------------------------------------------" << std::endl;
      std::cout << ">  Outer iteration "      << outer_iter << std::endl;
      std::cout << "    Eval count        : " << this->eval_count_ << std::endl;
      std::cout << "    Result            : " << TaoConvergedReasons[reason] <<std::endl;
      std::cout << "    Initial objective : " << f_init      << std::endl;
      std::cout << "    Final objective   : " << f_opt       << std::endl;
      std::cout << "    Cost              : " << cost_final_estimate_ << std::endl;
      std::cout << "    Elem Req.         : " << elemRequest << std::endl;
      std::cout << "    Max Step Penalty  : " << maxSpenalty << std::endl;
      std::cout << "    Eigen violations  : " << eigenvalue_violation_count << std::endl;
      std::cout << "    Avg Step FrobNorm : " << avgStepNorm    << std::endl;
      std::cout << "    Error Est.        : " << errEst         << std::endl;
      std::cout << "    Error Reduction   : " << errEst/error0_ << std::endl;
      std::cout << "------------------------------------------" << std::endl;
      //std::cout << std::endl;
    }

    Real prev_ICM  = ICM;

    constraint = cost_final_estimate_/targetCost_ - 1;

    Real newlam = lambda_ + rho_ * constraint;
    ICM = fabs(constraint);
    lambda_ = MIN(MAX(lam_min, newlam), lam_max);

    if (ICM > tau * prev_ICM)
      rho_ *= gam;

    total_eval_count += this->eval_count_;
    outer_iter++;
  }
  while (fabs(constraint) > 1e-6 && outer_iter < 10);

  this->eval_count_ = total_eval_count;

  if (nodalMetrics_->comm()->rank() == 0)
  {
    std::cout << "    Total eval count  : " << this->eval_count_<< std::endl;
    std::cout << "------------------------------------------" << std::endl;
  }

  objective_reduction_ratio_ = f_opt/f_init;

  nodalMetrics_->setNodalStepMatrix(Sfld_);

  TAO_STATUS( VecDestroy(&X) );
  TAO_STATUS( TaoDestroy(&tao) );
}

//---------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
MOESS_SANS_Optimizer<PhysDim,TopoDim,TAO>::~MOESS_SANS_Optimizer()
{
}

//---------------------------------------------------------------------------//
//The function that TAO calls to evaluate the objective function
template <class PhysDim, class TopoDim>
PetscErrorCode
MOESS_SANS_Optimizer<PhysDim,TopoDim,TAO>::
FormFunctionGradientAugLag(Tao tao, Vec X, PetscReal *f, Vec G, void *ctx)
{
  MOESS_SANS_Optimizer<PhysDim,TopoDim, TAO>* solver = reinterpret_cast<MOESS_SANS_Optimizer<PhysDim,TopoDim, TAO>*>(ctx);

  solver->computeObjectiveAugLag(X, f, G); //forward the call

  return 0;
}

#if 0
//---------------------------------------------------------------------------//
//The function that TAO calls to evaluate the objective function
template <class PhysDim, class TopoDim>
PetscErrorCode
MOESS_SANS_Optimizer<PhysDim,TopoDim,TAO>::
FormFunctionGradient(Tao tao, Vec X, PetscReal *f, Vec G, void *ctx)
{
  MOESS_SANS_Optimizer<PhysDim,TopoDim, TAO>* solver = reinterpret_cast<MOESS_SANS_Optimizer<PhysDim,TopoDim, TAO>*>(ctx);

  solver->computeObjective(X, f, G); //forward the call

  return 0;
}

//---------------------------------------------------------------------------//
//The function that TAO calls to evaluate the cost constraint
template <class PhysDim, class TopoDim>
PetscErrorCode
MOESS_SANS_Optimizer<PhysDim,TopoDim,TAO>::
FormEqualityConstraints(Tao tao, Vec X, Vec CI, void *ctx)
{
  MOESS_SANS_Optimizer<PhysDim,TopoDim, TAO>* solver = reinterpret_cast<MOESS_SANS_Optimizer<PhysDim,TopoDim, TAO>*>(ctx);

  PetscScalar *c;

  TAO_STATUS( VecGetArray(CI,&c) );

  // compute the constraint
  c[0] = solver->computeCostConstraint(X);

  TAO_STATUS( VecRestoreArray(CI,&c) );

  return 0;
}

//---------------------------------------------------------------------------//
//The function that TAO calls to evaluate the cost constraint jacboian
template <class PhysDim, class TopoDim>
PetscErrorCode
MOESS_SANS_Optimizer<PhysDim,TopoDim,TAO>::
FormEqualityJacobian(Tao tao, Vec X, Mat JE, Mat JEpre, void *ctx)
{
  MOESS_SANS_Optimizer<PhysDim,TopoDim, TAO>* solver = reinterpret_cast<MOESS_SANS_Optimizer<PhysDim,TopoDim, TAO>*>(ctx);

  PetscInt nLocal;
  TAO_STATUS( VecGetLocalSize(X,&nLocal) );
  std::vector<PetscInt> cols = linspace(0, nLocal-1);

  // compute the constraint jacobian
  solver->computeCostConstraintJacobian(X, 0, cols, JE);

  TAO_STATUS( MatAssemblyBegin(JE,MAT_FINAL_ASSEMBLY) );
  TAO_STATUS( MatAssemblyEnd(JE,MAT_FINAL_ASSEMBLY) );

  return 0;
}
#endif

//---------------------------------------------------------------------------//
//Routine that computes the objective function and its derivative wrt optimization variables
template <class PhysDim, class TopoDim>
void
MOESS_SANS_Optimizer<PhysDim,TopoDim,TAO>::
computeObjectiveAugLag(Vec X, PetscReal *f, Vec G)
{
  PetscScalar       *grad;
  const PetscScalar *x;
  PetscInt          nLocal;

  TAO_STATUS( VecGetLocalSize(X,&nLocal) );
  TAO_STATUS( VecGetArrayRead(X,&x) );
  TAO_STATUS( VecGetArray(G,&grad) );

  const int nNodePossessed = Sfld_.nDOFpossessed();
  SANS_ASSERT_MSG( nNodePossessed*MatrixSym::SIZE == nLocal,
                   "%d == %d", nNodePossessed*MatrixSym::SIZE, nLocal );

  //Populate step-matrices from the solution vector x
  for (int node = 0; node < nNodePossessed; node++)
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      Sfld_.DOF(node).value(ind) = x[node*MatrixSym::SIZE + ind];

  // synchronize ghost/zombie DOFs
  Sfld_.syncDOFs_MPI_Cached();

  // get the objective
  *f = BaseType::computeObjective(Sfld_, nLocal, grad);

  // add the constraint via augmented Lagrangian
  std::vector<Real> grad_const(nLocal);
  Real constraint = BaseType::computeCostConstraint(Sfld_, nLocal, grad_const.data());

  Real h = constraint + lambda_ / rho_;
  *f += 0.5 * rho_ * h*h;
  for (int j = 0; j < nLocal; ++j)
    grad[j] += (rho_ * h) * grad_const[j];

  TAO_STATUS( VecRestoreArrayRead(X,&x) );
  TAO_STATUS( VecRestoreArray(G,&grad) );
}

#if 0
//---------------------------------------------------------------------------//
//Routine that computes the objective function and its derivative wrt optimization variables
template <class PhysDim, class TopoDim>
void
MOESS_SANS_Optimizer<PhysDim,TopoDim,TAO>::
computeObjective(Vec X, PetscReal *f, Vec G)
{
  PetscScalar       *grad;
  const PetscScalar *x;
  PetscInt          nLocal;

  TAO_STATUS( VecGetLocalSize(X,&nLocal) );
  TAO_STATUS( VecGetArrayRead(X,&x) );
  TAO_STATUS( VecGetArray(G,&grad) );

  const int nNodePossessed = Sfld_.nDOFpossessed();
  SANS_ASSERT_MSG( nNodePossessed*MatrixSym::SIZE == nLocal,
                   "%d == %d", nNodePossessed*MatrixSym::SIZE, nLocal );

  //Populate step-matrices from the solution vector x
  for (int node = 0; node < nNodePossessed; node++)
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      Sfld_.DOF(node).value(ind) = x[node*MatrixSym::SIZE + ind];

  // synchronize ghost/zombie DOFs
  Sfld_.syncDOFs_MPI_Cached();

  *f = BaseType::computeObjective(Sfld_, nLocal, grad);

  TAO_STATUS( VecRestoreArrayRead(X,&x) );
  TAO_STATUS( VecRestoreArray(G,&grad) );
}

//---------------------------------------------------------------------------//
//Routine that computes the cost constraint and its derivative wrt optimization variables
//Constraint form: computeCostConstraint(x) == 0
template <class PhysDim, class TopoDim>
PetscReal
MOESS_SANS_Optimizer<PhysDim,TopoDim,TAO>::
computeCostConstraint(Vec X)
{
  const PetscScalar *x;
  PetscInt          nLocal;

  TAO_STATUS( VecGetLocalSize(X,&nLocal) );
  TAO_STATUS( VecGetArrayRead(X,&x) );

  const int nNodePossessed = Sfld_.nDOFpossessed();
  SANS_ASSERT_MSG( nNodePossessed*MatrixSym::SIZE == nLocal,
                   "%d == %d", nNodePossessed*MatrixSym::SIZE, nLocal );

  //Populate step-matrices from the solution vector x
  for (int node = 0; node < nNodePossessed; node++)
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      Sfld_.DOF(node).value(ind) = x[node*MatrixSym::SIZE + ind];

  // synchronize ghost/zombie DOFs
  Sfld_.syncDOFs_MPI_Cached();

  Real constraint = BaseType::computeCostConstraint(Sfld_, 0, nullptr);

  TAO_STATUS( VecRestoreArrayRead(X,&x) );

  return constraint;
}

//---------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
void
MOESS_SANS_Optimizer<PhysDim,TopoDim,TAO>::
computeCostConstraintJacobian(Vec X, PetscInt row, std::vector<PetscInt>& cols, Mat JE)
{
  const PetscScalar *x;
  PetscInt          nLocal;

  TAO_STATUS( VecGetLocalSize(X,&nLocal) );
  TAO_STATUS( VecGetArrayRead(X,&x) );

  const int nNodePossessed = Sfld_.nDOFpossessed();
  SANS_ASSERT_MSG( nNodePossessed*MatrixSym::SIZE == nLocal,
                   "%d == %d", nNodePossessed*MatrixSym::SIZE, nLocal );

  //Populate step-matrices from the solution vector x
  for (int node = 0; node < nNodePossessed; node++)
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      Sfld_.DOF(node).value(ind) = x[node*MatrixSym::SIZE + ind];

  // synchronize ghost/zombie DOFs
  Sfld_.syncDOFs_MPI_Cached();

  std::vector<PetscReal> grad(nLocal);

  BaseType::computeCostConstraint(Sfld_, grad.size(), grad.data());

  TAO_STATUS( MatSetValues(JE, 1, &row, cols.size(), cols.data(), grad.data(), INSERT_VALUES) );

  TAO_STATUS( VecRestoreArrayRead(X,&x) );
}
#endif

}
