// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef IMPLIEDMETRIC_OPTIMIZER_TAO_H_
#define IMPLIEDMETRIC_OPTIMIZER_TAO_H_

#include <string>
#include <vector>

#include <petsctao.h>

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"
#include "Topology/ElementTopology.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"

#include "Adaptation/MOESS/NodalMetrics.h"

namespace SANS
{

class TAO;

template <class PhysDim, class TopoDim, class Optimizer>
class ImpliedMetric_Optimizer;


template <class PhysDim, class TopoDim>
class ImpliedMetric_Optimizer<PhysDim,TopoDim,TAO>
{
public:
  static const int D = PhysDim::D;   // physical dimensions

  typedef DLA::MatrixSymS<D,Real> MatrixSym;

  typedef NodalMetrics<PhysDim,TopoDim> NodalMetricsType;
  typedef typename NodalMetricsType::ParamsType ParamsType;
  typedef typename NodalMetricsType::MatrixSymFieldType MatrixSymFieldType;

  ImpliedMetric_Optimizer( NodalMetricsType& nodalMetrics );
  ~ImpliedMetric_Optimizer();

  //The function that TAO calls to evaluate the objective function and it's gradient
  static PetscErrorCode FormFunctionGradient(Tao tao, Vec X, PetscReal *f, Vec G, void *ctx);

  const MatrixSymFieldType& logMfld() { return logMfld_; }
protected:

  //Combined objected function of complexity and edge length deviation
  void computeComplexityObjective(Vec X, PetscReal *f, Vec G);

  NodalMetricsType& nodalMetrics_; //Reference to an instance of the MOESS class so that we can access its members
  MatrixSymFieldType logMfld_;

  int eval_count_; //No. of objective function evaluations
};

}

#endif // IMPLIEDMETRIC_OPTIMIZER_TAO_H_
