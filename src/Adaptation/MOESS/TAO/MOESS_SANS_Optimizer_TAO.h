// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef MOESS_SANS_OPTIMIZER_TAO_H_
#define MOESS_SANS_OPTIMIZER_TAO_H_

#include <ostream>
#include <vector>
#include <memory> // std::shared_ptr

#include <petsctao.h>

#include "tools/SANSnumerics.h"     // Real
#include "Topology/Dimension.h"
#include "Topology/ElementTopology.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"

#include "Adaptation/MOESS/NodalMetrics.h"
#include "Adaptation/MOESS/ErrorModel.h"

#include "Adaptation/MOESS/MOESS_SANS_Optimizer_Base.h"

namespace SANS
{

class TAO;

template <class PhysDim, class TopoDim, class Optimizer>
class MOESS_SANS_Optimizer;

template <class PhysDim, class TopoDim>
class MOESS_SANS_Optimizer<PhysDim,TopoDim,TAO> : public MOESS_SANS_Optimizer_Base<PhysDim,TopoDim>
{
public:
  typedef MOESS_SANS_Optimizer_Base<PhysDim,TopoDim> BaseType;

  static const int D = PhysDim::D;   // physical dimensions

  typedef NodalMetrics<PhysDim,TopoDim> NodalMetricsType;
  typedef typename NodalMetricsType::ParamsType ParamsType;
  typedef typename NodalMetricsType::MatrixSymFieldType MatrixSymFieldType;

  typedef ErrorModel<PhysDim,TopoDim> ErrorModelType;

  typedef DLA::MatrixSymS<D,Real> MatrixSym;

  MOESS_SANS_Optimizer( const Real targetCost, const Real h_domain_max,
                        std::shared_ptr<ErrorModelType>& errorModel,
                        const XField<PhysDim,TopoDim>& xfld_linear,
                        const std::vector<int>& cellgroup_list,
                        const SolverInterfaceBase<PhysDim,TopoDim>& problem,
                        const PyDict& paramsDict );
  ~MOESS_SANS_Optimizer();

  //Computes the objective function and its derivative wrt optimization variables
  static PetscErrorCode FormFunctionGradientAugLag(Tao tao, Vec X, PetscReal *f, Vec G, void *ctx);

#if 0
  //The function that TAO calls to evaluate the objective function and gradient
  static PetscErrorCode FormFunctionGradient(Tao tao, Vec X, PetscReal *f, Vec G, void *ctx);

  //The function that TAO calls to evaluate the cost constraint and gradient
  static PetscErrorCode FormEqualityConstraints(Tao tao, Vec X, Vec CI, void *ctx);
  static PetscErrorCode FormEqualityJacobian(Tao tao, Vec X, Mat JI, Mat JIpre,  void *ctx);
#endif

protected:
  //Routine that computes the objective function and its derivative wrt optimization variables
  void computeObjectiveAugLag(Vec X, PetscReal *f, Vec G);

#if 0
  //Computes the objective function and its derivative wrt optimization variables
  void computeObjective(Vec X, PetscReal *f, Vec G);

  //Computes the cost constraint and its derivative wrt optimization variables
  PetscReal computeCostConstraint(Vec X);
  void computeCostConstraintJacobian(Vec X, PetscInt row, std::vector<PetscInt>& cols, Mat JI);
#endif

  MatrixSymFieldType Sfld_;

  // augmented lagrangian
  Real lambda_, rho_;

  using BaseType::errorModel_;
  using BaseType::nodalMetrics_;
  using BaseType::h_coarsen_factors_;
  using BaseType::h_refine_factor_;
  using BaseType::targetCost_;
  using BaseType::error0_;
  using BaseType::cost_initial_estimate_;
  using BaseType::cost_final_estimate_;
  using BaseType::objective_reduction_ratio_;
};

}

#endif /* MOESS_SANS_OPTIMIZER_TAO_H_ */
