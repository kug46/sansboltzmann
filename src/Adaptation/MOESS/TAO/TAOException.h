// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef TAOEXCEPTION_H
#define TAOEXCEPTION_H

#include "tools/SANSException.h"

#include <petsctao.h>

namespace SANS
{

//=============================================================================
struct TAOException : public SANSException
{
  explicit TAOException(const PetscErrorCode status);

  virtual ~TAOException() throw() {}
};

#define TAO_STATUS( API_call ) \
  {  \
    PetscErrorCode _status_from_api_call_ = API_call; \
    if ( unlikely(_status_from_api_call_ != 0) ) \
      BOOST_THROW_EXCEPTION( SANS::TAOException(_status_from_api_call_) ); \
  }

} // namespace SANS

#endif // TAOEXCEPTION_H
