// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(IMPLIEDMETRIC_OPTIMIZER_TAO_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "../ImpliedMetric_Optimizer_TAO.h"
#include "../TAOException.h"

#include <vector>

#include <petscvec.h>

#include "Adaptation/MOESS/MOESS.h"

#include "Surreal/SurrealS.h"
#include "tools/smoothmath.h"

#include "Meshing/Metric/MetricOps.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

template <class PhysDim, class TopoDim>
ImpliedMetric_Optimizer<PhysDim,TopoDim,TAO>::
ImpliedMetric_Optimizer( NodalMetricsType& nodalMetrics )
: nodalMetrics_(nodalMetrics),
  logMfld_(nodalMetrics.xfld_linear_, 1, BasisFunctionCategory_Lagrange, nodalMetrics.cellgroup_list_),
  eval_count_(0)
{
  Tao tao; // the tao optimization object

  TAO_STATUS( TaoCreate(*nodalMetrics.comm(), &tao) );

  // TAOBQNLS will replace TAOBLMVM in newer version of PETSc
#if PETSC_VERSION_LT(3,10,0)
  TAO_STATUS( TaoSetType(tao, TAOBLMVM) );
#else
  TAO_STATUS( TaoSetType(tao, TAOBQNLS) );
#endif

  TAO_STATUS( TaoSetObjectiveAndGradientRoutine(tao, FormFunctionGradient, reinterpret_cast<void*>(this)) );

  //Termination conditions
  //Real xtol_rel = MOESS_base_.paramsDict_.get(ParamsType::params.ImpliedMetric_XTol_Rel);
  PetscReal ftol_rel = nodalMetrics.paramsDict_.get(ParamsType::params.ImpliedMetric_FTol_Rel);
  PetscInt max_eval  = nodalMetrics.paramsDict_.get(ParamsType::params.ImpliedMetric_MaxEval);

  TAO_STATUS( TaoSetTolerances(tao,1e-10,1e-10,ftol_rel) );

  //stop when the maximum number of function evaluations is reached
  TAO_STATUS( TaoSetMaximumFunctionEvaluations(tao, max_eval) );
  TAO_STATUS( TaoSetMaximumIterations(tao, 10*max_eval) );
#if 0
  // we are done if the objective function falls below this
  opt_.set_stopval(1e-10);

  //stop when every parameter changes by less than the tolerance multiplied by the absolute value of the parameter.
  opt_.set_xtol_rel(xtol_rel);

  //stop when the objective function value changes by less than the tolerance multiplied by the absolute value of the function value
  opt_.set_ftol_rel(ftol_rel);
#endif

  const int nNode = logMfld_.nDOF();
  const int nNodePossessed = logMfld_.nDOFpossessed();
  const int nLocal = nNodePossessed*MatrixSym::SIZE;

  Vec X, G;

  TAO_STATUS( VecCreateMPI(*nodalMetrics.comm(), nLocal, PETSC_DETERMINE, &X) );

  //Initialize solution vector the log of the metric
  {
    PetscScalar *x;
    TAO_STATUS( VecGetArray(X,&x) );

    for (int node = 0; node < nNode; node++)
    {
      MatrixSym logM = log(nodalMetrics.implied_metric_.DOF(node));

      logMfld_.DOF(node) = logM;

      // do give set anything beyond possessed DOF to TAO
      if (node >= nNodePossessed) continue;

      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
        x[node*MatrixSym::SIZE + ind] = logM.value(ind);
    }
    TAO_STATUS( VecRestoreArray(X,&x) );
    TAO_STATUS( TaoSetInitialVector(tao, X) );
  }

  std::vector<MatrixSym> d_dlogM;

  //Compute the global complexity and its derivatives
  Real complexity = 0.0;
  nodalMetrics.computeComplexity(logMfld_, complexity, d_dlogM);

  //Compute the global error estimate and its derivatives
  Real EdgeLenDev = 0.0;
  nodalMetrics.computeEdgeLengthDeviation(logMfld_, EdgeLenDev, d_dlogM);

  // get the initial objective value
  PetscReal f_init;
  TAO_STATUS( VecDuplicate(X, &G) );
  computeComplexityObjective(X, &f_init, G);
  TAO_STATUS( VecDestroy(&G) );

  if (nodalMetrics.comm()->rank() == 0)
  {
    std::cout << std::scientific << std::setprecision(4) << std::endl;
    std::cout << "------------------------------------------" << std::endl;
    std::cout << "Starting TAO Implied Metric Optimization" << std::endl;
    std::cout << "     Target Complexity      : " << nodalMetrics.complexity_initial_true_ << std::endl;
    std::cout << "     Implied Complexity     : " << complexity << std::endl;
    std::cout << "     Edge Length Deviation  : " << EdgeLenDev << std::endl;
    std::cout << "     Initial Objective      : " << f_init << std::endl;
    std::cout << "------------------------------------------" << std::endl;
  }

  eval_count_ = 0;

  // perform the optimization
  TAO_STATUS( TaoSolve(tao) );

  PetscReal f_opt, gnorm, cnorm, xdiff;
  PetscInt its;
  TaoConvergedReason reason;

  // get the final result
  TAO_STATUS( TaoGetSolutionStatus(tao, &its, &f_opt, &gnorm, &cnorm, &xdiff, &reason) );

  // Retrieve the final log metrics
  const PetscScalar *x;
  TAO_STATUS( VecGetArrayRead(X,&x) );
  for (int node = 0; node < nNodePossessed; node++)
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      logMfld_.DOF(node).value(ind) = x[node*MatrixSym::SIZE + ind];

  TAO_STATUS( VecRestoreArrayRead(X,&x) );

  // synchronize ghost/zombie DOFs
  logMfld_.syncDOFs_MPI_Cached();

  //Update the implied metric with the optimized step matrix
  for (int node = 0; node < nNode; node++)
    nodalMetrics.implied_metric_.DOF(node) = exp(logMfld_.DOF(node));

  TAO_STATUS( VecDestroy(&X) );

  //Compute the global complexity
  complexity = 0;
  nodalMetrics.computeComplexity(logMfld_, complexity, d_dlogM);

  //Compute the global edge length deviation
  EdgeLenDev = 0;
  nodalMetrics.computeEdgeLengthDeviation(logMfld_, EdgeLenDev, d_dlogM);

  if (nodalMetrics.comm()->rank() == 0)
  {
    std::cout << "     Eval count             : " << eval_count_ << std::endl;
    std::cout << "     Iterations             : " << its << std::endl;
    std::cout << "     Result                 : " << TaoConvergedReasons[reason] << std::endl;
    std::cout << "     Target Complexity      : " << nodalMetrics.complexity_initial_true_ << std::endl;
    std::cout << "     Implied Complexity     : " << complexity << std::endl;
    std::cout << "     Edge Len Deviation     : " << EdgeLenDev << std::endl;
    std::cout << "     Objective              : " << f_opt << std::endl;
    std::cout << "     ||g(X)||               : " << gnorm << std::endl;
    std::cout << "------------------------------------------" << std::endl;
    std::cout << std::endl;
    std::cout << std::fixed;
  }

  TAO_STATUS( TaoDestroy(&tao) );
}

//---------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
ImpliedMetric_Optimizer<PhysDim,TopoDim,TAO>::~ImpliedMetric_Optimizer()
{
}

//---------------------------------------------------------------------------//
//The function that TAO calls to evaluate the gradation constraint
template <class PhysDim, class TopoDim>
PetscErrorCode
ImpliedMetric_Optimizer<PhysDim,TopoDim,TAO>::
FormFunctionGradient(Tao tao, Vec X, PetscReal *f, Vec G, void *ctx)
{
  ImpliedMetric_Optimizer<PhysDim,TopoDim,TAO>* solver = reinterpret_cast<ImpliedMetric_Optimizer<PhysDim,TopoDim,TAO>*>(ctx);

  solver->computeComplexityObjective(X, f, G); //forward the call

  return 0;
}

//---------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
void
ImpliedMetric_Optimizer<PhysDim,TopoDim,TAO>::
computeComplexityObjective(Vec X, PetscReal *f, Vec G)
{
  eval_count_++;

  PetscScalar       *grad;
  const PetscScalar *x;
  PetscInt          nLocal;

  TAO_STATUS( VecGetLocalSize(X,&nLocal) );
  TAO_STATUS( VecGetArrayRead(X,&x) );
  TAO_STATUS( VecGetArray(G,&grad) );

  const int nNode = logMfld_.nDOF();
  const int nNodePossessed = logMfld_.nDOFpossessed();
  SANS_ASSERT_MSG( nNodePossessed*MatrixSym::SIZE == nLocal,
                   "%d == %d", nNodePossessed*MatrixSym::SIZE, nLocal );

  //Populate log-matrices from the solution vector x
  for (int node = 0; node < nNodePossessed; node++)
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      logMfld_.DOF(node).value(ind) = x[node*MatrixSym::SIZE + ind];

  // synchronize ghost/zombie DOFs
  logMfld_.syncDOFs_MPI_Cached();

  Real complexity = 0.0;
  std::vector<MatrixSym> dcomplexity_dlogM(grad == nullptr ? 0 : nNode, 0.0);

  const Real complexity_true = nodalMetrics_.complexity_initial_true_;

  //Compute the global complexity estimate and its derivatives
  nodalMetrics_.computeComplexity(logMfld_, complexity, dcomplexity_dlogM);
  complexity /= complexity_true;

  Real EdgeLenDev = 0.0;
  std::vector<MatrixSym> dEdgeLenDev_dlogM(grad == nullptr ? 0 : nNode, 0.0);

  //Compute the edge length deviation from the allowable lengths
  nodalMetrics_.computeEdgeLengthDeviation(logMfld_, EdgeLenDev, dEdgeLenDev_dlogM);

  // Complexity ideally matches the initial complexity
  *f = pow( complexity - 1, 2.0 ) + EdgeLenDev;

  //Populate grad vector from dError_dSvec
  for (int node = 0; node < nNodePossessed; node++)
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      grad[node*MatrixSym::SIZE + ind] =
          2.0*(complexity - 1)*dcomplexity_dlogM[node].value(ind)/complexity_true +
                               dEdgeLenDev_dlogM[node].value(ind);

  //std::cout << "eval_count = " << eval_count_ << " | obj = " << obj;

  TAO_STATUS( VecRestoreArrayRead(X,&x) );
  TAO_STATUS( VecRestoreArray(G,&grad) );
}

}
