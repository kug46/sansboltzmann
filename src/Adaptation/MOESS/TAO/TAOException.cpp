// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "TAOException.h"

#include <petscerror.h>

namespace SANS
{

//=============================================================================
TAOException::TAOException( const PetscErrorCode status )
{
  errString += "TAO Error\n";

  const char *error_message = nullptr;
  PetscErrorMessage(status, &error_message, NULL);

  if (error_message == nullptr)
    errString += "No TAO error message available";
  else
    errString += error_message;
}

} // namespace SANS
