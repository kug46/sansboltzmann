// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef MOESS_H_
#define MOESS_H_

//#ifdef SANS_PETSC
//#include <petscmat.h>
//#endif

#include <vector>
#include <memory> // std::shared_ptr

#include "tools/SANSnumerics.h"     // Real
#include "Topology/Dimension.h"
#include "Topology/ElementTopology.h"

#include "Field/Field.h"
#include "Field/XField.h"

#include "Field/Field_NodalView.h"

#include "MOESSParams.h"
#include "SolverInterfaceBase.h"
#include "ErrorModel.h"
#include "NodalMetrics.h"

namespace SANS
{

template <class PhysDim, class TopoDim, class Optimizer>
class MOESS_SANS_Optimizer;

template <class PhysDim, class TopoDim, class Optimizer>
class MOESS_PX_Optimizer;

template <class PhysDim, class TopoDim, class Optimizer>
class MOESS_Gradation_Optimizer;

template <class PhysDim, class TopoDim, class Optimizer>
class ImpliedMetric_Optimizer;

template <class PhysDim, class TopoDim>
class SolverInterface_MetricError;

template <class PhysDim, class TopoDim>
class MOESS
{
public:

  typedef PhysDim PhysD;
  typedef TopoDim TopoD;
  static const int D = PhysDim::D;   // physical dimensions

  typedef DLA::VectorS<D,Real> VectorX;

  typedef DLA::MatrixSymS<D,Real> MatrixSym;
  typedef DLA::VectorS<D,Real> Vector;
  typedef DLA::MatrixS<D,D,Real> Matrix;
  typedef DLA::VectorS<MatrixSym::SIZE, Real> ArrayMatSym; //Data-type to represent symmetric matrix as vector

  typedef std::vector<std::vector<MatrixSym>> ElementalMetricVectorType;

  typedef Field_CG_Cell<PhysDim, TopoDim, Real> ScalarFieldType_Nodal;
  typedef Field_CG_Cell<PhysDim, TopoDim, DLA::VectorS<D, Real>> VectorFieldType_Nodal;
  typedef Field_CG_Cell<PhysDim, TopoDim, MatrixSym> MatrixSymFieldType;
  typedef Field_CG_Cell<PhysDim, TopoDim, ArrayMatSym> MatrixSymFieldType_Nodal;
  typedef Field_DG_Cell<PhysDim, TopoDim, ArrayMatSym> MatrixSymFieldType_Elemental;

  typedef MOESS<PhysDim, TopoDim> MOESSClass;
  typedef MOESSParams ParamsType;

  template<class, class, class> friend class MOESS_SANS_Optimizer;
  template<class, class, class> friend class MOESS_PX_Optimizer;
  template<class, class, class> friend class MOESS_Gradation_Optimizer;
  template<class, class, class> friend class ImpliedMetric_Optimizer;

  MOESS(const XField<PhysDim,TopoDim>& xfld_linear,
        const XField<PhysDim,TopoDim>& xfld_curved,
        const std::vector<int>& cellgroup_list,
        const SolverInterfaceBase<PhysDim,TopoDim>& problem,
        const Real targetCost,
        const PyDict& paramsDict);

  //Use this constructor if we only have linear meshes
  MOESS(const XField<PhysDim,TopoDim>& xfld_linear,
        const std::vector<int>& cellgroup_list,
        const SolverInterfaceBase<PhysDim,TopoDim>& problem,
        const Real targetCost,
        const PyDict& paramsDict);

  ~MOESS() {}

  void computeError(const MatrixSymFieldType& Sfld, Real& Error, std::vector<MatrixSym>& dError_dS);

  void getNodalMetricRequestField(MatrixSymFieldType& metric_fld);
  void getNodalImpliedMetricField(MatrixSymFieldType& metric_fld);
  void getStepMatrixField(MatrixSymFieldType_Nodal& step_fld);
  void getStepMatrixEigField(VectorFieldType_Nodal& eig_fld);
  void getEdgeLengthField(ScalarFieldType_Nodal& edgeLen_fld);
  void getEdgeLengths(std::vector<Real>& lengths);
  void getRateMatrixField(MatrixSymFieldType_Elemental& rate_fld);
  void getRateMatrixField(MatrixSymFieldType_Nodal& rate_fld);

  void computeMetricError( const MatrixSymFieldType& targetMetric ,
                           const MatrixSymFieldType& Sfld ,
                           Real& Error , std::vector<MatrixSym>& dError_dS );

  void printAdaptInfo(std::fstream& f, const int& adapt_iter, const Real& errInd, const Real& errEst, const Real& errTrue, const Real& output);
  void printOptimizationInfo(std::fstream& f);

#if 0 //def SANS_PETSC
  void computeErrorHessian(const std::vector<MatrixSym>& Svec, Mat H);
#endif



//  Real getObjectiveReduction()  { return objective_reduction_ratio_; }
//
//  Real getCost_InitialTrue()    { return cost_initial_true_;    }
//  Real getCost_InitialEstimate(){ return cost_initial_estimate_;}
//  Real getCost_FinalEstimate()  { return cost_final_estimate_;  }
//
//  const std::vector<Real>& getObjectiveHistory() { return objective_history_; }
//  const std::vector<Real>& getObjectiveGradNormHistory() { return objective_gradnorm_history_; }
//
//  const std::vector<Real>& getCostConstraintHistory() { return cost_constraint_history_; }
//  const std::vector<Real>& getCostConstraintGradNormHistory() { return cost_constraint_gradnorm_history_; }
//
//  const std::vector<Real>& getStepConstraintHistory() { return step_constraint_history_; }
//  const std::vector<Real>& getStepConstraintGradNormHistory() { return step_constraint_gradnorm_history_; }
//
//  const std::vector<Real>& getGradationConstraintHistory() { return gradation_constraint_history_; }

  // communicator accessor
  std::shared_ptr<mpi::communicator> comm() const { return xfld_linear_.comm(); }

        NodalMetrics<PhysDim,TopoDim>& nodalMetrics()       { return *nodalMetrics_; }
  const NodalMetrics<PhysDim,TopoDim>& nodalMetrics() const { return *nodalMetrics_; }

protected:

  //maximum edge length in the domain - estimated from bounding box
  void computeMaxDomainSize();

  template<class Topology>
  void computeError_cellgroup(const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
                              const int cellGroupGlobal, const std::vector<MatrixSym>& Svec,
                              Real& Error, std::vector<MatrixSym>& dError_dSvec);

  template<class Topology>
  void getRateMatrix_cellgroup(const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
                               const int cellGroupGlobal, MatrixSymFieldType_Elemental& ratefld);

  const XField<PhysDim, TopoDim>& xfld_linear_;
  const XField<PhysDim, TopoDim>& xfld_curved_; //reference to the curved mesh (if any), to be used for local solves
  const std::vector<int>& cellgroup_list_;

  const SolverInterfaceBase<PhysDim,TopoDim>& problem_;
  const SpaceType spaceType_;
  const Real targetCost_;
  const PyDict& paramsDict_;

  const ParamsType::VerbosityOptions::ExtractType verbosity_;

  Real h_domain_max_; // maximum domain size

  int  cost_initial_true_ = 0;
  int  nElemInitial_ = 0;
  Real cost_initial_estimate_ = 0.0;
  Real cost_final_estimate_ = 0.0;

  Real complexity_initial_true_ = 0.0;

  int eval_count_ = 0;

  std::vector<Real> objective_history_; //History of objective function evaluations
  std::vector<Real> objective_gradnorm_history_; //History of the norm of the objective function gradient
  Real objective_reduction_ratio_ = 0.0; //Reduction in objective function value as a result of optimization = (final_obj/initial_obj)

  std::vector<Real> cost_constraint_history_; //History of cost constraint evaluations
  std::vector<Real> cost_constraint_gradnorm_history_; //History of cost constraint evaluations

  std::vector<Real> step_constraint_history_; //History of step matrix constraint evaluations
  std::vector<Real> step_constraint_gradnorm_history_; //History of step matrix constraint evaluations

  std::vector<Real> gradation_constraint_history_; //History of gradation constraint evaluations

  std::shared_ptr<ErrorModel<PhysDim,TopoDim>> errorModel_;
  std::shared_ptr<NodalMetrics<PhysDim,TopoDim>> nodalMetrics_;
};

}

#endif /* MOESS_H_ */
