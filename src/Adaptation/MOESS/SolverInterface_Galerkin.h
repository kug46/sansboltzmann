// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOLVERINTERFACE_GALERKIN_H_
#define SOLVERINTERFACE_GALERKIN_H_

#include <ostream>
#include <vector>

// Output Functional Stuff that needs to be removed
#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/JacobianFunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/FunctionalBoundaryTrace_Dispatch_Galerkin.h"
#include "Discretization/Galerkin/JacobianFunctionalBoundaryTrace_Dispatch_Galerkin.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#include "MPI/communicator_fwd.h"
#endif

// What will eventually be the true Solver Interface. This is a stop gap solution until output functionals work
#include "Adaptation/MOESS/SolverInterface.h"

#include "ErrorEstimate/Galerkin/ErrorEstimate_StrongForm_Galerkin.h"
#include "ErrorEstimate/Galerkin/ErrorEstimate_Galerkin_Stabilized.h"
#include "ErrorEstimate/Galerkin/ErrorEstimateNodal_Galerkin.h"
#include "ErrorEstimate/Galerkin/OutputCorrection_Galerkin_Stabilized.h"
#include "ErrorEstimate/DG/ErrorEstimate_DGAdvective.h"

#ifdef BOUNDARYOUTPUT
// HACK: These boundary integrands are needed for the boundary output at the moment.
//       They should be removed in the future.
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_sansLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Flux_mitState_Nitsche.h"
#endif

#include "Discretization/Galerkin/IntegrandCell_Project.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Project.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Local_Galerkin_Stabilized.h"

#include "tools/make_unique.h"

namespace SANS
{

template<class SolutionClass, class AlgebraicEqSet, class OutputIntegrandType>
class SolverInterface_Galerkin : public SolverInterface<SolutionClass, AlgebraicEqSet, OutputIntegrandType>
{
public:
  typedef SolverInterface<SolutionClass, AlgebraicEqSet, OutputIntegrandType> BaseType;

  typedef typename AlgebraicEqSet::PhysDim PhysDim;
  typedef typename AlgebraicEqSet::TopoDim TopoDim;

  const int D = PhysDim::D;
  typedef XField<PhysDim, TopoDim> XFieldType;

  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef typename SolutionClass::ParamFieldBuilderType ParamFieldBuilderType;

  typedef typename AlgebraicEqSet::NDPDEClass NDPDEClass;
  typedef typename NDPDEClass::template ArrayQ<Real> PDEArrayQ;

  typedef AlgebraicEqSet AlgEqnSet_Global;

  typedef typename LocalEquationSet<AlgEqnSet_Global>::type AlgEqnSet_Local;
  typedef typename AlgEqnSet_Global::FieldBundle FieldBundle;
  typedef typename AlgEqnSet_Global::ErrorEstimateClass ErrorEstimateClass;
  typedef typename AlgEqnSet_Global::IntegrandCellClass IntegrandCellClass;
  typedef typename AlgEqnSet_Local::FieldBundle_Local FieldBundle_Local;

  typedef SolverInterface_Galerkin SolverInterfaceClass;

  typedef typename OutputIntegrandType::template ArrayJ<Real> ArrayJ;

  typedef typename AlgEqnSet_Global::ArrayQ ArrayQ;

  template< class... BCArgs >
  SolverInterface_Galerkin(SolutionClass& sol, const ResidualNormType& resNormType, std::vector<Real>& tol, const int quadOrder,
                        const std::vector<int>& cellgroups, const std::vector<int>& interiortracegroups,
                        PyDict& BCList, const std::map< std::string, std::vector<int> >& BCBoundaryGroups,
                        const PyDict& NonlinearSolverDict, const PyDict& AdjLinearSolverDict,
                        const OutputIntegrandType& fcnOutput, BCArgs&... args )
    : BaseType( sol, resNormType, tol, quadOrder, cellgroups, interiortracegroups, BCList, BCBoundaryGroups, NonlinearSolverDict,
                AdjLinearSolverDict, fcnOutput, args... ),
      adjoint_wo_subtraction( xfld_, sol_.adjoint.order, sol_.adjoint, sol_.mitlg_boundarygroups ),
      adjoint_p( xfld_, sol_.primal.order, sol_.primal, sol_.mitlg_boundarygroups )
  {
    /*
    errorIndicator_ = 0.0;
    errorEstimate_ = 0.0;

    //Loop over each cellgroup and find the no. of DOF per cell and solution order for that cellgroup
    nDOFperCell_.resize((int) xfld_.nCellGroups(), -1);
    orderVec_.resize((int) xfld_.nCellGroups(), -1);
    for_each_CellGroup<TopoDim>::apply( CellwiseOperations(cellgroup_list_, *this), xfld_);
    */
  }

  virtual ~SolverInterface_Galerkin() {};

  virtual void solveGlobalPrimalProblem() override;
  virtual void solveGlobalAdjointProblem() override;
  virtual LocalSolveStatus solveLocalProblem(const XField_Local_Base<PhysDim, TopoDim>& local_xfld,
                                             std::vector<Real>& local_error) const override;

  const Field_CG_Cell<PhysDim,TopoDim,PDEArrayQ>& getAdjField() const { return adjoint_wo_subtraction.qfld; }
  const Field_CG_Cell<PhysDim,TopoDim,PDEArrayQ>& getPAdjField() const { return adjoint_p.qfld; }

  // a specialist local problem, derived from the traditional local problem, but re-defining the estimation process
  class LocalProblem_Galerkin : public BaseType::LocalProblem
  {
    public:
    typedef typename SolverInterface<SolutionClass,AlgebraicEqSet,OutputIntegrandType>::LocalProblem LocalProblemBaseType;
    LocalProblem_Galerkin( const SolverInterfaceClass& interface,
                           const XField_Local_Base<PhysDim,TopoDim>& xfld_local, LocalSolveStatus& status ) :
    LocalProblemBaseType(interface,xfld_local,status),
    construct_timer_(),
    interface_(interface),
    up_local_est_fld_(SANS::make_unique<Field_Local<Field_DG_Cell<PhysDim,TopoDim,Real>>>
        ( xfld_local, interface_.pErrorEstimate_->efld_DG_nodal(), 1, BasisFunctionCategory_Lagrange))
    {
      status.time_constructor += construct_timer_.elapsed();
    }

    // // construct a local estimate field
    // std::unique_ptr<Field_DG_Cell<PhysDim,TopoDim,Real>> up_local_est_fld
    //   = SANS::make_unique<Field_Local<Field_DG_Cell<PhysDim,TopoDim,Real>>>
    //       ( xfld_local, interface_.pErrorEstimate_->efld_DG_nodal(), 1, BasisFunctionCategory_Lagrange);


    virtual ~LocalProblem_Galerkin() {};

    virtual void estimateLocalError( const XField_Local_Base<PhysDim,TopoDim>& local_xfld,
                                     FieldBundle_Local& primal, FieldBundle_Local& adjoint ) override;

    // virtual std::vector<Real> getLocalError() override
    // {
    //   // Solve
    //   this->solveLocalPrimalProblem(xfld_local_, primal_local_);
    //
    //   // Estimate
    //   estimateLocalError( xfld_local_, primal_local_, adjoint_local_ );
    //
    //   return local_error_;
    // };


    protected:
    timer construct_timer_;

    // need an actual reference here because we need to access protected members, and friend status only extends to nested classes
    // rather than to derived classes nested classes.
    // Thus can access protected members of a SolverInterface_Galerkin class, but none from a SolverInterface class
    const SolverInterfaceClass& interface_;
    std::unique_ptr<Field_DG_Cell<PhysDim,TopoDim,Real>> up_local_est_fld_; // for transfering the global estimate
    using LocalProblemBaseType::xfld_local_;
    using LocalProblemBaseType::status_;
    using LocalProblemBaseType::quadrule_;
    using LocalProblemBaseType::maxIter;
    using LocalProblemBaseType::BCList_local_;
    using LocalProblemBaseType::BCBoundaryGroups_local_;
    using LocalProblemBaseType::local_cellGroups_;
    using LocalProblemBaseType::local_interiorTraceGroups_;
    using LocalProblemBaseType::active_local_boundaries_;
    using LocalProblemBaseType::primal_local_;
    using LocalProblemBaseType::adjoint_local_;
    using LocalProblemBaseType::pliftedQuantityfld_local_;
    using LocalProblemBaseType::parambuilder_;
    using LocalProblemBaseType::stol;
    using LocalProblemBaseType::local_error_;
  };

  // function to fill the residual field pointer based on the solve
  void prepareResidualField();

protected:

  using BaseType::sol_; //class containing all the solution data
  using BaseType::xfld_; // mesh
  using BaseType::resNormType_;
  using BaseType::tol_;
  using BaseType::quadOrder_;
  using BaseType::cellgroup_list_;
  using BaseType::interiortracegroup_list_;
  using BaseType::BCList_;
  using BaseType::BCBoundaryGroups_;
  using BaseType::NonlinearSolverDict_;
  using BaseType::AdjLinearSolverDict_;
  using BaseType::verbosity_;

  using BaseType::AlgEqnSet_; // AlgebraicEquationSet

  using BaseType::pErrorEstimate_;

  using BaseType::errorArray_; //Array of elemental error estimates (index:[cellgroup][elem])
  using BaseType::fcnOutput_;
  using BaseType::errorIndicator_;
  using BaseType::errorEstimate_;
  using BaseType::outputJ_; //output functional evaluated at current solution

  using BaseType::nDOFperCell_; //indexing: [global cellgrp] - number of degrees of freedom per cell
  using BaseType::orderVec_; //indexing: [global cellgrp] - solution order of each cellgroup

  FieldBundle adjoint_wo_subtraction; // Adjoint without removing the p solution, useful for debugging
  FieldBundle adjoint_p; // Adjoint in space p, useful for debugging
};


template<class SolutionClass, class AlgebraicEqSet, class OutputIntegrandType>
void
SolverInterface_Galerkin<SolutionClass, AlgebraicEqSet, OutputIntegrandType>::
prepareResidualField()
{
  // The residual field needs to be stored in a DG field
  // declare the pointer for the residual field here. It is actually owned by the FieldBundle class

  //transfer solution to DG field so that residual fcn can be re-used
  // Make sure that this is an exact copy of the above field, need to be sure that the dofs match up later?
  Field_DG_Cell<PhysDim,TopoDim,ArrayQ> qfld_dg(xfld_,sol_.primal.order, sol_.primal.basis_cell,cellgroup_list_);

  sol_.primal.qfld.projectTo(qfld_dg);

  sol_.primal.up_resfld = SANS::make_unique<Field_DG_Cell<PhysDim,TopoDim,ArrayQ>>(qfld_dg, FieldCopy());
  *sol_.primal.up_resfld = 0;

  // TO DO: Call the residual to fill this field up
  AlgEqnSet_.fillResidualField( qfld_dg, sol_.primal.up_resfld );

  //SYNC RESIDUAL FIELD
  sol_.primal.up_resfld->syncDOFs_MPI_noCache();
//  output_Tecplot( *(sol_.primal.up_resfld), "tmp/rsdfld.plt" );

  // Once the residual field is filled/set by the call to this function
  // if a local field is created using the global field bundle, it will also transfer the DG field
}


template<class SolutionClass, class AlgebraicEqSet, class OutputIntegrandType>
void
SolverInterface_Galerkin<SolutionClass, AlgebraicEqSet, OutputIntegrandType>::
solveGlobalPrimalProblem()
{
  timer clock;

  this->template solveGlobalPrimalContinuation<typename NDPDEClass::Temporal>();

  if (get<-1>(xfld_).comm()->rank() == 0 && verbosity_ )
    std::cout << "Primal solve time: " << std::fixed << std::setprecision(3) << clock.elapsed() << "s" << std::endl;

  // evaluate output function
  outputJ_ = 0.0;

  QuadratureOrder quadrule(xfld_, quadOrder_);

#ifndef BOUNDARYOUTPUT
  IntegrateCellGroups<TopoDim>::integrate( FunctionalCell_Galerkin( fcnOutput_, outputJ_ ),
                                           sol_.paramfld, sol_.primal.qfld,
                                           quadrule.cellOrders.data(), quadrule.cellOrders.size() );
#else
  //AlgEqnSet_.dispatchBC().dispatch_sansFT(
  //  FunctionalBoundaryTrace_Dispatch_Galerkin(fcnOutput_, sol_.paramfld, sol_.primal.qfld,
  //                          quadrule.boundaryTraceOrders.data(), quadrule.boundaryTraceOrders.size(), outputJ_ ) );
  AlgEqnSet_.dispatchBC().dispatch(
      FunctionalBoundaryTrace_FieldTrace_Dispatch_Galerkin(fcnOutput_, sol_.paramfld, sol_.primal.qfld, sol_.primal.lgfld,
                                                           quadrule.boundaryTraceOrders.data(),
                                                           quadrule.boundaryTraceOrders.size(), outputJ_ ),
      FunctionalBoundaryTrace_Dispatch_Galerkin(fcnOutput_, sol_.paramfld, sol_.primal.qfld,
                                                quadrule.boundaryTraceOrders.data(),
                                                quadrule.boundaryTraceOrders.size(), outputJ_ ) );

  // FunctionalCell_Galerkin already has an all_reduce built in!
  #ifdef SANS_MPI
    outputJ_ = boost::mpi::all_reduce( *get<-1>(xfld_).comm(), outputJ_, std::plus<Real>() );
  #endif
#endif

#if !defined(WHOLEPATCH) && !defined(INNERPATCH)
  //compute global dg residual field for local solves later
  prepareResidualField();
#endif
}

template<class SolutionClass, class AlgebraicEqSet, class OutputIntegrandType>
void
SolverInterface_Galerkin<SolutionClass, AlgebraicEqSet, OutputIntegrandType>::
solveGlobalAdjointProblem()
{
  typedef typename AlgEqnSet_Global::SystemVector SystemVectorClass;
  typedef typename AlgEqnSet_Global::SystemMatrix SystemMatrixClass;

  const int iPDE = AlgEqnSet_.iPDE;

  QuadratureOrder quadrule(xfld_, quadOrder_);

  // Field bundle for prolongating the p adjoint for subtraction
  FieldBundle adj_prolong( xfld_, sol_.adjoint.order, sol_.adjoint,
                           sol_.mitlg_boundarygroups );

#define USE_PSOLVE 1 // Whether to use a P solve or to use a projection
#if USE_PSOLVE
  // The estimates used in CG need a p solution subtracted, because the test space doesn't contain constants
  timer clock_p;

  {
    if (get<-1>(xfld_).comm()->rank() == 0 && verbosity_ )
      std::cout<< "using solve for p adjoint" << std::endl;
    // Using a P solve, works but seems wasteful/projection seems a little more stable
    // Should re-use the final primal jacobian
    SystemVectorClass rhs_p( AlgEqnSet_.vectorEqSize() );
    rhs_p = 0;

  #ifndef BOUNDARYOUTPUT
      IntegrateCellGroups<TopoDim>::integrate( JacobianFunctionalCell_Galerkin( fcnOutput_, rhs_p(iPDE) ),
                                               sol_.paramfld, sol_.primal.qfld,
                                               quadrule.cellOrders.data(), quadrule.cellOrders.size() );
  #else
      typedef SurrealS<NDPDEClass::N> SurrealClass;
      const int iBC = AlgEqnSet_.iBC;

      AlgEqnSet_.dispatchBC().dispatch(
          JacobianFunctionalBoundaryTrace_FieldTrace_Dispatch_Galerkin<SurrealClass>(fcnOutput_, sol_.paramfld, sol_.primal.qfld, sol_.primal.lgfld,
                                                                                     quadrule.boundaryTraceOrders.data(),
                                                                                     quadrule.boundaryTraceOrders.size(), rhs_p(iPDE), rhs_p(iBC) ),
          JacobianFunctionalBoundaryTrace_Dispatch_Galerkin<SurrealClass>(fcnOutput_, sol_.paramfld, sol_.primal.qfld,
                                                                          quadrule.boundaryTraceOrders.data(),
                                                                          quadrule.boundaryTraceOrders.size(), rhs_p(iPDE) ) );
  #endif

    // Adjoint solver
    SLA::LinearSolver< SystemMatrixClass > solver_p( AdjLinearSolverDict_, AlgEqnSet_, SLA::TransposeSolve );

    SystemVectorClass adj_p( AlgEqnSet_.vectorStateSize() );
    adj_p = 0; // set initial guess

    solver_p.solve( rhs_p, adj_p );

    // update solution
    AlgEqnSet_.setSolutionField( adj_p, adjoint_p.qfld, adjoint_p.lgfld );

    adjoint_p.qfld.syncDOFs_MPI_noCache();
    adjoint_p.lgfld.syncDOFs_MPI_noCache();
  }

  // prolongate to the P+1 basis
  adjoint_p.qfld.projectTo(adj_prolong.qfld);
  adjoint_p.lgfld.projectTo(adj_prolong.lgfld);

  if (get<-1>(xfld_).comm()->rank() == 0 && verbosity_ )
    std::cout << "Adjoint order=" << sol_.primal.order << " solve time: "
              << std::fixed << std::setprecision(3) << clock_p.elapsed() << "s" << std::endl;

#endif



  timer clock_Pp1;

  //Create primal solutions in richer solution space
  FieldBundle primal_pro(xfld_, sol_.adjoint.order,
                         sol_.primal, sol_.mitlg_boundarygroups );

  // Prolongate the primal solution to the richer solution space
  sol_.primal.projectTo(primal_pro);

  // Create param fields with prolongated primal solution
  ParamFieldBuilderType parambuilder(sol_.paramfld, primal_pro.qfld, sol_.parambuilder.dict);

  //Create AlgebraicEquationSets for solutions in richer space
  AlgEqnSet_Global AlgEqnSet_Pro(parambuilder.fld, primal_pro, sol_.pliftedQuantityfld,
                                 sol_.pde, sol_.disc, quadrule, resNormType_, tol_,
                                 cellgroup_list_, interiortracegroup_list_,
                                 BCList_, BCBoundaryGroups_);

  // Functional integral
  SystemVectorClass rhs(AlgEqnSet_Pro.vectorEqSize());
  rhs = 0;

  #ifndef BOUNDARYOUTPUT
    IntegrateCellGroups<TopoDim>::integrate( JacobianFunctionalCell_Galerkin( fcnOutput_, rhs(iPDE) ),
                                             parambuilder.fld, primal_pro.qfld,
                                             quadrule.cellOrders.data(), quadrule.cellOrders.size() );
  #else
    typedef SurrealS<NDPDEClass::N> SurrealClass;
    const int iBC = AlgEqnSet_.iBC;

    AlgEqnSet_Pro.dispatchBC().dispatch(
        JacobianFunctionalBoundaryTrace_FieldTrace_Dispatch_Galerkin<SurrealClass>(fcnOutput_, parambuilder.fld, primal_pro.qfld, primal_pro.lgfld,
                                                                                   quadrule.boundaryTraceOrders.data(),
                                                                                   quadrule.boundaryTraceOrders.size(), rhs(iPDE), rhs(iBC) ),
        JacobianFunctionalBoundaryTrace_Dispatch_Galerkin<SurrealClass>(fcnOutput_, parambuilder.fld, primal_pro.qfld,
                                                                        quadrule.boundaryTraceOrders.data(),
                                                                        quadrule.boundaryTraceOrders.size(), rhs(iPDE) ) );
  #endif

  // adjoint solver
  SLA::LinearSolver< SystemMatrixClass > solver(AdjLinearSolverDict_, AlgEqnSet_Pro, SLA::TransposeSolve);

  SystemVectorClass adj(AlgEqnSet_Pro.vectorStateSize());

#if USE_PSOLVE
  // set the initial guess as the prolongated adjoint solution
  AlgEqnSet_Pro.fillSystemVector(adj_prolong.qfld, adj_prolong.lgfld, adj);
#else
  adj = 0.0; // set the initial guess to 0
#endif

  solver.solve(rhs, adj);

  // update solution
  AlgEqnSet_Pro.setSolutionField(adj, sol_.adjoint.qfld, sol_.adjoint.lgfld );

  sol_.adjoint.qfld.syncDOFs_MPI_noCache();
  sol_.adjoint.lgfld.syncDOFs_MPI_noCache();

  if (get<-1>(xfld_).comm()->rank() == 0 && verbosity_ )
    std::cout << "Adjoint order=" << sol_.adjoint.order << " solve time: "
              << std::fixed << std::setprecision(3) << clock_Pp1.elapsed() << "s" << std::endl;

#if !USE_PSOLVE

  timer clock_p;
  // Use an L2 Project to get the p adjoint!
  if (get<-1>(xfld_).comm()->rank() == 0 && verbosity_ )
    std::cout<< "using projection for p adjoint" << std::endl;
  // Using projection
  typedef IntegrandCell_Project<NDPDEClass> IntegrandCellProject;

  typedef FieldTuple<Field<PhysDim,TopoDim,ArrayQ>, XFieldType, TupleClass<>> FieldTupleType_Q;
  typedef AlgebraicEquationSet_Project< FieldTupleType_Q, IntegrandCellProject, TopoDim, AlgEqSetTraits_Sparse> AlgEqnSetProject_Q;

  const FieldTupleType_Q& fieldTuple_q = ( (sol_.adjoint.qfld ), xfld_);

  adjoint_p.qfld = 0.0;

  IntegrandCellProject fcnCell( cellgroup_list_ );
  const std::array<Real,1> tol_Q  = { {tol_[iPDE]} };
  AlgEqnSetProject_Q AlgEqnSet_Project_Q(   fieldTuple_q,  adjoint_p.qfld, fcnCell, quadrule, tol_Q );

  typedef typename AlgEqnSetProject_Q::SystemVector  SystemVectorClass_Q;
  typedef typename AlgEqnSetProject_Q::SystemMatrix  SystemMatrixClass_Q;

  SystemVectorClass_Q adj_p(AlgEqnSet_Project_Q.vectorStateSize());
  SystemVectorClass_Q rhs_p(AlgEqnSet_Project_Q.vectorEqSize());

  // QField
  AlgEqnSet_Project_Q.fillSystemVector(adj_p);
  rhs_p = 0;
  AlgEqnSet_Project_Q.residual(rhs_p);

  SLA::LinearSolver< SystemMatrixClass_Q > solver_p_Q(AdjLinearSolverDict_, AlgEqnSet_Project_Q );
  SLA::LinearSolveStatus solverStatus = solver_p_Q.solve( rhs_p, adj_p );

  SANS_ASSERT_MSG( solverStatus.success == true, "P Projection Solve Failed!" ); // check that it actually solved

  // Update the projection solution
  AlgEqnSet_Project_Q.setSolutionField( adj_p );

  // Send solution across processors
  adjoint_p.qfld.syncDOFs_MPI_noCache();

  // zero the Lagrange multipliers for now.... This is a cheap trick, but mitLG don't work anywhere else either
  adj_prolong.lgfld = 0.0;

  adjoint_p.qfld.projectTo(adj_prolong.qfld);

  if (get<-1>(xfld_).comm()->rank() == 0 && verbosity_ )
    std::cout << "Adjoint order=" << sol_.primal.order << " projection solve time: "
              << std::fixed << std::setprecision(3) << clock_p.elapsed() << "s" << std::endl;
#endif

  // subtract off the prolongated adjoint from the higher-order adjoint
  for (int i = 0; i < adj_prolong.qfld.nDOF(); i++)
  {
    adjoint_wo_subtraction.qfld.DOF(i) = sol_.adjoint.qfld.DOF(i);
    sol_.adjoint.qfld.DOF(i) = sol_.adjoint.qfld.DOF(i) - adj_prolong.qfld.DOF(i);
  }

//  output_Tecplot(sol_.adjoint.qfld, "tmp/adj_delta.plt" );

}

// template<class SolutionClass, class AlgebraicEqSet, class OutputIntegrandType>
// LocalSolveStatus
// SolverInterface_Galerkin<SolutionClass, AlgebraicEqSet, OutputIntegrandType>::
// solveLocalProblem(const XField_Local_Base<PhysDim, TopoDim>& xfld_local, std::vector<Real>& local_error) const
// {
//   // std::cout<< "Using the CG local solve call " << std::endl;
//   const int maxIter = 10;
//   LocalSolveStatus status(false, maxIter);
//
//   LocalProblem_Galerkin localProblem( *this, xfld_local, status );
//   local_error = localProblem.getLocalError();
//   return status;
// }


template<class SolutionClass, class AlgebraicEqSet, class OutputIntegrandType>
LocalSolveStatus
SolverInterface_Galerkin<SolutionClass, AlgebraicEqSet, OutputIntegrandType>::
solveLocalProblem(const XField_Local_Base<PhysDim, TopoDim>& xfld_local, std::vector<Real>& local_error) const
{
  // std::cout<< "Using the standard local solve call " << std::endl;
  const int maxIter = 10;
  LocalSolveStatus status(false, maxIter);

  LocalProblem_Galerkin localProblem( *this, xfld_local, status );
  local_error = localProblem.getLocalError();
  return status;
}


// separating off the estimation part
template<class SolutionClass, class AlgebraicEqSet, class OutputIntegrandType>
void
SolverInterface_Galerkin<SolutionClass, AlgebraicEqSet, OutputIntegrandType>::LocalProblem_Galerkin::
estimateLocalError( const XField_Local_Base<PhysDim, TopoDim>& xfld_local,
                    FieldBundle_Local& primal_local, FieldBundle_Local& adjoint_local )
{
  //Compute local error estimates
  timer clock_errest;
  // std::cout<< "Using the virtual CG local estimate call " << std::endl;

  // Only estimate error for cellgroup 0! this is the split group
  ErrorEstimateClass ErrorEstimate(parambuilder_.fld, primal_local, adjoint_local,
                                   up_local_est_fld_, pliftedQuantityfld_local_,
                                   interface_.sol_.pde, interface_.sol_.disc, quadrule_,
                                   local_cellGroups_, local_interiorTraceGroups_,
                                   BCList_local_, BCBoundaryGroups_local_);

  ErrorEstimate.getLocalSolveErrorIndicator(local_error_); //Accumulate errors only from cellgroup 0 - (main cells)

  status_.time_errest += clock_errest.elapsed();
}


} // namespace SANS



#endif /* SOLVERINTERFACE_GALERKIN_H_ */
