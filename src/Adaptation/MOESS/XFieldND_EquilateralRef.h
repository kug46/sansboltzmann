// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELDND_EQUILATERALREF
#define XFIELDND_EQUILATERALREF

#include "Topology/ElementTopology.h"

#include "Field/XField.h"

#include "MPI/communicator_fwd.h"

namespace SANS
{

/*
   A unit grid that consists of one equilateral reference element within a single group
*/

template <class PhysDim, class TopoDim, class Topology>
class XFieldND_EquilateralRef : public XField<PhysDim,TopoDim>
{
public:
  typedef XField<PhysDim,TopoDim> BaseType;

  XFieldND_EquilateralRef(mpi::communicator& comm_local);

  using BaseType::DOF;

protected:
  using BaseType::resizeDOF;
  using BaseType::resizeInteriorTraceGroups;
  using BaseType::resizeBoundaryTraceGroups;
  using BaseType::resizeCellGroups;
  using BaseType::checkGrid;

  using BaseType::nDOF_;
  using BaseType::DOF_;

  using BaseType::nElem_;
  using BaseType::cellGroups_;
  using BaseType::boundaryTraceGroups_;
};

}

#endif //XFIELDND_EQUILATERALREF
