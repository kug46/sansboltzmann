// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef MOESS_SANS_OPTIMIZER_MMA_H_
#define MOESS_SANS_OPTIMIZER_MMA_H_

#include <ostream>
#include <vector>
#include <memory> // std::shared_ptr

#include "tools/SANSnumerics.h"     // Real
#include "Topology/Dimension.h"
#include "Topology/ElementTopology.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"

#include "Adaptation/MOESS/NodalMetrics.h"
#include "Adaptation/MOESS/ErrorModel.h"
#include "Adaptation/MOESS/MOESS_SANS_Optimizer_Base.h"

namespace SANS
{

class MMA;

template <class PhysDim, class TopoDim, class Optimizer>
class MOESS_SANS_Optimizer;

template <class PhysDim, class TopoDim>
class MOESS_SANS_Optimizer<PhysDim,TopoDim,MMA> : public MOESS_SANS_Optimizer_Base<PhysDim,TopoDim>
{
public:
  typedef MOESS_SANS_Optimizer_Base<PhysDim,TopoDim> BaseType;

  static const int D = PhysDim::D;   // physical dimensions

  typedef NodalMetrics<PhysDim,TopoDim> NodalMetricsType;
  typedef typename NodalMetricsType::ParamsType ParamsType;
  typedef typename NodalMetricsType::MatrixSymFieldType MatrixSymFieldType;

  typedef ErrorModel<PhysDim,TopoDim> ErrorModelType;

  typedef DLA::MatrixSymS<D,Real> MatrixSym;

  MOESS_SANS_Optimizer( const Real targetCost, const Real h_domain_max,
                        std::shared_ptr<ErrorModelType>& errorModel,
                        const XField<PhysDim,TopoDim>& xfld_linear,
                        const std::vector<int>& cellgroup_list,
                        const SolverInterfaceBase<PhysDim,TopoDim>& problem,
                        const PyDict& paramsDict );
  ~MOESS_SANS_Optimizer();

protected:
  //Use MMA stolen from NLopt with TAO to solve the dual problem
  void optimizeMMA(const Real h_domain_max, const SpaceType space, const PyDict& paramsDict, Real& f_init, Real& f_opt);

  //Computes the MMA dual objective function and its derivative wrt optimization variables
  void computeMMADual(const Real y, Real& f, Real& grad);

  // Solves the MMA dual problem wia MMA again!
  void solveMMADual(const Real dual_ub, Real &y, Real& f);

  // computes both the objective and constraint as a single functions
  void computeObjectiveAndCostConstraint(const std::vector<Real>& x,
                                         Real& f, std::vector<Real>& dfdx,
                                         Real& fc, std::vector<Real>& dfcdx);

  MatrixSymFieldType Sfld_;

  using BaseType::errorModel_;
  using BaseType::nodalMetrics_;
  using BaseType::h_coarsen_factors_;
  using BaseType::h_refine_factor_;
  using BaseType::targetCost_;
  using BaseType::error0_;
  using BaseType::cost_initial_estimate_;
  using BaseType::cost_final_estimate_;
  using BaseType::objective_reduction_ratio_;

  // stolen shamelessly from mma.c in NLopt
  // m is the number of constrains, which is hardcoded to 1 here
  /* function for MMA's dual solution of the approximate problem */
  struct dual_data
  {
    int count = 0; /* evaluation count, incremented each call */
    int nLocal;
    const Real *x, *dfdx; /* arrays of length n */
    const Real *lb, *ub, *sigma; /* arrays of length n */
    const Real *dfcdx; /* m-by-n array of fc gradients */
    Real fval, rho; /* must be set on input */
    Real fcval, rhoc; /* arrays of length m */
    Real *xcur; /* array of length n, output each time */
    Real gval, wval, gcval; /* output each time (array length m) */
  };
  dual_data dd_;
};

}

#endif /* MOESS_SANS_OPTIMIZER_MMA_H_ */
