// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(MOESS_SANS_OPTIMIZER_MMA_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "../MOESS_SANS_Optimizer_MMA.h"

#include "Adaptation/MOESS/MOESS.h"

#include <vector>
#include <limits>
#include <array>

#include "Field/XField.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_FrobNorm.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Trace.h"

#include "Surreal/SurrealS.h"
#include "tools/smoothmath.h"
#include "tools/linspace.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#include <boost/mpi/collectives/broadcast.hpp>
#include <boost/mpi/datatype.hpp>
#include <boost/serialization/access.hpp>
#include "MPI/boost_serialization_array.h"

namespace SANS
{
  struct MMADualData;
}

namespace boost
{
namespace mpi
{
// This allows Boost.MPI to avoid extraneous copy operations
// Only works when the datatype is of fixed size (no pointers or dynamically sized objects)
template<>
struct is_mpi_datatype< SANS::MMADualData > : mpl::true_ {};
} // namespace mpi
} // namespace boost
#endif

//#define MMA_DEBUG

namespace SANS
{

// stolen shamelessly from mma.c in NLopt
/* magic minimum value for rho in MMA ... the 2002 paper says it should
   be a "fixed, strictly positive `small' number, e.g. 1e-5"
   ... grrr, I hate these magic numbers, which seem like they
   should depend on the objective function in some way ... in particular,
   note that rho is dimensionful (= dimensions of objective function) */
#define MMA_RHOMIN 1e-5


//---------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
MOESS_SANS_Optimizer<PhysDim,TopoDim,MMA>::
MOESS_SANS_Optimizer( const Real targetCost, const Real h_domain_max,
                      std::shared_ptr<ErrorModelType>& errorModel,
                      const XField<PhysDim,TopoDim>& xfld_linear,
                      const std::vector<int>& cellgroup_list,
                      const SolverInterfaceBase<PhysDim,TopoDim>& problem,
                      const PyDict& paramsDict )
  : BaseType(targetCost, errorModel),
    Sfld_(xfld_linear, 1, BasisFunctionCategory_Lagrange, cellgroup_list)
{
  if (xfld_linear.comm()->rank() == 0)
  {
    std::cout << std::endl;
    std::cout << "SANS-MMA optimizing metrics" << std::endl;
    std::cout << std::endl;
  }

  // construct the nodal implied metric field
  nodalMetrics_ = std::make_shared<NodalMetricsType>(xfld_linear, cellgroup_list,
                                                     problem, paramsDict);

  const int nNode = Sfld_.nDOF();

  h_coarsen_factors_.resize(nNode);
  h_refine_factor_ = sqrt(2); //paramsDict.get(ParamsType::params.hRefineFactorMax);
  Real beta;
  if (problem.spaceType() == SpaceType::Continuous)
    beta = 1.1; // Must be consistent with ErrorModel::computeNodalError
  else
    beta = 1.5; // Must be consistent with ErrorModel::computeError_cellgroup
  h_refine_factor_ = pow(h_refine_factor_, sqrt(D)*beta);

  nodalMetrics_->initializeCostStepMatrices(h_domain_max, h_refine_factor_,
                                            targetCost_, cost_initial_estimate_,
                                            Sfld_, h_coarsen_factors_);

  std::vector<MatrixSym> d_dSvec;

  //Compute the global error estimate as the scaling factor
  errorModel_->computeError(Sfld_, error0_, d_dSvec);

  // just so we don't divide by zero
  if (error0_ == 0) error0_ = std::numeric_limits<Real>::epsilon();

  // perform the optimization
  Real f_init, f_opt;
  optimizeMMA(h_domain_max, problem.spaceType(), paramsDict, f_init, f_opt);

  const int nNodePossessed = Sfld_.nDOFpossessed();

  // report results
  Real SnormSq = 0.;
  Real trSq = 0.;
  int maxFrobNode = 0;
  int maxTrNode = 0;
  for (int node = 0; node < nNodePossessed; node++)
  {
    Real SnormSq_temp = DLA::FrobNormSq(Sfld_.DOF(node));
    if ( SnormSq_temp > SnormSq )
    {
      SnormSq = SnormSq_temp;
      maxFrobNode = node;
    }
    Real trSq_temp = pow(DLA::tr(Sfld_.DOF(node)),2);
    if ( trSq_temp > trSq )
    {
      trSq = trSq_temp;
      maxTrNode = node;
    }
  }

#ifdef SANS_MPI
  SnormSq = boost::mpi::all_reduce(*nodalMetrics_->comm(), SnormSq, boost::mpi::maximum<Real>());
#endif

  Real frobNormSqTargetOuter = pow(sqrt(D)*2.0*log(h_refine_factor_),2.0);
  Real frobNormSqTargetInner = pow(        2.0*log(h_refine_factor_),2.0);
  Real frobNormSqTargetDel = D == 1 ? 1e-2 : frobNormSqTargetOuter/frobNormSqTargetInner - 1;

  // calculate the penalty for the max norm node
  Real maxSpenalty;
  this->frobNormPenalty( Sfld_.DOF(maxFrobNode), frobNormSqTargetInner, frobNormSqTargetDel, maxSpenalty );

  // calculate the penalty for the trace square
  Real maxTrPenalty;
  this->tracePenalty( Sfld_.DOF(maxTrNode), frobNormSqTargetInner, frobNormSqTargetDel, maxTrPenalty );

  //Compute the global error estimate
  Real errEst = 0;
  errorModel_->computeError(Sfld_, errEst, d_dSvec);

  //Compute the cost estimate
  nodalMetrics_->computeCost(Sfld_, cost_final_estimate_, d_dSvec);

  Real elemRequst = nodalMetrics_->computeRequestedElements(Sfld_);

  int eigenvalue_violation_count = this->countStepMatrixViolations(Sfld_);

  // Compute the average frobenius norm of the change
  Real avgStepNorm = this->computeAverageFrobeniusNorm(Sfld_);

  if (nodalMetrics_->comm()->rank() == 0)
  {
    std::cout << "MOESS SANS-MMA Summary" << std::endl;
    std::cout << "------------------------------------------" << std::endl;
    std::cout << "    Eval count        : " << this->eval_count_ << std::endl;
    std::cout << "    Initial objective : " << f_init      << std::endl;
    std::cout << "    Final objective   : " << f_opt       << std::endl;
    std::cout << "    Cost              : " << cost_final_estimate_ << std::endl;
    std::cout << "    Elem Req.         : " << elemRequst << std::endl;
    std::cout << "    Max Step Penalty  : " << maxSpenalty << std::endl;
    std::cout << "    Max Tr Penalty    : " << maxTrPenalty << std::endl;
    std::cout << "    Avg Step FrobNorm : " << avgStepNorm    << std::endl;
    std::cout << "    Eigen violations  : " << eigenvalue_violation_count << std::endl;
    std::cout << "    Error Est.        : " << errEst         << std::endl;
    std::cout << "    Error Reduction   : " << errEst/error0_ << std::endl;
    std::cout << "------------------------------------------" << std::endl;
    //std::cout << std::endl;
  }

  objective_reduction_ratio_ = f_opt/f_init;

  nodalMetrics_->setNodalStepMatrix(Sfld_);
}

//---------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
MOESS_SANS_Optimizer<PhysDim,TopoDim,MMA>::~MOESS_SANS_Optimizer()
{
}

//---------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
void
MOESS_SANS_Optimizer<PhysDim,TopoDim,MMA>::
optimizeMMA(const Real h_domain_max, const SpaceType space, const PyDict& paramsDict, Real& f_init, Real& f_opt)
{
  const int comm_rank = nodalMetrics_->comm()->rank();

  // constraint tolerance
  Real reltol = 1e-7;
  Real fc_tol = 0;
  Real minf_max = 1e-8;

  //const int nNode = Sfld_.nDOF();
  const int nNodePossessed = Sfld_.nDOFpossessed();
  const int nLocal = nNodePossessed*MatrixSym::SIZE;

  std::vector<Real> xcur(nLocal);

  //Initialize solution vector
  for (int node = 0; node < nNodePossessed; node++)
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      xcur[node*MatrixSym::SIZE + ind] = Sfld_.DOF(node).value(ind);

  std::vector<Real> x(xcur);

  Real y = 0;
  Real dual_ub = std::numeric_limits<Real>::max();

  {
  int nDim = nLocal;
#ifdef SANS_MPI
  int nDim_Global = 0;
  boost::mpi::reduce(*nodalMetrics_->comm(), nDim, nDim_Global, std::plus<int>(), 0);
  nDim = nDim_Global;
#endif

  if (comm_rank == 0)
  {
    std::cout << std::endl;
    std::cout << "------------------------------------------" << std::endl;
    std::cout << "Starting SANS-MMA Optimization"             << std::endl;
    std::cout << "------------------------------------------" << std::endl;
    std::cout << "    Variables         : " << nDim           << std::endl;
    std::cout << "------------------------------------------" << std::endl;
    //std::cout << std::endl;
  }
  }


  Real minf = std::numeric_limits<Real>::max();
  int k = 0;
  double infeasibility;

  std::vector<Real> sigma(nLocal);
  std::vector<Real> lb(nLocal), ub(nLocal);

  for (int node = 0; node < nNodePossessed; node++)
  {
    //Set bounds on the step matrix at this node
    Real lower_bound = -2.0*log(h_coarsen_factors_[node]); //max coarsening factor = h_refine_factor_limited
    Real upper_bound = +2.0*log(h_refine_factor_);         //max refinement factor = h_refine_factor_limited

    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
    {
      lb[node*MatrixSym::SIZE + ind] = lower_bound;
      ub[node*MatrixSym::SIZE + ind] = upper_bound;
    }
  } //loop over nodes

  for (int j = 0; j < nLocal; ++j)
    sigma[j] = 0.5 * (ub[j] - lb[j]);

  std::vector<Real> xprev(nLocal), xprevprev(nLocal);

  bool feasible = true; infeasibility = 0;

  Real fval_cur, fcval_cur;
  std::vector<Real> dfdx_cur(nLocal);
  std::vector<Real> dfcdx_cur(nLocal);

  computeObjectiveAndCostConstraint(xcur,
                                    fval_cur,  dfdx_cur,
                                    fcval_cur, dfcdx_cur);
  f_init = fval_cur;
  std::vector<Real> dfdx(dfdx_cur);
  std::vector<Real> dfcdx(dfcdx_cur);


  dd_.nLocal = nLocal;
  dd_.fval   = fval_cur;
  dd_.fcval  = fcval_cur;
  dd_.x      = x.data();
  dd_.lb     = lb.data();
  dd_.ub     = ub.data();
  dd_.sigma  = sigma.data();
  dd_.dfdx   = dfdx.data();
  dd_.dfcdx  = dfcdx.data();
  dd_.rho    = 1.0;
  dd_.rhoc   = 1.0;
  dd_.xcur   = xcur.data();
  dd_.gcval  = 0;
  dd_.count  = 0;

#ifdef MMA_DEBUG
  {
    boost::mpi::broadcast(*nodalMetrics_->comm(), dd_.fval, 0);
    boost::mpi::broadcast(*nodalMetrics_->comm(), dd_.fcval, 0);
    boost::mpi::broadcast(*nodalMetrics_->comm(), dd_.rho, 0);
    boost::mpi::broadcast(*nodalMetrics_->comm(), dd_.rhoc, 0);

    Real f, grad;
    computeMMADual(y, f, grad);

    //if (nodalMetrics_->comm()->rank() == 0)
    {
      printf("nLocal = %d, fval_cur=%g, fcval_cur=%g\n",
             nLocal, fval_cur, fcval_cur);
      printf("Dual f=%g, grad=%g\n", f, grad);
      printf("Dual dd_.gval=%g, dd_.wval=%g dd_.gcval=%g\n", dd_.gval, dd_.wval, dd_.gcval);
    }
  }
#endif

  if (comm_rank == 0)
  {
    feasible = dd_.fcval <= 0;
    if (dd_.fcval > infeasibility) infeasibility = dd_.fcval;

    /* For non-feasible initial points, set a finite (large)
       upper-bound on the dual variables.  What this means is that,
       if no feasible solution is found from the dual problem, it
       will minimize the dual objective with the unfeasible
       constraint weighted by 1e40 -- basically, minimizing the
       unfeasible constraint until it becomes feasible or until we at
       least obtain a step towards a feasible point.

       Svanberg suggested a different approach in his 1987 paper, basically
       introducing additional penalty variables for unfeasible constraints,
       but this is easier to implement and at least as efficient. */
    if ( !feasible )
      dual_ub = 1e40;
  }

  bool outer_done = false;
  while (1) /* outer iterations */
  {
    Real fprev = fval_cur;
    if (feasible && minf < minf_max) outer_done = true;
    if (outer_done) break;

    if (++k > 1) xprevprev = xprev;
    xprev = xcur;

    while (1) /* inner iterations */
    {

#ifdef MMA_DEBUG
      //if (nodalMetrics_->comm()->rank() == 0)
      {
      printf("MMA solving dual\n");
      }
#endif

#ifdef SANS_MPI
      {
        std::array<Real,2> sync{{dd_.rho, dd_.rhoc}};
        boost::mpi::broadcast(*nodalMetrics_->comm(), sync, 0);
        dd_.rho  = sync[0];
        dd_.rhoc = sync[1];
      }
#endif

      /* solve dual problem */
      Real min_dual;
      dd_.count = 0;
      solveMMADual(dual_ub, y, min_dual);

#ifdef MMA_DEBUG
      //if (nodalMetrics_->comm()->rank() == 0)
      {
      printf("MMA dual converged in %d iterations to g=%g:\n",
             dd_.count, dd_.gval);
      printf("    MMA y[0]=%g, gc[0]=%g\n",
             y, dd_.gcval);
      }
#endif

      computeObjectiveAndCostConstraint(xcur,
                                        fval_cur,  dfdx_cur,
                                        fcval_cur, dfcdx_cur);

#ifdef MMA_DEBUG
      //if (nodalMetrics_->comm()->rank() == 0)
      {
      printf("MMA  fval_cur=%g, fcval_cur=%g:\n",
             fval_cur, fcval_cur);
      }
#endif


      Real infeasibility_cur = 0.0;
      bool feasible_cur = true;
      bool new_infeasible_constraint = false;
      bool inner_done = false;
      bool save_cur = false;

      if (comm_rank == 0)
      {
        inner_done = dd_.gval >= fval_cur;
        feasible_cur = feasible_cur && (fcval_cur <= fc_tol);
        inner_done = inner_done && (dd_.gcval >= fcval_cur);

        if (fcval_cur > 0)
          new_infeasible_constraint = true;
        if (fcval_cur > infeasibility_cur)
          infeasibility_cur = fcval_cur;

        save_cur = (fval_cur < minf && (inner_done || feasible_cur || !feasible))
                    || (!feasible && infeasibility_cur < infeasibility);
      }


#ifdef SANS_MPI
      boost::mpi::broadcast(*nodalMetrics_->comm(), save_cur, 0);
#endif
      if (save_cur)
      {
#ifdef MMA_DEBUG
        if (nodalMetrics_->comm()->rank() == 0)
        {
        if (!feasible_cur)
          printf("MMA - using infeasible point?\n");
        }
#endif
        dd_.fval  = minf = fval_cur;
        dd_.fcval = fcval_cur;
        x         = xcur;
        dfdx      = dfdx_cur;
        dfcdx     = dfcdx_cur;
        infeasibility = infeasibility_cur;

        /* once we have reached a feasible solution, the
           algorithm should never make the solution infeasible
           again (if inner_done), although the constraints may
           be violated slightly by rounding errors etc. so we
           must be a little careful about checking feasibility */

        if (infeasibility_cur == 0)
        {
          if (!feasible) /* reset upper bounds to infin. */
          {
            dual_ub = std::numeric_limits<Real>::max();
          }
          feasible = true;
        }
        else if (new_infeasible_constraint) feasible = false;

      }
      if (feasible && minf < minf_max)
      {
        inner_done = true;
        outer_done = true;
      }

      if (this->eval_count_ >= 2000 )
      {
        inner_done = true;
        outer_done = true;
      }

#ifdef SANS_MPI
      boost::mpi::broadcast(*nodalMetrics_->comm(), inner_done, 0);
#endif
      if (inner_done) break;

      if (comm_rank == 0)
      {
        if (fval_cur > dd_.gval)
          dd_.rho  = MIN(10*dd_.rho , 1.1 * (dd_.rho  + (fval_cur-dd_.gval) / dd_.wval));

        if (fcval_cur > dd_.gcval)
          dd_.rhoc = MIN(10*dd_.rhoc, 1.1 * (dd_.rhoc + (fcval_cur-dd_.gcval) / dd_.wval));
      }

#ifdef MMA_DEBUG
      if (nodalMetrics_->comm()->rank() == 0)
      {
      printf("MMA inner iteration: rho -> %g\n", dd_.rho);
      printf("                 MMA rhoc[0] -> %g\n", dd_.rhoc);
      printf("                 dd_.gcval   -> %g\n", dd_.gcval);
      printf("                 fcval_cur   -> %g\n", fcval_cur);
      printf("                 fabs(fcur - fprev) -> %g\n",fabs(fval_cur - fprev));
      }
#endif
    }

    // relative tolerance check
    if (comm_rank == 0)
      if ( fabs(fval_cur - fprev) < reltol * (fabs(fval_cur) + fabs(fprev)) * 0.5 || (reltol > 0 && fval_cur == fprev) )
        outer_done = true;

    //if (nlopt_stop_x(stop, xcur, xprev))
    //  ret = NLOPT_XTOL_REACHED;

#ifdef SANS_MPI
      boost::mpi::broadcast(*nodalMetrics_->comm(), outer_done, 0);
#endif
    if (outer_done) break;

    if (comm_rank == 0)
    {
      /* update rho and sigma for iteration k+1 */
      dd_.rho  = MAX(0.1 * dd_.rho , MMA_RHOMIN);
      dd_.rhoc = MAX(0.1 * dd_.rhoc, MMA_RHOMIN);
    }

#ifdef MMA_DEBUG
    //if (nodalMetrics_->comm()->rank() == 0)
    {
    printf("MMA outer iteration: rho -> %g\n", dd_.rho);
    printf("                 MMA rhoc[0] -> %g\n", dd_.rhoc);
    }
#endif

    if (k > 1)
    {
      for (int j = 0; j < nLocal; ++j)
      {
        Real dx2 = (xcur[j]-xprev[j]) * (xprev[j]-xprevprev[j]);
        Real gam = dx2 < 0 ? 0.7 : (dx2 > 0 ? 1.2 : 1);
        sigma[j] *= gam;
        sigma[j] = MIN(sigma[j],   10*(ub[j]-lb[j]));
        sigma[j] = MAX(sigma[j], 0.01*(ub[j]-lb[j]));
      }

#ifdef MMA_DEBUG
      //if (nodalMetrics_->comm()->rank() == 0)
      {
      printf("                 MMA sigma[0] -> %g\n", sigma[0]);
      }
#endif
    }
  }

  f_opt = fval_cur;
}

#define SQR(x) ((x)*(x))

//---------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
void
MOESS_SANS_Optimizer<PhysDim,TopoDim,MMA>::
solveMMADual(const Real dual_ub, Real& y, Real& f)
{
  const int comm_rank = nodalMetrics_->comm()->rank();

  Real reltol = 1e-14;

  Real rho = 1;

  Real grad = 0;

  Real gval = 0;
  Real wval = 0;

  Real sigma = 1;
  Real ycur = y, yprev = y, yprevprev = y;
  Real fcur = 0;
  Real dfdy = 0;

  int k = 0;
  computeMMADual(y, fcur, grad);

  while (1)  /* outer iterations */
  {
    Real fprev = fcur;

    if (++k > 1) yprevprev = yprev;
    yprev = y;

    while (1)  /* inner iterations */
    {
      gval = fcur;
      wval = 0;
      dfdy = grad;

      if (comm_rank == 0)
      {
        Real u, v, dy, denominv, c, sigma2, dy2;

        u = dfdy;
        v = fabs(dfdy) * sigma + 0.5 * rho;

        u *= (sigma2 = SQR(sigma));
        dy = (u/v) / (-1 - sqrt(fabs(1 - SQR(u/(v*sigma)))));

        /* function value: */
        dy2 = dy * dy;
        denominv = 1.0 / (sigma2 - dy2);
        ycur = y + dy;

        if (ycur > dual_ub) ycur = dual_ub;
        if (ycur <       0) ycur = 0;

        dy = ycur - y;

        /* update gval, wval (approximant functions) */
        c = sigma2 * dy;
        gval += ( dfdy * c + (fabs( dfdy)*sigma + 0.5*rho ) * dy2) * denominv;
        wval += 0.5 * dy2 * denominv;
      }

#ifdef SANS_MPI
      boost::mpi::broadcast(*nodalMetrics_->comm(), ycur, 0);
#endif

      computeMMADual(ycur, fcur, grad);
      y = ycur;

      bool inner_done = false;
      if (comm_rank == 0)
      {
        if (fcur > gval)
          rho = MIN(10*rho, 1.1 * (rho + (fcur-gval) / wval));
        else
          inner_done = true;
      }

#ifdef SANS_MPI
      boost::mpi::broadcast(*nodalMetrics_->comm(), inner_done, 0);
#endif
      if (inner_done) break;
    }

    // relative tolerance check
    bool outer_done = false;
    if (comm_rank == 0)
      if ( fabs(fcur - fprev) < reltol * (fabs(fcur) + fabs(fprev)) * 0.5 || (reltol > 0 && fcur == fprev) )
        outer_done = true;

#ifdef SANS_MPI
    boost::mpi::broadcast(*nodalMetrics_->comm(), outer_done, 0);
#endif
    if (outer_done) break;

    if (comm_rank == 0)
    {
      /* update rho and sigma for iteration k+1 */
      rho = MAX(0.1 * rho, MMA_RHOMIN);

      if (k > 1)
      {
        Real dy2 = (y-yprev) * (yprev-yprevprev);
        Real gam = dy2 < 0 ? 0.7 : (dy2 > 0 ? 1.2 : 1);
        sigma *= gam;
      }
    }
  }
}

//---------------------------------------------------------------------------//
struct MMADualData
{
  Real val;
  Real gval;
  Real wval;
  Real gcval;

#ifdef SANS_MPI
protected:
  friend class boost::serialization::access;

  template<class Archive>
  void serialize(Archive & ar, const unsigned int version)
  {
    ar & val;
    ar & gval;
    ar & wval;
    ar & gcval;
  }
#endif
};

#ifdef SANS_MPI
inline MMADualData operator+(const MMADualData& dl, const MMADualData& dr)
{
  MMADualData dd;
  dd.val   = dl.val   + dr.val;
  dd.gval  = dl.gval  + dr.gval;
  dd.wval  = dl.wval  + dr.wval;
  dd.gcval = dl.gcval + dr.gcval;

  return dd;
}
#endif

//---------------------------------------------------------------------------//
//Routine that computes the objective function and its derivative wrt optimization variables
template <class PhysDim, class TopoDim>
void
MOESS_SANS_Optimizer<PhysDim,TopoDim,MMA>::
computeMMADual(const Real y, Real& f, Real& grad)
{
  int nLocal = dd_.nLocal;
  const Real *x = dd_.x;
  const Real *dfdx = dd_.dfdx;
  const Real *dfcdx = dd_.dfcdx;

  const Real *lb = dd_.lb, *ub = dd_.ub, *sigma = dd_.sigma;
  const Real rho = dd_.rho;
  const Real rhoc = dd_.rhoc;

  Real *xcur = dd_.xcur;

  dd_.count++;

  MMADualData dd;

  dd.val = 0;
  dd.gval = 0;
  dd.wval = 0;
  dd.gcval = 0;

  for (int j = 0; j < nLocal; ++j)
  {
    Real u, v, dx, denominv, c, sigma2, dx2;

    /* first, compute xcur[j] for y.  Because this objective is
      separable, we can minimize over x analytically, and the minimum
      dx is given by the solution of a quadratic equation:
              u dx^2 + 2 v sigma^2 dx + u sigma^2 = 0
      where u and v are defined by the sums below.  Because of
      the definitions, it is guaranteed that |u/v| <= sigma,
      and it follows that the only dx solution with |dx| <= sigma
      is given by:
              (v/u) sigma^2 (-1 + sqrt(1 - (u / v sigma)^2))
          = (u/v) / (-1 - sqrt(1 - (u / v sigma)^2))
      (which goes to zero as u -> 0).  The latter expression
      is less susceptible to roundoff error. */

    if (sigma[j] == 0) /* special case for lb[i] == ub[i] dims, dx=0 */
    {
      xcur[j] = x[j];
      continue;
    }

    u = dfdx[j];
    v = fabs(dfdx[j]) * sigma[j] + 0.5 * rho;

    u += dfcdx[j] * y;
    v += (fabs(dfcdx[j]) * sigma[j] + 0.5 * rhoc) * y;

    u *= (sigma2 = SQR(sigma[j]));
    dx = (u/v) / (-1 - sqrt(fabs(1 - SQR(u/(v*sigma[j])))));
    xcur[j] = x[j] + dx;

    if (xcur[j] > ub[j]) xcur[j] = ub[j];
    if (xcur[j] < lb[j]) xcur[j] = lb[j];

    if (xcur[j] > x[j]+0.9*sigma[j]) xcur[j] = x[j]+0.9*sigma[j];
    if (xcur[j] < x[j]-0.9*sigma[j]) xcur[j] = x[j]-0.9*sigma[j];

    dx = xcur[j] - x[j];

    /* function value: */
    dx2 = dx * dx;
    denominv = 1.0 / (sigma2 - dx2);
    dd.val += (u * dx + v * dx2) * denominv;

    /* update gval, wval, gcval (approximant functions) */
    c = sigma2 * dx;
    dd.gval  += ( dfdx[j] * c + (fabs( dfdx[j])*sigma[j] + 0.5*rho ) * dx2) * denominv;
    dd.wval  += 0.5 * dx2 * denominv;
    dd.gcval += (dfcdx[j] * c + (fabs(dfcdx[j])*sigma[j] + 0.5*rhoc) * dx2) * denominv;
  }

#ifdef SANS_MPI
  dd = boost::mpi::all_reduce(*nodalMetrics_->comm(), dd, std::plus<MMADualData>());
#endif

  // this avoid accumulating dd_.fval and dd_.fcval across all processors
  dd.val   += dd_.fval + y * dd_.fcval;
  dd_.gval  = dd.gval  +     dd_.fval;
  dd_.wval  = dd.wval;
  dd_.gcval = dd.gcval +     dd_.fcval;

  /* gradient is easy to compute: since we are at a minimum x (dval/dx=0),
     we only need the partial derivative with respect to y, and
     we negate because we are maximizing: */
  grad = -dd_.gcval;
  f = -dd.val;
}

//---------------------------------------------------------------------------//
// computes both the objective and constraint as a single functions
template <class PhysDim, class TopoDim>
void
MOESS_SANS_Optimizer<PhysDim,TopoDim,MMA>::
computeObjectiveAndCostConstraint(const std::vector<Real>& x,
                                  Real& f, std::vector<Real>& dfdx,
                                  Real& fc, std::vector<Real>& dfcdx)
{
  const int nLocal = x.size();
  const int nNodePossessed = Sfld_.nDOFpossessed();
  SANS_ASSERT_MSG( nNodePossessed*MatrixSym::SIZE == nLocal,
                   "%d == %d", nNodePossessed*MatrixSym::SIZE, nLocal );

  //Populate step-matrices from the solution vector x
  for (int node = 0; node < nNodePossessed; node++)
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      Sfld_.DOF(node).value(ind) = x[node*MatrixSym::SIZE + ind];

  // synchronize ghost/zombie DOFs
  Sfld_.syncDOFs_MPI_Cached();

  f  = BaseType::computeObjective(Sfld_, dfdx.size(), dfdx.data());
  fc = BaseType::computeCostConstraint(Sfld_, dfcdx.size(), dfcdx.data());
}

} // namespace SANS
