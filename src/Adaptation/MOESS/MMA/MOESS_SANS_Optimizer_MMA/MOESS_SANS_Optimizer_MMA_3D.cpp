// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define MOESS_SANS_OPTIMIZER_MMA_INSTANTIATE
#include "MOESS_SANS_Optimizer_MMA_impl.h"

#include "Field/XFieldVolume.h"

#include "Field/FieldVolume_CG_Cell.h"

namespace SANS
{

//Explicit instantiations
template class MOESS_SANS_Optimizer<PhysD3,TopoD3,MMA>;

}
