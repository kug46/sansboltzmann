// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef REFERENCESTEPMATRIX_H_
#define REFERENCESTEPMATRIX_H_

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "Topology/Dimension.h"
#include "Topology/ElementTopology.h"

#include "Meshing/Metric/MetricOps.h"
#include "BasisFunction/BasisFunction_RefElement_Split.h"

#include "Adaptation/MOESS/LocalSplitConfig.h"

#include "Field/Local/XField_LocalPatch.h"

#include "XFieldND_EquilateralRef.h"

#include "MPI/communicator_fwd.h"

namespace SANS
{

template <class PhysDim, class TopoDim>
class ReferenceStepMatrix
{

public:
  typedef typename Simplex<TopoDim>::type Topology;
  static const int D = PhysDim::D;   // physical dimensions
  typedef DLA::MatrixSymS<D,Real> MatrixSym;

  explicit ReferenceStepMatrix(mpi::communicator& comm_local, const std::vector<LocalSplitConfig>& split_config_list)
  : split_config_list_(split_config_list)
  {
    Nconfig_ = (int) split_config_list_.size();
    compute(comm_local);
  }

  ~ReferenceStepMatrix() {}

  std::vector<MatrixSym> getStepMatrixList() const;

  MatrixSym getStepMatrix(int config_index) const
  {
    SANS_ASSERT( (config_index >= 0) && (config_index < Nconfig_) );
    return stepmatrix_list_[config_index];
  }

  MatrixSym getStepMatrix(int config_index, int sub_index) const
  {
    SANS_ASSERT( (config_index >= 0) && (config_index < Nconfig_) );
    SANS_ASSERT( (sub_index >= 0) && (sub_index < static_cast<int>(stepmatrix_sub_list_[config_index].size()) ) );
    return stepmatrix_sub_list_[config_index][sub_index];
  }

protected:
  const std::vector<LocalSplitConfig>& split_config_list_;
  int Nconfig_; //number of split configurations

  std::vector<MatrixSym> stepmatrix_list_; //List of step matrices for each split configuration
  std::vector<std::vector<MatrixSym>> stepmatrix_sub_list_; //List of list of sub-step matrices for each split configuration

  void compute(mpi::communicator& comm_local);

};


template <class PhysDim, class TopoDim>
void
ReferenceStepMatrix<PhysDim, TopoDim>::compute(mpi::communicator& comm_local)
{
  typedef typename Simplex<TopoDim>::type Topology;
  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef typename XFieldCellGroupType::BasisType BasisType;

  //Create an XField containing a single reference equilateral element
  XFieldND_EquilateralRef<PhysDim, TopoDim, Topology> xfld(comm_local);

  const XFieldCellGroupType& xfldCellGroup_unsplit = xfld.template getCellGroup<Topology>(0);
  const BasisType* cell_basis_unsplit = xfldCellGroup_unsplit.basis();
  ElementXFieldClass xfldElem_unsplit( cell_basis_unsplit ); //xfld element

  int ref_elem = 0;
  xfldCellGroup_unsplit.getElement(xfldElem_unsplit, ref_elem); //Get reference element

  //Get implied metric of reference element (should be just the identity matrix)
  MatrixSym Mref;
  xfldElem_unsplit.impliedMetric(Mref);

  //------------Perform local splits according to specified configurations----------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysDim,TopoDim> connectivity(xfld);

  //Extract the local mesh for the central element (group = 0, elem = 0)
  XField_LocalPatchConstructor<PhysDim,Topology> xfld_local(comm_local,connectivity,0,0);

  stepmatrix_list_.resize(Nconfig_);
  stepmatrix_sub_list_.resize(Nconfig_);

  // std::cout<< std::endl << "D = " << PhysDim::D << std::endl << std::endl;

  for (int i = 0; i < Nconfig_; i++)
  {
    XField_LocalPatch<PhysDim,Topology> xfld_split_local(xfld_local,
                                                        split_config_list_[i].type,
                                                        split_config_list_[i].edge_index);

    const XFieldCellGroupType& xfldCellGroup = xfld_split_local.template getCellGroup<Topology>(0);
    const BasisType* cell_basis = xfldCellGroup.basis();
    ElementXFieldClass xfldElem_sub( cell_basis ); //xfld element for sub-cell

    int nMainCells = xfldCellGroup.nElem();
    std::vector<MatrixSym> Msubcell_vec(nMainCells); // vector to hold implied metrics of subcells
    std::vector<MatrixSym> Ssubcell_vec(nMainCells); // vector to hold step matrices for subcells

    //std::cout<<"Config "<<i<<", nCells:"<<nMainCells<<std::endl;

    for (int cell = 0; cell < nMainCells; cell++)
    {
      xfldCellGroup.getElement(xfldElem_sub, cell); //Get sub-cell from split mesh

      //Get implied metric of sub-cell
      xfldElem_sub.impliedMetric(Msubcell_vec[cell]);

      // compute step to sub-cell metric
      Ssubcell_vec[cell] = Metric::computeStepMatrix(Mref,Msubcell_vec[cell]);
      // std::cout << " i = " << i << ", j = " << cell << std::endl;
      // std::cout << std::setprecision(16) << Ssubcell_vec[cell] <<std::endl;
    }

    //Compute the affine invariant average of the implied metrics on the subcells
    MatrixSym Mavg = Metric::computeAffineInvAvg(Msubcell_vec);

    //Compute the step matrix from the original metric (Mref) to the averaged metric (Mavg) of this config
    stepmatrix_list_[i] = Metric::computeStepMatrix(Mref, Mavg);

    // save off list of sub cell steps
    stepmatrix_sub_list_[i] = Ssubcell_vec;
    // MatrixSym Savg = (Ssubcell_vec[0]+Ssubcell_vec[1])/2;
    // std::cout << "step_matrix[" << i << "] = " << std::endl << stepmatrix_list_[i] << std::endl;
    // std::cout << "step_matrix_avg[" << i << "] = " << std::endl << Savg << std::endl;
  }

}

}

#endif /* REFERENCESTEPMATRIX_H_ */
