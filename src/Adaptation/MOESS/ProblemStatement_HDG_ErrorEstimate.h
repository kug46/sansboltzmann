// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PROBLEMSTATEMENT_HDG_ERRORESTIMATE_H_
#define PROBLEMSTATEMENT_HDG_ERRORESTIMATE_H_

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/timer.h"
#include "Topology/Dimension.h"
#include "Topology/ElementTopology.h"

#include "SolverInterfaceBase.h"
#include "ProblemStatement_FieldTypes_HDG.h"

#include "Discretization/HDG/FunctionalCell_HDG.h"
#include "Discretization/HDG/JacobianFunctionalCell_HDG.h"

#include "Discretization/HDG/FunctionalBoundaryTrace_Dispatch_HDG.h"
#include "Discretization/HDG/JacobianFunctionalBoundaryTrace_Dispatch_HDG.h"
#include "Discretization/HDG/JacobianBoundaryTrace_Dispatch_HDG_AuxiliaryVariable.h"

#include "Field/Local/XField_Local_Base.h"
#include "Field/Element/ElementProjection_L2.h"
#include "Field/output_Tecplot.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"
#include "Field/ProjectSoln/ProjectGlobalField.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"
#include "LinearAlgebra/DenseLinAlg/InverseLUP.h"

// Estimation
#include "ErrorEstimate/HDG/ErrorEstimate_HDG.h"

namespace SANS
{

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class DiscTag, class XFieldType, class OutputIntegrandType>
class ProblemStatement_HDG_ErrorEstimate : public SolverInterfaceBase<typename NDPDEClass::PhysDim, typename XFieldType::TopoDim>
{

public:
  typedef typename NDPDEClass::PhysDim PhysDim;
  typedef typename XFieldType::TopoDim TopoDim;
  const int D = PhysDim::D;
//  typedef Field_DG_Cell<PhysDim, TopoDim, Real> EFieldType;

  typedef Disc<NDPDEClass, BCNDConvert, BCVector, DiscTag, XFieldType> DiscType;
  typedef typename DiscType::AlgEqnSet_Global AlgEqnSet_Global;
  typedef typename DiscType::AlgEqnSet_Local AlgEqnSet_Local;

  typedef typename DiscType::QFieldType QFieldType;
  typedef typename DiscType::AuxFieldType AuxFieldType;
  typedef typename DiscType::QIFieldType QIFieldType;
  typedef typename DiscType::LGFieldType LGFieldType;

  typedef typename DiscType::QFieldType_Local QFieldType_Local;
  typedef typename DiscType::AuxFieldType_Local AuxFieldType_Local;
  typedef typename DiscType::QIFieldType_Local QIFieldType_Local;
  typedef typename DiscType::LGFieldType_Local LGFieldType_Local;

  typedef typename DiscType::ArrayQ ArrayQ;

  typedef typename DiscType::Params DiscParams;

  typedef ProblemStatement_HDG_ErrorEstimate<NDPDEClass, BCNDConvert, BCVector, DiscTag, XFieldType, OutputIntegrandType> ProblemStatementClass;

  typedef typename OutputIntegrandType::template ArrayJ<Real> ArrayJ;

  template< class... BCArgs >
  ProblemStatement_HDG_ErrorEstimate(const XFieldType& xfld, const NDPDEClass& pde, DiscParams& params,
                                     std::vector<Real>& tol, const int quadOrder,
                                     const int order, const BasisFunctionCategory category_cell, const BasisFunctionCategory category_trace,
                                     const std::vector<int>& cellgroup_list, const std::vector<int>& InteriorTraceGroups,
                                     PyDict& BCList, const std::map< std::string, std::vector<int> >& BCBoundaryGroups,
                                     const PyDict& NewtonSolverDict,
                                     const OutputIntegrandType& fcnOutput, BCArgs&... args )
    : xfld_(xfld),
      pde_(pde),
      disc_params_(params),
      tol_(tol),
      quadOrder_(quadOrder),
      order_(order),
      basis_category_cell(category_cell),
      basis_category_trace(category_trace),
      cellgroup_list_(cellgroup_list),
      interiortracegroup_list_(InteriorTraceGroups),
      BCList_(BCList),
      BCBoundaryGroups_(BCBoundaryGroups),
      active_BGroup_list_(AlgEqnSet_Global::BCParams::getLGBoundaryGroups(BCList_, BCBoundaryGroups_)),
      NewtonSolverDict_(NewtonSolverDict),
      qfld_          (xfld_, order_  , basis_category_cell),
      auxfld_        (xfld_, order_  , basis_category_cell),
      qIfld_         (xfld_, order_  , basis_category_cell),
      lgfld_         (xfld_, order_-1, basis_category_trace, active_BGroup_list_),
      q_pro_fld_     (xfld_, order_+1, basis_category_cell),
      aux_pro_fld_   (xfld_, order_+1, basis_category_cell),
      qI_pro_fld_    (xfld_, order_+1, basis_category_cell),
      lg_pro_fld_    (xfld_, order_  , basis_category_trace, active_BGroup_list_),
      qadj_pro_fld_  (xfld_, order_+1, basis_category_cell),
      auxadj_pro_fld_(xfld_, order_+1, basis_category_cell),
      qIadj_pro_fld_ (xfld_, order_+1, basis_category_cell),
      lgadj_pro_fld_ (xfld_, order_  , basis_category_trace, active_BGroup_list_),
      AlgEqnSet_Primal_(xfld_, qfld_, auxfld_, qIfld_, lgfld_, pde_,
                        disc_params_, QuadratureOrder(xfld_, quadOrder_), tol_,
                        cellgroup_list_, interiortracegroup_list_, BCList_, BCBoundaryGroups_, args...),
      AlgEqnSet_Primal_Pro_(xfld_, q_pro_fld_, aux_pro_fld_, qI_pro_fld_, lg_pro_fld_, pde_,
                            disc_params_, QuadratureOrder(xfld_, quadOrder_), tol_,
                            cellgroup_list_, interiortracegroup_list_, BCList_, BCBoundaryGroups_, args...),
      fcnOutput_(fcnOutput)
  {

    qfld_ = 0;
    auxfld_ = 0;
    qIfld_ = 0;
    lgfld_ = 0;

    qadj_pro_fld_ = 0;
    auxadj_pro_fld_ = 0;
    qIadj_pro_fld_ = 0;
    lgadj_pro_fld_ = 0;

    errorIndicator_ = 0.0;
    errorEstimate_ = 0.0;

    //Loop over each cellgroup and find the no. of DOF per cell and solution order for that cellgroup
    nDOFperCell_.resize((int) xfld_.nCellGroups(), -1);
    orderVec_.resize((int) xfld_.nCellGroups(), -1);
    for_each_CellGroup<TopoDim>::apply( CellwiseOperations(cellgroup_list_, *this), xfld_);

  }

  virtual ~ProblemStatement_HDG_ErrorEstimate() {}

  virtual void solveGlobalPrimalProblem() override;
  virtual void solveGlobalAdjointProblem() override;

  virtual void computeErrorEstimates() override;
  virtual Real getElementalErrorEstimate(int cellgroup, int elem) const override;
  virtual Real getGlobalErrorIndicator() const override;
  virtual Real getGlobalErrorEstimate() const override;

  virtual void output_EField(const std::string& filename) const override;

  virtual LocalSolveStatus solveLocalProblem(const XField_Local_Base<PhysDim, TopoDim>& local_xfld, std::vector<Real>& local_error) const override;

  virtual Real getnDOFperCell(const int cellgroup) const override;
  virtual int getSolutionOrder(const int cellgroup) const override;

  void setInitialSolution(const ArrayQ& q);

  template <class SolutionFunctionType>
  void setInitialSolution(const SolutionFunctionType& fcn, const std::vector<int>& cellGroups);

  void setInitialSolution(const Field<PhysDim, TopoDim, ArrayQ>& qfldFrom);

  virtual Real getOutput() const override;

  const QFieldType& getPrimalSolutionField() { return qfld_; }
  void exportPrimalSolutionTo(const std::string& filename) const;
  void exportAdjointSolutionTo(const std::string& filename) const;

  SolverInterface_impl<ProblemStatementClass>
  CellwiseOperations(const std::vector<int>& cellgroup_list, ProblemStatementClass& base );

  template<class Topology>
  void getSolutionInfo_cellgroup(const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
                                 const int cellGroupGlobal);

  // This isn't really true -> HDG uses the qIfld DOF really. Need to make this assessment better
  virtual int getCost() const override { return qfld_.nDOF(); }

protected:

  const XFieldType& xfld_; // mesh
  const NDPDEClass& pde_;  // PDE
  DiscParams& disc_params_;
  std::vector<Real>& tol_;
  const int quadOrder_;
  int order_;              // solution order
  const BasisFunctionCategory basis_category_cell;
  const BasisFunctionCategory basis_category_trace;
  const std::vector<int>& cellgroup_list_;
  const std::vector<int>& interiortracegroup_list_;
  PyDict& BCList_;
  const std::map< std::string, std::vector<int> >& BCBoundaryGroups_;
  std::vector<int> active_BGroup_list_; //list of BoundaryTraceGroups which have lagrange multiplier DOFs (mitLG)
  const PyDict& NewtonSolverDict_;

  QFieldType qfld_;              // solution field
  AuxFieldType auxfld_;          // auxiliary variable field
  QIFieldType qIfld_;            // interior trace solution field
  LGFieldType lgfld_;            // lagrange multiplier field

  QFieldType q_pro_fld_;         // solution field prolonged to richer space (i.e P+1)
  AuxFieldType aux_pro_fld_;     // auxiliary variable field prolonged to richer space (i.e P+1)
  QIFieldType qI_pro_fld_;       // interior trace solution field prolonged to richer space (i.e P+1)
  LGFieldType lg_pro_fld_;       // lagrange multiplier field prolonged to richer space (i.e P+1)

  QFieldType qadj_pro_fld_;      // adjoint solution field in richer space
  AuxFieldType auxadj_pro_fld_;  // auxiliary variable adjoint field in richer space
  QIFieldType qIadj_pro_fld_;    // interior trace adjoint solution field in richer space
  LGFieldType lgadj_pro_fld_;    // lagrange multiplier adjoint field in richer space

//  EFieldType efld_;        // elemental error estimate field

  AlgEqnSet_Global AlgEqnSet_Primal_; // AlgebraicEquationSet for global primal problem
  AlgEqnSet_Global AlgEqnSet_Primal_Pro_; // AlgebraicEquationSet for global primal in richer space

  std::vector<std::vector<Real>> errorArray_; //Array of elemental error estimates (index:[cellgroup][elem])
  const OutputIntegrandType& fcnOutput_;
  Real errorIndicator_;
  Real errorEstimate_;
  ArrayJ outputJ_; //output functional evaluated at current solution

  std::vector<Real> nDOFperCell_; //indexing: [global cellgrp] - number of degrees of freedom per cell
  std::vector<int> orderVec_; //indexing: [global cellgrp] - solution order of each cellgroup

};

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class DiscTag, class XFieldType, class OutputIntegrandType>
void
ProblemStatement_HDG_ErrorEstimate<NDPDEClass,BCNDConvert,BCVector,DiscTag,XFieldType,OutputIntegrandType>::
solveGlobalPrimalProblem()
{
  typedef typename AlgEqnSet_Global::SystemMatrix SystemMatrixClass;
  typedef typename AlgEqnSet_Global::SystemVector SystemVectorClass;

  timer clock;

  // Create the nonlinear solver object
  NewtonSolver<SystemMatrixClass> nonlinear_solver( AlgEqnSet_Primal_, NewtonSolverDict_ );

  // set initial condition from current solution in solution fields
  SystemVectorClass sln0(AlgEqnSet_Primal_.vectorStateSize());
  AlgEqnSet_Primal_.fillSystemVector(sln0);

  // nonlinear solve
  SystemVectorClass sln(AlgEqnSet_Primal_.vectorStateSize());

  SolveStatus status = nonlinear_solver.solve(sln0, sln);
  AlgEqnSet_Primal_.setSolutionField(sln);

  if (status.converged == false)
  {
    exportPrimalSolutionTo("tmp/qfld_unconverged.plt");
    SANS_DEVELOPER_EXCEPTION("Nonlinear solver failed to converge.");
  }

  outputJ_ = 0.0;
  std::cout<<"Primal solve time: " << std::fixed << std::setprecision(3) << clock.elapsed() << "s" << std::endl;

  #ifndef BOUNDARYOUTPUT
    // evaluate output function
    const int nCellGroups = xfld_.nCellGroups();
    SANS_ASSERT(nCellGroups > 0);
    std::vector<int> quadratureOrder(nCellGroups,-1);

    IntegrateCellGroups<TopoDim>::integrate(
        FunctionalCell_HDG( fcnOutput_, outputJ_ ), xfld_, (qfld_, auxfld_), quadratureOrder.data(), nCellGroups );
  #else
    const int nBoundaryTraceGroups = xfld_.nBoundaryTraceGroups();
    SANS_ASSERT(nBoundaryTraceGroups > 0);

    std::vector<int> quadratureOrder(nBoundaryTraceGroups,-1);

    AlgEqnSet_Primal_.dispatchBC().dispatch_HDG(
      FunctionalBoundaryTrace_Dispatch_HDG(fcnOutput_, xfld_, qfld_, auxfld_, qIfld_, quadratureOrder.data(), nBoundaryTraceGroups, outputJ_ ) );
  #endif
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class DiscTag, class XFieldType, class OutputIntegrandType>
void
ProblemStatement_HDG_ErrorEstimate<NDPDEClass,BCNDConvert,BCVector,DiscTag,XFieldType,OutputIntegrandType>::
solveGlobalAdjointProblem()
{
  typedef typename AlgEqnSet_Global::SystemMatrix SystemMatrixClass;
  typedef typename AlgEqnSet_Global::SystemVector SystemVectorClass;

  typedef SurrealS<NDPDEClass::N> SurrealClass;

  timer clock;

  // Prolongate the primal solution to the richer solution space
  qfld_.projectTo(q_pro_fld_);
  auxfld_.projectTo(aux_pro_fld_);
  qIfld_.projectTo(qI_pro_fld_);
  lgfld_.projectTo(lg_pro_fld_);

  // Functional integral
  SystemVectorClass rhs(AlgEqnSet_Primal_Pro_.vectorEqSize());
  rhs = 0;

  const int iPDE = AlgEqnSet_Primal_Pro_.iPDE;

  QuadratureOrder quadratureOrder(xfld_, quadOrder_);

#ifndef BOUNDARYOUTPUT
  IntegrateCellGroups<TopoDim>::integrate(
      JacobianFunctionalCell_HDG<SurrealClass>( fcnOutput_, rhs(iPDE) ),
      xfld_, (q_pro_fld_, aux_pro_fld_), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
#else
  const int iINT = AlgEqnSet_Primal_Pro_.iINT;

  typedef typename NDPDEClass::template VectorMatrixQ<Real> VectorMatrixQ;

  FieldDataMatrixD_BoundaryCell<VectorMatrixQ> jacAUX_q_bcell(q_pro_fld_);
  FieldDataMatrixD_BoundaryTrace<VectorMatrixQ> jacAUX_qI_btrace(q_pro_fld_, qI_pro_fld_);
  jacAUX_q_bcell = 0.0;
  jacAUX_qI_btrace = 0.0;

  std::vector<std::vector<std::vector<int>>> mapDOFGlobal_qI(xfld_.getXField().nBoundaryTraceGroups());

  AlgEqnSet_Primal_Pro_.dispatchBC().dispatch_HDG(
      JacobianBoundaryTrace_Dispatch_HDG_AuxiliaryVariable<SurrealClass>( xfld_, q_pro_fld_, aux_pro_fld_, qI_pro_fld_,
                                                                          quadratureOrder.boundaryTraceOrders.data(),
                                                                          quadratureOrder.boundaryTraceOrders.size(),
                                                                          jacAUX_q_bcell, jacAUX_qI_btrace, mapDOFGlobal_qI ) );

  AlgEqnSet_Primal_Pro_.dispatchBC().dispatch_HDG(
      JacobianFunctionalBoundaryTrace_Dispatch_HDG<SurrealClass>(fcnOutput_,
                                                                 AlgEqnSet_Primal_Pro_.getCellIntegrand(),
                                                                 AlgEqnSet_Primal_Pro_.getInteriorTraceIntegrand(),
                                                                 AlgEqnSet_Primal_Pro_.getAuxiliaryJacobianInverse(),
                                                                 jacAUX_q_bcell, jacAUX_qI_btrace, mapDOFGlobal_qI,
                                                                 AlgEqnSet_Primal_Pro_.getXField_CellToTrace(),
                                                                 xfld_, q_pro_fld_, aux_pro_fld_, qI_pro_fld_,
                                                                 auxadj_pro_fld_,
                                                                 quadratureOrder.cellOrders.data(),
                                                                 quadratureOrder.cellOrders.size(),
                                                                 quadratureOrder.interiorTraceOrders.data(),
                                                                 quadratureOrder.interiorTraceOrders.size(),
                                                                 quadratureOrder.boundaryTraceOrders.data(),
                                                                 quadratureOrder.boundaryTraceOrders.size(),
                                                                 rhs(iPDE), rhs(iINT) ) );
#endif

  // adjoint solve
  SLA::UMFPACK<SystemMatrixClass> solverAdj(AlgEqnSet_Primal_Pro_, SLA::TransposeSolve);

  SystemVectorClass adj(AlgEqnSet_Primal_Pro_.vectorEqSize());
  solverAdj.solve(rhs, adj);

  // update solution
  AlgEqnSet_Primal_Pro_.setAdjointField(adj, qadj_pro_fld_, auxadj_pro_fld_, qIadj_pro_fld_, lgadj_pro_fld_);

  std::cout<<"Adjoint solve time: " << std::fixed << std::setprecision(3) << clock.elapsed() << "s" << std::endl;
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class DiscTag, class XFieldType, class OutputIntegrandType>
void
ProblemStatement_HDG_ErrorEstimate<NDPDEClass,BCNDConvert,BCVector,DiscTag,XFieldType,OutputIntegrandType>::
exportPrimalSolutionTo(const std::string& filename) const
{
  // Tecplot dump
  std::cout << "Exporting primal solution to: " << filename << std::endl;
  output_Tecplot( qfld_, filename );
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class DiscTag, class XFieldType, class OutputIntegrandType>
void
ProblemStatement_HDG_ErrorEstimate<NDPDEClass,BCNDConvert,BCVector,DiscTag,XFieldType,OutputIntegrandType>::
exportAdjointSolutionTo(const std::string& filename) const
{
  // Tecplot dump
  std::cout << "Exporting adjoint solution to: " << filename << std::endl;
  output_Tecplot( qadj_pro_fld_, filename );
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class DiscTag, class XFieldType, class OutputIntegrandType>
void
ProblemStatement_HDG_ErrorEstimate<NDPDEClass,BCNDConvert,BCVector,DiscTag,XFieldType,OutputIntegrandType>::
computeErrorEstimates()
{
  typedef ErrorEstimate_HDG<NDPDEClass, BCNDConvert, BCVector, XFieldType> ErrorEstimateClass;

  timer clock;

  QuadratureOrder quadrule(xfld_, quadOrder_);

  ErrorEstimateClass ErrorEstimate(xfld_,
                                   qfld_, auxfld_, qIfld_, lgfld_,
                                   qadj_pro_fld_, auxadj_pro_fld_, qIadj_pro_fld_, lgadj_pro_fld_,
                                   pde_, disc_params_, quadrule, cellgroup_list_, interiortracegroup_list_,
                                   BCList_, BCBoundaryGroups_);

  ErrorEstimate.fillEArray(errorArray_);

  errorIndicator_ = 0.0, errorEstimate_ = 0.0;
  ErrorEstimate.getErrorIndicator(errorIndicator_);
  ErrorEstimate.getErrorEstimate(errorEstimate_);

  std::cout<<"Error estimation time: " << std::fixed << std::setprecision(3) << clock.elapsed() << "s" << std::endl;
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class DiscTag, class XFieldType, class OutputIntegrandType>
void
ProblemStatement_HDG_ErrorEstimate<NDPDEClass,BCNDConvert,BCVector,DiscTag,XFieldType,OutputIntegrandType>::
output_EField(const std::string& filename) const
{
  typedef ErrorEstimate_HDG<NDPDEClass, BCNDConvert, BCVector, XFieldType> ErrorEstimateClass;

  QuadratureOrder quadrule(xfld_, quadOrder_);

  ErrorEstimateClass ErrorEstimate(xfld_,
                                   qfld_, auxfld_, qIfld_, lgfld_,
                                   qadj_pro_fld_, auxadj_pro_fld_, qIadj_pro_fld_, lgadj_pro_fld_,
                                   pde_, disc_params_, quadrule, cellgroup_list_, interiortracegroup_list_,
                                   BCList_, BCBoundaryGroups_);

//  output_Tecplot(ErrorEstimate.getEField(), filename, {"Error Estimate"});
  ErrorEstimate.outputFields( filename );
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class DiscTag, class XFieldType, class OutputIntegrandType>
Real
ProblemStatement_HDG_ErrorEstimate<NDPDEClass,BCNDConvert,BCVector,DiscTag,XFieldType,OutputIntegrandType>::
getElementalErrorEstimate(int cellgroup, int elem) const
{
  return errorArray_[cellgroup][elem];
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class DiscTag, class XFieldType, class OutputIntegrandType>
Real
ProblemStatement_HDG_ErrorEstimate<NDPDEClass,BCNDConvert,BCVector,DiscTag,XFieldType,OutputIntegrandType>::
getGlobalErrorIndicator() const
{
  return errorIndicator_;
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class DiscTag, class XFieldType, class OutputIntegrandType>
Real
ProblemStatement_HDG_ErrorEstimate<NDPDEClass,BCNDConvert,BCVector,DiscTag,XFieldType,OutputIntegrandType>::
getGlobalErrorEstimate() const
{
  return errorEstimate_;
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class DiscTag, class XFieldType, class OutputIntegrandType>
LocalSolveStatus
ProblemStatement_HDG_ErrorEstimate<NDPDEClass,BCNDConvert,BCVector,DiscTag,XFieldType,OutputIntegrandType>::
solveLocalProblem(const XField_Local_Base<PhysDim, TopoDim>& xfld_local, std::vector<Real>& local_error) const
{
  typedef typename AlgEqnSet_Local::SystemMatrix SystemMatrixClass;
  typedef typename AlgEqnSet_Local::SystemVector SystemVectorClass;
  typedef typename AlgEqnSet_Local::SystemNonZeroPattern SystemNonZeroPattern;

  typedef ErrorEstimate_HDG<NDPDEClass, BCNDConvert, BCVector,XFieldType> ErrorEstimateClass;

  //Find the mapping between the boundary conditions of local problem and the global problem
  PyDict BCList_local;
  std::map<std::string, std::vector<int>> BCBoundaryGroups_local;

  std::vector<int> boundaryTraceGroups = xfld_local.getReSolveBoundaryTraceGroups();

  for (std::size_t local_group = 0; local_group < boundaryTraceGroups.size(); local_group++)
  {
    //Get the global boundary-trace group index for the current local boundary-trace group
    int globalBTraceGroup = xfld_local.getGlobalBoundaryTraceMap({local_group,0}).first;

    SANS_ASSERT(globalBTraceGroup >= 0); //Check for non-negative index

    for (std::map<std::string,std::vector<int>>::const_iterator it = BCBoundaryGroups_.begin(); it != BCBoundaryGroups_.end(); ++it)
    {
      const std::vector<int>& grouplist = it->second;
      bool found = false;

      for (int j = 0; j < (int) grouplist.size(); j++)
      {
        if (globalBTraceGroup == grouplist[j])
        {
          //Found the BC associated with the globalBTraceGroup we want, so add it to the local BTraceGroup map
          BCBoundaryGroups_local[it->first].push_back(local_group);
          BCList_local[it->first] = BCList_[it->first];
          found = true;
          break;
        }
      }

      if (found) break;
    }
  } //loop over local btracegroups

  std::vector<int> active_local_BGroup_list = AlgEqnSet_Local::BCParams::getLGBoundaryGroups(BCList_local, BCBoundaryGroups_local);

  QuadratureOrder quadratureOrder(xfld_local, quadOrder_);

  std::vector<int> cellGroups = {0,1};

  //TODO: Only main-neighbor or main-main interior traces for now (no mixed meshes, so max 2 interior trace groups)
  // No need to re-solve for neighbor-neighbor interior trace solution DOFs
  std::vector<int> interiorTraceGroups = xfld_local.getReSolveInteriorTraceGroups();

  QFieldType_Local qfld_local(xfld_local, qfld_, order_, basis_category_cell);
  AuxFieldType_Local auxfld_local(xfld_local, auxfld_, order_, basis_category_cell);
  QIFieldType_Local qIfld_local(xfld_local, qIfld_, order_, basis_category_cell, interiorTraceGroups, boundaryTraceGroups);
  LGFieldType_Local lgfld_local(xfld_local, lgfld_, order_-1, basis_category_trace, active_local_BGroup_list);

  //Project solution from cell field to "new" interior trace DOFs in qIfld_local
  Field_Local_Transfer<TopoDim>::transferFromFieldCell(qfld_, qIfld_local);

  AlgEqnSet_Local algEqnSet_local(xfld_local, qfld_local, auxfld_local, qIfld_local, lgfld_local,
                                  pde_, disc_params_, quadratureOrder, tol_,
                                  cellGroups, interiorTraceGroups, BCList_local, BCBoundaryGroups_local);

  // Local vectors
  SystemVectorClass q_local   (algEqnSet_local.vectorStateSize()); // current solution
  SystemVectorClass q0_local  (algEqnSet_local.vectorStateSize()); // previous solution
  SystemVectorClass dsln_local(algEqnSet_local.vectorStateSize()); // solution update
  SystemVectorClass rsd_local (algEqnSet_local.vectorEqSize()); // residual

  // Convert the projected qfld to a vector
  algEqnSet_local.fillSystemVector(q_local);

  // Compute the auxiliary variable field
  algEqnSet_local.setSolutionField(q_local);

  // Local Jacobian
  SystemNonZeroPattern nz_local(algEqnSet_local.matrixSize());
  SystemMatrixClass jac_local(nz_local);

  int maxIter = 10;
  Real stol = 1e-10;
  Real s = stol + 1.0;
  LocalSolveStatus status(false, maxIter);

  timer clock_residual;
  rsd_local = 0;
  algEqnSet_local.residual(rsd_local);
  status.time_residual += clock_residual.elapsed();

  // save off the current residual with the projected solution
  std::vector<std::vector<Real>> rsdNorm0( algEqnSet_local.residualNorm(rsd_local) );
  std::vector<std::vector<Real>> rsdNorm = rsdNorm0;
  std::vector<std::vector<Real>> rsdNorm1 = rsdNorm0;

  for (int iter = 0; iter < maxIter && s > stol; iter++)
  {
//    std::vector<std::vector<Real>> nrmRsd = algEqnSet_local.residualNorm(rsd_local);
//    std::cout << "Local Newton iteration " << iter  << ": Res_L2norm = " << nrmRsd << std::endl;

    if ( algEqnSet_local.convergedResidual(rsdNorm) )
    {
      status.converged = true;
      status.iter = iter;
      break;
    }

    // save the current solution and residual norm
    q0_local = q_local;

    //Jacobian
    timer clock_jacobian;
    jac_local = 0;
    algEqnSet_local.jacobian(jac_local);
    status.time_jacobian += clock_jacobian.elapsed();

    //Perform local solve
    timer clock_solve;
    dsln_local = DLA::InverseLUP::Solve(jac_local, rsd_local);
    status.time_solve += clock_solve.elapsed();

    // use a halving line search to update the solution
    Real s = 2.0;
    while (s > stol)
    {
      s = s/2.0;

      //Update local solution vector
      q_local = q0_local - s*dsln_local;

      // check that the solution is valid (this sets the qfld and also computes the afld)
      bool isValidState = algEqnSet_local.isValidStateSystemVector(q_local);
      if ( !isValidState ) continue;

      // Compute the new residual
      timer clock_residual;
      rsd_local = 0;
      algEqnSet_local.residual(rsd_local);
      status.time_residual += clock_residual.elapsed();

      // break out if the residual decreased
      rsdNorm1 = algEqnSet_local.residualNorm(rsd_local);
      if ( algEqnSet_local.decreasedResidual(rsdNorm, rsdNorm1) ) break;
    }

    // save the residual for this iteration
    rsdNorm = algEqnSet_local.residualNorm(rsd_local);
  }

  //Gather some information about the residual norms if the solve didn't converge
  if (status.converged == false)
  {
    status.rsdNorm0_unconverged = rsdNorm0;
    status.rsdNorm_unconverged = rsdNorm;
  }

  //Create local adjoint fields - copies the relevant DOFs from the global adjoint solution
  QFieldType_Local qadj_pro_fld_local(xfld_local, qadj_pro_fld_, order_+1, basis_category_cell);
  AuxFieldType_Local auxadj_pro_fld_local(xfld_local, auxadj_pro_fld_, order_+1, basis_category_cell);
  QIFieldType_Local qIadj_pro_fld_local(xfld_local, qIadj_pro_fld_, order_+1, basis_category_cell, interiorTraceGroups, boundaryTraceGroups);
  LGFieldType_Local lgadj_pro_fld_local(xfld_local, lgadj_pro_fld_, order_, basis_category_trace, active_local_BGroup_list);

  //Compute local error estimates
  timer clock_errest;
  ErrorEstimateClass ErrorEstimate(xfld_local,
                                   qfld_local, auxfld_local, qIfld_local, lgfld_local,
                                   qadj_pro_fld_local, auxadj_pro_fld_local, qIadj_pro_fld_local, lgadj_pro_fld_local,
                                   pde_, disc_params_, quadratureOrder, cellGroups, interiorTraceGroups,
                                   BCList_local, BCBoundaryGroups_local);

#if 1
  ErrorEstimate.getLocalSolveErrorIndicator(local_error); //Accumulate errors only from cellgroup 0 - (main cells)
#else
  ErrorEstimate.getLocalSolveErrorEstimate(local_error); //Accumulate errors only from cellgroup 0 - (main cells)
#endif
  status.time_errest += clock_errest.elapsed();

  return status;
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class DiscTag, class XFieldType, class OutputIntegrandType>
Real
ProblemStatement_HDG_ErrorEstimate<NDPDEClass,BCNDConvert,BCVector,DiscTag,XFieldType,OutputIntegrandType>::
getnDOFperCell(const int cellgroup) const
{
  SANS_ASSERT(cellgroup >= 0 && cellgroup < (int) nDOFperCell_.size());
  return nDOFperCell_[cellgroup];
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class DiscTag, class XFieldType, class OutputIntegrandType>
int
ProblemStatement_HDG_ErrorEstimate<NDPDEClass,BCNDConvert,BCVector,DiscTag,XFieldType,OutputIntegrandType>::
getSolutionOrder(const int cellgroup) const
{
  SANS_ASSERT(cellgroup >= 0 && cellgroup < (int) orderVec_.size());
  return orderVec_[cellgroup];
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class DiscTag, class XFieldType, class OutputIntegrandType>
Real
ProblemStatement_HDG_ErrorEstimate<NDPDEClass,BCNDConvert,BCVector,DiscTag,XFieldType,OutputIntegrandType>::
getOutput() const
{
  return outputJ_;
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class DiscTag, class XFieldType, class OutputIntegrandType>
void
ProblemStatement_HDG_ErrorEstimate<NDPDEClass,BCNDConvert,BCVector,DiscTag,XFieldType,OutputIntegrandType>::
setInitialSolution(const ArrayQ& q)
{
  qfld_ = q;
  auxfld_ = 0;
  qIfld_ = q;// Really should add another qI version of this
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class DiscTag, class XFieldType, class OutputIntegrandType>
template<class SolutionFunctionType>
void
ProblemStatement_HDG_ErrorEstimate<NDPDEClass,BCNDConvert,BCVector,DiscTag,XFieldType,OutputIntegrandType>::
setInitialSolution(const SolutionFunctionType& fcn, const std::vector<int>& cellGroups)
{
  for_each_CellGroup<TopoDim>::apply( ProjectSolnCell_Discontinuous(fcn, cellGroups), (xfld_, qfld_) );
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class DiscTag, class XFieldType, class OutputIntegrandType>
void
ProblemStatement_HDG_ErrorEstimate<NDPDEClass,BCNDConvert,BCVector,DiscTag,XFieldType,OutputIntegrandType>::
setInitialSolution(const Field<PhysDim, TopoDim, ArrayQ>& qfldFrom)
{
  ProjectGlobalField(qfldFrom, qfld_);
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class DiscTag, class XFieldType, class OutputIntegrandType>
SolverInterface_impl<ProblemStatement_HDG_ErrorEstimate<NDPDEClass,BCNDConvert,BCVector,DiscTag,XFieldType,OutputIntegrandType>>
ProblemStatement_HDG_ErrorEstimate<NDPDEClass,BCNDConvert,BCVector,DiscTag,XFieldType,OutputIntegrandType>::
CellwiseOperations(const std::vector<int>& cellgroup_list, ProblemStatementClass& base)
{
  return SolverInterface_impl<ProblemStatementClass>(cellgroup_list, base);
}


template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class DiscTag, class XFieldType, class OutputIntegrandType>
template<class Topology>
void
ProblemStatement_HDG_ErrorEstimate<NDPDEClass,BCNDConvert,BCVector,DiscTag,XFieldType,OutputIntegrandType>::
getSolutionInfo_cellgroup(
    const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
    const int cellGroupGlobal)
{
  typedef typename QFieldType::template FieldCellGroupType<Topology> QFieldCellGroupType;
  const QFieldCellGroupType& cellgrp = qfld_.template getCellGroup<Topology>(cellGroupGlobal);

  nDOFperCell_[cellGroupGlobal] = cellgrp.basis()->nBasis();
  orderVec_[cellGroupGlobal]    = cellgrp.basis()->order();
}

}

#endif /* PROBLEMSTATEMENT_HDG_ERRORESTIMATE_H_ */
