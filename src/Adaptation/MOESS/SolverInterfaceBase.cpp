// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "SolverInterfaceBase.h"     // Real

namespace SANS
{

template class SolverInterfaceBase<PhysD1, TopoD1>;
template class SolverInterfaceBase<PhysD2, TopoD2>;
template class SolverInterfaceBase<PhysD3, TopoD3>;
template class SolverInterfaceBase<PhysD4, TopoD4>;

}
