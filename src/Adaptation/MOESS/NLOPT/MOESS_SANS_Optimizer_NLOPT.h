// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef MOESS_SANS_OPTIMIZER_NLOPT_H_
#define MOESS_SANS_OPTIMIZER_NLOPT_H_

#include <ostream>
#include <vector>
#include <memory> // std::shared_ptr

#include "tools/SANSnumerics.h"     // Real
#include "Topology/Dimension.h"
#include "Topology/ElementTopology.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"

#include "Adaptation/MOESS/NodalMetrics.h"
#include "Adaptation/MOESS/ErrorModel.h"
#include "Adaptation/MOESS/MOESS_SANS_Optimizer_Base.h"

namespace SANS
{

class NLOPT;

template <class PhysDim, class TopoDim, class Optimizer>
class MOESS_SANS_Optimizer;

template <class PhysDim, class TopoDim>
class MOESS_SANS_Optimizer<PhysDim,TopoDim,NLOPT> : public MOESS_SANS_Optimizer_Base<PhysDim,TopoDim>
{
public:
  typedef MOESS_SANS_Optimizer_Base<PhysDim,TopoDim> BaseType;

  static const int D = PhysDim::D;   // physical dimensions

  typedef NodalMetrics<PhysDim,TopoDim> NodalMetricsType;
  typedef typename NodalMetricsType::ParamsType ParamsType;
  typedef typename NodalMetricsType::MatrixSymFieldType MatrixSymFieldType;

  typedef ErrorModel<PhysDim,TopoDim> ErrorModelType;

  typedef DLA::MatrixSymS<D,Real> MatrixSym;

  MOESS_SANS_Optimizer( const Real targetCost, const Real h_domain_max,
                        std::shared_ptr<ErrorModelType>& errorModel,
                        const XField<PhysDim,TopoDim>& xfld_linear,
                        const std::vector<int>& cellgroup_list,
                        const SolverInterfaceBase<PhysDim,TopoDim>& problem,
                        const PyDict& paramsDict );

  //The function that NLOPT calls to evaluate the objective function
  static double NLOPT_Objective(unsigned n, const double* x, double* grad, void* f_data);

  //The function that NLOPT calls to evaluate the cost constraint
  static double NLOPT_CostConstraint(unsigned n, const double* x, double* grad, void* f_data);

protected:
  // performs the optimization
  void optimize(const Real h_domain_max, const SpaceType space, const PyDict& paramsDict);

  //Computes the objective function and its derivative wrt optimization variables
  double computeObjective(unsigned n, const double* x, double* grad);

  //Computes the cost constraint and its derivative wrt optimization variables
  //Constraint form: computeCostConstraint(x) <= 0
  double computeCostConstraint(unsigned n, const double* x, double* grad);

  std::shared_ptr<MatrixSymFieldType> pSfld_;

  using BaseType::errorModel_;
  using BaseType::nodalMetrics_;
  using BaseType::h_coarsen_factors_;
  using BaseType::h_refine_factor_;
  using BaseType::targetCost_;
  using BaseType::error0_;
  using BaseType::cost_initial_estimate_;
  using BaseType::cost_final_estimate_;
  using BaseType::objective_reduction_ratio_;
};

}

#endif /* MOESS_SANS_OPTIMIZER_NLOPT_H_ */
