// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef MOESS_PX_OPTIMIZER_NLOPT_H_
#define MOESS_PX_OPTIMIZER_NLOPT_H_

#include <ostream>
#include <vector>
#include <memory> // std::shared_ptr

#include "tools/SANSnumerics.h"     // Real
#include "Topology/Dimension.h"
#include "Topology/ElementTopology.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"

#include "Adaptation/MOESS/NodalMetrics.h"
#include "Adaptation/MOESS/ErrorModel.h"

namespace SANS
{

class NLOPT;

template <class PhysDim, class TopoDim, class Optimizer>
class MOESS_PX_Optimizer;


template <class PhysDim, class TopoDim>
class MOESS_PX_Optimizer<PhysDim,TopoDim,NLOPT>
{
public:
  static const int D = PhysDim::D;   // physical dimensions

  typedef NodalMetrics<PhysDim,TopoDim> NodalMetricsType;
  typedef typename NodalMetricsType::ParamsType ParamsType;
  typedef typename NodalMetricsType::MatrixSymFieldType MatrixSymFieldType;

  typedef ErrorModel<PhysDim,TopoDim> ErrorModelType;

  typedef DLA::MatrixSymS<D,Real> MatrixSym;

  MOESS_PX_Optimizer( const Real targetCost, const Real h_domain_max,
                      std::shared_ptr<ErrorModelType>& errorModel,
                      const XField<PhysDim,TopoDim>& xfld_linear,
                      const std::vector<int>& cellgroup_list,
                      const SolverInterfaceBase<PhysDim,TopoDim>& problem,
                      const PyDict& paramsDict );

  //The function that NLOPT calls to evaluate the objective function
  static double NLOPT_Objective(unsigned n, const double* x, double* grad, void* f_data);

  //The function that NLOPT calls to evaluate the cost constraint
  static double NLOPT_CostConstraint(unsigned n, const double* x, double* grad, void* f_data);

  //The function that NLOPT calls to evaluate the global Frobenius norm squared sum constraint
  static double NLOPT_FrobNormSqSumConstraint(unsigned n, const double* x, double* grad, void* f_data);

  std::shared_ptr<NodalMetricsType> nodalMetrics() const { return nodalMetrics_; }

  Real eval_count() const { return eval_count_; }
  Real cost_initial_estimate() const { return cost_initial_estimate_; }
  Real cost_final_estimate() const { return cost_final_estimate_; }
  Real objective_reduction_ratio() const { return objective_reduction_ratio_; }

protected:
  // performs the optimization
  void optimize(const Real h_domain_max, const PyDict& paramsDict);

  //Computes the objective function and its derivative wrt optimization variables
  double computeObjective(unsigned n, const double* x, double* grad);

  //Computes the cost constraint and its derivative wrt optimization variables
  //Constraint form: computeCostConstraint(x) <= 0
  double computeCostConstraint(unsigned n, const double* x, double* grad);

  //Computes the Frobenius norm squared sum constraint and its derivative wrt optimization variables
  double computeFrobNormSqSumConstraint(unsigned n, const double* x, double* grad);

  //Sets bounds on the step matrix values based on maximum edge refinement factors
  void setStepMatrixBounds(const Real h_refine_factor, const std::vector<Real>& h_coarsen_factors);

  //Computes the desired (target) squared Frobenius norm of the step matrix at each node
  void computeFrobNormSqTargets();

  void computeFrobNormPenalty(const MatrixSymFieldType& Sfld, Real& penalty, std::vector<MatrixSym>& dpenalty_dSvec);
  void computeFrobNormPenalty(const MatrixSymFieldType& Sfld, Real& penalty);

  //Check resulting step-matrices to see if eigenvalues are above bounds and increase penalty weights
  int handleStepMatrixViolations(const std::vector<Real>& x, const Real h_refine_factor, const std::vector<Real>& h_coarsen_factors);

  Real targetCost_;
  std::shared_ptr<ErrorModelType> errorModel_;
  std::shared_ptr<NodalMetricsType> nodalMetrics_;

  std::shared_ptr<MatrixSymFieldType> pSfld_;

  Real error0_;          //initial error scaling applied to error estimate function
  Real h_refine_factor_; // maximum refinement factor
  Real FrobNormSqSum_Fraction_;

  int eval_count_; //No. of objective function evaluations

  Real cost_initial_estimate_ = 0.0;
  Real cost_final_estimate_ = 0.0;

  std::vector<Real> FrobNormSq_targets_; //target Frobenius norm sq of each nodal step matrix
  std::vector<Real> penalty_weights_;    //nodal weights for Frobenius norm sq penalty term

  std::vector<Real> objective_history_; //History of objective function evaluations
  std::vector<Real> objective_gradnorm_history_; //History of the norm of the objective function gradient
  Real objective_reduction_ratio_ = 0.0; //Reduction in objective function value as a result of optimization = (final_obj/initial_obj)

  std::vector<Real> cost_constraint_history_; //History of cost constraint evaluations
  std::vector<Real> cost_constraint_gradnorm_history_; //History of cost constraint evaluations

  std::vector<Real> step_constraint_history_; //History of step matrix constraint evaluations
  std::vector<Real> step_constraint_gradnorm_history_; //History of step matrix constraint evaluations

  std::vector<Real> gradation_constraint_history_; //History of gradation constraint evaluations
};

}

#endif /* MOESS_PX_OPTIMIZER_NLOPT_H_ */
