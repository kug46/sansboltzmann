// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define IMPLIEDMETRIC_OPTIMIZER_NLOPT_INSTANTIATE
#include "ImpliedMetric_Optimizer_NLOPT_impl.h"

#include "Field/XFieldLine.h"

#include "Field/FieldLine_CG_Cell.h"

namespace SANS
{

//Explicit instantiations
template class ImpliedMetric_Optimizer<PhysD1,TopoD1,NLOPT>;

}
