// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(IMPLIEDMETRIC_OPTIMIZER_NLOPT_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "../ImpliedMetric_Optimizer_NLOPT.h"

#include <nlopt.hpp>

#include <vector>

#include "../nloptResultDescription.h"

#include "Adaptation/MOESS/NodalMetrics.h"

#include "Surreal/SurrealS.h"
#include "tools/smoothmath.h"

#include "Meshing/Metric/MetricOps.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

template <class PhysDim, class TopoDim>
ImpliedMetric_Optimizer<PhysDim,TopoDim,NLOPT>::
ImpliedMetric_Optimizer( NodalMetricsType& nodalMetrics )
  : nodalMetrics_(nodalMetrics),
    logMfld_(nodalMetrics.xfld_linear(), 1, BasisFunctionCategory_Lagrange, nodalMetrics.cellgroup_list())
{
  SANS_ASSERT(nodalMetrics.xfld_linear().comm()->size() == 1);

  const int nNode = logMfld_.nDOF();
  const int nDim = nNode*MatrixSym::SIZE;

  //nlopt optimizer object
  nlopt::opt opt(nlopt::LD_MMA, nDim);

  opt.set_min_objective(NLOPT_Objective, reinterpret_cast<void*>(this) );

  //Termination conditions
  Real xtol_rel = nodalMetrics_.paramsDict_.get(ParamsType::params.ImpliedMetric_XTol_Rel);
  Real ftol_rel = nodalMetrics_.paramsDict_.get(ParamsType::params.ImpliedMetric_FTol_Rel);
  int max_eval  = nodalMetrics_.paramsDict_.get(ParamsType::params.ImpliedMetric_MaxEval);

  // we are done if the objective function falls below this
  if (quality_calc_ == QualityCalculation::none)
  {
    const Real tol = 1e-7;
    opt.set_stopval(tol);
  }
  else if (quality_calc_ == QualityCalculation::non_invert)
  {
    const Real tol = 1e-4;
    opt.set_stopval(-1+tol); // mean quality of 0.999
  }
  else if (quality_calc_ == QualityCalculation::invert)
  {
    const Real tol = 1e-4;
    opt.set_stopval(1+tol); // mean quality of 0.999
  }

  //stop when every parameter changes by less than the tolerance multiplied by the absolute value of the parameter.
  opt.set_xtol_rel(xtol_rel);

  //stop when the objective function value changes by less than the tolerance multiplied by the absolute value of the function value
  opt.set_ftol_rel(ftol_rel);

  //stop when the maximum number of function evaluations is reached
  opt.set_maxeval(max_eval);

  eval_count_ = 0;

  //Initialize solution vector the log of the metric
  std::vector<Real> x(nDim);

  for (int node = 0; node < nNode; node++)
  {
    MatrixSym logM = log(nodalMetrics_.implied_metric_.DOF(node));

    logMfld_.DOF(node) = logM;
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      x[node*MatrixSym::SIZE + ind] = logM.value(ind);
  }

  // precompute the set of edges for updating the lengths

  std::vector<MatrixSym> d_dlogM;

  //Compute the global complexity and its derivatives
  Real complexity = 0.0;
  nodalMetrics_.computeComplexity(logMfld_, complexity, d_dlogM);

  //Compute the global error estimate and its derivatives
  Real EdgeLenDev = 0.0;
  nodalMetrics_.computeEdgeLengthDeviation(logMfld_, EdgeLenDev, d_dlogM);

  //Compute a gradation penalty
  Real GradationDev = 0.0;
  nodalMetrics_.computeGradationPenalty_logM(logMfld_, GradationDev, d_dlogM);

  //Compute a step matrix deviation
  Real StepDev = 0.0;
  nodalMetrics_.computeStepDeviation_logM(logMfld_, StepDev, d_dlogM);

  Real Qmean,Qmin,Qmax;
  nodalMetrics_.computeQualityStatistics_logM(logMfld_,Qmean,Qmin,Qmax);

  // Comptue the initaial objective
  double f_opt = NLOPT_Objective(x.size(), x.data(), nullptr, (void*)this);

  std::cout << std::scientific << std::setprecision(4) << std::endl;
  std::cout << "------------------------------------------" << std::endl;
  std::cout << "Starting NLopt Implied Metric Optimization" << std::endl;
  std::cout << "   Variables                : " << nDim << std::endl;
  std::cout << "   Target Complexity        : " << nodalMetrics_.complexity_initial_true_ << std::endl;
  std::cout << "   Implied Complexity       : " << complexity << std::endl;
  std::cout << "   Edge Length Deviation    : " << EdgeLenDev << std::endl;
  std::cout << "   Gradation                : " << GradationDev << std::endl;
  std::cout << "   Step Deviation           : " << StepDev << std::endl;
  std::cout << "   Avg, Min, Max Quality    : " << Qmean << ", " << Qmin << ", " << Qmax << std::endl;
  std::cout << "   Initial Objective        : " << f_opt << std::endl;
  std::cout << "------------------------------------------" << std::endl;

  eval_count_ = 0;
  nlopt::result result = nlopt::result::FAILURE;

  try
  {
    result = opt.optimize(x, f_opt);
  }
  catch (const nlopt::roundoff_limited& failure )
  {
    SANS_DEVELOPER_EXCEPTION( "NLopt - Roundoff failure : %s", failure.what());
  }
  catch (const nlopt::forced_stop& failure )
  {
    SANS_DEVELOPER_EXCEPTION( "NLopt - Forced stop : %s", failure.what());
  }
  catch (const std::runtime_error& failure )
  {
    SANS_DEVELOPER_EXCEPTION( "NLopt - Runtime failure : %s", failure.what());
  }
  catch (const std::invalid_argument& failure )
  {
    SANS_DEVELOPER_EXCEPTION( "NLopt - Invalid Arguments failure : %s", failure.what());
  }
  catch (const std::bad_alloc& failure )
  {
    SANS_DEVELOPER_EXCEPTION( "NLopt - Out of Memory failure : %s", failure.what());
  }

  // compute the gradient so the norm can be reported
  std::vector<Real> grad(nDim);
  NLOPT_Objective(x.size(), x.data(), grad.data(), (void*)this);

  MatrixSym M;
  double gnorm = 0;
  for (int node = 0; node < nNode; node++)
  {
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
    {
      gnorm += pow(grad[node*MatrixSym::SIZE + ind],2);
      logMfld_.DOF(node).value(ind) = x[node*MatrixSym::SIZE + ind];
    }

    //Update the implied metric with the optimized log metric if the denominator is positive
    M = exp(logMfld_.DOF(node));
    Real detM = DLA::Det(M);
    if ( detM <= 0.0 )
    {
      std::cout << "WARNING: negative nodal metric: node = " << node << " det(M) = " << detM << std::endl;
      std::cout << M << std::endl;
    }
    else
      nodalMetrics_.implied_metric_.DOF(node) = M;
  }
  gnorm = sqrt(gnorm);

  //Compute the global complexity
  complexity = 0;
  nodalMetrics_.computeComplexity(logMfld_, complexity, d_dlogM);

  //Compute the global edge length deviation
  EdgeLenDev = 0;
  nodalMetrics_.computeEdgeLengthDeviation(logMfld_, EdgeLenDev, d_dlogM);

  //Compute a gradation penalty
  GradationDev = 0.0;
  nodalMetrics_.computeGradationPenalty_logM(logMfld_, GradationDev, d_dlogM);

  //Compute a step matrix deviation
  StepDev = 0.0;
  nodalMetrics_.computeStepDeviation_logM(logMfld_, StepDev, d_dlogM);

  nodalMetrics_.computeQualityStatistics_logM(logMfld_,Qmean,Qmin,Qmax);

  std::cout << "   Eval count               : " << eval_count_ << std::endl;
  std::cout << "   Result                   : " << nloptResultDescription(result) << std::endl;
  std::cout << "   Target Complexity        : " << nodalMetrics_.complexity_initial_true_ << std::endl;
  std::cout << "   Implied Complexity       : " << complexity << std::endl;
  std::cout << "   Edge Len Deviation       : " << EdgeLenDev << std::endl;
  std::cout << "   Gradation                : " << GradationDev << std::endl;
  std::cout << "   Step Deviation           : " << StepDev << std::endl;
  std::cout << "   Mean, Min, Max Quality   : " << Qmean << ", " << Qmin << ", " << Qmax << std::endl;
  std::cout << "   Objective                : " << f_opt << std::endl;
  std::cout << "   ||g(X)||                 : " << gnorm << std::endl;
  std::cout << "------------------------------------------" << std::endl;
  std::cout << std::endl;
  std::cout << std::fixed;
}

//---------------------------------------------------------------------------//
//The function that NLOPT calls to evaluate the objetive function and it's gradient
template <class PhysDim, class TopoDim>
double
ImpliedMetric_Optimizer<PhysDim,TopoDim,NLOPT>::
NLOPT_Objective(unsigned n, const double* x, double* grad, void* f_data)
{
  ImpliedMetric_Optimizer<PhysDim,TopoDim,NLOPT>* solver = reinterpret_cast<ImpliedMetric_Optimizer<PhysDim,TopoDim,NLOPT>*>(f_data);

  return (*solver).computeComplexityObjective(n, x, grad); //forward the call
  //return (*solver).computeQualityObjective(n, x, grad); //forward the call
  //return (*solver).computeStepObjective(n, x, grad); //forward the call
}

//---------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
double
ImpliedMetric_Optimizer<PhysDim,TopoDim,NLOPT>::
computeComplexityObjective(unsigned n, const double* x, double* grad)
{
  eval_count_++;

  const int nNode = logMfld_.nDOF();
  SANS_ASSERT( nNode*MatrixSym::SIZE == (int)n );

  //Populate l-matrices from the solution vector x
  for (int node = 0; node < nNode; node++)
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      logMfld_.DOF(node).value(ind) = x[node*MatrixSym::SIZE + ind];

  Real complexity = 0.0;
  std::vector<MatrixSym> dcomplexity_dlogM(grad == nullptr ? 0 : nNode, 0.0);

  //Compute the global complexity estimate and its derivatives
  nodalMetrics_.computeComplexity(logMfld_, complexity, dcomplexity_dlogM);
  complexity /= nodalMetrics_.complexity_initial_true_;

  Real EdgeLenDev = 0.0;
  std::vector<MatrixSym> dEdgeLenDev_dlogM(grad == nullptr ? 0 : nNode, 0.0);

  //Compute the edge length deviation from the allowable lengths
  nodalMetrics_.computeEdgeLengthDeviation(logMfld_, EdgeLenDev, dEdgeLenDev_dlogM);

  //Compute a gradation penalty
  //Real GradationDev = 0.0;
  //std::vector<MatrixSym> dGradationDev_dlogM(grad == nullptr ? 0 : nNode, 0.0);
  //nodalMetrics_.computeGradationPenalty_logM(logMfld_, GradationDev, dGradationDev_dlogM);

  // Complexity ideally matches the initial complexity
  Real obj = pow( complexity - 1, 2.0 ) + EdgeLenDev; // + GradationDev;

  if (grad)
  {
    //Populate grad vector from dError_dSvec
    for (int node = 0; node < nNode; node++)
      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
        grad[node*MatrixSym::SIZE + ind] =
            2.0*(complexity - 1)*dcomplexity_dlogM[node].value(ind)/nodalMetrics_.complexity_initial_true_ +
                                 dEdgeLenDev_dlogM[node].value(ind); // +
                                 //dGradationDev_dlogM[node].value(ind);
  }

  return obj;
}

//---------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
double
ImpliedMetric_Optimizer<PhysDim,TopoDim,NLOPT>::
computeQualityObjective(unsigned n, const double* x, double* grad)
{
  eval_count_++;

  const int nNode = logMfld_.nDOF();
  SANS_ASSERT( nNode*MatrixSym::SIZE == (int)n );

  //Populate l-matrices from the solution vector x
  for (int node = 0; node < nNode; node++)
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      logMfld_.DOF(node).value(ind) = x[node*MatrixSym::SIZE + ind];

  Real complexity = 0.0;
  std::vector<MatrixSym> dcomplexity_dlogM(grad == nullptr ? 0 : nNode, 0.0);

  //Compute the global complexity estimate and its derivatives
  nodalMetrics_.computeComplexity(logMfld_, complexity, dcomplexity_dlogM);
  complexity /= nodalMetrics_.complexity_initial_true_;

  Real QAvg = 0.0;
  std::vector<MatrixSym> dQAvg_dlogM(grad == nullptr ? 0 : nNode, 0.0);

  // compute the (inverted) quality sum under the metric
  if (quality_calc_ == QualityCalculation::invert)
    nodalMetrics_.computeQualityAverage(logMfld_, QAvg, dQAvg_dlogM, true);
  else if (quality_calc_ == QualityCalculation::non_invert)
    nodalMetrics_.computeQualityAverage(logMfld_, QAvg, dQAvg_dlogM, false);


  Real EdgeLenDev = 0.0;
  std::vector<MatrixSym> dEdgeLenDev_dlogM(grad == nullptr ? 0 : nNode, 0.0);

  // Compute the edge length deviation from the allowable lengths
  nodalMetrics_.computeEdgeLengthDeviation(logMfld_, EdgeLenDev, dEdgeLenDev_dlogM);

  // real objective is Qavg, EdgeLenDev is a penalty implementation of the edge length constraint
  Real obj = pow( complexity - 1, 2.0 ) + EdgeLenDev;

  // compute the (inverted) quality sum under the metric
  if (quality_calc_ == QualityCalculation::invert)
    obj += QAvg; // minimizing average 1/Q
  else if (quality_calc_ == QualityCalculation::non_invert)
    obj += QAvg; // minimizing average 1-Q (a.k.a maximizing average Q)

  if (grad)
  {
    //Populate grad vector from dError_dSvec
    for (int node = 0; node < nNode; node++)
      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      {
        grad[node*MatrixSym::SIZE + ind] = dEdgeLenDev_dlogM[node].value(ind)
        + 2.0*(complexity - 1)*dcomplexity_dlogM[node].value(ind)/nodalMetrics_.complexity_initial_true_;

        // compute the (inverted) quality sum under the metric
        if (quality_calc_ == QualityCalculation::invert)
          grad[node*MatrixSym::SIZE + ind] += dQAvg_dlogM[node].value(ind);
        else if (quality_calc_ == QualityCalculation::non_invert)
          grad[node*MatrixSym::SIZE + ind] += dQAvg_dlogM[node].value(ind);

      }
  }

  // if (eval_count_%100 == 0)
  // if (inverted_)
  // {
  //   std::cout << "eval, objm1, comp, Qm1, Edge = " << eval_count_ << ", "
  //         << obj-1 << ", " <<  pow( complexity - 1, 2.0 ) << ", " <<  QAvg-1 << ", " <<  EdgeLenDev << std::endl;
  // }
  // else
  // {
  //   std::cout << "eval, objp1, comp, Qm1, Edge = " << eval_count_ << ", "
  //         << obj+1 << ", " <<  pow( complexity - 1, 2.0 ) << ", " <<  1-QAvg << ", " <<  EdgeLenDev << std::endl;
  // }
  //
  // if (obj != obj)
  // {
  //   std::cout << "eval = " << eval_count_ << " obj - 1 = " << obj-1 << std::endl;
  //   SANS_DEVELOPER_EXCEPTION("nan");
  // }
  return obj;
}

//---------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
double
ImpliedMetric_Optimizer<PhysDim,TopoDim,NLOPT>::
computeStepObjective(unsigned n, const double* x, double* grad)
{
  eval_count_++;

  const int nNode = logMfld_.nDOF();
  SANS_ASSERT( nNode*MatrixSym::SIZE == (int)n );

  //Populate l-matrices from the solution vector x
  for (int node = 0; node < nNode; node++)
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      logMfld_.DOF(node).value(ind) = x[node*MatrixSym::SIZE + ind];

  Real StepDev = 0.0;
  std::vector<MatrixSym> dStepDev_dlogM(grad == nullptr ? 0 : nNode, 0.0);

  // Compute the edge length deviation from the allowable lengths
  nodalMetrics_.computeStepDeviation_logM(logMfld_, StepDev, dStepDev_dlogM);

  // real objective
  Real obj = StepDev;

  if (grad)
  {
    //Populate grad vector from dError_dSvec
    for (int node = 0; node < nNode; node++)
      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      {
        grad[node*MatrixSym::SIZE + ind] = dStepDev_dlogM[node].value(ind);
      }
  }

  return obj;
}

} // namespace SANS
