// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(MOESS_SANS_OPTIMIZER_NLOPT_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "../MOESS_SANS_Optimizer_NLOPT.h"

#include <nlopt.hpp>

#include <vector>
#include <limits>

#include "../nloptResultDescription.h"

#include "Adaptation/MOESS/MOESS.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_FrobNorm.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Trace.h"

#include "Surreal/SurrealS.h"
#include "tools/smoothmath.h"

#include <unistd.h> // gethostname
#include <limits.h> // HOST_NAME_MAX
#ifndef HOST_NAME_MAX
#  if defined(_POSIX_HOST_NAME_MAX)
#    define HOST_NAME_MAX _POSIX_HOST_NAME_MAX
#  elif defined(_SC_HOST_NAME_MAX)
#    define HOST_NAME_MAX _SC_HOST_NAME_MAX
#  else
#    define HOST_NAME_MAX 255
#  endif
#endif // HOST_NAME_MAX

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

//---------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
MOESS_SANS_Optimizer<PhysDim,TopoDim,NLOPT>::
MOESS_SANS_Optimizer( const Real targetCost, const Real h_domain_max,
                      std::shared_ptr<ErrorModelType>& errorModel,
                      const XField<PhysDim,TopoDim>& xfld_linear,
                      const std::vector<int>& cellgroup_list,
                      const SolverInterfaceBase<PhysDim,TopoDim>& problem,
                      const PyDict& paramsDict )
  : BaseType(targetCost, errorModel)
{
#ifdef SANS_MPI
  // create a serialize grid on processor 0
  XField<PhysDim,TopoDim> xfld_rank0(xfld_linear, XFieldBalance::Serial);

  std::shared_ptr<ErrorModel<PhysDim,TopoDim>> errorModelGlobal = errorModel_;

  // serialize the error model
  errorModel_ = std::make_shared<ErrorModelType>(xfld_rank0, cellgroup_list, problem, paramsDict);
  errorModel_->serialize(*errorModelGlobal);

  // construct the serial step matrix field
  pSfld_ = std::make_shared<MatrixSymFieldType>(xfld_rank0, 1, BasisFunctionCategory_Lagrange, cellgroup_list);

#else
  // construct the step matrix field
  pSfld_ = std::make_shared<MatrixSymFieldType>(xfld_linear, 1, BasisFunctionCategory_Lagrange, cellgroup_list);
#endif

  if (xfld_linear.comm()->rank() == 0)
  {
#ifdef SANS_MPI
    char hostname[HOST_NAME_MAX];
    gethostname(hostname, HOST_NAME_MAX);
    std::cout << std::endl;
    std::cout << "NLopt SANS optimizing metrics serially on host: " << hostname << std::endl;
    std::cout << std::endl;

    nodalMetrics_ = std::make_shared<NodalMetricsType>(xfld_rank0, cellgroup_list,
                                                       problem, paramsDict);

#else
    nodalMetrics_ = std::make_shared<NodalMetricsType>(xfld_linear, cellgroup_list,
                                                       problem, paramsDict);
#endif

    // perform the optimization
    optimize(h_domain_max, problem.spaceType(), paramsDict);
  }

#ifdef SANS_MPI
  std::shared_ptr<NodalMetricsType> nodalMetrics_rank0 = nodalMetrics_;

  nodalMetrics_ = std::make_shared<NodalMetricsType>(xfld_linear, cellgroup_list,
                                                     problem, paramsDict, *nodalMetrics_rank0);

  errorModel_ = errorModelGlobal;

  // just so the other processors wait while rank 0 performs the optimization
  xfld_linear.comm()->barrier();
#endif
}

//-----------------------------------------------------------------------------
template <class PhysDim, class TopoDim>
void
MOESS_SANS_Optimizer<PhysDim,TopoDim,NLOPT>::
optimize(const Real h_domain_max, const SpaceType space, const PyDict& paramsDict)
{
  MatrixSymFieldType& Sfld = *pSfld_;

  const int nNode = Sfld.nDOF();
  const int nDim = nNode*MatrixSym::SIZE;

  //nlopt optimizer object
  nlopt::opt opt(nlopt::LD_MMA, nDim);

  opt.set_min_objective        ( NLOPT_Objective     , reinterpret_cast<void*>(this) );
  opt.add_inequality_constraint( NLOPT_CostConstraint, reinterpret_cast<void*>(this) );

  //Termination conditions
  Real xtol_rel = paramsDict.get(ParamsType::params.Optimizer_XTol_Rel);
  Real ftol_rel = paramsDict.get(ParamsType::params.Optimizer_FTol_Rel);
  int max_eval  = paramsDict.get(ParamsType::params.Optimizer_MaxEval);

  //stop when every parameter changes by less than the tolerance multiplied by the absolute value of the parameter.
  opt.set_xtol_rel(xtol_rel);

  //stop when the objective function value changes by less than the tolerance multiplied by the absolute value of the function value
  opt.set_ftol_rel(ftol_rel);

  //stop when the maximum number of function evaluations is reached
  opt.set_maxeval(max_eval);

  error0_ = 1.0;

  h_refine_factor_ = sqrt(2); //paramsDict.get(ParamsType::params.hRefineFactorMax);
  Real beta;
  if (space == SpaceType::Continuous)
    beta = 1.1; // Must be consistent with ErrorModel::computeNodalError
  else
    beta = 1.5; // Must be consistent with ErrorModel::computeError_cellgroup
  h_refine_factor_ = pow(h_refine_factor_, sqrt(D)*beta);

  h_coarsen_factors_.resize(nNode);

  nodalMetrics_->initializeCostStepMatrices(h_domain_max, h_refine_factor_,
                                            targetCost_, cost_initial_estimate_,
                                            Sfld, h_coarsen_factors_);

  // set the step matrix hard bounds to help NLOPT stay on track
  {
    std::vector<Real> lower_bound_vector(nDim);
    std::vector<Real> upper_bound_vector(nDim);

    for (int node = 0; node < nNode; node++)
    {
      //Set bounds on the step matrix at this node
      Real lower_bound = -2.0*log(h_coarsen_factors_[node]); //max coarsening factor = h_refine_factor_limited
      Real upper_bound = +2.0*log(h_refine_factor_);         //max refinement factor = h_refine_factor_limited

      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      {
        lower_bound_vector[node*MatrixSym::SIZE + ind] = lower_bound;
        upper_bound_vector[node*MatrixSym::SIZE + ind] = upper_bound;
      }
    } //loop over nodes

    opt.set_lower_bounds(lower_bound_vector);
    opt.set_upper_bounds(upper_bound_vector);
  }

  std::vector<MatrixSym> d_dSvec;

  //Initialize solution vector
  std::vector<Real> x(nDim, 0.0);

  //Set the initial value
  for (int node = 0; node < nNode; node++)
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      x[node*MatrixSym::SIZE + ind] = Sfld.DOF(node).value(ind);

  //Compute the global error estimate as the scaling factor
  errorModel_->computeError(Sfld, error0_, d_dSvec);

  // just so we don't divide by zero
  if (error0_ == 0) error0_ = std::numeric_limits<Real>::epsilon();

  // get the initial value with the penalty terms possibly active
  Real f_init = this->computeObjective(nDim, x.data(), nullptr);
  Real f_opt = 0.0;

  std::cout << std::endl;
  std::cout << "------------------------------------------" << std::endl;
  std::cout << "Starting NLopt Optimization" << std::endl;
  std::cout << "------------------------------------------" << std::endl;
  std::cout << "    Variables         : " << nDim        << std::endl;
  std::cout << "------------------------------------------" << std::endl;
  std::cout << std::endl;

  nlopt::result result = nlopt::result::FAILURE;

  try
  {
    result = opt.optimize(x, f_opt);
  }
  catch (const std::runtime_error& failure )
  {
    SANS_DEVELOPER_EXCEPTION( "NLopt - Runtime failure.");
  }

  Real SnormSq = 0.;
  Real trSq = 0.;
  int maxFrobNode = 0;
  int maxTrNode = 0;
  for (int node = 0; node < nNode; node++)
  {
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      Sfld.DOF(node).value(ind) = x[node*MatrixSym::SIZE + ind];

    Real SnormSq_temp = DLA::FrobNormSq(Sfld.DOF(node));
    if ( SnormSq_temp > SnormSq )
    {
      SnormSq = SnormSq_temp;
      maxFrobNode = node;
    }
    Real trSq_temp = pow(DLA::tr(Sfld.DOF(node)),2);
    if ( trSq_temp > trSq )
    {
      trSq = trSq_temp;
      maxTrNode = node;
    }
  }

  Real frobNormSqTargetOuter = pow(sqrt(D)*2.0*log(h_refine_factor_),2.0);
  Real frobNormSqTargetInner = pow(        2.0*log(h_refine_factor_),2.0);
  Real frobNormSqTargetDel = D == 1 ? 1e-2 : frobNormSqTargetOuter/frobNormSqTargetInner - 1;

  // calculate the penalty for the max norm node
  Real maxSpenalty;
  this->frobNormPenalty( Sfld.DOF(maxFrobNode), frobNormSqTargetInner, frobNormSqTargetDel, maxSpenalty );

  // calculate the penalty for the trace square
  Real maxTrPenalty;
  this->tracePenalty( Sfld.DOF(maxTrNode), frobNormSqTargetInner, frobNormSqTargetDel, maxTrPenalty );

  //Compute the global error estimate
  Real errEst = 0;
  errorModel_->computeError(Sfld, errEst, d_dSvec);

  //Compute the cost estimate
  nodalMetrics_->computeCost(Sfld, cost_final_estimate_, d_dSvec);

  // Compute the eigen value vioalations
  int eigenvalue_violation_count = this->countStepMatrixViolations(Sfld);

  // Compute the average frobenius norm of the change
  Real avgStepNorm = this->computeAverageFrobeniusNorm(Sfld);

  std::cout << "------------------------------------------" << std::endl;
  std::cout << "MOESS NLopt Summary" << std::endl;
  std::cout << "------------------------------------------" << std::endl;
  std::cout << "    Result            : " << nloptResultDescription(result) <<std::endl;
  std::cout << "    Total eval count  : " << this->eval_count_ << std::endl;
  std::cout << "    Initial objective : " << f_init      << std::endl;
  std::cout << "    Final objective   : " << f_opt       << std::endl;
  std::cout << "    Cost              : " << cost_final_estimate_ << std::endl;
  std::cout << "    Elem Req.         : " << nodalMetrics_->computeRequestedElements(Sfld) << std::endl;
  std::cout << "    Max Step Penalty  : " << maxSpenalty << std::endl;
  std::cout << "    Max Tr Penalty    : " << maxTrPenalty << std::endl;
  std::cout << "    Eigen violations  : " << eigenvalue_violation_count << std::endl;
  std::cout << "    Avg Step FrobNorm : " << avgStepNorm    << std::endl;
  std::cout << "    Error Est.        : " << errEst         << std::endl;
  std::cout << "    Error Reduction   : " << errEst/error0_ << std::endl;
  std::cout << "------------------------------------------" << std::endl;

  objective_reduction_ratio_ = f_opt/f_init;

  nodalMetrics_->setNodalStepMatrix(Sfld);
}

//---------------------------------------------------------------------------//
//The function that NLopt calls to evaluate the objective function
template <class PhysDim, class TopoDim>
double
MOESS_SANS_Optimizer<PhysDim,TopoDim,NLOPT>::NLOPT_Objective(unsigned n, const double* x, double* grad, void* f_data)
{
  MOESS_SANS_Optimizer<PhysDim,TopoDim, NLOPT>* solver = reinterpret_cast<MOESS_SANS_Optimizer<PhysDim,TopoDim,NLOPT>*>(f_data);

  return solver->computeObjective(n, x, grad); //forward the call
}

//---------------------------------------------------------------------------//
//The function that NLopt calls to evaluate the cost constraint
template <class PhysDim, class TopoDim>
double
MOESS_SANS_Optimizer<PhysDim,TopoDim,NLOPT>::NLOPT_CostConstraint(unsigned n, const double* x, double* grad, void* f_data)
{
  MOESS_SANS_Optimizer<PhysDim,TopoDim, NLOPT>* solver = reinterpret_cast<MOESS_SANS_Optimizer<PhysDim,TopoDim,NLOPT>*>(f_data);

  return solver->computeCostConstraint(n, x, grad); //forward the call
}

//---------------------------------------------------------------------------//
//Routine that computes the objective function and its derivative wrt optimization variables
template <class PhysDim, class TopoDim>
double
MOESS_SANS_Optimizer<PhysDim,TopoDim,NLOPT>::computeObjective(unsigned n, const double* x, double* grad)
{
  MatrixSymFieldType& Sfld = *pSfld_;

  const int nNode = Sfld.nDOF();

  //Populate step-matrices from the solution vector x
  for (int node = 0; node < nNode; node++)
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      Sfld.DOF(node).value(ind) = x[node*MatrixSym::SIZE + ind];

  return BaseType::computeObjective(Sfld, n, grad);
}

//---------------------------------------------------------------------------//
//Routine that computes the cost constraint and its derivative wrt optimization variables
//Constraint form: computeCostConstraint(x) <= 0
template <class PhysDim, class TopoDim>
double
MOESS_SANS_Optimizer<PhysDim,TopoDim,NLOPT>::computeCostConstraint(unsigned n, const double* x, double* grad)
{
  MatrixSymFieldType& Sfld = *pSfld_;

  const int nNode = Sfld.nDOF();

  //Populate step-matrices from the solution vector x
  for (int node = 0; node < nNode; node++)
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      Sfld.DOF(node).value(ind) = x[node*MatrixSym::SIZE + ind];

  return BaseType::computeCostConstraint(Sfld, n, grad);;
}

}
