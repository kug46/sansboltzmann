// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef IMPLIEDMETRIC_OPTIMIZER_NLOPT_H_
#define IMPLIEDMETRIC_OPTIMIZER_NLOPT_H_

#include <string>

#include "tools/SANSnumerics.h"     // Real
#include <boost/functional/hash.hpp>

#include "Topology/Dimension.h"
#include "Topology/ElementTopology.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"

#include "Adaptation/MOESS/NodalMetrics.h"

namespace SANS
{

class NLOPT;

template <class PhysDim, class TopoDim, class Optimizer>
class ImpliedMetric_Optimizer;

enum class QualityCalculation{ none = 0, invert = 1, non_invert = 2};

template <class PhysDim, class TopoDim>
class ImpliedMetric_Optimizer<PhysDim,TopoDim,NLOPT>
{
public:
  static const int D = PhysDim::D;   // physical dimensions

  typedef DLA::MatrixSymS<D,Real> MatrixSym;

  typedef NodalMetrics<PhysDim,TopoDim> NodalMetricsType;
  typedef typename NodalMetricsType::ParamsType ParamsType;
  typedef typename NodalMetricsType::MatrixSymFieldType MatrixSymFieldType;

  ImpliedMetric_Optimizer( NodalMetricsType& nodalMetrics );

  //The function that NLOPT calls to evaluate the objective function
  static double NLOPT_Objective(unsigned n, const double* x, double* grad, void* f_data);
  static double NLOPT_ComplexityConstraint(unsigned n, const double* x, double* grad, void* f_data);

  const MatrixSymFieldType& logMfld() { return logMfld_; }
protected:

  //Combined objected function of complexity and edge length deviation
  double computeComplexityObjective(unsigned n, const double* x, double* grad);

  //Combined objected function of sum of ivnerse quality and edge length deviation
  double computeQualityObjective(unsigned n, const double* x, double* grad);

  //Penalize the step matrix between implied nodal and implied elemental metrics
  double computeStepObjective(unsigned n, const double* x, double* grad);

  NodalMetricsType& nodalMetrics_; //Reference to an instance of the MOESS class so that we can access its members
  MatrixSymFieldType logMfld_;

  int eval_count_; //No. of objective function evaluations
  const QualityCalculation quality_calc_ = QualityCalculation::none; // whether objective is min average inverted quality or max average quality
};

}

#endif // IMPLIEDMETRIC_OPTIMIZER_NLOPT_H_
