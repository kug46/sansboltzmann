// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef NLOPT_RESULT_DESCRIPTION_H_
#define NLOPT_RESULT_DESCRIPTION_H_

#include <string>
#include <nlopt.hpp>

namespace SANS
{

std::string nloptResultDescription(nlopt::result resultcode);


}

#endif //NLOPT_RESULT_DESCRIPTION_H_
