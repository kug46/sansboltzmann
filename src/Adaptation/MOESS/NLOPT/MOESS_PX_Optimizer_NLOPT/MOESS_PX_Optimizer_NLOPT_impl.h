// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(MOESS_PX_OPTIMIZER_NLOPT_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "../MOESS_PX_Optimizer_NLOPT.h"

#include <vector>
#include <limits>

#include <nlopt.hpp>

#include "../nloptResultDescription.h"

#include "Adaptation/MOESS/MOESS.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_FrobNorm.h"
#include "Surreal/SurrealS.h"
#include "tools/smoothmath.h"

#include <unistd.h> // gethostname
#include <limits.h> // HOST_NAME_MAX
#ifndef HOST_NAME_MAX
#  if defined(_POSIX_HOST_NAME_MAX)
#    define HOST_NAME_MAX _POSIX_HOST_NAME_MAX
#  elif defined(_SC_HOST_NAME_MAX)
#    define HOST_NAME_MAX _SC_HOST_NAME_MAX
#  else
#    define HOST_NAME_MAX 255
#  endif
#endif // HOST_NAME_MAX

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

template <class PhysDim, class TopoDim>
MOESS_PX_Optimizer<PhysDim,TopoDim,NLOPT>::
MOESS_PX_Optimizer( const Real targetCost, const Real h_domain_max,
                    std::shared_ptr<ErrorModelType>& errorModel,
                    const XField<PhysDim,TopoDim>& xfld_linear,
                    const std::vector<int>& cellgroup_list,
                    const SolverInterfaceBase<PhysDim,TopoDim>& problem,
                    const PyDict& paramsDict )
  : targetCost_(targetCost),
    errorModel_(errorModel)
{

#ifdef SANS_MPI
  // create a serialize grid on processor 0
  XField<PhysDim,TopoDim> xfld_rank0(xfld_linear, XFieldBalance::Serial);

  std::shared_ptr<ErrorModel<PhysDim,TopoDim>> errorModelGlobal = errorModel_;

  // serialize the error model
  errorModel_ = std::make_shared<ErrorModelType>(xfld_rank0, cellgroup_list, problem, paramsDict);
  errorModel_->serialize(*errorModelGlobal);

  // construct the serial step matrix field
  pSfld_ = std::make_shared<MatrixSymFieldType>(xfld_rank0, 1, BasisFunctionCategory_Lagrange, cellgroup_list);

#else
  // construct the step matrix field
  pSfld_ = std::make_shared<MatrixSymFieldType>(xfld_linear, 1, BasisFunctionCategory_Lagrange, cellgroup_list);
#endif

  if (xfld_linear.comm()->rank() == 0)
  {
#ifdef SANS_MPI
    char hostname[HOST_NAME_MAX];
    gethostname(hostname, HOST_NAME_MAX);
    std::cout << std::endl;
    std::cout << "NLopt PX optimizing metrics serially on host: " << hostname << std::endl;
    std::cout << std::endl;

    nodalMetrics_ = std::make_shared<NodalMetricsType>(xfld_rank0, cellgroup_list,
                                                       problem, paramsDict);

#else
    nodalMetrics_ = std::make_shared<NodalMetricsType>(xfld_linear, cellgroup_list,
                                                       problem, paramsDict);
#endif

    // perform the optimization
    optimize(h_domain_max, paramsDict);
  }

#ifdef SANS_MPI
  std::shared_ptr<NodalMetricsType> nodalMetrics_rank0 = nodalMetrics_;

  nodalMetrics_ = std::make_shared<NodalMetricsType>(xfld_linear, cellgroup_list,
                                                     problem, paramsDict, *nodalMetrics_rank0);

  errorModel_ = errorModelGlobal;

  // just so the other processors wait while rank 0 performs the optimization
  xfld_linear.comm()->barrier();
#endif
}

//-----------------------------------------------------------------------------
template <class PhysDim, class TopoDim>
void
MOESS_PX_Optimizer<PhysDim,TopoDim,NLOPT>::
optimize(const Real h_domain_max, const PyDict& paramsDict)
{
  MatrixSymFieldType& Sfld = *pSfld_;

  const int nNode = Sfld.nDOF();
  const int nDim = nNode*MatrixSym::SIZE;

  //nlopt optimizer object
  nlopt::opt opt(nlopt::LD_MMA, nDim);

  //initialize penalty weights for each node
  penalty_weights_.resize(nNode, 1e-4);

  opt.set_min_objective        ( NLOPT_Objective              , reinterpret_cast<void*>(this) );
  opt.add_inequality_constraint( NLOPT_CostConstraint         , reinterpret_cast<void*>(this) );
  opt.add_inequality_constraint( NLOPT_FrobNormSqSumConstraint, reinterpret_cast<void*>(this) );

  //Termination conditions
  Real xtol_rel = paramsDict.get(ParamsType::params.Optimizer_XTol_Rel);
  Real ftol_rel = paramsDict.get(ParamsType::params.Optimizer_FTol_Rel);
  int max_eval  = paramsDict.get(ParamsType::params.Optimizer_MaxEval);

  //stop when every parameter changes by less than the tolerance multiplied by the absolute value of the parameter.
  opt.set_xtol_rel(xtol_rel);

  //stop when the objective function value changes by less than the tolerance multiplied by the absolute value of the function value
  opt.set_ftol_rel(ftol_rel);

  //stop when the maximum number of function evaluations is reached
  opt.set_maxeval(max_eval);

  // The fraction of FrobNormSqSum that is allowed
  FrobNormSqSum_Fraction_ = paramsDict.get(ParamsType::params.FrobNormSqSum_GlobalFraction);

  error0_ = 1.0;
  h_refine_factor_ = 2; //paramsDict.get(ParamsType::params.hRefineFactorMax);


  eval_count_ = 0;
  objective_history_.clear();
  objective_gradnorm_history_.clear();
  cost_constraint_history_.clear();
  cost_constraint_gradnorm_history_.clear();
  step_constraint_history_.clear();
  step_constraint_gradnorm_history_.clear();
  gradation_constraint_history_.clear();

  objective_history_.clear();
  objective_gradnorm_history_.clear();

  Sfld = 0;

  //maximum allowed coarsening factors penalized in the egien value penalty term
  std::vector<Real> h_coarsen_factors(nNode);

  nodalMetrics_->initializeStepMatrices(h_domain_max, h_refine_factor_,
                                        targetCost_, cost_initial_estimate_,
                                        Sfld, h_coarsen_factors);

  // set the step matrix hard bounds
  {
    std::vector<Real> lower_bound_vector(nDim);
    std::vector<Real> upper_bound_vector(nDim);

    for (int node = 0; node < nNode; node++)
    {
      //Set bounds on the step matrix at this node
      Real lower_bound = -2.0*log(h_coarsen_factors[node]); //max coarsening factor = h_refine_factor_limited
      Real upper_bound = +2.0*log(h_refine_factor_);        //max refinement factor = h_refine_factor_limited

      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      {
        lower_bound_vector[node*MatrixSym::SIZE + ind] = lower_bound;
        upper_bound_vector[node*MatrixSym::SIZE + ind] = upper_bound;
      }
    } //loop over nodes

    opt.set_lower_bounds(lower_bound_vector);
    opt.set_upper_bounds(upper_bound_vector);
  }

  computeFrobNormSqTargets();

  std::vector<MatrixSym> d_dSvec;

  //Initialize solution vector
  std::vector<Real> x(nDim, 0.0);

  //Compute the global error estimate as the scaling factor
  errorModel_->computeError(Sfld, error0_, d_dSvec);

  // just so we don't divide by zero
  if (error0_ == 0) error0_ = std::numeric_limits<Real>::epsilon();

  //Set the initial value
  for (int node = 0; node < nNode; node++)
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      x[node*MatrixSym::SIZE + ind] = Sfld.DOF(node).value(ind);

  // get the initial value with the penalty terms possibly active
  Real f_init = computeObjective(nDim, x.data(), nullptr);
  Real f_opt = 0.0;

  int total_eval_count = 0;
  int outer_iter = 0;
  int eigenvalue_violation_count = 0;

  std::cout << std::endl;
  std::cout << "---------------------------" << std::endl;
  std::cout << "Starting NLopt Optimization" << std::endl;
  std::cout << "---------------------------" << std::endl;
  std::cout << std::endl;


  do //Outer loop - until eigenvalues of step-matrix requests are within accepted bounds
  {
    eval_count_ = 0;
    nlopt::result result = nlopt::result::FAILURE;

    try
    {
      result = opt.optimize(x, f_opt);
    }
    catch (const std::runtime_error& failure )
    {
      SANS_DEVELOPER_EXCEPTION( "NLopt - Runtime failure.");
    }

    Real frobNormSqTargetOuter = pow(sqrt(D)*2.0*log(h_refine_factor_),2.0);
    Real frobNormSqTargetInner = pow(        2.0*log(h_refine_factor_),2.0);

    Real frobNormSqTargetDel = D == 1 ? 1e-2 : frobNormSqTargetOuter/frobNormSqTargetInner - 1;

    Real SnormSq = 0.;
    for (int node = 0; node < nNode; node++)
    {
      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
        Sfld.DOF(node).value(ind) = x[node*MatrixSym::SIZE + ind];

      Real SnormSq_temp = DLA::FrobNormSq(Sfld.DOF(node));
      if ( SnormSq_temp > SnormSq )
      {
        SnormSq = SnormSq_temp;
      }
    }
    Real tmp = SnormSq/frobNormSqTargetInner - 1 - frobNormSqTargetDel/2;
    Real maxSpenalty = smoothmaxC2(0.0, tmp, frobNormSqTargetDel);


    //Compute the global error estimate
    Real errEst = 0;
    errorModel_->computeError(Sfld, errEst, d_dSvec);

    //Compute the cost estimate
    nodalMetrics_->computeCost(Sfld, cost_final_estimate_, d_dSvec);

    eigenvalue_violation_count = handleStepMatrixViolations(x, h_refine_factor_, h_coarsen_factors);

    total_eval_count += eval_count_;

    std::cout << ">  Outer iteration " << outer_iter << std::endl;
    std::cout << "     Result        : " << nloptResultDescription(result) <<std::endl;
    std::cout << "     Eval count    : " << eval_count_ << std::endl;
    std::cout << "     Objective     : " << f_opt << std::endl;
    std::cout << "     Error Est.    : " << errEst << std::endl;
    std::cout << "     Cost          : " << cost_final_estimate_ << std::endl;
    std::cout << "     Elem Req.     : " << nodalMetrics_->computeRequestedElements(Sfld) << std::endl;
    std::cout << "  Max Step Penalty : " << maxSpenalty << std::endl;
    std::cout << "     Step matrix eigenvalue violations : "<< eigenvalue_violation_count << std::endl;
    std::cout << std::endl << std::endl;

    outer_iter++;
  }
  while (eigenvalue_violation_count > 0 && outer_iter < 10);

  eval_count_ = total_eval_count;

  std::cout << "-------------------------" << std::endl;
  std::cout << "   MOESS NLopt Summary   " << std::endl;
  std::cout << "-------------------------" << std::endl;
  std::cout << "Variables         : " << nDim       << std::endl;
  std::cout << "Total eval count  : " << eval_count_<< std::endl;
  std::cout << "Initial objective : " << f_init     << std::endl;
  std::cout << "Final objective   : " << f_opt      << std::endl;
  std::cout << "-------------------------" << std::endl << std::endl;

  objective_reduction_ratio_ = f_opt/f_init;

  nodalMetrics_->setNodalStepMatrix(Sfld);
}

//-----------------------------------------------------------------------------
//The function that NLopt calls to evaluate the objective function
template <class PhysDim, class TopoDim>
double
MOESS_PX_Optimizer<PhysDim,TopoDim,NLOPT>::NLOPT_Objective(unsigned n, const double* x, double* grad, void* f_data)
{
  MOESS_PX_Optimizer<PhysDim,TopoDim,NLOPT>* solver = reinterpret_cast<MOESS_PX_Optimizer<PhysDim,TopoDim,NLOPT>*>(f_data);

  return solver->computeObjective(n, x, grad); //forward the call
}

//-----------------------------------------------------------------------------
//The function that NLopt calls to evaluate the cost constraint
template <class PhysDim, class TopoDim>
double
MOESS_PX_Optimizer<PhysDim,TopoDim,NLOPT>::NLOPT_CostConstraint(unsigned n, const double* x, double* grad, void* f_data)
{
  MOESS_PX_Optimizer<PhysDim,TopoDim,NLOPT>* solver = reinterpret_cast<MOESS_PX_Optimizer<PhysDim,TopoDim,NLOPT>*>(f_data);

  return solver->computeCostConstraint(n, x, grad); //forward the call
}

//-----------------------------------------------------------------------------
//The function that NLopt calls to evaluate the global Frobenius norm sum constraint
template <class PhysDim, class TopoDim>
double
MOESS_PX_Optimizer<PhysDim,TopoDim,NLOPT>::NLOPT_FrobNormSqSumConstraint(unsigned n, const double* x, double* grad, void* f_data)
{
  MOESS_PX_Optimizer<PhysDim,TopoDim,NLOPT>* solver = reinterpret_cast<MOESS_PX_Optimizer<PhysDim,TopoDim,NLOPT>*>(f_data);

  return solver->computeFrobNormSqSumConstraint(n, x, grad); //forward the call
}

//-----------------------------------------------------------------------------
//Routine that computes the objective function and its derivative wrt optimization variables
template <class PhysDim, class TopoDim>
double
MOESS_PX_Optimizer<PhysDim,TopoDim,NLOPT>::computeObjective(unsigned n, const double* x, double* grad)
{
  eval_count_++;

  MatrixSymFieldType& Sfld = *pSfld_;

  const int nNode = Sfld.nDOF();

  //Populate step-matrices from the solution vector x
  for (int node = 0; node < nNode; node++)
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      Sfld.DOF(node).value(ind) = x[node*MatrixSym::SIZE + ind];

  //---------------------------------------------------------------
  Real Error = 0.0;
  std::vector<MatrixSym> dError_dSvec(grad == nullptr ? 0 : nNode, 0.0);

  //Compute the global error estimate and its derivatives
  errorModel_->computeError(Sfld, Error, dError_dSvec);
  //---------------------------------------------------------------

  //---------------------------------------------------------------
  Real penalty = 0.0;
  std::vector<MatrixSym> dpenalty_dSvec(grad == nullptr ? 0 : nNode, 0.0);

  //Compute the Frobenius norm sq penalty term and its derivatives
  if (grad)
    computeFrobNormPenalty(Sfld, penalty, dpenalty_dSvec);
  else
    computeFrobNormPenalty(Sfld, penalty);
  //---------------------------------------------------------------

  if (grad)
  {
    Real L2norm = 0.0;

    //Populate grad vector from dError_dSvec
    for (int node = 0; node < nNode; node++)
    {
      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      {
        Real obj_Svec = dError_dSvec[node].value(ind)/error0_ + dpenalty_dSvec[node].value(ind);

        grad[node*MatrixSym::SIZE + ind] = obj_Svec;

        L2norm += obj_Svec*obj_Svec;
      }
    }

    L2norm = sqrt(L2norm);
    objective_gradnorm_history_.push_back(L2norm);
  }

  Real obj = Error/error0_ + penalty;
  objective_history_.push_back(obj);

  return obj;
}

//Routine that computes the cost constraint and its derivative wrt optimization variables
//Constraint form: computeCostConstraint(x) <= 0
template <class PhysDim, class TopoDim>
double
MOESS_PX_Optimizer<PhysDim,TopoDim,NLOPT>::computeCostConstraint(unsigned n, const double* x, double* grad)
{
  MatrixSymFieldType& Sfld = *pSfld_;

  const int nNode = Sfld.nDOF();

  //Populate step-matrices from the solution vector x
  for (int node = 0; node < nNode; node++)
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      Sfld.DOF(node).value(ind) = x[node*MatrixSym::SIZE + ind];

  Real Cost = 0.0;
  std::vector<MatrixSym> dCost_dSvec(grad == nullptr ? 0 : nNode, 0.0);

  //Compute the cost estimate and its derivatives
  nodalMetrics_->computeCost(Sfld, Cost, dCost_dSvec);

  if (grad)
  {
    Real L2norm = 0.0;
    //Populate grad vector from dCost_dSvec
    for (int node = 0; node < nNode; node++)
    {
      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      {
        grad[node*MatrixSym::SIZE + ind] = dCost_dSvec[node].value(ind)/targetCost_;

        L2norm += pow(grad[node*MatrixSym::SIZE + ind],2);
      }
    }

    L2norm = sqrt(L2norm);
//    std::cout<<"L2norm cost constraint: " <<L2norm << std::endl;
    cost_constraint_gradnorm_history_.push_back(L2norm);
  }

  Real constraint = Cost/targetCost_ - 1;
  cost_constraint_history_.push_back(constraint);

  return constraint;
}

//Routine that computes the Frobenius norm squared sum constraint and its derivative wrt optimization variables
//Constraint form: computeFrobNormSqSumConstraint(x) <= 0
template <class PhysDim, class TopoDim>
double
MOESS_PX_Optimizer<PhysDim,TopoDim,NLOPT>::computeFrobNormSqSumConstraint(unsigned n, const double* x, double* grad)
{
  const int SurrealSize = MatrixSym::SIZE;
  typedef SurrealS<SurrealSize> SurrealClass;
  typedef DLA::MatrixSymS<D,SurrealClass> MatrixSymSurreal;

  const int nNode = pSfld_->nDOF();

  // compute the target FrobNormSqSum
  Real FrobNormSqSumTarget = 0.0;
  for (int node = 0; node < nNode; node++)
    FrobNormSqSumTarget += FrobNormSq_targets_[node];
  FrobNormSqSumTarget *= FrobNormSqSum_Fraction_;

  Real FrobNormSqSum = 0.0;
  Real L2norm = 0.0;

  if (grad)
  {
    MatrixSymSurreal S; //step matrix for this node

    for (int node = 0; node < nNode; node++)
    {
      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      {
        S.value(ind) = x[node*MatrixSym::SIZE + ind];
        S.value(ind).deriv(ind) = 1.0;
      }

      //Calculate Frobenius norm square
      SurrealClass normSq = DLA::FrobNormSq(S);

      FrobNormSqSum += normSq.value();

      //Populate grad vector
      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      {
        grad[node*MatrixSym::SIZE + ind] = normSq.deriv(ind)/FrobNormSqSumTarget;
        L2norm += pow(grad[node*MatrixSym::SIZE + ind],2);
      }
    } //loop over nodes
  }
  else
  {
    MatrixSym S; //step matrix for a given node

    for (int node = 0; node < nNode; node++)
    {
      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
        S.value(ind) = x[node*MatrixSym::SIZE + ind];

      //Calculate Frobenius norm square
      Real normSq = DLA::FrobNormSq(S);

      FrobNormSqSum += normSq;
    } //loop over nodes
  }

  L2norm = sqrt(L2norm);

  Real constraint = FrobNormSqSum/FrobNormSqSumTarget - 1;
  step_constraint_history_.push_back(constraint);
  step_constraint_gradnorm_history_.push_back(L2norm);

  return constraint;
}

template <class PhysDim, class TopoDim>
void
MOESS_PX_Optimizer<PhysDim,TopoDim,NLOPT>::
computeFrobNormSqTargets()
{
  const int nNode = pSfld_->nDOF();
  FrobNormSq_targets_.resize(nNode);

  for (int node = 0; node < nNode; node++)
    FrobNormSq_targets_[node] = pow(2.0*log(h_refine_factor_),2.0);
}

template <class PhysDim, class TopoDim>
void
MOESS_PX_Optimizer<PhysDim,TopoDim,NLOPT>::
computeFrobNormPenalty(const MatrixSymFieldType& Sfld,
                       Real& penalty, std::vector<MatrixSym>& dpenalty_dSvec)
{
  const int SurrealSize = MatrixSym::SIZE;
  typedef SurrealS<SurrealSize> SurrealClass;
  typedef DLA::MatrixSymS<D,SurrealClass> MatrixSymSurreal;

  Real FrobNormSq_refine = pow(2.0*log(h_refine_factor_),2.0);

  penalty = 0.0;

  const int nNode = Sfld.nDOF();
  for (int node = 0; node < nNode; node++)
  {
    MatrixSymSurreal S = Sfld.DOF(node); //step matrix for this node

    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      S.value(ind).deriv(ind) = 1.0;

    //Calculate Frobenius norm square
    SurrealClass normSq = DLA::FrobNormSq(S);

    //Calculate difference between current Frobenius norm sq and target
    SurrealClass tmp = (normSq - FrobNormSq_targets_[node])/FrobNormSq_refine;

    //Only add penalty if current Frobenius norm sq is larger (i.e. if tmp >= 0 - but smoothly)
    SurrealClass zero = 0.0;
    SurrealClass tmp_clipped = smoothmax(zero, tmp, 20.0);

//    if (tmp.value() > 0)
//      tmp_clipped = tmp;
//    else
//      tmp_clipped = 0.0;

    //Apply weighted quadratic penalty
    SurrealClass node_penalty = penalty_weights_[node]*tmp_clipped*tmp_clipped;

    penalty += node_penalty.value();

    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      dpenalty_dSvec[node].value(ind) = node_penalty.deriv(ind);

  } //loop over nodes
}

template <class PhysDim, class TopoDim>
void
MOESS_PX_Optimizer<PhysDim,TopoDim,NLOPT>::
computeFrobNormPenalty(const MatrixSymFieldType& Sfld, Real& penalty)
{
  Real FrobNormSq_refine = pow(2.0*log(h_refine_factor_),2.0);

  penalty = 0.0;

  const int nNode = Sfld.nDOF();
  for (int node = 0; node < nNode; node++)
  {
    MatrixSym S = Sfld.DOF(node); //step matrix for this node

    //Calculate Frobenius norm square
    Real normSq = DLA::FrobNormSq(S);

    //Calculate difference between current Frobenius norm sq and target
    Real tmp = (normSq - FrobNormSq_targets_[node])/FrobNormSq_refine;

    //Only add penalty if current Frobenius norm sq is larger (i.e. if tmp >= 0 - but smoothly)
    Real zero = 0.0;
    Real tmp_clipped = smoothmax(zero, tmp, 20.0);

//    if (tmp.value() > 0)
//      tmp_clipped = tmp;
//    else
//      tmp_clipped = 0.0;

    //Apply weighted quadratic penalty
    Real node_penalty = penalty_weights_[node]*tmp_clipped*tmp_clipped;

    penalty += node_penalty;
  } //loop over nodes
}

template <class PhysDim, class TopoDim>
int
MOESS_PX_Optimizer<PhysDim,TopoDim,NLOPT>::
handleStepMatrixViolations(const std::vector<Real>& x, const Real h_refine_factor, const std::vector<Real>& h_coarsen_factors)
{
  int violation_count = 0;

  const int nNode = pSfld_->nDOF();
  for (int node = 0; node < nNode; node++)
  {
    MatrixSym S;

    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      S.value(ind) = x[node*MatrixSym::SIZE + ind];

    //Obtain eigenvalues of S
    SANS::DLA::VectorS<D,Real> L;
    DLA::EigenValues( S, L );

    for (int d = 0; d < D; d++)
    {
      if ( L[d] < -2.0*log(h_coarsen_factors[node]) ||
           L[d] > +2.0*log(h_refine_factor) )
      {
        //Increase the penalty weight for this node if an eigenvalue is larger than the accepted bound
        penalty_weights_[node] *= 10.0;
        violation_count++;
        break;
      }
    }
  } //loop over nodes

  return violation_count;
}

}
