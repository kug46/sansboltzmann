// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef MOESS_GRADATION_OPTIMIZER_NLOPT_H_
#define MOESS_GRADATION_OPTIMIZER_NLOPT_H_

#include <ostream>
#include <vector>

#include <nlopt.hpp>

#include "tools/SANSnumerics.h"     // Real
#include "Topology/Dimension.h"
#include "Topology/ElementTopology.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"

namespace SANS
{

class NLOPT;

template <class PhysDim, class TopoDim, class Optimizer>
class MOESS_Gradation_Optimizer;


template <class PhysDim, class TopoDim>
class MOESS_Gradation_Optimizer<PhysDim,TopoDim,NLOPT>
{
public:
  typedef typename MOESSClass::PhysD PhysDim;
  static const int D = PhysDim::D;   // physical dimensions

  typedef typename MOESSClass::ParamsType ParamsType;

  typedef DLA::MatrixSymS<D,Real> MatrixSym;

  MOESS_Gradation_Optimizer( MOESSClass& base, const unsigned int nNode );

  //The function that NLOPT calls to evaluate the objective function
  static double NLOPT_Objective(unsigned n, const double* x, double* grad, void* f_data);

  //The function that NLOPT calls to evaluate the cost constraint
  static double NLOPT_CostConstraint(unsigned n, const double* x, double* grad, void* f_data);

  //The function that NLOPT calls to evaluate the global Frobenius norm squared sum constraint
  static double NLOPT_EigenValueConstraint(unsigned n, const double* x, double* grad, void* f_data);

  //The function that NLOPT calls to evaluate the global Gradation constraint
  static double NLOPT_GradationConstraint(unsigned n, const double* x, double* grad, void* f_data);

  //The function that NLOPT calls to evaluate the global Frobenius norm squared sum constraint
  static double NLOPT_FrobNormSqSumConstraint(unsigned n, const double* x, double* grad, void* f_data);

  void optimize(std::vector<MatrixSym>& Svec);

protected:

  //Computes the objective function and its derivative wrt optimization variables
  double computeObjective(unsigned n, const double* x, double* grad);

  //Computes the cost constraint and its derivative wrt optimization variables
  //Constraint form: computeCostConstraint(x) <= 0
  double computeCostConstraint(unsigned n, const double* x, double* grad);

  //Computes the Frobenius norm squared sum constraint and its derivative wrt optimization variables
  double computeFrobNormSqSumConstraint(unsigned n, const double* x, double* grad);

  //Sets bounds on the step matrix values based on maximum edge refinement factors
  void setStepMatrixBounds(const Real h_refine_factor, const std::vector<Real>& h_coarsen_factors);

  //Computes the desired (target) squared Frobenius norm of the step matrix at each node
  void computeFrobNormSqTargets(const std::vector<Real>& h_coarsen_factors);

  void computeFrobNormPenalty(const std::vector<MatrixSym>& Svec, Real& penalty, std::vector<MatrixSym>& dpenalty_dSvec);
  void computeFrobNormPenalty(const std::vector<MatrixSym>& Svec, Real& penalty);

  //Check resulting step-matrices to see if eigenvalues are above bounds and increase penalty weights
  int handleStepMatrixViolations(const std::vector<Real>& x, const Real h_refine_factor, const std::vector<Real>& h_coarsen_factors);

  //Computes the Eigen Value constraint and its derivative wrt optimization variables
  double computeEigenValueConstraint(unsigned n, const double* x, double* grad);

  //Computes the Gradation constraint and its derivative wrt optimization variables
  double computeGradationConstraint(unsigned n, const double* x, double* grad);

  // Eigen value penalty term
  void computeEigenValuePenalty(const std::vector<MatrixSym>& Svec, Real& penalty, std::vector<MatrixSym>& dpenalty_dSvec);

  // Cost penalty term
  void computeCostPenalty(const std::vector<MatrixSym>& Svec, Real& costPenalty, std::vector<MatrixSym>& dcostPenalty_dSvec);

  MOESSClass& MOESS_base_; //Reference to an instance of the MOESS class so that we can access its members
  const unsigned int nDim_; //No. of optimization variables (problem dimension)
  const int nNode_; //No. of nodes in mesh
  const Real& targetCost_;

  Real error0_; //initial error value

  nlopt::opt opt_;
  int eval_count_; //No. of objective function evaluations

  std::vector<Real> FrobNormSq_targets_; //target Frobenius norm sq of each nodal step matrix
  std::vector<Real> penalty_weights_;    //nodal weights for Frobenius norm sq penalty term
};

}

#endif /* MOESS_GRADATION_OPTIMIZER_NLOPT_H_ */
