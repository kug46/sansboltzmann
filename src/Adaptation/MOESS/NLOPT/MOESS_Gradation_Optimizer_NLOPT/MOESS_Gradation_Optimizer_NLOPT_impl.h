// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(MOESS_GRADATION_OPTIMIZER_NLOPT_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "../MOESS_Gradation_Optimizer_NLOPT.h"

#include <vector>

#include "../nloptResultDescription.h"

#include "Adaptation/MOESS/MOESS.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_FrobNorm.h"
#include "Surreal/SurrealS.h"
#include "tools/smoothmath.h"

#include <unistd.h> // gethostname
#include <limits.h> // HOST_NAME_MAX
#ifndef HOST_NAME_MAX
#  if defined(_POSIX_HOST_NAME_MAX)
#    define HOST_NAME_MAX _POSIX_HOST_NAME_MAX
#  elif defined(_SC_HOST_NAME_MAX)
#    define HOST_NAME_MAX _SC_HOST_NAME_MAX
#  else
#    define HOST_NAME_MAX 255
#  endif
#endif // HOST_NAME_MAX

#define GRADATION_PENALTY
//#define NO_PENALTY

namespace SANS
{


template <class MOESSClass>
MOESS_Gradation_Optimizer<MOESSClass,NLOPT>::MOESS_Gradation_Optimizer( MOESSClass& base, const unsigned int nNode )
: MOESS_base_(base), nDim_(nNode*MatrixSym::SIZE),
  nNode_(nNode), targetCost_(MOESS_base_.targetCost_),
  opt_(nlopt::LD_MMA, nDim_)
{
  //initialize penalty weights for each node
  penalty_weights_.resize(nNode_, 1e-4);

  opt_.set_min_objective        ( NLOPT_Objective           , reinterpret_cast<void*>(this) );
  opt_.add_inequality_constraint( NLOPT_CostConstraint      , reinterpret_cast<void*>(this) );
  //opt_.add_inequality_constraint( NLOPT_EigenValueConstraint, reinterpret_cast<void*>(this) );
  //opt_.add_inequality_constraint( NLOPT_GradationConstraint , reinterpret_cast<void*>(this) );
  //opt_.add_inequality_constraint( NLOPT_FrobNormSqSumConstraint, reinterpret_cast<void*>(this) );

  //Termination conditions
  Real xtol_rel = MOESS_base_.paramsDict_.get(ParamsType::params.Optimizer_XTol_Rel);
  Real ftol_rel = MOESS_base_.paramsDict_.get(ParamsType::params.Optimizer_FTol_Rel);
  int max_eval  = MOESS_base_.paramsDict_.get(ParamsType::params.Optimizer_MaxEval);

  //stop when every parameter changes by less than the tolerance multiplied by the absolute value of the parameter.
  opt_.set_xtol_rel(xtol_rel);

  //stop when the objective function value changes by less than the tolerance multiplied by the absolute value of the function value
  opt_.set_ftol_rel(ftol_rel);

  //stop when the maximum number of function evaluations is reached
  opt_.set_maxeval(max_eval);

  error0_ = 0;

  eval_count_ = 0;
  MOESS_base_.objective_history_.clear();
  MOESS_base_.objective_gradnorm_history_.clear();
  MOESS_base_.cost_constraint_history_.clear();
  MOESS_base_.cost_constraint_gradnorm_history_.clear();
  MOESS_base_.step_constraint_history_.clear();
  MOESS_base_.step_constraint_gradnorm_history_.clear();
  MOESS_base_.gradation_constraint_history_.clear();

  //MOESS_base_.cost_constraint_history_.resize(max_eval, 0);
  //MOESS_base_.cost_constraint_gradnorm_history_.resize(max_eval, 0);
  MOESS_base_.step_constraint_history_.resize(max_eval, 0);
  MOESS_base_.step_constraint_gradnorm_history_.resize(max_eval, 0);
  MOESS_base_.gradation_constraint_history_.resize(max_eval, 0);
}

//The function that NLOPT calls to evaluate the objective function
template <class MOESSClass>
double
MOESS_Gradation_Optimizer<MOESSClass,NLOPT>::NLOPT_Objective(unsigned n, const double* x, double* grad, void* f_data)
{
  MOESS_Gradation_Optimizer<MOESSClass, NLOPT>* solver = reinterpret_cast<MOESS_Gradation_Optimizer<MOESSClass, NLOPT>*>(f_data);

  return (*solver).computeObjective(n, x, grad); //forward the call
}

//The function that NLOPT calls to evaluate the cost constraint
template <class MOESSClass>
double
MOESS_Gradation_Optimizer<MOESSClass,NLOPT>::NLOPT_CostConstraint(unsigned n, const double* x, double* grad, void* f_data)
{
  MOESS_Gradation_Optimizer<MOESSClass, NLOPT>* solver = reinterpret_cast<MOESS_Gradation_Optimizer<MOESSClass, NLOPT>*>(f_data);

  return (*solver).computeCostConstraint(n, x, grad); //forward the call
}

//The function that NLOPT calls to evaluate the global Frobenius norm sum constraint
template <class MOESSClass>
double
MOESS_Gradation_Optimizer<MOESSClass,NLOPT>::NLOPT_EigenValueConstraint(unsigned n, const double* x, double* grad, void* f_data)
{
  MOESS_Gradation_Optimizer<MOESSClass, NLOPT>* solver = reinterpret_cast<MOESS_Gradation_Optimizer<MOESSClass, NLOPT>*>(f_data);

  return (*solver).computeEigenValueConstraint(n, x, grad); //forward the call
}

//The function that NLOPT calls to evaluate the gradation constraint
template <class MOESSClass>
double
MOESS_Gradation_Optimizer<MOESSClass,NLOPT>::NLOPT_GradationConstraint(unsigned n, const double* x, double* grad, void* f_data)
{
  MOESS_Gradation_Optimizer<MOESSClass, NLOPT>* solver = reinterpret_cast<MOESS_Gradation_Optimizer<MOESSClass, NLOPT>*>(f_data);

  return (*solver).computeGradationConstraint(n, x, grad); //forward the call
}

//The function that NLOPT calls to evaluate the global Frobenius norm sum constraint
template <class MOESSClass>
double
MOESS_Gradation_Optimizer<MOESSClass,NLOPT>::NLOPT_FrobNormSqSumConstraint(unsigned n, const double* x, double* grad, void* f_data)
{
  MOESS_Gradation_Optimizer<MOESSClass, NLOPT>* solver = reinterpret_cast<MOESS_Gradation_Optimizer<MOESSClass, NLOPT>*>(f_data);

  return (*solver).computeFrobNormSqSumConstraint(n, x, grad); //forward the call
}


//Routine that computes the objective function and its derivative wrt optimization variables
template <class MOESSClass>
double
MOESS_Gradation_Optimizer<MOESSClass,NLOPT>::computeObjective(unsigned n, const double* x, double* grad)
{
  eval_count_++;

  std::vector<MatrixSym> Svec(nNode_); //Step-matrices at each node

  //Populate step-matrices from the solution vector x
  for (int node = 0; node < nNode_; node++)
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      Svec[node].value(ind) = x[node*MatrixSym::SIZE + ind];

  //---------------------------------------------------------------
  Real Error = 0.0;
  std::vector<MatrixSym> dError_dSvec(grad == nullptr ? 0 : nNode_, 0.0);

  //Compute the global error estimate and its derivatives
  MOESS_base_.computeError(Svec, Error, dError_dSvec);

  //---------------------------------------------------------------

  Real penalty = 0.0;
  std::vector<MatrixSym> dpenalty_dSvec(grad == nullptr ? 0 : nNode_, 0.0);

  //Compute the Frobenius norm sq penalty term and its derivatives
  if (grad)
    computeFrobNormPenalty(Svec, penalty, dpenalty_dSvec);
  else
    computeFrobNormPenalty(Svec, penalty);

  //computeEigenValuePenalty(Svec, penalty, dpenalty_dSvec);

  //Cost penalty
  //computeCostPenalty(Svec, penalty, dpenalty_dSvec);

  // Add the gradation penalty
  MOESS_base_.computeGradationPenalty(Svec, penalty, dpenalty_dSvec);
  //---------------------------------------------------------------

  Real obj = Error/error0_ + penalty;
  MOESS_base_.objective_history_.push_back(obj);

  if (grad)
  {
    Real L2norm = 0.0;

    //Populate grad vector from dError_dSvec
    for (int node = 0; node < nNode_; node++)
    {
      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      {
        Real obj_Svec = dError_dSvec[node].value(ind)/error0_ + dpenalty_dSvec[node].value(ind);

        grad[node*MatrixSym::SIZE + ind] = obj_Svec;

        L2norm += obj_Svec*obj_Svec;
      }
    }

    L2norm = sqrt(L2norm);
    MOESS_base_.objective_gradnorm_history_.push_back(L2norm);
  }

  return obj;
}

//Routine that computes the cost constraint and its derivative wrt optimization variables
//Constraint form: computeCostConstraint(x) <= 0
template <class MOESSClass>
double
MOESS_Gradation_Optimizer<MOESSClass,NLOPT>::computeCostConstraint(unsigned n, const double* x, double* grad)
{
  std::vector<MatrixSym> Svec(nNode_); //Step-matrices at each node

  //Populate step-matrices from the solution vector x
  for (int node = 0; node < nNode_; node++)
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      Svec[node].value(ind) = x[node*MatrixSym::SIZE + ind];

  Real Cost = 0.0;
  std::vector<MatrixSym> dCost_dSvec(grad == nullptr ? 0 : nNode_, 0.0);

  //Compute the cost estimate and its derivatives
  MOESS_base_.computeCost(Svec, Cost, dCost_dSvec);

  if (grad)
  {
    Real L2norm = 0.0;
    //Populate grad vector from dCost_dSvec
    for (int node = 0; node < nNode_; node++)
    {
      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      {
        grad[node*MatrixSym::SIZE + ind] = dCost_dSvec[node].value(ind);

        L2norm += pow(dCost_dSvec[node].value(ind),2);
      }
    }

    L2norm = sqrt(L2norm);
    MOESS_base_.cost_constraint_gradnorm_history_.push_back(L2norm);
  }

  Real constraint = Cost - targetCost_;
  MOESS_base_.cost_constraint_history_.push_back(constraint);

  return constraint;
}

//Routine that computes the Frobenius norm squared sum constraint and its derivative wrt optimization variables
//Constraint form: computeFrobNormSqSumConstraint(x) <= 0
template <class MOESSClass>
double
MOESS_Gradation_Optimizer<MOESSClass,NLOPT>::computeFrobNormSqSumConstraint(unsigned n, const double* x, double* grad)
{
  const int SurrealSize = MatrixSym::SIZE;
  typedef SurrealS<SurrealSize> SurrealClass;
  typedef DLA::MatrixSymS<D,SurrealClass> MatrixSymSurreal;

  Real FrobNormSqSum_Fraction = MOESS_base_.paramsDict_.get(ParamsType::params.FrobNormSqSum_GlobalFraction);

  Real FrobNormSqSum = 0.0;
  Real FrobNormSqSumTarget = 0.0;

  Real L2norm = 0.0;

  if (grad)
  {
    MatrixSymSurreal S; //step matrix for this node

    for (int node = 0; node < nNode_; node++)
    {
      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      {
        S.value(ind) = x[node*MatrixSym::SIZE + ind];
        S.value(ind).deriv(ind) = 1.0;
      }

      //Calculate Frobenius norm square
      SurrealClass normSq = DLA::FrobNormSq(S);

      FrobNormSqSum += normSq.value();
      FrobNormSqSumTarget += FrobNormSqSum_Fraction*FrobNormSq_targets_[node];

      //Populate grad vector
      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      {
        grad[node*MatrixSym::SIZE + ind] = normSq.deriv(ind);
        L2norm += pow(normSq.deriv(ind),2);
      }
    } //loop over nodes
  }
  else
  {
    MatrixSym S; //step matrix for a given node

    for (int node = 0; node < nNode_; node++)
    {
      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
        S.value(ind) = x[node*MatrixSym::SIZE + ind];

      //Calculate Frobenius norm square
      Real normSq = DLA::FrobNormSq(S);

      FrobNormSqSum += normSq;
      FrobNormSqSumTarget += FrobNormSqSum_Fraction*FrobNormSq_targets_[node];
    } //loop over nodes
  }

  L2norm = sqrt(L2norm);

  Real constraint = FrobNormSqSum - FrobNormSqSumTarget;
  MOESS_base_.step_constraint_history_.push_back(constraint);
  MOESS_base_.step_constraint_gradnorm_history_.push_back(L2norm);

  return constraint;
}

template <class MOESSClass>
void
MOESS_Gradation_Optimizer<MOESSClass,NLOPT>::
optimize(std::vector<MatrixSym>& Svec)
{
  //Initialize solution vector
  std::vector<Real> x(nDim_, 0.0);

  std::vector<MatrixSym> d_dSvec(0);

  //Compute initial error value - with 0 step-matrices
  MOESS_base_.computeError(Svec, error0_, d_dSvec);

  Real h_refine_factor;
  std::vector<Real> h_coarsen_factors(nNode_);
  MOESS_base_.initializeStepMatrices(Svec, h_refine_factor, h_coarsen_factors);
  setStepMatrixBounds(h_refine_factor, h_coarsen_factors);
  computeFrobNormSqTargets(h_coarsen_factors);

  for (int node = 0; node < nNode_; node++)
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      x[node*MatrixSym::SIZE + ind] = Svec[node].value(ind);

  MOESS_base_.objective_history_.clear();
  MOESS_base_.objective_gradnorm_history_.clear();

  // Compute the initial objective
  double f_opt = computeObjective(x.size(), x.data(), nullptr);
  Real f_init = f_opt;

  int total_eval_count = 0;
  int outer_iter = 0;
  int eigenvalue_violation_count = 0;

  Real ErrorEst = 0.0;
  MOESS_base_.computeError(Svec, ErrorEst, d_dSvec);

  Real frobPenalty = 0.0;
  computeFrobNormPenalty(Svec, frobPenalty);

  Real gradPenalty = 0.0;
  MOESS_base_.computeGradationPenalty(Svec, gradPenalty, d_dSvec);

  std::cout<<std::endl;
  std::cout << "------------------------------------------" << std::endl;
  std::cout << "Starting NLopt Optimization" << std::endl;
  std::cout << "     Initial Objective      : " << f_opt << std::endl;
  std::cout << "     Err. Est.              : " << ErrorEst << std::endl;
  std::cout << "     Frob Penalty           : " << frobPenalty << std::endl;
  std::cout << "     Gradation Penalty      : " << gradPenalty << std::endl;
  std::cout << "------------------------------------------" << std::endl;
  std::cout<<std::endl;

  nlopt::result result = nlopt::result::FAILURE;

  do //Outer loop - until eigenvalues of step-matrix requests are within accepted bounds
  {
    eval_count_ = 0;

    try
    {
      result = opt_.optimize(x, f_opt);
    }
    catch (const nlopt::roundoff_limited& failure )
    {
      SANS_DEVELOPER_EXCEPTION( "NLOPT - Roundoff failure : %s", failure.what());
    }
    catch (const nlopt::forced_stop& failure )
    {
      SANS_DEVELOPER_EXCEPTION( "NLOPT - Forced stop : %s", failure.what());
    }
    catch (const std::runtime_error& failure )
    {
      SANS_DEVELOPER_EXCEPTION( "NLOPT - Runtime failure : %s", failure.what());
    }
    catch (const std::invalid_argument& failure )
    {
      SANS_DEVELOPER_EXCEPTION( "NLOPT - Invalid Arguments failure : %s", failure.what());
    }
    catch (const std::bad_alloc& failure )
    {
      SANS_DEVELOPER_EXCEPTION( "NLOPT - Out of Memory failure : %s", failure.what());
    }

    for (int node = 0; node < nNode_; node++)
      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
        Svec[node].value(ind) = x[node*MatrixSym::SIZE + ind];

    //Compute the cost estimate
    Real Cost = 0;
    MOESS_base_.computeCost(Svec, Cost, d_dSvec);

    Real ErrorEst = 0.0;
    MOESS_base_.computeError(Svec, ErrorEst, d_dSvec);

    Real frobPenalty = 0.0;
    computeFrobNormPenalty(Svec, frobPenalty);

    Real gradPenalty = 0.0;
    MOESS_base_.computeGradationPenalty(Svec, gradPenalty, d_dSvec);

    eigenvalue_violation_count = handleStepMatrixViolations(x, h_refine_factor, h_coarsen_factors);

    total_eval_count += eval_count_;

    std::cout<<">  Outer iteration "<<outer_iter<<std::endl;
    std::cout<<"     Result     : "<< nloptResultDescription(result)<<std::endl;
    std::cout<<"     Eval count : "<< eval_count_<<std::endl;
    std::cout<<"     Objective. : "<< f_opt << std::endl;
    std::cout<<"     Cost       : "<< Cost << std::endl;
    std::cout<<"     Step matrix eigenvalue violation count : "<< eigenvalue_violation_count<<std::endl;
    std::cout << "     Err. Est.              : " << ErrorEst << std::endl;
    std::cout << "     Frob Penalty           : " << frobPenalty << std::endl;
    std::cout << "     Gradation Penalty      : " << gradPenalty << std::endl;
    std::cout<<std::endl<<std::endl;

    outer_iter++;
  }
  while (eigenvalue_violation_count > 0 && outer_iter < 10);

  eval_count_ = total_eval_count;

  //Compute the cost estimate
  Real Cost = 0;
  MOESS_base_.computeCost(Svec, Cost, d_dSvec);

  ErrorEst = 0.0;
  MOESS_base_.computeError(Svec, ErrorEst, d_dSvec);

  gradPenalty = 0.0;
  MOESS_base_.computeGradationPenalty(Svec, gradPenalty, d_dSvec);

  std::cout << "-------------------------" << std::endl;
  std::cout << "   MOESS NLopt Summary   " << std::endl;
  std::cout << "-------------------------" << std::endl;
  std::cout << "     Result            : " << nloptResultDescription(result) << std::endl;
  std::cout << "     Eval count        : " << total_eval_count << std::endl;
  std::cout << "     Final objective   : " << f_opt << std::endl;
  std::cout << "     Cost              : " << Cost << std::endl;
  std::cout << "     Elem Req.         : " << MOESS_base_.computeRequestedElements(Svec) << std::endl;
  //std::cout <<"     Eigen Penalty     : " << eigenPenalty << std::endl;
  std::cout << "     Error Est.        : " << ErrorEst << std::endl;
  std::cout << "     Gradation Penalty : " << gradPenalty << std::endl;
  std::cout << "-------------------------" << std::endl << std::endl;

  MOESS_base_.objective_reduction_ratio_ = f_opt/f_init;
}

template <class MOESSClass>
void
MOESS_Gradation_Optimizer<MOESSClass,NLOPT>::
setStepMatrixBounds(const Real h_refine_factor, const std::vector<Real>& h_coarsen_factors)
{
  std::vector<Real> lower_bound_vector(nDim_);
  std::vector<Real> upper_bound_vector(nDim_);

  for (int node = 0; node < nNode_; node++)
  {
    //Set bounds on the step matrix at this node
    Real lower_bound = -2.0*log(h_coarsen_factors[node]); //max coarsening factor = h_refine_factor_limited
    Real upper_bound = +2.0*log(h_refine_factor);         //max refinement factor = h_refine_factor_limited

    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
    {
      lower_bound_vector[node*MatrixSym::SIZE + ind] = lower_bound;
      upper_bound_vector[node*MatrixSym::SIZE + ind] = upper_bound;
    }
  } //loop over nodes

  opt_.set_lower_bounds(lower_bound_vector);
  opt_.set_upper_bounds(upper_bound_vector);

}

template <class MOESSClass>
void
MOESS_Gradation_Optimizer<MOESSClass,NLOPT>::
computeFrobNormSqTargets(const std::vector<Real>& h_coarsen_factors)
{
  FrobNormSq_targets_.resize(nNode_);
  SANS_ASSERT( FrobNormSq_targets_.size() == h_coarsen_factors.size() );

  const int nNode = nNode_;
  for (int node = 0; node < nNode; node++)
    FrobNormSq_targets_[node] = pow(2.0*log(h_coarsen_factors[node]),2.0);
}

template <class MOESSClass>
void
MOESS_Gradation_Optimizer<MOESSClass,NLOPT>::
computeFrobNormPenalty(const std::vector<MatrixSym>& Svec,
                       Real& penalty, std::vector<MatrixSym>& dpenalty_dSvec)
{
  const int SurrealSize = MatrixSym::SIZE;
  typedef SurrealS<SurrealSize> SurrealClass;
  typedef DLA::MatrixSymS<D,SurrealClass> MatrixSymSurreal;

  SurrealClass zero = 0.0;

  const int nNode = nNode_;
  for (int node = 0; node < nNode; node++)
  {
    MatrixSymSurreal S = Svec[node]; //step matrix for this node

    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      S.value(ind).deriv(ind) = 1.0;

    //Calculate Frobenius norm square
    SurrealClass ci = DLA::FrobNormSq(S);

    //Calculate difference between current Frobenius norm sq and target
    ci -= FrobNormSq_targets_[node];

    //Only add penalty if current Frobenius norm sq is larger (i.e. if tmp >= 0 - but smoothly)
    ci = smoothmax(zero, ci, 20.0);

//    if (tmp.value() > 0)
//      tmp_clipped = tmp;
//    else
//      tmp_clipped = 0.0;

    //Apply weighted quadratic penalty
    SurrealClass node_penalty = penalty_weights_[node]*ci*ci;

    penalty += node_penalty.value();

    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      dpenalty_dSvec[node].value(ind) += node_penalty.deriv(ind);

  } //loop over nodes
}

template <class MOESSClass>
void
MOESS_Gradation_Optimizer<MOESSClass,NLOPT>::
computeFrobNormPenalty(const std::vector<MatrixSym>& Svec, Real& penalty)
{
  for (int node = 0; node < nNode_; node++)
  {
    MatrixSym S = Svec[node]; //step matrix for this node

    //Calculate Frobenius norm square
    Real norm = DLA::FrobNorm(S);
    Real tmp = norm*norm;

    //Calculate difference between current Frobenius norm sq and target
    tmp -= FrobNormSq_targets_[node];

    //Only add penalty if current Frobenius norm sq is larger (i.e. if tmp >= 0 - but smoothly)
    Real zero = 0.0;
    Real tmp_clipped = smoothmax(zero, tmp, 20.0);

//    if (tmp.value() > 0)
//      tmp_clipped = tmp;
//    else
//      tmp_clipped = 0.0;

    //Apply weighted quadratic penalty
    Real node_penalty = penalty_weights_[node]*tmp_clipped*tmp_clipped;

    penalty += node_penalty;
  } //loop over nodes
}


template <class MOESSClass>
int
MOESS_Gradation_Optimizer<MOESSClass,NLOPT>::
handleStepMatrixViolations(const std::vector<Real>& x, const Real h_refine_factor, const std::vector<Real>& h_coarsen_factors)
{
  int violation_count = 0;

  for (int node = 0; node < nNode_; node++)
  {
    MatrixSym S;

    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      S.value(ind) = x[node*MatrixSym::SIZE + ind];

    //Obtain eigenvalues of S
    SANS::DLA::VectorS<D,Real> L;
    DLA::EigenValues( S, L );

    for (int d = 0; d < D; d++)
    {
      if ( L[d] < -2.0*log(h_coarsen_factors[node]) ||
           L[d] > +2.0*log(h_refine_factor) )
      {
        //Increase the penalty weight for this node if an eigenvalue is larger than the accepted bound
        penalty_weights_[node] *= 10.0;
        violation_count++;
        break;
      }
    }
  } //loop over nodes

  return violation_count;
}


//Routine that computes the Gradation constraint and its derivative wrt optimization variables
//Constraint form: computeGradationConstraint(x) <= 0
template <class MOESSClass>
double
MOESS_Gradation_Optimizer<MOESSClass,NLOPT>::computeGradationConstraint(unsigned n, const double* x, double* grad)
{
  //Populate step-matrices from the solution vector x
  std::vector<MatrixSym> Svec(nNode_); //Step-matrices at each node

  //Populate step-matrices from the solution vector x
  for (int node = 0; node < nNode_; node++)
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      Svec[node].value(ind) = x[node*MatrixSym::SIZE + ind];

  //---------------------------------------------------------------

  Real gradPenalty = 0.0;
  std::vector<MatrixSym> dgradPenalty_dSvec(nNode_, 0.0);

  Real offset = 1e-6;

  //Compute the gradation penalty term and its derivatives
  MOESS_base_.computeGradationPenalty(Svec, gradPenalty, dgradPenalty_dSvec);
  //if ( gradPenalty - offset >  0 )
  //  std::cout << "eval = " << eval_count_ << " | gradPenalty = " << gradPenalty - 1e-3 << std::endl;

  //---------------------------------------------------------------

  if (grad)
  {
    //Real L2norm = 0.0;

    //Populate grad vector from dError_dSvec
    for (int node = 0; node < nNode_; node++)
    {
      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      {
        grad[node*MatrixSym::SIZE + ind] = dgradPenalty_dSvec[node].value(ind);

        //L2norm += grad[node*MatrixSym::SIZE + ind]*grad[node*MatrixSym::SIZE + ind];
      }
    }
    //MOESS_base_.objective_gradnorm_history_.push_back(sqrt(L2norm));
  }

  //Real obj = Error + frobPenalty + gradPenalty;
  //MOESS_base_.objective_history_.push_back(gradPenalty);

  MOESS_base_.gradation_constraint_history_.push_back(gradPenalty - offset);

  //std::cout << gradPenalty - 1e-3 << " " << std::endl;
  return gradPenalty - offset;
}

template <class MOESSClass>
void
MOESS_Gradation_Optimizer<MOESSClass,NLOPT>::computeEigenValuePenalty(const std::vector<MatrixSym>& Svec,
                                                                      Real& penalty, std::vector<MatrixSym>& dpenalty_dSvec)
{
#if 0
  const int SurrealSize = MatrixSym::SIZE;
  typedef SurrealS<SurrealSize> SurrealClass;
  typedef DLA::MatrixSymS<D,SurrealClass> MatrixSymSurreal;

  SurrealClass zero = 0.0;

  for (int node = 0; node < nNode_; node++)
  {
    MatrixSymSurreal S = Svec[node]; //step matrix for this node

    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      S.value(ind).deriv(ind) = 1.0;

    //Obtain eigenvalues of S
    SANS::DLA::VectorS<D,SurrealClass> L;
    DLA::EigenValues( S, L );

    SurrealClass tmp = 0;
    for (int d = 0; d < D; d++)
    {
      SurrealClass diff = fabs(L[d]) - 2.0*log(h_coarsen_factors_[node]);
      tmp += pow(max(zero, diff), 2);
    }

    //Apply weighted quadratic penalty
    //SurrealClass node_penalty = tmp*MOESS_base_.nodalError0_[node];
    SurrealClass node_penalty = tmp;

    penalty += node_penalty.value();

    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      dpenalty_dSvec[node].value(ind) += node_penalty.deriv(ind);

  } //loop over nodes
#endif
}


//Routine that computes the Eigen Value constraint and its derivative wrt optimization variables
//Constraint form: computeEigenValueConstraint(x) <= 0
template <class MOESSClass>
double
MOESS_Gradation_Optimizer<MOESSClass,NLOPT>::computeEigenValueConstraint(unsigned n, const double* x, double* grad)
{
  SANS_DEVELOPER_EXCEPTION("Not implemented");
#if 0
  const int SurrealSize = MatrixSym::SIZE;
  typedef SurrealS<SurrealSize> SurrealClass;
  typedef DLA::MatrixSymS<D,SurrealClass> MatrixSymSurreal;

  SurrealClass zero = 0;
  Real constraint = 0.0;

  Real L2norm = 0.0;

  for (int node = 0; node < nNode_; node++)
  {
    MatrixSymSurreal S; //step matrix for this node

    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
    {
      S.value(ind) = x[node*MatrixSym::SIZE + ind];
      S.value(ind).deriv(ind) = 1.0;
    }

    //Obtain eigenvalues of S
    SANS::DLA::VectorS<D,SurrealClass> L;
    DLA::EigenValues( S, L );

    SurrealClass tmp = 0;
    for (int d = 0; d < D; d++)
    {
      SurrealClass diff = fabs(L[d]) - 2.0*log(h_coarsen_factors_[node]);
      tmp += pow(max(zero, diff),2);
    }

    // Sum up the constraints
    constraint += tmp.value();

    if (grad)
    {
      //Set the grad vector
      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      {
        grad[node*MatrixSym::SIZE + ind] = tmp.deriv(ind);

        L2norm += pow(tmp.deriv(ind),2);
      }
    }
  } //loop over nodes

  Real offset = 1e-6;

  MOESS_base_.step_constraint_history_.push_back(constraint - offset);
  MOESS_base_.step_constraint_gradnorm_history_.push_back(sqrt(L2norm));

  return constraint - offset;
#endif
  return 0;
}

//Routine that computes the cost penalty and its derivative wrt optimization variables
//Constraint form: computeCostConstraint(x) <= 0
template <class MOESSClass>
void
MOESS_Gradation_Optimizer<MOESSClass,NLOPT>::computeCostPenalty(const std::vector<MatrixSym>& Svec, Real& costPenalty,
                                                                std::vector<MatrixSym>& dcostPenalty_dSvec)
{
  Real Cost = 0.0;
  std::vector<MatrixSym> dCost_dSvec(nNode_, 0.0);

  //Compute the global error estimate and its derivatives
  MOESS_base_.computeCost(Svec, Cost, dCost_dSvec);

  costPenalty += MOESS_base_.error0_*pow(Cost/targetCost_ - 1, 2);

  //Populate grad vector
  for (int node = 0; node < nNode_; node++)
    dcostPenalty_dSvec[node] += MOESS_base_.error0_*2*(Cost/targetCost_ - 1)*dCost_dSvec[node];
}

}
