// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ERRORMODEL_H_
#define ERRORMODEL_H_

#ifdef SANS_PETSC
#include <petscmat.h>
#endif

#include "Field/XField.h"
#include "Field/Field_NodalView.h"
#include "Field/XField_CellToTrace.h"

#include "MOESSParams.h"
#include "ErrorModel_Local.h"
#include "SolverInterfaceBase.h"

namespace SANS
{

// forward declare, actually defined in impl
template<class PhysDim>
struct ErrorStepPairDim;

template <class PhysDim, class TopoDim>
class ErrorModel
{
public:
  static const int D = PhysDim::D;   // physical dimensions

  typedef ErrorStepPairDim<PhysDim> ErrorStepPair; // data type for storing error step pairs

  typedef DLA::MatrixS<D,D,Real> Matrix;
  typedef DLA::MatrixSymS<D,Real> MatrixSym;
  typedef DLA::VectorS<MatrixSym::SIZE, Real> ArrayMatSym; //Data-type to represent symmetric matrix as vector

  typedef std::vector<std::vector<ErrorModel_Local<PhysDim>>> ErrorModelVectorType;

  typedef Field_CG_Cell<PhysDim, TopoDim, MatrixSym> MatrixSymFieldType;
  typedef Field_DG_Cell<PhysDim, TopoDim, ArrayMatSym> MatrixSymFieldType_Elemental;
  typedef Field_CG_Cell<PhysDim, TopoDim, ArrayMatSym> MatrixSymFieldType_Nodal;

  typedef MOESSParams ParamsType;

  ErrorModel(const XField<PhysDim,TopoDim>& xfld_linear,
             const std::vector<int>& cellgroup_list,
             const SolverInterfaceBase<PhysDim,TopoDim>& problem,
             const PyDict& paramsDict);

  void synthesize(const XField<PhysDim, TopoDim>& xfld_curved); // Perform local sampling and construct local error models
  void serialize(const ErrorModel<PhysDim, TopoDim>& errorModelGlobal);

  void getCellsError0(const typename Field_NodalView::IndexVector& cell_list,
                      std::vector<Real>& error0_list) const;

  void computeError(const MatrixSymFieldType& Sfld,
                    Real& Error, std::vector<MatrixSym>& dError_dSvec);

#if 0 //def SANS_PETSC
  void computeErrorHessian(const std::vector<MatrixSym>& Svec, Mat H);
#endif

  void getRateMatrixField(MatrixSymFieldType_Elemental& rate_fld);
  void getRateMatrixField(MatrixSymFieldType_Nodal& rate_fld);

  const ErrorModelVectorType& getErrorModelVector() const {return errorModels_;}

// NOTE ideally we want the following section to be 'protected' but there might be a bug with the
// gnu compilers that don't look up which friend classes can access the protected methods...
// it works in clang and gnu8
public:

  template<class,class> friend class Synthesis_Element_impl;
  template<class,class> friend class computeError_impl;
  template<class,class> friend class RateField_impl;

  template<class Topology>
  void synthesize_element_cellgroup(const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
                            const int cellGroupGlobal,
                            const XField_CellToTrace<PhysDim, TopoDim>& xfld_connectivity,
                            std::vector<Real>& time_breakdown);

#ifdef SANS_MPI
  template<class,class> friend class Serialize_CellGroup_impl;

  template <class Topology>
  void serialize_cellgroup( const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
                            const int cellGroupGlobal,
                            const ErrorModel<PhysDim, TopoDim>& errorModelGlobal );
#endif

  // error model mit derivative
  template<class Topology>
  void computeError_cellgroup(const typename MatrixSymFieldType::template FieldCellGroupType<Topology>& SfldCellGroup,
                              const int cellGroupGlobal,
                              Real& Error, std::vector<MatrixSym>& dError_dSvec);

  void computeNodalError( const MatrixSymFieldType& Sfld,
                          Real& Error, std::vector<MatrixSym>& dError_dSvec);

#ifdef SANS_PETSC
  template<class,class> friend class ErrorHessian_impl;

  template <class Topology>
  void computeErrorHessian_cellgroup(const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
                                     const int cellGroupGlobal, const std::vector<MatrixSym>& Svec,
                                     Mat H);
#endif

  template <class Topology>
  void getRateMatrix_cellgroup(const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
                               const int cellGroupGlobal, MatrixSymFieldType_Elemental& ratefld);

protected: // won't be needed anymore if we sort out the 'protected/public' comment above

  // make nodal based models using edge based local patches
  void synthesize_edge_node( const XField_CellToTrace<PhysDim, TopoDim>& xfld_connectivity,
                        std::vector<Real>& time_breakdown );

  // make element based models using edge based local patches
  void synthesize_edge_element( const XField_CellToTrace<PhysDim, TopoDim>& xfld_connectivity,
                                std::vector<Real>& time_breakdown );


  void syncErrorModels(); // synchronize error models in ghost/zombie cells

  const XField<PhysDim, TopoDim>& xfld_linear_;
  const std::vector<int>& cellgroup_list_;

  const SolverInterfaceBase<PhysDim,TopoDim>& problem_;

  const bool uniform_refinement_; //flag to indicate if the uniform refinement split config is included

  const std::string LocalSolve_;
  const ParamsType::VerbosityOptions::ExtractType verbosity_;

  const bool use_modified_model_; // flag to use the modified model

  //Error models for each element - constructed from local sampling
  ErrorModelVectorType errorModels_; //indexing: [global cellgrp][elem]
};

} // namespace SANS

#endif // ERRORMODEL_H_
