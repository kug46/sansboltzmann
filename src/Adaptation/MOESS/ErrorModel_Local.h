// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ERRORMODEL_LOCAL_H_
#define ERRORMODEL_LOCAL_H_

#include <vector>

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"
#include "Topology/ElementTopology.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Trace.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_FrobNorm.h"

#ifdef SANS_MPI
#include "MPI/serialize_DenseLinAlg_MatrixS.h"
#endif

namespace SANS
{

template <class PhysDim>
class ErrorModel_Local
{

public:
  static const int D = PhysDim::D;   // physical dimensions
  typedef DLA::MatrixSymS<D,Real> MatrixSym;
  typedef DLA::MatrixS<D,D,Real> Matrix;

  ErrorModel_Local();

  ErrorModel_Local(const std::vector<Real>& error1_list, const std::vector<MatrixSym>& step_matrix_list,
                   const Real error0, const int order );

  ~ErrorModel_Local() {}

  void getData(const int config_index, Real& error1, MatrixSym& step_matrix) const;

  Real getError0() const { return error0_; }
  MatrixSym getRateMatrix() const { return rate_matrix_; }
  int getOrder() const { return order_; }

  //Computes an error estimate using the model for a given step matrix S
  template <class T>
  void getErrorEstimate(const DLA::MatrixSymS<D,T>& S, T& error);

  template <class T>
  void getErrorEstimate(const DLA::MatrixSymS<D,T>& S, const T& dSt2, T& error);

  const std::vector<Real>& error1_list() const {return error1_list_;}
  const std::vector<MatrixSym>& step_matrix_list() const {return step_matrix_list_;}

protected:
#ifdef SANS_MPI
  friend class boost::serialization::access;

  template<class Archive>
  void serialize(Archive & ar, const unsigned int version)
  {
    ar & error1_list_;
    ar & step_matrix_list_;
    ar & Nconfig_;
    ar & error0_;
    ar & order_;
    ar & rate_matrix_;
    ar & alpha_;
  }
#endif

  void computeRateMatrix();

  std::vector<Real> error1_list_; // Log error ratio for each split configuration
  std::vector<MatrixSym> step_matrix_list_; //Step matrices for each split configuration
  int Nconfig_; //number of split configurations
  Real error0_; //Error on original (unsplit) element
  int order_; //Solution order (needed for theoretical bound)

  MatrixSym rate_matrix_;
  Real alpha_;
};

template <class PhysDim>
template <class T>
void
ErrorModel_Local<PhysDim>::getErrorEstimate(const DLA::MatrixSymS<D,T>& S, T& error)
{
  error = error0_*exp(DLA::tr(rate_matrix_*S));
}

template <class PhysDim>
template <class T>
void
ErrorModel_Local<PhysDim>::
getErrorEstimate(const DLA::MatrixSymS<D,T>& S, const T& St2, T& error)
{
  // dSt2 makes up all the regularization terms
  error = error0_*exp(DLA::tr(rate_matrix_*S) + alpha_*St2);
}

}

#endif /* ERRORMODEL_LOCAL_H_ */
