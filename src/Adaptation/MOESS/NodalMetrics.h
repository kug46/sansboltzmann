// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef NODALMETRICS_H_
#define NODALMETRICS_H_

#include <vector>
#include <set>

#include "tools/SANSnumerics.h"

#include "Quadrature/QuadratureLine.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Type.h"

#include "Field/XField.h"
#include "Field/Field.h"

#include "MOESSParams.h"
#include "SolverInterfaceBase.h"
#include "CostModel_LogEuclidean.h"

namespace SANS
{

template <class PhysDim, class TopoDim>
class NodalMetrics
{
public:
  typedef PhysDim PhysD;
  static const int D = PhysDim::D;   // physical dimensions

  typedef DLA::VectorS<D,Real> VectorX;

  typedef DLA::MatrixSymS<D,Real> MatrixSym;
  typedef DLA::VectorS<MatrixSym::SIZE, Real> ArrayMatSym; //Data-type to represent symmetric matrix as vector

  typedef std::vector<std::vector<MatrixSym>> ElementalMetricVectorType;

  typedef Field_CG_Cell<PhysDim, TopoDim, Real> ScalarFieldType_Nodal;
  typedef Field_CG_Cell<PhysDim, TopoDim, DLA::VectorS<D, Real>> VectorFieldType_Nodal;
  typedef Field_CG_Cell<PhysDim, TopoDim, MatrixSym> MatrixSymFieldType;
  typedef Field_CG_Cell<PhysDim, TopoDim, ArrayMatSym> MatrixSymFieldType_Nodal;
  typedef Field_DG_Cell<PhysDim, TopoDim, ArrayMatSym> MatrixSymFieldType_Elemental;

  enum MetricNorm
  {
    L2,
    Linf
  };

  typedef MOESSParams ParamsType;

  NodalMetrics(const XField<PhysDim,TopoDim>& xfld_linear,
               const std::vector<int>& cellgroup_list,
               const SolverInterfaceBase<PhysDim,TopoDim>& problem,
               const PyDict& paramsDict);

  NodalMetrics(const XField<PhysDim,TopoDim>& xfld_linear,
               const std::vector<int>& cellgroup_list,
               const SolverInterfaceBase<PhysDim,TopoDim>& problem,
               const PyDict& paramsDict,
               const NodalMetrics<PhysDim,TopoDim>& nodalMetrics_rank0);

  NodalMetrics(const XField<PhysDim,TopoDim>& xfld_linear,
               const std::vector<int>& cellgroup_list,
               const MatrixSymFieldType& target,
               const SolverInterfaceBase<PhysDim,TopoDim>& problem,
               const PyDict& paramsDict);

  void computeCost(const MatrixSymFieldType& Sfld,
                   Real& Cost, std::vector<MatrixSym>& dCost_dS) const;
  Real computeRequestedElements(const MatrixSymFieldType& Sfld) const;
  void computeComplexity(const MatrixSymFieldType& logMfld,
                         Real& Complexity, std::vector<MatrixSym>& dComplexity_dlogM) const;

  void initializeStepMatrices(const Real h_domain_max, const Real hRefineFactorMax,
                              const Real targetCost, Real& cost_initial_estimate,
                              MatrixSymFieldType& Sfld, std::vector<Real>& h_coarsen_factors) const;
  void initializeCostStepMatrices(const Real h_domain_max, const Real hRefineFactorMax,
                                  Real& targetCost, Real& cost_initial_estimate,
                                  MatrixSymFieldType& Sfld, std::vector<Real>& h_coarsen_factors) const;

  void computeEdgeLengthDeviation(const MatrixSymFieldType& logMfld,
                                  Real& EdgeLenDev, std::vector<MatrixSym>& dEdgeLenDev_dlogM);

  void computeQualityAverage( const MatrixSymFieldType& logMfld,
                              Real& QAvg, std::vector<MatrixSym>& dQAvg_dlogM, const bool inverted);

  void computeQualityStatistics( const MatrixSymFieldType& Mfld, Real& Qmean, Real& Qmin, Real& Qmax);
  void computeQualityStatistics_logM( const MatrixSymFieldType& logMfld, Real& Qmean, Real& Qmin, Real& Qmax);

  void computeGradationPenalty_S(const MatrixSymFieldType& Sfld,
                                 Real& penalty, std::vector<MatrixSym>& dpenalty_dS);
  void computeGradationPenalty_logM(const MatrixSymFieldType& logMfld,
                                    Real& penalty, std::vector<MatrixSym>& dpenalty_dlogM);

  void computeStepDeviation_logM(const MatrixSymFieldType& logMfld,
                                 Real& penalty, std::vector<MatrixSym>& dpenalty_dlogM);

  void getStepMatrixField(MatrixSymFieldType_Nodal& step_fld);
  void getStepMatrixEigField(VectorFieldType_Nodal& eig_fld);
  void getEdgeLengthField(ScalarFieldType_Nodal& edgeLen_fld);
  void getEdgeLengths( std::vector<Real>& lengths );
  void getNodalMetricRequestField( MatrixSymFieldType& metric_fld );
  void getNodalImpliedMetricField( MatrixSymFieldType& metric_fld );

  void setNodalStepMatrix(const MatrixSymFieldType& Sfld);

  int nDOF() const { return implied_metric_.nDOF(); }

  // communicator accessor
  std::shared_ptr<mpi::communicator> comm() const { return xfld_linear_.comm(); }

  Real getComplexityTrue() { return complexity_initial_true_; }

  const MatrixSymFieldType& impliedMetrics() const { return implied_metric_; }

  void matchTargetMetric( const MatrixSymFieldType& target );

  const XField<PhysDim, TopoDim>& xfld_linear() { return xfld_linear_; }
  const std::vector<int>& cellgroup_list() { return cellgroup_list_; }

protected:

  template<class, class, class> friend class ImpliedMetric_Optimizer;
  template<class, class> friend class ExtractEdges;

  template <class T>
  void edgeLengthDeviation(const QuadratureLine& quadrature,
                           const Element<DLA::MatrixSymS<D, T>, TopoD1, Line>& mfldElem,
                           const VectorX& eij,
                           T& delUnity);

  template <class T>
  void edgeLength(const QuadratureLine& quadrature,
                  const Element<DLA::MatrixSymS<D, T>, TopoD1, Line>& mfldElem,
                  const VectorX& eij,
                  T& delUnity);

  template <class T>
  void gradationPenalty(const DLA::MatrixSymS<D,T> M[], const VectorX& eij, const Real beta, T& penalty);

  void computeImpliedMetrics();
  void computeNodalError0();
  void computeInitialCostTrue();


  typedef std::pair<int,int> NodePair;
  void extractEdges();
  template<class XTraits, class ITraits>
  void insertEdges( const FieldAssociativity< XTraits >& xfldGroup,
                    const FieldAssociativity< ITraits >& ifldGroup,
                    std::map<NodePair, NodePair>& edges,
                    std::map<NodePair, NodePair>& edgesGhost );

  const XField<PhysDim, TopoDim>& xfld_linear_;
  const std::vector<int> cellgroup_list_;

  Real cost_initial_true_ = 0.0;
  Real cost_initial_estimate_ = 0.0;

  Real complexity_initial_true_ = 0.0;

  //Initial implied metrics at each node
  MatrixSymFieldType implied_metric_;

  //Step matrices at each node - used to store solution from optimizer
  std::vector<MatrixSym> nodalStepMatrices_;

  //Metric requests at each node, corresponding to the step matrix solution
  //MatrixSymFieldType metric_request_;

  std::vector<MatrixSym> nodalMetricSqrt_;
  std::vector<Real> nodalMetricLamMax_;
  std::vector<Real> nodalError0_;

  CostModel_LogEuclidean<PhysDim, TopoDim> costModel_;

  const SolverInterfaceBase<PhysDim,TopoDim>& problem_;

  const std::string ImpliedMetric_;
  const ParamsType::VerbosityOptions::ExtractType verbosity_;
  const Real DOFIncreaseFactor_; // maximum growth factor for cost increase

  MetricNorm metricNorm_;
  const PyDict paramsDict_;

  struct Edge
  {
    NodePair ifld;
    NodePair xfld;
  };

  std::vector<Edge> edges_;
  // Ghost edges needed to complete gradients but not to compute a value
  std::vector<Edge> edgesGhost_;
};

} // namespace SANS

#endif //NODALMETRICS_H_
