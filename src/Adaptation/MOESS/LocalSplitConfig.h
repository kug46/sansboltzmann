// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef LOCALSPLITCONFIG_H_
#define LOCALSPLITCONFIG_H_

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h" // Real

#include "BasisFunction/BasisFunction_RefElement_Split.h"

namespace SANS
{

struct LocalSplitConfig
{
  LocalSplitConfig() : type(ElementSplitType::Edge), edge_index(-1) {}

  LocalSplitConfig(const ElementSplitType type, const int edge)
    : type(type), edge_index(edge) {}

  ElementSplitType type;
  int edge_index;
};

template <class Topology>
struct LocalSplitConfigList;

template <>
struct LocalSplitConfigList<Line>
{
  static std::vector<LocalSplitConfig> get(const bool uniform_refinement)
  {
    return {LocalSplitConfig(ElementSplitType::Isotropic,-1)};
  }
};

template <>
struct LocalSplitConfigList<Triangle>
{
  static std::vector<LocalSplitConfig> get(const bool uniform_refinement)
  {
    std::vector<LocalSplitConfig> vec(3);
    for (int i=0; i<3; i++)
      vec[i].edge_index = i;

    if (uniform_refinement)
      vec.push_back(LocalSplitConfig(ElementSplitType::Isotropic,-1));

    return vec;
  }
};

template <>
struct LocalSplitConfigList<Quad>
{
  static std::vector<LocalSplitConfig> get(const bool uniform_refinement)
  {
    SANS_DEVELOPER_EXCEPTION( "LocalSplitConfigList<Quad>::get - Quad splitting not supported yet.");
    std::vector<LocalSplitConfig> vec;
    return vec;
  }
};

template <>
struct LocalSplitConfigList<Tet>
{
  static std::vector<LocalSplitConfig> get(const bool uniform_refinement)
  {
    std::vector<LocalSplitConfig> vec(6);
    for (int i = 0; i < 6; i++)
      vec[i].edge_index = i;

    if (uniform_refinement)
      vec.push_back(LocalSplitConfig(ElementSplitType::Isotropic,-1));

    return vec;
  }
};

template <>
struct LocalSplitConfigList<Hex>
{
  static std::vector<LocalSplitConfig> get(const bool uniform_refinement)
  {
    SANS_DEVELOPER_EXCEPTION( "LocalSplitConfigList<Hex>::get - Hex splitting not supported yet.");
    std::vector<LocalSplitConfig> vec;
    return vec;
  }
};

template <>
struct LocalSplitConfigList<Pentatope>
{
  static std::vector<LocalSplitConfig> get(const bool uniform_refinement)
  {
    std::vector<LocalSplitConfig> vec(10);
    for (int i=0; i<10; i++)
      vec[i].edge_index = i;

    SANS_ASSERT_MSG( !uniform_refinement , "uniform refinement not supported on pentatopes");
    return vec;
  }
};

}

#endif /* LOCALSPLITCONFIG_H_ */
