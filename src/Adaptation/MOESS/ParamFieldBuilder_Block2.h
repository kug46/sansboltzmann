// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PARAMFIELDBUILDER_BLOCK2_H_
#define PARAMFIELDBUILDER_BLOCK2_H_

#include "tools/Tuple.h"

#include "Field/Tuple/FieldTuple.h"

#include "Field/Local/XField_Local_Base.h"

#include "Field/HField/HField_DG.h"
#include "Field/HField/HField_CG.h"
#include "Field/HField/GenHField_CG.h"

#include "ParamFieldBuilder.h"

namespace SANS
{

//class ParamType_None {};
class ParamType_ArtificialViscosity {};

template<class Type, class PhysD, class TopoD, class QFieldType0, class QFieldType1>
struct ParamFieldBuilder_Block2;

template<class PhysD, class TopoD, class QFieldType0, class QFieldType1>
struct ParamFieldBuilder_Block2<ParamType_None, PhysD, TopoD, QFieldType0, QFieldType1>
{
  typedef PhysD PhysDim;
  typedef TopoD TopoDim;

  typedef XField<PhysDim, TopoDim> FieldType0;
  typedef XField<PhysDim, TopoDim> FieldType1;

  ParamFieldBuilder_Block2(const XField<PhysDim, TopoDim>& xfld, const QFieldType0& qfld0, const QFieldType1& qfld1)
  : fld0(xfld), fld1(xfld) {}

  ParamFieldBuilder_Block2(const XField_Local_Base<PhysDim, TopoDim>& xfld_local,
                           const ParamFieldBuilder_Block2& parambuilder_global,
                           const QFieldType0& qfld0, const QFieldType1& qfld1)
  : fld0(xfld_local), fld1(xfld_local) {}

  const FieldType0& fld0;
  const FieldType1& fld1;
};

template<class PhysD, class TopoD, class QFieldType0, class QFieldType1>
struct ParamFieldBuilder_Block2<ParamType_ArtificialViscosity, PhysD, TopoD, QFieldType0, QFieldType1>
{
  typedef PhysD PhysDim;
  typedef TopoD TopoDim;

  typedef typename QFieldType0::ArrayQ ArrayQ0;
  typedef typename QFieldType1::ArrayQ ArrayQ1;

//  typedef typename Real HType;
  typedef typename DLA::MatrixSymS<PhysDim::D,Real> HType;

  typedef typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, HType>,
                                         Field<PhysDim, TopoDim, ArrayQ1>,
                                         XField<PhysDim, TopoDim>>::type FieldType0;
  typedef typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, HType>,
                                         Field<PhysDim, TopoDim, ArrayQ0>,
                                         XField<PhysDim, TopoDim>>::type FieldType1;

  ParamFieldBuilder_Block2(const XField<PhysDim, TopoDim>& xfld, const QFieldType0& qfld0, const QFieldType1& qfld1)
  : hfld(xfld),
    fld0((hfld,qfld1),xfld),
    fld1((hfld,qfld0),xfld)
  {
    SANS_ASSERT( &xfld == &qfld0.getXField() );
    SANS_ASSERT( &xfld == &qfld1.getXField() );
  }

  ParamFieldBuilder_Block2(const XField_Local_Base<PhysDim, TopoDim>& xfld_local,
                           const ParamFieldBuilder_Block2& parambuilder_global,
                           const QFieldType0& qfld0, const QFieldType1& qfld1)
  : hfld(xfld_local, parambuilder_global.hfld),
    fld0((hfld,qfld1),xfld_local),
    fld1((hfld,qfld0),xfld_local)
  {
    SANS_ASSERT( &xfld_local == &qfld0.getXField() );
    SANS_ASSERT( &xfld_local == &qfld1.getXField() );
  }

//  HField_CG<PhysDim, TopoDim> hfld;
  GenHField_CG<PhysDim, TopoDim> hfld;
  FieldType0 fld0; //non const since they include the primal and sensor solutions
  FieldType1 fld1;
};

}

#endif /* PARAMFIELDBUILDER_BLOCK2_H_ */
