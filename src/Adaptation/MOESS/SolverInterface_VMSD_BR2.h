// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOLVERINTERFACE_VMSD_BR2_H_
#define SOLVERINTERFACE_VMSD_BR2_H_

#include <ostream>
#include <vector>


#include "Discretization/VMSD/AlgebraicEquationSet_VMSD.h"
#include "Discretization/VMSD/AlgebraicEquationSet_Local_VMSD.h"
#include "Discretization/VMSD/AlgebraicEquationSet_VMSD_Linear.h"
#include "Discretization/VMSD/AlgebraicEquationSet_Local_VMSD_Linear.h"
#include "Discretization/VMSDBR2/AlgebraicEquationSet_VMSD_BR2.h"
#include "Discretization/VMSDBR2/AlgebraicEquationSet_Local_VMSD_BR2.h"
#include "Discretization/VMSD/FunctionalCell_VMSD.h"
#include "Discretization/VMSD/JacobianFunctionalCell_VMSD.h"

#include "Discretization/VMSDBR2/FunctionalBoundaryTrace_Dispatch_VMSD_BR2.h"
#include "Discretization/VMSDBR2/JacobianFunctionalBoundaryTrace_Dispatch_VMSD_BR2.h"

#include "SolutionTrek/Continuation/PseudoTime/AlgebraicEquationSet_PTC_VMSD.h"
#include "SolutionTrek/Continuation/PseudoTime/AlgebraicEquationSet_PTC.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#include "MPI/communicator_fwd.h"
#endif

// What will eventually be the true Solver Interface. This is a stop gap solution until output functionals work
#include "Adaptation/MOESS/SolverInterface.h"

#include "ErrorEstimate/VMSD/ErrorEstimate_VMSD.h"
#include "ErrorEstimate/VMSD/ErrorEstimate_VMSD_Linear.h"
#include "ErrorEstimate/VMSD/ErrorEstimate_VMSD_BR2.h"

#ifdef BOUNDARYOUTPUT
// HACK: These boundary integrands are needed for the boundary output at the moment.
//       They should be removed in the future.
#include "Discretization/VMSDBR2/IntegrandBoundaryTrace_Flux_mitState_VMSD_BR2.h"
#endif

#define VMSDPTC

namespace SANS
{

template<class SolutionClass, class AlgebraicEqSet, class OutputIntegrandType>
class SolverInterface_VMSD_BR2 : public SolverInterface<SolutionClass, AlgebraicEqSet, OutputIntegrandType>
{
public:
  typedef SolverInterface<SolutionClass, AlgebraicEqSet, OutputIntegrandType> BaseType;

  typedef typename AlgebraicEqSet::PhysDim PhysDim;
  typedef typename AlgebraicEqSet::TopoDim TopoDim;

  const int D = PhysDim::D;
  typedef XField<PhysDim, TopoDim> XFieldType;

  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef typename SolutionClass::ParamFieldBuilderType ParamFieldBuilderType;

  typedef typename AlgebraicEqSet::NDPDEClass NDPDEClass;
  typedef typename NDPDEClass::template ArrayQ<Real> PDEArrayQ;

  typedef AlgebraicEqSet AlgEqnSet_Global;

  typedef typename LocalEquationSet<AlgEqnSet_Global>::type AlgEqnSet_Local;
  typedef typename AlgEqnSet_Global::FieldBundle FieldBundle;
  typedef typename AlgEqnSet_Global::ErrorEstimateClass ErrorEstimateClass;
  typedef typename AlgEqnSet_Local::FieldBundle_Local FieldBundle_Local;

  typedef SolverInterface_VMSD_BR2 SolverInterfaceClass;

  typedef typename OutputIntegrandType::template ArrayJ<Real> ArrayJ;

  typedef typename AlgEqnSet_Global::ArrayQ ArrayQ;

  template< class... BCArgs >
  SolverInterface_VMSD_BR2(SolutionClass& sol, const ResidualNormType& resNormType, std::vector<Real>& tol, const int quadOrder,
                        const std::vector<int>& cellgroups, const std::vector<int>& interiortracegroups,
                        PyDict& BCList, const std::map< std::string, std::vector<int> >& BCBoundaryGroups,
                        const PyDict& NonlinearSolverDict, const PyDict& AdjLinearSolverDict,
                        const OutputIntegrandType& fcnOutput, BCArgs&... args )
    : BaseType( sol, resNormType, tol, quadOrder, cellgroups, interiortracegroups, BCList, BCBoundaryGroups, NonlinearSolverDict,
                AdjLinearSolverDict, fcnOutput, args... ),
       adjoint_wo_subtraction( xfld_, sol_.adjoint.order, sol_.adjoint.orderp, sol_.adjoint, sol_.mitlg_boundarygroups ),
       adjoint_p( xfld_, sol_.primal.order, sol_.primal.orderp, sol_.primal, sol_.mitlg_boundarygroups )
  {
    /*
    errorIndicator_ = 0.0;
    errorEstimate_ = 0.0;

    //Loop over each cellgroup and find the no. of DOF per cell and solution order for that cellgroup
    nDOFperCell_.resize((int) xfld_.nCellGroups(), -1);
    orderVec_.resize((int) xfld_.nCellGroups(), -1);
    for_each_CellGroup<TopoDim>::apply( CellwiseOperations(cellgroup_list_, *this), xfld_);
    */
  }

  virtual ~SolverInterface_VMSD_BR2() {}

  virtual void solveGlobalPrimalProblem() override;
  virtual void solveGlobalAdjointProblem() override;
  virtual LocalSolveStatus solveLocalProblem(const XField_Local_Base<PhysDim, TopoDim>& local_xfld,
                                             std::vector<Real>& local_error) const override;


  const Field_CG_Cell<PhysDim,TopoDim,PDEArrayQ>& getAdjField() const { return adjoint_wo_subtraction.qfld; }
  const Field_EG_Cell<PhysDim,TopoDim,PDEArrayQ>& getAdjPField() const { return adjoint_wo_subtraction.qpfld; }
  const Field_CG_Cell<PhysDim,TopoDim,PDEArrayQ>& getPAdjField() const { return adjoint_p.qfld; }

  // a specialist local problem, derived from the traditional local problem, but re-defining the estimation process
  class LocalProblem_VMSD : public BaseType::LocalProblem
  {
    public:
    typedef typename SolverInterface<SolutionClass,AlgebraicEqSet,OutputIntegrandType>::LocalProblem LocalProblemBaseType;
    LocalProblem_VMSD( const SolverInterfaceClass& interface,
                       const XField_Local_Base<PhysDim,TopoDim>& xfld_local, LocalSolveStatus& status ) :
    LocalProblemBaseType(interface, xfld_local, status),
    construct_timer_(),
    interface_(interface),
    up_local_est_fld_(SANS::make_unique<Field_Local<Field_DG_Cell<PhysDim,TopoDim,Real>>>
        ( xfld_local, interface_.pErrorEstimate_->efld_DG_nodal(), 1, BasisFunctionCategory_Lagrange))
    {
      //Add the 0-0 interior trace groups to local_interiorTraceGroups_
      std::vector<int> tmp = findInteriorTraceGroup(xfld_local_,0,0);

      if (!tmp.empty())
      {
        local_interiorTraceGroups_.push_back(tmp[0]);
        std::sort(local_interiorTraceGroups_.begin(),local_interiorTraceGroups_.end());
      }

      status.time_constructor += construct_timer_.elapsed();
    }


    virtual ~LocalProblem_VMSD() {};

    // This override warning needs to come before the definition of getLocalError()
    virtual void estimateLocalError( const XField_Local_Base<PhysDim,TopoDim>& local_xfld,
                                     FieldBundle_Local& primal, FieldBundle_Local& adjoint ) override;

    protected:

    template<class Temporal>
    typename std::enable_if< std::is_same<Temporal,TemporalMarch>::value, void >::type
    solveGlobalPrimalContinuation();

    timer construct_timer_;

    virtual void dumpLocalFields() override
    {
      output_Tecplot(   primal_local_.qfld,
        interface_.filename_base_ + "qfld_local_unconverged.plt", std::vector<std::string>(), true );
      output_Tecplot(  primal_local_.qpfld,
        interface_.filename_base_ + "qpfld_local_unconverged.plt", std::vector<std::string>(), true );
      output_Tecplot(  adjoint_local_.qfld,
        interface_.filename_base_ + "adjfld_local_unconverged.plt", std::vector<std::string>(), true );
      output_Tecplot( adjoint_local_.qpfld,
        interface_.filename_base_ + "adjpfld_local_unconverged.plt", std::vector<std::string>(), true );
    };

    // need an actual reference here because we need to access protected members, and friend status only extends to nested classes
    // rather than to derived classes nested classes.
    // Thus can access protected members of a SolverInterface_VMSD_BR2 class, but none from a SolverInterface class
    const SolverInterfaceClass& interface_;
    std::unique_ptr<Field_DG_Cell<PhysDim,TopoDim,Real>> up_local_est_fld_; // for transfering the global estimate
    using LocalProblemBaseType::xfld_local_;
    using LocalProblemBaseType::status_;
    using LocalProblemBaseType::quadrule_;
    using LocalProblemBaseType::maxIter;
    using LocalProblemBaseType::BCList_local_;
    using LocalProblemBaseType::BCBoundaryGroups_local_;
    using LocalProblemBaseType::local_cellGroups_;
    using LocalProblemBaseType::local_interiorTraceGroups_;
    using LocalProblemBaseType::active_local_boundaries_;
    using LocalProblemBaseType::primal_local_;
    using LocalProblemBaseType::adjoint_local_;
    using LocalProblemBaseType::pliftedQuantityfld_local_;
    using LocalProblemBaseType::parambuilder_;
    using LocalProblemBaseType::stol;
    using LocalProblemBaseType::local_error_;
  };

  // function to fill the residual field pointer based on the solve
  void prepareResidualField();

  void dumpResidualFields()
  {
    typedef typename AlgEqnSet_Global::SystemVector SystemVectorClass;

    // compute the resiual
    SystemVectorClass rsd(AlgEqnSet_.vectorEqSize());
    AlgEqnSet_.residual(rsd);

    // create fields for the residual
    Field_EG_Cell<PhysDim,TopoDim,ArrayQ> respfld(xfld_,sol_.primal.orderp, sol_.primal.basis_dgcell, cellgroup_list_);
    Field_CG_Cell<PhysDim,TopoDim,ArrayQ> resfld (xfld_,sol_.primal.order , sol_.primal.basis_cgcell, cellgroup_list_);

    // fill the residual fields
    for (int i = 0; i < rsd[0].m(); i++)
      respfld->DOF(i) = rsd[0][i];

    for (int i = 0; i < rsd[1].m(); i++)
      resfld->DOF(i) = rsd[1][i];

    output_Tecplot( respfld, filename_base_ + "respfld.dat" );
    output_Tecplot( resfld , filename_base_ + "resfld.dat" );
  }

protected:

  template<class Temporal>
  typename std::enable_if< std::is_same<Temporal,TemporalMarch>::value, void >::type
  solveGlobalPrimalContinuation();

  template<class Temporal>
  typename std::enable_if< std::is_same<Temporal,TemporalSpaceTime>::value, void >::type
  solveGlobalPrimalContinuation();

//  template<class Temporal>
//  typename std::enable_if< std::is_same<Temporal,TemporalSpaceTime>::value, void >::type
//  solveGlobalPrimalContinuation() override;

  using BaseType::sol_; //class containing all the solution data
  using BaseType::xfld_; // mesh
  using BaseType::resNormType_;
  using BaseType::tol_;
  using BaseType::quadOrder_;
  using BaseType::cellgroup_list_;
  using BaseType::interiortracegroup_list_;
  using BaseType::BCList_;
  using BaseType::BCBoundaryGroups_;
  using BaseType::NonlinearSolverDict_;
  using BaseType::AdjLinearSolverDict_;
  using BaseType::verbosity_;

  using BaseType::AlgEqnSet_; // AlgebraicEquationSet

  using BaseType::errorArray_; //Array of elemental error estimates (index:[cellgroup][elem])
  using BaseType::fcnOutput_;
  using BaseType::errorIndicator_;
  using BaseType::errorEstimate_;
  using BaseType::outputJ_; //output functional evaluated at current solution

  using BaseType::nDOFperCell_; //indexing: [global cellgrp] - number of degrees of freedom per cell
  using BaseType::orderVec_; //indexing: [global cellgrp] - solution order of each cellgroup
  using BaseType::filename_base_;

  FieldBundle adjoint_wo_subtraction; // Adjoint without removing the p solution, useful for debugging
  FieldBundle adjoint_p; // Adjoint in space p, useful for debugging
};


template<class SolutionClass, class AlgebraicEqSet, class OutputIntegrandType>
void
SolverInterface_VMSD_BR2<SolutionClass, AlgebraicEqSet, OutputIntegrandType>::
prepareResidualField()
{
  // The residual field needs to be stored in a DG field
  // declare the pointer for the residual field here. It is actually owned by the FieldBundle class
  sol_.primal.up_resfld = SANS::make_unique<Field_DG_Cell<PhysDim,TopoDim,ArrayQ>>
                          (xfld_,sol_.primal.order, sol_.primal.basis_cgcell, cellgroup_list_);

  //transfer solution to DG field so that residual fcn can be re-used
  Field_DG_Cell<PhysDim,TopoDim,ArrayQ> qfld_dg(xfld_, sol_.primal.order, sol_.primal.basis_cgcell, cellgroup_list_);

  sol_.primal.qfld.projectTo( qfld_dg );

  // TO DO: Call the residual to fill this field up
  AlgEqnSet_.fillResidualField( qfld_dg, sol_.primal.up_resfld );

  //SYNC RESIDUAL FIELD
  sol_.primal.up_resfld->syncDOFs_MPI_noCache();

//  output_Tecplot( *(sol_.primal.up_resfld), "tmp/rsdfld.plt" );

  // Once the residual field is filled/set by the call to this function
  // if a local field is created using the global field bundle, it will also transfer the DG field
}

template<class SolutionClass, class AlgebraicEqSet, class OutputIntegrandType>
void
SolverInterface_VMSD_BR2<SolutionClass, AlgebraicEqSet, OutputIntegrandType>::
solveGlobalPrimalProblem()
{
  timer clock;

  this->template solveGlobalPrimalContinuation<typename NDPDEClass::Temporal>();

  if (get<-1>(xfld_).comm()->rank() == 0 && verbosity_ )
    std::cout << "Primal solve time: " << std::fixed << std::setprecision(3) << clock.elapsed() << "s" << std::endl;

  // evaluate output function
  outputJ_ = 0.0;

  QuadratureOrder quadrule(xfld_, quadOrder_);

#ifndef BOUNDARYOUTPUT
  IntegrateCellGroups<TopoDim>::integrate( FunctionalCell_VMSD( fcnOutput_, outputJ_ ),
                                           sol_.paramfld, (sol_.primal.qfld, sol_.primal.qpfld),
                                           quadrule.cellOrders.data(), quadrule.cellOrders.size() );
#else
  AlgEqnSet_.dispatchBC().dispatch_VMSD(
      FunctionalBoundaryTrace_Dispatch_VMSD_BR2(fcnOutput_, sol_.paramfld, sol_.primal.qfld, sol_.primal.qpfld, sol_.primal.rbfld,
                                                quadrule.boundaryTraceOrders.data(),
                                                quadrule.boundaryTraceOrders.size(), outputJ_ ) );

  // FunctionalCell_Galerkin already has an all_reduce built in!
  #ifdef SANS_MPI
    outputJ_ = boost::mpi::all_reduce( *get<-1>(xfld_).comm(), outputJ_, std::plus<Real>() );
  #endif
#endif

    //compute global dg residual field for local solves later
#if (!defined(WHOLEPATCH)) && (!defined(INNERPATCH))
    prepareResidualField();
#endif
}


template<class SolutionClass, class AlgebraicEqSet, class OutputIntegrandType>
template <class Temporal>
typename std::enable_if<std::is_same<Temporal, TemporalSpaceTime>::value, void>::type
SolverInterface_VMSD_BR2<SolutionClass, AlgebraicEqSet, OutputIntegrandType>::
solveGlobalPrimalContinuation()
{
  typedef typename AlgEqnSet_Global::SystemMatrix SystemMatrixClass;
  typedef typename AlgEqnSet_Global::SystemVector SystemVectorClass;

  // TODO: note that currently it defaults to non-manifold integrand in PTC
  typedef AlgebraicEquationSet_PTC<NDPDEClass, AlgEqSetTraits_Sparse, PTC, ParamFieldType> AlgebraicEquationSet_PTCClass;

  typedef SolverContinuationParams<typename NDPDEClass::Temporal> ContinuationParams;

  //Get the requested continuation
  DictKeyPair continuation = NonlinearSolverDict_.get(ContinuationParams::params.Continuation);

  if (continuation == ContinuationParams::params.Continuation.None)
  {
    // Create the nonlinear solver object
    NonLinearSolver<SystemMatrixClass> nonlinear_solver( AlgEqnSet_, continuation );

    // set initial condition from current solution in solution fields
    SystemVectorClass sln0(AlgEqnSet_.vectorStateSize());
    AlgEqnSet_.fillSystemVector(sln0);

    // nonlinear solve
    SystemVectorClass sln(AlgEqnSet_.vectorStateSize());
    SolveStatus status = nonlinear_solver.solve(sln0, sln);

    if (status.converged == false)
    {
      sol_.dumpPrimalSolution( filename_base_, "_unconverged.plt" );

      if (sol_.pliftedQuantityfld)
        output_Tecplot( *sol_.pliftedQuantityfld, filename_base_ + "liftedQuantityfld_unconverged.plt" );

      SANS_DEVELOPER_EXCEPTION("Nonlinear solver failed to converge.");
    }
  }
  else if (continuation == ContinuationParams::params.Continuation.PseudoTime)
  {
    AlgebraicEquationSet_PTCClass AlgEqSetPTC(sol_.paramfld,
                                              sol_.primal.qfld, sol_.pde,
                                              QuadratureOrder(xfld_, quadOrder_),
                                              cellgroup_list_, AlgEqnSet_);
    // Create the pseudo time continuation class
    PseudoTime<SystemMatrixClass> PTC(continuation, AlgEqSetPTC);

    if (!PTC.solve())
    {
      output_Tecplot( sol_.primal.qfld, filename_base_ + "qfld_unconverged.plt" );

      if (sol_.pliftedQuantityfld)
        output_Tecplot( *sol_.pliftedQuantityfld, filename_base_ + "liftedQuantityfld_unconverged.plt" );

      SANS_DEVELOPER_EXCEPTION("Nonlinear solver with PTC failed to converge.");
    }
  }
  else
    SANS_DEVELOPER_EXCEPTION("SolverInterface::solveGlobalPrimalProblem - Unknown nonlinear solver continuation type.");
}


template<class SolutionClass, class AlgebraicEqSet, class OutputIntegrandType>
template<class Temporal>
typename std::enable_if< std::is_same<Temporal,TemporalMarch>::value, void >::type
SolverInterface_VMSD_BR2<SolutionClass, AlgebraicEqSet, OutputIntegrandType>::
solveGlobalPrimalContinuation()
{
  typedef typename AlgEqnSet_Global::SystemMatrix SystemMatrixClass;
  typedef typename AlgEqnSet_Global::SystemVector SystemVectorClass;

  // TODO: note that currently it defaults to non-manifold integrand in PTC
#ifdef VMSDPTC
  typedef AlgebraicEquationSet_PTC_VMSD<NDPDEClass, AlgEqSetTraits_Sparse, PTC, ParamFieldType, AlgebraicEqSet> AlgebraicEquationSet_PTCClass;
#else
  typedef AlgebraicEquationSet_PTC<NDPDEClass, AlgEqSetTraits_Sparse, PTC, ParamFieldType> AlgebraicEquationSet_PTCClass;
#endif
  typedef AlgebraicEquationSet_IRS<NDPDEClass, AlgEqSetTraits_Sparse, IRS, ParamFieldType> AlgebraicEquationSet_IRSClass;

  typedef SolverContinuationParams<typename NDPDEClass::Temporal> ContinuationParams;

  //Get the requested continuation
  DictKeyPair continuation = NonlinearSolverDict_.get(ContinuationParams::params.Continuation);

  if (continuation == ContinuationParams::params.Continuation.None)
  {
    // Create the nonlinear solver object
    NonLinearSolver<SystemMatrixClass> nonlinear_solver( AlgEqnSet_, continuation );

    // set initial condition from current solution in solution fields
    SystemVectorClass sln0(AlgEqnSet_.vectorStateSize());
    AlgEqnSet_.fillSystemVector(sln0);

    // nonlinear solve
    SystemVectorClass sln(AlgEqnSet_.vectorStateSize());
    SolveStatus status = nonlinear_solver.solve(sln0, sln);

    if (status.converged == false)
    {
      sol_.dumpPrimalSolution( filename_base_, "_unconverged.plt" );

      if (sol_.pliftedQuantityfld)
        output_Tecplot( *sol_.pliftedQuantityfld, filename_base_ + "liftedQuantityfld_unconverged.plt" );

      SANS_DEVELOPER_EXCEPTION("Nonlinear solver failed to converge.");
    }
  }
  else if (continuation == ContinuationParams::params.Continuation.PseudoTime)
  {
#ifdef VMSDPTC
    AlgebraicEquationSet_PTCClass AlgEqSetPTC(sol_.paramfld,
                                              sol_.primal.qfld,
                                              sol_.primal.qpfld, sol_.pde,
                                              QuadratureOrder(xfld_, quadOrder_),
                                              cellgroup_list_, AlgEqnSet_);
#else
    AlgebraicEquationSet_PTCClass AlgEqSetPTC(sol_.paramfld,
                                              sol_.primal.qfld, sol_.pde,
                                              QuadratureOrder(xfld_, quadOrder_),
                                              cellgroup_list_, AlgEqnSet_);
#endif

    // Create the pseudo time continuation class
    PseudoTime<SystemMatrixClass> PTC(continuation, AlgEqSetPTC);

    if (!PTC.solve())
    {
      sol_.dumpPrimalSolution( filename_base_, "_unconverged.plt" );

      if (sol_.pliftedQuantityfld)
        output_Tecplot( *sol_.pliftedQuantityfld, filename_base_ + "liftedQuantityfld_unconverged.plt" );

      SANS_DEVELOPER_EXCEPTION("Nonlinear solver with PTC failed to converge.");
    }
  }
  else if (continuation == ContinuationParams::params.Continuation.ImplicitResidualSmoothing)
  {
    AlgebraicEquationSet_IRSClass AlgEqSetPTC(sol_.paramfld,
                                              sol_.primal.qfld, sol_.pde,
                                              QuadratureOrder(xfld_, quadOrder_),
                                              cellgroup_list_, AlgEqnSet_);
    // Create the pseudo time continuation class
    ImplicitResidualSmoothing<SystemMatrixClass> IRS(continuation, AlgEqSetPTC);

    if (!IRS.solve())
    {
      output_Tecplot( sol_.primal.qfld, filename_base_ + "qfld_unconverged.plt" );

      if (sol_.pliftedQuantityfld)
        output_Tecplot( *sol_.pliftedQuantityfld, filename_base_ + "liftedQuantityfld_unconverged.plt" );

      SANS_DEVELOPER_EXCEPTION("Nonlinear solver with IRS failed to converge.");
    }
  }
  else
    SANS_DEVELOPER_EXCEPTION("SolverInterface::solveGlobalPrimalProblem - Unknown nonlinear solver continuation type.");
}

template<class SolutionClass, class AlgebraicEqSet, class OutputIntegrandType>
void
SolverInterface_VMSD_BR2<SolutionClass, AlgebraicEqSet, OutputIntegrandType>::
solveGlobalAdjointProblem()
{
  typedef typename AlgEqnSet_Global::SystemVector SystemVectorClass;
  typedef typename AlgEqnSet_Global::SystemMatrix SystemMatrixClass;

  const int iPDE = AlgEqnSet_.iPDE;
  const int iPDEp = AlgEqnSet_.iPDEp;

  QuadratureOrder quadrule(xfld_, quadOrder_);

  timer clock_Pp1;

  // Field bundle for prolongating the p adjoint for subtraction
  FieldBundle adj_prolong( xfld_, sol_.adjoint.order, sol_.adjoint.orderp, sol_.adjoint,
                           sol_.mitlg_boundarygroups );

  // The estimates used in CG need a p solution subtracted, because the test space doesn't contain constants
  timer clock_p;

  {
    if (get<-1>(xfld_).comm()->rank() == 0 && verbosity_ )
      std::cout<< "using solve for p adjoint" << std::endl;

    for (int i = 0; i < adjoint_p.rfld.nDOF(); i++)
      adjoint_p.rfld.DOF(i) = 0;

    for (int i = 0; i < adjoint_p.rbfld.nDOF(); i++)
      adjoint_p.rbfld.DOF(i) = 0;

    // Using a P solve, works but seems wasteful/projection seems a little more stable
    // Should re-use the final primal jacobian
    SystemVectorClass rhs_p( AlgEqnSet_.vectorEqSize() );
    rhs_p = 0;

  #ifndef BOUNDARYOUTPUT
      IntegrateCellGroups<TopoDim>::integrate( JacobianFunctionalCell_VMSD( fcnOutput_, rhs_p(iPDEp), rhs_p(iPDE) ),
                                               sol_.paramfld, (sol_.primal.qfld, sol_.primal.qpfld),
                                               quadrule.cellOrders.data(), quadrule.cellOrders.size() );
  #else
      typedef SurrealS<NDPDEClass::N> SurrealClass;

      AlgEqnSet_.dispatchBC().dispatch_VMSD(
          JacobianFunctionalBoundaryTrace_Dispatch_VMSD_BR2<SurrealClass>(fcnOutput_, sol_.paramfld, sol_.primal.qfld, sol_.primal.qpfld,
                                                                       sol_.primal.rbfld, adjoint_p.rbfld,
                                                                          quadrule.boundaryTraceOrders.data(),
                                                                          quadrule.boundaryTraceOrders.size(), rhs_p(iPDE), rhs_p(iPDEp) ) );
  #endif

    // Adjoint solver
    SLA::LinearSolver< SystemMatrixClass > solver_p( AdjLinearSolverDict_, AlgEqnSet_, SLA::TransposeSolve );

    SystemVectorClass adj_p( AlgEqnSet_.vectorStateSize() );
    adj_p = 0; // set initial guess

    solver_p.solve( rhs_p, adj_p );

    // update solution
    AlgEqnSet_.setAdjointField( adj_p, adjoint_p.qfld, adjoint_p.qpfld, adjoint_p.rfld, adjoint_p.rbfld, adjoint_p.lgfld );

    adjoint_p.qfld.syncDOFs_MPI_noCache();
    adjoint_p.qpfld.syncDOFs_MPI_noCache();
    adjoint_p.lgfld.syncDOFs_MPI_noCache();
  }

  // prolongate to the P+1 basis
  adjoint_p.qfld.projectTo(adj_prolong.qfld);
  adjoint_p.qpfld.projectTo(adj_prolong.qpfld);
  adjoint_p.rfld.projectTo(adj_prolong.rfld);
  adjoint_p.rbfld.projectTo(adj_prolong.rbfld);
//  for (int i = 0; i < adj_prolong.rfld.nDOF(); i++)
//    adj_prolong.rfld.DOF(i) = 0;

//  for (int i = 0; i < adj_prolong.rbfld.nDOF(); i++)
//    adj_prolong.rbfld.DOF(i) = 0;

  adjoint_p.lgfld.projectTo(adj_prolong.lgfld);

  if (get<-1>(xfld_).comm()->rank() == 0 && verbosity_ )
    std::cout << "Adjoint order=" << sol_.primal.order << " solve time: "
              << std::fixed << std::setprecision(3) << clock_p.elapsed() << "s" << std::endl;

  //Create primal solutions in richer solution space
  FieldBundle primal_pro(xfld_, sol_.adjoint.order, sol_.adjoint.orderp,
                         sol_.primal, sol_.mitlg_boundarygroups );

  // Prolongate the primal solution to the richer solution space
  sol_.primal.projectTo(primal_pro);

  // Create param fields with prolongated primal solution
  ParamFieldBuilderType parambuilder(sol_.paramfld, primal_pro.qfld, sol_.parambuilder.dict);

  //Create AlgebraicEquationSets for solutions in richer space
  DiscretizationVMSD discAdj(sol_.disc);

  discAdj.setNitscheOrder( sol_.primal.order );

  AlgEqnSet_Global AlgEqnSet_Pro(parambuilder.fld, primal_pro, sol_.pliftedQuantityfld,
                                 sol_.pde, discAdj, quadrule, resNormType_, tol_,
                                 cellgroup_list_, interiortracegroup_list_,
                                 BCList_, BCBoundaryGroups_);

  // Functional integral
  SystemVectorClass rhs(AlgEqnSet_Pro.vectorEqSize());
  rhs = 0;

  sol_.adjoint.rbfld = 0;
  sol_.adjoint.rfld = 0;

#ifndef BOUNDARYOUTPUT
  IntegrateCellGroups<TopoDim>::integrate( JacobianFunctionalCell_VMSD( fcnOutput_, rhs(iPDEp), rhs(iPDE) ),
                                           parambuilder.fld, (primal_pro.qfld, primal_pro.qpfld),
                                           quadrule.cellOrders.data(), quadrule.cellOrders.size() );
#else
  typedef SurrealS<NDPDEClass::N> SurrealClass;

  AlgEqnSet_Pro.dispatchBC().dispatch_VMSD(
      JacobianFunctionalBoundaryTrace_Dispatch_VMSD_BR2<SurrealClass>(fcnOutput_, parambuilder.fld, primal_pro.qfld, primal_pro.qpfld,
                                                                   primal_pro.rbfld,  sol_.adjoint.rbfld,
                                                                      quadrule.boundaryTraceOrders.data(),
                                                                      quadrule.boundaryTraceOrders.size(), rhs(iPDE), rhs(iPDEp) ) );
#endif

  // adjoint solver
  SLA::LinearSolver< SystemMatrixClass > solver(AdjLinearSolverDict_, AlgEqnSet_Pro, SLA::TransposeSolve);

  SystemVectorClass adj(AlgEqnSet_Pro.vectorStateSize());

#if 0
  // set the initial guess as the prolongated adjoint solution
  AlgEqnSet_Pro.fillSystemVector(adj_prolong.qfld, adj_prolong.qpfld, adj_prolong.lgfld, adj);
#else
  adj = 0.0; // set the initial guess to 0
#endif


  solver.solve(rhs, adj);

  // update solution
  AlgEqnSet_Pro.setAdjointField(adj, sol_.adjoint.qfld, sol_.adjoint.qpfld,
                                sol_.adjoint.rfld, sol_.adjoint.rbfld, sol_.adjoint.lgfld );

  sol_.adjoint.qfld.syncDOFs_MPI_noCache();
  sol_.adjoint.qpfld.syncDOFs_MPI_noCache();
  sol_.adjoint.lgfld.syncDOFs_MPI_noCache();

  if (get<-1>(xfld_).comm()->rank() == 0 && verbosity_ )
    std::cout << "Adjoint order=" << sol_.adjoint.order << "w/ p' order = "
               << sol_.adjoint.orderp << " solve time: "
              << std::fixed << std::setprecision(3) << clock_Pp1.elapsed() << "s" << std::endl;


  // subtract off the prolongated adjoint from the higher-order adjoint
  for (int i = 0; i < adj_prolong.qfld.nDOF(); i++)
  {
    adjoint_wo_subtraction.qfld.DOF(i) = sol_.adjoint.qfld.DOF(i);
    sol_.adjoint.qfld.DOF(i) -= adj_prolong.qfld.DOF(i);
  }

  for (int i = 0; i < adj_prolong.qpfld.nDOF(); i++)
  {
    adjoint_wo_subtraction.qpfld.DOF(i) = sol_.adjoint.qpfld.DOF(i);
    sol_.adjoint.qpfld.DOF(i) -= adj_prolong.qpfld.DOF(i);
  }

  for (int i = 0; i < adj_prolong.rfld.nDOF(); i++)
  {
    adjoint_wo_subtraction.rfld.DOF(i) = sol_.adjoint.rfld.DOF(i);
    sol_.adjoint.rfld.DOF(i) -= adj_prolong.rfld.DOF(i);
  }

  for (int i = 0; i < adj_prolong.rbfld.nDOF(); i++)
  {
    adjoint_wo_subtraction.rbfld.DOF(i) = sol_.adjoint.rbfld.DOF(i);
    sol_.adjoint.rbfld.DOF(i) -= adj_prolong.rbfld.DOF(i);
  }


//  output_Tecplot(sol_.adjoint.qfld, "tmp/adj_delta.plt" );

}

template<class SolutionClass, class AlgebraicEqSet, class OutputIntegrandType>
LocalSolveStatus
SolverInterface_VMSD_BR2<SolutionClass, AlgebraicEqSet, OutputIntegrandType>::
solveLocalProblem(const XField_Local_Base<PhysDim, TopoDim>& xfld_local, std::vector<Real>& local_error) const
{
  // std::cout<< "Using the standard local solve call " << std::endl;
  const int maxIter = 10;
  LocalSolveStatus status(false, maxIter);

  LocalProblem_VMSD localProblem( *this, xfld_local, status );
  local_error = localProblem.getLocalError();
  return status;
}

// separating off the estimation part
template<class SolutionClass, class AlgebraicEqSet, class OutputIntegrandType>
void
SolverInterface_VMSD_BR2<SolutionClass, AlgebraicEqSet, OutputIntegrandType>::LocalProblem_VMSD::
estimateLocalError( const XField_Local_Base<PhysDim, TopoDim>& xfld_local,
                    FieldBundle_Local& primal_local, FieldBundle_Local& adjoint_local )
{
  //Compute local error estimates
  timer clock_errest;
  // std::cout<< "Using the virtual CG local estimate call " << std::endl;

  // the local cell groups, interior trace groups and BC boundary groups account for only computing those needed for cell group 0.
  ErrorEstimateClass ErrorEstimate(parambuilder_.fld, primal_local, adjoint_local,
                                   up_local_est_fld_, pliftedQuantityfld_local_,
                                   interface_.sol_.pde, interface_.sol_.disc, quadrule_,
                                   local_cellGroups_, local_interiorTraceGroups_,
                                   BCList_local_, BCBoundaryGroups_local_);

  ErrorEstimate.getLocalSolveErrorIndicator(local_error_); //Accumulate errors only from cellgroup 0 - (main cells)

  status_.time_errest += clock_errest.elapsed();
}


} // namespace SANS

#endif /* SOLVERINTERFACE_VMSD_BR2_H_ */
