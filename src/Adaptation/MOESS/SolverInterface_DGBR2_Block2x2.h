// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOLVERINTERFACE_DGBR2_BLOCK2X2_H_
#define SOLVERINTERFACE_DGBR2_BLOCK2X2_H_

#include <boost/mpl/vector_c.hpp>

#include <ostream>
#include <vector>
#include <type_traits>

#include "tools/SANSnumerics.h"     // Real
#include "tools/timer.h"
#include "Topology/Dimension.h"
#include "Topology/ElementTopology.h"

#include "SolverInterfaceBase.h"

#include "Discretization/DG/AlgebraicEquationSet_DGBR2.h"
#include "Discretization/DG/AlgebraicEquationSet_Local_DG.h"

#include "Discretization/JacobianParam.h"
#include "Discretization/JacobianParam_Local.h"
#include "Discretization/Block/AlgebraicEquationSet_Block2x2.h"
#include "Discretization/IntegrateCellGroups.h"

#include "Discretization/ResidualNormType.h"
#include "Discretization/AlgebraicEquationSet_Type.h" // Meta class for getting local EqSet from a global EqSet

#include "Discretization/DG/FunctionalCell_DGBR2.h"
#include "Discretization/DG/JacobianFunctionalCell_DGBR2.h"

#include "Discretization/DG/FunctionalBoundaryTrace_Dispatch_DGBR2.h"
#include "Discretization/DG/JacobianFunctionalBoundaryTrace_Dispatch_DGBR2.h"

#include "Field/Local/XField_Local_Base.h"
#include "Field/output_Tecplot.h"
#include "Field/tools/for_each_CellGroup.h"

#include "SolutionTrek/Continuation/Continuation.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"
#include "LinearAlgebra/DenseLinAlg/InverseLUP.h"

#include "Adaptation/MOESS/ParamFieldBuilder_Block2.h"

#include "ErrorEstimate/DG/ErrorEstimate_DGBR2.h"

namespace SANS
{

template<class PhysD, class TopoD,
         class NDPDEClass0, template<class,class> class BCNDConvert0, class BCVector0,
         class NDPDEClass1, template<class,class> class BCNDConvert1, class BCVector1,
         class ParamBuilderType, class SolutionClass,
         class AlgebraicEqSet0, class AlgebraicEqSet1,
         class OutputIntegrandType>
class SolverInterface_DGBR2_Block2x2 : public SolverInterfaceBase<PhysD, TopoD>
{

public:
  typedef PhysD PhysDim;
  typedef TopoD TopoDim;

  const int D = PhysDim::D;
  typedef XField<PhysDim, TopoDim> XFieldType;

//  typedef SolutionData_DGBR2_Block2<PhysDim, TopoDim, NDPDEClass0, NDPDEClass1, ParamBuilderType> SolutionClass;

  typedef typename NDPDEClass0::template MatrixParam<Real> PDEMatrixParam0;
  typedef typename NDPDEClass1::template MatrixParam<Real> PDEMatrixParam1;

  typedef typename NDPDEClass0::template ArrayQ<Real> PDEArrayQ0;
  typedef typename NDPDEClass1::template ArrayQ<Real> PDEArrayQ1;

  typedef AlgebraicEqSet0 AlgEqnSet_Global0;
  typedef AlgebraicEqSet1 AlgEqnSet_Global1;

  typedef typename AlgEqnSet_Global0::TraitsTag TraitsTag0;
  typedef typename AlgEqnSet_Global1::TraitsTag TraitsTag1;

  static_assert( std::is_same<TraitsTag0,TraitsTag1>::value, "TraitsTag of AlgEqSet 0 and 1 must match");
  static_assert( std::is_same<TraitsTag0,AlgEqSetTraits_Sparse>::value, "Must be Sparse AlgebraicEquationSets");

  typedef typename LocalEquationSet<AlgEqnSet_Global0>::type AlgEqnSet_Local0;
  typedef typename LocalEquationSet<AlgEqnSet_Global1>::type AlgEqnSet_Local1;

  typedef typename SolutionClass::FieldBundleType0 FieldBundle0;
  typedef typename AlgEqnSet_Global0::ErrorEstimateClass ErrorEstimateClass0;
  typedef typename AlgEqnSet_Local0::FieldBundle_Local FieldBundle_Local0;

  typedef typename SolutionClass::FieldBundleType1 FieldBundle1;
  typedef typename AlgEqnSet_Global1::ErrorEstimateClass ErrorEstimateClass1;
  typedef typename AlgEqnSet_Local1::FieldBundle_Local FieldBundle_Local1;

  typedef SolverInterface_DGBR2_Block2x2 SolverInterfaceClass;

  typedef typename OutputIntegrandType::template ArrayJ<Real> ArrayJ;

  typedef typename AlgEqnSet_Global0::ArrayQ ArrayQ0;
  typedef typename AlgEqnSet_Global1::ArrayQ ArrayQ1;

  template< class... BCArgs >
  SolverInterface_DGBR2_Block2x2(SolutionClass& sol,
                                 const ResidualNormType& resNormType0, const ResidualNormType& resNormType1,
                                 std::vector<Real>& tol0, std::vector<Real>& tol1, const int quadOrder,
                                 const std::vector<int>& cellgroups, const std::vector<int>& interiortracegroups,
                                 PyDict& BCList0, PyDict& BCList1,
                                 const std::map< std::string, std::vector<int> >& BCBoundaryGroups0,
                                 const std::map< std::string, std::vector<int> >& BCBoundaryGroups1,
                                 const PyDict& NonlinearSolverDict,
                                 const OutputIntegrandType& fcnOutput, BCArgs&... args )
    : sol_(sol),
      xfld_(sol_.xfld),
      resNormType0_(resNormType0), resNormType1_(resNormType1),
      tol0_(tol0), tol1_(tol1),
      quadOrder_(quadOrder),
      cellgroup_list_(cellgroups),
      interiortracegroup_list_(interiortracegroups),
      BCList0_(BCList0), BCList1_(BCList1),
      BCBoundaryGroups0_(BCBoundaryGroups0), BCBoundaryGroups1_(BCBoundaryGroups1),
      NonlinearSolverDict_(NonlinearSolverDict),

      AlgEqnSet0_(sol_.paramfld0, sol_.primal0, {},
                  sol_.pde0, sol_.disc0, QuadratureOrder(xfld_, quadOrder_), resNormType0_, tol0_,
                  cellgroup_list_, interiortracegroup_list_, BCList0_, BCBoundaryGroups0_, args...),

      AlgEqnSet1_(sol_.paramfld1, sol_.primal1, {},
                  sol_.pde1, sol_.disc1, QuadratureOrder(xfld_, quadOrder_), resNormType1_, tol1_,
                  cellgroup_list_, interiortracegroup_list_, BCList1_, BCBoundaryGroups1_, args...),

      fcnOutput_(fcnOutput),
      estimatesEvaluated_(false)
  {

    errorIndicator0_ = 0.0, errorIndicator1_ = 0.0, errorIndicatorSum_ = 0.0;
    errorEstimate0_ = 0.0, errorEstimate1_ = 0.0, errorEstimateSum_ = 0.0;

    //Loop over each cellgroup and find the no. of DOF per cell and solution order for that cellgroup
    nDOFperCell_.resize((int) xfld_.nCellGroups(), -1);
    orderVec_.resize((int) xfld_.nCellGroups(), -1);
    for_each_CellGroup<TopoDim>::apply( CellwiseOperations(cellgroup_list_, *this), xfld_);
  }

  virtual ~SolverInterface_DGBR2_Block2x2() {}

  virtual void solveGlobalPrimalProblem() override;
  virtual void solveGlobalAdjointProblem() override;

  void solveGlobalPrimalProblemIsolated(const int pde_index);

  virtual void computeErrorEstimates() override;
  virtual Real getElementalErrorEstimate(int cellgroup, int elem) const override;
  virtual Real getGlobalErrorIndicator() const override;
  virtual Real getGlobalErrorEstimate() const override;

  //Block2x2 specific interfaces for returning error estimates separately for each PDE
  std::array<Real,2> getElementalErrorEstimates(int cellgroup, int elem) const;
  std::array<Real,2> getGlobalErrorIndicators() const;
  std::array<Real,2> getGlobalErrorEstimates() const;

  virtual void output_EField(const std::string& filename) const override;

  virtual LocalSolveStatus solveLocalProblem(const XField_Local_Base<PhysDim, TopoDim>& local_xfld, std::vector<Real>& local_error) const override;

  virtual Real getnDOFperCell(const int cellgroup) const override;
  virtual int getSolutionOrder(const int cellgroup) const override;

  virtual Real getOutput() const override;

  SolverInterface_impl<SolverInterfaceClass>
  CellwiseOperations(const std::vector<int>& cellgroup_list, SolverInterfaceClass& base );

  template<class Topology>
  void getSolutionInfo_cellgroup(const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
                                 const int cellGroupGlobal);

  virtual int getCost() const override { return sol_.primal0.qfld.nDOF() +sol_.primal1.qfld.nDOF(); }
protected:

  template<class Temporal>
  typename std::enable_if< std::is_same<Temporal,TemporalMarch>::value, void >::type
  solveGlobalPrimalContinuation();

  template<class Temporal>
  typename std::enable_if< std::is_same<Temporal,TemporalSpaceTime>::value, void >::type
  solveGlobalPrimalContinuation();

  SolutionClass& sol_; //class containing all the solution data
  const XFieldType& xfld_; // mesh
  const ResidualNormType resNormType0_;
  const ResidualNormType resNormType1_;
  std::vector<Real>& tol0_;
  std::vector<Real>& tol1_;
  const int quadOrder_;
  const std::vector<int>& cellgroup_list_;
  const std::vector<int>& interiortracegroup_list_;
  PyDict& BCList0_;
  PyDict& BCList1_;
  const std::map< std::string, std::vector<int> >& BCBoundaryGroups0_, BCBoundaryGroups1_;
  const PyDict& NonlinearSolverDict_;

  AlgEqnSet_Global0 AlgEqnSet0_; // AlgebraicEquationSet for PDE0
  AlgEqnSet_Global1 AlgEqnSet1_; // AlgebraicEquationSet for PDE1


  // pointers to the estimate
  std::shared_ptr<ErrorEstimateClass0> pErrorEstimate0_;
  std::shared_ptr<ErrorEstimateClass1> pErrorEstimate1_;

  std::vector<std::vector<Real>> errorArray0_; //Array of elemental error estimates from PDE 0 (index:[cellgroup][elem])
  std::vector<std::vector<Real>> errorArray1_; //Array of elemental error estimates from PDE 1(index:[cellgroup][elem])
  std::vector<std::vector<Real>> errorArraySum_; //Array of elemental error estimates (total) (index:[cellgroup][elem])
  const OutputIntegrandType& fcnOutput_;
  Real errorIndicator0_, errorIndicator1_, errorIndicatorSum_;
  Real errorEstimate0_, errorEstimate1_, errorEstimateSum_;
  ArrayJ outputJ_; //output functional evaluated at current solution

  std::vector<Real> nDOFperCell_; //indexing: [global cellgrp] - number of degrees of freedom per cell
  std::vector<int> orderVec_; //indexing: [global cellgrp] - solution order of each cellgroup

  bool estimatesEvaluated_;
};

template<class PhysDim, class TopoDim,
         class NDPDEClass0, template<class,class> class BCNDConvert0, class BCVector0,
         class NDPDEClass1, template<class,class> class BCNDConvert1, class BCVector1,
         class ParamBuilderType, class SolutionClass,
         class AlgebraicEqSet0, class AlgebraicEqSet1,
         class OutputIntegrandType>
void
SolverInterface_DGBR2_Block2x2<PhysDim, TopoDim,
                               NDPDEClass0, BCNDConvert0, BCVector0,
                               NDPDEClass1, BCNDConvert1, BCVector1,
                               ParamBuilderType, SolutionClass,
                               AlgebraicEqSet0, AlgebraicEqSet1,
                               OutputIntegrandType>::
solveGlobalPrimalProblem()
{
  timer clock;

  solveGlobalPrimalContinuation<typename NDPDEClass0::Temporal>();

  std::cout<<"Primal solve time: " << std::fixed << std::setprecision(3) << clock.elapsed() << "s" << std::endl;

  // evaluate output function
  outputJ_ = 0.0;

  QuadratureOrder quadrule(xfld_, quadOrder_);

#ifndef BOUNDARYOUTPUT
  IntegrateCellGroups<TopoDim>::integrate( FunctionalCell_DGBR2( fcnOutput_, outputJ_ ),
                                           xfld_, (sol_.primal0.qfld, sol_.primal0.rfld), quadrule.cellOrders.data(), quadrule.cellOrders.size() );
#else
  AlgEqnSet_Primal_.dispatchBC().dispatch_DGBR2(
    FunctionalBoundaryTrace_Dispatch_DGBR2(fcnOutput_, xfld_, sol_.primal0.qfld, sol_.primal0.rfld,
                                           quadrule.boundaryTraceOrders.data(), quadrule.boundaryTraceOrders.size(), outputJ_ ) );
#endif
}

template< class PhysDim, class TopoDim,
          class NDPDEClass0, template<class,class> class BCNDConvert0, class BCVector0,
          class NDPDEClass1, template<class,class> class BCNDConvert1, class BCVector1,
          class ParamBuilderType, class SolutionClass,
          class AlgebraicEqSet0, class AlgebraicEqSet1,
          class OutputIntegrandType>
template<class Temporal>
typename std::enable_if< std::is_same<Temporal,TemporalMarch>::value, void >::type
SolverInterface_DGBR2_Block2x2<PhysDim, TopoDim,
                               NDPDEClass0, BCNDConvert0, BCVector0,
                               NDPDEClass1, BCNDConvert1, BCVector1,
                               ParamBuilderType, SolutionClass,
                               AlgebraicEqSet0, AlgebraicEqSet1,
                               OutputIntegrandType>::
solveGlobalPrimalContinuation()
{
  typedef typename AlgEqnSet_Global0::SystemMatrix SystemMatrixClass00;
  typedef typename AlgEqnSet_Global1::SystemMatrix SystemMatrixClass11;

  typedef SolverContinuationParams<typename NDPDEClass0::Temporal> ContinuationParams;

  // Get the requested continuation
  DictKeyPair continuation = NonlinearSolverDict_.get(ContinuationParams::params.Continuation);

  // Generate off diagonal AlgEqnSets
  typedef PDEMatrixParam0 MatrixQ01;
  typedef PDEMatrixParam1 MatrixQ10;

  typedef SLA::SparseMatrix_CRS<MatrixQ01> MatrixClass01;
  typedef SLA::SparseMatrix_CRS<MatrixQ10> MatrixClass10;

  typedef DLA::MatrixD<MatrixClass01> SystemMatrixClass01;
  typedef DLA::MatrixD<MatrixClass10> SystemMatrixClass10;

  typedef JacobianParam<boost::mpl::vector1_c<int,1>,AlgEqnSet_Global0> JacobianParam_PDE0;
  typedef JacobianParam<boost::mpl::vector1_c<int,1>,AlgEqnSet_Global1> JacobianParam_PDE1;

  //HACK:: Only works when parameter 1 is the unknown in the otrher PDE
  std::map<int,int> PDEMap0;
  PDEMap0[1] = AlgEqnSet1_.iq;

  std::map<int,int> PDEMap1;
  PDEMap1[1] = AlgEqnSet0_.iq;

  JacobianParam_PDE0 JP01(AlgEqnSet0_, PDEMap0);
  JacobianParam_PDE1 JP10(AlgEqnSet1_, PDEMap1);

  // Generate 2x2 Block system
  typedef AlgebraicEquationSet_Block2x2<SystemMatrixClass00, SystemMatrixClass01,
                                        SystemMatrixClass10, SystemMatrixClass11> AlgEqSetType_2x2;

  AlgEqSetType_2x2 BlockAES(AlgEqnSet0_, JP01,
                            JP10, AlgEqnSet1_);

  typedef typename AlgEqSetType_2x2::SystemMatrix BlockMatrixClass;
  typedef typename AlgEqSetType_2x2::SystemVector BlockVectorClass;

  if (continuation == ContinuationParams::params.Continuation.None)
  {
    // Create the nonlinear solver object
    NonLinearSolver<BlockMatrixClass> nonlinear_solver( BlockAES, continuation );

    // set initial condition from current solution in solution fields
    BlockVectorClass sln_initial(BlockAES.vectorStateSize());
    BlockAES.fillSystemVector(sln_initial);

    // nonlinear solve
    BlockVectorClass sln(BlockAES.vectorStateSize());
    SolveStatus status = nonlinear_solver.solve(sln_initial, sln);

    if (status.converged == false)
    {
      output_Tecplot( sol_.primal0.qfld, "tmp/qfld0_unconverged.plt" );
      output_Tecplot( sol_.primal1.qfld, "tmp/qfld1_unconverged.plt" );
//      SANS_DEVELOPER_EXCEPTION("Nonlinear solver failed to converge.");
    }
  }
  else if (continuation == ContinuationParams::params.Continuation.PseudoTime)
  {
    //solve with pseudo-time continuation
#if 1
    SANS_DEVELOPER_EXCEPTION("SolverInterface_DGBR2_Block2x2::solveGlobalPrimalProblem - Pseudo-time continuation not working yet.");
#else
    HField_DG<PhysDim,TopoDim> hfld(xfld_);

    // Create the pseudo time continuation class
    PseudoTime<NDPDEClass, ParamFieldType> PTC(continuation, sol_.paramfld,
                                               sol_.primal.qfld, hfld, sol_.pde,
                                               cellgroup_list_, AlgEqnSet_);

    if (!PTC.iterate(100))
    {
      output_Tecplot( sol_.primal.qfld, "tmp/qfld_unconverged.plt" );
      SANS_DEVELOPER_EXCEPTION("Nonlinear solver with PTC failed to converge.");
    }
#endif
  }
  else
    SANS_DEVELOPER_EXCEPTION("SolverInterface_DGBR2_Block2x2::solveGlobalPrimalProblem - Unknown nonlinear solver continuation type.");

}

template< class PhysDim, class TopoDim,
          class NDPDEClass0, template<class,class> class BCNDConvert0, class BCVector0,
          class NDPDEClass1, template<class,class> class BCNDConvert1, class BCVector1,
          class ParamBuilderType, class SolutionClass,
          class AlgebraicEqSet0, class AlgebraicEqSet1,
          class OutputIntegrandType>
template<class Temporal>
typename std::enable_if< std::is_same<Temporal,TemporalSpaceTime>::value, void >::type
SolverInterface_DGBR2_Block2x2<PhysDim, TopoDim,
                               NDPDEClass0, BCNDConvert0, BCVector0,
                               NDPDEClass1, BCNDConvert1, BCVector1,
                               ParamBuilderType, SolutionClass,
                               AlgebraicEqSet0, AlgebraicEqSet1,
                               OutputIntegrandType>::
solveGlobalPrimalContinuation()
{
  typedef typename AlgEqnSet_Global0::SystemMatrix SystemMatrixClass00;
  typedef typename AlgEqnSet_Global1::SystemMatrix SystemMatrixClass11;

  typedef SolverContinuationParams<typename NDPDEClass0::Temporal> ContinuationParams;

  // Get the requested continuation
  DictKeyPair continuation = NonlinearSolverDict_.get(ContinuationParams::params.Continuation);

  // Generate off diagonal AlgEqnSets
  typedef PDEMatrixParam0 MatrixQ01;
  typedef PDEMatrixParam1 MatrixQ10;

  typedef SLA::SparseMatrix_CRS<MatrixQ01> MatrixClass01;
  typedef SLA::SparseMatrix_CRS<MatrixQ10> MatrixClass10;

  typedef DLA::MatrixD<MatrixClass01> SystemMatrixClass01;
  typedef DLA::MatrixD<MatrixClass10> SystemMatrixClass10;

  typedef JacobianParam<boost::mpl::vector1_c<int,1>,AlgEqnSet_Global0> JacobianParam_PDE0;
  typedef JacobianParam<boost::mpl::vector1_c<int,1>,AlgEqnSet_Global1> JacobianParam_PDE1;

  //HACK:: Only works when parameter 1 is the unknown in the otrher PDE
  std::map<int,int> PDEMap0;
  PDEMap0[1] = AlgEqnSet1_.iq;

  std::map<int,int> PDEMap1;
  PDEMap1[1] = AlgEqnSet0_.iq;

  JacobianParam_PDE0 JP01(AlgEqnSet0_, PDEMap0);
  JacobianParam_PDE1 JP10(AlgEqnSet1_, PDEMap1);

  // Generate 2x2 Block system
  typedef AlgebraicEquationSet_Block2x2<SystemMatrixClass00, SystemMatrixClass01,
                                        SystemMatrixClass10, SystemMatrixClass11> AlgEqSetType_2x2;

  AlgEqSetType_2x2 BlockAES(AlgEqnSet0_, JP01,
                            JP10, AlgEqnSet1_);

  typedef typename AlgEqSetType_2x2::SystemMatrix BlockMatrixClass;
  typedef typename AlgEqSetType_2x2::SystemVector BlockVectorClass;

  if (continuation == ContinuationParams::params.Continuation.None)
  {
    // Create the nonlinear solver object
    NonLinearSolver<BlockMatrixClass> nonlinear_solver( BlockAES, continuation );

    // set initial condition from current solution in solution fields
    BlockVectorClass sln_initial(BlockAES.vectorStateSize());
    BlockAES.fillSystemVector(sln_initial);

    // nonlinear solve
    BlockVectorClass sln(BlockAES.vectorStateSize());
    SolveStatus status = nonlinear_solver.solve(sln_initial, sln);

    if (status.converged == false)
    {
      output_Tecplot( sol_.primal0.qfld, "tmp/qfld0_unconverged.plt" );
      output_Tecplot( sol_.primal1.qfld, "tmp/qfld1_unconverged.plt" );
      SANS_DEVELOPER_EXCEPTION("Nonlinear solver failed to converge.");
    }
  }
  else
    SANS_DEVELOPER_EXCEPTION("SolverInterface_DGBR2_Block2x2::solveGlobalPrimalProblem - Unknown nonlinear solver continuation type.");
}

template<class PhysDim, class TopoDim,
         class NDPDEClass0, template<class,class> class BCNDConvert0, class BCVector0,
         class NDPDEClass1, template<class,class> class BCNDConvert1, class BCVector1,
         class ParamBuilderType, class SolutionClass,
         class AlgebraicEqSet0, class AlgebraicEqSet1,
         class OutputIntegrandType>
void
SolverInterface_DGBR2_Block2x2<PhysDim, TopoDim,
                               NDPDEClass0, BCNDConvert0, BCVector0,
                               NDPDEClass1, BCNDConvert1, BCVector1,
                               ParamBuilderType, SolutionClass,
                               AlgebraicEqSet0, AlgebraicEqSet1,
                               OutputIntegrandType>::
solveGlobalPrimalProblemIsolated(const int pde_index)
{
  typedef typename AlgEqnSet_Global0::SystemVector SystemVectorClass0;
  typedef typename AlgEqnSet_Global0::SystemMatrix SystemMatrixClass00;

  typedef typename AlgEqnSet_Global1::SystemVector SystemVectorClass1;
  typedef typename AlgEqnSet_Global1::SystemMatrix SystemMatrixClass11;

  typedef SolverContinuationParams<typename NDPDEClass0::Temporal> ContinuationParams;

  //Get the requested continuation
  DictKeyPair continuation = NonlinearSolverDict_.get(ContinuationParams::params.Continuation);

  if (pde_index == 0)
  {
    //Solve PDE0 independently, to initialize primal0 to be consistent with primal1.
    NonLinearSolver<SystemMatrixClass00> solver_pde0( AlgEqnSet0_, continuation );

    SystemVectorClass0 sln0_initial(AlgEqnSet0_.vectorStateSize());
    AlgEqnSet0_.fillSystemVector(sln0_initial);

    SystemVectorClass0 sln0(AlgEqnSet0_.vectorStateSize());
    SolveStatus status0 = solver_pde0.solve(sln0_initial, sln0);
    SANS_ASSERT_MSG( status0.converged, "Initialization of primal0 failed." );
  }
  else if (pde_index == 1)
  {
    //Solve PDE1 independently, to initialize primal1 to be consistent with primal0.
    NonLinearSolver<SystemMatrixClass11> solver_pde1( AlgEqnSet1_, continuation );

    SystemVectorClass1 sln1_initial(AlgEqnSet1_.vectorStateSize());
    AlgEqnSet1_.fillSystemVector(sln1_initial);

    SystemVectorClass1 sln1(AlgEqnSet1_.vectorStateSize());
    SolveStatus status1 = solver_pde1.solve(sln1_initial, sln1);
    SANS_ASSERT_MSG( status1.converged, "Initialization of primal1 failed." );
  }
  else
    SANS_DEVELOPER_EXCEPTION("SolverInterface_DGBR2_Block2x2::solveGlobalPrimalProblemIsolated - pde_index should be 0 or 1.");
}

template<class PhysDim, class TopoDim,
         class NDPDEClass0, template<class,class> class BCNDConvert0, class BCVector0,
         class NDPDEClass1, template<class,class> class BCNDConvert1, class BCVector1,
         class ParamBuilderType, class SolutionClass,
         class AlgebraicEqSet0, class AlgebraicEqSet1,
         class OutputIntegrandType>
void
SolverInterface_DGBR2_Block2x2<PhysDim, TopoDim,
                               NDPDEClass0, BCNDConvert0, BCVector0,
                               NDPDEClass1, BCNDConvert1, BCVector1,
                               ParamBuilderType, SolutionClass,
                               AlgebraicEqSet0, AlgebraicEqSet1,
                               OutputIntegrandType>::
solveGlobalAdjointProblem()
{
  typedef typename AlgEqnSet_Global0::SystemMatrix SystemMatrixClass00;
  typedef typename AlgEqnSet_Global1::SystemMatrix SystemMatrixClass11;

  typedef SurrealS<NDPDEClass0::N> SurrealClass;

  timer clock;

  //Create primal solutions in richer solution space
//  typedef typename SolutionClass::FieldBundleType0 FieldBundleType0;
//  typedef typename SolutionClass::FieldBundleType1 FieldBundleType1;

  FieldBundle0 primal_pro0(xfld_, sol_.adjoint0.order,
                           sol_.primal0.basis_cell, sol_.primal0.basis_trace, sol_.mitlg_boundarygroups0);

  FieldBundle1 primal_pro1(xfld_, sol_.adjoint1.order,
                           sol_.primal1.basis_cell, sol_.primal1.basis_trace, sol_.mitlg_boundarygroups1);

  // Prolongate the primal solution to the richer solution space
  sol_.primal0.projectTo(primal_pro0);
  sol_.primal1.projectTo(primal_pro1);

  // Create param fields with prolongated primal solution
  typedef ParamFieldBuilder_Block2<ParamBuilderType, PhysDim, TopoDim,
                                   typename FieldBundle0::QFieldType,
                                   typename FieldBundle1::QFieldType> ParamFieldBuilder;

  ParamFieldBuilder parambuilder(xfld_, primal_pro0.qfld, primal_pro1.qfld);

  QuadratureOrder quadrule(xfld_, quadOrder_);

  //Create AlgebraicEquationSets for solutions in richer space
  AlgEqnSet_Global0 AlgEqnSet_Pro0(parambuilder.fld0, primal_pro0, {},
                                   sol_.pde0, sol_.disc0, quadrule, resNormType0_, tol0_,
                                   cellgroup_list_, interiortracegroup_list_, BCList0_, BCBoundaryGroups0_);

  AlgEqnSet_Global1 AlgEqnSet_Pro1(parambuilder.fld1, primal_pro1, {},
                                   sol_.pde1, sol_.disc1, quadrule, resNormType1_, tol1_,
                                   cellgroup_list_, interiortracegroup_list_, BCList1_, BCBoundaryGroups1_);

  // Generate off diagonal AlgEqnSets
  typedef PDEMatrixParam0 MatrixQ01;
  typedef PDEMatrixParam1 MatrixQ10;

  typedef SLA::SparseMatrix_CRS<MatrixQ01> MatrixClass01;
  typedef SLA::SparseMatrix_CRS<MatrixQ10> MatrixClass10;

  typedef DLA::MatrixD<MatrixClass01> SystemMatrixClass01;
  typedef DLA::MatrixD<MatrixClass10> SystemMatrixClass10;

  typedef JacobianParam<boost::mpl::vector1_c<int,1>,AlgEqnSet_Global0> JacobianParam_PDE0;
  typedef JacobianParam<boost::mpl::vector1_c<int,1>,AlgEqnSet_Global1> JacobianParam_PDE1;

  //HACK:: Only works when parameter 1 is the unknown in the otrher PDE
  std::map<int,int> PDEMap0;
  PDEMap0[1] = AlgEqnSet1_.iq;

  std::map<int,int> PDEMap1;
  PDEMap1[1] = AlgEqnSet0_.iq;

  JacobianParam_PDE0 JP01(AlgEqnSet_Pro0, PDEMap0);
  JacobianParam_PDE1 JP10(AlgEqnSet_Pro1, PDEMap1);

  // Generate 2x2 Block system
  typedef AlgebraicEquationSet_Block2x2<SystemMatrixClass00, SystemMatrixClass01,
                                        SystemMatrixClass10, SystemMatrixClass11> AlgEqSetType_2x2;

  AlgEqSetType_2x2 BlockAES_Pro(AlgEqnSet_Pro0, JP01,
                                JP10, AlgEqnSet_Pro1);

  typedef typename AlgEqSetType_2x2::SystemMatrix BlockMatrixClass;
  typedef typename AlgEqSetType_2x2::SystemVector BlockVectorClass;

  // Functional integral
  BlockVectorClass rhs(BlockAES_Pro.vectorEqSize());
  rhs = 0;

  const int iPDE = AlgEqnSet_Pro0.iPDE;

#ifndef BOUNDARYOUTPUT
  IntegrateCellGroups<TopoDim>::integrate( JacobianFunctionalCell_DGBR2<SurrealClass>( fcnOutput_, rhs.v0(iPDE) ),
                                           xfld_, (primal_pro0.qfld, primal_pro0.rfld), quadrule.cellOrders.data(), quadrule.cellOrders.size() );
#else
  AlgEqnSet_Pro0.dispatchBC().dispatch_DGBR2(
      JacobianFunctionalBoundaryTrace_Dispatch_DGBR2<SurrealClass>(fcnOutput_, xfld_, primal_pro0.qfld, primal_pro0.rfld,
                                                                   quadrule.boundaryTraceOrders.data(), quadrule.boundaryTraceOrders.size(),
                                                                   rhs(0)(iPDE) ) );
#endif

  // adjoint solve
  SLA::UMFPACK<BlockMatrixClass> solver(BlockAES_Pro, SLA::TransposeSolve);

  BlockVectorClass adj(BlockAES_Pro.vectorEqSize());
  adj = 0; // set initial guess

  solver.solve(rhs, adj);

  // update solution
  AlgEqnSet_Pro0.setAdjointField(adj.v0, sol_.adjoint0.qfld, sol_.adjoint0.rfld, sol_.adjoint0.lgfld);
  AlgEqnSet_Pro1.setAdjointField(adj.v1, sol_.adjoint1.qfld, sol_.adjoint1.rfld, sol_.adjoint1.lgfld);

  std::cout<<"Adjoint solve time: " << std::fixed << std::setprecision(3) << clock.elapsed() << "s" << std::endl;
}


template<class PhysDim, class TopoDim,
         class NDPDEClass0, template<class,class> class BCNDConvert0, class BCVector0,
         class NDPDEClass1, template<class,class> class BCNDConvert1, class BCVector1,
         class ParamBuilderType, class SolutionClass,
         class AlgebraicEqSet0, class AlgebraicEqSet1,
         class OutputIntegrandType>
void
SolverInterface_DGBR2_Block2x2<PhysDim, TopoDim,
                               NDPDEClass0, BCNDConvert0, BCVector0,
                               NDPDEClass1, BCNDConvert1, BCVector1,
                               ParamBuilderType, SolutionClass,
                               AlgebraicEqSet0, AlgebraicEqSet1,
                               OutputIntegrandType>::
computeErrorEstimates()
{
  timer clock;

  QuadratureOrder quadrule(xfld_, quadOrder_);

  // nullptr is needed because lifted quantity field is not supported right now.
  // TODO: Add the pLiftedQuantityfld to Block 2x2
  ErrorEstimateClass0 ErrorEstimate0(sol_.paramfld0, sol_.primal0, sol_.adjoint0, nullptr,
                                     sol_.pde0, sol_.disc0, quadrule, cellgroup_list_, interiortracegroup_list_,
                                     BCList0_, BCBoundaryGroups0_);

  ErrorEstimateClass1 ErrorEstimate1(sol_.paramfld1, sol_.primal1, sol_.adjoint1, nullptr,
                                     sol_.pde1, sol_.disc1, quadrule, cellgroup_list_, interiortracegroup_list_,
                                     BCList1_, BCBoundaryGroups1_);


  pErrorEstimate0_ = std::make_shared<ErrorEstimateClass0>(sol_.paramfld0, sol_.primal0, sol_.adjoint0, nullptr,
                                                           sol_.pde0, sol_.disc0, quadrule, cellgroup_list_, interiortracegroup_list_,
                                                           BCList0_, BCBoundaryGroups0_);
  pErrorEstimate1_ = std::make_shared<ErrorEstimateClass1>(sol_.paramfld1, sol_.primal1, sol_.adjoint1, nullptr,
                                                           sol_.pde1, sol_.disc1, quadrule, cellgroup_list_, interiortracegroup_list_,
                                                           BCList1_, BCBoundaryGroups1_);

  pErrorEstimate0_->fillEArray(errorArray0_);
  pErrorEstimate1_->fillEArray(errorArray1_);

  //Add the error estimates of pde0 and pde1
  SANS_ASSERT(errorArray0_.size() == errorArray1_.size());
  errorArraySum_.resize(errorArray0_.size());

  for (std::size_t i = 0; i < errorArray0_.size(); i++)
  {
    SANS_ASSERT(errorArray0_[i].size() == errorArray1_[i].size());
    errorArraySum_[i].resize(errorArray0_[i].size());

    for (std::size_t j = 0; j < errorArray0_[i].size(); j++)
      errorArraySum_[i][j] = errorArray0_[i][j] + errorArray1_[i][j];
  }

  errorIndicator0_ = 0.0, errorIndicator1_ = 0.0;
  pErrorEstimate0_->getErrorIndicator(errorIndicator0_);
  pErrorEstimate1_->getErrorIndicator(errorIndicator1_);
  errorIndicatorSum_ = fabs(errorIndicator0_) + fabs(errorIndicator1_);

  errorEstimate0_ = 0.0, errorEstimate1_ = 0.0;
  pErrorEstimate0_->getErrorEstimate(errorEstimate0_);
  pErrorEstimate1_->getErrorEstimate(errorEstimate1_);
  errorEstimateSum_ = fabs(errorEstimate0_ + errorEstimate1_);

  std::cout<<"Error estimation time: " << std::fixed << std::setprecision(3) << clock.elapsed() << "s" << std::endl;
}

template<class PhysDim, class TopoDim,
         class NDPDEClass0, template<class,class> class BCNDConvert0, class BCVector0,
         class NDPDEClass1, template<class,class> class BCNDConvert1, class BCVector1,
         class ParamBuilderType, class SolutionClass,
         class AlgebraicEqSet0, class AlgebraicEqSet1,
         class OutputIntegrandType>
void
SolverInterface_DGBR2_Block2x2<PhysDim, TopoDim,
                               NDPDEClass0, BCNDConvert0, BCVector0,
                               NDPDEClass1, BCNDConvert1, BCVector1,
                               ParamBuilderType, SolutionClass,
                               AlgebraicEqSet0, AlgebraicEqSet1,
                               OutputIntegrandType>::
output_EField(const std::string& filename) const
{
  // would be nice, but you need the member variable const at the MeshAdapt level -- Hugh
//  if (!estimatesEvaluated_)
//    computeErrorEstimates();

  pErrorEstimate0_->outputFields(filename);
  // TODO: This should dump both of the fields somehow
}

template<class PhysDim, class TopoDim,
         class NDPDEClass0, template<class,class> class BCNDConvert0, class BCVector0,
         class NDPDEClass1, template<class,class> class BCNDConvert1, class BCVector1,
         class ParamBuilderType, class SolutionClass,
         class AlgebraicEqSet0, class AlgebraicEqSet1,
         class OutputIntegrandType>
Real
SolverInterface_DGBR2_Block2x2<PhysDim, TopoDim,
                               NDPDEClass0, BCNDConvert0, BCVector0,
                               NDPDEClass1, BCNDConvert1, BCVector1,
                               ParamBuilderType, SolutionClass,
                               AlgebraicEqSet0, AlgebraicEqSet1,
                               OutputIntegrandType>::
getElementalErrorEstimate(int cellgroup, int elem) const
{
  return errorArraySum_[cellgroup][elem];
}

template<class PhysDim, class TopoDim,
         class NDPDEClass0, template<class,class> class BCNDConvert0, class BCVector0,
         class NDPDEClass1, template<class,class> class BCNDConvert1, class BCVector1,
         class ParamBuilderType, class SolutionClass,
         class AlgebraicEqSet0, class AlgebraicEqSet1,
         class OutputIntegrandType>
std::array<Real,2>
SolverInterface_DGBR2_Block2x2<PhysDim, TopoDim,
                               NDPDEClass0, BCNDConvert0, BCVector0,
                               NDPDEClass1, BCNDConvert1, BCVector1,
                               ParamBuilderType, SolutionClass,
                               AlgebraicEqSet0, AlgebraicEqSet1,
                               OutputIntegrandType>::
getElementalErrorEstimates(int cellgroup, int elem) const
{
  return {{errorArray0_[cellgroup][elem], errorArray1_[cellgroup][elem]}};
}

template<class PhysDim, class TopoDim,
         class NDPDEClass0, template<class,class> class BCNDConvert0, class BCVector0,
         class NDPDEClass1, template<class,class> class BCNDConvert1, class BCVector1,
         class ParamBuilderType, class SolutionClass,
         class AlgebraicEqSet0, class AlgebraicEqSet1,
         class OutputIntegrandType>
Real
SolverInterface_DGBR2_Block2x2<PhysDim, TopoDim,
                               NDPDEClass0, BCNDConvert0, BCVector0,
                               NDPDEClass1, BCNDConvert1, BCVector1,
                               ParamBuilderType, SolutionClass,
                               AlgebraicEqSet0, AlgebraicEqSet1,
                               OutputIntegrandType>::
getGlobalErrorEstimate() const
{
  return errorEstimateSum_;
}

template<class PhysDim, class TopoDim,
         class NDPDEClass0, template<class,class> class BCNDConvert0, class BCVector0,
         class NDPDEClass1, template<class,class> class BCNDConvert1, class BCVector1,
         class ParamBuilderType, class SolutionClass,
         class AlgebraicEqSet0, class AlgebraicEqSet1,
         class OutputIntegrandType>
std::array<Real,2>
SolverInterface_DGBR2_Block2x2<PhysDim, TopoDim,
                               NDPDEClass0, BCNDConvert0, BCVector0,
                               NDPDEClass1, BCNDConvert1, BCVector1,
                               ParamBuilderType, SolutionClass,
                               AlgebraicEqSet0, AlgebraicEqSet1,
                               OutputIntegrandType>::
getGlobalErrorEstimates() const
{
  return {{errorEstimate0_, errorEstimate1_}};
}

template<class PhysDim, class TopoDim,
         class NDPDEClass0, template<class,class> class BCNDConvert0, class BCVector0,
         class NDPDEClass1, template<class,class> class BCNDConvert1, class BCVector1,
         class ParamBuilderType, class SolutionClass,
         class AlgebraicEqSet0, class AlgebraicEqSet1,
         class OutputIntegrandType>
Real
SolverInterface_DGBR2_Block2x2<PhysDim, TopoDim,
                               NDPDEClass0, BCNDConvert0, BCVector0,
                               NDPDEClass1, BCNDConvert1, BCVector1,
                               ParamBuilderType, SolutionClass,
                               AlgebraicEqSet0, AlgebraicEqSet1,
                               OutputIntegrandType>::
getGlobalErrorIndicator() const
{
  return errorIndicatorSum_;
}

template<class PhysDim, class TopoDim,
         class NDPDEClass0, template<class,class> class BCNDConvert0, class BCVector0,
         class NDPDEClass1, template<class,class> class BCNDConvert1, class BCVector1,
         class ParamBuilderType, class SolutionClass,
         class AlgebraicEqSet0, class AlgebraicEqSet1,
         class OutputIntegrandType>
std::array<Real,2>
SolverInterface_DGBR2_Block2x2<PhysDim, TopoDim,
                               NDPDEClass0, BCNDConvert0, BCVector0,
                               NDPDEClass1, BCNDConvert1, BCVector1,
                               ParamBuilderType, SolutionClass,
                               AlgebraicEqSet0, AlgebraicEqSet1,
                               OutputIntegrandType>::
getGlobalErrorIndicators() const
{
  return {{errorIndicator0_, errorIndicator1_}};
}

template<class PhysDim, class TopoDim,
         class NDPDEClass0, template<class,class> class BCNDConvert0, class BCVector0,
         class NDPDEClass1, template<class,class> class BCNDConvert1, class BCVector1,
         class ParamBuilderType, class SolutionClass,
         class AlgebraicEqSet0, class AlgebraicEqSet1,
         class OutputIntegrandType>
LocalSolveStatus
SolverInterface_DGBR2_Block2x2<PhysDim, TopoDim,
                               NDPDEClass0, BCNDConvert0, BCVector0,
                               NDPDEClass1, BCNDConvert1, BCVector1,
                               ParamBuilderType, SolutionClass,
                               AlgebraicEqSet0, AlgebraicEqSet1,
                               OutputIntegrandType>::
solveLocalProblem(const XField_Local_Base<PhysDim, TopoDim>& xfld_local, std::vector<Real>& local_error) const
{
  typedef typename AlgEqnSet_Local0::SystemMatrix SystemMatrixClass00;
  typedef typename AlgEqnSet_Local1::SystemMatrix SystemMatrixClass11;

//  typedef ErrorEstimate_DGBR2<NDPDEClass0, BCNDConvert0, BCVector0, typename SolutionClass::ParamFieldType0> ErrorEstimateClass0;
//  typedef ErrorEstimate_DGBR2<NDPDEClass1, BCNDConvert1, BCVector1, typename SolutionClass::ParamFieldType1> ErrorEstimateClass1;

  //Find the mapping between the boundary conditions of local problem and the global problem
  PyDict BCList_local0;
  PyDict BCList_local1;
  std::map<std::string, std::vector<int>> BCBoundaryGroups_local0, BCBoundaryGroups_local1;

  std::vector<int> boundaryTraceGroups = xfld_local.getReSolveBoundaryTraceGroups();

  for (std::size_t local_group = 0; local_group < boundaryTraceGroups.size(); local_group++)
  {
    //Get the global boundary-trace group index for the current local boundary-trace group
    int globalBTraceGroup = xfld_local.getGlobalBoundaryTraceMap({local_group,0}).first;

    SANS_ASSERT(globalBTraceGroup >= 0); //Check for non-negative index

    for (std::map<std::string,std::vector<int>>::const_iterator it = BCBoundaryGroups0_.begin(); it != BCBoundaryGroups0_.end(); ++it)
    {
      const std::vector<int>& grouplist = it->second;
      bool found = false;

      for (int j = 0; j < (int) grouplist.size(); j++)
      {
        if (globalBTraceGroup == grouplist[j])
        {
          //Found the BC associated with the globalBTraceGroup we want, so add it to the local BTraceGroup map
          BCBoundaryGroups_local0[it->first].push_back(local_group);
          BCList_local0[it->first] = BCList0_[it->first];
          found = true;
          break;
        }
      }

      if (found) break;
    }

    for (std::map<std::string,std::vector<int>>::const_iterator it = BCBoundaryGroups1_.begin(); it != BCBoundaryGroups1_.end(); ++it)
    {
      const std::vector<int>& grouplist = it->second;
      bool found = false;

      for (int j = 0; j < (int) grouplist.size(); j++)
      {
        if (globalBTraceGroup == grouplist[j])
        {
          //Found the BC associated with the globalBTraceGroup we want, so add it to the local BTraceGroup map
          BCBoundaryGroups_local1[it->first].push_back(local_group);
          BCList_local1[it->first] = BCList1_[it->first];
          found = true;
          break;
        }
      }

      if (found) break;
    }
  } //loop over local btracegroups


  std::vector<int> active_local_BGroup_list0 = AlgEqnSet_Local0::BCParams::getLGBoundaryGroups(BCList_local0, BCBoundaryGroups_local0);
  std::vector<int> active_local_BGroup_list1 = AlgEqnSet_Local1::BCParams::getLGBoundaryGroups(BCList_local1, BCBoundaryGroups_local1);

  //Create local solution fields
//  typedef FieldBundle_DGBR2_Local<typename SolutionClass::FieldBundleType0> FieldBundleType_Local0;
//  typedef FieldBundle_DGBR2_Local<typename SolutionClass::FieldBundleType1> FieldBundleType_Local1;

  // the bgroups go into vec of vecs because FieldBundles need to bundle local groups that came from the same global group
  // This makes no difference in DG, but has fundamental effects on CG
  FieldBundle_Local0 primal_local0(xfld_local, sol_.primal0, sol_.primal0.order, {active_local_BGroup_list0});
  FieldBundle_Local1 primal_local1(xfld_local, sol_.primal1, sol_.primal1.order, {active_local_BGroup_list1});

  typedef ParamFieldBuilder_Block2<ParamBuilderType, PhysDim, TopoDim,
                                   typename FieldBundle0::QFieldType,
                                   typename FieldBundle1::QFieldType> ParamFieldBuilder_Local;

  ParamFieldBuilder_Local parambuilder(xfld_local, sol_.parambuilder, primal_local0.qfld, primal_local1.qfld);

  QuadratureOrder quadrule(xfld_local, quadOrder_);

  std::vector<int> cellGroups = {0};

  //TODO: Only main-neighbor or main-main interior traces for now (no mixed meshes, so max 2 interior trace groups)
  std::vector<int> interiorTraceGroups = xfld_local.getReSolveInteriorTraceGroups();

  AlgEqnSet_Local0 algEqnSet_local0(parambuilder.fld0, primal_local0, {},
                                    sol_.pde0, sol_.disc0, quadrule, resNormType0_, tol0_,
                                    cellGroups, interiorTraceGroups, BCList_local0, BCBoundaryGroups_local0);

  AlgEqnSet_Local1 algEqnSet_local1(parambuilder.fld1, primal_local1, {},
                                    sol_.pde1, sol_.disc1, quadrule, resNormType1_, tol1_,
                                    cellGroups, interiorTraceGroups, BCList_local1, BCBoundaryGroups_local1);

  // Generate off diagonal AlgEqnSets
  typedef PDEMatrixParam0 MatrixQ01;
  typedef PDEMatrixParam1 MatrixQ10;

  typedef DLA::MatrixD<MatrixQ01> MatrixClass01;
  typedef DLA::MatrixD<MatrixQ10> MatrixClass10;

  typedef DLA::MatrixD<MatrixClass01> SystemMatrixClass01;
  typedef DLA::MatrixD<MatrixClass10> SystemMatrixClass10;

  typedef JacobianParam_Local<boost::mpl::vector1_c<int,1>,AlgEqnSet_Local0> JacobianParam_PDE0;
  typedef JacobianParam_Local<boost::mpl::vector1_c<int,1>,AlgEqnSet_Local1> JacobianParam_PDE1;

  //HACK:: Only works when parameter 1 is the unknown in the other PDE
  std::map<int,int> PDEMap0;
  PDEMap0[1] = algEqnSet_local0.iq;

  std::map<int,int> PDEMap1;
  PDEMap1[1] = algEqnSet_local1.iq;

  JacobianParam_PDE0 JP01(algEqnSet_local0, PDEMap0);
  JacobianParam_PDE1 JP10(algEqnSet_local1, PDEMap1);

  // Generate 2x2 Block system
  typedef AlgebraicEquationSet_Block2x2<SystemMatrixClass00, SystemMatrixClass01,
                                        SystemMatrixClass10, SystemMatrixClass11> AlgEqSetType_2x2;

  AlgEqSetType_2x2 BlockAES_local(algEqnSet_local0, JP01,
                                  JP10, algEqnSet_local1);

  typedef typename AlgEqSetType_2x2::SystemMatrix BlockMatrixClass;
  typedef typename AlgEqSetType_2x2::SystemVector BlockVectorClass;
  typedef typename AlgEqSetType_2x2::SystemNonZeroPattern BlockNonZeroPattern;

  // Local vectors
  BlockVectorClass q_local   (BlockAES_local.vectorStateSize()); // current solution
  BlockVectorClass q0_local  (BlockAES_local.vectorStateSize()); // previous solution
  BlockVectorClass dsln_local(BlockAES_local.vectorStateSize()); // solution update
  BlockVectorClass rsd_local (BlockAES_local.vectorEqSize()); // residual

  // Convert the projected qfld to a vector
  BlockAES_local.fillSystemVector(q_local);

  // Compute the lifting operators (rfld)
  BlockAES_local.setSolutionField(q_local);

  // Local Jacobian
  BlockNonZeroPattern nz_local(BlockAES_local.matrixSize());
  BlockMatrixClass jac_local(nz_local);

  int maxIter = 10;
  Real stol = 1e-10;
  Real s = stol + 1.0;
  LocalSolveStatus status(false, maxIter);

  timer clock_residual;
  rsd_local = 0;
  BlockAES_local.residual(rsd_local);
  status.time_residual += clock_residual.elapsed();

  // save off the current residual with the projected solution
  std::vector<std::vector<Real>> rsdNorm0( BlockAES_local.residualNorm(rsd_local) );
  std::vector<std::vector<Real>> rsdNorm = rsdNorm0;
  std::vector<std::vector<Real>> rsdNorm1 = rsdNorm0;

  for (int iter = 0; iter < maxIter && s > stol; iter++)
  {
//    std::vector<std::vector<Real>> nrmRsd = BlockAES_local.residualNorm(rsd_local);
//    std::cout << "Local Newton iteration " << iter  << ": Res_L2norm = " << nrmRsd << std::endl;

    if ( BlockAES_local.convergedResidual(rsdNorm) )
    {
      status.converged = true;
      status.iter = iter;
      break;
    }

    // save the current solution and residual norm
    q0_local = q_local;

    //Jacobian
    timer clock_jacobian;
    jac_local = 0;
    BlockAES_local.jacobian(jac_local);
    status.time_jacobian += clock_jacobian.elapsed();

    //Perform local solve
    timer clock_solve;
    dsln_local = DLA::InverseLUP::Solve(jac_local, rsd_local);
    status.time_solve += clock_solve.elapsed();

    // use a halving line search to update the solution
    Real s = 2.0;
    while (s > stol)
    {
      s = s/2.0;

      //Update local solution vector
      q_local = q0_local - s*dsln_local;

      // check that the solution is valid (this sets the qfld and also computes the rfld)
      bool isValidState = BlockAES_local.isValidStateSystemVector(q_local);
      if ( !isValidState ) continue;

      // Compute the new residual
      timer clock_residual;
      rsd_local = 0;
      BlockAES_local.residual(rsd_local);
      rsdNorm1 = BlockAES_local.residualNorm(rsd_local);
      status.time_residual += clock_residual.elapsed();

      // break out if the residual decreased
      if ( BlockAES_local.decreasedResidual(rsdNorm, rsdNorm1) ) break;
    }

    // save the residual for this iteration
    rsdNorm = BlockAES_local.residualNorm(rsd_local);
  }

  //Gather some information about the residual norms if the solve didn't converge
  if (status.converged == false)
  {
    status.rsdNorm0_unconverged = rsdNorm0;
    status.rsdNorm_unconverged = rsdNorm;
  }

  //Create local adjoint fields - copies the relevant DOFs from the global adjoint solution
  FieldBundle_Local0 adjoint_local0(xfld_local, sol_.adjoint0, sol_.adjoint0.order, {active_local_BGroup_list0});
  FieldBundle_Local1 adjoint_local1(xfld_local, sol_.adjoint1, sol_.adjoint1.order, {active_local_BGroup_list1});

  //Compute local error estimates
  timer clock_errest;
  ErrorEstimateClass0 ErrorEstimate0(parambuilder.fld0, primal_local0, adjoint_local0, {},
                                     sol_.pde0, sol_.disc0, quadrule, cellGroups, interiorTraceGroups,
                                     BCList_local0, BCBoundaryGroups_local0);

  ErrorEstimateClass1 ErrorEstimate1(parambuilder.fld1, primal_local1, adjoint_local1, {},
                                     sol_.pde1, sol_.disc1, quadrule, cellGroups, interiorTraceGroups,
                                     BCList_local1, BCBoundaryGroups_local1);

  std::vector<Real> local_error0, local_error1;
#if 1
  ErrorEstimate0.getLocalSolveErrorIndicator(local_error0); //Accumulate errors only from cellgroup 0 - (main cells)
  ErrorEstimate1.getLocalSolveErrorIndicator(local_error1); //Accumulate errors only from cellgroup 0 - (main cells)
#else
  ErrorEstimate0.getLocalSolveErrorEstimate(local_error0); //Accumulate errors only from cellgroup 0 - (main cells)
  ErrorEstimate1.getLocalSolveErrorEstimate(local_error1); //Accumulate errors only from cellgroup 0 - (main cells)
#endif
  local_error.resize(local_error0.size());
  for (std::size_t i = 0; i < local_error.size(); i ++)
    local_error[i] = local_error0[i] + local_error1[i];
  status.time_errest += clock_errest.elapsed();

  return status;
}

template<class PhysDim, class TopoDim,
         class NDPDEClass0, template<class,class> class BCNDConvert0, class BCVector0,
         class NDPDEClass1, template<class,class> class BCNDConvert1, class BCVector1,
         class ParamBuilderType, class SolutionClass,
         class AlgebraicEqSet0, class AlgebraicEqSet1,
         class OutputIntegrandType>
Real
SolverInterface_DGBR2_Block2x2<PhysDim, TopoDim,
                               NDPDEClass0, BCNDConvert0, BCVector0,
                               NDPDEClass1, BCNDConvert1, BCVector1,
                               ParamBuilderType, SolutionClass,
                               AlgebraicEqSet0, AlgebraicEqSet1,
                               OutputIntegrandType>::
getnDOFperCell(const int cellgroup) const
{
  SANS_ASSERT(cellgroup >= 0 && cellgroup < (int) nDOFperCell_.size());
  return nDOFperCell_[cellgroup];
}

template<class PhysDim, class TopoDim,
         class NDPDEClass0, template<class,class> class BCNDConvert0, class BCVector0,
         class NDPDEClass1, template<class,class> class BCNDConvert1, class BCVector1,
         class ParamBuilderType, class SolutionClass,
         class AlgebraicEqSet0, class AlgebraicEqSet1,
         class OutputIntegrandType>
int
SolverInterface_DGBR2_Block2x2<PhysDim, TopoDim,
                               NDPDEClass0, BCNDConvert0, BCVector0,
                               NDPDEClass1, BCNDConvert1, BCVector1,
                               ParamBuilderType, SolutionClass,
                               AlgebraicEqSet0, AlgebraicEqSet1,
                               OutputIntegrandType>::
getSolutionOrder(const int cellgroup) const
{
  SANS_ASSERT(cellgroup >= 0 && cellgroup < (int) orderVec_.size());
  return orderVec_[cellgroup];
}

template<class PhysDim, class TopoDim,
         class NDPDEClass0, template<class,class> class BCNDConvert0, class BCVector0,
         class NDPDEClass1, template<class,class> class BCNDConvert1, class BCVector1,
         class ParamBuilderType, class SolutionClass,
         class AlgebraicEqSet0, class AlgebraicEqSet1,
         class OutputIntegrandType>
Real
SolverInterface_DGBR2_Block2x2<PhysDim, TopoDim,
                               NDPDEClass0, BCNDConvert0, BCVector0,
                               NDPDEClass1, BCNDConvert1, BCVector1,
                               ParamBuilderType, SolutionClass,
                               AlgebraicEqSet0, AlgebraicEqSet1,
                               OutputIntegrandType>::
getOutput() const
{
  return outputJ_;
}

template<class PhysDim, class TopoDim,
         class NDPDEClass0, template<class,class> class BCNDConvert0, class BCVector0,
         class NDPDEClass1, template<class,class> class BCNDConvert1, class BCVector1,
         class ParamBuilderType, class SolutionClass,
         class AlgebraicEqSet0, class AlgebraicEqSet1,
         class OutputIntegrandType>
SolverInterface_impl<SolverInterface_DGBR2_Block2x2<PhysDim, TopoDim,
                                                    NDPDEClass0, BCNDConvert0, BCVector0,
                                                    NDPDEClass1, BCNDConvert1, BCVector1,
                                                    ParamBuilderType, SolutionClass,
                                                    AlgebraicEqSet0, AlgebraicEqSet1,
                                                    OutputIntegrandType>>
SolverInterface_DGBR2_Block2x2<PhysDim, TopoDim,
                              NDPDEClass0, BCNDConvert0, BCVector0,
                              NDPDEClass1, BCNDConvert1, BCVector1,
                              ParamBuilderType, SolutionClass,
                              AlgebraicEqSet0, AlgebraicEqSet1,
                              OutputIntegrandType>::
CellwiseOperations(const std::vector<int>& cellgroup_list, SolverInterfaceClass& base)
{
  return SolverInterface_impl<SolverInterfaceClass>(cellgroup_list, base);
}

template<class PhysDim, class TopoDim,
         class NDPDEClass0, template<class,class> class BCNDConvert0, class BCVector0,
         class NDPDEClass1, template<class,class> class BCNDConvert1, class BCVector1,
         class ParamBuilderType, class SolutionClass,
         class AlgebraicEqSet0, class AlgebraicEqSet1,
         class OutputIntegrandType>
template<class Topology>
void
SolverInterface_DGBR2_Block2x2<PhysDim, TopoDim,
                               NDPDEClass0, BCNDConvert0, BCVector0,
                               NDPDEClass1, BCNDConvert1, BCVector1,
                               ParamBuilderType, SolutionClass,
                               AlgebraicEqSet0, AlgebraicEqSet1,
                               OutputIntegrandType>::
getSolutionInfo_cellgroup(const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
                          const int cellGroupGlobal)
{
  typedef typename FieldBundle0::QFieldType::template FieldCellGroupType<Topology> QFieldCellGroupType0;

  const QFieldCellGroupType0& cellgrp0 = sol_.primal0.qfld.template getCellGroup<Topology>(cellGroupGlobal);

  nDOFperCell_[cellGroupGlobal] = cellgrp0.basis()->nBasis(); // + cellgrp1.basis()->nBasis();
  orderVec_[cellGroupGlobal]    = cellgrp0.basis()->order(); //max(cellgrp0.basis()->order(), cellgrp1.basis()->order());
}

}

#endif /* SOLVERINTERFACE_DGBR2_BLOCK2X2_H_ */
