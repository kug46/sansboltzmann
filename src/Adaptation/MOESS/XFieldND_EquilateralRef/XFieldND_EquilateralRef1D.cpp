// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "../XFieldND_EquilateralRef.h"

#include "Field/XFieldLine.h"

#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionLine.h"

namespace SANS
{

/*
   A unit grid that consists of one equilateral reference element within a single group
*/

//Copied from XField1D_1Line_X1_1Group
template <class PhysDim, class TopoDim, class Topology>
XFieldND_EquilateralRef<PhysDim, TopoDim, Topology>::XFieldND_EquilateralRef(mpi::communicator& comm_local)
  : XField<PhysDim, TopoDim>(comm_local)
{
  //Create the DOF arrays
  resizeDOF(2);

  //Create the element groups
  resizeInteriorTraceGroups(0);
  resizeBoundaryTraceGroups(2);
  resizeCellGroups(1);

  // nodal coordinates for one line segment.
  DOF(0) = {0};
  DOF(1) = {1};

  // area field variable
  typename BaseType::template
  FieldCellGroupType<Line>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionLineBase::HierarchicalP1, 1 );

  fldAssocCell.setAssociativity( 0 ).setRank( 0 );

  //element cell associativity
  fldAssocCell.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1} );

  cellGroups_[0] = new typename BaseType::template FieldCellGroupType<Line>( fldAssocCell );
  cellGroups_[0]->setDOF(DOF_, nDOF_);

  nElem_ = 1;

  // interior-trace field variable

  // none

  // boundary-trace field variable

  typename BaseType::template FieldTraceGroupType<Node>::FieldAssociativityConstructorType fldAssocBnode0( BasisFunctionNodeBase::P0, 1 );
  typename BaseType::template FieldTraceGroupType<Node>::FieldAssociativityConstructorType fldAssocBnode1( BasisFunctionNodeBase::P0, 1 );

  fldAssocBnode0.setAssociativity( 0 ).setRank( 0 );
  fldAssocBnode1.setAssociativity( 0 ).setRank( 0 );

  // edge-element associativity
  fldAssocBnode0.setAssociativity( 0 ).setNodeGlobalMapping( {0} );
  fldAssocBnode1.setAssociativity( 0 ).setNodeGlobalMapping( {1} );

  fldAssocBnode0.setAssociativity( 0 ).setNormalSignL( -1 );
//    fldAssocBnode0.setAssociativity( 0 ).setNormalSignR( -1 ); //TODO
  fldAssocBnode1.setAssociativity( 0 ).setNormalSignL(  1 );

  // edge-to-cell connectivity
  fldAssocBnode0.setGroupLeft( 0 );
  fldAssocBnode0.setElementLeft( 0, 0 );
  fldAssocBnode0.setCanonicalTraceLeft( CanonicalTraceToCell(1, 0), 0 );

  fldAssocBnode1.setGroupLeft( 0 );
  fldAssocBnode1.setElementLeft( 0, 0 );
  fldAssocBnode1.setCanonicalTraceLeft( CanonicalTraceToCell(0, 0), 0 );

  boundaryTraceGroups_[0] = new typename BaseType::template FieldTraceGroupType<Node>( fldAssocBnode0 );
  boundaryTraceGroups_[1] = new typename BaseType::template FieldTraceGroupType<Node>( fldAssocBnode1 );

  boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[1]->setDOF(DOF_, nDOF_);

  //Check that the grid is correct
  checkGrid();
}

// Explicit instantiation
template class XFieldND_EquilateralRef<PhysD1, TopoD1, Line>;

}
