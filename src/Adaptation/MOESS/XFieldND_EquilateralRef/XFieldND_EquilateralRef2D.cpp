// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "../XFieldND_EquilateralRef.h"

#include "Field/XFieldArea.h"

#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "BasisFunction/BasisFunctionArea_Quad.h"

namespace SANS
{

/*
   A unit grid that consists of one equilateral reference element within a single group
*/

// Copied (nodes coordinates modified) from XField2D_1Triangle_X1_1Group
template <class PhysDim, class TopoDim, class Topology>
XFieldND_EquilateralRef<PhysDim, TopoDim, Topology>::XFieldND_EquilateralRef(mpi::communicator& comm_local)
  : XField<PhysDim, TopoDim>(comm_local)
{
  //Create the DOF arrays
  resizeDOF(3);

  //Create the element groups
  resizeInteriorTraceGroups(0);
  resizeBoundaryTraceGroups(3);
  resizeCellGroups(1);

  // nodal coordinates for the equilateral triangle.
  DOF(0) = {0.0, 0.0};
  DOF(1) = {1.0, 0.0};
  DOF(2) = {0.5, 0.5*sqrt(3)};

  // area field variable
  typename BaseType::template
  FieldCellGroupType<Triangle>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionAreaBase<Triangle>::HierarchicalP1, 1 );

  fldAssocCell.setAssociativity( 0 ).setRank( 0 );

  //element area associativity
  fldAssocCell.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1, 2} );

  // edge signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( 0 ).setEdgeSign( +1, 0 );

  cellGroups_[0] = new typename BaseType::template FieldCellGroupType<Triangle>( fldAssocCell );
  cellGroups_[0]->setDOF(DOF_, nDOF_);

  nElem_ = 1;

  // interior-edge field variable

  // none

  // boundary-edge field variable
  typedef typename BaseType::template FieldTraceGroupType<Line>::FieldAssociativityConstructorType FieldAssociativityConstructorType;
  FieldAssociativityConstructorType fldAssocBedge0( BasisFunctionLineBase::HierarchicalP1, 1 );
  FieldAssociativityConstructorType fldAssocBedge1( BasisFunctionLineBase::HierarchicalP1, 1 );
  FieldAssociativityConstructorType fldAssocBedge2( BasisFunctionLineBase::HierarchicalP1, 1 );

  fldAssocBedge0.setAssociativity( 0 ).setRank( 0 );
  fldAssocBedge1.setAssociativity( 0 ).setRank( 0 );
  fldAssocBedge2.setAssociativity( 0 ).setRank( 0 );

  // edge-element associativity
  fldAssocBedge0.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1} );
  fldAssocBedge1.setAssociativity( 0 ).setNodeGlobalMapping( {1, 2} );
  fldAssocBedge2.setAssociativity( 0 ).setNodeGlobalMapping( {2, 0} );

  // edge-to-cell connectivity
  fldAssocBedge0.setGroupLeft( 0 );
  fldAssocBedge1.setGroupLeft( 0 );
  fldAssocBedge2.setGroupLeft( 0 );
  fldAssocBedge0.setElementLeft( 0, 0 );
  fldAssocBedge1.setElementLeft( 0, 0 );
  fldAssocBedge2.setElementLeft( 0, 0 );
  fldAssocBedge0.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), 0 );
  fldAssocBedge1.setCanonicalTraceLeft( CanonicalTraceToCell(0, 1), 0 );
  fldAssocBedge2.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), 0 );

  boundaryTraceGroups_[0] = new typename BaseType::template FieldTraceGroupType<Line>( fldAssocBedge0 );
  boundaryTraceGroups_[1] = new typename BaseType::template FieldTraceGroupType<Line>( fldAssocBedge1 );
  boundaryTraceGroups_[2] = new typename BaseType::template FieldTraceGroupType<Line>( fldAssocBedge2 );

  boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[1]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[2]->setDOF(DOF_, nDOF_);

  //Check that the grid is correct
  checkGrid();
}

#if 0

//Copied from XField2D_1Quad_X1_1Group
XFieldND_EquilateralRef<PhysD2, TopoD2, Quad>::XFieldND_EquilateralRef(mpi::communicator& comm_local)
  : XField<PhysD2, TopoD2>(comm_local)
{
  //Create the DOF arrays
  resizeDOF(4);

  //Create the element groups
  resizeInteriorTraceGroups(0);
  resizeBoundaryTraceGroups(4);
  resizeCellGroups(1);

  // nodal coordinates for the quad.
  DOF(0) = {0, 0};
  DOF(1) = {1, 0};
  DOF(2) = {1, 1};
  DOF(3) = {0, 1};

  // area field variable
  FieldCellGroupType<Quad>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionAreaBase<Quad>::HierarchicalP1, 1 );

  fldAssocCell.setAssociativity( 0 ).setRank( 0 );

  //element area associativity
  fldAssocCell.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1, 2, 3} );

  cellGroups_[0] = new FieldCellGroupType<Quad>( fldAssocCell );
  cellGroups_[0]->setDOF(DOF_, nDOF_);

  nElem_ = 1;

  // interior-edge field variable

  // none

  // boundary-edge field variable

  FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocBedge0( BasisFunctionLineBase::HierarchicalP1, 1 );
  FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocBedge1( BasisFunctionLineBase::HierarchicalP1, 1 );
  FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocBedge2( BasisFunctionLineBase::HierarchicalP1, 1 );
  FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocBedge3( BasisFunctionLineBase::HierarchicalP1, 1 );

  fldAssocBedge0.setAssociativity( 0 ).setRank( 0 );
  fldAssocBedge1.setAssociativity( 0 ).setRank( 0 );
  fldAssocBedge2.setAssociativity( 0 ).setRank( 0 );
  fldAssocBedge3.setAssociativity( 0 ).setRank( 0 );

  // edge-element associativity
  fldAssocBedge0.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1} );
  fldAssocBedge1.setAssociativity( 0 ).setNodeGlobalMapping( {1, 2} );
  fldAssocBedge2.setAssociativity( 0 ).setNodeGlobalMapping( {2, 3} );
  fldAssocBedge3.setAssociativity( 0 ).setNodeGlobalMapping( {3, 0} );

  // edge-to-cell connectivity
  fldAssocBedge0.setGroupLeft( 0 );
  fldAssocBedge1.setGroupLeft( 0 );
  fldAssocBedge2.setGroupLeft( 0 );
  fldAssocBedge3.setGroupLeft( 0 );
  fldAssocBedge0.setElementLeft( 0, 0 );
  fldAssocBedge1.setElementLeft( 0, 0 );
  fldAssocBedge2.setElementLeft( 0, 0 );
  fldAssocBedge3.setElementLeft( 0, 0 );
  fldAssocBedge0.setCanonicalTraceLeft( CanonicalTraceToCell(0, 1), 0 );
  fldAssocBedge1.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), 0 );
  fldAssocBedge2.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), 0 );
  fldAssocBedge3.setCanonicalTraceLeft( CanonicalTraceToCell(3, 1), 0 );

  boundaryTraceGroups_[0] = new FieldTraceGroupType<Line>( fldAssocBedge0 );
  boundaryTraceGroups_[1] = new FieldTraceGroupType<Line>( fldAssocBedge1 );
  boundaryTraceGroups_[2] = new FieldTraceGroupType<Line>( fldAssocBedge2 );
  boundaryTraceGroups_[3] = new FieldTraceGroupType<Line>( fldAssocBedge3 );

  boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[1]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[2]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[3]->setDOF(DOF_, nDOF_);

  //Check that the grid is correct
  checkGrid();
}
#endif

// Explicit instantiation
template class XFieldND_EquilateralRef<PhysD2, TopoD2, Triangle>;

}
