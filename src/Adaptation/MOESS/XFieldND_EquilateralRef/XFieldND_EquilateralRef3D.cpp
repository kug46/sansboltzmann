// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "../XFieldND_EquilateralRef.h"

#include "Field/XFieldVolume.h"

#include "BasisFunction/BasisFunctionCategory.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron.h"
#include "BasisFunction/BasisFunctionVolume_Hexahedron.h"

namespace SANS
{

/*
   A unit grid that consists of one equilateral reference element within a single group
*/

// Copied (nodes coordinates modified) from XField3D_1Tet_X1_1Group
template <class PhysDim, class TopoDim, class Topology>
XFieldND_EquilateralRef<PhysDim, TopoDim, Topology>::XFieldND_EquilateralRef(mpi::communicator& comm_local)
  : XField<PhysDim, TopoDim>(comm_local)
{
  //Create the DOF arrays
  resizeDOF(4);

  //Create the element groups
  resizeInteriorTraceGroups(0);
  resizeBoundaryTraceGroups(4);
  resizeCellGroups(1);

  // nodal coordinates for the equilateral tet
  DOF(0) = {0.0, 0.0, 0.0};
  DOF(1) = {1.0, 0.0, 0.0};
  DOF(2) = {0.5, sqrt(3.0)/2.0, 0.0};
  DOF(3) = {0.5, sqrt(3.0)/6.0, sqrt(2.0/3.0)};

  // volume field variable
  typename BaseType::template
  FieldCellGroupType<Tet>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionVolumeBase<Tet>::HierarchicalP1, 1 );

  fldAssocCell.setAssociativity( 0 ).setRank( 0 );

  //element volume associativity
  fldAssocCell.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1, 2, 3} );

  // face signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( 0 ).setFaceSign( +1, 0 );
  fldAssocCell.setAssociativity( 0 ).setFaceSign( +1, 1 );
  fldAssocCell.setAssociativity( 0 ).setFaceSign( +1, 2 );
  fldAssocCell.setAssociativity( 0 ).setFaceSign( +1, 3 );


  cellGroups_[0] = new typename BaseType::template FieldCellGroupType<Tet>( fldAssocCell );
  cellGroups_[0]->setDOF(DOF_, nDOF_);

  nElem_ = 1;

  // interior-edge field variable

  // none

  // boundary-edge field variable
  typedef typename BaseType::template FieldTraceGroupType<Triangle>::FieldAssociativityConstructorType FieldAssociativityConstructorType;
  FieldAssociativityConstructorType fldAssocBface0( BasisFunctionAreaBase<Triangle>::HierarchicalP1, 1 );
  FieldAssociativityConstructorType fldAssocBface1( BasisFunctionAreaBase<Triangle>::HierarchicalP1, 1 );
  FieldAssociativityConstructorType fldAssocBface2( BasisFunctionAreaBase<Triangle>::HierarchicalP1, 1 );
  FieldAssociativityConstructorType fldAssocBface3( BasisFunctionAreaBase<Triangle>::HierarchicalP1, 1 );

  fldAssocBface0.setAssociativity( 0 ).setRank( 0 );
  fldAssocBface1.setAssociativity( 0 ).setRank( 0 );
  fldAssocBface2.setAssociativity( 0 ).setRank( 0 );
  fldAssocBface3.setAssociativity( 0 ).setRank( 0 );

  // face-element associativity
  fldAssocBface0.setAssociativity( 0 ).setNodeGlobalMapping( {1, 2, 3} );
  fldAssocBface1.setAssociativity( 0 ).setNodeGlobalMapping( {0, 3, 2} );
  fldAssocBface2.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1, 3} );
  fldAssocBface3.setAssociativity( 0 ).setNodeGlobalMapping( {0, 2, 1} );

  // face-to-cell connectivity
  fldAssocBface0.setGroupLeft( 0 );
  fldAssocBface1.setGroupLeft( 0 );
  fldAssocBface2.setGroupLeft( 0 );
  fldAssocBface3.setGroupLeft( 0 );
  fldAssocBface0.setElementLeft( 0, 0 );
  fldAssocBface1.setElementLeft( 0, 0 );
  fldAssocBface2.setElementLeft( 0, 0 );
  fldAssocBface3.setElementLeft( 0, 0 );
  fldAssocBface0.setCanonicalTraceLeft( CanonicalTraceToCell(0, 1), 0 );
  fldAssocBface1.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), 0 );
  fldAssocBface2.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), 0 );
  fldAssocBface3.setCanonicalTraceLeft( CanonicalTraceToCell(3, 1), 0 );

  boundaryTraceGroups_[0] = new typename BaseType::template FieldTraceGroupType<Triangle>( fldAssocBface0 );
  boundaryTraceGroups_[1] = new typename BaseType::template FieldTraceGroupType<Triangle>( fldAssocBface1 );
  boundaryTraceGroups_[2] = new typename BaseType::template FieldTraceGroupType<Triangle>( fldAssocBface2 );
  boundaryTraceGroups_[3] = new typename BaseType::template FieldTraceGroupType<Triangle>( fldAssocBface3 );

  boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[1]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[2]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[3]->setDOF(DOF_, nDOF_);

  //Check that the grid is correct
  checkGrid();
}

#if 0
//Copied from XField3D_1Hex_X1_1Group
XFieldND_EquilateralRef<PhysD3, TopoD3, Hex>::XFieldND_EquilateralRef(mpi::communicator& comm_local)
  : XField<PhysD3, TopoD3>(comm_local)
{
  //Create the DOF arrays
  resizeDOF(8);

  //Create the element groups
  resizeInteriorTraceGroups(0);
  resizeBoundaryTraceGroups(6);
  resizeCellGroups(1);

  // nodal coordinates for the hex
  DOF(0) = {0, 0, 0};
  DOF(1) = {1, 0, 0};
  DOF(2) = {1, 1, 0};
  DOF(3) = {0, 1, 0};

  DOF(4) = {0, 0, 1};
  DOF(5) = {1, 0, 1};
  DOF(6) = {1, 1, 1};
  DOF(7) = {0, 1, 1};

  // volume field variable
  FieldCellGroupType<Hex>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionVolumeBase<Hex>::HierarchicalP1, 1 );

  fldAssocCell.setAssociativity( 0 ).setRank( 0 );

  //element volume associativity
  fldAssocCell.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1, 2, 3, 4, 5, 6, 7} );

  // face signs for elements (L is +, R is -)
  fldAssocCell.setAssociativity( 0 ).setFaceSign( +1, 0 );
  fldAssocCell.setAssociativity( 0 ).setFaceSign( +1, 1 );
  fldAssocCell.setAssociativity( 0 ).setFaceSign( +1, 2 );
  fldAssocCell.setAssociativity( 0 ).setFaceSign( +1, 3 );
  fldAssocCell.setAssociativity( 0 ).setFaceSign( +1, 4 );
  fldAssocCell.setAssociativity( 0 ).setFaceSign( +1, 5 );


  cellGroups_[0] = new FieldCellGroupType<Hex>( fldAssocCell );

  cellGroups_[0]->setDOF(DOF_, nDOF_);

  nElem_ = 1;

  // interior-edge field variable

  // none

  // boundary-edge field variable

  FieldTraceGroupType<Quad>::FieldAssociativityConstructorType fldAssocBface0( BasisFunctionAreaBase<Quad>::HierarchicalP1, 1 );
  FieldTraceGroupType<Quad>::FieldAssociativityConstructorType fldAssocBface1( BasisFunctionAreaBase<Quad>::HierarchicalP1, 1 );
  FieldTraceGroupType<Quad>::FieldAssociativityConstructorType fldAssocBface2( BasisFunctionAreaBase<Quad>::HierarchicalP1, 1 );
  FieldTraceGroupType<Quad>::FieldAssociativityConstructorType fldAssocBface3( BasisFunctionAreaBase<Quad>::HierarchicalP1, 1 );
  FieldTraceGroupType<Quad>::FieldAssociativityConstructorType fldAssocBface4( BasisFunctionAreaBase<Quad>::HierarchicalP1, 1 );
  FieldTraceGroupType<Quad>::FieldAssociativityConstructorType fldAssocBface5( BasisFunctionAreaBase<Quad>::HierarchicalP1, 1 );

  fldAssocBface0.setAssociativity( 0 ).setRank( 0 );
  fldAssocBface1.setAssociativity( 0 ).setRank( 0 );
  fldAssocBface2.setAssociativity( 0 ).setRank( 0 );
  fldAssocBface3.setAssociativity( 0 ).setRank( 0 );
  fldAssocBface4.setAssociativity( 0 ).setRank( 0 );
  fldAssocBface5.setAssociativity( 0 ).setRank( 0 );

  // face-element associativity
  fldAssocBface0.setAssociativity( 0 ).setNodeGlobalMapping( {0, 3, 2, 1} );
  fldAssocBface1.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1, 5, 4} );
  fldAssocBface2.setAssociativity( 0 ).setNodeGlobalMapping( {1, 2, 6, 5} );
  fldAssocBface3.setAssociativity( 0 ).setNodeGlobalMapping( {3, 7, 6, 2} );
  fldAssocBface4.setAssociativity( 0 ).setNodeGlobalMapping( {0, 4, 7, 3} );
  fldAssocBface5.setAssociativity( 0 ).setNodeGlobalMapping( {4, 5, 6, 7} );

  // face-to-cell connectivity
  fldAssocBface0.setGroupLeft( 0 );
  fldAssocBface1.setGroupLeft( 0 );
  fldAssocBface2.setGroupLeft( 0 );
  fldAssocBface3.setGroupLeft( 0 );
  fldAssocBface4.setGroupLeft( 0 );
  fldAssocBface5.setGroupLeft( 0 );
  fldAssocBface0.setElementLeft( 0, 0 );
  fldAssocBface1.setElementLeft( 0, 0 );
  fldAssocBface2.setElementLeft( 0, 0 );
  fldAssocBface3.setElementLeft( 0, 0 );
  fldAssocBface4.setElementLeft( 0, 0 );
  fldAssocBface5.setElementLeft( 0, 0 );
  fldAssocBface0.setCanonicalTraceLeft( CanonicalTraceToCell(0, 1), 0 );
  fldAssocBface1.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), 0 );
  fldAssocBface2.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), 0 );
  fldAssocBface3.setCanonicalTraceLeft( CanonicalTraceToCell(3, 1), 0 );
  fldAssocBface4.setCanonicalTraceLeft( CanonicalTraceToCell(4, 1), 0 );
  fldAssocBface5.setCanonicalTraceLeft( CanonicalTraceToCell(5, 1), 0 );

  boundaryTraceGroups_[0] = new FieldTraceGroupType<Quad>( fldAssocBface0 );
  boundaryTraceGroups_[1] = new FieldTraceGroupType<Quad>( fldAssocBface1 );
  boundaryTraceGroups_[2] = new FieldTraceGroupType<Quad>( fldAssocBface2 );
  boundaryTraceGroups_[3] = new FieldTraceGroupType<Quad>( fldAssocBface3 );
  boundaryTraceGroups_[4] = new FieldTraceGroupType<Quad>( fldAssocBface4 );
  boundaryTraceGroups_[5] = new FieldTraceGroupType<Quad>( fldAssocBface5 );

  boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[1]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[2]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[3]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[4]->setDOF(DOF_, nDOF_);
  boundaryTraceGroups_[5]->setDOF(DOF_, nDOF_);

  //Check that the grid is correct
  checkGrid();
}
#endif

//Explicit instantiation
template class XFieldND_EquilateralRef<PhysD3, TopoD3, Tet>;

}
