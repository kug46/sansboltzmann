// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOLVERINTERFACE_DGADVECTIVE_H_
#define SOLVERINTERFACE_DGADVECTIVE_H_

#include <ostream>
#include <vector>

#include "Discretization/DG/AlgebraicEquationSet_DGAdvective.h"
#include "Discretization/DG/AlgebraicEquationSet_Local_DG.h"

// Output Functional Stuff that needs to be removed
#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/JacobianFunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/FunctionalBoundaryTrace_Dispatch_Galerkin.h"
#include "Discretization/Galerkin/JacobianFunctionalBoundaryTrace_Dispatch_Galerkin.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#include "MPI/communicator_fwd.h"
#endif

// What will eventually be the true Solver Interface. This is a stop gap solution until output functionals work
#include "Adaptation/MOESS/SolverInterface.h"

#include "ErrorEstimate/DG/ErrorEstimate_DGAdvective.h"

#ifdef BOUNDARYOUTPUT
// HACK: These boundary integrands are needed for the boundary output at the moment.
//       They should be removed in the future.
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_sansLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Flux_mitState_Nitsche.h"
#endif

namespace SANS
{

template<class SolutionClass, class AlgebraicEqSet, class OutputIntegrandType>
class SolverInterface_DGAdvective : public SolverInterface<SolutionClass, AlgebraicEqSet, OutputIntegrandType>
{
public:
  typedef SolverInterface<SolutionClass, AlgebraicEqSet, OutputIntegrandType> BaseType;

  typedef typename AlgebraicEqSet::PhysDim PhysDim;
  typedef typename AlgebraicEqSet::TopoDim TopoDim;

  const int D = PhysDim::D;
  typedef XField<PhysDim, TopoDim> XFieldType;

  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef typename SolutionClass::ParamFieldBuilderType ParamFieldBuilderType;

  typedef typename AlgebraicEqSet::NDPDEClass NDPDEClass;
  typedef typename NDPDEClass::template ArrayQ<Real> PDEArrayQ;

  typedef AlgebraicEqSet AlgEqnSet_Global;

  typedef typename LocalEquationSet<AlgEqnSet_Global>::type AlgEqnSet_Local;
  typedef typename AlgEqnSet_Global::FieldBundle FieldBundle;
  typedef typename AlgEqnSet_Global::ErrorEstimateClass ErrorEstimateClass;
  typedef typename AlgEqnSet_Local::FieldBundle FieldBundle_Local;

  typedef SolverInterface_DGAdvective SolverInterfaceClass;

  typedef typename OutputIntegrandType::template ArrayJ<Real> ArrayJ;

  typedef typename AlgEqnSet_Global::ArrayQ ArrayQ;

  template< class... BCArgs >
  SolverInterface_DGAdvective(SolutionClass& sol, const ResidualNormType& resNormType, std::vector<Real>& tol, const int quadOrder,
                        const std::vector<int>& cellgroups, const std::vector<int>& interiortracegroups,
                        PyDict& BCList, const std::map< std::string, std::vector<int> >& BCBoundaryGroups,
                        const PyDict& NonlinearSolverDict, const PyDict& AdjLinearSolverDict,
                        const OutputIntegrandType& fcnOutput, BCArgs&... args )
    : BaseType( sol, resNormType, tol, quadOrder, cellgroups, interiortracegroups, BCList, BCBoundaryGroups, NonlinearSolverDict,
                AdjLinearSolverDict, fcnOutput, args... )
  {
    /*
    errorIndicator_ = 0.0;
    errorEstimate_ = 0.0;

    //Loop over each cellgroup and find the no. of DOF per cell and solution order for that cellgroup
    nDOFperCell_.resize((int) xfld_.nCellGroups(), -1);
    orderVec_.resize((int) xfld_.nCellGroups(), -1);
    for_each_CellGroup<TopoDim>::apply( CellwiseOperations(cellgroup_list_, *this), xfld_);
    */
  }

  virtual ~SolverInterface_DGAdvective() {}

  virtual void solveGlobalPrimalProblem() override;
  virtual void solveGlobalAdjointProblem() override;

protected:

  using BaseType::sol_; //class containing all the solution data
  using BaseType::xfld_; // mesh
  using BaseType::resNormType_;
  using BaseType::tol_;
  using BaseType::quadOrder_;
  using BaseType::cellgroup_list_;
  using BaseType::interiortracegroup_list_;
  using BaseType::BCList_;
  using BaseType::BCBoundaryGroups_;
  using BaseType::NonlinearSolverDict_;
  using BaseType::AdjLinearSolverDict_;
  using BaseType::verbosity_;

  using BaseType::AlgEqnSet_; // AlgebraicEquationSet

  using BaseType::errorArray_; //Array of elemental error estimates (index:[cellgroup][elem])
  using BaseType::fcnOutput_;
  using BaseType::errorIndicator_;
  using BaseType::errorEstimate_;
  using BaseType::outputJ_; //output functional evaluated at current solution

  using BaseType::nDOFperCell_; //indexing: [global cellgrp] - number of degrees of freedom per cell
  using BaseType::orderVec_; //indexing: [global cellgrp] - solution order of each cellgroup
};

template<class SolutionClass, class AlgebraicEqSet, class OutputIntegrandType>
void
SolverInterface_DGAdvective<SolutionClass, AlgebraicEqSet, OutputIntegrandType>::
solveGlobalPrimalProblem()
{
  timer clock;

  this->template solveGlobalPrimalContinuation<typename NDPDEClass::Temporal>();

  if (get<-1>(xfld_).comm()->rank() == 0 && verbosity_ )
    std::cout << "Primal solve time: " << std::fixed << std::setprecision(3) << clock.elapsed() << "s" << std::endl;

  // evaluate output function
  outputJ_ = 0.0;

  QuadratureOrder quadrule(xfld_, quadOrder_);

#ifndef BOUNDARYOUTPUT
  IntegrateCellGroups<TopoDim>::integrate( FunctionalCell_Galerkin( fcnOutput_, outputJ_ ),
                                           sol_.paramfld, sol_.primal.qfld,
                                           quadrule.cellOrders.data(), quadrule.cellOrders.size() );
#else
  //AlgEqnSet_.dispatchBC().dispatch_sansFT(
  //  FunctionalBoundaryTrace_Dispatch_Galerkin(fcnOutput_, sol_.paramfld, sol_.primal.qfld,
  //                          quadrule.boundaryTraceOrders.data(), quadrule.boundaryTraceOrders.size(), outputJ_ ) );
  AlgEqnSet_.dispatchBC().dispatch(
      FunctionalBoundaryTrace_FieldTrace_Dispatch_Galerkin(fcnOutput_, sol_.paramfld, sol_.primal.qfld, sol_.primal.lgfld,
                                                           quadrule.boundaryTraceOrders.data(),
                                                           quadrule.boundaryTraceOrders.size(), outputJ_ ),
      FunctionalBoundaryTrace_Dispatch_Galerkin(fcnOutput_, sol_.paramfld, sol_.primal.qfld,
                                                quadrule.boundaryTraceOrders.data(),
                                                quadrule.boundaryTraceOrders.size(), outputJ_ ) );
#endif
}

template<class SolutionClass, class AlgebraicEqSet, class OutputIntegrandType>
void
SolverInterface_DGAdvective<SolutionClass, AlgebraicEqSet, OutputIntegrandType>::
solveGlobalAdjointProblem()
{
  typedef typename AlgEqnSet_Global::SystemVector SystemVectorClass;
  typedef typename AlgEqnSet_Global::SystemMatrix SystemMatrixClass;

  timer clock;

  //Create primal solutions in richer solution space
  FieldBundle primal_pro(xfld_, sol_.adjoint.order,
                         sol_.primal, sol_.mitlg_boundarygroups );

  // Prolongate the primal solution to the richer solution space
  sol_.primal.projectTo(primal_pro);

  // Create param fields with prolongated primal solution
  ParamFieldBuilderType parambuilder(xfld_, primal_pro.qfld, sol_.parambuilder.dict);

  QuadratureOrder quadrule(xfld_, quadOrder_);

  DiscretizationObject disc;

  //Create AlgebraicEquationSets for solutions in richer space
  AlgEqnSet_Global AlgEqnSet_Pro(parambuilder.fld, primal_pro, sol_.pliftedQuantityfld,
                                 sol_.pde, disc, quadrule, resNormType_, tol_,
                                 cellgroup_list_, interiortracegroup_list_,
                                 BCList_, BCBoundaryGroups_);

  // Functional integral
  SystemVectorClass rhs(AlgEqnSet_Pro.vectorEqSize());
  rhs = 0;

  const int iPDE = AlgEqnSet_Pro.iPDE;

#ifndef BOUNDARYOUTPUT
  IntegrateCellGroups<TopoDim>::integrate( JacobianFunctionalCell_Galerkin( fcnOutput_, rhs(iPDE) ),
                                           parambuilder.fld, primal_pro.qfld,
                                           quadrule.cellOrders.data(), quadrule.cellOrders.size() );
#else
  typedef SurrealS<NDPDEClass::N> SurrealClass;
//  AlgEqnSet_Pro.dispatchBC().dispatch_sansFT(
//      JacobianFunctionalBoundaryTrace_Dispatch_Galerkin<SurrealClass>(fcnOutput_, parambuilder.fld, primal_pro.qfld,
//                                                     quadrule.boundaryTraceOrders.data(), quadrule.boundaryTraceOrders.size(),
//                                                     rhs(iPDE) ) );
  const int iBC = AlgEqnSet_Pro.iBC;

  AlgEqnSet_Pro.dispatchBC().dispatch(
      JacobianFunctionalBoundaryTrace_FieldTrace_Dispatch_Galerkin<SurrealClass>(fcnOutput_, parambuilder.fld, primal_pro.qfld, primal_pro.lgfld,
                                                                                 quadrule.boundaryTraceOrders.data(),
                                                                                 quadrule.boundaryTraceOrders.size(), rhs(iPDE), rhs(iBC) ),
      JacobianFunctionalBoundaryTrace_Dispatch_Galerkin<SurrealClass>(fcnOutput_, parambuilder.fld, primal_pro.qfld,
                                                                      quadrule.boundaryTraceOrders.data(),
                                                                      quadrule.boundaryTraceOrders.size(), rhs(iPDE) ) );
#endif

  // adjoint solver
  SLA::LinearSolver< SystemMatrixClass > solver(AdjLinearSolverDict_, AlgEqnSet_Pro, SLA::TransposeSolve);

  SystemVectorClass adj(AlgEqnSet_Pro.vectorStateSize());
  adj = 0; // set initial guess

  solver.solve(rhs, adj);

//  adj -= d_adj;

  // update solution
  AlgEqnSet_Pro.setSolutionField(adj, sol_.adjoint.qfld, sol_.adjoint.lgfld );

  if (get<-1>(xfld_).comm()->rank() == 0 && verbosity_ )
    std::cout << "Adjoint order=" << sol_.adjoint.order << " solve time: "
              << std::fixed << std::setprecision(3) << clock.elapsed() << "s" << std::endl;


}

} // namespace SANS

#endif /* SOLVERINTERFACE_DGADVECTIVE_H_ */
