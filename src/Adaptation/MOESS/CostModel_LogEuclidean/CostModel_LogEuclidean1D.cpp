// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define COSTMODEL_LOGEUCLIDEAN_INSTANTIATE
#include "CostModel_LogEuclidean_impl.h"

#include "Field/FieldLine_CG_Cell.h"

#include "Field/XFieldLine.h"

namespace SANS
{

//=============================================================================
//Explicit instantiations
template class CostModel_LogEuclidean<PhysD1,TopoD1>;

}
