// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(COSTMODEL_LOGEUCLIDEAN_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "../CostModel_LogEuclidean.h"

#include "Surreal/SurrealS.h"

#include "Adaptation/MOESS/ReferenceElementCost.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Trace.h"

#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/JacobianFunctionalCell_Galerkin.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/Integrand_Type.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{


//=============================================================================
//
// Cost = integral( c_p exp(0.5*c(x)) ) dx
// where c(x) = interpolant of [ log( det(M)*exp(trace(S)) ) ]
//
template <class PhysDim_>
class IntegrandCell_CostModel_LogEuclidean :
    public IntegrandCellType< IntegrandCell_CostModel_LogEuclidean<PhysDim_> >
{
public:
  typedef PhysDim_ PhysDim;

  // Array of the field variables integrated
  template<class T>
  using ArrayQ = T;

  // Array of output functionals
  template<class T>
  using ArrayJ = T;

  // Matrix required to represent the Jacobian of this functional
  template<class T>
  using MatrixJ = T;


  IntegrandCell_CostModel_LogEuclidean(const Real nDOF0, const std::vector<int>& cellGroups)
    : nDOF0_(nDOF0), cellGroups_(cellGroups) {}

  std::size_t nCellGroups() const          { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n];     }

  template<class T, class TopoDim, class Topology>
  class Functor
  {
  public:
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;
    typedef QuadraturePoint<TopoDim> QuadPointType;
    typedef typename ElementXFieldType::VectorX VectorX;
    typedef Element<ArrayQ<T>, TopoDim, Topology> ElementCFieldType;

    Functor( const ElementXFieldType& xfldElem,
             const ElementCFieldType& cfldElem, const Real nDOF0 ) :
             xfldElem_(xfldElem),
             cfldElem_(cfldElem),
             nDOF_(cfldElem_.nDOF()),
             phi_( new Real[nDOF_] ),
             cp_(ReferenceElementCost<Topology>::getCost(nDOF0)) {}

    ~Functor()
    {
      delete [] phi_;
    }

    // element integrand
    template<class Ti>
    void operator()( const QuadPointType& sRef, ArrayJ<Ti>& integrand ) const
    {
      ArrayQ<T> c;

      cfldElem_.eval( sRef, c );

      integrand = cp_*exp(0.5*c);
    }


    // element integrand
    void operator()(const Real dJ, const QuadPointType& sRef, DLA::VectorD<Real>& mtxPDEElem ) const
    {
      typedef SurrealS<1> SurrealClass;

      ArrayQ<Real> c;
      ArrayQ<SurrealClass> cSurreal;

      cfldElem_.eval( sRef, c );
      cSurreal = c;

      cfldElem_.evalBasis( sRef, phi_, nDOF_ );

      // element integrand/residual
      ArrayJ<SurrealClass> integrandSurreal;
      MatrixJ<Real> J_q =0; // temporary storage

      DLA::index(cSurreal, 0).deriv(0) = 1;

      integrandSurreal = cp_*exp(0.5*cSurreal);

      // accumulate derivatives into element jacobian
      DLA::index(cSurreal, 0).deriv(0) = 0; // Reset the derivative

      // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
      DLA::index(J_q,0,0) = DLA::index(integrandSurreal, 0).deriv(0);

      for (int j = 0; j < cfldElem_.nDOF(); j++)
        mtxPDEElem[j] += dJ*phi_[j]*J_q;

    }

  protected:
    const ElementXFieldType& xfldElem_;
    const ElementCFieldType& cfldElem_;
    const int nDOF_;
    mutable Real *phi_;
    const Real cp_;
  };

  template<class T, class TopoDim, class Topology>
  Functor<T, TopoDim, Topology> integrand(const ElementXField<PhysDim, TopoDim, Topology>& xfldElem,
                                          const Element<ArrayQ<T>, TopoDim, Topology>& cfldElem) const
  {
    return Functor<T, TopoDim, Topology>(xfldElem, cfldElem, nDOF0_);
  }

private:
  const Real nDOF0_; //No. of DOF on original element
  const std::vector<int> cellGroups_;
};

//=============================================================================
template <class PhysDim, class TopoDim>
CostModel_LogEuclidean<PhysDim, TopoDim>::CostModel_LogEuclidean(
    const Field_CG_Cell<PhysDim,TopoDim,MatrixSym>& implied_metric,
    const std::vector<int>& cellgroup_list )
 : xfld_(implied_metric.getXField()), cellgroup_list_(cellgroup_list),
   implied_metric_(implied_metric),
   quadratureOrder_(3), // 3 to be consistent with EPIC

   // Create a P1 CG field for the quantity c that needs to be interpolated
   // c_node = log( det(M_node) ) + tr(S_node)
   //   or
   // c_node = tr(logM_node)
   cfld_(xfld_, 1, BasisFunctionCategory_Lagrange, cellgroup_list)
{
}


//=============================================================================
template <class PhysDim, class TopoDim>
Real
CostModel_LogEuclidean<PhysDim, TopoDim>::computeCost_Implied(const Real nDOF0) const
{
  typedef IntegrandCell_CostModel_LogEuclidean<PhysDim> IntegrandClass;

  const int nNode = implied_metric_.nDOF();

  for (int node = 0; node < nNode; node++)
  {
    //Get nodal metric determinant
    Real detM = DLA::Det(implied_metric_.DOF(node));
    SANS_ASSERT_MSG( detM > 0.0 , "negative nodal metric! detM = %e", detM );

    cfld_.DOF(node) = log(detM);
  }

  IntegrandClass fcnIntegrand(nDOF0, cellgroup_list_);
  const int nCellGroups = xfld_.nCellGroups();
  SANS_ASSERT(nCellGroups > 0);

  std::vector<int> quadratureOrderVec(nCellGroups, quadratureOrder_);

  // Compute the cost
  Real Cost = 0.0;
  IntegrateCellGroups<TopoDim>::integrate(
      FunctionalCell_Galerkin( fcnIntegrand, Cost ),
      xfld_, cfld_, quadratureOrderVec.data(), quadratureOrderVec.size() );

  return Cost;
}

//=============================================================================
template <class PhysDim, class TopoDim>
void
CostModel_LogEuclidean<PhysDim, TopoDim>::computeCost_S(
    const Real nDOF0, const MatrixSymFieldType& Sfld, Real& Cost, std::vector<MatrixSym>& dCost_dS) const
{
  typedef IntegrandCell_CostModel_LogEuclidean<PhysDim> IntegrandClass;

  const int nNode = implied_metric_.nDOF();

  SANS_ASSERT( (Sfld.nDOF() == (int)dCost_dS.size()) || (dCost_dS.size() == 0) );
  SANS_ASSERT( Sfld.nDOF() == nNode );

  for (int node = 0; node < nNode; node++)
  {
    //Get nodal metric determinant
    Real detM = DLA::Det(implied_metric_.DOF(node));
    SANS_ASSERT_MSG( detM > 0.0 , "negative nodal metric! detM = %e", detM );

    //Get nodal step matrix trace
    Real traceS = DLA::tr(Sfld.DOF(node));

    cfld_.DOF(node) = log(detM) + traceS;
  }

  IntegrandClass fcnIntegrand(nDOF0, cellgroup_list_);
  const int nCellGroups = xfld_.nCellGroups();
  SANS_ASSERT(nCellGroups > 0);

  std::vector<int> quadratureOrderVec(nCellGroups, quadratureOrder_);

  // Compute the cost
  Cost = 0.0;
  IntegrateCellGroups<TopoDim>::integrate(
      FunctionalCell_Galerkin( fcnIntegrand, Cost ),
      xfld_, cfld_, quadratureOrderVec.data(), quadratureOrderVec.size() );

  if (dCost_dS.size() > 0)
  {
    SANS_ASSERT(cfld_.nDOFpossessed() == implied_metric_.nDOFpossessed());
    const int nNodePossessed = implied_metric_.nDOFpossessed();

    //Compute the derivative of the cost wrt DOFs in cfld
    DLA::VectorD<Real> dCost_dc(nNodePossessed, 0.0);

    IntegrateCellGroups<TopoDim>::integrate(
        JacobianFunctionalCell_Galerkin( fcnIntegrand, dCost_dc ),
        xfld_, cfld_, quadratureOrderVec.data(), quadratureOrderVec.size() );

    //Chain rule: d(Cost)/d(S[node](i,j)) = d(Cost)/d(c[node]) * d(c[node])/d(S[node](i,j))
    //From expression of c : dc/d(S(i,j)) = Identity
    for (int node = 0; node < nNodePossessed; node++)
    {
      dCost_dS[node] = 0.0;
      for (int d = 0; d < D; d++)
        dCost_dS[node](d,d) = dCost_dc[node];
    }
  }
}

//=============================================================================
template <class PhysDim, class TopoDim>
void
CostModel_LogEuclidean<PhysDim, TopoDim>::computeCost_logM(
    const Real nDOF0, const MatrixSymFieldType& logMfld, Real& Cost, std::vector<MatrixSym>& dCost_dlogM) const
{
  typedef IntegrandCell_CostModel_LogEuclidean<PhysDim> IntegrandClass;

  const int nNode = implied_metric_.nDOF();

  SANS_ASSERT( (logMfld.nDOF() == (int)dCost_dlogM.size()) || (dCost_dlogM.size() == 0) );
  SANS_ASSERT( logMfld.nDOF() == nNode );
  SANS_ASSERT( cfld_.nDOF() == nNode );

  for (int node = 0; node < nNode; node++)
    cfld_.DOF(node) = DLA::tr(logMfld.DOF(node));

  IntegrandClass fcnIntegrand(nDOF0, cellgroup_list_);
  const int nCellGroups = xfld_.nCellGroups();
  SANS_ASSERT(nCellGroups > 0);

  std::vector<int> quadratureOrderVec(nCellGroups, quadratureOrder_);

  // Compute the cost
  Cost = 0.0;
  IntegrateCellGroups<TopoDim>::integrate(
      FunctionalCell_Galerkin( fcnIntegrand, Cost ),
      xfld_, cfld_, quadratureOrderVec.data(), quadratureOrderVec.size() );

  if (dCost_dlogM.size() > 0)
  {
    SANS_ASSERT(cfld_.nDOFpossessed() == implied_metric_.nDOFpossessed());
    const int nNodePossessed = implied_metric_.nDOFpossessed();

    //Compute the derivative of the cost wrt DOFs in cfld
    DLA::VectorD<Real> dCost_dc(nNodePossessed, 0.0);

    IntegrateCellGroups<TopoDim>::integrate(
        JacobianFunctionalCell_Galerkin( fcnIntegrand, dCost_dc ),
        xfld_, cfld_, quadratureOrderVec.data(), quadratureOrderVec.size() );

    //Chain rule: d(Cost)/d(logM[node](i,j)) = d(Cost)/d(c[node]) * d(c[node])/d(logM[node](i,j))
    //From expression of c : dc/d(logM(i,j)) = Identity
    for (int node = 0; node < nNodePossessed; node++)
    {
      dCost_dlogM[node] = 0.0;
      for (int d = 0; d < D; d++)
        dCost_dlogM[node](d,d) = dCost_dc[node];
    }
  }
}

}
