// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef MOESS_SANS_OPTIMIZER_BASE_H_
#define MOESS_SANS_OPTIMIZER_BASE_H_

#include <ostream>
#include <vector>
#include <memory> // std::shared_ptr

#include "tools/SANSnumerics.h"     // Real
#include "Topology/Dimension.h"
#include "Topology/ElementTopology.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"

#include "Adaptation/MOESS/NodalMetrics.h"
#include "Adaptation/MOESS/ErrorModel.h"

namespace SANS
{

template <class PhysDim, class TopoDim>
class MOESS_SANS_Optimizer_Base
{
public:
  static const int D = PhysDim::D;   // physical dimensions

  typedef NodalMetrics<PhysDim,TopoDim> NodalMetricsType;
  typedef typename NodalMetricsType::ParamsType ParamsType;
  typedef typename NodalMetricsType::MatrixSymFieldType MatrixSymFieldType;

  typedef ErrorModel<PhysDim,TopoDim> ErrorModelType;

  typedef DLA::MatrixSymS<D,Real> MatrixSym;

  MOESS_SANS_Optimizer_Base( const Real targetCost,
                             std::shared_ptr<ErrorModelType>& errorModel );
  ~MOESS_SANS_Optimizer_Base();

  std::shared_ptr<NodalMetricsType> nodalMetrics() const { return nodalMetrics_; }

  Real eval_count() const { return eval_count_; }
  Real cost_initial_estimate() const { return cost_initial_estimate_; }
  Real cost_final_estimate() const { return cost_final_estimate_; }
  Real objective_reduction_ratio() const { return objective_reduction_ratio_; }

  Real computeAverageFrobeniusNorm( const MatrixSymFieldType& Sfld) const;

protected:
  //Computes the objective function and its derivative wrt optimization variables
  Real computeObjective(const MatrixSymFieldType& Sfld, const int nDim, Real* grad);

  //Computes the cost constraint and its derivative wrt optimization variables
  Real computeCostConstraint(const MatrixSymFieldType& Sfld, const int nDim, Real* grad);

  //Adds a squared Frobenius norm of the step matrix penalty term for each node
  void addFrobNormPenalty(const MatrixSymFieldType& Sfld, Real& penalty, std::vector<MatrixSym>& dpenalty_dSvec);
  template <class T>
  void frobNormPenalty(const DLA::MatrixSymS<D,T>& S, const Real frobNormSqTargetInner, const Real frobNormSqTargetDel, T& penalty) const;

  //Adds a squared trace of the step matrix penalty term for each node
  void addTracePenalty(const MatrixSymFieldType& Sfld, Real& penalty, std::vector<MatrixSym>& dpenalty_dSvec);
  template <class T>
  void tracePenalty(const DLA::MatrixSymS<D,T>& S, const Real sqTargetInner, const Real sqTargetDel, T& penalty) const;

  //Adds an Eigen value of the step matrix penalty term for each node
  void addEigenValuePenalty(const MatrixSymFieldType& Sfld, Real& penalty, std::vector<MatrixSym>& dpenalty_dSvec);
  template <class T>
  void eigenValuePenalty(const int node, const DLA::MatrixSymS<D,T>& S, T& penalty) const;

  //Check resulting step-matrices to see if eigenvalues are above bounds
  int countStepMatrixViolations(const MatrixSymFieldType& Sfld) const;

  Real targetCost_;
  std::shared_ptr<ErrorModelType> errorModel_;
  std::shared_ptr<NodalMetricsType> nodalMetrics_;

  Real error0_; //initial error scaling applied to error estimate function
  Real h_refine_factor_; // maximum refinement factor

  int eval_count_; //No. of objective function evaluations

  Real cost_initial_estimate_ = 0.0;
  Real cost_final_estimate_ = 0.0;

  std::vector<Real> h_coarsen_factors_;  //maximum allowed coarsening factors penalized in the egien value penalty term

  std::vector<Real> objective_history_; //History of objective function evaluations
  std::vector<Real> objective_gradnorm_history_; //History of the norm of the objective function gradient
  Real objective_reduction_ratio_ = 0.0; //Reduction in objective function value as a result of optimization = (final_obj/initial_obj)

  std::vector<Real> cost_constraint_history_; //History of cost constraint evaluations
  std::vector<Real> cost_constraint_gradnorm_history_; //History of cost constraint evaluations

  std::vector<Real> step_constraint_history_; //History of step matrix constraint evaluations
  std::vector<Real> step_constraint_gradnorm_history_; //History of step matrix constraint evaluations

  std::vector<Real> gradation_constraint_history_; //History of gradation constraint evaluations
};

}

#endif /* MOESS_SANS_OPTIMIZER_BASE_H_ */
