// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(MOESS_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "Adaptation/MOESS/MOESS.h"

#include <array>
#include <ostream>
#include <fstream>
#include <limits>
#include <iomanip> // std::setprecision

#include <boost/version.hpp>

#include "tools/smoothmath.h"
#include "tools/timer.h"
#include "tools/output_std_vector.h"

#include "Field/Field.h"

#include "Field/Field_NodalView.h"

#include "Field/tools/for_each_CellGroup.h"

#include "Meshing/Metric/MetricOps.h"

#include "Adaptation/MOESS/NLOPT/MOESS_SANS_Optimizer_NLOPT.h"
#include "Adaptation/MOESS/MMA/MOESS_SANS_Optimizer_MMA.h"
#include "Adaptation/MOESS/NLOPT/MOESS_PX_Optimizer_NLOPT.h"
//#include "Adaptation/MOESS/NLOPT/MOESS_Gradation_Optimizer_NLOPT.h"
#ifdef SANS_PETSC
#include "Adaptation/MOESS/TAO/MOESS_SANS_Optimizer_TAO.h"
#endif

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_MPI
#include "tools/plus_std_vector.h"
#include "MPI/operations.h"

#include <boost/mpi/collectives/all_reduce.hpp>
#include <boost/serialization/vector.hpp>

#endif // SANS_MPI

namespace SANS
{

//===========================================================================//
template <class PhysDim, class TopoDim>
MOESS<PhysDim, TopoDim>::MOESS(const XField<PhysDim,TopoDim>& xfld_linear,
                               const std::vector<int>& cellgroup_list,
                               const SolverInterfaceBase<PhysDim,TopoDim>& problem,
                               const Real targetCost,
                               const PyDict& paramsDict) :
  MOESS(xfld_linear, xfld_linear, cellgroup_list, problem, targetCost, paramsDict) {}

//===========================================================================//
template <class PhysDim, class TopoDim>
MOESS<PhysDim, TopoDim>::MOESS(const XField<PhysDim,TopoDim>& xfld_linear,
                               const XField<PhysDim,TopoDim>& xfld_curved,
                               const std::vector<int>& cellgroup_list,
                               const SolverInterfaceBase<PhysDim,TopoDim>& problem,
                               const Real targetCost,
                               const PyDict& paramsDict)
: xfld_linear_(xfld_linear),
  xfld_curved_(xfld_curved),
  cellgroup_list_(cellgroup_list),
  problem_(problem),
  spaceType_(problem.spaceType()),
  targetCost_(targetCost),
  paramsDict_(paramsDict),
  verbosity_(paramsDict_.get(ParamsType::params.Verbosity))
{
  cost_initial_true_ = problem.getCost();

  // count the number of elements possessed by this processor
  for (int group = 0; group < xfld_linear_.nCellGroups(); group++)
    for (int elem = 0; elem < xfld_linear_.nElem(); elem++ )
      if (xfld_linear_.template getCellGroupGlobal<typename Simplex<TopoDim>::type>(group).associativity(elem).rank()
        == xfld_linear_.comm()->rank())
        nElemInitial_++;

#ifdef SANS_MPI
  int nElemtotal = 0;
  boost::mpi::reduce(*xfld_linear_.comm(), nElemInitial_, nElemtotal, std::plus<int>(), 0 );
  nElemInitial_ = nElemtotal; // only processor 0 actually will use this info, so only need a reduce
#endif


  //xfld_linear_ should be linear (i.e. Q1), and is used for implied metric and cost calculations etc.
  //xfld_curved_ can be of higher-order, and this will be used for the local solves.
  for (int i = 0; i < xfld_linear_.nCellGroups(); i++)
    SANS_ASSERT( xfld_linear_.getCellGroupBase(i).order() == 1 );

  // construct a parallel error model and perform the synthesis
  errorModel_ = std::make_shared<ErrorModel<PhysDim,TopoDim>>(xfld_linear, cellgroup_list, problem, paramsDict);
  if (paramsDict_.get(ParamsType::params.MetricOptimization) == ParamsType::params.MetricOptimization.None)
  {
    // Don't optimize, just use implied metric
    nodalMetrics_ = std::make_shared<NodalMetrics<PhysDim,TopoDim>>(xfld_linear, cellgroup_list,
                                                                    problem, paramsDict);
  }
  else
  {
    errorModel_->synthesize(xfld_curved_);
  }

  //Compute maximum length of an edge in the domain (based on bounding box)
  computeMaxDomainSize();

  timer clock;

  std::string Optimization = paramsDict_.get(ParamsType::params.MetricOptimization);
  if (Optimization == ParamsType::params.MetricOptimization.FrobNorm)
  {
    MOESS_PX_Optimizer<PhysDim,TopoDim,NLOPT> optimizer(targetCost_, h_domain_max_,
                                                        errorModel_,
                                                        xfld_linear, cellgroup_list, problem, paramsDict);

    nodalMetrics_ = optimizer.nodalMetrics();
    eval_count_ = optimizer.eval_count();
    cost_initial_estimate_ = optimizer.cost_initial_estimate();
    cost_final_estimate_ = optimizer.cost_final_estimate();
    objective_reduction_ratio_ = optimizer.objective_reduction_ratio();
  }
#if 0
  else if (Optimization == ParamsType::params.MetricOptimization.Gradation)
  {
    MOESS_Gradation_Optimizer<PhysDim,TopoDim, NLOPT> optimizer(targetCost_, nodalMetrics_, errorModel_, paramsDict);
    optimizer.optimize(Svec);
  }
#endif
  else if (Optimization == ParamsType::params.MetricOptimization.SANS)
  {
    MOESS_SANS_Optimizer<PhysDim,TopoDim,NLOPT> optimizer(targetCost_, h_domain_max_,
                                                          errorModel_,
                                                          xfld_linear, cellgroup_list, problem, paramsDict);

    nodalMetrics_ = optimizer.nodalMetrics();
    eval_count_ = optimizer.eval_count();
    cost_initial_estimate_ = optimizer.cost_initial_estimate();
    cost_final_estimate_ = optimizer.cost_final_estimate();
    objective_reduction_ratio_ = optimizer.objective_reduction_ratio();
  }
  else if (Optimization == ParamsType::params.MetricOptimization.SANSparallel)
  {
    MOESS_SANS_Optimizer<PhysDim,TopoDim,MMA> optimizer(targetCost_, h_domain_max_,
                                                        errorModel_,
                                                        xfld_linear, cellgroup_list, problem, paramsDict);

    nodalMetrics_ = optimizer.nodalMetrics();
    eval_count_ = optimizer.eval_count();
    cost_initial_estimate_ = optimizer.cost_initial_estimate();
    cost_final_estimate_ = optimizer.cost_final_estimate();
    objective_reduction_ratio_ = optimizer.objective_reduction_ratio();
  }
  else if (Optimization == ParamsType::params.MetricOptimization.None)
  {
    // Do Nothing
  }
  else
    SANS_DEVELOPER_EXCEPTION( "Unknown Optimization. "); // Should never get here if checkInputs was called

  if (xfld_linear_.comm()->rank() == 0)
  {
    Real optimization_time = clock.elapsed();
    if (verbosity_ >= ParamsType::VerbosityOptions::Progressbar )
      std::cout << "MOESS optimization time: " << std::fixed << std::setprecision(3) << optimization_time << "s" << std::endl<<std::endl;

    //Predict the cost of the next mesh
    if (verbosity_ >= ParamsType::VerbosityOptions::Progressbar )
    {
      std::cout.unsetf(std::ios_base::floatfield);
      std::cout << "Predicted cost estimate = " << std::scientific << std::setprecision(3) << cost_final_estimate_ << std::endl;
    }
  }
}

//===========================================================================//
template <class PhysDim, class TopoDim>
void
MOESS<PhysDim, TopoDim>::
computeError(const MatrixSymFieldType& Sfld, Real& Error,
             std::vector<MatrixSym>& dError_dS)
{
  errorModel_->computeError(Sfld, Error, dError_dS);
}

#if 0 //def SANS_PETSC
//===========================================================================//
template <class PhysDim, class TopoDim>
void
MOESS<PhysDim, TopoDim>::
computeErrorHessian(const std::vector<MatrixSym>& Svec, Mat H)
{
  errorModel_->computeErrorHessian(Svec, H);
}
#endif

//===========================================================================//
template <class PhysDim, class TopoDim>
void
MOESS<PhysDim, TopoDim>::
getNodalMetricRequestField( MatrixSymFieldType& metric_fld )
{
  nodalMetrics_->getNodalMetricRequestField(metric_fld);
}


template <class PhysDim, class TopoDim>
void
MOESS<PhysDim, TopoDim>::
getNodalImpliedMetricField( MatrixSymFieldType& metric_fld )
{
  nodalMetrics_->getNodalImpliedMetricField(metric_fld);
}


//===========================================================================//
template <class PhysDim, class TopoDim>
void
MOESS<PhysDim, TopoDim>::
getStepMatrixField(MatrixSymFieldType_Nodal& step_fld)
{
  nodalMetrics_->getStepMatrixField(step_fld);
}

//===========================================================================//
template <class PhysDim, class TopoDim>
void
MOESS<PhysDim, TopoDim>::
getStepMatrixEigField(VectorFieldType_Nodal& eig_fld)
{
  nodalMetrics_->getStepMatrixEigField(eig_fld);
}

//===========================================================================//
template <class PhysDim, class TopoDim>
void
MOESS<PhysDim, TopoDim>::
getEdgeLengthField(ScalarFieldType_Nodal& edgeLen_fld)
{
  nodalMetrics_->getEdgeLengthField(edgeLen_fld);
}

//===========================================================================//
template <class PhysDim, class TopoDim>
void
MOESS<PhysDim, TopoDim>::
getEdgeLengths(std::vector<Real>& lengths)
{
  nodalMetrics_->getEdgeLengths(lengths);
}

//===========================================================================//
template <class PhysDim, class TopoDim>
void
MOESS<PhysDim, TopoDim>::
getRateMatrixField(MatrixSymFieldType_Elemental& rate_fld)
{
  errorModel_->getRateMatrixField(rate_fld);
}

//===========================================================================//
template <class PhysDim, class TopoDim>
void
MOESS<PhysDim, TopoDim>::
getRateMatrixField(MatrixSymFieldType_Nodal& rate_fld)
{
  errorModel_->getRateMatrixField(rate_fld);
}

//===========================================================================//
template <class PhysDim, class TopoDim>
void
MOESS<PhysDim, TopoDim>::computeMaxDomainSize()
{
  std::vector<Real> min_coords(D, std::numeric_limits<Real>::max());
  std::vector<Real> max_coords(D, std::numeric_limits<Real>::min());

  for (int i = 0; i < xfld_curved_.nDOF(); i++)
  {
    for (int d = 0; d < D; d++)
    {
      min_coords[d] = min(min_coords[d], xfld_curved_.DOF(i)[d]);
      max_coords[d] = max(max_coords[d], xfld_curved_.DOF(i)[d]);
    }
  }

#ifdef SANS_MPI
  // reduce the minimum and maximum coordinate across all processors
  min_coords = boost::mpi::all_reduce(*xfld_curved_.comm(), min_coords, boost::mpi::minimum<std::vector<Real>>());
  max_coords = boost::mpi::all_reduce(*xfld_curved_.comm(), max_coords, boost::mpi::maximum<std::vector<Real>>());
#endif

  h_domain_max_ = 0;
  for (int d = 0; d < D; d++)
  {
    h_domain_max_ = max(h_domain_max_, max_coords[d] - min_coords[d]);
  }

  if (verbosity_ >= ParamsType::VerbosityOptions::Progressbar && xfld_curved_.comm()->rank() == 0)
    std::cout << "Maximum h in domain = " << h_domain_max_ << std::endl;
}

//===========================================================================//
template <class PhysDim, class TopoDim>
void
MOESS<PhysDim, TopoDim>::printAdaptInfo(std::fstream& f, const int& adapt_iter, const Real& errInd, const Real& errEst,
                                        const Real& errTrue, const Real& output)
{
  if (xfld_linear_.comm()->rank() == 0)
  {
    if (adapt_iter == 0)
    {
      f << std::setw(4)  << "Iter"
          << std::setw(12) << "DOFTrue"
          << std::setw(16) << "DOFEstimate"
          << std::setw(16) << "DOFPrediction"
          << std::setw(20) << "ErrorIndicator"
          << std::setw(20) << "ErrorEstimate"
          << std::setw(20) << "ErrorTrue"
          << std::setw(22) << "Output"
          << std::setw(11) << "EvalCount"
          << std::setw(15) << "ObjReduction"
          << std::setw(15) << "Elements"
          << std::endl;
    }

    f << std::setw(4)  << adapt_iter
      << std::setw(12) << (int) cost_initial_true_
      << std::setw(16) << std::setprecision(3) << std::fixed << cost_initial_estimate_
      << std::setw(16) << std::setprecision(3) << std::fixed << cost_final_estimate_
      << std::setw(20) << std::setprecision(10) << std::scientific << errInd
      << std::setw(20) << std::setprecision(10) << std::scientific << errEst
      << std::setw(20) << std::setprecision(10) << std::scientific << errTrue
      << std::setw(22) << std::setprecision(12) << std::scientific << output
      << std::setw(11) << eval_count_
      << std::setw(15) << std::setprecision(4) << std::scientific << objective_reduction_ratio_
      << std::setw(15) << nElemInitial_
      << std::endl;
  }
}

//===========================================================================//
template <class PhysDim, class TopoDim>
void
MOESS<PhysDim, TopoDim>::printOptimizationInfo(std::fstream& f)
{
  if (xfld_linear_.comm()->rank() == 0)
  {
    f << std::setw(20) << "VARIABLES="
      << std::setw(20) << "Iter"
      << std::setw(20) << "ObjectiveVal"
      << std::setw(20) << "ObjGradNorm"
      << std::setw(20) << "CostConstraintVal"
      << std::setw(30) << "CostConstraintGradNorm";
    if (step_constraint_history_.size() > 0)
    f << std::setw(20) << "StepConstraintVal"
      << std::setw(30) << "StepConstraintGradNorm";
    f << std::endl;

    for (int i = 0; i < (int) objective_history_.size(); i++)
    {
      f << i
        << std::setw(20) << std::setprecision(5) << std::scientific << objective_history_[i]
        << std::setw(20) << std::setprecision(5) << std::scientific << objective_gradnorm_history_[i]
        << std::setw(20) << std::setprecision(5) << std::scientific << cost_constraint_history_[i]
        << std::setw(30) << std::setprecision(5) << std::scientific << cost_constraint_gradnorm_history_[i];
      if (step_constraint_history_.size() > 0)
      f << std::setw(20) << std::setprecision(5) << std::scientific << step_constraint_history_[i]
        << std::setw(30) << std::setprecision(5) << std::scientific << step_constraint_gradnorm_history_[i];
      f << std::endl;
    }
  }
}

}
