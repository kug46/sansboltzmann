// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define MOESS_INSTANTIATE
#include "MOESS_impl.h"

#include "Field/FieldArea_CG_Cell.h"

#include "Field/FieldArea_DG_Cell.h"

#include "Field/XFieldArea.h"

namespace SANS
{

//===========================================================================//
//Explicit instantiations
template class MOESS<PhysD2,TopoD2>;

}
