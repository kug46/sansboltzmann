// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PARAMFIELDBUILDER_H_
#define PARAMFIELDBUILDER_H_

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "tools/Tuple.h"

#include "Field/Tuple/FieldTuple.h"

#include "Field/Local/XField_Local_Base.h"
#include "Field/Local/Field_Local.h"

#include "Field/DistanceFunction/DistanceFunction.h"

#include "Field/HField/GenHField_CG.h"

#include "Field/HField/GenHField_DG.h"

namespace SANS
{

class ParamType_None {};
class ParamType_Distance {};
class ParamType_DistanceGradients {};
class ParamType_GenH_CG {};
class ParamType_GenH_DG {};
class ParamType_GenH_CG_Distance {};
class ParamType_GenH_DG_Distance {};


template<class Type, class PhysD, class TopoD, class QFieldType>
struct ParamFieldBuilder;
template<class Type, class PhysD, class TopoD, class QFieldType>
struct ParamFieldBuilder_Local;

/*--------------------------------------------------------------------------------------*/

template<class PhysD, class TopoD, class QFieldType>
struct ParamFieldBuilder<ParamType_None, PhysD, TopoD, QFieldType>
{
  typedef PhysD PhysDim;
  typedef TopoD TopoDim;

  typedef XField<PhysDim, TopoDim> FieldType;

  ParamFieldBuilder(const XField<PhysDim, TopoDim>& xfld, const QFieldType& qfld,
                    const PyDict& d)
  : dict(d), fld(xfld) {}

  ParamFieldBuilder(const ParamFieldBuilder& paramfld, const QFieldType& qfld,
                    const PyDict& d)
  : dict(d), fld(paramfld.fld) {}

  const PyDict& dict;
  const FieldType& fld;
};

template<class PhysD, class TopoD, class QFieldType>
struct ParamFieldBuilder_Local<ParamType_None, PhysD, TopoD, QFieldType>
{
  typedef PhysD PhysDim;
  typedef TopoD TopoDim;

  typedef ParamFieldBuilder<ParamType_None, PhysD, TopoD, QFieldType> ParamFieldBuilderGlobal;

  typedef XField<PhysDim, TopoDim> FieldType;

  ParamFieldBuilder_Local(const XField_Local_Base<PhysDim, TopoDim>& xfld_local,
                          const ParamFieldBuilderGlobal& parambuilder_global, const QFieldType& qfld0)
  : dict(parambuilder_global.dict), fld(xfld_local) {}

  const PyDict& dict;
  const FieldType& fld;
};

/*--------------------------------------------------------------------------------------*/

template<class PhysD, class TopoD, class QFieldType>
struct ParamFieldBuilder<ParamType_Distance, PhysD, TopoD, QFieldType>
{
  typedef PhysD PhysDim;
  typedef TopoD TopoDim;

  typedef typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, Real>,
                                         XField<PhysDim, TopoDim>>::type FieldType;

  typedef Field_CG_Cell<PhysDim,TopoDim,Real> DistFieldType;

  ParamFieldBuilder(const FieldType& flds, const QFieldType& qfld,
                    const PyDict& d)
  : dict(d),
    distfld(static_cast<const DistFieldType&>( get<0>(flds) )),
    order(distfld.getCellGroupBase(0).order()),
    fld(distfld, get<1>(flds))
  {
    for (int i = 1; i < distfld.nCellGroups(); i++)
      SANS_ASSERT(order == distfld.getCellGroupBase(i).order());
  }

  ParamFieldBuilder(const ParamFieldBuilder& paramfld, const QFieldType& qfld,
                    const PyDict& d)
  : dict(d),
    distfld(paramfld.distfld),
    order(paramfld.order),
    fld(paramfld.fld)
  {
    for (int i = 0; i < distfld.nCellGroups(); i++)
      SANS_ASSERT(order == distfld.getCellGroupBase(i).order());
  }

  const PyDict& dict;
  const DistFieldType& distfld;
  const int order;
  const FieldType fld;
};


template<class PhysD, class TopoD, class QFieldType>
struct ParamFieldBuilder_Local<ParamType_Distance, PhysD, TopoD, QFieldType>
{
  typedef PhysD PhysDim;
  typedef TopoD TopoDim;

  typedef typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, Real>,
                                         XField<PhysDim, TopoDim>>::type FieldType;

  typedef Field_CG_Cell<PhysDim,TopoDim,Real> DistFieldType;

  typedef ParamFieldBuilder<ParamType_Distance, PhysD, TopoD, QFieldType> ParamFieldBuilderGlobal;

  ParamFieldBuilder_Local(const XField_Local_Base<PhysDim, TopoDim>& xfld_local,
                          const ParamFieldBuilderGlobal& parambuilder_global, const QFieldType& qfld0)
  : dict(parambuilder_global.dict),
    order(parambuilder_global.order),
    distfld(xfld_local, order, BasisFunctionCategory_Lagrange),
    fld(distfld, xfld_local)
  {
    Field_Local<Field_CG_Cell<PhysDim,TopoDim,Real>> distfld_local(xfld_local, parambuilder_global.distfld, order,
                                                                   BasisFunctionCategory_Lagrange);
    distfld_local.projectTo(distfld);
  }

  const PyDict& dict;
  const int order;
  DistFieldType distfld;
  const FieldType fld;
};


/*--------------------------------------------------------------------------------------*/

template<class PhysD, class TopoD, class QFieldType>
struct ParamFieldBuilder<ParamType_DistanceGradients, PhysD, TopoD, QFieldType>
{
  typedef PhysD PhysDim;
  typedef TopoD TopoDim;

  typedef typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, Real>,
                                         Field<PhysDim, TopoDim, DLA::VectorS<PhysD::D,Real>>,
                                         XField<PhysDim, TopoDim>>::type FieldType;

  typedef Field_CG_Cell<PhysDim,TopoDim,Real> DistFieldType;
  typedef Field_DG_Cell<PhysDim,TopoDim,DLA::VectorS<PhysD::D,Real>> DistGradFieldType;

  ParamFieldBuilder(const FieldType& flds, const QFieldType& qfld,
                    const PyDict& d)
  : dict(d),
    distfld(static_cast<const DistFieldType&>( get<0>(flds) )),
    distgradfld(static_cast<const DistGradFieldType&>( get<1>(flds) )),
    order(distfld.getCellGroupBase(0).order()),
    fld(distfld, distgradfld, get<2>(flds))
  {
    for (int i = 1; i < distfld.nCellGroups(); i++)
      SANS_ASSERT(order == distfld.getCellGroupBase(i).order());
  }

  ParamFieldBuilder(const ParamFieldBuilder& paramfld, const QFieldType& qfld,
                    const PyDict& d)
  : dict(d),
    distfld(paramfld.distfld),
    distgradfld(paramfld.distgradfld),
    order(paramfld.order),
    fld(paramfld.fld)
  {
    for (int i = 0; i < distfld.nCellGroups(); i++)
      SANS_ASSERT(order == distfld.getCellGroupBase(i).order());
  }

  const PyDict& dict;
  const DistFieldType& distfld;
  const DistGradFieldType& distgradfld;
  const int order;
  const FieldType fld;
};


template<class PhysD, class TopoD, class QFieldType>
struct ParamFieldBuilder_Local<ParamType_DistanceGradients, PhysD, TopoD, QFieldType>
{
  typedef PhysD PhysDim;
  typedef TopoD TopoDim;

  typedef typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, Real>,
                                         Field<PhysDim, TopoDim, DLA::VectorS<PhysD::D,Real>>,
                                         XField<PhysDim, TopoDim>>::type FieldType;

  typedef Field_CG_Cell<PhysDim,TopoDim,Real> DistFieldType;
  typedef Field_DG_Cell<PhysDim,TopoDim,DLA::VectorS<PhysD::D,Real>> DistGradFieldType;

  typedef ParamFieldBuilder<ParamType_DistanceGradients, PhysD, TopoD, QFieldType> ParamFieldBuilderGlobal;

  ParamFieldBuilder_Local(const XField_Local_Base<PhysDim, TopoDim>& xfld_local,
                          const ParamFieldBuilderGlobal& parambuilder_global, const QFieldType& qfld0)
  : dict(parambuilder_global.dict),
    order(parambuilder_global.order),
    distfld(xfld_local, order, BasisFunctionCategory_Lagrange),
    distgradfld(xfld_local, order, BasisFunctionCategory_Lagrange),
    fld(distfld, xfld_local)
  {
    Field_Local<Field_CG_Cell<PhysDim,TopoDim,Real>> distfld_local(xfld_local, parambuilder_global.distfld, order,
                                                                   BasisFunctionCategory_Lagrange);

    Field_Local<Field_DG_Cell<PhysDim,TopoDim,DLA::VectorS<PhysD::D,Real>>>
       distgradfld_local(xfld_local, parambuilder_global.distfld, order, BasisFunctionCategory_Lagrange);

    distfld_local.projectTo(distfld);
    distgradfld_local.projectTo(distgradfld);
  }

  const PyDict& dict;
  const int order;
  DistFieldType distfld;
  DistGradFieldType distgradfld;
  const FieldType fld;
};


/*--------------------------------------------------------------------------------------*/
//ParamFieldBuilder for creating a CG generalized h-tensor field

template<class PhysD, class TopoD, class QFieldType>
struct ParamFieldBuilder<ParamType_GenH_CG, PhysD, TopoD, QFieldType>
{
  typedef PhysD PhysDim;
  typedef TopoD TopoDim;

  typedef typename DLA::MatrixSymS<PhysDim::D,Real> HType;

  typedef GenHField_CG<PhysDim, TopoDim> HFieldType;

  typedef typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, HType>,
                                         XField<PhysDim, TopoDim>>::type FieldType;

  ParamFieldBuilder(const FieldType& flds, const QFieldType& qfld,
                    const PyDict& d)
  : dict(d),
    hfld(static_cast<const HFieldType&>( get<0>(flds) )),
    fld(hfld, get<1>(flds))
  {
    SANS_ASSERT( &hfld.getXField() == &qfld.getXField() );
  }

  ParamFieldBuilder(const ParamFieldBuilder& paramfld, const QFieldType& qfld,
                    const PyDict& d)
  : dict(d),
    hfld(paramfld.hfld),
    fld(paramfld.fld)
  {
    SANS_ASSERT( &hfld.getXField() == &qfld.getXField() );
  }

  const PyDict& dict;
  const HFieldType& hfld;
  const FieldType fld;
};

template<class PhysD, class TopoD, class QFieldType>
struct ParamFieldBuilder_Local<ParamType_GenH_CG, PhysD, TopoD, QFieldType>
{
  typedef PhysD PhysDim;
  typedef TopoD TopoDim;

  typedef typename DLA::MatrixSymS<PhysDim::D,Real> HType;

  typedef GenHField_CG<PhysDim, TopoDim> HFieldType;

  typedef typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, HType>,
                                         XField<PhysDim, TopoDim>>::type FieldType;

  typedef ParamFieldBuilder<ParamType_GenH_CG, PhysD, TopoD, QFieldType> ParamFieldBuilderGlobal;

  ParamFieldBuilder_Local(const XField_Local_Base<PhysDim, TopoDim>& xfld_local,
                          const ParamFieldBuilderGlobal& parambuilder_global, const QFieldType& qfld0)
  : dict(parambuilder_global.dict),
    hfld(xfld_local, parambuilder_global.hfld),
    fld(hfld, xfld_local)
  {
    SANS_ASSERT( &xfld_local == &qfld0.getXField() );
  }

  const PyDict& dict;
  HFieldType hfld;
  const FieldType fld;
};

/*--------------------------------------------------------------------------------------*/
//ParamFieldBuilder for creating a DG generalized h-tensor field

template<class PhysD, class TopoD, class QFieldType>
struct ParamFieldBuilder<ParamType_GenH_DG, PhysD, TopoD, QFieldType>
{
  typedef PhysD PhysDim;
  typedef TopoD TopoDim;

  typedef typename DLA::MatrixSymS<PhysDim::D,Real> HType;

  typedef GenHField_DG<PhysDim, TopoDim> HFieldType;

  typedef typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, HType>,
                                         XField<PhysDim, TopoDim>>::type FieldType;

  ParamFieldBuilder(const FieldType& flds, const QFieldType& qfld,
                    const PyDict& d)
  : dict(d),
    hfld(static_cast<const HFieldType&>( get<0>(flds) )),
    fld(hfld, get<1>(flds))
  {
    SANS_ASSERT( &hfld.getXField() == &qfld.getXField() );
  }

  ParamFieldBuilder(const ParamFieldBuilder& paramfld, const QFieldType& qfld,
                    const PyDict& d)
  : dict(d),
    hfld(paramfld.hfld),
    fld(paramfld.fld)
  {
    SANS_ASSERT( &hfld.getXField() == &qfld.getXField() );
  }

  const PyDict& dict;
  const HFieldType& hfld;
  const FieldType fld;
};

template<class PhysD, class TopoD, class QFieldType>
struct ParamFieldBuilder_Local<ParamType_GenH_DG, PhysD, TopoD, QFieldType>
{
  typedef PhysD PhysDim;
  typedef TopoD TopoDim;

  typedef typename DLA::MatrixSymS<PhysDim::D,Real> HType;

  typedef GenHField_DG<PhysDim, TopoDim> HFieldType;

  typedef typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, HType>,
                                         XField<PhysDim, TopoDim>>::type FieldType;

  typedef ParamFieldBuilder<ParamType_GenH_DG, PhysD, TopoD, QFieldType> ParamFieldBuilderGlobal;

  ParamFieldBuilder_Local(const XField_Local_Base<PhysDim, TopoDim>& xfld_local,
                          const ParamFieldBuilderGlobal& parambuilder_global, const QFieldType& qfld0)
  : dict(parambuilder_global.dict),
    hfld(xfld_local, parambuilder_global.hfld),
    fld(hfld, xfld_local)
  {
    SANS_ASSERT( &xfld_local == &qfld0.getXField() );
  }

  const PyDict& dict;
  HFieldType hfld;
  const FieldType fld;
};

/*--------------------------------------------------------------------------------------*/
//ParamFieldBuilder for CG generalized h-tensor + distance field

template<class PhysD, class TopoD, class QFieldType>
struct ParamFieldBuilder<ParamType_GenH_CG_Distance, PhysD, TopoD, QFieldType>
{
  typedef PhysD PhysDim;
  typedef TopoD TopoDim;

  typedef typename DLA::MatrixSymS<PhysDim::D,Real> HType;

  typedef GenHField_CG<PhysDim, TopoDim> HFieldType;
  typedef Field_CG_Cell<PhysDim,TopoDim,Real> DistFieldType;

  typedef typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, HType>,
                                         Field<PhysDim, TopoDim, Real>,
                                         XField<PhysDim, TopoDim>>::type FieldType;

  ParamFieldBuilder(const FieldType& flds, const QFieldType& qfld,
                    const PyDict& d)
  : dict(d),
    hfld(static_cast<const HFieldType&>( get<0>(flds) )),
    distfld(static_cast<const DistFieldType&>( get<1>(flds) )),
    order(distfld.getCellGroupBase(0).order()),
    fld((hfld, distfld), get<2>(flds))
  {
    SANS_ASSERT( &hfld.getXField() == &qfld.getXField() );

    for (int i = 1; i < distfld.nCellGroups(); i++)
      SANS_ASSERT(order == distfld.getCellGroupBase(i).order());
  }

  ParamFieldBuilder(const ParamFieldBuilder& paramfld, const QFieldType& qfld,
                    const PyDict& d)
  : dict(d),
    hfld(paramfld.hfld),
    distfld(paramfld.distfld),
    order(paramfld.order),
    fld(paramfld.fld)
  {
    SANS_ASSERT( &hfld.getXField() == &qfld.getXField() );

    for (int i = 0; i < distfld.nCellGroups(); i++)
      SANS_ASSERT(order == distfld.getCellGroupBase(i).order());
  }

  const PyDict& dict;
  const HFieldType& hfld;
  const DistFieldType& distfld;
  const int order;
  const FieldType fld;
};

template<class PhysD, class TopoD, class QFieldType>
struct ParamFieldBuilder_Local<ParamType_GenH_CG_Distance, PhysD, TopoD, QFieldType>
{
  typedef PhysD PhysDim;
  typedef TopoD TopoDim;

  typedef typename DLA::MatrixSymS<PhysDim::D,Real> HType;

  typedef GenHField_CG<PhysDim, TopoDim> HFieldType;
  typedef Field_CG_Cell<PhysDim,TopoDim,Real> DistFieldType;

  typedef typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, HType>,
                                         Field<PhysDim, TopoDim, Real>,
                                         XField<PhysDim, TopoDim>>::type FieldType;

  typedef ParamFieldBuilder<ParamType_GenH_CG_Distance, PhysD, TopoD, QFieldType> ParamFieldBuilderGlobal;

  ParamFieldBuilder_Local(const XField_Local_Base<PhysDim, TopoDim>& xfld_local,
                          const ParamFieldBuilderGlobal& parambuilder_global, const QFieldType& qfld0)
  : dict(parambuilder_global.dict),
    hfld(xfld_local, parambuilder_global.hfld),
    order(parambuilder_global.order),
    distfld(xfld_local, order, BasisFunctionCategory_Lagrange),
    fld((hfld, distfld), xfld_local)
  {
    SANS_ASSERT( &xfld_local == &qfld0.getXField() );

    Field_Local<Field_CG_Cell<PhysDim,TopoDim,Real>> distfld_local(xfld_local, parambuilder_global.distfld, order,
                                                                   BasisFunctionCategory_Lagrange);
    distfld_local.projectTo(distfld);
  }

  const PyDict& dict;
  HFieldType hfld;
  const int order;
  DistFieldType distfld;
  const FieldType fld;
};

/*--------------------------------------------------------------------------------------*/
//ParamFieldBuilder for DG generalized h-tensor + distance field

template<class PhysD, class TopoD, class QFieldType>
struct ParamFieldBuilder<ParamType_GenH_DG_Distance, PhysD, TopoD, QFieldType>
{
  typedef PhysD PhysDim;
  typedef TopoD TopoDim;

  typedef typename DLA::MatrixSymS<PhysDim::D,Real> HType;

  typedef GenHField_DG<PhysDim, TopoDim> HFieldType;
  typedef Field_CG_Cell<PhysDim,TopoDim,Real> DistFieldType;

  typedef typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, HType>,
                                         Field<PhysDim, TopoDim, Real>,
                                         XField<PhysDim, TopoDim>>::type FieldType;

  ParamFieldBuilder(const FieldType& flds, const QFieldType& qfld,
                    const PyDict& d)
  : dict(d),
    hfld(static_cast<const HFieldType&>( get<0>(flds) )),
    distfld(static_cast<const DistFieldType&>( get<1>(flds) )),
    order(distfld.getCellGroupBase(0).order()),
    fld((hfld, distfld), get<2>(flds))
  {
    SANS_ASSERT( &hfld.getXField() == &qfld.getXField() );

    for (int i = 1; i < distfld.nCellGroups(); i++)
      SANS_ASSERT(order == distfld.getCellGroupBase(i).order());
  }

  ParamFieldBuilder(const ParamFieldBuilder& paramfld, const QFieldType& qfld,
                    const PyDict& d)
  : dict(d),
    hfld(paramfld.hfld),
    distfld(paramfld.distfld),
    order(paramfld.order),
    fld(paramfld.fld)
  {
    SANS_ASSERT( &hfld.getXField() == &qfld.getXField() );

    for (int i = 0; i < distfld.nCellGroups(); i++)
      SANS_ASSERT(order == distfld.getCellGroupBase(i).order());
  }

  const PyDict& dict;
  const HFieldType& hfld;
  const DistFieldType& distfld;
  const int order;
  const FieldType fld;
};

template<class PhysD, class TopoD, class QFieldType>
struct ParamFieldBuilder_Local<ParamType_GenH_DG_Distance, PhysD, TopoD, QFieldType>
{
  typedef PhysD PhysDim;
  typedef TopoD TopoDim;

  typedef typename DLA::MatrixSymS<PhysDim::D,Real> HType;

  typedef GenHField_DG<PhysDim, TopoDim> HFieldType;
  typedef Field_CG_Cell<PhysDim,TopoDim,Real> DistFieldType;

  typedef typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, HType>,
                                         Field<PhysDim, TopoDim, Real>,
                                         XField<PhysDim, TopoDim>>::type FieldType;

  typedef ParamFieldBuilder<ParamType_GenH_DG_Distance, PhysD, TopoD, QFieldType> ParamFieldBuilderGlobal;

  ParamFieldBuilder_Local(const XField_Local_Base<PhysDim, TopoDim>& xfld_local,
                          const ParamFieldBuilderGlobal& parambuilder_global, const QFieldType& qfld0)
  : dict(parambuilder_global.dict),
    hfld(xfld_local, parambuilder_global.hfld),
    order(parambuilder_global.order),
    distfld(xfld_local, order, BasisFunctionCategory_Lagrange),
    fld((hfld, distfld), xfld_local)
  {
    SANS_ASSERT( &xfld_local == &qfld0.getXField() );

    Field_Local<Field_CG_Cell<PhysDim,TopoDim,Real>> distfld_local(xfld_local, parambuilder_global.distfld, order,
                                                                   BasisFunctionCategory_Lagrange);
    distfld_local.projectTo(distfld);
  }

  const PyDict& dict;
  HFieldType hfld;
  const int order;
  DistFieldType distfld;
  const FieldType fld;
};

}

#endif /* PARAMFIELDBUILDER_H_ */
