// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOLVERINTERFACE_AGLS_LIFTEDBOUNDARY_H_
#define SOLVERINTERFACE_AGLS_LIFTEDBOUNDARY_H_

#include <ostream>
#include <vector>

// Output Functional Stuff that needs to be removed
#include "Discretization/Galerkin/FunctionalBoundaryTrace_Dispatch_Galerkin.h"
#include "Discretization/Galerkin/JacobianFunctionalBoundaryTrace_Dispatch_Galerkin.h"

#include "Discretization/DG/JacobianFunctionalBoundaryTrace_Dispatch_DGBR2.h"
#include "Discretization/DG/FunctionalBoundaryTrace_Dispatch_DGBR2.h"

#include "Discretization/Galerkin/Stabilization_Galerkin.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#include "MPI/communicator_fwd.h"
#endif

// What will eventually be the true Solver Interface. This is a stop gap solution until output functionals work
#include "Adaptation/MOESS/SolverInterface.h"

#include "ErrorEstimate/Galerkin/ErrorEstimate_StrongForm_Galerkin.h"
#include "ErrorEstimate/Galerkin/ErrorEstimateNodal_Galerkin.h"
#include "ErrorEstimate/Galerkin/ErrorEstimateNodal_Galerkin_LiftedBoundary.h"
#include "../../ErrorEstimate/Galerkin/OutputCorrection_Galerkin_Stabilized.h"
#include "ErrorEstimate/DG/ErrorEstimate_DGAdvective.h"

#ifdef BOUNDARYOUTPUT
// HACK: These boundary integrands are needed for the boundary output at the moment.
//       They should be removed in the future.
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_sansLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Flux_mitState_Nitsche.h"
#include "Discretization/DG/IntegrandBoundaryTrace_LinearScalar_sansLG_DGBR2.h"
#include "Discretization/DG/IntegrandBoundaryTrace_Flux_mitState_DGBR2.h"

#include "Discretization/Galerkin/IntegrandBoundaryTrace_None_Galerkin.h"
#endif

#include "Discretization/Galerkin/IntegrandCell_Project.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Project.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin_Stabilized_LiftedBoundary.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Local_Galerkin_Stabilized_LiftedBoundary.h"

namespace SANS
{

template<class SolutionClass, class AlgebraicEqSet, class OutputIntegrandType>
class SolverInterface_AGLS_LiftedBoundary : public SolverInterface<SolutionClass, AlgebraicEqSet, OutputIntegrandType>
{
public:
  typedef SolverInterface<SolutionClass, AlgebraicEqSet, OutputIntegrandType> BaseType;
  typedef typename SolverInterface<SolutionClass, AlgebraicEqSet, OutputIntegrandType>::LocalProblem LocalBaseType;

  typedef typename AlgebraicEqSet::PhysDim PhysDim;
  typedef typename AlgebraicEqSet::TopoDim TopoDim;

  const int D = PhysDim::D;
  typedef XField<PhysDim, TopoDim> XFieldType;

  typedef typename SolutionClass::ParamFieldType ParamFieldType;
  typedef typename SolutionClass::ParamFieldBuilderType ParamFieldBuilderType;

  typedef typename AlgebraicEqSet::NDPDEClass NDPDEClass;
  typedef typename NDPDEClass::template ArrayQ<Real> PDEArrayQ;

  typedef AlgebraicEqSet AlgEqnSet_Global;

  typedef typename LocalEquationSet<AlgEqnSet_Global>::type AlgEqnSet_Local;
  typedef typename AlgEqnSet_Global::FieldBundle FieldBundle;
  typedef typename AlgEqnSet_Global::OutputCorrectionClass OutputCorrectionClass;
  typedef typename AlgEqnSet_Global::ErrorEstimateClass ErrorEstimateClass;

  typedef typename AlgEqnSet_Local::FieldBundle_Local FieldBundle_Local;

  typedef SolverInterface_AGLS_LiftedBoundary SolverInterfaceClass;

  typedef typename OutputIntegrandType::template ArrayJ<Real> ArrayJ;

  typedef typename AlgEqnSet_Global::ArrayQ ArrayQ;

  template< class... BCArgs >
  SolverInterface_AGLS_LiftedBoundary(SolutionClass& sol, const ResidualNormType& resNormType, std::vector<Real>& tol, const int quadOrder,
                        const std::vector<int>& cellgroups, const std::vector<int>& interiortracegroups,
                        PyDict& BCList, const std::map< std::string, std::vector<int> >& BCBoundaryGroups,
                        const PyDict& NonlinearSolverDict, const PyDict& AdjLinearSolverDict,
                        const OutputIntegrandType& fcnOutput, BCArgs&... args )
    : BaseType( sol, resNormType, tol, quadOrder, cellgroups, interiortracegroups, BCList, BCBoundaryGroups, NonlinearSolverDict,
                AdjLinearSolverDict, fcnOutput, args... ),
      adjoint_wo_subtraction( xfld_, sol_.adjoint.order, sol_.adjoint, sol_.mitlg_boundarygroups ),
      adjoint_p( xfld_, sol_.primal.order, sol_.adjoint, sol_.mitlg_boundarygroups ),
      primStabMtx_(StabilizationType::AGLSAdjoint, TauType::Glasby, (sol_.primal.order), 1,
                   sol_.disc.getNitscheOrder(), sol_.disc.getNitscheConstant(D)  ),
      adjStabMtx_(StabilizationType::AGLSAdjoint, TauType::Glasby, (sol_.adjoint.order), 1,
                  sol_.disc.getNitscheOrder(), sol_.disc.getNitscheConstant(D)  )
  {
    /*
    errorIndicator_ = 0.0;
    errorEstimate_ = 0.0;

    //Loop over each cellgroup and find the no. of DOF per cell and solution order for that cellgroup
    nDOFperCell_.resize((int) xfld_.nCellGroups(), -1);
    orderVec_.resize((int) xfld_.nCellGroups(), -1);
    for_each_CellGroup<TopoDim>::apply( CellwiseOperations(cellgroup_list_, *this), xfld_);
    */
  }

  virtual ~SolverInterface_AGLS_LiftedBoundary() {}

  virtual void solveGlobalPrimalProblem() override;
  virtual void solveGlobalAdjointProblem() override;

//  virtual void computeErrorEstimates() override;
//  virtual Real getElementalErrorEstimate(int cellgroup, int elem) const override;
//  virtual Real getGlobalErrorIndicator() const override;
//  virtual Real getGlobalErrorEstimate() const override;
//
//  virtual void output_EField(const std::string& filename) const override;
//

//
//  virtual Real getnDOFperCell(const int cellgroup) const override;
//  virtual int getSolutionOrder(const int cellgroup) const override;
//
//  virtual Real getOutput() const override;
//
//  SolverInterface_impl<SolverInterfaceClass>
//  CellwiseOperations(const std::vector<int>& cellgroup_list, SolverInterfaceClass& base );

//  template<class Topology>
//  void getSolutionInfo_cellgroup(const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
//                                 const int cellGroupGlobal);

  const Field_CG_Cell<PhysDim,TopoDim,PDEArrayQ>& getAdjField() const
  {
    return adjoint_wo_subtraction.qfld;
  }

  const Field_CG_Cell<PhysDim,TopoDim,PDEArrayQ>& getPAdjField() const
  {
    return adjoint_p.qfld;
  }

protected:

//  template<class Temporal>
//  typename std::enable_if< std::is_same<Temporal,TemporalMarch>::value, void >::type
//  solveGlobalPrimalContinuation() override;
//
//  template<class Temporal>
//  typename std::enable_if< std::is_same<Temporal,TemporalSpaceTime>::value, void >::type
//  solveGlobalPrimalContinuation() override;

  using BaseType::sol_; //class containing all the solution data
  using BaseType::xfld_; // mesh
  using BaseType::resNormType_;
  using BaseType::tol_;
  using BaseType::quadOrder_;
  using BaseType::cellgroup_list_;
  using BaseType::interiortracegroup_list_;
  using BaseType::BCList_;
  using BaseType::BCBoundaryGroups_;
  using BaseType::NonlinearSolverDict_;
  using BaseType::AdjLinearSolverDict_;
  using BaseType::verbosity_;
  using BaseType::quadrule_;
  using BaseType::AlgEqnSet_; // AlgebraicEquationSet

  using BaseType::pErrorEstimate_;

  using BaseType::errorArray_; //Array of elemental error estimates (index:[cellgroup][elem])
  using BaseType::fcnOutput_;
  using BaseType::errorIndicator_;
  using BaseType::errorEstimate_;
  using BaseType::outputJ_; //output functional evaluated at current solution

  using BaseType::nDOFperCell_; //indexing: [global cellgrp] - number of degrees of freedom per cell
  using BaseType::orderVec_; //indexing: [global cellgrp] - solution order of each cellgroup
  using BaseType::estimatesEvaluated_;

  FieldBundle adjoint_wo_subtraction; // Adjoint without removing the p solution, useful for debugging
  FieldBundle adjoint_p; // Adjoint without removing the p solution, useful for debugging
  StabilizationMatrix primStabMtx_;
  StabilizationMatrix adjStabMtx_;
};

template<class SolutionClass, class AlgebraicEqSet, class OutputIntegrandType>
void
SolverInterface_AGLS_LiftedBoundary<SolutionClass, AlgebraicEqSet, OutputIntegrandType>::
solveGlobalPrimalProblem()
{
  timer clock;

  this->template solveGlobalPrimalContinuation<typename NDPDEClass::Temporal>();

  if (get<-1>(xfld_).comm()->rank() == 0 && verbosity_ )
    std::cout << "Primal solve time: " << std::fixed << std::setprecision(3) << clock.elapsed() << "s" << std::endl;

  // evaluate output function
  outputJ_ = 0.0;

  QuadratureOrder quadrule(xfld_, quadOrder_);

#ifndef BOUNDARYOUTPUT
  IntegrateCellGroups<TopoDim>::integrate( FunctionalCell_Galerkin( fcnOutput_, outputJ_ ),
                                           sol_.paramfld, sol_.primal.qfld,
                                           quadrule.cellOrders.data(), quadrule.cellOrders.size() );
#else
  AlgEqnSet_.dispatchBC().dispatch_DGBR2(
    FunctionalBoundaryTrace_Dispatch_DGBR2(fcnOutput_, sol_.paramfld, sol_.primal.qfld, sol_.primal.rfld,
                                           quadrule.boundaryTraceOrders.data(), quadrule.boundaryTraceOrders.size(), outputJ_ ),
    FunctionalBoundaryTrace_Dispatch_Galerkin(fcnOutput_, sol_.paramfld, sol_.primal.qfld,
                                              quadrule.boundaryTraceOrders.data(),
                                              quadrule.boundaryTraceOrders.size(), outputJ_ ) );
#ifdef SANS_MPI
  outputJ_ = boost::mpi::all_reduce( *get<-1>(xfld_).comm(), outputJ_, std::plus<Real>() );
#endif

#endif
}

template<class SolutionClass, class AlgebraicEqSet, class OutputIntegrandType>
void
SolverInterface_AGLS_LiftedBoundary<SolutionClass, AlgebraicEqSet, OutputIntegrandType>::
solveGlobalAdjointProblem()
{
  typedef typename AlgEqnSet_Global::SystemVector SystemVectorClass;
  typedef typename AlgEqnSet_Global::SystemMatrix SystemMatrixClass;

  const int iPDE = AlgEqnSet_.iPDE;

  QuadratureOrder quadrule(xfld_, quadOrder_);

  // Because the estimate used in Galerkin methods is strong form, thus need to subtract off a P solution
  timer clock_p;

  {

    if (get<-1>(xfld_).comm()->rank() == 0 && verbosity_ )
      std::cout<< "using solve for p adjoint" << std::endl;

    // Using a P solve, works but seems wasteful/projection seems a little more stable
    // Should re-use the final primal jacobian
    SystemVectorClass rhs_p( AlgEqnSet_.vectorEqSize() );
    rhs_p = 0;

    AlgEqnSet_Global algEqnSetAdj(sol_.paramfld, sol_.primal, sol_.pliftedQuantityfld,
                  sol_.pde, primStabMtx_, quadrule_, resNormType_, tol_,
                  cellgroup_list_, interiortracegroup_list_, BCList_, BCBoundaryGroups_);

#ifndef BOUNDARYOUTPUT
    OutputIntegrandType fcnOutput2(sol_.pde, fcnOutput_.getOutputFcn(), fcnOutput_.cellGroups(), primStabMtx_);
    IntegrateCellGroups<TopoDim>::integrate( JacobianFunctionalCell_Galerkin( fcnOutput2, rhs_p(iPDE) ),
                                             sol_.paramfld, sol_.primal.qfld,
                                             quadrule.cellOrders.data(), quadrule.cellOrders.size() );
#else
    typedef SurrealS<NDPDEClass::N> SurrealClass;

    algEqnSetAdj.dispatchBC().dispatch_DGBR2(
        JacobianFunctionalBoundaryTrace_Dispatch_DGBR2<SurrealClass>(fcnOutput_, sol_.paramfld,
                                                                     sol_.primal.qfld, sol_.primal.rfld, adjoint_p.rfld,
                                                                     quadrule.boundaryTraceOrders.data(),
                                                                     quadrule.boundaryTraceOrders.size(), rhs_p(iPDE) ),
        JacobianFunctionalBoundaryTrace_Dispatch_Galerkin<SurrealClass>(fcnOutput_, sol_.paramfld,
                                                                        sol_.primal.qfld,
                                                                        quadrule.boundaryTraceOrders.data(),
                                                                        quadrule.boundaryTraceOrders.size(), rhs_p(iPDE) ) );
#endif

    // Adjoint solver
    SLA::LinearSolver< SystemMatrixClass > solver_p( AdjLinearSolverDict_, algEqnSetAdj, SLA::TransposeSolve );

    SystemVectorClass adj_p( algEqnSetAdj.vectorStateSize() );
    adj_p = 0; // set initial guess

    solver_p.solve( rhs_p, adj_p );

    // update solution
    algEqnSetAdj.setAdjointField( adj_p, adjoint_p.qfld, adjoint_p.rfld, adjoint_p.lgfld );

    adjoint_p.qfld.syncDOFs_MPI_noCache();
    adjoint_p.lgfld.syncDOFs_MPI_noCache();
  }

  FieldBundle adj_prolong( xfld_, sol_.adjoint.order, sol_.adjoint,
                           sol_.mitlg_boundarygroups );


  adjoint_p.qfld.projectTo(adj_prolong.qfld);
  adjoint_p.rfld.projectTo(adj_prolong.rfld);
  adjoint_p.lgfld.projectTo(adj_prolong.lgfld);

#if 0
  output_Tecplot( sol_.adjoint.qfld, "tmp/adjfld_Pp1.plt");
  output_Tecplot(  adj_prolong.qfld, "tmp/adjfld_p.plt");
#endif


  if (get<-1>(xfld_).comm()->rank() == 0 && verbosity_ )
    std::cout << "Adjoint order=" << sol_.primal.order << " solve time: "
              << std::fixed << std::setprecision(3) << clock_p.elapsed() << "s" << std::endl;

  timer clock_Pp1;

  //Create primal solutions in richer solution space
  FieldBundle primal_pro(xfld_, sol_.adjoint.order,
                         sol_.primal, sol_.mitlg_boundarygroups );

  // Prolongate the primal solution to the richer solution space
  sol_.primal.projectTo(primal_pro);

  // Create param fields with prolongated primal solution
  ParamFieldBuilderType parambuilder(sol_.paramfld, primal_pro.qfld, sol_.parambuilder.dict);

  //Create AlgebraicEquationSets for solutions in richer space
  AlgEqnSet_Global AlgEqnSet_Pro(parambuilder.fld, primal_pro, sol_.pliftedQuantityfld,
                                 sol_.pde, adjStabMtx_, quadrule, resNormType_, tol_,
                                 cellgroup_list_, interiortracegroup_list_,
                                 BCList_, BCBoundaryGroups_);

  // Functional integral
  SystemVectorClass rhs(AlgEqnSet_Pro.vectorEqSize());
  rhs = 0;

#ifndef BOUNDARYOUTPUT
  OutputIntegrandType fcnOutput2(sol_.pde, fcnOutput_.getOutputFcn(), fcnOutput_.cellGroups(), adjStabMtx_);
  IntegrateCellGroups<TopoDim>::integrate( JacobianFunctionalCell_Galerkin( fcnOutput2, rhs(iPDE) ),
                                           parambuilder.fld, primal_pro.qfld,
                                           quadrule.cellOrders.data(), quadrule.cellOrders.size() );
#else
  typedef SurrealS<NDPDEClass::N> SurrealClass;
  AlgEqnSet_Pro.dispatchBC().dispatch_DGBR2(
      JacobianFunctionalBoundaryTrace_Dispatch_DGBR2<SurrealClass>(fcnOutput_, parambuilder.fld,
                                                                   primal_pro.qfld, primal_pro.rfld, sol_.adjoint.rfld,
                                                                   quadrule.boundaryTraceOrders.data(),
                                                                   quadrule.boundaryTraceOrders.size(), rhs(iPDE) ),
      JacobianFunctionalBoundaryTrace_Dispatch_Galerkin<SurrealClass>(fcnOutput_, parambuilder.fld,
                                                                      primal_pro.qfld,
                                                                      quadrule.boundaryTraceOrders.data(),
                                                                      quadrule.boundaryTraceOrders.size(), rhs(iPDE) ) );
#endif

  // adjoint solver
  SLA::LinearSolver< SystemMatrixClass > solver(AdjLinearSolverDict_, AlgEqnSet_Pro, SLA::TransposeSolve);

  SystemVectorClass adj(AlgEqnSet_Pro.vectorStateSize());

#if 0
  // set the initial guess as the prolongated adjoint solution
  AlgEqnSet_Pro.fillSystemVector(adj_prolong.qfld, adj_prolong.lgfld, adj);
#else
  //if p adjoint is poor quality, initial guess will be bad
  adj = 0;
#endif

  solver.solve(rhs, adj);

  // update solution
  AlgEqnSet_Pro.setAdjointField(adj, sol_.adjoint.qfld, sol_.adjoint.rfld, sol_.adjoint.lgfld );

  sol_.adjoint.qfld.syncDOFs_MPI_noCache();
  sol_.adjoint.lgfld.syncDOFs_MPI_noCache();

  if (get<-1>(xfld_).comm()->rank() == 0 && verbosity_ )
    std::cout << "Adjoint order=" << sol_.adjoint.order << " solve time: "
              << std::fixed << std::setprecision(3) << clock_Pp1.elapsed() << "s" << std::endl;

  // subtract off the prolongated adjoint from the higher-order adjoint
  for (int i = 0; i < adj_prolong.qfld.nDOF(); i++)
  {
    adjoint_wo_subtraction.qfld.DOF(i) = sol_.adjoint.qfld.DOF(i); //p+1 adjoint wpp1
    sol_.adjoint.qfld.DOF(i) = sol_.adjoint.qfld.DOF(i) - adj_prolong.qfld.DOF(i); //wpp1 - wp
    sol_.adjoint.rfld.DOF(i) = sol_.adjoint.rfld.DOF(i) - adj_prolong.rfld.DOF(i); //spp1 - sp
  }

  //CORRECT OUTPUT FOR STABILIZED ( L* - L, tau*strongPDE )
  OutputCorrectionClass correction( sol_.paramfld, sol_.primal, adjoint_p,
                           sol_.pliftedQuantityfld, sol_.pde, sol_.disc, quadrule, cellgroup_list_, interiortracegroup_list_,
                           BCList_, BCBoundaryGroups_ );

  Real outputCorrection = 0;
  correction.getErrorEstimate(outputCorrection);
  outputJ_ += outputCorrection;

//  output_Tecplot(sol_.adjoint.qfld, "tmp/adj_delta.plt" );

}



} // namespace SANS

#endif /* SOLVERINTERFACE_AGLS_H_ */
