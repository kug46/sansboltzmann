// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef MOESSPARAMS_H_
#define MOESSPARAMS_H_

#include "Python/PyDict.h"
#include "Python/Parameter.h"

namespace SANS
{

struct MOESSParams : noncopyable
{
  const ParameterBool UniformRefinement{"UniformRefinement", false, "Include uniform refinement local split configuration?"};

//  const ParameterNumeric<Real> hRefineFactorMax
//  {
//    "hRefineFactorMax", 2.0, 1.05, NO_LIMIT, "Maximum factor by which an edge can be refined (or coarsened)"
//  };

  // const ParameterNumeric<Real> InverseRegularizerCoefficient
  // {
  //   "InverseRegularizerCoefficient", 1.1, 1, NO_LIMIT,
  //   "Coefficient in quadratic regularizer +  norm(R) norm(S)^2/ (coeff*sqrt(d)*log(2))"
  // };

  const ParameterNumeric<Real> DOFIncreaseFactor
  {
    "DOFIncreaseFactor", 0.5, 0.1 , 1.0,
    "Factor by which the target cost is increased relative to the maximum growth by give by hRefineFactorMax on each iteration"
  };

  const ParameterNumeric<Real> FrobNormSqSum_GlobalFraction
  {
    "FrobNormSqSum_Fraction", 0.25, 0.0, 1e5,
    "Fraction of maximum allowable sum of Frobenius norm sq, to use in global constraint for step-matrices"
  };

  const ParameterNumeric<Real> GradationFactor{"GradationFactor", 4.0, 1.0, NO_LIMIT, "Relative growth factor between nodal metrics"};

  const ParameterNumeric<Real> Optimizer_XTol_Rel{"Optimizer_XTol_Rel", 1e-7, 0.0, NO_LIMIT, "Relative x-tolerance for optimizer"};
  const ParameterNumeric<Real> Optimizer_FTol_Rel{"Optimizer_FTol_Rel", 1e-7, 0.0, NO_LIMIT, "Relative f-tolerance for optimizer"};
  const ParameterNumeric<int>  Optimizer_MaxEval {"Optimizer_MaxEval", 2000, 0, NO_LIMIT, "Maximum evaluation limit for optimizer"};

  const ParameterNumeric<Real> ImpliedMetric_XTol_Rel{"ImpliedMetric_XTol_Rel", 1e-7, 0.0, NO_LIMIT, "Relative x-tolerance for optimizer"};
  const ParameterNumeric<Real> ImpliedMetric_FTol_Rel{"ImpliedMetric_FTol_Rel", 1e-7, 0.0, NO_LIMIT, "Relative f-tolerance for optimizer"};
  const ParameterNumeric<int>  ImpliedMetric_MaxEval {"ImpliedMetric_MaxEval", 200, 0, NO_LIMIT, "Maximum evaluation limit for optimizer"};

  struct CostModelOptions
  {
    typedef std::string ExtractType;
    const std::string LogEuclidean = "LogEuclidean";

    const std::vector<std::string> options{LogEuclidean};
  };
  const ParameterOption<CostModelOptions> CostModel =
        ParameterOption<CostModelOptions>("CostModel", "LogEuclidean", "Cost model type");

  struct ImpliedMetricOptions
  {
    typedef std::string ExtractType;
    const std::string VolumeWeighted = "VolumeWeighted";
    const std::string UniformWeighted = "UniformWeighted";
    const std::string AffineInvariant = "AffineInvariant";
    const std::string InverseLengthTensor = "InverseLengthTensor";
    const std::string Optimized = "Optimized";

    const std::vector<std::string> options{VolumeWeighted, UniformWeighted, AffineInvariant, InverseLengthTensor, Optimized};
  };
  const ParameterOption<ImpliedMetricOptions> ImpliedMetric =
        ParameterOption<ImpliedMetricOptions>("ImpliedMetric", "Optimized", "Implied Metric calculation");

  struct MetricOptimizationOptions
  {
    typedef std::string ExtractType;
    const std::string FrobNorm = "FrobNorm";
    //const std::string Gradation = "Gradation";
    const std::string SANS = "SANS";
    const std::string SANSparallel = "SANSparallel";
    const std::string None = "None";

    const std::vector<std::string> options{FrobNorm, SANS, SANSparallel, None}; //, Gradation
  };
  const ParameterOption<MetricOptimizationOptions> MetricOptimization =
        ParameterOption<MetricOptimizationOptions>("MetricOptimization", "FrobNorm", "Scheme for metric optimization");

  struct VerbosityOptions
  {
    typedef int ExtractType;
    enum Options
    {
      None = 0,
      Progressbar = 1,
      Detailed = 2
    };
    const std::vector<int> options{None, Progressbar, Detailed};
  };
  const ParameterOption<VerbosityOptions> Verbosity =
        ParameterOption<VerbosityOptions>("Verbosity", VerbosityOptions::Progressbar, "Verbosity level");

  struct LocalSolveOptions
  {
    typedef std::string ExtractType;
    const std::string Element     = "Element";
    const std::string Edge        = "Edge";
    const std::string EdgeElement = "EdgeElement";

    const std::vector<std::string> options{Element, Edge, EdgeElement};
  };
  const ParameterOption<LocalSolveOptions> LocalSolve =
        ParameterOption<LocalSolveOptions>("LocalSolve", "Element", "Local Solve patch used");

  struct MetricNormOptions
  {
    typedef std::string ExtractType;
    const std::string L2 = "L2";
    const std::string Linf = "Linf";

    const std::vector<std::string> options{L2, Linf};
  };
  const ParameterOption<MetricNormOptions> MetricNorm =
        ParameterOption<MetricNormOptions>("MetricNorm", "L2", "Metric norm type");

  static void checkInputs(PyDict d);
  static MOESSParams params;
};

} // namespace SANS

#endif //MOESSPARAMS_H_
