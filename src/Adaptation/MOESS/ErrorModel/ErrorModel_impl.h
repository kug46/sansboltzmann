// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(ERRORMODEL_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "../ErrorModel.h"

#include <boost/version.hpp>
#include <boost/functional/hash.hpp>

#include <set>
#include <array>
#include <iomanip> //std::setprecision
#include <numeric> // std::accumulate
#include <algorithm> // std::max_element, std::distance
#include <unordered_map>
#include <unordered_set>

#include "Surreal/SurrealS.h"

#include "tools/timer.h"
#include "tools/ProgressBar.h"
#include "tools/safe_at.h"
#include "tools/output_std_vector.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_SVD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_FrobNorm.h"

#include "Field/Element/ElementXFieldJacobianEquilateral.h"
#include "Field/tools/for_each_CellGroup.h"

#include "Field/XField_CellToTrace.h"
#include "Field/Local/XField_LocalPatch.h"

#include "BasisFunction/ElementEdges.h"

#include "Adaptation/MOESS/LocalSplitConfig.h"
#include "Adaptation/MOESS/ReferenceStepMatrix.h"

#include "tools/KahanSum.h"

#ifdef SANS_PETSC
#include "Adaptation/MOESS/TAO/TAOException.h"
#endif

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_MPI
#include "MPI/serialize_DenseLinAlg_MatrixS.h"
#include "tools/plus_std_vector.h"
#include <boost/mpi/collectives/reduce.hpp>
#include <boost/mpi/collectives/gather.hpp>
#include <boost/mpi/collectives/all_gather.hpp>
#include <boost/mpi/collectives/all_reduce.hpp>
#include <boost/mpi/collectives/all_to_all.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>

// handles serialization of arrays, depending on version of boost
#include "MPI/boost_serialization_array.h"


namespace boost
{
namespace serialization
{
template<class Archive>
void serialize(Archive & ar, SANS::GroupElemIndex& cell, const unsigned int version)
{
  ar & cell.group;
  ar & cell.elem;
}
} // namespace serialization

namespace mpi
{
// This allows Boost.MPI to avoid extraneous copy operations
// Only works when the datatype is of fixed size (no pointers or dynamically sized objects)
template <>
struct is_mpi_datatype< SANS::GroupElemIndex > : mpl::true_ {};
}
} // namespace boost

#endif


namespace SANS
{


// struct for storing error step pairs
template<class PhysDim>
struct ErrorStepPairDim
{
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  ErrorStepPairDim() {} // Not initializing data
  ErrorStepPairDim(const Real& error, const MatrixSym& step) : error(error), step(step) {}

  Real error;
  MatrixSym step;

  // for debugging
  void dump() const { std::cout << "error = " << error << std::endl << "step = " << std::endl << step; }

#ifdef SANS_MPI
protected:
  friend class boost::serialization::access;

  template<class Archive>
  void serialize(Archive & ar, const unsigned int version)
  {
    ar & error;
    ar & step;
  }
#endif
};


//===========================================================================//
template <class PhysDim, class TopoDim>
ErrorModel<PhysDim, TopoDim>::
ErrorModel(const XField<PhysDim,TopoDim>& xfld_linear,
           const std::vector<int>& cellgroup_list,
           const SolverInterfaceBase<PhysDim,TopoDim>& problem,
           const PyDict& paramsDict)
  : xfld_linear_(xfld_linear),
    cellgroup_list_(cellgroup_list),
    problem_(problem),
    uniform_refinement_(paramsDict.get(ParamsType::params.UniformRefinement)),
    LocalSolve_(paramsDict.get(ParamsType::params.LocalSolve)),
    verbosity_(paramsDict.get(ParamsType::params.Verbosity)),
    // use the new model if running sans opt
    use_modified_model_(paramsDict.get(ParamsType::params.MetricOptimization) == ParamsType::params.MetricOptimization.SANS ||
                        paramsDict.get(ParamsType::params.MetricOptimization) == ParamsType::params.MetricOptimization.SANSparallel)
{
  //xfld_linear_ should be linear (i.e. Q1), and is used for the implied metric
  for (int i = 0; i < xfld_linear_.nCellGroups(); i++)
    SANS_ASSERT( xfld_linear_.getCellGroupBase(i).order() == 1 );
}


//===========================================================================//
template <class PhysDim, class TopoDim>
class Synthesis_Element : public GroupFunctorCellType<Synthesis_Element<PhysDim, TopoDim>>
{
public:
  Synthesis_Element( const XField_CellToTrace<PhysDim, TopoDim>& xfld_connectivity,
                     const std::vector<int>& cellgroup_list,
                     ErrorModel<PhysDim, TopoDim>& errorModel,
                     std::vector<Real>& time_breakdown ) :
    xfld_connectivity_(xfld_connectivity), cellgroup_list_(cellgroup_list),
    errorModel_(errorModel), time_breakdown_(time_breakdown) {}

  std::size_t nCellGroups() const          { return cellgroup_list_.size(); }
  std::size_t cellGroup(const int n) const { return cellgroup_list_[n];     }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology>
  void
  apply(const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
        const int cellGroupGlobal)
  {
    errorModel_.template synthesize_element_cellgroup<Topology>(xfldCellGroup, cellGroupGlobal,
                                                                xfld_connectivity_, time_breakdown_);
  }

protected:
  const XField_CellToTrace<PhysDim, TopoDim>& xfld_connectivity_;
  const std::vector<int> cellgroup_list_;
  ErrorModel<PhysDim, TopoDim>& errorModel_;
  std::vector<Real>& time_breakdown_;
};

enum SynthesisTimes
{
  eMesh_t  , // Local mesh manipulations (extraction and splits)
  eResid_t , // Residual construction
  eJac_t   , // Jacobian construction
  eSolve_t , // Solve
  eErrEst_t, // Error estimation
  eAlgEq_t , // AlgEqSet constructor
  eConst_t , // Class contructor
  eDiag_t  ,
  eComm_t  , // Communication
  eSynth_t , // Constructing/sythesizing models
  eLocal_t , // Time on each processor that excludes communication
  eSynthTimes,
};


//===========================================================================//
template <class PhysDim, class TopoDim>
void
ErrorModel<PhysDim, TopoDim>::synthesize(const XField<PhysDim, TopoDim>& xfld_curved)
{
  timer clock;

  errorModels_.resize(xfld_linear_.nCellGroups());

  const XField_CellToTrace<PhysDim, TopoDim> xfld_connectivity(xfld_curved); //build connectivity of curved mesh, as required for local solves

  std::vector<Real> time_breakdown(eSynthTimes, 0.0); //stores the times taken for each part of the synthesis process

  if ( LocalSolve_ == ParamsType::params.LocalSolve.Element )
  {
    if (problem_.spaceType() == SpaceType::Discontinuous)
    {
      for_each_CellGroup<TopoDim>::apply(
          Synthesis_Element<PhysDim,TopoDim>(xfld_connectivity, cellgroup_list_, *this, time_breakdown),
          xfld_linear_ );
    }
    else if (problem_.spaceType() == SpaceType::Continuous)
    {
      // std::cout << "Element Local solves for CG are being deprecated! You should use LocalSolve.Edge" << std::endl;
      SANS_DEVELOPER_EXCEPTION("Element Local solves for CG are being deprecated! You should use LocalSolve.Edge");
      for_each_CellGroup<TopoDim>::apply(
          Synthesis_Element<PhysDim,TopoDim>(xfld_connectivity, cellgroup_list_, *this, time_breakdown),
          xfld_linear_ );
    }
    else
    {
      SANS_DEVELOPER_EXCEPTION("Unexpected SpaceType");
    }
  }
  else if ( LocalSolve_ == ParamsType::params.LocalSolve.Edge )
  {
    // edge version - builds node models but using edge focused local solves

    synthesize_edge_node(xfld_connectivity,time_breakdown);

  }
  else if ( LocalSolve_ == ParamsType::params.LocalSolve.EdgeElement )
  {
    // edge version - builds node models but using edge focused local solves

    synthesize_edge_element(xfld_connectivity,time_breakdown);
  }
  else
    SANS_DEVELOPER_EXCEPTION("Unknown Local Solve method");


  timer MPI;

  if (LocalSolve_ == ParamsType::params.LocalSolve.Element ||
      LocalSolve_ == ParamsType::params.LocalSolve.EdgeElement)
  {
    // synchronize the error models in ghost/zombie elements
    syncErrorModels();
  }

  time_breakdown[eComm_t] += MPI.elapsed();

  Real wall_time = clock.elapsed();

#ifdef SANS_MPI
  // send the total time to the root processor
  std::vector<Real> time_breakdown_Global;
#if BOOST_VERSION <= 105400 //TODO: The boost version here might need to be higher
  boost::mpi::reduce(*xfld_linear_.comm(), time_breakdown, time_breakdown_Global, std::plus<std::vector<Real>>(), 0 );
#else
  boost::mpi::reduce(*xfld_linear_.comm(), time_breakdown, time_breakdown_Global, std::plus<Real>(), 0 );
#endif
  time_breakdown = time_breakdown_Global;
#endif

  if (verbosity_ >= ParamsType::VerbosityOptions::Progressbar && xfld_linear_.comm()->rank() == 0 )
  {
    Real comm_size = xfld_linear_.comm()->size();

    Real local_cpu_time = time_breakdown[eLocal_t]; // Time on each processor that excludes communication
    Real total_cpu_time = wall_time*comm_size;

    Real rem_time = total_cpu_time;
    for (std::size_t i = 0; i < time_breakdown.size(); i++)
    {
      if (i == eLocal_t) continue; // don't subtract of overlapping timings
      rem_time -= time_breakdown[i];
    }

    Real ideal_local_cpu_time = local_cpu_time/comm_size;

    std::cout << std::fixed << std::setprecision(3);
    std::cout << "Sampling and Synthesis Timings"  << std::endl;
    std::cout << "Wall time       : " << wall_time << "s" << std::endl;
    std::cout << "Efficiency      : " << 100*ideal_local_cpu_time/wall_time << "%" << std::endl;
    std::cout << "CPU time        : " << total_cpu_time  << "s" <<std::endl;
    std::cout << "  - mesher      : " << time_breakdown[eMesh_t  ] << "s, " << 100*time_breakdown[eMesh_t  ]/total_cpu_time << "%" << std::endl;
    std::cout << "  - residual    : " << time_breakdown[eResid_t ] << "s, " << 100*time_breakdown[eResid_t ]/total_cpu_time << "%" << std::endl;
    std::cout << "  - jacobian    : " << time_breakdown[eJac_t   ] << "s, " << 100*time_breakdown[eJac_t   ]/total_cpu_time << "%" << std::endl;
    std::cout << "  - solver      : " << time_breakdown[eSolve_t ] << "s, " << 100*time_breakdown[eSolve_t ]/total_cpu_time << "%" << std::endl;
    std::cout << "  - error est   : " << time_breakdown[eErrEst_t] << "s, " << 100*time_breakdown[eErrEst_t]/total_cpu_time << "%" << std::endl;
    std::cout << "  - algeq set   : " << time_breakdown[eAlgEq_t ] << "s, " << 100*time_breakdown[eAlgEq_t ]/total_cpu_time << "%" << std::endl;
    std::cout << "  - construct   : " << time_breakdown[eConst_t ] << "s, " << 100*time_breakdown[eConst_t ]/total_cpu_time << "%" << std::endl;
    std::cout << "  - diagnose    : " << time_breakdown[eDiag_t  ] << "s, " << 100*time_breakdown[eDiag_t  ]/total_cpu_time << "%" << std::endl;
    std::cout << "  - communicate : " << time_breakdown[eComm_t  ] << "s, " << 100*time_breakdown[eComm_t  ]/total_cpu_time << "%" << std::endl;
    std::cout << "  - synthesis   : " << time_breakdown[eSynth_t ] << "s, " << 100*time_breakdown[eSynth_t ]/total_cpu_time << "%" << std::endl;
    std::cout << "  - other       : " << rem_time                  << "s, " << 100*rem_time                 /total_cpu_time << "%" << std::endl;
    std::cout << std::endl;
  }
}


//===========================================================================//
template <class PhysDim, class TopoDim>
class SyncNeededCells : public GroupFunctorCellType<SyncNeededCells<PhysDim, TopoDim>>
{
public:
  SyncNeededCells( const std::vector<int> cellgroup_list, const int comm_rank,
                   const std::vector<std::vector<int>>& cellIDs,
                   std::vector<std::vector<std::vector<int>>>& needCell ) :
    cellgroup_list_(cellgroup_list), comm_rank_(comm_rank), cellIDs_(cellIDs), needCell_(needCell) {}

  std::size_t nCellGroups() const          { return cellgroup_list_.size(); }
  std::size_t cellGroup(const int n) const { return cellgroup_list_[n];     }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology >
  void
  apply(const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
        const int cellGroupGlobal)
  {
    const int nElem = xfldCellGroup.nElem();
    int nElemPossessed = 0;

    // count the number of elements possessed by this processor
    for (int elem = 0; elem < nElem; elem++ )
      if (xfldCellGroup.associativity(elem).rank() == comm_rank_)
        nElemPossessed++;

    // all the ghost elements are last

    const std::vector<int>& cellIDs = cellIDs_[cellGroupGlobal];

    // add to the required DOFs from other ranks
    for (int elem = nElemPossessed; elem < nElem; elem++)
      needCell_[xfldCellGroup.associativity(elem).rank()][cellGroupGlobal].push_back(cellIDs[elem]);
  }

protected:
  const std::vector<int> cellgroup_list_;
  const int comm_rank_;
  const std::vector<std::vector<int>>& cellIDs_;
  std::vector<std::vector<std::vector<int>>>& needCell_;
};

//===========================================================================//
template <class PhysDim, class TopoDim>
void
ErrorModel<PhysDim, TopoDim>::syncErrorModels()
{
#ifdef SANS_MPI
  std::vector<std::map<int,int>> native2localCellmap(xfld_linear_.nCellGroups());

  for (std::size_t igroup = 0; igroup < native2localCellmap.size(); igroup++)
  {
    const std::vector<int>& cellIDs = xfld_linear_.cellIDs(igroup);

    // save off the mapping to get the local DOF based on native DOF number
    for (std::size_t icell = 0; icell < cellIDs.size(); icell++)
      native2localCellmap[igroup][cellIDs[icell]] = icell;
  }

  std::vector<std::vector<std::vector<int>>> sendCell(xfld_linear_.comm()->size());
  std::vector<std::vector<std::vector<int>>> needCell(xfld_linear_.comm()->size());
  for (std::size_t rank = 0; rank < needCell.size(); rank++)
    needCell[rank].resize(xfld_linear_.nCellGroups());

  // extract all ghosted cells that are needed on this processor
  for_each_CellGroup<TopoDim>::apply(
      SyncNeededCells<PhysDim,TopoDim>(cellgroup_list_, xfld_linear_.comm()->rank(), xfld_linear_.cellIDs(), needCell ),
      xfld_linear_ );

  // send the requested native DOF indexing to the other ranks
  boost::mpi::all_to_all(*xfld_linear_.comm(), needCell, sendCell);

  std::vector<ErrorModelVectorType> sendErrorModel(sendCell.size());
  std::vector<ErrorModelVectorType> recvErrorModel(sendCell.size());

  // translate the native DOF index to a local index
  for ( std::size_t rank = 0; rank < sendCell.size(); rank++ )
  {
    sendErrorModel[rank].resize(xfld_linear_.nCellGroups());

    const std::vector<std::vector<int>>& sendCell_rank = sendCell[rank];
    for (std::size_t igroup = 0; igroup < native2localCellmap.size(); igroup++)
    {
      sendErrorModel[rank][igroup].resize(sendCell_rank[igroup].size());

      // copy values to be sent
      for ( std::size_t i = 0; i < sendCell_rank[igroup].size(); i++ )
      {
        int cellLocal = safe_at(native2localCellmap[igroup], sendCell_rank[igroup][i]);

        sendErrorModel[rank][igroup][i] = errorModels_[igroup][cellLocal];
      }
    }
  }

  // send the DOF values to the other ranks
  boost::mpi::all_to_all(*xfld_linear_.comm(), sendErrorModel, recvErrorModel);
  sendErrorModel.clear();

  for ( std::size_t rank = 0; rank < needCell.size(); rank++ )
  {
    const std::vector<std::vector<int>>& needCell_rank = needCell[rank];
    for (std::size_t igroup = 0; igroup < needCell_rank.size(); igroup++)
      for (std::size_t icell = 0; icell < needCell_rank[igroup].size(); icell++)
      {
        int cellLocal = safe_at(native2localCellmap[igroup], needCell_rank[igroup][icell]);

        // assign models from other ranks
        errorModels_[igroup][cellLocal] = recvErrorModel[rank][igroup][icell];
      }
  }
#endif
}

//===========================================================================//
template <class PhysDim, class TopoDim>
template <class Topology>
void
ErrorModel<PhysDim, TopoDim>::
synthesize_element_cellgroup(const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup_linear,
                             const int cellGroupGlobal,
                             const XField_CellToTrace<PhysDim, TopoDim>& xfld_connectivity_curve,
                             std::vector<Real>& time_breakdown)
{
  timer clock_local;

  //Cell Group Types
  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

  ElementXFieldClass xfldElem(xfldCellGroup_linear.basis() );

  // get the rank of the current processor
  int comm_rank = xfld_linear_.comm()->rank();
  mpi::communicator comm_local = xfld_linear_.comm()->split(comm_rank);

  int nElem = xfldCellGroup_linear.nElem(); //Number of elements in cell group

  errorModels_[cellGroupGlobal].resize(nElem);

  int order = problem_.getSolutionOrder(cellGroupGlobal); //Get solution order
  SANS_ASSERT( order >= 0 );

  //Get the list of local split configurations for this topology
  std::vector<LocalSplitConfig> split_config_list = LocalSplitConfigList<Topology>::get(uniform_refinement_);

  int Nconfig = (int) split_config_list.size(); //Number of split configurations

  //Compute the step matrices for each local split of the reference equilateral element
  ReferenceStepMatrix<PhysDim, TopoDim> RefStepMatrixList(comm_local, split_config_list);

  // determine whether we will extract a DG or CG field
  std::shared_ptr<Field_NodalView> xfld_nodalview_curve = nullptr;
  SpaceType spaceType = problem_.spaceType();
  std::string spaceTypeStr = "DG";
  if (spaceType == SpaceType::Continuous)
  {
    // we need to build the nodal view for CG patch extraction
    xfld_nodalview_curve = std::make_shared<Field_NodalView>(xfld_connectivity_curve.getXField(),cellgroup_list_);
    spaceTypeStr = "CG";
  }

  std::stringstream ss;
  ss << "Sampling cellgroup " << cellGroupGlobal << " for " << spaceTypeStr;
  ProgressBar progressbar(ss.str());

  long localsolve_count = 0;
  long localsolve_unconverged_count = 0;
  long localsolve_iter_sum = 0;
  std::vector<std::vector<Real>> maxRsdNorm0_unconverged;
  std::vector<std::vector<Real>> maxRsdNorm_unconverged;

  int print_rank = 0;
#ifdef SANS_MPI
  {
    int nElemPossessed = 0;

    // count the number of elements possessed by this processor
    for (int elem = 0; elem < nElem; elem++ )
      if (xfldCellGroup_linear.associativity(elem).rank() == comm_rank)
        nElemPossessed++;

    // all the ghost elements should be last, just assert that assumption
    for (int elem = nElemPossessed+1; elem < nElem; elem++ )
      SANS_ASSERT(xfldCellGroup_linear.associativity(elem).rank() != comm_rank);

    // shorten the number of elements considered in the synthesis to those possessed
    nElem = nElemPossessed;

    // Only let the processors with the most elements print the progress bar
    std::vector<int> nElemOnRank;
    boost::mpi::all_gather(*xfld_linear_.comm(), nElem, nElemOnRank);
    print_rank = std::distance(nElemOnRank.begin(), std::max_element(nElemOnRank.begin(), nElemOnRank.end()));
  }
#endif

  Real R_Fr_max = 0, R_Fr_min = std::numeric_limits<Real>::max();
  MatrixSym R_max = 0, R_min = 0;

  for (int elem = 0; elem < nElem; elem++ )
  {
    // make sure the cell is possessed by this processor
    SANS_ASSERT (xfldCellGroup_linear.associativity(elem).rank() == comm_rank);

    xfldCellGroup_linear.getElement(xfldElem, elem);

    //Calculate element Jacobian
    Matrix Jref;
    xfldElem.jacobian(Jref);

    //Transformation matrix from the unit equilateral element to the reference element
    const Matrix& invJeq = JacobianEquilateralTransform<PhysDim,TopoDim,Topology>::invJeq;

    Matrix J = Jref*invJeq; //Jacobian from equilateral element to physical element

    Matrix U, VT;
    DLA::VectorS<D,Real> sigma;

    //Compute SVD of Jacobian
    SVD(J, U, sigma, VT);

    timer local_mesh_clock;

    // extract the local mesh for the current element
    XField_LocalPatchConstructor<PhysDim,Topology> xfld_local( comm_local, xfld_connectivity_curve,
                                                               cellGroupGlobal, elem, spaceType, xfld_nodalview_curve.get() );

    time_breakdown[eMesh_t] += local_mesh_clock.elapsed();

    //Get error estimate on original element
    Real error0 = problem_.getElementalErrorEstimate(cellGroupGlobal, elem);
    error0 = fabs(error0);

    if (verbosity_ == ParamsType::VerbosityOptions::Detailed)
      std::cout << "Cellgroup " << cellGroupGlobal << ", Elem " << elem << " : error0 = "
                << std::scientific << std::setprecision(3) << error0 <<std::endl;

    //Vectors to store local sampling data
    std::vector<Real> error1_vec(Nconfig);
    std::vector<MatrixSym> stepmatrix_vec(Nconfig);

    for (int config = 0; config < Nconfig; config++)
    {
      //Compute the step matrix in physical space, for this local split configuration
      MatrixSym S_eq = RefStepMatrixList.getStepMatrix(config);
      MatrixSym S = Metric::rotateRefStepMatrix( S_eq, U, VT );

      timer local_mesh_split_clock;

      // split the local mesh
      XField_LocalPatch<PhysDim,Topology> xfld_split_local(xfld_local,
                                                           split_config_list[config].type,
                                                           split_config_list[config].edge_index);
      time_breakdown[eMesh_t] += local_mesh_split_clock.elapsed();

      //Solve on the split local mesh and compute local error
      //-----------------------------------------------------
      std::vector<Real> split_elem_error(1); //sum of the errors in the split-elements

      LocalSolveStatus status;
      status = problem_.solveLocalProblem(xfld_split_local, split_elem_error);

      localsolve_count++;
      localsolve_iter_sum += status.iter;
      time_breakdown[eResid_t ] += status.time_residual;
      time_breakdown[eJac_t   ] += status.time_jacobian;
      time_breakdown[eSolve_t ] += status.time_solve;
      time_breakdown[eErrEst_t] += status.time_errest;
      time_breakdown[eAlgEq_t ] += status.time_algeqset;
      time_breakdown[eConst_t ] += status.time_constructor;
      time_breakdown[eDiag_t  ] += status.time_diagnose;

      if (!status.converged)
      {
        //Keep track of the maximum unconverged residual norm
        if (localsolve_unconverged_count == 0)
        {
          maxRsdNorm0_unconverged = status.rsdNorm0_unconverged;
          maxRsdNorm_unconverged = status.rsdNorm_unconverged;
        }
        else
        {
          for (std::size_t i = 0; i < maxRsdNorm_unconverged.size(); i++)
            for (std::size_t j = 0; j < maxRsdNorm_unconverged[i].size(); j++)
            {
              if (maxRsdNorm_unconverged[i][j] < status.rsdNorm_unconverged[i][j])
              {
                maxRsdNorm0_unconverged[i][j] = status.rsdNorm0_unconverged[i][j];
                maxRsdNorm_unconverged[i][j] = status.rsdNorm_unconverged[i][j];
              }
            }
        }
        localsolve_unconverged_count++;
      }

      Real error1 = fabs(split_elem_error[0]);

      if (verbosity_ == ParamsType::VerbosityOptions::Progressbar && comm_rank == print_rank)
      {
        progressbar.update((Real)(elem*Nconfig + config)/(Real)(nElem*Nconfig - 1));
      }
      else if (verbosity_ == ParamsType::VerbosityOptions::Detailed)
      {
        std::cout << "  Split " << config << ": error = "
                  << std::scientific << std::setprecision(3) << error1 << std::endl;
      }
      //-------------------------------------------------

      stepmatrix_vec[config] = S;
      error1_vec[config] = error1;

    } //loop over split configs

    timer model_construct;

    //Create an error model for this element using the local sampling data and save for later use
    ErrorModel_Local<PhysDim> error_model(error1_vec, stepmatrix_vec, error0, order );
    errorModels_[cellGroupGlobal][elem] = error_model;

    time_breakdown[eSynth_t] += model_construct.elapsed();

    MatrixSym R = error_model.getRateMatrix();

    Real R_Fr = DLA::FrobNorm(R);
    if (R_Fr > R_Fr_max)
    {
      R_Fr_max = R_Fr;
      R_max = R;
    }
    if (R_Fr < R_Fr_min)
    {
      R_Fr_min = R_Fr;
      R_min = R;
    }
  } //loop over elements

  time_breakdown[eLocal_t] += clock_local.elapsed();

  timer MPI_report;

#ifdef SANS_MPI
  long localsolve_unconverged_count_Global = 0;
  boost::mpi::reduce(*xfld_linear_.comm(), localsolve_unconverged_count, localsolve_unconverged_count_Global, std::plus<long>(), 0);
  localsolve_unconverged_count = localsolve_unconverged_count_Global;

  long localsolve_count_Global = 0;
  boost::mpi::reduce(*xfld_linear_.comm(), localsolve_count, localsolve_count_Global, std::plus<long>(), 0);
  localsolve_count = localsolve_count_Global;

  long localsolve_iter_sum_Global = 0;
  boost::mpi::reduce(*xfld_linear_.comm(), localsolve_iter_sum, localsolve_iter_sum_Global, std::plus<long>(), 0);
  localsolve_iter_sum = localsolve_iter_sum_Global;

  std::vector<MatrixSym> R_max_rank, R_min_rank;
  boost::mpi::gather(*xfld_linear_.comm(), R_max, R_max_rank, 0 );
  boost::mpi::gather(*xfld_linear_.comm(), R_min, R_min_rank, 0 );

  R_Fr_max = 0, R_Fr_min = std::numeric_limits<Real>::max();
  for (const MatrixSym& R : R_max_rank)
  {
    Real R_Fr = DLA::FrobNorm(R);
    if (R_Fr > R_Fr_max)
    {
      R_Fr_max = R_Fr;
      R_max = R;
    }
    if (R_Fr < R_Fr_min)
    {
      R_Fr_min = R_Fr;
      R_min = R;
    }
  }

  //TODO: Reduce maxRsdNorm0_unconverged and maxRsdNorm_unconverged
#endif

  if (verbosity_ == ParamsType::VerbosityOptions::Progressbar && comm_rank == print_rank) progressbar.end();

  if (localsolve_unconverged_count > 0 && xfld_linear_.comm()->rank() == 0)
  {
    std::cout << "----- MOESS WARNING - Unconverged local solves! -----" << std::endl;
    std::cout << "Initial unconverged Res_L2norm = " << std::setprecision(5) << std::scientific << maxRsdNorm0_unconverged << std::endl;
    std::cout << "Maximum unconverged Res_L2norm = " << std::setprecision(5) << std::scientific << maxRsdNorm_unconverged << std::endl;
    std::cout << std::fixed << std::endl;
    std::cout << std::endl;
  }

  if (verbosity_ >= ParamsType::VerbosityOptions::Progressbar && xfld_linear_.comm()->rank() == 0 )
  {
    std::cout << "Unconverged local solves: " << localsolve_unconverged_count << " out of " << localsolve_count << std::endl;
    std::cout << "Average nonlinear iteration count: " << (Real) localsolve_iter_sum / (Real) localsolve_count << std::endl;
    std::cout << std::endl;

    {
    MatrixSym Rt = R_max;
    Real r = DLA::tr(R_max)/D;
    for (int d = 0; d < D; d++)
      Rt(d,d) -= r;

    std::cout << "Maximum Rate Matrix: " << std::endl << std::scientific << R_max;
    std::cout << "Norm: " << std::scientific << DLA::FrobNorm(R_max) << std::endl;
    std::cout << "Trace Free: " << std::scientific << std::endl << Rt;
    std::cout << "Trace Free Norm: " << std::scientific << DLA::FrobNorm(Rt) << std::endl;
    std::cout << "Trace: " << std::scientific << r << std::endl;
    std::cout << std::endl;
    }

    {
    MatrixSym Rt = R_min;
    Real r = DLA::tr(R_min)/D;
    for (int d = 0; d < D; d++)
      Rt(d,d) -= r;

    std::cout << "Minimum Rate Matrix: " << std::endl << std::scientific << R_min;
    std::cout << "Norm: " << std::scientific << DLA::FrobNorm(R_min) << std::endl;
    std::cout << "Trace Free: " << std::scientific << std::endl << Rt;
    std::cout << "Trace Free Norm: " << std::scientific << DLA::FrobNorm(Rt) << std::endl;
    std::cout << "Trace: " << std::scientific << r << std::endl;
    std::cout << std::endl;
    }
  }

  time_breakdown[eComm_t] += MPI_report.elapsed();
}

//===========================================================================//
template< class PhysDim, class TopoDim>
void
ErrorModel<PhysDim, TopoDim>::
synthesize_edge_node( const XField_CellToTrace<PhysDim, TopoDim>& xfld_connectivity_curved,
                      std::vector<Real>& time_breakdown )
{
  timer clock_local_1;

  typedef typename Simplex<TopoDim>::type Topology; // MOESS assumes purely simplicial meshes

  //Cell Group Types
  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename Field<PhysDim, TopoDim, Real>::template FieldCellGroupType<Topology> QFieldCellGroupType;
  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

  // -------------------------------------- //
  /*
    NOTES:
    Global numbering is the numbering ON the processor
    Native numbering is the UNIQUE numbering ACROSS processors
    Local numbering is the numbering IN the LOCAL patch (for instance the edge to be split is always 0-1 in this numbering)

    The method local2nativeDOFmap is actually erroneously named. It really is the map from PROCESSOR LOCAL (a.k.a. GLOBAL) numbering
    to the native numbering.

    In general the actual patch Local numbering is largely unneeded, so if in doubt, local probably actually means the numbering on this processor
  */
  // -------------------------------------- //

  SANS_ASSERT_MSG( problem_.pifld(), "SolverInterface requires the pifld() method to point to the indicator field");
  const Field<PhysDim,TopoDim,Real>& ifld_curved = *problem_.pifld(); // the efield to be used

  const XField<PhysDim,TopoDim>& xfld_curved = xfld_connectivity_curved.getXField();

  SANS_ASSERT_MSG( &xfld_curved == &ifld_curved.getXField(), "ifld must be built from the same xfield used in the solution" );

  // get the rank of the current processor
  const int comm_rank = xfld_linear_.comm()->rank();
  mpi::communicator comm_local = xfld_linear_.comm()->split(comm_rank);

  // Assuming constant polynomial order solutions
  const int order = problem_.getSolutionOrder(0); //Get solution order
  SANS_ASSERT( order >= 0 );

  // -------------------------------------------------------
  // Constructing the edge_list
  // -------------------------------------------------------

  std::stringstream ss1;
  ss1 << "Constructing edges";

  typedef typename std::vector<std::array<int,Line::NNode>> EdgeList;
  EdgeList edge_list_curved; // This uses the Global node numbering - Those for the rank in question

  const int (*EdgeNodes)[Line::NNode] = ElementEdges<Topology>::EdgeNodes;

  timer edge;

  typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

  // ifield defined on the LINEAR grid - This is needed to match up the models with the SField indexing.
  Field_CG_Cell<PhysDim,TopoDim,Real> ifld_linear(xfld_linear_, 1, BasisFunctionCategory_Lagrange);

  // map from native coordinates to the set of processors that posses a node
  std::map<int,int> node_rank_list; // This uses native numbering

  std::map<int,int> xfldNative2ifldLocalMap; // map from xfld curved native dof to ifld dof in local

  std::array<int,Topology::NNode> xNodeMap, iNodeMap;
  for ( int group = 0; group < xfld_curved.nCellGroups(); group++ )
  {
    const XFieldCellGroupType& xfld_curvedCellGroup = xfld_curved.template getCellGroupGlobal<Topology>(group);
    const QFieldCellGroupType& ifld_curvedCellGroup = ifld_curved.template getCellGroupGlobal<Topology>(group);
          QFieldCellGroupType& ifld_linearCellGroup = ifld_linear.template getCellGroupGlobal<Topology>(group);

    SANS_ASSERT_MSG( ifld_curvedCellGroup.order()==1, "edge local patch can only be used with a nodal error estimate field" );
    SANS_ASSERT( ifld_curvedCellGroup.basis() == ifld_linearCellGroup.basis() );

    ElementQFieldClass ifldElem( ifld_curvedCellGroup.basis() );

    SANS_ASSERT(ifld_curvedCellGroup.nElem() == ifld_linearCellGroup.nElem());

    const int nElem = xfld_curvedCellGroup.nElem(); // The number of elements on the processor
    for ( int elem = 0; elem < nElem; elem++ )
    {
      // extract the ifield dofs from the curved grid
      ifld_curvedCellGroup.getElement( ifldElem, elem );

      // set the element the ifld on the linear grid
      ifld_linearCellGroup.setElement( ifldElem, elem );

      xfld_curvedCellGroup.associativity(elem).getNodeGlobalMapping( xNodeMap.data(), xNodeMap.size() );
      ifld_linearCellGroup.associativity(elem).getNodeGlobalMapping( iNodeMap.data(), iNodeMap.size() );

      for (std::size_t node = 0; node < iNodeMap.size(); node++)
        xfldNative2ifldLocalMap.insert( std::pair<int,int>( xfld_curved.local2nativeDOFmap(xNodeMap[node]),iNodeMap[node]) );

      // loop over the edges
      for ( int edge = 0; edge < Topology::NEdge; edge++ )
      {
        std::array<int,2> xEdge; // global coordinates
        xEdge[0] = xNodeMap[ EdgeNodes[edge][0] ];
        xEdge[1] = xNodeMap[ EdgeNodes[edge][1] ];

        std::array<int,2> iEdge; // global coordinates
        iEdge[0] = iNodeMap[ EdgeNodes[edge][0] ];
        iEdge[1] = iNodeMap[ EdgeNodes[edge][1] ];

        if (ifld_linear.local2nativeDOFmap(iEdge[0]) > ifld_linear.local2nativeDOFmap(iEdge[1]))
        {
          std::swap(xEdge[0], xEdge[1]);
          std::swap(iEdge[0], iEdge[1]);
        }

        // only consider edges where the primary node is possessed (possessed nodes comes first in ifield vector)
        // this rank is responsible for getting the results of this local solve to every other rank
        if (iEdge[0] >= ifld_linear.nDOFpossessed()) continue;

        edge_list_curved.push_back(xEdge); // global numbering becase these are used to construct the patch

        for (std::size_t node = 0; node < iNodeMap.size(); node++)
        {
          if (iNodeMap[node] >= ifld_linear.nDOFpossessed()) // this node is possessed by another processor
          {
            // use ifld to get the rank
            int ghost_rank = ifld_linear.DOFghost_rank(iNodeMap[node] - ifld_linear.nDOFpossessed());

            // index by the cuved x-node to be consistent with sampling processes
            int xNativeNode = xfld_curved.local2nativeDOFmap(xNodeMap[node]);

            node_rank_list[xNativeNode]= ghost_rank;
          }
        }
#if 0
        if ( xNativeEdge[0] == 93 && xNativeEdge[1] == 118 )
        {
           std::cout << "rank " << comm_rank << " has xNativeEdge (93,118) in list" << std::endl;
           std::cout << "rank " << comm_rank << " xEdge = (" << xEdge[0] << "," << xEdge[1] << ")" << std::endl;
           std::cout << "rank " << comm_rank << " iEdge = (" << iEdge[0] << "," << iEdge[1] << ")" << std::endl;
           std::cout << "rank " << comm_rank << " iNativeEdge = (" << ifld_linear.local2nativeDOFmap(iEdge[0]) << ","
                                                                   << ifld_linear.local2nativeDOFmap(iEdge[1]) << ")" << std::endl;
           std::cout << "rank " << comm_rank << " ifld.nDOFpossessed() = " << ifld_curved.nDOFpossessed() << std::endl;
           std::cout << "rank " << comm_rank << " xNodeMap ";
           for ( const int node : xNodeMap ) std::cout << node << ", ";
           std::cout << std::endl;
           std::cout << "rank " << comm_rank << " iNodeMap ";
           for ( const int node : iNodeMap ) std::cout << node << ", ";
           std::cout << std::endl;
        }
#endif
      }
    }
  }

  {
    // unordered_set is likely quicker for unique-ing according to StackOverflow discussions
    // every edge is duplicated approx x2 for 2D, x5 for 3D, x20 for 4D
    // Probably over-kill, but I was learning a lot about how these things work -- Hugh
    std::unordered_set<std::array<int,Line::NNode>,boost::hash<std::array<int,Line::NNode>>> uSetEdges;
    uSetEdges.reserve(edge_list_curved.size());
    // std::set<std::array<int,Line::NNode>> uSetEdges;

    for (auto const& edge : edge_list_curved)
      uSetEdges.insert(edge);
    edge_list_curved.assign( uSetEdges.begin(), uSetEdges.end() );

    edge_list_curved.shrink_to_fit(); // free up the memory that was taken by all the duplicates.
  }

  time_breakdown[eSynth_t] += edge.elapsed();

  // -------------------------------------------------------------------- //
  //            Collect the Element Geometry Data
  // -------------------------------------------------------------------- //

  // unordered_map because we need unique, but we don't actually care about ordered
  // just need the key to find the right value. unordered_map uses a hash function
  // and we need only generate the hash table once because we know the number of entries in advance!
  // We calculate the geometry information for ghost cells too!

  // Linear element for calculating Jacobians -- use linear grid
  ElementXFieldClass xfldElem( xfld_linear_.template getCellGroupGlobal<Topology>(0).basis() );

  int nElem = 0; // This will be used later to reserve additional maps
  for (int group = 0; group < xfld_linear_.nCellGroups(); group++)
    nElem += xfld_linear_.getCellGroupBaseGlobal(group).nElem();

  std::unordered_map<GroupElemIndex,Matrix,boost::hash<GroupElemIndex>> UVT_map;
  UVT_map.reserve(nElem); // reserve enough space in the hash table for all the elements

  Matrix J, Jref; // Jacobians for element, reference element and regular to right angle reference
  Matrix U, VT; // matrices for storing left and right singular vectors
  DLA::VectorS<D,Real> sigma; // for storing the singular values
  const Matrix& invJeq = JacobianEquilateralTransform<PhysDim,TopoDim,Topology>::invJeq;

  // The keys here are global group and elem
  for (int group = 0; group < xfld_linear_.nCellGroups(); group++)
  {
    // all MOESS cell groups have the same Topology
    const XFieldCellGroupType& xfldCellGroup = xfld_linear_.template getCellGroupGlobal<Topology>(group);
    for (int elem = 0; elem < xfldCellGroup.nElem(); elem++)
    {
      xfldCellGroup.getElement( xfldElem, elem );
      xfldElem.jacobian(Jref);

      J = Jref*invJeq;

      SVD( J, U, sigma, VT );

      // The hash table is already reserved so this is a quick insertion
      UVT_map[GroupElemIndex(group,elem)] = U*VT; // place into the map at the relevant location
    }
  }
  // UVT_vec[GroupElemIndex] now contains the relevant rotation matrices for the rotations

  // -------------------------------------------------------
  // Operating on the edge_list
  // -------------------------------------------------------

  typedef typename Field_NodalView::IndexVector IndexVector;

  // struct for storing data for an error model: an initial error and a map from edges to (error,Step) pairs.
  struct ErrorModelData
  {
    ErrorModelData() : error0(), errorStepVector() {}
    ErrorModelData(Real error0) : error0(error0), errorStepVector() {}
    Real error0; // initial error

    std::vector< ErrorStepPair > errorStepVector;
  };

  SANS_ASSERT_MSG(uniform_refinement_ == false, "Uniform Refinement is incompatible with edge split" );

  std::vector<LocalSplitConfig> split_config_list = LocalSplitConfigList<Topology>::get(uniform_refinement_);

  ReferenceStepMatrix<PhysDim, TopoDim> RefStepMatrixList(comm_local, split_config_list); // no isotropic split allowed

  long localsolve_count = 0;
  long localsolve_unconverged_count = 0;
  long localsolve_iter_sum = 0;
  std::vector<std::vector<Real>> maxRsdNorm0_unconverged;
  std::vector<std::vector<Real>> maxRsdNorm_unconverged;

  std::stringstream ss2;
  ss2 << "Sampling edge list";
  ProgressBar progressbar2(ss2.str());

  int print_rank = 0;
#ifdef SANS_MPI
  {
    int nEdgePossessed = edge_list_curved.size();

    // Only let the processors with the most edges print the progress bar
    std::vector<int> nEdgeOnRank;
    boost::mpi::all_gather(*xfld_linear_.comm(), nEdgePossessed, nEdgeOnRank);
    print_rank = std::distance(nEdgeOnRank.begin(), std::max_element(nEdgeOnRank.begin(), nEdgeOnRank.end()));
  }
#endif
  std::vector<Real> split_error; // vector for storing error estimates from the re-solve

  IndexVector nodeGroup0, nodeGroup1; // group elem sets attached to node 0 and node 1
  IndexVector attachedCells; // set of cells attached to the edge
  std::vector<int> attachedNodes; // vector of global numbering nodes in and opposite the edge

  // map from local GroupElemIndex pairs to array of sub-cell steps and whether the edge is reversed
  // use unordered_map as we don't need sort, only unique. Also uses the boost hashing function
  // as works with the GroupElemIndex struct.

  std::unordered_map<GroupElemIndex, MatrixSym, boost::hash<GroupElemIndex> > stepmatrix_map;

  // typedef std::array<MatrixSym,3> MatrixSymArray;
  // std::unordered_map<GroupElemIndex, MatrixSymArray, boost::hash<GroupElemIndex> > stepmatrix_map;
  // std::unordered_map<GroupElemIndex, bool, boost::hash<GroupElemIndex> > edgeReversal_map; // is the edge reversed for the element?

  // Native node to error step pairs
  // All the nodes in this map are native.
  std::map<int, std::map<std::array<int,2>, ErrorStepPair>> nodeErrorStepPairMap;


  // --------------------------------------------------------------------------//
  //                                EDGE LOOP
  // --------------------------------------------------------------------------//

  // Perform the local solves and collect the data into a large map from edges to Map of node to error step pairs
  Field_NodalView nodalView_curved(xfld_curved, cellgroup_list_);

  const Real list_length = edge_list_curved.size()-1;
  for ( std::size_t i_edge = 0; i_edge < edge_list_curved.size(); i_edge++)
  {
    const std::array<int,2>& edge = edge_list_curved[i_edge];

    // construct a split mesh
    timer local_mesh_split_clock;

#define SOLVE_FOR_ERROR0 0

#if SOLVE_FOR_ERROR0
    XField_LocalPatchConstructor<PhysDim,Topology> xfld_construct( comm_local, xfld_connectivity_curved,
                                                                   edge, problem_.spaceType(), &nodalView_curved );
    XField_LocalPatch<PhysDim,Topology> xfld_local_unsplit( xfld_construct );

    attachedNodes = xfld_local_unsplit.getLinearCommonNodes(); // These are in GLOBAL numbering for the LINEAR grid
    std::vector<Real> unsplit_error(attachedNodes.size());
    LocalSolveStatus status_unsplit = problem_.solveLocalProblem(xfld_local_unsplit, unsplit_error);
#endif

    XField_LocalPatch<PhysDim,Topology> xfld_local( comm_local, xfld_connectivity_curved, edge, problem_.spaceType(), &nodalView_curved );

    time_breakdown[eMesh_t] += local_mesh_split_clock.elapsed();

    // nodes that make up the edge, and the first two are node 0 and node 1 from the edge
    // The attached nodes came from when the local grid was generated
    attachedNodes = xfld_local.getLinearCommonNodes(); // These are in GLOBAL numbering for the LINEAR local grid
    split_error.resize(attachedNodes.size()); // These are vertex errors, the first two share the new dof

    // Solve on the split local mesh and compute local error
    LocalSolveStatus status = problem_.solveLocalProblem(xfld_local, split_error);

#if SOLVE_FOR_ERROR0
    for ( std::size_t i_e = 0; i_e < split_error.size(); i_e++)
      split_error[i_e] /= unsplit_error[i_e];
#endif

//     std::cout << "edge = " << edge[0] << "," << edge[1] << std::endl;
//     std::cout << "split_error = " << std::scientific << std::endl;
//     for ( Real err : split_error )
//       std::cout << err << ",";
//     std::cout << std::fixed << std::endl;

    localsolve_count++;
    localsolve_iter_sum += status.iter;
    time_breakdown[eResid_t ] += status.time_residual;
    time_breakdown[eJac_t   ] += status.time_jacobian;
    time_breakdown[eSolve_t ] += status.time_solve;
    time_breakdown[eErrEst_t] += status.time_errest;
    time_breakdown[eAlgEq_t ] += status.time_algeqset;
    time_breakdown[eConst_t ] += status.time_constructor;
    time_breakdown[eDiag_t  ] += status.time_diagnose;

    if (!status.converged)
    {
      //Keep track of the maximum unconverged residual norm
      if (localsolve_unconverged_count == 0)
      {
        maxRsdNorm0_unconverged = status.rsdNorm0_unconverged;
        maxRsdNorm_unconverged = status.rsdNorm_unconverged;
      }
      else
      {
        for (std::size_t i = 0; i < maxRsdNorm_unconverged.size(); i++)
          for (std::size_t j = 0; j < maxRsdNorm_unconverged[i].size(); j++)
          {
            if (maxRsdNorm_unconverged[i][j] < status.rsdNorm_unconverged[i][j])
            {
              maxRsdNorm0_unconverged[i][j] = status.rsdNorm0_unconverged[i][j];
              maxRsdNorm_unconverged[i][j] = status.rsdNorm_unconverged[i][j];
            }
          }
      }
      localsolve_unconverged_count++;
    }

    timer model_colate; // timer for how long it takes to send the local solve data to the right places

    // find the intersection of the two sets
    // these are group and cell from the global grid
    nodeGroup0 = nodalView_curved.getCellList( edge[0] );
    nodeGroup1 = nodalView_curved.getCellList( edge[1] );
    attachedCells.clear();
    std::set_intersection( nodeGroup0.begin(), nodeGroup0.end(), nodeGroup1.begin(), nodeGroup1.end(),
                           std::back_inserter( attachedCells ) );

    // Loop over the attached elements and extract the reference sub-cell steps
    // Also store whether the edge is split relative to the canonical edge.
    // This is necessary to ensure the correct step is used for vertex 0 and 1
    // This map is based on GLOBAL CELL ELEMENT pairs
    stepmatrix_map.clear(); // clear but don't de-allocate memory
    stepmatrix_map.reserve(attachedCells.size()); // reserve more if necessary

    for ( auto const& cell : attachedCells ) // cell and element from the global grid
    {
      // Nodes from the global grid
      const XFieldCellGroupType& xfldCellGroup = xfld_curved.template getCellGroupGlobal<Topology>(cell.group);
      xfldCellGroup.associativity(cell.elem).getNodeGlobalMapping(xNodeMap.data(), xNodeMap.size());

      std::array<int,2> local_edge; // these are global coordinates. edge is also.

      for (int canon_edge = 0; canon_edge < Topology::NEdge; canon_edge++)
      {
        // find the canonical Edge for the current edge
        local_edge[0] = xNodeMap[EdgeNodes[canon_edge][0]];
        local_edge[1] = xNodeMap[EdgeNodes[canon_edge][1]];

        // Make
        const bool firstNodeisNode0 = (local_edge[0] == edge[0]);
        const bool secondNodeisNode0 = (local_edge[1] == edge[0]);
        const bool firstNodeisNode1 = (local_edge[0] == edge[1]);
        const bool secondNodeisNode1 = (local_edge[1] == edge[1]);
        const bool containsNode0 = ( firstNodeisNode0 || secondNodeisNode0 );
        const bool containsNode1 = ( firstNodeisNode1 || secondNodeisNode1 );

        if ( containsNode0 && containsNode1 )
        {
          // compute the edge step and rotate it appropriately
          // MatrixSym Selem = (RefStepMatrixList.getStepMatrix(canon_edge,0) + RefStepMatrixList.getStepMatrix(canon_edge,1))/2;
          const MatrixSym Selem = RefStepMatrixList.getStepMatrix(canon_edge);

          stepmatrix_map[cell] = Metric::rotateRefStepMatrix( Selem, UVT_map[cell] );
          break; // found the canonical edge, don't need to look at the rest
        }
      }
    }

    // Need to figure out which canonical edge the nodes make up for each attached cell
    // and then insert the error into the mapped set corresponding
    SANS_ASSERT_MSG( split_error.size() == attachedNodes.size(), "%i and %i", split_error.size(), attachedNodes.size() );

    const std::array<int,2> xNativeEdge{{ xfld_curved.local2nativeDOFmap(edge[0]), xfld_curved.local2nativeDOFmap(edge[1]) }};

    // loop over the computed nodal error estimates.
    // dispatch them, with their correct Step, to the modelVector indexed by the node
    for (std::size_t iN = 0; iN < split_error.size(); iN++)
    {
      const int node = attachedNodes[iN]; // actual node from the global grid
      const int nativeNode = xfld_curved.local2nativeDOFmap(node); // the native node
      const Real error = split_error[iN]; // indexed using iN, but correspond to attachedNodes

      IndexVector nodeAttachedCells = nodalView_curved.getCellList(node);

      // get the relevant sub-cell steps - averaging if edge is opposite
      MatrixSym S = 0;
      for ( auto const& cell : attachedCells ) // all of the cells attached to the edge
      {
        // is this cell attached to the node we're currently looking at?
        const bool attachedToNode = std::find(nodeAttachedCells.begin(),nodeAttachedCells.end(), cell) != nodeAttachedCells.end();

        if ( !attachedToNode ) continue; // skip a cell whose step doesn't contribute to this node

        S += stepmatrix_map[cell];
      }
      // This step is now the average of all those involved in the split.
      // To get the Log-Euclidean vertex interpolation, need to take the mean
      S /= nodeAttachedCells.size();

      // insert the error, step pair into the map for the node.
      nodeErrorStepPairMap[nativeNode][xNativeEdge] = ErrorStepPair(error,S);
    }
    time_breakdown[eSynth_t] += model_colate.elapsed();

    if (verbosity_ == ParamsType::VerbosityOptions::Progressbar && comm_rank == print_rank )
    {
      progressbar2.update(i_edge/list_length);
    }
    else if (verbosity_ == ParamsType::VerbosityOptions::Detailed)
    {
      std::cout << "  Nodes (" << edge[0] << "," << edge[1] << ") : error = "
                << std::scientific << std::setprecision(3);
      for (std::size_t i = 0; i < split_error.size()-1; i ++ )
        std::cout << split_error[i] << ", ";
      std::cout << split_error[split_error.size()-1] << std::endl;

      std::cout << "                 error0 = ";
      for (std::size_t i = 0; i < attachedNodes.size()-1; i ++ )
        std::cout << ifld_curved.DOF(attachedNodes[i]) << ", ";
      std::cout << ifld_curved.DOF(attachedNodes.size()-1) << std::endl << std::fixed;
    }
  }

  if (verbosity_ == ParamsType::VerbosityOptions::Progressbar && comm_rank == print_rank ) progressbar2.end();

  time_breakdown[eLocal_t] += clock_local_1.elapsed();

#ifdef SANS_MPI
  {
    timer MPI;
    // std::cout << "rank " << comm_rank << " starting to process maps " << std::endl;
    // The map from native Edges to model data has now been filled in.
    // Now it needs to be synchronized across processors

    // construct a set of receive buffers for the maps to be received
    // every processor will fill what they think should be sent, then a processor will only request to update itself
    std::vector< std::map<int, std::map<std::array<int,2> ,ErrorStepPair> > > nodeErrorStepPairMap_Send;
    std::vector< std::map<int, std::map<std::array<int,2>, ErrorStepPair> > > nodeErrorStepPairMap_Recv;

    nodeErrorStepPairMap_Send.resize(xfld_linear_.comm()->size());
    nodeErrorStepPairMap_Recv.resize(xfld_linear_.comm()->size());

    // std::cout << "rank " << comm_rank << " comm_size = " << xfld_curved.comm()->size() << std::endl;

    for (const std::pair<int, int>& node_ranks : node_rank_list)
    {
      const int xNativeNode = node_ranks.first;
      const int rank = node_ranks.second;

      SANS_ASSERT( safe_at(xfldNative2ifldLocalMap, xNativeNode) >= ifld_linear.nDOFpossessed() );

      // copy the keyValue pair into the to send
      nodeErrorStepPairMap_Send[rank][xNativeNode] = safe_at(nodeErrorStepPairMap, xNativeNode);
      nodeErrorStepPairMap.erase(xNativeNode); // not needed by this processor

    }

    boost::mpi::all_to_all( *xfld_linear_.comm(), nodeErrorStepPairMap_Send, nodeErrorStepPairMap_Recv);
    nodeErrorStepPairMap_Send.clear();

    for (const auto& Map : nodeErrorStepPairMap_Recv)
    {
      for (const auto& node_error : Map)
      {
        const int xNativeNode = node_error.first;
        const std::map<std::array<int,2>, ErrorStepPair>& errorStepMap = node_error.second;

        SANS_ASSERT( safe_at(xfldNative2ifldLocalMap, xNativeNode) < ifld_linear.nDOFpossessed() );

        // std::map<std::array<int,2>, ErrorStepPair>& errorMap = safe_at(nodeErrorStepPairMap, xNativeNode);
        std::map<std::array<int,2>, ErrorStepPair>& errorMap = nodeErrorStepPairMap[xNativeNode];
        errorMap.insert(errorStepMap.begin(), errorStepMap.end());
      }
    }
    // std::cout << "rank " << comm_rank << " finished processing maps " << std::endl;
    time_breakdown[eComm_t] += MPI.elapsed();
  }
#endif

  timer clock_local_2;

  // -------------------------------------------------------
  // Collect the data from edge solves to the nodes
  // -------------------------------------------------------

  std::vector<ErrorModelData> modelVector; // indexed using ifld nodes
  modelVector.reserve(ifld_linear.nDOFpossessed()); // local node

  for (int dof = 0; dof < ifld_linear.nDOFpossessed(); dof++) // possessed dofs are all first
    modelVector.emplace_back( ifld_linear.DOF(dof) );

  // -------------------------------------------------------
  // Constructing the nodal error models
  // -------------------------------------------------------

  SANS_ASSERT( (int)nodeErrorStepPairMap.size() == ifld_linear.nDOFpossessed());
  for (const auto& node_error : nodeErrorStepPairMap)
  {
    int xNativeNode = node_error.first;

    int iLocalNode = safe_at(xfldNative2ifldLocalMap, xNativeNode);

    SANS_ASSERT( iLocalNode < ifld_linear.nDOFpossessed() ); // should only be owned by this processor

    for (const std::pair<std::array<int,2>,ErrorStepPair>& error : node_error.second)
      modelVector[iLocalNode].errorStepVector.push_back(error.second);
  }


  // -------------------------------------------------------
  // Constructing the nodal error models
  // -------------------------------------------------------
  timer model_construct;

  // resize and reserve
  errorModels_.resize(1); // Monolithic nodal group - Treat all nodes at the same time.
  errorModels_[0].reserve( ifld_linear.nDOFpossessed() ); // number of possessed nodes in the grid

  Real R_Fr_max = 0, R_Fr_min = std::numeric_limits<Real>::max();
  MatrixSym R_max = 0, R_min = 0;

  // Need to loop over the vector and convert the maps into vectors to pass to ErrorModel_Local
  std::vector<Real> error1_vec;
  std::vector<MatrixSym> stepmatrix_vec;
  for (int node = 0; node < ifld_linear.nDOFpossessed(); node++)
  {
    // extract the vectors from the maps
    error1_vec.clear();
    error1_vec.reserve(modelVector[node].errorStepVector.size());
    stepmatrix_vec.clear();
    stepmatrix_vec.reserve(modelVector[node].errorStepVector.size());

    Real error0 = modelVector[node].error0;

    for (auto const& val: modelVector[node].errorStepVector ) // key through the map
    {
#if SOLVE_FOR_ERROR0
      error1_vec.push_back(val.error*error0);
#else
      error1_vec.push_back(val.error);
#endif
      stepmatrix_vec.push_back(val.step);
    }

    errorModels_[0].push_back(ErrorModel_Local<PhysDim>(error1_vec, stepmatrix_vec, error0, order));

    MatrixSym R = errorModels_[0].back().getRateMatrix();

    Real R_Fr = DLA::FrobNorm(R);
    if (R_Fr > R_Fr_max)
    {
      R_Fr_max = R_Fr;
      R_max = R;
    }
    if (R_Fr < R_Fr_min)
    {
      R_Fr_min = R_Fr;
      R_min = R;
    }
  }
  time_breakdown[eSynth_t] += model_construct.elapsed();
  time_breakdown[eLocal_t] += clock_local_2.elapsed();

#if 0
  std::cout << "errorModels_[0] = " << std::scientific << std::endl;
  for (std::size_t i = 0; i < errorModels_[0].size(); i++)
  {
    std::cout << "node = " << i << std::endl;
    std::cout << "error0 = " << errorModels_[0][i].getError0() << std::endl;
    std::cout << "error1_list() = " <<std::endl;
    for (std::size_t j = 0; j < errorModels_[0][i].error1_list().size(); j++)
      std::cout << errorModels_[0][i].error1_list()[j] << ", ";
    std::cout << "step_matrix_list()(0,0) = " <<std::endl;
    for (std::size_t j = 0; j < errorModels_[0][i].step_matrix_list().size(); j++)
      std::cout << errorModels_[0][i].step_matrix_list()[j](0,0) << ", ";
    std::cout << std::endl;
    std::cout << "step_matrix_list()(0,1) = " <<std::endl;
    for (std::size_t j = 0; j < errorModels_[0][i].step_matrix_list().size(); j++)
      std::cout << errorModels_[0][i].step_matrix_list()[j](0,1) << ", ";
    std::cout << std::endl;
    std::cout << "step_matrix_list()(1,1) = " <<std::endl;
    for (std::size_t j = 0; j < errorModels_[0][i].step_matrix_list().size(); j++)
      std::cout << errorModels_[0][i].step_matrix_list()[j](1,1) << ", ";
    std::cout<< std::endl;
    std::cout << "rate_matrix = " <<std::endl;
    std::cout << errorModels_[0][i].getRateMatrix()(0,0) << ", "
              << errorModels_[0][i].getRateMatrix()(0,1) << ", "
              << errorModels_[0][i].getRateMatrix()(1,1) << std::endl;
  }
  std::cout << std::fixed << std::endl;
#endif

  timer MPI_report;

#ifdef SANS_MPI
  long localsolve_unconverged_count_Global = 0;
  boost::mpi::reduce(*xfld_linear_.comm(), localsolve_unconverged_count, localsolve_unconverged_count_Global, std::plus<long>(), 0);
  localsolve_unconverged_count = localsolve_unconverged_count_Global;

  long localsolve_count_Global = 0;
  boost::mpi::reduce(*xfld_linear_.comm(), localsolve_count, localsolve_count_Global, std::plus<long>(), 0);
  localsolve_count = localsolve_count_Global;

  long localsolve_iter_sum_Global = 0;
  boost::mpi::reduce(*xfld_linear_.comm(), localsolve_iter_sum, localsolve_iter_sum_Global, std::plus<long>(), 0);
  localsolve_iter_sum = localsolve_iter_sum_Global;

  //TODO: Reduce maxRsdNorm0_unconverged and maxRsdNorm_unconverged

  std::vector<MatrixSym> R_max_rank, R_min_rank;
  boost::mpi::gather(*xfld_linear_.comm(), R_max, R_max_rank, 0 );
  boost::mpi::gather(*xfld_linear_.comm(), R_min, R_min_rank, 0 );

  R_Fr_max = 0;
  for (const MatrixSym& R : R_max_rank)
  {
    Real R_Fr = DLA::FrobNorm(R);
    if (R_Fr > R_Fr_max)
    {
      R_Fr_max = R_Fr;
      R_max = R;
    }
  }
  R_Fr_max = std::numeric_limits<Real>::max();
  for (const MatrixSym& R : R_min_rank)
  {
    Real R_Fr = DLA::FrobNorm(R);
    if (R_Fr < R_Fr_min)
    {
      R_Fr_min = R_Fr;
      R_min = R;
    }
  }
#endif

#if 0
  if (localsolve_unconverged_count > 0 && xfld_linear_.comm()->rank() == 0 )
  {
    std::cout << "node = " << i << std::endl;
    std::cout << "error0 = " << errorModels_[0][i].getError0() << std::endl;
    std::cout << "error1_list() = " <<std::endl;
    for (std::size_t j = 0; j < errorModels_[0][i].error1_list().size(); j++)
      std::cout << errorModels_[0][i].error1_list()[j] << ", ";
    std::cout << "step_matrix_list()(0,0) = " <<std::endl;
    for (std::size_t j = 0; j < errorModels_[0][i].step_matrix_list().size(); j++)
      std::cout << errorModels_[0][i].step_matrix_list()[j](0,0) << ", ";
    std::cout << std::endl;
    std::cout << "step_matrix_list()(0,1) = " <<std::endl;
    for (std::size_t j = 0; j < errorModels_[0][i].step_matrix_list().size(); j++)
      std::cout << errorModels_[0][i].step_matrix_list()[j](0,1) << ", ";
    std::cout << std::endl;
    std::cout << "step_matrix_list()(1,1) = " <<std::endl;
    for (std::size_t j = 0; j < errorModels_[0][i].step_matrix_list().size(); j++)
      std::cout << errorModels_[0][i].step_matrix_list()[j](1,1) << ", ";
    std::cout<< std::endl;
    std::cout << "rate_matrix = " <<std::endl;
    std::cout << errorModels_[0][i].getRateMatrix()(0,0) << ", "
              << errorModels_[0][i].getRateMatrix()(0,1) << ", "
              << errorModels_[0][i].getRateMatrix()(1,1) << std::endl;
  }
  std::cout << std::fixed << std::endl;
#endif

  if (localsolve_unconverged_count > 0 && xfld_linear_.comm()->rank() == 0 )
  {
    std::cout << "----- MOESS WARNING - Unconverged local solves! -----" << std::endl;
    std::cout << "Initial unconverged Res_L2norm = " << std::setprecision(5) << std::scientific << maxRsdNorm0_unconverged << std::endl;
    std::cout << "Maximum unconverged Res_L2norm = " << std::setprecision(5) << std::scientific << maxRsdNorm_unconverged << std::endl;
    std::cout << std::fixed << std::endl;
    std::cout << std::endl;
  }

#ifdef SANS_MPI
  if ( verbosity_ >= ParamsType::VerbosityOptions::Progressbar )
  {
    std::size_t minEdgeCount = 0, maxEdgeCount = 0;
    boost::mpi::reduce(*xfld_linear_.comm(), edge_list_curved.size(), minEdgeCount, boost::mpi::minimum<std::size_t>(), 0);
    boost::mpi::reduce(*xfld_linear_.comm(), edge_list_curved.size(), maxEdgeCount, boost::mpi::maximum<std::size_t>(), 0);
    if (comm_rank == 0)
      std::cout << "Min, Max Edge Count : " << minEdgeCount << " " << maxEdgeCount
                << " (ratio " << (Real)maxEdgeCount/(Real)minEdgeCount << ")" << std::endl;
  }
#endif

  if ( verbosity_ >= ParamsType::VerbosityOptions::Progressbar && xfld_linear_.comm()->rank() == 0  )
  {
    std::cout << "Unconverged local solves: " << localsolve_unconverged_count << " out of " << localsolve_count << std::endl;
    std::cout << "Average nonlinear iteration count: " << (Real) localsolve_iter_sum / (Real) localsolve_count << std::endl << std::endl;

    {
    MatrixSym Rt = R_max;
    Real r = DLA::tr(R_max)/D;
    for (int d = 0; d < D; d++)
      Rt(d,d) -= r;

    std::cout << "Maximum Rate Matrix: " << std::endl << std::scientific << R_max;
    std::cout << "Norm: " << std::scientific << DLA::FrobNorm(R_max) << std::endl;
    std::cout << "Trace Free: " << std::scientific << std::endl << Rt;
    std::cout << "Trace Free Norm: " << std::scientific << DLA::FrobNorm(Rt) << std::endl;
    std::cout << "Trace: " << std::scientific << r << std::endl;
    std::cout << std::endl;
    }

    {
    MatrixSym Rt = R_min;
    Real r = DLA::tr(R_min)/D;
    for (int d = 0; d < D; d++)
      Rt(d,d) -= r;

    std::cout << "Minimum Rate Matrix: " << std::endl << std::scientific << R_min;
    std::cout << "Norm: " << std::scientific << DLA::FrobNorm(R_min) << std::endl;
    std::cout << "Trace Free: " << std::scientific << std::endl << Rt;
    std::cout << "Trace Free Norm: " << std::scientific << DLA::FrobNorm(Rt) << std::endl;
    std::cout << "Trace: " << std::scientific << r << std::endl;
    std::cout << std::endl;
    }
  }

  time_breakdown[eComm_t] += MPI_report.elapsed();
}


//===========================================================================//
template< class PhysDim, class TopoDim>
void
ErrorModel<PhysDim, TopoDim>::
synthesize_edge_element( const XField_CellToTrace<PhysDim, TopoDim>& xfld_connectivity_curved,
                         std::vector<Real>& time_breakdown )
{
  timer clock_local_1;

  typedef typename Simplex<TopoDim>::type Topology; // MOESS assumes purely simplicial meshes

  //Cell Group Types
  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename Field<PhysDim, TopoDim, Real>::template FieldCellGroupType<Topology> QFieldCellGroupType;
  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

  // -------------------------------------- //
  /*
    NOTES:
    Global numbering is the numbering ON the processor
    Native numbering is the UNIQUE numbering ACROSS processors
    Local numbering is the numbering IN the LOCAL patch (for instance the edge to be split is always 0-1 in this numbering)

    The method local2nativeDOFmap is actually erroneously named. It really is the map from PROCESSOR LOCAL (a.k.a. GLOBAL) numbering
    to the native numbering.

    In general the actual patch Local numbering is largely unneeded, so if in doubt, local probably actually means the numbering on this processor
  */
  // -------------------------------------- //

  const XField<PhysDim,TopoDim>& xfld_curved = xfld_connectivity_curved.getXField();

  SANS_ASSERT_MSG( problem_.pifld(), "SolverInterface requires the pifld() method to point to the indicator field");
  Field_CG_Cell<PhysDim,TopoDim,Real> ifld_curved(xfld_curved, 1, BasisFunctionCategory_Lagrange); // the efield to be used

  {
    const Field<PhysDim,TopoDim,Real>& ifld = *problem_.pifld();
    for (int n = 0; n < ifld.nDOFpossessed(); n++)
      ifld_curved.DOF(n) = ifld.DOF(n);
  }
  ifld_curved.syncDOFs_MPI_noCache();

  // get the rank of the current processor
  const int comm_rank = xfld_linear_.comm()->rank();
  mpi::communicator comm_local = xfld_linear_.comm()->split(comm_rank);

  // Assuming constant polynomial order solutions
  const int order = problem_.getSolutionOrder(0); //Get solution order
  SANS_ASSERT( order >= 0 );

  // -------------------------------------------------------
  // Constructing the edge_list
  // -------------------------------------------------------

  std::stringstream ss1;
  ss1 << "Constructing edges";

  typedef typename std::vector<std::array<int,Line::NNode>> EdgeList;
  EdgeList edge_list_curved; // This uses the Global node numbering - Those for the rank in question

  const int (*EdgeNodes)[Line::NNode] = ElementEdges<Topology>::EdgeNodes;

  timer edge;

  // size the elemental error models
  errorModels_.resize(xfld_curved.nCellGroups());

  std::array<int,Topology::NNode> xNodeMap, iNodeMap;
  for ( int group = 0; group < xfld_curved.nCellGroups(); group++ )
  {
    const XFieldCellGroupType& xfld_curvedCellGroup = xfld_curved.template getCellGroupGlobal<Topology>(group);
    const QFieldCellGroupType& ifld_curvedCellGroup = ifld_curved.template getCellGroupGlobal<Topology>(group);

    SANS_ASSERT_MSG( ifld_curvedCellGroup.order()==1, "edge local patch can only be used with a nodal error estimate field" );

    const int nElem = xfld_curvedCellGroup.nElem(); // The number of elements on the processor

    // size the elemental models for this group
    errorModels_[group].resize(nElem);

    for ( int elem = 0; elem < nElem; elem++ )
    {
      xfld_curvedCellGroup.associativity(elem).getNodeGlobalMapping( xNodeMap.data(), xNodeMap.size() );
      ifld_curvedCellGroup.associativity(elem).getNodeGlobalMapping( iNodeMap.data(), iNodeMap.size() );

      // loop over the edges
      for ( int edge = 0; edge < Topology::NEdge; edge++ )
      {
        std::array<int,2> xEdge; // global coordinates
        xEdge[0] = xNodeMap[ EdgeNodes[edge][0] ];
        xEdge[1] = xNodeMap[ EdgeNodes[edge][1] ];

        std::array<int,2> iEdge; // global coordinates
        iEdge[0] = iNodeMap[ EdgeNodes[edge][0] ];
        iEdge[1] = iNodeMap[ EdgeNodes[edge][1] ];

        if (ifld_curved.local2nativeDOFmap(iEdge[0]) > ifld_curved.local2nativeDOFmap(iEdge[1]))
        {
          std::swap(xEdge[0], xEdge[1]);
          std::swap(iEdge[0], iEdge[1]);
        }

        // only consider edges where the primary node is possessed (possessed nodes comes first in ifield vector)
        // this rank is responsible for getting the results of this local solve to every other rank
        if (iEdge[0] >= ifld_curved.nDOFpossessed()) continue;

        edge_list_curved.push_back(xEdge); // global numbering becase these are used to construct the patch

#if 0
        if ( xNativeEdge[0] == 93 && xNativeEdge[1] == 118 )
        {
           std::cout << "rank " << comm_rank << " has xNativeEdge (93,118) in list" << std::endl;
           std::cout << "rank " << comm_rank << " xEdge = (" << xEdge[0] << "," << xEdge[1] << ")" << std::endl;
           std::cout << "rank " << comm_rank << " iEdge = (" << iEdge[0] << "," << iEdge[1] << ")" << std::endl;
           std::cout << "rank " << comm_rank << " iNativeEdge = (" << ifld_linear.local2nativeDOFmap(iEdge[0]) << ","
                                                                   << ifld_linear.local2nativeDOFmap(iEdge[1]) << ")" << std::endl;
           std::cout << "rank " << comm_rank << " ifld.nDOFpossessed() = " << ifld_curved.nDOFpossessed() << std::endl;
           std::cout << "rank " << comm_rank << " xNodeMap ";
           for ( const int node : xNodeMap ) std::cout << node << ", ";
           std::cout << std::endl;
           std::cout << "rank " << comm_rank << " iNodeMap ";
           for ( const int node : iNodeMap ) std::cout << node << ", ";
           std::cout << std::endl;
        }
#endif
      }
    }
  }

  {
    // unordered_set is likely quicker for unique-ing according to StackOverflow discussions
    // every edge is duplicated approx x2 for 2D, x5 for 3D, x20 for 4D
    // Probably over-kill, but I was learning a lot about how these things work -- Hugh
    std::unordered_set<std::array<int,Line::NNode>,boost::hash<std::array<int,Line::NNode>>> uSetEdges;
    uSetEdges.reserve(edge_list_curved.size());
    // std::set<std::array<int,Line::NNode>> uSetEdges;

    for (auto const& edge : edge_list_curved)
      uSetEdges.insert(edge);
    edge_list_curved.assign( uSetEdges.begin(), uSetEdges.end() );

    edge_list_curved.shrink_to_fit(); // free up the memory that was taken by all the duplicates.
  }

  time_breakdown[eSynth_t] += edge.elapsed();

  // -------------------------------------------------------------------- //
  //            Collect the Element Geometry Data
  // -------------------------------------------------------------------- //

  // unordered_map because we need unique, but we don't actually care about ordered
  // just need the key to find the right value. unordered_map uses a hash function
  // and we need only generate the hash table once because we know the number of entries in advance!
  // We calculate the geometry information for ghost cells too!

  // Linear element for calculating Jacobians -- use linear grid
  ElementXFieldClass xfldElem( xfld_linear_.template getCellGroupGlobal<Topology>(0).basis() );

  int nElem = 0; // This will be used later to reserve additional maps
  for (int group = 0; group < xfld_linear_.nCellGroups(); group++)
    nElem += xfld_linear_.getCellGroupBaseGlobal(group).nElem();

  std::unordered_map<GroupElemIndex,Matrix,boost::hash<GroupElemIndex>> UVT_map;
  UVT_map.reserve(nElem); // reserve enough space in the hash table for all the elements

  Matrix J, Jref; // Jacobians for element, reference element and regular to right angle reference
  Matrix U, VT; // matrices for storing left and right singular vectors
  DLA::VectorS<D,Real> sigma; // for storing the singular values
  const Matrix& invJeq = JacobianEquilateralTransform<PhysDim,TopoDim,Topology>::invJeq;

  // The keys here are global group and elem
  for (int group = 0; group < xfld_linear_.nCellGroups(); group++)
  {
    // all MOESS cell groups have the same Topology
    const XFieldCellGroupType& xfldCellGroup = xfld_linear_.template getCellGroupGlobal<Topology>(group);
    for (int elem = 0; elem < xfldCellGroup.nElem(); elem++)
    {
      xfldCellGroup.getElement( xfldElem, elem );
      xfldElem.jacobian(Jref);

      J = Jref*invJeq;

      SVD( J, U, sigma, VT );

      // The hash table is already reserved so this is a quick insertion
      UVT_map[GroupElemIndex(group,elem)] = U*VT; // place into the map at the relevant location
    }
  }
  // UVT_vec[GroupElemIndex] now contains the relevant rotation matrices for the rotations

  // -------------------------------------------------------
  // Operating on the edge_list
  // -------------------------------------------------------

  typedef typename Field_NodalView::IndexVector IndexVector;

  // struct for storing data for an error model: an initial error and a map from edges to (error,Step) pairs.
  struct ErrorModelData
  {
    ErrorModelData() : error0(), errorStepVector() {}
    ErrorModelData(Real error0) : error0(error0), errorStepVector() {}
    Real error0; // initial error

    std::vector< ErrorStepPair > errorStepVector;
  };

  SANS_ASSERT_MSG(uniform_refinement_ == false, "Uniform Refinement is incompatible with edge split" );

  std::vector<LocalSplitConfig> split_config_list = LocalSplitConfigList<Topology>::get(uniform_refinement_);

  ReferenceStepMatrix<PhysDim, TopoDim> RefStepMatrixList(comm_local, split_config_list); // no isotropic split allowed

  long localsolve_count = 0;
  long localsolve_unconverged_count = 0;
  long localsolve_iter_sum = 0;
  std::vector<std::vector<Real>> maxRsdNorm0_unconverged;
  std::vector<std::vector<Real>> maxRsdNorm_unconverged;

  std::stringstream ss2;
  ss2 << "Sampling edge list";
  ProgressBar progressbar2(ss2.str());

  int print_rank = 0;
#ifdef SANS_MPI
  {
    int nEdgePossessed = edge_list_curved.size();

    // Only let the processors with the most edges print the progress bar
    std::vector<int> nEdgeOnRank;
    boost::mpi::all_gather(*xfld_linear_.comm(), nEdgePossessed, nEdgeOnRank);
    print_rank = std::distance(nEdgeOnRank.begin(), std::max_element(nEdgeOnRank.begin(), nEdgeOnRank.end()));
  }
#endif
  std::vector<Real> split_error; // vector for storing error estimates from the re-solve

  IndexVector nodeGroup0, nodeGroup1; // group elem sets attached to node 0 and node 1
  IndexVector attachedCells; // set of cells attached to the edge
  std::vector<int> attachedNodes; // vector of global numbering nodes in and opposite the edge

  // map from local GroupElemIndex pairs to array of sub-cell steps and whether the edge is reversed
  // use unordered_map as we don't need sort, only unique. Also uses the boost hashing function
  // as works with the GroupElemIndex struct.

#define SOLVE_FOR_ERROR0 0

  std::unordered_map<GroupElemIndex, Real                      , boost::hash<GroupElemIndex> > error0_map;
  std::unordered_map<GroupElemIndex, Real                      , boost::hash<GroupElemIndex> > error1_map;
  std::unordered_map<GroupElemIndex, MatrixSym                 , boost::hash<GroupElemIndex> > stepmatrix_map;
  std::unordered_map<GroupElemIndex, std::vector<ErrorStepPair>, boost::hash<GroupElemIndex> > elemErrorStepPairMap;

#if SOLVE_FOR_ERROR0
  std::unordered_map<GroupElemIndex, Real                      , boost::hash<GroupElemIndex> > error1_unsplit_map;
#endif

  {
    Field_NodalView iNodalView(ifld_curved);

    const std::vector<int>& nodeDOFList = iNodalView.getNodeDOFList();
    for (int node : nodeDOFList)
    {
      attachedCells = iNodalView.getCellList( node );

      for ( const GroupElemIndex& cell : attachedCells ) // cell and element from the global indicator field
      {
        const QFieldCellGroupType& ifldCellGroup = ifld_curved.template getCellGroupGlobal<Topology>(cell.group);
        if (ifldCellGroup.associativity(cell.elem).rank() != comm_rank) continue;

        // add the fraction if the nodal error0 to the current element
        error0_map[cell] += ifld_curved.DOF(node)/attachedCells.size();
      }
    }
  }

  // --------------------------------------------------------------------------//
  //                                EDGE LOOP
  // --------------------------------------------------------------------------//

  // Perform the local solves and collect the data into a large map from edges to Map of node to error step pairs
  Field_NodalView nodalView_curved(xfld_curved, cellgroup_list_);

  const Real list_length = edge_list_curved.size()-1;
  for ( std::size_t i_edge = 0; i_edge < edge_list_curved.size(); i_edge++)
  {
    const std::array<int,2>& edge = edge_list_curved[i_edge];

    // construct a split mesh
    timer local_mesh_split_clock;

#if SOLVE_FOR_ERROR0
    XField_LocalPatchConstructor<PhysDim,Topology> xfld_construct( comm_local, xfld_connectivity_curved,
                                                                   edge, problem_.spaceType(), &nodalView_curved );
    XField_LocalPatch<PhysDim,Topology> xfld_local_unsplit( xfld_construct );

    attachedNodes = xfld_local_unsplit.getLinearCommonNodes(); // These are in GLOBAL numbering for the LINEAR grid
    std::vector<Real> unsplit_error(attachedNodes.size());
    LocalSolveStatus status_unsplit = problem_.solveLocalProblem(xfld_local_unsplit, unsplit_error);
#endif

    XField_LocalPatch<PhysDim,Topology> xfld_local( comm_local, xfld_connectivity_curved, edge, problem_.spaceType(), &nodalView_curved );

    time_breakdown[eMesh_t] += local_mesh_split_clock.elapsed();

    // nodes that make up the edge, and the first two are node 0 and node 1 from the edge
    // The attached nodes came from when the local grid was generated
    attachedNodes = xfld_local.getLinearCommonNodes(); // These are in GLOBAL numbering for the LINEAR local grid
    split_error.resize(attachedNodes.size()); // These are vertex errors, the first two share the new dof

    // Solve on the split local mesh and compute local error
    LocalSolveStatus status = problem_.solveLocalProblem(xfld_local, split_error);

    // std::cout << "edge = " << edge[0] << "," << edge[1] << std::endl;
    // std::cout << "split_error = " << std::scientific << std::endl;
    // for ( Real err : split_error )
    //   std::cout << err << ",";
    // std::cout << std::fixed << std::endl;

    localsolve_count++;
    localsolve_iter_sum += status.iter;
    time_breakdown[eResid_t ] += status.time_residual;
    time_breakdown[eJac_t   ] += status.time_jacobian;
    time_breakdown[eSolve_t ] += status.time_solve;
    time_breakdown[eErrEst_t] += status.time_errest;
    time_breakdown[eAlgEq_t ] += status.time_algeqset;
    time_breakdown[eConst_t ] += status.time_constructor;
    time_breakdown[eDiag_t  ] += status.time_diagnose;

    if (!status.converged)
    {
      //Keep track of the maximum unconverged residual norm
      if (localsolve_unconverged_count == 0)
      {
        maxRsdNorm0_unconverged = status.rsdNorm0_unconverged;
        maxRsdNorm_unconverged = status.rsdNorm_unconverged;
      }
      else
      {
        for (std::size_t i = 0; i < maxRsdNorm_unconverged.size(); i++)
          for (std::size_t j = 0; j < maxRsdNorm_unconverged[i].size(); j++)
          {
            if (maxRsdNorm_unconverged[i][j] < status.rsdNorm_unconverged[i][j])
            {
              maxRsdNorm0_unconverged[i][j] = status.rsdNorm0_unconverged[i][j];
              maxRsdNorm_unconverged[i][j] = status.rsdNorm_unconverged[i][j];
            }
          }
      }
      localsolve_unconverged_count++;
    }

    timer model_colate; // timer for how long it takes to send the local solve data to the right places

    // find the intersection of the two sets
    // these are group and cell from the global grid
    nodeGroup0 = nodalView_curved.getCellList( edge[0] );
    nodeGroup1 = nodalView_curved.getCellList( edge[1] );
    attachedCells.clear();
    std::set_intersection( nodeGroup0.begin(), nodeGroup0.end(), nodeGroup1.begin(), nodeGroup1.end(),
                           std::back_inserter( attachedCells ) );

    // Loop over the attached elements and extract the reference sub-cell steps
    // Also store whether the edge is split relative to the canonical edge.
    // This is necessary to ensure the correct step is used for vertex 0 and 1
    // This map is based on GLOBAL CELL ELEMENT pairs
    stepmatrix_map.clear(); // clear but don't de-allocate memory
    stepmatrix_map.reserve(attachedCells.size()); // reserve more if necessary
    error1_map.clear();
#if SOLVE_FOR_ERROR0
    error1_unsplit_map.clear();
#endif

    for ( auto const& cell : attachedCells ) // cell and element from the global grid
    {
      // Nodes from the global grid
      const XFieldCellGroupType& xfldCellGroup = xfld_curved.template getCellGroupGlobal<Topology>(cell.group);
      xfldCellGroup.associativity(cell.elem).getNodeGlobalMapping(xNodeMap.data(), xNodeMap.size());

      std::array<int,2> local_edge; // these are global coordinates. edge is also.

      for (int canon_edge = 0; canon_edge < Topology::NEdge; canon_edge++)
      {
        // find the canonical Edge for the current edge
        local_edge[0] = xNodeMap[EdgeNodes[canon_edge][0]];
        local_edge[1] = xNodeMap[EdgeNodes[canon_edge][1]];

        // Make
        const bool firstNodeisNode0 = (local_edge[0] == edge[0]);
        const bool secondNodeisNode0 = (local_edge[1] == edge[0]);
        const bool firstNodeisNode1 = (local_edge[0] == edge[1]);
        const bool secondNodeisNode1 = (local_edge[1] == edge[1]);
        const bool containsNode0 = ( firstNodeisNode0 || secondNodeisNode0 );
        const bool containsNode1 = ( firstNodeisNode1 || secondNodeisNode1 );

        if ( containsNode0 && containsNode1 )
        {
          // compute the edge step and rotate it appropriately
          // MatrixSym Selem = (RefStepMatrixList.getStepMatrix(canon_edge,0) + RefStepMatrixList.getStepMatrix(canon_edge,1))/2;
          const MatrixSym Selem = RefStepMatrixList.getStepMatrix(canon_edge);

          stepmatrix_map[cell] = Metric::rotateRefStepMatrix( Selem, UVT_map[cell] );
          break; // found the canonical edge, don't need to look at the rest
        }
      }
    }

    // Need to figure out which canonical edge the nodes make up for each attached cell
    // and then insert the error into the mapped set corresponding
    SANS_ASSERT_MSG( split_error.size() == attachedNodes.size(), "%i and %i", split_error.size(), attachedNodes.size() );

    // loop over the computed nodal error estimates.
    // dispatch them, with their correct Step, to the modelVector indexed by the node
    for (std::size_t iN = 0; iN < split_error.size(); iN++)
    {
      const int node = attachedNodes[iN]; // actual node from the global grid
      Real error1 = split_error[iN];      // indexed using iN, but correspond to attachedNodes

      IndexVector nodeAttachedCells = nodalView_curved.getCellList(node);

      // distribute the error across each element attached to the node
      error1 /= nodeAttachedCells.size();

#if SOLVE_FOR_ERROR0
      Real error0 = unsplit_error[iN];
      error0 /= nodeAttachedCells.size();
#endif

      // get the relevant sub-cell steps
      for ( auto const& cell : attachedCells ) // all of the cells attached to the edge
      {
        const XFieldCellGroupType& xfldCellGroup = xfld_curved.template getCellGroupGlobal<Topology>(cell.group);
        xfldCellGroup.associativity(cell.elem).getNodeGlobalMapping(xNodeMap.data(), xNodeMap.size());

        // only add the error to elements that contain the node
        if ( std::find(xNodeMap.begin(), xNodeMap.end(), node) != xNodeMap.end() )
        {
          error1_map[cell] += error1;
#if SOLVE_FOR_ERROR0
          error1_unsplit_map[cell] += error0;
#endif
        }
      }
    }

    for ( const std::pair<GroupElemIndex, Real>& cell_error : error1_map )
    {
      const GroupElemIndex& cell = cell_error.first;
#if SOLVE_FOR_ERROR0
      Real error1 = cell_error.second/error1_unsplit_map[cell];
      elemErrorStepPairMap[cell].emplace_back(error1, safe_at(stepmatrix_map, cell));
#else
      elemErrorStepPairMap[cell].emplace_back(cell_error.second, safe_at(stepmatrix_map, cell));
#endif
    }

    time_breakdown[eSynth_t] += model_colate.elapsed();

    if (verbosity_ == ParamsType::VerbosityOptions::Progressbar && comm_rank == print_rank )
    {
      progressbar2.update(i_edge/list_length);
    }
    else if (verbosity_ == ParamsType::VerbosityOptions::Detailed)
    {
      std::cout << "  Nodes (" << edge[0] << "," << edge[1] << ") : error = "
                << std::scientific << std::setprecision(3);
      for (std::size_t i = 0; i < split_error.size()-1; i ++ )
        std::cout << split_error[i] << ", ";
      std::cout << split_error[split_error.size()-1] << std::endl;

      std::cout << "                 error0 = ";
      for (std::size_t i = 0; i < attachedNodes.size()-1; i ++ )
        std::cout << ifld_curved.DOF(attachedNodes[i]) << ", ";
      std::cout << ifld_curved.DOF(attachedNodes.size()-1) << std::endl << std::fixed;

      for ( const std::pair<GroupElemIndex, Real>& cell_error : error1_map )
      {
        const GroupElemIndex& cell = cell_error.first;
        std::cout << "      elem " << cell.elem << std::scientific << std::setprecision(3)
                  <<  " error0 " << safe_at(error0_map, cell) << " error1 ";

        for ( const ErrorStepPair& error1 : safe_at(elemErrorStepPairMap, cell) )
          std::cout << error1.error << ", ";
        std::cout << std::endl << std::fixed;
      }
    }

  }

  if (verbosity_ == ParamsType::VerbosityOptions::Progressbar && comm_rank == print_rank ) progressbar2.end();

  time_breakdown[eLocal_t] += clock_local_1.elapsed();

#ifdef SANS_MPI
  {
    timer MPI;
    // The map from native Edges to model data has now been filled in.
    // Now it needs to be synchronized across processors

    // construct a set of receive buffers for the maps to be received
    // every processor will fill what they think should be sent, then a processor will only request to update itself
    std::vector< std::map<GroupElemIndex, std::vector<ErrorStepPair> > > elemErrorStepPairMap_Send;
    std::vector< std::map<GroupElemIndex, std::vector<ErrorStepPair> > > elemErrorStepPairMap_Recv;

    elemErrorStepPairMap_Send.resize(xfld_linear_.comm()->size());
    elemErrorStepPairMap_Recv.resize(xfld_linear_.comm()->size());

    const std::vector<std::vector<int>>& cellIDs = xfld_curved.cellIDs();
    std::vector<std::map<int,int>> cellID2elem(cellIDs.size());

    for ( int group = 0; group < xfld_curved.nCellGroups(); group++ )
    {
      const XFieldCellGroupType& xfldCellGroup = xfld_curved.template getCellGroupGlobal<Topology>(group);

      const int nElem = xfldCellGroup.nElem(); // The number of elements on the processor
      for ( int elem = 0; elem < nElem; elem++ )
      {
        const int cellID = cellIDs[group][elem];
        cellID2elem[group][cellID] = elem;

        const int rank = xfldCellGroup.associativity(elem).rank();
        if (rank == comm_rank) continue;

        GroupElemIndex cell(group, elem);

        auto it = elemErrorStepPairMap.find(cell);
        if ( it != elemErrorStepPairMap.end())
        {
          GroupElemIndex cellNative(cell.group, cellID);

          elemErrorStepPairMap_Send[rank][cellNative] = it->second;
          elemErrorStepPairMap.erase(it);
        }
      }
    }

    boost::mpi::all_to_all( *xfld_linear_.comm(), elemErrorStepPairMap_Send, elemErrorStepPairMap_Recv);
    elemErrorStepPairMap_Send.clear();

    for (const auto& Map : elemErrorStepPairMap_Recv)
    {
      for (const auto& cell_error : Map)
      {
        const GroupElemIndex& cellNative = cell_error.first;
        const std::vector<ErrorStepPair>& errorStepRecv = cell_error.second;

        const int group = cellNative.group;
        const int elem = safe_at(cellID2elem[group], cellNative.elem);
        GroupElemIndex cell(group, elem);

        const XFieldCellGroupType& xfldCellGroup = xfld_curved.template getCellGroupGlobal<Topology>(group);
        SANS_ASSERT( xfldCellGroup.associativity(elem).rank() == comm_rank );

        // don't use .at() here as the cell may not exist on this processor
        std::vector<ErrorStepPair>& errorVector = elemErrorStepPairMap[cell];
        errorVector.insert(errorVector.begin(), errorStepRecv.begin(), errorStepRecv.end());
      }
    }
    // std::cout << "rank " << comm_rank << " finished processing maps " << std::endl;
    time_breakdown[eComm_t] += MPI.elapsed();
  }
#endif

  timer clock_local_2;

  // -------------------------------------------------------
  // Constructing the elemental error models
  // -------------------------------------------------------
  timer model_construct;

  Real R_Fr_max = 0, R_Fr_min = std::numeric_limits<Real>::max();
  MatrixSym R_max = 0, R_min = 0;

  // Need to loop over the vector and convert the maps into vectors to pass to ErrorModel_Local
  std::vector<Real> error1_vec;
  std::vector<MatrixSym> stepmatrix_vec;
  for (const std::pair<GroupElemIndex, std::vector<ErrorStepPair>>& cell_error : elemErrorStepPairMap )
  {
    const GroupElemIndex& cell = cell_error.first;
    const std::vector<ErrorStepPair>& errorStepVector = cell_error.second;

    const XFieldCellGroupType& xfldCellGroup = xfld_curved.template getCellGroupGlobal<Topology>(cell.group);
    SANS_ASSERT( xfldCellGroup.associativity(cell.elem).rank() == comm_rank );

    // extract the vectors from the maps
    error1_vec.clear();
    error1_vec.reserve(errorStepVector.size());
    stepmatrix_vec.clear();
    stepmatrix_vec.reserve(errorStepVector.size());

    Real error0 = safe_at(error0_map, cell);

    for (auto const& val: errorStepVector ) // key through the map
    {
#if SOLVE_FOR_ERROR0
      error1_vec.push_back(val.error*error0);
#else
      error1_vec.push_back(val.error);
#endif
      stepmatrix_vec.push_back(val.step);
    }

    errorModels_[cell.group][cell.elem] = ErrorModel_Local<PhysDim>(error1_vec, stepmatrix_vec, error0, order);

    const MatrixSym& R = errorModels_[cell.group][cell.elem].getRateMatrix();

    Real R_Fr = DLA::FrobNorm(R);
    if (R_Fr > R_Fr_max)
    {
      R_Fr_max = R_Fr;
      R_max = R;
    }
    if (R_Fr < R_Fr_min)
    {
      R_Fr_min = R_Fr;
      R_min = R;
    }
  }
  time_breakdown[eSynth_t] += model_construct.elapsed();
  time_breakdown[eLocal_t] += clock_local_2.elapsed();

#if 0
  std::cout << "errorModels_[0] = " << std::scientific << std::endl;
  for (std::size_t i = 0; i < errorModels_[0].size(); i++)
  {
    std::cout << "node = " << i << std::endl;
    std::cout << "error0 = " << errorModels_[0][i].getError0() << std::endl;
    std::cout << "error1_list() = " <<std::endl;
    for (std::size_t j = 0; j < errorModels_[0][i].error1_list().size(); j++)
      std::cout << errorModels_[0][i].error1_list()[j] << ", ";
    std::cout << "step_matrix_list()(0,0) = " <<std::endl;
    for (std::size_t j = 0; j < errorModels_[0][i].step_matrix_list().size(); j++)
      std::cout << errorModels_[0][i].step_matrix_list()[j](0,0) << ", ";
    std::cout << std::endl;
    std::cout << "step_matrix_list()(0,1) = " <<std::endl;
    for (std::size_t j = 0; j < errorModels_[0][i].step_matrix_list().size(); j++)
      std::cout << errorModels_[0][i].step_matrix_list()[j](0,1) << ", ";
    std::cout << std::endl;
    std::cout << "step_matrix_list()(1,1) = " <<std::endl;
    for (std::size_t j = 0; j < errorModels_[0][i].step_matrix_list().size(); j++)
      std::cout << errorModels_[0][i].step_matrix_list()[j](1,1) << ", ";
    std::cout<< std::endl;
    std::cout << "rate_matrix = " <<std::endl;
    std::cout << errorModels_[0][i].getRateMatrix()(0,0) << ", "
              << errorModels_[0][i].getRateMatrix()(0,1) << ", "
              << errorModels_[0][i].getRateMatrix()(1,1) << std::endl;
  }
  std::cout << std::fixed << std::endl;
#endif

  timer MPI_report;

#ifdef SANS_MPI
  long localsolve_unconverged_count_Global = 0;
  boost::mpi::reduce(*xfld_linear_.comm(), localsolve_unconverged_count, localsolve_unconverged_count_Global, std::plus<long>(), 0);
  localsolve_unconverged_count = localsolve_unconverged_count_Global;

  long localsolve_count_Global = 0;
  boost::mpi::reduce(*xfld_linear_.comm(), localsolve_count, localsolve_count_Global, std::plus<long>(), 0);
  localsolve_count = localsolve_count_Global;

  long localsolve_iter_sum_Global = 0;
  boost::mpi::reduce(*xfld_linear_.comm(), localsolve_iter_sum, localsolve_iter_sum_Global, std::plus<long>(), 0);
  localsolve_iter_sum = localsolve_iter_sum_Global;

  //TODO: Reduce maxRsdNorm0_unconverged and maxRsdNorm_unconverged

  std::vector<MatrixSym> R_max_rank, R_min_rank;
  boost::mpi::gather(*xfld_linear_.comm(), R_max, R_max_rank, 0 );
  boost::mpi::gather(*xfld_linear_.comm(), R_min, R_min_rank, 0 );

  R_Fr_max = 0;
  for (const MatrixSym& R : R_max_rank)
  {
    Real R_Fr = DLA::FrobNorm(R);
    if (R_Fr > R_Fr_max)
    {
      R_Fr_max = R_Fr;
      R_max = R;
    }
  }
  R_Fr_max = std::numeric_limits<Real>::max();
  for (const MatrixSym& R : R_min_rank)
  {
    Real R_Fr = DLA::FrobNorm(R);
    if (R_Fr < R_Fr_min)
    {
      R_Fr_min = R_Fr;
      R_min = R;
    }
  }
#endif

#if 0
  if (localsolve_unconverged_count > 0 && xfld_linear_.comm()->rank() == 0 )
  {
    std::cout << "node = " << i << std::endl;
    std::cout << "error0 = " << errorModels_[0][i].getError0() << std::endl;
    std::cout << "error1_list() = " <<std::endl;
    for (std::size_t j = 0; j < errorModels_[0][i].error1_list().size(); j++)
      std::cout << errorModels_[0][i].error1_list()[j] << ", ";
    std::cout << "step_matrix_list()(0,0) = " <<std::endl;
    for (std::size_t j = 0; j < errorModels_[0][i].step_matrix_list().size(); j++)
      std::cout << errorModels_[0][i].step_matrix_list()[j](0,0) << ", ";
    std::cout << std::endl;
    std::cout << "step_matrix_list()(0,1) = " <<std::endl;
    for (std::size_t j = 0; j < errorModels_[0][i].step_matrix_list().size(); j++)
      std::cout << errorModels_[0][i].step_matrix_list()[j](0,1) << ", ";
    std::cout << std::endl;
    std::cout << "step_matrix_list()(1,1) = " <<std::endl;
    for (std::size_t j = 0; j < errorModels_[0][i].step_matrix_list().size(); j++)
      std::cout << errorModels_[0][i].step_matrix_list()[j](1,1) << ", ";
    std::cout<< std::endl;
    std::cout << "rate_matrix = " <<std::endl;
    std::cout << errorModels_[0][i].getRateMatrix()(0,0) << ", "
              << errorModels_[0][i].getRateMatrix()(0,1) << ", "
              << errorModels_[0][i].getRateMatrix()(1,1) << std::endl;
  }
  std::cout << std::fixed << std::endl;
#endif

  if (localsolve_unconverged_count > 0 && xfld_linear_.comm()->rank() == 0 )
  {
    std::cout << "----- MOESS WARNING - Unconverged local solves! -----" << std::endl;
    std::cout << "Initial unconverged Res_L2norm = " << std::setprecision(5) << std::scientific << maxRsdNorm0_unconverged << std::endl;
    std::cout << "Maximum unconverged Res_L2norm = " << std::setprecision(5) << std::scientific << maxRsdNorm_unconverged << std::endl;
    std::cout << std::fixed << std::endl;
    std::cout << std::endl;
  }

#ifdef SANS_MPI
  if ( verbosity_ >= ParamsType::VerbosityOptions::Progressbar )
  {
    std::size_t minEdgeCount = 0, maxEdgeCount = 0;
    boost::mpi::reduce(*xfld_linear_.comm(), edge_list_curved.size(), minEdgeCount, boost::mpi::minimum<std::size_t>(), 0);
    boost::mpi::reduce(*xfld_linear_.comm(), edge_list_curved.size(), maxEdgeCount, boost::mpi::maximum<std::size_t>(), 0);
    if (comm_rank == 0)
      std::cout << "Min, Max Edge Count : " << minEdgeCount << " " << maxEdgeCount
                << " (ratio " << (Real)maxEdgeCount/(Real)minEdgeCount << ")" << std::endl;
  }
#endif

  if ( verbosity_ >= ParamsType::VerbosityOptions::Progressbar && xfld_linear_.comm()->rank() == 0  )
  {
    std::cout << "Unconverged local solves: " << localsolve_unconverged_count << " out of " << localsolve_count << std::endl;
    std::cout << "Average nonlinear iteration count: " << (Real) localsolve_iter_sum / (Real) localsolve_count << std::endl << std::endl;

    {
    MatrixSym Rt = R_max;
    Real r = DLA::tr(R_max)/D;
    for (int d = 0; d < D; d++)
      Rt(d,d) -= r;

    std::cout << "Maximum Rate Matrix: " << std::endl << std::scientific << R_max;
    std::cout << "Norm: " << std::scientific << DLA::FrobNorm(R_max) << std::endl;
    std::cout << "Trace Free: " << std::scientific << std::endl << Rt;
    std::cout << "Trace Free Norm: " << std::scientific << DLA::FrobNorm(Rt) << std::endl;
    std::cout << "Trace: " << std::scientific << r << std::endl;
    std::cout << std::endl;
    }

    {
    MatrixSym Rt = R_min;
    Real r = DLA::tr(R_min)/D;
    for (int d = 0; d < D; d++)
      Rt(d,d) -= r;

    std::cout << "Minimum Rate Matrix: " << std::endl << std::scientific << R_min;
    std::cout << "Norm: " << std::scientific << DLA::FrobNorm(R_min) << std::endl;
    std::cout << "Trace Free: " << std::scientific << std::endl << Rt;
    std::cout << "Trace Free Norm: " << std::scientific << DLA::FrobNorm(Rt) << std::endl;
    std::cout << "Trace: " << std::scientific << r << std::endl;
    std::cout << std::endl;
    }
  }

  time_breakdown[eComm_t] += MPI_report.elapsed();
}

#ifdef SANS_MPI
//===========================================================================//
template <class PhysDim, class TopoDim>
class Serialize_CellGroup : public GroupFunctorCellType<Serialize_CellGroup<PhysDim, TopoDim>>
{
public:
  Serialize_CellGroup( const std::vector<int>& cellgroup_list,
                       ErrorModel<PhysDim, TopoDim>& errorModel,
                       const ErrorModel<PhysDim, TopoDim>& errorModelGlobal ) :
    cellgroup_list_(cellgroup_list), errorModel_(errorModel), errorModelGlobal_(errorModelGlobal) {}

  std::size_t nCellGroups() const          { return cellgroup_list_.size(); }
  std::size_t cellGroup(const int n) const { return cellgroup_list_[n];     }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology >
  void
  apply(const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
        const int cellGroupGlobal)
  {
    errorModel_.template serialize_cellgroup<Topology>(xfldCellGroup, cellGroupGlobal, errorModelGlobal_);
  }

protected:
  const std::vector<int> cellgroup_list_;
  ErrorModel<PhysDim, TopoDim>& errorModel_;
  const ErrorModel<PhysDim, TopoDim>& errorModelGlobal_;
};
#endif

//===========================================================================//
template <class PhysDim, class TopoDim>
void
ErrorModel<PhysDim, TopoDim>::
serialize(const ErrorModel<PhysDim, TopoDim>& errorModelGlobal)
{
  // what solve type is in use in the global error model
  const std::string GlobalSolve = errorModelGlobal.LocalSolve_;

  // Re-use the local solve mode from the global solve
  SANS_ASSERT_MSG( LocalSolve_.compare(GlobalSolve) == 0, "Can only serialize from using error models from the same solve type" );

#ifdef SANS_MPI

  if ( LocalSolve_ == ParamsType::params.LocalSolve.Edge ) // error models indexed by [0][ifld native node]
  {
    errorModels_.resize(1);

    std::shared_ptr<mpi::communicator> comm_global = errorModelGlobal.problem_.pifld()->comm();

    // get the rank of the current processor
    int comm_rank = comm_global->rank();
    int comm_size = comm_global->size();

    const Field<PhysDim,TopoDim,Real>& ifld = *errorModelGlobal.problem_.pifld(); // need to extract the maps

    const int nDOFpossessed = ifld.nDOFpossessed();

    const int* pLocal2nativeDOFmap = ifld.local2nativeDOFmap(); // pointer to the dof map
    std::vector<int> local2nativeDOFmap(pLocal2nativeDOFmap,pLocal2nativeDOFmap+nDOFpossessed); // ptr to ptr+offset

    const int MAPID_TAG = 0;
    const int ERRORMODEL_TAG = 1;

    if (comm_rank == 0) // receiving all the models
    {
      // collect the number of dofs on the processors
      std::vector<int> nDOFpossessed_rank;
      boost::mpi::gather(*comm_global,nDOFpossessed,nDOFpossessed_rank, 0 );
      int nDOF = std::accumulate( nDOFpossessed_rank.begin(), nDOFpossessed_rank.end(), 0);
      errorModels_[0].resize(nDOF);

      // copy the error models on rank 0
      for (int node = 0; node < nDOFpossessed; node++)
        errorModels_[0][local2nativeDOFmap[node]] = errorModelGlobal.errorModels_[0][node];

      // receive the models from the other processors and copy them in to the relevant native nodes
      for (int rank = 1; rank < comm_size; rank++)
      {
        // receive the map from the rank
        std::vector<int> local2nativeDOFmap_remote;
        comm_global->recv(rank, MAPID_TAG, local2nativeDOFmap_remote);

        // receive the error models from the rank
        std::vector<ErrorModel_Local<PhysDim>> remoteErrorModels(nDOFpossessed_rank[rank]);
        comm_global->recv(rank, ERRORMODEL_TAG, remoteErrorModels.data(), remoteErrorModels.size());

        for (int node = 0; node < nDOFpossessed_rank[rank]; node++)
          errorModels_[0][local2nativeDOFmap_remote[node]] = remoteErrorModels[node];
      }
    }
    else // sending all the models
    {
      // send the number of possessed dofs to rank 0
      boost::mpi::gather(*comm_global, nDOFpossessed, 0);

      // send the map to rank 0
      comm_global->send(0, MAPID_TAG, local2nativeDOFmap);

      // send the possessed node error models to rank 0
      comm_global->send(0, ERRORMODEL_TAG, errorModelGlobal.errorModels_[0].data(), nDOFpossessed);
    }
  }
  else if (LocalSolve_ == ParamsType::params.LocalSolve.Element ||
           LocalSolve_ == ParamsType::params.LocalSolve.EdgeElement) // error models is indexed by [group][elem]
  {
    // The serialization is done using cell group and elements
    errorModels_.resize(xfld_linear_.nCellGroups());

    for_each_CellGroup<TopoDim>::apply(
        Serialize_CellGroup<PhysDim,TopoDim>(cellgroup_list_, *this, errorModelGlobal),
        errorModelGlobal.xfld_linear_ );
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION("Local Solve type does not work in parallel");
  }

#else
  // serial implementation is a simple copy
  errorModels_.resize(errorModelGlobal.errorModels_.size());
  for (std::size_t i = 0; i < errorModels_.size(); i++)
  {
    errorModels_[i].resize(errorModelGlobal.errorModels_[i].size());
    for (std::size_t j = 0; j < errorModels_[i].size(); j++)
      errorModels_[i][j] = errorModelGlobal.errorModels_[i][j];
  }
#endif
}



#ifdef SANS_MPI
//===========================================================================//
template <class PhysDim, class TopoDim>
template <class Topology>
void
ErrorModel<PhysDim, TopoDim>::
serialize_cellgroup( const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
                     const int cellGroupGlobal,
                     const ErrorModel<PhysDim, TopoDim>& errorModelGlobal )
{
  //Cell Group Types
  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

  ElementXFieldClass xfldElem(xfldCellGroup.basis() );

  // Use the communicator from the global error modal as the
  // serialized xfld_linear_ communicator in this error model has been split into two groups
  // with just rank0 in one of the groups
  std::shared_ptr<mpi::communicator> comm_global = errorModelGlobal.xfld_linear_.comm();

  // get the rank of the current processor
  int comm_rank = comm_global->rank();
  int comm_size = comm_global->size();

  int nElemGlobal = xfldCellGroup.nElem();

  int nElemPossessed = 0;
  for (int elem = 0; elem < nElemGlobal; elem++)
    if (xfldCellGroup.associativity(elem).rank() == comm_rank)
      nElemPossessed++;

  const std::vector<int>& cellIDs = errorModelGlobal.xfld_linear_.cellIDs(cellGroupGlobal);

  const int CELLID_TAG = 0;
  const int ERRORMODEL_TAG = 1;

  if (comm_rank == 0)
  {
    std::vector<int> nElemPossessedRank;
    boost::mpi::gather(*comm_global, nElemPossessed, nElemPossessedRank, 0 );

    int nElemTotal = std::accumulate(nElemPossessedRank.begin(), nElemPossessedRank.end(), 0);

    errorModels_[cellGroupGlobal].resize(nElemTotal);

    // copy over the error models on rank 0
    for (int elem = 0; elem < nElemPossessed; elem++)
      errorModels_[cellGroupGlobal][cellIDs[elem]] = errorModelGlobal.errorModels_[cellGroupGlobal][elem];

    for (int rank = 1; rank < comm_size; rank++)
    {
      std::vector<int> remoteCellIDs;
      comm_global->recv(rank, CELLID_TAG, remoteCellIDs);

      std::vector<ErrorModel_Local<PhysDim>> remoteErrorModels(nElemPossessedRank[rank]);

      comm_global->recv(rank, ERRORMODEL_TAG, remoteErrorModels.data(), remoteErrorModels.size());

      for (std::size_t elem = 0; elem < remoteErrorModels.size(); elem++)
        errorModels_[cellGroupGlobal][remoteCellIDs[elem]] = remoteErrorModels[elem];
    }
  }
  else
  {
    // send the number of cells to rank 0
    boost::mpi::gather(*comm_global, nElemPossessed, 0);

    // send the CellIDs to rank 0
    comm_global->send(0, CELLID_TAG, cellIDs);

    // send cell error models to rank 0
    // this takes advantage of the fact that all ghost cells are last
    comm_global->send(0, ERRORMODEL_TAG, errorModelGlobal.errorModels_[cellGroupGlobal].data(), nElemPossessed);
  }
}
#endif

//===========================================================================//
template <class PhysDim, class TopoDim>
void
ErrorModel<PhysDim, TopoDim>::
getCellsError0(const typename Field_NodalView::IndexVector& cell_list,
               std::vector<Real>& error0_list) const
{
  SANS_ASSERT( cell_list.size() == error0_list.size() );

  for (std::size_t i = 0; i < cell_list.size(); i++)
  {
    int cellgrp = cell_list[i].group;
    int elem = cell_list[i].elem;

    //Get the original error on this element
    error0_list[i] = errorModels_[cellgrp][elem].getError0();
  }
}

//===========================================================================//
template <class PhysDim, class TopoDim>
class ComputeError : public GroupFunctorCellType<ComputeError<PhysDim, TopoDim>>
{
public:
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;
  typedef Field_CG_Cell<PhysDim, TopoDim, MatrixSym> MatrixSymFieldType;

  ComputeError( const std::vector<int>& cellgroup_list,
                ErrorModel<PhysDim, TopoDim>& errorModel,
                const MatrixSymFieldType& Sfld,
                Real& Error, std::vector<MatrixSym>& dError_dS )
  : cellgroup_list_(cellgroup_list), errorModel_(errorModel), Sfld_(Sfld),
    Error_(Error), dError_dS_(dError_dS) {}

  std::size_t nCellGroups() const          { return cellgroup_list_.size(); }
  std::size_t cellGroup(const int n) const { return cellgroup_list_[n];     }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology>
  void
  apply(const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
        const int cellGroupGlobal)
  {
    errorModel_.template computeError_cellgroup<Topology>(Sfld_.template getCellGroupGlobal<Topology>(cellGroupGlobal),
                                                          cellGroupGlobal,
                                                          Error_, dError_dS_);
  }
protected:
  const std::vector<int> cellgroup_list_;

  //Reference to an instance of the error model class so that we can access its members
  ErrorModel<PhysDim, TopoDim>& errorModel_;

  const MatrixSymFieldType& Sfld_;
  Real& Error_;
  std::vector<MatrixSym>& dError_dS_;
};

//===========================================================================//
/*
  Compute the error model when the rate matrices and errors are defined at the nodes rather than elements
*/
template <class PhysDim, class TopoDim>
void
ErrorModel<PhysDim, TopoDim>::
computeNodalError( const MatrixSymFieldType& Sfld,
                   Real& Error, std::vector<MatrixSym>& dError_dS )
{
  // Do surreals need to be evaluated?
  const bool computeDerivatives = (dError_dS.size() != 0);

  // scaling for the diameter of the Frobenius ball!
  // Needs to be epsilon larger than sqrt(D), choose 10% arbitrarily
  const Real beta = 1.1*sqrt(PhysDim::D);
  const Real alpha = 1/(beta*log(2));

  KahanSum<Real> Sum{0.0};
  if (computeDerivatives == false)
  { // No Surreals
    Real elem_error, St2;

    for (int node = 0; node < Sfld.nDOFpossessed(); node++)
    {
      elem_error = 0;
      const MatrixSym& S = Sfld.DOF(node);

      if ( use_modified_model_ )
      {
#if 0
        MatrixSym St = S;
        Real s = DLA::tr(S)/D;
        for (int d = 0; d < D; d++)
          St(d,d) -= s;
#endif
        St2 = 0.5*alpha*DLA::FrobNormSq(S);
        errorModels_[0][node].getErrorEstimate(S, St2, elem_error);
      }
      else
        errorModels_[0][node].getErrorEstimate(S, elem_error);

      Sum += elem_error;
    } // looping over node
  } // not computing derivatives
  else
  { // Surreals
    const int SurrealSize = MatrixSym::SIZE;
    typedef SurrealS<SurrealSize> SurrealClass;
    typedef DLA::MatrixSymS<D,SurrealClass> MatrixSymSurreal;

    MatrixSymSurreal S = 0;
    SurrealClass elem_error;
    SurrealClass St2;

    // set up surreal matrix so that all the derivatives can be computed in one shot
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      S.value(ind).deriv(ind) = 1.0;

    for (int node = 0; node < Sfld.nDOFpossessed(); node++)
    {
      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
        S.value(ind).value() = Sfld.DOF(node).value(ind);

      elem_error = 0;
      if ( use_modified_model_ )
      {
#if 0
        MatrixSymSurreal St = S;
        SurrealClass s = DLA::tr(S)/D;
        for (int d = 0; d < D; d++)
          St(d,d) -= s;
#endif
        St2 = 0.5*alpha*DLA::FrobNormSq(S);
        errorModels_[0][node].getErrorEstimate(S, St2, elem_error);
      }
      else
        errorModels_[0][node].getErrorEstimate(S, elem_error);

      Sum += elem_error.value();

      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
        dError_dS[node].value(ind) += elem_error.deriv(ind);

    } // looping over node
  } // computing derivatives

  Error = static_cast<Real>(Sum);
}

//===========================================================================//
template <class PhysDim, class TopoDim>
void
ErrorModel<PhysDim, TopoDim>::
computeError(const MatrixSymFieldType& Sfld,
             Real& Error, std::vector<MatrixSym>& dError_dSvec)
{
  //Check if the node count is consistent
  SANS_ASSERT( (Sfld.nDOF() == (int)dError_dSvec.size()) || (dError_dSvec.size() == 0) );

  Error = 0.0;
  for (std::size_t i = 0; i < dError_dSvec.size(); i++ )
    dError_dSvec[i] = 0.0;

  if ( LocalSolve_ == ParamsType::params.LocalSolve.Edge )
  {
    // std::cout<< "computing Nodal Error" << std::endl;
    computeNodalError( Sfld, Error, dError_dSvec );
  }
  else
  {
    // This converts nodal step matrices into elemental matrices and then uses them to evaluate the model
    for_each_CellGroup<TopoDim>::apply( ComputeError<PhysDim, TopoDim>(cellgroup_list_, *this, Sfld, Error, dError_dSvec),
                                        xfld_linear_);
  }

#ifdef SANS_MPI
  Error = boost::mpi::all_reduce(*xfld_linear_.comm(), Error, std::plus<Real>());
#endif
}

//===========================================================================//
template <class PhysDim, class TopoDim>
template <class Topology>
void
ErrorModel<PhysDim, TopoDim>::
computeError_cellgroup(const typename MatrixSymFieldType::template FieldCellGroupType<Topology>& SfldCellGroup,
                       const int cellGroupGlobal,
                       Real& Error, std::vector<MatrixSym>& dError_dSvec)
{
  // Do surreals need to be evaluated?
  const bool computeDerivatives = (dError_dSvec.size() != 0 );

  int nElem = SfldCellGroup.nElem(); //Number of elements in cell group
  int globalNodeMap[Topology::NNode];
  int rank = xfld_linear_.comm()->rank();

  // coefficients for the modified model - Hard wired presently
  // Going to 2.0*sqrt(PhysDim::D) gives poor results for L2 corner singularity
  const Real beta = 1.5*sqrt(PhysDim::D); // diameter of the Frobenius ball
  const Real alpha = 1.0/(beta*log(2));

  if ( computeDerivatives == false )
  {
    Real dSt2,elem_error;
    MatrixSym Savg;
    for (int elem = 0; elem < nElem; elem++ )
    {
      if (SfldCellGroup.associativity(elem).rank() != rank) continue;

      //Get the global nodeDOF map for this cell
      SfldCellGroup.associativity(elem).getNodeGlobalMapping(globalNodeMap,Topology::NNode);

      dSt2 = 0; Savg = 0.0; //Average of nodal step matrices

      for (int n = 0; n < Topology::NNode; n++)
      {
        const int node = globalNodeMap[n];
        const MatrixSym& S = SfldCellGroup.DOF(node);
        Savg += S;

        if ( use_modified_model_ )
          dSt2 += 0.5*alpha*DLA::FrobNormSq(S);
      }
      Savg /= Topology::NNode;

      //Evaluate the elemental error using average step matrix
      elem_error = 0.0;

      if (use_modified_model_)
      {
        dSt2 /= Topology::NNode;
        errorModels_[cellGroupGlobal][elem].getErrorEstimate(Savg, dSt2, elem_error);
      }
      else
        errorModels_[cellGroupGlobal][elem].getErrorEstimate(Savg, elem_error);

      Error += elem_error;
    } // loop over elements
  } // not computing Derivatives
  else
  {
    const int SurrealSize = MatrixSym::SIZE * Topology::NNode;
    typedef SurrealS<SurrealSize> SurrealClass;
    typedef DLA::MatrixSymS<D,SurrealClass> MatrixSymSurreal;

    //Create Surreal step matrices for each node of current cell
    MatrixSymSurreal S_elemnodes[Topology::NNode];

    for (int n = 0; n < Topology::NNode; n++)
    {
      S_elemnodes[n] = 0;

      //Set up Surreal matrices so that all derivatives can be computed in one shot
      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
        S_elemnodes[n].value(ind).deriv(n*MatrixSym::SIZE + ind) = 1.0;
    }

    SurrealClass dSt2, elem_error;
    MatrixSymSurreal Savg; //Average of nodal step matrices
    for (int elem = 0; elem < nElem; elem++ )
    {

      //Get the global nodeDOF map for this cell
      SfldCellGroup.associativity(elem).getNodeGlobalMapping(globalNodeMap,Topology::NNode);

      Savg = 0.0; dSt2 = 0.0;
      for (int n = 0; n < Topology::NNode; n++)
      {
        const int node = globalNodeMap[n];
        for (int ind = 0; ind < MatrixSym::SIZE; ind++)
          S_elemnodes[n].value(ind).value() = SfldCellGroup.DOF(node).value(ind);

        Savg += S_elemnodes[n];

        if (use_modified_model_)
          dSt2 += 0.5*alpha*DLA::FrobNormSq(S_elemnodes[n]);
      }
      Savg /= Topology::NNode;


      //Evaluate the elemental error using average step matrix
      elem_error = 0.0;

      if (use_modified_model_)
      {
        dSt2 /= Topology::NNode;
        errorModels_[cellGroupGlobal][elem].getErrorEstimate(Savg, dSt2, elem_error);
      }
      else
        errorModels_[cellGroupGlobal][elem].getErrorEstimate(Savg, elem_error);

      // only accumulate errors from possessed elements
      // derivative information needs to be computed on ghosts as well
      if (SfldCellGroup.associativity(elem).rank() == rank)
        Error += elem_error.value();

      //Redistribute derivatives to nodal step matrices
      for (int n = 0; n < Topology::NNode; n++)
      {
        const int node = globalNodeMap[n];

        //Set up Surreal matrices so that all derivatives can be computed in one shot
        for (int ind = 0; ind < MatrixSym::SIZE; ind++)
          dError_dSvec[node].value(ind) += elem_error.deriv(n*MatrixSym::SIZE + ind);
      }
    } //loop over elements
  } // computing Derivatives
}


//===========================================================================//
#if 0 //def SANS_PETSC
template <class PhysDim, class TopoDim>
class ErrorHessian_impl : public GroupFunctorCellType<ErrorHessian_impl<PhysDim, TopoDim>>
{
public:
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;

  ErrorHessian_impl( const std::vector<int>& cellgroup_list,
                     ErrorModel<PhysDim, TopoDim>& errorModel,
                     const std::vector<MatrixSym>& Svec,
                     Mat H )
  : cellgroup_list_(cellgroup_list), errorModel_(errorModel), Svec_(Svec),
    H_(H) {}

  std::size_t nCellGroups() const          { return cellgroup_list_.size(); }
  std::size_t cellGroup(const int n) const { return cellgroup_list_[n];     }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology>
  void
  apply(const typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
        const int cellGroupGlobal)
  {
    errorModel_.template computeErrorHessian_cellgroup<Topology>(xfldCellGroup, cellGroupGlobal,
                                                                 Svec_, H_);
  }

protected:
  const std::vector<int> cellgroup_list_;

  //Reference to an instance of the error model class so that we can access its members
  ErrorModel<PhysDim, TopoDim>& errorModel_;
  const std::vector<MatrixSym>& Svec_;
  Mat H_;
};

//===========================================================================//
template <class PhysDim, class TopoDim>
void
ErrorModel<PhysDim, TopoDim>::
computeErrorHessian(const std::vector<MatrixSym>& Svec, Mat H)
{
  //Check if the node count is consistent
  SANS_ASSERT( Svec.size() == node_list_.size() );

  for_each_CellGroup<TopoDim>::apply( ErrorHessian_impl<PhysDim, TopoDim>(cellgroup_list_, *this, Svec, H),
                                      xfld_linear_);
}

//===========================================================================//
template <class PhysDim, class TopoDim>
template <class Topology>
void
ErrorModel<PhysDim, TopoDim>::
computeErrorHessian_cellgroup(const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
                              const int cellGroupGlobal, const std::vector<MatrixSym>& Svec,
                              Mat H)
{
  const int SurrealSize = MatrixSym::SIZE * Topology::NNode;
  typedef SurrealS<SurrealSize, SurrealS<SurrealSize,PetscReal>> SurrealClass;
  typedef DLA::MatrixSymS<D,SurrealClass> MatrixSymSurreal;

  //Cell Group Types
  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

  ElementXFieldClass xfldElem(xfldCellGroup.basis() );

  int nElem = xfldCellGroup.nElem(); //Number of elements in cell group
  int globalNodeMap[Topology::NNode];

  //Create Surreal step matrices for each node of current cell
  std::vector<MatrixSymSurreal> S_elemnodes(Topology::NNode,0.0);

  for (int node = 0; node < Topology::NNode; node++)
  {
    //Set up Surreal matrices so that all derivatives can be computed in one shot
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
    {
      S_elemnodes[node].value(ind).deriv(node*MatrixSym::SIZE + ind) = 1.0;
      S_elemnodes[node].value(ind).value().deriv(node*MatrixSym::SIZE + ind) = 1.0;
    }
  }


  for (int elem = 0; elem < nElem; elem++ )
  {
    //Get the global nodeDOF map for this cell
    xfldCellGroup.associativity(elem).getNodeGlobalMapping(globalNodeMap,Topology::NNode);

    MatrixSymSurreal Savg = 0.0; //Average of nodal step matrices

    for (int node = 0; node < Topology::NNode; node++)
    {
      const int local_node = inv_node_list_[globalNodeMap[node]];

      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
        S_elemnodes[node].value(ind).value().value() = Svec[local_node].value(ind);

      Savg += S_elemnodes[node];
    }

    Savg /= Topology::NNode;

    //Evaluate the elemental error using average step matrix
    SurrealClass elem_error = 0.0;
    errorModels_[cellGroupGlobal][elem].getErrorEstimate(Savg, elem_error);

    for (int node_row = 0; node_row < Topology::NNode; node_row++)
    {
      const int local_node_row = inv_node_list_[globalNodeMap[node_row]];
      for (int ind_row = 0; ind_row < MatrixSym::SIZE; ind_row++)
      {
        PetscInt row = local_node_row*MatrixSym::SIZE + ind_row;
        for (int node_col = 0; node_col < Topology::NNode; node_col++)
        {
          const int local_node_col = inv_node_list_[globalNodeMap[node_col]];
          for (int ind_col = 0; ind_col < MatrixSym::SIZE; ind_col++)
          {
            PetscInt col = local_node_col*MatrixSym::SIZE + ind_col;
            int drow = node_row*MatrixSym::SIZE + ind_row;
            int dcol = node_col*MatrixSym::SIZE + ind_col;
            TAO_STATUS( MatSetValues(H, 1, &row, 1, &col, &elem_error.deriv(drow).deriv(dcol), ADD_VALUES) );
          }
        }
      }
    }
  } //loop over elements
}
#endif // SANS_PETSC

//===========================================================================//
template <class PhysDim, class TopoDim>
class RateField_impl : public GroupFunctorCellType<RateField_impl<PhysDim, TopoDim>>
{
public:
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;
  typedef DLA::VectorS<MatrixSym::SIZE, Real> ArrayMatSym; //Data-type to represent symmetric matrix as vector
  typedef Field_DG_Cell<PhysDim, TopoDim, ArrayMatSym> MatrixSymFieldType_Elemental;

  RateField_impl( const std::vector<int>& cellgroup_list,
                  ErrorModel<PhysDim, TopoDim>& errorModel,
                  MatrixSymFieldType_Elemental& ratifld )
  : cellgroup_list_(cellgroup_list), errorModel_(errorModel), ratifld_(ratifld){}

  std::size_t nCellGroups() const          { return cellgroup_list_.size(); }
  std::size_t cellGroup(const int n) const { return cellgroup_list_[n];     }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology>
  void
  apply(const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
        const int cellGroupGlobal)
  {
    errorModel_.template getRateMatrix_cellgroup<Topology>(xfldCellGroup, cellGroupGlobal, ratifld_);
  }

protected:
  const std::vector<int> cellgroup_list_;
  ErrorModel<PhysDim, TopoDim>& errorModel_;
  MatrixSymFieldType_Elemental& ratifld_;
};

//===========================================================================//
template <class PhysDim, class TopoDim>
void
ErrorModel<PhysDim, TopoDim>::
getRateMatrixField(MatrixSymFieldType_Elemental& rate_fld)
{
  SANS_ASSERT( &rate_fld.getXField() == &xfld_linear_ );
  SANS_ASSERT( rate_fld.nCellGroups() == (int)errorModels_.size() );

  for_each_CellGroup<TopoDim>::apply(
      RateField_impl<PhysDim, TopoDim>(cellgroup_list_, *this, rate_fld),
      xfld_linear_ );
}

//===========================================================================//
template <class PhysDim, class TopoDim>
void
ErrorModel<PhysDim, TopoDim>::
getRateMatrixField(MatrixSymFieldType_Nodal& rate_fld)
{
  SANS_ASSERT( &rate_fld.getXField() == &xfld_linear_ );
  SANS_ASSERT(      errorModels_.size() == 1 );
  SANS_ASSERT( (int)errorModels_[0].size() == rate_fld.nDOFpossessed() );

  for (std::size_t dof = 0; dof < errorModels_[0].size(); dof++)
  {
    const MatrixSym& R = errorModels_[0][dof].getRateMatrix();
    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      rate_fld.DOF(dof)[ind] = R.value(ind);
  }

  rate_fld.syncDOFs_MPI_noCache();
}

//===========================================================================//
template <class PhysDim, class TopoDim>
template <class Topology>
void
ErrorModel<PhysDim, TopoDim>::
getRateMatrix_cellgroup(const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
                        const int cellGroupGlobal, MatrixSymFieldType_Elemental& ratifld)
{
  //Cell Group Types
  typedef typename MatrixSymFieldType_Elemental::template FieldCellGroupType<Topology> FieldCellGroupType;
  typedef typename FieldCellGroupType::template ElementType<> ElementFieldClass;

  FieldCellGroupType& fldCellGroup = ratifld.template getCellGroupGlobal<Topology>(cellGroupGlobal);

  ElementFieldClass fldElem(fldCellGroup.basis() );
  SANS_ASSERT_MSG( fldElem.nDOF() == 1, "Rate matrix field needs to be a DG P0 field!" );

  int nElem = fldCellGroup.nElem(); //Number of elements in cell group

  SANS_ASSERT( nElem == (int)errorModels_[cellGroupGlobal].size() );

  for (int elem = 0; elem < nElem; elem++ )
  {
    //Get the global nodeDOF map for this cell
    fldCellGroup.getElement(fldElem, elem);

    //Get the rate matrix from the elemental error model
    const MatrixSym& R = errorModels_[cellGroupGlobal][elem].getRateMatrix();

    for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      fldElem.DOF(0)[ind] = R.value(ind);

    fldCellGroup.setElement(fldElem, elem);
  } //loop over elements
}

} // namespace SANS
