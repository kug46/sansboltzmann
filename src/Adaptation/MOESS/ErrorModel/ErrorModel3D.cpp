// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ERRORMODEL_INSTANTIATE
#include "ErrorModel_impl.h"

#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_CG_Cell.h"

#include "Field/XFieldVolume.h"

namespace SANS
{

//===========================================================================//
//Explicit instantiations
template class ErrorModel<PhysD3,TopoD3>;

} // namespace SANS
