// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef REFERENCEELEMENTCOST_H_
#define REFERENCEELEMENTCOST_H_

#include "tools/SANSException.h"
#include "tools/SANSnumerics.h" // Real
#include "Topology/ElementTopology.h"
#include <cmath> //sqrt

namespace SANS
{

// For CG calculations DOF count's can be fractional, so nDOF must be Real

template <class Topology>
struct ReferenceElementCost;

template <>
struct ReferenceElementCost<Line>
{
  static Real getCost(const Real nDOF)
  {
    return nDOF == 0 ? 1 : fabs(nDOF);
  }
};

template <>
struct ReferenceElementCost<Triangle>
{
  static Real getCost(const Real nDOF)
  {
    if (nDOF == 0)
      return 1;
    else if (nDOF > 0) // Equilateral reference element
      return nDOF*4.0/sqrt(3.0);
    else              // Right angle reference element
      return -nDOF/2.0;
  }
};

template <>
struct ReferenceElementCost<Quad>
{
  static Real getCost(const Real nDOF)
  {
    return nDOF == 0 ? 1 : fabs(nDOF);
  }
};

template <>
struct ReferenceElementCost<Tet>
{
  static Real getCost(const Real nDOF)
  {
    if (nDOF == 0)
      return 1;
    else if (nDOF > 0) // Equilateral reference element
      return nDOF*12.0/sqrt(2.0);
    else              // Right angle reference element
      return -nDOF/6.0;
  }
};

template <>
struct ReferenceElementCost<Hex>
{
  static Real getCost(const Real nDOF)
  {
    return nDOF == 0 ? 1 : fabs(nDOF);
  }
};

template <>
struct ReferenceElementCost<Pentatope>
{
  static Real getCost(const Real nDOF)
  {
    if (nDOF == 0)
      return 1;
    else if (nDOF > 0) // Equilateral reference element
      return nDOF*96/sqrt(5.0);
    else              // Right angle reference element
      return -nDOF/24;
  }
};

}

#endif /* REFERENCEELEMENTCOST_H_ */
