// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "ErrorModel_Local.h"

#include "LinearAlgebra/DenseLinAlg/InverseQR.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_FrobNorm.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

namespace SANS
{

template <class PhysDim>
ErrorModel_Local<PhysDim>::ErrorModel_Local()
: Nconfig_(-1), error0_(0), order_(-1), rate_matrix_(0), alpha_(0) {}

template <class PhysDim>
ErrorModel_Local<PhysDim>::ErrorModel_Local(
    const std::vector<Real>& error1_list, const std::vector<MatrixSym>& step_matrix_list,
    const Real error0, const int order )
: error1_list_(error1_list), step_matrix_list_(step_matrix_list),
  Nconfig_(0), error0_(error0), order_(order), rate_matrix_(0), alpha_(0)
{
  SANS_ASSERT(error1_list.size() == step_matrix_list.size());
  Nconfig_ = (int) error1_list_.size();

  computeRateMatrix();
}

template <class PhysDim>
void
ErrorModel_Local<PhysDim>::getData(const int config_index,
                                       Real& error1, MatrixSym& step_matrix) const
{
  SANS_ASSERT( (config_index >= 0) && (config_index < Nconfig_) );
  error1 = error1_list_[config_index];
  step_matrix = step_matrix_list_[config_index];
}

template <class PhysDim>
void
ErrorModel_Local<PhysDim>::computeRateMatrix()
{
  // RateMatrix = arg min_{Q in SymMatrix} sum_{i=0 to Nconfig-1} ( logErrorRatio[i] - trace(Q*StepMatrix[i]) )^2
  SANS_ASSERT_MSG( error0_ >= 0.0,
                   "Rate matrices can only be calculated with non-negative error0. error0_ = %lf", error0_);

  if (error0_ == 0.0)
  {
    //Error on unsplit element is zero --> rate matrix doesn't matter
    rate_matrix_ = 0.0;
    return;
  }
  else
  {
    int nDOF = D*(D+1)/2;

    DLA::MatrixD<Real> A(Nconfig_,nDOF);
    DLA::VectorD<Real> bvec(Nconfig_);
    DLA::VectorD<Real> Rvec(nDOF); //Rate matrix solution - vectorized

    for (int i = 0; i < Nconfig_; i++)
    {
      if (D == 1)
      {
        A(i,0) = step_matrix_list_[i](0,0);
      }
      else if (D == 2)
      {
        //Off-diagonals multiplied by 2 because of symmetric matrix multiplication
        A(i,0) =   step_matrix_list_[i](0,0);
        A(i,1) = 2*step_matrix_list_[i](1,0);
        A(i,2) =   step_matrix_list_[i](1,1);
      }
      else if (D == 3)
      {
        //Off-diagonals multiplied by 2 because of symmetric matrix multiplication
        A(i,0) =   step_matrix_list_[i](0,0);
        A(i,1) = 2*step_matrix_list_[i](1,0);
        A(i,2) =   step_matrix_list_[i](1,1);
        A(i,3) = 2*step_matrix_list_[i](2,0);
        A(i,4) = 2*step_matrix_list_[i](2,1);
        A(i,5) =   step_matrix_list_[i](2,2);
      }
      else if (D == 4)
      {
        //Off-diagonals multiplied by 2 because of symmetric matrix multiplication
        A(i,0) =   step_matrix_list_[i](0,0);
        A(i,1) = 2*step_matrix_list_[i](1,0);
        A(i,2) =   step_matrix_list_[i](1,1);
        A(i,3) = 2*step_matrix_list_[i](2,0);
        A(i,4) = 2*step_matrix_list_[i](2,1);
        A(i,5) =   step_matrix_list_[i](2,2);
        A(i,6) = 2*step_matrix_list_[i](3,0);
        A(i,7) = 2*step_matrix_list_[i](3,1);
        A(i,8) = 2*step_matrix_list_[i](3,2);
        A(i,9) =   step_matrix_list_[i](3,3);
      }
      else
        SANS_DEVELOPER_EXCEPTION( "ErrorModel<PhysDim>::computeRateMatrix - Unsupported dimension (=%d).", D);

      SANS_ASSERT_MSG( error1_list_[i] >= 0.0, "Rate matrices can only be calculated with non-negative error1. error1 = %lf", error1_list_[i] );

      //Limit error reduction to be within a theoretical bound
      const int order_mod = max(order_, 1); //order needs to be at least 1

      Real f = 0.0;


      // deal with the situation where the refinement gave zero error
      // f += error1_list_[i] == 0 ? error0_ : log(error1_list_[i]/error0_);
      f += error1_list_[i] == 0 ? -2.0*order_mod*log(2.0) : log(error1_list_[i]/error0_); // Max out the reduction, the abs below will take care of it

      // if ( fabs(f) >= 2.0*order_mod*log(2.0) )
      //   std::cout << "correcting data!"<< std::endl;

      f = -min( fabs(f), 2.0*order_mod*log(2.0) );

      // // Subtract off the quadratic trace form
      // Real s = DLA::tr(step_matrix_list_[i])/D;
      // f -= 1e-1*pow(s,2);
      //
      // // Subtract off the quadratic trace free form
      // MatrixSym St = step_matrix_list_[i];
      // for (int d = 0; d < D; d++)
      //   St(d,d) -= s;
      //
      // f -= 2.0*(order_/D)*DLA::FrobNormSq(St);

      bvec[i] = f;
    }


    //Perform least-squares regression
    Rvec = DLA::InverseQR::Solve(A, bvec);

    if (D == 1)
    {
      rate_matrix_(0,0) = Rvec(0);
    }
    else if (D == 2)
    {
      rate_matrix_(0,0) = Rvec(0);
      rate_matrix_(1,0) = Rvec(1);
      rate_matrix_(1,1) = Rvec(2);
    }
    else if (D == 3)
    {
      rate_matrix_(0,0) = Rvec(0);
      rate_matrix_(1,0) = Rvec(1);
      rate_matrix_(1,1) = Rvec(2);
      rate_matrix_(2,0) = Rvec(3);
      rate_matrix_(2,1) = Rvec(4);
      rate_matrix_(2,2) = Rvec(5);
    }
    else if (D == 4)
    {
      rate_matrix_(0,0) = Rvec(0);
      rate_matrix_(1,0) = Rvec(1);
      rate_matrix_(1,1) = Rvec(2);
      rate_matrix_(2,0) = Rvec(3);
      rate_matrix_(2,1) = Rvec(4);
      rate_matrix_(2,2) = Rvec(5);
      rate_matrix_(3,0) = Rvec(6);
      rate_matrix_(3,1) = Rvec(7);
      rate_matrix_(3,2) = Rvec(8);
      rate_matrix_(3,3) = Rvec(9);
    }

#if 0
    Real trR = DLA::tr(rate_matrix_);

    // std::cout << std::endl;
    if ( trR > -1e-9 )
    {
      Real detR = DLA::Det(rate_matrix_);
      Real trR = DLA::tr(rate_matrix_);
      SANS_ASSERT( D == 2 );
      Real lambda1 = 0.5*(trR + sqrt( pow(trR,2) - 4*detR ) );
      Real lambda2 = 0.5*(trR - sqrt( pow(trR,2) - 4*detR ) );
      std::cout << std::scientific << "error0 = " << error0_ << ", lambda = (" << lambda1 << "," << lambda2 << ")" << std::endl;

      // std::cout << std::scientific << "bvec = ";
      // for (int b = 0; b < bvec.size(); b++)
      //   std::cout << bvec[b] << ",";
      // std::cout << std::endl;
      for (std::size_t k = 0; k < step_matrix_list_.size(); k++)
      {
        std::cout << std::scientific << "bvec[" << k <<"] = " << bvec[k] << ", S[" << k <<"] = ";
        for (int i = 0; i < D; i++)
          for (int j = i; j < D; j++)
            std::cout << step_matrix_list_[k](i,j) << ", ";
        std::cout << std::endl;
      }
      std::cout << std::scientific << "rate_matrix_ = " << std::endl << rate_matrix_;
    }
#endif

    // write to the identity
    // rate_matrix_ = DLA::Identity();
    // rate_matrix_ *= -1.0;

#if 0
    alpha_ = 1.0;

    MatrixSym Rt = rate_matrix_;
    Real r = DLA::tr(Rt)/D;
    for (int d = 0; d < D; d++)
      Rt(d,d) -= r;

    Real Rt_Fr = DLA::FrobNorm(Rt);
    if (Rt_Fr > 2.*log(2.))
    {
      Real sigma = 2.*log(2.)/Rt_Fr;
      rate_matrix_ = sigma*Rt;
      for (int d = 0; d < D; d++)
        rate_matrix_(d,d) += r;
    }
    MatrixSym R = rate_matrix_;
    rate_matrix_ = R*(2.*log(2.)/DLA::FrobNorm(rate_matrix_));
#endif

    // This multiplies the St2 term in the eror model.
    // St2 is a scaled FrobNormSq of the step matrix.
    alpha_ = DLA::FrobNorm(rate_matrix_);
  }

}


//Explicit instantiations
template class ErrorModel_Local<PhysD1>;
template class ErrorModel_Local<PhysD2>;
template class ErrorModel_Local<PhysD3>;
template class ErrorModel_Local<PhysD4>;

}
