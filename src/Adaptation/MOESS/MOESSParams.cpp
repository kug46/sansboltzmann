// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "MOESSParams.h"

#define PYDICT_INSTANTIATE
#include "Python/PyDict_impl.h"

namespace SANS
{

PYDICT_PARAMETER_OPTION_INSTANTIATE(MOESSParams::CostModelOptions)
PYDICT_PARAMETER_OPTION_INSTANTIATE(MOESSParams::ImpliedMetricOptions)
PYDICT_PARAMETER_OPTION_INSTANTIATE(MOESSParams::MetricOptimizationOptions)
PYDICT_PARAMETER_OPTION_INSTANTIATE(MOESSParams::VerbosityOptions)
PYDICT_PARAMETER_OPTION_INSTANTIATE(MOESSParams::LocalSolveOptions)
PYDICT_PARAMETER_OPTION_INSTANTIATE(MOESSParams::MetricNormOptions)

// cppcheck-suppress passedByValue
void MOESSParams::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.UniformRefinement));
  //allParams.push_back(d.checkInputs(params.hRefineFactorMax));
  allParams.push_back(d.checkInputs(params.DOFIncreaseFactor));
  allParams.push_back(d.checkInputs(params.FrobNormSqSum_GlobalFraction));
  allParams.push_back(d.checkInputs(params.GradationFactor));
  allParams.push_back(d.checkInputs(params.Optimizer_XTol_Rel));
  allParams.push_back(d.checkInputs(params.Optimizer_FTol_Rel));
  allParams.push_back(d.checkInputs(params.Optimizer_MaxEval));
  allParams.push_back(d.checkInputs(params.ImpliedMetric));
  allParams.push_back(d.checkInputs(params.ImpliedMetric_XTol_Rel));
  allParams.push_back(d.checkInputs(params.ImpliedMetric_FTol_Rel));
  allParams.push_back(d.checkInputs(params.ImpliedMetric_MaxEval));
  allParams.push_back(d.checkInputs(params.MetricOptimization));
  allParams.push_back(d.checkInputs(params.CostModel));
  allParams.push_back(d.checkInputs(params.Verbosity));
  allParams.push_back(d.checkInputs(params.LocalSolve));
  allParams.push_back(d.checkInputs(params.MetricNorm));
  d.checkUnknownInputs(allParams);
}
MOESSParams MOESSParams::params;

}
