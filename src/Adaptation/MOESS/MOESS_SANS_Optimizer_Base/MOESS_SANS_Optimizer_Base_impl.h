// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(MOESS_SANS_OPTIMIZER_BASE_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "../MOESS_SANS_Optimizer_Base.h"

#include <vector>

#include "Field/XField.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_FrobNorm.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Trace.h"

#include "Surreal/SurrealS.h"
#include "tools/smoothmath.h"
#include "tools/linspace.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#endif

namespace SANS
{

//---------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
MOESS_SANS_Optimizer_Base<PhysDim,TopoDim>::
MOESS_SANS_Optimizer_Base( const Real targetCost,
                           std::shared_ptr<ErrorModelType>& errorModel )
  : targetCost_(targetCost),
    errorModel_(errorModel),
    error0_(0),
    h_refine_factor_(0),
    eval_count_(0)
{
}

//---------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
MOESS_SANS_Optimizer_Base<PhysDim,TopoDim>::~MOESS_SANS_Optimizer_Base()
{
}

//---------------------------------------------------------------------------//
//Routine that computes the objective function and its derivative wrt optimization variables
template <class PhysDim, class TopoDim>
Real
MOESS_SANS_Optimizer_Base<PhysDim,TopoDim>::
computeObjective(const MatrixSymFieldType& Sfld, const int nDim, Real* grad)
{
  eval_count_++;

  const int nNode = Sfld.nDOF();
  const int nNodePossessed = Sfld.nDOFpossessed();

  if (grad != nullptr)
    SANS_ASSERT(nDim == nNodePossessed*MatrixSym::SIZE);

  //---------------------------------------------------------------
  Real Error = 0.0;
  std::vector<MatrixSym> dError_dS( grad == nullptr ? 0 : nNode, 0.0);

  //Compute the global error estimate and its derivatives
  errorModel_->computeError(Sfld, Error, dError_dS);
  //---------------------------------------------------------------

  //---------------------------------------------------------------
  Real penalty = 0.0;
  std::vector<MatrixSym> dpenalty_dS( grad == nullptr ? 0 : nNode, 0.0);

  //Add the Frobenius norm sq penalty term and its derivatives
  // addFrobNormPenalty(Sfld, penalty, dpenalty_dS);

  //Add the Trace sq penalty term and its derivatives
  addTracePenalty(Sfld, penalty, dpenalty_dS);

  //Add the eigen value penalty terms if any and its derivatives
  addEigenValuePenalty(Sfld, penalty, dpenalty_dS);

#ifdef SANS_MPI
  if (nodalMetrics_->comm()->size() > 1)
    penalty = boost::mpi::all_reduce(*nodalMetrics_->comm(), penalty, std::plus<Real>());
#endif
  //---------------------------------------------------------------

  // The objective function
  Real obj = Error/error0_ + penalty;
  objective_history_.push_back(obj);

  Real L2norm = 0.0;
  if (grad != nullptr)
  {
    //Populate grad vector
    for (int node = 0; node < nNodePossessed; node++)
    {
      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      {
        Real dobj_dS = dError_dS[node].value(ind)/error0_ + dpenalty_dS[node].value(ind);

        grad[node*MatrixSym::SIZE + ind] = dobj_dS;

        L2norm += dobj_dS*dobj_dS;
      }
    }
  }
#ifdef SANS_MPI
  if (nodalMetrics_->comm()->size() > 1)
    L2norm = boost::mpi::all_reduce(*nodalMetrics_->comm(), L2norm, std::plus<Real>());
#endif
  L2norm = sqrt(L2norm);
  objective_gradnorm_history_.push_back(L2norm);

  return obj;
}

//---------------------------------------------------------------------------//
//Routine that computes the cost constraint and its derivative wrt optimization variables
//Constraint form: computeCostConstraint(x) <= 0
template <class PhysDim, class TopoDim>
Real
MOESS_SANS_Optimizer_Base<PhysDim,TopoDim>::
computeCostConstraint(const MatrixSymFieldType& Sfld, const int nDim, Real* grad)
{
  const int nNode = Sfld.nDOF();
  const int nNodePossessed = Sfld.nDOFpossessed();

  if (grad != nullptr)
    SANS_ASSERT(nDim == nNodePossessed*MatrixSym::SIZE);

  Real Cost = 0.0;
  std::vector<MatrixSym> dCost_dS(grad == nullptr ? 0 : nNode, 0.0);

  //Compute the cost estimate and its derivatives
  nodalMetrics_->computeCost(Sfld, Cost, dCost_dS);

  // compute the constraint
  Real constraint = Cost/targetCost_ - 1;
  cost_constraint_history_.push_back(constraint);

  Real L2norm = 0.0;
  if (grad != nullptr)
  {
    //Populate grad vector from dCost_dSvec
    for (int node = 0; node < nNodePossessed; node++)
    {
      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
      {
        grad[node*MatrixSym::SIZE + ind] = dCost_dS[node].value(ind)/targetCost_;

        L2norm += pow(dCost_dS[node].value(ind),2);
      }
    }
  }
#ifdef SANS_MPI
  if (nodalMetrics_->comm()->size() > 1)
    L2norm = boost::mpi::all_reduce(*nodalMetrics_->comm(), L2norm, std::plus<Real>());
#endif
  L2norm = sqrt(L2norm);
  cost_constraint_gradnorm_history_.push_back(L2norm);

  return constraint;
}

//---------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
void
MOESS_SANS_Optimizer_Base<PhysDim,TopoDim>::
addFrobNormPenalty(const MatrixSymFieldType& Sfld,
                   Real& penalty, std::vector<MatrixSym>& dpenalty_dSvec)
{
  const int SurrealSize = MatrixSym::SIZE;
  typedef SurrealS<SurrealSize> SurrealClass;
  typedef DLA::MatrixSymS<D,SurrealClass> MatrixSymSurreal;

  Real frobNormSqTargetOuter = pow(sqrt(D)*2.0*log(h_refine_factor_),2.0);
  Real frobNormSqTargetInner = pow(        2.0*log(h_refine_factor_),2.0);

  Real frobNormSqTargetDel = D == 1 ? 1e-2 : frobNormSqTargetOuter/frobNormSqTargetInner - 1;

  // smoothmaxC2 with C2 continuity that matches at x = +- eps/2
  //
  // We want x - eps/2 == frobNormSqTargetInner and
  //         x + eps/2 == frobNormSqTargetOuter
  // thus
  //    smoothmaxC2(zero, normSq - frobNormSqTargetInner - frobNormSqTargetDel/2, frobNormSqTargetDel )
  // which normalized is
  //    smoothmaxC2(zero, normSq/frobNormSqTargetInner - 1 - frobNormSqTargetDel/frobNormSqTargetInner/2, frobNormSqTargetDel/frobNormSqTargetInner )

  const int nNodePossessed = Sfld.nDOFpossessed();

  if (dpenalty_dSvec.size() == 0)
  {
    Real node_penalty = 0;
    for (int node = 0; node < nNodePossessed; node++)
    {
      frobNormPenalty(Sfld.DOF(node), frobNormSqTargetInner, frobNormSqTargetDel, node_penalty);

      // accumulate the penalty
      penalty += node_penalty;
    }
  }
  else
  {
    SurrealClass node_penalty = 0;
    for (int node = 0; node < nNodePossessed; node++)
    {
      MatrixSymSurreal S = Sfld.DOF(node); //step matrix for this node

      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
        S.value(ind).deriv(ind) = 1.0;

      frobNormPenalty(S, frobNormSqTargetInner, frobNormSqTargetDel, node_penalty);

      // accumulate the penalty
      penalty += node_penalty.value();

      // and gradient
      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
        dpenalty_dSvec[node].value(ind) += node_penalty.deriv(ind);
    }
  }
}

//---------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
template <class T>
void
MOESS_SANS_Optimizer_Base<PhysDim,TopoDim>::
frobNormPenalty(const DLA::MatrixSymS<D,T>& S, const Real frobNormSqTargetInner, const Real frobNormSqTargetDel, T& penalty) const
{
  //Calculate Frobenius norm square
  T normSq = DLA::FrobNormSq(S);

  //Calculate difference between current Frobenius norm sq and target
  T tmp = normSq/frobNormSqTargetInner - 1 - frobNormSqTargetDel/2;

  //Only add penalty if current Frobenius norm sq is larger (i.e. if tmp >= 0 - but smoothly)
  T tmp_clipped = smoothmaxC2(T(0), tmp, frobNormSqTargetDel );
//  T tmp_clipped = smoothmax(zero, tmp, 20.0);
//  T tmp_clipped = max(T(0), tmp);

  //return quadratic penalty
  penalty = tmp_clipped*tmp_clipped;
}


//---------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
void
MOESS_SANS_Optimizer_Base<PhysDim,TopoDim>::
addTracePenalty(const MatrixSymFieldType& Sfld,
                Real& penalty, std::vector<MatrixSym>& dpenalty_dSvec)
{
  const int SurrealSize = MatrixSym::SIZE;
  typedef SurrealS<SurrealSize> SurrealClass;
  typedef DLA::MatrixSymS<D,SurrealClass> MatrixSymSurreal;

  Real sqTargetOuter = pow(sqrt(D)*2.0*log(h_refine_factor_),2.0);
  Real sqTargetInner = pow(        2.0*log(h_refine_factor_),2.0);

  Real sqTargetDel = D == 1 ? 1e-2 : sqTargetOuter/sqTargetInner - 1;

  // smoothmaxC2 with C2 continuity that matches at x = +- eps/2
  //
  // We want x - eps/2 == sqTargetInner and
  //         x + eps/2 == sqTargetOuter
  // thus
  //    smoothmaxC2(zero, normSq - sqTargetInner - sqTargetDel/2, sqTargetDel )
  // which normalized is
  //    smoothmaxC2(zero, normSq/sqTargetInner - 1 - frobNormSqTargetDel/frobNormSqTargetInner/2, frobNormSqTargetDel/frobNormSqTargetInner )

  const int nNodePossessed = Sfld.nDOFpossessed();

  if (dpenalty_dSvec.size() == 0)
  {
    Real node_penalty = 0;
    for (int node = 0; node < nNodePossessed; node++)
    {
      tracePenalty(Sfld.DOF(node), sqTargetInner, sqTargetDel, node_penalty);

      // accumulate the penalty
      penalty += node_penalty;
    }
  }
  else
  {
    SurrealClass node_penalty = 0;
    for (int node = 0; node < nNodePossessed; node++)
    {
      MatrixSymSurreal S = Sfld.DOF(node); //step matrix for this node

      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
        S.value(ind).deriv(ind) = 1.0;

      tracePenalty(S, sqTargetInner, sqTargetDel, node_penalty);

      // accumulate the penalty
      penalty += node_penalty.value();

      // and gradient
      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
        dpenalty_dSvec[node].value(ind) += node_penalty.deriv(ind);
    }
  }
}

//---------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
template <class T>
void
MOESS_SANS_Optimizer_Base<PhysDim,TopoDim>::
tracePenalty(const DLA::MatrixSymS<D,T>& S, const Real sqTargetInner, const Real sqTargetDel, T& penalty) const
{
  //Calculate Frobenius norm square
  T normSq = pow(DLA::tr(S), 2);

  //Calculate difference between current Frobenius norm sq and target
  T tmp = normSq/sqTargetInner - 1 - sqTargetDel/2;

  //Only add penalty if current Frobenius norm sq is larger (i.e. if tmp >= 0 - but smoothly)
  T tmp_clipped = smoothmaxC2(T(0), tmp, sqTargetDel );

  //return quadratic penalty
  penalty = tmp_clipped*tmp_clipped;
}

//---------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
void
MOESS_SANS_Optimizer_Base<PhysDim,TopoDim>::
addEigenValuePenalty(const MatrixSymFieldType& Sfld,
                     Real& penalty, std::vector<MatrixSym>& dpenalty_dSvec)
{
  const int SurrealSize = MatrixSym::SIZE;
  typedef SurrealS<SurrealSize> SurrealClass;
  typedef DLA::MatrixSymS<D,SurrealClass> MatrixSymSurreal;

  const int nNodePossessed = Sfld.nDOFpossessed();

  if (dpenalty_dSvec.size() == 0)
  {
    Real node_penalty = 0;
    for (int node = 0; node < nNodePossessed; node++)
    {
      eigenValuePenalty(node, Sfld.DOF(node), node_penalty);

      // accumulate the penalty
      penalty += node_penalty;
    }
  }
  else
  {
    SurrealClass node_penalty = 0;
    for (int node = 0; node < nNodePossessed; node++)
    {
      MatrixSymSurreal S = Sfld.DOF(node); //step matrix for this node

      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
        S.value(ind).deriv(ind) = 1.0;

      eigenValuePenalty(node, S, node_penalty);

      // accumulate the penalty
      penalty += node_penalty.value();

      // and gradient
      for (int ind = 0; ind < MatrixSym::SIZE; ind++)
        dpenalty_dSvec[node].value(ind) += node_penalty.deriv(ind);
    }
  }
}

//---------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
template <class T>
void
MOESS_SANS_Optimizer_Base<PhysDim,TopoDim>::
eigenValuePenalty(const int node, const DLA::MatrixSymS<D,T>& S, T& penalty) const
{
  if (h_coarsen_factors_[node] < h_refine_factor_)
  {
    //Obtain eigenvalues of S
    SANS::DLA::VectorS<D,T> L;
    DLA::EigenValues( S, L );

    for (int d = 0; d < D; d++)
    {
      if (L[d] < 0)
      {
        // Calculate difference between current Eigen value and target
        // Want L[d] > -2.0*log(h_coarsen_factors_[node])
        // i.e. -L[d] - 2.0*log(h_coarsen_factors_[node]) < 0
        T tmp = -L[d] - 2.0*log(h_coarsen_factors_[node]);

        //Only add penalty if current Frobenius norm sq is larger (i.e. if tmp >= 0 - but smoothly)
        //T zero = 0.0;
        //T tmp_clipped = smoothmax(zero, tmp, 20.0);

        T tmp_clipped;
        if (tmp > 0)
          tmp_clipped = tmp;
        else
          tmp_clipped = 0.0;

        //return quadratic penalty
        penalty = tmp_clipped*tmp_clipped;
      }
      else
      {
        // Calculate difference between current Eigen value and target
        // Want L[d] < +2.0*log(h_refine_factor_)
        // i.e. L[d] - 2.0*log(h_refine_factor_) < 0
        T tmp = L[d] - 2.0*log(h_refine_factor_);

        //Only add penalty if current Frobenius norm sq is larger (i.e. if tmp >= 0 - but smoothly)
        //T zero = 0.0;
        //T tmp_clipped = smoothmax(zero, tmp, 20.0);

        T tmp_clipped;
        if (tmp > 0)
          tmp_clipped = tmp;
        else
          tmp_clipped = 0.0;

        //return quadratic penalty
        penalty = tmp_clipped*tmp_clipped;
      }
    }
  }
}

//---------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
int
MOESS_SANS_Optimizer_Base<PhysDim,TopoDim>::
countStepMatrixViolations(const MatrixSymFieldType& Sfld) const
{
  int violation_count = 0;

  const int nNode = Sfld.nDOFpossessed();
  for (int node = 0; node < nNode; node++)
  {
    //Obtain eigenvalues of S
    SANS::DLA::VectorS<D,Real> L;
    DLA::EigenValues( Sfld.DOF(node), L );

    for (int d = 0; d < D; d++)
    {
      if ( L[d] < -2.0*log(h_coarsen_factors_[node]) ||
           L[d] > +2.0*log(h_refine_factor_) )
      {
        //Increase the penalty weight for this node if an eigenvalue is larger than the accepted bound
        violation_count++;
        break;
      }
    }
  } //loop over nodes

  return violation_count;
}


//---------------------------------------------------------------------------//
template <class PhysDim, class TopoDim>
Real
MOESS_SANS_Optimizer_Base<PhysDim,TopoDim>::
computeAverageFrobeniusNorm(const MatrixSymFieldType& Sfld) const
{
  Real norm = 0;

  int nNode = Sfld.nDOFpossessed();
  for (int node = 0; node < nNode; node++)
  {
    norm += DLA::FrobNormSq(Sfld.DOF(node));

  } //loop over nodes

#ifdef SANS_MPI
  nNode = boost::mpi::all_reduce( *Sfld.comm(), nNode, std::plus<int>() );
#endif
  norm /= nNode;

  norm = sqrt(norm);

  return norm;
}

}
