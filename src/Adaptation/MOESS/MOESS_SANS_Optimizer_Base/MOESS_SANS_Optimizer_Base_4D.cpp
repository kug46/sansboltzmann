// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define MOESS_SANS_OPTIMIZER_BASE_INSTANTIATE
#include "MOESS_SANS_Optimizer_Base_impl.h"

#include "Field/XFieldSpacetime.h"

#include "Field/FieldSpacetime_CG_Cell.h"

namespace SANS
{

//Explicit instantiations
template class MOESS_SANS_Optimizer_Base<PhysD4,TopoD4>;

template void MOESS_SANS_Optimizer_Base<PhysD4,TopoD4>::frobNormPenalty<Real>(
    const DLA::MatrixSymS<D,Real>& S, const Real sqTargetInner, const Real sqTargetDel, Real& penalty) const;
template void MOESS_SANS_Optimizer_Base<PhysD4,TopoD4>::tracePenalty<Real>(
    const DLA::MatrixSymS<D,Real>& S, const Real sqTargetInner, const Real sqTargetDel, Real& penalty) const;

}
