// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PROBLEMSTATEMENT_FIELDTYPES_HDG_H_
#define PROBLEMSTATEMENT_FIELDTYPES_HDG_H_

#include "Discretization/HDG/AlgebraicEquationSet_HDG.h"
#include "Discretization/HDG/AlgebraicEquationSet_Local_HDG.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_Trace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldVolume_DG_Cell.h"
//#include "Field/FieldVolume_DG_Trace.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"

#include "Field/Local/Field_Local.h"

namespace SANS
{

class HDG;

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class DiscTag, class XFieldType>
struct Disc;

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class XFieldType>
struct Disc<NDPDEClass, BCNDConvert, BCVector, HDG, XFieldType>
{
  typedef AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvert, BCVector, AlgEqSetTraits_Sparse, XFieldType> AlgEqnSet_Global;
  typedef AlgebraicEquationSet_Local_HDG<NDPDEClass, BCNDConvert, BCVector, AlgEqSetTraits_Dense, XFieldType> AlgEqnSet_Local;

  typedef typename NDPDEClass::PhysDim PhysDim;
  typedef typename XFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDPDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef Field_DG_Cell<PhysDim, TopoDim, ArrayQ> QFieldType;
  typedef Field_DG_Cell<PhysDim, TopoDim, VectorArrayQ> AuxFieldType;
  typedef Field_DG_Trace<PhysDim, TopoDim, ArrayQ> QIFieldType;
  typedef Field_DG_BoundaryTrace<PhysDim, TopoDim, ArrayQ> LGFieldType;

  typedef Field_Local<QFieldType> QFieldType_Local;
  typedef Field_Local<AuxFieldType> AuxFieldType_Local;
  typedef Field_Local<QIFieldType> QIFieldType_Local;
  typedef Field_Local<LGFieldType> LGFieldType_Local;

  typedef DiscretizationHDG<NDPDEClass> Params;
};

}

#endif /* PROBLEMSTATEMENT_FIELDTYPES_HDG_H_ */
