// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOLVERINTERFACE_L2PROJECT_H_
#define SOLVERINTERFACE_L2PROJECT_H_

#include <ostream>
#include <vector>

#include "tools/timer.h"
#include "tools/linspace.h"

// Output Functional Stuff that needs to be removed
#include "Discretization/Galerkin/FunctionalCell_Galerkin.h"
#include "Discretization/Galerkin/FunctionalBoundaryTrace_Dispatch_Galerkin.h"
#include "Discretization/Galerkin/JacobianFunctionalBoundaryTrace_Dispatch_Galerkin.h"
#include "Discretization/Galerkin/IntegrandCell_ProjectFunction.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_Output.h"
#include "Discretization/IntegrateCellGroups.h"

#include "pde/OutputCell_SolutionErrorSquared.h"
#include "pde/NDConvert/OutputNDConvert_fwd.h"
#include "pde/NDConvert/FunctionNDConvert_fwd.h"
#include "pde/NDConvert/SolnNDConvert_fwd.h"

#include "pde/AdvectionDiffusion/AdvectionDiffusion_Traits.h"

#include "Adaptation/MOESS/SolverInterfaceBase.h"

#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"
#include "Field/Local/Field_Local.h"
#include "Field/Local/XField_Local_Base.h"

#include "Discretization/Galerkin/IntegrandCell_Project.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Project.h"
#include "Discretization/Galerkin/AlgebraicEquationSet_Local_Project.h"

#include "ErrorEstimate/ErrorEstimate_L2.h"

#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_LinearSolver.h"
#include "LinearAlgebra/DenseLinAlg/InverseLUP.h"

//#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"

#include "Field/output_Tecplot.h"

#include "tools/split_cat_std_vector.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#include "MPI/communicator_fwd.h"
#endif

#include "Meshing/libMeshb/WriteMesh_libMeshb.h"
#include "Meshing/libMeshb/WriteSolution_libMeshb.h"

namespace SANS
{

template<class PhysDim_, class TopoDim_, class SolutionExact, template<class,class,class> class FieldConstructor>
class SolverInterface_L2Project : public SolverInterfaceBase<PhysDim_, TopoDim_>
{
public:
  typedef SolverInterfaceBase<PhysDim_, TopoDim_> BaseType;
  typedef PhysDim_ PhysDim;
  typedef TopoDim_ TopoDim;

  typedef IntegrandCell_ProjectFunction<FunctionNDConvertSpace<PhysDim,Real>,IntegrandCell_ProjFcn_detail::FcnX> IntegrandCellFunctionClass;

  typedef AlgebraicEquationSet_Project< XField<PhysDim, TopoDim>,
      IntegrandCellFunctionClass, TopoDim, AlgEqSetTraits_Sparse > AlgebraicEqSet;

  const int D = PhysDim::D;
  typedef XField<PhysDim, TopoDim> XFieldType;

  typedef typename AlgebraicEqSet::SystemMatrix SystemMatrixClass;
  typedef typename AlgebraicEqSet::SystemVector SystemVectorClass;

  typedef Real ArrayQ;
  typedef Real ArrayJ;

  typedef AlgebraicEqSet AlgEqnSet_Global;

  typedef SolverInterface_L2Project SolverInterfaceClass;
  typedef ErrorEstimate_L2<FunctionNDConvertSpace<PhysDim,Real>,PhysDim,TopoDim,ArrayQ> ErrorEstimateClass;

  // typedef ErrorEstimate_L2 ErrorEstimateClass;

  typedef OutputNDConvertSpace<PhysDim, OutputCell_SolutionErrorSquared<AdvectionDiffusionTraits<PhysDim>, SolutionExact>> L2ErrorClass;
  typedef IntegrandCell_Galerkin_Output<L2ErrorClass> L2ErrorIntegrandClass;

  SolverInterface_L2Project(FieldConstructor<PhysDim,TopoDim,Real>& qfld, const int quadOrder,
                            const std::vector<int>& cellGroups,
                            const std::array<Real,1>& tol_projection,
                            const SolutionExact& solnExact,
                            const PyDict& LinearSolverDict,
                            bool elementwise_projection = false) :
      qfld_(qfld),
      solnExact_(solnExact),
      quadOrder_(quadOrder),
      quadratureOrder_( qfld.getXField(), quadOrder ),
      quadratureOrderLocal_( quadratureOrder_ ),
      fcnND_(solnExact_),
      tol_projection_(tol_projection),
      LinearSolverDict_( LinearSolverDict ),
      cellGroups_(cellGroups),
      outputJ_(0),
      elementwise_projection_(elementwise_projection)
  {

    //Loop over each cellgroup and find the no. of DOF per cell and solution order for that cellgroup
    nDOFperCell_.resize((int) qfld_.nCellGroups(), -1);
    orderVec_.resize((int) qfld_.nCellGroups(), -1);
    basisCat_.resize((int) qfld_.nCellGroups());
    for_each_CellGroup<TopoDim>::apply( CellwiseOperations(cellGroups, *this), qfld_.getXField());

    //Compute the total number of qfld DOFs
    nDOFGlobal_ = qfld_.nDOFpossessed();
#ifdef SANS_MPI
    nDOFGlobal_ = boost::mpi::all_reduce(*qfld_.comm(), nDOFGlobal_, std::plus<int>());
#endif
  }

  virtual ~SolverInterface_L2Project() {}

  virtual void solveGlobalPrimalProblem() override;
  virtual void solveGlobalAdjointProblem() override {}

  virtual void computeErrorEstimates() override;
  virtual Real getElementalErrorEstimate(int cellgroup, int elem) const override { return errorArray_[cellgroup][elem]; }
  // For convenience The estimation is all done for \int (u-u_h)^2, so we sqrt here
  virtual Real getGlobalErrorIndicator() const override { return sqrt(errorIndicator_); }
  virtual Real getGlobalErrorEstimate() const override { return sqrt(errorEstimate_); }

  virtual void output_EField(const std::string& filename) const override;
  virtual const Field<PhysDim,TopoDim,Real>* pifld() const override { return &pErrorEstimate_->getIField(); };

  virtual LocalSolveStatus solveLocalProblem(const XField_Local_Base<PhysDim, TopoDim>& local_xfld, std::vector<Real>& local_error) const override;

  virtual Real getnDOFperCell(const int cellgroup) const override
  {
    SANS_ASSERT(cellgroup >= 0 && cellgroup < (int) nDOFperCell_.size());
    return nDOFperCell_[cellgroup];
  }

  virtual int getSolutionOrder(const int cellgroup) const override
  {
    SANS_ASSERT(cellgroup >= 0 && cellgroup < (int) orderVec_.size());
    return orderVec_[cellgroup];
  }

  virtual SpaceType spaceType() const override { return qfld_.spaceType(); }

  virtual Real getOutput() const override { return outputJ_; }
  virtual int getCost() const override { return nDOFGlobal_; }

  SolverInterface_impl<SolverInterfaceClass>
  CellwiseOperations(const std::vector<int>& cellgroup_list, SolverInterfaceClass& base )
  {
    return SolverInterface_impl<SolverInterfaceClass>(cellgroup_list, base);
  }

  template<class Topology>
  void getSolutionInfo_cellgroup(const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
                                 const int cellGroupGlobal);

protected:
  FieldConstructor<PhysDim,TopoDim,Real>& qfld_;
  std::unique_ptr<Field_DG_Cell<PhysDim,TopoDim,Real>> up_resfld_; // internal field for storing the rglobal residual field
  const SolutionExact solnExact_;
  const int quadOrder_;
  QuadratureOrder quadratureOrder_;
  QuadratureOrder quadratureOrderLocal_;
  FunctionNDConvertSpace<PhysDim,Real> fcnND_;
  const std::array<Real,1> tol_projection_;

  PyDict LinearSolverDict_;
  std::vector<int> cellGroups_;

  std::vector<std::vector<Real>> errorArray_; //Array of elemental error estimates (index:[cellgroup][elem])

  ArrayJ outputJ_; //output functional evaluated at current solution
  Real errorEstimate_; // error `estimate' returned by ErrorEstimateClass
  Real errorIndicator_; // error `estimate' returned by ErrorEstimateClass
  int nDOFGlobal_;

  // has to be Real rather than int, because continuous schemes end up with effectively fractional dof
  std::vector<Real> nDOFperCell_; //indexing: [global cellgrp] - number of degrees of freedom per cell
  std::vector<int> orderVec_; //indexing: [global cellgrp] - solution order of each cellgroup
  std::vector<BasisFunctionCategory> basisCat_;

  bool elementwise_projection_; // whether the projection is done globally or elementwise

  const bool verbosity_ = true; // whether to print timing to screen

  std::unique_ptr<ErrorEstimateClass> pErrorEstimate_;
  bool estimatesEvaluated_=false;

};

template<class PhysDim, class TopoDim, class SolutionExact, template<class,class,class> class FieldConstructor>
void
SolverInterface_L2Project<PhysDim, TopoDim, SolutionExact,FieldConstructor>::
solveGlobalPrimalProblem()
{
  timer clock;

  // Elementwise projection can only be done for Discontinuous fields
  // Check the global field space type
  if (elementwise_projection_ && qfld_.spaceType() == SpaceType::Discontinuous )
  {
    qfld_ = 0;

    for_each_CellGroup<TopoDim>::apply( ProjectSolnCell_Discontinuous(solnExact_, {0}),
                                       (qfld_.getXField(), qfld_) );
  }
  else
  {
    IntegrandCellFunctionClass fcnCell( fcnND_, cellGroups_ );
    AlgEqnSet_Global ProjEqSet(qfld_.getXField(), qfld_, fcnCell, quadratureOrder_, tol_projection_ );

    SystemVectorClass q(ProjEqSet.vectorStateSize());
    SystemVectorClass rhs(ProjEqSet.vectorEqSize());

    ProjEqSet.fillSystemVector(q);

    rhs = 0;
    ProjEqSet.residual(rhs);

    // solver
    SLA::LinearSolver< SystemMatrixClass > solver( LinearSolverDict_, ProjEqSet );

    SystemVectorClass dq(ProjEqSet.vectorStateSize());
    dq = 0; // set initial guess

    solver.solve(rhs, dq);

    q -= dq;

    ProjEqSet.setSolutionField(q);

    // if not whole patch or essential inner patch, then need to construct the residual field
    // for transfer to the local patch because of the broken group interface
#if !defined(WHOLEPATCH) && !defined(INNERPATCH)
    // Fill the residual field - Needed for CG local sovles
    if (qfld_.spaceType() == SpaceType::Continuous)
    {
      // Make a dg version of the field and then do a residual evaluation
      Field_DG_Cell<PhysDim,TopoDim,ArrayQ> qfld_DG(qfld_.getXField(),qfld_.getCellGroupBase(0).order(),
                                                                      qfld_.getCellGroupBase(0).basisCategory(),cellGroups_);

      qfld_.projectTo(qfld_DG);

      // Use the dg field representation to create a dg residual field
      // loops over the elements one at a time and fills - this is as fast as it will get
      DLA::VectorD<ArrayQ> rsd_DG(qfld_DG.nDOFpossessed()); rsd_DG = 0;

      IntegrateCellGroups<TopoDim>::integrate( ResidualCell_Galerkin(fcnCell, rsd_DG),
                                               qfld_DG.getXField(), qfld_DG,
                                               quadratureOrder_.cellOrders.data(), quadratureOrder_.cellOrders.size() );

      // make the residual field. Can use a copy to save on time constructing
      up_resfld_ = SANS::make_unique<Field_DG_Cell<PhysDim,TopoDim,ArrayQ>>(qfld_DG, FieldCopy());

      for (int i = 0; i < qfld_DG.nDOFpossessed(); i++)
        up_resfld_->DOF(i) = rsd_DG[i];

      up_resfld_->syncDOFs_MPI_noCache();
    }
#endif
  }

  if (qfld_.comm()->rank() == 0 )
    std::cout << "Primal solve time: " << std::fixed << std::setprecision(3) << clock.elapsed() << "s" << std::endl;

  // evaluate output function
  outputJ_ = 0.0;

  //Output functional
  L2ErrorClass L2ErrorOutput(solnExact_);
  L2ErrorIntegrandClass L2ErrorIntegrand(L2ErrorOutput, cellGroups_);

  IntegrateCellGroups<TopoDim>::integrate( FunctionalCell_Galerkin( L2ErrorIntegrand, outputJ_ ),
                                           qfld_.getXField(), qfld_,
                                           quadratureOrder_.cellOrders.data(), quadratureOrder_.cellOrders.size() );
  // finish the L2-error with the sqrt
  if (outputJ_ < 0.0)
  {
    // The Grundmann-Moeller quadrature rules (used in 4d) can have negative weights.
    // While the sum of the weights is still 1, this can
    // cause roundoff problems if we are integrating a
    // very wiggly function on a coarse mesh.
    // The output can be negative so take abs before taking
    // sqrt...this will likely cause a wrong answer but
    // as we refine, hopefully the function is better
    // approximated so that there is less wiggliness over the pentatopes.
    SANS_ASSERT( PhysDim::D == 4 );
    printf("warning: quadrature causing numerical roundoff problems -> taking sqrt(abs(L2_Error)).\n");
    printf("         the answer is probably wrong so think about the wiggliness of your function\n");
    printf("         on a coarse mesh such as the one you are currently using.\n");
    outputJ_ = fabs(outputJ_);
  }
  outputJ_ = sqrt(outputJ_);
}

template<class PhysDim, class TopoDim, class SolutionExact, template<class,class,class> class FieldConstructor>
void
SolverInterface_L2Project<PhysDim, TopoDim, SolutionExact,FieldConstructor>::
computeErrorEstimates()
{
  timer clock;
  // Use the implicit casting from bool to int
  const bool estimateOrder = ( qfld_.spaceType() == SpaceType::Continuous ); // 1 if CG, 0 if DG
  // const bool estimateOrder = 1; // 1 if CG, 0 if DG

  pErrorEstimate_ = make_unique<ErrorEstimateClass>( qfld_.getXField(), qfld_, solnExact_, quadratureOrder_,
                                                          estimateOrder, cellGroups_ );

  pErrorEstimate_ -> fillEArray(errorArray_); // fill the vector

  pErrorEstimate_ -> getErrorEstimate(errorEstimate_);
  pErrorEstimate_ -> getErrorIndicator(errorIndicator_);

  // std::cout << "computed global ErrorEstimate " << std::endl;

  if (qfld_.getXField().comm()->rank() == 0 && verbosity_ )
    std::cout << "Error estimation time: " << std::fixed << std::setprecision(3) << clock.elapsed() << "s" << std::endl;

  estimatesEvaluated_ = true;
}


template<class PhysDim, class TopoDim, class SolutionExact, template<class,class,class> class FieldConstructor>
void
SolverInterface_L2Project<PhysDim, TopoDim, SolutionExact,FieldConstructor>::
output_EField(const std::string& filename ) const
{
  SANS_ASSERT_MSG( estimatesEvaluated_, "computeErrorEstimates() must be called before estimates can be output");

  pErrorEstimate_->outputFields(filename);
}


template<class PhysDim, class TopoDim, class SolutionExact, template<class,class,class> class FieldConstructor>
LocalSolveStatus
SolverInterface_L2Project<PhysDim, TopoDim, SolutionExact,FieldConstructor>::
solveLocalProblem(const XField_Local_Base<PhysDim, TopoDim>& local_xfld, std::vector<Real>& local_error) const
{
  // std::cout<< "Solving Local Problem" << std::endl;

#if (!defined(WHOLEPATCH)) && (!defined(INNERPATCH))
  // Split the cell groups!
  Field_Local<FieldConstructor<PhysDim,TopoDim,ArrayQ>> qfld(SANS::split(linspace(0,local_xfld.nCellGroups()-1)),
                                                             local_xfld, qfld_, orderVec_[0], basisCat_[0]);
#else
  // Don't split the cell groups!
  Field_Local<FieldConstructor<PhysDim,TopoDim,ArrayQ>> qfld(local_xfld, qfld_, orderVec_[0], basisCat_[0],linspace(0,local_xfld.nCellGroups()-1));
#endif

  std::vector<int> local_cellGroups = {0};

  QuadratureOrder quadratureOrder(local_xfld, quadOrder_);
  LocalSolveStatus status(true, 0);

  if ( qfld.spaceType() == SpaceType::Discontinuous ) // DG Local Solve!
  {
    qfld = 0;
    for_each_CellGroup<TopoDim>::apply( ProjectSolnCell_Discontinuous(solnExact_, local_cellGroups),
                                        (local_xfld, qfld) );

  }
  else if (qfld.spaceType() == SpaceType::Continuous ) // CG Local Solve!
  {
    // std::cout << "USING CG LOCAL SOLVES" << std::endl;
    // CG LOCAL PATCHES AND LOCAL SOLVE
    // Applies boundary conditions on the CG local patch
    // This represents an approximation, unlike for DG where there is no approximation

    typedef AlgebraicEquationSet_Local_Project< XField<PhysDim, TopoDim>,
        IntegrandCellFunctionClass, TopoDim, AlgEqSetTraits_Dense > AlgEqnSet_Local;

    typedef typename AlgEqnSet_Local::SystemVector SystemVectorClass;
    typedef typename AlgEqnSet_Local::SystemMatrix SystemMatrixClass;
    typedef typename AlgEqnSet_Local::SystemNonZeroPattern SystemNonZeroPattern;

    std::unique_ptr<Field_DG_Cell<PhysDim,TopoDim,Real>> up_local_res_fld;

    // If not whole or inner patch, then need to construct the local residual field for use in the solve
#if !defined(WHOLEPATCH) && !defined(INNERPATCH)
    SANS_ASSERT_MSG( up_resfld_, "CG Local solves require a residual field" );
    timer construct1;
    up_local_res_fld = SANS::make_unique<Field_Local<Field_DG_Cell<PhysDim,TopoDim,Real>>>
      (local_xfld,*up_resfld_, up_resfld_->getCellGroupBase(0).order(), up_resfld_->getCellGroupBase(0).basisCategory());
    status.time_constructor += construct1.elapsed();
#endif

    // whole patch requires that all cell groups be solved over
#ifdef WHOLEPATCH
    local_cellGroups = linspace(0,local_xfld.nCellGroups()-1);
#endif

    IntegrandCellFunctionClass fcnCell( fcnND_, local_cellGroups );

    timer algeqset;
    AlgEqnSet_Local algEqnSet_local(local_xfld, qfld, up_local_res_fld, fcnCell, quadratureOrder, tol_projection_);
    status.time_algeqset += algeqset.elapsed();

    // Local vectors
    SystemVectorClass q_local   (algEqnSet_local.vectorStateSize()); // current solution
    SystemVectorClass dsln_local(algEqnSet_local.vectorStateSize()); // solution update
    SystemVectorClass rsd_local (algEqnSet_local.vectorEqSize()); // residual

    // Convert the projected qfld to a vector
    algEqnSet_local.fillSystemVector(q_local);

    // Compute the lifting operators (rfld)
    algEqnSet_local.setSolutionField(q_local);

    // Local Jacobian
    SystemNonZeroPattern nz_local(algEqnSet_local.matrixSize());
    SystemMatrixClass jac_local(nz_local);

    timer clock_residual;
    rsd_local = 0;
    algEqnSet_local.residual(rsd_local);

    status.time_residual += clock_residual.elapsed();

    // save off the current residual with the projected solution
    std::vector<std::vector<Real>> rsdNorm0( algEqnSet_local.residualNorm(rsd_local) );
    std::vector<std::vector<Real>> rsdNorm = rsdNorm0;

    //Jacobian
    timer clock_jacobian;
    jac_local = 0;
    algEqnSet_local.jacobian(jac_local);

    status.time_jacobian += clock_jacobian.elapsed();

    //Perform local solve
    timer clock_solve;

    dsln_local = DLA::InverseLUP::Solve(jac_local, rsd_local);

    status.time_solve += clock_solve.elapsed();

    //Update local solution vector
    q_local -= dsln_local;

    // save the residual for this iteration
    rsdNorm = algEqnSet_local.residualNorm(rsd_local);

    // checking the convergence at the end of the loop ensures at least one Newton iteration
    // this helps the error model as the tol used in algEqnSet_local is not necessarily precise
    if ( algEqnSet_local.convergedResidual(rsdNorm) )
    {
      status.converged = true;
    }

    algEqnSet_local.setSolutionField(q_local);

  }

  // Compute the local error indicator
  const bool estimateOrder = ( qfld_.spaceType() == SpaceType::Continuous ); // 1 if CG, 0 if DG
  // const bool estimateOrder = 1; // 1 if CG, 0 if DG

  std::unique_ptr<Field_DG_Cell<PhysDim,TopoDim,Real>> up_local_est_fld_;

  // If not whole patch, then fill the estimate field based on the global solve
  // this is then transfered into the local patch
#ifndef WHOLEPATCH
  if (estimateOrder == 1)
  {
    timer construct2;
    // create a local estimate field for the outer patch region (cell group 1)
    up_local_est_fld_ = SANS::make_unique<Field_Local<Field_DG_Cell<PhysDim,TopoDim,Real>>>
        ( local_xfld, pErrorEstimate_->efld_DG_nodal(), 1, BasisFunctionCategory_Lagrange );
    status.time_constructor += construct2.elapsed();
  }
#endif
  if ( up_local_est_fld_ != nullptr)
    local_cellGroups = {0};
  else
    local_cellGroups = {0,1};


  timer clock_errest;
  // the field only gets used if doing nodal estimates
  ErrorEstimateClass errorEstimate( local_xfld, qfld, up_local_est_fld_, solnExact_, quadratureOrder,
                                                      estimateOrder, local_cellGroups );

  errorEstimate.getLocalSolveErrorIndicator(local_error);
  status.time_errest += clock_errest.elapsed();


  // if (local_xfld.nBoundaryTraceGroups() > 0 )
  // {
  //   output_Tecplot(errorEstimate.getEField(),"tmp/efld_project_local.plt");
  //   SANS_DEVELOPER_EXCEPTION("printing a boundary");
  // }

  return status;
}

template<class PhysDim, class TopoDim, class SolutionExact, template<class,class,class> class FieldConstructor>
template<class Topology>
void
SolverInterface_L2Project<PhysDim, TopoDim, SolutionExact,FieldConstructor>::
getSolutionInfo_cellgroup(const typename XField<PhysDim,TopoDim>::template FieldCellGroupType<Topology>& xfldCellGroup,
                          const int cellGroupGlobal)
{
  typedef typename Field<PhysDim, TopoDim, Real>::template FieldCellGroupType<Topology> QFieldCellGroupType;
  const QFieldCellGroupType& cellgrp = qfld_.template getCellGroup<Topology>(cellGroupGlobal);

  nDOFperCell_[cellGroupGlobal] = cellgrp.basis()->nBasis();
  orderVec_[cellGroupGlobal]    = cellgrp.basis()->order();
  basisCat_[cellGroupGlobal]    = cellgrp.basis()->category();

  const int order = cellgrp.basis()->order();

  if ( qfld_.spaceType() == SpaceType::Continuous)
  {
    switch ( AlgebraicEqSet::PhysDim::D )
    {
    case 1:
      // 2*1 node dof -> 2*1*1/2 node dof = 1 dof;
      nDOFperCell_[cellGroupGlobal] -= 1; // the P1 dofs are shared
      break;
    case 2:
      /*
        6 regular triangles can share a point, so node dofs are shared by 6
        2 regular triangles can share an edge, so edge dofs are shared by 2
        cell dofs are solely owned

        3*1 node dofs -> 3*1/6 node dof = 1/2 dof
        3*(order-1) edge dofs -> 3*(order-1)*1/2 dof =
      */
      nDOFperCell_[cellGroupGlobal] -= (3 - 3.0/6.0); // the node dofs are shared by 6
      nDOFperCell_[cellGroupGlobal] -= (3 - 3.0/2.0)* max(0,(order - 1 )); // if there are edge dofs they are shared
      break;
    case 3:

      /*
        20 regular tetrahedra can share a point, so P1 dofs are shared by 20
        6 regular tetrahedra can share an edge, so edge dofs are shared by 6
        2 regular tetrahedra can share a face, so face dofs are shared by 2
        cell dofs are solely owned

        4*1 node dof -> 4 *1/20 node dof = 1/5 dof
        6*(order-1) edge dofs -> 6*(order-1)*1/6 edge dof = (order-1) dof
        4*(order-2)*(order-1)/2 face dofs -> 4*(order-2)*(order-1)/2 *1/2 face dofs = 2(order-2)(order-1) dof

      */

      nDOFperCell_[cellGroupGlobal] -= (4 - 4.0/20.0); // the node dofs are shared by 20
      nDOFperCell_[cellGroupGlobal] -= (6 - 6.0/(11./2.0))*max(0,(order-1)); // if there are edge dofs they are shared by 11/2
      nDOFperCell_[cellGroupGlobal] -= (4 - 4.0/2.0)*max(0,(order-1)*(order-2)/2); // if there are face dofs they are shared by 2

      break;
    case 4:

      /*
       * 120 regular pentatopes can share a point so vertex dofs are shared by 120
       * 20 regular pentatopes can share an edge so edge DOFs are shared by 20
       * 6 regular pentatopes can share a triangle so area DOFs are shared by 6
       * 2 regular pentatopes can share a tetrahedron, so face DOFs are shared by 2
       * cell dofs are solely owned
       *
       * 5 nodes,  1/120 dof shares per node, 1/24 dof share per elem,  1 dof per node
       * 10 edges, 1/20 edge shares per node, 1/2 edge share per elem,  (order - 1) dof per edge (interior)
       * 10 areas, 1/6 area share per node,   5/3 area share per elem,  (order - 2)*(order - 1)/2 dofs per area (interior)
       * 5 traces, 1/2 trace share per node,  5/2 trace share per elem, (order - 3)*(order - 2)*(order - 1)/6 dofs per trace (interior)
       */

      nDOFperCell_[cellGroupGlobal] -= ( 5 - 1./24);
      nDOFperCell_[cellGroupGlobal] -= (10 - 1./2)*max(0, (order - 1));
      nDOFperCell_[cellGroupGlobal] -= (10 - 5./3)*max(0, (order - 1)*(order - 2)/2);
      nDOFperCell_[cellGroupGlobal] -= ( 5 - 5./2)*max(0, (order - 1)*(order - 2)*(order - 3)/6);

      // /*
      //   90 regular pentatopes can share a point, so P1 dofs are shared by 20
      //   20 regular pentatopes can share an edge, so edge dofs are shared by 20
      //   6 regular pentatopes can share a triangle, so area dofs are shared by 6
      //   2 regular pentatopes can share a face, so face dofs are shared by 2
      //   cell dofs are solely owned
      //
      //   5*1 node dof -> 5*1/90 node dof = 1/18 dof
      //   10*(order-1) edge dofs -> 10*(order-1)*1/20 edge dof = (order-1)/2 dofs
      //   10*(order-2)*(order-1)/2 area dofs -> 10*(order-2)*(order-1)/2 *1/6 face dofs = 5/6*(order-2)(order-1) dof
      //   5*(order - 3)*(order - 2)*(order - 1)/6 trace dofs -> 5/12*(order-3)*(order-2)*(order-1) dof
      // */
      //
      // // TODO: Figure out the optimality connectivity here
      //
      // // SANS_ASSERT(order<=1);
      // std::cout << "WARNING: sketchy optimality connectivity in 4D! " << __FILE__ << ": " << __LINE__ << std::endl;
      // // the node dofs are shared by 90
      // nDOFperCell_[cellGroupGlobal] -= (5 - 1./18.);
      // // if there are edge dofs they are shared by 20
      // nDOFperCell_[cellGroupGlobal] -= (10 - 1./2.)*max(0,(order-1));
      // // if there are area dofs they are shared by 6
      // nDOFperCell_[cellGroupGlobal] -= (10 - 5/6)*max(0,(order-1)*(order-2)/2);
      // // if there are face dofs they are shared by 2
      // nDOFperCell_[cellGroupGlobal] -= (5 - 5/12)*max(0,(order-1)*(order-2)*(order-3)/6);

      break;
    }
  }
}

} // namespace SANS

#endif /* SOLVERINTERFACE_GALERKIN_H_ */
