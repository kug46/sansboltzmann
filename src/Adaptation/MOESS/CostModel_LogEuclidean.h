// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef COSTMODEL_LOGEUCLIDEAN_H
#define COSTMODEL_LOGEUCLIDEAN_H

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"

#include "Field/Element/Element.h"
#include "Field/XField.h"

namespace SANS
{

template <class PhysDim>
class IntegrandCell_CostModel_LogEuclidean;

template <class PhysDim, class TopoDim>
class CostModel_LogEuclidean
{

public:
  static const int D = PhysDim::D; // physical dimensions
  typedef DLA::MatrixSymS<D,Real> MatrixSym;
  typedef Field_CG_Cell<PhysDim, TopoDim, MatrixSym> MatrixSymFieldType;

  CostModel_LogEuclidean(const Field_CG_Cell<PhysDim,TopoDim,MatrixSym>& implied_metric,
                         const std::vector<int>& cellgroup_list);
  ~CostModel_LogEuclidean() {}

  //Computes a cost estimate and its derivatives, given nodal step matrix vector Svec
  Real computeCost_Implied(const Real nDOF0) const;
  void computeCost_S(const Real nDOF0, const MatrixSymFieldType& Sfld, Real& Cost, std::vector<MatrixSym>& dCost_dSvec) const;
  void computeCost_logM(const Real nDOF0, const MatrixSymFieldType& logMfld, Real& Cost, std::vector<MatrixSym>& dCost_dlogM) const;

protected:
  const XField<PhysDim, TopoDim>& xfld_;
  const std::vector<int> cellgroup_list_;

  //Metrics at each node
  const MatrixSymFieldType& implied_metric_;

  const int quadratureOrder_;

  // base cost field computed from the implied metric
  mutable Field_CG_Cell<PhysDim, TopoDim, Real> cfld_;
};

}

#endif // COSTMODEL_LOGEUCLIDEAN_H
