// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef CALLMESHER_H_
#define CALLMESHER_H_

#include "tools/SANSnumerics.h"     // Real

#include "Field/XField.h"
#include "Field/Field.h"

#include "Python/PyDict.h"

namespace SANS
{

template<class PhysDim, class TopoDim>
struct callMesher
{
  typedef XField<PhysDim, TopoDim> XFieldType;
  typedef DLA::MatrixSymS<PhysDim::D,Real> MatrixSym;
  typedef Field_CG_Cell<PhysDim,TopoDim,MatrixSym> MetricFieldType;

  typedef std::shared_ptr<XFieldType> MeshPtr;
  typedef std::pair<MeshPtr,MeshPtr> MeshPtrPair; //{linear mesh, curved mesh}

  static MeshPtrPair call(const XFieldType& xfld, const MetricFieldType& metric_req,
                          const int adapt_iter, const PyDict& MesherDict);
};

} // namespace SANS

#endif //CALLMESHER_H_
