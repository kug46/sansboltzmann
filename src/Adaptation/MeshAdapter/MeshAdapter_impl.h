// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(MESHADAPTER_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "tools/SANSnumerics.h"     // Real

#include <fstream>

#include "Adaptation/MeshAdapter.h"
#include "Adaptation/MetricConformity.h"
#include "../callMesher.h"

#include "Field/output_Tecplot.h"

#include "Field/XField.h"

#include "Field/FieldTypes.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

// cppcheck-suppress passedByValue
template<class PhysDim, class TopoDim>
void MeshAdapterParams<PhysDim, TopoDim>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.TargetCost));
  allParams.push_back(d.checkInputs(params.Algorithm));
  allParams.push_back(d.checkInputs(params.Mesher));
  allParams.push_back(d.checkInputs(params.FilenameBase));
  allParams.push_back(d.checkInputs(params.dumpMetric));
  allParams.push_back(d.checkInputs(params.dumpImpliedMetric));
  allParams.push_back(d.checkInputs(params.dumpStepMatrix));
  allParams.push_back(d.checkInputs(params.dumpRateMatrix));
  allParams.push_back(d.checkInputs(params.dumpEdgeLength));
  allParams.push_back(d.checkInputs(params.dumpError));
  allParams.push_back(d.checkInputs(params.dumpOptimizationInfo));
  allParams.push_back(d.checkInputs(params.hasTrueOutput));
  allParams.push_back(d.checkInputs(params.TrueOutput));
  d.checkUnknownInputs(allParams);
}
template<class PhysDim, class TopoDim>
MeshAdapterParams<PhysDim,TopoDim> MeshAdapterParams<PhysDim,TopoDim>::params;

template <class PhysDim, class TopoDim>
MeshAdapter<PhysDim, TopoDim>::MeshAdapter(const PyDict& d, std::fstream& fadapthist) :
  targetCost_(d.get(MeshAdapterParams<PhysDim, TopoDim>::params.TargetCost)),
  MOESSDict_(d.get(MeshAdapterParams<PhysDim, TopoDim>::params.Algorithm)),
  MesherDict_(d.get(MeshAdapterParams<PhysDim, TopoDim>::params.Mesher)),
  filename_base_(d.get(MeshAdapterParams<PhysDim, TopoDim>::params.FilenameBase)),
  dumpMetric_(d.get(MeshAdapterParams<PhysDim, TopoDim>::params.dumpMetric)),
  dumpImpliedMetric_(d.get(MeshAdapterParams<PhysDim, TopoDim>::params.dumpImpliedMetric)),
  dumpStepMatrix_(d.get(MeshAdapterParams<PhysDim, TopoDim>::params.dumpStepMatrix)),
  dumpRateMatrix_(d.get(MeshAdapterParams<PhysDim, TopoDim>::params.dumpRateMatrix)),
  dumpEdgeLength_(d.get(MeshAdapterParams<PhysDim, TopoDim>::params.dumpEdgeLength)),
  dumpError_(d.get(MeshAdapterParams<PhysDim, TopoDim>::params.dumpError)),
  dumpOptimizationInfo_(d.get(MeshAdapterParams<PhysDim, TopoDim>::params.dumpOptimizationInfo)),
  hasTrueOutput_(d.get(MeshAdapterParams<PhysDim, TopoDim>::params.hasTrueOutput)),
  trueOutput_(d.get(MeshAdapterParams<PhysDim, TopoDim>::params.TrueOutput)),
  fadapthist_(fadapthist)
{
  //Override filename base in mesher dictionary
  MesherDict_["FilenameBase"] = filename_base_;
}

template <class PhysDim, class TopoDim>
typename MeshAdapter<PhysDim, TopoDim>::MeshPtr
MeshAdapter<PhysDim, TopoDim>::adapt(
    const XFieldType& xfld, const std::vector<int>& cellGroups, const SolverInterfaceBase<PhysDim,TopoDim>& interface, const int adapt_iter)
{
  return adapt(xfld, xfld, cellGroups, interface, adapt_iter).first;
}

template <class PhysDim, class TopoDim>
typename MeshAdapter<PhysDim, TopoDim>::MeshPtrPair
MeshAdapter<PhysDim, TopoDim>::adapt(
    const XFieldType& xfld_linear, const XFieldType& xfld_curved,
    const std::vector<int>& cellGroups, const SolverInterfaceBase<PhysDim,TopoDim>& interface, const int adapt_iter)
{
  typedef DLA::MatrixSymS<D,Real> MatrixSym;
  typedef DLA::VectorS<MatrixSym::SIZE, Real> ArrayMatSym;

  //Get the mesher type
  std::string mesher_type = MesherDict_.get(MeshAdapterParams<PhysDim, TopoDim>::params.Mesher.Name);

  if (mesher_type != "Epic" && &xfld_linear != &xfld_curved)
    SANS_DEVELOPER_EXCEPTION("Only Epic supports curved meshes.");

  //Create MOESS class and optimize
  MOESS<PhysDim, TopoDim> MOESSClass(xfld_linear, xfld_curved, cellGroups, interface, targetCost_, MOESSDict_);

  //Get nodal metric request for new mesh, pcaplan changed to Lagrange from Hierarchical
  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> metric_req(xfld_linear, 1, BasisFunctionCategory_Lagrange, cellGroups);
  MOESSClass.getNodalMetricRequestField(metric_req);

  //Field dumps
  if (dumpMetric_)
  {
    std::string metric_filename = filename_base_ + "metricRequest_a" + std::to_string(adapt_iter) + ".dat";

    // if (adapt_iter%5 == 0)
    metric_req.syncDOFs_MPI_noCache();
    output_Tecplot_Metric( metric_req, metric_filename );
  }
  if (dumpImpliedMetric_)
  {
    std::string metric_filename = filename_base_ + "metricImplied_a" + std::to_string(adapt_iter) + ".dat";

    //Get nodal implied metric
    Field_CG_Cell<PhysDim,TopoDim,MatrixSym> metric_imp(xfld_linear, 1, BasisFunctionCategory_Lagrange, cellGroups);
    MOESSClass.getNodalImpliedMetricField(metric_imp);
    metric_imp.syncDOFs_MPI_noCache();

    output_Tecplot_Metric( metric_imp, metric_filename );
  }

  if (dumpStepMatrix_)
  {
    Field_CG_Cell<PhysDim,TopoDim,DLA::VectorS<D, Real>> step_eig_fld(xfld_linear, 1, BasisFunctionCategory_Lagrange, cellGroups);
    MOESSClass.getStepMatrixEigField(step_eig_fld);

    std::string stepmatrix_filename = filename_base_ + "stepmatrix_a" + std::to_string(adapt_iter) + ".dat";

    if (D == 1)
      output_Tecplot( step_eig_fld, stepmatrix_filename, {"Step eig[0]"} );
    else if (D == 2)
      output_Tecplot( step_eig_fld, stepmatrix_filename, {"Step eig[0]","Step eig[1]"} );
    else if (D == 3)
      output_Tecplot( step_eig_fld, stepmatrix_filename, {"Step eig[0]","Step eig[1]","Step eig[2]"} );
  }

  if (dumpRateMatrix_)
  {
    std::string ratematrix_filename = filename_base_ + "ratematrix_a" + std::to_string(adapt_iter) + ".dat";
    if ( MOESSDict_.get(MOESSParams::params.LocalSolve) == MOESSParams::params.LocalSolve.Edge )
    {
      Field_CG_Cell<PhysDim,TopoDim,ArrayMatSym> rate_matrix_fld(xfld_linear, 1, BasisFunctionCategory_Hierarchical, cellGroups);
      MOESSClass.getRateMatrixField(rate_matrix_fld);

      if (D == 1)
        output_Tecplot( rate_matrix_fld, ratematrix_filename, {"Rate(0,0)"} );
      else if (D == 2)
        output_Tecplot( rate_matrix_fld, ratematrix_filename, {"Rate(0,0)",
                                                               "Rate(1,0)","Rate(1,1)"} );
      else if (D == 3)
        output_Tecplot( rate_matrix_fld, ratematrix_filename, {"Rate(0,0)",
                                                               "Rate(1,0)","Rate(1,1)",
                                                               "Rate(2,0)","Rate(2,1)","Rate(2,2)"} );
    }
    else
    {
      Field_DG_Cell<PhysDim,TopoDim,ArrayMatSym> rate_matrix_fld(xfld_linear, 0, BasisFunctionCategory_Legendre, cellGroups);
      MOESSClass.getRateMatrixField(rate_matrix_fld);

      if (D == 1)
        output_Tecplot( rate_matrix_fld, ratematrix_filename, {"Rate(0,0)"} );
      else if (D == 2)
        output_Tecplot( rate_matrix_fld, ratematrix_filename, {"Rate(0,0)",
                                                               "Rate(1,0)","Rate(1,1)"} );
      else if (D == 3)
        output_Tecplot( rate_matrix_fld, ratematrix_filename, {"Rate(0,0)",
                                                               "Rate(1,0)","Rate(1,1)",
                                                               "Rate(2,0)","Rate(2,1)","Rate(2,2)"} );
    }

  }

  if (dumpEdgeLength_)
  {
    Field_CG_Cell<PhysDim,TopoDim,Real> edgeLen_fld(xfld_linear, 1, BasisFunctionCategory_Hierarchical, cellGroups);
    MOESSClass.getEdgeLengthField(edgeLen_fld);
    std::string edgelen_filename = filename_base_ + "edgelen_a" + std::to_string(adapt_iter) + ".dat";
    output_Tecplot( edgeLen_fld, edgelen_filename, {"Avg Edge Len"} );
  }

  if (dumpError_)
  {
    std::string error_filename = filename_base_ + "error_a" + std::to_string(adapt_iter) + ".dat";
    interface.output_EField( error_filename );
  }

  if (xfld_linear.comm()->rank() == 0)
  {
    //Book-keeping
    if (hasTrueOutput_)
    {
      Real trueError = interface.getOutput() - trueOutput_;
      MOESSClass.printAdaptInfo(fadapthist_, adapt_iter, interface.getGlobalErrorIndicator(), interface.getGlobalErrorEstimate(),
                                trueError, interface.getOutput());
    }
    else
      MOESSClass.printAdaptInfo(fadapthist_, adapt_iter, interface.getGlobalErrorIndicator(), interface.getGlobalErrorEstimate(),
                                0.0, interface.getOutput());

    if (dumpOptimizationInfo_)
    {
      std::string objhist_filename    = filename_base_ + "optimizerhist_a" + std::to_string(adapt_iter) + ".dat";
      std::fstream fobjhist( objhist_filename, std::fstream::out );
      MOESSClass.printOptimizationInfo(fobjhist);
      fobjhist.close();
    }
  }

  return callMesher<PhysDim, TopoDim>::call(xfld_linear, metric_req, adapt_iter, MesherDict_);
}

// function to adapt mesh to target metric
template <class PhysDim, class TopoDim>
std::shared_ptr< XField<PhysDim,TopoDim> >
MeshAdapter<PhysDim, TopoDim>::adapt_metric(
    const XField<PhysDim,TopoDim>& xfld , const std::vector<int>& cellGroups,
    Field_CG_Cell<PhysDim,TopoDim,MatrixSym>& targetMetric , const int adapt_iter )
{
  typedef DLA::MatrixSymS<D,Real> MatrixSym;

  // create the dummy solver interface
  // SolverInterfaceDummy<PhysDim,TopoDim> problem;
  Field_NodalView xfld_nodalview(xfld, cellGroups);

  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> metric_req(xfld, 1, BasisFunctionCategory_Hierarchical, cellGroups);

  // limit the metric by limiting the step matrices from the implied one
  TargetMetric<PhysDim,TopoDim> target( xfld , cellGroups , targetMetric , MOESSDict_ );

  target.getNodalMetricRequestField(metric_req);

  //Field dumps
  if (dumpMetric_)
  {
    std::string metric_filename = filename_base_ + "metricRequest_a" + std::to_string(adapt_iter) + ".dat";

    output_Tecplot_Metric( metric_req, metric_filename );
#if 0
    metric_filename = filename_base_ + "metricImplied_a" + std::to_string(adapt_iter) + ".dat";

    //Get nodal implied metric
    Field_CG_Cell<PhysDim,TopoDim,MatrixSym> metric_imp(xfld_linear, 1, BasisFunctionCategory_Hierarchical, cellGroups);

    MOESSClass.getNodalImpliedMetricField(metric_imp);
    metric_imp.syncDOFs_MPI_noCache();

    output_Tecplot_Metric( metric_imp, metric_filename );
#endif
  }

  if (TopoDim::D==2)
    target.template dumpMetricConformity<Triangle>( filename_base_ , "analytic"+std::to_string(adapt_iter) );
  else if (TopoDim::D==3)
    target.template dumpMetricConformity<Tet>( filename_base_ , "analytic"+std::to_string(adapt_iter) );
  else if (TopoDim::D==4)
    target.template dumpMetricConformity<Pentatope>( filename_base_ , "analytic"+std::to_string(adapt_iter) );

  // call the mesher
  std::shared_ptr<XField<PhysDim,TopoDim>> pxfld = callMesher<PhysDim, TopoDim>::call(xfld , metric_req, adapt_iter, MesherDict_).first;

  return pxfld;
}



// function to adapt mesh to IMPLIED metric
template <class PhysDim, class TopoDim>
typename MeshAdapter<PhysDim, TopoDim>::MeshPtr
MeshAdapter<PhysDim, TopoDim>::adapt_implied_metric(
    const XField<PhysDim,TopoDim>& xfld, const std::vector<int>& cellGroups, const int adapt_iter)
{
  return adapt_implied_metric(xfld,xfld,cellGroups,adapt_iter).first;
}

// function to adapt mesh to IMPLIED metric
template <class PhysDim, class TopoDim>
typename MeshAdapter<PhysDim, TopoDim>::MeshPtrPair
MeshAdapter<PhysDim, TopoDim>::adapt_implied_metric(
    const XFieldType& xfld_linear, const XFieldType& xfld_curved,
    const std::vector<int>& cellGroups, const int adapt_iter)
{
  typedef DLA::MatrixSymS<D,Real> MatrixSym;

  // create the dummy solver interface
  SolverInterfaceDummy<PhysDim,TopoDim> problem;

  Field_CG_Cell<PhysDim,TopoDim,MatrixSym> metric_req(xfld_linear, 1, BasisFunctionCategory_Hierarchical, cellGroups);

  NodalMetrics<PhysDim,TopoDim> nodalMetrics( xfld_linear, cellGroups, problem, MOESSDict_ );

  nodalMetrics.getNodalImpliedMetricField(metric_req);

  // call the mesher
  return callMesher<PhysDim, TopoDim>::call(xfld_linear, metric_req, adapt_iter, MesherDict_);
}


} // namespace SANS
