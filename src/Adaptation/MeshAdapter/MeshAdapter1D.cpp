// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define MESHADAPTER_INSTANTIATE
#include "MeshAdapter_impl.h"

#include "Field/XFieldLine.h"

#include "Field/FieldLine_DG_Cell.h"

#include "Field/FieldLine_CG_Cell.h"

#define PYDICT_INSTANTIATE
#include "Python/PyDict_impl.h"

namespace SANS
{

template<class PhysDim, class TopoDim>
typename callMesher<PhysDim, TopoDim>::MeshPtrPair
callMesher<PhysDim, TopoDim>::
call(const XFieldType& xfld, const MetricFieldType& metric_req,
     const int adapt_iter, const PyDict& MesherDict)
{
  //Get the mesher type
  std::string mesher_type = MesherDict.get(MeshAdapterParams<PhysDim, TopoDim>::params.Mesher.Name);

  if (mesher_type == MeshAdapterParams<PhysDim, TopoDim>::params.Mesher.embedding)
  {
    MesherInterface<PhysD1,TopoD1,EmbeddingMesher> mesher( adapt_iter , MesherDict );
    MeshPtr pxfld_new = mesher.adapt(metric_req);
    return {pxfld_new,pxfld_new};
  }
  else
    SANS_DEVELOPER_EXCEPTION("Unknown mesher type : %s", mesher_type.c_str());

  return {nullptr,nullptr};
}

// Explicit instantiations
typedef MesherOptions<PhysD1, TopoD1> MesherOptions1D;
PYDICT_PARAMETER_OPTION_INSTANTIATE( MesherOptions1D )
PYDICT_PARAMETER_OPTION_INSTANTIATE( MeshAdapter_detail::AlgorithmOptions )

template struct MeshAdapterParams<PhysD1, TopoD1>;
template struct callMesher<PhysD1, TopoD1>;
template class MeshAdapter<PhysD1, TopoD1>;

} // namespace SANS
