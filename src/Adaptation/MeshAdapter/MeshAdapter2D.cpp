// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define MESHADAPTER_INSTANTIATE
#include "MeshAdapter_impl.h"

#include "Field/XFieldArea.h"

#include "Field/FieldArea_DG_Cell.h"

#include "Field/FieldArea_CG_Cell.h"

#define PYDICT_INSTANTIATE
#include "Python/PyDict_impl.h"

namespace SANS
{

template<class PhysDim, class TopoDim>
typename callMesher<PhysDim, TopoDim>::MeshPtrPair
callMesher<PhysDim, TopoDim>::
call(const XFieldType& xfld, const MetricFieldType& metric_req,
     const int adapt_iter, const PyDict& MesherDict)
{
  //Get the mesher type
  std::string mesher_type = MesherDict.get(MeshAdapterParams<PhysDim, TopoDim>::params.Mesher.Name);

  if (mesher_type == MeshAdapterParams<PhysDim, TopoDim>::params.Mesher.Epic)
  {
    MesherInterface<PhysD2, TopoD2, Epic> mesher(adapt_iter, MesherDict);
    return mesher.adapt(metric_req);
  }
  else if (mesher_type == MeshAdapterParams<PhysDim, TopoDim>::params.Mesher.FeFloa)
  {
    MesherInterface<PhysD2, TopoD2, FeFloa> mesher(adapt_iter, MesherDict);
    MeshPtr pxfld_new = mesher.adapt(metric_req);
    return {pxfld_new, pxfld_new};
  }
#ifdef SANS_REFINE
  else if (mesher_type == MeshAdapterParams<PhysDim, TopoDim>::params.Mesher.refine)
  {
    MesherInterface<PhysD2, TopoD2, refine> mesher(adapt_iter, MesherDict);
    MeshPtr pxfld_new = mesher.adapt(metric_req);
    return {pxfld_new, pxfld_new};
  }
#endif
#if 0 //AFLR not supported yet
  else if (mesher_type == MeshAdapterParams<PhysDim, TopoDim>::params.Mesher.AFLR2)
  {
    MeshPtr pxfld_new(new AFLR2(metric_req));
    return {pxfld_new, pxfld_new};
  }
#endif
#ifdef SANS_AVRO
  else if (mesher_type == MeshAdapterParams<PhysDim, TopoDim>::params.Mesher.avro)
  {
    MesherInterface<PhysD2, TopoD2, avroMesher> mesher(adapt_iter, MesherDict);
    MeshPtr pxfld_new = mesher.adapt(metric_req, xfld);
    return {pxfld_new, pxfld_new};
  }
#endif
  else
    SANS_DEVELOPER_EXCEPTION("Unknown mesher type : %s", mesher_type.c_str());

  // remove compiler warning
  return {nullptr, nullptr};
}

// Explicit instantiations
typedef MesherOptions<PhysD2, TopoD2> MesherOptions2D;
PYDICT_PARAMETER_OPTION_INSTANTIATE( MesherOptions2D )
PYDICT_PARAMETER_OPTION_INSTANTIATE( MeshAdapter_detail::AlgorithmOptions )

template struct MeshAdapterParams<PhysD2, TopoD2>;
template struct callMesher<PhysD2, TopoD2>;
template class MeshAdapter<PhysD2, TopoD2>;

} // namespace SANS
