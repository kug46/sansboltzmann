// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define MESHADAPTER_INSTANTIATE
#include "MeshAdapter_impl.h"

#include "Field/XFieldSpacetime.h"

#include "Field/FieldSpacetime_DG_Cell.h"

#include "Field/FieldSpacetime_CG_Cell.h"

#define PYDICT_INSTANTIATE
#include "Python/PyDict_impl.h"

namespace SANS
{

template<class PhysDim, class TopoDim>
typename callMesher<PhysDim, TopoDim>::MeshPtrPair
callMesher<PhysDim, TopoDim>::
call(const XFieldType& xfld, const MetricFieldType& metric_req,
     const int adapt_iter, const PyDict& MesherDict)
{

  #ifdef SANS_AVRO

  // get the mesher type
  std::string mesher_type = MesherDict.get(MeshAdapterParams<PhysDim, TopoDim>::params.Mesher.Name);

  if (mesher_type == MeshAdapterParams<PhysDim, TopoDim>::params.Mesher.avro)
  {
    MesherInterface<PhysD4, TopoD4, avroMesher> mesher(adapt_iter, MesherDict);
    MeshPtr pxfld_new = mesher.adapt(metric_req, xfld);
    if (!mesher.conforms()) printf("avro was not happy about conformity!\n");
    return {pxfld_new, pxfld_new};
  }
  else
    SANS_DEVELOPER_EXCEPTION("Unknown mesher type : %s", mesher_type.c_str());

  #endif

  // remove compiler warning
    return {nullptr, nullptr};
}

// Explicit instantiations
typedef MesherOptions<PhysD4, TopoD4> MesherOptions4D;
PYDICT_PARAMETER_OPTION_INSTANTIATE( MesherOptions4D )
PYDICT_PARAMETER_OPTION_INSTANTIATE( MeshAdapter_detail::AlgorithmOptions )

template struct MeshAdapterParams<PhysD4, TopoD4>;
template struct callMesher<PhysD4, TopoD4>;
template class MeshAdapter<PhysD4, TopoD4>;

} // namespace SANS
