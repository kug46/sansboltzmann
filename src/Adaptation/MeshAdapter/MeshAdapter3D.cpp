// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define MESHADAPTER_INSTANTIATE
#include "MeshAdapter_impl.h"

#include "Field/XFieldVolume.h"

#include "Field/FieldVolume_DG_Cell.h"

#include "Field/FieldVolume_CG_Cell.h"

#define PYDICT_INSTANTIATE
#include "Python/PyDict_impl.h"

namespace SANS
{

template<class PhysDim, class TopoDim>
typename callMesher<PhysDim, TopoDim>::MeshPtrPair
callMesher<PhysDim, TopoDim>::
call(const XFieldType& xfld, const MetricFieldType& metric_req,
     const int adapt_iter, const PyDict& MesherDict)
{
  //Get the mesher type
  std::string mesher_type = MesherDict.get(MeshAdapterParams<PhysDim, TopoDim>::params.Mesher.Name);

  if (mesher_type == MeshAdapterParams<PhysDim, TopoDim>::params.Mesher.Epic)
  {
    MesherInterface<PhysD3, TopoD3, Epic> mesher(adapt_iter, MesherDict);
    return mesher.adapt(metric_req);
  }
  else if (mesher_type == MeshAdapterParams<PhysDim, TopoDim>::params.Mesher.FeFloa)
  {
    MesherInterface<PhysD3, TopoD3, FeFloa> mesher(adapt_iter, MesherDict);
    MeshPtr pxfld_new = mesher.adapt(metric_req);
    return {pxfld_new, pxfld_new};
  }
#ifdef SANS_AFLR
  else if (mesher_type == MeshAdapterParams<PhysDim, TopoDim>::params.Mesher.AFLR)
  {
    MeshPtr pxfld_new(new AFLR3(metric_req));
    return {pxfld_new, pxfld_new};
  }
#endif
  else if (mesher_type == MeshAdapterParams<PhysDim, TopoDim>::params.Mesher.refine)
  {
    MesherInterface<PhysD3, TopoD3, refine> mesher(adapt_iter, MesherDict);
    MeshPtr pxfld_new = mesher.adapt(metric_req);
    return {pxfld_new, pxfld_new};
  }
#ifdef SANS_AVRO
  else if (mesher_type == MeshAdapterParams<PhysDim, TopoDim>::params.Mesher.avro)
  {
    MesherInterface<PhysD3, TopoD3, avroMesher> mesher(adapt_iter, MesherDict);
    MeshPtr pxfld_new = mesher.adapt(metric_req, xfld);
    return {pxfld_new, pxfld_new};
  }
#endif
  else
    SANS_DEVELOPER_EXCEPTION("Unknown mesher type : %s", mesher_type.c_str());

  // remove compiler warning
    return {nullptr, nullptr};
}

// Explicit instantiations
typedef MesherOptions<PhysD3, TopoD3> MesherOptions3D;
PYDICT_PARAMETER_OPTION_INSTANTIATE( MesherOptions3D )
PYDICT_PARAMETER_OPTION_INSTANTIATE( MeshAdapter_detail::AlgorithmOptions )

template struct MeshAdapterParams<PhysD3, TopoD3>;
template struct callMesher<PhysD3, TopoD3>;
template class MeshAdapter<PhysD3, TopoD3>;

} // namespace SANS
