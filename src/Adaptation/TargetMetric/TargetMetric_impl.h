// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(TARGETMETRIC_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "Adaptation/MetricConformity.h"
#include "Adaptation/TargetMetric.h"

#include "Field/XField.h"

#include "Adaptation/MOESS/ReferenceElementCost.h"

#include <iomanip> // std::setprecision
#include <unistd.h> // gethostname
#include <limits.h> // HOST_NAME_MAX
#ifndef HOST_NAME_MAX
#define HOST_NAME_MAX _POSIX_HOST_NAME_MAX
#endif

//#define PYDICT_INSTANTIATE
//#include "Python/PyDict_impl.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

template<class PhysDim,class TopoDim>
TargetMetric<PhysDim,TopoDim>::TargetMetric(
    const XField<PhysD,TopoD>& xfld ,
    const std::vector<int>& cellGroups ,
    Field_CG_Cell<PhysD,TopoD,MatrixSym>& metric ,
    const PyDict& params ) :
    xfld_(xfld),
    target_(metric)
{
  SolverInterfaceDummy<PhysDim,TopoDim> problem;

#ifdef SANS_MPI

  // create a serialize grid on processor 0
  XField<PhysDim,TopoDim> xfld_rank0(xfld, XFieldBalance::Serial);
#endif

  if (xfld.comm()->rank() == 0)
  {
#ifdef SANS_MPI
    char hostname[HOST_NAME_MAX];
    gethostname(hostname, HOST_NAME_MAX);
    std::cout << std::endl;
    std::cout << "Computing target metric serially on host: " << hostname << std::endl;
    std::cout << std::endl;

    nodalMetrics_ = std::make_shared<NodalMetrics<PhysDim,TopoDim>>(xfld_rank0, cellGroups, problem, params );
#else
    nodalMetrics_ = std::make_shared<NodalMetrics<PhysDim,TopoDim>>(xfld, cellGroups, problem, params );
#endif

    // compute the nodal metrics which get as close as possible to the
    // target without changing the current mesh (implied metric) by a certain factor
    nodalMetrics_->matchTargetMetric(metric);
  }

#ifdef SANS_MPI
  // save the pointer the nodal previously computed metrics
  std::shared_ptr<NodalMetrics<PhysDim,TopoDim>> nodalMetrics_rank0 = nodalMetrics_;

  // create the nodal metrics on the root processor
  nodalMetrics_ = std::make_shared<NodalMetrics<PhysDim,TopoDim>>(xfld, cellGroups, problem, params, *nodalMetrics_rank0);

  // just so the other processors wait while rank 0 performs the optimization
  xfld.comm()->barrier();

#endif

}

template<class PhysDim,class TopoDim>
template<class Topology>
void
TargetMetric<PhysDim,TopoDim>::dumpMetricConformity( const std::string& dir , const std::string& label )
{
  // evaluate metric conformity on the current mesh
  std::vector<Real> lengths;
  std::vector<Real> quality;
  evaluateMetricConformity<PhysDim,TopoDim,Topology>(xfld_,target_,lengths,quality);

  FILE *fid;

  // get and dump the edge lengths
  std::string length_filename = dir+"/length_"+label+".dat";
  fid = fopen(length_filename.c_str(),"w");
  for (size_t k=0;k<lengths.size();k++)
    fprintf(fid,"%1.16e\n",lengths[k]);
  fclose(fid);

  // get and dump element quality
  std::string quality_filename = dir+"/quality_"+label+".dat";
  fid = fopen(quality_filename.c_str(),"w");
  for (size_t k=0;k<quality.size();k++)
    fprintf(fid,"%1.16e\n",quality[k]);
  fclose(fid);
}

#define INSTANTIATE_CONFORMITY_DUMP( PhysD , TopoD , TOPO ) \
template void TargetMetric<PhysD,TopoD>::dumpMetricConformity<TOPO>( const std::string& dir , const std::string& label )

} // SANS
