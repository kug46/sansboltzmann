// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define TARGETMETRIC_INSTANTIATE
#include "TargetMetric_impl.h"

#include "Field/XFieldSpacetime.h"

#include "Field/FieldSpacetime_CG_Cell.h"

namespace SANS
{

INSTANTIATE_CONFORMITY_DUMP( PhysD4 , TopoD4 , Pentatope );

template class TargetMetric<PhysD4,TopoD4>;

} // SANS
