// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define TARGETMETRIC_INSTANTIATE
#include "TargetMetric_impl.h"

#include "Field/XFieldVolume.h"

#include "Field/FieldVolume_CG_Cell.h"

namespace SANS
{

INSTANTIATE_CONFORMITY_DUMP( PhysD3 , TopoD3 , Tet );

template class TargetMetric<PhysD3,TopoD3>;

} // SANS
