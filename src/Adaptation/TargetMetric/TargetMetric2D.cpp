// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define TARGETMETRIC_INSTANTIATE
#include "TargetMetric_impl.h"

#include "Field/XFieldArea.h"

//#include "Field/FieldArea_DG_Cell.h"

#include "Field/FieldArea_CG_Cell.h"

namespace SANS
{

INSTANTIATE_CONFORMITY_DUMP( PhysD2 , TopoD2 , Triangle );

template class TargetMetric<PhysD2,TopoD2>;

} // SANS
