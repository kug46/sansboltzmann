// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define TARGETMETRIC_INSTANTIATE
#include "TargetMetric_impl.h"

#include "Field/XFieldLine.h"

#include "Field/FieldLine_DG_Cell.h"

#include "Field/FieldLine_CG_Cell.h"

namespace SANS
{

//INSTANTIATE_CONFORMITY_DUMP( PhysD1 , TopoD1 , Line );

template class TargetMetric<PhysD1,TopoD1>;

} // SANS
