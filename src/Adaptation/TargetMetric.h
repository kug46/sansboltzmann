// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ADAPTATION_TARGETMETRIC_H_
#define ADAPTATION_TARGETMETRIC_H_

#include "Field/XField.h"
#include "Field/Local/XField_Local_Base.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Type.h"

#include "MOESS/MOESS.h"
#include "MOESS/SolverInterfaceBase.h"

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <memory>

namespace SANS
{

//
// dummy solver interface class needed to create NodalMetrics class
//
template<class PhysDim, class TopoDim>
class SolverInterfaceDummy : public SolverInterfaceBase<PhysDim,TopoDim>
{
  static const int D = PhysDim::D;   // physical dimensions
  typedef DLA::MatrixSymS<D,Real> MatrixSym;
  typedef Field_CG_Cell<PhysDim, TopoDim, MatrixSym> MatrixSymFieldType;

public:
  SolverInterfaceDummy()
  {}

  ~SolverInterfaceDummy() {}

  void solveGlobalPrimalProblem() {}
  void solveGlobalAdjointProblem() {}

  void computeErrorEstimates() {}
  Real getElementalErrorEstimate(int cellgroup, int elem) const { return 0.0; }
  Real getGlobalErrorIndicator() const { return 0.0; }
  Real getGlobalErrorEstimate() const { return 0.0; }

  LocalSolveStatus
  solveLocalProblem( const XField_Local_Base<PhysDim,TopoDim>& local_xfld,
    std::vector<Real>& local_error) const
  { return LocalSolveStatus(true,1); }


  Real computeInitialError( const XField_Local_Base<PhysDim,TopoDim>& local_xfld) const
  { return 0.0; }


  Real getnDOFperCell(int cellgroup) const { return TopoDim::D+1; }
  int getSolutionOrder(int cellgroup) const { return 1; }

  Real getOutput() const { return 0.; }

  void output_EField(const std::string& filename) const {}

  int getCost() const { return 0.; }

  SpaceType spaceType() const { return SpaceType::Unspecified; }
};

template<class PhysDim,class TopoDim>
class TargetMetric
{
  typedef PhysDim PhysD;
  typedef TopoDim TopoD;
  static const int D = PhysDim::D;

  typedef DLA::VectorS<D,Real> VectorX;
  typedef DLA::MatrixSymS<D,Real> MatrixSym;
  typedef DLA::VectorS<D,Real> Vector;

  typedef Field_CG_Cell<PhysDim, TopoDim, MatrixSym> MatrixSymFieldType;

public:
  TargetMetric( const XField<PhysD,TopoD>& xfld ,
                const std::vector<int>& cellGroups ,
                MatrixSymFieldType& metric ,
                const PyDict& dict_ );

  void getNodalMetricRequestField(MatrixSymFieldType& metric_fld)
  { nodalMetrics_->getNodalMetricRequestField(metric_fld); }

  void getNodalMetricImpliedField(MatrixSymFieldType& metric_fld)
  { nodalMetrics_->getNodalImpliedMetricField(metric_fld); }

  const NodalMetrics<PhysDim,TopoDim>& nodalMetrics() const { return *nodalMetrics_; }
  NodalMetrics<PhysDim,TopoDim>& nodalMetrics() { return *nodalMetrics_; }

  template<class Topology>
  void dumpMetricConformity( const std::string& dir , const std::string& label );

private:
  const XField<PhysD,TopoD>& xfld_;
  MatrixSymFieldType& target_;
  std::shared_ptr<NodalMetrics<PhysDim,TopoDim>> nodalMetrics_;
};

} // SANS

#endif
