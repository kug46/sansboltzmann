#!/bin/bash -xue

if [ ! -d avro ]; then
  git clone ssh://acdl.mit.edu/home/svnroot/avro
  cd avro 
elif [ ! -d avro/.git ]; then
  set +x
  echo "'avro' directory exists and is not a git repostory."
  echo "Please run this script in a different directory."
  exit 1
else
  cd avro 
  git pull
fi

git archive --format=tar.gz --prefix=avro/ HEAD > ../avro.tgz
set +x
echo "Created avro.tgz!"