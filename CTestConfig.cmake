## This file should be placed in the root directory of your project.
## Then modify the CMakeLists.txt file in the root directory of your
## project to incorporate the testing dashboard.
## # The following are required to uses Dart and the Cdash dashboard
##   ENABLE_TESTING()
##   INCLUDE(CTest)
set(CTEST_PROJECT_NAME "SANSproto")
set(CTEST_NIGHTLY_START_TIME "01:00:00 UTC")

set(CTEST_DROP_METHOD "http")
set(CTEST_DROP_SITE "belliveau.mit.edu")
set(CTEST_DROP_LOCATION "/CDash/submit.php?project=SANSproto")
set(CTEST_DROP_SITE_CDASH TRUE)

find_program(CTEST_COVERAGE_COMMAND NAMES gcov)
find_program(CTEST_MEMORYCHECK_COMMAND NAMES valgrind)

mark_as_advanced( CTEST_COVERAGE_COMMAND CTEST_MEMORYCHECK_COMMAND )
