// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// List of system files to be used as a pre-compiled header to reduce compile time.
// These files will be implicitly included to all unit test btest.cpp files.
// Hence, the includes here should be restricted to files included in nearly all btest.cpp files.

#ifdef __clang__
#pragma clang system_header
#elif defined(__GNUC__)
#pragma GCC system_header
#endif

// Don't include python. It significantly bloats the precompiled header

// std library
#include <cmath>
#include <cstdio>
#include <map>
#include <vector>
#include <list>
#include <array>
#include <string>
#include <memory>
#include <algorithm>
#include <type_traits>
#include <typeinfo>
#include <ostream>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>

// Surreal uses these
#include <boost/type_traits/is_arithmetic.hpp>
#include <boost/preprocessor/cat.hpp>

// SANS_ASSERT uses this
#include <boost/exception/exception.hpp>
#include <boost/throw_exception.hpp>
#include <boost/preprocessor/stringize.hpp>

// Boost meta programming library
#include <boost/mpl/assert.hpp>
#include <boost/mpl/vector.hpp>

// Boost unit testing framework
#include <boost/test/unit_test.hpp>
#include <boost/test/output_test_stream.hpp>
