// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SANS_CHECK_CLOSE

#include <cmath>

#define SANS_CHECK_CLOSE( expectedTruth, actualComputed, small_tol, close_tol ) \
  if ( std::abs(expectedTruth) < small_tol ) BOOST_CHECK_SMALL( actualComputed, small_tol ); \
  else BOOST_CHECK_CLOSE( expectedTruth, actualComputed, close_tol );

#endif //SANS_CHECK_CLOSE
