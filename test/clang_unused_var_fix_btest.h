// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef CLANG_UNUSED_VAR_FIX_BTEST_H
#define CLANG_UNUSED_VAR_FIX_BTEST_H
/*
 *  Clang warns about
 *  unused variable 'check_is_close'
 *  and
 *  unused variable 'check_is_small'
 *  if BOOST_*_CLOSE and BOOST_*_SMALL
 *  are not used in any test cases. Include
 *  this file in the test suite to avoid
 *  this warning.
 */
#ifdef __clang__

// Use an unnamed namespace so this file can be included in many files with the same test suite
namespace
{

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-function"

//----------------------------------------------------------------------------//
void clang_unused_variable_fix()
{
  BOOST_WARN_CLOSE( 1., 1., 1. );
  BOOST_WARN_SMALL( 0.,  1. );
}

#pragma clang diagnostic pop

}

#endif

#endif
