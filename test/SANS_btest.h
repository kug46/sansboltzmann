// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SANS_BTEST_H
#define SANS_BTEST_H

//This is a header file that contains include files suitable for all SANS unit tests

#include "SANS_CHECK_CLOSE_btest.h"

#endif //SANS_BTEST_H
