// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Field2D_DG_btest
// testing of Field2D_DG_* classes
//

//#define SLEEP_FOR_OUTPUT
#ifdef SLEEP_FOR_OUTPUT
#define SLEEP_MILLISECONDS 500
#include <chrono>
#include <thread>
#endif

#include <set>
#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;


#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"


#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_EG_Cell.h"

#include "Field/output_Tecplot.h"
#include "tools/output_std_vector.h"

#ifdef SANS_MPI
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/mpi/collectives/all_gather.hpp>
#endif

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


//Explicitly instantiate classes to get proper coverage information
namespace SANS
{
template class Field_CG_Cell< PhysD2, TopoD2, Real >;
template class Field_EG_Cell< PhysD2, TopoD2, Real >;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( Field_EG_Triangle_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EG_Area_LegendreP0 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef Field_EG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_EG_Area;
  typedef QField2D_EG_Area::FieldCellGroupType<Triangle> QFieldAreaClass;
  typedef QFieldAreaClass::ElementType<> ElementQFieldClass;

  XField2D_2Triangle_X1_1Group xfld1;

  int cgorder = 1;
  int order = 0;
  QField2D_CG_Area cgfld(xfld1, cgorder, BasisFunctionCategory_Lagrange);
  QField2D_EG_Area qfld1(cgfld, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK( qfld1.spaceType() == SpaceType::Discontinuous );

  BOOST_CHECK_EQUAL( 2, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  BOOST_CHECK_EQUAL( 2, qfld1.nDOFCellGroup(0) );
  BOOST_CHECK_THROW( qfld1.nDOFInteriorTraceGroup(0), SANSException);
  BOOST_CHECK_THROW( qfld1.nDOFBoundaryTraceGroup(0), SANSException);

  XField2D_4Triangle_X1_1Group xfld2;

  QField2D_CG_Area cgfld2(xfld2, cgorder, BasisFunctionCategory_Lagrange);
  QField2D_EG_Area qfld2(cgfld2, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 4, qfld2.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld2.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld2, &qfld2.getXField() );

  BOOST_CHECK_EQUAL( 4, qfld2.nDOFCellGroup(0) );
  BOOST_CHECK_THROW( qfld2.nDOFInteriorTraceGroup(0), SANSException);
  BOOST_CHECK_THROW( qfld2.nDOFBoundaryTraceGroup(0), SANSException);

  const int nDOFPDE = qfld2.nDOF();
  for (int n = 0; n < nDOFPDE; n++)
    qfld2.DOF(n) = n;

  Field< PhysD2, TopoD2, ArrayQ > qfld3(qfld2, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld2.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld2.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nCellGroups()         ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld2.getXField()           , &qfld3.getXField() );

  BOOST_CHECK_THROW( qfld3.nDOFCellGroup(0), SANSException);
  BOOST_CHECK_THROW( qfld3.nDOFInteriorTraceGroup(0), SANSException);
  BOOST_CHECK_THROW( qfld3.nDOFBoundaryTraceGroup(0), SANSException);

  const QFieldAreaClass& qfld2Area = qfld2.getCellGroup<Triangle>(0);
  const QFieldAreaClass& qfld3Area = qfld3.getCellGroup<Triangle>(0);

  for (int n = 0; n < nDOFPDE; n++)
  {
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[0], n );
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[0], qfld2Area.DOF(n)[0] );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0], n );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0], qfld3Area.DOF(n)[0] );
  }

  for (int n = 0; n < nDOFPDE; n++)
    qfld2.DOF(n) = 0;

  for (int n = 0; n < nDOFPDE; n++)
  {
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[0], 0 );
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[0], qfld2Area.DOF(n)[0] );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0], n );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0], qfld3Area.DOF(n)[0] );
  }


  // Test the constant assignment operator
  const QFieldAreaClass& qfld1Area = qfld1.getCellGroup<Triangle>(0);
  ElementQFieldClass qfldElem(qfld1Area.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfld1Area.nElem(); elem++)
  {
    qfld1Area.getElement(qfldElem, elem);

    qfldElem.eval(0.25, 0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfld1Area.nElem(); elem++)
  {
    qfld1Area.getElement(qfldElem, elem);

    qfldElem.eval(0.25, 0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EG_Area_LegendreP1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef Field_EG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_EG_Area;
  typedef QField2D_EG_Area::FieldCellGroupType<Triangle> QFieldAreaClass;
  typedef QFieldAreaClass::ElementType<> ElementQFieldClass;

  XField2D_2Triangle_X1_1Group xfld1;

  int cgorder = 1;
  int order = 1;
  QField2D_CG_Area cgfld(xfld1, cgorder, BasisFunctionCategory_Lagrange);
  QField2D_EG_Area qfld1(cgfld, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 2*3, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  BOOST_CHECK_EQUAL( 2*3, qfld1.nDOFCellGroup(0) );
  BOOST_CHECK_THROW( qfld1.nDOFInteriorTraceGroup(0), SANSException);
  BOOST_CHECK_THROW( qfld1.nDOFBoundaryTraceGroup(0), SANSException);

  XField2D_4Triangle_X1_1Group xfld2;

  QField2D_CG_Area cgfld2(xfld2, cgorder, BasisFunctionCategory_Lagrange);
  QField2D_EG_Area qfld2(cgfld2, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 4*3, qfld2.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld2.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld2, &qfld2.getXField() );

  BOOST_CHECK_EQUAL( 4*3, qfld2.nDOFCellGroup(0) );
  BOOST_CHECK_THROW( qfld2.nDOFInteriorTraceGroup(0), SANSException);
  BOOST_CHECK_THROW( qfld2.nDOFBoundaryTraceGroup(0), SANSException);

  const int nDOFPDE = qfld2.nDOF();
  for (int n = 0; n < nDOFPDE; n++)
    qfld2.DOF(n) = n;

  Field< PhysD2, TopoD2, ArrayQ > qfld3(qfld2, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld2.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld2.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nCellGroups()         ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld2.getXField()           , &qfld3.getXField() );

  BOOST_CHECK_THROW( qfld3.nDOFCellGroup(0), SANSException);
  BOOST_CHECK_THROW( qfld3.nDOFInteriorTraceGroup(0), SANSException);
  BOOST_CHECK_THROW( qfld3.nDOFBoundaryTraceGroup(0), SANSException);

        QFieldAreaClass& qfld2Area = qfld2.getCellGroup<Triangle>(0);
  const QFieldAreaClass& qfld3Area = qfld3.getCellGroup<Triangle>(0);

  for (int n = 0; n < nDOFPDE; n++)
  {
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[0], n );
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[0], qfld2Area.DOF(n)[0] );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0], n );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0], qfld3Area.DOF(n)[0] );
  }

  for (int n = 0; n < nDOFPDE; n++)
    qfld2.DOF(n) = 0;

  for (int n = 0; n < nDOFPDE; n++)
  {
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[0], 0 );
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[0], qfld2Area.DOF(n)[0] );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0], n );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0], qfld3Area.DOF(n)[0] );
  }


  // Test the constant assignment operator
  const QFieldAreaClass& qfld1Area = qfld1.getCellGroup<Triangle>(0);
  ElementQFieldClass qfldElem(qfld1Area.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfld1Area.nElem(); elem++)
  {
    qfld1Area.getElement(qfldElem, elem);

    qfldElem.eval(0.25, 0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfld1Area.nElem(); elem++)
  {
    qfld1Area.getElement(qfldElem, elem);

    qfldElem.eval(0.25, 0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EG_Area_LegendreP2 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef Field_EG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_EG_Area;
  typedef QField2D_EG_Area::FieldCellGroupType<Triangle> QFieldAreaClass;
  typedef QFieldAreaClass::ElementType<> ElementQFieldClass;

  XField2D_2Triangle_X1_1Group xfld1;

  int cgorder = 1;
  int order = 2;
  QField2D_CG_Area cgfld(xfld1, cgorder, BasisFunctionCategory_Lagrange);
  QField2D_EG_Area qfld1(cgfld, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 2*6, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  BOOST_CHECK_EQUAL( 2*6, qfld1.nDOFCellGroup(0) );
  BOOST_CHECK_THROW( qfld1.nDOFInteriorTraceGroup(0), SANSException);
  BOOST_CHECK_THROW( qfld1.nDOFBoundaryTraceGroup(0), SANSException);

  XField2D_4Triangle_X1_1Group xfld2;

  QField2D_CG_Area cgfld2(xfld2, cgorder, BasisFunctionCategory_Lagrange);
  QField2D_EG_Area qfld2(cgfld2, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 4*6, qfld2.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld2.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld2, &qfld2.getXField() );

  const int nDOFPDE = qfld2.nDOF();
  for (int n = 0; n < nDOFPDE; n++)
    qfld2.DOF(n) = n;

  Field< PhysD2, TopoD2, ArrayQ > qfld3(qfld2, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld2.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld2.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nCellGroups()      ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld2.getXField()           , &qfld3.getXField() );

  BOOST_CHECK_THROW( qfld3.nDOFCellGroup(0), SANSException);
  BOOST_CHECK_THROW( qfld3.nDOFInteriorTraceGroup(0), SANSException);
  BOOST_CHECK_THROW( qfld3.nDOFBoundaryTraceGroup(0), SANSException);

        QFieldAreaClass& qfld2Area = qfld2.getCellGroup<Triangle>(0);
  const QFieldAreaClass& qfld3Area = qfld3.getCellGroup<Triangle>(0);

  for (int n = 0; n < nDOFPDE; n++)
  {
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[0], n );
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[0], qfld2Area.DOF(n)[0] );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0], n );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0], qfld3Area.DOF(n)[0] );
  }

  for (int n = 0; n < nDOFPDE; n++)
    qfld2.DOF(n) = 0;

  for (int n = 0; n < nDOFPDE; n++)
  {
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[0], 0 );
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[0], qfld2Area.DOF(n)[0] );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0], n );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0], qfld3Area.DOF(n)[0] );
  }


  // Test the constant assignment operator
  const QFieldAreaClass& qfld1Area = qfld1.getCellGroup<Triangle>(0);
  ElementQFieldClass qfldElem(qfld1Area.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfld1Area.nElem(); elem++)
  {
    qfld1Area.getElement(qfldElem, elem);

    qfldElem.eval(0.25, 0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfld1Area.nElem(); elem++)
  {
    qfld1Area.getElement(qfldElem, elem);

    qfldElem.eval(0.25, 0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EG_Area_LegendreP3 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef Field_EG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_EG_Area;
  typedef QField2D_EG_Area::FieldCellGroupType<Triangle> QFieldAreaClass;
  typedef QFieldAreaClass::ElementType<> ElementQFieldClass;

  XField2D_2Triangle_X1_1Group xfld1;

  int cgorder = 1;
  int order = 3;
  QField2D_CG_Area cgfld(xfld1, cgorder, BasisFunctionCategory_Lagrange);
  QField2D_EG_Area qfld1(cgfld, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 2*10, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  BOOST_CHECK_EQUAL( 2*10, qfld1.nDOFCellGroup(0) );
  BOOST_CHECK_THROW( qfld1.nDOFInteriorTraceGroup(0), SANSException);
  BOOST_CHECK_THROW( qfld1.nDOFBoundaryTraceGroup(0), SANSException);

  XField2D_4Triangle_X1_1Group xfld2;

  QField2D_CG_Area cgfld2(xfld2, cgorder, BasisFunctionCategory_Lagrange);
  QField2D_EG_Area qfld2(cgfld2, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 4*10, qfld2.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld2.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld2, &qfld2.getXField() );

  const int nDOFPDE = qfld2.nDOF();
  for (int n = 0; n < nDOFPDE; n++)
    qfld2.DOF(n) = n;

  Field< PhysD2, TopoD2, ArrayQ > qfld3(qfld2, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld2.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld2.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nCellGroups()      ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld2.getXField()           , &qfld3.getXField() );

  BOOST_CHECK_THROW( qfld3.nDOFCellGroup(0), SANSException);
  BOOST_CHECK_THROW( qfld3.nDOFInteriorTraceGroup(0), SANSException);
  BOOST_CHECK_THROW( qfld3.nDOFBoundaryTraceGroup(0), SANSException);

        QFieldAreaClass& qfld2Area = qfld2.getCellGroup<Triangle>(0);
  const QFieldAreaClass& qfld3Area = qfld3.getCellGroup<Triangle>(0);

  for (int n = 0; n < nDOFPDE; n++)
  {
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[0], n );
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[0], qfld2Area.DOF(n)[0] );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0], n );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0], qfld3Area.DOF(n)[0] );
  }

  for (int n = 0; n < nDOFPDE; n++)
    qfld2.DOF(n) = 0;

  for (int n = 0; n < nDOFPDE; n++)
  {
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[0], 0 );
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[0], qfld2Area.DOF(n)[0] );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0], n );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0], qfld3Area.DOF(n)[0] );
  }


  // Test the constant assignment operator
  const QFieldAreaClass& qfld1Area = qfld1.getCellGroup<Triangle>(0);
  ElementQFieldClass qfldElem(qfld1Area.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfld1Area.nElem(); elem++)
  {
    qfld1Area.getElement(qfldElem, elem);

    qfldElem.eval(0.25, 0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfld1Area.nElem(); elem++)
  {
    qfld1Area.getElement(qfldElem, elem);

    qfldElem.eval(0.25, 0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}



//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EG_Area_ProjectPtoPp1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef Field_EG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_EG_Area;
  typedef QField2D_EG_Area::FieldCellGroupType<Triangle> QFieldAreaClass;
  typedef QFieldAreaClass::ElementType<> ElementQFieldClass;

  ArrayQ q0, q1;

  XField2D_4Triangle_X1_1Group xfld1;
  int cgorder = 1;
  QField2D_CG_Area cgfld(xfld1, cgorder, BasisFunctionCategory_Lagrange);

  for (int order = 0; order < BasisFunctionArea_Triangle_LegendrePMax; order++)
  {
    for (int orderinc = 1; orderinc <= BasisFunctionArea_Triangle_LegendrePMax-order; orderinc++)
    {
      QField2D_EG_Area qfldP  (cgfld, order         , BasisFunctionCategory_Legendre);
      QField2D_EG_Area qfldPp1(cgfld, order+orderinc, BasisFunctionCategory_Legendre);

      typedef QField2D_EG_Area::FieldCellGroupType<Triangle> QFieldAreaClass;

      QFieldAreaClass& qfldAeraP   = qfldP.getCellGroup<Triangle>(0);
      QFieldAreaClass& qfldAeraPp1 = qfldPp1.getCellGroup<Triangle>(0);

      ElementQFieldClass qfldElemP(qfldAeraP.basis());
      ElementQFieldClass qfldElemPp1(qfldAeraPp1.basis());

      //Give some non-zero initial condition
      for (int n = 0; n < qfldP.nDOF(); n++)
        qfldP.DOF(n) = n+1;

      for (int n = 0; n < qfldPp1.nDOF(); n++)
        qfldPp1.DOF(n) = 0;

      //Use area function projectTo
      qfldAeraP.projectTo(qfldAeraPp1);

      for (int elem = 0; elem < qfldAeraP.nElem(); elem++)
      {
        qfldAeraP.getElement(qfldElemP, elem);
        qfldAeraPp1.getElement(qfldElemPp1, elem);

        qfldElemP.eval(0.25, 0.25, q0);
        qfldElemPp1.eval(0.25, 0.25, q1);
        BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
      }

      //Wipe out DOF's for P1
      for (int n = 0; n < qfldPp1.nDOF(); n++)
        qfldPp1.DOF(n) = -1;

      //Use base function projectTo
      qfldP.projectTo(qfldPp1);

      for (int elem = 0; elem < qfldAeraP.nElem(); elem++)
      {
        qfldAeraP.getElement(qfldElemP, elem);
        qfldAeraPp1.getElement(qfldElemPp1, elem);

        qfldElemP.eval(0.25, 0.25, q0);
        qfldElemPp1.eval(0.25, 0.25, q1);
        BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
      }
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ParallelCopy )
{
  typedef Real ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef Field_EG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_EG_Area;
  typedef QField2D_EG_Area::FieldCellGroupType<Triangle> QFieldAreaClass;

  // global communicator
  mpi::communicator world;

  int ii = 4;
  int jj = 5;

  // Generate a partitioned grid
  XField2D_Box_Triangle_Lagrange_X1 xfld(world, ii, jj);

  BOOST_CHECK_EQUAL( (ii+1)*(jj+1), xfld.nDOFnative());

  // fields
  int cgorder = 1;
  QField2D_CG_Area cgfld1(xfld, cgorder, BasisFunctionCategory_Lagrange);

  int order = 1;
  QField2D_EG_Area qfld1(cgfld1, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( (order+1)*(order+2)*ii*jj, qfld1.nDOFnative() );

  const int nDOFPDE = qfld1.nDOF();
  for (int n = 0; n < nDOFPDE; n++)
    qfld1.DOF(n) = n;

  QField2D_EG_Area qfld2(qfld1, FieldCopy());

  BOOST_CHECK_EQUAL( (order+1)*(order+2)*ii*jj, qfld2.nDOFnative() );

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld2.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld2.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld2.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()         ,  qfld2.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld2.getXField() );

  const QFieldAreaClass& qfld1Area = qfld1.getCellGroup<Triangle>(0);
  const QFieldAreaClass& qfld2Area = qfld2.getCellGroup<Triangle>(0);

  for (int n = 0; n < nDOFPDE; n++)
  {
    BOOST_CHECK_EQUAL( qfld1.DOF(n), n );
    BOOST_CHECK_EQUAL( qfld1.DOF(n), qfld1Area.DOF(n) );
    BOOST_CHECK_EQUAL( qfld2.DOF(n), n );
    BOOST_CHECK_EQUAL( qfld2.DOF(n), qfld2Area.DOF(n) );
    BOOST_CHECK_EQUAL( qfld1.local2nativeDOFmap(n), qfld2.local2nativeDOFmap(n) );
  }

  for (int n = 0; n < qfld1.nDOF() - qfld1.nDOFpossessed(); n++)
  {
     BOOST_CHECK_EQUAL( qfld1.DOFghost_rank(n), qfld2.DOFghost_rank(n) );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PartitionedNativeIndexing )
{
  typedef Real ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef Field_EG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_EG_Area;
  typedef QField2D_EG_Area::FieldCellGroupType<Triangle> QFieldAreaClass;

  // global communicator
  mpi::communicator world;

  int ii = 4;
  int jj = 5;

  // Generate a global grid (identical on all processors)
  XField2D_Box_Triangle_Lagrange_X1 xfld_global(world.split(world.rank()), ii, jj);

  // fields
  int cgorder = 1;
  QField2D_CG_Area cgfld_global(xfld_global, cgorder, BasisFunctionCategory_Lagrange);

  // Generate a partitioned grid
  XField2D_Box_Triangle_Lagrange_X1 xfld_local(world, ii, jj);
  QField2D_CG_Area cgfld_local(xfld_local, cgorder, BasisFunctionCategory_Lagrange);

  BOOST_CHECK_EQUAL(xfld_global.nDOFnative(), xfld_local.nDOFnative());

  // local and global fields
  int order = 1;
  QField2D_EG_Area qfld_local(cgfld_local, order, BasisFunctionCategory_Legendre);
  QField2D_EG_Area qfld_global(cgfld_global, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL(qfld_global.nDOFnative(), qfld_local.nDOFnative());

  // gather the local native DOFs and make sure they are unique for each processor
  std::vector<int> nativeDOF(qfld_local.nDOFpossessed());
  for (int idof = 0; idof < qfld_local.nDOFpossessed(); idof++)
    nativeDOF[idof] = qfld_local.local2nativeDOFmap(idof);

  // send the native DOF to all other ranks
  std::vector<std::vector<int>> globalnativeDOF(world.size());
#ifdef SANS_MPI
  boost::mpi::all_gather(world, nativeDOF, globalnativeDOF);
#else
  globalnativeDOF[0] = nativeDOF;
#endif

  // check that the possessed nativeDOF is unieque to each processor
  std::set<int> uniqueNativeDOF;
  for (std::size_t rank = 0; rank < globalnativeDOF.size(); rank++)
  {
    for ( const int nativeDOF : globalnativeDOF[rank])
    {
      BOOST_CHECK(uniqueNativeDOF.find(nativeDOF) == uniqueNativeDOF.end());
      uniqueNativeDOF.insert(nativeDOF);
    }
  }

  // check that the unique count adds up to the total DOF count
  BOOST_CHECK_EQUAL(qfld_global.nDOF(), uniqueNativeDOF.size());


  // check that local2nativeDOFmap is identity on a single processor
  if (world.size() == 1)
    for (int idof = 0; idof < qfld_local.nDOF(); idof++)
      BOOST_CHECK_EQUAL(idof, qfld_local.local2nativeDOFmap(idof));

  for (int idof = 0; idof < qfld_global.nDOF(); idof++)
    BOOST_CHECK_EQUAL(idof, qfld_global.local2nativeDOFmap(idof));


  int group = 0;

  // local and global cell groups
  const QFieldAreaClass& qfldCellGroup_local = qfld_local.getCellGroup<Triangle>(group);
  const QFieldAreaClass& qfldCellGroup_global = qfld_global.getCellGroup<Triangle>(group);

  int nBasis = qfldCellGroup_local.basis()->nBasis();
  std::vector<int> map_local(nBasis);
  std::vector<int> map_global(nBasis);

  const std::vector<int>& groupCellID = xfld_local.cellIDs(group);

  world.barrier();
  for (int irank = 0; irank < world.size(); irank++)
  {
    std::cout << std::flush;
#ifdef SLEEP_FOR_OUTPUT
    std::this_thread::sleep_for( std::chrono::milliseconds(SLEEP_MILLISECONDS) );
#endif
    world.barrier();
    if (irank != world.rank() ) continue;

    // collect the DOF indexing on the current rank in order to construct
    // the global continuous mapping
    for (int elem_local = 0; elem_local < qfldCellGroup_local.nElem(); elem_local++)
    {
      int elem_global = groupCellID[elem_local];
      qfldCellGroup_local.associativity(elem_local).getGlobalMapping(map_local.data(), map_local.size());
      qfldCellGroup_global.associativity(elem_global).getGlobalMapping(map_global.data(), map_global.size());

#if 0
      int elemRank = qfldCellGroup_local.associativity(elem_local).rank();
      std::cout << "rank " << world.rank() << " elemRank = " << elemRank
                << " elem_global = " << elem_global << " map_global = " << map_global << " l2n = ";
      for (int n = 0; n < nBasis; n++)
        std::cout << qfld_local.local2nativeDOFmap(map_local[n]) << " ";
      std::cout << std::endl;
#endif
      for (int n = 0; n < nBasis; n++)
        BOOST_CHECK_EQUAL(map_global[n], qfld_local.local2nativeDOFmap(map_local[n]));
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PartitionedContinuousIndexing )
{
  typedef Real ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef Field_EG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_EG_Area;
  typedef QField2D_EG_Area::FieldCellGroupType<Triangle> QFieldAreaClass;

  // global communicator
  mpi::communicator world;

  int ii = 4;
  int jj = 5;

  // Generate a partitioned grid
  XField2D_Box_Triangle_Lagrange_X1 xfld(world, ii, jj);

  // fields
  int cgorder = 1;
  QField2D_CG_Area cgfld(xfld, cgorder, BasisFunctionCategory_Lagrange);

  int order = 1;
  QField2D_EG_Area qfld(cgfld, order, BasisFunctionCategory_Legendre);

  int group = 0;

  // local cell group
  const QFieldAreaClass& qfldCellGroup = qfld.getCellGroup<Triangle>(group);
  int nBasis = qfldCellGroup.basis()->nBasis();
  std::vector<int> map(nBasis);

  const std::vector<int>& groupCellID = xfld.cellIDs(group);

  const GlobalContinuousMap& continuousGlobalMap = qfld.continuousGlobalMap();

  std::map<int,int> cellID2local;
  int nElemPossessed = 0;

  world.barrier();
  for (int irank = 0; irank < world.size(); irank++)
  {
    std::cout << std::flush;
#ifdef SLEEP_FOR_OUTPUT
    std::this_thread::sleep_for( std::chrono::milliseconds(SLEEP_MILLISECONDS) );
#endif
    world.barrier();
    if (irank != world.rank() ) continue;

    // collect the DOF indexing on the current rank in order to construct
    // the global continuous mapping
    for (int elem = 0; elem < qfldCellGroup.nElem(); elem++)
    {
      if (qfldCellGroup.associativity(elem).rank() == world.rank())
      {
        //std::cout << irank << " : ID " << cellID2local[groupCellID[elem]] << " : " << " elem " << elem << " = " << map << std::endl;
        cellID2local[groupCellID[elem]] = elem;
        nElemPossessed++;
      }
    }
  }
  world.barrier();


  // send the DOF index to all other ranks
  std::vector<int> nElemOnRank(world.size());
  std::vector<std::map<int,int>> globalCellID2local(world.size());
#ifdef SANS_MPI
  boost::mpi::all_gather(world, nElemPossessed, nElemOnRank);
  boost::mpi::all_gather(world, cellID2local, globalCellID2local);
#else
  nElemOnRank[0] = nElemPossessed;
  globalCellID2local[0] = cellID2local;
#endif

  // compute the DOF rank offset on all ranks
  std::vector<int> nDOF_rank_offset(world.size(), 0);
  for (int rank = 1; rank < world.size(); rank++)
    nDOF_rank_offset[rank] = nDOF_rank_offset[rank-1] + nBasis*nElemOnRank[rank-1];

  // construct a continuous indexing across all processors
  std::vector<std::vector<std::vector<int>>> continuousDOFindx(world.size());
  int continuousIdx = 0;
  for (int rank = 0; rank < world.size(); rank++)
  {
    continuousDOFindx[rank].resize(nElemOnRank[rank]);
    for (std::size_t elem = 0; elem < continuousDOFindx[rank].size(); elem++)
    {
      continuousDOFindx[rank][elem].resize(nBasis);
      for (int n = 0; n < nBasis; n++)
        continuousDOFindx[rank][elem][n] = continuousIdx++;
    }
  }

  // check that the nDOF rank offset is correct
  BOOST_CHECK_EQUAL( nDOF_rank_offset[world.rank()], continuousGlobalMap.nDOF_rank_offset );

  int nElemZombie = 0;
  world.barrier();
  for (int irank = 0; irank < world.size(); irank++)
  {
    std::cout << std::flush;
#ifdef SLEEP_FOR_OUTPUT
    std::this_thread::sleep_for( std::chrono::milliseconds(SLEEP_MILLISECONDS) );
#endif
    world.barrier();
    if (irank != world.rank() ) continue;

    for (int elem = 0; elem < qfldCellGroup.nElem(); elem++)
    {
      qfldCellGroup.associativity(elem).getGlobalMapping(map.data(), map.size());

      int rank   = qfldCellGroup.associativity(elem).rank();
      int cellID = groupCellID[elem];

      // Zombies are not included as part of the continuous map
      if (map[0] >= qfld.nDOFpossessed() + qfld.nDOFghost())
      {
        nElemZombie++;
        continue;
      }

      // the local DOF indexing should stride with the nBasis and local element number (skipping over Zombie elements)
      for (int i = 0; i < nBasis; i++)
        BOOST_CHECK_EQUAL( (elem-nElemZombie)*nBasis + i, map[i] );

      if (rank == world.rank())
      {
        // check that the global continuous indexing is correct for elements possessed by this processor
        for (int i = 0; i < nBasis; i++)
          BOOST_CHECK_EQUAL( continuousDOFindx[rank][elem][i], map[i] + continuousGlobalMap.nDOF_rank_offset );
      }
      else
      {
        int remoteElem = globalCellID2local[rank].at(cellID);

#if 0
        std::cout << world.rank() << " " << rank << " : cellID " << cellID << " remoteElem " << remoteElem
                  << " = " << continuousDOFindx[rank][remoteElem] <<  " : ";
        for (int i = 0; i < nBasis; i++)
        {
          int localGhostIndex = map[i] - continuousGlobalMap.nDOFpossessed;
          int globalIndex = continuousGlobalMap.remoteGhostIndex[localGhostIndex];
          std::cout << globalIndex << " ";
        }
        std::cout << " : ";
#endif

        for (int i = 0; i < nBasis; i++)
        {
          int localGhostIndex = map[i] - continuousGlobalMap.nDOFpossessed;
          int nativeIndex = continuousGlobalMap.remoteGhostIndex[localGhostIndex];

          BOOST_CHECK_EQUAL( continuousDOFindx[rank][remoteElem][i], nativeIndex );
        }
      }
      std::cout << std::flush;
    }
  }
  world.barrier();

  //output_Tecplot(xfld, "tmp/test" + std::to_string(world.rank()) + ".dat");
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PartitionedSyncDOF )
{
  typedef Real ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef Field_EG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_EG_Area;
  typedef QField2D_EG_Area::FieldCellGroupType<Triangle> QFieldAreaClass;

  // global communicator
  mpi::communicator world;

  int ii = 4;
  int jj = 5;

  // Generate a partitioned grid
  XField2D_Box_Triangle_Lagrange_X1 xfld(world, ii, jj);

  // fields
  int cgorder = 1;
  QField2D_CG_Area cgfld(xfld, cgorder, BasisFunctionCategory_Lagrange);

  int order = 1;
  QField2D_EG_Area qfld(cgfld, order, BasisFunctionCategory_Legendre);

  int group = 0;

  // local cell group
  const QFieldAreaClass& qfldCellGroup = qfld.getCellGroup<Triangle>(group);
  int nBasis = qfldCellGroup.basis()->nBasis();
  std::vector<int> map(nBasis);

  const std::vector<int>& groupCellID = xfld.cellIDs(group);


  std::map<int,int> cellID2local;
  int nElemPossessed = 0;

  world.barrier();
  for (int irank = 0; irank < world.size(); irank++)
  {
    std::cout << std::flush;
#ifdef SLEEP_FOR_OUTPUT
    std::this_thread::sleep_for( std::chrono::milliseconds(SLEEP_MILLISECONDS) );
#endif
    world.barrier();
    if (irank != world.rank() ) continue;

    // collect the DOF indexing on the current rank in order to construct
    // the global continuous mapping
    for (int elem = 0; elem < qfldCellGroup.nElem(); elem++)
    {
      if (qfldCellGroup.associativity(elem).rank() == world.rank())
      {
        //std::cout << irank << " : ID " << cellID2local[groupCellID[elem]] << " : " << " elem " << elem << " = " << map << std::endl;
        cellID2local[groupCellID[elem]] = elem;
        nElemPossessed++;
      }
    }
  }
  world.barrier();

  // send the DOF index to all other ranks
  std::vector<int> nElemOnRank(world.size());
  std::vector<std::map<int,int>> globalCellID2local(world.size());
#ifdef SANS_MPI
  boost::mpi::all_gather(world, nElemPossessed , nElemOnRank);
  boost::mpi::all_gather(world, cellID2local, globalCellID2local);
#else
  nElemOnRank[0]  = nElemPossessed;
  globalCellID2local[0] = cellID2local;
#endif

  // construct a continuous indexing across all processors
  std::vector<std::vector<std::vector<int>>> continuousDOFindx(world.size());
  int continuousIdx = 0;
  for (int rank = 0; rank < world.size(); rank++)
  {
    continuousDOFindx[rank].resize(nElemOnRank[rank]);
    for (std::size_t elem = 0; elem < continuousDOFindx[rank].size(); elem++)
    {
      continuousDOFindx[rank][elem].resize(nBasis);
      for (int n = 0; n < nBasis; n++)
        continuousDOFindx[rank][elem][n] = continuousIdx++;
    }
  }

  // set all DOFs to -1
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = -1;

  for (int elem = 0; elem < qfldCellGroup.nElem(); elem++)
  {
    qfldCellGroup.associativity(elem).getGlobalMapping(map.data(), map.size());

    int rank = qfldCellGroup.associativity(elem).rank();

    if (rank == world.rank())
    {
      // assign the continuous index to possessed DOFs
      for (int i = 0; i < nBasis; i++)
        qfld.DOF(map[i]) = continuousDOFindx[rank][elem][i];
    }
  }

  // synchronize the DOFs. all ghosts and zombies should now get the continuous index
  qfld.syncDOFs_MPI_noCache();

  world.barrier();
  for (int irank = 0; irank < world.size(); irank++)
  {
    std::cout << std::flush;
#ifdef SLEEP_FOR_OUTPUT
    std::this_thread::sleep_for( std::chrono::milliseconds(SLEEP_MILLISECONDS) );
#endif
    world.barrier();
    if (irank != world.rank() ) continue;

    for (int elem = 0; elem < qfldCellGroup.nElem(); elem++)
    {
      qfldCellGroup.associativity(elem).getGlobalMapping(map.data(), map.size());

      int rank = qfldCellGroup.associativity(elem).rank();

      if (rank == world.rank())
      {
        // check that the global indexing has not been modified
        for (int i = 0; i < nBasis; i++)
          BOOST_CHECK_EQUAL( qfld.DOF(map[i]), continuousDOFindx[rank][elem][i] );
      }
      else
      {
        int cellID = groupCellID[elem];
        int remoteElem = globalCellID2local[rank].at(cellID);

        for (int i = 0; i < nBasis; i++)
          BOOST_CHECK_EQUAL( qfld.DOF(map[i]), continuousDOFindx[rank][remoteElem][i] );
      }
      std::cout << std::flush;
    }
  }
  world.barrier();

  //output_Tecplot(xfld, "tmp/test" + std::to_string(world.rank()) + ".dat");
}


# if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_Exception )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_DG_Area;
  typedef Field_DG_InteriorTrace< PhysD2, TopoD2, ArrayQ > QField2D_DG_InteriorEdge;
  typedef Field_DG_BoundaryTrace< PhysD2, TopoD2, ArrayQ > QField2D_DG_BoundaryEdge;

  XField2D_2Triangle_X1_1Group xfld1;

  int order = 999; //This should always be an order that is not available

  BOOST_CHECK_THROW( QField2D_DG_Area qfld1(xfld1, order, BasisFunctionCategory_Legendre), DeveloperException );
  BOOST_CHECK_THROW( QField2D_DG_Area qfld1(xfld1, order, BasisFunctionCategory_Legendre), DeveloperException );
  BOOST_CHECK_THROW( QField2D_DG_InteriorEdge qfld1(xfld1, order, BasisFunctionCategory_Legendre), DeveloperException );
  BOOST_CHECK_THROW( QField2D_DG_BoundaryEdge qfld1(xfld1, order, BasisFunctionCategory_Legendre), DeveloperException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_DG_Area;
  typedef Field_DG_InteriorTrace< PhysD2, TopoD2, ArrayQ > QField2D_DG_InteriorEdge;
  typedef Field_DG_BoundaryTrace< PhysD2, TopoD2, ArrayQ > QField2D_DG_BoundaryEdge;

  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Field/Field2D_DG_Triangle_pattern.txt", true );

  XField2D_2Triangle_X1_1Group xfld1;

  QField2D_DG_Area qfld1(xfld1, 0, BasisFunctionCategory_Legendre);
  qfld1 = 0;
  qfld1.dump( 2, output );

  QField2D_DG_InteriorEdge qfld2(xfld1, 2, BasisFunctionCategory_Legendre);
  qfld2 = 0;
  qfld2.dump( 2, output );

  QField2D_DG_BoundaryEdge qfld3(xfld1, 2, BasisFunctionCategory_Legendre);
  qfld3 = 0;
  qfld3.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}
#endif


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
