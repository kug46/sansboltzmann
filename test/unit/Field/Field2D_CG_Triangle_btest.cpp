// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Field2D_CG_Triangle_btest
// testing of Field2D_CG_* classes
//

//#define SLEEP_FOR_OUTPUT
#ifdef SLEEP_FOR_OUTPUT
#define SLEEP_MILLISECONDS 500
#include <chrono>
#include <thread>
#endif

#include <ostream>
#include <set>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_2Triangle_GhostBoundary_X1_1Group.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_4Group.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/output_std_vector.h"

#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Field/FieldArea_DG_Cell.h" // Project CG to DG

#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_CG_Trace.h"
#include "Field/FieldArea_CG_InteriorTrace.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"

#ifdef SANS_MPI
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/mpi/collectives/all_gather.hpp>
#endif

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


//Explicitly instantiate classes to get proper coverage information
namespace SANS
{
template class Field_CG_Cell< PhysD2, TopoD2, Real >;
template class Field_CG_Trace< PhysD2, TopoD2, Real >;
template class Field_CG_InteriorTrace< PhysD2, TopoD2, Real >;
template class Field_CG_BoundaryTrace< PhysD2, TopoD2, Real >;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( Field2D_CG_Triangle_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_Area_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef XField2D_2Triangle_X1_1Group::FieldCellGroupType<Triangle> XFieldAreaClass;
  typedef QField2D_CG_Area::FieldCellGroupType<Triangle> QFieldAreaClass;
  typedef QField2D_CG_Area::FieldTraceGroupType<Line> QFieldTraceClass;
  typedef QFieldAreaClass::ElementType<> ElementQFieldClass;
  typedef std::array<int,3> Int3;

  int nodeMap[3];

  XField2D_2Triangle_X1_1Group xfld1;


  const XFieldAreaClass& xfldGroup1 = xfld1.getCellGroup<Triangle>(0);

  int order = 1;
  QField2D_CG_Area qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK( qfld1.spaceType() == SpaceType::Continuous );

  BOOST_CHECK_EQUAL( 4, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfld1.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldAreaClass& qfldGroup1 = qfld1.getCellGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup1.nElem() );

  qfldGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 0, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1, nodeMap[1] );
  BOOST_CHECK_EQUAL( 2, nodeMap[2] );

  qfldGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 3, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2, nodeMap[1] );
  BOOST_CHECK_EQUAL( 1, nodeMap[2] );

  Int3 xedgeSign, qedgeSign;

  xedgeSign = xfldGroup1.associativity(0).edgeSign();
  qedgeSign = qfldGroup1.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( xedgeSign[0], qedgeSign[0] );
  BOOST_CHECK_EQUAL( xedgeSign[1], qedgeSign[1] );
  BOOST_CHECK_EQUAL( xedgeSign[2], qedgeSign[2] );

  xedgeSign = xfldGroup1.associativity(1).edgeSign();
  qedgeSign = qfldGroup1.associativity(1).edgeSign();
  BOOST_CHECK_EQUAL( xedgeSign[0], qedgeSign[0] );
  BOOST_CHECK_EQUAL( xedgeSign[1], qedgeSign[1] );
  BOOST_CHECK_EQUAL( xedgeSign[2], qedgeSign[2] );

  const QFieldTraceClass& qfldBTraceGroup1 = qfld1.getBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( 4, qfldBTraceGroup1.nElem() );

  qfldBTraceGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 0, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1, nodeMap[1] );

  qfldBTraceGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 1, nodeMap[0] );
  BOOST_CHECK_EQUAL( 3, nodeMap[1] );

  qfldBTraceGroup1.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 3, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2, nodeMap[1] );

  qfldBTraceGroup1.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 2, nodeMap[0] );
  BOOST_CHECK_EQUAL( 0, nodeMap[1] );

  XField2D_4Triangle_X1_1Group xfld2;

  const XFieldAreaClass& xfldGroup2 = xfld2.getCellGroup<Triangle>(0);

  QField2D_CG_Area qfld2(xfld2, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 6, qfld2.nDOF() );
  BOOST_CHECK_EQUAL( 4, qfld2.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld2.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld2, &qfld2.getXField() );

  QField2D_CG_Area qfld3(qfld2, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld2.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld2.nElem()               ,  qfld3.nElem() );
  BOOST_CHECK_EQUAL(  qfld2.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nCellGroups()         ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld2.getXField()           , &qfld3.getXField() );

  const QFieldAreaClass& qfldGroup2 = qfld2.getCellGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( 4, qfldGroup2.nElem() );

  qfldGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 0, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1, nodeMap[1] );
  BOOST_CHECK_EQUAL( 2, nodeMap[2] );

  qfldGroup2.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 3, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2, nodeMap[1] );
  BOOST_CHECK_EQUAL( 1, nodeMap[2] );

  qfldGroup2.associativity(2).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 2, nodeMap[0] );
  BOOST_CHECK_EQUAL( 4, nodeMap[1] );
  BOOST_CHECK_EQUAL( 0, nodeMap[2] );

  qfldGroup2.associativity(3).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 1, nodeMap[0] );
  BOOST_CHECK_EQUAL( 0, nodeMap[1] );
  BOOST_CHECK_EQUAL( 5, nodeMap[2] );

  const QFieldTraceClass& qfldBTraceGroup2 = qfld2.getBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( 6, qfldBTraceGroup2.nElem() );

  qfldBTraceGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 0, nodeMap[0] );
  BOOST_CHECK_EQUAL( 5, nodeMap[1] );

  qfldBTraceGroup2.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 5, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1, nodeMap[1] );

  qfldBTraceGroup2.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 1, nodeMap[0] );
  BOOST_CHECK_EQUAL( 3, nodeMap[1] );

  qfldBTraceGroup2.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 3, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2, nodeMap[1] );

  qfldBTraceGroup2.associativity(4).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 2, nodeMap[0] );
  BOOST_CHECK_EQUAL( 4, nodeMap[1] );

  qfldBTraceGroup2.associativity(5).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 4, nodeMap[0] );
  BOOST_CHECK_EQUAL( 0, nodeMap[1] );


  xedgeSign = xfldGroup2.associativity(0).edgeSign();
  qedgeSign = qfldGroup2.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( xedgeSign[0], qedgeSign[0] );
  BOOST_CHECK_EQUAL( xedgeSign[1], qedgeSign[1] );
  BOOST_CHECK_EQUAL( xedgeSign[2], qedgeSign[2] );

  xedgeSign = xfldGroup2.associativity(1).edgeSign();
  qedgeSign = qfldGroup2.associativity(1).edgeSign();
  BOOST_CHECK_EQUAL( xedgeSign[0], qedgeSign[0] );
  BOOST_CHECK_EQUAL( xedgeSign[1], qedgeSign[1] );
  BOOST_CHECK_EQUAL( xedgeSign[2], qedgeSign[2] );

  xedgeSign = xfldGroup2.associativity(2).edgeSign();
  qedgeSign = qfldGroup2.associativity(2).edgeSign();
  BOOST_CHECK_EQUAL( xedgeSign[0], qedgeSign[0] );
  BOOST_CHECK_EQUAL( xedgeSign[1], qedgeSign[1] );
  BOOST_CHECK_EQUAL( xedgeSign[2], qedgeSign[2] );

  xedgeSign = xfldGroup2.associativity(3).edgeSign();
  qedgeSign = qfldGroup2.associativity(3).edgeSign();
  BOOST_CHECK_EQUAL( xedgeSign[0], qedgeSign[0] );
  BOOST_CHECK_EQUAL( xedgeSign[1], qedgeSign[1] );
  BOOST_CHECK_EQUAL( xedgeSign[2], qedgeSign[2] );


  // Test the constant assignment operator
  const QFieldAreaClass& qfld1Area = qfld1.getCellGroup<Triangle>(0);
  ElementQFieldClass qfldElem(qfld1Area.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfld1Area.nElem(); elem++)
  {
    qfld1Area.getElement(qfldElem, elem);

    qfldElem.eval(0.25, 0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfld1Area.nElem(); elem++)
  {
    qfld1Area.getElement(qfldElem, elem);

    qfldElem.eval(0.25, 0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_BoundaryTrace_P1_Empty )
{
  typedef DLA::VectorS<2, Real> ArrayQ;

  XField2D_4Triangle_X1_4Group xfld1;

  int order = 1;
  Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> qfld1(xfld1, order, BasisFunctionCategory_Hierarchical, {}); // Empty with no groups

  BOOST_CHECK_EQUAL( 0, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_Area_P1_4Group )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef XField2D_4Triangle_X1_4Group::FieldCellGroupType<Triangle> XFieldAreaClass;
  typedef QField2D_CG_Area::FieldCellGroupType<Triangle> QFieldAreaClass;
  typedef QField2D_CG_Area::FieldTraceGroupType<Triangle> QFieldTraceClass;
  typedef QFieldAreaClass::ElementType<> ElementQFieldClass;
  typedef std::array<int,3> Int3;

  int nodeMap[3];

  XField2D_4Triangle_X1_4Group xfld1;

  const XFieldAreaClass& xfldGroup0 = xfld1.getCellGroup<Triangle>(0);
  const XFieldAreaClass& xfldGroup1 = xfld1.getCellGroup<Triangle>(1);
  const XFieldAreaClass& xfldGroup2 = xfld1.getCellGroup<Triangle>(2);
  const XFieldAreaClass& xfldGroup3 = xfld1.getCellGroup<Triangle>(3);

  int order = 1;
  QField2D_CG_Area qfld1( {{0}, {1,2,3}}, xfld1, order, BasisFunctionCategory_Hierarchical );

  BOOST_CHECK( qfld1.spaceType() == SpaceType::Continuous );

  BOOST_CHECK_EQUAL( 9, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 4, qfld1.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 3, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 4, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldAreaClass& qfld1Group0 = qfld1.getCellGroup<Triangle>(0);
  const QFieldAreaClass& qfld1Group1 = qfld1.getCellGroup<Triangle>(1);
  const QFieldAreaClass& qfld1Group2 = qfld1.getCellGroup<Triangle>(2);
  const QFieldAreaClass& qfld1Group3 = qfld1.getCellGroup<Triangle>(3);

  BOOST_CHECK_EQUAL( 1, qfld1Group0.nElem() );
  BOOST_CHECK_EQUAL( 1, qfld1Group1.nElem() );
  BOOST_CHECK_EQUAL( 1, qfld1Group2.nElem() );
  BOOST_CHECK_EQUAL( 1, qfld1Group3.nElem() );

  qfld1Group0.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 0, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1, nodeMap[1] );
  BOOST_CHECK_EQUAL( 2, nodeMap[2] );

  qfld1Group1.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 6, nodeMap[0] );
  BOOST_CHECK_EQUAL( 5, nodeMap[1] );
  BOOST_CHECK_EQUAL( 4, nodeMap[2] );

  qfld1Group2.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 5, nodeMap[0] );
  BOOST_CHECK_EQUAL( 7, nodeMap[1] );
  BOOST_CHECK_EQUAL( 3, nodeMap[2] );

  qfld1Group3.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 4, nodeMap[0] );
  BOOST_CHECK_EQUAL( 3, nodeMap[1] );
  BOOST_CHECK_EQUAL( 8, nodeMap[2] );

  const QFieldTraceClass& qfld1BTraceGroup0 = qfld1.getBoundaryTraceGroup<Line>(0);
  const QFieldTraceClass& qfld1BTraceGroup1 = qfld1.getBoundaryTraceGroup<Line>(1);
  const QFieldTraceClass& qfld1BTraceGroup2 = qfld1.getBoundaryTraceGroup<Line>(2);

  BOOST_CHECK_EQUAL( 2, qfld1BTraceGroup0.nElem() );
  BOOST_CHECK_EQUAL( 2, qfld1BTraceGroup1.nElem() );
  BOOST_CHECK_EQUAL( 2, qfld1BTraceGroup2.nElem() );

  qfld1BTraceGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 4, nodeMap[0] );
  BOOST_CHECK_EQUAL( 6, nodeMap[1] );

  qfld1BTraceGroup0.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 6, nodeMap[0] );
  BOOST_CHECK_EQUAL( 5, nodeMap[1] );

  qfld1BTraceGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 5, nodeMap[0] );
  BOOST_CHECK_EQUAL( 7, nodeMap[1] );

  qfld1BTraceGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 7, nodeMap[0] );
  BOOST_CHECK_EQUAL( 3, nodeMap[1] );

  qfld1BTraceGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 3, nodeMap[0] );
  BOOST_CHECK_EQUAL( 8, nodeMap[1] );

  qfld1BTraceGroup2.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 8, nodeMap[0] );
  BOOST_CHECK_EQUAL( 4, nodeMap[1] );

  Int3 xedgeSign, qedgeSign;

  xedgeSign = xfldGroup0.associativity(0).edgeSign();
  qedgeSign = qfld1Group0.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( xedgeSign[0], qedgeSign[0] );
  BOOST_CHECK_EQUAL( xedgeSign[1], qedgeSign[1] );
  BOOST_CHECK_EQUAL( xedgeSign[2], qedgeSign[2] );

  xedgeSign = xfldGroup1.associativity(0).edgeSign();
  qedgeSign = qfld1Group1.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( xedgeSign[0], qedgeSign[0] );
  BOOST_CHECK_EQUAL( xedgeSign[1], qedgeSign[1] );
  BOOST_CHECK_EQUAL( xedgeSign[2], qedgeSign[2] );

  xedgeSign = xfldGroup2.associativity(0).edgeSign();
  qedgeSign = qfld1Group2.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( xedgeSign[0], qedgeSign[0] );
  BOOST_CHECK_EQUAL( xedgeSign[1], qedgeSign[1] );
  BOOST_CHECK_EQUAL( xedgeSign[2], qedgeSign[2] );

  xedgeSign = xfldGroup3.associativity(0).edgeSign();
  qedgeSign = qfld1Group3.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( xedgeSign[0], qedgeSign[0] );
  BOOST_CHECK_EQUAL( xedgeSign[1], qedgeSign[1] );
  BOOST_CHECK_EQUAL( xedgeSign[2], qedgeSign[2] );


  // Test the constant assignment operator
  for (int group = 0; group < 3; group++)
  {
    const QFieldAreaClass& qfld1Area = qfld1.getCellGroup<Triangle>(group);
    ElementQFieldClass qfldElem(qfld1Area.basis());

    qfld1 = 1.23;
    ArrayQ q1;

    for (int elem = 0; elem < qfld1Area.nElem(); elem++)
    {
      qfld1Area.getElement(qfldElem, elem);

      qfldElem.eval(0.25, 0.25, q1);
      BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
      BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
    }

    ArrayQ q0 = {4.56, 7.89};
    qfld1 = q0;

    for (int elem = 0; elem < qfld1Area.nElem(); elem++)
    {
      qfld1Area.getElement(qfldElem, elem);

      qfldElem.eval(0.25, 0.25, q1);
      BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
      BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
    }
  }

  QField2D_CG_Area qfld2( {{0,1}, {2,3}}, xfld1, order, BasisFunctionCategory_Hierarchical );

  BOOST_CHECK( qfld2.spaceType() == SpaceType::Continuous );

  BOOST_CHECK_EQUAL( 9, qfld2.nDOF() );
  BOOST_CHECK_EQUAL( 4, qfld2.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 3, qfld2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 4, qfld2.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld2.getXField() );

  const QFieldAreaClass& qfld2Group0 = qfld2.getCellGroup<Triangle>(0);
  const QFieldAreaClass& qfld2Group1 = qfld2.getCellGroup<Triangle>(1);
  const QFieldAreaClass& qfld2Group2 = qfld2.getCellGroup<Triangle>(2);
  const QFieldAreaClass& qfld2Group3 = qfld2.getCellGroup<Triangle>(3);

  BOOST_CHECK_EQUAL( 1, qfld2Group0.nElem() );
  BOOST_CHECK_EQUAL( 1, qfld2Group1.nElem() );
  BOOST_CHECK_EQUAL( 1, qfld2Group2.nElem() );
  BOOST_CHECK_EQUAL( 1, qfld2Group3.nElem() );

  qfld2Group0.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 0, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1, nodeMap[1] );
  BOOST_CHECK_EQUAL( 2, nodeMap[2] );

  qfld2Group1.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 3, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2, nodeMap[1] );
  BOOST_CHECK_EQUAL( 1, nodeMap[2] );

  qfld2Group2.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 6, nodeMap[0] );
  BOOST_CHECK_EQUAL( 7, nodeMap[1] );
  BOOST_CHECK_EQUAL( 4, nodeMap[2] );

  qfld2Group3.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 5, nodeMap[0] );
  BOOST_CHECK_EQUAL( 4, nodeMap[1] );
  BOOST_CHECK_EQUAL( 8, nodeMap[2] );

  const QFieldTraceClass& qfld2BTraceGroup0 = qfld2.getBoundaryTraceGroup<Line>(0);
  const QFieldTraceClass& qfld2BTraceGroup1 = qfld2.getBoundaryTraceGroup<Line>(1);
  const QFieldTraceClass& qfld2BTraceGroup2 = qfld2.getBoundaryTraceGroup<Line>(2);

  BOOST_CHECK_EQUAL( 2, qfld2BTraceGroup0.nElem() );
  BOOST_CHECK_EQUAL( 2, qfld2BTraceGroup1.nElem() );
  BOOST_CHECK_EQUAL( 2, qfld2BTraceGroup2.nElem() );

  qfld2BTraceGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 1, nodeMap[0] );
  BOOST_CHECK_EQUAL( 3, nodeMap[1] );

  qfld2BTraceGroup0.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 3, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2, nodeMap[1] );

  qfld2BTraceGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 6, nodeMap[0] );
  BOOST_CHECK_EQUAL( 7, nodeMap[1] );

  qfld2BTraceGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 7, nodeMap[0] );
  BOOST_CHECK_EQUAL( 4, nodeMap[1] );

  qfld2BTraceGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 4, nodeMap[0] );
  BOOST_CHECK_EQUAL( 8, nodeMap[1] );

  qfld2BTraceGroup2.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 8, nodeMap[0] );
  BOOST_CHECK_EQUAL( 5, nodeMap[1] );


  xedgeSign = xfldGroup0.associativity(0).edgeSign();
  qedgeSign = qfld2Group0.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( xedgeSign[0], qedgeSign[0] );
  BOOST_CHECK_EQUAL( xedgeSign[1], qedgeSign[1] );
  BOOST_CHECK_EQUAL( xedgeSign[2], qedgeSign[2] );

  xedgeSign = xfldGroup1.associativity(0).edgeSign();
  qedgeSign = qfld2Group1.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( xedgeSign[0], qedgeSign[0] );
  BOOST_CHECK_EQUAL( xedgeSign[1], qedgeSign[1] );
  BOOST_CHECK_EQUAL( xedgeSign[2], qedgeSign[2] );

  xedgeSign = xfldGroup2.associativity(0).edgeSign();
  qedgeSign = qfld2Group2.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( xedgeSign[0], qedgeSign[0] );
  BOOST_CHECK_EQUAL( xedgeSign[1], qedgeSign[1] );
  BOOST_CHECK_EQUAL( xedgeSign[2], qedgeSign[2] );

  xedgeSign = xfldGroup3.associativity(0).edgeSign();
  qedgeSign = qfld2Group3.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( xedgeSign[0], qedgeSign[0] );
  BOOST_CHECK_EQUAL( xedgeSign[1], qedgeSign[1] );
  BOOST_CHECK_EQUAL( xedgeSign[2], qedgeSign[2] );

}

//----------------------------------------------------------------------------//
// NOTE: DOF ordering for P2 is interior edges, then boundary edges, then nodes
BOOST_AUTO_TEST_CASE( CG_Area_P2 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef QField2D_CG_Area::FieldCellGroupType<Triangle> QFieldAreaClass;
  //typedef QField2D_CG_Area::FieldTraceGroupType<Line> QFieldInteriorEdgeClass;
  typedef QField2D_CG_Area::FieldTraceGroupType<Line> QFieldTraceClass;
  typedef QFieldAreaClass::ElementType<> ElementQFieldClass;

  int nodeMap[3];
  int edgeMap[3];

  XField2D_2Triangle_X1_1Group xfld1;

  int order = 2;
  QField2D_CG_Area qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 4+5, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldAreaClass& qfldAreaGroup1 = qfld1.getCellGroup<Triangle>(0);

  qfldAreaGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 5, nodeMap[0] );
  BOOST_CHECK_EQUAL( 6, nodeMap[1] );
  BOOST_CHECK_EQUAL( 7, nodeMap[2] );

  qfldAreaGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 8, nodeMap[0] );
  BOOST_CHECK_EQUAL( 7, nodeMap[1] );
  BOOST_CHECK_EQUAL( 6, nodeMap[2] );

  qfldAreaGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( 2, edgeMap[0] );
  BOOST_CHECK_EQUAL( 1, edgeMap[1] );
  BOOST_CHECK_EQUAL( 0, edgeMap[2] );

  qfldAreaGroup1.associativity(1).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( 2, edgeMap[0] );
  BOOST_CHECK_EQUAL( 3, edgeMap[1] );
  BOOST_CHECK_EQUAL( 4, edgeMap[2] );

#if 0
  // TODO: I don't think we want trace groups on a cell field....
  const QFieldInteriorEdgeClass& qfldInteriorGroup1 = qfld1.getInteriorTraceGroup<Line>(0);

  qfldInteriorGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 6, nodeMap[0] );
  BOOST_CHECK_EQUAL( 7, nodeMap[1] );

  qfldInteriorGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 0, edgeMap[0] );
#endif

  const QFieldTraceClass& qfldBoundaryGroup1 = qfld1.getBoundaryTraceGroup<Line>(0);

  qfldBoundaryGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 5, nodeMap[0] );
  BOOST_CHECK_EQUAL( 6, nodeMap[1] );

  qfldBoundaryGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 6, nodeMap[0] );
  BOOST_CHECK_EQUAL( 8, nodeMap[1] );

  qfldBoundaryGroup1.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 8, nodeMap[0] );
  BOOST_CHECK_EQUAL( 7, nodeMap[1] );

  qfldBoundaryGroup1.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 7, nodeMap[0] );
  BOOST_CHECK_EQUAL( 5, nodeMap[1] );

  qfldBoundaryGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 0, edgeMap[0] );

  qfldBoundaryGroup1.associativity(1).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 3, edgeMap[0] );

  qfldBoundaryGroup1.associativity(2).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 4, edgeMap[0] );

  qfldBoundaryGroup1.associativity(3).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 1, edgeMap[0] );

  XField2D_4Triangle_X1_1Group xfld2;

  QField2D_CG_Area qfld2(xfld2, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 6+3+6, qfld2.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld2.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld2, &qfld2.getXField() );

  QField2D_CG_Area qfld3(qfld2, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld2.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld2.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nCellGroups()         ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld2.getXField()           , &qfld3.getXField() );

  const QFieldAreaClass& qfldAreaGroup2 = qfld2.getCellGroup<Triangle>(0);

  qfldAreaGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL(  9, nodeMap[0] );
  BOOST_CHECK_EQUAL( 10, nodeMap[1] );
  BOOST_CHECK_EQUAL( 11, nodeMap[2] );

  qfldAreaGroup2.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 12, nodeMap[0] );
  BOOST_CHECK_EQUAL( 11, nodeMap[1] );
  BOOST_CHECK_EQUAL( 10, nodeMap[2] );

  qfldAreaGroup2.associativity(2).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 11, nodeMap[0] );
  BOOST_CHECK_EQUAL( 13, nodeMap[1] );
  BOOST_CHECK_EQUAL(  9, nodeMap[2] );

  qfldAreaGroup2.associativity(3).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 10, nodeMap[0] );
  BOOST_CHECK_EQUAL(  9, nodeMap[1] );
  BOOST_CHECK_EQUAL( 14, nodeMap[2] );

#if 0
  // TODO: I don't think we want trace groups in a cell field
  const QFieldInteriorEdgeClass& qfldInteriorGroup2 = qfld2.getInteriorTraceGroup<Line>(0);

  qfldInteriorGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 10, nodeMap[0] );
  BOOST_CHECK_EQUAL( 11, nodeMap[1] );

  qfldInteriorGroup2.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 11, nodeMap[0] );
  BOOST_CHECK_EQUAL(  9, nodeMap[1] );

  qfldInteriorGroup2.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL(  9, nodeMap[0] );
  BOOST_CHECK_EQUAL( 10, nodeMap[1] );

  qfldInteriorGroup2.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 0, edgeMap[0] );

  qfldInteriorGroup2.associativity(1).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 1, edgeMap[0] );

  qfldInteriorGroup2.associativity(2).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 2, edgeMap[0] );
#endif

  const QFieldTraceClass& qfldBoundaryGroup2 = qfld2.getBoundaryTraceGroup<Line>(0);

  qfldBoundaryGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL(  9, nodeMap[0] );
  BOOST_CHECK_EQUAL( 14, nodeMap[1] );

  qfldBoundaryGroup2.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 14, nodeMap[0] );
  BOOST_CHECK_EQUAL( 10, nodeMap[1] );

  qfldBoundaryGroup2.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 10, nodeMap[0] );
  BOOST_CHECK_EQUAL( 12, nodeMap[1] );

  qfldBoundaryGroup2.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 12, nodeMap[0] );
  BOOST_CHECK_EQUAL( 11, nodeMap[1] );

  qfldBoundaryGroup2.associativity(4).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 11, nodeMap[0] );
  BOOST_CHECK_EQUAL( 13, nodeMap[1] );

  qfldBoundaryGroup2.associativity(5).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 13, nodeMap[0] );
  BOOST_CHECK_EQUAL(  9, nodeMap[1] );

  qfldBoundaryGroup2.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 3, edgeMap[0] );

  qfldBoundaryGroup2.associativity(1).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 6, edgeMap[0] );

  qfldBoundaryGroup2.associativity(2).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 5, edgeMap[0] );

  qfldBoundaryGroup2.associativity(3).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 7, edgeMap[0] );

  qfldBoundaryGroup2.associativity(4).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 8, edgeMap[0] );

  qfldBoundaryGroup2.associativity(5).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 2, edgeMap[0] );

  // Test the constant assignment operator
  const QFieldAreaClass& qfld1Area = qfld1.getCellGroup<Triangle>(0);
  ElementQFieldClass qfldElem(qfld1Area.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfld1Area.nElem(); elem++)
  {
    qfld1Area.getElement(qfldElem, elem);

    qfldElem.eval(0.25, 0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfld1Area.nElem(); elem++)
  {
    qfld1Area.getElement(qfldElem, elem);

    qfldElem.eval(0.25, 0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_Area_P2_4Group )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef XField2D_4Triangle_X1_4Group::FieldCellGroupType<Triangle> XFieldAreaClass;
  typedef QField2D_CG_Area::FieldCellGroupType<Triangle> QFieldAreaClass;
  typedef QField2D_CG_Area::FieldTraceGroupType<Triangle> QFieldTraceClass;
  typedef QFieldAreaClass::ElementType<> ElementQFieldClass;
  typedef std::array<int,3> Int3;

  int nodeMap[3];
  int edgeMap[3];

  XField2D_4Triangle_X1_4Group xfld1;

  const XFieldAreaClass& xfldGroup0 = xfld1.getCellGroup<Triangle>(0);
  const XFieldAreaClass& xfldGroup1 = xfld1.getCellGroup<Triangle>(1);
  const XFieldAreaClass& xfldGroup2 = xfld1.getCellGroup<Triangle>(2);
  const XFieldAreaClass& xfldGroup3 = xfld1.getCellGroup<Triangle>(3);

  int order = 2;

  QField2D_CG_Area qfld1( {{0}, {1,2,3}}, xfld1, order, BasisFunctionCategory_Hierarchical );

  BOOST_CHECK( qfld1.spaceType() == SpaceType::Continuous );

  BOOST_CHECK_EQUAL( 21, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 4, qfld1.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 3, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 4, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldAreaClass& qfld1Group0 = qfld1.getCellGroup<Triangle>(0);
  const QFieldAreaClass& qfld1Group1 = qfld1.getCellGroup<Triangle>(1);
  const QFieldAreaClass& qfld1Group2 = qfld1.getCellGroup<Triangle>(2);
  const QFieldAreaClass& qfld1Group3 = qfld1.getCellGroup<Triangle>(3);

  BOOST_CHECK_EQUAL( 1, qfld1Group0.nElem() );
  BOOST_CHECK_EQUAL( 1, qfld1Group1.nElem() );
  BOOST_CHECK_EQUAL( 1, qfld1Group2.nElem() );
  BOOST_CHECK_EQUAL( 1, qfld1Group3.nElem() );

  qfld1Group0.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 12, nodeMap[0] );
  BOOST_CHECK_EQUAL( 13, nodeMap[1] );
  BOOST_CHECK_EQUAL( 14, nodeMap[2] );

  qfld1Group1.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 18, nodeMap[0] );
  BOOST_CHECK_EQUAL( 17, nodeMap[1] );
  BOOST_CHECK_EQUAL( 16, nodeMap[2] );

  qfld1Group2.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 17, nodeMap[0] );
  BOOST_CHECK_EQUAL( 19, nodeMap[1] );
  BOOST_CHECK_EQUAL( 15, nodeMap[2] );

  qfld1Group3.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 16, nodeMap[0] );
  BOOST_CHECK_EQUAL( 15, nodeMap[1] );
  BOOST_CHECK_EQUAL( 20, nodeMap[2] );

  qfld1Group0.associativity(0).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL(  2, edgeMap[0] );
  BOOST_CHECK_EQUAL(  1, edgeMap[1] );
  BOOST_CHECK_EQUAL(  0, edgeMap[2] );

  qfld1Group1.associativity(0).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL(  7, edgeMap[0] );
  BOOST_CHECK_EQUAL(  8, edgeMap[1] );
  BOOST_CHECK_EQUAL( 10, edgeMap[2] );

  qfld1Group2.associativity(0).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL(  5, edgeMap[0] );
  BOOST_CHECK_EQUAL(  4, edgeMap[1] );
  BOOST_CHECK_EQUAL( 11, edgeMap[2] );

  qfld1Group3.associativity(0).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL(  6, edgeMap[0] );
  BOOST_CHECK_EQUAL(  9, edgeMap[1] );
  BOOST_CHECK_EQUAL(  3, edgeMap[2] );

  const QFieldTraceClass& qfld1BTraceGroup0 = qfld1.getBoundaryTraceGroup<Line>(0);
  const QFieldTraceClass& qfld1BTraceGroup1 = qfld1.getBoundaryTraceGroup<Line>(1);
  const QFieldTraceClass& qfld1BTraceGroup2 = qfld1.getBoundaryTraceGroup<Line>(2);

  BOOST_CHECK_EQUAL( 2, qfld1BTraceGroup0.nElem() );
  BOOST_CHECK_EQUAL( 2, qfld1BTraceGroup1.nElem() );
  BOOST_CHECK_EQUAL( 2, qfld1BTraceGroup2.nElem() );

  qfld1BTraceGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 16, nodeMap[0] );
  BOOST_CHECK_EQUAL( 18, nodeMap[1] );

  qfld1BTraceGroup0.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 18, nodeMap[0] );
  BOOST_CHECK_EQUAL( 17, nodeMap[1] );

  qfld1BTraceGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 17, nodeMap[0] );
  BOOST_CHECK_EQUAL( 19, nodeMap[1] );

  qfld1BTraceGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 19, nodeMap[0] );
  BOOST_CHECK_EQUAL( 15, nodeMap[1] );

  qfld1BTraceGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 15, nodeMap[0] );
  BOOST_CHECK_EQUAL( 20, nodeMap[1] );

  qfld1BTraceGroup2.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 20, nodeMap[0] );
  BOOST_CHECK_EQUAL( 16, nodeMap[1] );

  qfld1BTraceGroup0.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL(  8, edgeMap[0] );

  qfld1BTraceGroup0.associativity(1).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 10, edgeMap[0] );

  qfld1BTraceGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 11, edgeMap[0] );

  qfld1BTraceGroup1.associativity(1).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL(  5, edgeMap[0] );

  qfld1BTraceGroup2.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL(  6, edgeMap[0] );

  qfld1BTraceGroup2.associativity(1).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL(  9, edgeMap[0] );


  Int3 xedgeSign, qedgeSign;

  xedgeSign = xfldGroup0.associativity(0).edgeSign();
  qedgeSign = qfld1Group0.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( xedgeSign[0], qedgeSign[0] );
  BOOST_CHECK_EQUAL( xedgeSign[1], qedgeSign[1] );
  BOOST_CHECK_EQUAL( xedgeSign[2], qedgeSign[2] );

  xedgeSign = xfldGroup1.associativity(0).edgeSign();
  qedgeSign = qfld1Group1.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( xedgeSign[0], qedgeSign[0] );
  BOOST_CHECK_EQUAL( xedgeSign[1], qedgeSign[1] );
  BOOST_CHECK_EQUAL( xedgeSign[2], qedgeSign[2] );

  xedgeSign = xfldGroup2.associativity(0).edgeSign();
  qedgeSign = qfld1Group2.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( xedgeSign[0], qedgeSign[0] );
  BOOST_CHECK_EQUAL( xedgeSign[1], qedgeSign[1] );
  BOOST_CHECK_EQUAL( xedgeSign[2], qedgeSign[2] );

  xedgeSign = xfldGroup3.associativity(0).edgeSign();
  qedgeSign = qfld1Group3.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( xedgeSign[0], qedgeSign[0] );
  BOOST_CHECK_EQUAL( xedgeSign[1], qedgeSign[1] );
  BOOST_CHECK_EQUAL( xedgeSign[2], qedgeSign[2] );


  // Test the constant assignment operator
  for (int group = 0; group < 3; group++)
  {
    const QFieldAreaClass& qfld1Area = qfld1.getCellGroup<Triangle>(group);
    ElementQFieldClass qfldElem(qfld1Area.basis());

    qfld1 = 1.23;
    ArrayQ q1;

    for (int elem = 0; elem < qfld1Area.nElem(); elem++)
    {
      qfld1Area.getElement(qfldElem, elem);

      qfldElem.eval(0.25, 0.25, q1);
      BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
      BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
    }

    ArrayQ q0 = {4.56, 7.89};
    qfld1 = q0;

    for (int elem = 0; elem < qfld1Area.nElem(); elem++)
    {
      qfld1Area.getElement(qfldElem, elem);

      qfldElem.eval(0.25, 0.25, q1);
      BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
      BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
    }
  }

  QField2D_CG_Area qfld2( {{0,1}, {2,3}}, xfld1, order, BasisFunctionCategory_Hierarchical );

  BOOST_CHECK( qfld2.spaceType() == SpaceType::Continuous );

  BOOST_CHECK_EQUAL( 20, qfld2.nDOF() );
  BOOST_CHECK_EQUAL( 4, qfld2.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 3, qfld2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 4, qfld2.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld2.getXField() );

  const QFieldAreaClass& qfld2Group0 = qfld2.getCellGroup<Triangle>(0);
  const QFieldAreaClass& qfld2Group1 = qfld2.getCellGroup<Triangle>(1);
  const QFieldAreaClass& qfld2Group2 = qfld2.getCellGroup<Triangle>(2);
  const QFieldAreaClass& qfld2Group3 = qfld2.getCellGroup<Triangle>(3);

  BOOST_CHECK_EQUAL( 1, qfld2Group0.nElem() );
  BOOST_CHECK_EQUAL( 1, qfld2Group1.nElem() );
  BOOST_CHECK_EQUAL( 1, qfld2Group2.nElem() );
  BOOST_CHECK_EQUAL( 1, qfld2Group3.nElem() );

  qfld2Group0.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 11, nodeMap[0] );
  BOOST_CHECK_EQUAL( 12, nodeMap[1] );
  BOOST_CHECK_EQUAL( 13, nodeMap[2] );

  qfld2Group1.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 14, nodeMap[0] );
  BOOST_CHECK_EQUAL( 13, nodeMap[1] );
  BOOST_CHECK_EQUAL( 12, nodeMap[2] );

  qfld2Group2.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 17, nodeMap[0] );
  BOOST_CHECK_EQUAL( 18, nodeMap[1] );
  BOOST_CHECK_EQUAL( 15, nodeMap[2] );

  qfld2Group3.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 16, nodeMap[0] );
  BOOST_CHECK_EQUAL( 15, nodeMap[1] );
  BOOST_CHECK_EQUAL( 19, nodeMap[2] );

  qfld2Group0.associativity(0).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL(  2, edgeMap[0] );
  BOOST_CHECK_EQUAL(  1, edgeMap[1] );
  BOOST_CHECK_EQUAL(  0, edgeMap[2] );

  qfld2Group1.associativity(0).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL(  2, edgeMap[0] );
  BOOST_CHECK_EQUAL(  3, edgeMap[1] );
  BOOST_CHECK_EQUAL(  4, edgeMap[2] );

  qfld2Group2.associativity(0).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL(  7, edgeMap[0] );
  BOOST_CHECK_EQUAL(  6, edgeMap[1] );
  BOOST_CHECK_EQUAL( 10, edgeMap[2] );

  qfld2Group3.associativity(0).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL(  8, edgeMap[0] );
  BOOST_CHECK_EQUAL(  9, edgeMap[1] );
  BOOST_CHECK_EQUAL(  5, edgeMap[2] );

  const QFieldTraceClass& qfld2BTraceGroup0 = qfld2.getBoundaryTraceGroup<Line>(0);
  const QFieldTraceClass& qfld2BTraceGroup1 = qfld2.getBoundaryTraceGroup<Line>(1);
  const QFieldTraceClass& qfld2BTraceGroup2 = qfld2.getBoundaryTraceGroup<Line>(2);

  BOOST_CHECK_EQUAL( 2, qfld2BTraceGroup0.nElem() );
  BOOST_CHECK_EQUAL( 2, qfld2BTraceGroup1.nElem() );
  BOOST_CHECK_EQUAL( 2, qfld2BTraceGroup2.nElem() );

  qfld2BTraceGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 12, nodeMap[0] );
  BOOST_CHECK_EQUAL( 14, nodeMap[1] );

  qfld2BTraceGroup0.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 14, nodeMap[0] );
  BOOST_CHECK_EQUAL( 13, nodeMap[1] );

  qfld2BTraceGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 17, nodeMap[0] );
  BOOST_CHECK_EQUAL( 18, nodeMap[1] );

  qfld2BTraceGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 18, nodeMap[0] );
  BOOST_CHECK_EQUAL( 15, nodeMap[1] );

  qfld2BTraceGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 15, nodeMap[0] );
  BOOST_CHECK_EQUAL( 19, nodeMap[1] );

  qfld2BTraceGroup2.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 19, nodeMap[0] );
  BOOST_CHECK_EQUAL( 16, nodeMap[1] );

  qfld2BTraceGroup0.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 3, edgeMap[0] );

  qfld2BTraceGroup0.associativity(1).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 4, edgeMap[0] );

  qfld2BTraceGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 10, edgeMap[0] );

  qfld2BTraceGroup1.associativity(1).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 7, edgeMap[0] );

  qfld2BTraceGroup2.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 8, edgeMap[0] );

  qfld2BTraceGroup2.associativity(1).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 9, edgeMap[0] );

  xedgeSign = xfldGroup0.associativity(0).edgeSign();
  qedgeSign = qfld2Group0.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( xedgeSign[0], qedgeSign[0] );
  BOOST_CHECK_EQUAL( xedgeSign[1], qedgeSign[1] );
  BOOST_CHECK_EQUAL( xedgeSign[2], qedgeSign[2] );

  xedgeSign = xfldGroup1.associativity(0).edgeSign();
  qedgeSign = qfld2Group1.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( xedgeSign[0], qedgeSign[0] );
  BOOST_CHECK_EQUAL( xedgeSign[1], qedgeSign[1] );
  BOOST_CHECK_EQUAL( xedgeSign[2], qedgeSign[2] );

  xedgeSign = xfldGroup2.associativity(0).edgeSign();
  qedgeSign = qfld2Group2.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( xedgeSign[0], qedgeSign[0] );
  BOOST_CHECK_EQUAL( xedgeSign[1], qedgeSign[1] );
  BOOST_CHECK_EQUAL( xedgeSign[2], qedgeSign[2] );

  xedgeSign = xfldGroup3.associativity(0).edgeSign();
  qedgeSign = qfld2Group3.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( xedgeSign[0], qedgeSign[0] );
  BOOST_CHECK_EQUAL( xedgeSign[1], qedgeSign[1] );
  BOOST_CHECK_EQUAL( xedgeSign[2], qedgeSign[2] );

}

//----------------------------------------------------------------------------//
// NOTE: DOF ordering for P2 is interior edges, then boundary edges, then nodes
BOOST_AUTO_TEST_CASE( CG_Area_GhostBoundary_P2 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef QField2D_CG_Area::FieldCellGroupType<Triangle> QFieldAreaClass;
  //typedef QField2D_CG_Area::FieldTraceGroupType<Line> QFieldBoundaryEdgeClass;
//  typedef QFieldAreaClass::ElementType<>::IntNEdge IntNEdge;

  const int nNode = 3, nEdge = 3;
  int nodeMap1[nNode], nodeMap2[nNode];
  int edgeMap1[nEdge], edgeMap2[nEdge];
//  IntNEdge orientation1, orientation2;

  XField2D_2Triangle_X1_1Group xfld1;
  XField2D_2Triangle_GhostBoundary_X1_1Group xfld2;

  int order = 2;
  QField2D_CG_Area qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 4+5, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  QField2D_CG_Area qfld2(xfld2, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 4+5, qfld2.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld2.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld2, &qfld2.getXField() );

  const QFieldAreaClass& qfld1_AreaGroup = qfld1.getCellGroup<Triangle>(0);
  const QFieldAreaClass& qfld2_AreaGroup = qfld2.getCellGroup<Triangle>(0);

  for (int elem = 0; elem < qfld1_AreaGroup.nElem(); elem++)
  {
    qfld1_AreaGroup.associativity(elem).getNodeGlobalMapping( nodeMap1, nNode );
    qfld2_AreaGroup.associativity(elem).getNodeGlobalMapping( nodeMap2, nNode );

    qfld1_AreaGroup.associativity(elem).getEdgeGlobalMapping( edgeMap1, nEdge );
    qfld2_AreaGroup.associativity(elem).getEdgeGlobalMapping( edgeMap2, nEdge );

//    orientation1 = qfld1_AreaGroup.associativity(elem).traceOrientations( );
//    orientation2 = qfld2_AreaGroup.associativity(elem).traceOrientations( );

    for (int i = 0; i < nNode; i++)
      BOOST_CHECK_EQUAL( nodeMap1[i], nodeMap2[i] );

    for (int i = 0; i < nEdge; i++)
      BOOST_CHECK_EQUAL( edgeMap1[i], edgeMap2[i] );

//    for (int i = 0; i < orientation1.size(); i++)
//      BOOST_CHECK_EQUAL( orientation1[i], orientation2[i] );
  }
}

//----------------------------------------------------------------------------//
// NOTE: DOF ordering for P3 is cells, interior edges, then boundary edges, then nodes
BOOST_AUTO_TEST_CASE( CG_Area_P3 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef QField2D_CG_Area::FieldCellGroupType<Triangle> QFieldAreaClass;
  typedef QField2D_CG_Area::FieldTraceGroupType<Triangle> QFieldTraceClass;
  typedef QFieldAreaClass::ElementType<> ElementQFieldClass;

  int nodeMap[3];
  int edgeMap[6];
  int cellMap[1];

  XField2D_2Triangle_X1_1Group xfld1;

  int order = 3;
  QField2D_CG_Area qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2 + 2*5 + 4, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldAreaClass& qfldAreaGroup1 = qfld1.getCellGroup<Triangle>(0);
  //cout << "btest: qfldAreaGroup1 =" << endl; qfldAreaGroup1.dump(2);

  qfldAreaGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 12, nodeMap[0] );
  BOOST_CHECK_EQUAL( 13, nodeMap[1] );
  BOOST_CHECK_EQUAL( 14, nodeMap[2] );

  qfldAreaGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 15, nodeMap[0] );
  BOOST_CHECK_EQUAL( 14, nodeMap[1] );
  BOOST_CHECK_EQUAL( 13, nodeMap[2] );

  qfldAreaGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 6 );
  BOOST_CHECK_EQUAL(  6, edgeMap[0] );
  BOOST_CHECK_EQUAL(  7, edgeMap[1] );
  BOOST_CHECK_EQUAL(  4, edgeMap[2] );
  BOOST_CHECK_EQUAL(  5, edgeMap[3] );
  BOOST_CHECK_EQUAL(  2, edgeMap[4] );
  BOOST_CHECK_EQUAL(  3, edgeMap[5] );

  qfldAreaGroup1.associativity(1).getEdgeGlobalMapping( edgeMap, 6 );
  BOOST_CHECK_EQUAL(  6, edgeMap[0] );
  BOOST_CHECK_EQUAL(  7, edgeMap[1] );
  BOOST_CHECK_EQUAL(  8, edgeMap[2] );
  BOOST_CHECK_EQUAL(  9, edgeMap[3] );
  BOOST_CHECK_EQUAL( 10, edgeMap[4] );
  BOOST_CHECK_EQUAL( 11, edgeMap[5] );

  qfldAreaGroup1.associativity(0).getCellGlobalMapping( cellMap, 1 );
  BOOST_CHECK_EQUAL( 0, cellMap[0] );

  qfldAreaGroup1.associativity(1).getCellGlobalMapping( cellMap, 1 );
  BOOST_CHECK_EQUAL( 1, cellMap[0] );

#if 0
  // TODO: I don't think we want trace groups in a cell field
  const QFieldLineClass& qfldInteriorGroup1 = qfld1.getInteriorTraceGroup<Line>(0);

  qfldInteriorGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 13, nodeMap[0] );
  BOOST_CHECK_EQUAL( 14, nodeMap[1] );

  qfldInteriorGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 2, edgeMap[0] );
  BOOST_CHECK_EQUAL( 3, edgeMap[1] );
#endif

  const QFieldTraceClass& qfldBoundaryGroup1 = qfld1.getBoundaryTraceGroup<Line>(0);

  qfldBoundaryGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 12, nodeMap[0] );
  BOOST_CHECK_EQUAL( 13, nodeMap[1] );

  qfldBoundaryGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 13, nodeMap[0] );
  BOOST_CHECK_EQUAL( 15, nodeMap[1] );

  qfldBoundaryGroup1.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 15, nodeMap[0] );
  BOOST_CHECK_EQUAL( 14, nodeMap[1] );

  qfldBoundaryGroup1.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 14, nodeMap[0] );
  BOOST_CHECK_EQUAL( 12, nodeMap[1] );

  qfldBoundaryGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL(  2, edgeMap[0] );
  BOOST_CHECK_EQUAL(  3, edgeMap[1] );

  qfldBoundaryGroup1.associativity(1).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL(  8, edgeMap[0] );
  BOOST_CHECK_EQUAL(  9, edgeMap[1] );

  qfldBoundaryGroup1.associativity(2).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 10, edgeMap[0] );
  BOOST_CHECK_EQUAL( 11, edgeMap[1] );

  qfldBoundaryGroup1.associativity(3).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL(  4, edgeMap[0] );
  BOOST_CHECK_EQUAL(  5, edgeMap[1] );

  XField2D_4Triangle_X1_1Group xfld2;

  QField2D_CG_Area qfld2(xfld2, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 4 + 2*3 + 6 + 2*6, qfld2.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld2.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld2, &qfld2.getXField() );

  QField2D_CG_Area qfld3(qfld2, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld2.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld2.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nCellGroups()         ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld2.getXField()           , &qfld3.getXField() );

  const QFieldAreaClass& qfldAreaGroup2 = qfld2.getCellGroup<Triangle>(0);
  //cout << "btest: qfldAreaGroup2 =" << endl; qfldAreaGroup2.dump(2);

  qfldAreaGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 22, nodeMap[0] );
  BOOST_CHECK_EQUAL( 23, nodeMap[1] );
  BOOST_CHECK_EQUAL( 24, nodeMap[2] );

  qfldAreaGroup2.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 25, nodeMap[0] );
  BOOST_CHECK_EQUAL( 24, nodeMap[1] );
  BOOST_CHECK_EQUAL( 23, nodeMap[2] );

  qfldAreaGroup2.associativity(2).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 24, nodeMap[0] );
  BOOST_CHECK_EQUAL( 26, nodeMap[1] );
  BOOST_CHECK_EQUAL( 22, nodeMap[2] );

  qfldAreaGroup2.associativity(3).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 23, nodeMap[0] );
  BOOST_CHECK_EQUAL( 22, nodeMap[1] );
  BOOST_CHECK_EQUAL( 27, nodeMap[2] );

  qfldAreaGroup2.associativity(0).getEdgeGlobalMapping( edgeMap, 6 );
  BOOST_CHECK_EQUAL( 12, edgeMap[0] );
  BOOST_CHECK_EQUAL( 13, edgeMap[1] );
  BOOST_CHECK_EQUAL(  6, edgeMap[2] );
  BOOST_CHECK_EQUAL(  7, edgeMap[3] );
  BOOST_CHECK_EQUAL(  4, edgeMap[4] );
  BOOST_CHECK_EQUAL(  5, edgeMap[5] );

  qfldAreaGroup2.associativity(1).getEdgeGlobalMapping( edgeMap, 6 );
  BOOST_CHECK_EQUAL( 12, edgeMap[0] );
  BOOST_CHECK_EQUAL( 13, edgeMap[1] );
  BOOST_CHECK_EQUAL( 14, edgeMap[2] );
  BOOST_CHECK_EQUAL( 15, edgeMap[3] );
  BOOST_CHECK_EQUAL( 18, edgeMap[4] );
  BOOST_CHECK_EQUAL( 19, edgeMap[5] );

  qfldAreaGroup2.associativity(2).getEdgeGlobalMapping( edgeMap, 6 );
  BOOST_CHECK_EQUAL(  8, edgeMap[0] );
  BOOST_CHECK_EQUAL(  9, edgeMap[1] );
  BOOST_CHECK_EQUAL(  6, edgeMap[2] );
  BOOST_CHECK_EQUAL(  7, edgeMap[3] );
  BOOST_CHECK_EQUAL( 20, edgeMap[4] );
  BOOST_CHECK_EQUAL( 21, edgeMap[5] );

  qfldAreaGroup2.associativity(3).getEdgeGlobalMapping( edgeMap, 6 );
  BOOST_CHECK_EQUAL( 10, edgeMap[0] );
  BOOST_CHECK_EQUAL( 11, edgeMap[1] );
  BOOST_CHECK_EQUAL( 16, edgeMap[2] );
  BOOST_CHECK_EQUAL( 17, edgeMap[3] );
  BOOST_CHECK_EQUAL(  4, edgeMap[4] );
  BOOST_CHECK_EQUAL(  5, edgeMap[5] );

  qfldAreaGroup2.associativity(0).getCellGlobalMapping( cellMap, 1 );
  BOOST_CHECK_EQUAL( 0, cellMap[0] );

  qfldAreaGroup2.associativity(1).getCellGlobalMapping( cellMap, 1 );
  BOOST_CHECK_EQUAL( 1, cellMap[0] );

  qfldAreaGroup2.associativity(2).getCellGlobalMapping( cellMap, 1 );
  BOOST_CHECK_EQUAL( 2, cellMap[0] );

  qfldAreaGroup2.associativity(3).getCellGlobalMapping( cellMap, 1 );
  BOOST_CHECK_EQUAL( 3, cellMap[0] );

#if 0
  // TODO: I don't think we want trace groups in a cell field
  const QFieldLineClass& qfldInteriorGroup2 = qfld2.getInteriorTraceGroup<Line>(0);

  qfldInteriorGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 23, nodeMap[0] );
  BOOST_CHECK_EQUAL( 24, nodeMap[1] );

  qfldInteriorGroup2.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 24, nodeMap[0] );
  BOOST_CHECK_EQUAL( 22, nodeMap[1] );

  qfldInteriorGroup2.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 22, nodeMap[0] );
  BOOST_CHECK_EQUAL( 23, nodeMap[1] );

  qfldInteriorGroup2.associativity(0).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 4, edgeMap[0] );
  BOOST_CHECK_EQUAL( 5, edgeMap[1] );

  qfldInteriorGroup2.associativity(1).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 6, edgeMap[0] );
  BOOST_CHECK_EQUAL( 7, edgeMap[1] );

  qfldInteriorGroup2.associativity(2).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 8, edgeMap[0] );
  BOOST_CHECK_EQUAL( 9, edgeMap[1] );
#endif

  const QFieldTraceClass& qfldBoundaryGroup2 = qfld2.getBoundaryTraceGroup<Line>(0);

  qfldBoundaryGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 22, nodeMap[0] );
  BOOST_CHECK_EQUAL( 27, nodeMap[1] );

  qfldBoundaryGroup2.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 27, nodeMap[0] );
  BOOST_CHECK_EQUAL( 23, nodeMap[1] );

  qfldBoundaryGroup2.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 23, nodeMap[0] );
  BOOST_CHECK_EQUAL( 25, nodeMap[1] );

  qfldBoundaryGroup2.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 25, nodeMap[0] );
  BOOST_CHECK_EQUAL( 24, nodeMap[1] );

  qfldBoundaryGroup2.associativity(4).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 24, nodeMap[0] );
  BOOST_CHECK_EQUAL( 26, nodeMap[1] );

  qfldBoundaryGroup2.associativity(5).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 26, nodeMap[0] );
  BOOST_CHECK_EQUAL( 22, nodeMap[1] );

  qfldBoundaryGroup2.associativity(0).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 10, edgeMap[0] );
  BOOST_CHECK_EQUAL( 11, edgeMap[1] );

  qfldBoundaryGroup2.associativity(1).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 16, edgeMap[0] );
  BOOST_CHECK_EQUAL( 17, edgeMap[1] );

  qfldBoundaryGroup2.associativity(2).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 14, edgeMap[0] );
  BOOST_CHECK_EQUAL( 15, edgeMap[1] );

  qfldBoundaryGroup2.associativity(3).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 18, edgeMap[0] );
  BOOST_CHECK_EQUAL( 19, edgeMap[1] );

  qfldBoundaryGroup2.associativity(4).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 20, edgeMap[0] );
  BOOST_CHECK_EQUAL( 21, edgeMap[1] );

  qfldBoundaryGroup2.associativity(5).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL(  8, edgeMap[0] );
  BOOST_CHECK_EQUAL(  9, edgeMap[1] );

  // Test the constant assignment operator
  const QFieldAreaClass& qfld1Area = qfld1.getCellGroup<Triangle>(0);
  ElementQFieldClass qfldElem(qfld1Area.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfld1Area.nElem(); elem++)
  {
    qfld1Area.getElement(qfldElem, elem);

    qfldElem.eval(0.25, 0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfld1Area.nElem(); elem++)
  {
    qfld1Area.getElement(qfldElem, elem);

    qfldElem.eval(0.25, 0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 5e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 5e-12);
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_Area_P3_4Group )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef XField2D_4Triangle_X1_4Group::FieldCellGroupType<Triangle> XFieldAreaClass;
  typedef QField2D_CG_Area::FieldCellGroupType<Triangle> QFieldAreaClass;
  typedef QField2D_CG_Area::FieldTraceGroupType<Line> QFieldTraceClass;
  typedef QFieldAreaClass::ElementType<> ElementQFieldClass;
  typedef std::array<int,3> Int3;

  int nodeMap[3];
  int edgeMap[6];
  int cellMap[1];

  XField2D_4Triangle_X1_4Group xfld1;

  const XFieldAreaClass& xfldGroup0 = xfld1.getCellGroup<Triangle>(0);
  const XFieldAreaClass& xfldGroup1 = xfld1.getCellGroup<Triangle>(1);
  const XFieldAreaClass& xfldGroup2 = xfld1.getCellGroup<Triangle>(2);
  const XFieldAreaClass& xfldGroup3 = xfld1.getCellGroup<Triangle>(3);

  int order = 3;

  QField2D_CG_Area qfld1( {{0}, {1,2,3}}, xfld1, order, BasisFunctionCategory_Hierarchical );

  BOOST_CHECK( qfld1.spaceType() == SpaceType::Continuous );

  BOOST_CHECK_EQUAL( 37, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 4, qfld1.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 3, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 4, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldAreaClass& qfld1Group0 = qfld1.getCellGroup<Triangle>(0);
  const QFieldAreaClass& qfld1Group1 = qfld1.getCellGroup<Triangle>(1);
  const QFieldAreaClass& qfld1Group2 = qfld1.getCellGroup<Triangle>(2);
  const QFieldAreaClass& qfld1Group3 = qfld1.getCellGroup<Triangle>(3);

  BOOST_CHECK_EQUAL( 1, qfld1Group0.nElem() );
  BOOST_CHECK_EQUAL( 1, qfld1Group1.nElem() );
  BOOST_CHECK_EQUAL( 1, qfld1Group2.nElem() );
  BOOST_CHECK_EQUAL( 1, qfld1Group3.nElem() );

  qfld1Group0.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 28, nodeMap[0] );
  BOOST_CHECK_EQUAL( 29, nodeMap[1] );
  BOOST_CHECK_EQUAL( 30, nodeMap[2] );

  qfld1Group1.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 34, nodeMap[0] );
  BOOST_CHECK_EQUAL( 33, nodeMap[1] );
  BOOST_CHECK_EQUAL( 32, nodeMap[2] );

  qfld1Group2.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 33, nodeMap[0] );
  BOOST_CHECK_EQUAL( 35, nodeMap[1] );
  BOOST_CHECK_EQUAL( 31, nodeMap[2] );

  qfld1Group3.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 32, nodeMap[0] );
  BOOST_CHECK_EQUAL( 31, nodeMap[1] );
  BOOST_CHECK_EQUAL( 36, nodeMap[2] );

  qfld1Group0.associativity(0).getEdgeGlobalMapping( edgeMap, 6 );
  BOOST_CHECK_EQUAL(  8, edgeMap[0] );
  BOOST_CHECK_EQUAL(  9, edgeMap[1] );
  BOOST_CHECK_EQUAL(  6, edgeMap[2] );
  BOOST_CHECK_EQUAL(  7, edgeMap[3] );
  BOOST_CHECK_EQUAL(  4, edgeMap[4] );
  BOOST_CHECK_EQUAL(  5, edgeMap[5] );

  qfld1Group1.associativity(0).getEdgeGlobalMapping( edgeMap, 6 );
  BOOST_CHECK_EQUAL( 18, edgeMap[0] );
  BOOST_CHECK_EQUAL( 19, edgeMap[1] );
  BOOST_CHECK_EQUAL( 20, edgeMap[2] );
  BOOST_CHECK_EQUAL( 21, edgeMap[3] );
  BOOST_CHECK_EQUAL( 24, edgeMap[4] );
  BOOST_CHECK_EQUAL( 25, edgeMap[5] );

  qfld1Group2.associativity(0).getEdgeGlobalMapping( edgeMap, 6 );
  BOOST_CHECK_EQUAL( 14, edgeMap[0] );
  BOOST_CHECK_EQUAL( 15, edgeMap[1] );
  BOOST_CHECK_EQUAL( 12, edgeMap[2] );
  BOOST_CHECK_EQUAL( 13, edgeMap[3] );
  BOOST_CHECK_EQUAL( 26, edgeMap[4] );
  BOOST_CHECK_EQUAL( 27, edgeMap[5] );

  qfld1Group3.associativity(0).getEdgeGlobalMapping( edgeMap, 6 );
  BOOST_CHECK_EQUAL( 16, edgeMap[0] );
  BOOST_CHECK_EQUAL( 17, edgeMap[1] );
  BOOST_CHECK_EQUAL( 22, edgeMap[2] );
  BOOST_CHECK_EQUAL( 23, edgeMap[3] );
  BOOST_CHECK_EQUAL( 10, edgeMap[4] );
  BOOST_CHECK_EQUAL( 11, edgeMap[5] );

  qfld1Group0.associativity(0).getCellGlobalMapping( cellMap, 1 );
  BOOST_CHECK_EQUAL( 0, cellMap[0] );

  qfld1Group1.associativity(0).getCellGlobalMapping( cellMap, 1 );
  BOOST_CHECK_EQUAL( 1, cellMap[0] );

  qfld1Group2.associativity(0).getCellGlobalMapping( cellMap, 1 );
  BOOST_CHECK_EQUAL( 2, cellMap[0] );

  qfld1Group3.associativity(0).getCellGlobalMapping( cellMap, 1 );
  BOOST_CHECK_EQUAL( 3, cellMap[0] );

  const QFieldTraceClass& qfld1BoundaryGroup0 = qfld1.getBoundaryTraceGroup<Line>(0);
  const QFieldTraceClass& qfld1BoundaryGroup1 = qfld1.getBoundaryTraceGroup<Line>(1);
  const QFieldTraceClass& qfld1BoundaryGroup2 = qfld1.getBoundaryTraceGroup<Line>(2);

  qfld1BoundaryGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 32, nodeMap[0] );
  BOOST_CHECK_EQUAL( 34, nodeMap[1] );

  qfld1BoundaryGroup0.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 34, nodeMap[0] );
  BOOST_CHECK_EQUAL( 33, nodeMap[1] );

  qfld1BoundaryGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 33, nodeMap[0] );
  BOOST_CHECK_EQUAL( 35, nodeMap[1] );

  qfld1BoundaryGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 35, nodeMap[0] );
  BOOST_CHECK_EQUAL( 31, nodeMap[1] );

  qfld1BoundaryGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 31, nodeMap[0] );
  BOOST_CHECK_EQUAL( 36, nodeMap[1] );

  qfld1BoundaryGroup2.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 36, nodeMap[0] );
  BOOST_CHECK_EQUAL( 32, nodeMap[1] );

  qfld1BoundaryGroup0.associativity(0).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 20, edgeMap[0] );
  BOOST_CHECK_EQUAL( 21, edgeMap[1] );

  qfld1BoundaryGroup0.associativity(1).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 24, edgeMap[0] );
  BOOST_CHECK_EQUAL( 25, edgeMap[1] );

  qfld1BoundaryGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 26, edgeMap[0] );
  BOOST_CHECK_EQUAL( 27, edgeMap[1] );

  qfld1BoundaryGroup1.associativity(1).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 14, edgeMap[0] );
  BOOST_CHECK_EQUAL( 15, edgeMap[1] );

  qfld1BoundaryGroup2.associativity(0).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 16, edgeMap[0] );
  BOOST_CHECK_EQUAL( 17, edgeMap[1] );

  qfld1BoundaryGroup2.associativity(1).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 22, edgeMap[0] );
  BOOST_CHECK_EQUAL( 23, edgeMap[1] );

  Int3 xedgeSign, qedgeSign;

  xedgeSign = xfldGroup0.associativity(0).edgeSign();
  qedgeSign = qfld1Group0.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( xedgeSign[0], qedgeSign[0] );
  BOOST_CHECK_EQUAL( xedgeSign[1], qedgeSign[1] );
  BOOST_CHECK_EQUAL( xedgeSign[2], qedgeSign[2] );

  xedgeSign = xfldGroup1.associativity(0).edgeSign();
  qedgeSign = qfld1Group1.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( xedgeSign[0], qedgeSign[0] );
  BOOST_CHECK_EQUAL( xedgeSign[1], qedgeSign[1] );
  BOOST_CHECK_EQUAL( xedgeSign[2], qedgeSign[2] );

  xedgeSign = xfldGroup2.associativity(0).edgeSign();
  qedgeSign = qfld1Group2.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( xedgeSign[0], qedgeSign[0] );
  BOOST_CHECK_EQUAL( xedgeSign[1], qedgeSign[1] );
  BOOST_CHECK_EQUAL( xedgeSign[2], qedgeSign[2] );

  xedgeSign = xfldGroup3.associativity(0).edgeSign();
  qedgeSign = qfld1Group3.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( xedgeSign[0], qedgeSign[0] );
  BOOST_CHECK_EQUAL( xedgeSign[1], qedgeSign[1] );
  BOOST_CHECK_EQUAL( xedgeSign[2], qedgeSign[2] );


  // Test the constant assignment operator
  for (int group = 0; group < 3; group++)
  {
    const QFieldAreaClass& qfld1Area = qfld1.getCellGroup<Triangle>(group);
    ElementQFieldClass qfldElem(qfld1Area.basis());

    qfld1 = 1.23;
    ArrayQ q1;

    for (int elem = 0; elem < qfld1Area.nElem(); elem++)
    {
      qfld1Area.getElement(qfldElem, elem);

      qfldElem.eval(0.25, 0.25, q1);
      BOOST_CHECK_CLOSE(1.23, q1[0], 1e-11);
      BOOST_CHECK_CLOSE(1.23, q1[1], 1e-11);
    }

    ArrayQ q0 = {4.56, 7.89};
    qfld1 = q0;

    for (int elem = 0; elem < qfld1Area.nElem(); elem++)
    {
      qfld1Area.getElement(qfldElem, elem);

      qfldElem.eval(0.25, 0.25, q1);
      BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-11);
      BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-11);
    }
  }

  QField2D_CG_Area qfld2( {{0,1}, {2,3}}, xfld1, order, BasisFunctionCategory_Hierarchical );

  BOOST_CHECK( qfld2.spaceType() == SpaceType::Continuous );

  BOOST_CHECK_EQUAL( 35, qfld2.nDOF() );
  BOOST_CHECK_EQUAL( 4, qfld2.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 3, qfld2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 4, qfld2.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld2.getXField() );

  const QFieldAreaClass& qfld2Group0 = qfld2.getCellGroup<Triangle>(0);
  const QFieldAreaClass& qfld2Group1 = qfld2.getCellGroup<Triangle>(1);
  const QFieldAreaClass& qfld2Group2 = qfld2.getCellGroup<Triangle>(2);
  const QFieldAreaClass& qfld2Group3 = qfld2.getCellGroup<Triangle>(3);

  BOOST_CHECK_EQUAL( 1, qfld2Group0.nElem() );
  BOOST_CHECK_EQUAL( 1, qfld2Group1.nElem() );
  BOOST_CHECK_EQUAL( 1, qfld2Group2.nElem() );
  BOOST_CHECK_EQUAL( 1, qfld2Group3.nElem() );

  qfld2Group0.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 26, nodeMap[0] );
  BOOST_CHECK_EQUAL( 27, nodeMap[1] );
  BOOST_CHECK_EQUAL( 28, nodeMap[2] );

  qfld2Group1.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 29, nodeMap[0] );
  BOOST_CHECK_EQUAL( 28, nodeMap[1] );
  BOOST_CHECK_EQUAL( 27, nodeMap[2] );

  qfld2Group2.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 32, nodeMap[0] );
  BOOST_CHECK_EQUAL( 33, nodeMap[1] );
  BOOST_CHECK_EQUAL( 30, nodeMap[2] );

  qfld2Group3.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( 31, nodeMap[0] );
  BOOST_CHECK_EQUAL( 30, nodeMap[1] );
  BOOST_CHECK_EQUAL( 34, nodeMap[2] );

  qfld2Group0.associativity(0).getEdgeGlobalMapping( edgeMap, 6 );
  BOOST_CHECK_EQUAL(  8, edgeMap[0] );
  BOOST_CHECK_EQUAL(  9, edgeMap[1] );
  BOOST_CHECK_EQUAL(  6, edgeMap[2] );
  BOOST_CHECK_EQUAL(  7, edgeMap[3] );
  BOOST_CHECK_EQUAL(  4, edgeMap[4] );
  BOOST_CHECK_EQUAL(  5, edgeMap[5] );

  qfld2Group1.associativity(0).getEdgeGlobalMapping( edgeMap, 6 );
  BOOST_CHECK_EQUAL(  8, edgeMap[0] );
  BOOST_CHECK_EQUAL(  9, edgeMap[1] );
  BOOST_CHECK_EQUAL( 10, edgeMap[2] );
  BOOST_CHECK_EQUAL( 11, edgeMap[3] );
  BOOST_CHECK_EQUAL( 12, edgeMap[4] );
  BOOST_CHECK_EQUAL( 13, edgeMap[5] );

  qfld2Group2.associativity(0).getEdgeGlobalMapping( edgeMap, 6 );
  BOOST_CHECK_EQUAL( 18, edgeMap[0] );
  BOOST_CHECK_EQUAL( 19, edgeMap[1] );
  BOOST_CHECK_EQUAL( 16, edgeMap[2] );
  BOOST_CHECK_EQUAL( 17, edgeMap[3] );
  BOOST_CHECK_EQUAL( 24, edgeMap[4] );
  BOOST_CHECK_EQUAL( 25, edgeMap[5] );

  qfld2Group3.associativity(0).getEdgeGlobalMapping( edgeMap, 6 );
  BOOST_CHECK_EQUAL( 20, edgeMap[0] );
  BOOST_CHECK_EQUAL( 21, edgeMap[1] );
  BOOST_CHECK_EQUAL( 22, edgeMap[2] );
  BOOST_CHECK_EQUAL( 23, edgeMap[3] );
  BOOST_CHECK_EQUAL( 14, edgeMap[4] );
  BOOST_CHECK_EQUAL( 15, edgeMap[5] );

  qfld2Group0.associativity(0).getCellGlobalMapping( cellMap, 1 );
  BOOST_CHECK_EQUAL( 0, cellMap[0] );

  qfld2Group1.associativity(0).getCellGlobalMapping( cellMap, 1 );
  BOOST_CHECK_EQUAL( 1, cellMap[0] );

  qfld2Group2.associativity(0).getCellGlobalMapping( cellMap, 1 );
  BOOST_CHECK_EQUAL( 2, cellMap[0] );

  qfld2Group3.associativity(0).getCellGlobalMapping( cellMap, 1 );
  BOOST_CHECK_EQUAL( 3, cellMap[0] );

  const QFieldTraceClass& qfld2BoundaryGroup0 = qfld2.getBoundaryTraceGroup<Line>(0);
  const QFieldTraceClass& qfld2BoundaryGroup1 = qfld2.getBoundaryTraceGroup<Line>(1);
  const QFieldTraceClass& qfld2BoundaryGroup2 = qfld2.getBoundaryTraceGroup<Line>(2);

  qfld2BoundaryGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 27, nodeMap[0] );
  BOOST_CHECK_EQUAL( 29, nodeMap[1] );

  qfld2BoundaryGroup0.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 29, nodeMap[0] );
  BOOST_CHECK_EQUAL( 28, nodeMap[1] );

  qfld2BoundaryGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 32, nodeMap[0] );
  BOOST_CHECK_EQUAL( 33, nodeMap[1] );

  qfld2BoundaryGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 33, nodeMap[0] );
  BOOST_CHECK_EQUAL( 30, nodeMap[1] );

  qfld2BoundaryGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 30, nodeMap[0] );
  BOOST_CHECK_EQUAL( 34, nodeMap[1] );

  qfld2BoundaryGroup2.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 34, nodeMap[0] );
  BOOST_CHECK_EQUAL( 31, nodeMap[1] );

  qfld2BoundaryGroup0.associativity(0).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 10, edgeMap[0] );
  BOOST_CHECK_EQUAL( 11, edgeMap[1] );

  qfld2BoundaryGroup0.associativity(1).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 12, edgeMap[0] );
  BOOST_CHECK_EQUAL( 13, edgeMap[1] );

  qfld2BoundaryGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 24, edgeMap[0] );
  BOOST_CHECK_EQUAL( 25, edgeMap[1] );

  qfld2BoundaryGroup1.associativity(1).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 18, edgeMap[0] );
  BOOST_CHECK_EQUAL( 19, edgeMap[1] );

  qfld2BoundaryGroup2.associativity(0).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 20, edgeMap[0] );
  BOOST_CHECK_EQUAL( 21, edgeMap[1] );

  qfld2BoundaryGroup2.associativity(1).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 22, edgeMap[0] );
  BOOST_CHECK_EQUAL( 23, edgeMap[1] );


  xedgeSign = xfldGroup0.associativity(0).edgeSign();
  qedgeSign = qfld2Group0.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( xedgeSign[0], qedgeSign[0] );
  BOOST_CHECK_EQUAL( xedgeSign[1], qedgeSign[1] );
  BOOST_CHECK_EQUAL( xedgeSign[2], qedgeSign[2] );

  xedgeSign = xfldGroup1.associativity(0).edgeSign();
  qedgeSign = qfld2Group1.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( xedgeSign[0], qedgeSign[0] );
  BOOST_CHECK_EQUAL( xedgeSign[1], qedgeSign[1] );
  BOOST_CHECK_EQUAL( xedgeSign[2], qedgeSign[2] );

  xedgeSign = xfldGroup2.associativity(0).edgeSign();
  qedgeSign = qfld2Group2.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( xedgeSign[0], qedgeSign[0] );
  BOOST_CHECK_EQUAL( xedgeSign[1], qedgeSign[1] );
  BOOST_CHECK_EQUAL( xedgeSign[2], qedgeSign[2] );

  xedgeSign = xfldGroup3.associativity(0).edgeSign();
  qedgeSign = qfld2Group3.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( xedgeSign[0], qedgeSign[0] );
  BOOST_CHECK_EQUAL( xedgeSign[1], qedgeSign[1] );
  BOOST_CHECK_EQUAL( xedgeSign[2], qedgeSign[2] );

}

//----------------------------------------------------------------------------//
// NOTE: DOF ordering for P2 is interior edges, then boundary edges, then nodes
BOOST_AUTO_TEST_CASE( CG_Area_GhostBoundary_P3 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef QField2D_CG_Area::FieldCellGroupType<Triangle> QFieldAreaClass;
  //typedef QField2D_CG_Area::FieldTraceGroupType<Line> QFieldInteriorEdgeClass;
  //typedef QField2D_CG_Area::FieldTraceGroupType<Line> QFieldBoundaryEdgeClass;
//  typedef QFieldAreaClass::ElementType<>::IntNEdge IntNEdge;

  const int nNode = 3, nEdge = 6, nCell = 1;
  int nodeMap1[nNode], nodeMap2[nNode];
  int edgeMap1[nEdge], edgeMap2[nEdge];
  int cellMap1[nCell], cellMap2[nCell];
//  IntNEdge orientation1, orientation2;

  XField2D_2Triangle_X1_1Group xfld1;
  XField2D_2Triangle_GhostBoundary_X1_1Group xfld2;

  int order = 3;
  QField2D_CG_Area qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 4*4, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nBoundaryTraceGroups() );
//  BOOST_CHECK_EQUAL( 0, qfld1.nGhostBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  QField2D_CG_Area qfld2(xfld2, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 4*4, qfld2.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld2.nBoundaryTraceGroups() ); // This makes me a bit unhappy
//  BOOST_CHECK_EQUAL( 0, qfld2.nGhostBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld2.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld2, &qfld2.getXField() );

  const QFieldAreaClass& qfld1_AreaGroup = qfld1.getCellGroup<Triangle>(0);
  const QFieldAreaClass& qfld2_AreaGroup = qfld2.getCellGroup<Triangle>(0);

  for (int elem = 0; elem < qfld1_AreaGroup.nElem(); elem++)
  {
    qfld1_AreaGroup.associativity(elem).getNodeGlobalMapping( nodeMap1, nNode );
    qfld2_AreaGroup.associativity(elem).getNodeGlobalMapping( nodeMap2, nNode );

    qfld1_AreaGroup.associativity(elem).getEdgeGlobalMapping( edgeMap1, nEdge );
    qfld2_AreaGroup.associativity(elem).getEdgeGlobalMapping( edgeMap2, nEdge );

    qfld1_AreaGroup.associativity(elem).getCellGlobalMapping( cellMap1, nCell );
    qfld2_AreaGroup.associativity(elem).getCellGlobalMapping( cellMap2, nCell );

//    orientation1 = qfld1_AreaGroup.associativity(elem).traceOrientations( );
//    orientation2 = qfld2_AreaGroup.associativity(elem).traceOrientations( );

    for (int i = 0; i < nNode; i++)
      BOOST_CHECK_EQUAL( nodeMap1[i], nodeMap2[i] );

    for (int i = 0; i < nEdge; i++)
      BOOST_CHECK_EQUAL( edgeMap1[i], edgeMap2[i] );

    for (int i = 0; i < nCell; i++)
      BOOST_CHECK_EQUAL( cellMap1[i], cellMap2[i] );

//    for (int i = 0; i < orientation1.size(); i++)
//      BOOST_CHECK_EQUAL( orientation1[i], orientation2[i] );
  }
}


//----------------------------------------------------------------------------//
// check that solution trace is identical for adjacent elements
void CG_Area_CheckTrace(const int order, const BasisFunctionCategory category)
{
  typedef DLA::VectorS<2, Real> ArrayQ;

  typedef XField2D_2Triangle_X1_1Group::FieldCellGroupType<Triangle> XFieldAreaClass;
  typedef XField2D_2Triangle_X1_1Group::FieldTraceGroupType<Line> XFieldLineClass;

  typedef XFieldAreaClass::ElementType<> ElementXFieldAreaClass;
  typedef XFieldLineClass::ElementType<> ElementXFieldLineClass;

  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef QField2D_CG_Area::FieldCellGroupType<Triangle> QFieldAreaClass;
  typedef QFieldAreaClass::ElementType<> ElementQFieldAreaClass;


  const Real small_tol = 5e-12;
  const Real close_tol = 5e-10;

  XField2D_2Triangle_X1_1Group xfld;

  const XFieldAreaClass& xfldArea = xfld.getCellGroup<Triangle>(0);
  const XFieldLineClass& xfldEdge = xfld.getInteriorTraceGroup<Line>(0);

  QField2D_CG_Area qfld(xfld, order, category);

  const QFieldAreaClass& qfldArea = qfld.getCellGroup<Triangle>(0);

  // initialize solution DOFs
  for (int k = 0; k < qfld.nDOF(); k++)
    qfld.DOF(k) = pow(-1, k) / sqrt(k+1);

  // element field variables
  ElementXFieldLineClass xfldElemEdge( xfldEdge.basis() );
  ElementXFieldAreaClass xfldElemL( xfldArea.basis() );
  ElementXFieldAreaClass xfldElemR( xfldArea.basis() );
  ElementQFieldAreaClass qfldElemL( qfldArea.basis() );
  ElementQFieldAreaClass qfldElemR( qfldArea.basis() );

  int nedge = xfldEdge.nElem();
  for (int edge = 0; edge < nedge; edge++)
  {
    int elemL = xfldEdge.getElementLeft( edge );
    int elemR = xfldEdge.getElementRight( edge );
    CanonicalTraceToCell canonicalEdgeL = xfldEdge.getCanonicalTraceLeft( edge );
    CanonicalTraceToCell canonicalEdgeR = xfldEdge.getCanonicalTraceRight( edge );

    // copy global grid/solution DOFs to element
    xfldEdge.getElement( xfldElemEdge, edge );
    xfldArea.getElement( xfldElemL, elemL );
    xfldArea.getElement( xfldElemR, elemR );
    qfldArea.getElement( qfldElemL, elemL );
    qfldArea.getElement( qfldElemR, elemR );

    int kmax = 4;
    for (int k = 0; k < kmax; k++)
    {
      Real sRefL, tRefL, sRefR, tRefR;
      Real sRef;
      ArrayQ qL, qR;

      sRef = k/static_cast<Real>(kmax-1);

      // left/right reference-element coords
      TraceToCellRefCoord<Line, TopoD2, Triangle>::eval( canonicalEdgeL, sRef, sRefL, tRefL );
      TraceToCellRefCoord<Line, TopoD2, Triangle>::eval( canonicalEdgeR, sRef, sRefR, tRefR );

      // solution trace from L/R elements
      qfldElemL.eval( sRefL, tRefL, qL );
      qfldElemR.eval( sRefR, tRefR, qR );

      SANS_CHECK_CLOSE( qL(0), qR(0), small_tol, close_tol );
    }
  }
}

//----------------------------------------------------------------------------//
// check that solution trace is identical for adjacent elements
BOOST_AUTO_TEST_CASE( CG_Area_CheckTrace_Hierarchical )
{
  for (int order = 1; order <= BasisFunctionArea_Triangle_HierarchicalPMax; order++)
  {
    CG_Area_CheckTrace(order, BasisFunctionCategory_Hierarchical);
  }
}

//----------------------------------------------------------------------------//
// check that solution trace is identical for adjacent elements
BOOST_AUTO_TEST_CASE( CG_Area_CheckTrace_Lagrange )
{
  for (int order = 1; order <= BasisFunctionArea_Triangle_LagrangePMax; order++)
  {
    CG_Area_CheckTrace(order, BasisFunctionCategory_Lagrange);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_Area_ProjectPtoPp1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef QField2D_CG_Area::FieldCellGroupType<Triangle> QFieldAreaClass;
  typedef QFieldAreaClass::ElementType<> ElementQFieldClass;

  ArrayQ q0, q1;

  // global communicator
  mpi::communicator world;

  int ii = 3;
  int jj = 4;

  // Generate a partitioned grid
  XField2D_Box_Triangle_Lagrange_X1 xfld(world, ii, jj);

  for (int order = 1; order < BasisFunctionArea_Triangle_HierarchicalPMax; order++)
  {
    for (int orderinc = 0; orderinc <= BasisFunctionArea_Triangle_HierarchicalPMax-order; orderinc++)
    {
      QField2D_CG_Area qfldP  (xfld, order, BasisFunctionCategory_Hierarchical);
      QField2D_CG_Area qfldPp1(xfld, order+orderinc, BasisFunctionCategory_Hierarchical);

      QFieldAreaClass& qfldAeraP   = qfldP.getCellGroup<Triangle>(0);
      QFieldAreaClass& qfldAeraPp1 = qfldPp1.getCellGroup<Triangle>(0);

      ElementQFieldClass qfldElemP(qfldAeraP.basis());
      ElementQFieldClass qfldElemPp1(qfldAeraPp1.basis());

      //Give some non-zero initial condition
      for (int n = 0; n < qfldP.nDOF(); n++)
        qfldP.DOF(n) = n+1;

      for (int n = 0; n < qfldPp1.nDOF(); n++)
        qfldPp1.DOF(n) = 0;

      //Use area function projectTo
      qfldAeraP.projectTo(qfldAeraPp1);

      for (int elem = 0; elem < qfldAeraP.nElem(); elem++)
      {
        qfldAeraP.getElement(qfldElemP, elem);
        qfldAeraPp1.getElement(qfldElemPp1, elem);

        qfldElemP.eval(0.25, 0.25, q0);
        qfldElemPp1.eval(0.25, 0.25, q1);
        BOOST_CHECK_GT(q0[0], 1);
        BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
      }

      //Wipe out DOF's for P1
      for (int n = 0; n < qfldPp1.nDOF(); n++)
        qfldPp1.DOF(n) = -1;

      //Use base function projectTo
      qfldP.projectTo(qfldPp1);

      for (int elem = 0; elem < qfldAeraP.nElem(); elem++)
      {
        qfldAeraP.getElement(qfldElemP, elem);
        qfldAeraPp1.getElement(qfldElemPp1, elem);

        qfldElemP.eval(0.25, 0.25, q0);
        qfldElemPp1.eval(0.25, 0.25, q1);
        BOOST_CHECK_GT(q0[0], 1);
        BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
      }
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_Area_Project_to_DG )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef Field_DG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_DG_Area;
  typedef QField2D_CG_Area::FieldCellGroupType<Triangle> QFieldAreaClass;
  typedef QFieldAreaClass::ElementType<> ElementQFieldClass;

  ArrayQ q0, q1;

  // global communicator
  mpi::communicator world;

  int ii = 3;
  int jj = 4;

  // Generate a partitioned grid
  XField2D_Box_Triangle_Lagrange_X1 xfld(world, ii, jj);

  for (int order = 3; order < BasisFunctionArea_Triangle_HierarchicalPMax; order++)
  {
    for (int orderinc = 0; orderinc <= BasisFunctionArea_Triangle_HierarchicalPMax-order; orderinc++)
    {
      QField2D_CG_Area qfldP  (xfld, order, BasisFunctionCategory_Hierarchical);
      QField2D_DG_Area qfldPp1(xfld, order+orderinc, BasisFunctionCategory_Hierarchical);

      QFieldAreaClass& qfldAeraP   = qfldP.getCellGroup<Triangle>(0);
      QFieldAreaClass& qfldAeraPp1 = qfldPp1.getCellGroup<Triangle>(0);

      ElementQFieldClass qfldElemP(qfldAeraP.basis());
      ElementQFieldClass qfldElemPp1(qfldAeraPp1.basis());

      //Give some non-zero initial condition
      for (int n = 0; n < qfldP.nDOF(); n++)
        qfldP.DOF(n) = n+1;

      for (int n = 0; n < qfldPp1.nDOF(); n++)
        qfldPp1.DOF(n) = 0;

      //Use area function projectTo
      qfldAeraP.projectTo(qfldAeraPp1);

      for (int elem = 0; elem < qfldAeraP.nElem(); elem++)
      {
        qfldAeraP.getElement(qfldElemP, elem);
        qfldAeraPp1.getElement(qfldElemPp1, elem);

        qfldElemP.eval(0.25, 0.25, q0);
        qfldElemPp1.eval(0.25, 0.25, q1);
        BOOST_CHECK_GT(q0[0], 1);
        BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
      }

      //Wipe out DOF's for P1
      for (int n = 0; n < qfldPp1.nDOF(); n++)
        qfldPp1.DOF(n) = -1;

      //Use base function projectTo
      qfldP.projectTo(qfldPp1);

      for (int elem = 0; elem < qfldAeraP.nElem(); elem++)
      {
        qfldAeraP.getElement(qfldElemP, elem);
        qfldAeraPp1.getElement(qfldElemPp1, elem);

        qfldElemP.eval(0.25, 0.25, q0);
        qfldElemPp1.eval(0.25, 0.25, q1);
        BOOST_CHECK_GT(q0[0], 1);
        BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
      }
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ParallelCopy )
{
  typedef Real ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef QField2D_CG_Area::FieldCellGroupType<Triangle> QFieldAreaClass;

  // global communicator
  mpi::communicator world;

  int ii = 4;
  int jj = 5;

  // Generate a partitioned grid
  XField2D_Box_Triangle_Lagrange_X1 xfld(world, ii, jj);

  BOOST_CHECK_EQUAL( (ii+1)*(jj+1), xfld.nDOFnative());

  // fields
  int order = 1;
  QField2D_CG_Area qfld1(xfld, order, BasisFunctionCategory_Lagrange);

  BOOST_CHECK_EQUAL( (ii+1)*(jj+1), qfld1.nDOFnative() );

  const int nDOFPDE = qfld1.nDOF();
  for (int n = 0; n < nDOFPDE; n++)
    qfld1.DOF(n) = n;

  QField2D_CG_Area qfld2(qfld1, FieldCopy());

  BOOST_CHECK_EQUAL( (ii+1)*(jj+1), qfld2.nDOFnative() );

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld2.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld2.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld2.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()         ,  qfld2.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld2.getXField() );

  const QFieldAreaClass& qfld1Area = qfld1.getCellGroup<Triangle>(0);
  const QFieldAreaClass& qfld2Area = qfld2.getCellGroup<Triangle>(0);

  for (int n = 0; n < nDOFPDE; n++)
  {
    BOOST_CHECK_EQUAL( qfld1.DOF(n), n );
    BOOST_CHECK_EQUAL( qfld1.DOF(n), qfld1Area.DOF(n) );
    BOOST_CHECK_EQUAL( qfld2.DOF(n), n );
    BOOST_CHECK_EQUAL( qfld2.DOF(n), qfld2Area.DOF(n) );
    BOOST_CHECK_EQUAL( qfld1.local2nativeDOFmap(n), qfld2.local2nativeDOFmap(n) );
  }

  for (int n = 0; n < qfld1.nDOF() - qfld1.nDOFpossessed(); n++)
  {
     BOOST_CHECK_EQUAL( qfld1.DOFghost_rank(n), qfld2.DOFghost_rank(n) );
  }
}

//----------------------------------------------------------------------------//
void checkPartitionedNativeIndexing(const int order)
{
  typedef Real ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef QField2D_CG_Area::FieldCellGroupType<Triangle> QFieldAreaClass;

  // global communicator
  mpi::communicator world;

  int ii = 4;
  int jj = 5;

  // Generate a global grid (identical on all processors)
  XField2D_Box_Triangle_Lagrange_X1 xfld_global(world.split(world.rank()), ii, jj);

  // Generate a partitioned grid
  XField2D_Box_Triangle_Lagrange_X1 xfld_local(world, ii, jj);

  BOOST_CHECK_EQUAL(xfld_global.nDOFnative(), xfld_local.nDOFnative());

  // local and global fields
  QField2D_CG_Area qfld_local(xfld_local, order, BasisFunctionCategory_Lagrange);
  QField2D_CG_Area qfld_global(xfld_global, order, BasisFunctionCategory_Lagrange);

  BOOST_CHECK_EQUAL(qfld_global.nDOFnative(), qfld_local.nDOFnative());

  // gather the local native DOFs and make sure they are unique for each processor
  std::vector<int> nativeDOF(qfld_local.nDOFpossessed());
  for (int idof = 0; idof < qfld_local.nDOFpossessed(); idof++)
    nativeDOF[idof] = qfld_local.local2nativeDOFmap(idof);

  // send the native DOF to all other ranks
  std::vector<std::vector<int>> globalnativeDOF(world.size());
#ifdef SANS_MPI
  boost::mpi::all_gather(world, nativeDOF, globalnativeDOF);
#else
  globalnativeDOF[0] = nativeDOF;
#endif

  // check that the possessed nativeDOF is unieque to each processor
  std::set<int> uniqueNativeDOF;
  for (std::size_t rank = 0; rank < globalnativeDOF.size(); rank++)
  {
    for ( const int nativeDOF : globalnativeDOF[rank])
    {
      BOOST_CHECK(uniqueNativeDOF.find(nativeDOF) == uniqueNativeDOF.end());
      uniqueNativeDOF.insert(nativeDOF);
    }
  }

  // check that the unique count adds up to the total DOF count
  BOOST_CHECK_EQUAL(qfld_global.nDOF(), uniqueNativeDOF.size());


  // check that local2nativeDOFmap is identity on a single processor
  if (world.size() == 1)
    for (int idof = 0; idof < qfld_local.nDOF(); idof++)
      BOOST_CHECK_EQUAL(idof, qfld_local.local2nativeDOFmap(idof));

  for (int idof = 0; idof < qfld_global.nDOF(); idof++)
    BOOST_CHECK_EQUAL(idof, qfld_global.local2nativeDOFmap(idof));


  int group = 0;

  // local and global cell groups
  const QFieldAreaClass& qfldCellGroup_local = qfld_local.getCellGroup<Triangle>(group);
  const QFieldAreaClass& qfldCellGroup_global = qfld_global.getCellGroup<Triangle>(group);

  int nBasis = qfldCellGroup_local.basis()->nBasis();
  std::vector<int> map_local(nBasis);
  std::vector<int> map_global(nBasis);

  const std::vector<int>& groupCellID = xfld_local.cellIDs(group);


  world.barrier();
  for (int irank = 0; irank < world.size(); irank++)
  {
    std::cout << std::flush;
#ifdef SLEEP_FOR_OUTPUT
    std::this_thread::sleep_for( std::chrono::milliseconds(SLEEP_MILLISECONDS) );
#endif
    world.barrier();
    if (irank != world.rank() ) continue;

    // collect the DOF indexing on the current rank in order to construct
    // the global continuous mapping
    for (int elem_local = 0; elem_local < qfldCellGroup_local.nElem(); elem_local++)
    {
      int elem_global = groupCellID[elem_local];
      qfldCellGroup_local.associativity(elem_local).getGlobalMapping(map_local.data(), map_local.size());
      qfldCellGroup_global.associativity(elem_global).getGlobalMapping(map_global.data(), map_global.size());

#if 0
      int elemRank = qfldCellGroup_local.associativity(elem_local).rank();
      std::cout << "rank " << world.rank() << " elemRank = " << elemRank
                << " elem_global = " << elem_global << " map_global = " << map_global << " l2n = ";
      for (int n = 0; n < nBasis; n++)
        std::cout << qfld_local.local2nativeDOFmap(map_local[n]) << " ";
      std::cout << std::endl;
#endif
      for (int n = 0; n < nBasis; n++)
        BOOST_CHECK_EQUAL(map_global[n], qfld_local.local2nativeDOFmap(map_local[n]));
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PartitionedNativeIndexing_P1 )
{
  checkPartitionedNativeIndexing(1);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PartitionedNativeIndexing_P2 )
{
  checkPartitionedNativeIndexing(2);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PartitionedNativeIndexing_P3 )
{
  checkPartitionedNativeIndexing(3);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PartitionedNativeIndexing_P4 )
{
  checkPartitionedNativeIndexing(4);
}

//----------------------------------------------------------------------------//
void checkPartitionedContinuousIndexing(const int order)
{
  typedef Real ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;

  // global communicator
  mpi::communicator world;

  const int comm_size = world.size();
  const int comm_rank = world.rank();

  int ii = 4;
  int jj = 5;

  // Generate a partitioned grid
  XField2D_Box_Triangle_Lagrange_X1 xfld(world, ii, jj);

  QField2D_CG_Area qfld(xfld, order, BasisFunctionCategory_Lagrange);

  const GlobalContinuousMap& continuousGlobalMap = qfld.continuousGlobalMap();

  // collect the DOF indexing on the current rank in order to construct
  // the global continuous mapping
  std::map<int,int> native2localDOFmap;
  for (int idof = 0; idof < qfld.nDOFpossessed(); idof++)
    native2localDOFmap[qfld.local2nativeDOFmap(idof)] = idof;

  // send the DOF index to all other ranks
  std::vector<int> nDOFonRank(world.size());;
  std::vector<std::map<int,int>> globalnative2localDOFmap(world.size());
#ifdef SANS_MPI
  boost::mpi::all_gather(world, qfld.nDOFpossessed(), nDOFonRank);
  boost::mpi::all_gather(world, native2localDOFmap, globalnative2localDOFmap);
#else
  nDOFonRank[0] = qfld.nDOFpossessed();
  globalnative2localDOFmap[0] = native2localDOFmap;
#endif

  // construct a continuous indexing across all processors
  std::vector<std::vector<int>> continuousDOFindx(world.size());
  int idxDOF = 0;
  for (int rank = 0; rank < world.size(); rank++)
  {
    continuousDOFindx[rank].resize(nDOFonRank[rank]);
    for (std::size_t i = 0; i < continuousDOFindx[rank].size(); i++)
      continuousDOFindx[rank][i] = idxDOF++;
  }

  // compute the DOF rank offset on all ranks
  std::vector<int> nDOF_rank_offset(comm_size, 0);
  for (int rank = 1; rank < comm_size; rank++)
    nDOF_rank_offset[rank] = nDOF_rank_offset[rank-1] + nDOFonRank[rank-1];

  // check that the nDOF rank offset is correct
  BOOST_CHECK_EQUAL( nDOF_rank_offset[comm_rank], continuousGlobalMap.nDOF_rank_offset );

  world.barrier();
  for (int irank = 0; irank < comm_size; irank++)
  {
    std::cout << std::flush;
#ifdef SLEEP_FOR_OUTPUT
    std::this_thread::sleep_for( std::chrono::milliseconds(SLEEP_MILLISECONDS) );
#endif
    world.barrier();
    if (irank != comm_rank ) continue;

    // check possessed DOFs
    for (int idof = 0; idof < qfld.nDOFpossessed(); idof++)
      BOOST_CHECK_EQUAL( continuousDOFindx[comm_rank][idof], idof + continuousGlobalMap.nDOF_rank_offset );

    // only ghost DOFs are included in the continuousMap
    for (int ighost = 0; ighost < qfld.nDOFghost() ; ighost++)
    {
      int remoteRank = qfld.DOFghost_rank(ighost);
      int nativeDOF = qfld.local2nativeDOFmap(ighost + qfld.nDOFpossessed());
      int remoteDOF = globalnative2localDOFmap[remoteRank].at(nativeDOF);

      BOOST_CHECK_EQUAL( continuousDOFindx[remoteRank][remoteDOF], continuousGlobalMap.remoteGhostIndex[ighost] );
    }

    std::cout << std::flush;
  }
  world.barrier();

  //output_Tecplot(xfld, "tmp/test" + std::to_string(world.rank()) + ".dat");
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PartitionedContinuousIndexing_P1 )
{
  checkPartitionedContinuousIndexing(1);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PartitionedContinuousIndexing_P2 )
{
  checkPartitionedContinuousIndexing(2);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PartitionedContinuousIndexing_P3 )
{
  checkPartitionedContinuousIndexing(3);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PartitionedContinuousIndexing_P4 )
{
  checkPartitionedContinuousIndexing(4);
}

//----------------------------------------------------------------------------//
void checkPartitionedSyncDOF(const int order)
{
  typedef Real ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;

  // global communicator
  mpi::communicator world;

  const int comm_rank = world.rank();

  int ii = 4;
  int jj = 5;

  // Generate a partitioned grid
  XField2D_Box_Triangle_Lagrange_X1 xfld(world, ii, jj);

  QField2D_CG_Area qfld(xfld, order, BasisFunctionCategory_Lagrange);

  // collect the DOF indexing on the current rank in order to construct
  // the global continuous mapping
  std::map<int,int> native2localDOFmap;
  for (int idof = 0; idof < qfld.nDOFpossessed(); idof++)
    native2localDOFmap[qfld.local2nativeDOFmap(idof)] = idof;

  // send the DOF index to all other ranks
  std::vector<int> nDOFonRank(world.size());;
  std::vector<std::map<int,int>> globalnative2localDOFmap(world.size());
#ifdef SANS_MPI
  boost::mpi::all_gather(world, qfld.nDOFpossessed(), nDOFonRank);
  boost::mpi::all_gather(world, native2localDOFmap, globalnative2localDOFmap);
#else
  nDOFonRank[0] = qfld.nDOFpossessed();
  globalnative2localDOFmap[0] = native2localDOFmap;
#endif

  // construct a continuous indexing across all processors
  std::vector<std::vector<int>> continuousDOFindx(world.size());
  int idxDOF = 0;
  for (int rank = 0; rank < world.size(); rank++)
  {
    continuousDOFindx[rank].resize(nDOFonRank[rank]);
    for (std::size_t i = 0; i < continuousDOFindx[rank].size(); i++)
      continuousDOFindx[rank][i] = idxDOF++;
  }

  // set all DOFs to -1
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = -1;

  BOOST_REQUIRE_EQUAL(qfld.nDOFpossessed(), continuousDOFindx[comm_rank].size());

  // assign the continuous index to possessed DOFs
  for (int idof = 0; idof < qfld.nDOFpossessed(); idof++)
    qfld.DOF(idof) = continuousDOFindx[comm_rank][idof];

  // synchronize the DOFs. all ghosts and zombies should now get the continuous index
  qfld.syncDOFs_MPI_noCache();

  world.barrier();
  for (int irank = 0; irank < world.size(); irank++)
  {
    std::cout << std::flush;
#ifdef SLEEP_FOR_OUTPUT
    std::this_thread::sleep_for( std::chrono::milliseconds(SLEEP_MILLISECONDS) );
#endif
    world.barrier();
    if (irank != world.rank() ) continue;

    // check that possessed DOFs have not changed
    for (int idof = 0; idof < qfld.nDOFpossessed(); idof++)
      BOOST_CHECK_EQUAL( continuousDOFindx[comm_rank][idof], qfld.DOF(idof) );

    // both ghost and zombie DOFs are synchronized
    for (int ighost = qfld.nDOFpossessed(); ighost < qfld.nDOF(); ighost++)
    {
      int remoteRank = qfld.DOFghost_rank(ighost - qfld.nDOFpossessed());
      int nativeDOF = qfld.local2nativeDOFmap(ighost);
      int remoteDOF = globalnative2localDOFmap[remoteRank].at(nativeDOF);

      BOOST_CHECK_EQUAL( continuousDOFindx[remoteRank][remoteDOF], qfld.DOF(ighost) );
    }

    std::cout << std::flush;
  }
  world.barrier();

  //output_Tecplot(xfld, "tmp/test" + std::to_string(world.rank()) + ".dat");
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PartitionedSyncDOF_P1 )
{
  checkPartitionedSyncDOF(1);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PartitionedSyncDOF_P2 )
{
  checkPartitionedSyncDOF(2);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PartitionedSyncDOF_P3 )
{
  checkPartitionedSyncDOF(3);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( PartitionedSyncDOF_P4 )
{
  checkPartitionedSyncDOF(4);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_InteriorEdge_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef QField2D_CG_Area::FieldTraceGroupType<Line> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  int nodeMap[2];

  XField2D_4Triangle_X1_1Group xfld1;

  int order = 1;
  Field_CG_InteriorTrace< PhysD2, TopoD2, ArrayQ > qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 3, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldLineClass& qfldGroup1 = qfld1.getInteriorTraceGroup<Line>(0);

  qfldGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 1, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2, nodeMap[1] );

  qfldGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 2, nodeMap[0] );
  BOOST_CHECK_EQUAL( 0, nodeMap[1] );

  qfldGroup1.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 0, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1, nodeMap[1] );

  Field< PhysD2, TopoD2, ArrayQ > qfld3(qfld1, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()      ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld3.getXField() );


  // Test the constant assignment operator
  ElementQFieldLineClass qfldElem(qfldGroup1.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfldGroup1.nElem(); elem++)
  {
    qfldGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfldGroup1.nElem(); elem++)
  {
    qfldGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_InteriorEdge_P2 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef QField2D_CG_Area::FieldTraceGroupType<Line> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  int nodeMap[2];
  int edgeMap[1];

  XField2D_4Triangle_X1_1Group xfld1;

  int nEdgeDOF = 3;

  int order = 2;
  Field_CG_InteriorTrace<PhysD2, TopoD2, ArrayQ> qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 6, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldLineClass& qfldGroup1 = qfld1.getInteriorTraceGroup<Line>(0);

  qfldGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 1 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2 + nEdgeDOF, nodeMap[1] );

  qfldGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 2 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 0 + nEdgeDOF, nodeMap[1] );

  qfldGroup1.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 0 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1 + nEdgeDOF, nodeMap[1] );

  qfldGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 2, edgeMap[0] );

  qfldGroup1.associativity(1).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 1, edgeMap[0] );

  qfldGroup1.associativity(2).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 0, edgeMap[0] );

  Field< PhysD2, TopoD2, ArrayQ > qfld3(qfld1, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()      ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld3.getXField() );


  // Test the constant assignment operator
  ElementQFieldLineClass qfldElem(qfldGroup1.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfldGroup1.nElem(); elem++)
  {
    qfldGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfldGroup1.nElem(); elem++)
  {
    qfldGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_InteriorEdge_ProjectPtoPp1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef QField2D_CG_Area::FieldTraceGroupType<Line> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  ArrayQ q1, q2;

  XField2D_4Triangle_X1_1Group xfld1;

  for (int order = 1; order < BasisFunctionLine_HierarchicalPMax; order++)
  {
    for (int orderinc = 1; orderinc <= BasisFunctionLine_HierarchicalPMax-order; orderinc++)
    {
      Field_CG_InteriorTrace<PhysD2, TopoD2, ArrayQ> qfldP(xfld1, order, BasisFunctionCategory_Hierarchical);
      Field_CG_InteriorTrace<PhysD2, TopoD2, ArrayQ> qfldPp1(xfld1, order+orderinc, BasisFunctionCategory_Hierarchical);

      for ( int group = 0; group < qfldP.nInteriorTraceGroups(); group++ )
      {
        QFieldLineClass& qfldLineGroupP   = qfldP.getInteriorTraceGroup<Line>(group);
        QFieldLineClass& qfldLineGroupPp1 = qfldPp1.getInteriorTraceGroup<Line>(group);

        ElementQFieldLineClass qfldElemP(qfldLineGroupP.basis());
        ElementQFieldLineClass qfldElemPp1(qfldLineGroupPp1.basis());

        //Give some non-zero initial condition
        for (int n = 0; n < qfldP.nDOF(); n++)
          qfldP.DOF(n) = n+1;

        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = 0;

        //Use line function projectTo
        qfldLineGroupP.projectTo(qfldLineGroupPp1);

        for (int elem = 0; elem < qfldLineGroupP.nElem(); elem++)
        {
          qfldLineGroupP.getElement(qfldElemP, elem);
          qfldLineGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(0.25, q1);
          qfldElemPp1.eval(0.25, q2);
          BOOST_CHECK_GT(q1[0], 1);
          BOOST_CHECK_CLOSE(q1[0], q2[0], 1e-12);
        }

        //Wipe out DOF's for P1
        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = -1;

        //Use base function projectTo
        qfldP.projectTo(qfldPp1);

        for (int elem = 0; elem < qfldLineGroupP.nElem(); elem++)
        {
          qfldLineGroupP.getElement(qfldElemP, elem);
          qfldLineGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(0.75, q1);
          qfldElemPp1.eval(0.75, q2);
          BOOST_CHECK_CLOSE(q1[0], q2[0], 1e-12);
        }
      }
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_BoundaryEdge_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef QField2D_CG_Area::FieldTraceGroupType<Line> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  int nodeMap[2];

  XField2D_2Triangle_X1_1Group xfld1;

  int order = 1;
  Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( 4, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldLineClass& qfldGroup1 = qfld1.getBoundaryTraceGroup<Line>(0);

  qfldGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 0, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1, nodeMap[1] );

  qfldGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 1, nodeMap[0] );
  BOOST_CHECK_EQUAL( 3, nodeMap[1] );

  qfldGroup1.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 3, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2, nodeMap[1] );

  qfldGroup1.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 2, nodeMap[0] );
  BOOST_CHECK_EQUAL( 0, nodeMap[1] );

  Field< PhysD2, TopoD2, ArrayQ > qfld3(qfld1, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()      ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld3.getXField() );


  // Test the constant assignment operator
  ElementQFieldLineClass qfldElem(qfldGroup1.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfldGroup1.nElem(); elem++)
  {
    qfldGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfldGroup1.nElem(); elem++)
  {
    qfldGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_BoundaryEdge_P3_3Group )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef QField2D_CG_Area::FieldTraceGroupType<Line> QFieldLineClass;

  int nodeMap[2], edgeMap[2];

  XField2D_4Triangle_X1_4Group xfld1;

  int order = 3;
  Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> qfld1({{0},{1},{2}}, xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( 21, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 3, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldLineClass& qfldGroup0 = qfld1.getBoundaryTraceGroup<Line>(0);
  const QFieldLineClass& qfldGroup1 = qfld1.getBoundaryTraceGroup<Line>(1);
  const QFieldLineClass& qfldGroup2 = qfld1.getBoundaryTraceGroup<Line>(2);

  qfldGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 12, nodeMap[0] );
  BOOST_CHECK_EQUAL( 14, nodeMap[1] );

  qfldGroup0.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 14, nodeMap[0] );
  BOOST_CHECK_EQUAL( 13, nodeMap[1] );

  qfldGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 16, nodeMap[0] );
  BOOST_CHECK_EQUAL( 17, nodeMap[1] );

  qfldGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 17, nodeMap[0] );
  BOOST_CHECK_EQUAL( 15, nodeMap[1] );

  qfldGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 18, nodeMap[0] );
  BOOST_CHECK_EQUAL( 20, nodeMap[1] );

  qfldGroup2.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 20, nodeMap[0] );
  BOOST_CHECK_EQUAL( 19, nodeMap[1] );

  qfldGroup0.associativity(0).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 0, edgeMap[0] );
  BOOST_CHECK_EQUAL( 1, edgeMap[1] );

  qfldGroup0.associativity(1).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 2, edgeMap[0] );
  BOOST_CHECK_EQUAL( 3, edgeMap[1] );

  qfldGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 6, edgeMap[0] );
  BOOST_CHECK_EQUAL( 7, edgeMap[1] );

  qfldGroup1.associativity(1).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 4, edgeMap[0] );
  BOOST_CHECK_EQUAL( 5, edgeMap[1] );

  qfldGroup2.associativity(0).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 8, edgeMap[0] );
  BOOST_CHECK_EQUAL( 9, edgeMap[1] );

  qfldGroup2.associativity(1).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 10, edgeMap[0] );
  BOOST_CHECK_EQUAL( 11, edgeMap[1] );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_BoundaryEdge_P2 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef QField2D_CG_Area::FieldTraceGroupType<Line> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  int nodeMap[2];
  int edgeMap[1];

  XField2D_2Triangle_X1_1Group xfld1;

  int nEdgeDOF = 4;

  int order = 2;
  Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 8, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldLineClass& qfldGroup1 = qfld1.getBoundaryTraceGroup<Line>(0);

  qfldGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 0 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1 + nEdgeDOF, nodeMap[1] );

  qfldGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 1 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 3 + nEdgeDOF, nodeMap[1] );

  qfldGroup1.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 3 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2 + nEdgeDOF, nodeMap[1] );

  qfldGroup1.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 2 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 0 + nEdgeDOF, nodeMap[1] );

  qfldGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 0, edgeMap[0] );

  qfldGroup1.associativity(1).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 2, edgeMap[0] );

  qfldGroup1.associativity(2).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 3, edgeMap[0] );

  qfldGroup1.associativity(3).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 1, edgeMap[0] );

  Field< PhysD2, TopoD2, ArrayQ > qfld3(qfld1, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()         ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld3.getXField() );

  // Test the constant assignment operator
  ElementQFieldLineClass qfldElem(qfldGroup1.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfldGroup1.nElem(); elem++)
  {
    qfldGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfldGroup1.nElem(); elem++)
  {
    qfldGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_BoundaryEdge_ProjectPtoPp1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef QField2D_CG_Area::FieldTraceGroupType<Triangle> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  ArrayQ q1, q2;
  XField2D_4Triangle_X1_1Group xfld1;

  for (int order = 1; order < BasisFunctionLine_HierarchicalPMax; order++)
  {
    for (int orderinc = 1; orderinc <= BasisFunctionLine_HierarchicalPMax-order; orderinc++)
    {
      Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> qfldP(xfld1, order, BasisFunctionCategory_Hierarchical);
      Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> qfldPp1(xfld1, order+orderinc, BasisFunctionCategory_Hierarchical);

      for ( int group = 0; group < qfldP.nBoundaryTraceGroups(); group++ )
      {
        QFieldLineClass& qfldLineGroupP   = qfldP.getBoundaryTraceGroup<Line>(group);
        QFieldLineClass& qfldLineGroupPp1 = qfldPp1.getBoundaryTraceGroup<Line>(group);

        ElementQFieldLineClass qfldElemP(qfldLineGroupP.basis());
        ElementQFieldLineClass qfldElemPp1(qfldLineGroupPp1.basis());

        //Give some non-zero initial condition
        for (int n = 0; n < qfldP.nDOF(); n++)
          qfldP.DOF(n) = n+1;

        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = 0;

        //Use line function projectTo
        qfldLineGroupP.projectTo(qfldLineGroupPp1);

        for (int elem = 0; elem < qfldLineGroupP.nElem(); elem++)
        {
          qfldLineGroupP.getElement(qfldElemP, elem);
          qfldLineGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(0.25, q1);
          qfldElemPp1.eval(0.25, q2);
          BOOST_CHECK_GT( abs(q1[0]), 1e-12 );
          BOOST_CHECK_CLOSE(q1[0], q2[0], 1e-12);
        }

        //Wipe out DOF's for P1
        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = -1;

        //Use base function projectTo
        qfldP.projectTo(qfldPp1);

        for (int elem = 0; elem < qfldLineGroupP.nElem(); elem++)
        {
          qfldLineGroupP.getElement(qfldElemP, elem);
          qfldLineGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(0.75, q1);
          qfldElemPp1.eval(0.75, q2);
          BOOST_CHECK_GT( abs(q1[0]), 1e-12 );
          BOOST_CHECK_CLOSE(q1[0], q2[0], 1e-12);
        }
      }
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_BoundaryEdge_Independent_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef QField2D_CG_Area::FieldTraceGroupType<Line> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  int nodeMap[2];

  XField2D_2Triangle_X1_1Group xfld1;

  int order = 1;
  std::vector<std::vector<int>> boundaryGroupSets = {{0}};
  Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> qfld1(boundaryGroupSets, xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 4, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 4, qfld1.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldLineClass& qfldGroup1 = qfld1.getBoundaryTraceGroup<Line>(0);

  qfldGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 0, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1, nodeMap[1] );

  qfldGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 1, nodeMap[0] );
  BOOST_CHECK_EQUAL( 3, nodeMap[1] );

  qfldGroup1.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 3, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2, nodeMap[1] );

  qfldGroup1.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 2, nodeMap[0] );
  BOOST_CHECK_EQUAL( 0, nodeMap[1] );

  Field< PhysD2, TopoD2, ArrayQ > qfld3(qfld1, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()         ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld3.getXField() );

  // Test the constant assignment operator
  ElementQFieldLineClass qfldElem(qfldGroup1.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfldGroup1.nElem(); elem++)
  {
    qfldGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfldGroup1.nElem(); elem++)
  {
    qfldGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_BoundaryEdge_Independent_P2 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef QField2D_CG_Area::FieldTraceGroupType<Line> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  int nodeMap[2];
  int edgeMap[1];

  XField2D_2Triangle_X1_1Group xfld1;

  int order = 2;
  std::vector<std::vector<int>> boundaryGroupSets = {{0}};
  Field_CG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> qfld1(boundaryGroupSets, xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 8, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 4, qfld1.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldLineClass& qfldGroup1 = qfld1.getBoundaryTraceGroup<Line>(0);

  qfldGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 4, nodeMap[0] );
  BOOST_CHECK_EQUAL( 5, nodeMap[1] );

  qfldGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 5, nodeMap[0] );
  BOOST_CHECK_EQUAL( 7, nodeMap[1] );

  qfldGroup1.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 7, nodeMap[0] );
  BOOST_CHECK_EQUAL( 6, nodeMap[1] );

  qfldGroup1.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 6, nodeMap[0] );
  BOOST_CHECK_EQUAL( 4, nodeMap[1] );

  qfldGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 0, edgeMap[0] );

  qfldGroup1.associativity(1).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 2, edgeMap[0] );

  qfldGroup1.associativity(2).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 3, edgeMap[0] );

  qfldGroup1.associativity(3).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 1, edgeMap[0] );

  Field< PhysD2, TopoD2, ArrayQ > qfld3(qfld1, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()      ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld3.getXField() );

  // Test the constant assignment operator
  ElementQFieldLineClass qfldElem(qfldGroup1.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfldGroup1.nElem(); elem++)
  {
    qfldGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfldGroup1.nElem(); elem++)
  {
    qfldGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_BoundaryEdge_Independent_ProjectPtoPp1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_BoundaryTrace< PhysD2, TopoD2, ArrayQ > QField2D_CG_BoundaryTrace;
  typedef QField2D_CG_BoundaryTrace::FieldTraceGroupType<Triangle> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  ArrayQ q1, q2;

  XField2D_4Triangle_X1_1Group xfld1;

  std::vector<std::vector<int>> boundaryGroupSets = {{0}};

  for (int order = 1; order < BasisFunctionLine_HierarchicalPMax; order++)
  {
    for (int orderinc = 1; orderinc <= BasisFunctionLine_HierarchicalPMax-order; orderinc++)
    {
      QField2D_CG_BoundaryTrace qfldP(boundaryGroupSets, xfld1, order, BasisFunctionCategory_Hierarchical);
      QField2D_CG_BoundaryTrace qfldPp1(boundaryGroupSets, xfld1, order+orderinc, BasisFunctionCategory_Hierarchical);

      for ( int group = 0; group < qfldP.nBoundaryTraceGroups(); group++ )
      {
        QFieldLineClass& qfldLineGroupP   = qfldP.getBoundaryTraceGroup<Line>(group);
        QFieldLineClass& qfldLineGroupPp1 = qfldPp1.getBoundaryTraceGroup<Line>(group);

        ElementQFieldLineClass qfldElemP(qfldLineGroupP.basis());
        ElementQFieldLineClass qfldElemPp1(qfldLineGroupPp1.basis());

        //Give some non-zero initial condition
        for (int n = 0; n < qfldP.nDOF(); n++)
          qfldP.DOF(n) = n+1;

        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = 0;

        //Use line function projectTo
        qfldLineGroupP.projectTo(qfldLineGroupPp1);

        for (int elem = 0; elem < qfldLineGroupP.nElem(); elem++)
        {
          qfldLineGroupP.getElement(qfldElemP, elem);
          qfldLineGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(0.25, q1);
          qfldElemPp1.eval(0.25, q2);
          BOOST_CHECK_CLOSE(q1[0], q2[0], 1e-12);
        }

        //Wipe out DOF's for P1
        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = -1;

        //Use base function projectTo
        qfldP.projectTo(qfldPp1);

        for (int elem = 0; elem < qfldLineGroupP.nElem(); elem++)
        {
          qfldLineGroupP.getElement(qfldElemP, elem);
          qfldLineGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(0.75, q1);
          qfldElemPp1.eval(0.75, q2);
          BOOST_CHECK_CLOSE(q1[0], q2[0], 1e-12);
        }
      }
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_TraceEdge_Hierarchical_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef QField2D_CG_Area::FieldTraceGroupType<Line> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  int nodeMap[2];

  XField2D_4Triangle_X1_1Group xfld1;

  int order = 1;
  Field_CG_Trace< PhysD2, TopoD2, ArrayQ > qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( 6, qfld1.nDOF() );
  BOOST_REQUIRE_EQUAL( 1, qfld1.nInteriorTraceGroups() );
  BOOST_REQUIRE_EQUAL( 1, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  // check interior node maps
  const QFieldLineClass& qfldGroupI = qfld1.getInteriorTraceGroup<Line>(0);

  BOOST_REQUIRE_EQUAL( 3, qfldGroupI.nElem() );
  qfldGroupI.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 1, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2, nodeMap[1] );

  qfldGroupI.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 2, nodeMap[0] );
  BOOST_CHECK_EQUAL( 0, nodeMap[1] );

  qfldGroupI.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 0, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1, nodeMap[1] );

  // check boundary node maps
  const QFieldLineClass& qfldGroupB = qfld1.getBoundaryTraceGroup<Line>(0);

  BOOST_REQUIRE_EQUAL( 6, qfldGroupB.nElem() );
  qfldGroupB.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 0, nodeMap[0] );
  BOOST_CHECK_EQUAL( 5, nodeMap[1] );

  qfldGroupB.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 5, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1, nodeMap[1] );

  qfldGroupB.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 1, nodeMap[0] );
  BOOST_CHECK_EQUAL( 3, nodeMap[1] );

  qfldGroupB.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 3, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2, nodeMap[1] );

  qfldGroupB.associativity(4).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 2, nodeMap[0] );
  BOOST_CHECK_EQUAL( 4, nodeMap[1] );

  qfldGroupB.associativity(5).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 4, nodeMap[0] );
  BOOST_CHECK_EQUAL( 0, nodeMap[1] );

  Field< PhysD2, TopoD2, ArrayQ > qfld3(qfld1, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()         ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld3.getXField() );


  // Test the constant assignment operator
  ElementQFieldLineClass qfldElem(qfldGroupI.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfldGroupI.nElem(); elem++)
  {
    qfldGroupI.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  for (int elem = 0; elem < qfldGroupB.nElem(); elem++)
  {
    qfldGroupB.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfldGroupI.nElem(); elem++)
  {
    qfldGroupI.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }

  for (int elem = 0; elem < qfldGroupB.nElem(); elem++)
  {
    qfldGroupB.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_TraceEdge_Hierarchical_P2 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef QField2D_CG_Area::FieldTraceGroupType<Line> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  int nodeMap[2];
  int edgeMap[1];

  XField2D_4Triangle_X1_1Group xfld1;

  int nEdgeDOF = 9;

  int order = 2;
  Field_CG_Trace<PhysD2, TopoD2, ArrayQ> qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( 15, qfld1.nDOF() );
  BOOST_REQUIRE_EQUAL( 1, qfld1.nInteriorTraceGroups() );
  BOOST_REQUIRE_EQUAL( 1, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  // check interior node maps
  const QFieldLineClass& qfldGroupI = qfld1.getInteriorTraceGroup<Line>(0);

  qfldGroupI.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 1 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2 + nEdgeDOF, nodeMap[1] );

  qfldGroupI.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 2 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 0 + nEdgeDOF, nodeMap[1] );

  qfldGroupI.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 0 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1 + nEdgeDOF, nodeMap[1] );

  // check boundary node maps
  const QFieldLineClass& qfldGroupB = qfld1.getBoundaryTraceGroup<Line>(0);

  BOOST_REQUIRE_EQUAL( 6, qfldGroupB.nElem() );
  qfldGroupB.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 0 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 5 + nEdgeDOF, nodeMap[1] );

  qfldGroupB.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 5 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1 + nEdgeDOF, nodeMap[1] );

  qfldGroupB.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 1 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 3 + nEdgeDOF, nodeMap[1] );

  qfldGroupB.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 3 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2 + nEdgeDOF, nodeMap[1] );

  qfldGroupB.associativity(4).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 2 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 4 + nEdgeDOF, nodeMap[1] );

  qfldGroupB.associativity(5).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 4 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 0 + nEdgeDOF, nodeMap[1] );

  // check interior edge maps

  qfldGroupI.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 4, edgeMap[0] );

  qfldGroupI.associativity(1).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 1, edgeMap[0] );

  qfldGroupI.associativity(2).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 0, edgeMap[0] );

  // check boundary edge maps

  qfldGroupB.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 3, edgeMap[0] );

  qfldGroupB.associativity(1).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 6, edgeMap[0] );

  qfldGroupB.associativity(2).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 5, edgeMap[0] );

  qfldGroupB.associativity(3).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 7, edgeMap[0] );

  qfldGroupB.associativity(4).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 8, edgeMap[0] );

  qfldGroupB.associativity(5).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 2, edgeMap[0] );


  Field< PhysD2, TopoD2, ArrayQ > qfld3(qfld1, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()      ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld3.getXField() );

  // Test the constant assignment operator
  ElementQFieldLineClass qfldElem(qfldGroupI.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfldGroupI.nElem(); elem++)
  {
    qfldGroupI.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  for (int elem = 0; elem < qfldGroupB.nElem(); elem++)
  {
    qfldGroupB.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfldGroupI.nElem(); elem++)
  {
    qfldGroupI.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }

  for (int elem = 0; elem < qfldGroupB.nElem(); elem++)
  {
    qfldGroupB.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_TraceEdge_Hierarchical_P3 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef QField2D_CG_Area::FieldTraceGroupType<Line> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  int nodeMap[2];
  int edgeMap[2];

  XField2D_4Triangle_X1_1Group xfld1;

  int nEdgeDOF = 18;

  int order = 3;
  Field_CG_Trace<PhysD2, TopoD2, ArrayQ> qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_REQUIRE_EQUAL( 24, qfld1.nDOF() );
  BOOST_REQUIRE_EQUAL( 1, qfld1.nInteriorTraceGroups() );
  BOOST_REQUIRE_EQUAL( 1, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  // check interior node maps
  const QFieldLineClass& qfldGroupI = qfld1.getInteriorTraceGroup<Line>(0);

  qfldGroupI.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 1 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2 + nEdgeDOF, nodeMap[1] );

  qfldGroupI.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 2 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 0 + nEdgeDOF, nodeMap[1] );

  qfldGroupI.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 0 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1 + nEdgeDOF, nodeMap[1] );

  // check boundary node maps
  const QFieldLineClass& qfldGroupB = qfld1.getBoundaryTraceGroup<Line>(0);

  BOOST_REQUIRE_EQUAL( 6, qfldGroupB.nElem() );
  qfldGroupB.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 0 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 5 + nEdgeDOF, nodeMap[1] );

  qfldGroupB.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 5 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1 + nEdgeDOF, nodeMap[1] );

  qfldGroupB.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 1 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 3 + nEdgeDOF, nodeMap[1] );

  qfldGroupB.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 3 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2 + nEdgeDOF, nodeMap[1] );

  qfldGroupB.associativity(4).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 2 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 4 + nEdgeDOF, nodeMap[1] );

  qfldGroupB.associativity(5).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 4 + nEdgeDOF, nodeMap[0] );
  BOOST_CHECK_EQUAL( 0 + nEdgeDOF, nodeMap[1] );

  // check interior edge maps
  qfldGroupI.associativity(0).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 8, edgeMap[0] );
  BOOST_CHECK_EQUAL( 9, edgeMap[1] );

  qfldGroupI.associativity(1).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 2, edgeMap[0] );
  BOOST_CHECK_EQUAL( 3, edgeMap[1] );

  qfldGroupI.associativity(2).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 0, edgeMap[0] );
  BOOST_CHECK_EQUAL( 1, edgeMap[1] );

  // check boundary edge maps
  qfldGroupB.associativity(0).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 6, edgeMap[0] );
  BOOST_CHECK_EQUAL( 7, edgeMap[1] );

  qfldGroupB.associativity(1).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 12, edgeMap[0] );
  BOOST_CHECK_EQUAL( 13, edgeMap[1] );

  qfldGroupB.associativity(2).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 10, edgeMap[0] );
  BOOST_CHECK_EQUAL( 11, edgeMap[1] );

  qfldGroupB.associativity(3).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 14, edgeMap[0] );
  BOOST_CHECK_EQUAL( 15, edgeMap[1] );

  qfldGroupB.associativity(4).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 16, edgeMap[0] );
  BOOST_CHECK_EQUAL( 17, edgeMap[1] );

  qfldGroupB.associativity(5).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 4, edgeMap[0] );
  BOOST_CHECK_EQUAL( 5, edgeMap[1] );

  Field< PhysD2, TopoD2, ArrayQ > qfld3(qfld1, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()      ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld3.getXField() );

  // Test the constant assignment operator
  ElementQFieldLineClass qfldElem(qfldGroupI.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfldGroupI.nElem(); elem++)
  {
    qfldGroupI.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  for (int elem = 0; elem < qfldGroupB.nElem(); elem++)
  {
    qfldGroupB.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfldGroupI.nElem(); elem++)
  {
    qfldGroupI.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }

  for (int elem = 0; elem < qfldGroupB.nElem(); elem++)
  {
    qfldGroupB.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_TraceEdge_Hierarchical_ProjectPtoPp1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef QField2D_CG_Area::FieldTraceGroupType<Line> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  ArrayQ q1, q2;

  XField2D_4Triangle_X1_1Group xfld1;

  for (int order = 1; order < BasisFunctionLine_HierarchicalPMax; order++)
  {
    for (int orderinc = 1; orderinc <= BasisFunctionLine_HierarchicalPMax-order; orderinc++)
    {
      Field_CG_Trace<PhysD2, TopoD2, ArrayQ> qfldP(xfld1, order, BasisFunctionCategory_Hierarchical);
      Field_CG_Trace<PhysD2, TopoD2, ArrayQ> qfldPp1(xfld1, order+orderinc, BasisFunctionCategory_Hierarchical);

      //Give some non-zero initial condition
      for (int n = 0; n < qfldP.nDOF(); n++)
        qfldP.DOF(n) = n+1;

      for (int n = 0; n < qfldPp1.nDOF(); n++)
        qfldPp1.DOF(n) = 0;

      //Use base function projectTo (can't project individual fields due to CG)
      qfldP.projectTo(qfldPp1);

      // inerior group
      for ( int group = 0; group < qfldP.nInteriorTraceGroups(); group++ )
      {
        QFieldLineClass& qfldLineGroupP   = qfldP.getInteriorTraceGroup<Line>(group);
        QFieldLineClass& qfldLineGroupPp1 = qfldPp1.getInteriorTraceGroup<Line>(group);

        ElementQFieldLineClass qfldElemP(qfldLineGroupP.basis());
        ElementQFieldLineClass qfldElemPp1(qfldLineGroupPp1.basis());

        for (int elem = 0; elem < qfldLineGroupP.nElem(); elem++)
        {
          qfldLineGroupP.getElement(qfldElemP, elem);
          qfldLineGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(0.75, q1);
          qfldElemPp1.eval(0.75, q2);
          BOOST_CHECK_CLOSE(q1[0], q2[0], 1e-12);
        }
      }

      // Boundary group
      for ( int group = 0; group < qfldP.nBoundaryTraceGroups(); group++ )
      {
        QFieldLineClass& qfldLineGroupP   = qfldP.getBoundaryTraceGroup<Line>(group);
        QFieldLineClass& qfldLineGroupPp1 = qfldPp1.getBoundaryTraceGroup<Line>(group);

        ElementQFieldLineClass qfldElemP(qfldLineGroupP.basis());
        ElementQFieldLineClass qfldElemPp1(qfldLineGroupPp1.basis());

        for (int elem = 0; elem < qfldLineGroupP.nElem(); elem++)
        {
          qfldLineGroupP.getElement(qfldElemP, elem);
          qfldLineGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(0.75, q1);
          qfldElemPp1.eval(0.75, q2);
          BOOST_CHECK_CLOSE(q1[0], q2[0], 1e-12);
        }
      }
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_Exception )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef Field_CG_Trace< PhysD2, TopoD2, ArrayQ > QField2D_CG_Edge;
  typedef Field_CG_InteriorTrace< PhysD2, TopoD2, ArrayQ > QField2D_CG_InteriorTrace;
  typedef Field_CG_BoundaryTrace< PhysD2, TopoD2, ArrayQ > QField2D_CG_BoundaryTrace;


  XField2D_2Triangle_X1_1Group xfld1;

  int order = 999; //This should always higher than the maximum available order
  BOOST_CHECK_THROW( QField2D_CG_Area qfld1(xfld1, order, BasisFunctionCategory_Hierarchical), DeveloperException );
  BOOST_CHECK_THROW( QField2D_CG_Edge qfld1(xfld1, order, BasisFunctionCategory_Hierarchical), DeveloperException );
  BOOST_CHECK_THROW( QField2D_CG_InteriorTrace qfld1(xfld1, order, BasisFunctionCategory_Hierarchical), DeveloperException );
  BOOST_CHECK_THROW( QField2D_CG_BoundaryTrace qfld1(xfld1, order, BasisFunctionCategory_Hierarchical), DeveloperException );

  // Can't use Legendre basis functions
  order = 1;
  BOOST_CHECK_THROW( QField2D_CG_Area qfld1(xfld1, order, BasisFunctionCategory_Legendre), AssertionException );
  BOOST_CHECK_THROW( QField2D_CG_Edge qfld1(xfld1, order, BasisFunctionCategory_Legendre), AssertionException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef Field_CG_Trace< PhysD2, TopoD2, ArrayQ > QField2D_CG_Edge;
  typedef Field_CG_InteriorTrace< PhysD2, TopoD2, ArrayQ > QField2D_CG_InteriorTrace;
  typedef Field_CG_BoundaryTrace< PhysD2, TopoD2, ArrayQ > QField2D_CG_BoundaryTrace;

  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Field/Field2D_CG_Triangle_pattern.txt", true );

  XField2D_2Triangle_X1_1Group xfld;

  QField2D_CG_Area qfld1(xfld, 2, BasisFunctionCategory_Hierarchical);
  qfld1 = 0;
  qfld1.dump( 2, output );

  QField2D_CG_Edge qfld2(xfld, 2, BasisFunctionCategory_Hierarchical);
  qfld2 = 0;
  qfld2.dump( 2, output );

  QField2D_CG_InteriorTrace qfld3(xfld, 2, BasisFunctionCategory_Hierarchical);
  qfld3 = 0;
  qfld3.dump( 2, output );

  QField2D_CG_BoundaryTrace qfld4(xfld, 2, BasisFunctionCategory_Hierarchical);
  qfld4 = 0;
  qfld4.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
