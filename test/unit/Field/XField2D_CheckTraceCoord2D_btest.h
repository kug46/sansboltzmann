// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef XFIELD2D_CHECKTRACECOORD_BTEST_H
#define XFIELD2D_CHECKTRACECOORD_BTEST_H

#include <vector>

#include "Field/XField.h"

#include "Field/tools/for_each_InteriorFieldTraceGroup_Cell.h"
#include "Field/tools/for_each_BoundaryFieldTraceGroup_Cell.h"

namespace SANS
{

//----------------------------------------------------------------------------//
class CheckInteriorTraceCoordinates2D:
    public GroupFunctorInteriorTraceType<CheckInteriorTraceCoordinates2D>
{
public:
  typedef PhysD2 PhysDim;

  explicit CheckInteriorTraceCoordinates2D( const Real small_tol, const Real close_tol,
                                            const std::vector<int>& interiorTraceGroups ) :
    small_tol(small_tol), close_tol(close_tol), interiorTraceGroups_(interiorTraceGroups) {}

  std::size_t nInteriorTraceGroups() const { return interiorTraceGroups_.size(); }
  std::size_t interiorTraceGroup(const int n) const { return interiorTraceGroups_[n]; }

  //----------------------------------------------------------------------------//
  // Distribution function that redistributes the error in each cell group
  template< class TopologyTrace, class TopologyL, class TopologyR>
  void
  apply( const typename XField<PhysDim, typename TopologyL::TopoDim>::template FieldCellGroupType<TopologyL>& xfldCellL,
         const typename XField<PhysDim, typename TopologyR::TopoDim>::template FieldCellGroupType<TopologyR>& xfldCellR,
         const typename XField<PhysDim, typename TopologyTrace::CellTopoDim>::template FieldTraceGroupType<TopologyTrace>& xIfld,
         const typename XField<PhysDim, typename TopologyTrace::CellTopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
         const int traceGroupGlobal)
  {
    typedef typename TopologyL::TopoDim TopoDim;
    // Cell Group Types
    typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyR> XFieldCellGroupTypeR;
    typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldCellClassL;
    typedef typename XFieldCellGroupTypeR::template ElementType<> ElementXFieldCellClassR;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;

    typedef typename XField<PhysDim, TopoDim>::VectorX VectorX;

    // Construct the elements
    ElementXFieldCellClassL xfldElemL(xfldCellL.basis() );
    ElementXFieldCellClassR xfldElemR(xfldCellR.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // loop over elements
    for (int elem = 0; elem < xfldTrace.nElem();elem++)
    {
      int elemL = xfldTrace.getElementLeft( elem );
      int elemR = xfldTrace.getElementRight( elem );
      CanonicalTraceToCell canonicalEdgeL = xfldTrace.getCanonicalTraceLeft( elem );
      CanonicalTraceToCell canonicalEdgeR = xfldTrace.getCanonicalTraceRight( elem );

      // copy global grid/solution DOFs to element
      xfldTrace.getElement( xfldElemTrace, elem );
      xfldCellL.getElement( xfldElemL, elemL );
      xfldCellR.getElement( xfldElemR, elemR );

      int kmax = 5;
      for (int k = 0; k < kmax; k++)
      {
        Real sRefL, tRefL, sRefR, tRefR;
        Real sRef;
        VectorX xL, xR, xT;

        sRef = k/static_cast<Real>(kmax-1);

        // left/right reference-element coords
        TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyL>::eval( canonicalEdgeL, sRef, sRefL, tRefL );
        TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyR>::eval( canonicalEdgeR, sRef, sRefR, tRefR );

        // element coordinates from L/R elements and the trace
        xfldElemTrace.eval( sRef, xT );
        xfldElemL.eval( sRefL, tRefL, xL );
        xfldElemR.eval( sRefR, tRefR, xR );

        // check if the face coordinates match the cell coordinates
        SANS_CHECK_CLOSE( xT[0], xL[0], small_tol, close_tol );
        SANS_CHECK_CLOSE( xT[1], xL[1], small_tol, close_tol );

        // check if the left and right coordinates match
        SANS_CHECK_CLOSE( xL[0], xR[0], small_tol, close_tol );
        SANS_CHECK_CLOSE( xL[1], xR[1], small_tol, close_tol );
      }
    }
  }
protected:
  const Real small_tol;
  const Real close_tol;

  const std::vector<int> interiorTraceGroups_;
};


//----------------------------------------------------------------------------//
class CheckBoundaryTraceCoordinates2D:
    public GroupFunctorBoundaryTraceType<CheckBoundaryTraceCoordinates2D>
{
public:
  typedef PhysD2 PhysDim;

  explicit CheckBoundaryTraceCoordinates2D( const Real small_tol, const Real close_tol,
                                            const std::vector<int>& boundaryTraceGroups ) :
    small_tol(small_tol), close_tol(close_tol), boundaryTraceGroups_(boundaryTraceGroups) {}

  std::size_t nBoundaryTraceGroups() const { return boundaryTraceGroups_.size(); }
  std::size_t boundaryTraceGroup(const int n) const { return boundaryTraceGroups_[n]; }

  //----------------------------------------------------------------------------//
  // Distribution function that redistributes the error in each cell group
  template< class TopologyTrace, class TopologyL>
  void
  apply( const typename XField<PhysDim, typename TopologyL::TopoDim>::template FieldCellGroupType<TopologyL>& xfldCellL,
         const typename XField<PhysDim, typename TopologyL::TopoDim>::template FieldTraceGroupType<TopologyTrace>& xBfld,
         const typename XField<PhysDim, typename TopologyL::TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
         const int traceGroupGlobal)
  {
    typedef typename XField<PhysDim, typename TopologyL::TopoDim>::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename XField<PhysDim, typename TopologyL::TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldCellClassL;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;

    typedef typename XField<PhysDim, typename TopologyL::TopoDim>::VectorX VectorX;

    // Construct the elements
    ElementXFieldCellClassL xfldElemL(xfldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // loop over elements
    for (int elem = 0; elem < xfldTrace.nElem();elem++)
    {
      int elemL = xfldTrace.getElementLeft( elem );
      CanonicalTraceToCell canonicalEdgeL = xfldTrace.getCanonicalTraceLeft( elem );

      // copy global grid/solution DOFs to element
      xfldTrace.getElement( xfldElemTrace, elem );
      xfldCellL.getElement( xfldElemL, elemL );

      int kmax = 5;
      for (int k = 0; k < kmax; k++)
      {
        Real sRefL, tRefL;
        Real sRef;
        VectorX xL, xT;

        sRef = k/static_cast<Real>(kmax-1);

        // left/right reference-element coords
        TraceToCellRefCoord<TopologyTrace, typename TopologyL::TopoDim, TopologyL>::eval( canonicalEdgeL, sRef, sRefL, tRefL );

        // element coordinates from L/R elements and the trace
        xfldElemTrace.eval( sRef, xT );
        xfldElemL.eval( sRefL, tRefL, xL );

        // check if the face coordinates match the cell coordinates
        SANS_CHECK_CLOSE( xT[0], xL[0], small_tol, close_tol );
        SANS_CHECK_CLOSE( xT[1], xL[1], small_tol, close_tol );
      }
    }
  }
protected:
  const Real small_tol;
  const Real close_tol;

  const std::vector<int>& boundaryTraceGroups_;
};

}

#endif //XFIELD2D_CHECKTRACECOORD_BTEST_H
