// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Field_Tuple_btest
// testing of FieldTuple classes
//

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/Tuple.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_InteriorTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

#include "Field/Tuple/FieldTuple.h"
#include "Field/Tuple/ElementTuple.h"
#include "Field/Tuple/SurrealizedElementTuple.h"

using namespace std;
using namespace SANS;


//Explicitly instantiate classes to get proper coverage information
namespace SANS
{
template class FieldTuple< XField<PhysD2, TopoD2>, Field< PhysD2, TopoD2, Real >, TupleClass<> >;
template class FieldTuple< Field< PhysD2, TopoD2, Real >, XField<PhysD2, TopoD2>, TupleClass<> >;
template class FieldTuple< Field< PhysD2, TopoD2, Real >, Field< PhysD2, TopoD2, Real >, TupleClass<> >;

template class ElementTuple< Element< Real, TopoD2, Triangle >, Element< Real, TopoD2, Triangle >, TupleClass<> >;
template class ElementTuple< Element< Real, TopoD2, Triangle >, ElementXField< PhysD2, TopoD2, Triangle >, TupleClass<> >;
template class ElementTuple< ElementXField< PhysD2, TopoD2, Triangle >, Element< Real, TopoD2, Triangle >, TupleClass<> >;
template class ElementTuple< ElementXField< PhysD2, TopoD2, Triangle >, ElementXField< PhysD2, TopoD2, Triangle >, TupleClass<> >;


template class BasisTuple< BasisFunctionAreaBase<Triangle>, BasisFunctionAreaBase<Triangle>, TupleClass<> >;
template class BasisPointTuple< BasisFunctionAreaBase<Triangle>, BasisFunctionAreaBase<Triangle>, TupleClass<> >;
template class ParamTuple< Real, Real, TupleClass<> >;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( Field_Tuple_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( statics )
{
}

template<class L, class R, class Fld0, class Fld1, int Level>
void testTupleParenOperator( const FieldTuple< L, R, TupleClass<Level> >& tuple, const Fld0& fld0, const Fld1& fld1)
{
  BOOST_CHECK_EQUAL( &fld0, &get<0>(tuple) );
  BOOST_CHECK_EQUAL( &fld1, &get<1>(tuple) );
  BOOST_CHECK( &tuple.getXField() == &fld0.getXField() );
  BOOST_CHECK( &tuple.getXField() == &fld1.getXField() );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldTuple_2Fld_Cell_test )
{
  typedef DLA::VectorS<2, Real> ArrayP;
  typedef Field_DG_Cell< PhysD2, TopoD2, ArrayP > PField2D_DG_Area;
  XField2D_2Triangle_X1_1Group xfld;

  int order = 0;
  PField2D_DG_Area pfldCell(xfld, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 2, pfldCell.nDOF() );
  pfldCell.DOF(0) = 3.14;
  pfldCell.DOF(1) = 5.13;

  typedef FieldTuple< XField<PhysD2, TopoD2>, Field<PhysD2, TopoD2, ArrayP>, TupleClass<> > FieldTupleCell;

  FieldTupleCell fldCellTuple = (xfld, pfldCell);

  BOOST_CHECK_EQUAL( &xfld, &get<0>(fldCellTuple) );
  BOOST_CHECK_EQUAL( &pfldCell, &get<1>(fldCellTuple) );

  FieldTupleCell fldCellTupleCopy(fldCellTuple);

  BOOST_CHECK_EQUAL( &xfld, &get<0>(fldCellTupleCopy) );
  BOOST_CHECK_EQUAL( &pfldCell, &get<1>(fldCellTupleCopy) );

  testTupleParenOperator( (xfld, pfldCell), xfld, pfldCell );


  BOOST_CHECK_THROW( fldCellTuple.nInteriorTraceGroups(), AssertionException );
  BOOST_CHECK_THROW( fldCellTuple.nBoundaryTraceGroups(), AssertionException );
  BOOST_CHECK_EQUAL( 1, fldCellTuple.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld, &fldCellTuple.getXField() );


  BOOST_CHECK_THROW( fldCellTuple.getInteriorTraceGroupBase(0), AssertionException );
  BOOST_CHECK_THROW( fldCellTuple.getBoundaryTraceGroupBase(0), AssertionException );
  BOOST_CHECK( typeid(Triangle) == fldCellTuple.getCellGroupBase(0).topoTypeID() );

  BOOST_CHECK_EQUAL( xfld.getCellGroupBase(0).nElem() , fldCellTuple.getCellGroupBase(0).nElem() );


  BOOST_CHECK_THROW( fldCellTuple.getInteriorTraceGroup<Line>(0), AssertionException );
  BOOST_CHECK_THROW( fldCellTuple.getBoundaryTraceGroup<Line>(0), AssertionException );
  const FieldTupleCell::FieldCellGroupType<Triangle>& cellGroup = fldCellTuple.getCellGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( &xfld.getCellGroup<Triangle>(0), &cellGroup.fldL() );
  BOOST_CHECK_EQUAL( &pfldCell.getCellGroup<Triangle>(0), &cellGroup.fldR() );
  BOOST_CHECK_EQUAL( 2, cellGroup.nElem() );

  BOOST_CHECK_EQUAL( &xfld.getCellGroup<Triangle>(0), &get<0>(cellGroup) );
  BOOST_CHECK_EQUAL( &pfldCell.getCellGroup<Triangle>(0), &get<1>(cellGroup) );

  typedef FieldTupleCell::FieldCellGroupType<Triangle>::ElementType<> ElementType;
  ElementType cellElem( cellGroup.basis() );

  cellGroup.getElement( cellElem, 0 );

  BOOST_CHECK_EQUAL( 0, cellElem.elemL().DOF(0)[0] );
  BOOST_CHECK_EQUAL( 0, cellElem.elemL().DOF(0)[1] );

  BOOST_CHECK_EQUAL( 1, cellElem.elemL().DOF(1)[0] );
  BOOST_CHECK_EQUAL( 0, cellElem.elemL().DOF(1)[1] );

  BOOST_CHECK_EQUAL( 0, cellElem.elemL().DOF(2)[0] );
  BOOST_CHECK_EQUAL( 1, cellElem.elemL().DOF(2)[1] );


  BOOST_CHECK_EQUAL( 0, get<0>(cellElem).DOF(0)[0] );
  BOOST_CHECK_EQUAL( 0, get<0>(cellElem).DOF(0)[1] );

  BOOST_CHECK_EQUAL( 1, get<0>(cellElem).DOF(1)[0] );
  BOOST_CHECK_EQUAL( 0, get<0>(cellElem).DOF(1)[1] );

  BOOST_CHECK_EQUAL( 0, get<0>(cellElem).DOF(2)[0] );
  BOOST_CHECK_EQUAL( 1, get<0>(cellElem).DOF(2)[1] );



  BOOST_CHECK_CLOSE( 3.14, cellElem.elemR().DOF(0)[0], 1e-12 );
  BOOST_CHECK_CLOSE( 3.14, cellElem.elemR().DOF(0)[1], 1e-12 );

  BOOST_CHECK_CLOSE( 3.14, get<1>(cellElem).DOF(0)[0], 1e-12 );
  BOOST_CHECK_CLOSE( 3.14, get<1>(cellElem).DOF(0)[1], 1e-12 );


  ElementType::T p;

  cellElem.eval({1./2.,1./2.}, p);

  BOOST_CHECK_CLOSE( 0.5, get<0>(p)[0], 1e-12 );
  BOOST_CHECK_CLOSE( 0.5, get<0>(p)[1], 1e-12 );

  BOOST_CHECK_CLOSE( 3.14, get<1>(p)[0], 1e-12 );

  // Zero out for the next test
  p = 0;

  BOOST_CHECK_SMALL( get<0>(p)[0], 1e-12 );
  BOOST_CHECK_SMALL( get<0>(p)[1], 1e-12 );
  BOOST_CHECK_SMALL( get<1>(p)[0], 1e-12 );

  // Evaluate the basis at a point and then compute the parameter from the basis functions
  FieldTupleCell::FieldCellGroupType<Triangle>::BasisType::BasisPointType phi(cellElem.basis());

  cellElem.evalBasis({1./2.,1./2.}, phi);
  cellElem.evalFromBasis(phi, phi.size(), p);

  BOOST_CHECK_CLOSE( 0.5, get<0>(p)[0], 1e-12 );
  BOOST_CHECK_CLOSE( 0.5, get<0>(p)[1], 1e-12 );

  BOOST_CHECK_CLOSE( 3.14, get<1>(p)[0], 1e-12 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Tuple_FieldTuple_2Fld_Cell_test )
{
  typedef DLA::VectorS<2, Real> ArrayP;
  typedef Field_DG_Cell< PhysD2, TopoD2, ArrayP > PField2D_DG_Area;
  XField2D_2Triangle_X1_1Group xfld;

  int order = 0;
  PField2D_DG_Area pfldCell1(xfld, order, BasisFunctionCategory_Legendre);
  PField2D_DG_Area pfldCell2(xfld, order, BasisFunctionCategory_Legendre);
  PField2D_DG_Area pfldCell3(xfld, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 2, pfldCell1.nDOF() );
  pfldCell1.DOF(0) = 3.14;
  pfldCell1.DOF(1) = 5.13;

  BOOST_CHECK_EQUAL( 2, pfldCell2.nDOF() );
  pfldCell2.DOF(0) = 6.28;
  pfldCell2.DOF(1) = 2.72;

  BOOST_CHECK_EQUAL( 2, pfldCell3.nDOF() );
  pfldCell3.DOF(0) = 1.62;
  pfldCell3.DOF(1) = 5.10;

  typedef FieldTuple< XField<PhysD2, TopoD2>, Field<PhysD2, TopoD2, ArrayP>, TupleClass<0> > FieldTupleCell1;
  typedef FieldTuple< Field<PhysD2, TopoD2, ArrayP>, Field<PhysD2, TopoD2, ArrayP>, TupleClass<0> > FieldTupleCell2;
  typedef FieldTuple< FieldTupleCell1, FieldTupleCell2, TupleClass<1> > FieldTupleCell3;

  FieldTupleCell3 fldCellTuple3 = ((xfld, pfldCell1), (pfldCell2, pfldCell3));

  BOOST_CHECK_EQUAL( &xfld     , &fldCellTuple3.getXField() );
  BOOST_CHECK_EQUAL( &xfld     , &get<0>(get<0>(fldCellTuple3)) );
  BOOST_CHECK_EQUAL( &pfldCell1, &get<1>(get<0>(fldCellTuple3)) );
  BOOST_CHECK_EQUAL( &pfldCell2, &get<0>(get<1>(fldCellTuple3)) );
  BOOST_CHECK_EQUAL( &pfldCell3, &get<1>(get<1>(fldCellTuple3)) );

  FieldTupleCell3 fldCellTuple3Copy(fldCellTuple3);

  BOOST_CHECK_EQUAL( &xfld     , &fldCellTuple3Copy.getXField() );
  BOOST_CHECK_EQUAL( &xfld     , &get<0>(get<0>(fldCellTuple3Copy)) );
  BOOST_CHECK_EQUAL( &pfldCell1, &get<1>(get<0>(fldCellTuple3Copy)) );
  BOOST_CHECK_EQUAL( &pfldCell2, &get<0>(get<1>(fldCellTuple3Copy)) );
  BOOST_CHECK_EQUAL( &pfldCell3, &get<1>(get<1>(fldCellTuple3Copy)) );

  BOOST_CHECK_THROW( fldCellTuple3.nInteriorTraceGroups(), AssertionException );
  BOOST_CHECK_THROW( fldCellTuple3.nBoundaryTraceGroups(), AssertionException );
  BOOST_CHECK_EQUAL( 1, fldCellTuple3.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld, &get<0>(fldCellTuple3).getXField() );

  BOOST_CHECK_THROW( fldCellTuple3.getInteriorTraceGroupBase(0), AssertionException );
  BOOST_CHECK_THROW( fldCellTuple3.getBoundaryTraceGroupBase(0), AssertionException );
  BOOST_CHECK( typeid(Triangle) == fldCellTuple3.getCellGroupBase(0).topoTypeID() );

  BOOST_CHECK_EQUAL( xfld.getCellGroupBase(0).nElem() , fldCellTuple3.getCellGroupBase(0).nElem() );

  {
    // testing appending a tuple to a tuple
    FieldTupleCell1 fldCellTuple1 = (xfld, pfldCell1);
    FieldTupleCell3 fldCellTuple3 = (fldCellTuple1, (pfldCell2, pfldCell3));

    BOOST_CHECK_EQUAL( &xfld     , &fldCellTuple3.getXField() );
    BOOST_CHECK_EQUAL( &xfld     , &get<0>(get<0>(fldCellTuple3)) );
    BOOST_CHECK_EQUAL( &pfldCell1, &get<1>(get<0>(fldCellTuple3)) );
    BOOST_CHECK_EQUAL( &pfldCell2, &get<0>(get<1>(fldCellTuple3)) );
    BOOST_CHECK_EQUAL( &pfldCell3, &get<1>(get<1>(fldCellTuple3)) );
  }

  BOOST_CHECK_THROW( fldCellTuple3.getInteriorTraceGroup<Line>(0), AssertionException );
  BOOST_CHECK_THROW( fldCellTuple3.getBoundaryTraceGroup<Line>(0), AssertionException );
  const FieldTupleCell1::FieldCellGroupType<Triangle>& cellGroup1 = get<0>(fldCellTuple3).getCellGroup<Triangle>(0);
  const FieldTupleCell2::FieldCellGroupType<Triangle>& cellGroup2 = get<1>(fldCellTuple3).getCellGroup<Triangle>(0);
  const FieldTupleCell3::FieldCellGroupType<Triangle>& cellGroup3 = fldCellTuple3.getCellGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( &xfld.getCellGroup<Triangle>(0), &cellGroup1.fldL() );
  BOOST_CHECK_EQUAL( &pfldCell1.getCellGroup<Triangle>(0), &cellGroup1.fldR() );
  BOOST_CHECK_EQUAL( 2, cellGroup1.nElem() );

  BOOST_CHECK_EQUAL( &pfldCell2.getCellGroup<Triangle>(0), &cellGroup2.fldL() );
  BOOST_CHECK_EQUAL( &pfldCell3.getCellGroup<Triangle>(0), &cellGroup2.fldR() );
  BOOST_CHECK_EQUAL( 2, cellGroup2.nElem() );

  typedef FieldTupleCell1::FieldCellGroupType<Triangle>::ElementType<> ElementType1;
  typedef FieldTupleCell2::FieldCellGroupType<Triangle>::ElementType<> ElementType2;
  typedef FieldTupleCell3::FieldCellGroupType<Triangle>::ElementType<> ElementType3;
  ElementType1 cellElem1( cellGroup1.basis() );
  ElementType2 cellElem2( cellGroup2.basis() );
  ElementType3 cellElem3( cellGroup3.basis() );

  cellGroup1.getElement( cellElem1, 0 );
  cellGroup2.getElement( cellElem2, 0 );
  cellGroup3.getElement( cellElem3, 0 );

  // Left of Tuple 1 vs Left of Left of Tuple 3

  // compare xfld
  BOOST_REQUIRE_EQUAL( cellElem1.elemL().nDOF() , 3);
  BOOST_REQUIRE_EQUAL( cellElem3.elemL().elemL().nDOF() , 3);

  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(0)[0] , cellElem3.elemL().elemL().DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(0)[1] , cellElem3.elemL().elemL().DOF(0)[1] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(1)[0] , cellElem3.elemL().elemL().DOF(1)[0] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(1)[1] , cellElem3.elemL().elemL().DOF(1)[1] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(2)[0] , cellElem3.elemL().elemL().DOF(2)[0] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(2)[1] , cellElem3.elemL().elemL().DOF(2)[1] );

  // compare xfld
  BOOST_REQUIRE_EQUAL( cellElem1.elemL().nDOF() , 3);
  BOOST_REQUIRE_EQUAL( get<0>(cellElem3.elemL()).nDOF() , 3);

  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(0)[0] , get<0>(cellElem3.elemL()).DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(0)[1] , get<0>(cellElem3.elemL()).DOF(0)[1] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(1)[0] , get<0>(cellElem3.elemL()).DOF(1)[0] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(1)[1] , get<0>(cellElem3.elemL()).DOF(1)[1] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(2)[0] , get<0>(cellElem3.elemL()).DOF(2)[0] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(2)[1] , get<0>(cellElem3.elemL()).DOF(2)[1] );

  // compare xfld
  BOOST_REQUIRE_EQUAL( cellElem1.elemL().nDOF() , 3);
  BOOST_REQUIRE_EQUAL( get<0>(cellElem3).elemL().nDOF() , 3);

  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(0)[0] , get<0>(cellElem3).elemL().DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(0)[1] , get<0>(cellElem3).elemL().DOF(0)[1] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(1)[0] , get<0>(cellElem3).elemL().DOF(1)[0] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(1)[1] , get<0>(cellElem3).elemL().DOF(1)[1] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(2)[0] , get<0>(cellElem3).elemL().DOF(2)[0] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(2)[1] , get<0>(cellElem3).elemL().DOF(2)[1] );

  // compare xfld
  BOOST_REQUIRE_EQUAL( cellElem1.elemL().nDOF() , 3);
  BOOST_REQUIRE_EQUAL( get<0>(get<0>(cellElem3)).nDOF() , 3);

  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(0)[0] , get<0>(get<0>(cellElem3)).DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(0)[1] , get<0>(get<0>(cellElem3)).DOF(0)[1] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(1)[0] , get<0>(get<0>(cellElem3)).DOF(1)[0] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(1)[1] , get<0>(get<0>(cellElem3)).DOF(1)[1] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(2)[0] , get<0>(get<0>(cellElem3)).DOF(2)[0] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(2)[1] , get<0>(get<0>(cellElem3)).DOF(2)[1] );


  // Right of Tuple 1 vs Right of Left of Tuple 3

  // compare pfldCell1
  BOOST_REQUIRE_EQUAL( cellElem1.elemR().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( cellElem3.elemL().elemR().nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem1.elemR().DOF(0)[0] , cellElem3.elemL().elemR().DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem1.elemR().DOF(0)[1] , cellElem3.elemL().elemR().DOF(0)[1] );

  // compare pfldCell1
  BOOST_REQUIRE_EQUAL( cellElem1.elemR().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( get<1>(cellElem3.elemL()).nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem1.elemR().DOF(0)[0] , get<1>(cellElem3.elemL()).DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem1.elemR().DOF(0)[1] , get<1>(cellElem3.elemL()).DOF(0)[1] );

  // compare pfldCell1
  BOOST_REQUIRE_EQUAL( cellElem1.elemR().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( get<0>(cellElem3).elemR().nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem1.elemR().DOF(0)[0] , get<0>(cellElem3).elemR().DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem1.elemR().DOF(0)[1] , get<0>(cellElem3).elemR().DOF(0)[1] );

  // compare pfldCell1
  BOOST_REQUIRE_EQUAL( cellElem1.elemR().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( get<1>(get<0>(cellElem3)).nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem1.elemR().DOF(0)[0] , get<1>(get<0>(cellElem3)).DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem1.elemR().DOF(0)[1] , get<1>(get<0>(cellElem3)).DOF(0)[1] );


  // Left of Tuple 2 vs Left of Right of Tuple 3

  // compare pfldCell2
  BOOST_REQUIRE_EQUAL( cellElem2.elemL().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( cellElem3.elemR().elemL().nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem2.elemL().DOF(0)[0] , cellElem3.elemR().elemL().DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem2.elemL().DOF(0)[1] , cellElem3.elemR().elemL().DOF(0)[1] );

  // compare pfldCell2
  BOOST_REQUIRE_EQUAL( cellElem2.elemL().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( get<0>(cellElem3.elemR()).nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem2.elemL().DOF(0)[0] , get<0>(cellElem3.elemR()).DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem2.elemL().DOF(0)[1] , get<0>(cellElem3.elemR()).DOF(0)[1] );

  // compare pfldCell2
  BOOST_REQUIRE_EQUAL( cellElem2.elemL().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( get<1>(cellElem3).elemL().nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem2.elemL().DOF(0)[0] , get<1>(cellElem3).elemL().DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem2.elemL().DOF(0)[1] , get<1>(cellElem3).elemL().DOF(0)[1] );

  // compare pfldCell2
  BOOST_REQUIRE_EQUAL( cellElem2.elemL().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( get<0>(get<1>(cellElem3)).nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem2.elemL().DOF(0)[0] , get<0>(get<1>(cellElem3)).DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem2.elemL().DOF(0)[1] , get<0>(get<1>(cellElem3)).DOF(0)[1] );


  // Right of Tuple 2 vs Right of Right of Tuple 3

  // compare pfldCell3
  BOOST_REQUIRE_EQUAL( cellElem2.elemR().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( cellElem3.elemR().elemR().nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem2.elemR().DOF(0)[0] , cellElem3.elemR().elemR().DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem2.elemR().DOF(0)[1] , cellElem3.elemR().elemR().DOF(0)[1] );

  // compare pfldCell3
  BOOST_REQUIRE_EQUAL( cellElem2.elemR().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( get<1>(cellElem3.elemR()).nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem2.elemR().DOF(0)[0] , get<1>(cellElem3.elemR()).DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem2.elemR().DOF(0)[1] , get<1>(cellElem3.elemR()).DOF(0)[1] );

  // compare pfldCell3
  BOOST_REQUIRE_EQUAL( cellElem2.elemR().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( get<1>(cellElem3).elemR().nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem2.elemR().DOF(0)[0] , get<1>(cellElem3).elemR().DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem2.elemR().DOF(0)[1] , get<1>(cellElem3).elemR().DOF(0)[1] );

  // compare pfldCell3
  BOOST_REQUIRE_EQUAL( cellElem2.elemR().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( get<1>(get<1>(cellElem3)).nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem2.elemR().DOF(0)[0] , get<1>(get<1>(cellElem3)).DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem2.elemR().DOF(0)[1] , get<1>(get<1>(cellElem3)).DOF(0)[1] );


  ElementType1::T p1;
  ElementType2::T p2;
  ElementType3::T p3;

  cellElem1.eval({1./2.,1./2.}, p1);
  cellElem2.eval({1./2.,1./2.}, p2);
  cellElem3.eval({1./2.,1./2.}, p3);

  BOOST_CHECK_CLOSE( 0.5,  get<0>(get<0>(p3))[0], 1e-12 );
  BOOST_CHECK_CLOSE( 0.5,  get<0>(get<0>(p3))[1], 1e-12 );
  BOOST_CHECK_CLOSE( 3.14, get<1>(get<0>(p3))[0], 1e-12 );
  BOOST_CHECK_CLOSE( 3.14, get<1>(get<0>(p3))[1], 1e-12 );
  BOOST_CHECK_CLOSE( 6.28, get<0>(get<1>(p3))[0], 1e-12 );
  BOOST_CHECK_CLOSE( 6.28, get<0>(get<1>(p3))[1], 1e-12 );
  BOOST_CHECK_CLOSE( 1.62, get<1>(get<1>(p3))[0], 1e-12 );
  BOOST_CHECK_CLOSE( 1.62, get<1>(get<1>(p3))[1], 1e-12 );

  BOOST_CHECK_CLOSE( get<0>(p1)[0], get<0>(get<0>(p3))[0], 1e-12 );
  BOOST_CHECK_CLOSE( get<0>(p1)[1], get<0>(get<0>(p3))[1], 1e-12 );
  BOOST_CHECK_CLOSE( get<1>(p1)[0], get<1>(get<0>(p3))[0], 1e-12 );
  BOOST_CHECK_CLOSE( get<1>(p1)[1], get<1>(get<0>(p3))[1], 1e-12 );
  BOOST_CHECK_CLOSE( get<0>(p2)[0], get<0>(get<1>(p3))[0], 1e-12 );
  BOOST_CHECK_CLOSE( get<0>(p2)[1], get<0>(get<1>(p3))[1], 1e-12 );
  BOOST_CHECK_CLOSE( get<1>(p2)[0], get<1>(get<1>(p3))[0], 1e-12 );
  BOOST_CHECK_CLOSE( get<1>(p2)[1], get<1>(get<1>(p3))[1], 1e-12 );

  // Zero out for the next test
  p3 = 0;

  BOOST_CHECK_SMALL( get<0>(get<0>(p3))[0], 1e-12 );
  BOOST_CHECK_SMALL( get<0>(get<0>(p3))[1], 1e-12 );
  BOOST_CHECK_SMALL( get<0>(get<1>(p3))[0], 1e-12 );
  BOOST_CHECK_SMALL( get<0>(get<1>(p3))[1], 1e-12 );
  BOOST_CHECK_SMALL( get<1>(get<0>(p3))[0], 1e-12 );
  BOOST_CHECK_SMALL( get<1>(get<0>(p3))[1], 1e-12 );
  BOOST_CHECK_SMALL( get<1>(get<1>(p3))[0], 1e-12 );
  BOOST_CHECK_SMALL( get<1>(get<1>(p3))[1], 1e-12 );

#if 0 // Not sure if we will need this
  // Evaluate the basis at a point and then compute the parameter from the basis functions
  FieldTupleCell3::FieldCellGroupType<Triangle>::BasisType::BasisPointType phi(cellElem3.basis());

  cellElem3.evalBasis({1./2.,1./2.}, phi);
  cellElem3.evalFromBasis(phi, phi.size(), p3);

  BOOST_CHECK_CLOSE( 0.5,  get<0>(get<0>(p3))[0], 1e-12 );
  BOOST_CHECK_CLOSE( 0.5,  get<0>(get<0>(p3))[1], 1e-12 );
  BOOST_CHECK_CLOSE( 3.14, get<1>(get<0>(p3))[0], 1e-12 );
  BOOST_CHECK_CLOSE( 5.13, get<1>(get<0>(p3))[1], 1e-12 );
  BOOST_CHECK_CLOSE( 6.28, get<0>(get<1>(p3))[0], 1e-12 );
  BOOST_CHECK_CLOSE( 2.72, get<0>(get<1>(p3))[1], 1e-12 );
  BOOST_CHECK_CLOSE( 1.62, get<1>(get<1>(p3))[0], 1e-12 );
  BOOST_CHECK_CLOSE( 5.10, get<1>(get<1>(p3))[1], 1e-12 );
#endif
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( TripleDouble_FieldTuple_2Fld_Cell_test )
{
  typedef DLA::VectorS<2, Real> ArrayP;
  typedef Field_DG_Cell< PhysD2, TopoD2, ArrayP > PField2D_DG_Area;
  XField2D_2Triangle_X1_1Group xfld;

  int order = 0;
  PField2D_DG_Area pfldCell1(xfld, order, BasisFunctionCategory_Legendre);
  PField2D_DG_Area pfldCell2(xfld, order, BasisFunctionCategory_Legendre);
  PField2D_DG_Area pfldCell3(xfld, order, BasisFunctionCategory_Legendre);
  PField2D_DG_Area pfldCell4(xfld, order, BasisFunctionCategory_Legendre);
  PField2D_DG_Area pfldCell5(xfld, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 2, pfldCell1.nDOF() );
  pfldCell1.DOF(0) = 3.14;
  pfldCell1.DOF(1) = 5.13;

  BOOST_CHECK_EQUAL( 2, pfldCell2.nDOF() );
  pfldCell2.DOF(0) = 6.28;
  pfldCell2.DOF(1) = 2.72;

  BOOST_CHECK_EQUAL( 2, pfldCell3.nDOF() );
  pfldCell3.DOF(0) = 1.62;
  pfldCell3.DOF(1) = 5.10;

  BOOST_CHECK_EQUAL( 2, pfldCell4.nDOF() );
  pfldCell4.DOF(0) = 2*1.62;
  pfldCell4.DOF(1) = 2*5.10;

  BOOST_CHECK_EQUAL( 2, pfldCell5.nDOF() );
  pfldCell5.DOF(0) = -1*1.62;
  pfldCell5.DOF(1) = -1*5.10;

  typedef FieldTuple< XField<PhysD2, TopoD2>, Field<PhysD2, TopoD2, ArrayP>, TupleClass<0> > FieldTupleCell1; // double tuple type 1 - tier 0
  typedef FieldTuple< Field<PhysD2, TopoD2, ArrayP>, Field<PhysD2, TopoD2, ArrayP>, TupleClass<0> > FieldTupleCell2; // double tuple type 2 - tier 0
  typedef FieldTuple< FieldTupleCell1, FieldTupleCell2, TupleClass<1> > FieldTupleCell3; // double tuple - tier 1
  typedef FieldTuple< FieldTupleCell3, FieldTupleCell2, TupleClass<1> > FieldTupleCell4; // triple tuple - tier 1

  FieldTupleCell4 fldCellTuple4 = ((xfld, pfldCell1), (pfldCell2, pfldCell3), (pfldCell4,pfldCell5));

  BOOST_CHECK_EQUAL( &xfld     , &fldCellTuple4.getXField() );
  BOOST_CHECK_EQUAL( &xfld     , &get<0>(get<0>(fldCellTuple4)) );
  BOOST_CHECK_EQUAL( &pfldCell1, &get<1>(get<0>(fldCellTuple4)) );
  BOOST_CHECK_EQUAL( &pfldCell2, &get<0>(get<1>(fldCellTuple4)) );
  BOOST_CHECK_EQUAL( &pfldCell3, &get<1>(get<1>(fldCellTuple4)) );
  BOOST_CHECK_EQUAL( &pfldCell4, &get<0>(get<2>(fldCellTuple4)) );
  BOOST_CHECK_EQUAL( &pfldCell5, &get<1>(get<2>(fldCellTuple4)) );

  FieldTupleCell4 fldCellTuple4Copy(fldCellTuple4);

  BOOST_CHECK_EQUAL( &xfld     , &fldCellTuple4Copy.getXField() );
  BOOST_CHECK_EQUAL( &xfld     , &get<0>(get<0>(fldCellTuple4Copy)) );
  BOOST_CHECK_EQUAL( &pfldCell1, &get<1>(get<0>(fldCellTuple4Copy)) );
  BOOST_CHECK_EQUAL( &pfldCell2, &get<0>(get<1>(fldCellTuple4Copy)) );
  BOOST_CHECK_EQUAL( &pfldCell3, &get<1>(get<1>(fldCellTuple4Copy)) );
  BOOST_CHECK_EQUAL( &pfldCell4, &get<0>(get<2>(fldCellTuple4Copy)) );
  BOOST_CHECK_EQUAL( &pfldCell5, &get<1>(get<2>(fldCellTuple4Copy)) );

  BOOST_CHECK_THROW( fldCellTuple4.nInteriorTraceGroups(), AssertionException );
  BOOST_CHECK_THROW( fldCellTuple4.nBoundaryTraceGroups(), AssertionException );
  BOOST_CHECK_EQUAL( 1, fldCellTuple4.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld, &get<0>(fldCellTuple4).getXField() );

  BOOST_CHECK_THROW( fldCellTuple4.getInteriorTraceGroupBase(0), AssertionException );
  BOOST_CHECK_THROW( fldCellTuple4.getBoundaryTraceGroupBase(0), AssertionException );
  BOOST_CHECK( typeid(Triangle) == fldCellTuple4.getCellGroupBase(0).topoTypeID() );

  BOOST_CHECK_EQUAL( xfld.getCellGroupBase(0).nElem() , fldCellTuple4.getCellGroupBase(0).nElem() );


  BOOST_CHECK_THROW( fldCellTuple4.getInteriorTraceGroup<Line>(0), AssertionException );
  BOOST_CHECK_THROW( fldCellTuple4.getBoundaryTraceGroup<Line>(0), AssertionException );
  const FieldTupleCell1::FieldCellGroupType<Triangle>& cellGroup1 = get<0>(fldCellTuple4).getCellGroup<Triangle>(0); // first double tuple
  const FieldTupleCell2::FieldCellGroupType<Triangle>& cellGroup2 = get<1>(fldCellTuple4).getCellGroup<Triangle>(0); // second double tuple
  const FieldTupleCell2::FieldCellGroupType<Triangle>& cellGroup3 = get<2>(fldCellTuple4).getCellGroup<Triangle>(0); // third double tuple
  const FieldTupleCell4::FieldCellGroupType<Triangle>& cellGroup4 = fldCellTuple4.getCellGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( &xfld.getCellGroup<Triangle>(0), &cellGroup1.fldL() );
  BOOST_CHECK_EQUAL( &pfldCell1.getCellGroup<Triangle>(0), &cellGroup1.fldR() );
  BOOST_CHECK_EQUAL( 2, cellGroup1.nElem() );

  BOOST_CHECK_EQUAL( &pfldCell2.getCellGroup<Triangle>(0), &cellGroup2.fldL() );
  BOOST_CHECK_EQUAL( &pfldCell3.getCellGroup<Triangle>(0), &cellGroup2.fldR() );
  BOOST_CHECK_EQUAL( 2, cellGroup2.nElem() );

  BOOST_CHECK_EQUAL( &pfldCell4.getCellGroup<Triangle>(0), &cellGroup3.fldL() );
  BOOST_CHECK_EQUAL( &pfldCell5.getCellGroup<Triangle>(0), &cellGroup3.fldR() );
  BOOST_CHECK_EQUAL( 2, cellGroup3.nElem() );

  typedef FieldTupleCell1::FieldCellGroupType<Triangle>::ElementType<> ElementType1;
  typedef FieldTupleCell2::FieldCellGroupType<Triangle>::ElementType<> ElementType2;
  //typedef FieldTupleCell3::FieldCellGroupType<Triangle>::ElementType<> ElementType3;
  typedef FieldTupleCell4::FieldCellGroupType<Triangle>::ElementType<> ElementType4;

  ElementType1 cellElem1( cellGroup1.basis() );
  ElementType2 cellElem2( cellGroup2.basis() );
  ElementType2 cellElem3( cellGroup3.basis() );
  ElementType4 cellElem4( cellGroup4.basis() );

  cellGroup1.getElement( cellElem1, 0 );
  cellGroup2.getElement( cellElem2, 0 );
  cellGroup3.getElement( cellElem3, 0 );
  cellGroup4.getElement( cellElem4, 0 );

  // Tuple4 = ( Tuple1, Tuple2, Tuple3 )

  // Left of Tuple 1 vs Left of First of Tuple 4
  // compare xfld
  BOOST_REQUIRE_EQUAL( cellElem1.elemL().nDOF() , 3);
  BOOST_REQUIRE_EQUAL( cellElem4.elemL().elemL().elemL().nDOF() , 3);

  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(0)[0] , cellElem4.elemL().elemL().elemL().DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(0)[1] , cellElem4.elemL().elemL().elemL().DOF(0)[1] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(1)[0] , cellElem4.elemL().elemL().elemL().DOF(1)[0] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(1)[1] , cellElem4.elemL().elemL().elemL().DOF(1)[1] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(2)[0] , cellElem4.elemL().elemL().elemL().DOF(2)[0] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(2)[1] , cellElem4.elemL().elemL().elemL().DOF(2)[1] );

  // compare xfld
  BOOST_REQUIRE_EQUAL( cellElem1.elemL().nDOF() , 3);
  BOOST_REQUIRE_EQUAL( get<0>(cellElem4.elemL().elemL()).nDOF() , 3);

  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(0)[0] , get<0>(cellElem4.elemL().elemL()).DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(0)[1] , get<0>(cellElem4.elemL().elemL()).DOF(0)[1] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(1)[0] , get<0>(cellElem4.elemL().elemL()).DOF(1)[0] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(1)[1] , get<0>(cellElem4.elemL().elemL()).DOF(1)[1] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(2)[0] , get<0>(cellElem4.elemL().elemL()).DOF(2)[0] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(2)[1] , get<0>(cellElem4.elemL().elemL()).DOF(2)[1] );

  // compare xfld
  BOOST_REQUIRE_EQUAL( cellElem1.elemL().nDOF() , 3);
  BOOST_REQUIRE_EQUAL( get<0>(cellElem4).elemL().nDOF() , 3);

  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(0)[0] , get<0>(cellElem4).elemL().DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(0)[1] , get<0>(cellElem4).elemL().DOF(0)[1] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(1)[0] , get<0>(cellElem4).elemL().DOF(1)[0] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(1)[1] , get<0>(cellElem4).elemL().DOF(1)[1] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(2)[0] , get<0>(cellElem4).elemL().DOF(2)[0] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(2)[1] , get<0>(cellElem4).elemL().DOF(2)[1] );

  // compare xfld
  BOOST_REQUIRE_EQUAL( cellElem1.elemL().nDOF() , 3);
  BOOST_REQUIRE_EQUAL( get<0>(get<0>(cellElem4)).nDOF() , 3);

  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(0)[0] , get<0>(get<0>(cellElem4)).DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(0)[1] , get<0>(get<0>(cellElem4)).DOF(0)[1] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(1)[0] , get<0>(get<0>(cellElem4)).DOF(1)[0] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(1)[1] , get<0>(get<0>(cellElem4)).DOF(1)[1] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(2)[0] , get<0>(get<0>(cellElem4)).DOF(2)[0] );
  BOOST_CHECK_EQUAL( cellElem1.elemL().DOF(2)[1] , get<0>(get<0>(cellElem4)).DOF(2)[1] );


  // Right of Tuple 1 vs Right of First of Tuple 4

  // compare pfldCell1
  BOOST_REQUIRE_EQUAL( cellElem1.elemR().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( cellElem4.elemL().elemL().elemR().nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem1.elemR().DOF(0)[0] , cellElem4.elemL().elemL().elemR().DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem1.elemR().DOF(0)[1] , cellElem4.elemL().elemL().elemR().DOF(0)[1] );

  // compare pfldCell1
  BOOST_REQUIRE_EQUAL( cellElem1.elemR().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( get<1>(cellElem4.elemL().elemL()).nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem1.elemR().DOF(0)[0] , get<1>(cellElem4.elemL().elemL()).DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem1.elemR().DOF(0)[1] , get<1>(cellElem4.elemL().elemL()).DOF(0)[1] );

  // compare pfldCell1
  BOOST_REQUIRE_EQUAL( cellElem1.elemR().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( get<0>(cellElem4).elemR().nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem1.elemR().DOF(0)[0] , get<0>(cellElem4).elemR().DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem1.elemR().DOF(0)[1] , get<0>(cellElem4).elemR().DOF(0)[1] );

  // compare pfldCell1
  BOOST_REQUIRE_EQUAL( cellElem1.elemR().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( get<1>(get<0>(cellElem4)).nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem1.elemR().DOF(0)[0] , get<1>(get<0>(cellElem4)).DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem1.elemR().DOF(0)[1] , get<1>(get<0>(cellElem4)).DOF(0)[1] );


  // Left of Tuple 2 vs Left of Second of Tuple 4
  // compare pfldCell2
  BOOST_REQUIRE_EQUAL( cellElem2.elemL().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( cellElem4.elemR().elemL().nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem2.elemL().DOF(0)[0] , cellElem4.elemL().elemR().elemL().DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem2.elemL().DOF(0)[1] , cellElem4.elemL().elemR().elemL().DOF(0)[1] );

  // compare pfldCell2
  BOOST_REQUIRE_EQUAL( cellElem2.elemL().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( get<0>(cellElem4.elemR()).nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem2.elemL().DOF(0)[0] , get<0>(cellElem4.elemL().elemR()).DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem2.elemL().DOF(0)[1] , get<0>(cellElem4.elemL().elemR()).DOF(0)[1] );

  // compare pfldCell2
  BOOST_REQUIRE_EQUAL( cellElem2.elemL().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( get<1>(cellElem4).elemL().nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem2.elemL().DOF(0)[0] , get<1>(cellElem4).elemL().DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem2.elemL().DOF(0)[1] , get<1>(cellElem4).elemL().DOF(0)[1] );

  // compare pfldCell2
  BOOST_REQUIRE_EQUAL( cellElem2.elemL().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( get<0>(get<1>(cellElem4)).nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem2.elemL().DOF(0)[0] , get<0>(get<1>(cellElem4)).DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem2.elemL().DOF(0)[1] , get<0>(get<1>(cellElem4)).DOF(0)[1] );


  // Right of Tuple 2 vs Right of Second of Tuple 4
  // compare pfldCell3
  BOOST_REQUIRE_EQUAL( cellElem2.elemR().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( cellElem4.elemR().elemR().nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem2.elemR().DOF(0)[0] , cellElem4.elemL().elemR().elemR().DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem2.elemR().DOF(0)[1] , cellElem4.elemL().elemR().elemR().DOF(0)[1] );

  // compare pfldCell3
  BOOST_REQUIRE_EQUAL( cellElem2.elemR().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( get<1>(cellElem4.elemR()).nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem2.elemR().DOF(0)[0] , get<1>(cellElem4.elemL().elemR()).DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem2.elemR().DOF(0)[1] , get<1>(cellElem4.elemL().elemR()).DOF(0)[1] );

  // compare pfldCell3
  BOOST_REQUIRE_EQUAL( cellElem2.elemR().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( get<1>(cellElem4).elemR().nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem2.elemR().DOF(0)[0] , get<1>(cellElem4).elemR().DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem2.elemR().DOF(0)[1] , get<1>(cellElem4).elemR().DOF(0)[1] );

  // compare pfldCell3
  BOOST_REQUIRE_EQUAL( cellElem2.elemR().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( get<1>(get<1>(cellElem4)).nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem2.elemR().DOF(0)[0] , get<1>(get<1>(cellElem4)).DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem2.elemR().DOF(0)[1] , get<1>(get<1>(cellElem4)).DOF(0)[1] );


  // Left of Tuple 3 vs Left of Third of Tuple 4
  // compare pfldCell2
  BOOST_REQUIRE_EQUAL( cellElem3.elemL().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( cellElem4.elemR().elemL().nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem3.elemL().DOF(0)[0] , cellElem4.elemR().elemL().DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem3.elemL().DOF(0)[1] , cellElem4.elemR().elemL().DOF(0)[1] );

  // compare pfldCell2
  BOOST_REQUIRE_EQUAL( cellElem3.elemL().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( get<0>(cellElem4.elemR()).nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem3.elemL().DOF(0)[0] , get<0>(cellElem4.elemR()).DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem3.elemL().DOF(0)[1] , get<0>(cellElem4.elemR()).DOF(0)[1] );

  // compare pfldCell2
  BOOST_REQUIRE_EQUAL( cellElem3.elemL().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( get<1>(cellElem4).elemL().nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem3.elemL().DOF(0)[0] , get<2>(cellElem4).elemL().DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem3.elemL().DOF(0)[1] , get<2>(cellElem4).elemL().DOF(0)[1] );

  // compare pfldCell2
  BOOST_REQUIRE_EQUAL( cellElem3.elemL().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( get<0>(get<1>(cellElem4)).nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem3.elemL().DOF(0)[0] , get<0>(get<2>(cellElem4)).DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem3.elemL().DOF(0)[1] , get<0>(get<2>(cellElem4)).DOF(0)[1] );


  // Right of Tuple 2 vs Right of Third of Tuple 4

  // compare pfldCell3
  BOOST_REQUIRE_EQUAL( cellElem3.elemR().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( cellElem4.elemR().elemR().nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem3.elemR().DOF(0)[0] , cellElem4.elemR().elemR().DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem3.elemR().DOF(0)[1] , cellElem4.elemR().elemR().DOF(0)[1] );

  // compare pfldCell3
  BOOST_REQUIRE_EQUAL( cellElem3.elemR().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( get<1>(cellElem4.elemR()).nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem3.elemR().DOF(0)[0] , get<1>(cellElem4.elemR()).DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem3.elemR().DOF(0)[1] , get<1>(cellElem4.elemR()).DOF(0)[1] );

  // compare pfldCell3
  BOOST_REQUIRE_EQUAL( cellElem3.elemR().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( get<1>(cellElem4).elemR().nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem3.elemR().DOF(0)[0] , get<2>(cellElem4).elemR().DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem3.elemR().DOF(0)[1] , get<2>(cellElem4).elemR().DOF(0)[1] );

  // compare pfldCell3
  BOOST_REQUIRE_EQUAL( cellElem3.elemR().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( get<1>(get<1>(cellElem4)).nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem3.elemR().DOF(0)[0] , get<1>(get<2>(cellElem4)).DOF(0)[0] );
  BOOST_CHECK_EQUAL( cellElem3.elemR().DOF(0)[1] , get<1>(get<2>(cellElem4)).DOF(0)[1] );


  ElementType1::T p1;
  ElementType2::T p2;
  ElementType2::T p3;
  ElementType4::T p4;

  cellElem1.eval({1./2.,1./2.}, p1);
  cellElem2.eval({1./2.,1./2.}, p2);
  cellElem3.eval({1./2.,1./2.}, p3);
  cellElem4.eval({1./2.,1./2.}, p4);

  // The elems are VectorS<2,Real> thus the scalar assign earlier is setting both values
  BOOST_CHECK_CLOSE(   0.5,  get<0>(get<0>(p4))[0], 1e-12 );
  BOOST_CHECK_CLOSE(   0.5,  get<0>(get<0>(p4))[1], 1e-12 );
  BOOST_CHECK_CLOSE(   3.14, get<1>(get<0>(p4))[0], 1e-12 );
  BOOST_CHECK_CLOSE(   3.14, get<1>(get<0>(p4))[1], 1e-12 );
  BOOST_CHECK_CLOSE(   6.28, get<0>(get<1>(p4))[0], 1e-12 );
  BOOST_CHECK_CLOSE(   6.28, get<0>(get<1>(p4))[1], 1e-12 );
  BOOST_CHECK_CLOSE(   1.62, get<1>(get<1>(p4))[0], 1e-12 );
  BOOST_CHECK_CLOSE(   1.62, get<1>(get<1>(p4))[1], 1e-12 );
  BOOST_CHECK_CLOSE( 2*1.62, get<0>(get<2>(p4))[0], 1e-12 );
  BOOST_CHECK_CLOSE( 2*1.62, get<0>(get<2>(p4))[1], 1e-12 );
  BOOST_CHECK_CLOSE(-1*1.62, get<1>(get<2>(p4))[0], 1e-12 );
  BOOST_CHECK_CLOSE(-1*1.62, get<1>(get<2>(p4))[1], 1e-12 );

  BOOST_CHECK_CLOSE( get<0>(p1)[0], get<0>(get<0>(p4))[0], 1e-12 );
  BOOST_CHECK_CLOSE( get<0>(p1)[1], get<0>(get<0>(p4))[1], 1e-12 );
  BOOST_CHECK_CLOSE( get<1>(p1)[0], get<1>(get<0>(p4))[0], 1e-12 );
  BOOST_CHECK_CLOSE( get<1>(p1)[1], get<1>(get<0>(p4))[1], 1e-12 );
  BOOST_CHECK_CLOSE( get<0>(p2)[0], get<0>(get<1>(p4))[0], 1e-12 );
  BOOST_CHECK_CLOSE( get<0>(p2)[1], get<0>(get<1>(p4))[1], 1e-12 );
  BOOST_CHECK_CLOSE( get<1>(p2)[0], get<1>(get<1>(p4))[0], 1e-12 );
  BOOST_CHECK_CLOSE( get<1>(p2)[1], get<1>(get<1>(p4))[1], 1e-12 );
  BOOST_CHECK_CLOSE( get<0>(p3)[0], get<0>(get<2>(p4))[0], 1e-12 );
  BOOST_CHECK_CLOSE( get<0>(p3)[1], get<0>(get<2>(p4))[1], 1e-12 );
  BOOST_CHECK_CLOSE( get<1>(p3)[0], get<1>(get<2>(p4))[0], 1e-12 );
  BOOST_CHECK_CLOSE( get<1>(p3)[1], get<1>(get<2>(p4))[1], 1e-12 );

  // Zero out for the next test
  p4 = 0;

  BOOST_CHECK_SMALL( get<0>(get<0>(p4))[0], 1e-12 );
  BOOST_CHECK_SMALL( get<0>(get<0>(p4))[1], 1e-12 );
  BOOST_CHECK_SMALL( get<1>(get<0>(p4))[0], 1e-12 );
  BOOST_CHECK_SMALL( get<1>(get<0>(p4))[1], 1e-12 );
  BOOST_CHECK_SMALL( get<0>(get<1>(p4))[0], 1e-12 );
  BOOST_CHECK_SMALL( get<0>(get<1>(p4))[1], 1e-12 );
  BOOST_CHECK_SMALL( get<1>(get<1>(p4))[0], 1e-12 );
  BOOST_CHECK_SMALL( get<1>(get<1>(p4))[1], 1e-12 );
  BOOST_CHECK_SMALL( get<0>(get<2>(p4))[0], 1e-12 );
  BOOST_CHECK_SMALL( get<0>(get<2>(p4))[1], 1e-12 );
  BOOST_CHECK_SMALL( get<1>(get<2>(p4))[0], 1e-12 );
  BOOST_CHECK_SMALL( get<1>(get<2>(p4))[1], 1e-12 );

#if 0 // Not sure if we will need this
  // Evaluate the basis at a point and then compute the parameter from the basis functions
  FieldTupleCell4::FieldCellGroupType<Triangle>::BasisType::BasisPointType phi(cellElem4.basis());

  cellElem4.evalBasis({1./2.,1./2.}, phi);
  cellElem4.evalFromBasis(phi, phi.size(), p4);
 // Why is this different evaluation to above?
  BOOST_CHECK_CLOSE( 0.5,  get<0>(get<0>(p4))[0], 1e-12 );
  BOOST_CHECK_CLOSE( 0.5,  get<0>(get<0>(p4))[1], 1e-12 );
  BOOST_CHECK_CLOSE( 3.14, get<1>(get<0>(p4))[0], 1e-12 );
  BOOST_CHECK_CLOSE( 5.13, get<1>(get<0>(p4))[1], 1e-12 );
  BOOST_CHECK_CLOSE( 6.28, get<0>(get<1>(p4))[0], 1e-12 );
  BOOST_CHECK_CLOSE( 2.72, get<0>(get<1>(p4))[1], 1e-12 );
  BOOST_CHECK_CLOSE( 1.62, get<1>(get<1>(p4))[0], 1e-12 );
  BOOST_CHECK_CLOSE( 5.10, get<1>(get<1>(p4))[1], 1e-12 );
#endif
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldTuple_2Fld_InteriorTrace_test )
{
  typedef DLA::VectorS<2, Real> ArrayP;
  typedef Field_DG_InteriorTrace< PhysD2, TopoD2, ArrayP > PField2D_DG_InteriorTrace;
  typedef FieldTuple< XField<PhysD2, TopoD2>, Field<PhysD2, TopoD2, ArrayP>, TupleClass<> > FieldTupleITrace;

  XField2D_2Triangle_X1_1Group xfld;

  int order = 0;
  PField2D_DG_InteriorTrace pfldI(xfld, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 1, pfldI.nDOF() );
  pfldI.DOF(0) = 3.14;

  FieldTupleITrace fldITraceTuple = (xfld, pfldI);

  BOOST_CHECK_EQUAL( &xfld, &get<0>(fldITraceTuple) );
  BOOST_CHECK_EQUAL( &pfldI, &get<1>(fldITraceTuple) );

  FieldTupleITrace fldITraceTupleCopy(fldITraceTuple);

  BOOST_CHECK_EQUAL( &xfld, &get<0>(fldITraceTupleCopy) );
  BOOST_CHECK_EQUAL( &pfldI, &get<1>(fldITraceTupleCopy) );

  testTupleParenOperator( (xfld, pfldI), xfld, pfldI );


  BOOST_CHECK_EQUAL( 1, fldITraceTuple.nInteriorTraceGroups() );
  BOOST_CHECK_THROW( fldITraceTuple.nBoundaryTraceGroups(), AssertionException );
  BOOST_CHECK_THROW( fldITraceTuple.nCellGroups(), AssertionException );
  BOOST_CHECK_EQUAL( &xfld, &fldITraceTuple.getXField() );


  BOOST_CHECK( typeid(Line) == fldITraceTuple.getInteriorTraceGroupBase(0).topoTypeID() );
  BOOST_CHECK_THROW( fldITraceTuple.getBoundaryTraceGroupBase(0), AssertionException );
  BOOST_CHECK_THROW( fldITraceTuple.getCellGroupBase(0), AssertionException );

  BOOST_CHECK_EQUAL( xfld.getInteriorTraceGroupBase(0).nElem() , fldITraceTuple.getInteriorTraceGroupBase(0).nElem() );


  const FieldTupleITrace::FieldTraceGroupType<Line>& ITraceGroup = fldITraceTuple.getInteriorTraceGroup<Line>(0);
  BOOST_CHECK_THROW( fldITraceTuple.getBoundaryTraceGroup<Line>(0), AssertionException );
  BOOST_CHECK_THROW( fldITraceTuple.getCellGroup<Triangle>(0), AssertionException );

  BOOST_CHECK_EQUAL( &xfld.getInteriorTraceGroup<Line>(0), &ITraceGroup.fldL() );
  BOOST_CHECK_EQUAL( &pfldI.getInteriorTraceGroup<Line>(0), &ITraceGroup.fldR() );
  BOOST_CHECK_EQUAL( 1, ITraceGroup.nElem() );

  BOOST_CHECK_EQUAL( &xfld.getInteriorTraceGroup<Line>(0), &get<0>(ITraceGroup) );
  BOOST_CHECK_EQUAL( &pfldI.getInteriorTraceGroup<Line>(0), &get<1>(ITraceGroup) );

  typedef FieldTupleITrace::FieldTraceGroupType<Line>::ElementType<> ElementType;
  ElementType traceElem( ITraceGroup.basis() );

  ITraceGroup.getElement( traceElem, 0 );

  BOOST_CHECK_EQUAL( 1, traceElem.elemL().DOF(0)[0] );
  BOOST_CHECK_EQUAL( 0, traceElem.elemL().DOF(0)[1] );

  BOOST_CHECK_EQUAL( 0, traceElem.elemL().DOF(1)[0] );
  BOOST_CHECK_EQUAL( 1, traceElem.elemL().DOF(1)[1] );


  BOOST_CHECK_EQUAL( 1, get<0>(traceElem).DOF(0)[0] );
  BOOST_CHECK_EQUAL( 0, get<0>(traceElem).DOF(0)[1] );

  BOOST_CHECK_EQUAL( 0, get<0>(traceElem).DOF(1)[0] );
  BOOST_CHECK_EQUAL( 1, get<0>(traceElem).DOF(1)[1] );


  BOOST_CHECK_CLOSE( 3.14, traceElem.elemR().DOF(0)[0], 1e-12 );

  BOOST_CHECK_CLOSE( 3.14, get<1>(traceElem).DOF(0)[0], 1e-12 );


  ElementType::T p;

  traceElem.eval({1./2.}, p);

  BOOST_CHECK_CLOSE( 0.5, get<0>(p)[0], 1e-12 );
  BOOST_CHECK_CLOSE( 0.5, get<0>(p)[1], 1e-12 );

  BOOST_CHECK_CLOSE( 3.14, get<1>(p)[0], 1e-12 );

  // Zero out for the next test
  p = 0;

  BOOST_CHECK_SMALL( get<0>(p)[0], 1e-12 );
  BOOST_CHECK_SMALL( get<0>(p)[1], 1e-12 );
  BOOST_CHECK_SMALL( get<1>(p)[0], 1e-12 );

  // Evaluate the basis at a point and then compute the parameter from the basis functions
  FieldTupleITrace::FieldTraceGroupType<Line>::BasisType::BasisPointType phi(traceElem.basis());

  traceElem.evalBasis({1./2.}, phi);
  traceElem.evalFromBasis(phi, phi.size(), p);

  BOOST_CHECK_CLOSE( 0.5, get<0>(p)[0], 1e-12 );
  BOOST_CHECK_CLOSE( 0.5, get<0>(p)[1], 1e-12 );

  BOOST_CHECK_CLOSE( 3.14, get<1>(p)[0], 1e-12 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldTuple_2Fld_BoundaryTrace_test )
{
  typedef DLA::VectorS<2, Real> ArrayP;
  typedef Field_DG_BoundaryTrace< PhysD2, TopoD2, ArrayP > PField2D_DG_BoundaryTrace;
  typedef FieldTuple< XField<PhysD2, TopoD2>, Field<PhysD2, TopoD2, ArrayP>, TupleClass<> > FieldTupleBTrace;

  XField2D_2Triangle_X1_1Group xfld;

  int order = 0;
  PField2D_DG_BoundaryTrace pfldB(xfld, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 4, pfldB.nDOF() );
  pfldB.DOF(0) = 3.14;
  pfldB.DOF(1) = 5.13;
  pfldB.DOF(2) = 6.17;
  pfldB.DOF(3) = 5.08;

  FieldTupleBTrace fldBTraceTuple = (xfld, pfldB);

  BOOST_CHECK_EQUAL( &xfld, &get<0>(fldBTraceTuple) );
  BOOST_CHECK_EQUAL( &pfldB, &get<1>(fldBTraceTuple) );

  FieldTupleBTrace fldBTraceTupleCopy(xfld, pfldB);

  BOOST_CHECK_EQUAL( &xfld, &get<0>(fldBTraceTupleCopy) );
  BOOST_CHECK_EQUAL( &pfldB, &get<1>(fldBTraceTupleCopy) );

  testTupleParenOperator( (xfld, pfldB), xfld, pfldB );


  BOOST_CHECK_THROW( fldBTraceTuple.nInteriorTraceGroups(), AssertionException );
  BOOST_CHECK_EQUAL( 1, fldBTraceTuple.nBoundaryTraceGroups() );
  BOOST_CHECK_THROW( fldBTraceTuple.nCellGroups(), AssertionException );
  BOOST_CHECK_EQUAL( &xfld, &fldBTraceTuple.getXField() );


  BOOST_CHECK_THROW( fldBTraceTuple.getInteriorTraceGroupBase(0), AssertionException );
  BOOST_CHECK( typeid(Line) == fldBTraceTuple.getBoundaryTraceGroupBase(0).topoTypeID() );
  BOOST_CHECK_THROW( fldBTraceTuple.getCellGroupBase(0), AssertionException );

  BOOST_CHECK_EQUAL( xfld.getBoundaryTraceGroupBase(0).nElem() , fldBTraceTuple.getBoundaryTraceGroupBase(0).nElem() );


  BOOST_CHECK_THROW( fldBTraceTuple.getInteriorTraceGroup<Line>(0), AssertionException );
  const FieldTupleBTrace::FieldTraceGroupType<Line>& BTraceGroup = fldBTraceTuple.getBoundaryTraceGroup<Line>(0);
  BOOST_CHECK_THROW( fldBTraceTuple.getCellGroup<Triangle>(0), AssertionException );

  BOOST_CHECK_EQUAL( &xfld.getBoundaryTraceGroup<Line>(0), &BTraceGroup.fldL() );
  BOOST_CHECK_EQUAL( &pfldB.getBoundaryTraceGroup<Line>(0), &BTraceGroup.fldR() );
  BOOST_CHECK_EQUAL( 4, BTraceGroup.nElem() );

  BOOST_CHECK_EQUAL( &xfld.getBoundaryTraceGroup<Line>(0), &get<0>(BTraceGroup) );
  BOOST_CHECK_EQUAL( &pfldB.getBoundaryTraceGroup<Line>(0), &get<1>(BTraceGroup) );


  typedef FieldTupleBTrace::FieldTraceGroupType<Line>::ElementType<> ElementType;
  ElementType traceElem( BTraceGroup.basis() );

  BTraceGroup.getElement( traceElem, 0 );

  BOOST_CHECK_EQUAL( 0, traceElem.elemL().DOF(0)[0] );
  BOOST_CHECK_EQUAL( 0, traceElem.elemL().DOF(0)[1] );

  BOOST_CHECK_EQUAL( 1, traceElem.elemL().DOF(1)[0] );
  BOOST_CHECK_EQUAL( 0, traceElem.elemL().DOF(1)[1] );


  BOOST_CHECK_EQUAL( 0, get<0>(traceElem).DOF(0)[0] );
  BOOST_CHECK_EQUAL( 0, get<0>(traceElem).DOF(0)[1] );

  BOOST_CHECK_EQUAL( 1, get<0>(traceElem).DOF(1)[0] );
  BOOST_CHECK_EQUAL( 0, get<0>(traceElem).DOF(1)[1] );


  BOOST_CHECK_CLOSE( 3.14, traceElem.elemR().DOF(0)[0], 1e-12 );

  BOOST_CHECK_CLOSE( 3.14, get<1>(traceElem).DOF(0)[0], 1e-12 );


  ElementType::T p;

  traceElem.eval({1./2.}, p);

  BOOST_CHECK_CLOSE( 0.5, get<0>(p)[0], 1e-12 );
  BOOST_CHECK_SMALL(      get<0>(p)[1], 1e-12 );

  BOOST_CHECK_CLOSE( 3.14, get<1>(p)[0], 1e-12 );

  // Zero out for the next test
  p = 0;

  BOOST_CHECK_SMALL( get<0>(p)[0], 1e-12 );
  BOOST_CHECK_SMALL( get<0>(p)[1], 1e-12 );
  BOOST_CHECK_SMALL( get<1>(p)[0], 1e-12 );

  // Evaluate the basis at a point and then compute the parameter from the basis functions
  FieldTupleBTrace::FieldTraceGroupType<Line>::BasisType::BasisPointType phi(traceElem.basis());

  traceElem.evalBasis({1./2.}, phi);
  traceElem.evalFromBasis(phi, phi.size(), p);

  BOOST_CHECK_CLOSE( 0.5, get<0>(p)[0], 1e-12 );
  BOOST_CHECK_SMALL(      get<0>(p)[1], 1e-12 );

  BOOST_CHECK_CLOSE( 3.14, get<1>(p)[0], 1e-12 );
}

template<class L, class R, class Fld0, class Fld1, class Fld2>
void testTupleParenOperator( const FieldTuple< L, R, TupleClass<> >& tuple, const Fld0& fld0, const Fld1& fld1, const Fld2& fld2)
{
  BOOST_CHECK_EQUAL( &fld0, &get<0>(tuple) );
  BOOST_CHECK_EQUAL( &fld1, &get<1>(tuple) );
  BOOST_CHECK_EQUAL( &fld2, &get<2>(tuple) );
  BOOST_CHECK( &tuple.getXField() == &fld0.getXField() );
  BOOST_CHECK( &tuple.getXField() == &fld1.getXField() );
  BOOST_CHECK( &tuple.getXField() == &fld2.getXField() );
}

// This can be used to show what the two types that should be the same actually are
//#define TEMPLATE_ERROR_MESSAGE
#ifdef TEMPLATE_ERROR_MESSAGE
template<class , class >
struct ErrorMsg;

template<class T>
struct ErrorMsg<T, T> {};
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldTuple_3Fld_Cell_test )
{
  typedef DLA::VectorS<2, Real> ArrayP;
  typedef Field_DG_Cell< PhysD2, TopoD2, ArrayP > PField2D_DG_Area;

  XField2D_2Triangle_X1_1Group xfld;

  int order = 0;
  PField2D_DG_Area pfldCell1(xfld, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 2, pfldCell1.nDOF() );
  pfldCell1.DOF(0) = 3.14;
  pfldCell1.DOF(1) = 5.13;

  PField2D_DG_Area pfldCell2(xfld, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 2, pfldCell2.nDOF() );
  pfldCell2.DOF(0) = 2*3.14;
  pfldCell2.DOF(1) = 2*5.13;

  typedef FieldTuple< XField<PhysD2, TopoD2>, Field<PhysD2, TopoD2, ArrayP>, TupleClass<> > FieldTupleCell2;
  typedef FieldTuple< FieldTupleCell2, Field<PhysD2, TopoD2, ArrayP>, TupleClass<> > FieldTupleCell3;

  FieldTupleCell3 fldCellTuple3 = (xfld, pfldCell1, pfldCell2);

  BOOST_CHECK_EQUAL( &xfld, &get<0>(fldCellTuple3) );
  BOOST_CHECK_EQUAL( &pfldCell1, &get<1>(fldCellTuple3) );
  BOOST_CHECK_EQUAL( &pfldCell2, &get<2>(fldCellTuple3) );

  FieldTupleCell3 fldCellTuple3Copy(fldCellTuple3);

  BOOST_CHECK_EQUAL( &xfld, &get<0>(fldCellTuple3Copy) );
  BOOST_CHECK_EQUAL( &pfldCell1, &get<1>(fldCellTuple3Copy) );
  BOOST_CHECK_EQUAL( &pfldCell2, &get<2>(fldCellTuple3Copy) );

  testTupleParenOperator( (xfld, pfldCell1, pfldCell2), xfld, pfldCell1, pfldCell2 );

  {
    // Test appending a field to an existing tuple
    FieldTupleCell2 fldCellTuple2 = (xfld, pfldCell1);
    FieldTupleCell3 fldCellTuple3 = (fldCellTuple2, pfldCell2);

    BOOST_CHECK_EQUAL( &xfld, &get<0>(fldCellTuple3) );
    BOOST_CHECK_EQUAL( &pfldCell1, &get<1>(fldCellTuple3) );
    BOOST_CHECK_EQUAL( &pfldCell2, &get<2>(fldCellTuple3) );
  }


  BOOST_CHECK_THROW( fldCellTuple3.nInteriorTraceGroups(), AssertionException );
  BOOST_CHECK_THROW( fldCellTuple3.nBoundaryTraceGroups(), AssertionException );
  BOOST_CHECK_EQUAL( 1, fldCellTuple3.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld, &fldCellTuple3.getXField() );


  BOOST_CHECK_THROW( fldCellTuple3.getInteriorTraceGroupBase(0), AssertionException );
  BOOST_CHECK_THROW( fldCellTuple3.getBoundaryTraceGroupBase(0), AssertionException );
  BOOST_CHECK( typeid(Triangle) == fldCellTuple3.getCellGroupBase(0).topoTypeID() );

  BOOST_CHECK_EQUAL( xfld.getCellGroupBase(0).nElem() , fldCellTuple3.getCellGroupBase(0).nElem() );


  BOOST_CHECK_THROW( fldCellTuple3.getInteriorTraceGroup<Line>(0), AssertionException );
  BOOST_CHECK_THROW( fldCellTuple3.getBoundaryTraceGroup<Line>(0), AssertionException );
  const FieldTupleCell3::FieldCellGroupType<Triangle>& cellGroup3 = fldCellTuple3.getCellGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( &pfldCell2.getCellGroup<Triangle>(0), &cellGroup3.fldR() );
  BOOST_CHECK_EQUAL( &pfldCell1.getCellGroup<Triangle>(0), &cellGroup3.fldL().fldR() );
  BOOST_CHECK_EQUAL( &xfld.getCellGroup<Triangle>(0), &cellGroup3.fldL().fldL() );

  BOOST_CHECK_EQUAL( &xfld.getCellGroup<Triangle>(0), &get<0>(cellGroup3) );
  BOOST_CHECK_EQUAL( &pfldCell1.getCellGroup<Triangle>(0), &get<1>(cellGroup3) );
  BOOST_CHECK_EQUAL( &pfldCell2.getCellGroup<Triangle>(0), &get<2>(cellGroup3) );

  typedef FieldTupleCell3::FieldCellGroupType<Triangle>::ElementType<> ElementType;
  ElementType cellElem( cellGroup3.basis() );

  cellGroup3.getElement( cellElem, 0 );

  BOOST_CHECK_EQUAL( 0, cellElem.elemL().elemL().DOF(0)[0] );
  BOOST_CHECK_EQUAL( 0, cellElem.elemL().elemL().DOF(0)[1] );

  BOOST_CHECK_EQUAL( 1, cellElem.elemL().elemL().DOF(1)[0] );
  BOOST_CHECK_EQUAL( 0, cellElem.elemL().elemL().DOF(1)[1] );

  BOOST_CHECK_EQUAL( 0, cellElem.elemL().elemL().DOF(2)[0] );
  BOOST_CHECK_EQUAL( 1, cellElem.elemL().elemL().DOF(2)[1] );


  BOOST_CHECK_EQUAL( 0, get<0>(cellElem).DOF(0)[0] );
  BOOST_CHECK_EQUAL( 0, get<0>(cellElem).DOF(0)[1] );

  BOOST_CHECK_EQUAL( 1, get<0>(cellElem).DOF(1)[0] );
  BOOST_CHECK_EQUAL( 0, get<0>(cellElem).DOF(1)[1] );

  BOOST_CHECK_EQUAL( 0, get<0>(cellElem).DOF(2)[0] );
  BOOST_CHECK_EQUAL( 1, get<0>(cellElem).DOF(2)[1] );



  BOOST_CHECK_CLOSE( 3.14, cellElem.elemL().elemR().DOF(0)[0], 1e-12 );
  BOOST_CHECK_CLOSE( 3.14, cellElem.elemL().elemR().DOF(0)[1], 1e-12 );

  BOOST_CHECK_CLOSE( 3.14, get<1>(cellElem).DOF(0)[0], 1e-12 );
  BOOST_CHECK_CLOSE( 3.14, get<1>(cellElem).DOF(0)[1], 1e-12 );



  BOOST_CHECK_CLOSE( 2*3.14, cellElem.elemR().DOF(0)[0], 1e-12 );
  BOOST_CHECK_CLOSE( 2*3.14, cellElem.elemR().DOF(0)[1], 1e-12 );

  BOOST_CHECK_CLOSE( 2*3.14, get<2>(cellElem).DOF(0)[0], 1e-12 );
  BOOST_CHECK_CLOSE( 2*3.14, get<2>(cellElem).DOF(0)[1], 1e-12 );


  ElementType::T p;

  cellElem.eval({1./2.,1./2.}, p);

  BOOST_CHECK_CLOSE( 0.5, get<0>(p)[0], 1e-12 );
  BOOST_CHECK_CLOSE( 0.5, get<0>(p)[1], 1e-12 );

  BOOST_CHECK_CLOSE( 3.14, get<1>(p)[0], 1e-12 );

  BOOST_CHECK_CLOSE( 2*3.14, get<2>(p)[0], 1e-12 );

  // Zero out for the next test
  p = 0;

  BOOST_CHECK_SMALL( get<0>(p)[0], 1e-12 );
  BOOST_CHECK_SMALL( get<0>(p)[1], 1e-12 );
  BOOST_CHECK_SMALL( get<1>(p)[0], 1e-12 );
  BOOST_CHECK_SMALL( get<2>(p)[0], 1e-12 );

  // Evaluate the basis at a point and then compute the parameter from the basis functions
  FieldTupleCell3::FieldCellGroupType<Triangle>::BasisType::BasisPointType phi(cellElem.basis());

  cellElem.evalBasis({1./2.,1./2.}, phi);
  cellElem.evalFromBasis(phi, phi.size(), p);

  BOOST_CHECK_CLOSE( 0.5, get<0>(p)[0], 1e-12 );
  BOOST_CHECK_CLOSE( 0.5, get<0>(p)[1], 1e-12 );

  BOOST_CHECK_CLOSE( 3.14, get<1>(p)[0], 1e-12 );

  BOOST_CHECK_CLOSE( 2*3.14, get<2>(p)[0], 1e-12 );


  typedef SurrealizedElementTuple<FieldTupleCell3::FieldCellGroupType<Triangle>, SurrealS<1>, 1>::type SurrealElementType1;
  typedef SurrealizedElementTuple<FieldTupleCell3::FieldCellGroupType<Triangle>, SurrealS<1>, 2>::type SurrealElementType2;

  typedef FieldGroupAreaTraits<Triangle, DLA::VectorS<2, Real> >::ElementType<Real>        ElementPFieldClass;
  typedef FieldGroupAreaTraits<Triangle, DLA::VectorS<2, Real> >::ElementType<SurrealS<1>> ElementSFieldClass;
  typedef XFieldGroupAreaTraits<PhysD2, TopoD2, Triangle>::ElementType<Real>               ElementXFieldClass;
  typedef MakeTuple<ElementTuple, ElementXFieldClass, ElementSFieldClass, ElementPFieldClass>::type TrueSurrealElementType1;
  typedef MakeTuple<ElementTuple, ElementXFieldClass, ElementPFieldClass, ElementSFieldClass>::type TrueSurrealElementType2;

#if TEMPLATE_ERROR_MESSAGE
  ErrorMsg<TrueSurrealElementType1, SurrealElementType1> b;
#endif

  static_assert( std::is_same<TrueSurrealElementType1, SurrealElementType1>::value, "These should be the same");
  static_assert( std::is_same<TrueSurrealElementType2, SurrealElementType2>::value, "These should be the same");
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Tuple_FieldTuple_3Fld_Cell_test )
{

  typedef DLA::VectorS<2, Real> ArrayP;
  typedef Field_DG_Cell< PhysD2, TopoD2, ArrayP > PField2D_DG_Area;

  XField2D_2Triangle_X1_1Group xfld;

  int order = 0;
  PField2D_DG_Area pfldCell1(xfld, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 2, pfldCell1.nDOF() );
  pfldCell1.DOF(0) = 3.14;
  pfldCell1.DOF(1) = 5.13;

  PField2D_DG_Area pfldCell2(xfld, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 2, pfldCell2.nDOF() );
  pfldCell2.DOF(0) = 2*3.14;
  pfldCell2.DOF(1) = 2*5.13;

  PField2D_DG_Area pfldCell3(xfld, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 2, pfldCell3.nDOF() );
  pfldCell3.DOF(0) = -1*3.14;
  pfldCell3.DOF(1) = -1*5.13;

  PField2D_DG_Area pfldCell4(xfld, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 2, pfldCell4.nDOF() );
  pfldCell4.DOF(0) = -2*3.14;
  pfldCell4.DOF(1) = -2*5.13;

  PField2D_DG_Area pfldCell5(xfld, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 2, pfldCell5.nDOF() );
  pfldCell5.DOF(0) = 3*3.14;
  pfldCell5.DOF(1) = 3*5.13;

  typedef FieldTuple< XField<PhysD2, TopoD2>, Field<PhysD2, TopoD2, ArrayP>, TupleClass<0> > FieldTupleCell2;
  typedef FieldTuple< FieldTupleCell2, Field<PhysD2, TopoD2, ArrayP>, TupleClass<0> > FieldTupleCell3;
  typedef FieldTuple< Field<PhysD2, TopoD2, ArrayP>, Field<PhysD2, TopoD2, ArrayP>, TupleClass<0> > FieldTupleCell4;
  typedef FieldTuple< FieldTupleCell4, Field<PhysD2, TopoD2, ArrayP>, TupleClass<0> > FieldTupleCell5;
  typedef FieldTuple< FieldTupleCell3, FieldTupleCell5, TupleClass<1> > FieldTupleCell6;

  FieldTupleCell3 fldCellTuple3 = (xfld,      pfldCell1, pfldCell2);
  FieldTupleCell5 fldCellTuple5 = (pfldCell3, pfldCell4, pfldCell5);
  FieldTupleCell6 fldCellTuple6 = ( fldCellTuple3, fldCellTuple5 );


  // testing address and brackets precedence
  BOOST_CHECK_EQUAL( &xfld,       &get<0>(get<0>(fldCellTuple6)) );
  BOOST_CHECK_EQUAL( &pfldCell1,  &get<1>(get<0>(fldCellTuple6)) );
  BOOST_CHECK_EQUAL( &pfldCell2,  &get<2>(get<0>(fldCellTuple6)) );
  BOOST_CHECK_EQUAL( &pfldCell3,  &get<0>(get<1>(fldCellTuple6)) );
  BOOST_CHECK_EQUAL( &pfldCell4,  &get<1>(get<1>(fldCellTuple6)) );
  BOOST_CHECK_EQUAL( &pfldCell5,  &get<2>(get<1>(fldCellTuple6)) );

  BOOST_CHECK_EQUAL( &xfld,       &get<0>(fldCellTuple6.left()) );
  BOOST_CHECK_EQUAL( &pfldCell1,  &get<1>(fldCellTuple6.left()) );
  BOOST_CHECK_EQUAL( &pfldCell2,  &get<2>(fldCellTuple6.left()) );
  BOOST_CHECK_EQUAL( &pfldCell3,  &get<0>(fldCellTuple6.right()) );
  BOOST_CHECK_EQUAL( &pfldCell4,  &get<1>(fldCellTuple6.right()) );
  BOOST_CHECK_EQUAL( &pfldCell5,  &get<2>(fldCellTuple6.right()) );

  FieldTupleCell6 fldCellTuple6Copy(fldCellTuple6);

  // testing copy operator
  BOOST_CHECK_EQUAL( &xfld,       &get<0>(get<0>(fldCellTuple6Copy)) );
  BOOST_CHECK_EQUAL( &pfldCell1,  &get<1>(get<0>(fldCellTuple6Copy)) );
  BOOST_CHECK_EQUAL( &pfldCell2,  &get<2>(get<0>(fldCellTuple6Copy)) );
  BOOST_CHECK_EQUAL( &pfldCell3,  &get<0>(get<1>(fldCellTuple6Copy)) );
  BOOST_CHECK_EQUAL( &pfldCell4,  &get<1>(get<1>(fldCellTuple6Copy)) );
  BOOST_CHECK_EQUAL( &pfldCell5,  &get<2>(get<1>(fldCellTuple6Copy)) );

  BOOST_CHECK_EQUAL( &xfld,       &get<0>(fldCellTuple6Copy.left()) );
  BOOST_CHECK_EQUAL( &pfldCell1,  &get<1>(fldCellTuple6Copy.left()) );
  BOOST_CHECK_EQUAL( &pfldCell2,  &get<2>(fldCellTuple6Copy.left()) );
  BOOST_CHECK_EQUAL( &pfldCell3,  &get<0>(fldCellTuple6Copy.right()) );
  BOOST_CHECK_EQUAL( &pfldCell4,  &get<1>(fldCellTuple6Copy.right()) );
  BOOST_CHECK_EQUAL( &pfldCell5,  &get<2>(fldCellTuple6Copy.right()) );

  FieldTupleCell6 paren = ( (xfld, pfldCell1, pfldCell2) , (pfldCell3, pfldCell4, pfldCell5 ) );

  // testing parenthesis, comma operator and bracket precedence
  BOOST_CHECK_EQUAL( &get<0>(get<0>(fldCellTuple6)), &get<0>(get<0>(paren)) );
  BOOST_CHECK_EQUAL( &get<1>(get<0>(fldCellTuple6)), &get<1>(get<0>(paren)) );
  BOOST_CHECK_EQUAL( &get<2>(get<0>(fldCellTuple6)), &get<2>(get<0>(paren)) );
  BOOST_CHECK_EQUAL( &get<0>(get<1>(fldCellTuple6)), &get<0>(get<1>(paren)) );
  BOOST_CHECK_EQUAL( &get<1>(get<1>(fldCellTuple6)), &get<1>(get<1>(paren)) );
  BOOST_CHECK_EQUAL( &get<2>(get<1>(fldCellTuple6)), &get<2>(get<1>(paren)) );

  BOOST_CHECK_EQUAL( &get<0>(get<0>(fldCellTuple6)), &get<0>(paren.left()) );
  BOOST_CHECK_EQUAL( &get<1>(get<0>(fldCellTuple6)), &get<1>(paren.left()) );
  BOOST_CHECK_EQUAL( &get<2>(get<0>(fldCellTuple6)), &get<2>(paren.left()) );
  BOOST_CHECK_EQUAL( &get<0>(get<1>(fldCellTuple6)), &get<0>(paren.right()) );
  BOOST_CHECK_EQUAL( &get<1>(get<1>(fldCellTuple6)), &get<1>(paren.right()) );
  BOOST_CHECK_EQUAL( &get<2>(get<1>(fldCellTuple6)), &get<2>(paren.right()) );

  BOOST_CHECK_THROW( fldCellTuple6.nInteriorTraceGroups(), AssertionException );
  BOOST_CHECK_THROW( fldCellTuple6.nBoundaryTraceGroups(), AssertionException );
  BOOST_CHECK_EQUAL( 1, fldCellTuple6.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld, &get<0>(fldCellTuple6).getXField() );
  BOOST_CHECK_EQUAL( &xfld, &fldCellTuple6.left().getXField() );


  BOOST_CHECK_THROW( fldCellTuple3.getInteriorTraceGroupBase(0), AssertionException );
  BOOST_CHECK_THROW( fldCellTuple3.getBoundaryTraceGroupBase(0), AssertionException );
  BOOST_CHECK( typeid(Triangle) == fldCellTuple6.left().getCellGroupBase(0).topoTypeID() );
  BOOST_CHECK( typeid(Triangle) == fldCellTuple6.right().getCellGroupBase(0).topoTypeID() );
  BOOST_CHECK( typeid(Triangle) == get<0>(fldCellTuple6).getCellGroupBase(0).topoTypeID() );
  BOOST_CHECK( typeid(Triangle) == get<1>(fldCellTuple6).getCellGroupBase(0).topoTypeID() );

  BOOST_CHECK_EQUAL( xfld.getCellGroupBase(0).nElem() , fldCellTuple6.left().getCellGroupBase(0).nElem() );
  BOOST_CHECK_EQUAL( xfld.getCellGroupBase(0).nElem() , get<0>(fldCellTuple6).getCellGroupBase(0).nElem() );

  BOOST_CHECK_THROW( fldCellTuple6.getInteriorTraceGroup<Line>(0), AssertionException );
  BOOST_CHECK_THROW( fldCellTuple6.getBoundaryTraceGroup<Line>(0), AssertionException );
  const FieldTupleCell3::FieldCellGroupType<Triangle>& cellGroup3 = fldCellTuple3.getCellGroup<Triangle>(0);
  const FieldTupleCell5::FieldCellGroupType<Triangle>& cellGroup5 = fldCellTuple5.getCellGroup<Triangle>(0);
  const FieldTupleCell6::FieldCellGroupType<Triangle>& cellGroup6 = fldCellTuple6.getCellGroup<Triangle>(0);

  // left triple
  BOOST_CHECK_EQUAL( &pfldCell2.getCellGroup<Triangle>(0),  &cellGroup6.fldL().fldR() );
  BOOST_CHECK_EQUAL( &pfldCell1.getCellGroup<Triangle>(0),  &cellGroup6.fldL().fldL().fldR() );
  BOOST_CHECK_EQUAL( &xfld.getCellGroup<Triangle>(0),       &cellGroup6.fldL().fldL().fldL() );

  BOOST_CHECK_EQUAL( &pfldCell2.getCellGroup<Triangle>(0),  &(cellGroup6.fldL()).fldR() );
  BOOST_CHECK_EQUAL( &pfldCell1.getCellGroup<Triangle>(0),  &(cellGroup6.fldL()).fldL().fldR() );
  BOOST_CHECK_EQUAL( &xfld.getCellGroup<Triangle>(0),       &(cellGroup6.fldL()).fldL().fldL() );

  BOOST_CHECK_EQUAL( &xfld.getCellGroup<Triangle>(0),       &get<0>(get<0>(cellGroup6)) );
  BOOST_CHECK_EQUAL( &pfldCell1.getCellGroup<Triangle>(0),  &get<1>(get<0>(cellGroup6)) );
  BOOST_CHECK_EQUAL( &pfldCell2.getCellGroup<Triangle>(0),  &get<2>(get<0>(cellGroup6)) );

  BOOST_CHECK_EQUAL( &xfld.getCellGroup<Triangle>(0),       &get<0>(cellGroup6.fldL() ) );
  BOOST_CHECK_EQUAL( &pfldCell1.getCellGroup<Triangle>(0),  &get<1>(cellGroup6.fldL() ) );
  BOOST_CHECK_EQUAL( &pfldCell2.getCellGroup<Triangle>(0),  &get<2>(cellGroup6.fldL() ) );

  // right triple
  BOOST_CHECK_EQUAL( &pfldCell5.getCellGroup<Triangle>(0),  &cellGroup6.fldR().fldR() );
  BOOST_CHECK_EQUAL( &pfldCell4.getCellGroup<Triangle>(0),  &cellGroup6.fldR().fldL().fldR() );
  BOOST_CHECK_EQUAL( &pfldCell3.getCellGroup<Triangle>(0),  &cellGroup6.fldR().fldL().fldL() );

  BOOST_CHECK_EQUAL( &pfldCell5.getCellGroup<Triangle>(0),  &(cellGroup6.fldR()).fldR() );
  BOOST_CHECK_EQUAL( &pfldCell4.getCellGroup<Triangle>(0),  &(cellGroup6.fldR()).fldL().fldR() );
  BOOST_CHECK_EQUAL( &pfldCell3.getCellGroup<Triangle>(0),  &(cellGroup6.fldR()).fldL().fldL() );

  BOOST_CHECK_EQUAL( &pfldCell3.getCellGroup<Triangle>(0),  &get<0>(get<1>(cellGroup6)) );
  BOOST_CHECK_EQUAL( &pfldCell4.getCellGroup<Triangle>(0),  &get<1>(get<1>(cellGroup6)) );
  BOOST_CHECK_EQUAL( &pfldCell5.getCellGroup<Triangle>(0),  &get<2>(get<1>(cellGroup6)) );

  BOOST_CHECK_EQUAL( &pfldCell3.getCellGroup<Triangle>(0),  &get<0>(cellGroup6.fldR() ) );
  BOOST_CHECK_EQUAL( &pfldCell4.getCellGroup<Triangle>(0),  &get<1>(cellGroup6.fldR() ) );
  BOOST_CHECK_EQUAL( &pfldCell5.getCellGroup<Triangle>(0),  &get<2>(cellGroup6.fldR() ) );

  typedef FieldTupleCell3::FieldCellGroupType<Triangle>::ElementType<> ElementType3;
  typedef FieldTupleCell5::FieldCellGroupType<Triangle>::ElementType<> ElementType5;
  typedef FieldTupleCell6::FieldCellGroupType<Triangle>::ElementType<> ElementType6;
  ElementType3 cellElem3( cellGroup3.basis() );
  ElementType5 cellElem5( cellGroup5.basis() );
  ElementType6 cellElem6( cellGroup6.basis() );

  //testing left Triple
  cellGroup3.getElement( cellElem3, 0 );
  cellGroup6.getElement( cellElem6, 0 );

  // compare xfld
  BOOST_REQUIRE_EQUAL( cellElem3.elemL().elemL().nDOF() , 3);
  BOOST_REQUIRE_EQUAL( cellElem6.elemL().elemL().elemL().nDOF() , 3);

  BOOST_CHECK_EQUAL( cellElem3.elemL().elemL().DOF(0)[0] , cellElem6.elemL().elemL().elemL().DOF(0)[0]);
  BOOST_CHECK_EQUAL( cellElem3.elemL().elemL().DOF(0)[1] , cellElem6.elemL().elemL().elemL().DOF(0)[1]);
  BOOST_CHECK_EQUAL( cellElem3.elemL().elemL().DOF(1)[0] , cellElem6.elemL().elemL().elemL().DOF(1)[0]);
  BOOST_CHECK_EQUAL( cellElem3.elemL().elemL().DOF(1)[1] , cellElem6.elemL().elemL().elemL().DOF(1)[1]);
  BOOST_CHECK_EQUAL( cellElem3.elemL().elemL().DOF(2)[0] , cellElem6.elemL().elemL().elemL().DOF(2)[0]);
  BOOST_CHECK_EQUAL( cellElem3.elemL().elemL().DOF(2)[1] , cellElem6.elemL().elemL().elemL().DOF(2)[1]);

  // compare pfldCell1
  BOOST_REQUIRE_EQUAL( cellElem3.elemL().elemR().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( cellElem6.elemL().elemL().elemR().nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem3.elemL().elemR().DOF(0)[0] , cellElem6.elemL().elemL().elemR().DOF(0)[0]);
  BOOST_CHECK_EQUAL( cellElem3.elemL().elemR().DOF(0)[1] , cellElem6.elemL().elemL().elemR().DOF(0)[1]);

  // compare pfldCell2
  BOOST_REQUIRE_EQUAL( cellElem3.elemR().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( cellElem6.elemL().elemR().nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem3.elemR().DOF(0)[0] , cellElem6.elemL().elemR().DOF(0)[0]);
  BOOST_CHECK_EQUAL( cellElem3.elemR().DOF(0)[1] , cellElem6.elemL().elemR().DOF(0)[1]);

  // compare xfld
  BOOST_REQUIRE_EQUAL( cellElem3.elemL().elemL().nDOF() , 3);
  BOOST_REQUIRE_EQUAL( get<0>(cellElem6.elemL()).nDOF() , 3);

  BOOST_CHECK_EQUAL( cellElem3.elemL().elemL().DOF(0)[0] , get<0>(cellElem6.elemL()).DOF(0)[0]);
  BOOST_CHECK_EQUAL( cellElem3.elemL().elemL().DOF(0)[1] , get<0>(cellElem6.elemL()).DOF(0)[1]);
  BOOST_CHECK_EQUAL( cellElem3.elemL().elemL().DOF(1)[0] , get<0>(cellElem6.elemL()).DOF(1)[0]);
  BOOST_CHECK_EQUAL( cellElem3.elemL().elemL().DOF(1)[1] , get<0>(cellElem6.elemL()).DOF(1)[1]);
  BOOST_CHECK_EQUAL( cellElem3.elemL().elemL().DOF(2)[0] , get<0>(cellElem6.elemL()).DOF(2)[0]);
  BOOST_CHECK_EQUAL( cellElem3.elemL().elemL().DOF(2)[1] , get<0>(cellElem6.elemL()).DOF(2)[1]);

  // compare pfldCell1
  BOOST_REQUIRE_EQUAL( cellElem3.elemL().elemR().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( get<1>(cellElem6.elemL()).nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem3.elemL().elemR().DOF(0)[0] , get<1>(cellElem6.elemL()).DOF(0)[0]);
  BOOST_CHECK_EQUAL( cellElem3.elemL().elemR().DOF(0)[1] , get<1>(cellElem6.elemL()).DOF(0)[1]);

  // compare pfldCell2
  BOOST_REQUIRE_EQUAL( cellElem3.elemR().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( get<2>(cellElem6.elemL()).nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem3.elemR().DOF(0)[0] , get<2>(cellElem6.elemL()).DOF(0)[0]);
  BOOST_CHECK_EQUAL( cellElem3.elemR().DOF(0)[1] , get<2>(cellElem6.elemL()).DOF(0)[1]);

  // compare xfld
  BOOST_REQUIRE_EQUAL( cellElem3.elemL().elemL().nDOF() , 3);
  BOOST_REQUIRE_EQUAL( get<0>(get<0>(cellElem6)).nDOF() , 3);

  BOOST_CHECK_EQUAL( cellElem3.elemL().elemL().DOF(0)[0] , get<0>(get<0>(cellElem6)).DOF(0)[0]);
  BOOST_CHECK_EQUAL( cellElem3.elemL().elemL().DOF(0)[1] , get<0>(get<0>(cellElem6)).DOF(0)[1]);
  BOOST_CHECK_EQUAL( cellElem3.elemL().elemL().DOF(1)[0] , get<0>(get<0>(cellElem6)).DOF(1)[0]);
  BOOST_CHECK_EQUAL( cellElem3.elemL().elemL().DOF(1)[1] , get<0>(get<0>(cellElem6)).DOF(1)[1]);
  BOOST_CHECK_EQUAL( cellElem3.elemL().elemL().DOF(2)[0] , get<0>(get<0>(cellElem6)).DOF(2)[0]);
  BOOST_CHECK_EQUAL( cellElem3.elemL().elemL().DOF(2)[1] , get<0>(get<0>(cellElem6)).DOF(2)[1]);

  // compare pfldCell1
  BOOST_REQUIRE_EQUAL( cellElem3.elemL().elemR().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( get<1>(get<0>(cellElem6)).nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem3.elemL().elemR().DOF(0)[0] , get<1>(get<0>(cellElem6)).DOF(0)[0]);
  BOOST_CHECK_EQUAL( cellElem3.elemL().elemR().DOF(0)[1] , get<1>(get<0>(cellElem6)).DOF(0)[1]);

  // compare pfldCell2
  BOOST_REQUIRE_EQUAL( cellElem3.elemR().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( get<2>(get<0>(cellElem6)).nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem3.elemR().DOF(0)[0] , get<2>(get<0>(cellElem6)).DOF(0)[0]);
  BOOST_CHECK_EQUAL( cellElem3.elemR().DOF(0)[1] , get<2>(get<0>(cellElem6)).DOF(0)[1]);


  //testing right Triple
  cellGroup5.getElement( cellElem5, 0 );

  // compare pfldCell3
  BOOST_REQUIRE_EQUAL( cellElem5.elemL().elemL().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( cellElem6.elemR().elemL().elemL().nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem5.elemL().elemL().DOF(0)[0] , cellElem6.elemR().elemL().elemL().DOF(0)[0]);
  BOOST_CHECK_EQUAL( cellElem5.elemL().elemL().DOF(0)[1] , cellElem6.elemR().elemL().elemL().DOF(0)[1]);

  // compare pfldCell4
  BOOST_REQUIRE_EQUAL( cellElem5.elemL().elemR().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( cellElem6.elemR().elemL().elemR().nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem5.elemL().elemR().DOF(0)[0] , cellElem6.elemR().elemL().elemR().DOF(0)[0]);
  BOOST_CHECK_EQUAL( cellElem5.elemL().elemR().DOF(0)[1] , cellElem6.elemR().elemL().elemR().DOF(0)[1]);

  // compare pfldCell5
  BOOST_REQUIRE_EQUAL( cellElem5.elemR().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( cellElem6.elemR().elemR().nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem5.elemR().DOF(0)[0] , cellElem6.elemR().elemR().DOF(0)[0]);
  BOOST_CHECK_EQUAL( cellElem5.elemR().DOF(0)[1] , cellElem6.elemR().elemR().DOF(0)[1]);


  // compare pfldCell3
  BOOST_REQUIRE_EQUAL( cellElem5.elemL().elemL().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( get<0>(cellElem6.elemR()).nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem5.elemL().elemL().DOF(0)[0] , get<0>(cellElem6.elemR()).DOF(0)[0]);
  BOOST_CHECK_EQUAL( cellElem5.elemL().elemL().DOF(0)[1] , get<0>(cellElem6.elemR()).DOF(0)[1]);

  // compare pfldCell4
  BOOST_REQUIRE_EQUAL( cellElem5.elemL().elemR().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( get<1>(cellElem6.elemR()).nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem5.elemL().elemR().DOF(0)[0] , get<1>(cellElem6.elemR()).DOF(0)[0]);
  BOOST_CHECK_EQUAL( cellElem5.elemL().elemR().DOF(0)[1] , get<1>(cellElem6.elemR()).DOF(0)[1]);

  // compare pfldCell5
  BOOST_REQUIRE_EQUAL( cellElem5.elemR().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( get<2>(cellElem6.elemR()).nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem5.elemR().DOF(0)[0] , get<2>(cellElem6.elemR()).DOF(0)[0]);
  BOOST_CHECK_EQUAL( cellElem5.elemR().DOF(0)[1] , get<2>(cellElem6.elemR()).DOF(0)[1]);


  // compare pfldCell3
  BOOST_REQUIRE_EQUAL( cellElem5.elemL().elemL().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( get<0>(get<1>(cellElem6)).nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem5.elemL().elemL().DOF(0)[0] , get<0>(get<1>(cellElem6)).DOF(0)[0]);
  BOOST_CHECK_EQUAL( cellElem5.elemL().elemL().DOF(0)[1] , get<0>(get<1>(cellElem6)).DOF(0)[1]);

  // compare pfldCell4
  BOOST_REQUIRE_EQUAL( cellElem5.elemL().elemR().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( get<1>(get<1>(cellElem6)).nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem5.elemL().elemR().DOF(0)[0] , get<1>(get<1>(cellElem6)).DOF(0)[0]);
  BOOST_CHECK_EQUAL( cellElem5.elemL().elemR().DOF(0)[1] , get<1>(get<1>(cellElem6)).DOF(0)[1]);

  // compare pfldCell5
  BOOST_REQUIRE_EQUAL( cellElem5.elemR().nDOF() , 1);
  BOOST_REQUIRE_EQUAL( get<2>(get<1>(cellElem6)).nDOF() , 1);

  BOOST_CHECK_EQUAL( cellElem5.elemR().DOF(0)[0] , get<2>(get<1>(cellElem6)).DOF(0)[0]);
  BOOST_CHECK_EQUAL( cellElem5.elemR().DOF(0)[1] , get<2>(get<1>(cellElem6)).DOF(0)[1]);


  ElementType3::T p3;
  ElementType5::T p5;
  ElementType6::T p6;

  cellElem3.eval({1./2.,1./2.}, p3);
  cellElem5.eval({1./2.,1./2.}, p5);
  cellElem6.eval({1./2.,1./2.}, p6);

  BOOST_CHECK_CLOSE( get<0>(p3)[0], get<0>(get<0>(p6))[0], 1e-12 );
  BOOST_CHECK_CLOSE( get<0>(p3)[1], get<0>(get<0>(p6))[1], 1e-12 );
  BOOST_CHECK_CLOSE( get<1>(p3)[0], get<1>(get<0>(p6))[0], 1e-12 );
  BOOST_CHECK_CLOSE( get<1>(p3)[1], get<1>(get<0>(p6))[1], 1e-12 );
  BOOST_CHECK_CLOSE( get<2>(p3)[0], get<2>(get<0>(p6))[0], 1e-12 );
  BOOST_CHECK_CLOSE( get<2>(p3)[1], get<2>(get<0>(p6))[1], 1e-12 );

  BOOST_CHECK_CLOSE( get<0>(p5)[0], get<0>(get<1>(p6))[0], 1e-12 );
  BOOST_CHECK_CLOSE( get<0>(p5)[1], get<0>(get<1>(p6))[1], 1e-12 );
  BOOST_CHECK_CLOSE( get<1>(p5)[0], get<1>(get<1>(p6))[0], 1e-12 );
  BOOST_CHECK_CLOSE( get<1>(p5)[1], get<1>(get<1>(p6))[1], 1e-12 );
  BOOST_CHECK_CLOSE( get<2>(p5)[0], get<2>(get<1>(p6))[0], 1e-12 );
  BOOST_CHECK_CLOSE( get<2>(p5)[1], get<2>(get<1>(p6))[1], 1e-12 );


  // Zero out for the next test
  p6 = 0;
  BOOST_CHECK_SMALL( get<0>(get<0>(p6))[0], 1e-12 );
  BOOST_CHECK_SMALL( get<0>(get<0>(p6))[1], 1e-12 );
  BOOST_CHECK_SMALL( get<1>(get<0>(p6))[0], 1e-12 );
  BOOST_CHECK_SMALL( get<1>(get<0>(p6))[1], 1e-12 );
  BOOST_CHECK_SMALL( get<2>(get<0>(p6))[0], 1e-12 );
  BOOST_CHECK_SMALL( get<2>(get<0>(p6))[1], 1e-12 );

  BOOST_CHECK_SMALL( get<0>(get<1>(p6))[0], 1e-12 );
  BOOST_CHECK_SMALL( get<0>(get<1>(p6))[1], 1e-12 );
  BOOST_CHECK_SMALL( get<1>(get<1>(p6))[0], 1e-12 );
  BOOST_CHECK_SMALL( get<1>(get<1>(p6))[1], 1e-12 );
  BOOST_CHECK_SMALL( get<2>(get<1>(p6))[0], 1e-12 );
  BOOST_CHECK_SMALL( get<2>(get<1>(p6))[1], 1e-12 );

#if 0 // Not sure if this will be used
  // Evaluate the basis at a point and then compute the parameter from the basis functions
  FieldTupleCell6::FieldCellGroupType<Triangle>::BasisType::BasisPointType phi(cellElem6.basis());

  cellElem6.evalBasis({1./2.,1./2.}, phi);
  cellElem6.evalFromBasis(phi, phi.size(), p6);

  BOOST_CHECK_CLOSE( get<0>(p3)[0], get<0>(get<0>(p6))[0], 1e-12 );
  BOOST_CHECK_CLOSE( get<0>(p3)[1], get<0>(get<0>(p6))[1], 1e-12 );
  BOOST_CHECK_CLOSE( get<1>(p3)[0], get<1>(get<0>(p6))[0], 1e-12 );
  BOOST_CHECK_CLOSE( get<1>(p3)[1], get<1>(get<0>(p6))[1], 1e-12 );
  BOOST_CHECK_CLOSE( get<2>(p3)[0], get<2>(get<0>(p6))[0], 1e-12 );
  BOOST_CHECK_CLOSE( get<2>(p3)[1], get<2>(get<0>(p6))[1], 1e-12 );

  BOOST_CHECK_CLOSE( get<0>(p5)[0], get<0>(get<1>(p6))[0], 1e-12 );
  BOOST_CHECK_CLOSE( get<0>(p5)[1], get<0>(get<1>(p6))[1], 1e-12 );
  BOOST_CHECK_CLOSE( get<1>(p5)[0], get<1>(get<1>(p6))[0], 1e-12 );
  BOOST_CHECK_CLOSE( get<1>(p5)[1], get<1>(get<1>(p6))[1], 1e-12 );
  BOOST_CHECK_CLOSE( get<2>(p5)[0], get<2>(get<1>(p6))[0], 1e-12 );
  BOOST_CHECK_CLOSE( get<2>(p5)[1], get<2>(get<1>(p6))[1], 1e-12 );
#endif
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldTuple_3Fld_InteriorTrace_test )
{
  typedef DLA::VectorS<2, Real> ArrayP;
  typedef Field_DG_InteriorTrace< PhysD2, TopoD2, ArrayP > PField2D_DG_InteriorTrace;

  XField2D_2Triangle_X1_1Group xfld;

  typedef FieldTuple< XField<PhysD2, TopoD2>, Field<PhysD2, TopoD2, ArrayP>, TupleClass<> > FieldTupleITrace2;
  typedef FieldTuple< FieldTupleITrace2, Field<PhysD2, TopoD2, ArrayP>, TupleClass<> > FieldTupleITrace3;

  int order = 0;
  PField2D_DG_InteriorTrace pfldI1(xfld, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 1, pfldI1.nDOF() );
  pfldI1.DOF(0) = 3.14;

  PField2D_DG_InteriorTrace pfldI2(xfld, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 1, pfldI2.nDOF() );
  pfldI2.DOF(0) = 2*3.14;

  FieldTupleITrace3 fldITraceTuple3 = (xfld, pfldI1, pfldI2);

  BOOST_CHECK_EQUAL( &xfld, &get<0>(fldITraceTuple3) );
  BOOST_CHECK_EQUAL( &pfldI1, &get<1>(fldITraceTuple3) );
  BOOST_CHECK_EQUAL( &pfldI2, &get<2>(fldITraceTuple3) );

  FieldTupleITrace3 fldITraceTuple3Copy(fldITraceTuple3);

  BOOST_CHECK_EQUAL( &xfld, &get<0>(fldITraceTuple3Copy) );
  BOOST_CHECK_EQUAL( &pfldI1, &get<1>(fldITraceTuple3Copy) );
  BOOST_CHECK_EQUAL( &pfldI2, &get<2>(fldITraceTuple3Copy) );

  testTupleParenOperator( (xfld, pfldI1, pfldI2), xfld, pfldI1, pfldI2 );


  BOOST_CHECK_EQUAL( 1, fldITraceTuple3.nInteriorTraceGroups() );
  BOOST_CHECK_THROW( fldITraceTuple3.nBoundaryTraceGroups(), AssertionException );
  BOOST_CHECK_THROW( fldITraceTuple3.nCellGroups(), AssertionException );
  BOOST_CHECK_EQUAL( &xfld, &fldITraceTuple3.getXField() );


  BOOST_CHECK( typeid(Line) == fldITraceTuple3.getInteriorTraceGroupBase(0).topoTypeID() );
  BOOST_CHECK_THROW( fldITraceTuple3.getBoundaryTraceGroupBase(0), AssertionException );
  BOOST_CHECK_THROW( fldITraceTuple3.getCellGroupBase(0), AssertionException );

  BOOST_CHECK_EQUAL( xfld.getInteriorTraceGroupBase(0).nElem() , fldITraceTuple3.getInteriorTraceGroupBase(0).nElem() );


  const FieldTupleITrace3::FieldTraceGroupType<Line>& ITraceGroup3 = fldITraceTuple3.getInteriorTraceGroup<Line>(0);
  BOOST_CHECK_THROW( fldITraceTuple3.getBoundaryTraceGroup<Line>(0), AssertionException );
  BOOST_CHECK_THROW( fldITraceTuple3.getCellGroup<Triangle>(0), AssertionException );

  BOOST_CHECK_EQUAL( &xfld.getInteriorTraceGroup<Line>(0), &ITraceGroup3.fldL().fldL() );
  BOOST_CHECK_EQUAL( &pfldI1.getInteriorTraceGroup<Line>(0), &ITraceGroup3.fldL().fldR() );
  BOOST_CHECK_EQUAL( &pfldI2.getInteriorTraceGroup<Line>(0), &ITraceGroup3.fldR() );

  BOOST_CHECK_EQUAL( &xfld.getInteriorTraceGroup<Line>(0), &get<0>(ITraceGroup3) );
  BOOST_CHECK_EQUAL( &pfldI1.getInteriorTraceGroup<Line>(0), &get<1>(ITraceGroup3) );
  BOOST_CHECK_EQUAL( &pfldI2.getInteriorTraceGroup<Line>(0), &get<2>(ITraceGroup3) );

  typedef FieldTupleITrace3::FieldTraceGroupType<Line>::ElementType<> ElementType;
  ElementType traceElem( ITraceGroup3.basis() );

  ITraceGroup3.getElement( traceElem, 0 );

  BOOST_CHECK_EQUAL( 1, traceElem.elemL().elemL().DOF(0)[0] );
  BOOST_CHECK_EQUAL( 0, traceElem.elemL().elemL().DOF(0)[1] );

  BOOST_CHECK_EQUAL( 0, traceElem.elemL().elemL().DOF(1)[0] );
  BOOST_CHECK_EQUAL( 1, traceElem.elemL().elemL().DOF(1)[1] );


  BOOST_CHECK_EQUAL( 1, get<0>(traceElem).DOF(0)[0] );
  BOOST_CHECK_EQUAL( 0, get<0>(traceElem).DOF(0)[1] );

  BOOST_CHECK_EQUAL( 0, get<0>(traceElem).DOF(1)[0] );
  BOOST_CHECK_EQUAL( 1, get<0>(traceElem).DOF(1)[1] );


  BOOST_CHECK_CLOSE( 3.14, traceElem.elemL().elemR().DOF(0)[0], 1e-12 );

  BOOST_CHECK_CLOSE( 3.14, get<1>(traceElem).DOF(0)[0], 1e-12 );


  BOOST_CHECK_CLOSE( 2*3.14, traceElem.elemR().DOF(0)[0], 1e-12 );

  BOOST_CHECK_CLOSE( 2*3.14, get<2>(traceElem).DOF(0)[0], 1e-12 );


  ElementType::T p;

  traceElem.eval({1./2.}, p);

  BOOST_CHECK_CLOSE( 0.5, get<0>(p)[0], 1e-12 );
  BOOST_CHECK_CLOSE( 0.5, get<0>(p)[1], 1e-12 );

  BOOST_CHECK_CLOSE( 3.14, get<1>(p)[0], 1e-12 );

  BOOST_CHECK_CLOSE( 2*3.14, get<2>(p)[0], 1e-12 );

  // Zero out for the next test
  p = 0;

  BOOST_CHECK_SMALL( get<0>(p)[0], 1e-12 );
  BOOST_CHECK_SMALL( get<0>(p)[1], 1e-12 );
  BOOST_CHECK_SMALL( get<1>(p)[0], 1e-12 );
  BOOST_CHECK_SMALL( get<2>(p)[0], 1e-12 );

  // Evaluate the basis at a point and then compute the parameter from the basis functions
  FieldTupleITrace3::FieldTraceGroupType<Line>::BasisType::BasisPointType phi(traceElem.basis());

  traceElem.evalBasis({1./2.}, phi);
  traceElem.evalFromBasis(phi, phi.size(), p);

  BOOST_CHECK_CLOSE( 0.5, get<0>(p)[0], 1e-12 );
  BOOST_CHECK_CLOSE( 0.5, get<0>(p)[1], 1e-12 );

  BOOST_CHECK_CLOSE( 3.14, get<1>(p)[0], 1e-12 );

  BOOST_CHECK_CLOSE( 2*3.14, get<2>(p)[0], 1e-12 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldTuple_3Fld_BoundaryTrace_test )
{
  typedef DLA::VectorS<2, Real> ArrayP;
  typedef Field_DG_BoundaryTrace< PhysD2, TopoD2, ArrayP > PField2D_DG_BoundaryTrace;

  XField2D_2Triangle_X1_1Group xfld;

  typedef FieldTuple< XField<PhysD2, TopoD2>, Field<PhysD2, TopoD2, ArrayP>, TupleClass<> > FieldTupleBTrace2;
  typedef FieldTuple< FieldTupleBTrace2, Field<PhysD2, TopoD2, ArrayP>, TupleClass<> > FieldTupleBTrace3;

  int order = 0;
  PField2D_DG_BoundaryTrace pfldB1(xfld, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 4, pfldB1.nDOF() );
  pfldB1.DOF(0) = 3.14;
  pfldB1.DOF(1) = 5.13;
  pfldB1.DOF(2) = 6.17;
  pfldB1.DOF(3) = 5.08;

  PField2D_DG_BoundaryTrace pfldB2(xfld, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 4, pfldB2.nDOF() );
  pfldB2.DOF(0) = 2*3.14;
  pfldB2.DOF(1) = 2*5.13;
  pfldB2.DOF(2) = 2*6.17;
  pfldB2.DOF(3) = 2*5.08;

  FieldTupleBTrace3 fldBTraceTuple3 = (xfld, pfldB1, pfldB2);

  BOOST_CHECK_EQUAL( &xfld, &get<0>(fldBTraceTuple3) );
  BOOST_CHECK_EQUAL( &pfldB1, &get<1>(fldBTraceTuple3) );
  BOOST_CHECK_EQUAL( &pfldB2, &get<2>(fldBTraceTuple3) );

  FieldTupleBTrace3 fldBTraceTuple3Copy(fldBTraceTuple3);

  BOOST_CHECK_EQUAL( &xfld, &get<0>(fldBTraceTuple3Copy) );
  BOOST_CHECK_EQUAL( &pfldB1, &get<1>(fldBTraceTuple3Copy) );
  BOOST_CHECK_EQUAL( &pfldB2, &get<2>(fldBTraceTuple3Copy) );

  testTupleParenOperator( (xfld, pfldB1, pfldB2), xfld, pfldB1, pfldB2 );


  BOOST_CHECK_THROW( fldBTraceTuple3.nInteriorTraceGroups(), AssertionException );
  BOOST_CHECK_EQUAL( 1, fldBTraceTuple3.nBoundaryTraceGroups() );
  BOOST_CHECK_THROW( fldBTraceTuple3.nCellGroups(), AssertionException );
  BOOST_CHECK_EQUAL( &xfld, &fldBTraceTuple3.getXField() );


  BOOST_CHECK_THROW( fldBTraceTuple3.getInteriorTraceGroupBase(0), AssertionException );
  BOOST_CHECK( typeid(Line) == fldBTraceTuple3.getBoundaryTraceGroupBase(0).topoTypeID() );
  BOOST_CHECK_THROW( fldBTraceTuple3.getCellGroupBase(0), AssertionException );

  BOOST_CHECK_EQUAL( xfld.getBoundaryTraceGroupBase(0).nElem() , fldBTraceTuple3.getBoundaryTraceGroupBase(0).nElem() );


  BOOST_CHECK_THROW( fldBTraceTuple3.getInteriorTraceGroup<Line>(0), AssertionException );
  const FieldTupleBTrace3::FieldTraceGroupType<Line>& BTraceGroup3 = fldBTraceTuple3.getBoundaryTraceGroup<Line>(0);
  BOOST_CHECK_THROW( fldBTraceTuple3.getCellGroup<Triangle>(0), AssertionException );

  BOOST_CHECK_EQUAL( &xfld.getBoundaryTraceGroup<Line>(0), &BTraceGroup3.fldL().fldL() );
  BOOST_CHECK_EQUAL( &pfldB1.getBoundaryTraceGroup<Line>(0), &BTraceGroup3.fldL().fldR() );
  BOOST_CHECK_EQUAL( &pfldB2.getBoundaryTraceGroup<Line>(0), &BTraceGroup3.fldR() );

  BOOST_CHECK_EQUAL( &xfld.getBoundaryTraceGroup<Line>(0), &get<0>(BTraceGroup3) );
  BOOST_CHECK_EQUAL( &pfldB1.getBoundaryTraceGroup<Line>(0), &get<1>(BTraceGroup3) );
  BOOST_CHECK_EQUAL( &pfldB2.getBoundaryTraceGroup<Line>(0), &get<2>(BTraceGroup3) );


  typedef FieldTupleBTrace3::FieldTraceGroupType<Line>::ElementType<> ElementType;
  ElementType traceElem( BTraceGroup3.basis() );

  BTraceGroup3.getElement( traceElem, 0 );

  BOOST_CHECK_EQUAL( 0, traceElem.elemL().elemL().DOF(0)[0] );
  BOOST_CHECK_EQUAL( 0, traceElem.elemL().elemL().DOF(0)[1] );

  BOOST_CHECK_EQUAL( 1, traceElem.elemL().elemL().DOF(1)[0] );
  BOOST_CHECK_EQUAL( 0, traceElem.elemL().elemL().DOF(1)[1] );


  BOOST_CHECK_EQUAL( 0, get<0>(traceElem).DOF(0)[0] );
  BOOST_CHECK_EQUAL( 0, get<0>(traceElem).DOF(0)[1] );

  BOOST_CHECK_EQUAL( 1, get<0>(traceElem).DOF(1)[0] );
  BOOST_CHECK_EQUAL( 0, get<0>(traceElem).DOF(1)[1] );


  BOOST_CHECK_CLOSE( 3.14, traceElem.elemL().elemR().DOF(0)[0], 1e-12 );

  BOOST_CHECK_CLOSE( 3.14, get<1>(traceElem).DOF(0)[0], 1e-12 );


  BOOST_CHECK_CLOSE( 2*3.14, traceElem.elemR().DOF(0)[0], 1e-12 );

  BOOST_CHECK_CLOSE( 2*3.14, get<2>(traceElem).DOF(0)[0], 1e-12 );


  ElementType::T p;

  traceElem.eval({1./2.}, p);

  BOOST_CHECK_CLOSE( 0.5, get<0>(p)[0], 1e-12 );
  BOOST_CHECK_SMALL(      get<0>(p)[1], 1e-12 );

  BOOST_CHECK_CLOSE( 3.14, get<1>(p)[0], 1e-12 );

  BOOST_CHECK_CLOSE( 2*3.14, get<2>(p)[0], 1e-12 );

  // Zero out for the next test
  p = 0;

  BOOST_CHECK_SMALL( get<0>(p)[0], 1e-12 );
  BOOST_CHECK_SMALL( get<0>(p)[1], 1e-12 );
  BOOST_CHECK_SMALL( get<1>(p)[0], 1e-12 );
  BOOST_CHECK_SMALL( get<2>(p)[0], 1e-12 );

  // Evaluate the basis at a point and then compute the parameter from the basis functions
  FieldTupleBTrace3::FieldTraceGroupType<Line>::BasisType::BasisPointType phi(traceElem.basis());

  traceElem.evalBasis({1./2.}, phi);
  traceElem.evalFromBasis(phi, phi.size(), p);

  BOOST_CHECK_CLOSE( 0.5, get<0>(p)[0], 1e-12 );
  BOOST_CHECK_SMALL(      get<0>(p)[1], 1e-12 );

  BOOST_CHECK_CLOSE( 3.14, get<1>(p)[0], 1e-12 );

  BOOST_CHECK_CLOSE( 2*3.14, get<2>(p)[0], 1e-12 );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ParamTuple_test )
{
  typedef MakeTuple<ParamTuple,int,double,long,bool>::type ParamType;

  // variadic template constructor

  ParamType param0(1, 2., 3, true);

  BOOST_CHECK_EQUAL( 1, get<0>(param0) );
  BOOST_CHECK_EQUAL( 2., get<1>(param0) );
  BOOST_CHECK_EQUAL( 3, get<2>(param0) );
  BOOST_CHECK_EQUAL( true, get<3>(param0) );

  ParamType param1;
  param1.set(1, 2., 3, true);

  BOOST_CHECK_EQUAL( 1, get<0>(param1) );
  BOOST_CHECK_EQUAL( 2., get<1>(param1) );
  BOOST_CHECK_EQUAL( 3, get<2>(param1) );
  BOOST_CHECK_EQUAL( true, get<3>(param1) );

  // L/R param constructor

  typedef MakeTuple<ParamTuple,int,double>::type ParamType2;

  ParamType2 param2(1, 2.);

  const int& param2_L = param2.paramL();
  const double& param2_R = param2.paramR();

  BOOST_CHECK_EQUAL( 1,  param2_L );
  BOOST_CHECK_EQUAL( 2., param2_R );

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
