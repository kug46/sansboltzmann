// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "Field/Local/XField_LocalPatch.h"
#include "Field/Local/Field_Local.h"
#include "Field/Local/FieldLift_Local.h"
#include "Field/Field_NodalView.h"

#include "tools/make_unique.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"


//Explicitly instantiate classes to get proper coverage information
namespace SANS
{

// Function for looping over a local field and field and comparing the two
template< class PhysDim, class TopoDim, class ArrayQ, template<class,class,class> class FieldType >
void checkLocalField( const Field_Local<FieldType<PhysDim,TopoDim,ArrayQ>>& fld_local,
                      const             FieldType<PhysDim,TopoDim,ArrayQ>& fld_global,
                      const Real small_tol, const Real close_tol, const bool isSplit )
{
  static const int M = ArrayQ::M;

  // Assuming only simplex fields
  typedef typename Simplex<TopoDim>::type Topology;
  typedef typename Topology::TopologyTrace TopologyTrace;
  typedef typename TopoDim::TopoDTrace TopoDTrace;

  SANS_ASSERT_MSG( &fld_local.getQField() == &fld_global, "Can only check a local and global fields that match" );
  // SANS_ASSERT_MSG( &xfld_local, &fld_local.getXField(), "Local QField must match the Local XField" );

  SANS_ASSERT_MSG( fld_local.getXField().derivedTypeID() == typeid(XField_LocalPatch<PhysDim,Topology>),
                   "only for testing XField_LocalPatch" );

  // get the xfield from the local field, and cast its pointer to a LocalPatch. This is slightly shady
  const XField_LocalPatch<PhysDim,Topology>& xfld_local = *(static_cast<const XField_LocalPatch<PhysDim,Topology>*>(&fld_local.getXField()));

  typedef typename Field<PhysDim,TopoDim,DLA::VectorS<M,Real>>::template FieldCellGroupType<Topology> FieldCellGroupType;
  typedef typename FieldCellGroupType::template ElementType<> ElementFieldClass;

  // state vectors for evaluation
  ArrayQ qeval_main, qeval_local;

  typedef DLA::VectorS<TopoDim::D,Real> RefCoordType;
  RefCoordType RefCoord_main, RefCoord_sub; // reference-element coordinates in cell

  // check cell groups
  for (int cell_group = 0; cell_group < fld_local.nCellGroups(); cell_group++)
  {
    const FieldCellGroupType& cellgrp_local = fld_local.template getCellGroup<Topology>(cell_group);

    Quadrature<TopoDim, Topology> cellQuadrature(fld_local.template getCellGroup<Topology>(cell_group).order()+2);
    const int nCellquad = cellQuadrature.nQuadrature();

    const int order = cellgrp_local.order();

    for (int elem = 0; elem < fld_local.template getCellGroup<Topology>(cell_group).nElem(); elem++)
    {
      std::pair<int,int> groupElem_global = xfld_local.getGlobalCellMap(std::pair<int,int>(cell_group,elem));
      const FieldCellGroupType& cellgrp_global = fld_global.template getCellGroup<Topology>(groupElem_global.first);

      ElementFieldClass fldElem_global( cellgrp_global.basis() );
      ElementFieldClass fldElem_local( cellgrp_local.basis() );

      cellgrp_global.getElement( fldElem_global, groupElem_global.second );
      cellgrp_local.getElement( fldElem_local, elem );

      ElementSplitInfo split_info = xfld_local.getCellSplitInfo(std::pair<int,int>(cell_group,elem));

      if (split_info.split_flag == ElementSplitFlag::Unsplit)
      {
        for (int iquad = 0; iquad < nCellquad; iquad++)
        {
          cellQuadrature.coordinates( iquad, RefCoord_main );
          fldElem_local.eval( RefCoord_main, qeval_local);
          fldElem_global.eval( RefCoord_main, qeval_main);
          for (int i = 0; i < M; i++)
            SANS_CHECK_CLOSE( qeval_local[i], qeval_main[i], small_tol, close_tol );
        }

        if ( !(order >= 3 && cellgrp_global.basis()->category() == BasisFunctionCategory_Hierarchical ) )
        {
          // Check if the local dofs are identical
          for (int dof = 0; dof < fldElem_global.nDOF(); dof++)
            for (int i = 0; i < M; i++)
              SANS_CHECK_CLOSE( fldElem_global.DOF(dof)[i], fldElem_local.DOF(dof)[i], small_tol, close_tol );
        }
      }
      else if ( split_info.split_flag == ElementSplitFlag::Split)
      {
        BOOST_REQUIRE( isSplit );
        for (int iquad = 0; iquad < nCellquad; iquad++)
        {
          // coordinates in the local element
          cellQuadrature.coordinates( iquad, RefCoord_sub );
          fldElem_local.eval( RefCoord_sub, qeval_local);

          // coordinates in the global element
          BasisFunction_RefElement_Split<TopoDim,Topology>::
            transform( RefCoord_sub, split_info.split_type, split_info.edge_index, split_info.subcell_index, RefCoord_main);
          fldElem_global.eval(RefCoord_main, qeval_main);

          for (int i = 0; i < M; i++)
            SANS_CHECK_CLOSE(qeval_local[i], qeval_main[i], small_tol, close_tol);
        }
      }
    }
  }

  typedef typename Field<PhysDim,TopoDim,DLA::VectorS<M,Real>>::template FieldTraceGroupType<TopologyTrace> FieldTraceGroupType;
  typedef typename FieldTraceGroupType::template ElementType<> ElementTraceFieldClass;

  typedef DLA::VectorS<MAX(1,TopoDTrace::D),Real> TraceRefCoordType;
  TraceRefCoordType TRefCoord_main, TRefCoord_sub; // reference-element coordinates in trace

  for (int trace_group = 0; trace_group < fld_local.nBoundaryTraceGroups(); trace_group++)
  {
    const FieldTraceGroupType& local_tracegrp  = fld_local.template getBoundaryTraceGroup<TopologyTrace>(trace_group);

    Quadrature<TopoDTrace, TopologyTrace> traceQuadrature(TopoDTrace::D == 0 ? 0 : local_tracegrp.order()+2);
    const int nTracequad = traceQuadrature.nQuadrature();
    for (int elem = 0; elem < fld_local.template getBoundaryTraceGroup<TopologyTrace>(trace_group).nElem(); elem++)
    {
      // extract the Global group and element from the map
      std::pair<int,int> groupElem_global = xfld_local.getGlobalBoundaryTraceMap(std::pair<int,int>(trace_group,elem));
      const FieldTraceGroupType& global_tracegrp = fld_global.template getBoundaryTraceGroup<TopologyTrace>(groupElem_global.first);

      ElementTraceFieldClass fldElem_global( global_tracegrp.basis() );
      ElementTraceFieldClass fldElem_local(local_tracegrp.basis());

      local_tracegrp. getElement( fldElem_local,  elem );
      global_tracegrp.getElement( fldElem_global, groupElem_global.second );

      ElementSplitInfo split_info = xfld_local.getBoundaryTraceSplitInfo( std::pair<int,int>(trace_group,elem) );

      if (split_info.split_flag == ElementSplitFlag::Unsplit)
      {
        for (int iquad = 0; iquad < nTracequad; iquad++)
        {
          traceQuadrature.coordinates( iquad, TRefCoord_main );
          fldElem_local.eval( TRefCoord_main, qeval_local);
          fldElem_global.eval( TRefCoord_main, qeval_main);
          for (int i = 0; i < 2; i++)
            SANS_CHECK_CLOSE( qeval_local[i], qeval_main[i], small_tol, close_tol );
        }
      }
      else if (split_info.split_flag == ElementSplitFlag::Split)
      {
        BOOST_REQUIRE( isSplit );
        for (int iquad = 0; iquad < nTracequad; iquad++)
        {
          traceQuadrature.coordinates( iquad, TRefCoord_sub );

          // coordinates in the local elements
          fldElem_local.eval( TRefCoord_sub, qeval_local);

          BasisFunction_RefElement_Split<TopoDTrace,TopologyTrace>::
            transform( TRefCoord_sub, split_info.split_type, split_info.edge_index, split_info.subcell_index, TRefCoord_main);

          fldElem_global.eval(TRefCoord_main, qeval_main);

          for (int i = 0; i < 2; i++)
          {
            SANS_CHECK_CLOSE(qeval_local[i], qeval_main[i], small_tol, close_tol);
          }
        }
      }
    }
  }
}


// Function for looping over a local field and field and comparing the two
template< class PhysDim, class TopoDim, class VectorArrayQ >
void checkLocalField( const FieldLift_Local<FieldLift_DG_Cell<PhysDim,TopoDim,VectorArrayQ>>& fld_local,
                      const                 FieldLift_DG_Cell<PhysDim,TopoDim,VectorArrayQ>& fld_global,
                      const Real small_tol, const Real close_tol, const bool isSplit )
{
  static const int M = VectorArrayQ::Ttype::M;

  // Assuming only simplex fields
  typedef typename Simplex<TopoDim>::type Topology;

  //SANS_ASSERT_MSG( &fld_local.getQField() == &fld_global, "Can only check a local and global fields that match" );
  // SANS_ASSERT_MSG( &xfld_local, &fld_local.getXField(), "Local QField must match the Local XField" );

  SANS_ASSERT_MSG( fld_local.getXField().derivedTypeID() == typeid(XField_LocalPatch<PhysDim,Topology>),
                   "only for testing XField_LocalPatch" );

  // get the xfield from the local field, and cast its pointer to a LocalPatch. This is slightly shady
  const XField_LocalPatch<PhysDim,Topology>& xfld_local = *(static_cast<const XField_LocalPatch<PhysDim,Topology>*>(&fld_local.getXField()));

  typedef typename FieldLift_DG_Cell<PhysDim,TopoDim,VectorArrayQ>::template FieldCellGroupType<Topology> FieldCellGroupType;
  typedef typename FieldCellGroupType::template ElementType<> ElementFieldClass;

  // state vectors for evaluation
  VectorArrayQ qeval_main, qeval_local;

  typedef DLA::VectorS<TopoDim::D,Real> RefCoordType;
  RefCoordType RefCoord_main, RefCoord_sub; // reference-element coordinates in cell

  // check cell groups
  for (int cell_group = 0; cell_group < fld_local.nCellGroups(); cell_group++)
  {
    const FieldCellGroupType& cellgrp_local = fld_local.template getCellGroup<Topology>(cell_group);

    Quadrature<TopoDim, Topology> cellQuadrature(fld_local.template getCellGroup<Topology>(cell_group).order()+2);
    const int nCellquad = cellQuadrature.nQuadrature();

    const int order = cellgrp_local.order();

    for (int elem = 0; elem < fld_local.template getCellGroup<Topology>(cell_group).nElem(); elem++)
    {
      std::pair<int,int> groupElem_global = xfld_local.getGlobalCellMap(std::pair<int,int>(cell_group,elem));
      const FieldCellGroupType& cellgrp_global = fld_global.template getCellGroup<Topology>(groupElem_global.first);

      ElementFieldClass fldElem_global( cellgrp_global.basis() );
      ElementFieldClass fldElem_local( cellgrp_local.basis() );

      for (int trace = 0; trace < Topology::NTrace; trace++)
      {
        cellgrp_global.getElement( fldElem_global, groupElem_global.second, trace );
        cellgrp_local.getElement( fldElem_local, elem, trace );

        ElementSplitInfo split_info = xfld_local.getCellSplitInfo(std::pair<int,int>(cell_group,elem));

        if (split_info.split_flag == ElementSplitFlag::Unsplit)
        {
          for (int iquad = 0; iquad < nCellquad; iquad++)
          {
            cellQuadrature.coordinates( iquad, RefCoord_main );
            fldElem_local.eval( RefCoord_main, qeval_local);
            fldElem_global.eval( RefCoord_main, qeval_main);
            for (int d = 0; d < PhysDim::D; d++)
              for (int i = 0; i < M; i++)
                SANS_CHECK_CLOSE( qeval_local[d][i], qeval_main[d][i], small_tol, close_tol );
          }

          if ( !(order >= 3 && cellgrp_global.basis()->category() == BasisFunctionCategory_Hierarchical ) )
          {
            // Check if the local dofs are identical
            for (int dof = 0; dof < fldElem_global.nDOF(); dof++)
              for (int d = 0; d < PhysDim::D; d++)
                for (int i = 0; i < M; i++)
                  SANS_CHECK_CLOSE( fldElem_global.DOF(dof)[d][i], fldElem_local.DOF(dof)[d][i], small_tol, close_tol );
          }
        }
        else if ( split_info.split_flag == ElementSplitFlag::Split)
        {
          BOOST_REQUIRE( isSplit );
          for (int iquad = 0; iquad < nCellquad; iquad++)
          {
            // coordinates in the local element
            cellQuadrature.coordinates( iquad, RefCoord_sub );
            fldElem_local.eval( RefCoord_sub, qeval_local);

            // coordinates in the global element
            BasisFunction_RefElement_Split<TopoDim,Topology>::
            transform( RefCoord_sub, split_info.split_type, split_info.edge_index, split_info.subcell_index, RefCoord_main);
            fldElem_global.eval(RefCoord_main, qeval_main);

            for (int i = 0; i < M; i++)
              for (int d = 0; d < PhysDim::D; d++)
                SANS_CHECK_CLOSE(qeval_local[d][i], qeval_main[d][i], small_tol, close_tol);
          }
        }
      }
    }
  }
}


// a helper function to construct a solver interface and test all the elements on a grid
template <class PhysDim, class TopoDim>
void localElementExtractAndTransfer(const XField<PhysDim, TopoDim> &xfld, const BasisFunctionCategory basis_category,
                                    const int order, const Real small_tol, const Real close_tol, const bool isSplit)
{
  typedef typename Simplex<TopoDim>::type Topology;

  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef DLA::VectorS<PhysDim::D, ArrayQ> VectorArrayQ;
  typedef Field_DG_Cell< PhysDim, TopoDim, ArrayQ > QField_Cell;
  typedef Field_DG_BoundaryTrace< PhysDim, TopoDim, ArrayQ > QField_BTrace;
  typedef FieldLift_DG_Cell< PhysDim, TopoDim, VectorArrayQ > RFieldType;

  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;

  const int nEdge = Topology::NEdge; // Number of different edges

  const int comm_rank = xfld.comm()->rank();
  mpi::communicator comm_local = xfld.comm()->split(comm_rank);

  // fields and traces
  QField_Cell qfld(xfld, order, basis_category);
  RFieldType rfld(xfld, order, basis_category);
  QField_BTrace lgfld(xfld, order, basis_category);

  // set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n)= {10*pow(-1, n%2) + n, 10*pow(-1, n%2) + n};

  // set some non-zero lagrange multiplier/boundary solution
  for (int n = 0; n < lgfld.nDOF(); n++)
    lgfld.DOF(n)= {1*(-n%3) - 1, (n%3) + 1};

  // set some non-zero solution
  for (int n = 0; n < rfld.nDOF(); n++)
    for (int d = 0; d < PhysDim::D; d++)
      rfld.DOF(n)[d] = {10*pow(-1, n%2) + n + d, 10*pow(-1, n%2) + n + d};

  // loop over cell groups in the mesh
  for (int group = 0; group < xfld.nCellGroups(); group++)
  {
    // grab the group and its metadata
    const XFieldCellGroupType& xfldCellGroup = xfld.template getCellGroup<Topology>(group);
    const int nElem = xfldCellGroup.nElem();

    const XField_CellToTrace<PhysDim, TopoDim> xfld_connectivity(xfld);

    // loop over all the elements in the cell group
    for (int elem = 0; elem < nElem; elem++)
    {
      // only consider possessed elements
      if (xfldCellGroup.associativity(elem).rank() != comm_rank) continue;

      // get ready to store local patches
      typedef XField_LocalPatch<PhysDim, Topology> XFieldLocal;

      // when we split, we loop over edges to split, otherwise just go through once
      for (int edge= 0; edge < (isSplit ? nEdge : 1); edge++)
      {
        std::unique_ptr<XFieldLocal> pxfld_local;
        // in the absence of splitting, grab the unsplit local patch
        XField_LocalPatchConstructor<PhysDim, Topology> xfld_construct(comm_local, xfld_connectivity, group, elem, SpaceType::Discontinuous);
        if (!isSplit)
          pxfld_local= make_unique<XFieldLocal>(xfld_construct);
        else
          pxfld_local= make_unique<XFieldLocal>(xfld_construct, ElementSplitType::Edge, edge);

        const XFieldLocal& xfld_local = *pxfld_local;

        // then grab the solution on the local patch
        Field_Local<QField_Cell> qfld_local(xfld_local, qfld, order, basis_category);
        Field_Local<QField_BTrace> lgfld_local(xfld_local, lgfld, order, basis_category );
        FieldLift_Local<RFieldType> rfld_local(xfld_local, rfld, order, basis_category );

        // and make sure that all solution transfers go through appropriately
        checkLocalField( qfld_local, qfld, small_tol, close_tol, isSplit );
        checkLocalField( rfld_local, rfld, small_tol, close_tol, isSplit );
        checkLocalField( lgfld_local, lgfld, small_tol, close_tol, isSplit );
      }
    }
  }
}

// A helper function to construct a Solver interface and test all the edges on a grid
template < class PhysDim, class TopoDim >
void localEdgeExtractAndTransfer(const XField<PhysDim, TopoDim>& xfld, const BasisFunctionCategory basis_category,
                             const int order, const Real small_tol, const Real close_tol, const bool isSplit )
{
  typedef typename Simplex<TopoDim>::type Topology;

  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysDim, TopoDim, ArrayQ > QField_Cell;
  typedef Field_CG_BoundaryTrace< PhysDim, TopoDim, ArrayQ > QField_BTrace;

  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

  typedef typename QField_Cell::template FieldCellGroupType<Topology> QFieldCellGroupType;

  const int nEdge = Topology::NEdge; // Number of different edges
  typedef typename std::set<std::array<int,Line::NNode>> EdgeList;

  const int comm_rank = xfld.comm()->rank();
  mpi::communicator comm_local = xfld.comm()->split(comm_rank);

  QField_Cell qfld(xfld, order, basis_category);
  QField_BTrace lgfld(xfld, order, basis_category);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {10*pow(-1,n%2)+n, 10*pow(-1,n%2)+n};

  //Set some non-zero solution
  for (int n = 0; n < lgfld.nDOF(); n++)
    lgfld.DOF(n) = {1*(-n%3)-1, (n%3)+1};

  ///////////////////////
  // LOOP OVER CELL GROUPS ->
  ///////////////////////
  const int (*EdgeNodes)[Line::NNode] = ElementEdges<Topology>::EdgeNodes;
  for (int group = 0; group  < xfld.nCellGroups(); group++)
  {
    EdgeList edge_list;

    // edge_list is cleared with each loop over the groups
    XFieldCellGroupType xfldCellGroup = xfld.template getCellGroup<Topology>(group);
    QFieldCellGroupType qfldCellGroup = qfld.template getCellGroup<Topology>(group);
    const int nElem = xfldCellGroup.nElem();

    ElementXFieldClass xfldElem(xfldCellGroup.basis() );

    ///////////////////////
    // EDGE LIST EXTRACTION
    ///////////////////////

    for ( int elem = 0; elem < nElem; elem++ )
    {
      xfldCellGroup.getElement(xfldElem,elem);
      std::array<int,Topology::NNode> xnodeMap;
      std::array<int,Topology::NNode> qnodeMap;

      xfldCellGroup.associativity(elem).getNodeGlobalMapping( xnodeMap.data(), xnodeMap.size() );
      qfldCellGroup.associativity(elem).getNodeGlobalMapping( qnodeMap.data(), qnodeMap.size() );

      for ( int edge = 0; edge < nEdge; edge++ )
      {
        // loop over the edges
        std::array<int,2> xEdge;
        xEdge[0] = xnodeMap[ EdgeNodes[edge][0] ];
        xEdge[1] = xnodeMap[ EdgeNodes[edge][1] ];

        std::array<int,2> qEdge;
        qEdge[0] = qnodeMap[ EdgeNodes[edge][0] ];
        qEdge[1] = qnodeMap[ EdgeNodes[edge][1] ];

        if (xfld.local2nativeDOFmap(xEdge[0]) > xfld.local2nativeDOFmap(xEdge[1]))
        {
          std::swap(xEdge[0], xEdge[1]);
          std::swap(qEdge[0], qEdge[1]);
        }

        // only consider edges where the primary node is possessed
        if (qEdge[0] >= qfld.nDOFpossessed()) continue;

        edge_list.insert(xEdge);
      }
    }

    /////////////////////////////////////////////
    // EDGE LOCAL MESH CONSTRUCT
    /////////////////////////////////////////////

    const XField_CellToTrace<PhysDim,TopoDim> xfld_connectivity(xfld);
    const Field_NodalView nodalView(xfld, {group});

    typedef typename Field_NodalView::IndexVector IndexVector;

    for ( const std::array<int,Line::NNode>& edge : edge_list )
    {
      /////////////////////////////////////////////
      // EDGE LOCAL MESH CONSTRUCT
      /////////////////////////////////////////////

      IndexVector nodeGroup0, nodeGroup1;
      nodeGroup0 = nodalView.getCellList( edge[0] );
      nodeGroup1 = nodalView.getCellList( edge[1] );

      // find the interesction of the two sets
      std::set<GroupElemIndex> attachedCells;
      typename std::set<GroupElemIndex>::iterator attachedCells_it;
      std::set_intersection( nodeGroup0.begin(), nodeGroup0.end(), nodeGroup1.begin(), nodeGroup1.end(),
                             std::inserter( attachedCells, attachedCells.begin() ) );

      typedef XField_LocalPatch<PhysDim,Topology> XFieldLocal;
      std::unique_ptr<XFieldLocal> pxfld_local;
      if (isSplit)
      {
        // create a split patch
        pxfld_local = make_unique<XFieldLocal>( comm_local, xfld_connectivity, edge, SpaceType::Continuous, &nodalView );
      }
      else
      {
        // create an unsplit patch
        XField_LocalPatchConstructor<PhysDim,Topology> xfld_local(comm_local, xfld_connectivity, edge, SpaceType::Continuous, &nodalView );
        pxfld_local = make_unique<XFieldLocal>( xfld_local );
      }

      const XFieldLocal& xfld_local = *pxfld_local;

      /////////////////////////////////////////////
      // LOCAL SOLUTION TRANSFER
      /////////////////////////////////////////////

      Field_Local<QField_Cell> qfld_local(xfld_local, qfld, order, basis_category);
      Field_Local<QField_BTrace> lgfld_local(xfld_local, lgfld, order, basis_category );

      checkLocalField( qfld_local, qfld, small_tol, close_tol, isSplit );
      checkLocalField( lgfld_local, lgfld, small_tol, close_tol, isSplit );
    }
  }
}

} // namespace SANS
