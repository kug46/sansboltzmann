// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Field3D_EdgeLocal_CG_btest
//

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "Field/FieldSpacetime_CG_Cell.h"
#include "Field/FieldSpacetime_CG_BoundaryTrace.h"

#include "Field_LocalPatch_toolkit_btest.h"

#include "unit/UnitGrids/XField4D_24Ptope_X1_1Group.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

//Explicitly instantiate classes to get proper coverage information
namespace SANS
{
template class Field_CG_Cell< PhysD4, TopoD4, Real >;
template class Field_CG_BoundaryTrace< PhysD4, TopoD4, Real >;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( Field4D_LocalPatch_Edge_CG_test_suite )

// Simpler Orientation tests

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Lagrange_24Pentatope_P1 )
{
  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-8;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = false;

  XField4D_24Ptope_X1_1Group xfld;
  localEdgeExtractAndTransfer<PhysD4,TopoD4>( xfld, basis_category, order, small_tol, close_tol, split_test );

  split_test = true;

  localEdgeExtractAndTransfer<PhysD4,TopoD4>( xfld, basis_category, order, small_tol, close_tol, split_test );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Lagrange_24Pentatope_P2 )
{
  int order = 2;
  const Real small_tol = 1e-10, close_tol = 1e-8;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = false;

  XField4D_24Ptope_X1_1Group xfld;
  localEdgeExtractAndTransfer<PhysD4,TopoD4>( xfld, basis_category, order, small_tol, close_tol, split_test );

  split_test = true;

  localEdgeExtractAndTransfer<PhysD4,TopoD4>( xfld, basis_category, order, small_tol, close_tol, split_test );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
