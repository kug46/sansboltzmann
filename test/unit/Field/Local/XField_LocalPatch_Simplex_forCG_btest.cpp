// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Note: unit grids tested in "unit/UnitGrids/UnitGrids_btest.cpp"

#include <ostream>
#include <utility> // std::pair

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "Topology/ElementTopology.h"

#include "BasisFunction/ElementEdges.h"

#include "Field/Field_NodalView.h"
#include "Field/XField_CellToTrace.h"
#include "Field/Local/XField_LocalPatch.h"

#include "unit/UnitGrids/XField_KuhnFreudenthal.h"
#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

template<typename type>
bool
vectorHas( const std::vector<type>& v, type value )
{
  if (std::find(v.begin(),v.end(),value)==v.end())
    return false;
  return true;
}

template<class PhysDim,class TopoDim>
void
listInteriorGroups( const XField<PhysDim,TopoDim>& xfld, std::vector<std::pair<int,int>>& groups )
{
  // lists interior trace groups by pairs of left and right elements
  groups.clear();
  for (int igroup=0;igroup<xfld.nInteriorTraceGroups();igroup++)
    groups.push_back( {xfld.getInteriorTraceGroupBase(igroup).getGroupLeft(),
                       xfld.getInteriorTraceGroupBase(igroup).getGroupRight() } );
}


// helper function to retrieve a main cell on a specific boundary group
// can also return an entirely interior
template<class PhysDim,class TopoDim,class Topology>
class UnitTestHelper
{
  typedef typename Topology::TopologyTrace TraceTopology;
  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TraceTopology> XFieldTraceGroupType;

public:
  UnitTestHelper( const XField<PhysDim,TopoDim>& xfld, const XField_CellToTrace<PhysDim,TopoDim>& connectivity) :
    xfld_(xfld), connectivity_(connectivity)
  {}

  std::pair<int,int> getBoundaryCell( int bgroup )
  {
    // returns a cell on bgroup boundary
    const XFieldTraceGroupType& group = xfld_.template getBoundaryTraceGroup<TraceTopology>(bgroup);
    int groupL = group.getGroupLeft();
    int elemL = group.getElementLeft(0); // return the first element
    return {groupL,elemL};
  }

  std::pair<int,int> getInteriorCell_withTraceNeighbourBoundary( int bgroup )
  {
    const XFieldTraceGroupType& group = xfld_.template getBoundaryTraceGroup<TraceTopology>(bgroup);
    int groupL = group.getGroupLeft();
    for (int ielem=0;ielem<group.nElem();ielem++)
    {
      int elemL = group.getElementLeft(ielem);
      // pick the first trace that isn't on the boundary
      for (int j=0;j<Topology::NTrace;j++)
      {
        TraceInfo info = connectivity_.getTrace(groupL,elemL,j);
        if (info.type==TraceInfo::Boundary) continue;
        // retrieve the interior trace group
        const XFieldTraceGroupType& interior_group = xfld_.template getInteriorTraceGroup<TraceTopology>(info.group);

        int groupR,elemR;
        if (interior_group.getGroupLeft()==groupL)
        {
          groupR = interior_group.getGroupRight();
          elemR  = interior_group.getElementRight(info.elem);
        }
        else
        {
          groupR = interior_group.getGroupLeft();
          elemR  = interior_group.getElementLeft(info.elem);
        }
        if (groupR<0) continue;
        return {groupR,elemR};
      }
    }
    SANS_DEVELOPER_EXCEPTION("did not find interior cell with boundary trace neighbour");
    return {-1,-1};
  }

  bool touchesBoundary( int group, int elem )
  {
    for (int j=0;j<Topology::NTrace;j++)
    {
      TraceInfo info = connectivity_.getTrace(group,elem,j);
      if (info.type==TraceInfo::Boundary) return true;
    }
    return false;
  }

  std::pair<int,int> getFullyInteriorCell( const Field_NodalView& nodalview )
  {
    // returns a fully interior cell where node neighbours don't even touch boundaries
    std::vector<int> cellDOF(Topology::NNode);
    for (int igroup=0;igroup<xfld_.nCellGroups();igroup++)
    {
      const XFieldCellGroupType& group = xfld_.template getCellGroup<Topology>(igroup);
      for (int ielem=0;ielem<group.nElem();ielem++)
      {
        // retrieve the dof
        group.associativity(ielem).getNodeGlobalMapping(cellDOF.data(),cellDOF.size());

        // use the nodal view to get cells attached to this node
        bool ok = true;
        for (std::size_t j=0;j<cellDOF.size();j++)
        {
          std::vector<GroupElemIndex> ball = nodalview.getCellList( cellDOF[j] );

          for (std::size_t i=0;i<ball.size();i++)
          {
            if (touchesBoundary(ball[i].group,ball[i].elem))
            {
              ok = false;
              break;
            }
          }
          if (!ok) break; // skip the rest of the node checks
        }

        if (ok) return {igroup,ielem};
      }
    }
    SANS_DEVELOPER_EXCEPTION("could not find fully interior cell");
    return {-1,-1};
  }

  void listBoundaryGroups( int group, int elem, std::vector<int>& groups, bool clear=true )
  {
    // lists the boundary groups the input cell touches (via the traces)
    // option to restart the list or concatenate
    if (clear) groups.clear();
    for (int j=0;j<Topology::NTrace;j++)
    {
      TraceInfo info = connectivity_.getTrace(group,elem,j);
      if (info.type==TraceInfo::Boundary && !vectorHas(groups,info.group))
        groups.push_back(info.group);
    }
  }

  std::pair<int,int> getBoundaryEdge( int group, int elem )
  {
    const int (*edges)[Line::NNode] = ElementEdges<TraceTopology>::EdgeNodes;
    std::vector<int> traceDOF( TraceTopology::NNode );
    for (int j=0;j<Topology::NTrace;j++)
    {
      TraceInfo info = connectivity_.getTrace(group,elem,j);
      if (info.type==TraceInfo::Boundary)
      {
        // any edge of the trace topology will do, pick the first one
        xfld_.template getBoundaryTraceGroup<TraceTopology>(info.group).associativity(info.elem).
                                                           getNodeGlobalMapping(traceDOF.data(),traceDOF.size());
        return {traceDOF[edges[0][0]],traceDOF[edges[0][1]]};
      }
    }
    SANS_DEVELOPER_EXCEPTION("did not find a boundary edge?");
    return {-1,-1};
  }

  std::pair<int,int> getInteriorEdge( int group, int elem )
  {
    const int (*edges)[Line::NNode] = ElementEdges<Topology>::EdgeNodes;
    const int (*traceEdges)[Line::NNode] = ElementEdges<TraceTopology>::EdgeNodes;

    std::vector<int> cellDOF( Topology::NNode );
    xfld_.template getCellGroup<Topology>(group).associativity(elem).
                                                      getNodeGlobalMapping(cellDOF.data(),cellDOF.size());

    std::set< std::pair<int,int> > cellEdges;
    for (int iedge=0;iedge<Topology::NEdge;iedge++)
    {
      std::pair<int,int> edge = {cellDOF[edges[iedge][0]],cellDOF[edges[iedge][1]]};
      if (edge.first>edge.second) std::swap(edge.first,edge.second);
      cellEdges.insert(edge);
    }

    // loop through the traces and flag any that are on the boundary
    std::vector<int> traceDOF( TraceTopology::NNode );
    for (int j=0;j<Topology::NTrace;j++)
    {
      TraceInfo info = connectivity_.getTrace(group,elem,j);
      if (info.type==TraceInfo::Boundary)
      {
        // any edge of the trace topology will do, pick the first one
        xfld_.template getBoundaryTraceGroup<TraceTopology>(info.group).associativity(info.elem).
                                                           getNodeGlobalMapping(traceDOF.data(),traceDOF.size());
        // any edge on this trace DOF is on a boundary
        for (int iedge=0;iedge<TraceTopology::NEdge;iedge++)
        {
          std::pair<int,int> edge = {traceDOF[traceEdges[iedge][0]],traceDOF[traceEdges[iedge][1]]};
          if (edge.first>edge.second) std::swap(edge.first,edge.second);

          std::set< std::pair<int,int> >::iterator it = cellEdges.find(edge);
          if (it==cellEdges.end()) continue; // we may have already erased it (in 3d or 4d)
          cellEdges.erase(it);
        }
      }
    }

    if (cellEdges.size()==0)
      SANS_DEVELOPER_EXCEPTION("there are no interior edges");
    return *cellEdges.begin();
  }

private:
  const XField<PhysDim,TopoDim>& xfld_;
  const XField_CellToTrace<PhysDim,TopoDim>& connectivity_;
};

//############################################################################//
BOOST_AUTO_TEST_SUITE( XField_LocalPatch_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField_LocalPatch_Triangle_Continuous_test )
{

  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;
  typedef Triangle Topology;

  const int RESOLVE_CELL_GROUP = 0;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  // change the mesh to whatever you want, the unit test should still work
  // (as long as it has the correct number of boundary groups and is large enough)
  XField2D_Box_Triangle_X1 xfld(20,20);

  // build cell to trace connectivity structure
  XField_CellToTrace<PhysDim,TopoDim> connectivity(xfld);

  // define the helper
  UnitTestHelper<PhysDim,TopoDim,Topology> helper(xfld,connectivity);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // create the local patch xfield around some boundary cell
  {
    std::pair<int,int> global_elem = helper.getBoundaryCell( 0 ); // boundary group 0
    XField_LocalPatchConstructor<PhysDim,Topology> xfld_local(comm_local,xfld,connectivity,&nodalview);

    // check initial settings
    BOOST_CHECK_EQUAL( xfld_local.nResolve(), 0 );
    BOOST_CHECK_EQUAL( xfld_local.nFixed(), 0 );
    BOOST_CHECK_EQUAL( xfld_local.nDOF(), 0 );
    BOOST_CHECK( xfld_local.spaceType() == SpaceType::Discontinuous ); // default
    BOOST_CHECK_EQUAL( xfld_local.mainCellNodes().size(), 0 ); // not yet defined

    xfld_local.addResolveElement( global_elem.first, global_elem.second );
    BOOST_CHECK_EQUAL( xfld_local.nResolve(), 1 );
    BOOST_CHECK_EQUAL( xfld_local.mainCellNodes().size(), 0 ); // still not yet defined

    TraceInfo info;
    info.type = TraceInfo::Interior;
    BOOST_CHECK_EQUAL( xfld_local.isBoundaryTrace(info,RESOLVE_CELL_GROUP,0), false );
    info.type = TraceInfo::Boundary;
    BOOST_CHECK_EQUAL( xfld_local.isBoundaryTrace(info,RESOLVE_CELL_GROUP,0), true );

    // get all the boundary groups the main cell touches
    std::vector<int> groups;
    helper.listBoundaryGroups( global_elem.first, global_elem.second, groups );
    BOOST_REQUIRE( groups.size() > 0 );

    // add the trace neighbours
    xfld_local.addSharedNodeNeighbours();
    BOOST_CHECK( xfld_local.spaceType() == SpaceType::Continuous ); // modified

    // extract the local mesh
    xfld_local.extract();

    // there is always one ghost boundary trace group and less than 2 interior trace groups
    // we have less than two interior trace groups because there should be no 0-0 group
    BOOST_CHECK_EQUAL( (xfld_local.nGhostBoundaryTraceGroups()), 1 );
    BOOST_CHECK( (xfld_local.nInteriorTraceGroups()) <= 2 );

    std::vector< std::pair<int,int> > interiorGroups;
    listInteriorGroups(xfld_local,interiorGroups);

    // there should be >= boundary trace groups because some might be split across resolve/fixed cell group
    BOOST_CHECK( (xfld_local.nBoundaryTraceGroups()) >= (int)groups.size() );
    for (int j=0;j<xfld_local.nBoundaryTraceGroups();j++)
      BOOST_CHECK_EQUAL( vectorHas(groups, xfld_local.globalBoundaryGroup(j)), true );

    // split an interior edge
    {
      std::pair<int,int> edge = helper.getInteriorEdge( global_elem.first, global_elem.second );

      XField_LocalPatch<PhysDim,Topology> xfld_local_split_bnd( xfld_local, edge.first, edge.second );

      // check the DOF and element counts
      BOOST_CHECK_EQUAL( xfld_local.nDOF()+1, xfld_local_split_bnd.nDOF() );
      std::set< std::pair<int,int> > cavity;
      int e0 = xfld_local.global2localDOFmap().at(edge.first);
      int e1 = xfld_local.global2localDOFmap().at(edge.second);
      XField_LocalPatch_Utils::allWithEdge<PhysDim,Topology>( xfld_local, NULL, e0, e1, cavity );
      int nElem = xfld_local_split_bnd.getCellGroupBase(0).nElem() + xfld_local_split_bnd.getCellGroupBase(1).nElem();
      BOOST_CHECK_EQUAL( xfld_local.nFixed() + xfld_local.nResolve() + cavity.size(), nElem );

      // there is always one ghost boundary trace group and less than 3 interior trace groups
      // we might have 3 interior trace groups now because of the newly introduced 0-0 group
      BOOST_CHECK_EQUAL( (xfld_local_split_bnd.nGhostBoundaryTraceGroups()), 1 );
      BOOST_CHECK( (xfld_local_split_bnd.nInteriorTraceGroups()) <= 3 );

      // we must have all the same interior groups plus the {0,0} one
      std::vector< std::pair<int,int> > interiorGroups_split;
      listInteriorGroups(xfld_local_split_bnd,interiorGroups_split);
      for (std::size_t j=0;j<interiorGroups.size();j++)
        BOOST_CHECK_EQUAL( vectorHas(interiorGroups_split,interiorGroups[j]), true );
      BOOST_CHECK_EQUAL( vectorHas(interiorGroups_split,{0,0} ), true );

      // make sure we have the same boundary groups or more (because the might be split across cell groups)
      BOOST_CHECK( (xfld_local_split_bnd.nBoundaryTraceGroups()) >= (int) groups.size() );
      for (int j=0;j<xfld_local_split_bnd.nBoundaryTraceGroups();j++)
        BOOST_CHECK_EQUAL( vectorHas(groups, xfld_local_split_bnd.globalBoundaryGroup(j)), true );

    }

    // split a boundary edge
    {
      std::pair<int,int> edge = helper.getBoundaryEdge( global_elem.first, global_elem.second );

      XField_LocalPatch<PhysDim,Topology> xfld_local_split_bnd( xfld_local, edge.first, edge.second );

      // check the DOF and element counts
      BOOST_CHECK_EQUAL( xfld_local.nDOF()+1, xfld_local_split_bnd.nDOF() );
      std::set< std::pair<int,int> > cavity;
      int e0 = xfld_local.global2localDOFmap().at(edge.first);
      int e1 = xfld_local.global2localDOFmap().at(edge.second);
      XField_LocalPatch_Utils::allWithEdge<PhysDim,Topology>( xfld_local, NULL, e0, e1, cavity );
      int nElem = xfld_local_split_bnd.getCellGroupBase(0).nElem() + xfld_local_split_bnd.getCellGroupBase(1).nElem();
      BOOST_CHECK_EQUAL( xfld_local.nFixed() + xfld_local.nResolve() + cavity.size(), nElem );

      // there is always one ghost boundary trace group and less than 3 interior trace groups
      // we might have 3 interior trace groups now because of the newly introduced 0-0 group
      BOOST_CHECK_EQUAL( (xfld_local_split_bnd.nGhostBoundaryTraceGroups()), 1 );
      BOOST_CHECK( (xfld_local_split_bnd.nInteriorTraceGroups()) <= 3 );

      // we must have all the same interior groups plus the {0,0} one
      std::vector< std::pair<int,int> > interiorGroups_split;
      listInteriorGroups(xfld_local_split_bnd,interiorGroups_split);
      for (std::size_t j=0;j<interiorGroups.size();j++)
        BOOST_CHECK_EQUAL( vectorHas(interiorGroups_split,interiorGroups[j]), true );
      BOOST_CHECK_EQUAL( vectorHas(interiorGroups_split,{0,0} ), true );

      // make sure we have the same boundary groups (or more if they are split across cell groups)
      BOOST_CHECK( (xfld_local_split_bnd.nBoundaryTraceGroups()) >= (int)groups.size() );
      for (int j=0;j<xfld_local_split_bnd.nBoundaryTraceGroups();j++)
        BOOST_CHECK_EQUAL( vectorHas(groups, xfld_local_split_bnd.globalBoundaryGroup(j)), true );
    }
  }

  // extract a local patch about a cell whose neighbour touches a boundary
  {
    std::pair<int,int> global_elem = helper.getInteriorCell_withTraceNeighbourBoundary( 0 ); // boundary group 0
    XField_LocalPatchConstructor<PhysDim,Topology> xfld_local(comm_local,xfld,connectivity,&nodalview);

    // check initial settings
    BOOST_CHECK_EQUAL( xfld_local.nResolve(), 0 );
    BOOST_CHECK_EQUAL( xfld_local.nFixed(), 0 );
    BOOST_CHECK_EQUAL( xfld_local.nDOF(), 0 );
    BOOST_CHECK( xfld_local.spaceType() == SpaceType::Discontinuous ); // default
    BOOST_CHECK_EQUAL( xfld_local.mainCellNodes().size(), 0 ); // not yet defined

    xfld_local.addResolveElement( global_elem.first, global_elem.second );
    BOOST_CHECK_EQUAL( xfld_local.nResolve(), 1 );
    BOOST_CHECK_EQUAL( xfld_local.mainCellNodes().size(), 0 ); // still not yet defined

    TraceInfo info;
    info.type = TraceInfo::Interior;
    BOOST_CHECK_EQUAL( xfld_local.isBoundaryTrace(info,RESOLVE_CELL_GROUP,0), false );
    info.type = TraceInfo::Boundary;
    BOOST_CHECK_EQUAL( xfld_local.isBoundaryTrace(info,RESOLVE_CELL_GROUP,0), true );

    // get all the boundary groups the main cell touches
    std::vector<int> groups;
    helper.listBoundaryGroups( global_elem.first, global_elem.second, groups );

    // add the trace neighbours
    xfld_local.addSharedNodeNeighbours();
    BOOST_CHECK( xfld_local.spaceType() == SpaceType::Continuous ); // changed

    // extract the local mesh
    xfld_local.extract();

    // there is always one ghost boundary trace group and less than 2 interior trace groups
    // we have less than two interior trace groups because there should be no 0-0 group
    BOOST_CHECK_EQUAL( (xfld_local.nGhostBoundaryTraceGroups()), 1 );
    BOOST_CHECK( (xfld_local.nInteriorTraceGroups()) <= 2 );

    std::vector< std::pair<int,int> > interiorGroups;
    listInteriorGroups(xfld_local,interiorGroups);

    // make sure we have the same boundary groups (or more if they are split across cell groups)
    BOOST_CHECK( (xfld_local.nBoundaryTraceGroups()) >= (int) groups.size() );
    std::vector<int> bgroups = xfld_local.globalBoundaryGroups();
    BOOST_CHECK( (xfld_local.nBoundaryTraceGroups() == (int) bgroups.size()) );
    for (std::size_t j=0;j<groups.size();j++)
      BOOST_CHECK_EQUAL( vectorHas( bgroups, groups[j] ), true );

    // split an interior edge...there is no boundary edge to split
    {
      std::pair<int,int> edge = helper.getInteriorEdge( global_elem.first, global_elem.second );

      XField_LocalPatch<PhysDim,Topology> xfld_local_split_bnd( xfld_local, edge.first, edge.second );

      // check the DOF and element counts
      BOOST_CHECK_EQUAL( xfld_local.nDOF()+1, xfld_local_split_bnd.nDOF() );
      std::set< std::pair<int,int> > cavity;
      int e0 = xfld_local.global2localDOFmap().at(edge.first);
      int e1 = xfld_local.global2localDOFmap().at(edge.second);
      XField_LocalPatch_Utils::allWithEdge<PhysDim,Topology>( xfld_local, NULL, e0, e1, cavity );
      int nElem = xfld_local_split_bnd.getCellGroupBase(0).nElem() + xfld_local_split_bnd.getCellGroupBase(1).nElem();
      BOOST_CHECK_EQUAL( xfld_local.nFixed() + xfld_local.nResolve() + cavity.size(), nElem );

      // there is always one ghost boundary trace group and less than 3 interior trace groups
      // we might have 3 interior trace groups now because of the newly introduced 0-0 group
      BOOST_CHECK_EQUAL( (xfld_local_split_bnd.nGhostBoundaryTraceGroups()), 1 );
      BOOST_CHECK( (xfld_local_split_bnd.nInteriorTraceGroups()) <= 3 );

      // we must have all the same interior groups plus the {0,0} one
      std::vector< std::pair<int,int> > interiorGroups_split;
      listInteriorGroups(xfld_local_split_bnd,interiorGroups_split);
      for (std::size_t j=0;j<interiorGroups.size();j++)
        BOOST_CHECK_EQUAL( vectorHas(interiorGroups_split,interiorGroups[j]), true );
      BOOST_CHECK_EQUAL( vectorHas(interiorGroups_split,{0,0} ), true );

      // make sure we have the same boundary groups
      BOOST_CHECK_EQUAL( (xfld_local_split_bnd.nBoundaryTraceGroups()), bgroups.size() );
      for (int j=0;j<xfld_local_split_bnd.nBoundaryTraceGroups();j++)
        BOOST_CHECK_EQUAL( vectorHas(bgroups, xfld_local_split_bnd.globalBoundaryGroup(j)), true );
    }
  }

  // extract a local patch about a completely interior cell
  {
    std::pair<int,int> global_elem = helper.getFullyInteriorCell(nodalview);
    XField_LocalPatchConstructor<PhysDim,Topology> xfld_local(comm_local,xfld,connectivity,&nodalview);

    // check initial settings
    BOOST_CHECK_EQUAL( xfld_local.nResolve(), 0 );
    BOOST_CHECK_EQUAL( xfld_local.nFixed(), 0 );
    BOOST_CHECK_EQUAL( xfld_local.nDOF(), 0 );
    BOOST_CHECK( xfld_local.spaceType() == SpaceType::Discontinuous ); // default
    BOOST_CHECK_EQUAL( xfld_local.mainCellNodes().size(), 0 ); // not yet defined

    xfld_local.addResolveElement( global_elem.first, global_elem.second );
    BOOST_CHECK_EQUAL( xfld_local.nResolve(), 1 );
    BOOST_CHECK_EQUAL( xfld_local.mainCellNodes().size(), 0 ); // still not yet defined

    TraceInfo info;
    info.type = TraceInfo::Interior;
    BOOST_CHECK_EQUAL( xfld_local.isBoundaryTrace(info,RESOLVE_CELL_GROUP,0), false );
    info.type = TraceInfo::Boundary;
    BOOST_CHECK_EQUAL( xfld_local.isBoundaryTrace(info,RESOLVE_CELL_GROUP,0), true );

    // get all the boundary groups the main cell touches
    std::vector<int> groups;
    helper.listBoundaryGroups( global_elem.first, global_elem.second, groups );
    BOOST_CHECK_EQUAL( groups.size(), 0 ); // fully interior cell -> no boundary groups

    // add the trace neighbours
    xfld_local.addSharedNodeNeighbours();

    // extract the local mesh
    xfld_local.extract();

    // there is always one ghost boundary trace group and less than 2 interior trace groups
    // we have less than two interior trace groups because there should be no 0-0 group
    BOOST_CHECK_EQUAL( (xfld_local.nGhostBoundaryTraceGroups()), 1 );
    BOOST_CHECK( (xfld_local.nInteriorTraceGroups()) <= 2 );

    std::vector< std::pair<int,int> > interiorGroups;
    listInteriorGroups(xfld_local,interiorGroups);

    // make sure we have the same boundary groups
    BOOST_CHECK_EQUAL( (xfld_local.nBoundaryTraceGroups()), groups.size() );
    for (int j=0;j<xfld_local.nBoundaryTraceGroups();j++)
      BOOST_CHECK_EQUAL( vectorHas(groups, xfld_local.globalBoundaryGroup(j)), true );

    // split an interior edge...there is no boundary edge to split
    {
      std::pair<int,int> edge = helper.getInteriorEdge( global_elem.first, global_elem.second );

      XField_LocalPatch<PhysDim,Topology> xfld_local_split_bnd( xfld_local, edge.first, edge.second );

      // check the DOF and element counts
      BOOST_CHECK_EQUAL( xfld_local.nDOF()+1, xfld_local_split_bnd.nDOF() );
      std::set< std::pair<int,int> > cavity;
      int e0 = xfld_local.global2localDOFmap().at(edge.first);
      int e1 = xfld_local.global2localDOFmap().at(edge.second);
      XField_LocalPatch_Utils::allWithEdge<PhysDim,Topology>( xfld_local, NULL, e0, e1, cavity );
      int nElem = xfld_local_split_bnd.getCellGroupBase(0).nElem() + xfld_local_split_bnd.getCellGroupBase(1).nElem();
      BOOST_CHECK_EQUAL( xfld_local.nFixed() + xfld_local.nResolve() + cavity.size(), nElem );

      // there is always one ghost boundary trace group and less than 3 interior trace groups
      // we might have 3 interior trace groups now because of the newly introduced 0-0 group
      BOOST_CHECK_EQUAL( (xfld_local_split_bnd.nGhostBoundaryTraceGroups()), 1 );
      BOOST_CHECK( (xfld_local_split_bnd.nInteriorTraceGroups()) <= 3 );

      // we must have all the same interior groups plus the {0,0} one
      std::vector< std::pair<int,int> > interiorGroups_split;
      listInteriorGroups(xfld_local_split_bnd,interiorGroups_split);
      for (std::size_t j=0;j<interiorGroups.size();j++)
        BOOST_CHECK_EQUAL( vectorHas(interiorGroups_split,interiorGroups[j]), true );
      BOOST_CHECK_EQUAL( vectorHas(interiorGroups_split,{0,0} ), true );

      // make sure we have the same boundary groups
      BOOST_CHECK_EQUAL( (xfld_local_split_bnd.nBoundaryTraceGroups()), groups.size() );
      for (int j=0;j<xfld_local_split_bnd.nBoundaryTraceGroups();j++)
        BOOST_CHECK_EQUAL( vectorHas(groups, xfld_local_split_bnd.globalBoundaryGroup(j)), true );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField_LocalPatch_Tet_Continuous_test )
{
  typedef PhysD3 PhysDim;
  typedef TopoD3 TopoDim;
  typedef Tet Topology;

  const int RESOLVE_CELL_GROUP = 0;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  // change the mesh to whatever you want, the unit test should still work
  // (as long as it has the correct number of boundary groups and is large enough)
  XField3D_Box_Tet_X1 xfld(comm_local,10,10,10);

  // build cell to trace connectivity structure
  XField_CellToTrace<PhysDim,TopoDim> connectivity(xfld);

  // define the helper
  UnitTestHelper<PhysDim,TopoDim,Topology> helper(xfld,connectivity);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // create the local patch xfield around some boundary cell
  {
    std::pair<int,int> global_elem = helper.getBoundaryCell( 0 ); // boundary group 0
    XField_LocalPatchConstructor<PhysDim,Topology> xfld_local(comm_local,xfld,connectivity,&nodalview);

    // check initial settings
    BOOST_CHECK_EQUAL( xfld_local.nResolve(), 0 );
    BOOST_CHECK_EQUAL( xfld_local.nFixed(), 0 );
    BOOST_CHECK_EQUAL( xfld_local.nDOF(), 0 );
    BOOST_CHECK( xfld_local.spaceType() == SpaceType::Discontinuous ); // default
    BOOST_CHECK_EQUAL( xfld_local.mainCellNodes().size(), 0 ); // not yet defined

    xfld_local.addResolveElement( global_elem.first, global_elem.second );
    BOOST_CHECK_EQUAL( xfld_local.nResolve(), 1 );
    BOOST_CHECK_EQUAL( xfld_local.mainCellNodes().size(), 0 ); // still not yet defined

    TraceInfo info;
    info.type = TraceInfo::Interior;
    BOOST_CHECK_EQUAL( xfld_local.isBoundaryTrace(info,RESOLVE_CELL_GROUP,0), false );
    info.type = TraceInfo::Boundary;
    BOOST_CHECK_EQUAL( xfld_local.isBoundaryTrace(info,RESOLVE_CELL_GROUP,0), true );

    // get all the boundary groups the main cell touches
    std::vector<int> groups;
    helper.listBoundaryGroups( global_elem.first, global_elem.second, groups );
    BOOST_REQUIRE( groups.size() > 0 );

    // add the trace neighbours
    xfld_local.addSharedNodeNeighbours();

    // extract the local mesh
    xfld_local.extract();

    // there is always one ghost boundary trace group and less than 2 interior trace groups
    // we have less than two interior trace groups because there should be no 0-0 group
    BOOST_CHECK_EQUAL( (xfld_local.nGhostBoundaryTraceGroups()), 1 );
    BOOST_CHECK( (xfld_local.nInteriorTraceGroups()) <= 2 );

    std::vector< std::pair<int,int> > interiorGroups;
    listInteriorGroups(xfld_local,interiorGroups);

    // make sure we have the same boundary groups
    BOOST_CHECK( (xfld_local.nBoundaryTraceGroups() >= (int)groups.size() ) );
    std::vector<int> bgroups = xfld_local.globalBoundaryGroups();
    for (int j=0;j<xfld_local.nBoundaryTraceGroups();j++)
      BOOST_CHECK_EQUAL( vectorHas(bgroups, xfld_local.globalBoundaryGroup(j)), true );

    // split a boundary edge
    {
      std::pair<int,int> edge = helper.getBoundaryEdge( global_elem.first, global_elem.second );

      XField_LocalPatch<PhysDim,Topology> xfld_local_split_bnd( xfld_local, edge.first, edge.second );

      // check the DOF and element counts
      BOOST_CHECK_EQUAL( xfld_local.nDOF()+1, xfld_local_split_bnd.nDOF() );
      std::set< std::pair<int,int> > cavity;
      int e0 = xfld_local.global2localDOFmap().at(edge.first);
      int e1 = xfld_local.global2localDOFmap().at(edge.second);
      XField_LocalPatch_Utils::allWithEdge<PhysDim,Topology>( xfld_local, NULL, e0, e1, cavity );
      int nElem = xfld_local_split_bnd.getCellGroupBase(0).nElem() + xfld_local_split_bnd.getCellGroupBase(1).nElem();
      BOOST_CHECK_EQUAL( xfld_local.nFixed() + xfld_local.nResolve() + cavity.size(), nElem );

      // there is always one ghost boundary trace group and less than 3 interior trace groups
      // we might have 3 interior trace groups now because of the newly introduced 0-0 group
      BOOST_CHECK_EQUAL( (xfld_local_split_bnd.nGhostBoundaryTraceGroups()), 1 );
      BOOST_CHECK( (xfld_local_split_bnd.nInteriorTraceGroups()) <= 3 );

      // we must have all the same interior groups plus the {0,0} one
      std::vector< std::pair<int,int> > interiorGroups_split;
      listInteriorGroups(xfld_local_split_bnd,interiorGroups_split);
      for (std::size_t j=0;j<interiorGroups.size();j++)
        BOOST_CHECK_EQUAL( vectorHas(interiorGroups_split,interiorGroups[j]), true );
      BOOST_CHECK_EQUAL( vectorHas(interiorGroups_split,{0,0} ), true );

      // make sure we have the same boundary groups
      BOOST_CHECK_EQUAL( (xfld_local_split_bnd.nBoundaryTraceGroups()),
                         (xfld_local.nBoundaryTraceGroups()) );
      for (int j=0;j<xfld_local_split_bnd.nBoundaryTraceGroups();j++)
        BOOST_CHECK_EQUAL( vectorHas(bgroups, xfld_local_split_bnd.globalBoundaryGroup(j)), true );
    }
  }

  // extract a local patch about a completely interior cell
  {
    std::pair<int,int> global_elem = helper.getFullyInteriorCell(nodalview);
    XField_LocalPatchConstructor<PhysDim,Topology> xfld_local(comm_local,xfld,connectivity,&nodalview);

    // check initial settings
    BOOST_CHECK_EQUAL( xfld_local.nResolve(), 0 );
    BOOST_CHECK_EQUAL( xfld_local.nFixed(), 0 );
    BOOST_CHECK_EQUAL( xfld_local.nDOF(), 0 );
    BOOST_CHECK( xfld_local.spaceType() == SpaceType::Discontinuous ); // default
    BOOST_CHECK_EQUAL( xfld_local.mainCellNodes().size(), 0 ); // not yet defined

    xfld_local.addResolveElement( global_elem.first, global_elem.second );
    BOOST_CHECK_EQUAL( xfld_local.nResolve(), 1 );
    BOOST_CHECK_EQUAL( xfld_local.mainCellNodes().size(), 0 ); // still not yet defined

    TraceInfo info;
    info.type = TraceInfo::Interior;
    BOOST_CHECK_EQUAL( xfld_local.isBoundaryTrace(info,RESOLVE_CELL_GROUP,0), false );
    info.type = TraceInfo::Boundary;
    BOOST_CHECK_EQUAL( xfld_local.isBoundaryTrace(info,RESOLVE_CELL_GROUP,0), true );

    // get all the boundary groups the main cell touches
    std::vector<int> groups;
    helper.listBoundaryGroups( global_elem.first, global_elem.second, groups );
    BOOST_CHECK_EQUAL( groups.size(), 0 ); // fully interior cell -> no boundary groups

    // add the trace neighbours
    xfld_local.addSharedNodeNeighbours();

    // extract the local mesh
    xfld_local.extract();

    // there is always one ghost boundary trace group and less than 2 interior trace groups
    // we have less than two interior trace groups because there should be no 0-0 group
    BOOST_CHECK_EQUAL( (xfld_local.nGhostBoundaryTraceGroups()), 1 );
    BOOST_CHECK( (xfld_local.nInteriorTraceGroups()) <= 2 );

    std::vector< std::pair<int,int> > interiorGroups;
    listInteriorGroups(xfld_local,interiorGroups);

    // make sure we have the same boundary groups
    BOOST_CHECK_EQUAL( (xfld_local.nBoundaryTraceGroups()), groups.size() );
    for (int j=0;j<xfld_local.nBoundaryTraceGroups();j++)
      BOOST_CHECK_EQUAL( vectorHas(groups, xfld_local.globalBoundaryGroup(j)), true );

    // split an interior edge...there is no boundary edge to split
    {
      std::pair<int,int> edge = helper.getInteriorEdge( global_elem.first, global_elem.second );

      XField_LocalPatch<PhysDim,Topology> xfld_local_split_bnd( xfld_local, edge.first, edge.second );

      // check the DOF and element counts
      BOOST_CHECK_EQUAL( xfld_local.nDOF()+1, xfld_local_split_bnd.nDOF() );
      std::set< std::pair<int,int> > cavity;
      int e0 = xfld_local.global2localDOFmap().at(edge.first);
      int e1 = xfld_local.global2localDOFmap().at(edge.second);
      XField_LocalPatch_Utils::allWithEdge<PhysDim,Topology>( xfld_local, NULL, e0, e1, cavity );
      int nElem = xfld_local_split_bnd.getCellGroupBase(0).nElem() + xfld_local_split_bnd.getCellGroupBase(1).nElem();
      BOOST_CHECK_EQUAL( xfld_local.nFixed() + xfld_local.nResolve() + cavity.size(), nElem );

      // there is always one ghost boundary trace group and less than 3 interior trace groups
      // we might have 3 interior trace groups now because of the newly introduced 0-0 group
      BOOST_CHECK_EQUAL( (xfld_local_split_bnd.nGhostBoundaryTraceGroups()), 1 );
      BOOST_CHECK( (xfld_local_split_bnd.nInteriorTraceGroups()) <= 3 );

      // we must have all the same interior groups plus the {0,0} one
      std::vector< std::pair<int,int> > interiorGroups_split;
      listInteriorGroups(xfld_local_split_bnd,interiorGroups_split);
      for (std::size_t j=0;j<interiorGroups.size();j++)
        BOOST_CHECK_EQUAL( vectorHas(interiorGroups_split,interiorGroups[j]), true );
      BOOST_CHECK_EQUAL( vectorHas(interiorGroups_split,{0,0} ), true );

      // make sure we have the same boundary groups
      BOOST_CHECK_EQUAL( (xfld_local_split_bnd.nBoundaryTraceGroups()), groups.size() );
      for (int j=0;j<xfld_local_split_bnd.nBoundaryTraceGroups();j++)
        BOOST_CHECK_EQUAL( vectorHas(groups, xfld_local_split_bnd.globalBoundaryGroup(j)), true );
    }
  }
}

//----------------------------------------------------------------------------//
#ifdef SANS_AVRO // needed for CKF triangulation
BOOST_AUTO_TEST_CASE( XField_LocalPatch_Pentatope_Continuous_test )
{
  typedef PhysD4 PhysDim;
  typedef TopoD4 TopoDim;
  typedef Pentatope Topology;

  const int RESOLVE_CELL_GROUP = 0;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  // change the mesh to whatever you want, the unit test should still work
  // (as long as it has the correct number of boundary groups and is large enough)
  int nx = 6; // this seems to be big enough
  XField_KuhnFreudenthal<PhysDim,TopoDim> xfld(comm_local,{nx,nx,nx,nx});

  // build cell to trace connectivity structure
  XField_CellToTrace<PhysDim,TopoDim> connectivity(xfld);

  // define the helper
  UnitTestHelper<PhysDim,TopoDim,Topology> helper(xfld,connectivity);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // create the local patch xfield around some boundary cell
  {
    std::pair<int,int> global_elem = helper.getBoundaryCell( 0 ); // boundary group 0
    XField_LocalPatchConstructor<PhysDim,Topology> xfld_local(comm_local,xfld,connectivity,&nodalview);

    // check initial settings
    BOOST_CHECK_EQUAL( xfld_local.nResolve(), 0 );
    BOOST_CHECK_EQUAL( xfld_local.nFixed(), 0 );
    BOOST_CHECK_EQUAL( xfld_local.nDOF(), 0 );
    BOOST_CHECK( xfld_local.spaceType() == SpaceType::Discontinuous ); // default
    BOOST_CHECK_EQUAL( xfld_local.mainCellNodes().size(), 0 ); // not yet defined

    xfld_local.addResolveElement( global_elem.first, global_elem.second );
    BOOST_CHECK_EQUAL( xfld_local.nResolve(), 1 );
    BOOST_CHECK_EQUAL( xfld_local.mainCellNodes().size(), 0 ); // still not yet defined

    TraceInfo info;
    info.type = TraceInfo::Interior;
    BOOST_CHECK_EQUAL( xfld_local.isBoundaryTrace(info,RESOLVE_CELL_GROUP,0), false );
    info.type = TraceInfo::Boundary;
    BOOST_CHECK_EQUAL( xfld_local.isBoundaryTrace(info,RESOLVE_CELL_GROUP,0), true );

    // get all the boundary groups the main cell touches
    std::vector<int> groups;
    helper.listBoundaryGroups( global_elem.first, global_elem.second, groups );
    BOOST_REQUIRE( groups.size() > 0 );

    // add the trace neighbours
    xfld_local.addSharedNodeNeighbours();

    // extract the local mesh
    xfld_local.extract();

    // there is always one ghost boundary trace group and less than 2 interior trace groups
    // we have less than two interior trace groups because there should be no 0-0 group
    BOOST_CHECK_EQUAL( (xfld_local.nGhostBoundaryTraceGroups()), 1 );
    BOOST_CHECK( (xfld_local.nInteriorTraceGroups()) <= 2 );

    std::vector< std::pair<int,int> > interiorGroups;
    listInteriorGroups(xfld_local,interiorGroups);

    // make sure we have the same boundary groups
    BOOST_CHECK( (xfld_local.nBoundaryTraceGroups()) >= (int)groups.size() );
    std::vector<int> bgroups = xfld_local.globalBoundaryGroups();
    for (int j=0;j<xfld_local.nBoundaryTraceGroups();j++)
      BOOST_CHECK_EQUAL( vectorHas(bgroups, xfld_local.globalBoundaryGroup(j)), true );

    // split a boundary edge
    {
      std::pair<int,int> edge = helper.getBoundaryEdge( global_elem.first, global_elem.second );

      XField_LocalPatch<PhysDim,Topology> xfld_local_split_bnd( xfld_local, edge.first, edge.second );

      // check the DOF and element counts
      BOOST_CHECK_EQUAL( xfld_local.nDOF()+1, xfld_local_split_bnd.nDOF() );
      std::set< std::pair<int,int> > cavity;
      int e0 = xfld_local.global2localDOFmap().at(edge.first);
      int e1 = xfld_local.global2localDOFmap().at(edge.second);
      XField_LocalPatch_Utils::allWithEdge<PhysDim,Topology>( xfld_local, NULL, e0, e1, cavity );
      int nElem = xfld_local_split_bnd.getCellGroupBase(0).nElem() + xfld_local_split_bnd.getCellGroupBase(1).nElem();
      BOOST_CHECK_EQUAL( xfld_local.nFixed() + xfld_local.nResolve() + cavity.size(), nElem );

      // there is always one ghost boundary trace group and less than 3 interior trace groups
      // we might have 3 interior trace groups now because of the newly introduced 0-0 group
      BOOST_CHECK_EQUAL( (xfld_local_split_bnd.nGhostBoundaryTraceGroups()), 1 );
      BOOST_CHECK( (xfld_local_split_bnd.nInteriorTraceGroups()) <= 3 );

      // we must have all the same interior groups plus the {0,0} one
      std::vector< std::pair<int,int> > interiorGroups_split;
      listInteriorGroups(xfld_local_split_bnd,interiorGroups_split);
      for (std::size_t j=0;j<interiorGroups.size();j++)
        BOOST_CHECK_EQUAL( vectorHas(interiorGroups_split,interiorGroups[j]), true );
      BOOST_CHECK_EQUAL( vectorHas(interiorGroups_split,{0,0} ), true );

      // make sure we have the same boundary groups
      BOOST_CHECK_EQUAL( (xfld_local_split_bnd.nBoundaryTraceGroups()),
                         (xfld_local.nBoundaryTraceGroups()) );
      for (int j=0;j<xfld_local_split_bnd.nBoundaryTraceGroups();j++)
        BOOST_CHECK_EQUAL( vectorHas(bgroups, xfld_local_split_bnd.globalBoundaryGroup(j)), true );
    }
  }

  // extract a local patch about a completely interior cell
  {
    std::pair<int,int> global_elem = helper.getFullyInteriorCell(nodalview);
    XField_LocalPatchConstructor<PhysDim,Topology> xfld_local(comm_local,xfld,connectivity,&nodalview);

    // check initial settings
    BOOST_CHECK_EQUAL( xfld_local.nResolve(), 0 );
    BOOST_CHECK_EQUAL( xfld_local.nFixed(), 0 );
    BOOST_CHECK_EQUAL( xfld_local.nDOF(), 0 );
    BOOST_CHECK( xfld_local.spaceType() == SpaceType::Discontinuous ); // default
    BOOST_CHECK_EQUAL( xfld_local.mainCellNodes().size(), 0 ); // not yet defined

    xfld_local.addResolveElement( global_elem.first, global_elem.second );
    BOOST_CHECK_EQUAL( xfld_local.nResolve(), 1 );
    BOOST_CHECK_EQUAL( xfld_local.mainCellNodes().size(), 0 ); // still not yet defined

    TraceInfo info;
    info.type = TraceInfo::Interior;
    BOOST_CHECK_EQUAL( xfld_local.isBoundaryTrace(info,RESOLVE_CELL_GROUP,0), false );
    info.type = TraceInfo::Boundary;
    BOOST_CHECK_EQUAL( xfld_local.isBoundaryTrace(info,RESOLVE_CELL_GROUP,0), true );

    // get all the boundary groups the main cell touches
    std::vector<int> groups;
    helper.listBoundaryGroups( global_elem.first, global_elem.second, groups );
    BOOST_CHECK_EQUAL( groups.size(), 0 ); // fully interior cell -> no boundary groups

    // add the trace neighbours
    xfld_local.addSharedNodeNeighbours();

    // extract the local mesh
    xfld_local.extract();

    // there is always one ghost boundary trace group and less than 2 interior trace groups
    // we have less than two interior trace groups because there should be no 0-0 group
    BOOST_CHECK_EQUAL( (xfld_local.nGhostBoundaryTraceGroups()), 1 );
    BOOST_CHECK( (xfld_local.nInteriorTraceGroups()) <= 2 );

    std::vector< std::pair<int,int> > interiorGroups;
    listInteriorGroups(xfld_local,interiorGroups);

    // make sure we have the same boundary groups
    BOOST_CHECK_EQUAL( (xfld_local.nBoundaryTraceGroups()), groups.size() );
    for (int j=0;j<xfld_local.nBoundaryTraceGroups();j++)
      BOOST_CHECK_EQUAL( vectorHas(groups, xfld_local.globalBoundaryGroup(j)), true );

    // split an interior edge...there is no boundary edge to split
    {
      std::pair<int,int> edge = helper.getInteriorEdge( global_elem.first, global_elem.second );

      XField_LocalPatch<PhysDim,Topology> xfld_local_split_bnd( xfld_local, edge.first, edge.second );

      // check the DOF and element counts
      BOOST_CHECK_EQUAL( xfld_local.nDOF()+1, xfld_local_split_bnd.nDOF() );
      std::set< std::pair<int,int> > cavity;
      int e0 = xfld_local.global2localDOFmap().at(edge.first);
      int e1 = xfld_local.global2localDOFmap().at(edge.second);
      XField_LocalPatch_Utils::allWithEdge<PhysDim,Topology>( xfld_local, NULL, e0, e1, cavity );
      int nElem = xfld_local_split_bnd.getCellGroupBase(0).nElem() + xfld_local_split_bnd.getCellGroupBase(1).nElem();
      BOOST_CHECK_EQUAL( xfld_local.nFixed() + xfld_local.nResolve() + cavity.size(), nElem );

      // there is always one ghost boundary trace group and less than 3 interior trace groups
      // we might have 3 interior trace groups now because of the newly introduced 0-0 group
      BOOST_CHECK_EQUAL( (xfld_local_split_bnd.nGhostBoundaryTraceGroups()), 1 );
      BOOST_CHECK( (xfld_local_split_bnd.nInteriorTraceGroups()) <= 3 );

      // we must have all the same interior groups plus the {0,0} one
      std::vector< std::pair<int,int> > interiorGroups_split;
      listInteriorGroups(xfld_local_split_bnd,interiorGroups_split);
      for (std::size_t j=0;j<interiorGroups.size();j++)
        BOOST_CHECK_EQUAL( vectorHas(interiorGroups_split,interiorGroups[j]), true );
      BOOST_CHECK_EQUAL( vectorHas(interiorGroups_split,{0,0} ), true );

      // make sure we have the same boundary groups
      BOOST_CHECK_EQUAL( (xfld_local_split_bnd.nBoundaryTraceGroups()), groups.size() );
      for (int j=0;j<xfld_local_split_bnd.nBoundaryTraceGroups();j++)
        BOOST_CHECK_EQUAL( vectorHas(groups, xfld_local_split_bnd.globalBoundaryGroup(j)), true );
    }
  }
}
#endif

BOOST_AUTO_TEST_SUITE_END()
//############################################################################//
