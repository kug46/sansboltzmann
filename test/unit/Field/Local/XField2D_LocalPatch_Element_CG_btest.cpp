// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"     // Real
#include "Topology/ElementTopology.h"

#include "Field/XField_CellToTrace.h"
#include "Field/Local/XField_LocalPatch.h"
#include "Field/Field_NodalView.h"

#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_LocalRefine_Triangle_X1.h"

#include "XField_LocalGlobal_Equiv_btest.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( XField2D_LocalPatch_ElementDual_test_suite )

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_2Triangle_InteriorTrace_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField2D_2Triangle_X1_1Group xfld;

  // Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // connecting nodes
  const int group = 0, elem = 0;

  //Extract the local mesh for the bottom-left triangle
  XField_LocalPatchConstructor<PhysD2,Triangle> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Continuous,&nodalview);
  XField_LocalPatch<PhysD2,Triangle> xfld_local(xfld_construct);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 4 );
  BOOST_CHECK_EQUAL( xfld_construct.nDOF(), 4 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld_local.DOF(0)[0],  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(0)[1],  0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(1)[0],  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(1)[1],  0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(2)[0],  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(2)[1],  1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(3)[0],  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(3)[1],  1 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_local.nCellGroups(), 2 );
  for (int i = 0; i < xfld_local.nCellGroups(); i++)
    BOOST_REQUIRE( xfld_local.getCellGroupBase(i).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_local.getCellGroup<Triangle>(0);
  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup1 = xfld_local.getCellGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 1 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup = xfld_local.getInteriorTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldITraceGroup.nElem(), 1 );

  // boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nBoundaryTraceGroups(), 2 );

  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(1).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup1 = xfld_local.getBoundaryTraceGroup<Line>(1);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.nElem(), 2 );

  // ghost boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldGBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 0 );

  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_4Triangle_Interior_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField2D_4Triangle_X1_1Group xfld;

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // connecting nodes
  const int group = 0, elem = 0;

  //Extract the local mesh for the central triangle
  XField_LocalPatchConstructor<PhysD2,Triangle> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Continuous,&nodalview);
  XField_LocalPatch<PhysD2,Triangle> xfld_local(xfld_construct);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 6 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld_local.DOF(0)[0],  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(0)[1],  0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(1)[0],  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(1)[1],  0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(2)[0],  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(2)[1],  1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(3)[0], -1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(3)[1],  1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(4)[0],  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(4)[1], -1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(5)[0],  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(5)[1],  1 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_local.getCellGroup<Triangle>(0);
  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup1 = xfld_local.getCellGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 3 );

  // interior-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup0 = xfld_local.getInteriorTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 3 );

  // boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nBoundaryTraceGroups(), 1 );

  for (int i = 0; i < xfld_local.nBoundaryTraceGroups(); i++)
    BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(i).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 6 );

  // ghost boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldGBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 0 );


  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_Box_Triangle_Interior_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  int ii = 3, jj = 3;
  // make it ([0,3],[0,3]) so the node locations easier
  XField2D_Box_Triangle_Lagrange_X1 xfld( comm_local, ii, jj, 0, 3, 0, 3 );

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // connecting nodes
  const int group = 0, elem = 8;

  //Extract the local mesh for the central triangle
  XField_LocalPatchConstructor<PhysD2,Triangle> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Continuous,&nodalview);
  XField_LocalPatch<PhysD2,Triangle> xfld_local(xfld_construct);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 12 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld_local.DOF(0)[0],  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(0)[1],  1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(1)[0],  2 );  BOOST_CHECK_EQUAL( xfld_local.DOF(1)[1],  1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(2)[0],  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(2)[1],  2 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(3)[0],  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(3)[1],  1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(4)[0],  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(4)[1],  0 );

  BOOST_CHECK_EQUAL( xfld_local.DOF(5)[0],  2 );  BOOST_CHECK_EQUAL( xfld_local.DOF(5)[1],  0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(6)[0],  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(6)[1],  2 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(7)[0],  3 );  BOOST_CHECK_EQUAL( xfld_local.DOF(7)[1],  0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(8)[0],  3 );  BOOST_CHECK_EQUAL( xfld_local.DOF(8)[1],  1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(9)[0],  2 );  BOOST_CHECK_EQUAL( xfld_local.DOF(9)[1],  2 );

  BOOST_CHECK_EQUAL( xfld_local.DOF(10)[0], 0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(10)[1], 3 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(11)[0], 1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(11)[1], 3 );

  // area field variable

  BOOST_REQUIRE_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_local.getCellGroup<Triangle>(0);
  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup1 = xfld_local.getCellGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 12 );

  // interior-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nInteriorTraceGroups(), 2 );
  for (int i = 0; i < 2; i++)
    BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Line>(i).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup0 = xfld_local.getInteriorTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup1 = xfld_local.getInteriorTraceGroup<Line>(1);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 12 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 3 );

  // boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nBoundaryTraceGroups(), 4 );

  for (int i = 0; i < xfld_local.nBoundaryTraceGroups(); i++)
    BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(i).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup1 = xfld_local.getBoundaryTraceGroup<Line>(1);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup2 = xfld_local.getBoundaryTraceGroup<Line>(2);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup3 = xfld_local.getBoundaryTraceGroup<Line>(3);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.nElem(), 1 );

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );

  for (int i = 0; i < 1; i++)
    BOOST_REQUIRE( xfld_local.getGhostBoundaryTraceGroup<Line>(i).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldGBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 3 );


  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_WholeGrid_Box_Triangle_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  int ii = 3, jj = 3;
  // make it ([0,3],[0,3]) so the node locations easier
  XField2D_Box_Triangle_Lagrange_X1 xfld( comm_local, ii, jj, 0, 3, 0, 3 );

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  // Build not to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  for (int group = 0; group < xfld.nCellGroups(); group++)
  {
    for ( int elem = 0; elem < xfld.getCellGroup<Triangle>(group).nElem(); elem++ )
    {
      // Construct unsplit grid
      XField_LocalPatchConstructor<PhysD2,Triangle> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Continuous,&nodalview);
      XField_LocalPatch<PhysD2,Triangle> xfld_local(xfld_construct);

      // check that all local elements match the global elements
      XField_LocalGlobal_Equiv(xfld_local);
    }
  }
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_2Triangle_Boundary_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  Real small_tol = 1e-12, close_tol = 1e-12;

  XField2D_2Triangle_X1_1Group xfld;

  // Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  // Build not to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  const int group = 0, elem = 0, edge = 2;

  // Extract the unsplit grid
  XField_LocalPatchConstructor<PhysD2,Triangle> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Continuous,&nodalview);

  // Split the grid using alternate constructor
  XField_LocalPatch<PhysD2,Triangle> xfld_local(xfld_construct, ElementSplitType::Edge, edge);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 5 );

  //Check the node values
  SANS_CHECK_CLOSE(  0., xfld_local.DOF(0)[0],  small_tol, close_tol );  SANS_CHECK_CLOSE( 0., xfld_local.DOF(0)[1],  small_tol, close_tol );
  SANS_CHECK_CLOSE(  1., xfld_local.DOF(1)[0],  small_tol, close_tol );  SANS_CHECK_CLOSE( 0., xfld_local.DOF(1)[1],  small_tol, close_tol );
  SANS_CHECK_CLOSE(  0., xfld_local.DOF(2)[0],  small_tol, close_tol );  SANS_CHECK_CLOSE( 1., xfld_local.DOF(2)[1],  small_tol, close_tol );
  SANS_CHECK_CLOSE(  1., xfld_local.DOF(3)[0],  small_tol, close_tol );  SANS_CHECK_CLOSE( 1., xfld_local.DOF(3)[1],  small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.5, xfld_local.DOF(4)[0],  small_tol, close_tol );  SANS_CHECK_CLOSE( 0., xfld_local.DOF(4)[1],  small_tol, close_tol );

  // area field variable

  BOOST_REQUIRE_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_local.getCellGroup<Triangle>(0);
  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup1 = xfld_local.getCellGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 1 );

  // interior-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nInteriorTraceGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Line>(1).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup0 = xfld_local.getInteriorTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup1 = xfld_local.getInteriorTraceGroup<Line>(1);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 1 );

  // boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nBoundaryTraceGroups(), 2 );

  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(1).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup1 = xfld_local.getBoundaryTraceGroup<Line>(1);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 3 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.nElem(), 2 );

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );

  for (int i = 0; i < 1; i++)
    BOOST_REQUIRE( xfld_local.getGhostBoundaryTraceGroup<Line>(i).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldGBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 0 );


  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_4Triangle_Interior_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  Real small_tol = 1e-12, close_tol = 1e-12;

  XField2D_4Triangle_X1_1Group xfld;

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // connecting nodes
  const int group = 0, elem = 0, edge = 2;

  // Extract the unsplit grid
  XField_LocalPatchConstructor<PhysD2,Triangle> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Continuous,&nodalview);

  // Split the grid using alternate constructor
  XField_LocalPatch<PhysD2,Triangle> xfld_local(xfld_construct, ElementSplitType::Edge, edge);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 7 );

  //Check the node values
  SANS_CHECK_CLOSE(  0., xfld_local.DOF(0)[0], small_tol, close_tol );  SANS_CHECK_CLOSE(  0., xfld_local.DOF(0)[1], small_tol, close_tol );
  SANS_CHECK_CLOSE(  1., xfld_local.DOF(1)[0], small_tol, close_tol );  SANS_CHECK_CLOSE(  0., xfld_local.DOF(1)[1], small_tol, close_tol );
  SANS_CHECK_CLOSE(  0., xfld_local.DOF(2)[0], small_tol, close_tol );  SANS_CHECK_CLOSE(  1., xfld_local.DOF(2)[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( -1., xfld_local.DOF(3)[0], small_tol, close_tol );  SANS_CHECK_CLOSE(  1., xfld_local.DOF(3)[1], small_tol, close_tol );
  SANS_CHECK_CLOSE(  1., xfld_local.DOF(4)[0], small_tol, close_tol );  SANS_CHECK_CLOSE( -1., xfld_local.DOF(4)[1], small_tol, close_tol );
  SANS_CHECK_CLOSE(  1., xfld_local.DOF(5)[0], small_tol, close_tol );  SANS_CHECK_CLOSE(  1., xfld_local.DOF(5)[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( 0.5, xfld_local.DOF(6)[0], small_tol, close_tol );  SANS_CHECK_CLOSE(  0., xfld_local.DOF(6)[1], small_tol, close_tol );

  // area field variable

  BOOST_REQUIRE_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_local.getCellGroup<Triangle>(0);
  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup1 = xfld_local.getCellGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 4 );

  // interior-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nInteriorTraceGroups(), 3 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup0 = xfld_local.getInteriorTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup1 = xfld_local.getInteriorTraceGroup<Line>(1);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup2 = xfld_local.getInteriorTraceGroup<Line>(2);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 4 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.nElem(), 1 );

  // boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nBoundaryTraceGroups(), 1 );

  for (int i = 0; i < xfld_local.nBoundaryTraceGroups(); i++)
    BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(i).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 6 );

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );

  for (int i = 0; i < 1; i++)
    BOOST_REQUIRE( xfld_local.getGhostBoundaryTraceGroup<Line>(i).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldGBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 0 );


  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Box_Triangle_Interior_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  int ii = 3, jj = 3;
  // make it ([0,3],[0,3]) so the node locations easier
  XField2D_Box_Triangle_Lagrange_X1 xfld( comm_local, ii, jj, 0, 3, 0, 3 );

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  const int group = 0, elem = 8, edge = 0;

  // Extract the unsplit grid
  XField_LocalPatchConstructor<PhysD2,Triangle> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Continuous,&nodalview);

  // Split the grid using alternate constructor
  XField_LocalPatch<PhysD2,Triangle> xfld_local(xfld_construct, ElementSplitType::Edge, edge);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_REQUIRE_EQUAL( xfld_local.nDOF(), 13 );

  //Check the node values
  Real small_tol = 1e-13, close_tol = 1e-13;
  SANS_CHECK_CLOSE(  1., xfld_local.DOF(0)[0],  small_tol, close_tol );  SANS_CHECK_CLOSE(  1., xfld_local.DOF(0)[1],  small_tol, close_tol );
  SANS_CHECK_CLOSE(  2., xfld_local.DOF(1)[0],  small_tol, close_tol );  SANS_CHECK_CLOSE(  1., xfld_local.DOF(1)[1],  small_tol, close_tol );
  SANS_CHECK_CLOSE(  1., xfld_local.DOF(2)[0],  small_tol, close_tol );  SANS_CHECK_CLOSE(  2., xfld_local.DOF(2)[1],  small_tol, close_tol );
  SANS_CHECK_CLOSE(  0., xfld_local.DOF(3)[0],  small_tol, close_tol );  SANS_CHECK_CLOSE(  1., xfld_local.DOF(3)[1],  small_tol, close_tol );
  SANS_CHECK_CLOSE(  1., xfld_local.DOF(4)[0],  small_tol, close_tol );  SANS_CHECK_CLOSE(  0., xfld_local.DOF(4)[1],  small_tol, close_tol );

  SANS_CHECK_CLOSE(  2., xfld_local.DOF(5)[0],  small_tol, close_tol );  SANS_CHECK_CLOSE(  0., xfld_local.DOF(5)[1],  small_tol, close_tol );
  SANS_CHECK_CLOSE(  0., xfld_local.DOF(6)[0],  small_tol, close_tol );  SANS_CHECK_CLOSE(  2., xfld_local.DOF(6)[1],  small_tol, close_tol );
  SANS_CHECK_CLOSE(  3., xfld_local.DOF(7)[0],  small_tol, close_tol );  SANS_CHECK_CLOSE(  0., xfld_local.DOF(7)[1],  small_tol, close_tol );
  SANS_CHECK_CLOSE(  3., xfld_local.DOF(8)[0],  small_tol, close_tol );  SANS_CHECK_CLOSE(  1., xfld_local.DOF(8)[1],  small_tol, close_tol );
  SANS_CHECK_CLOSE(  2., xfld_local.DOF(9)[0],  small_tol, close_tol );  SANS_CHECK_CLOSE(  2., xfld_local.DOF(9)[1],  small_tol, close_tol );

  SANS_CHECK_CLOSE(  0., xfld_local.DOF(10)[0], small_tol, close_tol );  SANS_CHECK_CLOSE(  3., xfld_local.DOF(10)[1], small_tol, close_tol );
  SANS_CHECK_CLOSE(  1., xfld_local.DOF(11)[0], small_tol, close_tol );  SANS_CHECK_CLOSE(  3., xfld_local.DOF(11)[1], small_tol, close_tol );
  SANS_CHECK_CLOSE( 1.5, xfld_local.DOF(12)[0], small_tol, close_tol );  SANS_CHECK_CLOSE( 1.5, xfld_local.DOF(12)[1], small_tol, close_tol );

  // area field variable

  BOOST_REQUIRE_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_local.getCellGroup<Triangle>(0);
  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup1 = xfld_local.getCellGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 13 );

  // interior-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nInteriorTraceGroups(), 3 );
  for (int i = 0; i < xfld_local.nInteriorTraceGroups(); i++)
    BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Line>(i).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup0 = xfld_local.getInteriorTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup1 = xfld_local.getInteriorTraceGroup<Line>(1);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup2 = xfld_local.getInteriorTraceGroup<Line>(2);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1  );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 13 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.nElem(), 4  );

  // boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nBoundaryTraceGroups(), 4 );

  for (int i = 0; i < xfld_local.nBoundaryTraceGroups(); i++)
    BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(i).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup1 = xfld_local.getBoundaryTraceGroup<Line>(1);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup2 = xfld_local.getBoundaryTraceGroup<Line>(2);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup3 = xfld_local.getBoundaryTraceGroup<Line>(3);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.nElem(), 1 );

  // ghost boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );

  for (int i = 0; i < xfld_local.nGhostBoundaryTraceGroups(); i++)
    BOOST_REQUIRE( xfld_local.getGhostBoundaryTraceGroup<Line>(i).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldGBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 3 );


  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_WholeGrid_Box_Triangle_test )
{
  typedef XField<PhysD2, TopoD2>::FieldCellGroupType<Triangle> XFieldCellGroupType;

  mpi::communicator world;
  const int comm_rank = world.rank();

  mpi::communicator comm_local = world.split(comm_rank);

  int size = 3*3*world.size();
  int ii = sqrt(size), jj = sqrt(size);
  // make it ([0,3],[0,3]) so the node locations easier
  XField2D_Box_Triangle_Lagrange_X1 xfld( world, ii, jj, 0, 3, 0, 3 );

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // Loop over all elements in grid and construct local grids
  for (int group = 0; group < xfld.nCellGroups(); group++)
  {
    const XFieldCellGroupType& xfldCellGroup = xfld.getCellGroup<Triangle>(group);

    for ( int elem = 0; elem < xfldCellGroup.nElem(); elem++ )
    {
      // only consider possessed elements
      if (xfldCellGroup.associativity(elem).rank() != comm_rank) continue;

      // The unsplit grid
      XField_LocalPatchConstructor<PhysD2,Triangle> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Continuous,&nodalview);

      for (int edge = 0; edge < Triangle::NEdge; edge++)
      {
        // Split the grid using alternate constructor
        XField_LocalPatch<PhysD2,Triangle> xfld_local(xfld_construct, ElementSplitType::Edge, edge);

        // check that all local elements match the global elements
        XField_LocalGlobal_Equiv(xfld_local);
      }
    }
  }
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
