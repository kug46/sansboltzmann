// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"     // Real
#include "Topology/ElementTopology.h"

#include "Field/XField_CellToTrace.h"
#include "Field/Local/XField_LocalPatch.h"
#include "Field/Field_NodalView.h"

#include "Field/FieldVolume_CG_Cell.h"

#include "unit/UnitGrids/XField3D_1Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_2Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_5Tet_X1_1Group_AllOrientations.h"
#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"

#include "XField_LocalGlobal_Equiv_btest.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( XField3D_LocalPatch_Edge_test_suite )

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_1Tet_InteriorTrace_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField3D_1Tet_X1_1Group xfld;

  // Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // connecting nodes
  std::array<int,2> edge{{0,3}};

  //Extract the local mesh for the bottom-left triangle
  XField_LocalPatchConstructor<PhysD3,Tet> xfld_construct(comm_local,connectivity,edge,SpaceType::Continuous,&nodalview);
  XField_LocalPatch<PhysD3,Tet> xfld_local(xfld_construct);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 4 );

  int vertices[4][3] = {{ 0, 0, 0},
                        { 0, 0, 1}, // node 3 from the global
                        { 1, 0, 0},
                        { 0, 1, 0}};

  //Check the node values
  for (int k = 0; k < xfld_local.nDOF(); k++)
    for (int d = 0; d < PhysD3::D; d++)
      BOOST_CHECK_EQUAL( xfld_local.DOF(k)[d], vertices[k][d] );

  // volume field variable

  BOOST_CHECK_EQUAL( xfld_local.nCellGroups(), 2 );
  for (int i = 0; i < xfld_local.nCellGroups(); i++)
    BOOST_REQUIRE( xfld_local.getCellGroupBase(i).topoTypeID() == typeid(Tet) );

  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup0 = xfld_local.getCellGroup<Tet>(0);
  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup1 = xfld_local.getCellGroup<Tet>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 0 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nInteriorTraceGroups(), 0 );

  // boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nBoundaryTraceGroups(), 4 );

  for (int i = 0; i < xfld_local.nBoundaryTraceGroups(); i++)
    BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Triangle>(i).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup0  = xfld_local.getBoundaryTraceGroup<Triangle>(0);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup1  = xfld_local.getBoundaryTraceGroup<Triangle>(1);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup2  = xfld_local.getBoundaryTraceGroup<Triangle>(2);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup3  = xfld_local.getBoundaryTraceGroup<Triangle>(3);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.nElem(), 1 );

  // ghost-boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );

  for (int i = 0; i < xfld_local.nGhostBoundaryTraceGroups(); i++)
    BOOST_REQUIRE( xfld_local.getGhostBoundaryTraceGroup<Triangle>(i).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldGBTraceGroup0  = xfld_local.getGhostBoundaryTraceGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 0 );


  //Checking the commonNodes_ interfaces
  std::vector<int> commonNodes = xfld_local.getLinearCommonNodes();
  std::vector<int> localCommonNodes = xfld_local.getLocalLinearCommonNodes();

  BOOST_CHECK_EQUAL( commonNodes.size(), 4 );
  BOOST_CHECK_EQUAL( localCommonNodes.size(), 4 );

  // Check that the first two nodes are the edge
  BOOST_CHECK_EQUAL( commonNodes[0], edge[0] );
  BOOST_CHECK_EQUAL( commonNodes[1], edge[1] );

  // Check that the edge is nodes 0 and 1
  BOOST_CHECK_EQUAL( localCommonNodes[0], 0 );
  BOOST_CHECK_EQUAL( localCommonNodes[1], 1 );

  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_5Tet_InteriorTrace_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField3D_5Tet_X1_1Group_AllOrientations xfld(-1);

  // Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // connecting nodes
  std::array<int,2> edge{{0,3}};

  //Extract the local mesh for the bottom-left triangle
  XField_LocalPatchConstructor<PhysD3,Tet> xfld_construct(comm_local,connectivity,edge,SpaceType::Continuous,&nodalview);
  XField_LocalPatch<PhysD3,Tet> xfld_local(xfld_construct);


  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 8 );

  int vertices[8][3] = {{ 0, 0, 0},
                        { 0, 0, 1},
                        { 1, 0, 0},
                        { 0, 1, 0},
                        {-1, 0, 0},
                        { 0,-1, 0},
                        { 0, 0,-1},
                        { 1, 1, 1}};

  //Check the node values
  for (int k = 0; k < xfld_local.nDOF(); k++)
    for (int d = 0; d < PhysD3::D; d++)
      BOOST_CHECK_EQUAL( xfld_local.DOF(k)[d], vertices[k][d] );

  // volume field variable

  BOOST_REQUIRE_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Tet) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Tet) );

  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup0 = xfld_local.getCellGroup<Tet>(0);
  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup1 = xfld_local.getCellGroup<Tet>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 3 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 2 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nInteriorTraceGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Triangle>(1).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup0 = xfld_local.getInteriorTraceGroup<Triangle>(0);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup1 = xfld_local.getInteriorTraceGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 2 );

  // boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nBoundaryTraceGroups(), 2 );

  for (int i = 0; i < xfld_local.nBoundaryTraceGroups(); i++)
    BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Triangle>(i).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup0  = xfld_local.getBoundaryTraceGroup<Triangle>(0);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup1  = xfld_local.getBoundaryTraceGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 6 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.nElem(), 6 );

  // ghost-boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );

  for (int i = 0; i < xfld_local.nGhostBoundaryTraceGroups(); i++)
    BOOST_REQUIRE( xfld_local.getGhostBoundaryTraceGroup<Triangle>(i).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldGBTraceGroup0  = xfld_local.getGhostBoundaryTraceGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 0 );


  //Checking the commonNodes_ interfaces
  std::vector<int> commonNodes = xfld_local.getLinearCommonNodes();
  std::vector<int> localCommonNodes = xfld_local.getLocalLinearCommonNodes();

  BOOST_CHECK_EQUAL( commonNodes.size(), 6 );
  BOOST_CHECK_EQUAL( localCommonNodes.size(), 6 );

  // Check that the first two nodes are the edge
  BOOST_CHECK_EQUAL( commonNodes[0], edge[0] );
  BOOST_CHECK_EQUAL( commonNodes[1], edge[1] );

  // Check that the edge is nodes 0 and 1
  BOOST_CHECK_EQUAL( localCommonNodes[0], 0 );
  BOOST_CHECK_EQUAL( localCommonNodes[1], 1 );

  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_1Tet_InteriorTrace_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField3D_1Tet_X1_1Group xfld;

  // Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // connecting nodes
  std::array<int,2> edge{{0,3}};

  // construct a split local mesh
  XField_LocalPatch<PhysD3,Tet> xfld_local(comm_local,connectivity,edge,SpaceType::Continuous,&nodalview);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 5 );

  Real vertices[5][3] = {{ 0, 0,   0},
                         { 0, 0,   1}, // node 3 from the global
                         { 1, 0,   0},
                         { 0, 1,   0},
                         { 0, 0, 0.5}}; // halfway to node 3


  const Real small_tol = 1e-10, close_tol = 1e-10;
  //Check the node values
  for (int k = 0; k < xfld_local.nDOF(); k++)
    for (int d = 0; d < 3; d++)
      SANS_CHECK_CLOSE( xfld_local.DOF(k)[d], vertices[k][d], small_tol, close_tol );

  // volume field variable

  BOOST_REQUIRE_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Tet) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Tet) );

  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup0 = xfld_local.getCellGroup<Tet>(0);
  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup1 = xfld_local.getCellGroup<Tet>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 0 );

  // interior-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroupBase(0).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup0  = xfld_local.getInteriorTraceGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1 );

  // boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nBoundaryTraceGroups(), 4 );

  for (int i = 0; i < xfld_local.nBoundaryTraceGroups(); i++)
    BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Triangle>(i).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup0  = xfld_local.getBoundaryTraceGroup<Triangle>(0);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup1  = xfld_local.getBoundaryTraceGroup<Triangle>(1);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup2  = xfld_local.getBoundaryTraceGroup<Triangle>(2);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup3  = xfld_local.getBoundaryTraceGroup<Triangle>(3);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.nElem(), 1 );

  // ghost-boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );

  for (int i = 0; i < xfld_local.nGhostBoundaryTraceGroups(); i++)
    BOOST_REQUIRE( xfld_local.getGhostBoundaryTraceGroup<Triangle>(i).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldGBTraceGroup0  = xfld_local.getGhostBoundaryTraceGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 0 );


  //Checking the commonNodes_ interfaces
  std::vector<int> commonNodes = xfld_local.getLinearCommonNodes();
  std::vector<int> localCommonNodes = xfld_local.getLocalLinearCommonNodes();

  BOOST_CHECK_EQUAL( commonNodes.size(), 4 );
  BOOST_CHECK_EQUAL( localCommonNodes.size(), 4 );

  // Check that the first two nodes are the edge
  BOOST_CHECK_EQUAL( commonNodes[0], edge[0] );
  BOOST_CHECK_EQUAL( commonNodes[1], edge[1] );

  // Check that the edge is nodes 0 and 1
  BOOST_CHECK_EQUAL( localCommonNodes[0], 0 );
  BOOST_CHECK_EQUAL( localCommonNodes[1], 1 );

  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_Local_5Tet_X1_1Group_InteriorTrace_Target_Dual_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField3D_5Tet_X1_1Group_AllOrientations xfld(-1);

  // Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // connecting nodes
  std::array<int,2> edge{{0,3}};

  //Extract the local mesh for the bottom-left triangle
  XField_LocalPatch<PhysD3,Tet> xfld_local(comm_local,connectivity,edge,SpaceType::Continuous,&nodalview);

  BOOST_CHECK_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 9 );

  Real vertices[9][3] = {{ 0, 0,   0},
                         { 0, 0,   1},
                         { 1, 0,   0},
                         { 0, 1,   0},
                         {-1, 0,   0},
                         { 0,-1,   0},
                         { 0, 0,  -1},
                         { 1, 1,   1},
                         { 0, 0, 0.5}};

  const Real small_tol = 1e-10, close_tol = 1e-10;

  //Check the node values
  for (int k = 0; k < xfld_local.nDOF(); k++)
    for (int d = 0; d < PhysD3::D; d++)
    {
      // std::cout<< "k = " << k << ", d = " << d << std::endl;
      SANS_CHECK_CLOSE( vertices[k][d], xfld_local.DOF(k)[d], small_tol, close_tol );
    }
  // volume field variable

  BOOST_CHECK_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Tet) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Tet) );

  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup0 = xfld_local.getCellGroup<Tet>(0);
  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup1 = xfld_local.getCellGroup<Tet>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 6 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 2 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nInteriorTraceGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Triangle>(1).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup0 = xfld_local.getInteriorTraceGroup<Triangle>(0);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup1 = xfld_local.getInteriorTraceGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 2*2 + 3 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 2 );

  // boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nBoundaryTraceGroups(), 2 );

  for (int i = 0; i < xfld_local.nBoundaryTraceGroups(); i++)
    BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Triangle>(i).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup0  = xfld_local.getBoundaryTraceGroup<Triangle>(0);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup1  = xfld_local.getBoundaryTraceGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 8 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.nElem(), 6 );

  // ghost-boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );

  for (int i = 0; i < xfld_local.nGhostBoundaryTraceGroups(); i++)
    BOOST_REQUIRE( xfld_local.getGhostBoundaryTraceGroup<Triangle>(i).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldGBTraceGroup0  = xfld_local.getGhostBoundaryTraceGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 0 );


  //Checking the commonNodes_ interfaces
  std::vector<int> commonNodes = xfld_local.getLinearCommonNodes();
  std::vector<int> localCommonNodes = xfld_local.getLocalLinearCommonNodes();

  BOOST_CHECK_EQUAL( commonNodes.size(), 6 );
  BOOST_CHECK_EQUAL( localCommonNodes.size(), 6 );

  // Check that the first two nodes are the edge
  BOOST_CHECK_EQUAL( commonNodes[0], edge[0] );
  BOOST_CHECK_EQUAL( commonNodes[1], edge[1] );

  // Check that the edge is nodes 0 and 1
  BOOST_CHECK_EQUAL( localCommonNodes[0], 0 );
  BOOST_CHECK_EQUAL( localCommonNodes[1], 1 );

  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( WholeGrid_Box_Tet_test )
{
  typedef XField<PhysD3, TopoD3>::FieldCellGroupType<Tet> XFieldCellGroupType;

  typedef Field_CG_Cell< PhysD3, TopoD3, Real > QFieldType;
  typedef typename QFieldType::template FieldCellGroupType<Tet> QFieldCellGroupType;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  int size = 3*3*3*world.size();
  int ii = pow(size, 1./3.), jj = ii, kk = ii;
  // make it ([0,3],[0,3]) so the node locations easier
  XField3D_Box_Tet_X1 xfld( world, ii, jj, kk, 0, 3, 0, 3, 0, 3 );

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // Field needed to get nodal rank
  QFieldType qfld(xfld, 1, BasisFunctionCategory_Lagrange);

  // Construct list of all edges in grid

  const int nEdge = Tet::NEdge; // Number of different edges

  typedef typename std::set<std::array<int,Line::NNode>> EdgeList;
  EdgeList edge_list;

  const int (*EdgeNodes)[Line::NNode] = ElementEdges<Tet>::EdgeNodes;

  for (int group = 0; group < xfld.nCellGroups(); group++)
  {
    const XFieldCellGroupType& xfldCellGroup = xfld.template getCellGroup<Tet>(group);
    const QFieldCellGroupType& qfldCellGroup = qfld.template getCellGroup<Tet>(group);

    for ( int elem = 0; elem < xfldCellGroup.nElem(); elem++ )
    {
      std::array<int,Tet::NNode> xnodeMap;
      std::array<int,Tet::NNode> qnodeMap;

      xfldCellGroup.associativity(elem).getNodeGlobalMapping( xnodeMap.data(), xnodeMap.size() );
      qfldCellGroup.associativity(elem).getNodeGlobalMapping( qnodeMap.data(), qnodeMap.size() );

      for ( int edge = 0; edge < nEdge; edge++ )
      {
        // loop over the edges
        std::array<int,Line::NNode> xEdge;
        xEdge[0] = xnodeMap[ EdgeNodes[edge][0] ];
        xEdge[1] = xnodeMap[ EdgeNodes[edge][1] ];

        std::array<int,Line::NNode> qEdge;
        qEdge[0] = qnodeMap[ EdgeNodes[edge][0] ];
        qEdge[1] = qnodeMap[ EdgeNodes[edge][1] ];

        if (xfld.local2nativeDOFmap(xEdge[0]) > xfld.local2nativeDOFmap(xEdge[1]))
        {
          std::swap(xEdge[0], xEdge[1]);
          std::swap(qEdge[0], qEdge[1]);
        }

        // only consider edges where the primary node is possessed
        if (qEdge[0] >= qfld.nDOFpossessed()) continue;

        edge_list.insert(xEdge);
      }
    }

    for ( const std::array<int,Line::NNode>& edge : edge_list )
    {
      // Construct split grid
      XField_LocalPatch<PhysD3,Tet> xfld_local(comm_local,connectivity,edge,SpaceType::Continuous,&nodalview);

      // check that all local elements match the global elements
      XField_LocalGlobal_Equiv(xfld_local);
    }
  }
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
