// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// XField4D_Local_btest
// testing of XField4D_Local
//
// Note: unit grids tested in "unit/UnitGrids/UnitGrids_btest.cpp"

#include <ostream>
#include <utility> // std::pair

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "Topology/ElementTopology.h"

#include "BasisFunction/ElementEdges.h"

#include "Field/Field_NodalView.h"
#include "Field/XField_CellToTrace.h"
#include "Field/Local/XField_LocalPatch.h"

#include "unit/UnitGrids/XField_KuhnFreudenthal.h"
#include "unit/UnitGrids/XField4D_6Ptope_X1_1Group_NegativeTraceOrientation.h"
#include "unit/UnitGrids/XField4D_1Ptope_X1_1Group.h"

#include "XField_LocalGlobal_Equiv_btest.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( XField4D_Local_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField4D_LocalPatch_1Pentatope_1Group_InteriorCell_Target_test )
{

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField4D_1Ptope_X1_1Group xfld;

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD4,TopoD4> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  int group = 0, elem = 0;

  //Extract the local mesh for the central tet
  XField_LocalPatchConstructor<PhysD4,Pentatope> xfld_local(comm_local,xfld,connectivity);

  xfld_local.addResolveElement( group, elem );
  xfld_local.extract();

  // create a split patch and split edge (0,1)
  XField_LocalPatch<PhysD4,Pentatope> xfld_split( xfld_local, 0, 1 );

  // check that all elements match the global elements
  XField_LocalGlobal_Equiv(xfld_split);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField4D_LocalPatch_6Pentatope_1Group_InteriorCell_Target_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField4D_6Ptope_X1_1Group_NegativeTraceOrientation xfld(comm_local);
  //xfld.dump(1);

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD4,TopoD4> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  int group = 0, elem = 0;

  //Extract the local mesh for the central tet
  XField_LocalPatchConstructor<PhysD4,Pentatope> xfld_local(comm_local,xfld,connectivity);

  xfld_local.addResolveElement( group, elem );
  xfld_local.addFixedElement( 0, 1 );
  xfld_local.addFixedElement( 0, 2 );
  xfld_local.addFixedElement( 0, 3 );
  xfld_local.addFixedElement( 0, 4 );
  xfld_local.addFixedElement( 0, 5 );

  xfld_local.extract();

  // create a split patch and split edge (0,1)
  XField_LocalPatch<PhysD4,Pentatope> xfld_split( xfld_local, 0, 1 );

  // check that all elements match the global elements
  XField_LocalGlobal_Equiv(xfld_split);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField4D_LocalPatchAddNeighbours_6Pentatope_1Group_InteriorCell_Target_test )
{
  #ifdef SANS_AVRO

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  //XField4D_6Ptope_X1_1Group_NegativeTraceOrientation xfld(comm_local);
  //xfld.dump(1);
  XField_KuhnFreudenthal<PhysD4,TopoD4> xfld( comm_local, {3,2,3,2} );

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD4,TopoD4> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  time_t t0 = clock();
  int nsplit = 0;
  for (int group = 0; group < xfld.nCellGroups(); group++)
  {
    for (int elem = 0; elem < xfld.getCellGroupBase(group).nElem(); elem++)
    {

      //printf("=== processing elem %d ===\n",elem);

      // extract the local mesh for the central tet
      XField_LocalPatchConstructor<PhysD4,Pentatope> xfld_local(comm_local,xfld,connectivity,&nodalview);

      xfld_local.addResolveElement( group, elem );
      //xfld_local.addSharedTraceNeighbours();
      xfld_local.addSharedNodeNeighbours();

      xfld_local.extract();

      // get the cell dof
      std::vector<int> cellDOF( Pentatope::NNode, -1 );
      xfld.getCellGroup<Pentatope>(group).associativity(elem).getNodeGlobalMapping( cellDOF.data(), cellDOF.size() );

      // split every edge
      const int (*edges)[Line::NNode] = ElementEdges<Pentatope>::EdgeNodes;
      for (int edge = 0; edge < Pentatope::NEdge; edge++)
      {

        int e0 = cellDOF[ edges[edge][0] ];
        int e1 = cellDOF[ edges[edge][1] ];

        XField_LocalPatch<PhysD4,Pentatope> xfld_split( xfld_local, e0, e1 );

        nsplit++;

        // check that all elements match the global elements
        XField_LocalGlobal_Equiv(xfld_split);
      }
    }
  }

  printf("performed nsplit = %d in %g seconds\n",nsplit,double(clock()-t0)/CLOCKS_PER_SEC);
  #endif

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
