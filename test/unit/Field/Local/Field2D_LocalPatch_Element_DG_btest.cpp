// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Field2D_Local_Split_DG_btest
//

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunctionArea_Triangle.h"

#include "Field/Local/XField_LocalPatch.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_InteriorTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"
#include "Field/Local/Field_Local.h"

// #include "Field_LocalPatch_Element_DG_btest.h"
#include "Field_LocalPatch_toolkit_btest.h"
#include "XField_LocalGlobal_Equiv_btest.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

//Explicitly instantiate classes to get proper coverage information
namespace SANS
{
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( Field2D_Local_Split_DG_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field2D_DG_Local_4Triangle_X1_ExtractCells_SplitTraces_P1 )
{
  // this test will run through all of the possible splits on all edges of all
  // elements of all groups and make sure, on each of the above, the solution
  // transfers cleanly between the given split and the original grids. no
  // metadata is tested, though, so this isn't a conclusive test.

  Real tol = 1e-11;

  XField2D_4Triangle_X1_1Group xfld;

  int order = 1;

  localElementExtractAndTransfer(xfld, BasisFunctionCategory_Legendre, order, tol, tol, true);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field2D_DG_Local_4Triangle_X1_ExtractCells_SplitTraces_P2 )
{
  // this test will run through all of the possible splits on all edges of all
  // elements of all groups and make sure, on each of the above, the solution
  // transfers cleanly between the given split and the original grids. no
  // metadata is tested, though, so this isn't a conclusive test.

  Real tol = 5e-10;

  XField2D_4Triangle_X1_1Group xfld;

  int order = 2;

  localElementExtractAndTransfer(xfld, BasisFunctionCategory_Legendre, order, tol, tol, true);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field2D_DG_Local_4Triangle_X1_ExtractCells_SplitTraces_P3 )
{
  // this test will run through all of the possible splits on all edges of all
  // elements of all groups and make sure, on each of the above, the solution
  // transfers cleanly between the given split and the original grids. no
  // metadata is tested, though, so this isn't a conclusive test.

  Real tol = 5e-10;

  XField2D_4Triangle_X1_1Group xfld;

  int order = 3;

  localElementExtractAndTransfer(xfld, BasisFunctionCategory_Legendre, order, tol, tol, true);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE(Field2D_DG_Local_4Triangle_X1_ExtractCells_SplitTraces_metadata_P1)
{
  // this test will run through all of the possible splits on all edges of all
  // elements of all groups and make sure, on each of the above, the metadata is
  // correct in as far as is knowable. use in conjunction with the solution
  // transfer tests above.

  typedef typename Simplex<TopoD2>::type Topology;

  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_DG_Area;
  // typedef Field_DG_BoundaryTrace< PhysD2, TopoD2, ArrayQ > QField2D_DG_Line;

  // typedef DLA::VectorS<TopoD2::D,Real> RefCoordType;

  // typedef QField2D_DG_Area::FieldCellGroupType<Triangle> FieldCellGroupType;
  // typedef FieldCellGroupType::ElementType<> ElementFieldClass;

  typedef typename XField<PhysD2, TopoD2>::template FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

  const int nEdge= Topology::NEdge; // Number of different edges

  mpi::communicator world;
  mpi::communicator comm_local= world.split(world.rank());

  // Real tol= 1e-11;
  bool isSplit= true;

  XField2D_4Triangle_X1_1Group xfld;

  int order= 1;
  QField2D_DG_Area qfld(xfld, order, BasisFunctionCategory_Legendre);
  // QField2D_DG_Line lgfld(xfld, order, BasisFunctionCategory_Legendre);

  // set some non-zero solution
  for (int n= 0; n < qfld.nDOF(); n++)
    qfld.DOF(n)= {10*pow(-1, n%2) + n, 10*pow(-1, n%2) + n};

  // // set some non-zero lagrange multiplier/boundary solution
  // for (int n= 0; n < lgfld.nDOF(); n++)
  //   lgfld.DOF(n)= {1*(-n%3) - 1, (n%3) + 1};

  // loop over cell groups in the mesh
  for (int group= 0; group < xfld.nCellGroups(); group++)
  {
    // grab the group and its metadata
    XFieldCellGroupType xfldCellGroup= xfld.getCellGroup<Topology>(group);
    const int nElem= xfldCellGroup.nElem();

    // prep to grab individual elements/element data
    ElementXFieldClass xfldElem(xfldCellGroup.basis());

    const XField_CellToTrace<PhysD2, TopoD2> xfld_connectivity(xfld);

    // loop over all the elements in the cell group
    for (int elem= 0; elem < nElem; elem++)
    {
      // get an element's data
      xfldCellGroup.getElement(xfldElem, elem);
      std::array<int, Topology::NNode> nodeMap;

      xfldCellGroup.associativity(elem).getNodeGlobalMapping(nodeMap.data(), nodeMap.size());

      // get ready to store local patches
      typedef XField_LocalPatch<PhysD2, Topology> XFieldLocal;
      std::unique_ptr<XFieldLocal> pxfld_local;

      // if we split, we loop over edges to split, otherwise just go through once
      for (int edge= 0; edge < (isSplit ? nEdge : 1); edge++)
      {
        // in the absence of splitting, grab the unsplit local patch
        XField_LocalPatchConstructor<PhysD2, Topology> xfld_construct(comm_local, xfld_connectivity, group, elem, SpaceType::Discontinuous);
        if (!isSplit)
          pxfld_local= make_unique<XFieldLocal>(xfld_construct);
        else
          pxfld_local= make_unique<XFieldLocal>(xfld_construct, ElementSplitType::Edge, edge);

        const XFieldLocal& xfld_local= *pxfld_local;

        // then grab the solution on the local patch
        Field_Local<QField2D_DG_Area> qfld_local(xfld_local, qfld, order, BasisFunctionCategory_Legendre);
        // Field_Local<QField2D_DG_Line> lgfld_local(xfld_local, lgfld, order, BasisFunctionCategory_Legendre);

        // connectivities should match
        BOOST_CHECK_EQUAL(&xfld_connectivity, &xfld_local.connectivity());

        // dofs on constructor should be at least a triangle
        BOOST_CHECK(xfld_construct.nDOF() > 2);
        // dofs on split should be one more than the constructor
        BOOST_CHECK_EQUAL(xfld_construct.nDOF(), xfld_local.nDOF() - 1);

        // two cellgroups -> one for active split and one for others
        BOOST_CHECK_EQUAL(xfld_local.nCellGroups(), 2);
        BOOST_REQUIRE(xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle));
        BOOST_REQUIRE(xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle));

        // break out the cellgroups
        const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0= xfld_local.getCellGroup(0);
        // const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup1= xfld_local.getCellGroup(1);

        // split cellgroup should have two newly split elements
        BOOST_CHECK_EQUAL(xfldCellGroup0.nElem(), 2);

        // qfld cell groups
        BOOST_CHECK_EQUAL(qfld_local.nCellGroups(), 2);
        BOOST_REQUIRE(qfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle));
        BOOST_REQUIRE(qfld_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle));

        // // break out the cellgroups
        // const Field<PhysD2, TopoD2, ArrayQ>::FieldCellGroupType<Triangle>& qfldCellGroup0= qfld_local.getCellGroup(0);
        // const Field<PhysD2, TopoD2, ArrayQ>::FieldCellGroupType<Triangle>& qfldCellGroup1= qfld_local.getCellGroup(1);
        //
        // // split cellgroup should have two newly split elements
        // BOOST_CHECK_EQUAL(qfldCellGroup0.nElem(), 2);

        // interior-edge field variable
        BOOST_REQUIRE(xfld_local.nInteriorTraceGroups() > 0 && xfld_local.nInteriorTraceGroups() <= 3 ); // new split, localpatch boundaries

        const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup0= xfld_local.getInteriorTraceGroup(0);
        const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup1= xfld_local.getInteriorTraceGroup(1);
        BOOST_REQUIRE_EQUAL(xfldITraceGroup0.nElem(), 1);
        BOOST_REQUIRE(xfldITraceGroup1.nElem() > 0);

        BOOST_REQUIRE(xfldITraceGroup0.topoTypeID() == typeid(Line));
        BOOST_REQUIRE(xfldITraceGroup1.topoTypeID() == typeid(Line));

        // boundary-edge field variable
        for (int bnd= 0; bnd < xfld_local.nBoundaryTraceGroups(); bnd++)
        {
          const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup= xfld_local.getBoundaryTraceGroup(bnd);

          BOOST_REQUIRE(xfldBTraceGroup.topoTypeID() == typeid(Line));

          BOOST_REQUIRE(xfldBTraceGroup.nElem() > 0);

        }

        BOOST_REQUIRE_EQUAL(xfld_local.nGhostBoundaryTraceGroups(), 1);

        for (int bnd= 0; bnd < xfld_local.nGhostBoundaryTraceGroups(); bnd++)
        {
          const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldGBTraceGroup= xfld_local.getGhostBoundaryTraceGroup(bnd);
          BOOST_REQUIRE(xfldGBTraceGroup.nElem() == 2 || xfldGBTraceGroup.nElem() == 6); // either two touching elements or six!
        }

        // check that all local elements match the global elements
        XField_LocalGlobal_Equiv(xfld_local);
      }
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE(Field2D_DG_Local_4Triangle_X1_ExtractCells_SplitTraces_metadata_P2)
{
  // this test will run through all of the possible splits on all edges of all
  // elements of all groups and make sure, on each of the above, the metadata is
  // correct in as far as is knowable. use in conjunction with the solution
  // transfer tests above.

  typedef typename Simplex<TopoD2>::type Topology;

  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_DG_Area;
  // typedef Field_DG_BoundaryTrace< PhysD2, TopoD2, ArrayQ > QField2D_DG_Line;

  // typedef DLA::VectorS<TopoD2::D,Real> RefCoordType;

  // typedef QField2D_DG_Area::FieldCellGroupType<Triangle> FieldCellGroupType;
  // typedef FieldCellGroupType::ElementType<> ElementFieldClass;

  typedef typename XField<PhysD2, TopoD2>::template FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

  const int nEdge= Topology::NEdge; // Number of different edges

  mpi::communicator world;
  mpi::communicator comm_local= world.split(world.rank());

  // Real tol= 1e-11;
  bool isSplit= true;

  XField2D_4Triangle_X1_1Group xfld;

  int order= 2;
  QField2D_DG_Area qfld(xfld, order, BasisFunctionCategory_Legendre);
  // QField2D_DG_Line lgfld(xfld, order, BasisFunctionCategory_Legendre);

  // set some non-zero solution
  for (int n= 0; n < qfld.nDOF(); n++)
    qfld.DOF(n)= {10*pow(-1, n%2) + n, 10*pow(-1, n%2) + n};

  // // set some non-zero lagrange multiplier/boundary solution
  // for (int n= 0; n < lgfld.nDOF(); n++)
  //   lgfld.DOF(n)= {1*(-n%3) - 1, (n%3) + 1};

  // loop over cell groups in the mesh
  for (int group= 0; group < xfld.nCellGroups(); group++)
  {
    // grab the group and its metadata
    XFieldCellGroupType xfldCellGroup= xfld.getCellGroup<Topology>(group);
    const int nElem= xfldCellGroup.nElem();

    // prep to grab individual elements/element data
    ElementXFieldClass xfldElem(xfldCellGroup.basis());

    const XField_CellToTrace<PhysD2, TopoD2> xfld_connectivity(xfld);

    // loop over all the elements in the cell group
    for (int elem= 0; elem < nElem; elem++)
    {
      // get an element's data
      xfldCellGroup.getElement(xfldElem, elem);
      std::array<int, Topology::NNode> nodeMap;

      xfldCellGroup.associativity(elem).getNodeGlobalMapping(nodeMap.data(), nodeMap.size());

      // get ready to store local patches
      typedef XField_LocalPatch<PhysD2, Topology> XFieldLocal;
      std::unique_ptr<XFieldLocal> pxfld_local;

      // if we split, we loop over edges to split, otherwise just go through once
      for (int edge= 0; edge < (isSplit ? nEdge : 1); edge++)
      {
        // in the absence of splitting, grab the unsplit local patch
        XField_LocalPatchConstructor<PhysD2, Topology> xfld_construct(comm_local, xfld_connectivity, group, elem, SpaceType::Discontinuous);
        if (!isSplit)
          pxfld_local= make_unique<XFieldLocal>(xfld_construct);
        else
          pxfld_local= make_unique<XFieldLocal>(xfld_construct, ElementSplitType::Edge, edge);

        const XFieldLocal& xfld_local= *pxfld_local;

        // then grab the solution on the local patch
        Field_Local<QField2D_DG_Area> qfld_local(xfld_local, qfld, order, BasisFunctionCategory_Legendre);
        // Field_Local<QField2D_DG_Line> lgfld_local(xfld_local, lgfld, order, BasisFunctionCategory_Legendre);

        // connectivities should match
        BOOST_CHECK_EQUAL(&xfld_connectivity, &xfld_local.connectivity());

        // dofs on constructor should be at least a triangle
        BOOST_CHECK(xfld_construct.nDOF() > 2);
        // dofs on split should be one more than the constructor
        BOOST_CHECK_EQUAL(xfld_construct.nDOF(), xfld_local.nDOF() - 1);

        // two cellgroups -> one for active split and one for others
        BOOST_CHECK_EQUAL(xfld_local.nCellGroups(), 2);
        BOOST_REQUIRE(xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle));
        BOOST_REQUIRE(xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle));

        // break out the cellgroups
        const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0= xfld_local.getCellGroup(0);
        // const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup1= xfld_local.getCellGroup(1);

        // split cellgroup should have two newly split elements
        BOOST_CHECK_EQUAL(xfldCellGroup0.nElem(), 2);

        // qfld cell groups
        BOOST_CHECK_EQUAL(qfld_local.nCellGroups(), 2);
        BOOST_REQUIRE(qfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle));
        BOOST_REQUIRE(qfld_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle));

        // // break out the cellgroups
        // const Field<PhysD2, TopoD2, ArrayQ>::FieldCellGroupType<Triangle>& qfldCellGroup0= qfld_local.getCellGroup(0);
        // const Field<PhysD2, TopoD2, ArrayQ>::FieldCellGroupType<Triangle>& qfldCellGroup1= qfld_local.getCellGroup(1);
        //
        // // split cellgroup should have two newly split elements
        // BOOST_CHECK_EQUAL(qfldCellGroup0.nElem(), 2);

        // interior-edge field variable
        BOOST_REQUIRE(xfld_local.nInteriorTraceGroups() > 0 && xfld_local.nInteriorTraceGroups() <= 3 ); // new split, localpatch boundaries

        const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup0= xfld_local.getInteriorTraceGroup(0);
        const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup1= xfld_local.getInteriorTraceGroup(1);
        BOOST_REQUIRE_EQUAL(xfldITraceGroup0.nElem(), 1);
        BOOST_REQUIRE(xfldITraceGroup1.nElem() > 0);

        BOOST_REQUIRE(xfldITraceGroup0.topoTypeID() == typeid(Line));
        BOOST_REQUIRE(xfldITraceGroup1.topoTypeID() == typeid(Line));

        // boundary-edge field variable
        for (int bnd= 0; bnd < xfld_local.nBoundaryTraceGroups(); bnd++)
        {
          const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup= xfld_local.getBoundaryTraceGroup(bnd);

          BOOST_REQUIRE(xfldBTraceGroup.topoTypeID() == typeid(Line));

          BOOST_REQUIRE(xfldBTraceGroup.nElem() > 0);

        }

        BOOST_REQUIRE_EQUAL(xfld_local.nGhostBoundaryTraceGroups(), 1);

        for (int bnd= 0; bnd < xfld_local.nGhostBoundaryTraceGroups(); bnd++)
        {
          const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldGBTraceGroup= xfld_local.getGhostBoundaryTraceGroup(bnd);
          BOOST_REQUIRE(xfldGBTraceGroup.nElem() == 2 || xfldGBTraceGroup.nElem() == 6); // either two touching elements or six!
        }

        // check that all local elements match the global elements
        XField_LocalGlobal_Equiv(xfld_local);
      }
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE(Field2D_DG_Local_4Triangle_X1_ExtractCells_SplitTraces_metadata_P3)
{
  // this test will run through all of the possible splits on all edges of all
  // elements of all groups and make sure, on each of the above, the metadata is
  // correct in as far as is knowable. use in conjunction with the solution
  // transfer tests above.

  typedef typename Simplex<TopoD2>::type Topology;

  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_DG_Area;
  // typedef Field_DG_BoundaryTrace< PhysD2, TopoD2, ArrayQ > QField2D_DG_Line;

  // typedef DLA::VectorS<TopoD2::D,Real> RefCoordType;

  // typedef QField2D_DG_Area::FieldCellGroupType<Triangle> FieldCellGroupType;
  // typedef FieldCellGroupType::ElementType<> ElementFieldClass;

  typedef typename XField<PhysD2, TopoD2>::template FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

  const int nEdge= Topology::NEdge; // Number of different edges

  mpi::communicator world;
  mpi::communicator comm_local= world.split(world.rank());

  // Real tol= 1e-11;
  bool isSplit= true;

  XField2D_4Triangle_X1_1Group xfld;

  int order= 3;
  QField2D_DG_Area qfld(xfld, order, BasisFunctionCategory_Legendre);
  // QField2D_DG_Line lgfld(xfld, order, BasisFunctionCategory_Legendre);

  // set some non-zero solution
  for (int n= 0; n < qfld.nDOF(); n++)
    qfld.DOF(n)= {10*pow(-1, n%2) + n, 10*pow(-1, n%2) + n};

  // // set some non-zero lagrange multiplier/boundary solution
  // for (int n= 0; n < lgfld.nDOF(); n++)
  //   lgfld.DOF(n)= {1*(-n%3) - 1, (n%3) + 1};

  // loop over cell groups in the mesh
  for (int group= 0; group < xfld.nCellGroups(); group++)
  {
    // grab the group and its metadata
    XFieldCellGroupType xfldCellGroup= xfld.getCellGroup<Topology>(group);
    const int nElem= xfldCellGroup.nElem();

    // prep to grab individual elements/element data
    ElementXFieldClass xfldElem(xfldCellGroup.basis());

    const XField_CellToTrace<PhysD2, TopoD2> xfld_connectivity(xfld);

    // loop over all the elements in the cell group
    for (int elem= 0; elem < nElem; elem++)
    {
      // get an element's data
      xfldCellGroup.getElement(xfldElem, elem);
      std::array<int, Topology::NNode> nodeMap;

      xfldCellGroup.associativity(elem).getNodeGlobalMapping(nodeMap.data(), nodeMap.size());

      // get ready to store local patches
      typedef XField_LocalPatch<PhysD2, Topology> XFieldLocal;
      std::unique_ptr<XFieldLocal> pxfld_local;

      // if we split, we loop over edges to split, otherwise just go through once
      for (int edge= 0; edge < (isSplit ? nEdge : 1); edge++)
      {
        // in the absence of splitting, grab the unsplit local patch
        XField_LocalPatchConstructor<PhysD2, Topology> xfld_construct(comm_local, xfld_connectivity, group, elem, SpaceType::Discontinuous);
        if (!isSplit)
          pxfld_local= make_unique<XFieldLocal>(xfld_construct);
        else
          pxfld_local= make_unique<XFieldLocal>(xfld_construct, ElementSplitType::Edge, edge);

        const XFieldLocal& xfld_local= *pxfld_local;

        // then grab the solution on the local patch
        Field_Local<QField2D_DG_Area> qfld_local(xfld_local, qfld, order, BasisFunctionCategory_Legendre);
        // Field_Local<QField2D_DG_Line> lgfld_local(xfld_local, lgfld, order, BasisFunctionCategory_Legendre);

        // connectivities should match
        BOOST_CHECK_EQUAL(&xfld_connectivity, &xfld_local.connectivity());

        // dofs on constructor should be at least a triangle
        BOOST_CHECK(xfld_construct.nDOF() > 2);
        // dofs on split should be one more than the constructor
        BOOST_CHECK_EQUAL(xfld_construct.nDOF(), xfld_local.nDOF() - 1);

        // two cellgroups -> one for active split and one for others
        BOOST_CHECK_EQUAL(xfld_local.nCellGroups(), 2);
        BOOST_REQUIRE(xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle));
        BOOST_REQUIRE(xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle));

        // break out the cellgroups
        const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0= xfld_local.getCellGroup(0);
        // const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup1= xfld_local.getCellGroup(1);

        // split cellgroup should have two newly split elements
        BOOST_CHECK_EQUAL(xfldCellGroup0.nElem(), 2);

        // qfld cell groups
        BOOST_CHECK_EQUAL(qfld_local.nCellGroups(), 2);
        BOOST_REQUIRE(qfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle));
        BOOST_REQUIRE(qfld_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle));

        // // break out the cellgroups
        // const Field<PhysD2, TopoD2, ArrayQ>::FieldCellGroupType<Triangle>& qfldCellGroup0= qfld_local.getCellGroup(0);
        // const Field<PhysD2, TopoD2, ArrayQ>::FieldCellGroupType<Triangle>& qfldCellGroup1= qfld_local.getCellGroup(1);
        //
        // // split cellgroup should have two newly split elements
        // BOOST_CHECK_EQUAL(qfldCellGroup0.nElem(), 2);

        // interior-edge field variable
        BOOST_REQUIRE(xfld_local.nInteriorTraceGroups() > 0 && xfld_local.nInteriorTraceGroups() <= 3 ); // new split, localpatch boundaries

        const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup0= xfld_local.getInteriorTraceGroup(0);
        const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup1= xfld_local.getInteriorTraceGroup(1);
        BOOST_REQUIRE_EQUAL(xfldITraceGroup0.nElem(), 1);
        BOOST_REQUIRE(xfldITraceGroup1.nElem() > 0);

        BOOST_REQUIRE(xfldITraceGroup0.topoTypeID() == typeid(Line));
        BOOST_REQUIRE(xfldITraceGroup1.topoTypeID() == typeid(Line));

        // boundary-edge field variable
        for (int bnd= 0; bnd < xfld_local.nBoundaryTraceGroups(); bnd++)
        {
          const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup= xfld_local.getBoundaryTraceGroup(bnd);

          BOOST_REQUIRE(xfldBTraceGroup.topoTypeID() == typeid(Line));

          BOOST_REQUIRE(xfldBTraceGroup.nElem() > 0);

        }

        BOOST_REQUIRE_EQUAL(xfld_local.nGhostBoundaryTraceGroups(), 1);

        for (int bnd= 0; bnd < xfld_local.nGhostBoundaryTraceGroups(); bnd++)
        {
          const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldGBTraceGroup= xfld_local.getGhostBoundaryTraceGroup(bnd);
          BOOST_REQUIRE(xfldGBTraceGroup.nElem() == 2 || xfldGBTraceGroup.nElem() == 6); // either two touching elements or six!
        }

        // check that all local elements match the global elements
        XField_LocalGlobal_Equiv(xfld_local);
      }
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
