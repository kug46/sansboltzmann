// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"     // Real
#include "Topology/ElementTopology.h"

#include "Field/XField_CellToTrace.h"
#include "Field/Local/XField_LocalPatch.h"
#include "Field/Field_NodalView.h"

#include "Meshing/XField1D/XField1D.h"

#include "XField_LocalGlobal_Equiv_btest.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( XField1D_LocalPatch_ElementDual_test_suite )

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_3Line_Interior_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField1D xfld(3,0,3); //Linear mesh with 3 lines from 0 to 3

  // Build cell to trace connectivity structure
  XField_CellToTrace<PhysD1,TopoD1> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // connecting nodes
  const int group = 0, elem = 1;

  //Extract the local mesh for the bottom-left Line
  XField_LocalPatchConstructor<PhysD1,Line> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Continuous,&nodalview);
  XField_LocalPatch<PhysD1,Line> xfld_local(xfld_construct);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 4 );
  BOOST_CHECK_EQUAL( xfld_construct.nDOF(), 4 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld_local.DOF(0)[0],  1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(1)[0],  2 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(2)[0],  0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(3)[0],  3 );

  // area field variable

  BOOST_REQUIRE_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Line) );

  const XField<PhysD1,TopoD1>::FieldCellGroupType<Line>& xfldCellGroup0 = xfld_local.getCellGroup<Line>(0);
  const XField<PhysD1,TopoD1>::FieldCellGroupType<Line>& xfldCellGroup1 = xfld_local.getCellGroup<Line>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 2 );

  // interior-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Node>(0).topoTypeID() == typeid(Node) );

  const XField<PhysD1,TopoD1>::FieldTraceGroupType<Node>& xfldITraceGroup = xfld_local.getInteriorTraceGroup<Node>(0);

  BOOST_CHECK_EQUAL( xfldITraceGroup.nElem(), 2 );

  // boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nBoundaryTraceGroups(), 2 );

  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Node>(0).topoTypeID() == typeid(Node) );
  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Node>(1).topoTypeID() == typeid(Node) );

  const XField<PhysD1,TopoD1>::FieldTraceGroupType<Node>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Node>(0);
  const XField<PhysD1,TopoD1>::FieldTraceGroupType<Node>& xfldBTraceGroup1 = xfld_local.getBoundaryTraceGroup<Node>(1);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.nElem(), 1 );

  // ghost boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 ); // Another quirk of the local patch

  const XField<PhysD1,TopoD1>::FieldTraceGroupType<Node>& xfldGBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 0 );

  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);

}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_3Line_Interior_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField1D xfld(3,0,3); //Linear mesh with 3 lines from 0 to 3

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD1,TopoD1> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // connecting nodes
  const int group = 0, elem = 1, edge = 0;

  //Extract the local mesh for the central Line
  XField_LocalPatchConstructor<PhysD1,Line> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Continuous,&nodalview);
  XField_LocalPatch<PhysD1,Line> xfld_local(xfld_construct, ElementSplitType::Edge, edge);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 5 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld_local.DOF(0)[0],  1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(1)[0],  2 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(2)[0],  0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(3)[0],  3 );
  BOOST_CHECK_CLOSE( xfld_local.DOF(4)[0],  3./2, 1e-8 );

  // area field variable

  BOOST_REQUIRE_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Line) );

  const XField<PhysD1,TopoD1>::FieldCellGroupType<Line>& xfldCellGroup0 = xfld_local.getCellGroup<Line>(0);
  const XField<PhysD1,TopoD1>::FieldCellGroupType<Line>& xfldCellGroup1 = xfld_local.getCellGroup<Line>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 2 );

  // interior-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nInteriorTraceGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Node>(0).topoTypeID() == typeid(Node) );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Node>(1).topoTypeID() == typeid(Node) );

  const XField<PhysD1,TopoD1>::FieldTraceGroupType<Node>& xfldITraceGroup0 = xfld_local.getInteriorTraceGroup<Node>(0);
  const XField<PhysD1,TopoD1>::FieldTraceGroupType<Node>& xfldITraceGroup1 = xfld_local.getInteriorTraceGroup<Node>(1);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 2 );

  // boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nBoundaryTraceGroups(), 2 );

  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Node>(0).topoTypeID() == typeid(Node) );
  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Node>(1).topoTypeID() == typeid(Node) );

  const XField<PhysD1,TopoD1>::FieldTraceGroupType<Node>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Node>(0);
  const XField<PhysD1,TopoD1>::FieldTraceGroupType<Node>& xfldBTraceGroup1 = xfld_local.getBoundaryTraceGroup<Node>(1);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.nElem(), 1 );

  // ghost boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 ); // Another quirk of the local patch

  const XField<PhysD1,TopoD1>::FieldTraceGroupType<Node>& xfldGBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 0 );

  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_3Line_Boundary0_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField1D xfld(3,0,3); //Linear mesh with 3 lines from 0 to 3

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD1,TopoD1> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // connecting nodes
  const int group = 0, elem = 0;

  //Extract the local mesh for the central Line
  XField_LocalPatchConstructor<PhysD1,Line> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Continuous,&nodalview);
  XField_LocalPatch<PhysD1,Line> xfld_local(xfld_construct);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 3 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld_local.DOF(0)[0],  0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(1)[0],  1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(2)[0],  2 );

  // area field variable

  BOOST_REQUIRE_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Line) );

  const XField<PhysD1,TopoD1>::FieldCellGroupType<Line>& xfldCellGroup0 = xfld_local.getCellGroup<Line>(0);
  const XField<PhysD1,TopoD1>::FieldCellGroupType<Line>& xfldCellGroup1 = xfld_local.getCellGroup<Line>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 1 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Node>(0).topoTypeID() == typeid(Node) );

  const XField<PhysD1,TopoD1>::FieldTraceGroupType<Node>& xfldITraceGroup = xfld_local.getInteriorTraceGroup<Node>(0);

  BOOST_CHECK_EQUAL( xfldITraceGroup.nElem(), 1 );

  // boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Node>(0).topoTypeID() == typeid(Node) );

  const XField<PhysD1,TopoD1>::FieldTraceGroupType<Node>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Node>(0);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 1 );

  // ghost boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );

  const XField<PhysD1,TopoD1>::FieldTraceGroupType<Node>& xfldGBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 1 );

  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_3Line_Boundary0_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField1D xfld(3,0,3); //Linear mesh with 3 lines from 0 to 3

  // Build cell to trace connectivity structure
  XField_CellToTrace<PhysD1,TopoD1> connectivity(xfld);

  // Build not to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  const int group = 0, elem = 0, edge = 0;

  // Extract the unsplit grid
  XField_LocalPatchConstructor<PhysD1,Line> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Continuous,&nodalview);

  // Split the grid
  XField_LocalPatch<PhysD1,Line> xfld_local(xfld_construct, ElementSplitType::Edge, edge);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_REQUIRE_EQUAL( xfld_local.nDOF(), 4 );

//  //Check the node values
  BOOST_CHECK_EQUAL( xfld_local.DOF(0)[0],  0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(1)[0],  1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(2)[0],  2 );
  BOOST_CHECK_CLOSE( xfld_local.DOF(3)[0],  1./2, 1e-8 );

  // area field variable

  BOOST_REQUIRE_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Line) );

  const XField<PhysD1,TopoD1>::FieldCellGroupType<Line>& xfldCellGroup0 = xfld_local.getCellGroup<Line>(0);
  const XField<PhysD1,TopoD1>::FieldCellGroupType<Line>& xfldCellGroup1 = xfld_local.getCellGroup<Line>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 1 );

  // interior-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nInteriorTraceGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Node>(0).topoTypeID() == typeid(Node) );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Node>(1).topoTypeID() == typeid(Node) );

  const XField<PhysD1,TopoD1>::FieldTraceGroupType<Node>& xfldITraceGroup0 = xfld_local.getInteriorTraceGroup<Node>(0);
  const XField<PhysD1,TopoD1>::FieldTraceGroupType<Node>& xfldITraceGroup1 = xfld_local.getInteriorTraceGroup<Node>(1);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 1 );

  // boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Node>(0).topoTypeID() == typeid(Node) );

  const XField<PhysD1,TopoD1>::FieldTraceGroupType<Node>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Node>(0);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 1 );

  // ghost boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );

  const XField<PhysD1,TopoD1>::FieldTraceGroupType<Node>& xfldGBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 1 );

  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_3Line_Boundary1_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField1D xfld(3,0,3); //Linear mesh with 3 lines from 0 to 3

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD1,TopoD1> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // connecting nodes
  const int group = 0, elem = 2;

  // Extract the unsplit grid
  XField_LocalPatchConstructor<PhysD1,Line> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Continuous,&nodalview);
  XField_LocalPatch<PhysD1,Line> xfld_local(xfld_construct);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_REQUIRE_EQUAL( xfld_local.nDOF(), 3 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld_local.DOF(0)[0],  2 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(1)[0],  3 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(2)[0],  1 );

  // area field variable

  BOOST_REQUIRE_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Line) );

  const XField<PhysD1,TopoD1>::FieldCellGroupType<Line>& xfldCellGroup0 = xfld_local.getCellGroup<Line>(0);
  const XField<PhysD1,TopoD1>::FieldCellGroupType<Line>& xfldCellGroup1 = xfld_local.getCellGroup<Line>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 1 );

  // interior-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Node>(0).topoTypeID() == typeid(Node) );

  const XField<PhysD1,TopoD1>::FieldTraceGroupType<Node>& xfldITraceGroup = xfld_local.getInteriorTraceGroup<Node>(0);

  BOOST_CHECK_EQUAL( xfldITraceGroup.nElem(), 1 );

  // boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nBoundaryTraceGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Node>(0).topoTypeID() == typeid(Node) );

  const XField<PhysD1,TopoD1>::FieldTraceGroupType<Node>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Node>(0);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 1 );

  // ghost boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );

  const XField<PhysD1,TopoD1>::FieldTraceGroupType<Node>& xfldGBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 1 );

  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_3Line_Boundary1_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField1D xfld(3,0,3); //Linear mesh with 3 lines from 0 to 3

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD1,TopoD1> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  const int group = 0, elem = 2, edge = 0;

  // Extract the unsplit grid
  XField_LocalPatchConstructor<PhysD1,Line> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Continuous,&nodalview);

  // Split the grid
  XField_LocalPatch<PhysD1,Line> xfld_local(xfld_construct, ElementSplitType::Edge, edge);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_REQUIRE_EQUAL( xfld_local.nDOF(), 4 );

//  //Check the node values
  BOOST_CHECK_EQUAL( xfld_local.DOF(0)[0],  2 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(1)[0],  3 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(2)[0],  1 );
  BOOST_CHECK_CLOSE( xfld_local.DOF(3)[0],  5./2, 1e-8 );

  // area field variable

  BOOST_REQUIRE_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Line) );

  const XField<PhysD1,TopoD1>::FieldCellGroupType<Line>& xfldCellGroup0 = xfld_local.getCellGroup<Line>(0);
  const XField<PhysD1,TopoD1>::FieldCellGroupType<Line>& xfldCellGroup1 = xfld_local.getCellGroup<Line>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 1 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nInteriorTraceGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Node>(0).topoTypeID() == typeid(Node) );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Node>(1).topoTypeID() == typeid(Node) );

  const XField<PhysD1,TopoD1>::FieldTraceGroupType<Node>& xfldITraceGroup0 = xfld_local.getInteriorTraceGroup<Node>(0);
  const XField<PhysD1,TopoD1>::FieldTraceGroupType<Node>& xfldITraceGroup1 = xfld_local.getInteriorTraceGroup<Node>(1);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 1 );

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Node>(0).topoTypeID() == typeid(Node) );

  const XField<PhysD1,TopoD1>::FieldTraceGroupType<Node>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Node>(0);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 1 );

  // ghost boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );

  const XField<PhysD1,TopoD1>::FieldTraceGroupType<Node>& xfldGBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 1 );

  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
