INCLUDE( ${CMAKE_SOURCE_DIR}/CMakeInclude/ForceOutOfSource.cmake ) #This must be the first thing included

RUN_WITH_MPIEXEC( ${MPIEXEC_MAX_NUMPROCS} )

GenerateUnitTests( UnitGridsLib
                   FieldLib
                   XField1DLib
                   DenseLinAlgLib
                   BasisFunctionLib
                   QuadratureLib
                   TopologyLib
                   toolsLib
                 )

IF (BUILD_TESTING AND ${XDIR_FOLDER})
  SET_TESTS_PROPERTIES(Field4D_LocalPatch_Edge_CG_btest    PROPERTIES TIMEOUT 180)
  SET_TESTS_PROPERTIES(Field4D_LocalPatch_Element_DG_btest PROPERTIES TIMEOUT 180)
ENDIF()
