// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Field2D_EdgeLocal_CG_btest
//

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Meshing/XField1D/XField1D.h"

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunctionLine.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLiftLine_DG_Cell.h"
#include "Field/Local/Field_Local.h"
#include "Field/Field_NodalView.h"

#include "Field/Local/XField_LocalPatch.h"

#include "Field_LocalPatch_toolkit_btest.h"

#include "tools/make_unique.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

//Explicitly instantiate classes to get proper coverage information
namespace SANS
{
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( Field1D_LocalPatch_Element_DG_test_suite )

//============================================================================//
// Hierarchical
//============================================================================//

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_Hierarchical_P1 )
{
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5

  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = false;
  localElementExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Hierarchical_P1 )
{
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5

  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = true;
  localElementExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_Hierarchical_P2 )
{
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5

  int order = 2;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = false;
  localElementExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Hierarchical_P2 )
{
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5

  int order = 2;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = true;
  localElementExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_Hierarchical_P3 )
{
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5

  int order = 3;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = false;
  localElementExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Hierarchical_P3 )
{
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5

  int order = 3;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = true;
  localElementExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

//============================================================================//
// Lagrange
//============================================================================//

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_Lagrange_P1 )
{
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5

  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = false;
  localElementExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Lagrange_P1 )
{
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5

  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = true;
  localElementExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_Lagrange_P2 )
{
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5

  int order = 2;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = false;
  localElementExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Lagrange_P2 )
{
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5

  int order = 2;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = true;
  localElementExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_Lagrange_P3 )
{
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5

  int order = 3;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = false;
  localElementExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Lagrange_P3 )
{
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5

  int order = 3;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = true;
  localElementExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

//============================================================================//
// Legendre
//============================================================================//

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_Legendre_P1 )
{
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5

  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Legendre;

  bool split_test = false;
  localElementExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Legendre_P1 )
{
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5

  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Legendre;

  bool split_test = true;
  localElementExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_Legendre_P2 )
{
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5

  int order = 2;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Legendre;

  bool split_test = false;
  localElementExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Legendre_P2 )
{
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5

  int order = 2;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Legendre;

  bool split_test = true;
  localElementExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_Legendre_P3 )
{
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5

  int order = 3;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Legendre;

  bool split_test = false;
  localElementExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Legendre_P3 )
{
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5

  int order = 3;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Legendre;

  bool split_test = true;
  localElementExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
