// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"     // Real
#include "Topology/ElementTopology.h"

#include "Field/XField_CellToTrace.h"
#include "Field/Local/XField_LocalPatch.h"
#include "Field/Field_NodalView.h"

#include "Field/FieldArea_CG_Cell.h"

#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_LocalRefine_Triangle_X1.h"

#include "XField_LocalGlobal_Equiv_btest.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( XField2D_LocalPatch_Edge_test_suite )

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_2Triangle_InteriorTrace_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField2D_2Triangle_X1_1Group xfld;

  // Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // connecting nodes
  std::array<int,2> edge{{1,2}};

  //Extract the local mesh for the bottom-left triangle
  XField_LocalPatchConstructor<PhysD2,Triangle> xfld_construct(comm_local,connectivity,edge,SpaceType::Continuous,&nodalview);
  XField_LocalPatch<PhysD2,Triangle> xfld_local(xfld_construct);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 4 );
  BOOST_CHECK_EQUAL( xfld_construct.nDOF(), 4 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld_local.DOF(0)(0),  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(0)(1),  0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(1)(0),  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(1)(1),  1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(2)(0),  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(2)(1),  0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(3)(0),  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(3)(1),  1 );

  // area field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_local.getCellGroup(0);
  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup1 = xfld_local.getCellGroup(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 0 ); // This is a quirk of XField_LocalPatch

  // interior-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup = xfld_local.getInteriorTraceGroup(0);

  BOOST_CHECK_EQUAL( xfldITraceGroup.nElem(), 1 );

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup(0);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 4 );

  // ghost boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 ); // Another quirk of the local patch

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldGBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 0 );

  //Checking the commonNodes_ interfaces
  std::vector<int> commonNodes = xfld_local.getLinearCommonNodes();
  std::vector<int> localCommonNodes = xfld_local.getLocalLinearCommonNodes();

  BOOST_CHECK_EQUAL( commonNodes.size(), 4 );
  BOOST_CHECK_EQUAL( localCommonNodes.size(), 4 );

  // Check that the first two nodes are the edge
  BOOST_CHECK_EQUAL( commonNodes[0], edge[0] );
  BOOST_CHECK_EQUAL( commonNodes[1], edge[1] );

  // Check that the edge is nodes 0 and 1
  BOOST_CHECK_EQUAL( localCommonNodes[0], 0 );
  BOOST_CHECK_EQUAL( localCommonNodes[1], 1 );

  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_2Triangle_BoundaryTrace_1_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField2D_2Triangle_X1_1Group xfld;

  // Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  // Build not to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // connecting nodes
  std::array<int,2> edge{{0,1}};

  //Extract the local mesh for the bottom-left triangle
  XField_LocalPatchConstructor<PhysD2,Triangle> xfld_construct(comm_local,connectivity,edge,SpaceType::Continuous,&nodalview);
  XField_LocalPatch<PhysD2,Triangle> xfld_local(xfld_construct);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );
  BOOST_CHECK_EQUAL( xfld_construct.nDOF(), 4 );
  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 4 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld_local.DOF(0)(0),  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(0)(1),  0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(1)(0),  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(1)(1),  0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(2)(0),  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(2)(1),  1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(3)(0),  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(3)(1),  1 );

  // area field variable
  BOOST_CHECK_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_local.getCellGroup<Triangle>(0);
  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup1 = xfld_local.getCellGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 1 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup = xfld_local.getInteriorTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldITraceGroup.nElem(), 1 );

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nBoundaryTraceGroups(), 2 );

  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(1).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup1 = xfld_local.getBoundaryTraceGroup<Line>(1);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.nElem(), 2 );

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 ); // Another quirk of the local patch

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldGBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 0 );

  //Checking the commonNodes_ interfaces
  std::vector<int> commonNodes = xfld_local.getLinearCommonNodes();
  std::vector<int> localCommonNodes = xfld_local.getLocalLinearCommonNodes();

  BOOST_CHECK_EQUAL( commonNodes.size(), 3 );
  BOOST_CHECK_EQUAL( localCommonNodes.size(), 3 );

  // Check that the first two nodes are the edge
  BOOST_CHECK_EQUAL( commonNodes[0], edge[0] );
  BOOST_CHECK_EQUAL( commonNodes[1], edge[1] );

  // Check that the edge is nodes 0 and 1
  BOOST_CHECK_EQUAL( localCommonNodes[0], 0 );
  BOOST_CHECK_EQUAL( localCommonNodes[1], 1 );

  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_2Triangle_BoundaryTrace_2_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField2D_2Triangle_X1_1Group xfld;

  // Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  // Build not to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // connecting nodes
  std::array<int,2> edge{{1,3}};

  //Extract the local mesh for the bottom-left triangle
  XField_LocalPatchConstructor<PhysD2,Triangle> xfld_construct(comm_local,connectivity,edge,SpaceType::Continuous,&nodalview);
  XField_LocalPatch<PhysD2,Triangle> xfld_local(xfld_construct);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 4 );
  BOOST_CHECK_EQUAL( xfld_construct.nDOF(), 4 );

  //Check the node values, first three added by first elem
  BOOST_CHECK_EQUAL( xfld_local.DOF(0)(0),  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(0)(1),  0 ); // top right
  BOOST_CHECK_EQUAL( xfld_local.DOF(1)(0),  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(1)(1),  1 ); // top left
  BOOST_CHECK_EQUAL( xfld_local.DOF(2)(0),  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(2)(1),  1 ); // bottom right
  BOOST_CHECK_EQUAL( xfld_local.DOF(3)(0),  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(3)(1),  0 ); // bottom left

  // area field variable
  BOOST_CHECK_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_local.getCellGroup<Triangle>(0);
  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup1 = xfld_local.getCellGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 1 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup = xfld_local.getInteriorTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldITraceGroup.nElem(), 1 );

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nBoundaryTraceGroups(), 2 );

  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(1).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup1 = xfld_local.getBoundaryTraceGroup<Line>(1);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.nElem(), 2 );

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 ); // Another quirk of the local patch

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldGBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 0 );

  //Checking the commonNodes_ interfaces
  std::vector<int> commonNodes = xfld_local.getLinearCommonNodes();
  std::vector<int> localCommonNodes = xfld_local.getLocalLinearCommonNodes();

  BOOST_CHECK_EQUAL( commonNodes.size(), 3 );
  BOOST_CHECK_EQUAL( localCommonNodes.size(), 3 );

  // Check that the first two nodes are the edge
  BOOST_CHECK_EQUAL( commonNodes[0], edge[0] );
  BOOST_CHECK_EQUAL( commonNodes[1], edge[1] );

  // Check that the edge is nodes 0 and 1
  BOOST_CHECK_EQUAL( localCommonNodes[0], 0 );
  BOOST_CHECK_EQUAL( localCommonNodes[1], 1 );

  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_4Triangle_InteriorEdge0_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());


  XField2D_4Triangle_X1_1Group xfld;

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // connecting nodes
  std::array<int,2> edge{{1,2}};

  //Extract the local mesh for the central triangle
  XField_LocalPatchConstructor<PhysD2,Triangle> xfld_construct(comm_local,connectivity,edge,SpaceType::Continuous,&nodalview);
  XField_LocalPatch<PhysD2,Triangle> xfld_local(xfld_construct);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_construct.nDOF(), 6 );
  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 6 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld_local.DOF(0)(0),  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(0)(1),  0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(1)(0),  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(1)(1),  1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(2)(0),  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(2)(1),  0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(3)(0),  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(3)(1),  1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(4)(0), -1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(4)(1),  1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(5)(0),  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(5)(1), -1 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nInteriorTraceGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup(1).topoTypeID() == typeid(Line) );

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nBoundaryTraceGroups(), 2 );

  for (int i = 0; i < 2; i++)
    BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup(i).topoTypeID() == typeid(Line) );

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 ); // Another quirk of the local patch

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldGBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 0 );

  //Checking the commonNodes_ interfaces
  std::vector<int> commonNodes = xfld_local.getLinearCommonNodes();
  std::vector<int> localCommonNodes = xfld_local.getLocalLinearCommonNodes();

  BOOST_CHECK_EQUAL( commonNodes.size(), 4 );
  BOOST_CHECK_EQUAL( localCommonNodes.size(), 4 );

  // Check that the first two nodes are the edge
  BOOST_CHECK_EQUAL( commonNodes[0], edge[0] );
  BOOST_CHECK_EQUAL( commonNodes[1], edge[1] );

  // Check that the edge is nodes 0 and 1
  BOOST_CHECK_EQUAL( localCommonNodes[0], 0 );
  BOOST_CHECK_EQUAL( localCommonNodes[1], 1 );

  std::vector<int> reSolveCellGroup = xfld_local.getReSolveCellGroups();
  BOOST_CHECK_EQUAL( reSolveCellGroup.size(), 2 );
  BOOST_CHECK_EQUAL( reSolveCellGroup[0], 0 );
  BOOST_CHECK_EQUAL( reSolveCellGroup[1], 1 );

  std::vector<int> reSolveInteriorTraceGroup = xfld_local.getReSolveInteriorTraceGroups();
  BOOST_CHECK_EQUAL( reSolveInteriorTraceGroup.size(), 2 );
  BOOST_CHECK_EQUAL( reSolveInteriorTraceGroup[0], 0 );
  BOOST_CHECK_EQUAL( reSolveInteriorTraceGroup[1], 1 );

  std::vector<int> reSolveBoundaryTraceGroup = xfld_local.getReSolveBoundaryTraceGroups();
  BOOST_CHECK_EQUAL( reSolveBoundaryTraceGroup.size(), 2 );
  BOOST_CHECK_EQUAL( reSolveBoundaryTraceGroup[0], 0 );
  BOOST_CHECK_EQUAL( reSolveBoundaryTraceGroup[1], 1 );

  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_4Triangle_BoundaryEdge5_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField2D_4Triangle_X1_1Group xfld;

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // connecting nodes
  std::array<int,2> edge{{4,0}};

  //Extract the local mesh for the bottom-left triangle
  XField_LocalPatchConstructor<PhysD2,Triangle> xfld_construct(comm_local,connectivity,edge,SpaceType::Continuous,&nodalview);
  XField_LocalPatch<PhysD2,Triangle> xfld_local(xfld_construct);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_construct.nDOF(), 6 );
  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 6 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld_local.DOF(0)(0), -1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(0)(1),  1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(1)(0),  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(1)(1),  0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(2)(0),  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(2)(1),  1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(3)(0),  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(3)(1),  0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(4)(0),  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(4)(1),  1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(5)(0),  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(5)(1), -1 );

  // area field variable
  BOOST_CHECK_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_local.getCellGroup<Triangle>(0);
  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup1 = xfld_local.getCellGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 3 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nInteriorTraceGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Line>(1).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup0 = xfld_local.getInteriorTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup1 = xfld_local.getInteriorTraceGroup<Line>(1);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 2 );

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nBoundaryTraceGroups(), 2 );

  for (int i = 0; i < xfld_local.nBoundaryTraceGroups(); i++)
    BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(i).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup1 = xfld_local.getBoundaryTraceGroup<Line>(1);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.nElem(), 4 );

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldGTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldGTraceGroup0.nElem(), 0 );

  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_Box_Triangle_InteriorEdge0_Exceptions_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;

  typedef Simplex<TopoDim>::type Topology;

  int ii = 3, jj = 3;
  // make it ([0,3],[0,3]) so the node locations easier
  XField2D_Box_Triangle_Lagrange_X1 xfld( comm_local, ii, jj, 0, 3, 0, 3 );

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysDim,TopoDim> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  typedef XField_LocalPatchConstructor<PhysDim,Topology> XField_LocalConstructor;

  // check that non matching edge throws exceptions
  std::array<int,2> edge{{10,5}};
  BOOST_CHECK_THROW( XField_LocalConstructor xfld_constructor(comm_local,connectivity,edge,SpaceType::Continuous,&nodalview), AssertionException );

  // check that passing two identical nodes throws exceptions
  edge[1] = edge[0];
  BOOST_CHECK_THROW( XField_LocalConstructor xfld_constructor(comm_local,connectivity,edge,SpaceType::Continuous,&nodalview), AssertionException );

  // check that not passing a pointer to a nodalview throws exceptions
  edge[0] = 9; edge[1] = 6;
  BOOST_CHECK_THROW( XField_LocalConstructor xfld_constructor(comm_local,connectivity,edge,SpaceType::Continuous,nullptr), AssertionException );

  XField_LocalConstructor xfld_constructor(comm_local,connectivity,edge,SpaceType::Continuous,&nodalview);
  XField_LocalPatch<PhysDim,Topology> xfld_local(xfld_constructor);

  for (int dof = 0; dof < xfld_constructor.nDOF(); dof++)
    for (int d = 0; d < PhysDim::D; d++)
      BOOST_CHECK_EQUAL( xfld_constructor.DOF(dof)(d), xfld_local.DOF(dof)(d) );

  // check that the groups and element numbers match. Also check that the template hiding getInterface works

  BOOST_CHECK_EQUAL( xfld_constructor.nCellGroups(), xfld_local.nCellGroups() );
  for (int group = 0; group < xfld_constructor.nCellGroups(); group++)
    BOOST_CHECK_EQUAL( xfld_constructor.getCellGroup(group).nElem(), xfld_local.getCellGroup(group).nElem() );

  BOOST_CHECK_EQUAL( xfld_constructor.nInteriorTraceGroups(), xfld_local.nInteriorTraceGroups() );
  for (int group = 0; group < xfld_constructor.nInteriorTraceGroups(); group++)
    BOOST_CHECK_EQUAL( xfld_constructor.getInteriorTraceGroup(group).nElem(), xfld_local.getInteriorTraceGroup(group).nElem() );

  BOOST_CHECK_EQUAL( xfld_constructor.nBoundaryTraceGroups(), xfld_local.nBoundaryTraceGroups() );
  for (int group = 0; group < xfld_constructor.nBoundaryTraceGroups(); group++)
    BOOST_CHECK_EQUAL( xfld_constructor.getBoundaryTraceGroup(group).nElem(), xfld_local.getBoundaryTraceGroup(group).nElem() );

  BOOST_CHECK_EQUAL( xfld_constructor.nGhostBoundaryTraceGroups(), xfld_local.nGhostBoundaryTraceGroups() );
  for (int group = 0; group < xfld_constructor.nGhostBoundaryTraceGroups(); group++)
    BOOST_CHECK_EQUAL( xfld_constructor.getGhostBoundaryTraceGroup(group).nElem(), xfld_local.getGhostBoundaryTraceGroup(group).nElem() );

  // check resolve groups

  {
  std::vector<int> resolve_constr = xfld_constructor.getReSolveCellGroups();
  std::vector<int> resolve_local = xfld_local.getReSolveCellGroups();
  BOOST_CHECK_EQUAL( resolve_constr.size(), resolve_local.size() );
  for (std::size_t group = 0; group < resolve_constr.size(); group++)
    BOOST_CHECK_EQUAL( resolve_constr[group], resolve_local[group] );
  }

  {
  std::vector<int> resolve_constr = xfld_constructor.getReSolveBoundaryTraceGroups();
  std::vector<int> resolve_local = xfld_local.getReSolveBoundaryTraceGroups();
  BOOST_CHECK_EQUAL( resolve_constr.size(), resolve_local.size() );
  for (std::size_t group = 0; group < resolve_constr.size(); group++)
    BOOST_CHECK_EQUAL( resolve_constr[group], resolve_local[group] );
  }

  {
  std::vector<int> resolve_constr = xfld_constructor.getReSolveInteriorTraceGroups();
  std::vector<int> resolve_local = xfld_local.getReSolveInteriorTraceGroups();
  BOOST_CHECK_EQUAL( resolve_constr.size(), resolve_local.size() );
  for (std::size_t group = 0; group < resolve_constr.size(); group++)
    BOOST_CHECK_EQUAL( resolve_constr[group], resolve_local[group] );
  }

  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_WholeGrid_Box_Triangle_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  int ii = 3, jj = 3;
  // make it ([0,3],[0,3]) so the node locations easier
  XField2D_Box_Triangle_Lagrange_X1 xfld( comm_local, ii, jj, 0, 3, 0, 3 );

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // Construct list of all edges in grid

  const int nEdge = Triangle::NEdge; // Number of different edges
  const int nNode = Triangle::NNode; // Number of nodes (vertices of the cell)

  typedef typename std::vector<std::array<int,Line::NNode>> EdgeList;
  EdgeList edge_list;

  const int (*EdgeNodes)[Line::NNode] = ElementEdges<Triangle>::EdgeNodes;

  for (int i = 0; i < xfld.nCellGroups(); i++)
  {
    for ( int elem = 0; elem < xfld.getCellGroup<Triangle>(i).nElem(); elem++ )
    {
      int nodeMap[Triangle::NNode];

      xfld.getCellGroup<Triangle>(i).associativity(elem).getNodeGlobalMapping( nodeMap, nNode );

      for ( int edge = 0; edge < nEdge; edge++ )
      {
        // loop over the edges
        std::array<int,2> newEdge;
        newEdge[0] = std::min(nodeMap[EdgeNodes[edge][0]],nodeMap[EdgeNodes[edge][1]]);
        newEdge[1] = std::max(nodeMap[EdgeNodes[edge][0]],nodeMap[EdgeNodes[edge][1]]);

        edge_list.push_back(newEdge);
      }
    }

    // need unique, std::vector is faster due to memory locality
    std::sort(edge_list.begin(), edge_list.end());
    edge_list.erase( std::unique(edge_list.begin(), edge_list.end() ), edge_list.end() );

    for ( std::size_t edge = 0; edge < edge_list.size(); edge++)
    {
      // Construct unsplit grid

      XField_LocalPatchConstructor<PhysD2,Triangle> xfld_construct(comm_local,connectivity,edge_list[edge],SpaceType::Continuous,&nodalview);
      XField_LocalPatch<PhysD2,Triangle> xfld_local(xfld_construct);

      // check that all local elements match the global elements
      XField_LocalGlobal_Equiv(xfld_local);
    }
  }
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_2Triangle_InteriorTrace_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField2D_2Triangle_X1_1Group xfld;

  // Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // connecting nodes
  std::array<int,2> edge{{1,2}};

  //Extract the local mesh
  XField_LocalPatch<PhysD2,Triangle> xfld_local(comm_local,connectivity,edge,SpaceType::Continuous,&nodalview);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 5 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld_local.DOF(0)(0),  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(0)(1),  0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(1)(0),  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(1)(1),  1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(2)(0),  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(2)(1),  0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(3)(0),  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(3)(1),  1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(4)(0),0.5 );  BOOST_CHECK_EQUAL( xfld_local.DOF(4)(1),0.5 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_local.getCellGroup(0);
  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup1 = xfld_local.getCellGroup(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 4 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 0 ); // This is a quirk of XField_LocalPatch

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup = xfld_local.getInteriorTraceGroup(0);

  BOOST_CHECK_EQUAL( xfldITraceGroup.nElem(), 4 );

  // boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nBoundaryTraceGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup(0);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 4 );

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 ); // Another quirk of the local patch

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldGBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 0 );

  //Checking the commonNodes_ interfaces
  std::vector<int> commonNodes = xfld_local.getLinearCommonNodes();
  std::vector<int> localCommonNodes = xfld_local.getLocalLinearCommonNodes();

  BOOST_CHECK_EQUAL( commonNodes.size(), 4 );
  BOOST_CHECK_EQUAL( localCommonNodes.size(), 4 );

  // Check that the first two nodes are the edge
  BOOST_CHECK_EQUAL( commonNodes[0], edge[0] );
  BOOST_CHECK_EQUAL( commonNodes[1], edge[1] );

  // Check that the edge is nodes 0 and 1
  BOOST_CHECK_EQUAL( localCommonNodes[0], 0 );
  BOOST_CHECK_EQUAL( localCommonNodes[1], 1 );

  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_2Triangle_BoundaryTrace_1_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  Real small_tol = 1e-12, close_tol = 1e-12;

  XField2D_2Triangle_X1_1Group xfld;

  // Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  // Build not to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // connecting nodes
  std::array<int,2> edge{{0,1}}; // bottom edge

  //Extract the local mesh
  XField_LocalPatch<PhysD2,Triangle> xfld_local(comm_local,connectivity,edge,SpaceType::Continuous,&nodalview);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 5 );

  //Check the node values
  SANS_CHECK_CLOSE( xfld_local.DOF(0)[0],   0., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(0)[1],  0., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(1)[0],   1., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(1)[1],  0., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(2)[0],   0., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(2)[1],  1., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(3)[0],   1., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(3)[1],  1., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(4)[0],  0.5, small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(4)[1],  0., small_tol, close_tol );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_local.getCellGroup<Triangle>(0);
  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup1 = xfld_local.getCellGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 1 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nInteriorTraceGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Line>(1).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup0 = xfld_local.getInteriorTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup1 = xfld_local.getInteriorTraceGroup<Line>(1);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 1 );

  // boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nBoundaryTraceGroups(), 2 );

  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(1).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup1 = xfld_local.getBoundaryTraceGroup<Line>(1);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 3 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.nElem(), 2 );

  //Checking the commonNodes_ interfaces
  std::vector<int> commonNodes = xfld_local.getLinearCommonNodes();
  std::vector<int> localCommonNodes = xfld_local.getLocalLinearCommonNodes();

  BOOST_CHECK_EQUAL( commonNodes.size(), 3 );
  BOOST_CHECK_EQUAL( localCommonNodes.size(), 3 );

  // Check that the first two nodes are the edge
  BOOST_CHECK_EQUAL( commonNodes[0], edge[0] );
  BOOST_CHECK_EQUAL( commonNodes[1], edge[1] );

  // Check that the edge is nodes 0 and 1
  BOOST_CHECK_EQUAL( localCommonNodes[0], 0 );
  BOOST_CHECK_EQUAL( localCommonNodes[1], 1 );

  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_2Triangle_BoundaryTrace_2_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField2D_2Triangle_X1_1Group xfld;

  // Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  // Build not to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // connecting nodes
  std::array<int,2> edge{{1,3}}; // right edge

  //Extract the local mesh
  XField_LocalPatch<PhysD2,Triangle> xfld_local(comm_local,connectivity,edge,SpaceType::Continuous,&nodalview);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 5 );

  //Check the node values, first three added by first elem
  Real small_tol = 1e-13, close_tol = 1e-13;
  SANS_CHECK_CLOSE( xfld_local.DOF(0)[0],  1., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(0)[1],  0., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(1)[0],  1., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(1)[1],  1., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(2)[0],  0., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(2)[1],  1., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(3)[0],  0., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(3)[1],  0., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(4)[0],  1., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(4)[1],  0.5, small_tol, close_tol );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_local.getCellGroup<Triangle>(0);
  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup1 = xfld_local.getCellGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 1 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nInteriorTraceGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Line>(1).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup0 = xfld_local.getInteriorTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup1 = xfld_local.getInteriorTraceGroup<Line>(1);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 1 );

  // boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nBoundaryTraceGroups(), 2 );

  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(1).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup1 = xfld_local.getBoundaryTraceGroup<Line>(1);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 3 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.nElem(), 2 );


  //Checking the commonNodes_ interfaces
  std::vector<int> commonNodes = xfld_local.getLinearCommonNodes();
  std::vector<int> localCommonNodes = xfld_local.getLocalLinearCommonNodes();

  BOOST_CHECK_EQUAL( commonNodes.size(), 3 );
  BOOST_CHECK_EQUAL( localCommonNodes.size(), 3 );

  // Check that the first two nodes are the edge
  BOOST_CHECK_EQUAL( commonNodes[0], edge[0] );
  BOOST_CHECK_EQUAL( commonNodes[1], edge[1] );

  // Check that the edge is nodes 0 and 1
  BOOST_CHECK_EQUAL( localCommonNodes[0], 0 );
  BOOST_CHECK_EQUAL( localCommonNodes[1], 1 );

  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_4Triangle_InteriorEdge0_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField2D_4Triangle_X1_1Group xfld;

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // connecting nodes
  std::array<int,2> edge{{1,2}};

  //Extract the local mesh
  XField_LocalPatch<PhysD2,Triangle> xfld_local(comm_local,connectivity,edge,SpaceType::Continuous,&nodalview);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 7 );

  //Check the node values
  Real small_tol = 1e-13, close_tol = 1e-13;
  SANS_CHECK_CLOSE( xfld_local.DOF(0)[0],  1., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(0)[1],  0., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(1)[0],  0., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(1)[1],  1., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(2)[0],  0., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(2)[1],  0., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(3)[0],  1., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(3)[1],  1., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(4)[0], -1., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(4)[1],  1., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(5)[0],  1., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(5)[1], -1., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(6)[0], 0.5, small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(6)[1], 0.5, small_tol, close_tol );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_local.getCellGroup<Triangle>(0);
  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup1 = xfld_local.getCellGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 4 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 2 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nInteriorTraceGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Line>(1).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup0 = xfld_local.getInteriorTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup1 = xfld_local.getInteriorTraceGroup<Line>(1);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 4 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 2 );

  // boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nBoundaryTraceGroups(), 2 );

  for (int i = 0; i < xfld_local.nBoundaryTraceGroups(); i++)
    BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(i).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup1 = xfld_local.getBoundaryTraceGroup<Line>(1);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.nElem(), 4 );


  //Checking the commonNodes_ interfaces
  std::vector<int> commonNodes = xfld_local.getLinearCommonNodes();
  std::vector<int> localCommonNodes = xfld_local.getLocalLinearCommonNodes();

  BOOST_CHECK_EQUAL( commonNodes.size(), 4 );
  BOOST_CHECK_EQUAL( localCommonNodes.size(), 4 );

  // Check that the first two nodes are the edge
  BOOST_CHECK_EQUAL( commonNodes[0], edge[0] );
  BOOST_CHECK_EQUAL( commonNodes[1], edge[1] );

  // Check that the edge is nodes 0 and 1
  BOOST_CHECK_EQUAL( localCommonNodes[0], 0 );
  BOOST_CHECK_EQUAL( localCommonNodes[1], 1 );

  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_4Triangle_BoundaryEdge_5_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField2D_4Triangle_X1_1Group xfld;

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // connecting nodes
  std::array<int,2> edge{{4,0}};

  //Extract the local mesh
  XField_LocalPatch<PhysD2,Triangle> xfld_local(comm_local,connectivity,edge,SpaceType::Continuous,&nodalview);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 7 );

  //Check the node values
  Real small_tol = 1e-13, close_tol = 1e-13;
  SANS_CHECK_CLOSE( xfld_local.DOF(0)[0],  -1., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(0)[1],  1., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(1)[0],   0., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(1)[1],  0., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(2)[0],   0., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(2)[1],  1., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(3)[0],   1., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(3)[1],  0., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(4)[0],   1., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(4)[1],  1., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(5)[0],   1., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(5)[1], -1., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(6)[0], -0.5, small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(6)[1], 0.5, small_tol, close_tol );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_local.getCellGroup<Triangle>(0);
  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup1 = xfld_local.getCellGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 3 );

  // interior-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nInteriorTraceGroups(), 3 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Line>(1).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Line>(2).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup0 = xfld_local.getInteriorTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup1 = xfld_local.getInteriorTraceGroup<Line>(1);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup2 = xfld_local.getInteriorTraceGroup<Line>(2);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.nElem(), 2 );

  // boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nBoundaryTraceGroups(), 2 );

  for (int i = 0; i < xfld_local.nBoundaryTraceGroups(); i++)
    BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(i).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup1 = xfld_local.getBoundaryTraceGroup<Line>(1);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 3 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.nElem(), 4 );

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );
  BOOST_CHECK_EQUAL( xfld_local.getGhostBoundaryTraceGroup<Line>(0).nElem(), 0 );


  //Checking the commonNodes_ interfaces
  std::vector<int> commonNodes = xfld_local.getLinearCommonNodes();
  std::vector<int> localCommonNodes = xfld_local.getLocalLinearCommonNodes();

  BOOST_CHECK_EQUAL( commonNodes.size(), 3 );
  BOOST_CHECK_EQUAL( localCommonNodes.size(), 3 );

  // Check that the first two nodes are the edge
  BOOST_CHECK_EQUAL( commonNodes[0], edge[0] );
  BOOST_CHECK_EQUAL( commonNodes[1], edge[1] );

  // Check that the edge is nodes 0 and 1
  BOOST_CHECK_EQUAL( localCommonNodes[0], 0 );
  BOOST_CHECK_EQUAL( localCommonNodes[1], 1 );

  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Box_Triangle_InteriorEdge0_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  int ii = 3, jj = 3;
  // make it ([0,3],[0,3]) so the node locations easier
  XField2D_Box_Triangle_Lagrange_X1 xfld( comm_local, ii, jj, 0, 3, 0, 3 );

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  typedef XField_LocalPatch<PhysD2,Triangle> XField_Local;

  // check that non matching edge throws exceptions
  std::array<int,2> edge{{10,5}};
  BOOST_CHECK_THROW( XField_Local xfld_local(comm_local,connectivity,edge,SpaceType::Continuous,&nodalview), AssertionException );

  // check that passing two identical nodes throws exceptions
  edge[1] = edge[0];
  BOOST_CHECK_THROW( XField_Local xfld_local(comm_local,connectivity,edge,SpaceType::Continuous,&nodalview), AssertionException );

  // check that not passing a pointer to a nodalview throws exceptions
  edge[0] = 9; edge[1] = 6;
  BOOST_CHECK_THROW( XField_Local xfld_local(comm_local,connectivity,edge,SpaceType::Continuous,nullptr), AssertionException );

  XField_Local xfld_local(comm_local,connectivity,edge,SpaceType::Continuous,&nodalview);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 15 );

  //Check the node values
  Real small_tol = 1e-13, close_tol = 1e-13;
  SANS_CHECK_CLOSE( xfld_local.DOF(0)[0],   1., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(0)[1],   2., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(1)[0],   2., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(1)[1],   1., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(2)[0],   1., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(2)[1],   1., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(3)[0],   2., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(3)[1],   2., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(4)[0],   0., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(4)[1],   1., small_tol, close_tol );

  SANS_CHECK_CLOSE( xfld_local.DOF(5)[0],   1., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(5)[1],   0., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(6)[0],   2., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(6)[1],   0., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(7)[0],   0., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(7)[1],   2., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(8)[0],   3., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(8)[1],   0., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(9)[0],   3., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(9)[1],   1., small_tol, close_tol );

  SANS_CHECK_CLOSE( xfld_local.DOF(10)[0],  0., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(10)[1],   3., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(11)[0],  1., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(11)[1],   3., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(12)[0],  3., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(12)[1],   2., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(13)[0],  2., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(13)[1],   3., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(14)[0], 1.5, small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(14)[1], 1.5, small_tol, close_tol );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_local.getCellGroup<Triangle>(0);
  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup1 = xfld_local.getCellGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 4 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 14 );

  // interior-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nInteriorTraceGroups(), 3 );
  for (int i = 0; i < 3; i++)
    BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Line>(i).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup0 = xfld_local.getInteriorTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup1 = xfld_local.getInteriorTraceGroup<Line>(1);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup2 = xfld_local.getInteriorTraceGroup<Line>(2);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(),  4 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 14 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.nElem(),  4 );

  // boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nBoundaryTraceGroups(), 4 );

  for (int i = 0; i < xfld_local.nBoundaryTraceGroups(); i++)
    BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(i).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup1 = xfld_local.getBoundaryTraceGroup<Line>(1);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup2 = xfld_local.getBoundaryTraceGroup<Line>(2);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup3 = xfld_local.getBoundaryTraceGroup<Line>(3);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.nElem(), 2 );

  // ghost boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );

  for (int i = 0; i < xfld_local.nGhostBoundaryTraceGroups(); i++)
    BOOST_REQUIRE( xfld_local.getGhostBoundaryTraceGroup<Line>(i).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldGBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 2 );


  //Checking the commonNodes_ interfaces
  std::vector<int> commonNodes = xfld_local.getLinearCommonNodes();
  std::vector<int> localCommonNodes = xfld_local.getLocalLinearCommonNodes();

  BOOST_CHECK_EQUAL( commonNodes.size(), 4 );
  BOOST_CHECK_EQUAL( localCommonNodes.size(), 4 );

  // Check that the first two nodes are the edge
  BOOST_CHECK_EQUAL( commonNodes[0], edge[0] );
  BOOST_CHECK_EQUAL( commonNodes[1], edge[1] );

  // Check that the edge is nodes 0 and 1
  BOOST_CHECK_EQUAL( localCommonNodes[0], 0 );
  BOOST_CHECK_EQUAL( localCommonNodes[1], 1 );

  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_WholeGrid_Box_Triangle_test )
{
  typedef typename XField<PhysD2, TopoD2>::template FieldCellGroupType<Triangle> XFieldCellGroupType;

  typedef Field_CG_Cell< PhysD2, TopoD2, Real > QFieldType;
  typedef typename QFieldType::template FieldCellGroupType<Triangle> QFieldCellGroupType;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  int size = 3*3*world.size();
  int ii = sqrt(size), jj = ii;
  // make it ([0,3],[0,3]) so the node locations easier
  XField2D_Box_Triangle_Lagrange_X1 xfld( world, ii, jj, 0, 3, 0, 3 );

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // Field needed to get nodal rank
  QFieldType qfld(xfld, 1, BasisFunctionCategory_Lagrange);

  // Construct list of all edges in grid

  const int nEdge = Triangle::NEdge; // Number of different edges

  typedef typename std::set<std::array<int,Line::NNode>> EdgeList;
  EdgeList edge_list;

  const int (*EdgeNodes)[Line::NNode] = ElementEdges<Triangle>::EdgeNodes;

  for (int group = 0; group < xfld.nCellGroups(); group++)
  {
    const XFieldCellGroupType& xfldCellGroup = xfld.template getCellGroup<Triangle>(group);
    const QFieldCellGroupType& qfldCellGroup = qfld.template getCellGroup<Triangle>(group);

    for ( int elem = 0; elem < xfldCellGroup.nElem(); elem++ )
    {
      std::array<int,Triangle::NNode> xnodeMap;
      std::array<int,Triangle::NNode> qnodeMap;

      xfldCellGroup.associativity(elem).getNodeGlobalMapping( xnodeMap.data(), xnodeMap.size() );
      qfldCellGroup.associativity(elem).getNodeGlobalMapping( qnodeMap.data(), qnodeMap.size() );

      for ( int edge = 0; edge < nEdge; edge++ )
      {
        // loop over the edges
        std::array<int,Line::NNode> xEdge;
        xEdge[0] = xnodeMap[ EdgeNodes[edge][0] ];
        xEdge[1] = xnodeMap[ EdgeNodes[edge][1] ];

        std::array<int,Line::NNode> qEdge;
        qEdge[0] = qnodeMap[ EdgeNodes[edge][0] ];
        qEdge[1] = qnodeMap[ EdgeNodes[edge][1] ];

        if (xfld.local2nativeDOFmap(xEdge[0]) > xfld.local2nativeDOFmap(xEdge[1]))
        {
          std::swap(xEdge[0], xEdge[1]);
          std::swap(qEdge[0], qEdge[1]);
        }

        // only consider edges where the primary node is possessed
        if (qEdge[0] >= qfld.nDOFpossessed()) continue;

        edge_list.insert(xEdge);
      }
    }

    for ( const std::array<int,Line::NNode>& edge : edge_list )
    {
      // Construct split grid
      XField_LocalPatch<PhysD2,Triangle> xfld_local(comm_local,connectivity,edge,SpaceType::Continuous,&nodalview);

      // check that all local elements match the global elements
      XField_LocalGlobal_Equiv(xfld_local);
    }
  }
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
