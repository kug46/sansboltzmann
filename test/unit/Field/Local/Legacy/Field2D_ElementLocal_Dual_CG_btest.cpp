// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Field2D_ElementLocal_CG_btest
//

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_LocalRefine_Triangle_X1.h"

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunctionArea_Triangle.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_CG_InteriorTrace.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/Local/Field_Local.h"
#include "Field/Field_NodalView.h"

#include "Field/XField_ElementLocal.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

//Explicitly instantiate classes to get proper coverage information
namespace SANS
{
template class Field_CG_Cell< PhysD2, TopoD2, Real >;
template class Field_CG_InteriorTrace< PhysD2, TopoD2, Real >;
template class Field_CG_BoundaryTrace< PhysD2, TopoD2, Real >;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( Field2D_ElementLocal_Dual_CG_test_suite )

// A helper function to construct a Solver interface and test all the edges on a grid
void localEdgeExtractAndTransfer(const XField<PhysD2, TopoD2>& xfld, const BasisFunctionCategory basis_category,
                             const int order, const Real small_tol, const Real close_tol, const bool isSplit )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef Field_CG_BoundaryTrace< PhysD2, TopoD2, ArrayQ > QField2D_CG_BTrace;

  typedef DLA::VectorS<TopoD2::D,Real> RefCoordType;
  typedef DLA::VectorS<TopoD1::D,Real> TraceRefCoordType;

  typedef QField2D_CG_Area::FieldCellGroupType<Triangle> FieldCellGroupType;
  typedef FieldCellGroupType::ElementType<> ElementFieldClass;
  typedef typename XField<PhysD2, TopoD2>::template FieldCellGroupType<Triangle> XFieldCellGroupType;
  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

  typedef QField2D_CG_BTrace::FieldTraceGroupType<Line> FieldTraceGroupType;
  typedef FieldTraceGroupType::ElementType<> ElementTraceFieldClass;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  QField2D_CG_Area qfld(xfld, order, basis_category);
  QField2D_CG_BTrace lgfld(xfld, order, basis_category);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {10*pow(-1,n%2)+n, 10*pow(-1,n%2)+n};

  //Set some non-zero solution
  for (int n = 0; n < lgfld.nDOF(); n++)
    lgfld.DOF(n) = {1*(-n%3)-1, (n%3)+1};

  ///////////////////////
  // LOOP OVER CELL GROUPS ->
  ///////////////////////
  const int (*EdgeNodes)[Line::NNode] = ElementEdges<Triangle>::EdgeNodes;
  for (int group = 0; group  < xfld.nCellGroups(); group++)
  {
    // edge_list is cleared with each loop over the groups
    XFieldCellGroupType xfldCellGroup = xfld.template getCellGroup<Triangle>(group);
    // const int nElem = xfldCellGroup.nElem();

    ElementXFieldClass xfldElem(xfldCellGroup.basis() );

    /////////////////////////////////////////////
    // ELEMENT DUAL LOCAL MESH CONSTRUCT
    /////////////////////////////////////////////

    const XField_CellToTrace<PhysD2,TopoD2> xfld_connectivity(xfld);
    const Field_NodalView nodalView(xfld, {group});

    // typedef typename Field_NodalView::IndexVector IndexVector;

    for ( int elem = 0; elem < xfldCellGroup.nElem(); elem++ )
    {
      /////////////////////////////////////////////
      // ELEMENT DUAL LOCAL MESH CONSTRUCT
      /////////////////////////////////////////////

      // Unsplit grid
      XField_ElementLocal<PhysD2,TopoD2> xfld_local_unsplit( comm_local, xfld_connectivity, nodalView, group, elem, Neighborhood::Dual );

      Quadrature<TopoD2, Triangle> cellQuadrature(order+2);
      const int nCellquad = cellQuadrature.nQuadrature();

      Quadrature<TopoD1, Line> traceQuadrature(order+2);
      const int nTracequad = traceQuadrature.nQuadrature();

      if (!isSplit)
      {
        /////////////////////////////////////////////
        // LOCAL SOLUTION TRANSFER
        /////////////////////////////////////////////

        Field_Local<QField2D_CG_Area> qfld_local(xfld_local_unsplit, qfld, order, basis_category);
        Field_Local<QField2D_CG_BTrace> lgfld_local(xfld_local_unsplit, lgfld, order, basis_category );

        //-------------CHECK LOCAL FIELD----------------
        BOOST_CHECK_EQUAL( &qfld_local.getXField(), &xfld_local_unsplit );
        BOOST_CHECK_EQUAL( &lgfld_local.getXField(), &xfld_local_unsplit );

        /////////////////////////////////////////////
        // EDGE LOCAL UNSPLIT CHECK
        /////////////////////////////////////////////

        RefCoordType RefCoord;  // reference-element coordinates
        ArrayQ qeval_main, qeval_local;

        for (int cellGroup = 0; cellGroup < qfld_local.nCellGroups(); cellGroup++)
          for (int elem = 0; elem < xfld_local_unsplit.getCellGroup<Triangle>(cellGroup).nElem(); elem++)
          {
            std::pair<int,int> globGroupElem = xfld_local_unsplit.getGlobalCellMap(std::make_pair(cellGroup,elem));

            const FieldCellGroupType& global_cellgrp = qfld.getCellGroup<Triangle>(globGroupElem.first);
            ElementFieldClass fldElem_global( global_cellgrp.basis() );
            global_cellgrp.getElement(fldElem_global, globGroupElem.second);

            const FieldCellGroupType& local_cellgrp = qfld_local.getCellGroup<Triangle>(cellGroup);
            ElementFieldClass fldElem_local( global_cellgrp.basis() );
            local_cellgrp.getElement(fldElem_local, elem);

            //Check if the solutions are equal at a few cellQuadrature points
            for (int iquad = 0; iquad < nCellquad; iquad++)
            {
              cellQuadrature.coordinates( iquad, RefCoord );

              fldElem_local.eval( RefCoord, qeval_local);

              fldElem_global.eval(RefCoord, qeval_main);

              for (int i=0; i<2; i++)
                SANS_CHECK_CLOSE(qeval_local[i], qeval_main[i], small_tol, close_tol);
            }

            if ( !(order >= 3 && basis_category == BasisFunctionCategory_Hierarchical ) )
            {
              // Check if the local dofs are identical
              for (int dof = 0; dof < fldElem_global.nDOF(); dof++)
                for (int i = 0; i < 2; i++)
                  SANS_CHECK_CLOSE( fldElem_global.DOF(dof)[i], fldElem_local.DOF(dof)[i], small_tol, close_tol );
            }

          }

        TraceRefCoordType TRefCoord; // reference-element coordinates in trace
        for (int traceGroup = 0; traceGroup < lgfld_local.nBoundaryTraceGroups(); traceGroup++)
        {
          const int xFieldBGroupGlobal = lgfld_local.getGlobalBoundaryTraceGroupMap( traceGroup ); // matching xfield_local bgroups

          // to get global xfield group
          std::pair<int,int> globGroupTrace = xfld_local_unsplit.getGlobalBoundaryTraceMap( std::make_pair(xFieldBGroupGlobal,0) );
          const int lgFieldBGroupGlobal = lgfld.getLocalBoundaryTraceGroupMap( globGroupTrace.first ); // The lgfield group matching xfield

          const FieldTraceGroupType& local_tracegrp  = lgfld_local.template getBoundaryTraceGroup<Line>(traceGroup);
          const FieldTraceGroupType& global_tracegrp = lgfld.template getBoundaryTraceGroup<Line>(lgFieldBGroupGlobal);

          for (int trace = 0; trace < local_tracegrp.nElem(); trace++)
          {
            ElementTraceFieldClass fldElem_global( global_tracegrp.basis() );
            ElementTraceFieldClass fldElem_local (  local_tracegrp.basis() );

            global_tracegrp.getElement( fldElem_global, globGroupTrace.second );
            local_tracegrp. getElement( fldElem_local,  trace );

            for (int iquad = 0; iquad < nTracequad; iquad++)
            {
              traceQuadrature.coordinates( iquad, TRefCoord );

              fldElem_local.eval( TRefCoord, qeval_local);
              fldElem_global.eval( TRefCoord, qeval_main);

              for (int i = 0; i < 2; i++)
                SANS_CHECK_CLOSE( qeval_local[i], qeval_main[i], small_tol, close_tol );

            // Check if the local dofs are identical -- can do this because lgfld is always a DG basis.
            for (int dof = 0; dof < fldElem_global.nDOF(); dof++)
              for (int i = 0; i < 2; i++)
                SANS_CHECK_CLOSE( fldElem_global.DOF(dof)[i], fldElem_local.DOF(dof)[i], small_tol, close_tol );
            }
          }
        }
      }
      else if (isSplit)
      {
        std::array<int,Triangle::NNode> cellNodeMap;
        std::array<int,Line::NNode> edgeNodeMap;

        for (int edge = 0; edge < Triangle::NEdge; edge++)
        {
          /////////////////////////////////////////////
          // EDGE LOCAL SPLIT CHECK FOR CELLGROUP 0
          /////////////////////////////////////////////

          XField_ElementLocal<PhysD2,TopoD2> xfld_local(xfld_local_unsplit,edge);

          /////////////////////////////////////////////
          // LOCAL SOLUTION TRANSFER
          /////////////////////////////////////////////

          Field_Local<QField2D_CG_Area> qfld_local(xfld_local, qfld, order, basis_category);
          Field_Local<QField2D_CG_BTrace> lgfld_local(xfld_local, lgfld, order, basis_category );

          //-------------CHECK LOCAL FIELD----------------
          BOOST_CHECK_EQUAL( &qfld_local.getXField(), &xfld_local );
          BOOST_CHECK_EQUAL( &lgfld_local.getXField(), &xfld_local );

          RefCoordType RefCoord_main, RefCoord_sub;  // reference-element coordinates
          ArrayQ qeval_main0, qeval_main1, qeval_local0, qeval_local1;

          // Loop over half the number of cells, because evaluating two at a time
          std::pair<int,int> globGroupElem = xfld_local.getGlobalCellMap(std::make_pair(0,0));

          // Extract the global element
          const FieldCellGroupType& global_cellgrp = qfld.getCellGroup<Triangle>(globGroupElem.first);
          ElementFieldClass fldElem_global( global_cellgrp.basis() );
          global_cellgrp.getElement(fldElem_global, globGroupElem.second);

          // Extract the two local elements
          const FieldCellGroupType& local_cellgrp = qfld_local.getCellGroup<Triangle>(0);
          ElementFieldClass fldElem_local0( global_cellgrp.basis() ), fldElem_local1( global_cellgrp.basis() );
          local_cellgrp.getElement(fldElem_local0, 0);
          local_cellgrp.getElement(fldElem_local1, 1);

          // Figure out the canonical trace of the global element that is getting split
          // Assumes that the xfield and field cellgroup and element numbers match

          const XFieldCellGroupType& xfld_global_cellgrp = xfld.getCellGroup<Triangle>(globGroupElem.first);
          xfld_global_cellgrp.associativity(globGroupElem.second)
              .getNodeGlobalMapping( cellNodeMap.data(), cellNodeMap.size() );

          edgeNodeMap.at(0) = cellNodeMap[EdgeNodes[edge][0]];
          edgeNodeMap.at(1) = cellNodeMap[EdgeNodes[edge][1]];

          CanonicalTraceToCell canonical = TraceToCellRefCoord<Line, TopoD2, Triangle>
            ::getCanonicalTrace( edgeNodeMap.data(), edgeNodeMap.size(), cellNodeMap.data(), cellNodeMap.size() );

          const int localEdge0 = canonical.orientation > 0 ? 0 : 1;
          const int localEdge1 = canonical.orientation > 0 ? 1 : 0;

          //Check if the solutions are equal at a few cellQuadrature points
          for (int iquad = 0; iquad < nCellquad; iquad++)
          {
            cellQuadrature.coordinates( iquad, RefCoord_sub );

            // coordinates in the local elements
            fldElem_local0.eval( RefCoord_sub, qeval_local0);
            fldElem_local1.eval( RefCoord_sub, qeval_local1);

            BasisFunction_RefElement_Split<TopoD2,Triangle>::
              transform( RefCoord_sub, ElementSplitType::Edge, canonical.trace, localEdge0, RefCoord_main);

            fldElem_global.eval(RefCoord_main, qeval_main0);

            BasisFunction_RefElement_Split<TopoD2,Triangle>::
              transform( RefCoord_sub, ElementSplitType::Edge, canonical.trace, localEdge1, RefCoord_main);

            fldElem_global.eval(RefCoord_main, qeval_main1);

            for (int i=0; i<2; i++)
            {
              SANS_CHECK_CLOSE(qeval_local0[i], qeval_main0[i], small_tol, close_tol);
              SANS_CHECK_CLOSE(qeval_local1[i], qeval_main1[i], small_tol, close_tol);
            }
          }

          TraceRefCoordType TRefCoord_main, TRefCoord_sub; // reference-element coordinates in trace
          for (int traceGroup = 0; traceGroup < lgfld_local.nBoundaryTraceGroups(); traceGroup++)
          {
            const int xFieldBGroupGlobal = lgfld_local.getGlobalBoundaryTraceGroupMap( traceGroup ); // matching xfield_local bgroups

            // to get global xfield group
            std::pair<int,int> globGroupTrace = xfld_local.getGlobalBoundaryTraceMap( std::make_pair(xFieldBGroupGlobal,0) );
            const int lgFieldBGroupGlobal = lgfld.getLocalBoundaryTraceGroupMap( globGroupTrace.first ); // The lgfield group matching xfield

            const FieldTraceGroupType& local_tracegrp  = lgfld_local.template getBoundaryTraceGroup<Line>(traceGroup);
            const FieldTraceGroupType& global_tracegrp = lgfld.template getBoundaryTraceGroup<Line>(lgFieldBGroupGlobal);

            if ( local_tracegrp.nElem() == 2 ) // SPLIT TRACE
            {
              ElementTraceFieldClass fldElem_global( global_tracegrp.basis() );
              ElementTraceFieldClass fldElem_local0(  local_tracegrp.basis() ), fldElem_local1(  local_tracegrp.basis() );

              global_tracegrp.getElement( fldElem_global, globGroupTrace.second );
              local_tracegrp. getElement( fldElem_local0, 0 );
              local_tracegrp. getElement( fldElem_local1, 1 );

              for (int iquad = 0; iquad < nTracequad; iquad++)
              {
                traceQuadrature.coordinates( iquad, TRefCoord_sub );

                // coordinates in the local trace
                fldElem_local0.eval( TRefCoord_sub, qeval_local0 );
                fldElem_local1.eval( TRefCoord_sub, qeval_local1 );

                BasisFunction_RefElement_Split<TopoD1,Line>::
                  transform( TRefCoord_sub, ElementSplitType::Isotropic, -1, 0, TRefCoord_main);

                fldElem_global.eval(TRefCoord_main, qeval_main0);

                BasisFunction_RefElement_Split<TopoD1,Line>::
                  transform( TRefCoord_sub, ElementSplitType::Isotropic, -1, 1, TRefCoord_main);

                fldElem_global.eval(TRefCoord_main, qeval_main1);

                for (int i = 0; i < 2; i++)
                {
                  SANS_CHECK_CLOSE(qeval_local0[i], qeval_main0[i], small_tol, close_tol);
                  SANS_CHECK_CLOSE(qeval_local1[i], qeval_main1[i], small_tol, close_tol);
                }
              }
            }
            else if ( local_tracegrp.nElem() == 1 ) // UNSPLIT TRACE
            {
              ElementTraceFieldClass fldElem_global( global_tracegrp.basis() );
              ElementTraceFieldClass fldElem_local (  local_tracegrp.basis() );

              global_tracegrp.getElement( fldElem_global, globGroupTrace.second );
              local_tracegrp. getElement( fldElem_local,  0 );

              for (int iquad = 0; iquad < nTracequad; iquad++)
              {
                traceQuadrature.coordinates( iquad, TRefCoord_main );

                fldElem_local.eval( TRefCoord_main, qeval_local0);
                fldElem_global.eval( TRefCoord_main, qeval_main0);

                for (int i = 0; i < 2; i++)
                  SANS_CHECK_CLOSE( qeval_local0[i], qeval_main0[i], small_tol, close_tol );


              // Check if the local dofs are identical -- can do this because lgfld is always a DG basis.
              for (int dof = 0; dof < fldElem_global.nDOF(); dof++)
                for (int i = 0; i < 2; i++)
                  SANS_CHECK_CLOSE( fldElem_global.DOF(dof)[i], fldElem_local.DOF(dof)[i], small_tol, close_tol );
              }
            }
            else
              BOOST_CHECK_EQUAL( 1, 0 ); // Shouldn't be possible to get here, btraces should have 1 or 2 elements
          }


          if (xfld_local.nCellGroups() == 2)
          {
            /////////////////////////////////////////////
            // EDGE LOCAL UNSPLIT CHECK FOR CELLGROUP 1
            /////////////////////////////////////////////

            RefCoordType RefCoord;  // reference-element coordinates
            int initialSize = xfld_local.getCellGroup<Triangle>(1).nElem();
            for (int elem1 = 0; elem1 < initialSize; elem1++)
            {
              std::pair<int,int> globGroupElem = xfld_local.getGlobalCellMap(std::make_pair(1,elem1));

              const FieldCellGroupType& global_cellgrp = qfld.getCellGroup<Triangle>(globGroupElem.first);

              ElementFieldClass fldElem_global( global_cellgrp.basis() );
              global_cellgrp.getElement(fldElem_global, globGroupElem.second);

              const FieldCellGroupType& local_cellgrp = qfld_local.getCellGroup<Triangle>(1);

              local_cellgrp.getElement(fldElem_local0, elem1);

              // Figure out if the element got split
              ElementSplitInfo elem_info = xfld_local.getCellSplitInfo( std::pair<int,int>(1,elem1) );

              if (elem_info.split_flag == ElementSplitFlag::Unsplit)
              {
                //Check if the solutions are equal at a few cellQuadrature points
                for (int iquad = 0; iquad < nCellquad; iquad++)
                {
                  cellQuadrature.coordinates( iquad, RefCoord );

                  fldElem_local0.eval( RefCoord, qeval_local0);

                  fldElem_global.eval( RefCoord, qeval_main0);

                  for (int i=0; i<2; i++)
                    SANS_CHECK_CLOSE(qeval_local0[i], qeval_main0[i], small_tol, close_tol);
                }

                if ( !(order >= 3 && basis_category == BasisFunctionCategory_Hierarchical ) )
                {
                  // Check if the local dofs are identical
                  for (int dof = 0; dof < fldElem_global.nDOF(); dof++)
                    for (int i = 0; i < 2; i++)
                      SANS_CHECK_CLOSE( fldElem_global.DOF(dof)[i], fldElem_local0.DOF(dof)[i], small_tol, close_tol );
                }
              }
              else if ( elem_info.split_flag == ElementSplitFlag::Split)
              {
                // Only one can be split in 2D, we can just look for the last
                local_cellgrp.getElement(fldElem_local1, local_cellgrp.nElem()-1); // get the last, cheating given 2D

                // Modify the domain of the loop, the last element doesn't need to be done twice
                initialSize--;

                // It was split, inversely than the main element
                //Check if the solutions are equal at a few cellQuadrature points
                for (int iquad = 0; iquad < nCellquad; iquad++)
                {
                  cellQuadrature.coordinates( iquad, RefCoord_sub );

                  // coordinates in the local elements
                  fldElem_local0.eval( RefCoord_sub, qeval_local0);
                  fldElem_local1.eval( RefCoord_sub, qeval_local1);

                  BasisFunction_RefElement_Split<TopoD2,Triangle>::
                    transform( RefCoord_sub, ElementSplitType::Edge, canonical.trace, localEdge1, RefCoord_main);

                  fldElem_global.eval(RefCoord_main, qeval_main0);

                  BasisFunction_RefElement_Split<TopoD2,Triangle>::
                    transform( RefCoord_sub, ElementSplitType::Edge, canonical.trace, localEdge0, RefCoord_main);

                  fldElem_global.eval(RefCoord_main, qeval_main1);

                  for (int i=0; i<2; i++)
                  {
                    SANS_CHECK_CLOSE(qeval_local0[i], qeval_main0[i], small_tol, close_tol);
                    SANS_CHECK_CLOSE(qeval_local1[i], qeval_main1[i], small_tol, close_tol);
                  }
                }


              }

            }
          }

        }
      }
//      return;
    }
  }

}


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementLocalSolve2D_Galerkin_AD_4Triangle_X1_1Group_Hierarchical_P1 )
{
  XField2D_2Triangle_X1_1Group xfld;

  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = false;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementLocalSolve2D_Galerkin_AD_4Triangle_X1_1Group_Hierarchical_Split_P1 )
{
  XField2D_2Triangle_X1_1Group xfld;

  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = true;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementLocalSolve2D_Galerkin_AD_4Triangle_X1_1Group_Hierarchical_P3 )
{
  XField2D_4Triangle_X1_1Group xfld;

  int order = 3;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = false;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementLocalSolve2D_Galerkin_AD_4Triangle_X1_1Group_Hierarchical_Split_P3 )
{
  XField2D_4Triangle_X1_1Group xfld;

  int order = 3;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = true;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementLocalSolve2D_Galerkin_AD_1Box_Triangle_Hierarchical_P1 )
{
  int ii = 4;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = false;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementLocalSolve2D_Galerkin_AD_1Box_Triangle_Hierarchical_Split_P1 )
{
  int ii = 4;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 1;
  const Real small_tol = 5e-10, close_tol = 5e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = true;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementLocalSolve2D_Galerkin_AD_1Box_Triangle_Hierarchical_P2 )
{
  int ii = 4;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 2;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = false;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementLocalSolve2D_Galerkin_AD_1Box_Triangle_Hierarchical_Split_P2 )
{
  int ii = 4;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 2;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = true;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementLocalSolve2D_Galerkin_AD_1Box_Triangle_Hierarchical_P3 )
{
  int ii = 4;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 3;
  const Real small_tol = 1e-9, close_tol = 1e-9;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = false;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementLocalSolve2D_Galerkin_AD_1Box_Triangle_Hierarchical_Split_P3 )
{
  int ii = 4;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 3;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = true;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementLocalSolve2D_Galerkin_AD_1Box_Triangle_Lagrange_P1 )
{
  int ii = 4;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = false;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementLocalSolve2D_Galerkin_AD_1Box_Triangle_Lagrange_Split_P1 )
{
  int ii = 4;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 1;
  const Real small_tol = 5e-10, close_tol = 1e-9;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = true;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementLocalSolve2D_Galerkin_AD_1Box_Triangle_Lagrange_P2 )
{
  int ii = 4;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj,0,ii,0,jj);

  int order = 2;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = false;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementLocalSolve2D_Galerkin_AD_1Box_Triangle_Lagrange_Split_P2 )
{
  int ii = 4;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 2;
  const Real small_tol = 5e-10, close_tol = 5e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = true;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementLocalSolve2D_Galerkin_AD_1Box_Triangle_Lagrange_P3 )
{
  int ii = 4;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 3;
  const Real small_tol = 5e-10, close_tol = 5e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = false;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ElementLocalSolve2D_Galerkin_AD_1Box_Triangle_Lagrange_Split_P3 )
{
  int ii = 4;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 3;
  const Real small_tol = 5e-10, close_tol = 5e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = true;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
