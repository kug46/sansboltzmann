// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Field2D_Local_DG_btest
//

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunctionArea_Triangle.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_InteriorTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/Local/Field_Local.h"

#include "Field/XFieldArea_Local.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

//Explicitly instantiate classes to get proper coverage information
namespace SANS
{
template class Field_DG_Cell< PhysD2, TopoD2, Real >;
template class Field_DG_InteriorTrace< PhysD2, TopoD2, Real >;
template class Field_DG_BoundaryTrace< PhysD2, TopoD2, Real >;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( Field2D_Local_DG_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field2D_DG_Local_4Triangle_X1_ExtractInteriorCell_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_DG_Area;

  typedef std::array<int,3> Int3;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField2D_4Triangle_X1_1Group xfld;

  int order = 1;
  QField2D_DG_Area qfld(xfld, order, BasisFunctionCategory_Legendre);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh for the interior triangle
  XField_Local<PhysD2,TopoD2> xfld_local(comm_local,connectivity,0,0);

  Field_Local<QField2D_DG_Area> qfld_local(xfld_local, qfld, order, BasisFunctionCategory_Legendre);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  qfld_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_local.getXField(), &xfld_local );

  BOOST_CHECK_EQUAL( qfld_local.nDOF(), 12 );

  for (int n = 0; n < qfld_local.nDOF(); n++)
    for (int i=0; i<2; i++)
      BOOST_CHECK_EQUAL(qfld_local.DOF(n)[i], qfld.DOF(n)[i]);


  BOOST_CHECK_EQUAL( qfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( qfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( qfld_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  const Field<PhysD2,TopoD2,ArrayQ>::FieldCellGroupType<Triangle>& qfldCellGroup0 = qfld_local.getCellGroup<Triangle>(0);
  const Field<PhysD2,TopoD2,ArrayQ>::FieldCellGroupType<Triangle>& qfldCellGroup1 = qfld_local.getCellGroup<Triangle>(1);

  int DOFMap[6];

  //Node DOFs
  BOOST_CHECK_EQUAL( qfldCellGroup0.associativity(0).nNode(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(0).nNode(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(1).nNode(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(2).nNode(), 0 );

  //Edge DOFs
  BOOST_CHECK_EQUAL( qfldCellGroup0.associativity(0).nEdge(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(0).nEdge(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(1).nEdge(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(2).nEdge(), 0 );

  //Cell DOFs
  BOOST_CHECK_EQUAL( qfldCellGroup0.associativity(0).nCell(), 3 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(0).nCell(), 3 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(1).nCell(), 3 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(2).nCell(), 3 );

  qfldCellGroup0.associativity(0).getCellGlobalMapping( DOFMap, 3 );
  BOOST_CHECK_EQUAL( DOFMap[0], 0 );
  BOOST_CHECK_EQUAL( DOFMap[1], 1 );
  BOOST_CHECK_EQUAL( DOFMap[2], 2 );

  qfldCellGroup1.associativity(0).getCellGlobalMapping( DOFMap, 3 );
  BOOST_CHECK_EQUAL( DOFMap[0], 3 );
  BOOST_CHECK_EQUAL( DOFMap[1], 4 );
  BOOST_CHECK_EQUAL( DOFMap[2], 5 );

  qfldCellGroup1.associativity(1).getCellGlobalMapping( DOFMap, 3 );
  BOOST_CHECK_EQUAL( DOFMap[0], 6 );
  BOOST_CHECK_EQUAL( DOFMap[1], 7 );
  BOOST_CHECK_EQUAL( DOFMap[2], 8 );

  qfldCellGroup1.associativity(2).getCellGlobalMapping( DOFMap, 3 );
  BOOST_CHECK_EQUAL( DOFMap[0], 9 );
  BOOST_CHECK_EQUAL( DOFMap[1], 10 );
  BOOST_CHECK_EQUAL( DOFMap[2], 11 );

  Int3 edgeSign;

  edgeSign = qfldCellGroup0.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  for (int cell=0; cell<3; cell++)
  {
    edgeSign = qfldCellGroup1.associativity(cell).edgeSign();
    BOOST_CHECK_EQUAL( edgeSign[0], +1 );
    BOOST_CHECK_EQUAL( edgeSign[1], +1 );
    BOOST_CHECK_EQUAL( edgeSign[2], +1 );
  }

  BOOST_CHECK_EQUAL( qfld_local.nInteriorTraceGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_local.nBoundaryTraceGroups(), 0 );

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field2D_DG_Local_4Triangle_X1_ExtractInteriorCell_P2 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_DG_Area;

  typedef std::array<int,3> Int3;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField2D_4Triangle_X1_1Group xfld;

  int order = 2;
  QField2D_DG_Area qfld(xfld, order, BasisFunctionCategory_Legendre);

  // interface solution
//  Field_DG_InteriorTrace<PhysD2, TopoD2, ArrayQ> uIfld(xfld, order, BasisFunctionCategory_Legendre);
//  uIfld.dump(3,std::cout);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh for the interior triangle
  XField_Local<PhysD2,TopoD2> xfld_local(comm_local,connectivity,0,0);

  Field_Local<QField2D_DG_Area> qfld_local(xfld_local, qfld, order, BasisFunctionCategory_Legendre);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  qfld_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_local.getXField(), &xfld_local );

  BOOST_CHECK_EQUAL( qfld_local.nDOF(), 24 );

  for (int n = 0; n < qfld_local.nDOF(); n++)
    for (int i=0; i<2; i++)
      BOOST_CHECK_EQUAL(qfld_local.DOF(n)[i], qfld.DOF(n)[i]);


  BOOST_CHECK_EQUAL( qfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( qfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( qfld_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  const Field<PhysD2,TopoD2,ArrayQ>::FieldCellGroupType<Triangle>& qfldCellGroup0 = qfld_local.getCellGroup<Triangle>(0);
  const Field<PhysD2,TopoD2,ArrayQ>::FieldCellGroupType<Triangle>& qfldCellGroup1 = qfld_local.getCellGroup<Triangle>(1);

  int DOFMap[6];

  //Node DOFs
  BOOST_CHECK_EQUAL( qfldCellGroup0.associativity(0).nNode(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(0).nNode(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(1).nNode(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(2).nNode(), 0 );

  //Edge DOFs
  BOOST_CHECK_EQUAL( qfldCellGroup0.associativity(0).nEdge(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(0).nEdge(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(1).nEdge(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(2).nEdge(), 0 );

  //Cell DOFs
  BOOST_CHECK_EQUAL( qfldCellGroup0.associativity(0).nCell(), 6 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(0).nCell(), 6 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(1).nCell(), 6 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(2).nCell(), 6 );

  qfldCellGroup0.associativity(0).getCellGlobalMapping( DOFMap, 6 );
  BOOST_CHECK_EQUAL( DOFMap[0], 0 );
  BOOST_CHECK_EQUAL( DOFMap[1], 1 );
  BOOST_CHECK_EQUAL( DOFMap[2], 2 );
  BOOST_CHECK_EQUAL( DOFMap[3], 3 );
  BOOST_CHECK_EQUAL( DOFMap[4], 4 );
  BOOST_CHECK_EQUAL( DOFMap[5], 5 );

  qfldCellGroup1.associativity(0).getCellGlobalMapping( DOFMap, 6 );
  BOOST_CHECK_EQUAL( DOFMap[0], 6 );
  BOOST_CHECK_EQUAL( DOFMap[1], 7 );
  BOOST_CHECK_EQUAL( DOFMap[2], 8 );
  BOOST_CHECK_EQUAL( DOFMap[3], 9 );
  BOOST_CHECK_EQUAL( DOFMap[4], 10 );
  BOOST_CHECK_EQUAL( DOFMap[5], 11 );

  qfldCellGroup1.associativity(1).getCellGlobalMapping( DOFMap, 6 );
  BOOST_CHECK_EQUAL( DOFMap[0], 12 );
  BOOST_CHECK_EQUAL( DOFMap[1], 13 );
  BOOST_CHECK_EQUAL( DOFMap[2], 14 );
  BOOST_CHECK_EQUAL( DOFMap[3], 15 );
  BOOST_CHECK_EQUAL( DOFMap[4], 16 );
  BOOST_CHECK_EQUAL( DOFMap[5], 17 );

  qfldCellGroup1.associativity(2).getCellGlobalMapping( DOFMap, 6 );
  BOOST_CHECK_EQUAL( DOFMap[0], 18 );
  BOOST_CHECK_EQUAL( DOFMap[1], 19 );
  BOOST_CHECK_EQUAL( DOFMap[2], 20 );
  BOOST_CHECK_EQUAL( DOFMap[3], 21 );
  BOOST_CHECK_EQUAL( DOFMap[4], 22 );
  BOOST_CHECK_EQUAL( DOFMap[5], 23 );

  Int3 edgeSign;

  edgeSign = qfldCellGroup0.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  for (int cell=0; cell<3; cell++)
  {
    edgeSign = qfldCellGroup1.associativity(cell).edgeSign();
    BOOST_CHECK_EQUAL( edgeSign[0], +1 );
    BOOST_CHECK_EQUAL( edgeSign[1], +1 );
    BOOST_CHECK_EQUAL( edgeSign[2], +1 );
  }

  BOOST_CHECK_EQUAL( qfld_local.nInteriorTraceGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_local.nBoundaryTraceGroups(), 0 );

}

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field2D_DG_Local_4Triangle_X2_ExtractInteriorCell_P2 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_DG_Area;

  typedef std::array<int,3> Int3;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField2D_4Triangle_X1_1Group xfld_X1;

  XField<PhysD2,TopoD2> xfld(xfld_X1,2); //Construct Q2 mesh from linear mesh

  int order = 2;
  QField2D_DG_Area qfld(xfld, order, BasisFunctionCategory_Legendre);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh for the interior triangle
  XField_Local<PhysD2,TopoD2> xfld_local(comm_local,connectivity,0,0);

  Field_Local<QField2D_DG_Area> qfld_local(xfld_local, qfld, order, BasisFunctionCategory_Legendre);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  qfld_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_local.getXField(), &xfld_local );

  BOOST_CHECK_EQUAL( qfld_local.nDOF(), 24 );

  for (int n = 0; n < qfld_local.nDOF(); n++)
    for (int i=0; i<2; i++)
      BOOST_CHECK_EQUAL(qfld_local.DOF(n)[i], qfld.DOF(n)[i]);


  BOOST_CHECK_EQUAL( qfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( qfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( qfld_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  const Field<PhysD2,TopoD2,ArrayQ>::FieldCellGroupType<Triangle>& qfldCellGroup0 = qfld_local.getCellGroup<Triangle>(0);
  const Field<PhysD2,TopoD2,ArrayQ>::FieldCellGroupType<Triangle>& qfldCellGroup1 = qfld_local.getCellGroup<Triangle>(1);

  int DOFMap[6];

  //Node DOFs
  BOOST_CHECK_EQUAL( qfldCellGroup0.associativity(0).nNode(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(0).nNode(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(1).nNode(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(2).nNode(), 0 );

  //Edge DOFs
  BOOST_CHECK_EQUAL( qfldCellGroup0.associativity(0).nEdge(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(0).nEdge(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(1).nEdge(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(2).nEdge(), 0 );

  //Cell DOFs
  BOOST_CHECK_EQUAL( qfldCellGroup0.associativity(0).nCell(), 6 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(0).nCell(), 6 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(1).nCell(), 6 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(2).nCell(), 6 );

  qfldCellGroup0.associativity(0).getCellGlobalMapping( DOFMap, 6 );
  BOOST_CHECK_EQUAL( DOFMap[0], 0 );
  BOOST_CHECK_EQUAL( DOFMap[1], 1 );
  BOOST_CHECK_EQUAL( DOFMap[2], 2 );
  BOOST_CHECK_EQUAL( DOFMap[3], 3 );
  BOOST_CHECK_EQUAL( DOFMap[4], 4 );
  BOOST_CHECK_EQUAL( DOFMap[5], 5 );

  qfldCellGroup1.associativity(0).getCellGlobalMapping( DOFMap, 6 );
  BOOST_CHECK_EQUAL( DOFMap[0], 6 );
  BOOST_CHECK_EQUAL( DOFMap[1], 7 );
  BOOST_CHECK_EQUAL( DOFMap[2], 8 );
  BOOST_CHECK_EQUAL( DOFMap[3], 9 );
  BOOST_CHECK_EQUAL( DOFMap[4], 10 );
  BOOST_CHECK_EQUAL( DOFMap[5], 11 );

  qfldCellGroup1.associativity(1).getCellGlobalMapping( DOFMap, 6 );
  BOOST_CHECK_EQUAL( DOFMap[0], 12 );
  BOOST_CHECK_EQUAL( DOFMap[1], 13 );
  BOOST_CHECK_EQUAL( DOFMap[2], 14 );
  BOOST_CHECK_EQUAL( DOFMap[3], 15 );
  BOOST_CHECK_EQUAL( DOFMap[4], 16 );
  BOOST_CHECK_EQUAL( DOFMap[5], 17 );

  qfldCellGroup1.associativity(2).getCellGlobalMapping( DOFMap, 6 );
  BOOST_CHECK_EQUAL( DOFMap[0], 18 );
  BOOST_CHECK_EQUAL( DOFMap[1], 19 );
  BOOST_CHECK_EQUAL( DOFMap[2], 20 );
  BOOST_CHECK_EQUAL( DOFMap[3], 21 );
  BOOST_CHECK_EQUAL( DOFMap[4], 22 );
  BOOST_CHECK_EQUAL( DOFMap[5], 23 );

  Int3 edgeSign;

  edgeSign = qfldCellGroup0.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  for (int cell=0; cell<3; cell++)
  {
    edgeSign = qfldCellGroup1.associativity(cell).edgeSign();
    BOOST_CHECK_EQUAL( edgeSign[0], +1 );
    BOOST_CHECK_EQUAL( edgeSign[1], +1 );
    BOOST_CHECK_EQUAL( edgeSign[2], +1 );
  }

  BOOST_CHECK_EQUAL( qfld_local.nInteriorTraceGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_local.nBoundaryTraceGroups(), 0 );

}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field2D_DG_Local_4Triangle_X1_ExtractBoundaryCell_P2 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_DG_Area;

  typedef std::array<int,3> Int3;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField2D_4Triangle_X1_1Group xfld;

  int order = 2;
  QField2D_DG_Area qfld(xfld, order, BasisFunctionCategory_Legendre);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh for a boundary triangle
  XField_Local<PhysD2,TopoD2> xfld_local(comm_local,connectivity,0,1);

  Field_Local<QField2D_DG_Area> qfld_local(xfld_local, qfld, order, BasisFunctionCategory_Legendre);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  qfld_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_local.getXField(), &xfld_local );

  BOOST_CHECK_EQUAL( qfld_local.nDOF(), 12 );

  BOOST_CHECK_EQUAL(qfld_local.DOF(0)[0], -7); BOOST_CHECK_EQUAL(qfld_local.DOF(0)[1], 7);
  BOOST_CHECK_EQUAL(qfld_local.DOF(1)[0], -8); BOOST_CHECK_EQUAL(qfld_local.DOF(1)[1], 8);
  BOOST_CHECK_EQUAL(qfld_local.DOF(2)[0], -9); BOOST_CHECK_EQUAL(qfld_local.DOF(2)[1], 9);
  BOOST_CHECK_EQUAL(qfld_local.DOF(3)[0],-10); BOOST_CHECK_EQUAL(qfld_local.DOF(3)[1],10);
  BOOST_CHECK_EQUAL(qfld_local.DOF(4)[0],-11); BOOST_CHECK_EQUAL(qfld_local.DOF(4)[1],11);
  BOOST_CHECK_EQUAL(qfld_local.DOF(5)[0],-12); BOOST_CHECK_EQUAL(qfld_local.DOF(5)[1],12);

  BOOST_CHECK_EQUAL(qfld_local.DOF( 6)[0], -1); BOOST_CHECK_EQUAL(qfld_local.DOF( 6)[1], 1);
  BOOST_CHECK_EQUAL(qfld_local.DOF( 7)[0], -2); BOOST_CHECK_EQUAL(qfld_local.DOF( 7)[1], 2);
  BOOST_CHECK_EQUAL(qfld_local.DOF( 8)[0], -3); BOOST_CHECK_EQUAL(qfld_local.DOF( 8)[1], 3);
  BOOST_CHECK_EQUAL(qfld_local.DOF( 9)[0], -4); BOOST_CHECK_EQUAL(qfld_local.DOF( 9)[1], 4);
  BOOST_CHECK_EQUAL(qfld_local.DOF(10)[0], -5); BOOST_CHECK_EQUAL(qfld_local.DOF(10)[1], 5);
  BOOST_CHECK_EQUAL(qfld_local.DOF(11)[0], -6); BOOST_CHECK_EQUAL(qfld_local.DOF(11)[1], 6);


  BOOST_CHECK_EQUAL( qfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( qfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( qfld_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  const Field<PhysD2,TopoD2,ArrayQ>::FieldCellGroupType<Triangle>& qfldCellGroup0 = qfld_local.getCellGroup<Triangle>(0);
  const Field<PhysD2,TopoD2,ArrayQ>::FieldCellGroupType<Triangle>& qfldCellGroup1 = qfld_local.getCellGroup<Triangle>(1);

  int DOFMap[6];

  //Node DOFs
  BOOST_CHECK_EQUAL( qfldCellGroup0.associativity(0).nNode(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(0).nNode(), 0 );

  //Edge DOFs
  BOOST_CHECK_EQUAL( qfldCellGroup0.associativity(0).nEdge(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(0).nEdge(), 0 );

  //Cell DOFs
  BOOST_CHECK_EQUAL( qfldCellGroup0.associativity(0).nCell(), 6 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(0).nCell(), 6 );

  qfldCellGroup0.associativity(0).getCellGlobalMapping( DOFMap, 6 );
  BOOST_CHECK_EQUAL( DOFMap[0], 0 );
  BOOST_CHECK_EQUAL( DOFMap[1], 1 );
  BOOST_CHECK_EQUAL( DOFMap[2], 2 );
  BOOST_CHECK_EQUAL( DOFMap[3], 3 );
  BOOST_CHECK_EQUAL( DOFMap[4], 4 );
  BOOST_CHECK_EQUAL( DOFMap[5], 5 );

  qfldCellGroup1.associativity(0).getCellGlobalMapping( DOFMap, 6 );
  BOOST_CHECK_EQUAL( DOFMap[0], 6 );
  BOOST_CHECK_EQUAL( DOFMap[1], 7 );
  BOOST_CHECK_EQUAL( DOFMap[2], 8 );
  BOOST_CHECK_EQUAL( DOFMap[3], 9 );
  BOOST_CHECK_EQUAL( DOFMap[4], 10 );
  BOOST_CHECK_EQUAL( DOFMap[5], 11 );

  Int3 edgeSign;

  edgeSign = qfldCellGroup0.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  edgeSign = qfldCellGroup1.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  BOOST_CHECK_EQUAL( qfld_local.nInteriorTraceGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_local.nBoundaryTraceGroups(), 0 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field2D_DG_Local_1Triangle_X1_P2 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_DG_Area;

  typedef std::array<int,3> Int3;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField2D_1Triangle_X1_1Group xfld;

  int order = 2;
  QField2D_DG_Area qfld(xfld, order, BasisFunctionCategory_Legendre);

  // interface solution
//  Field_DG_InteriorTrace<PhysD2, TopoD2, ArrayQ> uIfld(xfld, order, BasisFunctionCategory_Legendre);
//  uIfld.dump(3,std::cout);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh
  XField_Local<PhysD2,TopoD2> xfld_local(comm_local,connectivity,0,0);

  Field_Local<QField2D_DG_Area> qfld_local(xfld_local, qfld, order, BasisFunctionCategory_Legendre);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  qfld_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_local.getXField(), &xfld_local );

  BOOST_CHECK_EQUAL( qfld_local.nDOF(), 6 );

  for (int n = 0; n < qfld_local.nDOF(); n++)
    for (int i=0; i<2; i++)
      BOOST_CHECK_EQUAL(qfld_local.DOF(n)[i], qfld.DOF(n)[i]);


  BOOST_CHECK_EQUAL( qfld_local.nCellGroups(), 1 );
  BOOST_REQUIRE( qfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );

  const Field<PhysD2,TopoD2,ArrayQ>::FieldCellGroupType<Triangle>& qfldCellGroup0 = qfld_local.getCellGroup<Triangle>(0);

  int DOFMap[6];

  //Node DOFs
  BOOST_CHECK_EQUAL( qfldCellGroup0.associativity(0).nNode(), 0 );

  //Edge DOFs
  BOOST_CHECK_EQUAL( qfldCellGroup0.associativity(0).nEdge(), 0 );

  //Cell DOFs
  BOOST_CHECK_EQUAL( qfldCellGroup0.associativity(0).nCell(), 6 );

  qfldCellGroup0.associativity(0).getCellGlobalMapping( DOFMap, 6 );
  BOOST_CHECK_EQUAL( DOFMap[0], 0 );
  BOOST_CHECK_EQUAL( DOFMap[1], 1 );
  BOOST_CHECK_EQUAL( DOFMap[2], 2 );
  BOOST_CHECK_EQUAL( DOFMap[3], 3 );
  BOOST_CHECK_EQUAL( DOFMap[4], 4 );
  BOOST_CHECK_EQUAL( DOFMap[5], 5 );

  Int3 edgeSign;

  edgeSign = qfldCellGroup0.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  BOOST_CHECK_EQUAL( qfld_local.nInteriorTraceGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_local.nBoundaryTraceGroups(), 0 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field2D_DG_Local_4Triangle_X1_ExtractInteriorCell_InteriorTrace_P2 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_InteriorTrace<PhysD2, TopoD2, ArrayQ> QField2D_DG_ITrace;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  Real tol = 1e-12;

  XField2D_4Triangle_X1_1Group xfld;

  int order = 2;
  QField2D_DG_ITrace qfld(xfld, order, BasisFunctionCategory_Legendre);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh for the interior triangle
  XField_Local<PhysD2,TopoD2> xfld_local(comm_local,connectivity,0,0);

  Field_Local<QField2D_DG_ITrace> qfld_local(xfld_local, qfld, order, BasisFunctionCategory_Legendre);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  qfld_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_local.getXField(), &xfld_local );

  BOOST_CHECK_EQUAL( qfld_local.nDOF(), 9 );

  for (int n = 0; n < qfld_local.nDOF(); n++)
    for (int i=0; i<2; i++)
      SANS_CHECK_CLOSE(qfld_local.DOF(n)[i], qfld.DOF(n)[i], tol, tol);


  BOOST_CHECK_EQUAL( qfld_local.nCellGroups(), 0 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( qfld_local.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( qfld_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const QField2D_DG_ITrace::FieldTraceGroupType<Line>& qldITraceGroup = qfld_local.getInteriorTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( qldITraceGroup.nElem(), 3 );

  int DOFMap[3];

  //Node DOFs
  BOOST_CHECK_EQUAL( qldITraceGroup.associativity(0).nNode(), 0 );
  BOOST_CHECK_EQUAL( qldITraceGroup.associativity(1).nNode(), 0 );
  BOOST_CHECK_EQUAL( qldITraceGroup.associativity(2).nNode(), 0 );

  //Edge DOFs
  BOOST_CHECK_EQUAL( qldITraceGroup.associativity(0).nEdge(), 3 );
  qldITraceGroup.associativity(0).getEdgeGlobalMapping( DOFMap, 3 );
  BOOST_CHECK_EQUAL( DOFMap[0], 0 );
  BOOST_CHECK_EQUAL( DOFMap[1], 1 );
  BOOST_CHECK_EQUAL( DOFMap[2], 2 );

  BOOST_CHECK_EQUAL( qldITraceGroup.associativity(1).nEdge(), 3 );
  qldITraceGroup.associativity(1).getEdgeGlobalMapping( DOFMap, 3 );
  BOOST_CHECK_EQUAL( DOFMap[0], 3 );
  BOOST_CHECK_EQUAL( DOFMap[1], 4 );
  BOOST_CHECK_EQUAL( DOFMap[2], 5 );

  BOOST_CHECK_EQUAL( qldITraceGroup.associativity(2).nEdge(), 3 );
  qldITraceGroup.associativity(2).getEdgeGlobalMapping( DOFMap, 3 );
  BOOST_CHECK_EQUAL( DOFMap[0], 6 );
  BOOST_CHECK_EQUAL( DOFMap[1], 7 );
  BOOST_CHECK_EQUAL( DOFMap[2], 8 );

  BOOST_CHECK_EQUAL( qfld_local.nBoundaryTraceGroups(), 0 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field2D_DG_Local_4Triangle_X1_ExtractBoundaryCell_InteriorTrace_P2 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_InteriorTrace<PhysD2, TopoD2, ArrayQ> QField2D_DG_ITrace;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  Real tol = 1e-12;

  XField2D_4Triangle_X1_1Group xfld;

  int order = 2;
  QField2D_DG_ITrace qfld(xfld, order, BasisFunctionCategory_Legendre);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh for the boundary triangle
  XField_Local<PhysD2,TopoD2> xfld_local(comm_local,connectivity,0,2);

  Field_Local<QField2D_DG_ITrace> qfld_local(xfld_local, qfld, order, BasisFunctionCategory_Legendre);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  qfld_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_local.getXField(), &xfld_local );

  BOOST_CHECK_EQUAL( qfld_local.nDOF(), 3 );

  for (int i=0; i < 2; i++)
  {
    SANS_CHECK_CLOSE( qfld_local.DOF(0)[i], qfld.DOF(0+3)[i], tol, tol);
    SANS_CHECK_CLOSE(-qfld_local.DOF(1)[i], qfld.DOF(1+3)[i], tol, tol); //Negative linear mode DOF because trace is reversed
    SANS_CHECK_CLOSE( qfld_local.DOF(2)[i], qfld.DOF(2+3)[i], tol, tol);
  }

  BOOST_CHECK_EQUAL( qfld_local.nCellGroups(), 0 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( qfld_local.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( qfld_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const QField2D_DG_ITrace::FieldTraceGroupType<Line>& qldITraceGroup = qfld_local.getInteriorTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( qldITraceGroup.nElem(), 1 );

  int DOFMap[3];

  //Node DOFs
  BOOST_CHECK_EQUAL( qldITraceGroup.associativity(0).nNode(), 0 );

  //Edge DOFs
  BOOST_CHECK_EQUAL( qldITraceGroup.associativity(0).nEdge(), 3 );
  qldITraceGroup.associativity(0).getEdgeGlobalMapping( DOFMap, 3 );
  BOOST_CHECK_EQUAL( DOFMap[0], 0 );
  BOOST_CHECK_EQUAL( DOFMap[1], 1 );
  BOOST_CHECK_EQUAL( DOFMap[2], 2 );

  BOOST_CHECK_EQUAL( qfld_local.nBoundaryTraceGroups(), 0 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field2D_DG_Local_4Triangle_X1_ExtractInteriorCell_BoundaryTrace_P2 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> QField2D_DG_BTrace;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField2D_4Triangle_X1_1Group xfld;

  int order = 2;
  QField2D_DG_BTrace qfld(xfld, order, BasisFunctionCategory_Legendre);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh for the interior triangle
  XField_Local<PhysD2,TopoD2> xfld_local(comm_local,connectivity,0,0);

  Field_Local<QField2D_DG_BTrace> qfld_local(xfld_local, qfld, order, BasisFunctionCategory_Legendre);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  qfld_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_local.getXField(), &xfld_local );

  BOOST_CHECK_EQUAL( qfld_local.nDOF(), 0 );

  BOOST_CHECK_EQUAL( qfld_local.nCellGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_local.nInteriorTraceGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_local.nBoundaryTraceGroups(), 0 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field2D_DG_Local_4Triangle_X1_ExtractBoundaryCell_BoundaryTrace_P2 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ> QField2D_DG_BTrace;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField2D_4Triangle_X1_1Group xfld;

  int order = 2;
  QField2D_DG_BTrace qfld(xfld, order, BasisFunctionCategory_Legendre);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh for the interior triangle
  XField_Local<PhysD2,TopoD2> xfld_local(comm_local,connectivity,0,1);

  Field_Local<QField2D_DG_BTrace> qfld_local(xfld_local, qfld, order, BasisFunctionCategory_Legendre);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  qfld_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_local.getXField(), &xfld_local );

  BOOST_CHECK_EQUAL( qfld_local.nDOF(), 3*2 );

  for (int i=0; i<2; i++)
  {
    BOOST_CHECK_EQUAL(qfld_local.DOF(0)[i], qfld.DOF(6)[i]);
    BOOST_CHECK_EQUAL(qfld_local.DOF(1)[i], qfld.DOF(7)[i]);
    BOOST_CHECK_EQUAL(qfld_local.DOF(2)[i], qfld.DOF(8)[i]);

    BOOST_CHECK_EQUAL(qfld_local.DOF(3)[i], qfld.DOF(9)[i]);
    BOOST_CHECK_EQUAL(qfld_local.DOF(4)[i], qfld.DOF(10)[i]);
    BOOST_CHECK_EQUAL(qfld_local.DOF(5)[i], qfld.DOF(11)[i]);
  }

  BOOST_CHECK_EQUAL( qfld_local.nCellGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_local.nInteriorTraceGroups(), 0 );

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( qfld_local.nBoundaryTraceGroups(), 2 );
  BOOST_REQUIRE( qfld_local.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( qfld_local.getBoundaryTraceGroup<Line>(1).topoTypeID() == typeid(Line) );

  const QField2D_DG_BTrace::FieldTraceGroupType<Line>& qldBTraceGroup0 = qfld_local.getBoundaryTraceGroup<Line>(0);
  const QField2D_DG_BTrace::FieldTraceGroupType<Line>& qldBTraceGroup1 = qfld_local.getBoundaryTraceGroup<Line>(1);

  BOOST_CHECK_EQUAL( qldBTraceGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( qldBTraceGroup1.nElem(), 1 );

  int DOFMap[3];

  BOOST_CHECK_EQUAL( qldBTraceGroup0.associativity(0).nNode(), 0 );
  BOOST_CHECK_EQUAL( qldBTraceGroup0.associativity(0).nEdge(), 3 );

  qldBTraceGroup0.associativity(0).getEdgeGlobalMapping( DOFMap, 3 );
  BOOST_CHECK_EQUAL( DOFMap[0], 3*0 + 0 );
  BOOST_CHECK_EQUAL( DOFMap[1], 3*0 + 1 );
  BOOST_CHECK_EQUAL( DOFMap[2], 3*0 + 2 );


  BOOST_CHECK_EQUAL( qldBTraceGroup1.associativity(0).nNode(), 0 );
  BOOST_CHECK_EQUAL( qldBTraceGroup1.associativity(0).nEdge(), 3 );

  qldBTraceGroup1.associativity(0).getEdgeGlobalMapping( DOFMap, 3 );
  BOOST_CHECK_EQUAL( DOFMap[0], 3*1 + 0 );
  BOOST_CHECK_EQUAL( DOFMap[1], 3*1 + 1 );
  BOOST_CHECK_EQUAL( DOFMap[2], 3*1 + 2 );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
