// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Field1D_Local_Split_DG_btest
//

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Meshing/XField1D/XField1D.h"

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunctionLine.h"

#include "Field/XFieldLine_Local_Split.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_InteriorTrace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/Local/Field_Local.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


//Explicitly instantiate classes to get proper coverage information
namespace SANS
{
template class Field_DG_Cell< PhysD1, TopoD1, Real >;
template class Field_DG_InteriorTrace< PhysD1, TopoD1, Real >;
template class Field_DG_BoundaryTrace< PhysD1, TopoD1, Real >;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( Field1D_Local_Split_DG_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field1D_DG_Local_Split_3Line_X1_ExtractInteriorCell_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD1, TopoD1, ArrayQ > QField1D_DG_Line;

  typedef QField1D_DG_Line::FieldCellGroupType<Line> FieldCellGroupType;
  typedef FieldCellGroupType::ElementType<> ElementFieldClass;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  Real tol = 1e-11;

  XField1D xfld(3); //Linear mesh with 3 lines

  int order = 1;
  QField1D_DG_Line qfld(xfld, order, BasisFunctionCategory_Legendre);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD1,TopoD1> connectivity(xfld);

  //Extract the local mesh for the interior line
  int main_group = 0;
  int main_elem = 1;
  XField_Local<PhysD1,TopoD1> xfld_local(comm_local,connectivity,main_group,main_elem);

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD1,TopoD1> connectivity_local(xfld_local);

  XField_Local_Split<PhysD1,TopoD1> xfld_split_local(xfld_local, connectivity_local);

  Field_Local<QField1D_DG_Line> qfld_split_local(xfld_split_local, qfld, order, BasisFunctionCategory_Legendre);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  qfld_split_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_split_local.getXField(), &xfld_split_local );

  BOOST_CHECK_EQUAL( qfld_split_local.nDOF(), 8 );

  //Get global element
  const FieldCellGroupType& global_cellgrp = qfld.getCellGroup<Line>(main_group);
  ElementFieldClass fldElem_global( global_cellgrp.basis() );
  global_cellgrp.getElement(fldElem_global, main_elem);

  const FieldCellGroupType& local_cellgrp = qfld_split_local.getCellGroup<Line>(0);
  ElementFieldClass fldElem_local0( global_cellgrp.basis() );
  ElementFieldClass fldElem_local1( global_cellgrp.basis() );
  local_cellgrp.getElement(fldElem_local0, 0);
  local_cellgrp.getElement(fldElem_local1, 1);

  int N = 5;
  for (int k=0; k<N; k++)
  {
    Real sref = ((Real)k)/((Real)(N-1));

    for (int i=0; i<2; i++)
    {
      SANS_CHECK_CLOSE(fldElem_local0.eval(sref)[i], fldElem_global.eval(0.5*sref      )[i], tol, tol);
      SANS_CHECK_CLOSE(fldElem_local1.eval(sref)[i], fldElem_global.eval(0.5*sref + 0.5)[i], tol, tol);
    }
  }

  for (int i=0; i<2; i++)
  {
    BOOST_CHECK_EQUAL(qfld_split_local.DOF(4)[i], qfld.DOF(4)[i]);
    BOOST_CHECK_EQUAL(qfld_split_local.DOF(5)[i], qfld.DOF(5)[i]);

    BOOST_CHECK_EQUAL(qfld_split_local.DOF(6)[i], qfld.DOF(0)[i]);
    BOOST_CHECK_EQUAL(qfld_split_local.DOF(7)[i], qfld.DOF(1)[i]);
  }


  BOOST_CHECK_EQUAL( qfld_split_local.nCellGroups(), 2 );
  BOOST_REQUIRE( qfld_split_local.getCellGroupBase(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( qfld_split_local.getCellGroupBase(1).topoTypeID() == typeid(Line) );

  const QField1D_DG_Line::FieldCellGroupType<Line>& qfldCellGroup0 = qfld_split_local.getCellGroup<Line>(0);
  const QField1D_DG_Line::FieldCellGroupType<Line>& qfldCellGroup1 = qfld_split_local.getCellGroup<Line>(1);

  BOOST_CHECK_EQUAL( qfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.nElem(), 2 );

  int DOFMap[2];

  qfldCellGroup0.associativity(0).getEdgeGlobalMapping( DOFMap, 2 );
  BOOST_CHECK_EQUAL( DOFMap[0], 0 );
  BOOST_CHECK_EQUAL( DOFMap[1], 1 );

  qfldCellGroup0.associativity(1).getEdgeGlobalMapping( DOFMap, 2 );
  BOOST_CHECK_EQUAL( DOFMap[0], 2 );
  BOOST_CHECK_EQUAL( DOFMap[1], 3 );

  qfldCellGroup1.associativity(0).getEdgeGlobalMapping( DOFMap, 2 );
  BOOST_CHECK_EQUAL( DOFMap[0], 4 );
  BOOST_CHECK_EQUAL( DOFMap[1], 5 );

  qfldCellGroup1.associativity(1).getEdgeGlobalMapping( DOFMap, 2 );
  BOOST_CHECK_EQUAL( DOFMap[0], 6 );
  BOOST_CHECK_EQUAL( DOFMap[1], 7 );

  BOOST_CHECK_EQUAL( qfld_split_local.nInteriorTraceGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_split_local.nBoundaryTraceGroups(), 0 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field1D_DG_Local_Split_3Line_X1_ExtractInteriorCell_P2 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD1, TopoD1, ArrayQ > QField1D_DG_Line;

  typedef QField1D_DG_Line::FieldCellGroupType<Line> FieldCellGroupType;
  typedef FieldCellGroupType::ElementType<> ElementFieldClass;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  Real tol = 1e-11;

  XField1D xfld(3); //Linear mesh with 3 lines

  int order = 2;
  QField1D_DG_Line qfld(xfld, order, BasisFunctionCategory_Legendre);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD1,TopoD1> connectivity(xfld);

  //Extract the local mesh for the interior line
  int main_group = 0;
  int main_elem = 1;
  XField_Local<PhysD1,TopoD1> xfld_local(comm_local,connectivity,main_group,main_elem);

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD1,TopoD1> connectivity_local(xfld_local);

  XField_Local_Split<PhysD1,TopoD1> xfld_split_local(xfld_local, connectivity_local);

  Field_Local<QField1D_DG_Line> qfld_split_local(xfld_split_local, qfld, order, BasisFunctionCategory_Legendre);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  qfld_split_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_split_local.getXField(), &xfld_split_local );

  BOOST_CHECK_EQUAL( qfld_split_local.nDOF(), 12 );

  //Get global element
  const FieldCellGroupType& global_cellgrp = qfld.getCellGroup<Line>(main_group);
  ElementFieldClass fldElem_global( global_cellgrp.basis() );
  global_cellgrp.getElement(fldElem_global, main_elem);

  const FieldCellGroupType& local_cellgrp = qfld_split_local.getCellGroup<Line>(0);
  ElementFieldClass fldElem_local0( global_cellgrp.basis() );
  ElementFieldClass fldElem_local1( global_cellgrp.basis() );
  local_cellgrp.getElement(fldElem_local0, 0);
  local_cellgrp.getElement(fldElem_local1, 1);

  int N = 5;
  for (int k=0; k<N; k++)
  {
    Real sref = ((Real)k)/((Real)(N-1));

    for (int i=0; i<2; i++)
    {
      SANS_CHECK_CLOSE(fldElem_local0.eval(sref)[i], fldElem_global.eval(0.5*sref      )[i], tol, tol);
      SANS_CHECK_CLOSE(fldElem_local1.eval(sref)[i], fldElem_global.eval(0.5*sref + 0.5)[i], tol, tol);
    }
  }

  for (int i=0; i<2; i++)
  {
    BOOST_CHECK_EQUAL(qfld_split_local.DOF(6)[i], qfld.DOF(6)[i]);
    BOOST_CHECK_EQUAL(qfld_split_local.DOF(7)[i], qfld.DOF(7)[i]);
    BOOST_CHECK_EQUAL(qfld_split_local.DOF(8)[i], qfld.DOF(8)[i]);

    BOOST_CHECK_EQUAL(qfld_split_local.DOF( 9)[i], qfld.DOF(0)[i]);
    BOOST_CHECK_EQUAL(qfld_split_local.DOF(10)[i], qfld.DOF(1)[i]);
    BOOST_CHECK_EQUAL(qfld_split_local.DOF(11)[i], qfld.DOF(2)[i]);
  }


  BOOST_CHECK_EQUAL( qfld_split_local.nCellGroups(), 2 );
  BOOST_REQUIRE( qfld_split_local.getCellGroupBase(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( qfld_split_local.getCellGroupBase(1).topoTypeID() == typeid(Line) );

  const QField1D_DG_Line::FieldCellGroupType<Line>& qfldCellGroup0 = qfld_split_local.getCellGroup<Line>(0);
  const QField1D_DG_Line::FieldCellGroupType<Line>& qfldCellGroup1 = qfld_split_local.getCellGroup<Line>(1);

  BOOST_CHECK_EQUAL( qfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.nElem(), 2 );

  int DOFMap[3];

  qfldCellGroup0.associativity(0).getEdgeGlobalMapping( DOFMap, 3 );
  BOOST_CHECK_EQUAL( DOFMap[0], 0 );
  BOOST_CHECK_EQUAL( DOFMap[1], 1 );
  BOOST_CHECK_EQUAL( DOFMap[2], 2 );

  qfldCellGroup0.associativity(1).getEdgeGlobalMapping( DOFMap, 3 );
  BOOST_CHECK_EQUAL( DOFMap[0], 3 );
  BOOST_CHECK_EQUAL( DOFMap[1], 4 );
  BOOST_CHECK_EQUAL( DOFMap[2], 5 );

  qfldCellGroup1.associativity(0).getEdgeGlobalMapping( DOFMap, 3 );
  BOOST_CHECK_EQUAL( DOFMap[0], 6 );
  BOOST_CHECK_EQUAL( DOFMap[1], 7 );
  BOOST_CHECK_EQUAL( DOFMap[2], 8 );

  qfldCellGroup1.associativity(1).getEdgeGlobalMapping( DOFMap, 3 );
  BOOST_CHECK_EQUAL( DOFMap[0], 9 );
  BOOST_CHECK_EQUAL( DOFMap[1], 10 );
  BOOST_CHECK_EQUAL( DOFMap[2], 11 );

  BOOST_CHECK_EQUAL( qfld_split_local.nInteriorTraceGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_split_local.nBoundaryTraceGroups(), 0 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field1D_DG_Local_Split_3Line_X1_ExtractBoundaryCell_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD1, TopoD1, ArrayQ > QField1D_DG_Line;

  typedef QField1D_DG_Line::FieldCellGroupType<Line> FieldCellGroupType;
  typedef FieldCellGroupType::ElementType<> ElementFieldClass;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  Real tol = 1e-11;

  XField1D xfld(3); //Linear mesh with 3 lines

  int order = 1;
  QField1D_DG_Line qfld(xfld, order, BasisFunctionCategory_Legendre);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD1,TopoD1> connectivity(xfld);

  //Extract the local mesh for the rightmost line
  int main_group = 0;
  int main_elem = 2;
  XField_Local<PhysD1,TopoD1> xfld_local(comm_local,connectivity,main_group,main_elem);

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD1,TopoD1> connectivity_local(xfld_local);

  XField_Local_Split<PhysD1,TopoD1> xfld_split_local(xfld_local, connectivity_local);

  Field_Local<QField1D_DG_Line> qfld_split_local(xfld_split_local, qfld, order, BasisFunctionCategory_Legendre);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  qfld_split_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_split_local.getXField(), &xfld_split_local );

  BOOST_CHECK_EQUAL( qfld_split_local.nDOF(), 6 );

  //Get global element
  const FieldCellGroupType& global_cellgrp = qfld.getCellGroup<Line>(main_group);
  ElementFieldClass fldElem_global( global_cellgrp.basis() );
  global_cellgrp.getElement(fldElem_global, main_elem);

  const FieldCellGroupType& local_cellgrp = qfld_split_local.getCellGroup<Line>(0);
  ElementFieldClass fldElem_local0( global_cellgrp.basis() );
  ElementFieldClass fldElem_local1( global_cellgrp.basis() );
  local_cellgrp.getElement(fldElem_local0, 0);
  local_cellgrp.getElement(fldElem_local1, 1);

  int N = 5;
  for (int k=0; k<N; k++)
  {
    Real sref = ((Real)k)/((Real)(N-1));

    for (int i=0; i<2; i++)
    {
      SANS_CHECK_CLOSE(fldElem_local0.eval(sref)[i], fldElem_global.eval(0.5*sref      )[i], tol, tol);
      SANS_CHECK_CLOSE(fldElem_local1.eval(sref)[i], fldElem_global.eval(0.5*sref + 0.5)[i], tol, tol);
    }
  }

  for (int i=0; i<2; i++)
  {
    BOOST_CHECK_EQUAL(qfld_split_local.DOF(4)[i], qfld.DOF(2)[i]);
    BOOST_CHECK_EQUAL(qfld_split_local.DOF(5)[i], qfld.DOF(3)[i]);
  }


  BOOST_CHECK_EQUAL( qfld_split_local.nCellGroups(), 2 );
  BOOST_REQUIRE( qfld_split_local.getCellGroupBase(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( qfld_split_local.getCellGroupBase(1).topoTypeID() == typeid(Line) );

  const QField1D_DG_Line::FieldCellGroupType<Line>& qfldCellGroup0 = qfld_split_local.getCellGroup<Line>(0);
  const QField1D_DG_Line::FieldCellGroupType<Line>& qfldCellGroup1 = qfld_split_local.getCellGroup<Line>(1);

  BOOST_CHECK_EQUAL( qfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.nElem(), 1 );

  int DOFMap[2];

  qfldCellGroup0.associativity(0).getEdgeGlobalMapping( DOFMap, 2 );
  BOOST_CHECK_EQUAL( DOFMap[0], 0 );
  BOOST_CHECK_EQUAL( DOFMap[1], 1 );

  qfldCellGroup0.associativity(1).getEdgeGlobalMapping( DOFMap, 2 );
  BOOST_CHECK_EQUAL( DOFMap[0], 2 );
  BOOST_CHECK_EQUAL( DOFMap[1], 3 );

  qfldCellGroup1.associativity(0).getEdgeGlobalMapping( DOFMap, 2 );
  BOOST_CHECK_EQUAL( DOFMap[0], 4 );
  BOOST_CHECK_EQUAL( DOFMap[1], 5 );

  BOOST_CHECK_EQUAL( qfld_split_local.nInteriorTraceGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_split_local.nBoundaryTraceGroups(), 0 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field1D_DG_Local_Split_3Line_X1_ExtractInteriorCell_BoundaryTrace )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_BoundaryTrace< PhysD1, TopoD1, ArrayQ > QField1D_DG_BTrace;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField1D xfld(3); //Linear mesh with 3 lines

  int order = 1;
  QField1D_DG_BTrace qfld(xfld, order, BasisFunctionCategory_Legendre);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD1,TopoD1> connectivity(xfld);

  //Extract the local mesh for the interior line
  int main_group = 0;
  int main_elem = 1;
  XField_Local<PhysD1,TopoD1> xfld_local(comm_local,connectivity,main_group,main_elem);

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD1,TopoD1> connectivity_local(xfld_local);

  XField_Local_Split<PhysD1,TopoD1> xfld_split_local(xfld_local, connectivity_local);

  Field_Local<QField1D_DG_BTrace> qfld_split_local(xfld_split_local, qfld, order, BasisFunctionCategory_Legendre);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  qfld_split_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_split_local.getXField(), &xfld_split_local );

  BOOST_CHECK_EQUAL( qfld_split_local.nDOF(), 0 );

  BOOST_CHECK_EQUAL( qfld_split_local.nCellGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_split_local.nInteriorTraceGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_split_local.nBoundaryTraceGroups(), 0 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field1D_DG_Local_Split_3Line_X1_ExtractInteriorCell_InteriorTrace)
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_InteriorTrace< PhysD1, TopoD1, ArrayQ > QField1D_DG_ITrace;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField1D xfld(3); //Linear mesh with 3 lines

  int order = 0;
  QField1D_DG_ITrace qfld(xfld, order, BasisFunctionCategory_Legendre);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD1,TopoD1> connectivity(xfld);

  //Extract the local mesh for the interior line
  XField_Local<PhysD1,TopoD1> xfld_local(comm_local,connectivity,0,1);

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD1,TopoD1> connectivity_local(xfld_local);

  XField_Local_Split<PhysD1,TopoD1> xfld_split_local(xfld_local, connectivity_local);

  Field_Local<QField1D_DG_ITrace> qfld_split_local(xfld_split_local, qfld, order, BasisFunctionCategory_Legendre);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  qfld_split_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_split_local.getXField(), &xfld_split_local );

  BOOST_CHECK_EQUAL( qfld_split_local.nDOF(), 3 );

  for (int i=0; i<2; i++)
  {
    BOOST_CHECK_EQUAL(qfld_split_local.DOF(0)[i], qfld.DOF(1)[i]);
    BOOST_CHECK_EQUAL(qfld_split_local.DOF(1)[i], qfld.DOF(0)[i]);
    BOOST_CHECK_EQUAL(qfld_split_local.DOF(2)[i], 0.0);
  }

  BOOST_CHECK_EQUAL( qfld_split_local.nCellGroups(), 0 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( qfld_split_local.nInteriorTraceGroups(), 2 );
  BOOST_REQUIRE( qfld_split_local.getInteriorTraceGroup<Node>(0).topoTypeID() == typeid(Node) );
  BOOST_REQUIRE( qfld_split_local.getInteriorTraceGroup<Node>(1).topoTypeID() == typeid(Node) );

  const QField1D_DG_ITrace::FieldTraceGroupType<Node>& qldITraceGroup0 = qfld_split_local.getInteriorTraceGroup<Node>(0);
  const QField1D_DG_ITrace::FieldTraceGroupType<Node>& qldITraceGroup1 = qfld_split_local.getInteriorTraceGroup<Node>(1);

  BOOST_CHECK_EQUAL( qldITraceGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( qldITraceGroup1.nElem(), 1 );

  int DOFMap[1];

  //Node DOFs
  BOOST_CHECK_EQUAL( qldITraceGroup0.associativity(0).nNode(), 1 );
  qldITraceGroup0.associativity(0).getNodeGlobalMapping( DOFMap, 1 );
  BOOST_CHECK_EQUAL( DOFMap[0], 0 );

  BOOST_CHECK_EQUAL( qldITraceGroup0.associativity(1).nNode(), 1 );
  qldITraceGroup0.associativity(1).getNodeGlobalMapping( DOFMap, 1 );
  BOOST_CHECK_EQUAL( DOFMap[0], 1 );

  BOOST_CHECK_EQUAL( qldITraceGroup1.associativity(0).nNode(), 1 );
  qldITraceGroup1.associativity(0).getNodeGlobalMapping( DOFMap, 1 );
  BOOST_CHECK_EQUAL( DOFMap[0], 2 );

  BOOST_CHECK_EQUAL( qfld_split_local.nBoundaryTraceGroups(), 0 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field1D_DG_Local_Split_3Line_X1_ExtractInteriorCell_InteriorTrace_ProjectFromCell)
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_InteriorTrace< PhysD1, TopoD1, ArrayQ > QField1D_DG_ITrace;

  typedef Field_DG_Cell< PhysD1, TopoD1, ArrayQ > QField1D_DG_Cell;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField1D xfld(3); //Linear mesh with 3 lines

  int order = 1;
  QField1D_DG_ITrace qfld(xfld, order, BasisFunctionCategory_Hierarchical);
  qfld = 0.0;

  QField1D_DG_Cell qfldcell(xfld, order, BasisFunctionCategory_Hierarchical);

  //Set some non-zero solution
  for (int n = 0; n < qfldcell.nDOF(); n++)
    qfldcell.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD1,TopoD1> connectivity(xfld);

  //Extract the local mesh for the interior line
  XField_Local<PhysD1,TopoD1> xfld_local(comm_local,connectivity,0,1);

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD1,TopoD1> connectivity_local(xfld_local);

  XField_Local_Split<PhysD1,TopoD1> xfld_split_local(xfld_local, connectivity_local);

  Field_Local<QField1D_DG_ITrace> qfld_split_local(xfld_split_local, qfld, order, BasisFunctionCategory_Hierarchical);

  Field_Local_Transfer<TopoD1>::transferFromFieldCell(qfldcell, qfld_split_local);

#if 0
  qfldcell.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  qfld_split_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_split_local.getXField(), &xfld_split_local );

  BOOST_CHECK_EQUAL( qfld_split_local.nDOF(), 3 );

  for (int i=0; i<2; i++)
  {
    BOOST_CHECK_EQUAL(qfld_split_local.DOF(0)[i], 0.0);
    BOOST_CHECK_EQUAL(qfld_split_local.DOF(1)[i], 0.0);
  }

  BOOST_CHECK_EQUAL(qfld_split_local.DOF(2)[0], -3.5);
  BOOST_CHECK_EQUAL(qfld_split_local.DOF(2)[1],  3.5);

  BOOST_CHECK_EQUAL( qfld_split_local.nCellGroups(), 0 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( qfld_split_local.nInteriorTraceGroups(), 2 );
  BOOST_REQUIRE( qfld_split_local.getInteriorTraceGroup<Node>(0).topoTypeID() == typeid(Node) );
  BOOST_REQUIRE( qfld_split_local.getInteriorTraceGroup<Node>(1).topoTypeID() == typeid(Node) );

  const QField1D_DG_ITrace::FieldTraceGroupType<Node>& qldITraceGroup0 = qfld_split_local.getInteriorTraceGroup<Node>(0);
  const QField1D_DG_ITrace::FieldTraceGroupType<Node>& qldITraceGroup1 = qfld_split_local.getInteriorTraceGroup<Node>(1);

  BOOST_CHECK_EQUAL( qldITraceGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( qldITraceGroup1.nElem(), 1 );

  int DOFMap[1];

  //Node DOFs
  BOOST_CHECK_EQUAL( qldITraceGroup0.associativity(0).nNode(), 1 );
  qldITraceGroup0.associativity(0).getNodeGlobalMapping( DOFMap, 1 );
  BOOST_CHECK_EQUAL( DOFMap[0], 0 );

  BOOST_CHECK_EQUAL( qldITraceGroup0.associativity(1).nNode(), 1 );
  qldITraceGroup0.associativity(1).getNodeGlobalMapping( DOFMap, 1 );
  BOOST_CHECK_EQUAL( DOFMap[0], 1 );

  BOOST_CHECK_EQUAL( qldITraceGroup1.associativity(0).nNode(), 1 );
  qldITraceGroup1.associativity(0).getNodeGlobalMapping( DOFMap, 1 );
  BOOST_CHECK_EQUAL( DOFMap[0], 2 );

  BOOST_CHECK_EQUAL( qfld_split_local.nBoundaryTraceGroups(), 0 );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
