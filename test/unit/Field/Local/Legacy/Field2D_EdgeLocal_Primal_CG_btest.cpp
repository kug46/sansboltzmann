// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Field2D_EdgeLocal_CG_btest
//

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_LocalRefine_Triangle_X1.h"

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunctionArea_Triangle.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_CG_InteriorTrace.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"
#include "Field/Local/Field_Local.h"
#include "Field/Field_NodalView.h"

#include "Field/XField_EdgeLocal.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

//Explicitly instantiate classes to get proper coverage information
namespace SANS
{
template class Field_CG_Cell< PhysD2, TopoD2, Real >;
template class Field_CG_InteriorTrace< PhysD2, TopoD2, Real >;
template class Field_CG_BoundaryTrace< PhysD2, TopoD2, Real >;
}



//############################################################################//
BOOST_AUTO_TEST_SUITE( Field2D_EdgeLocal_Primal_CG_test_suite )

// A helper function to construct a Solver interface and test all the edges on a grid
void localEdgeExtractAndTransfer(const XField<PhysD2, TopoD2>& xfld, const BasisFunctionCategory basis_category,
                             const int order, const Real small_tol, const Real close_tol, const bool isSplit )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_CG_Area;
  typedef Field_CG_BoundaryTrace< PhysD2, TopoD2, ArrayQ > QField2D_CG_BTrace;

  typedef DLA::VectorS<TopoD2::D,Real> RefCoordType;
  typedef DLA::VectorS<TopoD1::D,Real> TraceRefCoordType;

  typedef QField2D_CG_Area::FieldCellGroupType<Triangle> FieldCellGroupType;
  typedef FieldCellGroupType::ElementType<> ElementFieldClass;
  typedef typename XField<PhysD2, TopoD2>::template FieldCellGroupType<Triangle> XFieldCellGroupType;
  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

  typedef QField2D_CG_BTrace::FieldTraceGroupType<Line> FieldTraceGroupType;
  typedef FieldTraceGroupType::ElementType<> ElementTraceFieldClass;

  const int nEdge = Triangle::NEdge; // Number of different edges
  typedef typename std::set<std::array<int,Line::NNode>> EdgeList;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  QField2D_CG_Area qfld(xfld, order, basis_category);
  QField2D_CG_BTrace lgfld(xfld, order, basis_category);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {1*(-n)-1, (n)+1};

  //Set some non-zero solution
  for (int n = 0; n < lgfld.nDOF(); n++)
    lgfld.DOF(n) = {1*(-n)-1, (n)+1};

  ///////////////////////
  // LOOP OVER CELL GROUPS ->
  ///////////////////////
  const int (*EdgeNodes)[Line::NNode] = ElementEdges<Triangle>::EdgeNodes;
  for (int group = 0; group  < xfld.nCellGroups(); group++)
  {
    EdgeList edge_list, failed_edge_list;
    typename EdgeList::iterator edge_list_it;

    // edge_list is cleared with each loop over the groups
    XFieldCellGroupType xfldCellGroup = xfld.template getCellGroup<Triangle>(group);
    const int nElem = xfldCellGroup.nElem();

    ElementXFieldClass xfldElem(xfldCellGroup.basis() );

    ///////////////////////
    // EDGE LIST EXTRACTION
    ///////////////////////

    for ( int elem = 0; elem < nElem; elem++ )
    {
      xfldCellGroup.getElement(xfldElem,elem);
      std::array<int,Triangle::NNode> nodeMap;

      xfldCellGroup.associativity(elem).getNodeGlobalMapping( nodeMap.data(), nodeMap.size() );

      for ( int edge = 0; edge < nEdge; edge++ )
      {
        // loop over the edges
        std::array<int,2> newEdge;
        newEdge[0] = MIN(nodeMap[(EdgeNodes[edge][0])],nodeMap[(EdgeNodes[edge][1])]);
        newEdge[1] = MAX(nodeMap[(EdgeNodes[edge][0])],nodeMap[(EdgeNodes[edge][1])]);

        edge_list.insert(newEdge);
      }
    }

    /////////////////////////////////////////////
    // EDGE LOCAL MESH CONSTRUCT
    /////////////////////////////////////////////

    const XField_CellToTrace<PhysD2,TopoD2> xfld_connectivity(xfld);
    const Field_NodalView nodalView(xfld, {group});

    typedef typename Field_NodalView::IndexVector IndexVector;

    for ( edge_list_it = edge_list.begin(); edge_list_it != edge_list.end(); ++edge_list_it)
    {
      /////////////////////////////////////////////
      // EDGE LOCAL MESH CONSTRUCT
      /////////////////////////////////////////////

      IndexVector nodeGroup0, nodeGroup1;
      nodeGroup0 = nodalView.getCellList( (*edge_list_it)[0] );
      nodeGroup1 = nodalView.getCellList( (*edge_list_it)[1] );

      // find the interesction of the two sets
      std::set<GroupElemIndex> attachedCells;
      typename std::set<GroupElemIndex>::iterator attachedCells_it;
      std::set_intersection( nodeGroup0.begin(), nodeGroup0.end(), nodeGroup1.begin(), nodeGroup1.end(),
                             std::inserter( attachedCells, attachedCells.begin() ) );

      std::pair<int,int> nodes;
      nodes = std::make_pair((*edge_list_it)[0],(*edge_list_it)[1]);
//      nodes = std::make_pair(2,3);
//      std::cout<< "==================" << std::endl;
//      std::cout<< "nodes = (" << nodes.first << "," << nodes.second <<")" <<std::endl;
//      std::cout<< "==================" << std::endl;

      XField_EdgeLocal<PhysD2,TopoD2> xfld_local( comm_local, xfld_connectivity, nodalView, nodes, Neighborhood::Dual, isSplit );

      /////////////////////////////////////////////
      // LOCAL SOLUTION TRANSFER
      /////////////////////////////////////////////

      Field_Local<QField2D_CG_Area> qfld_local(xfld_local, qfld, order, basis_category);
      Field_Local<QField2D_CG_BTrace> lgfld_local(xfld_local, lgfld, order, basis_category );

      //-------------CHECK LOCAL FIELD----------------
      BOOST_CHECK_EQUAL( &qfld_local.getXField(), &xfld_local );
      BOOST_CHECK_EQUAL( &lgfld_local.getXField(), &xfld_local );

      Quadrature<TopoD2, Triangle> cellQuadrature(order+2);
      const int nCellquad = cellQuadrature.nQuadrature();

      Quadrature<TopoD1, Line> traceQuadrature(order+2);
      const int nTracequad = traceQuadrature.nQuadrature();

      if (!isSplit)
      {
        /////////////////////////////////////////////
        // EDGE LOCAL UNSPLIT CHECK
        /////////////////////////////////////////////

        RefCoordType RefCoord;  // reference-element coordinates
        ArrayQ qeval_main, qeval_local;

        for (int cellGroup = 0; cellGroup < qfld_local.nCellGroups(); cellGroup++)
          for (int elem = 0; elem < xfld_local.getCellGroup<Triangle>(cellGroup).nElem(); elem++)
          {
            std::pair<int,int> globGroupElem = xfld_local.getGlobalCellMap(std::make_pair(cellGroup,elem));

            const FieldCellGroupType& global_cellgrp = qfld.getCellGroup<Triangle>(globGroupElem.first);
            ElementFieldClass fldElem_global( global_cellgrp.basis() );
            global_cellgrp.getElement(fldElem_global, globGroupElem.second);

            const FieldCellGroupType& local_cellgrp = qfld_local.getCellGroup<Triangle>(cellGroup);
            ElementFieldClass fldElem_local( global_cellgrp.basis() );
            local_cellgrp.getElement(fldElem_local, elem);

            //Check if the solutions are equal at a few cellQuadrature points
            for (int iquad = 0; iquad < nCellquad; iquad++)
            {
              cellQuadrature.coordinates( iquad, RefCoord );

              fldElem_local.eval( RefCoord, qeval_local);

              fldElem_global.eval(RefCoord, qeval_main);

              for (int i=0; i<2; i++)
                SANS_CHECK_CLOSE(qeval_local[i], qeval_main[i], small_tol, close_tol);
            }

            if ( !(order >= 3 && basis_category == BasisFunctionCategory_Hierarchical ) )
            {
              // Check if the local dofs are identical
              for (int dof = 0; dof < fldElem_global.nDOF(); dof++)
                for (int i = 0; i < 2; i++)
                  SANS_CHECK_CLOSE( fldElem_global.DOF(dof)[i], fldElem_local.DOF(dof)[i], small_tol, close_tol );
            }

          }

        TraceRefCoordType TRefCoord; // reference-element coordinates in trace
        for (int traceGroup = 0; traceGroup < lgfld_local.nBoundaryTraceGroups(); traceGroup++)
        {
          const int xFieldBGroupGlobal = lgfld_local.getGlobalBoundaryTraceGroupMap( traceGroup ); // matching xfield_local bgroups

          // to get global xfield group
          std::pair<int,int> globGroupTrace = xfld_local.getGlobalBoundaryTraceMap( std::make_pair(xFieldBGroupGlobal,0) );
          const int lgFieldBGroupGlobal = lgfld.getLocalBoundaryTraceGroupMap( globGroupTrace.first ); // The lgfield group matching xfield

          const FieldTraceGroupType& local_tracegrp  = lgfld_local.template getBoundaryTraceGroup<Line>(traceGroup);
          const FieldTraceGroupType& global_tracegrp = lgfld.template getBoundaryTraceGroup<Line>(lgFieldBGroupGlobal);

          for (int trace = 0; trace < local_tracegrp.nElem(); trace++)
          {
            ElementTraceFieldClass fldElem_global( global_tracegrp.basis() );
            ElementTraceFieldClass fldElem_local (  local_tracegrp.basis() );

            global_tracegrp.getElement( fldElem_global, globGroupTrace.second );
            local_tracegrp. getElement( fldElem_local,  trace );

            for (int iquad = 0; iquad < nTracequad; iquad++)
            {
              traceQuadrature.coordinates( iquad, TRefCoord );

              fldElem_local.eval( TRefCoord, qeval_local);
              fldElem_global.eval( TRefCoord, qeval_main);

              for (int i = 0; i < 2; i++)
                SANS_CHECK_CLOSE( qeval_local[i], qeval_main[i], small_tol, close_tol );


            // Check if the local dofs are identical -- can do this because lgfld is always a DG basis.
            for (int dof = 0; dof < fldElem_global.nDOF(); dof++)
              for (int i = 0; i < 2; i++)
                SANS_CHECK_CLOSE( fldElem_global.DOF(dof)[i], fldElem_local.DOF(dof)[i], small_tol, close_tol );
            }
          }
        }
      }
      else if (isSplit)
      {
        /////////////////////////////////////////////
        // EDGE LOCAL SPLIT CHECK FOR CELLGROUP 0
        /////////////////////////////////////////////

        RefCoordType RefCoord_main, RefCoord_sub;  // reference-element coordinates
        ArrayQ qeval_main0, qeval_main1, qeval_local0, qeval_local1;

        // Loop over half the number of cells, because evaluating two at a time
        int cellGroup = 0; // Checking the split elements
        for (int elem = 0; elem < xfld_local.getCellGroup<Triangle>(cellGroup).nElem()/2; elem++)
        {
          std::pair<int,int> globGroupElem = xfld_local.getGlobalCellMap(std::make_pair(cellGroup,elem));

          // Extract the global element
          const FieldCellGroupType& global_cellgrp = qfld.getCellGroup<Triangle>(globGroupElem.first);
          ElementFieldClass fldElem_global( global_cellgrp.basis() );
          global_cellgrp.getElement(fldElem_global, globGroupElem.second);

          // Extract the two local elements
          const FieldCellGroupType& local_cellgrp = qfld_local.getCellGroup<Triangle>(cellGroup);
          ElementFieldClass fldElem_local0( global_cellgrp.basis() ), fldElem_local1( global_cellgrp.basis() );
          local_cellgrp.getElement(fldElem_local0, elem);
          local_cellgrp.getElement(fldElem_local1, elem + xfld_local.getCellGroup<Triangle>(cellGroup).nElem()/2);

          // Figure out the canonical trace of the global element that is getting split
          // Assumes that the xfield and field cellgroup and element numbers match

          std::array<int,3> cellNodeMap;
          std::array<int,2> edgeNodeMap;
          edgeNodeMap[0] = nodes.first;
          edgeNodeMap[1] = nodes.second;

          const XFieldCellGroupType& xfld_global_cellgrp = xfld.getCellGroup<Triangle>(globGroupElem.first);
          xfld_global_cellgrp.associativity(globGroupElem.second)
              .getNodeGlobalMapping( cellNodeMap.data(), cellNodeMap.size() );

          CanonicalTraceToCell canonical = TraceToCellRefCoord<Line, TopoD2, Triangle>
            ::getCanonicalTrace( edgeNodeMap.data(), edgeNodeMap.size(), cellNodeMap.data(), cellNodeMap.size() );

          const int localEdge0 = canonical.orientation > 0 ? 0 : 1;
          const int localEdge1 = canonical.orientation > 0 ? 1 : 0;

          //Check if the solutions are equal at a few cellQuadrature points
          for (int iquad = 0; iquad < nCellquad; iquad++)
          {
            cellQuadrature.coordinates( iquad, RefCoord_sub );

            // coordinates in the local elements
            fldElem_local0.eval( RefCoord_sub, qeval_local0);
            fldElem_local1.eval( RefCoord_sub, qeval_local1);

            BasisFunction_RefElement_Split<TopoD2,Triangle>::
              transform( RefCoord_sub, ElementSplitType::Edge, canonical.trace, localEdge0, RefCoord_main);

            fldElem_global.eval(RefCoord_main, qeval_main0);

            BasisFunction_RefElement_Split<TopoD2,Triangle>::
              transform( RefCoord_sub, ElementSplitType::Edge, canonical.trace, localEdge1, RefCoord_main);

            fldElem_global.eval(RefCoord_main, qeval_main1);

            for (int i=0; i<2; i++)
            {
              SANS_CHECK_CLOSE(qeval_local0[i], qeval_main0[i], small_tol, close_tol);
              SANS_CHECK_CLOSE(qeval_local1[i], qeval_main1[i], small_tol, close_tol);
            }
          }
        }

        TraceRefCoordType TRefCoord_main, TRefCoord_sub; // reference-element coordinates in trace
        for (int traceGroup = 0; traceGroup < lgfld_local.nBoundaryTraceGroups(); traceGroup++)
        {
          const int xFieldBGroupGlobal = lgfld_local.getGlobalBoundaryTraceGroupMap( traceGroup ); // matching xfield_local bgroups

          // to get global xfield group
          std::pair<int,int> globGroupTrace = xfld_local.getGlobalBoundaryTraceMap( std::make_pair(xFieldBGroupGlobal,0) );
          const int lgFieldBGroupGlobal = lgfld.getLocalBoundaryTraceGroupMap( globGroupTrace.first ); // The lgfield group matching xfield

          const FieldTraceGroupType& local_tracegrp  = lgfld_local.template getBoundaryTraceGroup<Line>(traceGroup);
          const FieldTraceGroupType& global_tracegrp = lgfld.template getBoundaryTraceGroup<Line>(lgFieldBGroupGlobal);

          if ( local_tracegrp.nElem() == 2 ) // SPLIT TRACE
          {
            ElementTraceFieldClass fldElem_global( global_tracegrp.basis() );
            ElementTraceFieldClass fldElem_local0(  local_tracegrp.basis() ), fldElem_local1(  local_tracegrp.basis() );

            global_tracegrp.getElement( fldElem_global, globGroupTrace.second );
            local_tracegrp. getElement( fldElem_local0, 0 );
            local_tracegrp. getElement( fldElem_local1, 1 );

            for (int iquad = 0; iquad < nTracequad; iquad++)
            {
              traceQuadrature.coordinates( iquad, TRefCoord_sub );

              // coordinates in the local trace
              fldElem_local0.eval( TRefCoord_sub, qeval_local0 );
              fldElem_local1.eval( TRefCoord_sub, qeval_local1 );

              BasisFunction_RefElement_Split<TopoD1,Line>::
                transform( TRefCoord_sub, ElementSplitType::Isotropic, -1, 0, TRefCoord_main);

              fldElem_global.eval(TRefCoord_main, qeval_main0);

              BasisFunction_RefElement_Split<TopoD1,Line>::
                transform( TRefCoord_sub, ElementSplitType::Isotropic, -1, 1, TRefCoord_main);

              fldElem_global.eval(TRefCoord_main, qeval_main1);

              for (int i = 0; i < 2; i++)
              {
                SANS_CHECK_CLOSE(qeval_local0[i], qeval_main0[i], small_tol, close_tol);
                SANS_CHECK_CLOSE(qeval_local1[i], qeval_main1[i], small_tol, close_tol);
              }
            }
          }
          else if ( local_tracegrp.nElem() == 1 ) // UNSPLIT TRACE
          {
            ElementTraceFieldClass fldElem_global( global_tracegrp.basis() );
            ElementTraceFieldClass fldElem_local (  local_tracegrp.basis() );

            global_tracegrp.getElement( fldElem_global, globGroupTrace.second );
            local_tracegrp. getElement( fldElem_local,  0 );

            for (int iquad = 0; iquad < nTracequad; iquad++)
            {
              traceQuadrature.coordinates( iquad, TRefCoord_main );

              fldElem_local.eval( TRefCoord_main, qeval_local0);
              fldElem_global.eval( TRefCoord_main, qeval_main0);

              for (int i = 0; i < 2; i++)
                SANS_CHECK_CLOSE( qeval_local0[i], qeval_main0[i], small_tol, close_tol );


            // Check if the local dofs are identical -- can do this because lgfld is always a DG basis.
            for (int dof = 0; dof < fldElem_global.nDOF(); dof++)
              for (int i = 0; i < 2; i++)
                SANS_CHECK_CLOSE( fldElem_global.DOF(dof)[i], fldElem_local.DOF(dof)[i], small_tol, close_tol );
            }
          }
          else
            BOOST_CHECK_EQUAL( 1, 0 ); // Shouldn't be possible to get here, btraces should have 1 or 2 elements
        }


        if (xfld_local.nCellGroups() == 2)
        {
          /////////////////////////////////////////////
          // EDGE LOCAL UNSPLIT CHECK FOR CELLGROUP 1
          /////////////////////////////////////////////

          // check the unsplit groups
          cellGroup = 1;

          RefCoordType RefCoord;  // reference-element coordinates
          ArrayQ qeval_main, qeval_local;

          for (int elem = 0; elem < xfld_local.getCellGroup<Triangle>(cellGroup).nElem(); elem++)
          {
            std::pair<int,int> globGroupElem = xfld_local.getGlobalCellMap(std::make_pair(cellGroup,elem));

            const FieldCellGroupType& global_cellgrp = qfld.getCellGroup<Triangle>(globGroupElem.first);
            ElementFieldClass fldElem_global( global_cellgrp.basis() );
            global_cellgrp.getElement(fldElem_global, globGroupElem.second);

            const FieldCellGroupType& local_cellgrp = qfld_local.getCellGroup<Triangle>(cellGroup);
            ElementFieldClass fldElem_local( global_cellgrp.basis() );
            local_cellgrp.getElement(fldElem_local, elem);

            //Check if the solutions are equal at a few cellQuadrature points
            for (int iquad = 0; iquad < nCellquad; iquad++)
            {
              cellQuadrature.coordinates( iquad, RefCoord );

              fldElem_local.eval( RefCoord, qeval_local);

              fldElem_global.eval(RefCoord, qeval_main);

              for (int i=0; i<2; i++)
                SANS_CHECK_CLOSE(qeval_local[i], qeval_main[i], small_tol, close_tol);
            }

            if ( !(order >= 3 && basis_category == BasisFunctionCategory_Hierarchical ) )
            {
              // Check if the local dofs are identical
              for (int dof = 0; dof < fldElem_global.nDOF(); dof++)
                for (int i = 0; i < 2; i++)
                  SANS_CHECK_CLOSE( fldElem_global.DOF(dof)[i], fldElem_local.DOF(dof)[i], small_tol, close_tol );
            }
          }
        }

      }
//      return;
    }
  }

}

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_2Box_Triangle_Hierarchical_P1 )
{
  int ii = 4;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = false;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_2Box_Triangle_Hierarchical_Split_P1 )
{
  int ii = 4;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = true;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_2Box_Triangle_Hierarchical_P2 )
{
  int ii = 4;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 2;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = false;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_2Box_Triangle_Hierarchical_Split_P2 )
{
  int ii = 4;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 2;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = true;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_2Box_Triangle_Hierarchical_P3 )
{
  int ii = 4;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 3;
  const Real small_tol = 1e-9, close_tol = 1e-9;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = false;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_2Box_Triangle_Hierarchical_Split_P3 )
{
  int ii = 4;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 3;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = true;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_2Box_Triangle_Lagrange_P1 )
{
  int ii = 4;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = false;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_2Box_Triangle_Lagrange_Split_P1 )
{
  int ii = 4;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = true;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_2Box_Triangle_Lagrange_P2 )
{
  int ii = 4;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj,0,ii,0,jj);

  int order = 2;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = false;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_2Box_Triangle_Lagrange_Split_P2 )
{
  int ii = 4;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 2;
  const Real small_tol = 5e-10, close_tol = 1e-9;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = true;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_2Box_Triangle_Lagrange_P3 )
{
  int ii = 4;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 3;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = false;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_2Box_Triangle_Lagrange_Split_P3 )
{
  int ii = 4;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 3;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = true;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_3Box_Triangle_Hierarchical_P1 )
{
  int ii = 6;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = false;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_3Box_Triangle_Hierarchical_Split_P1 )
{
  int ii = 6;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = true;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_3Box_Triangle_Hierarchical_P2 )
{
  int ii = 6;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 2;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = false;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_3Box_Triangle_Hierarchical_Split_P2 )
{
  int ii = 6;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 2;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = true;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_3Box_Triangle_Hierarchical_P3 )
{
  int ii = 6;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 3;
  const Real small_tol = 1e-9, close_tol = 1e-9;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = false;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_3Box_Triangle_Hierarchical_Split_P3 )
{
  int ii = 6;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 3;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = true;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_3Box_Triangle_Lagrange_P1 )
{
  int ii = 6;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = false;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_3Box_Triangle_Lagrange_Split_P1 )
{
  int ii = 6;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = true;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_3Box_Triangle_Lagrange_P2 )
{
  int ii = 6;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj,0,ii,0,jj);

  int order = 2;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = false;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_3Box_Triangle_Lagrange_Split_P2 )
{
  int ii = 6;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 2;
  const Real small_tol = 5e-10, close_tol = 1e-9;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = true;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_3Box_Triangle_Lagrange_P3 )
{
  int ii = 6;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 3;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = false;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( EdgeLocalSolve2D_Galerkin_AD_3Box_Triangle_Lagrange_Split_P3 )
{
  int ii = 6;
  int jj = ii;
  XField2D_Box_Triangle_X1 xfld(ii,jj);

  int order = 3;
  const Real small_tol = 2e-10, close_tol = 2e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = true;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
