// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// XField3D_Local_Split_Linear_IsotropicSplit_btest
// testing of XField3D_Local_Split_Linear
//
// Note: unit grids tested in "unit/UnitGrids/UnitGrids_btest.cpp"

#include <ostream>
#include <utility> // std::pair

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "Topology/ElementTopology.h"

#include "Field/XFieldVolume_Local_Split.h"

#include "unit/UnitGrids/XField3D_2Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_5Tet_X1_1Group_AllOrientations.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( XField3D_Local_Split_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_Local_2Tet_X1_1Group_RightCell_Target_IsotropicSplit_test )
{
  typedef std::array<int,4> Int4;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  Real tol = 1e-12;

  XField3D_2Tet_X1_1Group xfld;

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  //Extract the local mesh for the right tet
  XField_Local<PhysD3,TopoD3> xfld_local(comm_local, connectivity,0,0);

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity_local(xfld_local);

  int split_edge_index = -1;
  XField_Local_Split<PhysD3,TopoD3> xfld_split_local(xfld_local, connectivity_local,
                                                     ElementSplitType::Isotropic, split_edge_index);

#if 0
  xfld_local.dump(3,std::cout);
  std::cout<<std::endl;
  xfld_split_local.dump(3,std::cout);
#endif

  BOOST_CHECK_EQUAL( &connectivity, &xfld_split_local.getConnectivity() );

  //Check local to global mappings
  BOOST_CHECK( xfld_split_local.getGlobalCellMap({0,0}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_split_local.getGlobalCellMap({0,1}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_split_local.getGlobalCellMap({0,2}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_split_local.getGlobalCellMap({0,3}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_split_local.getGlobalCellMap({0,4}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_split_local.getGlobalCellMap({0,5}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_split_local.getGlobalCellMap({0,6}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_split_local.getGlobalCellMap({0,7}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_split_local.getGlobalCellMap({1,0}) == std::make_pair(0,1));
  BOOST_CHECK( xfld_split_local.getGlobalCellMap({1,1}) == std::make_pair(0,1));
  BOOST_CHECK( xfld_split_local.getGlobalCellMap({1,2}) == std::make_pair(0,1));
  BOOST_CHECK( xfld_split_local.getGlobalCellMap({1,3}) == std::make_pair(0,1));

  BOOST_CHECK( xfld_split_local.getGlobalInteriorTraceMap({0,0}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_split_local.getGlobalInteriorTraceMap({0,1}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_split_local.getGlobalInteriorTraceMap({0,2}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_split_local.getGlobalInteriorTraceMap({0,3}) == std::make_pair(0,0));

  BOOST_CHECK( xfld_split_local.getGlobalBoundaryTraceMap({0,0}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_split_local.getGlobalBoundaryTraceMap({0,1}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_split_local.getGlobalBoundaryTraceMap({0,2}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_split_local.getGlobalBoundaryTraceMap({0,3}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_split_local.getGlobalBoundaryTraceMap({1,0}) == std::make_pair(1,0));
  BOOST_CHECK( xfld_split_local.getGlobalBoundaryTraceMap({1,1}) == std::make_pair(1,0));
  BOOST_CHECK( xfld_split_local.getGlobalBoundaryTraceMap({1,2}) == std::make_pair(1,0));
  BOOST_CHECK( xfld_split_local.getGlobalBoundaryTraceMap({1,3}) == std::make_pair(1,0));
  BOOST_CHECK( xfld_split_local.getGlobalBoundaryTraceMap({2,0}) == std::make_pair(2,0));
  BOOST_CHECK( xfld_split_local.getGlobalBoundaryTraceMap({2,1}) == std::make_pair(2,0));
  BOOST_CHECK( xfld_split_local.getGlobalBoundaryTraceMap({2,2}) == std::make_pair(2,0));
  BOOST_CHECK( xfld_split_local.getGlobalBoundaryTraceMap({2,3}) == std::make_pair(2,0));

  BOOST_CHECK_EQUAL( xfld_split_local.nDOF(), 11 );

  BOOST_CHECK_EQUAL( xfld_split_local.getNewNodeDOFs().size(), 6 );
  BOOST_CHECK_EQUAL( xfld_split_local.getNewNodeDOFs()[0], 4 );
  BOOST_CHECK_EQUAL( xfld_split_local.getNewNodeDOFs()[1], 5 );
  BOOST_CHECK_EQUAL( xfld_split_local.getNewNodeDOFs()[2], 6 );
  BOOST_CHECK_EQUAL( xfld_split_local.getNewNodeDOFs()[3], 7 );
  BOOST_CHECK_EQUAL( xfld_split_local.getNewNodeDOFs()[4], 8 );
  BOOST_CHECK_EQUAL( xfld_split_local.getNewNodeDOFs()[5], 9 );

  Real vertices[11][3] = {{0.0,0.0,0.0},
                          {1.0,0.0,0.0},
                          {0.0,1.0,0.0},
                          {0.0,0.0,1.0},
                          {0.0,0.5,0.5},
                          {0.5,0.0,0.5},
                          {0.5,0.5,0.0},
                          {0.0,0.5,0.0},
                          {0.0,0.0,0.5},
                          {0.5,0.0,0.0},
                          {-1.0,0.0,0.0}};

  //Check the node values
  for (int k=0; k<xfld_split_local.nDOF(); k++)
    for (int d=0; d<3; d++)
      SANS_CHECK_CLOSE( xfld_split_local.DOF(k)[d], vertices[k][d], tol, tol );

  // volume field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_split_local.getCellGroupBase(0).topoTypeID() == typeid(Tet) );
  BOOST_REQUIRE( xfld_split_local.getCellGroupBase(1).topoTypeID() == typeid(Tet) );

  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup0 = xfld_split_local.getCellGroup<Tet>(0);
  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup1 = xfld_split_local.getCellGroup<Tet>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 8 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 4 );

  int nodeMap[4];

  //Node DOFs
  xfldCellGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 7 );
  BOOST_CHECK_EQUAL( nodeMap[1], 6 );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 );
  BOOST_CHECK_EQUAL( nodeMap[3], 4 );

  xfldCellGroup0.associativity(1).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 8 );
  BOOST_CHECK_EQUAL( nodeMap[1], 5 );
  BOOST_CHECK_EQUAL( nodeMap[2], 4 );
  BOOST_CHECK_EQUAL( nodeMap[3], 3 );

  xfldCellGroup0.associativity(2).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 9 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );
  BOOST_CHECK_EQUAL( nodeMap[2], 6 );
  BOOST_CHECK_EQUAL( nodeMap[3], 5 );

  xfldCellGroup0.associativity(3).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 9 );
  BOOST_CHECK_EQUAL( nodeMap[2], 7 );
  BOOST_CHECK_EQUAL( nodeMap[3], 8 );

  xfldCellGroup0.associativity(4).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 7 );
  BOOST_CHECK_EQUAL( nodeMap[1], 6 );
  BOOST_CHECK_EQUAL( nodeMap[2], 4 );
  BOOST_CHECK_EQUAL( nodeMap[3], 5 );

  xfldCellGroup0.associativity(5).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 8 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );
  BOOST_CHECK_EQUAL( nodeMap[2], 5 );
  BOOST_CHECK_EQUAL( nodeMap[3], 7 );

  xfldCellGroup0.associativity(6).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 9 );
  BOOST_CHECK_EQUAL( nodeMap[1], 5 );
  BOOST_CHECK_EQUAL( nodeMap[2], 6 );
  BOOST_CHECK_EQUAL( nodeMap[3], 7 );

  xfldCellGroup0.associativity(7).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 9 );
  BOOST_CHECK_EQUAL( nodeMap[1], 7 );
  BOOST_CHECK_EQUAL( nodeMap[2], 8 );
  BOOST_CHECK_EQUAL( nodeMap[3], 5 );

  xfldCellGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0],10 );
  BOOST_CHECK_EQUAL( nodeMap[1], 8 );
  BOOST_CHECK_EQUAL( nodeMap[2], 4 );
  BOOST_CHECK_EQUAL( nodeMap[3], 3 );

  xfldCellGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0],10 );
  BOOST_CHECK_EQUAL( nodeMap[1], 7 );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 );
  BOOST_CHECK_EQUAL( nodeMap[3], 4 );

  xfldCellGroup1.associativity(2).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0],10 );
  BOOST_CHECK_EQUAL( nodeMap[1], 0 );
  BOOST_CHECK_EQUAL( nodeMap[2], 7 );
  BOOST_CHECK_EQUAL( nodeMap[3], 8 );

  xfldCellGroup1.associativity(3).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0],10 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );
  BOOST_CHECK_EQUAL( nodeMap[2], 8 );
  BOOST_CHECK_EQUAL( nodeMap[3], 7 );


  Int4 faceSign;

  faceSign = xfldCellGroup0.associativity(0).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], +1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], +1 );

  faceSign = xfldCellGroup0.associativity(1).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], +1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], +1 );

  faceSign = xfldCellGroup0.associativity(2).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], +1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], +1 );

  faceSign = xfldCellGroup0.associativity(3).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], +1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], +1 );

  faceSign = xfldCellGroup0.associativity(4).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], +1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], -1 );

  faceSign = xfldCellGroup0.associativity(5).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], -3 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], -1 );

  faceSign = xfldCellGroup0.associativity(6).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], -3 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], -1 );

  faceSign = xfldCellGroup0.associativity(7).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], -2 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], -1 );
  BOOST_CHECK_EQUAL( faceSign[3], -1 );

  faceSign = xfldCellGroup1.associativity(0).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], -1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], +1 );

  faceSign = xfldCellGroup1.associativity(1).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], -1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], +1 );

  faceSign = xfldCellGroup1.associativity(2).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], -1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], +1 );

  faceSign = xfldCellGroup1.associativity(3).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], -2 );
  BOOST_CHECK_EQUAL( faceSign[1], -1 );
  BOOST_CHECK_EQUAL( faceSign[2], -1 );
  BOOST_CHECK_EQUAL( faceSign[3], -1 );


  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nInteriorTraceGroups(), 3 );

  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Triangle>(1).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Triangle>(2).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup0 = xfld_split_local.getInteriorTraceGroup<Triangle>(0);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup1 = xfld_split_local.getInteriorTraceGroup<Triangle>(1);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup2 = xfld_split_local.getInteriorTraceGroup<Triangle>(2);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 4 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 8 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.nElem(), 3 );

  //Node DOFs
  xfldITraceGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 8 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );
  BOOST_CHECK_EQUAL( nodeMap[2], 4 );

  xfldITraceGroup0.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 7 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 );

  xfldITraceGroup0.associativity(2).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 8 );
  BOOST_CHECK_EQUAL( nodeMap[2], 7 );

  xfldITraceGroup0.associativity(3).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 8 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );
  BOOST_CHECK_EQUAL( nodeMap[2], 7 );

  xfldITraceGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 7 );
  BOOST_CHECK_EQUAL( nodeMap[1], 6 );
  BOOST_CHECK_EQUAL( nodeMap[2], 4 );

  xfldITraceGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 8 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );
  BOOST_CHECK_EQUAL( nodeMap[2], 5 );

  xfldITraceGroup1.associativity(2).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 9 );
  BOOST_CHECK_EQUAL( nodeMap[1], 5 );
  BOOST_CHECK_EQUAL( nodeMap[2], 6 );

  xfldITraceGroup1.associativity(3).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 9 );
  BOOST_CHECK_EQUAL( nodeMap[1], 7 );
  BOOST_CHECK_EQUAL( nodeMap[2], 8 );

  xfldITraceGroup1.associativity(4).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 7 );
  BOOST_CHECK_EQUAL( nodeMap[1], 5 );
  BOOST_CHECK_EQUAL( nodeMap[2], 4 );

  xfldITraceGroup1.associativity(5).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 7 );
  BOOST_CHECK_EQUAL( nodeMap[1], 6 );
  BOOST_CHECK_EQUAL( nodeMap[2], 5 );

  xfldITraceGroup1.associativity(6).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 8 );
  BOOST_CHECK_EQUAL( nodeMap[1], 7 );
  BOOST_CHECK_EQUAL( nodeMap[2], 5 );

  xfldITraceGroup1.associativity(7).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 9 );
  BOOST_CHECK_EQUAL( nodeMap[1], 5 );
  BOOST_CHECK_EQUAL( nodeMap[2], 7 );

  xfldITraceGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0],10 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );
  BOOST_CHECK_EQUAL( nodeMap[2], 8 );

  xfldITraceGroup2.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0],10 );
  BOOST_CHECK_EQUAL( nodeMap[1], 7 );
  BOOST_CHECK_EQUAL( nodeMap[2], 4 );

  xfldITraceGroup2.associativity(2).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0],10 );
  BOOST_CHECK_EQUAL( nodeMap[1], 8 );
  BOOST_CHECK_EQUAL( nodeMap[2], 7 );


  // interior edge-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getGroupLeft() , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getGroupRight(), 1 );

  BOOST_CHECK_EQUAL( xfldITraceGroup1.getGroupLeft() , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getGroupRight(), 0 );

  BOOST_CHECK_EQUAL( xfldITraceGroup2.getGroupLeft() , 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.getGroupRight(), 1 );

  int trace_elem = 0;
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementLeft(trace_elem) , 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementRight(trace_elem), 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(trace_elem) .trace      , 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(trace_elem) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(trace_elem).trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(trace_elem).orientation,-1 );

  trace_elem = 1;
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementLeft(trace_elem) , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementRight(trace_elem), 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(trace_elem) .trace      , 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(trace_elem) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(trace_elem).trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(trace_elem).orientation,-1 );

  trace_elem = 2;
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementLeft(trace_elem) , 3 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementRight(trace_elem), 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(trace_elem) .trace      , 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(trace_elem) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(trace_elem).trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(trace_elem).orientation,-1 );

  trace_elem = 3;
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementLeft(trace_elem) , 5 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementRight(trace_elem), 3 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(trace_elem) .trace      , 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(trace_elem) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(trace_elem).trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(trace_elem).orientation,-2 );

  trace_elem = 0;
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getElementLeft(trace_elem) , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getElementRight(trace_elem), 4 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceLeft(trace_elem) .trace      , 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceLeft(trace_elem) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceRight(trace_elem).trace      , 3 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceRight(trace_elem).orientation,-1 );

  trace_elem = 1;
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getElementLeft(trace_elem) , 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getElementRight(trace_elem), 5 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceLeft(trace_elem) .trace      , 3 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceLeft(trace_elem) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceRight(trace_elem).trace      , 3 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceRight(trace_elem).orientation,-1 );

  trace_elem = 2;
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getElementLeft(trace_elem) , 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getElementRight(trace_elem), 6 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceLeft(trace_elem) .trace      , 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceLeft(trace_elem) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceRight(trace_elem).trace      , 3 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceRight(trace_elem).orientation,-1 );

  trace_elem = 3;
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getElementLeft(trace_elem) , 3 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getElementRight(trace_elem), 7 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceLeft(trace_elem) .trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceLeft(trace_elem) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceRight(trace_elem).trace      , 3 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceRight(trace_elem).orientation,-1 );

  trace_elem = 4;
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getElementLeft(trace_elem) , 4 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getElementRight(trace_elem), 5 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceLeft(trace_elem) .trace      , 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceLeft(trace_elem) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceRight(trace_elem).trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceRight(trace_elem).orientation,-3 );

  trace_elem = 5;
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getElementLeft(trace_elem) , 4 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getElementRight(trace_elem), 6 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceLeft(trace_elem) .trace      , 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceLeft(trace_elem) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceRight(trace_elem).trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceRight(trace_elem).orientation,-3 );

  trace_elem = 6;
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getElementLeft(trace_elem) , 5 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getElementRight(trace_elem), 7 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceLeft(trace_elem) .trace      , 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceLeft(trace_elem) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceRight(trace_elem).trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceRight(trace_elem).orientation,-2 );

  trace_elem = 7;
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getElementLeft(trace_elem) , 6 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getElementRight(trace_elem), 7 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceLeft(trace_elem) .trace      , 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceLeft(trace_elem) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceRight(trace_elem).trace      , 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceRight(trace_elem).orientation,-1 );

  trace_elem = 0;
  BOOST_CHECK_EQUAL( xfldITraceGroup2.getElementLeft(trace_elem) , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.getElementRight(trace_elem), 3 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.getCanonicalTraceLeft(trace_elem) .trace      , 3 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.getCanonicalTraceLeft(trace_elem) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.getCanonicalTraceRight(trace_elem).trace      , 3 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.getCanonicalTraceRight(trace_elem).orientation,-1 );

  trace_elem = 1;
  BOOST_CHECK_EQUAL( xfldITraceGroup2.getElementLeft(trace_elem) , 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.getElementRight(trace_elem), 3 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.getCanonicalTraceLeft(trace_elem) .trace      , 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.getCanonicalTraceLeft(trace_elem) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.getCanonicalTraceRight(trace_elem).trace      , 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.getCanonicalTraceRight(trace_elem).orientation,-1 );

  trace_elem = 2;
  BOOST_CHECK_EQUAL( xfldITraceGroup2.getElementLeft(trace_elem) , 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.getElementRight(trace_elem), 3 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.getCanonicalTraceLeft(trace_elem) .trace      , 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.getCanonicalTraceLeft(trace_elem) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.getCanonicalTraceRight(trace_elem).trace      , 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.getCanonicalTraceRight(trace_elem).orientation,-1 );


  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nBoundaryTraceGroups(), 3 );

  BOOST_REQUIRE( xfld_split_local.getBoundaryTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_split_local.getBoundaryTraceGroup<Triangle>(1).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_split_local.getBoundaryTraceGroup<Triangle>(2).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup0 = xfld_split_local.getBoundaryTraceGroup<Triangle>(0);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup1 = xfld_split_local.getBoundaryTraceGroup<Triangle>(1);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup2 = xfld_split_local.getBoundaryTraceGroup<Triangle>(2);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 4 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.nElem(), 4 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.nElem(), 4 );

  //Node DOFs
  xfldBTraceGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 6 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );
  BOOST_CHECK_EQUAL( nodeMap[2], 4 );

  xfldBTraceGroup0.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 5 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );
  BOOST_CHECK_EQUAL( nodeMap[2], 3 );

  xfldBTraceGroup0.associativity(2).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 6 );
  BOOST_CHECK_EQUAL( nodeMap[2], 5 );

  xfldBTraceGroup0.associativity(3).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 6 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );
  BOOST_CHECK_EQUAL( nodeMap[2], 5 );

  xfldBTraceGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 9 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );
  BOOST_CHECK_EQUAL( nodeMap[2], 5 );

  xfldBTraceGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 8 );
  BOOST_CHECK_EQUAL( nodeMap[1], 5 );
  BOOST_CHECK_EQUAL( nodeMap[2], 3 );

  xfldBTraceGroup1.associativity(2).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 9 );
  BOOST_CHECK_EQUAL( nodeMap[2], 8 );

  xfldBTraceGroup1.associativity(3).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 9 );
  BOOST_CHECK_EQUAL( nodeMap[1], 5 );
  BOOST_CHECK_EQUAL( nodeMap[2], 8 );

  xfldBTraceGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 7 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );
  BOOST_CHECK_EQUAL( nodeMap[2], 6 );

  xfldBTraceGroup2.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 9 );
  BOOST_CHECK_EQUAL( nodeMap[1], 6 );
  BOOST_CHECK_EQUAL( nodeMap[2], 1 );

  xfldBTraceGroup2.associativity(2).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 7 );
  BOOST_CHECK_EQUAL( nodeMap[2], 9 );

  xfldBTraceGroup2.associativity(3).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 9 );
  BOOST_CHECK_EQUAL( nodeMap[1], 7 );
  BOOST_CHECK_EQUAL( nodeMap[2], 6 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getGroupLeft() , 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getGroupRight(),-1 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getGroupLeft() , 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getGroupRight(),-1 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getGroupLeft() , 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getGroupRight(),-1 );

  trace_elem = 0;
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementLeft(trace_elem), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(trace_elem).trace, 0 );

  trace_elem = 1;
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementLeft(trace_elem), 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(trace_elem).trace, 0 );

  trace_elem = 2;
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementLeft(trace_elem), 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(trace_elem).trace, 0 );

  trace_elem = 3;
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementLeft(trace_elem), 4 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(trace_elem).trace, 0 );

  trace_elem = 0;
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getElementLeft(trace_elem), 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceLeft(trace_elem).trace, 2 );

  trace_elem = 1;
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getElementLeft(trace_elem), 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceLeft(trace_elem).trace, 2 );

  trace_elem = 2;
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getElementLeft(trace_elem), 3 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceLeft(trace_elem).trace, 2 );

  trace_elem = 3;
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getElementLeft(trace_elem), 7 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceLeft(trace_elem).trace, 1 );

  trace_elem = 0;
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getElementLeft(trace_elem), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getCanonicalTraceLeft(trace_elem).trace, 3 );

  trace_elem = 1;
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getElementLeft(trace_elem), 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getCanonicalTraceLeft(trace_elem).trace, 3 );

  trace_elem = 2;
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getElementLeft(trace_elem), 3 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getCanonicalTraceLeft(trace_elem).trace, 3 );

  trace_elem = 3;
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getElementLeft(trace_elem), 6 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getCanonicalTraceLeft(trace_elem).trace, 1 );

  for (int trace_elem=0; trace_elem<4; trace_elem++)
  {
    BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementRight(trace_elem),-1 );
    BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(trace_elem) .orientation, 1 );
    BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceRight(trace_elem).trace      ,-1 );
    BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceRight(trace_elem).orientation, 0 );

    BOOST_CHECK_EQUAL( xfldBTraceGroup1.getElementRight(trace_elem),-1 );
    BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceLeft(trace_elem) .orientation, 1 );
    BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceRight(trace_elem).trace      ,-1 );
    BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceRight(trace_elem).orientation, 0 );

    BOOST_CHECK_EQUAL( xfldBTraceGroup2.getElementRight(trace_elem),-1 );
    BOOST_CHECK_EQUAL( xfldBTraceGroup2.getCanonicalTraceLeft(trace_elem) .orientation, 1 );
    BOOST_CHECK_EQUAL( xfldBTraceGroup2.getCanonicalTraceRight(trace_elem).trace      ,-1 );
    BOOST_CHECK_EQUAL( xfldBTraceGroup2.getCanonicalTraceRight(trace_elem).orientation, 0 );
  }


  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nGhostBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_split_local.getGhostBoundaryTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldGBTraceGroup0 = xfld_split_local.getGhostBoundaryTraceGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 6 );

  //Node DOFs
  xfldGBTraceGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0],10 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );
  BOOST_CHECK_EQUAL( nodeMap[2], 4 );

  xfldGBTraceGroup0.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0],10 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 );

  xfldGBTraceGroup0.associativity(2).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0],10 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );
  BOOST_CHECK_EQUAL( nodeMap[2], 7 );

  xfldGBTraceGroup0.associativity(3).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0],10 );
  BOOST_CHECK_EQUAL( nodeMap[1], 7 );
  BOOST_CHECK_EQUAL( nodeMap[2], 0 );

  xfldGBTraceGroup0.associativity(4).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0],10 );
  BOOST_CHECK_EQUAL( nodeMap[1], 0 );
  BOOST_CHECK_EQUAL( nodeMap[2], 8 );

  xfldGBTraceGroup0.associativity(5).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0],10 );
  BOOST_CHECK_EQUAL( nodeMap[1], 8 );
  BOOST_CHECK_EQUAL( nodeMap[2], 3 );

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getGroupLeft() , 1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getGroupRight(),-1 );

  trace_elem = 0;
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getElementLeft(trace_elem), 0 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceLeft(trace_elem).trace, 1 );

  trace_elem = 1;
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getElementLeft(trace_elem), 1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceLeft(trace_elem).trace, 1 );

  trace_elem = 2;
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getElementLeft(trace_elem), 1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceLeft(trace_elem).trace, 3 );

  trace_elem = 3;
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getElementLeft(trace_elem), 2 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceLeft(trace_elem).trace, 3 );

  trace_elem = 4;
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getElementLeft(trace_elem), 2 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceLeft(trace_elem).trace, 2 );

  trace_elem = 5;
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getElementLeft(trace_elem), 0 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceLeft(trace_elem).trace, 2 );

  for (int trace_elem=0; trace_elem<6; trace_elem++)
  {
    BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getElementRight(trace_elem),-1 );
    BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceLeft(trace_elem) .orientation, 1 );
    BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceRight(trace_elem).trace      ,-1 );
    BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceRight(trace_elem).orientation, 0 );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_Local_2Tet_X1_1Group_LeftCell_Target_IsotropicSplit_test )
{
  typedef std::array<int,4> Int4;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  Real tol = 1e-12;

  XField3D_2Tet_X1_1Group xfld;

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  //Extract the local mesh for the left tet
  XField_Local<PhysD3,TopoD3> xfld_local(comm_local, connectivity,0,1);

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity_local(xfld_local);

  int split_edge_index = -1;
  XField_Local_Split<PhysD3,TopoD3> xfld_split_local(xfld_local, connectivity_local,
                                                     ElementSplitType::Isotropic, split_edge_index);

#if 0
  xfld_local.dump(3,std::cout);
  std::cout<<std::endl;
  xfld_split_local.dump(3,std::cout);
#endif

  BOOST_CHECK_EQUAL( &connectivity, &xfld_split_local.getConnectivity() );

  //Check local to global mappings
  BOOST_CHECK( xfld_split_local.getGlobalCellMap({0,0}) == std::make_pair(0,1));
  BOOST_CHECK( xfld_split_local.getGlobalCellMap({0,1}) == std::make_pair(0,1));
  BOOST_CHECK( xfld_split_local.getGlobalCellMap({0,2}) == std::make_pair(0,1));
  BOOST_CHECK( xfld_split_local.getGlobalCellMap({0,3}) == std::make_pair(0,1));
  BOOST_CHECK( xfld_split_local.getGlobalCellMap({0,4}) == std::make_pair(0,1));
  BOOST_CHECK( xfld_split_local.getGlobalCellMap({0,5}) == std::make_pair(0,1));
  BOOST_CHECK( xfld_split_local.getGlobalCellMap({0,6}) == std::make_pair(0,1));
  BOOST_CHECK( xfld_split_local.getGlobalCellMap({0,7}) == std::make_pair(0,1));
  BOOST_CHECK( xfld_split_local.getGlobalCellMap({1,0}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_split_local.getGlobalCellMap({1,1}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_split_local.getGlobalCellMap({1,2}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_split_local.getGlobalCellMap({1,3}) == std::make_pair(0,0));

  BOOST_CHECK( xfld_split_local.getGlobalInteriorTraceMap({0,0}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_split_local.getGlobalInteriorTraceMap({0,1}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_split_local.getGlobalInteriorTraceMap({0,2}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_split_local.getGlobalInteriorTraceMap({0,3}) == std::make_pair(0,0));

  BOOST_CHECK( xfld_split_local.getGlobalBoundaryTraceMap({0,0}) == std::make_pair(3,0));
  BOOST_CHECK( xfld_split_local.getGlobalBoundaryTraceMap({0,1}) == std::make_pair(3,0));
  BOOST_CHECK( xfld_split_local.getGlobalBoundaryTraceMap({0,2}) == std::make_pair(3,0));
  BOOST_CHECK( xfld_split_local.getGlobalBoundaryTraceMap({0,3}) == std::make_pair(3,0));
  BOOST_CHECK( xfld_split_local.getGlobalBoundaryTraceMap({1,0}) == std::make_pair(4,0));
  BOOST_CHECK( xfld_split_local.getGlobalBoundaryTraceMap({1,1}) == std::make_pair(4,0));
  BOOST_CHECK( xfld_split_local.getGlobalBoundaryTraceMap({1,2}) == std::make_pair(4,0));
  BOOST_CHECK( xfld_split_local.getGlobalBoundaryTraceMap({1,3}) == std::make_pair(4,0));
  BOOST_CHECK( xfld_split_local.getGlobalBoundaryTraceMap({2,0}) == std::make_pair(5,0));
  BOOST_CHECK( xfld_split_local.getGlobalBoundaryTraceMap({2,1}) == std::make_pair(5,0));
  BOOST_CHECK( xfld_split_local.getGlobalBoundaryTraceMap({2,2}) == std::make_pair(5,0));
  BOOST_CHECK( xfld_split_local.getGlobalBoundaryTraceMap({2,3}) == std::make_pair(5,0));

  BOOST_CHECK_EQUAL( xfld_split_local.nDOF(), 11 );

  BOOST_CHECK_EQUAL( xfld_split_local.getNewNodeDOFs().size(), 6 );
  BOOST_CHECK_EQUAL( xfld_split_local.getNewNodeDOFs()[0], 4 );
  BOOST_CHECK_EQUAL( xfld_split_local.getNewNodeDOFs()[1], 5 );
  BOOST_CHECK_EQUAL( xfld_split_local.getNewNodeDOFs()[2], 6 );
  BOOST_CHECK_EQUAL( xfld_split_local.getNewNodeDOFs()[3], 7 );
  BOOST_CHECK_EQUAL( xfld_split_local.getNewNodeDOFs()[4], 8 );
  BOOST_CHECK_EQUAL( xfld_split_local.getNewNodeDOFs()[5], 9 );

  Real vertices[11][3] = {{-1.0,0.0,0.0},
                          {0.0,0.0,0.0},
                          {0.0,1.0,0.0},
                          {0.0,0.0,1.0},
                          {0.0,0.5,0.5},
                          {0.0,0.0,0.5},
                          {0.0,0.5,0.0},
                          {-0.5,0.5,0.0},
                          {-0.5,0.0,0.5},
                          {-0.5,0.0,0.0},
                          {1.0,0.0,0.0}};

  //Check the node values
  for (int k=0; k<xfld_split_local.nDOF(); k++)
    for (int d=0; d<3; d++)
      SANS_CHECK_CLOSE( xfld_split_local.DOF(k)[d], vertices[k][d], tol, tol );

  // volume field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_split_local.getCellGroupBase(0).topoTypeID() == typeid(Tet) );
  BOOST_REQUIRE( xfld_split_local.getCellGroupBase(1).topoTypeID() == typeid(Tet) );

  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup0 = xfld_split_local.getCellGroup<Tet>(0);
  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup1 = xfld_split_local.getCellGroup<Tet>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 8 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 4 );

  int nodeMap[4];

  //Node DOFs
  xfldCellGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 7 );
  BOOST_CHECK_EQUAL( nodeMap[1], 6 );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 );
  BOOST_CHECK_EQUAL( nodeMap[3], 4 );

  xfldCellGroup0.associativity(1).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 8 );
  BOOST_CHECK_EQUAL( nodeMap[1], 5 );
  BOOST_CHECK_EQUAL( nodeMap[2], 4 );
  BOOST_CHECK_EQUAL( nodeMap[3], 3 );

  xfldCellGroup0.associativity(2).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 9 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );
  BOOST_CHECK_EQUAL( nodeMap[2], 6 );
  BOOST_CHECK_EQUAL( nodeMap[3], 5 );

  xfldCellGroup0.associativity(3).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 9 );
  BOOST_CHECK_EQUAL( nodeMap[2], 7 );
  BOOST_CHECK_EQUAL( nodeMap[3], 8 );

  xfldCellGroup0.associativity(4).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 7 );
  BOOST_CHECK_EQUAL( nodeMap[1], 6 );
  BOOST_CHECK_EQUAL( nodeMap[2], 4 );
  BOOST_CHECK_EQUAL( nodeMap[3], 5 );

  xfldCellGroup0.associativity(5).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 8 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );
  BOOST_CHECK_EQUAL( nodeMap[2], 5 );
  BOOST_CHECK_EQUAL( nodeMap[3], 7 );

  xfldCellGroup0.associativity(6).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 9 );
  BOOST_CHECK_EQUAL( nodeMap[1], 5 );
  BOOST_CHECK_EQUAL( nodeMap[2], 6 );
  BOOST_CHECK_EQUAL( nodeMap[3], 7 );

  xfldCellGroup0.associativity(7).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 9 );
  BOOST_CHECK_EQUAL( nodeMap[1], 7 );
  BOOST_CHECK_EQUAL( nodeMap[2], 8 );
  BOOST_CHECK_EQUAL( nodeMap[3], 5 );

  xfldCellGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 6 );
  BOOST_CHECK_EQUAL( nodeMap[1],10 );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 );
  BOOST_CHECK_EQUAL( nodeMap[3], 4 );

  xfldCellGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 5 );
  BOOST_CHECK_EQUAL( nodeMap[1],10 );
  BOOST_CHECK_EQUAL( nodeMap[2], 4 );
  BOOST_CHECK_EQUAL( nodeMap[3], 3 );

  xfldCellGroup1.associativity(2).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1],10 );
  BOOST_CHECK_EQUAL( nodeMap[2], 6 );
  BOOST_CHECK_EQUAL( nodeMap[3], 5 );

  xfldCellGroup1.associativity(3).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 4 );
  BOOST_CHECK_EQUAL( nodeMap[1],10 );
  BOOST_CHECK_EQUAL( nodeMap[2], 5 );
  BOOST_CHECK_EQUAL( nodeMap[3], 6 );


  Int4 faceSign;

  faceSign = xfldCellGroup0.associativity(0).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], +1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], +1 );

  faceSign = xfldCellGroup0.associativity(1).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], +1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], +1 );

  faceSign = xfldCellGroup0.associativity(2).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], +1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], +1 );

  faceSign = xfldCellGroup0.associativity(3).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], +1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], +1 );

  faceSign = xfldCellGroup0.associativity(4).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], +1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], -1 );

  faceSign = xfldCellGroup0.associativity(5).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], -3 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], -1 );

  faceSign = xfldCellGroup0.associativity(6).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], -3 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], -1 );

  faceSign = xfldCellGroup0.associativity(7).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], -2 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], -1 );
  BOOST_CHECK_EQUAL( faceSign[3], -1 );

  faceSign = xfldCellGroup1.associativity(0).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], +1 );
  BOOST_CHECK_EQUAL( faceSign[1], -1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], +1 );

  faceSign = xfldCellGroup1.associativity(1).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], +1 );
  BOOST_CHECK_EQUAL( faceSign[1], -1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], +1 );

  faceSign = xfldCellGroup1.associativity(2).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], +1 );
  BOOST_CHECK_EQUAL( faceSign[1], -1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], +1 );

  faceSign = xfldCellGroup1.associativity(3).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], -1 );
  BOOST_CHECK_EQUAL( faceSign[1], -2 );
  BOOST_CHECK_EQUAL( faceSign[2], -3 );
  BOOST_CHECK_EQUAL( faceSign[3], -2 );


  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nInteriorTraceGroups(), 3 );

  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Triangle>(1).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Triangle>(2).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup0 = xfld_split_local.getInteriorTraceGroup<Triangle>(0);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup1 = xfld_split_local.getInteriorTraceGroup<Triangle>(1);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup2 = xfld_split_local.getInteriorTraceGroup<Triangle>(2);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 4 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 8 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.nElem(), 3 );

  //Node DOFs
  xfldITraceGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 6 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );
  BOOST_CHECK_EQUAL( nodeMap[2], 4 );

  xfldITraceGroup0.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 5 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );
  BOOST_CHECK_EQUAL( nodeMap[2], 3 );

  xfldITraceGroup0.associativity(2).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 6 );
  BOOST_CHECK_EQUAL( nodeMap[2], 5 );

  xfldITraceGroup0.associativity(3).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 6 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );
  BOOST_CHECK_EQUAL( nodeMap[2], 5 );

  xfldITraceGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 7 );
  BOOST_CHECK_EQUAL( nodeMap[1], 6 );
  BOOST_CHECK_EQUAL( nodeMap[2], 4 );

  xfldITraceGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 8 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );
  BOOST_CHECK_EQUAL( nodeMap[2], 5 );

  xfldITraceGroup1.associativity(2).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 9 );
  BOOST_CHECK_EQUAL( nodeMap[1], 5 );
  BOOST_CHECK_EQUAL( nodeMap[2], 6 );

  xfldITraceGroup1.associativity(3).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 9 );
  BOOST_CHECK_EQUAL( nodeMap[1], 7 );
  BOOST_CHECK_EQUAL( nodeMap[2], 8 );

  xfldITraceGroup1.associativity(4).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 7 );
  BOOST_CHECK_EQUAL( nodeMap[1], 5 );
  BOOST_CHECK_EQUAL( nodeMap[2], 4 );

  xfldITraceGroup1.associativity(5).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 7 );
  BOOST_CHECK_EQUAL( nodeMap[1], 6 );
  BOOST_CHECK_EQUAL( nodeMap[2], 5 );

  xfldITraceGroup1.associativity(6).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 8 );
  BOOST_CHECK_EQUAL( nodeMap[1], 7 );
  BOOST_CHECK_EQUAL( nodeMap[2], 5 );

  xfldITraceGroup1.associativity(7).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 9 );
  BOOST_CHECK_EQUAL( nodeMap[1], 5 );
  BOOST_CHECK_EQUAL( nodeMap[2], 7 );

  xfldITraceGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 6 );
  BOOST_CHECK_EQUAL( nodeMap[1],10 );
  BOOST_CHECK_EQUAL( nodeMap[2], 4 );

  xfldITraceGroup2.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 5 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );
  BOOST_CHECK_EQUAL( nodeMap[2],10 );

  xfldITraceGroup2.associativity(2).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0],10 );
  BOOST_CHECK_EQUAL( nodeMap[1], 6 );
  BOOST_CHECK_EQUAL( nodeMap[2], 5 );


  // interior edge-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getGroupLeft() , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getGroupRight(), 1 );

  BOOST_CHECK_EQUAL( xfldITraceGroup1.getGroupLeft() , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getGroupRight(), 0 );

  BOOST_CHECK_EQUAL( xfldITraceGroup2.getGroupLeft() , 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.getGroupRight(), 1 );

  int trace_elem = 0;
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementLeft(trace_elem) , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementRight(trace_elem), 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(trace_elem) .trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(trace_elem) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(trace_elem).trace      , 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(trace_elem).orientation,-1 );

  trace_elem = 1;
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementLeft(trace_elem) , 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementRight(trace_elem), 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(trace_elem) .trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(trace_elem) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(trace_elem).trace      , 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(trace_elem).orientation,-1 );

  trace_elem = 2;
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementLeft(trace_elem) , 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementRight(trace_elem), 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(trace_elem) .trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(trace_elem) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(trace_elem).trace      , 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(trace_elem).orientation,-1 );

  trace_elem = 3;
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementLeft(trace_elem) , 4 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementRight(trace_elem), 3 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(trace_elem) .trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(trace_elem) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(trace_elem).trace      , 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(trace_elem).orientation,-2 );

  trace_elem = 0;
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getElementLeft(trace_elem) , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getElementRight(trace_elem), 4 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceLeft(trace_elem) .trace      , 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceLeft(trace_elem) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceRight(trace_elem).trace      , 3 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceRight(trace_elem).orientation,-1 );

  trace_elem = 1;
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getElementLeft(trace_elem) , 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getElementRight(trace_elem), 5 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceLeft(trace_elem) .trace      , 3 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceLeft(trace_elem) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceRight(trace_elem).trace      , 3 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceRight(trace_elem).orientation,-1 );

  trace_elem = 2;
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getElementLeft(trace_elem) , 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getElementRight(trace_elem), 6 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceLeft(trace_elem) .trace      , 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceLeft(trace_elem) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceRight(trace_elem).trace      , 3 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceRight(trace_elem).orientation,-1 );

  trace_elem = 3;
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getElementLeft(trace_elem) , 3 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getElementRight(trace_elem), 7 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceLeft(trace_elem) .trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceLeft(trace_elem) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceRight(trace_elem).trace      , 3 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceRight(trace_elem).orientation,-1 );

  trace_elem = 4;
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getElementLeft(trace_elem) , 4 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getElementRight(trace_elem), 5 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceLeft(trace_elem) .trace      , 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceLeft(trace_elem) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceRight(trace_elem).trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceRight(trace_elem).orientation,-3 );

  trace_elem = 5;
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getElementLeft(trace_elem) , 4 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getElementRight(trace_elem), 6 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceLeft(trace_elem) .trace      , 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceLeft(trace_elem) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceRight(trace_elem).trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceRight(trace_elem).orientation,-3 );

  trace_elem = 6;
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getElementLeft(trace_elem) , 5 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getElementRight(trace_elem), 7 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceLeft(trace_elem) .trace      , 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceLeft(trace_elem) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceRight(trace_elem).trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceRight(trace_elem).orientation,-2 );

  trace_elem = 7;
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getElementLeft(trace_elem) , 6 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getElementRight(trace_elem), 7 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceLeft(trace_elem) .trace      , 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceLeft(trace_elem) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceRight(trace_elem).trace      , 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.getCanonicalTraceRight(trace_elem).orientation,-1 );

  trace_elem = 0;
  BOOST_CHECK_EQUAL( xfldITraceGroup2.getElementLeft(trace_elem) , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.getElementRight(trace_elem), 3 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.getCanonicalTraceLeft(trace_elem) .trace      , 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.getCanonicalTraceLeft(trace_elem) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.getCanonicalTraceRight(trace_elem).trace      , 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.getCanonicalTraceRight(trace_elem).orientation,-3 );

  trace_elem = 1;
  BOOST_CHECK_EQUAL( xfldITraceGroup2.getElementLeft(trace_elem) , 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.getElementRight(trace_elem), 3 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.getCanonicalTraceLeft(trace_elem) .trace      , 3 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.getCanonicalTraceLeft(trace_elem) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.getCanonicalTraceRight(trace_elem).trace      , 3 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.getCanonicalTraceRight(trace_elem).orientation,-2 );

  trace_elem = 2;
  BOOST_CHECK_EQUAL( xfldITraceGroup2.getElementLeft(trace_elem) , 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.getElementRight(trace_elem), 3 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.getCanonicalTraceLeft(trace_elem) .trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.getCanonicalTraceLeft(trace_elem) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.getCanonicalTraceRight(trace_elem).trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.getCanonicalTraceRight(trace_elem).orientation,-1 );


  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nBoundaryTraceGroups(), 3 );

  BOOST_REQUIRE( xfld_split_local.getBoundaryTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_split_local.getBoundaryTraceGroup<Triangle>(1).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_split_local.getBoundaryTraceGroup<Triangle>(2).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup0 = xfld_split_local.getBoundaryTraceGroup<Triangle>(0);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup1 = xfld_split_local.getBoundaryTraceGroup<Triangle>(1);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup2 = xfld_split_local.getBoundaryTraceGroup<Triangle>(2);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 4 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.nElem(), 4 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.nElem(), 4 );

  //Node DOFs
  xfldBTraceGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 8 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );
  BOOST_CHECK_EQUAL( nodeMap[2], 4 );

  xfldBTraceGroup0.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 7 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 );

  xfldBTraceGroup0.associativity(2).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 8 );
  BOOST_CHECK_EQUAL( nodeMap[2], 7 );

  xfldBTraceGroup0.associativity(3).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 8 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );
  BOOST_CHECK_EQUAL( nodeMap[2], 7 );

  xfldBTraceGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 9 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );
  BOOST_CHECK_EQUAL( nodeMap[2], 5 );

  xfldBTraceGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 8 );
  BOOST_CHECK_EQUAL( nodeMap[1], 5 );
  BOOST_CHECK_EQUAL( nodeMap[2], 3 );

  xfldBTraceGroup1.associativity(2).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 9 );
  BOOST_CHECK_EQUAL( nodeMap[2], 8 );

  xfldBTraceGroup1.associativity(3).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 9 );
  BOOST_CHECK_EQUAL( nodeMap[1], 5 );
  BOOST_CHECK_EQUAL( nodeMap[2], 8 );

  xfldBTraceGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 7 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );
  BOOST_CHECK_EQUAL( nodeMap[2], 6 );

  xfldBTraceGroup2.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 9 );
  BOOST_CHECK_EQUAL( nodeMap[1], 6 );
  BOOST_CHECK_EQUAL( nodeMap[2], 1 );

  xfldBTraceGroup2.associativity(2).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 7 );
  BOOST_CHECK_EQUAL( nodeMap[2], 9 );

  xfldBTraceGroup2.associativity(3).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 9 );
  BOOST_CHECK_EQUAL( nodeMap[1], 7 );
  BOOST_CHECK_EQUAL( nodeMap[2], 6 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getGroupLeft() , 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getGroupRight(),-1 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getGroupLeft() , 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getGroupRight(),-1 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getGroupLeft() , 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getGroupRight(),-1 );

  trace_elem = 0;
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementLeft(trace_elem), 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(trace_elem).trace, 1 );

  trace_elem = 1;
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementLeft(trace_elem), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(trace_elem).trace, 1 );

  trace_elem = 2;
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementLeft(trace_elem), 3 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(trace_elem).trace, 1 );

  trace_elem = 3;
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementLeft(trace_elem), 5 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(trace_elem).trace, 2 );

  trace_elem = 0;
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getElementLeft(trace_elem), 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceLeft(trace_elem).trace, 2 );

  trace_elem = 1;
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getElementLeft(trace_elem), 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceLeft(trace_elem).trace, 2 );

  trace_elem = 2;
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getElementLeft(trace_elem), 3 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceLeft(trace_elem).trace, 2 );

  trace_elem = 3;
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getElementLeft(trace_elem), 7 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceLeft(trace_elem).trace, 1 );

  trace_elem = 0;
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getElementLeft(trace_elem), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getCanonicalTraceLeft(trace_elem).trace, 3 );

  trace_elem = 1;
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getElementLeft(trace_elem), 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getCanonicalTraceLeft(trace_elem).trace, 3 );

  trace_elem = 2;
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getElementLeft(trace_elem), 3 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getCanonicalTraceLeft(trace_elem).trace, 3 );

  trace_elem = 3;
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getElementLeft(trace_elem), 6 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getCanonicalTraceLeft(trace_elem).trace, 1 );

  for (int trace_elem=0; trace_elem<4; trace_elem++)
  {
    BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementRight(trace_elem),-1 );
    BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(trace_elem) .orientation, 1 );
    BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceRight(trace_elem).trace      ,-1 );
    BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceRight(trace_elem).orientation, 0 );

    BOOST_CHECK_EQUAL( xfldBTraceGroup1.getElementRight(trace_elem),-1 );
    BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceLeft(trace_elem) .orientation, 1 );
    BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceRight(trace_elem).trace      ,-1 );
    BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceRight(trace_elem).orientation, 0 );

    BOOST_CHECK_EQUAL( xfldBTraceGroup2.getElementRight(trace_elem),-1 );
    BOOST_CHECK_EQUAL( xfldBTraceGroup2.getCanonicalTraceLeft(trace_elem) .orientation, 1 );
    BOOST_CHECK_EQUAL( xfldBTraceGroup2.getCanonicalTraceRight(trace_elem).trace      ,-1 );
    BOOST_CHECK_EQUAL( xfldBTraceGroup2.getCanonicalTraceRight(trace_elem).orientation, 0 );
  }


  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nGhostBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_split_local.getGhostBoundaryTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldGBTraceGroup0 = xfld_split_local.getGhostBoundaryTraceGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 6 );

  //Node DOFs
  xfldGBTraceGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0],10 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );
  BOOST_CHECK_EQUAL( nodeMap[2], 4 );

  xfldGBTraceGroup0.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0],10 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );
  BOOST_CHECK_EQUAL( nodeMap[2], 3 );

  xfldGBTraceGroup0.associativity(2).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 5 );
  BOOST_CHECK_EQUAL( nodeMap[1],10 );
  BOOST_CHECK_EQUAL( nodeMap[2], 3 );

  xfldGBTraceGroup0.associativity(3).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1],10 );
  BOOST_CHECK_EQUAL( nodeMap[2], 5 );

  xfldGBTraceGroup0.associativity(4).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 6 );
  BOOST_CHECK_EQUAL( nodeMap[2],10 );

  xfldGBTraceGroup0.associativity(5).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 6 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );
  BOOST_CHECK_EQUAL( nodeMap[2],10 );

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getGroupLeft() , 1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getGroupRight(),-1 );

  trace_elem = 0;
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getElementLeft(trace_elem), 0 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceLeft(trace_elem).trace, 0 );

  trace_elem = 1;
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getElementLeft(trace_elem), 1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceLeft(trace_elem).trace, 0 );

  trace_elem = 2;
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getElementLeft(trace_elem), 1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceLeft(trace_elem).trace, 2 );

  trace_elem = 3;
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getElementLeft(trace_elem), 2 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceLeft(trace_elem).trace, 2 );

  trace_elem = 4;
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getElementLeft(trace_elem), 2 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceLeft(trace_elem).trace, 3 );

  trace_elem = 5;
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getElementLeft(trace_elem), 0 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceLeft(trace_elem).trace, 3 );

  for (int trace_elem=0; trace_elem<6; trace_elem++)
  {
    BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getElementRight(trace_elem),-1 );
    BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceLeft(trace_elem) .orientation, 1 );
    BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceRight(trace_elem).trace      ,-1 );
    BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceRight(trace_elem).orientation, 0 );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_Local_5Tet_AllOrientations_X1_1Group_InteriorCell_Target_IsotropicSplit_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  Real tol = 1e-12;

  Real vertices[14][3] = {{0.0,0.0,0.0},
                          {1.0,0.0,0.0},
                          {0.0,1.0,0.0},
                          {0.0,0.0,1.0},
                          {0.0,0.5,0.5},
                          {0.5,0.0,0.5},
                          {0.5,0.5,0.0},
                          {0.0,0.5,0.0},
                          {0.0,0.0,0.5},
                          {0.5,0.0,0.0},
                          {1.0,1.0,1.0},
                          {-1.0,0.0,0.0},
                          {0.0,-1.0,0.0},
                          {0.0,0.0,-1.0}};

  //Loop across all orientations of AllOrientation grid, and simply generate the split meshes
  //xfld.checkGrid() will automatically perform connectivity and jacobian checks.

  for (int orient=-3; orient<=3; orient++)
  {
    if (orient==0) continue;

    XField3D_5Tet_X1_1Group_AllOrientations xfld(orient);

    //Build cell to trace connectivity structure
    XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

    //Extract the local mesh for the center tet
    XField_Local<PhysD3,TopoD3> xfld_local(comm_local, connectivity,0,0);

    //Build cell to trace connectivity structure
    XField_CellToTrace<PhysD3,TopoD3> connectivity_local(xfld_local);

    int split_edge_index = -1;
    XField_Local_Split<PhysD3,TopoD3> xfld_split_local(xfld_local, connectivity_local,
                                                       ElementSplitType::Isotropic, split_edge_index);

#if 0
    xfld_local.dump(3,std::cout);
    std::cout<<std::endl;
    xfld_split_local.dump(3,std::cout);
#endif

    BOOST_CHECK_EQUAL( xfld_split_local.nDOF(), 14 );

    //Check the node values
    for (int k=0; k<xfld_split_local.nDOF(); k++)
      for (int d=0; d<3; d++)
        SANS_CHECK_CLOSE( xfld_split_local.DOF(k)[d], vertices[k][d], tol, tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_Local_5Tet_AllOrientations_X2_1Group_InteriorCell_Target_IsotropicSplit_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  Real small_tol = 1e-12;
  Real close_tol = 1e-11;

  Real vertices[14][3] = {{0.0,0.0,0.0},
                          {1.0,0.0,0.0},
                          {0.0,1.0,0.0},
                          {0.0,0.0,1.0},
                          {0.0,0.5,0.5},
                          {0.5,0.0,0.5},
                          {0.5,0.5,0.0},
                          {0.0,0.5,0.0},
                          {0.0,0.0,0.5},
                          {0.5,0.0,0.0},
                          {1.0,1.0,1.0},
                          {-1.0,0.0,0.0},
                          {0.0,-1.0,0.0},
                          {0.0,0.0,-1.0}};

  //Loop across all orientations of AllOrientation grid, and simply generate the split meshes
  //xfld.checkGrid() will automatically perform connectivity and jacobian checks.

  for (int orient=-3; orient<=3; orient++)
  {
    if (orient==0) continue;

    XField3D_5Tet_X1_1Group_AllOrientations xfld_X1(orient);

    XField<PhysD3,TopoD3> xfld(xfld_X1,2); //Construct Q2 mesh from linear mesh

    //Build cell to trace connectivity structure
    XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

    //Extract the local mesh for the center tet
    XField_Local<PhysD3,TopoD3> xfld_local(comm_local, connectivity,0,0);

    //Build cell to trace connectivity structure
    XField_CellToTrace<PhysD3,TopoD3> connectivity_local(xfld_local);

    int split_edge_index = -1;
    XField_Local_Split<PhysD3,TopoD3> xfld_split_local(xfld_local, connectivity_local,
                                                       ElementSplitType::Isotropic, split_edge_index);

#if 0
    xfld_local.dump(3,std::cout);
    std::cout<<std::endl;
    xfld_split_local.dump(3,std::cout);
#endif

    BOOST_CHECK_EQUAL( xfld_split_local.nDOF(), 63 );

    //Check if edge DOFs are zero
    for (int k = 0; k < 49; k++)
      for (int d = 0; d < 3; d++)
        SANS_CHECK_CLOSE( xfld_split_local.DOF(k)[d], 0.0, small_tol, close_tol );

    //Check the node values
    for (int k = 0; k < 14; k++)
      for (int d = 0; d < 3; d++)
        SANS_CHECK_CLOSE( xfld_split_local.DOF(49 + k)[d], vertices[k][d], small_tol, close_tol );
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
