// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Field3D_Local_DG_btest
//

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "unit/UnitGrids/XField3D_1Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_2Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_5Tet_X1_1Group_AllOrientations.h"

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunctionArea_Triangle.h"

#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_InteriorTrace.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/Local/Field_Local.h"

#include "Field/XFieldVolume_Local.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

//Explicitly instantiate classes to get proper coverage information
namespace SANS
{
template class Field_DG_Cell< PhysD3, TopoD3, Real >;
template class Field_DG_InteriorTrace< PhysD3, TopoD3, Real >;
template class Field_DG_BoundaryTrace< PhysD3, TopoD3, Real >;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( Field3D_Local_DG_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field3D_DG_Local_1Tet_X1_ExtractInteriorCell_P0 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD3, TopoD3, ArrayQ > QField3D_DG_Volume;

  typedef std::array<int,4> Int4;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField3D_1Tet_X1_1Group xfld;

  int order = 0;
  QField3D_DG_Volume qfld(xfld, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 1, qfld.nDOFCellGroup(0));
  BOOST_CHECK_THROW( qfld.nDOFInteriorTraceGroup(0), SANSException);
  BOOST_CHECK_THROW( qfld.nDOFBoundaryTraceGroup(0), SANSException);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  //Extract the local mesh for the interior tet
  XField_Local<PhysD3,TopoD3> xfld_local(comm_local,connectivity,0,0);

  Field_Local<QField3D_DG_Volume> qfld_local(xfld_local, qfld, order, BasisFunctionCategory_Legendre);

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_local.getXField(), &xfld_local );

  BOOST_CHECK_EQUAL( qfld_local.nDOF(), 1 );

  BOOST_CHECK_EQUAL( 1, qfld_local.nDOFCellGroup(0));
  BOOST_CHECK_THROW( qfld_local.nDOFInteriorTraceGroup(0), SANSException);
  BOOST_CHECK_THROW( qfld_local.nDOFBoundaryTraceGroup(0), SANSException);

  for (int n = 0; n < qfld_local.nDOF(); n++)
    for (int i=0; i<2; i++)
      BOOST_CHECK_EQUAL(qfld_local.DOF(n)[i], qfld.DOF(n)[i]);


  BOOST_CHECK_EQUAL( qfld_local.nCellGroups(), 1 );
  BOOST_REQUIRE( qfld_local.getCellGroupBase(0).topoTypeID() == typeid(Tet) );

  const QField3D_DG_Volume::FieldCellGroupType<Tet>& qfldCellGroup0 = qfld_local.getCellGroup<Tet>(0);

  int DOFMap[1];

  //Node DOFs
  BOOST_CHECK_EQUAL( qfldCellGroup0.associativity(0).nNode(), 0 );

  //Edge DOFs
  BOOST_CHECK_EQUAL( qfldCellGroup0.associativity(0).nEdge(), 0 );

  //Face DOFs
  BOOST_CHECK_EQUAL( qfldCellGroup0.associativity(0).nFace(), 0 );

  //Cell DOFs
  BOOST_CHECK_EQUAL( qfldCellGroup0.associativity(0).nCell(), 1 );

  qfldCellGroup0.associativity(0).getCellGlobalMapping( DOFMap, 1 );
  BOOST_CHECK_EQUAL( DOFMap[0], 0 );

  Int4 faceSign;

  faceSign = qfldCellGroup0.associativity(0).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], +1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], +1 );

  BOOST_CHECK_EQUAL( qfld_local.nInteriorTraceGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_local.nBoundaryTraceGroups(), 0 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field3D_DG_Local_1Tet_X1_ExtractInteriorCell_BoundaryTrace_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_BoundaryTrace< PhysD3, TopoD3, ArrayQ > QField3D_DG_BTrace;

//  typedef std::array<int,4> Int4;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField3D_1Tet_X1_1Group xfld;

  int order = 1;
  QField3D_DG_BTrace qfld(xfld, order, BasisFunctionCategory_Legendre);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  //Extract the local mesh for the interior tet
  XField_Local<PhysD3,TopoD3> xfld_local(comm_local,connectivity,0,0);

  Field_Local<QField3D_DG_BTrace> qfld_local(xfld_local, qfld, order, BasisFunctionCategory_Legendre);

#if 0
  qfld_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_local.getXField(), &xfld_local );

  BOOST_CHECK_EQUAL( qfld_local.nDOF(), 12 );

  for (int n = 0; n < qfld_local.nDOF(); n++)
    for (int i=0; i<2; i++)
      BOOST_CHECK_EQUAL(qfld_local.DOF(n)[i], qfld.DOF(n)[i]);


  BOOST_CHECK_EQUAL( qfld_local.nCellGroups(), 0 );

  int DOFMap[3];

  BOOST_CHECK_EQUAL( qfld_local.nInteriorTraceGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_local.nBoundaryTraceGroups(), 4 );

  BOOST_REQUIRE( qfld_local.getBoundaryTraceGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( qfld_local.getBoundaryTraceGroupBase(1).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( qfld_local.getBoundaryTraceGroupBase(2).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( qfld_local.getBoundaryTraceGroupBase(3).topoTypeID() == typeid(Triangle) );

  const QField3D_DG_BTrace::FieldTraceGroupType<Triangle>& qfldBTraceGroup0 = qfld_local.getBoundaryTraceGroup<Triangle>(0);
  const QField3D_DG_BTrace::FieldTraceGroupType<Triangle>& qfldBTraceGroup1 = qfld_local.getBoundaryTraceGroup<Triangle>(1);
  const QField3D_DG_BTrace::FieldTraceGroupType<Triangle>& qfldBTraceGroup2 = qfld_local.getBoundaryTraceGroup<Triangle>(2);
  const QField3D_DG_BTrace::FieldTraceGroupType<Triangle>& qfldBTraceGroup3 = qfld_local.getBoundaryTraceGroup<Triangle>(3);

  BOOST_CHECK_EQUAL( qfldBTraceGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( qfldBTraceGroup1.nElem(), 1 );
  BOOST_CHECK_EQUAL( qfldBTraceGroup2.nElem(), 1 );
  BOOST_CHECK_EQUAL( qfldBTraceGroup3.nElem(), 1 );

  BOOST_CHECK_EQUAL( qfldBTraceGroup0.associativity(0).nNode(), 0 );
  BOOST_CHECK_EQUAL( qfldBTraceGroup1.associativity(0).nNode(), 0 );
  BOOST_CHECK_EQUAL( qfldBTraceGroup2.associativity(0).nNode(), 0 );
  BOOST_CHECK_EQUAL( qfldBTraceGroup3.associativity(0).nNode(), 0 );

  BOOST_CHECK_EQUAL( qfldBTraceGroup0.associativity(0).nEdge(), 0 );
  BOOST_CHECK_EQUAL( qfldBTraceGroup1.associativity(0).nEdge(), 0 );
  BOOST_CHECK_EQUAL( qfldBTraceGroup2.associativity(0).nEdge(), 0 );
  BOOST_CHECK_EQUAL( qfldBTraceGroup3.associativity(0).nEdge(), 0 );

  BOOST_CHECK_EQUAL( qfldBTraceGroup0.associativity(0).nCell(), 3 );
  BOOST_CHECK_EQUAL( qfldBTraceGroup1.associativity(0).nCell(), 3 );
  BOOST_CHECK_EQUAL( qfldBTraceGroup2.associativity(0).nCell(), 3 );
  BOOST_CHECK_EQUAL( qfldBTraceGroup3.associativity(0).nCell(), 3 );

  qfldBTraceGroup0.associativity(0).getCellGlobalMapping( DOFMap, 3 );
  BOOST_CHECK_EQUAL( DOFMap[0], 0 );
  BOOST_CHECK_EQUAL( DOFMap[1], 1 );
  BOOST_CHECK_EQUAL( DOFMap[2], 2 );

  qfldBTraceGroup1.associativity(0).getCellGlobalMapping( DOFMap, 3 );
  BOOST_CHECK_EQUAL( DOFMap[0], 3 );
  BOOST_CHECK_EQUAL( DOFMap[1], 4 );
  BOOST_CHECK_EQUAL( DOFMap[2], 5 );

  qfldBTraceGroup2.associativity(0).getCellGlobalMapping( DOFMap, 3 );
  BOOST_CHECK_EQUAL( DOFMap[0], 6 );
  BOOST_CHECK_EQUAL( DOFMap[1], 7 );
  BOOST_CHECK_EQUAL( DOFMap[2], 8 );

  qfldBTraceGroup3.associativity(0).getCellGlobalMapping( DOFMap, 3 );
  BOOST_CHECK_EQUAL( DOFMap[0], 9 );
  BOOST_CHECK_EQUAL( DOFMap[1], 10 );
  BOOST_CHECK_EQUAL( DOFMap[2], 11 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field3D_DG_Local_5Tet_AllOrientations_X1_ExtractInteriorCell_P0 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD3, TopoD3, ArrayQ > QField3D_DG_Volume;

  typedef std::array<int,4> Int4;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField3D_5Tet_X1_1Group_AllOrientations xfld(-1);

  int order = 0;
  QField3D_DG_Volume qfld(xfld, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 5, qfld.nDOFCellGroup(0));
  BOOST_CHECK_THROW( qfld.nDOFInteriorTraceGroup(0), SANSException);
  BOOST_CHECK_THROW( qfld.nDOFBoundaryTraceGroup(0), SANSException);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  //Extract the local mesh for the interior tet
  XField_Local<PhysD3,TopoD3> xfld_local(comm_local,connectivity,0,0);

  Field_Local<QField3D_DG_Volume> qfld_local(xfld_local, qfld, order, BasisFunctionCategory_Legendre);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl;
  qfld_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_local.getXField(), &xfld_local );

  BOOST_CHECK_EQUAL( qfld_local.nDOF(), 5 );

  BOOST_CHECK_EQUAL( 1, qfld_local.nDOFCellGroup(0));
  BOOST_CHECK_EQUAL( 4, qfld_local.nDOFCellGroup(1));
  BOOST_CHECK_THROW( qfld_local.nDOFInteriorTraceGroup(0), SANSException);
  BOOST_CHECK_THROW( qfld_local.nDOFBoundaryTraceGroup(0), SANSException);

  for (int n = 0; n < qfld_local.nDOF(); n++)
    for (int i=0; i<2; i++)
      BOOST_CHECK_EQUAL(qfld_local.DOF(n)[i], qfld.DOF(n)[i]);


  BOOST_CHECK_EQUAL( qfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( qfld_local.getCellGroupBase(0).topoTypeID() == typeid(Tet) );
  BOOST_REQUIRE( qfld_local.getCellGroupBase(1).topoTypeID() == typeid(Tet) );

  const QField3D_DG_Volume::FieldCellGroupType<Tet>& qfldCellGroup0 = qfld_local.getCellGroup<Tet>(0);
  const QField3D_DG_Volume::FieldCellGroupType<Tet>& qfldCellGroup1 = qfld_local.getCellGroup<Tet>(1);

  int DOFMap[1];

  //Node DOFs
  BOOST_CHECK_EQUAL( qfldCellGroup0.associativity(0).nNode(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(0).nNode(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(1).nNode(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(2).nNode(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(3).nNode(), 0 );

  //Edge DOFs
  BOOST_CHECK_EQUAL( qfldCellGroup0.associativity(0).nEdge(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(0).nEdge(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(1).nEdge(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(2).nEdge(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(3).nEdge(), 0 );

  //Face DOFs
  BOOST_CHECK_EQUAL( qfldCellGroup0.associativity(0).nFace(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(0).nFace(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(1).nFace(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(2).nFace(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(3).nFace(), 0 );

  //Cell DOFs
  BOOST_CHECK_EQUAL( qfldCellGroup0.associativity(0).nCell(), 1 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(0).nCell(), 1 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(1).nCell(), 1 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(2).nCell(), 1 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(3).nCell(), 1 );

  qfldCellGroup0.associativity(0).getCellGlobalMapping( DOFMap, 1 );
  BOOST_CHECK_EQUAL( DOFMap[0], 0 );

  qfldCellGroup1.associativity(0).getCellGlobalMapping( DOFMap, 1 );
  BOOST_CHECK_EQUAL( DOFMap[0], 1 );

  qfldCellGroup1.associativity(1).getCellGlobalMapping( DOFMap, 1 );
  BOOST_CHECK_EQUAL( DOFMap[0], 2 );

  qfldCellGroup1.associativity(2).getCellGlobalMapping( DOFMap, 1 );
  BOOST_CHECK_EQUAL( DOFMap[0], 3 );

  qfldCellGroup1.associativity(3).getCellGlobalMapping( DOFMap, 1 );
  BOOST_CHECK_EQUAL( DOFMap[0], 4 );


  Int4 faceSign;

  faceSign = qfldCellGroup0.associativity(0).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], +1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], +1 );

  faceSign = qfldCellGroup1.associativity(0).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], +1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], +1 );

  faceSign = qfldCellGroup1.associativity(1).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], +1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], +1 );

  faceSign = qfldCellGroup1.associativity(2).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], +1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], +1 );

  faceSign = qfldCellGroup1.associativity(3).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], +1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], +1 );

  BOOST_CHECK_EQUAL( qfld_local.nInteriorTraceGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_local.nBoundaryTraceGroups(), 0 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field3D_DG_Local_5Tet_AllOrientations_X1_ExtractBoundaryCell_P0 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD3, TopoD3, ArrayQ > QField3D_DG_Volume;

  typedef std::array<int,4> Int4;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField3D_5Tet_X1_1Group_AllOrientations xfld(-1);

  int order = 0;
  QField3D_DG_Volume qfld(xfld, order, BasisFunctionCategory_Legendre);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  //Extract the local mesh for the boundary tet
  XField_Local<PhysD3,TopoD3> xfld_local(comm_local,connectivity,0,1);

  Field_Local<QField3D_DG_Volume> qfld_local(xfld_local, qfld, order, BasisFunctionCategory_Legendre);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl;
  qfld_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_local.getXField(), &xfld_local );

  BOOST_CHECK_EQUAL( qfld_local.nDOF(), 2 );

  for (int i=0; i<2; i++)
  {
    BOOST_CHECK_EQUAL(qfld_local.DOF(0)[i], qfld.DOF(1)[i]);
    BOOST_CHECK_EQUAL(qfld_local.DOF(1)[i], qfld.DOF(0)[i]);
  }


  BOOST_CHECK_EQUAL( qfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( qfld_local.getCellGroupBase(0).topoTypeID() == typeid(Tet) );
  BOOST_REQUIRE( qfld_local.getCellGroupBase(1).topoTypeID() == typeid(Tet) );

  const QField3D_DG_Volume::FieldCellGroupType<Tet>& qfldCellGroup0 = qfld_local.getCellGroup<Tet>(0);
  const QField3D_DG_Volume::FieldCellGroupType<Tet>& qfldCellGroup1 = qfld_local.getCellGroup<Tet>(1);

  BOOST_CHECK_EQUAL( qfldCellGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.nElem(), 1 );

  int DOFMap[1];

  //Node DOFs
  BOOST_CHECK_EQUAL( qfldCellGroup0.associativity(0).nNode(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(0).nNode(), 0 );

  //Edge DOFs
  BOOST_CHECK_EQUAL( qfldCellGroup0.associativity(0).nEdge(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(0).nEdge(), 0 );

  //Face DOFs
  BOOST_CHECK_EQUAL( qfldCellGroup0.associativity(0).nFace(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(0).nFace(), 0 );

  //Cell DOFs
  BOOST_CHECK_EQUAL( qfldCellGroup0.associativity(0).nCell(), 1 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(0).nCell(), 1 );

  qfldCellGroup0.associativity(0).getCellGlobalMapping( DOFMap, 1 );
  BOOST_CHECK_EQUAL( DOFMap[0], 0 );

  qfldCellGroup1.associativity(0).getCellGlobalMapping( DOFMap, 1 );
  BOOST_CHECK_EQUAL( DOFMap[0], 1 );

  Int4 faceSign;

  faceSign = qfldCellGroup0.associativity(0).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], +1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], +1 );

  faceSign = qfldCellGroup1.associativity(0).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], +1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], +1 );

  BOOST_CHECK_EQUAL( qfld_local.nInteriorTraceGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_local.nBoundaryTraceGroups(), 0 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field3D_DG_Local_5Tet_AllOrientations_X1_ExtractInteriorCell_BoundaryTrace_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_BoundaryTrace< PhysD3, TopoD3, ArrayQ > QField3D_DG_BTrace;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField3D_5Tet_X1_1Group_AllOrientations xfld(-1);

  int order = 1;
  QField3D_DG_BTrace qfld(xfld, order, BasisFunctionCategory_Legendre);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  //Extract the local mesh for the interior tet
  XField_Local<PhysD3,TopoD3> xfld_local(comm_local,connectivity,0,0);

  Field_Local<QField3D_DG_BTrace> qfld_local(xfld_local, qfld, order, BasisFunctionCategory_Legendre);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl;
  qfld_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_local.getXField(), &xfld_local );

  BOOST_CHECK_EQUAL( qfld_local.nDOF(), 0 );
  BOOST_CHECK_EQUAL( qfld_local.nCellGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_local.nInteriorTraceGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_local.nBoundaryTraceGroups(), 0 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field3D_DG_Local_5Tet_AllOrientations_X1_ExtractBoundaryCell_InteriorTrace_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_InteriorTrace< PhysD3, TopoD3, ArrayQ > QField3D_DG_ITrace;

  typedef QField3D_DG_ITrace::FieldTraceGroupType<Triangle> FieldTraceGroupType;
  typedef FieldTraceGroupType::ElementType<> ElementFieldClass;

  typedef DLA::VectorS<TopoD2::D,Real> RefCoordType;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  Real tol = 1e-12;

  XField3D_5Tet_X1_1Group_AllOrientations xfld(-1);

  int order = 1;
  QField3D_DG_ITrace qfld(xfld, order, BasisFunctionCategory_Legendre);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  //Extract the local mesh for the 2nd boundary tet
  XField_Local<PhysD3,TopoD3> xfld_local(comm_local,connectivity,0,2);

  Field_Local<QField3D_DG_ITrace> qfld_local(xfld_local, qfld, order, BasisFunctionCategory_Legendre);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl;
  qfld_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_local.getXField(), &xfld_local );

  BOOST_CHECK_EQUAL( qfld_local.nDOF(), 3 );
  BOOST_CHECK_EQUAL( qfld_local.nCellGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_local.nInteriorTraceGroups(), 1 );

  BOOST_REQUIRE( qfld_local.getInteriorTraceGroupBase(0).topoTypeID() == typeid(Triangle) );

  const FieldTraceGroupType& qfldITraceGroup0 = qfld_local.getInteriorTraceGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( qfldITraceGroup0.nElem(), 1 );

  BOOST_CHECK_EQUAL( qfldITraceGroup0.associativity(0).nNode(), 0 );
  BOOST_CHECK_EQUAL( qfldITraceGroup0.associativity(0).nEdge(), 0 );
  BOOST_CHECK_EQUAL( qfldITraceGroup0.associativity(0).nCell(), 3 );

  int DOFMap[3];

  qfldITraceGroup0.associativity(0).getCellGlobalMapping( DOFMap, 3 );
  BOOST_CHECK_EQUAL( DOFMap[0], 0 );
  BOOST_CHECK_EQUAL( DOFMap[1], 1 );
  BOOST_CHECK_EQUAL( DOFMap[2], 2 );

  BOOST_CHECK_EQUAL( qfld_local.nBoundaryTraceGroups(), 0 );

  const FieldTraceGroupType& qfldGlobal_ITraceGroup0 = qfld.getInteriorTraceGroup<Triangle>(0);

  ElementFieldClass fldElem_global( qfldGlobal_ITraceGroup0.basis() );
  ElementFieldClass fldElem_local( qfldITraceGroup0.basis() );

  qfldGlobal_ITraceGroup0.getElement(fldElem_global, 1);
  qfldITraceGroup0.getElement(fldElem_local, 0);

  RefCoordType sRef;  // reference-element coordinates on face
  ArrayQ q_global, q_local;

  //Get order 3 quad points
  Quadrature<TopoD2, Triangle> quadrature(3);
  const int nquad = quadrature.nQuadrature();

  //Check if the solutions are equal at a few quadrature points
  for (int iquad = 0; iquad < nquad; iquad++)
  {
    quadrature.coordinates( iquad, sRef );

    fldElem_global.eval(sRef[0], sRef[1], q_global);
    fldElem_local.eval(sRef[1], sRef[0], q_local); //coordinates swapped because trace is reversed (viewed from other side)

    for (int i = 0; i < 2; i++)
      SANS_CHECK_CLOSE(q_global[i], q_local[i], tol, tol);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field3D_DG_Local_5Tet_AllOrientations_X1_ExtractBoundaryCell_BoundaryTrace_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_BoundaryTrace< PhysD3, TopoD3, ArrayQ > QField3D_DG_BTrace;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField3D_5Tet_X1_1Group_AllOrientations xfld(-1);

  int order = 1;
  QField3D_DG_BTrace qfld(xfld, order, BasisFunctionCategory_Legendre);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  //Extract the local mesh for the boundary tet
  XField_Local<PhysD3,TopoD3> xfld_local(comm_local,connectivity,0,1);

  Field_Local<QField3D_DG_BTrace> qfld_local(xfld_local, qfld, order, BasisFunctionCategory_Legendre);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl;
  qfld_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_local.getXField(), &xfld_local );

  BOOST_CHECK_EQUAL( qfld_local.nDOF(), 3*3 );

  for ( int j=0; j<3*3; j++)
    for (int i=0; i<2; i++)
      BOOST_CHECK_EQUAL(qfld_local.DOF(j)[i], qfld.DOF(j)[i]);

  BOOST_CHECK_EQUAL( qfld_local.nCellGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_local.nInteriorTraceGroups(), 0 );

  BOOST_CHECK_EQUAL( qfld_local.nBoundaryTraceGroups(), 3 );

  BOOST_REQUIRE( qfld_local.getBoundaryTraceGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( qfld_local.getBoundaryTraceGroupBase(1).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( qfld_local.getBoundaryTraceGroupBase(2).topoTypeID() == typeid(Triangle) );

  const QField3D_DG_BTrace::FieldTraceGroupType<Triangle>& qfldBTraceGroup0 = qfld_local.getBoundaryTraceGroup<Triangle>(0);
  const QField3D_DG_BTrace::FieldTraceGroupType<Triangle>& qfldBTraceGroup1 = qfld_local.getBoundaryTraceGroup<Triangle>(1);
  const QField3D_DG_BTrace::FieldTraceGroupType<Triangle>& qfldBTraceGroup2 = qfld_local.getBoundaryTraceGroup<Triangle>(2);

  BOOST_CHECK_EQUAL( qfldBTraceGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( qfldBTraceGroup1.nElem(), 1 );
  BOOST_CHECK_EQUAL( qfldBTraceGroup2.nElem(), 1 );

  int DOFMap[3];

  BOOST_CHECK_EQUAL( qfldBTraceGroup0.associativity(0).nNode(), 0 );
  BOOST_CHECK_EQUAL( qfldBTraceGroup0.associativity(0).nEdge(), 0 );
  BOOST_CHECK_EQUAL( qfldBTraceGroup0.associativity(0).nCell(), 3 );

  qfldBTraceGroup0.associativity(0).getCellGlobalMapping( DOFMap, 3 );
  BOOST_CHECK_EQUAL( DOFMap[0], 3*0 + 0 );
  BOOST_CHECK_EQUAL( DOFMap[1], 3*0 + 1 );
  BOOST_CHECK_EQUAL( DOFMap[2], 3*0 + 2 );

  BOOST_CHECK_EQUAL( qfldBTraceGroup1.associativity(0).nNode(), 0 );
  BOOST_CHECK_EQUAL( qfldBTraceGroup1.associativity(0).nEdge(), 0 );
  BOOST_CHECK_EQUAL( qfldBTraceGroup1.associativity(0).nCell(), 3 );

  qfldBTraceGroup1.associativity(0).getCellGlobalMapping( DOFMap, 3 );
  BOOST_CHECK_EQUAL( DOFMap[0], 3*1 + 0 );
  BOOST_CHECK_EQUAL( DOFMap[1], 3*1 + 1 );
  BOOST_CHECK_EQUAL( DOFMap[2], 3*1 + 2 );

  BOOST_CHECK_EQUAL( qfldBTraceGroup2.associativity(0).nNode(), 0 );
  BOOST_CHECK_EQUAL( qfldBTraceGroup2.associativity(0).nEdge(), 0 );
  BOOST_CHECK_EQUAL( qfldBTraceGroup2.associativity(0).nCell(), 3 );

  qfldBTraceGroup2.associativity(0).getCellGlobalMapping( DOFMap, 3 );
  BOOST_CHECK_EQUAL( DOFMap[0], 3*2 + 0 );
  BOOST_CHECK_EQUAL( DOFMap[1], 3*2 + 1 );
  BOOST_CHECK_EQUAL( DOFMap[2], 3*2 + 2 );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
