// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Field1D_Local_DG_btest
//

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Meshing/XField1D/XField1D.h"

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunctionLine.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_InteriorTrace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLine_DG_Trace.h"
#include "Field/Local/Field_Local.h"

#include "Field/XFieldLine_Local.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


//Explicitly instantiate classes to get proper coverage information
namespace SANS
{
template class Field_DG_Cell< PhysD1, TopoD1, Real >;
template class Field_DG_InteriorTrace< PhysD1, TopoD1, Real >;
template class Field_DG_BoundaryTrace< PhysD1, TopoD1, Real >;
template class Field_DG_Trace< PhysD1, TopoD1, Real >;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( Field1D_Local_DG_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field1D_DG_Local_3Line_X1_ExtractInteriorCell_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD1, TopoD1, ArrayQ > QField1D_DG_Line;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField1D xfld(3); //Linear mesh with 3 lines

  int order = 1;
  QField1D_DG_Line qfld(xfld, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 2*3, qfld.nDOFCellGroup(0) );
  BOOST_CHECK_THROW( qfld.nDOFInteriorTraceGroup(0), SANSException);
  BOOST_CHECK_THROW( qfld.nDOFBoundaryTraceGroup(0), SANSException);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD1,TopoD1> connectivity(xfld);

  //Extract the local mesh for the interior line
  XField_Local<PhysD1,TopoD1> xfld_local(comm_local,connectivity,0,1);

  Field_Local<QField1D_DG_Line> qfld_local(xfld_local, qfld, order, BasisFunctionCategory_Legendre);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  qfld_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_local.getXField(), &xfld_local );

  BOOST_CHECK_EQUAL( qfld_local.nDOF(), 6 );

  BOOST_CHECK_EQUAL( 2*1, qfld_local.nDOFCellGroup(0) );
  BOOST_CHECK_EQUAL( 2*2, qfld_local.nDOFCellGroup(1) );
  BOOST_CHECK_THROW( qfld_local.nDOFInteriorTraceGroup(0), SANSException);
  BOOST_CHECK_THROW( qfld_local.nDOFBoundaryTraceGroup(0), SANSException);

  for (int i=0; i<2; i++)
  {
    BOOST_CHECK_EQUAL(qfld_local.DOF(0)[i], qfld.DOF(2)[i]);
    BOOST_CHECK_EQUAL(qfld_local.DOF(1)[i], qfld.DOF(3)[i]);

    BOOST_CHECK_EQUAL(qfld_local.DOF(2)[i], qfld.DOF(4)[i]);
    BOOST_CHECK_EQUAL(qfld_local.DOF(3)[i], qfld.DOF(5)[i]);

    BOOST_CHECK_EQUAL(qfld_local.DOF(4)[i], qfld.DOF(0)[i]);
    BOOST_CHECK_EQUAL(qfld_local.DOF(5)[i], qfld.DOF(1)[i]);
  }


  BOOST_CHECK_EQUAL( qfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( qfld_local.getCellGroupBase(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( qfld_local.getCellGroupBase(1).topoTypeID() == typeid(Line) );

  const QField1D_DG_Line::FieldCellGroupType<Line>& qfldCellGroup0 = qfld_local.getCellGroup<Line>(0);
  const QField1D_DG_Line::FieldCellGroupType<Line>& qfldCellGroup1 = qfld_local.getCellGroup<Line>(1);

  BOOST_CHECK_EQUAL( qfldCellGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.nElem(), 2 );

  int DOFMap[2];

  //Node DOFs
  BOOST_CHECK_EQUAL( qfldCellGroup0.associativity(0).nNode(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(0).nNode(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(1).nNode(), 0 );

  //Edge DOFs
  BOOST_CHECK_EQUAL( qfldCellGroup0.associativity(0).nEdge(), 2 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(0).nEdge(), 2 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(1).nEdge(), 2 );

  qfldCellGroup0.associativity(0).getEdgeGlobalMapping( DOFMap, 2 );
  BOOST_CHECK_EQUAL( DOFMap[0], 0 );
  BOOST_CHECK_EQUAL( DOFMap[1], 1 );

  qfldCellGroup1.associativity(0).getEdgeGlobalMapping( DOFMap, 2 );
  BOOST_CHECK_EQUAL( DOFMap[0], 2 );
  BOOST_CHECK_EQUAL( DOFMap[1], 3 );

  qfldCellGroup1.associativity(1).getEdgeGlobalMapping( DOFMap, 2 );
  BOOST_CHECK_EQUAL( DOFMap[0], 4 );
  BOOST_CHECK_EQUAL( DOFMap[1], 5 );

  BOOST_CHECK_EQUAL( qfld_local.nInteriorTraceGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_local.nBoundaryTraceGroups(), 0 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field1D_DG_Local_3Line_X1_ExtractInteriorCell_P2 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD1, TopoD1, ArrayQ > QField1D_DG_Line;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField1D xfld(3); //Linear mesh with 3 lines

  int order = 2;
  QField1D_DG_Line qfld(xfld, order, BasisFunctionCategory_Legendre);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD1,TopoD1> connectivity(xfld);

  //Extract the local mesh for the interior line
  XField_Local<PhysD1,TopoD1> xfld_local(comm_local,connectivity,0,1);

  Field_Local<QField1D_DG_Line> qfld_local(xfld_local, qfld, order, BasisFunctionCategory_Legendre);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  qfld_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_local.getXField(), &xfld_local );

  BOOST_CHECK_EQUAL( qfld_local.nDOF(), 9 );

  for (int i=0; i<2; i++)
  {
    BOOST_CHECK_EQUAL(qfld_local.DOF(0)[i], qfld.DOF(3)[i]);
    BOOST_CHECK_EQUAL(qfld_local.DOF(1)[i], qfld.DOF(4)[i]);
    BOOST_CHECK_EQUAL(qfld_local.DOF(2)[i], qfld.DOF(5)[i]);

    BOOST_CHECK_EQUAL(qfld_local.DOF(3)[i], qfld.DOF(6)[i]);
    BOOST_CHECK_EQUAL(qfld_local.DOF(4)[i], qfld.DOF(7)[i]);
    BOOST_CHECK_EQUAL(qfld_local.DOF(5)[i], qfld.DOF(8)[i]);

    BOOST_CHECK_EQUAL(qfld_local.DOF(6)[i], qfld.DOF(0)[i]);
    BOOST_CHECK_EQUAL(qfld_local.DOF(7)[i], qfld.DOF(1)[i]);
    BOOST_CHECK_EQUAL(qfld_local.DOF(8)[i], qfld.DOF(2)[i]);
  }


  BOOST_CHECK_EQUAL( qfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( qfld_local.getCellGroupBase(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( qfld_local.getCellGroupBase(1).topoTypeID() == typeid(Line) );

  const QField1D_DG_Line::FieldCellGroupType<Line>& qfldCellGroup0 = qfld_local.getCellGroup<Line>(0);
  const QField1D_DG_Line::FieldCellGroupType<Line>& qfldCellGroup1 = qfld_local.getCellGroup<Line>(1);

  BOOST_CHECK_EQUAL( qfldCellGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.nElem(), 2 );

  int DOFMap[3];

  //Node DOFs
  BOOST_CHECK_EQUAL( qfldCellGroup0.associativity(0).nNode(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(0).nNode(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(1).nNode(), 0 );

  //Edge DOFs
  BOOST_CHECK_EQUAL( qfldCellGroup0.associativity(0).nEdge(), 3 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(0).nEdge(), 3 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(1).nEdge(), 3 );

  qfldCellGroup0.associativity(0).getEdgeGlobalMapping( DOFMap, 3 );
  BOOST_CHECK_EQUAL( DOFMap[0], 0 );
  BOOST_CHECK_EQUAL( DOFMap[1], 1 );
  BOOST_CHECK_EQUAL( DOFMap[2], 2 );

  qfldCellGroup1.associativity(0).getEdgeGlobalMapping( DOFMap, 3 );
  BOOST_CHECK_EQUAL( DOFMap[0], 3 );
  BOOST_CHECK_EQUAL( DOFMap[1], 4 );
  BOOST_CHECK_EQUAL( DOFMap[2], 5 );

  qfldCellGroup1.associativity(1).getEdgeGlobalMapping( DOFMap, 3 );
  BOOST_CHECK_EQUAL( DOFMap[0], 6 );
  BOOST_CHECK_EQUAL( DOFMap[1], 7 );
  BOOST_CHECK_EQUAL( DOFMap[2], 8 );

  BOOST_CHECK_EQUAL( qfld_local.nInteriorTraceGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_local.nBoundaryTraceGroups(), 0 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field1D_DG_Local_3Line_X1_ExtractBoundaryCell_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD1, TopoD1, ArrayQ > QField1D_DG_Line;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField1D xfld(3); //Linear mesh with 3 lines

  int order = 1;
  QField1D_DG_Line qfld(xfld, order, BasisFunctionCategory_Legendre);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD1,TopoD1> connectivity(xfld);

  //Extract the local mesh for the rightmost line
  XField_Local<PhysD1,TopoD1> xfld_local(comm_local,connectivity,0,2);

  Field_Local<QField1D_DG_Line> qfld_local(xfld_local, qfld, order, BasisFunctionCategory_Legendre);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  qfld_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_local.getXField(), &xfld_local );

  BOOST_CHECK_EQUAL( qfld_local.nDOF(), 4 );

  for (int i=0; i<2; i++)
  {
    BOOST_CHECK_EQUAL(qfld_local.DOF(0)[i], qfld.DOF(4)[i]);
    BOOST_CHECK_EQUAL(qfld_local.DOF(1)[i], qfld.DOF(5)[i]);

    BOOST_CHECK_EQUAL(qfld_local.DOF(2)[i], qfld.DOF(2)[i]);
    BOOST_CHECK_EQUAL(qfld_local.DOF(3)[i], qfld.DOF(3)[i]);
  }


  BOOST_CHECK_EQUAL( qfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( qfld_local.getCellGroupBase(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( qfld_local.getCellGroupBase(1).topoTypeID() == typeid(Line) );

  const QField1D_DG_Line::FieldCellGroupType<Line>& qfldCellGroup0 = qfld_local.getCellGroup<Line>(0);
  const QField1D_DG_Line::FieldCellGroupType<Line>& qfldCellGroup1 = qfld_local.getCellGroup<Line>(1);

  BOOST_CHECK_EQUAL( qfldCellGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.nElem(), 1 );

  int DOFMap[2];

  //Node DOFs
  BOOST_CHECK_EQUAL( qfldCellGroup0.associativity(0).nNode(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(0).nNode(), 0 );

  //Edge DOFs
  BOOST_CHECK_EQUAL( qfldCellGroup0.associativity(0).nEdge(), 2 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(0).nEdge(), 2 );

  qfldCellGroup0.associativity(0).getEdgeGlobalMapping( DOFMap, 2 );
  BOOST_CHECK_EQUAL( DOFMap[0], 0 );
  BOOST_CHECK_EQUAL( DOFMap[1], 1 );

  qfldCellGroup1.associativity(0).getEdgeGlobalMapping( DOFMap, 2 );
  BOOST_CHECK_EQUAL( DOFMap[0], 2 );
  BOOST_CHECK_EQUAL( DOFMap[1], 3 );

  BOOST_CHECK_EQUAL( qfld_local.nInteriorTraceGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_local.nBoundaryTraceGroups(), 0 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field1D_DG_Local_3Line_X1_ExtractBoundaryCell_P2 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD1, TopoD1, ArrayQ > QField1D_DG_Line;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField1D xfld(3); //Linear mesh with 3 lines

  int order = 2;
  QField1D_DG_Line qfld(xfld, order, BasisFunctionCategory_Legendre);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD1,TopoD1> connectivity(xfld);

  //Extract the local mesh for the rightmost line
  XField_Local<PhysD1,TopoD1> xfld_local(comm_local,connectivity,0,2);

  Field_Local<QField1D_DG_Line> qfld_local(xfld_local, qfld, order, BasisFunctionCategory_Legendre);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  qfld_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_local.getXField(), &xfld_local );

  BOOST_CHECK_EQUAL( qfld_local.nDOF(), 6 );

  for (int i=0; i<2; i++)
  {
    BOOST_CHECK_EQUAL(qfld_local.DOF(0)[i], qfld.DOF(6)[i]);
    BOOST_CHECK_EQUAL(qfld_local.DOF(1)[i], qfld.DOF(7)[i]);
    BOOST_CHECK_EQUAL(qfld_local.DOF(2)[i], qfld.DOF(8)[i]);

    BOOST_CHECK_EQUAL(qfld_local.DOF(3)[i], qfld.DOF(3)[i]);
    BOOST_CHECK_EQUAL(qfld_local.DOF(4)[i], qfld.DOF(4)[i]);
    BOOST_CHECK_EQUAL(qfld_local.DOF(5)[i], qfld.DOF(5)[i]);
  }


  BOOST_CHECK_EQUAL( qfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( qfld_local.getCellGroupBase(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( qfld_local.getCellGroupBase(1).topoTypeID() == typeid(Line) );

  const QField1D_DG_Line::FieldCellGroupType<Line>& qfldCellGroup0 = qfld_local.getCellGroup<Line>(0);
  const QField1D_DG_Line::FieldCellGroupType<Line>& qfldCellGroup1 = qfld_local.getCellGroup<Line>(1);

  BOOST_CHECK_EQUAL( qfldCellGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.nElem(), 1 );

  int DOFMap[3];

  //Node DOFs
  BOOST_CHECK_EQUAL( qfldCellGroup0.associativity(0).nNode(), 0 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(0).nNode(), 0 );

  //Edge DOFs
  BOOST_CHECK_EQUAL( qfldCellGroup0.associativity(0).nEdge(), 3 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.associativity(0).nEdge(), 3 );

  qfldCellGroup0.associativity(0).getEdgeGlobalMapping( DOFMap, 3 );
  BOOST_CHECK_EQUAL( DOFMap[0], 0 );
  BOOST_CHECK_EQUAL( DOFMap[1], 1 );
  BOOST_CHECK_EQUAL( DOFMap[2], 2 );

  qfldCellGroup1.associativity(0).getEdgeGlobalMapping( DOFMap, 3 );
  BOOST_CHECK_EQUAL( DOFMap[0], 3 );
  BOOST_CHECK_EQUAL( DOFMap[1], 4 );
  BOOST_CHECK_EQUAL( DOFMap[2], 5 );

  BOOST_CHECK_EQUAL( qfld_local.nInteriorTraceGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_local.nBoundaryTraceGroups(), 0 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field1D_DG_Local_3Line_X1_ExtractInteriorCell_InteriorTrace)
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_InteriorTrace< PhysD1, TopoD1, ArrayQ > QField1D_DG_ITrace;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField1D xfld(3); //Linear mesh with 3 lines

  int order = 0;
  QField1D_DG_ITrace qfld(xfld, order, BasisFunctionCategory_Legendre);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD1,TopoD1> connectivity(xfld);

  //Extract the local mesh for the interior line
  XField_Local<PhysD1,TopoD1> xfld_local(comm_local,connectivity,0,1);

  Field_Local<QField1D_DG_ITrace> qfld_local(xfld_local, qfld, order, BasisFunctionCategory_Legendre);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  qfld_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_local.getXField(), &xfld_local );

  BOOST_CHECK_EQUAL( qfld_local.nDOF(), 2 );

  for (int i=0; i<2; i++)
  {
    BOOST_CHECK_EQUAL(qfld_local.DOF(0)[i], qfld.DOF(1)[i]);
    BOOST_CHECK_EQUAL(qfld_local.DOF(1)[i], qfld.DOF(0)[i]);
  }

  BOOST_CHECK_EQUAL( qfld_local.nCellGroups(), 0 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( qfld_local.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( qfld_local.getInteriorTraceGroup<Node>(0).topoTypeID() == typeid(Node) );

  const QField1D_DG_ITrace::FieldTraceGroupType<Node>& qldITraceGroup = qfld_local.getInteriorTraceGroup<Node>(0);

  BOOST_CHECK_EQUAL( qldITraceGroup.nElem(), 2 );

  int DOFMap[1];

  //Node DOFs
  BOOST_CHECK_EQUAL( qldITraceGroup.associativity(0).nNode(), 1 );
  qldITraceGroup.associativity(0).getNodeGlobalMapping( DOFMap, 1 );
  BOOST_CHECK_EQUAL( DOFMap[0], 0 );

  BOOST_CHECK_EQUAL( qldITraceGroup.associativity(1).nNode(), 1 );
  qldITraceGroup.associativity(1).getNodeGlobalMapping( DOFMap, 1 );
  BOOST_CHECK_EQUAL( DOFMap[0], 1 );

  BOOST_CHECK_EQUAL( qfld_local.nBoundaryTraceGroups(), 0 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field1D_DG_Local_3Line_X1_ExtractBoundaryCell_InteriorTrace)
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_InteriorTrace< PhysD1, TopoD1, ArrayQ > QField1D_DG_ITrace;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField1D xfld(3); //Linear mesh with 3 lines

  int order = 0;
  QField1D_DG_ITrace qfld(xfld, order, BasisFunctionCategory_Legendre);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD1,TopoD1> connectivity(xfld);

  //Extract the local mesh for the interior line
  XField_Local<PhysD1,TopoD1> xfld_local(comm_local,connectivity,0,2);

  Field_Local<QField1D_DG_ITrace> qfld_local(xfld_local, qfld, order, BasisFunctionCategory_Legendre);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  qfld_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_local.getXField(), &xfld_local );

  BOOST_CHECK_EQUAL( qfld_local.nDOF(), 1 );

  for (int i=0; i<2; i++)
  {
    BOOST_CHECK_EQUAL(qfld_local.DOF(0)[i], qfld.DOF(1)[i]);
  }

  BOOST_CHECK_EQUAL( qfld_local.nCellGroups(), 0 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( qfld_local.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( qfld_local.getInteriorTraceGroup<Node>(0).topoTypeID() == typeid(Node) );

  const QField1D_DG_ITrace::FieldTraceGroupType<Node>& qldITraceGroup = qfld_local.getInteriorTraceGroup<Node>(0);

  BOOST_CHECK_EQUAL( qldITraceGroup.nElem(), 1 );

  int DOFMap[1];

  //Node DOFs
  BOOST_CHECK_EQUAL( qldITraceGroup.associativity(0).nNode(), 1 );
  qldITraceGroup.associativity(0).getNodeGlobalMapping( DOFMap, 1 );
  BOOST_CHECK_EQUAL( DOFMap[0], 0 );

  BOOST_CHECK_EQUAL( qfld_local.nBoundaryTraceGroups(), 0 );

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field1D_DG_Local_3Line_X1_ExtractInteriorCell_Trace)
{

  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Trace< PhysD1, TopoD1, ArrayQ > QField1D_DG_Trace;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField1D xfld(3); //Linear mesh with 3 lines

  int order = 0;
  QField1D_DG_Trace qfld(xfld, order, BasisFunctionCategory_Legendre);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD1,TopoD1> connectivity(xfld);

  //Extract the local mesh for the interior line
  XField_Local<PhysD1,TopoD1> xfld_local(comm_local,connectivity,0,1);

  Field_Local<QField1D_DG_Trace> qfld_local(xfld_local, qfld, order, BasisFunctionCategory_Legendre);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  qfld_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_local.getXField(), &xfld_local );

  BOOST_CHECK_EQUAL( qfld_local.nDOF(), 2 );

  for (int i=0; i<2; i++)
  {
    BOOST_CHECK_EQUAL(qfld_local.DOF(0)[i], qfld.DOF(1)[i]);
    BOOST_CHECK_EQUAL(qfld_local.DOF(1)[i], qfld.DOF(0)[i]);
  }

  BOOST_CHECK_EQUAL( qfld_local.nCellGroups(), 0 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( qfld_local.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( qfld_local.getInteriorTraceGroup<Node>(0).topoTypeID() == typeid(Node) );

  const QField1D_DG_Trace::FieldTraceGroupType<Node>& qldTraceGroup = qfld_local.getInteriorTraceGroup<Node>(0);

  BOOST_CHECK_EQUAL( qldTraceGroup.nElem(), 2 );

  int DOFMap[1];

  //Node DOFs
  BOOST_CHECK_EQUAL( qldTraceGroup.associativity(0).nNode(), 1 );
  qldTraceGroup.associativity(0).getNodeGlobalMapping( DOFMap, 1 );
  BOOST_CHECK_EQUAL( DOFMap[0], 0 );

  BOOST_CHECK_EQUAL( qldTraceGroup.associativity(1).nNode(), 1 );
  qldTraceGroup.associativity(1).getNodeGlobalMapping( DOFMap, 1 );
  BOOST_CHECK_EQUAL( DOFMap[0], 1 );

  BOOST_CHECK_EQUAL( qfld_local.nBoundaryTraceGroups(), 0 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field1D_DG_Local_3Line_X1_ExtractInteriorCell_BoundaryTrace)
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_BoundaryTrace< PhysD1, TopoD1, ArrayQ > QField1D_DG_BTrace;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField1D xfld(3); //Linear mesh with 3 lines

  int order = 0;
  QField1D_DG_BTrace qfld(xfld, order, BasisFunctionCategory_Legendre);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD1,TopoD1> connectivity(xfld);

  //Extract the local mesh for the interior line
  XField_Local<PhysD1,TopoD1> xfld_local(comm_local,connectivity,0,1);

  Field_Local<QField1D_DG_BTrace> qfld_local(xfld_local, qfld, order, BasisFunctionCategory_Legendre);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  qfld_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_local.getXField(), &xfld_local );

  BOOST_CHECK_EQUAL( qfld_local.nDOF(), 0 );

  BOOST_CHECK_EQUAL( qfld_local.nCellGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_local.nInteriorTraceGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_local.nBoundaryTraceGroups(), 0 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field1D_DG_Local_3Line_X1_ExtractBoundaryCell_BoundaryTrace)
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_BoundaryTrace< PhysD1, TopoD1, ArrayQ > QField1D_DG_BTrace;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField1D xfld(3); //Linear mesh with 3 lines

  int order = 0;
  QField1D_DG_BTrace qfld(xfld, order, BasisFunctionCategory_Legendre);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD1,TopoD1> connectivity(xfld);

  //Extract the local mesh for the interior line
  XField_Local<PhysD1,TopoD1> xfld_local(comm_local,connectivity,0,2);

  Field_Local<QField1D_DG_BTrace> qfld_local(xfld_local, qfld, order, BasisFunctionCategory_Legendre);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  qfld_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_local.getXField(), &xfld_local );

  BOOST_CHECK_EQUAL( qfld_local.nDOF(), 1 );

  for (int i=0; i<2; i++)
  {
    BOOST_CHECK_EQUAL(qfld_local.DOF(0)[i], qfld.DOF(1)[i]);
  }

  BOOST_CHECK_EQUAL( qfld_local.nCellGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_local.nInteriorTraceGroups(), 0 );

  // boundary-node field variable
  BOOST_CHECK_EQUAL( qfld_local.nBoundaryTraceGroups(), 1 );
  BOOST_REQUIRE( qfld_local.getBoundaryTraceGroup<Node>(0).topoTypeID() == typeid(Node) );

  const QField1D_DG_BTrace::FieldTraceGroupType<Node>& qldBTraceGroup0 = qfld_local.getBoundaryTraceGroup<Node>(0);

  BOOST_CHECK_EQUAL( qldBTraceGroup0.nElem(), 1 );

  int DOFMap[1];

  //Node DOFs
  BOOST_CHECK_EQUAL( qldBTraceGroup0.associativity(0).nNode(), 1 );
  qldBTraceGroup0.associativity(0).getNodeGlobalMapping( DOFMap, 1 );
  BOOST_CHECK_EQUAL( DOFMap[0], 0 );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
