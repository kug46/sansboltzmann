// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// XField2D_Local_btest
// testing of XField2D_Local
//
// Note: unit grids tested in "unit/UnitGrids/UnitGrids_btest.cpp"

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"     // Real
#include "Topology/ElementTopology.h"

#include "Field/XField_CellToTrace.h"
#include "Field/XField_EdgeLocal.h"
#include "Field/Field_NodalView.h"

#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_LocalRefine_Triangle_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( XField2D_EdgeLocal_Split_test_suite )

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_Local_2Triangle_X1_1Group_InteriorTrace_Target_Primal_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  typedef std::array<int,3> Int3;
  Real small_tol = 1e-12, close_tol = 1e-12;

  XField2D_2Triangle_X1_1Group xfld;

  // Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // connecting nodes
  std::pair<int,int> nodes(1,2);

  //Extract the local mesh for the bottom-left triangle
  XField_EdgeLocal<PhysD2,TopoD2> xfld_local(comm_local,connectivity,nodalview,nodes,Neighborhood::Primal,true);

#if 0
  xfld.dump(3,std::cout);
  std::cout<<std::endl;
  xfld_local.dump(3,std::cout);
#endif

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.getConnectivity() );

  //Check local to global mappings
  BOOST_CHECK( xfld_local.getGlobalCellMap({0,0}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_local.getGlobalCellMap({0,1}) == std::make_pair(0,1));
  BOOST_CHECK( xfld_local.getGlobalCellMap({0,2}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_local.getGlobalCellMap({0,3}) == std::make_pair(0,1));

  BOOST_CHECK( xfld_local.getGlobalInteriorTraceMap({0,0}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_local.getGlobalInteriorTraceMap({0,1}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_local.getGlobalInteriorTraceMap({0,2}) == std::make_pair(-1,-1));
  BOOST_CHECK( xfld_local.getGlobalInteriorTraceMap({0,3}) == std::make_pair(-1,-2));

  BOOST_CHECK( xfld_local.getGlobalBoundaryTraceMap({0,0}) == std::make_pair(0,3));
  BOOST_CHECK( xfld_local.getGlobalBoundaryTraceMap({1,0}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_local.getGlobalBoundaryTraceMap({2,0}) == std::make_pair(0,1));
  BOOST_CHECK( xfld_local.getGlobalBoundaryTraceMap({3,0}) == std::make_pair(0,2));

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 5 );

  //Check the node values
  SANS_CHECK_CLOSE( xfld_local.DOF(0)[0],   1., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(0)[1],   0., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(1)[0],   0., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(1)[1],   1., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(2)[0],   0., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(2)[1],   0., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(3)[0],   1., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(3)[1],   1., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(4)[0],  0.5, small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(4)[1],  0.5, small_tol, close_tol );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_local.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_local.getCellGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 4 );

  int nodeMap[3];

  //Node DOFs
  xfldCellGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 0 );
  BOOST_CHECK_EQUAL( nodeMap[2], 4 );

  xfldCellGroup0.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );
  BOOST_CHECK_EQUAL( nodeMap[2], 0 );


  xfldCellGroup0.associativity(2).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );
  BOOST_CHECK_EQUAL( nodeMap[2], 1 );

  xfldCellGroup0.associativity(3).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );
  BOOST_CHECK_EQUAL( nodeMap[2], 4 );

  Int3 edgeSign;
  edgeSign = xfldCellGroup0.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  edgeSign = xfldCellGroup0.associativity(1).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], -1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  edgeSign = xfldCellGroup0.associativity(2).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], -1 );

  edgeSign = xfldCellGroup0.associativity(3).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], -1 );
  BOOST_CHECK_EQUAL( edgeSign[1], -1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup = xfld_local.getInteriorTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldITraceGroup.nElem(), 4 );

  //Node DOFs
  xfldITraceGroup.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );

  xfldITraceGroup.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 4 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );

  xfldITraceGroup.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 4 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );

  xfldITraceGroup.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );

  // interior edge-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldITraceGroup.getGroupLeft() , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getGroupRight(), 0 );

  BOOST_CHECK_EQUAL( xfldITraceGroup.getElementLeft(0) , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getElementRight(0), 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceLeft(0) .trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceLeft(0) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceRight(0).trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceRight(0).orientation,-1 );

  BOOST_CHECK_EQUAL( xfldITraceGroup.getElementLeft(1) , 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getElementRight(1), 3 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceLeft(1) .trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceLeft(1) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceRight(1).trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceRight(1).orientation,-1 );

  BOOST_CHECK_EQUAL( xfldITraceGroup.getElementLeft(2) , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getElementRight(2), 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceLeft(2) .trace      , 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceLeft(2) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceRight(2).trace      , 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceRight(2).orientation,-1 );

  BOOST_CHECK_EQUAL( xfldITraceGroup.getElementLeft(3) , 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getElementRight(3), 3 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceLeft(3) .trace      , 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceLeft(3) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceRight(3).trace      , 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceRight(3).orientation,-1 );

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nBoundaryTraceGroups(), 4 );

  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(1).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(2).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(3).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup1 = xfld_local.getBoundaryTraceGroup<Line>(1);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup2 = xfld_local.getBoundaryTraceGroup<Line>(2);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup3 = xfld_local.getBoundaryTraceGroup<Line>(3);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.nElem(), 1 );

  //Node DOFs
  xfldBTraceGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );

  xfldBTraceGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 0 );

  xfldBTraceGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );

  xfldBTraceGroup3.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getGroupRight(), -1 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementLeft(0) , 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementRight(0),-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(0) .trace      , 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(0) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceRight(0).trace      ,-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceRight(0).orientation, 0 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getGroupRight(), -1 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getElementLeft(0) , 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getElementRight(0),-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceLeft(0) .trace      , 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceLeft(0) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceRight(0).trace      ,-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceRight(0).orientation, 0 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getGroupRight(), -1 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getElementLeft(0) , 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getElementRight(0),-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getCanonicalTraceLeft(0) .trace      , 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getCanonicalTraceLeft(0) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getCanonicalTraceRight(0).trace      ,-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getCanonicalTraceRight(0).orientation, 0 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup3.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.getGroupRight(), -1 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup3.getElementLeft(0) , 3 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.getElementRight(0),-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.getCanonicalTraceLeft(0) .trace      , 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.getCanonicalTraceLeft(0) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.getCanonicalTraceRight(0).trace      ,-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.getCanonicalTraceRight(0).orientation, 0 );

}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_Local_2Triangle_X1_1Group_BoundaryTrace_1_Target_Primal_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  typedef std::array<int,3> Int3;
  Real small_tol = 1e-12, close_tol = 1e-12;

  XField2D_2Triangle_X1_1Group xfld;

  // Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  // Build not to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // connecting nodes
  std::pair<int,int> nodes(0,1); // bottom edge

  //Extract the local mesh for the bottom-left triangle
  XField_EdgeLocal<PhysD2,TopoD2> xfld_local(comm_local,connectivity,nodalview,nodes,Neighborhood::Primal,true);

#if 0
  xfld.dump(3,std::cout);
  std::cout<<std::endl;
  xfld_local.dump(3,std::cout);
#endif

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.getConnectivity() );

  //Check local to global mappings
  BOOST_CHECK( xfld_local.getGlobalCellMap({0,0}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_local.getGlobalCellMap({0,1}) == std::make_pair(0,0));

  BOOST_CHECK( xfld_local.getGlobalInteriorTraceMap({0,0}) == std::make_pair(-1,-1));

  BOOST_CHECK( xfld_local.getGlobalBoundaryTraceMap({0,0}) == std::make_pair(0,3));
  BOOST_CHECK( xfld_local.getGlobalBoundaryTraceMap({1,0}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_local.getGlobalBoundaryTraceMap({1,1}) == std::make_pair(0,0));

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 4 );

  //Check the node values
  SANS_CHECK_CLOSE( xfld_local.DOF(0)[0],   0., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(0)[1],  0., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(1)[0],   1., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(1)[1],  0., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(2)[0],   0., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(2)[1],  1., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(3)[0],  0.5, small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(3)[1],  0., small_tol, close_tol );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_local.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_local.getCellGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );
  int nodeMap[3];

  //Node DOFs
  xfldCellGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 );

  xfldCellGroup0.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 );

  Int3 edgeSign;
  edgeSign = xfldCellGroup0.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  edgeSign = xfldCellGroup0.associativity(1).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], -1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup0 = xfld_local.getInteriorTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1 );

  //Node DOFs
  xfldITraceGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );

  // interior edge-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getGroupLeft() , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getGroupRight(), 0 );

  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementLeft(0) , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementRight(0), 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(0) .trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(0) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(0).trace      , 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(0).orientation,-1 );

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nBoundaryTraceGroups(), 2 );

  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(1).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup1 = xfld_local.getBoundaryTraceGroup<Line>(1);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.nElem(), 2 );

  //Node DOFs

  xfldBTraceGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 0 );

  xfldBTraceGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );

  xfldBTraceGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getGroupRight(), -1 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementLeft(0) , 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementRight(0),-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(0) .trace      , 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(0) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceRight(0).trace      ,-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceRight(0).orientation, 0 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getGroupRight(), -1 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getElementLeft(0) , 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getElementRight(0),-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceLeft(0) .trace      , 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceLeft(0) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceRight(0).trace      ,-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceRight(0).orientation, 0 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getElementLeft(1) , 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getElementRight(1),-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceLeft(0) .trace      , 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceLeft(0) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceRight(0).trace      ,-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceRight(0).orientation, 0 );

}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_Local_2Box_X1_1Group_BoundaryTrace_1_Target_Primal_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  typedef std::array<int,3> Int3;
  Real small_tol = 1e-12, close_tol = 1e-12;

  XField2D_Box_Triangle_X1 xfld(2,2,0,2,0,2);

  // Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  // Build not to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // connecting nodes
  std::pair<int,int> nodes(0,1); // bottom edge

  //Extract the local mesh for the bottom-left triangle
  XField_EdgeLocal<PhysD2,TopoD2> xfld_local(comm_local,connectivity,nodalview,nodes,Neighborhood::Primal,true);

#if 0
  xfld.dump(3,std::cout);
  std::cout<<std::endl;
  xfld_local.dump(3,std::cout);
#endif

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.getConnectivity() );

  //Check local to global mappings
  BOOST_CHECK( xfld_local.getGlobalCellMap({0,0}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_local.getGlobalCellMap({0,1}) == std::make_pair(0,0));

  BOOST_CHECK( xfld_local.getGlobalInteriorTraceMap({0,0}) == std::make_pair(-1,-1));

  BOOST_CHECK( xfld_local.getGlobalBoundaryTraceMap({0,0}) == std::make_pair(3,1));
  BOOST_CHECK( xfld_local.getGlobalBoundaryTraceMap({1,0}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_local.getGlobalBoundaryTraceMap({1,1}) == std::make_pair(0,0));

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 4 );

  //Check the node values
  SANS_CHECK_CLOSE( xfld_local.DOF(0)[0],   0., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(0)[1],  0., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(1)[0],   1., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(1)[1],  0., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(2)[0],   0., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(2)[1],  1., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(3)[0],  0.5, small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(3)[1],  0., small_tol, close_tol );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_local.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_local.getCellGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );
  int nodeMap[3];

  //Node DOFs
  xfldCellGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 );

  xfldCellGroup0.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 );

  Int3 edgeSign;
  edgeSign = xfldCellGroup0.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  edgeSign = xfldCellGroup0.associativity(1).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], -1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup0 = xfld_local.getInteriorTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1 );

  //Node DOFs
  xfldITraceGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );

  // interior edge-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getGroupLeft() , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getGroupRight(), 0 );

  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementLeft(0) , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementRight(0), 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(0) .trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(0) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(0).trace      , 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(0).orientation,-1 );

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nBoundaryTraceGroups(), 2 );

  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(1).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup1 = xfld_local.getBoundaryTraceGroup<Line>(1);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.nElem(), 2 );

  //Node DOFs

  xfldBTraceGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 0 );

  xfldBTraceGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );

  xfldBTraceGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getGroupRight(), -1 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementLeft(0) , 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementRight(0),-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(0) .trace      , 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(0) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceRight(0).trace      ,-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceRight(0).orientation, 0 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getGroupRight(), -1 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getElementLeft(0) , 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getElementRight(0),-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceLeft(0) .trace      , 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceLeft(0) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceRight(0).trace      ,-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceRight(0).orientation, 0 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getElementLeft(1) , 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getElementRight(1),-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceLeft(0) .trace      , 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceLeft(0) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceRight(0).trace      ,-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceRight(0).orientation, 0 );

}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_Local_2Triangle_X1_1Group_BoundaryTrace_2_Target_Primal_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  typedef std::array<int,3> Int3;

  XField2D_2Triangle_X1_1Group xfld;

  // Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  // Build not to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // connecting nodes
  std::pair<int,int> nodes(1,3);

  //Extract the local mesh for the bottom-left triangle
  XField_EdgeLocal<PhysD2,TopoD2> xfld_local(comm_local,connectivity,nodalview,nodes,Neighborhood::Primal,true);

#if 0
  xfld.dump(3,std::cout);
  std::cout<<std::endl;
  xfld_local.dump(3,std::cout);
#endif

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.getConnectivity() );

  //Check local to global mappings
  BOOST_CHECK( xfld_local.getGlobalCellMap({0,0}) == std::make_pair(0,1));
  BOOST_CHECK( xfld_local.getGlobalCellMap({0,1}) == std::make_pair(0,1));

  BOOST_CHECK( xfld_local.getGlobalInteriorTraceMap({0,0}) == std::make_pair(-1,-1));

  BOOST_CHECK( xfld_local.getGlobalBoundaryTraceMap({0,0}) == std::make_pair(0,1));
  BOOST_CHECK( xfld_local.getGlobalBoundaryTraceMap({0,1}) == std::make_pair(0,1));
  BOOST_CHECK( xfld_local.getGlobalBoundaryTraceMap({1,0}) == std::make_pair(0,2));

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 4 );

  //Check the node values, first three added by first elem
  Real small_tol = 1e-13, close_tol = 1e-13;
  SANS_CHECK_CLOSE( xfld_local.DOF(0)[0],  1., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(0)[1],  0., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(1)[0],  1., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(1)[1],  1., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(2)[0],  0., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(2)[1],  1., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(3)[0],  1., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(3)[1],  0.5, small_tol, close_tol );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_local.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_local.getCellGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );
  int nodeMap[3];

  //Node DOFs
  xfldCellGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );
  BOOST_CHECK_EQUAL( nodeMap[2], 0 );

  xfldCellGroup0.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );
  BOOST_CHECK_EQUAL( nodeMap[2], 3 );

  Int3 edgeSign;
  edgeSign = xfldCellGroup0.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  edgeSign = xfldCellGroup0.associativity(1).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], -1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup0 = xfld_local.getInteriorTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1 );

  //Node DOFs

  xfldITraceGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );

  // interior edge-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getGroupLeft() , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getGroupRight(), 0 );

  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementLeft(0) , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementRight(0), 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(0) .trace      , 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(0) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(0).trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(0).orientation,-1 );

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nBoundaryTraceGroups(), 2 );

  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(1).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup1 = xfld_local.getBoundaryTraceGroup<Line>(1);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.nElem(), 1 );

  //Node DOFs

  xfldBTraceGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );

  xfldBTraceGroup0.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );

  xfldBTraceGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getGroupRight(), -1 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementLeft(0) , 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementRight(0),-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(0) .trace      , 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(0) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceRight(0).trace      ,-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceRight(0).orientation, 0 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementLeft(1) , 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementRight(1),-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(1) .trace      , 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(1) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceRight(1).trace      ,-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceRight(1).orientation, 0 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getGroupRight(), -1 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getElementLeft(0) , 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getElementRight(0),-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceLeft(0) .trace      , 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceLeft(0) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceRight(0).trace      ,-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceRight(0).orientation, 0 );

}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_Local_4Triangle_X1_1Group_InteriorEdge0_Target_Primal_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  typedef std::array<int,3> Int3;

  XField2D_4Triangle_X1_1Group xfld;

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // connecting nodes
  std::pair<int,int> nodes(1,2);

  //Extract the local mesh for the central triangle
  XField_EdgeLocal<PhysD2,TopoD2> xfld_local(comm_local,connectivity,nodalview,nodes,Neighborhood::Primal,true);

#if 0
  xfld.dump(3,std::cout);
  std::cout<<std::endl;
  xfld_local.dump(3,std::cout);
#endif

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.getConnectivity() );

  //Check local to global mappings
  BOOST_CHECK( xfld_local.getGlobalCellMap({0,0}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_local.getGlobalCellMap({0,1}) == std::make_pair(0,1));
  BOOST_CHECK( xfld_local.getGlobalCellMap({0,2}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_local.getGlobalCellMap({0,3}) == std::make_pair(0,1));

  BOOST_CHECK( xfld_local.getGlobalInteriorTraceMap({0,0}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_local.getGlobalInteriorTraceMap({0,1}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_local.getGlobalInteriorTraceMap({0,2}) == std::make_pair(-1,-1));
  BOOST_CHECK( xfld_local.getGlobalInteriorTraceMap({0,3}) == std::make_pair(-1,-2));

  BOOST_CHECK( xfld_local.getGlobalBoundaryTraceMap({0,0}) == std::make_pair(0,2));
  BOOST_CHECK( xfld_local.getGlobalBoundaryTraceMap({1,0}) == std::make_pair(0,3));

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 5 );

  //Check the node values
  Real small_tol = 1e-13, close_tol = 1e-13;
  SANS_CHECK_CLOSE( xfld_local.DOF(0)[0],  1., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(0)[1],  0., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(1)[0],  0., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(1)[1],  1., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(2)[0],  0., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(2)[1],  0., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(3)[0],  1., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(3)[1],  1., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(4)[0], 0.5, small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(4)[1], 0.5, small_tol, close_tol );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_local.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_local.getCellGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 4 );

  int nodeMap[3];

  //Node DOFs
  xfldCellGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 0 );
  BOOST_CHECK_EQUAL( nodeMap[2], 4 );

  xfldCellGroup0.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );
  BOOST_CHECK_EQUAL( nodeMap[2], 0 );

  xfldCellGroup0.associativity(2).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );
  BOOST_CHECK_EQUAL( nodeMap[2], 1 );

  xfldCellGroup0.associativity(3).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );
  BOOST_CHECK_EQUAL( nodeMap[2], 4 );

  Int3 edgeSign;

  edgeSign = xfldCellGroup0.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  edgeSign = xfldCellGroup0.associativity(1).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], -1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  edgeSign = xfldCellGroup0.associativity(2).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], -1 );

  edgeSign = xfldCellGroup0.associativity(3).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], -1 );
  BOOST_CHECK_EQUAL( edgeSign[1], -1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup0 = xfld_local.getInteriorTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 4 );

  //Node DOFs

  xfldITraceGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );

  xfldITraceGroup0.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 4 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );

  xfldITraceGroup0.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 4 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );

  xfldITraceGroup0.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );

  // interior edge-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getGroupLeft() , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getGroupRight(), 0 );

  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementLeft(0) , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementRight(0), 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(0) .trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(0) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(0).trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(0).orientation,-1 );

  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementLeft(1) , 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementRight(1), 3 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(1) .trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(1) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(1).trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(1).orientation,-1 );

  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementLeft(2) , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementRight(2), 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(2) .trace      , 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(2) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(2).trace      , 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(2).orientation,-1 );

  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementLeft(3) , 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementRight(3), 3 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(3) .trace      , 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(3) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(3).trace      , 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(3).orientation,-1 );


  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nBoundaryTraceGroups(), 2 );

  for (int i = 0; i < 2; i++)
    BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(i).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup1 = xfld_local.getBoundaryTraceGroup<Line>(1);

  //Node DOFs

  xfldBTraceGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );

  xfldBTraceGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getGroupRight(), -1 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementLeft(0) , 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementRight(0),-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(0) .trace      , 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(0) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceRight(0).trace      ,-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceRight(0).orientation, 0 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getGroupRight(), -1 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getElementLeft(0) , 3 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getElementRight(0),-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceLeft(0) .trace      , 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceLeft(0) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceRight(0).trace      ,-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceRight(0).orientation, 0 );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_Local_4Triangle_X1_1Group_BoundaryEdge5_Target_Primal_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  typedef std::array<int,3> Int3;

  XField2D_4Triangle_X1_1Group xfld;

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // connecting nodes
  std::pair<int,int> nodes(4,0);

  //Extract the local mesh for the central triangle
  XField_EdgeLocal<PhysD2,TopoD2> xfld_local(comm_local,connectivity,nodalview,nodes,Neighborhood::Primal,true);

#if 0
  xfld.dump(3,std::cout);
  std::cout<<std::endl;
  xfld_local.dump(3,std::cout);
#endif

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.getConnectivity() );

  //Check local to global mappings
  BOOST_CHECK( xfld_local.getGlobalCellMap({0,0}) == std::make_pair(0,2));
  BOOST_CHECK( xfld_local.getGlobalCellMap({0,1}) == std::make_pair(0,2));

  BOOST_CHECK( xfld_local.getGlobalInteriorTraceMap({0,0}) == std::make_pair(-1,-1));

  BOOST_CHECK( xfld_local.getGlobalBoundaryTraceMap({0,0}) == std::make_pair(0,5));
  BOOST_CHECK( xfld_local.getGlobalBoundaryTraceMap({0,1}) == std::make_pair(0,5));
  BOOST_CHECK( xfld_local.getGlobalBoundaryTraceMap({1,0}) == std::make_pair(0,4));

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 4 );

  //Check the node values
  Real small_tol = 1e-13, close_tol = 1e-13;
  SANS_CHECK_CLOSE( xfld_local.DOF(0)[0],  -1., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(0)[1],  1., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(1)[0],   0., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(1)[1],  0., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(2)[0],   0., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(2)[1],  1., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(3)[0], -0.5, small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(3)[1], 0.5, small_tol, close_tol );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_local.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_local.getCellGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );

  int nodeMap[3];

  //Node DOFs
  xfldCellGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 0 );
  BOOST_CHECK_EQUAL( nodeMap[2], 3 );

  xfldCellGroup0.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );
  BOOST_CHECK_EQUAL( nodeMap[2], 1 );

  Int3 edgeSign;

  edgeSign = xfldCellGroup0.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  edgeSign = xfldCellGroup0.associativity(1).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], -1 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup0 = xfld_local.getInteriorTraceGroup<Line>(0);

  //Node DOFs

  xfldITraceGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );

  // interior edge-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getGroupLeft() , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getGroupRight(), 0 );

  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementLeft(0) , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementRight(0), 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(0) .trace      , 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(0) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(0).trace      , 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(0).orientation,-1 );

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nBoundaryTraceGroups(), 2 );

  for (int i = 0; i < 2; i++)
    BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(i).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup1 = xfld_local.getBoundaryTraceGroup<Line>(1);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.nElem(), 1 );

  //Node DOFs

  xfldBTraceGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );

  xfldBTraceGroup0.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );

  xfldBTraceGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 0 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getGroupRight(), -1 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementLeft(0) , 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementRight(0),-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(0) .trace      , 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(0) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceRight(0).trace      ,-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceRight(0).orientation, 0 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementLeft(1) , 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementRight(1),-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(1) .trace      , 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(1) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceRight(1).trace      ,-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceRight(1).orientation, 0 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getGroupRight(), -1 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getElementLeft(0) , 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getElementRight(0),-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceLeft(0) .trace      , 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceLeft(0) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceRight(0).trace      ,-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceRight(0).orientation, 0 );

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );

  for (int i = 0; i < 1; i++)
    BOOST_REQUIRE( xfld_local.getGhostBoundaryTraceGroup<Line>(i).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldGBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 1 );

  //Node DOFs
  xfldGBTraceGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getGroupRight(), -1 );

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getElementLeft(0) , 1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getElementRight(0),-1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceLeft(0) .trace      , 1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceLeft(0) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceRight(0).trace      ,-1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceRight(0).orientation, 0 );
}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_Local_UnionJack_LocalRefine_InteriorEdge0_Target_Primal_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  typedef std::array<int,3> Int3;

  int ii = 3, jj = 3;
  bool refine = false;
  // make it ([0,3],[0,3]) so the node locations are easier
  XField2D_Box_UnionJack_LocalRefine_Triangle_X1 xfld( ii, jj, refine, 0, 3, 0, 3 );

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // connecting nodes
  std::pair<int,int> nodes(11,5);

  typedef XField_EdgeLocal<PhysD2,TopoD2> XField_EdgeLocal2D;

  // check that non matching nodes throws exceptions
  BOOST_CHECK_THROW( XField_EdgeLocal2D xfld_local(comm_local,connectivity,nodalview,nodes,Neighborhood::Primal,true), AssertionException );

  //Extract the local mesh for the central triangle
  nodes = std::make_pair(10,5);
  XField_EdgeLocal2D xfld_local(comm_local,connectivity,nodalview,nodes,Neighborhood::Primal,true);

#if 0
  xfld.dump(3,std::cout);
  std::cout<<std::endl;
  xfld_local.dump(3,std::cout);
#endif

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.getConnectivity() );

  //Check local to global mappings
  BOOST_CHECK( xfld_local.getGlobalCellMap({0,0}) == std::make_pair(0,8));
  BOOST_CHECK( xfld_local.getGlobalCellMap({0,1}) == std::make_pair(0,9));
  BOOST_CHECK( xfld_local.getGlobalCellMap({0,2}) == std::make_pair(0,8));
  BOOST_CHECK( xfld_local.getGlobalCellMap({0,3}) == std::make_pair(0,9));

  BOOST_CHECK( xfld_local.getGlobalInteriorTraceMap({0,0}) == std::make_pair(2,5));
  BOOST_CHECK( xfld_local.getGlobalInteriorTraceMap({0,1}) == std::make_pair(2,5));
  BOOST_CHECK( xfld_local.getGlobalInteriorTraceMap({0,2}) == std::make_pair(-1,-1));
  BOOST_CHECK( xfld_local.getGlobalInteriorTraceMap({0,3}) == std::make_pair(-1,-2));

  // actual boundaries

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 5 );

  //Check the node values
  Real small_tol = 1e-12, close_tol = 1e-12;
  SANS_CHECK_CLOSE( xfld_local.DOF(0)[0],  2., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(0)[1],  2., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(1)[0],  1., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(1)[1],  1., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(2)[0],  1., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(2)[1],  2., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(3)[0],  2., small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(3)[1],  1., small_tol, close_tol );
  SANS_CHECK_CLOSE( xfld_local.DOF(4)[0], 1.5, small_tol, close_tol );  SANS_CHECK_CLOSE( xfld_local.DOF(4)[1], 1.5, small_tol, close_tol );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_local.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_local.getCellGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 4 );

  int nodeMap[3];

  //Node DOFs
  // Group 0 is pretty much always right by construction in these tests
  xfldCellGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );
  BOOST_CHECK_EQUAL( nodeMap[2], 0 );

  xfldCellGroup0.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 0 );
  BOOST_CHECK_EQUAL( nodeMap[2], 4 );

  xfldCellGroup0.associativity(2).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );
  BOOST_CHECK_EQUAL( nodeMap[2], 4 );

  xfldCellGroup0.associativity(3).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );
  BOOST_CHECK_EQUAL( nodeMap[2], 1 );

  Int3 edgeSign;

  edgeSign = xfldCellGroup0.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  edgeSign = xfldCellGroup0.associativity(1).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], -1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  edgeSign = xfldCellGroup0.associativity(2).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], -1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  edgeSign = xfldCellGroup0.associativity(3).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], -1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], -1 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nInteriorTraceGroups(), 1 );
  for (int i = 0; i < 1; i++)
    BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Line>(i).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup0 = xfld_local.getInteriorTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 4 );

  //Node DOFs
  xfldITraceGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 4 );
  BOOST_CHECK_EQUAL( nodeMap[1], 0 );

  xfldITraceGroup0.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );

  xfldITraceGroup0.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );

  xfldITraceGroup0.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 4 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );

  // interior edge-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getGroupLeft() , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getGroupRight(), 0 );

  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementLeft(0) , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementRight(0), 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(0) .trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(0) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(0).trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(0).orientation,-1 );

  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementLeft(1) , 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementRight(1), 3 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(1) .trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(1) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(1).trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(1).orientation,-1 );

  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementLeft(2) , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementRight(2), 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(2) .trace      , 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(2) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(2).trace      , 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(2).orientation,-1 );

  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementLeft(3) , 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getElementRight(3), 3 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(3) .trace      , 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceLeft(3) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(3).trace      , 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup0.getCanonicalTraceRight(3).orientation,-1 );

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nBoundaryTraceGroups(), 0 );

  for (int i = 0; i < 0; i++)
    BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(i).topoTypeID() == typeid(Line) );

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );

  for (int i = 0; i < 1; i++)
    BOOST_REQUIRE( xfld_local.getGhostBoundaryTraceGroup<Line>(i).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldGBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup<Line>(0);

  // outer boundary
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 4 );

  //Node DOFs
  xfldGBTraceGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 );

  xfldGBTraceGroup0.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 );

  xfldGBTraceGroup0.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );

  xfldGBTraceGroup0.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 0 );

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getGroupRight(), -1 );

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getElementLeft(0) , 0 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getElementRight(0),-1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceLeft(0) .trace      , 1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceLeft(0) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceRight(0).trace      ,-1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceRight(0).orientation, 0 );

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getElementLeft(1) , 2 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getElementRight(1),-1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceLeft(1) .trace      , 2 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceLeft(1) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceRight(1).trace      ,-1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceRight(1).orientation, 0 );

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getElementLeft(2) , 3 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getElementRight(2),-1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceLeft(2) .trace      , 1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceLeft(2) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceRight(2).trace      ,-1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceRight(2).orientation, 0 );

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getElementLeft(3) , 1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getElementRight(3),-1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceLeft(3) .trace      , 2 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceLeft(3) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceRight(3).trace      ,-1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceRight(3).orientation, 0 );
}
#endif
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
