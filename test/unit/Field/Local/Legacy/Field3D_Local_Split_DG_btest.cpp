// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Field3D_Local_Split_DG_btest
//

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "unit/UnitGrids/XField3D_2Tet_X1_1Group.h"

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunctionVolume_Tetrahedron.h"

#include "Field/XFieldVolume_Local_Split.h"

#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_InteriorTrace.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/Local/Field_Local.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

//Explicitly instantiate classes to get proper coverage information
namespace SANS
{
template class Field_DG_Cell< PhysD3, TopoD3, Real >;
template class Field_DG_InteriorTrace< PhysD3, TopoD3, Real >;
template class Field_DG_BoundaryTrace< PhysD3, TopoD3, Real >;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( Field3D_Local_Split_DG_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field3D_DG_Local_2Tet_X1_ExtractRightCell_SplitEdge0_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD3, TopoD3, ArrayQ > QField3D_DG_Volume;

  typedef DLA::VectorS<TopoD3::D,Real> RefCoordType;

  typedef QField3D_DG_Volume::FieldCellGroupType<Tet> FieldCellGroupType;
  typedef FieldCellGroupType::ElementType<> ElementFieldClass;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  Real tol = 1e-11;

  XField3D_2Tet_X1_1Group xfld;

  int order = 1;
  QField3D_DG_Volume qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  //Extract the local mesh for the right tet
  int main_group = 0;
  int main_elem = 0;
  XField_Local<PhysD3,TopoD3> xfld_local(comm_local,connectivity,main_group,main_elem);

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity_local(xfld_local);

  int split_edge_index = 0;
  XField_Local_Split<PhysD3,TopoD3> xfld_split_local(xfld_local, connectivity_local,
                                                     ElementSplitType::Edge, split_edge_index);

  Field_Local<QField3D_DG_Volume> qfld_split_local(xfld_split_local, qfld, order, BasisFunctionCategory_Hierarchical);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  qfld_split_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_split_local.getXField(), &xfld_split_local );

  BOOST_CHECK_EQUAL( qfld_split_local.nDOF(), 16 );

  //Get global element
  const FieldCellGroupType& global_cellgrp = qfld.getCellGroup<Tet>(main_group);
  ElementFieldClass fldElem_global( global_cellgrp.basis() );
  global_cellgrp.getElement(fldElem_global, main_elem);

  const FieldCellGroupType& local_cellgrp = qfld_split_local.getCellGroup<Tet>(0);
  ElementFieldClass fldElem_local0( global_cellgrp.basis() );
  ElementFieldClass fldElem_local1( global_cellgrp.basis() );
  local_cellgrp.getElement(fldElem_local0, 0);
  local_cellgrp.getElement(fldElem_local1, 1);

  //Get order 3 quad points
  Quadrature<TopoD3, Tet> quadrature(3);
  const int nquad = quadrature.nQuadrature();

  RefCoordType Ref_sub;  // reference-element coordinates on sub-element
  RefCoordType Ref_main; // reference-element coordinates on main-element
  ArrayQ qeval_main0, qeval_main1, qeval_sub0, qeval_sub1;

  //Check if the solutions are equal at a few quadrature points
  for (int iquad = 0; iquad < nquad; iquad++)
  {
    quadrature.coordinates( iquad, Ref_sub );

    fldElem_local0.eval(Ref_sub,qeval_sub0);
    fldElem_local1.eval(Ref_sub,qeval_sub1);

    BasisFunction_RefElement_Split<TopoD3, Tet>::transform(Ref_sub, ElementSplitType::Edge, split_edge_index, 0, Ref_main);
    fldElem_global.eval(Ref_main, qeval_main0);

    BasisFunction_RefElement_Split<TopoD3, Tet>::transform(Ref_sub, ElementSplitType::Edge, split_edge_index, 1, Ref_main);
    fldElem_global.eval(Ref_main, qeval_main1);

    for (int i=0; i<2; i++)
    {
      SANS_CHECK_CLOSE(qeval_sub0[i], qeval_main0[i], tol, tol);
      SANS_CHECK_CLOSE(qeval_sub1[i], qeval_main1[i], tol, tol);
    }
  }


  //Check solutions on split neighbor cell
  const FieldCellGroupType& global_cellgrp_neighbor = qfld.getCellGroup<Tet>(0);
  global_cellgrp_neighbor.getElement(fldElem_global, 1);

  const FieldCellGroupType& local_cellgrp_neighbor = qfld_split_local.getCellGroup<Tet>(1);
  local_cellgrp_neighbor.getElement(fldElem_local0, 0);
  local_cellgrp_neighbor.getElement(fldElem_local1, 1);

  int neighbor_split_edge_index = 0;

  //Check if the solutions are equal at a few quadrature points
  for (int iquad = 0; iquad < nquad; iquad++)
  {
    quadrature.coordinates( iquad, Ref_sub );

    fldElem_local0.eval(Ref_sub,qeval_sub0);
    fldElem_local1.eval(Ref_sub,qeval_sub1);

    BasisFunction_RefElement_Split<TopoD3, Tet>::transform(Ref_sub, ElementSplitType::Edge, neighbor_split_edge_index, 0, Ref_main);
    fldElem_global.eval(Ref_main, qeval_main0);

    BasisFunction_RefElement_Split<TopoD3, Tet>::transform(Ref_sub, ElementSplitType::Edge, neighbor_split_edge_index, 1, Ref_main);
    fldElem_global.eval(Ref_main, qeval_main1);

    for (int i=0; i<2; i++)
    {
      SANS_CHECK_CLOSE(qeval_sub0[i], qeval_main0[i], tol, tol);
      SANS_CHECK_CLOSE(qeval_sub1[i], qeval_main1[i], tol, tol);
    }
  }

  BOOST_CHECK_EQUAL( qfld_split_local.nCellGroups(), 2 );
  BOOST_REQUIRE( qfld_split_local.getCellGroupBase(0).topoTypeID() == typeid(Tet) );
  BOOST_REQUIRE( qfld_split_local.getCellGroupBase(1).topoTypeID() == typeid(Tet) );

  const Field<PhysD3,TopoD3,ArrayQ>::FieldCellGroupType<Tet>& qfldCellGroup0 = qfld_split_local.getCellGroup<Tet>(0);
  const Field<PhysD3,TopoD3,ArrayQ>::FieldCellGroupType<Tet>& qfldCellGroup1 = qfld_split_local.getCellGroup<Tet>(1);

  BOOST_CHECK_EQUAL( qfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.nElem(), 2 );

  int DOFMap[4];

  int DOFcount = 0;

  for (int elem=0; elem < qfldCellGroup0.nElem(); elem++)
  {
    qfldCellGroup0.associativity(elem).getNodeGlobalMapping( DOFMap, 4 );
    for (int k=0; k<4; k++)
    {
      BOOST_CHECK_EQUAL( DOFMap[k], DOFcount );
      DOFcount++;
    }
  }

  for (int elem=0; elem < qfldCellGroup1.nElem(); elem++)
  {
    qfldCellGroup1.associativity(elem).getNodeGlobalMapping( DOFMap, 4 );
    for (int k=0; k<4; k++)
    {
      BOOST_CHECK_EQUAL( DOFMap[k], DOFcount );
      DOFcount++;
    }
  }

  BOOST_CHECK_EQUAL( qfld_split_local.nInteriorTraceGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_split_local.nBoundaryTraceGroups(), 0 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field3D_DG_Local_2Tet_X1_ExtractRightCell_SplitEdge2_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD3, TopoD3, ArrayQ > QField3D_DG_Volume;

  typedef DLA::VectorS<TopoD3::D,Real> RefCoordType;

  typedef QField3D_DG_Volume::FieldCellGroupType<Tet> FieldCellGroupType;
  typedef FieldCellGroupType::ElementType<> ElementFieldClass;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  Real tol = 1e-11;

  XField3D_2Tet_X1_1Group xfld;

  int order = 1;
  QField3D_DG_Volume qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  //Extract the local mesh for the right tet
  int main_group = 0;
  int main_elem = 0;
  XField_Local<PhysD3,TopoD3> xfld_local(comm_local,connectivity,main_group,main_elem);

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity_local(xfld_local);

  int split_edge_index = 2;
  XField_Local_Split<PhysD3,TopoD3> xfld_split_local(xfld_local, connectivity_local,
                                                     ElementSplitType::Edge, split_edge_index);

  Field_Local<QField3D_DG_Volume> qfld_split_local(xfld_split_local, qfld, order, BasisFunctionCategory_Hierarchical);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  qfld_split_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_split_local.getXField(), &xfld_split_local );

  BOOST_CHECK_EQUAL( qfld_split_local.nDOF(), 12 );

  //Get global element
  const FieldCellGroupType& global_cellgrp = qfld.getCellGroup<Tet>(main_group);
  ElementFieldClass fldElem_global( global_cellgrp.basis() );
  global_cellgrp.getElement(fldElem_global, main_elem);

  const FieldCellGroupType& local_cellgrp = qfld_split_local.getCellGroup<Tet>(0);
  ElementFieldClass fldElem_local0( global_cellgrp.basis() );
  ElementFieldClass fldElem_local1( global_cellgrp.basis() );
  local_cellgrp.getElement(fldElem_local0, 0);
  local_cellgrp.getElement(fldElem_local1, 1);

  //Get order 3 quad points
  Quadrature<TopoD3, Tet> quadrature(3);
  const int nquad = quadrature.nQuadrature();

  RefCoordType Ref_sub;  // reference-element coordinates on sub-element
  RefCoordType Ref_main; // reference-element coordinates on main-element
  ArrayQ qeval_main0, qeval_main1, qeval_sub0, qeval_sub1;

  //Check if the solutions are equal at a few quadrature points
  for (int iquad = 0; iquad < nquad; iquad++)
  {
    quadrature.coordinates( iquad, Ref_sub );

    fldElem_local0.eval(Ref_sub,qeval_sub0);
    fldElem_local1.eval(Ref_sub,qeval_sub1);

    BasisFunction_RefElement_Split<TopoD3, Tet>::transform(Ref_sub, ElementSplitType::Edge, split_edge_index, 0, Ref_main);
    fldElem_global.eval(Ref_main, qeval_main0);

    BasisFunction_RefElement_Split<TopoD3, Tet>::transform(Ref_sub, ElementSplitType::Edge, split_edge_index, 1, Ref_main);
    fldElem_global.eval(Ref_main, qeval_main1);

    for (int i=0; i<2; i++)
    {
      SANS_CHECK_CLOSE(qeval_sub0[i], qeval_main0[i], tol, tol);
      SANS_CHECK_CLOSE(qeval_sub1[i], qeval_main1[i], tol, tol);
    }
  }

  //Check solutions on unsplit neighbor cell
  const FieldCellGroupType& global_cellgrp_neighbor = qfld.getCellGroup<Tet>(0);
  global_cellgrp_neighbor.getElement(fldElem_global, 1);

  const FieldCellGroupType& local_cellgrp_neighbor = qfld_split_local.getCellGroup<Tet>(1);
  local_cellgrp_neighbor.getElement(fldElem_local0, 0);

  //Check if the solutions are equal at a few quadrature points
  for (int iquad = 0; iquad < nquad; iquad++)
  {
    quadrature.coordinates( iquad, Ref_sub );

    fldElem_local0.eval(Ref_sub,qeval_sub0);

    Ref_main = Ref_sub;
    fldElem_global.eval(Ref_main, qeval_main0);

    for (int i=0; i<2; i++)
      SANS_CHECK_CLOSE(qeval_sub0[i], qeval_main0[i], tol, tol);
  }

  //DOFs on unsplit neighbor cell should be exactly equal to those on original mesh
  for (int n = 0; n < 4; n++)
    for (int i=0; i<2; i++)
      BOOST_CHECK_EQUAL(qfld_split_local.DOF(n+8)[i], qfld.DOF(n+4)[i]);


  BOOST_CHECK_EQUAL( qfld_split_local.nCellGroups(), 2 );
  BOOST_REQUIRE( qfld_split_local.getCellGroupBase(0).topoTypeID() == typeid(Tet) );
  BOOST_REQUIRE( qfld_split_local.getCellGroupBase(1).topoTypeID() == typeid(Tet) );

  const Field<PhysD3,TopoD3,ArrayQ>::FieldCellGroupType<Tet>& qfldCellGroup0 = qfld_split_local.getCellGroup<Tet>(0);
  const Field<PhysD3,TopoD3,ArrayQ>::FieldCellGroupType<Tet>& qfldCellGroup1 = qfld_split_local.getCellGroup<Tet>(1);

  BOOST_CHECK_EQUAL( qfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.nElem(), 1 );

  int DOFMap[4];

  int DOFcount = 0;

  for (int elem=0; elem < qfldCellGroup0.nElem(); elem++)
  {
    qfldCellGroup0.associativity(elem).getNodeGlobalMapping( DOFMap, 4 );
    for (int k=0; k<4; k++)
    {
      BOOST_CHECK_EQUAL( DOFMap[k], DOFcount );
      DOFcount++;
    }
  }

  for (int elem=0; elem < qfldCellGroup1.nElem(); elem++)
  {
    qfldCellGroup1.associativity(elem).getNodeGlobalMapping( DOFMap, 4 );
    for (int k=0; k<4; k++)
    {
      BOOST_CHECK_EQUAL( DOFMap[k], DOFcount );
      DOFcount++;
    }
  }

  BOOST_CHECK_EQUAL( qfld_split_local.nInteriorTraceGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_split_local.nBoundaryTraceGroups(), 0 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field3D_DG_Local_2Tet_X1_ExtractRightCell_SplitEdge4_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD3, TopoD3, ArrayQ > QField3D_DG_Volume;

  typedef DLA::VectorS<TopoD3::D,Real> RefCoordType;

  typedef QField3D_DG_Volume::FieldCellGroupType<Tet> FieldCellGroupType;
  typedef FieldCellGroupType::ElementType<> ElementFieldClass;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  Real tol = 1e-11;

  XField3D_2Tet_X1_1Group xfld;

  int order = 1;
  QField3D_DG_Volume qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  //Extract the local mesh for the right tet
  int main_group = 0;
  int main_elem = 0;
  XField_Local<PhysD3,TopoD3> xfld_local(comm_local,connectivity,main_group,main_elem);

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity_local(xfld_local);

  int split_edge_index = 4;
  XField_Local_Split<PhysD3,TopoD3> xfld_split_local(xfld_local, connectivity_local,
                                                     ElementSplitType::Edge, split_edge_index);

  Field_Local<QField3D_DG_Volume> qfld_split_local(xfld_split_local, qfld, order, BasisFunctionCategory_Hierarchical);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  qfld_split_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_split_local.getXField(), &xfld_split_local );

  BOOST_CHECK_EQUAL( qfld_split_local.nDOF(), 16 );

  //Get global element
  const FieldCellGroupType& global_cellgrp = qfld.getCellGroup<Tet>(main_group);
  ElementFieldClass fldElem_global( global_cellgrp.basis() );
  global_cellgrp.getElement(fldElem_global, main_elem);

  const FieldCellGroupType& local_cellgrp = qfld_split_local.getCellGroup<Tet>(0);
  ElementFieldClass fldElem_local0( global_cellgrp.basis() );
  ElementFieldClass fldElem_local1( global_cellgrp.basis() );
  local_cellgrp.getElement(fldElem_local0, 0);
  local_cellgrp.getElement(fldElem_local1, 1);

  //Get order 3 quad points
  Quadrature<TopoD3, Tet> quadrature(3);
  const int nquad = quadrature.nQuadrature();

  RefCoordType Ref_sub;  // reference-element coordinates on sub-element
  RefCoordType Ref_main; // reference-element coordinates on main-element
  ArrayQ qeval_main0, qeval_main1, qeval_sub0, qeval_sub1;

  //Check if the solutions are equal at a few quadrature points
  for (int iquad = 0; iquad < nquad; iquad++)
  {
    quadrature.coordinates( iquad, Ref_sub );

    fldElem_local0.eval(Ref_sub,qeval_sub0);
    fldElem_local1.eval(Ref_sub,qeval_sub1);

    BasisFunction_RefElement_Split<TopoD3, Tet>::transform(Ref_sub, ElementSplitType::Edge, split_edge_index, 0, Ref_main);
    fldElem_global.eval(Ref_main, qeval_main0);

    BasisFunction_RefElement_Split<TopoD3, Tet>::transform(Ref_sub, ElementSplitType::Edge, split_edge_index, 1, Ref_main);
    fldElem_global.eval(Ref_main, qeval_main1);

    for (int i=0; i<2; i++)
    {
      SANS_CHECK_CLOSE(qeval_sub0[i], qeval_main0[i], tol, tol);
      SANS_CHECK_CLOSE(qeval_sub1[i], qeval_main1[i], tol, tol);
    }
  }


  //Check solutions on split neighbor cell
  const FieldCellGroupType& global_cellgrp_neighbor = qfld.getCellGroup<Tet>(0);
  global_cellgrp_neighbor.getElement(fldElem_global, 1);

  const FieldCellGroupType& local_cellgrp_neighbor = qfld_split_local.getCellGroup<Tet>(1);
  local_cellgrp_neighbor.getElement(fldElem_local0, 0);
  local_cellgrp_neighbor.getElement(fldElem_local1, 1);

  int neighbor_split_edge_index = 1;

  //Check if the solutions are equal at a few quadrature points
  for (int iquad = 0; iquad < nquad; iquad++)
  {
    quadrature.coordinates( iquad, Ref_sub );

    fldElem_local0.eval(Ref_sub,qeval_sub0);
    fldElem_local1.eval(Ref_sub,qeval_sub1);

    BasisFunction_RefElement_Split<TopoD3, Tet>::transform(Ref_sub, ElementSplitType::Edge, neighbor_split_edge_index, 1, Ref_main);
    fldElem_global.eval(Ref_main, qeval_main0);

    BasisFunction_RefElement_Split<TopoD3, Tet>::transform(Ref_sub, ElementSplitType::Edge, neighbor_split_edge_index, 0, Ref_main);
    fldElem_global.eval(Ref_main, qeval_main1);

    for (int i=0; i<2; i++)
    {
      SANS_CHECK_CLOSE(qeval_sub0[i], qeval_main0[i], tol, tol);
      SANS_CHECK_CLOSE(qeval_sub1[i], qeval_main1[i], tol, tol);
    }
  }

  BOOST_CHECK_EQUAL( qfld_split_local.nCellGroups(), 2 );
  BOOST_REQUIRE( qfld_split_local.getCellGroupBase(0).topoTypeID() == typeid(Tet) );
  BOOST_REQUIRE( qfld_split_local.getCellGroupBase(1).topoTypeID() == typeid(Tet) );

  const Field<PhysD3,TopoD3,ArrayQ>::FieldCellGroupType<Tet>& qfldCellGroup0 = qfld_split_local.getCellGroup<Tet>(0);
  const Field<PhysD3,TopoD3,ArrayQ>::FieldCellGroupType<Tet>& qfldCellGroup1 = qfld_split_local.getCellGroup<Tet>(1);

  BOOST_CHECK_EQUAL( qfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.nElem(), 2 );

  int DOFMap[4];

  int DOFcount = 0;

  for (int elem=0; elem < qfldCellGroup0.nElem(); elem++)
  {
    qfldCellGroup0.associativity(elem).getNodeGlobalMapping( DOFMap, 4 );
    for (int k=0; k<4; k++)
    {
      BOOST_CHECK_EQUAL( DOFMap[k], DOFcount );
      DOFcount++;
    }
  }

  for (int elem=0; elem < qfldCellGroup1.nElem(); elem++)
  {
    qfldCellGroup1.associativity(elem).getNodeGlobalMapping( DOFMap, 4 );
    for (int k=0; k<4; k++)
    {
      BOOST_CHECK_EQUAL( DOFMap[k], DOFcount );
      DOFcount++;
    }
  }

  BOOST_CHECK_EQUAL( qfld_split_local.nInteriorTraceGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_split_local.nBoundaryTraceGroups(), 0 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field3D_DG_Local_2Tet_X1_ExtractLeftCell_SplitEdge1_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD3, TopoD3, ArrayQ > QField3D_DG_Volume;

  typedef DLA::VectorS<TopoD3::D,Real> RefCoordType;

  typedef QField3D_DG_Volume::FieldCellGroupType<Tet> FieldCellGroupType;
  typedef FieldCellGroupType::ElementType<> ElementFieldClass;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  Real tol = 1e-11;

  XField3D_2Tet_X1_1Group xfld;

  int order = 1;
  QField3D_DG_Volume qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  //Extract the local mesh for the left tet
  int main_group = 0;
  int main_elem = 1;
  XField_Local<PhysD3,TopoD3> xfld_local(comm_local,connectivity,main_group,main_elem);

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity_local(xfld_local);

  int split_edge_index = 1;
  XField_Local_Split<PhysD3,TopoD3> xfld_split_local(xfld_local, connectivity_local,
                                                     ElementSplitType::Edge, split_edge_index);

  Field_Local<QField3D_DG_Volume> qfld_split_local(xfld_split_local, qfld, order, BasisFunctionCategory_Hierarchical);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  qfld_split_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_split_local.getXField(), &xfld_split_local );

  BOOST_CHECK_EQUAL( qfld_split_local.nDOF(), 16 );

  //Get global element
  const FieldCellGroupType& global_cellgrp = qfld.getCellGroup<Tet>(main_group);
  ElementFieldClass fldElem_global( global_cellgrp.basis() );
  global_cellgrp.getElement(fldElem_global, main_elem);

  const FieldCellGroupType& local_cellgrp = qfld_split_local.getCellGroup<Tet>(0);
  ElementFieldClass fldElem_local0( global_cellgrp.basis() );
  ElementFieldClass fldElem_local1( global_cellgrp.basis() );
  local_cellgrp.getElement(fldElem_local0, 0);
  local_cellgrp.getElement(fldElem_local1, 1);

  //Get order 3 quad points
  Quadrature<TopoD3, Tet> quadrature(3);
  const int nquad = quadrature.nQuadrature();

  RefCoordType Ref_sub;  // reference-element coordinates on sub-element
  RefCoordType Ref_main; // reference-element coordinates on main-element
  ArrayQ qeval_main0, qeval_main1, qeval_sub0, qeval_sub1;

  //Check if the solutions are equal at a few quadrature points
  for (int iquad = 0; iquad < nquad; iquad++)
  {
    quadrature.coordinates( iquad, Ref_sub );

    fldElem_local0.eval(Ref_sub,qeval_sub0);
    fldElem_local1.eval(Ref_sub,qeval_sub1);

    BasisFunction_RefElement_Split<TopoD3, Tet>::transform(Ref_sub, ElementSplitType::Edge, split_edge_index, 0, Ref_main);
    fldElem_global.eval(Ref_main, qeval_main0);

    BasisFunction_RefElement_Split<TopoD3, Tet>::transform(Ref_sub, ElementSplitType::Edge, split_edge_index, 1, Ref_main);
    fldElem_global.eval(Ref_main, qeval_main1);

    for (int i=0; i<2; i++)
    {
      SANS_CHECK_CLOSE(qeval_sub0[i], qeval_main0[i], tol, tol);
      SANS_CHECK_CLOSE(qeval_sub1[i], qeval_main1[i], tol, tol);
    }
  }


  //Check solutions on split neighbor cell
  const FieldCellGroupType& global_cellgrp_neighbor = qfld.getCellGroup<Tet>(0);
  global_cellgrp_neighbor.getElement(fldElem_global, 0);

  const FieldCellGroupType& local_cellgrp_neighbor = qfld_split_local.getCellGroup<Tet>(1);
  local_cellgrp_neighbor.getElement(fldElem_local0, 0);
  local_cellgrp_neighbor.getElement(fldElem_local1, 1);

  int neighbor_split_edge_index = 4;

  //Check if the solutions are equal at a few quadrature points
  for (int iquad = 0; iquad < nquad; iquad++)
  {
    quadrature.coordinates( iquad, Ref_sub );

    fldElem_local0.eval(Ref_sub,qeval_sub0);
    fldElem_local1.eval(Ref_sub,qeval_sub1);

    BasisFunction_RefElement_Split<TopoD3, Tet>::transform(Ref_sub, ElementSplitType::Edge, neighbor_split_edge_index, 1, Ref_main);
    fldElem_global.eval(Ref_main, qeval_main0);

    BasisFunction_RefElement_Split<TopoD3, Tet>::transform(Ref_sub, ElementSplitType::Edge, neighbor_split_edge_index, 0, Ref_main);
    fldElem_global.eval(Ref_main, qeval_main1);

    for (int i=0; i<2; i++)
    {
      SANS_CHECK_CLOSE(qeval_sub0[i], qeval_main0[i], tol, tol);
      SANS_CHECK_CLOSE(qeval_sub1[i], qeval_main1[i], tol, tol);
    }
  }

  BOOST_CHECK_EQUAL( qfld_split_local.nCellGroups(), 2 );
  BOOST_REQUIRE( qfld_split_local.getCellGroupBase(0).topoTypeID() == typeid(Tet) );
  BOOST_REQUIRE( qfld_split_local.getCellGroupBase(1).topoTypeID() == typeid(Tet) );

  const Field<PhysD3,TopoD3,ArrayQ>::FieldCellGroupType<Tet>& qfldCellGroup0 = qfld_split_local.getCellGroup<Tet>(0);
  const Field<PhysD3,TopoD3,ArrayQ>::FieldCellGroupType<Tet>& qfldCellGroup1 = qfld_split_local.getCellGroup<Tet>(1);

  BOOST_CHECK_EQUAL( qfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.nElem(), 2 );

  int DOFMap[4];

  int DOFcount = 0;

  for (int elem=0; elem < qfldCellGroup0.nElem(); elem++)
  {
    qfldCellGroup0.associativity(elem).getNodeGlobalMapping( DOFMap, 4 );
    for (int k=0; k<4; k++)
    {
      BOOST_CHECK_EQUAL( DOFMap[k], DOFcount );
      DOFcount++;
    }
  }

  for (int elem=0; elem < qfldCellGroup1.nElem(); elem++)
  {
    qfldCellGroup1.associativity(elem).getNodeGlobalMapping( DOFMap, 4 );
    for (int k=0; k<4; k++)
    {
      BOOST_CHECK_EQUAL( DOFMap[k], DOFcount );
      DOFcount++;
    }
  }

  BOOST_CHECK_EQUAL( qfld_split_local.nInteriorTraceGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_split_local.nBoundaryTraceGroups(), 0 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field3D_DG_Local_2Tet_X1_ExtractLeftCell_SplitEdge2_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD3, TopoD3, ArrayQ > QField3D_DG_Volume;

  typedef DLA::VectorS<TopoD3::D,Real> RefCoordType;

  typedef QField3D_DG_Volume::FieldCellGroupType<Tet> FieldCellGroupType;
  typedef FieldCellGroupType::ElementType<> ElementFieldClass;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  Real tol = 1e-11;

  XField3D_2Tet_X1_1Group xfld;

  int order = 1;
  QField3D_DG_Volume qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  //Extract the local mesh for the left tet
  int main_group = 0;
  int main_elem = 1;
  XField_Local<PhysD3,TopoD3> xfld_local(comm_local,connectivity,main_group,main_elem);

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity_local(xfld_local);

  int split_edge_index = 2;
  XField_Local_Split<PhysD3,TopoD3> xfld_split_local(xfld_local, connectivity_local,
                                                     ElementSplitType::Edge, split_edge_index);

  Field_Local<QField3D_DG_Volume> qfld_split_local(xfld_split_local, qfld, order, BasisFunctionCategory_Hierarchical);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  qfld_split_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_split_local.getXField(), &xfld_split_local );

  BOOST_CHECK_EQUAL( qfld_split_local.nDOF(), 16 );

  //Get global element
  const FieldCellGroupType& global_cellgrp = qfld.getCellGroup<Tet>(main_group);
  ElementFieldClass fldElem_global( global_cellgrp.basis() );
  global_cellgrp.getElement(fldElem_global, main_elem);

  const FieldCellGroupType& local_cellgrp = qfld_split_local.getCellGroup<Tet>(0);
  ElementFieldClass fldElem_local0( global_cellgrp.basis() );
  ElementFieldClass fldElem_local1( global_cellgrp.basis() );
  local_cellgrp.getElement(fldElem_local0, 0);
  local_cellgrp.getElement(fldElem_local1, 1);

  //Get order 3 quad points
  Quadrature<TopoD3, Tet> quadrature(3);
  const int nquad = quadrature.nQuadrature();

  RefCoordType Ref_sub;  // reference-element coordinates on sub-element
  RefCoordType Ref_main; // reference-element coordinates on main-element
  ArrayQ qeval_main0, qeval_main1, qeval_sub0, qeval_sub1;

  //Check if the solutions are equal at a few quadrature points
  for (int iquad = 0; iquad < nquad; iquad++)
  {
    quadrature.coordinates( iquad, Ref_sub );

    fldElem_local0.eval(Ref_sub,qeval_sub0);
    fldElem_local1.eval(Ref_sub,qeval_sub1);

    BasisFunction_RefElement_Split<TopoD3, Tet>::transform(Ref_sub, ElementSplitType::Edge, split_edge_index, 0, Ref_main);
    fldElem_global.eval(Ref_main, qeval_main0);

    BasisFunction_RefElement_Split<TopoD3, Tet>::transform(Ref_sub, ElementSplitType::Edge, split_edge_index, 1, Ref_main);
    fldElem_global.eval(Ref_main, qeval_main1);

    for (int i=0; i<2; i++)
    {
      SANS_CHECK_CLOSE(qeval_sub0[i], qeval_main0[i], tol, tol);
      SANS_CHECK_CLOSE(qeval_sub1[i], qeval_main1[i], tol, tol);
    }
  }


  //Check solutions on split neighbor cell
  const FieldCellGroupType& global_cellgrp_neighbor = qfld.getCellGroup<Tet>(0);
  global_cellgrp_neighbor.getElement(fldElem_global, 0);

  const FieldCellGroupType& local_cellgrp_neighbor = qfld_split_local.getCellGroup<Tet>(1);
  local_cellgrp_neighbor.getElement(fldElem_local0, 0);
  local_cellgrp_neighbor.getElement(fldElem_local1, 1);

  int neighbor_split_edge_index = 3;

  //Check if the solutions are equal at a few quadrature points
  for (int iquad = 0; iquad < nquad; iquad++)
  {
    quadrature.coordinates( iquad, Ref_sub );

    fldElem_local0.eval(Ref_sub,qeval_sub0);
    fldElem_local1.eval(Ref_sub,qeval_sub1);

    BasisFunction_RefElement_Split<TopoD3, Tet>::transform(Ref_sub, ElementSplitType::Edge, neighbor_split_edge_index, 1, Ref_main);
    fldElem_global.eval(Ref_main, qeval_main0);

    BasisFunction_RefElement_Split<TopoD3, Tet>::transform(Ref_sub, ElementSplitType::Edge, neighbor_split_edge_index, 0, Ref_main);
    fldElem_global.eval(Ref_main, qeval_main1);

    for (int i=0; i<2; i++)
    {
      SANS_CHECK_CLOSE(qeval_sub0[i], qeval_main0[i], tol, tol);
      SANS_CHECK_CLOSE(qeval_sub1[i], qeval_main1[i], tol, tol);
    }
  }

  BOOST_CHECK_EQUAL( qfld_split_local.nCellGroups(), 2 );
  BOOST_REQUIRE( qfld_split_local.getCellGroupBase(0).topoTypeID() == typeid(Tet) );
  BOOST_REQUIRE( qfld_split_local.getCellGroupBase(1).topoTypeID() == typeid(Tet) );

  const Field<PhysD3,TopoD3,ArrayQ>::FieldCellGroupType<Tet>& qfldCellGroup0 = qfld_split_local.getCellGroup<Tet>(0);
  const Field<PhysD3,TopoD3,ArrayQ>::FieldCellGroupType<Tet>& qfldCellGroup1 = qfld_split_local.getCellGroup<Tet>(1);

  BOOST_CHECK_EQUAL( qfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.nElem(), 2 );

  int DOFMap[4];

  int DOFcount = 0;

  for (int elem=0; elem < qfldCellGroup0.nElem(); elem++)
  {
    qfldCellGroup0.associativity(elem).getNodeGlobalMapping( DOFMap, 4 );
    for (int k=0; k<4; k++)
    {
      BOOST_CHECK_EQUAL( DOFMap[k], DOFcount );
      DOFcount++;
    }
  }

  for (int elem=0; elem < qfldCellGroup1.nElem(); elem++)
  {
    qfldCellGroup1.associativity(elem).getNodeGlobalMapping( DOFMap, 4 );
    for (int k=0; k<4; k++)
    {
      BOOST_CHECK_EQUAL( DOFMap[k], DOFcount );
      DOFcount++;
    }
  }

  BOOST_CHECK_EQUAL( qfld_split_local.nInteriorTraceGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_split_local.nBoundaryTraceGroups(), 0 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field3D_DG_Local_2Tet_X1_ExtractLeftCell_SplitEdge5_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD3, TopoD3, ArrayQ > QField3D_DG_Volume;

  typedef DLA::VectorS<TopoD3::D,Real> RefCoordType;

  typedef QField3D_DG_Volume::FieldCellGroupType<Tet> FieldCellGroupType;
  typedef FieldCellGroupType::ElementType<> ElementFieldClass;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  Real tol = 1e-11;

  XField3D_2Tet_X1_1Group xfld;

  int order = 1;
  QField3D_DG_Volume qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  //Extract the local mesh for the right tet
  int main_group = 0;
  int main_elem = 1;
  XField_Local<PhysD3,TopoD3> xfld_local(comm_local,connectivity,main_group,main_elem);

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity_local(xfld_local);

  int split_edge_index = 5;
  XField_Local_Split<PhysD3,TopoD3> xfld_split_local(xfld_local, connectivity_local,
                                                     ElementSplitType::Edge, split_edge_index);

  Field_Local<QField3D_DG_Volume> qfld_split_local(xfld_split_local, qfld, order, BasisFunctionCategory_Hierarchical);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  qfld_split_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_split_local.getXField(), &xfld_split_local );

  BOOST_CHECK_EQUAL( qfld_split_local.nDOF(), 12 );

  //Get global element
  const FieldCellGroupType& global_cellgrp = qfld.getCellGroup<Tet>(main_group);
  ElementFieldClass fldElem_global( global_cellgrp.basis() );
  global_cellgrp.getElement(fldElem_global, main_elem);

  const FieldCellGroupType& local_cellgrp = qfld_split_local.getCellGroup<Tet>(0);
  ElementFieldClass fldElem_local0( global_cellgrp.basis() );
  ElementFieldClass fldElem_local1( global_cellgrp.basis() );
  local_cellgrp.getElement(fldElem_local0, 0);
  local_cellgrp.getElement(fldElem_local1, 1);

  //Get order 3 quad points
  Quadrature<TopoD3, Tet> quadrature(3);
  const int nquad = quadrature.nQuadrature();

  RefCoordType Ref_sub;  // reference-element coordinates on sub-element
  RefCoordType Ref_main; // reference-element coordinates on main-element
  ArrayQ qeval_main0, qeval_main1, qeval_sub0, qeval_sub1;

  //Check if the solutions are equal at a few quadrature points
  for (int iquad = 0; iquad < nquad; iquad++)
  {
    quadrature.coordinates( iquad, Ref_sub );

    fldElem_local0.eval(Ref_sub,qeval_sub0);
    fldElem_local1.eval(Ref_sub,qeval_sub1);

    BasisFunction_RefElement_Split<TopoD3, Tet>::transform(Ref_sub, ElementSplitType::Edge, split_edge_index, 0, Ref_main);
    fldElem_global.eval(Ref_main, qeval_main0);

    BasisFunction_RefElement_Split<TopoD3, Tet>::transform(Ref_sub, ElementSplitType::Edge, split_edge_index, 1, Ref_main);
    fldElem_global.eval(Ref_main, qeval_main1);

    for (int i=0; i<2; i++)
    {
      SANS_CHECK_CLOSE(qeval_sub0[i], qeval_main0[i], tol, tol);
      SANS_CHECK_CLOSE(qeval_sub1[i], qeval_main1[i], tol, tol);
    }
  }

  //Check solutions on unsplit neighbor cell
  const FieldCellGroupType& global_cellgrp_neighbor = qfld.getCellGroup<Tet>(0);
  global_cellgrp_neighbor.getElement(fldElem_global, 0);

  const FieldCellGroupType& local_cellgrp_neighbor = qfld_split_local.getCellGroup<Tet>(1);
  local_cellgrp_neighbor.getElement(fldElem_local0, 0);

  //Check if the solutions are equal at a few quadrature points
  for (int iquad = 0; iquad < nquad; iquad++)
  {
    quadrature.coordinates( iquad, Ref_sub );

    fldElem_local0.eval(Ref_sub,qeval_sub0);

    Ref_main = Ref_sub;
    fldElem_global.eval(Ref_main, qeval_main0);

    for (int i=0; i<2; i++)
      SANS_CHECK_CLOSE(qeval_sub0[i], qeval_main0[i], tol, tol);
  }

  //DOFs on unsplit neighbor cell should be exactly equal to those on original mesh
  for (int n = 0; n < 4; n++)
    for (int i=0; i<2; i++)
      BOOST_CHECK_EQUAL(qfld_split_local.DOF(n+8)[i], qfld.DOF(n)[i]);


  BOOST_CHECK_EQUAL( qfld_split_local.nCellGroups(), 2 );
  BOOST_REQUIRE( qfld_split_local.getCellGroupBase(0).topoTypeID() == typeid(Tet) );
  BOOST_REQUIRE( qfld_split_local.getCellGroupBase(1).topoTypeID() == typeid(Tet) );

  const Field<PhysD3,TopoD3,ArrayQ>::FieldCellGroupType<Tet>& qfldCellGroup0 = qfld_split_local.getCellGroup<Tet>(0);
  const Field<PhysD3,TopoD3,ArrayQ>::FieldCellGroupType<Tet>& qfldCellGroup1 = qfld_split_local.getCellGroup<Tet>(1);

  BOOST_CHECK_EQUAL( qfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.nElem(), 1 );

  int DOFMap[4];

  int DOFcount = 0;

  for (int elem=0; elem < qfldCellGroup0.nElem(); elem++)
  {
    qfldCellGroup0.associativity(elem).getNodeGlobalMapping( DOFMap, 4 );
    for (int k=0; k<4; k++)
    {
      BOOST_CHECK_EQUAL( DOFMap[k], DOFcount );
      DOFcount++;
    }
  }

  for (int elem=0; elem < qfldCellGroup1.nElem(); elem++)
  {
    qfldCellGroup1.associativity(elem).getNodeGlobalMapping( DOFMap, 4 );
    for (int k=0; k<4; k++)
    {
      BOOST_CHECK_EQUAL( DOFMap[k], DOFcount );
      DOFcount++;
    }
  }

  BOOST_CHECK_EQUAL( qfld_split_local.nInteriorTraceGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_split_local.nBoundaryTraceGroups(), 0 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field3D_DG_Local_2Tet_X1_ExtractLeftCell_SplitIsotropic_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD3, TopoD3, ArrayQ > QField3D_DG_Volume;

  typedef DLA::VectorS<TopoD3::D,Real> RefCoordType;

  typedef QField3D_DG_Volume::FieldCellGroupType<Tet> FieldCellGroupType;
  typedef FieldCellGroupType::ElementType<> ElementFieldClass;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  Real tol = 1e-11;

  XField3D_2Tet_X1_1Group xfld;

  int order = 1;
  QField3D_DG_Volume qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  //Extract the local mesh for the left tet
  int main_group = 0;
  int main_elem = 1;
  XField_Local<PhysD3,TopoD3> xfld_local(comm_local,connectivity,main_group,main_elem);

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity_local(xfld_local);

  int split_edge_index = -1;
  XField_Local_Split<PhysD3,TopoD3> xfld_split_local(xfld_local, connectivity_local,
                                                     ElementSplitType::Isotropic, split_edge_index);

  Field_Local<QField3D_DG_Volume> qfld_split_local(xfld_split_local, qfld, order, BasisFunctionCategory_Hierarchical);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  qfld_split_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_split_local.getXField(), &xfld_split_local );

  BOOST_CHECK_EQUAL( qfld_split_local.nDOF(), 12*4 );

  //Get global element
  const FieldCellGroupType& global_cellgrp = qfld.getCellGroup<Tet>(main_group);
  ElementFieldClass fldElem_global( global_cellgrp.basis() );
  global_cellgrp.getElement(fldElem_global, main_elem);

  const FieldCellGroupType& local_cellgrp = qfld_split_local.getCellGroup<Tet>(0);
  ElementFieldClass fldElem_local( global_cellgrp.basis() );

  //Get order 3 quad points
  Quadrature<TopoD3, Tet> quadrature(3);
  const int nquad = quadrature.nQuadrature();

  RefCoordType Ref_sub;  // reference-element coordinates on sub-element
  RefCoordType Ref_main; // reference-element coordinates on main-element
  ArrayQ qeval_main, qeval_sub;

  for (int subelem = 0; subelem < 8; subelem++)
  {
    local_cellgrp.getElement(fldElem_local, subelem);

    //Check if the solutions are equal at a few quadrature points
    for (int iquad = 0; iquad < nquad; iquad++)
    {
      quadrature.coordinates( iquad, Ref_sub );

      fldElem_local.eval(Ref_sub,qeval_sub);

      BasisFunction_RefElement_Split<TopoD3, Tet>::transform(Ref_sub, ElementSplitType::Isotropic,
                                                             split_edge_index, subelem, Ref_main);
      fldElem_global.eval(Ref_main, qeval_main);

      for (int i=0; i<2; i++)
        SANS_CHECK_CLOSE(qeval_sub[i], qeval_main[i], tol, tol);
    }
  }


  //Check solutions on split neighbor cells
  const FieldCellGroupType& global_cellgrp_neighbor = qfld.getCellGroup<Tet>(0);
  global_cellgrp_neighbor.getElement(fldElem_global, 0);

  const FieldCellGroupType& local_cellgrp_neighbor = qfld_split_local.getCellGroup<Tet>(1);

  int neighbor_split_edge_index = 1;
  int subcell_order[4] = {1,0,2,3};

  for (int subelem = 0; subelem < 4; subelem++)
  {
    local_cellgrp_neighbor.getElement(fldElem_local, subelem);

    //Check if the solutions are equal at a few quadrature points
    for (int iquad = 0; iquad < nquad; iquad++)
    {
      quadrature.coordinates( iquad, Ref_sub );

      fldElem_local.eval(Ref_sub,qeval_sub);

      BasisFunction_RefElement_Split<TopoD3, Tet>::transform(Ref_sub, ElementSplitType::IsotropicFace,
                                                             neighbor_split_edge_index, subcell_order[subelem], Ref_main);
      fldElem_global.eval(Ref_main, qeval_main);

      for (int i=0; i<2; i++)
        SANS_CHECK_CLOSE(qeval_sub[i], qeval_main[i], tol, tol);
    }
  }

  BOOST_CHECK_EQUAL( qfld_split_local.nCellGroups(), 2 );
  BOOST_REQUIRE( qfld_split_local.getCellGroupBase(0).topoTypeID() == typeid(Tet) );
  BOOST_REQUIRE( qfld_split_local.getCellGroupBase(1).topoTypeID() == typeid(Tet) );

  const Field<PhysD3,TopoD3,ArrayQ>::FieldCellGroupType<Tet>& qfldCellGroup0 = qfld_split_local.getCellGroup<Tet>(0);
  const Field<PhysD3,TopoD3,ArrayQ>::FieldCellGroupType<Tet>& qfldCellGroup1 = qfld_split_local.getCellGroup<Tet>(1);

  BOOST_CHECK_EQUAL( qfldCellGroup0.nElem(), 8 );
  BOOST_CHECK_EQUAL( qfldCellGroup1.nElem(), 4 );

  int DOFMap[4];

  int DOFcount = 0;

  for (int elem=0; elem < qfldCellGroup0.nElem(); elem++)
  {
    qfldCellGroup0.associativity(elem).getNodeGlobalMapping( DOFMap, 4 );
    for (int k=0; k<4; k++)
    {
      BOOST_CHECK_EQUAL( DOFMap[k], DOFcount );
      DOFcount++;
    }
  }

  for (int elem=0; elem < qfldCellGroup1.nElem(); elem++)
  {
    qfldCellGroup1.associativity(elem).getNodeGlobalMapping( DOFMap, 4 );
    for (int k=0; k<4; k++)
    {
      BOOST_CHECK_EQUAL( DOFMap[k], DOFcount );
      DOFcount++;
    }
  }

  BOOST_CHECK_EQUAL( qfld_split_local.nInteriorTraceGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_split_local.nBoundaryTraceGroups(), 0 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field3D_DG_Local_2Tet_X1_ExtractLeftCell_SplitEdge2_InteriorTrace_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_InteriorTrace<PhysD3, TopoD3, ArrayQ> QField3D_DG_ITrace;

  typedef DLA::VectorS<TopoD2::D,Real> RefCoordType;

  typedef QField3D_DG_ITrace::FieldTraceGroupType<Triangle> FieldTraceGroupType;
  typedef FieldTraceGroupType::ElementType<> ElementFieldClass;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  Real tol = 1e-11;

  XField3D_2Tet_X1_1Group xfld;

  int order = 1;
  QField3D_DG_ITrace qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  //Extract the local mesh for the left tet
  int main_group = 0;
  int main_elem = 1;
  XField_Local<PhysD3,TopoD3> xfld_local(comm_local,connectivity,main_group,main_elem);

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity_local(xfld_local);

  int split_edge_index = 2;
  XField_Local_Split<PhysD3,TopoD3> xfld_split_local(xfld_local, connectivity_local,
                                                     ElementSplitType::Edge, split_edge_index);

  Field_Local<QField3D_DG_ITrace> qfld_split_local(xfld_split_local, qfld, order, BasisFunctionCategory_Hierarchical);

#if 0
  xfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  xfld_split_local.dump(3,std::cout);

  qfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  qfld_split_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_split_local.getXField(), &xfld_split_local );

  BOOST_CHECK_EQUAL( qfld_split_local.nDOF(), 4*3 );

  //Get order 3 quad points
  Quadrature<TopoD2, Triangle> quadrature(3);
  const int nquad = quadrature.nQuadrature();

  RefCoordType Ref_sub;  // reference-element coordinates on sub-element
  RefCoordType Ref_main; // reference-element coordinates on main-element
  ArrayQ qeval_main0, qeval_main1, qeval_sub0, qeval_sub1;

  //Get global and local elements
  const FieldTraceGroupType& global_tracegrp0 = qfld.getInteriorTraceGroup<Triangle>(0);
  const FieldTraceGroupType& local_tracegrp0 = qfld_split_local.getInteriorTraceGroup<Triangle>(0);

  ElementFieldClass fldElem_global( global_tracegrp0.basis() );
  ElementFieldClass fldElem_local0( global_tracegrp0.basis() );
  ElementFieldClass fldElem_local1( global_tracegrp0.basis() );

  //------------Global BTraceGroup 0, Elem 0-----------------
  global_tracegrp0.getElement(fldElem_global, 0);

  //Rotate global fldElem to orientation (-1) because interior trace was flipped from global mesh to local mesh
  ElementFieldClass fldElem_global_reoriented( global_tracegrp0.basis() );
  fldElem_global_reoriented.DOF(0) = fldElem_global.DOF(0);
  fldElem_global_reoriented.DOF(1) = fldElem_global.DOF(2);
  fldElem_global_reoriented.DOF(2) = fldElem_global.DOF(1);

  //Split traces
  //Check if the solutions are equal at a few quadrature points
  for (int iquad = 0; iquad < nquad; iquad++)
  {
    quadrature.coordinates( iquad, Ref_sub );


    local_tracegrp0.getElement(fldElem_local0, 0);
    local_tracegrp0.getElement(fldElem_local1, 1);

    fldElem_local0.eval(Ref_sub,qeval_sub0);
    fldElem_local1.eval(Ref_sub,qeval_sub1);

    int Itrace_split_edge_index = 2;

    BasisFunction_RefElement_Split<TopoD2, Triangle>::transform(Ref_sub, ElementSplitType::Edge, Itrace_split_edge_index, 0, Ref_main);
    fldElem_global_reoriented.eval(Ref_main, qeval_main0);

    BasisFunction_RefElement_Split<TopoD2, Triangle>::transform(Ref_sub, ElementSplitType::Edge, Itrace_split_edge_index, 1, Ref_main);
    fldElem_global_reoriented.eval(Ref_main, qeval_main1);

    for (int i=0; i<2; i++)
    {
      SANS_CHECK_CLOSE(qeval_sub0[i], qeval_main0[i], tol, tol);
      SANS_CHECK_CLOSE(qeval_sub1[i], qeval_main1[i], tol, tol);
    }
  }

  //DOFs for newly added interior traces should be zero
  for (int n = 0; n < 2*3; n++)
    for (int i=0; i<2; i++)
      BOOST_CHECK_EQUAL(qfld_split_local.DOF(n+6)[i], 0.0);


  BOOST_CHECK_EQUAL( qfld_split_local.nCellGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_split_local.nInteriorTraceGroups(), 3 );
  BOOST_CHECK_EQUAL( qfld_split_local.nBoundaryTraceGroups(), 0 );

  BOOST_REQUIRE( qfld_split_local.getInteriorTraceGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( qfld_split_local.getInteriorTraceGroupBase(1).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( qfld_split_local.getInteriorTraceGroupBase(2).topoTypeID() == typeid(Triangle) );

  const Field<PhysD3,TopoD3,ArrayQ>::FieldTraceGroupType<Triangle>& qfldITraceGroup0 = qfld_split_local.getInteriorTraceGroup<Triangle>(0);
  const Field<PhysD3,TopoD3,ArrayQ>::FieldTraceGroupType<Triangle>& qfldITraceGroup1 = qfld_split_local.getInteriorTraceGroup<Triangle>(1);
  const Field<PhysD3,TopoD3,ArrayQ>::FieldTraceGroupType<Triangle>& qfldITraceGroup2 = qfld_split_local.getInteriorTraceGroup<Triangle>(2);

  BOOST_CHECK_EQUAL( qfldITraceGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( qfldITraceGroup1.nElem(), 1 );
  BOOST_CHECK_EQUAL( qfldITraceGroup2.nElem(), 1 );

  int DOFMap[3];

  int DOFcount = 0;

  for (int elem=0; elem < qfldITraceGroup0.nElem(); elem++)
  {
    qfldITraceGroup0.associativity(elem).getNodeGlobalMapping( DOFMap, 3 );
    for (int k=0; k<3; k++)
    {
      BOOST_CHECK_EQUAL( DOFMap[k], DOFcount );
      DOFcount++;
    }
  }

  for (int elem=0; elem < qfldITraceGroup1.nElem(); elem++)
  {
    qfldITraceGroup1.associativity(elem).getNodeGlobalMapping( DOFMap, 3 );
    for (int k=0; k<3; k++)
    {
      BOOST_CHECK_EQUAL( DOFMap[k], DOFcount );
      DOFcount++;
    }
  }

  for (int elem=0; elem < qfldITraceGroup2.nElem(); elem++)
  {
    qfldITraceGroup2.associativity(elem).getNodeGlobalMapping( DOFMap, 3 );
    for (int k=0; k<3; k++)
    {
      BOOST_CHECK_EQUAL( DOFMap[k], DOFcount );
      DOFcount++;
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field3D_DG_Local_2Tet_X1_ExtractRightCell_SplitEdge0_InteriorTrace_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_InteriorTrace<PhysD3, TopoD3, ArrayQ> QField3D_DG_ITrace;

  typedef DLA::VectorS<TopoD2::D,Real> RefCoordType;

  typedef QField3D_DG_ITrace::FieldTraceGroupType<Triangle> FieldTraceGroupType;
  typedef FieldTraceGroupType::ElementType<> ElementFieldClass;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  Real tol = 1e-11;

  XField3D_2Tet_X1_1Group xfld;

  int order = 1;
  QField3D_DG_ITrace qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  //Extract the local mesh for the right tet
  int main_group = 0;
  int main_elem = 0;
  XField_Local<PhysD3,TopoD3> xfld_local(comm_local,connectivity,main_group,main_elem);

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity_local(xfld_local);

  int split_edge_index = 0;
  XField_Local_Split<PhysD3,TopoD3> xfld_split_local(xfld_local, connectivity_local,
                                                     ElementSplitType::Edge, split_edge_index);

  Field_Local<QField3D_DG_ITrace> qfld_split_local(xfld_split_local, qfld, order, BasisFunctionCategory_Hierarchical);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  qfld_split_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_split_local.getXField(), &xfld_split_local );

  BOOST_CHECK_EQUAL( qfld_split_local.nDOF(), 4*3 );

  //Get order 3 quad points
  Quadrature<TopoD2, Triangle> quadrature(3);
  const int nquad = quadrature.nQuadrature();

  RefCoordType Ref_sub;  // reference-element coordinates on sub-element
  RefCoordType Ref_main; // reference-element coordinates on main-element
  ArrayQ qeval_main0, qeval_main1, qeval_sub0, qeval_sub1;

  //Get global and local elements
  const FieldTraceGroupType& global_tracegrp0 = qfld.getInteriorTraceGroup<Triangle>(0);
  const FieldTraceGroupType& local_tracegrp0 = qfld_split_local.getInteriorTraceGroup<Triangle>(0);

  ElementFieldClass fldElem_global( global_tracegrp0.basis() );
  ElementFieldClass fldElem_local0( global_tracegrp0.basis() );
  ElementFieldClass fldElem_local1( global_tracegrp0.basis() );

  //Split traces
  //Check if the solutions are equal at a few quadrature points
  for (int iquad = 0; iquad < nquad; iquad++)
  {
    quadrature.coordinates( iquad, Ref_sub );

    //------------Global BTraceGroup 0, Elem 0-----------------
    global_tracegrp0.getElement(fldElem_global, 0);
    local_tracegrp0.getElement(fldElem_local0, 0);
    local_tracegrp0.getElement(fldElem_local1, 1);

    fldElem_local0.eval(Ref_sub,qeval_sub0);
    fldElem_local1.eval(Ref_sub,qeval_sub1);

    int Itrace_split_edge_index = 0;

    BasisFunction_RefElement_Split<TopoD2, Triangle>::transform(Ref_sub, ElementSplitType::Edge, Itrace_split_edge_index, 1, Ref_main);
    fldElem_global.eval(Ref_main, qeval_main0);

    BasisFunction_RefElement_Split<TopoD2, Triangle>::transform(Ref_sub, ElementSplitType::Edge, Itrace_split_edge_index, 0, Ref_main);
    fldElem_global.eval(Ref_main, qeval_main1);

    for (int i=0; i<2; i++)
    {
      SANS_CHECK_CLOSE(qeval_sub0[i], qeval_main0[i], tol, tol);
      SANS_CHECK_CLOSE(qeval_sub1[i], qeval_main1[i], tol, tol);
    }
  }

  //DOFs for newly added interior traces should be zero
  for (int n = 0; n < 2*3; n++)
    for (int i=0; i<2; i++)
      BOOST_CHECK_EQUAL(qfld_split_local.DOF(n+6)[i], 0.0);


  BOOST_CHECK_EQUAL( qfld_split_local.nCellGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_split_local.nInteriorTraceGroups(), 3 );
  BOOST_CHECK_EQUAL( qfld_split_local.nBoundaryTraceGroups(), 0 );

  BOOST_REQUIRE( qfld_split_local.getInteriorTraceGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( qfld_split_local.getInteriorTraceGroupBase(1).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( qfld_split_local.getInteriorTraceGroupBase(2).topoTypeID() == typeid(Triangle) );

  const Field<PhysD3,TopoD3,ArrayQ>::FieldTraceGroupType<Triangle>& qfldITraceGroup0 = qfld_split_local.getInteriorTraceGroup<Triangle>(0);
  const Field<PhysD3,TopoD3,ArrayQ>::FieldTraceGroupType<Triangle>& qfldITraceGroup1 = qfld_split_local.getInteriorTraceGroup<Triangle>(1);
  const Field<PhysD3,TopoD3,ArrayQ>::FieldTraceGroupType<Triangle>& qfldITraceGroup2 = qfld_split_local.getInteriorTraceGroup<Triangle>(2);

  BOOST_CHECK_EQUAL( qfldITraceGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( qfldITraceGroup1.nElem(), 1 );
  BOOST_CHECK_EQUAL( qfldITraceGroup2.nElem(), 1 );

  int DOFMap[3];

  int DOFcount = 0;

  for (int elem=0; elem < qfldITraceGroup0.nElem(); elem++)
  {
    qfldITraceGroup0.associativity(elem).getNodeGlobalMapping( DOFMap, 3 );
    for (int k=0; k<3; k++)
    {
      BOOST_CHECK_EQUAL( DOFMap[k], DOFcount );
      DOFcount++;
    }
  }

  for (int elem=0; elem < qfldITraceGroup1.nElem(); elem++)
  {
    qfldITraceGroup1.associativity(elem).getNodeGlobalMapping( DOFMap, 3 );
    for (int k=0; k<3; k++)
    {
      BOOST_CHECK_EQUAL( DOFMap[k], DOFcount );
      DOFcount++;
    }
  }

  for (int elem=0; elem < qfldITraceGroup2.nElem(); elem++)
  {
    qfldITraceGroup2.associativity(elem).getNodeGlobalMapping( DOFMap, 3 );
    for (int k=0; k<3; k++)
    {
      BOOST_CHECK_EQUAL( DOFMap[k], DOFcount );
      DOFcount++;
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field3D_DG_Local_2Tet_X1_ExtractLeftCell_SplitIsotropic_InteriorTrace_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_InteriorTrace<PhysD3, TopoD3, ArrayQ> QField3D_DG_ITrace;

  typedef DLA::VectorS<TopoD2::D,Real> RefCoordType;

  typedef QField3D_DG_ITrace::FieldTraceGroupType<Triangle> FieldTraceGroupType;
  typedef FieldTraceGroupType::ElementType<> ElementFieldClass;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  Real tol = 1e-11;

  XField3D_2Tet_X1_1Group xfld;

  int order = 1;
  QField3D_DG_ITrace qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  //Extract the local mesh for the left tet
  int main_group = 0;
  int main_elem = 1;
  XField_Local<PhysD3,TopoD3> xfld_local(comm_local,connectivity,main_group,main_elem);

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity_local(xfld_local);

  int split_edge_index = -1;
  XField_Local_Split<PhysD3,TopoD3> xfld_split_local(xfld_local, connectivity_local,
                                                     ElementSplitType::Isotropic, split_edge_index);

  Field_Local<QField3D_DG_ITrace> qfld_split_local(xfld_split_local, qfld, order, BasisFunctionCategory_Hierarchical);

#if 0
  xfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  xfld_split_local.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;

  std::cout<<std::endl<<std::endl;
  qfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  qfld_split_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_split_local.getXField(), &xfld_split_local );

  BOOST_CHECK_EQUAL( qfld_split_local.nDOF(), 15*3 );

  //Get order 3 quad points
  Quadrature<TopoD2, Triangle> quadrature(3);
  const int nquad = quadrature.nQuadrature();

  RefCoordType Ref_sub;  // reference-element coordinates on sub-element
  RefCoordType Ref_main; // reference-element coordinates on main-element
  ArrayQ qeval_main0, qeval_main1, qeval_sub0, qeval_sub1;

  //Get global and local elements
  const FieldTraceGroupType& global_tracegrp0 = qfld.getInteriorTraceGroup<Triangle>(0);
  const FieldTraceGroupType& local_tracegrp0 = qfld_split_local.getInteriorTraceGroup<Triangle>(0);

  ElementFieldClass fldElem_global( global_tracegrp0.basis() );
  ElementFieldClass fldElem_local0( global_tracegrp0.basis() );
//  ElementFieldClass fldElem_local1( global_tracegrp0.basis() );

  //------------Global ITraceGroup 0, Elem 0-----------------
  global_tracegrp0.getElement(fldElem_global, 0);

  //Rotate global fldElem to orientation (-1) because interior trace was flipped from global mesh to local mesh
  ElementFieldClass fldElem_global_reoriented( global_tracegrp0.basis() );
  fldElem_global_reoriented.DOF(0) = fldElem_global.DOF(0);
  fldElem_global_reoriented.DOF(1) = fldElem_global.DOF(2);
  fldElem_global_reoriented.DOF(2) = fldElem_global.DOF(1);

  //Split traces
  //Check if the solutions are equal at a few quadrature points
  for (int iquad = 0; iquad < nquad; iquad++)
  {
    quadrature.coordinates( iquad, Ref_sub );

    for (int subcell = 0; subcell < 4; subcell++)
    {
      local_tracegrp0.getElement(fldElem_local0, subcell);
      fldElem_local0.eval(Ref_sub,qeval_sub0);

      BasisFunction_RefElement_Split<TopoD2, Triangle>::transform(Ref_sub, ElementSplitType::Isotropic, -1, subcell, Ref_main);
      fldElem_global_reoriented.eval(Ref_main, qeval_main0);

      for (int i=0; i<2; i++)
        SANS_CHECK_CLOSE(qeval_sub0[i], qeval_main0[i], tol, tol);
    }
  }

  //DOFs for newly added interior traces should be zero
  for (int n = 0; n < 4*3; n++)
    for (int i=0; i<2; i++)
      BOOST_CHECK_EQUAL(qfld_split_local.DOF(n+12)[i], 0.0);


  BOOST_CHECK_EQUAL( qfld_split_local.nCellGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_split_local.nInteriorTraceGroups(), 3 );
  BOOST_CHECK_EQUAL( qfld_split_local.nBoundaryTraceGroups(), 0 );

  BOOST_REQUIRE( qfld_split_local.getInteriorTraceGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( qfld_split_local.getInteriorTraceGroupBase(1).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( qfld_split_local.getInteriorTraceGroupBase(2).topoTypeID() == typeid(Triangle) );

  const Field<PhysD3,TopoD3,ArrayQ>::FieldTraceGroupType<Triangle>& qfldITraceGroup0 = qfld_split_local.getInteriorTraceGroup<Triangle>(0);
  const Field<PhysD3,TopoD3,ArrayQ>::FieldTraceGroupType<Triangle>& qfldITraceGroup1 = qfld_split_local.getInteriorTraceGroup<Triangle>(1);
  const Field<PhysD3,TopoD3,ArrayQ>::FieldTraceGroupType<Triangle>& qfldITraceGroup2 = qfld_split_local.getInteriorTraceGroup<Triangle>(2);

  BOOST_CHECK_EQUAL( qfldITraceGroup0.nElem(), 4 );
  BOOST_CHECK_EQUAL( qfldITraceGroup1.nElem(), 8 );
  BOOST_CHECK_EQUAL( qfldITraceGroup2.nElem(), 3 );

  int DOFMap[3];

  int DOFcount = 0;

  for (int elem=0; elem < qfldITraceGroup0.nElem(); elem++)
  {
    qfldITraceGroup0.associativity(elem).getNodeGlobalMapping( DOFMap, 3 );
    for (int k=0; k<3; k++)
    {
      BOOST_CHECK_EQUAL( DOFMap[k], DOFcount );
      DOFcount++;
    }
  }

  for (int elem=0; elem < qfldITraceGroup1.nElem(); elem++)
  {
    qfldITraceGroup1.associativity(elem).getNodeGlobalMapping( DOFMap, 3 );
    for (int k=0; k<3; k++)
    {
      BOOST_CHECK_EQUAL( DOFMap[k], DOFcount );
      DOFcount++;
    }
  }

  for (int elem=0; elem < qfldITraceGroup2.nElem(); elem++)
  {
    qfldITraceGroup2.associativity(elem).getNodeGlobalMapping( DOFMap, 3 );
    for (int k=0; k<3; k++)
    {
      BOOST_CHECK_EQUAL( DOFMap[k], DOFcount );
      DOFcount++;
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field3D_DG_Local_2Tet_X1_ExtractRightCell_SplitIsotropic_InteriorTrace_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_InteriorTrace<PhysD3, TopoD3, ArrayQ> QField3D_DG_ITrace;

  typedef DLA::VectorS<TopoD2::D,Real> RefCoordType;

  typedef QField3D_DG_ITrace::FieldTraceGroupType<Triangle> FieldTraceGroupType;
  typedef FieldTraceGroupType::ElementType<> ElementFieldClass;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  Real tol = 1e-11;

  XField3D_2Tet_X1_1Group xfld;

  int order = 1;
  QField3D_DG_ITrace qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  //Extract the local mesh for the right tet
  int main_group = 0;
  int main_elem = 0;
  XField_Local<PhysD3,TopoD3> xfld_local(comm_local,connectivity,main_group,main_elem);

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity_local(xfld_local);

  int split_edge_index = -1;
  XField_Local_Split<PhysD3,TopoD3> xfld_split_local(xfld_local, connectivity_local,
                                                     ElementSplitType::Isotropic, split_edge_index);

  Field_Local<QField3D_DG_ITrace> qfld_split_local(xfld_split_local, qfld, order, BasisFunctionCategory_Hierarchical);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  qfld_split_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_split_local.getXField(), &xfld_split_local );

  BOOST_CHECK_EQUAL( qfld_split_local.nDOF(), 15*3 );

  //Get order 3 quad points
  Quadrature<TopoD2, Triangle> quadrature(3);
  const int nquad = quadrature.nQuadrature();

  RefCoordType Ref_sub;  // reference-element coordinates on sub-element
  RefCoordType Ref_main; // reference-element coordinates on main-element
  ArrayQ qeval_main0, qeval_main1, qeval_sub0, qeval_sub1;

  //Get global and local elements
  const FieldTraceGroupType& global_tracegrp0 = qfld.getInteriorTraceGroup<Triangle>(0);
  const FieldTraceGroupType& local_tracegrp0 = qfld_split_local.getInteriorTraceGroup<Triangle>(0);

  ElementFieldClass fldElem_global( global_tracegrp0.basis() );
  ElementFieldClass fldElem_local0( global_tracegrp0.basis() );
  ElementFieldClass fldElem_local1( global_tracegrp0.basis() );

  //Split traces
  //Check if the solutions are equal at a few quadrature points
  for (int iquad = 0; iquad < nquad; iquad++)
  {
    quadrature.coordinates( iquad, Ref_sub );

    //------------Global BTraceGroup 0, Elem 0-----------------
    global_tracegrp0.getElement(fldElem_global, 0);

    for (int subcell = 0; subcell < 4; subcell++)
    {
      local_tracegrp0.getElement(fldElem_local0, subcell);
      fldElem_local0.eval(Ref_sub,qeval_sub0);

      BasisFunction_RefElement_Split<TopoD2, Triangle>::transform(Ref_sub, ElementSplitType::Isotropic, -1, subcell, Ref_main);
      fldElem_global.eval(Ref_main, qeval_main0);

      for (int i=0; i<2; i++)
        SANS_CHECK_CLOSE(qeval_sub0[i], qeval_main0[i], tol, tol);
    }
  }

  //DOFs for newly added interior traces should be zero
  for (int n = 0; n < 4*3; n++)
    for (int i=0; i<2; i++)
      BOOST_CHECK_EQUAL(qfld_split_local.DOF(n+12)[i], 0.0);


  BOOST_CHECK_EQUAL( qfld_split_local.nCellGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_split_local.nInteriorTraceGroups(), 3 );
  BOOST_CHECK_EQUAL( qfld_split_local.nBoundaryTraceGroups(), 0 );

  BOOST_REQUIRE( qfld_split_local.getInteriorTraceGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( qfld_split_local.getInteriorTraceGroupBase(1).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( qfld_split_local.getInteriorTraceGroupBase(2).topoTypeID() == typeid(Triangle) );

  const Field<PhysD3,TopoD3,ArrayQ>::FieldTraceGroupType<Triangle>& qfldITraceGroup0 = qfld_split_local.getInteriorTraceGroup<Triangle>(0);
  const Field<PhysD3,TopoD3,ArrayQ>::FieldTraceGroupType<Triangle>& qfldITraceGroup1 = qfld_split_local.getInteriorTraceGroup<Triangle>(1);
  const Field<PhysD3,TopoD3,ArrayQ>::FieldTraceGroupType<Triangle>& qfldITraceGroup2 = qfld_split_local.getInteriorTraceGroup<Triangle>(2);

  BOOST_CHECK_EQUAL( qfldITraceGroup0.nElem(), 4 );
  BOOST_CHECK_EQUAL( qfldITraceGroup1.nElem(), 8 );
  BOOST_CHECK_EQUAL( qfldITraceGroup2.nElem(), 3 );

  int DOFMap[3];

  int DOFcount = 0;

  for (int elem=0; elem < qfldITraceGroup0.nElem(); elem++)
  {
    qfldITraceGroup0.associativity(elem).getNodeGlobalMapping( DOFMap, 3 );
    for (int k=0; k<3; k++)
    {
      BOOST_CHECK_EQUAL( DOFMap[k], DOFcount );
      DOFcount++;
    }
  }

  for (int elem=0; elem < qfldITraceGroup1.nElem(); elem++)
  {
    qfldITraceGroup1.associativity(elem).getNodeGlobalMapping( DOFMap, 3 );
    for (int k=0; k<3; k++)
    {
      BOOST_CHECK_EQUAL( DOFMap[k], DOFcount );
      DOFcount++;
    }
  }

  for (int elem=0; elem < qfldITraceGroup2.nElem(); elem++)
  {
    qfldITraceGroup2.associativity(elem).getNodeGlobalMapping( DOFMap, 3 );
    for (int k=0; k<3; k++)
    {
      BOOST_CHECK_EQUAL( DOFMap[k], DOFcount );
      DOFcount++;
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field3D_DG_Local_2Tet_X1_ExtractRightCell_SplitEdge3_BoundaryTrace_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> QField3D_DG_BTrace;

  typedef DLA::VectorS<TopoD2::D,Real> RefCoordType;

  typedef QField3D_DG_BTrace::FieldTraceGroupType<Triangle> FieldTraceGroupType;
  typedef FieldTraceGroupType::ElementType<> ElementFieldClass;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  Real tol = 1e-11;

  XField3D_2Tet_X1_1Group xfld;

  int order = 1;
  QField3D_DG_BTrace qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  //Extract the local mesh for the right tet
  int main_group = 0;
  int main_elem = 0;
  XField_Local<PhysD3,TopoD3> xfld_local(comm_local,connectivity,main_group,main_elem);

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity_local(xfld_local);

  int split_edge_index = 3;
  XField_Local_Split<PhysD3,TopoD3> xfld_split_local(xfld_local, connectivity_local,
                                                     ElementSplitType::Edge, split_edge_index);

  Field_Local<QField3D_DG_BTrace> qfld_split_local(xfld_split_local, qfld, order, BasisFunctionCategory_Hierarchical);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  qfld_split_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_split_local.getXField(), &xfld_split_local );

  BOOST_CHECK_EQUAL( qfld_split_local.nDOF(), 4*3 );

  //Get order 3 quad points
  Quadrature<TopoD2, Triangle> quadrature(3);
  const int nquad = quadrature.nQuadrature();

  RefCoordType Ref_sub;  // reference-element coordinates on sub-element
  RefCoordType Ref_main; // reference-element coordinates on main-element
  ArrayQ qeval_main0, qeval_main1, qeval_sub0, qeval_sub1;

  //Get global element
  const FieldTraceGroupType& global_tracegrp0 = qfld.getBoundaryTraceGroup<Triangle>(0);
  const FieldTraceGroupType& global_tracegrp1 = qfld.getBoundaryTraceGroup<Triangle>(1);
  const FieldTraceGroupType& global_tracegrp2 = qfld.getBoundaryTraceGroup<Triangle>(2);
//  const FieldTraceGroupType& global_tracegrp3 = qfld.getBoundaryTraceGroup<Triangle>(3);
//  const FieldTraceGroupType& global_tracegrp4 = qfld.getBoundaryTraceGroup<Triangle>(4);
//  const FieldTraceGroupType& global_tracegrp5 = qfld.getBoundaryTraceGroup<Triangle>(5);

  const FieldTraceGroupType& local_tracegrp0 = qfld_split_local.getBoundaryTraceGroup<Triangle>(0);
  const FieldTraceGroupType& local_tracegrp1 = qfld_split_local.getBoundaryTraceGroup<Triangle>(1);
  const FieldTraceGroupType& local_tracegrp2 = qfld_split_local.getBoundaryTraceGroup<Triangle>(2);

  ElementFieldClass fldElem_global( global_tracegrp0.basis() );
  ElementFieldClass fldElem_local0( global_tracegrp0.basis() );
  ElementFieldClass fldElem_local1( global_tracegrp0.basis() );

//  std::cout<<xfld_split_local.getBoundaryTraceSplitInfo({2,0}).edge_index<<","
//           <<xfld_split_local.getBoundaryTraceSplitInfo({2,0}).subcell_index<<std::endl;
//
//  std::cout<<xfld_split_local.getBoundaryTraceSplitInfo({2,1}).edge_index<<","
//           <<xfld_split_local.getBoundaryTraceSplitInfo({2,1}).subcell_index<<std::endl;

  //Unsplit traces
  //Check if the solutions are equal at a few quadrature points
  for (int iquad = 0; iquad < nquad; iquad++)
  {
    quadrature.coordinates( iquad, Ref_sub );
    Ref_main = Ref_sub;

    //------------Global BTraceGroup 0, Elem 0-----------------
    global_tracegrp0.getElement(fldElem_global, 0);
    local_tracegrp0.getElement(fldElem_local0, 0);

    fldElem_local0.eval(Ref_sub,qeval_sub0);
    fldElem_global.eval(Ref_main, qeval_main0);

    for (int i=0; i<2; i++)
      SANS_CHECK_CLOSE(qeval_sub0[i], qeval_main0[i], tol, tol);

    //------------Global BTraceGroup 1, Elem 0-----------------
    global_tracegrp1.getElement(fldElem_global, 0);
    local_tracegrp1.getElement(fldElem_local0, 0);

    fldElem_local0.eval(Ref_sub,qeval_sub0);
    fldElem_global.eval(Ref_main, qeval_main0);

    for (int i=0; i<2; i++)
      SANS_CHECK_CLOSE(qeval_sub0[i], qeval_main0[i], tol, tol);
  }


  //Split traces
  //Check if the solutions are equal at a few quadrature points
  for (int iquad = 0; iquad < nquad; iquad++)
  {
    quadrature.coordinates( iquad, Ref_sub );

    //------------Global BTraceGroup 2, Elem 0-----------------
    global_tracegrp2.getElement(fldElem_global, 0);
    local_tracegrp2.getElement(fldElem_local0, 0);
    local_tracegrp2.getElement(fldElem_local1, 1);

    fldElem_local0.eval(Ref_sub,qeval_sub0);
    fldElem_local1.eval(Ref_sub,qeval_sub1);

    int btrace_split_edge_index = 2;

    BasisFunction_RefElement_Split<TopoD2, Triangle>::transform(Ref_sub, ElementSplitType::Edge, btrace_split_edge_index, 1, Ref_main);
    fldElem_global.eval(Ref_main, qeval_main0);

    BasisFunction_RefElement_Split<TopoD2, Triangle>::transform(Ref_sub, ElementSplitType::Edge, btrace_split_edge_index, 0, Ref_main);
    fldElem_global.eval(Ref_main, qeval_main1);

    for (int i=0; i<2; i++)
    {
      SANS_CHECK_CLOSE(qeval_sub0[i], qeval_main0[i], tol, tol);
      SANS_CHECK_CLOSE(qeval_sub1[i], qeval_main1[i], tol, tol);
    }
  }

  BOOST_CHECK_EQUAL( qfld_split_local.nCellGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_split_local.nInteriorTraceGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_split_local.nBoundaryTraceGroups(), 3 );

  BOOST_REQUIRE( qfld_split_local.getBoundaryTraceGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( qfld_split_local.getBoundaryTraceGroupBase(1).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( qfld_split_local.getBoundaryTraceGroupBase(2).topoTypeID() == typeid(Triangle) );

  const Field<PhysD3,TopoD3,ArrayQ>::FieldTraceGroupType<Triangle>& qfldBTraceGroup0 = qfld_split_local.getBoundaryTraceGroup<Triangle>(0);
  const Field<PhysD3,TopoD3,ArrayQ>::FieldTraceGroupType<Triangle>& qfldBTraceGroup1 = qfld_split_local.getBoundaryTraceGroup<Triangle>(1);
  const Field<PhysD3,TopoD3,ArrayQ>::FieldTraceGroupType<Triangle>& qfldBTraceGroup2 = qfld_split_local.getBoundaryTraceGroup<Triangle>(2);

  BOOST_CHECK_EQUAL( qfldBTraceGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( qfldBTraceGroup1.nElem(), 1 );
  BOOST_CHECK_EQUAL( qfldBTraceGroup2.nElem(), 2 );

  int DOFMap[3];

  int DOFcount = 0;

  for (int elem=0; elem < qfldBTraceGroup0.nElem(); elem++)
  {
    qfldBTraceGroup0.associativity(elem).getNodeGlobalMapping( DOFMap, 3 );
    for (int k=0; k<3; k++)
    {
      BOOST_CHECK_EQUAL( DOFMap[k], DOFcount );
      DOFcount++;
    }
  }

  for (int elem=0; elem < qfldBTraceGroup1.nElem(); elem++)
  {
    qfldBTraceGroup1.associativity(elem).getNodeGlobalMapping( DOFMap, 3 );
    for (int k=0; k<3; k++)
    {
      BOOST_CHECK_EQUAL( DOFMap[k], DOFcount );
      DOFcount++;
    }
  }

  for (int elem=0; elem < qfldBTraceGroup2.nElem(); elem++)
  {
    qfldBTraceGroup2.associativity(elem).getNodeGlobalMapping( DOFMap, 3 );
    for (int k=0; k<3; k++)
    {
      BOOST_CHECK_EQUAL( DOFMap[k], DOFcount );
      DOFcount++;
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field3D_DG_Local_2Tet_X1_ExtractLeftCell_SplitIsotropic_BoundaryTrace_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> QField3D_DG_BTrace;

  typedef DLA::VectorS<TopoD2::D,Real> RefCoordType;

  typedef QField3D_DG_BTrace::FieldTraceGroupType<Triangle> FieldTraceGroupType;
  typedef FieldTraceGroupType::ElementType<> ElementFieldClass;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  Real tol = 1e-11;

  XField3D_2Tet_X1_1Group xfld;

  int order = 1;
  QField3D_DG_BTrace qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  //Extract the local mesh for the left tet
  int main_group = 0;
  int main_elem = 1;
  XField_Local<PhysD3,TopoD3> xfld_local(comm_local,connectivity,main_group,main_elem);

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity_local(xfld_local);

  int split_edge_index = -1;
  XField_Local_Split<PhysD3,TopoD3> xfld_split_local(xfld_local, connectivity_local,
                                                     ElementSplitType::Isotropic, split_edge_index);

  Field_Local<QField3D_DG_BTrace> qfld_split_local(xfld_split_local, qfld, order, BasisFunctionCategory_Hierarchical);

#if 0
  qfld.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  qfld_split_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_split_local.getXField(), &xfld_split_local );

  BOOST_CHECK_EQUAL( qfld_split_local.nDOF(), 12*3 );

  //Get order 3 quad points
  Quadrature<TopoD2, Triangle> quadrature(3);
  const int nquad = quadrature.nQuadrature();

  RefCoordType Ref_sub;  // reference-element coordinates on sub-element
  RefCoordType Ref_main; // reference-element coordinates on main-element
  ArrayQ qeval_main, qeval_sub;

  //Get global element
//  const FieldTraceGroupType& global_tracegrp0 = qfld.getBoundaryTraceGroup<Triangle>(0);
//  const FieldTraceGroupType& global_tracegrp1 = qfld.getBoundaryTraceGroup<Triangle>(1);
//  const FieldTraceGroupType& global_tracegrp2 = qfld.getBoundaryTraceGroup<Triangle>(2);
  const FieldTraceGroupType& global_tracegrp3 = qfld.getBoundaryTraceGroup<Triangle>(3);
  const FieldTraceGroupType& global_tracegrp4 = qfld.getBoundaryTraceGroup<Triangle>(4);
  const FieldTraceGroupType& global_tracegrp5 = qfld.getBoundaryTraceGroup<Triangle>(5);

  const FieldTraceGroupType& local_tracegrp0 = qfld_split_local.getBoundaryTraceGroup<Triangle>(0);
  const FieldTraceGroupType& local_tracegrp1 = qfld_split_local.getBoundaryTraceGroup<Triangle>(1);
  const FieldTraceGroupType& local_tracegrp2 = qfld_split_local.getBoundaryTraceGroup<Triangle>(2);

  ElementFieldClass fldElem_global( global_tracegrp3.basis() );
  ElementFieldClass fldElem_local( global_tracegrp3.basis() );

  //Split traces
  //Check if the solutions are equal at a few quadrature points
  for (int iquad = 0; iquad < nquad; iquad++)
  {
    quadrature.coordinates( iquad, Ref_sub );

    //------------Global BTraceGroup 3, Elem 0-----------------
    global_tracegrp3.getElement(fldElem_global, 0);
    int btrace_split_edge_index = -1;

    for (int subelem=0; subelem < 4; subelem++)
    {
      local_tracegrp0.getElement(fldElem_local, subelem);
      fldElem_local.eval(Ref_sub,qeval_sub);

      BasisFunction_RefElement_Split<TopoD2, Triangle>::transform(Ref_sub, ElementSplitType::Isotropic, btrace_split_edge_index, subelem, Ref_main);
      fldElem_global.eval(Ref_main, qeval_main);

      for (int i=0; i<2; i++)
        SANS_CHECK_CLOSE(qeval_sub[i], qeval_main[i], tol, tol);
    }

    //------------Global BTraceGroup 4, Elem 0-----------------
    global_tracegrp4.getElement(fldElem_global, 0);
    btrace_split_edge_index = -1;

    for (int subelem=0; subelem < 4; subelem++)
    {
      local_tracegrp1.getElement(fldElem_local, subelem);
      fldElem_local.eval(Ref_sub,qeval_sub);

      BasisFunction_RefElement_Split<TopoD2, Triangle>::transform(Ref_sub, ElementSplitType::Isotropic, btrace_split_edge_index, subelem, Ref_main);
      fldElem_global.eval(Ref_main, qeval_main);

      for (int i=0; i<2; i++)
        SANS_CHECK_CLOSE(qeval_sub[i], qeval_main[i], tol, tol);
    }

    //------------Global BTraceGroup 5, Elem 0-----------------
    global_tracegrp5.getElement(fldElem_global, 0);
    btrace_split_edge_index = -1;

    for (int subelem=0; subelem < 4; subelem++)
    {
      local_tracegrp2.getElement(fldElem_local, subelem);
      fldElem_local.eval(Ref_sub,qeval_sub);

      BasisFunction_RefElement_Split<TopoD2, Triangle>::transform(Ref_sub, ElementSplitType::Isotropic, btrace_split_edge_index, subelem, Ref_main);
      fldElem_global.eval(Ref_main, qeval_main);

      for (int i=0; i<2; i++)
        SANS_CHECK_CLOSE(qeval_sub[i], qeval_main[i], tol, tol);
    }

  } //loop over quadpoints

  BOOST_CHECK_EQUAL( qfld_split_local.nCellGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_split_local.nInteriorTraceGroups(), 0 );
  BOOST_CHECK_EQUAL( qfld_split_local.nBoundaryTraceGroups(), 3 );

  BOOST_REQUIRE( qfld_split_local.getBoundaryTraceGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( qfld_split_local.getBoundaryTraceGroupBase(1).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( qfld_split_local.getBoundaryTraceGroupBase(2).topoTypeID() == typeid(Triangle) );

  const Field<PhysD3,TopoD3,ArrayQ>::FieldTraceGroupType<Triangle>& qfldBTraceGroup0 = qfld_split_local.getBoundaryTraceGroup<Triangle>(0);
  const Field<PhysD3,TopoD3,ArrayQ>::FieldTraceGroupType<Triangle>& qfldBTraceGroup1 = qfld_split_local.getBoundaryTraceGroup<Triangle>(1);
  const Field<PhysD3,TopoD3,ArrayQ>::FieldTraceGroupType<Triangle>& qfldBTraceGroup2 = qfld_split_local.getBoundaryTraceGroup<Triangle>(2);

  BOOST_CHECK_EQUAL( qfldBTraceGroup0.nElem(), 4 );
  BOOST_CHECK_EQUAL( qfldBTraceGroup1.nElem(), 4 );
  BOOST_CHECK_EQUAL( qfldBTraceGroup2.nElem(), 4 );

  int DOFMap[3];

  int DOFcount = 0;

  for (int elem=0; elem < qfldBTraceGroup0.nElem(); elem++)
  {
    qfldBTraceGroup0.associativity(elem).getNodeGlobalMapping( DOFMap, 3 );
    for (int k=0; k<3; k++)
    {
      BOOST_CHECK_EQUAL( DOFMap[k], DOFcount );
      DOFcount++;
    }
  }

  for (int elem=0; elem < qfldBTraceGroup1.nElem(); elem++)
  {
    qfldBTraceGroup1.associativity(elem).getNodeGlobalMapping( DOFMap, 3 );
    for (int k=0; k<3; k++)
    {
      BOOST_CHECK_EQUAL( DOFMap[k], DOFcount );
      DOFcount++;
    }
  }

  for (int elem=0; elem < qfldBTraceGroup2.nElem(); elem++)
  {
    qfldBTraceGroup2.associativity(elem).getNodeGlobalMapping( DOFMap, 3 );
    for (int k=0; k<3; k++)
    {
      BOOST_CHECK_EQUAL( DOFMap[k], DOFcount );
      DOFcount++;
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Field3D_DG_Local_2Tet_X1_ExtractRightCell_SplitIsotropic_InteriorTrace_ProjectFromCell_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_InteriorTrace<PhysD3, TopoD3, ArrayQ> QField3D_DG_ITrace;
  typedef Field_DG_Cell<PhysD3, TopoD3, ArrayQ> QField3D_DG_Cell;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  Real tol = 1e-12;

  XField3D_2Tet_X1_1Group xfld;

  int order = 1;
  QField3D_DG_ITrace qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  QField3D_DG_Cell qfldcell(xfld, order, BasisFunctionCategory_Hierarchical);

  //Set some non-zero solution
  for (int n = 0; n < qfldcell.nDOF(); n++)
    qfldcell.DOF(n) = {-n-1, n+1};

  //-------------CREATE LOCAL FIELD----------------

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  //Extract the local mesh for the right tet
  int main_group = 0;
  int main_elem = 0;
  XField_Local<PhysD3,TopoD3> xfld_local(comm_local,connectivity,main_group,main_elem);

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity_local(xfld_local);

  int split_edge_index = -1;
  XField_Local_Split<PhysD3,TopoD3> xfld_split_local(xfld_local, connectivity_local,
                                                     ElementSplitType::Isotropic, split_edge_index);

  Field_Local<QField3D_DG_ITrace> qfld_split_local(xfld_split_local, qfld, order, BasisFunctionCategory_Hierarchical);
  qfld_split_local = 0;

  Field_Local_Transfer<TopoD3>::transferFromFieldCell(qfldcell, qfld_split_local);

#if 0
  qfldcell.dump(3,std::cout);
  std::cout<<std::endl<<std::endl;
  qfld_split_local.dump(3,std::cout);
#endif

  //-------------CHECK LOCAL FIELD----------------

  BOOST_CHECK_EQUAL( &qfld_split_local.getXField(), &xfld_split_local );

  BOOST_CHECK_EQUAL( qfld_split_local.nDOF(), 15*3 );

  //Interior trace DOFs between main-cell and neighbor-cell are zero
  //Not transferred from the cell field because those traces are not new)
  for (int i = 0; i < 12; i++)
  {
    SANS_CHECK_CLOSE(qfld_split_local.DOF(i)[0], 0.0, tol, tol);
    SANS_CHECK_CLOSE(qfld_split_local.DOF(i)[1], 0.0, tol, tol);
  }

  Real itraceDOF_grp1[24] = {2.0, 2.5, 3.5,
                             2.5, 3.5, 3.0,
                             1.5, 3.0, 2.5,
                             1.5, 2.0, 2.5,
                             2.0, 3.0, 3.5,
                             2.0, 2.5, 3.0,
                             2.5, 2.0, 3.0,
                             1.5, 3.0, 2.0};

  for (int i = 0; i < 24; i++)
  {
    SANS_CHECK_CLOSE(qfld_split_local.DOF(12 + i)[0], -itraceDOF_grp1[i], tol, tol);
    SANS_CHECK_CLOSE(qfld_split_local.DOF(12 + i)[1],  itraceDOF_grp1[i], tol, tol);
  }

  Real itraceDOF_grp2[9] = {5.0, 6.5, 7.5,
                            5.0, 7.5, 7.0,
                            5.0, 7.0, 6.5};

  for (int i = 0; i < 9; i++)
  {
    SANS_CHECK_CLOSE(qfld_split_local.DOF(36 + i)[0], -itraceDOF_grp2[i], tol, tol);
    SANS_CHECK_CLOSE(qfld_split_local.DOF(36 + i)[1],  itraceDOF_grp2[i], tol, tol);
  }

  BOOST_CHECK_EQUAL( qfld_split_local.nCellGroups(), 0 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( qfld_split_local.nInteriorTraceGroups(), 3 );
  BOOST_REQUIRE( qfld_split_local.getInteriorTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( qfld_split_local.getInteriorTraceGroup<Triangle>(1).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( qfld_split_local.getInteriorTraceGroup<Triangle>(2).topoTypeID() == typeid(Triangle) );

  const QField3D_DG_ITrace::FieldTraceGroupType<Triangle>& qldITraceGroup0 = qfld_split_local.getInteriorTraceGroup<Triangle>(0);
  const QField3D_DG_ITrace::FieldTraceGroupType<Triangle>& qldITraceGroup1 = qfld_split_local.getInteriorTraceGroup<Triangle>(1);
  const QField3D_DG_ITrace::FieldTraceGroupType<Triangle>& qldITraceGroup2 = qfld_split_local.getInteriorTraceGroup<Triangle>(2);

  BOOST_CHECK_EQUAL( qldITraceGroup0.nElem(), 4 );
  BOOST_CHECK_EQUAL( qldITraceGroup1.nElem(), 8 );
  BOOST_CHECK_EQUAL( qldITraceGroup2.nElem(), 3 );

  BOOST_CHECK_EQUAL( qfld_split_local.nBoundaryTraceGroups(), 0 );
}
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
