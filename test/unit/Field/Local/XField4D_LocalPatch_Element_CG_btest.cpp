// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// XField2D_Local_btest
// testing of XField2D_Local
//
// Note: unit grids tested in "unit/UnitGrids/UnitGrids_btest.cpp"

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "Topology/ElementTopology.h"

#include "Field/XField_CellToTrace.h"
#include "Field/Local/XField_LocalPatch.h"
#include "Field/Field_NodalView.h"

#include "XField_LocalGlobal_Equiv_btest.h"

#include "Field/FieldSpacetime_DG_Cell.h"
#include "Field/FieldSpacetime_DG_InteriorTrace.h"
#include "Field/FieldSpacetime_DG_BoundaryTrace.h"

#include "unit/UnitGrids/XField4D_1Ptope_X1_1Group.h"
#include "unit/UnitGrids/XField4D_2Ptope_X1_1Group.h"
#include "unit/UnitGrids/XField4D_6Ptope_X1_1Group_NegativeTraceOrientation.h"
#include "unit/UnitGrids/XField4D_24Ptope_X1_1Group.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// ########################################################################## //
BOOST_AUTO_TEST_SUITE(XField4D_LocalPatch_Element_CG_test_suite)

// -------------------------------------------------------------------------- //
//                               UNSPLIT TESTS                                //
// -------------------------------------------------------------------------- //

BOOST_AUTO_TEST_CASE(Unsplit_1Ptope_X1_1Group_test)
{
  mpi::communicator world;
  mpi::communicator comm_local= world.split(world.rank());

  XField4D_1Ptope_X1_1Group xfld;

  // build cell-to-trace connectivity structure
  XField_CellToTrace<PhysD4, TopoD4> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld, {0});

  // extract the local mesh for a boundary triangle
  XField_LocalPatchConstructor<PhysD4, Pentatope> xfld_construct(comm_local,
                                                                 connectivity,
                                                                 0, 0,
                                                                 SpaceType::Continuous,
                                                                 &nodalview);
  XField_LocalPatch<PhysD4, Pentatope> xfld_local(xfld_construct);

  BOOST_CHECK_EQUAL(&connectivity, &xfld_local.connectivity());

  BOOST_CHECK_EQUAL(xfld_local.nDOF(), 5);

  // area field variable

  BOOST_CHECK_EQUAL(xfld_local.nCellGroups(), 2);
  for (int iCG= 0; iCG < xfld_local.nCellGroups(); iCG++)
    BOOST_REQUIRE(xfld_local.getCellGroupBase(iCG).topoTypeID() == typeid(Pentatope));
  BOOST_CHECK_EQUAL(xfld_local.getCellGroupBase(0).nElem(), 1); // the unsplit element
  BOOST_CHECK_EQUAL(xfld_local.getCellGroupBase(1).nElem(), 0); // other elements on the local mesh

  // interior-edge field variable
  BOOST_CHECK_EQUAL(xfld_local.nInteriorTraceGroups(), 0);
  for (int iITG= 0; iITG < xfld_local.nInteriorTraceGroups(); iITG++)
    BOOST_REQUIRE(xfld_local.getInteriorTraceGroupBase(iITG).topoTypeID() == typeid(Tet));

  BOOST_CHECK_EQUAL(xfld_local.nBoundaryTraceGroups(), 5);
  for (int iBTG= 0; iBTG < xfld_local.nBoundaryTraceGroups(); iBTG++)
  {
    BOOST_REQUIRE(xfld_local.getBoundaryTraceGroupBase(iBTG).topoTypeID() == typeid(Tet));
    BOOST_CHECK_EQUAL(xfld_local.getBoundaryTraceGroupBase(iBTG).nElem(), 1);
  }

  BOOST_CHECK_EQUAL(xfld_local.nGhostBoundaryTraceGroups(), 1);
  for (int iGBTG= 0; iGBTG < xfld_local.nGhostBoundaryTraceGroups(); iGBTG++)
  {
    BOOST_REQUIRE(xfld_local.getGhostBoundaryTraceGroupBase(iGBTG).topoTypeID() == typeid(Tet));
    BOOST_CHECK_EQUAL(xfld_local.getGhostBoundaryTraceGroupBase(iGBTG).nElem(), 0);
  }

  XField_LocalGlobal_Equiv(xfld_local);
}

BOOST_AUTO_TEST_CASE(Unsplit_2Ptope_X1_1Group_test)
{
  mpi::communicator world;
  mpi::communicator comm_local= world.split(world.rank());

  XField4D_2Ptope_X1_1Group xfld;

  // build cell-to-trace connectivity structure
  XField_CellToTrace<PhysD4, TopoD4> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld, {0});

  // loop over elements in original grid
  for (int elem= 0; elem < xfld.nElem(); elem++)
  {
    // extract the local mesh for a boundary triangle
    XField_LocalPatchConstructor<PhysD4, Pentatope> xfld_construct(comm_local,
                                                                   connectivity,
                                                                   0, elem,
                                                                   SpaceType::Continuous,
                                                                   &nodalview);
    XField_LocalPatch<PhysD4, Pentatope> xfld_local(xfld_construct);

    BOOST_CHECK_EQUAL(&connectivity, &xfld_local.connectivity());

    BOOST_CHECK_EQUAL(xfld_local.nDOF(), 6);

    // area field variable

    BOOST_CHECK_EQUAL(xfld_local.nCellGroups(), 2);
    for (int iCG= 0; iCG < xfld_local.nCellGroups(); iCG++)
      BOOST_REQUIRE(xfld_local.getCellGroupBase(iCG).topoTypeID() == typeid(Pentatope));
    BOOST_CHECK_EQUAL(xfld_local.getCellGroupBase(0).nElem(), 1); // the unsplit elements
    BOOST_CHECK_EQUAL(xfld_local.getCellGroupBase(1).nElem(), 1); // other elements on the local mesh

    // interior-edge field variable
    BOOST_CHECK_EQUAL(xfld_local.nInteriorTraceGroups(), 1);
    for (int iITG= 0; iITG < xfld_local.nInteriorTraceGroups(); iITG++)
      BOOST_REQUIRE(xfld_local.getInteriorTraceGroupBase(iITG).topoTypeID() == typeid(Tet));
    BOOST_CHECK_EQUAL(xfld_local.getInteriorTraceGroupBase(0).nElem(), 1); // the interface between the two elements

    BOOST_CHECK_EQUAL(xfld_local.nBoundaryTraceGroups(), 8);
    for (int iBTG= 0; iBTG < xfld_local.nBoundaryTraceGroups(); iBTG++)
    {
      BOOST_REQUIRE(xfld_local.getBoundaryTraceGroupBase(iBTG).topoTypeID() == typeid(Tet));
      // there are two boundary planes that have two elements, others one
      BOOST_CHECK(xfld_local.getBoundaryTraceGroupBase(iBTG).nElem() == 1
                      || xfld_local.getBoundaryTraceGroupBase(iBTG).nElem() == 2);
    }

    BOOST_CHECK_EQUAL(xfld_local.nGhostBoundaryTraceGroups(), 1);
    for (int iGBTG= 0; iGBTG < xfld_local.nGhostBoundaryTraceGroups(); iGBTG++)
      BOOST_REQUIRE(xfld_local.getGhostBoundaryTraceGroupBase(iGBTG).topoTypeID() == typeid(Tet));

    XField_LocalGlobal_Equiv(xfld_local);
  }
}

BOOST_AUTO_TEST_CASE(Unsplit_6Ptope_X1_1Group_test)
{
  mpi::communicator world;
  mpi::communicator comm_local= world.split(world.rank());

  XField4D_6Ptope_X1_1Group_NegativeTraceOrientation xfld(world);

  // build cell-to-trace connectivity structure
  XField_CellToTrace<PhysD4, TopoD4> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld, {0});

  // make sure central element behaves as expected (a pure local patch)
  {
    // extract the local mesh for a boundary triangle
    XField_LocalPatchConstructor<PhysD4, Pentatope> xfld_construct(comm_local,
                                                                   connectivity,
                                                                   0, 0,
                                                                   SpaceType::Continuous,
                                                                   &nodalview);
    XField_LocalPatch<PhysD4, Pentatope> xfld_local(xfld_construct);

    BOOST_CHECK_EQUAL(&connectivity, &xfld_local.connectivity());

    BOOST_CHECK_EQUAL(xfld_local.nDOF(), 10); // whole jont in play

    // area field variable
    BOOST_CHECK_EQUAL(xfld_local.nCellGroups(), 2);
    for (int iCG= 0; iCG < xfld_local.nCellGroups(); iCG++)
      BOOST_REQUIRE(xfld_local.getCellGroupBase(iCG).topoTypeID() == typeid(Pentatope));
    BOOST_CHECK_EQUAL(xfld_local.getCellGroupBase(0).nElem(), 1); // the unsplit elements
    BOOST_CHECK_EQUAL(xfld_local.getCellGroupBase(1).nElem(), 5); // other elements on the local mesh

    // interior-edge field variable
    BOOST_CHECK_EQUAL(xfld_local.nInteriorTraceGroups(), 1);
    for (int iITG= 0; iITG < xfld_local.nInteriorTraceGroups(); iITG++)
      BOOST_REQUIRE(xfld_local.getInteriorTraceGroupBase(iITG).topoTypeID() == typeid(Tet));
    BOOST_CHECK_EQUAL(xfld_local.getInteriorTraceGroupBase(0).nElem(), 5); // the interface between the elements

    BOOST_CHECK_EQUAL(xfld_local.nBoundaryTraceGroups(), 1);
    for (int iBTG= 0; iBTG < xfld_local.nBoundaryTraceGroups(); iBTG++)
    {
      BOOST_REQUIRE(xfld_local.getBoundaryTraceGroupBase(iBTG).topoTypeID() == typeid(Tet));
      BOOST_CHECK_EQUAL(xfld_local.getBoundaryTraceGroupBase(iBTG).nElem(), 20); // every exterior neighbor facet is in play
    }

    BOOST_CHECK_EQUAL(xfld_local.nGhostBoundaryTraceGroups(), 1);
    for (int iGBTG= 0; iGBTG < xfld_local.nGhostBoundaryTraceGroups(); iGBTG++)
      BOOST_REQUIRE(xfld_local.getGhostBoundaryTraceGroupBase(iGBTG).topoTypeID() == typeid(Tet));

    XField_LocalGlobal_Equiv(xfld_local);

  }

  // loop over the other elements in grid
  for (int elem= 1; elem < xfld.nElem(); elem++)
  {
    // extract the local mesh for a boundary triangle
    XField_LocalPatchConstructor<PhysD4, Pentatope> xfld_construct(comm_local,
                                                                   connectivity,
                                                                   0, elem,
                                                                   SpaceType::Continuous,
                                                                   &nodalview);
    XField_LocalPatch<PhysD4, Pentatope> xfld_local(xfld_construct);

    BOOST_CHECK_EQUAL(&connectivity, &xfld_local.connectivity());

    BOOST_CHECK_EQUAL(xfld_local.nDOF(), 10); // whole jont in play

    // area field variable

    BOOST_CHECK_EQUAL(xfld_local.nCellGroups(), 2);
    for (int iCG= 0; iCG < xfld_local.nCellGroups(); iCG++)
      BOOST_REQUIRE(xfld_local.getCellGroupBase(iCG).topoTypeID() == typeid(Pentatope));
    BOOST_CHECK_EQUAL(xfld_local.getCellGroupBase(0).nElem(), 1); // the unsplit elements
    BOOST_CHECK_EQUAL(xfld_local.getCellGroupBase(1).nElem(), 5); // other elements on the local mesh

    // interior-edge field variable
    BOOST_CHECK_EQUAL(xfld_local.nInteriorTraceGroups(), 2);
    for (int iITG= 0; iITG < xfld_local.nInteriorTraceGroups(); iITG++)
      BOOST_REQUIRE(xfld_local.getInteriorTraceGroupBase(iITG).topoTypeID() == typeid(Tet));
    BOOST_CHECK_EQUAL(xfld_local.getInteriorTraceGroupBase(0).nElem(), 1); // the interface between the split element and neighbor
    BOOST_CHECK_EQUAL(xfld_local.getInteriorTraceGroupBase(1).nElem(), 4); // the interface between the neighbor elements

    BOOST_CHECK_EQUAL(xfld_local.nBoundaryTraceGroups(), 2);
    for (int iBTG= 0; iBTG < xfld_local.nBoundaryTraceGroups(); iBTG++)
    {
      BOOST_REQUIRE(xfld_local.getBoundaryTraceGroupBase(iBTG).topoTypeID() == typeid(Tet));
      BOOST_CHECK(xfld_local.getBoundaryTraceGroupBase(iBTG).nElem() || 4           // unless the split happens out in space
                      || xfld_local.getBoundaryTraceGroupBase(iBTG).nElem() || 20); // every exterior neighbor facet is in play
    }

    BOOST_CHECK_EQUAL(xfld_local.nGhostBoundaryTraceGroups(), 1);
    for (int iGBTG= 0; iGBTG < xfld_local.nGhostBoundaryTraceGroups(); iGBTG++)
      BOOST_REQUIRE(xfld_local.getGhostBoundaryTraceGroupBase(iGBTG).topoTypeID() == typeid(Tet));

    XField_LocalGlobal_Equiv(xfld_local);
  }
}

BOOST_AUTO_TEST_CASE(Unsplit_24Ptope_X1_1Group_test)
{
  mpi::communicator world;
  mpi::communicator comm_local= world.split(world.rank());

  XField4D_24Ptope_X1_1Group xfld;

  // build cell-to-trace connectivity structure
  XField_CellToTrace<PhysD4, TopoD4> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld, {0});

  // loop over elements in original grid
  for (int elem= 0; elem < xfld.nElem(); elem++)
  {
    // extract the local mesh for a boundary triangle
    XField_LocalPatchConstructor<PhysD4, Pentatope> xfld_construct(comm_local,
                                                                   connectivity,
                                                                   0, elem,
                                                                   SpaceType::Continuous,
                                                                   &nodalview);
    XField_LocalPatch<PhysD4, Pentatope> xfld_local(xfld_construct);

    BOOST_CHECK_EQUAL(&connectivity, &xfld_local.connectivity());

    // every pentatope is on two boundaries for CKF unit tesseract
    // thus any given pentatope only has 3 neighboring pentatopes
    BOOST_CHECK_EQUAL(xfld_local.nDOF(), 16);

    // area field variable

    BOOST_CHECK_EQUAL(xfld_local.nCellGroups(), 2);
    for (int iCG= 0; iCG < xfld_local.nCellGroups(); iCG++)
      BOOST_REQUIRE(xfld_local.getCellGroupBase(iCG).topoTypeID() == typeid(Pentatope));
    BOOST_CHECK_EQUAL(xfld_local.getCellGroupBase(0).nElem(), 1); // the unsplit elements
    BOOST_CHECK_EQUAL(xfld_local.getCellGroupBase(1).nElem(), 23); // other elements on the local mesh

    // interior-edge field variable
    BOOST_CHECK_EQUAL(xfld_local.nInteriorTraceGroups(), 2);
    for (int iITG= 0; iITG < xfld_local.nInteriorTraceGroups(); iITG++)
      BOOST_REQUIRE(xfld_local.getInteriorTraceGroupBase(iITG).topoTypeID() == typeid(Tet));
    BOOST_CHECK(xfld_local.getInteriorTraceGroupBase(0).nElem() == 3           // either the complement of the boundaries touched by any given element
                    || xfld_local.getInteriorTraceGroupBase(0).nElem() == 33); // or all the other interior ones
    BOOST_CHECK(xfld_local.getInteriorTraceGroupBase(1).nElem() == 3           // either the complement of the boundaries touched by any given element
                    || xfld_local.getInteriorTraceGroupBase(1).nElem() == 33); // or all the other interior ones

    BOOST_CHECK_EQUAL(xfld_local.nBoundaryTraceGroups(), 2);
    for (int iBTG= 0; iBTG < xfld_local.nBoundaryTraceGroups(); iBTG++)
      BOOST_REQUIRE(xfld_local.getBoundaryTraceGroupBase(iBTG).topoTypeID() == typeid(Tet));
    BOOST_CHECK_EQUAL(xfld_local.getBoundaryTraceGroupBase(0).nElem(), 2); // the two boundaries touched by any given element in CKF
    BOOST_CHECK_EQUAL(xfld_local.getBoundaryTraceGroupBase(1).nElem(), 46); // the other boundaries that exist on the 1x1x1x1

    BOOST_CHECK_EQUAL(xfld_local.nGhostBoundaryTraceGroups(), 1);
    for (int iGBTG= 0; iGBTG < xfld_local.nGhostBoundaryTraceGroups(); iGBTG++)
      BOOST_REQUIRE(xfld_local.getGhostBoundaryTraceGroupBase(iGBTG).topoTypeID() == typeid(Tet));

    XField_LocalGlobal_Equiv(xfld_local);
  }
}

// -------------------------------------------------------------------------- //
//                                SPLIT  TESTS                                //
// -------------------------------------------------------------------------- //

BOOST_AUTO_TEST_CASE(Split_1Ptope_X1_1Group_test)
{
  mpi::communicator world;
  mpi::communicator comm_local= world.split(world.rank());

  XField4D_1Ptope_X1_1Group xfld;

  // build cell-to-trace connectivity structure
  XField_CellToTrace<PhysD4, TopoD4> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld, {0});

  // extract the local mesh for a boundary triangle
  XField_LocalPatchConstructor<PhysD4, Pentatope> xfld_construct(comm_local,
                                                                 connectivity,
                                                                 0, 0,
                                                                 SpaceType::Continuous,
                                                                 &nodalview);

  for (int edge= 0; edge < Pentatope::NEdge; edge++)
  {
    XField_LocalPatch<PhysD4, Pentatope> xfld_local(xfld_construct,
                                                    ElementSplitType::Edge,
                                                    edge);

    BOOST_CHECK_EQUAL(&connectivity, &xfld_local.connectivity());

    BOOST_CHECK_EQUAL(xfld_local.nDOF(), 6); // extra DOF added!

    // area field variable

    BOOST_CHECK_EQUAL(xfld_local.nCellGroups(), 2);
    for (int iCG= 0; iCG < xfld_local.nCellGroups(); iCG++)
      BOOST_REQUIRE(xfld_local.getCellGroupBase(iCG).topoTypeID() == typeid(Pentatope));
    BOOST_CHECK_EQUAL(xfld_local.getCellGroupBase(0).nElem(), 2); // the newly split elements
    BOOST_CHECK_EQUAL(xfld_local.getCellGroupBase(1).nElem(), 0); // other elements on the local mesh

    // interior-edge field variable
    BOOST_CHECK_EQUAL(xfld_local.nInteriorTraceGroups(), 1);
    for (int iITG= 0; iITG < xfld_local.nInteriorTraceGroups(); iITG++)
      BOOST_REQUIRE(xfld_local.getInteriorTraceGroupBase(iITG).topoTypeID() == typeid(Tet));
    BOOST_CHECK_EQUAL(xfld_local.getInteriorTraceGroupBase(0).nElem(), 1); // interface between newly split elements
    // BOOST_CHECK_EQUAL(xfld_local.getInteriorTraceGroupBase(1).nElem(), 0); // other interfaces on local patch
    // BOOST_CHECK_EQUAL(xfld_local.getInteriorTraceGroupBase(2).nElem(), 0); // interface between opposing split elements

    BOOST_CHECK_EQUAL(xfld_local.nBoundaryTraceGroups(), 5);
    for (int iBTG= 0; iBTG < xfld_local.nBoundaryTraceGroups(); iBTG++)
    {
      BOOST_REQUIRE(xfld_local.getBoundaryTraceGroupBase(iBTG).topoTypeID() == typeid(Tet));
      // either 1 or 2 depending if boundary is split
      BOOST_CHECK(xfld_local.getBoundaryTraceGroupBase(iBTG).nElem() == 1
                      || xfld_local.getBoundaryTraceGroupBase(iBTG).nElem() == 2);
    }

    BOOST_CHECK_EQUAL(xfld_local.nGhostBoundaryTraceGroups(), 1);
    for (int iGBTG= 0; iGBTG < xfld_local.nGhostBoundaryTraceGroups(); iGBTG++)
      BOOST_REQUIRE(xfld_local.getGhostBoundaryTraceGroupBase(iGBTG).topoTypeID() == typeid(Tet));
    BOOST_CHECK_EQUAL(xfld_local.getGhostBoundaryTraceGroupBase(0).nElem(), 0);

    XField_LocalGlobal_Equiv(xfld_local);
  }
}

BOOST_AUTO_TEST_CASE(Split_2Ptope_X1_1Group_test)
{
  mpi::communicator world;
  mpi::communicator comm_local= world.split(world.rank());

  XField4D_2Ptope_X1_1Group xfld;

  // build cell-to-trace connectivity structure
  XField_CellToTrace<PhysD4, TopoD4> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld, {0});

  // loop over elements in original grid
  for (int elem= 0; elem < xfld.nElem(); elem++)
  {
    // extract the local mesh for a boundary triangle
    XField_LocalPatchConstructor<PhysD4, Pentatope> xfld_construct(comm_local,
                                                                   connectivity,
                                                                   0, elem,
                                                                   SpaceType::Continuous,
                                                                   &nodalview);

    for (int edge= 0; edge < Pentatope::NEdge; edge++)
    {
      XField_LocalPatch<PhysD4, Pentatope> xfld_local(xfld_construct,
                                                      ElementSplitType::Edge,
                                                      edge);

      BOOST_CHECK_EQUAL(&connectivity, &xfld_local.connectivity());

      BOOST_CHECK_EQUAL(xfld_local.nDOF(), 7);

      // area field variable

      BOOST_CHECK_EQUAL(xfld_local.nCellGroups(), 2);
      for (int iCG= 0; iCG < xfld_local.nCellGroups(); iCG++)
        BOOST_REQUIRE(xfld_local.getCellGroupBase(iCG).topoTypeID() == typeid(Pentatope));
      BOOST_CHECK_EQUAL(xfld_local.getCellGroupBase(0).nElem(), 2); // the newly split elements
      BOOST_CHECK(xfld_local.getCellGroupBase(1).nElem() == 1          // other elements on the local mesh
                      || xfld_local.getCellGroupBase(1).nElem() == 2); // --> either split is on interface or no

      // interior-edge field variable
      BOOST_REQUIRE(xfld_local.nInteriorTraceGroups() == 2        // split is either on the boundaries
                      || xfld_local.nInteriorTraceGroups() == 3); // or it is on the interface
      for (int iITG= 0; iITG < xfld_local.nInteriorTraceGroups(); iITG++)
        BOOST_REQUIRE(xfld_local.getInteriorTraceGroupBase(iITG).topoTypeID() == typeid(Tet));
      if (xfld_local.nInteriorTraceGroups() == 2)
      {
      BOOST_CHECK_EQUAL(xfld_local.getInteriorTraceGroupBase(0).nElem(), 1); // interface between newly split elements
      BOOST_CHECK_EQUAL(xfld_local.getInteriorTraceGroupBase(1).nElem(), 1); // other interfaces on local patch
      // BOOST_CHECK_EQUAL(xfld_local.getInteriorTraceGroupBase(2).nElem(), 0); // interface between opposing split elements
      }
      else
      {
        BOOST_CHECK_EQUAL(xfld_local.getInteriorTraceGroupBase(0).nElem(), 1); // interface between newly split elements
        BOOST_CHECK_EQUAL(xfld_local.getInteriorTraceGroupBase(1).nElem(), 2); // other interfaces on local patch
        BOOST_CHECK_EQUAL(xfld_local.getInteriorTraceGroupBase(2).nElem(), 1); // interface between opposing split elements
      }

      BOOST_CHECK_EQUAL(xfld_local.nBoundaryTraceGroups(), 8);
      for (int iBTG= 0; iBTG < xfld_local.nBoundaryTraceGroups(); iBTG++)
      {
        BOOST_REQUIRE(xfld_local.getBoundaryTraceGroupBase(iBTG).topoTypeID() == typeid(Tet));
        // there are two boundary planes that have two elements, others one
        BOOST_CHECK(xfld_local.getBoundaryTraceGroupBase(iBTG).nElem() == 1
                        || xfld_local.getBoundaryTraceGroupBase(iBTG).nElem() == 2);
      }

      BOOST_CHECK_EQUAL(xfld_local.nGhostBoundaryTraceGroups(), 1);
      for (int iGBTG= 0; iGBTG < xfld_local.nGhostBoundaryTraceGroups(); iGBTG++)
        BOOST_REQUIRE(xfld_local.getGhostBoundaryTraceGroupBase(iGBTG).topoTypeID() == typeid(Tet));

      XField_LocalGlobal_Equiv(xfld_local);
    }
  }
}

BOOST_AUTO_TEST_CASE(Split_6Ptope_X1_1Group_test)
{
  mpi::communicator world;
  mpi::communicator comm_local= world.split(world.rank());

  XField4D_6Ptope_X1_1Group_NegativeTraceOrientation xfld(world);

  // build cell-to-trace connectivity structure
  XField_CellToTrace<PhysD4, TopoD4> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld, {0});

  // make sure central element behaves as expected (a pure local patch)
  {
    // extract the local mesh for a boundary triangle
    XField_LocalPatchConstructor<PhysD4, Pentatope> xfld_construct(comm_local,
                                                                   connectivity,
                                                                   0, 0,
                                                                   SpaceType::Continuous,
                                                                   &nodalview);

    for (int edge= 0; edge < Pentatope::NEdge; edge++)
    {
      XField_LocalPatch<PhysD4, Pentatope> xfld_local(xfld_construct,
                                                      ElementSplitType::Edge,
                                                      edge);

      BOOST_CHECK_EQUAL(&connectivity, &xfld_local.connectivity());

      BOOST_CHECK_EQUAL(xfld_local.nDOF(), 11);

      // area field variable

      BOOST_CHECK_EQUAL(xfld_local.nCellGroups(), 2);
      for (int iCG= 0; iCG < xfld_local.nCellGroups(); iCG++)
        BOOST_REQUIRE(xfld_local.getCellGroupBase(iCG).topoTypeID() == typeid(Pentatope));
      BOOST_CHECK_EQUAL(xfld_local.getCellGroupBase(0).nElem(), 2); // the unsplit elements
      BOOST_CHECK_EQUAL(xfld_local.getCellGroupBase(1).nElem(), 8); // other elements on the local mesh (3 split neighbors)

      // interior-edge field variable
      BOOST_CHECK_EQUAL(xfld_local.nInteriorTraceGroups(), 3);
      for (int iITG= 0; iITG < xfld_local.nInteriorTraceGroups(); iITG++)
        BOOST_REQUIRE(xfld_local.getInteriorTraceGroupBase(iITG).topoTypeID() == typeid(Tet));
      BOOST_CHECK_EQUAL(xfld_local.getInteriorTraceGroupBase(0).nElem(), 1); // interface between newly split elements
      BOOST_CHECK_EQUAL(xfld_local.getInteriorTraceGroupBase(1).nElem(), 8); // other interfaces on local patch
      BOOST_CHECK_EQUAL(xfld_local.getInteriorTraceGroupBase(2).nElem(), 3); // interface between opposing split elements

      BOOST_CHECK_EQUAL(xfld_local.nBoundaryTraceGroups(), 1);
      for (int iBTG= 0; iBTG < xfld_local.nBoundaryTraceGroups(); iBTG++)
        BOOST_REQUIRE(xfld_local.getBoundaryTraceGroupBase(iBTG).topoTypeID() == typeid(Tet));
      BOOST_CHECK_EQUAL(xfld_local.getBoundaryTraceGroupBase(0).nElem(), 26); // all the outward facing tets

      BOOST_CHECK_EQUAL(xfld_local.nGhostBoundaryTraceGroups(), 1);
      for (int iGBTG= 0; iGBTG < xfld_local.nGhostBoundaryTraceGroups(); iGBTG++)
        BOOST_REQUIRE(xfld_local.getGhostBoundaryTraceGroupBase(iGBTG).topoTypeID() == typeid(Tet));

      XField_LocalGlobal_Equiv(xfld_local);
    }
  }

  // loop over the other elements in grid
  for (int elem= 1; elem < xfld.nElem(); elem++)
  {
    // extract the local mesh for a boundary triangle
    XField_LocalPatchConstructor<PhysD4, Pentatope> xfld_construct(comm_local,
                                                                   connectivity,
                                                                   0, elem,
                                                                   SpaceType::Continuous,
                                                                   &nodalview);

    for (int edge= 0; edge < Pentatope::NEdge; edge++)
    {
      XField_LocalPatch<PhysD4, Pentatope> xfld_local(xfld_construct,
                                                      ElementSplitType::Edge,
                                                      edge);

      BOOST_CHECK_EQUAL(&connectivity, &xfld_local.connectivity());

      BOOST_CHECK_EQUAL(xfld_local.nDOF(), 11);

      // area field variable

      BOOST_CHECK_EQUAL(xfld_local.nCellGroups(), 2);
      for (int iCG= 0; iCG < xfld_local.nCellGroups(); iCG++)
        BOOST_REQUIRE(xfld_local.getCellGroupBase(iCG).topoTypeID() == typeid(Pentatope));
      BOOST_CHECK_EQUAL(xfld_local.getCellGroupBase(0).nElem(), 2); // the newly split elements
      BOOST_CHECK(xfld_local.getCellGroupBase(1).nElem() >= 5); // other elements on the local mesh
      BOOST_CHECK(xfld_local.getCellGroupBase(1).nElem() <= 8); // between 0 and 3 add'l elements could be split

      // interior-edge field variable
      BOOST_REQUIRE_EQUAL(xfld_local.nInteriorTraceGroups(), 3);
      for (int iITG= 0; iITG < xfld_local.nInteriorTraceGroups(); iITG++)
        BOOST_REQUIRE(xfld_local.getInteriorTraceGroupBase(iITG).topoTypeID() == typeid(Tet));
      BOOST_CHECK_EQUAL(xfld_local.getInteriorTraceGroupBase(0).nElem(), 1); // interface between newly split elements
      BOOST_CHECK(xfld_local.getInteriorTraceGroupBase(1).nElem() == 1          // other interfaces on local patch
                      || xfld_local.getInteriorTraceGroupBase(1).nElem() == 2   // either 3, 1, or 0 incidental splits
                      || xfld_local.getInteriorTraceGroupBase(1).nElem() == 9); // therefore 1 2 or 9 unperturbed boundaries
      BOOST_CHECK(xfld_local.getInteriorTraceGroupBase(2).nElem() == 2          // likewise there can either be
                      || xfld_local.getInteriorTraceGroupBase(2).nElem() == 4   // 2 4 or 9 incidentatal split boundaries
                      || xfld_local.getInteriorTraceGroupBase(2).nElem() == 9); // interfaces between opposing split elements

      BOOST_CHECK_EQUAL(xfld_local.nBoundaryTraceGroups(), 2);
      for (int iBTG= 0; iBTG < xfld_local.nBoundaryTraceGroups(); iBTG++)
        BOOST_REQUIRE(xfld_local.getBoundaryTraceGroupBase(iBTG).topoTypeID() == typeid(Tet));
      BOOST_CHECK(xfld_local.getBoundaryTraceGroupBase(0).nElem() == 6          // on a split element, four faces on orig. ptope
                      || xfld_local.getBoundaryTraceGroupBase(0).nElem() == 7); // two or three of which could possibly have split
      BOOST_CHECK(xfld_local.getBoundaryTraceGroupBase(1).nElem() == 16          // on neighbors there are 16 faces on orig. ptope
                      || xfld_local.getBoundaryTraceGroupBase(1).nElem() == 20); // either none or four of those end up split (2 boundary neighbors)

      BOOST_CHECK_EQUAL(xfld_local.nGhostBoundaryTraceGroups(), 1);
      for (int iGBTG= 0; iGBTG < xfld_local.nGhostBoundaryTraceGroups(); iGBTG++)
        BOOST_REQUIRE(xfld_local.getGhostBoundaryTraceGroupBase(iGBTG).topoTypeID() == typeid(Tet));

      XField_LocalGlobal_Equiv(xfld_local);
    }
  }
}

BOOST_AUTO_TEST_CASE(Split_24Ptope_X1_1Group_test)
{
  mpi::communicator world;
  mpi::communicator comm_local= world.split(world.rank());

  XField4D_24Ptope_X1_1Group xfld;

  // build cell-to-trace connectivity structure
  XField_CellToTrace<PhysD4, TopoD4> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld, {0});

  // loop over elements in original grid
  for (int elem= 0; elem < xfld.nElem(); elem++)
  {
    // extract the local mesh for a boundary triangle
    XField_LocalPatchConstructor<PhysD4, Pentatope> xfld_construct(comm_local,
                                                                   connectivity,
                                                                   0, elem,
                                                                   SpaceType::Continuous,
                                                                   &nodalview);

    for (int edge= 0; edge < Pentatope::NEdge; edge++)
    {
      XField_LocalPatch<PhysD4, Pentatope> xfld_local(xfld_construct,
                                                      ElementSplitType::Edge,
                                                      edge);

      BOOST_CHECK_EQUAL(&connectivity, &xfld_local.connectivity());

      BOOST_CHECK_EQUAL(xfld_local.nDOF(), 17);

      // area field variable

      BOOST_CHECK_EQUAL(xfld_local.nCellGroups(), 2);
      for (int iCG= 0; iCG < xfld_local.nCellGroups(); iCG++)
        BOOST_REQUIRE(xfld_local.getCellGroupBase(iCG).topoTypeID() == typeid(Pentatope));
      BOOST_CHECK_EQUAL(xfld_local.getCellGroupBase(0).nElem(), 2); // the unsplit elements
      BOOST_CHECK(xfld_local.getCellGroupBase(1).nElem() > 0); // other elements on the local mesh

      // interior-edge field variable
      BOOST_CHECK_EQUAL(xfld_local.nInteriorTraceGroups(), 3);
      // BOOST_REQUIRE(xfld_local.nInteriorTraceGroups() == 2        // split is either on the boundaries
      //                 || xfld_local.nInteriorTraceGroups() == 3); // or it is on the interface
      for (int iITG= 0; iITG < xfld_local.nInteriorTraceGroups(); iITG++)
        BOOST_REQUIRE(xfld_local.getInteriorTraceGroupBase(iITG).topoTypeID() == typeid(Tet));
      BOOST_CHECK_EQUAL(xfld_local.getInteriorTraceGroupBase(0).nElem(), 1); // interface between newly split elements
      BOOST_CHECK(xfld_local.getInteriorTraceGroupBase(1).nElem() >= 0); // other interfaces on the local mesh
      BOOST_CHECK(xfld_local.getInteriorTraceGroupBase(2).nElem() >= 0); // interface between opposing split elements

      BOOST_CHECK_EQUAL(xfld_local.nBoundaryTraceGroups(), 2);
      for (int iBTG= 0; iBTG < xfld_local.nBoundaryTraceGroups(); iBTG++)
      {
        BOOST_REQUIRE(xfld_local.getBoundaryTraceGroupBase(iBTG).topoTypeID() == typeid(Tet));
        // there are two boundary planes that have two elements, others one
        BOOST_CHECK(xfld_local.getBoundaryTraceGroupBase(iBTG).nElem() >= 0); // we can get two... I'm not sure why
      }

      BOOST_CHECK_EQUAL(xfld_local.nGhostBoundaryTraceGroups(), 1);
      for (int iGBTG= 0; iGBTG < xfld_local.nGhostBoundaryTraceGroups(); iGBTG++)
      BOOST_REQUIRE(xfld_local.getGhostBoundaryTraceGroupBase(iGBTG).topoTypeID() == typeid(Tet));

      XField_LocalGlobal_Equiv(xfld_local);
    }
  }
}

// ########################################################################## //
BOOST_AUTO_TEST_SUITE_END()
