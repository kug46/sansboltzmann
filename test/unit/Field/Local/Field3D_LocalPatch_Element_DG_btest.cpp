// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Field3D_EdgeLocal_CG_btest
//

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
// #include "BasisFunction/BasisFunctionArea_Triangle.h"

#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/FieldLiftVolume_DG_Cell.h"

#include "Field_LocalPatch_toolkit_btest.h"

#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"
#include "unit/UnitGrids/XField3D_5Tet_X1_1Group_AllOrientations.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

//Explicitly instantiate classes to get proper coverage information
namespace SANS
{
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( Field3D_LocalPatch_Element_DG_test_suite )

//============================================================================//
// Hierarchical
//============================================================================//

// Simpler Orientation tests
#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Hierarchical_5Tet_P1 )
{
  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = false;

  // loop over the possible orientations
  for (int orient = -3; orient <= 3; orient++)
  {
    if (orient == 0) continue; // no 0-th orientation

    XField3D_5Tet_X1_1Group_AllOrientations xfld(orient);
    localElementExtractAndTransfer<PhysD3,TopoD3>( xfld, basis_category, order, small_tol, close_tol, split_test );
  }

  split_test = true;

  // loop over the possible orientations
  for (int orient = -3; orient <= 3; orient++)
  {
    if (orient == 0) continue; // no 0-th orientation

    XField3D_5Tet_X1_1Group_AllOrientations xfld(orient);
    localElementExtractAndTransfer<PhysD3,TopoD3>( xfld, basis_category, order, small_tol, close_tol, split_test );
  }
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Hierarchical_5Tet_P2 )
{
  int order = 2;
  const Real small_tol = 1e-10, close_tol = 1e-9;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = false;

  // loop over the possible orientations
  for (int orient = -3; orient <= 3; orient++)
  {
    if (orient == 0) continue; // no 0-th orientation

    XField3D_5Tet_X1_1Group_AllOrientations xfld(orient);
    localElementExtractAndTransfer<PhysD3,TopoD3>( xfld, basis_category, order, small_tol, close_tol, split_test );
  }

  split_test = true;

  // loop over the possible orientations
  for (int orient = -3; orient <= 3; orient++)
  {
    if (orient == 0) continue; // no 0-th orientation

    XField3D_5Tet_X1_1Group_AllOrientations xfld(orient);
    localElementExtractAndTransfer<PhysD3,TopoD3>( xfld, basis_category, order, small_tol, close_tol, split_test );
  }
}
#endif

//============================================================================//
// Lagrange
//============================================================================//

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Lagrange_5Tet_P1 )
{
  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = false;

  // loop over the possible orientations
  for (int orient = -3; orient <= 3; orient++)
  {
    if (orient == 0) continue; // no 0-th orientation

    XField3D_5Tet_X1_1Group_AllOrientations xfld(orient);
    localElementExtractAndTransfer<PhysD3,TopoD3>( xfld, basis_category, order, small_tol, close_tol, split_test );
  }

  split_test = true;

  // loop over the possible orientations
  for (int orient = -3; orient <= 3; orient++)
  {
    if (orient == 0) continue; // no 0-th orientation

    XField3D_5Tet_X1_1Group_AllOrientations xfld(orient);
    localElementExtractAndTransfer<PhysD3,TopoD3>( xfld, basis_category, order, small_tol, close_tol, split_test );
  }
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Lagrange_5Tet_P2 )
{
  int order = 2;
  const Real small_tol = 1e-10, close_tol = 1e-9;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = false;

  // loop over the possible orientations
  for (int orient = -3; orient <= 3; orient++)
  {
    if (orient == 0) continue; // no 0-th orientation

    XField3D_5Tet_X1_1Group_AllOrientations xfld(orient);
    localElementExtractAndTransfer<PhysD3,TopoD3>( xfld, basis_category, order, small_tol, close_tol, split_test );
  }

  split_test = true;

  // loop over the possible orientations
  for (int orient = -3; orient <= 3; orient++)
  {
    if (orient == 0) continue; // no 0-th orientation

    XField3D_5Tet_X1_1Group_AllOrientations xfld(orient);
    localElementExtractAndTransfer<PhysD3,TopoD3>( xfld, basis_category, order, small_tol, close_tol, split_test );
  }
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Lagrange_5Tet_P3 )
{
  int order = 3;
  Real small_tol = 1e-11, close_tol = 1e-11;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = false;

  // loop over the possible orientations
  for (int orient = -3; orient <= 3; orient++)
  {
    if (orient == 0) continue; // no 0-th orientation

    XField3D_5Tet_X1_1Group_AllOrientations xfld(orient);
    localElementExtractAndTransfer<PhysD3,TopoD3>( xfld, basis_category, order, small_tol, close_tol, split_test );
  }

  split_test = true;
  small_tol = 5e-9; close_tol = 5e-9;

  // loop over the possible orientations
  for (int orient = -3; orient <= 3; orient++)
  {
    if (orient == 0) continue; // no 0-th orientation

    XField3D_5Tet_X1_1Group_AllOrientations xfld(orient);
    localElementExtractAndTransfer<PhysD3,TopoD3>( xfld, basis_category, order, small_tol, close_tol, split_test );
  }
}
#endif

//============================================================================//
// Larger Box grids
//============================================================================//

//============================================================================//
// Hierarchical
//============================================================================//

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unplit_Hierarchical_1Box_Tet_P1 )
{
  mpi::communicator world;

  int size = 2*2*2*world.size();
  int ii = pow(size, 1./3.), jj = ii, kk = ii;

  XField3D_Box_Tet_X1 xfld( world, ii, jj, kk );

  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = false;
  localElementExtractAndTransfer<PhysD3,TopoD3>( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Hierarchical_1Box_Tet_P1 )
{
  mpi::communicator world;

  int size = 2*2*2*world.size();
  int ii = pow(size, 1./3.), jj = ii, kk = ii;

  XField3D_Box_Tet_X1 xfld( world, ii, jj, kk );

  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = true;
  localElementExtractAndTransfer<PhysD3,TopoD3>( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_Hierarchical_1Box_Tet_P2 )
{
  mpi::communicator world;

  int size = 2*2*2*world.size();
  int ii = pow(size, 1./3.), jj = ii, kk = ii;

  XField3D_Box_Tet_X1 xfld( world, ii, jj, kk );

  int order = 2;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = false;
  localElementExtractAndTransfer<PhysD3,TopoD3>( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Hierarchical_1Box_Tet_P2 )
{
  mpi::communicator world;

  int size = 2*2*2*world.size();
  int ii = pow(size, 1./3.), jj = ii, kk = ii;

  XField3D_Box_Tet_X1 xfld( world, ii, jj, kk );

  int order = 2;
  const Real small_tol = 1e-10, close_tol = 1e-9;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = true;
  localElementExtractAndTransfer<PhysD3,TopoD3>( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

//============================================================================//
// Lagrange
//============================================================================//

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_Lagrange_1Box_Tet_P1 )
{
  mpi::communicator world;

  int size = 2*2*2*world.size();
  int ii = pow(size, 1./3.), jj = ii, kk = ii;

  XField3D_Box_Tet_X1 xfld( world, ii, jj, kk );

  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = false;
  localElementExtractAndTransfer<PhysD3,TopoD3>( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Lagrange_1Box_Tet_P1 )
{
  mpi::communicator world;

  int size = 2*2*2*world.size();
  int ii = pow(size, 1./3.), jj = ii, kk = ii;

  XField3D_Box_Tet_X1 xfld( world, ii, jj, kk );

  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-9;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = true;
  localElementExtractAndTransfer<PhysD3,TopoD3>( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_Lagrange_1Box_Tet_P2 )
{
  mpi::communicator world;

  int size = 2*2*2*world.size();
  int ii = pow(size, 1./3.), jj = ii, kk = ii;

  XField3D_Box_Tet_X1 xfld( world, ii, jj, kk );

  int order = 2;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = false;
  localElementExtractAndTransfer<PhysD3,TopoD3>( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Lagrange_1Box_Tet_P2 )
{
  mpi::communicator world;

  int size = 2*2*2*world.size();
  int ii = pow(size, 1./3.), jj = ii, kk = ii;

  XField3D_Box_Tet_X1 xfld( world, ii, jj, kk );

  int order = 2;
  const Real small_tol = 1e-10, close_tol = 5e-8;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = true;
  localElementExtractAndTransfer<PhysD3,TopoD3>( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_Lagrange_1Box_Tet_P3 )
{
  mpi::communicator world;

  int size = 2*2*2*world.size();
  int ii = pow(size, 1./3.), jj = ii, kk = ii;

  XField3D_Box_Tet_X1 xfld( world, ii, jj, kk );

  int order = 3;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = false;
  localElementExtractAndTransfer<PhysD3,TopoD3>( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Lagrange_1Box_Tet_P3 )
{
  mpi::communicator world;

  int size = 2*2*2*world.size();
  int ii = pow(size, 1./3.), jj = ii, kk = ii;

  XField3D_Box_Tet_X1 xfld( world, ii, jj, kk );

  int order = 3;
  const Real small_tol = 1e-10, close_tol = 1e-7;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = true;
  localElementExtractAndTransfer<PhysD3,TopoD3>( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
