// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// XField3D_LocalPatch_btest
// testing of XField3D_Local
//
// Note: unit grids tested in "unit/UnitGrids/UnitGrids_btest.cpp"

#include <ostream>
#include <utility> // std::pair
#include <random>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "Topology/ElementTopology.h"

#include "BasisFunction/ElementEdges.h"

#include "Field/Field_NodalView.h"
#include "Field/XField_CellToTrace.h"
#include "Field/Local/XField_LocalPatch.h"

#include "unit/UnitGrids/XField3D_1Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_5Tet_X1_1Group_AllOrientations.h"
#include "unit/UnitGrids/XField_KuhnFreudenthal.h"

#include "XField_LocalGlobal_Equiv_btest.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( XField3D_Local_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_HighOrderLocalPatch_1Tet_1Group_InteriorCell_Target_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-7;

  XField3D_1Tet_X1_1Group xfld0;

  int order = 3;
  XField<PhysD3,TopoD3> xfld(xfld0,order,BasisFunctionCategory_Lagrange);

  // initialize a uniform distribution
  std::mt19937_64 rng;
  std::uniform_real_distribution<double> unif(-0.01, 0.01);

  // Perturb the DOFs so high order elements are curved
  for (int n = 0; n < xfld.nDOF(); n++)
  {
    xfld.DOF(n)[0] += unif(rng);
    xfld.DOF(n)[1] += unif(rng);
    xfld.DOF(n)[2] += unif(rng);
  }

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  int group = 0, elem = 0;

  //Extract the local mesh for the central tet
  XField_LocalPatchConstructor<PhysD3,Tet> xfld_local(comm_local,connectivity,group,elem);

  // create a split patch and split an edge
  int edge = 0;
  XField_LocalPatch<PhysD3,Tet> xfld_split( xfld_local, ElementSplitType::Edge, edge );

  BOOST_REQUIRE_EQUAL( xfld_split.getNewNodeDOFs().size(), 1 );
  BOOST_CHECK_EQUAL( xfld_split.getNewNodeDOFs()[0], 29 );

  BOOST_REQUIRE_EQUAL( xfld_split.getNewLinearNodeDOFs().size(), 1 );
  BOOST_CHECK_EQUAL( xfld_split.getNewLinearNodeDOFs()[0], 4 );

  // check that all elements match the global elements
  XField_LocalGlobal_Equiv(xfld_split, small_tol, close_tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_HighOrderLocalPatch_5Tet_1Group_InteriorCell_Target_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  const Real small_tol = 1e-12;
  const Real close_tol = 5e-8;

  XField3D_5Tet_X1_1Group_AllOrientations xfld0(-1);

  int order = 4;
  XField<PhysD3,TopoD3> xfld(xfld0,order,BasisFunctionCategory_Lagrange);

  // initialize a uniform distribution
  std::mt19937_64 rng;
  std::uniform_real_distribution<double> unif(-0.01, 0.01);

  // Perturb the DOFs so high order elements are curved
  for (int n = 0; n < xfld.nDOF(); n++)
  {
    xfld.DOF(n)[0] += unif(rng);
    xfld.DOF(n)[1] += unif(rng);
    xfld.DOF(n)[2] += unif(rng);
  }

  // build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  // build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  int group = 0, elem = 0;

  //Extract the local mesh for the central tet
  XField_LocalPatchConstructor<PhysD3,Tet> xfld_local(comm_local,connectivity,group,elem);

  // create a split patch and split an edge
  for (int edge = 0; edge < Tet::NEdge; edge++)
  {
    XField_LocalPatch<PhysD3,Tet> xfld_split( xfld_local, ElementSplitType::Edge, edge );

    BOOST_REQUIRE_EQUAL( xfld_split.getNewNodeDOFs().size(), 1 );
    BOOST_CHECK_EQUAL( xfld_split.getNewNodeDOFs()[0], 154 );

    BOOST_REQUIRE_EQUAL( xfld_split.getNewLinearNodeDOFs().size(), 1 );
    BOOST_CHECK_EQUAL( xfld_split.getNewLinearNodeDOFs()[0], 8 );

    // check that all elements match the global elements
    XField_LocalGlobal_Equiv(xfld_split, small_tol, close_tol);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_HighOrderLocalPatchAddNeighbours_CKF_InteriorCell_Target_test )
{
#ifdef SANS_AVRO

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  const Real small_tol = 1e-12;
  const Real close_tol = 5e-7;

  XField_KuhnFreudenthal<PhysD3,TopoD3> xfld0( comm_local, {3,3,3} );

  int order = 3;
  XField<PhysD3,TopoD3> xfld(xfld0,order,BasisFunctionCategory_Lagrange);

  // initialize a uniform distribution
  std::mt19937_64 rng;
  std::uniform_real_distribution<double> unif(-0.005, 0.005);

  // Perturb the DOFs so high order elements are curved
  for (int n = 0; n < xfld.nDOF(); n++)
  {
    xfld.DOF(n)[0] += unif(rng);
    xfld.DOF(n)[1] += unif(rng);
    xfld.DOF(n)[2] += unif(rng);
  }

  // build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  // build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  time_t t0 = clock();
  int nsplit = 0;
  for (int group = 0; group < xfld.nCellGroups(); group++)
  {
    for (int elem = 0;elem < xfld.getCellGroupBase(group).nElem(); elem++)
    {
      // extract the local mesh for the central tet
      XField_LocalPatchConstructor<PhysD3,Tet> xfld_local(comm_local,connectivity,group,elem);

      // split every edge
      for (int edge = 0; edge < Tet::NEdge; edge++)
      {
        XField_LocalPatch<PhysD3,Tet> xfld_split(xfld_local, ElementSplitType::Edge, edge);
        nsplit++;

        // check that all elements match the global elements
        XField_LocalGlobal_Equiv(xfld_split, small_tol, close_tol);
      }
    }
  }

  printf("performed nsplit = %d in %g seconds\n",nsplit,double(clock()-t0)/CLOCKS_PER_SEC);
#endif

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
