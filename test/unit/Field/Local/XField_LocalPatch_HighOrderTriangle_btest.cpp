// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// XField2D_LocalPatch_btest
// testing of XField4D_Local
//
// Note: unit grids tested in "unit/UnitGrids/UnitGrids_btest.cpp"

#include <ostream>
#include <utility> // std::pair
#include <random>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "Topology/ElementTopology.h"

#include "BasisFunction/ElementEdges.h"

#include "Field/Field_NodalView.h"
#include "Field/XField_CellToTrace.h"
#include "Field/Local/XField_LocalPatch.h"
#include "Field/Partition/XField_Lagrange.h"

#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"
#include "unit/UnitGrids/XField_KuhnFreudenthal.h"

#include "XField_LocalGlobal_Equiv_btest.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( XField_LocalPatch_HighOrderTriangle_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_Lagrange_4Triangle_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-10;

  XField2D_4Triangle_X1_1Group xfld0;

  // elevate the order
  int order = 3;
  XField<PhysD2,TopoD2> xfld(xfld0, order, BasisFunctionCategory_Lagrange);

  // initialize a uniform distribution between -0.01 and 0.01
  std::mt19937_64 rng;
  std::uniform_real_distribution<double> unif(-0.01, 0.01);

  // Perturb the DOFs so high order elements are curved
  for (int n = 0; n < xfld.nDOF(); n++)
  {
    xfld.DOF(n)[0] += unif(rng);
    xfld.DOF(n)[1] += unif(rng);
  }

  // build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  int group = 0, elem = 0;

  //Extract the local mesh for the central tet
  XField_LocalPatchConstructor<PhysD2,Triangle> xfld_local(comm_local, connectivity, group, elem, SpaceType::Discontinuous);

  // create an unsplit patch
  XField_LocalPatch<PhysD2,Triangle> xfld_unsplit( xfld_local );

  //Check the element count
  BOOST_CHECK_EQUAL( xfld_unsplit.nElem(), 4 );
  BOOST_CHECK_EQUAL( xfld_unsplit.nCellGroups(), 2 );
  BOOST_CHECK_EQUAL( xfld_unsplit.nInteriorTraceGroups(), 1 );
  BOOST_CHECK_EQUAL( xfld_unsplit.nBoundaryTraceGroups(), 0 );
  BOOST_CHECK_EQUAL( xfld_unsplit.nGhostBoundaryTraceGroups(), 1 );

  for (int group = 0; group < xfld_unsplit.nCellGroups(); group++)
  {
    typedef typename XField<PhysD2, TopoD2>::template FieldCellGroupType<Triangle> XFieldCellGroupType;
    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

    XFieldCellGroupType& cellGroup = xfld_unsplit.template getCellGroup<Triangle>(group);
    ElementXFieldClass xfldElem( cellGroup.basis() );

    for (int elem = 0; elem < cellGroup.nElem(); elem++)
    {
      // retrieve the split info
      ElementSplitInfo info = xfld_unsplit.getCellSplitInfo({group,elem});
      BOOST_CHECK_EQUAL( (int)ElementSplitFlag::Unsplit, (int)info.split_flag );
    }
  }

  for (int group = 0; group < xfld_unsplit.nInteriorTraceGroups(); group++)
  {
    typedef typename XField<PhysD2, TopoD2>::template FieldTraceGroupType<Line> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldClass;

    XFieldTraceGroupType& traceGroup = xfld_unsplit.template getInteriorTraceGroup<Line>(group);
    ElementXFieldClass xfldElem( traceGroup.basis() );

    BOOST_CHECK_EQUAL( traceGroup.nElem(), 3 );

    for (int elem = 0; elem < traceGroup.nElem(); elem++)
    {
      // retrieve the split info
      ElementSplitInfo info = xfld_unsplit.getInteriorTraceSplitInfo({group,elem});
      BOOST_CHECK_EQUAL( (int)ElementSplitFlag::Unsplit, (int)info.split_flag );
    }
  }

  for (int group = 0; group < xfld_unsplit.nGhostBoundaryTraceGroups(); group++)
  {
    typedef typename XField<PhysD2, TopoD2>::template FieldTraceGroupType<Line> XFieldTraceGroupType;

    const XFieldTraceGroupType& traceGroup = xfld_unsplit.template getGhostBoundaryTraceGroup<Line>(group);

    BOOST_CHECK_EQUAL( traceGroup.nElem(), 6 );
  }

  BOOST_REQUIRE_EQUAL( xfld_local.getNewNodeDOFs().size(), 0 );
  BOOST_REQUIRE_EQUAL( xfld_local.getNewLinearNodeDOFs().size(), 0 );

  // check that all elements match the global elements
  XField_LocalGlobal_Equiv(xfld_unsplit, small_tol, close_tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_Hierarchical_4Triangle_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  const Real small_tol = 1e-12;
  const Real close_tol = 5e-7;

  XField2D_4Triangle_X1_1Group xfld0;

  // elevate the order
  int order = 3;
  XField<PhysD2,TopoD2> xfld(xfld0, order, BasisFunctionCategory_Hierarchical);

  // initialize a uniform distribution between -0.01 and 0.01
  std::mt19937_64 rng;
  std::uniform_real_distribution<double> unif(-0.01, 0.01);

  // Perturb the DOFs so high order elements are curved
  for (int n = 0; n < xfld.nDOF(); n++)
  {
    xfld.DOF(n)[0] += unif(rng);
    xfld.DOF(n)[1] += unif(rng);
  }

  // build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  int group = 0, elem = 0;

  //Extract the local mesh for the central triangle
  XField_LocalPatchConstructor<PhysD2,Triangle> xfld_local(comm_local, connectivity, group, elem, SpaceType::Discontinuous);

  // create an unsplit patch
  XField_LocalPatch<PhysD2,Triangle> xfld_unsplit( xfld_local );

  //Check the element count
  BOOST_CHECK_EQUAL( xfld_unsplit.nElem(), 4 );
  BOOST_CHECK_EQUAL( xfld_unsplit.nCellGroups(), 2 );
  BOOST_CHECK_EQUAL( xfld_unsplit.nInteriorTraceGroups(), 1 );
  BOOST_CHECK_EQUAL( xfld_unsplit.nBoundaryTraceGroups(), 0 );
  BOOST_CHECK_EQUAL( xfld_unsplit.nGhostBoundaryTraceGroups(), 1 );

  for (int group = 0; group < xfld_unsplit.nCellGroups(); group++)
  {
    typedef typename XField<PhysD2, TopoD2>::template FieldCellGroupType<Triangle> XFieldCellGroupType;
    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

    XFieldCellGroupType& cellGroup = xfld_unsplit.template getCellGroup<Triangle>(group);
    ElementXFieldClass xfldElem( cellGroup.basis() );

    for (int elem = 0; elem < cellGroup.nElem(); elem++)
    {
      // retrieve the split info
      ElementSplitInfo info = xfld_unsplit.getCellSplitInfo({group,elem});
      BOOST_CHECK_EQUAL( (int)ElementSplitFlag::Unsplit, (int)info.split_flag );
    }
  }

  for (int group = 0; group < xfld_unsplit.nInteriorTraceGroups(); group++)
  {
    typedef typename XField<PhysD2, TopoD2>::template FieldTraceGroupType<Line> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldClass;

    XFieldTraceGroupType& traceGroup = xfld_unsplit.template getInteriorTraceGroup<Line>(group);
    ElementXFieldClass xfldElem( traceGroup.basis() );

    BOOST_CHECK_EQUAL( traceGroup.nElem(), 3 );

    for (int elem = 0; elem < traceGroup.nElem(); elem++)
    {
      // retrieve the split info
      ElementSplitInfo info = xfld_unsplit.getInteriorTraceSplitInfo({group,elem});
      BOOST_CHECK_EQUAL( (int)ElementSplitFlag::Unsplit, (int)info.split_flag );
    }
  }

  for (int group = 0; group < xfld_unsplit.nGhostBoundaryTraceGroups(); group++)
  {
    typedef typename XField<PhysD2, TopoD2>::template FieldTraceGroupType<Line> XFieldTraceGroupType;

    const XFieldTraceGroupType& traceGroup = xfld_unsplit.template getGhostBoundaryTraceGroup<Line>(group);

    BOOST_CHECK_EQUAL( traceGroup.nElem(), 6 );
  }

  BOOST_REQUIRE_EQUAL( xfld_local.getNewNodeDOFs().size(), 0 );
  BOOST_REQUIRE_EQUAL( xfld_local.getNewLinearNodeDOFs().size(), 0 );

  // check that all elements match the global elements
  XField_LocalGlobal_Equiv(xfld_unsplit, small_tol, close_tol);
}

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_1Triangle_InteriorCell_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-10;

  // initial q1 mesh
  XField2D_1Triangle_X1_1Group xfld0;

  // elevate the order
  int order = 3;
  XField<PhysD2,TopoD2> xfld(xfld0,order,BasisFunctionCategory_Lagrange);

  // initialize a uniform distribution between -0.01 and 0.01
  std::mt19937_64 rng;
  std::uniform_real_distribution<double> unif(-0.01, 0.01);

  // Perturb the DOFs so high order elements are curved
  for (int n = 0; n < xfld.nDOF(); n++)
  {
    xfld.DOF(n)[0] += unif(rng);
    xfld.DOF(n)[1] += unif(rng);
  }

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  int group = 0, elem = 0;

  // extract the local mesh for the central triangle
  XField_LocalPatchConstructor<PhysD2,Triangle> xfld_local(comm_local,connectivity,group,elem);

  // create a split patch and split an edge
  int edge = 2;
  XField_LocalPatch<PhysD2,Triangle> xfld_split( xfld_local, ElementSplitType::Edge, edge );

  BOOST_REQUIRE_EQUAL( xfld_split.getNewNodeDOFs().size(), 1 );
  BOOST_CHECK_EQUAL( xfld_split.getNewNodeDOFs()[0], 15 );

  BOOST_REQUIRE_EQUAL( xfld_split.getNewLinearNodeDOFs().size(), 1 );
  BOOST_CHECK_EQUAL( xfld_split.getNewLinearNodeDOFs()[0], 3 );

  // check that all elements match the global elements
  XField_LocalGlobal_Equiv(xfld_split, small_tol, close_tol);
}
#endif

//----------------------------------------------------------------------------//
/*  The linear portion (0-1-2) of the element is invalid
 *
 *           __-3-__
 *         _/       \_
 *       _/           \__
 *     _/         _-5-__ \_
 *    /         _/      \  \
 *   / _--4----0         ---1
 *  2-/
 */

class XField_InvalidLinear : public XField<PhysD2,TopoD2>
{
public:
  XField_InvalidLinear( mpi::communicator& comm ) : XField<PhysD2,TopoD2>(comm)
  {
    build();
  }

  void build()
  {
    XField_Lagrange<PhysD2> xfld(*this->comm());

    xfld.sizeDOF( 6 );
    xfld.addDOF( { 0.0,  0.00} );
    xfld.addDOF( { 1.0,  0.00} );
    xfld.addDOF( {-1.0, -0.10} );
    xfld.addDOF( { 0.0,  1.00} );
    xfld.addDOF( {-0.5,  0.00} );
    xfld.addDOF( { 0.5,  0.25} );

    xfld.sizeCells(1);
    xfld.addCell(0, eTriangle, 2, {0,1,2,3,4,5});

    xfld.sizeBoundaryTrace(3);
    xfld.addBoundaryTrace(0, eLine, {0,1});
    xfld.addBoundaryTrace(1, eLine, {1,2});
    xfld.addBoundaryTrace(2, eLine, {2,0});

    this->buildFrom(xfld);
  }
};

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_1Triangle_InvalidLinear_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  // initial q2 mesh with an invalid linear portion
  XField_InvalidLinear xfld(comm_local);

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  int group = 0, elem = 0;

  // extract the local mesh for the central triangle
  XField_LocalPatchConstructor<PhysD2,Triangle> xfld_local(comm_local,connectivity,group,elem);

  // split every edge
  for (int edge = 0; edge < Triangle::NEdge; edge++)
  {
    XField_LocalPatch<PhysD2,Triangle> xfld_split( xfld_local, ElementSplitType::Edge, edge );

    BOOST_REQUIRE_EQUAL( xfld_split.getNewNodeDOFs().size(), 1 );
    BOOST_CHECK_EQUAL( xfld_split.getNewNodeDOFs()[0], 8 );

    BOOST_REQUIRE_EQUAL( xfld_split.getNewLinearNodeDOFs().size(), 1 );
    BOOST_CHECK_EQUAL( xfld_split.getNewLinearNodeDOFs()[0], 3 );

    // check that all elements match the global elements
    XField_LocalGlobal_Equiv(xfld_split);
  }
}


#if 0
// i (philip) was using this when i was debugging the high-order projection
class TestField : public XField<PhysD2,TopoD2>
{
public:
  TestField( mpi::communicator& comm ) : XField<PhysD2,TopoD2>(comm)
  {
    build();
  }

  void build()
  {
    XField_Lagrange<PhysD2> xfld(*this->comm());

    xfld.sizeDOF( 6 );
    xfld.addDOF( {0,0} );
    xfld.addDOF( {1,0} );
    xfld.addDOF( {0,1} );
    xfld.addDOF( {-1,1} );
    xfld.addDOF( {1,-1} );
    xfld.addDOF( {1,1} );

    xfld.sizeCells(4);
    xfld.addCell(0,eTriangle,1,{0,1,2});
    xfld.addCell(1,eTriangle,1,{2,3,0});
    xfld.addCell(1,eTriangle,1,{1,0,4});
    xfld.addCell(1,eTriangle,1,{5,2,1});

    xfld.sizeBoundaryTrace(6);
    xfld.addBoundaryTrace(0,eLine,{3,0});
    xfld.addBoundaryTrace(0,eLine,{0,4});
    xfld.addBoundaryTrace(0,eLine,{4,1});
    xfld.addBoundaryTrace(0,eLine,{1,5});
    xfld.addBoundaryTrace(0,eLine,{2,3});
    xfld.addBoundaryTrace(0,eLine,{5,2});

    this->buildFrom(xfld);
  }

};

BOOST_AUTO_TEST_CASE( XField_wtf )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  TestField xfld0(comm_local);

  std::ofstream file0;
  file0.open("xfld0_q1.txt");
  xfld0.dump(1,file0);

  int order = 3;
  XField<PhysD2,TopoD2> xfld(xfld0,order,BasisFunctionCategory_Lagrange);

  std::ofstream file1;
  file1.open("xfld0_q3.txt");
  xfld.dump(1,file1);


}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_4Triangle_1Group_InteriorCell_Target_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  const Real small_tol = 1e-12;
  const Real close_tol = 5e-9;

  XField2D_4Triangle_X1_1Group xfld0;

  // elevate the order
  int order = 3;
  XField<PhysD2,TopoD2> xfld(xfld0,order,BasisFunctionCategory_Lagrange);

  // initialize a uniform distribution between -0.01 and 0.01
  std::mt19937_64 rng;
  std::uniform_real_distribution<double> unif(-0.01, 0.01);

  // Perturb the DOFs so high order elements are curved
  for (int n = 0; n < xfld.nDOF(); n++)
  {
    xfld.DOF(n)[0] += unif(rng);
    xfld.DOF(n)[1] += unif(rng);
  }

  // build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  int group = 0, elem = 0;

  //Extract the local mesh for the central tet
  XField_LocalPatchConstructor<PhysD2,Triangle> xfld_local(comm_local,connectivity,group,elem);

  // create a split patch and split an edge
  int edge = 2;
  XField_LocalPatch<PhysD2,Triangle> xfld_split( xfld_local, ElementSplitType::Edge, edge );

  BOOST_REQUIRE_EQUAL( xfld_split.getNewNodeDOFs().size(), 1 );
  BOOST_CHECK_EQUAL( xfld_split.getNewNodeDOFs()[0], 36 );

  BOOST_REQUIRE_EQUAL( xfld_split.getNewLinearNodeDOFs().size(), 1 );
  BOOST_CHECK_EQUAL( xfld_split.getNewLinearNodeDOFs()[0], 6 );

  // check that all elements match the global elements
  XField_LocalGlobal_Equiv(xfld_split, small_tol, close_tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_HighOrderLocalPatchAddNeighbours_InteriorCell_Target_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  const Real small_tol = 1e-12;
  const Real close_tol = 9e-8;

  XField2D_Box_Triangle_Lagrange_X1 xfld0( comm_local, 6, 6 );

  // elevate the order
  int order = 4;
  XField<PhysD2,TopoD2> xfld(xfld0,order,BasisFunctionCategory_Lagrange);

  // initialize a uniform distribution between -0.001 and 0.001
  std::mt19937_64 rng;
  std::uniform_real_distribution<double> unif(-0.001, 0.001);

  // Perturb the DOFs so high order elements are curved
  for (int n = 0; n < xfld.nDOF(); n++)
  {
    xfld.DOF(n)[0] += unif(rng);
    xfld.DOF(n)[1] += unif(rng);
  }

  // build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  // build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  time_t t0 = clock();
  int nsplit = 0;
  for (int group = 0; group < xfld.nCellGroups(); group++)
  {
    for (int elem = 0; elem < xfld.getCellGroupBase(group).nElem(); elem++)
    {
      // extract the local mesh for the central tet
      XField_LocalPatchConstructor<PhysD2,Triangle> xfld_local(comm_local,connectivity,group,elem,SpaceType::Continuous,&nodalview);

      // split every edge
      for (int edge = 0;edge < Triangle::NEdge; edge++)
      {
        XField_LocalPatch<PhysD2,Triangle> xfld_split( xfld_local, ElementSplitType::Edge, edge );

        for (int j=0;j<xfld_split.nCellGroups();j++)
          BOOST_CHECK_EQUAL( xfld_split.getCellGroupBase(j).order(), order );
        nsplit++;

        // check that all elements match the global elements
        XField_LocalGlobal_Equiv(xfld_split, small_tol, close_tol);
      }
    }
  }

  printf("performed nsplit = %d in %g seconds\n",nsplit,double(clock()-t0)/CLOCKS_PER_SEC);

}

#ifdef SANS_AVRO
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_HighOrderLocalPatchAddNeighbours_CKF_InteriorCell_Target_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField_KuhnFreudenthal<PhysD2,TopoD2> xfld0( comm_local, {6,6} );

  // elevate the order
  int order = 1;
  //XField<PhysD2,TopoD2> xfld(xfld0,order,BasisFunctionCategory_Lagrange);

  XField<PhysD2,TopoD2>& xfld = xfld0;

  // build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  // build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  time_t t0 = clock();
  int nsplit = 0;
  for (int group=0;group<xfld.nCellGroups();group++)
  {
    for (int elem=0;elem<xfld.getCellGroupBase(group).nElem();elem++)
    {
      // extract the local mesh for the central tet
      XField_LocalPatchConstructor<PhysD2,Triangle> xfld_local(comm_local,connectivity,group,elem,SpaceType::Continuous,&nodalview);

      // split every edge
      for (int edge = 0;edge < Triangle::NEdge; edge++)
      {
        XField_LocalPatch<PhysD2,Triangle> xfld_split( xfld_local, ElementSplitType::Edge, edge );

        for (int j=0;j<xfld_split.nCellGroups();j++)
          BOOST_CHECK_EQUAL( xfld_split.getCellGroupBase(j).order(), order );
        nsplit++;
      }
    }
  }

  printf("performed nsplit = %d in %g seconds\n",nsplit,double(clock()-t0)/CLOCKS_PER_SEC);

}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
