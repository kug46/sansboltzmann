// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "tools/minmax.h"

#include "Field/Local/XField_LocalPatch.h"
#include "Field/Element/ElementProjection_L2.h"

#include "Quadrature/Quadrature.h"

namespace SANS
{

template<class PhysDim, class Topology>
void
XField_LocalGlobal_Equiv(const XField_LocalPatch<PhysDim,Topology>& xfld_local,
                         const Real small_tol = 1e-12, const Real close_tol = 1e-11)
{
  typedef typename Topology::TopologyTrace TraceTopology;
  typedef typename Topology::TopoDim TopoDim;

  typedef DLA::VectorS<TopoDim::D,Real> RefCoordCellType;
  typedef DLA::VectorS<MAX(1,TraceTopology::TopoDim::D),Real> RefCoordTraceType;
  typedef typename XField<PhysDim, TopoDim>::VectorX VectorX;

  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TraceTopology> XFieldTraceGroupType;

  const XField<PhysDim,TopoDim>& xfld_global = xfld_local.xfld_global();

  Quadrature<TopoDim, Topology> cellQuadrature(xfld_local.order()+2);
  const int nCellquad = cellQuadrature.nQuadrature();

  int traceOrder = TraceTopology::TopoDim::D == 0 ? 0 : xfld_local.order()+2;

  Quadrature<typename TraceTopology::TopoDim, TraceTopology> traceQuadrature(traceOrder);
  const int nTracequad = traceQuadrature.nQuadrature();

  // check DOF's for unsplit elements match global and local
  for (int group = 0; group < xfld_local.nCellGroups(); group++)
  {
    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

    const XFieldCellGroupType& cellGroup = xfld_local.getCellGroup(group);
    ElementXFieldClass xfldElemLocal( cellGroup.basis() );
    for (int elem = 0; elem < cellGroup.nElem(); elem++)
    {
      // retrieve the split info
      ElementSplitInfo info = xfld_local.getCellSplitInfo({group,elem});

      std::pair<int,int> globalElem = xfld_local.getGlobalCellMap( {group, elem} );

      ElementXFieldClass xfldElemGlobal( xfld_global.template getCellGroup<Topology>( globalElem.first ).basis() );

      // retrieve the global and local elements
      xfld_global.template getCellGroup<Topology>( globalElem.first ).getElement( xfldElemGlobal, globalElem.second );
      cellGroup.getElement( xfldElemLocal, elem );

      BOOST_CHECK( info.split_flag == ElementSplitFlag::Unsplit ||
                   info.split_flag == ElementSplitFlag::Split      );

      if ( info.split_flag == ElementSplitFlag::Unsplit )
      {
        // check the DOF's match
        for (int n = 0; n < xfldElemGlobal.nDOF(); n++)
          for (int d = 0; d < PhysDim::D; d++)
            SANS_CHECK_CLOSE(xfldElemGlobal.DOF(n)[d], xfldElemLocal.DOF(n)[d], small_tol, close_tol);
      }
      else if ( info.split_flag == ElementSplitFlag::Split )
      {
        ElementSplitInfo splitInfo = xfld_local.getCellSplitInfo(std::make_pair(group,elem));

        RefCoordCellType RefCoord_main, RefCoord_sub;  // reference-element coordinates
        VectorX X_main, X_local;

        //Check if the solutions are equal at a few cellQuadrature points
        for (int iquad = 0; iquad < nCellquad; iquad++)
        {
          cellQuadrature.coordinates( iquad, RefCoord_sub );

          // coordinates in the local elements
          xfldElemLocal.eval( RefCoord_sub, X_local);

          BasisFunction_RefElement_Split<TopoDim,Topology>::
            transform( RefCoord_sub, splitInfo.split_type, splitInfo.edge_index, splitInfo.subcell_index, RefCoord_main);

          xfldElemGlobal.eval(RefCoord_main, X_main);

          for (int d = 0; d < PhysDim::D; d++)
            SANS_CHECK_CLOSE(X_main[d], X_local[d], small_tol, close_tol);
        }
      }
    }
  }

  // Check interior trace equivalence
  for (int group = 0; group < xfld_local.nInteriorTraceGroups(); group++)
  {
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldClass;

    const XFieldTraceGroupType& traceGroup = xfld_local.getInteriorTraceGroup(group);
    ElementXFieldClass xfldElemLocal( traceGroup.basis() );

    //Element_Subdivision_Projector<TopoDimTrace, TopoTrace> elemProjector(local_tracegrp.basis());

    for (int elem = 0; elem < traceGroup.nElem(); elem++)
    {
      ElementSplitInfo splitInfo = xfld_local.getInteriorTraceSplitInfo({group, elem});

      traceGroup.getElement( xfldElemLocal, elem );

      int xfld_local_cellgroupL   = traceGroup.getGroupLeft();
      int xfld_local_cellelemL    = traceGroup.getElementLeft(elem);
      int trace_orientation_local = traceGroup.getCanonicalTraceLeft(elem).orientation; // Main cell group is always to the left

      std::pair<int,int> global_cell_pair = xfld_local.getGlobalCellMap({xfld_local_cellgroupL, xfld_local_cellelemL});

      if (splitInfo.split_flag == ElementSplitFlag::New)
      {
#if 0
        typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldCell;

        //Get global cell element
        const XFieldCellGroupType& cellGroup_global = xfld_global.template getCellGroup<Topology>( global_cell_pair.first );

        ElementXFieldCell xfldElemGlobal( cellGroup_global.basis() );
        cellGroup_global.getElement( xfldElemGlobal, global_cell_pair.second );

        RefCoordTraceType  RefCoord_sub;  // reference-element coordinates
        RefCoordCellType RefCoord_main;
        VectorX X_main, X_local;

        ElementSplitInfo cell_splitinfo = xfld_local.getCellSplitInfo({xfld_local_cellgroupL, xfld_local_cellelemL});

        //Check if the solutions are equal at a few cellQuadrature points
        for (int iquad = 0; iquad < nTracequad; iquad++)
        {
          traceQuadrature.coordinates( iquad, RefCoord_sub );

          // coordinates in the local elements
          xfldElemLocal.eval( RefCoord_sub, X_local);

          BasisFunction_RefElement_Split_TraceToCell<TopoDim, Topology>::
            transform(RefCoord_sub, cell_splitinfo.split_type, cell_splitinfo.edge_index, splitInfo.subcell_index, RefCoord_main);

          xfldElemGlobal.eval(RefCoord_main, X_main);

          for (int d = 0; d < PhysDim::D; d++)
            SANS_CHECK_CLOSE(X_main[d], X_local[d], small_tol, close_tol);
        }
#endif
      }
      else
      {
        std::pair<int,int> globalElem = xfld_local.getGlobalInteriorTraceMap({group, elem});

        //Get global interior trace element
        const XFieldTraceGroupType& traceGroup_global = xfld_global.template getInteriorTraceGroup<TraceTopology>( globalElem.first );

        ElementXFieldClass xfldElemGlobal( traceGroup_global.basis() );
        traceGroup_global.getElement( xfldElemGlobal, globalElem.second );

        //Check if the DOFs obtained above need to be re-oriented due to orientation changes in interior traces
        //between the global and local meshes
        int xfld_global_cellgroupL = traceGroup_global.getGroupLeft();
        int xfld_global_cellelemL  = traceGroup_global.getElementLeft(globalElem.second);

        int xfld_global_cellgroupR = traceGroup_global.getGroupRight();
        int xfld_global_cellelemR  = traceGroup_global.getElementRight(globalElem.second);

        int trace_orientation_global = 0;

        // If the global cell is to the left of the local edge, the orientation has not changed
        // If the global cell is to the right of the local edge, the orientation has changed

        if ( xfld_global_cellgroupL == global_cell_pair.first &&
             xfld_global_cellelemL  == global_cell_pair.second )
        {
          //Local left cell is to the left of the global trace
          trace_orientation_global = traceGroup_global.getCanonicalTraceLeft(globalElem.second).orientation;
        }
        else if ( xfld_global_cellgroupR == global_cell_pair.first &&
                  xfld_global_cellelemR  == global_cell_pair.second )
        {
          //Local left cell is to the right of the global trace
          trace_orientation_global = traceGroup_global.getCanonicalTraceRight(globalElem.second).orientation;
        }
        else
          SANS_DEVELOPER_EXCEPTION( "Field_Local<PhysDim, TopoDim, T>::transferInteriorTraceGroup - "
                                    "Local mesh inconsistent with global mesh. Code should not get here!" );

        if (trace_orientation_global != trace_orientation_local) //Orientations are different, so need to do a projection
        {
          //Create temporary fldelem and copy global trace
          ElementXFieldClass xfldElem_tmp( xfldElemGlobal );

          //Correct the orientation of global trace DOFs
          Element_Trace_Projection_L2(xfldElem_tmp, trace_orientation_global, xfldElemGlobal, trace_orientation_local);
        }

        if (splitInfo.split_flag == ElementSplitFlag::Unsplit)
        {
          // check the DOF's match
          for (int n = 0; n < xfldElemGlobal.nDOF(); n++)
            for (int d = 0; d < PhysDim::D; d++)
              SANS_CHECK_CLOSE(xfldElemGlobal.DOF(n)[d], xfldElemLocal.DOF(n)[d], small_tol, close_tol);
        }
        else if (splitInfo.split_flag == ElementSplitFlag::Split)
        {
          RefCoordTraceType RefCoord_main, RefCoord_sub;  // reference-element coordinates
          VectorX X_main, X_local;

          //Check if the solutions are equal at a few cellQuadrature points
          for (int iquad = 0; iquad < nTracequad; iquad++)
          {
            traceQuadrature.coordinates( iquad, RefCoord_sub );

            // coordinates in the local elements
            xfldElemLocal.eval( RefCoord_sub, X_local);

            BasisFunction_RefElement_Split<typename TraceTopology::TopoDim,TraceTopology>::
              transform( RefCoord_sub, splitInfo.split_type, splitInfo.edge_index, splitInfo.subcell_index, RefCoord_main);

            xfldElemGlobal.eval(RefCoord_main, X_main);

            for (int d = 0; d < PhysDim::D; d++)
              SANS_CHECK_CLOSE(X_main[d], X_local[d], small_tol, close_tol);
          }
        }
        else
          SANS_DEVELOPER_EXCEPTION( "Field_Local<PhysDim, TopoDim, T>::transferInteriorTraceGroup - Unknown ElementSplitFlag." );
      }
    } //loop over elements
  }

  // check DOF's for unsplit elements match global and local and quadrature points on split elements
  for (int group = 0; group < xfld_local.nBoundaryTraceGroups(); group++)
  {
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldClass;

    const XFieldTraceGroupType& traceGroup = xfld_local.getBoundaryTraceGroup(group);
    ElementXFieldClass xfldElemLocal( traceGroup.basis() );
    for (int elem = 0; elem < traceGroup.nElem(); elem++)
    {
      // retrieve the split info
      ElementSplitInfo splitInfo = xfld_local.getBoundaryTraceSplitInfo({group,elem});

      std::pair<int,int> globalElem = xfld_local.getGlobalBoundaryTraceMap( {group, elem} );

      ElementXFieldClass xfldElemGlobal( xfld_global.template getBoundaryTraceGroup<TraceTopology>( globalElem.first ).basis() );

      BOOST_CHECK( splitInfo.split_flag == ElementSplitFlag::Unsplit ||
                   splitInfo.split_flag == ElementSplitFlag::Split      );

      // retrieve the global and local elements
      xfld_global.template getBoundaryTraceGroup<TraceTopology>( globalElem.first ).getElement( xfldElemGlobal, globalElem.second );
      traceGroup.getElement( xfldElemLocal, elem );

      if ( splitInfo.split_flag == ElementSplitFlag::Unsplit )
      {
        // check the DOF's match
        for (int n = 0; n < xfldElemGlobal.nDOF(); n++)
          for (int d = 0; d < PhysDim::D; d++)
            SANS_CHECK_CLOSE(xfldElemGlobal.DOF(n)[d], xfldElemLocal.DOF(n)[d], small_tol, close_tol);
      }
      else if ( splitInfo.split_flag == ElementSplitFlag::Split )
      {
        RefCoordTraceType RefCoord_main, RefCoord_sub;  // reference-element coordinates
        VectorX X_main, X_local;

        //Check if the solutions are equal at a few cellQuadrature points
        for (int iquad = 0; iquad < nTracequad; iquad++)
        {
          traceQuadrature.coordinates( iquad, RefCoord_sub );

          // coordinates in the local elements
          xfldElemLocal.eval( RefCoord_sub, X_local);

          BasisFunction_RefElement_Split<typename TraceTopology::TopoDim,TraceTopology>::
            transform( RefCoord_sub, splitInfo.split_type, splitInfo.edge_index, splitInfo.subcell_index, RefCoord_main);

          xfldElemGlobal.eval(RefCoord_main, X_main);

          for (int d = 0; d < PhysDim::D; d++)
            SANS_CHECK_CLOSE(X_main[d], X_local[d], small_tol, close_tol);
        }
      }
    }
  }
}

}
