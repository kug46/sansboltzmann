// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// XField2D_Local_btest
// testing of XField2D_Local
//
// Note: unit grids tested in "unit/UnitGrids/UnitGrids_btest.cpp"

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "Topology/ElementTopology.h"

#include "Field/XField_CellToTrace.h"
#include "Field/Local/XField_LocalPatch.h"

#include "XField_LocalGlobal_Equiv_btest.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_InteriorTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"
#include "unit/UnitGrids/XField2D_Annulus_Triangle_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( XField2D_LocalPatch_Element_DG_test_suite )

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_2Triangle_X1_1Group_LeftCell_Target_test )
{
  typedef std::array<int,3> Int3;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField2D_2Triangle_X1_1Group xfld;

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  // Extract the local mesh for the bottom-left triangle
  XField_LocalPatchConstructor<PhysD2, Triangle> xfld_construct(comm_local,
      connectivity, 0, 0);
  XField_LocalPatch<PhysD2, Triangle> xfld_local(xfld_construct);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  //Check local to global mappings
  BOOST_CHECK( xfld_local.getGlobalCellMap({0,0}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_local.getGlobalCellMap({1,0}) == std::make_pair(0,1));

  BOOST_CHECK( xfld_local.getGlobalInteriorTraceMap({0,0}) == std::make_pair(0,0));

  BOOST_CHECK( xfld_local.getGlobalBoundaryTraceMap({0,0}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_local.getGlobalBoundaryTraceMap({0,1}) == std::make_pair(0,3));

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 4 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld_local.DOF(0)[0],  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(0)[1],  0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(1)[0],  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(1)[1],  0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(2)[0],  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(2)[1],  1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(3)[0],  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(3)[1],  1 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_local.getCellGroup<Triangle>(0);
  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup1 = xfld_local.getCellGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 1 );

  Int3 edgeSign;

  edgeSign = xfldCellGroup0.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  edgeSign = xfldCellGroup1.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], -1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup = xfld_local.getInteriorTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldITraceGroup.nElem(), 1 );

  // interior edge-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldITraceGroup.getGroupLeft() , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getGroupRight(), 1 );

  BOOST_CHECK_EQUAL( xfldITraceGroup.getElementLeft(0) , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getElementRight(0), 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceLeft(0) .trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceLeft(0) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceRight(0).trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceRight(0).orientation,-1 );

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 2 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getGroupRight(), -1 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementLeft(0) , 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementRight(0),-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(0) .trace      , 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(0) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceRight(0).trace      ,-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceRight(0).orientation, 0 );

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_local.getGhostBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldGBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 2 );

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getGroupLeft(), 1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getGroupRight(), -1 );

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getElementLeft(0) , 0 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getElementRight(0),-1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceLeft(0) .trace      , 1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceLeft(0) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceRight(0).trace      ,-1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceRight(0).orientation, 0 );

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getElementLeft(1) , 0 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getElementRight(1),-1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceLeft(1) .trace      , 2 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceLeft(1) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceRight(1).trace      ,-1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceRight(1).orientation, 0 );

  XField_LocalGlobal_Equiv(xfld_local);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_2Triangle_X1_1Group_RightCell_Target_test )
{
  typedef std::array<int,3> Int3;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField2D_2Triangle_X1_1Group xfld;

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh for the top-right triangle
  XField_LocalPatchConstructor<PhysD2, Triangle> xfld_construct(comm_local, connectivity, 0, 1);
  XField_LocalPatch<PhysD2, Triangle> xfld_local(xfld_construct);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  //Check local to global mappings
  BOOST_CHECK( xfld_local.getGlobalCellMap({0,0}) == std::make_pair(0,1));
  BOOST_CHECK( xfld_local.getGlobalCellMap({1,0}) == std::make_pair(0,0));

  BOOST_CHECK( xfld_local.getGlobalInteriorTraceMap({0,0}) == std::make_pair(0,0));

  BOOST_CHECK( xfld_local.getGlobalBoundaryTraceMap({0,0}) == std::make_pair(0,2));
  BOOST_CHECK( xfld_local.getGlobalBoundaryTraceMap({0,1}) == std::make_pair(0,1));

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 4 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld_local.DOF(0)[0],  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(0)[1],  1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(1)[0],  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(1)[1],  1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(2)[0],  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(2)[1],  0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(3)[0],  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(3)[1],  0 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_local.getCellGroup<Triangle>(0);
  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup1 = xfld_local.getCellGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 1 );

  Int3 edgeSign;

  edgeSign = xfldCellGroup0.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  edgeSign = xfldCellGroup1.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], -1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup = xfld_local.getInteriorTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldITraceGroup.nElem(), 1 );

  // interior edge-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldITraceGroup.getGroupLeft() , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getGroupRight(), 1 );

  BOOST_CHECK_EQUAL( xfldITraceGroup.getElementLeft(0) , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getElementRight(0), 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceLeft(0) .trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceLeft(0) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceRight(0).trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceRight(0).orientation,-1 );

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 2 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getGroupRight(), -1 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementLeft(0) , 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementRight(0),-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(0) .trace      , 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(0) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceRight(0).trace      ,-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceRight(0).orientation, 0 );

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_local.getGhostBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldGBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 2 );

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getGroupLeft(), 1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getGroupRight(), -1 );

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getElementLeft(0) , 0 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getElementRight(0),-1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceLeft(0) .trace      , 1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceLeft(0) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceRight(0).trace      ,-1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceRight(0).orientation, 0 );

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getElementLeft(1) , 0 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getElementRight(1),-1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceLeft(1) .trace      , 2 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceLeft(1) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceRight(1).trace      ,-1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceRight(1).orientation, 0 );

  XField_LocalGlobal_Equiv(xfld_local);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_2Triangle_X3_1Group_RightCell_Target_test )
{
  typedef std::array<int,3> Int3;

  const Real tol = 5e-12;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField2D_2Triangle_X1_1Group xfld_X1; //Linear mesh

  XField<PhysD2, TopoD2> xfld(xfld_X1, 3, BasisFunctionCategory_Hierarchical); //Construct Q3 mesh from linear mesh

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  // Extract the local mesh for the top-right triangle
  XField_LocalPatchConstructor<PhysD2, Triangle> xfld_construct(comm_local, connectivity, 0, 1);
  XField_LocalPatch<PhysD2, Triangle> xfld_local(xfld_construct);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  //Check local to global mappings
  BOOST_CHECK( xfld_local.getGlobalCellMap({0,0}) == std::make_pair(0,1));
  BOOST_CHECK( xfld_local.getGlobalCellMap({1,0}) == std::make_pair(0,0));

  BOOST_CHECK( xfld_local.getGlobalInteriorTraceMap({0,0}) == std::make_pair(0,0));

  BOOST_CHECK( xfld_local.getGlobalBoundaryTraceMap({0,0}) == std::make_pair(0,2));
  BOOST_CHECK( xfld_local.getGlobalBoundaryTraceMap({0,1}) == std::make_pair(0,1));

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 16 );

  //Check the node values
  for (int k=0; k<12; k++)
  {
    BOOST_CHECK_SMALL( xfld_local.DOF(k)[0], tol );  BOOST_CHECK_SMALL( xfld_local.DOF(k)[1], tol );
  }

  BOOST_CHECK_CLOSE( xfld_local.DOF(12)[0],  1, tol );  BOOST_CHECK_CLOSE( xfld_local.DOF(12)[1],  1, tol );
  BOOST_CHECK_SMALL( xfld_local.DOF(13)[0],     tol );  BOOST_CHECK_CLOSE( xfld_local.DOF(13)[1],  1, tol );
  BOOST_CHECK_CLOSE( xfld_local.DOF(14)[0],  1, tol );  BOOST_CHECK_SMALL( xfld_local.DOF(14)[1],     tol );
  BOOST_CHECK_SMALL( xfld_local.DOF(15)[0],     tol );  BOOST_CHECK_SMALL( xfld_local.DOF(15)[1],     tol );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_local.getCellGroup<Triangle>(0);
  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup1 = xfld_local.getCellGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 1 );

  Int3 edgeSign;

  edgeSign = xfldCellGroup0.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  edgeSign = xfldCellGroup1.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], -1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup = xfld_local.getInteriorTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldITraceGroup.nElem(), 1 );

  // interior edge-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldITraceGroup.getGroupLeft() , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getGroupRight(), 1 );

  BOOST_CHECK_EQUAL( xfldITraceGroup.getElementLeft(0) , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getElementRight(0), 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceLeft(0) .trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceLeft(0) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceRight(0).trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceRight(0).orientation,-1 );

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 2 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getGroupRight(), -1 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementLeft(0) , 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementRight(0),-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(0) .trace      , 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(0) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceRight(0).trace      ,-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceRight(0).orientation, 0 );

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_local.getGhostBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldGBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 2 );

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getGroupLeft(), 1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getGroupRight(), -1 );

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getElementLeft(0) , 0 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getElementRight(0),-1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceLeft(0) .trace      , 1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceLeft(0) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceRight(0).trace      ,-1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceRight(0).orientation, 0 );

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getElementLeft(1) , 0 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getElementRight(1),-1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceLeft(1) .trace      , 2 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceLeft(1) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceRight(1).trace      ,-1 );
  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.getCanonicalTraceRight(1).orientation, 0 );

  XField_LocalGlobal_Equiv(xfld_local);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_4Triangle_X2_1Group_InteriorCell_Target_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField2D_4Triangle_X1_1Group xfld_X1; //Linear mesh

  XField<PhysD2,TopoD2> xfld(xfld_X1,2); //Construct Q2 mesh from linear mesh

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh for the central triangle
  XField_LocalPatchConstructor<PhysD2, Triangle> xfld_construct(comm_local, connectivity, 0, 0);
  XField_LocalPatch<PhysD2, Triangle> xfld_local(xfld_construct);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  //Check local to global mappings
  BOOST_CHECK( xfld_local.getGlobalCellMap({0,0}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_local.getGlobalCellMap({1,0}) == std::make_pair(0,1));
  BOOST_CHECK( xfld_local.getGlobalCellMap({1,1}) == std::make_pair(0,2));
  BOOST_CHECK( xfld_local.getGlobalCellMap({1,2}) == std::make_pair(0,3));

  BOOST_CHECK( xfld_local.getGlobalInteriorTraceMap({0,0}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_local.getGlobalInteriorTraceMap({0,1}) == std::make_pair(0,1));
  BOOST_CHECK( xfld_local.getGlobalInteriorTraceMap({0,2}) == std::make_pair(0,2));

  BOOST_CHECK_THROW( xfld_local.getGlobalBoundaryTraceMap({0,0}), AssertionException); //empty map

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 15 );

  //Check the node values
  for (int i=0; i<9; i++)
  {
    //All higher-order node values need to be zero since the Q2 mesh is also still essentially linear
    BOOST_CHECK_EQUAL( xfld_local.DOF(i)[0], 0);
    BOOST_CHECK_EQUAL( xfld_local.DOF(i)[1], 0);
  }
  BOOST_CHECK_EQUAL( xfld_local.DOF( 9)[0],  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF( 9)[1],  0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(10)[0],  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(10)[1],  0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(11)[0],  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(11)[1],  1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(12)[0],  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(12)[1],  1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(13)[0], -1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(13)[1],  1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(14)[0],  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(14)[1], -1 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup = xfld_local.getInteriorTraceGroup<Line>(0);

  // interior edge-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldITraceGroup.getGroupLeft() , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getGroupRight(), 1 );

  BOOST_CHECK_EQUAL( xfldITraceGroup.getElementLeft(0) , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getElementRight(0), 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceLeft(0) .trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceLeft(0) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceRight(0).trace      , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceRight(0).orientation,-1 );

  BOOST_CHECK_EQUAL( xfldITraceGroup.getElementLeft(1) , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getElementRight(1), 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceLeft(1) .trace      , 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceLeft(1) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceRight(1).trace      , 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceRight(1).orientation,-1 );

  BOOST_CHECK_EQUAL( xfldITraceGroup.getElementLeft(2), 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getElementRight(2), 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceLeft(2) .trace      , 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceLeft(2) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceRight(2).trace      , 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceRight(2).orientation,-1 );

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nBoundaryTraceGroups(), 0 );

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_local.getGhostBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup = xfld_local.getGhostBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldBTraceGroup.getGroupLeft(), 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup.getGroupRight(), -1 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup.getElementLeft(0) , 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup.getElementRight(0),-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup.getCanonicalTraceLeft(0) .trace      , 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup.getCanonicalTraceLeft(0) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup.getCanonicalTraceRight(0).trace      ,-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup.getCanonicalTraceRight(0).orientation, 0 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup.getElementLeft(1) , 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup.getElementRight(1),-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup.getCanonicalTraceLeft(1) .trace      , 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup.getCanonicalTraceLeft(1) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup.getCanonicalTraceRight(1).trace      ,-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup.getCanonicalTraceRight(1).orientation, 0 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup.getElementLeft(2) , 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup.getElementRight(2),-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup.getCanonicalTraceLeft(2) .trace      , 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup.getCanonicalTraceLeft(2) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup.getCanonicalTraceRight(2).trace      ,-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup.getCanonicalTraceRight(2).orientation, 0 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup.getElementLeft(3) , 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup.getElementRight(3),-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup.getCanonicalTraceLeft(3) .trace      , 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup.getCanonicalTraceLeft(3) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup.getCanonicalTraceRight(3).trace      ,-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup.getCanonicalTraceRight(3).orientation, 0 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup.getElementLeft(4) , 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup.getElementRight(4),-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup.getCanonicalTraceLeft(4) .trace      , 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup.getCanonicalTraceLeft(4) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup.getCanonicalTraceRight(4).trace      ,-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup.getCanonicalTraceRight(4).orientation, 0 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup.getElementLeft(5) , 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup.getElementRight(5),-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup.getCanonicalTraceLeft(5) .trace      , 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup.getCanonicalTraceLeft(5) .orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup.getCanonicalTraceRight(5).trace      ,-1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup.getCanonicalTraceRight(5).orientation, 0 );

  XField_LocalGlobal_Equiv(xfld_local);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_4Triangle_X1_1Group_BoundaryCell_Target_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField2D_4Triangle_X1_1Group xfld;

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh for a boundary triangle
  XField_LocalPatchConstructor<PhysD2, Triangle> xfld_construct(comm_local, connectivity, 0, 1);
  XField_LocalPatch<PhysD2, Triangle> xfld_local(xfld_construct);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  //Check local to global mappings
  BOOST_CHECK( xfld_local.getGlobalCellMap({0,0}) == std::make_pair(0,1));
  BOOST_CHECK( xfld_local.getGlobalCellMap({1,0}) == std::make_pair(0,0));

  BOOST_CHECK( xfld_local.getGlobalInteriorTraceMap({0,0}) == std::make_pair(0,0));

  BOOST_CHECK( xfld_local.getGlobalBoundaryTraceMap({0,0}) == std::make_pair(0,3));

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 4 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld_local.DOF(0)[0],  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(0)[1],  1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(1)[0],  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(1)[1],  1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(2)[0],  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(2)[1],  0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(3)[0],  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(3)[1],  0 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_local.getGhostBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  XField_LocalGlobal_Equiv(xfld_local);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_4Triangle_X2_1Group_BoundaryCell_Target_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField2D_4Triangle_X1_1Group xfld_X1; //Linear mesh

  XField<PhysD2,TopoD2> xfld(xfld_X1,2); //Construct Q2 mesh from linear mesh

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh for a boundary triangle
  XField_LocalPatchConstructor<PhysD2, Triangle> xfld_construct(comm_local, connectivity, 0, 1);
  XField_LocalPatch<PhysD2, Triangle> xfld_local(xfld_construct);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  //Check local to global mappings
  BOOST_CHECK( xfld_local.getGlobalCellMap({0,0}) == std::make_pair(0,1));
  BOOST_CHECK( xfld_local.getGlobalCellMap({1,0}) == std::make_pair(0,0));

  BOOST_CHECK( xfld_local.getGlobalInteriorTraceMap({0,0}) == std::make_pair(0,0));

  BOOST_CHECK( xfld_local.getGlobalBoundaryTraceMap({0,0}) == std::make_pair(0,3));

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 9 );

  //Check the node values
  for (int i=0; i<5; i++)
  {
    //All higher-order node values need to be zero since the Q2 mesh is also still essentially linear
    BOOST_CHECK_EQUAL( xfld_local.DOF(i)[0], 0);
    BOOST_CHECK_EQUAL( xfld_local.DOF(i)[1], 0);
  }
  BOOST_CHECK_EQUAL( xfld_local.DOF(5)[0],  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(5)[1],  1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(6)[0],  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(6)[1],  1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(7)[0],  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(7)[1],  0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(8)[0],  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(8)[1],  0 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  XField_LocalGlobal_Equiv(xfld_local);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_1Triangle_X1_1Group_test )
{

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField2D_1Triangle_X1_1Group xfld;

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh for a boundary triangle
  XField_LocalPatchConstructor<PhysD2, Triangle> xfld_construct(comm_local, connectivity, 0, 0);
  XField_LocalPatch<PhysD2, Triangle> xfld_local(xfld_construct);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  //Check local to global mappings
  BOOST_CHECK( xfld_local.getGlobalCellMap({0,0}) == std::make_pair(0,0));

  BOOST_CHECK( xfld_local.getGlobalBoundaryTraceMap({0,0}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_local.getGlobalBoundaryTraceMap({1,0}) == std::make_pair(2,0));
  BOOST_CHECK( xfld_local.getGlobalBoundaryTraceMap({2,0}) == std::make_pair(1,0));

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 3 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld_local.DOF(0)[0],  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(0)[1],  0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(1)[0],  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(1)[1],  0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(2)[0],  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(2)[1],  1 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) ); // this is a quirk of local patch

  BOOST_CHECK_EQUAL(xfld_local.getCellGroupBase(0).nElem(), 1);
  BOOST_CHECK_EQUAL(xfld_local.getCellGroupBase(1).nElem(), 0);

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nInteriorTraceGroups(), 0 );

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nBoundaryTraceGroups(), 3 );

  BOOST_CHECK_EQUAL(xfld_local.getBoundaryTraceGroupBase(0).nElem(), 1);
  BOOST_CHECK_EQUAL(xfld_local.getBoundaryTraceGroupBase(1).nElem(), 1);
  BOOST_CHECK_EQUAL(xfld_local.getBoundaryTraceGroupBase(2).nElem(), 1);

  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );

  XField_LocalGlobal_Equiv(xfld_local);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_1Triangle_X2_1Group_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField2D_1Triangle_X1_1Group xfld_X1; //Linear mesh

  XField<PhysD2,TopoD2> xfld(xfld_X1,2); //Construct Q2 mesh from linear mesh

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh for a boundary triangle
  XField_LocalPatchConstructor<PhysD2, Triangle> xfld_construct(comm_local, connectivity, 0, 0);
  XField_LocalPatch<PhysD2, Triangle> xfld_local(xfld_construct);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  //Check local to global mappings
  BOOST_CHECK( xfld_local.getGlobalCellMap({0,0}) == std::make_pair(0,0));

  BOOST_CHECK( xfld_local.getGlobalBoundaryTraceMap({0,0}) == std::make_pair(0,0));
  BOOST_CHECK( xfld_local.getGlobalBoundaryTraceMap({1,0}) == std::make_pair(2,0));
  BOOST_CHECK( xfld_local.getGlobalBoundaryTraceMap({2,0}) == std::make_pair(1,0));


  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 6 );

  //Check the node values
  for (int i=0; i<3; i++)
  {
    //All higher-order node values need to be zero since the Q2 mesh is also still essentially linear
    BOOST_CHECK_EQUAL( xfld_local.DOF(i)[0], 0);
    BOOST_CHECK_EQUAL( xfld_local.DOF(i)[1], 0);
  }
  BOOST_CHECK_EQUAL( xfld_local.DOF(3)[0],  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(3)[1],  0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(4)[0],  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(4)[1],  0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(5)[0],  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(5)[1],  1 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  BOOST_CHECK_EQUAL(xfld_local.getCellGroupBase(0).nElem(), 1);
  BOOST_CHECK_EQUAL(xfld_local.getCellGroupBase(1).nElem(), 0); // this is a quirk of localpatch

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nInteriorTraceGroups(), 0 );

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nBoundaryTraceGroups(), 3 );

  BOOST_CHECK_EQUAL(xfld_local.getBoundaryTraceGroupBase(0).nElem(), 1);
  BOOST_CHECK_EQUAL(xfld_local.getBoundaryTraceGroupBase(1).nElem(), 1);
  BOOST_CHECK_EQUAL(xfld_local.getBoundaryTraceGroupBase(2).nElem(), 1);

  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );

  BOOST_CHECK_EQUAL(xfld_local.getGhostBoundaryTraceGroupBase(0).nElem(), 0);

  XField_LocalGlobal_Equiv(xfld_local);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_4Triangle_X2_1Group_BoundaryCell_Target_test2 )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField2D_4Triangle_X1_1Group xfld_X1; //Linear mesh

  XField<PhysD2,TopoD2> xfld(xfld_X1,2); //Construct Q2 mesh from linear mesh

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh for a boundary triangle
  XField_LocalPatchConstructor<PhysD2, Triangle> xfld_construct(comm_local, connectivity, 0, 2);
  XField_LocalPatch<PhysD2, Triangle> xfld_local(xfld_construct);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  //Check local to global mappings
  BOOST_CHECK( xfld_local.getGlobalCellMap({0,0}) == std::make_pair(0,2));
  BOOST_CHECK( xfld_local.getGlobalCellMap({1,0}) == std::make_pair(0,0));

  BOOST_CHECK( xfld_local.getGlobalInteriorTraceMap({0,0}) == std::make_pair(0,1));

  BOOST_CHECK( xfld_local.getGlobalBoundaryTraceMap({0,0}) == std::make_pair(0,4));
  // BOOST_CHECK( xfld_local.getGlobalBoundaryTraceMap({1,0}) == std::make_pair(0,4));

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 9 );

  //Check the node values
  for (int i=0; i<5; i++)
  {
    //All higher-order node values need to be zero since the Q2 mesh is also still essentially linear
    BOOST_CHECK_EQUAL( xfld_local.DOF(i)[0], 0);
    BOOST_CHECK_EQUAL( xfld_local.DOF(i)[1], 0);
  }
  BOOST_CHECK_EQUAL( xfld_local.DOF(5)[0],  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(5)[1],  1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(6)[0], -1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(6)[1],  1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(7)[0],  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(7)[1],  0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(8)[0],  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(8)[1],  0 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  BOOST_CHECK_EQUAL(xfld_local.getCellGroupBase(0).nElem(), 1);
  BOOST_CHECK_EQUAL(xfld_local.getCellGroupBase(1).nElem(), 1);

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  BOOST_CHECK_EQUAL(xfld_local.getInteriorTraceGroup<Line>(0).nElem(), 1);

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  BOOST_CHECK_EQUAL(xfld_local.getBoundaryTraceGroup<Line>(0).nElem(), 2);

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_local.getGhostBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  BOOST_CHECK_EQUAL(xfld_local.getGhostBoundaryTraceGroup<Line>(0).nElem(), 2);

  XField_LocalGlobal_Equiv(xfld_local);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_Box_Triangle_X1_CenterRightCell_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField2D_Box_Triangle_Lagrange_X1 xfld(comm_local, 2, 1);

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh for the center-right triangle
  XField_LocalPatchConstructor<PhysD2, Triangle> xfld_construct(comm_local, connectivity, 0, 2);
  XField_LocalPatch<PhysD2, Triangle> xfld_local(xfld_construct);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 5 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld_local.DOF(0)[0],  0.5 );  BOOST_CHECK_EQUAL( xfld_local.DOF(0)[1],  0.0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(1)[0],  1.0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(1)[1],  0.0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(2)[0],  0.5 );  BOOST_CHECK_EQUAL( xfld_local.DOF(2)[1],  1.0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(3)[0],  1.0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(3)[1],  1.0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(4)[0],  0.0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(4)[1],  1.0 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_local.getCellGroup<Triangle>(0);
  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup1 = xfld_local.getCellGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 2 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup = xfld_local.getInteriorTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldITraceGroup.nElem(), 2 );

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 1 );

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_local.getGhostBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  XField_LocalGlobal_Equiv(xfld_local);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_WholeGrid_Box_Triangle_test )
{
  typedef typename XField<PhysD2, TopoD2>::template FieldCellGroupType<Triangle> XFieldCellGroupType;

  mpi::communicator world;
  mpi::communicator comm_local= world.split(world.rank());

  int size= 3*3*world.size();
  int ii= sqrt(size);
  int jj= ii;

  XField2D_Box_Triangle_Lagrange_X1 xfld(world, ii, jj);

  // Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2, TopoD2> connectivity(xfld);

  for (int group = 0; group < xfld.nCellGroups(); group++)
  {
    const XFieldCellGroupType& xfldCellGroup = xfld.template getCellGroup<Triangle>(group);

    for (int elem = 0; elem < xfld.getCellGroup<Triangle>(group).nElem(); elem++)
    {
      // only consider possessed elements
      if (xfldCellGroup.associativity(elem).rank() != world.rank()) continue;

      // Construct unsplit grid
      XField_LocalPatchConstructor<PhysD2, Triangle> xfld_construct(comm_local, connectivity, group, elem, SpaceType::Discontinuous);
      XField_LocalPatch<PhysD2, Triangle> xfld_local(xfld_construct);

      // check that all local elements match the global elements
      XField_LocalGlobal_Equiv(xfld_local);
    }
  }
}
#endif

#if 1
//--------------------------------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE(Split_4Triangle_X1_1Group_InteriorCell_SplitEdge0_metadata_test)
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField2D_4Triangle_X1_1Group xfld;

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh for the central triangle
  XField_LocalPatchConstructor<PhysD2, Triangle> xfld_local(comm_local, connectivity, 0, 0);

  int split_edge_index = 0;
  XField_LocalPatch<PhysD2, Triangle> xfld_split_local(xfld_local,
                                                       ElementSplitType::Edge,
                                                       split_edge_index);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_split_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_split_local.nDOF(), 7 );

  BOOST_CHECK_EQUAL( xfld_split_local.getNewNodeDOFs().size(), 1 );
  BOOST_CHECK_EQUAL( xfld_split_local.getNewNodeDOFs()[0], 6 );

  BOOST_CHECK_EQUAL( xfld_split_local.getNewLinearNodeDOFs().size(), 1 );
  BOOST_CHECK_EQUAL( xfld_split_local.getNewLinearNodeDOFs()[0], 6 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_split_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_split_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_split_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_split_local.getCellGroup<Triangle>(0);
  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup1 = xfld_split_local.getCellGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 4 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nInteriorTraceGroups(), 3 );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(1).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(2).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup0 = xfld_split_local.getInteriorTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup1 = xfld_split_local.getInteriorTraceGroup<Line>(1);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup2 = xfld_split_local.getInteriorTraceGroup<Line>(2);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1 ); // the split trace
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 4 ); // the four traces of the split element
  BOOST_CHECK_EQUAL( xfldITraceGroup2.nElem(), 1 ); // ???

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nBoundaryTraceGroups(), 0 );

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nGhostBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_split_local.getGhostBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup = xfld_split_local.getGhostBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldBTraceGroup.nElem(), 6 );

  XField_LocalGlobal_Equiv(xfld_split_local);
}
#endif

#if 1
//--------------------------------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE(Split_4Triangle_X1_1Group_InteriorCell_SplitEdge1_metadata_test)
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField2D_4Triangle_X1_1Group xfld;

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh for the central triangle
  XField_LocalPatchConstructor<PhysD2, Triangle> xfld_local(comm_local, connectivity, 0, 0);

  int split_edge_index= 1;
  XField_LocalPatch<PhysD2, Triangle> xfld_split_local(xfld_local,
                                                       ElementSplitType::Edge,
                                                       split_edge_index);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_split_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_split_local.nDOF(), 7 );

  BOOST_CHECK_EQUAL( xfld_split_local.getNewNodeDOFs().size(), 1 );
  BOOST_CHECK_EQUAL(xfld_split_local.getNewNodeDOFs()[0], 6 );

  BOOST_CHECK_EQUAL(xfld_split_local.getNewLinearNodeDOFs().size(), 1 );
  BOOST_CHECK_EQUAL(xfld_split_local.getNewLinearNodeDOFs()[0], 6 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_split_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_split_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_split_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_split_local.getCellGroup<Triangle>(0);
  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup1 = xfld_split_local.getCellGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 4 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nInteriorTraceGroups(), 3 );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(1).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(2).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup0 = xfld_split_local.getInteriorTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup1 = xfld_split_local.getInteriorTraceGroup<Line>(1);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup2 = xfld_split_local.getInteriorTraceGroup<Line>(2);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1 ); // the split trace
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 4 ); // the four traces of the split element
  BOOST_CHECK_EQUAL( xfldITraceGroup2.nElem(), 1 ); // ???

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nBoundaryTraceGroups(), 0 );

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nGhostBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_split_local.getGhostBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup = xfld_split_local.getGhostBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldBTraceGroup.nElem(), 6 );

  XField_LocalGlobal_Equiv(xfld_split_local);
}
#endif

#if 1
//--------------------------------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE(Split_4Triangle_X1_1Group_InteriorCell_SplitEdge2_metadata_test)
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField2D_4Triangle_X1_1Group xfld;

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh for the central triangle
  XField_LocalPatchConstructor<PhysD2, Triangle> xfld_local(comm_local, connectivity, 0, 0);

  int split_edge_index = 2;
  XField_LocalPatch<PhysD2, Triangle> xfld_split_local(xfld_local,
                                                       ElementSplitType::Edge,
                                                       split_edge_index);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_split_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_split_local.nDOF(), 7 );

  BOOST_CHECK_EQUAL( xfld_split_local.getNewNodeDOFs().size(), 1 );
  BOOST_CHECK_EQUAL( xfld_split_local.getNewNodeDOFs()[0], 6 );

  BOOST_CHECK_EQUAL( xfld_split_local.getNewLinearNodeDOFs().size(), 1 );
  BOOST_CHECK_EQUAL( xfld_split_local.getNewLinearNodeDOFs()[0], 6 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_split_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_split_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_split_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_split_local.getCellGroup<Triangle>(0);
  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup1 = xfld_split_local.getCellGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 4 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nInteriorTraceGroups(), 3 );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(1).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(2).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup0 = xfld_split_local.getInteriorTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup1 = xfld_split_local.getInteriorTraceGroup<Line>(1);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup2 = xfld_split_local.getInteriorTraceGroup<Line>(2);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1 ); // the split trace
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 4 ); // the four traces of the split element
  BOOST_CHECK_EQUAL( xfldITraceGroup2.nElem(), 1 ); // ???

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nBoundaryTraceGroups(), 0 );

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nGhostBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_split_local.getGhostBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup = xfld_split_local.getGhostBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldBTraceGroup.nElem(), 6 );

  XField_LocalGlobal_Equiv(xfld_split_local);
}
#endif

#if 1
//--------------------------------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE(Split_4Triangle_X2_1Group_InteriorCell_SplitEdge0_metadata_test)
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  // linear grid
  XField2D_4Triangle_X1_1Group xfld_X1;

  // promote grid to quadratic
  int order= 2;
  XField<PhysD2, TopoD2> xfld(xfld_X1, order);

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh for the central triangle
  XField_LocalPatchConstructor<PhysD2, Triangle> xfld_local(comm_local, connectivity, 0, 0);

  int split_edge_index = 0;
  XField_LocalPatch<PhysD2, Triangle> xfld_split_local(xfld_local,
                                                       ElementSplitType::Edge,
                                                       split_edge_index);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_split_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_split_local.nDOF(), 19 );

  BOOST_CHECK_EQUAL( xfld_split_local.getNewNodeDOFs().size(), 1 );
  BOOST_CHECK_EQUAL( xfld_split_local.getNewNodeDOFs()[0], 18 );

  BOOST_CHECK_EQUAL( xfld_split_local.getNewLinearNodeDOFs().size(), 1 );
  BOOST_CHECK_EQUAL( xfld_split_local.getNewLinearNodeDOFs()[0], 6 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_split_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_split_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_split_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_split_local.getCellGroup<Triangle>(0);
  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup1 = xfld_split_local.getCellGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 4 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nInteriorTraceGroups(), 3 );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(1).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(2).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup0 = xfld_split_local.getInteriorTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup1 = xfld_split_local.getInteriorTraceGroup<Line>(1);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup2 = xfld_split_local.getInteriorTraceGroup<Line>(2);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1 ); // the split trace
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 4 ); // the four traces of the split element
  BOOST_CHECK_EQUAL( xfldITraceGroup2.nElem(), 1 ); // ???

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nBoundaryTraceGroups(), 0 );

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nGhostBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_split_local.getGhostBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup = xfld_split_local.getGhostBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldBTraceGroup.nElem(), 6 );

  XField_LocalGlobal_Equiv(xfld_split_local);
}
#endif

#if 1
//--------------------------------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE(Split_4Triangle_X2_1Group_InteriorCell_SplitEdge1_metadata_test)
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  // linear grid
  XField2D_4Triangle_X1_1Group xfld_X1;

  // promote grid to quadratic
  int order= 2;
  XField<PhysD2, TopoD2> xfld(xfld_X1, order);

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh for the central triangle
  XField_LocalPatchConstructor<PhysD2, Triangle> xfld_local(comm_local, connectivity, 0, 0);

  int split_edge_index = 1;
  XField_LocalPatch<PhysD2, Triangle> xfld_split_local(xfld_local,
                                                       ElementSplitType::Edge,
                                                       split_edge_index);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_split_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_split_local.nDOF(), 19 );

  BOOST_CHECK_EQUAL( xfld_split_local.getNewNodeDOFs().size(), 1 );
  BOOST_CHECK_EQUAL( xfld_split_local.getNewNodeDOFs()[0], 18 );

  BOOST_CHECK_EQUAL( xfld_split_local.getNewLinearNodeDOFs().size(), 1 );
  BOOST_CHECK_EQUAL( xfld_split_local.getNewLinearNodeDOFs()[0], 6 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_split_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_split_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_split_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_split_local.getCellGroup<Triangle>(0);
  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup1 = xfld_split_local.getCellGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 4 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nInteriorTraceGroups(), 3 );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(1).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(2).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup0 = xfld_split_local.getInteriorTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup1 = xfld_split_local.getInteriorTraceGroup<Line>(1);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup2 = xfld_split_local.getInteriorTraceGroup<Line>(2);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1 ); // the split trace
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 4 ); // the four traces of the split element
  BOOST_CHECK_EQUAL( xfldITraceGroup2.nElem(), 1 ); // ???

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nBoundaryTraceGroups(), 0 );

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nGhostBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_split_local.getGhostBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup = xfld_split_local.getGhostBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldBTraceGroup.nElem(), 6 );

  XField_LocalGlobal_Equiv(xfld_split_local);
}
#endif

#if 1
//--------------------------------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE(Split_4Triangle_X2_1Group_InteriorCell_SplitEdge2_metadata_test)
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  // linear grid
  XField2D_4Triangle_X1_1Group xfld_X1;

  // promote grid to quadratic
  int order= 2;
  XField<PhysD2, TopoD2> xfld(xfld_X1, order);

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh for the central triangle
  XField_LocalPatchConstructor<PhysD2, Triangle> xfld_local(comm_local, connectivity, 0, 0);

  int split_edge_index = 2;
  XField_LocalPatch<PhysD2, Triangle> xfld_split_local(xfld_local,
                                                       ElementSplitType::Edge,
                                                       split_edge_index);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_split_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_split_local.nDOF(), 19 );

  BOOST_CHECK_EQUAL( xfld_split_local.getNewNodeDOFs().size(), 1 );
  BOOST_CHECK_EQUAL( xfld_split_local.getNewNodeDOFs()[0], 18 );

  BOOST_CHECK_EQUAL( xfld_split_local.getNewLinearNodeDOFs().size(), 1 );
  BOOST_CHECK_EQUAL( xfld_split_local.getNewLinearNodeDOFs()[0], 6 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_split_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_split_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_split_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_split_local.getCellGroup<Triangle>(0);
  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup1 = xfld_split_local.getCellGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 4 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nInteriorTraceGroups(), 3 );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(1).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(2).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup0 = xfld_split_local.getInteriorTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup1 = xfld_split_local.getInteriorTraceGroup<Line>(1);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup2 = xfld_split_local.getInteriorTraceGroup<Line>(2);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1 ); // the split trace
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 4 ); // the four traces of the split element
  BOOST_CHECK_EQUAL( xfldITraceGroup2.nElem(), 1 ); // ???

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nBoundaryTraceGroups(), 0 );

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nGhostBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_split_local.getGhostBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup = xfld_split_local.getGhostBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldBTraceGroup.nElem(), 6 );

  XField_LocalGlobal_Equiv(xfld_split_local);
}
#endif

#if 1
//--------------------------------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE(Split_4Triangle_X3_1Group_InteriorCell_SplitEdge0_metadata_test)
{
  const Real tol= 1e-10;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  // linear grid
  XField2D_4Triangle_X1_1Group xfld_X1;

  // promote grid to quadratic
  int order= 3;
  XField<PhysD2, TopoD2> xfld(xfld_X1, order);

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh for the central triangle
  XField_LocalPatchConstructor<PhysD2, Triangle> xfld_local(comm_local, connectivity, 0, 0);

  int split_edge_index = 0;
  XField_LocalPatch<PhysD2, Triangle> xfld_split_local(xfld_local,
                                                       ElementSplitType::Edge,
                                                       split_edge_index);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_split_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_split_local.nDOF(), 37 );

  BOOST_CHECK_EQUAL( xfld_split_local.getNewNodeDOFs().size(), 1 );
  BOOST_CHECK_EQUAL( xfld_split_local.getNewNodeDOFs()[0], 36 );

  BOOST_CHECK_EQUAL( xfld_split_local.getNewLinearNodeDOFs().size(), 1 );
  BOOST_CHECK_EQUAL( xfld_split_local.getNewLinearNodeDOFs()[0], 6 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_split_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_split_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_split_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_split_local.getCellGroup<Triangle>(0);
  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup1 = xfld_split_local.getCellGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 4 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nInteriorTraceGroups(), 3 );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(1).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(2).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup0 = xfld_split_local.getInteriorTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup1 = xfld_split_local.getInteriorTraceGroup<Line>(1);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup2 = xfld_split_local.getInteriorTraceGroup<Line>(2);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1 ); // the split trace
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 4 ); // the four traces of the split element
  BOOST_CHECK_EQUAL( xfldITraceGroup2.nElem(), 1 ); // ???

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nBoundaryTraceGroups(), 0 );

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nGhostBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_split_local.getGhostBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup = xfld_split_local.getGhostBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldBTraceGroup.nElem(), 6 );

  XField_LocalGlobal_Equiv(xfld_split_local, tol, tol);
}
#endif

#if 1
//--------------------------------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE(Split_4Triangle_X3_1Group_InteriorCell_SplitEdge1_metadata_test)
{
  const Real tol= 1e-10;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  // linear grid
  XField2D_4Triangle_X1_1Group xfld_X1;

  // promote grid to quadratic
  int order= 3;
  XField<PhysD2, TopoD2> xfld(xfld_X1, order);

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh for the central triangle
  XField_LocalPatchConstructor<PhysD2, Triangle> xfld_local(comm_local, connectivity, 0, 0);

  int split_edge_index = 1;
  XField_LocalPatch<PhysD2, Triangle> xfld_split_local(xfld_local,
                                                       ElementSplitType::Edge,
                                                       split_edge_index);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_split_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_split_local.nDOF(), 37 );

  BOOST_CHECK_EQUAL( xfld_split_local.getNewNodeDOFs().size(), 1 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_split_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_split_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_split_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_split_local.getCellGroup<Triangle>(0);
  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup1 = xfld_split_local.getCellGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 4 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nInteriorTraceGroups(), 3 );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(1).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(2).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup0 = xfld_split_local.getInteriorTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup1 = xfld_split_local.getInteriorTraceGroup<Line>(1);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup2 = xfld_split_local.getInteriorTraceGroup<Line>(2);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1 ); // the split trace
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 4 ); // the four traces of the split element
  BOOST_CHECK_EQUAL( xfldITraceGroup2.nElem(), 1 ); // ???

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nBoundaryTraceGroups(), 0 );

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nGhostBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_split_local.getGhostBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup = xfld_split_local.getGhostBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldBTraceGroup.nElem(), 6 );

  XField_LocalGlobal_Equiv(xfld_split_local, tol, tol);
}
#endif

#if 1
//--------------------------------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE(Split_4Triangle_X3_1Group_InteriorCell_SplitEdge2_metadata_test)
{
  const Real tol= 1e-10;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  // linear grid
  XField2D_4Triangle_X1_1Group xfld_X1;

  // promote grid to quadratic
  int order= 3;
  XField<PhysD2, TopoD2> xfld(xfld_X1, order);

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh for the central triangle
  XField_LocalPatchConstructor<PhysD2, Triangle> xfld_local(comm_local, connectivity, 0, 0);

  int split_edge_index = 2;
  XField_LocalPatch<PhysD2, Triangle> xfld_split_local(xfld_local,
                                                       ElementSplitType::Edge,
                                                       split_edge_index);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_split_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_split_local.nDOF(), 37 );

  BOOST_CHECK_EQUAL( xfld_split_local.getNewNodeDOFs().size(), 1 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_split_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_split_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_split_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_split_local.getCellGroup<Triangle>(0);
  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup1 = xfld_split_local.getCellGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 4 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nInteriorTraceGroups(), 3 );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(1).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(2).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup0 = xfld_split_local.getInteriorTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup1 = xfld_split_local.getInteriorTraceGroup<Line>(1);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup2 = xfld_split_local.getInteriorTraceGroup<Line>(2);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1 ); // the split trace
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 4 ); // the four traces of the split element
  BOOST_CHECK_EQUAL( xfldITraceGroup2.nElem(), 1 ); // ???

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nBoundaryTraceGroups(), 0 );

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nGhostBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_split_local.getGhostBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup = xfld_split_local.getGhostBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldBTraceGroup.nElem(), 6 );

  XField_LocalGlobal_Equiv(xfld_split_local, tol, tol);
}
#endif

#if 1
//--------------------------------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_2Triangle_X1_1Group_LeftCell_SplitEdge0_metadata_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField2D_2Triangle_X1_1Group xfld;

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh for the bottom-left triangle
  XField_LocalPatchConstructor<PhysD2, Triangle> xfld_local(comm_local, connectivity, 0, 0);

  int split_edge_index = 0;
  XField_LocalPatch<PhysD2, Triangle> xfld_split_local(xfld_local,
                                                       ElementSplitType::Edge,
                                                       split_edge_index);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_split_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_split_local.nDOF(), 5 );

  BOOST_CHECK_EQUAL( xfld_split_local.getNewNodeDOFs().size(), 1 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_split_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_split_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_split_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_split_local.getCellGroup<Triangle>(0);
  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup1 = xfld_split_local.getCellGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 2 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nInteriorTraceGroups(), 3 );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(1).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(2).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup0 = xfld_split_local.getInteriorTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup1 = xfld_split_local.getInteriorTraceGroup<Line>(1);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup2 = xfld_split_local.getInteriorTraceGroup<Line>(2);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1 ); // the interface of the split
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 2 ); // the other interfaces of the local grid
  BOOST_CHECK_EQUAL( xfldITraceGroup2.nElem(), 1 ); // the interface of the mirrored split element

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_split_local.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup0 = xfld_split_local.getBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 2 );

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nGhostBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_split_local.getGhostBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldGBTraceGroup0 = xfld_split_local.getGhostBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 2 );

  XField_LocalGlobal_Equiv(xfld_split_local);
}
#endif

#if 1
//--------------------------------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_2Triangle_X1_1Group_LeftCell_SplitEdge1_metadata_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField2D_2Triangle_X1_1Group xfld;

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh for the bottom-left triangle
  XField_LocalPatchConstructor<PhysD2, Triangle> xfld_local(comm_local, connectivity, 0, 0);

  int split_edge_index = 1;
  XField_LocalPatch<PhysD2, Triangle> xfld_split_local(xfld_local,
                                                       ElementSplitType::Edge,
                                                       split_edge_index);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_split_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_split_local.nDOF(), 5 );

  BOOST_CHECK_EQUAL( xfld_split_local.getNewNodeDOFs().size(), 1 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_split_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_split_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_split_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_split_local.getCellGroup<Triangle>(0);
  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup1 = xfld_split_local.getCellGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 1 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nInteriorTraceGroups(), 2 );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(1).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup0 = xfld_split_local.getInteriorTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup1 = xfld_split_local.getInteriorTraceGroup<Line>(1);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1 ); // the interface of the split
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 1 ); // the other interfaces of the local grid

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_split_local.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup0 = xfld_split_local.getBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 3 );

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nGhostBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_split_local.getGhostBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldGBTraceGroup0 = xfld_split_local.getGhostBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 2 );

  XField_LocalGlobal_Equiv(xfld_split_local);
}
#endif

#if 1
//--------------------------------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_2Triangle_X1_1Group_LeftCell_SplitEdge2_metadata_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField2D_2Triangle_X1_1Group xfld;

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh for the bottom-left triangle
  XField_LocalPatchConstructor<PhysD2, Triangle> xfld_local(comm_local, connectivity, 0, 0);

  int split_edge_index = 2;
  XField_LocalPatch<PhysD2, Triangle> xfld_split_local(xfld_local,
                                                       ElementSplitType::Edge,
                                                       split_edge_index);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_split_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_split_local.nDOF(), 5 );

  BOOST_CHECK_EQUAL( xfld_split_local.getNewNodeDOFs().size(), 1 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_split_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_split_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_split_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_split_local.getCellGroup<Triangle>(0);
  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup1 = xfld_split_local.getCellGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 1 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nInteriorTraceGroups(), 2 );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(1).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup0 = xfld_split_local.getInteriorTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup1 = xfld_split_local.getInteriorTraceGroup<Line>(1);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1 ); // the interface of the split
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 1 ); // the other interfaces of the local grid

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_split_local.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup0 = xfld_split_local.getBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 3 );

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nGhostBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_split_local.getGhostBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldGBTraceGroup0 = xfld_split_local.getGhostBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 2 );

  XField_LocalGlobal_Equiv(xfld_split_local);
}
#endif

#if 1
//--------------------------------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_2Triangle_X1_1Group_RightCell_SplitEdge0_metadata_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField2D_2Triangle_X1_1Group xfld;

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh for the bottom-left triangle
  XField_LocalPatchConstructor<PhysD2, Triangle> xfld_local(comm_local, connectivity, 0, 1);

  int split_edge_index = 0;
  XField_LocalPatch<PhysD2, Triangle> xfld_split_local(xfld_local,
                                                       ElementSplitType::Edge,
                                                       split_edge_index);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_split_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_split_local.nDOF(), 5 );

  BOOST_CHECK_EQUAL( xfld_split_local.getNewNodeDOFs().size(), 1 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_split_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_split_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_split_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_split_local.getCellGroup<Triangle>(0);
  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup1 = xfld_split_local.getCellGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 2 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nInteriorTraceGroups(), 3 );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(1).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(2).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup0 = xfld_split_local.getInteriorTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup1 = xfld_split_local.getInteriorTraceGroup<Line>(1);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup2 = xfld_split_local.getInteriorTraceGroup<Line>(2);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1 ); // the interface of the split
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 2 ); // the other interfaces of the local grid
  BOOST_CHECK_EQUAL( xfldITraceGroup2.nElem(), 1 ); // the interface of the mirrored split element

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_split_local.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup0 = xfld_split_local.getBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 2 );

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nGhostBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_split_local.getGhostBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldGBTraceGroup0 = xfld_split_local.getGhostBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 2 );

  XField_LocalGlobal_Equiv(xfld_split_local);
}
#endif

#if 1
//--------------------------------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_2Triangle_X1_1Group_RightCell_SplitEdge1_metadata_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField2D_2Triangle_X1_1Group xfld;

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh for the bottom-left triangle
  XField_LocalPatchConstructor<PhysD2, Triangle> xfld_local(comm_local, connectivity, 0, 1);

  int split_edge_index = 1;
  XField_LocalPatch<PhysD2, Triangle> xfld_split_local(xfld_local,
                                                       ElementSplitType::Edge,
                                                       split_edge_index);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_split_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_split_local.nDOF(), 5 );

  BOOST_CHECK_EQUAL( xfld_split_local.getNewNodeDOFs().size(), 1 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_split_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_split_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_split_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_split_local.getCellGroup<Triangle>(0);
  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup1 = xfld_split_local.getCellGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 1 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nInteriorTraceGroups(), 2 );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(1).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup0 = xfld_split_local.getInteriorTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup1 = xfld_split_local.getInteriorTraceGroup<Line>(1);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1 ); // the interface of the split
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 1 ); // the other interfaces of the local grid

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_split_local.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup0 = xfld_split_local.getBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 3 );

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nGhostBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_split_local.getGhostBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldGBTraceGroup0 = xfld_split_local.getGhostBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 2 );

  XField_LocalGlobal_Equiv(xfld_split_local);
}
#endif

#if 1
//--------------------------------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_2Triangle_X1_1Group_RightCell_SplitEdge2_metadata_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField2D_2Triangle_X1_1Group xfld;

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  //Extract the local mesh for the bottom-left triangle
  XField_LocalPatchConstructor<PhysD2, Triangle> xfld_local(comm_local, connectivity, 0, 1);

  int split_edge_index = 2;
  XField_LocalPatch<PhysD2, Triangle> xfld_split_local(xfld_local,
                                                       ElementSplitType::Edge,
                                                       split_edge_index);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_split_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_split_local.nDOF(), 5 );

  BOOST_CHECK_EQUAL( xfld_split_local.getNewNodeDOFs().size(), 1 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld_split_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_split_local.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_split_local.getCellGroupBase(1).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup0 = xfld_split_local.getCellGroup<Triangle>(0);
  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldCellGroup1 = xfld_split_local.getCellGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 1 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nInteriorTraceGroups(), 2 );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld_split_local.getInteriorTraceGroup<Line>(1).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup0 = xfld_split_local.getInteriorTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldITraceGroup1 = xfld_split_local.getInteriorTraceGroup<Line>(1);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1 ); // the interface of the split
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 1 ); // the other interfaces of the local grid

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_split_local.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBTraceGroup0 = xfld_split_local.getBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 3 );

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_split_local.nGhostBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_split_local.getGhostBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldGBTraceGroup0 = xfld_split_local.getGhostBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 2 );

  XField_LocalGlobal_Equiv(xfld_split_local);
}
#endif

#if 1
//--------------------------------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_4Triangle_X1_1Group_Lagrange_metadata_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField2D_4Triangle_X1_1Group xfld(BasisFunctionCategory_Lagrange);

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  int nElem = xfld.nElem();

  // //Get the list of local split configurations for this topology - including uniform refinement
  // std::vector<LocalSplitConfig> split_config_list = LocalSplitConfigList<Triangle>::get(true);

  // int Nconfig = (int) split_config_list.size(); //Number of split configurations
  BOOST_CHECK_EQUAL(xfld.nInteriorTraceGroups(), 1);
  int Nconfig= xfld.getInteriorTraceGroup<Line>(0).nElem();

  for (int elem = 0; elem < nElem; elem++)
  {
    //Extract the local mesh for current element
    XField_LocalPatchConstructor<PhysD2,Triangle> xfld_local(comm_local, connectivity, 0, elem);

    for (int config = 0; config < Nconfig; config++)
    {
      ElementSplitType split_type= ElementSplitType::Edge;
      int split_edge_index= config;

      //XField's checkGrid() does a series of validity checks after the construction
      XField_LocalPatch<PhysD2, Triangle> xfld_split_local(xfld_local, split_type, split_edge_index);

      BOOST_CHECK_EQUAL( &connectivity, &xfld_split_local.connectivity() );

      XField_LocalGlobal_Equiv(xfld_split_local);
    }
  }
}
#endif

#if 1
//--------------------------------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Annulus_Triangle_X3_metadata_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  const Real tol= 1e-8;

  const int ii = 5;
  const int jj = 5;
  const Real R1 = 1.0;         // R_1: inner radius
  const Real h = 1.0;          // thickness. Must be 1
  const Real thetamin = 0.0;   // degree angle
  const Real thetamax = 135.0;  // degree angle

  XField2D_Annulus_Triangle_X1 xfld_X1( ii, jj, R1, h, thetamin, thetamax );

  XField<PhysD2,TopoD2> xfld(xfld_X1, 3); //Construct Q3 mesh from linear mesh

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  int nElem = xfld.nElem();

  int Nconfig= Triangle::NEdge;

  for (int elem = 0; elem < nElem; elem++)
  {
    //Extract the local mesh for current element
    XField_LocalPatchConstructor<PhysD2, Triangle> xfld_local(comm_local, connectivity, 0, elem);

    for (int config = 0; config < Nconfig; config++)
    {
      ElementSplitType split_type= ElementSplitType::Edge;
      int split_edge_index= config;;

      //XField's checkGrid() does a series of validity checks after the construction
      XField_LocalPatch<PhysD2, Triangle> xfld_split_local(xfld_local, split_type, split_edge_index);

      BOOST_CHECK_EQUAL( &connectivity, &xfld_split_local.connectivity() );

      XField_LocalGlobal_Equiv(xfld_split_local, tol, tol);
    }
  }
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_WholeGrid_Box_Triangle_test )
{
  typedef typename XField<PhysD2, TopoD2>::template FieldCellGroupType<Triangle> XFieldCellGroupType;

  mpi::communicator world;
  mpi::communicator comm_local= world.split(world.rank());

  int size= 3*3*world.size();
  int ii= sqrt(size);
  int jj= ii;

  XField2D_Box_Triangle_Lagrange_X1 xfld(world, ii, jj);

  // Build cell to trace connectivity structure
  XField_CellToTrace<PhysD2, TopoD2> connectivity(xfld);

  for (int group = 0; group < xfld.nCellGroups(); group++)
  {
    const XFieldCellGroupType& xfldCellGroup = xfld.template getCellGroup<Triangle>(group);

    for (int elem = 0; elem < xfld.getCellGroup<Triangle>(group).nElem(); elem++)
    {
      // only consider possessed elements
      if (xfldCellGroup.associativity(elem).rank() != world.rank()) continue;

      // Construct unsplit grid
      XField_LocalPatchConstructor<PhysD2, Triangle> xfld_construct(comm_local, connectivity, group, elem, SpaceType::Discontinuous);

      for (int edge= 0; edge < Triangle::NEdge; edge++)
      {
        XField_LocalPatch<PhysD2, Triangle> xfld_local(xfld_construct, ElementSplitType::Edge, edge);

        BOOST_CHECK_EQUAL(&connectivity, &xfld_local.connectivity());

        // check that all local elements match the global elements
        XField_LocalGlobal_Equiv(xfld_local);
      }
    }
  }
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
