// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
//
// Note: unit grids tested in "unit/UnitGrids/UnitGrids_btest.cpp"

#include <ostream>
#include <utility> // std::pair
#include <set>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "Topology/ElementTopology.h"

#include "BasisFunction/ElementEdges.h"

#include "Field/Field_NodalView.h"
#include "Field/XField_CellToTrace.h"
#include "Field/Local/XField_LocalPatch.h"

#include "Meshing/XField1D/XField1D.h"

#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField1D_2Line_X1_1Group.h"
#include "unit/UnitGrids/XField1D_4Line_X1_2Group.h"

#include "XField_LocalGlobal_Equiv_btest.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( XField1D_Local_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField1D_LocalPatch_1Line_Unsplit_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField1D_2Line_X1_1Group xfld;

  // build cell to trace connectivity structure
  XField_CellToTrace<PhysD1,TopoD1> connectivity(xfld);

  int group = 0, elem = 0;

  //Extract the local mesh for the central tet
  XField_LocalPatchConstructor<PhysD1,Line> xfld_local(comm_local, connectivity, group, elem, SpaceType::Discontinuous);

  // create an unsplit patch
  XField_LocalPatch<PhysD1,Line> xfld_unsplit( xfld_local );

  //Check the element count
  BOOST_CHECK_EQUAL( xfld_unsplit.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfld_unsplit.nCellGroups(), 2 );
  BOOST_CHECK_EQUAL( xfld_unsplit.nInteriorTraceGroups(), 1 );
  BOOST_CHECK_EQUAL( xfld_unsplit.nBoundaryTraceGroups(), 1 );
  BOOST_CHECK_EQUAL( xfld_unsplit.nGhostBoundaryTraceGroups(), 1 );

  for (int group = 0; group < xfld_unsplit.nCellGroups(); group++)
  {
    typedef typename XField<PhysD1, TopoD1>::template FieldCellGroupType<Line> XFieldCellGroupType;
    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

    XFieldCellGroupType& cellGroup = xfld_unsplit.template getCellGroup<Line>(group);
    ElementXFieldClass xfldElem( cellGroup.basis() );

    for (int elem = 0; elem < cellGroup.nElem(); elem++)
    {
      // retrieve the split info
      ElementSplitInfo info = xfld_unsplit.getCellSplitInfo({group,elem});
      BOOST_CHECK_EQUAL( (int)ElementSplitFlag::Unsplit, (int)info.split_flag );
    }
  }

  for (int group = 0; group < xfld_unsplit.nInteriorTraceGroups(); group++)
  {
    typedef typename XField<PhysD1, TopoD1>::template FieldTraceGroupType<Node> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldClass;

    XFieldTraceGroupType& traceGroup = xfld_unsplit.template getInteriorTraceGroup<Node>(group);
    ElementXFieldClass xfldElem( traceGroup.basis() );

    BOOST_CHECK_EQUAL( traceGroup.nElem(), 1 );

    for (int elem = 0; elem < traceGroup.nElem(); elem++)
    {
      // retrieve the split info
      ElementSplitInfo info = xfld_unsplit.getInteriorTraceSplitInfo({group,elem});
      BOOST_CHECK_EQUAL( (int)ElementSplitFlag::Unsplit, (int)info.split_flag );
    }
  }

  for (int group = 0; group < xfld_unsplit.nBoundaryTraceGroups(); group++)
  {
    typedef typename XField<PhysD1, TopoD1>::template FieldTraceGroupType<Node> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldClass;

    XFieldTraceGroupType& traceGroup = xfld_unsplit.template getBoundaryTraceGroup<Node>(group);
    ElementXFieldClass xfldElem( traceGroup.basis() );

    BOOST_CHECK_EQUAL( traceGroup.nElem(), 1 );

    for (int elem = 0; elem < traceGroup.nElem(); elem++)
    {
      // retrieve the split info
      ElementSplitInfo info = xfld_unsplit.getBoundaryTraceSplitInfo({group,elem});
      BOOST_CHECK_EQUAL( (int)ElementSplitFlag::Unsplit, (int)info.split_flag );
    }
  }

  for (int group = 0; group < xfld_unsplit.nGhostBoundaryTraceGroups(); group++)
  {
    typedef typename XField<PhysD1, TopoD1>::template FieldTraceGroupType<Node> XFieldTraceGroupType;

    const XFieldTraceGroupType& traceGroup = xfld_unsplit.template getGhostBoundaryTraceGroup<Node>(group);

    BOOST_CHECK_EQUAL( traceGroup.nElem(), 1 );
  }

  // check that all elements match the global elements
  XField_LocalGlobal_Equiv(xfld_unsplit);
}

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField1D_LocalPatch_1Triangle_1Group_InteriorCell_Target_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField1D_1Line_X1_1Group xfld;

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD1,TopoD1> connectivity(xfld);

  int group = 0, elem = 0;

  //Extract the local mesh for the central triangle
  XField_LocalPatchConstructor<PhysD1,Line> xfld_local(comm_local, xfld, connectivity);

  xfld_local.addResolveElement( group, elem );
  xfld_local.extract();

  // create a split patch and split edge (0,1)
  XField_LocalPatch<PhysD1,Line> xfld_split( xfld_local, 0, 1 );

  // check that all elements match the global elements
  XField_LocalGlobal_Equiv(xfld_split);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField1D_LocalPatch_4Line_1Group_InteriorCell_Target_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField1D_4Line_X1_2Group xfld;

  // build cell to trace connectivity structure
  XField_CellToTrace<PhysD1,TopoD1> connectivity(xfld);

  int group = 0, elem = 1;

  //Extract the local mesh for the central tet
  XField_LocalPatchConstructor<PhysD1,Line> xfld_local(comm_local, xfld, connectivity);

  xfld_local.addResolveElement( group, elem );
  xfld_local.addFixedElement( 0, 0 );
  xfld_local.addFixedElement( 1, 0 );

  xfld_local.extract();

  // create a split patch and split edge (0,1)
  XField_LocalPatch<PhysD1,Line> xfld_split( xfld_local, 0, 1 );

  // check that all elements match the global elements
  XField_LocalGlobal_Equiv(xfld_split);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField1D_LocalPatch_EdgePatch_Target_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField1D xfld(5);

  typedef Line Topology;
  typedef TopoD1 TopoDim;
  typedef PhysD1 PhysDim;
  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
  const int nEdge = Topology::NEdge; // Number of different edges

  typedef typename std::set<std::array<int,Line::NNode>> EdgeSet;
  EdgeSet edge_set;

  const int (*EdgeNodes)[Line::NNode] = ElementEdges<Topology>::EdgeNodes;

  std::array<int,Topology::NNode> nodeMap;

  for ( int group = 0; group < xfld.nCellGroups(); group++ )
  {
    const XFieldCellGroupType& xfldCellGroup = xfld.template getCellGroupGlobal<Topology>(group);
    const int nElem = xfldCellGroup.nElem();
    for ( int elem = 0; elem < nElem; elem++ )
    {
      xfldCellGroup.associativity(elem).getNodeGlobalMapping( nodeMap.data(), nodeMap.size() );

      for ( int edge = 0; edge < nEdge; edge++ )
      {
        // loop over the edges
        std::array<int,Line::NNode> newEdge;
        newEdge[0] = std::min(nodeMap[EdgeNodes[edge][0]],nodeMap[EdgeNodes[edge][1]]);
        newEdge[1] = std::max(nodeMap[EdgeNodes[edge][0]],nodeMap[EdgeNodes[edge][1]]);

        edge_set.insert(newEdge);
      }
    }
  }


  // build cell to trace connectivity structure
  XField_CellToTrace<PhysD1,TopoD1> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  for ( const auto& edge : edge_set )
  {
    //Extract the local mesh for the target edge
    // extract the split version because it makes an unsplit internally as it goes
    XField_LocalPatch<PhysD1,Line> xfld_split(comm_local,connectivity,edge,SpaceType::Continuous,&nodalview);

    // check that all elements match the global elements
    XField_LocalGlobal_Equiv(xfld_split);
  }

}
#endif

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_LocalPatchAddNeighbours_CKF_InteriorCell_Target_test )
{
  #ifdef SANS_AVRO

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  //XField4D_6Ptope_X1_1Group_NegativeTraceOrientation xfld(comm_local);
  //xfld.dump(1);
  XField_KuhnFreudenthal<PhysD2,TopoD2> xfld( comm_local, {5,5} );

//  xfld.dump(1);

  // build cell to trace connectivity structure
  XField_CellToTrace<PhysD2,TopoD2> connectivity(xfld);

  // build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  time_t t0 = clock();
  int nsplit = 0;
  for (int group = 0; group < xfld.nCellGroups(); group++)
  {
    for (int elem = 0; elem < xfld.getCellGroupBase(group).nElem(); elem++)
    {
      // extract the local mesh for the central tet
      XField_LocalPatchConstructor<PhysD2,Triangle> xfld_local(comm_local,xfld,connectivity,&nodalview);

      xfld_local.addResolveElement( group, elem );
      //xfld_local.addSharedTraceNeighbours();
      xfld_local.addSharedNodeNeighbours();

      xfld_local.extract();

      // get the cell dof
      std::vector<int> cellDOF( Triangle::NNode, -1 );
      xfld.getCellGroup<Triangle>(group).associativity(elem).getNodeGlobalMapping( cellDOF.data(), cellDOF.size() );

      // split every edge
      const int (*edges)[Line::NNode] = ElementEdges<Triangle>::EdgeNodes;
      for (int edge = 0; edge < Triangle::NEdge; edge++)
      {
        int e0 = cellDOF[ edges[edge][0] ];
        int e1 = cellDOF[ edges[edge][1] ];

        int E[2] = {e0,e1};
        XField_LocalPatch<PhysD2,Triangle> xfld_split( xfld_local, E );
        nsplit++;

        // check that all elements match the global elements
        XField_LocalGlobal_Equiv(xfld_split);
      }
    }
  }

  printf("performed nsplit = %d in %g seconds\n",nsplit,double(clock()-t0)/CLOCKS_PER_SEC);

  #endif
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
