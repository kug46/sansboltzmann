// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Field2D_EdgeLocal_CG_btest
//

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
// #include "BasisFunction/BasisFunctionArea_Triangle.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"

#include "Field_LocalPatch_toolkit_btest.h"

#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"
#include "unit/UnitGrids/XField2D_Box_UnionJack_Triangle_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

//Explicitly instantiate classes to get proper coverage information
namespace SANS
{
template class Field_CG_Cell< PhysD2, TopoD2, Real >;
template class Field_CG_BoundaryTrace< PhysD2, TopoD2, Real >;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( Field2D_LocalPatch_Edge_CG_test_suite )

//============================================================================//
// Hierarchical
//============================================================================//

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_4Triangle_Hierarchical_P1 )
{
  XField2D_4Triangle_X1_1Group xfld;

  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = false;
  localEdgeExtractAndTransfer<PhysD2,TopoD2>( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_4Triangle_Hierarchical_P1 )
{
  XField2D_4Triangle_X1_1Group xfld;

  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = true;
  localEdgeExtractAndTransfer<PhysD2,TopoD2>( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_4Triangle_Hierarchical_P3 )
{
  XField2D_4Triangle_X1_1Group xfld;

  int order = 3;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = false;
  localEdgeExtractAndTransfer<PhysD2,TopoD2>( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_4Triangle_Hierarchical_P3 )
{
  XField2D_4Triangle_X1_1Group xfld;

  int order = 3;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = true;
  localEdgeExtractAndTransfer<PhysD2,TopoD2>( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_1Box_Triangle_Hierarchical_P1 )
{
  mpi::communicator world;

  int size = 4*4*world.size();

  int ii = sqrt(size);
  int jj = ii;
  XField2D_Box_Triangle_Lagrange_X1 xfld( world, ii, jj );

  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = false;
  localEdgeExtractAndTransfer<PhysD2,TopoD2>( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_1Box_Triangle_Hierarchical_P1 )
{
  mpi::communicator world;

  int size = 4*4*world.size();

  int ii = sqrt(size);
  int jj = ii;
  XField2D_Box_Triangle_Lagrange_X1 xfld( world, ii, jj );

  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = true;
  localEdgeExtractAndTransfer<PhysD2,TopoD2>( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_1Box_Triangle_Hierarchical_P2 )
{
  mpi::communicator world;

  int size = 4*4*world.size();

  int ii = sqrt(size);
  int jj = ii;
  XField2D_Box_Triangle_Lagrange_X1 xfld( world, ii, jj );

  int order = 2;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = false;
  localEdgeExtractAndTransfer<PhysD2,TopoD2>( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_1Box_Triangle_Hierarchical_P2 )
{
  mpi::communicator world;

  int size = 4*4*world.size();

  int ii = sqrt(size);
  int jj = ii;
  XField2D_Box_Triangle_Lagrange_X1 xfld( world, ii, jj );

  int order = 2;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = true;
  localEdgeExtractAndTransfer<PhysD2,TopoD2>( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_1Box_Triangle_Hierarchical_P3 )
{
  mpi::communicator world;

  int size = 4*4*world.size();

  int ii = sqrt(size);
  int jj = ii;
  XField2D_Box_Triangle_Lagrange_X1 xfld( world, ii, jj );

  int order = 3;
  const Real small_tol = 1e-9, close_tol = 1e-9;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = false;
  localEdgeExtractAndTransfer<PhysD2,TopoD2>( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_1Box_Triangle_Hierarchical_P3 )
{
  mpi::communicator world;

  int size = 4*4*world.size();

  int ii = sqrt(size);
  int jj = ii;
  XField2D_Box_Triangle_Lagrange_X1 xfld( world, ii, jj );

  int order = 3;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = true;
  localEdgeExtractAndTransfer<PhysD2,TopoD2>( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

//============================================================================//
// Lagrange
//============================================================================//

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_1Box_Triangle_Lagrange_P1 )
{
  mpi::communicator world;

  int size = 4*4*world.size();

  int ii = sqrt(size);
  int jj = ii;
  XField2D_Box_Triangle_Lagrange_X1 xfld( world, ii, jj );

  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = false;
  localEdgeExtractAndTransfer<PhysD2,TopoD2>( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_1Box_Triangle_Lagrange_P1 )
{
  mpi::communicator world;

  int size = 4*4*world.size();

  int ii = sqrt(size);
  int jj = ii;
  XField2D_Box_Triangle_Lagrange_X1 xfld( world, ii, jj );

  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = true;
  localEdgeExtractAndTransfer<PhysD2,TopoD2>( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_1Box_Triangle_Lagrange_P2 )
{
  mpi::communicator world;

  int size = 4*4*world.size();

  int ii = sqrt(size);
  int jj = ii;
  XField2D_Box_Triangle_Lagrange_X1 xfld( world, ii, jj );

  int order = 2;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = false;
  localEdgeExtractAndTransfer<PhysD2,TopoD2>( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_1Box_Triangle_Lagrange_P2 )
{
  mpi::communicator world;

  int size = 4*4*world.size();

  int ii = sqrt(size);
  int jj = ii;
  XField2D_Box_Triangle_Lagrange_X1 xfld( world, ii, jj );

  int order = 2;
  const Real small_tol = 5e-10, close_tol = 5e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = true;
  localEdgeExtractAndTransfer<PhysD2,TopoD2>( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_1Box_Triangle_Lagrange_P3 )
{
  mpi::communicator world;

  int size = 4*4*world.size();

  int ii = sqrt(size);
  int jj = ii;
  XField2D_Box_Triangle_Lagrange_X1 xfld( world, ii, jj );

  int order = 3;
  const Real small_tol = 5e-10, close_tol = 5e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = false;
  localEdgeExtractAndTransfer<PhysD2,TopoD2>( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_1Box_Triangle_Lagrange_P3 )
{
  mpi::communicator world;

  int size = 4*4*world.size();

  int ii = sqrt(size);
  int jj = ii;
  XField2D_Box_Triangle_Lagrange_X1 xfld( world, ii, jj );

  int order = 3;
  const Real small_tol = 5e-9, close_tol = 5e-9;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = true;
  localEdgeExtractAndTransfer<PhysD2,TopoD2>( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

// UNION JACK TESTS

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_UnionJack_Hierarchical_P1 )
{
  int ii = 4;
  int jj = ii;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);

  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = false;
  localEdgeExtractAndTransfer<PhysD2,TopoD2>( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_UnionJack_Hierarchical_P1 )
{
  int ii = 4;
  int jj = ii;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);

  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = true;
  localEdgeExtractAndTransfer<PhysD2,TopoD2>( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_UnionJack_Hierarchical_P2 )
{
  int ii = 4;
  int jj = ii;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);

  int order = 2;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = false;
  localEdgeExtractAndTransfer<PhysD2,TopoD2>( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_UnionJack_Hierarchical_Split_P2 )
{
  int ii = 4;
  int jj = ii;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);

  int order = 2;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = true;
  localEdgeExtractAndTransfer<PhysD2,TopoD2>( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_UnionJack_Hierarchical_P3 )
{
  int ii = 4;
  int jj = ii;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);

  int order = 3;
  const Real small_tol = 1e-9, close_tol = 1e-9;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = false;
  localEdgeExtractAndTransfer<PhysD2,TopoD2>( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_UnionJack_Hierarchical_Split_P3 )
{
  int ii = 4;
  int jj = ii;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);

  int order = 3;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = true;
  localEdgeExtractAndTransfer<PhysD2,TopoD2>( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_UnionJack_Lagrange_P1 )
{
  int ii = 4;
  int jj = ii;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);

  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = false;
  localEdgeExtractAndTransfer<PhysD2,TopoD2>( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_UnionJack_Lagrange_P1 )
{
  int ii = 4;
  int jj = ii;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);

  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = true;
  localEdgeExtractAndTransfer<PhysD2,TopoD2>( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_UnionJack_Lagrange_P2 )
{
  int ii = 4;
  int jj = ii;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj,0,ii,0,jj);

  int order = 2;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = false;
  localEdgeExtractAndTransfer<PhysD2,TopoD2>( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_UnionJack_Lagrange_P2 )
{
  int ii = 4;
  int jj = ii;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);

  int order = 2;
  const Real small_tol = 5e-10, close_tol = 5e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = true;
  localEdgeExtractAndTransfer<PhysD2,TopoD2>( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_UnionJack_Lagrange_P3 )
{
  int ii = 4;
  int jj = ii;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);

  int order = 3;
  const Real small_tol = 5e-10, close_tol = 5e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = false;
  localEdgeExtractAndTransfer<PhysD2,TopoD2>( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_UnionJack_Lagrange_P3 )
{
  int ii = 4;
  int jj = ii;
  XField2D_Box_UnionJack_Triangle_X1 xfld(ii,jj);

  int order = 3;
  const Real small_tol = 5e-10, close_tol = 5e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = true;
  localEdgeExtractAndTransfer<PhysD2,TopoD2>( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif



//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
