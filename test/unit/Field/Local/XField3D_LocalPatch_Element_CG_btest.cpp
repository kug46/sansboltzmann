// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"     // Real
#include "Topology/ElementTopology.h"

#include "Field/XField_CellToTrace.h"
#include "Field/Local/XField_LocalPatch.h"
#include "Field/Field_NodalView.h"

#include "unit/UnitGrids/XField3D_1Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_2Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_6Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_5Tet_X1_1Group_AllOrientations.h"
#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"

#include "XField_LocalGlobal_Equiv_btest.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( XField3D_LocalPatch_ElementDual_test_suite )

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_1Tet_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField3D_1Tet_X1_1Group xfld;

  // Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // connecting nodes
  const int group = 0, elem = 0;

  //Extract the local mesh for the bottom-left triangle
  XField_LocalPatchConstructor<PhysD3,Tet> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Continuous,&nodalview);
  XField_LocalPatch<PhysD3,Tet> xfld_local(xfld_construct);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 4 );

  int vertices[4][3] = {{ 0, 0, 0}, // node 3 from the global
                        { 1, 0, 0},
                        { 0, 1, 0},
                        { 0, 0, 1}};

  //Check the node values
  for (int k = 0; k < xfld_local.nDOF(); k++)
    for (int d = 0; d < PhysD3::D; d++)
      BOOST_CHECK_EQUAL( xfld_local.DOF(k)[d], vertices[k][d] );

  // volume field variable

  BOOST_REQUIRE_EQUAL( xfld_local.nCellGroups(), 2 );
  for (int i = 0; i < xfld_local.nCellGroups(); i++)
    BOOST_REQUIRE( xfld_local.getCellGroupBase(i).topoTypeID() == typeid(Tet) );

  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup0 = xfld_local.getCellGroup<Tet>(0);
  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup1 = xfld_local.getCellGroup<Tet>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 0 );

  // interior-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nInteriorTraceGroups(), 0 );

  // boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nBoundaryTraceGroups(), 4 );

  for (int i = 0; i < xfld_local.nBoundaryTraceGroups(); i++)
    BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Triangle>(i).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup0  = xfld_local.getBoundaryTraceGroup<Triangle>(0);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup1  = xfld_local.getBoundaryTraceGroup<Triangle>(1);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup2  = xfld_local.getBoundaryTraceGroup<Triangle>(2);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup3  = xfld_local.getBoundaryTraceGroup<Triangle>(3);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.nElem(), 1 );

  // ghost boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 ); // Another quirk of the local patch

  for (int i = 0; i < xfld_local.nGhostBoundaryTraceGroups(); i++)
    BOOST_REQUIRE( xfld_local.getGhostBoundaryTraceGroup<Triangle>(i).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldGBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 0 );

  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_5Tet_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField3D_5Tet_X1_1Group_AllOrientations xfld(-1);

  // Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // connecting nodes
  const int group = 0, elem = 0;

  //Extract the local mesh for the bottom-left triangle
  XField_LocalPatchConstructor<PhysD3,Tet> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Continuous,&nodalview);
  XField_LocalPatch<PhysD3,Tet> xfld_local(xfld_construct);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 8 );

  int vertices[8][3] = {{ 0, 0, 0},
                        { 1, 0, 0},
                        { 0, 1, 0},
                        { 0, 0, 1},
                        {-1, 0, 0},
                        { 0,-1, 0},
                        { 0, 0,-1},
                        { 1, 1, 1}};

  //Check the node values
  for (int k = 0; k < xfld_local.nDOF(); k++)
    for (int d = 0; d < PhysD3::D; d++)
      BOOST_CHECK_EQUAL( xfld_local.DOF(k)[d], vertices[k][d] );

  // volume field variable

  BOOST_REQUIRE_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Tet) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Tet) );

  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup0 = xfld_local.getCellGroup<Tet>(0);
  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup1 = xfld_local.getCellGroup<Tet>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 4 );

  // interior-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup0 = xfld_local.getInteriorTraceGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 4 );

  // boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nBoundaryTraceGroups(), 1 );

  for (int i = 0; i < xfld_local.nBoundaryTraceGroups(); i++)
    BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Triangle>(i).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup0  = xfld_local.getBoundaryTraceGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 12 );

  // ghost boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 ); // Another quirk of the local patch

  for (int i = 0; i < xfld_local.nGhostBoundaryTraceGroups(); i++)
    BOOST_REQUIRE( xfld_local.getGhostBoundaryTraceGroup<Triangle>(i).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldGBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 0 );


  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unspli_6Tet_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField3D_6Tet_X1_1Group xfld;

  // Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // connecting nodes
  const int group = 0, elem = 0;

  //Extract the local mesh for the bottom-left triangle
  XField_LocalPatchConstructor<PhysD3,Tet> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Continuous,&nodalview);
  XField_LocalPatch<PhysD3,Tet> xfld_local(xfld_construct);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 7 );

  int vertices[7][3] = {{ 0, 0, 0},
                        { 1, 0, 0},
                        { 0, 1, 0},
                        { 0, 0, 1},
                        { 1, 1, 0},
                        { 1, 0, 1},
                        { 0, 1, 1}};

  //Check the node values
  for (int k = 0; k < xfld_local.nDOF(); k++)
    for (int d = 0; d < PhysD3::D; d++)
      BOOST_CHECK_EQUAL( xfld_local.DOF(k)[d], vertices[k][d] );

  // volume field variable

  BOOST_REQUIRE_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Tet) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Tet) );

  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup0 = xfld_local.getCellGroup<Tet>(0);
  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup1 = xfld_local.getCellGroup<Tet>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 4 );

  // interior-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nInteriorTraceGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Triangle>(1).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup0 = xfld_local.getInteriorTraceGroup<Triangle>(0);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup1 = xfld_local.getInteriorTraceGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 4 );

  // boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nBoundaryTraceGroups(), 2 );

  for (int i = 0; i < xfld_local.nBoundaryTraceGroups(); i++)
    BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Triangle>(i).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup0  = xfld_local.getBoundaryTraceGroup<Triangle>(0);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup1  = xfld_local.getBoundaryTraceGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 3 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.nElem(), 6 );

  // ghost boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 ); // Another quirk of the local patch

  for (int i = 0; i < xfld_local.nGhostBoundaryTraceGroups(); i++)
    BOOST_REQUIRE( xfld_local.getGhostBoundaryTraceGroup<Triangle>(i).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldGBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 1 );


  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_1Tet_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField3D_1Tet_X1_1Group xfld;

  // Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // connecting nodes
  const int group = 0, elem = 0, edge = 0;

  //Extract the local mesh for the bottom-left triangle
  XField_LocalPatchConstructor<PhysD3,Tet> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Continuous,&nodalview);

  // Split the grid
  XField_LocalPatch<PhysD3,Tet> xfld_local(xfld_construct, ElementSplitType::Edge, edge);

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 5 );

  Real vertices[5][3] = {{ 0, 0, 0}, // node 3 from the global
                         { 1, 0, 0},
                         { 0, 1, 0},
                         { 0, 0, 1},
                         { 0, 1./2, 1./2}};

  //Check the node values

  const Real small_tol = 1e-13, close_tol = 1e-13;
  for (int k = 0; k < xfld_local.nDOF(); k++)
    for (int d = 0; d < 3; d++)
      SANS_CHECK_CLOSE( xfld_local.DOF(k)[d], vertices[k][d], small_tol, close_tol );

  // volume field variable

  BOOST_REQUIRE_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Tet) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Tet) );

  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup0 = xfld_local.getCellGroup<Tet>(0);
  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup1 = xfld_local.getCellGroup<Tet>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 0 );

  // interior-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nInteriorTraceGroups(), 1 );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup0 = xfld_local.getInteriorTraceGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1 );

  // boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nBoundaryTraceGroups(), 4 );

  for (int i = 0; i < xfld_local.nBoundaryTraceGroups(); i++)
    BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Triangle>(i).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup0  = xfld_local.getBoundaryTraceGroup<Triangle>(0);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup1  = xfld_local.getBoundaryTraceGroup<Triangle>(1);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup2  = xfld_local.getBoundaryTraceGroup<Triangle>(2);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup3  = xfld_local.getBoundaryTraceGroup<Triangle>(3);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.nElem(), 2 );

  // ghost boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 ); // Another quirk of the local patch

  for (int i = 0; i < xfld_local.nGhostBoundaryTraceGroups(); i++)
    BOOST_REQUIRE( xfld_local.getGhostBoundaryTraceGroup<Triangle>(i).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldGBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 0 );


  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_5Tet_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField3D_5Tet_X1_1Group_AllOrientations xfld(-1);

  // Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // connecting nodes
  const int group = 0, elem = 0, edge = 0;

  //Extract the local mesh for the bottom-left triangle
  XField_LocalPatchConstructor<PhysD3,Tet> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Continuous,&nodalview);

  // Split the grid
  XField_LocalPatch<PhysD3,Tet> xfld_local(xfld_construct, ElementSplitType::Edge, edge);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 9 );

  Real vertices[9][3] = {{ 0, 0, 0},
                         { 1, 0, 0},
                         { 0, 1, 0},
                         { 0, 0, 1},
                         {-1, 0, 0},
                         { 0,-1, 0},
                         { 0, 0,-1},
                         { 1, 1, 1},
                         { 0, 1./2, 1./2}};

  //Check the node values
  const Real small_tol = 1e-13, close_tol = 1e-13;
  for (int k = 0; k < xfld_local.nDOF(); k++)
    for (int d = 0; d < 3; d++)
      SANS_CHECK_CLOSE( xfld_local.DOF(k)[d], vertices[k][d], small_tol, close_tol );

  // volume field variable

  BOOST_REQUIRE_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Tet) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Tet) );

  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup0 = xfld_local.getCellGroup<Tet>(0);
  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup1 = xfld_local.getCellGroup<Tet>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 6 );

  // interior-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nInteriorTraceGroups(), 3 );
  for (int i = 0; i < 3; i++)
    BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Triangle>(i).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup0 = xfld_local.getInteriorTraceGroup<Triangle>(0);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup1 = xfld_local.getInteriorTraceGroup<Triangle>(1);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup2 = xfld_local.getInteriorTraceGroup<Triangle>(2);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 6 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.nElem(), 2 );

  // boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nBoundaryTraceGroups(), 1 );

  for (int i = 0; i < xfld_local.nBoundaryTraceGroups(); i++)
    BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Triangle>(i).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup0  = xfld_local.getBoundaryTraceGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 14 );

  // ghost boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 ); // Another quirk of the local patch

  for (int i = 0; i < xfld_local.nGhostBoundaryTraceGroups(); i++)
    BOOST_REQUIRE( xfld_local.getGhostBoundaryTraceGroup<Triangle>(i).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldGBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 0 );


  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_6Tet_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField3D_6Tet_X1_1Group xfld;

  // Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // connecting nodes
  const int group = 0, elem = 0, edge = 5;

  //Extract the local mesh for the bottom-left triangle
  XField_LocalPatchConstructor<PhysD3,Tet> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Continuous,&nodalview);

  // Split the grid
  XField_LocalPatch<PhysD3,Tet> xfld_local(xfld_construct, ElementSplitType::Edge, edge);

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_REQUIRE_EQUAL( xfld_local.nDOF(), 8 );

  Real vertices[8][3] = {{ 0.0, 0.0, 0.0},
                         { 1.0, 0.0, 0.0},
                         { 0.0, 1.0, 0.0},
                         { 0.0, 0.0, 1.0},
                         { 1.0, 1.0, 0.0},
                         { 1.0, 0.0, 1.0},
                         { 0.0, 1.0, 1.0},
                         { 0.5, 0.0, 0.0}};

  //Check the node values
  const Real small_tol = 1e-13, close_tol = 1e-13;
  for (int k = 0; k < xfld_local.nDOF(); k++)
    for (int d = 0; d < PhysD3::D; d++)
      SANS_CHECK_CLOSE( xfld_local.DOF(k)[d], vertices[k][d], small_tol, close_tol );

  // volume field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Tet) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Tet) );

  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup0 = xfld_local.getCellGroup<Tet>(0);
  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup1 = xfld_local.getCellGroup<Tet>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 4 );

  // interior-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nInteriorTraceGroups(), 3 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Triangle>(1).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Triangle>(1).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup0 = xfld_local.getInteriorTraceGroup<Triangle>(0);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup1 = xfld_local.getInteriorTraceGroup<Triangle>(1);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup2 = xfld_local.getInteriorTraceGroup<Triangle>(2);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.nElem(), 4 );


  // boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nBoundaryTraceGroups(), 2 );

  for (int i = 0; i < xfld_local.nBoundaryTraceGroups(); i++)
    BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Triangle>(i).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Triangle>(0);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup1 = xfld_local.getBoundaryTraceGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 5 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.nElem(), 6 );

  // ghost boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 ); // Another quirk of the local patch

  for (int i = 0; i < xfld_local.nGhostBoundaryTraceGroups(); i++)
    BOOST_REQUIRE( xfld_local.getGhostBoundaryTraceGroup<Triangle>(i).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldGBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 1 );


  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( WholeGrid_5Tet_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  for (int orientation = -1; orientation > -4; orientation--)
  {
    XField3D_5Tet_X1_1Group_AllOrientations xfld(orientation);

    //Build cell to trace connectivity structure
    XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

    // Build node to cell connectivity structure
    Field_NodalView nodalview(xfld,{0});

    // Loop over all elements in grid and construct local grids
    for (int group = 0; group < xfld.nCellGroups(); group++)
      for ( int elem = 0; elem < xfld.getCellGroup<Tet>(group).nElem(); elem++ )
      {
        //Extract the local mesh for the bottom-left triangle
        XField_LocalPatchConstructor<PhysD3,Tet> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Continuous,&nodalview);

        for (int edge = 0; edge < Tet::NEdge; edge++)
        {
          // Split the grid
          XField_LocalPatch<PhysD3,Tet> xfld_local(xfld_construct, ElementSplitType::Edge, edge);

          // check that all local elements match the global elements
          XField_LocalGlobal_Equiv(xfld_local);
        }
      }
  }
}
#endif

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_WholeGrid_Box_Tet_test )
{
  typedef XField<PhysD3, TopoD3>::FieldCellGroupType<Tet> XFieldCellGroupType;

  mpi::communicator world;
  const int comm_rank = world.rank();

  mpi::communicator comm_local = world.split(comm_rank);

  int size = 3*3*3*world.size();
  int ii = pow(size, 1./3.), jj = ii, kk = ii;
  // make it ([0,3],[0,3]) so the node locations easier
  XField3D_Box_Tet_X1 xfld( world, ii, jj, kk, 0, 3, 0, 3, 0, 3 );

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  // Loop over all elements in grid and construct local grids
  for (int group = 0; group < xfld.nCellGroups(); group++)
  {
    const XFieldCellGroupType& xfldCellGroup = xfld.getCellGroup<Tet>(group);

    for ( int elem = 0; elem < xfldCellGroup.nElem(); elem++ )
    {
      // only consider possessed elements
      if (xfldCellGroup.associativity(elem).rank() != comm_rank) continue;

      //Extract the local mesh
      XField_LocalPatchConstructor<PhysD3,Tet> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Continuous,&nodalview);

      for (int edge = 0; edge < Tet::NEdge; edge++)
      {
        // Split the grid
        XField_LocalPatch<PhysD3,Tet> xfld_local(xfld_construct, ElementSplitType::Edge, edge);

        // check that all local elements match the global elements
        XField_LocalGlobal_Equiv(xfld_local);
      }
    }
  }
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
