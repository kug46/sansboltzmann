// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// XField3D_LocalPatch_btest
// testing of XField3D_Local
//
// Note: unit grids tested in "unit/UnitGrids/UnitGrids_btest.cpp"

#include <ostream>
#include <utility> // std::pair

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "Topology/ElementTopology.h"

#include "BasisFunction/ElementEdges.h"

#include "Field/Field_NodalView.h"
#include "Field/XField_CellToTrace.h"
#include "Field/Local/XField_LocalPatch.h"

#include "unit/UnitGrids/XField3D_1Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_5Tet_X1_1Group_AllOrientations.h"
#include "unit/UnitGrids/XField_KuhnFreudenthal.h"

#include "XField_LocalGlobal_Equiv_btest.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( XField3D_Local_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_LocalPatch_1Tet_1Group_InteriorCell_Target_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField3D_1Tet_X1_1Group xfld;

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  // Build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  int group = 0, elem = 0;

  //Extract the local mesh for the central tet
  XField_LocalPatchConstructor<PhysD3,Tet> xfld_local(comm_local,xfld,connectivity);

  xfld_local.addResolveElement( group, elem );
  xfld_local.extract();

  // create a split patch and split edge (0,1)
  XField_LocalPatch<PhysD3,Tet> xfld_split( xfld_local, 0, 1 );

  // check that all elements match the global elements
  XField_LocalGlobal_Equiv(xfld_split);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_LocalPatch_5Tet_1Group_InteriorCell_Target_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField3D_5Tet_X1_1Group_AllOrientations xfld(-1);

  // build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  // build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  int group = 0, elem = 0;

  // extract the local mesh for the central tet
  XField_LocalPatchConstructor<PhysD3,Tet> xfld_local(comm_local,xfld,connectivity);

  xfld_local.addResolveElement( group, elem );
  xfld_local.addFixedElement( 0, 1 );
  xfld_local.addFixedElement( 0, 2 );
  xfld_local.addFixedElement( 0, 3 );
  xfld_local.addFixedElement( 0, 4 );

  xfld_local.extract();

  // create a split patch and split edge (0,1)
  XField_LocalPatch<PhysD3,Tet> xfld_split( xfld_local, 0, 1 );

  // check that all elements match the global elements
  XField_LocalGlobal_Equiv(xfld_split);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_LocalPatchAddNeighbours_CKF_InteriorCell_Target_test )
{
  #ifdef SANS_AVRO

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField_KuhnFreudenthal<PhysD3,TopoD3> xfld( comm_local, {6,6,6} );

  // build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  // build node to cell connectivity structure
  Field_NodalView nodalview(xfld,{0});

  time_t t0 = clock();
  int nsplit = 0;
  for (int group=0;group<xfld.nCellGroups();group++)
  {
    for (int elem=0;elem<xfld.getCellGroupBase(group).nElem();elem++)
    {
      // extract the local mesh for the central tet
      XField_LocalPatchConstructor<PhysD3,Tet> xfld_local(comm_local,xfld,connectivity,&nodalview);

      xfld_local.addResolveElement( group, elem );
      xfld_local.addSharedTraceNeighbours();

      xfld_local.extract();

      // get the cell dof
      std::vector<int> cellDOF( Tet::NNode, -1 );
      xfld.getCellGroup<Tet>(group).associativity(elem).getNodeGlobalMapping( cellDOF.data(), cellDOF.size() );

      // list of edges
      const int (*edges)[Line::NNode] = ElementEdges<Tet>::EdgeNodes;

      // split every edge
      for (int edge = 0; edge < Tet::NEdge; edge++)
      {

        int e0 = cellDOF[ edges[edge][0] ];
        int e1 = cellDOF[ edges[edge][1] ];

        XField_LocalPatch<PhysD3,Tet> xfld_split( xfld_local, e0, e1 );

        nsplit++;

        // check that all elements match the global elements
        XField_LocalGlobal_Equiv(xfld_split);
      }
    }
  }

  printf("performed nsplit = %d in %g seconds\n",nsplit,double(clock()-t0)/CLOCKS_PER_SEC);
  #endif

}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
