// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
#include <ostream>
#include <utility> // std::pair

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "Topology/ElementTopology.h"

#include "Field/XField_CellToTrace.h"
#include "Field/Local/XField_LocalPatch.h"

#include "unit/UnitGrids/XField3D_1Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_2Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_5Tet_X1_1Group_AllOrientations.h"
#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"

#include "XField_LocalGlobal_Equiv_btest.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( XField3D_LocalPatch_Element_DG_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_1Tet_X1_InteriorCell_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField3D_1Tet_X1_1Group xfld;

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  // group and element
  const int group = 0, elem = 0;

  //Extract the local mesh
  XField_LocalPatchConstructor<PhysD3,Tet> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Discontinuous);
  XField_LocalPatch<PhysD3,Tet> xfld_local(xfld_construct);

#if 0
  xfld.dump(3,std::cout);
  std::cout<<std::endl;
  xfld_local.dump(3,std::cout);
#endif

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_REQUIRE_EQUAL( xfld_local.nDOF(), 4 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld_local.DOF(0)[0],  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(0)[1],  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(0)[2],  0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(1)[0],  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(1)[1],  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(1)[2],  0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(2)[0],  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(2)[1],  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(2)[2],  0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(3)[0],  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(3)[1],  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(3)[2],  1 );

  // volume field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Tet) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Tet) );

  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup0 = xfld_local.getCellGroup<Tet>(0);
  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup1 = xfld_local.getCellGroup<Tet>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 0 );

  // interior-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nInteriorTraceGroups(), 0 );

  // boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nBoundaryTraceGroups(), 4 );

  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Triangle>(1).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Triangle>(2).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Triangle>(3).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Triangle>(0);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup1 = xfld_local.getBoundaryTraceGroup<Triangle>(1);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup2 = xfld_local.getBoundaryTraceGroup<Triangle>(2);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup3 = xfld_local.getBoundaryTraceGroup<Triangle>(3);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.nElem(), 1 );

  // ghost boundary-trace field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldGBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 0 );

  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_1Tet_X2_InteriorCell_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField3D_1Tet_X1_1Group xfld_X1; //Linear mesh

  XField<PhysD3,TopoD3> xfld(xfld_X1,2); //Construct Q2 mesh from linear mesh

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  // group and element
  const int group = 0, elem = 0;

  //Extract the local mesh
  XField_LocalPatchConstructor<PhysD3,Tet> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Discontinuous);
  XField_LocalPatch<PhysD3,Tet> xfld_local(xfld_construct);

#if 0
  xfld.dump(3,std::cout);
  std::cout<<std::endl;
  xfld_local.dump(3,std::cout);
#endif

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_REQUIRE_EQUAL( xfld_local.nDOF(), 10 );

  //Check the node values
  for (int k=0; k<5; k++)
  {
    BOOST_CHECK_EQUAL( xfld_local.DOF(k)[0],  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(k)[1],  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(k)[2],  0 );
  }
  BOOST_CHECK_EQUAL( xfld_local.DOF(6)[0],  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(6)[1],  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(6)[2],  0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(7)[0],  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(7)[1],  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(7)[2],  0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(8)[0],  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(8)[1],  1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(8)[2],  0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(9)[0],  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(9)[1],  0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(9)[2],  1 );

  // volume field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Tet) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Tet) );

  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup0 = xfld_local.getCellGroup<Tet>(0);
  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup1 = xfld_local.getCellGroup<Tet>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 0 );

  // interior-trace field variable
  BOOST_CHECK_EQUAL( xfld_local.nInteriorTraceGroups(), 0 );

  // boundary-trace field variable
  BOOST_CHECK_EQUAL( xfld_local.nBoundaryTraceGroups(), 4 );

  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Triangle>(1).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Triangle>(2).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Triangle>(3).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Triangle>(0);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup1 = xfld_local.getBoundaryTraceGroup<Triangle>(1);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup2 = xfld_local.getBoundaryTraceGroup<Triangle>(2);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup3 = xfld_local.getBoundaryTraceGroup<Triangle>(3);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.nElem(), 1 );

  // ghost boundary-trace field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldGBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 0 );

  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_5Tet_AllOrientations_X2_1Group_InteriorCell_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField3D_5Tet_X1_1Group_AllOrientations xfld_X1(-1); //Linear mesh

  XField<PhysD3,TopoD3> xfld(xfld_X1,2); //Construct Q2 mesh from linear mesh

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  // group and element
  const int group = 0, elem = 0;

  //Extract the local mesh
  XField_LocalPatchConstructor<PhysD3,Tet> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Discontinuous);
  XField_LocalPatch<PhysD3,Tet> xfld_local(xfld_construct);

#if 0
  xfld.dump(3,std::cout);
  std::cout<<std::endl;
  xfld_local.dump(3,std::cout);
#endif

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_REQUIRE_EQUAL( xfld_local.nDOF(), 26 );

  //Check the node values
  for (int i=0; i<18; i++)
  {
    //All higher-order node values need to be zero since the Q2 mesh is also still essentially linear
    BOOST_CHECK_EQUAL( xfld_local.DOF(i)[0], 0);
    BOOST_CHECK_EQUAL( xfld_local.DOF(i)[1], 0);
    BOOST_CHECK_EQUAL( xfld_local.DOF(i)[2], 0);
  }
  BOOST_CHECK_EQUAL( xfld_local.DOF(18)[0], 0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(18)[1], 0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(18)[2], 0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(19)[0], 1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(19)[1], 0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(19)[2], 0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(20)[0], 0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(20)[1], 1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(20)[2], 0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(21)[0], 0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(21)[1], 0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(21)[2], 1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(22)[0], 1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(22)[1], 1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(22)[2], 1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(23)[0],-1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(23)[1], 0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(23)[2], 0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(24)[0], 0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(24)[1],-1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(24)[2], 0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(25)[0], 0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(25)[1], 0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(25)[2],-1 );


  // volume field variable
  BOOST_CHECK_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Tet) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Tet) );

  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup0 = xfld_local.getCellGroup<Tet>(0);
  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup1 = xfld_local.getCellGroup<Tet>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 4 );

  // interior-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup = xfld_local.getInteriorTraceGroup<Triangle>(0);
  BOOST_CHECK_EQUAL( xfldITraceGroup.nElem(), 4 );

  // boundary-trace field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nBoundaryTraceGroups(), 0 );

  // ghost boundary-trace field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getGhostBoundaryTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 12 );

  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_5Tet_AllOrientations_X2_BoundaryCell_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField3D_5Tet_X1_1Group_AllOrientations xfld_X1(-1); //Linear mesh

  XField<PhysD3,TopoD3> xfld(xfld_X1,2); //Construct Q2 mesh from linear mesh

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  // group and element
  const int group = 0, elem = 1;

  //Extract the local mesh
  XField_LocalPatchConstructor<PhysD3,Tet> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Discontinuous);
  XField_LocalPatch<PhysD3,Tet> xfld_local(xfld_construct);

#if 0
  xfld.dump(3,std::cout);
  std::cout<<std::endl;
  xfld_local.dump(3,std::cout);
#endif

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_REQUIRE_EQUAL( xfld_local.nDOF(), 14 );

  //Check the node values
  for (int i = 0; i < 9; i++)
  {
    //All higher-order node values need to be zero since the Q2 mesh is also still essentially linear
    BOOST_CHECK_EQUAL( xfld_local.DOF(i)[0], 0);
    BOOST_CHECK_EQUAL( xfld_local.DOF(i)[1], 0);
    BOOST_CHECK_EQUAL( xfld_local.DOF(i)[2], 0);
  }
  BOOST_CHECK_EQUAL( xfld_local.DOF( 9)[0], 1 );  BOOST_CHECK_EQUAL( xfld_local.DOF( 9)[1], 0 );  BOOST_CHECK_EQUAL( xfld_local.DOF( 9)[2], 0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(10)[0], 0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(10)[1], 1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(10)[2], 0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(11)[0], 0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(11)[1], 0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(11)[2], 1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(12)[0], 1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(12)[1], 1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(12)[2], 1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(13)[0], 0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(13)[1], 0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(13)[2], 0 );

  // volume field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Tet) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Tet) );

  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup0 = xfld_local.getCellGroup<Tet>(0);
  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup1 = xfld_local.getCellGroup<Tet>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 1 );

  // interior-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup = xfld_local.getInteriorTraceGroup<Triangle>(0);
  BOOST_CHECK_EQUAL( xfldITraceGroup.nElem(), 1 );

  // boundary-trace field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nBoundaryTraceGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 3 );

  // ghost boundary-trace field variable
  BOOST_CHECK_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getGhostBoundaryTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldGBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 3 );

  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_5Tet_AllOrientations_X2_BoundaryCell2_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  XField3D_5Tet_X1_1Group_AllOrientations xfld_X1(+2); //Linear mesh

  XField<PhysD3,TopoD3> xfld(xfld_X1,2); //Construct Q2 mesh from linear mesh

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  // group and element
  const int group = 0, elem = 2;

  //Extract the local mesh
  XField_LocalPatchConstructor<PhysD3,Tet> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Discontinuous);
  XField_LocalPatch<PhysD3,Tet> xfld_local(xfld_construct);

#if 0
  xfld.dump(3,std::cout);
  std::cout<<std::endl;
  xfld_local.dump(3,std::cout);
#endif

  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_REQUIRE_EQUAL( xfld_local.nDOF(), 14 );

  //Check the node values
  for (int i=0; i<9; i++)
  {
    //All higher-order node values need to be zero since the Q2 mesh is also still essentially linear
    BOOST_CHECK_EQUAL( xfld_local.DOF(i)[0], 0);
    BOOST_CHECK_EQUAL( xfld_local.DOF(i)[1], 0);
    BOOST_CHECK_EQUAL( xfld_local.DOF(i)[2], 0);
  }
  BOOST_CHECK_EQUAL( xfld_local.DOF( 9)[0], 0 );  BOOST_CHECK_EQUAL( xfld_local.DOF( 9)[1], 0 );  BOOST_CHECK_EQUAL( xfld_local.DOF( 9)[2], 1 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(10)[0], 0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(10)[1], 1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(10)[2], 0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(11)[0], 0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(11)[1], 0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(11)[2], 0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(12)[0],-1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(12)[1], 0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(12)[2], 0 );
  BOOST_CHECK_EQUAL( xfld_local.DOF(13)[0], 1 );  BOOST_CHECK_EQUAL( xfld_local.DOF(13)[1], 0 );  BOOST_CHECK_EQUAL( xfld_local.DOF(13)[2], 0 );

  // volume field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Tet) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Tet) );

  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup0 = xfld_local.getCellGroup<Tet>(0);
  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup1 = xfld_local.getCellGroup<Tet>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 1 );

  // interior-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup = xfld_local.getInteriorTraceGroup<Triangle>(0);
  BOOST_CHECK_EQUAL( xfldITraceGroup.nElem(), 1 );

  // boundary-trace field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nBoundaryTraceGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 3 );

  // ghost boundary-trace field variable
  BOOST_CHECK_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );
  BOOST_REQUIRE( xfld_local.getGhostBoundaryTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldGBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 3 );

  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Edge0_2Tet_X1_RightCell_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  Real tol = 1e-12;

  XField3D_2Tet_X1_1Group xfld;

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  // group and element
  const int group = 0, elem = 0;
  int split_edge_index = 0;

  //Extract the local mesh and split it
  XField_LocalPatchConstructor<PhysD3,Tet> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Discontinuous);
  XField_LocalPatch<PhysD3,Tet> xfld_local(xfld_construct, ElementSplitType::Edge, split_edge_index);

#if 0
  xfld_local.dump(3,std::cout);
  std::cout<<std::endl;
  xfld_local.dump(3,std::cout);
#endif


  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_REQUIRE_EQUAL( xfld_local.getNewNodeDOFs().size(), 1 );
  BOOST_CHECK_EQUAL( xfld_local.getNewNodeDOFs()[0], 5 );

  BOOST_REQUIRE_EQUAL( xfld_local.nDOF(), 6 );

  Real vertices[6][3] = {{ 0.0, 0.0, 0.0},
                         { 1.0, 0.0, 0.0},
                         { 0.0, 1.0, 0.0},
                         { 0.0, 0.0, 1.0},
                         {-1.0, 0.0, 0.0},
                         { 0.0, 0.5, 0.5}};

  //Check the node values
  for (int k = 0; k < xfld_local.nDOF(); k++)
    for (int d = 0; d < PhysD3::D; d++)
      SANS_CHECK_CLOSE( vertices[k][d], xfld_local.DOF(k)[d], tol, tol );

  // volume field variable
  BOOST_CHECK_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Tet) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Tet) );

  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup0 = xfld_local.getCellGroup<Tet>(0);
  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup1 = xfld_local.getCellGroup<Tet>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 2 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nInteriorTraceGroups(), 3 );

  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Triangle>(1).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Triangle>(2).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup0 = xfld_local.getInteriorTraceGroup<Triangle>(0);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup1 = xfld_local.getInteriorTraceGroup<Triangle>(1);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup2 = xfld_local.getInteriorTraceGroup<Triangle>(2);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.nElem(), 1 );

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nBoundaryTraceGroups(), 3 );

  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Triangle>(1).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Triangle>(2).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Triangle>(0);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup1 = xfld_local.getBoundaryTraceGroup<Triangle>(1);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup2 = xfld_local.getBoundaryTraceGroup<Triangle>(2);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.nElem(), 2 );

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_local.getGhostBoundaryTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldGBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 4 );

  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Edge1_2Tet_X1_RightCell_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  Real tol = 1e-12;

  XField3D_2Tet_X1_1Group xfld;

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  // group and element
  const int group = 0, elem = 0;
  int split_edge_index = 1;

  //Extract the local mesh and split it
  XField_LocalPatchConstructor<PhysD3,Tet> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Discontinuous);
  XField_LocalPatch<PhysD3,Tet> xfld_local(xfld_construct, ElementSplitType::Edge, split_edge_index);

#if 0
  xfld_local.dump(3,std::cout);
  std::cout<<std::endl;
  xfld_local.dump(3,std::cout);
#endif


  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_local.getNewNodeDOFs().size(), 1 );
  BOOST_CHECK_EQUAL( xfld_local.getNewNodeDOFs()[0], 5 );

  BOOST_REQUIRE_EQUAL( xfld_local.nDOF(), 6 );

  Real vertices[6][3] = {{ 0.0, 0.0, 0.0},
                         { 1.0, 0.0, 0.0},
                         { 0.0, 1.0, 0.0},
                         { 0.0, 0.0, 1.0},
                         {-1.0, 0.0, 0.0},
                         { 0.5, 0.0, 0.5}};

  //Check the node values
  for (int k = 0; k < xfld_local.nDOF(); k++)
    for (int d = 0; d < PhysD3::D; d++)
      SANS_CHECK_CLOSE( xfld_local.DOF(k)[d], vertices[k][d], tol, tol );


  // volume field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Tet) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Tet) );

  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup0 = xfld_local.getCellGroup<Tet>(0);
  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup1 = xfld_local.getCellGroup<Tet>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 1 );

  // interior-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nInteriorTraceGroups(), 2 );

  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Triangle>(1).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup0 = xfld_local.getInteriorTraceGroup<Triangle>(0);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup1 = xfld_local.getInteriorTraceGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 1 );

  // boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nBoundaryTraceGroups(), 3 );

  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Triangle>(1).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Triangle>(2).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Triangle>(0);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup1 = xfld_local.getBoundaryTraceGroup<Triangle>(1);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup2 = xfld_local.getBoundaryTraceGroup<Triangle>(2);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.nElem(), 2 );

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_local.getGhostBoundaryTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldGBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 3 );

  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Edge2_2Tet_X1_1Group_LeftCell_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  Real tol = 1e-12;

  XField3D_2Tet_X1_1Group xfld;

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  // group and element
  const int group = 0, elem = 1;
  int split_edge_index = 2;

  //Extract the local mesh and split it
  XField_LocalPatchConstructor<PhysD3,Tet> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Discontinuous);
  XField_LocalPatch<PhysD3,Tet> xfld_local(xfld_construct, ElementSplitType::Edge, split_edge_index);

#if 0
  xfld_local.dump(3,std::cout);
  std::cout<<std::endl;
  xfld_local.dump(3,std::cout);
#endif


  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 6 );

  BOOST_CHECK_EQUAL( xfld_local.getNewNodeDOFs().size(), 1 );
  BOOST_CHECK_EQUAL( xfld_local.getNewNodeDOFs()[0], 5 );

  BOOST_REQUIRE_EQUAL( xfld_local.nDOF(), 6 );

  Real vertices[6][3] = {{-1.0, 0.0, 0.0},
                         { 0.0, 0.0, 0.0},
                         { 0.0, 1.0, 0.0},
                         { 0.0, 0.0, 1.0},
                         { 1.0, 0.0, 0.0},
                         { 0.0, 0.5, 0.0}};

  //Check the node values - should be all zero since DOFs have not been projected yet
  for (int k = 0; k < xfld_local.nDOF(); k++)
    for (int d = 0; d < PhysD3::D; d++)
      SANS_CHECK_CLOSE( vertices[k][d], xfld_local.DOF(k)[d], tol, tol );


  // volume field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Tet) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Tet) );

  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup0 = xfld_local.getCellGroup<Tet>(0);
  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup1 = xfld_local.getCellGroup<Tet>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 2 );


  // interior-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nInteriorTraceGroups(), 3 );

  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Triangle>(1).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Triangle>(2).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup0 = xfld_local.getInteriorTraceGroup<Triangle>(0);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup1 = xfld_local.getInteriorTraceGroup<Triangle>(1);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup2 = xfld_local.getInteriorTraceGroup<Triangle>(2);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.nElem(), 1 );

  // boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nBoundaryTraceGroups(), 3 );

  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Triangle>(1).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Triangle>(2).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Triangle>(0);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup1 = xfld_local.getBoundaryTraceGroup<Triangle>(1);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup2 = xfld_local.getBoundaryTraceGroup<Triangle>(2);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.nElem(), 1 );


  // ghost boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_local.getGhostBoundaryTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldGBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 4 );

  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Edge3_2Tet_X1_1Group_LeftCell_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  Real tol = 1e-12;

  XField3D_2Tet_X1_1Group xfld;

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  // group and element
  const int group = 0, elem = 1;
  int split_edge_index = 3;

  //Extract the local mesh and split it
  XField_LocalPatchConstructor<PhysD3,Tet> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Discontinuous);
  XField_LocalPatch<PhysD3,Tet> xfld_local(xfld_construct, ElementSplitType::Edge, split_edge_index);

#if 0
  xfld_local.dump(3,std::cout);
  std::cout<<std::endl;
  xfld_local.dump(3,std::cout);
#endif


  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 6 );

  BOOST_CHECK_EQUAL( xfld_local.getNewNodeDOFs().size(), 1 );
  BOOST_CHECK_EQUAL( xfld_local.getNewNodeDOFs()[0], 5 );

  Real vertices[6][3] = {{-1.0,0.0,0.0},
                         {0.0,0.0,0.0},
                         {0.0,1.0,0.0},
                         {0.0,0.0,1.0},
                         {1.0,0.0,0.0},
                         {-0.5,0.5,0.0}};

  //Check the node values
  for (int k = 0; k < xfld_local.nDOF(); k++)
    for (int d = 0; d < PhysD3::D; d++)
      SANS_CHECK_CLOSE( xfld_local.DOF(k)[d], vertices[k][d], tol, tol );


  // volume field variable
  BOOST_CHECK_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Tet) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Tet) );

  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup0 = xfld_local.getCellGroup<Tet>(0);
  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup1 = xfld_local.getCellGroup<Tet>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 1 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nInteriorTraceGroups(), 2 );

  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Triangle>(1).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup0 = xfld_local.getInteriorTraceGroup<Triangle>(0);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup1 = xfld_local.getInteriorTraceGroup<Triangle>(1);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 1 );

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nBoundaryTraceGroups(), 3 );

  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Triangle>(1).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Triangle>(2).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Triangle>(0);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup1 = xfld_local.getBoundaryTraceGroup<Triangle>(1);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup2 = xfld_local.getBoundaryTraceGroup<Triangle>(2);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.nElem(), 2 );

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_local.getGhostBoundaryTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldGBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 3 );

  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Edge4_2Tet_X1_1Group_RightCell_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  Real tol = 1e-12;

  XField3D_2Tet_X1_1Group xfld;

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  // group and element
  const int group = 0, elem = 0;
  int split_edge_index = 4;

  //Extract the local mesh and split it
  XField_LocalPatchConstructor<PhysD3,Tet> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Discontinuous);
  XField_LocalPatch<PhysD3,Tet> xfld_local(xfld_construct, ElementSplitType::Edge, split_edge_index);

#if 0
  xfld_local.dump(3,std::cout);
  std::cout<<std::endl;
  xfld_local.dump(3,std::cout);
#endif


  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_REQUIRE_EQUAL( xfld_local.nDOF(), 6 );

  BOOST_CHECK_EQUAL( xfld_local.getNewNodeDOFs().size(), 1 );
  BOOST_CHECK_EQUAL( xfld_local.getNewNodeDOFs()[0], 5 );

  Real vertices[6][3] = {{0.0,0.0,0.0},
                         {1.0,0.0,0.0},
                         {0.0,1.0,0.0},
                         {0.0,0.0,1.0},
                         {-1.0,0.0,0.0},
                         {0.0,0.0,0.5}};

  //Check the node values
  for (int k=0; k<xfld_local.nDOF(); k++)
    for (int d=0; d<3; d++)
      SANS_CHECK_CLOSE( xfld_local.DOF(k)[d], vertices[k][d], tol, tol );


  // volume field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Tet) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Tet) );

  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup0 = xfld_local.getCellGroup<Tet>(0);
  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup1 = xfld_local.getCellGroup<Tet>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 2 );


  // interior-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nInteriorTraceGroups(), 3 );

  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Triangle>(1).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Triangle>(2).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup0 = xfld_local.getInteriorTraceGroup<Triangle>(0);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup1 = xfld_local.getInteriorTraceGroup<Triangle>(1);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup2 = xfld_local.getInteriorTraceGroup<Triangle>(2);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.nElem(), 1 );

  // boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nBoundaryTraceGroups(), 3 );

  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Triangle>(1).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getBoundaryTraceGroup<Triangle>(2).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup0 = xfld_local.getBoundaryTraceGroup<Triangle>(0);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup1 = xfld_local.getBoundaryTraceGroup<Triangle>(1);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup2 = xfld_local.getBoundaryTraceGroup<Triangle>(2);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.nElem(), 1 );

  // ghost boundary-edge field variable
  BOOST_REQUIRE_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_local.getGhostBoundaryTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldGBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( xfldGBTraceGroup0.nElem(), 4 );

  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Edge5_5Tet_X1_InteriorCell_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  Real tol = 1e-12;

  XField3D_5Tet_X1_1Group_AllOrientations xfld(-1);

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  // group and element
  const int group = 0, elem = 0;
  int split_edge_index = 5;

  //Extract the local mesh and split it
  XField_LocalPatchConstructor<PhysD3,Tet> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Discontinuous);
  XField_LocalPatch<PhysD3,Tet> xfld_local(xfld_construct, ElementSplitType::Edge, split_edge_index);

#if 0
  xfld_local.dump(3,std::cout);
  std::cout<<std::endl;
  xfld_local.dump(3,std::cout);
#endif


  BOOST_CHECK_EQUAL( &connectivity, &xfld_local.connectivity() );

  BOOST_CHECK_EQUAL( xfld_local.nDOF(), 9 );

  Real vertices[9][3] = {{0.0,0.0,0.0},
                         {1.0,0.0,0.0},
                         {0.0,1.0,0.0},
                         {0.0,0.0,1.0},
                         {1.0,1.0,1.0},
                         {-1.0,0.0,0.0},
                         {0.0,-1.0,0.0},
                         {0.0,0.0,-1.0},
                         {0.5,0.0,0.0}};

  //Check the node values
  for (int k = 0; k < xfld_local.nDOF(); k++)
    for (int d = 0; d < PhysD3::D; d++)
      SANS_CHECK_CLOSE( xfld_local.DOF(k)[d], vertices[k][d], tol, tol );


  // volume field variable
  BOOST_CHECK_EQUAL( xfld_local.nCellGroups(), 2 );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(0).topoTypeID() == typeid(Tet) );
  BOOST_REQUIRE( xfld_local.getCellGroupBase(1).topoTypeID() == typeid(Tet) );

  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup0 = xfld_local.getCellGroup<Tet>(0);
  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldCellGroup1 = xfld_local.getCellGroup<Tet>(1);

  BOOST_CHECK_EQUAL( xfldCellGroup0.nElem(), 2 );
  BOOST_CHECK_EQUAL( xfldCellGroup1.nElem(), 6 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nInteriorTraceGroups(), 3 );

  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Triangle>(1).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld_local.getInteriorTraceGroup<Triangle>(2).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup0 = xfld_local.getInteriorTraceGroup<Triangle>(0);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup1 = xfld_local.getInteriorTraceGroup<Triangle>(1);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup2 = xfld_local.getInteriorTraceGroup<Triangle>(2);

  BOOST_CHECK_EQUAL( xfldITraceGroup0.nElem(), 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup1.nElem(), 6 );
  BOOST_CHECK_EQUAL( xfldITraceGroup2.nElem(), 2 );

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nBoundaryTraceGroups(), 0 );

  // ghost boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld_local.nGhostBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld_local.getGhostBoundaryTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup0 = xfld_local.getGhostBoundaryTraceGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 14 );

  // check that all local elements match the global elements
  XField_LocalGlobal_Equiv(xfld_local);
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_AllEdges_5Tet_InteriorCell_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  Real tol = 1e-12;

  Real vertices[9][3] = {{ 0.0, 0.0, 0.0},
                         { 1.0, 0.0, 0.0},
                         { 0.0, 1.0, 0.0},
                         { 0.0, 0.0, 1.0},
                         { 1.0, 1.0, 1.0},
                         {-1.0, 0.0, 0.0},
                         { 0.0,-1.0, 0.0},
                         { 0.0, 0.0,-1.0},
                         { 0.0, 0.5, 0.5}};

  //Location of new node for each of the 6 edge split configurations
  Real newDOF[6][3] = {{0.0, 0.5, 0.5},
                       {0.5, 0.0, 0.5},
                       {0.5, 0.5, 0.0},
                       {0.0, 0.5, 0.0},
                       {0.0, 0.0, 0.5},
                       {0.5, 0.0, 0.0}};

  //Loop across all orientations of AllOrientation grid, and simply generate the split meshes
  //xfld.checkGrid() will automatically perform connectivity and jacobian checks.
  for (int orient=-3; orient<=3; orient++)
  {
    if (orient==0) continue;

    XField3D_5Tet_X1_1Group_AllOrientations xfld(orient);

    //Build cell to trace connectivity structure
    XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

    // group and element
    const int group = 0, elem = 0;

    //Extract the local mesh and split it
    XField_LocalPatchConstructor<PhysD3,Tet> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Discontinuous);

    //Loop across the 6 different edge-split configurations
    for (int split_edge_index = 0; split_edge_index < Tet::NEdge; split_edge_index++)
    {
      XField_LocalPatch<PhysD3,Tet> xfld_local(xfld_construct, ElementSplitType::Edge, split_edge_index);

      BOOST_CHECK_EQUAL( xfld_local.nDOF(), 9 );

      //Update the new node in the vertex list according to the split_edge_index
      for (int k = 0; k < PhysD3::D; k++)
        vertices[8][k] = newDOF[split_edge_index][k];

      //Check the node values
      for (int k = 0; k < xfld_local.nDOF(); k++)
        for (int d = 0; d < PhysD3::D; d++)
          SANS_CHECK_CLOSE( xfld_local.DOF(k)[d], vertices[k][d], tol, tol );

      // check that all local elements match the global elements
      XField_LocalGlobal_Equiv(xfld_local);
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_AllEdges_5Tet_X2_InteriorCell_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  Real small_tol = 1e-12;
  Real close_tol = 1e-10;

  Real vertices[9][3] = {{ 0.0, 0.0, 0.0},
                         { 1.0, 0.0, 0.0},
                         { 0.0, 1.0, 0.0},
                         { 0.0, 0.0, 1.0},
                         { 1.0, 1.0, 1.0},
                         {-1.0, 0.0, 0.0},
                         { 0.0,-1.0, 0.0},
                         { 0.0, 0.0,-1.0},
                         { 0.0, 0.5, 0.5}};

  //Location of new node for each of the 6 edge split configurations
  Real newDOF[6][3] = {{0.0, 0.5, 0.5},
                       {0.5, 0.0, 0.5},
                       {0.5, 0.5, 0.0},
                       {0.0, 0.5, 0.0},
                       {0.0, 0.0, 0.5},
                       {0.5, 0.0, 0.0}};

  //Loop across all orientations of AllOrientation grid, and simply generate the split meshes
  //xfld.checkGrid() will automatically perform connectivity and jacobian checks.
  for (int orient = -3; orient <= 3; orient++)
  {
    if (orient==0) continue;

    XField3D_5Tet_X1_1Group_AllOrientations xfld_X1(orient);

    XField<PhysD3,TopoD3> xfld(xfld_X1,2); //Construct Q2 mesh from linear mesh

    //Build cell to trace connectivity structure
    XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

    // group and element
    const int group = 0, elem = 0;

    //Extract the local mesh and split it
    XField_LocalPatchConstructor<PhysD3,Tet> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Discontinuous);

    //Loop across the 6 different edge-split configurations
    for (int split_edge_index = 0; split_edge_index < Tet::NEdge; split_edge_index++)
    {
      XField_LocalPatch<PhysD3,Tet> xfld_local(xfld_construct, ElementSplitType::Edge, split_edge_index);

#if 0
      xfld_local.dump(3,std::cout);
      std::cout<<std::endl;
      xfld_local.dump(3,std::cout);
#endif

      BOOST_REQUIRE_EQUAL( xfld_local.nDOF(), 32 );

      //Update the new node in the vertex list according to the split_edge_index
      for (int k = 0; k < PhysD3::D; k++)
        vertices[8][k] = newDOF[split_edge_index][k];

      //Check if the edge DOFs are zero
      for (int k = 0; k < 23; k++)
        for (int d = 0; d < PhysD3::D; d++)
          SANS_CHECK_CLOSE( xfld_local.DOF(k)[d], 0.0, small_tol, close_tol );

      //Check the node values
      for (int k = 0; k < 9; k++)
        for (int d = 0; d < PhysD3::D; d++)
          SANS_CHECK_CLOSE( xfld_local.DOF(23 + k)[d], vertices[k][d], small_tol, close_tol );

      // check that all local elements match the global elements
      XField_LocalGlobal_Equiv(xfld_local, small_tol, close_tol);
    }
  }
}


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_WholeGrid_Box_Tet_test )
{
  typedef XField<PhysD3, TopoD3>::FieldCellGroupType<Tet> XFieldCellGroupType;

  mpi::communicator world;
  const int comm_rank = world.rank();

  mpi::communicator comm_local = world.split(comm_rank);

  int size = 3*3*3*world.size();
  int ii = pow(size, 1./3.), jj = ii, kk = ii;
  // make it ([0,3],[0,3]) so the node locations easier
  XField3D_Box_Tet_X1 xfld( world, ii, jj, kk, 0, 3, 0, 3, 0, 3 );

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD3,TopoD3> connectivity(xfld);

  // Loop over all elements in grid and construct local grids
  for (int group = 0; group < xfld.nCellGroups(); group++)
  {
    const XFieldCellGroupType& xfldCellGroup = xfld.getCellGroup<Tet>(group);

    for ( int elem = 0; elem < xfldCellGroup.nElem(); elem++ )
    {
      // only consider possessed elements
      if (xfldCellGroup.associativity(elem).rank() != comm_rank) continue;

      //Extract the local mesh
      XField_LocalPatchConstructor<PhysD3,Tet> xfld_construct(comm_local,connectivity,group,elem,SpaceType::Discontinuous);

      for (int edge = 0; edge < Tet::NEdge; edge++)
      {
        // Split the grid
        XField_LocalPatch<PhysD3,Tet> xfld_local(xfld_construct, ElementSplitType::Edge, edge);

        // check that all local elements match the global elements
        XField_LocalGlobal_Equiv(xfld_local);
      }
    }
  }
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
