// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Field2D_EdgeLocal_CG_btest
//

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "Meshing/XField1D/XField1D.h"

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunctionLine.h"

#include "Field/FieldLine_CG_Cell.h"
#include "Field/FieldLine_CG_BoundaryTrace.h"
#include "Field/Local/Field_Local.h"
#include "Field/Field_NodalView.h"

#include "Field/Local/XField_LocalPatch.h"

#include "Field_LocalPatch_toolkit_btest.h"

#include "tools/make_unique.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

//Explicitly instantiate classes to get proper coverage information
namespace SANS
{
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( Field1D_LocalPatch_Edge_CG_test_suite )

#if 0
// This is a specialization for 1D to handle the TopoD0 and Node issue.

// A helper function to construct a Solver interface and test all the edges on a grid
void localEdgeExtractAndTransfer(const XField<PhysD1, TopoD1>& xfld, const BasisFunctionCategory basis_category,
                             const int order, const Real small_tol, const Real close_tol, const bool isSplit )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD1, TopoD1, ArrayQ > QField1D_CG_Area;
  typedef Field_CG_BoundaryTrace< PhysD1, TopoD1, ArrayQ > QField1D_CG_BTrace;

  typedef DLA::VectorS<TopoD1::D,Real> RefCoordType;
  typedef DLA::VectorS<1,Real> TraceRefCoordType;

  typedef QField1D_CG_Area::FieldCellGroupType<Line> FieldCellGroupType;
  typedef FieldCellGroupType::ElementType<> ElementFieldClass;
  typedef typename XField<PhysD1, TopoD1>::template FieldCellGroupType<Line> XFieldCellGroupType;
  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

  typedef QField1D_CG_BTrace::FieldTraceGroupType<Node> FieldTraceGroupType;
  typedef FieldTraceGroupType::ElementType<> ElementTraceFieldClass;

  const int nEdge = Line::NEdge; // Number of different edges
  typedef typename std::set<std::array<int,Line::NNode>> EdgeList;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  QField1D_CG_Area qfld(xfld, order, basis_category);
  QField1D_CG_BTrace lgfld(xfld, 1, basis_category);

  //Set some non-zero solution
  for (int n = 0; n < qfld.nDOF(); n++)
    qfld.DOF(n) = {1*(-n)-1, (n)+1};

  //Set some non-zero solution
  for (int n = 0; n < lgfld.nDOF(); n++)
    lgfld.DOF(n) = {1*(-n)-1, (n)+1};

  ///////////////////////
  // LOOP OVER CELL GROUPS ->
  ///////////////////////
  const int (*EdgeNodes)[Line::NNode] = ElementEdges<Line>::EdgeNodes;
  for (int group = 0; group  < xfld.nCellGroups(); group++)
  {
    EdgeList edge_list, failed_edge_list;

    // edge_list is cleared with each loop over the groups
    XFieldCellGroupType xfldCellGroup = xfld.template getCellGroup<Line>(group);
    const int nElem = xfldCellGroup.nElem();

    ElementXFieldClass xfldElem(xfldCellGroup.basis() );

    ///////////////////////
    // EDGE LIST EXTRACTION
    ///////////////////////

    for ( int elem = 0; elem < nElem; elem++ )
    {
      xfldCellGroup.getElement(xfldElem,elem);
      std::array<int,Line::NNode> nodeMap;

      xfldCellGroup.associativity(elem).getNodeGlobalMapping( nodeMap.data(), nodeMap.size() );

      for ( int edge = 0; edge < nEdge; edge++ )
      {
        // loop over the edges
        std::array<int,2> newEdge;
        newEdge[0] = MIN(nodeMap[(EdgeNodes[edge][0])],nodeMap[(EdgeNodes[edge][1])]);
        newEdge[1] = MAX(nodeMap[(EdgeNodes[edge][0])],nodeMap[(EdgeNodes[edge][1])]);

        edge_list.insert(newEdge);
      }
    }

    /////////////////////////////////////////////
    // EDGE LOCAL MESH CONSTRUCT
    /////////////////////////////////////////////

    const XField_CellToTrace<PhysD1,TopoD1> xfld_connectivity(xfld);
    const Field_NodalView nodalView(xfld, {group});

    typedef typename Field_NodalView::IndexVector IndexVector;

    for ( const auto& edge : edge_list )
    {
      /////////////////////////////////////////////
      // EDGE LOCAL MESH CONSTRUCT
      /////////////////////////////////////////////

      IndexVector nodeGroup0, nodeGroup1;
      nodeGroup0 = nodalView.getCellList( edge[0] );
      nodeGroup1 = nodalView.getCellList( edge[1] );

      // find the interesction of the two sets
      std::set<GroupElemIndex> attachedCells;
      typename std::set<GroupElemIndex>::iterator attachedCells_it;
      std::set_intersection( nodeGroup0.begin(), nodeGroup0.end(), nodeGroup1.begin(), nodeGroup1.end(),
                             std::inserter( attachedCells, attachedCells.begin() ) );


      typedef XField_LocalPatch<PhysD1,Line> XFieldLocal;
      std::unique_ptr<XFieldLocal> pxfld_local;
      if (isSplit)
      {
        // create a split patch
        pxfld_local = make_unique<XFieldLocal>( comm_local, xfld_connectivity, edge, SpaceType::Continuous, &nodalView );
      }
      else
      {
        // create an unsplit patch
        XField_LocalPatchConstructor<PhysD1,Line> xfld_local(comm_local, xfld_connectivity, edge, SpaceType::Continuous, &nodalView );
        pxfld_local = make_unique<XFieldLocal>( xfld_local );
      }

      const XFieldLocal& xfld_local = *pxfld_local;

      /////////////////////////////////////////////
      // LOCAL SOLUTION TRANSFER
      /////////////////////////////////////////////

      Field_Local<QField1D_CG_Area> qfld_local( xfld_local, qfld, order, basis_category);
      Field_Local<QField1D_CG_BTrace> lgfld_local(xfld_local, lgfld, 1, basis_category );

      //-------------CHECK LOCAL FIELD----------------
      BOOST_CHECK_EQUAL( &qfld_local.getXField(), &xfld_local );
      BOOST_CHECK_EQUAL( &lgfld_local.getXField(), &xfld_local );

      Quadrature<TopoD1, Line> cellQuadrature(order+2);
      const int nCellquad = cellQuadrature.nQuadrature();

      Quadrature<TopoD0, Node> traceQuadrature(0);
      const int nTracequad = traceQuadrature.nQuadrature();

      if (!isSplit)
      {
        /////////////////////////////////////////////
        // EDGE LOCAL UNSPLIT CHECK
        /////////////////////////////////////////////

        RefCoordType RefCoord;  // reference-element coordinates
        ArrayQ qeval_main, qeval_local;

        for (int cellGroup = 0; cellGroup < qfld_local.nCellGroups(); cellGroup++)
          for (int elem = 0; elem < xfld_local.getCellGroup<Line>(cellGroup).nElem(); elem++)
          {
            std::pair<int,int> globGroupElem = xfld_local.getGlobalCellMap(std::make_pair(cellGroup,elem));

            const FieldCellGroupType& global_cellgrp = qfld.getCellGroup<Line>(globGroupElem.first);
            ElementFieldClass fldElem_global( global_cellgrp.basis() );
            global_cellgrp.getElement(fldElem_global, globGroupElem.second);

            const FieldCellGroupType& local_cellgrp = qfld_local.getCellGroup<Line>(cellGroup);
            ElementFieldClass fldElem_local( global_cellgrp.basis() );
            local_cellgrp.getElement(fldElem_local, elem);

            //Check if the solutions are equal at a few cellQuadrature points
            for (int iquad = 0; iquad < nCellquad; iquad++)
            {
              cellQuadrature.coordinates( iquad, RefCoord );

              fldElem_local.eval( RefCoord, qeval_local);

              fldElem_global.eval(RefCoord, qeval_main);

              for (int i=0; i<2; i++)
                SANS_CHECK_CLOSE(qeval_local[i], qeval_main[i], small_tol, close_tol);
            }

            if ( !(order >= 3 && global_cellgrp.basis()->category() == BasisFunctionCategory_Hierarchical) )
            {
              // Check if the local dofs are identical
              for (int dof = 0; dof < fldElem_global.nDOF(); dof++)
                for (int i = 0; i < 2; i++)
                  SANS_CHECK_CLOSE( fldElem_global.DOF(dof)[i], fldElem_local.DOF(dof)[i], small_tol, close_tol );
            }

          }

        TraceRefCoordType TRefCoord; // reference-element coordinates in trace
        for (int traceGroup = 0; traceGroup < lgfld_local.nBoundaryTraceGroups(); traceGroup++)
        {
          const int xFieldBGroupGlobal = lgfld_local.getGlobalBoundaryTraceGroupMap( traceGroup ); // matching xfield_local bgroups

          // to get global xfield group
          std::pair<int,int> globGroupTrace = xfld_local.getGlobalBoundaryTraceMap( std::make_pair(xFieldBGroupGlobal,0) );
          const int lgFieldBGroupGlobal = lgfld.getLocalBoundaryTraceGroupMap( globGroupTrace.first ); // The lgfield group matching xfield

          const FieldTraceGroupType& local_tracegrp  = lgfld_local.template getBoundaryTraceGroup<Node>(traceGroup);
          const FieldTraceGroupType& global_tracegrp = lgfld.template getBoundaryTraceGroup<Node>(lgFieldBGroupGlobal);

          for (int trace = 0; trace < local_tracegrp.nElem(); trace++)
          {
            ElementTraceFieldClass fldElem_global( global_tracegrp.basis() );
            ElementTraceFieldClass fldElem_local (  local_tracegrp.basis() );

            global_tracegrp.getElement( fldElem_global, globGroupTrace.second );
            local_tracegrp. getElement( fldElem_local,  trace );

            for (int iquad = 0; iquad < nTracequad; iquad++)
            {
              traceQuadrature.coordinates( iquad, TRefCoord );

              fldElem_local.eval( TRefCoord, qeval_local);
              fldElem_global.eval( TRefCoord, qeval_main);

              for (int i = 0; i < 2; i++)
                SANS_CHECK_CLOSE( qeval_local[i], qeval_main[i], small_tol, close_tol );

            // Check if the local dofs are identical -- can do this because lgfld is always a DG basis.
            for (int dof = 0; dof < fldElem_global.nDOF(); dof++)
              for (int i = 0; i < 2; i++)
                SANS_CHECK_CLOSE( fldElem_global.DOF(dof)[i], fldElem_local.DOF(dof)[i], small_tol, close_tol );
            }
          }
        }
      }
      else if (isSplit)
      {
        /////////////////////////////////////////////
        // EDGE LOCAL SPLIT CHECK FOR CELLGROUP 0
        /////////////////////////////////////////////

        RefCoordType RefCoord_main, RefCoord_sub;  // reference-element coordinates
        ArrayQ qeval_main0, qeval_main1, qeval_local0, qeval_local1;

        // Loop over half the number of cells, because evaluating two at a time
        int cellGroup = 0; // Checking the split elements
        for (int elem = 0; elem < xfld_local.getCellGroup<Line>(cellGroup).nElem()/2; elem++)
        {
          std::pair<int,int> globGroupElem = xfld_local.getGlobalCellMap(std::make_pair(cellGroup,elem));

          // Extract the global element
          const FieldCellGroupType& global_cellgrp = qfld.getCellGroup<Line>(globGroupElem.first);
          ElementFieldClass fldElem_global( global_cellgrp.basis() );
          global_cellgrp.getElement(fldElem_global, globGroupElem.second);

          // Extract the two local elements
          const FieldCellGroupType& local_cellgrp = qfld_local.getCellGroup<Line>(cellGroup);
          ElementFieldClass fldElem_local0( global_cellgrp.basis() ), fldElem_local1( global_cellgrp.basis() );
          local_cellgrp.getElement(fldElem_local0, elem);
          local_cellgrp.getElement(fldElem_local1, elem + xfld_local.getCellGroup<Line>(cellGroup).nElem()/2);

          // Figure out the canonical trace of the global element that is getting split
          // Assumes that the xfield and field cellgroup and element numbers match

          std::array<int,Line::NNode> localCellNodeMap;
          xfld_local.getCellGroup<Line>(cellGroup).associativity(elem).getNodeGlobalMapping( localCellNodeMap.data(), localCellNodeMap.size() );

          int localEdge0, localEdge1;

          // check if contains the start or the end node of the split edge
          for (int i = 0; i < Line::NNode; i++)
          {
            if (localCellNodeMap[i] == 0)
            {
              localEdge0 = 0; localEdge1 = 1;
              break;
            }
            else if (localCellNodeMap[i] == 1)
            {
              localEdge0 = 1; localEdge1 = 0;
              break;
            }
          }

          //Check if the solutions are equal at a few cellQuadrature points
          for (int iquad = 0; iquad < nCellquad; iquad++)
          {
            cellQuadrature.coordinates( iquad, RefCoord_sub );

            // coordinates in the local elements
            fldElem_local0.eval( RefCoord_sub, qeval_local0);
            fldElem_local1.eval( RefCoord_sub, qeval_local1);

            BasisFunction_RefElement_Split<TopoD1,Line>::
              transform( RefCoord_sub, ElementSplitType::Isotropic, -1, localEdge0, RefCoord_main);

            fldElem_global.eval(RefCoord_main, qeval_main0);

            BasisFunction_RefElement_Split<TopoD1,Line>::
              transform( RefCoord_sub, ElementSplitType::Isotropic, -1, localEdge1, RefCoord_main);

            fldElem_global.eval(RefCoord_main, qeval_main1);

            for (int i=0; i<2; i++)
            {
              SANS_CHECK_CLOSE(qeval_local0[i], qeval_main0[i], small_tol, close_tol);
              SANS_CHECK_CLOSE(qeval_local1[i], qeval_main1[i], small_tol, close_tol);
            }
          }
        }

        TraceRefCoordType TRefCoord_main, TRefCoord_sub; // reference-element coordinates in trace
        for (int traceGroup = 0; traceGroup < lgfld_local.nBoundaryTraceGroups(); traceGroup++)
        {
          const int xFieldBGroupGlobal = lgfld_local.getGlobalBoundaryTraceGroupMap( traceGroup ); // matching xfield_local bgroups

          // to get global xfield group
          std::pair<int,int> globGroupTrace = xfld_local.getGlobalBoundaryTraceMap( std::make_pair(xFieldBGroupGlobal,0) );
          const int lgFieldBGroupGlobal = lgfld.getLocalBoundaryTraceGroupMap( globGroupTrace.first ); // The lgfield group matching xfield

          const FieldTraceGroupType& local_tracegrp  = lgfld_local.template getBoundaryTraceGroup<Node>(traceGroup);
          const FieldTraceGroupType& global_tracegrp = lgfld.template getBoundaryTraceGroup<Node>(lgFieldBGroupGlobal);

          if ( local_tracegrp.nElem() == 2 ) // SPLIT TRACE
          {
            BOOST_CHECK_EQUAL(1,0); // shouldn't get here
          }
          else if ( local_tracegrp.nElem() == 1 ) // UNSPLIT TRACE
          {
            ElementTraceFieldClass fldElem_global( global_tracegrp.basis() );
            ElementTraceFieldClass fldElem_local (  local_tracegrp.basis() );

            global_tracegrp.getElement( fldElem_global, globGroupTrace.second );
            local_tracegrp. getElement( fldElem_local,  0 );

            for (int iquad = 0; iquad < nTracequad; iquad++)
            {
              traceQuadrature.coordinates( iquad, TRefCoord_main );

              fldElem_local.eval( TRefCoord_main, qeval_local0);
              fldElem_global.eval( TRefCoord_main, qeval_main0);

              for (int i = 0; i < 2; i++)
                SANS_CHECK_CLOSE( qeval_local0[i], qeval_main0[i], small_tol, close_tol );


            // Check if the local dofs are identical -- can do this because lgfld is always a DG basis.
            for (int dof = 0; dof < fldElem_global.nDOF(); dof++)
              for (int i = 0; i < 2; i++)
                SANS_CHECK_CLOSE( fldElem_global.DOF(dof)[i], fldElem_local.DOF(dof)[i], small_tol, close_tol );
            }
          }
          else
            BOOST_CHECK_EQUAL( 1, 0 ); // Shouldn't be possible to get here, btraces should have 1 or 2 elements
        }

        if (xfld_local.nCellGroups() == 2)
        {
          /////////////////////////////////////////////
          // EDGE LOCAL UNSPLIT CHECK FOR CELLGROUP 1
          /////////////////////////////////////////////

          // check the unsplit groups
          cellGroup = 1;

          RefCoordType RefCoord;  // reference-element coordinates
          ArrayQ qeval_main, qeval_local;

          for (int elem = 0; elem < xfld_local.getCellGroup<Line>(cellGroup).nElem(); elem++)
          {
            std::pair<int,int> globGroupElem = xfld_local.getGlobalCellMap(std::make_pair(cellGroup,elem));

            const FieldCellGroupType& global_cellgrp = qfld.getCellGroup<Line>(globGroupElem.first);
            ElementFieldClass fldElem_global( global_cellgrp.basis() );
            global_cellgrp.getElement(fldElem_global, globGroupElem.second);

            const FieldCellGroupType& local_cellgrp = qfld_local.getCellGroup<Line>(cellGroup);
            ElementFieldClass fldElem_local( global_cellgrp.basis() );
            local_cellgrp.getElement(fldElem_local, elem);

            //Check if the solutions are equal at a few cellQuadrature points
            for (int iquad = 0; iquad < nCellquad; iquad++)
            {
              cellQuadrature.coordinates( iquad, RefCoord );

              fldElem_local.eval( RefCoord, qeval_local);

              fldElem_global.eval(RefCoord, qeval_main);

              for (int i=0; i<2; i++)
                SANS_CHECK_CLOSE(qeval_local[i], qeval_main[i], small_tol, close_tol);
            }

            if ( !(order >= 3 && global_cellgrp.basis()->category() == BasisFunctionCategory_Hierarchical) )
            {
              // Check if the local dofs are identical
              for (int dof = 0; dof < fldElem_global.nDOF(); dof++)
                for (int i = 0; i < 2; i++)
                  SANS_CHECK_CLOSE( fldElem_global.DOF(dof)[i], fldElem_local.DOF(dof)[i], small_tol, close_tol );
            }
          }
        }

      }
    }
  }

}
#endif

//============================================================================//
// Hierarchical
//============================================================================//

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_Hierarchical_P1 )
{
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5

  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = false;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Hierarchical_P1 )
{
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5

  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = true;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_Hierarchical_P2 )
{
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5

  int order = 2;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = false;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Hierarchical_P2 )
{
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5

  int order = 2;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = true;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_Hierarchical_P3 )
{
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5

  int order = 3;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = false;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Hierarchical_P3 )
{
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5

  int order = 3;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Hierarchical;

  bool split_test = true;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

//============================================================================//
// Lagrange
//============================================================================//

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_Lagrange_P1 )
{
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5

  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = false;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Lagrange_P1 )
{
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5

  int order = 1;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = true;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_Lagrange_P2 )
{
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5

  int order = 2;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = false;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Lagrange_P2 )
{
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5

  int order = 2;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = true;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Unsplit_Lagrange_P3 )
{
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5

  int order = 3;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = false;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif


#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Split_Lagrange_P3 )
{
  XField1D xfld(5,0,5); //Linear mesh with 5 lines from 0 to 5

  int order = 3;
  const Real small_tol = 1e-10, close_tol = 1e-10;
  const BasisFunctionCategory basis_category = BasisFunctionCategory_Lagrange;

  bool split_test = true;
  localEdgeExtractAndTransfer( xfld, basis_category, order, small_tol, close_tol, split_test );
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
