// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// GenHField_DG_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "Field/HField/GenHFieldLine_DG.h"
#include "Field/HField/GenHFieldArea_DG.h"
#include "Field/HField/GenHFieldVolume_DG.h"

#include "Field/Local/XField_LocalPatch.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Pow.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Log.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField3D_6Tet_X1_1Group.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( GenHField_DG_test_suite )

BOOST_AUTO_TEST_CASE( statics )
{
  BOOST_CHECK( (GenHField_DG<PhysD1,TopoD1>::D == 1) );
  BOOST_CHECK( (GenHField_DG<PhysD2,TopoD2>::D == 2) );
  BOOST_CHECK( (GenHField_DG<PhysD3,TopoD3>::D == 3) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Line_grid_equal_test )
{
  // grid
  XField1D xfld( 8 );

  typedef GenHField_DG<PhysD1,TopoD1>::FieldCellGroupType<Line> HFieldLineClass;
  typedef HFieldLineClass::ElementType<> ElementHFieldLineClass;
  typedef DLA::MatrixSymS<PhysD1::D,Real> MatrixSym;

  GenHField_DG<PhysD1,TopoD1> hfld( xfld );

  Real exactSize = 1.0/8.0;

  MatrixSym logH;
  HFieldLineClass& hfld1Line = hfld.getCellGroup<Line>(0);
  ElementHFieldLineClass hfldElem(hfld1Line.basis());

  for (int elem = 0; elem < hfld1Line.nElem(); elem++)
  {
    hfld1Line.getElement(hfldElem, elem);

    hfldElem.eval(0.25, logH);
    BOOST_CHECK_CLOSE(logH(0,0), log(exactSize), 1e-12);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Line_grid_varying_test )
{
  // grid
  XField1D xfld( 4 );
  xfld.DOF(0) = -0.1;
  xfld.DOF(1) =  0.0;
  xfld.DOF(2) =  0.2;
  xfld.DOF(3) =  0.5;
  xfld.DOF(4) =  0.9;

  GenHField_DG<PhysD1,TopoD1> logHfld( xfld );

  const Real small_tol = 1e-13;

  //Check if the h-value at each node is the average size of the cells around that node
  BOOST_CHECK_CLOSE(logHfld.DOF(0)(0,0), log(0.1), small_tol);
  BOOST_CHECK_CLOSE(logHfld.DOF(1)(0,0), log(0.2), small_tol);
  BOOST_CHECK_CLOSE(logHfld.DOF(2)(0,0), log(0.3), small_tol);
  BOOST_CHECK_CLOSE(logHfld.DOF(3)(0,0), log(0.4), small_tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Area_grid_equal_test )
{
  // grid
  XField2D_4Triangle_X1_1Group xfld;

  typedef GenHField_DG<PhysD2,TopoD2>::FieldCellGroupType<Triangle> HFieldTriangleClass;
  typedef HFieldTriangleClass::ElementType<> ElementHFieldTriangleClass;
  typedef DLA::MatrixSymS<PhysD2::D,Real> MatrixSym;

  GenHField_DG<PhysD2,TopoD2> hfld( xfld );

  MatrixSym logH;
  HFieldTriangleClass& hfld1Triangle = hfld.getCellGroup<Triangle>(0);
  ElementHFieldTriangleClass hfldElem(hfld1Triangle.basis());

  for (int elem = 0; elem < hfld1Triangle.nElem(); elem++)
  {
    hfld1Triangle.getElement(hfldElem, elem);

    hfldElem.eval(1./3., 1./3., logH);
    BOOST_CHECK_CLOSE(logH(0,0),  0.0719205181129452318598, 1e-12);
    BOOST_CHECK_CLOSE(logH(1,0), -0.2746530721670274228488, 1e-12);
    BOOST_CHECK_CLOSE(logH(1,1),  0.0719205181129452318598, 1e-12);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Area_grid_equal_parallel_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  // Grids are intentionally not uniform

  int ii = 3;
  int jj = 4;
  std::vector<Real> xvec(ii+1);
  std::vector<Real> yvec(jj+1);

  // construct vectors
  for (int i = 0; i < ii+1; i++)
    xvec[i] = i*i/Real(ii*ii);

  for (int j = 0; j < jj+1; j++)
    yvec[j] = 2*j*j/Real(jj*jj);

  // create identical global mesh on each processors
  XField2D_Box_Triangle_Lagrange_X1 xfld_global(comm_local, xvec, yvec);

  // create a parallelized grid
  XField2D_Box_Triangle_Lagrange_X1 xfld(world, xvec, yvec);

  typedef GenHField_DG<PhysD2,TopoD2>::FieldCellGroupType<Triangle> HFieldTriangleClass;
  typedef HFieldTriangleClass::ElementType<> ElementHFieldTriangleClass;
  typedef DLA::MatrixSymS<PhysD2::D,Real> MatrixSym;

  GenHField_DG<PhysD2,TopoD2> hfld_global( xfld_global );
  GenHField_DG<PhysD2,TopoD2> hfld( xfld );

  const std::vector<int> cellIDs = xfld.cellIDs(0);

  MatrixSym logH_global, logH;
  HFieldTriangleClass& hfld1Triangle_global = hfld_global.getCellGroup<Triangle>(0);
  ElementHFieldTriangleClass hfldElem_global(hfld1Triangle_global.basis());

  HFieldTriangleClass& hfld1Triangle = hfld.getCellGroup<Triangle>(0);
  ElementHFieldTriangleClass hfldElem(hfld1Triangle.basis());

  for (std::size_t elem = 0; elem < cellIDs.size(); elem++)
  {
    int elem_global = cellIDs[elem];
    hfld1Triangle_global.getElement(hfldElem_global, elem_global);
    hfld1Triangle.getElement(hfldElem, elem);

    hfldElem_global.eval(1./3., 1./3., logH_global);
    hfldElem.eval(1./3., 1./3., logH);

    BOOST_CHECK_CLOSE(logH_global(0,0), logH(0,0), 1e-12);
    BOOST_CHECK_CLOSE(logH_global(1,0), logH(1,0), 1e-12);
    BOOST_CHECK_CLOSE(logH_global(1,1), logH(1,1), 1e-12);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Line_local_mesh_isotropic_split_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  // grid
  XField1D xfld( 3 );
  xfld.DOF(0) = 0.0;
  xfld.DOF(1) = 0.1;
  xfld.DOF(2) = 0.5;
  xfld.DOF(3) = 1.0;

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD1,TopoD1> connectivity(xfld);

  const int cellgroup = 0;
  const int cellelem = 1;

  // Extract the unsplit grid
  XField_LocalPatchConstructor<PhysD1,Line> xfld_construct(comm_local,connectivity,cellgroup, cellelem,SpaceType::Discontinuous);

  // Split the grid
  XField_LocalPatch<PhysD1,Line> xfld_split_local(xfld_construct, ElementSplitType::Edge, 0);


  GenHField_DG<PhysD1,TopoD1> logHfld( xfld );

  GenHField_DG<PhysD1,TopoD1> logHfld_local( xfld_split_local, logHfld );

  BOOST_CHECK_EQUAL(logHfld_local.nDOF(), 4);

  SANS_CHECK_CLOSE(logHfld_local.DOF(0)(0,0), log(0.2), small_tol, close_tol);
  SANS_CHECK_CLOSE(logHfld_local.DOF(1)(0,0), log(0.2), small_tol, close_tol);
  SANS_CHECK_CLOSE(logHfld_local.DOF(2)(0,0), log(0.5), small_tol, close_tol);
  SANS_CHECK_CLOSE(logHfld_local.DOF(3)(0,0), log(0.1), small_tol, close_tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Area_local_mesh_edge_split_test )
{
  static const int D = PhysD2::D;

  typedef typename GenHField_DG<PhysD2,TopoD2>::template FieldCellGroupType<Triangle> FieldCellGroupType;
  typedef typename FieldCellGroupType::template ElementType<> ElementFieldClass;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-10;

  // grid
  XField2D_Box_Triangle_X1 xfld(3, 3, 0, 3, 0, 3);

  typedef DLA::MatrixSymS<D,Real> MatrixSym;

  for (int orderQ = 1; orderQ <= 3; orderQ++)
  {
    XField<PhysD2, TopoD2> xfldQ(xfld, orderQ);

    //Build cell to trace connectivity structure
    XField_CellToTrace<PhysD2,TopoD2> connectivity(xfldQ);

    const int cellgroup = 0;
    const int cellelem = 8;

    //Extract the local mesh for the central triangle
    XField_LocalPatchConstructor<PhysD2, Triangle> xfld_construct(comm_local, connectivity, cellgroup, cellelem);

    int split_edge_index = 0;
    XField_LocalPatch<PhysD2, Triangle> xfld_split_local(xfld_construct,
                                                         ElementSplitType::Edge,
                                                         split_edge_index);

    GenHField_DG<PhysD2,TopoD2> hfld( xfldQ );

    GenHField_DG<PhysD2,TopoD2> hfld_local( xfld_split_local, hfld );

    MatrixSym M0 = {{1},{0.5,1}}; //elemental metric of unsplit triangles

    MatrixSym M00 = {{1},{0,3}}; //elemental metric of split element in local cellgroup 0, elem 0
    MatrixSym M01 = {{3},{0,1}}; //elemental metric of split element in local cellgroup 0, elem 1

    BOOST_CHECK_EQUAL(hfld_local.nDOF(), 6);

    for (int cell_group = 0; cell_group < hfld_local.nCellGroups(); cell_group++)
    {
      const FieldCellGroupType& cellgrp_local = hfld_local.template getCellGroup<Triangle>(cell_group);

      ElementFieldClass fldElem_local( cellgrp_local.basis() );

      for (int elem = 0; elem < cellgrp_local.nElem(); elem++)
      {
        ElementSplitInfo split_info = xfld_split_local.getCellSplitInfo(std::pair<int,int>(cell_group,elem));

        MatrixSym logH_true;

        if (split_info.split_flag == ElementSplitFlag::Unsplit)
          logH_true = log(pow(M0, -0.5));
        else
        {
          if (split_info.subcell_index == 0)
            logH_true = log(pow(M00, -0.5));
          else
            logH_true = log(pow(M01, -0.5));
        }

        cellgrp_local.getElement( fldElem_local, elem );
        MatrixSym logH = fldElem_local.DOF(0);

        SANS_CHECK_CLOSE(logH_true(0,0), logH(0,0), small_tol, close_tol);
        SANS_CHECK_CLOSE(logH_true(1,0), logH(1,0), small_tol, close_tol);
        SANS_CHECK_CLOSE(logH_true(1,1), logH(1,1), small_tol, close_tol);
      }
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Area_local_mesh_isotropic_split_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  // grid
  XField2D_Box_Triangle_X1 xfld(3, 3, 0, 3, 0, 3);

  for (int orderQ = 1; orderQ <= 3; orderQ++)
  {
    XField<PhysD2, TopoD2> xfldQ(xfld, orderQ);

    //Build cell to trace connectivity structure
    XField_CellToTrace<PhysD2,TopoD2> connectivity(xfldQ);

    const int cellgroup = 0;
    const int cellelem = 8;

    //Extract the local mesh for the central triangle
    XField_LocalPatchConstructor<PhysD2, Triangle> xfld_local(comm_local, connectivity, cellgroup, cellelem);

    typedef XField_LocalPatch<PhysD2, Triangle> XField_Local;

    // if we ever use Isotropic splits, remove the exception check and uncomment the tests
    int split_edge_index = -1;
    BOOST_CHECK_THROW(XField_Local xfld_split_local(xfld_local,
                                                         ElementSplitType::Isotropic,
                                                         split_edge_index);, AssertionException );

#if 0
    GenHField_DG<PhysD2,TopoD2> hfld( xfldQ );

    GenHField_DG<PhysD2,TopoD2> hfld_local( xfld_split_local, hfld );

    static const int D = PhysD2::D;
    typedef DLA::MatrixSymS<D,Real> MatrixSym;

    MatrixSym M0 = {{1},{0.5,1}}; //elemental metric of unsplit triangles

    MatrixSym M00 = {{4},{2,4}}; //elemental metric of split elements in local cellgroup 0

    MatrixSym M10 = {{3},{0,1}}; //elemental metric of split element in local cellgroup 1, elem 0
    MatrixSym M11 = {{1},{0,3}}; //elemental metric of split element in local cellgroup 1, elem 1

    MatrixSym M12 = {{1},{1,4}}; //elemental metric of split element in local cellgroup 1, elem 2
    MatrixSym M13 = {{3},{3,4}}; //elemental metric of split element in local cellgroup 1, elem 3

    MatrixSym M14 = {{4},{3,3}}; //elemental metric of split element in local cellgroup 1, elem 4
    MatrixSym M15 = {{4},{1,1}}; //elemental metric of split element in local cellgroup 1, elem 5

    BOOST_CHECK_EQUAL(hfld_local.nDOF(), 10);

    //List of metrics of each split element in local mesh
    std::vector<MatrixSym> metric_list = {M00, M00, M00, M00, M10, M11, M12, M13, M14, M15};

    const Real small_tol = 1e-11;
    const Real close_tol = 1e-10;

    for (int i = 0; i < (int) metric_list.size(); i++)
    {
      MatrixSym logH_true = log(pow(metric_list[i], -0.5));
      MatrixSym logH = hfld_local.DOF(i);

      SANS_CHECK_CLOSE(logH(0,0), logH_true(0,0), small_tol, close_tol);
      SANS_CHECK_CLOSE(logH(1,0), logH_true(1,0), small_tol, close_tol);
      SANS_CHECK_CLOSE(logH(1,1), logH_true(1,1), small_tol, close_tol);
    }
#endif
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
