// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// FieldSequence_btest
// testing of FieldSequence class
//

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;


#include "unit/UnitGrids/XField1D_2Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_2Quad_X1_1Group.h"
#include "unit/UnitGrids/XField3D_2Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_2Hex_X1_1Group.h"

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/TraceToCellRefCoord.h"
#include "Field/FieldSequence.h"

#include "Field/FieldLine_CG_Cell.h"
#include "Field/FieldLine_CG_BoundaryTrace.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_CG_BoundaryTrace.h"

#include "Field/FieldVolume_CG_Cell.h"
#include "Field/FieldVolume_CG_BoundaryTrace.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_InteriorTrace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_InteriorTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_InteriorTrace.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"

using namespace std;
using namespace SANS;

namespace SANS
{
//Explicitly instantiate classes to get proper coverage information

}


//############################################################################//
BOOST_AUTO_TEST_SUITE( FieldSequence_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_1D )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field< PhysD1, TopoD1, ArrayQ > FieldType;
  typedef FieldSequence<PhysD1, TopoD1, ArrayQ> QField1D_Sequence;
  typedef QField1D_Sequence::FieldTraceGroupType<Node> QFieldNodeSequenceClass;
  typedef QField1D_Sequence::FieldCellGroupType<Line> QFieldLineSequenceClass;
  typedef QFieldLineSequenceClass::ElementType<> ElementType;

  XField1D_2Line_X1_1Group xfld1;

  int order = 1;
  QField1D_Sequence qflds1(2, FieldConstructor<Field_CG_Cell>(), xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, qflds1.nFields() );
  BOOST_CHECK_EQUAL( 3, qflds1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qflds1.nElem() );
  BOOST_CHECK_EQUAL( 0, qflds1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 2, qflds1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qflds1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qflds1.getXField() );

  const FieldType& qfld1 = qflds1[0];

  BOOST_CHECK_EQUAL( 3, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfld1.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 2, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldLineSequenceClass& qfldGroup1 = qflds1.getCellGroup<Line>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup1.nFieldGroups() );
  BOOST_CHECK_EQUAL( 3, qfldGroup1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup1.nElem() );

  BOOST_CHECK_EQUAL( 3, qfldGroup1[0].nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup1[0].nElem() );

  // copy constructor
  QField1D_Sequence qflds2(qflds1);

  BOOST_CHECK_EQUAL(  qflds1.nDOF()                ,  qflds2.nDOF() );
  BOOST_CHECK_EQUAL(  qflds1.nElem()               ,  qflds2.nElem() );
  BOOST_CHECK_EQUAL(  qflds1.nInteriorTraceGroups(),  qflds2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qflds1.nBoundaryTraceGroups(),  qflds2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qflds1.nCellGroups()         ,  qflds2.nCellGroups() );
  BOOST_CHECK_EQUAL( &qflds1.getXField()           , &qflds2.getXField() );

  const QFieldLineSequenceClass& qfldGroup2 = qflds2.getCellGroup<Line>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup2.nFieldGroups() );
  BOOST_CHECK_EQUAL( 3, qfldGroup2.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup2.nElem() );

  BOOST_CHECK_EQUAL( 3, qfldGroup2[0].nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup2[0].nElem() );


  QField1D_Sequence qflds3(2, FieldConstructor<Field_CG_BoundaryTrace>(), xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, qflds3.nFields() );
  BOOST_CHECK_EQUAL( 2, qflds3.nDOF() );
  BOOST_CHECK_EQUAL( 2, qflds3.nElem() );
  BOOST_CHECK_EQUAL( 0, qflds3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 2, qflds3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qflds3.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qflds3.getXField() );

  const FieldType& qfld3 = qflds3[0];

  BOOST_CHECK_EQUAL( 2, qfld3.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfld3.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 2, qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld3.getXField() );

  const QFieldNodeSequenceClass& qfldGroup3 = qflds3.getBoundaryTraceGroup<Node>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup3.nFieldGroups() );
  BOOST_CHECK_EQUAL( 2, qfldGroup3.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldGroup3.nElem() );

  BOOST_CHECK_EQUAL( 2, qfldGroup3[0].nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldGroup3[0].nElem() );


  QField1D_Sequence qflds4(2, qfld3);

  BOOST_CHECK_EQUAL( 2, qflds4.nFields() );
  BOOST_CHECK_EQUAL( 2, qflds4.nDOF() );
  BOOST_CHECK_EQUAL( 2, qflds4.nElem() );
  BOOST_CHECK_EQUAL( 0, qflds4.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 2, qflds4.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qflds4.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qflds4.getXField() );

  const FieldType& qfld4 = qflds4[0];

  BOOST_CHECK_EQUAL( 2, qfld4.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfld4.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld4.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 2, qfld4.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld4.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld4.getXField() );

  const QFieldNodeSequenceClass& qfldGroup4 = qflds4.getBoundaryTraceGroup<Node>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup4.nFieldGroups() );
  BOOST_CHECK_EQUAL( 2, qfldGroup4.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldGroup4.nElem() );

  BOOST_CHECK_EQUAL( 2, qfldGroup4[0].nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldGroup4[0].nElem() );


  // can't have a zero length array...
  BOOST_CHECK_THROW( QField1D_Sequence qflds5(0, FieldConstructor<Field_CG_Cell>(), xfld1,
                                              order, BasisFunctionCategory_Hierarchical);, AssertionException );

  // Check element extraction
  qflds1[0] = 1;
  qflds1[1] = 2;

  ElementType qfldElem(qfldGroup1.basis());

  qfldGroup1.getElement(qfldElem, 0);

  Real tol = 1e-12;
  BOOST_CHECK_CLOSE(qfldElem[0].DOF(0)[0], 1, tol);
  BOOST_CHECK_CLOSE(qfldElem[1].DOF(0)[0], 2, tol);

  ElementType qfldElem2(qfldElem);

  BOOST_CHECK_CLOSE(qfldElem2[0].DOF(0)[0], 1, tol);
  BOOST_CHECK_CLOSE(qfldElem2[1].DOF(0)[0], 2, tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_1D )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field< PhysD1, TopoD1, ArrayQ > FieldType;
  typedef FieldSequence<PhysD1, TopoD1, ArrayQ> QField1D_Sequence;
  typedef QField1D_Sequence::FieldTraceGroupType<Node> QFieldNodeSequenceClass;
  typedef QField1D_Sequence::FieldCellGroupType<Line> QFieldLineSequenceClass;
  typedef QFieldLineSequenceClass::ElementType<> ElementType;

  XField1D_2Line_X1_1Group xfld1;

  int order = 1;
  QField1D_Sequence qflds1(2, FieldConstructor<Field_DG_Cell>(), xfld1, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 2, qflds1.nFields() );
  BOOST_CHECK_EQUAL( 4, qflds1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qflds1.nElem() );
  BOOST_CHECK_EQUAL( 0, qflds1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qflds1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qflds1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qflds1.getXField() );

  const FieldType& qfld1 = qflds1[0];

  BOOST_CHECK_EQUAL( 4, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfld1.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldLineSequenceClass& qfldGroup1 = qflds1.getCellGroup<Line>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup1.nFieldGroups() );
  BOOST_CHECK_EQUAL( 4, qfldGroup1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup1.nElem() );

  BOOST_CHECK_EQUAL( 4, qfldGroup1[0].nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup1[0].nElem() );

  // copy constructor
  QField1D_Sequence qflds2(qflds1);

  BOOST_CHECK_EQUAL(  qflds1.nDOF()                ,  qflds2.nDOF() );
  BOOST_CHECK_EQUAL(  qflds1.nElem()               ,  qflds2.nElem() );
  BOOST_CHECK_EQUAL(  qflds1.nInteriorTraceGroups(),  qflds2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qflds1.nBoundaryTraceGroups(),  qflds2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qflds1.nCellGroups()         ,  qflds2.nCellGroups() );
  BOOST_CHECK_EQUAL( &qflds1.getXField()           , &qflds2.getXField() );

  const QFieldLineSequenceClass& qfldGroup2 = qflds2.getCellGroup<Line>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup2.nFieldGroups() );
  BOOST_CHECK_EQUAL( 4, qfldGroup2.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup2.nElem() );

  BOOST_CHECK_EQUAL( 4, qfldGroup2[0].nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup2[0].nElem() );


  QField1D_Sequence qflds3(2, FieldConstructor<Field_DG_InteriorTrace>(), xfld1, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 2, qflds3.nFields() );
  BOOST_CHECK_EQUAL( 1, qflds3.nDOF() );
  BOOST_CHECK_EQUAL( 1, qflds3.nElem() );
  BOOST_CHECK_EQUAL( 1, qflds3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qflds3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qflds3.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qflds3.getXField() );

  const FieldType& qfld3 = qflds3[0];

  BOOST_CHECK_EQUAL( 1, qfld3.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld3.nElem() );
  BOOST_CHECK_EQUAL( 1, qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld3.getXField() );

  const QFieldNodeSequenceClass& qfldGroup3 = qflds3.getInteriorTraceGroup<Node>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup3.nFieldGroups() );
  BOOST_CHECK_EQUAL( 1, qfldGroup3.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldGroup3.nElem() );

  BOOST_CHECK_EQUAL( 1, qfldGroup3[0].nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldGroup3[0].nElem() );


  QField1D_Sequence qflds4(2, FieldConstructor<Field_DG_BoundaryTrace>(), xfld1, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 2, qflds4.nFields() );
  BOOST_CHECK_EQUAL( 2, qflds4.nDOF() );
  BOOST_CHECK_EQUAL( 2, qflds4.nElem() );
  BOOST_CHECK_EQUAL( 0, qflds4.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 2, qflds4.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qflds4.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qflds4.getXField() );

  const FieldType& qfld4 = qflds4[0];

  BOOST_CHECK_EQUAL( 2, qfld4.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfld4.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld4.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 2, qfld4.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld4.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld4.getXField() );

  const QFieldNodeSequenceClass& qfldGroup4 = qflds4.getBoundaryTraceGroup<Node>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup4.nFieldGroups() );
  BOOST_CHECK_EQUAL( 2, qfldGroup4.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldGroup4.nElem() );

  BOOST_CHECK_EQUAL( 2, qfldGroup4[0].nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldGroup4[0].nElem() );


  QField1D_Sequence qflds5(2, qfld4);

  BOOST_CHECK_EQUAL( 2, qflds5.nFields() );
  BOOST_CHECK_EQUAL( 2, qflds5.nDOF() );
  BOOST_CHECK_EQUAL( 2, qflds5.nElem() );
  BOOST_CHECK_EQUAL( 0, qflds5.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 2, qflds5.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qflds5.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qflds5.getXField() );

  const FieldType& qfld5 = qflds5[0];

  BOOST_CHECK_EQUAL( 2, qfld5.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfld5.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld5.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 2, qfld5.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld5.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld5.getXField() );

  const QFieldNodeSequenceClass& qfldGroup5 = qflds5.getBoundaryTraceGroup<Node>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup5.nFieldGroups() );
  BOOST_CHECK_EQUAL( 2, qfldGroup5.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldGroup5.nElem() );

  BOOST_CHECK_EQUAL( 2, qfldGroup5[0].nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldGroup5[0].nElem() );


  // can't have a zero length array...
  BOOST_CHECK_THROW( QField1D_Sequence qflds6(0, FieldConstructor<Field_DG_Cell>(), xfld1,
                                              order, BasisFunctionCategory_Legendre);, AssertionException );

  // Check element extraction
  qflds1[0] = 1;
  qflds1[1] = 2;

  ElementType qfldElem(qfldGroup1.basis());

  qfldGroup1.getElement(qfldElem, 0);

  Real tol = 1e-12;
  BOOST_CHECK_CLOSE(qfldElem[0].DOF(0)[0], 1, tol);
  BOOST_CHECK_CLOSE(qfldElem[1].DOF(0)[0], 2, tol);

  ElementType qfldElem2(qfldElem);

  BOOST_CHECK_CLOSE(qfldElem2[0].DOF(0)[0], 1, tol);
  BOOST_CHECK_CLOSE(qfldElem2[1].DOF(0)[0], 2, tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_2D_Triangle )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field< PhysD2, TopoD2, ArrayQ > FieldType;
  typedef FieldSequence<PhysD2, TopoD2, ArrayQ> QField2D_Sequence;
  typedef QField2D_Sequence::FieldTraceGroupType<Line> QFieldLineSequenceClass;
  typedef QField2D_Sequence::FieldCellGroupType<Triangle> QFieldTriangleSequenceClass;
  typedef QFieldTriangleSequenceClass::ElementType<> ElementType;

  XField2D_2Triangle_X1_1Group xfld1;

  int order = 1;
  QField2D_Sequence qflds1(2, FieldConstructor<Field_CG_Cell>(), xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, qflds1.nFields() );
  BOOST_CHECK_EQUAL( 4, qflds1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qflds1.nElem() );
  BOOST_CHECK_EQUAL( 0, qflds1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qflds1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qflds1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qflds1.getXField() );

  const FieldType& qfld1 = qflds1[0];

  BOOST_CHECK_EQUAL( 4, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfld1.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldTriangleSequenceClass& qfldGroup1 = qflds1.getCellGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup1.nFieldGroups() );
  BOOST_CHECK_EQUAL( 4, qfldGroup1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup1.nElem() );

  BOOST_CHECK_EQUAL( 4, qfldGroup1[0].nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup1[0].nElem() );


  QField2D_Sequence qflds2(qflds1);

  BOOST_CHECK_EQUAL(  qflds1.nDOF()                ,  qflds2.nDOF() );
  BOOST_CHECK_EQUAL(  qflds1.nElem()               ,  qflds2.nElem() );
  BOOST_CHECK_EQUAL(  qflds1.nInteriorTraceGroups(),  qflds2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qflds1.nBoundaryTraceGroups(),  qflds2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qflds1.nCellGroups()         ,  qflds2.nCellGroups() );
  BOOST_CHECK_EQUAL( &qflds1.getXField()           , &qflds2.getXField() );

  const QFieldTriangleSequenceClass& qfldGroup2 = qflds2.getCellGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup2.nFieldGroups() );
  BOOST_CHECK_EQUAL( 4, qfldGroup2.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup2.nElem() );

  BOOST_CHECK_EQUAL( 4, qfldGroup2[0].nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup2[0].nElem() );


  QField2D_Sequence qflds3(2, FieldConstructor<Field_CG_BoundaryTrace>(), xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, qflds3.nFields() );
  BOOST_CHECK_EQUAL( 4, qflds3.nDOF() );
  BOOST_CHECK_EQUAL( 4, qflds3.nElem() );
  BOOST_CHECK_EQUAL( 0, qflds3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qflds3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qflds3.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qflds3.getXField() );

  const FieldType& qfld4 = qflds3[0];

  BOOST_CHECK_EQUAL( 4, qfld4.nDOF() );
  BOOST_CHECK_EQUAL( 4, qfld4.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld4.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld4.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld4.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld4.getXField() );

  const QFieldLineSequenceClass& qfldGroup4 = qflds3.getBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup4.nFieldGroups() );
  BOOST_CHECK_EQUAL( 4, qfldGroup4.nDOF() );
  BOOST_CHECK_EQUAL( 4, qfldGroup4.nElem() );

  BOOST_CHECK_EQUAL( 4, qfldGroup4[0].nDOF() );
  BOOST_CHECK_EQUAL( 4, qfldGroup4[0].nElem() );


  // can't have a zero length array...
  BOOST_CHECK_THROW( QField2D_Sequence qflds5(0, FieldConstructor<Field_CG_Cell>(), xfld1,
                                              order, BasisFunctionCategory_Hierarchical);, AssertionException );


  // Check element extraction
  qflds1[0] = 1;
  qflds1[1] = 2;

  ElementType qfldElem(qfldGroup1.basis());

  qfldGroup1.getElement(qfldElem, 0);

  Real tol = 1e-12;
  BOOST_CHECK_CLOSE(qfldElem[0].DOF(0)[0], 1, tol);
  BOOST_CHECK_CLOSE(qfldElem[1].DOF(0)[0], 2, tol);

  ElementType qfldElem2(qfldElem);

  BOOST_CHECK_CLOSE(qfldElem2[0].DOF(0)[0], 1, tol);
  BOOST_CHECK_CLOSE(qfldElem2[1].DOF(0)[0], 2, tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_2D_Quad )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field< PhysD2, TopoD2, ArrayQ > FieldType;
  typedef FieldSequence<PhysD2, TopoD2, ArrayQ> QField2D_Sequence;
  typedef QField2D_Sequence::FieldTraceGroupType<Line> QFieldLineSequenceClass;
  typedef QField2D_Sequence::FieldCellGroupType<Quad> QFieldQuadSequenceClass;
  typedef QFieldQuadSequenceClass::ElementType<> ElementType;

  XField2D_2Quad_X1_1Group xfld1;

  int order = 1;
  QField2D_Sequence qflds1(2, FieldConstructor<Field_CG_Cell>(), xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, qflds1.nFields() );
  BOOST_CHECK_EQUAL( 6, qflds1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qflds1.nElem() );
  BOOST_CHECK_EQUAL( 0, qflds1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qflds1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qflds1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qflds1.getXField() );

  const FieldType& qfld1 = qflds1[0];

  BOOST_CHECK_EQUAL( 6, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfld1.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldQuadSequenceClass& qfldGroup1 = qflds1.getCellGroup<Quad>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup1.nFieldGroups() );
  BOOST_CHECK_EQUAL( 6, qfldGroup1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup1.nElem() );

  BOOST_CHECK_EQUAL( 6, qfldGroup1[0].nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup1[0].nElem() );


  QField2D_Sequence qflds2(qflds1);

  BOOST_CHECK_EQUAL(  qflds1.nDOF()                ,  qflds2.nDOF() );
  BOOST_CHECK_EQUAL(  qflds1.nElem()               ,  qflds2.nElem() );
  BOOST_CHECK_EQUAL(  qflds1.nInteriorTraceGroups(),  qflds2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qflds1.nBoundaryTraceGroups(),  qflds2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qflds1.nCellGroups()         ,  qflds2.nCellGroups() );
  BOOST_CHECK_EQUAL( &qflds1.getXField()           , &qflds2.getXField() );

  const QFieldQuadSequenceClass& qfldGroup2 = qflds2.getCellGroup<Quad>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup2.nFieldGroups() );
  BOOST_CHECK_EQUAL( 6, qfldGroup2.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup2.nElem() );

  BOOST_CHECK_EQUAL( 6, qfldGroup2[0].nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup2[0].nElem() );


  QField2D_Sequence qflds3(2, FieldConstructor<Field_CG_BoundaryTrace>(), xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, qflds3.nFields() );
  BOOST_CHECK_EQUAL( 6, qflds3.nDOF() );
  BOOST_CHECK_EQUAL( 6, qflds3.nElem() );
  BOOST_CHECK_EQUAL( 0, qflds3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qflds3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qflds3.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qflds3.getXField() );

  const FieldType& qfld4 = qflds3[0];

  BOOST_CHECK_EQUAL( 6, qfld4.nDOF() );
  BOOST_CHECK_EQUAL( 6, qfld4.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld4.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld4.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld4.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld4.getXField() );

  const QFieldLineSequenceClass& qfldGroup4 = qflds3.getBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup4.nFieldGroups() );
  BOOST_CHECK_EQUAL( 6, qfldGroup4.nDOF() );
  BOOST_CHECK_EQUAL( 6, qfldGroup4.nElem() );

  BOOST_CHECK_EQUAL( 6, qfldGroup4[0].nDOF() );
  BOOST_CHECK_EQUAL( 6, qfldGroup4[0].nElem() );


  // can't have a zero length array...
  BOOST_CHECK_THROW( QField2D_Sequence qflds5(0, FieldConstructor<Field_CG_Cell>(), xfld1,
                                              order, BasisFunctionCategory_Hierarchical);, AssertionException );


  // Check element extraction
  qflds1[0] = 1;
  qflds1[1] = 2;

  ElementType qfldElem(qfldGroup1.basis());

  qfldGroup1.getElement(qfldElem, 0);

  Real tol = 1e-12;
  BOOST_CHECK_CLOSE(qfldElem[0].DOF(0)[0], 1, tol);
  BOOST_CHECK_CLOSE(qfldElem[1].DOF(0)[0], 2, tol);

  ElementType qfldElem2(qfldElem);

  BOOST_CHECK_CLOSE(qfldElem2[0].DOF(0)[0], 1, tol);
  BOOST_CHECK_CLOSE(qfldElem2[1].DOF(0)[0], 2, tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_2D_Triangle )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field< PhysD2, TopoD2, ArrayQ > FieldType;
  typedef FieldSequence<PhysD2, TopoD2, ArrayQ> QField2D_Sequence;
  typedef QField2D_Sequence::FieldTraceGroupType<Line> QFieldLineSequenceClass;
  typedef QField2D_Sequence::FieldCellGroupType<Triangle> QFieldTriangleSequenceClass;
  typedef QFieldTriangleSequenceClass::ElementType<> ElementType;

  XField2D_2Triangle_X1_1Group xfld1;

  int order = 1;
  QField2D_Sequence qflds1(2, FieldConstructor<Field_DG_Cell>(), xfld1, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 2, qflds1.nFields() );
  BOOST_CHECK_EQUAL( 6, qflds1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qflds1.nElem() );
  BOOST_CHECK_EQUAL( 0, qflds1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qflds1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qflds1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qflds1.getXField() );

  const FieldType& qfld1 = qflds1[0];

  BOOST_CHECK_EQUAL( 6, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfld1.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldTriangleSequenceClass& qfldGroup1 = qflds1.getCellGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup1.nFieldGroups() );
  BOOST_CHECK_EQUAL( 6, qfldGroup1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup1.nElem() );

  BOOST_CHECK_EQUAL( 6, qfldGroup1[0].nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup1[0].nElem() );

  // copy constructor
  QField2D_Sequence qflds2(qflds1);

  BOOST_CHECK_EQUAL(  qflds1.nDOF()                ,  qflds2.nDOF() );
  BOOST_CHECK_EQUAL(  qflds1.nElem()               ,  qflds2.nElem() );
  BOOST_CHECK_EQUAL(  qflds1.nInteriorTraceGroups(),  qflds2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qflds1.nBoundaryTraceGroups(),  qflds2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qflds1.nCellGroups()         ,  qflds2.nCellGroups() );
  BOOST_CHECK_EQUAL( &qflds1.getXField()           , &qflds2.getXField() );

  const QFieldTriangleSequenceClass& qfldGroup2 = qflds2.getCellGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup2.nFieldGroups() );
  BOOST_CHECK_EQUAL( 6, qfldGroup2.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup2.nElem() );

  BOOST_CHECK_EQUAL( 6, qfldGroup2[0].nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup2[0].nElem() );


  QField2D_Sequence qflds3(2, FieldConstructor<Field_DG_InteriorTrace>(), xfld1, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 2, qflds3.nFields() );
  BOOST_CHECK_EQUAL( 2, qflds3.nDOF() );
  BOOST_CHECK_EQUAL( 1, qflds3.nElem() );
  BOOST_CHECK_EQUAL( 1, qflds3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qflds3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qflds3.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qflds3.getXField() );

  const FieldType& qfld3 = qflds3[0];

  BOOST_CHECK_EQUAL( 2, qfld3.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld3.nElem() );
  BOOST_CHECK_EQUAL( 1, qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld3.getXField() );

  const QFieldLineSequenceClass& qfldGroup3 = qflds3.getInteriorTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup3.nFieldGroups() );
  BOOST_CHECK_EQUAL( 2, qfldGroup3.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldGroup3.nElem() );

  BOOST_CHECK_EQUAL( 2, qfldGroup3[0].nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldGroup3[0].nElem() );


  QField2D_Sequence qflds4(2, FieldConstructor<Field_DG_BoundaryTrace>(), xfld1, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 2, qflds4.nFields() );
  BOOST_CHECK_EQUAL( 8, qflds4.nDOF() );
  BOOST_CHECK_EQUAL( 4, qflds4.nElem() );
  BOOST_CHECK_EQUAL( 0, qflds4.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qflds4.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qflds4.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qflds4.getXField() );

  const FieldType& qfld4 = qflds4[0];

  BOOST_CHECK_EQUAL( 8, qfld4.nDOF() );
  BOOST_CHECK_EQUAL( 4, qfld4.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld4.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld4.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld4.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld4.getXField() );

  const QFieldLineSequenceClass& qfldGroup4 = qflds4.getBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup4.nFieldGroups() );
  BOOST_CHECK_EQUAL( 8, qfldGroup4.nDOF() );
  BOOST_CHECK_EQUAL( 4, qfldGroup4.nElem() );

  BOOST_CHECK_EQUAL( 8, qfldGroup4[0].nDOF() );
  BOOST_CHECK_EQUAL( 4, qfldGroup4[0].nElem() );

  // can't have a zero length array...
  BOOST_CHECK_THROW( QField2D_Sequence qflds5(0, FieldConstructor<Field_DG_Cell>(), xfld1,
                                              order, BasisFunctionCategory_Legendre);, AssertionException );


  // Check element extraction
  qflds1[0] = 1;
  qflds1[1] = 2;

  ElementType qfldElem(qfldGroup1.basis());

  qfldGroup1.getElement(qfldElem, 0);

  Real tol = 1e-12;
  BOOST_CHECK_CLOSE(qfldElem[0].DOF(0)[0], 1, tol);
  BOOST_CHECK_CLOSE(qfldElem[1].DOF(0)[0], 2, tol);

  ElementType qfldElem2(qfldElem);

  BOOST_CHECK_CLOSE(qfldElem2[0].DOF(0)[0], 1, tol);
  BOOST_CHECK_CLOSE(qfldElem2[1].DOF(0)[0], 2, tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_2D_Quad )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field< PhysD2, TopoD2, ArrayQ > FieldType;
  typedef FieldSequence<PhysD2, TopoD2, ArrayQ> QField2D_Sequence;
  typedef QField2D_Sequence::FieldTraceGroupType<Line> QFieldLineSequenceClass;
  typedef QField2D_Sequence::FieldCellGroupType<Quad> QFieldQuadSequenceClass;
  typedef QFieldQuadSequenceClass::ElementType<> ElementType;

  XField2D_2Quad_X1_1Group xfld1;

  int order = 1;
  QField2D_Sequence qflds1(2, FieldConstructor<Field_DG_Cell>(), xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, qflds1.nFields() );
  BOOST_CHECK_EQUAL( 8, qflds1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qflds1.nElem() );
  BOOST_CHECK_EQUAL( 0, qflds1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qflds1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qflds1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qflds1.getXField() );

  const FieldType& qfld1 = qflds1[0];

  BOOST_CHECK_EQUAL( 8, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfld1.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldQuadSequenceClass& qfldGroup1 = qflds1.getCellGroup<Quad>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup1.nFieldGroups() );
  BOOST_CHECK_EQUAL( 8, qfldGroup1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup1.nElem() );

  BOOST_CHECK_EQUAL( 8, qfldGroup1[0].nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup1[0].nElem() );

  // copy constructor
  QField2D_Sequence qflds2(qflds1);

  BOOST_CHECK_EQUAL(  qflds1.nDOF()                ,  qflds2.nDOF() );
  BOOST_CHECK_EQUAL(  qflds1.nElem()               ,  qflds2.nElem() );
  BOOST_CHECK_EQUAL(  qflds1.nInteriorTraceGroups(),  qflds2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qflds1.nBoundaryTraceGroups(),  qflds2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qflds1.nCellGroups()         ,  qflds2.nCellGroups() );
  BOOST_CHECK_EQUAL( &qflds1.getXField()           , &qflds2.getXField() );

  const QFieldQuadSequenceClass& qfldGroup2 = qflds2.getCellGroup<Quad>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup2.nFieldGroups() );
  BOOST_CHECK_EQUAL( 8, qfldGroup2.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup2.nElem() );

  BOOST_CHECK_EQUAL( 8, qfldGroup2[0].nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup2[0].nElem() );


  QField2D_Sequence qflds3(2, FieldConstructor<Field_DG_InteriorTrace>(), xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, qflds3.nFields() );
  BOOST_CHECK_EQUAL( 2, qflds3.nDOF() );
  BOOST_CHECK_EQUAL( 1, qflds3.nElem() );
  BOOST_CHECK_EQUAL( 1, qflds3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qflds3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qflds3.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qflds3.getXField() );

  const FieldType& qfld3 = qflds3[0];

  BOOST_CHECK_EQUAL( 2, qfld3.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld3.nElem() );
  BOOST_CHECK_EQUAL( 1, qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld3.getXField() );

  const QFieldLineSequenceClass& qfldGroup3 = qflds3.getInteriorTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup3.nFieldGroups() );
  BOOST_CHECK_EQUAL( 2, qfldGroup3.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldGroup3.nElem() );

  BOOST_CHECK_EQUAL( 2, qfldGroup3[0].nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldGroup3[0].nElem() );


  QField2D_Sequence qflds4(2, FieldConstructor<Field_DG_BoundaryTrace>(), xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, qflds4.nFields() );
  BOOST_CHECK_EQUAL(12, qflds4.nDOF() );
  BOOST_CHECK_EQUAL( 6, qflds4.nElem() );
  BOOST_CHECK_EQUAL( 0, qflds4.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qflds4.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qflds4.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qflds4.getXField() );

  const FieldType& qfld4 = qflds4[0];

  BOOST_CHECK_EQUAL(12, qfld4.nDOF() );
  BOOST_CHECK_EQUAL( 6, qfld4.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld4.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld4.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld4.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld4.getXField() );

  const QFieldLineSequenceClass& qfldGroup4 = qflds4.getBoundaryTraceGroup<Line>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup4.nFieldGroups() );
  BOOST_CHECK_EQUAL(12, qfldGroup4.nDOF() );
  BOOST_CHECK_EQUAL( 6, qfldGroup4.nElem() );

  BOOST_CHECK_EQUAL(12, qfldGroup4[0].nDOF() );
  BOOST_CHECK_EQUAL( 6, qfldGroup4[0].nElem() );

  // can't have a zero length array...
  BOOST_CHECK_THROW( QField2D_Sequence qflds5(0, FieldConstructor<Field_DG_Cell>(), xfld1,
                                              order, BasisFunctionCategory_Hierarchical);, AssertionException );


  // Check element extraction
  qflds1[0] = 1;
  qflds1[1] = 2;

  ElementType qfldElem(qfldGroup1.basis());

  qfldGroup1.getElement(qfldElem, 0);

  Real tol = 1e-12;
  BOOST_CHECK_CLOSE(qfldElem[0].DOF(0)[0], 1, tol);
  BOOST_CHECK_CLOSE(qfldElem[1].DOF(0)[0], 2, tol);

  ElementType qfldElem2(qfldElem);

  BOOST_CHECK_CLOSE(qfldElem2[0].DOF(0)[0], 1, tol);
  BOOST_CHECK_CLOSE(qfldElem2[1].DOF(0)[0], 2, tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_3D_Tet )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field< PhysD3, TopoD3, ArrayQ > FieldType;
  typedef FieldSequence<PhysD3, TopoD3, ArrayQ> QField3D_Sequence;
  typedef QField3D_Sequence::FieldTraceGroupType<Triangle> QFieldTriangleSequenceClass;
  typedef QField3D_Sequence::FieldCellGroupType<Tet> QFieldTetSequenceClass;
  typedef QFieldTetSequenceClass::ElementType<> ElementType;

  XField3D_2Tet_X1_1Group xfld1;

  int order = 1;
  QField3D_Sequence qflds1(2, FieldConstructor<Field_CG_Cell>(), xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, qflds1.nFields() );
  BOOST_CHECK_EQUAL( 5, qflds1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qflds1.nElem() );
  BOOST_CHECK_EQUAL( 0, qflds1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 6, qflds1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qflds1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qflds1.getXField() );

  const FieldType& qfld1 = qflds1[0];

  BOOST_CHECK_EQUAL( 5, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfld1.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 6, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldTetSequenceClass& qfldGroup1 = qflds1.getCellGroup<Tet>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup1.nFieldGroups() );
  BOOST_CHECK_EQUAL( 5, qfldGroup1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup1.nElem() );

  BOOST_CHECK_EQUAL( 5, qfldGroup1[0].nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup1[0].nElem() );


  QField3D_Sequence qflds2(qflds1);

  BOOST_CHECK_EQUAL(  qflds1.nDOF()                ,  qflds2.nDOF() );
  BOOST_CHECK_EQUAL(  qflds1.nElem()               ,  qflds2.nElem() );
  BOOST_CHECK_EQUAL(  qflds1.nInteriorTraceGroups(),  qflds2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qflds1.nBoundaryTraceGroups(),  qflds2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qflds1.nCellGroups()         ,  qflds2.nCellGroups() );
  BOOST_CHECK_EQUAL( &qflds1.getXField()           , &qflds2.getXField() );

  const QFieldTetSequenceClass& qfldGroup2 = qflds2.getCellGroup<Tet>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup2.nFieldGroups() );
  BOOST_CHECK_EQUAL( 5, qfldGroup2.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup2.nElem() );

  BOOST_CHECK_EQUAL( 5, qfldGroup2[0].nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup2[0].nElem() );


  QField3D_Sequence qflds3(2, FieldConstructor<Field_CG_BoundaryTrace>(), xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, qflds3.nFields() );
  BOOST_CHECK_EQUAL( 5, qflds3.nDOF() );
  BOOST_CHECK_EQUAL( 6, qflds3.nElem() );
  BOOST_CHECK_EQUAL( 0, qflds3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 6, qflds3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qflds3.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qflds3.getXField() );

  const FieldType& qfld4 = qflds3[0];

  BOOST_CHECK_EQUAL( 5, qfld4.nDOF() );
  BOOST_CHECK_EQUAL( 6, qfld4.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld4.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 6, qfld4.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld4.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld4.getXField() );

  const QFieldTriangleSequenceClass& qfldGroup4 = qflds3.getBoundaryTraceGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup4.nFieldGroups() );
  BOOST_CHECK_EQUAL( 5, qfldGroup4.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldGroup4.nElem() );

  BOOST_CHECK_EQUAL( 5, qfldGroup4[0].nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldGroup4[0].nElem() );


  // can't have a zero length array...
  BOOST_CHECK_THROW( QField3D_Sequence qflds5(0, FieldConstructor<Field_CG_Cell>(), xfld1,
                                              order, BasisFunctionCategory_Hierarchical);, AssertionException );


  // Check element extraction
  qflds1[0] = 1;
  qflds1[1] = 2;

  ElementType qfldElem(qfldGroup1.basis());

  qfldGroup1.getElement(qfldElem, 0);

  Real tol = 1e-12;
  BOOST_CHECK_CLOSE(qfldElem[0].DOF(0)[0], 1, tol);
  BOOST_CHECK_CLOSE(qfldElem[1].DOF(0)[0], 2, tol);

  ElementType qfldElem2(qfldElem);

  BOOST_CHECK_CLOSE(qfldElem2[0].DOF(0)[0], 1, tol);
  BOOST_CHECK_CLOSE(qfldElem2[1].DOF(0)[0], 2, tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_3D_Hex )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field< PhysD3, TopoD3, ArrayQ > FieldType;
  typedef FieldSequence<PhysD3, TopoD3, ArrayQ> QField3D_Sequence;
  typedef QField3D_Sequence::FieldTraceGroupType<Quad> QFieldQuadSequenceClass;
  typedef QField3D_Sequence::FieldCellGroupType<Hex> QFieldHexSequenceClass;
  typedef QFieldHexSequenceClass::ElementType<> ElementType;

  XField3D_2Hex_X1_1Group xfld1;

  int order = 1;
  QField3D_Sequence qflds1(2, FieldConstructor<Field_CG_Cell>(), xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, qflds1.nFields() );
  BOOST_CHECK_EQUAL(12, qflds1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qflds1.nElem() );
  BOOST_CHECK_EQUAL( 0, qflds1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 6, qflds1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qflds1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qflds1.getXField() );

  const FieldType& qfld1 = qflds1[0];

  BOOST_CHECK_EQUAL(12, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfld1.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 6, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldHexSequenceClass& qfldGroup1 = qflds1.getCellGroup<Hex>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup1.nFieldGroups() );
  BOOST_CHECK_EQUAL(12, qfldGroup1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup1.nElem() );

  BOOST_CHECK_EQUAL(12, qfldGroup1[0].nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup1[0].nElem() );


  QField3D_Sequence qflds2(qflds1);

  BOOST_CHECK_EQUAL(  qflds1.nDOF()                ,  qflds2.nDOF() );
  BOOST_CHECK_EQUAL(  qflds1.nElem()               ,  qflds2.nElem() );
  BOOST_CHECK_EQUAL(  qflds1.nInteriorTraceGroups(),  qflds2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qflds1.nBoundaryTraceGroups(),  qflds2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qflds1.nCellGroups()         ,  qflds2.nCellGroups() );
  BOOST_CHECK_EQUAL( &qflds1.getXField()           , &qflds2.getXField() );

  const QFieldHexSequenceClass& qfldGroup2 = qflds2.getCellGroup<Hex>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup2.nFieldGroups() );
  BOOST_CHECK_EQUAL(12, qfldGroup2.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup2.nElem() );

  BOOST_CHECK_EQUAL(12, qfldGroup2[0].nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup2[0].nElem() );


  QField3D_Sequence qflds3(2, FieldConstructor<Field_CG_BoundaryTrace>(), xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, qflds3.nFields() );
  BOOST_CHECK_EQUAL(12, qflds3.nDOF() );
  BOOST_CHECK_EQUAL(10, qflds3.nElem() );
  BOOST_CHECK_EQUAL( 0, qflds3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 6, qflds3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qflds3.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qflds3.getXField() );

  const FieldType& qfld4 = qflds3[0];

  BOOST_CHECK_EQUAL(12, qfld4.nDOF() );
  BOOST_CHECK_EQUAL(10, qfld4.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld4.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 6, qfld4.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld4.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld4.getXField() );

  const QFieldQuadSequenceClass& qfldGroup4 = qflds3.getBoundaryTraceGroup<Quad>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup4.nFieldGroups() );
  BOOST_CHECK_EQUAL(12, qfldGroup4.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldGroup4.nElem() );

  BOOST_CHECK_EQUAL(12, qfldGroup4[0].nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldGroup4[0].nElem() );


  // can't have a zero length array...
  BOOST_CHECK_THROW( QField3D_Sequence qflds5(0, FieldConstructor<Field_CG_Cell>(), xfld1,
                                              order, BasisFunctionCategory_Hierarchical);, AssertionException );


  // Check element extraction
  qflds1[0] = 1;
  qflds1[1] = 2;

  ElementType qfldElem(qfldGroup1.basis());

  qfldGroup1.getElement(qfldElem, 0);

  Real tol = 1e-12;
  BOOST_CHECK_CLOSE(qfldElem[0].DOF(0)[0], 1, tol);
  BOOST_CHECK_CLOSE(qfldElem[1].DOF(0)[0], 2, tol);

  ElementType qfldElem2(qfldElem);

  BOOST_CHECK_CLOSE(qfldElem2[0].DOF(0)[0], 1, tol);
  BOOST_CHECK_CLOSE(qfldElem2[1].DOF(0)[0], 2, tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_2D_Tet )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field< PhysD3, TopoD3, ArrayQ > FieldType;
  typedef FieldSequence<PhysD3, TopoD3, ArrayQ> QField3D_Sequence;
  typedef QField3D_Sequence::FieldTraceGroupType<Triangle> QFieldTriangleSequenceClass;
  typedef QField3D_Sequence::FieldCellGroupType<Tet> QFieldTetSequenceClass;
  typedef QFieldTetSequenceClass::ElementType<> ElementType;

  XField3D_2Tet_X1_1Group xfld1;

  int order = 1;
  QField3D_Sequence qflds1(2, FieldConstructor<Field_DG_Cell>(), xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, qflds1.nFields() );
  BOOST_CHECK_EQUAL( 8, qflds1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qflds1.nElem() );
  BOOST_CHECK_EQUAL( 0, qflds1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qflds1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qflds1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qflds1.getXField() );

  const FieldType& qfld1 = qflds1[0];

  BOOST_CHECK_EQUAL( 8, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfld1.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldTetSequenceClass& qfldGroup1 = qflds1.getCellGroup<Tet>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup1.nFieldGroups() );
  BOOST_CHECK_EQUAL( 8, qfldGroup1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup1.nElem() );

  BOOST_CHECK_EQUAL( 8, qfldGroup1[0].nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup1[0].nElem() );

  // copy constructor
  QField3D_Sequence qflds2(qflds1);

  BOOST_CHECK_EQUAL(  qflds1.nDOF()                ,  qflds2.nDOF() );
  BOOST_CHECK_EQUAL(  qflds1.nElem()               ,  qflds2.nElem() );
  BOOST_CHECK_EQUAL(  qflds1.nInteriorTraceGroups(),  qflds2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qflds1.nBoundaryTraceGroups(),  qflds2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qflds1.nCellGroups()         ,  qflds2.nCellGroups() );
  BOOST_CHECK_EQUAL( &qflds1.getXField()           , &qflds2.getXField() );

  const QFieldTetSequenceClass& qfldGroup2 = qflds2.getCellGroup<Tet>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup2.nFieldGroups() );
  BOOST_CHECK_EQUAL( 8, qfldGroup2.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup2.nElem() );

  BOOST_CHECK_EQUAL( 8, qfldGroup2[0].nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup2[0].nElem() );


  QField3D_Sequence qflds3(2, FieldConstructor<Field_DG_InteriorTrace>(), xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, qflds3.nFields() );
  BOOST_CHECK_EQUAL( 3, qflds3.nDOF() );
  BOOST_CHECK_EQUAL( 1, qflds3.nElem() );
  BOOST_CHECK_EQUAL( 1, qflds3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qflds3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qflds3.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qflds3.getXField() );

  const FieldType& qfld3 = qflds3[0];

  BOOST_CHECK_EQUAL( 3, qfld3.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld3.nElem() );
  BOOST_CHECK_EQUAL( 1, qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld3.getXField() );

  const QFieldTriangleSequenceClass& qfldGroup3 = qflds3.getInteriorTraceGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup3.nFieldGroups() );
  BOOST_CHECK_EQUAL( 3, qfldGroup3.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldGroup3.nElem() );

  BOOST_CHECK_EQUAL( 3, qfldGroup3[0].nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldGroup3[0].nElem() );


  QField3D_Sequence qflds4(2, FieldConstructor<Field_DG_BoundaryTrace>(), xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, qflds4.nFields() );
  BOOST_CHECK_EQUAL(18, qflds4.nDOF() );
  BOOST_CHECK_EQUAL( 6, qflds4.nElem() );
  BOOST_CHECK_EQUAL( 0, qflds4.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 6, qflds4.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qflds4.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qflds4.getXField() );

  const FieldType& qfld4 = qflds4[0];

  BOOST_CHECK_EQUAL(18, qfld4.nDOF() );
  BOOST_CHECK_EQUAL( 6, qfld4.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld4.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 6, qfld4.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld4.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld4.getXField() );

  const QFieldTriangleSequenceClass& qfldGroup4 = qflds4.getBoundaryTraceGroup<Triangle>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup4.nFieldGroups() );
  BOOST_CHECK_EQUAL(18, qfldGroup4.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldGroup4.nElem() );

  BOOST_CHECK_EQUAL(18, qfldGroup4[0].nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldGroup4[0].nElem() );

  // can't have a zero length array...
  BOOST_CHECK_THROW( QField3D_Sequence qflds5(0, FieldConstructor<Field_DG_Cell>(), xfld1,
                                              order, BasisFunctionCategory_Hierarchical);, AssertionException );


  // Check element extraction
  qflds1[0] = 1;
  qflds1[1] = 2;

  ElementType qfldElem(qfldGroup1.basis());

  qfldGroup1.getElement(qfldElem, 0);

  Real tol = 1e-12;
  BOOST_CHECK_CLOSE(qfldElem[0].DOF(0)[0], 1, tol);
  BOOST_CHECK_CLOSE(qfldElem[1].DOF(0)[0], 2, tol);

  ElementType qfldElem2(qfldElem);

  BOOST_CHECK_CLOSE(qfldElem2[0].DOF(0)[0], 1, tol);
  BOOST_CHECK_CLOSE(qfldElem2[1].DOF(0)[0], 2, tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_2D_Hex )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field< PhysD3, TopoD3, ArrayQ > FieldType;
  typedef FieldSequence<PhysD3, TopoD3, ArrayQ> QField3D_Sequence;
  typedef QField3D_Sequence::FieldTraceGroupType<Quad> QFieldQuadSequenceClass;
  typedef QField3D_Sequence::FieldCellGroupType<Hex> QFieldHexSequenceClass;
  typedef QFieldHexSequenceClass::ElementType<> ElementType;

  XField3D_2Hex_X1_1Group xfld1;

  int order = 1;
  QField3D_Sequence qflds1(2, FieldConstructor<Field_DG_Cell>(), xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, qflds1.nFields() );
  BOOST_CHECK_EQUAL(16, qflds1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qflds1.nElem() );
  BOOST_CHECK_EQUAL( 0, qflds1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qflds1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qflds1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qflds1.getXField() );

  const FieldType& qfld1 = qflds1[0];

  BOOST_CHECK_EQUAL(16, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfld1.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldHexSequenceClass& qfldGroup1 = qflds1.getCellGroup<Hex>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup1.nFieldGroups() );
  BOOST_CHECK_EQUAL(16, qfldGroup1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup1.nElem() );

  BOOST_CHECK_EQUAL(16, qfldGroup1[0].nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup1[0].nElem() );

  // copy constructor
  QField3D_Sequence qflds2(qflds1);

  BOOST_CHECK_EQUAL(  qflds1.nDOF()                ,  qflds2.nDOF() );
  BOOST_CHECK_EQUAL(  qflds1.nElem()               ,  qflds2.nElem() );
  BOOST_CHECK_EQUAL(  qflds1.nInteriorTraceGroups(),  qflds2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qflds1.nBoundaryTraceGroups(),  qflds2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qflds1.nCellGroups()         ,  qflds2.nCellGroups() );
  BOOST_CHECK_EQUAL( &qflds1.getXField()           , &qflds2.getXField() );

  const QFieldHexSequenceClass& qfldGroup2 = qflds2.getCellGroup<Hex>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup2.nFieldGroups() );
  BOOST_CHECK_EQUAL(16, qfldGroup2.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup2.nElem() );

  BOOST_CHECK_EQUAL(16, qfldGroup2[0].nDOF() );
  BOOST_CHECK_EQUAL( 2, qfldGroup2[0].nElem() );


  QField3D_Sequence qflds3(2, FieldConstructor<Field_DG_InteriorTrace>(), xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, qflds3.nFields() );
  BOOST_CHECK_EQUAL( 4, qflds3.nDOF() );
  BOOST_CHECK_EQUAL( 1, qflds3.nElem() );
  BOOST_CHECK_EQUAL( 1, qflds3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qflds3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qflds3.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qflds3.getXField() );

  const FieldType& qfld3 = qflds3[0];

  BOOST_CHECK_EQUAL( 4, qfld3.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld3.nElem() );
  BOOST_CHECK_EQUAL( 1, qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld3.getXField() );

  const QFieldQuadSequenceClass& qfldGroup3 = qflds3.getInteriorTraceGroup<Quad>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup3.nFieldGroups() );
  BOOST_CHECK_EQUAL( 4, qfldGroup3.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldGroup3.nElem() );

  BOOST_CHECK_EQUAL( 4, qfldGroup3[0].nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldGroup3[0].nElem() );


  QField3D_Sequence qflds4(2, FieldConstructor<Field_DG_BoundaryTrace>(), xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, qflds4.nFields() );
  BOOST_CHECK_EQUAL(40, qflds4.nDOF() );
  BOOST_CHECK_EQUAL(10, qflds4.nElem() );
  BOOST_CHECK_EQUAL( 0, qflds4.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 6, qflds4.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qflds4.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qflds4.getXField() );

  const FieldType& qfld4 = qflds4[0];

  BOOST_CHECK_EQUAL(40, qfld4.nDOF() );
  BOOST_CHECK_EQUAL(10, qfld4.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld4.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 6, qfld4.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld4.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld4.getXField() );

  const QFieldQuadSequenceClass& qfldGroup4 = qflds4.getBoundaryTraceGroup<Quad>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup4.nFieldGroups() );
  BOOST_CHECK_EQUAL(40, qfldGroup4.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldGroup4.nElem() );

  BOOST_CHECK_EQUAL(40, qfldGroup4[0].nDOF() );
  BOOST_CHECK_EQUAL( 1, qfldGroup4[0].nElem() );

  // can't have a zero length array...
  BOOST_CHECK_THROW( QField3D_Sequence qflds5(0, FieldConstructor<Field_DG_Cell>(), xfld1,
                                              order, BasisFunctionCategory_Hierarchical);, AssertionException );


  // Check element extraction
  qflds1[0] = 1;
  qflds1[1] = 2;

  ElementType qfldElem(qfldGroup1.basis());

  qfldGroup1.getElement(qfldElem, 0);

  Real tol = 1e-12;
  BOOST_CHECK_CLOSE(qfldElem[0].DOF(0)[0], 1, tol);
  BOOST_CHECK_CLOSE(qfldElem[1].DOF(0)[0], 2, tol);

  ElementType qfldElem2(qfldElem);

  BOOST_CHECK_CLOSE(qfldElem2[0].DOF(0)[0], 1, tol);
  BOOST_CHECK_CLOSE(qfldElem2[1].DOF(0)[0], 2, tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( rotate1_test )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field< PhysD1, TopoD1, ArrayQ > FieldType;
  typedef FieldSequence<PhysD1, TopoD1, ArrayQ> QField1D_Sequence;
  typedef FieldType::FieldCellGroupType<Line> QFieldLineClass;
  typedef FieldType::FieldTraceGroupType<Node> QFieldNodeClass;

  XField1D_2Line_X1_1Group xfld;

  int order = 1;
  QField1D_Sequence qfldsCell(1, FieldConstructor<Field_DG_Cell>(), xfld, order, BasisFunctionCategory_Legendre);
  QField1D_Sequence qfldsITrace(1, FieldConstructor<Field_DG_InteriorTrace>(), xfld, order, BasisFunctionCategory_Legendre);
  QField1D_Sequence qfldsBTrace(1, FieldConstructor<Field_DG_BoundaryTrace>(), xfld, order, BasisFunctionCategory_Legendre);

  BOOST_REQUIRE_EQUAL( qfldsCell.nFields(), 1 );
  BOOST_REQUIRE_EQUAL( qfldsITrace.nFields(), 1 );
  BOOST_REQUIRE_EQUAL( qfldsBTrace.nFields(), 1 );

  const FieldType& qfldCell0 = qfldsCell[0];
  const QFieldLineClass& qfldCellGroup0 = qfldsCell.getCellGroup<Line>(0)[0];

  const FieldType& qfldITrace0 = qfldsITrace[0];
  const QFieldNodeClass& qfldITraceGroup0 = qfldsITrace.getInteriorTraceGroup<Node>(0)[0];

  const FieldType& qfldBTrace0 = qfldsBTrace[0];
  const QFieldNodeClass& qfldBTraceGroup0 = qfldsBTrace.getBoundaryTraceGroup<Node>(0)[0];

  // Rotate the last field so it's first. i.e. {fld0} -> {fld0}
  qfldsCell.rotate();
  qfldsITrace.rotate();
  qfldsBTrace.rotate();

  BOOST_CHECK_EQUAL( &qfldCell0, &qfldsCell[0] );
  BOOST_CHECK_EQUAL( &qfldCellGroup0, &qfldsCell.getCellGroup<Line>(0)[0] );

  BOOST_CHECK_EQUAL( &qfldITrace0, &qfldsITrace[0] );
  BOOST_CHECK_EQUAL( &qfldITraceGroup0, &qfldsITrace.getInteriorTraceGroup<Node>(0)[0] );

  BOOST_CHECK_EQUAL( &qfldBTrace0, &qfldsBTrace[0] );
  BOOST_CHECK_EQUAL( &qfldBTraceGroup0, &qfldsBTrace.getBoundaryTraceGroup<Node>(0)[0] );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( rotate2_test )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field< PhysD1, TopoD1, ArrayQ > FieldType;
  typedef FieldSequence<PhysD1, TopoD1, ArrayQ> QField1D_Sequence;
  typedef FieldType::FieldCellGroupType<Line> QFieldLineClass;
  typedef FieldType::FieldTraceGroupType<Node> QFieldNodeClass;

  XField1D_2Line_X1_1Group xfld;

  int order = 1;
  QField1D_Sequence qfldsCell(2, FieldConstructor<Field_DG_Cell>(), xfld, order, BasisFunctionCategory_Legendre);
  QField1D_Sequence qfldsITrace(2, FieldConstructor<Field_DG_InteriorTrace>(), xfld, order, BasisFunctionCategory_Legendre);
  QField1D_Sequence qfldsBTrace(2, FieldConstructor<Field_DG_BoundaryTrace>(), xfld, order, BasisFunctionCategory_Legendre);

  BOOST_REQUIRE_EQUAL( qfldsCell.nFields(), 2 );
  BOOST_REQUIRE_EQUAL( qfldsITrace.nFields(), 2 );
  BOOST_REQUIRE_EQUAL( qfldsBTrace.nFields(), 2 );

  const FieldType& qfldCell0 = qfldsCell[0];
  const FieldType& qfldCell1 = qfldsCell[1];
  const QFieldLineClass& qfldCellGroup0 = qfldsCell.getCellGroup<Line>(0)[0];
  const QFieldLineClass& qfldCellGroup1 = qfldsCell.getCellGroup<Line>(0)[1];

  const FieldType& qfldITrace0 = qfldsITrace[0];
  const FieldType& qfldITrace1 = qfldsITrace[1];
  const QFieldNodeClass& qfldITraceGroup0 = qfldsITrace.getInteriorTraceGroup<Node>(0)[0];
  const QFieldNodeClass& qfldITraceGroup1 = qfldsITrace.getInteriorTraceGroup<Node>(0)[1];

  const FieldType& qfldBTrace0 = qfldsBTrace[0];
  const FieldType& qfldBTrace1 = qfldsBTrace[1];
  const QFieldNodeClass& qfldBTraceGroup0 = qfldsBTrace.getBoundaryTraceGroup<Node>(0)[0];
  const QFieldNodeClass& qfldBTraceGroup1 = qfldsBTrace.getBoundaryTraceGroup<Node>(0)[1];

  // Rotate the last field so it's first. i.e. {fld0, fld1} -> {fld1, fld0}
  qfldsCell.rotate();
  qfldsITrace.rotate();
  qfldsBTrace.rotate();

  BOOST_CHECK_EQUAL( &qfldCell1, &qfldsCell[0] );
  BOOST_CHECK_EQUAL( &qfldCell0, &qfldsCell[1] );
  BOOST_CHECK_EQUAL( &qfldCellGroup1, &qfldsCell.getCellGroup<Line>(0)[0] );
  BOOST_CHECK_EQUAL( &qfldCellGroup0, &qfldsCell.getCellGroup<Line>(0)[1] );

  BOOST_CHECK_EQUAL( &qfldITrace1, &qfldsITrace[0] );
  BOOST_CHECK_EQUAL( &qfldITrace0, &qfldsITrace[1] );
  BOOST_CHECK_EQUAL( &qfldITraceGroup1, &qfldsITrace.getInteriorTraceGroup<Node>(0)[0] );
  BOOST_CHECK_EQUAL( &qfldITraceGroup0, &qfldsITrace.getInteriorTraceGroup<Node>(0)[1] );

  BOOST_CHECK_EQUAL( &qfldBTrace1, &qfldsBTrace[0] );
  BOOST_CHECK_EQUAL( &qfldBTrace0, &qfldsBTrace[1] );
  BOOST_CHECK_EQUAL( &qfldBTraceGroup1, &qfldsBTrace.getBoundaryTraceGroup<Node>(0)[0] );
  BOOST_CHECK_EQUAL( &qfldBTraceGroup0, &qfldsBTrace.getBoundaryTraceGroup<Node>(0)[1] );

  qfldsCell.rotate();
  qfldsITrace.rotate();
  qfldsBTrace.rotate();

  BOOST_CHECK_EQUAL( &qfldCell0, &qfldsCell[0] );
  BOOST_CHECK_EQUAL( &qfldCell1, &qfldsCell[1] );
  BOOST_CHECK_EQUAL( &qfldCellGroup0, &qfldsCell.getCellGroup<Line>(0)[0] );
  BOOST_CHECK_EQUAL( &qfldCellGroup1, &qfldsCell.getCellGroup<Line>(0)[1] );

  BOOST_CHECK_EQUAL( &qfldITrace0, &qfldsITrace[0] );
  BOOST_CHECK_EQUAL( &qfldITrace1, &qfldsITrace[1] );
  BOOST_CHECK_EQUAL( &qfldITraceGroup0, &qfldsITrace.getInteriorTraceGroup<Node>(0)[0] );
  BOOST_CHECK_EQUAL( &qfldITraceGroup1, &qfldsITrace.getInteriorTraceGroup<Node>(0)[1] );

  BOOST_CHECK_EQUAL( &qfldBTrace0, &qfldsBTrace[0] );
  BOOST_CHECK_EQUAL( &qfldBTrace1, &qfldsBTrace[1] );
  BOOST_CHECK_EQUAL( &qfldBTraceGroup0, &qfldsBTrace.getBoundaryTraceGroup<Node>(0)[0] );
  BOOST_CHECK_EQUAL( &qfldBTraceGroup1, &qfldsBTrace.getBoundaryTraceGroup<Node>(0)[1] );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( rotate4_test )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field< PhysD1, TopoD1, ArrayQ > FieldType;
  typedef FieldSequence<PhysD1, TopoD1, ArrayQ> QField1D_Sequence;
  typedef FieldType::FieldCellGroupType<Line> QFieldLineClass;
  typedef FieldType::FieldTraceGroupType<Node> QFieldNodeClass;

  XField1D_2Line_X1_1Group xfld;

  int order = 1;
  QField1D_Sequence qfldsCell(4, FieldConstructor<Field_DG_Cell>(), xfld, order, BasisFunctionCategory_Legendre);
  QField1D_Sequence qfldsITrace(4, FieldConstructor<Field_DG_InteriorTrace>(), xfld, order, BasisFunctionCategory_Legendre);
  QField1D_Sequence qfldsBTrace(4, FieldConstructor<Field_DG_BoundaryTrace>(), xfld, order, BasisFunctionCategory_Legendre);

  BOOST_REQUIRE_EQUAL( qfldsCell.nFields(), 4 );
  BOOST_REQUIRE_EQUAL( qfldsITrace.nFields(), 4 );
  BOOST_REQUIRE_EQUAL( qfldsBTrace.nFields(), 4 );

  const FieldType& qfldCell0 = qfldsCell[0];
  const FieldType& qfldCell1 = qfldsCell[1];
  const FieldType& qfldCell2 = qfldsCell[2];
  const FieldType& qfldCell3 = qfldsCell[3];
  const QFieldLineClass& qfldCellGroup0 = qfldsCell.getCellGroup<Line>(0)[0];
  const QFieldLineClass& qfldCellGroup1 = qfldsCell.getCellGroup<Line>(0)[1];
  const QFieldLineClass& qfldCellGroup2 = qfldsCell.getCellGroup<Line>(0)[2];
  const QFieldLineClass& qfldCellGroup3 = qfldsCell.getCellGroup<Line>(0)[3];

  const FieldType& qfldITrace0 = qfldsITrace[0];
  const FieldType& qfldITrace1 = qfldsITrace[1];
  const FieldType& qfldITrace2 = qfldsITrace[2];
  const FieldType& qfldITrace3 = qfldsITrace[3];
  const QFieldNodeClass& qfldITraceGroup0 = qfldsITrace.getInteriorTraceGroup<Node>(0)[0];
  const QFieldNodeClass& qfldITraceGroup1 = qfldsITrace.getInteriorTraceGroup<Node>(0)[1];
  const QFieldNodeClass& qfldITraceGroup2 = qfldsITrace.getInteriorTraceGroup<Node>(0)[2];
  const QFieldNodeClass& qfldITraceGroup3 = qfldsITrace.getInteriorTraceGroup<Node>(0)[3];

  const FieldType& qfldBTrace0 = qfldsBTrace[0];
  const FieldType& qfldBTrace1 = qfldsBTrace[1];
  const FieldType& qfldBTrace2 = qfldsBTrace[2];
  const FieldType& qfldBTrace3 = qfldsBTrace[3];
  const QFieldNodeClass& qfldBTraceGroup0 = qfldsBTrace.getBoundaryTraceGroup<Node>(0)[0];
  const QFieldNodeClass& qfldBTraceGroup1 = qfldsBTrace.getBoundaryTraceGroup<Node>(0)[1];
  const QFieldNodeClass& qfldBTraceGroup2 = qfldsBTrace.getBoundaryTraceGroup<Node>(0)[2];
  const QFieldNodeClass& qfldBTraceGroup3 = qfldsBTrace.getBoundaryTraceGroup<Node>(0)[3];

  // Rotate the last field so it's first. i.e. {fld0, fld1, fld2, fld3} -> {fld3, fld0, fld1, fld2}
  qfldsCell.rotate();
  qfldsITrace.rotate();
  qfldsBTrace.rotate();

  BOOST_CHECK_EQUAL( &qfldCell3, &qfldsCell[0] );
  BOOST_CHECK_EQUAL( &qfldCell0, &qfldsCell[1] );
  BOOST_CHECK_EQUAL( &qfldCell1, &qfldsCell[2] );
  BOOST_CHECK_EQUAL( &qfldCell2, &qfldsCell[3] );
  BOOST_CHECK_EQUAL( &qfldCellGroup3, &qfldsCell.getCellGroup<Line>(0)[0] );
  BOOST_CHECK_EQUAL( &qfldCellGroup0, &qfldsCell.getCellGroup<Line>(0)[1] );
  BOOST_CHECK_EQUAL( &qfldCellGroup1, &qfldsCell.getCellGroup<Line>(0)[2] );
  BOOST_CHECK_EQUAL( &qfldCellGroup2, &qfldsCell.getCellGroup<Line>(0)[3] );

  BOOST_CHECK_EQUAL( &qfldITrace3, &qfldsITrace[0] );
  BOOST_CHECK_EQUAL( &qfldITrace0, &qfldsITrace[1] );
  BOOST_CHECK_EQUAL( &qfldITrace1, &qfldsITrace[2] );
  BOOST_CHECK_EQUAL( &qfldITrace2, &qfldsITrace[3] );
  BOOST_CHECK_EQUAL( &qfldITraceGroup3, &qfldsITrace.getInteriorTraceGroup<Node>(0)[0] );
  BOOST_CHECK_EQUAL( &qfldITraceGroup0, &qfldsITrace.getInteriorTraceGroup<Node>(0)[1] );
  BOOST_CHECK_EQUAL( &qfldITraceGroup1, &qfldsITrace.getInteriorTraceGroup<Node>(0)[2] );
  BOOST_CHECK_EQUAL( &qfldITraceGroup2, &qfldsITrace.getInteriorTraceGroup<Node>(0)[3] );

  BOOST_CHECK_EQUAL( &qfldBTrace3, &qfldsBTrace[0] );
  BOOST_CHECK_EQUAL( &qfldBTrace0, &qfldsBTrace[1] );
  BOOST_CHECK_EQUAL( &qfldBTrace1, &qfldsBTrace[2] );
  BOOST_CHECK_EQUAL( &qfldBTrace2, &qfldsBTrace[3] );
  BOOST_CHECK_EQUAL( &qfldBTraceGroup3, &qfldsBTrace.getBoundaryTraceGroup<Node>(0)[0] );
  BOOST_CHECK_EQUAL( &qfldBTraceGroup0, &qfldsBTrace.getBoundaryTraceGroup<Node>(0)[1] );
  BOOST_CHECK_EQUAL( &qfldBTraceGroup1, &qfldsBTrace.getBoundaryTraceGroup<Node>(0)[2] );
  BOOST_CHECK_EQUAL( &qfldBTraceGroup2, &qfldsBTrace.getBoundaryTraceGroup<Node>(0)[3] );

  qfldsCell.rotate();
  qfldsITrace.rotate();
  qfldsBTrace.rotate();

  BOOST_CHECK_EQUAL( &qfldCell2, &qfldsCell[0] );
  BOOST_CHECK_EQUAL( &qfldCell3, &qfldsCell[1] );
  BOOST_CHECK_EQUAL( &qfldCell0, &qfldsCell[2] );
  BOOST_CHECK_EQUAL( &qfldCell1, &qfldsCell[3] );
  BOOST_CHECK_EQUAL( &qfldCellGroup2, &qfldsCell.getCellGroup<Line>(0)[0] );
  BOOST_CHECK_EQUAL( &qfldCellGroup3, &qfldsCell.getCellGroup<Line>(0)[1] );
  BOOST_CHECK_EQUAL( &qfldCellGroup0, &qfldsCell.getCellGroup<Line>(0)[2] );
  BOOST_CHECK_EQUAL( &qfldCellGroup1, &qfldsCell.getCellGroup<Line>(0)[3] );

  BOOST_CHECK_EQUAL( &qfldITrace2, &qfldsITrace[0] );
  BOOST_CHECK_EQUAL( &qfldITrace3, &qfldsITrace[1] );
  BOOST_CHECK_EQUAL( &qfldITrace0, &qfldsITrace[2] );
  BOOST_CHECK_EQUAL( &qfldITrace1, &qfldsITrace[3] );
  BOOST_CHECK_EQUAL( &qfldITraceGroup2, &qfldsITrace.getInteriorTraceGroup<Node>(0)[0] );
  BOOST_CHECK_EQUAL( &qfldITraceGroup3, &qfldsITrace.getInteriorTraceGroup<Node>(0)[1] );
  BOOST_CHECK_EQUAL( &qfldITraceGroup0, &qfldsITrace.getInteriorTraceGroup<Node>(0)[2] );
  BOOST_CHECK_EQUAL( &qfldITraceGroup1, &qfldsITrace.getInteriorTraceGroup<Node>(0)[3] );

  BOOST_CHECK_EQUAL( &qfldBTrace2, &qfldsBTrace[0] );
  BOOST_CHECK_EQUAL( &qfldBTrace3, &qfldsBTrace[1] );
  BOOST_CHECK_EQUAL( &qfldBTrace0, &qfldsBTrace[2] );
  BOOST_CHECK_EQUAL( &qfldBTrace1, &qfldsBTrace[3] );
  BOOST_CHECK_EQUAL( &qfldBTraceGroup2, &qfldsBTrace.getBoundaryTraceGroup<Node>(0)[0] );
  BOOST_CHECK_EQUAL( &qfldBTraceGroup3, &qfldsBTrace.getBoundaryTraceGroup<Node>(0)[1] );
  BOOST_CHECK_EQUAL( &qfldBTraceGroup0, &qfldsBTrace.getBoundaryTraceGroup<Node>(0)[2] );
  BOOST_CHECK_EQUAL( &qfldBTraceGroup1, &qfldsBTrace.getBoundaryTraceGroup<Node>(0)[3] );

  qfldsCell.rotate();
  qfldsITrace.rotate();
  qfldsBTrace.rotate();

  BOOST_CHECK_EQUAL( &qfldCell1, &qfldsCell[0] );
  BOOST_CHECK_EQUAL( &qfldCell2, &qfldsCell[1] );
  BOOST_CHECK_EQUAL( &qfldCell3, &qfldsCell[2] );
  BOOST_CHECK_EQUAL( &qfldCell0, &qfldsCell[3] );
  BOOST_CHECK_EQUAL( &qfldCellGroup1, &qfldsCell.getCellGroup<Line>(0)[0] );
  BOOST_CHECK_EQUAL( &qfldCellGroup2, &qfldsCell.getCellGroup<Line>(0)[1] );
  BOOST_CHECK_EQUAL( &qfldCellGroup3, &qfldsCell.getCellGroup<Line>(0)[2] );
  BOOST_CHECK_EQUAL( &qfldCellGroup0, &qfldsCell.getCellGroup<Line>(0)[3] );

  BOOST_CHECK_EQUAL( &qfldITrace1, &qfldsITrace[0] );
  BOOST_CHECK_EQUAL( &qfldITrace2, &qfldsITrace[1] );
  BOOST_CHECK_EQUAL( &qfldITrace3, &qfldsITrace[2] );
  BOOST_CHECK_EQUAL( &qfldITrace0, &qfldsITrace[3] );
  BOOST_CHECK_EQUAL( &qfldITraceGroup1, &qfldsITrace.getInteriorTraceGroup<Node>(0)[0] );
  BOOST_CHECK_EQUAL( &qfldITraceGroup2, &qfldsITrace.getInteriorTraceGroup<Node>(0)[1] );
  BOOST_CHECK_EQUAL( &qfldITraceGroup3, &qfldsITrace.getInteriorTraceGroup<Node>(0)[2] );
  BOOST_CHECK_EQUAL( &qfldITraceGroup0, &qfldsITrace.getInteriorTraceGroup<Node>(0)[3] );

  BOOST_CHECK_EQUAL( &qfldBTrace1, &qfldsBTrace[0] );
  BOOST_CHECK_EQUAL( &qfldBTrace2, &qfldsBTrace[1] );
  BOOST_CHECK_EQUAL( &qfldBTrace3, &qfldsBTrace[2] );
  BOOST_CHECK_EQUAL( &qfldBTrace0, &qfldsBTrace[3] );
  BOOST_CHECK_EQUAL( &qfldBTraceGroup1, &qfldsBTrace.getBoundaryTraceGroup<Node>(0)[0] );
  BOOST_CHECK_EQUAL( &qfldBTraceGroup2, &qfldsBTrace.getBoundaryTraceGroup<Node>(0)[1] );
  BOOST_CHECK_EQUAL( &qfldBTraceGroup3, &qfldsBTrace.getBoundaryTraceGroup<Node>(0)[2] );
  BOOST_CHECK_EQUAL( &qfldBTraceGroup0, &qfldsBTrace.getBoundaryTraceGroup<Node>(0)[3] );

  qfldsCell.rotate();
  qfldsITrace.rotate();
  qfldsBTrace.rotate();

  BOOST_CHECK_EQUAL( &qfldCell0, &qfldsCell[0] );
  BOOST_CHECK_EQUAL( &qfldCell1, &qfldsCell[1] );
  BOOST_CHECK_EQUAL( &qfldCell2, &qfldsCell[2] );
  BOOST_CHECK_EQUAL( &qfldCell3, &qfldsCell[3] );
  BOOST_CHECK_EQUAL( &qfldCellGroup0, &qfldsCell.getCellGroup<Line>(0)[0] );
  BOOST_CHECK_EQUAL( &qfldCellGroup1, &qfldsCell.getCellGroup<Line>(0)[1] );
  BOOST_CHECK_EQUAL( &qfldCellGroup2, &qfldsCell.getCellGroup<Line>(0)[2] );
  BOOST_CHECK_EQUAL( &qfldCellGroup3, &qfldsCell.getCellGroup<Line>(0)[3] );

  BOOST_CHECK_EQUAL( &qfldITrace0, &qfldsITrace[0] );
  BOOST_CHECK_EQUAL( &qfldITrace1, &qfldsITrace[1] );
  BOOST_CHECK_EQUAL( &qfldITrace2, &qfldsITrace[2] );
  BOOST_CHECK_EQUAL( &qfldITrace3, &qfldsITrace[3] );
  BOOST_CHECK_EQUAL( &qfldITraceGroup0, &qfldsITrace.getInteriorTraceGroup<Node>(0)[0] );
  BOOST_CHECK_EQUAL( &qfldITraceGroup1, &qfldsITrace.getInteriorTraceGroup<Node>(0)[1] );
  BOOST_CHECK_EQUAL( &qfldITraceGroup2, &qfldsITrace.getInteriorTraceGroup<Node>(0)[2] );
  BOOST_CHECK_EQUAL( &qfldITraceGroup3, &qfldsITrace.getInteriorTraceGroup<Node>(0)[3] );

  BOOST_CHECK_EQUAL( &qfldBTrace0, &qfldsBTrace[0] );
  BOOST_CHECK_EQUAL( &qfldBTrace1, &qfldsBTrace[1] );
  BOOST_CHECK_EQUAL( &qfldBTrace2, &qfldsBTrace[2] );
  BOOST_CHECK_EQUAL( &qfldBTrace3, &qfldsBTrace[3] );
  BOOST_CHECK_EQUAL( &qfldBTraceGroup0, &qfldsBTrace.getBoundaryTraceGroup<Node>(0)[0] );
  BOOST_CHECK_EQUAL( &qfldBTraceGroup1, &qfldsBTrace.getBoundaryTraceGroup<Node>(0)[1] );
  BOOST_CHECK_EQUAL( &qfldBTraceGroup2, &qfldsBTrace.getBoundaryTraceGroup<Node>(0)[2] );
  BOOST_CHECK_EQUAL( &qfldBTraceGroup3, &qfldsBTrace.getBoundaryTraceGroup<Node>(0)[3] );
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD1, TopoD1, ArrayQ > QField1D_CG_Cell;
//  typedef Field_CG_InteriorTrace< PhysD1, TopoD1, ArrayQ > QField1D_CG_InteriorTrace;
  typedef Field_CG_BoundaryTrace< PhysD1, TopoD1, ArrayQ > QField1D_CG_BoundaryTrace;

  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Field/Field1D_CG_pattern.txt", true );

  XField1D xfld(2);

  QField1D_CG_Cell qfld1(xfld, 2);
  qfld1.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

//  QField1D_CG_InteriorTrace qfld2(xfld, 2);
//  qfld2.dump( 2, output );
//  BOOST_CHECK( output.match_pattern() );

  QField1D_CG_BoundaryTrace qfld4(xfld, 2);
  qfld4.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
