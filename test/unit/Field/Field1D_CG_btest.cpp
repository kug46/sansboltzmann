// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Field1D_CG_btest
// testing of Field_CG_1D* classes
//

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;


#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField1D_2Line_X1_1Group.h"
#include "unit/UnitGrids/XField1D_4Line_X1_2Group.h"
#include "Meshing/XField1D/XField1D.h"

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/TraceToCellRefCoord.h"
#include "Field/FieldLine_CG_Cell.h"
#include "Field/FieldLine_CG_BoundaryTrace.h"

using namespace std;
using namespace SANS;

namespace SANS
{
//Explicitly instantiate classes to get proper coverage information
template class Field_CG_Cell< PhysD1, TopoD1, Real >;
template class Field_CG_BoundaryTrace< PhysD1, TopoD1, Real >;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( Field1D_CG_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_Cell_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD1, TopoD1, ArrayQ > QField1D_CG_Cell;
  typedef QField1D_CG_Cell::FieldCellGroupType<Line> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  int nodeMap[2];

  XField1D_2Line_X1_1Group xfld1;

  int order = 1;

  QField1D_CG_Cell qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 3, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfld1.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 2, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldLineClass& qfldGroup1 = qfld1.getCellGroup<Line>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup1.nElem() );

  qfldGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 0, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1, nodeMap[1] );

  qfldGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 1, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2, nodeMap[1] );

  XField1D xfld2(3);

  QField1D_CG_Cell qfld2(xfld2, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 4, qfld2.nDOF() );
  BOOST_CHECK_EQUAL( 3, qfld2.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 2, qfld2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld2.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld2, &qfld2.getXField() );

  QField1D_CG_Cell qfld3(qfld2, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld2.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld2.nElem()               ,  qfld3.nElem() );
  BOOST_CHECK_EQUAL(  qfld2.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nCellGroups()         ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld2.getXField()           , &qfld3.getXField() );

  const QFieldLineClass& qfldGroup2 = qfld2.getCellGroup<Line>(0);

  BOOST_CHECK_EQUAL( 3, qfldGroup2.nElem() );

  qfldGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 0, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1, nodeMap[1] );

  qfldGroup2.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 1, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2, nodeMap[1] );

  qfldGroup2.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 2, nodeMap[0] );
  BOOST_CHECK_EQUAL( 3, nodeMap[1] );


  // Test the constant assignment operator
  ElementQFieldLineClass qfldElem(qfldGroup1.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfldGroup1.nElem(); elem++)
  {
    qfldGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfldGroup1.nElem(); elem++)
  {
    qfldGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_Cell_P1_2Groups )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD1, TopoD1, ArrayQ > QField1D_CG_Cell;
  typedef QField1D_CG_Cell::FieldCellGroupType<Line> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  int nodeMap[2];

  XField1D_4Line_X1_2Group xfld1;

  int order = 1;
  std::vector<std::vector<int>> Groups = {{0},{1}};
  QField1D_CG_Cell qfld1( Groups, xfld1, order, BasisFunctionCategory_Hierarchical );

  BOOST_CHECK_EQUAL( 6, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 4, qfld1.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 2, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 2, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldLineClass& qfld1Group0 = qfld1.getCellGroup<Line>(0);
  const QFieldLineClass& qfld1Group1 = qfld1.getCellGroup<Line>(1);

  BOOST_CHECK_EQUAL( 2, qfld1Group0.nElem() );
  BOOST_CHECK_EQUAL( 2, qfld1Group1.nElem() );

  qfld1Group0.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 0, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1, nodeMap[1] );

  qfld1Group0.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 1, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2, nodeMap[1] );

  qfld1Group1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 3, nodeMap[0] );
  BOOST_CHECK_EQUAL( 4, nodeMap[1] );

  qfld1Group1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 4, nodeMap[0] );
  BOOST_CHECK_EQUAL( 5, nodeMap[1] );


  // Test the constant assignment operator
  ElementQFieldLineClass qfldElem0(qfld1Group0.basis());
  ElementQFieldLineClass qfldElem1(qfld1Group1.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfld1Group0.nElem(); elem++)
  {
    qfld1Group0.getElement(qfldElem0, elem);

    qfldElem0.eval(0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfld1Group0.nElem(); elem++)
  {
    qfld1Group0.getElement(qfldElem0, elem);

    qfldElem0.eval(0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}

//----------------------------------------------------------------------------//
// NOTE: DOF ordering for P2 is interior edges, then nodes
BOOST_AUTO_TEST_CASE( CG_Cell_P2 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD1, TopoD1, ArrayQ > QField1D_CG_Cell;
  typedef QField1D_CG_Cell::FieldCellGroupType<Line> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  int nodeMap[2];
  int edgeMap[1];

  XField1D_2Line_X1_1Group xfld1;

  int order = 2;
  QField1D_CG_Cell qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 3+2, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 2, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldLineClass& qfldLineGroup1 = qfld1.getCellGroup<Line>(0);

  BOOST_CHECK_EQUAL( 2, qfldLineGroup1.nElem() );

  qfldLineGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 0, edgeMap[0] );

  qfldLineGroup1.associativity(1).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 1, edgeMap[0] );

  qfldLineGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 2, nodeMap[0] );
  BOOST_CHECK_EQUAL( 3, nodeMap[1] );

  qfldLineGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 3, nodeMap[0] );
  BOOST_CHECK_EQUAL( 4, nodeMap[1] );


  XField1D xfld2(3);

  QField1D_CG_Cell qfld2(xfld2, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 4+3, qfld2.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 2, qfld2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld2.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld2, &qfld2.getXField() );

  QField1D_CG_Cell qfld3(qfld2, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld2.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld2.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nCellGroups()         ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld2.getXField()           , &qfld3.getXField() );

  const QFieldLineClass& qfldLineGroup2 = qfld2.getCellGroup<Line>(0);

  BOOST_CHECK_EQUAL( 3, qfldLineGroup2.nElem() );

  qfldLineGroup2.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 0, edgeMap[0] );

  qfldLineGroup2.associativity(1).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 1, edgeMap[0] );

  qfldLineGroup2.associativity(2).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 2, edgeMap[0] );

  qfldLineGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 3, nodeMap[0] );
  BOOST_CHECK_EQUAL( 4, nodeMap[1] );

  qfldLineGroup2.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 4, nodeMap[0] );
  BOOST_CHECK_EQUAL( 5, nodeMap[1] );

  qfldLineGroup2.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 5, nodeMap[0] );
  BOOST_CHECK_EQUAL( 6, nodeMap[1] );


  // Test the constant assignment operator
  ElementQFieldLineClass qfldElem(qfldLineGroup1.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfldLineGroup1.nElem(); elem++)
  {
    qfldLineGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfldLineGroup1.nElem(); elem++)
  {
    qfldLineGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}

//----------------------------------------------------------------------------//
// NOTE: DOF ordering for P2 is interior edges, then nodes
BOOST_AUTO_TEST_CASE( CG_Cell_P2_2Groups )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD1, TopoD1, ArrayQ > QField1D_CG_Cell;
  typedef QField1D_CG_Cell::FieldCellGroupType<Line> QFieldLineClass;

  int nodeMap[2];
  int edgeMap[1];

  XField1D_4Line_X1_2Group xfld1;

  int order = 2;
  std::vector<std::vector<int>> Groups = {{0},{1}};
  QField1D_CG_Cell qfld1( Groups, xfld1, order, BasisFunctionCategory_Hierarchical );

  BOOST_CHECK_EQUAL( 6+4, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 4, qfld1.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 2, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 2, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldLineClass& qfld1Group0 = qfld1.getCellGroup<Line>(0);
  const QFieldLineClass& qfld1Group1 = qfld1.getCellGroup<Line>(1);

  BOOST_CHECK_EQUAL( 2, qfld1Group0.nElem() );
  BOOST_CHECK_EQUAL( 2, qfld1Group1.nElem() );

  qfld1Group0.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 0, edgeMap[0] );

  qfld1Group0.associativity(1).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 1, edgeMap[0] );

  qfld1Group0.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 4, nodeMap[0] );
  BOOST_CHECK_EQUAL( 5, nodeMap[1] );

  qfld1Group0.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 5, nodeMap[0] );
  BOOST_CHECK_EQUAL( 6, nodeMap[1] );


  qfld1Group1.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 2, edgeMap[0] );

  qfld1Group1.associativity(1).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 3, edgeMap[0] );

  qfld1Group1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 7, nodeMap[0] );
  BOOST_CHECK_EQUAL( 8, nodeMap[1] );

  qfld1Group1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 8, nodeMap[0] );
  BOOST_CHECK_EQUAL( 9, nodeMap[1] );


  // Swapping the macro grouping
  // Doesn't change anything, as macro group numbering is independent
  Groups[0][0] = 1; Groups[1][0] = 0;
  QField1D_CG_Cell qfld2( Groups, xfld1, order, BasisFunctionCategory_Hierarchical );

  BOOST_CHECK_EQUAL( 6+4, qfld2.nDOF() );
  BOOST_CHECK_EQUAL( 4, qfld2.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 2, qfld2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 2, qfld2.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld2.getXField() );

  const QFieldLineClass& qfld2Group0 = qfld2.getCellGroup<Line>(0);
  const QFieldLineClass& qfld2Group1 = qfld2.getCellGroup<Line>(1);

  BOOST_CHECK_EQUAL( 2, qfld2Group0.nElem() );
  BOOST_CHECK_EQUAL( 2, qfld2Group1.nElem() );

  qfld2Group0.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 0, edgeMap[0] );

  qfld2Group0.associativity(1).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 1, edgeMap[0] );

  qfld2Group0.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 4, nodeMap[0] );
  BOOST_CHECK_EQUAL( 5, nodeMap[1] );

  qfld2Group0.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 5, nodeMap[0] );
  BOOST_CHECK_EQUAL( 6, nodeMap[1] );

  qfld2Group1.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 2, edgeMap[0] );

  qfld2Group1.associativity(1).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 3, edgeMap[0] );

  qfld2Group1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 7, nodeMap[0] );
  BOOST_CHECK_EQUAL( 8, nodeMap[1] );

  qfld2Group1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 8, nodeMap[0] );
  BOOST_CHECK_EQUAL( 9, nodeMap[1] );

}


//----------------------------------------------------------------------------//
// NOTE: DOF ordering for P3 is cells, interior edges, then boundary edges, then nodes
BOOST_AUTO_TEST_CASE( CG_Line_P3 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD1, TopoD1, ArrayQ > QField1D_CG_Cell;
  typedef QField1D_CG_Cell::FieldCellGroupType<Line> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  int nodeMap[2];
  int edgeMap[2];

  XField1D_2Line_X1_1Group xfld1;

  int order = 3;
  QField1D_CG_Cell qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 3 + 2*2, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 2, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldLineClass& qfldLineGroup1 = qfld1.getCellGroup<Line>(0);
  //cout << "btest: qfldLineGroup1 =" << endl; qfldLineGroup1.dump(2);

  BOOST_CHECK_EQUAL( 2, qfldLineGroup1.nElem() );

  qfldLineGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 0, edgeMap[0] );
  BOOST_CHECK_EQUAL( 1, edgeMap[1] );

  qfldLineGroup1.associativity(1).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 2, edgeMap[0] );
  BOOST_CHECK_EQUAL( 3, edgeMap[1] );

  qfldLineGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 4, nodeMap[0] );
  BOOST_CHECK_EQUAL( 5, nodeMap[1] );

  qfldLineGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 5, nodeMap[0] );
  BOOST_CHECK_EQUAL( 6, nodeMap[1] );



  XField1D xfld2(3);

  QField1D_CG_Cell qfld2(xfld2, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 4 + 3*2, qfld2.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 2, qfld2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld2.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld2, &qfld2.getXField() );

  QField1D_CG_Cell qfld3(qfld2, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld2.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld2.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nCellGroups()         ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld2.getXField()           , &qfld3.getXField() );

  const QFieldLineClass& qfldLineGroup2 = qfld2.getCellGroup<Line>(0);
  //cout << "btest: qfldLineGroup2 =" << endl; qfldLineGroup2.dump(2);

  BOOST_CHECK_EQUAL( 3, qfldLineGroup2.nElem() );

  qfldLineGroup2.associativity(0).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 0, edgeMap[0] );
  BOOST_CHECK_EQUAL( 1, edgeMap[1] );

  qfldLineGroup2.associativity(1).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 2, edgeMap[0] );
  BOOST_CHECK_EQUAL( 3, edgeMap[1] );

  qfldLineGroup2.associativity(2).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( 4, edgeMap[0] );
  BOOST_CHECK_EQUAL( 5, edgeMap[1] );

  qfldLineGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 6, nodeMap[0] );
  BOOST_CHECK_EQUAL( 7, nodeMap[1] );

  qfldLineGroup2.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 7, nodeMap[0] );
  BOOST_CHECK_EQUAL( 8, nodeMap[1] );

  qfldLineGroup2.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 8, nodeMap[0] );
  BOOST_CHECK_EQUAL( 9, nodeMap[1] );


  // Test the constant assignment operator
  ElementQFieldLineClass qfldElem(qfldLineGroup1.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfldLineGroup1.nElem(); elem++)
  {
    qfldLineGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfldLineGroup1.nElem(); elem++)
  {
    qfldLineGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}

//----------------------------------------------------------------------------//
// check that solution trace is identical for adjacent elements
BOOST_AUTO_TEST_CASE( CG_Line_CheckTrace_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;

  typedef XField1D::FieldTraceGroupType<Node> XFieldNodeClass;

  typedef Field_CG_Cell< PhysD1, TopoD1, ArrayQ > QField1D_CG_Cell;
  typedef QField1D_CG_Cell::FieldCellGroupType<Line> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  XField1D xfld(3);

  const XFieldNodeClass& xfldNode = xfld.getInteriorTraceGroup<Node>(0);

  int order = 1;
  QField1D_CG_Cell qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  const QFieldLineClass& qfldLine = qfld.getCellGroup<Line>(0);

  // initialize solution DOFs
  for (int k = 0; k < qfld.nDOF(); k++)
    qfld.DOF(k) = pow(-1, k) / sqrt(k+1);

  // element field variables
  ElementQFieldLineClass qfldElemL( qfldLine.basis() );
  ElementQFieldLineClass qfldElemR( qfldLine.basis() );

  int nnode = xfldNode.nElem();
  for (int node = 0; node < nnode; node++)
  {
    int elemL = xfldNode.getElementLeft( node );
    int elemR = xfldNode.getElementRight( node );
    CanonicalTraceToCell canonicalNodeL = xfldNode.getCanonicalTraceLeft( node );
    CanonicalTraceToCell canonicalNodeR = xfldNode.getCanonicalTraceRight( node );

    // copy global grid/solution DOFs to element
    qfldLine.getElement( qfldElemL, elemL );
    qfldLine.getElement( qfldElemR, elemR );

    Real sRefL, sRefR;
    ArrayQ qL, qR;

    // left/right reference-element coords
    TraceToCellRefCoord<Node, TopoD1, Line>::eval( canonicalNodeL, sRefL);
    TraceToCellRefCoord<Node, TopoD1, Line>::eval( canonicalNodeR, sRefR);

    // solution trace from L/R elements
    qfldElemL.eval( sRefL, qL );
    qfldElemR.eval( sRefR, qR );

    SANS_CHECK_CLOSE( qL(0), qR(0), small_tol, close_tol );
  }
}

//----------------------------------------------------------------------------//
// check that solution trace is identical for adjacent elements
BOOST_AUTO_TEST_CASE( CG_Line_CheckTrace_P2 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;

  typedef XField1D::FieldTraceGroupType<Node> XFieldNodeClass;

  typedef Field_CG_Cell< PhysD1, TopoD1, ArrayQ > QField1D_CG_Cell;
  typedef QField1D_CG_Cell::FieldCellGroupType<Line> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  XField1D xfld(3);

  const XFieldNodeClass& xfldNode = xfld.getInteriorTraceGroup<Node>(0);

  int order = 2;
  QField1D_CG_Cell qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  const QFieldLineClass& qfldLine = qfld.getCellGroup<Line>(0);

  // initialize solution DOFs
  for (int k = 0; k < qfld.nDOF(); k++)
    qfld.DOF(k) = pow(-1, k) / sqrt(k+1);

  // element field variables
  ElementQFieldLineClass qfldElemL( qfldLine.basis() );
  ElementQFieldLineClass qfldElemR( qfldLine.basis() );

  int nnode = xfldNode.nElem();
  for (int node = 0; node < nnode; node++)
  {
    int elemL = xfldNode.getElementLeft( node );
    int elemR = xfldNode.getElementRight( node );
    CanonicalTraceToCell canonicalNodeL = xfldNode.getCanonicalTraceLeft( node );
    CanonicalTraceToCell canonicalNodeR = xfldNode.getCanonicalTraceRight( node );

    // copy global grid/solution DOFs to element
    qfldLine.getElement( qfldElemL, elemL );
    qfldLine.getElement( qfldElemR, elemR );

    Real sRefL, sRefR;
    ArrayQ qL, qR;

    // left/right reference-element coords
    TraceToCellRefCoord<Node, TopoD1, Line>::eval( canonicalNodeL, sRefL);
    TraceToCellRefCoord<Node, TopoD1, Line>::eval( canonicalNodeR, sRefR);

    // solution trace from L/R elements
    qfldElemL.eval( sRefL, qL );
    qfldElemR.eval( sRefR, qR );

    SANS_CHECK_CLOSE( qL(0), qR(0), small_tol, close_tol );
  }
}


//----------------------------------------------------------------------------//
// check that solution trace is identical for adjacent elements
BOOST_AUTO_TEST_CASE( CG_Line_CheckTrace_P3 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;

  typedef XField1D::FieldTraceGroupType<Node> XFieldNodeClass;

  typedef Field_CG_Cell< PhysD1, TopoD1, ArrayQ > QField1D_CG_Cell;
  typedef QField1D_CG_Cell::FieldCellGroupType<Line> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  XField1D xfld(3);

  const XFieldNodeClass& xfldNode = xfld.getInteriorTraceGroup<Node>(0);

  int order = 3;
  QField1D_CG_Cell qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  const QFieldLineClass& qfldLine = qfld.getCellGroup<Line>(0);

  // initialize solution DOFs
  for (int k = 0; k < qfld.nDOF(); k++)
    qfld.DOF(k) = pow(-1, k) / sqrt(k+1);

  // element field variables
  ElementQFieldLineClass qfldElemL( qfldLine.basis() );
  ElementQFieldLineClass qfldElemR( qfldLine.basis() );

  int nnode = xfldNode.nElem();
  for (int node = 0; node < nnode; node++)
  {
    int elemL = xfldNode.getElementLeft( node );
    int elemR = xfldNode.getElementRight( node );
    CanonicalTraceToCell canonicalNodeL = xfldNode.getCanonicalTraceLeft( node );
    CanonicalTraceToCell canonicalNodeR = xfldNode.getCanonicalTraceRight( node );

    // copy global grid/solution DOFs to element
    qfldLine.getElement( qfldElemL, elemL );
    qfldLine.getElement( qfldElemR, elemR );

    Real sRefL, sRefR;
    ArrayQ qL, qR;

    // left/right reference-element coords
    TraceToCellRefCoord<Node, TopoD1, Line>::eval( canonicalNodeL, sRefL);
    TraceToCellRefCoord<Node, TopoD1, Line>::eval( canonicalNodeR, sRefR);

    // solution trace from L/R elements
    qfldElemL.eval( sRefL, qL );
    qfldElemR.eval( sRefR, qR );

    SANS_CHECK_CLOSE( qL(0), qR(0), small_tol, close_tol );
  }
}

//----------------------------------------------------------------------------//
// check that solution trace is identical for adjacent elements
BOOST_AUTO_TEST_CASE( CG_Line_CheckTrace_P4 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;

  typedef XField1D::FieldTraceGroupType<Node> XFieldNodeClass;

  typedef Field_CG_Cell< PhysD1, TopoD1, ArrayQ > QField1D_CG_Cell;
  typedef QField1D_CG_Cell::FieldCellGroupType<Line> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  XField1D xfld(3);

  const XFieldNodeClass& xfldNode = xfld.getInteriorTraceGroup<Node>(0);

  int order = 4;
  QField1D_CG_Cell qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  const QFieldLineClass& qfldLine = qfld.getCellGroup<Line>(0);

  // initialize solution DOFs
  for (int k = 0; k < qfld.nDOF(); k++)
    qfld.DOF(k) = pow(-1, k) / sqrt(k+1);

  // element field variables
  ElementQFieldLineClass qfldElemL( qfldLine.basis() );
  ElementQFieldLineClass qfldElemR( qfldLine.basis() );

  int nnode = xfldNode.nElem();
  for (int node = 0; node < nnode; node++)
  {
    int elemL = xfldNode.getElementLeft( node );
    int elemR = xfldNode.getElementRight( node );
    CanonicalTraceToCell canonicalNodeL = xfldNode.getCanonicalTraceLeft( node );
    CanonicalTraceToCell canonicalNodeR = xfldNode.getCanonicalTraceRight( node );

    // copy global grid/solution DOFs to element
    qfldLine.getElement( qfldElemL, elemL );
    qfldLine.getElement( qfldElemR, elemR );

    Real sRefL, sRefR;
    ArrayQ qL, qR;

    // left/right reference-element coords
    TraceToCellRefCoord<Node, TopoD1, Line>::eval( canonicalNodeL, sRefL);
    TraceToCellRefCoord<Node, TopoD1, Line>::eval( canonicalNodeR, sRefR);

    // solution trace from L/R elements
    qfldElemL.eval( sRefL, qL );
    qfldElemR.eval( sRefR, qR );

    SANS_CHECK_CLOSE( qL(0), qR(0), small_tol, close_tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_Line_ProjectPtoPp1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD1, TopoD1, ArrayQ > QField1D_CG_Cell;
  typedef QField1D_CG_Cell::FieldCellGroupType<Line> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  ArrayQ q0, q1;

  XField1D xfld1(3);

  for (int order = 1; order < BasisFunctionLine_HierarchicalPMax; order++)
  {
    for (int orderinc = 1; orderinc <= BasisFunctionLine_HierarchicalPMax-order; orderinc++)
    {
      QField1D_CG_Cell qfldP  (xfld1, order, BasisFunctionCategory_Hierarchical);
      QField1D_CG_Cell qfldPp1(xfld1, order+orderinc, BasisFunctionCategory_Hierarchical);

      QFieldLineClass& qfldLineP   = qfldP.getCellGroup<Line>(0);
      QFieldLineClass& qfldLinePp1 = qfldPp1.getCellGroup<Line>(0);

      ElementQFieldLineClass qfldElemP(qfldLineP.basis());
      ElementQFieldLineClass qfldElemPp1(qfldLinePp1.basis());

      //Give some non-zero initial condition
      for (int n = 0; n < qfldP.nDOF(); n++)
        qfldP.DOF(n) = n+1;

      for (int n = 0; n < qfldPp1.nDOF(); n++)
        qfldPp1.DOF(n) = 0;

      //Use Line function projectTo
      qfldLineP.projectTo(qfldLinePp1);

      for (int elem = 0; elem < qfldLineP.nElem(); elem++)
      {
        qfldLineP.getElement(qfldElemP, elem);
        qfldLinePp1.getElement(qfldElemPp1, elem);

        qfldElemP.eval(0.25, q0);
        qfldElemPp1.eval(0.25, q1);
        BOOST_CHECK_GT(q0[0], 1);
        BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
      }

      //Wipe out DOF's for P1
      for (int n = 0; n < qfldPp1.nDOF(); n++)
        qfldPp1.DOF(n) = -1;

      //Use base function projectTo
      qfldP.projectTo(qfldPp1);

      for (int elem = 0; elem < qfldLineP.nElem(); elem++)
      {
        qfldLineP.getElement(qfldElemP, elem);
        qfldLinePp1.getElement(qfldElemPp1, elem);

        qfldElemP.eval(0.25, q0);
        qfldElemPp1.eval(0.25, q1);
        BOOST_CHECK_GT(q0[0], 1);
        BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
      }
    }
  }
}

#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_InteriorEdge_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField1D_CG_Cell;
  typedef QField1D_CG_Cell::FieldTraceGroupType<Triangle> QFieldLineClass;

  int nodeMap[2];

  XField2D_4Triangle_X1_1Group xfld1;

  int order = 1;
  Field_CG_InteriorTrace< PhysD2, TopoD2, ArrayQ > qfld1(xfld1, order);

  BOOST_CHECK_EQUAL( 3, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldLineClass& qfldGroup1 = qfld1.getInteriorTraceGroup<Line>(0);

  qfldGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 1, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2, nodeMap[1] );

  qfldGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 2, nodeMap[0] );
  BOOST_CHECK_EQUAL( 0, nodeMap[1] );

  qfldGroup1.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 0, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1, nodeMap[1] );

  Field< PhysD2, TopoD2, ArrayQ > qfld3(qfld1);

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()      ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld3.getXField() );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_InteriorEdge_P2 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField1D_CG_Cell;
  typedef QField1D_CG_Cell::FieldTraceGroupType<Triangle> QFieldLineClass;

  int nodeMap[2];
  int edgeMap[1];

  XField2D_4Triangle_X1_1Group xfld1;

  int order = 2;
  Field_CG_InteriorTrace<PhysD2, TopoD2, ArrayQ> qfld1(xfld1, order);

  BOOST_CHECK_EQUAL( 6, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldLineClass& qfldGroup1 = qfld1.getInteriorTraceGroup<Line>(0);

  qfldGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 1, nodeMap[0] );
  BOOST_CHECK_EQUAL( 2, nodeMap[1] );

  qfldGroup1.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 2, nodeMap[0] );
  BOOST_CHECK_EQUAL( 0, nodeMap[1] );

  qfldGroup1.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( 0, nodeMap[0] );
  BOOST_CHECK_EQUAL( 1, nodeMap[1] );

  qfldGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 3, edgeMap[0] );

  qfldGroup1.associativity(1).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 4, edgeMap[0] );

  qfldGroup1.associativity(2).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 5, edgeMap[0] );

  Field< PhysD2, TopoD2, ArrayQ > qfld3(qfld1);

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()      ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld3.getXField() );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_InteriorEdge_ProjectPtoPp1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD2, TopoD2, ArrayQ > QField1D_CG_Cell;
  typedef QField1D_CG_Cell::FieldTraceGroupType<Triangle> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  ArrayQ q1, q2;

  XField2D_4Triangle_X1_1Group xfld1;

  for (int order = 1; order < BasisFunctionLine_HierarchicalPMax; order++)
  {
    for (int orderinc = 1; orderinc <= BasisFunctionLine_HierarchicalPMax-order; orderinc++)
    {
      Field_CG_InteriorTrace<PhysD2, TopoD2, ArrayQ> qfldP(xfld1, order);
      Field_CG_InteriorTrace<PhysD2, TopoD2, ArrayQ> qfldPp1(xfld1, order+orderinc);

      for ( int group = 0; group < qfldP.nInteriorTraceGroups(); group++ )
      {
        QFieldLineClass& qfldLineGroupP   = qfldP.getInteriorTraceGroup<Line>(group);
        QFieldLineClass& qfldLineGroupPp1 = qfldPp1.getInteriorTraceGroup<Line>(group);

        ElementQFieldLineClass qfldElemP(qfldLineGroupP.basis());
        ElementQFieldLineClass qfldElemPp1(qfldLineGroupPp1.basis());

        //Give some non-zero initial condition
        for (int n = 0; n < qfldP.nDOF(); n++)
          qfldP.DOF(n) = n+1;

        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = 0;

        //Use line function projectTo
        qfldLineGroupP.projectTo(qfldLineGroupPp1);

        for (int elem = 0; elem < qfldLineGroupP.nElem(); elem++)
        {
          qfldLineGroupP.getElement(qfldElemP, elem);
          qfldLineGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(0.25, q1);
          qfldElemPp1.eval(0.25, q2);
          BOOST_CHECK_GT(q1[0], 1);
          BOOST_CHECK_CLOSE(q1[0], q2[0], 1e-12);
        }

        //Wipe out DOF's for P1
        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = -1;

        //Use base function projectTo
        qfldP.projectTo(qfldPp1);

        for (int elem = 0; elem < qfldLineGroupP.nElem(); elem++)
        {
          qfldLineGroupP.getElement(qfldElemP, elem);
          qfldLineGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(0.75, q1);
          qfldElemPp1.eval(0.75, q2);
          BOOST_CHECK_CLOSE(q1[0], q2[0], 1e-12);
        }
      }
    }
  }
}
#endif

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_BoundaryNode_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD1, TopoD1, ArrayQ > QField1D_CG_Cell;
  typedef QField1D_CG_Cell::FieldTraceGroupType<Node> QFieldNodeClass;
  typedef QFieldNodeClass::ElementType<> ElementQFieldNodeClass;

  int nodeMap[1];

  XField1D_2Line_X1_1Group xfld1;

  int order = 1;
  Field_CG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfld1.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 2, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldNodeClass& qfldGroup0 = qfld1.getBoundaryTraceGroup<Node>(0);
  const QFieldNodeClass& qfldGroup1 = qfld1.getBoundaryTraceGroup<Node>(1);

  qfldGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( 0, nodeMap[0] );

  qfldGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( 1, nodeMap[0] );


  Field< PhysD1, TopoD1, ArrayQ > qfld3(qfld1, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()         ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld3.getXField() );


  // Test the constant assignment operator
  ElementQFieldNodeClass qfldElem(qfldGroup0.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfldGroup0.nElem(); elem++)
  {
    qfldGroup0.getElement(qfldElem, elem);

    qfldElem.eval(q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfldGroup0.nElem(); elem++)
  {
    qfldGroup0.getElement(qfldElem, elem);

    qfldElem.eval(q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_BoundaryNode_P2 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD1, TopoD1, ArrayQ > QField1D_CG_Cell;
  typedef QField1D_CG_Cell::FieldTraceGroupType<Node> QFieldNodeClass;
  typedef QFieldNodeClass::ElementType<> ElementQFieldNodeClass;

  int nodeMap[1];

  XField1D_2Line_X1_1Group xfld1;

  int order = 2;
  Field_CG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 2, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfld1.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 2, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldNodeClass& qfldGroup0 = qfld1.getBoundaryTraceGroup<Node>(0);
  const QFieldNodeClass& qfldGroup1 = qfld1.getBoundaryTraceGroup<Node>(1);

  qfldGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( 0, nodeMap[0] );

  qfldGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( 1, nodeMap[0] );


  Field< PhysD1, TopoD1, ArrayQ > qfld3(qfld1, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()         ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld3.getXField() );


  // Test the constant assignment operator
  ElementQFieldNodeClass qfldElem(qfldGroup0.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfldGroup0.nElem(); elem++)
  {
    qfldGroup0.getElement(qfldElem, elem);

    qfldElem.eval(q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfldGroup0.nElem(); elem++)
  {
    qfldGroup0.getElement(qfldElem, elem);

    qfldElem.eval(q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_BoundaryNode_P1_Empty )
{
  typedef DLA::VectorS<2, Real> ArrayQ;

  XField1D_2Line_X1_1Group xfld1;

  int order = 1;
  Field_CG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> qfld1(xfld1, order, BasisFunctionCategory_Hierarchical, {}); // Empty with no groups

  BOOST_CHECK_EQUAL( 0, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_BoundaryNode_P1_Subset )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD1, TopoD1, ArrayQ > QField1D_CG_Cell;
  typedef QField1D_CG_Cell::FieldTraceGroupType<Node> QFieldNodeClass;

  int nodeMap[1];

  XField1D_2Line_X1_1Group xfld1;

  int order = 1;
  Field_CG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> qfld1(xfld1, order, BasisFunctionCategory_Hierarchical, {1}); // Just the right BC group

  BOOST_CHECK_EQUAL( 1, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld1.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldNodeClass& qfldGroup0 = qfld1.getBoundaryTraceGroup<Node>(0);

  qfldGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( 0, nodeMap[0] );

  Field< PhysD1, TopoD1, ArrayQ > qfld3(qfld1, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()         ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld3.getXField() );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_BoundaryEdge_ProjectPtoPp1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD1, TopoD1, ArrayQ > QField1D_CG_Cell;
  typedef QField1D_CG_Cell::FieldTraceGroupType<Node> QFieldNodeClass;
  typedef QFieldNodeClass::ElementType<> ElementQFieldNodeClass;

  ArrayQ q1, q2;

  XField1D xfld1(3);

  for (int order = 1; order < BasisFunctionLine_HierarchicalPMax; order++)
  {
    for (int orderinc = 1; orderinc <= BasisFunctionLine_HierarchicalPMax-order; orderinc++)
    {
      Field_CG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> qfldP(xfld1, order, BasisFunctionCategory_Hierarchical);
      Field_CG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> qfldPp1(xfld1, order+orderinc, BasisFunctionCategory_Hierarchical);

      for ( int group = 0; group < qfldP.nBoundaryTraceGroups(); group++ )
      {
        QFieldNodeClass& qfldLineGroupP   = qfldP.getBoundaryTraceGroup<Node>(group);
        QFieldNodeClass& qfldLineGroupPp1 = qfldPp1.getBoundaryTraceGroup<Node>(group);

        ElementQFieldNodeClass qfldElemP(qfldLineGroupP.basis());
        ElementQFieldNodeClass qfldElemPp1(qfldLineGroupPp1.basis());

        //Give some non-zero initial condition
        for (int n = 0; n < qfldP.nDOF(); n++)
          qfldP.DOF(n) = n+1;

        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = 0;

        //Use line function projectTo
        qfldLineGroupP.projectTo(qfldLineGroupPp1);

        for (int elem = 0; elem < qfldLineGroupP.nElem(); elem++)
        {
          qfldLineGroupP.getElement(qfldElemP, elem);
          qfldLineGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(q1);
          qfldElemPp1.eval(q2);
          BOOST_CHECK_GT( abs(q1[0]), 1e-12 );
          BOOST_CHECK_CLOSE(q1[0], q2[0], 1e-12);
        }

        //Wipe out DOF's for P1
        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = -1;

        //Use base function projectTo
        qfldP.projectTo(qfldPp1);

        for (int elem = 0; elem < qfldLineGroupP.nElem(); elem++)
        {
          qfldLineGroupP.getElement(qfldElemP, elem);
          qfldLineGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(q1);
          qfldElemPp1.eval(q2);
          BOOST_CHECK_GT( abs(q1[0]), 1e-12 );
          BOOST_CHECK_CLOSE(q1[0], q2[0], 1e-12);
        }
      }
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( CG_Exception )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD1, TopoD1, ArrayQ > QField1D_CG_Cell;
  //typedef Field_CG_InteriorTrace< PhysD1, TopoD1, ArrayQ > QField1D_CG_InteriorTrace;
  //typedef Field_CG_BoundaryTrace< PhysD1, TopoD1, ArrayQ > QField1D_CG_BoundaryTrace;


  XField1D xfld1(3);

  int order = 999; //This should always higher than the maximum available order
  BOOST_CHECK_THROW( QField1D_CG_Cell qfld1(xfld1, order, BasisFunctionCategory_Hierarchical), DeveloperException );
  //BOOST_CHECK_THROW( QField1D_CG_InteriorTrace qfld1(xfld1, order), DeveloperException );
  //BOOST_CHECK_THROW( QField1D_CG_BoundaryTrace qfld1(xfld1, order), DeveloperException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysD1, TopoD1, ArrayQ > QField1D_CG_Cell;
//  typedef Field_CG_InteriorTrace< PhysD1, TopoD1, ArrayQ > QField1D_CG_InteriorTrace;
  typedef Field_CG_BoundaryTrace< PhysD1, TopoD1, ArrayQ > QField1D_CG_BoundaryTrace;

  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Field/Field1D_CG_pattern.txt", true );

  XField1D xfld(2);

  QField1D_CG_Cell qfld1(xfld, 2, BasisFunctionCategory_Hierarchical);
  qfld1 = 0;
  qfld1.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

//  QField1D_CG_InteriorTrace qfld2(xfld, 2);
//  qfld2.dump( 2, output );
//  BOOST_CHECK( output.match_pattern() );

  QField1D_CG_BoundaryTrace qfld4(xfld, 2, BasisFunctionCategory_Hierarchical);
  qfld4 = 0;
  qfld4.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
