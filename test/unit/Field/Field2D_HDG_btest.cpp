// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Field2D_HDG_btest
// Tests Field_DG_Cell< PhysD2, TopoD2, DLA::VectorS<2, ArrayQ> >
//

// This is nearly identical to Field2D_DG_Triangle, but is meant to test
// a field of VectorArrayQ (i.e. DLA::VectorS<2, ArrayQ>) used for
// the HDG gradient variable

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;


#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_InteriorTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_DG_Trace.h"

using namespace std;
using namespace SANS;


//Explicitly instantiate classes to get proper coverage information
namespace SANS
{
template class Field_DG_Cell< PhysD2, TopoD2, Real >;
template class Field_DG_Trace< PhysD2, TopoD2, Real >;
template class Field_DG_InteriorTrace< PhysD2, TopoD2, Real >;
template class Field_DG_BoundaryTrace< PhysD2, TopoD2, Real >;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( Field2D_HDG_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_Area_LegendreP0 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD2, TopoD2, DLA::VectorS<2, ArrayQ> > QField2D_DG_Area;
  typedef QField2D_DG_Area::FieldCellGroupType<Triangle> QFieldAreaClass;
  typedef QFieldAreaClass::ElementType<> ElementQFieldClass;

  XField2D_2Triangle_X1_1Group xfld1;

  int order = 0;
  QField2D_DG_Area qfld1(xfld1, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 2, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  XField2D_4Triangle_X1_1Group xfld2;

  QField2D_DG_Area qfld2(xfld2, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 4, qfld2.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld2.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld2, &qfld2.getXField() );

  const int nDOFPDE = qfld2.nDOF();
  for (int n = 0; n < nDOFPDE; n++)
    qfld2.DOF(n) = n;

  Field< PhysD2, TopoD2, DLA::VectorS<2, ArrayQ> > qfld3(qfld2, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld2.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld2.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nCellGroups()      ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld2.getXField()           , &qfld3.getXField() );

  typedef QField2D_DG_Area::FieldCellGroupType<Triangle> QFieldAreaClass;

  const QFieldAreaClass& qfld2Area = qfld2.getCellGroup<Triangle>(0);
  const QFieldAreaClass& qfld3Area = qfld3.getCellGroup<Triangle>(0);

  for (int n = 0; n < nDOFPDE; n++)
  {
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[0][0], n );
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[1][0], n );
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[0][0], qfld2Area.DOF(n)[0][0] );
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[1][0], qfld2Area.DOF(n)[1][0] );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0][0], n );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[1][0], n );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0][0], qfld3Area.DOF(n)[0][0] );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[1][0], qfld3Area.DOF(n)[1][0] );
  }

  for (int n = 0; n < nDOFPDE; n++)
    qfld2.DOF(n) = 0;

  for (int n = 0; n < nDOFPDE; n++)
  {
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[0][0], 0 );
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[1][0], 0 );
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[0][0], qfld2Area.DOF(n)[0][0] );
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[1][0], qfld2Area.DOF(n)[1][0] );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0][0], n );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[1][0], n );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0][0], qfld3Area.DOF(n)[0][0] );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[1][0], qfld3Area.DOF(n)[1][0] );
  }


  // Test the constant assignment operator
  const QFieldAreaClass& qfld1Area = qfld1.getCellGroup<Triangle>(0);
  ElementQFieldClass qfldElem(qfld1Area.basis());

  qfld1 = 1.23;
  DLA::VectorS<2, ArrayQ> q1;

  for (int elem = 0; elem < qfld1Area.nElem(); elem++)
  {
    qfld1Area.getElement(qfldElem, elem);

    qfldElem.eval(0.25, 0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0][0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[0][1], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1][0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1][1], 1e-12);
  }

  DLA::VectorS<2, ArrayQ> q0 = {{4.56, 6.3}, {7.89, -0.23}};
  qfld1 = q0;

  for (int elem = 0; elem < qfld1Area.nElem(); elem++)
  {
    qfld1Area.getElement(qfldElem, elem);

    qfldElem.eval(0.25, 0.25, q1);
    BOOST_CHECK_CLOSE(q0[0][0], q1[0][0], 1e-12);
    BOOST_CHECK_CLOSE(q0[0][1], q1[0][1], 1e-12);
    BOOST_CHECK_CLOSE(q0[1][0], q1[1][0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1][1], q1[1][1], 1e-12);
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_Area_LegendreP1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD2, TopoD2, DLA::VectorS<2, ArrayQ> > QField2D_DG_Area;

  XField2D_2Triangle_X1_1Group xfld1;

  int order = 1;
  QField2D_DG_Area qfld1(xfld1, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 2*3, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  XField2D_4Triangle_X1_1Group xfld2;

  QField2D_DG_Area qfld2(xfld2, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 4*3, qfld2.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld2.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld2, &qfld2.getXField() );

  const int nDOFPDE = qfld2.nDOF();
  for (int n = 0; n < nDOFPDE; n++)
    qfld2.DOF(n) = n;

  Field< PhysD2, TopoD2, DLA::VectorS<2, ArrayQ> > qfld3(qfld2, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld2.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld2.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nCellGroups()         ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld2.getXField()           , &qfld3.getXField() );

  typedef QField2D_DG_Area::FieldCellGroupType<Triangle> QFieldAreaClass;

        QFieldAreaClass& qfld2Area = qfld2.getCellGroup<Triangle>(0);
  const QFieldAreaClass& qfld3Area = qfld3.getCellGroup<Triangle>(0);

  for (int n = 0; n < nDOFPDE; n++)
  {
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[0][0], n );
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[1][0], n );
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[0][0], qfld2Area.DOF(n)[0][0] );
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[1][0], qfld2Area.DOF(n)[1][0] );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0][0], n );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[1][0], n );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0][0], qfld3Area.DOF(n)[0][0] );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[1][0], qfld3Area.DOF(n)[1][0] );
  }

  for (int n = 0; n < nDOFPDE; n++)
    qfld2.DOF(n) = 0;

  for (int n = 0; n < nDOFPDE; n++)
  {
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[0][0], 0 );
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[1][0], 0 );
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[0][0], qfld2Area.DOF(n)[0][0] );
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[1][0], qfld2Area.DOF(n)[1][0] );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0][0], n );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[1][0], n );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0][0], qfld3Area.DOF(n)[0][0] );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[1][0], qfld3Area.DOF(n)[1][0] );
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_Area_LegendreP2 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD2, TopoD2, DLA::VectorS<2, ArrayQ> > QField2D_DG_Area;

  XField2D_2Triangle_X1_1Group xfld1;

  int order = 2;
  QField2D_DG_Area qfld1(xfld1, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 2*6, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  XField2D_4Triangle_X1_1Group xfld2;

  QField2D_DG_Area qfld2(xfld2, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 4*6, qfld2.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld2.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld2, &qfld2.getXField() );

  const int nDOFPDE = qfld2.nDOF();
  for (int n = 0; n < nDOFPDE; n++)
    qfld2.DOF(n) = n;

  Field< PhysD2, TopoD2, DLA::VectorS<2, ArrayQ> > qfld3(qfld2, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld2.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld2.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nCellGroups()         ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld2.getXField()           , &qfld3.getXField() );

  typedef QField2D_DG_Area::FieldCellGroupType<Triangle> QFieldAreaClass;

        QFieldAreaClass& qfld2Area = qfld2.getCellGroup<Triangle>(0);
  const QFieldAreaClass& qfld3Area = qfld3.getCellGroup<Triangle>(0);

  for (int n = 0; n < nDOFPDE; n++)
  {
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[0][0], n );
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[1][0], n );
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[0][0], qfld2Area.DOF(n)[0][0] );
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[1][0], qfld2Area.DOF(n)[1][0] );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0][0], n );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[1][0], n );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0][0], qfld3Area.DOF(n)[0][0] );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[1][0], qfld3Area.DOF(n)[1][0] );
  }

  for (int n = 0; n < nDOFPDE; n++)
    qfld2.DOF(n) = 0;

  for (int n = 0; n < nDOFPDE; n++)
  {
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[0][0], 0 );
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[1][0], 0 );
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[0][0], qfld2Area.DOF(n)[0][0] );
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[1][0], qfld2Area.DOF(n)[1][0] );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0][0], n );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[1][0], n );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0][0], qfld3Area.DOF(n)[0][0] );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[1][0], qfld3Area.DOF(n)[1][0] );
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_Area_LegendreP3 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD2, TopoD2, DLA::VectorS<2, ArrayQ> > QField2D_DG_Area;

  XField2D_2Triangle_X1_1Group xfld1;

  int order = 3;
  QField2D_DG_Area qfld1(xfld1, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 2*10, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  XField2D_4Triangle_X1_1Group xfld2;

  QField2D_DG_Area qfld2(xfld2, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 4*10, qfld2.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld2.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld2, &qfld2.getXField() );

  const int nDOFPDE = qfld2.nDOF();
  for (int n = 0; n < nDOFPDE; n++)
    qfld2.DOF(n) = n;

  Field< PhysD2, TopoD2, DLA::VectorS<2, ArrayQ> > qfld3(qfld2, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld2.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld2.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nCellGroups()         ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld2.getXField()           , &qfld3.getXField() );

  typedef QField2D_DG_Area::FieldCellGroupType<Triangle> QFieldAreaClass;

        QFieldAreaClass& qfld2Area = qfld2.getCellGroup<Triangle>(0);
  const QFieldAreaClass& qfld3Area = qfld3.getCellGroup<Triangle>(0);

  for (int n = 0; n < nDOFPDE; n++)
  {
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[0][0], n );
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[1][0], n );
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[0][0], qfld2Area.DOF(n)[0][0] );
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[1][0], qfld2Area.DOF(n)[1][0] );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0][0], n );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[1][0], n );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0][0], qfld3Area.DOF(n)[0][0] );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[1][0], qfld3Area.DOF(n)[1][0] );
  }

  for (int n = 0; n < nDOFPDE; n++)
    qfld2.DOF(n) = 0;

  for (int n = 0; n < nDOFPDE; n++)
  {
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[0][0], 0 );
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[1][0], 0 );
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[0][0], qfld2Area.DOF(n)[0][0] );
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[1][0], qfld2Area.DOF(n)[1][0] );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0][0], n );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[1][0], n );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0][0], qfld3Area.DOF(n)[0][0] );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[1][0], qfld3Area.DOF(n)[1][0] );
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_Area_LegendreP4 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD2, TopoD2, DLA::VectorS<2, ArrayQ> > QField2D_DG_Area;

  XField2D_2Triangle_X1_1Group xfld1;

  int order = 4;
  QField2D_DG_Area qfld1(xfld1, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 2*15, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  XField2D_4Triangle_X1_1Group xfld2;

  QField2D_DG_Area qfld2(xfld2, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 4*15, qfld2.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld2.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld2, &qfld2.getXField() );

  const int nDOFPDE = qfld2.nDOF();
  for (int n = 0; n < nDOFPDE; n++)
    qfld2.DOF(n) = n;

  Field< PhysD2, TopoD2, DLA::VectorS<2, ArrayQ> > qfld3(qfld2, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld2.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld2.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nCellGroups()         ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld2.getXField()           , &qfld3.getXField() );

  typedef QField2D_DG_Area::FieldCellGroupType<Triangle> QFieldAreaClass;

        QFieldAreaClass& qfld2Area = qfld2.getCellGroup<Triangle>(0);
  const QFieldAreaClass& qfld3Area = qfld3.getCellGroup<Triangle>(0);

  for (int n = 0; n < nDOFPDE; n++)
  {
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[0][0], n );
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[1][0], n );
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[0][0], qfld2Area.DOF(n)[0][0] );
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[1][0], qfld2Area.DOF(n)[1][0] );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0][0], n );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[1][0], n );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0][0], qfld3Area.DOF(n)[0][0] );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[1][0], qfld3Area.DOF(n)[1][0] );
  }

  for (int n = 0; n < nDOFPDE; n++)
    qfld2.DOF(n) = 0;

  for (int n = 0; n < nDOFPDE; n++)
  {
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[0][0], 0 );
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[1][0], 0 );
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[0][0], qfld2Area.DOF(n)[0][0] );
    BOOST_CHECK_EQUAL( qfld2.DOF(n)[1][0], qfld2Area.DOF(n)[1][0] );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0][0], n );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[1][0], n );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[0][0], qfld3Area.DOF(n)[0][0] );
    BOOST_CHECK_EQUAL( qfld3.DOF(n)[1][0], qfld3Area.DOF(n)[1][0] );
  }

}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_Area_ProjectPtoPp1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD2, TopoD2, DLA::VectorS<2, ArrayQ> > QField2D_DG_Area;
  typedef QField2D_DG_Area::FieldCellGroupType<Triangle> QFieldAreaClass;
  typedef QFieldAreaClass::ElementType<> ElementQFieldClass;

  DLA::VectorS<2, ArrayQ> q0, q1;

  XField2D_4Triangle_X1_1Group xfld1;

  for (int order = 0; order < BasisFunctionArea_Triangle_LegendrePMax; order++)
  {
    for (int orderinc = 1; orderinc <= BasisFunctionArea_Triangle_LegendrePMax-order; orderinc++)
    {
      QField2D_DG_Area qfldP  (xfld1, order         , BasisFunctionCategory_Legendre);
      QField2D_DG_Area qfldPp1(xfld1, order+orderinc, BasisFunctionCategory_Legendre);

      typedef QField2D_DG_Area::FieldCellGroupType<Triangle> QFieldAreaClass;

      QFieldAreaClass& qfldAeraP   = qfldP.getCellGroup<Triangle>(0);
      QFieldAreaClass& qfldAeraPp1 = qfldPp1.getCellGroup<Triangle>(0);

      ElementQFieldClass qfldElemP(qfldAeraP.basis());
      ElementQFieldClass qfldElemPp1(qfldAeraPp1.basis());

      //Give some non-zero initial condition
      for (int n = 0; n < qfldP.nDOF(); n++)
        qfldP.DOF(n) = n+1;

      for (int n = 0; n < qfldPp1.nDOF(); n++)
        qfldPp1.DOF(n) = 0;

      //Use area function projectTo
      qfldAeraP.projectTo(qfldAeraPp1);

      for (int elem = 0; elem < qfldAeraP.nElem(); elem++)
      {
        qfldAeraP.getElement(qfldElemP, elem);
        qfldAeraPp1.getElement(qfldElemPp1, elem);

        qfldElemP.eval(0.25, 0.25, q0);
        qfldElemPp1.eval(0.25, 0.25, q1);
        BOOST_CHECK_CLOSE(q0[0][0], q1[0][0], 1e-12);
        BOOST_CHECK_CLOSE(q0[1][0], q1[0][1], 1e-12);
      }

      //Wipe out DOF's for P1
      for (int n = 0; n < qfldPp1.nDOF(); n++)
        qfldPp1.DOF(n) = -1;

      //Use base function projectTo
      qfldP.projectTo(qfldPp1);

      for (int elem = 0; elem < qfldAeraP.nElem(); elem++)
      {
        qfldAeraP.getElement(qfldElemP, elem);
        qfldAeraPp1.getElement(qfldElemPp1, elem);

        qfldElemP.eval(0.25, 0.25, q0);
        qfldElemPp1.eval(0.25, 0.25, q1);
        BOOST_CHECK_CLOSE(q0[0][0], q1[0][0], 1e-12);
        BOOST_CHECK_CLOSE(q0[1][0], q1[0][1], 1e-12);
      }
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_InteriorEdge_LegendreP0 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_InteriorTrace< PhysD2, TopoD2, ArrayQ > QField2D_DG_InteriorEdge;
  typedef QField2D_DG_InteriorEdge::FieldTraceGroupType<Line> QFieldLineClass;

  XField2D_4Triangle_X1_1Group xfld1;

  int edgeMap[1];

  int order = 0;
  QField2D_DG_InteriorEdge qfld1(xfld1, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 3, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  BOOST_CHECK_THROW( qfld1.nDOFCellGroup(0), SANSException);
  BOOST_CHECK_EQUAL( 3, qfld1.nDOFInteriorTraceGroup(0) );
  BOOST_CHECK_THROW( qfld1.nDOFBoundaryTraceGroup(0), SANSException);

  const QFieldLineClass& qfldGroup1 = qfld1.getInteriorTraceGroup<Line>(0);

  qfldGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 0, edgeMap[0] );

  qfldGroup1.associativity(1).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 1, edgeMap[0] );

  qfldGroup1.associativity(2).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 2, edgeMap[0] );

  Field< PhysD2, TopoD2, ArrayQ > qfld3(qfld1, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()         ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld3.getXField() );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_Edge_LegendreP0 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Trace< PhysD2, TopoD2, ArrayQ > QField2D_DG_Edge;
  typedef QField2D_DG_Edge::FieldTraceGroupType<Line> QFieldLineClass;

  XField2D_4Triangle_X1_1Group xfld1;

  int edgeMap[1];

  int order = 0;
  QField2D_DG_Edge qfld1(xfld1, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 9, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  BOOST_CHECK_THROW( qfld1.nDOFCellGroup(0), SANSException);
  BOOST_CHECK_EQUAL( 3, qfld1.nDOFInteriorTraceGroup(0) );
  BOOST_CHECK_EQUAL( 6, qfld1.nDOFBoundaryTraceGroup(0) );

  const QFieldLineClass& qfldGroup1 = qfld1.getInteriorTraceGroup<Line>(0);

  qfldGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 0, edgeMap[0] );

  qfldGroup1.associativity(1).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 1, edgeMap[0] );

  qfldGroup1.associativity(2).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( 2, edgeMap[0] );

  Field< PhysD2, TopoD2, ArrayQ > qfld3(qfld1, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()         ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld3.getXField() );

  BOOST_CHECK_THROW( qfld3.nDOFCellGroup(0), DeveloperException);
  BOOST_CHECK_THROW( qfld3.nDOFInteriorTraceGroup(0), DeveloperException);
  BOOST_CHECK_THROW( qfld3.nDOFBoundaryTraceGroup(0), DeveloperException);
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_InteriorEdge_Legendre_ProjectPtoPp1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_InteriorTrace< PhysD2, TopoD2, ArrayQ > QField2D_DG_InteriorEdge;
  typedef QField2D_DG_InteriorEdge::FieldTraceGroupType<Line> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  ArrayQ q1, q2;

  XField2D_4Triangle_X1_1Group xfld1;

  for (int order = 0; order < BasisFunctionLine_LegendrePMax; order++)
  {
    for (int orderinc = 1; orderinc <= BasisFunctionLine_LegendrePMax-order; orderinc++)
    {
      QField2D_DG_InteriorEdge qfldP(xfld1, order  , BasisFunctionCategory_Legendre);
      QField2D_DG_InteriorEdge qfldPp1(xfld1, order+orderinc, BasisFunctionCategory_Legendre);

      for ( int group = 0; group < qfldP.nInteriorTraceGroups(); group++ )
      {
        QFieldLineClass& qfldLineGroupP   = qfldP.getInteriorTraceGroup<Line>(group);
        QFieldLineClass& qfldLineGroupPp1 = qfldPp1.getInteriorTraceGroup<Line>(group);

        ElementQFieldLineClass qfldElemP(qfldLineGroupP.basis());
        ElementQFieldLineClass qfldElemPp1(qfldLineGroupPp1.basis());

        //Give some non-zero initial condition
        for (int n = 0; n < qfldP.nDOF(); n++)
          qfldP.DOF(n) = n+1;

        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = 0;

        //Use line function projectTo
        qfldLineGroupP.projectTo(qfldLineGroupPp1);

        for (int elem = 0; elem < qfldLineGroupP.nElem(); elem++)
        {
          qfldLineGroupP.getElement(qfldElemP, elem);
          qfldLineGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(0.25, q1);
          qfldElemPp1.eval(0.25, q2);
          BOOST_CHECK_CLOSE(q1[0], q2[0], 1e-12);
        }

        //Wipe out DOF's for P1
        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = -1;

        //Use base function projectTo
        qfldP.projectTo(qfldPp1);

        for (int elem = 0; elem < qfldLineGroupP.nElem(); elem++)
        {
          qfldLineGroupP.getElement(qfldElemP, elem);
          qfldLineGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(0.75, q1);
          qfldElemPp1.eval(0.75, q2);
          BOOST_CHECK_CLOSE(q1[0], q2[0], 1e-12);
        }
      }
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_Edge_Legendre_ProjectPtoPp1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Trace< PhysD2, TopoD2, ArrayQ > QField2D_DG_Edge;
  typedef QField2D_DG_Edge::FieldTraceGroupType<Line> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  ArrayQ q1, q2;

  XField2D_4Triangle_X1_1Group xfld1;

  for (int order = 0; order < BasisFunctionLine_LegendrePMax; order++)
  {
    for (int orderinc = 1; orderinc <= BasisFunctionLine_LegendrePMax-order; orderinc++)
    {
      QField2D_DG_Edge qfldP(xfld1, order  , BasisFunctionCategory_Legendre);
      QField2D_DG_Edge qfldPp1(xfld1, order+orderinc, BasisFunctionCategory_Legendre);

      for ( int group = 0; group < qfldP.nInteriorTraceGroups(); group++ )
      {
        QFieldLineClass& qfldLineGroupP   = qfldP.getInteriorTraceGroup<Line>(group);
        QFieldLineClass& qfldLineGroupPp1 = qfldPp1.getInteriorTraceGroup<Line>(group);

        ElementQFieldLineClass qfldElemP(qfldLineGroupP.basis());
        ElementQFieldLineClass qfldElemPp1(qfldLineGroupPp1.basis());

        //Give some non-zero initial condition
        for (int n = 0; n < qfldP.nDOF(); n++)
          qfldP.DOF(n) = n+1;

        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = 0;

        //Use line function projectTo
        qfldLineGroupP.projectTo(qfldLineGroupPp1);

        for (int elem = 0; elem < qfldLineGroupP.nElem(); elem++)
        {
          qfldLineGroupP.getElement(qfldElemP, elem);
          qfldLineGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(0.25, q1);
          qfldElemPp1.eval(0.25, q2);
          BOOST_CHECK_CLOSE(q1[0], q2[0], 1e-12);
        }

        //Wipe out DOF's for P1
        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = -1;

        //Use base function projectTo
        qfldP.projectTo(qfldPp1);

        for (int elem = 0; elem < qfldLineGroupP.nElem(); elem++)
        {
          qfldLineGroupP.getElement(qfldElemP, elem);
          qfldLineGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(0.75, q1);
          qfldElemPp1.eval(0.75, q2);
          BOOST_CHECK_CLOSE(q1[0], q2[0], 1e-12);
        }
      }

      for ( int group = 0; group < qfldP.nBoundaryTraceGroups(); group++ )
      {
        QFieldLineClass& qfldLineGroupP   = qfldP.getBoundaryTraceGroup<Line>(group);
        QFieldLineClass& qfldLineGroupPp1 = qfldPp1.getBoundaryTraceGroup<Line>(group);

        ElementQFieldLineClass qfldElemP(qfldLineGroupP.basis());
        ElementQFieldLineClass qfldElemPp1(qfldLineGroupPp1.basis());

        //Give some non-zero initial condition
        for (int n = 0; n < qfldP.nDOF(); n++)
          qfldP.DOF(n) = n+1;

        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = 0;

        //Use line function projectTo
        qfldLineGroupP.projectTo(qfldLineGroupPp1);

        for (int elem = 0; elem < qfldLineGroupP.nElem(); elem++)
        {
          qfldLineGroupP.getElement(qfldElemP, elem);
          qfldLineGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(0.25, q1);
          qfldElemPp1.eval(0.25, q2);
          BOOST_CHECK_CLOSE(q1[0], q2[0], 1e-12);
        }

        //Wipe out DOF's for P1
        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = -1;

        //Use base function projectTo
        qfldP.projectTo(qfldPp1);

        for (int elem = 0; elem < qfldLineGroupP.nElem(); elem++)
        {
          qfldLineGroupP.getElement(qfldElemP, elem);
          qfldLineGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(0.75, q1);
          qfldElemPp1.eval(0.75, q2);
          BOOST_CHECK_CLOSE(q1[0], q2[0], 1e-12);
        }
      }
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_BoundaryEdge_LegendreP0 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_BoundaryTrace< PhysD2, TopoD2, ArrayQ > QField2D_DG_BoundaryEdge;

  XField2D_2Triangle_X1_1Group xfld1;

  int order = 0;
  QField2D_DG_BoundaryEdge qfld1(xfld1, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 4, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  BOOST_CHECK_THROW( qfld1.nDOFCellGroup(0), SANSException);
  BOOST_CHECK_THROW( qfld1.nDOFInteriorTraceGroup(0), SANSException);
  BOOST_CHECK_EQUAL( 4, qfld1.nDOFBoundaryTraceGroup(0) );

  Field< PhysD2, TopoD2, ArrayQ > qfld3(qfld1, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()         ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld3.getXField() );

  BOOST_CHECK_THROW( qfld3.nDOFCellGroup(0), DeveloperException);
  BOOST_CHECK_THROW( qfld3.nDOFInteriorTraceGroup(0), DeveloperException);
  BOOST_CHECK_THROW( qfld3.nDOFBoundaryTraceGroup(0), DeveloperException);
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_BoundaryEdge_Legendre_ProjectPtoPp1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_BoundaryTrace< PhysD2, TopoD2, ArrayQ > QField2D_DG_BoundaryEdge;
  typedef QField2D_DG_BoundaryEdge::FieldTraceGroupType<Line> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  ArrayQ q1, q2;

  XField2D_4Triangle_X1_1Group xfld1;

  for (int order = 0; order < BasisFunctionLine_LegendrePMax; order++)
  {
    for (int orderinc = 1; orderinc <= BasisFunctionLine_LegendrePMax-order; orderinc++)
    {
      QField2D_DG_BoundaryEdge qfldP(xfld1, order  , BasisFunctionCategory_Legendre);
      QField2D_DG_BoundaryEdge qfldPp1(xfld1, order+orderinc, BasisFunctionCategory_Legendre);

      for ( int group = 0; group < qfldP.nBoundaryTraceGroups(); group++ )
      {
        QFieldLineClass& qfldLineGroupP   = qfldP.getBoundaryTraceGroup<Line>(group);
        QFieldLineClass& qfldLineGroupPp1 = qfldPp1.getBoundaryTraceGroup<Line>(group);

        ElementQFieldLineClass qfldElemP(qfldLineGroupP.basis());
        ElementQFieldLineClass qfldElemPp1(qfldLineGroupPp1.basis());

        //Give some non-zero initial condition
        for (int n = 0; n < qfldP.nDOF(); n++)
          qfldP.DOF(n) = n+1;

        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = 0;

        //Use line function projectTo
        qfldLineGroupP.projectTo(qfldLineGroupPp1);

        for (int elem = 0; elem < qfldLineGroupP.nElem(); elem++)
        {
          qfldLineGroupP.getElement(qfldElemP, elem);
          qfldLineGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(0.25, q1);
          qfldElemPp1.eval(0.25, q2);
          BOOST_CHECK_CLOSE(q1[0], q2[0], 1e-12);
        }

        //Wipe out DOF's for P1
        for (int n = 0; n < qfldPp1.nDOF(); n++)
          qfldPp1.DOF(n) = -1;

        //Use base function projectTo
        qfldP.projectTo(qfldPp1);

        for (int elem = 0; elem < qfldLineGroupP.nElem(); elem++)
        {
          qfldLineGroupP.getElement(qfldElemP, elem);
          qfldLineGroupPp1.getElement(qfldElemPp1, elem);

          qfldElemP.eval(0.75, q1);
          qfldElemPp1.eval(0.75, q2);
          BOOST_CHECK_CLOSE(q1[0], q2[0], 1e-12);
        }
      }
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_Exception )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD2, TopoD2, DLA::VectorS<2, ArrayQ> > QField2D_DG_Area;
  typedef Field_DG_InteriorTrace< PhysD2, TopoD2, ArrayQ > QField2D_DG_InteriorEdge;
  typedef Field_DG_BoundaryTrace< PhysD2, TopoD2, ArrayQ > QField2D_DG_BoundaryEdge;
  typedef Field_DG_Trace< PhysD2, TopoD2, ArrayQ > QField2D_DG_Edge;

  XField2D_2Triangle_X1_1Group xfld1;

  int order = 999; //This should always be an order that is not available

  BOOST_CHECK_THROW( QField2D_DG_Area qfld1(xfld1, order, BasisFunctionCategory_Legendre), DeveloperException );
  BOOST_CHECK_THROW( QField2D_DG_Area qfld1(xfld1, order, BasisFunctionCategory_Legendre), DeveloperException );
  BOOST_CHECK_THROW( QField2D_DG_InteriorEdge qfld1(xfld1, order, BasisFunctionCategory_Legendre), DeveloperException );
  BOOST_CHECK_THROW( QField2D_DG_BoundaryEdge qfld1(xfld1, order, BasisFunctionCategory_Legendre), DeveloperException );
  BOOST_CHECK_THROW( QField2D_DG_Edge qfld1(xfld1, order, BasisFunctionCategory_Legendre), DeveloperException );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD2, TopoD2, DLA::VectorS<2, ArrayQ> > QField2D_DG_Area;
  typedef Field_DG_InteriorTrace< PhysD2, TopoD2, ArrayQ > QField2D_DG_InteriorEdge;
  typedef Field_DG_Trace< PhysD2, TopoD2, ArrayQ > QField2D_DG_Edge;
  typedef Field_DG_BoundaryTrace< PhysD2, TopoD2, ArrayQ > QField2D_DG_BoundaryEdge;

  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Field/Field2D_HDG_pattern.txt", true );

  XField2D_2Triangle_X1_1Group xfld1;

  QField2D_DG_Area qfld1(xfld1, 0, BasisFunctionCategory_Legendre);
  qfld1 = 0;
  qfld1.dump( 2, output );

  QField2D_DG_InteriorEdge qfld2(xfld1, 2, BasisFunctionCategory_Legendre);
  qfld2 = 0;
  qfld2.dump( 2, output );

  QField2D_DG_BoundaryEdge qfld3(xfld1, 2, BasisFunctionCategory_Legendre);
  qfld3 = 0;
  qfld3.dump( 2, output );

  QField2D_DG_Edge qfld4(xfld1, 2, BasisFunctionCategory_Legendre);
  qfld4 = 0;
  qfld4.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
