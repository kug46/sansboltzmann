// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// FieldDataInvMassMatrix_Cell_btest
// testing of FieldDataInvMassMatrix_Cell
//

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField1D_2Line_X1_1Group.h"
#include "unit/UnitGrids/XField2D_2Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_2Quad_X1_1Group.h"
#include "unit/UnitGrids/XField2D_4Quad_X1_1Group.h"

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldData/FieldDataInvMassMatrix_Cell.h"

#include "Quadrature/Quadrature.h"

#include "LinearAlgebra/DenseLinAlg/InverseLU.h"

using namespace std;
using namespace SANS;


//Explicitly instantiate classes to get proper coverage information
namespace SANS
{
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( FieldDataInvMassMatrix_Cell_test_suite )


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Line_1D_Legendre )
{
  typedef Real ArrayQ;
  typedef XField< PhysD1, TopoD1> XField_1D;
  typedef XField_1D::FieldCellGroupType<Line> XFieldCellGroupType;
  typedef XFieldCellGroupType::ElementType<> XElementType;

  typedef Field_DG_Cell< PhysD1, TopoD1, ArrayQ > QField_DG_1D;
  typedef QField_DG_1D::FieldCellGroupType<Line> FieldCellGroupType;
  typedef FieldCellGroupType::ElementType<> ElementType;
  typedef ElementType::RefCoordType RefCoordType;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  Real weight;
  RefCoordType sRef;

  for (int order=0; order<=3; order++)
  {
    Quadrature<TopoD1, Line> quadrature( 2*order + 1 );

    XField1D_2Line_X1_1Group xfld;

    XFieldCellGroupType& xfldCell = xfld.getCellGroup<Line>(0);


    QField_DG_1D qfld(xfld, order, BasisFunctionCategory_Legendre);

    FieldCellGroupType& qfldCell = qfld.getCellGroupGlobal<Line>(0);


    // Compute the field of inverse mass matrices
    FieldDataInvMassMatrix_Cell mmfld(qfld);

    DLA::MatrixDView_Array<Real>& mmfldCell = mmfld.getCellGroupGlobal(0);


    XElementType xfldElem( xfldCell.basis() );
    ElementType qfldElem( qfldCell.basis() );
    const int nDOF = qfldCell.basis()->nBasis();
    std::vector<Real> phi(nDOF);

    for (int elem = 0; elem < qfldCell.nElem(); elem++)
    {
      // copy global grid DOFs to element
      xfldCell.getElement( xfldElem, elem );

      DLA::MatrixD<Real> mtx( nDOF, nDOF );
      mtx = 0;

      const int nquad = quadrature.nQuadrature();
      for (int iquad = 0; iquad < nquad; iquad++)
      {
        quadrature.weight( iquad, weight );
        quadrature.coordinates( iquad, sRef );

        Real dJ = weight * xfldElem.jacobianDeterminant( sRef );
        qfldElem.evalBasis( sRef, phi.data(), phi.size() );

        for (int i = 0; i < nDOF; i++)
          for (int j = 0; j < nDOF; j++)
            mtx(i,j) += dJ*phi[i]*phi[j];
      }

      mtx = DLA::InverseLU::Inverse(mtx);


      DLA::MatrixD<Real> Minv = mmfldCell[elem];
      for (int i = 0; i < Minv.m(); i++)
        for (int j = 0; j < Minv.n(); j++)
          SANS_CHECK_CLOSE(mtx(i,j), Minv(i,j), small_tol, close_tol );
    }
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Area_Triangle_2D_Legendre )
{
  typedef Real ArrayQ;
  typedef XField< PhysD2, TopoD2> XField_2D;
  typedef XField_2D::FieldCellGroupType<Triangle> XFieldCellGroupType;
  typedef XFieldCellGroupType::ElementType<> XElementType;

  typedef Field_DG_Cell< PhysD2, TopoD2, ArrayQ > QField_DG_2D;
  typedef QField_DG_2D::FieldCellGroupType<Triangle> FieldCellGroupType;
  typedef FieldCellGroupType::ElementType<> ElementType;
  typedef ElementType::RefCoordType RefCoordType;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  Real weight;
  RefCoordType sRef;

  for (int order=0; order<=3; order++)
  {
    Quadrature<TopoD2, Triangle> quadrature( 2*order + 1 );

    XField2D_4Triangle_X1_1Group xfld;

    XFieldCellGroupType& xfldCell = xfld.getCellGroup<Triangle>(0);


    QField_DG_2D qfld(xfld, order, BasisFunctionCategory_Legendre);

    FieldCellGroupType& qfldCell = qfld.getCellGroupGlobal<Triangle>(0);


    // Compute the field of inverse mass matrices
    FieldDataInvMassMatrix_Cell mmfld(qfld);

    DLA::MatrixDView_Array<Real>& mmfldCell = mmfld.getCellGroupGlobal(0);


    XElementType xfldElem( xfldCell.basis() );
    ElementType qfldElem( qfldCell.basis() );
    const int nDOF = qfldCell.basis()->nBasis();
    std::vector<Real> phi(nDOF);

    for (int elem = 0; elem < qfldCell.nElem(); elem++)
    {
      // copy global grid DOFs to element
      xfldCell.getElement( xfldElem, elem );

      DLA::MatrixD<Real> mtx( nDOF, nDOF );
      mtx = 0;

      const int nquad = quadrature.nQuadrature();
      for (int iquad = 0; iquad < nquad; iquad++)
      {
        quadrature.weight( iquad, weight );
        quadrature.coordinates( iquad, sRef );

        Real dJ = weight * xfldElem.jacobianDeterminant( sRef );
        qfldElem.evalBasis( sRef, phi.data(), phi.size() );

        for (int i = 0; i < nDOF; i++)
          for (int j = 0; j < nDOF; j++)
            mtx(i,j) += dJ*phi[i]*phi[j];
      }

      mtx = DLA::InverseLU::Inverse(mtx);


      DLA::MatrixD<Real> Minv = mmfldCell[elem];
      for (int i = 0; i < Minv.m(); i++)
        for (int j = 0; j < Minv.n(); j++)
          SANS_CHECK_CLOSE(mtx(i,j), Minv(i,j), small_tol, close_tol );
    }
  }
}

#if 0 // Disabled until we get quad legendre polynomial orders increased
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Area_Quad_2D_Legendre )
{
  typedef Real ArrayQ;
  typedef XField< PhysD2, TopoD2> XField_2D;
  typedef XField_2D::FieldCellGroupType<Quad> XFieldCellGroupType;
  typedef XFieldCellGroupType::ElementType<> XElementType;

  typedef Field_DG_Cell< PhysD2, TopoD2, ArrayQ > QField_DG_2D;
  typedef QField_DG_2D::FieldCellGroupType<Quad> FieldCellGroupType;
  typedef FieldCellGroupType::ElementType<> ElementType;
  typedef ElementType::RefCoordType RefCoordType;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  Real weight;
  RefCoordType sRef;

  for (int order=0; order<=3; order++)
  {
    Quadrature<TopoD2, Triangle> quadrature( 2*order + 1 );

    XField2D_4Quad_X1_1Group xfld;

    XFieldCellGroupType& xfldCell = xfld.getCellGroup<Quad>(0);


    QField_DG_2D qfld(xfld, order, BasisFunctionCategory_Legendre);

    FieldCellGroupType& qfldCell = qfld.getCellGroupGlobal<Quad>(0);


    // Compute the field of inverse mass matrices
    FieldDataInvMassMatrix_Cell mmfld(qfld);

    DLA::MatrixDView_Array<Real>& mmfldCell = mmfld.getCellGroupGlobal(0);


    XElementType xfldElem( xfldCell.basis() );
    ElementType qfldElem( qfldCell.basis() );
    const int nDOF = qfldCell.basis()->nBasis();
    std::vector<Real> phi(nDOF);

    for (int elem = 0; elem < qfldCell.nElem(); elem++)
    {
      // copy global grid DOFs to element
      xfldCell.getElement( xfldElem, elem );

      DLA::MatrixD<Real> mtx( nDOF, nDOF );
      mtx = 0;

      const int nquad = quadrature.nQuadrature();
      for (int iquad = 0; iquad < nquad; iquad++)
      {
        quadrature.weight( iquad, weight );
        quadrature.coordinates( iquad, sRef );

        Real dJ = weight * xfldElem.jacobianDeterminant( sRef );
        qfldElem.evalBasis( sRef, phi.data(), phi.size() );

        for (int i = 0; i < nDOF; i++)
          for (int j = 0; j < nDOF; j++)
            mtx(i,j) += dJ*phi[i]*phi[j];
      }

      mtx = DLA::InverseLU::Inverse(mtx);


      DLA::MatrixD<Real> Minv = mmfldCell[elem];
      for (int i = 0; i < Minv.m(); i++)
        for (int j = 0; j < Minv.n(); j++)
          SANS_CHECK_CLOSE(mtx(i,j), Minv(i,j), small_tol, close_tol );
    }
  }
}
#endif
#if 0
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD2, TopoD2, ArrayQ > QField2D_DG_Area;
  typedef Field_DG_InteriorTrace< PhysD2, TopoD2, ArrayQ > QField2D_DG_InteriorEdge;
  typedef Field_DG_BoundaryTrace< PhysD2, TopoD2, ArrayQ > QField2D_DG_BoundaryEdge;

  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Field/Field2D_DG_pattern.txt", true );

  XField2D_2Triangle_X1_1Group xfld1;

  QField2D_DG_Area qfld1(xfld1, 0, BasisFunctionCategory_Legendre);
  qfld1.dump( 2, output );

  QField2D_DG_InteriorEdge qfld2(xfld1, 2, BasisFunctionCategory_Legendre);
  qfld2.dump( 2, output );

  QField2D_DG_BoundaryEdge qfld3(xfld1, 2, BasisFunctionCategory_Legendre);
  qfld3.dump( 2, output );

  BOOST_CHECK( output.match_pattern() );
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
