// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// GenHField_CG_btest

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "Field/HField/GenHFieldLine_CG.h"
#include "Field/HField/GenHFieldArea_CG.h"
#include "Field/HField/GenHFieldVolume_CG.h"

#include "Field/Local/XField_LocalPatch.h"

#include "Meshing/Metric/MetricOps.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField3D_6Tet_X1_1Group.h"

#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct


//############################################################################//
BOOST_AUTO_TEST_SUITE( GenHField_CG_test_suite )

BOOST_AUTO_TEST_CASE( statics )
{
  BOOST_CHECK( (GenHField_CG<PhysD1,TopoD1>::D == 1) );
  BOOST_CHECK( (GenHField_CG<PhysD2,TopoD2>::D == 2) );
  BOOST_CHECK( (GenHField_CG<PhysD3,TopoD3>::D == 3) );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Line_grid_equal_test )
{
  // grid
  XField1D xfld( 8 );

  typedef GenHField_CG<PhysD1,TopoD1>::FieldCellGroupType<Line> HFieldLineClass;
  typedef HFieldLineClass::ElementType<> ElementHFieldLineClass;
  typedef DLA::MatrixSymS<PhysD1::D,Real> MatrixSym;

  GenHField_CG<PhysD1,TopoD1> hfld( xfld );

  Real exactSize = 1.0/8.0;

  MatrixSym logH;
  HFieldLineClass& hfld1Line = hfld.getCellGroup<Line>(0);
  ElementHFieldLineClass hfldElem(hfld1Line.basis());

  for (int elem = 0; elem < hfld1Line.nElem(); elem++)
  {
    hfld1Line.getElement(hfldElem, elem);

    hfldElem.eval(0.25, logH);
    BOOST_CHECK_CLOSE(logH(0,0), log(exactSize), 1e-12);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Line_grid_varying_test )
{
  // grid
  XField1D xfld( 4 );
  xfld.DOF(0) = -0.1;
  xfld.DOF(1) =  0.0;
  xfld.DOF(2) =  0.2;
  xfld.DOF(3) =  0.5;
  xfld.DOF(4) =  0.9;

  GenHField_CG<PhysD1,TopoD1> logHfld( xfld );

  const Real small_tol = 1e-13;

  //Check if the h-value at each node is the average size of the cells around that node
  BOOST_CHECK_CLOSE(logHfld.DOF(0)(0,0), log(0.10), small_tol);
  BOOST_CHECK_CLOSE(logHfld.DOF(1)(0,0), log(1.0/sqrt(50.0)), small_tol);
  BOOST_CHECK_CLOSE(logHfld.DOF(2)(0,0), log(1.0/sqrt(50.0/3.0)), small_tol);
  BOOST_CHECK_CLOSE(logHfld.DOF(3)(0,0), log(1.0/sqrt(25.0/3.0)), small_tol);
  BOOST_CHECK_CLOSE(logHfld.DOF(4)(0,0), log(0.40), small_tol);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Area_grid_equal_test )
{
  // grid
  XField2D_4Triangle_X1_1Group xfld;

  typedef GenHField_CG<PhysD2,TopoD2>::FieldCellGroupType<Triangle> HFieldTriangleClass;
  typedef HFieldTriangleClass::ElementType<> ElementHFieldTriangleClass;
  typedef DLA::MatrixSymS<PhysD2::D,Real> MatrixSym;

  GenHField_CG<PhysD2,TopoD2> hfld( xfld );

  MatrixSym logH;
  HFieldTriangleClass& hfld1Triangle = hfld.getCellGroup<Triangle>(0);
  ElementHFieldTriangleClass hfldElem(hfld1Triangle.basis());

  for (int elem = 0; elem < hfld1Triangle.nElem(); elem++)
  {
    hfld1Triangle.getElement(hfldElem, elem);

    hfldElem.eval(1./3., 1./3., logH);
    BOOST_CHECK_CLOSE(logH(0,0),  0.0719205181129452318598, 1e-12);
    BOOST_CHECK_CLOSE(logH(1,0), -0.2746530721670274228488, 1e-12);
    BOOST_CHECK_CLOSE(logH(1,1),  0.0719205181129452318598, 1e-12);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Area_grid_Q2_equal_test )
{
  // grid
  XField2D_4Triangle_X1_1Group xfld;
  XField<PhysD2, TopoD2> xfldQ2(xfld, 2);

  typedef GenHField_CG<PhysD2,TopoD2>::FieldCellGroupType<Triangle> HFieldTriangleClass;
  typedef HFieldTriangleClass::ElementType<> ElementHFieldTriangleClass;
  typedef DLA::MatrixSymS<PhysD2::D,Real> MatrixSym;

  GenHField_CG<PhysD2,TopoD2> hfld( xfldQ2 );

  MatrixSym logH;
  HFieldTriangleClass& hfld1Triangle = hfld.getCellGroup<Triangle>(0);
  ElementHFieldTriangleClass hfldElem(hfld1Triangle.basis());

  for (int elem = 0; elem < hfld1Triangle.nElem(); elem++)
  {
    hfld1Triangle.getElement(hfldElem, elem);

    hfldElem.eval(1./3., 1./3., logH);
    BOOST_CHECK_CLOSE(logH(0,0),  0.0719205181129452318598, 1e-12);
    BOOST_CHECK_CLOSE(logH(1,0), -0.2746530721670274228488, 1e-12);
    BOOST_CHECK_CLOSE(logH(1,1),  0.0719205181129452318598, 1e-12);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Area_grid_parallel_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  // Grids are intentionally not uniform

  int ii = 3;
  int jj = 4;
  std::vector<Real> xvec(ii+1);
  std::vector<Real> yvec(jj+1);

  // construct vectors
  for (int i = 0; i < ii+1; i++)
    xvec[i] = i*i/Real(ii*ii);

  for (int j = 0; j < jj+1; j++)
    yvec[j] = 2*j*j/Real(jj*jj);

  // create identical global mesh on each processors
  XField2D_Box_Triangle_Lagrange_X1 xfld_global(comm_local, xvec, yvec);

  // create a parallelized grid
  XField2D_Box_Triangle_Lagrange_X1 xfld(world, xvec, yvec);

  typedef GenHField_CG<PhysD2,TopoD2>::FieldCellGroupType<Triangle> HFieldTriangleClass;
  typedef HFieldTriangleClass::ElementType<> ElementHFieldTriangleClass;
  typedef DLA::MatrixSymS<PhysD2::D,Real> MatrixSym;

  GenHField_CG<PhysD2,TopoD2> hfld_global( xfld_global );
  GenHField_CG<PhysD2,TopoD2> hfld( xfld );

  const std::vector<int> cellIDs = xfld.cellIDs(0);

  MatrixSym logH_global, logH;
  HFieldTriangleClass& hfld1Triangle_global = hfld_global.getCellGroup<Triangle>(0);
  ElementHFieldTriangleClass hfldElem_global(hfld1Triangle_global.basis());

  HFieldTriangleClass& hfld1Triangle = hfld.getCellGroup<Triangle>(0);
  ElementHFieldTriangleClass hfldElem(hfld1Triangle.basis());

  for (std::size_t elem = 0; elem < cellIDs.size(); elem++)
  {
    int elem_global = cellIDs[elem];
    hfld1Triangle_global.getElement(hfldElem_global, elem_global);
    hfld1Triangle.getElement(hfldElem, elem);

    hfldElem_global.eval(1./3., 1./3., logH_global);
    hfldElem.eval(1./3., 1./3., logH);

    BOOST_CHECK_CLOSE(logH_global(0,0), logH(0,0), 1e-12);
    BOOST_CHECK_CLOSE(logH_global(1,0), logH(1,0), 1e-12);
    BOOST_CHECK_CLOSE(logH_global(1,1), logH(1,1), 1e-12);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Area_grid_Q2_parallel_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  // Grids are intentionally not uniform

  int ii = 3;
  int jj = 4;
  std::vector<Real> xvec(ii+1);
  std::vector<Real> yvec(jj+1);

  // construct vectors
  for (int i = 0; i < ii+1; i++)
    xvec[i] = i*i/Real(ii*ii);

  for (int j = 0; j < jj+1; j++)
    yvec[j] = 2*j*j/Real(jj*jj);

  // create identical global mesh on each processors
  XField2D_Box_Triangle_Lagrange_X1 xfld_global(comm_local, xvec, yvec);
  XField<PhysD2, TopoD2> xfldQ2_global(xfld_global, 2);

  // create a parallelized grid
  XField2D_Box_Triangle_Lagrange_X1 xfld(world, xvec, yvec);
  XField<PhysD2, TopoD2> xfldQ2(xfld, 2);

  typedef GenHField_CG<PhysD2,TopoD2>::FieldCellGroupType<Triangle> HFieldTriangleClass;
  typedef HFieldTriangleClass::ElementType<> ElementHFieldTriangleClass;
  typedef DLA::MatrixSymS<PhysD2::D,Real> MatrixSym;

  GenHField_CG<PhysD2,TopoD2> hfld_global( xfldQ2_global );
  GenHField_CG<PhysD2,TopoD2> hfld( xfldQ2 );

  const std::vector<int> cellIDs = xfld.cellIDs(0);

  MatrixSym logH_global, logH;
  HFieldTriangleClass& hfld1Triangle_global = hfld_global.getCellGroup<Triangle>(0);
  ElementHFieldTriangleClass hfldElem_global(hfld1Triangle_global.basis());

  HFieldTriangleClass& hfld1Triangle = hfld.getCellGroup<Triangle>(0);
  ElementHFieldTriangleClass hfldElem(hfld1Triangle.basis());

  for (std::size_t elem = 0; elem < cellIDs.size(); elem++)
  {
    int elem_global = cellIDs[elem];
    hfld1Triangle_global.getElement(hfldElem_global, elem_global);
    hfld1Triangle.getElement(hfldElem, elem);

    hfldElem_global.eval(1./3., 1./3., logH_global);
    hfldElem.eval(1./3., 1./3., logH);

    BOOST_CHECK_CLOSE(logH_global(0,0), logH(0,0), 1e-12);
    BOOST_CHECK_CLOSE(logH_global(1,0), logH(1,0), 1e-12);
    BOOST_CHECK_CLOSE(logH_global(1,1), logH(1,1), 1e-12);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Line_local_mesh_isotropic_split_test )
{
  static const int D = PhysD1::D;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  // grid
  XField1D xfld( 3 );
  xfld.DOF(0) = 0.0;
  xfld.DOF(1) = 0.1;
  xfld.DOF(2) = 0.5;
  xfld.DOF(3) = 1.0;

  typedef DLA::MatrixSymS<D,Real> MatrixSym;

  //Build cell to trace connectivity structure
  XField_CellToTrace<PhysD1,TopoD1> connectivity(xfld);

  const int cellgroup = 0;
  const int cellelem = 1;

  // Extract the unsplit grid
  XField_LocalPatchConstructor<PhysD1,Line> xfld_construct(comm_local, connectivity, cellgroup, cellelem, SpaceType::Discontinuous);

  // Split the grid
  XField_LocalPatch<PhysD1,Line> xfld_split_local(xfld_construct, ElementSplitType::Edge, 0);


  GenHField_CG<PhysD1,TopoD1> hfld( xfld );

  GenHField_CG<PhysD1,TopoD1> hfld_local( xfld_split_local, hfld );

  BOOST_CHECK_EQUAL(hfld_local.nDOF(), 5);

  {
    MatrixSym ML = {1.0 / pow(0.1, 2.0)};
    MatrixSym MR = {1.0 / pow(0.2, 2.0)};
    std::vector<MatrixSym> metric_list = {ML, MR};

    //Average metric around node
    DLA::EigenSystemPair<D,Real> avgM = Metric::computeLogEuclideanAvg(metric_list);
    MatrixSym logH_true = log(pow(avgM, -0.5));

    MatrixSym logH = hfld_local.DOF(0);

    SANS_CHECK_CLOSE(logH(0,0), logH_true(0,0), small_tol, close_tol);
  }

  {
    MatrixSym ML = {1.0 / pow(0.2, 2.0)};
    MatrixSym MR = {1.0 / pow(0.5, 2.0)};
    std::vector<MatrixSym> metric_list = {ML, MR};

    //Average metric around node
    DLA::EigenSystemPair<D,Real> avgM = Metric::computeLogEuclideanAvg(metric_list);
    MatrixSym logH_true = log(pow(avgM, -0.5));

    MatrixSym logH = hfld_local.DOF(1);

    SANS_CHECK_CLOSE(logH(0,0), logH_true(0,0), small_tol, close_tol);
  }

  {
    MatrixSym ML = {1.0 / pow(0.5, 2.0)};
    std::vector<MatrixSym> metric_list = {ML};

    //Average metric around node
    DLA::EigenSystemPair<D,Real> avgM = Metric::computeLogEuclideanAvg(metric_list);
    MatrixSym logH_true = log(pow(avgM, -0.5));

    MatrixSym logH = hfld_local.DOF(2);

    SANS_CHECK_CLOSE(logH(0,0), logH_true(0,0), small_tol, close_tol);
  }

  {
    MatrixSym MR = {1.0 / pow(0.1, 2.0)};
    std::vector<MatrixSym> metric_list = {MR};

    //Average metric around node
    DLA::EigenSystemPair<D,Real> avgM = Metric::computeLogEuclideanAvg(metric_list);
    MatrixSym logH_true = log(pow(avgM, -0.5));

    MatrixSym logH = hfld_local.DOF(3);

    SANS_CHECK_CLOSE(logH(0,0), logH_true(0,0), small_tol, close_tol);
  }

  {
    MatrixSym ML = {1.0 / pow(0.2, 2.0)};
    MatrixSym MR = {1.0 / pow(0.2, 2.0)};
    std::vector<MatrixSym> metric_list = {ML, MR};

    //Average metric around node
    DLA::EigenSystemPair<D,Real> avgM = Metric::computeLogEuclideanAvg(metric_list);
    MatrixSym logH_true = log(pow(avgM, -0.5));

    MatrixSym logH = hfld_local.DOF(4);

    SANS_CHECK_CLOSE(logH(0,0), logH_true(0,0), small_tol, close_tol);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Area_local_mesh_edge_split_test )
{
  static const int D = PhysD2::D;

  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-10;

  // grid
  XField2D_Box_Triangle_X1 xfld(3, 3, 0, 3, 0, 3);

  typedef DLA::MatrixSymS<D,Real> MatrixSym;

  int orderQmax = 3;

  for (int orderQ = 1; orderQ <= orderQmax; orderQ++)
  {
    XField<PhysD2, TopoD2> xfldQ(xfld, orderQ);

    //Build cell to trace connectivity structure
    XField_CellToTrace<PhysD2,TopoD2> connectivity(xfldQ);

    const int cellgroup = 0;
    const int cellelem = 8;

    //Extract the local mesh for the central triangle
    XField_LocalPatchConstructor<PhysD2, Triangle> xfld_construct(comm_local, connectivity, cellgroup, cellelem);

    int split_edge_index = 0;
    XField_LocalPatch<PhysD2, Triangle> xfld_split_local(xfld_construct,
                                                         ElementSplitType::Edge,
                                                         split_edge_index);

    GenHField_CG<PhysD2,TopoD2> hfld( xfldQ );

    GenHField_CG<PhysD2,TopoD2> hfld_local( xfld_split_local, hfld );

    MatrixSym M0 = {{1},{0.5,1}}; //elemental metric of unsplit triangles

    MatrixSym M00 = {{1},{0,3}}; //elemental metric of split element in local cellgroup 0, elem 0
    MatrixSym M01 = {{3},{0,1}}; //elemental metric of split element in local cellgroup 0, elem 1

    BOOST_REQUIRE_EQUAL(hfld_local.nDOF(), 7);

    {
      std::vector<MatrixSym> metric_list = {M0, M0, M0, M0, M00, M01, M0};

      //Average metric around node
      DLA::EigenSystemPair<D,Real> avgM = Metric::computeLogEuclideanAvg(metric_list);
      MatrixSym logH_true = log(pow(avgM, -0.5));

      MatrixSym logH = hfld_local.DOF(0);

      SANS_CHECK_CLOSE(logH_true(0,0), logH(0,0), small_tol, close_tol);
      SANS_CHECK_CLOSE(logH_true(1,0), logH(1,0), small_tol, close_tol);
      SANS_CHECK_CLOSE(logH_true(1,1), logH(1,1), small_tol, close_tol);
    }

    {
      std::vector<MatrixSym> metric_list = {M0, M0, M0, M0, M01, M00};

      //Average metric around node
      DLA::EigenSystemPair<D,Real> avgM = Metric::computeLogEuclideanAvg(metric_list);
      MatrixSym logH_true = log(pow(avgM, -0.5));

      MatrixSym logH = hfld_local.DOF(1);

      SANS_CHECK_CLOSE(logH_true(0,0), logH(0,0), small_tol, close_tol);
      SANS_CHECK_CLOSE(logH_true(1,0), logH(1,0), small_tol, close_tol);
      SANS_CHECK_CLOSE(logH_true(1,1), logH(1,1), small_tol, close_tol);
    }

    {
      std::vector<MatrixSym> metric_list = {M0, M0, M0, M0, M01, M00};

      //Average metric around node
      DLA::EigenSystemPair<D,Real> avgM = Metric::computeLogEuclideanAvg(metric_list);
      MatrixSym logH_true = log(pow(avgM, -0.5));

      MatrixSym logH = hfld_local.DOF(2);

      SANS_CHECK_CLOSE(logH_true(0,0), logH(0,0), small_tol, close_tol);
      SANS_CHECK_CLOSE(logH_true(1,0), logH(1,0), small_tol, close_tol);
      SANS_CHECK_CLOSE(logH_true(1,1), logH(1,1), small_tol, close_tol);
    }

    {
      std::vector<MatrixSym> metric_list = {M0, M0, M0, M0, M0, M00, M01};

      //Average metric around node
      DLA::EigenSystemPair<D,Real> avgM = Metric::computeLogEuclideanAvg(metric_list);
      MatrixSym logH_true = log(pow(avgM, -0.5));

      MatrixSym logH = hfld_local.DOF(3);

      SANS_CHECK_CLOSE(logH_true(0,0), logH(0,0), small_tol, close_tol);
      SANS_CHECK_CLOSE(logH_true(1,0), logH(1,0), small_tol, close_tol);
      SANS_CHECK_CLOSE(logH_true(1,1), logH(1,1), small_tol, close_tol);
    }

    {
      std::vector<MatrixSym> metric_list = {M0, M0, M0};

      //Average metric around node
      DLA::EigenSystemPair<D,Real> avgM = Metric::computeLogEuclideanAvg(metric_list);
      MatrixSym logH_true = log(pow(avgM, -0.5));

      MatrixSym logH = hfld_local.DOF(4);

      SANS_CHECK_CLOSE(logH_true(0,0), logH(0,0), small_tol, close_tol);
      SANS_CHECK_CLOSE(logH_true(1,0), logH(1,0), small_tol, close_tol);
      SANS_CHECK_CLOSE(logH_true(1,1), logH(1,1), small_tol, close_tol);
    }

    {
      std::vector<MatrixSym> metric_list = {M0, M0, M0};

      //Average metric around node
      DLA::EigenSystemPair<D,Real> avgM = Metric::computeLogEuclideanAvg(metric_list);
      MatrixSym logH_true = log(pow(avgM, -0.5));

      MatrixSym logH = hfld_local.DOF(5);

      SANS_CHECK_CLOSE(logH_true(0,0), logH(0,0), small_tol, close_tol);
      SANS_CHECK_CLOSE(logH_true(1,0), logH(1,0), small_tol, close_tol);
      SANS_CHECK_CLOSE(logH_true(1,1), logH(1,1), small_tol, close_tol);
    }

    {
      std::vector<MatrixSym> metric_list = {M00, M01, M00, M01};

      //Average metric around node
      DLA::EigenSystemPair<D,Real> avgM = Metric::computeLogEuclideanAvg(metric_list);
      MatrixSym logH_true = log(pow(avgM, -0.5));

      MatrixSym logH = hfld_local.DOF(6);

      SANS_CHECK_CLOSE(logH_true(0,0), logH(0,0), small_tol, close_tol);
      SANS_CHECK_CLOSE(logH_true(1,0), logH(1,0), small_tol, close_tol);
      SANS_CHECK_CLOSE(logH_true(1,1), logH(1,1), small_tol, close_tol);
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( Area_local_mesh_isotropic_split_test )
{
  mpi::communicator world;
  mpi::communicator comm_local = world.split(world.rank());

  // grid
  XField2D_Box_Triangle_X1 xfld(3, 3, 0, 3, 0, 3);

#ifdef SANS_MPI //TODO: HACK that must be fixed
  int orderQmax = 1;
#else
  int orderQmax = 3;
#endif

  for (int orderQ = 1; orderQ <= orderQmax; orderQ++)
  {
    XField<PhysD2, TopoD2> xfldQ(xfld, orderQ);

    //Build cell to trace connectivity structure
    XField_CellToTrace<PhysD2,TopoD2> connectivity(xfldQ);

    const int cellgroup = 0;
    const int cellelem = 8;

    //Extract the local mesh for the central triangle
    XField_LocalPatchConstructor<PhysD2, Triangle> xfld_local(comm_local, connectivity, cellgroup, cellelem);

    typedef XField_LocalPatch<PhysD2, Triangle> XField_Local;

    int split_edge_index = -1;
    BOOST_CHECK_THROW(XField_Local xfld_split_local(xfld_local,
                                                         ElementSplitType::Isotropic,
                                                         split_edge_index);, AssertionException );

#if 0
    GenHField_CG<PhysD2,TopoD2> hfld( xfldQ );

    GenHField_CG<PhysD2,TopoD2> hfld_local( xfld_split_local, hfld );

    static const int D = PhysD2::D;
    typedef DLA::MatrixSymS<D,Real> MatrixSym;

    MatrixSym M0 = {{1},{0.5,1}}; //elemental metric of unsplit triangles

    MatrixSym M00 = {{4},{2,4}}; //elemental metric of split elements in local cellgroup 0

    MatrixSym M10 = {{3},{0,1}}; //elemental metric of split element in local cellgroup 1, elem 0
    MatrixSym M11 = {{1},{0,3}}; //elemental metric of split element in local cellgroup 1, elem 1

    MatrixSym M12 = {{1},{1,4}}; //elemental metric of split element in local cellgroup 1, elem 2
    MatrixSym M13 = {{3},{3,4}}; //elemental metric of split element in local cellgroup 1, elem 3

    MatrixSym M14 = {{4},{3,3}}; //elemental metric of split element in local cellgroup 1, elem 4
    MatrixSym M15 = {{4},{1,1}}; //elemental metric of split element in local cellgroup 1, elem 5

    BOOST_CHECK_EQUAL(hfld_local.nDOF(), 9);

    const Real small_tol = 1e-11;
    const Real close_tol = 1e-9;

    {
      std::vector<MatrixSym> metric_list = {M00, M13, M0, M0, M0, M14};

      //Average metric around node
      DLA::EigenSystemPair<D,Real> avgM = Metric::computeLogEuclideanAvg(metric_list);
      MatrixSym logH_true = log(pow(avgM, -0.5));

      MatrixSym logH = hfld_local.DOF(0);

      SANS_CHECK_CLOSE(logH(0,0), logH_true(0,0), small_tol, close_tol);
      SANS_CHECK_CLOSE(logH(1,0), logH_true(1,0), small_tol, close_tol);
      SANS_CHECK_CLOSE(logH(1,1), logH_true(1,1), small_tol, close_tol);
    }

    {
      std::vector<MatrixSym> metric_list = {M00, M15, M0, M0, M0, M10};

      //Average metric around node
      DLA::EigenSystemPair<D,Real> avgM = Metric::computeLogEuclideanAvg(metric_list);
      MatrixSym logH_true = log(pow(avgM, -0.5));

      MatrixSym logH = hfld_local.DOF(1);

      SANS_CHECK_CLOSE(logH(0,0), logH_true(0,0), small_tol, close_tol);
      SANS_CHECK_CLOSE(logH(1,0), logH_true(1,0), small_tol, close_tol);
      SANS_CHECK_CLOSE(logH(1,1), logH_true(1,1), small_tol, close_tol);
    }

    {
      std::vector<MatrixSym> metric_list = {M00, M11, M0, M0, M0, M12};

      //Average metric around node
      DLA::EigenSystemPair<D,Real> avgM = Metric::computeLogEuclideanAvg(metric_list);
      MatrixSym logH_true = log(pow(avgM, -0.5));

      MatrixSym logH = hfld_local.DOF(2);

      SANS_CHECK_CLOSE(logH(0,0), logH_true(0,0), small_tol, close_tol);
      SANS_CHECK_CLOSE(logH(1,0), logH_true(1,0), small_tol, close_tol);
      SANS_CHECK_CLOSE(logH(1,1), logH_true(1,1), small_tol, close_tol);
    }

    {
      std::vector<MatrixSym> metric_list = {M00, M00, M00, M10, M11};

      //Average metric around node
      DLA::EigenSystemPair<D,Real> avgM = Metric::computeLogEuclideanAvg(metric_list);
      MatrixSym logH_true = log(pow(avgM, -0.5));

      MatrixSym logH = hfld_local.DOF(3);

      SANS_CHECK_CLOSE(logH(0,0), logH_true(0,0), small_tol, close_tol);
      SANS_CHECK_CLOSE(logH(1,0), logH_true(1,0), small_tol, close_tol);
      SANS_CHECK_CLOSE(logH(1,1), logH_true(1,1), small_tol, close_tol);
    }

    {
      std::vector<MatrixSym> metric_list = {M00, M00, M00, M12, M13};

      //Average metric around node
      DLA::EigenSystemPair<D,Real> avgM = Metric::computeLogEuclideanAvg(metric_list);
      MatrixSym logH_true = log(pow(avgM, -0.5));

      MatrixSym logH = hfld_local.DOF(4);

      SANS_CHECK_CLOSE(logH(0,0), logH_true(0,0), small_tol, close_tol);
      SANS_CHECK_CLOSE(logH(1,0), logH_true(1,0), small_tol, close_tol);
      SANS_CHECK_CLOSE(logH(1,1), logH_true(1,1), small_tol, close_tol);
    }

    {
      std::vector<MatrixSym> metric_list = {M00, M00, M00, M14, M15};

      //Average metric around node
      DLA::EigenSystemPair<D,Real> avgM = Metric::computeLogEuclideanAvg(metric_list);
      MatrixSym logH_true = log(pow(avgM, -0.5));

      MatrixSym logH = hfld_local.DOF(5);

      SANS_CHECK_CLOSE(logH(0,0), logH_true(0,0), small_tol, close_tol);
      SANS_CHECK_CLOSE(logH(1,0), logH_true(1,0), small_tol, close_tol);
      SANS_CHECK_CLOSE(logH(1,1), logH_true(1,1), small_tol, close_tol);
    }

    {
      std::vector<MatrixSym> metric_list = {M0, M0, M0, M0, M0, M11, M10};

      //Average metric around node
      DLA::EigenSystemPair<D,Real> avgM = Metric::computeLogEuclideanAvg(metric_list);
      MatrixSym logH_true = log(pow(avgM, -0.5));

      MatrixSym logH = hfld_local.DOF(6);

      SANS_CHECK_CLOSE(logH(0,0), logH_true(0,0), small_tol, close_tol);
      SANS_CHECK_CLOSE(logH(1,0), logH_true(1,0), small_tol, close_tol);
      SANS_CHECK_CLOSE(logH(1,1), logH_true(1,1), small_tol, close_tol);
    }

    {
      std::vector<MatrixSym> metric_list = {M0, M13, M12, M0};

      //Average metric around node
      DLA::EigenSystemPair<D,Real> avgM = Metric::computeLogEuclideanAvg(metric_list);
      MatrixSym logH_true = log(pow(avgM, -0.5));

      MatrixSym logH = hfld_local.DOF(7);

      SANS_CHECK_CLOSE(logH(0,0), logH_true(0,0), small_tol, close_tol);
      SANS_CHECK_CLOSE(logH(1,0), logH_true(1,0), small_tol, close_tol);
      SANS_CHECK_CLOSE(logH(1,1), logH_true(1,1), small_tol, close_tol);
    }

    {
      std::vector<MatrixSym> metric_list = {M0, M15, M14, M0};

      //Average metric around node
      DLA::EigenSystemPair<D,Real> avgM = Metric::computeLogEuclideanAvg(metric_list);
      MatrixSym logH_true = log(pow(avgM, -0.5));

      MatrixSym logH = hfld_local.DOF(8);

      SANS_CHECK_CLOSE(logH(0,0), logH_true(0,0), small_tol, close_tol);
      SANS_CHECK_CLOSE(logH(1,0), logH_true(1,0), small_tol, close_tol);
      SANS_CHECK_CLOSE(logH(1,1), logH_true(1,1), small_tol, close_tol);
    }
#endif
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
