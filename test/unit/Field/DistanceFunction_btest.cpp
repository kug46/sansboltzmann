// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// DistanceFunction_btest
// testing of distance function calculations

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/minmax.h"

#include "Quadrature/Quadrature.h"

#include "Field/FieldLine_CG_Cell.h"
#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldVolume_CG_Cell.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"

#include "Field/DistanceFunction/DistanceFunction.h"
//#include "Field/output_Tecplot.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"
#include "unit/UnitGrids/XField2D_Box_Quad_X1.h"
#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"
//#include "unit/UnitGrids/XField3D_Box_Hex_X1.h"

#include "unit/UnitGrids/XField3D_1Tet_X1_1Group.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct
namespace SANS
{
}

//############################################################################//
BOOST_AUTO_TEST_SUITE( DistanceFunction_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DistanceFunction_1D_Line_X1P1 )
{
  typedef PhysD1 PhysDim;
  typedef TopoD1 TopoDim;
  typedef Line Topology;
  typedef DLA::VectorS<1,Real> VectorX;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  // grids
  XField1D xfld(4);

  // solution
  int qorder = 1;
  Field_CG_Cell<PhysDim, TopoDim, Real> distfld(xfld, qorder, BasisFunctionCategory_Lagrange);
  Field_DG_Cell<PhysDim, TopoDim, VectorX> gdistfld(xfld, qorder, BasisFunctionCategory_Lagrange);

  std::vector<int> btraceGroups = {0, 1};
  DistanceFunction(distfld, btraceGroups, false);

  const XField<PhysDim, TopoDim     >::template FieldCellGroupType<Topology>& xfldCell = xfld.getCellGroup<Topology>(0);
  const Field<PhysDim, TopoDim, Real>::template FieldCellGroupType<Topology>& distfldCell = distfld.getCellGroup<Topology>(0);
//  const Field<PhysDim, TopoDim, VectorX>::template FieldCellGroupType<Topology>& gdistfldCell = gdistfld.getCellGroup<Topology>(0);

  typedef XField<PhysDim, TopoDim     >::template FieldCellGroupType<Topology>::template ElementType<> ElementXFieldClass;
  typedef Field<PhysDim, TopoDim, Real>::template FieldCellGroupType<Topology>::template ElementType<> ElementQFieldClass;
//  typedef Field<PhysDim, TopoDim, VectorX>::template FieldCellGroupType<Topology>::template ElementType<> ElementGFieldClass;

  typedef typename ElementXFieldClass::RefCoordType RefCoordType;

  // element field variables
  ElementXFieldClass xfldElem( xfldCell.basis() );
  ElementQFieldClass distfldElem( distfldCell.basis() );
//  ElementGFieldClass gdistfldElem( gdistfldCell.basis() );

  RefCoordType Ref;  // reference-element coordinates
  VectorX X;

  Quadrature<TopoDim, Topology> quadrature( 5 );
  const int nquad = quadrature.nQuadrature();

  // loop over elements within group
  const int nelem = xfldCell.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    // copy global grid/solution DOFs to element
    xfldCell.getElement( xfldElem, elem );
    distfldCell.getElement( distfldElem, elem );
//    gdistfldCell.getElement( gdistfldElem, elem );

    // loop over quadrature points
    for (int iquad = 0; iquad < nquad; iquad++)
    {
      quadrature.coordinates( iquad, Ref );

      // physical coordinates
      xfldElem.coordinates( Ref, X );

      Real distTrue = MIN(X[0], 1-X[0]);
      Real dist = distfldElem.eval(Ref);

      SANS_CHECK_CLOSE( distTrue, dist, small_tol, close_tol );

//      Real graddistTrue = 1;
//      if (X[0] > 0.5)
//        graddistTrue = -1;
//
//      VectorX graddist = gdistfldElem.eval(Ref);
//
//      SANS_CHECK_CLOSE( graddistTrue, graddist[0], small_tol, close_tol);


    }
  }


}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DistanceFunction_2D_Triangle_X1P1 )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;
  typedef Triangle Topology;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  mpi::communicator world;

  // grid sizes must be all the same so the discontinuity of distance function
  // can be captured exactly
  XField2D_Box_Triangle_Lagrange_X1 xfld(world,5,5);

  // solution
  int qorder = 1;
  Field_CG_Cell<PhysDim, TopoDim, Real> distfld(xfld, qorder, BasisFunctionCategory_Lagrange);

  std::vector<int> btraceGroups = {XField2D_Box_Triangle_Lagrange_X1::iBottom,
                                   XField2D_Box_Triangle_Lagrange_X1::iRight};
  DistanceFunction(distfld, btraceGroups, false);

  const XField<PhysDim, TopoDim     >::template FieldCellGroupType<Topology>& xfldCell = xfld.getCellGroup<Topology>(0);
  const Field<PhysDim, TopoDim, Real>::template FieldCellGroupType<Topology>& distfldCell = distfld.getCellGroup<Topology>(0);

  typedef XField<PhysDim, TopoDim     >::template FieldCellGroupType<Topology>::template ElementType<> ElementXFieldClass;
  typedef Field<PhysDim, TopoDim, Real>::template FieldCellGroupType<Topology>::template ElementType<> ElementQFieldClass;
  typedef typename ElementXFieldClass::RefCoordType RefCoordType;
  typedef typename ElementXFieldClass::VectorX VectorX;

  // element field variables
  ElementXFieldClass xfldElem( xfldCell.basis() );
  ElementQFieldClass distfldElem( distfldCell.basis() );

  RefCoordType Ref;  // reference-element coordinates
  VectorX X;

  Quadrature<TopoDim, Topology> quadrature( 5 );
  const int nquad = quadrature.nQuadrature();

  // loop over elements within group
  const int nelem = xfldCell.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    // copy global grid/solution DOFs to element
    xfldCell.getElement( xfldElem, elem );
    distfldCell.getElement( distfldElem, elem );

    // loop over quadrature points
    for (int iquad = 0; iquad < nquad; iquad++)
    {
      quadrature.coordinates( iquad, Ref );

      // physical coordinates
      xfldElem.coordinates( Ref, X );

      Real dist_bottom = X[1];
      Real dist_right = 1 - X[0];
      Real distTrue = MIN(dist_bottom, dist_right);

      Real dist = distfldElem.eval(Ref);
      SANS_CHECK_CLOSE( distTrue, dist, small_tol, close_tol );
    }
  }



}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DistanceFunction_2D_Triangle_X1P2 )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;
  typedef Triangle Topology;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  mpi::communicator world;

  // grid sizes must be all the same so the discontinuity of distance function
  // can be captured exactly
  XField2D_Box_Triangle_Lagrange_X1 xfld(world,5,5);

  // solution
  int qorder = 2;
  Field_DG_Cell<PhysDim, TopoDim, Real> distfld(xfld, qorder, BasisFunctionCategory_Lagrange);

  std::vector<int> btraceGroups = {XField2D_Box_Triangle_Lagrange_X1::iBottom,
                                   XField2D_Box_Triangle_Lagrange_X1::iRight};
  DistanceFunction(distfld, btraceGroups, false);

  const XField<PhysDim, TopoDim     >::template FieldCellGroupType<Topology>& xfldCell = xfld.getCellGroup<Topology>(0);
  const Field<PhysDim, TopoDim, Real>::template FieldCellGroupType<Topology>& distfldCell = distfld.getCellGroup<Topology>(0);

  typedef XField<PhysDim, TopoDim     >::template FieldCellGroupType<Topology>::template ElementType<> ElementXFieldClass;
  typedef Field<PhysDim, TopoDim, Real>::template FieldCellGroupType<Topology>::template ElementType<> ElementQFieldClass;
  typedef typename ElementXFieldClass::RefCoordType RefCoordType;
  typedef typename ElementXFieldClass::VectorX VectorX;

  // element field variables
  ElementXFieldClass xfldElem( xfldCell.basis() );
  ElementQFieldClass distfldElem( distfldCell.basis() );

  RefCoordType Ref;  // reference-element coordinates
  VectorX X;

  Quadrature<TopoDim, Topology> quadrature( 5 );
  const int nquad = quadrature.nQuadrature();

  // loop over elements within group
  const int nelem = xfldCell.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    // copy global grid/solution DOFs to element
    xfldCell.getElement( xfldElem, elem );
    distfldCell.getElement( distfldElem, elem );

    // loop over quadrature points
    for (int iquad = 0; iquad < nquad; iquad++)
    {
      quadrature.coordinates( iquad, Ref );

      // physical coordinates
      xfldElem.coordinates( Ref, X );

      Real dist_bottom = X[1];
      Real dist_right = 1 - X[0];
      Real distTrue = MIN(dist_bottom, dist_right);

      Real dist = distfldElem.eval(Ref);
      SANS_CHECK_CLOSE( distTrue, dist, small_tol, close_tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DistanceFunction_2D_Quad_X1P1 )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;
  typedef Quad Topology;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  // grids
  XField2D_Box_Quad_X1 xfld(4,4);

  // solution
  int qorder = 1;
  Field_CG_Cell<PhysDim, TopoDim, Real> distfld(xfld, qorder, BasisFunctionCategory_Lagrange);

  std::vector<int> btraceGroups = {0,2};
  DistanceFunction(distfld, btraceGroups, false);

  const XField<PhysDim, TopoDim     >::template FieldCellGroupType<Topology>& xfldCell = xfld.getCellGroup<Topology>(0);
  const Field<PhysDim, TopoDim, Real>::template FieldCellGroupType<Topology>& distfldCell = distfld.getCellGroup<Topology>(0);

  typedef XField<PhysDim, TopoDim     >::template FieldCellGroupType<Topology>::template ElementType<> ElementXFieldClass;
  typedef Field<PhysDim, TopoDim, Real>::template FieldCellGroupType<Topology>::template ElementType<> ElementQFieldClass;
  typedef typename ElementXFieldClass::RefCoordType RefCoordType;
  typedef typename ElementXFieldClass::VectorX VectorX;

  // element field variables
  ElementXFieldClass xfldElem( xfldCell.basis() );
  ElementQFieldClass distfldElem( distfldCell.basis() );

  RefCoordType Ref;  // reference-element coordinates
  VectorX X;

  Quadrature<TopoDim, Topology> quadrature( 5 );
  const int nquad = quadrature.nQuadrature();

  // loop over elements within group
  const int nelem = xfldCell.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    // copy global grid/solution DOFs to element
    xfldCell.getElement( xfldElem, elem );
    distfldCell.getElement( distfldElem, elem );

    // loop over quadrature points
    for (int iquad = 0; iquad < nquad; iquad++)
    {
      quadrature.coordinates( iquad, Ref );

      // physical coordinates
      xfldElem.coordinates( Ref, X );

      Real dist_bottom = X[1];
      Real dist_top = 1 - X[1];
      Real distTrue = MIN(dist_bottom, dist_top);;

      Real dist = distfldElem.eval(Ref);
      SANS_CHECK_CLOSE( distTrue, dist, small_tol, close_tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DistanceFunction_3D_1Tet_X1P1 )
{
  typedef PhysD3 PhysDim;
  typedef TopoD3 TopoDim;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-11;

  // Single tet, modified to check that the distance function works in all scenarios
  XField3D_1Tet_X1_1Group xfld;

  // solution
  int qorder = 1;
  Field_CG_Cell<PhysDim, TopoDim, Real> distfld(xfld, qorder, BasisFunctionCategory_Lagrange);

  std::vector<int> btraceGroups = {3};
  Real distTrue = -1;

  // point is inside the triangle
  xfld.DOF(3) = {1./3., 1/3, 1};

  DistanceFunction(distfld, btraceGroups, false);

  distTrue = 1;
  SANS_CHECK_CLOSE(        0, distfld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE(        0, distfld.DOF(1), small_tol, close_tol );
  SANS_CHECK_CLOSE(        0, distfld.DOF(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( distTrue, distfld.DOF(3), small_tol, close_tol );


  // point is outside edge 0
  xfld.DOF(3) = {1, 1, 1};

  DistanceFunction(distfld, btraceGroups, false);

  distTrue = sqrt( pow(1-0.5,2) + pow(1-0.5,2) + 1.);
  SANS_CHECK_CLOSE(        0, distfld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE(        0, distfld.DOF(1), small_tol, close_tol );
  SANS_CHECK_CLOSE(        0, distfld.DOF(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( distTrue, distfld.DOF(3), small_tol, close_tol );


  // point is outside edge 1
  xfld.DOF(3) = {-0.5, 0.5, 1};

  DistanceFunction(distfld, btraceGroups, false);

  distTrue = sqrt( pow(1-0.5,2) + 1.);
  SANS_CHECK_CLOSE(        0, distfld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE(        0, distfld.DOF(1), small_tol, close_tol );
  SANS_CHECK_CLOSE(        0, distfld.DOF(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( distTrue, distfld.DOF(3), small_tol, close_tol );


  // point is outside edge 2
  xfld.DOF(3) = {0.5, -0.5, 1};

  DistanceFunction(distfld, btraceGroups, false);

  distTrue = sqrt( pow(1-0.5,2) + 1.);
  SANS_CHECK_CLOSE(        0, distfld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE(        0, distfld.DOF(1), small_tol, close_tol );
  SANS_CHECK_CLOSE(        0, distfld.DOF(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( distTrue, distfld.DOF(3), small_tol, close_tol );



  // point is outside node 0
  xfld.DOF(3) = {-1, 0, 1};

  DistanceFunction(distfld, btraceGroups, false);

  distTrue = sqrt( 1. + 1.);
  SANS_CHECK_CLOSE(        0, distfld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE(        0, distfld.DOF(1), small_tol, close_tol );
  SANS_CHECK_CLOSE(        0, distfld.DOF(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( distTrue, distfld.DOF(3), small_tol, close_tol );

  // point is outside node 0
  xfld.DOF(3) = {0, -1, 1};

  DistanceFunction(distfld, btraceGroups, false);

  distTrue = sqrt( 1. + 1.);
  SANS_CHECK_CLOSE(        0, distfld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE(        0, distfld.DOF(1), small_tol, close_tol );
  SANS_CHECK_CLOSE(        0, distfld.DOF(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( distTrue, distfld.DOF(3), small_tol, close_tol );

  // point is outside node 0
  xfld.DOF(3) = {-1, -1, 1};

  DistanceFunction(distfld, btraceGroups, false);

  distTrue = sqrt( 1. + 1. + 1.);
  SANS_CHECK_CLOSE(        0, distfld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE(        0, distfld.DOF(1), small_tol, close_tol );
  SANS_CHECK_CLOSE(        0, distfld.DOF(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( distTrue, distfld.DOF(3), small_tol, close_tol );


  // point is outside node 1
  xfld.DOF(3) = {2, 0, 1};

  DistanceFunction(distfld, btraceGroups, false);

  distTrue = sqrt( 1. + 1.);
  SANS_CHECK_CLOSE(        0, distfld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE(        0, distfld.DOF(1), small_tol, close_tol );
  SANS_CHECK_CLOSE(        0, distfld.DOF(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( distTrue, distfld.DOF(3), small_tol, close_tol );

  // point is outside node 1
  xfld.DOF(3) = {1, -1, 1};

  DistanceFunction(distfld, btraceGroups, false);

  distTrue = sqrt( 1. + 1.);
  SANS_CHECK_CLOSE(        0, distfld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE(        0, distfld.DOF(1), small_tol, close_tol );
  SANS_CHECK_CLOSE(        0, distfld.DOF(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( distTrue, distfld.DOF(3), small_tol, close_tol );

  // point is outside node 1
  xfld.DOF(3) = {2, -1, 1};

  DistanceFunction(distfld, btraceGroups, false);

  distTrue = sqrt( 1. + 1. + 1.);
  SANS_CHECK_CLOSE(        0, distfld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE(        0, distfld.DOF(1), small_tol, close_tol );
  SANS_CHECK_CLOSE(        0, distfld.DOF(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( distTrue, distfld.DOF(3), small_tol, close_tol );


  // point is outside node 2
  xfld.DOF(3) = {0, 2, 1};

  DistanceFunction(distfld, btraceGroups, false);

  distTrue = sqrt( 1. + 1.);
  SANS_CHECK_CLOSE(        0, distfld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE(        0, distfld.DOF(1), small_tol, close_tol );
  SANS_CHECK_CLOSE(        0, distfld.DOF(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( distTrue, distfld.DOF(3), small_tol, close_tol );

  // point is outside node 2
  xfld.DOF(3) = {-1, 1, 1};

  DistanceFunction(distfld, btraceGroups, false);

  distTrue = sqrt( 1. + 1.);
  SANS_CHECK_CLOSE(        0, distfld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE(        0, distfld.DOF(1), small_tol, close_tol );
  SANS_CHECK_CLOSE(        0, distfld.DOF(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( distTrue, distfld.DOF(3), small_tol, close_tol );

  // point is outside node 2
  xfld.DOF(3) = {-1, 2, 1};

  DistanceFunction(distfld, btraceGroups, false);

  distTrue = sqrt( 1. + 1. + 1.);
  SANS_CHECK_CLOSE(        0, distfld.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE(        0, distfld.DOF(1), small_tol, close_tol );
  SANS_CHECK_CLOSE(        0, distfld.DOF(2), small_tol, close_tol );
  SANS_CHECK_CLOSE( distTrue, distfld.DOF(3), small_tol, close_tol );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DistanceFunction_3D_Tet_X1P1 )
{
  typedef PhysD3 PhysDim;
  typedef TopoD3 TopoDim;
  typedef Tet Topology;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-11;

  mpi::communicator world;

  // grid sizes must be all the same so the discontinuity of distance function
  // can be captured exactly
  XField3D_Box_Tet_X1 xfld(world,3,3,3);

  // solution
  int qorder = 1;
  Field_CG_Cell<PhysDim, TopoDim, Real> distfld(xfld, qorder, BasisFunctionCategory_Lagrange);

  std::vector<int> btraceGroups = {XField3D_Box_Tet_X1::iXmin,
                                   XField3D_Box_Tet_X1::iZmax};
  DistanceFunction(distfld, btraceGroups, false);

  //output_Tecplot(distfld, "tmp/distbox_rank" + std::to_string(world.rank()) + ".dat", {}, true);
  //output_Tecplot(distfld, "tmp/distbox.dat");

  const XField<PhysDim, TopoDim     >::template FieldCellGroupType<Topology>& xfldCell = xfld.getCellGroup<Topology>(0);
  const Field<PhysDim, TopoDim, Real>::template FieldCellGroupType<Topology>& distfldCell = distfld.getCellGroup<Topology>(0);

  typedef XField<PhysDim, TopoDim     >::template FieldCellGroupType<Topology>::template ElementType<> ElementXFieldClass;
  typedef Field<PhysDim, TopoDim, Real>::template FieldCellGroupType<Topology>::template ElementType<> ElementQFieldClass;
  typedef typename ElementXFieldClass::RefCoordType RefCoordType;
  typedef typename ElementXFieldClass::VectorX VectorX;

  // element field variables
  ElementXFieldClass xfldElem( xfldCell.basis() );
  ElementQFieldClass distfldElem( distfldCell.basis() );

  RefCoordType Ref;  // reference-element coordinates
  VectorX X;

  Quadrature<TopoDim, Topology> quadrature( 5 );
  const int nquad = quadrature.nQuadrature();

  // loop over elements within group
  const int nelem = xfldCell.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    // copy global grid/solution DOFs to element
    xfldCell.getElement( xfldElem, elem );
    distfldCell.getElement( distfldElem, elem );

    // loop over quadrature points
    for (int iquad = 0; iquad < nquad; iquad++)
    {
      quadrature.coordinates( iquad, Ref );

      // physical coordinates
      xfldElem.coordinates( Ref, X );

      Real dist_Xmin = X[0];
      Real dist_Zmax = 1 - X[2];
      Real distTrue = MIN(dist_Xmin, dist_Zmax);

      Real dist = distfldElem.eval(Ref);
      SANS_CHECK_CLOSE( distTrue, dist, small_tol, close_tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DistanceFunction_3D_Tet_X2P1 )
{
  typedef PhysD3 PhysDim;
  typedef TopoD3 TopoDim;
  typedef Tet Topology;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-11;

  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm = world.split(world.rank());

  // grid sizes must be all the same so the discontinuity of distance function
  // can be captured exactly
  //XField3D_Box_Tet_X1 xfld_linear(world,3,3,3);
  XField3D_Box_Tet_X1 xfld_linear(comm,3,3,3);

  // generate a 'curved' grid
  XField<PhysD3,TopoD3> xfld(xfld_linear, 2);

  // solution
  int qorder = 1;
  Field_CG_Cell<PhysDim, TopoDim, Real> distfld(xfld, qorder, BasisFunctionCategory_Lagrange);

  std::vector<int> btraceGroups = {XField3D_Box_Tet_X1::iXmin,
                                   XField3D_Box_Tet_X1::iZmax};
  DistanceFunction(distfld, btraceGroups, false);

  const XField<PhysDim, TopoDim     >::template FieldCellGroupType<Topology>& xfldCell = xfld.getCellGroup<Topology>(0);
  const Field<PhysDim, TopoDim, Real>::template FieldCellGroupType<Topology>& distfldCell = distfld.getCellGroup<Topology>(0);

  typedef XField<PhysDim, TopoDim     >::template FieldCellGroupType<Topology>::template ElementType<> ElementXFieldClass;
  typedef Field<PhysDim, TopoDim, Real>::template FieldCellGroupType<Topology>::template ElementType<> ElementQFieldClass;
  typedef typename ElementXFieldClass::RefCoordType RefCoordType;
  typedef typename ElementXFieldClass::VectorX VectorX;

  // element field variables
  ElementXFieldClass xfldElem( xfldCell.basis() );
  ElementQFieldClass distfldElem( distfldCell.basis() );

  RefCoordType Ref;  // reference-element coordinates
  VectorX X;

  Quadrature<TopoDim, Topology> quadrature( 5 );
  const int nquad = quadrature.nQuadrature();

  // loop over elements within group
  const int nelem = xfldCell.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    // copy global grid/solution DOFs to element
    xfldCell.getElement( xfldElem, elem );
    distfldCell.getElement( distfldElem, elem );

    // loop over quadrature points
    for (int iquad = 0; iquad < nquad; iquad++)
    {
      quadrature.coordinates( iquad, Ref );

      // physical coordinates
      xfldElem.coordinates( Ref, X );

      Real dist_Xmin = X[0];
      Real dist_Zmax = 1 - X[2];
      Real distTrue = MIN(dist_Xmin, dist_Zmax);

      Real dist = distfldElem.eval(Ref);
      SANS_CHECK_CLOSE( distTrue, dist, small_tol, close_tol );
    }
  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
