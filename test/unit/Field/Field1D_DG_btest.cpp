// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// Field1D_DG_btest
// testing of Field_DG_1D* classes
//

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;


#include "unit/UnitGrids/XField1D_1Line_X1_1Group.h"
#include "unit/UnitGrids/XField1D_2Line_X1_1Group.h"
#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField2D_Line_X1_2Group_AirfoilWithWake.h"

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/TraceToCellRefCoord.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_HubTrace.h"
#include "Field/FieldLine_DG_InteriorTrace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"

using namespace std;
using namespace SANS;

namespace SANS
{
//Explicitly instantiate classes to get proper coverage information
template class Field_DG_Cell< PhysD1, TopoD1, Real >;
template class Field_DG_InteriorTrace< PhysD1, TopoD1, Real >;
template class Field_DG_BoundaryTrace< PhysD1, TopoD1, Real >;
template class Field_DG_HubTrace< PhysD1, TopoD1, Real >;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( Field1D_DG_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_Cell_P1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD1, TopoD1, ArrayQ > QField1D_DG_Cell;
  typedef QField1D_DG_Cell::FieldCellGroupType<Line> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  int dofMap[2];

  XField1D_2Line_X1_1Group xfld1;

  int order = 1;
  QField1D_DG_Cell qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 4, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfld1.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld1.nHubTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldLineClass& qfldGroup1 = qfld1.getCellGroup<Line>(0);

  BOOST_CHECK_EQUAL( 2, qfldGroup1.nElem() );

  qfldGroup1.associativity(0).getGlobalMapping( dofMap, 2 );
  BOOST_CHECK_EQUAL( 0, dofMap[0] );
  BOOST_CHECK_EQUAL( 1, dofMap[1] );

  qfldGroup1.associativity(1).getGlobalMapping( dofMap, 2 );
  BOOST_CHECK_EQUAL( 2, dofMap[0] );
  BOOST_CHECK_EQUAL( 3, dofMap[1] );

  XField1D xfld2(3);

  QField1D_DG_Cell qfld2(xfld2, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 6, qfld2.nDOF() );
  BOOST_CHECK_EQUAL( 3, qfld2.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld2.nHubTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld2.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld2, &qfld2.getXField() );

  QField1D_DG_Cell qfld3(qfld2, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld2.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld2.nElem()               ,  qfld3.nElem() );
  BOOST_CHECK_EQUAL(  qfld2.nHubTraceGroups()     ,  qfld3.nHubTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nCellGroups()         ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld2.getXField()           , &qfld3.getXField() );

  const QFieldLineClass& qfldGroup2 = qfld2.getCellGroup<Line>(0);

  BOOST_CHECK_EQUAL( 3, qfldGroup2.nElem() );

  qfldGroup2.associativity(0).getGlobalMapping( dofMap, 2 );
  BOOST_CHECK_EQUAL( 0, dofMap[0] );
  BOOST_CHECK_EQUAL( 1, dofMap[1] );

  qfldGroup2.associativity(1).getGlobalMapping( dofMap, 2 );
  BOOST_CHECK_EQUAL( 2, dofMap[0] );
  BOOST_CHECK_EQUAL( 3, dofMap[1] );

  qfldGroup2.associativity(2).getGlobalMapping( dofMap, 2 );
  BOOST_CHECK_EQUAL( 4, dofMap[0] );
  BOOST_CHECK_EQUAL( 5, dofMap[1] );


  // Test the constant assignment operator
  ElementQFieldLineClass qfldElem(qfldGroup1.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfldGroup1.nElem(); elem++)
  {
    qfldGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfldGroup1.nElem(); elem++)
  {
    qfldGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_Cell_P2 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD1, TopoD1, ArrayQ > QField1D_DG_Cell;
  typedef QField1D_DG_Cell::FieldCellGroupType<Line> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  int dofMap[3];

  XField1D_2Line_X1_1Group xfld1;

  int order = 2;
  QField1D_DG_Cell qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 3*2, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nHubTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldLineClass& qfldLineGroup1 = qfld1.getCellGroup<Line>(0);

  BOOST_CHECK_EQUAL( 2, qfldLineGroup1.nElem() );

  qfldLineGroup1.associativity(0).getGlobalMapping( dofMap, 3 );
  BOOST_CHECK_EQUAL( 0, dofMap[0] );
  BOOST_CHECK_EQUAL( 1, dofMap[1] );
  BOOST_CHECK_EQUAL( 2, dofMap[2] );

  qfldLineGroup1.associativity(1).getGlobalMapping( dofMap, 3 );
  BOOST_CHECK_EQUAL( 3, dofMap[0] );
  BOOST_CHECK_EQUAL( 4, dofMap[1] );
  BOOST_CHECK_EQUAL( 5, dofMap[2] );


  XField1D xfld2(3);

  QField1D_DG_Cell qfld2(xfld2, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 3*3, qfld2.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld2.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld2, &qfld2.getXField() );

  QField1D_DG_Cell qfld3(qfld2, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld2.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld2.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nCellGroups()      ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld2.getXField()           , &qfld3.getXField() );

  const QFieldLineClass& qfldLineGroup2 = qfld2.getCellGroup<Line>(0);

  BOOST_CHECK_EQUAL( 3, qfldLineGroup2.nElem() );

  qfldLineGroup2.associativity(0).getGlobalMapping( dofMap, 3 );
  BOOST_CHECK_EQUAL( 0, dofMap[0] );
  BOOST_CHECK_EQUAL( 1, dofMap[1] );
  BOOST_CHECK_EQUAL( 2, dofMap[2] );

  qfldLineGroup2.associativity(1).getGlobalMapping( dofMap, 3 );
  BOOST_CHECK_EQUAL( 3, dofMap[0] );
  BOOST_CHECK_EQUAL( 4, dofMap[1] );
  BOOST_CHECK_EQUAL( 5, dofMap[2] );

  qfldLineGroup2.associativity(2).getGlobalMapping( dofMap, 3 );
  BOOST_CHECK_EQUAL( 6, dofMap[0] );
  BOOST_CHECK_EQUAL( 7, dofMap[1] );
  BOOST_CHECK_EQUAL( 8, dofMap[2] );


  // Test the constant assignment operator
  ElementQFieldLineClass qfldElem(qfldLineGroup1.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfldLineGroup1.nElem(); elem++)
  {
    qfldLineGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfldLineGroup1.nElem(); elem++)
  {
    qfldLineGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_Line_P3 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD1, TopoD1, ArrayQ > QField1D_DG_Cell;
  typedef QField1D_DG_Cell::FieldCellGroupType<Line> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  int dofMap[4];

  XField1D_2Line_X1_1Group xfld1;

  int order = 3;
  QField1D_DG_Cell qfld1(xfld1, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 4*2, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nHubTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldLineClass& qfldLineGroup1 = qfld1.getCellGroup<Line>(0);
  //cout << "btest: qfldLineGroup1 =" << endl; qfldLineGroup1.dump(2);

  BOOST_CHECK_EQUAL( 2, qfldLineGroup1.nElem() );

  qfldLineGroup1.associativity(0).getGlobalMapping( dofMap, 4 );
  BOOST_CHECK_EQUAL( 0, dofMap[0] );
  BOOST_CHECK_EQUAL( 1, dofMap[1] );
  BOOST_CHECK_EQUAL( 2, dofMap[2] );
  BOOST_CHECK_EQUAL( 3, dofMap[3] );

  qfldLineGroup1.associativity(1).getGlobalMapping( dofMap, 4 );
  BOOST_CHECK_EQUAL( 4, dofMap[0] );
  BOOST_CHECK_EQUAL( 5, dofMap[1] );
  BOOST_CHECK_EQUAL( 6, dofMap[2] );
  BOOST_CHECK_EQUAL( 7, dofMap[3] );


  XField1D xfld2(3);

  QField1D_DG_Cell qfld2(xfld2, order, BasisFunctionCategory_Hierarchical);

  BOOST_CHECK_EQUAL( 4*3, qfld2.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld2.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld2, &qfld2.getXField() );

  QField1D_DG_Cell qfld3(qfld2, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld2.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld2.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld2.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld2.nCellGroups()      ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld2.getXField()           , &qfld3.getXField() );

  const QFieldLineClass& qfldLineGroup2 = qfld2.getCellGroup<Line>(0);
  //cout << "btest: qfldLineGroup2 =" << endl; qfldLineGroup2.dump(2);

  BOOST_CHECK_EQUAL( 3, qfldLineGroup2.nElem() );

  qfldLineGroup2.associativity(0).getGlobalMapping( dofMap, 4 );
  BOOST_CHECK_EQUAL( 0, dofMap[0] );
  BOOST_CHECK_EQUAL( 1, dofMap[1] );
  BOOST_CHECK_EQUAL( 2, dofMap[2] );
  BOOST_CHECK_EQUAL( 3, dofMap[3] );

  qfldLineGroup2.associativity(1).getGlobalMapping( dofMap, 4 );
  BOOST_CHECK_EQUAL( 4, dofMap[0] );
  BOOST_CHECK_EQUAL( 5, dofMap[1] );
  BOOST_CHECK_EQUAL( 6, dofMap[2] );
  BOOST_CHECK_EQUAL( 7, dofMap[3] );

  qfldLineGroup2.associativity(2).getGlobalMapping( dofMap, 4 );
  BOOST_CHECK_EQUAL(  8, dofMap[0] );
  BOOST_CHECK_EQUAL(  9, dofMap[1] );
  BOOST_CHECK_EQUAL( 10, dofMap[2] );
  BOOST_CHECK_EQUAL( 11, dofMap[3] );


  // Test the constant assignment operator
  ElementQFieldLineClass qfldElem(qfldLineGroup1.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfldLineGroup1.nElem(); elem++)
  {
    qfldLineGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfldLineGroup1.nElem(); elem++)
  {
    qfldLineGroup1.getElement(qfldElem, elem);

    qfldElem.eval(0.25, q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_Line_ProjectPtoPp1 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD1, TopoD1, ArrayQ > QField1D_DG_Cell;
  typedef QField1D_DG_Cell::FieldCellGroupType<Line> QFieldLineClass;
  typedef QFieldLineClass::ElementType<> ElementQFieldLineClass;

  ArrayQ q0, q1;

  XField1D xfld1(3);

  for (int order = 1; order < BasisFunctionLine_HierarchicalPMax; order++)
  {
    for (int orderinc = 1; orderinc <= BasisFunctionLine_HierarchicalPMax-order; orderinc++)
    {
      QField1D_DG_Cell qfldP  (xfld1, order, BasisFunctionCategory_Hierarchical);
      QField1D_DG_Cell qfldPp1(xfld1, order+orderinc, BasisFunctionCategory_Hierarchical);

      QFieldLineClass& qfldLineP   = qfldP.getCellGroup<Line>(0);
      QFieldLineClass& qfldLinePp1 = qfldPp1.getCellGroup<Line>(0);

      ElementQFieldLineClass qfldElemP(qfldLineP.basis());
      ElementQFieldLineClass qfldElemPp1(qfldLinePp1.basis());

      //Give some non-zero initial condition
      for (int n = 0; n < qfldP.nDOF(); n++)
        qfldP.DOF(n) = n+1;

      for (int n = 0; n < qfldPp1.nDOF(); n++)
        qfldPp1.DOF(n) = 0;

      //Use Line function projectTo
      qfldLineP.projectTo(qfldLinePp1);

      for (int elem = 0; elem < qfldLineP.nElem(); elem++)
      {
        qfldLineP.getElement(qfldElemP, elem);
        qfldLinePp1.getElement(qfldElemPp1, elem);

        qfldElemP.eval(0.25, q0);
        qfldElemPp1.eval(0.25, q1);
        BOOST_CHECK_GT(q0[0], 1);
        BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
      }

      //Wipe out DOF's for P1
      for (int n = 0; n < qfldPp1.nDOF(); n++)
        qfldPp1.DOF(n) = -1;

      //Use base function projectTo
      qfldP.projectTo(qfldPp1);

      for (int elem = 0; elem < qfldLineP.nElem(); elem++)
      {
        qfldLineP.getElement(qfldElemP, elem);
        qfldLinePp1.getElement(qfldElemPp1, elem);

        qfldElemP.eval(0.25, q0);
        qfldElemPp1.eval(0.25, q1);
        BOOST_CHECK_GT(q0[0], 1);
        BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
      }
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_InteriorNode_P0 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD1, TopoD1, ArrayQ > QField1D_DG_Cell;
  typedef QField1D_DG_Cell::FieldTraceGroupType<Triangle> QFieldLineClass;

  int dofMap[1];

  XField1D xfld1(3);

  int order = 0;
  Field_DG_InteriorTrace< PhysD1, TopoD1, ArrayQ > qfld1(xfld1, order, BasisFunctionCategory_Lagrange);

  BOOST_CHECK_EQUAL( 2, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nHubTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldLineClass& qfldGroup1 = qfld1.getInteriorTraceGroup<Node>(0);

  qfldGroup1.associativity(0).getGlobalMapping( dofMap, 1 );
  BOOST_CHECK_EQUAL( 0, dofMap[0] );

  qfldGroup1.associativity(1).getGlobalMapping( dofMap, 1 );
  BOOST_CHECK_EQUAL( 1, dofMap[0] );

  Field< PhysD1, TopoD1, ArrayQ > qfld2(qfld1, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld2.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld2.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld2.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld2.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld2.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()         ,  qfld2.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld2.getXField() );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_BoundaryNode_P0 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD1, TopoD1, ArrayQ > QField1D_DG_Cell;
  typedef QField1D_DG_Cell::FieldTraceGroupType<Node> QFieldNodeClass;
  typedef QFieldNodeClass::ElementType<> ElementQFieldNodeClass;

  int nodeMap[1];

  XField1D_2Line_X1_1Group xfld1;

  int order = 0;
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> qfld1(xfld1, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 2, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 2, qfld1.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld1.nHubTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 2, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldNodeClass& qfldGroup0 = qfld1.getBoundaryTraceGroup<Node>(0);
  const QFieldNodeClass& qfldGroup1 = qfld1.getBoundaryTraceGroup<Node>(1);

  qfldGroup0.associativity(0).getGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( 0, nodeMap[0] );

  qfldGroup1.associativity(0).getGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( 1, nodeMap[0] );


  Field< PhysD1, TopoD1, ArrayQ > qfld3(qfld1, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()         ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld3.getXField() );


  // Test the constant assignment operator
  ElementQFieldNodeClass qfldElem(qfldGroup0.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfldGroup0.nElem(); elem++)
  {
    qfldGroup0.getElement(qfldElem, elem);

    qfldElem.eval(q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfldGroup0.nElem(); elem++)
  {
    qfldGroup0.getElement(qfldElem, elem);

    qfldElem.eval(q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_BoundaryNode_P1_Empty )
{
  typedef DLA::VectorS<2, Real> ArrayQ;

  XField1D_2Line_X1_1Group xfld1;

  int order = 0;
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> qfld1(xfld1, order, BasisFunctionCategory_Legendre, {}); // Empty with no groups

  BOOST_CHECK_EQUAL( 0, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 0, qfld1.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld1.nHubTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_BoundaryNode_P1_Subset )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD1, TopoD1, ArrayQ > QField1D_DG_Cell;
  typedef QField1D_DG_Cell::FieldTraceGroupType<Node> QFieldNodeClass;

  int dofMap[1];

  XField1D_2Line_X1_1Group xfld1;

  int order = 0;
  Field_DG_BoundaryTrace<PhysD1, TopoD1, ArrayQ> qfld1(xfld1, order, BasisFunctionCategory_Legendre, {1}); // Just the right BC group

  BOOST_CHECK_EQUAL( 1, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld1.nElem() );
  BOOST_CHECK_EQUAL( 0, qfld1.nHubTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 1, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldNodeClass& qfldGroup0 = qfld1.getBoundaryTraceGroup<Node>(0);

  qfldGroup0.associativity(0).getGlobalMapping( dofMap, 1 );
  BOOST_CHECK_EQUAL( 0, dofMap[0] );

  Field< PhysD1, TopoD1, ArrayQ > qfld3(qfld1, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()         ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld3.getXField() );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_HubNode_P0 )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD2, TopoD1, ArrayQ > QField1D_DG_Cell;
  typedef QField1D_DG_Cell::FieldTraceGroupType<Node> QFieldNodeClass;
  typedef QFieldNodeClass::ElementType<> ElementQFieldNodeClass;
  const int D = PhysD2::D;
  typedef DLA::VectorS<D,Real> VectorX;

  int nodeMap[1];

  // ---------- Set up a 2-element line grid ----------
  const int nelem_a = 2;  // number of elements on airfoil
  const int nelem_w = 2;  // number of elements on wake

  const int nnode_a = nelem_a + 1;
  const int nnode_w = nelem_w + 1;

  std::vector<VectorX> coordinates_a(nnode_a);
  coordinates_a[0] = {1, 0.1};
  coordinates_a[1] = {0, 0};
  coordinates_a[2] = {1, -0.1};

  std::vector<VectorX> coordinates_w(nnode_w);
  coordinates_w[0] = {1.1, 0};
  coordinates_w[1] = {2.1, 0.3};
  coordinates_w[2] = {3.1, -0.1};

  XField2D_Line_X1_2Group_AirfoilWithWake xfld1(coordinates_a,coordinates_w);

  int order = 0;
  Field_DG_HubTrace<PhysD2, TopoD1, ArrayQ> qfld1(xfld1, order, BasisFunctionCategory_Legendre);

  BOOST_CHECK_EQUAL( 1, qfld1.nDOF() );
  BOOST_CHECK_EQUAL( 1, qfld1.nElem() );
  BOOST_CHECK_EQUAL( 1, qfld1.nHubTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( 0, qfld1.nCellGroups() );
  BOOST_CHECK_EQUAL( &xfld1, &qfld1.getXField() );

  const QFieldNodeClass& qfldGroup0 = qfld1.getHubTraceGroup<Node>(0);

  qfldGroup0.associativity(0).getGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( 0, nodeMap[0] );


  Field< PhysD2, TopoD1, ArrayQ > qfld3(qfld1, FieldCopy());

  BOOST_CHECK_EQUAL(  qfld1.nDOF()                ,  qfld3.nDOF() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFpossessed()       ,  qfld3.nDOFpossessed() );
  BOOST_CHECK_EQUAL(  qfld1.nDOFghost()           ,  qfld3.nDOFghost() );
  BOOST_CHECK_EQUAL(  qfld1.nHubTraceGroups()     ,  qfld3.nHubTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nInteriorTraceGroups(),  qfld3.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nBoundaryTraceGroups(),  qfld3.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL(  qfld1.nCellGroups()         ,  qfld3.nCellGroups() );
  BOOST_CHECK_EQUAL( &qfld1.getXField()           , &qfld3.getXField() );


  // Test the constant assignment operator
  ElementQFieldNodeClass qfldElem(qfldGroup0.basis());

  qfld1 = 1.23;
  ArrayQ q1;

  for (int elem = 0; elem < qfldGroup0.nElem(); elem++)
  {
    qfldGroup0.getElement(qfldElem, elem);

    qfldElem.eval(q1);
    BOOST_CHECK_CLOSE(1.23, q1[0], 1e-12);
    BOOST_CHECK_CLOSE(1.23, q1[1], 1e-12);
  }

  ArrayQ q0 = {4.56, 7.89};
  qfld1 = q0;

  for (int elem = 0; elem < qfldGroup0.nElem(); elem++)
  {
    qfldGroup0.getElement(qfldElem, elem);

    qfldElem.eval(q1);
    BOOST_CHECK_CLOSE(q0[0], q1[0], 1e-12);
    BOOST_CHECK_CLOSE(q0[1], q1[1], 1e-12);
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DG_Exception )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD1, TopoD1, ArrayQ > QField1D_DG_Cell;

  XField1D xfld1(3);

  int order = 999; //This should always higher than the maximum available order
  BOOST_CHECK_THROW( QField1D_DG_Cell qfld1(xfld1, order, BasisFunctionCategory_Hierarchical), DeveloperException );
}

#if 0

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( IO )
{
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysD1, TopoD1, ArrayQ > QField1D_DG_Cell;
//  typedef Field_DG_InteriorTrace< PhysD1, TopoD1, ArrayQ > QField1D_DG_InteriorTrace;
  typedef Field_DG_BoundaryTrace< PhysD1, TopoD1, ArrayQ > QField1D_DG_BoundaryTrace;

  //Set the 2nd argument to false to regenerate the pattern file
  output_test_stream output( "IO/Field/Field1D_DG_pattern.txt", true );

  XField1D xfld(2);

  QField1D_DG_Cell qfld1(xfld, 2, BasisFunctionCategory_Hierarchical);
  qfld1.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );

//  QField1D_DG_InteriorTrace qfld2(xfld, 2);
//  qfld2.dump( 2, output );
//  BOOST_CHECK( output.match_pattern() );

  QField1D_DG_BoundaryTrace qfld4(xfld, 2);
  qfld4.dump( 2, output );
  BOOST_CHECK( output.match_pattern() );
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
