// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// FieldLinesearch_btest
// testing of FieldLinesearch_btest class
//

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_X1.h"
#include "unit/UnitGrids/XField3D_2Tet_X1_1Group.h"

#include "tools/SANSnumerics.h"     // Real
#include "BasisFunction/BasisFunctionArea_Triangle.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_Trace.h"

#include "Field/FieldArea_CG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldArea_DG_Trace.h"

#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_Trace.h"

#include "Field/FieldLinesearch/FieldLinesearch_Line.h"
#include "Field/FieldLinesearch/FieldLinesearch_Area.h"
#include "Field/FieldLinesearch/FieldLinesearch_Volume.h"

using namespace std;
using namespace SANS;


//Explicitly instantiate classes to get proper coverage information
namespace SANS
{
template class FieldLinesearch<PhysD1, TopoD1, DLA::VectorS<2, Real>>;
template class FieldLinesearch<PhysD1, TopoD1, DLA::VectorS<3, Real>>;

template class FieldLinesearch<PhysD2, TopoD2, DLA::VectorS<2, Real>>;
template class FieldLinesearch<PhysD2, TopoD2, DLA::VectorS<3, Real>>;

template class FieldLinesearch<PhysD3, TopoD3, DLA::VectorS<2, Real>>;
template class FieldLinesearch<PhysD3, TopoD3, DLA::VectorS<3, Real>>;
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( Field_Linesearch_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldLinesearch1D_DG_Cell )
{
  typedef PhysD1 PhysDim;
  typedef TopoD1 TopoDim;
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysDim, TopoDim, ArrayQ > QFieldType;

  XField1D xfld(3);

  int order = 1;
  QFieldType qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  FieldLinesearch<PhysDim, TopoDim, ArrayQ> linesearch_fld(qfld);

  BOOST_CHECK( linesearch_fld.spaceType() == SpaceType::Technical );

  BOOST_CHECK_EQUAL( 3, linesearch_fld.nDOF() );
  BOOST_CHECK_EQUAL( 1, linesearch_fld.nCellGroups() );
  BOOST_CHECK_EQUAL( 0, linesearch_fld.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, linesearch_fld.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( &xfld, &linesearch_fld.getXField() );

  BOOST_CHECK_EQUAL( 3, linesearch_fld.nDOFCellGroup(0) );
  BOOST_CHECK_THROW( linesearch_fld.nDOFInteriorTraceGroup(0), SANSException);
  BOOST_CHECK_THROW( linesearch_fld.nDOFBoundaryTraceGroup(0), SANSException);

  linesearch_fld = 0.5;

  for (int i = 0; i < linesearch_fld.nDOF(); i++)
    for (int n = 0; n < ArrayQ::M; n++)
      BOOST_CHECK_EQUAL( linesearch_fld.DOF(i)[n], 0.5 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldLinesearch1D_DG_Trace )
{
  typedef PhysD1 PhysDim;
  typedef TopoD1 TopoDim;
  typedef DLA::VectorS<3, Real> ArrayQ;
  typedef Field_DG_Trace< PhysDim, TopoDim, ArrayQ > QFieldType;

  XField1D xfld(3);

  int order = 1;
  QFieldType qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  FieldLinesearch<PhysDim, TopoDim, ArrayQ> linesearch_fld(qfld);

  BOOST_CHECK_EQUAL( 4, linesearch_fld.nDOF() );
  BOOST_CHECK_EQUAL( 0, linesearch_fld.nCellGroups() );
  BOOST_CHECK_EQUAL( 1, linesearch_fld.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 2, linesearch_fld.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( &xfld, &linesearch_fld.getXField() );

  BOOST_CHECK_THROW( linesearch_fld.nDOFCellGroup(0), SANSException);
  BOOST_CHECK_EQUAL( 2, linesearch_fld.nDOFInteriorTraceGroup(0) );
  BOOST_CHECK_EQUAL( 1, linesearch_fld.nDOFBoundaryTraceGroup(0) );
  BOOST_CHECK_EQUAL( 1, linesearch_fld.nDOFBoundaryTraceGroup(1) );

  linesearch_fld = 0.5;

  for (int i = 0; i < linesearch_fld.nDOF(); i++)
    for (int n = 0; n < ArrayQ::M; n++)
      BOOST_CHECK_EQUAL( linesearch_fld.DOF(i)[n], 0.5 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldLinesearch2D_DG_Cell )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysDim, TopoDim, ArrayQ > QFieldType;

  XField2D_4Triangle_X1_1Group xfld;

  int order = 1;
  QFieldType qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  FieldLinesearch<PhysDim, TopoDim, ArrayQ> linesearch_fld(qfld);

  BOOST_CHECK_EQUAL( 4, linesearch_fld.nDOF() );
  BOOST_CHECK_EQUAL( 1, linesearch_fld.nCellGroups() );
  BOOST_CHECK_EQUAL( 0, linesearch_fld.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, linesearch_fld.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( &xfld, &linesearch_fld.getXField() );

  BOOST_CHECK_EQUAL( 4, linesearch_fld.nDOFCellGroup(0) );
  BOOST_CHECK_THROW( linesearch_fld.nDOFInteriorTraceGroup(0), SANSException);
  BOOST_CHECK_THROW( linesearch_fld.nDOFBoundaryTraceGroup(0), SANSException);

  linesearch_fld = 0.5;

  for (int i = 0; i < linesearch_fld.nDOF(); i++)
    for (int n = 0; n < ArrayQ::M; n++)
      BOOST_CHECK_EQUAL( linesearch_fld.DOF(i)[n], 0.5 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldLinesearch2D_CG_Cell )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_CG_Cell< PhysDim, TopoDim, ArrayQ > QFieldType;

  XField2D_4Triangle_X1_1Group xfld;

  int order = 1;
  QFieldType qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  FieldLinesearch<PhysDim, TopoDim, ArrayQ> linesearch_fld(qfld);

  BOOST_CHECK_EQUAL( 4, linesearch_fld.nDOF() );
  BOOST_CHECK_EQUAL( 1, linesearch_fld.nCellGroups() );
  BOOST_CHECK_EQUAL( 0, linesearch_fld.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, linesearch_fld.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( &xfld, &linesearch_fld.getXField() );

  BOOST_CHECK_EQUAL( 4, linesearch_fld.nDOFCellGroup(0) );
  BOOST_CHECK_THROW( linesearch_fld.nDOFInteriorTraceGroup(0), SANSException);
  BOOST_CHECK_THROW( linesearch_fld.nDOFBoundaryTraceGroup(0), SANSException);

  linesearch_fld = 0.5;

  for (int i = 0; i < linesearch_fld.nDOF(); i++)
    for (int n = 0; n < ArrayQ::M; n++)
      BOOST_CHECK_EQUAL( linesearch_fld.DOF(i)[n], 0.5 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldLinesearch2D_DG_BTrace )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;
  typedef DLA::VectorS<3, Real> ArrayQ;
  typedef Field_DG_BoundaryTrace< PhysDim, TopoDim, ArrayQ > QFieldType;

  XField2D_Box_Triangle_X1 xfld(2,2);

  int order = 1;
  QFieldType qfld(xfld, order, BasisFunctionCategory_Hierarchical, {xfld.iBottom, xfld.iTop, xfld.iLeft});

  FieldLinesearch<PhysDim, TopoDim, ArrayQ> linesearch_fld(qfld);

  BOOST_CHECK_EQUAL( 6, linesearch_fld.nDOF() );
  BOOST_CHECK_EQUAL( 0, linesearch_fld.nCellGroups() );
  BOOST_CHECK_EQUAL( 0, linesearch_fld.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 3, linesearch_fld.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( &xfld, &linesearch_fld.getXField() );

  BOOST_CHECK_THROW( linesearch_fld.nDOFCellGroup(0), SANSException);
  BOOST_CHECK_THROW( linesearch_fld.nDOFInteriorTraceGroup(0), SANSException);
  BOOST_CHECK_EQUAL( 2, linesearch_fld.nDOFBoundaryTraceGroup(0) );
  BOOST_CHECK_EQUAL( 2, linesearch_fld.nDOFBoundaryTraceGroup(1) );
  BOOST_CHECK_EQUAL( 2, linesearch_fld.nDOFBoundaryTraceGroup(2) );

  linesearch_fld = 0.5;

  for (int i = 0; i < linesearch_fld.nDOF(); i++)
    for (int n = 0; n < ArrayQ::M; n++)
      BOOST_CHECK_EQUAL( linesearch_fld.DOF(i)[n], 0.5 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldLinesearch2D_DG_Trace )
{
  typedef PhysD2 PhysDim;
  typedef TopoD2 TopoDim;
  typedef DLA::VectorS<3, Real> ArrayQ;
  typedef Field_DG_Trace< PhysDim, TopoDim, ArrayQ > QFieldType;

  XField2D_Box_Triangle_X1 xfld(2,2);

  int order = 1;
  QFieldType qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  FieldLinesearch<PhysDim, TopoDim, ArrayQ> linesearch_fld(qfld);

  BOOST_CHECK_EQUAL( 16, linesearch_fld.nDOF() );
  BOOST_CHECK_EQUAL( 0, linesearch_fld.nCellGroups() );
  BOOST_CHECK_EQUAL( 3, linesearch_fld.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 4, linesearch_fld.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( &xfld, &linesearch_fld.getXField() );

  BOOST_CHECK_THROW( linesearch_fld.nDOFCellGroup(0), SANSException);
  BOOST_CHECK_EQUAL( 2, linesearch_fld.nDOFInteriorTraceGroup(0) );
  BOOST_CHECK_EQUAL( 2, linesearch_fld.nDOFInteriorTraceGroup(1) );
  BOOST_CHECK_EQUAL( 4, linesearch_fld.nDOFInteriorTraceGroup(2) );
  BOOST_CHECK_EQUAL( 2, linesearch_fld.nDOFBoundaryTraceGroup(0) );
  BOOST_CHECK_EQUAL( 2, linesearch_fld.nDOFBoundaryTraceGroup(1) );
  BOOST_CHECK_EQUAL( 2, linesearch_fld.nDOFBoundaryTraceGroup(2) );
  BOOST_CHECK_EQUAL( 2, linesearch_fld.nDOFBoundaryTraceGroup(3) );

  linesearch_fld = 0.5;

  for (int i = 0; i < linesearch_fld.nDOF(); i++)
    for (int n = 0; n < ArrayQ::M; n++)
      BOOST_CHECK_EQUAL( linesearch_fld.DOF(i)[n], 0.5 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldLinesearch3D_DG_Cell )
{
  typedef PhysD3 PhysDim;
  typedef TopoD3 TopoDim;
  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef Field_DG_Cell< PhysDim, TopoDim, ArrayQ > QFieldType;

  XField3D_2Tet_X1_1Group xfld;

  int order = 1;
  QFieldType qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  FieldLinesearch<PhysDim, TopoDim, ArrayQ> linesearch_fld(qfld);

  BOOST_CHECK_EQUAL( 2, linesearch_fld.nDOF() );
  BOOST_CHECK_EQUAL( 1, linesearch_fld.nCellGroups() );
  BOOST_CHECK_EQUAL( 0, linesearch_fld.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 0, linesearch_fld.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( &xfld, &linesearch_fld.getXField() );

  BOOST_CHECK_EQUAL( 2, linesearch_fld.nDOFCellGroup(0) );
  BOOST_CHECK_THROW( linesearch_fld.nDOFInteriorTraceGroup(0), SANSException);
  BOOST_CHECK_THROW( linesearch_fld.nDOFBoundaryTraceGroup(0), SANSException);

  linesearch_fld = 0.5;

  for (int i = 0; i < linesearch_fld.nDOF(); i++)
    for (int n = 0; n < ArrayQ::M; n++)
      BOOST_CHECK_EQUAL( linesearch_fld.DOF(i)[n], 0.5 );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( FieldLinesearch3D_DG_Trace )
{
  typedef PhysD3 PhysDim;
  typedef TopoD3 TopoDim;
  typedef DLA::VectorS<3, Real> ArrayQ;
  typedef Field_DG_Trace< PhysDim, TopoDim, ArrayQ > QFieldType;

  XField3D_2Tet_X1_1Group xfld;

  int order = 1;
  QFieldType qfld(xfld, order, BasisFunctionCategory_Hierarchical);

  FieldLinesearch<PhysDim, TopoDim, ArrayQ> linesearch_fld(qfld);

  BOOST_CHECK_EQUAL( 7, linesearch_fld.nDOF() );
  BOOST_CHECK_EQUAL( 0, linesearch_fld.nCellGroups() );
  BOOST_CHECK_EQUAL( 1, linesearch_fld.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 6, linesearch_fld.nBoundaryTraceGroups() );
  BOOST_CHECK_EQUAL( &xfld, &linesearch_fld.getXField() );

  BOOST_CHECK_THROW( linesearch_fld.nDOFCellGroup(0), SANSException);
  BOOST_CHECK_EQUAL( 1, linesearch_fld.nDOFInteriorTraceGroup(0) );
  BOOST_CHECK_EQUAL( 1, linesearch_fld.nDOFBoundaryTraceGroup(0) );
  BOOST_CHECK_EQUAL( 1, linesearch_fld.nDOFBoundaryTraceGroup(1) );
  BOOST_CHECK_EQUAL( 1, linesearch_fld.nDOFBoundaryTraceGroup(2) );
  BOOST_CHECK_EQUAL( 1, linesearch_fld.nDOFBoundaryTraceGroup(3) );
  BOOST_CHECK_EQUAL( 1, linesearch_fld.nDOFBoundaryTraceGroup(4) );
  BOOST_CHECK_EQUAL( 1, linesearch_fld.nDOFBoundaryTraceGroup(5) );

  linesearch_fld = 0.5;

  for (int i = 0; i < linesearch_fld.nDOF(); i++)
    for (int n = 0; n < ArrayQ::M; n++)
      BOOST_CHECK_EQUAL( linesearch_fld.DOF(i)[n], 0.5 );
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
