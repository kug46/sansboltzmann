// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// XField2D_Curved_btest
// testing of higher order XField2D grids
//
// Note: unit grids tested in "unit/UnitGrids/UnitGrids_btest.cpp"

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/linspace.h"

#include "Field/XFieldArea.h"
#include "unit/UnitGrids/XField2D_1Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"

#include "BasisFunction/BasisFunctionLine.h"
#include "BasisFunction/BasisFunctionArea_Triangle.h"

#include "unit/Field/XField2D_CheckTraceCoord2D_btest.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( XField2D_Curved_test_suite )

//----------------------------------------------------------------------------//
class XField2D_NegativeJacobian : public XField<PhysD2,TopoD2>
{
public:
  XField2D_NegativeJacobian()
  {
    //Create the DOF arrays
    resizeDOF(3);

    //Create the element groups
    resizeInteriorTraceGroups(0);
    resizeBoundaryTraceGroups(3);
    resizeCellGroups(1);

    // nodal coordinates for the triangle.
    DOF(0) = {0,  0};
    DOF(1) = {1,  0};
    DOF(2) = {0, -1};   //<---- Should be {0, 1} for positive jacobian

    // area field variable
    FieldCellGroupType<Triangle>::FieldAssociativityConstructorType fldAssocCell( BasisFunctionAreaBase<Triangle>::HierarchicalP1, 1 );

    // set the processor rank
    fldAssocCell.setAssociativity( 0 ).setRank( 0 );

    //element area associativity
    fldAssocCell.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1, 2} );

    // edge signs for elements (L is +, R is -)
    fldAssocCell.setAssociativity( 0 ).setEdgeSign( +1, 0 );


    FieldCellGroupType<Triangle>* xfldElementGroup = NULL;
    cellGroups_[0] = xfldElementGroup = new FieldCellGroupType<Triangle>( fldAssocCell );

    xfldElementGroup->setDOF(DOF_, nDOF_);

    nElem_ = 1;

    // interior-edge field variable

    // none

    // boundary-edge field variable

    FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocBedge0( BasisFunctionLineBase::HierarchicalP1, 1 );
    FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocBedge1( BasisFunctionLineBase::HierarchicalP1, 1 );
    FieldTraceGroupType<Line>::FieldAssociativityConstructorType fldAssocBedge2( BasisFunctionLineBase::HierarchicalP1, 1 );

    // set the processor rank
    fldAssocBedge0.setAssociativity( 0 ).setRank( 0 );
    fldAssocBedge1.setAssociativity( 0 ).setRank( 0 );
    fldAssocBedge2.setAssociativity( 0 ).setRank( 0 );

    // edge-element associativity
    fldAssocBedge0.setAssociativity( 0 ).setNodeGlobalMapping( {0, 1} );
    fldAssocBedge1.setAssociativity( 0 ).setNodeGlobalMapping( {1, 2} );
    fldAssocBedge2.setAssociativity( 0 ).setNodeGlobalMapping( {2, 0} );

    // edge-to-cell connectivity
    fldAssocBedge0.setGroupLeft( 0 );
    fldAssocBedge1.setGroupLeft( 0 );
    fldAssocBedge2.setGroupLeft( 0 );
    fldAssocBedge0.setElementLeft( 0, 0 );
    fldAssocBedge1.setElementLeft( 0, 0 );
    fldAssocBedge2.setElementLeft( 0, 0 );
    fldAssocBedge0.setCanonicalTraceLeft( CanonicalTraceToCell(2, 1), 0 );
    fldAssocBedge1.setCanonicalTraceLeft( CanonicalTraceToCell(0, 1), 0 );
    fldAssocBedge2.setCanonicalTraceLeft( CanonicalTraceToCell(1, 1), 0 );

    boundaryTraceGroups_[0] = new FieldTraceGroupType<Line>( fldAssocBedge0 );
    boundaryTraceGroups_[1] = new FieldTraceGroupType<Line>( fldAssocBedge1 );
    boundaryTraceGroups_[2] = new FieldTraceGroupType<Line>( fldAssocBedge2 );

    boundaryTraceGroups_[0]->setDOF(DOF_, nDOF_);
    boundaryTraceGroups_[1]->setDOF(DOF_, nDOF_);
    boundaryTraceGroups_[2]->setDOF(DOF_, nDOF_);

    //checkGrid(); // Don't check the grid so the curved grid catches the error instead
    initDefaultElmentIDs();
  }
};

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( NegativeJacobian )
{
  XField2D_NegativeJacobian xfld_X1;
  typedef XField<PhysD2,TopoD2> XField2D;

  BOOST_CHECK_THROW( XField2D xfld(xfld_X1, 2), XFieldException );
}

//--------------------CHECKING HIGHER ORDER GRIDS-----------------------------//

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_1Triangle_X2_Hierarchical_test )
{
  typedef std::array<int,3> Int3;

  XField2D_1Triangle_X1_1Group xfld_X1; //Linear mesh

  XField<PhysD2,TopoD2> xfld(xfld_X1, 2); //Construct Q2 mesh from linear mesh

  BOOST_CHECK_EQUAL( xfld.nDOF(), 6 );

  //Check the node values
  for (int i = 0; i < Triangle::NEdge; i++)
  {
    //All higher-order node values need to be zero since the Q2 mesh is also still essentially linear
    BOOST_CHECK_EQUAL( xfld.DOF(i)[0], 0);
    BOOST_CHECK_EQUAL( xfld.DOF(i)[1], 0);
  }
  BOOST_CHECK_EQUAL( xfld.DOF(3)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[1], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(4)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(4)[1], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(5)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(5)[1], 1 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldArea = xfld.getCellGroup<Triangle>(0);

  int nodeMap[3];
  int edgeMap[3];

  //Node DOFs
  xfldArea.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );
  BOOST_CHECK_EQUAL( nodeMap[2], 5 );

  //Edge DOFs
  xfldArea.associativity(0).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0], 2 );
  BOOST_CHECK_EQUAL( edgeMap[1], 1 );
  BOOST_CHECK_EQUAL( edgeMap[2], 0 );

  Int3 edgeSign;

  edgeSign = xfldArea.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  // interior-edge field variable

  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 0 );

  // boundary-edge field variable

  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 3 );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(1).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(2).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBedge0 = xfld.getBoundaryTraceGroup<Line>(0);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBedge1 = xfld.getBoundaryTraceGroup<Line>(1);
  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBedge2 = xfld.getBoundaryTraceGroup<Line>(2);

  //Node DOFs
  xfldBedge0.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );
  BOOST_CHECK_EQUAL( nodeMap[1], 4 );

  xfldBedge1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 4 );
  BOOST_CHECK_EQUAL( nodeMap[1], 5 );

  xfldBedge2.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 5 );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 );

  //Edge DOFs
  xfldBedge0.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( edgeMap[0], 0 );

  xfldBedge1.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( edgeMap[0], 2 );

  xfldBedge2.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( edgeMap[0], 1 );

  // boundary edge-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBedge0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBedge1.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBedge2.getGroupLeft(), 0 );

  BOOST_CHECK_EQUAL( xfldBedge0.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBedge1.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBedge2.getElementLeft(0), 0 );

  BOOST_CHECK_EQUAL( xfldBedge0.getCanonicalTraceLeft(0).trace, 2 );
  BOOST_CHECK_EQUAL( xfldBedge1.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldBedge2.getCanonicalTraceLeft(0).trace, 1 );

  BOOST_CHECK_EQUAL( xfldBedge0.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBedge1.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBedge2.getCanonicalTraceLeft(0).orientation, 1 );


  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  // Check that boundary trace coordinates match
  for_each_BoundaryFieldTraceGroup_Cell<TopoD2>::apply(
      CheckBoundaryTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );

  //Curve the P2 xfld by setting edge DOFs to some random nonzero value
  const int nEdges = Triangle::NEdge;
  for (int k = 0; k < nEdges; k++) xfld.DOF(k) = 0.1*(k+1);

  // Check that boundary trace coordinates match
  for_each_BoundaryFieldTraceGroup_Cell<TopoD2>::apply(
      CheckBoundaryTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_1Triangle_X3_Hierarchical_test )
{
  typedef std::array<int,3> Int3;

  XField2D_1Triangle_X1_1Group xfld_X1; //Linear mesh

  XField<PhysD2,TopoD2> xfld(xfld_X1, 3); //Construct Q3 mesh from linear mesh

  BOOST_CHECK_EQUAL( xfld.nDOF(), 10 );

  //Check the node values
  for (int i = 0; i < 7; i++)
  {
    //All higher-order node values need to be zero since the Q2 mesh is also still essentially linear
    BOOST_CHECK_EQUAL( xfld.DOF(i)[0], 0);
    BOOST_CHECK_EQUAL( xfld.DOF(i)[1], 0);
  }
  BOOST_CHECK_EQUAL( xfld.DOF(7)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(7)[1], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(8)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(8)[1], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(9)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(9)[1], 1 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldArea = xfld.getCellGroup<Triangle>(0);

  int nodeMap[3];
  int edgeMap[6];
  int cellMap[1];

  //Node DOFs
  xfldArea.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 7 );
  BOOST_CHECK_EQUAL( nodeMap[1], 8 );
  BOOST_CHECK_EQUAL( nodeMap[2], 9 );

  //Edge DOFs
  xfldArea.associativity(0).getEdgeGlobalMapping( edgeMap, 6 );
  BOOST_CHECK_EQUAL( edgeMap[0], 5 );
  BOOST_CHECK_EQUAL( edgeMap[1], 6 );
  BOOST_CHECK_EQUAL( edgeMap[2], 3 );
  BOOST_CHECK_EQUAL( edgeMap[3], 4 );
  BOOST_CHECK_EQUAL( edgeMap[4], 1 );
  BOOST_CHECK_EQUAL( edgeMap[5], 2 );

  //Cell DOFs
  xfldArea.associativity(0).getCellGlobalMapping( cellMap, 1 );
  BOOST_CHECK_EQUAL( cellMap[0], 0 );

  Int3 edgeSign;

  edgeSign = xfldArea.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  // interior-edge field variable

  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 0 );

  // boundary-edge field variable

  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 3 );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(1).topoTypeID() == typeid(Line) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(2).topoTypeID() == typeid(Line) );

  XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBedge0 = xfld.getBoundaryTraceGroup<Line>(0);
  XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBedge1 = xfld.getBoundaryTraceGroup<Line>(1);
  XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBedge2 = xfld.getBoundaryTraceGroup<Line>(2);

  //Node DOFs
  xfldBedge0.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 7 );
  BOOST_CHECK_EQUAL( nodeMap[1], 8 );

  xfldBedge1.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 8 );
  BOOST_CHECK_EQUAL( nodeMap[1], 9 );

  xfldBedge2.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 9 );
  BOOST_CHECK_EQUAL( nodeMap[1], 7 );

  //Edge DOFs
  xfldBedge0.associativity(0).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( edgeMap[0], 1 );
  BOOST_CHECK_EQUAL( edgeMap[1], 2 );

  xfldBedge1.associativity(0).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( edgeMap[0], 5 );
  BOOST_CHECK_EQUAL( edgeMap[1], 6 );

  xfldBedge2.associativity(0).getEdgeGlobalMapping( edgeMap, 2 );
  BOOST_CHECK_EQUAL( edgeMap[0], 3 );
  BOOST_CHECK_EQUAL( edgeMap[1], 4 );

  // boundary edge-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBedge0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBedge1.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBedge2.getGroupLeft(), 0 );

  BOOST_CHECK_EQUAL( xfldBedge0.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBedge1.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBedge2.getElementLeft(0), 0 );

  BOOST_CHECK_EQUAL( xfldBedge0.getCanonicalTraceLeft(0).trace, 2 );
  BOOST_CHECK_EQUAL( xfldBedge1.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldBedge2.getCanonicalTraceLeft(0).trace, 1 );

  BOOST_CHECK_EQUAL( xfldBedge0.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBedge1.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBedge2.getCanonicalTraceLeft(0).orientation, 1 );

  // test that modifying edge DOFS gives proper results for cell DOFS

  // EDGE 0 (0,0) -> (0,1)
  typedef XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>::template ElementType<> LineElementType;
  LineElementType edgeelem0(xfldBedge0.basis());
  xfldBedge0.getElement(edgeelem0, 0);

  //Modify edge DOFS
  edgeelem0.DOF(2)[0] = 0.0;
  edgeelem0.DOF(2)[1] = 0.15;
  edgeelem0.DOF(3)[0] = 0.0;
  edgeelem0.DOF(3)[1] = 0.25;

  Real stest = 0.3;
  Real doftest = 0.15*4.0*stest*(1-stest) + 0.25*6.0*sqrt(3)*stest*(1-stest)*(1-2*stest);

  Real tol = 1e-12;
  BOOST_CHECK_CLOSE( edgeelem0.eval(0)[0], 0, tol);
  BOOST_CHECK_CLOSE( edgeelem0.eval(0)[1], 0, tol);
  BOOST_CHECK_CLOSE( edgeelem0.eval(1)[0], 1, tol);
  BOOST_CHECK_CLOSE( edgeelem0.eval(1)[1], 0, tol);
  BOOST_CHECK_CLOSE( edgeelem0.eval(stest)[0], 0.3, tol);
  BOOST_CHECK_CLOSE( edgeelem0.eval(stest)[1], doftest, tol);

  xfldBedge0.setElement(edgeelem0, 0);

  typedef XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>::template ElementType<> CellElementType;
  CellElementType cellelem0(xfldArea.basis());

  xfldArea.getElement(cellelem0, 0);
  BOOST_CHECK_CLOSE( cellelem0.eval(stest,0)[0], stest, tol);
  BOOST_CHECK_CLOSE( cellelem0.eval(stest,0)[1], doftest, tol);

  //EDGE 1 - diagonal (1,0) -> (0,1)
  LineElementType edgeelem1(xfldBedge1.basis());
  xfldBedge1.getElement(edgeelem1, 0);

  //Modify edge DOFs
  edgeelem1.DOF(2)[0] = 0.0;
  edgeelem1.DOF(2)[1] = 0.15;
  edgeelem1.DOF(3)[0] = 0.0;
  edgeelem1.DOF(3)[1] = 0.25;

  doftest = 1*stest + 0.15*4.0*stest*(1-stest) + 0.25*6.0*sqrt(3)*stest*(1-stest)*(1-2*stest);

  BOOST_CHECK_CLOSE( edgeelem1.eval(0)[0], 1, tol);
  BOOST_CHECK_CLOSE( edgeelem1.eval(0)[1], 0, tol);
  BOOST_CHECK_CLOSE( edgeelem1.eval(1)[0], 0, tol);
  BOOST_CHECK_CLOSE( edgeelem1.eval(1)[1], 1, tol);
  BOOST_CHECK_CLOSE( edgeelem1.eval(stest)[0], (1-stest), tol);
  BOOST_CHECK_CLOSE( edgeelem1.eval(stest)[1], doftest, tol);

  xfldBedge1.setElement(edgeelem1, 0);

  CellElementType cellelem1(xfldArea.basis());

  xfldArea.getElement(cellelem1, 0);
  BOOST_CHECK_CLOSE( cellelem1.eval(1-stest,stest)[0], (1-stest), tol);
  BOOST_CHECK_CLOSE( cellelem1.eval(1-stest,stest)[1], doftest, tol);

//  for (int i=0; i<10; i++)
//    std::cout << cellelem1.DOF(i) << "\n";


  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  // Check that boundary trace coordinates match
  for_each_BoundaryFieldTraceGroup_Cell<TopoD2>::apply(
      CheckBoundaryTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );

  //Curve the xfld by setting edge DOFs to some random nonzero value
  for (int k = 0; k < 7; k++) xfld.DOF(k) = 0.1*(k+1);

  // Check that boundary trace coordinates match
  for_each_BoundaryFieldTraceGroup_Cell<TopoD2>::apply(
      CheckBoundaryTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_4Triangle_X2_1Group_Hierarchical_test )
{
  typedef std::array<int,3> Int3;

  XField2D_4Triangle_X1_1Group xfld_X1; //Linear mesh

  XField<PhysD2,TopoD2> xfld(xfld_X1, 2); //Construct Q2 mesh from linear mesh

  BOOST_CHECK_EQUAL( xfld.nDOF(), 15 );

  //Check the node values
  for (int i=0; i<9; i++)
  {
    //All higher-order node values need to be zero since the Q2 mesh is also still essentially linear
    BOOST_CHECK_EQUAL( xfld.DOF(i)[0], 0);
    BOOST_CHECK_EQUAL( xfld.DOF(i)[1], 0);
  }
  BOOST_CHECK_EQUAL( xfld.DOF( 9)[0],  0 );  BOOST_CHECK_EQUAL( xfld.DOF( 9)[1],  0 );
  BOOST_CHECK_EQUAL( xfld.DOF(10)[0],  1 );  BOOST_CHECK_EQUAL( xfld.DOF(10)[1],  0 );
  BOOST_CHECK_EQUAL( xfld.DOF(11)[0],  0 );  BOOST_CHECK_EQUAL( xfld.DOF(11)[1],  1 );
  BOOST_CHECK_EQUAL( xfld.DOF(12)[0],  1 );  BOOST_CHECK_EQUAL( xfld.DOF(12)[1],  1 );
  BOOST_CHECK_EQUAL( xfld.DOF(13)[0], -1 );  BOOST_CHECK_EQUAL( xfld.DOF(13)[1],  1 );
  BOOST_CHECK_EQUAL( xfld.DOF(14)[0],  1 );  BOOST_CHECK_EQUAL( xfld.DOF(14)[1], -1 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldArea = xfld.getCellGroup<Triangle>(0);

  int nodeMap[3];
  int edgeMap[3];

  //Node DOFs
  xfldArea.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0],  9 );
  BOOST_CHECK_EQUAL( nodeMap[1], 10 );
  BOOST_CHECK_EQUAL( nodeMap[2], 11 );

  xfldArea.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 12 );
  BOOST_CHECK_EQUAL( nodeMap[1], 11 );
  BOOST_CHECK_EQUAL( nodeMap[2], 10 );

  xfldArea.associativity(2).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 11 );
  BOOST_CHECK_EQUAL( nodeMap[1], 13 );
  BOOST_CHECK_EQUAL( nodeMap[2],  9 );

  xfldArea.associativity(3).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 10 );
  BOOST_CHECK_EQUAL( nodeMap[1],  9 );
  BOOST_CHECK_EQUAL( nodeMap[2], 14 );

  //Edge DOFs
  xfldArea.associativity(0).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0], 4 );
  BOOST_CHECK_EQUAL( edgeMap[1], 1 );
  BOOST_CHECK_EQUAL( edgeMap[2], 0 );

  xfldArea.associativity(1).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0], 4 );
  BOOST_CHECK_EQUAL( edgeMap[1], 5 );
  BOOST_CHECK_EQUAL( edgeMap[2], 7 );

  xfldArea.associativity(2).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0], 2 );
  BOOST_CHECK_EQUAL( edgeMap[1], 1 );
  BOOST_CHECK_EQUAL( edgeMap[2], 8 );

  xfldArea.associativity(3).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0], 3 );
  BOOST_CHECK_EQUAL( edgeMap[1], 6 );
  BOOST_CHECK_EQUAL( edgeMap[2], 0 );

  Int3 edgeSign;

  edgeSign = xfldArea.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  edgeSign = xfldArea.associativity(1).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], -1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  edgeSign = xfldArea.associativity(2).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], -1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  edgeSign = xfldArea.associativity(3).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], -1 );

  // interior-edge field variable

  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldIedge = xfld.getInteriorTraceGroup<Line>(0);

  //Node DOFs
  xfldIedge.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 10 );
  BOOST_CHECK_EQUAL( nodeMap[1], 11 );

  xfldIedge.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 11 );
  BOOST_CHECK_EQUAL( nodeMap[1],  9 );

  xfldIedge.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0],  9 );
  BOOST_CHECK_EQUAL( nodeMap[1], 10 );

  //Edge DOFs
  xfldIedge.associativity(0).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( edgeMap[0], 4 );

  xfldIedge.associativity(1).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( edgeMap[0], 1 );

  xfldIedge.associativity(2).getEdgeGlobalMapping( edgeMap, 1 );
  BOOST_CHECK_EQUAL( edgeMap[0], 0 );

  // interior edge-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldIedge.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldIedge.getGroupRight(), 0 );

  BOOST_CHECK_EQUAL( xfldIedge.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldIedge.getElementRight(0), 1 );
  BOOST_CHECK_EQUAL( xfldIedge.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldIedge.getCanonicalTraceRight(0).trace, 0 );

  BOOST_CHECK_EQUAL( xfldIedge.getElementLeft(1), 0 );
  BOOST_CHECK_EQUAL( xfldIedge.getElementRight(1), 2 );
  BOOST_CHECK_EQUAL( xfldIedge.getCanonicalTraceLeft(1).trace, 1 );
  BOOST_CHECK_EQUAL( xfldIedge.getCanonicalTraceRight(1).trace, 1 );

  BOOST_CHECK_EQUAL( xfldIedge.getElementLeft(2), 0 );
  BOOST_CHECK_EQUAL( xfldIedge.getElementRight(2), 3 );
  BOOST_CHECK_EQUAL( xfldIedge.getCanonicalTraceLeft(2).trace, 2 );
  BOOST_CHECK_EQUAL( xfldIedge.getCanonicalTraceRight(2).trace, 2 );

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBedge = xfld.getBoundaryTraceGroup<Line>(0);

  //Node DOFs
  xfldBedge.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0],  9 );
  BOOST_CHECK_EQUAL( nodeMap[1], 14 );

  xfldBedge.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 14 );
  BOOST_CHECK_EQUAL( nodeMap[1], 10 );

  xfldBedge.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 10 );
  BOOST_CHECK_EQUAL( nodeMap[1], 12 );

  xfldBedge.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 12 );
  BOOST_CHECK_EQUAL( nodeMap[1], 11 );

  xfldBedge.associativity(4).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 11 );
  BOOST_CHECK_EQUAL( nodeMap[1], 13 );

  xfldBedge.associativity(5).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 13 );
  BOOST_CHECK_EQUAL( nodeMap[1],  9 );

  //Edge DOFs
  xfldBedge.associativity(0).getEdgeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );

  xfldBedge.associativity(1).getEdgeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 6 );

  xfldBedge.associativity(2).getEdgeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 5 );

  xfldBedge.associativity(3).getEdgeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 7 );

  xfldBedge.associativity(4).getEdgeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 8 );

  xfldBedge.associativity(5).getEdgeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );

  BOOST_CHECK_EQUAL( xfldBedge.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBedge.getGroupRight(), -1 );

  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(0), 3 );
  BOOST_CHECK_EQUAL( xfldBedge.getElementRight(0), -1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceRight(0).trace, -1 );

  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(1), 3 );
  BOOST_CHECK_EQUAL( xfldBedge.getElementRight(1), -1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(1).trace, 1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceRight(1).trace, -1 );

  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(2), 1 );
  BOOST_CHECK_EQUAL( xfldBedge.getElementRight(2), -1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(2).trace, 1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceRight(2).trace, -1 );

  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(3), 1 );
  BOOST_CHECK_EQUAL( xfldBedge.getElementRight(3), -1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(3).trace, 2 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceRight(3).trace, -1 );

  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(4), 2 );
  BOOST_CHECK_EQUAL( xfldBedge.getElementRight(4), -1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(4).trace, 2 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceRight(4).trace, -1 );

  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(5), 2 );
  BOOST_CHECK_EQUAL( xfldBedge.getElementRight(5), -1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(5).trace, 0 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceRight(5).trace, -1 );


  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  // Check that boundary trace coordinates match
  for_each_InteriorFieldTraceGroup_Cell<TopoD2>::apply(
      CheckInteriorTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nInteriorTraceGroups()-1) ), xfld, xfld );

  for_each_BoundaryFieldTraceGroup_Cell<TopoD2>::apply(
      CheckBoundaryTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_4Triangle_X2_Lagrange_test )
{
  typedef std::array<int,3> Int3;

  XField2D_4Triangle_X1_1Group xfld_X1(BasisFunctionCategory_Lagrange); //Linear mesh

  XField<PhysD2,TopoD2> xfld(xfld_X1, 2); //Construct Q2 mesh from linear mesh

  BOOST_CHECK_EQUAL( xfld.nDOF(), 15 );

  //Check the edge values
  BOOST_CHECK_EQUAL( xfld.DOF(0)[0],  0.5 );  BOOST_CHECK_EQUAL( xfld.DOF(0)[1],  0.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(1)[0],  0.0 );  BOOST_CHECK_EQUAL( xfld.DOF(1)[1],  0.5 );
  BOOST_CHECK_EQUAL( xfld.DOF(2)[0], -0.5 );  BOOST_CHECK_EQUAL( xfld.DOF(2)[1],  0.5 );
  BOOST_CHECK_EQUAL( xfld.DOF(3)[0],  0.5 );  BOOST_CHECK_EQUAL( xfld.DOF(3)[1], -0.5 );
  BOOST_CHECK_EQUAL( xfld.DOF(4)[0],  0.5 );  BOOST_CHECK_EQUAL( xfld.DOF(4)[1],  0.5 );
  BOOST_CHECK_EQUAL( xfld.DOF(5)[0],  1.0 );  BOOST_CHECK_EQUAL( xfld.DOF(5)[1],  0.5 );
  BOOST_CHECK_EQUAL( xfld.DOF(6)[0],  1.0 );  BOOST_CHECK_EQUAL( xfld.DOF(6)[1], -0.5 );
  BOOST_CHECK_EQUAL( xfld.DOF(7)[0],  0.5 );  BOOST_CHECK_EQUAL( xfld.DOF(7)[1],  1.0 );
  BOOST_CHECK_EQUAL( xfld.DOF(8)[0], -0.5 );  BOOST_CHECK_EQUAL( xfld.DOF(8)[1],  1.0 );

  //Check the node values
  BOOST_CHECK_EQUAL( xfld.DOF( 9)[0],  0 );  BOOST_CHECK_EQUAL( xfld.DOF( 9)[1],  0 );
  BOOST_CHECK_EQUAL( xfld.DOF(10)[0],  1 );  BOOST_CHECK_EQUAL( xfld.DOF(10)[1],  0 );
  BOOST_CHECK_EQUAL( xfld.DOF(11)[0],  0 );  BOOST_CHECK_EQUAL( xfld.DOF(11)[1],  1 );
  BOOST_CHECK_EQUAL( xfld.DOF(12)[0],  1 );  BOOST_CHECK_EQUAL( xfld.DOF(12)[1],  1 );
  BOOST_CHECK_EQUAL( xfld.DOF(13)[0], -1 );  BOOST_CHECK_EQUAL( xfld.DOF(13)[1],  1 );
  BOOST_CHECK_EQUAL( xfld.DOF(14)[0],  1 );  BOOST_CHECK_EQUAL( xfld.DOF(14)[1], -1 );

  // area field variable

  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Triangle) );

  const XField<PhysD2,TopoD2>::FieldCellGroupType<Triangle>& xfldArea = xfld.getCellGroup<Triangle>(0);

  int nodeMap[3];
  int edgeMap[3];

  //Node DOFs
  xfldArea.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0],  9 );
  BOOST_CHECK_EQUAL( nodeMap[1], 10 );
  BOOST_CHECK_EQUAL( nodeMap[2], 11 );

  xfldArea.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 12 );
  BOOST_CHECK_EQUAL( nodeMap[1], 11 );
  BOOST_CHECK_EQUAL( nodeMap[2], 10 );

  xfldArea.associativity(2).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 11 );
  BOOST_CHECK_EQUAL( nodeMap[1], 13 );
  BOOST_CHECK_EQUAL( nodeMap[2],  9 );

  xfldArea.associativity(3).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 10 );
  BOOST_CHECK_EQUAL( nodeMap[1],  9 );
  BOOST_CHECK_EQUAL( nodeMap[2], 14 );

  //Edge DOFs
  xfldArea.associativity(0).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0], 4 );
  BOOST_CHECK_EQUAL( edgeMap[1], 1 );
  BOOST_CHECK_EQUAL( edgeMap[2], 0 );

  xfldArea.associativity(1).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0], 4 );
  BOOST_CHECK_EQUAL( edgeMap[1], 5 );
  BOOST_CHECK_EQUAL( edgeMap[2], 7 );

  xfldArea.associativity(2).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0], 2 );
  BOOST_CHECK_EQUAL( edgeMap[1], 1 );
  BOOST_CHECK_EQUAL( edgeMap[2], 8 );

  xfldArea.associativity(3).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0], 3 );
  BOOST_CHECK_EQUAL( edgeMap[1], 6 );
  BOOST_CHECK_EQUAL( edgeMap[2], 0 );

  Int3 edgeSign;

  edgeSign = xfldArea.associativity(0).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  edgeSign = xfldArea.associativity(1).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], -1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  edgeSign = xfldArea.associativity(2).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], -1 );
  BOOST_CHECK_EQUAL( edgeSign[2], +1 );

  edgeSign = xfldArea.associativity(3).edgeSign();
  BOOST_CHECK_EQUAL( edgeSign[0], +1 );
  BOOST_CHECK_EQUAL( edgeSign[1], +1 );
  BOOST_CHECK_EQUAL( edgeSign[2], -1 );

  // interior-edge field variable

  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld.getInteriorTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldIedge = xfld.getInteriorTraceGroup<Line>(0);

  //Node DOFs
  xfldIedge.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 10 );
  BOOST_CHECK_EQUAL( nodeMap[1], 11 );

  xfldIedge.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 11 );
  BOOST_CHECK_EQUAL( nodeMap[1],  9 );

  xfldIedge.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0],  9 );
  BOOST_CHECK_EQUAL( nodeMap[1], 10 );

  //Edge DOFs
  xfldIedge.associativity(0).getEdgeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 4 );

  xfldIedge.associativity(1).getEdgeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 );

  xfldIedge.associativity(2).getEdgeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 );

  // interior edge-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldIedge.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldIedge.getGroupRight(), 0 );

  BOOST_CHECK_EQUAL( xfldIedge.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldIedge.getElementRight(0), 1 );
  BOOST_CHECK_EQUAL( xfldIedge.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldIedge.getCanonicalTraceRight(0).trace, 0 );

  BOOST_CHECK_EQUAL( xfldIedge.getElementLeft(1), 0 );
  BOOST_CHECK_EQUAL( xfldIedge.getElementRight(1), 2 );
  BOOST_CHECK_EQUAL( xfldIedge.getCanonicalTraceLeft(1).trace, 1 );
  BOOST_CHECK_EQUAL( xfldIedge.getCanonicalTraceRight(1).trace, 1 );

  BOOST_CHECK_EQUAL( xfldIedge.getElementLeft(2), 0 );
  BOOST_CHECK_EQUAL( xfldIedge.getElementRight(2), 3 );
  BOOST_CHECK_EQUAL( xfldIedge.getCanonicalTraceLeft(2).trace, 2 );
  BOOST_CHECK_EQUAL( xfldIedge.getCanonicalTraceRight(2).trace, 2 );

  // boundary-edge field variable
  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 1 );

  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Line>(0).topoTypeID() == typeid(Line) );

  const XField<PhysD2,TopoD2>::FieldTraceGroupType<Line>& xfldBedge = xfld.getBoundaryTraceGroup<Line>(0);

  //Node DOFs
  xfldBedge.associativity(0).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0],  9 );
  BOOST_CHECK_EQUAL( nodeMap[1], 14 );

  xfldBedge.associativity(1).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 14 );
  BOOST_CHECK_EQUAL( nodeMap[1], 10 );

  xfldBedge.associativity(2).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 10 );
  BOOST_CHECK_EQUAL( nodeMap[1], 12 );

  xfldBedge.associativity(3).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 12 );
  BOOST_CHECK_EQUAL( nodeMap[1], 11 );

  xfldBedge.associativity(4).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 11 );
  BOOST_CHECK_EQUAL( nodeMap[1], 13 );

  xfldBedge.associativity(5).getNodeGlobalMapping( nodeMap, 2 );
  BOOST_CHECK_EQUAL( nodeMap[0], 13 );
  BOOST_CHECK_EQUAL( nodeMap[1],  9 );

  //Edge DOFs
  xfldBedge.associativity(0).getEdgeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 3 );

  xfldBedge.associativity(1).getEdgeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 6 );

  xfldBedge.associativity(2).getEdgeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 5 );

  xfldBedge.associativity(3).getEdgeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 7 );

  xfldBedge.associativity(4).getEdgeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 8 );

  xfldBedge.associativity(5).getEdgeGlobalMapping( nodeMap, 1 );
  BOOST_CHECK_EQUAL( nodeMap[0], 2 );

  BOOST_CHECK_EQUAL( xfldBedge.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBedge.getGroupRight(), -1 );

  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(0), 3 );
  BOOST_CHECK_EQUAL( xfldBedge.getElementRight(0), -1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceRight(0).trace, -1 );

  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(1), 3 );
  BOOST_CHECK_EQUAL( xfldBedge.getElementRight(1), -1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(1).trace, 1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceRight(1).trace, -1 );

  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(2), 1 );
  BOOST_CHECK_EQUAL( xfldBedge.getElementRight(2), -1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(2).trace, 1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceRight(2).trace, -1 );

  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(3), 1 );
  BOOST_CHECK_EQUAL( xfldBedge.getElementRight(3), -1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(3).trace, 2 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceRight(3).trace, -1 );

  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(4), 2 );
  BOOST_CHECK_EQUAL( xfldBedge.getElementRight(4), -1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(4).trace, 2 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceRight(4).trace, -1 );

  BOOST_CHECK_EQUAL( xfldBedge.getElementLeft(5), 2 );
  BOOST_CHECK_EQUAL( xfldBedge.getElementRight(5), -1 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceLeft(5).trace, 0 );
  BOOST_CHECK_EQUAL( xfldBedge.getCanonicalTraceRight(5).trace, -1 );


  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  // Check that boundary trace coordinates match
  for_each_InteriorFieldTraceGroup_Cell<TopoD2>::apply(
      CheckInteriorTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nInteriorTraceGroups()-1) ), xfld, xfld );

  for_each_BoundaryFieldTraceGroup_Cell<TopoD2>::apply(
      CheckBoundaryTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_CheckTrace_Hierarchical_test )
{
  const Real small_tol = 1e-12;
  const Real close_tol = 5e-11;

  for ( int order = 1; order <= BasisFunctionArea_Triangle_HierarchicalPMax; order++)
  {
    XField2D_4Triangle_X1_1Group xfld_X1; //Linear mesh

    //Construct curved mesh from linear mesh
    XField<PhysD2,TopoD2> xfld(xfld_X1, order, BasisFunctionCategory_Hierarchical);

    // Check that interior and boundary trace coordinates match
    for_each_InteriorFieldTraceGroup_Cell<TopoD2>::apply(
        CheckInteriorTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nInteriorTraceGroups()-1) ), xfld, xfld );

    for_each_BoundaryFieldTraceGroup_Cell<TopoD2>::apply(
        CheckBoundaryTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_CheckTrace_Lagrange_test )
{
  const Real small_tol = 1e-12;
  const Real close_tol = 5e-11;

  for ( int order = 1; order <= BasisFunctionArea_Triangle_LagrangePMax; order++)
  {
    XField2D_4Triangle_X1_1Group xfld_X1; //Linear mesh

    //Construct curved mesh from linear mesh
    XField<PhysD2,TopoD2> xfld(xfld_X1, order, BasisFunctionCategory_Lagrange);

    // Check that interior and boundary trace coordinates match
    for_each_InteriorFieldTraceGroup_Cell<TopoD2>::apply(
        CheckInteriorTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nInteriorTraceGroups()-1) ), xfld, xfld );

    for_each_BoundaryFieldTraceGroup_Cell<TopoD2>::apply(
        CheckBoundaryTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_Parallel_Lagrange_test )
{
  mpi::communicator world;

  const Real small_tol = 1e-12;
  const Real close_tol = 5e-11;

  for ( int order = 1; order <= BasisFunctionArea_Triangle_LagrangePMax; order++)
  {
    XField2D_Box_Triangle_Lagrange_X1 xfld_X1(world, 3, 3); //Linear mesh

    //Construct curved mesh from linear mesh
    XField<PhysD2,TopoD2> xfld(xfld_X1, order, BasisFunctionCategory_Lagrange);

    // Check that interior and boundary trace coordinates match
    for_each_InteriorFieldTraceGroup_Cell<TopoD2>::apply(
        CheckInteriorTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nInteriorTraceGroups()-1) ), xfld, xfld );

    for_each_BoundaryFieldTraceGroup_Cell<TopoD2>::apply(
        CheckBoundaryTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );

    // check the cellID matches
    BOOST_REQUIRE_EQUAL(xfld_X1.nCellGroups(), xfld.nCellGroups());

    for (int group = 0; group < xfld_X1.nCellGroups(); group++)
    {
      const std::vector<int>& cellIDs_X1 = xfld_X1.cellIDs(group);
      const std::vector<int>& cellIDs    = xfld.cellIDs(group);

      BOOST_REQUIRE_EQUAL(cellIDs_X1.size(), cellIDs.size());

      for (std::size_t ielem = 0; ielem < cellIDs.size(); ielem++)
        BOOST_CHECK_EQUAL(cellIDs_X1[ielem], cellIDs[ielem]);
    }


    // check the BoundaryTraceID matches
    BOOST_REQUIRE_EQUAL(xfld_X1.nBoundaryTraceGroups(), xfld.nBoundaryTraceGroups());

    for (int group = 0; group < xfld_X1.nBoundaryTraceGroups(); group++)
    {
      const std::vector<int>& boundaryIDs_X1 = xfld_X1.boundaryTraceIDs(group);
      const std::vector<int>& boundaryIDs    = xfld.boundaryTraceIDs(group);

      BOOST_REQUIRE_EQUAL(boundaryIDs_X1.size(), boundaryIDs.size());

      for (std::size_t ielem = 0; ielem < boundaryIDs.size(); ielem++)
        BOOST_CHECK_EQUAL(boundaryIDs_X1[ielem], boundaryIDs[ielem]);
    }

  }
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
