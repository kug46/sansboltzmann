// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// XField3D_Curved_Tet_btest
// testing of higher order XField3D tet grids
//
// Note: unit grids tested in "unit/UnitGrids/UnitGrids_btest.cpp"

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/linspace.h"

#include "BasisFunction/BasisFunctionArea_Triangle.h"
#include "BasisFunction/BasisFunctionArea_Quad.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron.h"
#include "BasisFunction/TraceToCellRefCoord.h"
#include "BasisFunction/ElementEdges.h"

#include "Field/XFieldVolume.h"
#include "Field/Field_CG/Field_CG_Topology.h"

#include "unit/UnitGrids/XField3D_1Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_2Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_5Tet_X1_1Group_AllOrientations.h"
#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"

#include "unit/Field/XField3D_CheckTraceCoord3D_btest.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


//############################################################################//
BOOST_AUTO_TEST_SUITE( XField3D_Curved_Tet_test_suite )

//--------------------CHECKING HIGHER ORDER GRIDS-----------------------------//

std::vector<int> getEdgeOrdering_1Tet(const int order)
{
  std::set<std::pair<int,int>> edges;
  const int (*EdgeNodes)[ Line::NNode ] = ElementEdges<Tet>::EdgeNodes;

  std::map<std::pair<int,int>,bool> edgeReverse;

  for (int iedge = 0; iedge < Tet::NEdge; iedge++)
  {
    // Use the fact that nodeMap is identity for a single Tet
    int node0 = EdgeNodes[iedge][0];
    int node1 = EdgeNodes[iedge][1];

    if (node0 < node1) //key is {node0, node1}
    {
      edges.insert({node0, node1});
      edgeReverse[{node0, node1}] = false;
    }
    else //key is {node1, node0}
    {
      edges.insert({node1, node0});
      edgeReverse[{node1, node0}] = true;
    }
  }

  //go through all edges and construct the DOF indexing
  const int nDOFperEdge = TopologyDOF_CG<Line>::count(order); //(order - 1) for lines
  int edgeDOF_index = 0;
  std::map<std::pair<int,int>,std::vector<int>> edgeDOFsMap;

  for ( const std::pair<int,int>& edge : edges )
  {
    // create the indexes for the edge DOFs
    for (int i = 0; i < nDOFperEdge; i++)
      edgeDOFsMap[edge].push_back(edgeDOF_index++);

    if (edgeReverse[edge])
      std::reverse(edgeDOFsMap[edge].begin(), edgeDOFsMap[edge].end());
  }

  std::vector<int> edgeDOForder;
  for (int iedge = 0; iedge < Tet::NEdge; iedge++)
  {
    // Use the fact that nodeMap is identity for a single Tet
    int node0 = EdgeNodes[iedge][0];
    int node1 = EdgeNodes[iedge][1];

    std::pair<int,int> key;
    if (node0 < node1) //key is {node0, node1}
    {
      key = {node0, node1};
    }
    else //key is {node1, node0}
    {
      key = {node1, node0};
    }

    // flatten the edge DOFs
    for (int i = 0; i < nDOFperEdge; i++)
      edgeDOForder.push_back(edgeDOFsMap.at(key)[i]);
  }

  return edgeDOForder;
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_1Tet_X2_Hierarchical_test )
{
  typedef std::array<int,4> Int4;

  XField3D_1Tet_X1_1Group xfld_X1; //Linear mesh

  const int order = 2;

  XField<PhysD3,TopoD3> xfld(xfld_X1,order); //Construct Q2 mesh from linear mesh

//  xfld.dump(3,std::cout);

  BOOST_CHECK_EQUAL( xfld.nDOF(), (order+1)*(order+2)*(order+3)/6 );

  //Check the node values
  for (int i = 0; i < 6; i++)
  {
    //All higher-order node values need to be zero since the Q2 mesh is also still essentially linear
    BOOST_CHECK_EQUAL( xfld.DOF(i)[0], 0);
    BOOST_CHECK_EQUAL( xfld.DOF(i)[1], 0);
    BOOST_CHECK_EQUAL( xfld.DOF(i)[2], 0);
  }
  BOOST_CHECK_EQUAL( xfld.DOF(6)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(6)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(6)[2], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(7)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(7)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(7)[2], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(8)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(8)[1], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(8)[2], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(9)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(9)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(9)[2], 1 );

  // volume field variable

  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Tet) );

  std::vector<int> edgeDOForder = getEdgeOrdering_1Tet(order);

  const int nEdgeDOF = 6*(order - 1);
  const int nNodeDOF = 4;

  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldVol = xfld.getCellGroup<Tet>(0);

  int edgeMap[nEdgeDOF];
  int nodeMap[nNodeDOF];

  //Edge DOFs
  xfldVol.associativity(0).getEdgeGlobalMapping( edgeMap, 6 );
  for (int n = 0; n < nEdgeDOF; n++)
    BOOST_CHECK_EQUAL( edgeMap[n], edgeDOForder[n] );

  //Node DOFs
  xfldVol.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 6 );
  BOOST_CHECK_EQUAL( nodeMap[1], 7 );
  BOOST_CHECK_EQUAL( nodeMap[2], 8 );
  BOOST_CHECK_EQUAL( nodeMap[3], 9 );

  Int4 faceSign;

  faceSign = xfldVol.associativity(0).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], +1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], +1 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 0 );

  // boundary-edge field variable

  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 4 );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(1).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(2).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(3).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup0 = xfld.getBoundaryTraceGroup<Triangle>(0);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup1 = xfld.getBoundaryTraceGroup<Triangle>(1);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup2 = xfld.getBoundaryTraceGroup<Triangle>(2);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup3 = xfld.getBoundaryTraceGroup<Triangle>(3);

  //Edge DOFs
  xfldBTraceGroup0.associativity(0).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0], 5 );
  BOOST_CHECK_EQUAL( edgeMap[1], 4 );
  BOOST_CHECK_EQUAL( edgeMap[2], 3 );

  xfldBTraceGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0], 5 );
  BOOST_CHECK_EQUAL( edgeMap[1], 1 );
  BOOST_CHECK_EQUAL( edgeMap[2], 2 );

  xfldBTraceGroup2.associativity(0).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0], 4 );
  BOOST_CHECK_EQUAL( edgeMap[1], 2 );
  BOOST_CHECK_EQUAL( edgeMap[2], 0 );

  xfldBTraceGroup3.associativity(0).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0], 3 );
  BOOST_CHECK_EQUAL( edgeMap[1], 0 );
  BOOST_CHECK_EQUAL( edgeMap[2], 1 );


  //Node DOFs
  xfldBTraceGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 7 );
  BOOST_CHECK_EQUAL( nodeMap[1], 8 );
  BOOST_CHECK_EQUAL( nodeMap[2], 9 );

  xfldBTraceGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 6 );
  BOOST_CHECK_EQUAL( nodeMap[1], 9 );
  BOOST_CHECK_EQUAL( nodeMap[2], 8 );

  xfldBTraceGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 6 );
  BOOST_CHECK_EQUAL( nodeMap[1], 7 );
  BOOST_CHECK_EQUAL( nodeMap[2], 9 );

  xfldBTraceGroup3.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 6 );
  BOOST_CHECK_EQUAL( nodeMap[1], 8 );
  BOOST_CHECK_EQUAL( nodeMap[2], 7 );


  // boundary face-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.getGroupLeft(), 0 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.getElementLeft(0), 0 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceLeft(0).trace, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getCanonicalTraceLeft(0).trace, 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.getCanonicalTraceLeft(0).trace, 3 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.getCanonicalTraceLeft(0).orientation, 1 );


  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  // Check that boundary trace coordinates match
  for_each_BoundaryFieldTraceGroup_Cell<TopoD3>::apply(
      CheckBoundaryTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );

  //Curve the P2 xfld by setting edge DOFs to some random nonzero value
  const int nEdges = Tet::NEdge;
  for (int k = 0; k < nEdges; k++) xfld.DOF(k) = 0.1*(k+1);

  // Check that boundary trace coordinates match
  for_each_BoundaryFieldTraceGroup_Cell<TopoD3>::apply(
      CheckBoundaryTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_5Tet_AllOrientations_X2_1Group_test )
{
  typedef std::array<int,4> Int4;

  XField3D_5Tet_X1_1Group_AllOrientations xfld_X1(-1); //Linear mesh

  XField<PhysD3,TopoD3> xfld(xfld_X1,2); //Construct Q2 mesh from linear mesh

//  xfld.dump(3,std::cout);

  BOOST_CHECK_EQUAL( xfld.nDOF(), 26 );

  //Check the node values
  for (int i=0; i<18; i++)
  {
    //All higher-order node values need to be zero since the Q2 mesh is also still essentially linear
    BOOST_CHECK_EQUAL( xfld.DOF(i)[0], 0);
    BOOST_CHECK_EQUAL( xfld.DOF(i)[1], 0);
    BOOST_CHECK_EQUAL( xfld.DOF(i)[2], 0);
  }
  BOOST_CHECK_EQUAL( xfld.DOF(18)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(18)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(18)[2], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(19)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(19)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(19)[2], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(20)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(20)[1], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(20)[2], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(21)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(21)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(21)[2], 1 );
  BOOST_CHECK_EQUAL( xfld.DOF(22)[0], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(22)[1], 1 );  BOOST_CHECK_EQUAL( xfld.DOF(22)[2], 1 );
  BOOST_CHECK_EQUAL( xfld.DOF(23)[0],-1 );  BOOST_CHECK_EQUAL( xfld.DOF(23)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(23)[2], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(24)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(24)[1],-1 );  BOOST_CHECK_EQUAL( xfld.DOF(24)[2], 0 );
  BOOST_CHECK_EQUAL( xfld.DOF(25)[0], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(25)[1], 0 );  BOOST_CHECK_EQUAL( xfld.DOF(25)[2],-1 );

  // volume field variable

  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Tet) );

  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldVol = xfld.getCellGroup<Tet>(0);
  BOOST_CHECK_EQUAL( xfldVol.nElem(), 5 );

  int nodeMap[4];
  int edgeMap[6];

  //Edge DOFs
  xfldVol.associativity(0).getEdgeGlobalMapping( edgeMap, 6 );
  BOOST_CHECK_EQUAL( edgeMap[0], 11 );
  BOOST_CHECK_EQUAL( edgeMap[1],  7 );
  BOOST_CHECK_EQUAL( edgeMap[2],  6 );
  BOOST_CHECK_EQUAL( edgeMap[3],  1 );
  BOOST_CHECK_EQUAL( edgeMap[4],  2 );
  BOOST_CHECK_EQUAL( edgeMap[5],  0 );

  xfldVol.associativity(1).getEdgeGlobalMapping( edgeMap, 6 );
  BOOST_CHECK_EQUAL( edgeMap[0], 15 );
  BOOST_CHECK_EQUAL( edgeMap[1], 12 );
  BOOST_CHECK_EQUAL( edgeMap[2], 11 );
  BOOST_CHECK_EQUAL( edgeMap[3],  7 );
  BOOST_CHECK_EQUAL( edgeMap[4],  8 );
  BOOST_CHECK_EQUAL( edgeMap[5],  6 );

  xfldVol.associativity(2).getEdgeGlobalMapping( edgeMap, 6 );
  BOOST_CHECK_EQUAL( edgeMap[0], 13 );
  BOOST_CHECK_EQUAL( edgeMap[1], 16 );
  BOOST_CHECK_EQUAL( edgeMap[2], 11 );
  BOOST_CHECK_EQUAL( edgeMap[3],  1 );
  BOOST_CHECK_EQUAL( edgeMap[4],  3 );
  BOOST_CHECK_EQUAL( edgeMap[5],  2 );

  xfldVol.associativity(3).getEdgeGlobalMapping( edgeMap, 6 );
  BOOST_CHECK_EQUAL( edgeMap[0], 17 );
  BOOST_CHECK_EQUAL( edgeMap[1],  9 );
  BOOST_CHECK_EQUAL( edgeMap[2],  7 );
  BOOST_CHECK_EQUAL( edgeMap[3],  2 );
  BOOST_CHECK_EQUAL( edgeMap[4],  4 );
  BOOST_CHECK_EQUAL( edgeMap[5],  0 );

  xfldVol.associativity(4).getEdgeGlobalMapping( edgeMap, 6 );
  BOOST_CHECK_EQUAL( edgeMap[0], 10 );
  BOOST_CHECK_EQUAL( edgeMap[1], 14 );
  BOOST_CHECK_EQUAL( edgeMap[2],  6 );
  BOOST_CHECK_EQUAL( edgeMap[3],  0 );
  BOOST_CHECK_EQUAL( edgeMap[4],  5 );
  BOOST_CHECK_EQUAL( edgeMap[5],  1 );


  //Node DOFs
  xfldVol.associativity(0).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 18 );
  BOOST_CHECK_EQUAL( nodeMap[1], 19 );
  BOOST_CHECK_EQUAL( nodeMap[2], 20 );
  BOOST_CHECK_EQUAL( nodeMap[3], 21 );

  xfldVol.associativity(1).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 19 );
  BOOST_CHECK_EQUAL( nodeMap[1], 20 );
  BOOST_CHECK_EQUAL( nodeMap[2], 21 );
  BOOST_CHECK_EQUAL( nodeMap[3], 22 );

  xfldVol.associativity(2).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 18 );
  BOOST_CHECK_EQUAL( nodeMap[1], 21 );
  BOOST_CHECK_EQUAL( nodeMap[2], 20 );
  BOOST_CHECK_EQUAL( nodeMap[3], 23 );

  xfldVol.associativity(3).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 18 );
  BOOST_CHECK_EQUAL( nodeMap[1], 19 );
  BOOST_CHECK_EQUAL( nodeMap[2], 21 );
  BOOST_CHECK_EQUAL( nodeMap[3], 24 );

  xfldVol.associativity(4).getNodeGlobalMapping( nodeMap, 4 );
  BOOST_CHECK_EQUAL( nodeMap[0], 18 );
  BOOST_CHECK_EQUAL( nodeMap[1], 20 );
  BOOST_CHECK_EQUAL( nodeMap[2], 19 );
  BOOST_CHECK_EQUAL( nodeMap[3], 25 );


  Int4 faceSign;

  faceSign = xfldVol.associativity(0).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], +1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], +1 );

  faceSign = xfldVol.associativity(1).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], +1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], -1 );

  faceSign = xfldVol.associativity(2).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], +1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], -1 );

  faceSign = xfldVol.associativity(3).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], +1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], -1 );

  faceSign = xfldVol.associativity(4).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], +1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], -1 );

  // interior-edge field variable

  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 1 );
  BOOST_REQUIRE( xfld.getInteriorTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldITraceGroup = xfld.getInteriorTraceGroup<Triangle>(0);
  BOOST_CHECK_EQUAL( xfldITraceGroup.nElem(), 4 );


  //Edge DOFs
  xfldITraceGroup.associativity(0).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0], 11 );
  BOOST_CHECK_EQUAL( edgeMap[1],  7 );
  BOOST_CHECK_EQUAL( edgeMap[2],  6 );

  xfldITraceGroup.associativity(1).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0], 11 );
  BOOST_CHECK_EQUAL( edgeMap[1],  1 );
  BOOST_CHECK_EQUAL( edgeMap[2],  2 );

  xfldITraceGroup.associativity(2).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0],  7 );
  BOOST_CHECK_EQUAL( edgeMap[1],  2 );
  BOOST_CHECK_EQUAL( edgeMap[2],  0 );

  xfldITraceGroup.associativity(3).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0],  6 );
  BOOST_CHECK_EQUAL( edgeMap[1],  0 );
  BOOST_CHECK_EQUAL( edgeMap[2],  1 );


  //Node DOFs
  xfldITraceGroup.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 19 );
  BOOST_CHECK_EQUAL( nodeMap[1], 20 );
  BOOST_CHECK_EQUAL( nodeMap[2], 21 );

  xfldITraceGroup.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 18 );
  BOOST_CHECK_EQUAL( nodeMap[1], 21 );
  BOOST_CHECK_EQUAL( nodeMap[2], 20 );

  xfldITraceGroup.associativity(2).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 18 );
  BOOST_CHECK_EQUAL( nodeMap[1], 19 );
  BOOST_CHECK_EQUAL( nodeMap[2], 21 );

  xfldITraceGroup.associativity(3).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 18 );
  BOOST_CHECK_EQUAL( nodeMap[1], 20 );
  BOOST_CHECK_EQUAL( nodeMap[2], 19 );


  // interior face-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldITraceGroup.getGroupLeft() , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getGroupRight(), 0 );

  BOOST_CHECK_EQUAL( xfldITraceGroup.getElementLeft(0) , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getElementLeft(1) , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getElementLeft(2) , 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getElementLeft(3) , 0 );

  BOOST_CHECK_EQUAL( xfldITraceGroup.getElementRight(0), 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getElementRight(1), 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getElementRight(2), 3 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getElementRight(3), 4 );

  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceLeft(1).trace, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceLeft(2).trace, 2 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceLeft(3).trace, 3 );

  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceLeft(1).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceLeft(2).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceLeft(3).orientation, 1 );

  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceRight(0).trace, 3 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceRight(1).trace, 3 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceRight(2).trace, 3 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceRight(3).trace, 3 );

  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceRight(0).orientation, -1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceRight(1).orientation, -1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceRight(2).orientation, -1 );
  BOOST_CHECK_EQUAL( xfldITraceGroup.getCanonicalTraceRight(3).orientation, -1 );


  // boundary-edge field variable

  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 1 );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup0 = xfld.getBoundaryTraceGroup<Triangle>(0);
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.nElem(), 12 );

  //Edge DOFs
  xfldBTraceGroup0.associativity(0).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0], 15 );
  BOOST_CHECK_EQUAL( edgeMap[1], 12 );
  BOOST_CHECK_EQUAL( edgeMap[2], 11 );

  xfldBTraceGroup0.associativity(1).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0], 15 );
  BOOST_CHECK_EQUAL( edgeMap[1],  7 );
  BOOST_CHECK_EQUAL( edgeMap[2],  8 );

  xfldBTraceGroup0.associativity(2).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0], 12 );
  BOOST_CHECK_EQUAL( edgeMap[1],  8 );
  BOOST_CHECK_EQUAL( edgeMap[2],  6 );

  xfldBTraceGroup0.associativity(3).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0], 13 );
  BOOST_CHECK_EQUAL( edgeMap[1], 16 );
  BOOST_CHECK_EQUAL( edgeMap[2], 11 );

  xfldBTraceGroup0.associativity(4).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0], 13 );
  BOOST_CHECK_EQUAL( edgeMap[1],  1 );
  BOOST_CHECK_EQUAL( edgeMap[2],  3 );

  xfldBTraceGroup0.associativity(5).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0], 16 );
  BOOST_CHECK_EQUAL( edgeMap[1],  3 );
  BOOST_CHECK_EQUAL( edgeMap[2],  2 );

  xfldBTraceGroup0.associativity(6).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0], 17 );
  BOOST_CHECK_EQUAL( edgeMap[1],  9 );
  BOOST_CHECK_EQUAL( edgeMap[2],  7 );

  xfldBTraceGroup0.associativity(7).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0], 17 );
  BOOST_CHECK_EQUAL( edgeMap[1],  2 );
  BOOST_CHECK_EQUAL( edgeMap[2],  4 );

  xfldBTraceGroup0.associativity(8).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0],  9 );
  BOOST_CHECK_EQUAL( edgeMap[1],  4 );
  BOOST_CHECK_EQUAL( edgeMap[2],  0 );

  xfldBTraceGroup0.associativity(9).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0], 10 );
  BOOST_CHECK_EQUAL( edgeMap[1], 14 );
  BOOST_CHECK_EQUAL( edgeMap[2],  6 );

  xfldBTraceGroup0.associativity(10).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0], 10 );
  BOOST_CHECK_EQUAL( edgeMap[1],  0 );
  BOOST_CHECK_EQUAL( edgeMap[2],  5 );

  xfldBTraceGroup0.associativity(11).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0], 14 );
  BOOST_CHECK_EQUAL( edgeMap[1],  5 );
  BOOST_CHECK_EQUAL( edgeMap[2],  1 );


  //Node DOFs
  xfldBTraceGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 20 );
  BOOST_CHECK_EQUAL( nodeMap[1], 21 );
  BOOST_CHECK_EQUAL( nodeMap[2], 22 );

  xfldBTraceGroup0.associativity(1).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 19 );
  BOOST_CHECK_EQUAL( nodeMap[1], 22 );
  BOOST_CHECK_EQUAL( nodeMap[2], 21 );

  xfldBTraceGroup0.associativity(2).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 19 );
  BOOST_CHECK_EQUAL( nodeMap[1], 20 );
  BOOST_CHECK_EQUAL( nodeMap[2], 22 );

  xfldBTraceGroup0.associativity(3).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 21 );
  BOOST_CHECK_EQUAL( nodeMap[1], 20 );
  BOOST_CHECK_EQUAL( nodeMap[2], 23 );

  xfldBTraceGroup0.associativity(4).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 18 );
  BOOST_CHECK_EQUAL( nodeMap[1], 23 );
  BOOST_CHECK_EQUAL( nodeMap[2], 20 );

  xfldBTraceGroup0.associativity(5).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 18 );
  BOOST_CHECK_EQUAL( nodeMap[1], 21 );
  BOOST_CHECK_EQUAL( nodeMap[2], 23 );

  xfldBTraceGroup0.associativity(6).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 19 );
  BOOST_CHECK_EQUAL( nodeMap[1], 21 );
  BOOST_CHECK_EQUAL( nodeMap[2], 24 );

  xfldBTraceGroup0.associativity(7).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 18 );
  BOOST_CHECK_EQUAL( nodeMap[1], 24 );
  BOOST_CHECK_EQUAL( nodeMap[2], 21 );

  xfldBTraceGroup0.associativity(8).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 18 );
  BOOST_CHECK_EQUAL( nodeMap[1], 19 );
  BOOST_CHECK_EQUAL( nodeMap[2], 24 );

  xfldBTraceGroup0.associativity(9).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 20 );
  BOOST_CHECK_EQUAL( nodeMap[1], 19 );
  BOOST_CHECK_EQUAL( nodeMap[2], 25 );

  xfldBTraceGroup0.associativity(10).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 18 );
  BOOST_CHECK_EQUAL( nodeMap[1], 25 );
  BOOST_CHECK_EQUAL( nodeMap[2], 19 );

  xfldBTraceGroup0.associativity(11).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 18 );
  BOOST_CHECK_EQUAL( nodeMap[1], 20 );
  BOOST_CHECK_EQUAL( nodeMap[2], 25 );


  // boundary face-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getGroupLeft(), 0 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementLeft(0), 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementLeft(1), 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementLeft(2), 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementLeft(3), 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementLeft(4), 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementLeft(5), 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementLeft(6), 3 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementLeft(7), 3 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementLeft(8), 3 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementLeft(9), 4 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementLeft(10), 4 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementLeft(11), 4 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(1).trace, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(2).trace, 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(3).trace, 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(4).trace, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(5).trace, 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(6).trace, 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(7).trace, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(8).trace, 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(9).trace, 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(10).trace, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(11).trace, 2 );

  for (int k=0; k<12; k++)
    BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(k).orientation, 1 );


  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  // Check that interior and boundary trace coordinates match
  for_each_InteriorFieldTraceGroup_Cell<TopoD3>::apply(
      CheckInteriorTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nInteriorTraceGroups()-1) ), xfld, xfld );

  for_each_BoundaryFieldTraceGroup_Cell<TopoD3>::apply(
      CheckBoundaryTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_CheckTrace_Hierarchical_test )
{
  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  mpi::communicator comm = world.split(world.rank());

  const Real small_tol = 1e-12;
  const Real close_tol = 5e-11;

  for ( int order = 1; order <= BasisFunctionVolume_Tet_HierarchicalPMax; order++)
  {
    for (int orientation : {-3,-2,-1, 1, 2, 3})
    {
      XField3D_5Tet_X1_1Group_AllOrientations xfld_X1(orientation); //Linear mesh

      //Construct curved mesh from linear mesh
      XField<PhysD3,TopoD3> xfld(xfld_X1, order, BasisFunctionCategory_Hierarchical);

      // Check that interior and boundary trace coordinates match
      for_each_InteriorFieldTraceGroup_Cell<TopoD3>::apply(
          CheckInteriorTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nInteriorTraceGroups()-1) ), xfld, xfld );

      for_each_BoundaryFieldTraceGroup_Cell<TopoD3>::apply(
          CheckBoundaryTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );
    }

    XField3D_Box_Tet_X1 xfld_X1(comm, 3,3,3); //Linear mesh

    //Construct curved mesh from linear mesh
    XField<PhysD3,TopoD3> xfld(xfld_X1, order, BasisFunctionCategory_Hierarchical);

    // Check that interior and boundary trace coordinates match
    for_each_InteriorFieldTraceGroup_Cell<TopoD3>::apply(
        CheckInteriorTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nInteriorTraceGroups()-1) ), xfld, xfld );

    for_each_BoundaryFieldTraceGroup_Cell<TopoD3>::apply(
        CheckBoundaryTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_1Tet_X2_Lagrange_test )
{
  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  typedef std::array<int,4> Int4;

  XField3D_1Tet_X1_1Group xfld_X1; //Linear mesh

  const int order = 2;

  XField<PhysD3,TopoD3> xfld(xfld_X1,order,BasisFunctionCategory_Lagrange); //Construct Q2 mesh from linear mesh

//  xfld.dump(3,std::cout);

  BOOST_CHECK_EQUAL( xfld.nDOF(), (order+1)*(order+2)*(order+3)/6 );

  // Get the Lagrange nodes to compare with the curved grid
  std::vector<Real> coord_s, coord_t, coord_u;
  BasisFunctionVolumeBase<Tet>::LagrangeP2->coordinates( coord_s, coord_t, coord_u );

  std::vector<int> edgeDOForder = getEdgeOrdering_1Tet(order);

  const int nCellDOF = (order - 1)*(order - 2)*(order - 3)/6;
  const int nFaceDOF = 4*(order - 1)*(order - 2)/2;
  const int nEdgeDOF = 6*(order - 1);
  const int nNodeDOF = 4;

  const int offset_edgeDOF = nCellDOF + nFaceDOF;
  const int offset_nodeDOF = nCellDOF + nFaceDOF + nEdgeDOF;

  // Note that the reference element is sorted
  // Nodes, Edge, Face
  // The grid DOFs are sorted
  // Face, Edge, Node

  // Check edge DOFs
  for (int i = 0; i < nEdgeDOF; i++)
  {
    SANS_CHECK_CLOSE( coord_s[i+nNodeDOF], xfld.DOF(edgeDOForder[i]+offset_edgeDOF)[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( coord_t[i+nNodeDOF], xfld.DOF(edgeDOForder[i]+offset_edgeDOF)[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( coord_u[i+nNodeDOF], xfld.DOF(edgeDOForder[i]+offset_edgeDOF)[2], small_tol, close_tol );
  }

  //Check the node DOFs
  for (int i = 0; i < nNodeDOF; i++)
  {
    SANS_CHECK_CLOSE( coord_s[i], xfld.DOF(i+offset_nodeDOF)[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( coord_t[i], xfld.DOF(i+offset_nodeDOF)[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( coord_u[i], xfld.DOF(i+offset_nodeDOF)[2], small_tol, close_tol );
  }

  // volume field variable

  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Tet) );


  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldVol = xfld.getCellGroup<Tet>(0);

  int edgeMap[nEdgeDOF];
  int nodeMap[nNodeDOF];

  //Edge DOFs
  xfldVol.associativity(0).getEdgeGlobalMapping( edgeMap, nEdgeDOF );
  for (int n = 0; n < nEdgeDOF; n++)
    BOOST_CHECK_EQUAL( edgeMap[n], edgeDOForder[n] + nCellDOF + nFaceDOF );

  //Node DOFs
  xfldVol.associativity(0).getNodeGlobalMapping( nodeMap, nNodeDOF );
  for (int n = 0; n < nNodeDOF; n++)
    BOOST_CHECK_EQUAL( nodeMap[n], n + nCellDOF + nFaceDOF + nEdgeDOF );

  Int4 faceSign;

  faceSign = xfldVol.associativity(0).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], +1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], +1 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 0 );

  // boundary-edge field variable

  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 4 );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(1).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(2).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(3).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup0 = xfld.getBoundaryTraceGroup<Triangle>(0);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup1 = xfld.getBoundaryTraceGroup<Triangle>(1);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup2 = xfld.getBoundaryTraceGroup<Triangle>(2);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup3 = xfld.getBoundaryTraceGroup<Triangle>(3);

  //Node DOFs
  xfldBTraceGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[2], 3 + nCellDOF + nFaceDOF + nEdgeDOF );

  xfldBTraceGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 + nCellDOF + nFaceDOF + nEdgeDOF );

  xfldBTraceGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[2], 3 + nCellDOF + nFaceDOF + nEdgeDOF );

  xfldBTraceGroup3.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[2], 1 + nCellDOF + nFaceDOF + nEdgeDOF );

  //Edge DOFs
  xfldBTraceGroup0.associativity(0).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0], 9 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[1], 8 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[2], 7 + nCellDOF + nFaceDOF - nNodeDOF );

  xfldBTraceGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0], 9 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[1], 5 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[2], 6 + nCellDOF + nFaceDOF - nNodeDOF );

  xfldBTraceGroup2.associativity(0).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0], 8 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[1], 6 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[2], 4 + nCellDOF + nFaceDOF - nNodeDOF );

  xfldBTraceGroup3.associativity(0).getEdgeGlobalMapping( edgeMap, 3 );
  BOOST_CHECK_EQUAL( edgeMap[0], 7 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[1], 4 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[2], 5 + nCellDOF + nFaceDOF - nNodeDOF );

  // boundary face-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.getGroupLeft(), 0 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.getElementLeft(0), 0 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceLeft(0).trace, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getCanonicalTraceLeft(0).trace, 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.getCanonicalTraceLeft(0).trace, 3 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.getCanonicalTraceLeft(0).orientation, 1 );

  // Check that boundary trace coordinates match
  for_each_BoundaryFieldTraceGroup_Cell<TopoD3>::apply(
      CheckBoundaryTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_1Tet_X3_Lagrange_test )
{
  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  typedef std::array<int,4> Int4;

  //Linear mesh
  XField3D_1Tet_X1_1Group xfld_X1;

  const int order = 3;

  //Construct Q3 mesh from linear mesh
  XField<PhysD3,TopoD3> xfld(xfld_X1, order, BasisFunctionCategory_Lagrange);

  BOOST_CHECK_EQUAL( xfld.nDOF(), (order+1)*(order+2)*(order+3)/6 );

  // Get the Lagrange nodes to compare with the curved grid
  std::vector<Real> coord_s, coord_t, coord_u;
  BasisFunctionVolumeBase<Tet>::LagrangeP3->coordinates( coord_s, coord_t, coord_u );

  std::vector<int> edgeDOForder = getEdgeOrdering_1Tet(order);

  const int nCellDOF = (order - 1)*(order - 2)*(order - 3)/6;
  const int nFaceDOF = 4*(order - 1)*(order - 2)/2;
  const int nEdgeDOF = 6*(order - 1);
  const int nNodeDOF = 4;

  const int offset_faceDOF = nCellDOF;
  const int offset_edgeDOF = offset_faceDOF + nFaceDOF;
  const int offset_nodeDOF = offset_edgeDOF + nEdgeDOF;

  int faceMap[nFaceDOF];
  int edgeMap[nEdgeDOF];
  int nodeMap[nNodeDOF];

  // Note that the reference element is sorted
  // Nodes, Edge, Face
  // The grid DOFs are sorted
  // Face, Edge, Node

  // volume field variable

  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Tet) );


  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldVol = xfld.getCellGroup<Tet>(0);

  //Face DOFs
  xfldVol.associativity(0).getFaceGlobalMapping( faceMap, nFaceDOF );
  BOOST_CHECK_EQUAL( faceMap[0], 3 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[1], 2 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[2], 1 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[3], 0 + nCellDOF );

  //Edge DOFs
  xfldVol.associativity(0).getEdgeGlobalMapping( edgeMap, nEdgeDOF );
  for (int n = 0; n < nEdgeDOF; n++)
    BOOST_CHECK_EQUAL( edgeMap[n], edgeDOForder[n] + nCellDOF + nFaceDOF );

  //Node DOFs
  xfldVol.associativity(0).getNodeGlobalMapping( nodeMap, nNodeDOF );
  for (int n = 0; n < nNodeDOF; n++)
    BOOST_CHECK_EQUAL( nodeMap[n], n + nCellDOF + nFaceDOF + nEdgeDOF );


  // Check face DOFs
  for (int i = 0; i < nFaceDOF; i++)
  {
    SANS_CHECK_CLOSE( coord_s[i+nNodeDOF+nEdgeDOF], xfld.DOF(faceMap[i])[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( coord_t[i+nNodeDOF+nEdgeDOF], xfld.DOF(faceMap[i])[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( coord_u[i+nNodeDOF+nEdgeDOF], xfld.DOF(faceMap[i])[2], small_tol, close_tol );
  }

  // Check edge DOFs
  for (int i = 0; i < nEdgeDOF; i++)
  {
    SANS_CHECK_CLOSE( coord_s[i+nNodeDOF], xfld.DOF(edgeDOForder[i]+offset_edgeDOF)[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( coord_t[i+nNodeDOF], xfld.DOF(edgeDOForder[i]+offset_edgeDOF)[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( coord_u[i+nNodeDOF], xfld.DOF(edgeDOForder[i]+offset_edgeDOF)[2], small_tol, close_tol );
  }

  //Check the node DOFs
  for (int i = 0; i < nNodeDOF; i++)
  {
    SANS_CHECK_CLOSE( coord_s[i], xfld.DOF(i+offset_nodeDOF)[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( coord_t[i], xfld.DOF(i+offset_nodeDOF)[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( coord_u[i], xfld.DOF(i+offset_nodeDOF)[2], small_tol, close_tol );
  }


  Int4 faceSign;

  faceSign = xfldVol.associativity(0).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], +1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], +1 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 0 );

  // boundary-edge field variable

  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 4 );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(1).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(2).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(3).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup0 = xfld.getBoundaryTraceGroup<Triangle>(0);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup1 = xfld.getBoundaryTraceGroup<Triangle>(1);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup2 = xfld.getBoundaryTraceGroup<Triangle>(2);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup3 = xfld.getBoundaryTraceGroup<Triangle>(3);

  //Node DOFs
  xfldBTraceGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[2], 3 + nCellDOF + nFaceDOF + nEdgeDOF );

  xfldBTraceGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 + nCellDOF + nFaceDOF + nEdgeDOF );

  xfldBTraceGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[2], 3 + nCellDOF + nFaceDOF + nEdgeDOF );

  xfldBTraceGroup3.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[2], 1 + nCellDOF + nFaceDOF + nEdgeDOF );

  //Edge DOFs
  xfldBTraceGroup0.associativity(0).getEdgeGlobalMapping( edgeMap, 6 );
  BOOST_CHECK_EQUAL( edgeMap[0], 14 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[1], 15 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[2], 13 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[3], 12 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[4], 10 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[5], 11 + nCellDOF + nFaceDOF - nNodeDOF );

  xfldBTraceGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 6 );
  BOOST_CHECK_EQUAL( edgeMap[0], 15 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[1], 14 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[2],  7 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[3],  6 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[4],  8 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[5],  9 + nCellDOF + nFaceDOF - nNodeDOF );

  xfldBTraceGroup2.associativity(0).getEdgeGlobalMapping( edgeMap, 6 );
  BOOST_CHECK_EQUAL( edgeMap[0], 12 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[1], 13 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[2],  9 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[3],  8 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[4],  4 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[5],  5 + nCellDOF + nFaceDOF - nNodeDOF );

  xfldBTraceGroup3.associativity(0).getEdgeGlobalMapping( edgeMap, 6 );
  BOOST_CHECK_EQUAL( edgeMap[0], 11 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[1], 10 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[2],  5 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[3],  4 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[4],  6 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[5],  7 + nCellDOF + nFaceDOF - nNodeDOF );

  // boundary face-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.getGroupLeft(), 0 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.getElementLeft(0), 0 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceLeft(0).trace, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getCanonicalTraceLeft(0).trace, 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.getCanonicalTraceLeft(0).trace, 3 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.getCanonicalTraceLeft(0).orientation, 1 );

  // Check that boundary trace coordinates match
  for_each_BoundaryFieldTraceGroup_Cell<TopoD3>::apply(
      CheckBoundaryTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_1Tet_X4_Lagrange_test )
{
  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  typedef std::array<int,4> Int4;

  //Linear mesh
  XField3D_1Tet_X1_1Group xfld_X1;

  const int order = 4;

  //Construct Q3 mesh from linear mesh
  XField<PhysD3,TopoD3> xfld(xfld_X1, order, BasisFunctionCategory_Lagrange);

  BOOST_CHECK_EQUAL( xfld.nDOF(), (order+1)*(order+2)*(order+3)/6 );

  // Get the Lagrange nodes to compare with the curved grid
  std::vector<Real> coord_s, coord_t, coord_u;
  BasisFunctionVolumeBase<Tet>::LagrangeP4->coordinates( coord_s, coord_t, coord_u );

  std::vector<int> edgeDOForder = getEdgeOrdering_1Tet(order);

  const int nCellDOF = (order - 1)*(order - 2)*(order - 3)/6;
  const int nFaceDOF = 4*(order - 1)*(order - 2)/2;
  const int nEdgeDOF = 6*(order - 1);
  const int nNodeDOF = 4;

  const int offset_cellDOF = 0;
  const int offset_faceDOF = nCellDOF;
  const int offset_edgeDOF = offset_faceDOF + nFaceDOF;
  const int offset_nodeDOF = offset_edgeDOF + nEdgeDOF;

  int cellMap[nCellDOF];
  int faceMap[nFaceDOF];
  int edgeMap[nEdgeDOF];
  int nodeMap[nNodeDOF];

  // Note that the reference element is sorted
  // Nodes, Edge, Face
  // The grid DOFs are sorted
  // Face, Edge, Node

  // volume field variable

  BOOST_CHECK_EQUAL( xfld.nCellGroups(), 1 );
  BOOST_REQUIRE( xfld.getCellGroupBase(0).topoTypeID() == typeid(Tet) );


  const XField<PhysD3,TopoD3>::FieldCellGroupType<Tet>& xfldVol = xfld.getCellGroup<Tet>(0);

  //Cell DOFs
  xfldVol.associativity(0).getCellGlobalMapping( cellMap, nCellDOF );
  for (int n = 0; n < nCellDOF; n++)
    BOOST_CHECK_EQUAL( cellMap[n], n );

  //Face DOFs
  xfldVol.associativity(0).getFaceGlobalMapping( faceMap, nFaceDOF );
  BOOST_CHECK_EQUAL( faceMap[ 0],  9 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[ 1], 10 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[ 2], 11 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[ 3],  6 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[ 4],  8 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[ 5],  7 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[ 6],  3 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[ 7],  4 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[ 8],  5 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[ 9],  0 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[10],  2 + nCellDOF );
  BOOST_CHECK_EQUAL( faceMap[11],  1 + nCellDOF );

  //Edge DOFs
  xfldVol.associativity(0).getEdgeGlobalMapping( edgeMap, nEdgeDOF );
  BOOST_CHECK_EQUAL( edgeMap[ 0], 15 + nCellDOF + nFaceDOF );
  BOOST_CHECK_EQUAL( edgeMap[ 1], 16 + nCellDOF + nFaceDOF );
  BOOST_CHECK_EQUAL( edgeMap[ 2], 17 + nCellDOF + nFaceDOF );
  BOOST_CHECK_EQUAL( edgeMap[ 3], 14 + nCellDOF + nFaceDOF );
  BOOST_CHECK_EQUAL( edgeMap[ 4], 13 + nCellDOF + nFaceDOF );
  BOOST_CHECK_EQUAL( edgeMap[ 5], 12 + nCellDOF + nFaceDOF );
  BOOST_CHECK_EQUAL( edgeMap[ 6],  9 + nCellDOF + nFaceDOF );
  BOOST_CHECK_EQUAL( edgeMap[ 7], 10 + nCellDOF + nFaceDOF );
  BOOST_CHECK_EQUAL( edgeMap[ 8], 11 + nCellDOF + nFaceDOF );
  BOOST_CHECK_EQUAL( edgeMap[ 9],  5 + nCellDOF + nFaceDOF );
  BOOST_CHECK_EQUAL( edgeMap[10],  4 + nCellDOF + nFaceDOF );
  BOOST_CHECK_EQUAL( edgeMap[11],  3 + nCellDOF + nFaceDOF );
  BOOST_CHECK_EQUAL( edgeMap[12],  6 + nCellDOF + nFaceDOF );
  BOOST_CHECK_EQUAL( edgeMap[13],  7 + nCellDOF + nFaceDOF );
  BOOST_CHECK_EQUAL( edgeMap[14],  8 + nCellDOF + nFaceDOF );
  BOOST_CHECK_EQUAL( edgeMap[15],  0 + nCellDOF + nFaceDOF );
  BOOST_CHECK_EQUAL( edgeMap[16],  1 + nCellDOF + nFaceDOF );
  BOOST_CHECK_EQUAL( edgeMap[17],  2 + nCellDOF + nFaceDOF );

  //Node DOFs
  xfldVol.associativity(0).getNodeGlobalMapping( nodeMap, nNodeDOF );
  for (int n = 0; n < nNodeDOF; n++)
    BOOST_CHECK_EQUAL( nodeMap[n], n + nCellDOF + nFaceDOF + nEdgeDOF );


  // Check cell DOFs
  for (int i = 0; i < nCellDOF; i++)
  {
    SANS_CHECK_CLOSE( coord_s[i+nNodeDOF+nEdgeDOF+nFaceDOF], xfld.DOF(i+offset_cellDOF)[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( coord_t[i+nNodeDOF+nEdgeDOF+nFaceDOF], xfld.DOF(i+offset_cellDOF)[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( coord_u[i+nNodeDOF+nEdgeDOF+nFaceDOF], xfld.DOF(i+offset_cellDOF)[2], small_tol, close_tol );
  }

  // Check face DOFs
  for (int i = 0; i < nFaceDOF; i++)
  {
    SANS_CHECK_CLOSE( coord_s[i+nNodeDOF+nEdgeDOF], xfld.DOF(faceMap[i])[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( coord_t[i+nNodeDOF+nEdgeDOF], xfld.DOF(faceMap[i])[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( coord_u[i+nNodeDOF+nEdgeDOF], xfld.DOF(faceMap[i])[2], small_tol, close_tol );
  }

  // Check edge DOFs
  for (int i = 0; i < nEdgeDOF; i++)
  {
    SANS_CHECK_CLOSE( coord_s[i+nNodeDOF], xfld.DOF(edgeDOForder[i]+offset_edgeDOF)[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( coord_t[i+nNodeDOF], xfld.DOF(edgeDOForder[i]+offset_edgeDOF)[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( coord_u[i+nNodeDOF], xfld.DOF(edgeDOForder[i]+offset_edgeDOF)[2], small_tol, close_tol );
  }

  //Check the node DOFs
  for (int i = 0; i < nNodeDOF; i++)
  {
    SANS_CHECK_CLOSE( coord_s[i], xfld.DOF(i+offset_nodeDOF)[0], small_tol, close_tol );
    SANS_CHECK_CLOSE( coord_t[i], xfld.DOF(i+offset_nodeDOF)[1], small_tol, close_tol );
    SANS_CHECK_CLOSE( coord_u[i], xfld.DOF(i+offset_nodeDOF)[2], small_tol, close_tol );
  }


  Int4 faceSign;

  faceSign = xfldVol.associativity(0).faceSign();
  BOOST_CHECK_EQUAL( faceSign[0], +1 );
  BOOST_CHECK_EQUAL( faceSign[1], +1 );
  BOOST_CHECK_EQUAL( faceSign[2], +1 );
  BOOST_CHECK_EQUAL( faceSign[3], +1 );

  // interior-edge field variable
  BOOST_CHECK_EQUAL( xfld.nInteriorTraceGroups(), 0 );

  // boundary-edge field variable

  BOOST_CHECK_EQUAL( xfld.nBoundaryTraceGroups(), 4 );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(0).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(1).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(2).topoTypeID() == typeid(Triangle) );
  BOOST_REQUIRE( xfld.getBoundaryTraceGroup<Triangle>(3).topoTypeID() == typeid(Triangle) );

  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup0 = xfld.getBoundaryTraceGroup<Triangle>(0);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup1 = xfld.getBoundaryTraceGroup<Triangle>(1);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup2 = xfld.getBoundaryTraceGroup<Triangle>(2);
  const XField<PhysD3,TopoD3>::FieldTraceGroupType<Triangle>& xfldBTraceGroup3 = xfld.getBoundaryTraceGroup<Triangle>(3);

  //Node DOFs
  xfldBTraceGroup0.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 1 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[2], 3 + nCellDOF + nFaceDOF + nEdgeDOF );

  xfldBTraceGroup1.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[1], 3 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[2], 2 + nCellDOF + nFaceDOF + nEdgeDOF );

  xfldBTraceGroup2.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[1], 1 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[2], 3 + nCellDOF + nFaceDOF + nEdgeDOF );

  xfldBTraceGroup3.associativity(0).getNodeGlobalMapping( nodeMap, 3 );
  BOOST_CHECK_EQUAL( nodeMap[0], 0 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[1], 2 + nCellDOF + nFaceDOF + nEdgeDOF );
  BOOST_CHECK_EQUAL( nodeMap[2], 1 + nCellDOF + nFaceDOF + nEdgeDOF );

  //Edge DOFs
  xfldBTraceGroup0.associativity(0).getEdgeGlobalMapping( edgeMap, 9 );
  BOOST_CHECK_EQUAL( edgeMap[0], 19 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[1], 20 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[2], 21 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[3], 18 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[4], 17 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[5], 16 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[6], 13 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[7], 14 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[8], 15 + nCellDOF + nFaceDOF - nNodeDOF );

  xfldBTraceGroup1.associativity(0).getEdgeGlobalMapping( edgeMap, 9 );
  BOOST_CHECK_EQUAL( edgeMap[0], 21 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[1], 20 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[2], 19 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[3],  9 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[4],  8 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[5],  7 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[6], 10 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[7], 11 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[8], 12 + nCellDOF + nFaceDOF - nNodeDOF );

  xfldBTraceGroup2.associativity(0).getEdgeGlobalMapping( edgeMap, 9 );
  BOOST_CHECK_EQUAL( edgeMap[0], 16 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[1], 17 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[2], 18 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[3], 12 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[4], 11 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[5], 10 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[6],  4 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[7],  5 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[8],  6 + nCellDOF + nFaceDOF - nNodeDOF );

  xfldBTraceGroup3.associativity(0).getEdgeGlobalMapping( edgeMap, 9 );
  BOOST_CHECK_EQUAL( edgeMap[0], 15 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[1], 14 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[2], 13 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[3],  6 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[4],  5 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[5],  4 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[6],  7 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[7],  8 + nCellDOF + nFaceDOF - nNodeDOF );
  BOOST_CHECK_EQUAL( edgeMap[8],  9 + nCellDOF + nFaceDOF - nNodeDOF );

  //Face DOFs
  xfldBTraceGroup0.associativity(0).getFaceGlobalMapping( faceMap, 3 );
  BOOST_CHECK_EQUAL( faceMap[0], 31 + nCellDOF - nNodeDOF - nEdgeDOF );
  BOOST_CHECK_EQUAL( faceMap[1], 32 + nCellDOF - nNodeDOF - nEdgeDOF );
  BOOST_CHECK_EQUAL( faceMap[2], 33 + nCellDOF - nNodeDOF - nEdgeDOF );

  xfldBTraceGroup1.associativity(0).getFaceGlobalMapping( faceMap, 3 );
  BOOST_CHECK_EQUAL( faceMap[0], 28 + nCellDOF - nNodeDOF - nEdgeDOF );
  BOOST_CHECK_EQUAL( faceMap[1], 30 + nCellDOF - nNodeDOF - nEdgeDOF );
  BOOST_CHECK_EQUAL( faceMap[2], 29 + nCellDOF - nNodeDOF - nEdgeDOF );

  xfldBTraceGroup2.associativity(0).getFaceGlobalMapping( faceMap, 3 );
  BOOST_CHECK_EQUAL( faceMap[0], 25 + nCellDOF - nNodeDOF - nEdgeDOF );
  BOOST_CHECK_EQUAL( faceMap[1], 26 + nCellDOF - nNodeDOF - nEdgeDOF );
  BOOST_CHECK_EQUAL( faceMap[2], 27 + nCellDOF - nNodeDOF - nEdgeDOF );

  xfldBTraceGroup3.associativity(0).getFaceGlobalMapping( faceMap, 3 );
  BOOST_CHECK_EQUAL( faceMap[0], 22 + nCellDOF - nNodeDOF - nEdgeDOF );
  BOOST_CHECK_EQUAL( faceMap[1], 24 + nCellDOF - nNodeDOF - nEdgeDOF );
  BOOST_CHECK_EQUAL( faceMap[2], 23 + nCellDOF - nNodeDOF - nEdgeDOF );

  // boundary face-to-cell connectivity
  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getGroupLeft(), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.getGroupLeft(), 0 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getElementLeft(0), 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.getElementLeft(0), 0 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(0).trace, 0 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceLeft(0).trace, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getCanonicalTraceLeft(0).trace, 2 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.getCanonicalTraceLeft(0).trace, 3 );

  BOOST_CHECK_EQUAL( xfldBTraceGroup0.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup1.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup2.getCanonicalTraceLeft(0).orientation, 1 );
  BOOST_CHECK_EQUAL( xfldBTraceGroup3.getCanonicalTraceLeft(0).orientation, 1 );

  // Check that boundary trace coordinates match
  for_each_BoundaryFieldTraceGroup_Cell<TopoD3>::apply(
      CheckBoundaryTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_CheckTrace_Lagrange_test )
{
  // global communicator
  mpi::communicator world;

  // split the communicator accross all processors
  //mpi::communicator comm = world.split(world.rank());

  const Real small_tol = 1e-12;
  const Real close_tol = 5e-11;

  for ( int order = 1; order <= BasisFunctionVolume_Tet_LagrangePMax; order++)
  {
    for (int orientation : {-3,-2,-1, 1, 2, 3})
    {
      XField3D_5Tet_X1_1Group_AllOrientations xfld_X1(orientation); //Linear mesh

      //Construct curved mesh from linear mesh
      XField<PhysD3,TopoD3> xfld(xfld_X1, order, BasisFunctionCategory_Lagrange);

      // Check that interior and boundary trace coordinates match
      for_each_InteriorFieldTraceGroup_Cell<TopoD3>::apply(
          CheckInteriorTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nInteriorTraceGroups()-1) ), xfld, xfld );

      for_each_BoundaryFieldTraceGroup_Cell<TopoD3>::apply(
          CheckBoundaryTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );
    }

    XField3D_Box_Tet_X1 xfld_X1(world, 3,3,3); //Linear mesh

    //Construct curved mesh from linear mesh
    XField<PhysD3,TopoD3> xfld(xfld_X1, order, BasisFunctionCategory_Lagrange);

    // Check that interior and boundary trace coordinates match
    for_each_InteriorFieldTraceGroup_Cell<TopoD3>::apply(
        CheckInteriorTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nInteriorTraceGroups()-1) ), xfld, xfld );

    for_each_BoundaryFieldTraceGroup_Cell<TopoD3>::apply(
        CheckBoundaryTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );


    // check the cellID matches
    BOOST_REQUIRE_EQUAL(xfld_X1.nCellGroups(), xfld.nCellGroups());

    for (int group = 0; group < xfld_X1.nCellGroups(); group++)
    {
      const std::vector<int>& cellIDs_X1 = xfld_X1.cellIDs(group);
      const std::vector<int>& cellIDs    = xfld.cellIDs(group);

      BOOST_REQUIRE_EQUAL(cellIDs_X1.size(), cellIDs.size());

      for (std::size_t ielem = 0; ielem < cellIDs.size(); ielem++)
        BOOST_CHECK_EQUAL(cellIDs_X1[ielem], cellIDs[ielem]);
    }

    // check the BoundaryTraceID matches
    BOOST_REQUIRE_EQUAL(xfld_X1.nBoundaryTraceGroups(), xfld.nBoundaryTraceGroups());

    for (int group = 0; group < xfld_X1.nBoundaryTraceGroups(); group++)
    {
      const std::vector<int>& boundaryIDs_X1 = xfld_X1.boundaryTraceIDs(group);
      const std::vector<int>& boundaryIDs    = xfld.boundaryTraceIDs(group);

      BOOST_REQUIRE_EQUAL(boundaryIDs_X1.size(), boundaryIDs.size());

      for (std::size_t ielem = 0; ielem < boundaryIDs.size(); ielem++)
        BOOST_CHECK_EQUAL(boundaryIDs_X1[ielem], boundaryIDs[ielem]);
    }

  }
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
