// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ProjectGlobalField_btest
// testing of solution projections of global Fields

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <kdtree++/kdtree.hpp>

#include <limits>

#include "tools/SANSnumerics.h"     // Real

#include "BasisFunction/BasisFunctionArea_Triangle_Lagrange.h"

#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"
#include "pde/AnalyticFunction/ScalarFunction3D.h"
#include "pde/NDConvert/SolnNDConvertSpace1D.h"
#include "pde/NDConvert/SolnNDConvertSpace2D.h"
#include "pde/NDConvert/SolnNDConvertSpace3D.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldVolume_DG_Cell.h"

#include "Field/ProjectSoln/ProjectGlobalField.h"
#include "Field/ProjectSoln/ProjectGlobalField_KDVertex.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"
#include "Field/tools/for_each_CellGroup.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField2D_Box_Triangle_Lagrange_X1.h"
#include "unit/UnitGrids/XField2D_Box_Quad_X1.h"
#include "unit/UnitGrids/XField3D_Box_Tet_X1.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct
namespace SANS
{
}


//############################################################################//
BOOST_AUTO_TEST_SUITE( ProjectGlobalField_test_suite )

struct TestPredicate
{
  typedef PhysD2 PhysDim;

  typedef ProjectGlobalField_KDVertex<PhysDim> KDVertex;

   bool operator()( KDVertex const& node ) const
   {
     int i, j, k;
     node.getInfo(i,j,k);
     return k > 0;  // anything, we are currently testing that it compiles.
   }
};

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( KDtree_nearest_if )
{
  typedef PhysD2 PhysDim;
  static const int D = PhysDim::D;

  typedef ProjectGlobalField_KDVertex<PhysDim> KDVertex;
  typedef KDTree::KDTree<D, KDVertex> KDTreeType;

  KDTreeType tree;

  tree.insert( KDVertex({0.5, 0.5}, 0, 0, 0) );
  tree.insert( KDVertex({0.8, 0.8}, 0, 0, 1) );
  tree.insert( KDVertex({0.3, 0.2}, 0, 0, 2) );
  tree.insert( KDVertex({0.0, 0.0}, 0, 0, 3) );

  KDVertex target({0.5, 0.5}, 0, 0, 3);
  std::pair<KDTreeType::const_iterator,double> found = tree.find_nearest_if(target, std::numeric_limits<Real>::max(), TestPredicate());

  KDVertex::VectorX xCoord = found.first->getCoordinates();
  BOOST_CHECK_EQUAL( 0.3, xCoord[0] );
  BOOST_CHECK_EQUAL( 0.2, xCoord[1] );
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ProjectGlobalField_1D_Line_X1P1 )
{
  typedef SolnNDConvertSpace<PhysD1, ScalarFunction1D_Linear> NDSolutionExact;
  typedef Real ArrayQ;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  Real a0 = 1;
  Real ax = 2;
  NDSolutionExact solnExact(a0, ax);

  // grids
  XField1D xfldFrom(3);
  XField1D xfldTo(5);

  // solution: P1 (aka Q1), which spans the space of the Linear solution
  // the check below assumes Hierarchical basis
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfldFrom(xfldFrom, qorder, BasisFunctionCategory_Hierarchical);
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfldTo  (xfldTo  , qorder, BasisFunctionCategory_Hierarchical);

  for_each_CellGroup<TopoD1>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfldFrom, qfldFrom) );

  ProjectGlobalField(qfldFrom, qfldTo);

  const XField1D              ::template FieldCellGroupType<Line>& xfldCell = xfldTo.getCellGroup<Line>(0);
  const Field<PhysD1, TopoD1, ArrayQ>::template FieldCellGroupType<Line>& qfldCell = qfldTo.getCellGroup<Line>(0);

  typedef XField1D              ::template FieldCellGroupType<Line>::template ElementType<> ElementXFieldClass;
  typedef Field<PhysD1, TopoD1, ArrayQ>::template FieldCellGroupType<Line>::template ElementType<> ElementQFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfldCell.basis() );
  ElementQFieldClass qfldElem( qfldCell.basis() );

  // loop over elements within group
  const int nelem = xfldCell.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    // copy global grid/solution DOFs to element
    xfldCell.getElement( xfldElem, elem );
    qfldCell.getElement( qfldElem, elem );

    BOOST_REQUIRE_EQUAL(xfldElem.nDOF(), qfldElem.nDOF());
    for (int n = 0; n < qfldElem.nDOF(); n++)
    {
      Real exact = solnExact(xfldElem.DOF(n));
      SANS_CHECK_CLOSE( exact, qfldElem.DOF(n), small_tol, close_tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ProjectGlobalField_1D_Line_X1P1_DifferentDomain )
{
  typedef SolnNDConvertSpace<PhysD1, ScalarFunction1D_Linear> NDSolutionExact;
  typedef Real ArrayQ;

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  Real a0 = 1;
  Real ax = 2;
  NDSolutionExact solnExact(a0, ax);

  // grids
  XField1D xfldFrom(3);
  XField1D xfldTo(6);
  xfldTo.DOF(5) = 1.0;
  xfldTo.DOF(6) = 1.05;

  // solution: P1 (aka Q1), which spans the space of the Linear solution
  // the check below assumes Hierarchical basis
  int qorder = 1;
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfldFrom(xfldFrom, qorder, BasisFunctionCategory_Hierarchical);
  Field_DG_Cell<PhysD1, TopoD1, ArrayQ> qfldTo  (xfldTo  , qorder, BasisFunctionCategory_Hierarchical);

  for_each_CellGroup<TopoD1>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfldFrom, qfldFrom) );

  ProjectGlobalField(qfldFrom, qfldTo);

  const XField1D              ::template FieldCellGroupType<Line>& xfldCell = xfldTo.getCellGroup<Line>(0);
  const Field<PhysD1, TopoD1, ArrayQ>::template FieldCellGroupType<Line>& qfldCell = qfldTo.getCellGroup<Line>(0);

  typedef XField1D              ::template FieldCellGroupType<Line>::template ElementType<> ElementXFieldClass;
  typedef Field<PhysD1, TopoD1, ArrayQ>::template FieldCellGroupType<Line>::template ElementType<> ElementQFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfldCell.basis() );
  ElementQFieldClass qfldElem( qfldCell.basis() );

  // loop over elements within group - don't check last element since the solution won't be exact
  const int nelem = xfldCell.nElem();
  for (int elem = 0; elem < nelem - 1; elem++)
  {
    // copy global grid/solution DOFs to element
    xfldCell.getElement( xfldElem, elem );
    qfldCell.getElement( qfldElem, elem );

    BOOST_REQUIRE_EQUAL(xfldElem.nDOF(), qfldElem.nDOF());
    for (int n = 0; n < qfldElem.nDOF(); n++)
    {
      Real exact = solnExact(xfldElem.DOF(n));
      SANS_CHECK_CLOSE( exact, qfldElem.DOF(n), small_tol, close_tol );
    }
  }

  //Check that last element (which is outside the old domain) gets initialized to closest value
  qfldCell.getElement( qfldElem, 5 );
#if 0 // L2 projection
  SANS_CHECK_CLOSE( 2.8591167563965421, qfldElem.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 2.8591167563965421, qfldElem.DOF(1), small_tol, close_tol );
#else // Fit
  SANS_CHECK_CLOSE( 3.0, qfldElem.DOF(0), small_tol, close_tol );
  SANS_CHECK_CLOSE( 3.0, qfldElem.DOF(1), small_tol, close_tol );
#endif
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ProjectGlobalField_2D_Triangle_X1P1 )
{
  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_Linear> NDSolutionExact;
  typedef Real ArrayQ;

  mpi::communicator world;

  Real a0 = 1;
  Real ax = 2;
  Real ay = 3;
  NDSolutionExact solnExact(a0, ax, ay);

  // grids
  XField2D_Box_Triangle_Lagrange_X1 xfldFrom(world,4,5);
  XField2D_Box_Triangle_Lagrange_X1 xfldTo(world,6,3);

  // solution: P1 (aka Q1), which spans the space of the Linear solution
  // the check below assumes Hierarchical basis
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfldFrom(xfldFrom, qorder, BasisFunctionCategory_Hierarchical);
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfldTo  (xfldTo  , qorder, BasisFunctionCategory_Hierarchical);

  for_each_CellGroup<TopoD2>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfldFrom, qfldFrom) );

  ProjectGlobalField(qfldFrom, qfldTo);

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  const XField<PhysD2, TopoD2       >::template FieldCellGroupType<Triangle>& xfldCell = xfldTo.getCellGroup<Triangle>(0);
  const Field<PhysD2, TopoD2, ArrayQ>::template FieldCellGroupType<Triangle>& qfldCell = qfldTo.getCellGroup<Triangle>(0);

  typedef XField<PhysD2, TopoD2       >::template FieldCellGroupType<Triangle>::template ElementType<> ElementXFieldClass;
  typedef Field<PhysD2, TopoD2, ArrayQ>::template FieldCellGroupType<Triangle>::template ElementType<> ElementQFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfldCell.basis() );
  ElementQFieldClass qfldElem( qfldCell.basis() );

  // loop over elements within group
  const int nelem = xfldCell.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    // copy global grid/solution DOFs to element
    xfldCell.getElement( xfldElem, elem );
    qfldCell.getElement( qfldElem, elem );

    BOOST_REQUIRE_EQUAL(xfldElem.nDOF(), qfldElem.nDOF());
    for (int n = 0; n < qfldElem.nDOF(); n++)
    {
      Real exact = solnExact(xfldElem.DOF(n));
      SANS_CHECK_CLOSE( exact, qfldElem.DOF(n), small_tol, close_tol );
    }
  }
}


//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ProjectGlobalField_2D_Triangle_X1P2 )
{
  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_Quadratic> NDSolutionExact;
  typedef Real ArrayQ;

  mpi::communicator world;

  Real a0 = 1;
  Real ax = 1.5;
  Real ay = -0.5;
  Real axx = 2;
  Real ayy = 3;
  Real axy = 4;
  NDSolutionExact solnExact(a0, ax, ay, axx, ayy, axy);

  // grids
  XField2D_Box_Triangle_Lagrange_X1 xfldFrom(world,4,5);
  XField2D_Box_Triangle_Lagrange_X1 xfldTo(world,6,3);

  // solution: P2
  int qorder = 2;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfldFrom(xfldFrom, qorder, BasisFunctionCategory_Legendre);
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfldTo  (xfldTo  , qorder, BasisFunctionCategory_Legendre);

  for_each_CellGroup<TopoD2>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfldFrom, qfldFrom) );

  ProjectGlobalField(qfldFrom, qfldTo);

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  const XField<PhysD2, TopoD2       >::template FieldCellGroupType<Triangle>& xfldCell = xfldTo.getCellGroup<Triangle>(0);
  const Field<PhysD2, TopoD2, ArrayQ>::template FieldCellGroupType<Triangle>& qfldCell = qfldTo.getCellGroup<Triangle>(0);

  typedef XField<PhysD2, TopoD2       >::template FieldCellGroupType<Triangle>::template ElementType<> ElementXFieldClass;
  typedef Field<PhysD2, TopoD2, ArrayQ>::template FieldCellGroupType<Triangle>::template ElementType<> ElementQFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfldCell.basis() );
  ElementQFieldClass qfldElem( qfldCell.basis() );

  std::vector<DLA::VectorS<TopoD2::D,Real>> sRef;
  LagrangeNodes<Triangle>::get(qorder, sRef);

  // loop over elements within group
  const int nelem = xfldCell.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    // copy global grid/solution DOFs to element
    xfldCell.getElement( xfldElem, elem );
    qfldCell.getElement( qfldElem, elem );

    for (std::size_t inode = 0; inode < sRef.size(); inode++)
    {
      ArrayQ q = qfldElem.eval(sRef[inode]);
      Real exact = solnExact(xfldElem.eval(sRef[inode]));
      SANS_CHECK_CLOSE( exact, q, small_tol, close_tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ProjectGlobalField_2D_Quad_X1P1 )
{
  typedef SolnNDConvertSpace<PhysD2, ScalarFunction2D_Linear> NDSolutionExact;
  typedef Real ArrayQ;

  Real a0 = 1;
  Real ax = 2;
  Real ay = 3;
  NDSolutionExact solnExact(a0, ax, ay);

  // grids
  XField2D_Box_Quad_X1 xfldFrom(2,2);
  XField2D_Box_Quad_X1 xfldTo(3,3);

  // solution: P1 (aka Q1), which spans the space of the Linear solution
  // the check below assumes Hierarchical basis
  int qorder = 1;
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfldFrom(xfldFrom, qorder, BasisFunctionCategory_Hierarchical);
  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> qfldTo  (xfldTo  , qorder, BasisFunctionCategory_Hierarchical);

  for_each_CellGroup<TopoD2>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfldFrom, qfldFrom) );

  ProjectGlobalField(qfldFrom, qfldTo);

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-12;

  const XField2D_Box_Quad_X1         ::template FieldCellGroupType<Quad>& xfldCell = xfldTo.getCellGroup<Quad>(0);
  const Field<PhysD2, TopoD2, ArrayQ>::template FieldCellGroupType<Quad>& qfldCell = qfldTo.getCellGroup<Quad>(0);

  typedef XField2D_Box_Quad_X1         ::template FieldCellGroupType<Quad>::template ElementType<> ElementXFieldClass;
  typedef Field<PhysD2, TopoD2, ArrayQ>::template FieldCellGroupType<Quad>::template ElementType<> ElementQFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfldCell.basis() );
  ElementQFieldClass qfldElem( qfldCell.basis() );

  // loop over elements within group
  const int nelem = xfldCell.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    // copy global grid/solution DOFs to element
    xfldCell.getElement( xfldElem, elem );
    qfldCell.getElement( qfldElem, elem );

    BOOST_REQUIRE_EQUAL(xfldElem.nDOF(), qfldElem.nDOF());
    for (int n = 0; n < qfldElem.nDOF(); n++)
    {
      Real exact = solnExact(xfldElem.DOF(n));
      SANS_CHECK_CLOSE( exact, qfldElem.DOF(n), small_tol, close_tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ProjectGlobalField_3D_Tet_X1P1 )
{
  typedef SolnNDConvertSpace<PhysD3, ScalarFunction3D_Linear> NDSolutionExact;
  typedef Real ArrayQ;

  mpi::communicator world;

  Real a0 = 1;
  Real ax = 2;
  Real ay = 3;
  Real az = 4;
  NDSolutionExact solnExact(a0, ax, ay, az);

  // grids
  XField3D_Box_Tet_X1 xfldFrom(world,2,3,4);
  XField3D_Box_Tet_X1 xfldTo(world,5,4,3);

  // solution: P1 (aka Q1), which spans the space of the Linear solution
  // the check below assumes Hierarchical basis
  int qorder = 1;
  Field_DG_Cell<PhysD3, TopoD3, ArrayQ> qfldFrom(xfldFrom, qorder, BasisFunctionCategory_Hierarchical);
  Field_DG_Cell<PhysD3, TopoD3, ArrayQ> qfldTo  (xfldTo  , qorder, BasisFunctionCategory_Hierarchical);

  for_each_CellGroup<TopoD3>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfldFrom, qfldFrom) );

  ProjectGlobalField(qfldFrom, qfldTo);

  const Real small_tol = 1e-12;
  const Real close_tol = 1e-11;

  const XField<PhysD3, TopoD3       >::template FieldCellGroupType<Tet>& xfldCell = xfldTo.getCellGroup<Tet>(0);
  const Field<PhysD3, TopoD3, ArrayQ>::template FieldCellGroupType<Tet>& qfldCell = qfldTo.getCellGroup<Tet>(0);

  typedef XField<PhysD3, TopoD3       >::template FieldCellGroupType<Tet>::template ElementType<> ElementXFieldClass;
  typedef Field<PhysD3, TopoD3, ArrayQ>::template FieldCellGroupType<Tet>::template ElementType<> ElementQFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfldCell.basis() );
  ElementQFieldClass qfldElem( qfldCell.basis() );

  // loop over elements within group
  const int nelem = xfldCell.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    // copy global grid/solution DOFs to element
    xfldCell.getElement( xfldElem, elem );
    qfldCell.getElement( qfldElem, elem );

    BOOST_REQUIRE_EQUAL(xfldElem.nDOF(), qfldElem.nDOF());
    for (int n = 0; n < qfldElem.nDOF(); n++)
    {
      Real exact = solnExact(xfldElem.DOF(n));
      SANS_CHECK_CLOSE( exact, qfldElem.DOF(n), small_tol, close_tol );
    }
  }
}

#if 0 //Disabled because hexes are missing Hessian calculations
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( ProjectGlobalField_3D_Hex_X1P1 )
{
  typedef SolnNDConvertSpace<PhysD3, ScalarFunction3D_Linear> NDSolutionExact;
  typedef Real ArrayQ;

  Real a0 = 1;
  Real ax = 2;
  Real ay = 3;
  Real az = 4;
  NDSolutionExact solnExact(a0, ax, ay, az);

  // grids
  XField3D_Box_Hex_X1 xfldFrom(2,2,2);
  XField3D_Box_Hex_X1 xfldTo(3,3,3);

  // solution: P1 (aka Q1), which spans the space of the Linear solution
  // the check below assumes Hierarchical basis
  int qorder = 1;
  Field_DG_Cell<PhysD3, TopoD3, ArrayQ> qfldFrom(xfldFrom, qorder, BasisFunctionCategory_Hierarchical);
  Field_DG_Cell<PhysD3, TopoD3, ArrayQ> qfldTo  (xfldTo  , qorder, BasisFunctionCategory_Hierarchical);

  for_each_CellGroup<TopoD3>::apply( ProjectSolnCell_Discontinuous(solnExact, {0}), (xfldFrom, qfldFrom) );

  ProjectGlobalField(qfldFrom, qfldTo);

  const Real small_tol = 5e-11;
  const Real close_tol = 5e-11;

  const XField3D_Box_Hex_X1          ::template FieldCellGroupType<Hex>& xfldCell = xfldTo.getCellGroup<Hex>(0);
  const Field<PhysD3, TopoD3, ArrayQ>::template FieldCellGroupType<Hex>& qfldCell = qfldTo.getCellGroup<Hex>(0);

  typedef XField3D_Box_Hex_X1          ::template FieldCellGroupType<Hex>::template ElementType<> ElementXFieldClass;
  typedef Field<PhysD3, TopoD3, ArrayQ>::template FieldCellGroupType<Hex>::template ElementType<> ElementQFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfldCell.basis() );
  ElementQFieldClass qfldElem( qfldCell.basis() );

  // loop over elements within group
  const int nelem = xfldCell.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    // copy global grid/solution DOFs to element
    xfldCell.getElement( xfldElem, elem );
    qfldCell.getElement( qfldElem, elem );

    BOOST_REQUIRE_EQUAL(xfldElem.nDOF(), qfldElem.nDOF());
    for (int n = 0; n < qfldElem.nDOF(); n++)
    {
      Real exact = solnExact(xfldElem.DOF(n));
      SANS_CHECK_CLOSE( exact, qfldElem.DOF(n), small_tol, close_tol );
    }
  }
}
#endif

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
