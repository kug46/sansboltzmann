// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <ostream>

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include "tools/linspace.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#include "Field/XFieldLine.h"   //Needed because we are using line XField
#include "Field/XFieldArea.h"   //Needed because we are using area XField
#include "Field/XFieldVolume.h" //Needed because we are using volume XField

#include "Field/Element/UniqueElemHash.h"

#include "Field/Partition/XField_Lagrange.h"

#include "unit/Field/XField1D_CheckTraceCoord1D_btest.h"
#include "unit/Field/XField2D_CheckTraceCoord2D_btest.h"
#include "unit/Field/XField3D_CheckTraceCoord3D_btest.h"

#include "Field/output_grm.h"
#include "Field/output_Tecplot.h"

using namespace SANS;

//############################################################################//
BOOST_AUTO_TEST_SUITE( XField_Lagrange_test_suite )

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( UniqueTrace_test )
{
  int elem0 = 1;
  int canonicalTrace0 = 2;
  UniqueTrace traceTri0(elem0, canonicalTrace0);

  // check constructor
  BOOST_CHECK_EQUAL(elem0,           traceTri0.cellElem);
  BOOST_CHECK_EQUAL(canonicalTrace0, traceTri0.canonicalTrace);

  UniqueTrace traceTri0_copy(traceTri0);

  // check copy constructor
  BOOST_CHECK_EQUAL(elem0,           traceTri0_copy.cellElem);
  BOOST_CHECK_EQUAL(canonicalTrace0, traceTri0_copy.canonicalTrace);
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( LagrangeElementGroup_test )
{
  int rank = 0;
  int order = 2, nCell = 2;

  LagrangeElementGroup grpTri0( eTriangle, order );

  BOOST_CHECK_EQUAL( 3    , grpTri0.nTracePerElem() );
  BOOST_CHECK_EQUAL( 2    , grpTri0.topologyDim() );
  BOOST_CHECK_EQUAL( order, grpTri0.order() );


  // Check exceptions when adding elements

  // Element index out of bounds
  BOOST_CHECK_THROW( grpTri0.addElement(rank,   -1, {0,1,2}), AssertionException );

  // DOF count incorrect
  BOOST_CHECK_THROW( grpTri0.addElement(rank, 0, {0,1,2,3}), AssertionException );

  // create two triangle elements

  /* Node indexes in the grid

      6-----7-----8
      | \         |
      |   \       |
      3     4     5
      |       \   |
      |         \ |
      0-----1-----2
  */

  /* SANS canonical node indexes for an element

    2
    | \
    |   \
    4     3
    |       \
    |         \
    0-----5-----1
  */

  std::vector<int> triL = {0, 2, 6,  // nodes
                           4,        // edge 1
                           3,        // edge 2
                           1};       // edge 3

  std::vector<int> triR = {8, 6, 2,  // nodes
                           4,        // edge 1
                           5,        // edge 2
                           7};       // edge 3

  grpTri0.addElement(rank, 0, triL); // element 0
  grpTri0.addElement(rank, 1, triR); // element 1

  BOOST_CHECK_EQUAL( nCell, grpTri0.nElem() );

  // all elements are not set and check should be good
  std::stringstream errmsg;
  BOOST_CHECK( grpTri0.check(0, errmsg) == true );

  // extract the element nodes
  std::vector<int> triNodes0 = grpTri0.elemNodes(0);
  std::vector<int> triNodes1 = grpTri0.elemNodes(1);

  BOOST_REQUIRE_EQUAL( 3, triNodes0.size() );
  BOOST_REQUIRE_EQUAL( 3, triNodes1.size() );

  for (int i = 0; i < 3; i++)
  {
    BOOST_CHECK_EQUAL( triL[i], triNodes0[i] );
    BOOST_CHECK_EQUAL( triR[i], triNodes1[i] );
  }

  // check the complete trace DOFs for elem0
  int elem = 0, canonicalTrace;
  std::vector<int> trace0 = grpTri0.traceDOFs(elem, canonicalTrace=0);
  BOOST_REQUIRE_EQUAL( 3, trace0.size() );
  BOOST_CHECK_EQUAL( 2, trace0[0] );  // node0
  BOOST_CHECK_EQUAL( 6, trace0[1] );  // node1
  BOOST_CHECK_EQUAL( 4, trace0[2] );  // edge

  std::vector<int> trace1 = grpTri0.traceDOFs(elem, canonicalTrace=1);
  BOOST_REQUIRE_EQUAL( 3, trace1.size() );
  BOOST_CHECK_EQUAL( 6, trace1[0] );  // node0
  BOOST_CHECK_EQUAL( 0, trace1[1] );  // node1
  BOOST_CHECK_EQUAL( 3, trace1[2] );  // edge

  std::vector<int> trace2 = grpTri0.traceDOFs(elem, canonicalTrace=2);
  BOOST_REQUIRE_EQUAL( 3, trace2.size() );
  BOOST_CHECK_EQUAL( 0, trace2[0] );  // node0
  BOOST_CHECK_EQUAL( 2, trace2[1] );  // node1
  BOOST_CHECK_EQUAL( 1, trace2[2] );  // edge


  // Extract unique traces from elements 0
  LagrangeElementGroup::UniqueTraceVector uniqueTraces0 = grpTri0.uniqueTraces(0);

  BOOST_REQUIRE_EQUAL( 3, uniqueTraces0.size() );

  UniqueElem elemTri0(eLine, std::vector<int>({trace0[0], trace0[1]}));
  UniqueElem elemTri1(eLine, std::vector<int>({trace1[0], trace1[1]}));
  UniqueElem elemTri2(eLine, std::vector<int>({trace2[0], trace2[1]}));

  UniqueTrace traceTri0(elem, canonicalTrace=0);
  UniqueTrace traceTri1(elem, canonicalTrace=1);
  UniqueTrace traceTri2(elem, canonicalTrace=2);

  BOOST_CHECK( uniqueTraces0[0].first == elemTri0 );
  BOOST_CHECK( uniqueTraces0[1].first == elemTri1 );
  BOOST_CHECK( uniqueTraces0[2].first == elemTri2 );

  BOOST_CHECK_EQUAL( traceTri0.cellElem, uniqueTraces0[0].second.cellElem );
  BOOST_CHECK_EQUAL( traceTri1.cellElem, uniqueTraces0[1].second.cellElem );
  BOOST_CHECK_EQUAL( traceTri2.cellElem, uniqueTraces0[2].second.cellElem );

  BOOST_CHECK_EQUAL( traceTri0.canonicalTrace, uniqueTraces0[0].second.canonicalTrace );
  BOOST_CHECK_EQUAL( traceTri1.canonicalTrace, uniqueTraces0[1].second.canonicalTrace );
  BOOST_CHECK_EQUAL( traceTri2.canonicalTrace, uniqueTraces0[2].second.canonicalTrace );

  // Extract elem0 as a unique elements (needed to match boundary elements)
  UniqueElem uniqueTri0 = grpTri0.uniqueElem(0);

  UniqueElem uniqueTri0_true(eTriangle, std::vector<int>({triL[0], triL[1], triL[2]}));

  BOOST_CHECK( uniqueTri0_true == uniqueTri0 );
}

//----------------------------------------------------------------------------//
// create line grid in unit length with ii elements
class XField1D_Line_Lagrange_Q1 : public XField<PhysD1,TopoD1>
{
public:

  XField1D_Line_Lagrange_Q1( mpi::communicator comm, int ii, Real xmin, Real xmax )
    : XField<PhysD1,TopoD1>(comm)
  {
    XField_Lagrange<PhysD1>::VectorX X;

    int order = 1;
    int nnode = (ii*order + 1);
    int nCell = ii;

    XField_Lagrange<PhysD1> xfldin(comm);

    // create the grid-coordinate DOF arrays
    xfldin.sizeDOF( nnode );

    if (comm.rank() == 0)
      for (int i = 0; i < (ii*order + 1); i++)
      {
        X[0] = xmin + (xmax - xmin)*i/Real(ii*order);
        xfldin.addDOF(X);
      }

    // Start the process of adding cells
    xfldin.sizeCells(nCell);

    // Add elements to rank 0
    if (comm.rank() == 0)
    {
      int group = 0;
      for (int i = 0; i < ii; i++)
      {
         /* Node indexes in the grid

               0-----------1
         */

        int n0 = i*order;
        int n1 = n0 + 1;

        std::vector<int> line = {n0, n1}; // nodes

        // add the indices to the group
        xfldin.addCell(group, eLine, order, line);
      }
    }

    // Start the process of adding boundary trace elements
    xfldin.sizeBoundaryTrace(2);

    // Add elements to rank 0
    if (comm.rank() == 0)
    {
      // left boundary
      {
        int group = 0;

        int i = 0;
        int n0 = i*order;

        xfldin.addBoundaryTrace( group, eNode, {n0} );
      }

      // right boundary
      {
        int group = 1;

        int i = ii;
        int n0 = i*order;

        xfldin.addBoundaryTrace( group, eNode, {n0} );
      }
    }

    this->buildFrom( xfldin );
  }
};

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField1D_Line_Lagrange_CheckTrace_Q1 )
{
  // global communicator
  mpi::communicator world;

  int ii = 12;

  XField1D_Line_Lagrange_Q1 xfld(world, ii, 0., 1.);


  BOOST_CHECK_EQUAL( 1, xfld.nCellGroups() );
  if (xfld.getCellGroupBase(0).nElem() > 1)
    BOOST_CHECK_EQUAL( 1, xfld.nInteriorTraceGroups() );
  else
    BOOST_CHECK_EQUAL( 0, xfld.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 2, xfld.nBoundaryTraceGroups() );

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  // check that grid trace is identical for adjacent elements
  if (xfld.nInteriorTraceGroups() > 0)
    for_each_InteriorFieldTraceGroup_Cell<TopoD1>::apply(
        CheckInteriorTraceCoordinates1D(small_tol, close_tol, linspace(0,xfld.nInteriorTraceGroups()-1) ), xfld, xfld );

  for_each_BoundaryFieldTraceGroup_Cell<TopoD1>::apply(
      CheckBoundaryTraceCoordinates1D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );

  //output_Tecplot(xfld, "tmp/test" + std::to_string(world.rank()) + ".dat");
}

//----------------------------------------------------------------------------//
// create triangle grid in unit box with ii x jj x 2 (triangle) elements
class XField2D_Box_Triangle_Lagrange_Q1 : public XField<PhysD2,TopoD2>
{
public:

  XField2D_Box_Triangle_Lagrange_Q1(mpi::communicator comm, int ii, int jj, Real xmin, Real xmax, Real ymin, Real ymax )
    : XField<PhysD2,TopoD2>(comm)
  {
    XField_Lagrange<PhysD2>::VectorX X;

    int order = 1;
    int nnode = (ii*order + 1)*(jj*order + 1);
    int nCell = ii*jj*2;

    Real xdiv = (xmax - xmin)/Real(ii)/10;
    Real ydiv = (ymax - ymin)/Real(ii)/10;

    XField_Lagrange<PhysD2> xfldin(comm);

    // create the wavy grid-coordinate DOF arrays
    xfldin.sizeDOF( nnode );

    if (comm.rank() == 0)
      for (int j = 0; j < (jj*order + 1); j++)
      {
        for (int i = 0; i < (ii*order + 1); i++)
        {
          X[0] = xmin + (xmax - xmin)*i/Real(ii*order) + xdiv*sin(2*PI*j/Real(jj*order));
          X[1] = ymin + (ymax - ymin)*j/Real(jj*order) + ydiv*sin(2*PI*i/Real(ii*order));
          xfldin.addDOF(X);
        }
      }

    // Start the process of adding cells
    xfldin.sizeCells(nCell);

    // Add elements to rank 0
    if (comm.rank() == 0)
    {
      int group = 0;
      for (int j = 0; j < jj; j++)
      {
        for (int i = 0; i < ii; i++)
        {
          /* Node indexes in the grid

              2-----------3
              | \         |
              |   \       |
              |     \     |
              |       \   |
              |         \ |
              0-----------1
          */

          /* SANS canonical node indexes for an element

            2
            | \
            |   \
            |     \
            |       \
            |         \
            0-----------1
          */

          int n0 = (j*order)*(ii*order + 1) + i*order + 0*(ii*order + 1);
          int n1 = n0 + 1;
          int n2 = (j*order)*(ii*order + 1) + i*order + 1*(ii*order + 1);
          int n3 = n2 + 1;

          std::vector<int> triL = {n0, n1, n2}; // nodes
          std::vector<int> triR = {n3, n2, n1}; // nodes

          // add the indices to the group
          xfldin.addCell(group, eTriangle, order, triL);
          xfldin.addCell(group, eTriangle, order, triR);
        }
      }
    }

    // Start the process of adding boundary trace elements
    xfldin.sizeBoundaryTrace(2*ii + 2*jj);

    // Add elements to rank 0
    if (comm.rank() == 0)
    {
      // lower boundary
      {
        int group = 0;

        int j = 0;
        for (int i = 0; i < ii; i++)
        {
          int n0 = (j*order)*(ii*order + 1) + i*order + 0*(ii*order + 1);
          int n1 = n0 + 1;

          xfldin.addBoundaryTrace( group, eLine, {n0, n1} );
        }
      }

      // right boundary
      {
        int group = 1;

        int i = ii-1;
        for (int j = 0; j < jj; j++)
        {
          int n0 = (j*order)*(ii*order + 1) + i*order + 0*(ii*order + 1);
          int n1 = n0 + 1;
          int n2 = (j*order)*(ii*order + 1) + i*order + 1*(ii*order + 1);
          int n3 = n2 + 1;

          xfldin.addBoundaryTrace( group, eLine, {n1, n3} );
        }
      }

      // upper boundary
      {
        int group = 2;

        int j = jj - 1;
        for (int i = ii-1; i >= 0; i--)
        {
          int n2 = (j*order)*(ii*order + 1) + i*order + 1*(ii*order + 1);
          int n3 = n2 + 1;

          xfldin.addBoundaryTrace( group, eLine, {n3, n2} );
        }
      }

      // left boundary
      {
        int group = 3;

        int i = 0;
        for (int j = jj-1; j >= 0; j--)
        {
          int n0 = (j*order)*(ii*order + 1) + i*order + 0*(ii*order + 1);
          int n2 = (j*order)*(ii*order + 1) + i*order + 1*(ii*order + 1);

          xfldin.addBoundaryTrace( group, eLine, {n2, n0} );
        }
      }
    }

    this->buildFrom( xfldin );
  }
};

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_Box_Triangle_Lagrange_CheckTrace_Q1 )
{
  // global communicator
  mpi::communicator world;

  int ii = 3, jj = 3;

  XField2D_Box_Triangle_Lagrange_Q1 xfld(world, ii, jj, 0., 1., 0., 1.);


  BOOST_CHECK_EQUAL( 1, xfld.nCellGroups() );
  if (xfld.getCellGroupBase(0).nElem() > 1)
    BOOST_CHECK_EQUAL( 1, xfld.nInteriorTraceGroups() );
  else
    BOOST_CHECK_EQUAL( 0, xfld.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 4, xfld.nBoundaryTraceGroups() );

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  // check that grid trace is identical for adjacent elements
  if (xfld.nInteriorTraceGroups() > 0)
    for_each_InteriorFieldTraceGroup_Cell<TopoD2>::apply(
        CheckInteriorTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nInteriorTraceGroups()-1) ), xfld, xfld );

  for_each_BoundaryFieldTraceGroup_Cell<TopoD2>::apply(
      CheckBoundaryTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );

  //output_Tecplot(xfld, "tmp/test" + std::to_string(world.rank()) + ".dat");
}


//----------------------------------------------------------------------------//
// create triangle grid in unit box with ii x jj x 2 (triangle) elements
class XField2D_Box_Triangle_Lagrange_Q2 : public XField<PhysD2,TopoD2>
{
public:

  XField2D_Box_Triangle_Lagrange_Q2(mpi::communicator comm, int ii, int jj, Real xmin, Real xmax, Real ymin, Real ymax )
    : XField<PhysD2,TopoD2>(comm)
  {
    XField_Lagrange<PhysD2>::VectorX X;

    int order = 2;
    int nnode = (ii*order + 1)*(jj*order + 1);
    int nCell = ii*jj*2;

    Real xdiv = (xmax - xmin)/Real(ii)/10;
    Real ydiv = (ymax - ymin)/Real(ii)/10;

    XField_Lagrange<PhysD2> xfldin(comm);

    // create the wavy grid-coordinate DOF arrays
    xfldin.sizeDOF( nnode );

    if (comm.rank() == 0)
    {
      for (int j = 0; j < (jj*order + 1); j++)
      {
        for (int i = 0; i < (ii*order + 1); i++)
        {
          X[0] = xmin + (xmax - xmin)*i/Real(ii*order) + xdiv*sin(2*PI*j/Real(jj*order));
          X[1] = ymin + (ymax - ymin)*j/Real(jj*order) + ydiv*sin(2*PI*i/Real(ii*order));
          xfldin.addDOF(X);
        }
      }
    }

    // Start the process of adding cells
    xfldin.sizeCells(nCell);

    // Add elements to rank 0
    if (comm.rank() == 0)
    {
      int group = 0;
      for (int j = 0; j < jj; j++)
      {
        for (int i = 0; i < ii; i++)
        {
          /* Node indexes in the grid

              6-----7-----8
              | \         |
              |   \       |
              3     4     5
              |       \   |
              |         \ |
              0-----1-----2
          */

          /* SANS canonical node indexes for an element

            2
            | \
            |   \
            4     3
            |       \
            |         \
            0-----5-----1
          */


          int m0 = (j*order)*(ii*order + 1) + i*order + 0*(ii*order + 1);

          int n0 = m0 + 0;
          int n1 = m0 + 1;
          int n2 = m0 + 2;

          int m1 = (j*order)*(ii*order + 1) + i*order + 1*(ii*order + 1);

          int n3 = m1 + 0;
          int n4 = m1 + 1;
          int n5 = m1 + 2;

          int m2 = (j*order)*(ii*order + 1) + i*order + 2*(ii*order + 1);

          int n6  = m2 + 0;
          int n7  = m2 + 1;
          int n8  = m2 + 2;

          std::vector<int> triL = {n0, n2, n6,  // nodes
                                   n4,          // edge 1
                                   n3,          // edge 2
                                   n1};         // edge 3

          std::vector<int> triR = {n8, n6, n2,  // nodes
                                   n4,          // edge 1
                                   n5,          // edge 2
                                   n7};         // edge 3

          // add the indices to the group
          xfldin.addCell(group, eTriangle, order, triL);
          xfldin.addCell(group, eTriangle, order, triR);
        }
      }
    }

    // Start the process of adding boundary trace elements
    xfldin.sizeBoundaryTrace(2*ii + 2*jj);

    // Add elements to rank 0
    if (comm.rank() == 0)
    {
      // lower boundary
      {
        int group = 0;

        int j = 0;
        for (int i = 0; i < ii; i++)
        {
          int m0 = (j*order)*(ii*order + 1) + i*order + 0*(ii*order + 1);

          int n0 = m0 + 0;
          int n2 = m0 + 2;

          xfldin.addBoundaryTrace( group, eLine, {n0, n2} );
        }
      }

      // right boundary
      {
        int group = 1;

        int i = ii-1;
        for (int j = 0; j < jj; j++)
        {
          int m0 = (j*order)*(ii*order + 1) + i*order + 0*(ii*order + 1);

          int n2 = m0 + 2;

          int m2 = (j*order)*(ii*order + 1) + i*order + 2*(ii*order + 1);

          int n8  = m2 + 2;

          xfldin.addBoundaryTrace( group, eLine, {n2, n8} );
        }
      }

      // upper boundary
      {
        int group = 2;

        int j = jj - 1;
        for (int i = ii-1; i >= 0; i--)
        {
          int m2 = (j*order)*(ii*order + 1) + i*order + 2*(ii*order + 1);

          int n6  = m2 + 0;
          int n8  = m2 + 2;

          xfldin.addBoundaryTrace( group, eLine, {n8, n6} );
        }
      }

      // left boundary
      {
        int group = 3;

        int i = 0;
        for (int j = jj-1; j >= 0; j--)
        {
          int m0 = (j*order)*(ii*order + 1) + i*order + 0*(ii*order + 1);

          int n0 = m0 + 0;

          int m2 = (j*order)*(ii*order + 1) + i*order + 2*(ii*order + 1);

          int n6  = m2 + 0;

          xfldin.addBoundaryTrace( group, eLine, {n6, n0} );
        }
      }
    }

    this->buildFrom( xfldin );
  }
};

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_Box_Triangle_Lagrange_CheckTrace_Q2 )
{
  // global communicator
  mpi::communicator world;

  int ii = 3, jj = 3;

  XField2D_Box_Triangle_Lagrange_Q2 xfld(world, ii, jj, 0., 1., 0., 1.);


  BOOST_CHECK_EQUAL( 1, xfld.nCellGroups() );
  if (xfld.getCellGroupBase(0).nElem() > 1)
    BOOST_CHECK_EQUAL( 1, xfld.nInteriorTraceGroups() );
  else
    BOOST_CHECK_EQUAL( 0, xfld.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 4, xfld.nBoundaryTraceGroups() );

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  // check that grid trace is identical for adjacent elements
  if (xfld.nInteriorTraceGroups() > 0)
    for_each_InteriorFieldTraceGroup_Cell<TopoD2>::apply(
         CheckInteriorTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nInteriorTraceGroups()-1) ), xfld, xfld );

  for_each_BoundaryFieldTraceGroup_Cell<TopoD2>::apply(
      CheckBoundaryTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );

  //output_Tecplot(xfld, "tmp/test" + std::to_string(world.rank()) + ".dat");
}

//----------------------------------------------------------------------------//
// create triangle grid in unit box with ii x jj x 2 (triangle) elements

class XField2D_Box_Triangle_Lagrange_Q3 : public XField<PhysD2,TopoD2>
{
public:

  XField2D_Box_Triangle_Lagrange_Q3( mpi::communicator comm, int ii, int jj, Real xmin, Real xmax, Real ymin, Real ymax )
    : XField<PhysD2,TopoD2>(comm)
  {
    XField_Lagrange<PhysD2>::VectorX X;

    int order = 3;
    int nnode = (ii*order + 1)*(jj*order + 1);
    int nCell = ii*jj*2;

    Real xdiv = (xmax - xmin)/Real(ii)/10;
    Real ydiv = (ymax - ymin)/Real(ii)/10;

    XField_Lagrange<PhysD2> xfldin(comm);

    // create the wavy grid-coordinate DOF arrays
    xfldin.sizeDOF(nnode);

    if (comm.rank() == 0)
    {
      for (int j = 0; j < (jj*order + 1); j++)
      {
        for (int i = 0; i < (ii*order + 1); i++)
        {
          X[0] = xmin + (xmax - xmin)*i/Real(ii*order) + xdiv*sin(2*PI*j/Real(jj*order));
          X[1] = ymin + (ymax - ymin)*j/Real(jj*order) + ydiv*sin(2*PI*i/Real(ii*order));
          xfldin.addDOF(X);
        }
      }
    }

    // Start the process of adding cells
    xfldin.sizeCells(nCell);

    if (comm.rank() == 0)
    {
      int group = 0;
      for (int j = 0; j < jj; j++)
      {
        for (int i = 0; i < ii; i++)
        {
          /* Node indexes in the grid

             12 -13 -14 -15
              | \         |
              8   9  10  11
              |     \     |
              4   5   6   7
              |         \ |
              0 - 1 - 2 - 3
          */

          /* SANS canonical Lagrange node indexes for an element

            2
            | \
            5   4
            |     \
            6  (9)  3
            |         \
            0---7---8---1
          */

          int m0 = (j*order)*(ii*order + 1) + i*order + 0*(ii*order + 1);

          int n0 = m0 + 0;
          int n1 = m0 + 1;
          int n2 = m0 + 2;
          int n3 = m0 + 3;

          int m1 = (j*order)*(ii*order + 1) + i*order + 1*(ii*order + 1);

          int n4 = m1 + 0;
          int n5 = m1 + 1;
          int n6 = m1 + 2;
          int n7 = m1 + 3;

          int m2 = (j*order)*(ii*order + 1) + i*order + 2*(ii*order + 1);

          int n8  = m2 + 0;
          int n9  = m2 + 1;
          int n10 = m2 + 2;
          int n11 = m2 + 3;

          int m3 = (j*order)*(ii*order + 1) + i*order + 3*(ii*order + 1);

          int n12 = m3 + 0;
          int n13 = m3 + 1;
          int n14 = m3 + 2;
          int n15 = m3 + 3;

          std::vector<int> triL = {n0, n3, n12, // nodes
                                   n6, n9,      // edge 1
                                   n8, n4,      // edge 2
                                   n1, n2,      // edge 3
                                   n5};         // cell

          std::vector<int> triR = {n15, n12, n3, // nodes
                                   n9, n6,       // edge 1
                                   n7, n11,      // edge 2
                                   n14, n13,     // edge 3
                                   n10};         // cell

          // add the indices to the group
          xfldin.addCell(group, eTriangle, order, triL);
          xfldin.addCell(group, eTriangle, order, triR);
        }
      }
    }

    // Start the process of adding boundary trace elements
    xfldin.sizeBoundaryTrace(2*ii + 2*jj);

    // Add elements to rank 0
    if (comm.rank() == 0)
    {
      // lower boundary
      {
        int group = 0;

        int j = 0;
        for (int i = 0; i < ii; i++)
        {
          int m0 = (j*order)*(ii*order + 1) + i*order + 0*(ii*order + 1);

          int n0 = m0 + 0;
          int n3 = m0 + 3;

          xfldin.addBoundaryTrace( group, eLine, {n0, n3} );
        }
      }

      // right boundary
      {
        int group = 1;

        int i = ii-1;
        for (int j = 0; j < jj; j++)
        {
          int m0 = (j*order)*(ii*order + 1) + i*order + 0*(ii*order + 1);
          int n3 = m0 + 3;

          int m3 = (j*order)*(ii*order + 1) + i*order + 3*(ii*order + 1);
          int n15 = m3 + 3;

          xfldin.addBoundaryTrace( group, eLine, {n3, n15} );
        }
      }

      // upper boundary
      {
        int group = 2;

        int j = jj - 1;
        for (int i = ii-1; i >= 0; i--)
        {
          int m3 = (j*order)*(ii*order + 1) + i*order + 3*(ii*order + 1);

          int n12 = m3 + 0;
          int n15 = m3 + 3;

          xfldin.addBoundaryTrace( group, eLine, {n15, n12} );
        }
      }

      // left boundary
      {
        int group = 3;

        int i = 0;
        for (int j = jj-1; j >= 0; j--)
        {
          int m0 = (j*order)*(ii*order + 1) + i*order + 0*(ii*order + 1);
          int n0 = m0 + 0;

          int m3 = (j*order)*(ii*order + 1) + i*order + 3*(ii*order + 1);
          int n12 = m3 + 0;

          xfldin.addBoundaryTrace( group, eLine, {n12, n0} );
        }
      }
    }

    this->buildFrom( xfldin );
  }
};



//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField2D_Box_Triangle_Lagrange_CheckTrace_Q3 )
{
  // global communicator
  mpi::communicator world;

  int ii = 3, jj = 3;

  XField2D_Box_Triangle_Lagrange_Q3 xfld(world, ii, jj, 0., 1., 0., 1.);


  BOOST_CHECK_EQUAL( 1, xfld.nCellGroups() );
  if (xfld.getCellGroupBase(0).nElem() > 1)
    BOOST_CHECK_EQUAL( 1, xfld.nInteriorTraceGroups() );
  else
    BOOST_CHECK_EQUAL( 0, xfld.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 4, xfld.nBoundaryTraceGroups() );

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  // check that grid trace is identical for adjacent elements
  if (xfld.nInteriorTraceGroups() > 0)
    for_each_InteriorFieldTraceGroup_Cell<TopoD2>::apply(
         CheckInteriorTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nInteriorTraceGroups()-1) ), xfld, xfld );

  for_each_BoundaryFieldTraceGroup_Cell<TopoD2>::apply(
      CheckBoundaryTraceCoordinates2D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );

  //output_Tecplot(xfld, "tmp/test" + std::to_string(world.rank()) + ".dat");
}


//----------------------------------------------------------------------------//
// create triangle grid in unit box with ii x jj x 2 (triangle) elements
class XField3D_Box_Tet_Lagrange_Q1 : public XField<PhysD3,TopoD3>
{
public:

  XField3D_Box_Tet_Lagrange_Q1(mpi::communicator comm, int ii, int jj, int kk,
                               Real xmin, Real xmax,
                               Real ymin, Real ymax,
                               Real zmin, Real zmax)
    : XField<PhysD3,TopoD3>(comm)
  {
    XField_Lagrange<PhysD3>::VectorX X;

    int order = 1;
    int nnode = (ii*order + 1)*(jj*order + 1)*(kk*order + 1);
    int nCell = ii*jj*kk*6;

    const int joffset = (ii+1);
    const int koffset = (ii+1)*(jj+1);

    Real xdiv = (xmax - xmin)/Real(ii)/10;
    Real ydiv = (ymax - ymin)/Real(jj)/10;
    Real zdiv = (zmax - zmin)/Real(kk)/10;

    XField_Lagrange<PhysD3> xfldin(comm);

    // create the wavy grid-coordinate DOF arrays
    xfldin.sizeDOF( nnode );

    if (comm.rank() == 0)
      for (int k = 0; k < (kk*order + 1); k++)
      {
        for (int j = 0; j < (jj*order + 1); j++)
        {
          for (int i = 0; i < (ii*order + 1); i++)
          {
            X[0] = xmin + (xmax - xmin)*i/Real(ii*order) + xdiv*sin(2*PI*j/Real(jj*order));
            X[1] = ymin + (ymax - ymin)*j/Real(jj*order) + ydiv*sin(2*PI*k/Real(kk*order));
            X[2] = zmin + (zmax - zmin)*k/Real(kk*order) + zdiv*sin(2*PI*i/Real(ii*order));
            xfldin.addDOF(X);
          }
        }
      }

    // Index table for each tet in the hex
    const int hextets[6][4] = { {0, 1, 2, 4},
                                {1, 4, 3, 2},
                                {6, 2, 3, 4},

                                {1, 4, 5, 3},
                                {4, 6, 5, 3},
                                {7, 6, 3, 5} };

    // Start the process of adding cells
    xfldin.sizeCells(nCell);

    // Add DOFs to rank 0
    if (comm.rank() == 0)
    {
      std::vector<int> tetnodes(4);

      int group = 0;
      for (int k = 0; k < kk; k++)
      {
        for (int j = 0; j < jj; j++)
        {
          for (int i = 0; i < ii; i++)
          {
            /*
            // Node ordering of the hex in the grid
            //
            //         y
            //  2----------3
            //  |\     ^   |\
            //  | \    |   | \
            //  |  \   |   |  \
            //  |   6------+---7
            //  |   |  +-- |-- | -> x
            //  0---+---\--1   |
            //   \  |    \  \  |
            //    \ |     \  \ |
            //     \|      z  \|
            //      4----------5
            // SANS canonical Nodes that make up each of the 6 hexahedron:
            //
            // Left prism
            // Tet[0]: {0, 1, 2, 4}
            // Tet[1]: {1, 4, 3, 2}
            // Tet[2]: {6, 2, 3, 4}
            //
            // Right prism
            // Tet[3]: {1, 4, 5, 3}
            // Tet[4]: {4, 6, 5, 3}
            // Tet[5]: {7, 6, 3, 5}
            */

            const int n0 = k*koffset + j*joffset + i;

            //All the nodes that make up an individial hex
            const int hexnodes[8] = { n0 + 0,
                                      n0 + 1,
                                      n0 + joffset + 0,
                                      n0 + joffset + 1,

                                      n0 + koffset + 0,
                                      n0 + koffset + 1,
                                      n0 + koffset + joffset + 0,
                                      n0 + koffset + joffset + 1 };

            //Loop over all tets that make up a hex
            for (int tet = 0; tet < 6; tet++)
            {
              //Get the nodes from the hex for each tet
              for (int n = 0; n < 4; n++)
                tetnodes[n] = hexnodes[hextets[tet][n]];

              // add the indices to the group
              xfldin.addCell(group, eTet, order, tetnodes);
            }
          }
        }
      }
    } // rank == 0

    // Start the process of adding boundary trace elements
    xfldin.sizeBoundaryTrace(2*(2*ii*jj + 2*jj*kk + 2*ii*kk));

    // Add elements to rank 0
    if (comm.rank() == 0)
    {
      const int (*TraceNodes)[ Triangle::NTrace ] = TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes;

      int hextetL, faceL;

      std::vector<int> faceNodes(3);

      // x-min boundary
      {
        int group = 0;

        for (int k = 0; k < kk; k++)
        {
          for (int j = 0; j < jj; j++)
          {
            int i = 0;
            {
              const int n0 = k*koffset + j*joffset + i;

              //All the nodes that make up the left hex
              const int hexnodes[8] = { n0 + 0,
                                        n0 + 1,
                                        n0 + joffset + 0,
                                        n0 + joffset + 1,

                                        n0 + koffset + 0,
                                        n0 + koffset + 1,
                                        n0 + koffset + joffset + 0,
                                        n0 + koffset + joffset + 1 };

              hextetL = 0; faceL = 1; //Hex nodes {0, X, 2, 4}
              for (int n = 0; n < 3; n++)
                faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];

              xfldin.addBoundaryTrace( group, eTriangle, faceNodes );

              hextetL = 2; faceL = 2; //Hex nodes {6, 2, X, 4}
              for (int n = 0; n < 3; n++)
                faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];

              xfldin.addBoundaryTrace( group, eTriangle, faceNodes );
            }
          }
        }
      }

      // x-max boundary
      {
        int group = 1;

        for (int k = 0; k < kk; k++)
        {
          for (int j = 0; j < jj; j++)
          {
            int i = ii-1;
            {
              const int n0 = k*koffset + j*joffset + i;

              //All the nodes that make up the left hex
              const int hexnodes[8] = { n0 + 0,
                                        n0 + 1,
                                        n0 + joffset + 0,
                                        n0 + joffset + 1,

                                        n0 + koffset + 0,
                                        n0 + koffset + 1,
                                        n0 + koffset + joffset + 0,
                                        n0 + koffset + joffset + 1 };

              hextetL = 3; faceL = 1; //Hex nodes {1, X, 5, 3}
              for (int n = 0; n < 3; n++)
                faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];

              xfldin.addBoundaryTrace( group, eTriangle, faceNodes );


              hextetL = 5; faceL = 1; //Hex nodes {7, X, 3, 5}
              for (int n = 0; n < 3; n++)
                faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];

              xfldin.addBoundaryTrace( group, eTriangle, faceNodes );
            }
          }
        }
      }

      // y-min boundary
      {
        int group = 2;

        for (int k = 0; k < kk; k++)
        {
          int j = 0;
          {
            for (int i = 0; i < ii; i++)
            {
              const int n0 = k*koffset + j*joffset + i;

              //All the nodes that make up the left hex
              const int hexnodes[8] = { n0 + 0,
                                        n0 + 1,
                                        n0 + joffset + 0,
                                        n0 + joffset + 1,

                                        n0 + koffset + 0,
                                        n0 + koffset + 1,
                                        n0 + koffset + joffset + 0,
                                        n0 + koffset + joffset + 1 };

              hextetL = 0; faceL = 2; //Hex nodes {0, 1, X, 4}
              for (int n = 0; n < 3; n++)
                faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];

              xfldin.addBoundaryTrace( group, eTriangle, faceNodes );

              hextetL = 3; faceL = 3; //Hex nodes {1, 4, 5, X}
              for (int n = 0; n < 3; n++)
                faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];

              xfldin.addBoundaryTrace( group, eTriangle, faceNodes );
            }
          }
        }
      }

      // y-max boundary
      {
        int group = 3;

        for (int k = 0; k < kk; k++)
        {
          int j = jj-1;
          {
            for (int i = 0; i < ii; i++)
            {
              const int n0 = k*koffset + j*joffset + i;

              //All the nodes that make up the left hex
              const int hexnodes[8] = { n0 + 0,
                                        n0 + 1,
                                        n0 + joffset + 0,
                                        n0 + joffset + 1,

                                        n0 + koffset + 0,
                                        n0 + koffset + 1,
                                        n0 + koffset + joffset + 0,
                                        n0 + koffset + joffset + 1 };

              hextetL = 2; faceL = 3; //Hex nodes {6, 2, 3, X}
              for (int n = 0; n < 3; n++)
                faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];

              xfldin.addBoundaryTrace( group, eTriangle, faceNodes );


              hextetL = 5; faceL = 3; //Hex nodes {7, 6, 3, X}
              for (int n = 0; n < 3; n++)
                faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];

              xfldin.addBoundaryTrace( group, eTriangle, faceNodes );
            }
          }
        }
      }

      // z-min boundary
      {
        int group = 4;

        int k = 0;
        {
          for (int j = 0; j < jj; j++)
          {
            for (int i = 0; i < ii; i++)
            {
              const int n0 = k*koffset + j*joffset + i;

              //All the nodes that make up the left hex
              const int hexnodes[8] = { n0 + 0,
                                        n0 + 1,
                                        n0 + joffset + 0,
                                        n0 + joffset + 1,

                                        n0 + koffset + 0,
                                        n0 + koffset + 1,
                                        n0 + koffset + joffset + 0,
                                        n0 + koffset + joffset + 1 };

              hextetL = 0; faceL = 3; //Hex nodes {0, 1, 2, X}
              for (int n = 0; n < 3; n++)
                faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];

              xfldin.addBoundaryTrace( group, eTriangle, faceNodes );


              hextetL = 1; faceL = 1; //Hex nodes {1, X, 3, 2}
              for (int n = 0; n < 3; n++)
                faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];

              xfldin.addBoundaryTrace( group, eTriangle, faceNodes );
            }
          }
        }
      }

      // z-max boundary
      {
        int group = 5;

        int k = kk-1;
        {
          for (int j = 0; j < jj; j++)
          {
            for (int i = 0; i < ii; i++)
            {
              const int n0 = k*koffset + j*joffset + i;

              //All the nodes that make up the left hex
              const int hexnodes[8] = { n0 + 0,
                                        n0 + 1,
                                        n0 + joffset + 0,
                                        n0 + joffset + 1,

                                        n0 + koffset + 0,
                                        n0 + koffset + 1,
                                        n0 + koffset + joffset + 0,
                                        n0 + koffset + joffset + 1 };

              hextetL = 4; faceL = 3; //Hex nodes {4, 6, 5, X}
              for (int n = 0; n < 3; n++)
                faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];

              xfldin.addBoundaryTrace( group, eTriangle, faceNodes );


              hextetL = 5; faceL = 2; //Hex nodes {7, 6, X, 5}
              for (int n = 0; n < 3; n++)
                faceNodes[n] = hexnodes[ hextets[hextetL][TraceNodes[faceL][n]] ];

              xfldin.addBoundaryTrace( group, eTriangle, faceNodes );
            }
          }
        }
      }
    }

    this->buildFrom( xfldin );
  }
};

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( XField3D_Box_Tet_Lagrange_CheckTrace_Q1 )
{
  // global communicator
  mpi::communicator world;

  int ii = 3, jj = 3, kk = 3;

  XField3D_Box_Tet_Lagrange_Q1 xfld(world, ii, jj, kk, 0., 1., 0., 1., 0., 1.);


  BOOST_CHECK_EQUAL( 1, xfld.nCellGroups() );
  if (xfld.getCellGroupBase(0).nElem() > 1)
    BOOST_CHECK_EQUAL( 1, xfld.nInteriorTraceGroups() );
  else
    BOOST_CHECK_EQUAL( 0, xfld.nInteriorTraceGroups() );
  BOOST_CHECK_EQUAL( 6, xfld.nBoundaryTraceGroups() );

  const Real small_tol = 1e-11;
  const Real close_tol = 1e-11;

  // check that grid trace is identical for adjacent elements
  if (xfld.nInteriorTraceGroups() > 0)
    for_each_InteriorFieldTraceGroup_Cell<TopoD3>::apply(
        CheckInteriorTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nInteriorTraceGroups()-1) ), xfld, xfld );

  for_each_BoundaryFieldTraceGroup_Cell<TopoD3>::apply(
      CheckBoundaryTraceCoordinates3D(small_tol, close_tol, linspace(0,xfld.nBoundaryTraceGroups()-1) ), xfld, xfld );

  //output_Tecplot(xfld, "tmp/test" + std::to_string(world.rank()) + ".dat");
}


//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
