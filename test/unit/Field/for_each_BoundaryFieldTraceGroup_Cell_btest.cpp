// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// ProjectSolnCell_Galerkin_btest
// testing of cell solution projection for Galerkin

#include <boost/test/unit_test.hpp>

#include "SANS_btest.h"

#include "tools/SANSnumerics.h"     // Real

#include "Field/Tuple/FieldTuple.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_Trace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_Trace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldVolume_DG_Cell.h"
// #include "Field/FieldVolume_DG_Trace.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"

#include "Field/tools/for_each_BoundaryFieldTraceGroup_Cell.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "Meshing/XField1D/XField1D.h"
#include "unit/UnitGrids/XField2D_4Triangle_X1_1Group.h"
#include "unit/UnitGrids/XField2D_4Quad_X1_1Group.h"
#include "unit/UnitGrids/XField3D_6Tet_X1_1Group.h"
#include "unit/UnitGrids/XField3D_2Hex_X1_1Group.h"

using namespace std;
using namespace SANS;


// Explicitly instantiate classes to generate all the functions so that coverage
// information is correct

//############################################################################//
BOOST_AUTO_TEST_SUITE( for_each_BoundaryFieldTraceGroup_test_suite )

// Class for testing boundary trace allocation, taken from HDG
template<class PhysDim_>
class DistributeBoundaryTraceField:
    public GroupFunctorBoundaryTraceType<DistributeBoundaryTraceField<PhysDim_>>
{
public:
  typedef PhysDim_ PhysDim;

  explicit DistributeBoundaryTraceField( const std::vector<int>& boundaryTraceGroups ) :
  boundaryTraceGroups_(boundaryTraceGroups) {}

  std::size_t nBoundaryTraceGroups() const { return boundaryTraceGroups_.size(); }
  std::size_t boundaryTraceGroup(const int n) const { return boundaryTraceGroups_[n]; }


  //----------------------------------------------------------------------------//
  // Distribution function that redistributes the error in each cell group
  template< class TopologyTrace, class TopologyL>
  void
  apply( const typename Field<PhysDim, typename TopologyL::TopoDim, Real>::template FieldCellGroupType<TopologyL>& efld,
         const typename Field<PhysDim, typename TopologyL::TopoDim, Real>::template FieldTraceGroupType<TopologyTrace>& eBfld,
         const typename XField<PhysDim, typename TopologyL::TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
         const int traceGroupGlobal)
  {
    typedef typename Field<PhysDim, typename TopologyL::TopoDim, Real>::template FieldCellGroupType<TopologyL>      EFieldCellGroupType;
    typedef typename Field<PhysDim, typename TopologyL::TopoDim, Real>::template FieldTraceGroupType<TopologyTrace> EBFieldTraceGroupType;

    typedef typename EFieldCellGroupType::template ElementType<> ElementEFieldClass;
    typedef typename EBFieldTraceGroupType::template ElementType<> ElementEBFieldTraceClass;

    // Elements
    ElementEFieldClass efldElem( efld.basis() );
    ElementEBFieldTraceClass eBfldElem( eBfld.basis() );

    // loop over elements in trace group
    for (int elem = 0; elem< xfldTrace.nElem(); elem++)
    {
      int elemL = xfldTrace.getElementLeft( elem );
      efld.getElement( efldElem, elemL);
      eBfld.getElement( eBfldElem, elem );
      efldElem.DOF(0) += eBfldElem.DOF(0);
      const_cast<EFieldCellGroupType&>(efld).setElement( efldElem, elemL);
    }
  }
protected:
  const std::vector<int>& boundaryTraceGroups_;
};

// Class for testing boundary trace allocation, taken from HDG, with added tupling
template<class PhysDim_>
class DistributeBoundaryTraceField_Tuple:
    public GroupFunctorBoundaryTraceType<DistributeBoundaryTraceField_Tuple<PhysDim_>>
{
public:
  typedef PhysDim_ PhysDim;

  explicit DistributeBoundaryTraceField_Tuple( const std::vector<int>& boundaryTraceGroups ) :
  boundaryTraceGroups_(boundaryTraceGroups) {}

  std::size_t nBoundaryTraceGroups() const { return boundaryTraceGroups_.size(); }
  std::size_t boundaryTraceGroup(const int n) const { return boundaryTraceGroups_[n]; }


  //----------------------------------------------------------------------------//
  // Distribution function that redistributes the error in each cell group
  template< class TopologyTrace, class TopologyL>
  void
  apply( const typename FieldTuple< XField<PhysDim,typename TopologyL::TopoDim>,
                                    Field<PhysDim, typename TopologyL::TopoDim, Real>, TupleClass<>>::template FieldCellGroupType<TopologyL>& fldsL,
         const typename Field<PhysDim, typename TopologyL::TopoDim, Real>::template FieldTraceGroupType<TopologyTrace>& eBfld,
         const typename XField<PhysDim, typename TopologyL::TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
         const int traceGroupGlobal)
  {
    typedef typename Field<PhysDim, typename TopologyL::TopoDim, Real>::template FieldCellGroupType<TopologyL>      EFieldCellGroupType;
    typedef typename Field<PhysDim, typename TopologyL::TopoDim, Real>::template FieldTraceGroupType<TopologyTrace> EBFieldTraceGroupType;

    typedef typename EFieldCellGroupType::template ElementType<> ElementEFieldClass;
    typedef typename EBFieldTraceGroupType::template ElementType<> ElementEBFieldTraceClass;

    EFieldCellGroupType efld = get<-1>(fldsL);

    // Elements
    ElementEFieldClass efldElem( efld.basis() );
    ElementEBFieldTraceClass eBfldElem( eBfld.basis() );

    // loop over elements in trace group
    for (int elem = 0; elem< xfldTrace.nElem(); elem++)
    {
      int elemL = xfldTrace.getElementLeft( elem );
      efld.getElement( efldElem, elemL);
      eBfld.getElement( eBfldElem, elem );
      efldElem.DOF(0) += eBfldElem.DOF(0);
      const_cast<EFieldCellGroupType&>(efld).setElement( efldElem, elemL);
    }
  }
protected:
  const std::vector<int>& boundaryTraceGroups_;
};

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DistributeBoundaryTraceField_1D_Line )
{
  // grid: three lines, P1 (aka X1)
  XField1D xfld(3);
  std::vector<int> boundaryTraceGroups(xfld.nBoundaryTraceGroups());
  for (int k = 0; k < xfld.nBoundaryTraceGroups(); k++)
    boundaryTraceGroups[k]=k;
  // solution: P1 (aka Q1), which spans the space of the Linear solution

  Field_DG_Cell<PhysD1, TopoD1, Real> efld( xfld, 0, BasisFunctionCategory_Legendre );
  Field_DG_BoundaryTrace<PhysD1, TopoD1, Real> eBfld( xfld, 0, BasisFunctionCategory_Legendre );

  efld = 0;
  eBfld = 1;

  for_each_BoundaryFieldTraceGroup_Cell<TopoD1>::apply( DistributeBoundaryTraceField<PhysD1>(boundaryTraceGroups), efld, eBfld );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  Field<PhysD1, TopoD1, Real>::template FieldCellGroupType<Line>& efldCell = efld.getCellGroup<Line>(0);
  typedef Field<PhysD1, TopoD1, Real>::template FieldCellGroupType<Line>::template ElementType<> ElementEFieldClass;

  // element field variables
  ElementEFieldClass efldElem( efldCell.basis() );

  // loop over elements within group
  for (int elem = 0; elem < efld.nElem(); elem++)
  {
    // copy global grid/solution DOFs to element
    efldCell.getElement( efldElem, elem );
    if ((elem == 0) || (elem ==2))
      SANS_CHECK_CLOSE( 1, efldElem.DOF(0), small_tol, close_tol )
    else
      SANS_CHECK_CLOSE( 0, efldElem.DOF(0), small_tol, close_tol )
  }

  // check that it also works with trace fields
  efld = 0;

  Field_DG_Trace<PhysD1, TopoD1, Real> eIfld(xfld, 0, BasisFunctionCategory_Legendre);
  eIfld = 1;

  for_each_BoundaryFieldTraceGroup_Cell<TopoD1>::apply( DistributeBoundaryTraceField<PhysD1>(boundaryTraceGroups), efld, eIfld );

  // loop over elements within group
  for (int elem = 0; elem < efld.nElem(); elem++)
  {
    // copy global grid/solution DOFs to element
    efldCell.getElement( efldElem, elem );
    if ((elem == 0) || (elem ==2))
      SANS_CHECK_CLOSE( 1, efldElem.DOF(0), small_tol, close_tol )
    else
      SANS_CHECK_CLOSE( 0, efldElem.DOF(0), small_tol, close_tol )
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DistributeBoundaryTraceField_1D_Line_Tuple )
{
  // grid: three lines, P1 (aka X1)
  XField1D xfld(3);

  // solution: P1 (aka Q1), which spans the space of the Linear solution

  Field_DG_Cell<PhysD1, TopoD1, Real> efld( xfld, 0, BasisFunctionCategory_Legendre );
  Field_DG_BoundaryTrace<PhysD1, TopoD1, Real> eBfld( xfld, 0, BasisFunctionCategory_Legendre );

  efld = 0;
  eBfld = 1;

  for_each_BoundaryFieldTraceGroup_Cell<TopoD1>::apply( DistributeBoundaryTraceField_Tuple<PhysD1>({0,1}), (xfld, efld), eBfld );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  Field<PhysD1, TopoD1, Real>::template FieldCellGroupType<Line>& efldCell = efld.getCellGroup<Line>(0);
  typedef Field<PhysD1, TopoD1, Real>::template FieldCellGroupType<Line>::template ElementType<> ElementEFieldClass;

  // element field variables
  ElementEFieldClass efldElem( efldCell.basis() );

  // loop over elements within group
  for (int elem = 0; elem < efld.nElem(); elem++)
  {
    // copy global grid/solution DOFs to element
    efldCell.getElement( efldElem, elem );

    if ( ( elem == 0) || (elem == 2) )
    {
      SANS_CHECK_CLOSE( 1.0, efldElem.DOF(0), small_tol, close_tol )
    }
    else
    {
      SANS_CHECK_CLOSE( 0, efldElem.DOF(0), small_tol, close_tol );
    }
  }

  // check that works with Field trace
  efld = 0;

  Field_DG_Trace<PhysD1,TopoD1,Real> eIfld( xfld, 0, BasisFunctionCategory_Legendre );

  eIfld = 1;

  for_each_BoundaryFieldTraceGroup_Cell<TopoD1>::apply( DistributeBoundaryTraceField_Tuple<PhysD1>({0,1}), (xfld, efld), eIfld );

  // loop over elements within group
  for (int elem = 0; elem < efld.nElem(); elem++)
  {
    // copy global grid/solution DOFs to element
    efldCell.getElement( efldElem, elem );

    if ( ( elem == 0) || (elem == 2) )
    {
      SANS_CHECK_CLOSE( 1.0, efldElem.DOF(0), small_tol, close_tol )
    }
    else
    {
      SANS_CHECK_CLOSE( 0, efldElem.DOF(0), small_tol, close_tol );
    }
  }

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DistributeBoundaryTraceField_2D_Triangle )
{
  // grid: four triangles, P1 (aka X1)
  XField2D_4Triangle_X1_1Group xfld;

  // solution: P1 (aka Q1), which spans the space of the Linear solution

  Field_DG_Cell<PhysD2, TopoD2, Real> efld( xfld, 0, BasisFunctionCategory_Legendre );
  Field_DG_BoundaryTrace<PhysD2, TopoD2, Real> eBfld( xfld, 0, BasisFunctionCategory_Legendre );

  efld = 0;
  eBfld = 1;

  for_each_BoundaryFieldTraceGroup_Cell<TopoD2>::apply( DistributeBoundaryTraceField<PhysD2>({0}), efld, eBfld );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  Field<PhysD2, TopoD2, Real>::template FieldCellGroupType<Triangle>& efldCell = efld.getCellGroup<Triangle>(0);
  typedef Field<PhysD2, TopoD2, Real>::template FieldCellGroupType<Triangle>::template ElementType<> ElementEFieldClass;

  // element field variables
  ElementEFieldClass efldElem( efldCell.basis() );

  // loop over elements within group
  for (int elem = 0; elem < efld.nElem(); elem++)
  {
    // copy global grid/solution DOFs to element
    efldCell.getElement( efldElem, elem );

    if ( elem == 0 )
    {
      SANS_CHECK_CLOSE( 0, efldElem.DOF(0), small_tol, close_tol );
    }
    else
    {
      SANS_CHECK_CLOSE( 2, efldElem.DOF(0), small_tol, close_tol );
    }
  }

  // checking FieldTrace
  efld = 0;

  Field_DG_Trace<PhysD2, TopoD2, Real> eIfld( xfld, 0, BasisFunctionCategory_Legendre );

  eIfld = 1;

  for_each_BoundaryFieldTraceGroup_Cell<TopoD2>::apply( DistributeBoundaryTraceField<PhysD2>({0}), efld, eIfld );

  // loop over elements within group
  for (int elem = 0; elem < efld.nElem(); elem++)
  {
    // copy global grid/solution DOFs to element
    efldCell.getElement( efldElem, elem );

    if ( elem == 0 )
    {
      SANS_CHECK_CLOSE( 0, efldElem.DOF(0), small_tol, close_tol );
    }
    else
    {
      SANS_CHECK_CLOSE( 2, efldElem.DOF(0), small_tol, close_tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DistributeBoundaryTraceField_2D_Triangle_Tuple )
{
  // grid: four triangles, P1 (aka X1)
  XField2D_4Triangle_X1_1Group xfld;

  // solution: P1 (aka Q1), which spans the space of the Linear solution

  Field_DG_Cell<PhysD2, TopoD2, Real> efld( xfld, 0, BasisFunctionCategory_Legendre );
  Field_DG_BoundaryTrace<PhysD2, TopoD2, Real> eBfld( xfld, 0, BasisFunctionCategory_Legendre );

  efld = 0;
  eBfld = 1;

  for_each_BoundaryFieldTraceGroup_Cell<TopoD2>::apply( DistributeBoundaryTraceField_Tuple<PhysD2>({0}), (xfld, efld), eBfld );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  Field<PhysD2, TopoD2, Real>::template FieldCellGroupType<Triangle>& efldCell = efld.getCellGroup<Triangle>(0);
  typedef Field<PhysD2, TopoD2, Real>::template FieldCellGroupType<Triangle>::template ElementType<> ElementEFieldClass;

  // element field variables
  ElementEFieldClass efldElem( efldCell.basis() );

  // loop over elements within group
  for (int elem = 0; elem < efld.nElem(); elem++)
  {
    // copy global grid/solution DOFs to element
    efldCell.getElement( efldElem, elem );

    if ( elem == 0 )
    {
      SANS_CHECK_CLOSE( 0, efldElem.DOF(0), small_tol, close_tol );
    }
    else
    {
      SANS_CHECK_CLOSE( 2, efldElem.DOF(0), small_tol, close_tol );
    }
  }

  // checking FieldTrace
  efld = 0;

  Field_DG_Trace<PhysD2, TopoD2, Real> eIfld( xfld, 0, BasisFunctionCategory_Legendre );

  eIfld = 1;

  for_each_BoundaryFieldTraceGroup_Cell<TopoD2>::apply( DistributeBoundaryTraceField_Tuple<PhysD2>({0}), (xfld, efld), eIfld );

  // loop over elements within group
  for (int elem = 0; elem < efld.nElem(); elem++)
  {
    // copy global grid/solution DOFs to element
    efldCell.getElement( efldElem, elem );

    if ( elem == 0 )
    {
      SANS_CHECK_CLOSE( 0, efldElem.DOF(0), small_tol, close_tol );
    }
    else
    {
      SANS_CHECK_CLOSE( 2, efldElem.DOF(0), small_tol, close_tol );
    }
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DistributeBoundaryTraceField_2D_Quad )
{
  // grid: four triangles, P1 (aka X1)
  XField2D_4Quad_X1_1Group xfld;

  // solution: P1 (aka Q1), which spans the space of the Linear solution

  Field_DG_Cell<PhysD2, TopoD2, Real> efld( xfld, 0, BasisFunctionCategory_Legendre );
  Field_DG_BoundaryTrace<PhysD2, TopoD2, Real> eBfld( xfld, 0, BasisFunctionCategory_Legendre );

  efld = 0;
  eBfld = 1;

  for_each_BoundaryFieldTraceGroup_Cell<TopoD2>::apply( DistributeBoundaryTraceField<PhysD2>({0}), efld, eBfld );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  Field<PhysD2, TopoD2, Real>::template FieldCellGroupType<Quad>& efldCell = efld.getCellGroup<Quad>(0);
  typedef Field<PhysD2, TopoD2, Real>::template FieldCellGroupType<Quad>::template ElementType<> ElementEFieldClass;

  // element field variables
  ElementEFieldClass efldElem( efldCell.basis() );

  // loop over elements within group
  for (int elem = 0; elem < efld.nElem(); elem++)
  {
    // copy global grid/solution DOFs to element
    efldCell.getElement( efldElem, elem );

    SANS_CHECK_CLOSE( 2, efldElem.DOF(0), small_tol, close_tol );
  }

  // checking FieldTrace
  efld = 0;

  Field_DG_Trace<PhysD2, TopoD2, Real> eIfld( xfld, 0, BasisFunctionCategory_Legendre );

  eIfld = 1;

  for_each_BoundaryFieldTraceGroup_Cell<TopoD2>::apply( DistributeBoundaryTraceField<PhysD2>({0}), efld, eIfld );

  // loop over elements within group
  for (int elem = 0; elem < efld.nElem(); elem++)
  {
    // copy global grid/solution DOFs to element
    efldCell.getElement( efldElem, elem );

    SANS_CHECK_CLOSE( 2, efldElem.DOF(0), small_tol, close_tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DistributeBoundaryTraceField_2D_Quad_Tuple )
{
  // grid: four triangles, P1 (aka X1)
  XField2D_4Quad_X1_1Group xfld;

  // solution: P1 (aka Q1), which spans the space of the Linear solution

  Field_DG_Cell<PhysD2, TopoD2, Real> efld( xfld, 0, BasisFunctionCategory_Legendre );
  Field_DG_BoundaryTrace<PhysD2, TopoD2, Real> eBfld( xfld, 0, BasisFunctionCategory_Legendre );

  efld = 0;
  eBfld = 1;

  for_each_BoundaryFieldTraceGroup_Cell<TopoD2>::apply( DistributeBoundaryTraceField_Tuple<PhysD2>({0}), (xfld, efld), eBfld );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  Field<PhysD2, TopoD2, Real>::template FieldCellGroupType<Quad>& efldCell = efld.getCellGroup<Quad>(0);
  typedef Field<PhysD2, TopoD2, Real>::template FieldCellGroupType<Quad>::template ElementType<> ElementEFieldClass;

  // element field variables
  ElementEFieldClass efldElem( efldCell.basis() );

  // loop over elements within group
  for (int elem = 0; elem < efld.nElem(); elem++)
  {
    // copy global grid/solution DOFs to element
    efldCell.getElement( efldElem, elem );

    SANS_CHECK_CLOSE( 2, efldElem.DOF(0), small_tol, close_tol );
  }

  // checking FieldTrace
  efld = 0;

  Field_DG_Trace<PhysD2, TopoD2, Real> eIfld( xfld, 0, BasisFunctionCategory_Legendre );

  eIfld = 1;

  for_each_BoundaryFieldTraceGroup_Cell<TopoD2>::apply( DistributeBoundaryTraceField_Tuple<PhysD2>({0}), (xfld, efld), eIfld );

  // loop over elements within group
  for (int elem = 0; elem < efld.nElem(); elem++)
  {
    // copy global grid/solution DOFs to element
    efldCell.getElement( efldElem, elem );

    SANS_CHECK_CLOSE( 2, efldElem.DOF(0), small_tol, close_tol );
  }
}

#if 1
//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DistributeBoundaryTraceField_3D_Tet )
{
  // grid: four triangles, P1 (aka X1)
  XField3D_6Tet_X1_1Group xfld;

  // solution: P1 (aka Q1), which spans the space of the Linear solution

  Field_DG_Cell<PhysD3, TopoD3, Real> efld( xfld, 0, BasisFunctionCategory_Legendre );
  Field_DG_BoundaryTrace<PhysD3, TopoD3, Real> eBfld( xfld, 0, BasisFunctionCategory_Legendre );

  efld = 0;
  eBfld = 1;

  for_each_BoundaryFieldTraceGroup_Cell<TopoD3>::apply( DistributeBoundaryTraceField<PhysD3>({0}), efld, eBfld );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  Field<PhysD3, TopoD3, Real>::template FieldCellGroupType<Tet>& efldCell = efld.getCellGroup<Tet>(0);
  typedef Field<PhysD3, TopoD3, Real>::template FieldCellGroupType<Tet>::template ElementType<> ElementEFieldClass;

  // element field variables
  ElementEFieldClass efldElem( efldCell.basis() );

  // loop over elements within group
  for (int elem = 0; elem < efld.nElem(); elem++)
  {
    // copy global grid/solution DOFs to element
    efldCell.getElement( efldElem, elem );
    if ((elem == 0) || (elem == 5))
      {SANS_CHECK_CLOSE( 3, efldElem.DOF(0), small_tol, close_tol );}
    if ( (elem == 1) || (elem == 4) )
      {SANS_CHECK_CLOSE( 1, efldElem.DOF(0), small_tol, close_tol );}
    if ( (elem == 3) || (elem == 2) )
      {SANS_CHECK_CLOSE( 2, efldElem.DOF(0), small_tol, close_tol );}
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DistributeBoundaryTraceField_3D_Tet_Tuple )
{
  // grid: four triangles, P1 (aka X1)
  XField3D_6Tet_X1_1Group xfld;

  // solution: P1 (aka Q1), which spans the space of the Linear solution

  Field_DG_Cell<PhysD3, TopoD3, Real> efld( xfld, 0, BasisFunctionCategory_Legendre );
  Field_DG_BoundaryTrace<PhysD3, TopoD3, Real> eBfld( xfld, 0, BasisFunctionCategory_Legendre );

  efld = 0;
  eBfld = 1;

  for_each_BoundaryFieldTraceGroup_Cell<TopoD3>::apply( DistributeBoundaryTraceField_Tuple<PhysD3>({0}), (xfld, efld) , eBfld );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  Field<PhysD3, TopoD3, Real>::template FieldCellGroupType<Tet>& efldCell = efld.getCellGroup<Tet>(0);
  typedef Field<PhysD3, TopoD3, Real>::template FieldCellGroupType<Tet>::template ElementType<> ElementEFieldClass;

  // element field variables
  ElementEFieldClass efldElem( efldCell.basis() );

  // loop over elements within group
  for (int elem = 0; elem < efld.nElem(); elem++)
  {
    // copy global grid/solution DOFs to element
    efldCell.getElement( efldElem, elem );
    if ((elem == 0) || (elem == 5))
      {SANS_CHECK_CLOSE( 3, efldElem.DOF(0), small_tol, close_tol );}
    if ( (elem == 1) || (elem == 4) )
      {SANS_CHECK_CLOSE( 1, efldElem.DOF(0), small_tol, close_tol );}
    if ( (elem == 3) || (elem == 2) )
      {SANS_CHECK_CLOSE( 2, efldElem.DOF(0), small_tol, close_tol );}
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DistributeBoundaryTraceField_3D_Hex )
{
  // grid: four triangles, P1 (aka X1)
  XField3D_2Hex_X1_1Group xfld;

  // solution: P1 (aka Q1), which spans the space of the Linear solution

  Field_DG_Cell<PhysD3, TopoD3, Real> efld( xfld, 0, BasisFunctionCategory_Legendre );
  Field_DG_BoundaryTrace<PhysD3, TopoD3, Real> eBfld( xfld, 0, BasisFunctionCategory_Legendre );

  efld = 0;
  eBfld = 1;

  for_each_BoundaryFieldTraceGroup_Cell<TopoD3>::apply( DistributeBoundaryTraceField<PhysD3>({0,1,2,3,4,5}), efld, eBfld );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  Field<PhysD3, TopoD3, Real>::template FieldCellGroupType<Hex>& efldCell = efld.getCellGroup<Hex>(0);
  typedef Field<PhysD3, TopoD3, Real>::template FieldCellGroupType<Hex>::template ElementType<> ElementEFieldClass;

  // element field variables
  ElementEFieldClass efldElem( efldCell.basis() );

  // loop over elements within group
  for (int elem = 0; elem < efld.nElem(); elem++)
  {
    // copy global grid/solution DOFs to element
    efldCell.getElement( efldElem, elem );
    SANS_CHECK_CLOSE( 5, efldElem.DOF(0), small_tol, close_tol );
  }
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE( DistributeBoundaryTraceField_3D_Hex_Tuple )
{
  // grid: four triangles, P1 (aka X1)
  XField3D_2Hex_X1_1Group xfld;

  // solution: P1 (aka Q1), which spans the space of the Linear solution

  Field_DG_Cell<PhysD3, TopoD3, Real> efld( xfld, 0, BasisFunctionCategory_Legendre );
  Field_DG_BoundaryTrace<PhysD3, TopoD3, Real> eBfld( xfld, 0, BasisFunctionCategory_Legendre );

  efld = 0;
  eBfld = 1;

  for_each_BoundaryFieldTraceGroup_Cell<TopoD3>::apply( DistributeBoundaryTraceField_Tuple<PhysD3>({0,1,2,3,4,5}), (xfld, efld) , eBfld );

  const Real small_tol = 1e-13;
  const Real close_tol = 1e-13;

  Field<PhysD3, TopoD3, Real>::template FieldCellGroupType<Hex>& efldCell = efld.getCellGroup<Hex>(0);
  typedef Field<PhysD3, TopoD3, Real>::template FieldCellGroupType<Hex>::template ElementType<> ElementEFieldClass;

  // element field variables
  ElementEFieldClass efldElem( efldCell.basis() );

  // loop over elements within group
  for (int elem = 0; elem < efld.nElem(); elem++)
  {
    // copy global grid/solution DOFs to element
    efldCell.getElement( efldElem, elem );
    SANS_CHECK_CLOSE( 5, efldElem.DOF(0), small_tol, close_tol );
  }
}
#endif
//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
