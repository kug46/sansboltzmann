// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// FieldAssociativitySpacetime_Pentatope_btest
// testing of FieldAssociativity class w/ pentatope

#include <boost/test/unit_test.hpp>
#include "SANS_btest.h"

#include <boost/test/output_test_stream.hpp>
using boost::test_tools::output_test_stream;

#include "tools/SANSnumerics.h"     // Real
#include "Topology/ElementTopology.h"
#include "BasisFunction/BasisFunctionSpacetime_Pentatope.h"

#include "Field/FieldGroupSpacetime_Traits.h"

using namespace std;
using namespace SANS;

// Explicitly instantiate the class to generate all the functions so that coverage
// information is correct
namespace SANS
{
template class FieldAssociativity<FieldGroupSpacetimeTraits<Pentatope, Real>>;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE(FieldAssociativitySpacetime_Pentatope_test_suite)

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE(statics_test)
{
  typedef FieldAssociativity< FieldGroupSpacetimeTraits<Pentatope, Real>> FieldSpacetimeClass;

  static_assert(std::is_same<typename FieldSpacetimeClass::FieldBase, FieldAssociativityBase<Real>>::value, "Incorrect Base type");
  static_assert(std::is_same<typename FieldSpacetimeClass::BasisType, BasisFunctionSpacetimeBase<Pentatope>>::value, "Incorrect Basis type");
  static_assert(std::is_same<typename FieldSpacetimeClass::TopologyType, Pentatope>::value, "Incorrect topology type");
  static_assert(std::is_same<typename FieldSpacetimeClass::FieldAssociativityConstructorType,
                              FieldAssociativityConstructor<ElementAssociativityConstructor<TopoD4, Pentatope>>>::value,
                 "Incorrect Associativity Constructor type");
}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE(constructor_test)
{
  typedef FieldAssociativity<FieldGroupSpacetimeTraits<Pentatope, Real>> FieldSpacetimeClass;
  typedef typename FieldSpacetimeClass::FieldAssociativityConstructorType FieldAssociativityConstructorType;
  typedef typename FieldSpacetimeClass::FieldBase FieldSpacetimeBase;

  const BasisFunctionSpacetimeBase<Pentatope> *basis= BasisFunctionSpacetimeBase<Pentatope>::LagrangeP1;

  ElementAssociativityConstructor<TopoD4, Pentatope> assoc1(basis);

  BOOST_CHECK_EQUAL(1, assoc1.order());
  BOOST_CHECK_EQUAL(5, assoc1.nNode());
  BOOST_CHECK_EQUAL(0, assoc1.nEdge());
  BOOST_CHECK_EQUAL(0, assoc1.nArea());
  BOOST_CHECK_EQUAL(0, assoc1.nFace());
  BOOST_CHECK_EQUAL(0, assoc1.nCell());

  assoc1.setRank(2);
  assoc1.setNodeGlobalMapping({3, 4, 5, 6, 7});

  int nElem= 1;
  FieldAssociativityConstructorType fldassoc1(basis, nElem);
  fldassoc1.setAssociativity(assoc1, 0);

  // constructor class
  FieldSpacetimeClass fld1(fldassoc1);

  BOOST_CHECK_EQUAL(1, fld1.nElem());
  BOOST_CHECK_EQUAL(5, fld1.nElemTrace());
  BOOST_CHECK_EQUAL(basis->nBasis(), fld1.nBasis());
  BOOST_CHECK_EQUAL(basis->order(), fld1.order());
  BOOST_CHECK_EQUAL(0, fld1.nDOF());

  BOOST_CHECK(fld1.topoTypeID() == typeid(Pentatope));

  // default constructor and operator=
  FieldSpacetimeClass fld2;

  BOOST_CHECK_EQUAL(0, fld2.nElem());
  BOOST_CHECK_EQUAL(0, fld2.nDOF());

  fld2= fld1;

  BOOST_CHECK_EQUAL(1, fld2.nElem());
  BOOST_CHECK_EQUAL(5, fld2.nElemTrace());
  BOOST_CHECK_EQUAL(basis->nBasis(), fld2.nBasis());
  BOOST_CHECK_EQUAL(basis->order(), fld2.order());
  BOOST_CHECK_EQUAL(0, fld2.nDOF());

  BOOST_CHECK(fld2.topoTypeID() == typeid(Pentatope));

  // copy constructor
  FieldSpacetimeClass fld3(fld1);

  BOOST_CHECK_EQUAL(1, fld3.nElem());
  BOOST_CHECK_EQUAL(5, fld3.nElemTrace());
  BOOST_CHECK_EQUAL(basis->nBasis(), fld3.nBasis());
  BOOST_CHECK_EQUAL(basis->order(), fld3.order());
  BOOST_CHECK_EQUAL(0, fld3.nDOF());

  BOOST_CHECK(fld3.topoTypeID() == typeid(Pentatope));

  // resize needed for new []
  FieldSpacetimeClass fld4;

  fld4.resize(fldassoc1);

  BOOST_CHECK_EQUAL(1, fld4.nElem());
  BOOST_CHECK_EQUAL(5, fld4.nElemTrace());
  BOOST_CHECK_EQUAL(basis->nBasis(), fld4.nBasis());
  BOOST_CHECK_EQUAL(basis->order(), fld4.order());
  BOOST_CHECK_EQUAL(0, fld4.nDOF());

  BOOST_CHECK(fld4.topoTypeID() == typeid(Pentatope));

  // clone
  FieldSpacetimeBase *fld5= fld1.clone();

  BOOST_CHECK_EQUAL(1, fld5->nElem());
  BOOST_CHECK_EQUAL(5, fld5->nElemTrace());
  BOOST_CHECK_EQUAL(basis->nBasis(), fld5->nBasis());
  BOOST_CHECK_EQUAL(basis->order(), fld5->order());
  BOOST_CHECK_EQUAL(0, fld5->nDOF());

  BOOST_CHECK(fld5->topoTypeID() == typeid(Pentatope));
  delete fld5;
}

BOOST_AUTO_TEST_CASE(basis_test)
{
  typedef FieldAssociativity<FieldGroupSpacetimeTraits<Pentatope, Real>> FieldSpacetimeClass;
  typedef typename FieldSpacetimeClass::FieldAssociativityConstructorType FieldAssociativityConstructorType;

  ElementAssociativityConstructor<TopoD4, Pentatope> assoc1(BasisFunctionSpacetimeBase<Pentatope>::LagrangeP1);
  assoc1.setRank(2);
  assoc1.setNodeGlobalMapping({3, 4, 5, 6, 7});

  int nElem= 1;
  FieldAssociativityConstructorType fldassoc1(BasisFunctionSpacetimeBase<Pentatope>::LagrangeP1, nElem);
  fldassoc1.setAssociativity(assoc1, 0);

  FieldSpacetimeClass fld(fldassoc1);

  BOOST_CHECK_EQUAL(1, fld.nElem());
  BOOST_CHECK_EQUAL(0, fld.nDOF());

  const BasisFunctionSpacetimeBase<Pentatope> *basis2= fld.basis();

  BOOST_CHECK_EQUAL(1, basis2->order());
  BOOST_CHECK_EQUAL(5, basis2->nBasis());
  BOOST_CHECK_EQUAL(5, basis2->nBasisNode());
  BOOST_CHECK_EQUAL(0, basis2->nBasisEdge());
  BOOST_CHECK_EQUAL(0, basis2->nBasisArea());
  BOOST_CHECK_EQUAL(0, basis2->nBasisFace());
  BOOST_CHECK_EQUAL(0, basis2->nBasisCell());
  BOOST_CHECK_EQUAL(basis2, BasisFunctionSpacetimeBase<Pentatope>::LagrangeP1);

}

BOOST_AUTO_TEST_CASE(DOF_associativity_test)
{
  typedef FieldAssociativity<FieldGroupSpacetimeTraits<Pentatope, Real>> FieldSpacetimeClass;
  typedef typename FieldSpacetimeClass::FieldAssociativityConstructorType FieldAssociativityConstructorType;

  ElementAssociativityConstructor<TopoD4, Pentatope> assoc1(BasisFunctionSpacetimeBase<Pentatope>::LagrangeP1);

  BOOST_CHECK_EQUAL(1, assoc1.order());
  BOOST_CHECK_EQUAL(5, assoc1.nNode());
  BOOST_CHECK_EQUAL(0, assoc1.nEdge());
  BOOST_CHECK_EQUAL(0, assoc1.nArea());
  BOOST_CHECK_EQUAL(0, assoc1.nFace());
  BOOST_CHECK_EQUAL(0, assoc1.nCell());

  assoc1.setRank(2);
  assoc1.setNodeGlobalMapping({3, 4, 5, 6, 7});

  int nElem= 1;
  FieldAssociativityConstructorType fldassoc1(BasisFunctionSpacetimeBase<Pentatope>::LagrangeP1, nElem);
  fldassoc1.setAssociativity(assoc1, 0);

  FieldSpacetimeClass fld1(fldassoc1);

  BOOST_CHECK_EQUAL(1, fld1.nElem());
  BOOST_CHECK_EQUAL(0, fld1.nDOF());

  int node1[5];
  fld1.associativity(0).getNodeGlobalMapping(node1, 5);
  BOOST_CHECK_EQUAL(3, node1[0]);
  BOOST_CHECK_EQUAL(4, node1[1]);
  BOOST_CHECK_EQUAL(5, node1[2]);
  BOOST_CHECK_EQUAL(6, node1[3]);
  BOOST_CHECK_EQUAL(7, node1[4]);

  ElementAssociativityConstructor<TopoD4, Pentatope> assoc2(BasisFunctionSpacetimeBase<Pentatope>::LagrangeP2);

  BOOST_CHECK_EQUAL(2, assoc2.order());
  BOOST_CHECK_EQUAL(5, assoc2.nNode());
  BOOST_CHECK_EQUAL(10, assoc2.nEdge());
  BOOST_CHECK_EQUAL(0, assoc2.nArea());
  BOOST_CHECK_EQUAL(0, assoc2.nFace());
  BOOST_CHECK_EQUAL(0, assoc2.nCell());

  assoc2.setRank(2);
  assoc2.setNodeGlobalMapping({3, 4, 5, 6, 7});
  assoc2.setEdgeGlobalMapping({8, 9, 10, 11, 12, 13, 14, 15, 16, 17});

  nElem= 1;
  FieldAssociativityConstructorType fldassoc2(BasisFunctionSpacetimeBase<Pentatope>::LagrangeP2, nElem);
  fldassoc2.setAssociativity(assoc2, 0);

  FieldSpacetimeClass fld2(fldassoc2);

  BOOST_CHECK_EQUAL(1, fld2.nElem());
  BOOST_CHECK_EQUAL(0, fld2.nDOF());

  int node2[5];
  int edge2[10];

  fld2.associativity(0).getNodeGlobalMapping(node2, 5);
  BOOST_CHECK_EQUAL(3, node2[0]);
  BOOST_CHECK_EQUAL(4, node2[1]);
  BOOST_CHECK_EQUAL(5, node2[2]);
  BOOST_CHECK_EQUAL(6, node2[3]);
  BOOST_CHECK_EQUAL(7, node2[4]);

  fld2.associativity(0).getEdgeGlobalMapping(edge2, 10);
  BOOST_CHECK_EQUAL(8, edge2[0]);
  BOOST_CHECK_EQUAL(9, edge2[1]);
  BOOST_CHECK_EQUAL(10, edge2[2]);
  BOOST_CHECK_EQUAL(11, edge2[3]);
  BOOST_CHECK_EQUAL(12, edge2[4]);
  BOOST_CHECK_EQUAL(13, edge2[5]);
  BOOST_CHECK_EQUAL(14, edge2[6]);
  BOOST_CHECK_EQUAL(15, edge2[7]);
  BOOST_CHECK_EQUAL(16, edge2[8]);
  BOOST_CHECK_EQUAL(17, edge2[9]);
}

BOOST_AUTO_TEST_CASE(accessor_test)
{
  typedef DLA::VectorS<2, Real> ArrayQ;

  typedef FieldAssociativity<FieldGroupSpacetimeTraits<Pentatope, ArrayQ>> FieldSpacetimeClass;
  typedef typename FieldSpacetimeClass::FieldAssociativityConstructorType FieldAssociativityConstructorType;

  ElementAssociativityConstructor<TopoD4, Pentatope> assoc1(BasisFunctionSpacetimeBase<Pentatope>::LagrangeP1);
  assoc1.setRank(2);
  assoc1.setNodeGlobalMapping({0, 1, 2, 3, 4});

  int nElem= 1;
  FieldAssociativityConstructorType fldassoc1(BasisFunctionSpacetimeBase<Pentatope>::LagrangeP1, nElem);
  fldassoc1.setAssociativity(assoc1, 0);

  FieldSpacetimeClass qfld(fldassoc1);

  BOOST_CHECK_EQUAL(1, qfld.nElem());
  BOOST_CHECK_EQUAL(0, qfld.nDOF());

  ArrayQ *qDOF= new ArrayQ[5];

  qfld.setDOF(qDOF, 5);
  BOOST_CHECK_EQUAL(5, qfld.nDOF());

  ArrayQ q1= {1, 2};
  ArrayQ q2= {3, 4};
  ArrayQ q3= {5, 6};
  ArrayQ q4= {7, 8};
  ArrayQ q5= {9, 10};

  qfld.DOF(0)= q1;
  qfld.DOF(1)= q2;
  qfld.DOF(2)= q3;
  qfld.DOF(3)= q4;
  qfld.DOF(4)= q5;

  const Real tol= 1e-13;
  ArrayQ q;

  for (int k= 0; k < 2; k++)
  {
    q= qfld.DOF(0);
    BOOST_CHECK_CLOSE(q1[k], q[k], tol);
    BOOST_CHECK_CLOSE(qDOF[0][k], q[k], tol);
    q= qfld.DOF(1);
    BOOST_CHECK_CLOSE(q2[k], q[k], tol);
    BOOST_CHECK_CLOSE(qDOF[1][k], q[k], tol);
    q= qfld.DOF(2);
    BOOST_CHECK_CLOSE(q3[k], q[k], tol);
    BOOST_CHECK_CLOSE(qDOF[2][k], q[k], tol);
    q= qfld.DOF(3);
    BOOST_CHECK_CLOSE(q4[k], q[k], tol);
    BOOST_CHECK_CLOSE(qDOF[3][k], q[k], tol);
    q= qfld.DOF(4);
    BOOST_CHECK_CLOSE(q5[k], q[k], tol);
    BOOST_CHECK_CLOSE(qDOF[4][k], q[k], tol);
  }

  // const accessors
  const FieldSpacetimeClass qfld2= qfld;

  for (int k= 0; k < 2; k++)
  {
    q= qfld2.DOF(0);
    BOOST_CHECK_CLOSE(q1[k], q[k], tol);
    BOOST_CHECK_CLOSE(qDOF[0][k], q[k], tol);
    q= qfld2.DOF(1);
    BOOST_CHECK_CLOSE(q2[k], q[k], tol);
    BOOST_CHECK_CLOSE(qDOF[1][k], q[k], tol);
    q= qfld2.DOF(2);
    BOOST_CHECK_CLOSE(q3[k], q[k], tol);
    BOOST_CHECK_CLOSE(qDOF[2][k], q[k], tol);
    q= qfld2.DOF(3);
    BOOST_CHECK_CLOSE(q4[k], q[k], tol);
    BOOST_CHECK_CLOSE(qDOF[3][k], q[k], tol);
    q= qfld2.DOF(4);
    BOOST_CHECK_CLOSE(q5[k], q[k], tol);
    BOOST_CHECK_CLOSE(qDOF[4][k], q[k], tol);
  }

  delete [] qDOF;

}

BOOST_AUTO_TEST_CASE(elementFieldP1)
{
  typedef DLA::VectorS<2, Real> ArrayQ;

  typedef FieldAssociativity<FieldGroupSpacetimeTraits<Pentatope, ArrayQ>> FieldSpacetimeClass;
  typedef typename FieldSpacetimeClass::FieldAssociativityConstructorType FieldAssociativityConstructorType;
  typedef typename FieldSpacetimeClass::template ElementType<> ElementSpacetimeClass;
  typedef std::array<int, 5> Int5;

  const Real tol= 1e-13;

  // DOF associativity
  ElementAssociativityConstructor<TopoD4, Pentatope> assoc(BasisFunctionSpacetimeBase<Pentatope>::LagrangeP1);

  BOOST_CHECK_EQUAL(1, assoc.order());
  BOOST_CHECK_EQUAL(5, assoc.nNode());
  BOOST_CHECK_EQUAL(0, assoc.nEdge());
  BOOST_CHECK_EQUAL(0, assoc.nArea());
  BOOST_CHECK_EQUAL(0, assoc.nFace());
  BOOST_CHECK_EQUAL(0, assoc.nCell());

  assoc.setRank(2);

  int node[5]= {3, 4, 5, 6, 7};
  assoc.setNodeGlobalMapping(node, 5);

  Int5 faceSign= {{+1, -1, +1, -1, +1}};
  assoc.faceSign()= faceSign;

  int nElem= 1;
  FieldAssociativityConstructorType fldassoc(BasisFunctionSpacetimeBase<Pentatope>::LagrangeP1, nElem);
  fldassoc.setAssociativity(assoc, 0);

  FieldSpacetimeClass qfld(fldassoc);

  BOOST_CHECK_EQUAL(1, qfld.nElem());
  BOOST_CHECK_EQUAL(0, qfld.nDOF());

  // solution DOFs
  ArrayQ q1= {1, 2};
  ArrayQ q2= {3, 4};
  ArrayQ q3= {5, 6};
  ArrayQ q4= {7, 8};
  ArrayQ q5= {9, 10};

  ArrayQ *qDOF= new ArrayQ[10];

  qfld.setDOF(qDOF, 10);
  BOOST_CHECK_EQUAL(10, qfld.nDOF());

  qfld.DOF(node[0])= q1;
  qfld.DOF(node[1])= q2;
  qfld.DOF(node[2])= q3;
  qfld.DOF(node[3])= q4;
  qfld.DOF(node[4])= q5;

  // get element DOFs

  ElementSpacetimeClass qfldElem(qfld.basis());

  qfld.getElement(qfldElem, 0);

  BOOST_CHECK_EQUAL(1, qfldElem.order());
  BOOST_CHECK_EQUAL(5, qfldElem.nDOF());

  ArrayQ q;
  for (int k= 0; k < 2; k++)
  {
    q= qfldElem.DOF(0);
    BOOST_CHECK_CLOSE(q1[k], q[k], tol);
    q= qfldElem.DOF(1);
    BOOST_CHECK_CLOSE(q2[k], q[k], tol);
    q= qfldElem.DOF(2);
    BOOST_CHECK_CLOSE(q3[k], q[k], tol);
    q= qfldElem.DOF(3);
    BOOST_CHECK_CLOSE(q4[k], q[k], tol);
    q= qfldElem.DOF(4);
    BOOST_CHECK_CLOSE(q5[k], q[k], tol);
  }

  // set element DOFs

  ArrayQ q1b= {7, 9};
  ArrayQ q2b= {4, 3};
  ArrayQ q3b= {2, 5};
  ArrayQ q4b= {1, 0};
  ArrayQ q5b= {8, 6};

  qfldElem.DOF(0)= q1b;
  qfldElem.DOF(1)= q2b;
  qfldElem.DOF(2)= q3b;
  qfldElem.DOF(3)= q4b;
  qfldElem.DOF(4)= q5b;

  qfld.setElement(qfldElem, 0);

  for (int k= 0; k < 2; k++)
  {
    q= qfld.DOF(node[0]);
    BOOST_CHECK_CLOSE(q1b[k], q[k], tol);
    q= qfld.DOF(node[1]);
    BOOST_CHECK_CLOSE(q2b[k], q[k], tol);
    q= qfld.DOF(node[2]);
    BOOST_CHECK_CLOSE(q3b[k], q[k], tol);
    q= qfld.DOF(node[3]);
    BOOST_CHECK_CLOSE(q4b[k], q[k], tol);
    q= qfld.DOF(node[4]);
    BOOST_CHECK_CLOSE(q5b[k], q[k], tol);
  }

  // edge signs
  Int5 faceSign2= qfldElem.faceSign();

  BOOST_CHECK_EQUAL(+1, faceSign2[0]);
  BOOST_CHECK_EQUAL(-1, faceSign2[1]);
  BOOST_CHECK_EQUAL(+1, faceSign2[2]);
  BOOST_CHECK_EQUAL(-1, faceSign2[3]);
  BOOST_CHECK_EQUAL(+1, faceSign2[4]);

  delete [] qDOF;

}

BOOST_AUTO_TEST_CASE(elementFieldP3)
{
  typedef DLA::VectorS<2, Real> ArrayQ;

  typedef FieldAssociativity<FieldGroupSpacetimeTraits<Pentatope, ArrayQ>> FieldSpacetimeClass;
  typedef typename FieldSpacetimeClass::FieldAssociativityConstructorType FieldAssociativityConstructorType;
  typedef typename FieldSpacetimeClass::template ElementType<> ElementSpacetimeClass;
  typedef std::array<int, 5> Int5;

  const Real tol= 1e-13;

  // DOF associativity
  ElementAssociativityConstructor<TopoD4, Pentatope> assoc(BasisFunctionSpacetimeBase<Pentatope>::LagrangeP3);

  BOOST_CHECK_EQUAL(3, assoc.order());
  BOOST_CHECK_EQUAL(5, assoc.nNode());
  BOOST_CHECK_EQUAL(20, assoc.nEdge());
  BOOST_CHECK_EQUAL(10, assoc.nArea());
  BOOST_CHECK_EQUAL(0, assoc.nFace());
  BOOST_CHECK_EQUAL(0, assoc.nCell());

  assoc.setRank(2);

  int node[5]= {3, 10, 20, 6, 29};
  assoc.setNodeGlobalMapping(node, 5);

  int edge[20]= {8, 9, 4, 2, 12, 13, 14, 15, 16, 17, 18, 19, 5, 21, 22, 23, 24, 25, 26, 27};
  assoc.setEdgeGlobalMapping(edge, 20);

  int area[10]= {28, 7, 30, 31, 32, 33, 34, 11, 1, 0};
  assoc.setAreaGlobalMapping(area, 10);

  Int5 faceSign= {{+1, -1, +1, -1, +1}};
  assoc.faceSign()= faceSign;

  int nElem= 1;
  FieldAssociativityConstructorType fldassoc(BasisFunctionSpacetimeBase<Pentatope>::LagrangeP3, nElem);
  fldassoc.setAssociativity(assoc, 0);

  FieldSpacetimeClass qfld(fldassoc);

  BOOST_CHECK_EQUAL(1, qfld.nElem());
  BOOST_CHECK_EQUAL(0, qfld.nDOF());

  // solution DOFs
  ArrayQ qTable[35]= {{13, 1}, {2, 9}, {6, 8}, {11, 16}, {20, 16},
                 {20, 4}, {4, 10}, {20, 9}, {20, 13}, {10, 15},
                 {17, 16}, {3, 6}, {9, 14}, {19, 14}, {16, 4},
                 {20, 3}, {14, 10}, {1, 20}, {17, 7}, {19, 12},
                 {14, 5}, {16, 16}, {15, 6}, {8, 11}, {14, 14},
                 {4, 18}, {15, 20}, {1, 11}, {6, 3}, {1, 3},
                 {2, 6}, {17, 17}, {14, 6}, {7, 17}, {20, 5}};

  ArrayQ *qDOF= new ArrayQ[35];

  qfld.setDOF(qDOF, 35);
  BOOST_CHECK_EQUAL(35, qfld.nDOF());

  for (int j= 0; j < 5; j++)
  {
    qfld.DOF(node[j])= qTable[j];
  }
  for (int j= 0; j < 20; j++)
  {
    qfld.DOF(edge[j])= qTable[5 + j];
  }
  for (int j= 0; j < 10; j++)
  {
    qfld.DOF(area[j])= qTable[25 + j];
  }

  // get element DOFs

  ElementSpacetimeClass qfldElem(qfld.basis());

  qfld.getElement(qfldElem, 0);

  BOOST_CHECK_EQUAL(3, qfldElem.order());
  BOOST_CHECK_EQUAL(35, qfldElem.nDOF());

  ArrayQ q;
  for (int j= 0; j < 35; j++)
  {
    for (int k= 0; k < 2; k++)
    {
      q= qfldElem.DOF(j);
      BOOST_CHECK_CLOSE(qTable[j][k], q[k], tol);
    }
  }

  // set element DOFs

  for (int n= 0; n < 35; n++)
  {
    qfldElem.DOF(n)= 0;

    q= qfldElem.DOF(n);
    BOOST_CHECK_SMALL(q[0], tol);
    BOOST_CHECK_SMALL(q[1], tol);
  }

  for (int j= 0; j < 5; j++)
    qfldElem.DOF(node[j])= qTable[j];
  for (int j= 0; j < 20; j++)
    qfldElem.DOF(edge[j])= qTable[5 + j];
  for (int j= 0; j < 10; j++)
    qfldElem.DOF(area[j])= qTable[25 + j];

  for (int k= 0; k < 2; k++)
  {
    for (int j= 0; j < 5; j++)
    {
      q= qfld.DOF(node[j]);
      BOOST_CHECK_CLOSE(qTable[j][k], q[k], tol);
    }
    for (int j= 0; j < 20; j++)
    {
      q= qfld.DOF(edge[j]);
      BOOST_CHECK_CLOSE(qTable[j + 5][k], q[k], tol);
    }
    for (int j= 0; j < 10; j++)
    {
      q= qfld.DOF(area[j]);
      BOOST_CHECK_CLOSE(qTable[j + 25][k], q[k], tol);
    }
  }

  delete [] qDOF;

}

//----------------------------------------------------------------------------//
BOOST_AUTO_TEST_CASE(IO_test)
{
  // set the 2nd argument to false to regenerate the pattern file
  output_test_stream output("IO/Field/FieldAssociativitySpacetime_Pentatope_pattern.txt", true);

  typedef DLA::VectorS<2, Real> ArrayQ;
  typedef FieldAssociativity<FieldGroupSpacetimeTraits<Pentatope, ArrayQ>> FieldSpacetimeClass;
  typedef typename FieldSpacetimeClass::FieldAssociativityConstructorType FieldAssociativityConstructorType;
  typedef std::array<int, 5> Int5;

  // DOF associativity
  ElementAssociativityConstructor<TopoD4, Pentatope> assoc(BasisFunctionSpacetimeBase<Pentatope>::LagrangeP1);
  assoc.setRank(2);
  int node[5]= {0, 1, 2, 3, 4};
  assoc.setNodeGlobalMapping(node, 5);

  Int5 faceSign= {{+1, -1, +1, -1, +1}};
  assoc.faceSign()= faceSign;

  int nElem= 1;
  FieldAssociativityConstructorType fldassoc(BasisFunctionSpacetimeBase<Pentatope>::LagrangeP1, nElem);
  fldassoc.setAssociativity(assoc, 0);

  FieldSpacetimeClass qfld(fldassoc);

  // solution DOFs
  ArrayQ q1= {1, 2};
  ArrayQ q2= {3, 4};
  ArrayQ q3= {5, 6};
  ArrayQ q4= {7, 8};
  ArrayQ q5= {9, 10};

  ArrayQ* qDOF= new ArrayQ[5];

  qfld.setDOF(qDOF, 5);
  BOOST_CHECK_EQUAL(5, qfld.nDOF());

  qfld.DOF(node[0])= q1;
  qfld.DOF(node[1])= q2;
  qfld.DOF(node[2])= q3;
  qfld.DOF(node[3])= q4;
  qfld.DOF(node[4])= q5;

  qfld.dump(2, output);
  BOOST_CHECK(output.match_pattern());

  delete [] qDOF;
}

//############################################################################//
BOOST_AUTO_TEST_SUITE_END()
